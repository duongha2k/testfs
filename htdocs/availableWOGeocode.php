<?php
	$page = "login";
	if (!$argc)
		require_once("headerStartSession.php");
	require_once("library/timeStamps2.php");
	require_once("library/caspioAPI.php");
	require_once("library/googleMapAPI.php");
	require_once("library/smtpMail.php");
	require_once("library/woACSTimeStamps.php");
	require_once("library/recruitmentEmailLib.php");
    require ("clients/autoassign/includes/AutoAssign.php");
	    
    AutoAssign::$baseDir = dirname(__FILE__) . '/clients/autoassign';

	$dateQuery = "DATEPART(mm, DateEntered) = '" . date("m") . "' AND DATEPART(dd, DateEntered) = '" . date("d") . "' AND DATEPART(yyyy, DateEntered) = '" . date("Y") . "'";
	
	// time stamp
	
	ini_set("display_errors",1);
	$woToTimeStamp = caspioSelectAdv("Work_Orders", "TB_UNID, UserName, Company_ID, Company_Name, Project_Name, ShowTechs, ISNULL(Tech_ID, ''), isProjectAutoAssign, ISNULL(FLS_ID,'')", "Deactivated = '0' AND Latitude IS NULL AND $dateQuery AND NOT EXISTS (SELECT WO_UNID FROM Work_Order_Timestamps WHERE TB_UNID = WO_UNID)", "", false, "`", "|", false);
		
//	print_r($woToTimeStamp);
	foreach ($woToTimeStamp as $wo) {
		if ($wo == "") continue;
		$wo = explode("|", $wo);
		$tbUNID = $wo[0];
		$username = trim($wo[1], "`");
		$companyID = trim($wo[2], "`");
		$companyname = trim($wo[3], "`");
		$projectName = trim($wo[4], "`");
		$showTechs = trim($wo[5], "`");
		$techID = trim($wo[6], "`");
		$isProjectAutoAssign = trim($wo[7], "`");
		$isProjectAutoAssign = $isProjectAutoAssign == "True";
		$flsID = trim($wo[8], "`");
//		echo "$tbUNID, $username, $companyID, $companyname, $projectName, $showTechs<br/>";
		createWOACSTimeStamp($tbUNID);
		Create_TimeStamp($tbUNID, $username, "Work Order Created (imported)", $companyID, $companyname, $projectName, "", "", "");
		if ($showTechs == "True") {
			Create_TimeStamp($tbUNID, $username, "Work Order Published", $companyID, $companyname, $projectName, "", "", "");
                        $NoticeClass = new Core_Api_NoticeClass();
                        $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_PUBLISHED);
			if ($techID == 0 && $isProjectAutoAssign) {
				
				AutoAssign::proccessAutoAssign($tbUNID);
			}
		}
		if ($techID != 0 || !empty($flsID)){
			Create_TimeStamp($tbUNID, $username, "Work Order Assigned", $companyID, $companyname, $projectName, "", "", "");
                        $NoticeClass = new Core_Api_NoticeClass();
                        $NoticeClass->createWOChangeStatusLog($tbUNID,Core_Api_NoticeClass::WOStatusID_FOR_ASSIGNED);
	}
	}
	
	// find WO that need geocode	
	$woLocation = caspioSelectAdv("Work_Orders", "TB_UNID, Address, ZipCode", "Deactivated = '0' AND Latitude IS NULL AND ISNULL(ZipCode,'') <> ''", "", false, "`", "|", false);
//	print_r($woLocation);
//	die();

/*	$woLocation = caspioSelectAdv("Work_Orders", "TOP 5 TB_UNID, Address, ZipCode, Latitude, Longitude", "Deactivated = '0' AND ISNULL(Tech_ID, '') = '' AND Latitude IS NULL AND ZipCode IS NOT NULL", "", false, "`", "|", false);
	print_r($woLocation);
	die();*/
	
//	if (sizeof($woLocation) == 1 && $woLocation[0] == "")
//		die("No WOs Found");
	
//	die();
	$cachedCoord = array();

	$wosImported = array();
	$total = 0;
	
	foreach ($woLocation as $wo) {
		if ($wo == "") continue;
		$wo = explode("|", $wo);
		$tbunid = $wo[0];
		$address = trim($wo[1], "`");
		$zip = trim($wo[2], "`");
		$wosImported[] = $tbunid;
		$location = "$address $zip";
//		$coord = geocodeAddress($location);
		if (!array_key_exists($zip, $cachedCoord))
			$coord = geocodeAddress($zip);
		else
			$coord = $cachedCoord[$zip];
		if (!$argc)
			echo "Looking up $zip: ";
		if ($coord) {
			if (!array_key_exists($zip, $cachedCoord))
				$cachedCoord[$zip] = $coord;
			if (!$argc)
				echo "Latitude = '{$coord[0]}', Longitude = '{$coord[1]}'";
//			echo "UPDATE Work_Orders SET Latitude = '{$coord[0]}', Longitude = '{$coord[1]}' WHERE TB_UNID = '$tbunid'";
			if (caspioUpdate("Work_Orders", "Latitude, Longitude", "'{$coord[0]}', '{$coord[1]}'", "TB_UNID = '$tbunid'", false))
				if (!$argc)
					echo " ... Update done.";
		}
		else if (!$argc)
			echo "Not Found";
		$total = sizeof($woLocation);
		if (!$argc)
			echo "<br/>";
	}
	
	// Pull in Additional Tech Info
//	$techInfoPulled = caspioUpdate("Work_Orders", "Tech_FName, Tech_LName, TechEmail, TechPhone", "(SELECT TOP 1 FirstName FROM TR_Master_List WHERE TechID = Tech_ID), (SELECT TOP 1 LastName FROM TR_Master_List WHERE TechID = Tech_ID), (SELECT TOP 1 PrimaryEmail FROM TR_Master_List WHERE TechID = Tech_ID),(SELECT TOP 1 PrimaryPhone FROM TR_Master_List WHERE TechID = Tech_ID)", "ISNULL(Tech_ID, '') <> '' AND (ISNULL(Tech_FName, '') = '' OR ISNULL(TechEmail,'') = '' OR ISNULL(TechPhone,'') = '')");
	
	$idCrit = "TechID = Tech_ID OR (FLSID IS NOT NULL AND FLSID != '' AND FLSID = FLS_ID)";
		
	$techInfoPulled = caspioUpdate("Work_Orders", "Tech_FName, Tech_LName, TechEmail, TechPhone, FLS_ID, Tech_ID", "(SELECT TOP 1 FirstName FROM TR_Master_List WHERE $idCrit), (SELECT TOP 1 LastName FROM TR_Master_List WHERE $idCrit), (SELECT TOP 1 PrimaryEmail FROM TR_Master_List WHERE $idCrit),(SELECT TOP 1 PrimaryPhone FROM TR_Master_List WHERE $idCrit),(SELECT TOP 1 FLSID FROM TR_Master_List WHERE  $idCrit),(SELECT TOP 1 TechID FROM TR_Master_List WHERE  $idCrit)", "(ISNULL(Tech_ID, '') <> '' OR ISNULL(FLS_ID, '') <> '') AND (ISNULL(Tech_FName, '') = '' OR ISNULL(TechEmail,'') = '' OR ISNULL(TechPhone,'') = '') AND TechPaid = '0'");
	
	$message = "Total Imported: $total *** Total tech info pulled: $techInfoPulled";

	// Auto Blast
	$idList = array();
	if ($total > 0) {
		$wosImported = "'" . implode("','", $wosImported) . "'";
		$wosToBlast = caspioSelectAdv("Work_Orders", "TB_UNID, Company_ID", "TB_UNID IN ($wosImported) AND ShowTechs = '1' AND ISNULL(Tech_ID, '') = '' AND $dateQuery", "", false, "`", "|", false);
	
		foreach ($wosToBlast as $wos) {
			if ($wos == "") continue;
			$wos = explode("|", $wos);
			$tbunid = $wos[0];
			$companyID = trim($wos[1], "`");
			if (!array_key_exists($companyID, $idList))
				$idList[$companyID] = array();
			$idList[$companyID][] = $tbunid;
		}
		
//		if (sizeof($wosToBlast) != 1 || $wosToBlast[0] != "")
			$message .= "TB_UNID IN ($wosImported) AND ShowTechs = '1' AND ISNULL(Tech_ID, '') = '' AND $dateQuery" . " *** Num WOs Blasted: " . sizeof($wosToBlast);

		foreach ($idList as $companyID => $list) {
			$wosToBlast = getWOForRecruitmentEmailById($list);
			blastRecruitmentEmailMainSite($wosToBlast, $companyID);
		}
	}	
	
	if ($argc) {
		smtpMailLogReceived("availableWOGeocode Script", "nobody@fieldsolutions.com", "tngo@fieldsolutions.com", "Main site After Import Ran", $message, $message, "availableWOGeocode.php");
	}
?>
