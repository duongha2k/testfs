<script type="text/javascript">
//alert("update_projects.php");
</script>

<?php
//ini_set("display_errors", 1);

// update_projects.php
// by Jeff Coleman, 5/5/2008

// This code queries the Projects list and creates a javascript array with the results.  It creates a second array of default amounts which correspond with the projects.

//require_once("../library/caspioAPI.php");

// $projectTable = "FLS_Projects"
// $projectID = "ProjectNo"
// $defaultAmount = "Default_Amount"

$results = caspioSelect($projectTable, "$projectID, $defaultAmount", "$projectID > 0", "$projectID ASC", false);

$count = sizeof($results);
//echo "NUMBER OF RESULTS: $count<br />";

echo "<script type='text/javascript'>";
echo "
	var i=0;
	var projectIDs = new Array();
	var defaultAmounts = new Array();
";

	foreach ($results as $bid) {
		// echo "$bid<br />";
	
		$parts = explode(",",$bid);
		$projectID = $parts[0];
		$defaultAmount = $parts[1];

		echo "try {";
			echo "defaultAmounts[i] = $defaultAmount;";
		echo "}";
		echo "catch(err) {";
			echo "defaultAmounts[i] = 0;";
//			echo "document.writeln('error' + defaultAmounts[i] + ' ' + defaultAmounts.length);";
		echo "}";
		
		echo "projectIDs[i] = $projectID;";

		echo "i++;";

	}
	
//	echo "document.write(defaultAmounts.length);";
echo "</script>";

?>


<script type="text/javascript" src="/update_projects.js">
</script>

