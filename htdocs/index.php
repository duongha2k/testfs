<?php
// Get Subdomain
$urlParts = explode('.', $_SERVER['HTTP_HOST']);
$template = $urlParts[0];
$checkIndexDisplay = $template;
if ($checkIndexDisplay == "dev" || $checkIndexDisplay == "new") $checkIndexDisplay = "field";

if($checkIndexDisplay=='fieldsolutions'){
	header('Location: /');
}else if($checkIndexDisplay=='fls'){
	header('Location: clients/');
}

require_once("headerStartSession.php");
/*$cookie_path = "/";
$cookie_timeout = "0";
$cookie_domain = ".fieldsolutions.com";
session_set_cookie_params($cookie_timeout, $cookie_path, $cookie_domain);
session_name('fsTemplate');
session_start();*/

if ($checkIndexDisplay == "www" || $checkIndexDisplay == "test") { // IF WWW
	header("Location: /wp" . (empty($_GET['r']) ? '' : '/?r=' . urlencode($_GET['r'])));
	exit;
//    require ("includes/indexClientTech.php");
} else if ($checkIndexDisplay == "admin") { // IF ADMIN
	require ("includes/indexAdmin.php");
} else if ($checkIndexDisplay == "ruo") { // IF RUO
	require ("templates/ruo/includes/header.pp");
	require ("templates/ruo/includes/indexRUO_Tech.php");
} else if ($checkIndexDisplay == "wap") { // IF ADMIN
	require ("WAP_Detect.php");
	require ("includes/indexWAP.php");
	
}else{
	header("Location: /clients/logIn.php" . (empty($_GET['r']) ? '' : '?r=' . urlencode($_GET['r'])));
	exit;
}

require ("footer.php");
