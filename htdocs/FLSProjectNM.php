<?php	//FLS_ProjectsReport.php
		// This report was created by GB in order to test how many projects per project
		// for comparision with the FLS Migration.

require_once("library/caspioAPI.php");

ini_set("display_errors", 1);
set_time_limit(1600);

$pid = "(SELECT TOP 1 Project_ID FROM TR_Client_Projects WHERE Project_Name = ProjectName AND Project_Company_ID = 'FLS')";
$count = "(SELECT COUNT(WorkOrderID) FROM FLS_Work_Orders WHERE FLS_Work_Orders.Project_ID = FLS_Projects.ProjectNo)";
$count2 = "(SELECT COUNT(TB_UNID) FROM Work_Orders WHERE Work_Orders.Project_ID = ($pid))";
$flsActive = "(SELECT TOP 1 WorkOrderID FROM FLS_Work_Orders WHERE ISNULL(FLS_Work_Orders.Deactivated, '') != 'Yes' AND ISNULL(Kickback, '0') = '0' AND ISNULL(DeclinedWorkOrder, '0') = '0' AND ISNULL(PayApprove, '0') = '0' AND Project_ID = ProjectNo)";
//$FLS_Projects = caspioSelectAdv("FLS_Projects", "ProjectNo, ProjectName, $pid, $count, $count2", "ProjectNo != '72' AND ProjectNo != '123' AND (ProjectNo = '114' OR ProjectNo = '13' OR $count != $count2 OR EXISTS ($flsActive))","$count DESC", false, "`", "|^",false);

$FLS_Projects = caspioSelectAdv("FLS_Projects", "ProjectNo, ProjectName, $pid, $count, $count2", "ProjectNo IN ('18','2','4','17','49','100','46','113','9','12','7','75','23','85','19','29','13','126','131','133','121','114','132','118','111')","$count DESC", false, "`", "|^",false);

echo "<table border=\"1\">";
echo "<tr style=\"font-weight:bold;\"><td>Project ID</td><td>Real Project ID</td><td>Project</td><td>WO's</td><td align=\"right\">Migrated WO's</td></tr>";
$sub1 = 0;
$sub2 = 0;

$code = "";
$code2 = array();

foreach ($FLS_Projects as $project) {	
	$fields = explode("|^", $project);
	
	$ProjectID = trim($fields[0], "`"); // Remove quotes	
	$Project = trim($fields[1], "`"); // Remove quotes	
	$mpid = trim($fields[2], "`");
	$FLS_Orders = trim($fields[3], "`");
	$Main_Orders = trim($fields[4], "`");
	$code .= "migrateWorkOrders('$ProjectID', '$mpid');<br/>";
	$code2[] = "$ProjectID";

//	$FLS_Orders = caspioSelectAdv("FLS_Work_Orders", "count(*)", "Project_ID = '$ProjectID'","", false, "`", "|^",false);
	//$qty = trim($FLS_Orders[0], "`"); // Remove quotes	
	$sub1 += $FLS_Orders;
	$sub2 += $Main_Orders;
//	$Active = caspioSelectAdv("FLS_Work_Orders", "COUNT(*)", "ISNULL(Deactivated, '') != 'Yes' AND ISNULL(Kickback, '0') = '0' AND ISNULL(DeclinedWorkOrder, '0') = '0' AND ISNULL(PayApprove, '0') = '0' AND Project_ID = '$ProjectID'", "", false, "`", "|^",false);
//	$sub2 += $Active[0];
//	if ($Active[0] > 0) {
//		echo "<tr><td><b>$ProjectID</b></td><td><b>$Project</b></td><td align=\"right\"><b>".number_format($FLS_Orders[0])."</b></td><td align=\"right\"><b>".number_format($Active[0])."</b></td></tr>";
//	} else {
		echo "<tr><td>$ProjectID</td><td>$mpid</td><td>$Project</td><td>".number_format($FLS_Orders)."</td><td align=\"right\">".number_format($Main_Orders)."</td></tr>";
//	}
	ob_flush();
	flush();
}

$code2 = "('" . implode("','", $code2) . "')";
echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><b>".number_format($sub1)."</b></td><td align=\"right\"><b>".number_format($sub2)."</b></td></tr>";
echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td align=\"right\">&nbsp;</td></tr>";

$FLS_Orders = caspioSelectAdv("FLS_Work_Orders", "count(*)", "ISNULL(Project_ID, '') = ''","", false, "`", "|^",false);
$Project_Migrated = caspioSelectAdv("Work_Orders", "count(TB_UNID)", "ISNULL(Project_ID, '') = '3222' AND TB_UNID > 123781","", false, "`", "|^",false);
//$FLS_Orders = caspioSelectAdv("Work_Orders", "MAX(TB_UNID)", "ISNULL(Project_ID, '') = '3222'","", false, "`", "|^",false);
/*$Active = caspioSelectAdv("FLS_Work_Orders", "COUNT(*)", "ISNULL(Deactivated, '') != 'Yes' AND ISNULL(Kickback, '0') = '0' AND ISNULL(DeclinedWorkOrder, '0') = '0' AND ISNULL(PayApprove, '0') = '0' AND ISNULL(Project_ID, '') = ''", "", false, "`", "|^",false);*/

echo "<tr><td>No Project Assigned Migrated</td><td>&nbsp;</td><td align=\"right\">&nbsp;</td><td>&nbsp;</td><td align=\"right\">".number_format($Project_Migrated[0])." / ".number_format($FLS_Orders[0])."</td></tr>";
$sub1 += $FLS_Orders[0];
/*$sub2 += $Active[0];*/
echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td align=\"right\"><b></b></td><td align=\"right\"><b>".number_format($sub1)."</b></td><td>&nbsp;</td></tr>";

echo "</table>";

//print_r(caspioSelectAdv("FLS_Work_Orders", "DateEntered", "ISNULL(Deactivated, '') != 'Yes' AND ISNULL(Kickback, '0') = '0' AND ISNULL(DeclinedWorkOrder, '0') = '0' AND ISNULL(PayApprove, '0') = '0' AND ISNULL(Project_ID, '') = ''", "", false, "`", "|^",false));

echo "<br/>$code<br/>$code2";

?>