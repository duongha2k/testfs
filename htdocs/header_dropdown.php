<?php include ("headerStartSession.php");

$siteTemplate = $_SESSION['template'];
if ($siteTemplate == "ruo") {
	include ("{$_SERVER['DOCUMENT_ROOT']}/templates/ruo/includes/header.php");
} else if ($siteTemplate == "wap") {
	// Don't load WAP Header
	header("Location: ". "http://wap.fieldsolutions.com/wap/index.php?pagetype=html");
	exit;

} else {
	include ("headerSimple_dd.php");
?>

<body>

<div id="mainContainer">
	<div id="headerSlider">
		<div id="headerRight" align="right">
			<div id="topNavBar">
    <ul>
    
<li><a href="https://<?php echo $siteTemplate;?>.fieldsolutions.com/clients/Training/FS_HowTo.php">Help</a></li>

<?php if ($page == "clients" || $page == "techs" || $page == "admin" || $page == "staff" || $page == "accounting"  || $page == "customers" ){?>
<!-- LOGOUT BUTTON -->
<li><a href="/logOut.php">Logout</a></li>
<?php } ?>
    
<?php if($displayHome!="no"){   ?>   
    <?php if($loggedIn=="yes" && $siteTemplate!="admin"){ //If Logged In  ?>
        
       <?php 
		
		if($loggedInAs=="client"){ //Logged in as client 
		echo '<li><a href="/clients/dashboard.php">Home</a></li>';
        }else if($loggedInAs=="tech"){ //Logged in as tech
		echo '<li><a href="/techs/dashboard.php">Home</a></li>';
		}else{ // Default link
        echo '<li><a href="/index.php">Home</a></li>';
        } 
		?>
        
<?php }else{ // Not logged in or could be admin ?>
<li><a href="http://<?php echo $siteTemplate;?>.fieldsolutions.com/index.php">Home</a></li>
<?php } ?>
<li><a href="https://<?php echo $siteTemplate;?>.fieldsolutions.com/content/aboutUs.php">About Us</a></li>

<li><a href="mailto:info@fieldsolutions.com">Contact Us</a></li>

<?php if ($loggedInAs == "tech") { ?>
<li><a href="http://groups.google.com/group/mytechnicianspace" target="_blank">Tech Community</a></li>
<?php } ?>
</ul>
<?php } else {?>
&nbsp;
<?php } ?>
			</div>
		</div>
	<div id="headerLogo"></div>
</div>
<?php } ?>
	