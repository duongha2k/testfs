<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Address Changed</title>
</head>

<body>
Sending email notification...<br /><br />

<?php

require_once("library/smtpMail.php");
require_once("library/quickbooks.php");
//require_once("techs/updateWOs_Tech_Info.php");


/*function sendRequest($Message)
{
		//$username = "mgkassoc";
		//$password = "race00";
		//$wsdl = "http://api.jangomail.com/api.asmx?wsdl";

        $user_agent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";
		$url = "http://api.jangomail.com/api.asmx/SendMassEmail";
	
		$params = $Message;
       	
		// Open Connection Handle
        $ch = curl_init();
        //curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);

        // Send Request
        $response = curl_exec ($ch);

        // Close Connection Handle
        curl_close ($ch);

        return $response;
}*/


function Update_And_Mail($TechID, $Name, $Address1, $Address2, $City, $State, $Zip, $Phone1, $Phone2)
{
/*	$wsdl = "https://bridge.caspio.net/ws/api.asmx?wsdl";
	$tableName = "TR_Master_List";
	$name = "mgkassoc";
	$profile = "EmailBlaster";
	$password = "Blaster000";*/
	# ------  Add code below here  ------
/*	try {	
			
		$client = new SoapClient($wsdl);
		$criteria = "TechID = '$TechID' ";		
		$fieldList = "FirstName, LastName, PrimaryEmail, Address1, Address2, City, State, ZipCode, PrimaryPhone, SecondaryPhone, FLSRegion";
		$records = $client->SelectDataRaw($name, $profile, $password, $tableName, false, $fieldList, $criteria, "", "", "");
		//print_r($records);
		//echo "<br>";
		//echo sizeof($records);
		
		if(sizeof($records) == 1) {
			//echo sizeof($records);
			//echo "<br>";
			$arr = explode(",", $records[0]);
			//echo sizeof($arr);
			//echo "<br>";
			//print_r($arr);
			//echo "<br>";
			if(sizeof($arr) >= 11) {
			
				$Name = $arr[0]." ".$arr[1];
				$Name = str_replace("'", "", $Name);
				
				$Email = $arr[2];
				$Email = str_replace("'", "", $Email);
				
				$Address1 = $arr[3];
				$Address1 = str_replace("'", "", $Address1);
	
				$Address2 = $arr[4];
				$Address2 = str_replace("'", "", $Address2);
				
				$City = $arr[5];
				$City = str_replace("'", "", $City);

				$State = $arr[6];
				$State = str_replace("'", "", $State);

				$Zip = $arr[7];
				$Zip = str_replace("'", "", $Zip);
				
				$Phone1 = $arr[8];
				$Phone1 = str_replace("'", "", $Phone1);

				$Phone2 = $arr[9];
				$Phone2 = str_replace("'", "", $Phone2);
				
				$Region = $arr[10];
				$Region = str_replace("'", "", $Region);
				
			}
			if(sizeof($arr) == 12){
				$Region .= ", " . $arr[11];
				$Region = str_replace("'", "", $Region);
				//echo "Worked!";
			}
			
*/			
			$fromName = 'FS Admin';
			$fromEmail = 'support@fieldsolutions.com';
			$subject = 'TECH ADDRESS CHANGE [' . $TechID . '] - Update Quickbooks Now!';
			$html_message = 'The following tech has changed their address.  Please verify and update their entry in QuickBooks now to prevent having to stop payment and re-issue a check.<br><br>';
			$html_message .= 'TechID: ' . $TechID . '<br>';
			$html_message .= 'Name: ' . $Name . '<br>';
			$html_message .= 'Address: ' . $Address1 . '<br>';
			if($Address2 != ""){
				$html_message .= '          ' . $Address2 . '<br>';			
			}
			
			$html_message .= 'City: ' . $City . '<br>';
			$html_message .= 'State: ' . $State . '<br>';
			$html_message .= 'Zip: ' . $Zip . '<br>';
			$html_message .= 'Telephone 1: ' . $Phone1 . '<br>';
			$html_message .= 'Telephone 2: ' . $Phone2 . '<br>';
			//$html_message .= 'FLS Region: ' . $Region . '<br>';

			$message = str_replace('<br/>', chr(13) . chr(10), $html_message); // Plain text version of message
			$message = str_replace('<br>', chr(13) . chr(10), $message); // Plain text version of message
/*			$toGroups = "";
			$toGroupsFilter = "";
			$webDB = "";*/
			$emailList = "addresschanges@fieldsolutions.com, codem01@gmail.com, gbailey@fieldsolutions.com";
//			$emailList = "codem01@gmail.com";
			
			//smtpMailLogReceived($fromName, $fromEmail, $emailList, $subject, $message, $html_message, "Email_Notif_AddressChange.php");
			// Do not send anymore as per Teddy - GB 2008_1015
			copyTechProfile(array($TechID));
			copyTechBankInfo(array($TechID));
			
//			smtpMailLogReceived($fromName, $fromEmail, "codem01@gmail.com", "Update complete [$TechID]", $message, $html_message, "Email_Notif_AddressChange.php");

			
/*			$options = "CharacterSet=UTF-8, ForceHandleBounces=False";
			
			$username = "mgkassoc";
			$password = "race00";*/

/*			$subject = urlencode($subject);
			$message = urlencode($message);
			$html_message = urlencode($html_message);*/

/*			$request = "";
			$request .= "Username=$username&Password=$password&FromEmail=$fromEmail&FromName=$fromName&ToGroups=$toGroups&ToGroupFilter=$toGroupsFilter&ToOther=$emailList&ToWebDatabase=$webDB&Subject=$subject&MessagePlain=$message&MessageHTML=$html_message&Options=$options";*/

/*			$response = sendRequest($request);
			$pos = strpos($response, "SUCCESS");
			if ($pos === false) {
				print_r($response);
			}*/
			
/*		}
	} catch (SoapFault $fault) {
			//SOAP fault handling
			$str = "<h1><b>Fault:</b> faultcode: {$fault->faultcode}, <b>faultstring:</b> {$fault->faultstring}</h1>";
	}
*/

}


$TechID=$_GET["TechID"];		
$Name=$_GET["Name"];		
$Address1=$_GET["Address1"];		
$Address2=$_GET["Address2"];		
$City=$_GET["City"];		
$State=$_GET["State"];		
$Zip=$_GET["Zip"];		
$Phone1=$_GET["Phone1"];		
$Phone2=$_GET["Phone2"];		
// $Region=$_GET["Region"];		


Update_And_Mail($TechID, $Name, $Address1, $Address2, $City, $State, $Zip, $Phone1, $Phone2);
//updateWOs_Tech_Info($TechID);
	
?>

<SCRIPT LANGUAGE="JavaScript"> 

	setTimeout('close()',100);

</SCRIPT> 

</body>
</html>
