<?php

    require("../headerStartSession.php");
    
    if (!is_numeric($_GET["TechID"]))
		die();
    
    $api = new Core_Api_Class();

    $api->techPrefer($_SESSION['UserName'].'|pmContext='.$_GET["CompanyID"], $_SESSION["UserPassword"], $_GET["TechID"]);
    
?>

This tech has been added to your client preferred list. Click <a href="javascript:close()">here</a> to close.

<script type="text/javascript">
function closePreferred() {
	try {
		opener.focus();
	}
	catch(e) {}
	setTimeout("window.close()", 3000);
}
closePreferred();
</script>