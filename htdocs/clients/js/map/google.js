var polygon         = '';
var map             = '';
var points          = Array();
var lassoPoints     = Array();
var lassoMarkers    = Array();
var lassoUse        = false;

function objMarker(id, point, email)
{
    this.id    = id;
    this.piont = point;
    this.email = email;
}

function initPoligon()
{
    GPolygon.prototype.containsLatLng = function(latLng) {
        var bounds = this.getBounds();

        if(!bounds.containsLatLng(latLng)) {
            return false;
        }

        var numPoints = this.getVertexCount();
        var inPoly = false;
        var i;
        var j = numPoints-1;

        for(var i=0; i < numPoints; i++) {
            var vertex1 = this.getVertex(i);
            var vertex2 = this.getVertex(j);

            if (vertex1.lng() < latLng.lng() && vertex2.lng() >= latLng.lng() || vertex2.lng() < latLng.lng() && vertex1.lng() >= latLng.lng())  {
                if (vertex1.lat() + (latLng.lng() - vertex1.lng()) / (vertex2.lng() - vertex1.lng()) * (vertex2.lat() - vertex1.lat()) < latLng.lat()) {
                    inPoly = !inPoly;
                }
            }

            j = i;
        }

        return inPoly;
    };
}

function lassoToggle()
{
    var mapElem = document.getElementById("gmap");
    if (lassoUse == false) {
        lassoUse = true;
        mapElem.className = "edit";
        if (document.getElementById('lassoToggleDiv')) {
            document.getElementById('lassoToggleDiv').innerHTML = '<a href="#" onclick="lassoToggle(); clearLasso(); return false;">Disable Lasso</a>';
            document.getElementById('lassoDrawDiv').style.display = 'inline';
        }
    } else {
        lassoUse = false;
        mapElem.className = "view";

        if (document.getElementById('lassoToggleDiv')) {
            document.getElementById('lassoToggleDiv').innerHTML = '<a href="#" onclick="lassoToggle(); clearLasso(); return false;">Enable Lasso</a>';
            document.getElementById('lassoDrawDiv').style.display = 'none';
        }
    }
}

function drawLasso()
{
    map.removeOverlay(polygon);
    polygon = new GPolygon(lassoPoints, "#f33f00", 0, 1, "#ff0000", 0.2);
    map.addOverlay(polygon);
    lassoToggle();
}

function clearLasso()
{
    map.removeOverlay(polygon);
    for (i=0; i<lassoMarkers.length; i++) {
        map.removeOverlay(lassoMarkers[i]);
    }
    lassoPoints = Array();
}

function collectSelectedMarkers()
{
    if (polygon) {
        var selectedMarkers = Array();
        for (i=0; i < points.length; i ++) {
            if (polygon.containsLatLng(points[i].piont)) {
                selectedMarkers[selectedMarkers.length] = points[i];
            }
        }
        doActionForSelected(selectedMarkers);
    }
}


function setMapCenter(lat, lng, zoom) {
    map.setCenter(new GLatLng(lat, lng), zoom);
}
