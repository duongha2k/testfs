var overlayEnabled = false;

function useOverlayToggle()
{
    if (overlayEnabled == false) {
        overlaySwitch(true);
    } else {
        overlaySwitch(false);
    }

    overlayRequest();
}

function overlaySwitch(newStatus) {
    if (newStatus == true) {
        overlayEnabled = true;
        document.getElementById('overlayToggle').innerHTML = '<a href="#" onclick="useOverlayToggle(); return false;">Disable Overlay of different search results</a><br /><a href="#" onclick="overlayRequest(); return false;">Add this search result to the map overlay</a>';
    } else {
        overlayEnabled = false;
        document.getElementById('overlayToggle').innerHTML = '<a href="#" onclick="useOverlayToggle(); return false;">Enable Overlay of different search results</a>';
    }
}

function overlayRequest()
{
     var allTechIds = Array();
     if (overlayEnabled == true) {
        for (i=0; i < points.length; i++) {
            allTechIds[allTechIds.length] = points[i].id;
        }
     }

     $.ajax({
            url: 'ajax/map_overlay.php',
            type: "POST",
            dataType:"html",
            data:{ enable:overlayEnabled, techId: allTechIds.join(',')},
            cache: false,
            success: function (data, textStatus) {
                if (overlayEnabled == false) {
                    window.location.href = window.location;
                }
            }
        });
}