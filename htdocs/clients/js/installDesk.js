var Company_ID = $("#EditRecordCompany_ID").attr("value");
var clientName = $("#EditRecordCompany_Name").attr("value");
var projectName = $("#EditRecordProject_Name").attr("value");

$(document).ready(function() {
	oldUpdateRequested = $("#EditRecordUpdate_Requested").attr("checked");
});

function UpdateCaspio(){	
	// time stamps
	queueTimeStampsCentralized();

	if (oldUpdateRequested != $("#EditRecordUpdate_Requested").attr("checked") && $("#EditRecordUpdate_Requested").attr("checked") == true) {
		// Work Order Published
		queueCreateTimeStamp($("#GetTB_UNID").val(), "Work Order Update Requested", Company_ID, clientName, projectName, "", "", "");
	}
			
	return true;
}

document.forms.caspioform.onsubmit = UpdateCaspio;