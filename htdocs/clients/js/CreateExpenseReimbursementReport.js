//-------------------------------------------------------------
//-------------------------------------------------------------
CreateExpenseReimbursementReport.Instances = null;
//-------------------------------------------------------------
CreateExpenseReimbursementReport.CreateObject = function(config) {
    var obj = null;
    if (CreateExpenseReimbursementReport.Instances != null) {
        obj = CreateExpenseReimbursementReport.Instances[config.id];
    }
    if (obj == null) {
        if (CreateExpenseReimbursementReport.Instances == null) {
            CreateExpenseReimbursementReport.Instances = new Object();
        }
        obj = new CreateExpenseReimbursementReport(config);
        CreateExpenseReimbursementReport.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function CreateExpenseReimbursementReport(config) {
    var Me = this;
    this.id = config.id;   
    this.ContainerID=config.ContainerID;
    this.CompanyID=config.CompanyID;
    this.Params=config.Params;
    //------------------------------------------------------------
    this.LoadingAjax = function(ContentId){
        jQuery(ContentId).html("<center><img src='/widgets/images/wait.gif' border='0' /></center>");
    }
    //---------------------------------------------------------------------
    this.btnRunReport_onClick=function(){       
        var listProjectIDSelected="";
        jQuery("#ListProject .classProjectItems:checked").each(function(){
            listProjectIDSelected+=jQuery(this).attr("id")+",";
        })
        listProjectIDSelected=listProjectIDSelected.replace(/chk/g,"");
        if(listProjectIDSelected.lenght>0)
            listProjectIDSelected=listProjectIDSelected.substr(0,listProjectIDSelected.length-1);
                
        Me.LoadingAjax(Me.ContainerID);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/reports/expense-reimbursement-report",
            data: {
                v:Me.CompanyID,
                ProjectIDs:'"'+listProjectIDSelected+'"'
            },
            success:function( html ) {
                jQuery(Me.ContainerID).html(html);                 
            }
        });
        
    }
    //---------------------------------------------------------------------
    this.BindEvent=function(){
        jQuery(".MasterItemsProject")
        .unbind("click", Me.MasterItemsProject_onClick)
        .bind("click", Me.MasterItemsProject_onClick);
        jQuery(".classProjectItems")
        .unbind("click", Me.classProjectItems_onClick)
        .bind("click", Me.classProjectItems_onClick);
    /*
        jQuery("#btnRunReport")
            .unbind("click", Me.btnRunReport_onClick)
            .bind("click", Me.btnRunReport_onClick);*/
        
    }
    //---------------------------------------------------------------------
    this.getdataCreateExpenseReimbursementReport = function()
    {
        Me.LoadingAjax(Me.ContainerID);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/reports/create-expense-reimbursement-report",
            data: {
                v:Me.CompanyID,
                Params:Me.Params
            },
            success:function( html ) {
                jQuery(Me.ContainerID).html(html);  
                $('#StartDateApprovalDateRange').calendar({
                    dateFormat: 'MDY/'
                });
                $('#EndDateApprovalDateRange').calendar({
                    dateFormat: 'MDY/'
                });
                $("#cbxApprovalDateRange").change(function(){ 
                    filterDate("#cbxApprovalDateRange",'#StartDateApprovalDateRange','#EndDateApprovalDateRange');
                })

                $('#StartDateStartDateRange').calendar({
                    dateFormat: 'MDY/'
                });
                $('#EndDateStartDateRange').calendar({
                    dateFormat: 'MDY/'
                });
                $("#cbxStartDateRange").change(function(){ 
                    filterDate("#cbxStartDateRange",'#StartDateStartDateRange','#EndDateStartDateRange');
                })


                $('#StartDateInvoiceDateRange').calendar({
                    dateFormat: 'MDY/'
                });
                $('#EndDateInvoiceDateRange').calendar({
                    dateFormat: 'MDY/'
                });
                $("#cbxInvoiceDateRange").change(function(){      
                        
                    filterDate("#cbxInvoiceDateRange",'#StartDateInvoiceDateRange','#EndDateInvoiceDateRange');
                })
                
                jQuery("#StartDateApprovalDateRange").watermark("MM/DD/YYYY");
                jQuery("#EndDateApprovalDateRange").watermark("MM/DD/YYYY");
                jQuery("#StartDateStartDateRange").watermark("MM/DD/YYYY");
                jQuery("#EndDateStartDateRange").watermark("MM/DD/YYYY");
                jQuery("#StartDateInvoiceDateRange").watermark("MM/DD/YYYY");
                jQuery("#EndDateInvoiceDateRange").watermark("MM/DD/YYYY");
                
                Me.InitPage();
                Me.BindEvent();
            }
        });
    }
    //------------------------------------------------------------
    this.MasterItemsProject_onClick=function(){
        var isCheck=jQuery(this).is(":checked");        
        jQuery("#ListProject .classProjectItems").each(function(){
            jQuery(this).attr("checked",isCheck);
        })
    }
    //------------------------------------------------------------
    this.classProjectItems_onClick=function(){
        var numItems= jQuery("#ListProject .classProjectItems").length;
        var numItemsChecked= jQuery("#ListProject .classProjectItems:checked").length;
        if(numItemsChecked==numItems)
            jQuery(".MasterItemsProject").attr("checked",true);
        else
            jQuery(".MasterItemsProject").attr("checked",false);
        
            
        
    }
    //------------------------------------------------------------
    this.InitPage=function(){
        
       
        if(Me.Params!="")
        {          
            var objParams= eval("("+ Base64.decode(decodeURIComponent(Me.Params))+")");            
            jQuery("#cbxApprovalDateRange"+" option[value=\""+objParams.cbxApprovalDateRange+"\"]").attr("selected","selected");
            jQuery("#StartDateApprovalDateRange").val(objParams.StartDateApprovalDateRange);
            jQuery("#EndDateApprovalDateRange").val(objParams.EndDateApprovalDateRange);
        
            jQuery("#cbxStartDateRange"+" option[value=\""+objParams.cbxStartDateRange+"\"]").attr("selected","selected");
            jQuery("#StartDateStartDateRange").val(objParams.StartDateStartDateRange);
            jQuery("#EndDateStartDateRange").val(objParams.EndDateStartDateRange);
        
            jQuery("#cbxInvoiceDateRange"+" option[value=\""+objParams.cbxInvoiceDateRange+"\"]").attr("selected","selected");
            jQuery("#StartDateInvoiceDateRange").val(objParams.StartDateInvoiceDateRange);
            jQuery("#EndDateInvoiceDateRange").val(objParams.EndDateInvoiceDateRange);
        
            jQuery("#txtWIN").val(objParams.txtWIN);
            jQuery("#txtClientWOID").val(objParams.txtClientWOID);
            jQuery("#txtFSTechID").val(objParams.txtFSTechID);
                       
            if(objParams.chkShowApprovedReimbursementsOnly==1)
            {
                jQuery("#chkShowApprovedReimbursementsOnly").attr("checked","checked");
            }
            else
            {
                jQuery("#chkShowApprovedReimbursementsOnly").removeAttr("checked");
            }
            if(objParams.chkShowDetailbyCategory==1)
            {
                jQuery("#chkShowDetailbyCategory").attr("checked","checked");
            }
            else
            {
                jQuery("#chkShowDetailbyCategory").removeAttr("checked");
            }
            
            if(objParams.chkShowTechClaim==1)
            {
                jQuery("#chkShowTechClaim").attr("checked","checked");
            }
            else
            {
                jQuery("#chkShowTechClaim").removeAttr("checked");
            }
            if(objParams.chkShowReimbursedthrough==1)
            {
                jQuery("#chkShowReimbursedthrough").attr("checked","checked");
            }
            else
            {
                jQuery("#chkShowReimbursedthrough").removeAttr("checked");
            }
            if(objParams.chkShowReimbursedOutside==1)
            {
                jQuery("#chkShowReimbursedOutside").attr("checked","checked");
            }
            else
            {
                jQuery("#chkShowReimbursedOutside").removeAttr("checked");
            }            
            if(typeof(objParams.chkProjectList)!="undefined")
            {
                var  arrProjectIDs=eval(objParams.chkProjectList);
           
                var lenghtArr=arrProjectIDs.length;
                var i=0;
                for( i=0;i<lenghtArr;i++)
                {                
                    jQuery("#ListProject .classProjectItems[value=\""+arrProjectIDs[i]+"\"]").each(function(){
                        jQuery(this).attr("checked",true);
                    })
                }
            }
            else                
            {
                jQuery("#ListProject .classProjectItems").each(function(){
                    jQuery(this).attr("checked",false);
                })
            }
            var numCheckBoxProjectID= jQuery("#ListProject .classProjectItems").length;
            var numCheckBoxProjectIDChecked=jQuery("#ListProject .classProjectItems:checked").length;
            
            if(numCheckBoxProjectID==numCheckBoxProjectIDChecked)
            {
                jQuery("#chkAllProject").attr("checked",true);
            }
            else
            {
                jQuery("#chkAllProject").removeAttr("checked");
            }
           
        }
        else
        {
            var isCheck=jQuery(".MasterItemsProject").is(":checked");
            jQuery("#ListProject .classProjectItems").each(function(){
                jQuery(this).attr("checked",isCheck);
            })
        }
        
    }
    //------------------------------------------------------------
    this.init = function() {
        jQuery(document).ready(function(){  
            Me.getdataCreateExpenseReimbursementReport();
        })
        
        
    }

}