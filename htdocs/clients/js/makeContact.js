function emailMode() {
	contactBox.value = email;
	contactLabel.innerHTML = "Email";
	subjectLabel.innerHTML = "Subject";
	messageLabel.innerHTML = "Message";
	copySelfRow.style.display = "";
}

function phoneMode() {
	contactBox.value = techName;
	contactLabel.innerHTML = "Contact Name";
	subjectLabel.innerHTML = "Reason";
	messageLabel.innerHTML = "Notes";
	copySelfRow.style.display = "none";
}

try {
	var emailRadio = document.getElementById("InsertRecordType0");
	var phoneRadio = document.getElementById("InsertRecordType1");
	var contactBox = document.getElementById("InsertRecordContact");
	var subjectBox = document.getElementById("InsertRecordSubject");
	var messageBox = document.getElementById("InsertRecordMessage");
	var contactLabel = contactBox.parentNode.parentNode.childNodes[0];
	var subjectLabel = subjectBox.parentNode.parentNode.childNodes[0];
	var messageLabel = messageBox.parentNode.parentNode.childNodes[0];
	var copySelf = document.getElementById("cbParamVirtual1");
	var copySelfRow = copySelf.parentNode.parentNode.parentNode;
	
	emailRadio.onclick = emailMode;
	phoneRadio.onclick = phoneMode;
	if (phoneRadio.checked) 
		phoneMode();
	else
		emailMode();
}
catch (e) {
}
