	var hideActionTechCP = null;
	var hideDelayTechCP = 500;
	var techCPBox = "#techCellPhoneLookupBox";
	function showDetailsTechCP(data, textStatus) {
		if (data != "") {
			parts = data.split("`");
			ratingInfo = parts[1].split(",");
			overallRanking = ratingInfo[0];
			numRanking = ratingInfo[1];
			techID = ratingInfo[2];
			sourceList = parts[0].split("|");
			html = "<table><thead><tr><td>Rating Source</td><td>Total</td></tr></thead>";
			for (i=0; i < sourceList.length; ++i) {
				info = sourceList[i].split(",");
				rankingSource = info[0];
				count = info[1];
				type = encodeURIComponent(rankingSource);
				html += "<tr><td><a href='techRatingsView.php?cbResetParam=1&id=" + techID + "&type=" + type + "' target='_blank'>" + rankingSource + "</a></td><td>" + count + "</td></tr>";
			}
			if (overallRanking != "" && numRanking != "") {
				html += "<tr><td colspan='2'></td></tr><tfoot><td colspan='2'>" + overallRanking + " average out of " + numRanking + " ratings</td></tr></tfoot>";
			}
			html += "</table>";
		}
		else {
			html = "No rating information available.";
		}
		$(techCPBox).html(html);
	}
	
	function showDetailsBoxTechCP() {
		resetHideDetailsBoxTechCP();
		var myid = (this.id.split("myRating",2))[1];
		var pos = $(this).offset();
		$.get("/ajax/ratingSourceBreakdown.php", { id: myid, rating: this.rating, numRating: this.numRating }, showDetailsTechCP);
		var height = $(techCPBox).height();
		$(techCPBox).css({top: pos.top - (height / 2), left: pos.left + 30, display: "none"});
		$(techCPBox).html("<img src='/images/loading.gif' />");
		$(techCPBox).fadeIn("slow");
	}
	
	function delayedHideDetailsBoxTechCP() {
		hideActionTechCP = setTimeout("hideDetailsBoxTechCP();", hideDelayTechCP);
	}
	
	function hideDetailsBox() {
		$(techCPBox).fadeOut("slow");
	}
	
	function resetHideDetailsBoxTechCP() {
		clearTimeout(hideActionTechCP);
	}
	
	$(document).ready(
		function () {
			$('body').append("<div id='techCellPhoneLookupBox'></div>");
			$('.techCellPhoneLookup').hover(showDetailsBoxTechCP, delayedHideDetailsBoxTechCP);
			$(techCPBox).hover(resetHideDetailsBoxTechCP, delayedHideDetailsBoxTechCP);
		}
	);