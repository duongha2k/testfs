//-------------------------------------------------------------
//-------------------------------------------------------------
TechUtilReport.Instances = null;
//-------------------------------------------------------------
TechUtilReport.CreateObject = function(config) {
    var obj = null;
    if (TechUtilReport.Instances != null) {
        obj = TechUtilReport.Instances[config.id];
    }
    if (obj == null) {
        if (TechUtilReport.Instances == null) {
            TechUtilReport.Instances = new Object();
        }
        obj = new TechUtilReport(config);
        TechUtilReport.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function TechUtilReport(config) {
    var Me = this;
    this.id = config.id;   
    this.ContainerID=config.ContainerID;
    this.CompanyID=config.CompanyID;
    this.parames = config.parames;
    //------------------------------------------------------------
    this.LoadingAjax = function(ContentId){
        jQuery(ContentId).html("<center><img src='/widgets/images/wait.gif' border='0' /></center>");
    }
    //---------------------------------------------------------------------
    this.classDownload_onClick=function(){
        alert("vao");
    }
    //---------------------------------------------------------------------
    this.getdataCreateTechUtilReport = function()
    {
        Me.LoadingAjax(Me.ContainerID);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/reports/tech-util-report",
            data: Me.parames,
            success:function( html ) {
                jQuery(Me.ContainerID).html(html);    
                jQuery(".classDownload")
                    .unbind("click",Me.classDownload_onClick)
                    .bind("click",Me.classDownload_onClick);
            }
        });
    }
    //------------------------------------------------------------
    this.init = function() {
         if(Me.parames != "")
        {
           // commenting out the line below because it was causing the report to not be displayed
           // not sure what the purpose of eval'ing the php request is
        	 
           // Me.parames = eval("("+Me.parames+")");
        }   
        jQuery(document).ready(function(){            
           Me.getdataCreateTechUtilReport();
        })
        
        
    }

}