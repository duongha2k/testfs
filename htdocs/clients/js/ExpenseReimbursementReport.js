//-------------------------------------------------------------
//-------------------------------------------------------------
ExpenseReimbursementReport.Instances = null;
//-------------------------------------------------------------
ExpenseReimbursementReport.CreateObject = function(config) {
    var obj = null;
    if (ExpenseReimbursementReport.Instances != null) {
        obj = ExpenseReimbursementReport.Instances[config.id];
    }
    if (obj == null) {
        if (ExpenseReimbursementReport.Instances == null) {
            ExpenseReimbursementReport.Instances = new Object();
        }
        obj = new ExpenseReimbursementReport(config);
        ExpenseReimbursementReport.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function ExpenseReimbursementReport(config) {
    var Me = this;
    this.id = config.id;   
    this.ContainerID=config.ContainerID;
    this.CompanyID=config.CompanyID;
    this.parames = config.parames;
    //------------------------------------------------------------
    this.LoadingAjax = function(ContentId){
        jQuery(ContentId).html("<center><img src='/widgets/images/wait.gif' border='0' /></center>");
    }
    //---------------------------------------------------------------------
    this.classDownload_onClick=function(){
        alert("vao");
    }
    //---------------------------------------------------------------------
    this.getdataCreateExpenseReimbursementReport = function()
    {
        Me.LoadingAjax(Me.ContainerID);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/reports/expense-reimbursement-report",
            data: Me.parames,
            success:function( html ) {
                jQuery(Me.ContainerID).html(html);    
                jQuery(".classDownload")
                    .unbind("click",Me.classDownload_onClick)
                    .bind("click",Me.classDownload_onClick);
            }
        });
    }
    //------------------------------------------------------------
    this.init = function() {
         if(Me.parames != "")
        {
            Me.parames = eval("("+Me.parames+")");
        }   
        jQuery(document).ready(function(){            
           Me.getdataCreateExpenseReimbursementReport();
        })
        
        
    }

}