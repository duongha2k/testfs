	var hideSATAction = null;
	var hideSATDelay = 100;
	
	function showSATDetailsBox() {
		resetHideSATDetailsBox();
		var pos = $(this).offset();
		var height = $('#SATDetailsBox').height();
		$('#SATDetailsBox').css({top: pos.top - (height / 2), left: pos.left + 30, display: "none"});
//		$('#SATDetailsBox').html("<img src='/images/loading.gif' />");
		$('#SATDetailsBox').fadeIn("slow");
	}
	
	function delayedHideSATDetailsBox() {
		hideSATAction = setTimeout("hideSATDetailsBox();", hideSATDelay);
	}
	
	function hideSATDetailsBox() {
		$('#SATDetailsBox').fadeOut("slow");
	}
	
	function resetHideSATDetailsBox() {
		clearTimeout(hideSATAction);
	}
	
	


	$(document).ready(
		function () {
			$('body').append("<div id='SATDetailsBox'>Preference score indicates a client’s overall satisfaction with a tech for performance, value and personal qualities. The rating question is: \"How likely would you be to recommend this tech\": 5 = Extremely Likely<br/><br/>Tech event Performance score indicates clients’ satisfaction with each particular assignment, of all kinds. The rating question is: \"The outcome of this field service event was...\"  5 = Excellent </div>");
			$('.SATDetailsOnHover').hover(showSATDetailsBox, delayedHideSATDetailsBox);
			$('.SATDetailsLink').click(function(){
				resetHideSATDetailsBox();
				var pos = $(this).offset();
				var height = $('#SATDetailsBox').height();
				$('#SATDetailsBox').css({top: pos.top - (height / 2), left: pos.left + 30, display: "none"});
//				$('#SATDetailsBox').html("<img src='/images/loading.gif' />");
				$('#SATDetailsBox').fadeIn("slow");
			});
			$('#SATDetailsBox').hover(resetHideSATDetailsBox, delayedHideSATDetailsBox).css({"position": "absolute", "text-align": "left", "border": "5px solid #CCCCCC", "background-color": "#FFFFFF", "padding": "10px", "overflow": "hidden", "cursor": "default", "display": "none", "width": "250px"});
		}
		
	);
