function replaceValueIfBlank(element, newValue) {
	var oldValue = $.trim(element.attr("value"));
	newValue = $.trim(newValue);
	if (oldValue == undefined || oldValue == "")
		element.attr("value", newValue);
}
	
function loadACSProjectOptions() {
	$.get("ajax/projectLookup.php", {"projectID" : projectID},
	function(data) {
		var prefix = (formType == "insert" ? "#InsertRecord" : "#EditRecord");
		var getProjectInfo = data.split("|^");
				
		parseACSProjectOptions(getProjectInfo);
	});
}

function parseACSProjectOptions(getProjectInfo) {
	var prefix = (formType == "insert" ? "#InsertRecord" : "#EditRecord");
	// load ACS options at create
	ACSApplyTo = getProjectInfo[44];
			
	if (ACSApplyTo == 3 && formType != "insert")
		// ignore load if choice was NEW WO ONLY
		return;

	ReminderAll = getProjectInfo[35] == 1;
	ReminderAcceptance = getProjectInfo[36] == 1;
	Reminder24Hr = getProjectInfo[37] == 1;
	Reminder1Hr = getProjectInfo[38] == 1;
	CheckInCall = getProjectInfo[39] == 1;
	CheckOutCall = getProjectInfo[40] == 1;
	ReminderNotMarkComplete = getProjectInfo[41] == 1;
	ReminderIncomplete = getProjectInfo[42] == 1;
	SMSBlast = getProjectInfo[43] == 1;
		
	$(prefix + "ReminderAll").attr("checked", ReminderAll);
	$(prefix + "ReminderAcceptance").attr("checked", ReminderAcceptance);
	$(prefix + "Reminder24Hr").attr("checked", Reminder24Hr);
	$(prefix + "Reminder1Hr").attr("checked", Reminder1Hr);
	$(prefix + "CheckInCall").attr("checked", CheckInCall);
	$(prefix + "CheckOutCall").attr("checked", CheckOutCall);
	$(prefix + "ReminderNotMarkComplete").attr("checked", ReminderNotMarkComplete);
	$(prefix + "ReminderIncomplete").attr("checked", ReminderIncomplete);
	$(prefix + "SMSBlast").attr("checked", SMSBlast);

	if (formType == "insert") {
		$("#cbParamVirtual11").attr("checked",SMSBlast || ReminderAll).attr("disabled", true);
		$("#cbParamVirtual12").attr("checked",ReminderAcceptance || ReminderAll).attr("disabled", true);
		$("#cbParamVirtual14").attr("checked",Reminder24Hr || ReminderAll).attr("disabled", true);
		$("#cbParamVirtual13").attr("checked",Reminder1Hr || ReminderAll).attr("disabled", true);
		$("#cbParamVirtual18").attr("checked",CheckInCall || ReminderAll).attr("disabled", true);
		$("#cbParamVirtual15").attr("checked",CheckOutCall || ReminderAll).attr("disabled", true);
		$("#cbParamVirtual16").attr("checked",ReminderNotMarkComplete || ReminderAll).attr("disabled", true);
		$("#cbParamVirtual17").attr("checked",ReminderIncomplete || ReminderAll).attr("disabled", true);
	}
	else {
		$("#cbParamVirtual30").attr("checked",SMSBlast || ReminderAll).attr("disabled", true);
		$("#cbParamVirtual35").attr("checked",ReminderAcceptance || ReminderAll).attr("disabled", true);
		$("#cbParamVirtual36").attr("checked",Reminder24Hr || ReminderAll).attr("disabled", true);
		$("#cbParamVirtual37").attr("checked",Reminder1Hr || ReminderAll).attr("disabled", true);
		$("#cbParamVirtual38").attr("checked",CheckInCall || ReminderAll).attr("disabled", true);
		$("#cbParamVirtual39").attr("checked",CheckOutCall || ReminderAll).attr("disabled", true);
		$("#cbParamVirtual40").attr("checked",ReminderNotMarkComplete || ReminderAll).attr("disabled", true);
		$("#cbParamVirtual31").attr("checked",ReminderIncomplete || ReminderAll).attr("disabled", true);
	}
}

function undefinedToFalse(val) {
	return (val == undefined ? false : val);
}

function ajaxProjectInfo() {
	var runonce = "done";
	// lookup project information - used in

	if(formType == "insert"){
		var projectID = document.forms[0].InsertRecordProject_ID.value;
		if (projectID == "") return;
	}else{
		var projectID = document.forms[0].EditRecordProject_ID.value;
		if (projectID == "") return;
	}

	$.get("ajax/projectLookup.php", {"projectID" : projectID},
	function(data) {
		var prefix = (formType == "insert" ? "#InsertRecord" : "#EditRecord");
		var getProjectInfo = data.split("|^");
		var num = parseFloat(getProjectInfo[5]);
		num = (isNaN(num) ? 0 : num);
		getProjectInfo[5] = num.toFixed(2);
		replaceValueIfBlank($(prefix + "Project_Name"), getProjectInfo[0]);
		replaceValueIfBlank($(prefix + "Description"), getProjectInfo[1]);
		replaceValueIfBlank($(prefix + "Requirements"), getProjectInfo[2]);
		replaceValueIfBlank($(prefix + "Tools"), getProjectInfo[3]);
		replaceValueIfBlank($(prefix + "SpecialInstructions"), getProjectInfo[4]);
		replaceValueIfBlank($(prefix + "PayMax"), getProjectInfo[5]);
		replaceValueIfBlank($(prefix + "Amount_Per"), getProjectInfo[6]);
		replaceValueIfBlank($(prefix + "Qty_Devices"), getProjectInfo[7]);
			
		replaceValueIfBlank($(prefix + "General1"), getProjectInfo[8]);
		replaceValueIfBlank($(prefix + "General2"), getProjectInfo[9]);
		replaceValueIfBlank($(prefix + "General3"), getProjectInfo[10]);
		replaceValueIfBlank($(prefix + "General4"), getProjectInfo[11]);
		replaceValueIfBlank($(prefix + "General5"), getProjectInfo[12]);
		replaceValueIfBlank($(prefix + "General6"), getProjectInfo[13]);
		replaceValueIfBlank($(prefix + "General7"), getProjectInfo[14]);
		replaceValueIfBlank($(prefix + "General8"), getProjectInfo[15]);
		replaceValueIfBlank($(prefix + "General9"), getProjectInfo[16]);
		replaceValueIfBlank($(prefix + "General10"), getProjectInfo[17]);
			
		replaceValueIfBlank($(prefix + "Headline"), getProjectInfo[18]);
		replaceValueIfBlank($(prefix + "WO_Category_ID"), getProjectInfo[19]);

		replaceValueIfBlank($(prefix + "ProjectManagerName"), getProjectInfo[20]);
		replaceValueIfBlank($(prefix + "ProjectManagerPhone"), getProjectInfo[21]);
		replaceValueIfBlank($(prefix + "ProjectManagerEmail"), getProjectInfo[22]);

		replaceValueIfBlank($(prefix + "ResourceCoordinatorName"), getProjectInfo[23]);
		replaceValueIfBlank($(prefix + "ResourceCoordinatorEmail"), getProjectInfo[24]);
		replaceValueIfBlank($(prefix + "ResourceCoordinatorPhone"), getProjectInfo[25]);

		replaceValueIfBlank($(prefix + "CheckInOutName"), getProjectInfo[26]);
		replaceValueIfBlank($(prefix + "CheckInOutNumber"), getProjectInfo[27]);
		replaceValueIfBlank($(prefix + "CheckInOutEmail"), getProjectInfo[28]);

		replaceValueIfBlank($(prefix + "EmergencyName"), getProjectInfo[29]);
		replaceValueIfBlank($(prefix + "EmergencyPhone"), getProjectInfo[30]);
		replaceValueIfBlank($(prefix + "EmergencyEmail"), getProjectInfo[31]);

		replaceValueIfBlank($(prefix + "TechnicalSupportName"), getProjectInfo[32]);
		replaceValueIfBlank($(prefix + "TechnicalSupportPhone"), getProjectInfo[33]);
		replaceValueIfBlank($(prefix + "TechnicalSupportEmail"), getProjectInfo[34]);

		// Auto-assign
		$(prefix + "isProjectAutoAssign").attr('checked', getProjectInfo[45] == 'True');

		$(prefix + "isWorkOrdersFirstBidder").attr('checked', getProjectInfo[46] == 'True');

		$(prefix + "P2TPreferredOnly").attr('checked', getProjectInfo[47] == 'True');

		replaceValueIfBlank($(prefix + "MinutesRemainPublished"), getProjectInfo[48]);

		$(prefix + "MinimumSelfRating").val(getProjectInfo[49]);
		$(prefix + "MaximumAllowableDistance").val(getProjectInfo[50]);
		
		$(prefix + "SignOffSheet_Required").attr('checked', getProjectInfo[51] == 'True');
		
		if (formType == "insert") {
			isProjectAutoAssign = $(prefix + "isProjectAutoAssign").attr('checked');
			isWorkOrderFirstBidder = $(prefix + "isWorkOrdersFirstBidder").attr('checked');
			P2TPreferredOnly = $(prefix + "P2TPreferredOnly").attr('checked');
			isProjectAutoAssign = undefinedToFalse(isProjectAutoAssign);
			isWorkOrderFirstBidder = undefinedToFalse(isWorkOrderFirstBidder);
			P2TPreferredOnly = undefinedToFalse(P2TPreferredOnly);

			$("#cbParamVirtual19").attr("checked",isProjectAutoAssign).attr("disabled", true);
			$("#cbParamVirtual200").attr("checked",isWorkOrderFirstBidder).attr("disabled", true);
			$("#cbParamVirtual201").attr("checked",!isWorkOrderFirstBidder).attr("disabled", true);
			$("#cbParamVirtual210").attr("checked",P2TPreferredOnly).attr("disabled", true);
			$("#cbParamVirtual211").attr("checked",!P2TPreferredOnly).attr("disabled", true);
			
			fillDaysHoursMinutesField();
			
			$("#cbParamVirtual22").attr("value",$(prefix + "MinimumSelfRating").val()).attr("disabled", true);
			$("#cbParamVirtual23").attr("value",$(prefix + "MaximumAllowableDistance").val()).attr("disabled", true);
			parseACSProjectOptions(getProjectInfo);
		}

	});

}


$("#InsertRecordProject_ID").change(ajaxProjectInfo);
$("#EditRecordProject_ID").change(ajaxProjectInfo);

