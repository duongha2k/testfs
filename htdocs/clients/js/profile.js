//-------------------------------------------------------------
//-------------------------------------------------------------
profile.Instances = null;
//-------------------------------------------------------------
profile.CreateObject = function(config) {
    var obj = null;
    if (profile.Instances != null) {
        obj = profile.Instances[config.id];
    }
    if (obj == null) {
        if (profile.Instances == null) {
            profile.Instances = new Object();
        }
        obj = new profile(config);
        profile.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function profile(config) {
    var Me = this;
    this.id = config.id;
    this._redirect = false;
    this._redirectTo = "";
    this._roll = new FSPopupRoll();
    this._rollMsg = new FSPopupRoll();
    this._rollWaitingSave = new FSPopupRoll();
    this._rollActivityLog = new FSPopupRoll();
    this._rollInstruction = new FSPopupRoll();
    this.win = config.win;
    this.tab = config.tab;
    this.emailclient = "";
    this.companyName = config.companyName;
    this.emailclient = "";
    this.notice = config.notice;

    //------------------------------------------------------------
    this.LoadingAjax = function(ContentId){
        jQuery(ContentId).html("<center><img src='/widgets/images/wait.gif' border='0' /></center>");
    }
    //---------------------------------------------------------------------
    this.cmdCollspan_onclick = function(clickElemnt)
    {
//		elementId = jQuery(this).attr("parent_element_id");
//		if (!elementId) elementId = Me.dicContentAccessID;
        var item_id = jQuery(this).attr("item_id");
        var display = jQuery(".trChildrenId"+item_id).css('display');
        var img = '';
        var childrenDisplay = '';
        if(display == 'none')
        {
            img = Me.urlImg_minus;
            childrenDisplay = '';
        }
        else
        {
            img = Me.urlImg_plus;
            childrenDisplay = 'none';    
        }
        jQuery(this).children('img').attr('src',img);
        jQuery(".trChildrenId"+item_id).css('display',childrenDisplay);
    }
    //-----------------------------------------------------------
    this.WaitingSave = function()
    {
        var html ='<img border="0" src="/widgets/images/wait.gif">';
        Me._rollWaitingSave.autohide(false);

        var opt = {};
        opt.width    = 200;
        opt.height   = '';
        opt.position = 'middle';
        opt.title    = '';
        opt.body     = html;
        
        return Me._rollWaitingSave.showNotAjax(null, null, opt);
    }
    //------------------------For Client profile-----------------------    
    /**
     * For Client profile
     */
    //------------------------------------------------------------------------
    
    this.showMessages = function(data,fnSuccess, successMsg, autohide) {
    
        var html = '';
        Me._rollWaitingSave.hide();
        if (Me._redirect && data.success) { //always refresh page on success
            if (window.opener) {
                try{
                    window.opener.reloadTabFrame();
                } catch (e) {}
            }
            //window.location = Me._redirectTo;
            if(typeof(fnSuccess) == "function")
            {
                fnSuccess();
            }
        //Me.getdataProfile();
        }
        if (data.success) {
            if (!successMsg)
                html += '<div class="success"><ul><li>Success!</li></ul></div>';
            else
                html = msg;
        }else{
            html+='<div class="errors"><ul>';
            for (i=0;i<data.errors.length;i++) {
                if(typeof data.errors[i] === 'object')
                    html+='<li>'+data.errors[i].message+'</li>';
                else
                    html+='<li>'+data.errors[i]+'</li>';
            }
            html+='</ul></div>';
        }
        if (!successMsg)
            html+='<div class="tCenter"><input type="button" class="link_button_popup" onclick="_profile._rollMsg.hide();" value="Ok" /></div>';
        if (autohide == undefined) autohide = false;
        Me._rollMsg.autohide(autohide);

        var opt = {};
        opt.width    = 400;
        opt.height   = '';
        opt.position = 'middle';
        opt.title    = '';
        opt.body     = html;
     
        if (data.success) {
            opt.width = 300;
        }
        return Me._rollMsg.showNotAjax(null, null, opt);
    }
    
    this.EditRecordCountry_onchange = function()
    {
        var contrycode = jQuery(this).children('option:selected').attr('contrycode');
        
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/client/getstatis?contry_id="+contrycode,
            data: "",
            success:function( html ) {
                jQuery("#EditRecordBusinessState").html(html);
            }
        });
    }
    //------------------------------------------------------------
   
    //------------------------------------------------------------
    //970
    this.cbxrule_Onchange = function()
    {
        var parentclass = $(this).parent().parent().attr("class");
        if($("#cbx"+ parentclass +" option:selected").val()=="")
        {
            $("."+parentclass +" .timeframe").css("display","none");
        }
        else
        {
            $("."+parentclass +" .timeframe").css("display","");
        }
        
    }
    
    this.isDisplayClientdenined_Onclick = function()
    {
        if($("#ClientdeninedNo").is(":checked"))
        {
            $("#clientdeninedyes").html("Yes");
            $(".tdClientdeninedyes").css("display","none");
            $("#cbxnoshows option").filter(function() {
                return $(this).val() == ""; 
            }).attr('selected', true)
            $("#cbxbackouts option").filter(function() {
                return $(this).val() == ""; 
            }).attr('selected', true)
            $("#cbxpreferencerating option").filter(function() {
                return $(this).val() == ""; 
            }).attr('selected', true)
            $("#cbxperformancerating option").filter(function() {
                return $(this).val() == ""; 
            }).attr('selected', true)
            $(".timeframe").css("display","none");
        }
        else
        {
            $(".tdClientdeninedyes").css("display","");
            $("#clientdeninedyes").html("Yes &ndash; Automatically client-deny the techs with any of the following, across all FieldSolutions clients:");
             
        }
    }
                
    //end 970
    this.cmdSaveProfile_Onclick = function()
    {
        //check valid
        var EIN = jQuery.trim(jQuery("#detailsContainer #EditRecordEIN").val());
        var data = {
            success:0,
            errors:['EIN or Tax ID must be XX-XXXXXXX.']
        };
        Me._redirect = false;    
        if(jQuery.trim(EIN) != '' && jQuery("#EditRecordCountry").val() == "US")
        {
            if(EIN.length != 10)
            {
                Me.showMessages(data);
                return false;
            }
            if(EIN[2] != '-')
            {
                Me.showMessages(data);
                return false;     
            }
        }
        //        if(EIN.substring(5, EIN.length) != '22333')
        //        {
        //            Me.showMessages(data);
        //            return false;     
        //        }

        jQuery("#detailsContainer #cmdSaveProfile").click();
//        $("#UpdateClientForm").submit();
    };
    //------------------------------------------------------------
    this.getdataProfile = function(_mode)
    {
        detailsWidget = new FSWidgetClientDetails({
            container:'detailsContainer',
            tab:'clientedit',
            win:Me.win,
            reftab:Me.tab
        },roll);
        detailsWidget.prepareForm = function() { 
            //970
                jQuery("#detailsContainer .cmdSaveprofile").unbind("click")
                .bind("click",Me.cmdSaveProfile_Onclick);
            jQuery("#detailsContainer #cmdSavesetting").unbind("click")
                .bind("click",Me.cmdSaveProfile_Onclick);
            
            jQuery("#detailsContainer .cbxrule").unbind("change")
                .bind("change",Me.cbxrule_Onchange);
            jQuery("#detailsContainer #ClientdeninedNo").unbind("click")
                    .bind("click",Me.isDisplayClientdenined_Onclick);
            jQuery("#detailsContainer #Clientdeninedyes").unbind("click")
                    .bind("click",Me.isDisplayClientdenined_Onclick);
            //end 970
        };
        detailsWidget.show({
            tab:'clientedit',
            params:{
                company:window._company,
                container:'detailsContainer',
                win:Me.win,
                tab:Me.tab,
                mode:_mode
            }
        });
    }
    //------------------------For Access Setting-----------------------    
    /**
     * For Access Setting
     */
    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    this.pageAccessLog = 1;
    this.ajaxLinkAccessLog_click = function()
    {
        Me._rollActivityLog.hide();
        Me.pageAccessLog = jQuery(this).attr('page');
        Me.cmdActivityLogAccessSettings();
    }
    //---------------------------------------------------------------------
    this.cmdActivityLogAccessSettings = function()
    {
        Me.WaitingSave();
        var el = this;
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/client/activitylogprofile?page="+Me.pageAccessLog+"&company="+Me.companyName,
            data: "",
            success:function( html ) {
                var autohide = false;
                Me._rollWaitingSave.hide();
                Me._rollActivityLog.autohide(autohide);
                var opt = {};
                opt.width    = 600;
                opt.height   = 400;
                opt.position = 'top';
                opt.wTop = 10;
                opt.title    = '<div style="text-align: center;">Profile Activity Log<div>';
                opt.body     = html;
                Me._rollActivityLog.showNotAjax(el, el, opt);
                // binh event
                jQuery("#AcivitiLogUserTN .ajaxLink").unbind("click",Me.ajaxLinkAccessLog_click)
                .bind("click",Me.ajaxLinkAccessLog_click);
            }
        });
    }
    //---------------------------------------------------------------------
    this.getdataAccess = function(mode)
    {
        Me.LoadingAjax(Me.dicContentAccessID);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/client/accesssettingprofile?client_id="+Me.client_id+'&mode='+mode,
            data: "",
            success:function( html ) {
                jQuery(Me.dicContentAccessID).html(html);
                //build event Save 
                jQuery(Me.dicContentAccessID +" #cmdActivityLog").unbind("click",Me.cmdActivityLogAccessSettings)
                .bind("click",Me.cmdActivityLogAccessSettings);
                jQuery(Me.dicContentAccessID +" .cmdCollspan").unbind("click",Me.cmdCollspan_onclick)
                .bind("click",Me.cmdCollspan_onclick);
                Me.onInitCheck();
            }
        });
    }
    //---------------------------------------------------------------------
    this.onInitCheck = function()
    {
        jQuery(Me.dicContentAccessID+" .trChildren").each(function(){
            var item_id = jQuery(this).attr("item_id");
            var el = this; 
            if(item_id == 0 || jQuery(this).find(".ckChildrenOpen").attr("checked") == "true")
            {
                var i =0;
                var countI = 0;
                jQuery(Me.dicContentAccessID+" .trChildrenId"+item_id).each(function(){
                    countI++;
                    if(jQuery(this).find(".ckChildrenOpen").attr("checked") == 'true')
                    {
                        i++;
                    }
                });
                
                if(i > 0 && i < countI)
                {
                    Me.checkSpuareOpen(el,'square'); 
                    Me.checkSpuareRe(el,'square'); 
                }
                else if(i == countI)
                {
                    Me.checkSpuareOpen(el,'checked');       
                }
                else if(i == 0)
                {
                    Me.checkSpuareRe(el,'checked');     
                }
              
            }
        
        });
    }
    //---------------------------------------------------------------------
    this.urlCheckFalse = '/widgets/images/checklist_flase.jpg';
    this.urlCheckTrue = '/widgets/images/checklist_true.jpg';
    this.urlCheckBlock = '/widgets/images/checklist_block.jpg';
    
    this.urlCheckTrueSquare = '/widgets/images/checklist_true_square.jpg';
    this.urlCheckBlockSquare = '/widgets/images/checklist_block_suare.jpg';
    
    this.urlImg_minus= '/widgets/css/images/triangle_up.gif';
    this.urlImg_plus= '/widgets/css/images/triangle_down.gif';
    this.dicContentAccessID = "#clientAccessSettings";
    //---------------------------------------------------------------------
    this.checkSpuareOpen = function(el,action)
    {
        var elOpen = jQuery(el).find('.ckChildrenOpen');
        var elRe = jQuery(el).find('.ckChildrenRestricted');
        if(action == 'none')
        {
            jQuery(elOpen).attr("src",Me.urlCheckFalse);
            jQuery(elOpen).attr("checked","false");
        }
        else if(action == 'square')
        {
            jQuery(elOpen).attr("src",Me.urlCheckTrueSquare);  
            jQuery(elOpen).attr("checked","square");
        }
        else if(action == 'checked')
        {
            jQuery(elOpen).attr("src",Me.urlCheckTrue);  
            jQuery(elOpen).attr("checked","true");
            jQuery(elRe).attr("src",Me.urlCheckFalse);  
            jQuery(elRe).attr("checked","false");
        }
    }
    //---------------------------------------------------------------------
    this.checkSpuareRe = function(el,action)
    {
        //var elOpen = jQuery(el).find('.ckChildrenOpen');
        var elRe = jQuery(el).find('.ckChildrenRestricted');
        if(action == 'none')
        {
            jQuery(elRe).attr("src",Me.urlCheckFalse);
            jQuery(elRe).attr("checked","false");
        }
        else if(action == 'square')
        {
            jQuery(elRe).attr("src",Me.urlCheckBlockSquare);  
            jQuery(elRe).attr("checked","square");
        }
        else if(action == 'checked')
        {
            jQuery(elRe).attr("src",Me.urlCheckBlock);  
            jQuery(elRe).attr("checked","true");
        }
    }
    /**
     * Quick Ticket
     */
    //-----------------------------------------------------------
    this.clientQuickTickets = "#clientQuickTickets";
    this.getdataQuickTickets = function()
    {
        Me.LoadingAjax(Me.clientQuickTickets);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/quick-ticket/client-list",
            data: "",
            success:function( html ) {
                jQuery(Me.clientQuickTickets).html(html);
                //build event Save 
                jQuery(Me.clientQuickTickets +" #cmdAddEditQuickTicket").unbind("click",Me.cmdAddEditQuickTickets)
                .bind("click",Me.cmdAddEditQuickTickets);
                jQuery(Me.clientQuickTickets +" .cmdAddEditQuickTicket").unbind("click",Me.cmdAddEditQuickTickets)
                .bind("click",Me.cmdAddEditQuickTickets);
                jQuery(".cmdEditProjectQT").unbind("click",Me.cmdEditProjectQT)
                .bind("click",Me.cmdEditProjectQT);
                jQuery(".cmdAddUserQT").unbind("click",Me.cmdAddUserQT)
                .bind("click",Me.cmdAddUserQT);
                jQuery(".cmdEditUserQT").unbind("click",Me.cmdEditUserQT)
                .bind("click",Me.cmdEditUserQT);
                jQuery(".cmdRemoveUserQT").unbind("click",Me.cmdRemoveUserQT)
                .bind("click",Me.cmdRemoveUserQT);
                jQuery("#cmdWhatIsQuickTicket").unbind("click",Me.cmdWhatIsQuickTicket)
                .bind("click",Me.cmdWhatIsQuickTicket);
                jQuery(".cmdSampleQT").unbind("click",Me.cmdSeeSample)
                .bind("click",Me.cmdSeeSample);
                jQuery(".Customsetting").unbind("click",Me.Customsetting_Onclick)
                .bind("click",Me.Customsetting_Onclick);
                if(jQuery("#isnewqt").val() == "1")
                {
                    jQuery("#cmdWhatIsQuickTicket").click();
                }
            }
        });
    }
    //----------------------------------------------------
    this.Customsetting_Onclick = function()
    {
        var typevalue = jQuery(this).attr("typevalue");
        var itemid = jQuery(this).attr("itemid");
        var checked = jQuery(this).is(":checked")?1:0;
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/quick-ticket/client-savecustomsetting",
            data: "typevalue="+typevalue+"&itemid="+itemid+"&checked="+checked,
            success:function( html ) {
               
            }
        });
    }
    this.clientMySettings = "#clientMySettings";
    this.getdataMySettings = function()
    {
        Me.LoadingAjax(Me.clientMySettings);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/client/my-settings",
            data: "",
            success:function( html ) {
                jQuery(Me.clientMySettings).html(html);
				jQuery(Me.clientMySettings +" .cmdCollspan").unbind("click",Me.cmdCollspan_onclick).bind("click",Me.cmdCollspan_onclick); 
            }
        });
    }
    /*
    this.clientMySettings = "#clientMySettings";
    this.getdataMySettings = function()
    {
        Me.LoadingAjax(Me.clientMySettings);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/client/my-settings",
            data: "",
            success:function( html ) {
                jQuery(Me.clientMySettings).html(html);
				jQuery(Me.clientMySettings +" .cmdCollspan").unbind("click",Me.cmdCollspan_onclick).bind("click",Me.cmdCollspan_onclick); 
            }
        });
    }
    */
    this.clientUsers = "#clientUsers";
    this.getdataUsers = function()
    {
    	Me.LoadingAjax(Me.clientUsers);
    	detailsWidget = new FSWidgetClientDetails({container:'clientUsers',tab:'list'},Me._roll);
        detailsWidget.setParams({Company_ID:Me.companyName});
        detailsWidget.show();
     // add to manager
        if(!wManager._widgets.mainContainer){
	        wManager.add(detailsWidget, 'mainContainer');
	        wManager._widgets.mainContainer = detailsWidget;
        }
        
    	$.ajax({
            type: "POST",
            url: "/widgets/dashboard/client/user-list",
            data: {size:"20", Company_ID: Me.companyName},
            success:function( html ) {
                jQuery(Me.clientUsers).html(html);
				jQuery(Me.clientUsers +" .cmdCollspan").unbind("click",Me.cmdCollspan_onclick).bind("click",Me.cmdCollspan_onclick); 
            }
        });
    }
    this.clientMySettings = "#clientMySettings";
    this.getdataMySettings = function()
    {
        Me.LoadingAjax(Me.clientMySettings);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/client/my-settings",
            data:{Company_ID:this.companyName,notice:Me.notice},
            success:function( html ) {
                jQuery(Me.clientMySettings).html(html);
				jQuery(Me.clientMySettings +" .cmdCollspan").unbind("click",Me.cmdCollspan_onclick).bind("click",Me.cmdCollspan_onclick);
				$("#restrictProjectSelect").unbind("change").change(Me.restrictProjectAdd).each(Me.hideSelectIfEmpty);
				$("#MyDashboard_FilterByProjectSelect").unbind("change").change(Me.MyDashboardSettingsProjectAdd).each(Me.hideSelectIfEmpty);
				$("#MySettingsForm").submit(Me.onSubmitMySettings);
				Me.prepareProjectRestriction();
				Me.prepareMyDashboardSettings();
				$("#FAQMyProjectsCreate").bt("Project Controllers are listed on the Basic Information tab of the Project Template. These restrictions will only be applied on Projects you control.",
{trigger:'click',positions:'right',fill: 'white',cssStyles:{color:'black'}});
				Me.onSubmitPrepare();
                                jQuery("#cbxMyNotices").unbind("change",Me.cbxMyNotices_change).bind("change",Me.cbxMyNotices_change);
                                jQuery(".radioMyNoticesSettings").unbind("click",Me.radioMyNoticesSettings).bind("click",Me.radioMyNoticesSettings);
				$("#MySettingsForm").data("_default", $("#MySettingsForm").serialize());
            }
        });
    }
    this.radioMyNoticesSettings = function(){
        Me.autoResize();
    }
	this.cbxMyNotices_change = function(){
            var value = jQuery(this).find("option:selected").val();
            var text = jQuery(this).find("option:selected").html();
            if (value == "" || value == 0) return false;    
            jQuery("#MyNotices").append("<tr><td style='padding-left:0px;'><input type='checkbox' value='"+value+"' checked='true' name='MyNotices[]' />&nbsp;&nbsp;&nbsp;<a target='_blank' href='projectsView_new.php?v=FLS&amp;tab=list&amp;project_id="+value+"'>"+value+"</a> - "+text+"</td></tr>");
            jQuery(this).find("option:selected").remove();
            if(jQuery("#cbxMyNotices option").size()==1)
            {
            jQuery("#divmyNotice").hide();
            }
            Me.autoResize();
        }
        this.autoResize = function(){
            if(Me.notice != ""){
                if(typeof(window.parent) != 'undefined'){
                    window.parent._global.autoResize(null,jQuery('body').height());
                }
            }
        }
	this.restrictProjectAdd = function() {
		projectSelected = $("#restrictProjectSelect option:selected");
		projectId = projectSelected.val();
		if (projectId == "" || projectId == 0) return false;
		// create section from template
		Me.appendProjectRestrictionPartial(projectId, projectSelected);
		// remove project from dropdown
		projectSelected.remove();
		Me.updateRestrictProjectList();
		$("#restrictProjectSelect").each(Me.hideSelectIfEmpty);
	}

	this.restrictUserAdd = function() {
		userSelected = $("option:selected", this);
		elementName = $(this).attr('id');
		pid = elementName.split("restrictUserSelect");
		pid = pid[1];
		// create section from template
		if (userSelected.val() == '') return;
		Me.appendUserRestrictionPartial(pid, userSelected.val(), userSelected.text());
		// remove user from dropdown
		userSelected.remove();
		$(this).each(Me.hideSelectIfEmpty);
	}

	this.onSubmitPrepare = function() {
		if (!$("#MyProjects_CreateWO_AllowAll").attr("checked")) { 
			MyProjectControls = {};
			$(".restrictProjectCheckbox").each(function() {
				pid = $(this).val();
				if (pid == 0) return;
				add = $(this).attr('checked');
				MyProjectControls[pid] = {'add': add, 'userlist_key' : [], 'userlist_value' : []};
				if (add) {
					$(".MyProjects_AllowUserCheckbox" + pid).each(function() {
						username = $(this).val();
						MyProjectControls[pid]['userlist_key'].push(username);
						MyProjectControls[pid]['userlist_value'].push($(this).attr('checked'));
					});
				}
			});
			$("#MyProjectControls").val(JSON.stringify(MyProjectControls));
		}

		// My Dashboard Setting
		if ($("#MyDashboard_UseCustom").attr("checked") && $("#MyDashboard_FilterByProject").attr("checked") && $("#MyDashboard_Project_UseCustom").attr("checked")) { 
			MyDashboardSettings = {};
			$(".MyDashboardProjectCheckbox").each(function() {
				pid = $(this).val();
				if (pid == 0) return;
				add = $(this).attr('checked');
				MyDashboardSettings[pid] = {'add': add };
			});
			$("#MyDashboardSettings").val(JSON.stringify(MyDashboardSettings));
		}
		// End My Dashboard Setting		
	}
	
	this.onSubmitMySettings = function() {
		Me.onSubmitPrepare();
		Me.WaitingSave();
		data = $("#MySettingsForm").serialize();
		$.ajax({
			type: "POST",
			url: "/widgets/dashboard/client/save-my-settings",
			data: data,
			success:function( html ) {
				if(html.success == 0)
					Me.showMessages(html);
				else
				{
					Me._rollWaitingSave.hide();
					Me.getdataMySettings();
                                        window.location.reload();
				}
			}
		});
		return false;
	}
	
	this.hideSelectIfEmpty = function(index) {
		if ($("option", this).size() == 1) $(this).hide();
	}

	this.prepareProjectRestriction = function() {
		$('.MyProjects_AllowOnlyMe').unbind('click').click(Me.onclickAllowOnlyMe);
		$('.MyProjects_AllowUsers').unbind('click').click(Me.onclickAllowUsers);
		$('.restrictUserSelect').unbind('change').change(Me.restrictUserAdd).each(Me.hideSelectIfEmpty);
		$('#MyProjects_CreateWO_AllowAll').unbind('click').click(Me.onclickMyProjects_CreateWO_AllowAll);
		$('#MyProjects_CreateWO_Restrict').unbind('click').click(Me.onclickMyProjects_CreateWO_Restrict);
		if ($('#restrictProjectSelect option').size() == 1 && $(".restrictProjectCheckbox").size() == 1) {
			$(".MyProjects_CreateWO_SectionHeader").hide();
			$("#MyProjects_CreateWO_Section").hide();
		}
	}
		
	this.appendProjectRestrictionPartial = function (pid, projectSelect) {
		projectList = $("#restrictProjectList");
		partial = $(".projectRestrictionPartial").clone();
		partial.removeClass("projectRestrictionPartial");
		Me.changeElementIdAndNameIndependent($("#MyProjects_AllowOnlyMe0", partial), 'MyProjects_AllowOnlyMe' + pid, 'MyProjects_AllowSetting' + pid);
		Me.changeElementIdAndNameIndependent($("#MyProjects_AllowUsers0", partial), 'MyProjects_AllowUsers' + pid, 'MyProjects_AllowSetting' + pid);
		Me.changeElementIdAndNameIndependent($("#MyProjects_AllowUsers0", partial), 'MyProjects_AllowSetting' + pid);
		Me.changeElementIdAndName($("#restrictProjectCheckbox0", partial), 'restrictProjectCheckbox' + pid);
		$("#restrictProjectCheckbox" + pid, partial).val(pid);
		Me.changeElementIdAndName($("#restrictUserSection0", partial), 'restrictUserSection' + pid);

		Me.changeElementIdAndName($("#restrictUserSelect0", partial), 'restrictUserSelect' + pid);
		Me.changeElementIdAndName($("#restrictUserList0", partial), 'restrictUserList' + pid);
		Me.changeElementIdAndName($("#restrictProjectName0", partial), 'restrictProjectName' + pid);
		$("#restrictProjectName" + pid + " a:first", partial).attr('href', "projectsView_new.php?v=" +  projectSelect.attr('_company') + "&tab=list&project_id=" + pid).text(pid);
		Me.changeElementIdAndName($("#restrictProjectNameText0", partial), 'restrictProjectNameText' + pid);
		
		$("#restrictProjectNameText" + pid, partial).text(projectSelect.text());
		$("#restrictUserSelect" + pid + " option", partial).each(function(index) {
			// filter out users not part of company
			if ($(this).val() == '') return;
			if ($(this).attr('_company') != projectSelect.attr('_company')) {
				$(this).remove();
			}
		});
		projectList.append(partial);
		Me.prepareProjectRestriction();
		$('#MyProjects_AllowOnlyMe' + pid).click();
	}

	this.appendUserRestrictionPartial = function (pid, username, text) {
		userList = $("#restrictUserList" + pid);
		partial = $(".userRestrictionPartial").clone();
		partial.removeClass("userRestrictionPartial");
		Me.changeElementIdAndName($("#MyProjects_AllowUser0", partial), 'MyProjects_AllowUser' + pid + "_" + username);
		Me.changeElementIdAndName($("#restrictUserName0", partial), 'restrictUserName' + pid + "_" + username);
		$("#MyProjects_AllowUser" + pid + "_" + username, partial).val(username);
		$("#restrictUserName" + pid + "_" + username, partial).text(text);
		$(".MyProjects_AllowUserCheckbox0", partial).removeClass("MyProjects_AllowUserCheckbox0").addClass("MyProjects_AllowUserCheckbox" + pid);
		userList.append(partial);
	}

	this.onclickAllowOnlyMe = function() {
		elementName = $(this).attr('id');
		pid = elementName.split("MyProjects_AllowOnlyMe");
		pid = pid[1];
		$("#restrictUserSection" + pid).hide();
	}
	
	this.onclickAllowUsers = function() {
		elementName = $(this).attr('id');
		pid = elementName.split("MyProjects_AllowUsers");
		pid = pid[1];
		if ($("#restrictUserSection" + pid).size() == 0) {
			Me.appendProjectRestrictionPartial(pid, $("#restrictProjectName" + pid));
		}
		$("#restrictUserSection" + pid).show();
	}
	
	this.onclickMyProjects_CreateWO_AllowAll = function () {

		$("#restrictProjectList").hide();
		$("#restrictProjectTr").hide();
	}

	this.onclickMyProjects_CreateWO_Restrict = function () {
		Me.updateRestrictProjectList();
		$("#restrictProjectList").show();
		$("#restrictProjectTr").show();
	}


	this.updateRestrictProjectList = function() {
		if ($(".restrictProjectCheckbox").size() == 1)
			$("#restrictProjectTr option:first").text("Select a Project");
		else
			$("#restrictProjectTr option:first").text("Select Another Project");
	}
	
	// My Dashboard
	
	this.onclickMyDashboard_UseDefault = function () {
		$("#MyDashboard_FilterBy").hide();
//		$("#restrictProjectTr").hide();
	}
	
	this.onclickMyDashboard_UseCustom = function () {				
		Me.updateMyDashboard_FilterByProjectTr();
		$("#MyDashboard_FilterBy").show();
	}
	
	this.onclickMyDashboard_UseDefaultFilters = function () {
		$("#MyDashboard_FilterByProjectSection").hide();
		$("#MyDashboard_FilterByWorkOrderSection").hide();
	}

	this.onclickMyDashboard_FilterByProject = function () {
		$("#MyDashboard_FilterByProjectSection").show();
		$("#MyDashboard_FilterByWorkOrderSection").hide();
	}

	this.onclickMyDashboard_FilterByWorkOrder = function () {
		$("#MyDashboard_FilterByProjectSection").hide();
		$("#MyDashboard_FilterByWorkOrderSection").show();
		Me.updateMyDashboard_FilterByProjectTr();
		$("#MyDashboard_FilterByProject").attr('checked', false);
	}
	
	this.onclickMyDashboard_Project_UseDefault = function () {
		$("#MyDashboard_FilterByProjectList").hide();
		$("#MyDashboard_FilterByProjectTr").hide();
		Me.updateMyDashboard_FilterByProjectTr();
		$("#MyDashboard_FilterByProject").attr('checked', true);
	}
	
	this.onclickMyDashboard_Project_UseCustom = function () {
		$("#MyDashboard_FilterByProjectList").show();
		$("#MyDashboard_FilterByProjectTr").show();
		Me.updateMyDashboard_FilterByProjectTr();
		$("#MyDashboard_FilterByProject").attr('checked', true);
	}
	
	this.updateMyDashboard_FilterByProjectTr = function() {
		if ($(".MyDashboardProjectCheckbox").size() == 1)
			$("#MyDashboard_FilterByProjectTr option:first").text("Select a Project");
		else
			$("#MyDashboard_FilterByProjectTr option:first").text("Select Another Project");
	}
	
	this.appendMyDashboardPartial = function (pid, projectSelect) {
		projectList = $("#MyDashboard_FilterByProjectList");
		partial = $(".MyDashboard_ProjectPartial").clone();
		partial.removeClass("MyDashboard_ProjectPartial");
		Me.changeElementIdAndName($("#MyDashboardProjectCheckbox0", partial), 'MyDashboardProjectCheckbox' + pid);
		$("#MyDashboardProjectCheckbox" + pid, partial).val(pid);
		Me.changeElementIdAndName($("#MyDashboardProjectName0", partial), 'MyDashboardProjectName' + pid);
		$("#MyDashboardProjectName" + pid + " a:first", partial).attr('href', "projectsView_new.php?v=" +  projectSelect.attr('_company') + "&tab=list&project_id=" + pid).text(pid);
		Me.changeElementIdAndName($("#MyDashboardProjectNameText0", partial), 'MyDashboardProjectNameText' + pid);
		$("#MyDashboardProjectNameText" + pid, partial).text(projectSelect.text());
		projectList.append(partial);
		Me.prepareMyDashboardSettings();
//		$('#MyProjects_AllowOnlyMe' + pid).click();
	}

	this.MyDashboardSettingsProjectAdd = function() {
		projectSelected = $("#MyDashboard_FilterByProjectSelect option:selected");
		projectId = projectSelected.val();
		if (projectId == "" || projectId == 0) return false;
		// create section from template
		Me.appendMyDashboardPartial(projectId, projectSelected);
		// remove project from dropdown
		projectSelected.remove();
		Me.updateMyDashboard_FilterByProjectTr();
		$("#MyDashboard_FilterByProjectSelect").each(Me.hideSelectIfEmpty)
	}
		
	this.prepareMyDashboardSettings = function() {
		$('#MyDashboard_Project_UseDefault').unbind('click').click(Me.onclickMyDashboard_Project_UseDefault);
		$('#MyDashboard_Project_UseCustom').unbind('click').click(Me.onclickMyDashboard_Project_UseCustom);
		$('#MyDashboard_FilterByDefault').unbind('click').click(Me.onclickMyDashboard_UseDefaultFilters);
		$('#MyDashboard_FilterByProject').unbind('click').click(Me.onclickMyDashboard_FilterByProject);
		$('#MyDashboard_FilterByWorkOrder').unbind('click').click(Me.onclickMyDashboard_FilterByWorkOrder);
		$('#MyDashboard_UseDefault').unbind('click').click(Me.onclickMyDashboard_UseDefault);
		$('#MyDashboard_UseCustom').unbind('click').click(Me.onclickMyDashboard_UseCustom);
		$('#MyDashboard_Sort_Default').unbind('click').click(Me.onclickMyDashboard_Sort_Default);
		$('#MyDashboard_Sort_Recent').unbind('click').click(Me.onclickMyDashboard_Sort_Default);
		$('#MyDashboard_Sort_Default_My').unbind('click').click(Me.onclickMyDashboard_Sort_Default_My);
		try {
			wd.currentTab = 'created';
			Me.prepareMyDashboardSettingsSort('createdSort');
                        wd.currentTab = 'todayswork';
			Me.prepareMyDashboardSettingsSort('todaysworkSort');
			wd.currentTab = 'published';
			Me.prepareMyDashboardSettingsSort('publishedSort');
			wd.currentTab = 'assigned';
			Me.prepareMyDashboardSettingsSort('assignedSort');
			wd.currentTab = 'workdone';
			Me.prepareMyDashboardSettingsSort('workDoneSort');
			wd.currentTab = 'incomplete';
			Me.prepareMyDashboardSettingsSort('incompleteSort');
			wd.currentTab = 'completed';
			Me.prepareMyDashboardSettingsSort('completedSort');
			wd.currentTab = 'all';
			Me.prepareMyDashboardSettingsSort('allSort');
			wd.currentTab = 'deactivated';
			Me.prepareMyDashboardSettingsSort('deactivatedSort');
		} catch (e) {
//			setTimeout("Me.prepareMyDashboardSettings()", 50);
		}
	}
	
	this.prepareMyDashboardSettingsSort = function(id) {
		wd.sortTool(new FSWidgetSortTool(id, id + 'Dir', wd, roll, 'quick'));
		sort = $("#" + id);
		dir = $("#" + id + "Dir");
		dSort = sort.attr('def');
		dDir = dir.attr('def');
		sort.unbind('change');
		dir.unbind('change');
		if (dSort) sort.val(dSort);
		if (dDir) dir.val(dDir == 0 ? 'asc' : 'desc');
		
	}
	
	this.onclickMyDashboard_Sort_Default = function() {
		$("#MyDashboard_Sort_Default_My_Section").hide();
	}

	this.onclickMyDashboard_Sort_Default_My = function() {
		$("#MyDashboard_Sort_Default_My_Section").show();
	}
	
	// End My Dashboard

	this.changeElementIdAndName = function(element, name) {
		Me.changeElementIdAndNameIndependent(element, name, name);
	}

	this.changeElementIdAndNameIndependent = function(element, id, name) {
		element.attr('id', id).attr('name', name);
	}

    this.clientUsers = "#clientUsers";
    this.getdataUsers = function()
    {
    	Me.LoadingAjax(Me.clientUsers);
    	detailsWidget = new FSWidgetClientDetails({container:'clientUsers',tab:'list'},Me._roll);
        detailsWidget.setParams({Company_ID:Me.companyName,AccountEnabled:"1",size:"20" });
        detailsWidget.show();
     // add to manager
        if(!wManager._widgets.mainContainer){
	        wManager.add(detailsWidget, 'mainContainer');
	        wManager._widgets.mainContainer = detailsWidget;
        }
        /*
    	$.ajax({
            type: "POST",
            url: "/widgets/dashboard/client/user-list",
            data: {size:"20", Company_ID: Me.companyName, AccountEnabled:"1"},
            success:function( html ) {
                jQuery(Me.clientUsers).html(html);
				jQuery(Me.clientUsers +" .cmdCollspan").unbind("click",Me.cmdCollspan_onclick).bind("click",Me.cmdCollspan_onclick); 
            }
        });
        */
    }

    //----------------------------------------------------
    this.cmdAddEditQuickTickets = function()
    {
        var  paddingleft = 0;
        if(jQuery(this).attr('id') == "cmdAddEditQuickTicket") paddingleft = -225;
        Me._roll.autohide(false);
        var opt = {};
        opt.width    = 400;
        opt.height   = 250;
        opt.paddingleft   = paddingleft;
        opt.paddingTop   = 0;
        //opt.position = 'middle';
        opt.title    = '<center>Add / Edit QuickTicket Views</center>';
        opt.body     = "<div id='contantAddEditQuickTicket'><center><img src='/widgets/images/wait.gif' border='0' /></center></div>";
        Me._roll.showNotAjax(this, this, opt);
        
        var el = this;
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/quick-ticket/client-edit",
            data: "",
            success:function( html ) {
                jQuery("#contantAddEditQuickTicket").html(html);
                //build event Save 
                jQuery("#cmdAddNewQuickTicket").unbind("click",Me.cmdAddNewQuickTicket)
                .bind("click",Me.cmdAddNewQuickTicket);
                jQuery("#cmdSaveQuickTecket").unbind("click",Me.cmdSaveQuickTecket)
                .bind("click",Me.cmdSaveQuickTecket);
                jQuery(".cmdCancelQuickTecket").unbind("click",Me.hideRoll)
                .bind("click",Me.hideRoll);
            }
        });
    }
    //----------------------------------------------------
    this.cmdAddNewQuickTicket = function()
    {
        var html = "<tr qtid='0'>\
                        <td style='padding:3px;'>"+Me.companyName+" - <input style='width:130px;' type='text' class='txtQuickTicketName' /></td>\
                        <td style='padding:3px;' align='center' width='20%'><input type='checkbox' checked='true' class='QuickTicketChk' /></td>\
                    </tr>";
        jQuery("#contentQT").find('tbody').append(html);
    }
    ////----------------------------------------------------
    this.cmdSaveQuickTecket = function()
    {
        var data = "";
        var error = false;
        var ik = 0;
        var e = null;
        var el = [];
        jQuery("#tbodyQuickTiket").find('tr').each(function(i) {
            var value = jQuery(this).find('.txtQuickTicketName').val();
            //var email = jQuery(this).find('.txtQuickTicketEmail').val();
            var active = jQuery(this).find('.QuickTicketChk').is(':checked')?1:0;
            var qtid = jQuery(this).attr("qtid");
            el[i] = jQuery(this).find('.txtQuickTicketName');
            jQuery(this).find('.txtQuickTicketName').removeClass('avaidQT');
            if(jQuery.trim(value) != "")
            {
                data += ',{"id":"'+qtid+'","value":"'+Base64.encode(value)+'","active":"'+active+'"}';
                jQuery(this).find('.txtQuickTicketName').removeClass('errorQT');
            }    
            else
            {
                if(jQuery.trim(value) == "")
                {
                    if(i==0) e = jQuery(this).find('.txtQuickTicketName');
                    jQuery(this).find('.txtQuickTicketName').addClass('errorQT');
                    error = true;
                    ik++;
                }
            }    
        });
        // check blank
        if(error) 
        {
            jQuery("#QuickTicketError").html('You missed '+ik+' fields. They have been highlighted below');
            jQuery(e).focus();
            return false;
        }    
        // check existing
        var error = false;
        var e1 = null;
        var k = 0;
        if(el.length > 0)
        {
            for(var i = 0; i < el.length; i++)
            {
                var val1 = jQuery(el[i]).val();
                e1 = jQuery(el[i]);
                for(var j = i+ 1 ; j < el.length; j++)
                {
                    var val2 = jQuery(el[j]).val();
                    if(jQuery.trim(val1) == jQuery.trim(val2))
                    {
                        jQuery(el[j]).addClass('errorQT');
                        if(!error) e = jQuery(el[j]);
                        k++;
                        error = true;
                    }    
                }  
                if(error) break;
            }    
        }   
        if(error) 
        {
            jQuery("#QuickTicketError").html('You have '+k+' fields existing. They have been highlighted below');
            jQuery(e1).addClass('avaidQT');
            jQuery(e).focus();
            return false;
        }
        // run
        if(data != "")
        {
            data = "["+data.substring(1, data.length) + "]";
        }  
        
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/quick-ticket/client-save",
            data: "json="+data,
            success:function( html ) {
                if(html.success && Me._roll != null)
                {
                    Me._roll.hide();
                    Me.getdataQuickTickets();
                }    
            }
        });
    }
    this.isEmail = function(value){
        var re = new RegExp("^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]{2,4}$");
        return (value.search(re) != -1);
    }
    ////--------------------------------------------------
    this.cmdEditProjectQT = function()
    {
        Me._roll.autohide(false);
        var opt = {};
        opt.width    = 400;
        opt.height   = 250;
        opt.paddingleft   = -250;
        opt.paddingTop   = 0;
        //opt.position = 'middle';
        opt.title    = '<center>Select Projects for '+jQuery(this).attr('CompanyName')+' '+jQuery(this).attr('CustomerName')+'</center>';
        opt.body     = "<div id='contantEditProjectQT'><center><img src='/widgets/images/wait.gif' border='0' /></center></div>";
        Me._roll.showNotAjax(this, this, opt);
        
        var el = this;
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/quick-ticket/client-project-edit",
            data: "id="+jQuery(el).attr('qtid'),
            success:function( html ) {
                jQuery("#contantEditProjectQT").html(html);
                jQuery(".cmdCancelQuickTecket").unbind("click",Me.hideRoll)
                .bind("click",Me.hideRoll);
                jQuery("#cmdSaveProjectQuickTecket").unbind("click",Me.cmdSaveProjectQuickTecket)
                .bind("click",Me.cmdSaveProjectQuickTecket);
            }
        });
    }
    ////----------------------------------------------------
    this.hideRoll = function()
    {
        if(Me._roll != null)
        {
            Me._roll.hide();
        }    
    }
    ////----------------------------------------------------
    this.cmdSaveProjectQuickTecket = function()
    {
        var data = "";
        jQuery("#contentProjectQT").find("#tbodyProjectQuickTiket .QuickTicketChk").each(function(){
            if(jQuery(this).is(":checked"))
            {
                data += ","+jQuery(this).attr("qtid");
            }    
        });
        if(data !="")
        {
            data = data.substring(1, data.length);
        }    
        var idqt = jQuery("#hidQTid").val();
        var el = this;
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/quick-ticket/client-save-project-edit",
            data: "proid="+data+"&id="+idqt,
            success:function( html ) {
                if(html.success && Me._roll != null)
                {
                    Me._roll.hide();
                    Me.getdataQuickTickets();
                }  
            }
        });
    }
    //------------------------------------------------------------
    this.cmdAddUserQT = function()
    {
        Me._roll.autohide(false);
        var opt = {};
        opt.width    = 300;
        opt.height   = 250;
        opt.paddingleft   = -160;
        opt.paddingTop   = 0;
        //opt.position = 'middle';
        opt.title    = '<center>Add/Edit Customer User</center>';
        opt.body     = "<div id='contantAddUserQT'><center><img src='/widgets/images/wait.gif' border='0' /></center></div>";
        Me._roll.showNotAjax(this, this, opt);
        
        var el = this;
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/quick-ticket/client-user-add",
            data: "id="+jQuery(el).attr('qtid')
            +"&CompanyName="+jQuery(el).attr("CompanyName")
            +"&CustomerName="+jQuery(el).attr('CustomerName'),
            success:function( html ) {
                jQuery("#contantAddUserQT").html(html);
                jQuery(".cmdCancelQuickTecket").unbind("click",Me.hideRoll)
                .bind("click",Me.hideRoll);
                jQuery("#cmdSaveUserQuickTecket").unbind("click",Me.cmdSaveUserQuickTecket)
                .bind("click",Me.cmdSaveUserQuickTecket);
            }
        });
    }
    //------------------------------------------------------------
    this.cmdSaveUserQuickTecket = function()
    {  
        if(Me.checkValidUser())
        {
            // save data
            var data = jQuery("#formAddUserQT").serialize();
            $.ajax({
                type: "POST",
                url: "/widgets/dashboard/quick-ticket/client-save-user-add",
                data: data,
                success:function( html ) {
                    if(html.success == 0)
                    {
                        Me.showMessages(html);
                    }   
                    else
                    {
                        Me._roll.hide();
                        Me.getdataQuickTickets();
                    }    
                }
            });
        } 
    }
    //------------------------------------------------------------
    this.checkValidUser = function()
    {
        Me._redirect = false;
        var updatestatus = {
            success:1, 
            errors:[]
        };

        if($('#ContactName').val() == '') updatestatus.errors.push('Contact Name is empty but reqired');
        if($('#ContactPhone1').val() == '') updatestatus.errors.push('Primary Phone is empty but reqired');
        if($('#Email1').val() == '') updatestatus.errors.push('Email Address is empty but reqired');
        if($('#Username').val() == '') updatestatus.errors.push('Desired Username is empty but reqired');
        if($('#Password').val() == '') updatestatus.errors.push('Desired Password is empty but reqired');
        if($('#PasswordConf').val() == '' && typeof($('#PasswordConf').get(0)) != 'undefined') updatestatus.errors.push('Confirm Password is empty but reqired');
        if(!Me.isValidContactPhone($('#ContactPhone1').val(),true) && $('#ContactPhone1').val()!='')
            updatestatus.errors.push('Primary Phone must be a valid 10 digit number with optional extension similar to ###-###-#### x ####');
        if(!Me.validEmailListNew($('#Email1').val()) && $('#Email1').val()!='')
            updatestatus.errors.push('Primary Email Address is not a valid email address');
        if($('#Password').val() != $('#PasswordConf').val() && typeof($('#PasswordConf').get(0)) != 'undefined') updatestatus.errors.push('Values in Desired Password and Confirm Password are not the same');

        if($('#Active').val() == '') updatestatus.errors.push('Active is empty but reqired');
        if(updatestatus.errors.length <= 0)
        {
            return true;
        }
        else
        {
            updatestatus.success = 0;
            Me.showMessages(updatestatus);
            return false;
        }    
    }
    //------------------------------------------------------------
    this.cmdEditUserQT = function()
    {
        Me._roll.autohide(false);
        var opt = {};
        opt.width    = 300;
        opt.height   = 230;
        //opt.position = 'middle';
        opt.title    = '<center>Edit Customer User</center>';
        opt.body     = "<div id='contantEditUserQT'><center><img src='/widgets/images/wait.gif' border='0' /></center></div>";
        Me._roll.showNotAjax(this, this, opt);
        
        var el = this;
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/quick-ticket/client-user-edit",
            data: "id="+jQuery(el).attr('qtid')
            +"&userid="+jQuery(el).attr('userid')
            +"&CompanyName="+jQuery(el).attr("CompanyName")
            +"&CustomerName="+jQuery(el).attr('CustomerName'),
            success:function( html ) {
                jQuery("#contantEditUserQT").html(html);
                jQuery(".cmdCancelQuickTecket").unbind("click",Me.hideRoll)
                .bind("click",Me.hideRoll);
                jQuery("#cmdSaveEditUserQuickTecket").unbind("click",Me.cmdSaveEditUserQuickTecket)
                .bind("click",Me.cmdSaveEditUserQuickTecket);
            }
        });
    }
    //------------------------------------------------------------
    this.cmdRemoveUserQT = function()
    {
        var userid = jQuery(this).attr("userid");
        var username = jQuery(this).attr("username");
        Me._roll.autohide(false);
        var opt = {};
        opt.width    = 300;
        opt.height   = 80;
        opt.paddingleft   = -250;
        opt.paddingTop   = 0;
        //opt.position = 'middle';
        opt.title    = '<center>Remove User</center>';
        opt.body     = "<div id='contantEditUserQT'>Are you sure to delete the '"+username+"' user?</div>"
        +"<div style='padding-top:5px;'>"
        +'<div style="float: right;"><input type="button" id="cmdDeleteUserQuickTecket" value="Delete" class="link_button_popup" /></div>'
        +'<div style="float: right;"><input type="button" value="Cancel" class="link_button_popup cmdCancelQuickTecket" /></div>'
        +"</div>";
        Me._roll.showNotAjax(this, this, opt);
        
        jQuery("#cmdDeleteUserQuickTecket").click(function() {
            Me.cmdDeleteUserQuickTecket(userid);
        });
        jQuery(".cmdCancelQuickTecket").unbind("click",Me.hideRoll)
        .bind("click",Me.hideRoll);
        
    }
    //------------------------------------------------------------
    this.cmdDeleteUserQuickTecket = function(userid)
    {
        var el = this;
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/quick-ticket/client-user-delete",
            data: "id="+userid,
            success:function( html ) {
                if(html.success == 0)
                {
                    Me.showMessages(html);
                }   
                else
                {
                    Me._roll.hide();
                    Me.getdataQuickTickets();
                }    
            }
        });
    }
    //------------------------------------------------------------
    this.cmdSaveEditUserQuickTecket = function()
    {  
        if(Me.checkValidUser())
        {
            // save data
            var data = jQuery("#formEditUserQT").serialize();
            $.ajax({
                type: "POST",
                url: "/widgets/dashboard/quick-ticket/client-save-user-edit",
                data: data,
                success:function( html ) {
                    if(html.success == 0)
                    {
                        Me.showMessages(html);
                    }   
                    else
                    {
                        Me._roll.hide();
                        Me.getdataQuickTickets();
                    }    
                }
            });
        } 
    }
    //------------------------------------------------------------
    this.cmdWhatIsQuickTicket = function()
    {
        Me._rollInstruction.autohide(false);
        var opt = {};
        opt.width    = 250;
        opt.height   = 450;
        //opt.position = 'middle';
        opt.title    = '<center>What is QuickTicket?</center>';
        opt.body     = "<div id='contantEditUserQT'>\n\
                            QuickTicket gives your customers visibility to the work you are running for them. Designated customer users can view, sort and download Work Orders from the projects you select, allowing them to track project status, but see no technician pay or project cost details.\n\
                        </div>\n\
                        <hr style='background-color:#a6a6a6;margin-left:20px;margin-right:20px;'>\n\
                        <center><b>Instructions</b></center>\n\
                        <table cellspacing='0' cellpadding='0' border='0' width='100%'>\n\
                            <tr>\n\
                                <td style='vertical-align:top; width:15px;'>1.</td>\n\
                                <td colspan='2'>Click the Add/Edit QuickTicket Views button in the upper right corner to create a QuickTicket View.</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td></td>\n\
                                <td style='vertical-align:top;width:15px;'>- </td>\n\
                                <td>Suggestion: Name the QuickTicket View after the Customer you are serving.</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td></td>\n\
                                <td style='vertical-align:top;'>- </td>\n\
                                <td>The Email Contact will be notified when your Customer Users add a QuickTicket Work Order. These Work Orders will be placed in the Created Tab of your Work Order Dashboard and will not be published without your approval</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td style='vertical-align:top;'>2. </td>\n\
                                <td colspan='2'>Click the Add/Remove Projects button to select the projects your Customer will see.</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td style='vertical-align:top;'>3. </td>\n\
                                <td colspan='2'>Click the Add Customer User button to create a user account for your Customer. You can add as many users as you wish.</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td style='vertical-align:top;'>4. </td>\n\
                                <td colspan='2'>Direct your Customer to <a href='http://www.fieldsolutions.com/quickticket' target='_blank'>www.fieldsolutions.com/quickticket</a> to view Work Orders from the Projects you chose.</td>\n\
                            </tr>\n\
                        </table>";
        Me._rollInstruction.showNotAjax(this, this, opt);
    }
    //------------------------------------------------------------
    this.validEmailList = function(list) {
        if (list == null || list == "") return false;
        list = list.replace(/\s/g);
        var listParts = list.split(",");
        var valid = true;
        for (var index in listParts) {
            var email = listParts[index];
            if (!isValidEmail(email)) {
                valid = false;
            }
        }
        return valid;
    }
    //------------------------------------------------------------
    this.validEmailListNew = function(list) {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var address = list;
        if(reg.test(address) == false) {
            return false;
        }
        return true;
    }
    //------------------------------------------------------------
    this.isValidContactPhone = function(val, allowExt) {
        if (val == null || val == "") return true;
        var valStrip = val.replace(/[^0-9]/g, "");
        if (valStrip.length == 10) {
            val = massagePhone(val);
            return isValidPhone(val);
        }
        else {
            if (!allowExt) return false;
            var validFormat = /^[(]?(\d{3})[)]?(?:\s|-|.)*(\d{3})[-\s.]*(\d{4})[^0-9]+([0-9]*)[^0-9]*([0-9]*)$/;
            var parts = val.match(validFormat);
            return !(parts == null);
        }
    }
    //----------------------getWorkOrderSettings------------------
    this.dicWorkOrderSettingsID = "#clientWorkOrderSettings";
    this.getWorkOrderSettings = function()
    {
        Me.LoadingAjax(Me.dicWorkOrderSettingsID);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/client/work-order-settings?client_id="+Me.client_id,
            data: "",
            success:function( html ) {
                jQuery(Me.dicWorkOrderSettingsID).html(html);
                jQuery("#cmdAddanotherCustomLabel")
                .unbind("click",Me.cmdAddanotherCustomLabel)
                .bind("click",Me.cmdAddanotherCustomLabel);
                jQuery("#EnableExpenseReportingTechPay")
                .unbind("click",Me.EnableExpenseReportingTechPay)
                .bind("click",Me.EnableExpenseReportingTechPay);  
                //376
                Me.EnableExpenseReportingTechPayfirsttime();  
//                Me.EnableExpenseReportingTechPay();  
                //end 376

                var btstr="FieldSolutions has partnered with DeVry University to deploy the &lsquo;Technician Field Service Real-Life Internship&rsquo; as a credit-bearing program for DeVry University students. <a href='/docs/FieldSolutions_DeVry_University_Press_Release.pdf' target='blank'>Click here</a> for more program details.<br/>Simply check the box to make your Work Orders available either for observation or work under the Internship Program.";
                $("#techworkcontrols").bt(btstr,{trigger:'click',positions:'right',fill: 'white',cssStyles:{color:'black'}});
                jQuery("#TechWorkControlsSave")
                .unbind("click",Me.TechWorkControlsSave)
                .bind("click",Me.TechWorkControlsSave);
                jQuery("#TechWorkControlsCancel").click(function(){
                    Me.getWorkOrderSettings();
                });    
                Me.EventCheckBoxList("masterCategoryAll","masterCategory");
                Me.EventCheckBoxList("masterReimbursableAll","masterReimbursable");
                Me.EventCheckBoxList("masterRequireDocumentAll","masterRequireDocument");
                jQuery("#WorkOrderSettingsTechPaySubmit")
                .unbind("click",Me.WorkOrderSettingsTechPaySubmit)
                .bind("click",Me.WorkOrderSettingsTechPaySubmit);
                jQuery("#WorkOrderSettingsTechPayCancel").click(function(){
                    Me.getWorkOrderSettings();
                });    
                jQuery("input:text[name='WorkOrderSettingsTechPay[bcatname][]']")
    .unbind("keyup",Me.onchangeWorkOrderSettingsTechPay)
    .bind("keyup",Me.onchangeWorkOrderSettingsTechPay);
				$("#clientWorkOrderSettings").data("_default", $("#clientWorkOrderSettings form").serialize());
            }
        });
    }
    this.WorkOrderSettingsTechPaySubmit = function()
    {
        Me.WaitingSave();
        var data = jQuery("#frmWorkOrderSettingsTechPay,frmWorkOrderSettingsCustomFields").serializeArray();
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/client/work-order-settings?client_id="+Me.client_id,
            data: data,
            success:function( data ) {
                Me._rollWaitingSave.hide();
                Me.showMessages(data);
                Me.getWorkOrderSettings();
            }
        });
    }
this.TechWorkControlsSave = function()
{
    var htmlstr = "\
<div id='TechWorkControlsContainer'>\n\
    <div id='optionBox' style='text-align:center;'>\n\
        <div id='optionMessage' style='margin: 0px 0 20px !important;font-size:14px;'><b>How would you like to apply these changes?</b></div>\n\
        <div>\n\
            <input type='button' chooseval='1' value='All Projects and All Work Orders' class='link_button wide_button220 TechWorkControlssave'/>\n\
        </div>\n\
        <div>\n\
            <input type='button' chooseval='2' value='Future Projects Only' class='link_button wide_button160 TechWorkControlssave'/>\n\
        </div>\n\
        <div>\n\
            <input type='button' chooseval='3' value='Cancel' class='link_button_popup TechWorkControlssave'/>\n\
        </div>\n\
    </div>\n\
</div>";
    var opt = {
        width       : 400,
        height      : '',
        title       : 'Update Options',//(abr == 'acs' ? "ACS" : "P2T&trade;") +' Update Options',
        position    : 'middle',
        //body        : $('#'+abr+'UpdateOptionsContainer').html()
        body        : htmlstr
    };
    Me._roll.showNotAjax(null, null, opt);
    $(".TechWorkControlssave").unbind('click').bind('click',Me.TechWorkControlsSaveaction);
   
}

this.TechWorkControlsSaveaction = function()
{
    var chooseval = $(this).attr('chooseval');
    
    var chooseval = $(this).attr('chooseval');
    if(chooseval == 3)
    {
        Me._roll.hide();
        return false;
    }
    
    Me.WaitingSave();
    var dataForm = jQuery("#TechWorkControls").serialize();
    dataForm += "&chooseval="+chooseval;
    
    $.ajax({
        type: "POST",
        url: "/widgets/dashboard/client/work-order-tech-Controls/",
        data: dataForm,
        success:function( data ) {
            Me._rollWaitingSave.hide();
            Me.showMessages(data);
            Me._roll.hide();
            Me.getWorkOrderSettings();
        }
    });
}

    this.EventCheckBoxList = function(all,item)
    {
        jQuery("#"+all).unbind("click",Me.checkAll).bind("click",Me.checkAll);
        jQuery("."+item).unbind("click",Me.checkItem).bind("click",Me.checkItem);
        Me.configCheckMaster(item);
    }
    this.checkAll = function()
    {
        var itemid = jQuery(this).attr("id");
        var _id = itemid;
        itemid = itemid.replace("All","");
        var checked = jQuery(this).is(":checked");
        jQuery("."+itemid).each(function(){
            var disabled = jQuery(this).attr("disabled");
            if(!disabled)
            {
                var ischeckCustem = true;
                if(_id == "masterCategoryAll"){
                    var el = jQuery(this).parent()
                    .children("input[name='WorkOrderSettingsTechPay[bcatname][]']");
                    if(jQuery.trim(jQuery(el).val()) == ""
                        && jQuery(el).attr("type") == "text")
                {
                        ischeckCustem = false;
                    }
                }
                if(checked && ischeckCustem)
                {
                    jQuery(this).attr("checked",true);
                    jQuery(this).parent().children("input:hidden[checked='true']").val("1");
                }
                else
                {
                    jQuery(this).removeAttr("checked");
                    jQuery(this).parent().children("input:hidden[checked='true']").val("0");
                }  
            }
        });
        Me.configCheckMaster(itemid,true);
    }
    this.checkItem = function()
    {
        var item = jQuery(this).attr("class");
        Me.configCheckMaster(item);
        //376
        if(item == "masterCategory" && jQuery(this).is(":checked"))
        {
            jQuery(this).parent().parent().find(".masterReimbursable").attr("checked","checked");
            jQuery(this).parent().parent().find("input:hidden[name='WorkOrderSettingsTechPay[reimbursable_thru_fs][]']").val("1");
            //$(".masterReimbursable").attr("checked","checked");
        }
        if(item == "masterCategory"){
            var checked = jQuery(this).is(":checked");
            jQuery(this).parent().parent().find("input").each(function(){
                if(jQuery(this).attr('class') != "masterCategory") 
                {
                    if((jQuery(this).attr('type') == "text" || jQuery(this).attr('type') == "checkbox")
                        && !checked)
                    {
                        if(jQuery(this).attr('type') == "text")
                            jQuery(this).attr("readonly",true); 
                        else
                            jQuery(this).attr("disabled","disabled");
                    } 
                    else 
                    {
                        jQuery(this).removeAttr("disabled");
                        jQuery(this).removeAttr("readonly");
                        if(jQuery(this).attr("class") != "masterRequireDocument")
                            jQuery(this).attr("checked",true);
                    } 
                }
            });
        }
        //end 376
        if(jQuery(this).is(":checked"))
        {
            if(jQuery(this).parent().attr("class")== "label_field")
            {
                jQuery(this).parent().children("input:hidden").each(function()
                {
                    if($(this).attr('name') == 'WorkOrderSettingsTechPay[ischecked][]')
                        $(this).val("1")
                });
            }
            else
            {
                jQuery(this).parent().children("input:hidden[checked=true]").val("1");
            }
        }    
        else
        {
            if(jQuery(this).parent().attr("class")== "label_field")
            {
                jQuery(this).parent().children("input:hidden").each(function()
                {
                    if($(this).attr('name') == 'WorkOrderSettingsTechPay[ischecked][]')
                        $(this).val("0")
                });
            }
            else
            {
                jQuery(this).parent().children("input:hidden[checked=true]").val("0");
            }
        }    
    }
    this.configCheckMaster = function(item,all)
    {
        var itemChecked = 0;
        var itemDisabled = 0;
        jQuery("."+item).each(function(){
            var checked = jQuery(this).is(":checked");
            var el = jQuery(this).parent().parent();
            var disabled = jQuery(this).attr("disabled");
            var isCustomer = false;
            if(item == "masterCategory")
            {
                var _el = jQuery(this).parent()
                .children("input[name='WorkOrderSettingsTechPay[bcatname][]']");
                if(jQuery.trim(jQuery(_el).val()) == ""
                    && jQuery(_el).attr("type") == "text")
                    {
                    isCustomer = true;
                }
            }    
            if((checked && !disabled) || isCustomer)
            {
                itemChecked++;
            } 
            if(disabled)
            {
                itemDisabled++;
            }    
            if(item == "masterCategory" && jQuery("#EnableExpenseReportingTechPay").is(":checked")  && all)
            {
                jQuery(el).find("input").each(function(){
                    if((jQuery(this).attr('type') == "text" || jQuery(this).attr('type') == "checkbox")
                        && jQuery(this).attr('class') != "masterCategory" && !checked)
                    {
                        if(jQuery(this).attr('type') == "text")
                            jQuery(this).attr("readonly",true); 
                        else
                            jQuery(this).attr("disabled","disabled");
                    } 
                    else if(checked)
                    {
                        jQuery(this).removeAttr("disabled");
                        jQuery(this).removeAttr("readonly");
                        if(jQuery(this).attr("class") != "masterRequireDocument")
                        jQuery(this).attr("checked",true);
                    }    
                });
            }    
        });
        if(item == "masterCategory" && jQuery("#EnableExpenseReportingTechPay").is(":checked"))
        {    
            if(itemChecked == 0)
            {
                jQuery("#masterReimbursableAll").attr("disabled","disabled");
                jQuery("#masterRequireDocumentAll").attr("disabled","disabled");
            } 
            else
            {
                jQuery("#masterReimbursableAll").removeAttr("disabled");
                jQuery("#masterRequireDocumentAll").removeAttr("disabled");
            }
            Me.configCheckMaster("masterReimbursable");
            Me.configCheckMaster("masterRequireDocument");
        }
        if(itemChecked + itemDisabled == jQuery("."+item).length && itemChecked > 0)
        {
            jQuery("#"+item+"All").attr("checked",true);
        } 
        else
        {
            jQuery("#"+item+"All").attr("checked",false);
        }    
    }
    //376
    this.EnableExpenseReportingTechPay = function()
    {
        var hname = jQuery("#hname").val();
        var hphone = jQuery("#hphone").val();
        var hemail = jQuery("#hemail").val();
        var data = {
            success:0,
            errors:['<div style="color:black;font-weight:normal;">For more information about Expense Reporting, contact your Account Manager <strong>'+hname+'</strong> at <strong>'+hphone+'</strong> or <strong>'+hemail+'</strong>.</div>']};
        var IsPM = jQuery("#EnableExpenseReportingTechPay").attr("ispm");
        
        Me.EnableExpenseReportingTechPayfirsttime();
        if(IsPM!=1)
        {
            if(jQuery("#EnableExpenseReportingTechPay").is(":checked"))
                Me.showMessages(data);
        }
    }
    
    this.EnableExpenseReportingTechPayfirsttime = function()
    {
        var checked = jQuery("#EnableExpenseReportingTechPay").is(":checked");
        jQuery("#WorkOrderSettingsTechPay input").each(function() {
                var id = jQuery(this).attr("id");
                if(id != "EnableExpenseReportingTechPay" 
                    && id != "WorkOrderSettingsTechPayCancel"
                    && id != "WorkOrderSettingsTechPaySubmit"
                    && id != 'action')
                    {    
                    if(checked)
                    {
                        if(jQuery(this).attr('type') == 'checkbox')
                            jQuery(this).removeAttr("disabled");
                        else
                           jQuery(this).removeAttr("readonly"); 
                    } 
                    else
                        if(jQuery(this).attr('type') == 'checkbox')
                            jQuery(this).attr("disabled","disabled");
                        else
                            jQuery(this).attr("readonly","true");
                }
            });
            if(checked)
            {
                jQuery("#cmdAddanotherCustomLabel")
                .unbind("click",Me.cmdAddanotherCustomLabel)
                .bind("click",Me.cmdAddanotherCustomLabel);
                Me.configCheckMaster("masterCategory",true);
            }
            else
                jQuery("#cmdAddanotherCustomLabel").unbind("click");
    }
    //end 376
    this.cmdAddanotherCustomLabel = function()
    {
        var html = '<tr class="trPeyTech">\
                            <td class="label_field">\
                                <input type="checkbox" value="1"  class="masterCategory" />&nbsp;<input type="text" style="background-image:url(\'/widgets/css/images/CustomCategory.png\') "  name="WorkOrderSettingsTechPay[bcatname][]" readonly/>\
                                <input type="hidden" checked="true" value="0" name="WorkOrderSettingsTechPay[ischecked][]" />\
                                <input type="hidden" value="0" name="WorkOrderSettingsTechPay[bcatid][]" />\
                                <input type="hidden" value="custom" name="WorkOrderSettingsTechPay[bcattype][]" />\
                            </td>\
                            <td>\
                                <input name="WorkOrderSettingsTechPay[bcatdesc][]" type="text" maxlength="20" readonly/>\
                            </td>\
                            <td >\
                                <input type="checkbox" value="1"  class="masterReimbursable" disabled/>\
                                <input type="hidden" checked="true" value="0" name="WorkOrderSettingsTechPay[reimbursable_thru_fs][]"/>\
                            </td>\
                            <td>\
                                <input type="checkbox" value="1"   class="masterRequireDocument" disabled/>\
                                <input type="hidden" checked="true" value="0" name="WorkOrderSettingsTechPay[require_doc_upl][]"  />\
                            </td>\
                        </tr>';
        jQuery("#WorkOrderSettingsTechPay .tableContentWDSGUI tbody")
        .append(html);
        Me.EventCheckBoxList("masterCategoryAll","masterCategory");
        Me.EventCheckBoxList("masterReimbursableAll","masterReimbursable");
        Me.EventCheckBoxList("masterRequireDocumentAll","masterRequireDocument");    
        jQuery("input:text[name='WorkOrderSettingsTechPay[bcatname][]']")
    .unbind("keyup",Me.onchangeWorkOrderSettingsTechPay)
    .bind("keyup",Me.onchangeWorkOrderSettingsTechPay);
    }
    this.onchangeWorkOrderSettingsTechPay = function()
{
    var val = jQuery(this).val();
    if(val == "")
    {
        jQuery(this).css("background-image","url('/widgets/css/images/CustomCategory.png'");
    }
    else
    {
        jQuery(this).css("background-image","");
    }    
}
    //------------------------------------------------------------
    this.configtab = function(tabid,buildtab)
    {
        if(typeof(tabid) == 'undefined' || jQuery.trim(tabid) == "")
        {
            tabid = 'detailsContainer';
        }
        switch (tabid)
        {
            case 'clientMySettings':
                Me.getdataMySettings();
                break;
            case 'detailsContainer':
                Me.getdataProfile('view');
                break;
            case 'clientAccessSettings':
                Me.getdataAccess('view');
                break;
            case 'clientProfileActivityLog':
                Me.getdataActivityLog();
                break; 
            case 'clientQuickTickets':
                Me.getdataQuickTickets();
                break; 
            case 'clientUsers':
                Me.getdataUsers();
                break; 
            case 'clientWorkOrderSettings':
                Me.getWorkOrderSettings();
                break;     
            default:
                Me.getdataProfile('view');
                break;
        }
        if(buildtab)
        {
            jQuery('#tabbedMenuNew li').removeClass('currenttab'); 
            jQuery('#tabbedMenuNew li a').removeClass('current'); 
            jQuery('#tabbedMenuNew li[tabid="'+tabid+'"]').addClass('currenttab');
            jQuery('#tabbedMenuNew li[tabid="'+tabid+'"] a').addClass('current');
        }
        jQuery('#'+tabid).show();
    }
    
    //------------------------------------------------------------
    this.cmdSeeSample = function()
    {
        $("<div></div>").fancybox({
            'autoDimensions' : true,
            'hideOnOverlayClick' : false,
            'content' : "<div style='width:900px;'><img style='width:900px;' src='/widgets/images/sampleQT.png' /><div>"
    }).trigger('click');
    }
    //------------------------------------------------------------
	
	this.toggleTab = function(tab) {
		if (tab == undefined) {
			if (Me._clickedTab == undefined) return;
			tab = Me._clickedTab;
		}
		jQuery("#tabbedMenuNew").find('li').attr('class','');
		jQuery("#tabbedMenuNew").find('li').children('a').attr('class','');
		jQuery(tab).attr("class","currenttab");
		jQuery(tab).children('a').attr("class","current");
		var tabid = jQuery(tab).attr("tabid");
		jQuery("#clientProfile").find(".contentBodyTab").css("display","none");
		jQuery("#"+tabid).css("display","");
		//show tab profile
		Me.configtab(tabid,false);		
	}
	
    this.init = function() {

        jQuery("#tabbedMenuNew li").click(function() {
			var currTab = $(".currenttab").attr('tabid');												   
			if (currTab == 'clientMySettings' || currTab == 'clientWorkOrderSettings') {
				defaultVal = currTab == 'clientMySettings' ? $("#MySettingsForm").data("_default") : $("#clientWorkOrderSettings").data("_default");
				Me.onSubmitPrepare();			
				currentVal = currTab == 'clientMySettings' ? $("#MySettingsForm").serialize() : $("#clientWorkOrderSettings form").serialize();
				if (defaultVal != currentVal) {
					Me._clickedTab = this;
					var messageHtml = "<div>Please confirm that you want to leave this tab. Any changes you have made will not be saved.</div><div style='text-align:center; margin-top: 5px'><input type='button' class='link_button middle2_button' onclick='$.fancybox.close();profile.Instances._profile.toggleTab()' value='Leave Tab' />&nbsp;<input type='button' class='link_button middle2_button' onclick='$.fancybox.close();updateSettingsOption();' value='Save Changes' /></div>";
					$("<div></div>").fancybox({
						'autoDimensions' : false,
						'width' : 300,
						'height' : 70,
						'transitionIn': 'none',
						'transitionOut' : 'none',
						'content' : messageHtml
					}).trigger('click');				
					return;
				}
			}
			Me.toggleTab(this);
        }); 
        
        Me.configtab(Me.tab,true);
    }

}