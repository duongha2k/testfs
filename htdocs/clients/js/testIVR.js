function clearSharedProperties() {
	$("#EditRecordTechMarkedComplete").attr("checked", false);
	$("#EditRecordInvoiced").attr("checked", false);
	$("#EditRecordDeactivated").attr("checked", false);
}

function setAcceptance() {
	clearSharedProperties();
	$("#EditRecordDate_Assigned").attr("value", yesterdayDate);
	$("#EditRecordStartDate").attr("value", more24HrsDate);
	$("#EditRecordStartTime").attr("value", more24HrsTime);
}

function set24Hr() {
	clearSharedProperties();	
	$("#EditRecordStartDate").attr("value", tomorrow24HrsDate);
	$("#EditRecordStartTime").attr("value", tomorrow24HrsTime);
}

function set1Hr() {
	clearSharedProperties();	
	$("#EditRecordStartDate").attr("value", oneHourDate);
	$("#EditRecordStartTime").attr("value", oneHourTime);
}

function setNotComplete() {
	clearSharedProperties();
	$("#EditRecordEndDate").attr("value", yesterdayDate);
}

function setIncomplete() {
	clearSharedProperties();
	$("#EditRecordDateIncomplete").attr("value", yesterdayDate);
}

function getDateTime() {
   $.get("ajax/testIVRDateTime.php", {"WOUNID" : WOUNID}, 
      function(data) {
 var info = data.split(",");
        now = info[0];
        yesterdayDate = info[1];
        more24HrsDate = info[2];
        more24HrsTime = info[3];
        tomorrow24HrsDate = info[4];
        tomorrow24HrsTime = info[5];
        oneHourDate = info[6];
        oneHourTime = info[7];
        $("#localTime").html("LOCAL TIME: " + now + "<br/><br/>");
      }); 
}

$(document).ready(function () {
	if ($("#EditRecordTech_ID").attr("value") == undefined)
		alert("Tech ID is empty. Fill in the Tech ID and click update first.");
   $("#acceptanceBtn").click(setAcceptance);
   $("#_24HrBtn").click(set24Hr);
   $("#_1HrBtn").click(set1Hr);
   $("#notCompleteBtn").click(setNotComplete);
   $("#inCompleteBtn").click(setIncomplete);
   getDateTime();
});
