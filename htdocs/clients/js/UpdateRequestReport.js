//-------------------------------------------------------------
//-------------------------------------------------------------
UpdateRequestReport.Instances = null;
//-------------------------------------------------------------
UpdateRequestReport.CreateObject = function(config) {
    var obj = null;
    if (UpdateRequestReport.Instances != null) {
        obj = UpdateRequestReport.Instances[config.id];
    }
    if (obj == null) {
        if (UpdateRequestReport.Instances == null) {
            UpdateRequestReport.Instances = new Object();
        }
        obj = new UpdateRequestReport(config);
        UpdateRequestReport.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function UpdateRequestReport(config) {
    var Me = this;
    this.id = config.id;   
    this.ContainerID=config.ContainerID;
    this.CompanyID=config.CompanyID;
    this.parames = config.parames;
    //------------------------------------------------------------
    this.LoadingAjax = function(ContentId){
        jQuery(ContentId).html("<center><img src='/widgets/images/wait.gif' border='0' /></center>");
    }
    //---------------------------------------------------------------------
    this.classDownload_onClick=function(){
        alert("vao");
    }
    //---------------------------------------------------------------------
    this.getdataCreateUpdateRequestReport = function()
    {
        Me.LoadingAjax(Me.ContainerID);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/reports/update-request-report",
            data: "parames="+Me.parames,
            success:function( html ) {
                jQuery(Me.ContainerID).html(html);    
                jQuery(".classDownload")
                    .unbind("click",Me.classDownload_onClick)
                    .bind("click",Me.classDownload_onClick);
            }
        });
    }
    //------------------------------------------------------------
    this.init = function() {
         if(Me.parames != "")
        {
           // commenting out the line below because it was causing the report to not be displayed
           // not sure what the purpose of eval'ing the php request is
        	 
           // Me.parames = eval("("+Me.parames+")");
        }   
        jQuery(document).ready(function(){            
           Me.getdataCreateUpdateRequestReport();
        })
        
        
    }

}