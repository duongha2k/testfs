function ajaxFunction() {
	var runonce = "done";
	// lookup client information
	if(formType == "insert"){
		var clientID = document.forms[0].InsertRecordClient_ID.value;

		$.get("ajax/clientLookup.php", {"clientID" : clientID}, 
		function(data) {
			var getClientInfo = data.split(",");
			document.forms[0].InsertRecordClient_Company_Name.value = getClientInfo[0];
			document.forms[0].InsertRecordClient_Name.value = getClientInfo[1];
			document.forms[0].InsertRecordClient_Email.value = getClientInfo[2];
		}
	);
	}else{
		var clientID = document.forms[0].EditRecordClient_ID.value;

		$.get("ajax/clientLookup.php", {"clientID" : clientID}, 
		function(data) {
			var getClientInfo = data.split(",");
			document.forms[0].EditRecordClient_Company_Name.value = getClientInfo[0];
			document.forms[0].EditRecordClient_Name.value = getClientInfo[1];
			document.forms[0].EditRecordClient_Email.value = getClientInfo[2];
		}
	);		
	}
}


$("#InsertRecordClient_ID").change(ajaxFunction)
$("#EditRecordClient_ID").change(ajaxFunction)
