	var hideActionClientUserLookup = null;
	var hideDelayClientUserLookup = 500;
	var clientUserLookupBox = "#clientUserLookupBox";
	
	function showDetailsClientUserLookup(data, textStatus) {
		if (data != "") {
			parts = data.split(",");
			email = parts[0];
			phone = parts[1];
			html = "Email: " + email + "<br/>Phone: " + phone;
		}
		else {
			html = "No profile found";
		}
		$(clientUserLookupBox).html(html);
	}
	
	function showDetailsBoxClientUserLookup() {
		resetHideDetailsBoxClientUserLookup();
		$.get("ajax/clientUserLookup.php", { username: $(this).text() }, showDetailsClientUserLookup);
		var pos = $(this).offset();
		var height = $(clientUserLookupBox).height();
		$(clientUserLookupBox).css({top: pos.top - (height / 2), left: pos.left + 30, display: "none"});
		$(clientUserLookupBox).html("<img src='/images/loading.gif' />");
		$(clientUserLookupBox).fadeIn("slow");
	}
	
	function delayedHideDetailsBoxClientUserLookup() {
		hideActionClientUserLookup = setTimeout("hideDetailsBoxClientUserLookup();", hideDetailsBoxClientUserLookup);
	}
	
	function hideDetailsBoxClientUserLookup() {
		$(clientUserLookupBox).fadeOut("slow");
	}
	
	function resetHideDetailsBoxClientUserLookup() {
		clearTimeout(hideActionClientUserLookup);
	}
	
	$(document).ready(
		function () {
			$('body').append("<div id='clientUserLookupBox'></div>");
			$('.clientUserLookup').hover(showDetailsBoxClientUserLookup, delayedHideDetailsBoxClientUserLookup);
			$(clientUserLookupBox).hover(resetHideDetailsBoxClientUserLookup, delayedHideDetailsBoxClientUserLookup);
		}
	);