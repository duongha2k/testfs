//-------------------------------------------------------------
//-------------------------------------------------------------
dashboardwo.Instances = null;
//-------------------------------------------------------------
dashboardwo.CreateObject = function(config) {
    var obj = null;
    if (dashboardwo.Instances != null) {
        obj = dashboardwo.Instances[config.id];
    }
    if (obj == null) {
        if (dashboardwo.Instances == null) {
            dashboardwo.Instances = new Object();
        }
        obj = new dashboardwo(config);
        dashboardwo.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
} 
//-------------------------------------------------------------
function dashboardwo(config) {
    var Me = this;
    this.id = config.id;
    this.detailsWidget = config.detailsWidget;
    this._roll = new FSPopupRoll();
    this.htmlReason = "";
    this.fb = true;
    //------------------------------------------------------------
    this.bindEvent = function(){
        jQuery(".classDeactivateAll").unbind("click",Me.classDeactivateAll)
        .bind("click",Me.classDeactivateAll);
        jQuery("#cmdResetDeactivate").unbind("click",Me.cmdResetDeactivate)
        .bind("click",Me.cmdResetDeactivate);
        jQuery(".classDeactivate").unbind("click",Me.classDeactivate)
        .bind("click",Me.classDeactivate); 
        jQuery("#MassAssignBtn").unbind("click",Me.MassAssignBtn)
        .bind("click",Me.MassAssignBtn);
        _global.eventOpenProfileTech();
    }
    //------------------------------------------------------------
    this.classDeactivateAll = function(){
        if(jQuery(this).is(":checked")){
            jQuery(".classDeactivate").attr("checked",true);
        }else{
            jQuery(".classDeactivate").removeAttr("checked");
        }
        Me.configWODeactivaAll(jQuery(this).is(":checked"));
    }
    //------------------------------------------------------------
    this.classDeactivate = function(){
        var lengthClassCheck = jQuery(".classDeactivate").length;
        var lengthChecked = 0;
        jQuery(".classDeactivate").each(function(){
            if(jQuery(this).is(":checked")){
                lengthChecked++;
            } 
        });
        if(lengthChecked == lengthClassCheck){
            jQuery(".classDeactivateAll").attr("checked",true);
        }else{
            jQuery(".classDeactivateAll").removeAttr("checked");
        }
        Me.configWODeactivaAll(jQuery(".classDeactivateAll").is(":checked"));
    }
    //------------------------------------------------------------
    this.configWODeactivaAll = function(checked){
        Me.detailsWidget.deactivaAll = checked?1:0;
    }
    //------------------------------------------------------------
    this.MassAssignBtn = function(){
        var info = assignTool.getMassAssignInfo();
        var techs = info['techs'];
        var countTech = 0;
        jQuery.each(techs,function(index,value){
            countTech++;
        });
        
        var countDeacviva = 0;
        jQuery(".classDeactivate").each(function(){
            if(jQuery(this).is(":checked")){
                countDeacviva++;
            } 
        });
        var html = "";
        html = "Assign Work Orders with Tech ID #s<br>";
        html += "<input type='button' id='cmdMassAssign' value='Mass Assign'class='link_button_sm' />";

        html += "<br><br>";
    
        if(Me.fb){
            html += "Select Work Orders to Deactivate<br>";
            html += "<input type='button' id='cmdMassDeactivatefb' value='Mass Deactivate'class='link_button_sm' />";
        }else{
            html += "Deactivate Selected Work Orders<br>";
            html += "<input type='button' id='cmdMassDeactivate' value='Mass Deactivate'class='link_button_sm' />";
        }
   
        if(countDeacviva <=0 && countTech == 1){
            Me.MassAssign();
            return false;
        };
        
        var opt = {
            width       : 210,
            height      : '',
            //position    : 'middle',
            title       : 'Mass Work Order Update',
            body        : html
        }; 
        Me._roll.showNotAjax(this, this, opt);
        jQuery("#cmdMassAssign").unbind("click",Me.cmdMassAssign)
        .bind("click",Me.cmdMassAssign);
        jQuery("#cmdMassDeactivate").unbind("click",Me.cmdMassDeactivate)
        .bind("click",Me.cmdMassDeactivate);
        jQuery("#cmdMassDeactivatefb").unbind("click",Me.cmdMassDeactivatefb)
        .bind("click",Me.cmdMassDeactivatefb);
    }
    //-----------------------------------------------------------
    this.cmdMassDeactivatefb = function(){
        Me.fb = false;
        jQuery("#titleAssignWO").hide();
        jQuery("#titleDeactivateWO").show();
        jQuery(".DeactivateWO").show();
        Me._roll.hide();
    }
    //------------------------------------------------------------
    this.cmdResetDeactivate = function(){
        Me.fb = true;
        jQuery("#titleAssignWO").show();
        jQuery("#titleDeactivateWO").hide();
        jQuery(".DeactivateWO").hide();
    }
    //------------------------------------------------------------
    this.cmdMassAssign = function(){
        Me._roll.hide();
        jQuery("#cmdMassAssignBtn").click();
        return false;
        /*
        var info = assignTool.getMassAssignInfo();
        var techs = info['techs'];
        var countTech = 0;
        jQuery.each(techs,function(index,value){
            countTech++;
        });
        var html = "";
        var textWo = "";
        if(countTech <= 0){
            Me._roll.hide();
            jQuery("#cmdMassAssignBtn").click();
            return false;
        }
        if(countTech == 1){
            textWo = "Are you sure you wish to Assign this Work Order?";
        }
        else{
            textWo = "Are you sure you wish to Assign these "+countTech+" Work Orders?";
        }
        html += textWo+"<br><br>";    
        html += "<div style='text-align:center;'><input type='button' id='cmdCancelSubmit' value='Cancel'class='link_button_sm' />&nbsp;&nbsp;<input type='button' id='cmdMassAssignSubmit' value='Assign'class='link_button_sm' /></div>";
        
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :true,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'content': html
        }
        ).trigger('click');
            
        jQuery("#cmdMassAssignSubmit").unbind("click",Me.MassAssign)
        .bind("click",Me.MassAssign);
        jQuery("#cmdCancelSubmit").unbind("click",Me.cmdCancelSubmit)
        .bind("click",Me.cmdCancelSubmit);*/
    }
    //------------------------------------------------------------
    this.cmdMassDeactivate = function(){
        var countDeacviva = 0;
        var event = this;
        jQuery(".classDeactivate").each(function(){
            if(jQuery(this).is(":checked")){
                countDeacviva++;
            } 
        });
        var html = "";
        var textWo = "";
        var width = 320;
        if(countDeacviva <= 0) return false;
        if(countDeacviva == 1){
            textWo = "Are you sure you wish to Deactivate this Work Order?";
            width = 320;
        }
        else{
            textWo = "Are you sure you wish to Deactivate these "+countDeacviva+" Work Orders?";
            if(countDeacviva < 10){
                width = 350;
            }else{
                width = 355;
            }
        }
        Me.getReason(function(cbxhtml){
            html += "<div>"+textWo+"</div>";
            html += "<div id='divError' style='color:red;display:none;'></div>"; 
            html += "<div style='padding-top:10px;'><span style='color:red;'>*</span> Reason: "+cbxhtml+"</div>";     
            html += "<div style='text-align:center;padding-top:10px;'><input type='button' id='cmdCancelSubmit' value='Cancel'class='link_button_sm' />&nbsp;&nbsp;<input type='button' id='cmdMassDeactivateSubmit' value='Deactivate'class='link_button_sm' /></div>";
            $("<div></div>").fancybox(
            {
                'autoDimensions' : true,
                'showCloseButton' :true,
                'hideOnOverlayClick' : false,
                'enableEscapeButton':false,
                'height':82,
                'width':width,
                'content': html
            }
            ).trigger('click');
            jQuery("#cmdMassDeactivateSubmit").unbind("click",Me.MassDeactivate)
            .bind("click",Me.MassDeactivate);
            jQuery("#cmdCancelSubmit").unbind("click",Me.cmdCancelSubmit)
            .bind("click",Me.cmdCancelSubmit);
        },event);
        
    }
    //------------------------------------------------------------
    this.MassDeactivate = function(){
        var deactivationCode = jQuery("#DeactivationCode option:selected").val();
        if(deactivationCode == ""){
            jQuery("#divError").html("Please choose a Reason.");
            jQuery("#divError").show();
            return false;
        }
        var wins = "";
        jQuery(".classDeactivate").each(function(){
            if(jQuery(this).is(":checked")){
                wins += jQuery(this).val()+",";
            } 
        });
        if(wins != ""){
            wins = wins.substring(0,wins.length-1);
        }
        console.log("HERE");
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/index/deactivation-wos",
            data: "wins="+wins+"&deactivationCode="+deactivationCode+"&company="+window._company+"&massDeactivate=true",
            success:function( data ) {
                if(data.success){
                    Me.cmdCancelSubmit();
                    Me._roll.hide();
                    Me.detailsWidget.show({
                        tab:Me.detailsWidget.currentTab,
                        params:Me.detailsWidget.getParams()
                    });
                    Me.fb = true;
                }
            },
            error:function(error) {
                alert('error; ' + eval(error));
            }
        });
    }
    //------------------------------------------------------------
    this.cmdCancelSubmit = function(){
        $.fancybox.close();
    }
    //------------------------------------------------------------
    this.MassAssign = function(){
        Me.cmdCancelSubmit();
        Me._roll.hide();
        jQuery("#cmdMassAssignBtn").click();
    }
    //------------------------------------------------------------
    this.getReason = function(onsuccess){
        if(Me.htmlReason != ""){
            onsuccess(Me.htmlReason);
        }else{
            $.ajax({
                type: "POST",
                url: "/widgets/dashboard/index/get-reason",
                data: "",
                success:function( html ) {
                    Me.htmlReason = html;
                    onsuccess(html);
                },
                error:function(error) {
                    alert('error; ' + eval(error));
                }
            });
        }
    }
    //------------------------------------------------------------
    this.configFilter = function(){
        if(Me.detailsWidget.currentTab == "todayswork"){
            jQuery("#filter_date").hide();
        }else{
            jQuery("#filter_date").show();
        }
    }
    this.loadQABox = function(WIN_NUM){
        var rel = '/widgets/dashboard/popup/clientquestionandanswer?WIN_NUM=' + WIN_NUM;
        

        $.ajax({
            type: "POST",
            url: rel,
            data: "",
            context     : this,
            success:function( html ) {
                jQuery("#comtantPopupID").html(html);
                jQuery("#cmdPostQuestion")
                .unbind("click",Me.PostQuestion)
                .bind("click",Me.PostQuestion); 
                jQuery(".techprofileforquestion")
                .unbind("click",Me.loadprofileQuestion)
                .bind("click",Me.loadprofileQuestion);
                jQuery(".bidinfoQuestion")
                .unbind("click",Me.loadbidinfoQuestion)
                .bind("click",Me.loadbidinfoQuestion);
                $("#space"+WIN_NUM).css("display","");
                $("#mark"+WIN_NUM).css("display","none");
                
            },
            error:function(error) {
                alert('error; ' + eval(error));
            }
        });
    }
    //------------------------------------------------------------
    this.loadQABoxFacibox = function(WIN_NUM){
        var content = "<div id='comtantPopupID'><div style='text-align:center;height:380px;width:900px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :true,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'content': content
        }
        ).trigger('click');
        Me.loadQABox(WIN_NUM);
    }
    //------------------------------------------------------------
    
      this.loadprofileQuestion = function(){
        var techid = jQuery(this).attr("techid");
        var comid = jQuery(this).attr("comid");
        
        $("<div></div>").fancybox(
        {
            'autoDimensions' : false,
            'showCloseButton' :true,
            'width':1010,
            'height':800,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'type':"iframe",
            'href':'/clients/wosViewTechProfile.php?simple=1&back=0&popup=1&TechID='+techid+'&v='+comid+'&backNotices=1&backclick='+Base64.encode('window.parent.wd._dashboardwo.loadQABoxFacibox('+Me.WIN_NUM+');')
        }
        ).trigger('click');
    }
    
     this.loadbidinfoQuestion = function(){
        var comid = jQuery(this).attr("comid");
        
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :true,
            'width':1010,
            'height':800,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'type':"iframe",
            'href':'/clients/wosApplicant.php?simple=1&v='+comid+'&sb=0&WorkOrderID='+Me.WIN_NUM+'&backNotices=1&backclick='+Base64.encode('window.parent.wd._dashboardwo.loadQABoxFacibox('+Me.WIN_NUM+');')
        }
        ).trigger('click');
    }
    //------------------------------------------------------------
    this.WIN_NUM = 0;
    this.QABox = function () {
       Me.WIN_NUM =  $(this).attr('WIN_NUM');
       Me.loadQABoxFacibox(Me.WIN_NUM);
    }
    
    //------------------------------------------------------------
    this.PostQuestion = function () {
        var data = jQuery("#frmPostQuestion").serialize();
        
        if($("#frmPostQuestion").find("#question").val()!="")
        {
        if(data != ""){
                data += "&techName="+jQuery("#frmPostQuestion").find("#techlist option:selected").attr('techname');
        }
        
        $.ajax({
            type: "POST",
            url: '/widgets/dashboard/popup/clientpostquestion',
            data: data,
            context : this,
            success:function( html ) {
                jQuery("#comtantPopupID").html("<div style='width:200px;'>"+html+"</div>");
                $('#fancybox-content').width(400);
                $('#fancybox-wrap').width(420);
                if(typeof(window.parent) != 'undefined'){
                    window.parent.$.fancybox.resize();
                }else{
                    $.fancybox.resize();
                }
                
            },
            error:function(error) {
                alert('error; ' + eval(error));
            }
        });
    }
    }
    //------------------------------------------------------------
    this.buildEventUpdatePaperwork_click = function(){

        var data = jQuery("#frmUpdatePaperwork").serialize() + "&por=yes";
        _global.url = "/widgets/dashboard/sections/tech-documents/";
        _global.data = data;
        _global.onSuccess = function(data){
            roll.hide();
        };
        _global.show(this);

    }
    this.buildEventUpdatePaperwork = function(){
        jQuery("#frmUpdatePaperwork #cmdSavePaperwork").unbind("click",Me.buildEventUpdatePaperwork_click)
        .bind("click",Me.buildEventUpdatePaperwork_click);
    }
    //------------------------------------------------------------
    this.init = function() {
        Me.bindEvent();
        Me.detailsWidget._dashboardwo = Me;
        //620
        jQuery(".ClientQABtn")
        .unbind("click", Me.QABox)
        .bind("click", Me.QABox);
        //end 620
        // config tab
        if(!Me.fb){
            Me.cmdMassDeactivatefb();
        }
        Me.configFilter();
        Me._wintags = wintags.CreateObject({id:"_wintags"});
    }

}