//-------------------------------------------------------------
//-------------------------------------------------------------
CreateUpdateRequestReport.Instances = null;
//-------------------------------------------------------------
CreateUpdateRequestReport.CreateObject = function(config) {
    var obj = null;
    if (CreateUpdateRequestReport.Instances != null) {
        obj = CreateUpdateRequestReport.Instances[config.id];
    }
    if (obj == null) {
        if (CreateUpdateRequestReport.Instances == null) {
            CreateUpdateRequestReport.Instances = new Object();
        }
        obj = new CreateUpdateRequestReport(config);
        CreateUpdateRequestReport.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function CreateUpdateRequestReport(config) {
    var Me = this;
    this.id = config.id;   
    this.ContainerID=config.ContainerID;
    this.CompanyID=config.CompanyID;
    this.Params=config.Params;
    //------------------------------------------------------------
    this.LoadingAjax = function(ContentId){
        jQuery(ContentId).html("<center><img src='/widgets/images/wait.gif' border='0' /></center>");
    }
    //---------------------------------------------------------------------
    this.btnRunReport_onClick=function(){       
        var listProjectIDSelected="";
        jQuery("#ListProject .classProjectItems:checked").each(function(){
            listProjectIDSelected+=jQuery(this).attr("id")+",";
        })
        listProjectIDSelected=listProjectIDSelected.replace(/chk/g,"");
        if(listProjectIDSelected.lenght>0)
            listProjectIDSelected=listProjectIDSelected.substr(0,listProjectIDSelected.length-1);
                
        Me.LoadingAjax(Me.ContainerID);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/reports/expense-reimbursement-report",
            data: {
                v:Me.CompanyID,
                ProjectIDs:'"'+listProjectIDSelected+'"'
            },
            success:function( html ) {
                jQuery(Me.ContainerID).html(html);                 
            }
        });
        
    }
    //---------------------------------------------------------------------
    this.BindEvent=function(){
        jQuery(".MasterItemsProject")
        .unbind("click", Me.MasterItemsProject_onClick)
        .bind("click", Me.MasterItemsProject_onClick);
        jQuery(".classProjectItems")
        .unbind("click", Me.classProjectItems_onClick)
        .bind("click", Me.classProjectItems_onClick);
    /*
        jQuery("#btnRunReport")
            .unbind("click", Me.btnRunReport_onClick)
            .bind("click", Me.btnRunReport_onClick);*/
        
    }
    //---------------------------------------------------------------------
    this.getdataCreateUpdateRequestReport = function()
    {
        Me.LoadingAjax(Me.ContainerID);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/reports/create-update-request-report",
            data: {
                v:Me.CompanyID,
                Params:Me.Params
            },
            success:function( html ) {
                jQuery(Me.ContainerID).html(html);  
                $('#StartDateStartDateRange').calendar({
                    dateFormat: 'MDY/'
                });
                $('#EndDateStartDateRange').calendar({
                    dateFormat: 'MDY/'
                });
                $("#cbxStartDateRange").change(function(){ 
                    filterDate("#cbxStartDateRange",'#StartDateStartDateRange','#EndDateStartDateRange');
                })
                
                $('#StartDateRequestDateRange').calendar({
                    dateFormat: 'MDY/'
                });
                $('#EndDateRequestDateRange').calendar({
                    dateFormat: 'MDY/'
                });
                $("#cbxRequestDateRange").change(function(){ 
                    filterDate("#cbxRequestDateRange",'#StartDateRequestDateRange','#EndDateRequestDateRange');
                })

                jQuery("#StartDateStartDateRange").watermark("MM/DD/YYYY");
                jQuery("#EndDateStartDateRange").watermark("MM/DD/YYYY");
                jQuery("#StartDateRequestDateRange").watermark("MM/DD/YYYY");
                jQuery("#EndDateRequestDateRange").watermark("MM/DD/YYYY");
                
                Me.InitPage();
                Me.BindEvent();
            }
        });
    }
    //------------------------------------------------------------
    this.MasterItemsProject_onClick=function(){
        var isCheck=jQuery(this).is(":checked");        
        jQuery("#ListProject .classProjectItems").each(function(){
            jQuery(this).attr("checked",isCheck);
        })
    }
    //------------------------------------------------------------
    this.classProjectItems_onClick=function(){
        var numItems= jQuery("#ListProject .classProjectItems").length;
        var numItemsChecked= jQuery("#ListProject .classProjectItems:checked").length;
        if(numItemsChecked==numItems)
            jQuery(".MasterItemsProject").attr("checked",true);
        else
            jQuery(".MasterItemsProject").attr("checked",false);
        
            
        
    }
    //------------------------------------------------------------
    this.InitPage=function(){
        if(Me.Params!="")
        {          
            var objParams= eval("("+ Base64.decode(decodeURIComponent(Me.Params))+")");   
                   
            jQuery("#cbxRequestDateRange"+" option[value=\""+objParams.cbxRequestDateRange+"\"]").attr("selected","selected");
            jQuery("#StartDateRequestDateRange").val(objParams.StartDateRequestDateRange);
            jQuery("#EndDateRequestDateRange").val(objParams.EndDateRequestDateRange);
        
            jQuery("#cbxStartDateRange"+" option[value=\""+objParams.cbxStartDateRange+"\"]").attr("selected","selected");
            jQuery("#StartDateStartDateRange").val(objParams.StartDateStartDateRange);
            jQuery("#EndDateStartDateRange").val(objParams.EndDateStartDateRange);
                
                       
            if(typeof(objParams.chkProjectList)!="undefined")
            {
                var  arrProjectIDs=eval(objParams.chkProjectList);
                var lenghtArr=arrProjectIDs.length;
                var i=0;
                for( i=0;i<lenghtArr;i++)
                {                
                    jQuery("#ListProject .classProjectItems[value=\""+arrProjectIDs[i]+"\"]").each(function(){
                        jQuery(this).attr("checked",true);
                    })
                }
            }
            else                
            {
                jQuery("#ListProject .classProjectItems").each(function(){
                    jQuery(this).attr("checked",false);
                })
            }
            var numCheckBoxProjectID= jQuery("#ListProject .classProjectItems").length;
            var numCheckBoxProjectIDChecked=jQuery("#ListProject .classProjectItems:checked").length;
            
            if(numCheckBoxProjectID==numCheckBoxProjectIDChecked)
            {
                jQuery("#chkAllProject").attr("checked",true);
            }
            else
            {
                jQuery("#chkAllProject").removeAttr("checked");
            }
           
        }
        else
        {
            var isCheck=jQuery(".MasterItemsProject").is(":checked");
            jQuery("#ListProject .classProjectItems").each(function(){
                jQuery(this).attr("checked",isCheck);
            })
        }
        
    }
    //------------------------------------------------------------
    this.init = function() {
        jQuery(document).ready(function(){  
            Me.getdataCreateUpdateRequestReport();
        })
        
        
    }

}