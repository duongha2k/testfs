/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//-------------------------------------------------------------
//-------------------------------------------------------------
var _wintagsv2 = null;
wintags.Instances = null;

//-------------------------------------------------------------
wintags.CreateObject = function(config) {
    var obj = null;
    if (wintags.Instances != null) {
        obj = wintags.Instances[config.id];
    }
    if (obj == null) {
        if (wintags.Instances == null) {
            wintags.Instances = new Object();
        }
        obj = new wintags(config);
        wintags.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}

//-------------------------------------------------------------
function wintags(config) {
    var Me = this;
    this.id = config.id;   
    this.flash = false;
    this.reload = typeof(config.reload) == 'undefined'?false:config.reload;
    this.width = 0;
    this.height = 0;
    this._roll = new FSPopupRoll();
    this._roll1 = new FSPopupRoll();
    
    //------------------------------------------------------------
    this.autoSize = function(){
        
        var height = _global.height - (90 + jQuery("#trHeaderWintags").height());
        if(jQuery("#divWintagsContent table").height() > height){
            jQuery("#divWintagsContent").height(height);
            jQuery("#divWintagsContent").children("table").height(height);
            jQuery("#divSecction1").height(height-30);
            jQuery("#divontime").css('max-height',height-195);
            Me.height = _global.height;
        }else{
            var _height = jQuery("#divWintagsContent table").height()+80 + jQuery("#trHeaderWintags").height();
            $('#fancybox-content').height(_height); 
            $('#fancybox-wrap').height(_height+20);
            Me.height = _height+20;
            jQuery("#comtantPopupID").height(_height+7);
            
            $.fancybox.resize();
            
        }
    }
    
    //------------------------------------------------------------
    this.cmdOpenWinTabs_click = function(){
        Me.flash = false;
        var win = jQuery(this).attr("win");
        _global.data = "win="+win;
        _global.url = "/widgets/dashboard/popup/win-tags";
        var _onSuccess = function(data){
            jQuery("#comtantPopupID").html(data);
            Me.autoSize();
            Me.buildEvent();
        }
        _global.onSuccess = _onSuccess;
        _global.showLoader(_global.width,_global.height,true);
        _global.show();
    }
    
    //------------------------------------------------------------
    this.cmdOpenWinTabs = function(win){
        Me.flash = false;
        _global.data = "win="+win;
        _global.url = "/widgets/dashboard/popup/win-tags";
        var _onSuccess = function(data){
            jQuery("#comtantPopupID").html(data);
            //Me.autoSize();
            Me.buildEvent();
        }
        _global.onSuccess = _onSuccess;
        _global.showLoader(_global.width,_global.height,true);
        _global.show();
    }
    this.openWintagFromSiteStatus_click = function(){
        var win ="";
        win = $(this).attr("WIN_NUM");
        Me.cmdOpenWinTabs(win);
    }
    
    //------------------------------------------------------------
    this.buildEvent = function(){
        jQuery("#frmWinTags select").unbind("change",Me.frmWinTags_change).bind("change",Me.frmWinTags_change);
        jQuery("#frmWinTags input:radio").unbind("change",Me.frmWinTags_change).bind("change",Me.frmWinTags_change);
        jQuery("#frmWinTags input:checkbox").unbind("change",Me.frmWinTags_change).bind("change",Me.frmWinTags_change);
        jQuery("#fancybox-close").unbind("click").bind("click",Me.fancybox_close);
        jQuery("#frmWinTags #cbxCompletionStatus").unbind("change",Me.cbxCompletionStatus_change).bind("change",Me.cbxCompletionStatus_change);
        jQuery("#frmWinTags #cmdSaveWinTags").unbind("click").bind("click",Me.cmdSaveWinTags);
        jQuery("#frmWinTags .cmdDeleteWinTagsCompletionStatus").unbind("click").bind("click",Me.cmdDeleteWinTagsCompletionStatus);
        jQuery("#frmWinTags #cmdCancelWinTags").unbind("click").bind("click",_global.hideLoader);
        jQuery("#frmWinTags .openTechProfile").unbind("click").bind("click",Me.openTechProfile);
        jQuery("#frmWinTags .CompletionStatuscheck").unbind("click").bind("click",Me.CompletionStatuscheck_click);
        jQuery("#frmWinTags .aCompletionStatus").unbind("click").bind("click",Me.CompletionStatuslink_click);
        jQuery("#frmWinTags .openSiteStatus").unbind("click",Me.openSiteStatus_click).bind("click",Me.openSiteStatus_click);

        //686
        jQuery("#frmWinTags #chkCompleteGroup").unbind("click").bind("click",Me.chkCompleteGroup_change);
        jQuery("#frmWinTags #chkIncompleteGroup").unbind("click").bind("click",Me.chkIncompleteGroup_change);
        jQuery("#frmWinTags #chkRescheduledGroup").unbind("click").bind("click",Me.chkRescheduledGroup_change);
        jQuery("#frmWinTags #chkCancelledGroup").unbind("click").bind("click",Me.chkCancelledGroup_change);
        
        jQuery("#frmWinTags #chk1stvisit").unbind("click").bind("click",Me.chk1stvisit_click);
        jQuery("#frmWinTags #chkReassigned").unbind("click").bind("click",Me.chkReassigned_click);
        jQuery("#frmWinTags #chkRevisitRequired").unbind("click").bind("click",Me.chkRevisitRequired_click);
        jQuery("#frmWinTags #chkRescheduled").unbind("click").bind("click",Me.chkRescheduled_click);
        
        jQuery("#frmWinTags #chkIncompleteSiteClientCaused").unbind("click").bind("click",Me.chkIncompleteSiteClientCaused_change);
        jQuery("#frmWinTags #chkIncompleteTechProviderCaused").unbind("click").bind("click",Me.chkIncompleteTechProviderCaused_change);
        jQuery("#frmWinTags #chkIncompleteNaturalCaused").unbind("click").bind("click",Me.chkIncompleteNaturalCaused_change);
        
        jQuery("#frmWinTags #chkRescheduledSiteClientCaused").unbind("click").bind("click",Me.chkRescheduledSiteClientCaused_change);
        jQuery("#frmWinTags #chkRescheduledTechProviderCaused").unbind("click").bind("click",Me.chkRescheduledTechProviderCaused_change);
        jQuery("#frmWinTags #chkRescheduledNaturalCaused").unbind("click").bind("click",Me.chkRescheduledNaturalCaused_change);
        
        jQuery("#frmWinTags #IncompleteSiteClientAdd").unbind("click").bind("click",Me.IncompleteSiteClientAdd_click);
        jQuery("#frmWinTags #Incompletetagsitemlist").unbind("change").bind("change",Me.SiteClienttagsitemlist_change);
        jQuery("#frmWinTags .IncompleteSiteClientStatus").unbind("click").bind("click",Me.IncompleteSiteClientCausedStatuslink_click);
        
        jQuery("#frmWinTags #IncompleteTechProviderAdd").unbind("click").bind("click",Me.IncompleteTechProviderAdd_click);
        jQuery("#frmWinTags #IncompleteTechProvidertagsitemlist").unbind("change").bind("change",Me.TechProviderTagsitemlist_change);
        jQuery("#frmWinTags .IncompleteTechProviderStatus").unbind("click").bind("click",Me.IncompleteTechProviderCausedStatuslink_click);
        
        jQuery("#frmWinTags #RescheduledSiteClientAdd").unbind("click").bind("click",Me.RescheduledSiteClientAdd_click);
        jQuery("#frmWinTags #RescheduledSiteClienttagsitemlist").unbind("change").bind("change",Me.RescheduledSiteClienttagsitemlist_change);
        jQuery("#frmWinTags .RescheduledSiteClientStatus").unbind("click").bind("click",Me.RescheduledSiteClientCausedStatuslink_click);
        
        jQuery("#frmWinTags #RescheduledTechProviderAdd").unbind("click").bind("click",Me.RescheduledTechProviderAdd_click);
        jQuery("#frmWinTags #RescheduledTechProvidertagsitemlist").unbind("change").bind("change",Me.RescheduledTechProviderTagsitemlist_change);
        jQuery("#frmWinTags .RescheduledTechProviderStatus").unbind("click").bind("click",Me.RescheduledTechProviderCausedStatuslink_click);
        
        jQuery("#frmWinTags .cmdDeletewintag").unbind("click").bind("click",Me.DeleteWintag_click);
        
        //end 686
        //847
        jQuery("#frmWinTags .cmdaddwintagnote").unbind("click").bind("click",Me.cmdaddwintagnote_click);
        //end 874
        var content = 'WIN-Tags&#8482; allow for Project-level reporting on On-Time Performance, Site Completion Results, Technician Performance, and more.';
        $('.cmdhelpWinTags').bt(content,
        {
            trigger:'click',
            positions:'bottom',
            fill: 'white',
            width:210,//686
            cssStyles:{
                color: 'black'
            }
        }); 
    }
    
    this.html = "";
    //------------------------------------------------------------
    this.openTechProfile = function(){
        _wintagsv2 = Me;
        jQuery("#frmWinTags").find("input:radio").each(function(){
            if(jQuery(this).is(":checked")){
                jQuery(this).attr("check_ed","true");
            } else{
                jQuery(this).attr("check_ed","false");
            }
        });
        Me.html = jQuery("#comtantPopupID").contents();
        var hrf = jQuery(this).attr("hrf");
        $("<div></div>").fancybox(
        {
            'autoDimensions' : false,
            'showCloseButton' :true,
            'width':_global.width,
            'height':_global.height,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'type':"iframe",
            'href':hrf+'&back=0&popup=1&backNotices='+Me.backNotices+"&backclick="+Base64.encode('window.parent._wintagsv2.WinTagsBack('+Me.flash+');')
        }
        ).trigger('click');
        Me.flash = false;    
    }
    
    //------------------------------------------------------------
    this.WinTagsBack = function(_flash){
        Me.flash = _flash;
        _global.showLoader(_global.width,Me.height,true);
        jQuery("#comtantPopupID").html(Me.html);
        //Me.autoSize();
        Me.buildEvent();
        jQuery("#frmWinTags").find("input:radio").each(function(){
            if(jQuery(this).attr("check_ed") == "true"){
                jQuery(this).attr("checked","checked");
            } else{
            //jQuery(this).attr("check_ed","false");
            }
        });
    }
    
    //------------------------------------------------------------
    this.cmdSaveWinTags = function(){
        _global.data = jQuery("#frmWinTags").serialize();
        _global.url = "/widgets/dashboard/popup/save-win-tags";

        _global.onSuccess = function(data){
            _global.hideLoader();
            _global.showMessages(data);
            if(Me.reload){
                window.location.reload();
            }
        };
        _global.show(this);
    }
    
    //------------------------------------------------------------
    this.dataCompletionStatus = function(id,ext,event){
        var val = jQuery(event).find("option:selected").val();
        var text = jQuery(event).find("option:selected").html();
        
        if(jQuery.trim(val) == "")
        {
            return false;
        }
        
        var displayNone = "";
        var paddingtext = "";
        
        if(val == 42)
        {
            displayNone = "display:none;"
            paddingtext = "style='padding-left:23px;'";
        }
        
        var html = '<div style="padding-top:5px;float:left;width:100%;">\n\
                        <div class="divWintagleft">\
                        <img class="cmdDeleteWinTagsCompletionStatus'+ext+'" style="vertical-align: middle;cursor: pointer;'+displayNone+'" src="/widgets/css/images/removedoc.png" >\
                        <input class="CompletionStatus" type="hidden" name="CompletionStatus[]"  value="'+val+'"/>\
                        <input class="CompletionStatusID" type="hidden" name="CompletionStatusID_'+val+'"  value="0"/>\
                            &nbsp;<span class="classText" '+paddingtext+'>'+text+'</span>\n\
                        </div>\
                        <div class="divWintagright">\n\
                            <select style="'+displayNone+'" name="cbxCompletionStatus_'+val+'">\
                            <option value="1">Not Invoiceable</option>\
                            <option value="2">Invoiceable</option>\
                            </select>\n\
                        </div>\n\
                    </div>';
        jQuery(id).append(html);
        //686
        ///jQuery(event).find("option:selected").remove();
        //end 686
        if(ext == "Main"){
            jQuery(".cmdDeleteWinTagsCompletionStatusMain").unbind("click").bind("click",Me.cmdDeleteWinTagsCompletionStatusMain);
        }else{
            jQuery(".cmdDeleteWinTagsCompletionStatus").unbind("click").bind("click",Me.cmdDeleteWinTagsCompletionStatus);
        }
        Me.configCompletionStatusSelect(event,jQuery(id));
        
        jQuery(event).find("option[value='']").attr("selected","true");
    }
    
    //------------------------------------------------------------
    this.configCompletionStatusSelect = function(cbx,div){
        if(jQuery.trim(jQuery(div).html()) ==  ""){
            jQuery(cbx).find("option[value='']").html("Select a WIN-Tag");
        }else{
            jQuery(cbx).find("option[value='']").html("Select another WIN-Tag");
        }
    }
    
    //------------------------------------------------------------
    this.cbxCompletionStatus_change = function(){
        Me.dataCompletionStatus("#divSecction1","",this);
        Me.autoSize();
    }
    
    //------------------------------------------------------------
    this.cmdDeleteWinTagsCompletionStatus = function(){
        Me.DeleteWinTagsCompletionStatus("#cbxCompletionStatus",this);
        Me.autoSize();
        Me.flash = true;
    }
    
    //------------------------------------------------------------
    this.cmdDeleteWinTagsCompletionStatusMain = function(){
        Me.DeleteWinTagsCompletionStatus("#cbxCompletionStatusMain",this);
    }
    
    //------------------------------------------------------------
    this.DeleteWinTagsCompletionStatus = function(id,ev){
        var event = jQuery(ev).parent().parent();
        var eventPareant = jQuery(event).parent();
        var val = jQuery(event).find(".CompletionStatus").val();
        var text = jQuery(event).find(".classText").html();
        jQuery("#frmWinTags "+id).append('<option sort="'+text+'" value="'+val+'">'+text+'</option>');
        jQuery(event).remove();
        jQuery("#frmWinTags "+id+">option")
        .tsort({
            order:'asc',
            attr:'sort'
        });
        Me.configCompletionStatusSelect(jQuery(id),jQuery(eventPareant));
    }
    
    //------------------------------------------------------------
    this.fancybox_close = function(){
        if(!Me.flash){
            _global.hideLoader();
            return false;
        }
        var html = '<div style=""><div>Would you like to save your changes before closing?</div><div style="text-align:center; margin-top: 5px"><input type="button" value="Close"  class="link_button middle2_button cmdCloseWinTags">&nbsp;<input type="button" value="Save Changes" class="link_button middle2_button cmdSaceChangesWintags"></div></div>';
        _global.showPopupNew({
            width       : 300,
            height      : 70,
            position    : 'middle',
            title       : '',
            zindex      : 9999,
            body        : html
        },"lightboxWinTags","",null);
        
        jQuery("#lightboxWinTags").html(html);
        jQuery(".cmdSaceChangesWintags").unbind("click");
        jQuery(".cmdSaceChangesWintags").click(function(){
            _global.ClosePopup();
            jQuery("#frmWinTags #cmdSaveWinTags").click();
        });
        jQuery(".cmdCloseWinTags").click(function(){
            _global.ClosePopup();
            _global.hideLoader();
        });
    }
    
    //------------------------------------------------------------
    this.cbxCompletionStatusMain = function(){
        Me.dataCompletionStatus("#divSecctionmainWintag","Main",this);
    }
    
    //------------------------------------------------------------
    this.frmWinTags_change = function(){
        Me.flash = true;
    }
    
    //------------------------------------------------------------
    /**
     * for Issue 13676
     * do 24/01/2013
     */
    //13871
    this.flagST = false;
    this.buildEventSiteStatus = function(){
        jQuery(".openSiteStatus").unbind("click",Me.woopenSiteStatus_click).bind("click",Me.woopenSiteStatus_click);
        jQuery(".openTechProfile").unbind("click",Me.woopentechprofile_click).bind("click",Me.woopentechprofile_click);
        //871
        jQuery(".SATRecommended").unbind("click",Me.woSATRecommended_change).bind("click",Me.woSATRecommended_change);
        jQuery(".SATPerformance").unbind("click",Me.woSATPerformance_change).bind("click",Me.woSATPerformance_change);
        //end 871
    }
    
    //------------------------------------------------------------
    this.cbxSiteStatus_change = function(){
        var val = jQuery(this).find("option:selected").val();
        jQuery("#frmSiteStatus .cancelEditItemSiteStatusV2").each(function(){
            if(jQuery(this).css("display") != "none"){
                var event = jQuery(this).parent().parent();
                if(val == 2 || val == "2"){
                    jQuery(event).find(".classInvoiceable").hide();
                    jQuery(event).find(".classInvoiceable").parent().children(".classTextShow").show();
                    jQuery(event).find(".classInvoiceable").val(jQuery(event).find(".classHiddenInvoiceable").val());
                    jQuery(event).find(".classInvoiceable").parent().children(".classTextShow").html("No");
                    jQuery(event).find(".classInvoiceable").val(1);
                }else{
                    jQuery(event).find(".classInvoiceable").show();
                    jQuery(event).find(".classTextShow").hide();
                }
            }
        })
    }
    
    //------------------------------------------------------------
    this.buildEventFormSiteStatus = function(){
        var content = 'With Site SLA Status Reporting, you can track and report on the status of a site requiring multiple work orders under the same project for completion.  Add WIN-Tags to your work orders to see a detailed Site History.';
        $('#frmSiteStatus .cmdhelpSiteStatus').bt(content,
        {
            trigger:'click',
            positions:'bottom',
            fill: 'white',
            cssStyles:{
                color:'black'
            }
        }); 
        jQuery("#frmSiteStatus .cmdEditSiteStatusCbx").unbind("click",Me.cmdEditSiteStatusCbx_click).bind("click",Me.cmdEditSiteStatusCbx_click);
        
        jQuery("#frmSiteStatus .editItemSiteStatus").unbind("click",Me.editItemSiteStatus_click).bind("click",Me.editItemSiteStatus_click);
        jQuery("#frmSiteStatus .cancelEditItemSiteStatus").unbind("click",Me.cancelEditItemSiteStatus_click).bind("click",Me.cancelEditItemSiteStatus_click);
        jQuery("#frmSiteStatus #cmdSaveSiteStatus").unbind("click",Me.cmdSaveSiteStatus_click).bind("click",Me.cmdSaveSiteStatus_click);
        jQuery("#frmSiteStatus #cmdAddSiteStatus").unbind("click",Me.cmdAddSiteStatus_click).bind("click",Me.cmdAddSiteStatus_click);
        
        jQuery("#frmSiteStatus .openWintagFromSiteStatus").unbind("click",Me.openWintagFromSiteStatus_click).bind("click",Me.openWintagFromSiteStatus_click);
        $('.calendarInput').calendar({
            dateFormat:'MDY/'
        });
        jQuery("#frmSiteStatus #cmdDownloadSiteStatus").unbind("click").bind("click",Me.cmdDownloadSiteStatus_click);
        jQuery("#cmdCancelSiteStatus").click(function(){
            Me.fancyboxSiteStatus_close();
        });
        jQuery("#fancybox-close").unbind("click").bind("click",Me.fancyboxSiteStatus_close);
        jQuery("#frmSiteStatus #cbxSiteStatus").unbind("change",Me.cbxSiteStatus_change).bind("change",Me.cbxSiteStatus_change);
    }
    
    //------------------------------------------------------------
    this.htmlDownloadStatus = "";
    this.cmdDownloadSiteStatus_click = function(){
        if(Me.htmlDownloadStatus == ""){
            Me.htmlDownloadStatus = jQuery("#htmlDownloadSiteStatus").html();
            jQuery("#htmlDownloadSiteStatus").html("");
        }
        _global.showPopupNew({
            width       : 250,
            height      : 100,
            title       : 'Download:',
            zindex      : 9999,
            body        : Me.htmlDownloadStatus
        },"lightboxDownloadSiteStatus","Download Options",this);
        
        jQuery("#lightboxDownloadSiteStatus").html(Me.htmlDownloadStatus);
        jQuery(".cmdSaceChangesWintags").unbind("click");
        jQuery(".cmdCancelDownloadSiteStatus").click(function(){
            _global.ClosePopup();
        });
    }
    
    //----------------------------------------------------------
    this.tr = 0;
    this.cmdAddSiteStatus_click = function(){
        var html='<div>\n\
                    <div id="ContainerID_frmSiteStatus">\n\
                    </div>\n\
                    <div style="text-align:center;" >\n\
                        <input type="button" id="cmdCancel_frmSiteStatus" class="link_button_popup" name="cmdCancel_frmSiteStatus" value="Cancel">\n\
                        <input type="button" id="cmdSave_frmSiteStatus" class="link_button_popup" name="cmdSave_frmSiteStatus" value="Save">\n\
                    </div>\n\
                </div>';
        var opt = {};
        opt.width    = 440;
        opt.height   = '';
        opt.position = '';
        opt.title    = '<div style="text-align:center;">Add WIN-Tags</div>';
        opt.body     = html;   
        opt.zindex = 99999;
        Me._roll.showNotAjax($("#cmdEditSiteStatusCbx"), $("#cmdEditSiteStatusCbx"), opt);        
        jQuery("#cmdCancel_frmSiteStatus").unbind("click",Me.cmdCancel_frmSiteStatus_onClick).bind("click",Me.cmdCancel_frmSiteStatus_onClick);
        jQuery("#cmdSave_frmSiteStatus").unbind("click",Me.cmdSave_frmSiteStatus_onClick).bind("click",Me.cmdSave_frmSiteStatus_onClick);
        
        var data='win='+jQuery(this).attr("WinNum")+'&update=true';
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/popup/edit-wintags",
            data: data,
            success:function( html ) {
                if(html != "")
                {
                    jQuery("#ContainerID_frmSiteStatus").html(html);
                    //                    Me.autoSizeSiteStatus();
                    Me.BindEventForFormEdit();
        }
    }
        });
        
    }
    
    //----------------------------------------------------------
    this.reloadwintagforAddSiteStatus_click = function(){
        var html='<div>\n\
                    <div id="ContainerID_frmSiteStatus">\n\
                    </div>\n\
                    <div style="text-align:center;" >\n\
                        <input type="button" id="cmdCancel_frmSiteStatus" class="link_button_popup" name="cmdCancel_frmSiteStatus" value="Cancel">\n\
                        <input type="button" id="cmdSave_frmSiteStatus" class="link_button_popup" name="cmdSave_frmSiteStatus" value="Save">\n\
                    </div>\n\
                </div>';
        var opt = {};
        opt.width    = 440;
        opt.height   = '';
        opt.position = '';
        opt.title    = '<div style="text-align:center;">Add WIN-Tags</div>';
        opt.body     = html;   
        opt.zindex = 99999;
        Me._roll.showNotAjax($("#cmdEditSiteStatusCbx"), $("#cmdEditSiteStatusCbx"), opt);        
        jQuery("#cmdCancel_frmSiteStatus").unbind("click",Me.cmdCancel_frmSiteStatus_onClick).bind("click",Me.cmdCancel_frmSiteStatus_onClick);
        jQuery("#cmdSave_frmSiteStatus").unbind("click",Me.cmdSave_frmSiteStatus_onClick).bind("click",Me.cmdSave_frmSiteStatus_onClick);
        
        var data='win='+jQuery(this).val()+'&update=true';
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/popup/edit-wintags",
            data: data,
            success:function( html ) {
                if(html != "")
                {
                    jQuery("#ContainerID_frmSiteStatus").html(html);
                    //                    Me.autoSizeSiteStatus();
                    Me.BindEventForFormEdit();
                }  
            }
        });
        
    }
    
    //----------------------------------------------------------
    this.changeclassTags = function(event){
        var text = jQuery(event).find(".classTags").find("option:selected").html();
        var val = jQuery(event).find(".classTags").val();
        if(text.indexOf("Complete") >= 0){
            jQuery(event).find(".classInvoiceable").hide();
            jQuery(event).find(".classInvoiceable").parent().children(".classTextShow").show();
            jQuery(event).find(".classInvoiceable").parent().children(".classTextShow").html("No");
            jQuery(event).find(".classInvoiceable").val(1);
        }else if(text.indexOf("Scheduled") >= 0){
            jQuery(event).find(".classInvoiceable").hide();
            jQuery(event).find(".classInvoiceable").parent().children(".classTextShow").show();
        }else {
            jQuery(event).find(".classInvoiceable").show();
            jQuery(event).find(".classInvoiceable").parent().children(".classTextShow").hide();
        }
        if(text.indexOf("Rescheduled") >= 0){
            jQuery("#frmSiteStatus #cbxSiteStatus").val(4);
        }
    }
    
    //------------------------------------------------------------
    
    //------------------------------------------------------------
    this.classTags_change = function(){
        var event = jQuery(this).parent().parent();
        Me.changeclassTags(event);
    }
    
    //------------------------------------------------------------
    this.setDataEdit = function(event){
        jQuery(event).find(".classWin").val(jQuery(event).find(".classHiddenWin").val());
        jQuery(event).find(".classDate").val(jQuery(event).find(".classHiddenDate").val());
        jQuery(event).find(".classTags").val(jQuery(event).find(".classHiddenTags").val());
        jQuery(event).find(".classInvoiceable").val(jQuery(event).find(".classHiddenInvoiceable").val());
    }
    
    //-------------------------------------------------
    this.cmdCancel_frmSiteStatus_onClick=function()
    {
        Me.edittag_close(); 
    }
    
    //-------------------------------------------------
    this.cmdSave_frmSiteStatus_onClick=function(){    
        var  data = jQuery("#frmWinTagsEdit").serialize();
        
        var  winnum=jQuery("#frmWinTagsEdit #WIN_Select option:selected").val();
        if(typeof(winnum)=="undefined")
        {
            winnum=jQuery("#frmWinTagsEdit #win").val()
        }
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/popup/updateandaddwintag",
            data: data,
            success:function( html ) {
                Me._roll.hide(); 
                Me.loadstatus(winnum);
            }
        });
    }
    
    //-------------------------------------------------
    this.BindEventForFormEdit=function(){
        jQuery("#frmWinTagsEdit input:checkbox").unbind("change").bind("change",Me.frmWinTags_change);
        var editwintagpopupid = $("#ContainerID_frmSiteStatus").parent().parent().parent().find(".popup-header-button").attr("id")
        jQuery("#"+editwintagpopupid).unbind("click").bind("click",Me.edittag_close);
        jQuery("#frmWinTagsEdit #cmdSaveWinTags").unbind("click").bind("click",Me.cmdSaveWinTags);
        jQuery("#frmWinTagsEdit .cmdDeleteWinTagsCompletionStatus").unbind("click").bind("click",Me.cmdDeleteWinTagsCompletionStatus);
        jQuery("#frmWinTagsEdit #cmdCancelWinTags").unbind("click").bind("click",_global.hideLoader);
        jQuery("#frmWinTagsEdit .openTechProfile").unbind("click").bind("click",Me.openTechProfile);
        jQuery("#frmWinTagsEdit .CompletionStatuscheck").unbind("click").bind("click",Me.CompletionStatuscheckEditForAddWinTag_click);
        jQuery("#frmWinTagsEdit .aCompletionStatus").unbind("click").bind("click",Me.CompletionStatuslinkEditForAddWinTag_click);
        jQuery("#frmWinTagsEdit .openSiteStatus").unbind("click",Me.openSiteStatusEditForAddWinTag_click).bind("click",Me.openSiteStatusEditForAddWinTag_click);
        //686
        jQuery("#frmWinTagsEdit #chkCompleteGroup").unbind("click").bind("click",Me.chkCompleteGroupEditForAddWinTag_change);
        jQuery("#frmWinTagsEdit #chkIncompleteGroup").unbind("click").bind("click",Me.chkIncompleteGroupEditForAddWinTag_change);
        jQuery("#frmWinTagsEdit #chkRescheduledGroup").unbind("click").bind("click",Me.chkRescheduledGroupEditForAddWinTag_change);
        jQuery("#frmWinTagsEdit #chkCancelledGroup").unbind("click").bind("click",Me.chkCancelledGroupEditForAddWinTag_change);
        
        jQuery("#frmWinTagsEdit #chk1stvisit").unbind("click").bind("click",Me.chk1stvisitEditForAddWinTag_click);
        jQuery("#frmWinTagsEdit #chkReassigned").unbind("click").bind("click",Me.chkReassignedEditForAddWinTag_click);
        jQuery("#frmWinTagsEdit #chkRevisitRequired").unbind("click").bind("click",Me.chkRevisitRequiredEditForAddWinTag_click);
        jQuery("#frmWinTagsEdit #chkRescheduled").unbind("click").bind("click",Me.chkRescheduledEditForAddWinTag_click);
        
        jQuery("#frmWinTagsEdit #chkIncompleteSiteClientCaused").unbind("click").bind("click",Me.chkIncompleteSiteClientCausedEditForAddWinTag_change);
        jQuery("#frmWinTagsEdit #chkIncompleteTechProviderCaused").unbind("click").bind("click",Me.chkIncompleteTechProviderCausedEditForAddWinTag_change);
        jQuery("#frmWinTagsEdit #chkIncompleteNaturalCaused").unbind("click").bind("click",Me.chkIncompleteNaturalCausedEditForAddWinTag_change);
        
        jQuery("#frmWinTagsEdit #chkRescheduledSiteClientCaused").unbind("click").bind("click",Me.chkRescheduledSiteClientCausedEditForAddWinTag_change);
        jQuery("#frmWinTagsEdit #chkRescheduledTechProviderCaused").unbind("click").bind("click",Me.chkRescheduledTechProviderCausedEditForAddWinTag_change);
        jQuery("#frmWinTagsEdit #chkRescheduledNaturalCaused").unbind("click").bind("click",Me.chkRescheduledNaturalCausedEditForAddWinTag_change);
        
        jQuery("#frmWinTagsEdit #IncompleteSiteClientAdd").unbind("click").bind("click",Me.IncompleteSiteClientAddEditForAddWinTag_click);
        jQuery("#frmWinTagsEdit #Incompletetagsitemlist").unbind("change").bind("change",Me.SiteClienttagsitemlistEditForAddWinTag_change);
        jQuery("#frmWinTagsEdit .IncompleteSiteClientStatus").unbind("click").bind("click",Me.IncompleteSiteClientCausedStatuslinkEditForAddWinTag_click);
        
        jQuery("#frmWinTagsEdit #IncompleteTechProviderAdd").unbind("click").bind("click",Me.IncompleteTechProviderAddEditForAddWinTag_click);
        jQuery("#frmWinTagsEdit #IncompleteTechProvidertagsitemlist").unbind("change").bind("change",Me.TechProviderTagsitemlistEditForAddWinTag_change);
        jQuery("#frmWinTagsEdit .IncompleteTechProviderStatus").unbind("click").bind("click",Me.IncompleteTechProviderCausedStatuslinkEditForAddWinTag_click);
        
        jQuery("#frmWinTagsEdit #RescheduledSiteClientAdd").unbind("click").bind("click",Me.RescheduledSiteClientAddEditForAddWinTag_click);
        jQuery("#frmWinTagsEdit #RescheduledSiteClienttagsitemlist").unbind("change").bind("change",Me.RescheduledSiteClienttagsitemlistEditForAddWinTag_change);
        jQuery("#frmWinTagsEdit .RescheduledSiteClientStatus").unbind("click").bind("click",Me.RescheduledSiteClientCausedStatuslinkEditForAddWinTag_click);
        
        jQuery("#frmWinTagsEdit #RescheduledTechProviderAdd").unbind("click").bind("click",Me.RescheduledTechProviderAddEditForAddWinTag_click);
        jQuery("#frmWinTagsEdit #RescheduledTechProvidertagsitemlist").unbind("change").bind("change",Me.RescheduledTechProviderTagsitemlistEditForAddWinTag_change);
        jQuery("#frmWinTagsEdit .RescheduledTechProviderStatus").unbind("click").bind("click",Me.RescheduledTechProviderCausedStatuslinkEditForAddWinTag_click);
        
        jQuery("#frmWinTagsEdit #WIN_Select").unbind("change").bind("change",Me.reloadwintagforAddSiteStatus_click);
        jQuery("#frmWinTagsEdit .cmdDeletewintag").unbind("click").bind("click",Me.DeleteWintagEditForAddWinTag_click);
        //end 686
        //847
        jQuery(".cmdaddwintagnote").unbind("click").bind("click",Me.cmdaddwintagnoteEditForAddWinTag_click);
    //end 874
    }
    
    this.edittag_close=function()
    {
        if(!Me.flash){
            Me._roll.hide();
            return false;
        }
        var html = '<div style=""><div>Would you like to save your changes before closing?</div><div style="text-align:center; margin-top: 5px"><input type="button" value="Cancel"  class="link_button middle2_button cmdCloseWinTags">&nbsp;<input type="button" value="Save" class="link_button middle2_button cmdSaceChangesWintags"></div></div>';
        _global.showPopupNew({
            width       : 300,
            height      : 70,
            position    : 'middle',
            title       : '',
            zindex      : 100001,
            body        : html
        },"lightboxWinTagseidt","",null);
        
        jQuery("#lightboxWinTagseidt").html(html);
        jQuery(".cmdSaceChangesWintags").unbind("click");
        jQuery(".cmdSaceChangesWintags").click(function(){
            $("#lightboxWinTagseidt").parent().parent().hide();
            Me.cmdSave_frmSiteStatus_onClick();
        });
        jQuery(".cmdCloseWinTags").click(function(){
            $("#lightboxWinTagseidt").parent().parent().hide();
            Me._roll.hide();
            Me.flash = false;
        });
    }
    
    //-------------------------------------------------
    this.editItemSiteStatus_click = function()
    {
        Me.flash = false;
        var html='<div>\n\
                    <div id="ContainerID_frmSiteStatus"></div>\n\
                    <div style="text-align:center;">\n\
                        <input type="button" id="cmdCancel_frmSiteStatus" class="link_button_popup" name="cmdCancel_frmSiteStatus" value="Cancel">\n\
                        <input type="button" id="cmdSave_frmSiteStatus" class="link_button_popup" name="cmdSave_frmSiteStatus" value="Save">\n\
                    </div>\n\
                </div>';
        var opt = {};
        opt.width    = 440;
        opt.height   = '';
        opt.position = '';
        opt.title    = '<div style="text-align:center;">Edit WIN-Tags</div>';
        opt.body     = html;   
        opt.zindex = 99999;
        Me._roll.showNotAjax($("#cmdEditSiteStatusCbx"), $("#cmdEditSiteStatusCbx"), opt);        
        jQuery("#cmdCancel_frmSiteStatus").unbind("click",Me.cmdCancel_frmSiteStatus_onClick).bind("click",Me.cmdCancel_frmSiteStatus_onClick);
        jQuery("#cmdSave_frmSiteStatus").unbind("click",Me.cmdSave_frmSiteStatus_onClick).bind("click",Me.cmdSave_frmSiteStatus_onClick);
        
        var data='win='+jQuery(this).attr("WinNum")+'&update=false';
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/popup/edit-wintags",
            data: data,
            success:function( html ) {
                if(html != "")
                {
                    jQuery("#ContainerID_frmSiteStatus").html(html);
                    Me.BindEventForFormEdit();
                //                    Me.autoSizeSiteStatus();
                }  
            }
        });
    }
    
    //------------------------------------------------------------
    this.cancelEditItemSiteStatus_click = function(){
        var event = jQuery(this).parent().parent();
        Me.setDataEdit(event);
        jQuery(event).find("select").hide();
        jQuery(event).find("input:text").hide();
        jQuery(event).find(".classTextShow").show();
        jQuery(this).hide();
        jQuery(event).find(".editItemSiteStatus").show();
        Me.autoSizeSiteStatus();
        Me.makingANYedits();
    }
    
    //------------------------------------------------------------
    this.cmdSaveSiteStatus_click = function(){
        _global.data = jQuery("#frmSiteStatus").serialize();
        _global.url = "/widgets/dashboard/popup/site-status";

        _global.onSuccess = function(data){
            _global.hideLoader();
            _global.showMessages(data);
            if(Me.reload){
                window.location.reload();
            }else{
                reloadTabFrame();
            }
        };
        _global.show(this);
    }
    
    //------------------------------------------------------------
    this.makingANYedits = function(){
        
        var show = jQuery("#frmSiteStatus #viewSiteStatus").css("display") == "none";
        var show_tags = false;
        jQuery("#frmSiteStatus .classWin").each(function() {
            if(jQuery(this).css("display") != "none"){
                show_tags = true;
                return false;
            }
        });
        if(show || show_tags){
            jQuery("#frmSiteStatus #divSave").show();
            jQuery("#frmSiteStatus #cmdDownloadSiteStatus").removeClass("download_button");
            jQuery("#frmSiteStatus #cmdDownloadSiteStatus").addClass("download_disabled_button");
            jQuery("#frmSiteStatus #cmdDownloadSiteStatus").unbind("click");
            Me.flashST = true;
        }else{
            jQuery("#frmSiteStatus #divSave").hide();
            jQuery("#frmSiteStatus #cmdDownloadSiteStatus").removeClass("download_disabled_button");
            jQuery("#frmSiteStatus #cmdDownloadSiteStatus").addClass("download_button");
            jQuery("#frmSiteStatus #cmdDownloadSiteStatus").unbind("click").bind("click",Me.cmdDownloadSiteStatus_click);
            Me.flashST = false;
        }
    }
    //847
    //------------------------------------------------------------
    this.fancyboxSiteStatus_close = function(){
        if(!Me.flashST){
            _global.hideLoader();
            return false;
        }
        var html = '<div style=""><div>Would you like to save your changes before closing?</div><div style="text-align:center; margin-top: 5px"><input type="button" value="Close"  class="link_button middle2_button cmdCloseWinTags">&nbsp;<input type="button" value="Save Changes" class="link_button middle2_button cmdSaceChangesWintags"></div></div>';
        _global.showPopupNew({
            width       : 300,
            height      : 70,
            position    : 'middle',
            title       : '',
            zindex      : 9999,
            body        : html
        },"lightboxWinStatus","",null);
        
        jQuery("#lightboxWinStatus").html(html);
        jQuery(".cmdSaceChangesWintags").unbind("click");
        jQuery(".cmdSaceChangesWintags").click(function(){
            _global.ClosePopup();
            jQuery("#frmSiteStatus #cmdSaveSiteStatus").click();
            Me._roll1.hide();
        });
        jQuery(".cmdCloseWinTags").click(function(){
            _global.ClosePopup();
            _global.hideLoader();
            Me._roll1.hide();
        });
    }
    //end 847
    //------------------------------------------------------------
    this.cmdEditSiteStatusCbx_click = function(){
        jQuery("#frmSiteStatus #viewSiteStatus").hide();
        jQuery("#frmSiteStatus #cbxSiteStatus").show();
        jQuery("#frmSiteStatus #cmdButtonEditSiteStatusCbx").removeClass("link_button_sm");
        jQuery("#frmSiteStatus #cmdButtonEditSiteStatusCbx").addClass("link_disabled_sm");
        Me.makingANYedits();
    }
    
    //------------------------------------------------------------
    this.loadstatus=function(win){
        Me.flashST = false;
        _global.data = "win="+win;
        _global.url = "/widgets/dashboard/popup/site-status";
        var _onSuccess = function(data){
            jQuery("#comtantPopupID").html(data);
            Me.buildEventFormSiteStatus();
            Me.autoSizeSiteStatus();
        }
        _global.onSuccess = _onSuccess;
        _global.showLoader(_global.width,_global.height,true);
        _global.show();
    }
    
    //------------------------------------------------------------
    this.autoSizeSiteStatus = function(){
        var height = _global.height - (90 + jQuery("#trHeaderSiteStatus").height() + jQuery("#divSave").height() );
        
        
        if(jQuery("#divSiteStatusContent table").height() > height){
            jQuery("#divSiteStatusContent").height(height);
            jQuery("#divSiteStatusContent").children("table").height(height);
            Me.height = _global.height;
        }else{
            var _height = jQuery("#divSiteStatusContent table").height()+90 + jQuery("#trHeaderSiteStatus").height()+ jQuery("#divSave").height();
            if(jQuery("#divSiteStatusContent table").height()==0)
            {
                _height = 430;
            }
            $('#fancybox-content').height(_height); 
            $('#fancybox-wrap').height(_height+20);
            Me.height = _height+20;
            jQuery("#comtantPopupID").height(_height+7);
            $.fancybox.resize();
            
        }
    }
    
    //686
    //general
    //------------------------------------------------------------
    this.CompletionStatuscheck_click = function()
    {
        var wintagid = $(this).val();
        var SiteStatus = $(this).attr('SiteStatus');
        var tagstatus = "";
        if( $("#frmWinTags #chk1stvisit").is(":checked") || 
            $("#frmWinTags #chkRevisitRequired").is(":checked") ||
            $("#frmWinTags #chkReassigned").is(":checked") ||
            $("#frmWinTags #chkRescheduled").is(":checked")
            )
            {
            tagstatus ="";
        }
        else
        {
            if(SiteStatus!=2)
            {
                tagstatus = " (<a class='aCompletionStatus' SiteStatus ='"+SiteStatus+"' wintagid='"+wintagid+"' style='cursor:pointer;' title='Click to mark this WIN-Tag as a &lsquo;non-billable&rsquo; event'>billable</a>)<input type='hidden' id='CompletionStatustype_"+wintagid+"' name='cbxCompletionStatus_"+wintagid+"' value='2'/>";
            }

            if($(this).is(":checked"))
            {
                $("#frmWinTags #WinTagbillable_"+wintagid).html(tagstatus);
                $("#frmWinTags #WinTagbillable_"+wintagid).css("display","");
                jQuery("#frmWinTags .aCompletionStatus").unbind("click").bind("click",Me.CompletionStatuslink_click);
            }
            else
            {
                $("#frmWinTags #WinTagbillable_"+wintagid).html("");
                $("#frmWinTags #WinTagbillable_"+wintagid).css("display","none");
            }
        }
    }
    
    //------------------------------------------------------------
    this.woCompletionStatuscheck_click = function()
    {
        var wintagid = $(this).val();
        var SiteStatus = $(this).attr('SiteStatus');
        var tagstatus = "";
        if( $("#chk1stvisit").is(":checked") || 
            $("#chkRevisitRequired").is(":checked") ||
            $("#chkReassigned").is(":checked") ||
            $("#chkRescheduled").is(":checked")
            )
            {
            tagstatus ="";
            $("#WinTagbillable_"+wintagid).html(tagstatus);
        }
        else
        {
            if(SiteStatus!=2)
            {
                tagstatus = " (<a class='aCompletionStatus' SiteStatus ='"+SiteStatus+"' wintagid='"+wintagid+"' style='cursor:pointer;' title='Click to mark this WIN-Tag as a &lsquo;non-billable&rsquo; event'>billable</a>)<input type='hidden' id='CompletionStatustype_"+wintagid+"' name='cbxCompletionStatus_"+wintagid+"' value='2'/>";
            }

            if($(this).is(":checked"))
            {
                $("#WinTagbillable_"+wintagid).html(tagstatus);
                $("#WinTagbillable_"+wintagid).css("display","");
                jQuery(".aCompletionStatus").unbind("click").bind("click",Me.woCompletionStatuslink_click);
            }
            else
            {
                $("#WinTagbillable_"+wintagid).html("");
                $("#WinTagbillable_"+wintagid).css("display","none");
            }
        }
    }
    
    this.loadtagstatusitem = function(el, frmname)
    {
        var wintagid = $(el).attr("wintagid");
        var CompletionStatustype = $(frmname + " #CompletionStatustype_"+wintagid).val();
        var CompletionStatustypeval = 2;
        var CompletionStatustypetext = "";
        var CompletionStatustypetitle = "";
        
        if(CompletionStatustype==1)
        {
            CompletionStatustypeval=2;
            CompletionStatustypetext ="billable";
            CompletionStatustypetitle = "Click to mark this WIN-Tag as a &lsquo;non-billable&rsquo; event";
        }
        else
        {
            CompletionStatustypeval =1;
            CompletionStatustypetext ="no $";
            CompletionStatustypetitle = "Click to mark this WIN-Tag as a &lsquo;billable&rsquo; event";
        }
        
        var tagstatus = " (<a class='aCompletionStatus' wintagid='"+wintagid+"' style='cursor:pointer;' title='"+CompletionStatustypetitle+"'>"+CompletionStatustypetext+"</a>)<input type='hidden' id='CompletionStatustype_"+wintagid+"' name='cbxCompletionStatus_"+wintagid+"' value='"+CompletionStatustypeval+"'/>";
        $("#WinTagbillable_"+wintagid).html(tagstatus);
        $("#WinTagbillable_"+wintagid).css("display","");
    }
    
    //------------------------------------------------------------
    this.CompletionStatuslink_click = function()
    {
        var wintagid = $(this).attr("wintagid");
        var CompletionStatustype = $("#frmWinTags #CompletionStatustype_"+wintagid).val();
        var CompletionStatustypeval = 2;
        var CompletionStatustypetext = "";
        var CompletionStatustypetitle = "";
        
        if(CompletionStatustype==1)
        {
            CompletionStatustypeval=2;
            CompletionStatustypetext ="billable";
            CompletionStatustypetitle = "Click to mark this WIN-Tag as a &lsquo;non-billable&rsquo; event";
        }
        else
        {
            CompletionStatustypeval =1;
            CompletionStatustypetext ="no $";
            CompletionStatustypetitle = "Click to mark this WIN-Tag as a &lsquo;billable&rsquo; event";
        }
        
        var tagstatus = " (<a class='aCompletionStatus' wintagid='"+wintagid+"' style='cursor:pointer;' title='"+CompletionStatustypetitle+"'>"+CompletionStatustypetext+"</a>)<input type='hidden' id='CompletionStatustype_"+wintagid+"' name='cbxCompletionStatus_"+wintagid+"' value='"+CompletionStatustypeval+"'/>";
        $("#frmWinTags  #WinTagbillable_"+wintagid).html(tagstatus);
        $("#frmWinTags #WinTagbillable_"+wintagid).css("display","");
        jQuery("#frmWinTags .aCompletionStatus").unbind("click").bind("click",Me.CompletionStatuslink_click);
    }
    
    //------------------------------------------------------------
    this.woCompletionStatuslink_click = function()
    {
        var wintagid = $(this).attr("wintagid");
        var CompletionStatustype = $("#CompletionStatustype_"+wintagid).val();
        var CompletionStatustypeval = 2;
        var CompletionStatustypetext = "";
        var CompletionStatustypetitle = "";
        
        if(CompletionStatustype==1)
        {
            CompletionStatustypeval=2;
            CompletionStatustypetext ="billable";
            CompletionStatustypetitle = "Click to mark this WIN-Tag as a &lsquo;non-billable&rsquo; event";
        }
        else
        {
            CompletionStatustypeval =1;
            CompletionStatustypetext ="no $";
            CompletionStatustypetitle = "Click to mark this WIN-Tag as a &lsquo;billable&rsquo; event";
        }
        
        var tagstatus = " (<a class='aCompletionStatus' wintagid='"+wintagid+"' style='cursor:pointer;' title='"+CompletionStatustypetitle+"'>"+CompletionStatustypetext+"</a>)<input type='hidden' id='CompletionStatustype_"+wintagid+"' name='cbxCompletionStatus_"+wintagid+"' value='"+CompletionStatustypeval+"'/>";
        $("#WinTagbillable_"+wintagid).html(tagstatus);
        $("#WinTagbillable_"+wintagid).css("display","");
        jQuery(".aCompletionStatus").unbind("click").bind("click",Me.woCompletionStatuslink_click);
    }
    
    //------------------------------------------------------------
    this.cmdAddTag=function(selectorid, selectWinTagObjID, divWinTaglistID, divCmdAddWinTagID, frmname )
    {
        var selectWinTagObj = $(frmname + " #"+selectWinTagObjID);
        $(frmname + " #"+divWinTaglistID).append(selectWinTagObj);
        $(frmname + " #"+selectWinTagObjID).css("display","");
        $(frmname + " #"+selectWinTagObjID + " #"+selectorid).css("display","");
        $(frmname + " #"+divCmdAddWinTagID).css("display","none"); 
    }
    
    //847
    //------------------------------------------------------------
    this.loadTagItem = function(divloadname, tagsessionname, tagNo, frmname, name)
    {
        var wintagid = $(frmname + " #div"+divloadname+"itemlist select option:selected").val();
        var wintagtitle = $(frmname + " #div"+divloadname+"itemlist select option:selected").html();
        var No = parseInt($(frmname + " #"+tagNo+"No").val());
        No +=1; 
        if(wintagid!="")
        {
            var tagstatus = "<span><img style='cursor:pointer;' src='/widgets/css/images/removedoc.png' height='15px'  title='Remove Win-Tag' class='cmdDeletewintag' />&nbsp;&nbsp;"+ wintagtitle+" \n\
                <span id='"+tagsessionname+"No_"+No+"' >\n\
                (<a class='"+tagsessionname+"Status' "+tagsessionname+"No='"+No+"' wintagid='"+wintagid+"' style='cursor:pointer;' title='Click to mark this WIN-Tag as a &lsquo;non-billable&rsquo; event'>billable</a>)\n\
                <input type='hidden' id='"+tagsessionname+"Statustype_"+wintagid+"' name='CompletionStatusval[]' value='2'/>\n\
                <input type='hidden' name='wintagid[]' value='"+wintagid+"'/>\n\
                </span>\n\
                <img id='wintagitemaddnote_"+wintagid+"' wintagitemaddnotevalueid ='"+wintagid+"' style='cursor:pointer;' src='/widgets/images/Pencilicon.png' height='15px'  title='Add a note to this WIN-Tag' class='cmdaddwintagnote' strname='"+name+": "+wintagtitle+"' /> \n\
                <input type='hidden' id='wintagitemaddnotevalue_"+wintagid+"' name='wintagitemaddnotevalue[]' value=''/>\n\
                <br/>\n\
                </span>";
            $(frmname + " #div"+divloadname+"itemlist select").val('');
            $(frmname+ " #"+tagsessionname+"CausedItems").append(tagstatus);
            $(frmname + " #"+tagsessionname+"No").val(No);
            $(frmname + " #div"+divloadname+"itemlist").css("display","none");
            $(frmname + " #div"+tagsessionname+"Add").css("display","");
            $(frmname + " #"+tagsessionname+"Add").css("display","");
            if(frmname!="#frmWinTagsEdit" && frmname !="")
            {
                Me.autoSize();
            }
        }
    }
    //end 847
    //wintag form
    this.DeleteWintag_click = function()
    {
        var wintagitemdeletevalueid = $(this).attr('wintagitemdeletevalueid');
        var tagsname = $(this).parent().parent().parent().attr("tagsname");
        var NumberOfitem = $(this).parent().parent().find("img").size();
        if(typeof(wintagitemdeletevalueid)=="undefined")
        {
            $(this).parent().remove();
        }
        else
        {
            $("#frmWinTags #wintagitemdeletevalueid_"+wintagitemdeletevalueid).val(wintagitemdeletevalueid);
            $(this).parent().css("display","none");
            $(this).remove();
        }
        
        if((NumberOfitem-1)<=0)
        {
            $("#frmWinTags #"+tagsname).attr('checked','');
            if(tagsname=='chkIncompleteSiteClientCaused')
            {
                Me.chkIncompleteSiteClientCaused_change();
    }
            else if(tagsname=='chkIncompleteTechProviderCaused')
            {
                Me.chkIncompleteTechProviderCaused_change();
            } else if(tagsname=='chkRescheduledSiteClientCaused')
            {
                Me.chkRescheduledSiteClientCaused_change();
            } else if(tagsname=='chkRescheduledTechProviderCaused')
            {
                Me.chkRescheduledTechProviderCaused_change();
            }

        }
    }

    //------------------------------------------------------------
    this.woDeleteWintag_click = function()
    {
        var wintagitemdeletevalueid = $(this).attr('wintagitemdeletevalueid');
        var tagsname = $(this).parent().parent().parent().attr("tagsname");
        var NumberOfitem = $(this).parent().parent().find("img").size();
        
        if(typeof(wintagitemdeletevalueid)=="undefined")
        {
            $(this).parent().remove();
        }
        else
        {
            $("#wintagitemdeletevalueid_"+wintagitemdeletevalueid).val(wintagitemdeletevalueid);
            $(this).parent().css("display","none");
            $(this).remove();
        }
        
        if((NumberOfitem-1)<=0)
        {
            $("#"+tagsname).attr('checked','');
            if(tagsname=='chkIncompleteSiteClientCaused')
            {
                Me.wochkIncompleteSiteClientCaused_change();
    }
            else if(tagsname=='chkIncompleteTechProviderCaused')
            {
                Me.wochkIncompleteTechProviderCaused_change();
            } else if(tagsname=='chkRescheduledSiteClientCaused')
            {
                Me.wochkRescheduledSiteClientCaused_change();
            } else if(tagsname=='chkRescheduledTechProviderCaused')
            {
                Me.wochkRescheduledTechProviderCaused_change();
            }
        }
    }
    
    //------------------------------------------------------------
    this.DeleteWintagEditForAddWinTag_click = function()
    {
        var wintagitemdeletevalueid = $(this).attr('wintagitemdeletevalueid');
        var tagsname = $(this).parent().parent().parent().attr("tagsname");
        var NumberOfitem = $(this).parent().parent().find("img").size();
        
        if(typeof(wintagitemdeletevalueid)=="undefined")
        {
            $(this).parent().remove();
        }
        else
        {
            $("#frmWinTagsEdit #wintagitemdeletevalueid_"+wintagitemdeletevalueid).val(wintagitemdeletevalueid);
            $(this).parent().css("display","none");
            $(this).remove();
        }
        
        if((NumberOfitem-1)<=0)
        {
            $("#frmWinTagsEdit #"+tagsname).attr('checked','');
            if(tagsname=='chkIncompleteSiteClientCaused')
            {
                Me.chkIncompleteSiteClientCausedEditForAddWinTag_change();
    }
            else if(tagsname=='chkIncompleteTechProviderCaused')
            {
                Me.chkIncompleteTechProviderCausedEditForAddWinTag_change();
            } else if(tagsname=='chkRescheduledSiteClientCaused')
            {
                Me.chkRescheduledSiteClientCausedEditForAddWinTag_change();
            } else if(tagsname=='chkRescheduledTechProviderCaused')
            {
                Me.chkRescheduledTechProviderCausedEditForAddWinTag_change();
            }
        }
    }
    
    //------------------------------------------------------------
    this.openSiteStatus_click = function()
    {
        var win = jQuery(this).attr("win");
        if(!Me.flash){
            Me.loadstatus(win);
            return false;
        }
        roll.autohide(false);
        var opt = {
            width : 350,
            height : 70,
            title : 'Would you like to save your changes  before closing',
            handler : '',
            context : wd,
            body : '<div style="text-align: center;height: 30px;width: 100%;">'+
            '<input type="button" class="link_button_sm" value="Cancel" id="cmdCancel">'+
            '&nbsp;&nbsp;&nbsp;'+
            '<input type="button" class="link_button_sm" value="Save" id="cmdSave">'+
            '</div>',
            zindex:10000
        };
        roll.showNotAjax($("#chkRevisitRequired"), $("#chkRevisitRequired"), opt);
        $("#cmdCancel").unbind("click").bind('click',
            function(){
                roll.hide();
                Me.loadstatus(win);
            }
            )
        $("#cmdSave").unbind("click").bind('click',
            function(){
                    
                roll.hide();
                Me.cmdSaveWinTags();
                Me.loadstatus(win);
            }
            )   
       
    }
    
    //------------------------------------------------------------
    this.openWintagfromSiteStatus_click = function()
    {
        var win = jQuery(this).attr("win");
        if(!Me.flash){
            Me.loadstatus(win);
            return false;
        }
        roll.autohide(false);
        var opt = {
            width : 350,
            height : 70,
            title : 'Would you like to save your changes  before closing',
            handler : '',
            context : wd,
            body : '<div style="text-align: center;height: 30px;width: 100%;">'+
            '<input type="button" class="link_button_sm" value="Cancel" id="cmdCancel">'+
            '&nbsp;&nbsp;&nbsp;'+
            '<input type="button" class="link_button_sm" value="Save" id="cmdSave">'+
            '</div>',
            zindex:10000
        };
        roll.showNotAjax($("#chkRevisitRequired"), $("#chkRevisitRequired"), opt);
        $("#cmdCancel").unbind("click").bind('click',
            function(){
                roll.hide();
                Me.loadstatus(win);
            }
            )
        $("#cmdSave").unbind("click").bind('click',
            function(){
                    
                roll.hide();
                Me.cmdSaveWinTags();
                Me.loadstatus(win);
            }
            )   
       
    }
    //847
    //------------------------------------------------------------
    this.CheckGroupchange = function(frmname, tablename, chkname)
    {
        if(!$(frmname + " " + chkname).is(":checked"))
        {
            $(frmname + " " + tablename + " input:checkbox").attr('checked','');
            $(frmname + " #Completenote_25").html("");
            $(frmname + " #Completenote_26").html("");
            $(frmname + " #Completenote_27").html("");
            $(frmname + " #Completenote_28").html("");
        }
    }
    //end 847
    //------------------------------------------------------------
    this.IsShowAddcommandAndCombobox = function(frmname, itemname, listname, linkname, divlistname)
    {
        if($(frmname + " " + itemname + " > span").size()>0)
        {
            $(frmname + " " + listname).css("display","none");
            $(frmname + " " + linkname).css("display","");
        }
        else
        {
            $(frmname + " " + divlistname).css("display","");
            $(frmname + " " + listname).css("display","");
            $(frmname + " " + linkname).css("display","none");
        }
    }
    
    //------------------------------------------------------------
    this.IncompleteSiteClientCausedchange = function(frmname, chkitem, itemname, listname, linkname, divlistname)
    {
        if($(frmname + " " + chkitem).is(":checked"))
        {
            $(frmname + " " + itemname).css("display","");
            Me.IsShowAddcommandAndCombobox(frmname, itemname, listname, linkname, divlistname);
            jQuery(frmname + " " + listname).unbind("change").bind("change",Me.SiteClienttagsitemlist_change);
        }
        else
        {
            $(frmname + " " + linkname).css("display","none");
            $(frmname + " " + itemname).css("display","none");
            jQuery(frmname + " " + listname).unbind("change");
        }
    }
    
    //------------------------------------------------------------
    this.IncompleteTechProviderCausedchange = function(frmname, chkitem, itemname, listname, linkname, divlistname)
    {
        if($(frmname + " " + chkitem).is(":checked"))
        {
            $(frmname + " " + itemname).css("display","");
            Me.IsShowAddcommandAndCombobox(frmname, itemname, listname, linkname, divlistname);
            jQuery(frmname + " " + listname).unbind("change").bind("change",Me.TechProviderTagsitemlist_change);
        }
        else
        {
            $(frmname + " " + linkname).css("display","none");
            $(frmname + " " + itemname).css("display","none");
            jQuery(frmname + " " + listname).unbind("change");
        }
    }
    
    //------------------------------------------------------------
    this.IncompleteNaturalCausedchange = function(frmname, chkname)
    {
        var wintagid = $(frmname + " " + chkname).attr("wintagid");
        var tagstatus = " (<a class='aCompletionStatus' wintagid='"+wintagid+"' style='cursor:pointer;' title='Click to mark this WIN-Tag as a &lsquo;non-billable&rsquo; event'>billable</a>)<input type='hidden' id='CompletionStatustype_"+wintagid+"' name='cbxCompletionStatus_"+wintagid+"' value='2'/>";
        //847
        var notepart = " <img id='wintagitemaddnote_"+wintagid+"' wintagitemaddnotevalueid ='"+wintagid+"' style='cursor:pointer;' src='/widgets/images/Pencilicon.png' height='15px'  title='Add a note to this WIN-Tag' class='cmdaddwintagnote' strname='Incomplete due to Natural Causes' /> <input type='hidden' id='wintagitemaddnotevalue_"+wintagid+"' name='wintagitemaddnotevalue"+wintagid+"' value=''/>";

        if($(frmname + " " + chkname).is(":checked"))
        {
            $(frmname + " #WinTagbillable_"+wintagid).html(tagstatus);
            $(frmname + " #WinTagbillable_"+wintagid).css("display","");
            $(frmname + " #WinTagbillable_"+wintagid).parent().append(notepart);
            jQuery(frmname + " .aCompletionStatus").unbind("click").bind("click",Me.CompletionStatuslink_click);
            jQuery(frmname + " .cmdaddwintagnote").unbind("click").bind("click",Me.cmdaddwintagnote_click);
        }
        else
        {
            $(frmname + " #WinTagbillable_"+wintagid).parent().find("img").remove();
            $(frmname + " #WinTagbillable_"+wintagid).parent().find("#wintagitemaddnotevalue_"+wintagid).remove();
            $(frmname + " #WinTagbillable_"+wintagid).html("");
            $(frmname + " #WinTagbillable_"+wintagid).css("display","none");
        }
    }
    //end 847
    //------------------------------------------------------------
    this.RescheduledSiteClientCausedchange = function(frmname, chkitem, itemname, listname, linkname, divlistname)
    {
        if($(frmname + " " + chkitem).is(":checked"))
        {
            $(frmname + " " + itemname).css("display","");
            Me.IsShowAddcommandAndCombobox(frmname, itemname, listname, linkname, divlistname);
            jQuery(frmname + " " + listname).unbind("change").bind("change",Me.RescheduledSiteClienttagsitemlist_change);
        }
        else
        {
            $(frmname + " " + linkname).css("display","none");
            $(frmname + " " + itemname).css("display","none");
            jQuery(frmname + " " + listname).unbind("change");
        }
    }
    
    //------------------------------------------------------------
    this.RescheduledTechProviderCausedchange = function(frmname, chkitem, itemname, listname, linkname, divlistname)
    {
        if($(frmname + " " + chkitem).is(":checked"))
        {
            $(frmname + " " + itemname).css("display","");
            Me.IsShowAddcommandAndCombobox(frmname, itemname, listname, linkname, divlistname);
            jQuery(frmname + " " + listname).unbind("change").bind("change",Me.RescheduledTechProviderTagsitemlist_change);
        }
        else
        {
            $(frmname + " " + itemname).css("display","none");
            $(frmname + " #RescheduledTechProviderAdd").css("display","none");
            jQuery(frmname + " " + listname).unbind("change");
        }
    }
    //847
    //------------------------------------------------------------
    this.RescheduledNaturalCausedchange = function(frmname, chkname)
    {
        var wintagid = $(frmname + " " + chkname).attr("wintagid");
        var tagstatus = " (<a class='aCompletionStatus' wintagid='"+wintagid+"' style='cursor:pointer;' title='Click to mark this WIN-Tag as a &lsquo;non-billable&rsquo; event'>billable</a>)<input type='hidden' id='CompletionStatustype_"+wintagid+"' name='cbxCompletionStatus_"+wintagid+"' value='2'/>";
        //847
        var notepart = " <img id='wintagitemaddnote_"+wintagid+"' wintagitemaddnotevalueid ='"+wintagid+"' style='cursor:pointer;' src='/widgets/images/Pencilicon.png' height='15px'  title='Add a note to this WIN-Tag' class='cmdaddwintagnote' strname='Rescheduled due to Natural Causes' /> <input type='hidden' id='wintagitemaddnotevalue_"+wintagid+"' name='wintagitemaddnotevalue"+wintagid+"' value=''/>";
        if($(frmname + " " + chkname).is(":checked"))
        {
            $(frmname + " #WinTagbillable_"+wintagid).html(tagstatus);
            $(frmname + " #WinTagbillable_"+wintagid).css("display","");
            $(frmname + " #WinTagbillable_"+wintagid).parent().append(notepart);
            jQuery(frmname + " .aCompletionStatus").unbind("click").bind("click",Me.CompletionStatuslink_click);
            jQuery(frmname + " .cmdaddwintagnote").unbind("click").bind("click",Me.cmdaddwintagnote_click);
        }
        else
        {
            $(frmname + " #WinTagbillable_"+wintagid).parent().find("img").remove();
            $(frmname + " #WinTagbillable_"+wintagid).parent().find("#wintagitemaddnotevalue_"+wintagid).remove();
            $(frmname + " #WinTagbillable_"+wintagid).html("");
            $(frmname + " #WinTagbillable_"+wintagid).css("display","none");
        }
    }
    //end 847
    //------------------------------------------------------------
    this.woIncompleteSiteClientCausedchange = function(frmname, chkitem, itemname, listname, linkname, divlistname)
    {
        if($(frmname + " " + chkitem).is(":checked"))
        {
            $(frmname + " " + itemname).css("display","");
            Me.IsShowAddcommandAndCombobox(frmname, itemname, listname, linkname, divlistname);
            jQuery(frmname + " " + listname).unbind("change").bind("change",Me.woSiteClienttagsitemlist_change);
        }
        else
        {
            $(frmname + " " + linkname).css("display","none");
            $(frmname + " " + itemname).css("display","none");
            jQuery(frmname + " " + listname).unbind("change");
        }
    }
    
    //------------------------------------------------------------
    this.woIncompleteTechProviderCausedchange = function(frmname, chkitem, itemname, listname, linkname, divlistname)
    {
        if($(frmname + " " + chkitem).is(":checked"))
        {
            $(frmname + " " + itemname).css("display","");
            Me.IsShowAddcommandAndCombobox(frmname, itemname, listname, linkname, divlistname);
            jQuery(frmname + " " + listname).unbind("change").bind("change",Me.woTechProviderTagsitemlist_change);
        }
        else
        {
            $(frmname + " " + linkname).css("display","none");
            $(frmname + " " + itemname).css("display","none");
            jQuery(frmname + " " + listname).unbind("change");
        }
    }
    
    //------------------------------------------------------------
    this.woIncompleteNaturalCausedchange = function(frmname, chkname)
    {
        var wintagid = $(frmname + " " + chkname).attr("wintagid");
        var tagstatus = " (<a class='aCompletionStatus' wintagid='"+wintagid+"' style='cursor:pointer;' title='Click to mark this WIN-Tag as a &lsquo;non-billable&rsquo; event'>billable</a>)<input type='hidden' id='CompletionStatustype_"+wintagid+"' name='cbxCompletionStatus_"+wintagid+"' value='2'/>";
        //847
        var notepart = " <img id='wintagitemaddnote_"+wintagid+"' wintagitemaddnotevalueid ='"+wintagid+"' style='cursor:pointer;' src='/widgets/images/Pencilicon.png' height='15px'  title='Add a note to this WIN-Tag' class='cmdaddwintagnote' strname='Incomplete due to Natural Causes' /> <input type='hidden' id='wintagitemaddnotevalue_"+wintagid+"' name='wintagitemaddnotevalue"+wintagid+"' value=''/>";
        if($(frmname + " " + chkname).is(":checked"))
        {
            $(frmname + " #WinTagbillable_"+wintagid).html(tagstatus);
            $(frmname + " #WinTagbillable_"+wintagid).css("display","");
            $(frmname + " #WinTagbillable_"+wintagid).parent().append(notepart);
            jQuery(frmname + " .aCompletionStatus").unbind("click").bind("click",Me.woCompletionStatuslink_click);
            jQuery(frmname + " .cmdaddwintagnote").unbind("click").bind("click",Me.wocmdaddwintagnote_click);
        }
        else
        {
            $(frmname + " #WinTagbillable_"+wintagid).parent().find("img").remove();
            $(frmname + " #WinTagbillable_"+wintagid).parent().find("#wintagitemaddnotevalue_"+wintagid).remove();
            $(frmname + " #WinTagbillable_"+wintagid).html("");
            $(frmname + " #WinTagbillable_"+wintagid).css("display","none");
        }
    }

    //------------------------------------------------------------
    this.woRescheduledSiteClientCausedchange = function(frmname, chkitem, itemname, listname, linkname, divlistname)
    {
        if($(frmname + " " + chkitem).is(":checked"))
        {
            $(frmname + " " + itemname).css("display","");
            Me.IsShowAddcommandAndCombobox(frmname, itemname, listname, linkname, divlistname);
            jQuery(frmname + " " + listname).unbind("change").bind("change",Me.woRescheduledSiteClienttagsitemlist_change);
        }
        else
        {
            $(frmname + " " + linkname).css("display","none");
            $(frmname + " " + itemname).css("display","none");
            jQuery(frmname + " " + listname).unbind("change");
        }
    }
    
    //------------------------------------------------------------
    this.woRescheduledTechProviderCausedchange = function(frmname, chkitem, itemname, listname, linkname, divlistname)
    {
        if($(frmname + " " + chkitem).is(":checked"))
        {
            $(frmname + " " + itemname).css("display","");
            Me.IsShowAddcommandAndCombobox(frmname, itemname, listname, linkname, divlistname);
            jQuery(frmname + " " + listname).unbind("change").bind("change",Me.woRescheduledTechProviderTagsitemlist_change);
        }
        else
        {
            $(frmname + " " + linkname).css("display","none");
            $(frmname + " " + itemname).css("display","none");
            jQuery(frmname + " " + listname).unbind("change");
        }
    }
    
    //------------------------------------------------------------
    this.woRescheduledNaturalCausedchange = function(frmname, chkname)
    {
        var wintagid = $(frmname + " " + chkname).attr("wintagid");
        var tagstatus = " (<a class='aCompletionStatus' wintagid='"+wintagid+"' style='cursor:pointer;' title='Click to mark this WIN-Tag as a &lsquo;non-billable&rsquo; event'>billable</a>)<input type='hidden' id='CompletionStatustype_"+wintagid+"' name='cbxCompletionStatus_"+wintagid+"' value='2'/>";
        //847
        var notepart = " <img id='wintagitemaddnote_"+wintagid+"' wintagitemaddnotevalueid ='"+wintagid+"' style='cursor:pointer;' src='/widgets/images/Pencilicon.png' height='15px'  title='Add a note to this WIN-Tag' class='cmdaddwintagnote' strname='Rescheduled due to Natural Causes' /> <input type='hidden' id='wintagitemaddnotevalue_"+wintagid+"' name='wintagitemaddnotevalue"+wintagid+"' value=''/>";
        if($(frmname + " " + chkname).is(":checked"))
        {
            $(frmname + " #WinTagbillable_"+wintagid).html(tagstatus);
            $(frmname + " #WinTagbillable_"+wintagid).css("display","");
            $(frmname + " #WinTagbillable_"+wintagid).parent().append(notepart);
            jQuery(frmname + " .aCompletionStatus").unbind("click").bind("click",Me.woCompletionStatuslink_click);
            jQuery(frmname + " .cmdaddwintagnote").unbind("click").bind("click",Me.wocmdaddwintagnote_click);
        }
        else
        {
            $(frmname + " #WinTagbillable_"+wintagid).parent().find("img").remove();
            $(frmname + " #WinTagbillable_"+wintagid).parent().find("#wintagitemaddnotevalue_"+wintagid).remove();
            $(frmname + " #WinTagbillable_"+wintagid).html("");
            $(frmname + " #WinTagbillable_"+wintagid).css("display","none");
        }
    }

    //------------------------------------------------------------
    //Complete
    //------------------------------------------------------------
    this.chkCompleteGroup_change = function(){
        Me.CheckGroupchange('#frmWinTags', '#tableCompleteGroup', '#chkCompleteGroup');
    }
    
    //------------------------------------------------------------
    this.isCompleteGroupCheck = function(frmname)
    {
        var chk1stvisit = $(frmname +" #chk1stvisit").is(":checked");
        var chkReassigned = $(frmname +" #chkReassigned").is(":checked");
        var chkRevisitRequired = $(frmname +" #chkRevisitRequired").is(":checked");
        var chkRescheduled = $(frmname +" #chkRescheduled").is(":checked");
        
        if(chk1stvisit || chkReassigned || chkRevisitRequired || chkRescheduled)
        {
            $(frmname +" #chkCompleteGroup").attr("checked","true");
        }
        else
        {
            $(frmname +" #chkCompleteGroup").attr("checked","");
        }
    }
    //847
    this.loadnotepart = function(wintagid)
    {
        var el = "";
        var strname="";
        if(wintagid==25)
        {
            el="#chk1stvisit";
            strname="Complete &ndash; 1<sup>st</sup> Visit";
        }
        else if(wintagid==26)
        {
            el="#chkRevisitRequired";
            strname="Complete &ndash; Multi&ndash;Visit";
        }
        else if(wintagid==27)
        {
            el="#chkRescheduled";
            strname="Complete &ndash; Reassigned";
        }
        else if(wintagid==28)
        {
            el="#chkReassigned";
            strname="Complete &ndash; Rescheduled";
        }
    
        var notepart = " <img id='wintagitemaddnote_"+wintagid+"' wintagitemaddnotevalueid ='"+wintagid+"' style='cursor:pointer;' src='/widgets/images/Pencilicon.png' height='15px' title='Add a note to this WIN-Tag' class='cmdaddwintagnote' strname='"+strname+"' />\n\
                        <input type='hidden' id='wintagitemaddnotevalue_"+wintagid+"' name='wintagitemaddnotevalue"+wintagid+"' value=''/>";
        if($("#frmWinTags " + el).is(":checked"))
        {
            $("#frmWinTags #Completenote_"+wintagid).html(notepart);
            jQuery("#frmWinTags .cmdaddwintagnote").unbind("click").bind("click",Me.cmdaddwintagnote_click);
        }
        else
        {
            $("#frmWinTags #Completenote_"+wintagid).html("");
        }
    }
    
    this.woloadnotepart = function(wintagid)
    {
        var el = "";
        var strname="";
        if(wintagid==25)
        {
            el="#chk1stvisit";
            strname="Complete &ndash; 1<sup>st</sup> Visit";
        }
        else if(wintagid==26)
        {
            el="#chkRevisitRequired";
            strname="Complete &ndash; Multi&ndash;Visit";
        }
        else if(wintagid==27)
        {
            el="#chkRescheduled";
            strname="Complete &ndash; Reassigned";
        }
        else if(wintagid==28)
        {
            el="#chkReassigned";
            strname="Complete &ndash; Rescheduled";
        }
        
        var notepart = " <img id='wintagitemaddnote_"+wintagid+"' wintagitemaddnotevalueid ='"+wintagid+"' style='cursor:pointer;' src='/widgets/images/Pencilicon.png' height='15px' title='Add a note to this WIN-Tag' class='cmdaddwintagnote' strname='"+strname+"' />\n\
                        <input type='hidden' id='wintagitemaddnotevalue_"+wintagid+"' name='wintagitemaddnotevalue"+wintagid+"' value=''/>";
        
        if($(el).is(":checked"))
        {
            $("#Completenote_"+wintagid).html(notepart);
            jQuery(".cmdaddwintagnote").unbind("click").bind("click",Me.wocmdaddwintagnote_click);
        }
        else
        {
            $("#Completenote_"+wintagid).html("");
        }
    }
    this.EditWintagloadnotepart = function(wintagid)
    {
        var el = "";
        var strname="";
        if(wintagid==25)
        {
            el="#chk1stvisit";
            strname="Complete &ndash; 1<sup>st</sup> Visit";
        }
        else if(wintagid==26)
        {
            el="#chkRevisitRequired";
            strname="Complete &ndash; Multi&ndash;Visit";
        }
        else if(wintagid==27)
        {
            el="#chkRescheduled";
            strname="Complete &ndash; Reassigned";
        }
        else if(wintagid==28)
        {
            el="#chkReassigned";
            strname="Complete &ndash; Rescheduled";
        }
        
        var notepart = " <img id='wintagitemaddnote_"+wintagid+"' wintagitemaddnotevalueid ='"+wintagid+"' style='cursor:pointer;' src='/widgets/images/Pencilicon.png' height='15px' title='Add a note to this WIN-Tag' class='cmdaddwintagnote' strname='"+strname+"' />\n\
            <input type='hidden' id='wintagitemaddnotevalue_"+wintagid+"' name='wintagitemaddnotevalue"+wintagid+"' value=''/>";
        
        if($("#frmWinTagsEdit "+el).is(":checked"))
        {
            $("#frmWinTagsEdit #Completenote_"+wintagid).html(notepart);
            jQuery("#frmWinTagsEdit .cmdaddwintagnote").unbind("click").bind("click",Me.cmdaddwintagnoteEditForAddWinTag_click);
        }
        else
        {
            $("#Completenote_"+wintagid).html("");
        }
    }
    //end 847
    //------------------------------------------------------------
    this.chk1stvisit_click = function()
    {
        Me.isCompleteGroupCheck('#frmWinTags');
        Me.loadnotepart(25);
    }
    
    //------------------------------------------------------------
    this.chkReassigned_click = function()
    {
        Me.isCompleteGroupCheck('#frmWinTags');
        Me.loadnotepart(28);
    }
    
    //------------------------------------------------------------
    this.chkRevisitRequired_click = function()
    {
        Me.isCompleteGroupCheck('#frmWinTags');
        Me.loadnotepart(26);
    }
    
    //------------------------------------------------------------
    this.chkRescheduled_click = function()
    {
        Me.isCompleteGroupCheck('#frmWinTags');
        Me.loadnotepart(27);
    }
    
    //Incomplete
    //------------------------------------------------------------
    this.chkIncompleteGroup_change = function(){
        Me.CheckGroupchange('#frmWinTags', '#tableIncompleteGroup', '#chkIncompleteGroup');
        Me.IncompleteSiteClientCausedchange('#frmWinTags', '#chkIncompleteSiteClientCaused', '#IncompleteSiteClientCausedItems', '#Incompletetagsitemlist', '#IncompleteSiteClientAdd', '#divIncompletetagsitemlist');
        Me.IncompleteTechProviderCausedchange('#frmWinTags', '#chkIncompleteTechProviderCaused', '#IncompleteTechProviderCausedItems', '#IncompleteTechProvidertagsitemlist', '#IncompleteTechProviderAdd', '#divIncompleteTechProvidertagsitemlist');
        Me.IncompleteNaturalCausedchange('#frmWinTags', '#chkIncompleteNaturalCaused');
    }
    
    //Incomplete Site/Client Status
    //------------------------------------------------------------
    this.isIncompleteGroupCheck = function(frmname)
    {
        var chkIncompleteSiteClientCaused = $(frmname + " #chkIncompleteSiteClientCaused").is(":checked");
        var chkIncompleteTechProviderCaused = $(frmname + " #chkIncompleteTechProviderCaused").is(":checked");
        var chkIncompleteNaturalCaused = $(frmname + " #chkIncompleteNaturalCaused").is(":checked");
        if(chkIncompleteSiteClientCaused || chkIncompleteTechProviderCaused || chkIncompleteNaturalCaused)
        {
            $(frmname + " #chkIncompleteGroup").attr("checked","true");
        }
        else
        {
            $(frmname + " #chkIncompleteGroup").attr("checked","");
        }
    }
    
    //------------------------------------------------------------
    this.IncompleteSiteClientCausedStatuslink_click = function()
    {
        Me.loadStatuslink(this, "#frmWinTags","IncompleteSiteClient");
        jQuery("#frmWinTags .IncompleteSiteClientStatus").unbind("click").bind("click",Me.IncompleteSiteClientCausedStatuslink_click);
    }
    
    //------------------------------------------------------------
    this.chkIncompleteSiteClientCaused_change = function(){
        Me.IncompleteSiteClientCausedchange('#frmWinTags', '#chkIncompleteSiteClientCaused', '#IncompleteSiteClientCausedItems', '#Incompletetagsitemlist', '#IncompleteSiteClientAdd', '#divIncompletetagsitemlist');
        Me.isIncompleteGroupCheck('#frmWinTags');
    }
    
    //------------------------------------------------------------
    this.SiteClienttagsitemlist_change = function(){
        //847
        Me.loadTagItem ('Incompletetags', 'IncompleteSiteClient', 'IncompleteSiteClient', '#frmWinTags', 'Incomplete due to Site / Client');
        jQuery("#frmWinTags .cmdaddwintagnote").unbind("click").bind("click",Me.cmdaddwintagnote_click);
        //end 847
        jQuery("#frmWinTags .IncompleteSiteClientStatus").unbind("click").bind("click",Me.IncompleteSiteClientCausedStatuslink_click);
        jQuery("#frmWinTags #IncompleteSiteClientAdd").unbind("click").bind("click",Me.IncompleteSiteClientAdd_click);
        jQuery("#frmWinTags .cmdDeletewintag").unbind("click").bind("click",Me.DeleteWintag_click);
        
        Me.autoSize();
    }
    
    //------------------------------------------------------------
    this.IncompleteSiteClientAdd_click = function()
    {   
        Me.cmdAddTag("Incompletetagsitemlist", "divIncompletetagsitemlist", "IncompleteSiteClientCausedItems", "divIncompleteSiteClientAdd", "#frmWinTags");
    }
    
    //Incomplete Tech / Provider Caused Status
    //------------------------------------------------------------
    this.IncompleteTechProviderCausedStatuslink_click = function()
    {
        Me.loadStatuslink(this, "#frmWinTags","IncompleteTechProvider");
        jQuery("#frmWinTags .IncompleteTechProviderStatus").unbind("click").bind("click",Me.IncompleteTechProviderCausedStatuslink_click);
    }
    
    //------------------------------------------------------------
    this.chkIncompleteTechProviderCaused_change = function()
    {
        Me.IncompleteTechProviderCausedchange('#frmWinTags', '#chkIncompleteTechProviderCaused', '#IncompleteTechProviderCausedItems', '#IncompleteTechProvidertagsitemlist', '#IncompleteTechProviderAdd', '#divIncompleteTechProvidertagsitemlist');
        Me.isIncompleteGroupCheck('#frmWinTags');
    }
    
    //------------------------------------------------------------
    this.TechProviderTagsitemlist_change = function()
    {
        //847
        Me.loadTagItem('IncompleteTechProvidertags', 'IncompleteTechProvider', 'IncompleteTechProvider', '#frmWinTags', 'Incomplete due to Tech / Provider');
        jQuery("#frmWinTags .cmdaddwintagnote").unbind("click").bind("click",Me.cmdaddwintagnote_click);
        //end 847
        jQuery("#frmWinTags .cmdDeletewintag").unbind("click").bind("click",Me.DeleteWintag_click);
        jQuery("#frmWinTags .IncompleteTechProviderStatus").unbind("click").bind("click",Me.IncompleteTechProviderCausedStatuslink_click);
        jQuery("#frmWinTags #IncompleteTechProviderAdd").unbind("click").bind("click",Me.IncompleteTechProviderAdd_click);
    }
    
    //------------------------------------------------------------
    this.IncompleteTechProviderAdd_click=function()
    {
        Me.cmdAddTag("IncompleteTechProvidertagsitemlist", "divIncompleteTechProvidertagsitemlist", "IncompleteTechProviderCausedItems", "divIncompleteTechProviderAdd", '#frmWinTags');
    }
    
    //Incomplete Natural Caused
    //------------------------------------------------------------
    this.chkIncompleteNaturalCaused_change = function(){
        Me.IncompleteNaturalCausedchange('#frmWinTags', '#chkIncompleteNaturalCaused');
        Me.isIncompleteGroupCheck('#frmWinTags');
    }
    
    //Rescheduled
    //------------------------------------------------------------
    this.chkRescheduledGroup_change = function(){
        Me.CheckGroupchange('#frmWinTags', '#tableRescheduledGroup', '#chkRescheduledGroup');
        Me.RescheduledSiteClientCausedchange('#frmWinTags', '#chkRescheduledSiteClientCaused', '#RescheduledSiteClientCausedItems', '#RescheduledSiteClienttagsitemlist', '#divRescheduledSiteClientAdd', '#divRescheduledSiteClienttagsitemlist');
        Me.RescheduledTechProviderCausedchange('#frmWinTags', '#chkRescheduledTechProviderCaused', '#RescheduledTechProviderCausedItems', '#RescheduledTechProvidertagsitemlist', '#RescheduledTechProviderAdd', '#divRescheduledTechProvidertagsitemlist')
        Me.RescheduledNaturalCausedchange('#frmWinTags', '#chkRescheduledNaturalCaused');
    }
    
    //Rescheduled Site/Client Caused Status
    //------------------------------------------------------------
    this.isRescheduledGroupCheck = function(frmname)
    {
        var chkRescheduledSiteClientCaused = $(frmname + " #chkRescheduledSiteClientCaused").is(":checked");
        var chkRescheduledTechProviderCaused = $(frmname + " #chkRescheduledTechProviderCaused").is(":checked");
        var chkRescheduledNaturalCaused = $(frmname + " #chkRescheduledNaturalCaused").is(":checked");
        if(chkRescheduledSiteClientCaused || chkRescheduledTechProviderCaused || chkRescheduledNaturalCaused)
        {
            $(frmname + " #chkRescheduledGroup").attr("checked","true");
        }
        else
        {
            $(frmname + " #chkRescheduledGroup").attr("checked","");
        }
    }
    
    //------------------------------------------------------------
    this.chkRescheduledSiteClientCaused_change = function(){
        Me.RescheduledSiteClientCausedchange('#frmWinTags', '#chkRescheduledSiteClientCaused', '#RescheduledSiteClientCausedItems', '#RescheduledSiteClienttagsitemlist', '#divRescheduledSiteClientAdd', '#divRescheduledSiteClienttagsitemlist');
        Me.isRescheduledGroupCheck('#frmWinTags');
    }
    
    //------------------------------------------------------------
    this.RescheduledSiteClientCausedStatuslink_click = function()
    {
        Me.loadStatuslink(this, "#frmWinTags","RescheduledSiteClient");
        jQuery("#frmWinTags .RescheduledSiteClientStatus").unbind("click").bind("click",Me.RescheduledSiteClientCausedStatuslink_click);
    }
    
    //------------------------------------------------------------
    this.RescheduledSiteClienttagsitemlist_change = function(){
        //847
        Me.loadTagItem('RescheduledSiteClienttags', 'RescheduledSiteClient', 'RescheduledSiteClient', '#frmWinTags', 'Rescheduled due to Site / Client');
        jQuery("#frmWinTags .cmdaddwintagnote").unbind("click").bind("click",Me.cmdaddwintagnote_click);
        //end 847
        jQuery("#frmWinTags .RescheduledSiteClientStatus").unbind("click").bind("click",Me.RescheduledSiteClientCausedStatuslink_click);
        jQuery("#frmWinTags #RescheduledSiteClientAdd").unbind("click").bind("click",Me.RescheduledSiteClientAdd_click);
        jQuery("#frmWinTags .cmdDeletewintag").unbind("click").bind("click",Me.DeleteWintag_click);
        Me.autoSize();
    }
    
    //------------------------------------------------------------
    this.RescheduledSiteClientAdd_click = function()
    {   
        Me.cmdAddTag("RescheduledSiteClienttagsitemlist", "divRescheduledSiteClienttagsitemlist","RescheduledSiteClientCausedItems","divRescheduledSiteClientAdd", '#frmWinTags');
    }
    //Rescheduled Tech / Provider Caused Status
    //------------------------------------------------------------
    this.RescheduledTechProviderCausedStatuslink_click = function()
    {
        Me.loadStatuslink(this, "#frmWinTags","RescheduledTechProvider");
        jQuery("#frmWinTags .RescheduledTechProviderStatus").unbind("click").bind("click",Me.RescheduledTechProviderCausedStatuslink_click);
    }
    
    //------------------------------------------------------------
    this.chkRescheduledTechProviderCaused_change = function(){
        Me.RescheduledTechProviderCausedchange('#frmWinTags', '#chkRescheduledTechProviderCaused', '#RescheduledTechProviderCausedItems', '#RescheduledTechProvidertagsitemlist', '#RescheduledTechProviderAdd', '#divRescheduledTechProvidertagsitemlist')
        Me.isRescheduledGroupCheck('#frmWinTags');
    }

    //------------------------------------------------------------
    this.RescheduledTechProviderTagsitemlist_change = function(){
        //847
        Me.loadTagItem('RescheduledTechProvidertags', 'RescheduledTechProvider', 'RescheduledTechProvider', '#frmWinTags', 'Rescheduled due to Tech / Provider');
        jQuery("#frmWinTags .cmdaddwintagnote").unbind("click").bind("click",Me.cmdaddwintagnote_click);
        //end 847
        jQuery("#frmWinTags .RescheduledTechProviderStatus").unbind("click").bind("click",Me.RescheduledTechProviderCausedStatuslink_click);
        jQuery("#frmWinTags #RescheduledTechProviderAdd").unbind("click").bind("click",Me.RescheduledTechProviderAdd_click);
        jQuery("#frmWinTags .cmdDeletewintag").unbind("click").bind("click",Me.DeleteWintag_click);
    }
    
    //------------------------------------------------------------
    this.RescheduledTechProviderAdd_click=function()
    {
        Me.cmdAddTag("RescheduledTechProvidertagsitemlist","divRescheduledTechProvidertagsitemlist","RescheduledTechProviderCausedItems","divRescheduledTechProviderAdd", '#frmWinTags');
    }
    
    //------------------------------------------------------------
    this.chkRescheduledNaturalCaused_change = function(){
        
        Me.RescheduledNaturalCausedchange('#frmWinTags', '#chkRescheduledNaturalCaused');
        Me.isRescheduledGroupCheck('#frmWinTags');
    }
    
    //Cancelled Group
    //------------------------------------------------------------
    this.chkCancelledGroup_change = function(){
        var wintagid = $(this).attr("wintagid");
        var tagstatus = " (<a class='aCompletionStatus' wintagid='"+wintagid+"' style='cursor:pointer;' title='Click to mark this WIN-Tag as a &lsquo;non-billable&rsquo; event'>billable</a>)<input type='hidden' id='CompletionStatustype_"+wintagid+"' name='cbxCompletionStatus_"+wintagid+"' value='2'/>";
        var notepart = " <img id='wintagitemaddnote_"+wintagid+"' wintagitemaddnotevalueid ='"+wintagid+"' style='cursor:pointer;' src='/widgets/images/Pencilicon.png' height='15px'  title='Add a note to this WIN-Tag' class='cmdaddwintagnote' strname='Site Cancelled' /> <input type='hidden' id='wintagitemaddnotevalue_"+wintagid+"' name='wintagitemaddnotevalue"+wintagid+"' value=''/>";
        //847
        if($(this).is(":checked"))
        {
            $("#frmWinTags #WinTagbillable_"+wintagid).html(tagstatus);
            $("#frmWinTags #WinTagbillable_"+wintagid).css("display","");
            $("#frmWinTags #WinTagbillable_"+wintagid).parent().append(notepart);
            jQuery(".aCompletionStatus").unbind("click").bind("click",Me.CompletionStatuslink_click);
            jQuery("#frmWinTags .cmdaddwintagnote").unbind("click").bind("click",Me.cmdaddwintagnote_click);
        }
        else
        {
            $("#frmWinTags #WinTagbillable_"+wintagid).parent().html("Site Cancelled<span id='WinTagbillable_40'></div>");
            $("#frmWinTags #WinTagbillable_"+wintagid).html("");
            $("#frmWinTags #WinTagbillable_"+wintagid).css("display","none");
        }
    //end 847
    }

    //847
    //------------------------------------------------------------
    this.cmdaddwintagnote_click = function()
    {
        var note = $(this).attr('title');
        var wintagitemaddnotevalue = $(this).attr('wintagitemaddnotevalueid');
        var strname = $(this).attr('strname');
        
        var html = Me.cmdaddwintagnote(note, wintagitemaddnotevalue, strname,"cmdwintagSavenote", "cmdwintagCancelnote", "frmwintag");
        var opt = {};
        opt.width    = 400;
        opt.height   = '';
        opt.position = '';
        opt.title    = '';
        opt.body     = html;   
        opt.position = 'middle',
        opt.zindex = 99999;
        Me._roll.showNotAjax(this, this, opt);
        jQuery("#cmdwintagCancelnote").unbind("click",Me.cmdwintagCancelnote_onClick).bind("click",Me.cmdwintagCancelnote_onClick);
        jQuery("#cmdwintagSavenote").unbind("click",Me.cmdwintagSavenote_onClick).bind("click",Me.cmdwintagSavenote_onClick);
    }
    
    //------------------------------------------------------------
    this.cmdaddwintagnoteEditForAddWinTag_click = function()
    {
        var note = $(this).attr('title');
        var wintagitemaddnotevalue = $(this).attr('wintagitemaddnotevalueid');
        var strname = $(this).attr('strname');
        
        var html = Me.cmdaddwintagnote(note, wintagitemaddnotevalue, strname,"cmdwoSavenoteEditForAddWinTag","cmdwoCancelnoteEditForAddWinTag", "frmditwintagnote");    
        var opt = {};
        opt.width    = 400;
        opt.height   = '';
        opt.position = '';
        opt.title    = '';
        opt.body     = html;   
        opt.position = 'middle',
        opt.zindex = 99999;
        Me._roll1.showNotAjax(this, this, opt);
        var el = $("#ContainerID_frmditwintagnote").parent().parent().parent().find('.popup-header-button').attr("id");
        $("#"+el).unbind("click",Me.cmdCancelnoteEditForAddWinTag_onClick).bind("click",Me.cmdCancelnoteEditForAddWinTag_onClick);
        jQuery("#cmdwoCancelnoteEditForAddWinTag").unbind("click",Me.cmdCancelnoteEditForAddWinTag_onClick).bind("click",Me.cmdCancelnoteEditForAddWinTag_onClick);
        jQuery("#cmdwoSavenoteEditForAddWinTag").unbind("click",Me.cmdSavenoteEditForAddWinTag_onClick).bind("click",Me.cmdSavenoteEditForAddWinTag_onClick);
    }
    //------------------------------------------------------------
    this.cmdaddwintagnote = function(note, wintagitemaddnotevalue, strname, cmdsave, cmdcancel, frmname)
    {
        if(note =="Add a note to this WIN-Tag")
            note="";
        var html='<div>\n\
                    <div id="ContainerID_'+frmname+'" style="margin-top:-20px;">\n\
                        <table width="100%" cellspacing="2" cellpadding="0" border="0">\n\
                            <colgroup>\n\
                                <col width="60px">\n\
                                <col width="">\n\
                            </colgroup>\n\
                            <tbody>\n\
                                <tr>\n\
                                    <td colspan="2"><b>WIN-Tag Notes</b></td>\n\
                                </tr>\n\
                                <tr>\n\
                                    <td style="vertical-align:top;"><b>WIN-Tag</b>:</td>\n\
                                    <td>'+strname+'</td>\n\
                                </tr>\n\
                                <tr>\n\
                                    <td><b>Notes</b>:</td>\n\
                                    <td><input type="text" value="'+note+'" name="" id="note_'+wintagitemaddnotevalue+'" size="40"></td>\n\
                                </tr>\n\
                                <tr>\n\
                                    <td colspan="2">\n\
                                        <div style="text-align:center;padding-top:10px">\n\
                                            <input type="button" id="'+cmdcancel+'" class="link_button_popup" value="Cancel">\n\
                                            <input type="button" id="'+cmdsave+'" class="link_button_popup" value="OK" wintagitemaddnotevalue="'+wintagitemaddnotevalue+'">\n\
                                        </div>\n\
                                    </td>\n\
                                </tr>\n\
                            </tbody>\n\
                        </table>\n\
                    </div>';
        return html;
    }
    
    //------------------------------------------------------------
    this.cmdwintagCancelnote_onClick = function()
    {
        Me._roll.hide(); 
    }
    
    //------------------------------------------------------------
    this.cmdwintagSavenote_onClick = function(){
        var itemid = $(this).attr('wintagitemaddnotevalue');
        var note = $("#note_"+itemid).val();
        Me.cmdSavenote("#frmWinTags", itemid, note);
        Me.flash = true;
        Me._roll.hide(); 
    }
    //------------------------------------------------------------
    this.cmdCancelnoteEditForAddWinTag_onClick = function()
    {
        Me._roll1.hide(); 
    }
    
    //------------------------------------------------------------
    this.cmdSavenoteEditForAddWinTag_onClick = function(){
        var itemid = $(this).attr('wintagitemaddnotevalue');
        var note = $("#note_"+itemid).val();
        Me.cmdSavenoteEditForAddWinTag("#frmWinTagsEdit", itemid, note);
        Me.flash = true;
        Me._roll1.hide(); 
    }
    //------------------------------------------------------------
    this.cmdSavenote = function(frname, itemid, note){
        var PencilBubbleimg = "/widgets/images/PencilBubbleicon.png";
        var Pencilimg = "/widgets/images/Pencilicon.png"
        if(note=="")
        {
            $(frname+" #wintagitemaddnotevalue_"+itemid).val(note);
            $(frname+" #wintagitemaddnote_"+itemid).attr('title',"Add a note to this WIN-Tag");
            $(frname+" #wintagitemaddnote_"+itemid).attr('src',Pencilimg);
        }
        else
        {
            $(frname+" #wintagitemaddnotevalue_"+itemid).val(note);
            $(frname+" #wintagitemaddnote_"+itemid).attr('title',note);
            $(frname+" #wintagitemaddnote_"+itemid).attr('src',PencilBubbleimg);
        }
        
        Me.flash = true;
        Me._roll.hide(); 
    }
    //------------------------------------------------------------
    this.cmdSavenoteEditForAddWinTag = function(frname, itemid, note){
        var PencilBubbleimg = "/widgets/images/PencilBubbleicon.png";
        var Pencilimg = "/widgets/images/Pencilicon.png"
        if(note=="")
        {
            $(frname+" #wintagitemaddnotevalue_"+itemid).val(note);
            $(frname+" #wintagitemaddnote_"+itemid).attr('title',"Add a note to this WIN-Tag");
            $(frname+" #wintagitemaddnote_"+itemid).attr('src',Pencilimg);
        }
        else
        {
            $(frname+" #wintagitemaddnotevalue_"+itemid).val(note);
            $(frname+" #wintagitemaddnote_"+itemid).attr('title',note);
            $(frname+" #wintagitemaddnote_"+itemid).attr('src',PencilBubbleimg);
        }
        
        Me.flash = true;
        Me._roll1.hide(); 
    }
    //end 847
    
    //work order form
    //Complete
    //------------------------------------------------------------
    this.wochkCompleteGroup_change = function(){
        Me.CheckGroupchange('', '#tableCompleteGroup', '#chkCompleteGroup');
    }
    
    //------------------------------------------------------------
    this.wochk1stvisit_click = function()
    {
        Me.isCompleteGroupCheck('');
        Me.woloadnotepart(25);
    }
    
    //------------------------------------------------------------
    this.wochkReassigned_click = function()
    {
        Me.isCompleteGroupCheck('');
        Me.woloadnotepart(28);
    }
    
    //------------------------------------------------------------
    this.wochkRevisitRequired_click = function()
    {
        Me.isCompleteGroupCheck('');
        Me.woloadnotepart(26);
    }
    
    //------------------------------------------------------------
    this.wochkRescheduled_click = function()
    {
        Me.isCompleteGroupCheck('');
        Me.woloadnotepart(27);
    }
    
    //------------------------------------------------------------
    this.woopenSiteStatus_click = function()
    {
        var win = jQuery(this).attr("win");
        _global.data = "win="+win;
        _global.url = "/widgets/dashboard/popup/site-status";
        var _onSuccess = function(data){
            jQuery("#comtantPopupID").html(data);
            Me.buildEventFormSiteStatus();
            Me.autoSizeSiteStatus();
        }
        _global.onSuccess = _onSuccess;
        _global.showLoader(_global.width,_global.height,true);
        _global.show();
    }
    //13871
    //------------------------------------------------------------
    this.woopentechprofile_click = function()
    {
        var hrf = jQuery(this).attr("hrf");
        $("<div></div>").fancybox(
        {
            'autoDimensions' : false,
            'showCloseButton' :true,
            'width':_global.width,
            'height':_global.height,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'type':"iframe",
            'href':hrf+'&back=0&popup=1'
        }
        ).trigger('click');
        Me.flash = false;    
    }
    
    //871
    //------------------------------------------------------------
    this.woSATRecommended_change = function()
    {
        var itemid = $(this).val();
        $('#edit_wintagSATRecommended'+itemid).attr('checked','checked');
        $('#edit_SATRecommended'+itemid).attr('checked','checked');
    }
    //------------------------------------------------------------
    this.woSATPerformance_change = function()
    {
        var itemid = $(this).val();
        $('#edit_wintagSATPerformance'+itemid).attr('checked','checked');
        $('#edit_SATPerformance'+itemid).attr('checked','checked');
    }
    //end 871
    
    //Incomplete
    //------------------------------------------------------------
    this.wochkIncompleteGroup_change = function(){
        Me.CheckGroupchange('', '#tableIncompleteGroup', '#chkIncompleteGroup');
        Me.woIncompleteSiteClientCausedchange('', '#chkIncompleteSiteClientCaused', '#IncompleteSiteClientCausedItems', '#Incompletetagsitemlist', '#IncompleteSiteClientAdd', '#divIncompletetagsitemlist');
        Me.woIncompleteTechProviderCausedchange('', '#chkIncompleteTechProviderCaused', '#IncompleteTechProviderCausedItems', '#IncompleteTechProvidertagsitemlist', '#IncompleteTechProviderAdd', '#divIncompleteTechProvidertagsitemlist');
        Me.woIncompleteNaturalCausedchange('', '#chkIncompleteNaturalCaused');
    }
    
    //Incomplete Site/Client Caused Status
    //------------------------------------------------------------
    this.wochkIncompleteSiteClientCaused_change = function(){
        Me.woIncompleteSiteClientCausedchange('', '#chkIncompleteSiteClientCaused', '#IncompleteSiteClientCausedItems', '#Incompletetagsitemlist', '#IncompleteSiteClientAdd', '#divIncompletetagsitemlist');
        Me.isIncompleteGroupCheck('');
    }
    
    //------------------------------------------------------------
    this.woIncompleteSiteClientCausedStatuslink_click = function()
    {
        Me.loadStatuslink(this, "","IncompleteSiteClient");
        jQuery(".IncompleteSiteClientStatus").unbind("click").bind("click",Me.woIncompleteSiteClientCausedStatuslink_click);
    }
    
    //------------------------------------------------------------
    this.woSiteClienttagsitemlist_change = function(){
        //847
        Me.loadTagItem('Incompletetags', 'IncompleteSiteClient', 'IncompleteSiteClient', '', 'Incomplete due to Site / Client');
        jQuery(".cmdaddwintagnote").unbind("click").bind("click",Me.wocmdaddwintagnote_click);
        //847
        jQuery(".IncompleteSiteClientStatus").unbind("click").bind("click",Me.woIncompleteSiteClientCausedStatuslink_click);
        jQuery("#IncompleteSiteClientAdd").unbind("click").bind("click",Me.woIncompleteSiteClientAdd_click);
        jQuery(" .cmdDeletewintag").unbind("click").bind("click",Me.woDeleteWintag_click);
    }
    
    //------------------------------------------------------------
    this.woIncompleteSiteClientAdd_click = function()
    {   
        Me.cmdAddTag("Incompletetagsitemlist", "divIncompletetagsitemlist", "IncompleteSiteClientCausedItems", "divIncompleteSiteClientAdd", "");
    }
    
    //Incomplete Tech / Provider Caused Status
    //------------------------------------------------------------
    this.wochkIncompleteTechProviderCaused_change = function(){
        Me.woIncompleteTechProviderCausedchange('', '#chkIncompleteTechProviderCaused', '#IncompleteTechProviderCausedItems', '#IncompleteTechProvidertagsitemlist', '#IncompleteTechProviderAdd', '#divIncompleteTechProvidertagsitemlist');
        Me.isIncompleteGroupCheck('');
    }
    
    //------------------------------------------------------------
    this.woIncompleteTechProviderCausedStatuslink_click = function()
    {
        Me.loadStatuslink(this, "","IncompleteTechProvider");
        jQuery(".IncompleteTechProviderStatus").unbind("click").bind("click",Me.woIncompleteTechProviderCausedStatuslink_click);
    }
    
    //------------------------------------------------------------
    this.woTechProviderTagsitemlist_change = function(){
        //847
        Me.loadTagItem('IncompleteTechProvidertags', 'IncompleteTechProvider', 'IncompleteTechProvider', '', 'Incomplete due to Tech / Provider');
        jQuery(".cmdaddwintagnote").unbind("click").bind("click",Me.wocmdaddwintagnote_click);
        //end 847
        jQuery(".IncompleteTechProviderStatus").unbind("click").bind("click",Me.woIncompleteTechProviderCausedStatuslink_click);
        jQuery("#IncompleteTechProviderAdd").unbind("click").bind("click",Me.woIncompleteTechProviderAdd_click);
        jQuery(" .cmdDeletewintag").unbind("click").bind("click",Me.woDeleteWintag_click);
        Me.autoSize();
    }
    
    //------------------------------------------------------------
    this.woIncompleteTechProviderAdd_click=function()
    {
        Me.cmdAddTag("IncompleteTechProvidertagsitemlist", "divIncompleteTechProvidertagsitemlist", "IncompleteTechProviderCausedItems", "divIncompleteTechProviderAdd", '');
    }
    
    //------------------------------------------------------------
    this.wochkIncompleteNaturalCaused_change = function(){
        Me.woIncompleteNaturalCausedchange('', '#chkIncompleteNaturalCaused');
        Me.isIncompleteGroupCheck('');
    }
    
    //Rescheduled
    //------------------------------------------------------------
    this.wochkRescheduledGroup_change = function(){
        Me.CheckGroupchange('', '#tableRescheduledGroup', '#chkRescheduledGroup');
        Me.woRescheduledSiteClientCausedchange('', '#chkRescheduledSiteClientCaused', '#RescheduledSiteClientCausedItems', '#RescheduledSiteClienttagsitemlist', '#divRescheduledSiteClientAdd', '#divRescheduledSiteClienttagsitemlist');
        Me.woRescheduledTechProviderCausedchange('', '#chkRescheduledTechProviderCaused', '#RescheduledTechProviderCausedItems', '#RescheduledTechProvidertagsitemlist', '#RescheduledTechProviderAdd', '#divRescheduledTechProvidertagsitemlist');
        Me.woRescheduledNaturalCausedchange('', '#chkRescheduledNaturalCaused');
    }
    
    //Rescheduled Site/Client Caused Status
    //------------------------------------------------------------
    this.woRescheduledSiteClientCausedStatuslink_click = function()
    {
        Me.loadStatuslink(this, "","RescheduledSiteClient");
        jQuery(".RescheduledSiteClientStatus").unbind("click").bind("click",Me.woRescheduledSiteClientCausedStatuslink_click);
    }
    
    //------------------------------------------------------------
    this.wochkRescheduledSiteClientCaused_change = function(){
        Me.woRescheduledSiteClientCausedchange('', '#chkRescheduledSiteClientCaused', '#RescheduledSiteClientCausedItems', '#RescheduledSiteClienttagsitemlist', '#divRescheduledSiteClientAdd', '#divRescheduledSiteClienttagsitemlist');
        Me.isRescheduledGroupCheck('');
    }
    
    //------------------------------------------------------------
    this.woRescheduledSiteClienttagsitemlist_change = function(){
        //847
        Me.loadTagItem('RescheduledSiteClienttags', 'RescheduledSiteClient', 'RescheduledSiteClient', '', 'Rescheduled due to Site / Client');
        jQuery(".cmdaddwintagnote").unbind("click").bind("click",Me.wocmdaddwintagnote_click);
        //end 847
        jQuery(".RescheduledSiteClientStatus").unbind("click").bind("click",Me.woRescheduledSiteClientCausedStatuslink_click);
        jQuery("#RescheduledSiteClientAdd").unbind("click").bind("click",Me.woRescheduledSiteClientAdd_click);
        jQuery(" .cmdDeletewintag").unbind("click").bind("click",Me.woDeleteWintag_click);
    }

    //------------------------------------------------------------
    this.woRescheduledSiteClientAdd_click = function()
    {   
        Me.cmdAddTag("RescheduledSiteClienttagsitemlist", "divRescheduledSiteClienttagsitemlist","RescheduledSiteClientCausedItems","divRescheduledSiteClientAdd", '');
    }
    
    //Rescheduled Tech / Provider Caused Status
    ////------------------------------------------------------------
    this.woRescheduledTechProviderCausedStatuslink_click = function()
    {
        Me.loadStatuslink(this, "","RescheduledTechProvider");
        jQuery(".RescheduledTechProviderStatus").unbind("click").bind("click",Me.woRescheduledTechProviderCausedStatuslink_click);
    }
    
    //------------------------------------------------------------
    this.wochkRescheduledTechProviderCaused_change = function(){
        Me.woRescheduledTechProviderCausedchange('', '#chkRescheduledTechProviderCaused', '#RescheduledTechProviderCausedItems', '#RescheduledTechProvidertagsitemlist', '#RescheduledTechProviderAdd', '#divRescheduledTechProvidertagsitemlist');
        Me.isRescheduledGroupCheck('');
    }
    
    //------------------------------------------------------------
    this.woRescheduledTechProviderTagsitemlist_change = function(){
        //847
        Me.loadTagItem('RescheduledTechProvidertags', 'RescheduledTechProvider', 'RescheduledTechProvider', '', 'Rescheduled due to Tech / Provider');
        jQuery(".cmdaddwintagnote").unbind("click").bind("click",Me.wocmdaddwintagnote_click);
        //end 847
        jQuery(".RescheduledTechProviderStatus").unbind("click").bind("click",Me.woRescheduledTechProviderCausedStatuslink_click);
        jQuery("#RescheduledTechProviderAdd").unbind("click").bind("click",Me.woRescheduledTechProviderAdd_click);
        jQuery(" .cmdDeletewintag").unbind("click").bind("click",Me.woDeleteWintag_click);
    }
    
    //------------------------------------------------------------
    this.woRescheduledTechProviderAdd_click=function()
    {
        Me.cmdAddTag("RescheduledTechProvidertagsitemlist", "divRescheduledTechProvidertagsitemlist","RescheduledTechProviderCausedItems","divRescheduledTechProviderAdd", '');
    }
    
    //------------------------------------------------------------
    this.wochkRescheduledNaturalCaused_change = function(){
        Me.woRescheduledNaturalCausedchange('', '#chkRescheduledNaturalCaused');
        Me.isRescheduledGroupCheck('');
    }
    
    //Cancelled Group
    //847
    //------------------------------------------------------------
    this.wochkCancelledGroup_change = function(){
        var wintagid = $(this).attr("wintagid");
        var tagstatus = " (<a class='aCompletionStatus' wintagid='"+wintagid+"' style='cursor:pointer;' title='Click to mark this WIN-Tag as a &lsquo;non-billable&rsquo; event'>billable</a>)<input type='hidden' id='CompletionStatustype_"+wintagid+"' name='cbxCompletionStatus_"+wintagid+"' value='2'/>\n\
        <img id='wintagitemaddnote_"+wintagid+"' wintagitemaddnotevalueid ='"+wintagid+"' style='cursor:pointer;' src='/widgets/images/Pencilicon.png' height='15px'  title='Add a note to this WIN-Tag' class='cmdaddwintagnote' strname='Site Cancelled' /> <input type='hidden' id='wintagitemaddnotevalue_"+wintagid+"' name='wintagitemaddnotevalue"+wintagid+"' value=''/>";

        if($(this).is(":checked"))
        {
            $("#WinTagbillable_"+wintagid).html(tagstatus);
            $("#WinTagbillable_"+wintagid).css("display","");
            jQuery(".aCompletionStatus").unbind("click").bind("click",Me.woCompletionStatuslink_click);
            jQuery(".cmdaddwintagnote").unbind("click").bind("click",Me.wocmdaddwintagnote_click);
        }
        else
        {
            $("#WinTagbillable_"+wintagid).parent().html("Site Cancelled<span id='WinTagbillable_40'></div>");
            $("#WinTagbillable_"+wintagid).html("");
            $("#WinTagbillable_"+wintagid).css("display","none");
        }
    }
    
    //847
    //------------------------------------------------------------
    this.wocmdaddwintagnote_click = function()
    {
        var note = $(this).attr('title');
        var wintagitemaddnotevalue = $(this).attr('wintagitemaddnotevalueid');
        var strname = $(this).attr('strname');
        
        var html = Me.cmdaddwintagnote(note, wintagitemaddnotevalue, strname,"cmdwoSavenote","cmdwoCancelnote", "frmwo");    
        var opt = {};
        opt.width    = 400;
        opt.height   = '';
        opt.position = '';
        opt.title    = '';
        opt.body     = html;   
        opt.position = 'middle',
        opt.zindex = 99999;
        Me._roll.showNotAjax(this, this, opt);
        jQuery("#cmdwoCancelnote").unbind("click",Me.cmdwoCancelnote_onClick).bind("click",Me.cmdwoCancelnote_onClick);
        jQuery("#cmdwoSavenote").unbind("click",Me.cmdwoSavenote_onClick).bind("click",Me.cmdwoSavenote_onClick);
    }
    
    //------------------------------------------------------------
    this.cmdwoCancelnote_onClick = function()
    {
        Me._roll.hide();
    }
    
    //------------------------------------------------------------
    this.cmdwoSavenote_onClick = function(){
        var itemid = $(this).attr('wintagitemaddnotevalue');
        var note = $("#note_"+itemid).val();
        Me.cmdSavenote("", itemid, note);
        Me.flash = true;
        Me._roll.hide(); 
    }
    //end 847
    //edit wintag form
    //847
    //------------------------------------------------------------
    this.editIncompleteSiteClientCausedchange = function(frmname, chkitem, itemname, listname, linkname, divlistname)
        {
        if($(frmname + " " + chkitem).is(":checked"))
        {
            $(frmname + " " + itemname).css("display","");
            Me.IsShowAddcommandAndCombobox(frmname, itemname, listname, linkname, divlistname);
            jQuery(frmname + " " + listname).unbind("change").bind("change",Me.SiteClienttagsitemlistEditForAddWinTag_change);
        }
        else
        {
            $(frmname + " " + linkname).css("display","none");
            $(frmname + " " + itemname).css("display","none");
            jQuery(frmname + " " + listname).unbind("change");
        }
    }
    //end 847
    //------------------------------------------------------------
    this.editIncompleteTechProviderCausedchange = function(frmname, chkitem, itemname, listname, linkname, divlistname)
        {
        if($(frmname + " " + chkitem).is(":checked"))
        {
            $(frmname + " " + itemname).css("display","");
            Me.IsShowAddcommandAndCombobox(frmname, itemname, listname, linkname, divlistname);
            jQuery(frmname + " " + listname).unbind("change").bind("change",Me.TechProviderTagsitemlistEditForAddWinTag_change);
        }
        else
        {
            $(frmname + " " + linkname).css("display","none");
            $(frmname + " " + itemname).css("display","none");
            jQuery(frmname + " " + listname).unbind("change");
        }
    }
    
    //------------------------------------------------------------
    this.CompletionStatuslinkEditForAddWinTag_click = function()
    {
        var wintagid = $(this).attr("wintagid");
        var CompletionStatustype = $("#frmWinTagsEdit #CompletionStatustype_"+wintagid).val();
        var CompletionStatustypeval = 2;
        var CompletionStatustypetext = "";
        var CompletionStatustypetitle = "";
        
        if(CompletionStatustype==1)
        {
            CompletionStatustypeval=2;
            CompletionStatustypetext ="billable";
            CompletionStatustypetitle = "Click to mark this WIN-Tag as a &lsquo;non-billable&rsquo; event";
        }
        else
        {
            CompletionStatustypeval =1;
            CompletionStatustypetext ="no $";
            CompletionStatustypetitle = "Click to mark this WIN-Tag as a &lsquo;billable&rsquo; event";
        }
        
        var tagstatus = " (<a class='aCompletionStatus' wintagid='"+wintagid+"' style='cursor:pointer;' title='"+CompletionStatustypetitle+"'>"+CompletionStatustypetext+"</a>)<input type='hidden' id='CompletionStatustype_"+wintagid+"' name='cbxCompletionStatus_"+wintagid+"' value='"+CompletionStatustypeval+"'/>";
        $("#frmWinTagsEdit  #WinTagbillable_"+wintagid).html(tagstatus);
        $("#frmWinTagsEdit #WinTagbillable_"+wintagid).css("display","");
        jQuery("#frmWinTagsEdit .aCompletionStatus").unbind("click").bind("click",Me.CompletionStatuslinkEditForAddWinTag_click);
    }
    
    //------------------------------------------------------------
    this.editRescheduledSiteClientCausedchange = function(frmname, chkitem, itemname, listname, linkname, divlistname)
        {
        if($(frmname + " " + chkitem).is(":checked"))
        {
            $(frmname + " " + itemname).css("display","");
            Me.IsShowAddcommandAndCombobox(frmname, itemname, listname, linkname, divlistname);
            jQuery(frmname + " " + listname).unbind("change").bind("change",Me.RescheduledSiteClienttagsitemlistEditForAddWinTag_change);
        }
        else
        {
            $(frmname + " " + linkname).css("display","none");
            $(frmname + " " + itemname).css("display","none");
            jQuery(frmname + " " + listname).unbind("change");
        }
    }
    
    //------------------------------------------------------------
    this.editRescheduledTechProviderCausedchange = function(frmname, chkitem, itemname, listname, linkname, divlistname)
        {
        if($(frmname + " " + chkitem).is(":checked"))
        {
            $(frmname + " " + itemname).css("display","");
            Me.IsShowAddcommandAndCombobox(frmname, itemname, listname, linkname, divlistname);
            jQuery(frmname + " " + listname).unbind("change").bind("change",Me.RescheduledTechProviderTagsitemlistEditForAddWinTag_change);
        }
        else
        {
            $(frmname + " " + linkname).css("display","none");
            $(frmname + " " + itemname).css("display","none");
            jQuery(frmname + " " + listname).unbind("change");
        }
    }
    
    //------------------------------------------------------------
    this.editRescheduledNaturalCausedchange = function(frmname, chkname)
    {
        var wintagid = $(frmname + " " + chkname).attr("wintagid");
        var tagstatus = " (<a class='aCompletionStatus' wintagid='"+wintagid+"' style='cursor:pointer;' title='Click to mark this WIN-Tag as a &lsquo;non-billable&rsquo; event'>billable</a>)<input type='hidden' id='CompletionStatustype_"+wintagid+"' name='cbxCompletionStatus_"+wintagid+"' value='2'/>";
        //847
        var notepart = " <img id='wintagitemaddnote_"+wintagid+"' wintagitemaddnotevalueid ='"+wintagid+"' style='cursor:pointer;' src='/widgets/images/Pencilicon.png' height='15px'  title='Add a note to this WIN-Tag' class='cmdaddwintagnote' strname='Rescheduled due to Natural Causes' /> <input type='hidden' id='wintagitemaddnotevalue_"+wintagid+"' name='wintagitemaddnotevalue"+wintagid+"' value=''/>";
        if($(frmname + " " + chkname).is(":checked"))
        {
            $(frmname + " #WinTagbillable_"+wintagid).html(tagstatus);
            $(frmname + " #WinTagbillable_"+wintagid).css("display","");
            $(frmname + " #WinTagbillable_"+wintagid).parent().append(notepart);
            jQuery(frmname + " .aCompletionStatus").unbind("click").bind("click",Me.woCompletionStatuslink_click);
            jQuery(frmname + " .cmdaddwintagnote").unbind("click").bind("click",Me.cmdaddwintagnoteEditForAddWinTag_click);
        }
        else
        {
            $(frmname + " #WinTagbillable_"+wintagid).parent().find("img").remove();
            $(frmname + " #WinTagbillable_"+wintagid).parent().find("#wintagitemaddnotevalue_"+wintagid).remove();
            $(frmname + " #WinTagbillable_"+wintagid).html("");
            $(frmname + " #WinTagbillable_"+wintagid).css("display","none");
        }
        
    }
    
    //------------------------------------------------------------
    this.editIncompleteNaturalCausedchange = function(frmname, chkname)
    {
        var wintagid = $(frmname + " " + chkname).attr("wintagid");
        var tagstatus = " (<a class='aCompletionStatus' wintagid='"+wintagid+"' style='cursor:pointer;' title='Click to mark this WIN-Tag as a &lsquo;non-billable&rsquo; event'>billable</a>)<input type='hidden' id='CompletionStatustype_"+wintagid+"' name='cbxCompletionStatus_"+wintagid+"' value='2'/>";
        //847
        var notepart = " <img id='wintagitemaddnote_"+wintagid+"' wintagitemaddnotevalueid ='"+wintagid+"' style='cursor:pointer;' src='/widgets/images/Pencilicon.png' height='15px'  title='Add a note to this WIN-Tag' class='cmdaddwintagnote' strname='Incomplete due to Natural Causes' /> <input type='hidden' id='wintagitemaddnotevalue_"+wintagid+"' name='wintagitemaddnotevalue"+wintagid+"' value=''/>";
        if($(frmname + " " + chkname).is(":checked"))
            {
            $(frmname + " #WinTagbillable_"+wintagid).html(tagstatus);
            $(frmname + " #WinTagbillable_"+wintagid).css("display","");
            $(frmname + " #WinTagbillable_"+wintagid).parent().append(notepart);
            jQuery(frmname + " .aCompletionStatus").unbind("click").bind("click",Me.CompletionStatuslinkEditForAddWinTag_click);
            jQuery(frmname + " .cmdaddwintagnote").unbind("click").bind("click",Me.cmdaddwintagnoteEditForAddWinTag_click);
        }
        else
        {
            $(frmname + " #WinTagbillable_"+wintagid).parent().find("img").remove();
            $(frmname + " #WinTagbillable_"+wintagid).parent().find("#wintagitemaddnotevalue_"+wintagid).remove();
            $(frmname + " #WinTagbillable_"+wintagid).html("");
            $(frmname + " #WinTagbillable_"+wintagid).css("display","none");
            }
    }

    //Complete groupe 
    //------------------------------------------------------------
    this.chkCompleteGroupEditForAddWinTag_change = function()
    {
        Me.CheckGroupchange('#frmWinTagsEdit', '#tableCompleteGroup', '#chkCompleteGroup');
            }
    
    //------------------------------------------------------------
    this.chk1stvisitEditForAddWinTag_click = function()
    {
        Me.isCompleteGroupCheck('#frmWinTagsEdit');
        Me.EditWintagloadnotepart(25);
    }
    
    //------------------------------------------------------------
    this.chkReassignedEditForAddWinTag_click = function()
    {
        Me.isCompleteGroupCheck('#frmWinTagsEdit');
        Me.EditWintagloadnotepart(28);
    }
    
    //------------------------------------------------------------
    this.chkRevisitRequiredEditForAddWinTag_click = function()
    {
        Me.isCompleteGroupCheck('#frmWinTagsEdit');
        Me.EditWintagloadnotepart(26);
    }
    
    //------------------------------------------------------------
    this.chkRescheduledEditForAddWinTag_click = function()
    {
        Me.isCompleteGroupCheck('#frmWinTagsEdit');
        Me.EditWintagloadnotepart(27);
    }
    
    //incomplete group
    //------------------------------------------------------------
    this.chkIncompleteGroupEditForAddWinTag_change = function(){
        Me.CheckGroupchange('#frmWinTagsEdit', '#tableIncompleteGroup', '#chkIncompleteGroup');
        Me.editIncompleteSiteClientCausedchange('#frmWinTagsEdit', '#chkIncompleteSiteClientCaused', '#IncompleteSiteClientCausedItems', '#Incompletetagsitemlist', '#IncompleteSiteClientAdd', '#divIncompletetagsitemlist');
        Me.editIncompleteTechProviderCausedchange('#frmWinTagsEdit', '#chkIncompleteTechProviderCaused', '#IncompleteTechProviderCausedItems', '#IncompleteTechProvidertagsitemlist', '#IncompleteTechProviderAdd', '#divIncompleteTechProvidertagsitemlist');
        Me.editIncompleteNaturalCausedchange('#frmWinTagsEdit', '#chkIncompleteNaturalCaused');
    }
    
    //Incomplete Site/Client Caused Status
    //------------------------------------------------------------
    this.chkIncompleteSiteClientCausedEditForAddWinTag_change = function(){
        Me.editIncompleteSiteClientCausedchange('#frmWinTagsEdit', '#chkIncompleteSiteClientCaused', '#IncompleteSiteClientCausedItems', '#Incompletetagsitemlist', '#IncompleteSiteClientAdd', '#divIncompletetagsitemlist');
        Me.isIncompleteGroupCheck('#frmWinTagsEdit');
    }
    
    //------------------------------------------------------------
    this.SiteClienttagsitemlistEditForAddWinTag_change = function(){
        //847
        Me.loadTagItem('Incompletetags','IncompleteSiteClient','IncompleteSiteClient', '#frmWinTagsEdit','Incomplete due to Site / Client')
        jQuery("#frmWinTagsEdit .cmdaddwintagnote").unbind("click").bind("click",Me.cmdaddwintagnoteEditForAddWinTag_click);
        //end 847
        jQuery("#frmWinTagsEdit .IncompleteSiteClientStatus").unbind("click").bind("click",Me.IncompleteSiteClientCausedStatuslinkEditForAddWinTag_click);
        jQuery("#frmWinTagsEdit #IncompleteSiteClientAdd").unbind("click").bind("click",Me.IncompleteSiteClientAddEditForAddWinTag_click);
        jQuery("#frmWinTagsEdit .cmdDeletewintag").unbind("click").bind("click",Me.DeleteWintagEditForAddWinTag_click);
        Me.frmWinTags_change();
    }

    //------------------------------------------------------------
    this.IncompleteSiteClientAddEditForAddWinTag_click = function()
    {   
        Me.cmdAddTag("Incompletetagsitemlist", "divIncompletetagsitemlist","IncompleteSiteClientCausedItems", "divIncompleteSiteClientAdd", "#frmWinTagsEdit");
    }
    
    //------------------------------------------------------------
    this.IncompleteSiteClientCausedStatuslinkEditForAddWinTag_click = function()
    {
        Me.loadStatuslink(this, "#frmWinTagsEdit","IncompleteSiteClient");
        jQuery("#frmWinTagsEdit .IncompleteSiteClientStatus").unbind("click").bind("click",Me.IncompleteSiteClientCausedStatuslinkEditForAddWinTag_click);
        Me.frmWinTags_change();
    }
    
    //Incomplete Tech / Provider Caused Status
    //------------------------------------------------------------
    this.chkIncompleteTechProviderCausedEditForAddWinTag_change = function(){
        Me.editIncompleteTechProviderCausedchange('#frmWinTagsEdit', '#chkIncompleteTechProviderCaused', '#IncompleteTechProviderCausedItems', '#IncompleteTechProvidertagsitemlist', '#IncompleteTechProviderAdd', '#divIncompleteTechProvidertagsitemlist');
        Me.isIncompleteGroupCheck('#frmWinTagsEdit');
    }
    
    //------------------------------------------------------------
    this.TechProviderTagsitemlistEditForAddWinTag_change = function()
    {
        //847
        Me.loadTagItem ('IncompleteTechProvidertags', 'IncompleteTechProvider', 'IncompleteTechProvider', '#frmWinTagsEdit','Incomplete due to Tech / Provider');
        jQuery("#frmWinTagsEdit .cmdaddwintagnote").unbind("click").bind("click",Me.cmdaddwintagnoteEditForAddWinTag_click);
        //end 847
        jQuery("#frmWinTagsEdit .IncompleteTechProviderStatus").unbind("click").bind("click",Me.IncompleteTechProviderCausedStatuslinkEditForAddWinTag_click);
        jQuery("#frmWinTagsEdit #IncompleteTechProviderAdd").unbind("click").bind("click",Me.IncompleteTechProviderAddEditForAddWinTag_click);
        jQuery("#frmWinTagsEdit .cmdDeletewintag").unbind("click").bind("click",Me.DeleteWintagEditForAddWinTag_click);
        Me.frmWinTags_change();
    }
    
    //------------------------------------------------------------
    this.IncompleteTechProviderAddEditForAddWinTag_click=function()
    {
        Me.cmdAddTag("IncompleteTechProvidertagsitemlist", "divIncompleteTechProvidertagsitemlist", "IncompleteTechProviderCausedItems", "divIncompleteTechProviderAdd", "#frmWinTagsEdit");
    }
    
    //------------------------------------------------------------
    this.IncompleteTechProviderCausedStatuslinkEditForAddWinTag_click = function()
    {
        Me.loadStatuslink(this, "#frmWinTagsEdit ","IncompleteTechProvider");
        jQuery("#frmWinTagsEdit .IncompleteTechProviderStatus").unbind("click").bind("click",Me.IncompleteTechProviderCausedStatuslinkEditForAddWinTag_click);
        Me.frmWinTags_change();
    }
    
    //------------------------------------------------------------
    this.chkIncompleteNaturalCausedEditForAddWinTag_change = function(){
        Me.editIncompleteNaturalCausedchange('#frmWinTagsEdit', '#chkIncompleteNaturalCaused');
        Me.isIncompleteGroupCheck('#frmWinTagsEdit');
    }
    
    //Rescheduled group
    //------------------------------------------------------------
    this.chkRescheduledGroupEditForAddWinTag_change = function(){
        Me.CheckGroupchange('#frmWinTagsEdit', '#tableRescheduledGroup', '#chkRescheduledGroup');
        Me.editRescheduledSiteClientCausedchange('#frmWinTagsEdit', '#chkRescheduledSiteClientCaused', '#RescheduledSiteClientCausedItems', '#RescheduledSiteClienttagsitemlist', '#divRescheduledSiteClientAdd', '#divRescheduledSiteClienttagsitemlist');
        Me.editRescheduledTechProviderCausedchange('#frmWinTagsEdit', '#chkRescheduledTechProviderCaused', '#RescheduledTechProviderCausedItems', '#RescheduledTechProvidertagsitemlist', '#RescheduledTechProviderAdd', '#divRescheduledTechProvidertagsitemlist')
        Me.editRescheduledNaturalCausedchange('#frmWinTagsEdit', '#chkRescheduledNaturalCaused');
    }
    
    //Rescheduled Site/Client Caused Status
    //------------------------------------------------------------
    this.chkRescheduledSiteClientCausedEditForAddWinTag_change = function(){
        Me.editRescheduledSiteClientCausedchange('#frmWinTagsEdit', '#chkRescheduledSiteClientCaused', '#RescheduledSiteClientCausedItems', '#RescheduledSiteClienttagsitemlist', '#divRescheduledSiteClientAdd', '#divRescheduledSiteClienttagsitemlist');
        Me.isRescheduledGroupCheck('#frmWinTagsEdit');
    }
    
    //------------------------------------------------------------
    this.RescheduledSiteClienttagsitemlistEditForAddWinTag_change = function(){
        //end 847
        Me.loadTagItem('RescheduledSiteClienttags','RescheduledSiteClient','RescheduledSiteClient', '#frmWinTagsEdit','Rescheduled due to Site / Client');
        jQuery("#frmWinTagsEdit .cmdaddwintagnote").unbind("click").bind("click",Me.cmdaddwintagnoteEditForAddWinTag_click);
        //end 847
        jQuery("#frmWinTagsEdit .RescheduledSiteClientStatus").unbind("click").bind("click",Me.RescheduledSiteClientCausedStatuslinkEditForAddWinTag_click);
        jQuery("#frmWinTagsEdit #RescheduledSiteClientAdd").unbind("click").bind("click",Me.RescheduledSiteClientAddEditForAddWinTag_click);
        jQuery("#frmWinTagsEdit .cmdDeletewintag").unbind("click").bind("click",Me.DeleteWintagEditForAddWinTag_click);
        Me.frmWinTags_change();
    }
    
    //------------------------------------------------------------
    this.RescheduledSiteClientAddEditForAddWinTag_click = function()
    {   
        Me.cmdAddTag("RescheduledSiteClienttagsitemlist", "divRescheduledSiteClienttagsitemlist", "RescheduledSiteClientCausedItems", "divRescheduledSiteClientAdd", "#frmWinTagsEdit");
    }
    
    //------------------------------------------------------------
    this.RescheduledSiteClientCausedStatuslinkEditForAddWinTag_click = function()
    {
        Me.loadStatuslink(this, "#frmWinTagsEdit","RescheduledSiteClient");
        jQuery("#frmWinTagsEdit .RescheduledSiteClientStatus").unbind("click").bind("click",Me.RescheduledSiteClientCausedStatuslinkEditForAddWinTag_click);
        Me.frmWinTags_change();
    }
    
    //Rescheduled Tech / Provider Caused Status
    //------------------------------------------------------------
    this.chkRescheduledTechProviderCausedEditForAddWinTag_change = function(){
        Me.editRescheduledTechProviderCausedchange('#frmWinTagsEdit', '#chkRescheduledTechProviderCaused', '#RescheduledTechProviderCausedItems', '#RescheduledSiteClienttagsitemlist', '#RescheduledTechProviderAdd', '#divRescheduledTechProvidertagsitemlist');
        Me.isRescheduledGroupCheck('#frmWinTagsEdit');
    }
    
    //------------------------------------------------------------
    this.RescheduledTechProviderTagsitemlistEditForAddWinTag_change = function(){
        //847
        Me.loadTagItem('RescheduledTechProvidertags','RescheduledTechProvider','RescheduledTechProvider', '#frmWinTagsEdit','Rescheduled due to Tech / Provider');
        jQuery("#frmWinTagsEdit .cmdaddwintagnote").unbind("click").bind("click",Me.cmdaddwintagnoteEditForAddWinTag_click);
        //end 847
        jQuery("#frmWinTagsEdit .RescheduledTechProviderStatus").unbind("click").bind("click",Me.RescheduledTechProviderCausedStatuslinkEditForAddWinTag_click);
        jQuery("#frmWinTagsEdit #RescheduledTechProviderAdd").unbind("click").bind("click",Me.RescheduledTechProviderAddEditForAddWinTag_click);
        jQuery("#frmWinTagsEdit .cmdDeletewintag").unbind("click").bind("click",Me.DeleteWintagEditForAddWinTag_click);
        Me.frmWinTags_change();
    }
    
    //------------------------------------------------------------
    this.RescheduledTechProviderAddEditForAddWinTag_click=function()
    {
        Me.cmdAddTag("RescheduledTechProvidertagsitemlist", "divRescheduledTechProvidertagsitemlist", "RescheduledTechProviderCausedItems", "divRescheduledTechProviderAdd", "#frmWinTagsEdit");
    }
    
    //------------------------------------------------------------
    this.RescheduledTechProviderCausedStatuslinkEditForAddWinTag_click = function()
    {
        Me.loadStatuslink(this, "#frmWinTagsEdit","RescheduledTechProvider");
        jQuery("#frmWinTagsEdit .RescheduledTechProviderStatus").unbind("click").bind("click",Me.RescheduledTechProviderCausedStatuslinkEditForAddWinTag_click);
        Me.frmWinTags_change();
    }
    
    //------------------------------------------------------------
    this.chkRescheduledNaturalCausedEditForAddWinTag_change = function(){
        Me.editRescheduledNaturalCausedchange('#frmWinTagsEdit', '#chkRescheduledNaturalCaused');
        Me.isRescheduledGroupCheck('#frmWinTagsEdit');
    }
    
    
    //Cancelled Group
    //------------------------------------------------------------
    this.chkCancelledGroupEditForAddWinTag_change = function(){
        var wintagid = $(this).attr("wintagid");
        var tagstatus = " (<a class='aCompletionStatus' wintagid='"+wintagid+"' style='cursor:pointer;' title='Click to mark this WIN-Tag as a &lsquo;non-billable&rsquo; event'>billable</a>)<input type='hidden' id='CompletionStatustype_"+wintagid+"' name='cbxCompletionStatus_"+wintagid+"' value='2'/>";
        //847
        var notepart = " <img id='wintagitemaddnote_"+wintagid+"' wintagitemaddnotevalueid ='"+wintagid+"' style='cursor:pointer;' src='/widgets/images/Pencilicon.png' height='15px'  title='Add a note to this WIN-Tag' class='cmdaddwintagnote' strname='Site Cancelled' /> <input type='hidden' id='wintagitemaddnotevalue_"+wintagid+"' name='wintagitemaddnotevalue"+wintagid+"' value=''/>";
        if($(this).is(":checked"))
        {
            $("#frmWinTagsEdit #WinTagbillable_"+wintagid).html(tagstatus);
            $("#frmWinTagsEdit #WinTagbillable_"+wintagid).css("display","");
            $("#frmWinTagsEdit #WinTagbillable_"+wintagid).parent().append(notepart);
            jQuery("#frmWinTagsEdit .aCompletionStatus").unbind("click").bind("click",Me.CompletionStatuslinkEditForAddWinTag_click);
            jQuery("#frmWinTagsEdit .cmdaddwintagnote").unbind("click").bind("click",Me.cmdaddwintagnoteEditForAddWinTag_click);
        }
        else
        {
            $("#frmWinTagsEdit #WinTagbillable_"+wintagid).parent().find("img").remove();
            $("#frmWinTagsEdit #WinTagbillable_"+wintagid).parent().find("#wintagitemaddnotevalue_"+wintagid).remove();
            $("#frmWinTagsEdit #WinTagbillable_"+wintagid).html("");
            $("#frmWinTagsEdit #WinTagbillable_"+wintagid).css("display","none");
        }
        //847
    }
    //end
    
    //------------------------------------------------------------
    this.loadStatuslink = function(el, frmname, pre)
    {
        var wintagid = $(el).attr("wintagid");
        var currTagNo = $(el).attr(pre + "No");
        var value_id = $(el).attr("value_id");
        var attvalue_id="";
        var wintagitemdeletevalueid ="";
        if(typeof(value_id)!="undefined")
        {
            wintagitemdeletevalueid = "<input type='hidden' name='wintagitemvalueid[]' value='" + value_id + "'/><input type='hidden' id='wintagitemdeletevalueid_" + value_id + "' wintagitemdeletevalueid name='wintagitemdeletevalueid[]' value=''/>";
            attvalue_id=" value_id ='"+value_id+"'";
        }
        
        var typeNo = $(frmname + " #" + pre + "No_"+ currTagNo + " #" + pre + "Statustype_"+wintagid).val();
        var typeval = 2;
        var text = "";
        var title = "";
        
        if(typeNo==1)
        {
            typeval=2;
            text ="billable";
            title = "Click to mark this WIN-Tag as a &lsquo;non-billable&rsquo; event";
        }
        else
        {
            typeval =1;
            text ="no $";
            title = "Click to mark this WIN-Tag as a &lsquo;billable&rsquo; event";
        }
        
        var tagstatus = " (<a "+ attvalue_id +" class='" + pre + "Status' " + pre + "No='"+currTagNo+"' wintagid='"+wintagid+"' style='cursor:pointer;' title='"+title+"'>"+text+"</a>)\n\
        <input type='hidden' id='" + pre + "Statustype_"+wintagid+"' name='CompletionStatusval[]' value='"+typeval+"'/>\n\
        <input type='hidden' name='wintagid[]' value='"+wintagid+"'/>"+wintagitemdeletevalueid;
        
        $(frmname + " #" + pre + "No_"+currTagNo).html(tagstatus);
        $(frmname + " #" + pre + "No_"+currTagNo).css("display","");
    }
    
    //end 686
    this.CompletionStatuscheckEditForAddWinTag_click = function()
    {
        var wintagid = $(this).val();
        var SiteStatus = $(this).attr('SiteStatus');
        var tagstatus = "";
        if( $("#frmWinTagsEdit #chk1stvisit").is(":checked") || 
            $("#frmWinTagsEdit #chkRevisitRequired").is(":checked") ||
            $("#frmWinTagsEdit #chkReassigned").is(":checked") ||
            $("#frmWinTagsEdit #chkRescheduled").is(":checked")
            )
            {
            tagstatus ="";
        }
        else
        {
            if(SiteStatus!=2)
            {
                tagstatus = " (<a class='aCompletionStatus' SiteStatus ='"+SiteStatus+"' wintagid='"+wintagid+"' style='cursor:pointer;' title='Click to mark this WIN-Tag as a &lsquo;non-billable&rsquo; event'>billable</a>)<input type='hidden' id='CompletionStatustype_"+wintagid+"' name='cbxCompletionStatus_"+wintagid+"' value='2'/>";
            }

            if($(this).is(":checked"))
            {
                $("#frmWinTagsEdit #WinTagbillable_"+wintagid).html(tagstatus);
                $("#frmWinTagsEdit #WinTagbillable_"+wintagid).css("display","");
                jQuery("#frmWinTagsEdit .aCompletionStatus").unbind("click").bind("click",Me.CompletionStatuslinkEditForAddWinTag_click);
            }
            else
            {
                $("#frmWinTagsEdit #WinTagbillable_"+wintagid).html("");
                $("#frmWinTagsEdit #WinTagbillable_"+wintagid).css("display","none");
            }
        }
    }
    
    //------------------------------------------------------------
    this.init = function() {
        jQuery(".cmdOpenWinTabs").unbind("click",Me.cmdOpenWinTabs_click).bind("click",Me.cmdOpenWinTabs_click);
        jQuery("#cbxCompletionStatusMain").unbind("change",Me.cbxCompletionStatusMain).bind("change",Me.cbxCompletionStatusMain);
        jQuery(".cmdDeleteWinTagsCompletionStatusMain").unbind("click").bind("click",Me.cmdDeleteWinTagsCompletionStatusMain);
        jQuery(".CompletionStatuscheck").unbind("click").bind("click",Me.woCompletionStatuscheck_click);
        jQuery(".aCompletionStatus").unbind("click").bind("click",Me.woCompletionStatuslink_click);
        //686
        jQuery("#chkCompleteGroup").unbind("click").bind("click",Me.wochkCompleteGroup_change);
        jQuery("#chkIncompleteGroup").unbind("click").bind("click",Me.wochkIncompleteGroup_change);
        jQuery("#chkRescheduledGroup").unbind("click").bind("click",Me.wochkRescheduledGroup_change);
        jQuery("#chkCancelledGroup").unbind("click").bind("click",Me.wochkCancelledGroup_change);
        
        jQuery("#chk1stvisit").unbind("click").bind("click",Me.wochk1stvisit_click);
        jQuery("#chkReassigned").unbind("click").bind("click",Me.wochkReassigned_click);
        jQuery("#chkRevisitRequired").unbind("click").bind("click",Me.wochkRevisitRequired_click);
        jQuery("#chkRescheduled").unbind("click").bind("click",Me.wochkRescheduled_click);
        
        
        
        jQuery("#chkIncompleteSiteClientCaused").unbind("click").bind("click",Me.wochkIncompleteSiteClientCaused_change);
        jQuery("#chkIncompleteTechProviderCaused").unbind("click").bind("click",Me.wochkIncompleteTechProviderCaused_change);
        jQuery("#chkIncompleteNaturalCaused").unbind("click").bind("click",Me.wochkIncompleteNaturalCaused_change);
        
        jQuery("#chkRescheduledSiteClientCaused").unbind("click").bind("click",Me.wochkRescheduledSiteClientCaused_change);
        jQuery("#chkRescheduledTechProviderCaused").unbind("click").bind("click",Me.wochkRescheduledTechProviderCaused_change);
        jQuery("#chkRescheduledNaturalCaused").unbind("click").bind("click",Me.wochkRescheduledNaturalCaused_change);
        
        jQuery("#IncompleteSiteClientAdd").unbind("click").bind("click",Me.woIncompleteSiteClientAdd_click);
        jQuery("#Incompletetagsitemlist").unbind("change").bind("change",Me.woSiteClienttagsitemlist_change);
        jQuery(".IncompleteSiteClientStatus").unbind("click").bind("click",Me.woIncompleteSiteClientCausedStatuslink_click);
        
        jQuery("#IncompleteTechProviderAdd").unbind("click").bind("click",Me.woIncompleteTechProviderAdd_click);
        jQuery("#IncompleteTechProvidertagsitemlist").unbind("change").bind("change",Me.woTechProviderTagsitemlist_change);
        jQuery(".IncompleteTechProviderStatus").unbind("click").bind("click",Me.woIncompleteTechProviderCausedStatuslink_click);
        
        jQuery("#RescheduledSiteClientAdd").unbind("click").bind("click",Me.woRescheduledSiteClientAdd_click);
        jQuery("#RescheduledSiteClienttagsitemlist").unbind("change").bind("change",Me.woRescheduledSiteClienttagsitemlist_change);
        jQuery(".RescheduledSiteClientStatus").unbind("click").bind("click",Me.woRescheduledSiteClientCausedStatuslink_click);
        
        jQuery("#RescheduledTechProviderAdd").unbind("click").bind("click",Me.woRescheduledTechProviderAdd_click);
        jQuery("#RescheduledTechProvidertagsitemlist").unbind("change").bind("change",Me.woRescheduledTechProviderTagsitemlist_change);
        jQuery(".RescheduledTechProviderStatus").unbind("click").bind("click",Me.woRescheduledTechProviderCausedStatuslink_click);
        
        jQuery(".cmdDeletewintag").unbind("click").bind("click",Me.woDeleteWintag_click);
        //end 686
        //847
        jQuery(".cmdaddwintagnote").unbind("click").bind("click",Me.wocmdaddwintagnote_click);
        //end 874
        Me.width = _global.width;
        Me.height= _global.height;
        Me.buildEventSiteStatus();
    }
}
