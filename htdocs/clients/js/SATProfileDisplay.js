function blankToZero(num) {
	if (num == "") num = 0;
	return num;
}

function SATProfileDisplay() {
	recommendedAvg = $("#SATRecommendedAvg").html();
	recommendedTotal = $("#SATRecommendedTotal").html();
	performanceAvg = $("#SATPerformanceAvg").html();
	performanceTotal = $("#SATPerformanceTotal").html();
	
	recommendedAvg = parseFloat(blankToZero(recommendedAvg));
	recommendedTotal = blankToZero(recommendedTotal);
	performanceAvg = parseFloat(blankToZero(performanceAvg));
	performanceTotal = blankToZero(performanceTotal);

	$("#SATProfileBox").html("<div>Preference and Performance Ratings by Clients</div><div style=\"position:relative; left: 15px\">Preference (Likelihood to Recommend): <span style='font-weight: normal'>" + recommendedAvg.toFixed(0) + "% - " + recommendedTotal + " ratings</span><br/><br/>" + "Performance (Greatly Exceeds Expectations):  <span style='font-weight: normal'>" + performanceAvg.toFixed(0) + "% - " + performanceTotal + " ratings</span></div><br/><br/>").show();
}