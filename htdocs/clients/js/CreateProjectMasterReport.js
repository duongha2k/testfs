//-------------------------------------------------------------
//-------------------------------------------------------------
CreateProjectMasterReport.Instances = null;
//-------------------------------------------------------------
CreateProjectMasterReport.CreateObject = function(config) {
    var obj = null;
    if (CreateProjectMasterReport.Instances != null) {
        obj = CreateProjectMasterReport.Instances[config.id];
    }
    if (obj == null) {
        if (CreateProjectMasterReport.Instances == null) {
            CreateProjectMasterReport.Instances = new Object();
        }
        obj = new CreateProjectMasterReport(config);
        CreateProjectMasterReport.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function CreateProjectMasterReport(config) {
    var Me = this;
    this.id = config.id;   
    this.ContainerID=config.ContainerID;
    this.CompanyID=config.CompanyID;
    this.Params=config.Params;
    //------------------------------------------------------------
    this.LoadingAjax = function(ContentId){
        jQuery(ContentId).html("<center><img src='/widgets/images/wait.gif' border='0' /></center>");
    }
    //---------------------------------------------------------------------
    this.btnRunReport_onClick=function(){       
        var listProjectIDSelected="";
        jQuery("#ListProject .classProjectItems:checked").each(function(){
            listProjectIDSelected+=jQuery(this).attr("id")+",";
        })
        listProjectIDSelected=listProjectIDSelected.replace(/chk/g,"");
        if(listProjectIDSelected.lenght>0)
            listProjectIDSelected=listProjectIDSelected.substr(0,listProjectIDSelected.length-1);
                
        Me.LoadingAjax(Me.ContainerID);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/reports/expense-reimbursement-report",
            data: {
                v:Me.CompanyID,
                ProjectIDs:'"'+listProjectIDSelected+'"'
            },
            success:function( html ) {
                jQuery(Me.ContainerID).html(html);                 
            }
        });
        
    }
    //---------------------------------------------------------------------
    this.BindEvent=function(){
        jQuery(".MasterItemsProject")
        .unbind("click", Me.MasterItemsProject_onClick)
        .bind("click", Me.MasterItemsProject_onClick);
        jQuery(".classProjectItems")
        .unbind("click", Me.classProjectItems_onClick)
        .bind("click", Me.classProjectItems_onClick);
    /*
        jQuery("#btnRunReport")
            .unbind("click", Me.btnRunReport_onClick)
            .bind("click", Me.btnRunReport_onClick);*/
        
    }
    //---------------------------------------------------------------------
    this.getdataCreateProjectMasterReport = function()
    {
        Me.LoadingAjax(Me.ContainerID);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/reports/create-project-master-report",
            data: {
                v:Me.CompanyID,
                Params:Me.Params
            },
			error: function (a, b, c) {
				//console (a, b, c);
			},
            success:function( html ) {
                jQuery(Me.ContainerID).html(html);  
                $('#StartDateCreatedDateRange').calendar({
                    dateFormat: 'MDY/'
                });
                $('#EndDateCreatedDateRange').calendar({
                    dateFormat: 'MDY/'
                });                
                $("#cbxCreatedDateRange").change(function(){ 
                    filterDate("#cbxCreatedDateRange",'#StartDateCreatedDateRange','#EndDateCreatedDateRange');
                })

                $('#StartDateStartDateRange').calendar({
                    dateFormat: 'MDY/'
                });
                $('#EndDateStartDateRange').calendar({
                    dateFormat: 'MDY/'
                });                
                $("#cbxStartDateRange").change(function(){ 
                    filterDate("#cbxStartDateRange",'#StartDateStartDateRange','#EndDateStartDateRange');
                })
                
                $('#StartDateEndDateRange').calendar({
                    dateFormat: 'MDY/'
                });
                $('#EndDateEndDateRange').calendar({
                    dateFormat: 'MDY/'
                });
                $("#cbxEndDateRange").change(function(){ 
                    filterDate("#cbxEndDateRange",'#StartDateEndDateRange','#EndDateEndDateRange');
                })

                $('#StartDateApprovedDateRange').calendar({
                    dateFormat: 'MDY/'
                });
                $('#EndDateApprovedDateRange').calendar({
                    dateFormat: 'MDY/'
                });
                $("#cbxApprovedDateRange").change(function(){ 
                    filterDate("#cbxApprovedDateRange",'#StartDateApprovedDateRange','#EndDateApprovedDateRange');
                })

                $('#StartDateInvoicedDateRange').calendar({
                    dateFormat: 'MDY/'
                });
                $('#EndDateInvoicedDateRange').calendar({
                    dateFormat: 'MDY/'
                });
                $("#cbxInvoicedDateRange").change(function(){ 
                    filterDate("#cbxInvoicedDateRange",'#StartDateInvoicedDateRange','#EndDateInvoicedDateRange');
                })
                
                jQuery("#StartDateCreatedDateRange").watermark("MM/DD/YYYY");
                jQuery("#EndDateCreatedDateRange").watermark("MM/DD/YYYY");
                jQuery("#StartDateStartDateRange").watermark("MM/DD/YYYY");
                jQuery("#EndDateStartDateRange").watermark("MM/DD/YYYY");
                jQuery("#StartDateEndDateRange").watermark("MM/DD/YYYY");
                jQuery("#EndDateEndDateRange").watermark("MM/DD/YYYY");
                jQuery("#StartDateApprovedDateRange").watermark("MM/DD/YYYY");
                jQuery("#EndDateApprovedDateRange").watermark("MM/DD/YYYY");
                jQuery("#StartDateInvoicedDateRange").watermark("MM/DD/YYYY");
                jQuery("#EndDateInvoicedDateRange").watermark("MM/DD/YYYY");
                
                
                Me.InitPage();
                Me.BindEvent();
            }
        });
    }
    //------------------------------------------------------------
    this.MasterItemsProject_onClick=function(){
        var isCheck=jQuery(this).is(":checked");        
        jQuery("#ListProject .classProjectItems").each(function(){
            jQuery(this).attr("checked",isCheck);
        })
    }
    //------------------------------------------------------------
    this.classProjectItems_onClick=function(){
        var numItems= jQuery("#ListProject .classProjectItems").length;
        var numItemsChecked= jQuery("#ListProject .classProjectItems:checked").length;
        if(numItemsChecked==numItems)
            jQuery(".MasterItemsProject").attr("checked",true);
        else
            jQuery(".MasterItemsProject").attr("checked",false);
        
            
        
    }
    //------------------------------------------------------------
    this.InitPage=function(){
        if(Me.Params!="")
        {          
            var objParams= eval("("+ Base64.decode(decodeURIComponent(Me.Params))+")");            
            jQuery("#cbxEndDateRange"+" option[value=\""+objParams.cbxEndDateRange+"\"]").attr("selected","selected");
            jQuery("#StartDateEndDateRange").val(objParams.StartDateEndDateRange);
            jQuery("#EndDateEndDateRange").val(objParams.EndDateEndDateRange);
        
            jQuery("#cbxStartDateRange"+" option[value=\""+objParams.cbxStartDateRange+"\"]").attr("selected","selected");
            jQuery("#StartDateStartDateRange").val(objParams.StartDateStartDateRange);
            jQuery("#EndDateStartDateRange").val(objParams.EndDateStartDateRange);
                
            jQuery("#txtClientWOID").val(objParams.txtClientWOID);
            jQuery("#txtFSTechID").val(objParams.txtFSTechID);
            jQuery("#txtContractorID").val(objParams.txtContractorID);
            jQuery("#ClientCredential"+" option[value=\""+objParams.ClientCredential+"\"]").attr("selected","selected");
                       
            if(typeof(objParams.chkProjectList)!="undefined")
            {
                var  arrProjectIDs=eval(objParams.chkProjectList);
           
                var lenghtArr=arrProjectIDs.length;
                var i=0;
                for( i=0;i<lenghtArr;i++)
                {                
                    jQuery("#ListProject .classProjectItems[value=\""+arrProjectIDs[i]+"\"]").each(function(){
                        jQuery(this).attr("checked",true);
                    })
                }
            }
            else                
            {
                jQuery("#ListProject .classProjectItems").each(function(){
                    jQuery(this).attr("checked",false);
                })
            }
            var numCheckBoxProjectID= jQuery("#ListProject .classProjectItems").length;
            var numCheckBoxProjectIDChecked=jQuery("#ListProject .classProjectItems:checked").length;
            
            if(numCheckBoxProjectID==numCheckBoxProjectIDChecked)
            {
                jQuery("#chkAllProject").attr("checked",true);
            }
            else
            {
                jQuery("#chkAllProject").removeAttr("checked");
            }
           
        }
        else
        {
            var isCheck=jQuery(".MasterItemsProject").is(":checked");
            jQuery("#ListProject .classProjectItems").each(function(){
                jQuery(this).attr("checked",isCheck);
            })
        }
        
    }
    //------------------------------------------------------------
    this.init = function() {
        jQuery(document).ready(function(){  
            Me.getdataCreateProjectMasterReport();
        })
        
        
    }

}