<?php
require_once("../library/caspioAPI.php");
//ini_set("display_errors",1);

class flsActivityReport {

	var $StartDate; // = "01/01/1901";
	var $EndDate; // = "12/31/2099";
	
	var $totalWOs;
	var $sourced;
	var $pcntSourced;
	var $unSourced;
	var $pcntUnsourced;
	var $complete;
	var $pcntComplete;	
	var $Incomplete;
	var $pcntIncomplete;	
	var $kickBack;
	var $pcntKickedBack;
	var $declined;
	var $pcntDeclined;
	var $deactivated;
	var $pcntDeactivated;
	var $late;
	var $noshow;
	var $shortNotice;
	var $notSourced14;
	var $newTechsAdded;
	var $totalPaid;
	var $totalPay;
	var $avgPay; // Approved WO's Only
	var $totalHours;
	var $avgHours; // Approved WO's Only
	var $totalOOS;
	var $pcntOOS;  // Total wos, total oos 
	var $projectList_QS; // Project List Query String (ProjectNo = '1' OR ProjectNo = '2', etc)
	var $sourcedByList_QS; // SourcedBy List Query String (SourcedBy = 'Gerald' OR SourcedBy = 'Trung', etc)
	var $stateList_QS;
	
	public function __construct()
	{
		$this->setDateRange("01-01-1901", "12-31-2099");
		//$ProjectList = array();
		$this->getProjects($companyID);
		$this->getSourcedBy($companyID);
		$this->getStates();
	//	$this->getWorkOrders();
	}
	
	
//	flsActivityReport::getProjects();
	//$projectReport = new flsActivityReport();
	//$projectReport->setProjects("");
		
	function setDateRange($start, $end)
	{
		$d = date_parse_from_format("m-d-Y", $start);
		if ($d["error_count"] == 0)
			$this->StartDate = $d["year"] . "-" . $d["month"] . "-" . $d["day"];
		else
			$this->StartDate = "1901-01-01";
		$d = date_parse_from_format("m-d-Y", $end);
		if ($d["error_count"] == 0)
			$this->EndDate = $d["year"] . "-" . $d["month"] . "-" . $d["day"];
		else
			$this->EndDate = "2099-12-31";
	}
	
	

	function getProjects($companyID)
	{
		
		$ProjectList = array();
		
		$db = Zend_Registry::get("DB");
		$ProjectList = $db->fetchPairs("SELECT Project_ID, Project_Name FROM projects WHERE " . $db->quoteInto("Project_Company_ID = ?", $companyID) . " ORDER BY Project_Name ASC", Zend_Db::FETCH_NUM);
					  
//		$projects = caspioSelectAdv("TR_Client_Projects", "Project_Name, Project_ID", "Project_Company_ID = '$companyID'", "Project_Name ASC", false, "`", "|^",false);
		
/*		foreach ($projects as $fields)
		{
			$ProjectList[$fields[1]] = $fields[0];
		}*/
		//if(!in_array("", $ProjectList))
		//	$ProjectList[0] = "BLANK";
	//	print_r($ProjectList);
//		$this->projects_to_qs($ProjectList);
		return $ProjectList; // returns ProjectList Array
	}
	
	function getCoordinators($companyID, $showGPM = false)
	{
		
		$CoordList = array();
		
		$db = Zend_Registry::get("DB");
		if (!$showGPM)
			$CoordList = $db->fetchPairs("SELECT UserName, ContactName FROM clients WHERE " . $db->quoteInto("Company_ID = ?", $companyID) . " ORDER BY ContactName ASC", Zend_Db::FETCH_NUM);
		else
			$CoordList = $db->fetchPairs("SELECT UserName, ContactName FROM clients WHERE " . $db->quoteInto("Company_ID = ?", $companyID) . " OR Company_ID = 'FS' ORDER BY ContactName ASC", Zend_Db::FETCH_NUM);
		
//		$projects = caspioSelectAdv("TR_Client_Projects", "Project_Name, Project_ID", "Project_Company_ID = '$companyID'", "Project_Name ASC", false, "`", "|^",false);
		
/*		foreach ($projects as $fields)
		{
			$ProjectList[$fields[1]] = $fields[0];
		}*/
		//if(!in_array("", $ProjectList))
		//	$ProjectList[0] = "BLANK";
	//	print_r($ProjectList);
//		$this->projects_to_qs($ProjectList);
		return $CoordList; // returns ProjectList Array
	}


	function setProjects($ProjectList)
	{
		$this->projects_to_qs($ProjectList);
	}

			
	function projects_to_qs($ProjectList)
	{
		if(sizeof($ProjectList) > 0) {
			$this->projectList_QS = "Project_ID IN (" . implode(",", array_keys($ProjectList)) . ")"; //. " OR IFNULL(Project_ID, '') = ''" ;
		} 
/*		else {
		
			$project = implode(array_keys($ProjectList));
		
			if($project == 0) {
				$this->projectList_QS = "Project_ID = '' OR IFNULL(Project_ID, '') = ''";		
			} else {
				$this->projectList_QS = "Project_ID = '" . implode(array_keys($ProjectList)) . "'";		
			}
		}*/

//		echo "<p>" . $this->projectList_QS . "</p><br><br>";
	}

	
	function getSourcedBy($companyID)
	{
		
		$SourcedByList = array();

		$db = Zend_Registry::get("DB");
//		echo "SELECT DISTINCT t.Username FROM timestamps AS t LEFT JOIN work_orders AS w ON t.WIN_NUM = w.WIN_NUM WHERE " . $db->quoteInto("Company_ID = ?", $companyID) . " AND t.Description = 'Work Order Assigned' ORDER BY t.Username ASC";
		$records = $db->fetchCol("SELECT DISTINCT t.Username FROM timestamps AS t LEFT JOIN work_orders AS w ON t.WIN_NUM = w.WIN_NUM WHERE " . $db->quoteInto("Company_ID = ?", $companyID) . " AND t.Description = 'Work Order Assigned' ORDER BY t.Username ASC", Zend_Db::FETCH_NUM);

//		$records = caspioSelectAdv("Work_Order_Timestamps", "DISTINCT Username", "Company_ID = '$companyID' AND CAST(Description as varchar) = 'Work Order Assigned'", "Username ASC", false, "`", "|^",false);
		foreach ($records as $fields)
		{
			if( $fields == "NULL")
				 $fields = "";
			if(!in_array($fields, $SourcedByList))
				$SourcedByList[] = $fields;
		}
//		$this->sourcedBy_to_qs($SourcedByList);
		return $SourcedByList; // returns SourcedByList Array
	}


	function setSourcedBy($SourcedByList)
	{
		$this->sourcedBy_to_qs($SourcedByList);
	}


	function sourcedBy_to_qs($SourcedByList)
	{
	
//		$this->sourcedByList_QS = "ISNULL((SELECT TOP 1 Username FROM Work_Order_Timestamps WHERE WO_UNID = TB_UNID AND CAST(Description as varchar) = 'Work Order Assigned' ORDER BY DateTime_Stamp DESC), '') IN ('". implode("', '", $SourcedByList) . "')";
	
		if(sizeof($SourcedByList) > 0) {
//				$this->sourcedByList_QS = "IFNULL((SELECT t.Username FROM timestamps AS t WHERE t.WIN_NUM = w.WIN_NUM AND t.Description = 'Work Order Assigned' ORDER BY DateTime_Stamp DESC LIMIT 1), '') IN ('". implode("', '", $SourcedByList) . "', '', 'NULL')";
				$this->sourcedByList_QS = "t.Username IN ('". implode("', '", $SourcedByList) . "')";
				
		} 
/*		else {
			if($SourcedByList[0] == '') {
				 $this->sourcedByList_QS = "RecruitedBy = '" . $SourcedByList[0] . "' OR ISNULL(RecruitedBy, '') = ''";
			} else {
				$this->sourcedByList_QS = "IFNULL((SELECT t.Username FROM timestamps AS t  WHERE t.WIN_NUM = w.WIN_NUM AND t.Description = 'Work Order Assigned' AND t.Username = '$SourcedByList[0]' ORDER BY DateTime_Stamp DESC LIMIT 1), '') IN ('". implode("', '", $SourcedByList) . "')";
//				echo "<p>". $SourcedByList[0] ."</p>";

				// $this->sourcedByList_QS = "RecruitedBy = '" . $SourcedByList[0] . "'";	
				
			}	
		}*/
	}

	
	function getStates()
	{
		
		$StatesList = array();

		$db = Zend_Registry::get("DB");
		$StatesList = $db->fetchPairs("SELECT  Abbreviation, State FROM states_list", Zend_Db::FETCH_NUM);

/*		$records = caspioSelectAdv("States_List", "State, Abbreviation", "", "State", false, "`", "|^",false);
		foreach ($records as $record)
		{
			$fields = explode("|^", $record);
			$StatesList[trim($fields[1], "`")] = trim($fields[0], "`");
		}*/
				
		//$this->states_to_qs($StatesList);
		return $StatesList; // returns StatesList Array
	}


	function setState($StatesList)
	{
		$this->states_to_qs($StatesList);
	}


	function states_to_qs($StatesList)
	{
		if(sizeof($StatesList) > 0) {
			$this->stateList_QS = "State = '" . implode("' OR State = '", array_keys($StatesList)) . "'"; // OR IFNULL(State, '') = ''";
		} 
/*		else {
			$state = implode(array_keys($StatesList));
			if($state == "") {
					$this->stateList_QS = "State = '' OR IFNULL(State, '') = ''";		
		//	echo "<p>State = " . $this->stateList_QS . "</p>";
			} else {
				$this->stateList_QS = "State = '" . $state . "'";		
			}
		}*/
	}

	function getWorkOrders($companyID, $Coordinators = NULL)
	{
		$countCriteria = array(
			"Sourced" => "IFNULL(w.Tech_ID,'') != ''",
			"Unsourced" => "IFNULL(w.Tech_ID,'') = '' AND Deactivated = 0",
			"Completed" => "TechMarkedComplete = 1",
			"KickedBack" => "DeactivationCode = 'Work order kicked back'",
			"Declined" => "DeactivationCode = 'Work order declined'",
			"Deactivated" => "Deactivated = 1",
			"LateArrival" => "Time_In > StartTime",
			"NoShows" => "IFNULL(NoShow_Tech, '') != ''",
			"ShortNotice" => "ShortNotice = 1",
//			"OOS" => ($companyID == "FTXS" ? "FLS_OOS" : "Work_Out_of_Scope") . " = 1 AND Deactivated = 0 AND DeactivationCode != 'Work order declined' AND DeactivationCode != 'Work order kicked back'",
			"OOS" => ($companyID == "FTXS" ? "FLS_OOS" : "Work_Out_of_Scope") . " = 1 AND Deactivated = 0",
//			"Incomplete" => "TechMarkedComplete = 0 AND Deactivated = 0 AND DeactivationCode != 'Work order declined' AND DeactivationCode != 'Work order kicked back'",
			"Incomplete" => "TechMarkedComplete = 0 AND Deactivated = 0",
//			"Paid" => "TechPaid = 1 AND Deactivated = 0 AND DeactivationCode != 'Work order declined' AND DeactivationCode != 'Work order kicked back'"
			"Paid" => "TechPaid = 1 AND Deactivated = 0"
		);
		
		$sumCriteria = array(
//			"Hours" => array("calculatedTechHrs", "TechPaid = 1 AND Deactivated = 0 AND DeactivationCode != 'Work order declined' AND DeactivationCode != 'Work order kicked back'"),
			"Hours" => array("calculatedTechHrs", "TechPaid = 1 AND Deactivated = 0"),
//			"PaidSum" => array("PayAmount", "TechPaid = 1 AND Deactivated = 0 AND DeactivationCode != 'Work order declined' AND DeactivationCode != 'Work order kicked back'")
			"PaidSum" => array("PayAmount", "TechPaid = 1 AND Deactivated = 0")
		);
		
		$aggergateQuery = array("COUNT(w.WIN_NUM) AS Total");
		
		foreach ($countCriteria as $key => $crit) {
			$aggergateQuery[] = "COUNT((CASE WHEN ($crit) THEN 1 ELSE NULL END)) AS $key";
		}
	
		foreach ($sumCriteria as $key => $crit) {
			$aggergateQuery[] = "SUM((CASE WHEN ({$crit[1]}) THEN {$crit[0]} ELSE NULL END)) AS $key";
		}

		$db = Zend_Registry::get("DB");

//		echo "SELECT " . implode(",", $aggergateQuery) . " FROM work_orders AS w " . (!empty($this->sourcedByList_QS)  ? "LEFT JOIN timestamps t ON (t.WIN_NUM = w.WIN_NUM AND t.Description = 'Work Order Assigned' AND DateTime_Stamp = (SELECT MAX(j.DateTime_Stamp) FROM timestamps AS j WHERE w.WIN_NUM = j.WIN_NUM AND j.Description = 'Work Order Assigned'))" : "") . " WHERE " . $db->quoteInto("StartDate >= ?", $this->StartDate) . " AND " . $db->quoteInto("StartDate <= ?", $this->EndDate) . (!empty($this->projectList_QS) ? " AND (" . $this->projectList_QS . ")" : "") . (!empty($this->sourcedByList_QS) ? " AND (" . $this->sourcedByList_QS . " )" : "") . (!empty($this->stateList_QS) ? " AND (" . $this->stateList_QS . ")" : "") . " AND " . $db->quoteInto("Company_ID = ?", $companyID);
		$records = $db->fetchRow("SELECT " . implode(",", $aggergateQuery) . " FROM work_orders AS w " . (!empty($this->sourcedByList_QS)  ? "LEFT JOIN timestamps t ON (t.WIN_NUM = w.WIN_NUM AND t.Description = 'Work Order Assigned' AND DateTime_Stamp = (SELECT MAX(j.DateTime_Stamp) FROM timestamps AS j WHERE w.WIN_NUM = j.WIN_NUM AND j.Description = 'Work Order Assigned'))" : "") . " WHERE " . $db->quoteInto("StartDate >= ?", $this->StartDate) . " AND " . $db->quoteInto("StartDate <= ?", $this->EndDate) . (!empty($this->projectList_QS) ? " AND (" . $this->projectList_QS . ")" : "") . (!empty($this->sourcedByList_QS) ? " AND (" . $this->sourcedByList_QS . " )" : "") . (!empty($this->stateList_QS) ? " AND (" . $this->stateList_QS . ")" : "") . (!empty($Coordinators) ? " AND (UserName IN('" . implode("','",$Coordinators). "'))" : "") . " AND " . $db->quoteInto("Company_ID = ?", $companyID)) ;

//		print_r($records);


		// Total WO's
		$TotalWOs = $records["Total"];
		$this->totalWOs = number_format($TotalWOs,0);
		
		if($TotalWOs > 0)
		{
		
			// Total Sourced
			$Sourced = $records["Sourced"];
			$this->sourced = number_format($Sourced,0);
			$this->pcntSourced = sprintf("%01.1f", round($Sourced / $TotalWOs, 3)*100) . "%"; 

			// Total Unsourced
			$UnSourced = $records["Unsourced"];
			$this->unSourced = number_format($UnSourced,0);
			$this->pcntUnsourced = sprintf("%01.1f", round($UnSourced / $TotalWOs, 3)*100) . "%";
			
			// Total Complete
			$Complete = $records["Completed"];
			$this->complete = number_format($Complete,0);
			$this->pcntComplete = sprintf("%01.1f", round($Complete / $TotalWOs, 3)*100) . "%";
			
			// Total Kicked Back
			$this->kickBack = number_format($records["KickedBack"],0);
			$this->pcntKickedBack = sprintf("%01.1f", round($records["KickedBack"] / $TotalWOs, 3)*100)  . "%";
			
			// Total Declined
			$this->declined = number_format($records["Declined"],0);
			$this->pcntDeclined = sprintf("%01.1f", round($records["Declined"] / $TotalWOs, 3)*100)  . "%";
			
			// Total Deactivated						
			$this->deactivated = number_format($records["Deactivated"],0);
			$this->pcntDeactivated = sprintf("%01.1f", round($records["Deactivated"] / $TotalWOs, 3)*100)  . "%";
				
			// Total Late Arrival
			$this->late = number_format($records["LateArrival"],0);
			
			// Total NoShows
			$this->noshow = number_format($records["NoShows"],0);
			
			// Total Short Notice
			$this->shortNotice = number_format($records["ShortNotice"],0);
			
			// Total Not Sournced in 14 Days from today (today + 14 days)
			// Doesn't make sense to me if running report for prev months
			$Today = date("m-d-Y");
			$FutureDate = date("m-d-Y", mktime(0, 0, 0, date("m"), date("d")+14, date("Y")));
			
			$notSource14 = $db->fetchOne("SELECT COUNT(w.WIN_NUM) FROM work_orders AS w " . (!empty($this->sourcedByList_QS) ? "LEFT JOIN timestamps t ON (t.WIN_NUM = w.WIN_NUM AND t.Description = 'Work Order Assigned' AND DateTime_Stamp = (SELECT MAX(j.DateTime_Stamp) FROM timestamps AS j WHERE w.WIN_NUM = j.WIN_NUM AND j.Description = 'Work Order Assigned'))" : "") . " WHERE " . $db->quoteInto("StartDate >= ?", $Today) . " AND " . $db->quoteInto("StartDate <= ?", $FutureDate) . (!empty($this->projectList_QS) ? " AND (" . $this->projectList_QS . ")" : "") . (!empty($this->sourcedByList_QS) ? " AND (" . $this->sourcedByList_QS . " )" : "") . (!empty($this->stateList_QS) ? " AND (" . $this->stateList_QS . ")" : "") . " AND (w.Tech_ID = '' OR IFNULL(w.Tech_ID, '') = '') AND Deactivated = 0 AND DeactivationCode != 'Work order declined' AND DeactivationCode != 'Work order kicked back' AND " . $db->quoteInto("Company_ID = ?", $companyID));
			$this->notSourced14 = number_format($notSource14);
	
			// Total New Techs Added - Date passed FLS Exam during this time period
			$newTech = caspioSelectAdv("Tech_T_Test", "COUNT (*) ", " DateTaken >= '" . $this->StartDate . "' AND DateTaken <= '" . $this->EndDate . "' AND Grade >= '92' ", "", false, "`", "|^",false);
			if(sizeof($newTech) == 1 && $newTech[0] == "") {
				$this->newTechsAdded = 0;
			} else {
				$this->newTechsAdded = number_format($newTech[0],0);
			}
			
			// Total Paid WO's and Total Paid
			$this->totalPay = "$" . number_format($records["PaidSum"],2);
			$this->totalPaid = number_format($records["Paid"],0);
			$this->avgPay = "$" . number_format($records["PaidSum"] / $records["Paid"],2);
			
			// Total Hours
			$total = $records["Hours"];
			$this->totalHours = number_format($total,1);
			$this->avgHours = number_format($total / $records["Paid"],1);
			
			// Total OOS
			$this->totalOOS = number_format($records["OOS"],0);
			$this->pcntOOS = sprintf("%01.1f", round($records["OOS"] / $TotalWOs, 3)*100)  . "%";

			// Total Incomplete
			$this->Incomplete = number_format($records["Incomplete"],0);
			$this->pcntIncomplete = sprintf("%01.1f", round($records["Incomplete"] / $TotalWOs, 3)*100)  . "%";
			
		} else {
		
			$this->totalWOs = 0;
			$this->sourced = 0;
			$this->pcntSourced = "0.0%";
			$this->pcntUnsourced = "0.0%";
			$this->complete = 0;
			$this->pcntComplete = "0.0%";
			$this->Incomplete = 0;
			$this->pcntIncomplete = 0;
			$this->kickBack = 0;
			$this->pcntKickedBack = "0.0%";
			$this->declined = 0;
			$this->pcntDeclined = "0.0%";
			$this->deactivated = 0;
			$this->pcntDeactivated = "0.0%";
			$this->unSourced = 0;
			$this->late = 0;
			$this->noshow = 0;
			$this->shortNotice = 0;
			$this->notSourced14 = 0;
			$this->newTechsAdded = 0;
			$this->totalPaid = 0;
			$this->totalPay = 0;
			$this->avgPay = 0; 
			$this->totalHours = 0;
			$this->avgHours = 0; 
			$this->totalOOS = 0;
			$this->pcntOOS = "0.0%";
				
		}

		
	}

}

function getHeader($Heading, $companyID)
{

	if ($companyID == "FTXS") {
		$cols = 30;
	} else {
		$cols = 24;
	}
	/*
	<form id="ActivityReport" name="ActivityReport" onsubmit="showLoadingMsg()" action="<?= $_SERVER['PHP_SELF']?>?v=<?= $_GET["v"] ?>" method="post">
	*/
//	echo "<br><div id=\"results\"><form id=\"formattedFields\" name=\"formattedFields\" action=\"" . $_SERVER['PHP_SELF'] . "\" method=\"post\">
	echo "<br><div id=\"results\"><form id=\"formattedFields\" name=\"formattedFields\" action=\"" . $_SERVER['PHP_SELF'] . "?v=" . $_GET["v"] . "\" method=\"post\">
	<table class=\"resultsTable\"><thead>";
	
	if ($Heading != "") {
		echo "	<tr class=\"resultTop\"><td colspan=\"";
		echo $cols-1;
		echo "\" valign=\"left\"><input type=\"image\" src=\"https://bridge.caspio.net/images/box_blue.gif\" name=\"image\" align=\"left\" border=\"0\"/></td></tr><tr class=\"resultHeader\"><td scope=\"col\" valign=\"bottom\">". $Heading . "</th>";
	
	} else {
	echo "	<tr class=\"resultTop\"><td colspan=\"";
	echo $cols-1;
	echo "\" valign=\"left\"><input type=\"image\" src=\"https://bridge.caspio.net/images/box_blue.gif\" name=\"image\" align=\"left\" border=\"0\"/></td></tr><tr class=\"resultHeader\">";

	}
		
    echo "<td scope=\"col\" valign=\"bottom\">Start Date</th>
    <td scope=\"col\" valign=\"bottom\">End Date</th>
    <td scope=\"col\" valign=\"bottom\">Total WO's</th>
    <td scope=\"col\" valign=\"bottom\">Assigned</th>
    <td scope=\"col\" valign=\"bottom\">% Assigned</th>
    <td scope=\"col\" valign=\"bottom\">Not Assigned</th>
    <td scope=\"col\" valign=\"bottom\">% Not Assigned</th>
    <td scope=\"col\" valign=\"bottom\">Complete</th>
    <td scope=\"col\" valign=\"bottom\">% Complete</th>
    <td scope=\"col\" valign=\"bottom\">Incomplete</th>
    <td scope=\"col\" valign=\"bottom\">% Incomplete</th>";
	
    if ($companyID == "FTXS") {
	echo "<td scope=\"col\" valign=\"bottom\">Kicked Back</th>
    <td scope=\"col\" valign=\"bottom\">% Kicked Back</th>
    <td scope=\"col\" valign=\"bottom\">Declined</th>
    <td scope=\"col\" valign=\"bottom\">% Declined</th>";
	}
	
    echo "<td scope=\"col\" valign=\"bottom\">Deactivated</th>
    <td scope=\"col\" valign=\"bottom\">% Deactivated</th>
    <td scope=\"col\" valign=\"bottom\">Late Arrivals</th>
    <td scope=\"col\" valign=\"bottom\">No Shows</th>";
	
	if ($companyID == "FTXS") {
    echo "<td scope=\"col\" valign=\"bottom\">Short Notice</th>
    <td scope=\"col\" valign=\"bottom\">Not Sourced 14 Days</th>
    <td scope=\"col\" valign=\"bottom\">New Techs</th>";
	}
	
    echo "<td scope=\"col\" valign=\"bottom\">WO's Paid</th>
    <td scope=\"col\" valign=\"bottom\">Total Pay</th>
    <td scope=\"col\" valign=\"bottom\">Avg Pay</th>
    <td scope=\"col\" valign=\"bottom\">Actual Hours</th>
    <td scope=\"col\" valign=\"bottom\">Avg Hours</th>";
    
    if ($companyID == "FTXS") {
	echo "<td scope=\"col\" valign=\"bottom\">Out of Scope</th>
    <td scope=\"col\" valign=\"bottom\">% Out of Scope</th>";
	}
	
  	echo "</tr></thead><tbody>";
}

function getFooter($formattedFields)
{
	echo "</tbody></table>  <textarea name=\"formattedFields\" id=\"formattedFields\" style=\"display:none\">" . $formattedFields . "</textarea></div>";
}

function getHeaderCSV($Heading, $companyID)
{
    if ($companyID == "FTXS") {
		if ($Heading != "") {
	    	echo $Heading . ",";
		}
		echo "Start,End,Total WO's, Assigned, % Assigned, Not Assigned, % Not Assigned, Complete, % Complete, Incomplete, % Incomplete, Kicked Back, %Kicked Back, Declined, %Declined, Deactivated, %Deactivated, Late Arrivals, No Shows, Short Notice, Not Sourced 14 Days, New Techs, WO's Paid, Total Pay, Avg Pay, Actual Hours, Avg Hours, OOS, %OOS \n";

	} else {

		if ($Heading != "") {
	    	echo $Heading . ",";
		}

		echo "Start,End,Total WO's, Assigned, % Assigned, Not Assigned, % Not Assigned, Complete, % Complete, Incomplete, % Incomplete, Deactivated, %Deactivated, Late Arrivals, No Shows, WO's Paid, Total Pay, Avg Pay, Actual Hours, Avg Hours \n";

	}
}

function escapeCSVField($value) {
	$delimiter = ",";
	$enclosure = "\"";
	if (strpos($value, ' ') !== false ||
		strpos($value, $delimiter) !== false ||
		strpos($value, $enclosure) !== false ||
		strpos($value, "\n") !== false ||
		strpos($value, "\r") !== false ||
		strpos($value, "\t") !== false)
	{
		$str2 = $enclosure;
		$escaped = 0;
		$len = strlen($value);
		for ($i=0;$i<$len;$i++)
		{
			if (!$escaped && $value[$i] == $enclosure)
				$str2 .= $enclosure;
			$str2 .= $value[$i];
		}
		$str2 .= $enclosure;
		$value = $str2;
	}
	return $value;
}


// The following sets up variables for the date pop-ups.

function getDay($Day){
	switch($Day) {
		case 1:
			return(2);
			break;
		case 2:
			return(3);
			break;
		case 3:
			return(4);
			break;						
		case 4:
			return(5);
			break;
		case 5:
			return(6);
			break;				
		case 6:
			return(7);
			break;				
		case 7:
			return(0);
			break;
	}
}


$Today = date("m-d-Y");
$Tomorrow  = date("m-d-Y", mktime(0, 0, 0, date("m"), date("d")+1, date("Y")));

// This Week (Mon = 1 - Sun = 7)
$BOW = date("m-d-Y", mktime(0, 0, 0, date("m"), date("d")-getDay(date("N"))+1, date("Y")));
$EOW = date("m-d-Y", mktime(0, 0, 0, date("m"), date("d")-getDay(date("N"))+7, date("Y")));

// last Week (Mon = 1 - Sun = 7)
$BOLW = date("m-d-Y", mktime(0, 0, 0, date("m"), date("d")-getDay(date("N"))-6, date("Y")));
$EOLW = date("m-d-Y", mktime(0, 0, 0, date("m"), date("d")-getDay(date("N")), date("Y")));

// This Month
$BOM = date("m-d-Y", mktime(0, 0, 0, date("m"), date("d")-date("d")+1, date("Y")));
$EOM = date("m-d-Y", mktime(0, 0, 0, date("m")+1, date("d")-date("d"), date("Y")));

// Last Month
$PrevBOM = date("m-d-Y", mktime(0, 0, 0, date("m")-1, date("d")-date("d")+1, date("Y")));
$PrevEOM = date("m-d-Y", mktime(0, 0, 0, date("m"), date("d")-date("d"), date("Y")));

// Q1
$Q1Start = date("m-d-Y", mktime(0, 0, 0, 1, 1, date("Y")));
$Q1End = date("m-d-Y", mktime(0, 0, 0, 4, date("d")-date("d"), date("Y")));

// Q2
$Q2Start = date("m-d-Y", mktime(0, 0, 0, 4, 1, date("Y")));
$Q2End = date("m-d-Y", mktime(0, 0, 0, 7, date("d")-date("d"), date("Y")));

// Q3
$Q3Start = date("m-d-Y", mktime(0, 0, 0, 7, 1, date("Y")));
$Q3End = date("m-d-Y", mktime(0, 0, 0, 10, date("d")-date("d"), date("Y")));

// Q4
$Q4Start = date("m-d-Y", mktime(0, 0, 0, 10, 1, date("Y")));
$Q4End = date("m-d-Y", mktime(0, 0, 0, 13, date("d")-date("d"), date("Y")));

// H1
$H1Start = date("m-d-Y", mktime(0, 0, 0, 1, 1, date("Y")));
$H1End = date("m-d-Y", mktime(0, 0, 0, 7, date("d")-date("d"), date("Y")));

// H2
$H2Start = date("m-d-Y", mktime(0, 0, 0, 7, 1, date("Y")));
$H2End = date("m-d-Y", mktime(0, 0, 0, 13, date("d")-date("d"), date("Y")));

// YTD
$YTDStart = date("m-d-Y", mktime(0, 0, 0, 1, 1, date("Y")));
$YTDEnd = date("m-d-Y", mktime(0, 0, 0, date("m"), date("d"), date("Y")));
	
?>
