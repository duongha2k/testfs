<?php 
    header("Content-Type: text/html; charset=utf-8");
    $page = 'clients';
    $option = 'email';
    $selected = 'emailBlast';
    
$StartDate = isset($_POST['StartDate'])?$_POST['StartDate']:"";
$EndDate = isset($_POST['EndDate'])?$_POST['EndDate']:"";
$Subject = isset($_POST['Subject'])?$_POST['Subject']:"Backup Technicians Needed!";
$Comments = isset($_POST['Comments'])?$_POST['Comments']:"";
$FromEmail = isset($_POST['FromEmail'])?$_POST['FromEmail']:"";


    require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');

    if($isFLSManager){
        $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
    } else {
        $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
    }
    
?>
<?php 
$isLB = !empty($_GET['isLB']);

$pid = isset($_POST['Project'])?$_POST['Project']:(isset($_GET['pid'])?$_GET['pid']:"");

if (!$isLB)
	require ("../header.php"); 
else
	require ("../headerSimple.php"); 
?>
<?php require ("includes/adminCheck.php"); ?>
<?php 
	if (!$isLB)
		require ("../navBar.php");
	else
		$displayPMTools = false;

        $v = $_GET["v"];
require ("includes/PMCheck.php"); 
?>
<!-- Add Content Here -->

<?php
//	ini_set("display_erorrs", 1);
	require_once("../library/caspioAPI.php");
	require_once("../library/smtpMail.php");

	if (isset($_POST["send"])) {
		if (!is_numeric($_POST["Project"])) die();
		$companyID = $_GET["v"];
		$startDate = $_POST["StartDate"];
		$endDate = $_POST["EndDate"];

		if (!empty($startDate)) {
			$date = new Zend_Date($startDate, 'M/d/yyyy');
			$startDate = $date->toString('yyyy-MM-dd');
		}
		if (!empty($endDate)) {
			$date = new Zend_Date($endDate, 'M/d/yyyy');
			$endDate = $date->toString('yyyy-MM-dd');
		}

		$criteriaDate = "";
		if (!empty($startDate)) {
			$criteriaDate .= " AND StartDate >= '$startDate'";
		}
		if (!empty($endDate)) {
			$criteriaDate .= " AND StartDate <= '$endDate'";
		}

		// get form info
		$projectID = $_POST["Project"];

		$db = Zend_Registry::get('DB');
		$sql = "SELECT DISTINCT TechID FROM work_orders AS w JOIN work_orders__bids AS b ON (w.WIN_NUM = b.WorkOrderID AND TechID != Tech_ID) WHERE Project_ID = ? $criteriaDate AND IFNULL(Tech_ID, '') != '' AND Invoiced = 0 AND Deactivated = 0 AND TechMarkedComplete = 0 AND w.Company_ID = ?";
		$techIds = $db->fetchCol($sql, array($projectID, $companyID));

		$woList = array();
		if (count($techIds)) {
			$woList = caspioSelectAdv("TR_Master_List", "PrimaryEmail", 'TechID IN (' . implode(',', $techIds) . ')', "", false, " ", "|");
		}

		if (empty($woList) || (sizeof($woList) == 1 && $woList[0] == "")) {
			// No work orders found
			echo "<div align=\"center\"><br/>No work orders found.<br/></div>";
			
			echo "
					<script>
			$(\"body\").css(\"width\", \"800px\");
			$(\"body\").css(\"margin\", \"0\");
			$(\"body\").css(\"overflow\", \"hidden\");
			$(\"#headerSlider\").css(\"display\", \"none\");
			$(\"#maintab\").css(\"display\", \"none\");
			$(\"#subSectionLabel\").css(\"display\", \"none\");
			$(\"#PMCompany\").css(\"display\", \"none\");
			$(\"table.form_table\").css(\"margin\", \"0\");
			$(\"table.form_table\").css(\"padding\", \"0\");
			$(\"table.form_table\").css(\"width\", \"400px\");
			$(\"div.footer_menu\").css(\"display\",\"none\");
			$(\"#content\").css(\"margin\", \"0\");
			$(\"#tabcontent\").css(\"display\", \"none\");
			$(\"#searchForm\").css(\"width\", \"800px\");
			$(\"#mainContainer\").css(\"width\", \"800px\");
			$(\"#mainContainer\").css(\"margin\", \"0\");
			</script>
			";
		}
		else {

			$subject = $_POST['Subject'];
			$projectName = $_POST['ProjectName'];
			$comments = $_POST['Comments'];
//                        "
//Thank you for applying for $projectName. While this site has already been assigned, we noticed your experience and skill set are a very good match for this opportunity. 
// 
//Since an additional resource may be required, would you be willing to allow us to reach out to
//you on short notice, if needed? If so, we may call within 2 hours of the start time of this site. 
// 
//If you accept, we will pay you 20% more than your bid. In addition, we will make a note on
//your account that you were flexible in accepting a short notice request. This flexibility is very
//valuable to us and our customers. To accept: simply reply to this email.
// 
//We appreciate your interest in this opportunity.
// 
//Thank you,
//The FieldSolutions Team
//";
			$copySelf = isset($_POST["copySelf"]);
			$emailSent = 0;

/*			$db = Zend_Registry::get('DB');
			$sql = "SELECT from_email FROM projects WHERE Project_ID=?";
			$fromEmail = $db->fetchOne($sql, array($projectID));*/
			$fromEmail = $_POST["FromEmail"];
			if (!empty($fromEmail)) {
				if($copySelf) {
					$woList[sizeof($woList)+1] = $fromEmail;
				}

                            $info = smtpMailLogReceivedBG("Field Solutions", $fromEmail, $woList, $subject, $comments, nl2br($comments), "projectBackupTechBlast.php", ",");
                                
                            
//                $mail = new Core_Mail();
//                //Send mail to tech
//                $message = $comments;
//
//                $mail->setBodyText($message);
//                $mail->setFromName("FieldSolutions");
//                $mail->setFromEmail('no-replies@fieldsolutions.com');
//                $mail->setToEmail("nguyenhau79@yahoo.com");
//                $mail->setSubject($subject);
//                $mail->send();


				echo "<div align=\"center\">";
				if ($copySelf) {
					echo "Copy sent to: $fromEmail<br/>";
				}
				echo "Emails sent: " . sizeof($woList) . "<br/>";
                                
				echo "</div>";
				echo "
					<script>
			$(\"body\").css(\"width\", \"800px\");
			$(\"body\").css(\"margin\", \"0\");
			$(\"body\").css(\"overflow\", \"hidden\");
			$(\"#headerSlider\").css(\"display\", \"none\");
			$(\"#maintab\").css(\"display\", \"none\");
			$(\"#subSectionLabel\").css(\"display\", \"none\");
			$(\"#PMCompany\").css(\"display\", \"none\");
			$(\"table.form_table\").css(\"margin\", \"0\");
			$(\"table.form_table\").css(\"padding\", \"0\");
			$(\"table.form_table\").css(\"width\", \"400px\");
			$(\"div.footer_menu\").css(\"display\",\"none\");
			$(\"#content\").css(\"margin\", \"0\");
			$(\"#tabcontent\").css(\"display\", \"none\");
			$(\"#searchForm\").css(\"width\", \"800px\");
			$(\"#mainContainer\").css(\"width\", \"800px\");
			$(\"#mainContainer\").css(\"margin\", \"0\");
			</script>
			";
			}
			else {
				echo "<div align=\"center\"><br/>From email address required<br/></div>";
			}
		}
	}
	if (isset($_GET["v"])) {
		$companyID = $_GET["v"];
		$db = Zend_Registry::get('DB');
		if ($db instanceof Zend_Db_Adapter_Abstract) {}
		$sql = "SELECT Project_ID,Project_Name from projects WHERE active=1 AND Project_Company_ID=? ORDER BY Project_Name ASC";
		$projectOptions = $db->fetchPairs($sql, array($companyID));
                if(!empty($pid))
                    $projectHtml = "<option value=\"None\" >Select Project</option>";
                else
                    $projectHtml = "<option value=\"None\" selected=\"selected\">Select Project</option>";
                
		foreach ($projectOptions as $id => $name) {
                    if($pid == $id)
                    {
                        $projectHtml .= "<option value=\"$id\" selected=\"selected\">$name</option>";
                    }
                    else
			$projectHtml .= "<option value=\"$id\">$name</option>";
		}
                
	}
?>

<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#StartDate').calendar({dateFormat: 'MDY/'});
		$('#EndDate').calendar({dateFormat: 'MDY/'});
		$('#Project').change(function() { 
			$.ajax({
				url: '/widgets/dashboard/project/ajax-get-project',
				data: {'Project_Company_ID' : '<?=$companyID?>', 'Project_ID' : $('#Project').val()},
				dataType: 'json',
				success: updateProjectInfo
			});
		});
                
                $.ajax({
                    url: '/widgets/dashboard/project/ajax-get-project',
                    data: {'Project_Company_ID' : '<?=$companyID?>', 'Project_ID' : $('#Project').val()},
                    dataType: 'json',
                    success: updateProjectInfo
                });
		
		
	});
	$('document').ready(function(){
	var ref = document.referrer.split("?")[0];
		if (ref.match("wosDetails.php")){
			$("body").css("width", "800px");
			$("body").css("margin", "0");
			$("body").css("overflow", "hidden");
			$("#headerSlider").css("display", "none");
			$("#maintab").css("display", "none");
			$("#subSectionLabel").css("display", "none");
			$("#PMCompany").css("display", "none");
			$("table.form_table").css("margin", "0");
			$("table.form_table").css("padding", "0");
			$("table.form_table").css("width", "400px");
			$("div.footer_menu").css("display","none");
			$("#content").css("margin", "0");
			$("#tabcontent").css("display", "none");
			$("#searchForm").css("width", "800px");
			$("#mainContainer").css("width", "800px");
			$("#mainContainer").css("margin", "0");
			
		}
});
</script>

<script type="text/javascript">
function saveUserInfo()
{
	if (document.forms.recruit.Project.selectedIndex == 0) {
		alert("Please select a project");
		return false;
	}

	if (document.forms.recruit.Subject.value == "") {
		alert("Please enter a subject for this message");
		return false;
	}

	var expireDate = new Date;
	expireDate.setMonth(expireDate.getMonth()+6);

	var eCopySelf = (document.forms.recruit.copySelf.checked ? "1" : "0");

	document.cookie = "blast_CopySelf=" + eCopySelf + ";expires=" + expireDate.toGMTString() + ";path=/";
}

function get_cookie(theCookie)
{
	var search = theCookie + "="
	var returnvalue = "";
	if (document.cookie.length > 0)
	{
		offset = document.cookie.indexOf(search);
		// if cookie exists
		if (offset != -1)
		{
			offset += search.length;
			// set index of beginning of value
			end = document.cookie.indexOf(";", offset);
			// set index of end of cookie value
			if (end == -1) end = document.cookie.length;
			returnvalue=unescape(document.cookie.substring(offset, end));
		}
	}
	return returnvalue;
}

function updateProjectInfo(data) {
    if(data['success'] !=0)
    {
	var email = data['project']['From_Email'];
	var projectName = data['project']['Project_Name'];
	if (email.indexOf('no-replies') != -1) email = '';
	$('#FromEmail').val(email);
	$('#ProjectName').val(projectName);;
    }
}
</script>

<style  TYPE="text/css">
.label{ font-size:12px; color:#385C7E;}
</style>

<div align="center">
<h1>Email bidders in this project to request backup technicians</h1>
<form id="recruit" name="recruit" action="<?=$_SERVER['PHP_SELF']?>?v=<?=$v?>&isLB=<?=$isLB?>" method="post">
    <table cellpadding="0" cellspacing="0" border="0" style="border:#385C7E solid medium;">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="5">
                    <tr>
                        <td class="label">From Email:</td>
                        <td align="left">
                            <input name="FromEmail" type="text" id="FromEmail" size="35"> <span style="color:#ff0000; font-weight:bold" value="<?=$fromEmail?>">* Required</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Project</td>
                        <td align="left">
                            <select id="Project" name="Project">
                                    <?=$projectHtml?>
                            </select>
                            &nbsp;<input name="copySelf" type="checkbox" checked="<?php echo (isset($_POST["copySelf"]) ? "checked" : "");?>"/>&nbsp;Copy&nbsp;to&nbsp;Self
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Start Date (>=)</td>
                        <td align="left">
                            <input name="StartDate" type="text" id="StartDate" size="15" value="<?= $StartDate;?>"> <span style="color:#ff0000; font-weight:bold">* Required</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Start Date (<=)</td>
                        <td align="left">
                            <input name="EndDate" type="text" id="EndDate" size="15" value="<?= $EndDate;?>"> <span style="color:#ff0000; font-weight:bold">* Required</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Subject</td>
                        <td align="left">
                            <textarea style="width: 30em; height: 1.3em" name="Subject" id="Subject" ><?= $Subject;?></textarea><span style="color:#ff0000; font-weight:bold">* Required</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Message</td>
                        <td align="left">
                            <textarea style="width: 30em; height: 10em" name="Comments" id="Comments" ><?= $Comments;?></textarea>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="text-align:center; background-color:#BACBDF; border-top:#385C7E solid medium; padding:5px;">
                <input id="ProjectName" name="ProjectName" value="" type="hidden"/>
                <input id="send" name="send" type="submit" value="Send" style="background-color:#385C7E; color:#FFFFFF; font-size:12px; font-weight:bold;" />
            </td>
        </tr>
    </table>
</form>

<script type="text/javascript">
	document.forms.recruit.copySelf.checked = (get_cookie("blast_CopySelf") == "1");
	document.forms.recruit.onsubmit = saveUserInfo;
</script>

<!--- End Content --->
<?php if (!$isLB) require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
