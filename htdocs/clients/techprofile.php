<?php $page = 'clients'; ?>
<?php $option = 'profile'; ?>
<?php
	require_once("../headerStartSession.php");
	require_once("../library/FSSession.php");
	FSSession::validateSessionParams(array("TechID" => "TechID"));
?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<script src="<?=empty($_SERVER['HTTPS']) ? 'http' : 'https'?>://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<link rel="stylesheet" href="<?=empty($_SERVER['HTTPS']) ? 'http' : 'https'?>://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/ui-lightness/jquery-ui.css" type="text/css" />
<link rel="stylesheet" href="/widgets/css/blueprint/screen.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="/widgets/css/blueprint/print.css" type="text/css" media="print" /> 
<script src="http://cdn.jquerytools.org/1.2.5/full/jquery.tools.min.js"></script>
<script src="/widgets/js/jquery.progressbar/js/jquery.progressbar.js"></script>
<link rel="stylesheet" type="text/css" href="/widgets/js/star-rating/jquery.rating.css" />
<script src="/widgets/js/star-rating/jquery.rating.js"></script>
<link rel="stylesheet" type="text/css" href="/widgets/css/tabs.css" />
<!--  script type="text/javascript" src="/widgets/js/FSTechProfile.js"></script -->
  <!--[if lt IE 8]>
    <link rel="stylesheet" href="/widgets/css/blueprint/ie.css" type="text/css" media="screen, projection" />
  <![endif]-->
<br/><br />

<script type="text/javascript">
$(document).ready(function() { 
 //tp = new FSTechProfile();
 changeCenterContent('expertise');
 $("body").data("isCollapse",false);
 
 $("#hideUnhide").click(function(){
		if($("body").data("isCollapse") == false){
			
			$("#collapseContent").html($("#moveContent").html());
		}else{
		}
	});
 
});

function setupLogbookTabs() {
	// setup ul.tabs to work as tabs for each div directly under div.panes
	$("ul.tabs").tabs("div.fsLogbookPanes > div");
}

function setupRatings(){
	$("#expRating1").progressBar(35);
	$("#expRating2").progressBar(65);
	$("#expRating3").progressBar(75);
	$("#expRating4").progressBar(85);
}

function changeCenterContent(id){
	$("#centerContent").html($("#"+id).html());
	if(id == "fslogbook"){
		setupLogbookTabs();
	}
	if(id == "expertise"){
		setupRatings();
	}
}




</script>
<style type="text/css">
.jobHistory{
	color: #000;
}
	.jobHistory th {
		background-color: #4790D4;
		border: 1px #FFFFFF solid;
		color: #000000;
		padding: 5px;
	}
	.jobHistory td{
		text-align: center;
		background-color: #F0F8FF;
		border: 1px #FFFFFF solid;
	}
	.skillsCat{
		text-align: left;
	}
	.underline{
		text-decoration: underline;
	}
	.woAssign{
		color:	#FF0000;
		float: right;
		margin: 0 5px 0 5px;
	}
	.wo{
		margin-bottom: 10px;
	}
	
	/* tab pane styling */
.panes div {
	display:none;		
	padding:1px 0px;
	/*border:1px solid #999;*/
	border-top:0;
	height:100px;
	font-size:14px;
	background-color:#fff;
}

#socialmedia{
	border-collapse: collapse;
	background: #BABABA;
}
#socialmedia td{
	margin: 0 2px 0 2px;
}

</style>
<div class="container" style="font-size: 14px; color: #4790D4;">
  
  <!-- TOP ROW -->   
      <div class="span-4 border">
        <div>
        	<img src="https://bridge.caspio.net/dpImages.aspx?appkey=193b0000335813e7887d4cfa9ead&fileID=287970" width="150" height="145" />
        	<div style="background-color: #4790D4; text-align: center; color: #FFF;">Gerald Bailey<br />512-555-5555 email</div>
        </div>
        
        
      	<table id="socialmedia">
      		<tr>
      			<td><img src="/widgets/css/images/socialmediaicons/twitter-24x24.png" alt="Follow Me On Twitter" /></td><td><img src="/widgets/css/images/socialmediaicons/facebook-24x24.png"  alt="Visit Me On Facebook!" /></td><td><img src="/widgets/css/images/socialmediaicons/linkedin-24x24.png" alt="LinkedIn" /></td><td><img src="/widgets/css/images/socialmediaicons/youtube-24x24.png" alt="Youtube" /></td><td><img src="/widgets/css/images/socialmediaicons/digg-this-24x24.png" alt="Digg This" /></td><td><img src="/widgets/css/images/socialmediaicons/share-24x24.png" alt="Share This" /></td>
      		</tr>
      	</table>
      </div>
      
     
	      <div class="span-15">
	      	<div class="span-15">
	      		<table>
	      			<tr>
	      				<td><h1 style="text-align: left;">Tech-At-A-Glance</h1></td>
	      			</tr>
	      			<tr>
	      				<td>FS-Tech ID#: xxxxxxxx</td>
	      			</tr>
	      			<tr>
	      				<td>FLS ID#: xxxxxxxx / FLS Status: xxxxxxx</td>
	      			</tr>
	      		</table>
	      		
	      	</div>
	      	<div class="span-7">
		      <table style="line-height: 19px;">
		      	<tr>
		      		<td></td><td><h2 style="text-decoration: underline;">12 Months</h2></td><td><h2 style="text-decoration: underline;">Lifetime</h2></td>
		      	</tr>
		      	<tr>
		      		<td>IMAC:</td><td>345</td><td>945</td>
		      	</tr>
		      	<tr>
		      		<td>Service:</td><td>18</td><td>68</td>
		      	</tr>
		      	<tr>
		      		<td>Back Outs:</td><td>6</td><td>10</td>
		      	</tr>
		      	<tr>
		      		<td>No Shows:</td><td>2</td><td>2</td>
		      	</tr>
		      	<tr>
		      		<td>Preference:</td><td>91%</td><td>91%</td>
		      	</tr>
		      	<tr>
		      		<td>Performance:</td><td>91%</td><td>91%</td>
		      	</tr>
		      	<tr>
		      		<td>Comments:</td><td>3</td><td>9</td>
		      	</tr>
		      </table>
	      </div>
	   
	       <div class="span-7">
				<table>
			      	<tr>
			      		<td colspan="2" style="text-align: center;"><h2 style="text-decoration: underline;">Business Statment</h2></td>
			      	</tr>
			      	<tr>
			      		<td colspan="2" style="padding-bottom: 15px;">
			      			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. (...more)
			      		</td>
			      	</tr>
		      </table>      
		  	</div>
	      </div>
	      
		  
		  <div class="span-5 last">
			  <table>
			  	<tr>
			  		<td>Resume</td>
			  	</tr>
			  	<tr>
			  		<td>My Website</td>
			  	</tr>
			  	<tr>
			  		<td>Drug Test (12/01/2011)</td>
			  	</tr>
			  	<tr>
			  		<td>Background Check (12/01/2011)</td>
			  	</tr>
			  	<tr>
			  		<td>ISO: Name</td>
			  	</tr>
			  	<tr>
			  		<td>Dispatcher Name</td>
			  	</tr>
			  	<tr>
			  		<td>Phone#</td>
			  	</tr>
			  	<tr>
			  		<td><hr /></td>
			  	</tr>
			  	<tr>
			  		<td>
			  			<table>
			  				<tr>
			  					<td><input type="checkbox" name="clientPreferred" /></td><td>Client Preferred</td>
			  				</tr>
			  				<tr>
			  					<td><input type="checkbox" name="clientDenied" /></td><td>Client Denied</td>
			  				</tr>
			  				<tr>
			  					<td><img></td><td>Likes (2) Like</td>
			  				</tr>
			  				<tr>
			  					<td><img></td><td>Comment on Tech</td>
			  				</tr>
			  			</table>
			  		</td>
			  	</tr>
			  </table>
		  </div>
		  
		  <!--  div class="span-1 last" style="height: 200px;background-color: #ADD8E6;">
		  	<span style="" id="hideUnhide">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hide/Unhide</span>
		  </div -->

	<!-- END TOP ROW -->
    <hr />
      <div class="span-4 border">
       
			<ul style="line-height: 30px; font-size: 17px;">
				<li><a href="#" onclick="javascript:changeCenterContent('expertise');">Expertise</a></li>
				<li><a href="#" onclick="javascript:changeCenterContent('fslogbook');">FS-LogBook&trade;</a></li>
				<li><a href="#" onclick="javascript:changeCenterContent('expertise');">Equipment</a></li>
				<li><a href="#" onclick="javascript:changeCenterContent('expertise');">Certifications</a></li>
				<li><a href="#" onclick="javascript:changeCenterContent('expertise');">FS-TrustedTech&trade;</a></li>
				<li><a href="#" onclick="javascript:changeCenterContent('expertise');">Comments</a></li>
				<li><a href="#" onclick="javascript:changeCenterContent('expertise');">My Schedule</a></li>
				<li><a href="#" onclick="javascript:changeCenterContent('expertise');">Contact Information</a></li>
			</ul>

      </div>
      
	      <div class="span-15" id="centerContent">
	      	
		      
		      
	      </div>
	      
	      <div class="span-5 last">
	        <img src="/widgets/css/images/Iphone_Gmaps_150.gif" width="190" height="255" />
	        <div style="background-color: #F0F8FF; color: #000; padding-bottom: 15px;">
	        	<div style="width: 65%; margin: 0 auto 0 auto; padding-top: 10px; text-align:center;">My Unassigned Work Orders</div>
	        	<div class="myUnassignedWO" style="background-color: #FFF; margin: 20px 15px 20px 15px; font-size: 11px;">
	        		<div class="wo">
	        			<span class="underline">POS Scanner Swap</span><span class="woAssign">ASSIGN!</span>
	        			<span>WIN 123456</span> - <span>WO 54673</span>
	        		</div>
	        		<div class="wo">
	        			<span class="underline">Digital Menu Install</span><span class="woAssign">ASSIGN!</span>
	        			<span>WIN 123456</span> - <span>WO 54673</span>
	        		</div>
	        		<div class="wo">
	        			<span class="underline">POS Scanner Swap</span><span class="woAssign">ASSIGN!</span>
	        			<span>WIN 123456</span> - <span>WO 54673</span>
	        		</div>
	        		<div class="wo">
	        			<span class="underline">Digital Menu Install</span><span class="woAssign">ASSIGN!</span>
	        			<span>WIN 123456</span> - <span>WO 54673</span>
	        		</div>
	        		<span style="margin-left: 110px;">(view all)</span>
	        		
	        	</div>
	        </div>
	      </div>
      
    </div>

  
  <div id="fslogbook" style="display:none;">
      	
      		<ul class="tabs">
      			<li><a href="#tabs-1">12 Months</a></li>
				<li><a href="#tabs-2">90 Days</a></li>
				<li><a href="#tabs-2">Lifetime</a></li>
				<li><a href="#tabs-2">FS-Logbook</a></li>
				<li><a href="#tabs-2">My Client Logbook</a></li>
      		</ul>
      		
      		<div class="fsLogbookPanes">
      		
      			<div>
			        <table class="jobHistory">
			        	<thead>
			        		<tr>
			        			<th></th>
			        			<th colspan="6" style="text-align: center;">12 Months</th>
			        		</tr>
			        		<tr>
			        			<th>Skill Category</th>
			        			<th>Jobs</th>
			        			<th>Backouts</th>
			        			<th>No Shows</th>
			        			<th>Preference</th>
			        			<th>Performance</th>
			        			<th>Rates</th>
			        		</tr>
			        	</thead>
			        	<tbody>
			        		<tr>
			        			<td style="text-align: left;">Cat5 Cabling</td>
			        			<td>120</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>
			        				60
								</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Low Voltage</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">PC - Desktop / Laptop</td>  <!--13675--
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Digital Signage</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Kiosk</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Point Of Sale</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Site Survey</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Wireless Networking</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Audio/Video (AV)</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Printer/Copiers</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left; font-weight: bold;">Total Jobs</td>
			        			<td style="font-weight: bold;">108</td>
			        			<td style="font-weight: bold;">0</td>
			        			<td style="font-weight: bold;">0</td>
			        			<td style="font-weight: bold;">98%</td>
			        			<td style="font-weight: bold;">98%</td>
			        			<td style="font-weight: bold;">60</td>
			        		</tr>
			        	</tbody>
			        </table>
		        </div>
		        
		        <div>
			        <table class="jobHistory">
			        	<thead>
			        		<tr>
			        			<th></th>
			        			<th colspan="6" style="text-align: center;">90 Months</th>
			        		</tr>
			        		<tr>
			        			<th>Skill Category</th>
			        			<th>Jobs</th>
			        			<th>Backouts</th>
			        			<th>No Shows</th>
			        			<th>Preference</th>
			        			<th>Performance</th>
			        			<th>Rates</th>
			        		</tr>
			        	</thead>
			        	<tbody>
			        		<tr>
			        			<td style="text-align: left;">Cat5 Cabling</td>
			        			<td>120</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Low Voltage</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Desktop/Laptop</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Digital Signage</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Kiosk</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Point Of Sale</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Site Survey</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Wireless Networking</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Audio/Video (AV)</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Printer/Copiers</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left; font-weight: bold;">Total Jobs</td>
			        			<td style="font-weight: bold;">108</td>
			        			<td style="font-weight: bold;">0</td>
			        			<td style="font-weight: bold;">0</td>
			        			<td style="font-weight: bold;">98%</td>
			        			<td style="font-weight: bold;">98%</td>
			        			<td style="font-weight: bold;">60</td>
			        		</tr>
			        	</tbody>
			        </table>
	       		</div>
	       		
	       		<div>
			        <table class="jobHistory">
			        	<thead>
			        		<tr>
			        			<th></th>
			        			<th colspan="6" style="text-align: center;">Lifetime</th>
			        		</tr>
			        		<tr>
			        			<th>Skill Category</th>
			        			<th>Jobs</th>
			        			<th>Backouts</th>
			        			<th>No Shows</th>
			        			<th>Preference</th>
			        			<th>Performance</th>
			        			<th>Rates</th>
			        		</tr>
			        	</thead>
			        	<tbody>
			        		<tr>
			        			<td style="text-align: left;">Cat5 Cabling</td>
			        			<td>120</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Low Voltage</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Desktop/Laptop</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Digital Signage</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Kiosk</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Point Of Sale</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Site Survey</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Wireless Networking</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Audio/Video (AV)</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">Printer/Copiers</td>
			        			<td>70</td>
			        			<td>0</td>
			        			<td>0</td>
			        			<td>98%</td>
			        			<td>98%</td>
			        			<td>60</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left; font-weight: bold;">Total Jobs</td>
			        			<td style="font-weight: bold;">108</td>
			        			<td style="font-weight: bold;">0</td>
			        			<td style="font-weight: bold;">0</td>
			        			<td style="font-weight: bold;">98%</td>
			        			<td style="font-weight: bold;">98%</td>
			        			<td style="font-weight: bold;">60</td>
			        		</tr>
			        	</tbody>
			        </table>
	       		</div>
	       		
	       		<div>
			        <table class="jobHistory">
			        	<thead>
			        		<tr>
			        			<th></th>
			        			<th colspan="6" style="text-align: center;">FS-LogBook</th>
			        		</tr>
			        		<tr>
			        			<th>Start</th>
			        			<th>Skill</th>
			        			<th>Headline</th>
			        			<th>Tech Pay</th>
			        			<th>Client Rating</th>
			        			<th>Comments</th>
			        		</tr>
			        	</thead>
			        	<tbody>
			        		<tr>
			        			<td style="text-align: left;">1/3/2011</td>
			        			<td>Point of Sale</td>
			        			<td>Setup POS system in training area</td>
			        			<td>$50/time</td>
			        			<td></td>
			        			<td></td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">1/3/2011</td>
			        			<td>Point of Sale</td>
			        			<td>Setup POS system in training area</td>
			        			<td>$50/time</td>
			        			<td></td>
			        			<td></td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">1/3/2011</td>
			        			<td>Point of Sale</td>
			        			<td>Setup POS system in training area</td>
			        			<td>$50/time</td>
			        			<td><input name="star3" type="radio" class="star" disabled="disabled"/>
									<input name="star3" type="radio" class="star" disabled="disabled" checked="checked"/>
									<input name="star3" type="radio" class="star" disabled="disabled" />
									<input name="star3" type="radio" class="star" disabled="disabled"/>
									<input name="star3" type="radio" class="star" disabled="disabled"/></td>
			        			<td>1</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">12/29/2010</td>
			        			<td>Point of Sale</td>
			        			<td>Setup POS system in training area</td>
			        			<td>$50/time</td>
			        			<td></td>
			        			<td></td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">12/28/2010</td>
			        			<td>Point of Sale</td>
			        			<td>Setup POS system in training area</td>
			        			<td>$50/time</td>
			        			<td></td>
			        			<td></td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">12/27/2010</td>
			        			<td>Point of Sale</td>
			        			<td>Setup POS system in training area</td>
			        			<td>$50/time</td>
			        			<td><input name="star4" type="radio" class="star" disabled="disabled"/>
									<input name="star4" type="radio" class="star" disabled="disabled"/>
									<input name="star4" type="radio" class="star" disabled="disabled" />
									<input name="star4" type="radio" class="star" disabled="disabled"/>
									<input name="star4" type="radio" class="star" disabled="disabled" checked="checked"/></td>
			        			<td>2</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">12/27/2010</td>
			        			<td>Point of Sale</td>
			        			<td>Setup POS system in training area</td>
			        			<td>$50/time</td>
			        			<td></td>
			        			<td></td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">12/15/2010</td>
			        			<td>Point of Sale</td>
			        			<td>Setup POS system in training area</td>
			        			<td>$50/time</td>
			        			<td><input name="star5" type="radio" class="star" disabled="disabled"/>
									<input name="star5" type="radio" class="star" disabled="disabled"/>
									<input name="star5" type="radio" class="star" disabled="disabled" />
									<input name="star5" type="radio" class="star" disabled="disabled" checked="checked"/>
									<input name="star5" type="radio" class="star" disabled="disabled"/></td>
			        			<td>2</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">11/30/2010</td>
			        			<td>Point of Sale</td>
			        			<td>Setup POS system in training area</td>
			        			<td>$50/time</td>
			        			<td><input name="star6" type="radio" class="star" disabled="disabled"/>
									<input name="star6" type="radio" class="star" disabled="disabled"/>
									<input name="star6" type="radio" class="star" disabled="disabled" />
									<input name="star6" type="radio" class="star" disabled="disabled"/>
									<input name="star6" type="radio" class="star" disabled="disabled" checked="checked"/></td>
			        			<td>1</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">11/26/2010</td>
			        			<td>Point of Sale</td>
			        			<td>Setup POS system in training area</td>
			        			<td>$50/time</td>
			        			<td><input name="star7" type="radio" class="star" disabled="disabled"/>
									<input name="star7" type="radio" class="star" disabled="disabled"/>
									<input name="star7" type="radio" class="star" disabled="disabled"/>
									<input name="star7" type="radio" class="star" disabled="disabled"  checked="checked"/>
									<input name="star7" type="radio" class="star" disabled="disabled"/></td>
			        			<td>2</td>
			        		</tr>
			        	</tbody>
			        </table>
	       		</div>
	       		
	       		<div>
			        <table class="jobHistory">
			        	<thead>
			        		<tr>
			        			<th></th>
			        			<th colspan="6" style="text-align: center;">My Client LogBook</th>
			        		</tr>
			        		<tr>
			        			<th>Start</th>
			        			<th>Skill</th>
			        			<th>Headline</th>
			        			<th>Tech Pay</th>
			        			<th>Client Rating</th>
			        			<th>Comments</th>
			        		</tr>
			        	</thead>
			        	<tbody>
			        		<tr>
			        			<td style="text-align: left;">1/3/2011</td>
			        			<td>Point of Sale</td>
			        			<td>Setup POS system in training area</td>
			        			<td>$50/time</td>
			        			<td><input name="star8" type="radio" class="star" disabled="disabled"/>
									<input name="star8" type="radio" class="star" disabled="disabled"/>
									<input name="star8" type="radio" class="star" disabled="disabled" checked="checked"/>
									<input name="star8" type="radio" class="star" disabled="disabled"/>
									<input name="star8" type="radio" class="star" disabled="disabled"/></td>
			        			<td></td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">1/3/2011</td>
			        			<td>Point of Sale</td>
			        			<td>Setup POS system in training area</td>
			        			<td>$50/time</td>
			        			<td></td>
			        			<td></td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">1/3/2011</td>
			        			<td>Point of Sale</td>
			        			<td>Setup POS system in training area</td>
			        			<td>$50/time</td>
			        			<td><input name="star9" type="radio" class="star" disabled="disabled"/>
									<input name="star9" type="radio" class="star" disabled="disabled" checked="checked"/>
									<input name="star9" type="radio" class="star" disabled="disabled" />
									<input name="star9" type="radio" class="star" disabled="disabled"/>
									<input name="star9" type="radio" class="star" disabled="disabled"/></td>
			        			<td>1</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">12/29/2010</td>
			        			<td>Point of Sale</td>
			        			<td>Setup POS system in training area</td>
			        			<td>$50/time</td>
			        			<td></td>
			        			<td></td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">12/28/2010</td>
			        			<td>Point of Sale</td>
			        			<td>Setup POS system in training area</td>
			        			<td>$50/time</td>
			        			<td></td>
			        			<td></td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">12/27/2010</td>
			        			<td>Point of Sale</td>
			        			<td>Setup POS system in training area</td>
			        			<td>$50/time</td>
			        			<td><input name="star10" type="radio" class="star" disabled="disabled"/>
									<input name="star10" type="radio" class="star" disabled="disabled" checked="checked"/>
									<input name="star10" type="radio" class="star" disabled="disabled" />
									<input name="star10" type="radio" class="star" disabled="disabled"/>
									<input name="star10" type="radio" class="star" disabled="disabled"/></td>
			        			<td>2</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">12/27/2010</td>
			        			<td>Point of Sale</td>
			        			<td>Setup POS system in training area</td>
			        			<td>$50/time</td>
			        			<td></td>
			        			<td></td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">12/15/2010</td>
			        			<td>Point of Sale</td>
			        			<td>Setup POS system in training area</td>
			        			<td>$50/time</td>
			        			<td><input name="star11" type="radio" class="star" disabled="disabled"/>
									<input name="star11" type="radio" class="star" disabled="disabled" />
									<input name="star11" type="radio" class="star" disabled="disabled" />
									<input name="star11" type="radio" class="star" disabled="disabled"/>
									<input name="star11" type="radio" class="star" disabled="disabled" checked="checked"/></td>
			        			<td>2</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">11/30/2010</td>
			        			<td>Point of Sale</td>
			        			<td>Setup POS system in training area</td>
			        			<td>$50/time</td>
			        			<td></td>
			        			<td>1</td>
			        		</tr>
			        		<tr>
			        			<td style="text-align: left;">11/26/2010</td>
			        			<td>Point of Sale</td>
			        			<td>Setup POS system in training area</td>
			        			<td>$50/time</td>
			        			<td><input name="star12" type="radio" class="star" disabled="disabled"/>
									<input name="star12" type="radio" class="star" disabled="disabled" />
									<input name="star12" type="radio" class="star" disabled="disabled" />
									<input name="star12" type="radio" class="star" disabled="disabled"/>
									<input name="star12" type="radio" class="star" disabled="disabled" checked="checked"/></td>
			        			<td>2</td>
			        		</tr>
			        	</tbody>
			        </table>
	       		</div>
	       		
	       	</div>
	      </div> <!-- end fslogbook -->
	      
	      <div id="expertise" style="display:none;">
	      	<table class="jobHistory">
			        	<thead>
			        		<tr>
			        			<th>Available</th>
			        			<th>Skill Category</th>
			        			<th>Work Orders</th>
			        			<th>Comments</th>
			        			<th>Favorable Rating</th>
			        		</tr>
			        	</thead>
			        	<tbody>
			        		<tr>
			        			<td><img align="absmiddle" src="http://dev.fieldsolutions.com/widgets/images/check_green.gif" /></td>
			        			<td style="text-align: left;">Cat5 Cabling</td>
			        			<td>632</td>
			        			<td>2</td>
			        			<td id="expRating1"></td>
			        		</tr>
			        		<tr>
			        			<td><img align="absmiddle" src="http://dev.fieldsolutions.com/widgets/images/check_green.gif" /></td>
			        			<td style="text-align: left;">Point of Sale</td>
			        			<td>365</td>
			        			<td>3</td>
			        			<td id="expRating2"></td>
			        		</tr>
			        		<tr>
			        			<td><img align="absmiddle" src="http://dev.fieldsolutions.com/widgets/images/check_green.gif" /></td>
			        			<td style="text-align: left;">Site Survey</td>
			        			<td>285</td>
			        			<td></td>
			        			<td id="expRating3"></td>
			        		</tr>
			        		<tr>
			        			<td><img align="absmiddle" src="http://dev.fieldsolutions.com/widgets/images/check_green.gif" /></td>
			        			<td style="text-align: left;">Desktop/Laptop</td>
			        			<td>251</td>
			        			<td>1</td>
			        			<td id="expRating4"></td>
			        		</tr>
			        		<tr>
			        			<td><img align="absmiddle" src="http://dev.fieldsolutions.com/widgets/images/check_green.gif" /></td>
			        			<td style="text-align: left;">Wireles Networking</td>
			        			<td>245</td>
			        			<td>1</td>
			        			<td></td>
			        		</tr>
			        		<tr>
			        			<td><img align="absmiddle" src="http://dev.fieldsolutions.com/widgets/images/check_green.gif" /></td>
			        			<td style="text-align: left;">Low Voltage</td>
			        			<td>211</td>
			        			<td></td>
			        			<td></td>
			        		</tr>
			        		<tr>
			        			<td><img align="absmiddle" src="http://dev.fieldsolutions.com/widgets/images/check_green.gif" /></td>
			        			<td style="text-align: left;">Kiosk</td>
			        			<td>45</td>
			        			<td></td>
			        			<td></td>
			        		</tr>
			        		<tr>
			        			<td><img align="absmiddle" src="http://dev.fieldsolutions.com/widgets/images/check_green.gif" /></td>
			        			<td style="text-align: left;">Digital Signage</td>
			        			<td>42</td>
			        			<td>1</td>
			        			<td></td>
			        		</tr>
			        		<tr>
			        			<td><img align="absmiddle" src="http://dev.fieldsolutions.com/widgets/images/check_green.gif" /></td>
			        			<td style="text-align: left;">Home Networking</td>
			        			<td>9</td>
			        			<td></td>
			        			<td></td>
			        		</tr>
			        		<tr>
			        			<td><img align="absmiddle" src="http://dev.fieldsolutions.com/widgets/images/check_green.gif" /></td>
			        			<td style="text-align: left;">General Wiring</td>
			        			<td>0</td>
			        			<td></td>
			        			<td></td>
			        		</tr>
			        	</tbody>
			        </table>
	      	</div> <!-- end expertise -->
  

<?php require ("../footer.php"); ?>