<?php
$includepA = true;

$page = 'clients';
$option = 'dashboard';
$selected = 'find';


$Company_ID = (!empty($_GET['v']) ) ? $_GET['v'] : $_SESSION['Company_ID'];
$UserType = (!empty($_SESSION['UserType']) ) ? $_SESSION['UserType'] : NULL;
$isFLSManager = (strtolower($Company_ID) == 'fls' && (strtolower($UserType) === 'install desk' || strtolower($UserType) === 'manager'));

if ($isFLSManager) {
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
} else {
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
}

if (empty($_GET["simple"])){
	require ("../header.php");
	require ("../navBar.php");
	require ("includes/adminCheck.php");
}
else {
	require ("../headerSimple.php");
?>      
	<body>
<?php
}
	// This page has a back button. wosViewTechProfileSimple.php doesn't
	$displayPMTools = false;
	require_once ("includes/PMCheck.php");
	
	require_once("../library/caspioAPI.php");
	$techID = (int)$_GET['TechID'];
	
    $companyID = $_GET['v'];
    $clientID = $_SESSION['ClientID'];
    $isEditCert = false;
    $Core_Api_User = new Core_Api_User();
    $Core_Api_User->loadById($clientID);
    if($Core_Api_User->getCompanyId() == 'MDS' || $Core_Api_User->isPM())
    {
        $isEditCert = true;
    }    
    
    $apiGPM = new  Core_Api_Class();
$isPM = $apiGPM->isGPM($clientID);
    
    //13912
    if($isPM) {
        $otherCompanyTag = 1;
        $callerIsGPM=1;

    } else {
        $otherCompanyTag = 0;
        $callerIsGPM=0;
    }
    $FSTagClass = new Core_Api_FSTagClass();
    $extParas = array('SortByTitle'=>1);
    $extParas_1 = array('UpdateData'=>'NO');//13910
    
    //839
    $FSTagList = $FSTagClass->getFSTagList_forTech_forCompany($techID,$Company_ID,$otherCompanyTag,1,$callerIsGPM,null,1,$extParas);
    $FSTagList_1 = $FSTagClass->getFSTagList_forTech_forCompany($techID,$Company_ID,$otherCompanyTag,1,$callerIsGPM,null,1,$extParas_1);//13910    
    //end 839
    //end 13912
   
    $likeName ="Like";
    $dataLike = Core_Tech::getTechLikes($techID, $clientID);
    if(!empty($dataLike))
    {
       if(!empty($dataLike['liked']))
       { 
            $likeName = $dataLike['liked'] ? "Unlike" : "Like";
       }
    } 
    $totalLikes = Core_Tech::getTechNetLikes($techID);
    $totalLikes = $totalLikes['numNetLikes'];
    $imageThumbs = '/widgets/images/thumbs-up.png';//14027
    if($totalLikes < 0)
    {
        $imageThumbs = '/widgets/images/thumbs-down.png';
    }
                  
?>
<!-- Add Content Here -->
<link rel="stylesheet" type ="text/css" href="/templates/<?=$siteTemplate?>/main.css?24012011" />
<link rel="stylesheet" type="text/css" href="/widgets/css/popup.css" />
<link rel="stylesheet" type="text/css" href="/widgets/css/details.css?22022011" />
<!--script src="/widgets/js/functions.js"></script-->
<?php script_nocache ("/widgets/js/functions.js"); ?>
<script src="/widgets/js/jquery-ui-1.8.min.js"></script>
<link rel=stylesheet type="text/css" href="/widgets/css/main.css" />
<!--link rel="stylesheet" href="<?=empty($_SERVER['HTTPS']) ? 'http' : 'https'?>://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/ui-lightness/jquery-ui.css" type="text/css" /-->
<link rel="stylesheet" href="/widgets/css/jquery-ui.css" type="text/css" />
<link rel="stylesheet" href="/widgets/css/blueprint/screen.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="/widgets/css/blueprint/print.css" type="text/css" media="print" />
<script src="/widgets/js/jquery.tools-1.2.5.min.js"></script>
<script src="/widgets/js/jquery.progressbar/js/jquery.progressbar.js"></script>
<script type="text/javascript" src="/widgets/js/star-rating/jquery.rating.js"></script>
<!--script type="text/javascript" src="/widgets/js/FSWidget.js"></script-->
<?php script_nocache ("/widgets/js/FSWidget.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSWidgetDashboard.js"></script-->
<?php script_nocache ("/widgets/js/FSWidgetDashboard.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSClientTechProfile.js"></script-->
<?php script_nocache ("/widgets/js/FSClientTechProfile.js"); ?>
<script type="text/javascript" src="/widgets/js/base64.js"></script>
<script src="/widgets/js/jquery.expander.min.js"></script>
<script src="/library/jquery/calendar/jquery-calendar.js"></script>
<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="/widgets/css/tabs.css" />
<style type="text/css">@import url(../library/ui.datepicker.css);</style>
<script type="text/javascript" src="../library/ui.datepicker.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.tablescroll.js"></script>
<script type="text/javascript" src="/library/jquery/jquery.bt.min.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.easing.pack.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-mod.js"></script>

<link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" href="/widgets/js/star-rating/jquery.rating.css" />
<!--script type="text/javascript" src="../techs/js/copier_skills_assessment.js"></script-->
<?php script_nocache ("/techs/js/copier_skills_assessment.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script-->
<?php script_nocache ("/widgets/js/FSPopupAbstract.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script-->
<?php script_nocache ("/widgets/js/FSPopupRoll.js"); ?>
<script type="text/javascript" src="../techs/js/BlackBox.js"></script> 
  <!--[if lt IE 8]>
    <link rel="stylesheet" href="/widgets/css/blueprint/ie.css" type="text/css" media="screen, projection" />
  <![endif]-->
<script type="text/javascript">
//$.getScript('/widgets/js/star-rating/jquery.rating.js');
	var _company = '<?=$_GET['v'] ?>';
        //624
        var _C = '<?=$_GET['C'] ?>';
        //end 624
        
        //910
        var _Cl = '<?=$_GET['cl'] ?>';
        //end 910
<?php 
if ($_GET['search'] != "")
{
?>
	var search = '<?=$_GET['search']?>';
	var v = '<?=$_GET['v'] ?>';
	var _company = '<?=$_GET['v'] ?>';
	var httpReferer = "/clients/techs.php?v="+v+"&search="+search;
	
<?php } else
{
    ?>
	
	var httpReferer = "<?php echo (isset($_GET["referer"]) ? $_GET["referer"] : $_SERVER['HTTP_REFERER'])?>";
	
<?php } ?>
<?php if (empty ($_GET["popup"])): ?>
	var isPopup = false;
<?php else: ?>
	var isPopup = true;
<?php endif; ?>
</script>

<script type="text/javascript">
var techProfile;
<?
if (isset($_GET['fFrame']) && !empty($_GET['fFrame']))
{
// force frame , lightbox doesn't detect as frame
    ?>
var isFrame = true;
var forceFrame = true;
    <?
} else
{
    ?>
var isFrame = window != window.top;
var forceFrame = false;
    <?
}
?>
var isPM = <?=$isPM?1:0;?>

function goBack () {
	if (document.referrer) {
		if (document.referrer.indexOf('wos.php')) {
			location.replace("/clients/wos.php?v=" + window._company);
		}
		else
			document.location = document.referrer;
	}
	else {
		location.replace("/clients/wos.php?v=" + window._company);
	}
}

function getStyle (selector, file_index) {
	var length, result = false;

	if (document.styleSheets) {
		if (typeof (file_index) != "undefined") {
			var rules;

			if (document.styleSheets[file_index].cssRules)
				rules = document.styleSheets[file_index].cssRules;
			else if (document.styleSheets[file_index].rules)
				rules = document.styleSheets[file_index].rules;
			else
				rules = new Array();

	 		for (n in rules) {
	 			if (rules[n].selectorText == selector) {
					if (!rules[n].style) rules[n].style = {};

					result = rules[n].style;
	            }
			}
									}
									else {
			file_index = 0;
			length = document.styleSheets.length;

			while (file_index < length && result === false) {
				result = getStyle (selector, file_index++);
			}
        }
    }

 	return result;
}

function commentDone() {
	setTimeout("$.fancybox.close();", 1000);
	techProfile.loadTechCommentsInfo();
					}
		
function viewSchedule(url){
	newwindow=window.open(url,'name','height=500, width=500, left=100, top=100, resizable=yes, scrollbars=yes, toolbar=no,status=no');
	if (window.focus) {newwindow.focus()}
						}

//13931
function fillDateMultiLevel(name,value) {
    var fstag='';
                fstag = name;
                fstag = fstag.substring(0, fstag.length-13);    
                if(value!='')
                {                   
                    $("#"+fstag+"Datetxt").val($.datepicker.formatDate('mm/dd/yy', new Date()));
                }
                else
                {
                    $("#"+fstag+"Datetxt").val('');
                }			
}

$('document').ready(function()
{
	
	var sortableStyle = getStyle (".table_head th.sortable");
	var ref = document.referrer.split("?")[0];
	
	if (sortableStyle) sortableStyle.fontWeight = "normal";

		if (ref.match("wosDetails.php")){
			$("body").css("width", "800px");
			$("body").css("margin", "0");
			$("body").css("overflow", "auto");
			$("#headerSlider").css("display", "none");
			$("#maintab").css("display", "none");
			$("#subSectionLabel").css("display", "none");
			$("#PMCompany").css("display", "none");
			$("table.form_table").css("margin", "0");
			$("table.form_table").css("padding", "0");
			$("table.form_table").css("width", "400px");
			$("div.footer_menu").css("display","none");
			$("#content").css("margin", "0");
			$("#tabcontent").css("display", "none");
			$("#searchForm").css("width", "800px");
			$("#mainContainer").css("width", "800px");
			$("#mainContainer").css("margin", "0");
			$("form#caspioform + div").css("display", "none");
		}
		
		 $("body").data("isCollapse",false);
		 
		 $("#hideUnhide").click(function(){
				if($("body").data("isCollapse") == false){

					$("#collapseContent").html($("#moveContent").html());
				}else{
				}
});

			techProfile = new FSClientTechProfile({container: 'wr', tab: 'client', companyID: '<?=$_GET['v'] ?>',  techId: '<?=$_GET['TechID']?>', clientID: '<?=$_SESSION['ClientID']?>'});
			techProfile.show({tab: 'client', params: {container: 'wr'}});

    if(_C==1)
    {
        $(".trustedTechSection").css('display',"none");
        changeCenterContent('fsTrustedTech');
        $("#leftNav a").each(function()
        {
            $(this).removeClass("currentLink");
	});

        $("#lnkfsTrustedTech").addClass("currentLink");
    }
    //910
    else if(_Cl==1)
    {
        changeCenterContent('comments');
        $("#leftNav a").each(function()
        {
            $(this).removeClass("currentLink");
	});

        $("#lnkfscomments").addClass("currentLink");
    }
    //end 910
    else
    {
			 changeCenterContent('expertise');
    }

    
    $("#leftNav a").click(function()
    {
        $("#leftNav a").each(function()
        {
					 $(this).removeClass("currentLink");
				 });
				 
				 $(this).addClass("currentLink");
			 });
                        function ClosePopupClientDenyTech(){
                            jQuery("#isDenied").removeAttr("checked");
                            $("#deniedLabel").css('color', "")
                                .css("font-weight", "")
                                .css("text-decoration","none")
                                .css("cursor","");
                        }
			 $("#isDenied").change(function(){
                            var isprefer = $("#isPreferred").is(':checked');
                            if($(this).attr("checked") == true)
                            {
					 //techProfile.updateTechDenied("add", '<?=$_SESSION['ClientID']?>');
                                        var  companyID="<?= $companyID?>";                                                                                                   
                                var  url="/widgets/dashboard/details/update-client-deny-tech?techID=" + <?=$techID?> +"&clientID="+<?=$clientID?>+"&companyID="+companyID + "&isprefer="+isprefer;
                                         $("<div id='ClientDenyTech'></div>").fancybox(
                                            {
                                            'type' : 'iframe',
                                            'href' : url,
                                            'autoDimensions' : false,
                                            'width' : 450,
                                            'height' : 180,
                                            'transitionIn': 'none',
                                            'transitionOut' : 'none'
                                }).trigger('click'); 
                                        jQuery("#fancybox-close")
                                            .unbind("click",ClosePopupClientDenyTech)
                                            .bind("click",ClosePopupClientDenyTech);
                            $("#edit_DenyTechV",parent.document).attr("checked",true);
                            $("#edit_PreferTechV",parent.document).attr("checked",false);
                            }
                            else
                            {
                            	$("#edit_DenyTechV",parent.document).attr("checked",false);
					 techProfile.updateTechDenied("remove", '<?=$_SESSION['ClientID']?>');
				 }
			 });
                         jQuery("#deniedLabel").click(function(){
                            var isClientDenyCheck=jQuery("#isDenied").is(":checked");
                            if(isClientDenyCheck){

                                this.rollWarning = new FSPopupRoll();
                                var content = "<div id='ContentClientDeny'><div style='text-align:center;height:150px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
                                var opt = {
                                    width       : 250,
                                    height      : '',
                                    position    : 'middle',
                                    title       : 'This technician was client denied by',
                                    body        : content
                                };
                                this.rollWarning.showNotAjax(this, this, opt);
                                data={
                                    companyID:"<?= $companyID?>"
                                    ,techID:<?=$techID?>
                                    ,clientID:<?=$clientID?>
                                };                            
                                $.ajax({
                                    url : "/widgets/dashboard/details/view-client-deny-tech",
                                    data : data,
                                    cache : false,
                                    type : 'POST',
                                    dataType : 'html',
                                    context : this,
                                    error : function (xhr, status, error) {                                    
                                    },
                                    success : function(data, status, xhr) {
                                        jQuery("#ContentClientDeny").html(data);
                                        var  clientUserName=jQuery("#ContentClientDeny #ClientName").html();
                                        if(clientUserName==""||clientUserName==null)                                    
                                        {
                                            jQuery("#ClientName").css("display","none").css('padding-bottom','20px');
                                            jQuery(".popup-header-title").html("Comments:");
                                            jQuery("#CommentsTitle").css("display","none");
                                        }

                                    }
                                });
                            }
                         });

			 $("#isPreferred").change(function()
              {
              	if($(this).attr("checked") == true)
                {
                	if($("#isDenied").is(':checked'))
                    {
                    	techProfile.updateTechDenied("remove", '<?=$_SESSION['ClientID']?>');
                        jQuery("#isDenied").attr('checked',false);
                    }
					 techProfile.updateTechPreferred("add");
					 $("#edit_PreferTechV",parent.document).attr("checked",true);
					 $("#edit_DenyTechV",parent.document).attr("checked",false);
                }else{
					 techProfile.updateTechPreferred("remove");
					 $("#edit_PreferTechV",parent.document).attr("checked",false);
				 }
			 });

			
				/*
			 $(".jobHistory input").each(function(index,value){
					$(this).attr("disabled","disabled");
				});*/

	if (isFrame) {
		//$("#backBtn").css ("display", "none");
		try {
			if (!isPopup) {
                                
				if ($("breaklineFooter")) $(".breaklineFooter > br").hide ();

				$("#backBtn").removeAttr ("onclick");
				parent.$("#loading").hide ();
				$("#backBtn").val ("Show Profile");
				$(".span-5").hide ();
				$(".span-16").hide ();
				$(".footer_menu").hide ();
				$("body > br").hide ();
				$("#backBtn").click (function () {
					parent.$("<div></div>").fancybox({
						"autoDimensions": false,
						"width": "90%",
						"height": "100%",
						"transitionIn": "none",
						"transitionOut": "none",
						"type": "iframe",
						"href": window.location + "&popup=1",
							"onClosed"  : function() {window.location.reload();}
					}).trigger("click");
				});
			}
			else {
                            //forceFrame
				if (forceFrame && '<?= empty($_GET["back"])?0:1 ?>' == '1') {
     
                    $("#backBtn").removeAttr("onclick");
                    $("#backBtn").click( function() {
                            window.parent.openLightBoxPage('<?=$_GET['prev']?>?v=<?=$_GET['v']?>&win=<?=$_GET['win']?>&clientWO=<?=$_GET['clientWO']?>&zip=<?=$_GET['zip']?>&distance=<?=$_GET['distance']?>&c=<?=$_GET['c']?>', 1024, 600);
                            
                    });
                } 
                else {
                            if('<?= $_GET["back"] ?>' == '1' && isPopup){
                                $("#backBtn").removeAttr("onclick");
                                $("#backBtn").unbind("click");
                                $("#backBtn").click( function() {
                                window.location = '<?=$_GET['prev'] ?>';
                                });
                            }else{
                            $("#backBtn").hide ();
                            }
			}
                if (forceFrame && '<?= empty($_GET["backtolist"])?0:1 ?>' == '1')
                {
                    if(isPM==0)
                    {
                    $("#backtolist").removeAttr("onclick");
                    $("#backtolist").click( function()
                    {
                        window.parent.openLightBoxPage('<?=$_GET['prev']?>?v=<?=$_GET['v']?>&win=<?=$_GET['win']?>&clientWO=<?=$_GET['clientWO']?>&zip=<?=$_GET['zip']?>&distance=<?=$_GET['distance']?>&c=<?=$_GET['c']?>', 1000, 600);
                    });
		}
		}
		}
        }
	catch (e)
        {
	}
    } else
    {
		if ('<?= empty($_GET["back"])?0:1 ?>' != '1')
		if ('<?= empty($_GET["back"])?0:1 ?>' != '1')
		if ('<?= empty($_GET["back"])?0:1 ?>' != '1')
            $("#backBtn").hide ();
	}
});

function changeCenterContent(id, interval){
        
	if($("#"+id).length != 0)
	{
	$("#centerContent").html($("#"+id).html());
	}
	

	/*
	$(".jobHistory input").each(function(index,value){
		$(this).attr("disabled","disabled");
	});
	*/
	
	if(id == "fslogbook"){
		setupLogbookTabs();
		techProfile.loadTechLogbookStats(interval);
		$("#fsLogbook12Month").addClass("current");
		$("#logbookTabs a").click(function(){
			 $("#logbookTabs a").each(function(){
				 $(this).removeClass("current");
			 });
			 $(this).addClass("current");
		 });
	
	}
	if(id == "expertise"){
		setupRatings();
		$("#expTabs").tabs("div.expPane > div");
		techProfile.loadTechExpertiseInfo();
	}
	if(id == "comments"){
		$("#commentsTabs").tabs("div.commentsPane > div");
		$('#addCommentBtn').show();
		techProfile.loadTechCommentsInfo();
		techProfile.loadWoCommentsInfo();
	}
	if(id == "toolsResources"){
		setupEquipTabs();
		techProfile.loadTechEquipmentInfo();
	}

    if(id == "certifications"){
        $("#certTabs").tabs("div.certificationsPane > div.certificationsSection");
    }
    
	if(id == "fsTrustedTech"){
        $("#fsTrustedTechTabs").tabs("div.fsTrustedTechPane > div.trustedTechSection");
            if(_C==1)
            {
                $("#InsurancesCredentials").attr('class','');
                $(".fsTrustedTechPane .trustedTechSection").css('display','none');
                $("#clientCredentials").attr('class','current');
                $("#fsTrustedTechForm").parent().css('display','');
                $("#editSaveBtns").show();
            }
            
		$(".fsTrustedTechTab a").click(function(){
			if($(this).attr("id") == "clientCredentials"){
				$("#editSaveBtns").show();
			}else{
				$("#editSaveBtns").hide();
			}
		});
                $("#publicEmailtoFS").click(function(){
                    var _clientId = <?php echo($clientID); ?>;
                    var _techId = <?php echo($techID); ?>;                        
                    $.post("/widgets/dashboard/client/sendemailtofs/rt/iframe/", 
                    {rt: 'iframe',clientId:_clientId,techId:_techId}, 
                    function (response) {
                        if(response == 1)
                        {
                            var messageHtml = "<h2>Success</h2>";
                            messageHtml += "<p>Your request for this technician's Certificate of Insurance has been sent to your FieldSolutions Account Manager.</p>";
                            messageHtml += "<b>Thank you!</b>";   
                            $("<div></div>").fancybox({
                                'width': 400,
                                'height' : 125,
                                'content' : messageHtml,
                                'autoDimensions' : false,
                                'centerOnScroll' : true
                            }).trigger('click');
                        }else
                        {
                            var messageHtml = "<h2>Send Email</h2>";
                            messageHtml += "<p>Send email unsuccessfully.</p>";
                            messageHtml += "<b>Thank you!</b>";   
                            $("<div></div>").fancybox({
                                'width': 350,
                                'height' : 125,
                                'content' : messageHtml,
                                'autoDimensions' : false,
                                'centerOnScroll' : true
                            }).trigger('click');
                        }      
                    });            
                    return false;                
                });

		
		$("#flsIdnput, #flsWhseInput, #flsCSPInput").attr("disabled", false);
	}
        if(id == "techSchedule"){
            techProfile.loadTechSchedule('schedule_FS');
        }
	
}

function viewclientcredentical()
{
    $('.trustedTechSection').css('display','none');
    changeCenterContent('fsTrustedTech');
    $('#leftNav a').each(function()
    {
        $(this).removeClass('currentLink');
    });
    $('#lnkfsTrustedTech').addClass('currentLink');
    $('#InsurancesCredentials').attr('class','');
    $('.fsTrustedTechPane .trustedTechSection').css('display','none');
    $('#clientCredentials').attr('class','current');
    $('#fsTrustedTechForm').parent().css('display','');
    $('#editSaveBtns').show();
}

function setupLogbookTabs() {
	// setup ul.tabs to work as tabs for each div directly under div.panes
	//$("#logbookTabs").tabs("div.fsLogbookPanes > div");
}

function setupRatings(){
	$("#expRating1").progressBar(35);
	$("#expRating2").progressBar(65);
	$("#expRating3").progressBar(75);
	$("#expRating4").progressBar(85);
}

function setupEquipTabs(){
	$("#toolsTabs").tabs("div.toolsPane > div.toolsSection");		
}
function ClosePopUpClientDenyTechAfterSave(){
        var value=$("#edit_DenyTechV").is(":checked");
        if(value)
            $("#edit_PreferTechV").removeAttr('checked');
        setTimeout("$.fancybox.close();", 1000);
        $("#deniedLabel").css('color', "#F69630")
            .css("font-weight", "bold")
            .css("text-decoration","underline")
            .css("cursor","pointer");

}
</script>
<style type="text/css">
table { font-size: 12px; border: 0; }

.reportedAbuse {
	color: #D00;
}
.helpfulHighlight {
	background-color: #4790D4;
}
.jobHistory{
	color: #000;
}
.jobHistory th {
	background-color: #4790D4;
	border: 1px #FFFFFF solid;
	/*color: #000000;*/
	padding: 5px 0 5px 0;
	text-align: center;
}
.jobHistory td{
	background-color: #F0F8FF;
	border: 1px #FFFFFF solid;
	font-size: 11px;
	text-align: center;
}
.jobHistory td:first-child{
	text-align: left;
	padding-left: 4px;
}
.jobHistory tr{
	height: 20px;
}
.skillsCat{
	text-align: left;
}
.underline{
	text-decoration: underline;
}
.woAssign{
	color:	#FF0000;
	float: right;
	margin: 0 5px 0 5px;
}
.wo{
	margin-bottom: 10px;
}

	/* tab pane styling */
.panes div {
	display:none;		
	padding:1px 0px;
	/*border:1px solid #999;*/
	border-top:0;
	height:100px;
	font-size:14px;
	background-color:#fff;
}

#socialmedia{
	border-collapse: collapse;
}
#socialmedia td{
	margin: 0 2px 0 2px;
}

h1, h2{
color: #595959;
}

#leftNav li{
  background: url("/widgets/images/triangle.gif") no-repeat scroll left 1px transparent;
  margin: 10px 0;
  padding: 0 0 0 20px;
  line-height: 21px;
}

#leftNav .currentLink{
	color: #595959;
}

a, .columnLink{
	cursor: pointer;
}

#fsLogbookColumns th{
	color: #FFF;
}

body{
	font-size: 12px;
}

.container{
	width: 990px;
	border-right: 1px solid #DDD;
}
.isoRow{
	display:none;
}

.contactIsoInfo{
	display:none;
}
.indent{
	padding-left: 15px;
}
div#loader{
	display: none;
	width:100px;
	height: 100px;
	position: fixed;
	top: 50%;
	left: 50%;
	background:url(/widgets/images/loader.gif) no-repeat center #fff;
	text-align:center;
	padding:10px;
	font:normal 16px Tahoma, Geneva, sans-serif;
	border:1px solid #666;
	margin-left: -50px;
	margin-top: -50px;
	z-index:2;
	overflow: auto;
}

.tableHeader{
	color: #FFFFFF;
}

hr{
	margin: 0 0 5px;
}

.clearfix:after,.container:after {
	clear:both;
}
.tableCellLeftAlign{
text-align: left;
}

#leftAlignTable td{
	text-align: left;
}
.telephonyTable td, .telephony3Column td{
	text-align: left;
}
#expTabs a{
	width: 186px;
}
#logbookTabs a{
	width: 102px;
}
#toolsTabs a{
	width: 150px;
}
#fsTrustedTechTabs a{
	width: 204px;
}
.telephonyIndent{
	padding-left: 50px;
}
.borderLeft{
	border-left: 1px solid #DDDDDD;
    margin-left: 5px;
    padding-left: 4px;
}
ul.tabs a{
	color: #FFF;
}
ul.tabs a.current{
	border-bottom: 3px solid #F69630;
	color: #FFF;
}
ul.tabs a:hover{
	color: #000;
}
ul.tabs a:hover {
	text-decoration: underline;
	background-color: #FFC24B;
}
ul.tabs a.current {
	background-color: #F69630;
    border-bottom: 1px solid #F69630;
    color: #ffffff;
    cursor: default;
    padding-top: 5px;
    height: 22px;
    top: 0;
}
ul.tabs a {
    color:#385C7E;
    padding: 5px 10px 6px;
    border-width: 0;
}
div.tabsbottom {
    height: 4px;
}
#clientCredentialsEditLink{
	color: #6699CC;
	font-size: 14px;
}
#clientCredentialsEditLink:hover{
	color: #0000FF;
}
        .divInsurances {
            background-color: #F0F8FF;
            padding-bottom: 5px;
            padding-left: 5px;
            padding-top: 5px;
        }
.hoverText {
	padding: 0 1px 0 1px;
}
</style>
<div id="loader">
	Loading...
	</div>

<div class="container" style="padding-top: 8px;">
  
  <!-- TOP ROW -->   
            
  	  <div class="span-24 last">
               <?php if(!empty($_GET["backNotices"])){ ?>
              <input value="Back" class="link_button_popup" type="button" onClick="<?= base64_decode($_GET['backclick']) ?>" />
        <?php } ?>
		<input style="margin-left: 5px;margin-bottom: 5px;height: 22px;" class="link_button middle2_button" name="backBtn" id="backBtn" type="button" value="Go Back" onClick="goBack();" />	
                
	</div>
  <div style="<?= !empty($_REQUEST["backtolist"])?"margin-bottom: 10px;":"display:none;" ?>">
      <a style="text-decoration: underline;"  id="backtolist" onClick="window.parent.$.fancybox.close();" >Back To List</a>
  </div>  
      <div class="span-4 border">
        <div>
        	<img id="badgePhotoImage" src="/widgets/images/profileAnon.jpg" width="150" height="145" />
        	<div style="background-color: #4790D4; text-align: center; color: #FFF;"><span id="FirstnameLabel"></span> <span id="LastnameLabel"></span>
        		<br />Day: <span id="PrimaryPhoneLabel"></span>
        		<span id="EvePhone"><br />Eve: <span id="SecondaryPhoneLabel"></span></span>
</div>
        </div>
              
      	<table id="socialmedia">
      		<tr>
      			<td>
      				<span id="Email" bt-xtitle="Send tech an email"><a href="javascript:void(0);" target="_blank"><img src="/widgets/css/images/socialmediaicons/email_icon.png" alt="Email" /></a></span>
      				<span id="SMS" bt-xtitle="Send tech a text message"><a href="javascript:void(0);" target="_blank"><img src="/widgets/css/images/socialmediaicons/sms_icon.png" alt="Youtube" /></a></span>
      				<span id="Twitter"><a href="javascript:void(0);" target="_blank"><img src="/widgets/css/images/socialmediaicons/twitter-24x24.png" alt="Follow Me On Twitter" /></a></span>
      				<span id="Facebook"><a href="javascript:void(0);" target="_blank"><img src="/widgets/css/images/socialmediaicons/facebook-24x24.png"  alt="Visit Me On Facebook!" /></a></span>
      				<span id="LinkedIn"><a href="javascript:void(0);" target="_blank"><img src="/widgets/css/images/socialmediaicons/linkedin-24x24.png" alt="LinkedIn" /></a></span>
      				<span id="YouTube" style="display:none;"><a href="javascript:void(0);" target="_blank"><img src="/widgets/css/images/socialmediaicons/youtube-24x24.png" alt="Youtube" /></a></span>
      			</td>
      		</tr>
      	</table>
      </div>
       
        <div class="span-6 border">
	      		<table>
	      			<tr>
	      				<td><h1 style="text-align: left;">At-A-Glance</h1></td>
	      			</tr>
	      			<tr>
	      				<td>FS-Tech ID#: <span id="TechIDLabel"></span></td>
	      			</tr>
                <?php
                if ($companyID == "FLS")
                {
                    ?>

	      			<tr>
	      				<td>FLS ID#: <span id="FLSIDLabel"></span> / FLS Status: <span id="FLSstatusLabel"></span></td>
	      			</tr>
	      			<?php } ?>
	      		</table>
	      	
		      <table style="line-height: 19px;">
		      	<tr><td><br /></td></tr>
		      	<tr>
		      		<td colspan="3"><h1 style=" text-align: center;">Work History</h1></td>
		      	</tr>
		      	<tr>
		      		<td></td><td><h2 style="text-decoration: underline;">12 Months</h2></td><td><h2 style="text-decoration: underline;">Lifetime</h2></td>
		      	</tr>
		      	<tr>
		      		<td>Total Calls:</td><td id="total12Months"></td><td id="totalLifetime"></td>
		      	</tr>
		      	<tr>
		      		<td>Back Outs:</td><td id="backOuts12Months"></td><td id="backOutsLifetime"></td>
		      	</tr>
		      	<tr>
		      		<td>No Shows:</td><td id="noShows12Months"></td><td id="noShowsLifetime"></td>
		      	</tr>
		      	<tr>
		      		<td>Preference:</td><td id="preference12Months"></td><td id="preferenceLifetime"></td>
		      	</tr>
		      	<tr>
		      		<td>Performance:</td><td id="performance12Months"></td><td id="performanceLifetime"></td>
		      	</tr>
		      	<!--  tr>
		      		<td>Comments:</td><td id="comments12Months"></td><td id="commentsLifetime"></td>
		      	</tr -->
		      	<tr>
			  		<td colspan="3">
			  			<table>
			  				<tr>
			  					<td><img src="/widgets/images/thumbs-up.png" width="17" height="22" /></td>
			  					<!-- 13910 -->
                               <!-- <td>Likes (<span id="numNetLikes"></span>)<span id="likeButton"></span></td>-->
                                    <td>(<span id="numNetLikes"><?=$totalLikes;?></span>)<span id="likeButton">
                                            <?
                                            if($likeName=="Like")
                                            {?>
                                                <a href="javascript:techProfile.likeTech()"><?=$likeName;?></a>
                                            <?}  else {?>
                                            <a href="javascript:techProfile.unlikeTech()"><?=$likeName;?></a>                                                
                                            <?}?>
                                           
                                        </span></td>
			  		<!--910-->			
                                        <td><img src="/widgets/images/Commentbubble_icon.png?t=1"/></td>
                                        <!--end 910-->
			  					<td><a href="javascript:techProfile.addComments()">Comment on Tech</a></td>
			  				</tr>
			  			</table>
			  		</td>
			  	</tr>
		      </table>
	      </div>
	   
	       <div class="span-7">
				<table>
			      	<tr>
			      		<td colspan="2" style="text-align: center;"><h1>My Business Statement</h2></td>
			      	</tr>
			      	<tr>
			      		<!--14105-->
			      		<td colspan="2">
						<span id="resumeFile" style="float:left;">Resume</span>
						<span id="MyWebSite" style="float:right;">My Website</span>
						</td>
						<!--End 14105-->
			      	</tr>
			      	<tr>
			      		<td colspan="2" style="padding-bottom: 15px;" id="BusinessStatementSection">
			      			<br />
			      			<div id="SpecificExperienceSection" style="padding-bottom: 8px; width: 270px;word-wrap:break-word;">
			      				<span><h2 style="text-decoration: underline;">Detailed Information:</h2></span>
				      			<span id="SpecificExperienceLabel" class="BusinessStatement"></span>
			      			</div>
			      			<div id="SiteRefSection" style="padding-bottom: 8px; width: 270px;word-wrap:break-word;">
			      				<span><h2 style="text-decoration: underline;">References: </h2></span>
				      			<span id="SiteRefLabel" class="BusinessStatement"></span>
			      			</div>
			      			<div id="EquipExperienceSection" style="padding-bottom: 8px; width: 270px;word-wrap:break-word;">
			      				<span><h2 style="text-decoration: underline;">Equipment Experience: </h2></span>
				      			<span id="EquipExperienceLabel" class="BusinessStatement"></span>
			      			</div>
			      		</td>
			      	</tr>
		      </table>   		         
		  	</div>
                    <!--735-->
		  <div class="span-7 last" style="border-left: 1px solid #DDDDDD;width:300px;"> <!--14028-->
			  <table style="line-height: 14px; margin-left:10px;">  <!--14028-->
			  	<tr>
			  		<td colspan="2"><h2 style=" text-align: center;">FS-TechTags&#8482;</h2></td>
			  	</tr>
                                        <tr>
                                    <td colspan="2" style="padding-top:5px;">
                                            <?
                                        //850
                                        $techInfo = Core_Tech::getProfile($techID, true, API_Tech::MODE_TECH, $companyID);
                                        $fsMobileDate ="";
                                        if(!empty($techInfo["credentialInfo"]))
                                        {
                                            if(!empty($techInfo["credentialInfo"]["fsMobileDate"]))
                                            {
                                                $fsMobileDate = $techInfo["credentialInfo"]["fsMobileDate"];
                                            }
                                        }
                                        //end 850
                                        //13761
                                        $apiTechClass = new Core_Api_TechClass();
                                        //735
                                        $apiFSExpert = new Core_Api_FSExpertClass();
                                        $FSExperts = $apiFSExpert->getFSExpertListForTech($techID);
                                        //end 735
                                        //830
                                        $extsUSAuth = $apiTechClass->getExts($techID);
                                        if ($extsUSAuth["I9Status"] == 2)
                                            {
                                            $USAuth['Title']="Authorized to perform services in the United States";
                                            $USAuth['Date'] = $extsUSAuth['I9Date'];
                                            $USAuth['ExistingImagePath']="/widgets/images/VerifiedTall2.png";
                                            $USAuth['IsExistingTag'] = 1;
                                        
                                                    }
                                        else if($extsUSAuth["I9Status"] == 1)
                                                    {
                                            $USAuth['Title']="Authorization to perform services in the United States is pending";
                                            $USAuth['Date'] = "";
                                            $USAuth['ExistingImagePath']="/widgets/images/PendingTall2.png";
                                            $USAuth['IsExistingTag'] = 1;
                                                    }
                                        //end 830
                                        
                                        //835
                                        $certSearch = new Core_TechCertifications;
                                        $techCertification = $certSearch->getTechCertification($techID);
                                        $cert = $techCertification[$techID];
                                        if (sizeof($cert) > 0 && array_key_exists(4, $cert))
                                        {
                                            $TBCCertInfo = $apiTechClass->getTechCertInfo(4,$techID);
                                            $TBCCert['Title']="Blue Ribbon Technician";
                                            $TBCCert['Date'] = $TBCCertInfo['date'];
                                            $TBCCert['ExistingImagePath']="/widgets/images/BRT_Tag.png";
                                            $TBCCert['IsExistingTag'] = 1;
                                        }
                                        
                                        if (sizeof($cert) > 0 && array_key_exists(3, $cert))
                                        {
                                            $BootCampInfo = $apiTechClass->getTechCertInfo(3,$techID);
                                            $BootCamp['Title']="Boot Camp Certified";
                                            //912
                                            $BootCamp['Name']="BootCampCertified";
                                            //end 912
                                            $BootCamp['Date'] = $BootCampInfo['date'];
                                            $BootCamp['ExistingImagePath']="/widgets/images/Boot_Tag.png";
                                            $BootCamp['IsExistingTag'] = 1;
                                            
                                        }
                                        else if (sizeof($cert) > 0 && array_key_exists(43, $cert))
                                        {
                                            $BootCampInfo = $apiTechClass->getTechCertInfo(43,$techID);
                                            $BootCamp['Title']="Boot Camp Plus";
                                            $BootCamp['Date'] = $BootCampInfo['date'];
                                            $BootCamp['ExistingImagePath']="/widgets/images/Boot_Tag_PLUS.png";
                                            $BootCamp['IsExistingTag'] = 1;
                                        }
                                        //end 835
                                        $BlackBoxCablingStatus = $apiTechClass->GetBlackBoxCablingStatus($techID);
                                        $BlackBoxTelecomStatus = $apiTechClass->getBlackBoxTelecomStatus($techID);
                                        $isdisplayBlackBoxCabling = !empty($BlackBoxCablingStatus) ? 1 : 0;
                                        $isdisplayBlackBoxTelecom = !empty($BlackBoxTelecomStatus) ? 1 : 0;
                                        $isdisplayblackbox = 0;
                                        
                                        if ($isPM || in_array($companyID, array('BBAL', 'BBGC', 'BBGE', 'BBCN', 'BCSST', 'BBGN', 'BBJC', 'BBLC', 'BBNSG', 'BBNC', 'BBS', 'BB')))
                                        {
                                            $isdisplayblackbox = 1;
                                        }
                                        //839
                                        $getFSTagListNew = array();
                                        foreach($FSTagList_1 as $elementKey => $element)
                                        {
                                            if($FSTagList_1[$elementKey]['Name']=="BlackBoxCabling") // && $BlackBoxCablingStatus['Status'] != 'Pending')
                                            {
                                                if($isdisplayblackbox==1)
                                                {
                                                    if($isdisplayBlackBoxCabling==1 )
                                                    {
                                                        $getFSTagListNew[] = $FSTagList_1[$elementKey];
                                                    }
                                                }
                                            }
                                            else if($FSTagList_1[$elementKey]['Name']=="BlackBoxTelecom") // && $BlackBoxTelecomStatus['Status'] != 'Pending')
                                            {
                                                if($isdisplayblackbox==1)
                                                {
                                                    if($isdisplayBlackBoxTelecom==1 )
                                                    {
                                                        $getFSTagListNew[] = $FSTagList_1[$elementKey];
                                                    }
                                                }
                                            }
                                            else if(!empty($FSTagList_1[$elementKey]['TechId']))
                                            {
                                                $getFSTagListNew[] = $FSTagList_1[$elementKey];
                                            }
                                        }

                                        $FSTagNo = count($getFSTagListNew);
                                        
                                        if (!empty($getFSTagListNew))
                                        {
                                            //830
                                            if(!empty($USAuth))
                                            {
                                                array_splice($getFSTagListNew, 0, 0, array($USAuth));
                                            }
                                            //end 830
                                            //835
                                            if(!empty($TBCCert))
                                            {
                                                array_splice($getFSTagListNew, 0, 0, array($TBCCert));
                                            }
                                            if(!empty($BootCamp))
                                            {
                                                array_splice($getFSTagListNew, 0, 0, array($BootCamp));
                                            }
                                            //end835
                                            //580
                                            if(!empty($fsMobileDate))
                                            {
                                                $fsmobile['Title']="I Have FS-Mobile";
                                                //912
                                                $fsmobile['Name']="FSMobile";
                                                //end 912
                                                $fsmobile['Date'] = $fsMobileDate;
                                                $fsmobile['ExistingImagePath']="/widgets/images/fs_mobile_36x32.png";
                                                $fsmobile['IsExistingTag'] = 1;

                                                if(!empty($getFSTagListNew))
                                                {
                                                    array_splice($getFSTagListNew, 0, 0, array($fsmobile));
                                                }
                                                else
                                                {
                                                    $getFSTagListNew=array($fsmobile);
                                                }
                                            }
                                            //end 850
                                            $FSTagNo = count($getFSTagListNew);
                                        }
                                        else
                                        {
                                            //830
                                            if(!empty($USAuth))
                                            {
                                                $getFSTagListNew=array($USAuth);
                                            }
                                            //850
                                            if(!empty($row['FSMobileDate']))
                                            {
                                                $fsmobile['Title']="I Have FS-Mobile";
                                                //912
                                                $fsmobile['Name']="FSMobile";
                                                //end 912
                                                $fsmobile['Date'] = $row['FSMobileDate'];
                                                $fsmobile['ExistingImagePath']="/widgets/images/fs_mobile_36x32.png";
                                                $fsmobile['IsExistingTag'] = 1;
                                            
                                                if(!empty($getFSTagListNew))
                                                {
                                                    array_splice($getFSTagListNew, 1, 0, array($fsmobile));
                                                }
                                                else
                                                {
                                                    $getFSTagListNew=array($fsmobile);
                                                }
                                            }
                                            //end 850
                                            //835
                                            if(!empty($TBCCert))
                                            {
                                                if(!empty($getFSTagListNew))
                                                {
                                                    array_splice($getFSTagListNew, 2, 0, array($TBCCert));
                                                }
                                                else
                                                {
                                                    $getFSTagListNew=array($TBCCert);
                                                }
                                            }

                                            if(!empty($BootCamp))
                                            {
                                                if(!empty($getFSTagListNew))
                                                {
                                                    array_splice($getFSTagListNew, 3, 0, array($BootCamp));
                                                }
                                                else
                                                {
                                                    $getFSTagListNew=array($BootCamp);
                                                }
                                            }
                                            //end 835

                                            //end 830
                                            $FSTagNo = count($getFSTagListNew);
                                            $firstline = $FSTagNo ;
                                            $secondline = 0;
                                        }
                                        
                                        $PurpleTechnicianUnverified = 0;
                                        $PurpleTechnicianCertified = 0;
                                        //912
                                        $defaultnotedit = array('ErgoMotionCertifiedTechnician'=>'ErgoMotionCertifiedTechnician',
                                            'TechForceICAgreement'=>'TechForceICAgreement',
                                            'Dell_MRA_Compliant'=>'Dell_MRA_Compliant',
                                            'BootCampCertified'=>'BootCampCertified',
                                            'FSMobile'=>'FSMobile');
                                        //end 912
                                        foreach($getFSTagListNew as $tagitem)
                                        {
                                            if(!empty($tagitem['FSTagId']))
                                            {
                                                if($tagitem['FSTagId']== 25)
                                                {
                                                    $PurpleTechnicianUnverified = 1;
                                                }
                                                if($tagitem['FSTagId']== 26)
                                                {
                                                    $PurpleTechnicianCertified = 1;
                                                }
                                            }
                                        }
                                        $style = "min-width: 60px";
                                        $field = "";
                                        $field .= "";
                                        $field .= "";
                                        if((count($FSExperts) > 0) && ($FSTagNo>=10))
                                        {
                                            $FSTagNomax = 9;
                                        }
                                        else
                                        {
                                            $FSTagNomax = $FSTagNo;
                                        }
                                        $FSExpertNo = 10-$FSTagNomax;
                                        for ($i = 0; $i < $FSTagNomax ; $i++)
                                        {
                                            if (empty($getFSTagListNew[$i]))
                                            {
                                                $field .= "<div style='height:19px;'>&nbsp;</div>";
                                            } else
                                            {
                                                if($getFSTagListNew[$i]['Name']=="BlackBoxTelecom")
                                                {
                                                    if(!empty($BlackBoxTelecomStatus))
                                                    {
                                                        $BlackBoxTelecomdate = $BlackBoxTelecomStatus['Date'];
                                                        if (!empty($BlackBoxTelecomdate))
                                                        {
                                                            $strBlackBoxTelecomDate = date_format(new DateTime($BlackBoxdate), "m/d/Y");
                                                        } else
                                                        {
                                                            $strBlackBoxTelecomDate = "";
                                                        }

                                                        if ($BlackBoxTelecomStatus['CertID'] == "49")
                                                        {
                                                            $BlackBoxTelecomimg = "BTT1.png";
                                                            $BlackBoxTelecomStatus = "Black Box Telecom - " . $BlackBoxTelecomStatus['LevelText'] . ": " . $strBlackBoxDate;
                                                        } else if ($BlackBoxTelecomStatus['CertID'] == "50")
                                                        {
                                                            $BlackBoxTelecomimg = "BTT2.png";
                                                            $BlackBoxTelecomStatus = "Black Box Telecom - " . $BlackBoxTelecomStatus['LevelText'] . ": " . $strBlackBoxDate;
                                                        }
                                                        $field .= "<span style='padding: 2px 2px;'>";
                                                        $color = "<img id='BlackBoxTelecomimgtag' style='" . ($BlackBoxTelecomStatus['Status'] != 'Pending' ? "display:'';" : "display:none;") . "' height='27px' bt-xtitle='" . $BlackBoxTelecomStatus . "' class='hoverText' src='/widgets/images/" . $BlackBoxTelecomimg . "' title=''/>";
                                                        $field .= $color;
                                                        $field .= "</span>";
                                                    }
                                                }
                                                else if($getFSTagListNew[$i]['Name']=="BlackBoxCabling")
                                                {
                                                    if(!empty($BlackBoxCablingStatus))
                                                    {
                                                        $BlackBoxdate = $BlackBoxCablingStatus['Date'];
                                                        if (!empty($BlackBoxdate))
                                                        {
                                                            $strBlackBoxDate = date_format(new DateTime($BlackBoxdate), "m/d/Y");
                                                        } else
                                                        {
                                                            $strBlackBoxDate = "";
                                                        }
                                                        if ($BlackBoxCablingStatus['CertID'] == "46")
                                                        {
                                                            $BlackBoximg = "BTC1.png";
                                                            $BlackBoxStatus = "Black Box Cabling - " . $BlackBoxCablingStatus['LevelText'] . ": " . $strBlackBoxDate;
                                                        } else if ($BlackBoxCablingStatus['CertID'] == "47")
                                                        {
                                                            $BlackBoximg = "BTC2.png";
                                                            $BlackBoxStatus = "Black Box Cabling - " . $BlackBoxCablingStatus['LevelText'] . ": " . $strBlackBoxDate;
                                                        } else if ($BlackBoxCablingStatus['CertID'] == "48")
                                                        {
                                                            $BlackBoximg = "BTC3.png";
                                                            $BlackBoxStatus = "Black Box Cabling - " . $BlackBoxCablingStatus['LevelText'] . ": " . $strBlackBoxDate;
                                                        } 
                                                        $field .= "<span style='padding: 2px 2px;'>";
                                                        $color = "<img id='BlackBoxCablingimgtag' style='" . ($BlackBoxCablingStatus['Status'] != 'Pending' ? "display:'';" : "display:none;") . "' height='27px' bt-xtitle='" . $BlackBoxStatus . "' class='hoverText' src='/widgets/images/" . $BlackBoximg . "' title=''/>";
                                                        $field .= $color;
                                                        $field .= "</span>";
                                                    }
                                                }
                                                else
                                                {
                                                    if (!empty($getFSTagListNew[$i]['Title']) )
                                                    {
                                                        if($getFSTagListNew[$i]['FSTagId'] == 25 && $PurpleTechnicianCertified==1)
                                                        {}
                                                        else
                                                        {
                                                            if (empty($getFSTagListNew[$i]['Date']))
                                                            {
                                                                $date = "";
                                                            } else
                                                            {
                                                                //850
                                                                if($getFSTagListNew[$i]['Title']!="I Have FS-Mobile")
                                                                {
                                                                    $date = new Zend_Date($getFSTagListNew[$i]['Date'], 'YYYY-mm-dd');
                                                                }
                                                                else
                                                                {
                                                                    $date = $getFSTagListNew[$i]['Date'];
                                                                }
                                                            }
                                                            //end 850
                                                            $ExistingImagePath = "";
                                                            if ($getFSTagListNew[$i]['IsExistingTag'] == 1)
                                                            {
                                                                if (!empty($getFSTagListNew[$i]['ExistingImagePath']))
                                                                {
                                                                    $ExistingImagePath = $getFSTagListNew[$i]['ExistingImagePath'];
                                                                }
                                                            } else
                                                            {
                                                                if (!empty($getFSTagListNew[$i]['TagArt']))
                                                                {
                                                                    $ExistingImagePath = "https://s3.amazonaws.com/wos-docs/tags/" . str_replace ("|", "", $getFSTagListNew[$i]['TagArt']);
                                                                }
                                                            }
                                                            if (empty($ExistingImagePath))
                                                            {
                                                                $ExistingImagePath = "/widgets/images/check_green.gif";
                                                            }
                                                            //931
                                                            $HasMultiTagLevels="";
                                                            if($getFSTagListNew[$i]['HasMultiTagLevels']==1 && !empty($getFSTagListNew[$i]['TagLevelsData']))
                                                            {      
                                                                $HasMultiTagLevels = ": L".$getFSTagListNew[$i]['LevelOrder']."&nbsp;-&nbsp;".$getFSTagListNew[$i]['LevelTitle'];
                                                            }
                                                            $Title = $getFSTagListNew[$i]['Title'];
                                                            if (!empty($date))
                                                            {
                                                                if($getFSTagListNew[$i]['Title']!="I Have FS-Mobile")
                                                                {
                                                                    $date = ": ".$date->toString('mm/dd/YYYY');
                                                                }
                                                            }
                                                            else
                                                            {
                                                                $date = "";
                                                            }
                                                            $field .= "<span style='padding: 2px 2px;'>";
                                                            $color = "<img src='$ExistingImagePath' class='hoverText' bt-xtitle='$Title$HasMultiTagLevels$date $untagstr' height='27px'/>";
                                                            $field .= $color;
                                                            $field .= "</span>";
                                                            //end 931
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        //735
                                        
                                        $i=0;
                                        if (sizeof($FSExperts) > 0)
                                        {
                                            foreach($FSExperts as $expert)
                                            {
                                                $i++;
                                                $k= $expert["FSExpertId"];
                                                $title = "";
                                                if (!empty($expert["Title"]))
                                                {
                                                    $title = "FS-Expert: <b>".$expert["Title"]."</b><div style=\"padding-left:15px;padding-top:10px;\"><ul><li style=\"list-style:disc outside none;\">".$expert["Content"]."</li></ul></div>";
                                                    $field .="<span style='padding: 2px 2px; width:70px;'><img src='/widgets/images/expert/".$FSExperts[$k]["Abbreviation"].".png' height='27px' k='$k' class='hoverTextnew'  bt-xtitle='$title' /></span>";
                                                }
                                                if($i==$FSExpertNo)
                                                {
                                                    break;
                                                }
                                            }
                                            
                                        }
                                        //end 735
                                        if($FSTagNo>$FSTagNomax)
                                        {
                                                $field .= "<span style='padding: 2px 2px; width:70px;white-space:nowrap;'><a href='javascript:;' onclick=\"javascript:changeCenterContent('fsTrustedTech');$('#clientCredentials').click();\" style='text-decoration:underline;' class='cmdOpenTechProfileShow'>See More...</a></span>";
                                        }
                                        echo($field);
                                        ?>
                                        <!--end 624-->
                                        <input type="hidden" value="<?= $ClientisNCRorGPM; ?>" name="ClientisNCRorGPM" id="ClientisNCRorGPM"/>
                                    </td>
			  	</tr>
			  	
			  	<tr>
                                    <td colspan="2" style="padding-right:10px;">
                                        <hr />
                                    </td>
			  	</tr>
                                <!--end 700-->
			  	
			  	<tr>
			  		<td style="padding-bottom: 5px">
			  			<input type="checkbox" name="isPreferred" id="isPreferred" style="background-color:#ffffff; border: 0px;"/>
			  			<span id="preferredLabel">Client Preferred</span>
			  		</td>
			  		<td style="padding-bottom: 5px">
			  			<div style="float:right; margin-right: 10px;"> 
			  		 		<input type="checkbox" name="isDenied" id="isDenied"  style="background-color:#ffffff; border: 0px;"/>
			  				<span id="deniedLabel">Client Denied</span>
			  			</div>
			  			</td>
			  	</tr>
<!--700-->
			  	<tr>
			  		<td colspan="2" style="padding-right:10px;">
                                        <hr />
                                    </td>
                                <!--end735-->
			  	</tr>
                                <!--end 700-->
			  	
			  	<tr>
			  		<td colspan="2"><h2 style=" text-align: center;">Tests</h2></td>
			  	</tr>
			  	<tr>
			  		<td colspan="2">
			  			<table style="line-height: 19px;">
			  				<tr>
			  					<td>Basic Background Check</td><td id="Bg_Test_Pass_Lite"></td><td id="Bg_Test_ResultsDate_Lite"></td>
			  				</tr>
			  				<tr>
			  					<td>Comp. Background Check</td><td id="Bg_Test_Pass_Full"></td><td id="Bg_Test_ResultsDate_Full"></td>
			  				</tr>
  <!--700-->
                                                        <tr>
                                                            <td>Gold Drug Test (9 Panel)</td>
                                                            <td id="DrugPassed"></td>
                                                            <td id="DrugPassedDate"></td>
			  				</tr>
                                                        <tr>
                                                            <td>Silver Drug Test (5 Panel)</td>
                                                            <td id="SilverDrugPassed"></td>
                                                            <td id="DatePassedSilverDrug"></td>
			  				</tr>
                                                        <!--end 700-->
			  			</table>
			  		</td>
			  	</tr>
			 
			  	<tr class="isoRow">
			  		<td colspan="2"><hr /></td>
			  	</tr>
			  	<tr class="isoRow">
			  		<td>ISO:</td><td id="isoName"></td>
			  	</tr>
			  	<tr class="isoRow">
			  		<td>Dispatcher Name:</td><td id="isoDispatcher"></td>
			  	</tr>
			  	<tr class="isoRow">
			  		<td>Phone#:</td><td id="isoPhoneNum"></td>
			  	</tr>
			
			  </table>
		  </div>

	<!-- END TOP ROW -->
    <hr />
    
      <div class="span-5">
       
			<ul style="line-height: 30px; font-size: 17px;" id="leftNav">
				<li><a class="currentLink" onClick="javascript:changeCenterContent('expertise');">Expertise</a></li>
				<li><a onClick="javascript:changeCenterContent('fslogbook', '12 Month');">FS-LogBook&trade;</a></li>
				<li><a onClick="javascript:changeCenterContent('toolsResources');">Equipment</a></li>
				<li><a onClick="javascript:changeCenterContent('certifications');">Certifications</a></li>
				<li><a id="lnkfsTrustedTech" onClick="javascript:changeCenterContent('fsTrustedTech');">FS-TrustedTech&trade;</a></li>
                                <!--910-->
				<li><a id="lnkfscomments" onClick="javascript:changeCenterContent('comments');">Comments</a></li>
                                <!--end 910-->
				<li><a onClick="javascript:changeCenterContent('techSchedule');">Tech Schedule</a></li>
				<li><a onClick="javascript:changeCenterContent('contactInfo');">Contact Information</a></li>
			</ul>
      </div>
      
	      <div class="span-16 borderLeft" id="centerContent" style="border: 0;">
	      </div>
	      
	      <!-- Remove Maps Widget for initial release -->
	      <div class="span-4 last">
	       <!--  img src="http://maps.googleapis.com/maps/api/staticmap?center=-15.800513,-47.91378&zoom=11&size=190x225&sensor=false" width="190" height ="225">
	        <div style="background-color: #F0F8FF; color: #000; padding-bottom: 15px;">
	        	<div style="width: 65%; margin: 0 auto 0 auto; padding-top: 10px; text-align:center;">My Unassigned Work Orders</div>
	        	<div class="myUnassignedWO" style="background-color: #FFF; margin: 20px 15px 20px 15px; font-size: 11px;">
	        		<div class="wo">
	        			<span class="underline">POS Scanner Swap</span><span class="woAssign">ASSIGN!</span>
	        			<span>WIN 123456</span> - <span>WO 54673</span>
	        		</div>
	        		<div class="wo">
	        			<span class="underline">Digital Menu Install</span><span class="woAssign">ASSIGN!</span>
	        			<span>WIN 123456</span> - <span>WO 54673</span>
	        		</div>
	        		<div class="wo">
	        			<span class="underline">POS Scanner Swap</span><span class="woAssign">ASSIGN!</span>
	        			<span>WIN 123456</span> - <span>WO 54673</span>
	        		</div>
	        		<div class="wo">
	        			<span class="underline">Digital Menu Install</span><span class="woAssign">ASSIGN!</span>
	        			<span>WIN 123456</span> - <span>WO 54673</span>
	        		</div>
	        		<span style="margin-left: 110px;">(view all)</span>
	        		
	        	</div>
	        </div -->
	      </div>
    </div>

 	 <div id="fslogbook" style="display:none;">
      	
		    <div style="position:relative; left: 1px; width: 628px; height:38px;">
	            <ul  class="tabs" id="logbookTabs" style="border: 0px;">
	      			<li class="currenttab" style="width:120px;"><a id="fsLogbook12Month" class="current" onClick="techProfile.loadTechLogbookStats('12 Month');" style="width: 99px; font-size: 12px;">12 Months</a></li>
					<li style="width:120px;"><a onClick="techProfile.loadTechLogbookStats('90 Day');" style="width: 99px; font-size: 12px;">90 Days</a></li>
					<li style="width:120px;"><a onClick="techProfile.loadTechLogbookStats();" style="width: 99px; font-size: 12px;">Lifetime</a></li>
					<li style="width:120px;"><a onClick="techProfile.loadTechLogbookWoInfo();" style="width: 99px; font-size: 12px;">FS-Logbook</a></li>
					<li style="width:120px;"><a onClick="techProfile.loadTechLogbookWoInfo('<?=$_GET['v'] ?>');" style="width: 99px; font-size: 12px;"><?=$_GET['v'] ?>-Logbook</a></li>
	            </ul>
	            <div class="tabsbottom" style="border: 0px;"></div>
		    </div>
      		<!--ul class="tabs" id="logbookTabs">
      			<li><a id="fsLogbook12Month" onclick="techProfile.loadTechLogbookStats('12 Month');">12 Months</a></li>
				<li><a onclick="techProfile.loadTechLogbookStats('90 Day');">90 Days</a></li>
				<li><a onclick="techProfile.loadTechLogbookStats();">Lifetime</a></li>
				<li><a onclick="techProfile.loadTechLogbookWoInfo();">FS-Logbook</a></li>
				<li><a onclick="techProfile.loadTechLogbookWoInfo('<?=$_GET['v'] ?>');"><?=$_GET['v'] ?>-Logbook</a></li>
      		</ul-->
      		
      		<div class="fsLogbookPanes" style="clear: left; position: relative; top: 4px;">
      		
      			<div>
			        <table class="jobHistory" id="fsLogbookTable" style="display:none;">
			        	<thead>
			        		<tr id="fsLogbookColumns" class="tableHeader">
			        			
			        		</tr>
			        	</thead>
			        	<tbody>
			        	
			        	</tbody>
			        </table>

			        <table class="jobHistory" id="fsLogbookTable2" style="display:none;">
			        	<thead>
			        		<tr id="fsLogbookColumns2" class="tableHeader">
			        			
			        		</tr>
			        	</thead>
			        	<tbody>
			        	
			        	</tbody>
			        </table>
		        </div>
	       	</div>
	      </div> <!-- end fslogbook -->
	      
	      <div id="toolsResources" style="display:none;">
	      
		    <div style="position:relative; left: 1px; width: 628px; height:38px;">
	            <ul class="tabs" style="border: 0px;" id="toolsTabs">
	                <li class="currenttab" style="width:150px;"><a class="current" href="javascript:void(0)" style="width: 129px; font-size: 12px;">Basic Equipment</a></li>
                        <li style="width:210px;"><a href="javascript:void(0)" style="width: 210px; font-size: 12px;">Cabling & Telephony Equipment</a></li>
	            </ul>
	            <div class="tabsbottom" style="border: 0px;"></div>
		    </div>
	      	<!--<ul class="tabs" id="toolsTabs">
	      		<li><a href="#">Basic Equipment</a></li>
	      		<li><a href="#">Telephony Equipment</a></li>
	      	</ul>-->
	      	
	      	<div class="toolsPane" style="clear: left; position: relative; top: 4px;">
	      	
				<div class="toolsSection">
					<table class="jobHistory" id="leftAlignTable">
						<tr>
							<td style="width: 60%;">
								<table>
									<tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="2" id="CellPhone" /> Cellphone-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="CellPhone" /> Cellphone
										</td>
									</tr>
									<tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="1" id="Laptop" /> Laptop (Microsoft XP or newer)-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="Laptop" /> Laptop (Microsoft XP or newer)
										</td>
									</tr>
									<tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="25" id="DigitalCam" /> Digital Camera-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="DigitalCam" /> Digital Camera
										</td>
									</tr>
									<tr>
										<td id="vehicleRow">
											<!--<input type="checkbox" name="cablingEquipment[]" value="27" id="Vehicle" /> Vehicle -->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="Vehicle" /> Vehicle
										</td>
									</tr>
									<tr>
										<td colspan=2 id="vehicleMisc" style=" padding-left: 25px;">
											<div style="padding-left: 2px;">
												<!--<input type="checkbox" name="cablingEquipment[]" value="30" id="PanelTruck" /> Panel Truck-->
												<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="PanelTruck" /> Panel Truck
											</div>
											
											<div style="padding-left: 2px;">
												<!--<input type="checkbox" name="cablingEquipment[]" value="28" id="TruckCarryLaddersCable" /> Vehicle can transport ladders &amp; supplies-->
												<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="TruckCarryLaddersCable" /> Vehicle can transport ladders &amp; supplies
											</div>
											<div>
												<span>Vehicle Image</span><span style="margin-left: 33px;"></span>
											</div>
											<div>
												<img id="vehicleImage" width="200" />
											</div>
											<div>
												<span>Vehicle Description: </span><span id="vehicleDesc"></span>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<!--<input type="checkbox" name="basicEquipment[]" value="Ladder" id="Ladder" /> Ladder-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="Ladder" /> Ladder
										</td>
									</tr>
									<tr  colspan=2 id="ladderMisc" style="display:none; padding-left: 25px;">
										<td>
											<table>
												<tr>
													<td>
														<!--<input type="checkbox" name="cablingEquipment[]" value="5" id="Ladder_6" /> 6' Ladder-->
														<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="Ladder_6" /> 6' Ladder
													</td>
												</tr>
												<tr>
													<td>
														<!--<input type="checkbox" name="cablingEquipment[]" value="6" id="Ladder_12" /> 12' Ladder-->
														<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="Ladder_12" /> 12' Ladder
													</td>
												</tr>
												<tr>
													<td>
														<!--<input type="checkbox" name="cablingEquipment[]" value="7" id="Ladder_20Plus" /> 20' Ladder-->
														<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="Ladder_20Plus" /> 20' Ladder
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="23" id="Screwdrivers" /> Set of Screwdrivers-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="Screwdrivers" /> Set of Screwdrivers
										</td>
									</tr>
									<tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="23" id="Screwdrivers" /> Set of Screwdrivers-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="HandTools" /> Hand Tools
										</td>
									</tr>
									<tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="23" id="Screwdrivers" /> Set of Screwdrivers-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="CordlessDrill" /> Cordless Drill
										</td>
									</tr>
									<tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="23" id="Screwdrivers" /> Set of Screwdrivers-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="ProtectionEquipment" /> Personal Protection Equipment (Gloves, Hard Hat, Boots)
										</td>
									</tr>
									<tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="23" id="Screwdrivers" /> Set of Screwdrivers-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="MachineLabelMaker" /> Machine Label Maker (P-Touch, Bradley, etc.)
										</td>
									</tr>
								</table>
							</td>
							<td style="width: 50%; padding-left: 10px; vertical-align: top;">
								<table>
									<tr>
										<td>
									 		<div style="vertical-align:top;">
											<div id="otherToolsLabel" style="display: none; padding-bottom: 4px;">Other Tools</div>
											<!--div><textarea cols=20 rows=5 name="Tools" id="Tools"></textarea></div-->
											<div id="Tools"></div>
	                                        <div style="height: 1.3em">&nbsp;</div>
	                                        <div>
	                                        	<span id="vehicleImageLabel"></span>
	                                        	<span id="yesVehicleImage" style="display:none;">
	                                        		<input type="checkbox" name="deleteVehicleImage" id="deleteVehicleImage" />Remove
	                                        	</span>
	                                        </div>
											</div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				
				<div class="toolsSection">
					<table class="jobHistory" id="leftAlignTable">
						<tr>
							<td style="width: 50%;">
								<table id="leftAlignTable">
									<tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="20" id="CordMasonryDrillHammer" /> Corded masonry drill hammer and bits up to 1" diameter-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="CordMasonryDrillHammer" /> Corded masonry drill hammer and bits up to 1" diameter
										</td>
									</tr>
									<tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="19" id="RotaryCoaxCable" /> Rotary coax cable stripper for RG-59 and RG-6-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="RotaryCoaxCable" /> Rotary coax cable stripper for RG-59 and RG-6
										</td>
									</tr>
									<tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="21" id="RechargeCCTV" /> Rechargeable CCTV installer's LCD 4-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="RechargeCCTV" /> Rechargeable CCTV installer's LCD 4
										</td>
									</tr>
									<tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="22" id="VoltOhmMeter" /> Volt/Ohm Meter-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="VoltOhmMeter" /> Volt/Ohm Meter
										</td>
									</tr>
									<tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="18" id="FishTape50" /> 50'+ Fish Tape-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="FishTape50" /> 50'+ Fish Tape
										</td>
									</tr>
									<tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="10" id="DigitalVOMMeter" /> Digital VOM Meter-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="DigitalVOMMeter" /> Digital VOM Meter
										</td>
									</tr>
									<tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="11" id="ContinuityTester" /> Continuity Tester-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="ContinuityTester" /> Continuity Tester
										</td>
									</tr>
									<tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="9" id="ButtSet" /> Butt Set-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="ButtSet" /> Butt Set
										</td>
									</tr>
									<tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="8" id="ToneGeneratorAndWand" /> Tone Generator and Wand-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="ToneGeneratorAndWand" /> Tone Generator and Wand
										</td>
									</tr>	
									<tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="24" id="PunchTool" /> Punch Tool-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="PunchTool" /> Punch Tool
										</td>
									</tr>
									<tr colspan=2 id="punchToolMisc" style="display:none; padding-left: 25px;">
										<td >
											<table>
												<tr>
													<td>
														<!--<input type="checkbox" name="cablingEquipment[]" value="12" id="PunchTool66" /> Punch Tool with 66 Blade-->
														<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="PunchTool66" /> Punch Tool with 66 Blade
													</td>
												</tr>
												<tr>
													<td>
														<!--<input type="checkbox" name="cablingEquipment[]" value="13" id="PunchTool110" /> Punch Tool with 110 Blade-->
														<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="PunchTool110" /> Punch Tool with 110 Blade
													</td>
												</tr>
												<tr>
													<td>
														<!--<input type="checkbox" name="cablingEquipment[]" value="14" id="PunchToolBix" /> Punch Tool with Bix Blade-->
														<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="PunchToolBix" /> Punch Tool with Bix Blade
													</td>
												</tr>
												<tr>
													<td>
														<!--<input type="checkbox" name="cablingEquipment[]" value="15" id="PunchToolKrone" /> Punch Tool With Krone Blade-->
														<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="PunchToolKrone" /> Punch Tool With Krone Blade
													</td>
												</tr>
											</table>
										</td>
									</tr>
									<tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="16" id="CrimpToolRJ11" /> Crimp Tool with RJ11-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="CrimpToolRJ11" /> Crimp Tool with RJ11
										</td>
									</tr>
									<tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="17" id="CrimpToolRJ45" /> Crimp Tool with RJ45-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="CrimpToolRJ45" /> Crimp Tool with RJ45
										</td>
									</tr>
									<tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="31" id="Cable" /> Cable-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="Cable" /> Cable
										</td>
									</tr>
									<tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="32" id="CableTesters" /> Cable Testers-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="CableTesters" /> Cable Testers
										</td>
									</tr>
                                                                        <tr>
										<td>
											<!--<input type="checkbox" name="cablingEquipment[]" value="33" id="CableTesters" /> Cable Certifier-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="CableCertifier" /> Cable Certifier (able to download/print results)
										</td>
									</tr>
                                                                        
                                                                        <tr>
										<td>
											
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="LacingNeedle" /> Lacing Needle
										</td>
									</tr>
                                                                        <tr>
										<td>
											
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="WireWrapGun" /> Wire Wrap Gun
										</td>
									</tr>
									<tr>
										<td>
										<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="GoferPoles" /> Gofer Poles (or Glow Rods)
										</td>
									</tr>
									<tr>
										<td>
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="FiberOpticdBLossMeter" /> Fiber Optic dB Loss Meter &amp; Light Source
										</td>
									</tr>
									<tr>
										<td>
										<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="FiberOpticTerminationKit" /> Fiber Optic Termination Kit
										</td>
									</tr>
								
								</table>
							</td>					
						</tr>
					</table>
				</div>
			</div>
	      
	      <div id="comments" style="display:none;">
		    <div style="position: relative; left: 1px; width: 628px; height:38px;">
	            <ul class="tabs" style="border: 0px;" id="commentsTabs">
	                <li class="currenttab" style="width:170px;"><a class="current" href="javascript:void(0)" style="width: 149px; font-size: 12px;" onClick="$('#addCommentBtn').show()">Tech Profile Comments</a></li>
	                <li style="width:170px;"><a href="javascript:void(0)" style="width: 149px; font-size: 12px;" onClick="$('#addCommentBtn').hide()">Work Order Comments</a></li>
                    <li><input id="addCommentBtn" type="button" class="link_button" value="Add Comment" onClick="techProfile.addComments()" style="position: relative; left: 14.5em; top: 0.5em"/></li>
	            </ul>
	            <div class="tabsbottom" style="border: 0px;"></div>
		    </div>
	      	<!--<div>
	      		<ul class="tabs" id="commentsTabs">
	      			<li><a href="#" onclick="$('#addCommentBtn').show()">Tech Profile Comments</a></li>
	      			<li><a href="#" onclick="$('#addCommentBtn').hide()">Work Order Comments</a></li>
                    <li><input id="addCommentBtn" type="button" class="link_button" value="Add Comment" onClick="techProfile.addComments()" style="position: relative; left: 14.5em; top: 0.5em"/></li>
	      		</ul>
	      	</div>-->            
	      	<div class="commentsPane" style="clear: left; position: relative; top: 4px;">
	      		<div id="techComments">
			      	<table id='techCommentsTable' class='techProfileCenterContent'>
					</table>
				</div>
	      		<div id="workOrderComments">
			      	<table id='workOrderCommentsTable' class='techProfileCenterContent'>
						<thead>
							<tr>
								<th align="center">Comments</th>
								<th align="center">Skill Category</th>
								<th align="center">Headline</th>
								<th align="center">Job Rating</th>
								<th align="center">Type</th>
							</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>				
			</div>
			
	      	</div> <!-- end comments -->
	      
	      <div id="expertise" style="display:none;">

		    <div style="position: relative; left: 1px; width: 628px; height:38px;" id="expTabs">
	            <ul  class="tabs" style="border: 0px;">
	                <li class="currenttab" style="width:150px;"><a class="current" href="javascript:void(0)" style="width: 129px; font-size: 12px;">Technician Skills</a></li>
	                <li style="width:150px;"><a href="javascript:void(0)" style="width: 129px; font-size: 12px;">Cabling Experience</a></li>
	                <li style="width:150px;"><a href="javascript:void(0)" style="width: 129px; font-size: 12px;">Telephony Experience</a></li>
                        <li style="width:150px;"><a href="javascript:void(0)" style="width: 129px; font-size: 12px;">Language Skills</a></li>
	            </ul>
	            <div class="tabsbottom" style="border: 0px;"></div>
		    </div>
	      	<!--div>
	      		<ul class="tabs" id="expTabs">
	      			<li><a href="#">Technician Skills</a></li>
	      			<li><a href="#">Cabling Experience</a></li>
	      			<li><a href="#">Telephony Experience</a></li>
	      		</ul>
	      	</div-->
	      	
	      	<div class="expPane" style="clear: left; position: relative; top: 4px;">
	      		<div>
			      	<table id='techExpertiseTable' class='jobHistory'  >
                                    <col width="150px"  />
                                    <col width="80px"  />
                                    <col width="110px" />
                                    <col width="150px"  />
						<thead>
							<tr class="tableHeader">
								<th align="center">Skill Category</th>
								<th align="center">Work Orders</th>
                                                                <!--13735-->
								<th align="center">FS-Expert&trade;<a id="expertinfocontrols" href="javascript:void(0)"><img style="vertical-align: middle;height:20px;" src="/widgets/images/get_info.png"></a></th>
								<th align="center">Self-Rating (5=Expert)</th>
							</tr>
						</thead>
					
						<tbody>
							
						</tbody>
					</table>
				</div>
				
				<div id="cablingExp" style="text-align: left;">
					<table id="leftAlignTable">
						<tr>
							<td>
								<table class="cablingTable jobHistory">
									<tr>
										<td colspan=2 style="vertical-align: top">
											<!--<input type="checkbox" name="telephonySkills[]" value="2" id="ReadWiringDiagrams" /> Can read wiring diagrams-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="ReadWiringDiagrams" /></td> <td style="vertical-align: top"> Can read wiring diagrams
										</td>
									</tr>
									<tr>
										<td colspan=2 style="vertical-align: top">
											<!--<input type="checkbox" name="telephonySkills[]" value="3" id="ExpInstallSurv" /> Has experience installing surveillance DVR/Cam systems-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="ExpInstallSurv" /> </td> <td style="vertical-align: top"> Has experience installing surveillance DVR/Cam systems
										</td>
									</tr>
									<tr>
										<td colspan=2 style="vertical-align: top">
											<!--<input type="checkbox" name="telephonySkills[]" value="41" id="SkilledCat5Fiber" /> Is skilled with Cat 5 and/or fiber cabling-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="SkilledCat5Fiber" /> </td> <td style="vertical-align: top"> Is skilled with Cat 5 and/or fiber cabling
										</td>
									</tr>
									<tr>
										<td colspan=2 style="vertical-align: top">
											<!--<input type="checkbox" name="telephonySkills[]" value="42" id="40HrsLast6Months" /> Has worked at least 40 hours of Cat 5 cabling contracts in the past 6 months-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="40HrsLast6Months" /></td> <td style="vertical-align: top"> Has worked at least 40 hours of Cat 5 cabling contracts in the past 6 months
										</td>
									</tr>
									<tr>
										<td colspan=2 style="vertical-align: top">
											<!--<input type="checkbox" name="cablingEquipment[]" value="29" id="PossessAllLicenses" /> Possesses all licenses required in city/state to perform low voltage work-->
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="PossessAllLicenses" /> </td> <td style="vertical-align: top"> Possesses all licenses required in city/state to perform low voltage work
										</td>
									</tr>
	
                                                                        
                                                                        
                                                                        <tr>
										<td colspan=2 style="vertical-align: top">
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="CableTVTelcoCLECExperience" /></td> <td style="vertical-align: top"> Has Cable TV Co., Telco, and/or CLEC work experience 
										</td>
									</tr>
                                                                        <tr>
										<td colspan=2 style="vertical-align: top">
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="AdvCableLacStitchSkills" /> </td> <td style="vertical-align: top"> Has advanced cable skills (including lacing and stitching) 
										</td>
									</tr>
                                                                        <tr>
										<td colspan=2 style="vertical-align: top">
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="BatteryDistributionFuseBay" /> </td> <td style="vertical-align: top"> Has worked in a Battery Distribution Fuse Bay 
										</td>
									</tr>
                                                                        <tr>
										<td colspan=2 style="vertical-align: top">
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="ReadingMOPsExperience" /></td> <td style="vertical-align: top"> Has experience reading MOPs (Methods of Procedures)
										</td>
									</tr>
	
									
									 <tr>
										<td colspan=2 style="vertical-align: top">
											<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="OperationScissorLiftExperience" /></td> <td style="vertical-align: top"> Knows how to operate a scissor lift and has extensive experience using a ladder
										</td>
									</tr>
									 <tr>
										<td colspan=2 style="vertical-align: top">
										<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="InstallationMultipleCableExperience" /></td> <td style="vertical-align: top"> Has experience with multiple cable installation methods (installing in conduit using fish tape, pull string, or shop vac)
										</td>
									</tr>                            
									<tr>
										<td colspan=2 style="vertical-align: top">
										<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="InstallingSleevesRacewaysExperience" /></td> <td style="vertical-align: top"> Has experience installing sleeves firestops, J-Hooks, Panduit, and Wire Mold raceways
										</td>
									</tr>
									 <tr>
										<td colspan=2 style="vertical-align: top">
										<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="InstallationVoiceDataEquipmentExperience" /></td> <td style="vertical-align: top"> Has experience with voice and data equipment room installation and build out (ex: data cabinets and racks, plywood, 66/110 blocks, patch panels, etc)
										</td>
									</tr>
									 <tr>
										<td colspan=2 style="vertical-align: top">
										  <img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="SingleAndMultiModeTerminationTestingExp" /></td> <td style="vertical-align: top"> Has experience in single-mode and multi-mode termination and testing
										
										</td>
									</tr>
									 <tr>
										<td colspan=2 style="vertical-align: top">
										  <img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="UsingLaptopForSwitchConfigurationExp" /> </td> <td style="vertical-align: top"> Has experience using a laptop for basic switch configuration (HyperTerminal, Terra Term, etc.)
										</td>
									</tr>
									
									
								</table>
							</td>
						</tr>
				   	</table>
			      </div>
			       
			       <div id="telephonyExp" style="text-align: left;">
					<table>
					<tr>
						<td>
							<table class="telephonyTable jobHistory">
								<tr>
									<td style="vertical-align: top">
									<!--<input type="checkbox" name="telephonySkills[]" value="4" id="InstallingTelephony" /> Has experience installing OR maintaining (break-fix) telephony systems-->
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="InstallingTelephony" />  </td> <td style="vertical-align: top"> Has experience installing OR maintaining (break-fix) telephony systems
									</td>
								</tr>
								<tr>
									<td style="vertical-align: top">
									<!--<input type="checkbox" name="telephonySkills[]" value="6" id="ServerTelephony" /> Has installed OR performed "break-fix" activity on a Server OR VOIP based telephony system-->
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="ServerTelephony" />  </td> <td style="vertical-align: top"> Has installed OR performed "break-fix" activity on a Server OR VOIP based telephony system
									</td>
								</tr>
								
								<tr>
									<td style="vertical-align: top">
									<!--<input type="checkbox" name="telephonySkills[]" value="4" id="InstallingTelephony" /> Has experience installing OR maintaining (break-fix) telephony systems-->
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="UnderstandTelecomColorCodeExperience" />  </td> <td style="vertical-align: top"> Understands the telecom color code and has experience with termination jacks and patch panels
									</td>
								</tr>
								<tr>
									<td style="vertical-align: top">
									<!--<input type="checkbox" name="telephonySkills[]" value="4" id="InstallingTelephony" /> Has experience installing OR maintaining (break-fix) telephony systems-->
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="DMARCRoomsEquipmentExperience" />  </td> <td style="vertical-align: top"> Has experience with DMARC rooms and equipment such as Rj-21X, Smart jacks, lightning protectors, and cross connect fields
									</td>
								</tr>
								<tr>
									<td style="vertical-align: top">
									<!--<input type="checkbox" name="telephonySkills[]" value="4" id="InstallingTelephony" /> Has experience installing OR maintaining (break-fix) telephony systems-->
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="OperationPBXExperience" />  </td> <td style="vertical-align: top"> Has experience with basic PBX / Key system operation (ex: general programming, station card and CO line card replacement, station / extension assignment)
									</td>
								</tr>
								
							</table>
							<table class="telephony3Column jobHistory" style="text-align: left;">
								<tr>
									<td colspan=3><hr /></td>
								</tr>
								
								<tr>
									<td></td><td>Experience or Training</td>
								</tr>
								<tr>
									<td colspan=3 style="font-weight:bold;">Avaya Small Systems</td>
								</tr>
								<tr>
									<td class="indent">Partner or Merlin</td>
									<td class="telephonyIndent">
									<!--<input type="checkbox" name="telephonySkills[]" value="8" id="AvayaPartnerMerlin" />-->
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="AvayaPartnerMerlin" /> 
									</td>
								</tr>
								<tr>
									<td class="indent">IP Office</td>
									<td class="telephonyIndent">
									<!--<input type="checkbox" name="telephonySkills[]" value="10" id="AvayaIPOffice" />-->
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="AvayaIPOffice" /> 
									</td>
								</tr>
								<tr>
									<td colspan=3 style="font-weight:bold;"> PBX</td>
								</tr>
								<tr>
									<td class="indent">Definity</td>
									<td class="telephonyIndent">
									<!--<input type="checkbox" name="telephonySkills[]" value="12" id="AvayaDefinity" />-->
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="AvayaDefinity" /> 
									</td>
								</tr>
								<tr>
									<td class="indent">8300/8500/8700</td>
									<td class="telephonyIndent">
									<!--<input type="checkbox" name="telephonySkills[]" value="14" id="AvayaPBX8300etc" />-->
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="AvayaPBX8300etc" /> 
									</td>
								</tr>
								<tr>
									<td class="indent">Gateway 250/350</td>
									<td class="telephonyIndent">
									<!--<input type="checkbox" name="telephonySkills[]" value="16" id="AyayaGateway" />-->
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="AyayaGateway" /> 
									</td>
								</tr>
								
								<tr>
									<td colspan=3 style="font-weight:bold;">Nortel Small Systems</td>
								</tr>
								<tr>
									<td class="indent">Norstar</td>
									<td class="telephonyIndent">
									<!--<input type="checkbox" name="telephonySkills[]" value="18" id="NortelNorstar" />-->
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="NortelNorstar" /> 
									</td>
								</tr>
								<tr>
									<td class="indent">BMS</td>
									<td class="telephonyIndent">
									<!--<input type="checkbox" name="telephonySkills[]" value="20" id="NortelBMS" />-->
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="NortelBMS" /> 
									</td>
								</tr>
								<tr>
									<td colspan=3 style="font-weight:bold;"> PBX</td>
								</tr>
								<tr>
									<td class="indent">Meridian</td>
									<td class="telephonyIndent">
									<!--<input type="checkbox" name="telephonySkills[]" value="22" id="NortelMeridian" />-->
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="NortelMeridian" /> 
									</td>
								</tr>
								<tr>
									<td class="indent">CS 1000/1500</td>
									<td class="telephonyIndent">
									<!--<input type="checkbox" name="telephonySkills[]" value="24" id="NortelCS" />-->
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="NortelCS" /> 
									</td>
								</tr>
								<tr>
									<td colspan=3 style="font-weight:bold;">Cisco</td>
								</tr>
								<tr>
									<td class="indent">Call Manager</td>
									<td class="telephonyIndent">
									<!--<input type="checkbox" name="telephonySkills[]" value="26" id="CiscoCM" />-->
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="CiscoCM" /> 
									</td>
								</tr>
								<tr>
									<td colspan=3 style="font-weight:bold;">Siemens (ROLM)</td>
								</tr>
								<tr>
									<td class="indent">9751</td>
									<td class="telephonyIndent">
									<!--<input type="checkbox" name="telephonySkills[]" value="28" id="Siemens9751" />-->
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="Siemens9751" /> 
									</td>
								</tr>
								<tr>
									<td class="indent">Hicom</td>
									<td class="telephonyIndent">
									<!--<input type="checkbox" name="telephonySkills[]" value="30" id="SiemensHicom" />-->
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="SiemensHicom" /> 
									</td>
								</tr>
								<tr>
									<td class="indent">Hipath</td>
									<td class="telephonyIndent">
									<!--<input type="checkbox" name="telephonySkills[]" value="32" id="SiemensHipath" />-->
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="SiemensHipath" /> 
									</td>
								</tr>
								
								<tr>
									<td style="font-weight:bold;">NEC</td>
									<td class="telephonyIndent">
									<!--<input type="checkbox" name="telephonySkills[]" value="33" id="NEC" />-->
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="NEC" /> 
									</td>
								</tr>
								<tr>
									<td style="font-weight:bold;">InterTel</td>
									<td class="telephonyIndent">
									<!--<input type="checkbox" name="telephonySkills[]" value="35" id="InterTel" />-->
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="InterTel" /> 
									</td>
								</tr>
								<tr>
									<td style="font-weight:bold;">Mitel</td>
									<td class="telephonyIndent">
									<!--<input type="checkbox" name="telephonySkills[]" value="37" id="Mitel" />-->
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="Mitel" /> 
									</td>
								</tr>
								<tr>
									<td style="font-weight:bold;">Toshiba</td>
									<td class="telephonyIndent">
									<!--<input type="checkbox" name="telephonySkills[]" value="39" id="Toshiba" />-->
									<img src="/widgets/images/checklist_flase.jpg" width="13" height="13" id="Toshiba" /> 
									</td>
								</tr>								
							</table>
						</td>
					</tr>
				</table>
				</div>
                    
                    <div>
                        <table id='LanguageTable' class='jobHistory'>
                            <?
                            $TechLanguages = new Core_TechLanguages();
                            $TechLanguagesList = $TechLanguages->getTechLanguages($techID);
                            if (!empty($TechLanguagesList))
                            {
                                foreach($TechLanguagesList as $Language)
                                {
                                ?>
                                <tr>
                                    <td width="17px">
                                        <?= ($Language["TechID"] != "" || !empty($Language["TechID"]))?"<img src=\"/widgets/images/checklist_true.jpg\" width=\"13\" height=\"13\">": "<img src=\"/widgets/images/checklist_flase.jpg\" width=\"13\" height=\"13\">"?>
                                    </td>
                                    <td style="text-align:left;">
                                        <?=$Language["name"].($Language["SpeakerLevel"]==1?" - Competent":($Language["SpeakerLevel"]==2?" - Fluent":""))?>
                                    </td>
                                </tr>
                                <?
                                }
                            }
                            ?>
                                        
                        </table>
			</div>

			</div>
			
	      	</div> <!-- end expertise -->
	      	
	      	<div id="certifications" style="display:none;">
			    <div style="position:relative; left: 1px; width: 628px; height:38px;">
		            <ul  class="tabs" style="border: 0px;" id="certTabs">
                    <li class="currenttab" style="width:250px;">
                        <a class="current" href="javascript:void(0)" style="width: 229px; font-size: 12px;">Industry & Product Certifications</a>
                    </li>
                    <li style="width:270px;">
                        <a id="programCertifications" href="javascript:void(0)" style="width: 249px; font-size: 12px;">Product & Program E-Learning</a>
                    </li>
		            </ul>
		            <div class="tabsbottom" style="border: 0px;"></div>
			    </div>
		      	<!--<div>
		      		<ul class="tabs" id="certTabs">
		      			<li><a href="#">Certifications</a></li>
		      		</ul>
		      	</div>-->
            <div class="certificationsPane" style="clear: left; position: relative; top: 4px;">
                <div class="certificationsSection">
		      		<table id='myCertifications' class='jobHistory'>
						<thead>
							<tr class="tableHeader">
								<th width="40%" >Industry Certifications</th>
								<th width="40%">Certification #</th>
								<th width="20%">Date Completed</th>
							</tr>
						</thead>
						<tbody>
						<tr id="DeVryCert">
								<td style="text-align:left;">DeVry Intern</td>
								<td id="DeVryCertStatus">
									<span id="DeVryCertNumDisplay"></span>
									</td>
									<td>
										<span id="DeVryCertDateDisplay"></span>
									</td>
							</tr>
							<tr id="mcseCert">
								<td style="text-align:left;">Microsoft Certified Systems Engineer (MCSE)</td>
								<td id="mcseCertStatus">
									<span id="mcseCertNumDisplay"></span>
									</td>
									<td>
										<span id="mcseCertDateDisplay"></span>
									</td>
							</tr>
                                                        <tr id="MCSACert">
                                                            <td style="text-align:left;">Microsoft Certified Solutions Associate (MCSA)</td>
                                                            <td id="MCSACertStatus">
                                                                <span id="MCSACertNumDisplay"></span>
                                                            </td>
                                                            <td>
                                                                    <span id="MCSACertDateDisplay"></span>
                                                            </td>
							</tr>
                                                         <tr id="MCPCert">
                                                            <td style="text-align:left;">Microsoft Certified Professional (MCP)</td>
                                                            <td id="MCPCertStatus">
                                                                <span id="MCPCertNumDisplay"></span>
                                                            </td>
                                                            <td>
                                                                    <span id="MCPCertDateDisplay"></span>
                                                            </td>
							</tr>
							<tr id="ccnaCert">
								<td style="text-align:left;">Cisco Certified Network Associate (CCNA)</td>
								<td id="ccnaCertStatus">
									<span id="ccnaCertNumDisplay"></span>
									</td>
									<td>
										<span id="ccnaCertDateDisplay"></span>
									</td>
							</tr>
							<tr id="compTiaCert">
								<td style="text-align:left;">CompTIA Registration #</td>
								<td id="compTiaCertStatus">
									<span id="compTiaCertNumDisplay"></span>
									</td>
									<td>
										<span id="compTiaCertDateDisplay"></span>
									</td>
							</tr>
							<tr id="CompTIA_A_PLUSCert">
								<td style="text-align:left;">CompTIA A+</td>
								<td id="CompTIA_A_PLUSCertStatus" align="center">
									<span id="aPlusCertNumDisplay"></span>
									</td>
									<td>
									</td>
							</tr>
                                                        <tr id="CompTIA_PDI_PLUSCert">
                                                            <td>CompTIA PDI+</td>
                                                            <td id="CompTIA_PDI_PLUSCertStatus" align="left">
                                                                <span id="CompTIA_PDI_PLUSCertNumDisplay"  class="CertNumDisplayNew"></span>
                                                                
                                                            </td>
                                                            <td>
                                                                <span id="CompTIA_PDI_PLUSCertDateDisplay"></span>
                                                                
                                                            </td>
                                                        </tr>
                                                        <tr id="CompTIA_Server_PLUSCert">
                                                            <td>CompTIA Server+</td>
                                                            <td id="CompTIA_Server_PLUSCertStatus" align="left">
                                                                <span id="CompTIA_Server_PLUSCertNumDisplay"  class="CertNumDisplayNew"></span>
                                                                
                                                            </td>
                                                            <td>
                                                                <span id="CompTIA_Server_PLUSCertDateDisplay"></span>
                                                               
                                                            </td>
                                                        </tr>
                                                        <tr id="CompTIA_Network_PLUSCert">
                                                            <td>CompTIA Network+</td>
                                                            <td id="CompTIA_Network_PLUSCertStatus" align="left">
                                                                <span id="CompTIA_Network_PLUSCertNumDisplay"  class="CertNumDisplayNew"></span>
                                                                
                                                            </td>
                                                            <td>
                                                                <span id="CompTIA_Network_PLUSCertDateDisplay"></span>
                                                                
                                                            </td>
                                                        </tr>
							<tr id="dellDcseCert">
								<td style="text-align:left;">Dell DCSE</td>
								<td id="dellDcseCertStatus">
									<span id="dellDcseCertNumDisplay"></span>
									</td>
									<td>
										<span id="dellDcseCertDateDisplay"></span>
									</td>
							</tr>
							<tr id="hpCert">
                                <td style="text-align:left;">HP Certification</td>
                                <td id="hpCertStatus">
                                    <span id="hpCertNumDisplay"></span>
                                    <input style="display:none;" id="hpCertNumInput" type="text" size=10 name="HP_CertNum" />
                                </td>
                                <td>
                                    <span id="hpCertDateDisplay"></span>
                                    <input id="hpCertDateInput" style="display:none;" type="text" size=15 name="Hallmark_Cert_Date" />
                                </td>
                            </tr>
							<tr id="bicsiCert">
								<td style="text-align:left;">BICSI Certification</td>
								<td id="bicsiCertStatus">
									<span id="bicsiCertNumDisplay"></span>
									</td>
									<td>
										<span id="bicsiCertDateDisplay"></span>
									</td>
							</tr>
                            <tr id="EMCCert">
                                <td style="vertical-align: top;" >EMC Certification</td>
                                <td id="EMCCertStatus" align="left" style="padding-left:3px;">
                                    <span id="EMCCertDisplay"></span>
                                </td>
                                <td style="padding-left:3px;">
                                    <span id="EMCCertDateDisplay"></span>
                                </td>
                            </tr>
					</tbody>
					</table>
				</div>



                <div class="certificationsSection">
                    <table id='programCertifications' class='jobHistory'>
                        <thead>
                            <tr class="tableHeader">
                                <th>Program Certifications</th>
                                <th>Status / Certification#</th>
                                <th>Date Completed</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr id="hallmarkCert">
                                <td style="text-align:left;">Hallmark POS Certification</td>
                                <td id="hallmarkCertStatus">
                                    <span id="hallmarkCertNumDisplay"></span>
                                    <input style="display:none;" id="hallmarkCertNumInput" type="text" size=10 name="Hallmark_Cert_Num" />
                                </td>
                                <td>
                                    <span id="hallmarkCertDateDisplay"></span>
                                    <input id="hallmarkCertDateInput" style="display:none;" type="text" size=15 name="Hallmark_Cert_Date" />
                                </td>
                            </tr>
                            <tr id="starbucksCert">
                                <td style="text-align:left;">Starbucks Certification</td>
                                <td id="starbucksCertStatus">
                                    <span id="starbucksCertNumDisplay"></span>
                                    <input style="display:none;" id="starbucksCertNumInput" type="text" size=10 name="Starbucks_Cert_Num" />
                                </td>
                                <td>
                                    <span id="starbucksCertDateDisplay"></span>
                                    <input id="starbucksCertDateInput" style="display:none;" type="text" size=15 name="Hallmark_Cert_Date" />
                                </td>
                            </tr>
                            <tr id="mauricesCert">
                                <td style="text-align:left;">Maurices Certification</td>
                                <td id="mauricesCertStatus">
                                    <span id="mauricesCertNumDisplay"></span>
                                    <input style="display:none;" id="mauricesCertNumInput" type="text" size=10 name="Maurices_Cert_Num" />
                                </td>
                                <td>
                                    <span id="mauricesCertDateDisplay"></span>
                                    <input id="mauricesCertDateInput" style="display:none;" type="text" size=15 name="Hallmark_Cert_Date" />
                                </td>
                            </tr>





















































                                                
                            <tr id="electroMechCert">
                                <td style="text-align:left;">Electro-Mechanical Certification</td>
                                <td id="electroMechCertStatus">
                                    <span id="electroMechCertNumDisplay"></span>
                                    <input style="display:none;" id="electroMechCertNumInput" type="text" size=10 name="Electro_Mechanical_Cert_Num" />
                                </td>
                                <td>
                                    <span id="electroMechCertDateDisplay"></span>
                                    <input id="electroMechCertDateInput" style="display:none;" type="text" size=15 name="Hallmark_Cert_Date" />
                                </td>
                            </tr>
                            <tr id="CopierSkillsAssessment">
                                <td style="text-align:left;"><a href="javascript:;" techid="" class="cmdCopiesSkillView">Copier Skills Assessment</a></td>
                                <td id="CopierSkillsAssessmentStatus">
                                    <span id="CopierSkillsAssessmentDisplay"></span>
                                </td>
                                <td>
                                    <span id="CopierSkillsAssessmentDateDisplay"></span>
                                </td>
                            </tr>
                            <script>
                                 var _copier_skills_assessment = copier_skills_assessment.CreateObject({
                                    id:'_copier_skills_assessment' 
                                 });
                            </script>
                        </tbody>
                    </table>
                </div>






















            </div>
	      	</div> <!-- End Certifications -->
	      	
	      	
	      	<div id="fsTrustedTech" style="display:none;">
			    <div style="position:relative; left: 1px; width: 628px; height:38px;">
		            <ul class="tabs" style="border: 0px;" id="fsTrustedTechTabs">
                                <li class="fsTrustedTechTab" style="width:170px;">
                                    <a id="InsurancesCredentials" href="javascript:void(0)" style="width: 149px; font-size: 12px;">Insurances</a>
                    </li>
                                <li class="fsTrustedTechTab" style="width:170px;">
                        <a id="publicCredentials" href="javascript:void(0)" style="width: 149px; font-size: 12px;">Public Credentials</a>
                    </li>
                                <li style="width:170px;" class="currenttab fsTrustedTechTab">
                                    <a id="clientCredentials" class="current"  href="javascript:void(0)" style="width: 149px; font-size: 12px;">Client Credentials</a>
                    </li>
		                <li id="editSaveBtns" style="position: relative; top: 3px; display: none;width:115px">
		               		
							<div class="cancelSaveSection" style="float:right;">
								<div class="editSection" style="float:right; margin-left: 40px; margin-top: 10px;"> 
									<span class="updateButton" id="clientCredentialsEditLink" onClick="techProfile.editSection('clientCredentials');" title="Edit">Edit</span>
								</div>
								<div id="cancelSaveBtns" style="display:none; margin-left: 20px;">
									<img class="cancelUpdateButton" onClick="techProfile.cancelSection('clientCredentials');" title="Cancel" src="/widgets/images/button_x.png">
									<img class="updateButton" onClick="techProfile.saveTechInfo('fsTrustedTechForm');" title="Save" src="/widgets/images/button_save.png">
								</div>
							</div>
		                </li>
		            </ul>
		            <div class="tabsbottom" style="border: 0px;"></div>
			    </div>
		      	<!--<div>
		      		<ul class="tabs">
			   			<li><a href="#">Public Credentials</a></li>
			   			<li><a href="#">Client Credentials</a></li>
			   			<li><a href="#" style="width: 150px;">Product/Program Training</a></li>
			      	</ul>
		      	</div>-->
		      	


		      	<div class="fsTrustedTechPane" style="clear: left; position: relative; top: 4px;">
                <div class="trustedTechSection">
                    <div class='divInsurances'>
                        <span>Never wonder if your technicians are current with their insurance payments. FieldSolutions provides all our clients coverage for technicians dispatched through our system for a specific work order.</span>
                        <br/>
                        <br/>
                        <span>FieldSolutions global coverages are:</span>
                        <br/>
                        <span>
                            <img src="/widgets/images/checklist_true.jpg" alt=""><span>&nbsp;&nbsp;$5,000,000 Liability Insurance</span>
                        </span>
                        <br/>
                        <span>
                            <img src="/widgets/images/checklist_true.jpg" alt=""><span>&nbsp;&nbsp;$2,000,000 Crime Liability</span>
                        </span>
                        <br/>
                        <span>
                            <img src="/widgets/images/checklist_true.jpg" alt=""><span>&nbsp;&nbsp;$2,000,000 Professional Liability</span>
                        </span>
                        <br/>
                        <span>
                            <img src="/widgets/images/checklist_true.jpg" alt=""><span>&nbsp;&nbsp;$1,000,000 Automobile Liability</span>
                        </span>
                        <br/>
                        <span>
                            <img src="/widgets/images/checklist_true.jpg" alt=""><span>&nbsp;&nbsp;$1,000,000 Workers' Compensation</span>
                        </span>
                        <br/>
                        <br/>
                        <span><a  id="publicEmailtoFS" href="javascript:void(0)" >Click here</a> to request a copy of FieldSolutions' Certificate of Insurance.</span>
                    </div>
                </div>
		      	
		      	<div class="trustedTechSection">
			      	<table>
						<tr>
							<td style="width: 45%;">
								<table id='publicCredentials' class='jobHistory'>
									<thead>
										<tr class="tableHeader">
											<th>Public Credentials</th>
											<th>Status</th>
											<th>Date Completed</th>
										</tr>
									</thead>
									<tbody>
										<tr id="bgCheck">
												<td style="text-align:left;">Background Check</td>
												<td id="credStatus"></td>
												<td>
													<div id="bgCheckDate"></div>
													<input id="bgCheckInput" style="display:none;" type="text" size="15" name="Bg_Test_ResultsDate_Lite" /> 
												</td>
										</tr>
										<tr id="bgCheckFull">
												<td style="text-align:left;">Comprehensive Background Check</td>
												<td id="credStatus"></td>
												<td>
													<div id="bgCheckFullDate"></div>
												</td>
										</tr>
 <!--700-->
										<tr id="drugTest">
												<td style="text-align:left;">Gold Drug Test (9 Panel)</td>
												<td id="DrugPassedpanel"></td>
												<td>
													<div id="drugTestDatepanel"></div>
												</td>
										</tr>
                                                                                <tr id=" SilverDrug">
												<td style="text-align:left;">Silver Drug Test (5 Panel)</td>
												<td id="SilverDrugPassedpanel"></td>
												<td>
													<div id="DatePassedSilverDrugpanel"></div>
												</td>
										</tr>
                                                                                <!--700-->
                                                                                <tr id="BlueRibbonTechnician">
												<td style="text-align:left;">Blue Ribbon Technician</td>
												<td id="BlueRibbonTechnicianCertStatus"></td>
												<td>
													<div id="BlueRibbonTechnicianDate"></div>
												</td>
										</tr>
                                                                                <tr id="BootCampCertified">
												<td style="text-align:left;">Boot Camp Certified</td>
												<td id="TBCCertStatus"></td>
												<td>
													<div id="TBCDate"></div>
												</td>
										</tr>
										<tr id="fsMobile">
												<td style="text-align:left;">FS-Mobile</td>
												<td id="fsMobileStatus"></td>
												<td>
													<div id="fsMobileDate"></div>
												</td>
										</tr>
										
										<tr id="w9">
												<td style="text-align:left;">W-9</td>
												<td id="credStatus"></td>
												<td>
													<div id="w9Date"></div>
													<input id="w9Input" style="display:none;" type="text" size="15" name="" /> 
												</td>
										</tr>
										<tr id="WorkAuthorization">
                                            <td id="USAuthorized" style="text-align:left;">US Verified</td>
                                            <td id="WorkAuthorizationStatus"></td>
                                            <td>
                                                <div id="WorkAuthorizationValue"></div>
                                               
                                            </td>
                                        </tr>
                                        <tr id="VerifiedSignLanguageCompetency">
                                            <td id="VerifiedSignLanguageCompetency" style="text-align:left;">Verified Sign Language Competency</td>
                                            <td id="VerifiedSignLanguageCompetencyStatus"></td>
                                            <td>
                                                <div id="VerifiedSignLanguageCompetencyValue"></div>
                                               
                                            </td>
                                        </tr>
                                                   <tr id="NACI">
                                                    <td>NACI Government Security Clearance</td>
                                                    <td>
                                                        <div id="NACIStatusLabel"></div>
        <!--                                                <input id="NACICertStatus" type="checkbox" name="NACIStatusChk" style="display:none;" />-->
                                                    </td>
                                                    <td>
                                                        <span id="NACIDate"></span>
        <!--                                                <input id="NACIInput" style="display:none;" type="text" size="15" name="NACIDate"  /> -->
                                                    </td>
                                                </tr>
										<tr id="interimGovt">
												<td style="text-align:left;">Interim Government Security Clearance</td>
												<td id="credStatus"></td>
												<td>
													<div id="interimGovtDate"></div>
													<input id="interimGovtInput" style="display:none;" type="text" size="15" name="InterimSecClearDate"  /> 
												</td>
										</tr>
                                                                                <!--14056-->	
										<tr id="fullGovt">
												<td style="text-align:left;">Secret Government Security Clearance</td>
												<td id="credStatus" ></td>
												<td>
													<div id="fullGovtDate"></div>
													<input id="fullGovtInput" style="display:none;" type="text" size="15" name="FullSecClearDate"  /> 
												</td>
										</tr>
										<tr id="topGovt">
												<td style="text-align:left;">Top Secret Government Security Clearance</td>
												<td id="credStatus"></td>
												<td>
													<div id="topGovtDate"></div>
													<input id="topGovtInput" style="display:none;" type="text" size="15" name="TopSecClearDate" /> 
												</td>
										</tr>
                                                                             <tr id="topGovtSCI">
                                                                                <td>Top Secret SCI Government Security Clearance</td>
                                                                                <td align="center" >
                                                                                    <div id="topSCIGovtStatusLabel"></div>                                                                                  
                                                                                </td>
                                                                                <td>
                                                                                    <div id="topSCIGovtDate"></div>
                                                                                    <input id="topSCIGovtInput" style="display:none;" type="text" size="15" name="TopSecSCIClearDate" /> 
                                                                                </td>
                                                                            </tr>
                                                                                <!--14056-->	
									</tbody>
								</table>
							</td>
						</tr>
					</table>
				</div>
				
				<div class="trustedTechSection">
					<form name="fsTrustedTechForm" id="fsTrustedTechForm" method="post">
					<table id='clientCredentials' class='jobHistory'>
					
						<thead>
							<tr class="tableHeader">
								<th>Client Credentials</th>
								<th>Status / Certification#</th>
								<th>Date Completed</th>
							</tr>
						</thead>
                                                <!--624-->
						<tbody>
                                <?
                                                //839
                                                $apiTechClass = new Core_Api_TechClass();
                                                //end 839
                                                    //624
                                                if (!empty($FSTagList))
                                                {
                                                    $DidPurpleTechnicianDisplay = false;
                                                    $DidAdjustableBedDisplay = false;
                                                    $DidCompuComDisplay = false;
                                                    foreach ($FSTagList as $FSTag)
                                                    {
                                                        //--- global
                                                        $isGlobalTag = $FSTag['VisibilityId']==2?true:false;
                                                        $belongWithCompany = $FSTag['BelongCompany'];
                                                        //--- $isEditCert
                                                        $isEditCert = false;
                                                        if($isPM || (!$isGlobalTag && $belongWithCompany))
                                                        {
                                                            $isEditCert = true;
                                                        } 
                                                        
														if ($isPM || $isGlobalTag || $belongWithCompany)
															$isShown = true;
														else
															$isShown = false;

                                                        //special case
                                                        //
                                                        //VonagePlus
                                                        if($FSTag['Name']=='VonagePlus' && $isShown)
                                                        {
                                                            if($Core_Api_User->getCompanyId() == 'MDS' || $Core_Api_User->isPM())
                                                            {
                                                                $isEditCert = true;
                                                            }    
                                                        } 
                                                        //--- date
                                                        if (!empty($FSTag['Date']) && !empty($FSTag['TechId']))
                                                        {
                                                            $date = date("m/d/Y",strtotime($FSTag['Date']));
                                                        }
                                                        else
                                                        {
                                                            $date = "";
                                                        }
                                                        //13931                                    //--- image 
                                                        if (empty($FSTag['ExistingImagePath']))
                                                        {
                                                            if(empty($FSTag['TagArt'])){
                                                                $ExistingImagePath = "/widgets/images/check_green.gif";
                                                            } else {
                                                                $ExistingImagePath = "https://s3.amazonaws.com/wos-docs/tags/" . str_replace ("|", "", $FSTag['TagArt']);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            $ExistingImagePath = $FSTag['ExistingImagePath'];
                                                        }

                                                        //912
                                                        $isAllowEditCert = 0;
                                                        if($isPM && ($FSTag["CanGPMEdit"]==1))
                                                        {
                                                            $isAllowEditCert = 1;
                                                        }
                                                        else if(!$isPM && ($FSTag["CanNonGPMEdit"]==1))
                                                        {
                                                            $isAllowEditCert = 1;
                                                        }
                                                        ?>
                                                        <?php
                                                        //839
                                                        if($FSTag['Name'] == "BlackBoxCabling" && $isShown)
                                                        {
                                                            $BlackBoxCablingStatus = $apiTechClass->GetBlackBoxCablingStatus($techID);
                                                            if(!empty($BlackBoxCablingStatus))
                                                            {   
                                                                $dateBTCS = $BlackBoxCablingStatus['Date'];
                                                                if(!empty($dateBTCS))
                                                                {
                                                                    $strDateBTCS =  date_format(new DateTime($dateBTCS), "m/d/Y");
                                                                }
                                                                else
                                                                {
                                                                    $strDateBTCS = "";
                                                                }

                                                                if($BlackBoxCablingStatus['CertID']=="46")
                                                                {
                                                                    $BlackBoxCablingimg = "BTC1.png";
                                                                }
                                                                else if($BlackBoxCablingStatus['CertID']=="47")
                                                                {
                                                                    $BlackBoxCablingimg = "BTC2.png";
                                                                }
                                                                else if($BlackBoxCablingStatus['CertID']=="48")
                                                                {
                                                                    $BlackBoxCablingimg = "BTC3.png";
                                                                }
                                                                if($BlackBoxCablingStatus['Status']=="Pending")
                                                                {
                                                                    $islinkcomfirmBCCS = "<a class='BlackBoxConfirm' href='javascript:;' CertID='".$BlackBoxCablingStatus['CertID']."' techid='".$techID."'>confirm</a> / <a class='BlackBoxDeny' href='javascript:;' CertID='".$BlackBoxCablingStatus['CertID']."' techid='".$techID."'>deny</a>";
                                                                    $strDateBTCS="";
                                                                    $isShowBCCSImg = "none";
                                                                    $BTCSStatus  = $BlackBoxCablingStatus['LevelText'] . " - ".$BlackBoxCablingStatus['Status'];
                                                                }
                                                                else
                                                                {
                                                                    $islinkcomfirmBCCS="";
                                                                    $isShowBCCSImg = "";
                                                                    $BTCSStatus  = $BlackBoxCablingStatus['LevelText'];
                                                                }
                                                                $BTCSID = $BlackBoxCablingStatus['CertID'];
                                                            }
                                                            ?>
                                                            <tr id="BlackBoxCabling">
                                                                <td>
                                                                    <span id="BlackBoxCablingnamelabel">
                                                                        Black Box Cabling
                                                                    </span>
                                                                </td>
                                                                <td align="center" id="BlackBoxCablingStatuscol">
                                                                    <div id="BlackBoxCablingStatus">
                                                                        <?
                                                                        if($BlackBoxCablingimg!="")
                                                                        {
                                                                            ?>
                                                                            <img id="BlackBoxCablingimg" height="15px" src="/widgets/images/<?=$BlackBoxCablingimg?>" style="display:<?=$isShowBCCSImg?>;" title="">
                                                                            <?
                                                                        }
                                                                        ?>
                                                                        <?=$BTCSStatus." <span>".$islinkcomfirmBCCS."</span>";?>
                                                                    </div>
                                                                    <input id="BlackBoxCablingid" type="hidden" name="BlackBoxCablingid" value="<?=$BTCSID?>">
                                                                    <select name="BlackBoxCablingcertifications" style="display:none; width:120px;" id="BlackBoxCablingNumInput">
                                                                        <option value="">Select Status</option>
                                                                        <option value="46" <?=(!empty($BlackBoxCablingStatus) && $BlackBoxCablingStatus['Status']!='Pending' && $BTCSID=='46')?"selected":"";?>>Helper (C1)</option>
                                                                        <option value="47" <?=(!empty($BlackBoxCablingStatus) && $BlackBoxCablingStatus['Status']!='Pending' && $BTCSID=='47')?"selected":"";?>>Advanced (C2)</option>
                                                                        <option value="48" <?=(!empty($BlackBoxCablingStatus) && $BlackBoxCablingStatus['Status']!='Pending' && $BTCSID=='48')?"selected":"";?>>Lead (C3)</option>
                                                                        <option value="pending" <?=(!empty($BlackBoxCablingStatus) && $BlackBoxCablingStatus['Status']=='Pending')?"selected":"";?>>Pending</option>
                                                                    </select>
                                                                </td>
                                                                <td align="left">
                                                                    <div id="BlackBoxCablingDateShow"><?=$strDateBTCS?></div>
                                                                    <input type="text" name="BlackBoxCablingcertDate" size="15" style="display:none;" id="BlackBoxCablingInput" value="<?=$strDateBTCS;?>">
                                                                </td>
                                                            </tr>
                                                            <?
                                                        }
                                                        else if($FSTag['Name'] == "BlackBoxTelecom" && $isShown)
                                                        {
                                                            $BlackBoxTelecomStatus = $apiTechClass->getBlackBoxTelecomStatus($techID);
                                                            if(!empty($BlackBoxTelecomStatus))
                                                            {   
                                                                $dateBTTS = $BlackBoxTelecomStatus['Date'];
                                                                if(!empty($dateBTTS))
                                                                {
                                                                    $strDateBTTS =  date_format(new DateTime($dateBTTS), "m/d/Y");
                                                                }
                                                                else
                                                                {
                                                                    $strDateBTTS = "";
                                                                }

                                                                $BTTSStatus = "Black Box Telecom - ".$BlackBoxTelecomStatus['LevelText'] .": ".$strDateBTTS;
                                                                if($BlackBoxTelecomStatus['CertID']=="49")
                                                                {
                                                                    $BlackBoxTelecomimg = "BTT1.png";
                                                                }
                                                                else if($BlackBoxTelecomStatus['CertID']=="50")
                                                                {
                                                                    $BlackBoxTelecomimg = "BTT2.png";
                                                                }
                                                                if($BlackBoxTelecomStatus['Status']=="Pending")
                                                                {
                                                                    $islinkcomfirmBCTS = "<a class='BlackBoxConfirm' href='javascript:;' CertID='".$BlackBoxTelecomStatus['CertID']."' techid='".$techID."'>confirm</a> / <a class='BlackBoxDeny' href='javascript:;' CertID='".$BlackBoxTelecomStatus['CertID']."' techid='".$techID."'>deny</a>";
                                                                    $strDateBTTS="";
                                                                    $isShowBCCSImg = "none";
                                                                    $BTTSStatus  = $BlackBoxTelecomStatus['LevelText'] . " - ".$BlackBoxTelecomStatus['Status'];
                                                                }
                                                                else
                                                                {
                                                                    $islinkcomfirmBCTS="";
                                                                    $isShowBCCSImg = "";
                                                                    $BTTSStatus  = $BlackBoxTelecomStatus['LevelText'];
                                                                }
                                                                $BTTSID = $BlackBoxTelecomStatus['CertID'];
                                                            }
                                                            ?>
                                                            <tr id="BlackBoxTelecom">
                                                                <td>
                                                                    <span id="BlackBoxTelecomnamelabel">
                                                                        Black Box Telecom
                                                                    </span>
                                                                    </td>
                                                                <td align="center" id="BlackBoxTelecomStatuscol">
                                                                    <div id="BlackBoxTelecomStatus">
                                                                        <?
                                                                        if($BlackBoxTelecomimg!="")
                                                                        {
                                                                            ?>
                                                                            <img id="BlackBoxTelecomimg" height="15px" src="/widgets/images/<?=$BlackBoxTelecomimg?>" style="display:<?=$isShowBCCSImg?>" title=""/>
                                                                            <?
                                                                        }
                                                                        ?>
                                                                        <?=$BTTSStatus." <span>".$islinkcomfirmBCTS."</span>";?>
                                                                    </div>
                                                                    <input id="BlackBoxTelecomid" type="hidden" name="BlackBoxTelecomid" value="<?=$BTTSID?>">
                                                                    <select name="BlackBoxTelecomcertifications" style="display:none; width:120px;" id="BlackBoxTelecomNumInput">
                                                                        <option value="">Select Status</option>
                                                                        <option value="49" <?=(!empty($BlackBoxTelecomStatus) && $BlackBoxTelecomStatus['Status']!='Pending' && $BTTSID=='49')?"selected":"";?>>Helper (T1)</option>
                                                                        <option value="50" <?=(!empty($BlackBoxTelecomStatus) && $BlackBoxTelecomStatus['Status']!='Pending' && $BTTSID=='50')?"selected":"";?>>Advanced (T2)</option>
                                                                        <option value="pending" <?=(!empty($BlackBoxTelecomStatus) && $BlackBoxTelecomStatus['Status']=='Pending')?"selected":"";?>>Pending</option>
                                                                    </select>
                                                                </td>
                                                                <td align="left">
                                                                    <div id="BlackBoxTelecomDateShow"><?=$strDateBTTS?></div>
                                                                    <input type="text" name="BlackBoxTelecomcertDate" size="15" style="display:none;" id="BlackBoxTelecomInput" value="<?=$strDateBTCS;?>">
                                                                </td>
                                                            </tr>
                                                            <?
                                                        }
                                                        else if ($FSTag['Name'] == "CSCDesksideSupportTech" && $isShown)
                                                        //end 839
                                                        {
                                                        ?>
                                                            <tr id="CSCDesksideSupportTech">
                                                                <td><?= $FSTag['Title'] ?></td>
                                                                <td id="CSCDesksideSupportTechStatus" align="center">
                                                                    <div id="CSCDesksideSupportTechStatusDisplay"></div>
                                                                    <?
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input id="CSCDesksideSupportTechStatusChk" type="checkbox" name="CSCDesksideSupportTechStatusChkInput" style="display:none;" />
                                                                            <input type="hidden" value="1" id="noCSCDesksideSupportTech">
                                                                        <?                    
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td align="left">
                                                                    <div id="CSCDesksideSupportTechDate"></div>
                                                                    <?
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input id="CSCDesksideSupportTechDatetxt" style="display:none;" type="text" size="15" name="CSCDesksideSupportTechDatetxt" /> 
                                                                        <?                    
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <script>
                                                                jQuery("#CSCDesksideSupportTechStatusChk").click(function() {
                                                                    if(jQuery("#CSCDesksideSupportTechDatetxt").val() == "" && jQuery(this).is(":checked"))
                                                                    {
                                                                        jQuery("#CSCDesksideSupportTechDatetxt").val('<?= date("m/d/Y") ?>');
                                                                    }    
                                                                });

                                                            </script>                                
                                                        <?php
                                                        }
                                                        else if ($FSTag['Name'] == "LMSPreferred" && $isShown)
                                                        {
                                                        ?>
                                                            <tr id="LMSPreferredCert">
                                                                <td><?= $FSTag['Title'] ?></td>
                                                                <td id="LMSPreferredCertStatus" align="center">
                                                                    <div id="LMSPreferredStatusLabel"></div>
                                                                    <?
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input id="LMSPreferredStatusInput" type="checkbox" name="LMSPreferredTechnicianStatusChkInput" style="display:none;" />
                                                                        <?                    
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td align="left">
                                                                    <span id="LMSPreferredDateDisplay"></span>
                                                                    <?
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input id="LMSPreferredDateInput" style="display:none;" type="text" size="15" name="LMSPreferredDateInput" /> 
                                                                        <?                    
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>           
                                                        <?php
                                                        }
                                                        else if ($FSTag['Name'] == "LMSPlusKey" && $isShown)
                                                        {
                                                        ?>
                                                            <tr id="LMSPlusKeyCert">
                                                                <td><?= $FSTag['Title'] ?></td>
                                                                <td id="LMSPlusKeyCertStatus" align="center">
                                                                    <div id="LMSPlusKeyStatusLabel"></div>
                                                                    <?
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input id="LMSPlusKeyStatusInput" type="checkbox" name="LMSPlusKeyTechnicianStatusChkInput" style="display:none;" />
                                                                        <?                    
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td align="left">
                                                                    <span id="LMSPlusKeyDateDisplay"></span>
                                                                    <?
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input id="LMSPlusKeyDateInput" style="display:none;" type="text" size="15" name="LMSPlusKeyDateInput" /> 
                                                                        <?                    
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                        <?php
                                                        }
                                                        else if ($FSTag['Name'] == "ncrBasic" && $isShown)
                                                        {
                                                        ?>
                                                            <tr id="ncrBasic">
                                                                <td style="text-align:left;"><?= $FSTag['Title'] ?></td>
                                                                <td id="ncrBasicStatus"></td>
                                                                <td>
                                                                    <div id="ncrBasicDate"></div>
                                                                    <?
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input id="ncrBasicInput" style="display:none;" type="text" size="15" name="NCR_Basic_Cert_Date"  /> 
                                                                        <?                    
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>                                
                                                        <?php
                                                        }
                                                        else if ($FSTag['Name'] == "FlextronicsRecruit" && $isShown)
                                                        {
                                                        ?>
                                                            <tr id="FlextronicsRecruit">
                                                                <td><?= $FSTag['Title'] ?></td>
                                                                <td  align="center">
                                                                    <div id="FlextronicsRecruitStatus"></div>
                                                                    <?
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input class="UpdateRequestReport" id="FlextronicsRecruitStatusInput" type="checkbox" name="FlextronicsRecruitStatusChkInput" style="display:none;" />
                                                                        <?                    
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td align="left">
                                                                    <div id="FlextronicsRecruitDateDisplay"></div>
                                                                    <?
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input id="FlextronicsRecruitDateInput" style="display:none;" type="text" size="15" name="FlextronicsRecruitDatetxt" /> 
                                                                        <?                    
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>                                              
                                                        <?php
                                                        }
                                                        else if ($FSTag['Name'] == "FlextronicsScreened" && $isShown)
                                                        {
                                                        ?>
                                                            <tr id="FlextronicsScreened">
                                                                <td><?= $FSTag['Title'] ?></td>
                                                                <td  align="center">
                                                                <div id="FlextronicsScreenedStatus"></div>
                                                                    <?
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input class="checkboxTodayStatusInput" id="FlextronicsScreenedStatusInput" type="checkbox" name="FlextronicsScreenedStatusChkInput" style="display:none;" />
                                                                        <?                    
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td align="left">
                                                                    <div id="FlextronicsScreenedDateDisplay"></div>
                                                                    <?
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input id="FlextronicsScreenedDateInput" style="display:none;" type="text" size="15" name="FlextronicsScreenedDatetxt" /> 
                                                                        <?                    
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>                                
                                                        <?php
                                                        }
                                                        else if ($FSTag['Name'] == "FlextronicsContractor" && $isShown)
                                                        {
                                                        ?>
                                                            <tr id="FlextronicsContractor">
                                                                <td><?= $FSTag['Title'] ?></td>
                                                                    <td  align="center" style="text-align:right;">
                                                                    <div id="FlextronicsContractorStatus" ></div>
                                                                    <?
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <div>
                                                                                <table border="0">
                                                                                    <tbody>
                                                                                        <tr>   
                                                                                            <td style="padding-left:0px;width:47%;">

                                                                                            </td>     
                                                                                            <td align="center" style="text-align:left;width:5%">    
                                                                                                <input class="checkboxTodayStatusInput" id="FlextronicsContractorStatusInput" type="checkbox" name="FlextronicsContractorStatusChkInput" style="display:none;" />
                                                                                            </td>     
                                                                                            <td align="left" style="text-align: left;">        
                                                                                                <input id="FlextronicsContractorNumStatusInput" size="9" stype="text" name="FlextronicsContractorNumStatusInput" style="display:none;padding-right:1px;" />
                                                                                            </td>
                                                                                            </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                            </div>
                                                                        <?                    
                                                                    }
                                                                    ?>

                                                                </td>
                                                                <td align="left">
                                                                    <div id="FlextronicsContractorDateDisplay"></div>
                                                                    <?
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input id="FlextronicsContractorDateInput" style="display:none;" type="text" size="15" name="FlextronicsContractorDatetxt" /> 
                                                                        <?                    
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <script>
                                                                jQuery(".checkboxTodayStatusInput").click(function() {
                                                                    var id = jQuery(this).attr("id");
                                                                    var key = id.replace("StatusInput","");
                                                                    if(jQuery("#"+key+"DateInput").val() == "" && jQuery(this).is(":checked"))
                                                                    {
                                                                        jQuery("#"+key+"DateInput").val('<?= date("m/d/Y") ?>');
                                                                    }    
                                                                });

                                                            </script> 
                                                        <?php
                                                        }
                                                        else if ($FSTag['Name'] == "NCRBT" && $isShown)
                                                        {
                                                        ?>
                                                            <tr id="NCRBadgedTechnician">
                                                                <td><?= $FSTag['Title'] ?></td>
                                                                <td id="NCRBadgedTechnicianStatus" align="center">
                                                                    <div id="NCRBadgedTechnicianStatusDisplay"></div>
                                                                    <?
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input id="NCRBadgedTechnicianStatusChk" type="checkbox" name="NCRBadgedTechnicianStatusChkInput" style="display:none;" />
                                                                        <?                    
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td align="left">
                                                                    <div id="NCRBadgedTechnicianDate"></div>
                                                                    <?
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input id="NCRBadgedTechnicianDatetxt" style="display:none;" type="text" size="15" name="NCRBadgedTechnicianDatetxt" /> 
                                                                        <?                    
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <script>
                                                                jQuery("#NCRBadgedTechnicianStatusChk").click(function() {
                                                                    if(jQuery("#NCRBadgedTechnicianDatetxt").val() == "" && jQuery(this).is(":checked"))
                                                                    {
                                                                        jQuery("#NCRBadgedTechnicianDatetxt").val('<?= date("m/d/Y") ?>');
                                                                    }    
                                                                });
                                                            </script>        
                                                        <?php
                                                        }
                                                        else if ($FSTag['Name'] == "FLSID" && $isShown)
                                                        {
                                                        ?>
                                                            <tr id="flsId">
                                                                <td style="text-align:left;"><?= $FSTag['Title'] ?></td>
                                                                <td id="flsIdStatus">
                                                                    <span id="flsIdStatusDisplay"></span>
                                                                    <?
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                        <input type="text" value="" name="FLSID" size="15" id="flsIdStatusInput" style="display:none;"> 
                                                                        <?
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <div id="flsIdDate"></div>
                                                                </td>
                                                            </tr>                                
                                                        <?php
                                                        }
                                                        else if ($FSTag['Name'] == "FLSWhse" && $isShown)
                                                        {
                                                        ?>
                                                            <tr id="flsWhse">
                                                                <td style="text-align:left;"><?= $FSTag['Title'] ?></td>
                                                                <td id="flsWhseStatus">
                                                                    <span id="flsWhseStatusDisplay"></span>
                                                                    <?
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                        <input type="text" value="" name="FLSWhse" size="15" id="flsWhseStatusInput" style="display:none;"></td>
                                                                        <?
                                                                    }
                                                                    ?>
                                                                <td>
                                                                    <div id="flsWhseDate">
                                                                        <span id="flsWhseDateDisplay"></span>
                                                                        <?
                                                                        if($isAllowEditCert==1)
                                                                        {
                                                                            ?>
                                                                            <input id="flsWhseDateInput" type="text" size="15" name="FLSWhseDate" style="display:none;" />
                                                                            <?
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        <?php
                                                        }
                                                        else if ($FSTag['Name'] == "flsCSP" && $isShown)
                                                        {
                                                        ?>
                                                            <tr id="flsCSP" >
                                                                <td style="text-align:left;"><?= $FSTag['Title'] ?></td>
                                                                <td id="flsCSPStatus">
                                                                    <span id="flsCSPStatusDisplay"></span>
                                                                    <?
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                        <input type="checkbox" name="FLSCSP_Rec" id="flsCSPStatusInput" style="display:none;">
                                                                        <?
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <div id="flsCSPDate">
                                                                        <span id="flsCSPDateDisplay"></span>
                                                                        <?
                                                                        if($isAllowEditCert==1)
                                                                        {
                                                                            ?>
                                                                                <input id="flsCSPDateInput" type="text" size="15" name="FLSCSP_RecDate" style="display:none;" />
                                                                            <?
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        <?php
                                                        }
                                                        else if ($FSTag['Name'] == "FLS_Photo_ID" && $isShown)
                                                        {
                                                        ?>
                                                            <tr id="FLS_Photo_ID">
                                                                <td style="text-align:left;"><?= $FSTag['Title'] ?></td>
                                                                <td id="FLS_Photo_IDStatus">
                                                                    <span id="FLS_Photo_IDStatusDisplay"></span>
                                                                    <?
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                        <input type="checkbox" name="FLS_Photo_ID" id="FLS_Photo_IDStatusInput" style="display:none;">
                                                                        <?
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td>
                                                                    <div id="flsPhotoDate">
                                                                        <span id="flsPhotoDateDisplay"></span>
                                                                    </div>
                                                                </td>
                                                            </tr>  
                                                        <?php
                                                        }
                                                        else if ($FSTag['Name'] == "ServRightBrother" && $isShown)
                                                        {
                                                        ?>
                                                            <tr id="ServRightBrother">
                                                                <td><?= $FSTag['Title'] ?></td>
                                                                <td  align="center">
                                                                    <div id="ServRightBrotherStatusDiv"></div>
                                                                    <?
                                                                        if($isAllowEditCert==1)
                                                                        {
                                                                            ?>
                                                                                <input type="hidden" id="ServRightBrotherHidden" />
                                                                                <input class="checkboxTodayStatusInput" id="ServRightBrotherStatusInput" type="checkbox" name="ServRightBrotherStatusChkInput" style="display:none;" />
                                                                            <?                    
                                                                        }
                                                                        ?>
                                                                    </td>
                                                                <td align="left">
                                                                    <div id="ServRightBrotherDateDisplay"></div>
                                                                    <?
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input id="ServRightBrotherDateInput" style="display:none;" type="text" size="15" name="ServRightBrotherDatetxt" /> 
                                                                        <?                    
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>    
                                                        <?php
                                                        }
                                                        else if ($FSTag['Name'] == "GGETechEvaluation" && $isShown)
                                                        {
                                                        ?>
                                                            <tr id="GGETechEvaluation">
                                                                <td>
                                                                    <span><?= $FSTag['Title'] ?></span>
                                                                    <span id="divGGETechEvaluationAllow" style="display: none;">
                                                                        <a href="javascript:;" id="cmdGGETechEvaluationAllow">(allow another attempt)</a>
                                                                    </span>
                                                                </td>
                                                                <td align="center">
                                                                    <div id="GGETechEvaluationStatusDisplay">
                                                                        1<sup>st</sup> Attempt - <span id="GGETechEvaluationFirst"></span>
                                                                        <br>
                                                                        <div id="divGGETechEvaluationSecond">2<sup>nd</sup> Attempt - <span id="GGETechEvaluationSecond"></span></div>
                                                                    </div>
                                                                    <div class="StatusLabel" id="GGETechEvaluationStatusLabel" style="display:none;" >
                                                                        <table>
                                                                            <tr>
                                                                                <td style="padding-right: 4px;text-align: right;" align="right">1<sup>st</sup> Attempt:</td>
                                                                                <td style="padding-right: 4px;text-align: left;">
                                                                                    <select style="width:75px" id="CbxGGETechEvaluationFirst" name="CbxGGETechEvaluationFirst">
                                                                                        <option value="">N/A</option>
                                                                                        <option value="Pending">Pending</option>
                                                                                        <?php
                                                                                        for($i = 1;$i<=20;$i++)
                                                                                        {
                                                                                            ?>
                                                                                            <option value="<?= $i ?>"><?= $i ?></option>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                    </select>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="padding-right: 4px;text-align: right;" align="right">2<sup>nd</sup> Attempt:</td>
                                                                                <td style="padding-right: 4px;text-align: left;">
                                                                                    <select style="width:75px"  id="CbxGGETechEvaluationSecond" name="CbxGGETechEvaluationSecond">
                                                                                        <option value="">N/A</option>
                                                                                        <option value="Pending">Pending</option>
                                                                                        <?php
                                                                                        for($i = 1;$i<=20;$i++)
                                                                                        {
                                                                                            ?>
                                                                                            <option value="<?= $i ?>"><?= $i ?></option>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                    </select>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>
                                                                </td>
                                                                <td align="left" valign="top" style="vertical-align: top;">
                                                                    <div id="GGETechEvaluationVals"></div>
                                                                    <div id="GGETechEvaluationInput" style="display:none;">
                                                                        <input class="calendarInput" id="GGETechEvaluationDateInputFirst"  type="text" size="15" name="GGETechEvaluationDateInputFirst" /> 
                                                                        <br>
                                                                        <input class="calendarInput" style="margin-top: 3px;" id="GGETechEvaluationDateInputSecond" type="text" size="15" name="GGETechEvaluationDateInputSecond" /> 
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        <?php
                                                        }
                                                        else if ($FSTag['Name'] == "Dell_MRA_Compliant" && $isShown)
                                                        {
                                                        ?>
                                                            <tr id="DellMRACompliant">
                                                                <td style="text-align:left;"><?= $FSTag['Title'] ?></td>
                                                                <td id="DellMRACompliantStatus"></td>
                                                                <td>
                                                                    <div id="DellMRACompliantDate"></div>
                                                                </td>
                                                            </tr>
                                                        <?php
                                                        }
                                                        else if ($FSTag['Name'] == "TrapolloMedicalCertified" && $isShown)
                                                        {
                                                        ?>
                                                            <tr id="TrapolloMedicalCertified">
                                                                <td><?= $FSTag['Title'] ?></td>
                                                                <td id="TrapolloMedicalCertifiedStatus" style"text-align:center">
                                                                    <span id="TMCStatusDisplay"></span>
                                                                    <?php if($isAllowEditCert) { ?>
                                                                    <input id="TMCStatusChk" type="checkbox" name="TMCStatusChk" style="display:none;" />
                                                                    <input id="TMCStatusHidden" type="hidden" name="TMCStatusHidden" value="" />
                                                                    <? } ?>
                                                                </td>
                                                                <td align="left">
                                                                    <span id="TMCDate"></span>
                                                                    <?php if($isAllowEditCert) { ?>
                                                                    <input id="TMCDateTxt" style="display:none;" type="text" size="15" name="TMCDateTxt" />
                                                                    <input id="TMCDateHidden" type="hidden" name="TMCDateHidden" value="" />
                                                                    <? } ?>

                                                                </td>
                                                            </tr>                      
                                                        <?php
                                                        }
                                                        else if ($FSTag['Name'] == "VonagePlus" && $isShown)
                                                        {
                                                        ?>
                                                            <tr id="VonagePlus">
                                                                <td><?= $FSTag['Title'] ?></td>
                                                                <td id="VonagePlusStatus" align="center">
                                                                    <div id="VonagePlusStatusDisplay"></div>
                                                                    <?php if($isAllowEditCert) { ?>
                                                                        <input id="VonagePlusStatusChk" type="checkbox" name="VonagePlusStatusChkInput" style="display:none;" />
                                                                    <?php } ?>
                                                                </td>
                                                                <td align="left">
                                                                    <div id="VonagePlusDate"></div>
                                                                    <?php if($isAllowEditCert) { ?>
                                                                        <input id="VOPDatetxt" style="display:none;" type="text" size="15" name="VOPDatetxt" /> 
                                                                    <? } ?>
                                                                </td>
                                                            </tr>
                                                            <script>
                                                                    jQuery("#VonagePlusStatusChk").click(function() {
                                                                    if(jQuery("#VOPDatetxt").val() == "" && jQuery(this).is(":checked"))
                                                                    {
                                                                        jQuery("#VOPDatetxt").val('<?= date("m/d/Y") ?>');
                                                                    }    
                                                                    });
                                                            </script>
                                                        <?php
                                                        }
                                                        else if ($FSTag['Name'] == "ComputerPlus" && $isShown)
                                                        {
                                                        ?>
                                                            <tr id="ComputerPlus">
                                                                <td><?= $FSTag['Title'] ?></td>
                                                                <td  align="center">
                                                                    <div id="ComputerPlusStatus"></div>
                                                                </td>
                                                                <td align="left">
                                                                    <div id="ComputerPlusDateDisplay"></div>
                                                                </td>
                                                            </tr>
                                                        <?php
                                                        }
                                                        else if ($FSTag['Name'] == "TechForceICAgreement" && $isShown)
                                                        {
                                                        ?>
                                                            <tr id="TechForceCredential">
                                                                <td><?= $FSTag['Title'] ?></td>
                                                                <td  align="center">
                                                                    <div id="TechForceCredentialStatus"></div>
                                                                </td>
                                                                <td align="left">
                                                                    <div id="TechForceCredentialDateDisplay"></div>
                                                                </td>
                                                            </tr>                                              
                                                        <?php
                                                        }
                                                        else if ( ($FSTag['Name'] == "CompuComTechnician" || $FSTag['Name'] == "CompuComAnalyst" || $FSTag['Name'] == "CompuComSpecialist" ) && ($DidCompuComDisplay===false) && $isShown )
                                                        {
                                                            $DidCompuComDisplay=true;
                                                        ?>
                                                            <tr id="CompuComCertified">
                                                                <td id="CompuComCertifiedtitle">CompuCom Certified</td>
                                                                <td  align="center">
                                                                    <div id="CompuComCertifiedStatus"></div>
                                                                    <?php
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <select id="CompuComCertifiedStatusInput" name="CompuComCertifiedStatusInput" style="display:none;" onChange="if(jQuery('#CompuComCertifiedDateInput').val() == '' && jQuery('#CompuComCertifiedStatusInput').find('option:selected').val()!=''){jQuery('#CompuComCertifiedDateInput').val('<?= date('m/d/Y') ?>');}else  if(jQuery('#CompuComCertifiedStatusInput').find('option:selected').val()==''){jQuery('#CompuComCertifiedDateInput').val('');}">
                                                                                <option value="">Select Level</option>
                                                                                <option value="33">Level 1 - Technician</option>
                                                                                <option value="34">Level 2 - Analyst</option>
                                                                                <option value="35">Level 3 - Specialist</option>
                                                                            </select>
                                                                            <input id="CompuComCertifiedStatusHidden" type="hidden" name="CompuComCertifiedStatusHidden" value="" />
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td align="left">
                                                                    <div id="CompuComCertifiedDateDisplay"></div>
                                                                    <?php
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input id="CompuComCertifiedDateInput" style="display:none;" type="text" size="15" name="CompuComCertifiedDateInput" />
                                                                            <input id="CompuComCertifiedDateHidden" type="hidden" name="CompuComCertifiedDateHidden" value="" />
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                        <?php
                                                        }
                                                        else if ( ($FSTag['Name'] == "PurpleTechnicianUnverified" || $FSTag['Name'] == "PurpleTechnicianCertified") && ($DidPurpleTechnicianDisplay===false) && $isShown )
                                                        {
                                                            $DidPurpleTechnicianDisplay=true;
                                                        ?>
                                                            <tr id="PurpleTechnicianCertified">                                        
                                                                <td id="PurpleTechnicianCertifiedtitle"><?= $FSTag['Title'] ?></td>
                                                                <td  align="center">
                                                                    <div id="PurpleTechnicianCertifiedStatus"></div>
                                                                    <?php
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <select id="PurpleTechnicianCertifiedStatusInput" name="PurpleTechnicianCertifiedStatusInput" style="display:none;" onChange="if(jQuery('#PurpleTechnicianCertifiedDateInput').val() == '' && jQuery('#PurpleTechnicianCertifiedStatusInput').find('option:selected').val()!=''){jQuery('#PurpleTechnicianCertifiedDateInput').val('<?= date('m/d/Y') ?>');}else if(jQuery('#PurpleTechnicianCertifiedStatusInput').find('option:selected').val()==''){jQuery('#PurpleTechnicianCertifiedDateInput').val('');}">
                                                                                <option value="">Select Level</option>
                                                                                <option value="39">Unverified Purple Technician</option>
                                                                                <option value="40">Verified Purple Technician</option>
                                                                            </select>
                                                                            <input id="PurpleTechnicianCertifiedStatusHidden" type="hidden" name="PurpleTechnicianCertifiedStatusHidden" value="" />
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td align="left">
                                                                    <div id="PurpleTechnicianCertifiedDateDisplay"></div>
                                                                    <?php
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input id="PurpleTechnicianCertifiedDateInput" style="display:none;" type="text" size="15" name="PurpleTechnicianCertifiedDateInput" />
                                                                            <input id="PurpleTechnicianCertifiedDateHidden" type="hidden" name="PurpleTechnicianCertifiedDateHidden" value="" />
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>            
                                                        <?php
                                                        }
                                                        else if ($FSTag['Name'] == "ErgoMotionKeyContractor" && $isShown)
                                                        {
                                                        ?>
                                                            <!--735-->
                                                            <tr id="ErgoMotionKeyContractor" 
                                                                image="/widgets/images/Adjustable-Bed-Certified.png" 
                                                                styleimg="width:26px;height:16px;"
                                                                text="Adjustable Bed Certified">
                                                                <td><?= $FSTag['Title'] ?></td>
                                                                <td  align="center">
                                                                    <div id="ErgoMotionKeyContractorStatus"></div>
                                                                    <?php
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input value="1" class="checkboxTodayStatusInput" id="ErgoMotionKeyContractorStatusInput" type="checkbox" name="certificationsStatus_41" style="display:none;" />
                                                                            <input  type="text" style="display: none;" value="41" name="certifications[41]"  />
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td align="left">
                                                                    <div id="ErgoMotionKeyContractorDateDisplay"></div>
                                                                    <?php
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input class="DateTime" id="ErgoMotionKeyContractorDateInput" style="display:none;" type="text" size="15" name="certificationsDate_41" /> 
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>                            
                                                        <?php
                                                        }
                                                        else if ($FSTag['Name'] == "ErgoMotionCertifiedTechnician" && $isShown)
                                                        {
                                                        ?>
                                                            <tr id="ErgoMotionCertifiedTechnician" 
                                                                image="/widgets/images/Adjustable-Bed-Expert-2.png"
                                                                styleimg="width:30px;height:16px;"
                                                                text="Adjustable Bed Expert">
                                                                <td><?= $FSTag['Title'] ?></td>
                                                                <td  align="center">
                                                                    <div id="ErgoMotionCertifiedTechnicianStatus"></div>
                                                                    <?php
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input value="1" class="checkboxTodayStatusInput" id="ErgoMotionCertifiedTechnicianStatusInput" type="checkbox" name="certificationsStatus_42" style="display:none;" />
                                                                            <input  type="text" style="display: none;" value="42" name="certifications[42]"  />
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td align="left">
                                                                    <div id="ErgoMotionCertifiedTechnicianDateDisplay"></div>
                                                                    <?php
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input class="DateTime" id="ErgoMotionCertifiedTechnicianDateInput" style="display:none;" type="text" size="15" name="certificationsDate_42" /> 
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>
                                                            <!--end 735-->
                                                        <?php
                                                        }
                                                        else if ($FSTag['Name'] == "EndeavorCertified" && $isShown)
                                                        {
                                                        ?>
                                                            <tr id="EndeavorCertified" 
                                                                image="/widgets/images/EndeavorTag.png"
                                                                styleimg="width:16px;height:16px;"
                                                                text="Endeavor Certified Technician">
                                                                <td><?= $FSTag['Title'] ?></td>
                                                                <td  align="center">
                                                                    <div id="EndeavorCertifiedStatus"></div>
                                                                    <?php
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input value="1" class="checkboxTodayStatusInput" id="EndeavorCertifiedStatusInput" type="checkbox" name="certificationsStatus_44" style="display:none;" />
                                                                            <input  type="text" style="display: none;" value="44" name="certifications[44]"  />
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </td>
                                                                <td align="left">
                                                                    <div id="EndeavorCertifiedDateDisplay"></div>
                                                                    <?php
                                                                    if($isAllowEditCert==1)
                                                                    {
                                                                        ?>
                                                                            <input class="DateTime" id="EndeavorCertifiedDateInput" style="display:none;" type="text" size="15" name="certificationsDate_44" /> 
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </td>
                                                            </tr>                                               
                                                        <?php
                                                        }
                                                        else if($FSTag['Name'] != "PurpleTechnicianUnverified" 
                                                                && $FSTag['Name'] != "PurpleTechnicianCertified"  
                                                                && $FSTag['Name'] != "CompuComTechnician" 
                                                                && $FSTag['Name'] != "CompuComAnalyst" 
                                                                && $FSTag['Name'] != "CompuComSpecialist" && $isShown )
                                                        {
                                                        ?>
                <tr id="CSCDesksideSupportTech$FSTag['']">
                    <td><?= $FSTag['Title'] ?> </td>
                    <td id="FSTag_<?= $FSTag['Name'] ?>_Status" align="center">
                        <div id="FSTag_<?= $FSTag['Name'] ?>_StatusDisplay" class="divFSTagImage">
                            <!--13931-->
                            <?php if (!empty($FSTag['TechId'])) {                            
                                    if($FSTag['HasMultiTagLevels']==1 && !empty($FSTag['TagLevelsData'])) {      
                            ?>
                    <img src="<?= $ExistingImagePath ?>" height="15px"/>&nbsp;&nbsp;L<?=$FSTag['LevelOrder']?>&nbsp;-&nbsp;<?=$FSTag['LevelTitle']?> 
                            <?php   } else {                                        
                            ?>
                                    <img src="<?= $ExistingImagePath ?>" height="15px"/>
                            <?php     
                                    }
                            ?>                                   
                            <?php } ?>
                        </div>
                        <?php if($isAllowEditCert==1) {                                 
                            if($FSTag['HasMultiTagLevels']==1 && !empty($FSTag['TagLevelsData'])) {                                                          
                            ?>
                            <div id="divHasMultiTagLevels">
                                <select name="FSTag_<?= $FSTag['Name'] ?>_MultiLevelCbx" style="display:none; width:120px;" id="FSTag_<?=$FSTag['Name'] ?>_MultiLevelCbx" class="cbxHasMultiTagLevels" onChange="fillDateMultiLevel(this.name,this.value);">
                                <option value="">Select Level</option>
                            <?php
                            
                                foreach ($FSTag['TagLevelsData']  as $itemLevel) {
                                ?>
                                    <option value="<?= $itemLevel['id'] ?>" <?=($FSTag['FSTagLevelId']==$itemLevel['id'])?"selected":"";?>>L<?=$itemLevel['DisplayOrder']?>&nbsp;-&nbsp;<?=$itemLevel['Title']?></option>                                    
                                <?php 
                              
                                } ?>  
                             </select>        
                            </div>                 
                            <?php   } else {                                        
                            ?>
                                    <input id="FSTag_<?= $FSTag['Name'] ?>_Chk" type="checkbox" name="FSTag_<?= $FSTag['Name'] ?>_Chk" style="display:none;" class="chkFSTag" <?php if (!empty($FSTag['TechId'])) {  ?> checked="checked" <?php } ?> />
                            <?php     
                                    }
                            ?>                         
                        <?php } ?>
                        <!--End 13931 -->
                    </td>
                    <td align="left">
                        <div class="divFSTagDate" id="FSTag_<?= $FSTag['Name'] ?>_Date"><?= $date ?></div>
                        <?php if($isAllowEditCert==1) { ?>
                                <input id="FSTag_<?= $FSTag['Name'] ?>_Datetxt" style="display:none;" type="text" size="15" name="FSTag_<?= $FSTag['Name'] ?>_Datetxt" class="txtFSTagDate" value="<?=$date?>" /> 
                        <?php } ?>
                    </td>
                </tr>
                                                            <script>
                                                                jQuery(".checkboxTodayStatusInput").click(function()
                                                                {
                                                                    var id = jQuery(this).attr("id");
                                                                    var key = id.replace("StatusInput","");

                                                                    if(jQuery("#"+key+"DateInput").val() == "" && jQuery(this).is(":checked")) {
                                                                        <?php
                                                                        $TagArt = "https://s3.amazonaws.com/wos-docs/tags/" . str_replace ("|", "", $FSTag[TagArt]);
                                                                        ?>
                                                                    } else {
                                                                        <?php
                                                                        $TagArt = "/widgets/images/check_green.gif";
                                                                        ?>
                                                                    }    
                                                                });
                                                            </script>
                                                        <?php
                                                        }//end general case
                                                    }// end of for $FSTagList
                                                }//end of if (!empty($FSTagList))
                                                //end 912
                                                ?>
						</tbody>
                                                <!--end 624-->
						<input type="hidden" id="FLS_Photo_IDHidden" name="FLS_Photo_IDHidden" />
						<input type="hidden" id="flsCSPHidden" name="flsCSPHidden" />
					</table>
                                            <script>
    _blackbox = BlackBox.CreateObject({id:'_blackbox'});
                              _blackbox.buildClientViewTechEvent();
                    </script>  
					</form>
				</div>

				</div>
			</div><!-- END FS Trusted Tech -->
			
			<div id="techSchedule" style="display:none;">
			    <div style="position:relative; left: 1px; width: 628px; height:38px;">
		            <ul  class="tabs" style="border: 0px;" id="scheduleTabs">
		                <li class="currenttab" style="width:150px;"><a class="current" href="javascript:void(0)" style="width: 129px; font-size: 12px;">Tech Schedule</a></li>
		            </ul>
		            <div class="tabsbottom" style="border: 0px;"></div>
			    </div>
				<!--<div>
		      		<ul class="tabs" id="scheduleTabs">
		      			<li><a href="#">Tech Schedule</a></li>
		      		</ul>
		      	</div>-->
                            <div class="fstechSchedule" style="clear: left; position: relative; top: 4px;">
                                <div>
                                    <table class="jobHistory" width="100%" id="fstechScheduleTable2" >
						<thead>
                                                    <tr id="fstechScheduleColumns2" class="tableHeader">
		      	
							</tr>
						</thead>
						<tbody>
						
						</tbody>
					</table>
		      	</div>
			</div>
			</div>
			
			<div id="contactInfo" style="display:none;">
			    <div style="height:38px;">
		            <ul  class="tabs" style="border: 0px;" id="scheduleTabs">
		                <li class="currenttab" style="width:150px;"><a class="current" href="javascript:void(0)" style="width: 129px; font-size: 12px;">Contact Information</a></li>
		            </ul>
		            <div class="tabsbottom" style="border: 0px;"></div>
			    </div>
				<!--<div>
		      		<ul class="tabs" id="scheduleTabs">
		      			<li><a href="#">Contact Information</a></li>
		      		</ul>
		      	</div>-->
		      	
		      	<div style="background-color: #F0F8FF; clear: left; position: relative; top: 4px;">
		      		<table id="contactInfoDisplay"  style="line-height: 17px;">
		      			<tr>
		      				<td>
		      					<div style="float:left;" >
		      						<span id="techName" style="float:left;"></span><br />
		      						<span id="techStreet"></span><br />
		      						<span id="techCityStateZip"></span>
									<span id="CountryNameLabel"></span>
		      					</div>
		      					<div style="float:right; text-align:right;">
                                            <table cellpadding="0" cellspacing ="0" >
                                                <tr>
                                                    <td style="text-align:right;">Date Registered:&nbsp;</td>
                                                    <td><span id="techDateRegistered"></span></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:right;">Last Login:&nbsp;</td>
                                                    <td><span id="techLastLogin"></span></td>
                                                </tr>
                                                <tr>
                                                    <td  style="text-align:right;">
		      						<span id="techResume"></span>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!---->
		      					</div>
		      				</td>
		      			</tr>
		      			<tr><td><br /></td></tr>
		      			<tr>
		      				<td>
		      					<table style="width: 240px;">
		      					<tr>
					      				<td>Day: </td>
					      				<td id="techDayPhone"></td>
					      			</tr>
					      			<tr id="techEvePhoneRow" style="display:none;">
					      				<td>Evening: </td>
					      				<td id="techEvePhone"></td>
					      			</tr>
					      			<tr>
					      				<td>Email: </td>
					      				<td id="techEmail"></td>
					      			</tr>
					      			<tr id="techSecondaryEmailRow" style="display:none;">
					      				<td>Secondary Email: </td>
					      				<td id="techSecondaryEmail"></td>
					      			</tr>
					      			<tr>
					      				<td>SMS: </td>
					      				<td id="techSMS"></td>
					      			</tr>
			      				</table>
			      			</td>
			      		</tr>
		      			
		      			<tr><td><br /></td></tr>
		      			
		      			<!--  tr>
		      				<td style="font-weight:bold;">Parts Shipping Address</td>
		      			</tr>
		      			<tr><td id="techShipName"></td></tr>
		      			<tr><td id="techShipStreet"></td></tr>
		      			<tr><td id="techShipCity"></td></tr>
		      			<tr><td id="techShipAttn"></td></tr>
		      			<tr><td id="techShipPhone"></td></tr -->
		      			
		      			<tr class="contactIsoInfo"><td style="font-weight:bold;">ISO Address</td></tr>
		      			<tr class="contactIsoInfo"><td id="isoName"></td></tr>
		      			<tr class="contactIsoInfo"><td id="isoContact"></td></tr>
		      			<tr class="contactIsoInfo"><td id="techIsoStreet"></td></tr>
		      			<tr class="contactIsoInfo"><td id="techIsoCity"></td></tr>
		      			<tr class="contactIsoInfo"><td id="techIsoAttn"></td></tr>
		      			<tr class="contactIsoInfo"><td id="techIsoPhone"></td></tr>
		      		</table>
		      	</div>
			</div>
  
<?php script_nocache ("/clients/js/SATProfileDisplay.js"); ?>
<?php
if (empty($_GET["simple"]) || !empty($_GET['back'])):
?>
<?php script_nocache ("/clients/js/backButton.js"); ?>
<?php
else:
?>

<script type="text/javascript">    	
	$("#Mod0EditRecord").hide();
	$("#backRelay").hide();
	$("#backRelay2").hide();
</script>
	</body>
</html>
<?php
endif;

require ("../footer.php");
?>

