<?php $page = "clients"; ?>
<?php $option = "reports"; ?>
<?php $selected = 'MetricsReport'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php require ("includes/adminCheck.php"); ?>
<?php
	$displayPMTools = true;
	require_once("includes/PMCheck.php");
?>
<?php

	$id = $_GET["v"];

?>

<!-- Add Content Here -->
<br /><br />

<div align="center">
<br /><br />
<table width="400" border="1" cellspacing="5" cellpadding="0">
  <tr>
    <th nowrap="nowrap" scope="col" align="left">Metrics Reports</th>
    </tr>
  <tr>
    <td nowrap="nowrap">&nbsp;</td>
  </tr>
  <tr>
    <td nowrap="nowrap"><a href="report_ActivitySummary.php?v=<?php echo $id; ?>">Metrics Summary</a><!--<div align="center"><a href="workorderfinancials.php">Work Order Financials</a></div> --></td>
    </tr>
  <tr>
    <td nowrap="nowrap"><!--<div align="center"><a href="workOrdersOOS.php">Out of Scope Work Orders</a></div> -->
      <a href="report_ActivityByProject.php?v=<?php echo $id; ?>">Metrics by Project</a></td>
    </tr>
  <tr>
    <td nowrap="nowrap"><!--<div align="center"><a href="approveworkorders.php">Work Orders: Tech Complete, Not Approved</a></div> -->
      <a href="report_ActivityByRecruiter.php?v=<?php echo $id; ?>">Metrics by Coordinator</a></td>
    </tr>
  <tr>
    <td nowrap="nowrap"><!--<div align="center"><a href="openWorkOrders.php">Open Work Order Report</a></div> -->
      <a href="report_ActivityByState.php?v=<?php echo $id; ?>">Metrics by State</a></td>
    </tr>
	 <tr>
	    <td nowrap="nowrap">
              <a href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=FAI_Financial%20Metrics%20Report.prpt')">Financial Metrics Report</a>
	</tr>
</table>



</div>


<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
