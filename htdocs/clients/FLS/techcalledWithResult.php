<?php
//ini_set("display_errors", 1);

require_once("../../library/timeStamps2.php");
require_once("../../library/smtpMail.php");
//ini_set("display_errors",1);
$db = Zend_Registry::get('DB');

$woID = $_GET['woID'];
$UserName = $_SESSION['UserName'];
$call = $_GET['call'];
$result = $_GET['result'];
$resultTxt = $_GET['resultTxt'];
$projectName = $_GET['Project'];
$TechID = $_GET['TechID'];
$TechName = $_GET['TechName'];
if ($call == '1') {
	$Description = "Pre-Call 1";
	$callField = "PreCall_1";
} else if ($call == '2') {
	$Description = "Pre-Call 2";
	$callField = "PreCall_2";
} else {
	// Some error occured, shouldn't ever happen though!
	$Description = "Pre-Call Errors!";
	smtpMail("Pre-Call Error", "nobody@fieldsolutions.com", "gbailey@fieldsolutions.com", "FTXS Tech Confirmed Pre-Call Script Error: " . '$Description' . "WO ID: " . '$woID', "$fault", "$fault", "techcalled.php");
}
$resultTxt = empty($resultTxt) ? "None" : "$resultTxt";
$Description .= " - $result - $resultTxt";

if ($result != "Backed Out") {
	if ($result == "Reset")
		$db->update("work_orders", array($callField => 0, $callField . "_Status" => new Zend_Db_Expr('NULL')), $db->quoteInto("WIN_NUM = ?", $woID));
	else
		$db->update("work_orders", array($callField => 1, $callField . "_Status" => $result), $db->quoteInto("WIN_NUM = ?", $woID));
}
else {
	$user = $_SESSION['UserName']."|pmContext=FLS";
	$pass =  $_SESSION['Password'];
	$api = new Core_Api_Class;
	$curr = $api->getWorkOrder($user, $pass, $woID);
	$curr = $curr->data[0];
	if ($curr->Approved || $curr->Invoiced) {
		echo "Error: Cannot back out approved work order";
		exit;
	}
	$wo = new API_WorkOrder;
	$wo->ShowTechs = true;
	$wo->TechMarkedComplete = false;
	$wo->Tech_ID = "";
	$wo->Tech_Bid_Amount = "";
	$wo->BackOut_Tech = $curr->Tech_ID;
	$api->updateWorkOrder($user, $pass, $woID, $wo);
	$db->update("work_orders", array($callField => 0, $callField . "_Status" => $result), $db->quoteInto("WIN_NUM = ?", $woID));
}

Create_TimeStamp($woID, $UserName, $Description, 'FLS', 'Fujitsu Transactions Solutions', $projectName, "", $TechID, $TechName);
//Create_TimeStamp($woID, $UserName, "Work Order Updated: FLS_ID,TechEmail,Tech_ID,TechPhone,Tech_FName,Tech_LName", 'FLS', 'Fujitsu Transactions Solutions', $projectName, "", $TechID, $TechName);

echo "$result";
