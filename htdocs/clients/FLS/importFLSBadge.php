<?php $page = "clients"; ?>
<?php $option = "techs"; ?>
<?php $selected = "flsidupload"; ?>
<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>
<?php require ("../includes/PMCheck.php"); ?>

<!-- Add Content Here -->

<style type="text/css">
</style>

<br/><br/>

<div style="text-align: center">
<form name="uploadImport" method="post" action="<?php echo $_SERVER['PHP_SELF']?>?<?=$_SERVER['QUERY_STRING']?>" enctype="multipart/form-data">
	<label for="importFile">FLS Badge Import:</label>
	<input name="importFile" type="file" size="40"/>
	<input name="ignoreWarning" type="checkbox" value="1" /> Ignore warnings<br/><br/>
	<input name="submit" type="submit" value="Submit" />
</form>

<br/>
<?php
	require_once("../../library/caspioAPI.php");
//	require_once("importInclude.php");
	
	if (isset($_POST["submit"])) {
		// CSR# - FLSID
		// Warehouse - FLSWhse
		// Fedex Tracking - FLSBadgeFedX (NO LONGER USED! - GB)
		// FLSBadge
		
		$requiredColumns = array("TechID", "FLSID");
		
		if (!isset($_FILES["importFile"]["tmp_name"]) || !file_exists($_FILES["importFile"]["tmp_name"])) {
			echo "No file Uploaded";
			die();
		}
		
		if ($_FILES["importFile"]["size"] > 150000) {
			// file too large
		}

		echo "Checking uploaded file ... <br/><br/>";
			
		$handle = fopen($_FILES["importFile"]["tmp_name"], "r");
		$importColumns = fgetcsv($handle);
		
		$sizeImport = sizeof($importColumns);
		$sizeRequired = sizeof($requiredColumns);
		if ($sizeImport != $sizeRequired) {
			echo "Unexpected number of columns found - expecting $sizeRequired but found $sizeImport";
			die();
		}
		
		$index = 0;
		foreach ($importColumns as $col) {
			if ($col != $requiredColumns[$index]) {
				echo "Unexpected column found - expecting '{$requiredColumns[$index]}' but found '$col'";
				die();
			}
			$index++;
		}
		
		$updateQuery = "";
		
		$TechIDList = array();
		$TechIDFoundMap = array();
		$updateQuery = array();
		
		while (!feof($handle)) {
			$importRow = fgetcsv($handle);
			if (sizeof($importRow) == 1 && $importRow[0] == '') continue;
			$TechID = $importRow[0];
			$TechIDList[] = $TechID;
			$FLSID = $importRow[1];
			
			if (!is_numeric($TechID)) {
				echo "TechID is not a number: $TechID";
				die();
			}
			if (!array_key_exists($TechID, $TechIDFoundMap)) {
				$TechIDFoundMap[$TechID] = 0;
			}
			else {
				echo "Duplicate TechID: $TechID";
				die();
			}
			$updateQuery[$TechID] = array($FLSID, 1);			
		}
		
		if (sizeof($TechIDList) == 0) {
			echo "No data found.";
			die();
		}
		
		$numImportedRows = sizeof($TechIDList);
//		$TechIDList = implode(",", $TechIDList);
		
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_TECH_BANK_INFO, array('TechID', 'FLSID'))->where('TechID IN (?)', $TechIDList);
		$TechIDFound = $db->fetchAll($select);
			
		$error = false;
		$warning = false;
		
		if ($numImportedRows != sizeof($TechIDFound)) {
			foreach ($TechIDFound as $row) {
				$id = $row['TechID'];
				if (array_key_exists($id, $TechIDFoundMap)) {
					$TechIDFoundMap[$id] = 1;
				}
			}
			echo "Unable to find the following TechID: <br/>";
			foreach ($TechIDFoundMap as $key=>$val) {
				if ($val == 1) continue;
				echo "$key<br/>";
				$error = true;
			}
			echo "<br/>";
		}
				
		foreach ($TechIDFound as $row) {
			$id = $row['TechID'];
			$flsID = $row['FLSID'];
			
			if ($flsID == null || $flsID == "") continue;
			$warning = true;
			echo "Warning: TechID $id already has a FLSID - $flsID<br/>";
		}
		
		if ($error || ($warning && !isset($_POST["ignoreWarning"])) ) {
			echo "<br/>No data imported.";
			die();
		}
					
		echo "<br/>Checks Successful.<br/>";
		
		echo "<br/>Importing ... <br/>";
		foreach ($updateQuery as $TechID => $values) {
//			echo "UPDATE (FLSID, FLSWhse, FLSBadgeFedX, FLSBadge) $values WHERE TechID = $TechID<br/>";			
			$db->update(Core_Database::TABLE_TECH_BANK_INFO, array_combine(array('FLSID', 'FLSBadge'), $values), $db->quoteInto('TechID = ?', $TechID));
			Core_History::logTechProfileUpdate($TechID, 'main');
		}
		
		echo "<br/>Import Complete.<br/>";
	}
?>
</div>
<?php require ("../../footer.php"); ?>
