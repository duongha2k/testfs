<?php $page =  'clients'; ?>
<?php $option = 'reports'; ?>
<?php $selected = 'FLS_OOS_Report'; ?>
<?php
	require_once("../../library/caspioAPI.php");
	if ($_SESSION['loggedIn'] != "yes" || $_SESSION['loggedInAs'] != "client")
		header("Location: /");
        $Company_ID = (!empty($_GET['v']) ) ? $_GET['v'] : $_SESSION['Company_ID'];
	function getProjects() {
		$db = Zend_Registry::get('DB');
		$projects = $db->fetchAll("SELECT Project_ID AS id, Project_Name AS name FROM projects WHERE Project_Company_ID = ? ORDER BY Project_Name", array($_GET["v"]));
		return $projects;
	}
	
	function FTXS_CR_putcsv($row, $fd=',', $quot='"')
    {
       $str='';
       foreach ($row as $cell)
       {
          $cell = str_replace($quot, $quot.$quot, $cell);
          if (strchr($cell, $fd) !== FALSE || strchr($cell, $quot) !== FALSE || strchr($cell, "\n") !== FALSE)
          {
             $str .= $quot.$cell.$quot.$fd;
          }
          else
          {
             $str .= $cell.$fd;
          }
       }
       echo substr($str, 0, -1)."\n";
       
//       return strlen($str);
    }
?>
<?php

$download = !empty($_GET["download"]);

if (!$download):
?>
<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>
<?php require ("../includes/adminCheck.php"); ?>
<?php require_once ("../includes/PMCheck.php"); ?>

<link rel="stylesheet" href="/library/jquery/calendar/jquery-calendar.css" type="text/css" />
<link rel="stylesheet" href="/widgets/css/main.css" type="text/css" />

<script type="text/javascript" src="/library/jquery/calendar/jquery-calendar.js"></script>

<script type="text/javascript" src="/widgets/js/FSWidgetManager.js"></script>
<!--  script type="text/javascript" src="/clients/js/popupWindow.js"></script -->
<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetDashboard.js?11232011"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetSortTool.js?11112011"></script>
<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
<?php if (strtolower($Company_ID) == 'fls'): ?>
    <script type="text/javascript" src="/widgets/js/FSWidgetSortToolFLS.js"></script>
<?php endif; ?>
<?php 
endif;?>
<?php if (!isset($_GET["search"])):?>

<script type="text/javascript">
    // fill select element with data
    function fillSelect(data, el_id) {
        var selectEl = document.getElementById(el_id);
        var idx,i;
        i = 1;
        if ( data && selectEl ) {
            for ( idx in data ) {
		try {
	                var prj = <?=!isset($_GET["project"])?"1":$_GET["project"]?>;
	                selectEl.add(new Option(data[idx].name, data[idx].id, false, false), null);
                        if (data[idx].id == prj)
                        {
                            selectEl.selectedIndex = i;
                        }
                        i++;
		} catch (e) {
			$(selectEl).append("<option value=\"" + data[idx].id + "\">" + data[idx].name + "</option>");
			selectEl.selectedIndex = 0;
		}
            }
        }
    }
    $(document).ready(function () {
	var projects = <?=json_encode(getProjects())?>;
        // Init calendar
        $.map(['find_StartDate_to','find_StartDate_from'], function (id) {
               
            $('#'+id).calendar({dateFormat:'MDY/'});
            $('#'+id).focus( function() {
                this.blur();
                $('#calendar_div').css('top', $(this).offset().top + 'px');
                $('#calendar_div').css('left', $(this).offset().left + $(this).width() + 7 + 'px');
                popUpCal.showFor(this);
            });
            // init buttons
            $('#' + id + '_img').click(function () {
                var id = $(this).attr('id').replace(/_img$/, '');
                var e  = document.getElementById(id);
                if ( e ) e.focus();
            });
        });
        fillSelect(projects, 'find_Project_ID');    // Make project select elements
        
/*        //send find form
        $('#FindWorkOrderForm').submit(function(event) {
            event.preventDefault();
            var idx, l, wd;
            var query = '';
            var formData = $(this).serializeArray();
            if ( opener ) {
                /* if opener isn't wos.php page open it */
/*                l = opener.location.toString().replace(/\?.*$/, '');
                if ( -1 === l.search(/wos\.php$/i) ) {
                    for ( idx in formData ) {
                        if ( formData[idx].value ) {
                            query += '&' + formData[idx].name + '=' + encodeURIComponent(formData[idx].value).replace(/\~/g, '%7E').replace(/\!/g, '%21').replace(/\'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29');
                        }
                    }

                    l  = opener.location.protocol + '//' + opener.location.host;
                    l += '/admin/wos.php?tab=all&search=1';
                    l += query;
                    opener.location = l;

                } else {        //  opener == wos.php
                    wd = opener.wd;
                    opener._search   = true;

                    wd.resetFilters();
                    
                    opener.$('#dashboardVersion').val('sp');

                    opener.window._company = $('#find_Company_ID').val();
                    
                    for ( idx in formData ) {
                        if ( !formData[idx].value ) {
                            wd.filters().set('_'+formData[idx].name, null);
                        } else {
                            wd.filters().set('_'+formData[idx].name, formData[idx].value);
                        }
                    }
                    wd.show({tab:'all', params:wd.getParams()});
                }
	            window.close();
            } else {
                for ( idx in formData ) {
                    if ( formData[idx].value ) {
                        query += '&' + formData[idx].name + '=' + encodeURIComponent(formData[idx].value).replace(/\~/g, '%7E').replace(/\!/g, '%21').replace(/\'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29');
                    }
                }

                l  = location.protocol + '//' + location.host;
                l += '/admin/wos.php?tab=all&search=1';
                l += query;
                //alert(l);
                location = l;
            }
            return false;
        });
		$("#find_win").focus();*/
    });
</script>
<div class="popup_form_holder" id="divFindWorkOrderForm" >
    <h1>FAI Tech Review & Confirm Report</h1>
<form action="<?=$_SERVER["PHP_SELF"]?>" method="get" id="FindWorkOrderForm" name="FindWorkOrderForm">
	<input type="hidden" id="v" name="v" value="<?=$_GET["v"]?>"/>
<table class="form_table" cellspacing="0">
    <col class="col1" />
    <col class="col2" />
    <tr>
        <td class="label_field">WIN #</td>
        <td><input type="text" value="<?=$_GET["win"]?>" id="find_win" name="win" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">Client Work Order #</td>
        <td><input type="text" value="<?=$_GET["wo_id"]?>" id="find_WO_ID" name="wo_id" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">Project</td>
        <td>
            <select id="find_Project_ID" name="project">
                <option value="0">Select Project</option>
            </select>
        </td>
    </tr>
    <tr>
        <td class="label_field">Start Date (&gt;=)</td>
        <td>
            <input type="text" style="cursor: pointer;" class="input_text input_date" name="date_from" id="find_StartDate_from" value="<?=!isset($_GET["date_from"])?"":$_GET["date_from"]?>" />&#160;
            <img id="find_StartDate_from_img" src="/widgets/images/calendar.gif" style="position:relative; top:4px; cursor: pointer;" />
        </td>
    </tr>
    <tr>
        <td class="label_field">Start Date (&lt;=)</td>
        <td>
            <input type="text" style="cursor: pointer;" class="input_text input_date" name="date_to" id="find_StartDate_to" value="<?=!isset($_GET["date_to"])?"":$_GET["date_to"]?>" />&#160;
            <img id="find_StartDate_to_img" src="/widgets/images/calendar.gif" style="position:relative; top:4px; cursor: pointer;" />
        </td>
    </tr>
    <tr>
        <td class="label_field">FLS ID</td>
        <td><input type="text" value="<?=$_GET["flsid"]?>" id="find_FLS_ID" name="flsid" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">Tech ID</td>
        <td><input type="text" value="<?=$_GET["tech"]?>" id="find_Tech_ID" name="tech" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">Tech FName</td>
        <td><input type="text" value="<?=$_GET["firstname"]?>" id="find_Tech_FName" name="firstname" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">Tech LName</td>
        <td><input type="text" value="<?=$_GET["lastname"]?>" id="find_Tech_LName" name="lastname" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">Pre Call 1</td>
        <td>
            <input type="radio" value="1" name="pc1" id="find_PreCall_1_1" <?=$_GET["pc1"] == '1'?"checked":""?> >Yes
            <input type="radio" value="0" name="pc1" id="find_PreCall_1_2" <?=$_GET["pc1"] == '0'?"checked":""?> >No
            <input type="radio" value="" name="pc1"  id="find_PreCall_1_3" <?=$_GET["pc1"] == ''?"checked":""?> >Any
        </td>
    </tr>
    <tr>
        <td class="label_field">Pre Call 2</td>
        <td>
            <input type="radio" value="1" name="pc2" id="tech_called_1" <?=$_GET["pc2"] == '1'?"checked":""?> >Yes
            <input type="radio" value="0" name="pc2" id="tech_called_2" <?=$_GET["pc2"] == '0'?"checked":""?> >No
            <input type="radio" value="" name="pc2" id="tech_called_3" <?=$_GET["pc2"] == ''?"checked":""?> >Any
        </td>
    </tr>
    <tr>
        <td class="label_field">Checked In</td>
        <td>
            <input type="radio" value="1" name="checkedin" id="find_CheckedIn_1" <?=$_GET["checkedin"] == '1'?"checked":""?>>Yes
            <input type="radio" value="0" name="checkedin" id="find_CheckedIn_2" <?=$_GET["checkedin"] == '0'?"checked":""?>>No
            <input type="radio" value="" name="checkedin"  id="find_CheckedIn_3" <?=$_GET["checkedin"] == ''?"checked":""?>>Any
        </td>
    </tr>

    <tr class="submit_raw">
        <td colspan="2" align="center"><input type="submit" name="search" id="search" value="Search" class="input_button" /></td>
    </tr>
</table>
</form>
</div>
<?php
else:
	$searchCriteria = array();

	function getOrderCriteria($sortColumn, $order, $specialColumn, $defaultSort, $sortMap) {
		// returns SQL code for current sorted column or returns default code
		if ($sortColumn == "")
			return $defaultSort;
		$order = ($order == 1 ? "DESC" : "ASC");
		if (array_key_exists($sortColumn, $sortMap))
			$sortCriteria = $sortMap[$sortColumn] . " " . $order;
		else if (array_key_exists($sortColumn, $specialColumn))
			$sortCriteria = $specialColumn[$sortColumn] . " " . $order;
		else
			$sortCriteria = caspioEscape($sortColumn) . " " . $order;
		if ($sortColumn != "BidAmount")
			$sortCriteria .= ", $defaultSort";
		return $sortCriteria;
	}

	// get variables

	//$db = Zend_Registry::get('DB');
    $db = Core_Database::getInstance();    

	$companyID = (isset($_GET["v"]) ? $_GET["v"] : "BLANKID");
	
	$sort = (isset($_GET["sort"]) ? $_GET["sort"] : "FLSID");
	$order = (isset($_GET["order"]) ? $_GET["order"] : 1);
        //14081
        $pc1 = $_GET["pc1"];
        $pc2 = $_GET["pc2"];
        $win = $_GET["win"];
        $wo_id = $_GET["wo_id"];
        $project = $_GET["project"];
        $flsid = $_GET["flsid"];
        $tech = $_GET["tech"];
        $firstname = $_GET["firstname"];
        $lastname = $_GET["lastname"];       
        $checkedin = $_GET["checkedin"];      
        $date_from = $_GET["date_from"];
        $date_to = $_GET["date_to"];

	// result page configuration
	$resultsPerPage = 25;
	if (isset($_GET["page"]) && is_numeric($_GET["page"]))
		$currentResultPage = $_GET["page"];
	else
		$currentResultPage = (isset($_SESSION["currentResultPage"]) ? $_SESSION["currentResultPage"] : 1);

	// fields pulled from DB
	$specialColumns = array();
        //858, 14156
	$fieldList = array("WIN_NUM","WO_ID","StartDate","StartTime","Project_Name","t.FLSID","w.Tech_ID","t.Firstname","t.Lastname","t.PrimaryPhone","PreCall_1","PreCall_2","SiteName","w.Address","w.City","w.State","w.Zipcode","Lead","Assist","PreCall_1_Status","PreCall_2_Status","STR_TO_DATE(concat('01/01/1800', StartTime),'%d/%m/%Y%h:%i %p') as StartTimesort","t.ISO_Affiliation_ID");
        //end 858, 14156
	$additionalFieldList = array("Bg_Test_Pass_Lite", "BG_Test_ResultsDate_Lite", "FLSCSP_Rec");

	// result page columns label to field name / sortable map. Format - "ColumnLabel" => "ColumnName|isSortable (0 = false, 1 = true)
        
        //14156 Remove Column  "FS Tech ID" => "Tech_ID|1", "FLS CSP" => "FLSCSP_Rec|1", Column "BC Check" not found
	$resultColumnMap = array("WIN #" => "WIN_NUM|1", "Client Work Order #" => "WO_ID|1", 
"Start Date" => "StartDate|1", "StartTime" => "StartTime|1", "Project Name" => "Project_Name|1", 
"FLS ID" => "FLSID|1","ISO Tech" => "ISO_Affiliation_ID|1",  "WO Tech FName" => "Firstname|1", 
"WO Tech LName" => "Lastname|1", "Tech PrimaryPhone" => "PrimaryPhone|1", "PreCall-1" => "PreCall_1|1", 
"PreCall-2" => "PreCall_2|1", "Site Name" => "SiteName|0", 
"Site Address" => "Address|1", "City" => "City|1", "State" => "State|1", "Zipcode" => "Zipcode|1", "Lead" => "Lead|1", "Assist" => "Assist|1", "Bkgd. Check" => "Bg_Test_Pass_Lite|0");

	$sortMap = array();

	$searchResult = array();

	// search order
	$defaultCriteria = "FLSID";
	$orderCriteria = getOrderCriteria($sort, $order, $specialColumns, $defaultCriteria, $sortMap);
	if (!$download)
		$offset = ($resultsPerPage * ($currentResultPage - 1));
	else
		$offset = 0;


	$select = $db->select();
	$select->from(array("w" => "work_orders"), $fieldList);
	$select->join(array("t" => "TechBankInfo"), "t.id = (SELECT z.id FROM TechBankInfo AS z WHERE z.TechID = w.Tech_ID)", array());
	$select->where("w.Company_ID = ? AND w.Deactivated = 0", $_GET["v"]);
	//$select->order($orderCriteria);
	        
        
	if (!empty($_GET["win"])) {
		$select->where("w.WIN_NUM = ?", $_GET["win"]);
	}
	if (!empty($_GET["wo_id"])) {
		$select->where("w.WO_ID = ?", $_GET["wo_id"]);
	}
	if (!empty($_GET["project"])) {
		$select->where("w.Project_ID = ?", $_GET["project"]);
	}
	if (!empty($_GET["date_from"])) {
		$ts = strtotime($_GET["date_from"]);
		if ($ts)
			$select->where("w.StartDate >= ?", date("Y-m-d", $ts));
	}
	if (!empty($_GET["date_to"])) {
		$ts = strtotime($_GET["date_to"]);
		if ($ts)
			$select->where("w.StartDate <= ?", date("Y-m-d", $ts));
	}
	if (!empty($_GET["flsid"])) {
		$select->where("t.FLSID = ?", $_GET["flsid"]);
	}
	if (!empty($_GET["tech"])) {
		$select->where("t.TechID = ?", $_GET["tech"]);
	}
	if (!empty($_GET["firstname"])) {
		$select->where("t.Firstname LIKE ?", "%" . $_GET["firstname"] . "%");
	}
	if (!empty($_GET["lastname"])) {
		$select->where("t.Lastname LIKE ?", "%" . $_GET["lastname"] . "%");
	}
        
        $callStatus_0 = "Unconfirmed";
        $callStatus_1 = "Confirmed";
	if (isset($_GET["pc1"]) && $_GET["pc1"] != "") {
            if($_GET["pc1"] == '0')
            {               
               $select->where("PreCall_1 = '0' OR (PreCall_1 = '1' AND PreCall_1_Status = '$callStatus_0')");
            }else
            {               
               $select->where("PreCall_1 = '1' AND PreCall_1_Status = '$callStatus_1'");
            }
	}
	if (isset($_GET["pc2"]) && $_GET["pc2"] != "") {
           
            if($_GET["pc2"] == '0')
            {               
               $select->where("PreCall_2 = '0' OR (PreCall_2 = '1' AND PreCall_2_Status = '$callStatus_0')");
            }else
            {               
               $select->where("PreCall_2 = '1' AND PreCall_2_Status = '$callStatus_1'");
            }
	}
	if (isset($_GET["checkedin"]) && $_GET["checkedin"] != "") {
		$select->where("CheckedIn = ?", $_GET["checkedin"]);
        }        
    
        
    if (isset($_GET["Sort1"]) && $_GET["Sort1"] != "") {
                $sort1=$_GET["Sort1"];
                $order1=$_GET["direct1"];
		$select->order($sort1." ".$order1);
		if( ($sort1 === "PreCall_1" || $sort1 === "PreCall_2")){
			if($order1 === "asc"){
				$select->order("IF(ISNULL(".$sort1."_Status),1,0),".$sort1."_Status ".$order1);
			}else{
				$select->order($sort1."_Status ".$order1);
			}
		}
	}
    else
    {
        //$sort1="WIN_NUM";
        //$order1="ASC";
        //$select->order($sort1." ".$order1);
    }
    if (isset($_GET["Sort2"]) && $_GET["Sort2"] != "") {
            $sort2=$_GET["Sort2"];
            $order2=$_GET["direct2"];
	        $select->order($sort2." ".$order2);
    		if( ($sort2 === "PreCall_1" || $sort2 === "PreCall_2")){
				if($order2 === "asc"){
					$select->order("IF(ISNULL(".$sort2."_Status),1,0),".$sort2."_Status ".$order2);
				}else{
					$select->order($sort2."_Status ".$order2);
				}
			}
	} 
    if (isset($_GET["Sort3"]) && $_GET["Sort3"] != "") {
                $sort3=$_GET["Sort3"];
                $order3=$_GET["direct3"];
		$select->order($sort3." ".$order3);
    	if( ($sort3 === "PreCall_1" || $sort3 === "PreCall_2")){
			if($order3 === "asc"){
				$select->order("IF(ISNULL(".$sort3."_Status),1,0),".$sort3."_Status ".$order3);
			}else{
				$select->order($sort3."_Status ".$order3);
			}
		}
	}   
    $searchResult = $db->fetchAll($select);                       

    $numResult=count($searchResult);
    
	if (sizeof($additionalFieldList) > 0) {
		$techList = array();
		foreach ($searchResult as $row) {
			if (empty($row["Tech_ID"])) continue;
			$techList[$row["Tech_ID"]] = 0;
		}
		$techList = array_keys($techList);
		if (sizeof($techList) > 0) {
			$additionalSearchResult = caspioSelectAdv("TR_Master_List", implode(",",$additionalFieldList) . ",TechID", "TechID IN (" . implode(",",$techList) .")", "", false, "`", "|", false);
		}

		$techList = array();
		$additionalFieldList[] = "TechID";
		$lastIndex = sizeof($additionalFieldList) - 1;
		if (!empty($additionalSearchResult)) {
			foreach ($additionalSearchResult as $row) {
				$row = explode("|", $row);
				$techList[$row[$lastIndex]] = array_combine($additionalFieldList, $row);
			}
		}

	}


	$resultsHtml = "";
	// pull in data based on quick search

	if ($numResult > 0) {
		// construct results page header
		$resultsHtml = "<table id=\"appTable\" class=\"resultsTable\"><thead><tr>";
		foreach ($resultColumnMap as $label => $map){
            $resultsHtml .= "<td>$label</td>";
		}
		$resultsHtml .= "</tr></thead><tbody>";
		// construct results page body
		$rowNumber = 1;
				
		foreach ($searchResult as &$row) {
			$winNum = trim($row["WIN_NUM"]);
			// alternate row color
			$rowColor = ($rowNumber % 2 == 0 ? "evenRow" : "oddRow");

			// grab commonly used fields
			$techID = $row["Tech_ID"];
			$resultsHtml .= "<tr class=\"$rowColor\">";
			foreach ($resultColumnMap as $label => $mapping) {
				// Processes each column returned and outputs result. By default, column is outputed as it is formated by the the SQL query
				$mapping = explode("|", $mapping);
				$mapping = $mapping[0];
				if (!isset($row[$mapping]))  {
					if (empty($techID) || !isset($techList[$techID][$mapping]))
						$row[$mapping] = "";
					else
						$row[$mapping] = $techList[$techID][$mapping];
				}
                                //13390
                                $fieldHTML="";
                                $field="";
				switch ($mapping) {
					case "WIN_NUM":
                                                $field=$row[$mapping];
						$fieldHTML = "<a href=\"javascript:openPopup('/clients/wosDetails.php?v=$companyID&id={$row[$mapping]}', 750, 400)\">{$row[$mapping]}</a>";
						break;
					case "Lead":
					case "Assist":
					case "PreCall_1":
					case "PreCall_2":
                                                $field=$row[$mapping] == 1 ? "Yes" : "No";
						$fieldHTML = $row[$mapping] == 1 ? "Yes" : "No";                                                
						break;
					case "FLSCSP_Rec":
                                                $field=$row[$mapping] === "True" ? "Yes" : "No";
						$fieldHTML = $row[$mapping] === "True" ? "Yes" : "No";
						break;
					case "StartDate":
						$ts = strtotime($row[$mapping]);
						if ($ts)
                                                {
                                                        $field=date("m/d/Y", $ts);
							$fieldHTML = date("m/d/Y", $ts);
                                                }
						else 
                                                {
                                                        $field="";
							$fieldHTML = "";
                                                }
						break;
					case "Bg_Test_Pass_Lite":
						if (trim($row[$mapping], "`") == "Pass") {
                                                        $field=date("m/d/Y", strtotime($techList[$techID]["BG_Test_ResultsDate_Lite"]));
							$fieldHTML = date("m/d/Y", strtotime($techList[$techID]["BG_Test_ResultsDate_Lite"]));
						} else {
                                                        $field="No";
							$fieldHTML = "No";	
						}
						break;
                                        case "FLSID":
                                            $field=$row[$mapping];
                                            $fieldHTML="<a target=\"_blank\" href=\"../wosViewTechProfile.php?simple=0&amp;v=".$companyID."&amp;TechID=".$row["Tech_ID"]."\" title=\"Tech Profile\" class=\"ajaxLin\">".$row[$mapping]."</a>";
                                            break;
                                        case "ISO_Affiliation_ID":
                                            $field=(is_null($row[$mapping])||$row[$mapping]==''||$row[$mapping]==NULL)? "No" : "Yes";
                                            $fieldHTML =(is_null($row[$mapping])||$row[$mapping]==''||$row[$mapping]==NULL)? "No" : "Yes";
                                            break;
					default:
                                                $field=$row[$mapping];
						$fieldHTML = $row[$mapping];
						if ($fieldHTML == "NULL") $fieldHTML = "";
                                                if($field=="NULL") $field="";
						break;
				}
				$row[$mapping] = $field;
				$resultsHtml .= "<td>$fieldHTML</td>";
                                //end 13390
			}
			$resultsHtml .= "</tr>";
			$rowNumber++;
		}

		$resultsHtml .= "</tbody>";
                //13390
                $resultsPerPage=$numResult;
                //13390
		// construct results page footer
		$prevPageBtn = ($currentResultPage > 1 ? "<span id=\"prevPageBtn\" onclick=\"prevPage()\">&lt;</span>" : "");
		$numPages = ceil($numResult / $resultsPerPage);
		$nextPageBtn = ($currentResultPage <  $numPages? "<span id=\"nextPageBtn\" onclick=\"nextPage()\">&gt;</span>" : "");
		$pageSelect = "<select id='pageSelect' name='pageSelect' style='width: 5em; text-align: center' onChange='gotoPage(this.selectedIndex + 1);'>";
		for ($i = 1; $i <= $numPages; $i++) {
			$start = 1 + (($i - 1) * $resultsPerPage);
			$pageSelect .= "<option value='$i'" . ($currentResultPage == $i ? " selected='selected' " :  "") . ">" . $start  . " - " . min($start + $resultsPerPage, $numResult) . "</option>";
		}
		$pageSelect .= "</select>";
		$resultsHtml .= "<tfoot><tr><td colspan=\"" . sizeof($resultColumnMap) . "\">$prevPageBtn Records " . $pageSelect . " of $numResult $nextPageBtn</td></tr></tfoot></table>";
	}
	else {
		$resultsHtml = "<div class=\"noResultsDiv\">No work order found.</div>";
	}

	if (isset($_GET["sort"]) || isset($_GET["order"]) || isset($_GET["page"])) {
		// parse querystring
		$arr = Array();
		$ignoreGet = array("sort" => 1, "order" => 1, "page" => 1);
		foreach ($_GET as $key => $val) {
			if (array_key_exists($key, $ignoreGet)) continue;
			$arr[] = "$key=$val";
		}
		$qs = implode("&", $arr);
	}
	else
		$qs = $_SERVER['QUERY_STRING'];
		
	if ($download):
		header("Content-Type: text/csv");
		header("Content-Disposition: attachment; filename=techConfirmReport.csv");
		// download
		$rowColumns = array();
		$rowLabels = array();
		foreach ($resultColumnMap as $label => $mapping) {
			$mapping = explode("|", $mapping);
			$mapping = $mapping[0];
			$rowColumns[] = $mapping;
			$rowLabels[] = $label;
		}
		FTXS_CR_putcsv($rowLabels);
		foreach ($searchResult as $_row) {
			$csvRow = array();
			foreach ($rowColumns as $col) {
				if ($_row[$col] == "Yes" && ($col == "PreCall_1" || $col == "PreCall_2")) {
					$csvRow[] = $_row[$col . "_Status"] == "Unconfirmed" ? "Pending" : "Yes";
				}
				else {
					$csvRow[] = $_row[$col];
                                }
			}
			FTXS_CR_putcsv($csvRow);
		}
	else:

?>

<style type="text/css">
	#emailBlast {
		text-align: center;
		max-width:
	}
	.verdana2 {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		clear: both;
	}

	.verdana2bold {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		font-weight: bold;
		clear: both;
	}

	.fieldGroup {
		padding-bottom: 5px;
		border-bottom: 2px solid #2A497D;
	}

	.formBox, .formBoxFooter {
		margin: 15px auto 0px auto;
		padding: 15px;
		width: 920px;
		color: #2A497D;
		font-size: 12px;
		border: 2px solid #2A497D;
	}

	.formBox input {
		font-size: 12px;
		height: 14px;
	}

	.formBoxFooter {
		background-color: #BACBDF;
		text-align: center;
		margin: 0px auto 10px auto;
		padding: 5px 15px;
		border-top: none;
	}
	.formRow3, .formRow4, .formRow5, .formRow6 {
		width: 100%;
		clear: both;
	}
	.formRow3 div {
		width: 33%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow4 div {
		width: 25%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow5 div {
		width: 20%;
		float: left;
		margin-bottom: 5px;
	}

	.formRow6 div {
		width: 16%;
		float: left;
		margin-bottom: 5px;
	}
	
	#searchControls {
		text-align:right;
	}

	.resultsTable {
/*		width: 80%;*/
		margin: 1px auto 0px auto;
		border: 2px solid #032D5F;
	}

	.resultsTable thead {
		text-align: center;
		color: #FFF;
		border: 1px solid #2A497D;
		background-color: #5091CB;
		cursor: default;
	}
	
	.bidComments {
		min-width: 200px;
	}
	
	.resultsTable thead td {
		padding: 7px 10px;
	}

	.sortAble, #nextPageBtn, #prevPageBtn {
		cursor: pointer;
	}
	
	.sortAble a, .sortAbleSelected a, .nonSortAble a {
		text-decoration: none;
		color: #fff;
	}
	
	.sortAble a:hover, .sortAbleSelected a:hover {
		text-decoration: underline;
		color: #00f;
	}

	.sortAbleSelected {
		cursor: pointer;
		background-color: #FF9900;
		color: #FFFFFF;
		font-weight: bold;
	}

	.resultsTable tfoot {
		text-align: center;
		color: #000;
		border: 1px solid #2A497D;
		background-color: #5091CB;
	}

	.resultsTable tfoot td {
		padding: 7px 10px;
	}
	
	.resultsTable td {
		padding: 3px 5px;
	}

	.resultsTable .evenRow {
		background-color: #E9EEF8;
	}

	.resultsTable .oddRow {
		background-color: #FBFCFD;
	}

	.resultsTable .preferred {
		background-color: #FFFF8C;
	}

	.searchControls {
		margin-top: 10px;
		text-align: center;
	}

	.noResultsDiv {
		margin-top: 10px;
		text-align: center;
	}
	
	.agreedTechPayBox {
		text-align: right;
	}
</style>

<script type="text/javascript">
	var qs = "<?=$qs?>";
	var myName = "<?php echo $_SERVER['PHP_SELF']?>";
	var currentResultPage = "<?=$currentResultPage?>";
	var currSort = "<?=$sort?>";
	var currOrder = "<?=$order?>";
	var closeTimeout = null;
	var closeOK = false;
	
	function checkCloseOK() {
		if (closeOK) {
			window.close();
		}
		else
			queueCheckCloseOK();
	}
	
	function clearCheckCloseOK() {
		if (closeTimeout != null) {
			try {
				clearTimeout(closeTimeout);
			} catch (e) {}
		}
		closeTimeout = null;
	}
	
	function queueCheckCloseOK() {
		clearCheckCloseOK();
		closeTimeout = setTimeout("checkCloseOK()", 500);
	}
	
	try {
		currentResultPage = parseInt(currentResultPage, 10);
	}
	catch (e) {}


	function updateSearchUrl() {
		setting = (currSort != "" ? "sort=" + currSort + "&order=" + currOrder : "");
		return myName + "?" + setting + "&" + qs;
	}

	function updateSearch() {
		frm = document.getElementById("searchForm");
		hideBanned = document.getElementById("hideBanned").checked;
		preferredOnly = document.getElementById("preferredOnly").checked;
		if (!hideBanned  || !preferredOnly) {
			settings = (hideBanned ? "" : "&sb=1");
			settings = settings + (preferredOnly ? "" : "&snp=1");
			frm.action = updateSearchUrl() + settings;
		}
		else
			frm.action = updateSearchUrl();
	}


	function sortBy(column) {
		if (column == "<?=$sort?>")
			order = (<?=$order?> == 0 ? 1 : 0);
		else
			order = 0;
		document.location.href = myName = "?sort=" + column + "&order=" + order + "&" + qs;
	}

	function gotoPage(num) {
		document.location.href = updateSearchUrl() + "&page=" + num;
	}

	function prevPage() {
		gotoPage(currentResultPage - 1);
	}

	function nextPage() {
		gotoPage(currentResultPage + 1);
	}



	function today() {
		var today = new Date();
		var tMth = today.getMonth();
		tMth++;
		return tMth+"/"+today.getDate()+"/"+today.getFullYear() + " " + today.getHours() + ":" + leadingZero(today.getMinutes()) + ":" + leadingZero(today.getSeconds());
	}

	String.prototype.trim = function() {
		a = this.replace(/^\s+/, '');
		return a.replace(/\s+$/, '');
	};

	function preCallField() {
		return $("input#EditRecordTechCalled");
	}

	function calledByField() {
		return $("input#EditRecordCalledBy");
	}

	function calledByDateTimeField() {
		return $("input#EditRecordDateTimeCalled");
	}

	function preCall2Field() {
		return $("input#EditRecordPreCall_2");
	}

	function calledBy2Field() {
		return $("input#EditRecordCalledBy_2");
	}

	function calledBy2DateTimeField() {
		return $("input#EditRecordDateTimeCalled_2");
	}


	function usernameField() {
		return "<?=$_SESSION["UserName"]?>";
	}

	function columnNumber(col) {
		return $("tr td:nth-child(" + col + ")");
	}

	function previous(source, count) {
		var result = source;
		for (i=0; i < count; i++) {
			result = result.prev();
		}
		return result;
	}

	function next(source, count) {
		var result = source;
		for (i=0; i < count; i++) {
			result = result.next();
		}
		return result;
	}

	function WorkOrderIDColumn() {
		return previous(PreCallColumn_1(), 9);
	}

	function PreCallColumn_1() {
		return columnNumber(10);
	}

	function PreCallColumn_2() {
		return columnNumber(11);
	}

	function openPopup(url,w,h) {
		w = w || 350;
		h = h || 200;
		newwindow=window.open(url,'precall','height='+h+', width='+w+', left=100, top=100, resizable=yes, scrollbars=yes, toolbar=no,status=no');
		w=0;h=0;
		if (window.focus) {newwindow.focus()}
	} 

	function updateCallResult(call, row, result, raw) {
		var aTag = $((call == "1" ? "#a" : "#b") + row);
		var rowTxt = "";
		switch (result) {
			case "Confirmed":
//				aTag = aTag.parent();
				rowTxt = "Yes";
				break;
			case "Unconfirmed":
				rowTxt = "Pending&nbsp;Call&nbsp;" + call;
				break;
			case "Backed Out":
				if (raw) rowTxt = raw;
				break;
			case "Reset":
				rowTxt = "Call " + call;
				break;
		}
		if (rowTxt == "") return;
		aTag.html(rowTxt);
	}

	var tableRows = <?=json_encode($searchResult)?>;

	$('document').ready(function() {
		WorkOrderIDColumn().each(function(i) {
			if (i == 0) return;
			var techID = next($(this),6).text().trim();
			var techName = next($(this),7).text().trim().replace("'", "\\'") + " " + next($(this),8).text().trim().replace("'", "\\'");
			var projectName = next($(this),4).text().trim().replace("'", "\\'");

			var PreCall_1 = next($(this),10);
			var PreCall_2 = next($(this),11);
			var WO_ID = $(this).text().trim();

//			var status_1 = next($(this), 22).text();
//			var status_2 = next($(this), 23).text();
			var status_1 = tableRows[i - 1]['PreCall_1_Status'];
			var status_2 = tableRows[i - 1]['PreCall_2_Status'];

			var c1Txt = '';
			if (PreCall_1.text() == "No") {
				c1Txt = 'Call 1';
			}
			else if (PreCall_1.text() == "Yes" && status_1 == "Unconfirmed") {
				c1Txt = 'Pending&nbsp;Call&nbsp;1';
			}
			else {
				c1Txt = PreCall_1.text();
			}
			PreCall_1.html("<a id='a" + i +"' href=\"javascript:openPopup('techCalledPopup.php?row=" + i + "&techID=" + techID + "&techName=" + techName + "&woID=" + WO_ID + "&project=" + projectName + "&call=1')\">" + c1Txt + "</a>");

			var c2Txt = '';
			if (PreCall_2.text() == "No") {
				c2Txt = 'Call 2';
			}
			else if (PreCall_2.text() == "Yes" && status_2 == "Unconfirmed") {
				c2Txt = 'Pending&nbsp;Call&nbsp;2';
			}
			else {
				c2Txt = PreCall_2.text();
			}
			PreCall_2.html("<a id='b" + i +"' href=\"javascript:openPopup('techCalledPopup.php?row=" + i + "&techID=" + techID + "&techName=" + techName + "&woID=" + WO_ID + "&project=" + projectName + "&call=2')\">"+ c2Txt +"</a>");
		});

		preCallField().change(function() {
			if (preCallField().attr("checked") == undefined) {
				calledByField().attr("value","");
				calledByDateTimeField().attr("value","");
			} else {
				calledByField().attr("value",usernameField());
				calledByDateTimeField().attr("value",today());
			}
		});
	
		preCall2Field().change(function() {
			if (preCall2Field().attr("checked") == undefined) {
				calledBy2Field().attr("value","");
				calledBy2DateTimeField().attr("value","");
			} else {
				calledBy2Field().attr("value",usernameField());
				calledBy2DateTimeField().attr("value",today());
			}
		});
                
                
                var stringSort="<DIV class=popup-body>";
                stringSort+="<TABLE class=sort_table cellSpacing=0 cellPadding=0 width=\"100%\">";
                stringSort+="<COLGROUP>";
                stringSort+="<COL width=\"50%\">";
                stringSort+="<COL width=\"50%\">";
                stringSort+="<TBODY>";
                stringSort+="<TR><TD colSpan=2>Sort by</TD></TR>";
                stringSort+="<TR class=even><TD>";
                stringSort+="<SELECT id=\"cbxSort1\"  name=\"cbxSort1\">";
                stringSort+="<OPTION value='' checked=\"checked\">None</OPTION>";
                stringSort+="<OPTION value=\"WIN_NUM\">WIN #</OPTION>";
		stringSort+="<OPTION value=\"WO_ID\">Client Work Order #</OPTION>";
		stringSort+="<OPTION value=\"StartDate\">Start Date</OPTION>";
                //858
		stringSort+="<OPTION value=\"StartTimesort\">StartTime</OPTION>";//
                //end 858
		stringSort+="<OPTION value=\"Project_Name\">Project Name</OPTION>";
		stringSort+="<OPTION value=\"FLSID\">FLS ID</OPTION>";
                //14156
                stringSort+="<OPTION value=\"ISO_Affiliation_ID\">ISO Tech</OPTION>";
		//stringSort+="<OPTION value=\"Tech_ID\">FS Tech ID</OPTION>";
                //End 14156
		stringSort+="<OPTION value=\"Firstname\">WO Tech FName</OPTION>";
		stringSort+="<OPTION value=\"Lastname\">WO Tech LName</OPTION>";
		stringSort+="<OPTION value=\"PrimaryPhone\">Tech PrimaryPhone</OPTION>";
		stringSort+="<OPTION value=\"PreCall_1\">PreCall-1</OPTION>";
		stringSort+="<OPTION value=\"PreCall_2\">PreCall-2</OPTION>";
		stringSort+="<OPTION value=\"SiteName\">Site Name</OPTION>";
		stringSort+="<OPTION value=\"Address\">Site Address</OPTION>";
                stringSort+="<OPTION value=\"City\">City </OPTION>";
                stringSort+="<OPTION value=\"State\">State</OPTION>";
                stringSort+="<OPTION value=\"Zipcode\">Zipcode</OPTION>";
                stringSort+="<OPTION value=\"Lead\">Lead</OPTION>";
                stringSort+="<OPTION value=\"Assist\">Assist</OPTION>";
                stringSort+="<OPTION value=\"Bg_Test_Pass_Lite\">Bkgd. Check</OPTION>";
                //14156
                //stringSort+="<OPTION value=\"FLSCSP_Rec\">FLS CSP</OPTION>";
                //end 14156
		stringSort+="</SELECT>";
                stringSort+="</TD><TD>";
		stringSort+="<INPUT id=\"radSortAscending1\"  name=\"radSortAscendingDescending1\" value=\"asc\" type=radio checked=checked> Ascending<BR>";
		stringSort+="<INPUT id=\"radSortDescending1\" name=\"radSortAscendingDescending1\" value=\"desc\" type=radio> Descending";
                stringSort+="</TD>";
                stringSort+="</TR><TR><TD colSpan=2>Then by</TD></TR><TR class=even><TD>";
		stringSort+="<SELECT id=\"cbxSort2\"  name=\"cbxSort2\">";
		stringSort+="<OPTION value='' checked=\"checked\">None</OPTION>";
		stringSort+="<OPTION value=\"WIN_NUM\">WIN #</OPTION>";
		stringSort+="<OPTION value=\"WO_ID\">Client Work Order #</OPTION>";
		stringSort+="<OPTION value=\"StartDate\">Start Date</OPTION>";
                //858
		stringSort+="<OPTION value=\"StartTimesort\">StartTime</OPTION>";
                //end 858
		stringSort+="<OPTION value=\"Project_Name\">Project Name</OPTION>";
		stringSort+="<OPTION value=\"FLSID\">FLS ID</OPTION>";
                //14156
                stringSort+="<OPTION value=\"ISO_Affiliation_ID\">ISO Tech</OPTION>";
		//stringSort+="<OPTION value=\"Tech_ID\">FS Tech ID</OPTION>";
                //end 14156
		stringSort+="<OPTION value=\"Firstname\">WO Tech FName</OPTION>";
		stringSort+="<OPTION value=\"Lastname\">WO Tech LName</OPTION>";
		stringSort+="<OPTION value=\"PrimaryPhone\">Tech PrimaryPhone</OPTION>";
		stringSort+="<OPTION value=\"PreCall_1\">PreCall-1</OPTION>";
		stringSort+="<OPTION value=\"PreCall_2\">PreCall-2</OPTION>";
		stringSort+="<OPTION value=\"SiteName\">Site Name</OPTION>";
		stringSort+="<OPTION value=\"Address\">Site Address</OPTION>";
                stringSort+="<OPTION value=\"City\">City </OPTION>";
                stringSort+="<OPTION value=\"State\">State</OPTION>";
                stringSort+="<OPTION value=\"Zipcode\">Zipcode</OPTION>";
                stringSort+="<OPTION value=\"Lead\">Lead</OPTION>";
                stringSort+="<OPTION value=\"Assist\">Assist</OPTION>";
                stringSort+="<OPTION value=\"Bg_Test_Pass_Lite\">Bkgd. Check</OPTION>";
                //14156
                //stringSort+="<OPTION value=\"FLSCSP_Rec\">FLS CSP</OPTION>";
                //end 14156
		stringSort+="</SELECT>";
                stringSort+="</TD><TD>";
		stringSort+="<INPUT id=\"radSortAscending2\"  name=\"radSortAscendingDescending2\" value=asc type=radio checked=checked> Ascending<BR>";
		stringSort+="<INPUT id=\"radSortDescending2\" name=\"radSortAscendingDescending2\" value=desc type=radio> Descending";
                stringSort+="</TD></TR><TR><TD colSpan=2>Then by</TD></TR><TR class=even><TD>";
		stringSort+="<SELECT id=\"cbxSort3\"  name=\"cbxSort3\">";
		stringSort+="<OPTION value='' checked=\"checked\">None</OPTION>";
		stringSort+="<OPTION value=\"WIN_NUM\">WIN #</OPTION>";
		stringSort+="<OPTION value=\"WO_ID\">Client Work Order #</OPTION>";
		stringSort+="<OPTION value=\"StartDate\">Start Date</OPTION>";
                //858
		stringSort+="<OPTION value=\"StartTimesort\">StartTime</OPTION>";
                //end 858
		stringSort+="<OPTION value=\"Project_Name\">Project Name</OPTION>";
		stringSort+="<OPTION value=\"FLSID\">FLS ID</OPTION>";
                //14156
                stringSort+="<OPTION value=\"ISO_Affiliation_ID\">ISO Tech</OPTION>";
		//stringSort+="<OPTION value=\"Tech_ID\">FS Tech ID</OPTION>";
                //end 14156
		stringSort+="<OPTION value=\"Firstname\">WO Tech FName</OPTION>";
		stringSort+="<OPTION value=\"Lastname\">WO Tech LName</OPTION>";
		stringSort+="<OPTION value=\"PrimaryPhone\">Tech PrimaryPhone</OPTION>";
		stringSort+="<OPTION value=\"PreCall_1\">PreCall-1</OPTION>";
		stringSort+="<OPTION value=\"PreCall_2\">PreCall-2</OPTION>";
		stringSort+="<OPTION value=\"SiteName\">Site Name</OPTION>";
		stringSort+="<OPTION value=\"Address\">Site Address</OPTION>";
                stringSort+="<OPTION value=\"City\">City </OPTION>";
                stringSort+="<OPTION value=\"State\">State</OPTION>";
                stringSort+="<OPTION value=\"Zipcode\">Zipcode</OPTION>";
                stringSort+="<OPTION value=\"Lead\">Lead</OPTION>";
                stringSort+="<OPTION value=\"Assist\">Assist</OPTION>";
                stringSort+="<OPTION value=\"Bg_Test_Pass_Lite\">Bkgd. Check</OPTION>";
                //14156
                //stringSort+="<OPTION value=\"FLSCSP_Rec\">FLS CSP</OPTION>";
                //end 14156
		stringSort+="</SELECT>";
                stringSort+="</TD><TD>";
		stringSort+="<INPUT id=\"radSortAscending3\"  name=\"radSortAscendingDescending3\" value=asc type=radio checked=checked> Ascending<BR>";
		stringSort+="<INPUT id=\"radSortDescending3\" name=\"radSortAscendingDescending3\" value=desc type=radio> Descending";
                stringSort+="</TD></TR><TR>";
                stringSort+="<TD class=submit_buttons colSpan=2 align=center>";
		stringSort+="<INPUT class=\"link_button_popup\" id=\"cmdOkPopup\" value=Ok type=button>";
		stringSort+="<INPUT class=\"link_button_popup\" id=\"cmdCancelPopup\" value=Cancel type=button>";
                stringSort+="<INPUT class=\"link_button_popup\" id=\"hidSort1\" type=hidden>";
                stringSort+="<INPUT class=\"link_button_popup\" id=\"hidSort2\" type=hidden>";
                stringSort+="<INPUT class=\"link_button_popup\" id=\"hidSort3\" type=hidden>";
                stringSort+="</TD></TR></TBODY></TABLE></DIV>";
                
                jQuery.extend({
                    parseQuerystring: function(){
                            var nvpair = {};
                            var qs = window.location.search.replace('?', '');
                            var pairs = qs.split('&');
                            $.each(pairs, function(i, v){
                                    var pair = v.split('=');
                                    nvpair[pair[0]] = pair[1];
                            });
                            return nvpair;
                    }
                });
                function bindEvent(){
                    jQuery("#cmdCancelPopup")
                        .unbind("click",cmdCancelPopup_onClick)
                        .bind("click",cmdCancelPopup_onClick);
                    jQuery("#cmdOkPopup")
                        .unbind("click",cmdOkPopup_onClick)
                        .bind("click",cmdOkPopup_onClick);
                    jQuery("#cbxSort1")
                        .unbind("change",cbxSort1_onChange)
                        .bind("change",cbxSort1_onChange);
                    jQuery("#cbxSort2")
                        .unbind("change",cbxSort2_onChange)
                        .bind("change",cbxSort2_onChange);
                    jQuery("#cbxSort3")
                        .unbind("change",cbxSort3_onChange)
                        .bind("change",cbxSort3_onChange);
                }
                function cmdOkPopup_onClick(){
                   var qs = jQuery.parseQuerystring();
                   if(jQuery("#Sort1 option:selected").val()!="")
                   {
                        qs["Sort1"]=jQuery("#cbxSort1 option:selected").val();
                        qs["direct1"]=jQuery("#radSortAscending1").is(":checked")?"asc":"desc";
                   }
                   else
                   {
                        qs["Sort1"]="";
                        qs["direct1"]="";
                   }                       
                       
                   if(jQuery("#cbxSort2 option:selected").val()!="")
                   {
                        qs["Sort2"]=jQuery("#cbxSort2 option:selected").val();
                        qs["direct2"]=jQuery("#radSortAscending2").is(":checked")?"asc":"desc";
                   }
                   else
                   {
                        qs["Sort2"]="";
                        qs["direct2"]="";
                   }
                   if(jQuery("#cbxSort3 option:selected").val()!="")
                   {
                        qs["Sort3"]=jQuery("#cbxSort3 option:selected").val();
                        qs["direct3"]=jQuery("#radSortAscending3").is(":checked")?"asc":"desc";
                   }
                   else
                   {
                        qs["Sort3"]="";
                        qs["direct3"]="";
                   }
                   if(qs['date_from']!="")                       
                        qs['date_from']=decodeURIComponent(qs['date_from']);
                   if(qs['date_to']!="")
                        qs['date_to']=decodeURIComponent(qs['date_to']);
                   if(qs['firstname']!="")
                        qs['firstname']=decodeURIComponent(qs['firstname']);
                   if(qs['lastname']!="")
                        qs['lastname']=decodeURIComponent(qs['lastname']);
                   if(['pc1']!="")
                        qs['pc1']=decodeURIComponent(qs['pc1']);
                   if(qs['pc2']!="")
                        qs['pc2']=decodeURIComponent(qs['pc2']);
                   if(qs['checkedin']!="")
                        qs['checkedin']=decodeURIComponent(qs['checkedin']);
                   if(qs['search']!="")
                        qs['search']=decodeURIComponent(qs['search']);
                   if(qs['tech']!="")
                        qs['tech']=decodeURIComponent(qs['tech']);                  
                   
                   var url = window.location.pathname;    
                   var myPageName = url.substring(url.lastIndexOf('/') + 1);
                   
                   //alert(window.location.pathname);
                   //alert(window.location.hostname);
                   //alert(window.location.protocol);                   
                   roll.hide();
                   var  params= jQuery.param(qs);                           
                   window.location=myPageName+"?"+params;
                }
                function cmdCancelPopup_onClick(){
                    roll.hide();
                }
                
                function cbxSort1_onChange(){
                    var value=jQuery(this).val();
                    var beforeSort1= jQuery("#hidSort1").val();                    
                    jQuery("#cbxSort2 option[value="+beforeSort1+"]").removeAttr("disabled");
                    jQuery("#cbxSort3 option[value="+beforeSort1+"]").removeAttr("disabled");
                    
                    if(beforeSort1!="")
                    {
                        jQuery("#cbxSort2 option[value="+value+"]").attr("disabled","disabled");
                        jQuery("#cbxSort3 option[value="+value+"]").attr("disabled","disabled");
                    }
                    jQuery("#hidSort1").val(value);
                    
                }
                function cbxSort2_onChange(){
                    var value=jQuery(this).val();
                    var beforeSort2= jQuery("#hidSort2").val();
                    
                    jQuery("#cbxSort1 option[value="+beforeSort2+"]").removeAttr("disabled");
                    jQuery("#cbxSort3 option[value="+beforeSort2+"]").removeAttr("disabled");
                     
                    if(value!="")
                    {
                        jQuery("#cbxSort1 option[value="+value+"]").attr("disabled","disabled");
                        jQuery("#cbxSort3 option[value="+value+"]").attr("disabled","disabled");
                    }
                    
                    jQuery("#hidSort2").val(value);
                }
                function cbxSort3_onChange(){
                    var value=jQuery(this).val();                   
                    var beforeSort3= jQuery("#hidSort3").val();
                   
                    jQuery("#cbxSort1 option[value=\""+beforeSort3+"\"]").removeAttr("disabled");
                    jQuery("#cbxSort2 option[value=\""+beforeSort3+"\"]").removeAttr("disabled");
                    
                    if(value!="")
                    {
                        jQuery("#cbxSort1 option[value=\""+value+"\"]").attr("disabled","disabled");
                        jQuery("#cbxSort2 option[value=\""+value+"\"]").attr("disabled","disabled");
                    }
                    
                    jQuery("#hidSort3").val(value);
                }
                function InitSort(){
                    var qs = jQuery.parseQuerystring();    
                    var  Valuesort1="";                    
                    if(typeof(qs["Sort1"])=="undefined"||qs["Sort1"]=="")
                    {
                        Valuesort1="WIN_NUM";                           
                    }
                    else
                    {
                        Valuesort1=qs["Sort1"];
                    }                   
                   
                    if(Valuesort1!="")
                    {                        
                        if(qs["direct1"]==""||qs["direct1"]==null||qs["direct1"].toUpperCase()=="asc".toUpperCase())
                        {
                            jQuery("#radSortAscending1").attr("checked","checked");
                            jQuery("#radSortDescending1").removeAttr("checked");
                        }
                        else
                        {
                            jQuery("#radSortDescending1").attr("checked","checked");
                            jQuery("#radSortAscending1").removeAttr("checked");
                        }
                        jQuery("#cbxSort2 option[value=\""+Valuesort1+"\"]").attr("disabled","disabled");
                        jQuery("#cbxSort3 option[value=\""+Valuesort1+"\"]").attr("disabled","disabled");
                        
                        jQuery("#cbxSort2 option[value=\"\"]").attr("checked","checked");
                        jQuery("#cbxSort3 option[value=\"\"]").attr("checked","checked");
                        
                        jQuery("#cbxSort1 option:selected").removeAttr("selected");
                        jQuery("#cbxSort1 option[value=\""+Valuesort1+"\"]").attr("selected","selected");
                        jQuery("#hidSort1").val(Valuesort1);
                    }
                    else
                    {
                        jQuery("#cbxSort1 option[value=\"\"]").attr("checked","checked");                        
                    }
                    if(qs["Sort2"]!="")
                    {                        
                        if(qs["direct2"]==""||qs["direct2"]==null||qs["direct2"].toUpperCase()=="asc".toUpperCase())
                        {
                            jQuery("#radSortAscending2").attr("checked","checked");
                            jQuery("#radSortDescending2").removeAttr("checked");
                        }
                        else
                        {
                            jQuery("#radSortDescending2").attr("checked","checked");
                            jQuery("#radSortAscending2").removeAttr("checked");
                        }
                        jQuery("#cbxSort1 option[value="+qs["Sort2"]+"]").attr("disabled","disabled");
                        jQuery("#cbxSort3 option[value="+qs["Sort2"]+"]").attr("disabled","disabled");  
                        
                        jQuery("#cbxSort2 option:selected").removeAttr("selected");
                        jQuery("#cbxSort2 option[value="+qs["Sort2"]+"]").attr("selected","selected");
                        jQuery("#hidSort2").val(qs["Sort2"]);
                    }
                    else
                    {
                        jQuery("#cbxSort2 option[value=\"\"]").attr("checked","checked");                        
                    }
                    if(qs["Sort3"]!="")
                    {                        
                        if(qs["direct3"]==""||qs["direct3"]==null||qs["direct3"].toUpperCase()=="asc".toUpperCase())
                        {
                            jQuery("#radSortAscending3").attr("checked","checked");
                            jQuery("#radSortDescending3").removeAttr("checked");
                        }
                        else
                        {
                            jQuery("#radSortDescending3").attr("checked","checked");
                            jQuery("#radSortAscending3").removeAttr("checked");
                        }
                        jQuery("#cbxSort1 option[value="+qs["Sort3"]+"]").attr("disabled","disabled");
                        jQuery("#cbxSort2 option[value="+qs["Sort3"]+"]").attr("disabled","disabled");
                        
                        jQuery("#cbxSort3 option:selected").removeAttr("selected");
                        jQuery("#cbxSort3 option[value="+qs["Sort3"]+"]").attr("selected","selected");
                        jQuery("#hidSort3").val(qs["Sort3"]);
                    }
                     else
                    {
                        jQuery("#cbxSort3 option[value=\"\"]").attr("checked","checked");                        
                    }
                    bindEvent();
                }
                wd=new FSWidgetDashboard({tab:'ReportView'});
                wd.setParams(wd.parseUrl());               
                roll = new FSPopupRoll();         
                $("#sortButton").click(function(event){                    
                    roll.autohide(false);
                    var opt = {
                        width   : 325,
                        height  : 225,
                        title   : 'Sort Work Orders...',                        
                        body    : stringSort
                    };
                    roll.showNotAjax(this,event,opt);
                    InitSort();
                    
                    
                        
                 });
                    
	});

</script>

<?php
	$contactTxt = "";
	if (!empty($_GET["contact"])) {
		$contact = $_GET["contact"];
		
		$result = $db->fetchAll("SELECT Project_Manager, Project_Manager_Phone, Project_Manager_Email, WO_ID FROM work_orders AS w LEFT JOIN projects AS p ON p.Project_ID = w.Project_ID WHERE " . $db->quoteInto("Company_ID = ?", $_GET["v"]) . " AND " . $db->quoteInto("WIN_NUM = ?", $contact));

		if (sizeof($result) > 0) {
			$result = $result[0];
			$contactTxt = "WIN # $contact / Client Work Order # " . $result["WO_ID"] . "&nbsp;Contact Info&nbsp;-&nbsp;Name:&nbsp;" . $result["Project_Manager"] . "&nbsp;Phone:&nbsp;" . $result["Project_Manager_Phone"] . "&nbsp;Email:&nbsp;" . $result["Project_Manager_Email"];
		}
	}
?>
<div align="center" id="divViewForm">
    <h1>FAI Tech Review & Confirm Report</h1>
	<?php if ($numResult > 0):?>
	<div align="left">
            <!--Start 13381-->
            <div class="toolBtn" style="float:left;padding-left: 3px; ">
                <input id="sortButton" type="button" value="Sort" class="link_button" />
            </div>
            <!-- end 13381-->
            <div style="float:left;padding-right:6px;">
                <input class="link_button download_button" value="Download" type="button" onClick="document.location='http://<?=$_SERVER["SERVER_NAME"]?><?=$_SERVER["PHP_SELF"]?>?<?=$_SERVER["QUERY_STRING"]?>&download=1'"/>
            </div>
             <div class="toolBtn" style="float:left;padding-left: 3px; ">
                <input id="filterButton" type="button" value="Edit Filters" class="link_button"  onClick="document.location='/clients/FLS/FTXS_Tech_ConfirmReport.php?v=<?=$companyID?>&win=<?=$win?>&wo_id=<?=$wo_id?>&project=<?=$project?>&date_from=<?=$date_from?>&date_to=<?=$date_to?>&flsid=<?=$flsid?>&tech=<?=$tech?>&firstname=<?=$firstname?>&lastname=<?=$lastname?>&pc1=<?=$pc1?>&pc2=<?=$pc2?>&checkedin=<?=$checkedin?>';" />
            </div>
            <div style="clear:both;"></div>
	</div>
        
	<?php endif;?>
	<br/>
	<div style="text-align:center"><?=$contactTxt?></div>
	<div>
	<?=$resultsHtml?>
	</div>
</div>


<?php require("../../footer.php");?>
<?php endif;?>
<?php
endif;
?>

