<?php
	set_time_limit(1600);

session_cache_limiter('public');
require_once ("../../headerStartSession.php");
$csv_filename = "FLSTechRegisteredTrained_" . session_id() . ".csv";

if (isset($_GET["download"])) {
	// download attachment
	header("Content-disposition: attachment; filename=$csv_filename");
	echo file_get_contents("/tmp/$csv_filename");
	die();
	}
?>
<?php
// Created on: 1-23-08 
// Author: CM
// Description: Display Techs who are trained and registered


$page = clients; 
$option = reports; 

?>
<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>
<!--?php
	$displayPMTools = true;
	require_once("includes/PMCheck.php");
?-->

<link rel="stylesheet" href="../../css/tablesorter.css" type="text/css" />
<script type="text/javascript" src="../../library/jquery/jquery-1.2.1.pack.js"></script>
<script type="text/JavaScript" src="../../library/jquery/tablesorter/jquery.tablesorter.pack.js"></script>

<!--script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script> <script type="text/javascript" language="javascript">try{f_cbload("193b0000d4j8c9d3a7g5f3j5g9a5","https:");}catch(li){;}</script> <div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000d4j8c9d3a7g5f3j5g9a5">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div-->

<!--script type="text/javascript">
	try {
		var test = adminID;
	}
	catch (e) {
		window.location.replace("./");
	}
</script-->
<!-- Add Content Here -->
 
<br /><br />

<div align="center">

<h2>Techs Trained and Registered</h2>
<br />

<table cellpadding="0" cellspacing="0" border="0" style="border:solid thin #2a497d">
	<tr>
<td>
<div align="right">
<a href="../../clients/FLS/TechTrainedRegistered.php?download=1">
<img src="../../images/downloadIcon.gif" /></a>
</div>
<table id="wos" class="tablesorter" cellspacing="0" cellpadding="0">             
<thead>
    <tr>
<th><nobr>Tech ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</nobr></th>
<th><nobr>Last Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</nobr></th>
<th><nobr>First Name&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</nobr></th>
<th><nobr>FLS ID&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</nobr></th>
<th><nobr>FLS Whse&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</nobr></th>
<th><nobr>First WO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</nobr></th>
<th><nobr>Last WO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</nobr></th>
<th><nobr>Date Trained&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</nobr></th>
	</tr>
</thead> 
<tbody> 
<?php

try {
require_once("../../library/caspioAPI.php");
require_once("../../library/smtpMail.php");

$result = "Success"; //Value required by our library
$calledFrom = (empty($_SERVER['HTTP_REFERER']) ? "Unknown" : $_SERVER['HTTP_REFERER']);// library needs this

// Get Tech Contact info
//CONVERT(VARCHAR(8), GETDATE(), 1) AS [MM/DD/YY]
$FirstWO = "SELECT Top 1 ISNULL(CONVERT(VARCHAR, StartDate, 101), '') FROM Work_Orders WHERE Work_Orders.FLS_ID = TR_master_List.FLSID AND Work_Orders.Company_ID = 'FLS' Order By StartDate ASC";

$LastWO = "SELECT Top 1 ISNULL(CONVERT(VARCHAR, StartDate, 101), '') FROM Work_Orders WHERE Work_Orders.FLS_ID = TR_master_List.FLSID AND Work_Orders.Company_ID = 'FLS' Order By StartDate DESC";

$TrainedDate = "(SELECT date_taken FROM FLS_Test WHERE FLS_Test.tech_id = " . Core_Database::TABLE_TECH_BANK_INFO . ".TechID Order By date_taken DESC LIMIT 1)";

$grade = "(SELECT score FROM FLS_Test WHERE FLS_Test.tech_id = " . Core_Database::TABLE_TECH_BANK_INFO . ".TechID Order By score LIMIT 1)";

//$records = caspioSelectAdv("TR_Master_List", "TechID, FirstName, LastName, FLSID, FLSWhse, '', '', ($TrainedDate), ($grade)", "FLSID != ' ' AND (FLSstatus='Trained' OR FLSstatus='Registered')", "TechID", false, "", "|");

$db = Zend_Registry::get("DB");
$select = $db->select();
$select->from(Core_Database::TABLE_TECH_BANK_INFO, array('TechID', 'FirstName', 'LastName', 'FLSID', 'FLSWhse', new Zend_Db_Expr("''"), new Zend_Db_Expr("''"), new Zend_Db_Expr($TrainedDate), new Zend_Db_Expr($grade)))
	->where("(FLSstatus='Trained' OR FLSstatus='Registered') AND TRIM(FLSID) != ''");
	
$records = $db->fetchAll($select, array(), Zend_Db::FETCH_NUM);

// Counts # of records returned
$count = sizeof($records);

$techMysqlDataMap = array();
foreach ($records as &$order) {
//	$order = str_replace("'","",$order);
//	$order = explode("|",$order);
	$techMysqlDataMap[$order[0]] = NULL;
}

//echo "SELECT Tech_ID, MAX(StartDate) AS lastWO, MIN(StartDate) AS firstWO FROM work_orders WHERE " . $db->quoteInto("Tech_ID IN (?)", array_keys($techMysqlDataMap)) . " GROUP BY Tech_ID";die();
$mysqlData = $db->fetchAll("SELECT Tech_ID, MAX(StartDate) AS lastWO, MIN(StartDate) AS firstWO FROM work_orders WHERE Company_ID = 'FLS' AND " . $db->quoteInto("Tech_ID IN (?)", array_keys($techMysqlDataMap)) . " GROUP BY Tech_ID");

foreach ($mysqlData as $data) {
	$techMysqlDataMap[$data["Tech_ID"]] = array("lastWO" => $data["lastWO"], "firstWO" => $data["firstWO"]);
}

// Populate array with Tech info
foreach ($records as $order) {
//	$order = str_replace("'","",$order);
//	$fields = explode("|", $order);
	$fields = $order;
	//$fields = getFields($order);

	$techID = $fields[0];
	$FirstName = $fields[1];
	$LastName = $fields[2];
	$FLSID = $fields[3];
	$FLSWhse = $fields[4];
//	$firstWO = $fields[5];
	$firstWO = $techMysqlDataMap[$techID]["firstWO"];
	$firstWO = date("m/d/Y", strtotime($firstWO));
	if($firstWO=="12/31/1969"){$firstWO="none";}
	
//	$lastWO = $fields[6];
	$lastWO = $techMysqlDataMap[$techID]["lastWO"];
	$lastWO = date("m/d/Y", strtotime($lastWO));
	if($lastWO=="12/31/1969"){$lastWO="none";}

	$getTrainedDate = $fields[7];
	if (!empty($getTrainedDate)) {
		$getTrainedDate = new Zend_Date($getTrainedDate, Zend_Date::ISO_8601);
		$getTrainedDate = $getTrainedDate->toString('MM/dd/yyyy');
	}
	$grade = $fields[8];

//	$getTrainedDate = date("m/d/Y", strtotime($getTrainedDate));
	if($getTrainedDate=="12/31/1969"){$getTrainedDate="01/01/2007";}
	if($grade < "92"){$getTrainedDate="01/01/2007";}
	
echo "<tr class='wosRow'>";
echo "<td>$techID</td>";
echo "<td>$FirstName</td>";
echo "<td>$LastName</td>";
echo "<td>$FLSID</td>";
echo "<td>$FLSWhse</td>";
echo "<td>$firstWO</td>";
echo "<td>$lastWO</td>";
echo "<td>$getTrainedDate</td>";
echo "</tr>";

}


echo "<tfoot>";
echo "<th colspan='8' align='right'>&nbsp;</th>";
echo "</tfoot>";
echo "</tbody>";
echo "</table>";
echo "</td></tr>";
echo "<tr><td colspan='8'>Tech Count: $count</tr>";
echo "</table>";
echo "</div>";


$columns = array("TechID", "FirstName", "LastName", "FLSID", "FLSWhse", "FirstWO", "LastWO", "DateTrained");
	$fp = fopen("/tmp/$csv_filename", 'w');
 
 fputcsv($fp, $columns);
 
 foreach ($records as $wo) {
//	 $wo = str_replace("'","",$wo);
//	 $fields = explode("|", $wo);
	
	$fields = $wo;
 
 	$techID = $fields[0];
	$FirstName = $fields[1];
	$LastName = $fields[2];
	$FLSID = $fields[3];
	$FLSWhse = $fields[4];
	$firstWO = $techMysqlDataMap[$techID]["firstWO"];
	$firstWO = date("m/d/Y", strtotime($firstWO));
	if($firstWO=="12/31/1969"){$firstWO="none";}
	
	$lastWO = $techMysqlDataMap[$techID]["lastWO"];
	$lastWO = date("m/d/Y", strtotime($lastWO));
	if($lastWO=="12/31/1969"){$lastWO="none";}

	$getTrainedDate = $fields[7];
	$grade = $fields[8];

	$getTrainedDate = date("m/d/Y", strtotime($getTrainedDate));
	if($getTrainedDate=="12/31/1969"){$getTrainedDate="01/01/2007";}
	if($grade < "92"){$getTrainedDate="01/01/2007";}
	
$formattedFields = array($techID,$FirstName,$LastName,$FLSID,$FLSWhse,$firstWO,$lastWO,$getTrainedDate);
  fputcsv($fp, $formattedFields);
  }
  
fclose($fp);


}catch (SoapFault $fault){
//smtpMail("Tech Trained and Registered", "nobody@technicianbureau.com", "collin.mcgarry@technicianbureau.com", "Tech Trained and Registered Script Error", "$fault", "$fault", "TechTrainedRegistered.php");
}

?>

<!--- End Content --->
<?php require ("../../footer.php"); ?><!-- ../ only if in sub-dir -->

