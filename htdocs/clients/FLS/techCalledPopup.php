<?php
	require_once("../../headerSimple.php");
	$row = $_GET["row"];
	$techID = $_GET["techID"];
	$techName = $_GET["techName"];
	$woID = $_GET["woID"];
	$project = $_GET["project"];
	$call = $_GET["call"];
?>
<style type="text/css">
	#callResult {
		margin: 20px 15px;
	}
	
	#callResult ul {
		padding-left: 55px;
		list-style:none;
	}

</style>
	<div id="callResult">
    	Win # <?=$woID?><br/><br/>
    	<ul>
        	<li><input id="confirmDateTime" name="confirmDateTime" type="checkbox" /> Scheduled Date/Time</li>
	    	<li><input id="confirmSiteName" name="confirmSiteName" type="checkbox" /> Site Name</li>
	    	<li><input id="confirmCityState" name="confirmCityState" type="checkbox" /> City/State</li>
	    	<li><input id="confirmDocuments" name="confirmDocuments" type="checkbox" /> Documents</li>
	    	<li><input id="confirmPunctuality" name="confirmPunctuality"  type="checkbox" /> Punctuality</li>
	    	<li><input id="confirmContact" name="confirmContact" type="checkbox" /> Contact</li>
		</ul>
        <br/>
        <div>
            <input id="confirmBtn" name="confirmBtn" type="button" value="Confirmed" onClick="UpdateTechCalled('Confirmed')" />
            <input id="unconfirmBtn" name="unconfirmBtn" type="button" value="Unconfirmed" onClick="UpdateTechCalled('Unconfirmed')" />
            <input id="backedOutBtn" name="backedOutBtn" type="button" value="Backed Out" onClick="UpdateTechCalled('Backed Out')" />
            <input id="resetBtn" name="resetBtn" type="button" value="Reset" onClick="UpdateTechCalled('Reset')" />
            <input id="row" name="row" type="hidden" value="<?=$row?>" />
            <input id="techID" name="techID" type="hidden" value="<?=$techID?>" />
            <input id="techName" name="techName" type="hidden" value="<?=$techName?>" />
            <input id="woID" name="woID" type="hidden" value="<?=$woID?>" />
            <input id="project" name="project" type="hidden" value="<?=$project?>" />
            <input id="call" name="call" type="hidden" value="<?=$call?>" />
		</div>
    </div>
    
<script type="text/javascript">
function UpdateTechCalled(result) {
	var resultChecked = Array();
	var resultTxt = "";
	
	var row = $("#row").val();
	var techID = $("#techID").val();
	var techName = $("#techName").val();
	var woID = $("#woID").val();
	var project = $("#project").val();
	var call = $("#call").val();
	
	if ($("#confirmDateTime").attr("checked")) resultChecked.push("Scheduled Date/Time");
	if ($("#confirmSiteName").attr("checked")) resultChecked.push("Site Name");
	if ($("#confirmCityState").attr("checked")) resultChecked.push("City/State");
	if ($("#confirmDocuments").attr("checked")) resultChecked.push("Documents");
	if ($("#confirmPunctuality").attr("checked")) resultChecked.push("Punctuality");
	if ($("#confirmContact").attr("checked")) resultChecked.push("Contact");
	
	for (i in resultChecked) {
		if (resultTxt != "") resultTxt += ",";
		resultTxt += resultChecked[i];
	}
	
	$.get("techcalledWithResult.php", {"TechID" : techID, "TechName" : techName, "woID" : woID, "Project" : project, "call" : call, "result" : result, "resultTxt" : resultTxt}, 
		function(data) {
			var rowTxt = "";
			switch (result) {
				case "Confirmed":
					rowTxt = "Yes";
					break;
				case "Unconfirmed":
					rowTxt = "Pending";
					break;
				case "Backed Out":
					break;
				case "Reset":
					rowTxt = "Reset";
					break;
			}
			try {
				if (rowTxt != "" || data.indexOf('Error') != -1) {
					opener.updateCallResult(call, row, result, data);
					//$("#a" + row).parent().html(rowTxt);
				}
				else {
					var newLocation = opener.location.href;
					newLocation = newLocation.replace(/&?contact=[^&]*/, "");
					opener.location.href = newLocation + (newLocation.indexOf("?") == -1 ? "?" : "&") + "contact=" + woID;
				}
			} catch (e) {}
			window.close();
		}
	);	
}
</script>