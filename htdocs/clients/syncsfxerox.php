<?php
$page = 'clients';
$option = 'home';
require_once("../headerStartSession.php");
defined("SHORT_AUTH_BYPASS") || define ("SHORT_AUTH_BYPASS", 1); // bypass using sessions to login since this is not a normal site user
if (empty($_SESSION['loggedIn'])) {
	$_SESSION['loggedIn'] = 'yes';
	$_SESSION['loggedInAs'] = 'clients';
}
require_once("../library/caspioAPI.php");

$case = $_GET['case'];
$caseTrimed = ltrim($case, "0");
if (empty($case)) exit;
$sf = new Salesforce_Api_Class;
$api = $sf->connect('alin@fieldsolutions.com', 'Tr1n1ty!', 'FsqSmv0whOmuap807JzHzbJ6');

/*INSERT INTO `clients` (`CompanyName`, `ContactName`, `ContactPhone1`, `ContactPhone2`, `Email1`, `Email2`, `WebsiteURL`, `UserName`, `Password`, `CCType`, `CCNumber`, `CCExp`, `CCCode`, `CCHolderName`, `BusinessAddress`, `BusinessCity`, `BusinessState`, `BusinessZip`, `CCBillingPhone`, `ContractTerms`, `AcceptTerms`, `StaffComments`, `EIN`, `CompanyDescription`, `RegDate`, `BillingType`, `AccountEnabled`, `ClientExpiration`, `AmountPaid`, `SearchZip`, `Company_Logo`, `CustomSignOff`, `Admin`, `Company_ID`, `ShowOnReports`, `PrimaryContact`, `Country`, `ProjectManager`, `CIB`, `PricingRuleID`, `UserType`, `CreatedBy`, `MarkDelete`, `MustChangePassword`, `DateOfLastPasswordChanged`) VALUES
('Xerox Corporation', 'Javier Solis', '(952) 955-9030', '', 'jsolis@fieldsolutions.com', '', '', 'xeroxapi', 'hAtRaR8s', '', '', '', '', '', '', '', '0', '', '', '', 0, '', '', '', '2011-08-31 11:05:10', '', 1, NULL, '', '', '', NULL, 1, 'Xer', 0, 0, '', 1, '', 0, 'Admin', '', 0, 0, '2013-01-24 06:46:16')*/

                $fields = array(
                        'ContactId','CaseNumber','Billing_Mailing_Street__c','Billing_Mailing_State__c','Billing_Mailing_City__c','Billing_Mailing_Zip__c',
                        'XSM_Number__c','Problem_issue_or_question__c','Details_of_any_triage_already_performed__c','SuppliedName','SuppliedEmail','SuppliedPhone',
                        'SuppliedCompany','Manufacturer_Name__c','Model_Name__c','Model_Class__c','Device_SLA__c ','Called_Customer__c','FieldSolutions_WO__c ',
                        'FieldSolutions_WO_Status__c','Tech_ID__c ','FS_Tech_Pay_Offer__c', 'CreatedDate', 'Amount_Per__c', 'Service_Description__c', 'End_Client_Name__c', 'AccountId'
                );

                $fieldsContact = array(
                        'FirstName', 'LastName'
                );
				
				$fieldsEntitlements = array(
						'PCR__c'
				);
				
		$db = Core_Database::getInstance();
                $r = $api->query("SELECT " . implode(",", $fields) . " FROM Case WHERE CaseNumber = " . $db->quoteInto('?', $case));
                $rc = $api->query("SELECT " . implode(",", $fieldsContact) . " FROM Contact WHERE Id = '{$r->records[0]->fields->ContactId}'");

	if ($r->size > 0) {
		$r = $r->records[0]->fields;
		$rc = $rc->records[0]->fields;
		$re = $api->query("SELECT " . implode(",", $fieldsEntitlements) . " FROM Entitlement__c WHERE Unit_Serial_Number__c = '" . $rc->FirstName . "' AND Asset_Tag__c = '" . $rc->LastName . "' AND Customer_Name__c = '{$r->ContactId}'");
		if (!empty($re))
			$re = $re->records[0]->fields;
		else
			$re = false;
	}
	else
		exit;

	$wo = new API_WorkOrder();
//	$wo->Route = $r->CaseNumber;
	$wo->Description = "Device Serial/Asset: {$rc->FirstName} {$rc->LastName}<br/>";
	$wo->Description .= "Problem issue or Question: {$r->Problem_issue_or_question__c}<br/>";
	$wo->Description .= "Details of any triage alread performed: {$r->Details_of_any_triage_already_performed__c}<br/>";
	$wo->Description .= "Manufacturer Name: {$r->Manufacturer_Name__c}<br/>";
	$wo->Description .= "Model Name: {$r->Model_Name__c}<br/>";
	$wo->Description .= "Model Class: {$r->Model_Class__c}<br/>";
	$wo->Description .= "Device SLA: {$r->Device_SLA__c}<br/>";
	$wo->Description .= "Called Customer: " . ($r->Called_Customer__c == "true" ? "Yes" : "No") . "<br/>";
	$wo->Description .= "Service Description:<br/>" . nl2br($r->Service_Description__c);
	$wo->Address = $r->Billing_Mailing_Street__c;
	$wo->State = $r->Billing_Mailing_State__c;
	$wo->City = $r->Billing_Mailing_City__c;
	$wo->Zipcode = $r->Billing_Mailing_Zip__c;
	$wo->WO_ID = $caseTrimed;
	$wo->Site_Contact_Name = $r->SuppliedName;
	$wo->SiteEmail = $r->SuppliedEmail;
	$wo->SitePhone = $r->SuppliedPhone;
	$wo->SiteName = $r->SuppliedCompany;
	if (!empty($re) && $r->End_Client_Name__c == "Bank of New York Mellon") {
		if (strpos($re->PCR__c, "BNYM PCR ") !== FALSE) {
			$re->PCR__c = str_replace("BNYM ", "", $re->PCR__c);
			$r->End_Client_Name__c .= " " . $re->PCR__c;
		}
	}
	$wo->Project_Name = $r->End_Client_Name__c;
//	$wo->Project_ID = 6012;
	$wo->showTechs = 0;
	$createdTs = strtotime($r->CreatedDate);
	$wo->StartDate = date("m/d/Y", $createdTs);
	$wo->StartTime = date("g:i A", $createdTs);
	if (!empty($r->Amount_Per__c))
		$wo->Amount_Per = $r->Amount_Per__c;

/*	if (!empty($r->Tech_ID__c)) {
		if (empty($r->FS_Tech_Pay_Offer__c)) $r->FS_Tech_Pay_Offer__c = $r->Final_total_tech_pay__c;
	}*/

	if (!empty($r->FS_Tech_Pay_Offer__c)) {
		if (!empty($r->Tech_ID__c)) $wo->Tech_ID = $r->Tech_ID__c;
		$wo->Tech_Bid_Amount = $r->FS_Tech_Pay_Offer__c;
		$wo->PayMax = $r->FS_Tech_Pay_Offer__c;
	}


	$filter = new API_WorkOrderFilter;
	$filter->WO_ID = $wo->WO_ID;
	if ($r->AccountId == '001a000001LuvtrAAB') {
		$filter->Company_ID = 'XXPS';
		$cred = array("Username" => 'xeroxxpsapi', "Password" => 'crefE8es');
	}
	else {
		$filter->Company_ID = 'Xer';
		$cred = array("Username" => 'xeroxapi', "Password" => 'hAtRaR8s');
	}
	$c = new Core_Api_Class;
	$res = $c->getWorkOrders($cred["Username"], $cred["Password"], "", null, null, null, $filter);

	if (!$res->success || sizeof($res->data) == 0) {
		// create wo if no matching client wo id
		$apiRes = $c->createWorkOrder($cred["Username"], $cred["Password"], $wo->ShowTechs == 1, $wo, true);
		if ($apiRes->success)
			echo "Created " . $apiRes->data[0]->WIN_NUM . "<br/>";
	}
	else if (!empty($res->data[0]->WIN_NUM)) {
		// update wo if matching client wo id
		$apiRes = $c->updateWorkOrder($cred["Username"], $cred["Password"], $res->data[0]->WIN_NUM, $wo, true);
		if ($apiRes->success)
			echo "Updated " . $apiRes->data[0]->WIN_NUM . "<br/>";
	}
	else
		$apiRes = false;

	if ($apiRes && !$apiRes->success) {
		echo $apiRes->errors[0]->message;
	}

/*	if ($apiRes->success) {

                                $updateFields = array (
                                        'FieldSolutions_WO__c' => $apiRes->data[0]->WIN_NUM
                                );
                                $sObject = new SObject();
                                $sObject->fields = $updateFields;
                                $sObject->type = 'Work_Order__c';
                                $api->update(array($sObject));
	}*/
