<?php
    $page       = 'clients';
    $option     = 'wos';
    $selected   = 'wosView';
    
     
    
    require_once '../header.php';
    require_once 'includes/adminCheck.php';
   
    $company    = ( isset($_GET['v']) ) ? $_GET['v'] : NULL;
    $isFLS      = ( strtolower(trim($company)) === 'fls' ) ? true : false;
	
    $isFLSManager = (strtolower($company) == 'fls' && (strtolower($_SESSION['UserType'])==='install desk' || strtolower($_SESSION['UserType'])==='manager'));
    
    if($isFLSManager){
        $toolButtons = array(/*'MyTechs',*/ 'FindWorkOrder', 'CreateWorkOrder');
    } else {
        $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
    }   
    
    require_once '../navBar.php';
    //---  country list, state list
    $common = new Core_Api_CommonClass;
    $countryOptions = $common->getCountries();
    $countries = $countryOptions->data;
    
    $arrStates = array();
    $arrStates["US"] = Core_State::getByCode("US");
    $arrStates["CA"] = Core_State::getByCode("CA");
    $arrStates["MX"] = Core_State::getByCode("MX");
    $arrStates["UK"] = Core_State::getByCode("UK");
    $arrStates["BR"] = Core_State::getByCode("BR");
    $arrStates["KY"] = Core_State::getByCode("KY");
    $arrStates["BS"] = Core_State::getByCode("BS");

    $arrStatesHtml = array();
    foreach($arrStates as $countryCode => $arrayState)
    {
        $statesHtml = "<option value=\"\">Select State</option>";
        foreach($arrayState as $code=>$name)    
        {
            $statesHtml .= "<option value=\"$code\">$name</option>";
        }
        $arrStatesHtml[$countryCode]=$statesHtml;
    }

    
?>

<!-- Add Content Here  -->

<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<link rel="stylesheet" href="/widgets/css/main.css" type="text/css" />
<style>
    #headerLogo {
        margin-top : 0;
    }
    #headerRight {
        display : none;
    }
    #mainContainer {
        min-width : 0;
    }
</style>
<script type="text/javascript" src="/widgets/dashboard/js/company-projects/company/<?=urlencode($company)?>/pstate/active/"></script>
<script type="text/javascript" src="/widgets/dashboard/js/states/"></script>
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        /* Init calendar */

        $.map(['start_date','end_date'], function (id) {
            $('#'+id).calendar({dateFormat:'MDY/'});

            $('#'+id).focus( function() {
                //this.blur();
                $('#calendar_div').css('top', $(this).offset().top + 'px');
                $('#calendar_div').css('left', $(this).offset().left + $(this).width() + 7 + 'px');
                popUpCal.showFor(this);
            });

            /* init buttons */
            $('#' + id + '_img').click(function () {
                var id = $(this).attr('id').replace(/_img$/, '');
                var e  = document.getElementById(id);
                if ( e ) e.focus();
            });
        });
        /* Init calendar */

        /* Make project select elements */
        var pSelect = document.getElementById('Projects');
        var idx;
        if ( projects && pSelect ) {
            for ( idx in projects ) {
		try {
	                pSelect.add(new Option(projects[idx].name, projects[idx].id, false, false), null);
		} catch (e) {
			$(pSelect).append("<option value=\"" + projects[idx].id + "\">" + projects[idx].name + "</option>");
			pSelect.selectedIndex = 0;
		}
            }
        }
        /* Make project select elements */

        /* Make state select elements */
        var sSelect = document.getElementById('States');
	try {
        if ( states && sSelect ) {
            for ( idx in states ) {
		try {
	                sSelect.add(new Option(states[idx].name, states[idx].id, false, false), null);
		} catch (e) {
			$(sSelect).append("<option value=\"" + states[idx].id + "\">" + states[idx].name + "</option>");
			sSelect.selectedIndex = 0;
		}
            }
        }
	} catch (e) {}
        /* Make state select elements */

        /*  */
        $('#FindWorkOrderForm').submit(function(event) {
            event.preventDefault();

            var idx, l, wd;
            var query = '';
            var formData = $(this).serializeArray();
            if ( opener ) {
                /* if opener isn't wos.php page open it */
                l = opener.location.toString().replace(/\?.*$/, '');
                if ( -1 === l.search(/wos\.php$/i) ) {
                    for ( idx in formData ) {
                        if ( formData[idx].value ) {
                            query += '&' + formData[idx].name + '=' + encodeURIComponent(formData[idx].value).replace(/\~/g, '%7E').replace(/\!/g, '%21').replace(/\'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29');
                        }
                    }

                    l  = opener.location.protocol + '//' + opener.location.host;
                    l += '/clients/wos.php?v=<?=$company?>&tab=all&search=1';
                    l += query;
                    opener.location = l;

                } else {        //  opener == wos.php
                    wd = opener.wd;
                    opener._search   = true;

                    wd.resetFilters();
                    for ( idx in formData ) {
                        if ( !formData[idx].value ) {
                            wd.filters().set('_'+formData[idx].name, null);
                        } else {
                            wd.filters().set('_'+formData[idx].name, formData[idx].value);
                        }
                    }
                    wd.show({tab:'all', params:wd.getParams()});
                }
	            window.close();
            } else {
                for ( idx in formData ) {
                    if ( formData[idx].value ) {
                        query += '&' + formData[idx].name + '=' + encodeURIComponent(formData[idx].value).replace(/\~/g, '%7E').replace(/\!/g, '%21').replace(/\'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29');
                    }
                }
                l  = location.protocol + '//' + location.host;
                l += '/clients/wos.php?v=<?=$company?>&tab=all&search=1';
                l += query;
                location = l;
            }
            return false;
        });
    });
</script>
<script>
$(document).ready(function(){
    $("#country").change(function(){
        var countryCode = $("#country").val();
        if(countryCode=='US') {$("#States").html('<?=$arrStatesHtml['US']?>'); }
        else if(countryCode=='CA') {$("#States").html('<?=$arrStatesHtml['CA']?>'); }
        else if(countryCode=='MX') {$("#States").html('<?=$arrStatesHtml['MX']?>'); }
        else if(countryCode=='UK') {$("#States").html('<?=$arrStatesHtml['UK']?>'); }
        else if(countryCode=='BR') {$("#States").html('<?=$arrStatesHtml['BR']?>'); }
        else if(countryCode=='KY') {$("#States").html('<?=$arrStatesHtml['KY']?>'); }
        else if(countryCode=='BS') {$("#States").html('<?=$arrStatesHtml['BS']?>'); }        
    });
});
</script>
<p>
</p>
<div class="popup_form_holder">
	<form action="/" method="post" id="FindWorkOrderForm" name="FindWorkOrderForm">
		<input type="hidden" name="full_search" value="1" />
		<table class="form_table" cellspacing="0">
			<col class="col1" />
			<col class="col2" />
			<tr>
				<td class="label_field">WIN#</td>
				<td><input type="text" class="input_text numeric_input" name="win" value="" /></td>
			</tr>
			<tr>
				<td class="label_field">Client Work Order #</td>
				<td><input type="text" class="input_text" name="client_wo" value="" /></td>
			</tr>
                        <!--14139-->
                        <tr>
				<td class="label_field"></td>
				<td><select class="inputtext" name="WO_ID_Mode" id="WO_ID_Mode" style="width:158px;">                                    
                                        <option value="1">Exact</option>
                                        <option value="0">Contains</option>                                    
                                    </select></td>
			</tr>
			<tr>
				<td class="label_field">Purchase Order</td>
				<td><input type="text" class="input_text" name="po" value="" /></td>
			</tr>
			<tr>
				<td class="label_field">Show To Techs?</td>
				<td>
					<input type="radio" name="show_to_tech" id="ShowToTechYes" value="1" /><label for="ShowToTechYes">Yes</label>
					<input type="radio" name="show_to_tech" id="ShowToTechNo" value="0" /><label for="ShowToTechNo">No</label>
					<input type="radio" name="show_to_tech" id="ShowToTechAny" value="" checked="checked" /><label for="ShowToTechAny">Any</label>
				</td>
			</tr>
			<tr>
				<td class="label_field">Work Start Date (&gt;=)</td>
				<td><input type="text" style="cursor: pointer;" class="input_text input_date" name="date_start" id="start_date" value="" />&#160;<img id="start_date_img" src="/widgets/images/calendar.gif" style="position:relative; top:4px; cursor: pointer;" /></td>
			</tr>
			<tr>
				<td class="label_field">Work Start Date (&lt;=)</td>
				<td><input type="text" style="cursor: pointer;" class="input_text input_date" name="date_end" id="end_date" value="" />&#160;<img id="end_date_img" src="/widgets/images/calendar.gif" style="position:relative; top:4px; cursor: pointer;" /></td>
			</tr>
  
			<tr>
				<td class="label_field">Region</td>
				<td><input type="text" class="input_text" name="region" value="" /></td>
			</tr>
			<tr>
				<!-- TODO Projects -->
				<td class="label_field">Project</td>
				<td>
					<select name="projects" id="Projects">
						<option value="">Select Project</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="label_field">Site Name</td>
				<td><input type="text" class="input_text" name="site_name" value="" /></td>
			</tr>
			<tr>
				<td class="label_field">Site Number</td>
				<td><input type="text" class="input_text" name="site_number" value=""></td>
			</tr>
			<tr>
				<td class="label_field">City</td>
				<td><input type="text" class="input_text" name="city" value="" /></td>
			</tr>
			<tr>
				<!-- TODO States -->
				<td class="label_field">State</td>
				<td>
					<select name="state" id="States">
						<option value="">Select State</option>
					</select>
				</td>
			</tr>
			<tr>
				<td class="label_field">Zip</td>
				<td><input type="text" class="input_text numeric_input" name="zip" value="" /></td>
			</tr>
			<tr>
                <td class="label_field">Country</td>
                <td>
                <select name="country" id="country">
                    <option value="">Select Country</option>
                    <?php
                    $dotline="----------------";
                    $icount=0;
                    foreach ($countries as $country) {
                        echo "<option value=\"$country->Code\">$country->Name</option>";
                        if($icount==2) {        
                            echo("<option value=\"\">$dotline</option>");        
                        }        
                        $icount = $icount+1;
                    }
                    
                    ?>                    
                </select>
                </td>
            </tr>            
            
			<tr>
				<td class="label_field">Update Requested?</td>
				<td>
					<input type="radio" name="update_req" id="UpdateRequestedYes" value="1" /><label for="UpdateRequestedYes">Yes</label>
					<input type="radio" name="update_req" id="UpdateRequestedNo" value="0" /><label for="UpdateRequestedNo">No</label>
					<input type="radio" name="update_req" id="UpdateRequestedAny" value="" checked="checked" /><label for="UpdateRequestedAny">Any</label>
				</td>
			</tr>
                        <!--732-->
			<tr>
				<td class="label_field">Site Contact Confirmed</td>
				<td>
					<input type="radio" name="store_notify" id="StoreNotifiedYes" value="1" /><label for="StoreNotifiedYes">Yes</label>
					<input type="radio" name="store_notify" id="StoreNotifiedNo" value="0" /><label for="StoreNotifiedNo">No</label>
					<input type="radio" name="store_notify" id="StoreNotifiedAny" value="" checked="checked" /><label for="StoreNotifiedAny">Any</label>
				</td>
			</tr>
			<tr>
				<td class="label_field">Site Contact Backed Out</td>
				<td>
					<input type="radio" name="SiteContactBackedOutChecked" id="SiteContactBackedOutCheckedYes" value="1" /><label for="SiteContactBackedOutCheckedYes">Yes</label>
					<input type="radio" name="SiteContactBackedOutChecked" id="SiteContactBackedOutCheckedNo" value="0" /><label for="SiteContactBackedOutCheckedNo">No</label>
					<input type="radio" name="SiteContactBackedOutChecked" id="SiteContactBackedOutCheckedAny" value="" checked="checked" /><label for="SiteContactBackedOutCheckedAny">Any</label>
				</td>
			</tr>
                        <!--end 732-->
			<tr>
				<td class="label_field">Tech Checked In?</td>
				<td>
					<input type="radio" name="checked_in" id="TechCheckedInYes" value="1" /><label for="TechCheckedInYes">Yes</label>
					<input type="radio" name="checked_in" id="TechCheckedInNo" value="0" /><label for="TechCheckedInNo">No</label>
					<input type="radio" name="checked_in" id="TechCheckedInAny" value="" checked="checked" /><label for="TechCheckedInAny">Any</label>
				</td>
			</tr>
			<tr>
				<td class="label_field">Tech Reviewed WO?</td>
				<td>
					<input type="radio" name="reviewed_wo" id="TechReviewedWOYes" value="1" /><label for="TechReviewedWOYes">Yes</label>
					<input type="radio" name="reviewed_wo" id="TechReviewedWONo" value="0" /><label for="TechReviewedWONo">No</label>
					<input type="radio" name="reviewed_wo" id="TechReviewedWOAny" value="" checked="checked" /><label for="TechReviewedWOAny">Any</label>
				</td>
			</tr>
			<tr>
				<td class="label_field">Tech Confirmed?</td>
				<td>
					<input type="radio" name="tech_confirmed" id="TechConfirmedYes" value="1" /><label for="TechConfirmedYes">Yes</label>
					<input type="radio" name="tech_confirmed" id="TechConfirmedNo" value="0" /><label for="TechConfirmedNo">No</label>
					<input type="radio" name="tech_confirmed" id="TechConfirmedAny" value="" checked="checked" /><label for="TechConfirmedAny">Any</label>
				</td>
			</tr>
			<tr>
				<td class="label_field">Tech Complete?</td>
				<td>
					<input type="radio" name="tech_complete" id="TechCompleteYes" value="1" /><label for="TechCompleteYes">Yes</label>
					<input type="radio" name="tech_complete" id="TechCompleteNo" value="0" /><label for="TechCompleteNo">No</label>
					<input type="radio" name="tech_complete" id="TechCompleteAny" value="" checked="checked" /><label for="TechCompleteAny">Any</label>
				</td>
			</tr>
			<tr>
				<td class="label_field">Site Complete?</td>
				<td>
					<input type="radio" name="site_complete" id="SiteCompleteYes" value="1" /><label for="SiteCompleteYes">Yes</label>
					<input type="radio" name="site_complete" id="SiteCompleteNo" value="0" /><label for="SiteCompleteNo">No</label>
					<input type="radio" name="site_complete" id="SiteCompleteAny" value="" checked="checked" /><label for="SiteCompleteAny">Any</label>
				</td>
			</tr>
			<tr>
				<td class="label_field">Audit Needed</td>
				<td>
					<input type="radio" name="AuditNeeded" id="AuditNeededYes" value="1" /><label for="AuditNeededYes">Yes</label>
					<input type="radio" name="AuditNeeded" id="AuditNeededNo" value="0" /><label for="AuditNeededNo">No</label>
					<input type="radio" name="AuditNeeded" id="AuditNeededAny" value="" checked="checked" /><label for="AuditNeededAny">Any</label>
				</td>
			</tr>
                        <tr>
				<td class="label_field">Audit Complete</td>
				<td>
					<input type="radio" name="AuditComplete" id="AuditCompleteYes" value="1" /><label for="AuditCompleteYes">Yes</label>
					<input type="radio" name="AuditComplete" id="AuditCompleteNo" value="0" /><label for="AuditCompleteNo">No</label>
					<input type="radio" name="AuditComplete" id="AuditCompleteAny" value="" checked="checked" /><label for="AuditCompleteAny">Any</label>
				</td>
			</tr>
			<tr>
				<td class="label_field">Approved</td>
				<td>
					<input type="radio" name="approved" id="ApprovedYes" value="1" /><label for="ApprovedYes">Yes</label>
					<input type="radio" name="approved" id="ApprovedNo" value="0" /><label for="ApprovedNo">No</label>
					<input type="radio" name="approved" id="ApprovedAny" value="" checked="checked" /><label for="ApprovedAny">Any</label>
				</td>
			</tr>
			<tr>
				<td class="label_field"><label for="AbortFee">Abort/Short Notice Pay</label></td>
				<td> <input type="checkbox" name="AbortFee" value="1" id="AbortFee" /> </td>
			</tr>
			<tr>
				<td class="label_field"><label for="extraPay">Extra Pay?</label></td>
				<td> <input type="checkbox" name="extra_pay" value="1" id="extraPay" /> </td>
			</tr>
			<tr>
				<td class="label_field">Work Out of Scope?</td>
				<td>
					<input type="radio" name="out_of_scope" id="WorkOutOfScopeYes" value="1" /><label for="WorkOutOfScopeYes">Yes</label>
					<input type="radio" name="out_of_scope" id="WorkOutOfScopeNo" value="0" /><label for="WorkOutOfScopeNo">No</label>
					<input type="radio" name="out_of_scope" id="WorkOutOfScopeAny" value="" checked="checked" /><label for="WorkOutOfScopeAny">Any</label>
				</td>
			</tr>
			<tr>
				<td class="label_field">Tech Paid?</td>
				<td>
					<input type="radio" name="tech_paid" id="TechPaidYes" value="1" /><label for="TechPaidYes">Yes</label>
					<input type="radio" name="tech_paid" id="TechPaidNo" value="0" /><label for="TechPaidNo">No</label>
					<input type="radio" name="tech_paid" id="TechPaidAny" value="" checked="checked" /><label for="TechPaidAny">Any</label>
				</td>
			</tr>
			<tr>
				<td class="label_field">Tech First Name (contains)</td>
				<td><input type="text" class="input_text" name="tech_fname" value="" /></td>
			</tr>
			<tr>
				<td class="label_field">Tech Last Name (contains)</td>
				<td><input type="text" class="input_text" name="tech_lname" value="" /></td>
			</tr>
			<tr>
				<td class="label_field">Tech ID</td>
				<td><input type="text" class="input_text numeric_input" name="tech" value="" /></td>
			</tr>
			<tr>
				<td class="label_field"><label for="showSourced">or Show All Sourced</label></td>
				<td><input type="checkbox" onclick="if (this.checked) $('#showNotSourced').attr('checked',false);" name="show_sourced" id="showSourced" value="1" /></td>
			</tr>
			<tr>
				<td class="label_field"><label for="showNotSourced">or Show All NOT Sourced</label></td>
				<td><input type="checkbox" onclick="if (this.checked) $('#showSourced').attr('checked',false);" name="show_not_sourced" id="showNotSourced" value="1" /></td>
			</tr>
			<tr>
				<td class="label_field">Paperwork Received?</td>
				<td>
					<input type="radio" name="paper_received" id="PaperworkReceivedYes" value="1" /><label for="PaperworkReceivedYes">Yes</label>
					<input type="radio" name="paper_received" id="PaperworkReceivedNo" value="0" /><label for="PaperworkReceivedNo">No</label>
					<input type="radio" name="paper_received" id="PaperworkReceivedAny" value="" checked="checked" /><label for="PaperworkReceivedAny">Any</label>
				</td>
			</tr>
			<tr>
				<td class="label_field">Lead</td>
				<td>
					<input type="radio" name="lead" id="LeadYes" value="1" /><label for="LeadYes">Yes</label>
					<input type="radio" name="lead" id="LeadNo" value="0" /><label for="LeadNo">No</label>
					<input type="radio" name="lead" id="LeadAny" value="" checked="checked" /><label for="LeadAny">Any</label>
				</td>
			</tr>
			<tr>
				<td class="label_field">Assist</td>
				<td>
					<input type="radio" name="assist" id="AssistYes" value="1" /><label for="AssistYes">Yes</label>
					<input type="radio" name="assist" id="AssistNo" value="0" /><label for="AssistNo">No</label>
					<input type="radio" name="assist" id="AssistAny" value="" checked="checked" /><label for="AssistAny">Any</label>
				</td>
			</tr>
			<?php if ( $isFLS ): ?>
			<tr>
				<td class="label_field">FLS ID</td>
				<td><input type="text" class="input_text" name="fls_id" value="" /></td>
			</tr>
			<?php endif; ?>
			<tr>
				<td class="label_field">Deactivated</td>
				<td>
					<input type="radio" name="deactivated" id="DeactivatedYes" value="1" /><label for="DeactivatedYes">Yes</label>
					<input type="radio" name="deactivated" id="DeactivatedNo" value="0" checked="checked" /><label for="DeactivatedNo">No</label>
					<input type="radio" name="deactivated" id="DeactivatedAny" value="" /><label for="DeactivatedAny">Any</label>
				</td>
			</tr>
			<?php if ( $isFLS ): ?>
			<tr>
				<td class="label_field">Short Notice</td>
				<td>
					<input type="radio" name="short_notice" id="shortNoticeYes" value="1" /><label for="shortNoticeYes">Yes</label>
					<input type="radio" name="short_notice" id="shortNoticeNo" value="0" /><label for="shortNoticeNo">No</label>
					<input type="radio" name="short_notice" id="shortNoticeAny" value="" checked="checked" /><label for="shortNoticeAny">Any</label>
				</td>
			</tr>
			<?php endif; ?>
			<tr class="submit_raw">
				<td colspan="2" align="center"><input type="submit" name="Go" id="Go" value="Search" class="input_button" /></td>
			</tr>
		</table>
	</form>
</div>
<!-- End Content -->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
