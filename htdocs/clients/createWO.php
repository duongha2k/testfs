<?php
    $page   = 'clients';
    $option = 'createWO';
    require ("../headerStartSession.php");

    $siteTemplate = ( !empty($_SESSION['template']) ) ? $_SESSION['template'] : NULL;
    if ($siteTemplate == "ruo") {
        require '../templates/ruo/includes/header.php';
    } else {
        require '../header.php';
    }

    $_SERVER['HTTPS'] = ( !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ) ? 'on' : 'off';
    $loggedIn   = ( !empty($_SESSION['loggedIn']) ) ? $_SESSION['loggedIn'] : NULL ;
    $Company_ID = ( !empty($_SESSION['Company_ID']) ) ? $_SESSION['Company_ID'] : NULL;

    if($loggedIn!="yes"){
        header( 'Location: http'.($_SERVER['HTTPS']=='on' ? 's':'').'://'.$_SERVER['HTTP_HOST'].'/clients/logIn.php' ); // Redirect to logIn.php
    }

    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'quickassign', 'quickapprove');
    if($_SESSION['UserType']=='Manager' || $_SESSION['UserType']=='Install Desk')$toolButtons = array('FindWorkOrder', 'CreateWorkOrder');
    require '../navBar.php';
    require 'includes/adminCheck.php';
    require 'includes/PMCheck.php';

//    $navBarState = isset($_COOKIE['wosDetailsNavBarState'])?$_COOKIE['wosDetailsNavBarState']:null;
    if (!$navBarState) {
        $navBarState = 'opened';
    }    
?>

<script type="text/javascript">
    window._company = '<?=$_GET['v']?>';
    //  use for load images and save it in browser cache only!!!
    var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
    var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";
    
</script>
<!--<script type="text/javascript" src="/widgets/js/jquery.min.js"></script>-->
<script type="text/javascript" src="/widgets/js/jquery.appear.min.js"></script>
<script src="<?=empty($_SERVER['HTTPS']) ? 'http' : 'https'?>://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<link rel="stylesheet" href="<?=empty($_SERVER['HTTPS']) ? 'http' : 'https'?>://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/ui-lightness/jquery-ui.css" type="text/css" />

<script type="text/javascript" src="/widgets/js/jquery.form.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.scrollFollow.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.easing.pack.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.iframe-auto-height.plugin.1.5.0.min.js"></script>
<!--<script type="text/javascript" src="/widgets/js/FSQuickAssignTool.js?11112011"></script>-->
<?php script_nocache ("/widgets/js/FSQuickAssignTool.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/functions.js"></script>-->
<?php script_nocache ("/widgets/js/functions.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>-->
<?php script_nocache ("/widgets/js/FSWidget.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/FSWidgetDashContentLeft.js"></script>-->
<?php script_nocache ("/widgets/js/FSWidgetDashContentLeft.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/FSWidgetDashContentRight.js"></script>-->
<?php script_nocache ("/widgets/js/FSWidgetDashContentRight.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/FSWidgetDashboard.js?11112011"></script>-->
<?php script_nocache ("/widgets/js/FSWidgetDashboard.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/FSWidgetWODetails.js?1111201103"></script>-->
<?php script_nocache ("/widgets/js/FSWidgetWODetails.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>-->
<?php script_nocache ("/widgets/js/FSPopupAbstract.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>-->
<?php script_nocache ("/widgets/js/FSPopupRoll.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/ProgressBar.js"></script>-->
<?php script_nocache ("/widgets/js/ProgressBar.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/ProgressBarAdapterAbstract.js"></script>-->
<?php script_nocache ("/widgets/js/ProgressBarAdapterAbstract.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/ProgressBarAdapterPopupSpin.js"></script>-->
<?php script_nocache ("/widgets/js/ProgressBarAdapterPopupSpin.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/FSFindWorkOrder.js"></script>-->
<?php script_nocache ("/widgets/js/FSFindWorkOrder.js"); ?>
<?php script_nocache ("/widgets/js/FSMultiselectControl.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>-->
<?php script_nocache ("/widgets/js/ajaxfileupload.js"); ?>
<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<link rel="stylesheet" href="/widgets/css/popup.css" type="text/css" />
<link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" />

<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.formatCurrency-1.4.0.js"></script>
<!--<script type="text/javascript" src="/widgets/js/FSTechPayUtils.js"></script>-->
<?php script_nocache ("/widgets/js/FSTechPayUtils.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/FSTechSchedule.js"></script>-->
<?php script_nocache ("/widgets/js/FSTechSchedule.js"); ?>
<!--<script type="text/javascript" src="wosDetails.js"></script>-->
<?php script_nocache ("/clients/wosDetails.js"); ?>
<script type="text/javascript" src="/widgets/js/jquery.scrollTo.js"></script>
<script type="text/javascript" src="../library/jquery/jquery.bt.min.js"></script>
<!--script type="text/javascript" src="/widgets/js/FSWidgetWODashboard.js"></script-->
<!--script type="text/javascript" src="/widgets/js/FSMyTechs.js"></script-->
<?php script_nocache ("/clients/js/wintags.js"); ?>

<?php script_nocache ("/widgets/js/FSCoreClasses.js"); ?>
<?php script_nocache ("/widgets/js/FSModelViewController.js"); ?>
<?php script_nocache ("/widgets/js/FSFlyOutPanel.js"); ?>
<?php script_nocache ("/widgets/js/FSSuggestedTechPay.js"); ?>

<script type="text/javascript">
    var schedule = new FSTechSchedule({width: 940, height: 450});
</script>

<style type="text/css">
/* Dialog
----------------------------------*/
.ui-dialog { position: relative; padding: .2em; width: 300px; }
.ui-dialog .ui-dialog-titlebar { padding: .5em .3em .3em 1em; position: relative;  }
.ui-dialog .ui-dialog-title { float: left; margin: .1em 0 .2em; } 
.ui-dialog .ui-dialog-titlebar-close { position: absolute; right: .3em; top: 50%; width: 19px; margin: -10px 0 0 0; padding: 1px; height: 18px; }
.ui-dialog .ui-dialog-titlebar-close span { display: block; margin: 1px; }
.ui-dialog .ui-dialog-titlebar-close:hover, .ui-dialog .ui-dialog-titlebar-close:focus { padding: 0; }
.ui-dialog .ui-dialog-content { border: 0; padding: .5em 1em; background: none; overflow: auto; zoom: 1; }
.ui-dialog .ui-dialog-buttonpane { text-align: left; border-width: 1px 0 0 0; background-image: none; margin: .5em 0 0 0; padding: .3em 1em .5em .4em; }
.ui-dialog .ui-dialog-buttonpane button { float: right; margin: .5em .4em .5em 0; cursor: pointer; padding: .2em .6em .3em .6em; line-height: 1.4em; width:auto; overflow:visible; }
.ui-dialog .ui-resizable-se { width: 14px; height: 14px; right: 3px; bottom: 3px; }
.ui-draggable .ui-dialog-titlebar { cursor: move; }

 #container2{
		position:relative;	/* This fixes the IE7 overflow hidden bug */
	clear:both;
	width:100%;			/* width of whole page */
	overflow: hidden;		/* This chops off any overhanging divs */
	    min-width: 1300px;
	}
	#center, #layout{
		float:left;
		width:100%;			/* width of page */
		position:relative;
	}
	#center #layout{
		right:78%;	
	}
	#container2 #detailsContainer{
		width:59%;			/* width of center column content (column width minus padding on either side) */
	left:98%;			/* 100% plus left padding of center column */
	margin-top:2%;
	/*overflow-x: scroll;*/
	}
	#container2 #dashleftcontent{
		width:21%;			/* Width of left column content (column width minus padding on either side) */
	left:20%;			/* width of (right column) plus (center column left and right padding) plus (left column left padding) */
	}
	#container2 #dashrightcontent{
		width:21%;			/* Width of right column content (column width minus padding on either side) */
	left:81%;			/* Please make note of the brackets here:
					(100% - left column width) plus (center column left and right padding) plus (left column left and right padding) plus (right column left padding) */
	}
	#dashleftcontent, #detailsContainer, #dashrightcontent{
		float:left;
	position:relative;
	padding:0 0 1em 0;	/* no left and right padding on columns, we just make them narrower instead 
					only padding top and bottom is included here, make it whatever value you need */
	overflow:hidden;
	}

		
#newsCenter{
		display: none;
}
	
.toolSubmit{
		background-color: #4790D4;
		color: #FFFFFF;
		width: 70px; 
		margin-top: 5px;
}
.toolSubmit:hover{
		font-weight: bold;
		text-decoration:underline;
		background-color: #032D5F;
		color: #FFFFFF;
		cursor: pointer;
}
	
.toolsRightContent div{
		line-height: 10px;
}
.right_col{
		float: none;
		width: auto;
		text-align: left;
		margin-left: 10px;
		margin-top: 25px;
}
.left_col{
		float: none;
		width: auto;
		text-align: left;
		margin-left: 10px;
		margin-top: 25px;
                background: transparent;
}
	#dashleftcontent {
		
		background-color:#FFFFFF;
/*		display: none;*/
		font: 10px/10px Verdana,Arial,Helvetica,sans-serif;
		
		padding-right: 16px;
		line-height: 15px;
	}

	#dashrightcontent {
		
		background-color:#FFFFFF;
	    display: none;
	    font: 10px/10px Verdana,Arial,Helvetica,sans-serif;
	    text-align: left;
	    white-space:nowrap;
	    
	    background: url("/widgets/images/border_right.png") no-repeat scroll left top transparent;
	    padding-left: 16px;
		}


	.use-sidebar-right #dashrightcontent {
	    display:table-cell;
	    width: 245px;
	    white-space:nowrap;
		overflow: hidden;
		margin-top: 10px;
	}
	
	.use-sidebar-left #dashleftcontent {
	    display:table-cell;
	    width: 245px;
	    white-space:nowrap;
		overflow: hidden;
	}

	
	.use-sidebar-right #separatorRight {
		margin-right: auto;
	    display: block;
	    outline: none;
	    width: 16px;
	    height: 16px;
	   	background: url('/widgets/images/arrows-right5.png');
	    border-color: #FFF;
	    margin-bottom: 10px;
	}
	
	.use-sidebar-left #separatorLeft {
	    display: block;
	    outline: none;
	    width: 16px;
	    height: 16px;
	   	background: url('/widgets/images/arrows-left5.png');
	    border-color: #FFF;
	    margin-left: auto;

	}
	
	 .sidebar-at-right #separatorRight {float: left;}
	 .sidebar-at-left #separatorLeft {float: left;}
	
	.sidebar-at-right #separatorRight{
		float: right;
	    display: block;
	    outline: none;
	    width: 16px;
	    height: 16px;
	   	background: url('/widgets/images/arrows-left5.png') no-repeat;
	   	/*margin-left: -200px;*/
	   	margin-top: 0px;
	
	  }
	  
	  .sidebar-at-left #separatorLeft{
		float: left;
	    display: block;
	    outline: none;
	    width: 16px;
	    height: 16px;
	   	background: url('/widgets/images/arrows-right5.png') no-repeat;
	   	margin-right: 2px;
	   	margin-top: 0px;
	  }
	 
	 #container2 #dashleftcontent{
		width:245px;
	 }
	 #container2 #dashrightcontent{
		width:245px;
		
	 }
	 
	 .heading{
		background: url("/widgets/images/triangle.gif") no-repeat scroll left 1px transparent;
    font-size: 13px;
    margin: 10px 0;
    padding: 0 0 0 20px;
	color: #595959;
	font-weight: bolder;
	}
	.toolsLeftContent{
		
		margin: 0px 0px 10px 10px;
		color:#10085a;
		
	}
	
	.toolsRightContent{
		
		margin: 10px 0px 10px 0px;
		text-align:left;
		color:#10085a;
		border-top: 1px #DDDDDD solid;
	}
	
	.toolSubmit{
		background-color: #4790D4;
		color: #FFFFFF;
		width: 70px; 
		margin-top: 5px;
	}
	.toolSubmit:hover{
		font-weight: bold;
		text-decoration:underline;
		background-color: #032D5F;
		color: #FFFFFF;
		cursor: pointer;
	}
	
	.toolsRightContent div{
		line-height: 1;
	}
	table tr.special td{
		border-top: 1px #DDDDDD solid;
	}
	#chevrons{
		width: 18%; 
		position:relative;
	}
	#separatorLeftInside{
		margin-top:20px;
	}
</style>


<script type="text/javascript">
    var LVDefaultFunction = function(){this.addFieldClass();}
    var LVDefaults = {
        onValid : LVDefaultFunction,
        onInvalid : LVDefaultFunction
    }
    var pmContext = "<?=$_GET["v"]?>";
    function gotoTab(name) {
        location = location.toString().replace('<?=$_SERVER['SCRIPT_NAME']?>', '/clients/wos.php') + '&tab=' + name;
        return false;
    }
    function toggleNavBar( btn )
    {
        var bar = $('#leftVerticalContainer');
        var content = $('#detailsContainer');
        btn = $(btn);

		if (btn.is(".closed")){
			btn.removeClass("closed");
                btn.parent().css('width', '18%');
				btn.prev('span').addClass('displayNone');
                bar.css('display', '');
                bar.parent().css('width', '18%');
                bar.parent().css('background', 'transparent url(/widgets/images/border_right.png) no-repeat scroll right top');
                content.css('width', '59%');
                setCookie('wosDetailsNavBarState', 'opened', 30);
			
		}else{
			btn.css("background-image","");
			btn.addClass("closed");
			btn.prev('span').removeClass('displayNone');
                btn.parent().css('width', '100%');
                bar.css('display', 'none');
                bar.parent().css('width', '3%');
                bar.parent().css('background', '');
                content.css('width', '96%');
                setCookie('wosDetailsNavBarState', 'closed', 30);
				
				
        }
        
        return false;
    }
    /**
     *  calendarInit
     *
     *  Display filter post handler
     */
    function calendarInit() {
        $.map(['edit_StartDate', 'edit_EndDate','edit_DateNotified'], function( id ) {
            //$('#' + roll.container().id + ' #' + id).focus( function() {
                //$('#calendar_div').css('z-index', ++roll.zindex);
                $('#'+id).calendar({dateFormat:'MDY/'});
            //});
        });
    }
    /**
     * calendarMove
     *
     * @param event  event
     * @access public
     * @return void
     */
    function calendarMove( input ) {
        $('#calendar_div').css('top', $(input).offset().top + $(input).height() + 6 + 'px');
        $('#calendar_div').css('left', $(input).offset().left + 'px');
        popUpCal.showFor(input);
    }
    var roll;
    var detailsWidget;
    var navWidget;
    var wd;
    var activityLog;    //  Activity Log object
    var findWorkOrder;  //  Find work order object
    var assignTool;
    var progressBar;    //  Progress bar object

        var checkMinWidth = function(){
		
				$('body').css('min-width', '1400px');
				
				<?php if (!isset($_GET['showrightpanel'])):?>
        if(objMain.hasClass('use-sidebar-left')){
								
                                $("#mainContainer").css("width", "100%");
								$("#detailsContainer").css("width", "74%").css("left", "98%");
								$("#dashleftcontent").css("left", "3%");
							$('body').css('min-width', '1135px');	
                        }
					if(!objMain.hasClass('use-sidebar-left')){
								
                               $("#detailsContainer").removeAttr("style");
						$("#dashrightcontent").removeAttr("style");
						$("#dashleftcontent").removeAttr("style");
						$("#detailsContainer").css("width", "90%").css("left", "81%");
								$("#dashleftcontent").css("left", "10%");		$('body').css('min-width', '1400px');
                        }	

				<?php endif;?>

<?php if (!empty($_GET['showrightpanel'])):?>

                if(objMain.hasClass('use-sidebar-left') && !objMain.hasClass('use-sidebar-right')){
								
                                $("#mainContainer").css("width", "100%");
								
													$("#detailsContainer").css("width", "77%");
$("#dashleftcontent").css("left", "0");
								
                        }
                if(objMain.hasClass('use-sidebar-left') && objMain.hasClass('use-sidebar-right')){
						$("#detailsContainer").removeAttr("style");
						$("#dashrightcontent").removeAttr("style");
						$("#dashleftcontent").removeAttr("style");
                        $("#mainContainer").css("min-width", "950px");
                        $("#container2").css("width", "100%");
						//both open
                }

                if(objMain.hasClass("use-sidebar-right") && !objMain.hasClass("use-sidebar-left")){

                        $("#mainContainer").css("min-width", "950px");
						$("#detailsContainer").css("left", "81%").css("width", "74%");
						$("#dashrightcontent").css("left", "81%");
						//open left
                        
                }

                if(!objMain.hasClass("use-sidebar-right") && !objMain.hasClass("use-sidebar-left")){

                        $("#mainContainer").css("min-width", "750px");
                  	
					$("#detailsContainer").css("width", "96%").css("left", "80%");
					
               }
				
				<?php endif;?>
        }
	
    $(document).ready(function() {
        roll = new FSPopupRoll();
        detailsWidget = new FSWidgetWODetails({container:'detailsContainer',tab:'create'},roll);
        detailsWidget.show({tab:'create',params:{company:window._company,container:'detailsContainer',tab:"new",project_id:'<?=$_GET['project_id']?>',techID:'<?=$_GET['techID']?>'}});
		objMain = $("#container2");

        dr = new FSWidgetDashContentRight({container:'dashrightcontent', objMain: objMain, checkMinWidth: checkMinWidth, company:window._company});
		<?php if (!empty($_GET['showrightpanel']) && !$isFLSManager):?>
        dr.show();
		dr.separatorClick();
		<?php endif;?>

        dl = new FSWidgetDashContentLeft({container:'dashleftcontent', objMain: objMain, dr: dr, checkMinWidth: checkMinWidth, company:window._company});
		<?php if (!$isFLSManager):?>
        dl.show();
		<?php endif;?>
		checkMinWidth();
		//dl.separatorClick();

        navWidget = new FSWidgetWODetails({container:'leftVerticalContainer',tab:'navigation',customfunctions:{showPreloader:function(){}}},null);
//        navWidget.show({tab:'navigation',params:{company:window._company,container:'leftVerticalContainer'}});
        <?php if ($navBarState == 'closed'): ?>
            var btn = $('#navBarToggle');
            var bar = $('#leftVerticalContainer');
            var content = $('#detailsContainer');        
            btn.text('>>');
            btn.parent().css('width', '100%');
			btn.prev('span').removeClass('displayNone');
            bar.css('display', 'none');
            bar.parent().css('width', '2%');
            bar.parent().css('background', '');
            content.css('width', '97%');
        <?php endif; ?>            
    });
</script>
<!-- Main content -->


<div id="container2" class="clr use-sidebar-left" style="width:100%;">
<div id="center">
<div class="left_col">
			<div style="display: none;" onclick="javascript:dl.separatorClick(this);" id="separatorLeftOutside">
			<a id="separatorLeft"></a>
	</div>
			<div onclick="javascript:dr.separatorClick(this);" id="separatorOutside">
			<a id="separatorRight"></a>
    </div>
</div>
		<div id="layout">
		<div id="detailsContainer"></div>
		<div id="dashleftcontent" class="left_col"></div>

		<script type="text/javascript">
		if (document.referrer.match(/wos./)){
			if(document.referrer.match(/wosDetails/)){
				document.write('&nbsp');
			}else{
		document.write('<br class="clear">');
		}
		}
		</script>
		<div id="dashrightcontent"></div>

	</div>
</div>
</div>
<script>
      var _wosDetails = null;
</script> 
<?php
/*** End Content ***/
require ("../footer.php");
?>
