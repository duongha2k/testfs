<?php

$page   = "clients";
$option = "dashboard";
require ("../headerStartSession.php");
$siteTemplate = $_SESSION['template'];
if ($siteTemplate == "ruo") {
    require ("../templates/ruo/includes/header.php");
} else {
    require ("../header.php");
}

$loggedIn   = $_SESSION['loggedIn'];
$Company_ID = $_SESSION['Company_ID'];

if($loggedIn!="yes"){
    header( 'Location: http://'.$_SERVER['HTTP_HOST'].'/clients/logIn.php' ); // Redirect to logIn.php
}
	$isFLSManager = (strtolower($Company_ID) == 'fls' && (strtolower($_SESSION['UserType'])==='install desk' || strtolower($_SESSION['UserType'])==='manager'));
	
    if($isFLSManager){
        $toolButtons = array(/*'MyTechs',*/ 'FindWorkOrder', 'CreateWorkOrder');
    } else {
        $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
    }

require ("../navBar.php");
require ("includes/adminCheck.php");
require ("includes/PMCheck.php");
echo '<script type="text/javascript" src="/library/jquery/jquery.bt.min.js"></script>';
echo '<script type="text/javascript" src="wosDetails.js"></script>';

script_nocache ("/widgets/js/FSMultiselectControl.js");
script_nocache ("/widgets/js/FSCoreClasses.js");
script_nocache ("/widgets/js/FSModelViewController.js");
script_nocache ("/widgets/js/FSFlyOutPanel.js");
script_nocache ("/widgets/js/FSSuggestedTechPay.js");

echo '<script>
      var _wosDetails = null;
    </script>   ';
/*** Add Content Here ***/
if ($siteTemplate == "ruo") {
	require ("../templates/ruo/includes/client_dashboardContent.php");
} else {
	require ("dashboardContent.php");
}

/*** End Content ***/
//require ("../footer.php");  

$filterID = 563;
$reportID = 94;

$TechClass = new Core_Api_TechClass();
//$TechClass = new Core_Api_TechClass();


//print_r($newComment);
//$Core_Comments = new Core_Comments();
//$Core_Comments->addComment('Test5', '1713', '53497', 'ROSS','',1,0); 

//echo("addComment_5: <pre>");echo("</pre>");

?>