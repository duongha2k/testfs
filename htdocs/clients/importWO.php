<?php
$page = "clients";
$option = "wos";
$selected = "importWO";

    if($isFLSManager){
        $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
    } else {
        $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
    }    

require_once("../header.php");
require_once("../navBar.php");
require_once("includes/PMCheck.php");
require_once("../library/caspioAPI.php");
require_once("includes/adminCheck.php");
require_once(dirname(__FILE__) . "/../../wdata/application/modules/dashboard/views/helpers/OwnerDropdownOptions.php");
$companyID = caspioEscape($_GET["v"]);
$helper = new Dashboard_View_Helper_OwnerDropdownOptions();

/*********************************************/
$valid_types = array('xls');
$max_file_size = 16 * 1048576; // 16 MB
$upload_dir = '/mnt/services/a2720/app/uploads/kettle/';
//$upload_dir = '/mnt/services/a3626/batchFiles/uploads/kettle/';
//$upload_dir = '../../../../uploads/kettle/';
/*********************************************/

$errors = array();
$success = '';
if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {
    $name = addslashes(trim($_SESSION['UserName']));

	$authData = array('login'=>$_SESSION['UserName'], 'password'=>$_SESSION['Password']);

	$user = new Core_Api_User();
	$user->checkAuthentication($authData);

	
    if (!$user->isAuthenticate()) {
        $errors[] = 'An error occured. Please contact administrator.';
    }
    $hash = $_SESSION['Password'];
    $mail = $user->getEmailPrimary();
    $clientID = $companyID;
    $woOwner = $_POST['workOrderOwner'];

    if (empty($name) || empty($hash) || empty($mail) || !isset($_FILES["userfile"]) || !isset($woOwner)) {
        $errors[] = "Error: All parameters required";
    } else {
    if (isset($_FILES["userfile"])) {
        if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {
            $filename = $_FILES['userfile']['tmp_name'];
            $ext = substr($_FILES['userfile']['name'],1+strrpos($_FILES['userfile']['name'], "."));
            $internalFileName = time() . '_' . preg_replace('/[^a-zA-Z0-9_\.]/', '_', $_FILES['userfile']['name']);
            
            if (filesize($filename) > $max_file_size) {
                $errors[] = 'Error: File size more then max.';
            } elseif (!in_array($ext, $valid_types)) {
                $errors[] = 'Error: Invalid file type.';
            } else {
                //upload file into the current dir
                if (move_uploaded_file($filename, $upload_dir.$internalFileName)) {
                    $success = executeKettleScript($name, $hash, $mail, substr($internalFileName,0,strrpos($internalFileName, ".")), $clientID, $woOwner);
                } else {
                    $errors[] = 'Error: moving file failed.';
                }
            }
        } else {
            $errors[] = "Error: empty file.";
        }
    }
    }
}

function executeKettleScript($name, $hash, $mail, $fileName, $clientID, $woOwner) {
	$db = Core_Database::getInstance();
	$db->insert("work_order_import_kettle", array('file' => $fileName, 'username' => $name, 'hash' => $hash, 'mail' => $mail, 'company_id' => $clientID, "work_order_owner" => $woOwner));
	$dbInfo = Zend_Registry::get("DB_Info");
	if ($dbInfo["dbname"] !== "gbailey_technicianbureau") {
		if (strtolower($_GET['v']) == 'fls') {
			exec('php ../kettle/kettleScriptFls.php '.escapeshellarg($fileName).' '.escapeshellarg($name).' '.escapeshellarg($hash).' '.escapeshellarg($mail).' FLS'.' '.escapeshellarg($woOwner));
		} else {
			exec('php ../kettle/kettleScript.php '.escapeshellarg($fileName).' '.escapeshellarg($name).' '.escapeshellarg($hash).' '.escapeshellarg($mail).' '.escapeshellarg($clientID).' '.escapeshellarg($woOwner));
		}
	}
    return 'Request has been accepted. Report will be sent to '.$mail;
}

$params = array();
$sortBy = "UserName";
$sortDir = "asc";
$page = "1";
$size = "20";

$search = new Core_Api_Class;
$allClients = $search->getClientsList(
	$_SESSION['UserName'] .'|pmContext=' . $_GET['v'], $_SESSION['Password'], $params, (!empty($sortBy) && !empty($sortDir) ) ? "{$sortBy} {$sortDir}" : null, ($page - 1) * $size
);
foreach( $allClients as $client ){
	print $client->ContactName;
}   

$clientID = $_SESSION['ClientID'];
$apiGPM = new  Core_Api_Class();
$isGPM = $apiGPM -> isGPM($clientID);
?>
<script type="text/javascript" src="/library/jquery/jquery.bt.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#woOwnerTip").bt("The Work Order Owner  can choose settings to view only their Work Orders on the Work Order Dashboard, and can override Project Communication Settings with their own Contact Information.",
					 {trigger:'click',positions:'right',fill: 'white',cssStyles:{color:'black'}});

	$("#woOwnerTip").hover(
							function(){
								$(this).css("cursor","hand");
							},
							function(){
								$(this).css("cursor", "pointer");
							});
});
</script>
<style type="text/css">
	td { padding: 0px 5px; text-align: center; font-size:xx-small; }
	.errorCell { background-color: #FF8800; cursor: default; }
	#grayOutLoading {
		background-color: #999999;
		width: 100%;
		height: 100%;
		position: absolute;
		top: 0px;
		left: 0px;
		filter: alpha(opacity=50);
		opacity: 0.5;
		display: none;
	}
	#loadingMsg {
		margin: 25% auto;
		font-size: 20px;
		color: #FFFFFF;
	}
</style>

<?php if  (count($errors) > 0 ) :?>
<div id="errorDiv" style="text-align: center; background-color: #FF0000; color: #FFFFFF; margin: 10px 0px; font-weight: bold;">
    <?php foreach ($errors as $e) {echo $e.'<br />';} ?>
</div>
<?php endif;?>
<?php if  ($success != '' ) :?>
<div id="successDiv" style="text-align: center; background-color: #00AA00; color: #FFFFFF; margin: 10px 0px; font-weight: bold;">
    <?=$success?>
</div>
<?php endif; ?>
<div style="text-align: center">
<!--	<a href="<?=$_SERVER['PHP_SELF']?>?download=1&v=<?=$_GET["v"]?>">Download Import Template with parts</a> -- or -- <a href="<?=$_SERVER['PHP_SELF']?>?download=2&v=<?=$_GET["v"]?>">Download Import Template without parts</a>-->

<?php		
	$templateName = $companyID == "FLS" ? "importDocs/fls_wo_import_template.xls" : "importDocs/wo_import_template.xls";
	$instructionsName = $companyID == "FLS" ? "importDocs/New_FLS Work_Order_Import_Tool.pdf" : "importDocs/New_Work_Order_Import_Tool.pdf";	
/*
	if (isset($_GET["download"])) {
		// download attachment
		if ($_GET["download"] == 1) {
			$fname = $_GET["v"] == "FLS" ? "importDocs/fls_wo_import_template.xls" : "importDocs/wo_import_template.xls";
			header("Content-disposition: attachment; filename=$fname");
			echo file_get_contents("$fname");
		}
		else {
			$fname = $_GET["v"] == "FLS" ? "importDocs/New_FLS Work_Order_Import_Tool.docx" : "importDocs/New_Work_Order_Import_Tool.docx";
			header("Content-disposition: attachment; filename=$fname");
			echo file_get_contents("$fname");
		}
		die();
	}
	*/
?>



<div style="width: 780px; margin: 30px auto; ">
<div style="width: 300px; float: left; padding: 45px 20px 0 0; color: #e00;">
November 1, 2012: The Import Template Format has changed, please confirm all the columns match your expectations. Download the template fresh to be certain..<br/><br/>
<div style="color: #000">
For importing Work Orders with Multiple Visits, please refer to the latest <a href="<?=$instructionsName?>">Import Instructions</a>.
</div>
</div>
<div style="width: 440px; float: right;">

	<div name="importInstructions"><a href="<?=$instructionsName?>">Download Import Instructions Here</a></div>
	<div name="importTemplate"><a href="<?=$templateName?>">Download the Latest Import Template Here</a></div>		


    <form id="uploadImport" name="uploadImport" method="post" onsubmit="showLoadingMsg()" action="<?php echo $_SERVER['PHP_SELF']?>?v=<?=$_GET["v"]?>" enctype="multipart/form-data">
        <br/>
        Upload your work orders (xls format)<br/>
        <input name="userfile" type="file" size="50" /><br /><br />
   </div>
   <div style="width: 450px; float: right;">
        Work Order Owner <span class="required">*</span>
        <select name="workOrderOwner" id="workOrderOwner" style="width: 300px;">
			<?=$helper->ownerDropdownOptions($allClients->data, $_GET['v'], $_SESSION['UserName'], $isGPM);?>
        </select>
        <span title="Info"><img id="woOwnerTip" src="/widgets/css/images/info-icon.jpg"  /></span><br /><br />
        <input name="submit" type="submit" value="Submit" />
    </form>
        <div id="grayOutLoading">
    	<div id="loadingMsg">
        	Loading ...
        </div>
    </div>

</div>

</div>


</div>

<script type="text/javascript">
	function showLoadingMsg() {
		$("#grayOutLoading").show();
	}
</script>

