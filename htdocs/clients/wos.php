<?php
$page = 'clients';
$option = 'wos';
$_GET['v'] = ( isset($_GET['v']) ) ? $_GET['v'] : NULL;
$resetreturn = ( isset($_GET['resetreturn']) ) ? (int) $_GET['resetreturn'] : 1;

require_once '../headerStartSession.php';

$isDash = !empty($_GET['isDash']);
$isDash = true;
$containerName = "projectSearch";

//if (!$isDash) 
require '../header.php';
//else
// require '../headerSimple.php';
if ($_SESSION) {
    $sort = $_SESSION['Auth_Sort'];
    $fillter = $_SESSION['Auth_Fillter'];
}
$full_search = true;
if(isset($_GET['full_search']))
{
    $full_search = $_GET['full_search'];
    $sort = $_GET;
    $fillter = $_GET;
    if(isset($fillter['start_date']))
    {
       $fillter['start_date'] = str_replace('_', '/', $fillter['start_date']);
    }
    if(isset($fillter['end_date']))
    {
       $fillter['end_date'] = str_replace('_', '/', $fillter['end_date']);
    }
    if(isset($fillter['projects']))
    {
       $_projects = ""; 
       for($i = 0;$i < count($fillter['projects']);$i++)
       {
           $_projects .= ",'".$fillter['projects'][$i]."'";
       }
       if($_projects != "")
       {
           $_projects = '['.substr($_projects, 1).']';
       }
       $fillter['projects'] = $_projects;
    }
    
    //017
    if(isset($fillter['ShortNotice']))
    {
       $fillter['Short_Notice'] = $fillter['ShortNotice'];
}
    //end 017
}

?>
<script>
    var isPostBack = false;
    var dataSort = {};
    var dataFillter = {};
    if(!<?= $resetreturn ?> || !<?= $full_search ?>)
    {
        dataSort.sort1 = '<?= $sort['sort1'] ?>';
        dataSort.sort2 = '<?= $sort['sort2'] ?>';
        dataSort.sort3 = '<?= $sort['sort3'] ?>';
        dataSort.dir1 = '<?= $sort['dir1'] ?>';
        dataSort.dir2 = '<?= $sort['dir2'] ?>';
        dataSort.dir3 = '<?= $sort['dir3'] ?>';
        dataFillter.start_date = '<?= $fillter['start_date'] ?>';
        dataFillter.end_date = '<?= $fillter['end_date'] ?>';
        dataFillter.state = '<?= $fillter['state'] ?>';
        dataFillter.zip = '<?= $fillter['zip'] ?>';
        dataFillter.tech = '<?= $fillter['tech'] ?>';
        dataFillter.country = '<?= $fillter['country'] ?>';
        dataFillter.po = '<?= $fillter['po'] ?>';
        dataFillter.region = '<?= $fillter['region'] ?>';
        dataFillter.call_type = '<?= $fillter['call_type'] ?>';
        dataFillter.projects = <?= empty($fillter['projects']) ? 'null' : $fillter['projects'] ?>;
        dataFillter.route = '<?= $fillter['route'] ?>';
        dataFillter.shortNotice = '<?= $fillter['ShortNotice'] ?>';
        dataFillter.WO_ID_Mode =<?= empty($_GET['WO_ID_Mode']) ? '0' : $_GET['WO_ID_Mode'] ?>;//14139
    }
</script>
<?php
$Company_ID = (!empty($_GET['v']) ) ? $_GET['v'] : $_SESSION['Company_ID'];
$UserType = (!empty($_SESSION['UserType']) ) ? $_SESSION['UserType'] : NULL;
$isFLSManager = (strtolower($Company_ID) == 'fls' && (strtolower($UserType) === 'install desk' || strtolower($UserType) === 'manager'));
$isFLSUser = (strtolower($Company_ID) == 'fls' && (strtolower($UserType) !== 'install desk' && strtolower($UserType) !== 'manager'));

$Core_Api_User = new Core_Api_User;
$dbVersion = strtolower($Core_Api_User->getdbVersion()) == "lite" ? 'lite' : 'full';
$authData = array('login'=>$_SESSION['UserName'], 'password'=>$_SESSION['Password']);
$Core_Api_User->load($authData);
$client_id = $Core_Api_User->getClientId();
$settings = Core_DashboardSettings::getSettings($client_id);
$dashboardSettingsSavedState = json_encode("");
if ($settings['filter'] == Core_DashboardSettings::FILTER_PROJECT_SELECT || $settings['filter'] == Core_DashboardSettings::FILTER_PROJECT_MY) {
		
	$dashboardSettingsFilterProject = $settings['filter'] == Core_DashboardSettings::FILTER_PROJECT_SELECT ? Core_DashboardSettings_Project::getSettings($client_id) : Core_DashboardSettings_Project::getProjectsMy($client_id);
	$companyIDList = $dashboardSettingsFilterProject;
	if (empty($dashboardSettingsFilterProject)) $dashboardSettingsFilterProject = array();
	else $dashboardSettingsFilterProject = array_keys($dashboardSettingsFilterProject);
	$list = array();
	foreach ($dashboardSettingsFilterProject as $i=>$val) {
		if ($companyIDList[$val]["Project_Company_ID"] != $Company_ID)
			continue;
		$list[] = strval($val);
	}
	$dashboardSettingsFilterProject = $list;
}
if ($settings['sort'] == Core_DashboardSettings::SORT_RECENT) {
//	if (!isset($_COOKIE['FSWidgetDashboardState']))
		// only populate if no cookie saved
	$dashboardSettingsSavedState = empty($settings['dbState']) ? $dashboardSettingsSavedState : $settings['dbState'];
}
$dashboardSettingsFilterWorkOrder = Core_DashboardSettings_WorkOrder::getSettings($client_id);
$showOnlyMyWO = !empty($dashboardSettingsFilterWorkOrder["filter"]) ? Core_DashboardSettings_WorkOrder::isFilterActive($dashboardSettingsFilterWorkOrder["filter"], Core_DashboardSettings_WorkOrder::FILTER_OWNED) : false;

$_COOKIE["dashboardVersionDefault"] = ($dbVersion == 'lite') ? 'Lite' : 'Full';
//$dbVersion    = (isset($_POST['version']) && strtolower($_POST['version'])==='lite')?'lite':'full';
if ($isFLSManager) {
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
} else {
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
}
?>
<script>
	dashboardSettings = <?=json_encode($settings);?>;
	dashboardSettingsSavedState = <?=$dashboardSettingsSavedState?>;
	dashboardSettingsFilterProject = <?=json_encode($dashboardSettingsFilterProject)?>
    //$(document).ready(function() {

    //    if (typeof(startup) == 'function') startup();

    //});
</script>    
<?php
//if (!$isDash)
require '../navBar.php';
//else {
//	include '../buttons.php';
//	$displayPMTools = false;
//}

require 'includes/adminCheck.php';


require 'includes/PMCheck.php';
?>
<!--script type="text/javascript" src="/widgets/js/functions.js?07242012"></script-->
<?php script_nocache ("/widgets/js/functions.js"); ?>
<!--script type="text/javascript" src="/widgets/js/jsDate.js"></script-->
<?php script_nocache ("/widgets/js/jsDate.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/FSWidgetManager.js"></script>-->
<?php script_nocache ("/widgets/js/FSWidgetManager.js"); ?>
<script src="/widgets/js/jquery.iframe-auto-height.plugin.1.5.0.min.js"></script>
<script src="/widgets/js/jquery.appear.min.js"></script>
<script src="/widgets/js/jquery.ie-select-width.js"></script>
<script type="text/javascript">
    var wManager = new FSWidgetManager();
</script>

<link rel=stylesheet type="text/css" href="/widgets/css/dashContent.css" />
<link rel=stylesheet type="text/css" href="/widgets/css/main.css" />
<link rel="stylesheet" type="text/css" href="/widgets/css/popup.css" />


<?php script_nocache ("/clients/js/wintags.js"); ?>

<!-- Check if logged in, if not redirect -->
<script type="text/javascript">
    try {
        companyID = window._company      = '<?= $Company_ID ?>';
        window._isFLSManager = <?= ($isFLSManager) ? 'true' : 'false'; ?>;
        window._search       = <?= (isset($_GET['search']) && $_GET['search'] + 0) ? 'true' : 'false'; ?>;
        var test = companyID;
        if ( !isPM && companyID !== window._company ) {
            window.location.replace("<?= $_SERVER['PHP_SELF'] ?>?v=" + companyID);
        }
    } catch (e) {
        window.location.replace("./");
    }
    //  use for load images and save it in browser cache only!!!
    var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
    var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";
</script>

<script type="text/javascript" src="/widgets/js/jquery.form.js"></script>
<!--script type="text/javascript" src="/widgets/js/FSWidget.js"></script-->
<?php script_nocache ("/widgets/js/FSWidget.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSWidgetDashContentLeft.js?11222011"></script-->
<?php script_nocache ("/widgets/js/FSWidgetDashContentLeft.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSWidgetDashContentRight.js"></script-->
<?php script_nocache ("/widgets/js/FSWidgetDashContentRight.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSWidgetDashboard.js?11072012"></script-->
<?php script_nocache ("/widgets/js/FSWidgetDashboard.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSWidgetSortTool.js?11142012"></script-->
	<?php script_nocache ("/widgets/js/FSWidgetSortTool.js"); ?>
<?php if (strtolower($Company_ID) == 'fls'): ?>
    <!--script type="text/javascript" src="/widgets/js/FSWidgetSortToolFLS.js?11112012"></script-->
	<?php script_nocache ("/widgets/js/FSWidgetSortToolFLS.js"); ?>
<?php endif; ?>
<!--script type="text/javascript" src="/widgets/js/FSWidgetDeactivationTool.js"></script-->
<?php script_nocache ("/widgets/js/FSWidgetDeactivationTool.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSWidgetActivityLog.js"></script-->
<?php script_nocache ("/widgets/js/FSWidgetActivityLog.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSQuickAssignTool.js"></script-->
<?php script_nocache ("/widgets/js/FSQuickAssignTool.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script-->
<?php script_nocache ("/widgets/js/FSPopupAbstract.js"); ?>
<script type="text/javascript" src="/widgets/js/jquery-ui.js"></script>
<!--script type="text/javascript" src="/widgets/js/livevalidation.js"></script-->
<?php script_nocache ("/widgets/js/livevalidation.js"); ?>
<script type="text/javascript" src="/library/jquery/contentmenu/jquery.contextmenu.js"></script>
<link rel="stylesheet" href="/library/jquery/contentmenu/jquery.contextmenu.css" type="text/css" />
<script type="text/javascript" src="/library/jquery/jquery.bt.min.js"></script>
<!--script type="text/javascript" src="js/dashboardwo.js"></script-->
<?php script_nocache ("/clients/js/dashboardwo.js"); ?>
<?php script_nocache ("/clients/wosDetails.js"); ?>
<!--<script type="text/javascript" src="wosDetails.js"></script>-->
<script type="text/javascript" src="/widgets/js/jquery.scrollTo.js"></script>

<script type="text/javascript">
    //Default validation settings
    var LVDefaultFunction = function(){this.addFieldClass();}
    var LVDefaults = {
        onValid : LVDefaultFunction,
        onInvalid : LVDefaultFunction
    }

    var pmContext = "<?= $_GET["v"] ?>";

    function calcBasePay(){
        if ($("#Approve_Amount_Per").val() != "Hour"
           && $("#Approve_Amount_Per").val() != "Visit"
           && $("#Approve_Amount_Per").val() != "Device") return;
        var bidAmount = $("#Approve_Tech_Bid_Amount").text();
        bidAmount = String(bidAmount);
        var parts = bidAmount.split(" / ");
        bidAmount = parseFloat(parts[0]) * ($("#Approve_calculatedTechHrs").val()*1);
        $("#Approve_baseTechPay").val(bidAmount).change();
    } 
    var _menu  = [ 
        {'Open new Tab/Window':function(menuItem,menu) { 
            var href = jQuery(this).attr("href");
            if(typeof(href) != 'undefined' && jQuery.trim(href) != "") 
            {
               if(href.indexOf('?') >= 0)
               {    
                   href += "&closing=1";      
               }
               else
               {
                   href += "?closing=1";  
               }   
               var uiTab = window.open(href); 
            }
        } }
    ];
</script>

<!--script type="text/javascript" src="/widgets/js/resizable.js"></script-->
<?php script_nocache ("/widgets/js/resizable.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script-->
<?php script_nocache ("/widgets/js/FSPopupRoll.js"); ?>
<!--script type="text/javascript" src="/widgets/js/ProgressBar.js"></script-->
<?php script_nocache ("/widgets/js/ProgressBar.js"); ?>
<!--script type="text/javascript" src="/widgets/js/ProgressBarAdapterAbstract.js"></script-->
<?php script_nocache ("/widgets/js/ProgressBarAdapterAbstract.js"); ?>
<!--script type="text/javascript" src="/widgets/js/ProgressBarAdapterPopupSpin.js"></script-->
<?php script_nocache ("/widgets/js/ProgressBarAdapterPopupSpin.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSTechPayUtils.js"></script-->
<?php script_nocache ("/widgets/js/FSTechPayUtils.js"); ?>

<link rel="stylesheet" href="/widgets/css/resizable.css" type="text/css" />

<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>

<link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" media="all" />

<script type="text/javascript">
    /**
     *  calendarInit
     *
     *  Display filter post handler
     */
    function calendarInit() {
        $.map(['filterStartDate', 'filterEndDate'], function( id ) {
            $('#' + roll.container().id + ' #' + id).focus( function() {
                $('#calendar_div').css('z-index', ++roll.zindex);
                $(this).calendar({dateFormat:'MDY/'});
            });
        });
    }
    /**
     * calendarMove
     *
     * @param event  event
     * @access public
     * @return void
     */
    function calendarMove( input ) {
        $('#calendar_div').css('top', $(input).offset().top + $(input).height() + 6 + 'px');
        $('#calendar_div').css('left', $(input).offset().left + 'px');
        popUpCal.showFor(input);
    }
    function toggleChecks(cbox)
    {
        switch ( wd.currentTab ) {
            case 'assigned' :
                $.each($('#widgetContainer input[name="rePublishCheckbox"]'), function (idx, item){ 
                    item.checked =  cbox.checked;
                    var win = /^rePublishCheckbox_(\d+)$/.exec($(item).attr('id'))[1];

                    if ( item.checked ) {
                        $("#rePublishOptions_" + win).removeClass('displayNone');
                    } else {
                        $("#rePublishOptions_" + win).addClass('displayNone');
                    }
                });
                break;
            default :
                $.each($('#widgetContainer input[name="approveCheckbox"]'), function (idx, item){ item.checked = cbox.checked; });
                break;
        }
    }
    /**
     * doApprove
     *
     * @access public
     * @return void
     */
    function doApprove() {
        var wos = [];
        var _url;
        $.each($('#widgetContainer input[name="approveCheckbox"]'), function (idx, item){ if (item.checked) wos.push($(item).val()); });

        var payAmounts = {};

        switch ( wd.currentTab ) {
            case 'workdone' :
            case 'incomplete' :
                _url = '/widgets/dashboard/do/approve/';
                progressBar.key('approve');

                var wosCount = wos.length;
                for (var i = 0; i < wosCount; i++) {
                    var wId = wos[i];
                    payAmounts[wId] = $("#approvePayAmount_" + wId).val();
                }
                break;
            case 'created'  :
                _url = '/widgets/dashboard/do/publish/';
                progressBar.key('publish');
                break;
            default         : return false; //  Return from function on any other tabs
        }

        if ( wos.length ) {
            detailObject.onInit.checkApprovedButton({
               wos :  wos,
               el : {payAmounts:payAmounts,company:pmContext,wd:wd,wos:wos},
               onSubmit : function(el){
            progressBar.run();

            $.ajax({
                url         : _url,
                dataType    : 'json',
                data        : { wo_ids : wos, payAmounts: payAmounts, company: pmContext},
                cache       : false,
                type        : 'post',
                context     : wd,
                success     : function (data, status, xhr) {
                    this.show({ tab:this.currentTab, params:this.getParams() });
                },
                error       : function ( xhr, status, error ) {
                    progressBar.stop();
                    this.show({ tab:this.currentTab, params:this.getParams() });
                }
            });
        }
            });
    }
    }
    /**
     * toggleTab
     *
     * Switch Tabs
     *
     * @param string name Name of currect Tab
     * @param object link Tab's link object
     * @param object wd wd object
     * @return false always
     */
    var hasData = false;
    function toggleTab( name, link, wd, withPage ) {
        /* Disable full search */
        var attributeId = jQuery(link).attr("attributeId");
        //wd.ajaxSuccess = function() { }; 
        $.post("/widgets/dashboard/client/accessctl/", {
            rt:'iframe',
            attributeId:attributeId
        },
        function (response) {
            if(response=="1") { 
                hasData = true;
                if ( name !== 'all' && (window._search || wd.filters().fullSearch() == 'true' || wd.filters().fullSearch() === true || wd.returnAllSearch)) {
                    wd.resetFilters();
                    window._search = false;
                    wd.returnAllSearch = false;
                }

                if (withPage == undefined) withPage = false;
                if (wd.currentTab != name) {
                    //wd.resetFilters();
                    if(typeof(wd._dashboardwo) != 'undefined') wd._dashboardwo.fb = true;
                    wd.deactivaAll = null;
                    wd.params.isBid = 0;
                    wd.currentTab = name;
                    wd.sortTool().prepareItems(version);
                    wd.sortTool().initSortTool(); 
                    wd.params.ajaxSuccess = null;
					params = wd.getParams(withPage);
					dbSortState = wd.sortTool().getSortSession();
					if (wd.sortTool()._useSessionDefault && dbSortState[wd.currentTab]) {
						params.sort1 = dbSortState[wd.currentTab].sort;
						params.sort2 = dbSortState[wd.currentTab].sort2;
						params.sort3 = dbSortState[wd.currentTab].sort3;
						params.dir1 = dbSortState[wd.currentTab].dir;
						params.dir2 = dbSortState[wd.currentTab].dir2;
						params.dir3 = dbSortState[wd.currentTab].dir3;
					}
					wd.setParams(params);
                }

                var ajaxSuccess = null;
                wd.show({tab:wd.currentTab,params:wd.getParams(withPage)});
                link.blur();
                $("#toppaginatorContainer").html("");

                //set default state, then add custom classes.

                $("#MassApproveBtn").addClass("displayNone");
                $("#deactivationButton").addClass("displayNone");
                $("#MassAssignBtn").addClass("displayNone");
<?php if ($isFLSManager) : ?>
                    $('#paperworkBtn').addClass("displayNone");
<?php endif; ?>
                $('#MassPublishBtn').addClass("displayNone");
                $('#dashboardVersion').removeAttr('disabled');
                $('MyTechsBtn').addClass("displayNone");
                jQuery('.toolGroup').css('display',"");
                switch ( name ) {
<?php if (!$isFLSManager): ?>
                        case 'created':
                            $('#MassApproveBtn').removeClass("displayNone").val('Mass Publish');
                            $('#QuickAssignBtn').removeClass("displayNone");
                            break;
                        case 'workdone' :
                            $('#MassApproveBtn').removeClass("displayNone").val('Mass Approve');
                            break;
                        case 'incomplete' :
                            $('#MassApproveBtn').removeClass("displayNone").val('Mass Approve');
                            break;
                        case 'published':
                            $('#MassAssignBtn').removeClass("displayNone");
                            break;
                        case 'assigned':
                            $('#MassPublishBtn').removeClass("displayNone");
                            break;
<?php else: ?>
                            case 'workdone' :
                                $('#paperworkBtn').removeClass("displayNone");
                                break;
<?php endif; ?>
                            case 'deactivated':
                                $('#dashboardVersion').attr('disabled', 'disabled');
                                $('#deactivationButton').removeClass("displayNone");
                                break;
                            }
                        } 
                        else {
                            if(!hasData)
                            {
                                jQuery('.toolGroup').css('display',"none");
                            }
                
                            Attention(response);
                        }                        
                    });
        
       
                    return false;
                }

                var wd;             //  Widgets object
                var roll;           //  Rollover object
                var popup;          //  Rollover object
                var progressBar;    //  Progress bar object
                var activityLog;    //  Activity Log object

                function saveDBState() {
                    p = wd.getParams();
                    p.page = wd.sortTool()._page;
					dbStateSort = wd.sortTool().getSortSession();
                    try {
                        $.cookie('FSWidgetDashboardState', JSON.stringify({tab: wd.currentTab, params: p, paramsSort: dbStateSort }));
						$.ajax({
							url         : "/clients/ajax/saveDBState.php",
							async		: false,
							cache       : false,
							type        : 'post'
						});
                    } catch (e) {}
                }

                try {
                    window.onbeforeunload = function() {
                        try {
                            saveDBState();
                        } catch (e) {}
                        return;
                    };
                } catch (e) {
                    $(window).unload(function() {
                        try {
                            saveDBState();
                        } catch (e) {}
                    });
                }

                function startup() {
                    var _ajaxSuccess = function(fun,_el,data) { 
                            var attributeId = 0;
                            if(typeof(_el) != 'undefined'){
                               attributeId = _el.getAttentionID(_el.currentTab);
                            }
	                    $.post("/widgets/dashboard/client/accessctl/", {
                            rt:'iframe',
                            attributeId:attributeId
                        }, 
                        function (response) {

                            if(response=="1") { 
                                //wd = new FSWidgetDashboard({container:'widgetContainer',tab:'workdone',dbVersion:'<?= $dbVersion ?>',saveStateCallback: saveDBState});    
                                hasData = true;
                                if(typeof(fun) =='function')
                                {
                                    fun(_el,data);
                                }
                                return false;
                            } 
                            else {
                                jQuery("#widgetContainer").html("");
                                jQuery('.toolGroup').css('display',"none");
                                Attention(response); 
                                return true;
                            }   
                            /* Find tag in URL params */
                        }); 
                    };
                    var tab = '<?= (!empty($_GET['tab'])) ? strtolower($_GET['tab']) : '__invalid__'; ?>';
                    var projectid = '<?= (!empty($_GET['projectid'])) ? strtolower($_GET['projectid']) : ''; ?>';
                    
                    dataFillter.projects = projectid;
                     <?php
                      
                        if(isset($_GET['AuditComplete'])) 
                            echo "dataFillter.AuditComplete = '".$_GET['AuditComplete']."';";
                        if(isset($_GET['AuditNeeded'])) echo "dataFillter.AuditNeeded = '".$_GET['AuditNeeded']."';";
                    ?>
                    var isBid = 0;
                    if(projectid != '')
                    {
                          isBid = '<?= (!empty($_GET['techid'])) ? strtolower($_GET['techid']) : ''; ?>';  
                    }
                    
                    //017
                    var ShortNotice = '<?= (!empty($_GET['ShortNotice'])) ? strtolower($_GET['ShortNotice']) : ''; ?>';
                    dataFillter.short_notice = ShortNotice;
                    //end 017
                    
                    wd = new FSWidgetDashboard({
                        container:'widgetContainer',
                        tab:'workdone',
                        dbVersion:'<?= $dbVersion ?>',
                        isBid:isBid,
                        saveStateCallback: saveDBState,
                        ajaxSuccess:_ajaxSuccess});  
                    wd.setParamsSession(dataFillter);
                    if ( wd._isValidTab(tab) ) {
                        wd.currentTab = tab;
                    }
                    /* Find tab in URL params */

                    roll        = new FSPopupRoll();
                    progressBar = new ProgressBar();
                    activityLog = new FSWidgetActivityLog( roll );

                    wd.htmlFilters();
<?php if (strtolower($Company_ID) == 'fls') : ?>
                        wd.sortTool(new FSWidgetSortToolFLS('sortSelect', 'directionSelect', wd, roll, 'quick', <?=json_encode($settings['sort'] == Core_DashboardSettings::SORT_RECENT)?>));
<?php else : ?>
                        wd.sortTool(new FSWidgetSortTool('sortSelect', 'directionSelect', wd, roll, 'quick', <?=json_encode($settings['sort'] == Core_DashboardSettings::SORT_RECENT)?>));
<?php endif; ?>
					<?php if ($settings['sort'] == Core_DashboardSettings::SORT_DEFAULT_MY):?>
							try {
								wd.sortTool().setMyDashboardDefaultByTab(dashboardSettings);
								wd.sortTool().prepareItems("<?=$dbVersion?>");
								wd.sortTool().initSortTool();
								wd.sortTool().update();
							} catch (e) {}
					<?php endif;?>
                    
                    wd.deactivationTool(new FSWidgetDeactivationTool(wd,roll));



                    /*  Load tab by parameters in URL */
                    savedState = $.cookie('FSWidgetDashboardState');
                    parse = wd.parseUrl();
                    size = 0;
                    for (key in parse) {
                        ++size;
                    }
                    if (size > 1) savedState = null;
					<?php if ($settings['sort'] == Core_DashboardSettings::SORT_RECENT && !empty($settings['dbState'])):?>
					try {
						wd.sortTool().setSortSession(dashboardSettingsSavedState.paramsSort);
						$(wd.sortTool()._sortSelect).val(dashboardSettingsSavedState.paramsSort[wd.currentTab].sort);
						$(wd.sortTool()._directionSelect).val(dashboardSettingsSavedState.paramsSort[wd.currentTab].dir);
						if (wd.sortTool()._useSessionDefault && dashboardSettingsSavedState.paramsSort[wd.currentTab]) {
							parse.sort1 = dashboardSettingsSavedState.paramsSort[wd.currentTab].sort;
							parse.sort2 = dashboardSettingsSavedState.paramsSort[wd.currentTab].sort2;
							parse.sort3 = dashboardSettingsSavedState.paramsSort[wd.currentTab].sort3;
							parse.dir1 = dashboardSettingsSavedState.paramsSort[wd.currentTab].dir;
							parse.dir2 = dashboardSettingsSavedState.paramsSort[wd.currentTab].dir2;
							parse.dir3 = dashboardSettingsSavedState.paramsSort[wd.currentTab].dir3;
							wd.sortTool().update();
						}
					} catch (e) {}
					<?php endif;?>
					if (!savedState) {
                        wd.setParams(parse);
						<?php if ($settings['filter'] == Core_DashboardSettings::FILTER_PROJECT_SELECT || $settings['filter'] == Core_DashboardSettings::FILTER_PROJECT_MY):?>
						wd.setProjectDefault(dashboardSettingsFilterProject);
						wd.filters().setProject(dashboardSettingsFilterProject);
						<?php endif;?>
						<?php if (!empty($_SESSION['UserName']) && $showOnlyMyWO): // default owner in dashboard filter?>
						wd.filters().Owner(<?=json_encode($_SESSION['UserName'])?>);
						<?php endif;?>
                        toggleTab(wd.currentTab, $('#'+wd.currentTab), wd);
                    }
                    /*  Load tab by parameters in URL */

                    else {
                        $.cookie('FSWidgetDashboardState', null);
						if (savedState)
	                        state = JSON.parse(savedState);
						else {
							state = {};
							state.params = parse;
							state.params.page = null;
							state.params.tab = wd.currentTab;
						}
                        wd.setParams(state.params);
                        wd.currentTab = state.params.tab;
                        //            wd.sortTool().prepareItems(version);
                        wd.sortTool().initSortTool();
                        $('#sortSelect').val(state.params.sort1);
                        $('#directionSelect').val(state.params.dir1);
                        wd.sortTool().update();
                        wd.sortTool().setPage(state.params.page);
                        try {
                            // IE 7 workaround
                            a = typeof state.params.projects[0];
                            if (a == 'function') {
                                state.params.projects.reverse();
                                state.params.projects.pop();
                                state.params.projects.reverse();
                            }
							wd.filters().setProject(state.params.projects);
                        } catch (e){}
                        //		wd.show(state);
                        toggleTab(wd.currentTab, $('#'+wd.currentTab), wd, true);

                        //	        toggleTab(state.tab, $('#'+state.tab), wd);
                    }
                    progressBar.key('approve');
                    progressBar.adapter(new ProgressBarAdapterPopupSpin(progressBar, null));
                    jQuery("#widgetContainer").html("");
        
                    if(dataSort.sort1 !== "" && typeof(dataSort.sort1) != 'undefined')
                    {
                        jQuery("#sortSelect").val(dataSort.sort1);  
                        jQuery("#directionSelect").val(dataSort.dir1);  
                    }

                    loadContent ();

                    return false;
        		}

                function reloadTabFrame() {
                    wd.show({tab: wd.currentTab, params: wd.getParams(true)});
                }

                function ShowCheckin (el,win,clientid,techid,fname,lname,Reviewed,TechCheckedIn24hrs,CheckedIn)
                {
                    $.ajax({
                        url : "/widgets/dashboard/popup/client-checkin/",
                        data : {win:win,clientid:clientid,techid:techid,fname:fname,lname:lname,reviewed:Reviewed,techcheckedin24hrs:TechCheckedIn24hrs,checkedin:CheckedIn},
                        cache : false,
                        type : 'POST',
                        dataType : 'html',
                        context : this,
                        error : function (xhr, status, error) {
                            $('body').css('cursor', '');
                            if ( xhr.responseText === "authentication required" ) {
                                document.location = "/clients/logIn.php";
                            } else {
                                FSWidget.fillContainer("Sorry, but an error occured. No results found.", this);
                            }
                            this.tabOpenXhrRequest = null;
                        },
                        success : function(data, status, xhr) {
                            $("<div></div>").fancybox(
                            {
                                'autoDimensions' : true,
                                'showCloseButton' : true,
                                'hideOnOverlayClick' : false,
                                'enableEscapeButton':false,
                                'content': data
                            }
                            ).trigger('click');
                            
                            jQuery("#cmdSaveclientcheckin").click(function() { 
                                var checkindate = jQuery("#edit_Date_Checked_in").val();;
                                var checkintime = jQuery("#checkinTime").val();
                                var checkoutdate = jQuery("#edit_Date_Checked_out").val();
                                var checkouttime = jQuery("#checkoutTime").val();
                                var Reviewed = jQuery("#chktechaccepted").is(':checked')?1:0;
                                var TechCheckedIn24hrs = jQuery("#chktechConfirmed").is(':checked')?1:0;
                                var CheckedIn = jQuery("#chktechchecked").is(':checked')?1:0;
                               $.ajax({
                                    url : "/widgets/dashboard/popup/save-client-checkin/",
                                    data : {
                                        win:                win,
                                        reviewed:           Reviewed,
                                        techcheckedin24hrs: TechCheckedIn24hrs,
                                        checkedin:          CheckedIn,
                                        checkindate:        checkindate,
                                        checkintime:        checkintime,
                                        checkoutdate:       checkoutdate,
                                        checkouttime:       checkouttime
                                        },
                                    cache : false,
                                    type : 'POST',
                                    context : this,
                                    success : function(data) {
                                        if(data.success){
                                            reloadTabFrame();
                                            $.fancybox.close();
                                        }else{
                                            var msg = '';
                                            for(var i = 0; i < data.errors.length; i++){
                                                msg += data.errors[i]+"<br>";
                                            }
                                            jQuery("#divError").html(msg);
                                        }
                                    }
                                }); 
                            });
                            
                            jQuery("#cmdcancelclientcheckin").click(function() { 
                                $.fancybox.close();
                            });
                        }
                })
            }

</script>

<input type="hidden" id="containerName" name="containerName" value="<?=$containerName?>" />
<div id="container" class="use-sidebar-right use-sidebar-left">
    <div id="dashleftcontent"></div>
    <div id="header" style="width: 100%">
        <div id="wrapper">
            <div id="content" align="center"></div>
        </div>

        <div style="display: none;" onclick="javascript:dl.separatorClick(this);" id="separatorLeftOutside">
            <a id="separatorLeft"></a>
        </div>
        <div style="display: none;" onclick="javascript:dr.separatorClick(this);" id="separatorOutside">
            <a id="separatorRight"></a>
        </div>
        <!-- DASHBOARD FILTER -->
        <div id="projectSearch" onclick="return true;">
            <form id="dashFilter" name="dashFilter" action="<?= $_SERVER['PHP_SELF'] ?>?v=<?= $_GET["v"] ?>" method="post">
                <input type="hidden" name="WO_ID_Mode" value="<?= $_GET['WO_ID_Mode']?>" />
                <div id="contents">
                    <?php require("includes/getWOCount.php"); ?>

                    <div class="toolGroup">
                        <div class="toolGroupLeft" style="min-width: 650px">
                            <div class="toolBtn">
                                <input type="button" id="cmdMassAssignBtn" value="Mass Update" style="display:none;" class="link_button displayNone" />
                                <input type="button" id="MassAssignBtn" value="Mass Update" class="link_button displayNone" />
                            </div>
                            <div class="toolBtn">
                                <input type="button" id="MassApproveBtn" value="Mass Approve" class="link_button displayNone" />
                            </div>
                            <div class="toolBtn">
                                <input type="button" id="MassPublishBtn" value="Mass Publish" class="link_button displayNone" />
                            </div>
                            <div class="toolBtn">
                                <input id="viewFiltersButton" type="button" value="Filter" class="link_button" />
                            </div>
                            <div class="toolBtn">
                                <input id="sortButton" type="button" value="Sort" class="link_button" />
                            </div>
                            <div class="toolBtn">
                                <input id="deactivationButton"type="button" value="View Deactivation Reason" class="link_button wide_button" />
                            </div>
                            <div class="toolBtn">
                                <input id="downloadButton" type="button" value="Download" class="link_button download_button" />
                            </div>
                            <?php if($_SESSION['Auth_User']['isPM']){ if (!$isFLSManager) : ?>
                            <div class="toolBtn">
                                <input id="mapperButton" type="button" value="Map Work Orders" class="link_button" />
                            </div>
                            <?php endif; } ?>
                            
                            <?php if ($isFLSManager) : ?>
                                <div class="toolBtn">
                                    <input id="paperworkBtn" type="button" value="Paperwork Received" class="link_button middle_button" />
                                </div>
                            <?php endif; ?>

                            <!--div class="toolBtn">
                                <input id="activityLogButton" type="button" value="View Activity Log" class="link_button" />
                            </div-->

                        </div>
                        <div class="toolGroupRight">
                            <div class="toolBtn topSm">
                                Quick Sort
                            </div>
                            <div class="toolBtn">
                                <select id="sortSelect" onchange="return wd.sortTool().applySort('quick');"></select>
                            </div>
                            <div class="toolBtn" style="margin:0;">
                                <select id="directionSelect" onchange="return wd.sortTool().applySort('quick');"></select>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                                    function viewDeactivationReason(win) {
                                        wd.deactivationTool().setWin(win);
                                        $("#deactivationButton").click();
                                    }
                                    var rightNavWidget;

                                    var checkMinWidth = function(){
                                        if(objMain.hasClass('use-sidebar-left') && !objMain.hasClass('use-sidebar-right')){

                                            $("#mainContainer").css("width", "100%");
                                            $("#header").css("width", "77%");
								
                                        }
                                        if(objMain.hasClass('use-sidebar-left') && objMain.hasClass('use-sidebar-right')){

                                            $("#mainContainer").css("min-width", "950px");
                                            $("#header").css("width", "54%");
                                        }

                                        if(objMain.hasClass("use-sidebar-right") && !objMain.hasClass("use-sidebar-left")){

                                            $("#mainContainer").css("min-width", "950px");
                                            $("#header").css("width", "77%");
                                        }

                                        if(!objMain.hasClass("use-sidebar-right") && !objMain.hasClass("use-sidebar-left")){

                                            $("#mainContainer").css("min-width", "750px");
                                            $("#header").css("width", "100%");
                                        }
                                    };

                                    //$(document).ready(function() {
                                    function loadContent () {
		
                                        objMain = $("#container");

                                        dr = new FSWidgetDashContentRight({container:'dashrightcontent', objMain: objMain, checkMinWidth: checkMinWidth, company:window._company});
<?php if (!$isFLSManager): ?>
                        dr.show();
                        dr.separatorClick();
<?php endif; ?>
                    dl = new FSWidgetDashContentLeft({container:'dashleftcontent', objMain: objMain, dr: dr, checkMinWidth: checkMinWidth, company:window._company});
<?php if (!$isFLSManager): ?>
                        dl.show();

                        dl.separatorClick();
<?php endif; ?>

                    //		rightNavWidget = new FSWidgetDashboard({container:'rightsidebar',tab:'rightside',customfunctions:{showPreloader:function(){}}},null);
                    //        rightNavWidget.show({tab:'rightside',params:{companyId:window._company,container:'rightsidebar'}});


		
                    $("#activityLogButton").click(function(event) {
                        roll.autohide(false);
                        var opt = {
                            width       : 604,
                            height      : '',
                            title       : 'View Activity Log',
                            body        : activityLog.buildHtml(),
                            context     : activityLog,
                            handler     : activityLog.initActivityLogTool
                        };
                        roll.showNotAjax(this, event, opt);
                    });
<?php /* if ($isFLSManager) : */ ?>
                    /*$("#QuickAssignBtn").click(function(event) {
                                roll.autohide(false);
                                var opt = {
                                    popup       : 'quickassigntool',
                                    title       : 'Quick Assign',
                                    width       : 400,
                                    preHandler  : assignTool.getWidgetHtml,
                                    postHandler : assignTool.initEvents,
                                    context     : assignTool,
                                    delay       : 0
                                }
                                roll.show(this,event,opt);
                            });*/

                    /*$("#QuickApproveBtn").click(function(event) {
                                roll.autohide(false);
                                var opt = {
                                    popup       : 'quickapprove',
                                    title       : 'Quick Approve',
                                    width       : 400,
                                    context     : wd,
                                    preHandler  : wd.getWidgetHtml,
                                    delay       : 0,
                                    postHandler : function() {
                                            $("#Approve_approveForm").ajaxForm({dataType: 'json', success: function(data) {
                                            if (data.error == 0) {
                                                roll.hide();
                                                reloadTabFrame();
                                            } else {
                                                $('#woErrorMsg').html(data.msg);
                                                $('#woErrorMsg').show('fast',function(){
                                                    setTimeout(function () { $('#woErrorMsg').hide(500); }, 3000);
                                                });
                                            }
                                                    }, beforeSubmit: function() {
                                                    progressBar.key('updatepayfinan');
                                                    progressBar.run();
                                                    return true;
                                                    }});
                                            }
                                }
                                roll.show(this,event,opt);
                            });*/
                    $("#paperworkBtn").click(function(event) {
                        params = [];
                        $("input[name=paperwork_received_data]:checked").each(function(index,value){params[index]=$(value).val()});
                        if (params.length>0) {
                            roll.autohide(false);
                            var opt = {
                                width       : 300,
                                height      : '',
                                position    : 'middle',
                                title       : 'Processing',
                                body        : "<div class=\"tCenter\"><img alt='Wait...' src='/widgets/images/progress.gif' /></div>"
                            };
                            roll.showNotAjax(null, null, opt);
                            $.ajax({
                                url:'/widgets/dashboard/do/receive-paperwork/',
                                cache:false,
                                type:'POST',
                                data: {'ids':params,'company':window._company},
                                success: function(data) {
                                    roll.hide();
                                    reloadTabFrame();
                                }
                            });
                        }
                    });

<?php /* endif; */ ?>
                    $("#MassApproveBtn").click(function (event) {
            
                        $.post("/widgets/dashboard/client/accessctl/", 
                        {rt:'iframe',attributeId:"8,2"}, 
                        function (response) {
                            if(response=="1") { 
                                var Finan_PayAmount = 1;
                                $('#widgetContainer input[name="approveCheckbox"]').each(function(){
                                    var val = jQuery(this).val();
                                    if(jQuery(this).is(":checked") && (
                                        jQuery("#approvePayAmount_"+val).val() == "--"
                                        || jQuery("#approvePayAmount_"+val).val() == ""
                                        || jQuery("#approvePayAmount_"+val).val() == "0.00"
                                        || jQuery("#approvePayAmount_"+val).val() == 0)){
                                        Finan_PayAmount = 0;
                                        return false;
                                    }
                                });
                                detailObject.onInit.ConfirmApproveWithFinalTotalTechByZero(function(){
                                    doApprove();
                                },
                                Finan_PayAmount,
                                true);
                            } 
                            else { Attention(response); }                        
                    });
                    });
        
                    $("#cmdMassAssignBtn").click(function (event) {
                        $.post("/widgets/dashboard/client/accessctl/", 
                            {rt:'iframe',attributeId:3}, 
                            function (response) {
                                if(response=="1") { 
                                    assignTool.massAssign(function(xml){
                                            if(xml != null){
                                                if(xml.error){
                                                    var msg = "";
                                                    if(xml.ext.length <= 0){
                                                        msg = xml.msg;
                                                    }else{
                                                        for(var j =0; j< xml.ext.length;j++){
                                                            msg +="<div style='color:black;'>Tech #"+xml.ext[j].tech+" for Win #"+xml.ext[j].win+":</div><div>"+xml.ext[j].errMsg+"</div>";
                                                        }
                                                    }
                                                    $("<div></div>").fancybox(
                                                    {
                                                        'autoDimensions' : true,
                                                        'showCloseButton' :true,
                                                        'hideOnOverlayClick' : false,
                                                        'enableEscapeButton':false,
                                                        'content': "<div style='padding:6px;color:red;'>"+msg+"</div>"
                                                    }
                                                    ).trigger('click');
                                               }
                                            }
                                            assignTool._wd.show({
                                                tab:assignTool._wd.currentTab,
                                                params:assignTool._wd.getParams()
                                    });
                                        });
                                } 
                                else { Attention(response); }                        
                        });
                    });
					
                    $("#MassPublishBtn").click(function(event) {
                        $.post("/widgets/dashboard/client/accessctl/", 
                        {rt:'iframe',attributeId:2}, 
                        function (response) {
                            if(response=="1") { 
                                assignTool.massPublish();
                            } 
                            else { Attention(response); }                        
                        });
            
                    });
//                    wd.setParamsSession(dataFillter);
                    $("#viewFiltersButton").click(function(event){
                        roll.autohide(false);
                        var opt = {
                            width       : 325,
                            height      : 380,
                            title       : 'Filter By',
                            context     : wd,
                            handler     : wd.prepareFilters,
                            postHandler : calendarInit,
                            body        : $(wd.filters().container()).html()
                        };
                        roll.showNotAjax(this, event, opt);
                    });

                    $("#downloadButton").click(function(event){
                        var opt = wd.getParams();
                        opt['download']='csv';
                        wd.open({
                            tab     : wd.currentTab,
                            params  : opt
                        });
                    });

                    $("#sortButton").click(function(event){
                        wd.sortTool().switchSortTools('full');
                        roll.autohide(false);
                        var opt = {
                            width   : 325,
                            height  : 225,
                            title   : 'Sort Work Orders...',
                            context : wd.sortTool(),
                            handler : wd.sortTool().initSortPopup,
                            body    : wd.sortTool().makeFullSortPopup()
                        };
                        roll.showNotAjax(this,event,opt);
                    });

                    $("#deactivationButton").click(function(event){
                        roll.autohide(false);
                        var opt = {
                            width       : 325,
                            height      : '',
                            title       : 'View Deactivation Reason',
                            context     : wd.deactivationTool(),
                            body        : wd.deactivationTool().getPopupHTML(),
                            postHandler : wd.deactivationTool().initAutofill
                        };
                        roll.showNotAjax(this,event,opt);
                    });

                    $("#mapperButton").click(function(event){
                        var opt = wd.getParams();
                        opt['download'] = 'json';
                        opt['size'] = '1000';
                        url = wd.open({
                            tab     : wd.currentTab,
                            params  : opt
                        });
						
                        $.getJSON(url,function(data){
                           data = $(data).toArray();
                           var wos = [];
                           $.each(data,function(key,winNum){
                               wos.push(winNum);
                           });

                           var form = document.createElement("form");
                           form.setAttribute("method", "post");
                           form.setAttribute("action", "/clients/techs.php?v="+pmContext);

                           var hiddenField = document.createElement("input");
                           hiddenField.setAttribute("type", "hidden");
                           hiddenField.setAttribute("name", "mapperWos");
                           hiddenField.setAttribute("value", wos.join(","));
                           form.appendChild(hiddenField);

                           filterSendParams = ["start_date", "end_date", "projects", "route", "tab"];
                           $.each(opt,function(k,v){
                               if($.inArray(k,filterSendParams) > -1){
                                   if(v === "workdone")v= "Work Done";
                            	   var hiddenField = document.createElement("input");
                                   hiddenField.setAttribute("type", "hidden");
                                   hiddenField.setAttribute("name", k);
                                   hiddenField.setAttribute("value",v);
                                   form.appendChild(hiddenField);
                               }
                             });

                           document.body.appendChild(form);  
                           form.submit();
                        });
                    });

                //});
                                    }
                    </script>

                    <br class="clear" />
                    <div id="tabbedMenu" style="height:16px;"><ul>
                            <li id="todayswork"><a attributeId="20"  href="javascript:void(0)" onclick="toggleTab('todayswork', this, wd);">Today&rsquo;s Work</a></li>
                            <li id="created"><a attributeId="14" classs href="javascript:void(0)" onclick="toggleTab('created', this, wd);">Created</a></li>
                            
                            <li id="published"><a attributeId="15" href="javascript:void(0)" onclick="toggleTab('published', this, wd);">Published</a></li>
                            <li id="assigned"><a attributeId="16" href="javascript:void(0)" onclick="toggleTab('assigned', this, wd);">Assigned</a></li>
                            <li id="workdone"><a attributeId="17" href="javascript:void(0)" onclick="toggleTab('workdone', this, wd);"><nobr>Work Done</nobr></a></li>
                            <li id="incomplete"><a attributeId="18" href="javascript:void(0)" onclick="toggleTab('incomplete', this, wd);">Incomplete</a></li>
                            <li id="completed"><a attributeId="19" href="javascript:void(0)" onclick="toggleTab('completed', this, wd);">Completed</a></li>
                            <li id="all"><a attributeId="20" href="javascript:void(0)" onclick="toggleTab('all', this, wd);">All</a></li>
                            <li id="deactivated" style="margin-left:10px;"><a attributeId="21" h href="javascript:void(0)" onclick="toggleTab('deactivated', this, wd);">Deactivated</a></li>
                        </ul>
                        <script>
                                        function onChangePageSize()
                                        {
                                            wd.sortTool().setPage(1);
                                            reloadTabFrame();
                                        }
                        </script>
                        <div id="toppaginatorSizeContainer"></div>
                        <table cellspacing='0' cellpadding='0' class="paginator_top"><tr><td class="tCenter" id="toppaginatorContainer"></td></tr></table>

                    </div>
                </div>
                <br class="clear" />

                <div id="widgetContainer"></div>

        </div>
        </form>
    </div>
    <div id="dashrightcontent"></div>
</div></div>
<!-- End Content -->
<script>
    //$(document).ready(function() {

        if (typeof(startup) == 'function') startup();

    //});
</script>    
<?php require ("../footer2.php"); ?>
