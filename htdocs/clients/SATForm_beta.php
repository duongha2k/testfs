<?php
    require_once("../headerSimple.php");
    $displayPMTools = false;
    require_once("includes/PMCheck.php");

    $woID = isset($_GET["id"]) ? $_GET["id"] : null;
    $techID = isset($_GET["id"]) ? $_GET["techID"] : null;
    $companyID = isset($_GET["v"]) ? $_GET["v"] : null;
?>


<script type="text/javascript" src="/widgets/js/jquery.min.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.scrollFollow.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.cookie.js"></script>

<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetSAT.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-mod.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
<link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" media="all" />

<script type="text/javascript">
    $(function(){
        widgetSAT = new FSWidgetSAT({container : 'plhFSWidgetSAT', data : { id : '<?=$woID?>', techID : '<?=$techID?>', companyID : '<?=$companyID?>'} });
        widgetSAT.show();
    })
</script>

<div id="plhFSWidgetSAT"></div>