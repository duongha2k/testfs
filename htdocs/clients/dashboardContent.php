<?php

/*$search = new Core_Api_Class();
$projects = $search->getAllProjects($_SESSION['UserName'].'|pmContext='.$_GET['v'],$_SESSION['Password']);


$login =  $_SESSION['UserName'].'|pmContext='.$_GET['v'];
$pass = $_GET['v'] . $_SESSION['Password'];
$fields = 'WIN_NUM, Tech_FName, Tech_LName, lastModDate, WO_ID';
$blank = "";
$page = "1";
$numRecords = "10";
$sort = "lastModDate DESC";
$filter = new API_WorkOrderFilter;

$wo = $search->getWorkOrders(
            $login,
            $pass,
            $fields,
            $blank,
            $blank,
            $blank,
            $filter,
            $page,
            $numRecords,
            $countRows,
            $sort
        );


$techs = $search->getMyRecentTechIds($_SESSION['UserName'],$_SESSION['Password'], $_GET['v']);*/

$installManager = FALSE;
if($_SESSION['UserType'] == "Install Desk" || $_SESSION['UserType'] == "Manager")
	$installManager = TRUE;
?>
<script src="/widgets/js/jquery.iframe-auto-height.plugin.1.5.0.min.js"
	type="text/javascript"></script>
<script src="/widgets/js/jquery.appear.min.js" type="text/javascript"></script>
<script src="/widgets/js/jquery.ie-select-width.js"
	type="text/javascript"></script>
<?php script_nocache ("/widgets/js/ajaxfileupload.js"); ?>
<?php script_nocache ("/widgets/js/FSWidgetDashContentLeft.js"); ?>
<?php script_nocache ("/widgets/js/FSWidgetDashContentRight.js"); ?>
<script type="text/javascript"
	src="../library/jquery/interface_floatingWindows.js"></script>
<script type="text/javascript"
	src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript"
	src="/widgets/js/jquery.formatCurrency-1.4.0.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.scrollFollow.js"></script>
<?php script_nocache ("/clients/js/wintags.js"); ?>

<script type="text/javascript" language="javascript">

 function truncateText(obj){

	var moreText = "...[Read More]";
	var lessText = "...[Hide Text]";
		
	var moreLink = $(obj);
	var text = $(obj).parent().find('#moreText');
	
	if(moreLink.text() == moreText) {
		moreLink.text(lessText);
		text.css("display", "inline");
	} else {
		moreLink.text(moreText);
		text.css("display", "none");
	}
	return false;
 }
 
</script>

<!-- 
<div id="window">
  <div id="windowTop">
    <div id="windowTopContent">FieldSolutions Enhancements</div>
    <img src="../techs/images/window_close.jpg" id="windowClose" />
  </div>
  <div id="windowBottom"><div id="windowBottomContent">&nbsp;</div></div>
    <div id="windowContent">
<p>This evening FieldSolutions is releasing a number of enhancements to our web based service management engine. These releases are designed to expand our service capability to those clients with projects requiring &quot;unlimited&quot; numbers of deliverables and documents from the working technician, as well as numerous client user system experience improvements.</p>

<p><b>Release Features:</b></p>
<p style="text-decloration:underline; font-weight:bold">Unlimited Document Uploads - Client and Technician: </underline></p>
<ul style="list-style: disc none"><li>After eliminating upload file size restrictions last release, now clients and technicians can upload unlimited numbers of documents, and individually name/label each uploaded item. This capability is unique to FieldSolutions and is particularly valuable for survey documents, serial number capture, test documents, and proof of work photos. And, of course, all deliverables and documents are attached to the FieldSolutions work order and maintained in our system.</li></ul>
<p style="text-decloration:underline; font-weight:bold">SLA Performance Report:</p>
<ul style="list-style: disc none"><li>Now available in the Reports Drop Down list is an &quot;SLA report&quot; listing for any project and date range any technician provider that has had a &quot;no show&quot; (i.e. a tech that did not arrive on site on time without prior notice to the client resource coordinator)  or a &quot;back out&quot; (i.e. a tech that cancels a scheduled appointment in advance)</li></ul>
<p style="text-decloration:underline; font-weight:bold">User Experience Enhancements:</p>
<ul style="list-style: disc none">
<li>Display Settings - select the number of work orders viewable in client dashboards reducing page loads to work long lists of work orders. This setting is retained as a viewing preference between uses. </li>
<li>Full Technician profile more accessible - now whenever you click on a technician ID, anywhere in the FieldSolutions website, their entire profile opens in a pop-up.</li>
<li>Global speed gadgets now available throughout the site - the blue &quot;buttons&quot; in the top center of the navigation bar are shortcuts for the most frequent actions, have been added throughout the client site for easier access. </li>
</ul>
   </div>
  <img src="../techs/images/window_resize.gif" id="windowResize" />
</div>
-->

<?  if( date("Ymd") >= 20130306 && date("Ymd") < 20130924 ){ ?>
<div id="window_BLUE">
	<div id="windowTop_BLUE">
		<div id="windowTopContent_BLUE" style="font-size:12px">FieldSolutions deploys FS-RUSH&trade;, adds Work Market suppliers</div>
		<img src="../techs/images/window_close.jpg" id="windowClose_Two" />
	</div>
<div id="windowBottom_BLUE"><div id="windowBottomContent_BLUE">&nbsp;</div></div>
<div id="windowContent_BLUE" style="font-size: 12px;">
	<p style="font-family: Arial, Helvetica, sans-serif;">September 16, 2013 - FieldSolutions, North America's premier contingent technology field resource provider, announces our newly enhanced auto assignment tool, FS-RUSH&trade;, including availability of the Work Market supplier base, all embedded within the FieldSolutions on-line service management system <span style="margin: 0px; font-weight: bold">at no added cost, no license fee, no new contracts, and no changes in payment terms.</span></p>
    <p style="font-family: Arial, Helvetica, sans-serif">FS-RUSH&trade; allows FieldSolutions clients to simultaneously publish and assign work in both the FieldSolutions and Work Market systems while retaining all the advantages of being within the quality controlled, risk insured, and highly supported FieldSolutions service programs. Additionally, clients gain the administrative advantage of paying these providers within their standard weekly FieldSolutions service invoice and with their established credit terms.</p>
    
    <p style="font-family: Arial, Helvetica, sans-serif; font-weight:bold">Embedding Work Market in the FieldSolutions environment creates value:</p>
    
    <p style="font-family: Arial, Helvetica, sans-serif">
    <ol style="margin-left: 15px">
    	<li>More Bids, Faster</li>
        	<ul style="list-style: disc">
            	<li>With just a click of the mouse, you can publish work to both FieldSolutions AND Work Market providers</li>
                <li>Target providers for assignment using robust new controls included in FS-RUSH&trade;</li>
            </ul>
    	<li>Best of Both Worlds - No More Swivel-Chairing</li>
        	<ul style="list-style: disc">
            	<li>FieldSolutions is now your one-stop-shop for all field service needs with Work Market's diverse provider base supplementing FieldSolutions' strong technology focus</li>
            </ul>
	</ol>
    </p>
	<p style="padding: 0px; font-family: Arial, Helvetica, sans-serif;">
    <a style="margin: 0px" href="FS-RUSH and Work Market Partnership - 2013-09-16.pdf">Read More...</a><br/><br/>
    <a style="margin: 0px" href="/docs/FS-RUSH_FAQ_2013-09-12.pdf">FAQ</a><br/><br/>
    Contact your FieldSolutions Account Manager or Client Services Director to enable Work Market sourcing today!<br/>
<br/>
Thanks,<br/>
Your FieldSolutions Team
	</p>
</div>

<img src="images/window_resize.gif" id="windowResize_Two" alt="" /></div>
<? } ?>


<? if( date("Ymd") < 20130820 ){ ?>
<div id="window_BLUE">
	<div id="windowTop_BLUE">
		<div id="windowTopContent_BLUE">CheckINN Hotel Savings Card Reduces Travel Costs</div>
		<img src="../techs/images/window_close.jpg" id="windowClose_Two" />
	</div>
<div id="windowBottom_BLUE"><div id="windowBottomContent_BLUE">&nbsp;</div></div>
<div id="windowContent_BLUE" style="font-size: 12px;">
    <p style="font-family: Arial, Helvetica, sans-serif;">August 8, 2013 - We are pleased to offer the FieldSolutions CheckINN Hotel Savings Card to all of our technicians and clients for use 
at over 10,800 participating hotels nationwide. This program is a NO COST service provided by FieldSolutions that helps our clients save money on travel expenses. 
Techs can save money on business and even personal travel, and so can our client's employees. <a style="margin-right:0px;margin-left:0px;" onClick="showClcShadowbox();" href="javascript:void(0);">Click Here</a> to enroll now!</p>
    
    <p style="font-family: Arial, Helvetica, sans-serif;">
   Cardholders enrolled in the FieldSolutions CheckINN lodging savings program can save up to 50% per night compared to standard corporate 
rates, savings that are passed to our clients in the form of reduced expenses for work requiring overnight travel.</p>
    
    <p style="font-family: Arial, Helvetica, sans-serif;">This exciting, new and unique offering is available only through FieldSolutions, and further delivers on our mission of providing a compelling 
competitive advantage to our technology service clients.</p>
    
    <p style="font-family: Arial, Helvetica, sans-serif;">Contact your Account Manager (listed on the Client Profile tab of your 'Settings' page) for more information regarding the CheckINN Hotel Savings Program.</p>
   
   	<p style="padding: 0px; font-family: Arial, Helvetica, sans-serif;">
		Thanks,<br/>
Your FieldSolutions Team
	</p>
</div>
<div id="windowBottom_BLUE">
<div id="windowBottomContent_BLUE">&nbsp;</div>
</div>
<div id="windowContent_BLUE" style="font-size: 12px;">
<p style="font-family: Arial, Helvetica, sans-serif;">August 8, 2013
- We are pleased to offer the FieldSolutions CheckINN Hotel Savings Card
to all of our technicians and clients for use at over 10,800
participating hotels nationwide. This program is a NO COST service
provided by FieldSolutions that helps our clients save money on travel
expenses. Techs can save money on business and even personal travel, and
so can our client's employees. <a
	style="margin-right: 0px; margin-left: 0px;"
	onclick="showClcShadowbox();" href="javascript:void(0);">Click Here</a>
to enroll now!</p>

<p style="font-family: Arial, Helvetica, sans-serif;">Cardholders
enrolled in the FieldSolutions CheckINN lodging savings program can save
up to 50% per night compared to standard corporate rates, savings that
are passed to our clients in the form of reduced expenses for work
requiring overnight travel.</p>

<p style="font-family: Arial, Helvetica, sans-serif;">This exciting,
new and unique offering is available only through FieldSolutions, and
further delivers on our mission of providing a compelling competitive
advantage to our technology service clients.</p>

<p style="font-family: Arial, Helvetica, sans-serif;">Contact your
Account Manager (listed on the Client Profile tab of your 'Settings'
page) for more information regarding the CheckINN Hotel Savings Program.</p>

<p style="padding: 0px; font-family: Arial, Helvetica, sans-serif;">
Thanks,<br />
Your FieldSolutions Team</p>
</div>
<img src="images/window_resize.gif" id="windowResize_Two" alt="" /></div>
<? }?>

<!--div id="window_RED">
	<div id="windowTop_RED">
		<div id="windowTopContent_RED">NEW Field Solutions Dashboards Go Live - 8AM Eastern Thursday July 15</div>
		<img src="images/window_close.jpg" id="windowClose_Two" />
	</div>
<div id="windowBottom_RED"><div id="windowBottomContent_RED">&nbsp;</div></div>

<div id="windowContent_RED">
<p>Tomorrow morning, the new Field Solutions Dashboards Go Live at 8:00 AM Eastern<p>
 
<p>What this means to you is a new experience, new speed gadgets, and new ways to "move around" the Field Solutions site.</p>
 
<p>For training: There are two more training Webinars:</p>
<p> 
&gt; this evening at 7:00 PM<br/>
&gt; tomorrow morning at 10:00 AM eastern.<br/>
</p>
<p>Pre-register at <a href="https://fieldsolutions.webex.com" target="_blank">https://fieldsolutions.webex.com</a> (password FStrain)</p>

<p>If you have any issues or concerns please speak with your Field Solutions Service Support person. team training with Q&A is available any time.</p>
 
<p>Thanks,<br />
The Field Solutions Team</p>
<img src="images/window_resize.gif" id="windowResize_Two" />
</div>
-->



<!--  script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script> <script type="text/javascript" language="javascript">try{f_cbload("I9H0J0B0F5I9H0J0B0F5","https:");}catch(li){;}</script> <div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=I9H0J0B0F5I9H0J0B0F5">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div -->
<div id="container" class="use-sidebar-right use-sidebar-left"><!--  div id="header">
	    <div id="wrapper">
	        <div id="content" align="center"></div>
	    </div>
	<br />
	
	</div-->
<style type="text/css">
#detailsContainer p {
	margin: 0px;
}

#container {
	margin-right: auto;
	margin-left: auto;
	margin-top: 20px;
}

.inner10 {
	padding: 0px 10px 0px 10px;
}

/*
	input[type="submit"] {
    	color: #FFFFFF;
	}
	*/
#newsCenter {
	display: none;
}

.toolSubmit {
	background-color: #4790D4;
	color: #FFFFFF;
	width: 70px;
	margin-top: 5px;
}

.toolSubmit:hover {
	font-weight: bold;
	text-decoration: underline;
	background-color: #032D5F;
	color: #FFFFFF;
	cursor: pointer;
}

.heading {
	background: url("/widgets/images/triangle.gif") no-repeat scroll left
		1px transparent;
	font-size: 13px;
	margin: 10px 0;
	padding: 0 0 0 20px;
	color: #595959;
	font-weight: bolder;
}

.toolsLeftContent {
	line-height: 2.5;
	margin: 0px 0px 10px 10px;
	color: #10085a;
}

.toolsRightContent {
	line-height: 2;
	margin: 10px 0px 10px 0px;
	text-align: left;
	color: #10085a;
	border-top: 1px #DDDDDD solid;
}

.toolsRightContent div {
	line-height: 1;
}

.right_col {
	float: none;
	width: auto;
	text-align: left;
	/*		margin-left: 10px;*/
	margin-top: 25px;
}

#dashleftcontent {
	float: left;
	min-height: 100%;
	height: 100%;
	background-color: #FFFFFF;
	display: none;
	font: 10px/ 10px Verdana, Arial, Helvetica, sans-serif;
	background: url("/widgets/images/border_right.png") no-repeat scroll
		right top transparent;
	padding-right: 26px;
}

#dashrightcontent {
	float: right;
	min-height: 100%;
	height: 100%;
	background-color: #FFFFFF;
	display: none;
	font: 10px/ 10px Verdana, Arial, Helvetica, sans-serif;
	text-align: left;
	white-space: nowrap;
	overflow: hidden;
	background: url("/widgets/images/border_right.png") no-repeat scroll
		left top transparent;
	padding-left: 26px;
}

#dashcentercontent {
	background-color: #FFFFFF;
	height: 100%;
	float: left;
	width: 50%;
}

.use-sidebar-right .use-sidebar-left #dashcentercontent {
	white-space: nowrap;
	overflow: hidden;
}

.use-sidebar-right #dashrightcontent {
	display: block;
	width: 245px;
	white-space: nowrap;
	overflow: hidden;
}

.use-sidebar-left #dashleftcontent {
	display: block; //
	width: 245px;
	white-space: nowrap;
	overflow: hidden;
}

.use-sidebar-right #separatorRight {
	margin-right: auto;
	display: block;
	outline: none;
	width: 16px;
	height: 16px;
	background: url('/widgets/images/arrows-right5.png');
	border-color: #FFF;
	margin-bottom: 10px;
}

.use-sidebar-left #separatorLeft {
	display: block;
	outline: none;
	width: 16px;
	height: 16px;
	background: url('/widgets/images/arrows-left5.png');
	border-color: #FFF;
	margin-left: auto;
}

.sidebar-at-right #separatorRight {
	float: left;
}

.sidebar-at-left #separatorLeft {
	float: left;
}

.sidebar-at-right #separatorRight {
	float: right;
	display: block;
	outline: none;
	width: 16px;
	height: 16px;
	background: url('/widgets/images/arrows-left5.png') no-repeat;
	/*margin-left: -200px;*/
	margin-top: 0px;
}

.sidebar-at-left #separatorLeft {
	float: left;
	display: block;
	outline: none;
	width: 16px;
	height: 16px;
	background: url('/widgets/images/arrows-right5.png') no-repeat;
	margin-right: 2px;
	margin-top: 0px;
}

a:hover {
	cursor: pointer;
	cursor: hand;
}

.newsBlurb {
	margin-top: 8px;
	margin-bottom: 8px;
	line-height: 1.5;
}

table tr.special td {
	border-top: 1px #DDDDDD solid;
}

#clcLogoSection img {
	margin-right: 15px;
}

#clcPopupContainer p {
	font: 11px Helvetica, Sans-Serif;
}
</style>

<input type="hidden" id="isDash" name="isDash" value="1" /> <script
	type="text/javascript">
    //  use for load images and save it in browser cache only!!!
    var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
    var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";
	</script> <script type="text/javascript">

	 /**
     *  calendarInit
     *
     *  Display filter post handler
     */
    function calendarInit() {
        $.map(['edit_StartDate', 'edit_EndDate','edit_DateNotified'], function( id ) {
                $('#'+id).calendar({dateFormat:'MDY/'});
        });
    }
    /**
     * calendarMove
     *
     * @param event  event
     * @access public
     * @return void
     */
    function calendarMove( input ) {
        $('#calendar_div').css('top', $(input).offset().top + $(input).height() + 6 + 'px');
        $('#calendar_div').css('left', $(input).offset().left + 'px');
        popUpCal.showFor(input);
    }

    function showNews(){
		$("#detailsContainer").html($("#newsCenter").html());
    }

    function findWorkOrderSuccess(company, id, techID){
		hideSidebar();
		detailsWidget.show({tab:'index',params:{company:company,container:'detailsContainer',tab:"new", win: id, techID:techID}});
		checkMinWidth();
	} 

	function showRecentWO(win){
		dr.showRecentWO(win);
	}

	function showTechProfile(tid){
		hideSidebar();
		$("#detailsContainer").html("");
		$("#detailsContainer").css("height: 2500px");
		$('<iframe />').attr("width", "96%").attr('height', '2500px').attr('src','/clients/wosViewTechProfile.php?simple=0&v=' + window._company + '&TechID=' + tid).appendTo($('#detailsContainer'));
		checkMinWidth();
	} 

	// Show sidebar
    var showSidebar = function (){
	dr.showSidebar();
/*   	   $("#separatorOutside").hide();
       $("#separatorInside").show();
       objMain.addClass('use-sidebar-right');
       objMain.removeClass('sidebar-at-right');
       $("#dashcentercontent").attr("id", "dashcentercontent");
       $.cookie('sidebar-pref2', 'use-sidebar-right', { expires: 30 });*/
   };

   // Hide sidebar
   var hideSidebar = function (){
	  dr.hideSidebar();
/*	   if(objMain.hasClass('use-sidebar-right')){
	   	   $("#separatorOutside").show();
	       $("#separatorInside").hide();
	       objMain.removeClass('use-sidebar-right');
	       objMain.addClass('sidebar-at-right');
	       $.cookie('sidebar-pref2', null, { expires: 30 });
	   }*/
   };

// Show sidebar
   var showLeftSidebar = function (){
	dl.showSidebar();
/*  	  $("#separatorLeftOutside").hide();
      $("#separatorLeftInside").show();
      objMain.addClass('use-sidebar-left');
      objMain.removeClass('sidebar-at-left');
      $("#dashcentercontent").attr("id", "dashcentercontent");
      $.cookie('sidebar-pref2', 'use-sidebar-left', { expires: 30 });*/
  };

  // Hide sidebar
  var hideLeftSidebar = function (){
	dl.hideSidebar();
/*	  if(objMain.hasClass('use-sidebar-left')){
	  	  $("#separatorLeftOutside").show();
	      $("#separatorLeftInside").hide();
	      objMain.removeClass('use-sidebar-left');
	      objMain.addClass('sidebar-at-left');
	      $.cookie('sidebar-pref2', null, { expires: 30 });
	  }*/
  };
	 
    var objMain;
	var roll;
    var detailsWidget;
    var navWidget;
    var objSeparator;

	// Sidebar separator
	var separatorRightClick = function(e){
		dr.separatorClick();
			//e.preventDefault();
/*			if ( objMain.hasClass('use-sidebar-right') ){
				    hideSidebar();
				}
			else {
			        showSidebar();
			}
			checkMinWidth();*/
		};

	// Sidebar separator
	var separatorLeftClick = function(e){
		dl.separatorClick();
			//e.preventDefault();
/*			if ( objMain.hasClass('use-sidebar-left') ){
					   hideLeftSidebar();
				}
			else {
				       showLeftSidebar();
			}
			checkMinWidth();*/
		};

	var checkMinWidth = function(){
		if(objMain.hasClass('use-sidebar-left') && !objMain.hasClass('use-sidebar-right')){
			
				$("#mainContainer").css("width", "100%");
				$("#dashcentercontent").css("width", "75%");
			}
		if(objMain.hasClass('use-sidebar-left') && objMain.hasClass('use-sidebar-right')){
		
			$("#mainContainer").css("min-width", "950px");
			$("#dashcentercontent").css("width", "50%");
		}

		if(objMain.hasClass("use-sidebar-right") && !objMain.hasClass("use-sidebar-left")){
			
			$("#mainContainer").css("min-width", "950px");
			$("#dashcentercontent").css("width", "75%");
		}

		if(!objMain.hasClass("use-sidebar-right") && !objMain.hasClass("use-sidebar-left")){
			
			$("#mainContainer").css("min-width", "750px");
			$("#dashcentercontent").css("width", "100%");
		}
	}
  
	$(document).ready(function() {

		objSeperator =  $('#separator');
		objMain = $('#container');

	    showNews();
	    
		//create WO 
	    roll = new FSPopupRoll();
        detailsWidget = new FSWidgetWODetails({container:'detailsContainer',tab:'create'},roll);

	dr = new FSWidgetDashContentRight({container:'dashrightcontent', objMain: objMain, checkMinWidth: checkMinWidth, company:window._company, showrightpanel: 1});
	<?php if (!$installManager):?>
	dr.show();
	<?php endif;?>

	dl = new FSWidgetDashContentLeft({container:'dashleftcontent', objMain: objMain, dr: dr, checkMinWidth: checkMinWidth, company:window._company});
	<?php if (!$installManager):?>
	dl.show();
	<?php endif;?>

        wd = new FSWidgetDashboard({container:'detailsContainer',tab:'index'});

		<?php if (!empty($_GET['goto'])):?>
		wd.openFramePage("<?=$_GET['goto']?>");
		<?php endif;?>

        findWorkOrder = new FSFindWorkOrder(wd, roll);
        findWorkOrder.buildHtml();     

		//create work order widget
/*        $("#CreateWorkOrderBtn", window.parent.document).unbind('click').click(function(){
                   $.post("/widgets/dashboard/client/accessctl/", {rt:'iframe',attributeId: 2},
                            function (response) {
                                if(response=="1") {
			    		hideSidebar();
			    		detailsWidget.show({tab:'create',params:{company:window._company,container:'detailsContainer',tab:"new"}});
			       		checkMinWidth();
                                }
                                else { Attention(response); }
                        });
                        return false;

        });  
*/        
     // Set the width via the plugin.
        $('select#Project_ID').ieSelectWidth
        ({
            width : 133,
            containerClassName : 'select-container',
            overlayClassName : 'select-overlay'
        });
    	
		$('iframe').iframeAutoHeight();  
  		
	});

	function clearDefaultProjectFields() {
		//if (oldProject == "Default Project") {
			$("#edit_WO_Category_ID", window.parent.document).val("");
			$("#edit_Headline", window.parent.document).val("");
			$("#edit_PcntDeduct", window.parent.document).removeAttr('checked');
			$("#edit_FixedBid", window.parent.document).removeAttr('checked');
			if ($("#edit_Ship_Contact_Info", window.parent.document).val() == "viewable only by the assigned tech") $("#edit_Ship_Contact_Info").val("");
		//}
	}

	//Create WO Functionality
	function gotoTab(name) {
		serverScript = '<?=$_SERVER["SCRIPT_NAME"]?>';
		
        location = location.toString().replace(serverScript, '/clients/wos.php') + '&tab=' + name;
        return false;
    }

	function showClcShadowbox(){
		$("<div></div>").fancybox({
			'autoScale' : false,
			'content' : $("#clcPopupContent").html(),
			'scrolling' : 'no',
			'height' : 520,
			'width' : 710,
			'autoDimensions' : false,
			'centerOnScroll' : true
		}).trigger('click');  
	}
	</script> <!--- LEFT COLUMN --->
<div id="dashleftcontent"></div>

<!--  br />
		
		
			<div id="welcomeModule">
				<h1>Welcome Back <span id="clientName">name</span></h1>
				<br /><br />
				<div id="window">
				  <div id="windowTop">
				  	<p><b>ANNOUNCEMENT:</b></p><br />
				    <div id="windowTopContent">Site Upgrade Notification - This Weekend</div>

				  </div>
				  <div id="windowBottom"><div id="windowBottomContent">&nbsp;</div></div>
				    <div id="windowContent">
					<p>This evening FieldSolutions is releasing two enhancements to our web-based service management engine. These enhancements improve our  clients&rsquo; ability to serve your clients in a unique and attractive way:</p>
				    <p>Release Features:
				    		<ul style="list-style: disc none">
				            	<li>New End Client Logos for printed work orders:</li>
				                <ul style="list-style: disc none">
					                <li>Now in addition to your logo being the standard logo printed at the top of your work orders, you can display YOUR client&rsquo;s logo on the techs&rsquo; printed work order on a project-by-project basis. This is particularly valuable in high control environments and for consumer/residential work.</li>
								</ul>
				               	<li>Documents Capability upgraded:</li>
				                <ul style="list-style: disc none">
				                	<li>To date clients could send up to 5 documents to techs via projects and work order uploads, and techs could upload 3 documents for proof of work deliverables, each with a 15MB size limit.</li>
				                    <li>The total count has not changed, however now, the size limitations are removed. All documents sizes are now available, which is particularly valuable for photo uploads. As always, zip files are acceptable so now any number of required deliverable documents can be uploaded without assistance.</li>
				                </ul>
				            </ul>
				    </p>
				    <p>Please call your Client Services Manager if you have any questions or suggestions.</p>
					<p>Thank you,<br/>
				    The FieldSolutions Team</p>
				   </div>
				  <img src="../techs/images/window_resize.gif" id="windowResize" />
				</div>
			
			
			</div>
				
			<br />
			
			<div id="update">
				<p><b>UPDATE:</b></p>
				<p>Field Solutions now has the field service outsourcing industry's most complete, statistically valid and effective measure of likely outcomes for provider assignments, all without the inherent bias caused by providers rating the clients.  Our new state-of-the-art ratings of providers' Preference &amp; Performance combines the client "ultimate'" indicator of technician preference with the hard facts of technician work order event performance for a complete view of technician quality. If you have any questions, call your Account Executive or Service Delivery Manager, or email <a href="mailto:info@fieldsolutions.com">info@fieldsolutions.com</a>.</p>
			</div>
			<br>
			
			<div id="news">
				<p><b>NEWS:</b></p><br />
				<p>Field Solutions offers web-enabled self service and full service project management for our clients. Our mission as a service company supports making sure "you're always covered", so call us for support or assistance for any field staffing or sourcing challenge.</p>
				<p>Field Solutions continues to grow. Read our media releases to chart our growth over the past several years. In the first half of 2009 our growth exceeds 85% over first half of 2008, and we continue to add over one new client per week! Our COSTS, ON-LINE SYSTEM, and BUSINESS RESULTS are helping our clients win in their markets! Call us for more ideas on how we can help you scale up or reduce costs! </p>
			</div --> <!-- CENTER CONTENT -->
<div id="dashcentercontent"><!-- Main content -->
<div class="inner10 clr indentTop" style="border: 2px solid #4790D4">
<div style="display: none;"
	onclick="javascript:separatorLeftClick(this);"
	id="separatorLeftOutside"><a id="separatorLeft"></a></div>
<div style="display: none;"
	onclick="javascript:separatorRightClick(this);" id="separatorOutside">
<a id="separatorRight"></a></div>
<div class="left_col"><!--  div style="width: 80%;" class="fl" id="leftVerticalContainer"></div>
			    <div style="width: 18%;" class="fr"><button onclick="return toggleNavBar(this);" id="navBarToggle">&lt;&lt;</button></div -->
</div>
<div id="detailsContainer" class="right_col"></div>


</div>
<!-- 
			<div id="" style="padding: 10px; border:inset; color:#FF0000">
		    	<p><b>Internet Explorer Security Warnings</b></p>
		        <p>If you get warnings from Internet Explorer about showing insecure content when you open a work order on our site, <a href="../content/How to turn off IE security warnings.pdf">this document</a> explains how to turn them off.</p>
		    </div>
		    
		  <div id="announcementArchive" style="border-bottom:1px dashed;border-color:#000000 #000000 -moz-use-text-color;padding-bottom: 20px;">
		    <p><b>ANNOUNCEMENT ARCHIVE:</b></p><br />
		    
		      
		    <div id="window_GREEN">
				Lite Dashboard View now available <a id="readMore" href="javascript:void(0);" onclick="truncateText(this);">...[Read More]</a>
				<div id="moreText" style="display: none;">
					<p> Your dashboards now allow you to choose a "Lite" view if you think your dashboards look too cluttered.The selector switch is at the upper right of your screen when you are viewing your dashboards. It should automatically change views, and stay on this setting for your machine until you change this selection.</p>
				</div>
			</div>
			</div>
			<br />
				<div id="window_RED">
					NEW Field Solutions Dashboards Go Live - 8AM Eastern Thursday July 15 <a id="readMore" href="javascript:void(0);" onclick="truncateText(this);">...[Read More]</a>
					
					<div id="moreText" style="display:none;">
						<p> Tomorrow morning, the new Field Solutions Dashboards Go Live at 8:00 AM Eastern</p>
						 
						<p>What this means to you is a new experience, new speed gadgets, and new ways to "move around" the Field Solutions site.</p>
						 
						<p>For training: There are two more training Webinars:</p>
						<p> 
						&gt; this evening at 7:00 PM<br/>
						&gt; tomorrow morning at 10:00 AM eastern.<br/>
						</p>
						<p>Pre-register at <a href="https://fieldsolutions.webex.com" target="_blank">https://fieldsolutions.webex.com</a> (password FStrain)</p>
						
						<p>If you have any issues or concerns please speak with your Field Solutions Service Support person. team training with Q&amp;A is available any time.</p>
						 
						<p>Thanks,<br />
						The Field Solutions Team</p>ss
					</div>
				</div>
				<div id="" style="padding: 10px"> 
			<p><b>Quick Links</b></p>
				 <p>	
					<ul id="todo">
					   <li><a href="http://www.mytechnicianspace.com" target="_blank">www.mytechnicianspace.com</a></li>
					 </ul>
				</p>
		</div>
		<div class="clearer">&nbsp;</div>
	
				--></div>
<!-- END CENTER CONENT --> <!--- RIGHT COLUMN --->
<div id="dashrightcontent"></div>

</div>

<!-- FOOTER -->
<div class="footer_menu clr" style="clear: both;">
<table cellspacing="10" cellpadding="10" align="center">
	<tbody>
		<tr>
			<td valign="top" class="footer_items"><a
				style="text-decoration: none;" href="/wp/our-services/"> <span
				style="color: rgb(140, 140, 140);"> Our Services </span> </a></td>

			<td valign="top" class="footer_items"><a
				style="text-decoration: none;" href="/wp/our-clients/"> <span
				style="color: rgb(140, 140, 140);"> Our Clients </span> </a></td>

			<td valign="top" class="footer_items"><a
				style="text-decoration: none;" href="/wp/our-members/"> <span
				style="color: rgb(140, 140, 140);"> Our Members </span> </a></td>

			<td valign="top" class="footer_items"><a
				style="text-decoration: none;" href="/wp/fieldsolutions-difference/">
			<span style="color: rgb(140, 140, 140);"> FieldSolutions
			Difference </span> </a></td>

			<td valign="top" class="footer_items"><a
				style="text-decoration: none;" href="/wp/news-and-events/"> <span
				style="color: rgb(140, 140, 140);"> News and Events </span> </a></td>

			<td valign="top" class="footer_items"><a
				style="text-decoration: none;" href="/wp/about-us/"> <span
				style="color: rgb(140, 140, 140);"> About Us </span> </a>
			<ul id="footer_aboutus">
				<li><a href="/wp/mission-and-values/"> <span
					style="color: rgb(140, 140, 140);"> Mission and Values </span> </a></li>

				<li><a href="/wp/careers/"> <span
					style="color: rgb(140, 140, 140);"> Careers </span> </a></li>

				<li><a href="/wp/management-team/"> <span
					style="color: rgb(140, 140, 140);"> Management Team </span> </a></li>

				<li><a href="../content/Terms_Use.pdf"> <span
					style="color: rgb(140, 140, 140);"> Terms Of Use </span> </a></li>
			</ul>
			</td>

			<td valign="top" class="footer_items"><a
				style="text-decoration: none;" href="/wp/contact-us/"> <span
				style="color: rgb(140, 140, 140);"> Contact Us </span> </a></td>
		</tr>
	</tbody>
</table>
<br>
<br>
<div class="copyright"><span style="color: rgb(140, 140, 140);">Copyright
&copy; 2007-<?PHP echo date("Y") ?>, FieldSolutions, Inc. All rights
reserved.&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp; </span> <a
	style="text-decoration: none;" title="Privacy Policy" target="_blank"
	href="https://www.fieldsolutions.com/wp/wp-content/themes/fieldsolutions/img/privacy_policy.pdf">
<span style="color: rgb(140, 140, 140);"> Privacy Policy </span> </a></div>

</div>

<div></div>

<div id="newsCenter">
<h1 style="text-align: left; padding: 8px;">FieldSolutions deploys
new FS-RUSH&trade; high-speed Work Order Assignment Tool and Adds Work
Market Suppliers within www.FieldSolutions.com</h1>

<p><img src="images/fsSwoop_no_arrow.png"
	style="margin-top: -40px; padding: 0px;" width="480" height="118" alt="" /></p>

<p style="padding: 8px;">September 16, 2013 - FieldSolutions, North
America's premier contingent technology field resource provider,
announces our newly enhanced auto assignment tool, FS-RUSH&trade;,
including availability of the Work Market supplier base, all embedded
within the FieldSolutions on-line service management system.
FS-RUSH&trade; offers clients the entire Work Market provider base at no
added cost and with NO FIXED LICENSE FEE with all the advantages of
being within the quality controlled, risk insured, and highly supported
FieldSolutions service programs. Additionally, clients gain the
administrative advantage of paying these providers within their standard
weekly FieldSolutions service invoice and with their established credit
terms.</p>

<p style="padding: 8px; font-weight: bold;">New FS-RUSH&trade;</p>

<p style="padding: 8px;">FieldSolutions' new FS-RUSH&trade;
automatic assignment tool in the FieldSolutions work order gives clients
the ability to simultaneously publish the same work in both the
FieldSolutions and Work Market systems in one easy step, and with no
additional fees or contracts.</p>

<p style="padding: 8px;">If a work order is assigned to a Work
Market provider, the work is still managed and invoiced through the
normal, intuitive, and preferred FieldSolutions system as normal, all
while enjoying FieldSolutions' best-in-class client support, global
insurance coverages, and credit terms. There are <b>no added fees,
no license expense, no new contracts and no changes in payment terms.</b></p>

<p style="padding: 8px; font-weight: bold;">What This Means for
FieldSolutions' Clients</p>

<p style="padding: 8px;">Adding Work Market providers to the
FieldSolutions environment creates value for FieldSolutions clients:</p>
<ul
	style="list-style: decimal none; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; padding: 15px; margin: 0 10px 0 10px;">
	<li>More Bids, Faster</li>
	<ul style="list-style: circle none; padding: 8px;">
		<li>Now, with just a click of the mouse, you can publish work to
		FieldSolutions AND Work Market providers, instantly expanding the
		provider base and tapping into the quick response ISO's in Work Market</li>
		<li>Robust new controls included in FS-RUSH&trade; allow clients
		to precisely target providers for automatic assignment, all while
		pushing for faster responses to available work</li>
	</ul>
	<li>Best of Both Worlds - No More Swivel-Chairing</li>
	<ul style="list-style: circle none; padding: 8px;">
		<li>FieldSolutions is now your one-stop-shop for all field
		service needs with Work Market's diverse provider base including work
		categories such as Painters, Plasterers, Riggers supplementing
		FieldSolutions' strong technology focus</li>
		<li>Continue to enjoy the full service client support, insurance
		coverage and reliability of FieldSolutions, even for work assigned to
		Work Market providers</li>
	</ul>
</ul>

<p style="padding: 8px;">Contact your FieldSolutions Account Manager
or Client Services Director (listed on the Client Profile tab of your
Settings page) to enable Work Market sourcing today!</p>

<p style="clear: both; padding: 8px;">Thanks!<br />
Your FieldSolutions Team</p>

</div>

<!-- CLC SHADOWBOX -->
<div id="clcPopupContent" style="display: none;">
<div id="clcPopupContainer" style="width: 710px; height: 550px;">
<div><img src="../techs/images/fs_logo_small.png" width="200"
	alt="" /></div>
<div><img src="../techs/images/clc_header_bg.png" height="100"
	width="700" alt="" />
<p style="margin: -96px 0 0 10px; color: #FFF;"><span
	style="font-size: 13px; font-weight: bold;">FieldSolutions
CheckINN&trade; Hotel Savings for Technicians &amp; Clients</span><br />
<span>Saves FieldSolutions Technicians &amp; Clients up to 50% on
Nationwide Hotels</span></p>
</div>


	</div>

<!-- CLC SHADOWBOX -->
	<div id="clcPopupContent" style="display:none;">
		<div id="clcPopupContainer" style="width: 710px; height: 550px;">
			<div>
				<img src="../techs/images/fs_logo_small.png" width="200"  />
			</div>
			<div>
				<img src="../techs/images/clc_header_bg.png" height="100" width="700" />
				<p style="margin: -96px 0 0 10px; color: #FFF;">
					<span style="font-size: 13px;font-weight:bold;">FieldSolutions CheckINN&trade; Hotel Savings for Technicians &amp; Clients</span><br />
					<span>Saves FieldSolutions Technicians &amp; Clients up to 50% on Nationwide Hotels</span>
				</p>
			</div>
			
			<div style="width: 700px;margin-top: 15px;font-size: 11px;">
				<div style="float:left;width: 450px;">
					<div style="color: #F69630;font-size:11px;">FIELDSOLUTIONS EXCLUSIVELY OFFERS OUR TECHNICIANS &amp; CLIENTS THE FIELDSOLUTIONS CHECKINN HOTEL SAVINGS CARD</div>
					<p style="margin-top: 5px; line-height: 15px;">FieldSolutions now offers you the benefits of the CheckINN&trade; hotel savings card for use at over 10,800 
					participating hotels. FieldSolutions' CheckINN&trade; discounted hotel rates are up to 50% less than standard 
					corporate rates. Travelers present their personalized FieldSolutions CheckINN&trade; card at check-in to receive 
					the reduced rates, with direct billing to your pre-authorized personal credit card. 
					</p>
					<div style="color: #F69630;font-size:11px;">CARD SAVINGS FEATURES</div><br />
					<ul style="list-style: disc;margin-left: 20px;line-height: 15px;color: #000;">
						<li>Rates are typically up to 50% less due to the CheckINN&trade; Card's enormous booking volumes</li>
						<li>Free to enroll</li>
						<li>Walk-in availability, no extra costs for last minute bookings, standard hotel cancellation policies</li>
						<li>Avoid extra room night charges with CLC's 24 hour stay policy - for example, check in at 10 pm, check out at 9:59pm the next day</li>
						<li>Easy hotel and rate access via free iPhone&trade; & Android&reg; Hotel Locator Apps</li>
						<li>Stay details and invoices posted to your online account for easy tracking and expense reimbursement</li>
						<li>Nightly $8.95 transaction fee plus taxes are automatically added to all rates, not applied for cancelled reservations</li>
					</ul><br />
					<div style="color: #F69630;font-size:11px;">PARTICIPATING HOTELS INCLUDE</div>
					<div id="clcLogoSection" style="width: 100%">
						<img src="../techs/images/hampton_inn_logo.png" style="float:left; width: 50px;"  />
						<img src="../techs/images/hilton_logo.png" style="float:left; width: 75px;" />
						<img src="../techs/images/hyatt_logo.png" style="float:left; width: 35px;" />
						<img src="../techs/images/radisson_logo.png" style="float:left; width: 59px;" />
						<img src="../techs/images/bestwest_logo.png" style="float:left; width: 39px;" />
						<img src="../techs/images/super8_logo.png" style="float:left; width: 32px;" />
						<img src="../techs/images/days_inn_logo.png" style="float:left; width: 50px;" />
					</div>
				</div>
				<div style="float:left; margin-left: 45px;">
					<div style="width: 200px; height: 70px;">
						<a href="https://www.clclodging.com/cid/join/CCacctsignup.cfm?key=FIELDSOLTECH&company=FieldSolutionsClient" target="_blank">
							<img src="../techs/images/enrollBtn_bg.png" height="60" width="200" style="position: relative;z-index: 5;" />
						</a>
					</div>
					<div style="width: 200px; height: 35px;">
						<p style="margin: 0px; padding: 0px;text-align:center;">You must enroll using this link to have the $9.95 enrollment fee waived.</p>
					</div>
					<div style="width: 200px; height: 110px;">
						<img src="../techs/images/dropshadow_bg.png" height="200" width="200" />
						<p style="margin-top: -194px; font-size: 10px; padding: 0 20px 0 22px; text-align: center;">For important information regarding the usage of your FieldSolutions CheckINN card, please review:<br /><br />
						<a href="FieldSolutionsCheckINNHotelSavingsProgram-OverviewforFieldSolutionsClients-2013-08-08.pdf" target="_blank">FieldSolutions CheckINN&trade; Program Overview</a></p>
					</div>
					<img src="../techs/images/checkinn_card.png" style="width: 200px;" />
				</div>
			</div>
		</div>
	</div>
	<!-- END CLC SHADOWBOX -->
 
<!--  news archive 

<h1 style="text-align: left;">Welcome to the New Client/User Landing Page</h1>
 					<h2>Date: May 20, 2011</h2>
					<p>Starting today, we've deployed this newly enhanced entry point for all our client users. Most of the functionality is available elsewhere in our site, and nothing has been removed from any place else in our system.</p>
					<p>The goal of this Landing Page is nothing other than to provide a best-in-the-industry work tool. Here’s an overview:</p>
				    <p>Three (3) sections:
				    		<ul style="padding: 0px 25px 0px 25px; list-style: disc none">
				            	<li>Left "Operations" sidebar (retractable) with a "Create A New Work Order" and "Find A Work Order" quick action tool. These are short cut tools that open the work order in the center of this window.</li>
					            <li>Center "News and strategies weekly discussion", which when you open an operations from the left, is replaced with the appropriate screen, such as a selected work order or a new blank work order.</li>
				               	<li>Right sidebar (retractable) is new: links to open the individual user’s most recently worked work orders and below this a list of your most recently used techs for quick reference.</li>
				               	</ul>
				    </p>
				    <p>As you open work orders or create new ones from the left sidebar action areas, these items will be displayed for work directly in the center of this Landing Page. This is a speed of use feature. You can still reach your dashboards by clicking on the "My Work Orders" link in the blue navigation bar. You can return to the Landing Page by clicking on the FieldSolutions logo the upper left corner.</p>
				    <p>Finally, at the bottom of the left sidebar is a list of the most recent systems enhancements and releases and user instructions for your reference.</p>
				    <p>If you would like training or have questions about this new page, please contact your FieldSolutions account Executive or Client Service Manager.</p>
					<p>Thank you,<br/>
				    Your FieldSolutions team</p>
end news archive -->

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-4102511-1");
pageTracker._setDomainName(".fieldsolutions.com");
pageTracker._trackPageview();
} catch(err) {}</script>


<body></body>
<html></html>


<!-- Hide Update button -->
<script type="text/javascript" language="javascript">
//document.getElementById('Mod0EditRecord').style.display = 'none';
</script>

<!-- Gets Client Name -->
<script language="javascript" type="text/javascript">
<!-- 
var ajaxDisplay = document.getElementById('clientName');
if (ajaxDisplay) {
    //ajaxDisplay.innerHTML = document.forms.caspioform.contactName.value;
}
var pmContext = "<?=$_GET["v"]?>";
// Manipulates menu based upon admin field in datapage

/*
if (document.forms.caspioform.admin.value == "Yes"){
    var ajaxDisplay = document.getElementById('users');
    if (ajaxDisplay) {
    ajaxDisplay.innerHTML = '<a href="https://<?php echo $siteTemplate;?>.fieldsolutions.com/clients/usersView.php">Users</a>';
    }
    var ajaxDisplay = document.getElementById('projects');
    if (ajaxDisplay) {
    ajaxDisplay.innerHTML = '<a href="https://<?php echo $siteTemplate;?>.fieldsolutions.com/clients/projectsView.php">Projects</a>';
    }
    var ajaxDisplay = document.getElementById('customers');
    if (ajaxDisplay) {
    ajaxDisplay.innerHTML = '<a href="https://<?php echo $siteTemplate;?>.fieldsolutions.com/clients/customersView.php">Customers</a>';
    }
}
*/

//-->
</script>

<!-- Get Client Type Variable -->
<script language="JavaScript" type="text/javascript">

/* var BillingType = document.forms[0].billingType.value;
var adminLevel = document.forms[0].admin.value;

createCookie('tbbillingType',BillingType,1)
createCookie('tbAdminLevel',adminLevel,1)
*/
var LVDefaultFunction = function(){this.addFieldClass();}
var LVDefaults = {
    onValid : LVDefaultFunction,
    onInvalid : LVDefaultFunction
}

function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

</script>



<!--script type="text/javascript">

$(document).ready(
  function()
  {
    $('#windowOpen_GREEN').bind(
      'click',
      function() {
        if($('#window_GREEN').css('display') == 'none') {
          $(this).TransferTo(
            {
              to:'window_GREEN',
              className:'transferer_Two', 
              duration: 400,
              complete: function()
              {
                $('#window_GREEN').show();
              }
            }
          );
        }
        this.blur();
        return false;
      }
    );
    $('#windowClose_Two').bind(
      'click',
      function()
      {
        $('#window_GREEN').TransferTo(
          {
            to:'windowOpen_GREEN',
            className:'transferer_Two', 
            duration: 400
          }
        ).hide();
      }
    );
    $('#windowMin').bind(
      'click',
      function()
      {
        $('#windowContent_GREEN').SlideToggleUp(300);
        $('#windowBottom_GREEN, #windowBottomContent_GREEN').animate({height: 10}, 300);
        $('#window_GREEN').animate({height:40},300).get(0).isMinimized = true;
        $(this).hide();
        $('#windowResize_GREEN').hide();
        $('#windowMax_GREEN').show();
      }
    );
    $('#windowMax_GREEN').bind(
      'click',
      function()
      {
        var windowSize_GREEN = $.iUtil.getSize(document.getElementById('windowContent_GREEN'));
        $('#windowContent_GREEN').SlideToggleUp(300);
        $('#windowBottom_GREEN, #windowBottomContent_GREEN').animate({height: windowSize.hb + 13}, 300);
        $('#window_GREEN').animate({height:windowSize_GREEN.hb+43}, 300).get(0).isMinimized = false;
        $(this).hide();
        $('#windowMin_GREEN, #windowResize_GREEN').show();
      }
    );
    $('#window_GREEN').Resizable(
      {
        minWidth: 200,
        minHeight: 60,
        maxWidth: 700,
        maxHeight: 400,
        dragHandle: '#windowTop_GREEN',
        handlers: {
          se: '#windowResize_Two'
        },
        onResize : function(size, position) {
          $('#windowBottom_GREEN, #windowBottomContent_GREEN').css('height', size.height-33 + 'px');
          var windowContentEl_GREEN = $('#windowContent_GREEN').css('width', size.width - 25 + 'px');
          if (!document.getElementById('window_GREEN').isMinimized) {
            windowContentEl_GREEN.css('height', size.height - 48 + 'px');
          }
        }
      }
    );
  }
);

</script-->



<!-- TURN OFF POPUP HERE -->
<!--script type="text/javascript">

$(document).ready(
 function()
  {
    $('#windowOpen').bind(
      'click',
      function() {
        if($('#window').css('display') == 'none') {
          $(this).TransferTo(
            {
              to:'window',
              className:'transferer2', 
              duration: 400,
              complete: function()
              {
                $('#window').show();
              }
            }
          );
        }
        this.blur();
        return false;
      }
    );
    $('#windowClose').bind(
      'click',
      function()
      {
        $('#window').TransferTo(
          {
            to:'windowOpen',
            className:'transferer2', 
            duration: 400
          }
        ).hide();
      }
    );

    $('#windowMin').bind(
      'click',
      function()
      {
        $('#windowContent').SlideToggleUp(300);
        $('#windowBottom, #windowBottomContent').animate({height: 10}, 300);
        $('#window').animate({height:40},300).get(0).isMinimized = true;
        $(this).hide();
        $('#windowResize').hide();
        $('#windowMax').show();
      }
    );
    $('#windowMax').bind(
      'click',
      function()
      {
        var windowSize = $.iUtil.getSize(document.getElementById('windowContent'));
        $('#windowContent').SlideToggleUp(300);
        $('#windowBottom, #windowBottomContent').animate({height: windowSize.hb + 13}, 300);
        $('#window').animate({height:windowSize.hb+43}, 300).get(0).isMinimized = false;
        $(this).hide();
        $('#windowMin, #windowResize').show();
      }
    );
    $('#window').Resizable(
      {
        minWidth: 200,
        minHeight: 60,
        maxWidth: 700,
        maxHeight: 400,
        dragHandle: '#windowTop',
        handlers: {
          se: '#windowResize'
        },
        onResize : function(size, position) {
          $('#windowBottom, #windowBottomContent').css('height', size.height-33 + 'px');
          var windowContentEl = $('#windowContent').css('width', size.width - 25 + 'px');
          if (!document.getElementById('window').isMinimized) {
            windowContentEl.css('height', size.height - 48 + 'px');
          }
        }
      }
    );
  }
);

</script-->

<!-- 3RD POPUP WINDOW -->
<script type="text/javascript">

$(document).ready(
  function()
  {
    $('#windowOpen_BLUE').bind(
      'click',
      function() {
        if($('#window_BLUE').css('display') == 'none') {
          $(this).TransferTo(
            {
              to:'window_BLUE',
              className:'transferer_Two', 
              duration: 400,
              complete: function()
              {
                $('#window_BLUE').show();
              }
            }
          );
        }
        this.blur();
        return false;
      }
    );
    $('#windowClose_Two').bind(
      'click',
      function()
      {
        $('#window_BLUE').TransferTo(
          {
            to:'windowOpen_BLUE',
            className:'transferer_Two', 
            duration: 400
          }
        ).hide();
      }
    );
    $('#windowMin').bind(
      'click',
      function()
      {
        $('#windowContent_BLUE').SlideToggleUp(300);
        $('#windowBottom_BLUE, #windowBottomContent_BLUE').animate({height: 10}, 300);
        $('#window_BLUE').animate({height:40},300).get(0).isMinimized = true;
        $(this).hide();
        $('#windowResize_BLUE').hide();
        $('#windowMax_BLUE').show();
      }
    );
    $('#windowMax_BLUE').bind(
      'click',
      function()
      {
        var windowSize_BLUE = $.iUtil.getSize(document.getElementById('windowContent_BLUE'));
        $('#windowContent_BLUE').SlideToggleUp(300);
        $('#windowBottom_BLUE, #windowBottomContent_BLUE').animate({height: windowSize.hb + 13}, 300);
        $('#window_BLUE').animate({height:windowSize_BLUE.hb+43}, 300).get(0).isMinimized = false;
        $(this).hide();
        $('#windowMin_BLUE, #windowResize_BLUE').show();
      }
    );
    $('#window_BLUE').Resizable(
      {
        minWidth: 200,
        minHeight: 60,
        maxWidth: 700,
        maxHeight: 400,
        dragHandle: '#windowTop_BLUE',
        handlers: {
          se: '#windowResize_Two'
        },
        onResize : function(size, position) {
          $('#windowBottom_BLUE, #windowBottomContent_BLUE').css('height', size.height-33 + 'px');
          var windowContentEl_BLUE = $('#windowContent_BLUE').css('width', size.width - 25 + 'px');
          if (!document.getElementById('window_BLUE').isMinimized) {
            windowContentEl_BLUE.css('height', size.height - 48 + 'px');
          }
        }
      }
    );
  }
);

</script>

<script type="text/javascript">
/*$(document).ready(
  function()
  {
    $('#windowOpen_RED').bind(
      'click',
      function() {
        if($('#window_RED').css('display') == 'none') {
          $(this).TransferTo(
            {
              to:'window_RED',
              className:'transferer_Two', 
              duration: 400,
              complete: function()
              {
                $('#window_RED').show();
              }
            }
          );
        }
        this.blur();
        return false;
      }
    );
    $('#windowClose_Two').bind(
      'click',
      function()
      {
        $('#window_RED').TransferTo(
          {
            to:'windowOpen_RED',
            className:'transferer_Two', 
            duration: 400
          }
        ).hide();
      }
    );
    $('#windowMin').bind(
      'click',
      function()
      {
        $('#windowContent_RED').SlideToggleUp(300);
        $('#windowBottom_RED, #windowBottomContent_RED').animate({height: 10}, 300);
        $('#window_RED').animate({height:40},300).get(0).isMinimized = true;
        $(this).hide();
        $('#windowResize_RED').hide();
        $('#windowMax_RED').show();
      }
    );
    $('#windowMax_RED').bind(
      'click',
      function()
      {
        var windowSize_RED = $.iUtil.getSize(document.getElementById('windowContent_RED'));
        $('#windowContent_RED').SlideToggleUp(300);
        $('#windowBottom_RED, #windowBottomContent_RED').animate({height: windowSize.hb + 13}, 300);
        $('#window_RED').animate({height:windowSize_RED.hb+43}, 300).get(0).isMinimized = false;
        $(this).hide();
        $('#windowMin_RED, #windowResize_RED').show();
      }
    );
    $('#window_RED').Resizable(
      {
        minWidth: 200,
        minHeight: 60,
        maxWidth: 700,
        maxHeight: 400,
        dragHandle: '#windowTop_RED',
        handlers: {
          se: '#windowResize_Two'
        },
        onResize : function(size, position) {
          $('#windowBottom_RED, #windowBottomContent_RED').css('height', size.height-33 + 'px');
          var windowContentEl_RED = $('#windowContent_RED').css('width', size.width - 25 + 'px');
          if (!document.getElementById('window_RED').isMinimized) {
            windowContentEl_RED.css('height', size.height - 48 + 'px');
          }
        }
      }
    );
  }
);
*/
</script>

