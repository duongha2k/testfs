<?php $page = "clients"; ?>
<?php $option = "wos"; ?>
<?php 
require ("../headerStartSession.php");
if ($_SESSION["UserType"] != "InstallDesk") 
	header("Location: ../");
?>
<?php $selected = 'createWO'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php require ("includes/adminCheck.php"); ?>
<?php require ("includes/PMCheck.php"); ?>
<?php require ("includes/getCompanyName.php"); ?>
<?php require ("../library/googleMapAPI.php"); ?>
<!-- Add Content Here -->

<?php
	require_once("../library/caspioAPI.php");
	$result = caspioSelectAdv(Client_Active_Projects, "TR_Client_Projects_Project_Name, TR_Client_Projects_Project_ID, TR_Client_Projects_Project_Company_ID", "", "TR_Client_Projects_Project_Name ASC", TRUE, "\"", "^");
	$projectList = str_replace("\"", "", implode("|", $result));
?>

<script type="text/javascript">
	function onReady() {
		initialize();
		$("#InsertRecordZipcode").change(mapShowAddress);
		onPointReturned = saveGeocode;
		document.getElementById("map_canvas").style.display = "none";
		$("#InsertRecordCompany_Name").attr("value", myCompanyName);
	}
	
	function saveGeocode(point) {
		$("#InsertRecordLatitude").attr("value", point.lat());
		$("#InsertRecordLongitude").attr("value", point.lng());
	};
</script>
<?php
	googleMapAPIJS("onReady");
?>

<script type="text/javascript">
	var projectList = "<?=$projectList?>";
	// requires for date filter dropdown outside the WO dashboard
	var v = "<?php echo $_GET["v"] ?>";
	function filterDate() {
		dropdown = document.getElementById('WOoptions');
		document.location = "wos.php?v=" + v + "&show=" + dropdown.selectedIndex;
	}
</script>


<br />
<div id="map_canvas"></div>

<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b00002a6a77d184e145d28cee","https:");}catch(v_e){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b00002a6a77d184e145d28cee">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>

<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
