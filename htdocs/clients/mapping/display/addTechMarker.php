    if (GBrowserIsCompatible()) { var arrLocalMarkers = [];
<?php
    $i = 0;
    $markerNameDef = 'marker' . time() . 'n';

    foreach ($res as $value) {
        $markerType = count($value)>1 ? 'multi' : 'one';
        echo "var markerIcon = new GIcon();";
        if (!empty($_POST['markerColor'])) {
            echo "markerIcon.image='". Map::getTechMarkerUrl($_POST['markerColor'],$markerType) ."';markerIcon.iconSize=new GSize(26,37); markerIcon.iconAnchor=new GPoint(9,31);markerIcon.infoWindowAnchor=new GPoint(9,31);markerIcon.shadow='';";
        } else {
            echo "markerIcon.image='". Map::getTechMarkerUrl('red',$markerType) ."';markerIcon.iconSize=new GSize(26,37); markerIcon.iconAnchor=new GPoint(9,31);markerIcon.infoWindowAnchor=new GPoint(9,31);markerIcon.shadow='';";
        }
        $markerName =  $markerNameDef.$i;
        
        echo "techArr = new Array();";
        
        if(count($value)>1) {
            $infoTech = '<div style=\'overflow-y:scroll;width:300px;height:195px;\'><b>'.count($value).' technicians:</b><br/><br/>';
        } else {
            $infoTech = '<div>';
        }
        $email = array();
        $lat = '';
        $lon = '';

        foreach ($value as $key=>$row) {
            $lat = $row['Latitude'];
            $lon = $row['Longitude'];
            $email[] = addslashes(htmlspecialchars($row['PrimaryEmail']));
            if (empty($row['Latitude']) || empty($row['Longitude'])) continue;
    
        	$infoTech .= '<b>Tech ID:</b> <a href=\"../wosViewTechProfile.php?TechID='. (int)$row['TechID'] .'&v='. addslashes(htmlspecialchars($companyID)) .'\" target=\"_blank\">'. (int)$row['TechID'] .'</a><b><br/>';
        	$infoTech .= addslashes(htmlspecialchars($row['FirstName'])).' '.addslashes(htmlspecialchars($row['LastName'])).'</b><br />';
            $flsid ='';
        	if ($companyID == 'FLS' && !empty($row['FLSID']) && $row['FLSID'] != 'NULL'){
        		$infoTech .= 'FLS ID: '.$row['FLSID'].' <br/>';
        		$flsid = $row['FLSID'];
            }
        	if (!empty($row['PrimPhoneType'])) $infoTech .= addslashes(htmlspecialchars($row['PrimPhoneType'])) . ': ';
    
        	$infoTech .= addslashes(htmlspecialchars($row['PrimaryPhone'])) . '<br/>';
        	$infoTech .= '<a href=\"mailto:' . addslashes(htmlspecialchars($row['PrimaryEmail'])) . '\">' . addslashes(htmlspecialchars($row['PrimaryEmail'])) . '</a><br/>';
        	$infoTech .= addslashes($row['Address1']) . ' ';
    
        	if (!empty($row['Address2'])) $infoTech .=  addslashes($row['Address2']) .' ';
        	if (!empty($row['City']))     $infoTech .=  ', '.addslashes($row['City']) .' ';
        	if (!empty($row['State']))    $infoTech .=  ', '.addslashes($row['State']) .' ';
        	if (!empty($row['ZipCode']))  $infoTech .=  ', '.addslashes($row['ZipCode']) .' ';
			
			$infoTech .= "<br/>";
    
        	if ($row['Preferred'] == 'Yes') $infoTech .= 'Preferred: Yes<br/>'; else $infoTech .= 'Preferred: No<br/>';

        	$infoTech .= 'Metrics:<br/>';
        	$infoTech .= 'IMAC Calls: '. (int)$row['Qty_IMAC_Calls'] .'<br/>';
        	$infoTech .= 'Service Calls: '. (int)$row['Qty_FLS_Service_Calls'] .'<br/>';
            $infoTech .= 'No Shows: '. (int)$row['No_Shows'] .'<br/>Back Outs: '. (int)$row['Back_Outs'] .'<br/>';	
			
			$row['SATRecommendedAvg'] = $row['SATRecommendedAvg'] == "NULL" ? 0 : $row['SATRecommendedAvg'];
			$row['SATRecommendedTotal'] = $row['SATRecommendedTotal'] == "NULL" ? 0 : $row['SATRecommendedTotal'];
			$row['SATPerformanceAvg'] = $row['SATPerformanceAvg'] == "NULL" ? 0 : $row['SATPerformanceAvg'];
			$row['SATPerformanceTotal'] = $row['SATPerformanceTotal'] == "NULL" ? 0 : $row['SATPerformanceTotal'];

        	$infoTech .= 'Preference: '. number_format((float)$row['SATRecommendedAvg'],1) .' ('.(int)$row['SATRecommendedTotal'] . ')<br/>';
        	$infoTech .= 'Performance: '. number_format((float)$row['SATPerformanceAvg'],1) .' ('.(int)$row['SATPerformanceTotal'] . ')<br/><br />';

            echo "techArr[techArr.length]=new markerTech('".addslashes($row['TechID'])."','".addslashes($row['LastName'])."','".addslashes($row['FirstName'])."','".addslashes($row['PrimaryPhone'])."','".addslashes($row['PrimPhoneType'])."','".addslashes($row['PrimaryEmail'])."','".addslashes($row['City'])."','".addslashes($row['State'])."','".addslashes($row['ZipCode'])."','".addslashes($row['Address1'])."','".addslashes($row['Address2'])."','".addslashes($row['No_Shows'])."','".addslashes($row['Back_Outs'])."','".addslashes($row['Qty_IMAC_Calls'])."','".addslashes($row['Qty_FLS_Service_Calls'])."','".addslashes($row['Latitude'])."','".addslashes($row['Latitude'])."','".addslashes($row['Preferred'])."','".addslashes($flsid)."','".addslashes($row['SATRecommendedAvg'])."','".addslashes($row['SATRecommendedTotal'])."','".addslashes($row['SATPerformanceAvg'])."','".addslashes($row['SATPerformanceTotal'])."');";
        }
        $infoTech .= '</div>';
        echo "var point=new GLatLng('" . $lat . "','" . $lon . "');var ". $markerName ."=new GMarker(point,markerIcon);GEvent.addListener(". $markerName .",'click',function(){". $markerName .".openInfoWindowHtml(\"". $infoTech ."\");});arrLocalMarkers.push(". $markerName .");points.push(new objMarker('". $i ."',point,'". implode(',', $email) ."','". $_POST['markerColor'] ."',techArr));";
        $i++;
    }
    if (!empty($centerLatLong[0]) && !empty($centerLatLong[1]))
        echo "setTimeout(function(){setMapCenter('" . $centerLatLong[0] . "','" . $centerLatLong[1] . "',9);},2500);";
    else
        echo "setTimeout(function(){if(goomap.getZoom()>7){goomap.zoomOut();goomap.zoomOut();}},2500);";
    ?>
    localCluster=createCluster('<?php echo $_POST['markerColor'];?>', '<?php echo Map::getTechClusterUrl($_POST['markerColor']);?>');localCluster._totalMarkers='<?php echo $rec_count;?>';localCluster.addMarkers(arrLocalMarkers); arrLocalMarkers=[];refreshMapDelayed();}