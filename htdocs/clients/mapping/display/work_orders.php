<?php
require("../headerStartSession.php");
require_once("../library/caspioAPI.php");

if ($_SESSION['loggedIn'] != "yes" || $_SESSION['loggedInAs'] != "client") {
		exit;
}

$companyID = '';

if (!empty($_SESSION['Company_ID'])) {
    $companyID = $_SESSION['Company_ID'];
}

if (!empty($_POST['PMCompany'])) {
    $companyID = $_POST['PMCompany'];
}

$companyIdEscaped = mysql_escape_string($companyID);

$sqlFilter = '';

if (!empty($_POST['State']) && $_POST['ZoomIn'] == 1) {
	$zoomInState = $_POST['State'];
	$zoomIn = $_POST['ZoomIn'];
}

if (!empty($_POST['Project']) && ($_POST['Project'] > 0)) {
    $sqlFilter .= ' AND (Project_ID = \'' . (int)$_POST['Project'] . '\')';
}

if (!empty($_POST['StartDate'])) {
    $sqlFilter .= ' AND StartDate >= \'' . mysql_escape_string($_POST['StartDate']) . '\'';
}

if (!empty($_POST['EndDate'])) {
    $sqlFilter .= ' AND StartDate <= \'' . mysql_escape_string($_POST['EndDate']) . '\'';
}

if (!empty($_POST['category'])) {
    $sqlFilter .= ' AND WO_Category_ID = \'' . (int)$_POST['category'] . '\'';
}

if (!empty($_POST['WorkOrder'])) {
	switch ($_POST['WorkOrder']){
		case 'Published':
			$sqlFilter .= ' AND ShowTechs = 1 AND Deactivated = 0 AND Tech_ID IS NULL';
			break;
		case 'Created':
			$sqlFilter .= ' AND ShowTechs = 0 AND Deactivated = 0 AND Tech_ID IS NULL';
			break;
		case 'Assigned':
			//$sqlFilter .= ' AND Deactivated = 0 AND Tech_ID IS NOT NULL AND TechMarkedComplete = 0 AND MissingComments IS';
			$sqlFilter .= ' AND Deactivated = 0 AND Tech_ID IS NOT NULL AND TechMarkedComplete = 0 AND (MissingComments IS NULL OR MissingComments LIKE \'\')';
			break;
		case 'Work_Done':
			$sqlFilter .= ' AND TechMarkedComplete = 1 AND Approved = 0 AND TechPaid = 0 AND Invoiced = 0 AND Deactivated = 0';
			break;
		case 'Approved':
			$sqlFilter .= ' AND Approved = 1 AND Invoiced = 0';
			break;
		case 'In_Accounting':
			$sqlFilter .= ' AND Invoiced = 1 AND TechPaid = 0';
			break;
		case 'Incomplete':
			$sqlFilter .= " AND Deactivated = 0 AND TechMarkedComplete=0 AND Approved=0 AND TechPaid=0 AND Invoiced=0 AND datalength(MissingComments) >0 AND MissingComments IS NOT NULL";
			break;
		case 'Complete':
			$sqlFilter .= ' AND TechPaid = 1';
			break;
		case 'Deactivated':
			$sqlFilter .= ' AND Deactivated = 1';
			break;
		case 'All':
			break;
		default:
			break;
	}
}
$sqlFilter .= ' AND Latitude IS NOT NULL AND Longitude IS NOT NULL';
$fieldList = 'TB_UNID,Deactivated,Latitude,Longitude,Tech_ID,TechMarkedComplete,WO_ID,StartDate,Project_ID,SiteName,Address,City,State,Zipcode,Tech_Bid_Amount,ShowTechs,StartTime,Amount_Per,ShowTechs,PayMax';
$workOrders = caspioSelectAdv("Work_Orders", $fieldList, "Company_ID LIKE '" . $companyIdEscaped . "'" . $sqlFilter, "", false, "`", "|");
$arrWoLocations  = array();

if ($workOrders !== false) {
	if (count($workOrders) == 1 && $workOrders[0] == null) {
		$resultCount = 0;
	} else {
		$resultCount = count($workOrders);
	}
} else {
	$resultCount = 0;
}

$projectsIds = array();
$techsIds    = array();

if ($resultCount) {
//add used marker color to session
if (!empty($_POST['markerColor'])) {
    Map::addUsedColorToSession($_POST['markerColor']);
}
echo "document.getElementById('msgDiv').innerHTML = 'Displaying " .$resultCount ." work orders';";
foreach ($workOrders as $keyRecord => &$record) {

    $workRecord = explode("|", $record);
    $woId = (int)trim($workRecord[0], "`");

    $arrWoLocations[$woId]['Latitude'] = trim($workRecord[2], "`");
    $arrWoLocations[$woId]['Longitude'] = trim($workRecord[3], "`");

    $woName      = addslashes(strip_tags(trim($workRecord[6], "`")));
    $startTime   = trim($workRecord[16], "`");
    $startDate   = trim(substr(trim($workRecord[7], "`"),0,-11));
    $projectId   = (int)trim($workRecord[8], "`");
    $site        = addslashes(strip_tags(trim($workRecord[9], "`")));
    $address     = addslashes(strip_tags(trim($workRecord[10], "`")));
    $city        = addslashes(strip_tags(trim($workRecord[11], "`")));
    $state       = addslashes(strip_tags(trim($workRecord[12], "`")));
    $Zip         = (int)trim($workRecord[13], "`");
    $bidAmount   = addslashes(strip_tags(trim($workRecord[14], "`")));
    $techId      = (int)trim($workRecord[4], "`");
    $published   = trim($workRecord[15], "`");
    $amountPer   = trim($workRecord[17], "`");
    $deactivated = trim($workRecord[1], "`");
    $showTechs   = trim($workRecord[18], "`");
    $payMax      = trim($workRecord[19], "`");

    $start = '';
    if (!empty($startDate) && $startDate != 'NULL')
    	$start .= $startDate.' ';
    if (!empty($startTime) && $startTime != 'NULL')
        $start .= $startTime;
    $location    = '';
    if (!empty($address) && $address!='NULL')
        $location .= $address.', ';
    if (!empty($city) && $city!='NULL')
        $location .= $city.' ';
    if (!empty($state) && $state!='NULL')
        $location .= $state.', ';
    if (!empty($Zip) && $Zip!='NULL')
        $location .= $Zip;
    
    // Get the project details
    if (!isset($arrProjectsIds[$projectId]) && $projectId > 0) {
    	$projectsIds[] = (int)$projectId;
    }
    // Get the assigned Tech details
    if ($techId > 0 && !isset($arrTechsAssigned[$techId])) {
    	$techsIds[] = (int)$techId;
    }
    $arrWoLocations[$woId]['pub']        = ($deactivated=='False' && $techId=='NULL') ? 1 : 0;
    $arrWoLocations[$woId]['TB_UNID']    = $woId;
    $arrWoLocations[$woId]['woID']       = $woName;
    $arrWoLocations[$woId]['Zip']        = $Zip!='NULL' ? $Zip : '';
    $arrWoLocations[$woId]['City']       = $city!='NULL' ? $city : '';
    $arrWoLocations[$woId]['State']      = $state!='NULL' ? $state : '';
    $arrWoLocations[$woId]['PayMax']     = $payMax;
    $arrWoLocations[$woId]['Start']      = $start;
    $arrWoLocations[$woId]['projectId']  = $projectId;
    $arrWoLocations[$woId]['techId']     = $techId;
    $arrWoLocations[$woId]['Amount_Per'] = $amountPer!='NULL' ? $amountPer : '';
    
    $arrWoInfo[$woId] = '<b>WIN#: </b> <a href=\"../wosDetails.php?v='.urlencode($companyID).'&id='.urlencode($woId).'\" target=\"_blank\">'.$woId.'</a><br />';
    $arrWoInfo[$woId] .= '<b>Client WO ID: </b>'.$woName.'<br />';
    if (!empty($start))
        $arrWoInfo[$woId] .= '<b>Start date/time:</b> '.$start.'<br />';

    if (!empty($site) && $site != 'NULL') {
        $arrWoInfo[$woId] .= '<b>Site:</b> ' . $site . '<br />';
        $arrWoLocations[$woId]['Site'] = $site;
    } else {
    	$arrWoLocations[$woId]['Site'] = '';
    }
    if (!empty($location)) {
    	$arrWoInfo[$woId] .= '<b>Location:</b> ' . $location . '<br />';
    }
    $arrWoLocations[$woId]['Location'] = $location;

    if (!empty($bidAmount) && $bidAmount != 'NULL') {
        $arrWoInfo[$woId] .= '<b>Bid amount:</b> $'.$bidAmount;
        if (!empty($amountPer) && $amountPer != 'NULL')
            $arrWoInfo[$woId] .= ' per '.$amountPer;
        $arrWoInfo[$woId] .= '<br />';
        $arrWoLocations[$woId]['Bid_amount'] = $bidAmount;
    } else {
    	$arrWoLocations[$woId]['Bid_amount'] = '';
    }
}
$projectsIds = implode(",", $projectsIds);

//$projectDetails = caspioSelectAdv("TR_Client_Projects", 'Project_Name,Project_ID', "Project_ID IN (".$projectsIds.")", "", false, "`", "|");
$db = Zend_Registry::get('DB');
$sql = "SELECT Project_Name, Project_ID FROM projects WHERE Project_ID IN (".$projectsIds.") ORDER BY Project_Name ASC";
$projectDetails = $db->fetchAll($sql, array(), Zend_Db::FETCH_NUM);
if (!empty($projectDetails[0])) {
	
	$projDet = array();
	foreach ($projectDetails as $record) {
		$projDet[$record[1]] = addslashes(strip_tags(trim($record[0], "`'")));
	}
	foreach ($arrWoLocations as $key=>&$row) {
		if (!empty($row['projectId']) && !empty($projDet[$row['projectId']])) {
	        $arrWoInfo[$key] .= '<b>Project:</b> '. $projDet[$row['projectId']] .'<br />';
	        $row['Project'] .= $projDet[$row['projectId']];
	    } else {
	    	$row['Project'] .= '';
	    }
	}
}

$techsIds = implode(",", $techsIds);

$techDetails = caspioSelectAdv("TR_Master_List", 'FirstName,LastName,TechID', "TechID IN (".$techsIds.")", "", false, "`", "|");
if (!empty($techDetails[0])) {
	$techDet = array();
	foreach ($techDetails as $record) {
		$record = explode("|", $record);
		$techDet[$record[2]]['fName'] = addslashes(strip_tags(trim($record[0], '`')));
		$techDet[$record[2]]['lName'] = addslashes(strip_tags(trim($record[1], '`')));
	}
	foreach ($arrWoLocations as $key=>&$row) {
		if (!empty($row['techId'])) {
	        $arrWoInfo[$key] .= '<b>Assigned Tech:</b> ' . $techDet[$row['techId']]['fName'] . ' ' . $techDet[$row['techId']]['lName'] . '<br />';
	        $row['Tech'] = $techDet[$row['techId']]['fName']. ' ' .$techDet[$row['techId']]['lName'];
	    } else {
	    	$row['Tech'] = '';
	    }
	}
}

?>

if (GBrowserIsCompatible()) {

    var arrLocalMarkers = [];
<?php

$markerNameDef = 'marker'. time() .'n';
$wos = array();

foreach ($arrWoLocations as $key=>$row) {
	$i = (string)$row['Latitude'].(string)$row['Longitude'];
	$q = $row;
	$q['TB_UNID'] = $key;
	$wos[$i][] = $q;
}

$i = 0;
foreach ($wos as $marker) {
    $markerType = count($marker)>1 ? 'multi' : 'one';
    echo "var markerIcon = new GIcon();";
    if (!empty($_POST['markerColor'])) {
        echo 'markerIcon.image  = "'. Map::getWoMarkerUrl($_POST['markerColor'],$markerType) .'";';
        echo "
		markerIcon.iconSize=new GSize(26, 37);
		markerIcon.iconAnchor=new GPoint(9, 31);
		markerIcon.infoWindowAnchor=new GPoint(9, 31);
		markerIcon.shadow='';
        ";
    } else {
        echo 'markerIcon.image = "'. Map::getWoMarkerUrl('red',$markerType) .'";';
        echo "
		markerIcon.iconSize=new GSize(26, 37);
		markerIcon.iconAnchor=new GPoint(9, 31);
		markerIcon.infoWindowAnchor=new GPoint(9, 31);
		markerIcon.shadow='';
        ";
    }
    
    $markerName =  $markerNameDef.$i;
    $lat = '';
    $lon = '';
    echo "woArr = new Array();";
    if(count($marker)>1) {
        $info = '<div style=\'overflow-y:scroll;width:300px;height:195px;\'><b>'.count($marker).' work orders:</b><br/><br/>';
    } else {
        $info = '<div>';
    }
	foreach ($marker as $value) {
		$info .= $arrWoInfo[$value['TB_UNID']].'<br />';
		$lat = $value['Latitude'];
        $lon = $value['Longitude'];
        echo "woArr[woArr.length] = new markerWo('".$value['projectId']."','".$value['techId']."','".$value['TB_UNID']."','".$value['woID']."','".$value['Start']."','".$value['Site']."','".$value['City']."','".$value['State']."','".$value['Zip']."','".$value['Bid_amount']."','".$value['Amount_Per']."','".$value['Project']."','".$value['Tech']."','".$value['Latitude']."','".$value['Latitude']."','".$value['pub']."','".$value['PayMax']."');";
	}
	$info .= '</div>';
    echo "var point = new GLatLng('". $lat ."','". $lon ."'); var ". $markerName ." = new GMarker(point, markerIcon);";
    echo "GEvent.addListener(". $markerName .",'click', function(){". $markerName .".openInfoWindowHtml(\"". $info ."\");});
    arrLocalMarkers.push(". $markerName .");pointsWo.push(new objMarkerWo('". $i ."',point,'".$value['Start']."','".$value['Project']."','".$value['Site']."','".$value['Location']."','".$value['Bid_amount']."','".$value['Amount_Per']."','".$value['Tech']."','". $_POST['markerColor'] ."',woArr));";
    $i++;
}

if ($zoomIn == 1 && !empty($zoomInState)) {
    echo "setCenterToAddress('".$zoomInState.",USA');";
} else {
    echo "setTimeout(function(){if (goomap.getZoom() > 7 ) {goomap.zoomOut(); goomap.zoomOut();}}, 2500);";
}
?>
localCluster = createCluster('<?php echo $_POST['markerColor'];?>', '<?php echo Map::getWoClusterUrl($_POST['markerColor']);?>');
localCluster._totalMarkers = '<?php echo $rec_count;?>'; localCluster.addMarkers(arrLocalMarkers); arrLocalMarkers = []; refreshMapDelayed();
}
<?php
}
?>
