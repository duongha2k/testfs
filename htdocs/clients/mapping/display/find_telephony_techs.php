<?php
    require("../headerStartSession.php");
    require_once("../library/Map/Control.php");
    require_once("../library/googleMapAPI.php");
    //require("includes/adminCheck.php");
    //require_once("includes/PMCheck.php");
    require_once("../library/caspioAPI.php");
    
    if ($_SESSION['loggedIn'] != "yes" || $_SESSION['loggedInAs'] != "client") {exit;}
    
    $companyID = '';
    if (!empty($_SESSION['Company_ID']))
        $companyID = $_SESSION['Company_ID'];
    
    if (!empty($_POST['PMCompany']))
        $companyID = $_POST['PMCompany'];
    
    $companyIdEscaped = mysql_escape_string($companyID);
    
    $params = $_POST;
    unset($params['formType']);
    unset($params['markerColor']);
    unset($params['hideBanned']);
    unset($params['preferredOnly']);
    unset($params['PMCompany']);

function getUserSearchCriteria($params, $mapOverlayTechs = array()) {
    // coverts search form fields into SQL search criteria
    $ignoreFields = array("doSearch" => 1, "doReset" => 1, "hideBanned" => 1, "preferredOnly" => 1, "newSearchBtn" => 1, "ProximityZipCode" => 1 , "ProximityLat" => 1 , "ProximityLng" => 1, "ProximityDistance" => 1); // ignores these fields on the search form
    $searchCriteria = array();
    foreach ($params as $key => $value) {
        // parse searchCriteria
        if (array_key_exists($key, $ignoreFields) || $value == "") continue; // ignore blank fields and certain fields
        $value = caspioEscape($value);
        if ($value == "Y") $value = 1;
        if ($value == "N") $value = 0;
        switch ($key) {
			case "FLSIDExists":
				// field not blank
				if ($value == "1")
					$searchCriteria[] = "ISNULL(FLSID, '') <> ''";
				break;
            case "CompTIA":
                $searchCriteria[] = "ISNULL($key, '') <> ''";
                break;
            case "DellCert":
                $searchCriteria[] = "$key LIKE '%$value%'";
                break;
            case "HourlyPay":
                $searchCriteria[] = "$key <= '$value'";
                break;
            case "No_Shows":
                if (!is_numeric($value))
                    throw new Exception("$key|must be numeric");
                $searchCriteria[] = "$key <= '$value'";
                break;
            case "Qty_IMAC_Calls":
			case "SATRecommendedAvg":
			case "SATPerformanceAvg":
			case "SATRecommendedTotal":
			case "SATPerformanceTotal":
            case "Qty_FLS_Service_Calls":
                $searchCriteria[] = "$key >= '$value'";
                break;
            default:
                $searchCriteria[] = "$key = '$value'";
                break;
        }
    }
    return $searchCriteria;
}

    try {
        $userSearchCriteria = getUserSearchCriteria($params);
    }catch (Exception $e) {
        // invalid field entered
        $error = explode("|", $e->getMessage());
    }

    // fields pulled from DB
    $specialColumns = array("AudioResume" => "(CASE WHEN CAST(ISNULL(AudioResume, '') AS varchar) = '' THEN 'No' ELSE 'Yes' END)", 
                           // "PreferLevel" => "(CASE WHEN(SELECT TOP 1 PreferLevel FROM Client_Preferred_Techs WHERE CompanyID = '$companyIdEscaped' AND TechID = Tech_ID) IS NOT NULL THEN 'Yes' ELSE 'No' END)", 
                            "SpecialDistance" => "calculated_distance");
	$preferredList = Core_Tech::getClientPreferredTechsArray($companyIdEscaped);
	(!empty($preferredList)) ? $specialColumns["PreferLevel"] = '(CASE WHEN(TechID IN ('.implode(",",$preferredList). ') THEN "Yes" ELSE "No" END)' : $specialColumns["PreferLevel"] = "No";
    
    $fieldList = "TechID,LastName,FirstName,PrimaryPhone,PrimPhoneType,PrimaryEmail,Address1,Address2,City,State,Qty_IMAC_Calls,Qty_FLS_Service_Calls,No_Shows,Back_Outs,FS_Cert_Test_Pass,
                  ZipCode,No_Shows,Back_Outs,Qty_IMAC_Calls,Qty_FLS_Service_Calls,Latitude,Longitude,FLSID,SATRecommendedAvg,SATRecommendedTotal,SATPerformanceAvg,SATPerformanceTotal";

    $preferredCriteria = '';
    if (!empty($_POST['preferredOnly'])){
		$preferredList = Core_Tech::getClientPreferredTechsArray($companyIdEscaped);
	    (!empty($preferredList)) ? $preferredCriteria .=  ' AND TechID IN ('.implode(",",$preferredList). ')' : $preferredCriteria .= "";
	   // $preferredCriteria = " AND TechID IN (SELECT TechID FROM Client_Preferred_Techs WHERE CompanyID = '" . $companyIdEscaped . "' AND TechID = Tech_ID)";
	}
    
    $clientDeniedCriteria = '';

	if (!empty($_POST['hideBanned'])) {
		$deniedList = Core_Tech::getClientDeniedTechsArray($companyIdEscaped);
		if (!empty($deniedList))
		    $clientDeniedCriteria = ' AND TechID NOT IN (' . implode(',',$deniedList) . ')';
	}

    $userSearchCriteria = implode(" AND ", $userSearchCriteria);
    if ($userSearchCriteria != "") $userSearchCriteria = "AND " . $userSearchCriteria;

    $searchCriteria = " AND ((Private = '0' OR (SELECT Client_ID FROM ISO WHERE UNID = ISO_Affiliation_ID) = '$companyIdEscaped') AND AcceptTerms = 'Yes' $userSearchCriteria $preferredCriteria $clientDeniedCriteria)";
    //take only techs with coordinates
    $searchCriteria .= " AND Latitude IS NOT NULL AND Longitude IS NOT NULL AND Deactivated <> 'True'";
    $proximitySearch = false;

    $quickResult = array();
    $searchResult = array();
    $records = array();
    // do quick proximity search
    if ($params["ProximityZipCode"] != "")  {
        $params["ProximityZipCode"]  = caspioEscape($params["ProximityZipCode"]);
        $params["ProximityDistance"] = caspioEscape($params["ProximityDistance"]);
        $centerLatLong = array(caspioEscape($params["ProximityLat"]), caspioEscape($params["ProximityLng"]));

        $quickResult = caspioProximitySearchByCoordinatesRaw("Tech_Telephony_Skills_View", true, $centerLatLong[0], $centerLatLong[1], "Latitude", "Longitude", "<= {$params["ProximityDistance"]}.49", "3", "TechID, PrimaryEmail, {$specialColumns["PreferLevel"]}", $searchCriteria, "", false, "|", "`", false);
        $proximitySearch = true;
    } else {
        $quickResult = caspioSelectAdv("Tech_Telephony_Skills_View", "TechID, PrimaryEmail, {$specialColumns["PreferLevel"]}", "TechID = TechID " . $searchCriteria, "", true, "`", "|", false);
    }

    $numResult = (sizeof($quickResult) != 1 || $quickResult[0] != "") ? sizeof($quickResult) : 0;

    $res = array();
    if ($numResult > 0) {
        $techList = array();
        $preferredTechList = array();
        
        foreach ($quickResult as $row) {
            $row = explode("|", $row);
            $techList[] = $row[0];
            $preferredTechList[(int)trim($row[0], "`'")] = trim($row[2], "`'");
        }
        $techsMatchingSearchCriteria = "TechID IN ('".implode("','", $techList)."')";

        if ($proximitySearch) {
            $searchResult = caspioProximitySearchByCoordinatesRaw("Tech_Telephony_Skills_View", true, $centerLatLong[0], $centerLatLong[1], "Latitude", "Longitude", "<= {$params["ProximityDistance"]}.49", "3", $fieldList, 'AND '.$techsMatchingSearchCriteria, 'TechID', true, "|", "`", false);
        } else {
            $searchResult = caspioSelectAdv("Tech_Telephony_Skills_View", $fieldList, $techsMatchingSearchCriteria, 'TechID', true, "`", "|", false);
        }

        $arrFieldList = explode(',', $fieldList);

        foreach ($searchResult as $key=>&$row) {
            $tmp = explode("|", $row);
            $arrColumn = array();

            foreach ($tmp as $colnumb=>&$column) {
                $arrColumn[$arrFieldList[$colnumb]] = trim($column, "`");
            }
            $records[$key] = $arrColumn;
    	}
    
        foreach ($records as $k=>$r) {
            $i = (string)$r['Latitude'].(string)$r['Longitude'];
            $techInfo = $r;
            $techInfo['Preferred'] = $preferredTechList[(int)trim($r['TechID'], "`'")];
            $res[$i][] = $techInfo;
        }
    }
    
    $rec_count = count($records);
    
if ($rec_count) {
//add used marker color to session
if (!empty($_POST['markerColor'])) {
    Map::addUsedColorToSession($_POST['markerColor']);
}
echo "document.getElementById('msgDiv').innerHTML='Displaying " .$rec_count ." technicians';";
include("addTechMarker.php");

}