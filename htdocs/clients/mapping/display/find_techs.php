<?php
/**
 * @author Sergey Petkevich
 */
require("../headerStartSession.php");
require_once("../library/Map/Control.php");
require_once("../library/googleMapAPI.php");
//require("includes/adminCheck.php");
//require_once("includes/PMCheck.php");
require_once("../library/caspioAPI.php");

if ($_SESSION['loggedIn'] != "yes" || $_SESSION['loggedInAs'] != "client") {exit;}

$companyID = '';
if (!empty($_SESSION['Company_ID']))$companyID = $_SESSION['Company_ID'];
if (!empty($_POST['PMCompany']))$companyID = $_POST['PMCompany'];

$companyIdEscaped = mysql_escape_string($companyID);
$params = $_POST;

unset($params['formType']);
unset($params['markerColor']);
unset($params['hideBanned']);
unset($params['preferredOnly']);
unset($params['PMCompany']);

$showBanned = (isset($params["sb"]) ? true : false);
$showNonPreferred = (isset($params["snp"]) ? true : false);

function getUserSearchCriteria($params) {
        // coverts search form fields into SQL search criteria
        $ignoreFields = array("doSearch" => 1, "doReset" => 1, "hideBanned" => 1, "preferredOnly" => 1, "newSearchBtn" => 1, "ProximityZipCode" => 1 , "ProximityLat" => 1 , "ProximityLng" => 1, "ProximityDistance" => 1); // ignores these fields on the search form
        $searchCriteria = array();
        foreach ($params as $key => $value) {
            // parse searchCriteria
            if (array_key_exists($key, $ignoreFields) || $value == "") continue; // ignore blank fields and certain fields
            $value = caspioEscape($value);
            if ($value == "Y") $value = 1;
            if ($value == "N") $value = 0;
            switch ($key) {
				case "FLSIDExists":
					// field not blank
					if ($value == "1")
						$searchCriteria[] = "ISNULL(FLSID, '') <> ''";
					break;
                case "CompTIA":
                    $searchCriteria[] = "ISNULL($key, '') <> ''";
                    break;
                case "DellCert":
                    $searchCriteria[] = "$key LIKE '%".addslashes($value)."%'";
                    break;
                case "No_Shows":
                    if (!is_numeric($value))
                        throw new Exception("$key|must be numeric");
                    $searchCriteria[] = "$key <= '$value'";
                    break;
                case "Qty_IMAC_Calls":
                case "SATRecommendedAvg":
                case "SATPerformanceAvg":
                case "Qty_FLS_Service_Calls":
					if (!is_numeric($value))
						throw new Exception("$key|must be numeric");
					$searchCriteria[] = "$key >= '$value'";
                    break;
                case "selfRating":
                	foreach ($value as $k=>$val) {
                		if (!is_numeric($val))
                        throw new Exception("$k|must be numeric");
                		$searchCriteria[] = "$k >= '$val'";
                	}
                	break;
                case "Bg_Test_Pass_Lite":
                    if (!is_numeric($value))
                        throw new Exception("$key|must be numeric");
                    $searchCriteria[] = "$key >= '$value'";
                    break;
				case "HP_CertNum":
					if ($value == "Any HP Cert")
						$searchCriteria[] = "ISNULL(HP_CertNum, '') <> ''";
					break;

                default:
                    $searchCriteria[] = "$key = '$value'";
                    break;
            }
        }
        return $searchCriteria;
    }

$showNonPreferred = (isset($_POST["snp"]) ? true : false);
$userSearchCriteria = getUserSearchCriteria($params);

$searchCriteria = array();
// fields pulled from DB
$specialColumns = array("AudioResume" => "(CASE WHEN CAST(ISNULL(AudioResume, '') AS varchar) = '' THEN 'No' ELSE 'Yes' END)", "SpecialDistance" => "calculated_distance");
$preferredList = Core_Tech::getClientPreferredTechsArray($companyIdEscaped);
(!empty($preferredList)) ? $specialColumns["PreferLevel"] = '(CASE WHEN(TechID IN ('.implode(",",$preferredList). ') THEN "Yes" ELSE "No" END)' : $specialColumns["PreferLevel"] = "No";
	
$fieldList = "TechID,LastName,FirstName,PrimaryPhone,PrimPhoneType,PrimaryEmail,City,State,ZipCode,Address1,Address2,No_Shows,Back_Outs,Qty_IMAC_Calls,Qty_FLS_Service_Calls,Latitude,Longitude,FLSID,SATRecommendedAvg,SATRecommendedTotal,SATPerformanceAvg,SATPerformanceTotal";

$quickResult = array();
$searchResult = array();

$preferredCriteria = '';
if (!empty($_POST['preferredOnly'])){
	$preferredList = Core_Tech::getClientPreferredTechsArray($companyIdEscaped);
   (!empty($preferredList)) ? $preferredCriteria .=  ' AND TechID IN ('.implode(",",$preferredList). ')' : $preferredCriteria .= "";
   // $preferredCriteria = " AND TechID IN (SELECT TechID FROM Client_Preferred_Techs WHERE CompanyID = '" . $companyIdEscaped . "' AND TechID = Tech_ID)";
   
}

$clientDeniedCriteria = '';
if (!empty($_POST['hideBanned'])) {
	$deniedList = Core_Tech::getClientDeniedTechsArray($companyIdEscaped);
	if (!empty($deniedList))
	    $clientDeniedCriteria = ' AND TechID NOT IN (' . implode(',',$deniedList) . ')';
}

$userSearchCriteria = implode(" AND ", $userSearchCriteria);
if ($userSearchCriteria != "") $userSearchCriteria = "AND " . $userSearchCriteria;

// search order
$defaultCriteria = "{$specialColumns["PreferLevel"]} DESC, {$specialColumns["AudioResume"]} DESC";
$searchCriteria = "AND (Private = '0' OR (SELECT Client_ID FROM ISO WHERE UNID = ISO_Affiliation_ID) = '$companyID') AND AcceptTerms = 'Yes' $userSearchCriteria  $clientDeniedCriteria $preferredCriteria";
//take only techs with coordinates
$searchCriteria .= " AND Latitude IS NOT NULL AND Longitude IS NOT NULL AND Deactivated <> 'True'";

if ($params["ProximityZipCode"] != "")  {
    $params["ProximityZipCode"] = caspioEscape($params["ProximityZipCode"]);
    $params["ProximityLat"] = caspioEscape($params["ProximityLat"]);
    $params["ProximityLng"] = caspioEscape($params["ProximityLng"]);
    $params["ProximityDistance"] = caspioEscape($params["ProximityDistance"]);
    $centerLatLong = array($params["ProximityLat"], $params["ProximityLng"]);
    print '/*';
    print_r(array("TR_Master_List", false, $centerLatLong[0], $centerLatLong[1], "Latitude", "Longitude", "<= {$params["ProximityDistance"]}.49", "3", "TechID, PrimaryEmail, {$specialColumns["PreferLevel"]}", $searchCriteria, $defaultCriteria, false, "|", "`", false));
    print '*/';
    $quickResult = caspioProximitySearchByCoordinatesRaw("TR_Master_List", false, $centerLatLong[0], $centerLatLong[1], "Latitude", "Longitude", "<= {$params["ProximityDistance"]}.49", "3", "TechID, PrimaryEmail, {$specialColumns["PreferLevel"]}", $searchCriteria, $defaultCriteria, false, "|", "`", false);
} else {
    $quickResult = caspioSelectAdv("TR_Master_List", "TechID, PrimaryEmail, {$specialColumns["PreferLevel"]}", "TechID = TechID " . $searchCriteria, "", false, "`", "|", false);
}
$records = array();

if (count($quickResult) > 0) {
    foreach ($quickResult as $row) {
        $row = explode("|", $row);
        $techList[] = $row[0];
        $preferredTechList[(int)trim($row[0], "`'")] = trim($row[2], "`'");
	}
	$quickResult = implode("','", $techList);

    $techsMatchingSearchCriteria = $quickResult;
    $orderCriteria = 'TechID';

    $searchResult = caspioSelectAdv("TR_Master_List", $fieldList, "TechID IN ('" . $techsMatchingSearchCriteria . "')", "", false, "`", "|", false);

    $arrFieldList = explode(',', $fieldList);

    foreach ($searchResult as $key=>&$row) {
        $tmp = explode("|", $row);
        $arrColumn = array();

        foreach ($tmp as $colnumb=>&$column) {
            $arrColumn[$arrFieldList[$colnumb]] = trim($column, "`");
        }
        if (!empty($arrColumn['Latitude']) && !empty($arrColumn['Longitude'])) {
        	$records[$key] = $arrColumn;
        }
	}
}

$res = array();
foreach ($records as $k=>$r) {
    $i = (string)$r['Latitude'].(string)$r['Longitude'];
    $techInfo = $r;
    $techInfo['Preferred'] = $preferredTechList[(int)trim($r['TechID'], "`'")];
    $res[$i][] = $techInfo;
}
$rec_count = count($records);

if ($rec_count) {
//add used marker color to session
if (!empty($_POST['markerColor'])) {
    Map::addUsedColorToSession($_POST['markerColor']);
}
echo "document.getElementById('msgDiv').innerHTML = 'Displaying " .$rec_count ." technicians';";

include("addTechMarker.php");

}