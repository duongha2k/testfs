var resAssignTechs = {};
function assignTechs(companyId) 
{
    gpm_company_id = $('#PMCompany').val();
    if (gpm_company_id != '')
        companyId = gpm_company_id;

    $('#assignErrorsDiv').empty();
    $('#assignErrorsDiv').css('display', 'none');
    errors = '';
    totalCount = 0;
    wos = new Array();
    var items = $('.woassign:checked');
    resAssignTechs = {};
    for (i = 0; i < items.length; i++) {
    	woId = items[i].value;
    	
    	var newIndex = woId;//resAssignTechs.length;
    	resAssignTechs[newIndex] = {};
    	resAssignTechs[newIndex]['TB_UNID'] = woId;
    	resAssignTechs[newIndex]['WO_ID'] = $('#' + woId + '_info').val();;
    	resAssignTechs[newIndex]['TechID'] = $('#' + woId + '_techId').val();
    	resAssignTechs[newIndex]['Agreed'] = $('#' + woId + '_techPay').val();
    	
        wos.push(resAssignTechs[newIndex]['TB_UNID']);
        if (resAssignTechs[newIndex]['TechID']){
            var reg = /^\d+$/;
            if(!reg.test(resAssignTechs[newIndex]['TechID']))
                errors += '<b>"Assign Tech" for "'+resAssignTechs[newIndex]['WO_ID']+'" requred and should be the numeric.</b><br/>';
        } else  errors += '<b>Tech ID for work order "'+resAssignTechs[newIndex]['WO_ID']+'" missing</b><br/>';
        
        if (resAssignTechs[newIndex]['Agreed']){
            var reg = /^[0-9.]+$/;
            if(!reg.test(resAssignTechs[newIndex]['Agreed']))
                errors += '<b>"Agreed $" for "'+resAssignTechs[newIndex]['WO_ID']+'" requred, should be the numeric and has ##.## format.</b><br/>';
        } else  errors += '<b>Agreed amount for work order "'+resAssignTechs[newIndex]['WO_ID']+'" missing.</b><br/>';
        totalCount++;
    }
    if(errors != '') {
        closeAmountPerPopup();
        $('#assignErrorsDiv').html(errors);
        $('#assignErrorsDiv').css('display', 'block');
        return false;
    }
    if (wos.length)
        assignWorkOrdersRecursive(wos, 0, companyId);
    return false;
}

function assignWorkOrdersRecursive(wos, num, companyId){
    showAmountPerPopup('Assigning WIN#'+wos[num]);
    $.ajax({
        type: "POST",
        url: "/widgets/mapping/index/assignwo/",
        dataType: "json",
        data: {companyId:companyId, woId:wos[num], techId: resAssignTechs[wos[num]]['TechID'], agreed: resAssignTechs[wos[num]]['Agreed']},
        success: function(data) {
            errors = '';
            jQuery.each(data.errors, function() {
                errors += '<b>'+this+'</b><br/>';
            });
            if (data.success == 1 && errors == ''){
                for (i=0; i < pointsWo.length; i ++) {
                    if (pointsWo[i]) {
                        p = pointsWo[i].wos;
                        for (j=0; j < p.length; j ++) {
                            if (p[j].TB_UNID == data.wo) {
                                pointsWo[i].wos[j].pub = 0;
                            }
                        }
                    }
                }
            } else {
                $('#assignErrorsDiv').append(errors);
                $('#assignErrorsDiv').css('display', 'block');
            }
            if (wos.length-1 == num){
            	if ($('#assignErrorsDiv').html() == '') {
                    closeAmountPerPopup();
                    woCangePage(1);
                    // reload data
                    processWoData(companyId);
            	}
            } else {
                assignWorkOrdersRecursive(wos, num+1, companyId);
            }
        }
    });
}

function fillTechIdPopupFields(techId) {
    checkedCheckBaxes = $('#assignWoLassoDetails input:checkbox:checked');
    
    checkedCheckBaxes.each(function(n,element){
        if(element.checked) {
            if (!resAssignTechs[element.name])
            	resAssignTechs[element.name] = new Array();
            resAssignTechs[element.name]['checked'] = 1;
            resAssignTechs[element.name]['TechID']  = techId;
            resAssignTechs[element.name]['TB_UNID'] = element.name;
            $('#'+element.name+'_techId').val(techId);
            $('#'+element.name+'_techPay').val('');
        }
    })
    return false;
}

function showAmountPerPopup(status)
{
    $('#amountPopupBg').css('display', 'block');
    $('#amontPopup').css('display', 'block');
    $('#assignProgressBarText').text(status);
}

function closeAmountPerPopup()
{
    $('#amountPopupBg').css('display', 'none');
    $('#amontPopup').css('display', 'none');
}

function checkForAssign(el, tb_unid, woId)
{
    if(el.checked) {
        if (!resAssignTechs[tb_unid])
        	resAssignTechs[tb_unid] = new Array();
        resAssignTechs[tb_unid]['checked'] = 1;
        resAssignTechs[tb_unid]['woId'] = woId;
        $('#'+tb_unid+'_techId').removeAttr("disabled");
        $('#'+tb_unid+'_techId').val("");
        $('#'+tb_unid+'_techPay').removeAttr("disabled");
        $('#'+tb_unid+'_techPay').val("");
    } else {
        if (resAssignTechs[tb_unid]) {
            delete resAssignTechs[tb_unid];
        }
        $('#'+tb_unid+'_techId').attr("disabled", true);
        $('#'+tb_unid+'_techId').val("Tech ID#");
        $('#'+tb_unid+'_techPay').attr("disabled", true);
        $('#'+tb_unid+'_techPay').val("Tech Pay#");
    }
}
