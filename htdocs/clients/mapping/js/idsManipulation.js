if ( typeof idsManipulation == 'undefined' || null == idsManipulation ) {
    idsManipulation = (function () {
        return {
            spliter     : '|',
            container   : null,
            ids         : null,

            /**
             *  Constructor
             */
            init : function() {
            },

            /**
             *  Get all selected items in array
             *  @param string|element container -    Id of element or element
             *  @return null|array
             */
            getIds : function ( container ) {
                var spliter = this.spliter;
                if ( typeof(container) == 'string' ) {
                    container = document.getElementById(container);
                }
                if ( typeof(container) == 'undefined'   ||
                    container == null                   ||
                    this.trim(container.value) == '') {
                    return null;
                }
                return this.array_unique(container.value.split(spliter));
            },

            /**
             *  Get all selected items in query string
             *  @param string|element container -    Id of element or element
             *  @param string name of array for insert ids
             *  @return null|string
             */
            getQueryString : function ( container )
            {
                var ids = this.getIds(container);
                var aname, i, outString = '';
                if ( ids === null ) return null;
                if ( typeof(arguments[1]) == "string" )
                    aname = arguments[1];
                else aname = "id[]";

                for ( i=0; i<ids.length; i++ ) {
                    outString += aname + "=" + ids[i];
                    if ( i < ids.length - 1 ) outString += "&";
                }
                return outString;
            },
            
             /**
             *  @param string|element container -    Id of element or element
             *  @param number|string|array ids
             *  @param function <Optional>  Callback Function
             *  @return bool    True if set ids to element or FALSE else
             */
            setIds : function (container, ids/*, func*/) {
                var spliter = this.spliter;
                if ( typeof(container) == 'string' ) {
                    container = document.getElementById(container);
                }
                if ( !(ids instanceof Array) )
                    ids = [ids];
                if ( typeof(container) == 'undefined' || container == null ) {
                    return false;
                }
                this.container = container;
                this.ids = ids;

                ids = this.array_unique(ids);
                container.value = ids.join(spliter);

                if ( arguments.length == 3 && typeof(arguments[2]) == 'function' ) {
                    return (arguments[2]).call(this);
                }
                return true;
            },

            /**
             *  Remove from container all values from ids
             *
             *  @param string|element container -    Id of element or element
             *  @param number|string|array ids
             *  @param function <Optional>  Callback Function
             *  @return bool
             */
            remIds : function ( container, ids ) {
                var oldValues;
                var spliter = this.spliter;
                if ( typeof(container) == 'string' ) {
                    container = document.getElementById(container);
                }
                if ( typeof(ids) == 'string' || typeof(ids) == 'number' )
                    ids = new Array(ids);
                if ( typeof(container) == 'undefined' || container == null ) {
                    return false;
                }
                this.container = container;
                this.ids = ids;

                oldValues = this.trim(container.value);
                if ( oldValues != '' ) {
                    var tmpArray = [];
                    tmpArray = oldValues.split(spliter);
                    tmpArray = this.array_diff(tmpArray, ids);
                    if ( tmpArray.length == 0 )
                        container.value = '';
                    else
                        tmpArray = this.deleteEmpty(tmpArray);
                    container.value = tmpArray.join(spliter);
                }

                if ( arguments.length == 3 && typeof(arguments[2]) == 'function' ) {
                    return (arguments[2]).call(this);
                }
                return true;
            },

            /**
             *  Remove all Ids from container
             *  @param string|element container -    Id of element or element
             *  @param function <Optional>  Callback Function
             *  @return bool
             */
            cleanIds : function ( container ) {
                if ( typeof(container) == 'string' ) {
                    container = document.getElementById(container);
                }
                if ( typeof(container) == 'undefined' || container == null ) {
                    return false;
                }
                this.container = container;
                this.ids = [];

                container.value = '';

                if ( arguments.length == 2 && typeof(arguments[1]) == 'function' ) {
                    return (arguments[1]).call(this);
                }
                return true;
            },

            /**
             *  Adds id(s) to container
             *
             *  @param string|element container -    Id of element or element
             *  @param number|string|array ids
             *  @param function <Optional>  Callback Function
             *  @return bool
             */
            addIds : function ( container, ids ) {
            	
                var oldValues;
                var spliter = this.spliter;
                if ( typeof(container) == 'string' ) {
                    container = document.getElementById(container);
                }
                if ( !(ids instanceof Array) )
                    ids = [ids];
                if ( typeof(container) == 'undefined' || container == null ) {
                    return false;
                }
                this.container = container;
                this.ids = ids;

                oldValues = this.trim(container.value);
                
                var tmpArray = [];
                tmpArray = oldValues.split(spliter);
                if (!(tmpArray instanceof Array)) {
                	tmpArray = [tmpArray];
                }
                
                tmpArray = this.deleteEmpty(this.array_unique(this.array_merge(tmpArray, ids)));

                container.value = tmpArray.join(spliter);

                if ( arguments.length == 3 && typeof(arguments[2]) == 'function' ) {
                    return (arguments[2]).call(this);
                }
                return true;
            },

            deleteEmpty : function ( array ) {
                if ( !(array instanceof Array) ) 
                	return [array];
                var copyArray = array.concat([]);	
                var tmp = [];
                var idx;
                while(copyArray.length > 0) {
                	idx = copyArray.shift();
                	if ( this.trim(idx) != '' )  
                		tmp.push(idx);
                }
                return tmp;
            },

            /**
             *  Merges elements from passed arrays into one array
             */
            array_merge : function () {
                var args = Array.prototype.slice.call(arguments);
                var retObj = [], k, j = 0, i = 0;
                var retArr;
                for (i=0, retArr=true; i < args.length; i++) {
                    if (!(args[i] instanceof Array)) {
                        retArr=false;
                        break;
                    }
                }
                if (!retArr) {
                    return args;
                }
                var ct = 0;
                for (i=0, ct=0; i < args.length; i++) {
                    if (args[i] instanceof Array) {
                        for (j=0; j < args[i].length; j++) {
                            retObj[ct++] = args[i][j];
                        }
                    } else {
                        for (k in args[i]) {
                            if (this.is_int(k)) {
                                retObj[ct++] = args[i][k];
                            } else {
                                retObj[k] = args[i][k];
                            }
                        }
                    }
                }
                return retObj;
            },
            is_int : function ( mixed_var ) {
                if (typeof mixed_var !== 'number') {
                    return false;
                }
                if (parseFloat(mixed_var) != parseInt(mixed_var, 10)) {
                    return false;
                }
                return true;
            },
            /**
             *  Returns the entries of arr1 that have values which are not present in any of the others arguments
             */
            array_diff : function () {
                var arr1 = arguments[0], retArr = [];
                var k1,i,k;
                var arr = [];

                newIterration:
                for (k1 in arr1) {
                    for (i = 1; i < arguments.length; i++) {
                        arr = arguments[i];
                        for (k in arr) {
                            if (arr[k] == arr1[k1]) {
                                // If it reaches here, it was found in at least one array, so try next value
                                continue newIterration;
                            }
                        }
                        retArr.push(arr1[k1]);
                    }
                }
                return retArr;
            },
            /**
             *  Removes duplicate values from array
             */
            array_unique : function ( array ) {
                var key = '', tmp_arr1 = [], tmp_arr2 = [];
                var val = '';
                tmp_arr1 = array;
                var __array_search = function (needle, haystack) {
                    var fkey = '';
                    for (fkey in haystack) {
                        if ((haystack[fkey] + '') === (needle + '')) {
                            return fkey;
                        }
                    }
                    return false;
                };
                for (key in tmp_arr1) {
                    val = tmp_arr1[key];
                    if (false === __array_search(val, tmp_arr2)) {
                        tmp_arr2[key] = val;
                    }
                    delete tmp_arr1[key];
                }
                return tmp_arr2;
            },
            trim : function(str, charlist) {
                // Strips whitespace from the beginning and end of a string
                var whitespace, l = 0, i = 0;
                str += '';
                if (!charlist) {
                    // default list
                    whitespace = " \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000";
                } else {
                    // preg_quote custom list
                    charlist += '';
                    whitespace = charlist.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '$1');
                }
                l = str.length;
                for (i = 0; i < l; i++) {
                    if (whitespace.indexOf(str.charAt(i)) === -1) {
                        str = str.substring(i);
                        break;
                    }
                }
                l = str.length;
                for (i = l - 1; i >= 0; i--) {
                    if (whitespace.indexOf(str.charAt(i)) === -1) {
                        str = str.substring(0, i + 1);
                        break;
                    }
                }
                return whitespace.indexOf(str.charAt(0)) === -1 ? str : '';
            },
            in_array : function (needle, haystack, argStrict) {
                var key = '', strict = !!argStrict;
                if (strict) {
                    for (key in haystack) {
                        if (haystack[key] === needle) {
                            return true;
                        }
                    }
                } else {
                    for (key in haystack) {
                        if (haystack[key] == needle) {
                            return true;
                        }
                    }
                }
                return false;
            }
        }
    })();
    idsManipulation.init();
}