var colorLayer = '';

function ColorLayerManager(div, divTitle, thisMap)
{
    this.div    = div;
    this.element = document.getElementById(div);

    this.divTitle    = divTitle;
    this.elementTitle = document.getElementById(divTitle);

    this.colors = Array();
    this.menu   = '';

    this.markersGroup = Array(); // Array of colors. Each color contains names of the markers

    this.map     = '';
    this.cluster = '';


    ColorLayerManager.prototype.addColor = function(val, text) {
        this.addColorObj(new colorOfMarker(val, text));
    };

    // The param must be instance of colorOfMarker
    ColorLayerManager.prototype.addColorObj = function(objColor) {
    	for(i=0;i < this.colors.length; i++) {
            if (this.colors[i].val == objColor.val) {
                return false;
            }
    	}

    	objColor.val = escape(objColor.val);
        objColor.text = escape(objColor.text);

        this.colors.push(objColor);
        return true;
    };

    ColorLayerManager.prototype.refresh = function () {
        this.redrawMenu();
    };

    ColorLayerManager.prototype.redrawMenu = function () {
    	this.menu   = '';
        for(i=0;i < this.colors.length; i++) {
            if (this.colors[i]) {
                this.menu += this.colors[i].text + ' [<a href="#" onclick="deleteMarkerColorFormSession(\''+this.colors[i].val+'\'); return false;">remove</a>]<br />';
            }
        }
        this.element.innerHTML = this.menu;

        if (this.colors.length > 0) {
            this.showTitle();
        } else {
            this.hideTitle();
        }
    };
    
    ColorLayerManager.prototype.delAllMarkers = function () {
	 	$.ajax({
	        type: "POST",
	        url: "ajax/delAllMarkers.php",
	        dataType: "html",
	        data: "",
	        success: colorLayer.deleteAllMarkers()
	    });
 	};

    ColorLayerManager.prototype.showTitle = function () {
        this.elementTitle.style.display = 'block';
        document.getElementById('delAllColors').style.display = 'block';
    };

    ColorLayerManager.prototype.hideTitle = function () {
        this.elementTitle.style.display = 'none';
        document.getElementById('delAllColors').style.display = 'none';
        $('#msgDiv').empty();
    };

    ColorLayerManager.prototype.deleteMarkersByColor = function (color) {

        for(i=0;i < this.colors.length; i++) {
            if (this.colors[i].val == color) {
                this.colors.splice(i, 1);
                break;
            }
        }

        for(i in cluster){
			if (cluster[i].color == color) {
                cluster[i].cluster._removeClusterMarkers();
                cluster[i].cluster.removeMarkers();
            }
		}
        resArr = new Array();
        if (points) { // Array of objMarker used for poligon
            for (i=0; i < points.length; i ++) {
            	if (points[i].color != color) {
                    resArr.push(points[i]);
                }
            }
        }
        points = resArr
        resArr = new Array();
        if (pointsWo) {
            for (i=0; i < pointsWo.length; i++) {
            	if (pointsWo[i].color != color) {
            	    resArr.push(pointsWo[i]);
                }
            }
        }
        pointsWo = resArr;
        $('#msgDiv').empty();
        this.redrawMenu();
    };
    
    ColorLayerManager.prototype.deleteAllMarkers = function () {
        this.colors.splice(0,this.colors.length);

        for(i in cluster){
            if(cluster[i].color != null) {
            	cluster[i].cluster.removeMarkers();
            }
		}

        if (points) { // Array of objMarker used for poligon
            points.splice(0,points.length);
        }
        if (pointsWo) { // Array of objMarker used for poligon
            pointsWo.splice(0,pointsWo.length);
        }
             
        this.redrawMenu();
    };

    ColorLayerManager.prototype.deleteMarkers = function (markersToDelete) {
        this.cluster.removeSelectedMarkers(markersToDelete);
        this.cluster.refresh(true);
    };

    ColorLayerManager.prototype.addMarkers = function (markers, color, thisMap, thisCluster) {
    	//this.map     = thisMap;
    	//this.cluster = thisCluster;
        //this.markersGroup.push(new markersGroupedByColor(markers, color));
    };

}

function colorOfMarker(val, text) {
    this.val = val;
    this.text = text;
}

function markersGroupedByColor(markers, color) {
    this.markers = markers;
    this.color   = color;
}

// For IE 6
if(!Array.indexOf){
 Array.prototype.indexOf = function(obj){
     for(var i=0; i<this.length; i++){
         if(this[i]==obj){
             return i;
         }
     }
     return -1;
 }
}

function deleteMarkerColorFormSession(color) {
 	$.ajax({
        type: "POST",
        url: "ajax/delUsedMarkerColor.php",
        dataType: "html",
        data: "color="+color,
        success: colorLayer.deleteMarkersByColor(color)
    });
 }
 
 function testt()
 {
    alert('asdf');
 }