var polygon         = '';
var goomap             = '';
var points          = Array();
var lassoPoints     = Array();
var lassoMarkers    = Array();
var lassoUse        = false;
var mapAvailable    = false; // map exists on the page
var cluster         = Array();

var pointsWo        = Array();
var exportPoints    = Array();
var exportMarkers   = Array();
var exportUse       = false;
//var markerTechsArr  = Array();
//var markerWosArr    = Array();

var geocoder = null;
var mapInitialized = false;
var percentsLoadingGlob = 0;


function clusterOptions(clusterMarkerIcon)
{
	this.clusterMarkerIcon = clusterMarkerIcon;
}

function markerTech(id,lname,fname,phone,phoneType,email,city,state,zip,addr1,addr2,noshow,backouts,qic,qfsc,lat,lon,pref,flsId,SATRecommendedAvg,SATRecommendedTotal,SATPerformanceAvg,SATPerformanceTotal)
{
    this.TechID = id;
    this.LastName = lname;
    this.FirstName = fname;
    this.PrimaryPhone = phone;
    this.PrimPhoneType = phoneType;
    this.PrimaryEmail = email;
    this.City = city;
    this.State = state;
    this.ZipCode = zip;
    this.Address1 = addr1;
    this.Address2 = addr2;
    this.No_Shows = noshow;
    this.Back_Outs = backouts;
    this.Qty_IMAC_Calls = qic;
    this.Qty_FLS_Service_Calls = qfsc;
    this.Latitude = lat;
    this.Longitude = lon;
    this.Preferred = pref;
    this.FLSID = flsId;
	this.SATRecommendedAvg = SATRecommendedAvg;
	this.SATRecommendedTotal = SATRecommendedTotal;
	this.SATPerformanceAvg = SATPerformanceAvg;
	this.SATPerformanceTotal = SATPerformanceTotal;
}

function markerWo(projectId,techId,tbunid,woId,Start,Site,City,State,Zip,Bid_amount,Amount_Per,Project,Tech,Latitude,Longitude,pub,PayMax)
{
    this.projectId = projectId;
    this.techId = techId;
    this.TB_UNID = tbunid;
    this.woId = woId;
    this.Latitude = Latitude;
    this.Longitude = Longitude;
    this.Start = Start;
    this.Site = Site;
    this.City = City;
    this.State = State;
    this.Zip = Zip;
    this.Bid_amount = Bid_amount;
    this.Amount_Per = Amount_Per;
    this.Project = Project;
    this.Tech = Tech;
    this.pub = pub;
    this.PayMax = PayMax;
}

function objMarker(id, point, email, color, techs)
{
    this.id    = id;
    this.point = point;
    this.email = email;
    this.color = color;
    this.techs = techs
}

function objMarkerWo(id,point,color,wos)
{
    this.id       = id;
    this.point    = point;
    this.color    = color;
    this.wos      = wos;
}

function initPoligon()
{
    if (typeof(goomap) == 'object' && mapAvailable == true) { // Init only once
        GPolygon.prototype.containsLatLng = function(latLng) {
            var bounds = this.getBounds();

            if(bounds){
            if(!bounds.containsLatLng(latLng))
                return false;
            }

            var numPoints = this.getVertexCount();
            var inPoly = false;
            var i;
            var j = numPoints-1;

            for(var i=0; i < numPoints; i++) {
                var vertex1 = this.getVertex(i);
                var vertex2 = this.getVertex(j);

                if (vertex1.lng() < latLng.lng() && vertex2.lng() >= latLng.lng() || vertex2.lng() < latLng.lng() && vertex1.lng() >= latLng.lng())  {
                    if (vertex1.lat() + (latLng.lng() - vertex1.lng()) / (vertex2.lng() - vertex1.lng()) * (vertex2.lat() - vertex1.lat()) < latLng.lat()) {
                        inPoly = !inPoly;
                    }
                }

                j = i;
            }

            return inPoly;
        };
    }
}

function lassoToggle()
{
    var mapElem = document.getElementById("gmap");
    if (lassoUse == false) {
        goomap.disableInfoWindow();
        for(i in cluster){
            if(cluster[i].cluster){
                cluster[i].cluster.removeClickEvent();
                cluster[i].cluster._lassoEnabled = true;
            }
        }
        lassoUse = true;
        mapElem.className = "edit";
        if (document.getElementById('lassoToggleDiv')) {
            document.getElementById('lassoToggleDiv').innerHTML = '<a href="#" onclick="lassoToggle(); clearLasso(); return false;">Disable FS-Mapper</a>';
            document.getElementById('lassoDrawDiv').style.display = 'inline';
        }
    } else {
        goomap.enableInfoWindow();
        for(i in cluster){
            if(cluster[i].cluster){
                cluster[i].cluster._lassoEnabled = false;
                cluster[i].cluster.refresh(true);
            }
        }
        lassoUse = false;
        mapElem.className = "view";

        if (document.getElementById('lassoToggleDiv')) {
            document.getElementById('lassoToggleDiv').innerHTML = '<a href="#" onclick="lassoToggle(); clearLasso(); return false;">Enable FS-Mapper</a>';
            document.getElementById('lassoDrawDiv').style.display = 'none';
        }
    }
    document.getElementById('lassoEmail').style.display = 'inline';
    document.getElementById('lassoAssign').style.display = 'inline';
    document.getElementById('lassoData').style.display = 'inline';
	document.getElementById('lassoRemove').style.display = 'inline';
}

function drawLasso()
{
    goomap.removeOverlay(polygon);
    polygon = new GPolygon(lassoPoints, "#f33f00", 0, 1, "#ff0000", 0.2);
    goomap.addOverlay(polygon);
    lassoToggle();
}

function clearLasso()
{
	document.getElementById('lassoEmail').style.display = 'none';
	document.getElementById('lassoAssign').style.display = 'none';
	document.getElementById('lassoData').style.display = 'none';
	document.getElementById('lassoRemove').style.display = 'none';

    goomap.removeOverlay(polygon);
    for (i=0; i<lassoMarkers.length; i++) {
        goomap.removeOverlay(lassoMarkers[i]);
    }
    lassoPoints = Array();
}

//***functions for export WO lasso***//
function exportToggle()
{
    var mapElem = document.getElementById("gmap");
    if (exportUse == false) {
        exportUse = true;
        mapElem.className = "edit";
        if (document.getElementById('exportToggleDiv')) {
            document.getElementById('exportToggleDiv').innerHTML = '<a href="#" onclick="exportToggle(); clearExport(); return false;">Disable Export</a>';
            document.getElementById('exportDrawDiv').style.display = 'inline';
        }
    } else {
        exportUse = false;
        mapElem.className = "view";

        if (document.getElementById('exportToggleDiv')) {
            document.getElementById('exportToggleDiv').innerHTML = '<a href="#" onclick="exportToggle(); clearExport(); return false;">Enable E</a>';
            document.getElementById('exportDrawDiv').style.display = 'none';
        }
    }
}

function drawExport()
{
    goomap.removeOverlay(polygon);
    polygon = new GPolygon(exportPoints, "#f33f00", 0, 1, "#ff0000", 0.2);
    goomap.addOverlay(polygon);
    exportToggle();
}

function clearExport()
{
    goomap.removeOverlay(polygon);
    for (i=0; i<exportMarkers.length; i++) {
        goomap.removeOverlay(exportMarkers[i]);
    }
    exportPoints = Array();
}

function collectExportMarkers()
{
    if (polygon) {
        var selectedMarkers = Array();
        for (i=0; i < pointsWo.length; i ++) {

            if (polygon.containsLatLng(pointsWo[i].point)) {
                selectedMarkers[selectedMarkers.length] = points[i];
            }
        }
        addWoMarkersToSession(selectedMarkers);
    }
}
//**********************//

function collectSelectedMarkers()
{
   if (polygon) {
        var selectedMarkers = Array();
        for (i=0; i < points.length; i ++) {
            if (polygon.containsLatLng(points[i].point)) {
                selectedMarkers[selectedMarkers.length] = points[i];
            }
        }
        
        doActionForSelected(selectedMarkers);
    }
}

function showSelectedMarkersData(companyId, assign)
{
    if(resAssignTechs)
        resAssignTechs = new Array();
    if (polygon) {
        var qstr = 'company_id='+companyId;
        if ((typeof(assign) != 'undefined' && assign) || (typeof(assign) == 'undefined' && $('#woAssign').val() == '1')) {
        	qstr += '&assign=1';
        }
        if ($('#woOrderBy').length) {
        	qstr += '&woOrderBy=' +  $('#woOrderBy').val();
        }
        if ($('#woOrderType').length) {
        	qstr += '&woOrderType=' +  $('#woOrderType').val();
        }
        if ($('#woPage').length) {
        	qstr += '&woPage=' +  $('#woPage').val();
        }
        if ($('#techOrderBy').length) {
        	qstr += '&techOrderBy=' +  $('#techOrderBy').val();
        }
        if ($('#techOrderType').length) {
        	qstr += '&techOrderType=' +  $('#techOrderType').val();
        }
        if ($('#techPage').length) {
        	qstr += '&techPage=' +  $('#techPage').val();
        }
        $('#panel_mask').empty();
        $('#loadingDiv').css('display', 'block');
        $('#loading_percent').val('Loading...');
        qstr += collectWorkOrdersIdsFromPolygon(false);
        qstr += collectoTechsIdsFromPolygon();
        
        $.ajax({
            type: "POST",
            url: "/widgets/mapping/index/getdetailform/",
            dataType: "html",
            data: qstr,
            success: function(res) {
                $('#panel_mask').html(res);
                $('#loadingDiv').css('display', 'none');
            }
        });
    }
}
/*
function collectAssignedWorkOrders(companyId, assign)
{
    if(resAssignTechs)
        resAssignTechs = new Array();
    if (polygon) {
        var qstr = 'formType=lasso_assign_data&firstTime=firstTime&company_id='+companyId;
        if (typeof(assign) != 'undefined' ) {
        	qstr += '&assign=1';
        }
        $('#panel_mask').empty();
        $('#loadingDiv').css('display', 'block');
        $('#loading_percent').val('Loading...');
        qstr += collectWorkOrdersFromPolygon(true);
        qstr += collectoTechsFromPolygon();
        
        $.ajax({
            type: "POST",
            url: "get_forms.php",
            dataType: "html",
            data: qstr,
            success: function(res) {
                $('#panel_mask').html(res);
                $('#loadingDiv').css('display', 'none');
            }
        });
    }
}
*/

/*
// to delete
function removeJustAssigned() {
    $.ajax({
        type: "POST",
        url: "ajax/removeAssigned.php",
        dataType: "json",
        data: '',
        success: function(data) {
            jQuery.each(data.assigned, function() {
                for (i=0; i < pointsWo.length; i ++) {
                    if (pointsWo[i]) {
                        p = pointsWo[i].wos;
                        for (j=0; j < p.length; j ++) {
                            if (p[j].TB_UNID == this) {
                            alert(pointsWo[i].wos[j].TB_UNID);
                                pointsWo[i].wos.splice(j, 1);;
                                
                            }
                        }
                    }
                }
            });
        }
    });
}
*/

function collectWorkOrdersIdsFromPolygon(pub) {
    var result = new Array();
    if (polygon) {
	    for (i=0; i < pointsWo.length; i ++) {
	        if (pointsWo[i]) {
		        if (polygon.containsLatLng(pointsWo[i].point)) {
		            p = pointsWo[i].wos;
		            for (j=0; j < p.length; j ++) {
		                /*if (p[j].pub !=1 && pub)
		                    continue;*/
		                result[result.length] = p[j].WIN_NUM;
		            }
		        }
	        }
	    }
    }
    return '&workorders=' + result.toString();
}
/*
// check to remove (replaced by id methods)
function collectWorkOrdersFromPolygon(pub) {
    res = '';
    if (polygon) {
    for (i=0; i < pointsWo.length; i ++) {
        if (pointsWo[i]) {
        if (polygon.containsLatLng(pointsWo[i].point)) {
            p = pointsWo[i].wos;
            for (j=0; j < p.length; j ++) {
                if (p[j].pub !=1 && pub)
                    continue;
                res += '&wos['+p[j].TB_UNID+'][projectId]='+p[j].projectId+
                       '&wos['+p[j].TB_UNID+'][techId]='+p[j].techId+
                       '&wos['+p[j].TB_UNID+'][StartDate]='+p[j].Start+
                       '&wos['+p[j].TB_UNID+'][Site]='+p[j].Site+
                       '&wos['+p[j].TB_UNID+'][City]='+p[j].City+
                       '&wos['+p[j].TB_UNID+'][State]='+p[j].State+
                       '&wos['+p[j].TB_UNID+'][Zipcode]='+p[j].Zip+
                       '&wos['+p[j].TB_UNID+'][Bid_amount]='+p[j].Bid_amount+
                       '&wos['+p[j].TB_UNID+'][Amount_Per]='+p[j].Amount_Per+
                       '&wos['+p[j].TB_UNID+'][Project]='+p[j].Project+
                       '&wos['+p[j].TB_UNID+'][woId]='+p[j].woId+
                       '&wos['+p[j].TB_UNID+'][TB_UNID]='+p[j].TB_UNID+
                       '&wos['+p[j].TB_UNID+'][PayMax]='+p[j].PayMax+
                       '&wos['+p[j].TB_UNID+'][Tech]='+p[j].Tech;
            }
        }
        }
    }
    }
    return res;
}
*/

function collectoTechsIdsFromPolygon() {
    var result = new Array();
    if (polygon) {
	    for (i=0; i < points.length; i ++) {
	        if (points[i]) {
		        if (polygon.containsLatLng(points[i].point)) {
		            p = points[i].techs;
		            for (j=0; j < p.length; j ++) {
		                /*if (p[j].pub !=1 && pub)
		                    continue;*/
		                result[result.length] = p[j].TechID;
		            }
		        }
	        }
	    }
    }
    return '&techs=' + result.toString();
}
/*
//replaced by collectoTechsIdsFromPolygon. check adn remove it.
function collectoTechsFromPolygon() {
    res = '';
    if (polygon) {
    for (i=0; i < points.length; i ++) {
        if (points[i]) {
        if (polygon.containsLatLng(points[i].point)) {
            p = points[i].techs;
            for (j=0; j < p.length; j ++) {
                res += '&techs['+p[j].TechID+'][TechID]='+p[j].TechID+
                       '&techs['+p[j].TechID+'][LastName]='+p[j].LastName+
                       '&techs['+p[j].TechID+'][FirstName]='+p[j].FirstName+
                       '&techs['+p[j].TechID+'][PrimaryPhone]='+p[j].PrimaryPhone+
                       '&techs['+p[j].TechID+'][PrimPhoneType]='+p[j].PrimPhoneType+
                       '&techs['+p[j].TechID+'][PrimaryEmail]='+p[j].PrimaryEmail+
                       '&techs['+p[j].TechID+'][Address1]='+p[j].Address1+
                       '&techs['+p[j].TechID+'][City]='+p[j].City+
                       '&techs['+p[j].TechID+'][State]='+p[j].State+
                       '&techs['+p[j].TechID+'][ZipCode]='+p[j].ZipCode+
                       '&techs['+p[j].TechID+'][Qty_IMAC_Calls]='+p[j].Qty_IMAC_Calls+
                       '&techs['+p[j].TechID+'][Qty_FLS_Service_Calls]='+p[j].Qty_FLS_Service_Calls+
                       '&techs['+p[j].TechID+'][No_Shows]='+p[j].No_Shows+
                       '&techs['+p[j].TechID+'][Back_Outs]='+p[j].Back_Outs+
                       '&techs['+p[j].TechID+'][Back_Outs]='+p[j].Back_Outs+
                       '&techs['+p[j].TechID+'][FLSID]='+p[j].FLSID+
                       '&techs['+p[j].TechID+'][SATRecommendedAvg]='+p[j].SATRecommendedAvg+
                       '&techs['+p[j].TechID+'][SATRecommendedTotal]='+p[j].SATRecommendedTotal+
                       '&techs['+p[j].TechID+'][SATPerformanceAvg]='+p[j].SATPerformanceAvg+
                       '&techs['+p[j].TechID+'][SATPerformanceTotal]='+p[j].SATPerformanceTotal;
            }
        }
        }
    }
    }
    return res;
}
*/
function setMapCenter(lat, lng, zoom) {
    goomap.setCenter(new GLatLng(lat, lng), zoom);
}


function initialize() {

			if (GBrowserIsCompatible()) {
				map = new GMap2(document.getElementById("map_canvas"));
				map.setCenter(new GLatLng(39.8333, -98.5833), 10);
				map.addControl(new GLargeMapControl());
				geocoder = new GClientGeocoder();
			}

    }

    function mapShowAddress() {
        address = $("#ProximityZipCode").attr("value");
        if(address.length >= 3)
		    processAddress(address); 
    }

	function processAddress(address) {
        if (geocoder && address) {

        	var re = /([A-Za-z])(\d)([A-Za-z])([ ]+)?(\d)([A-Za-z])(\d)/

        	if (result = re.exec(address) ) {
        		// it is canada zip
        	    var newAdress = RegExp.$1 + RegExp.$2 + RegExp.$3 + ' ' + RegExp.$5 + RegExp.$6 + RegExp.$7;
        	    newAdress += ',Canada';
        	    address = newAdress;
        	}
        	
            geocoder.getLatLng(
                address,
                function(point) {
                    if (!point) {
                        map.clearOverlays();
                        map.setCenter(new GLatLng(39.8333, -98.5833), 10);
						//if (onPointReturnedFailed != null) onPointReturnedFailed();
                    } else {
                        map.clearOverlays();
                        map.setCenter(point, 10);
                        var marker = new GMarker(point);
                        map.addOverlay(marker);
						if (onPointReturned != null) onPointReturned(point);
                    }
                    // logic zip checked
                    if ($("#ProximityZipCode").length && $("#ProximityZipCode").length) {
                    	$("#ProximityZipCode").attr("cvalue", $("#ProximityZipCode").val() );
                    }
                }
            );
        }
	}

function saveGeocode(point) {
	$("#ProximityLat").attr("value", point.lat());
	$("#ProximityLng").attr("value", point.lng());
};

function SaveGeoAndSubmitForm(point)
{
	saveGeocode(point);
	if ($("#searchForm").length) {
		$("#searchForm").submit();
	}
}

function initializeMapTechs()
{
        initialize();
		$("#ProximityZipCode").keyup(mapShowAddress);
		$("#ProximityZipCode").change(mapShowAddress);
		$("#ProximityZipCode").focusout(mapShowAddress);
		onPointReturned = saveGeocode;
}

function doActionForSelected(thisMarkers)
{
    var arrEmailList = Array();
    for (i=0; i < thisMarkers.length; i++) {
        arrEmailList[arrEmailList.length] = thisMarkers[i].email;
    }

    document.getElementById('emailList').value = arrEmailList.join(',');
    document.getElementById('goToEmail').submit();
}

function initializeMap() {
    if (GBrowserIsCompatible() && !mapInitialized) {
        goomap = new GMap2(document.getElementById("gmap"));
        goomap.setCenter(new GLatLng(38, -97), 4);
        goomap.addControl(new GSmallMapControl());
        goomap.addControl(new GMapTypeControl());
        goomap.getInfoWindow();

        var lassoIcon = new GIcon(G_DEFAULT_ICON);
        lassoIcon.image = "../../images/map/crosshairs.gif";
        lassoIcon.iconSize = new GSize(10,10);
        lassoIcon.iconAnchor = new GPoint(5,5);
        lassoIcon.shadow = "";

		var flag = true;
        GEvent.addListener(goomap, "zoomend", function(oldLevel,  newLevel) {
        	setTimeout(function() {
	            if(newLevel <= 6) {
	            	if (!$('#clusteringCheckBox').attr("checked")) {
	        		for(i in cluster){
	        			i = parseInt(i);
	        			if(cluster[i]) {
	                        if(cluster[i].cluster.clusteringEnabled==false) {
	        			        cluster[i].cluster.clusteringEnabled=true;
	        			        cluster[i].cluster.refresh(true);
	        		        }
	        			}
		            }
		            $('#clusteringCheckBox').attr("checked", true);
		            }
	        	} else {
	        		if ($('#clusteringCheckBox').attr("checked") == true) {

	        		for(i in cluster){
	        			i = parseInt(i);
	        			if(cluster[i]) {
    	                    if(cluster[i].cluster.clusteringEnabled==true && cluster[i].cluster._totalMarkers < 500) {
	        			        cluster[i].cluster.clusteringEnabled=false;
	        			        cluster[i].cluster.refresh(true);
	        			        flag = false;
	        		        }
	        			}
		            }
		            $('#clusteringCheckBox').attr("checked", false);
	        	}
	        		        	}

        	}, 500);

        });

        GEvent.addListener(goomap, "click", function(overlay,latlng) {

        if (latlng && lassoUse == true && lassoPoints.length < 50) {
            lassoPoints[lassoPoints.length] = latlng;
            var point1 = new GPoint(latlng.x, latlng.y);
            var lassoId = lassoMarkers.length;
            lassoMarkers[lassoId] = new GMarker(point1, lassoIcon);
            goomap.addOverlay(lassoMarkers[lassoId]);
        }
        });

        var exportIcon = new GIcon(G_DEFAULT_ICON);
        exportIcon.image = "../../images/map/crosshairs.gif";
        exportIcon.iconSize = new GSize(10,10);
        exportIcon.iconAnchor = new GPoint(5,5);
        exportIcon.shadow = "";

        GEvent.addListener(goomap, "click", function(overlay,latlng) {
        if (latlng && exportUse == true && exportPoints.length < 50) {
            exportPoints[exportPoints.length] = latlng;
            var point1 = new GPoint(latlng.x, latlng.y);
            var exportId = exportMarkers.length;
            exportMarkers[exportId] = new GMarker(point1, exportIcon);
            goomap.addOverlay(exportMarkers[exportId]);
        }
        });

        //cluster = new ClusterMarker(goomap);

        // Enable/Disable clustering
        /*var html='<div class="htmlControl" style="padding:0px 3px 3px 3px;border:solid black 1px;background-color:white;color:black;font-family:Arial, Helvetica, sans-serif;font-size:12px;	text-align:center; display:none;">Enable clustering: <input type="checkbox" onclick="toggleClustering()" id="clusteringCheckBox" /></div>';
		var control=new HtmlControl(html);
		goomap.addControl(control, new GControlPosition(G_ANCHOR_TOP_LEFT, new GSize(40,5)));

		$('#clusteringCheckBox').attr("checked", flag);
		*/
        mapInitialized = true;
        mapAvailable   = true;
    }
}

function toggleClustering() {
		for(i in cluster) {
			i = parseInt(i);
			if(cluster[i]) {
				cluster[i].cluster.clusteringEnabled=!cluster[i].cluster.clusteringEnabled;
				cluster[i].cluster.refresh(true);//	true required to force a full update of the markers - otherwise the update would occur next time that the map is zoomed or the active markers change
			}
		}
}

function createCluster(thisColor, icon) {
	options = new clusterOptions(icon);
	localCluster = new ClusterMarker(goomap, options);
	cluster.push(new ClusterBlock(localCluster, thisColor));
	return localCluster;
}

function updateLoading(persentsCurrent)
{
    if (persentsCurrent > percentsLoadingGlob || percentsLoadingGlob == 0) {
        percentsLoadingGlob = persentsCurrent;
        $('#loading_percent').val(persentsCurrent + '%  Loading...');
        if (percentsLoadingGlob == 100) {
            percentsLoadingGlob = 0;
        }
    }
}

function refreshMapDelayed(doReset)
{
    setTimeout(function(){
    	//updateLoading(90);
    	localCluster.refresh();
        updateLoading(100);
        }, 1300);
}

function setCenterToAddress(address)
{
    if (geocoder && address) {
        geocoder.getLatLng(
            address,
            function(point) {
                if (!point) {
                    setMapCenter('39.8333', '-98.5833', 10);
                } else {
                    goomap.setCenter(point, 6);
                }
            }
        );
    }
}
