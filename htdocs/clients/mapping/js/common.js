var overlayEnabled = false;

var waitLoadWO = {};
var waitLoadTechs = {};


function useOverlayToggle()
{
    if (overlayEnabled == false) {
        overlaySwitch(true);
    } else {
        overlaySwitch(false);
    }

    overlayRequest();
}

function overlaySwitch(newStatus) {
    if (newStatus == true) {
        overlayEnabled = true;
        document.getElementById('overlayToggle').innerHTML = '<a href="#" onclick="useOverlayToggle(); return false;">Disable Overlay of different search results</a><br /><a href="#" onclick="overlayRequest(); return false;">Add this search result to the map overlay</a>';
    } else {
        overlayEnabled = false;
        document.getElementById('overlayToggle').innerHTML = '<a href="#" onclick="useOverlayToggle(); return false;">Enable Overlay of different search results</a>';
    }
}

function overlayRequest()
{
     var allTechIds = Array();
     if (overlayEnabled == true) {
        for (i=0; i < points.length; i++) {
            allTechIds[allTechIds.length] = points[i].id;
        }
     }

     $.ajax({
            url: 'ajax/map_overlay.php',
            type: "POST",
            dataType:"html",
            data:{ enable:overlayEnabled, techId: allTechIds.join(',')},
            cache: false,
            success: function (data, textStatus) {
                if (overlayEnabled == false) {
                    window.location.href = window.location;
                }
            }
        });
}


function mappingFormSubmit()
{
	if ($("#ProximityZipCode").val() != '' && $("#ProximityZipCode").val() != $("#ProximityZipCode").attr("cvalue") ) {
		onPointReturned = SaveGeoAndSubmitForm;
		mapShowAddress();
		return false;
	}
	
	var serviceUrl = '/widgets/mapping/index/findtechs/';
	if ($('#formType').val() == 'wo' ) {
		serviceUrl = '/widgets/mapping/index/findwo/';
	}
	
    var searchParams = '';
    if ($('#PMCompany :selected').val()) {
        searchParams +='company=' + $('#PMCompany :selected').val() + '&';
    }
    var selectedColorOption = $('#markerColor :selected');
    var selectedColor = new colorOfMarker(selectedColorOption.val(), selectedColorOption.text());

    var $inputs = $('#searchForm input, #searchForm textarea, #searchForm select');
    var values = {};
    $inputs.each(function(i, el) {
    	if ($(el).val()) {
    		if (($(el).attr('type') != 'checkbox' && $(el).attr('type') != 'radio') || $(el).attr('checked') == true) {
                searchParams += $(el).attr('name') + '=' +$(el).val() +'&';
            }
        }
    });


    $('#errorDiv').empty();
    var flag = true;
    for(i=0;i < colorLayer.colors.length; i++) {
        if (colorLayer.colors[i] && selectedColor) {
            if(colorLayer.colors[i].val == selectedColor.val) {
            	$('#errorDiv').attr('innerHTML', selectedColor.text+' color of markers is in use already');
                document.getElementById('formDiv').style.display = 'none';
             	flag = false;
            }
        }
    }
    if (!selectedColor.val) {
        $('#errorDiv').attr('innerHTML', 'All marker colors in used');
        flag = false;
    }
    window.scroll(0,0);

    $('#panel_mask').empty();
    document.getElementById('loadingDiv').style.display = 'block';
    updateLoading(0);
    if(flag){
    	var delayFlag = false;
    	

    	
    $.ajax({
                 type: "POST",
                 url: serviceUrl,
                 dataType: "json",
                 data: searchParams,
                 success: function(res) {
                 	 updateLoading(30);
                 	 delayFlag = true;
                     setTimeout(function() {
                         if (res != '') {
                         	 updateLoading(35);
                         	 
                         	 makeGoogleMarkers(res);
                             
                             updateLoading(70);
                             colorLayer.addColorObj(selectedColor);
                             colorLayer.refresh();
                             updateLoading(90);
                         } else {
                             $('#msgDiv').text('No results');
                             setTimeout(function() {
    					    	$('#msgDiv').text('');
    					     }, 4000);
                         }
                         //hide masked conteiner for search form
                         setTimeout(function() {
					    	oPanel2.cancel();
					     }, 3000);

                     }, 300);

                 }
             }); //close $.ajax(

    setTimeout(function() {
    	if(!delayFlag){
    		updateLoading(15);
    	}
    }, 1500);
    } else {
        oPanel2.cancel();
    }
}


var markerName = Array();
var techInfo = [];
var woInfo = [];

function makeGoogleMarkers(result)
{
	if (typeof(result.result) == 'undefined' || result.result != 'ok') {
		// wrong responce
		$('#msgDiv').text('Error');
		return;
	}
	var res = result.data
	var consts = result.consts;
	var arrLocalMarkers = Array();
	
	var i = 0;
	for (var item in res) {
		
		if (isNaN(item)) {
			continue;
		}
		
	    //make key
		var markerIcon = new GIcon();
		if (res[item].length > 1) {
			markerIcon.image= consts.markerIconMultiple;
		} else {
			markerIcon.image= consts.markerIconOne;
		}
		
		markerIcon.iconSize=new GSize(26,37); 
		markerIcon.iconAnchor=new GPoint(9,31);
		markerIcon.infoWindowAnchor=new GPoint(9,31);
		markerIcon.shadow='';
		
		if (result.type == 'wo') {
			var woArr = new Array();
			var woIds = new Array();
			var emails = new Array();
			for (var tItem in res[item]) {
				if (isNaN(tItem)) {
					continue;
				}
				
				// make point
				woInfo[res[item][tItem].WIN_NUM] = res[item][tItem]
				woArr[woArr.length] = woInfo[res[item][tItem].WIN_NUM];
				woIds[woIds.length] = res[item][tItem].WIN_NUM;
			}
			
			var point = new GLatLng(res[item][0].Latitude, res[item][0].Longitude);
			markerName[i] = new GMarker(point, markerIcon);
			markerName[i].infoWOId = woIds;
			GEvent.addListener(markerName[i],'click',function(){
					if (typeof(woInfo[this.infoWOId[0]].Location) == 'undefined') {
						getWoDetailRequest(this.infoWOId, showWoDetailInGooglePopup );
					}
					this.openInfoWindowHtml('<div>' + showWOPopup(this.infoWOId) + '</div>');
				}
			);
			arrLocalMarkers.push(markerName[i]);
			//'". $i ."',point,'".$value['Start']."','".$value['Project']."','".$value['Site']."','".$value['Location']."','".$value['Bid_amount']."','".$value['Amount_Per']."','".$value['Tech']."','". $_POST['markerColor'] ."'
			pointsWo.push(new objMarkerWo(i++, point, /*res[item][tItem].Start, res[item][tItem].Project, res[item][tItem].Site, res[item][tItem].Location, res[item][tItem].Bid_amount, res[item][tItem].Amount_Per, res[item][tItem].Tech,*/ consts.color, woArr));
		} else {
		
			var techArr = new Array();
			var techIds = new Array();
			var emails = new Array();
			for (var tItem in res[item]) {
				if (isNaN(tItem)) {
					continue;
				}
				
				// make point
				techInfo[res[item][tItem].techId] = new markerTech(res[item][tItem].techId)
				techArr[techArr.length] = techInfo[res[item][tItem].techId];
				techIds[techIds.length] = res[item][tItem].techId;
				emails[emails.length]  = res[item][tItem].pEmail;
			}
			
			var point = new GLatLng(res[item][0].Latitude, res[item][0].Longitude);
			markerName[i] = new GMarker(point, markerIcon);
			markerName[i].infoTechId = techIds;
			GEvent.addListener(markerName[i],'click',function(){
					if (typeof(techInfo[this.infoTechId[0]].Firstname) == 'undefined') {
						getDetailRequest(this.infoTechId, showDetailInGooglePopup );
					}
					this.openInfoWindowHtml('<div>' + showTechsPopup(this.infoTechId) + '</div>');
				}
			);
			arrLocalMarkers.push(markerName[i]);
			points.push(new objMarker(i++, point, emails.toString(), consts.color, techArr));
		}
	}
	
    if (typeof(consts.mapCenter) == 'undefined') {
    	setTimeout(function(){if(goomap.getZoom()>7){goomap.zoomOut();goomap.zoomOut();}},2500);
    } else {
    	if (typeof(consts.mapCenter.Lng) != 'undefined') {
    		setTimeout(function(){setMapCenter(consts.mapCenter.Lat,consts.mapCenter.Lng,9);},2500);
    	} else if (typeof(consts.mapCenter.State) != 'undefined') {
    		setCenterToAddress(consts.mapCenter.State + ',USA');
    	}
    }

	
	localCluster = createCluster(consts.color, consts.markerCluster);
	localCluster._totalMarkers = i;
	localCluster.addMarkers(arrLocalMarkers); 
	arrLocalMarkers=Array();
	refreshMapDelayed();
}


function showWOPopup(wos)
{
	var company = document.getElementById('PMCompany').value;
	var result = '<div style=\'overflow-y:scroll;width:300px;height:195px;\'>';
	if (wos.length > 1) {
		result += '<b>' + wos.length + ' work orders:</b><br/><br/>'
	}
	for (var tItem in wos) {
		if (isNaN(tItem)) {
			continue;
		}
		
		result += showWOInfo(woInfo[wos[tItem]].WIN_NUM);
	}
	result += '</div>'
	return result;
}


function showTechsPopup(techs)
{
	var company = document.getElementById('PMCompany').value;
	var result = '<div style=\'overflow-y:scroll;width:300px;height:195px;\'>';
	if (techs.length > 1) {
		result += '<b>' + techs.length + ' technicians:</b><br/><br/>'
	}
	for (var tItem in techs) {
		if (isNaN(tItem)) {
			continue;
		}
		
		techItem = techInfo[techs[tItem]];
		result += showTechInfo(techItem.TechID);
	}
	result += '</div>'
	return result;
}


function showTechInfo(techId)
{
	var company = document.getElementById('PMCompany').value;
	var result = '';
	techItem = techInfo[techId];
	if (typeof(techItem.Firstname) == 'undefined') {
		//short version
		result += '<div id="techinfo_' + techId + '">';
		result += '<b>Tech ID:</b> <a href=\"../wosViewTechProfile.php?TechID=' + techItem.TechID + '&v=' + company + '\" target=\"_blank\">' + techItem.TechID + '</a><br/>';
		result += 'Loading...'; 
		result += '</div>';
		waitLoadTechs[techInfo[techId]] = window.setTimeout('displayTechInfo(' + techId + ')', 7000);
	} else {
		//full version
       	result += '<b>Tech ID:</b> <a href=\"../wosViewTechProfile.php?TechID=' + techItem.TechID + '&v=' + company + '\" target=\"_blank\">' + techItem.TechID + '</a><br/>';
    	result += techItem.Firstname + ' ' + techItem.Lastname + '</b><br />';
        $flsid ='';
    	if (company == 'FLS' && techItem.FLSID != '' && techItem.FLSID != 'NULL'){
    		result += 'FLS ID: ' + techItem.FLSID + ' <br/>';
    		$flsid = techItem.FLSID;
        }
    	if (techItem.PrimPhoneType != '') result += techItem.PrimPhoneType + ': ';

    	result += techItem.PrimaryPhone + '<br/>';
    	result += '<a href=\"mailto:' + techItem.PrimaryEmail + '\">' + techItem.PrimaryEmail + '</a><br/>';
    	result += techItem.Address1 + ' ';

    	if (techItem.Address2 != '') result +=  techItem.Address2 + ' ';
    	if (techItem.City != '')     result +=  ', ' + techItem.City + ' ';
    	if (techItem.State != '')    result +=  ', ' + techItem.State + ' ';
    	if (techItem.Zipcode != '')  result +=  ', ' + techItem.Zipcode + ' ';
		
		result += "<br/>";

    	if (techItem.Preferred == 'Yes') result += 'Preferred: Yes<br/>'; 
    	else result += 'Preferred: No<br/>';

    	result += 'Metrics:<br/>';
    	result += 'IMAC Calls: ' + techItem.Qty_IMAC_Calls + '<br/>';
    	result += 'Service Calls: ' + techItem.Qty_FLS_Service_Calls + '<br/>';
        result += 'No Shows: ' + techItem.No_Shows + '<br/>Back Outs: ' + techItem.Back_Outs + '<br/>';	

		var recAvg = parseFloat(techItem.SATRecommendedAvg);
		var prefAvg = parseFloat(techItem.SATPerformanceAvg);

    	result += 'Preference: ' + recAvg.toFixed(1) + ' (' + techItem.SATRecommendedTotal + ')<br/>';
    	result += 'Performance: ' + prefAvg.toFixed(1) + ' (' + techItem.SATPerformanceTotal + ')<br/><br />';
    	
    	if (typeof(waitLoadTechs[techId]) != 'undefined') {
    		clearInterval(waitLoadTechs[techId]);
    	}
	}
	
	return result;
}




function showWOInfo(woId)
{
	var company = document.getElementById('PMCompany').value;
	var result = '';
	woItem = woInfo[woId];

	if (typeof(woItem.Location) == 'undefined') {
		//short version
		result += '<div id="woinfo_' + woId + '">';
		result += '<b>WIN#: </b> <a href="../wosDetails.php?v=' + company + '&id=' + woItem.WIN_NUM + '" target="_blank">' + woItem.WIN_NUM + '</a><br />';
		result += 'Loading...'; 
		result += '</div>';
		waitLoadWO[woId] = window.setTimeout('displayWoInfo(' + woId + ')', 7000);
	} else {
		//full version
		result += '<b>WIN#: </b> <a href="../wosDetails.php?v=' + company + '&id=' + woItem.WIN_NUM + '" target="_blank">' + woItem.WIN_NUM + '</a><br />';
		result += '<b>Client WO ID: </b>' + woItem.WO_ID + '<br />';
		if (woItem.StartDate != '') {
			result += '<b>Start date/time: </b>' + woItem.StartDate + '<br />';
		}
		if (woItem.SiteName != '' && woItem.SiteName != 'NULL') {
			result += '<b>Site:</b> ' + woItem.SiteName + '<br />';
		}
		if (woItem.Location) {
			result += '<b>Location:</b> ' + woItem.Location + '<br />';
		}
		
		if (woItem.bidAmount && woItem.bidAmount != 'NULL') {
			result += '<b>Bid amount:</b> $' + woItem.bidAmount;
	        if (woItem.amountPer && woItem.amountPer != 'NULL')
	        	result += ' per ' + woItem.amountPer + '<br />';
	        result += '<br /><br />';
	    }
		
		if (typeof(waitLoadWO[woId]) != 'undefined') {
    		clearInterval(waitLoadWO[woId]);
    	}
	}
	
	return result;
}


function showDetailInGooglePopup(res)
{
	for (var item in res) {
		if (isNaN(item)) {
			continue;
		}
		
		techInfo[res[item].TechID] = res[item];
		displayTechInfo(res[item].TechID);
		/*if ($('#techinfo_' + res[item].TechID + '').length) {
			$('#techinfo_' + res[item].TechID + '')[0].innerHTML = showTechInfo(res[item].TechID);
		}*/
	}
}

function displayTechInfo(techId)
{
	if ($('#techinfo_' + techId + '').length) {
		$('#techinfo_' + techId + '')[0].innerHTML = showTechInfo(techId);
	}
}

function showWoDetailInGooglePopup(res)
{
	for (var item in res) {
		if (isNaN(item)) {
			continue;
		}
		
		woInfo[res[item].WIN_NUM] = res[item];
		if ($('#woinfo_' + res[item].WIN_NUM + '').length) {
			$('#woinfo_' + res[item].WIN_NUM + '')[0].innerHTML = showWOInfo(res[item].WIN_NUM);
		}
	}
}

function displayWoInfo(woId)
{
	if ($('#woinfo_' + woId + '').length) {
		$('#woinfo_' + woId + '')[0].innerHTML = showWOInfo(woId);
	}
}

function getDetailRequest(techs, resultEvent)
{
 	$.ajax({
        type: "POST",
        url: "/widgets/mapping/index/getdetails/",
        dataType: "json",
        data: 'company=' + $('#PMCompany :selected').val() + "&TechIDs=" + techs.toString(),
        success: resultEvent
    });
}

function getWoDetailRequest(wos, resultEvent)
{
 	$.ajax({
        type: "POST",
        url: "/widgets/mapping/index/getwodetails/",
        dataType: "json",
        data: 'company=' + $('#PMCompany :selected').val() + "&TB_UNIDs=" + wos.toString(),
        success: resultEvent
    });
}



function deleteMarkerColorFormSession(color) {
 	$.ajax({
        type: "POST",
        url: "ajax/delUsedMarkerColor.php",
        dataType: "html",
        data: "color="+color,
        success: colorLayer.deleteMarkersByColor(color)
    });
 }

 function updateProgressBar(percents)
 {
 	$('#errorDiv').attr('innerHTML', percents + ' percents...');
 }

 
 function techChagneOrder(col, order)
 {
    techCangePage(1);
    colCheckd = $(col).hasClass("orderChecked");

    if (!colCheckd) {
        $('#techOrderType').val(0);
    } else {
        if ($('#techOrderType').val() == 0) {
            $('#techOrderType').val(1);
        } else  {
            $('#techOrderType').val(0);
        }
    }
    $('#techOrderBy').val(order);
 }

 function techCangePage(page)
 {
     $('#techPage').val(page);
 }

 function processTechData(companyId)
 {
	 showSelectedMarkersData(companyId);
	 
 }

 function woChagneOrder(col, order)
 {
    woCangePage(1);
    colCheckd = $(col).hasClass("orderChecked");

    if (!colCheckd) {
        $('#woOrderType').val(0);
        $('#orderWo'+order).html("&darr;");
    } else {
        if ($('#woOrderType').val() == 0) {
            $('#woOrderType').val(1);
        } else  {
            $('#woOrderType').val(0);
        }
    }
    $('#woOrderBy').val(order);
 }

 function woCangePage(page)
 {
     $('#woPage').val(page);
 }

 function processWoData(companyId)
 {
	 showSelectedMarkersData(companyId);
	 
 }
 
 function getMappingForm(formType, v, divId)
 {
     var data = 'formType=' + escape(formType) + '&v=' + escape(v);
     $('#loading_percent').val('Loading...');
     //$('#loadingDiv').attr('innerHTML', '<img src="../../images/map/loading.gif" />');

     $.ajax({
                  type: "POST",
                  url: "get_forms.php",
                  dataType: "html",
                  data: data,
                  success: function(res) {
                  	 document.getElementById('loadingDiv').style.display = 'none';
                      $('#' + divId).attr('innerHTML', res);

                      setTimeout(function() {
                      	initializeMapTechs();

                      	document.getElementById('formDiv').style.display = 'block';
                      }, 500);
                  }
              }); //close $.ajax(
 }

 // For Work Orders
 function getMappingFormWo(formType, v, divId)
 {
     var data = 'formType=' + escape(formType) + '&v=' + escape(v);

     if ($('#PMCompany').val()) {
         data +='&PMCompany=' + $('#PMCompany').val();
     }
     $('#loading_percent').val('Loading...');
     $.ajax({
                  type: "POST",
                  url: "get_forms.php",
                  dataType: "html",
                  data: data,
                  success: function(res) {
                  	 document.getElementById('loadingDiv').style.display = 'none';
                      $('#' + divId).attr('innerHTML', res);

                      setTimeout(function() {
                      	$('#StartDate').calendar({dateFormat: 'MDY/'});
 		                $('#EndDate').calendar({dateFormat: 'MDY/'});
                      	document.getElementById('formDiv').style.display = 'block';
                      }, 1000);
                  }
              }); //close $.ajax(
 }
