<?php

$companyID = '';

if (!empty($_POST['v'])) {
    $companyID = $_POST['v'];
}

$projectHtml = getProjectsOptions();

?>
<script language="JavaScript">
$(document).ready(function(){
 $('#StartDate').calendar({dateFormat: 'MDY/'});
 $('#EndDate').calendar({dateFormat: 'MDY/'});
}); 
</script>
<h2>Select Work Orders</h2>
<br /><br />
<form id="searchForm" name="searchForm" onsubmit="mappingFormSubmit(); return false;" method="post">
<input name="formType" id="formType" type="hidden" value="wo" />
<select id="Project" name="Project">
<?=$projectHtml?>
</select>
<br /><br />
<span style="font-weight: bold">Date Range: Between</span>
<input type="text" name="StartDate" id="StartDate">
<span style="font-weight: bold">And</span>
<input type="text" name="EndDate" id="EndDate">
<br /><br />
<span style="font-weight: bold">Work Order Stage</span>
<select id="Work_Order" name="WorkOrder">
    <option value="Published" selected="selected">Published</option>
    <option value="Created" >Created</option>
    <option value="Assigned" >Assigned</option>
    <option value="Work_Done" >Work Done</option>
    <option value="Approved" >Approved</option>
    <option value="In_Accounting" >In Accounting</option>
    <option value="Incomplete" >Incomplete</option>
    <option value="Complete" >Complete</option>
    <option value="All" >All</option>
    <option value="Deactivated" >Deactivated</option>
</select>


<span style="font-weight: bold">Pin color</span>
<?php
$dd_disabled = false;
if (isset($_SESSION['usedColors'])) {
    if ($_SESSION['usedColors'] == Core_Map::getColorMarkers()) {
        $dd_disabled = true;
    }
}
if ($dd_disabled) {
?>
<select name="markerColor" id="markerColor" disabled>
</select>
<?php
} else {
?>	
<select name="markerColor" id="markerColor">
<?php
    $markerOptions = Core_Map::getColorMarkers();
    foreach ($markerOptions as $key=>$val) {
        //out if marker color not in list of used colors
        if (isset($_SESSION['usedColors'])) {
            if (!in_array($val, $_SESSION['usedColors'])) {
                echo '<option value="' . $key . '">' . $val . '</option>';
            }
        } else {
     	   echo '<option value="' . $key . '">' . $val . '</option>';
        }
    }
}
?>
</select> 
<br /><br />
<span style="font-weight: bold">Zoom In To: 
<input type="checkbox" name="ZoomIn" value="1" /> States:</span>
<select id="State" name="State" ><?=$statesHtml?></select><br /><br />
<div style="display:inline; padding-left:10px;"><b>Run: </b><input id="doSearch" type="submit" name="ReloadTabFrame" value=""></div>
</form>
<br /><br />