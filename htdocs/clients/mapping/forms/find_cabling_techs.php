<?php

$companyID = '';

if (!empty($_POST['v'])) {
    $companyID = $_POST['v'];
}

function getSubmittedOrDefaultField($name, $default = "") {
		return (isset($params[$name]) ? $params[$name] : $default);
}

?>
<h2>Find Cabling Techs:</h2>
<br /><br />
<form id="searchForm" name="searchForm" method="post" onsubmit="mappingFormSubmit(); return false;">
    <div class="formBox">
    <fieldset class="fieldGroup">
<div class="formRow6">
<b>Select Arrow Color:</b>
<?php
$dd_disabled = false;
if (isset($_SESSION['usedColors'])) {
    if ($_SESSION['usedColors'] == Core_Map::getColorMarkers()) {
        $dd_disabled = true;
    }
}
if ($dd_disabled) {
?>
<select name="markerColor" id="markerColor" disabled>
</select>
<?php
} else {
?>	
<select name="markerColor" id="markerColor">
<?php
    $markerOptions = Core_Map::getColorMarkers();
    foreach ($markerOptions as $key=>$val) {
        //out if marker color not in list of used colors
        if (isset($_SESSION['usedColors'])) {
            if (!in_array($val, $_SESSION['usedColors'])) {
                echo '<option value="' . $key . '">' . $val . '</option>';
            }
        } else {
     	   echo '<option value="' . $key . '">' . $val . '</option>';
        }
    }
}
?>
</select> &nbsp;&nbsp;
          <input id="hideBanned" name="hideBanned" type="checkbox" checked />
            Hide Client Denied &nbsp;&nbsp;
            <input id="preferredOnly" name="preferredOnly" type="checkbox" />
            Show Client Preferred Only
          </div>
          <br />
            <div class="verdana2bold">Search by Distance:</div>
            <div class="formRow6">
                <div>
                    ZIP Code
                </div>
                <div>
                    <input name="ProximityZipCode" id="ProximityZipCode" size="8" value="<?php echo getSubmittedOrDefaultField("ProximityZipCode")?>" type="text" />
                    <input name="ProximityLat" id="ProximityLat" type="hidden" value="<?php echo getSubmittedOrDefaultField("ProximityLat")?>" />
                    <input name="ProximityLng" id="ProximityLng" type="hidden" value="<?php echo getSubmittedOrDefaultField("ProximityLng")?>" />
                    <input name="formType" id="formType" type="hidden" value="find_cabling_techs" />
                </div>
                <div>
                    <select id="ProximityDistance" name="ProximityDistance" ><?=$distanceHtml?></select>
                </div>
                <div style="width:100%; clear:none;"></div>
                <div>
                    <b>Run: </b><input id="doSearch" name="doSearch" value="" type="submit" />
                </div>
            </div>
        </fieldset>

        <fieldset class="fieldGroup">
            <div class="verdana2bold">Filters:</div>
            <div class="verdana2bold">Search by Technician:</div>
            <div class="formRow3">
                   <div>
                    Tech ID<br/><input  size="10" maxlength="10" name="TechID" id="TechID" value="<?php echo getSubmittedOrDefaultField("TechID")?>" type="text" />
                </div>
                <div>
                    UserName<br/><input  size="25" maxlength="255" name="UserName" id="UserName" value="<?php echo getSubmittedOrDefaultField("UserName")?>" type="text" />
                </div>
                <div>
                    Primary Email<br/><input  size="25" maxlength="255" name="PrimaryEmail" id="PrimaryEmail" value="<?php echo getSubmittedOrDefaultField("PrimaryEmail")?>" type="text"/>
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Last Name<br/><input  size="25" maxlength="255" name="LastName" id="LastName" value="<?php echo getSubmittedOrDefaultField("LastName")?>" type="text" />
                </div>
                <div>
                    FirstName<br/><input  size="25" maxlength="255" name="FirstName" id="FirstName" value="<?php echo getSubmittedOrDefaultField("FirstName")?>" type="text" />
                </div>
                <div>
                    <br/><br/><br/>
                </div>
            </div>
            <div class="formRow3">
                <div>
                    City<br/><input  size="25" maxlength="255" name="City" id="City" value="<?php echo getSubmittedOrDefaultField("City")?>" type="text" />
                </div>
                <div>
                    State<br/><select id="State" name="State" ><?=$statesHtml?></select>
                </div>
                <div>
                    Country<br/><select id="Country" name="Country" ><?=$countriesHtml?></select>&nbsp;
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Hourly Pay (&lt; or =)<br/><input  size="10" maxlength="255" name="HourlyPay" id="HourlyPay" value="<?php echo getSubmittedOrDefaultField("HourlyPay")?>" type="text" />
                </div>
                <? if ($companyID == 'FLS') { ?>
				<div>
                	<table>
                    	<tr>
                        	<td>FLS ID</td><td><input name="FLS_Photo_ID" id="FLS_Photo_ID" value="1" type="checkbox" />Photo ID</td>
                        </tr>
                        <tr>
                        	<td><input  size="10" maxlength="10" name="FLSID" id="FLSID" value="" type="text" /> -or-&nbsp;&nbsp;</td><td><input name="FLSIDExists" id="FLSIDExists" value="1" type="checkbox" />All FLS Techs</td>
                        </tr>
					</table>
                </div>
                <div>
              		Warehouse ID<br/><input  size="10" maxlength="10" name="FLSWhse" id="FLSWhse" value="" type="text" />
				</div>                             
                <?}?>
            </div>
        </fieldset>

        <fieldset class="fieldGroup">
            <div class="verdana2bold">Search by Rankings:</div>
            <div class="formRow3">
                <div>
                    FS Certification
                </div>
                <div>
                </div>
                <div>
                    <input id="FS_Cert_Test_Pass" name="FS_Cert_Test_Pass" value="Y" <?php echo (getSubmittedOrDefaultField("FS_Cert_Test_Pass") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="FS_Cert_Test_Pass" name="FS_Cert_Test_Pass" value="N" <?php echo (getSubmittedOrDefaultField("FS_Cert_Test_Pass") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="FS_Cert_Test_Pass" name="FS_Cert_Test_Pass" value="" <?php echo (getSubmittedOrDefaultField("FS_Cert_Test_Pass") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    # of Overall Tech Recommendation Greater or Equal to:
                </div>
                <div>
                </div>
                <div>
                    <input  size="3" maxlength="3" name="SATRecommendedTotal" id="SATRecommendedTotal" value="<?php echo getSubmittedOrDefaultField("SATRecommendedTotal")?>" type="text" />
                </div>
            </div>            
            <div class="formRow3">
                <div>
                    # of Tech Event Performance Greater or Equal to:
                </div>
                <div>
                </div>
                <div>
                    <input  size="3" maxlength="3" name="SATPerformanceTotal" id="SATPerformanceTotal" value="<?php echo getSubmittedOrDefaultField("SATPerformanceTotal")?>" type="text" />
                </div>
            </div>
            <?php
				// Company Specific Criteria
				if ($companyID == "FLS") {
			?>

		    <div class="formRow3">
                 <div>
                    FLS CSP Form
                </div>
                <div>
                </div>
           		<div>
                	<input id="FLSCSP_Rec" name="FLSCSP_Rec" value="Y" type="radio">Yes<input id="FLSCSP_Rec" name="FLSCSP_Rec" value="N" type="radio">No<input id="FLSCSP_Rec" name="FLSCSP_Rec" value="" type="radio">Any
                </div>
            </div>
		    <div class="formRow3">
                 <div>
                    Cert Hallmark POS
                </div>
                <div>
                </div>
           		<div>
                	<input id="Cert_Hallmark_POS" name="Cert_Hallmark_POS" value="Y" type="radio">Yes<input id="Cert_Hallmark_POS" name="Cert_Hallmark_POS" value="N" type="radio">No<input id="Cert_Hallmark_POS" name="Cert_Hallmark_POS" value="" type="radio">Any
                </div>
            </div>
		    <div class="formRow3">
                 <div>
                    Cert Starbucks POS
                </div>
                <div>
                </div>
           		<div>
                	<input id="Starbucks_Cert" name="Starbucks_Cert" value="Y" type="radio">Yes<input id="Starbucks_Cert" name="Starbucks_Cert" value="N" type="radio">No<input id="Starbucks_Cert" name="Starbucks_Cert" value="" type="radio">Any
                </div>
            </div>
		    <div class="formRow3">
                 <div>
                    Cert Maurices POS
                </div>
                <div>
                </div>
           		<div>
                	<input id="Cert_Maurices_POS" name="Cert_Maurices_POS" value="Y" type="radio">Yes<input id="Cert_Maurices_POS" name="Cert_Maurices_POS" value="N" type="radio">No<input id="Cert_Maurices_POS" name="Cert_Maurices_POS" value="" type="radio">Any
                </div>
            </div>

            <?php
            }
			?>            
        </fieldset>
        <fieldset class="fieldGroup">
        	<br/>
            <div class="formRow3">
                <div>
                    Qty IMAC Calls (&gt;=)
                </div>
                <div>
                </div>
                <div>
                    <input  size="25" maxlength="255" name="Qty_IMAC_Call" id="Qty_IMAC_Call" value="<?php echo getSubmittedOrDefaultField("Qty_IMAC_Call")?>" type="text">
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Qty Service Calls (&gt;=)
                </div>
                <div>
                </div>
                <div>
                    <input  size="25" maxlength="255" name="Qty_FLS_Service_Call" id="Qty_FLS_Service_Call" value="<?php echo getSubmittedOrDefaultField("Qty_FLS_Service_Call")?>" type="text">&nbsp;
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Qty No Shows (&lt;=)
                </div>
                <div>
                </div>
                <div>
                    <input  size="8" maxlength="255" name="No_Shows" id="No_Shows" value="<?php echo getSubmittedOrDefaultField("No_Shows")?>" type="text">&nbsp;
                </div>
            </div>
            <div class="verdana2bold"><b>Ratings:</b> Your search will find all techs with average rating values greater than or equal to the values you specifty in each category. Leave rating categories blank that you don't want to include in the search.
            <br><br>Preference and Performance Ratings by Clients</div>
            <div class="formRow3">
                <div>
                    Overall Tech Recommendation (&gt;=)
                </div>
                <div>
                </div>
                <div>
                    <input  size="1" maxlength="1" name="SATRecommendedAvg" id="SATRecommendedAvg" value="" type="text"> 
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Tech Event Performance (&gt;=)
                </div>
                <div>
                </div>
                <div>
                    <input  size="1" maxlength="1" name="SATPerformanceAvg" id="SATPerformanceAvg" value="" type="text">
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Can read wiring diagrams?
                </div>
                <div>
                </div>
                <div>
                    <input id="ReadWiringDiagrams" name="ReadWiringDiagrams" value="Y" <?php echo (getSubmittedOrDefaultField("ReadWiringDiagrams") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="ReadWiringDiagrams" name="ReadWiringDiagrams" value="N" <?php echo (getSubmittedOrDefaultField("ReadWiringDiagrams") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="ReadWiringDiagrams" name="ReadWiringDiagrams" value="" <?php echo (getSubmittedOrDefaultField("ReadWiringDiagrams") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Has experience installing surveillance DVR/Cam systems?
                </div>
                <div>
                </div>
                <div>
                    <input id="ExpInstallSurv" name="ExpInstallSurv" value="Y" <?php echo (getSubmittedOrDefaultField("ExpInstallSurv") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="ExpInstallSurv" name="ExpInstallSurv" value="N" <?php echo (getSubmittedOrDefaultField("ExpInstallSurv") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="ExpInstallSurv" name="ExpInstallSurv" value="" <?php echo (getSubmittedOrDefaultField("ExpInstallSurv") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Skilled Cat5 and/or fiber cabling?
                </div>
                <div>
                </div>
                <div>
                    <input id="SkilledCat5Fiber" name="SkilledCat5Fiber" value="Y" <?php echo (getSubmittedOrDefaultField("SkilledCat5Fiber") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="SkilledCat5Fiber" name="SkilledCat5Fiber" value="N" <?php echo (getSubmittedOrDefaultField("SkilledCat5Fiber") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="SkilledCat5Fiber" name="SkilledCat5Fiber" value="" <?php echo (getSubmittedOrDefaultField("SkilledCat5Fiber") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Performed over 40 hours of Cat 5 cabling contracts in the past 6 months?
                </div>
                <div>
                </div>
                <div>
                    <input id="AtLeast40HrsLast6Months" name="AtLeast40HrsLast6Months" value="Y" <?php echo (getSubmittedOrDefaultField("AtLeast40HrsLast6Months") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="AtLeast40HrsLast6Months" name="AtLeast40HrsLast6Months" value="N" <?php echo (getSubmittedOrDefaultField("AtLeast40HrsLast6Months") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="AtLeast40HrsLast6Months" name="AtLeast40HrsLast6Months" value="" <?php echo (getSubmittedOrDefaultField("AtLeast40HrsLast6Months") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Have a truck to carry ladders and cable?
                </div>
                <div>
                </div>
                <div>
                    <input id="HaveTruck" name="HaveTruck" value="Y" <?php echo (getSubmittedOrDefaultField("HaveTruck") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="HaveTruck" name="HaveTruck" value="N" <?php echo (getSubmittedOrDefaultField("HaveTruck") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="HaveTruck" name="HaveTruck" value="" <?php echo (getSubmittedOrDefaultField("HaveTruck") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Own or can source, ladders, cable, and cable testers?
                </div>
                <div>
                </div>
                <div>
                    <input id="OwnOrSourceCablingEquipment" name="OwnOrSourceCablingEquipment" value="Y" <?php echo (getSubmittedOrDefaultField("OwnOrSourceCablingEquipment") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="OwnOrSourceCablingEquipment" name="OwnOrSourceCablingEquipment" value="N" <?php echo (getSubmittedOrDefaultField("OwnOrSourceCablingEquipment") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="OwnOrSourceCablingEquipment" name="OwnOrSourceCablingEquipment" value="" <?php echo (getSubmittedOrDefaultField("OwnOrSourceCablingEquipment") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Have required licenses for low voltage work?
                </div>
                <div>
                </div>
                <div>
                    <input id="LowVoltageLicensed" name="LowVoltageLicensed" value="Y" <?php echo (getSubmittedOrDefaultField("LowVoltageLicensed") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="LowVoltageLicensed" name="LowVoltageLicensed" value="N" <?php echo (getSubmittedOrDefaultField("LowVoltageLicensed") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="LowVoltageLicensed" name="LowVoltageLicensed" value="" <?php echo (getSubmittedOrDefaultField("LowVoltageLicensed") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
        </fieldset>

        <fieldset>
            <div class="verdana2bold">Search by Equipment:</div>
            <div class="formRow3">
                <div>
                    Corded masonry drill hammer and bits up to 1" diameter
                </div>
                <div>
                </div>
                <div>
                    <input id="CordMasonryDrillHammer" name="CordMasonryDrillHammer" value="Y" <?php echo (getSubmittedOrDefaultField("CordMasonryDrillHammer") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="CordMasonryDrillHammer" name="CordMasonryDrillHammer" value="N" <?php echo (getSubmittedOrDefaultField("CordMasonryDrillHammer") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="CordMasonryDrillHammer" name="CordMasonryDrillHammer" value="" <?php echo (getSubmittedOrDefaultField("CordMasonryDrillHammer") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                  Rotary coax cable stripper for RG-59 and RG-6 cable              </div>
                <div>
                </div>
                <div>
                    <input id="RotaryCoaxCable" name="RotaryCoaxCable" value="Y" <?php echo (getSubmittedOrDefaultField("RotaryCoaxCable") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="RotaryCoaxCable" name="RotaryCoaxCable" value="N" <?php echo (getSubmittedOrDefaultField("RotaryCoaxCable") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="RotaryCoaxCable" name="RotaryCoaxCable" value="" <?php echo (getSubmittedOrDefaultField("RotaryCoaxCable") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                  Rechargeable CCTV installer's LCD 4              </div>
                <div>
                </div>
                <div>
                    <input id="RechargeCCTV" name="RechargeCCTV" value="Y" <?php echo (getSubmittedOrDefaultField("RechargeCCTV") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="RechargeCCTV" name="RechargeCCTV" value="N" <?php echo (getSubmittedOrDefaultField("RechargeCCTV") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="RechargeCCTV" name="RechargeCCTV" value="" <?php echo (getSubmittedOrDefaultField("RechargeCCTV") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Volt/Ohm meter
                </div>
                <div>
                </div>
                <div>
                    <input id="VoltOhmMeter" name="VoltOhmMeter" value="Y" <?php echo (getSubmittedOrDefaultField("VoltOhmMeter") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="VoltOhmMeter" name="VoltOhmMeter" value="N" <?php echo (getSubmittedOrDefaultField("VoltOhmMeter") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="VoltOhmMeter" name="VoltOhmMeter" value="" <?php echo (getSubmittedOrDefaultField("VoltOhmMeter") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Ladder 6'
                </div>
                <div>
                </div>
                <div>
                    <input id="Ladder_6" name="Ladder_6" value="Y" <?php echo (getSubmittedOrDefaultField("Ladder_6") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="Ladder_6" name="Ladder_6" value="N" <?php echo (getSubmittedOrDefaultField("Ladder_6") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="Ladder_6" name="Ladder_6" value="" <?php echo (getSubmittedOrDefaultField("Ladder_6") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Ladder 12'
                </div>
                <div>
                </div>
                <div>
                    <input id="Ladder_12" name="Ladder_12" value="Y" <?php echo (getSubmittedOrDefaultField("Ladder_12") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="Ladder_12" name="Ladder_12" value="N" <?php echo (getSubmittedOrDefaultField("Ladder_12") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="Ladder_12" name="Ladder_12" value="" <?php echo (getSubmittedOrDefaultField("Ladder_12") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Ladder 20'+
                </div>
                <div>
                </div>
                <div>
                    <input id="Ladder_20Plus" name="Ladder_20Plus" value="Y" <?php echo (getSubmittedOrDefaultField("Ladder_20Plus") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="Ladder_20Plus" name="Ladder_20Plus" value="N" <?php echo (getSubmittedOrDefaultField("Ladder_20Plus") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="Ladder_20Plus" name="Ladder_20Plus" value="" <?php echo (getSubmittedOrDefaultField("Ladder_20Plus") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Tone Generator and Wand
                </div>
                <div>
                </div>
                <div>
                    <input id="ToneGeneratorAndWand" name="ToneGeneratorAndWand" value="Y" <?php echo (getSubmittedOrDefaultField("ToneGeneratorAndWand") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="ToneGeneratorAndWand" name="ToneGeneratorAndWand" value="N" <?php echo (getSubmittedOrDefaultField("ToneGeneratorAndWand") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="ToneGeneratorAndWand" name="ToneGeneratorAndWand" value="" <?php echo (getSubmittedOrDefaultField("ToneGeneratorAndWand") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Butt Set
                </div>
                <div>
                </div>
                <div>
                    <input id="ButtSet" name="ButtSet" value="Y" <?php echo (getSubmittedOrDefaultField("ButtSet") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="ButtSet" name="ButtSet" value="N" <?php echo (getSubmittedOrDefaultField("ButtSet") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="ButtSet" name="ButtSet" value="" <?php echo (getSubmittedOrDefaultField("ButtSet") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Digital VOM Meter
                </div>
                <div>
                </div>
                <div>
                    <input id="DigitalVOMMeter" name="DigitalVOMMeter" value="Y" <?php echo (getSubmittedOrDefaultField("DigitalVOMMeter") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="DigitalVOMMeter" name="DigitalVOMMeter" value="N" <?php echo (getSubmittedOrDefaultField("DigitalVOMMeter") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="DigitalVOMMeter" name="DigitalVOMMeter" value="" <?php echo (getSubmittedOrDefaultField("DigitalVOMMeter") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Continuity Tester
                </div>
                <div>
                </div>
                <div>
                    <input id="ContinuityTester" name="ContinuityTester" value="Y" <?php echo (getSubmittedOrDefaultField("ContinuityTester") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="ContinuityTester" name="ContinuityTester" value="N" <?php echo (getSubmittedOrDefaultField("ContinuityTester") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="ContinuityTester" name="ContinuityTester" value="" <?php echo (getSubmittedOrDefaultField("ContinuityTester") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Punch Tool with 66 blade
                </div>
                <div>
                </div>
                <div>
                    <input id="PunchTool66" name="PunchTool66" value="Y" <?php echo (getSubmittedOrDefaultField("PunchTool66") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="PunchTool66" name="PunchTool66" value="N" <?php echo (getSubmittedOrDefaultField("PunchTool66") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="PunchTool66" name="PunchTool66" value="" <?php echo (getSubmittedOrDefaultField("PunchTool66") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Punch Tool with 110 blade
                </div>
                <div>
                </div>
                <div>
                    <input id="PunchTool110" name="PunchTool110" value="Y" <?php echo (getSubmittedOrDefaultField("PunchTool110") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="PunchTool110" name="PunchTool110" value="N" <?php echo (getSubmittedOrDefaultField("PunchTool110") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="PunchTool110" name="PunchTool110" value="" <?php echo (getSubmittedOrDefaultField("PunchTool110") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Punch Tool with Bix blade
                </div>
                <div>
                </div>
                <div>
                    <input id="PunchToolBix" name="PunchToolBix" value="Y" <?php echo (getSubmittedOrDefaultField("PunchToolBix") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="PunchToolBix" name="PunchToolBix" value="N" <?php echo (getSubmittedOrDefaultField("PunchToolBix") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="PunchToolBix" name="PunchToolBix" value="" <?php echo (getSubmittedOrDefaultField("PunchToolBix") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Punch Tool with Krone blade
                </div>
                <div>
                </div>
                <div>
                    <input id="PunchToolKrone" name="PunchToolKrone" value="Y" <?php echo (getSubmittedOrDefaultField("PunchToolKrone") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="PunchToolKrone" name="PunchToolKrone" value="N" <?php echo (getSubmittedOrDefaultField("PunchToolKrone") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="PunchToolKrone" name="PunchToolKrone" value="" <?php echo (getSubmittedOrDefaultField("PunchToolKrone") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Crimp Tool with RJ11
                </div>
                <div>
                </div>
                <div>
                    <input id="CrimpToolRJ11" name="CrimpToolRJ11" value="Y" <?php echo (getSubmittedOrDefaultField("CrimpToolRJ11") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="CrimpToolRJ11" name="CrimpToolRJ11" value="N" <?php echo (getSubmittedOrDefaultField("CrimpToolRJ11") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="CrimpToolRJ11" name="CrimpToolRJ11" value="" <?php echo (getSubmittedOrDefaultField("CrimpToolRJ11") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Crimp Tool with RJ45
                </div>
                <div>
                </div>
                <div>
                    <input id="CrimpToolRJ45" name="CrimpToolRJ45" value="Y" <?php echo (getSubmittedOrDefaultField("CrimpToolRJ45") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="CrimpToolRJ45" name="CrimpToolRJ45" value="N" <?php echo (getSubmittedOrDefaultField("CrimpToolRJ45") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="CrimpToolRJ45" name="CrimpToolRJ45" value="" <?php echo (getSubmittedOrDefaultField("CrimpToolRJ45") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    50' Fish Tape
                </div>
                <div>
                </div>
                <div>
                    <input id="FishTape50" name="FishTape50" value="Y" <?php echo (getSubmittedOrDefaultField("FishTape50") == "Y" ? "checked=\"checked\"" : "")?> type="radio">Yes<input id="FishTape50" name="FishTape50" value="N" <?php echo (getSubmittedOrDefaultField("FishTape50") == "N" ? "checked=\"checked\"" : "")?> type="radio">No<input id="FishTape50" name="FishTape50" value="" <?php echo (getSubmittedOrDefaultField("FishTape50") == "" ? "checked=\"checked\"" : "")?> type="radio">Any
                </div>
            </div>
        </fieldset>
    </div>
</form>