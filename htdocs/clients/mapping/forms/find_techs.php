<?php

$companyID = '';

if (!empty($_POST['v'])) {
    $companyID = $_POST['v'];
}




$HP_Html = "<option value=\"\">Show All</option><option value=\"Any HP Cert\">Any HP Cert</option><option value=\"HP Cert # HP-Q01\">HP Cert # HP-Q01</option><option value=\"HP Cert # HP-H08\">HP Cert # HP-H08</option>";


?>

<h2>Find Techs:</h2>
<br /><br />
<form id="searchForm" name="searchForm" action="" method="post" onsubmit="mappingFormSubmit(); return false;">
    <div class="formBox">

    <fieldset class="fieldGroup">
        <div class="formRow6">
<b>Select Arrow Color:</b> 
<?php
$dd_disabled = false;
if (isset($_SESSION['usedColors'])) {
    if ($_SESSION['usedColors'] == Core_Map::getColorMarkers()) {
        $dd_disabled = true;
    }
}
if ($dd_disabled) {
?>
<select name="markerColor" id="markerColor" disabled>
</select>
<?php
} else {
?>	
<select name="markerColor" id="markerColor">
<?php
    $markerOptions = Core_Map::getColorMarkers();
    foreach ($markerOptions as $key=>$val) {
        //out if marker color not in list of used colors
        if (isset($_SESSION['usedColors'])) {
            if (!in_array($val, $_SESSION['usedColors'])) {
                echo '<option value="' . $key . '">' . $val . '</option>';
            }
        } else {
     	   echo '<option value="' . $key . '">' . $val . '</option>';
        }
    }
}
?>
</select>&nbsp;&nbsp;
        
          <input id="hideBanned" name="hideBanned" type="checkbox" checked />
            Hide Client Denied &nbsp;&nbsp;
            <input id="preferredOnly" name="preferredOnly" type="checkbox" />
            Show Client Preferred Only
          </div>
          <br />
            <div class="verdana2bold">Search by Distance:</div>
            <div class="formRow6">
                <div>
                    ZIP Code
                </div>
                <div>
                    <input name="ProximityZipCode" id="ProximityZipCode" size="8" value="" type="text" />
                    <input name="ProximityLat" id="ProximityLat" type="hidden" value="" />
                    <input name="ProximityLng" id="ProximityLng" type="hidden" value="" />
                    <input name="formType" id="formType" type="hidden" value="find_techs" />
                </div>
                <div>
                    <select id="ProximityDistance" name="ProximityDistance" ><?=$distanceHtml?></select>
                </div>
                
                <div id="map_canvas" style="width: 100%; height: 300px; display: none"></div>
                <div style="width:100%; clear:none;"></div>
                <div>
                    <b>Run: </b><input id="doSearch" name="doSearch" value="" type="submit" />
                </div>
            </div>
        </fieldset>

        <fieldset class="fieldGroup">
            <div class="verdana2bold">Filters:</div>
            <div class="verdana2bold">Search by Technician:</div>
            <div class="formRow3">
                   <div>
                    Tech ID<br/><input  size="10" maxlength="10" name="TechID" id="TechID" value="" type="text" />
                </div>
                <div>
                    UserName<br/><input  size="25" maxlength="255" name="UserName" id="UserName" value="" type="text" />
                </div>
                <div>
                    Primary Email<br/><input  size="25" maxlength="255" name="PrimaryEmail" id="PrimaryEmail" value="" type="text"/>                                                                                            </div>
            </div>
            <div class="formRow3">
                <div>
                    Last Name<br/><input  size="25" maxlength="255" name="LastName" id="LastName" value="" type="text" />
                </div>
                <div>
                    FirstName<br/><input  size="25" maxlength="255" name="FirstName" id="FirstName" value="" type="text" />
                </div>
                <div>
                    <br/><br/><br/>
                </div>
            </div>
            <div class="formRow3">
                <div>
                    City<br/><input  size="25" maxlength="255" name="City" id="City" value="" type="text" />
                </div>
                <div>
                    State<br/><select id="State" name="State" ><?=$statesHtml?></select>
                </div>
                <div>
                    Country<br/><select id="Country" name="Country" ><?=$countriesHtml?></select>&nbsp;
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Hourly Pay (&lt; or =)<br/><input  size="10" maxlength="255" name="HourlyPay" id="HourlyPay" value="" type="text" />
                </div>
                <? if ($companyID == 'FLS') { ?>
				<div>
                	<table>
                    	<tr>
                        	<td>FLS ID</td><td><input name="FLS_Photo_ID" id="FLS_Photo_ID" value="1" type="checkbox" />Photo ID</td>
                        </tr>
                        <tr>
                        	<td><input  size="10" maxlength="10" name="FLSID" id="FLSID" value="" type="text" /> -or-&nbsp;&nbsp;</td><td><input name="FLSIDExists" id="FLSIDExists" value="1" type="checkbox" />All FLS Techs</td>
                        </tr>
					</table>
                </div>
                <div>
              		Warehouse ID<br/><input  size="10" maxlength="10" name="FLSWhse" id="FLSWhse" value="" type="text" />
				</div>                                
                <?}?>
            </div>
        </fieldset>

        <fieldset class="fieldGroup">
            <div class="verdana2bold">Search by Rankings:</div>
            <div class="formRow3">
                <div>
                    CompTIA
                </div>
                <div>
                </div>
                <div>
                    <input name="CompTIA" id="CompTIA" value="1" type="checkbox" /> (check for only CompTIA techs)
                </div>
            </div>
            <div class="formRow3">
                <div>
                    FS Certification
                </div>
                <div>
                </div>
                <div>
                    <input id="FS_Cert_Test_Pass" name="FS_Cert_Test_Pass" value="Y" type="radio">Yes<input id="FS_Cert_Test_Pass" name="FS_Cert_Test_Pass" value="N" type="radio">No<input id="FS_Cert_Test_Pass" name="FS_Cert_Test_Pass" value="" type="radio">Any
                </div>
            </div>
            
            <div class="formRow3">
                <div>
					HP Certification
                </div>
                <div>
                </div>
                <div>
                	<select id="HP_CertNum" name="HP_CertNum" ><?=$HP_Html?></select>
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Samsung Certification
                </div>
                <div>
                </div>
                <div>
                    <input id="SamsungCert" name="SamsungCert" value="Y" type="radio">Yes<input id="SamsungCert" name="SamsungCert" value="N" type="radio">No<input id="SamsungCert" name="SamsungCert" value="" type="radio">Any
                </div>
            </div>

            
            <div class="formRow3">
                <div>
                    Background Check
                </div>
                <div>
                </div>
                <div>
                    <input name="Bg_Test_Pass_Lite" id="Bg_Test_Pass_Lite" value="1" type="checkbox" />
                </div>
            </div>
            <div class="formRow3">
                <div>
                    # of Overall Tech Recommendation Greater or Equal to:
                </div>
                <div>
                </div>
                <div>
                    <input  size="3" maxlength="3" name="SATRecommendedTotal" id="SATRecommendedTotal" value="" type="text" />
                </div>
            </div>
            <div class="formRow3">
                <div>
                    # of Tech Event Performance Greater or Equal to:
                </div>
                <div>
                </div>
                <div>
                    <input  size="3" maxlength="3" name="SATPerformanceTotal" id="SATPerformanceTotal" value="" type="text" />
                </div>
            </div>
            <div class="formRow3">
                <div>
                    A+
                </div>
                <div>
                </div>
                <div>
                    <input name="APlus" id="APlus" value="1" type="checkbox" /> (check for only A+ techs)
                </div>
            </div>
<!--            <div class="formRow3">
                <div>
                    Or A+
                </div>
                <div>
                </div>
                <div>
                    <input name="APlus" id="APlus" value="1" type="checkbox">&nbsp;
                </div>
            </div>-->
            <div class="formRow3">
                <div>
                    MCSE
                </div>
                <div>
                </div>
                <div>
                    <input name="MCSE" id="MCSE" value="1" type="checkbox" /> (check for only MCSE techs)
                </div>
            </div>
<!--            <div class="formRow3">
                <div>
                    Or MCSE
                </div>
                <div>
                </div>
                <div>
                    <input name="MCSE" id="MCSE" value="1" type="checkbox">&nbsp;
                </div>
            </div>-->
            <div class="formRow3">
                <div>
                    CCNA
                </div>
                <div>
                </div>
                <div>
                    <input name="CCNA" id="CCNA" value="1" type="checkbox" /> (check for only CCNA techs)
                </div>
            </div>
<!--            <div class="formRow3">
                <div>
                    Or CCNA
                </div>
                <div>
                </div>

                <div>
                    <input name="CCNA" id="CCNA" value="1" type="checkbox">&nbsp;
                </div>
            </div>-->
            <div class="formRow3">
                <div>
                    Dell Certification
                </div>
                <div>
                </div>
                <div>
                    <input size="25" maxlength="255" name="DellCert" id="DellCert" value="" type="text">
                </div>
            </div>
            <div class="formRow3">
                <div>
                    BICSI
                </div>
                <div>
                </div>
                <div>
                    <input id="BICSI" name="BICSI" value="Y" type="radio">Yes<input id="BICSI" name="BICSI" value="N" type="radio">No<input id="BICSI" name="BICSI" value="" type="radio">Any
                </div>
            </div>
            <?php
				// Company Specific Criteria
				if ($companyID == "FLS") {
			?>


		    <div class="formRow3">
                 <div>
                    FLS CSP Form
                </div>
                <div>
                </div>
           		<div>
                	<input id="FLSCSP_Rec" name="FLSCSP_Rec" value="Y" type="radio">Yes<input id="FLSCSP_Rec" name="FLSCSP_Rec" value="N" type="radio">No<input id="FLSCSP_Rec" name="FLSCSP_Rec" value="" type="radio">Any
                </div>
            </div>
		    <div class="formRow3">
                 <div>
                    Cert Hallmark POS
                </div>
                <div>
                </div>
           		<div>
                	<input id="Cert_Hallmark_POS" name="Cert_Hallmark_POS" value="Y" type="radio">Yes<input id="Cert_Hallmark_POS" name="Cert_Hallmark_POS" value="N" type="radio">No<input id="Cert_Hallmark_POS" name="Cert_Hallmark_POS" value="" type="radio">Any
                </div>
            </div>
		    <div class="formRow3">
                 <div>
                    Cert Starbucks POS
                </div>
                <div>
                </div>
           		<div>
                	<input id="Starbucks_Cert" name="Starbucks_Cert" value="Y" type="radio">Yes<input id="Starbucks_Cert" name="Starbucks_Cert" value="N" type="radio">No<input id="Starbucks_Cert" name="Starbucks_Cert" value="" type="radio">Any
                </div>
            </div>
		    <div class="formRow3">
                 <div>
                    Cert Maurices POS
                </div>
                <div>
                </div>
           		<div>
                	<input id="Cert_Maurices_POS" name="Cert_Maurices_POS" value="Y" type="radio">Yes<input id="Cert_Maurices_POS" name="Cert_Maurices_POS" value="N" type="radio">No<input id="Cert_Maurices_POS" name="Cert_Maurices_POS" value="" type="radio">Any
                </div>
            </div>

            <?php
            }
			?>
            
            <?php
                // Company Specific Criteria
                if ($companyID == "NCR") {
            ?>
            <div class="formRow3">
                <div>
                    NCR Basic Cert
                </div>
                <div>
                </div>
                <div>
                    <input id="NCR_Basic_Cert" name="NCR_Basic_Cert" value="Y" type="radio">Yes<input id="NCR_Basic_Cert" name="NCR_Basic_Cert" value="N" type="radio">No<input id="NCR_Basic_Cert" name="NCR_Basic_Cert" value="" type="radio">Any
                </div>
            </div>
            <?php
                }
            ?>
        </fieldset>
        <fieldset class="fieldGroup">
        	<br/>
            <div class="formRow3">
                <div>
                    Qty IMAC Calls (&gt;=)
                </div>
                <div>
                </div>
                <div>
                    <input  size="25" maxlength="255" name="Qty_IMAC_Calls" id="Qty_IMAC_Calls" value="" type="text">
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Qty FLS Service Calls (&gt;=)
                </div>
                <div>
                </div>
                <div>
                    <input  size="25" maxlength="255" name="Qty_FLS_Service_Call" id="Qty_FLS_Service_Call" value="" type="text">&nbsp;
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Qty No Shows (&lt;=)
                </div>
                <div>
                </div>
                <div>
                    <input  size="8" maxlength="255" name="No_Shows" id="No_Shows" value="" type="text">&nbsp;
                </div>
            </div>
            <div class="verdana2bold"><b>Ratings:</b> Your search will find all techs with average rating values greater than or equal to the values you specifty in each category. Leave rating categories blank that you don't want to include in the search.
            <br><br>Preference and Performance Ratings by Clients</div>
            <div class="formRow3">
<!--                <div style="width: 66%">
                    Overall Rating (Overall Experience with Technician)
                </div>
                <div>
                    <input  size="1" maxlength="1" name="OverallRank" id="OverallRank" value="" type="text">&nbsp;
                </div>-->
                <div>
                    Overall Tech Recommendation (&gt;=)
                </div>
                <div>
                </div>
                <div>
                    <input  size="1" maxlength="1" name="SATRecommendedAvg" id="SATRecommendedAvg" value="" type="text"> 
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Tech Event Performance (&gt;=)
                </div>
                <div>
                </div>
                <div>
                    <input  size="1" maxlength="1" name="SATPerformanceAvg" id="SATPerformanceAvg" value="" type="text">
                </div>
            </div>

            <div class="verdana2bold">Technician Self Ratings reflecting his/her technical skills (5=Expert, 1/0 = no experience)</div>
             <div class="formRow6">
                <div>
                    Audio / Video (A/V)
                </div>
                <div>
                    <input type="text" value="" id="electronicsSelfRating" name="selfRating[electronicsSelfRating]" maxlength="1" size="1"/>
                </div>
                <div>
                    ATM
                </div>
                <div>
                    <input type="text" value="" id="atmSelfRating" name="selfRating[atmSelfRating]" maxlength="1" size="1"/>
                </div>
                <div>
                    CAT5 Cabling
                </div>
                <div>
                    <input type="text" value="" id="cablingSelfRating" name="selfRating[cablingSelfRating]" maxlength="1" size="1"/>
                </div>
            </div>

            <div class="formRow6">
                <div>
                    CCTV / Surveillance
                </div>
                <div>
                    <input type="text" value="" id="CCTVSelfRating" name="selfRating[CCTVSelfRating]" maxlength="1" size="1"/>
                </div>
                <div>
                    Construction
                </div>
                <div>
                    <input type="text" value="" id="constructionSelfRating" name="selfRating[constructionSelfRating]" maxlength="1" size="1"/>
                </div>
                <div>
                    PC - Desktop / Laptop    <!--13675-->
                </div>
                <div>
                    <input type="text" value="" id="desktopSelfRating" name="selfRating[desktopSelfRating]" maxlength="1" size="1"/>
                </div>
           </div>

            <div class="formRow6">
                <div>
                    Digital Signage/Flat Panel
                </div>
                <div>
                    <input type="text" value="" id="DigitalSignageSelfRating" name="selfRating[DigitalSignageSelfRating]" maxlength="1" size="1"/>
                </div>
                <div>
                    DSL / DMARC / T1              <!--13875-->
                </div>
                <div>
                    <input type="text" value="" id="DslSelfRating" name="selfRating[DslSelfRating]" maxlength="1" size="1"/>
                </div>
                <div>
                    Electrical Wiring
                </div>
                <div>
                    <input type="text" value="" id="electricalSelfRating" name="selfRating[electricalSelfRating]" maxlength="1" size="1"/>
                </div>
           </div>

            <div class="formRow6">
                <div>
                    Fiber Cabling
                </div>
                <div>
                    <input type="text" value="" id="FiberCablingSelfRating" name="selfRating[FiberCablingSelfRating]" maxlength="1" size="1"/>
                </div>
                <div>
                    General Wiring
                </div>
                <div>
                    <input type="text" value="" id="GeneralWiringSelfRating" name="selfRating[GeneralWiringSelfRating]" maxlength="1" size="1"/>
                </div>
                <div>
                    Home Networking
                </div>
                <div>
                    <input type="text" value="" id="networkingSelfRating" name="selfRating[networkingSelfRating]" maxlength="1" size="1"/>
                </div>
           </div>

            <div class="formRow6">
                <div>
                    Kiosk
                </div>
                <div>
                    <input type="text" value="" id="KioskSelfRating" name="selfRating[KioskSelfRating]" maxlength="1" size="1"/>
                </div>
                <div>
                    Low Voltage
                </div>
                <div>
                    <input type="text" value="" id="LowVoltageSelfRating" name="selfRating[LowVoltageSelfRating]" maxlength="1" size="1"/>
                </div>
                <div>
                    Point of Sale
                </div>
                <div>
                    <input type="text" value="" id="posSelfRating" name="selfRating[posSelfRating]" maxlength="1" size="1"/>
                </div>
           </div>

            <div class="formRow6">
                <div>
                    Cooperative
                </div>
                <div>
                    <input  size="1" maxlength="1" name="selfRating[Cooperative]" id="Cooperative" value="" type="text">
                </div>
                <div>
                    Professionalism
                </div>
                <div>
                    <input  size="1" maxlength="1" name="selfRating[Professionalism]" id="Professionalism" value="" type="text">
                </div>
                <div>
                    Reliability
                </div>
                <div>
                    <input  size="1" maxlength="1" name="selfRating[Reliability]" id="Reliability" value="" type="text">
                </div>
            </div>

            <div class="formRow6">
                <div>
                    Printers / Copiers
                </div>
                <div>
                    <input type="text" value="" id="PrintersSelfRating" name="selfRating[PrintersSelfRating]" maxlength="1" size="1"/>
                </div>
                <div>
                    RFID
                </div>
                <div>
                    <input type="text" value="" id="RFIDSelfRating" name="selfRating[RFIDSelfRating]" maxlength="1" size="1"/>
                </div>
                <div>
                    Routers / Switches
                </div>
                <div>
                    <input type="text" value="" id="routersSelfRating" name="selfRating[routersSelfRating]" maxlength="1" size="1"/>
                </div>
           </div>
           
           <div class="formRow6">
                <div>
                    Satellite Install
                </div>
                <div>
                    <input type="text" value="" id="SatelliteSelfRating" name="selfRating[SatelliteSelfRating]" maxlength="1" size="1"/>
                </div>
                <div>
                    Server Hardware
                </div>
                <div>
                    <input type="text" value="" id="ServersSelfRating" name="selfRating[ServersSelfRating]" maxlength="1" size="1"/>
                </div>
                <div>
                    Server Software
                </div>
                <div>
                    <input type="text" value="" id="ServerSoftwareSelfRating" name="selfRating[ServerSoftwareSelfRating]" maxlength="1" size="1"/>
                </div>
           </div>
           
           <div class="formRow6">
                <div>
                    Site Survey
                </div>
                <div>
                    <input type="text" value="" id="SiteSurveySelfRating" name="selfRating[SiteSurveySelfRating]" maxlength="1" size="1"/>
                </div>
                <div>
                    Telephony - Non-VoIP
                </div>
                <div>
                    <input type="text" value="" id="telephonySelfRating" name="selfRating[telephonySelfRating]" maxlength="1" size="1"/>
                </div>
                <div>
                    Telephony - VoIP
                </div>
                <div>
                    <input type="text" value="" id="TelephonyVoIPSelfRating" name="selfRating[TelephonyVoIPSelfRating]" maxlength="1" size="1"/>
                </div>
           </div>
           
           <div class="formRow6">
                <div>
                    Wireless Networking
                </div>
                <div>
                    <input type="text" value="" id="WirelessSelfRating" name="selfRating[WirelessSelfRating]" maxlength="1" size="1"/>
                </div>
            </div>
            
            <div class="formRow6">
                <div>
                    Overall Technical
                </div>
                <div>
                    <input type="text" value="" id="Technical" name="selfRating[Technical]" maxlength="1" size="1"/>
                </div>
                <div>
                </div>
                <div>
                </div>
                <div>
                </div>
                <div>
                </div>
            </div>

            <div class="verdana2bold">Ratings below are the technician's self rating of experience in different industries/environments</div>
            <div class="formRow4">
                <div>
                    Field Service Experience
                </div>
                <div>
                    <input type="text" value="" id="FSExperience" name="selfRating[FSExperience]" maxlength="1" size="1"/>
                </div>
                <div>
                    Residential
                </div>
                <div>
                    <input type="text" value="" id="ResidentialSelfRating" name="selfRating[ResidentialSelfRating]" maxlength="1" size="1"/>
                </div>
                <div>
                    Commercial
                </div>
                <div>
                    <input type="text" value="" id="CommercialSelfRating" name="selfRating[CommercialSelfRating]" maxlength="1" size="1"/>
                </div>
                <div>
                    Government
                </div>
                <div>
                    <input type="text" value="" id="GovernmentSelfRating" name="selfRating[GovernmentSelfRating]" maxlength="1" size="1"/>
                </div>

            </div>
            
<!--            <div class="verdana2"> Ratings below here are soft skill ratings. They are subjective but over time they will give a more accurate indication of the technician's ability in each category</div>
            <div class="formRow6">
                <div>
                    Aptitude
                </div>
                <div>
                    <input  size="1" maxlength="1" name="Aptitude" id="Aptitude" value="" type="text">
                </div>
                <div>
                    Follow Instructions
                </div>
                <div>
                    <input  size="1" maxlength="1" name="Instructions" id="Instructions" value="" type="text">
                </div>
                <div>
                    Attention to Details
                </div>
                <div>
                    <input  size="1" maxlength="1" name="Details" id="Details" value="" type="text">
                </div>
            </div>

            <div class="formRow6">
                <div>
                    PR Skills
                </div>
                <div>
                    <input  size="1" maxlength="1" name="PRSkills" id="PRSkills" value="" type="text">
                </div>
                <div>
                    Responsiveness
                </div>
                <div>
                    <input  size="1" maxlength="1" name="Responsiveness" id="Responsiveness" value="" type="text">
                </div>
                <div>
                    Eagerness
                </div>
                <div>
                    <input  size="1" maxlength="1" name="Eagerness" id="Eagerness" value="" type="text">
                </div>
            </div>

            <div class="formRow6">
                <div>
                    Communication
                </div>
                <div>
                    <input  size="1" maxlength="1" name="Communication" id="Communication" value="" type="text">
                </div>
                <div>
                    Attitude
                </div>
                <div>
                    <input  size="1" maxlength="1" name="Attitude" id="Attitude" value="" type="text">
                </div>
                <div>
                    Hygiene
                </div>
                <div>
                    <input  size="1" maxlength="1" name="Hygiene" id="Hygiene" value="" type="text">
                </div>
            </div>

            <div class="formRow6">
                <div>
                    Professional Appearance
                </div>
                <div>
                    <input  size="1" maxlength="1" name="Appearance" id="Appearance" value="" type="text">
                </div>
                <div>
                    Professional Dress
                </div>
                <div>
                    <input  size="1" maxlength="1" name="Dress" id="Dress" value="" type="text">
                </div>
                <div>
                    Availability
                </div>
                <div>
                    <input  size="1" maxlength="1" name="Availability" id="Availability" value="" type="text">
                </div>
            </div>

            <div class="formRow6">
                <div>
                    Travel
                </div>
                <div>
                    <input  size="1" maxlength="1" name="Travel" id="Travel" value="" type="text">
                </div>
                <div>
                    Flexibility
                </div>
                <div>
                    <input  size="1" maxlength="1" name="Flexibility" id="Flexibility" value="" type="text">
                </div>
                <div>
                    Pay Flexibility
                </div>
                <div>
                    <input  size="1" maxlength="1" name="PayFlexibility" id="PayFlexibility" value="" type="text">
                </div>
            </div>

            <div class="formRow6">
                <div>
                    Cooperative
                </div>
                <div>
                    <input  size="1" maxlength="1" name="Cooperative" id="Cooperative" value="" type="text">
                </div>
                <div>
                    Professionalism
                </div>
                <div>
                    <input  size="1" maxlength="1" name="Professionalism" id="Professionalism" value="" type="text">
                </div>
                <div>
                    Reliability
                </div>
                <div>
                    <input  size="1" maxlength="1" name="Reliability" id="Reliability" value="" type="text">
                </div>
            </div>

            <div class="formRow6">
                <div>
                    Dependability
                </div>
                <div>
                    <input  size="1" maxlength="1" name="Dependability" id="Dependability" value="" type="text">
                </div>
                <div>
                    Consistency
                </div>
                <div>
                    <input  size="1" maxlength="1" name="Consistency" id="Consistency" value="" type="text">
                </div>
                <div>
                </div>
                <div>
                </div>
            </div>-->
        </fieldset>

        <fieldset class="fieldGroup">
            <div class="verdana2bold">Search by Equipment:</div>
            <div class="formRow3">
                <div>
                    Laptop?
                </div>
                <div>
                </div>
                <div>
                    <input id="Laptop" name="Laptop" value="Y" type="radio">Yes<input id="Laptop" name="Laptop" value="N" type="radio">No<input id="Laptop" name="Laptop" value="" type="radio" checked>Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Digital Camera?
                </div>
                <div>
                </div>
                <div>
                    <input id="DigitalCam" name="DigitalCam" value="Y" type="radio">Yes<input id="DigitalCam" name="DigitalCam" value="N" type="radio">No<input id="DigitalCam" name="DigitalCam" value="" type="radio" checked>Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Cell Phone?
                </div>
                <div>
                </div>
                <div>
                    <input id="CellPhone" name="CellPhone" value="Y" type="radio">Yes<input id="CellPhone" name="CellPhone" value="N" type="radio">No<input id="CellPhone" name="CellPhone" value="" type="radio" checked>Any
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Ladder?
                </div>
                <div>
                </div>
                <div>
                    <input id="Ladder" name="Ladder" value="Y" type="radio">Yes<input id="Ladder" name="Ladder" value="N" type="radio">No<input id="Ladder" name="Ladder" value="" type="radio" checked>Any
                </div>
            </div>
        </fieldset><br>
        <fieldset>
            <div class="formRow3">
                <div>
                    Helping Hands?
                </div>
                <div>
                </div>
                <div>
                    <input id="HelpingHands" name="HelpingHands" value="Y" type="radio">Yes<input id="Ladder" name="HelpingHands" value="N" type="radio" checked>No<input id="HelpingHands" name="HelpingHands" value="" type="radio">Any
                </div>
            </div>
        
        </fieldset>
    </div>
    
</form>