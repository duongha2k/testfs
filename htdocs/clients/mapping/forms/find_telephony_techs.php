<?php
$companyID = '';

if (!empty($_POST['v'])) {
    $companyID = $_POST['v'];
}

$companyIdEscaped = mysql_escape_string($companyID);


function getSubmittedOrDefaultField($name, $default = "") {
		return (isset($_POST[$name]) ? $_POST[$name] : $default);
}


?>

<h2>Find Telephony Techs:</h2>
<br /><br />

<form id="searchForm" name="searchForm" onsubmit="mappingFormSubmit(); return false;" method="post">
  <div class="formBox">

  <fieldset class="fieldGroup">
      <div class="formRow6">
      <b>Select Arrow Color:</b>

<?php
$dd_disabled = false;
if (isset($_SESSION['usedColors'])) {
    if ($_SESSION['usedColors'] == Core_Map::getColorMarkers()) {
        $dd_disabled = true;
    }
}
if ($dd_disabled) {
?>
<select name="markerColor" id="markerColor" disabled>
</select>
<?php
} else {
?>	
<select name="markerColor" id="markerColor">
<?php
    $markerOptions = Core_Map::getColorMarkers();
    foreach ($markerOptions as $key=>$val) {
        //out if marker color not in list of used colors
        if (isset($_SESSION['usedColors'])) {
            if (!in_array($val, $_SESSION['usedColors'])) {
                echo '<option value="' . $key . '">' . $val . '</option>';
            }
        } else {
     	   echo '<option value="' . $key . '">' . $val . '</option>';
        }
    }
}
?>
</select>&nbsp;&nbsp;
  <input id="hideBanned" name="hideBanned" type="checkbox" checked />
    Hide Client Denied &nbsp;&nbsp;
    <input id="preferredOnly" name="preferredOnly" type="checkbox" />
    Show Client Preferred Only
  </div>
  <br />
  <div class="verdana2bold">Search by Distance:</div>
  <div class="formRow6">
  <div> ZIP Code </div>
  <div>
    <input name="ProximityZipCode" id="ProximityZipCode" size="8" value="<?php echo getSubmittedOrDefaultField("ProximityZipCode")?>" type="text" />
    <input name="ProximityLat" id="ProximityLat" type="hidden" value="<?php echo getSubmittedOrDefaultField("ProximityLat")?>" />
    <input name="ProximityLng" id="ProximityLng" type="hidden" value="<?php echo getSubmittedOrDefaultField("ProximityLng")?>" />
    <input name="formType" id="formType" type="hidden" value="find_telephony_techs" />
  </div>
  <div>
    <select id="ProximityDistance" name="ProximityDistance" >
      <?=$distanceHtml?>
    </select>
  </div>
  <div id="map_canvas" style="width: 100%; height: 300px; display: none"> </div>
  <div style="width:100%; clear:none;"></div>
    <div>
        <b>Run: </b><input id="doSearch" name="doSearch" value="" type="submit" />
    </div>
  </fieldset>
  <fieldset class="fieldGroup">
  <div class="verdana2bold">Filters:</div>
  <div class="verdana2bold">Search by Technician:</div>
  <div class="formRow3">
    <div> Tech ID<br/>
      <input  size="10" maxlength="10" name="TechID" id="TechID" value="<?php echo getSubmittedOrDefaultField("TechID")?>" type="text" />
    </div>
    <div> UserName<br/>
      <input  size="25" maxlength="255" name="UserName" id="UserName" value="<?php echo getSubmittedOrDefaultField("UserName")?>" type="text" />
    </div>
    <div> Primary Email<br/>
      <input  size="25" maxlength="255" name="PrimaryEmail" id="PrimaryEmail" value="<?php echo getSubmittedOrDefaultField("PrimaryEmail")?>" type="text"/>
    </div>
  </div>
  <div class="formRow3">
    <div> Last Name<br/>
      <input  size="25" maxlength="255" name="LastName" id="LastName" value="<?php echo getSubmittedOrDefaultField("LastName")?>" type="text" />
    </div>
    <div> FirstName<br/>
      <input  size="25" maxlength="255" name="FirstName" id="FirstName" value="<?php echo getSubmittedOrDefaultField("FirstName")?>" type="text" />
    </div>
    <div> <br/>
      <br/>
      <br/>
    </div>
  </div>
  <div class="formRow3">
    <div> City<br/>
      <input  size="25" maxlength="255" name="City" id="City" value="<?php echo getSubmittedOrDefaultField("City")?>" type="text" />
    </div>
    <div> State<br/>
      <select id="State" name="State" >
        <?=$statesHtml?>
      </select>
    </div>
    <div> Country<br/>
      <select id="Country" name="Country" >
        <?=$countriesHtml?>
      </select>
      &nbsp; </div>
  </div>
  <div class="formRow3">
    <div> Hourly Pay (&lt; or =)<br/>
      <input  size="10" maxlength="255" name="HourlyPay" id="HourlyPay" value="<?php echo getSubmittedOrDefaultField("HourlyPay")?>" type="text" />
    </div>
    <? if ($companyID == 'FLS') { ?>
				<div>
                	<table>
                    	<tr>
                        	<td>FLS ID</td><td><input name="FLS_Photo_ID" id="FLS_Photo_ID" value="1" type="checkbox" />Photo ID</td>
                        </tr>
                        <tr>
                        	<td><input  size="10" maxlength="10" name="FLSID" id="FLSID" value="" type="text" /> -or-&nbsp;&nbsp;</td><td><input name="FLSIDExists" id="FLSIDExists" value="1" type="checkbox" />All FLS Techs</td>
                        </tr>
					</table>
                </div>
                <div>
              		Warehouse ID<br/><input  size="10" maxlength="10" name="FLSWhse" id="FLSWhse" value="" type="text" />
				</div>                                
    <?}?>
  </div>
  </fieldset>
  <fieldset class="fieldGroup">
  <div class="verdana2bold">Search by Rankings:</div>
  <div class="formRow3">
    <div> FS Certification </div>
    <div> </div>
    <div>
      <input id="FS_Cert_Test_Pass" name="FS_Cert_Test_Pass" value="Y" <?php echo (getSubmittedOrDefaultField("FS_Cert_Test_Pass") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="FS_Cert_Test_Pass" name="FS_Cert_Test_Pass" value="N" <?php echo (getSubmittedOrDefaultField("FS_Cert_Test_Pass") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="FS_Cert_Test_Pass" name="FS_Cert_Test_Pass" value="" <?php echo (getSubmittedOrDefaultField("FS_Cert_Test_Pass") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any </div>
  </div>
            <div class="formRow3">
                <div>
                    # of Overall Tech Recommendation Greater or Equal to:
                </div>
                <div>
                </div>
                <div>
                    <input  size="3" maxlength="3" name="SATRecommendedTotal" id="SATRecommendedTotal" value="<?php echo getSubmittedOrDefaultField("SATRecommendedTotal")?>" type="text" />
                </div>
            </div>
            <div class="formRow3">
                <div>
                    # of Tech Event Performance Greater or Equal to:
                </div>
                <div>
                </div>
                <div>
                    <input  size="3" maxlength="3" name="SATPerformanceTotal" id="SATPerformanceTotal" value="<?php echo getSubmittedOrDefaultField("SATPerformanceTotal")?>" type="text" />
                </div>
            </div>
            <?php
				// Company Specific Criteria
				if ($companyID == "FLS") {
			?>


		    <div class="formRow3">
                 <div>
                    FLS CSP Form
                </div>
                <div>
                </div>
           		<div>
                	<input id="FLSCSP_Rec" name="FLSCSP_Rec" value="Y" type="radio">Yes<input id="FLSCSP_Rec" name="FLSCSP_Rec" value="N" type="radio">No<input id="FLSCSP_Rec" name="FLSCSP_Rec" value="" type="radio">Any
                </div>
            </div>
		    <div class="formRow3">
                 <div>
                    Cert Hallmark POS
                </div>
                <div>
                </div>
           		<div>
                	<input id="Cert_Hallmark_POS" name="Cert_Hallmark_POS" value="Y" type="radio">Yes<input id="Cert_Hallmark_POS" name="Cert_Hallmark_POS" value="N" type="radio">No<input id="Cert_Hallmark_POS" name="Cert_Hallmark_POS" value="" type="radio">Any
                </div>
            </div>
		    <div class="formRow3">
                 <div>
                    Cert Starbucks POS
                </div>
                <div>
                </div>
           		<div>
                	<input id="Starbucks_Cert" name="Starbucks_Cert" value="Y" type="radio">Yes<input id="Starbucks_Cert" name="Starbucks_Cert" value="N" type="radio">No<input id="Starbucks_Cert" name="Starbucks_Cert" value="" type="radio">Any
                </div>
            </div>
		    <div class="formRow3">
                 <div>
                    Cert Maurices POS
                </div>
                <div>
                </div>
           		<div>
                	<input id="Cert_Maurices_POS" name="Cert_Maurices_POS" value="Y" type="radio">Yes<input id="Cert_Maurices_POS" name="Cert_Maurices_POS" value="N" type="radio">No<input id="Cert_Maurices_POS" name="Cert_Maurices_POS" value="" type="radio">Any
                </div>
            </div>

            <?php
            }
			?>
  </fieldset>
  <fieldset class="fieldGroup">
  <div class="formRow3">
  	<br/>
    <div> Qty IMAC Calls (&gt;=) </div>
    <div>&nbsp;</div>
    <div>
      <input  size="25" maxlength="255" name="Qty_IMAC_Call" id="Qty_IMAC_Call" value="<?php echo getSubmittedOrDefaultField("Qty_IMAC_Call")?>" type="text">
    </div>
  </div>
  <div class="formRow3">
    <div> Qty Service Calls (&gt;=) </div>
    <div>&nbsp;</div>
    <div>
      <input  size="25" maxlength="255" name="Qty_FLS_Service_Call" id="Qty_FLS_Service_Call" value="<?php echo getSubmittedOrDefaultField("Qty_FLS_Service_Call")?>" type="text">
      &nbsp; </div>
  </div>
  <div class="formRow3">
    <div> Qty No Shows (&lt;=) </div>
    <div>&nbsp;</div>
    <div>
      <input  size="8" maxlength="255" name="No_Shows" id="No_Shows" value="<?php echo getSubmittedOrDefaultField("No_Shows")?>" type="text">
      &nbsp; </div>
  </div>
  <div class="verdana2bold"><b>Ratings:</b> Your search will find all techs with average rating values greater than or equal to the values you specifty in each category. Leave rating categories blank that you don't want to include in the search. <br>
    <br>
    Preference and Performance Ratings by Clients</div>
            <div class="formRow3">
                <div>
                    Overall Tech Recommendation (&gt;=)
                </div>
                <div>
                </div>
                <div>
                    <input  size="1" maxlength="1" name="SATRecommendedAvg" id="SATRecommendedAvg" value="" type="text"> 
                </div>
            </div>
            <div class="formRow3">
                <div>
                    Tech Event Performance (&gt;=)
                </div>
                <div>
                </div>
                <div>
                    <input  size="1" maxlength="1" name="SATPerformanceAvg" id="SATPerformanceAvg" value="" type="text">
                </div>
            </div>
  </fieldset>
  <fieldset class="fieldGroup">
  <div class="formRow3">
    <div> Experienced at installing OR maintaining (break-fix) telephony systems? </div>
    <div>&nbsp;</div>
    <div>
      <input id="InstallingTelephony" name="InstallingTelephony" value="Y" <?php echo (getSubmittedOrDefaultField("InstallingTelephony") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="InstallingTelephony" name="InstallingTelephony" value="N" <?php echo (getSubmittedOrDefaultField("InstallingTelephony") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="InstallingTelephony" name="InstallingTelephony" value="" <?php echo (getSubmittedOrDefaultField("InstallingTelephony") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any </div>
  </div>
  <div class="formRow3">
    <div> Installed or performed "break-fix" activity on a Server OR VOIP based telephony system? </div>
    <div>&nbsp;</div>
    <div>
      <input id="ServerTelephony" name="ServerTelephony" value="Y" <?php echo (getSubmittedOrDefaultField("ServerTelephony") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="ServerTelephony" name="ServerTelephony" value="N" <?php echo (getSubmittedOrDefaultField("ServerTelephony") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="ServerTelephony" name="ServerTelephony" value="" <?php echo (getSubmittedOrDefaultField("ServerTelephony") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any </div>
  </div>
  </fieldset>
  <fieldset class="fieldGroup">
  <div class="formRow3">
    <div class="verdana2bold">Search by Experience:</div>
  </div>


  <div class="formRow3">
    <div><b>Avaya</b></div>
     <div> <b>Either Experience or Training</b> </div>
    <div> <b>Within Last 12 Months?</b> </div>
  </div>
  <div class="formRow3">
    <div><i>Small Systems</i> </div>
    <div>&nbsp;</div>
    <div>&nbsp;</div>
  </div>
  <div class="formRow3">
    <div> &nbsp;&nbsp;Partner or Merlin? </div>
    <div>
      <input id="AvayaPartnerMerlin" name="AvayaPartnerMerlin" value="Y" <?php echo (getSubmittedOrDefaultField("AvayaPartnerMerlin") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="AvayaPartnerMerlin" name="AvayaPartnerMerlin" value="N" <?php echo (getSubmittedOrDefaultField("AvayaPartnerMerlin") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="AvayaPartnerMerlin" name="AvayaPartnerMerlin" value="" <?php echo (getSubmittedOrDefaultField("AvayaPartnerMerlin") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any </div>
    <div>
      <input id="AvayaPartnerMerlin6mo" name="AvayaPartnerMerlin6mo" value="Y" <?php echo (getSubmittedOrDefaultField("AvayaPartnerMerlin6mo") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="AvayaPartnerMerlin6mo" name="AvayaPartnerMerlin6mo" value="N" <?php echo (getSubmittedOrDefaultField("AvayaPartnerMerlin6mo") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="AvayaPartnerMerlin6mo" name="AvayaPartnerMerlin6mo" value="" <?php echo (getSubmittedOrDefaultField("AvayaPartnerMerlin6mo") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any</div>
  </div>
  <div class="formRow3">
    <div> &nbsp;&nbsp;IP Office? </div>
    <div>
      <input id="AvayaIPOffice" name="AvayaIPOffice" value="Y" <?php echo (getSubmittedOrDefaultField("AvayaIPOffice") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="AvayaIPOffice" name="AvayaIPOffice" value="N" <?php echo (getSubmittedOrDefaultField("AvayaIPOffice") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="AvayaIPOffice" name="AvayaIPOffice" value="" <?php echo (getSubmittedOrDefaultField("AvayaIPOffice") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any </div>
    <div>
      <input id="AvayaIPOffice6mo" name="AvayaIPOffice6mo" value="Y" <?php echo (getSubmittedOrDefaultField("AvayaIPOffice6mo") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="AvayaIPOffice6mo" name="AvayaIPOffice6mo" value="N" <?php echo (getSubmittedOrDefaultField("AvayaIPOffice6mo") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="AvayaIPOffice6mo" name="AvayaIPOffice6mo" value="" <?php echo (getSubmittedOrDefaultField("AvayaIPOffice6mo") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any</div>
  </div>
    <div class="formRow3">
    <div><i>PBX</i> </div>
    <div>&nbsp;</div>
    <div>&nbsp;</div>
  </div>
  <div class="formRow3">
    <div> &nbsp;&nbsp;Definity</div>
    <div>
      <input id="AvayaDefinity" name="AvayaDefinity" value="Y" <?php echo (getSubmittedOrDefaultField("AvayaDefinity") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="AvayaDefinity" name="AvayaDefinity" value="N" <?php echo (getSubmittedOrDefaultField("AvayaDefinity") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="AvayaDefinity" name="AvayaDefinity" value="" <?php echo (getSubmittedOrDefaultField("AvayaDefinity") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any </div>
    <div>
      <input id="AvayaDefinity6mo" name="AvayaDefinity6mo" value="Y" <?php echo (getSubmittedOrDefaultField("AvayaDefinity6mo") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="AvayaDefinity6mo" name="AvayaDefinity6mo" value="N" <?php echo (getSubmittedOrDefaultField("AvayaDefinity6mo") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="AvayaDefinity6mo" name="AvayaDefinity6mo" value="" <?php echo (getSubmittedOrDefaultField("AvayaDefinity6mo") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any</div>
  </div>
  <div class="formRow3">
    <div> &nbsp;&nbsp;8300/8500/8700? </div>
    <div>
      <input id="AvayaPBX8300etc" name="AvayaPBX8300etc" value="Y" <?php echo (getSubmittedOrDefaultField("AvayaPBX8300etc") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="AvayaPBX8300etc" name="AvayaPBX8300etc" value="N" <?php echo (getSubmittedOrDefaultField("AvayaPBX8300etc") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="AvayaPBX8300etc" name="AvayaPBX8300etc" value="" <?php echo (getSubmittedOrDefaultField("AvayaPBX8300etc") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any </div>
    <div>
      <input id="AvayaPBX8300etc6mo" name="AvayaPBX8300etc6mo" value="Y" <?php echo (getSubmittedOrDefaultField("AvayaPBX8300etc6mo") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="AvayaPBX8300etc6mo" name="AvayaPBX8300etc6mo" value="N" <?php echo (getSubmittedOrDefaultField("AvayaPBX8300etc6mo") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="AvayaPBX8300etc6mo" name="AvayaPBX8300etc6mo" value="" <?php echo (getSubmittedOrDefaultField("AvayaPBX8300etc6mo") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any</div>
  </div>
  <div class="formRow3">
    <div> &nbsp;&nbsp;Gateway 250/350? </div>
    <div>
      <input id="AvayaGateway" name="AvayaGateway" value="Y" <?php echo (getSubmittedOrDefaultField("AvayaGateway") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="AvayaGateway" name="AvayaGateway" value="N" <?php echo (getSubmittedOrDefaultField("AvayaGateway") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="AvayaGateway" name="AvayaGateway" value="" <?php echo (getSubmittedOrDefaultField("AvayaGateway") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any </div>
    <div>
      <input id="AvayaGateway6mo" name="AvayaGateway6mo" value="Y" <?php echo (getSubmittedOrDefaultField("AvayaGateway6mo") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="AvayaGateway6mo" name="AvayaGateway6mo" value="N" <?php echo (getSubmittedOrDefaultField("AvayaGateway6mo") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="AvayaGateway6mo" name="AvayaGateway6mo" value="" <?php echo (getSubmittedOrDefaultField("AvayaGateway6mo") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any</div>
  </div>

      <div class="formRow3">&nbsp;</div>
  <div class="formRow3">
    <div><b>Nortel</b></div>
    <div>&nbsp;</div>
  </div>
  <div class="formRow3">
    <div><i>Small Systems</i> </div>
    <div>&nbsp;</div>
    <div>&nbsp;</div>
  </div>
  <div class="formRow3">
    <div> &nbsp;&nbsp;Northstar? </div>
    <div>
      <input id="NortelNorstar" name="NortelNorstar" value="Y" <?php echo (getSubmittedOrDefaultField("NortelNorstar") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="NortelNorstar" name="NortelNorstar" value="N" <?php echo (getSubmittedOrDefaultField("NortelNorstar") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="NortelNorstar" name="NortelNorstar" value="" <?php echo (getSubmittedOrDefaultField("NortelNorstar") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any </div>
    <div>
      <input id="NortelNorstar6mo" name="NortelNorstar6mo" value="Y" <?php echo (getSubmittedOrDefaultField("NortelNorstar6mo") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="NortelNorstar6mo" name="NortelNorstar6mo" value="N" <?php echo (getSubmittedOrDefaultField("NortelNorstar6mo") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="NortelNorstar6mo" name="NortelNorstar6mo" value="" <?php echo (getSubmittedOrDefaultField("NortelNorstar6mo") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any</div>
  </div>
  <div class="formRow3">
    <div> &nbsp;&nbsp;BMS? </div>
    <div>
      <input id="NortelBMS" name="NortelBMS" value="Y" <?php echo (getSubmittedOrDefaultField("NortelBMS") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="NortelBMS" name="NortelBMS" value="N" <?php echo (getSubmittedOrDefaultField("NortelBMS") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="NortelBMS" name="NortelBMS" value="" <?php echo (getSubmittedOrDefaultField("NortelBMS") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any </div>
    <div>
      <input id="NortelBMS6mo" name="NortelBMS6mo" value="Y" <?php echo (getSubmittedOrDefaultField("NortelBMS6mo") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="NortelBMS6mo" name="NortelBMS6mo" value="N" <?php echo (getSubmittedOrDefaultField("NortelBMS6mo") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="NortelBMS6mo" name="NortelBMS6mo" value="" <?php echo (getSubmittedOrDefaultField("NortelBMS6mo") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any</div>
  </div>
    <div class="formRow3">
    <div><i>PBX</i> </div>
    <div>&nbsp;</div>
    <div>&nbsp;</div>
  </div>
  <div class="formRow3">
    <div> &nbsp;&nbsp;Meridian</div>
    <div>
      <input id="NortelMeridian" name="NortelMeridian" value="Y" <?php echo (getSubmittedOrDefaultField("NortelMeridian") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="NortelMeridian" name="NortelMeridian" value="N" <?php echo (getSubmittedOrDefaultField("NortelMeridian") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="NortelMeridian" name="NortelMeridian" value="" <?php echo (getSubmittedOrDefaultField("NortelMeridian") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any </div>
    <div>
      <input id="NortelMeridian6mo" name="NortelMeridian6mo" value="Y" <?php echo (getSubmittedOrDefaultField("NortelMeridian6mo") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="NortelMeridian6mo" name="NortelMeridian6mo" value="N" <?php echo (getSubmittedOrDefaultField("NortelMeridian6mo") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="NortelMeridian6mo" name="NortelMeridian6mo" value="" <?php echo (getSubmittedOrDefaultField("NortelMeridian6mo") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any</div>
  </div>
  <div class="formRow3">
    <div> &nbsp;&nbsp;CS 1000/1500? </div>
    <div>
      <input id="NortelCS" name="NortelCS" value="Y" <?php echo (getSubmittedOrDefaultField("NortelCS") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="NortelCS" name="NortelCS" value="N" <?php echo (getSubmittedOrDefaultField("NortelCS") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="NortelCS" name="NortelCS" value="" <?php echo (getSubmittedOrDefaultField("NortelCS") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any </div>
    <div>
      <input id="NortelCS6mo" name="NortelCS6mo" value="Y" <?php echo (getSubmittedOrDefaultField("NortelCS6mo") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="NortelCS6mo" name="NortelCS6mo" value="N" <?php echo (getSubmittedOrDefaultField("NortelCS6mo") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="NortelCS6mo" name="NortelCS6mo" value="" <?php echo (getSubmittedOrDefaultField("NortelCS6mo") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any</div>
  </div>

    <div class="formRow3">&nbsp;</div>
    <div class="formRow3">
    <div><b>Cisco</b></div>
    <div>&nbsp;</div>
  </div>
  <div class="formRow3">
    <div> &nbsp;&nbsp;Call Manager? </div>
    <div>
      <input id="CiscoCM" name="CiscoCM" value="Y" <?php echo (getSubmittedOrDefaultField("CiscoCM") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="CiscoCM" name="CiscoCM" value="N" <?php echo (getSubmittedOrDefaultField("CiscoCM") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="CiscoCM" name="CiscoCM" value="" <?php echo (getSubmittedOrDefaultField("CiscoCM") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any </div>
    <div>
      <input id="CiscoCM6mo" name="CiscoCM6mo" value="Y" <?php echo (getSubmittedOrDefaultField("CiscoCM6mo") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="CiscoCM6mo" name="CiscoCM6mo" value="N" <?php echo (getSubmittedOrDefaultField("CiscoCM6mo") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="CiscoCM6mo" name="CiscoCM6mo" value="" <?php echo (getSubmittedOrDefaultField("CiscoCM6mo") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any</div>
  </div>

    <div class="formRow3">&nbsp;</div>
      <div class="formRow3">
    <div><b>Siemens</b></div>
    <div>&nbsp;</div>
  </div>
  <div class="formRow3">
    <div> &nbsp;&nbsp;9751? </div>
    <div>
      <input id="Siemens9751" name="Siemens9751" value="Y" <?php echo (getSubmittedOrDefaultField("Siemens9751") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="Siemens9751" name="Siemens9751" value="N" <?php echo (getSubmittedOrDefaultField("Siemens9751") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="Siemens9751" name="Siemens9751" value="" <?php echo (getSubmittedOrDefaultField("Siemens9751") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any </div>
    <div>
      <input id="Siemens97516mo" name="Siemens97516mo" value="Y" <?php echo (getSubmittedOrDefaultField("Siemens97516mo") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="Siemens97516mo" name="Siemens97516mo" value="N" <?php echo (getSubmittedOrDefaultField("Siemens97516mo") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="Siemens97516mo" name="Siemens97516mo" value="" <?php echo (getSubmittedOrDefaultField("Siemens97516mo") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any</div>
  </div>
  <div class="formRow3">
    <div> &nbsp;&nbsp;Hicom? </div>
    <div>
      <input id="SiemensHicom" name="SiemensHicom" value="Y" <?php echo (getSubmittedOrDefaultField("SiemensHicom") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="SiemensHicom" name="SiemensHicom" value="N" <?php echo (getSubmittedOrDefaultField("SiemensHicom") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="SiemensHicom" name="SiemensHicom" value="" <?php echo (getSubmittedOrDefaultField("SiemensHicom") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any </div>
    <div>
      <input id="SiemensHicom6mo" name="SiemensHicom6mo" value="Y" <?php echo (getSubmittedOrDefaultField("SiemensHicom6mo") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="SiemensHicom6mo" name="SiemensHicom6mo" value="N" <?php echo (getSubmittedOrDefaultField("SiemensHicom6mo") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="SiemensHicom6mo" name="SiemensHicom6mo" value="" <?php echo (getSubmittedOrDefaultField("SiemensHicom6mo") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any</div>
  </div>
  <div class="formRow3">
    <div> &nbsp;&nbsp;Hipath? </div>
    <div>
      <input id="SiemensHipath" name="SiemensHipath" value="Y" <?php echo (getSubmittedOrDefaultField("SiemensHipath") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="SiemensHipath" name="SiemensHipath" value="N" <?php echo (getSubmittedOrDefaultField("SiemensHipath") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="SiemensHipath" name="SiemensHipath" value="" <?php echo (getSubmittedOrDefaultField("SiemensHipath") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any </div>
    <div>
      <input id="SiemensHipath6mo" name="SiemensHipath6mo" value="Y" <?php echo (getSubmittedOrDefaultField("SiemensHipath6mo") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="SiemensHipath6mo" name="SiemensHipath6mo" value="N" <?php echo (getSubmittedOrDefaultField("SiemensHipath6mo") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="SiemensHipath6mo" name="SiemensHipath6mo" value="" <?php echo (getSubmittedOrDefaultField("SiemensHipath6mo") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any</div>
  </div>

    <div class="formRow3">&nbsp;</div>
    <div class="formRow3">
        <div><b>NEC</b></div>
        <div>
            <input id="NEC" name="NEC" value="Y" <?php echo (getSubmittedOrDefaultField("NEC") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
              Yes
              <input id="NEC" name="NEC" value="N" <?php echo (getSubmittedOrDefaultField("NEC") == "N" ? "checked=\"checked\"" : "")?> type="radio">
             No
              <input id="NEC" name="NEC" value="" <?php echo (getSubmittedOrDefaultField("NEC") == "" ? "checked=\"checked\"" : "")?> type="radio">
              Any</div>
        <div>
              <input id="NEC6mo" name="NEC6mo" value="Y" <?php echo (getSubmittedOrDefaultField("NEC6mo") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
              Yes
              <input id="NEC6mo" name="NEC6mo" value="N" <?php echo (getSubmittedOrDefaultField("NEC6mo") == "N" ? "checked=\"checked\"" : "")?> type="radio">
              No
              <input id="NEC6mo" name="NEC6mo" value="" <?php echo (getSubmittedOrDefaultField("NEC6mo") == "" ? "checked=\"checked\"" : "")?> type="radio">
              Any</div>
      </div>

    <div class="formRow3">&nbsp;</div>
    <div class="formRow3">
        <div><b>InterTel</b></div>
        <div>
            <input id="InterTel" name="InterTel" value="Y" <?php echo (getSubmittedOrDefaultField("InterTel") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
              Yes
              <input id="InterTel" name="InterTel" value="N" <?php echo (getSubmittedOrDefaultField("InterTel") == "N" ? "checked=\"checked\"" : "")?> type="radio">
             No
              <input id="InterTel" name="InterTel" value="" <?php echo (getSubmittedOrDefaultField("InterTel") == "" ? "checked=\"checked\"" : "")?> type="radio">
              Any</div>
        <div>
              <input id="InterTel6mo" name="InterTel6mo" value="Y" <?php echo (getSubmittedOrDefaultField("InterTel6mo") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
              Yes
              <input id="InterTel6mo" name="InterTel6mo" value="N" <?php echo (getSubmittedOrDefaultField("InterTel6mo") == "N" ? "checked=\"checked\"" : "")?> type="radio">
              No
              <input id="InterTel6mo" name="InterTel6mo" value="" <?php echo (getSubmittedOrDefaultField("InterTel6mo") == "" ? "checked=\"checked\"" : "")?> type="radio">
              Any</div>
      </div>
    <div class="formRow3">&nbsp;</div>
    <div class="formRow3">
        <div><b>Mitel</b></div>
        <div>
            <input id="Mitel" name="Mitel" value="Y" <?php echo (getSubmittedOrDefaultField("Mitel") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
              Yes
              <input id="Mitel" name="Mitel" value="N" <?php echo (getSubmittedOrDefaultField("Mitel") == "N" ? "checked=\"checked\"" : "")?> type="radio">
             No
              <input id="Mitel" name="Mitel" value="" <?php echo (getSubmittedOrDefaultField("Mitel") == "" ? "checked=\"checked\"" : "")?> type="radio">
              Any</div>
        <div>
              <input id="Mitel6mo" name="Mitel6mo" value="Y" <?php echo (getSubmittedOrDefaultField("Mitel6mo") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
              Yes
              <input id="Mitel6mo" name="Mitel6mo" value="N" <?php echo (getSubmittedOrDefaultField("Mitel6mo") == "N" ? "checked=\"checked\"" : "")?> type="radio">
              No
              <input id="Mitel6mo" name="Mitel6mo" value="" <?php echo (getSubmittedOrDefaultField("Mitel6mo") == "" ? "checked=\"checked\"" : "")?> type="radio">
              Any</div>
      </div>
   </fieldset>


  <fieldset>
  <div class="formRow3">
    <div class="verdana2bold">Search by tools:</div>
  </div>

    <div class="formRow3">
    <div> Laptop (Microsoft XP or newer? </div>
    <div>
      <input id="Laptop" name="Laptop" value="Y" <?php echo (getSubmittedOrDefaultField("Laptop") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="Laptop" name="Laptop" value="N" <?php echo (getSubmittedOrDefaultField("Laptop") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="Laptop" name="Laptop" value="" <?php echo (getSubmittedOrDefaultField("Laptop") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any </div>
     <div>&nbsp;</div>
 </div>
    <div class="formRow3">
    <div> Set of screw drivers? </div>
    <div>
      <input id="Screwdrivers" name="Screwdrivers" value="Y" <?php echo (getSubmittedOrDefaultField("Screwdrivers") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="Screwdrivers" name="Screwdrivers" value="N" <?php echo (getSubmittedOrDefaultField("Screwdrivers") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="Screwdrivers" name="Screwdrivers" value="" <?php echo (getSubmittedOrDefaultField("Screwdrivers") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any </div>
    <div>&nbsp;</div>
  </div>
    <div class="formRow3">
    <div> Punch down tool? </div>
    <div>
      <input id="PunchTool" name="PunchTool" value="Y" <?php echo (getSubmittedOrDefaultField("PunchTool") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="PunchTool" name="PunchTool" value="N" <?php echo (getSubmittedOrDefaultField("PunchTool") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="PunchTool" name="PunchTool" value="" <?php echo (getSubmittedOrDefaultField("PunchTool") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any </div>
     <div>&nbsp;</div>
 </div>
    <div class="formRow3">
    <div> Tone Generator? </div>
    <div>
      <input id="ToneGeneratorAndWand" name="ToneGeneratorAndWand" value="Y" <?php echo (getSubmittedOrDefaultField("ToneGeneratorAndWand") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="ToneGeneratorAndWand" name="ToneGeneratorAndWand" value="N" <?php echo (getSubmittedOrDefaultField("ToneGeneratorAndWand") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="ToneGeneratorAndWand" name="ToneGeneratorAndWand" value="" <?php echo (getSubmittedOrDefaultField("ToneGeneratorAndWand") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any </div>
    <div>&nbsp;</div>
  </div>
    <div class="formRow3">
    <div> Butt Set? </div>
    <div>
      <input id="ButtSet" name="ButtSet" value="Y" <?php echo (getSubmittedOrDefaultField("ButtSet") == "Y" ? "checked=\"checked\"" : "")?> type="radio">
      Yes
      <input id="ButtSet" name="ButtSet" value="N" <?php echo (getSubmittedOrDefaultField("ButtSet") == "N" ? "checked=\"checked\"" : "")?> type="radio">
      No
      <input id="ButtSet" name="ButtSet" value="" <?php echo (getSubmittedOrDefaultField("ButtSet") == "" ? "checked=\"checked\"" : "")?> type="radio">
      Any </div>
     <div>&nbsp;</div>
 </div>
  </fieldset>
  </div>
</form>