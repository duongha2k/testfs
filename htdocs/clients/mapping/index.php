<?php
/**
 * @author Sergey Petkevich
 * @author Alex Che
 */
$page = 'clients';
$option = 'fsmapper';

require_once("common.php");
require("../headerStartSession.php");
require_once("../library/Map/Control.php");

if ($_SESSION['loggedIn'] != "yes" || $_SESSION['loggedInAs'] != "client") {
    header("Location: ../");
}

require("../header.php");

    if($isFLSManager){
        $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
    } else {
        $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
    }

require("../navBar.php");
require("includes/adminCheck.php");
require_once("includes/PMCheck.php");

$companyID = isset($_GET["v"]) ? $_GET["v"] : '';

if (!empty($_SESSION['usedColors'])) {
	unset($_SESSION['usedColors']);
}

$searchPopUp = isset($_GET['search']) ? $_GET['search'] : '';

$cfgSite = Zend_Registry::get('CFG_SITE');

$googleKey = $cfgSite->get("site")->get("google_map_key");
$googleClientId = $cfgSite->get("site")->get("google_map_client_id");

?>

<script src="https://maps-api-ssl.google.com/maps?file=api&amp;v=2&amp;client=<?=$googleClientId?>&amp;sensor=false"
            type="text/javascript"></script>
<script src="js/google.js" type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>
<script src="js/colorLayer.js" type="text/javascript"></script>
<script src="js/ClusterMarker.js" type="text/javascript"></script>
<script src="js/HtmlControl.js" type="text/javascript"></script>
<script src="js/idsManipulation.js" type="text/javascript"></script>
<script src="js/caspioCall.js" type="text/javascript"></script>

<script type="text/javascript" src="js/yahoo-dom-event.js"></script>
<script type="text/javascript" src="js/dragdrop-min.js"></script>
<script type="text/javascript" src="js/container-min.js"></script>
<script type="text/javascript" src="js/containerariaplugin.js"></script>

<script type="text/javascript" src="../../library/jquery/calendar/jquery-calendar.js"></script>

<link rel="stylesheet" type="text/css" href="../library/ratingSourceBreakdown.css" />
<script type="text/javascript" src="../library/ratingSourceBreakdown.js"></script>
<link rel="stylesheet" href="../../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<link rel="stylesheet" href="css/main.css" type="text/css" />
<link rel="stylesheet" href="css/container.css" type="text/css" />
<link rel="stylesheet" href="css/fonts-min.css" type="text/css" />
<script type="text/javascript">
var globalInitCounter = 0;
$(document).ready(function(){
        globalInit();
  });

function globalInit()
{
	if (typeof window.GMap2 == 'function') {
    	colorLayer = new ColorLayerManager('colorManagerDiv', 'colorManagerTitleDiv');
        initializeMapTechs();
        initializeMap();
        initPoligon();
    
        window.onunload = function() {
                            GUnload();
                        }
    } else {
    	globalInitCounter ++;
    	if (globalInitCounter < 3) setTimeout("globalInit()", 1000);
        else alert('Please allow displaying unsecure content to make mapping service be availble for you.');
    }
}

var oPanel2 = '';
(function () {

	var Event = YAHOO.util.Event,
		Dom = YAHOO.util.Dom;

	Event.onDOMReady(
        function () {
		document.body.setAttribute('class','yui-skin-sam');
		oPanel2 = new YAHOO.widget.Dialog("panel-2", {
			//modal: true,
			modal: false,
			fixedcenter: false,
			constraintoviewport: true,
			width: "1000px",
			postmethod: "form",
			<? 
			if (empty($searchPopUp)) {
			    echo "visible: false";
			} else { 
			    echo "visible: true";
			}
			?>
		});

		oPanel2.render(document.body);
		Event.on("find_techs_link", "click", oPanel2.show, null, oPanel2);
		Event.on("find_cabling_techs_link", "click", oPanel2.show, null, oPanel2);
		Event.on("find_telephony_techs_link", "click", oPanel2.show, null, oPanel2);
		Event.on("wo_link", "click", oPanel2.show, null, oPanel2);
		Event.on("lassoData", "click", oPanel2.show, null, oPanel2);
		Event.on("lassoAssign", "click", oPanel2.show, null, oPanel2);

		var handleOK = function() {
			this.cancel();
		};
	});
}());
<?
if (!empty($searchPopUp)) {
	switch ($searchPopUp) {
	   case 'techs':
	       echo "getMappingForm('find_techs', 'suss', 'panel_mask');";
	       break;
	   case 'cabling':
	       echo "getMappingForm('find_cabling_techs', 'suss', 'panel_mask');";
	       break;
	   case 'telephony':
	       echo "getMappingForm('find_telephony_techs', 'suss', 'panel_mask');";
	       break;
	   case 'wos':
	       echo "getMappingFormWo('wo', 'suss', 'panel_mask');";
	       break;
}}
?>
</script>

<div id="panel-2">
	<div class="bd" id="loadingDiv">
		<form>
			<img src="../../images/map/loading.gif" /><input type="text" onfocus="this.blur()" id="loading_percent" readonly style="border:0px solid #FFF; background-color: #FFF;" />
		</form>
	</div>
	<div class="bd" id="panel_mask">

	</div>
</div>


<br />
<table border="0" cellspacing="2" cellpadding="0">
  <tr>
    <td width="200" valign="top">
    <div style="width:200px;">
        <a href="#" id="find_techs_link" onclick="getMappingForm('find_techs', '<?php echo $companyID; ?>', 'panel_mask'); return false;">Find Techs</a><br />
        <a href="#" id="find_cabling_techs_link" onclick="getMappingForm('find_cabling_techs', '<?php echo $companyID; ?>', 'panel_mask'); return false;">Find Cabling Techs</a><br />
        <a href="#" id="find_telephony_techs_link" onclick="getMappingForm('find_telephony_techs', '<?php echo $companyID; ?>', 'panel_mask'); return false;">Find Telephony Techs</a><br />
        <a href="#" id="wo_link" onclick="getMappingFormWo('wo', '<?php echo $companyID; ?>', 'panel_mask'); return false;">Find Work Orders</a><br />

        <br /><br />

        <div id="lassoToggleDiv" style="display:inline;"><a href="#" onclick="lassoToggle(); clearLasso(); return false;">Enable FS-Mapper</a></div><br />
        <div id="lassoDrawDiv" style="display:none;"><a href="#" onclick="drawLasso(); return false;">Highlight New FS-Mapper Area</a></div><br /><br />
        <a href="#" id="lassoData" onclick="showSelectedMarkersData('<?php echo $companyID; ?>', false); return false;" style="display:none;">Show Data within FS-Mapper Area</a><br />
        <a href="#" id="lassoEmail" onclick="collectSelectedMarkers(); return false;" style="display:none;">Email Blast FS-Mapper Area Techs</a><br />
        <a href="#" id="lassoAssign" onclick="showSelectedMarkersData('<?php echo $companyID; ?>', true); return false;" style="display:none;">Assign FS-Mapper Area Techs</a><br />
        <a href="#" id="lassoRemove" onclick="clearLasso(); return false;" style="display:none;">Remove FS-Mapper Area</a>

        <br /><br />

        <div id="overlayToggle" style="display:inline;"></div><br /><br />
        <div id="colorManagerTitleDiv" style="display:none; font-weight: bold;">Overlays:</div><br />
        <div id="colorManagerDiv"></div><br/>
        <div id="delAllColors" style="display:none;">All [<a href="#" onclick="colorLayer.delAllMarkers(); return false;">remove</a>]<br/></div>

        </div>
    </td>
    <td valign="top">
        <div id="formDiv" style="position:absolute;"></div>
        <div id="errorDiv"></div><div id="msgDiv"></div>
        <div id="mapLayout" align="center">
            <div id="gmap" style="width:700px; height:500px; float:left;"></div>
        </div>
    </td>
    <td valign="top" style="width:250px;border:1px solid #000; padding:5px; font-size:11px;" width="250px">
        <div style="width:250px;">
        <span><b>Marker Legend</b></span><div class='mapperServiceButtotn' style="float:right; position:relative; top: -5px"><a href='/content/FS-Mapper User Guide and FAQs.pdf' target='_blank'>Help/FAQ's</a></div><br/><br/>
        <span><b>Techs:</b><br /><br />
        <img src="../../images/map/tech_marker_blue_one.png" width="19" height="30" />&nbsp; One tech at this location<br />
        <img src="../../images/map/tech_marker_blue_multi.png" width="19" height="30" />&nbsp; Multiple techs at this location<br />
        <img src="../../images/map/tech_cluster_blue.png" width="19" height="30" />&nbsp; Zoom in to see more detail<br />
        <br /></span>
        <span><b>Work Orders:</b><br /><br />
        <img src="../../images/map/wo_marker_blue_one.png" width="19" height="30" />&nbsp; One work order at this location<br />
        <img src="../../images/map/wo_marker_blue_multi.png" width="19" height="30" />&nbsp; Multiple work orders at this location<br />
        <img src="../../images/map/wo_cluster_blue.png" width="19" height="30" />&nbsp; Zoom in to see more detail<br />
        </div></span>
    </td>
  </tr>
</table>
<div id="map_canvas" style="width: 1px; height: 1px; visibility:hidden; position:absolute;"></div>

<div style="display:none;">
<form name="goToEmail" id="goToEmail" action="../../Email_Form_Client.php" method="post" target="_blank">
<input type="hidden" name="emailList" id="emailList" value="" />
<input type="hidden" name="vSubmit" id="vSubmit" value="1" />
</form>

</div>
<?php
require_once("../footer.php");
?>