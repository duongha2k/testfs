<?php
/**
 * @author Sergey Petkevich
 */

require_once("common.php");

require_once realpath(dirname(__FILE__) . '/../../') . '/headerStartSession.php';
require_once realpath(dirname(__FILE__) . '/../../') . '/includes/_init.service.php';

function getStatesOptions()
{
    $client = new Core_Api_Class();
    $statesOptions = $client->getStates();
    $statesHtml = "<option value=\"\">All States</option>";
    foreach ($statesOptions->data as $abbr => $state) {
        $statesHtml .= "<option value=\"$abbr\">$state</option>";
    }
    return $statesHtml;
}


function getCountriesOptions()
{
    $client = new Core_Api_Class();
    $countryOptions = $client->getCountries($_SESSION['UserName'], $_SESSION['Password']);
    $countriesHtml = "<option value=\"\">Show All</option>";
    foreach ($countryOptions->data as $item) {
        $countriesHtml .= "<option value=\"{$item->Code}\">{$item->Name}</option>";
    }
    return $countriesHtml;
}


function getDistancesOptions()
{
    $client = new Core_Api_Class();
    $distanceOptions = $client->getDistances();
    $distanceHtml = "<option value=\"1\">Select Distance</option>";
    foreach ($distanceOptions->data as $abbr => $name) {
        if ($abbr == 50)
        	$distanceHtml .= "<option value=\"$abbr\" selected>$name</option>";
        else
            $distanceHtml .= "<option value=\"$abbr\">$name</option>";
    }
    return $distanceHtml;
}


function getProjectsOptions()
{
    $client = new Core_Api_Class();
    $projectOptions = $client->getAllProjects($_SESSION['UserName']. '|pmContext='.$_REQUEST['v'], $_SESSION['Password'] );
    
    $projectHtml = "<option value=\"0\">Select Project</option>";
    foreach ($projectOptions->data as $item) {
        if ($item->Active) {
            $projectHtml .= "<option value=\"{$item->Project_ID}\">{$item->Project_Name}</option>";
        }
    }
    return $projectHtml;
}

$statesHtml = getStatesOptions();

$countriesHtml = getCountriesOptions();

$distanceHtml = getDistancesOptions();

//require_once("../library/caspioAPI.php");

if (!empty($_POST['formType'])) {
    switch ($_POST['formType']) {
      case 'find_techs':
        include('forms/find_techs.php');
        break;

      case 'find_cabling_techs':
        include('forms/find_cabling_techs.php');
        break;

      case 'find_telephony_techs':
        include('forms/find_telephony_techs.php');
        break;

      case 'wo':
        include('forms/work_orders.php');
        break;
        
      case 'lasso_data':
        include('forms/lasso_data.php');
        break;

      case 'lasso_assign_data':
        include('forms/assign_data.php');
        break;

      default:
        // Do nothing
        break;
    }
}
?>