<?php
/**
 * @author Sergey Petkevich
 */
require_once("common.php");
require("../headerStartSession.php");

if (!empty($_POST['formType'])) {
    switch ($_POST['formType']) {
      case 'find_techs':
        include('display/find_techs.php');
        break;

      case 'find_cabling_techs':
        include('display/find_cabling_techs.php');
        break;

      case 'find_telephony_techs':
        include('display/find_telephony_techs.php');
        break;

      case 'wo':
        include('display/work_orders.php');
        break;

      default:
        // Do nothing
        break;
    }
    
}

if (!empty($_POST['color'])) {
	if (isset($_SESSION['usedColors'][$color])) {
	    	unset($_SESSION['usedColors'][$color]);
	    }
    //Map::delUsedColorFromSession($_POST['color']);
    exit();
}
?>