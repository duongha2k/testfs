<?php $page = "clients"; ?>
<?php $option = "reports"; ?>
<?php $selected = 'FTXSReport'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php require ("includes/adminCheck.php"); ?>
<?php
	$displayPMTools = true;
	require_once("includes/PMCheck.php");
?>
<?php
?>

<!-- Add Content Here -->
<br /><br />

<div align="center">
<br /><br />
<table width="400" border="1" cellspacing="5" cellpadding="0">
  <tr>
    <th nowrap="nowrap" scope="col">&nbsp;</th>
    <!--th nowrap="nowrap" scope="col">Work Orders</th-->
    <th nowrap="nowrap" scope="col">Techs</th>
    <th nowrap="nowrap" scope="col">Tests</th>
  </tr>
  <tr>
    <td nowrap="nowrap"><!--<div align="center"><a href="workorderfinancials.php">Work Order Financials</a></div> --></td>
    <td nowrap="nowrap"><div align="center"><a href="trainedtechsnobadge.php">Trained Techs w/o Badges</a></div></td>
    <td nowrap="nowrap"><div align="center"><a href="techtestresults.php">View Techs Test Results</a></div></td>
  </tr>
  <tr>
    <td nowrap="nowrap"><!--<div align="center"><a href="workOrdersOOS.php">Out of Scope Work Orders</a></div> --></td>
    <td nowrap="nowrap"><div align="center"><a href="No_IMACs.php">New Tech Assigned to Project Work</a></div></td>
    <td nowrap="nowrap"><div align="center"><a href="analyzeTestResults.php">Analyze FLS Test Results</a></div></td>
  </tr>
  <tr>
    <td nowrap="nowrap"><!--<div align="center"><a href="approveworkorders.php">Work Orders: Tech Complete, Not Approved</a></div> --></td>
    <td nowrap="nowrap"><div align="center"><a href="TechTrainedRegistered.php">Techs Trained and Registered</a></div></td>
    <td nowrap="nowrap"><div align="center"><a href="Hallmark_TestResults.php">Hallmark POS Test Results</a></div></td>
  </tr>
  <tr>
    <td nowrap="nowrap"><!--<div align="center"><a href="openWorkOrders.php">Open Work Order Report</a></div> --></td>
    <td nowrap="nowrap"><div align="center"><a href="techmapexport.php">Tech Map Export</a></div></td>
    <td nowrap="nowrap"><div align="center"><a href="Maurices_TestResults.php">Maurices Test Results</a></div></td>
  </tr>
  <tr>
    <td nowrap="nowrap"><!--<div align="center"><a href="confirmReport.php">Tech Review and Confirm</a></div> --></td>
    <td nowrap="nowrap"><div align="center"></div></td>
    <td nowrap="nowrap"><div align="center"></div></td>
  </tr>
  <tr>
    <td nowrap="nowrap"><!--<div align="center"><a href="workordersourced.php">Sourced Work Order Report</a></div> --></td>
    <td nowrap="nowrap"><div align="center"></div></td>
    <td nowrap="nowrap"><div align="center"></div></td>
  </tr>
  <tr>
    <td><div align="center"></div></td>
    <td><div align="center"></div></td>
    <td><div align="center"></div></td>
  </tr>
  <tr>
    <td><div align="center"></div></td>
    <td><div align="center"></div></td>
    <td><div align="center"></div></td>
  </tr>
  <tr>
    <td><div align="center"></div></td>
    <td><div align="center"></div></td>
    <td><div align="center"></div></td>
  </tr>
</table>



</div>


<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->