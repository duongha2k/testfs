<?php
$page = 'clients';
$option = 'wos';
$selected = 'FindWIN';

$_GET['v'] = ( isset($_GET['v']) ) ? $_GET['v'] : NULL;

ob_start();
require ("../header.php");
require ("../library/bootstrapAPI.php");

$db = Core_Database::getInstance();
$isPM = $db->fetchOne('SELECT ProjectManager FROM clients WHERE UserName=?', array($_SESSION['UserName']));
if (!$isPM) {
	header("Location: /");
	exit;
}

$win = '';
$notFound = false;

if (isset($_POST['win'])) {
	$win = trim($_POST['win']);
	$wo = $db->fetchRow("SELECT WIN_NUM,Company_ID FROM " . Core_Database::TABLE_WORK_ORDERS . " WHERE WIN_NUM=?", array($win));

    if ($wo && $wo['Company_ID']) {
        ob_end_clean();

        if ( null === $_GET['v'] || $_GET['v'] == $wo['Company_ID'] ) {
            header('Location: /clients/wos.php?v=' . urlencode($wo['Company_ID']) . '&tab=all&search=1&full_search=1&win=' . urlencode($wo['WIN_NUM']), true, 301);
        } else {
            header('Location: /clients/wos.php?v=' . urlencode($wo['Company_ID']) . '&tab=all&search=1&full_search=1&win=' . urlencode($wo['WIN_NUM']) . '&v1=' . urlencode($_GET['v']), true, 301);
        }
		exit;
	} else {
		$notFound = true;
	}
}
ob_end_flush();

    $Company_ID   = ( !empty($_GET['v']) ) ? $_GET['v'] : $_SESSION['Company_ID'];
    $UserType     = ( !empty($_SESSION['UserType']) ) ? $_SESSION['UserType'] : NULL;
    $isFLSManager = (strtolower($Company_ID) == 'fls' && (strtolower($UserType)==='install desk' || strtolower($UserType)==='manager'));
    if($isFLSManager){
        $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
    } else {
        $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
    }

require ("../navBar.php");
?>

<p>
</p>
<div class="popup_form_holder">
	<form action="/clients/GPM_FindWO.php?v=<?= $_GET['v'] ?>" method="post" id="FindWinForm">
		<input type="hidden" name="full_search" value="1" />
		<table class="form_table" cellspacing="0">
			<col class="col1" />
			<col class="col2" />
			<?php if ($notFound) : ?>
			<tr>
				<td colspan="2" class="tCenter error">Work order not found</td>
			</tr>
			<?php endif; ?>
			<tr>
				<td class="label_field">WIN# <span style="color: red">* Required</span></td>
				<td><input id="txtwinnum" type="text" class="input_text numeric_input" name="win" value="<?php echo htmlspecialchars($win) ?>" /></td>
			</tr>
			<tr class="submit_raw">
				<td colspan="2" align="center"><input type="button" name="Go" id="Go" value="Search" onclick="fcIsRestricted()" class="input_button" /></td>
			</tr>
		</table>
	</form>
</div>
<Script language="JavaScript">
//743 

        function fcIsRestricted()
        {
             var txtwinnum = $("#txtwinnum").val();
            $.ajax({
                    url     : '/widgets/dashboard/client/is-restricted/',
                    data    : 'winnum='+txtwinnum,
                    cache   : false,
                    type    : 'POST',
                    success : function (data, status, xhr) {
                        if(data['restrict'])
                        {
                            var FSCSDName = "";
                            var FSAMName = "";

                            if(data['csd']==null || data['csd']=="")
                            {
                                FSCSDName="CSD";
                            }
                            else
                            {
                                FSCSDName = "CSD ("+data['csd']+")";
                            }

                            if(data['am']==null || data['am']=="")
                            {
                                FSAMName="AM";
                            }
                            else
                            {
                                FSAMName = "Account Manager ("+data['am']+")";
                            }

                            centerPopup("This client&rsquo;s access has been restricted. Please contact your "+FSCSDName+" or "+FSAMName+" before creating or managing Work Orders.");
                            //load popup
                            loadPopup();
                            //CLOSING POPUP
                            //Click the x event!
                            $("#popupContactClose").click(function(){
                                disablePopup();
                                $("#FindWinForm").submit();
                            });
                            //Press Escape event!
                            $(document).keypress(function(e){
                                if(e.keyCode==27 && popupStatus==1){
                                    disablePopup();
                                }
                            }); 
                        }
                        else
                        {
                           $("#FindWinForm").submit();
                        }
                    },
                    error       : function ( xhr, status, error ) {
                        //detailObject.onInit._roll.hide();
                    }
                });
        }
        //end 743
</script>
<?php require ("../footer.php"); ?>
