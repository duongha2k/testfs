<?php
	$page = clients;
	$option = techs;
	require_once("../header.php");
	require_once("../navBar.php");
	require_once("../library/caspioAPI.php");
//	if (parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) != $_SERVER['HTTP_HOST']) die();
	if (!isset($_GET["id"]) || !is_numeric($_GET["id"])) die();
	$id = $_GET["id"];
	$type = (!isset($_GET["type"]) ? "" : caspioEscape($_GET["type"]));
		
	// build dropdown of ranking source 
	$sourceList = caspioSelect("TR_Tech_Rankings", "DISTINCT RankingSource", "TechID = '$id'", "RankingSource ASC", false);
	if (sizeof($sourceList) != 1 || $sourceList[0] != "") {
		$list = "<option " . ($type == "" ? "selected='selected' " : "") . "value=''>All</option>";
		foreach ($sourceList as $row) {
			$row = trim($row, "'");
			$list .= "<option " . ($type == $row ? "selected='selected' " : "") . "value='$row'>$row</option>";
		}
	}
?>

<script type="text/javascript">
	function sourceTypeChange() {
		myUrl = "<?php echo $_SERVER['PHP_SELF']?>?cbResetParam=1&id=<?=$id?>";
		if (this.value != "") {
			myUrl += "&type=" + this.value;
		}
		document.location.replace(myUrl);
	}

	$(document).ready(
		function () {
			$('#myType').change(sourceTypeChange);
		}
	);
	
</script>

<div align="center">
	Rating Source:
	<select id="myType" name="myType">
		<?=$list?>
	</select>
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b0000f1j8b7j2h5i4b1b2g8f0","https:");}catch(ilolli){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000f1j8b7j2h5i4b1b2g8f0">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>
</div>


<form id="navigation">
	<input type="button" value="BACK" id="back" onclick="javascript:history.back()" />
</form>

<?php
	require_once("../footer.php");
?>
		
