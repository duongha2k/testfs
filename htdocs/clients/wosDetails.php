<?php
$page = 'clients';
$option = 'wosDetails';
require_once ("../headerStartSession.php");

$isDash = !empty($_GET['isDash']);
$_GET['id'] = (!empty($_GET['id']) ) ? $_GET['id'] : NULL;

//14914
$v = $_GET['v'];
if (empty($_GET['id'])) {
    header('Location: http' . ($_SERVER['HTTPS'] == 'on' ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . '/clients/wos.php?v=' . $v.'&tab=workdone');
}
//end 14914

$siteTemplate = (!empty($_SESSION['template']) ) ? $_SESSION['template'] : NULL;
if ($siteTemplate == "ruo") {
    require '../templates/ruo/includes/header.php';
} else {
    if (!$isDash)
        require '../header.php';
    else
        require '../headerSimple.php';
}

$_SERVER['HTTPS'] = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ) ? 'on' : 'off';
$loggedIn = (!empty($_SESSION['loggedIn']) ) ? $_SESSION['loggedIn'] : NULL;
$Company_ID = (!empty($_SESSION['Company_ID']) ) ? $_SESSION['Company_ID'] : NULL;

if ($loggedIn != "yes") {
    header('Location: http' . ($_SERVER['HTTPS'] == 'on' ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . '/clients/logIn.php'); // Redirect to logIn.php
}


$UserType = $_SESSION['UserType'];
$isFLSManager = (strtolower($Company_ID) == 'fls' && isset($UserType) && (strtolower($UserType) === 'install desk' || strtolower($UserType) === 'manager'));
$dbVersion = (isset($_POST['version']) && strtolower($_POST['version']) === 'lite') ? 'lite' : 'full';
if (!$isDash)
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'quickassign', 'quickapprove');
else
    $toolButtons = array();
if ($_SESSION['UserType'] == 'Manager' || $_SESSION['UserType'] == 'Install Desk')
    $toolButtons = array('FindWorkOrder', 'CreateWorkOrder');
if (!$isDash)
    require '../navBar.php';
else
    $displayPMTools = false;
require 'includes/adminCheck.php';
require 'includes/PMCheck.php';

//    $navBarState = isset($_COOKIE['wosDetailsNavBarState'])?$_COOKIE['wosDetailsNavBarState']:null;
if (!$navBarState) {
    $navBarState = 'opened';
}
?>

<script type="text/javascript">
    window._company = '<?= $_GET['v'] ?>';
    //  use for load images and save it in browser cache only!!!
    var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
    var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";
</script>
<!--<script type="text/javascript" src="/widgets/js/jquery.min.js"></script>-->
<script type="text/javascript" src="/widgets/js/jquery.appear.min.js"></script>
<script src="<?= empty($_SERVER['HTTPS']) ? 'http' : 'https' ?>://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<link rel="stylesheet" href="<?= empty($_SERVER['HTTPS']) ? 'http' : 'https' ?>://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/ui-lightness/jquery-ui.css" type="text/css" />

<script type="text/javascript" src="/widgets/js/jquery.form.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.scrollFollow.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="/widgets/js/star-rating/jquery.rating.pack.js"/>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.easing.pack.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.iframe-auto-height.plugin.1.5.0.min.js"></script>
<!--script type="text/javascript" src="/widgets/js/FSQuickAssignTool.js?11112011"></script-->
<?php script_nocache ("/widgets/js/FSQuickAssignTool.js"); ?>
<!--script type="text/javascript" src="/widgets/js/functions.js?07242012"></script-->
<?php script_nocache ("/widgets/js/functions.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSWidget.js"></script-->
<?php script_nocache ("/widgets/js/FSWidget.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSWidgetDashContentLeft.js?11222011"></script-->
<?php script_nocache ("/widgets/js/FSWidgetDashContentLeft.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSWidgetDashContentRight.js"></script-->
<?php script_nocache ("/widgets/js/FSWidgetDashContentRight.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSWidgetDashboard.js?11222011"></script-->
<?php script_nocache ("/widgets/js/FSWidgetDashboard.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSWidgetWODetails.js?10192012"></script-->
<?php script_nocache ("/widgets/js/FSWidgetWODetails.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script-->
<?php script_nocache ("/widgets/js/FSPopupAbstract.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script-->
<?php script_nocache ("/widgets/js/FSPopupRoll.js"); ?>
<!--script type="text/javascript" src="/widgets/js/ProgressBar.js"></script-->
<?php script_nocache ("/widgets/js/ProgressBar.js"); ?>
<!--script type="text/javascript" src="/widgets/js/ProgressBarAdapterAbstract.js"></script-->
<?php script_nocache ("/widgets/js/ProgressBarAdapterAbstract.js"); ?>
<!--script type="text/javascript" src="/widgets/js/ProgressBarAdapterPopupSpin.js"></script-->
<?php script_nocache ("/widgets/js/ProgressBarAdapterPopupSpin.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSFindWorkOrder.js"></script-->
<?php script_nocache ("/widgets/js/FSFindWorkOrder.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSClientTechProfile.js"></script-->
<?php script_nocache ("/widgets/js/FSClientTechProfile.js"); ?>
<?php script_nocache ("/widgets/js/FSMultiselectControl.js"); ?>
<!--script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script-->
<?php script_nocache ("/widgets/js/ajaxfileupload.js"); ?>
<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<link rel="stylesheet" href="/widgets/css/popup.css" type="text/css" />
<link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" />
<link rel="stylesheet" href="/widgets/js/star-rating/jquery.rating.css" type="text/css"/>

<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.formatCurrency-1.4.0.js"></script>
<!--script type="text/javascript" src="/widgets/js/FSTechPayUtils.js"></script-->
<?php script_nocache ("/widgets/js/FSTechPayUtils.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSTechSchedule.js"></script-->
<?php script_nocache ("/widgets/js/FSTechSchedule.js"); ?>
<!--script type="text/javascript" src="/widgets/js/ProgressBar.js"></script-->
<?php script_nocache ("/widgets/js/ProgressBar.js"); ?>
<script src="/widgets/js/base64.js"></script>
<!--script type="text/javascript" src="/widgets/js/FSWidgetWODashboard.js"></script-->
<!--script type="text/javascript" src="/widgets/js/FSMyTechs.js"></script-->
<!--script type="text/javascript" src="wosDetails.js"></script-->
<?php script_nocache ("/clients/wosDetails.js"); ?>
<script type="text/javascript" src="/widgets/js/jquery.scrollTo.js"></script>
<script type="text/javascript" src="../library/jquery/jquery.bt.min.js"></script>

<?php script_nocache ("/clients/js/wintags.js"); ?>

<?php script_nocache ("/widgets/js/FSCoreClasses.js"); ?>
<?php script_nocache ("/widgets/js/FSModelViewController.js"); ?>
<?php script_nocache ("/widgets/js/FSFlyOutPanel.js"); ?>
<?php script_nocache ("/widgets/js/FSSuggestedTechPay.js"); ?>

<script type="text/javascript">
    var schedule = new FSTechSchedule({width: 940, height: 450});
</script>

<style type="text/css">
    /* Dialog
    ----------------------------------*/
    .ui-dialog { position: relative; padding: .2em; width: 300px; }
    .ui-dialog .ui-dialog-titlebar { padding: .5em .3em .3em 1em; position: relative;  }
    .ui-dialog .ui-dialog-title { float: left; margin: .1em 0 .2em; } 
    .ui-dialog .ui-dialog-titlebar-close { position: absolute; right: .3em; top: 50%; width: 19px; margin: -10px 0 0 0; padding: 1px; height: 18px; }
    .ui-dialog .ui-dialog-titlebar-close span { display: block; margin: 1px; }
    .ui-dialog .ui-dialog-titlebar-close:hover, .ui-dialog .ui-dialog-titlebar-close:focus { padding: 0; }
    .ui-dialog .ui-dialog-content { border: 0; padding: .5em 1em; background: none; overflow: auto; zoom: 1; }
    .ui-dialog .ui-dialog-buttonpane { text-align: left; border-width: 1px 0 0 0; background-image: none; margin: .5em 0 0 0; padding: .3em 1em .5em .4em; }
    .ui-dialog .ui-dialog-buttonpane button { float: right; margin: .5em .4em .5em 0; cursor: pointer; padding: .2em .6em .3em .6em; line-height: 1.4em; width:auto; overflow:visible; }
    .ui-dialog .ui-resizable-se { width: 14px; height: 14px; right: 3px; bottom: 3px; }
    .ui-draggable .ui-dialog-titlebar { cursor: move; }
    body{
        min-width:1100px;
        overflow-x:auto;
    }
    .sectionInner{
        padding:5px;
    }
    #container2{
        position:relative;	/* This fixes the IE7 overflow hidden bug */
        clear:both;
        width:100%;			/* width of whole page */

        min-width: 900px;
    }
    #center{
        min-width: 800px;
        position: relative;
        width: 100%;
    }
    #layout{
        float:left;
        width:100%;			/* width of page */
        position:relative;  
    }
    #details{

        width: 100%;
    }
    #container2 #detailsContainer{
        			/* width of center column content (column width minus padding on either side) */
       		/* 100% plus left padding of center column */
        margin-top:2%;
    }
    #container2 #dashleftcontent{
        			/* Width of left column content (column width minus padding on either side) */			/* width of (right column) plus (center column left and right padding) plus (left column left padding) */
    }
    #container2 #dashrightcontent{
       			/* Width of right column content (column width minus padding on either side) */
       			/* Please make note of the brackets here:
                                        (100% - left column width) plus (center column left and right padding) plus (left column left and right padding) plus (right column left padding) */
    }
    #dashleftcontent, #dashrightcontent{
        float:left;
        position:relative;
        padding:0 0 1em 0;	/* no left and right padding on columns, we just make them narrower instead 
                                        only padding top and bottom is included here, make it whatever value you need */
        overflow:hidden;
    }

    #detailsContainer{
        /*float:left;
        position:relative;
        padding:0 0 1em 0;*/	/* no left and right padding on columns, we just make them narrower instead 
                                        only padding top and bottom is included here, make it whatever value you need */

    }	
    #newsCenter{
        display: none;
    }

    .toolSubmit{
        background-color: #4790D4;
        color: #FFFFFF;
        width: 70px; 
        margin-top: 5px;
    }
    .toolSubmit:hover{
        font-weight: bold;
        text-decoration:underline;
        background-color: #032D5F;
        color: #FFFFFF;
        cursor: pointer;
    }

    .toolsRightContent div{
        line-height: 10px;
    }
    .right_col{
        float: none;
        width: auto;
        text-align: left;
        margin-left: 10px;
        margin-top: 25px;
    }
    .left_col{
        float: none;
        width: auto;
        text-align: left;
        margin-top: 25px;
    }
    .sidebarbtns{
        float: none;
        width: 95%;
        text-align: left;
        margin-left: auto;
        margin-top: 10px;
        margin-right: auto;
    }
    #dashleftcontent {

        background-color:#FFFFFF;
        /*		display: none;*/
        font: 10px/10px Verdana,Arial,Helvetica,sans-serif;
        /*background: url("/widgets/images/border_right.png") no-repeat scroll right top transparent;*/
        color: #6A6A6A;
        padding-right: 16px;
        line-height: 15px;
    }

    #dashrightcontent {

        background-color:#FFFFFF;
        display: none;
        font: 10px/10px Verdana,Arial,Helvetica,sans-serif;
        text-align: left;
        white-space:nowrap;

        background: url("/widgets/images/border_right.png") no-repeat scroll left top transparent;
        padding-left: 16px;
    }


    .use-sidebar-right #dashrightcontent {
        display:table-cell;
        width: 245px;
        white-space:nowrap;
        overflow: hidden;
        margin-top: 10px;
    }

    .use-sidebar-left #dashleftcontent {
        display:table-cell;
        width: 245px;
        white-space:nowrap;
        overflow: hidden;
    }


    .use-sidebar-right #separatorRight {
        margin-right: auto;
        display: block;
        outline: none;
        width: 16px;
        height: 16px;
        background: url('/widgets/images/arrows-right5.png');
        border-color: #FFF;
        margin-bottom: 10px;
    }

    .use-sidebar-left #separatorLeft {
        display: block;
        outline: none;
        width: 16px;
        height: 16px;
        background: url('/widgets/images/arrows-left5.png');
        border-color: #FFF;
        margin-left: auto;

    }

    .sidebar-at-right #separatorRight {float: left;}
    .sidebar-at-left #separatorLeft {float: left;}

    .sidebar-at-right #separatorRight{
        float: right;
        display: block;
        outline: none;
        width: 16px;
        height: 16px;
        background: url('/widgets/images/arrows-left5.png') no-repeat;
        /*margin-left: -200px;*/
        margin-top: 0px;

    }

    .sidebar-at-left #separatorLeft{
        float: left;
        display: block;
        outline: none;
        width: 16px;
        height: 16px;
        background: url('/widgets/images/arrows-right5.png') no-repeat;
        margin-right: 2px;
        margin-top: 0px;
    }

    #container2 #dashleftcontent{
        width:245px;
    }
    #container2 #dashrightcontent{
        width:245px;

    }

    .heading{
        background: url("/widgets/images/triangle.gif") no-repeat scroll left 1px transparent;
        font-size: 13px;
        margin: 10px 0;
        padding: 0 0 0 20px;
        color: #595959;
        font-weight: bolder;
    }
    .toolsLeftContent{

        margin: 0px 0px 10px 10px;
        color:#10085a;

    }

    .toolsRightContent{

        margin: 10px 0px 10px 0px;
        text-align:left;
        color:#10085a;
        border-top: 1px #DDDDDD solid;
    }

    .toolSubmit{
        background-color: #4790D4;
        color: #FFFFFF;
        width: 70px; 
        margin-top: 5px;
    }
    .toolSubmit:hover{
        font-weight: bold;
        text-decoration:underline;
        background-color: #032D5F;
        color: #FFFFFF;
        cursor: pointer;
    }

    .toolsRightContent div{
        line-height: 1;
    }
    table tr.special td{
        border-top: 1px #DDDDDD solid;
    }
    #chevrons{
        width: 18%; 
        position:relative;
    }
    #separatorLeftInside{
        margin-top:20px;
    }
    #details{
        overflow-x: hidden;
    }
    #details a.title{
        text-align: left;
    }
    #dashleftcontent ul{
        font-size: 11px;
         margin: 0 20px;
    padding: 0;
    }
     #dashleftcontent ul li{
        list-style: disc outside none;
        margin-bottom: 5px;

    }
    .left_col{
        background: transparent;
    }
</style>


<script type="text/javascript">
    var LVDefaultFunction = function(){this.addFieldClass();}
    var LVDefaults = {
        onValid : LVDefaultFunction,
        onInvalid : LVDefaultFunction
    }
    var pmContext = "<?= $_GET["v"] ?>";
    function gotoTab(name) {
        location = location.toString().replace('<?= $_SERVER['SCRIPT_NAME'] ?>', '/clients/wos.php') + '&tab=' + name;
        return false;
    }
    function toggleNavBar( btn )
    {
        var bar = $('#leftVerticalContainer');
        var content = $('#detailsContainer');
        btn = $(btn);

        if (btn.is(".closed")){
            btn.removeClass("closed");
            btn.parent().css('width', '18%');
            btn.prev('span').addClass('displayNone');
            bar.css('display', '');
            bar.parent().css('width', '18%');
            bar.parent().css('background', 'transparent url(/widgets/images/border_right.png) no-repeat scroll right top');
            content.css('width', '59%');
            setCookie('wosDetailsNavBarState', 'opened', 30);
			
        }else{
            btn.css("background-image","");
            btn.addClass("closed");
            btn.prev('span').removeClass('displayNone');
            btn.parent().css('width', '100%');
            bar.css('display', 'none');
            bar.parent().css('width', '3%');
            bar.parent().css('background', '');
            content.css('width', '96%');
            setCookie('wosDetailsNavBarState', 'closed', 30);
				
				
        }
        
        return false;
    }
    /**
     *  calendarInit
     *
     *  Display filter post handler
     */
    function calendarInit() {
        $.map(['edit_StartDate', 'edit_EndDate','edit_DateNotified'], function( id ) {
            //$('#' + roll.container().id + ' #' + id).focus( function() {
            //$('#calendar_div').css('z-index', ++roll.zindex);
            $('#'+id).calendar({dateFormat:'MDY/'});
            //});
        });
    }
    /**
     * calendarMove
     *
     * @param event  event
     * @access public
     * @return void
     */
    function calendarMove( input ) {
        $('#calendar_div').css('top', $(input).offset().top + $(input).height() + 6 + 'px');
        $('#calendar_div').css('left', $(input).offset().left + 'px');
        popUpCal.showFor(input);
    }
    var roll;
    var detailsWidget;
    var navWidget;
    var wd;
    var activityLog;    //  Activity Log object
    var findWorkOrder;  //  Find work order object
    var assignTool;
    var progressBar;    //  Progress bar object

    var checkMinWidth = function(){
		
				
				
<?php if (!isset($_GET['showrightpanel'])): ?>
            if(objMain.hasClass('use-sidebar-left')){
                $("#dashleftcontent").parent().show();
                $("#dashleftcontent").show();				
            }
            if(!objMain.hasClass('use-sidebar-left')){
                $("#dashleftcontent").parent().hide();
                $("#detailsContainer #details").css("width","840px");				
            }	

<?php endif; ?>

<?php if (!empty($_GET['showrightpanel'])): ?>
            //$("#center").css("min-width", "900px");
            if(objMain.hasClass('use-sidebar-left') && !objMain.hasClass('use-sidebar-right')){					
                $("#detailsContainer").css("width","");
                $("#detailsContainer").css("overflow-x","");
                $("#dashleftcontent").parent().show();
                $("#dashrightcontent").parent().hide();
                $("#dashleftcontent").show();
                $("#wodetails").removeAttr("style");
            }
            if(objMain.hasClass('use-sidebar-left') && objMain.hasClass('use-sidebar-right')){
                $("#detailsContainer").css("width","581px");
                $("#detailsContainer").css("overflow-x","auto");
                $("#dashrightcontent").parent().show();
                $("#dashleftcontent").parent().show();
                $("#dashleftcontent").show();
                $("#dashrightcontent").show();					
            }

            if(objMain.hasClass("use-sidebar-right") && !objMain.hasClass("use-sidebar-left")){
                $("#detailsContainer").css("width","");
                $("#detailsContainer").css("overflow-x","");
                $("#dashleftcontent").parent().hide();
                $("#dashrightcontent").parent().show();
                $("#dashrightcontent").show();
                //open left
                                    
            }
            if(!objMain.hasClass("use-sidebar-right") && !objMain.hasClass("use-sidebar-left")){
                $("#detailsContainer").css("width","");
                $("#detailsContainer").css("overflow-x","");
                $("#dashleftcontent").parent().hide();
                $("#dashrightcontent").parent().hide();
                $("#dashleftcontent").removeAttr("style").css("display", "none");
            }
            				
<?php endif; ?>
    };
	
    $(document).ready(function() {
        roll = new FSPopupRoll();
        detailsWidget = new FSWidgetWODetails({container:'detailsContainer',tab:'index',win:"<?= $_GET['id'] ?>",reftab:"<?= isset($_GET['tab']) ? $_GET['tab'] : '' ?>"},roll);
        detailsWidget.setPublish(<?= $_GET['publish'] ?>);
        detailsWidget.show({tab:'index', sectionTab: "<?= $_GET['sectionTab'] ?>",params:{company:window._company,container:'detailsContainer',win:"<?= $_GET['id'] ?>",tab:"<?= isset($_GET['tab']) ? $_GET['tab'] : '' ?>",techID:"<?= isset($_GET['techID']) ? $_GET['techID'] : '' ?>"}});

        objMain = $("#container2");
	
        dr = new FSWidgetDashContentRight({container:'dashrightcontent', objMain: objMain, checkMinWidth: checkMinWidth, company:window._company});
<?php if (!empty($_GET['showrightpanel']) && !$isFLSManager): ?>
            dr.show();
            dr.separatorClick();
            dr.separatorClick();
<?php endif; ?>

        dl = new FSWidgetDashContentLeft({container:'dashleftcontent', objMain: objMain, dr: dr, checkMinWidth: checkMinWidth, company:window._company});
<?php if (!$isFLSManager): ?>
            dl.show();
<?php endif; ?>
        checkMinWidth();
        //dl.separatorClick();
		
        /*wd = new FSWidgetDashboard({container:'detailsContainer', tab: 'index'});
                findWorkOrder = new FSFindWorkOrder(wd, roll);
        findWorkOrder.buildHtml(); */
        
        navWidget = new FSWidgetWODetails({container:'leftVerticalContainer',tab:'navigation',customfunctions:{showPreloader:function(){}}},null);
        //        navWidget.show({tab:'navigation',params:{company:window._company, id:<?= $_GET['id'] ?>,container:'leftVerticalContainer'}});
<?php if ($navBarState == 'closed'): ?>
            var btn = $('#navBarToggle');
            var bar = $('#leftVerticalContainer');
            var content = $('#detailsContainer');
            btn.text('>>');
            //btn.attr('title','Show sidebar');
            btn.parent().css('width', '100%');
            btn.prev('span').removeClass('displayNone');
            bar.css('display', 'none');
            bar.parent().css('width', '2%');
            bar.parent().css('background', '');
            content.css('width', '97%');

<?php endif; ?>

  
        assignTool  = new FSQuickAssignTool( detailsWidget );
        progressBar = new ProgressBar();
        progressBar.key('assign');
        progressBar.adapter(new ProgressBarAdapterPopupSpin(progressBar, null));

        /*        myTechs = new FSMyTechs(wd, roll);
        myTechs.buildHtml();*/

        $("#FindWorkOrderBtn").click(function(event) {
            roll.autohide(false);
            var opt = {
                width       : 325,
                height      : '',
                title       : 'Find a Work Order',
                handler		: findWorkOrder.prepare,
                context     : wd,
                body        : $(findWorkOrder.container()).html()
            };
            roll.showNotAjax(this, event, opt);
        });
        $("#QuickAssignBtn").click(function(event) {
            roll.autohide(false);
            var opt = {
                popup       : 'quickassigntool',
                title       : 'Quick Assign',
                width       : 400,
                preHandler  : assignTool.getWidgetHtml,
                postHandler : assignTool.initEvents,
                context     : assignTool,
                delay       : 0
            }
            roll.show(this,event,opt);
        });
        $("#QuickApproveBtn").click(function(event) {
            roll.autohide(false);
            var opt = {
                popup       : 'quickapprove',
                title       : 'Quick Approve',
                width       : 400,
                context     : wd,
                preHandler  : wd.getWidgetHtml,
                delay       : 0,
                postHandler : function() {
                    $("#Approve_approveForm").ajaxForm({dataType: 'json', success: function(data) {
                            if (data.error == 0) {
                                roll.hide();
                                detailsWidget.show({tab:'index',params:{company:window._company,container:'detailsContainer',win:"<?= $_GET['id'] ?>",tab:"<?= isset($_GET['tab']) ? $_GET['tab'] : '' ?>"}});
                            } else {
                                $('#woErrorMsg').html(data.msg);
                                $('#woErrorMsg').show('fast',function(){
                                    setTimeout(function () { $('#woErrorMsg').hide(500); }, 3000);
                                });
                            }
                        }, beforeSubmit: function() {
                            progressBar.key('updatepayfinan');
                            progressBar.run();
                            return true;
                        }});
                }
            }
            roll.show(this,event,opt);
        });

        /*        $("#MyTechsBtn").click(function(event) {
            roll.autohide(false);
            var opt = {
                width       : 325,
                height      : '',
                title       : 'My Techs',
                handler		: myTechs.prepare,
                context     : wd,
                body        : $(myTechs.container()).html()
            };
            roll.showNotAjax(this, event, opt);
        });*/

        /*$("#CreateWorkOrderBtn").click(function(event) {
                openPopup('/clients/createWO.php?v=<?php echo $_GET["v"] ?>');
        });*/
		
		
	

    });

    // reload parts count info
    function updatePartsCount()
    {
		location.reload();
    }
    /*function showMyTechs(){
                var myTechs; 		// 	My Techs Gadgets
                myTechs = new FSMyTechs(wd, roll);
        $("#mytechs").load(function(event) {
            var opt = {
                                width       : 325,
                                height      : '',
                                title       : 'My Techs',
                                handler		: myTechs.prepare,
                                context     : wd,
                                body        : $(myTechs.container()).html()
            };
            roll.showNotAjax(this, event, opt);
        });     
                myTechs.buildHtml();  
        }*/
	

</script>
<!-- Main content -->


<div id="container2" class="clr use-sidebar-left" style="width:100%;">
    <div id="center">
        <div class="sidebarbtns">
            <div style="display: none;" onclick="javascript:dl.separatorClick(this);" id="separatorLeftOutside">
                <a id="separatorLeft"></a>
            </div>
            <div onclick="javascript:dr.separatorClick(this);" id="separatorOutside">
                <a id="separatorRight"></a>
            </div>
        </div>
        <div id="layout">
            <table width="100%" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" align="left" style="width: 245px;padding-left: 10px;">
                        <div id="dashleftcontent" class="left_col"></div>
                    </td>
                    <td valign="top" align="left" style="">
                        <div id="detailsContainer"></div>
                    </td>
                    <td valign="top" align="left">
                        <div id="dashrightcontent"></div>
                    </td>
                </tr>
            </table>

            <script type="text/javascript">
                if (document.referrer.match(/wos./)){
                    if(document.referrer.match(/wosDetails/)){
                        document.write('&nbsp');
                    }else{
                        document.write('<br class="clear">');
                    }
                }
            </script>
            

        </div>
    </div>
</div>
<?php
/* * * End Content ** */
if (!$isDash)
    require ("../footer.php");
?>
<script>
    var _wosDetails = null;
</script>    
