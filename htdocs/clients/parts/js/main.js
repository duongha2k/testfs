$(document).ready(function(){
    $(".datepicker").calendar({dateFormat: 'MDY/'});
    if($('#newPartNumber'))
        $('#newPartNumber').focus();
});

function updatePartsCount() {
	try {
		if (typeof(wasChanged) == 'undefined' || wasChanged) {
			// update when was changes
			opener.updatePartsCount();
		}
	} catch (e) {}	
}
function shippingAddressEnabled(id){
    $('#shippingAddress'+id).val('');
    if($('#shipTo'+id).val()=='Other')
        $('#shippingAddress'+id).attr('disabled', false);
    else
        $('#shippingAddress'+id).attr('disabled', true);
}
function addNewPart(num, color){
    aldNum = num-1;
    wasChanged = true;
    $.ajax({type:"POST", url:"ajax/insertNewPartLine.php", dataType:"html", data:{partNum:num, color:color, woId: $("#woId").val()},
        success: function(data) {
        $('#addNewPartButton'+aldNum).before(data);
        $('#addNewPartButton'+aldNum).remove();
        $(".datepicker").calendar({dateFormat: 'MDY/'});
        }
    });
}

var startCount = 0;
var finishCount = 0;
function updateParts(woId){
    answer = confirm("Save changes?");
	if (answer){
		$('#assignErrorsDiv').empty();
        partsEx  = $("tr[name='partEx']");
        partsNew = $("tr[name='partNew']");
        startCount = partsEx.length+partsNew.length;
		pmc = $('#pmContext').val();
		$("*").css("cursor", "progress");
        updatePartsRecursive(partsEx, pmc, woId);
        addPartsRecursive(partsNew, pmc, woId);
        checkR();
	}
	return false;
}
function checkR(){
    setTimeout(function(){
	if(startCount==finishCount){
		updatePartsCount();
		if (parent) {
			location.reload();
		}
		else
			this.close();
		startCount=0;
		finishCount=0;
	}
	else {
		checkR();
	}
   },500);
}
function updatePartsRecursive(parts, num, woId){
    parts.each(function(){
        if($('#DeletePart'+this.id).attr('checked')) removePart(this.id, woId, num);
        else updatePart(this.id, woId, num);
    });
/*  if (parts[num]){
        partId = parts[num].id;
        if($('#DeletePart'+partId).attr('checked')) removePart(parts, woId, num);
        else updatePart(parts, woId, num);
    } else {
        updSt = 'w';
        partsNew = $("tr[name='partNew']");
        addPartsRecursive(partsNew, 0, woId);
    }*/
}
function addPartsRecursive(parts, num, woId){
    parts.each(function(){
        if($('#Delete'+this.id).attr('checked')){ $('#'+this.id).remove(); finishCount++;}
        else addPart(this.id, woId, num);
    });
/*  if (parts[num]){
        partId = parts[num].id;
        if($('#Delete'+partId).attr('checked')){
            $('#'+partId).remove();
            addPartsRecursive(parts, num+1, woId);
        } else addPart(parts, woId, num);
    } else {
        addSt = 'w';
        //this.close();
    }*/  
}
function addPart(parts, woId, num){
    //partId = parts[num].id;
    partId=parts;
    wasChanged = true;
    params = 'woId='+woId+'&pmContext='+num;
    allInputs = $('#'+partId+' input:enabled');
    allInputs.each(function(n,element){
        if (element.type == 'text') params += '&'+element.name+'='+element.value;
        if (element.type == 'checkbox'){
            params += '&'+element.name+'=';
            params += $(element).attr('checked')?'1':'0';
        }
    });
    allSelects = $('#'+partId+' select:enabled');
    allSelects.each(function(n,el){ params+='&'+el.name+'='+$(el).val(); });
    $.ajax({type:"POST", url:"ajax/addPart.php", dataType:"json", data:params,
        success: function(data) {
            finishCount++;
            errors = '';
            jQuery.each(data.errors, function() { errors += '<b>'+this+'</b><br/>'; });
            if (errors != '') {
                $('#assignErrorsDiv').html(errors);
                $('#assignErrorsDiv').css('color', '#FF0000');
            }/* else {
                if (data.res != ''){
                    $('#assignErrorsDiv').append('<br>Part #'+data.res+' has added.');
                    $('#assignErrorsDiv').css('color', '#00cc00');
					//updatePartsCount();
                }
            }*/
            //addPartsRecursive(parts, num+1, woId);
        }
    });
}
function updatePart(parts, woId, num){
    //partId = parts[num].id;
    partId=parts;
    wasChanged = true;
    params = 'partId='+partId+'&woId='+woId+'&pmContext='+num;
    allInputs = $('#'+partId+' input:enabled');
    allInputs.each(function(n,element){
        if (element.type == 'text') params += '&'+element.name+'='+element.value;
        if (element.type == 'checkbox'){
            params += '&'+element.name+'=';
            params += $(element).attr('checked')?'1':'0';
        }
    });
    allSelects = $('#'+partId+' select:enabled');
    allSelects.each(function(n,el){ params+='&'+el.name+'='+$(el).val(); });
    $.ajax({type:"POST", url:"ajax/updatePart.php", dataType:"json", data:params,
        success: function(data) {
            finishCount++;
            errors = '';
            jQuery.each(data.errors, function() { errors += '<b>'+this+'</b><br/>'; });
            if (errors != '') {
                $('#assignErrorsDiv').html(errors);
                $('#assignErrorsDiv').css('color', '#FF0000');
            }/* else {
                if (data.res != ''){
                    $('#assignErrorsDiv').append('<br>Part #'+data.res+' has updated.');
                    $('#assignErrorsDiv').css('color', '#00cc00');
					//updatePartsCount();
                }
            }*/
            //updatePartsRecursive(parts, num+1, woId);
        }
    });
}
function removePart(parts, woId, num){
    partId=parts;
    wasChanged = true;
    //partId = parts[num].id;
    $.ajax({
        type: "POST",
        url: "ajax/removePart.php",
        dataType: "json",
        data: {partId:partId,woId:woId,pmContext:num},
        success: function(data) {
            finishCount++;
            errors = '';
            jQuery.each(data.errors, function() {
                errors += '<b>'+this+'</b><br/>';
            });
            if (errors != ''){
                $('#assignErrorsDiv').html(errors);
                $('#assignErrorsDiv').css('color', '#FF0000');
            }else{
                if (data.res != ''){
                    //$('#assignErrorsDiv').append('<br>Part has removed.');
                    //$('#assignErrorsDiv').css('color', '#00cc00');
                    $('#'+data.res).remove();
					//updatePartsCount();
                }
            }
            //updatePartsRecursive(parts, num+1, woId);
        }
    });
}
function consumable(el, id){
    allInputs  = $('#partReturn'+id+' input');
    allSelects = $('#partReturn'+id+' select');
    if ($(el).attr('checked')) {
        $('#partReturn'+id).css('color', '#999999');
        allInputs.attr('disabled', 'true');
        allSelects.attr('disabled', 'true');
    } else {
        $('#partReturn'+id).css('color', '#000000');
        allInputs.removeAttr('disabled');
        allSelects.removeAttr('disabled');
    }
}
function deletePart(el, id){
    allReturnInputs = $('#partReturn'+id+' input');
    allExistInputs  = $('#partExist'+id+' input');
    allReturnSelects = $('#partReturn'+id+' select');
    allExistSelects = $('#partExist'+id+' select');
    if ($(el).attr('checked')) {
        $('#partReturn'+id).css('color', '#999999');
        $('#partExist'+id).css('color', '#999999');
        allReturnInputs.attr('disabled', 'true');
        allExistInputs.attr('disabled', 'true');
        allReturnSelects.attr('disabled', 'true');
        allExistSelects.attr('disabled', 'true');
    } else {
        $('#partReturn'+id).css('color', '#000000');
        $('#partExist'+id).css('color', '#000000');
        allReturnInputs.removeAttr('disabled');
        allExistInputs.removeAttr('disabled');
        allReturnSelects.removeAttr('disabled');
        allExistSelects.removeAttr('disabled');
        allExistSelects.removeAttr('disabled');
    }
    $('#consumable'+id).removeAttr('checked');
    el.disabled = false;
}
function showAddNewPart(){
    if($('#addNewPartDiv').css('display') == 'none') {
        $("#addNewPartDiv").slideDown(1000);
        $("#showhideNewPart").text('Add New Part <-');
    } else {
        $("#addNewPartDiv").slideUp(1000);
        $("#showhideNewPart").text('Add New Part ->');
    }
}
function newPartReturnAutoFill(param){
    if ($('#newPartReturn'+param).attr('type') == 'text') {
        $('#newPartReturn'+param).val($('#newPart'+param).val());
    } else {
        qwe = $('#newPart'+param).val();
        $('#newPartReturn'+param+' option').removeAttr('selected');
        $('#newPartReturn'+param+' option[@value='+qwe+']').attr('selected', true);
    }
}
function confirmation() {
	var answer = confirm("Are you sure?");
	if (answer) this.close();
	else return false;
}
