<?php
/**
 * @author Alex Scherba
 */

require_once("../../headerStartSession.php");
require_once("../../library/caspioAPI.php");
require_once("../../headerHead.php");
require_once('../../includes/parts/manager.php');
require_once('../../includes/parts/Auth.php');
//ini_set("display_errors",1);

$woId = (int)$_GET['woId'];
if ($_SESSION["loggedIn"]!='yes' || $_SESSION["loggedInAs"]!='client' || empty($woId)) {header("Location:/clients/logIn.php");}

if ( empty($_GET['v']) && !empty($_SESSION['Company_ID']) ) {
    $_GET['v'] = trim($_SESSION['Company_ID']);
}

$auth = new PartsManagerAuth();
$auth->setLogin($_SESSION['UserName'])
     ->setRole(PartsManagerAuth::AUTH_CLIENT);
$authData = $auth->getAuthData();

$manager = new PartsManager();
$manager->setWoId($woId)
        ->setUser($authData['login'])
        ->setHash($authData['password'])
        ->setPmContext(trim($_GET['v']))
        ->setRole(PartsManager::PARTS_MANAGER_CLIENT);
        
$errors = array();
$partAdded = false;

if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['new'])) {
    $consumable = (int)$_POST['new']['Consumable'];
    $asd = $consumable==1?'True':'False';
    $params = array_map("formatData", $_POST['new']);
    $xml='<PartEntry><Consumable>'.$asd.'</Consumable><NewPart><PartNum>'.$params['PartNum'].'</PartNum><ModelNum>'.$params['ModelNum'].'</ModelNum>'
        .'<SerialNum>'.$params['SerialNum'].'</SerialNum><Description>'.$params['Description'].'</Description>'
        .'<Carrier>'.$params['Carrier'].'</Carrier><TrackingNum>'.$params['TrackingNum'].'</TrackingNum><ETA>'.$params['ETA'].'</ETA>'
        .'<ShipTo>'.$params['ShipTo'].'</ShipTo><ShippingAddress>'.$params['ShippingAddress'].'</ShippingAddress></NewPart>';
    if ($consumable!=1) {
    $xml.='<ReturnPart><PartNum>'.$params['returnPartNum'].'</PartNum><ModelNum>'.$params['returnModelNum'].'</ModelNum>'
        . '<SerialNum>'.$params['returnSerialNum'].'</SerialNum><Description>'.$params['returnDescription'].'</Description>'
        . '<Carrier>'.$params['returnCarrier'].'</Carrier><TrackingNum>'.$params['returnTrackingNum'].'</TrackingNum><ETA>'.$params['returnETA'].'</ETA>'
        . '<RMA>'.$params['returnRMA'].'</RMA><Instructions>'.$params['returnInstructions'].'</Instructions></ReturnPart>';
    }
    $xml.='</PartEntry>';
    $result = @simplexml_load_string($manager->createPart($xml));
    
    if (is_object($result)) {
    	if (!empty($result->error)) {
    	    foreach ($result as $er) $errors[] = '<br/>Error : '.$er->description;
    	}
		$partAdded = true;
    } else {
        $errors[] = '<br/>Server Error.';
    }
}
$xml = $manager->getAllParts();
$parts = simplexml_load_string($xml);
?>
<script>
var wasChanged = false;
</script>
<script type="text/javascript" src="../../library/jquery/calendar/jquery-calendar.js"></script>
<link rel="stylesheet" href="../../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<link rel="stylesheet" href="css/main.css" type="text/css"/>
<script src="js/main.js" type="text/javascript"></script>
<script type="text/javascript">
	var pmc = "<?=trim($_GET['v'])?>";
</script>

<?php if ($partAdded): ?>
<script type="text/javascript"> updatePartsCount();</script>
<?php endif;?>
<body>
    <div>
        <div class='assignErrors' id='assignErrorsDiv'><?foreach($errors as $error)echo$error;?></div>
<!--All parts-->
        <div>
        <table cellpadding="0" cellspacing="0" border="0" id="partsListTable">
            <tr>
                <th>
                    
                    <span><div style='background-color:#4790d4;padding:10px;border-style:solid;border-color:#aaa;border-width:0px 1px;border-radius: 20px 20px 0px 0px;-moz-border-radius: 20px 20px 0px 0px;-webkit-border-radius: 20px 20px 0px 0px;-khtml-border-radius: 20px 20px 0px 0px;'><b>New Parts:</b></div></span>
                </th>
                <th>
                    
                    <span><div style='background-color:#4790d4;padding:10px;border-style:solid;border-color:#aaa;border-width:0px 1px;border-radius: 20px 20px 0px 0px;-moz-border-radius: 20px 20px 0px 0px;-webkit-border-radius: 20px 20px 0px 0px;-khtml-border-radius: 20px 20px 0px 0px;'><b>Return / Old Parts:</b></div></span>
                </th>
            </tr>
<?
$i = 0;
if (!empty($parts)) {
	foreach ($parts as $part) {
	    $i++;
		if(!empty($part['id'])) {
		    $pId = $part['id'];
            echo '<tr id="'.$pId.'" valign="top" name="partEx">';
            if (is_object($part->NewPart)) {
?>
<td class="<?if($i%2==0)echo'trblue';else echo'tryellow';?>" >
    <div id="partExist<?echo$pId?>">
    <table cellpadding="4">
	<colgroup width="82"></colgroup>
	<colgroup width="126"></colgroup>
	<colgroup width="76"></colgroup>
	<colgroup width="126"></colgroup>
        <tr>
            <td>Part#:</td>
            <td><input name="PartNum" type="text" value="<?echo (string)$part->NewPart->PartNum;?>" /></td>
            <td>Model#:</td>
            <td><input name="ModelNum" type="text" value="<?echo (string)$part->NewPart->ModelNum;?>" /></td>
        </tr>
		<tr>
			<td>Serial#:</td>
            <td><input name="SerialNum" type="text" value="<?echo (string)$part->NewPart->SerialNum;?>" /></td>
        
            <td>Est.Arrival:</td>
            <td><input name="ETA" class="datepicker" type="text" value="<?echo (string)$part->NewPart->ETA;?>"  /></td>
        </tr>
        <tr> 
		<td>Carrier:</td>
            <td><select name="Carrier" id="Carrier<?echo$pId?>" >
                <option value="Fedex" <?if((string)$part->NewPart->Carrier=='Fedex')echo'selected';?>>FedEx</option>
                <option value="UPS" <?if((string)$part->NewPart->Carrier=='UPS')echo'selected';?>>UPS</option>
                <option value="USPS" <?if((string)$part->NewPart->Carrier=='USPS')echo'selected';?>>USPS</option>
                <option value="DHL" <?if((string)$part->NewPart->Carrier=='DHL')echo'selected';?>>DHL</option>
                <option value="Pilot" <?if((string)$part->NewPart->Carrier=='Pilot')echo'selected';?>>Pilot</option>
                <option value="Ceva" <?if((string)$part->NewPart->Carrier=='Ceva')echo'selected';?>>Ceva</option>
                <option value="AIT" <?if((string)$part->NewPart->Carrier=='AIT')echo'selected';?>>AIT</option>
                </select>
            </td>
			<td>Tracking#:</td>
            <td><input name="TrackingNum" type="text" value="<?echo (string)$part->NewPart->TrackingNum;?>" /></td>
        </tr>
        <tr>
            <td>Info:</td>
            <td colspan="3"><input name="Description" style="width:326px;" type="text" value="<?echo (string)$part->NewPart->Description;?>" /></td>
        </tr>
        <tr>
            <td>Ship to:</td>
            <td><select name="ShipTo" id="shipTo<?echo$pId?>"  onChange="shippingAddressEnabled(<?echo$pId?>);">
                <option value="Site" <?if((string)$part->NewPart->ShipTo=='Site')echo'selected';?>>Site</option>
                <option value="Tech" <?if((string)$part->NewPart->ShipTo=='Tech')echo'selected';?>>Tech</option>
                <option value="Fedex Hold" <?if((string)$part->NewPart->ShipTo=='Fedex Hold')echo'selected';?>>Fedex Hold</option>
                <option value="UPS Store Hold" <?if((string)$part->NewPart->ShipTo=='UPS Store Hold')echo'selected';?>>UPS Store Hold</option>
                <option value="USPS Hold" <?if((string)$part->NewPart->ShipTo=='USPS Hold')echo'selected';?>>USPS Hold</option>
                <option value="Other" <?if((string)$part->NewPart->ShipTo=='Other')echo'selected';?>>Other</option>
                </select>
            </td>
            <td colspan="2">
                <input name="ShippingAddress" type="text" id="shippingAddress<?echo$pId?>" style="width:198px;" value="<?echo (string)$part->NewPart->ShippingAddress;?>" <?if((string)$part->NewPart->ShipTo!='Other')echo'disabled';?>/>
            </td>
        </tr>
        <tr>
            <td>Consumable: </td>
			<td><input id="consumable<?echo$pId?>" name="Consumable" onChange="consumable(this,<?echo$pId?>); return false;" type="checkbox" <?if((string)$part->Consumable=='True')echo'checked';?> value="1" /></td>
        <td style="color:#000000;">Delete part: </td>
		<td><input id="DeletePart<?echo$pId?>" name="DeletePart" onChange="deletePart(this,<?echo$pId?>); return false;" type="checkbox" /></td>
		</tr>
      
    </table>
    </div>
</td>
<?          }
            $isPart = $part->ReturnPart->PartNum;
?>
<td class="<?if($i%2==0)echo'trblue';else echo'tryellow';?>" width="50%">
    <div id="partReturn<?echo$pId?>" style="<?if(!$isPart)echo'color:#999999;'?>">
    <table cellpadding="4" >
	<colgroup width="82"></colgroup>
	<colgroup width="126"></colgroup>
	<colgroup width="76"></colgroup>
	<colgroup width="126"></colgroup>
        <tr>
            <td>Part#:</td>
            <td><input <?if(!$isPart)echo'disabled'?> name="returnPartNum" type="text" value="<?echo (string)$part->ReturnPart->PartNum;?>" /></td>
            <td>Model#:</td>
            <td><input <?if(!$isPart)echo'disabled'?> name="returnModelNum" type="text" value="<?echo (string)$part->ReturnPart->ModelNum;?>" /></td>
		</tr>
		<tr>
            <td>Serial#:</td>
            <td class="tRight"><input <?if(!$isPart)echo'disabled'?> name="returnSerialNum" type="text" value="<?echo (string)$part->ReturnPart->SerialNum;?>" /></td>
        
            <td>Est.Arrival:</td>
            <td><input id="123" <?if(!$isPart)echo'disabled'?> name="returnETA" class="datepicker" type="text" value="<?echo (string)$part->ReturnPart->ETA;?>" /></td>
         </tr>
        <tr>  
			<td>Carrier:</td>
            <td><select  <?if(!$isPart)echo'disabled'?> name="returnCarrier" id="returnCarrier<?echo$pId?>">
                <option value="Fedex" <?if((string)$part->ReturnPart->Carrier=='Fedex')echo'selected';?>>FedEx</option>
                <option value="UPS" <?if((string)$part->ReturnPart->Carrier=='UPS')echo'selected';?>>UPS</option>
                <option value="USPS" <?if((string)$part->ReturnPart->Carrier=='USPS')echo'selected';?>>USPS</option>
                <option value="DHL" <?if((string)$part->ReturnPart->Carrier=='DHL')echo'selected';?>>DHL</option>
                <option value="Pilot" <?if((string)$part->ReturnPart->Carrier=='Pilot')echo'selected';?>>Pilot</option>
                <option value="Ceva" <?if((string)$part->ReturnPart->Carrier=='Ceva')echo'selected';?>>Ceva</option>
                <option value="AIT" <?if((string)$part->ReturnPart->Carrier=='AIT')echo'selected';?>>AIT</option>
                </select>
            </td>
			<td>Tracking#:</td>
            <td><input <?if(!$isPart)echo'disabled'?>  name="returnTrackingNum" type="text" value="<?echo (string)$part->ReturnPart->TrackingNum;?>" /></td>
        </tr>
        <tr>
            <td>Info:</td>
            <td colspan="3"><input <?if(!$isPart)echo'disabled'?> name="returnDescription" type="text"  value="<?echo (string)$part->ReturnPart->Description;?>" style="width:330px;"/></td>
        </tr>
        <tr>
            <td>RMA:</td>
            <td colspan="3"><input <?if(!$isPart)echo'disabled'?> name="returnRMA" type="text"  value="<?echo (string)$part->ReturnPart->RMA;?>" style="width:330px;"/></td>
        </tr>
        <tr>
            <td>Return instructions:</td>
            <td colspan="3"><input <?if(!$isPart)echo'disabled'?> name="returnInstructions" type="text" value="<?echo (string)$part->ReturnPart->Instructions;?>" style="width:330px;"/></td>
        </tr>
    </table>
    </div>
</td>
<?
            echo "</tr>";
	    }
	}
} else {
?>
        <tr id="newPartLine" name="partNew" valign="top">
            <td class="<?if($i%2==0)echo'tryellow';else echo'trblue';?>">
                <div id="partExistNew">
                <table cellpadding="4" >
                    <tr>
                        <td>Part#:</td>
                        <td><input tabindex="1" id="newPartNumber" name="PartNum" onChange="newPartReturnAutoFill('Number');" onKeyUp="newPartReturnAutoFill('Number');" type="text" /></td>
                        <td>Model#:</td>
                        <td><input id="newPartModel" name="ModelNum" onChange="newPartReturnAutoFill('Model');" onKeyUp="newPartReturnAutoFill('Model');" type="text" /></td>
                    </tr>
                    <tr>  
						<td>Serial#:</td>
                        <td><input id="newPartSerial" name="SerialNum" type="text" /></td>
                    
                        <td>Est.Arrival:</td>
                        <td><input class="datepicker" name="ETA" type="text" /></td>
                    </tr>
					<tr>
						<td>Carrier:</td>
                        <td><select id="newPartCarrier" name="Carrier" onChange="newPartReturnAutoFill('Carrier');" ><option value="Fedex">FedEx</option><option value="UPS">UPS</option><option value="USPS">USPS</option><option value="DHL">DHL</option><option value="Pilot">Pilot</option><option value="Ceva">Ceva</option><option value="AIT">AIT</option></select></td>
						<td>Tracking#:</td>
                        <td><input id="newPartTracking" name="TrackingNum" type="text" /></td>
                    </tr>
                    <tr>
                        <td>Info:</td>
                        <td colspan="3"><input id="newPartDescription" name="Description" onChange="newPartReturnAutoFill('Description');" onKeyUp="newPartReturnAutoFill('Description');" style="width:330px;" type="text" /></td>
                    </tr>
                    <tr>
                        <td>Ship to:</td>
                        <td><select name="ShipTo"  onChange="shippingAddressEnabled('New');" id="shipToNew"><option value="Site">Site</option><option value="Tech">Tech</option><option value="Fedex Hold">Fedex Hold</option><option value="UPS Store Hold">UPS Store Hold</option><option value="USPS Hold">USPS Hold</option><option value="Other">Other</option></select></td>
                        <td colspan="2">
                            <input type="text" name="ShippingAddress" id="shippingAddressNew" value=""  disabled style="width:198px;"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Consumable:</td>
						<td><input id="consumableNew" name="Consumable" value="1" onChange="consumable(this,'New'); return false;" type="checkbox"/></td>
						<td style="color:#000000;">Delete part:</td>
						<td><input name="DeleteNewPart" id="DeletenewPartLine" onChange="deletePart(this,'New'); return false;" type="checkbox" /></td>
					</tr>
                </table>
                </div>
            </td>
            <td class="<?if($i%2==0)echo'tryellow';else echo'trblue';?>" >
                <div id="partReturnNew">
                <table cellpadding="4">
                    <tr>
                        <td>Part#:</td>
                        <td><input name="returnPartNum" id="newPartReturnNumber" type="text" /></td>
                        <td>Model#:</td>
                        <td><input name="returnModelNum" id="newPartReturnModel" type="text" /></td>
                    </tr>
                    <tr>    
						<td>Serial#:</td>
                        <td><input name="returnSerialNum" id="newPartReturnSerial" type="text" /></td>
                    
                        <td>Est.Arrival:</td>
                        <td><input name="returnETA" class="datepicker" type="text" /></td>
                    </tr>
					<tr>
						<td>Carrier:</td>
                        <td><select name="returnCarrier" id="newPartReturnCarrier"><option value="Fedex"/>FedEx<option value="UPS">UPS</option><option value="USPS">USPS</option><option value="DHL">DHL</option><option value="Pilot">Pilot</option><option value="Ceva">Ceva</option><option value="AIT">AIT</option></select></td>
						<td>Tracking#:</td>
                        <td><input name="returnTrackingNum" id="newPartReturnTracking" type="text" /></td>
                    </tr>
                    <tr>
                        <td>Info:</td>
                        <td colspan="3"><input name="returnDescription" type="text" style="width:322px;" id="newPartReturnDescription" /></td>
                    </tr>
                    <tr>
                        <td>RMA:</td>
                        <td colspan="3"><input name="returnRMA" type="text" id="newPartReturnInfo" style="width:322px;"/></td>
                    </tr>
                    <tr>
                        <td>Return instructions:</td>
                        <td colspan="3"><input name="returnInstructions" type="text" id="shippingAddress" style="width:322px;" value="" /></td>
                    </tr>
                </table>
                </div>
            </td>
        </tr>
<?}?>
        <tr id="addNewPartButton1">
			<td>
				<div style="float:left;">
					<div class='mapperServiceButtotn'><a href='' onClick="addNewPart(2, <?if($i)echo $i;else echo'1';?>); return false;">Add Another Part</a></div>
				</div>
			</td>
			<td>
				<div style="float:right;">
					<div class='mapperServiceButtotn'><a href='' onClick="updateParts(<?echo $woId?>); return false;">Save All Changes and Close</a></div>
					<div class='mapperServiceButtotn'><a href='' onClick="return confirmation();">Close without Saving Changes</a></div>
				</div>
			</td>
		</tr>
      
    </table>
    <input id="pmContext" type="hidden" name="pmContext" value="<?echo trim($_GET['v']);?>"/>
    <input id="woId" type="hidden" name="woId" value="<?echo trim($_GET['woId']);?>"/>
    </div>
    </div>
</body>
</html>
<?function formatData($data){return strip_tags(trim($data));}?>
