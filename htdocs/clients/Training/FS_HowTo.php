<?php $page = clients; ?>
<?php $option = howto; ?>
<?php 	include ("../../headerSimple.php");?>
<?php 

    if($isFLSManager){
        $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
    } else {
        $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
    }
    
?>

<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>
<?php require ("../includes/adminCheck.php"); ?>


<!-- Add Content Here -->

<br /><br />

<div align="center">

<TABLE cellSpacing=0 cellPadding=0 width=700 border=0 style="text-align:left">
<TBODY>
<TR>
<TD colSpan=2><BR>
<FONT face=Arial size=4><B>How-To Instructions</B></FONT> 
<BR>

<P>
<B>1</B>) <a href="How a Work Order Flows.pdf" target="_blank">How a Work Order Flows</a>
<P>
<b>2)</b> <a href="How to Create a Work Order.pdf" target="_blank">How to Create a Work Order</A>
<P>
<b>3)</b> <a href="How to Approve a Work Order.pdf" target="_blank">How to Approve a Work Order</A>
<br>
<b>&nbsp;&nbsp;&nbsp;a)</b> <a href="../../content/Approvals_Training.pdf" target="_blank">New Approvals Process User Training</a>  </p>
<B>4</B>) <a href="How to Find Technicians.pdf" target="_blank">  How to Find Technicians</a>
<P>
<B>5</B>) <a href="Preference&amp;Performance FAQ  April 2 2010.pdf" target="_blank">Preference &amp; Performance Technician/Event ratings</A>
<P>
<B>6</B>) <a href="How to Seach for a Work Order.pdf" target="_blank">How to Seach for a Work Order</A>
<P>
<B>7</B>) <a href="How to View Applicants for a work order.pdf" target="_blank">How to View Applicants for a work order</A>
<P>
<B>8</B>) <a href="How to Review Reports.pdf" target="_blank">How to Review Reports</A></TD>
</TR>
<TR>
  <TD colSpan=2>&nbsp;</TD>
</TR>
</TBODY></TABLE>



</div>

<!--- End Content --->
<?php require ("../../footer.php"); ?>
		
