<?php
	require_once("../headerSimple.php");

	if (!empty($_GET["sync"])) {
		require_once("{$_SERVER['DOCUMENT_ROOT']}/library/bootstrapAPI.php");
		Core_Database_CaspioSync::syncBG(TABLE_CLIENT_PROJECTS, "Project_ID", array($_GET["id"]));
	}
	if (!empty($_GET["set"])):
		require_once("{$_SERVER['DOCUMENT_ROOT']}/library/projectTimeStamps.php");
		ini_set("display_errors",1);
		if (!empty($_GET["id"]) && !empty($_GET["user"]))
			createProjectTimeStamp($_GET["id"], $_GET["user"]);
?>
		<script type="text/javascript">window.close();</script>
<?php
	else:
	if (empty($_GET["ts"])):
?>
		<script type="text/javascript">window.close();</script>
<?php
		die();
	endif;

?>
<style type="text/css">
	#optionBox, #waitMessageBox {
		width: 90%;
		text-align: center;
		margin: 0px auto;
	}
	#optionMessage, #waitMessage {
		margin: 90px 0px 20px 0px !important;
		font-size: 15px;
		font-weight: bold;		
	}
	#optionBox div, #waitMessageBox div {
		margin: 5px 0px;
	}
	
	.cbButton
	{
		/*Back Button Attributes*/
		color: #ffffff;
		font-size: 12px;
		font-family: Verdana;
		font-style: normal;
		font-weight: bold;
		text-align: center;
		vertical-align: middle;
		border-color: #5496C6;
		border-style: solid;
		border-width: 1px;
		background-color: #032D5F;
		/* Forced by default to add space between buttons */
		width: auto;
		height: auto;
		margin: 0 3px;
	}
	
	.cbButton:hover {
		background-color: #234D7F;
	}
</style>
<script type="text/javascript">
	var waitingForUpdate = true;
	var checkUpdateTimer = null;
	var oldSDT = "<?=$st?>";
	checkUpdateTimer = setInterval("checkUpdateFinished()", 50);

	function checkUpdateFinished() {
		try {
			if (oldSDT == opener.serverDateTime) return; // havan't loaded new page yet
			if (!(test = opener.projectViewResults))
				if (test = opener.projectViewDetails) {
					// form error
//					window.close();
					return;
				}
				else
					return;
			clearInterval(checkUpdateTimer);
		} catch (e) {
			return;
		}
		updateFinished();
	}
	function updateFinished() { 
		window.location.replace("<?=$_SERVER['PHP_SELF']?>?<?=empty($_GET["copyOnly"]) ? "set=1" : "copyOnly=1"?>&id=<?=$_GET["id"]?>" + "&user=<?=$_GET["user"]?>&sync=1");
	}
</script>
    <body>
        <div id="waitMessageBox">
            <div id="waitMessage">
            	Updating ... please wait.<br/><br/>
                <img src="../images/loading.gif"/>
            </div>
        </div>
    </body>
</html>
<?php
	endif;
?>
