<?php
header('Content-Type:text/html; charset=UTF-8');
$page = 'clients';
$option = 'projects';
$selected = 'projectsView';

$prj_id = $_GET['project_id'];
$cp = $_GET['cp'];
if(empty($prj_id)) header( 'Location: /clients/projectsView.php');

require ("../header.php");

$_SERVER['HTTPS'] = ( !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ) ? 'on' : 'off';
$loggedIn   = ( !empty($_SESSION['loggedIn']) ) ? $_SESSION['loggedIn'] : NULL ;
$Company_ID = ( !empty($_SESSION['Company_ID']) ) ? $_SESSION['Company_ID'] : NULL;

if(empty($prj_id)) header( 'Location: /clients/projectsView.php?v='.$Company_ID);

if($loggedIn!="yes"){
    header( 'Location: http'.($_SERVER['HTTPS']=='on' ? 's':'').'://'.$_SERVER['HTTP_HOST'].'/clients/logIn.php' ); // Redirect to logIn.php
}

    $Company_ID   = ( !empty($_GET['v']) ) ? $_GET['v'] : $_SESSION['Company_ID'];
    $UserType     = ( !empty($_SESSION['UserType']) ) ? $_SESSION['UserType'] : NULL;
    $isFLSManager = (strtolower($Company_ID) == 'fls' && (strtolower($UserType)==='install desk' || strtolower($UserType)==='manager'));
    if($isFLSManager){
        $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
    } else {
        $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
    }

require ("../navBar.php");
require ("includes/adminCheck.php");

$displayPMTools = true;
require_once("includes/PMCheck.php");
//require_once("../library/recruitmentEmailLib.php");
require_once("../library/caspioAPI.php");
?>
<script type="text/javascript">
    window._company = '<?=$_GET['v']?>';
    //  use for load images and save it in browser cache only!!!
    var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
    var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";
    var formType = 'update';
</script>

<!--<script type="text/javascript" src="/widgets/js/jquery.min.js"></script>-->
<!--<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>-->
<?php script_nocache ("/widgets/js/FSWidget.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/FSWidgetProjectDetails.js?08272012"></script>-->
<?php script_nocache ("/widgets/js/FSWidgetProjectDetails.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>-->
<?php script_nocache ("/widgets/js/FSPopupAbstract.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>-->
<?php script_nocache ("/widgets/js/FSPopupRoll.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/functions.js"></script>-->
<?php script_nocache ("/widgets/js/functions.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>-->
<?php script_nocache ("/widgets/js/ajaxfileupload.js"); ?>
<?php script_nocache ("/widgets/js/FSMultiselectControl.js"); ?>
<link rel="stylesheet" href="/widgets/css/popup.css" type="text/css" />
<script type="text/javascript" src="/widgets/js/jquery.formatCurrency-1.4.0.js"></script>
<link rel="stylesheet" href="../library/ui.datepicker.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript" src="../library/jquery/jquery.bt.min.js"></script>


<script type="text/javascript">
function clientShowField() {
	return $("input#cbParamVirtual2");
}
function projectShowField() {
	return $("input#InsertRecordShowOnReports");
}
function addCustomReminderCall()
{
    if ($('#CustomReminderCallTr').is(':hidden'))
    {
        $("#CustomReminderCallTr").css('display','table-row');
        $("#ReminderCustomHr").css('display','inline');
        $("#lblReminderCustomHr").css('display','none');        
    }
    else if ($('#CustomReminderCallTr_2').is(':hidden'))
    {
        $("#CustomReminderCallTr_2").css('display','table-row');
        $("#ReminderCustomHr_2").css('display','inline');
        $("#lblReminderCustomHr_2").css('display','none');        
    }
    else if ($('#CustomReminderCallTr_3').is(':hidden'))
    {
        $("#CustomReminderCallTr_3").css('display','table-row');
        $("#ReminderCustomHr_3").css('display','inline');
        $("#lblReminderCustomHr_3").css('display','none');        
    }    
    
    var customCount = 0;
    if ($('#CustomReminderCallTr').is(':visible'))
    {
        $("#ReminderCustomHr").css('display','inline');
        $("#lblReminderCustomHr").css('display','none'); 
        customCount++;       
    }    
    if ($('#CustomReminderCallTr_2').is(':visible'))
    {
        $("#ReminderCustomHr_2").css('display','inline');
        $("#lblReminderCustomHr_2").css('display','none');   
        customCount++;       
    }    
    if ($('#CustomReminderCallTr_3').is(':visible'))
    {
        $("#ReminderCustomHr_3").css('display','inline');
        $("#lblReminderCustomHr_3").css('display','none'); 
        customCount++;              
    }
    
    if(customCount==3)
    {
        $("#AddCustomReminderTr").css('display','none');
    }

}
var mainVar = 0;
var roll;
var detailsWidget;
var updateP2TWindow = null;
var updateOptionsWindow = null;
var autoSubmitAfterP2TAuthorize = false;
var acceptedP2T = false;
var backClicked = false;
var currentUser = <?php echo$_SESSION['login'];?>

$('document').ready(function(){
	var value = "N";
	if (clientShowField().attr("value") == "Yes")
	{
		value = "Y";
	}
	projectShowField().attr("value",value);

    roll = new FSPopupRoll();
    detailsWidget = new FSWidgetProjectDetails({container:'detailsContainer',tab:'update'},roll);
    //detailsWidget.show({params:{company:window._company,project_id:'<?php echo $prj_id?>'}});
     detailsWidget.show({params:{company:window._company,cp:'<?=$cp?>',project_id:'<?php echo $prj_id?>'}});
	
	$("#CreateWorkOrderBtn").unbind('click');
	$("#CreateWorkOrderBtn").click(function(event) {
            $.post("/widgets/dashboard/client/accessctl/", {
                rt:'iframe',
                attributeId:'45,2'
                }, 
                function (response) {
                    if(response=="1") {
		openPopup('/clients/createWO.php?v=<?=$Company_ID?>&project_id=<?=$prj_id?>');
                } 
                    else {
                        Attention(response);
                    }                        
});
});
});
</script>

<div align="center">
<br/>
<div id="detailsContainer"></div>
</div>

<?php require ("../footer.php"); ?>