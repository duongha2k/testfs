<?php $page = "clients"; ?>
<?php $option = "reports"; ?>
<?php $selected = 'MetricsReport'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php require ("includes/adminCheck.php"); ?>
<?php
	$displayPMTools = true;
	require_once("includes/PMCheck.php");
?>
<?php

	$id = $_GET["v"];

?>

<!--- Add Content Here --->
<br /><br />

<div align="center">
<br /><br />
<table width="400" border="1" cellspacing="5" cellpadding="0">
  <tr>
    <th nowrap="nowrap" scope="col" align="left">Project Management Reports</th>
    </tr>
  <tr>
    <td nowrap="nowrap">&nbsp;</td>
  </tr>
  <!--14019-->
  <?php if (($_GET['v'] == 'FLEX' ) ||($_GET['v'] == 'FATT' )):?>    
  <tr>
  
        <td nowrap="nowrap" align="left">
        <iframe id="ifrmDownloadSiteSurvey" name="ifrmDownloadSiteSurvey" style="height: 0px;width:0px;"></iframe>
        <form  method="post" enctype="multipart/form-data" target="ifrmDownloadSiteSurvey" id="frmDownload"  name="frmDownload"
              action="/widgets/dashboard/reports/download-site-survey">
            <a href="javascript:download();">Site Survey Report</a>
            
        </form> 
        </td>
        
        </tr>
  <?php endif;?>  
  <!---14019-->
    <!--871-->
    <tr>
        <td nowrap="nowrap" align="left">
        <!--896-->
        <a href="/clients/siteStatusReport.php?v=<?php echo $_GET["v"]?>">SLA Reports</a></td>
        <!--end 896-->
        </tr>
   <?php if (!empty($_SESSION['Auth_User']['isPM'])):?>    
    
    <!--end 871 -->
    <tr>
        <td align="left">
        <a href="/clients/createProjectMasterReport.php?v=<?php echo $_GET["v"]?>">Project Master Download</a>
        </td>
    </tr>
  <?php endif;?>
  <tr>
    <td nowrap="nowrap" align="left"><a href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=Project_Status_and_Deliverables_v6.prpt&ignoreDefaultDates=true&autoSubmit=false')">Project Status with Deliverables</a><!--<div align="center"><a href="workorderfinancials.php">Work Order Financials</a></div> --></td>
    </tr>
 
<?php if ($_GET['v'] != 'HXC' && $_GET['v'] != 'HXST'):?>    
  <tr>
    <td nowrap="nowrap" align="left"><!--<div align="center"><a href="workOrdersOOS.php">Out of Scope Work Orders</a></div> -->
      <a href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=ProjectBidderReport.prpt&autoSubmit=false')">Project Bidder Report</a></td>
    </tr>
<?php endif;?>
<?php if (!empty($_SESSION['Auth_User']['isPM'])):?>    
  <tr>
    <td nowrap="nowrap" align="left">
      <a href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=WorkOrderLeadTimeReport.prpt&autoSubmit=false')">Work Order Lead Time Report</a></td>
    </tr>
<?php endif;?>
	<tr>
    <td nowrap="nowrap" align="left"><!--<div align="center"><a href="workOrdersOOS.php">Out of Scope Work Orders</a></div> -->
      <a href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=MonthlyUtilizationReport.prpt&autoSubmit=false#name=MonthlyUtilizationReport.prpt&autoSubmit=false')">Monthly Utilization Report</a></td>
    </tr>
    <tr>
    <td nowrap="nowrap" align="left"><!--<div align="center"><a href="workOrdersOOS.php">Out of Scope Work Orders</a></div> -->
      <a attribid="36" href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=OutOfScope.prpt&ignoreDefaultDates=true&autoSubmit=false')">Out of Scope Report</a></td>
    </tr>
   
</table>



</div>
<script>
    
    var loaderVisible = false;
    function download()
    {
        
        showLoader();
        document.forms["frmDownload"].submit();
       
        
        setTimeout('getsite()', 2000);        
    }
    
    function getsite(){
      $.ajax({
          url: "/widgets/dashboard/reports/getdownloadsurveyreport",
          type: "POST",
          dataType: 'json',
          success: function(data) {
            if(data.status=="pending")
              setTimeout('getsite()', 2000);
            else
              hideLoader();
          }
      });
    }
    function showLoader(){
        if(!loaderVisible){
            loaderVisible = true;
            var content = "<div id='loader'>Loading...</div>";
            $("<div></div>").fancybox(
                {
                    'autoDimensions' : true,
                    'showCloseButton' :false,
                    'hideOnOverlayClick' : false,
                    'content': content
                }
            ).trigger('click');
        }
    }
    
    function hideLoader(){
         setTimeout (jQuery.fancybox.close, 500);
          loaderVisible = false;
    }
</script>


<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
