<?php
	if (isset($_GET["download"])) {
		require_once("../headerStartSession.php");
		require_once("../library/loginSessionChecks.php");
		checkClientLogin();
		// download attachment
		header("Content-disposition: attachment; filename=templateMain.csv");
		echo file_get_contents("templateMain.csv");
		die();
	}
	$page = "clients";
	$option = "wos";
	$selected = "importWO";
	
	require_once("../header.php");
	require_once("../navBar.php");
	require_once("includes/PMCheck.php");
	require_once("../library/caspioAPI.php");
	$companyID = caspioEscape($_GET["v"]);
	$importType = "Main";
	
?>
<style type="text/css">
	td { border:#385C7E solid medium; padding: 0px 5px; text-align: center; font-size:xx-small; }
	.errorCell { background-color: #FF8800; cursor: default; }
	#grayOutLoading {
		background-color: #999999;
		width: 100%;
		height: 100%;
		position: absolute;
		top: 0px;
		left: 0px;
		filter: alpha(opacity=50);
		opacity: 0.5;
		display: none;
	}
	#loadingMsg {
		margin: 25% auto;
		font-size: 20px;
		color: #FFFFFF;
	}
</style>

<div id="errorDiv" style="text-align: center; background-color: #FF0000; color: #FFFFFF; display: none; margin: 10px 0px; font-weight: bold;">Error</div>
<div style="text-align: center">
	<a href="<?=$_SERVER['PHP_SELF']?>?download=1">Download Import Template</a>
    <form id="uploadImport" name="uploadImport" method="post" onsubmit="showLoadingMsg()" action="<?php echo $_SERVER['PHP_SELF']?>?v=<?=$_GET["v"]?>" enctype="multipart/form-data">
<!--        Import type:
        <select name="importType">
            <option value="FLS" selected="selected">FLS</option>
            <option value="Main">Main</option>
        </select>-->
        <input name="importType" value="<?=$importType?>" type="hidden" />
        <br/>
        Upload your work orders (csv format and limit of 200 WO per file)<br/>
        <input name="importFile" type="file" size="50" /><br /><br />
        <input name="submit" type="submit" value="Submit" /><br /><br />
    </form>
    <div id="grayOutLoading">
    	<div id="loadingMsg">
        	Loading ...
        </div>
    </div>
</div>
<?php
//	ini_set("display_errors", 1);
	require_once("../library/importWOInclude_t2.php");
	
	parseWOImport();
?>

<script type="text/javascript">
	function showLoadingMsg() {
		$("#grayOutLoading").show();
	}
	if (<?=$errorCount?> > 0)
		document.getElementById("errorDiv").style.display = "";
</script>
