<?php
$page = 'clients';
$option = 'users';
$selected = 'usersList';
require_once '../headerStartSession.php';

if (strtoupper($_SESSION['tbAdminLevel']) != 'YES') { header("Location: ./"); die();}

$toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
if(!$isFLSManager) {
    $toolButtons[] = 'QuickAssign';
    $toolButtons[] = 'QuickApprove';
}
require ("../header.php");
require ("../navBar.php");
require ("includes/adminCheck.php");

$displayPMTools = true;
require_once("includes/PMCheck.php");
$company = ( !empty($_GET['v']) ) ? $_GET['v'] : $_SESSION['Company_ID'];
$project_id = $_GET['project_id'];
?>
<script type="text/javascript">companyID = window._company = '<?=$company?>';</script>
<!--<script type="text/javascript" src="/widgets/js/jquery.min.js"></script>-->
<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetSitelistDetails.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
<script type="text/javascript" src="/widgets/js/functions.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetTechSite.js"></script>
<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
<link rel="stylesheet" href="/widgets/css/popup.css" type="text/css" />
<script type="text/javascript" src="/widgets/js/jquery.formatCurrency-1.4.0.js"></script>

<script type="text/javascript">
var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";

var roll;
var detailsWidget;
var techSiteWidget;

$('document').ready(function(){
    roll = new FSPopupRoll();
    detailsWidget = new FSWidgetSitelistDetails({container:'detailsContainer',tab:'update'},roll);
    detailsWidget.show({params:{id:'<?php echo $_GET['id'];?>', Company_ID:'<?=$company?>', Project_ID:'<?=$project_id?>'}});
});

function showTechSite(element,siteNumber,siteName,projectID)
{    
    techSiteWidget = new FSWidgetTechSite({SiteNumber:siteNumber,ProjectID:projectID},roll,element);
    var companyID = '<?=$company?>';
    var width = 600;
    if(companyID=='FLS') width=700
    var opt = {};
        opt.width    = width;
        opt.height   = "";
        opt.paddingleft   = 0;
        opt.paddingTop   = 0;
        opt.title    = 'Technician for Site #' + siteNumber + ' - ' + siteName;
        opt.body     = "<div id='divShowTechsInSite'></div>";
    roll.showNotAjax(element, null, opt);
    techSiteWidget.doRequest();        
}

</script>

<div align="center">
<br/>
<div id="detailsContainer"></div>
</div>

<?php require ("../footer.php"); ?>