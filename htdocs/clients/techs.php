<?php
//require_once('function.debug.php');
// __debug(true);

 
$page = 'clients'; 
$option = 'techs'; 
$selected = 'find'; 

require_once ("../headerStartSession.php");
$isDash = !empty($_GET['isDash']);

require_once("../library/googleMapAPI.php");
	// check login
if ($_SESSION['loggedIn'] != "yes" || $_SESSION['loggedInAs'] != "client")
	header("Location: ./");
	if (!$isDash)
		require ("../header.php");
	else
		require '../headerSimple.php';


$companyID = ( !empty($_GET['v']) ) ? $_GET['v'] : $_SESSION['Company_ID'];

$Company_ID = ( !empty($_GET['v']) ) ? $_GET['v'] : $_SESSION['Company_ID'];

$isFLSManager = (strtolower($Company_ID) == 'fls' && (strtolower($UserType)==='install desk' || strtolower($UserType)==='manager'));

if($isFLSManager){
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
} else {
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
}

if (!$isDash)
require ("../navBar.php");
else
	$displayPMTools = false;

require ("includes/adminCheck.php");
require_once("includes/PMCheck.php");


$common = new Core_Api_CommonClass;
$categories = $common->getWOCategories();

$usStates = $common->getStatesArray('US');
$canadaStates = $common->getStatesArray('CA');
$mexicoStates = $common->getStatesArray('MX');

$CaymanIslandsStates = $common->getStatesArray('KY');
$BahamasStates = $common->getStatesArray('BS');
$BrazilStates = $common->getStatesArray('BR');
$EnglandStates = $common->getStatesArray('UK');

$countryOptions = $common->getCountries();
		
$usStatesHtml = "<option value=\"\">All States</option>";
foreach ($usStates->data as $code => $name) {
	$usStatesHtml .= "<option value=\"$code\">$name</option>";
}

$canadaStatesHtml = "<option value=\"\">All States</option>";
foreach ($canadaStates->data as $code => $name) {
	$canadaStatesHtml .= "<option value=\"$code\">$name</option>";
}

$mexicoStatesHtml = "<option value=\"\">All States</option>";
foreach ($mexicoStates->data as $code => $name) {
	$mexicoStatesHtml .= "<option value=\"$code\">$name</option>";
}

$CaymanIslandsStatesHtml = "<option value=\"\">All States</option>";
foreach ($CaymanIslandsStates->data as $code => $name) {
	$CaymanIslandsStatesHtml .= "<option value=\"$code\">$name</option>";
}

$BahamasStatesHtml = "<option value=\"\">All States</option>";
foreach ($BahamasStates->data as $code => $name) {
	$BahamasStatesHtml .= "<option value=\"$code\">$name</option>";
}

$BrazilStatesHtml = "<option value=\"\">All States</option>";
foreach ($BrazilStates->data as $code => $name) {
	$BrazilStatesHtml .= "<option value=\"$code\">$name</option>";
}

$EnglandStatesHtml = "<option value=\"\">All States</option>";
foreach ($EnglandStates->data as $code => $name) {
	$EnglandStatesHtml .= "<option value=\"$code\">$name</option>";
}
$countriesHtml = "<option value=\"\">All Countries</option>";
//14041
foreach ($countryOptions->data as $country) { 
  if($country->Code=='' || $country->Code==' '){
    $countriesHtml .= "<option value=\"$country->Code\" disabled='disabled'>$country->Name</option>";  
  }else{
    $countriesHtml .= "<option value=\"$country->Code\">$country->Name</option>";  
  }
	
}
//End 14041
if($_GET['search'] != ""){
	$rerunSearchQS = unserialize(gzuncompress(stripslashes(base64_decode(strtr($_GET['search'], '-_,', '+/=')))));
}

$cfgSite = Zend_Registry::get('CFG_SITE');

$googleKey = $cfgSite->get("site")->get("google_map_key");
$googleClientId = $cfgSite->get("site")->get("google_map_client_id");

//ini_set('display_errors',1);
//ini_set('error_prepend_string', "<div class='error'>");
//ini_set('error_append_string', "</div");
$resultsHtml = "";
$searchCriteria = "";
function getSubmittedOrDefaultField($name, $default = "") {
		return (isset($_POST[$name]) ? $_POST[$name] : $default);
}

function getVOPField($name, $default = "") {
    return (isset($_POST[$name]) ? $_POST[$name] : $default);
}
		
function getOrderCriteria($sortColumn, $order, $specialColumn, $defaultSort) {
		// returns SQL code for current sorted column or returns default code
		if ($sortColumn == "")
			return $defaultSort;
		$order = ($order == 1 ? "DESC" : "ASC");
		if (array_key_exists($sortColumn, $specialColumn))
			$sortCriteria = $specialColumn[$sortColumn] . " " . $order;
		else
			$sortCriteria = caspioEscape($sortColumn) . " " . $order;
		return $sortCriteria;
}
$clientID = $_SESSION['ClientID'];
$apiGPM = new  Core_Api_Class();
$isGPM = $apiGPM -> isGPM($clientID);
$isPM = $isGPM;

?>
<style type="text/css">
.container {
	width: 430px;
}
.welcomePoint {
	font-weight: bold;
}
.welcomePoint > div {
	font-weight: normal;
	margin-left: 30px;
}
hr {
	color: #5C5858;
	height: 1px;
}
.headerHr {
	color: #FFF;
	background: none repeat scroll 0 0 #FFF;
	margin-bottom: 0px;
}
.secsubhead {
	font-weight:bold;
	font-size: 11px;
	margin:0px;
}
/*735*/
.sechead {
	height:18px;
	background-color: #f2f2f2;
        font:10px/10px Verdana,Arial,Helvetica,sans-serif;
	font-weight:bold;
	cursor: pointer;
}
/*end 735*/
input {
}
label {
	margin-right:5px;
	margin-bottom:5px;
	display:inline-block;
	width:175px;
	padding:0;
	vertical-align:top;
	font-size: 10px;
}
.options {
	margin-left:5px;
	width:240px;
}
#ProximityZipCode {
	width:75px;
	margin-right:12px;
}
#ProximityDistance {
	width:150px;
}
#CityState {
	width:235px;
}
#FLSID {
	margin:0;
}
#FirstName {
	float:right;
	margin:0;
}
#LastName {
	float:right;
	margin:0;
}
#PrimaryEmail {
	float:right;
	margin:0;
}
form {
	float:left;
}
#searchcol {
	width:260px;
	background-color: #C6D9F1;
	border: solid 2px #DCE6F2;
	margin-top:2px;
}
#searchcat {
	margin:0;
	padding:0;
}
.toolSubmit {
	background-color: #032D5F;
	color: #FFFFFF;
	margin-top: 5px;
	width: 70px;
}
#emailBlast {
	text-align: center;
}
div#results {
		width:72%;
	float:left;
	margin-left:5px;
        /*735
	margin-top:10px;
        end 735*/
}
.verdana2 {
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size: small;
	clear: both;
}
.verdana2bold {
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size: small;
	font-weight: bold;
	clear: both;
}
.fieldGroup {
	padding-bottom: 5px;
	border-bottom: 2px solid #2A497D;
}
.formBox, .formBoxFooter {
	margin: 15px auto 0px auto;
	padding: 15px;
	width: 920px;
	color: #2A497D;
	font-size: 12px;
	border: 2px solid #2A497D;
}
.formBox input {
	font-size: 12px;
	height: 14px;
}
.formBoxFooter {
	background-color: #5091CB;
	text-align: center;
	margin: 0px auto 10px auto;
	padding: 5px 15px;
	border-top: none;
}
.formRow3, .formRow4, .formRow5, .formRow6 {
	width: 100%;
	clear: both;
}
.formRow3 div {
	width: 33%;
	float: left;
	padding: 0px;
	margin: 0px 0px 5px 0px;
}
.formRow4 div {
	width: 25%;
	float: left;
	padding: 0px;
	margin: 0px 0px 5px 0px;
}
.formRow5 div {
	width: 20%;
	float: left;
	margin-bottom: 5px;
}
.formRow6 div {
	width: 16%;
	float: left;
	margin-bottom: 5px;
}
.resultsTable {
	width: 80%;
	margin: 10px auto 0px auto;
	font: 10px/14px Verdana, Arial, Helvetica, sans-serif;
	float:left;
}
.resultsTable thead {
	text-align: center;
	color: #2A497D;
	border-bottom: 1px solid #000000;
	border-right: 1px solid #FFFFFF;
	border-top: 1px solid #000000;
	background-color: #5091CB;
	cursor: default;
	color: #FFFFFF;
}
.sortAble, #nextPageBtn, #prevPageBtn {
	cursor: pointer;
}
.sortAbleSelected {
	cursor: pointer;
	background-color: #FF9900;
	color: #FFFFFF;
	font-weight: bold;
}
.resultsTable tfoot {
	text-align: center;
	color: #FFFFFF;
	border: 1px solid #2A497D;
	background-color: #5091CB;
}
.resultsTable td {
	padding: 3px 5px;
	white-space: nowrap;
}
.resultsTable .evenRow {
	background-color: #FFFFEF;
	border-bottom: 1px solid #000000;
}
.resultsTable .oddRow {
	background-color: #F2F5F9;
	border-bottom: 1px solid #000000;
}
.resultsTable .preferred {
	background-color: #FFFF8C;
}
.resultsTable .highlighted {
	background-color: #C6D9F1;
}
.resultsTable .techGrey {
	background-color: #DEDEDE;
}
.resultsTable .techYellow {
	background-color: #FFFF8C;
}
.resultsTable .techRed {
	background-color: #FFBBBB;
}
.resultsTable .techGreen {
	background-color: #89FF89;
}
.searchControls {
	margin-top: 10px;
	text-align: center;
}
.noResultsDiv {
	margin-top: 10px;
	text-align: center;
}
#formlist {
	list-style-type:none;
}
ul#srskills input {
	width:20px;
}
.ui-slider-label {
	font-family: Arial, Helvetica, sans-serif;
	font-size:10px;
}
.ui-slider {
	margin-bottom:25px;
	width: 200px;
}
.ui-slider-horizontal {
	height: 0.5em;
}
#searchcat .scrolling {
	overflow-y: scroll;
}
#searchcat dd {
	display: none;
}
div#loader {
	width:100px;
	height: 100px;
	background:url(/widgets/images/loader.gif) no-repeat center #fff;
	text-align:center;
	padding:10px;
	font:normal 16px Tahoma, Geneva, sans-serif;
}
#resultsHeader {
	background: #FFF;
	text-align: left;
	color: #000;
	display: none;
}
#reqStar {
	color: rgb(255, 0, 0);
	font-size: 12px;
	font-family: Verdana;
	margin-left: 2px;
}
.indentFilter {
	margin-left: 10px;
}
#viewAssignWos, #viewAssignTechs {
	height: 100%;
	max-height: 900px;
	overflow-y: auto;
		width: 880px;
}
.viewAssignSectionOpen .toggle, .viewAssignSectionClose .toggle {
	width: 16px;
	height: 16px;
	float: left;
	margin-right: 5px;
	cursor: pointer;
}
.viewAssignSectionOpen .toggle {
	background: url("/widgets/images/arrows-up5.png") no-repeat scroll 0 0 transparent;
}
.viewAssignSectionClose .toggle {
	background: url("/widgets/images/arrows-down5.png") no-repeat scroll 0 0 transparent;
}
.viewAssignSectionClose #viewAssignWos, .viewAssignSectionClose #viewAssignTechs {
	display: none;
}
.viewAssignHeader {
	font-size: 15px;
	font-weight: bold;
}
ul.tabs a.techTabs {
	color: #000;
}
ul.tabs a.techTabs .current {
	color: #FFF;
}
.resultsTable hr {
	margin: 0 0 1px;
}
	.btnDisabledGreySmall{
		background: url("/widgets/images/button_disabled_sm.png") no-repeat scroll left top transparent;
    	border: medium none;
    	font: 10px/10px Verdana,Arial,Helvetica,sans-serif;
    	padding: 4px 0 8px;
    	width: 125px;
    	color: #808080;
	}
.frmbasictools li
{
    padding-bottom: 4px;
    padding-top: 4px;
}

.frmCablingExperience li
{
    padding-bottom: 4px;
    padding-top: 4px;
}
	
.frmTelephonyExperience li
{
    padding-bottom: 4px;
    padding-top: 4px;
}

.frmCablingTelephonyTools li
{
    padding-bottom: 4px;
    padding-top: 4px;
}
</style>
<script src="<?=empty($_SERVER['HTTPS']) ? 'http' : 'https'?>://maps.googleapis.com/maps/api/js?client=<?=$googleClientId?>&sensor=false&v=3.7" type="text/javascript"></script>
<script src="<?=empty($_SERVER['HTTPS']) ? 'http' : 'https'?>://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<link rel="stylesheet" href="<?=empty($_SERVER['HTTPS']) ? 'http' : 'https'?>://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/ui-lightness/jquery-ui.css" type="text/css" />
<!--script type="text/javascript" src="/widgets/js/FSWidget.js"></script-->
<?php script_nocache ("/widgets/js/FSWidget.js"); ?>
<link rel="stylesheet" type="text/css" href="../library/ratingSourceBreakdown.css" />
<script type="text/javascript" src="../library/techCallStats_SAT.js"></script>
<!--script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script-->
<?php script_nocache ("/widgets/js/FSPopupAbstract.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script-->
<?php script_nocache ("/widgets/js/FSPopupRoll.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSTechSchedule.js"></script-->
<?php script_nocache ("/widgets/js/FSTechSchedule.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSTechMapper.js"></script-->
<?php script_nocache ("/widgets/js/FSTechMapper.js"); ?>
<script type="text/javascript" src="/widgets/js/selectToUISlider.jQuery.js"></script>
<link rel="stylesheet" href="/widgets/css/ui.slider.extras.css" type="text/css" />
<script type="text/javascript" src="/widgets/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.easing.pack.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-mod.js"></script>
<link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" media="all" />
<link rel="stylesheet" href="/widgets/css/blueprint/screenWoGlobalStyles.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="/widgets/css/blueprint/print.css" type="text/css" media="print" />
<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.tinysort.js"></script>
<!--script type="text/javascript" src="/widgets/js/FSRateTool.js"></script-->
<?php script_nocache ("/widgets/js/FSRateTool.js"); ?>
<style type="text/css">
@import url(../library/ui.datepicker.css);
</style>
<script type="text/javascript" src="../library/ui.datepicker.js"></script>
<!--script type="text/javascript" src="/clients/mapping/js/MarkerClusterer.js"></script-->
<?php script_nocache ("/clients/mapping/js/MarkerClusterer.js"); ?>
<!--script type="text/javascript" src="/clients/mapping/js/PolygonPointDetect.js"></script-->
<?php script_nocache ("/clients/mapping/js/PolygonPointDetect.js"); ?>
<!--script type="text/javascript" src="/clients/mapping/js/infobubble.js"></script-->
<?php script_nocache ("/clients/mapping/js/infobubble.js"); ?>
<script type="text/javascript" src="/library/jquery/contentmenu/jquery.contextmenu.js"></script>
<link rel="stylesheet" href="/library/jquery/contentmenu/jquery.contextmenu.css" type="text/css" />

<script type="text/javascript">
    var schedule;
   
	var qs = "<?=$qs?>";
	var myName = "<?php echo $_SERVER['PHP_SELF']?>";
	var pmContext = "<?=$_GET['v']?>";
	var isPM = "<?=$isPM?>";

	$('body').data('currentResultPage', 1);
	
	$('body').data("sort", "<?=$sort?>");
	
	$('body').data("order", "<?=$order?>");
	
	$("body").data("clearSearch",false);

	$("body").data('currentSearchResults', "");
	$("body").data('currentQueryString', "");
	
	if(typeof LVDefaults === 'undefined'){
		var LVDefaultFunction = function(){this.addFieldClass();}
		var LVDefaults = {
		    onValid : LVDefaultFunction,
		    onInvalid : LVDefaultFunction
		}
	}

	if(typeof _menu === 'undefined'){
		 var _menu  = [{'Open new Tab/Window':function(menuItem,menu) { 
		    	var href = jQuery(this).attr("href");
		        if(typeof(href) != 'undefined' && jQuery.trim(href) != "") 
		        {
		        	if(href.indexOf('?') >= 0)
		            {    
		            	href += "&closing=1";      
		            }
		            else
		            {
		            	href += "?closing=1";  
		            }   
		            var uiTab = window.open(href); 
		        }
		    } }
		];
	}
	
	var zip  = '<?=$_GET['defaultZip']?>';

	var newSearch = '<?=$_GET['newSearch']?>';
	var fromNav = '<?=$_GET['navBar']?>';
	var v = '<?=$_GET['v']?>';
	var currTab = "list";
	var showWelcome = true;
	var mapLoaded = false;
	var currentTechsMapped = false;
	var goomap = "";
	var gooMarkers = [];

	//params from wo dashboard
	var dashboardWos = '<?=$_REQUEST['mapperWos']?>';
	var dashboardStartDate = '<?=$_REQUEST['start_date']?>';
	var dashboardEndDate = '<?=$_REQUEST['end_date']?>';
	var dashboardProjects = '<?=$_REQUEST['projects']?>';
	var dashboardRoute = '<?=$_REQUEST['route']?>';

	var rerunSearchQS = '<?=$rerunSearchQS?>';
	try {
		$('body').data('currentResultPage', parseInt($('body').data('currentResultPage'), 10));
		
	}
	catch (e) {}

	function assignApplicantFromBidders(){
		techMapper.mapWorkOrders(true);
	}

	function viewSchedule(url)
	{
		newwindow=window.open(url,'name','height=500, width=500, left=100, top=100, resizable=yes, scrollbars=yes, toolbar=no,status=no');
		if (window.focus) {newwindow.focus()}
	}

	function techRatings(url)
	{
 		newwindow=window.open(url,'name','height=500, width=650, left=100, top=100, resizable=yes, scrollbars=yes, toolbar=no,status=no');
		if (window.focus) {newwindow.focus()}
	}


	function updateSearchUrl() {
		setting = ($('body').data("sort") != "" ? "sort=" + $('body').data("sort") + "&order=" + $('body').data("order") : "");
		return setting + "&" + qs;
	}

	function updateSearch() {
		frm = document.getElementById("searchForm");
		hideBanned = document.getElementById("hideBanned").checked;
		preferredOnly = document.getElementById("preferredOnly").checked;
		if (!hideBanned  || !preferredOnly) {
			settings = (hideBanned ? "" : "&sb=1");
			settings = settings + (preferredOnly ? "" : "&snp=1");
			frm.action = updateSearchUrl() + settings;
		}
		else
			frm.action = updateSearchUrl();
	}

	function newSearch() {
		frm = document.getElementById("searchForm");
		var queryString = /*"newSearch=1&" +*/ qs;
		runSearch(queryString);
	}

	function sortBy(column, isMapper) {
		if (column == $('body').data("sort")){
			var phpOrder = $('body').data("order");
			phpOrder == 0 ? order = 1 : order = 0;
		}else
                {
                //735
		if (column == "PreferencePercent" || column == "TotalWOs" || column == "ClientCredentials" || column == "PublicCredentials" || column == "FSexpert")
				order = 1;
			else
				order = 0;
                //end 735
		}
		$('body').data("sort", column);
		$('body').data("order", order);
		var queryString = "sort=" + column + "&order=" + order + "&" + qs;
		
		(isMapper==true) ? techMapper.loadViewTech(1, queryString) : runSearch(queryString);
	}

	function gotoPage(num) {
		var queryString = updateSearchUrl() + "&page=" + num + "&sort=" + $('body').data("sort") + "&order=" + $('body').data("order");
		$('body').data('currentResultPage', num);
		runSearch(queryString);
	}

	function prevPage() {
		gotoPage($('body').data('currentResultPage') - 1);
	}

	function nextPage() {
		gotoPage($('body').data('currentResultPage') + 1);
	}
    var callService = true;   
    
  //14077
  var _country = $("#Country").val(); 
  
  //14077  
	function runSearch(queryString){
      //14077
      var _requirePopUp = true;
      var _countryNow = $("#Country").val();
      if(_countryNow === 'US' || _countryNow === 'MX' || _countryNow === 'CA'){
         if(_country !== _countryNow){
           _requirePopUp = true;
         }else if(_country === _countryNow){
           _requirePopUp = false;
         }else{
           _requirePopUp = true;
         }
      }else{
        _requirePopUp = false;
      }
      //End 14077
      
			var submitData = $('#searchForm').serializeArray();
			var qs = "";
			var run = true;

			techMapper._loadMoreTechsShowing = "";
			techMapper._loadMoreTechsPage = "";
			
			if(techMapper._projectMapped === true){
				//project mapper has been used and user selects criteria from sidebar, redirect search to techMapper.mapTechsAroundWos
				techMapper.mapTechsAroundWos(true);
				return;
			}
			
			if($("#Country").val() != '' && ($("#Country").val() == 'US' || $("#Country").val() == 'CA' || $("#Country").val() == 'MX') && $("#ProximityZipCode").val() == '' && $("#state").val() == ''){
        run = false;
      } 

			var fnLen = 0;
			var lnLn = 0;
			try {
				fnLen = $("#FirstName").val().length;
				lnLen = $("#LastName").val().length;
			} catch (e) {
			}

			if(callService === true && $("#ProximityZipCode").val() == '' && $("#state").val() == '' && ($("#Country").val() == '' 
                            || $("#Country").val() == 'US' || $("#Country").val() == 'CA' || $("#Country").val() == 'MX')
                            && ($("#TechID").val() == '' && $("#PrimaryEmail").val() == '') 
                            && $("#PrimaryPhone").val() == ''
                            && fnLen < 3 && lnLen < 3  
                            && ("<?=$companyID?>" != 'FLS' || $("#FLSID").val() == '')){
                              //14077
                              if(_requirePopUp===false){
				                        $("<div></div>").fancybox({
					                        'width': 350,
					                        'height' : 125,
					                        'content' : "<h1 class='error'>Zip Code/Postal Code or State/Province is required.</h1>",
					                        'onComplete': function(){
										                        setTimeout(function(){
											                        $.fancybox.close(); 
										                        },1000);
					                        },
					                        'autoDimensions' : false,
					                        'centerOnScroll' : true
				                        }).trigger('click');
				                        run = false;
			                        }
                              //14077
                            }   
			
			for(i in submitData){
				if(submitData[i].value != "0" && submitData[i].name != "emailList"){
					tmpString = submitData[i].name +"="+submitData[i].value+"&";
					qs += tmpString;
				}
			}
			if (!queryString || queryString == "")
				queryString = "sort=" + $('body').data("sort") + "&order=" + $('body').data("order");
			if(queryString != "") qs += queryString;

			if(run === true && callService === true){
				showLoader();
				
				$.ajax({
					type: "POST",
					url: "/widgets/dashboard/client/client-tech-search/",
		        	dataType    : 'json',
		       	 	cache       : false,
					data: qs,
					success: function (data) {
						hideLoader();

						$("#resultsWelcome").hide();
						$("body").data('currentQueryString', qs);

						
						showWelcome = false;
						
                        //13570
                        if(data!=null) {
						                $("#listView").html(data.listView);
						                if(data.mapView != null && data.mapView != ""){
							                $(".seeMore").hide();
							                $("#projectMappingSeeMore").show();
							                $("#techsShowing").text(data.mapView.length);
							                $("#techsTotal").text(data.mapViewTotal);
							                
							                if(data.mapViewTotal >= 100){
								                $("#seeMoreTechs").show();
							                }else{
								                $("#seeMoreTechs").hide();
							                }
							                
							                techMapper.setTechPopups(data.mapView);
														                
							                if($("#viewAssign .mapHeaderLink").hasClass("currentMapLink")){
								                techMapper.viewAssign();
							                }
								                
							                $("#cmdFindMapTechs img").attr("src","/widgets/css/images/btn_orange_savesite.png");
							                
							                try {
								                techMapper._allEmailList = emailList;
								                $("#emailList").val(emailList);
							                } catch (e) {
								                $("#emailList").val("");
							                }
							                if( $("#preferredOnly").attr("checked") == true){
								                v = "";
								                newSearch = "";
							                }else{
								                $("#resultsHeader").hide();
							                }
						                }
                        }
                        else
                        {
                            $("#listView").html('No Results Found');
                        }
					},
					error: function () {
						hideLoader();
					}
				});
			}
      //14077
      _country = $("#Country").val();  
      //End 14077
	}

	var loaderVisible = false;
	function showLoader(){
		if(!loaderVisible){
			loaderVisible = true;
			var content = "<div id='loader'>Loading...</div>";
			$("<div></div>").fancybox(
				{
					'autoDimensions' : true,
					'showCloseButton' :false,
					'hideOnOverlayClick' : false,
					'content': content
				}
			).trigger('click');
		}
	}

	function hideLoader(){
		  //if(loaderVisible){
			 setTimeout (jQuery.fancybox.close, 500);
			  loaderVisible = false;
		  //}
	}
	
	function toggleChecks(me) {
		$("input[name=assignCheckBox]").attr('checked', $("#checkboxToggler").attr('checked'));
		if(!$(me).is(":checked")){
			$("input[name=techId]").each(function() {
				$(this).val("");
			});
	}
	}
	
	function highlightRow(me) {
		$(".highlighted").removeClass('highlighted');
		$(me).parent().parent().addClass('highlighted');
	}
	
	function assignTechToWo(me) {
		highlightRow(me);
		$("input[name=assignCheckBox]:checked").each(function() {
			win = $(this).val();
			$("#" + win + "_TechId").val($(me).val());
		});
		techMapper.updateBundleViewAssignButtons();
	}

	function checkIfTechChecked(me){
		if($(me).is(":checked")){
			$("input[name=assignTechRadioBtn]").each(function() {
				win = $(me).val();
				if($(this).is(":checked")){
					techId = $(this).val();
					$("input[name=assignCheckBox]").each(function(){
						if($(this).is(":checked")){
							$("#" + win + "_TechId").val(techId);
							techMapper.updateBundleViewAssignButtons();
						}
					});
				}
			});
		}else{
			win = $(me).val();
			$("#" + win + "_TechId").val("");
		}
	}
	
	function toggleViewAssignSection(me) {
		me = $(me).parent();
		isClosed = $(me).hasClass("viewAssignSectionClose");
		if (isClosed) {
			$(me).removeClass("viewAssignSectionClose");
			$(me).addClass("viewAssignSectionOpen");
		}
		else {
			$(me).removeClass("viewAssignSectionOpen");
			$(me).addClass("viewAssignSectionClose");
		}
		resizeViewAssign();
	}
	
	function resizeViewAssign() {
		isClosedWO = $(".viewAssignSectionClose #viewAssignWos").length == 1;
		isClosedTech = $(".viewAssignSectionClose #viewAssignTechs").length == 1;
		if (!isClosedWO && !isClosedTech) {
			$("#viewAssignWos").css('height', '100%').css('max-height', '500px');
			$("#viewAssignTechs").css('height', '100%').css('max-height', '500px');
		}
		else if (!isClosedWO && isClosedTech) {
			$("#viewAssignWos").css('height', '100%').css('max-height', '900px');
			$("#viewAssignTechs").css('height', '100%').css('max-height', '500px');
		}
		else if (isClosedWO && !isClosedTech) {
			$("#viewAssignWos").css('height', '100%').css('max-height', '500px');
			$("#viewAssignTechs").css('height', '100%').css('max-height', '900px');
		}
	}
	
	function assignTechCallback(data) {
		alert(data);
	}
	
	function confirmViewAssign() {
		info = assignTool.getMassAssignInfo();
		var techCount = {};
		techs = info['techs'];
		if (techs.length == 0) return;
		var winList = [];
		for (wo in techs) {
			// count number of times tech assigned to a work order
			try {
				if (techCount[techs[wo]] == undefined) techCount[techs[wo]] = 0;
				++techCount[techs[wo]];
			} catch (e) {
				techCount[techs[wo]] = 1;
			}
			winList.push(wo);
		}
		
		if (winList.length == 0) return;
		msg = [];
		for (id in techCount) {
			wo = "Work Order" + (techCount[id] > 1 ? "s" : "");
			msg.push(techCount[id] + " " + wo + " to:<br/><br/>FS-Tech ID#" + id + " - " + $("#techName_" + id).html());
		}
		
		message += "<div>";
		if (i > 1)
			message += "<div style='text-align: center'>You are assigning:</div>";
		else
			message += "You are assigning";
		
		for (i in msg) {
			message += msg[i] + "<br/>";
		}
		message += "<br/><div style='text-align: center'>Do you wish to continue assigning?<br/><br/><input id='assignWosBtn' type='button' class='link_button middle2_button' value='Cancel' onclick='$.fancybox.close()' />&nbsp;<input id='assignWosBtn' type='button' class='link_button middle2_button' value='Assign' onclick='assignTool.massassign()' /></div>";
		message += "</div>";
		roll.showAjax(this, event, {
			url: "/widgets/dashboard/client/bundle-assign/",
			width: 270,
			height: 150,
			title: "Bundle & Assign Work Orders",
			paddingTop: 50,
			data: {
				wosList: msg,
				wins: winList
	}
		});
	}

	function clearListFilters(){
		callService = false;
    	objRate.clearAll();
    	
		$("#searchForm :input").each(function(){
        	if($(this).attr("id") == "ProximityZipCode" || $(this).attr("id") == "state"){
            	// nothing
                  
        	}else{
				          if( $(this).is(":checkbox") == true && $(this).is(":checked") == true ){
                      $(this).attr("checked",false);
                  }
					          if($(this).is(":text") == true
                       && $(this).attr("id") != "ProximityZipCode"
                    	 && $(this).attr("id") != "state"){
                         $(this).val("");
                       }
                      
                    if($(this).attr("id") == "Country"){
                      //14077
                      $(this).value('US');
                      //End 14077
                    }
                    if($(this).attr("type") == "select-one" && $(this).attr("id") != "Country"
                    && $(this).attr("id") != "cbxselectSkill"){
							          if($(this).attr("id") != "ProximityDistance"){
								          $(this).val("0").trigger("change");
                           	          }else if(jQuery.trim(jQuery("#ProximityZipCode").val()) == ""){
								          $("body").data("clearSearch", true);
								          $(this).val("50").trigger("change");
							          }
						          }
          }
            
				});
		//$("#results").html("");
		$("body").data("clearSearch",false);
        callService = true;
        
        if(jQuery.trim(jQuery("#ProximityZipCode").val()) == "" && jQuery.trim(jQuery("#state").val()) == ""){
        	$("#resultsWelcome").html(jQuery("#welcomeContent").html());  
         }else{
            runSearch();
         }    
	}

	function blastTechs(){
		var listArr = $("#emailList").val().split(",");
		
		var href = "/Email_Form_Client.php";
		var wos = techMapper.getMappedWos();
		//14043
		if(wos != false){
			href += "?wos="+wos;
		}else{
		   href += "?ewo=1";  
		}
		//End 14043
		if(isPM || listArr.length < 500){
			if (!$("#emailList").val()) return;
			$.cookie("blastEmail", $("#emailList").val(), {path: '/'});
			$("<div></div>").fancybox(
			{
				'type' : 'iframe',
				'autoDimensions' : false,
				'width' : 700,
				'height' : 400,
				'transitionIn': 'none',
				'transitionOut' : 'none',
				'href': href
			}
			).trigger('click'); 
		}else{ 
			$("<div></div>").fancybox({
				'width': 350,
				'height' : 125,
				'content' : "<h1 class='error'>Maximum # of Emails is 500 Please refine your search.</h1>",
				'onComplete': function(){
									setTimeout(function(){
										$.fancybox.close(); 
									},1000);
				},
				'autoDimensions' : false,
				'centerOnScroll' : true
			}).trigger('click');
		}
	}
	
	var objRate = DetailRateTool;

	 $(document).ready(function() {
      techMapper = new FSTechMapper({mapContainer: "googleMap", companyID: '<?php echo $companyID?>'});
			
		 	schedule = new FSTechSchedule({width: 940, height: 450});
	        $("#techScheduleLnk").click(function (event) {
	            schedule.show($(this),null,{data: {tech_ID:'<?php echo $techID?>',date:"",date_interval: "all",display_calendar: 1,display_date: 1,sort: "starttime",sort_dir: "desc"}});
	        });

		    objRate.Onit({_class:'.selfRatingTool',_cbxselectSkill :'#cbxselectSkill',ContentID:'#srskills'});
		    objRate.OnitBuildEventChooseSkill();
		    objRate.AfterClickChange = function() {
		    	runSearch(); 
		    };

	        
			<?php if ($isDash):?>
				$("body").css("width", "800px");
				$("body").css("margin", "0");
				$("body").css("overflow", "hidden");
				$("#headerSlider").css("display", "none");
				$("#maintab").css("display", "none");
				$("#subSectionLabel").css("display", "none");
				$("#PMCompany").css("display", "none");
				$("table.form_table").css("margin", "0");
				$("table.form_table").css("padding", "0");
				$("table.form_table").css("width", "400px");
				$("div.footer_menu").css("display","none");
				$("#content").css("margin", "0");
				$("#tabcontent").css("display", "none");
				$("#searchForm").css("width", "800px");
				$("#mainContainer").css("width", "800px");
				$("#mainContainer").css("margin", "0");
			<?php endif;?>
				$("#SATPerformanceAvg").selectToUISlider({sliderOptions: {stop: function(e,ui) {if($("#SATPerformanceAvg").val() != "" && $("#SATPerformanceAvg").val() != null ){runSearch();}}}}).hide();
				$("#SATRecommendedAvg").selectToUISlider({sliderOptions: {stop: function(e,ui) {if($("#SATRecommendedAvg").val() != "" && $("#SATRecommendedAvg").val() != null ){runSearch();}}}}).hide();
				$("#Qty_IMAC_Calls").selectToUISlider({sliderOptions: {stop: function(e,ui) {if($("#Qty_IMAC_Calls").val() != "" && $("#Qty_IMAC_Calls").val() != null ){runSearch();}}}}).hide();
				$("#No_Shows").selectToUISlider({sliderOptions: {stop: function(e,ui) {if($("#No_Shows").val() != "" && $("#No_Shows").val() != null ){runSearch();}}}}).hide();
				$("#Back_Outs").selectToUISlider({sliderOptions: {stop: function(e,ui) {if($("#Back_Outs").val() != "" && $("#Back_Outs").val() != null ){runSearch();}}}}).hide();
				$("#ProximityDistance").selectToUISlider({labels: 7,sliderOptions: {stop: function(e,ui) {if($("#ProximityZipCode").val().length >= 5 && $("#ProximityDistance").val() != "" && $("#ProximityDistance").val() != null && $("body").data("clearSearch") == false){runSearch();}}} }).hide();
				$(".selfRating").each(function(){	
						$(this).selectToUISlider(
								{sliderOptions: {stop: 
													function(e,ui) {
															//if(ui.value >= 1){
																runSearch();
															//}
														}
												}
								}
						).hide();
				});

				if($("#searchcat").length > 0){
					$("#searchcat dt").click(function(){
						var sel = $(this);
						sel.addClass("act current");

						sel.parent().children("dd").each(function(){
							if($(this).is(":visible") && !$(this).prev("dt").hasClass("current")){
								if($(this).prev("dt").hasClass("act")){
									$(this).prev("dt").removeClass("act");
								}
								$(this).slideUp(300);
							}
						});

						sel.next().slideToggle(300, function(){
							if(!$(this).is(":visible")){
								$(this).prev("dt").removeClass("act");
							}
							sel.removeClass("current");
						});

						return false;
					});
				}
				
				$("#locationSection").click();
                                //735
				//$("#technicianSection").click();
				//end 735
				$("#Blast").click(function (event) {

					if(techMapper._polygon != "" && techMapper._polygon != null){
						roll.autohide(false);
                        var opt = {
                            popup       : 'emailblast',
                            title       : 'Send Email Blast',
                            width       : 250,
                            height		: 130,
                            top		 	: 350,
                            body     	: techMapper.emailBlastPopup(),
                            delay       : 0
                        }
                        roll.showNotAjax(this,event,opt);
					}else{
						blastTechs();
					}										
				});

				$("#highlightAreaContent .bundleInviteBtn").click(function (event) {
					var techs = techMapper.getTechMarkers("TechID");
					if (techs.length == 0) {
						techMapper.showNoTechInAreaMessage();
						return;
					}
					roll.showAjax(this, event, {
						url: "/widgets/dashboard/client/bundle-invite/",
						width: 270,
						height: 150,
						title: "Bundle and Invite Techs to Bid",
						paddingTop: 50,
						data: {
							wins: techMapper.getWOMarkers("WIN_NUM"),
							eList: techMapper.getTechMarkers("Email"),
							highlight: $(this).hasClass("bundleInviteHightlight") ? 1 : 0
						}
					});
				});
				$("#viewAssignContent .bundleInviteBtn").click(function (event) {
					var info = techMapper.getViewAssignInfo();
					wins = info['winsCheckedOnly'];
					if (wins.length == 0) return;
					roll.showAjax(this, event, {
						url: "/widgets/dashboard/client/bundle-invite/",
						width: 270,
						height: 150,
						title: "Bundle and Invite Techs to Bid",
						paddingTop: 50,
						data: {
							wins: wins,
							eList: techMapper.getTechMarkers("Email"),
							highlight: $(this).hasClass("bundleInviteHightlight") ? 1 : 0
						}
					});
				});
				$("#viewAssignContent .bundleAssignBtn").click(techMapper.confirmViewAssign);
				
				$('#searchForm').submit(function(event) {
					event.preventDefault();
					
					var submitData = $(this).serializeArray();
					var qs = "";
					for(i in submitData){
						if(submitData[i].value != "0"){
							tmpString = submitData[i].name +"="+submitData[i].value+"&";
							qs += tmpString;
						}
					}
					runSearch();
				});

				$(".onChange").each(function(){
					$(this).change(function(){
            //14150
            //14150
            
            if($(this).attr('id')=='PrimaryPhone'){
              var _phone = $(this).val();
              var _phone = _phone.replace ( /[^\d.]/g, '' ); 
              if(_phone.length==20){
                  _phone = _phone.replace(/[^0-9]/g, '');
                  _phone = _phone.replace(/(\d{3})(\d{3})(\d{4})(\d{10})/, "($1) $2-$3 $4");  
                  $('#PrimaryPhone').val(_phone);
                  runSearch();   
              }else if(_phone.length==10){
                  _phone = _phone.replace(/[^0-9]/g, '');
                  _phone = _phone.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");  
                  $('#PrimaryPhone').val(_phone);
                  runSearch();   
              }else if(_phone.length==11){
                  _phone = _phone.replace(/[^0-9]/g, '');
                  _phone = _phone.replace(/(\d{1})(\d{3})(\d{3})(\d{4})/, "$1-$2-$3-$4");  
                  $('#PrimaryPhone').val(_phone);
                  runSearch();   
              }else{
                 $('#PrimaryPhone').val('');
              }
            //End 14150
            }else if($(this).attr("id") == "ProximityZipCode" || $(this).attr("id") == "state"){
							if($("#ProximityZipCode").val().length >= 5 || $("#state").val() != ''){
								$("span[id*=reqStar]").hide();
								if(techMapper._projectMappingOption1 != true)runSearch();
							}else{
								$("span[id*=reqStar]").show();
							}
						}else if($(this).attr("id") == "Country"){
                //14077
               
                $("#ProximityZipCode").val("");
                //End 14077
                  
								//14041
                //14077
                $("#state").val("");  
								if($(this).val()=="US" ||  $(this).val()=="CA"  ||  $(this).val()=="MX" ){
								   $(".colState").show(); 
                    $("span[id*=reqStar]").show();    
								}else{
								   $(".colState").hide(); 
                    $("span[id*=reqStar]").hide();    
								}
                //End 14077
								//End 14041
							if($(this).val()=="US"){
								$("#state").html('<?=$usStatesHtml;?>');
							}else if($(this).val()=="CA"){
								$("#ProximityZipCode").val("");
								$("#state").html('<?=$canadaStatesHtml;?>');
							}else if($(this).val() =="MX"){
								$("#ProximityZipCode").val("");
								$("#state").html('<?=$mexicoStatesHtml;?>');
							}else if($(this).val() =="KY"){
								$("#ProximityZipCode").val("");
								$("#state").html('<?=$CaymanIslandsStatesHtml;?>');
							}else if($(this).val() =="BS"){
								$("#ProximityZipCode").val("");
								$("#state").html('<?=$BahamasStatesHtml;?>');
							}else if($(this).val() =="BR"){
								$("#ProximityZipCode").val("");
								$("#state").html('<?=$BrazilStatesHtml;?>');
							}else if($(this).val() =="UK"){
								$("#ProximityZipCode").val("");
								$("#state").html('<?=$EnglandStatesHtml;?>');
							}
							if(techMapper._projectMappingOption1 != true){
                runSearch(); 
              }
						}else{
							if($(this).val() != "" && $(this).val() != null && techMapper._projectMappingOption1 != true)
								runSearch();
						}
					});
				});	

				$("#USAuthorizedDummy").click(function() {
					$("#USAuthorized").attr('checked', $("#USAuthorizedDummy").attr('checked'));
				});

				$("#USAuthorized").click(function() {
					$("#USAuthorizedDummy").attr('checked', $("#USAuthorized").attr('checked'));
				});


				$(".checkboxOnChange").each(function(){
					$(this).change(function(){

							if($(this).attr("id") == "12months"){
								var run = false;
								if($(this).attr("checked") == true){
									$(".telephonyCheck").each(function(){
										if($(this).attr("checked") == true)
											run = true;
									});
								}
								if(run == true) runSearch();
							}
              else if($(this).attr("id") == "FStagBlueRibbonTechnician")
              {
                  if($("#FStagBlueRibbonTechnician").is(":checked"))
                      $("#BlueRibbonTechnician").attr("checked","checked");
                  else
                      $("#BlueRibbonTechnician").attr("checked","");
                      runSearch();
                  }
              else if($(this).attr("id") == "FStagBootCampCertified")
              {
                  if($("#FStagBootCampCertified").is(":checked"))
                      $("#BootCampCertified").attr("checked","checked");
                  else
                      $("#BootCampCertified").attr("checked","");
                  
                  runSearch();
              }
              else if($(this).attr("id") == "BlueRibbonTechnician")
              {
                  if($("#BlueRibbonTechnician").is(":checked"))
                      $("#FStagBlueRibbonTechnician").attr("checked","checked");
                  else
                      $("#FStagBlueRibbonTechnician").attr("checked","");
                  
                  runSearch();
              }
              else if($(this).attr("id") == "BootCampCertified")
              {
                  if($("#BootCampCertified").is(":checked"))
                      $("#FStagBootCampCertified").attr("checked","checked");
                  else
                      $("#FStagBootCampCertified").attr("checked","");
                  
                  runSearch();
              }
              else if($(this).attr("id") == "cbxselectLanguage")
              {   var el = this;
                  var languageSkillid = $(el).find("option:selected").val();
                  var skillexist = false;
                  $("#cbxselectLanguage").parent().parent().find("li").each(function(index) {
                      if($(this).attr("id")=='liLanguage_'+languageSkillid)
                          skillexist = true;
                  });
                  if(!skillexist && $(el).find("option:selected").val()!="")
              {
                      $("<li id='liLanguage_"+languageSkillid+"' style='display:;'>\
                          <input type='hidden' value='"+languageSkillid+"' name='languageSkillId[]' />\
                          <input class='checkboxOnChange2' checked type='checkbox' name='chklanguagecompetent_"+languageSkillid+"' value='1'>\
                              <label  style='width:200px' for='chklanguage'>"+$(el).find("option:selected").text()+" - Competent</label><br/>\
                          <input class='checkboxOnChange2' style='margin-left:15px;' type='checkbox' name='chklanguagefluent_"+languageSkillid+"' value='1'>\
                          <label for='chklanguage'>Fluent</label>\
                          </li>").insertBefore($(el).parent());
                  jQuery(".checkboxOnChange2").unbind("change");            
                  jQuery(".checkboxOnChange2").change(function(){
                      runSearch();
                  });   
                  runSearch();
              }
              }
              //14056
              
              else if($(this).attr("id") == "NACI")
              {                                                            
                     if($("#NACI").is(":checked"))
                      $("#NACILapsed").attr("checked","");
                     else
                      $("#NACILapsed").attr("checked","");                                                            
                      runSearch();                                                                                                                      
              }              
                    else if($(this).attr("id") == "NACILapsed")
                    {
                        if($("#NACILapsed").is(":checked"))
                            $("#NACI").attr("checked","checked");
                                                                          
                            runSearch();
                    }  
                    else if($(this).attr("id") == "InterimSecClear")
                    {
                        if($("#InterimSecClear").is(":checked"))
                            $("#InterimSecClearLapsed").attr("checked","");
                        else
                            $("#InterimSecClearLapsed").attr("checked","");                                                            
                                runSearch();
                    }            
                    else if($(this).attr("id") == "InterimSecClearLapsed")
                    {
                        if($("#InterimSecClearLapsed").is(":checked"))
                            $("#InterimSecClear").attr("checked","checked");
                                                                          
                                runSearch();
                    }   
                    else if($(this).attr("id") == "FullSecClear")
                    {
                        if($("#FullSecClear").is(":checked"))
                            $("#FullSecClearLapsed").attr("checked","");
                        else
                            $("#FullSecClearLapsed").attr("checked","");                                                            
                                runSearch();
                    }            
                    else if($(this).attr("id") == "FullSecClearLapsed")
                    {
                        if($("#FullSecClearLapsed").is(":checked"))
                            $("#FullSecClear").attr("checked","checked");
                                                                                 
                                runSearch();
                    }  
                    else if($(this).attr("id") == "TopSecClear")
                    {
                        if($("#TopSecClear").is(":checked"))
                            $("#TopSecClearLapsed").attr("checked","");
                        else
                            $("#TopSecClearLapsed").attr("checked","");                                                            
                                runSearch();
                    }            
                    else if($(this).attr("id") == "TopSecClearLapsed")
                    {
                        if($("#TopSecClearLapsed").is(":checked"))
                            $("#TopSecClear").attr("checked","checked");
                                                                              
                                runSearch();
                    }  
                    else if($(this).attr("id") == "TopSecSCIClear")
                    {
                    if($("#TopSecSCIClear").is(":checked"))
                        $("#TopSecSCIClearLapsed").attr("checked","");
                    else
                        $("#TopSecSCIClearLapsed").attr("checked","");                                                            
                            runSearch();
                    }         
                    else if($(this).attr("id") == "TopSecSCIClearLapsed")
                    {
                    if($("#TopSecSCIClearLapsed").is(":checked"))
                        $("#TopSecSCIClear").attr("checked","checked");
                   
                                                                                
                            runSearch();
                    }            
                    else{
                            runSearch();
                    }

					});
				});	
                                
				$("#clearSearch").click(function(event){
                 	event.preventDefault();
                 	if($("#mapperView").is(":visible") && techMapper._woMarkers.length >= 1){
                 		 roll.autohide(false);
                         var opt = {
                             popup       : 'clearfilters',
                             title       : 'Which filters are you clearing?',
                             width       : 250,
                             height		 : 130,
                             top		 : 350,
                             body     	 : techMapper.clearFiltersPopup(),
                             delay       : 0
                         }
                         roll.showNotAjax(this,event,opt);
                 	}else{
                     	//clearListFilters();
                       //14077 
                           $('#searchForm')[0].reset();
                           $("#Country").val('US');
                           if($("#Country").val()=="US"){
                                $("#state").html('<?=$usStatesHtml;?>');
                                $("span[id*=reqStar]").show(); 
                              }else if($("#Country").val()=="CA"){
                                $("#state").html('<?=$canadaStatesHtml;?>');
                                $("span[id*=reqStar]").show(); 
                              }else if($("#Country").val() =="MX"){
                                $("span[id*=reqStar]").show(); 
                                $("#state").html('<?=$mexicoStatesHtml;?>');
                              }else if($("#Country").val() =="KY"){
                                $("#state").html('<?=$CaymanIslandsStatesHtml;?>');
                              }else if($("#Country").val() =="BS"){
                                $("#state").html('<?=$BahamasStatesHtml;?>');
                              }else if($("#Country").val() =="BR"){
                                $("#state").html('<?=$BrazilStatesHtml;?>');
                              }else if($("#Country").val() =="UK"){
                                $("#state").html('<?=$EnglandStatesHtml;?>');
                              }
                              $("#state").val('');
                              $(".colState").show();
                        //End 14077
                  }
				});

				if(v != "" && newSearch == 1){
					if(zip != ""){
						$("#ProximityZipCode").val(zip);
						runSearch();
					}else{
						//$("#preferredOnly").attr("checked",true);
					}
					$("#navBarWelcomeText").show();
					//runSearch();
				}

				if(rerunSearchQS != ""){
					var keyValues = rerunSearchQS.split(/&/);
				
					$.each(keyValues,function(index,val){
						var key = val.split(/=/);
					    if(key[1] != "" && key[1] != null){
						    if($("#"+key[0]).attr("type") == "select-one"){
						    	$("#"+key[0]).val(key[1]).trigger("change");
						    }else if($("#"+key[0]).attr("type") == "checkbox"){
					    		$("#"+key[0]).attr("checked",true);
						    }else if($("#"+key[0]).attr("type") != "undefined"){
					    		$("#"+key[0]).val(key[1]);
						    }
					    }
					});
					//for EMC
                                        jQuery(".EMCcheckbox").each(function(){
                                            if(jQuery(this).is(':checked'))
                                            {
                                                jQuery(this).parent().show();
                                                var liid = jQuery(this).attr('id');
                                                jQuery("#cbxemccert").children("option[value='"+liid+"']").remove();
                                            }    
                                        });
                     //for BasicTools
                                        jQuery(".BasicToolscheckbox").each(function(){
                                            if(jQuery(this).is(':checked'))
                                            {
                                                jQuery(this).parent().show();
                                                var btid = jQuery(this).attr('id');
                                                jQuery("#cbxbasictools").children("option[value='"+btid+"']").remove();
                                            }    
                                        });
                     jQuery(".TelephonyToolscheckbox").each(function(){
                                            if(jQuery(this).is(':checked'))
                                            {
                                                jQuery(this).parent().show();
                                                var ttid = jQuery(this).attr('id');
                                                jQuery("#cbxTelephonyTools").children("option[value='"+ttid+"']").remove();
                                            }    
                                        });   
                     jQuery(".CablingExperiencecheckbox").each(function(){
                                            if(jQuery(this).is(':checked'))
                                            {
                                                jQuery(this).parent().show();
                                                var ceid = jQuery(this).attr('id');
                                                jQuery("#cbxCablingExperiencetools").children("option[value='"+ceid+"']").remove();
                                            }    
                                        });                                        
                      jQuery(".TelephonyExperiencecheckbox").each(function(){
                                            if(jQuery(this).is(':checked'))
                                            {
                                                jQuery(this).parent().show();
                                                var teid = jQuery(this).attr('id');
                                                jQuery("#cbxTelephonyExperience").children("option[value='"+teid+"']").remove();
                                            }    
                                        }); 
					runSearch(rerunSearchQS);
				}
				$(".techTabs").click(function(){
					currId = $(this).attr("id");
					if(currId == "listTechs"){
						$("#listTechs").addClass("current");
						$("#fsMapper").removeClass("current");
						$("#listView").show();
						$("#mapperView").hide();
						$("#dlCallList").hide();
						if(showWelcome == true) $("#resultsWelcome").show();
						currTab = "list";
					}else{
						$("#listTechs").removeClass("current");
						$("#fsMapper").addClass("current");
						$("#listView").hide();
						$("#mapperView").show();
						$("#dlCallList").show();
						if(mapLoaded === false){
							techMapper.initMap(dashboardWos);
							mapLoaded = true;
						}
						if($("body").data("currentSearchResults") != "" && $("body").data('currentQueryString') != "" && currentTechsMapped === false){
							techMapper.mapTechs($("body").data("currentSearchResults"), $("body").data('currentQueryString'));
							currentTechsMapped = true;
						}
						$("#resultsWelcome").hide();
						currTab = "map";
					}
				});		
				
				$(".mapHeaderLink").click(function(){
					currMapLinkId = $(".currentMapLink").parent().attr("id");
					$.each($(".mapHeaderLink"),function(){
						if(this.nodeName.toLowerCase() !== "a"){
						if($(this).hasClass("currentMapLink")){
							$(this).removeClass("currentMapLink");
						}
						}
						tmpId = $(this).parent().attr("id");
						if(tmpId == "viewAssign")$("#viewAssignView").hide();
						$("#"+tmpId+"Content").hide();
					});
					var currId = "";
					var showWoDropDown = false;
					if(this.nodeName.toLowerCase() == "a"){
						if($(this).attr("id") == "mapperLeftArrow"){
							switch(currMapLinkId){
								case "findMapTechs":
									currId = "mapWorkOrders";
									showWoDropDown = true;
									break;
								case "highlightArea":
									currId = "findMapTechs";
									break;
								case "viewAssign":
									currId = "highlightArea";
									break;
								default:
									currId = currMapLinkId;
							}
						}else{
							switch(currMapLinkId){
								case "mapWorkOrders":
									currId = "findMapTechs";
									break;
								case "findMapTechs":
									currId = "highlightArea";
									break;
								case "highlightArea":
									currId = "viewAssign";
									break;
								default:
									currId = currMapLinkId;
							}
						}
					}else{
					currId = $(this).parent().attr("id");
					}
					switch(currId){
						case "mapWorkOrders":
							$("#mapperLeftArrow").css('cursor', 'default').find("img").attr("src","/widgets/images/arrow_grey_left.png");
							if(techMapper._polygonClickListener != "" && techMapper._polygonClickListener != null && techMapper._polygonClickListener != undefined){
								google.maps.event.removeListener(techMapper._polygonClickListener);
								techMapper._polygonClickListener = null;
							}
							$("#mapWorkOrders").find("span").addClass("currentMapLink");
							$("#viewAssignView").hide();
							$("#"+currId+"Content").show();
							if(showWoDropDown == true)$("#mapWoSubmit").trigger("click");
							$("#googleMap").show();
							
							break;
						case "findMapTechs":
							$("#mapperLeftArrow").css('cursor', 'pointer').find("img").attr("src","/widgets/images/arrow_orange_left.png");
							$("#mapperRightArrow").css('cursor', 'pointer').find("img").attr("src","/widgets/images/arrow_orange_right.png");
							if(techMapper._polygonClickListener != "" && techMapper._polygonClickListener != null && techMapper._polygonClickListener != undefined){
								google.maps.event.removeListener(techMapper._polygonClickListener);
								techMapper._polygonClickListener = null;
							}
							$("#findMapTechs").find("span").addClass("currentMapLink");
							$("#viewAssignView").hide();
							$("#"+currId+"Content").show();
							$("#googleMap").show();
							break;
							
						case "highlightArea":
							$("#mapperLeftArrow").css('cursor', 'pointer').find("img").attr("src","/widgets/images/arrow_orange_left.png");
							$("#mapperRightArrow").css('cursor', 'pointer').find("img").attr("src","/widgets/images/arrow_orange_right.png");
							techMapper.initPolygon();
							$("#viewAssignView").hide();
							$("#"+currId+"Content").show();
							$("#googleMap").show();
							$("#highlightArea").find("span").addClass("currentMapLink");
							break;
						case "viewAssign":
							if(!techMapper.polygonCheck()){
								 alert("Please set at least 3 markers to highlight an area.");
								 $("#highlightAreaContent").show();
								 $("#highlightArea").find("span").addClass("currentMapLink");
							}else{
								$("#mapperLeftArrow").css('cursor', 'pointer').find("img").attr("src","/widgets/images/arrow_orange_left.png");
								$("#mapperRightArrow").css('cursor', 'default').find("img").attr("src","/widgets/images/arrow_grey_right.png");
								 $("#viewAssignView").show();
								 $("#"+currId+"Content").css('display','table-cell');
								 $("#googleMap").hide();
								 $("#viewAssign").find("span").addClass("currentMapLink");
								techMapper.viewAssign();
							}
							break;
							}
					google.maps.event.trigger(techMapper._goomap,'resize');
				});	

				$("#StartDate").datepicker();	
				$("#EndDate").datepicker();	

				var today = new Date();
				
				if(dashboardStartDate === "")$("#mapperForm input[name='StartDate']").val(today.getMonth() + 1 +"/"+today.getDate()+"/"+today.getFullYear());
				toDate = new Date(today.getTime() + (7*24*60*60*1000));
				if(dashboardEndDate === "")$("#mapperForm input[name='EndDate']").val((toDate.getMonth()+1)+"/"+toDate.getDate()+"/"+toDate.getFullYear());

				$("#thisWeekBtn").click(function(event) {
                    roll.autohide(false);
                    var opt = {
                        popup       : 'quickassigntool',
                        title       : 'Select Your Start Date Range',
                        width       : 220,
                        height		: 180,
                        top			: 350,
                        body     	: techMapper.thisWeekHtml(),
                        delay       : 0
                    }
                    roll.showNotAjax(this,event,opt);
                    $("#startDate").datepicker();
                    $("#endDate").datepicker();
                    $("#thisWeekPopup input[name='thisWeekRadio']").live('change mousedown', function(event){
                        if($(this).attr("id") == "startDateRange"){
                            $(".thisWeekRange").show();
                        }else{
                        	$(".thisWeekRange").hide();
                        }
                    });
                });	
				$("#allProjectsBtn").click(function(event) {
                    roll.autohide(false);
                    var opt = {
                        popup       : 'quickassigntool',
                        title       : 'Select Your Project',
                        width       : 230,
                        height		: 220,
	                    body		: techMapper.allProjectsHtml(),
                        delay       : 0
                    }
                    
                    roll.showNotAjax(this,event,opt);
                });	
				$("#pubWorkOrdersBtn").click(function(event) {
                    roll.autohide(false);
                    var opt = {
                        popup       : 'quickassigntool',
                        title       : 'Select Work Order Stage',
                        width       : 230,
                        height		: 220,
                        body     	: techMapper.woStageHtml(),
                        delay       : 0
                    }
                    roll.showNotAjax(this,event,opt);
                });	
				$("#mapLegendBtn").click(function(event) {
                    roll.autohide(false);
                    var opt = {
                        popup       : 'maplegend',
                        title       : 'Instructions',
                        width       : 230,
                        height		: 260,
                        body     	: $("#mapperLegend").html(),
                        delay       : 0
                    }
                    roll.showNotAjax(this,event,opt);
                });	
                
				$("#routeInputBtn").click(function(event) {
                    roll.autohide(false);
                    var opt = {
                        popup       : 'route',
                        title       : 'Enter Route',
                        width       : 260,
                        height		: 100,
                        body     	: techMapper.routePopupHtml(),
                        delay       : 0
                    }
                    roll.showNotAjax(this,event,opt);
                });	

				$("#mapWoSubmit").click(function(event) {
                    roll.autohide(false);
                    var opt = {
                        popup       : 'region',
                        title       : 'Select Work Orders  <span style="margin-left: 15px;">(<a href="javascript:void(0);" onclick="techMapper.resetSelections(true);" style="text-decoration:underline;">reset</a>)</span>',
                        width       : 360,
                        height		: 380,
                        body     	: techMapper.selectWorkOrdersPopup(),
                        delay       : 0
                    }
                    roll.showNotAjax(this,event,opt);
                    $("#startDate").datepicker();	
                	$("#endDate").datepicker();
                	if($("#allProjects #Project").val() == "")
                		techMapper.allProjectsSelectAll($("#allProjectsCheck"));

                	

                	$( "#RegionPopupInput" ).autocomplete({
                		source: function (request, callback) {	
                			var Projects = [];
                			$.each($("input[name='allProjectsCheck']:checked"), function(){
                				if($(this).val() != "All")Projects.push($(this).val());
                    		});	
    						Projects = Projects.join(",");
                			var WorkOrder = $("#woStage").val();
                			var StartDate = $("#thisWeekPopup input[name='startDate']").val();
                			var EndDate = $("#thisWeekPopup  input[name='endDate']").val();      
                			          		
                			$.ajax({
                					url: '/widgets/dashboard/client/region-bundle-search',
                					data: {Project: Projects, WorkOrder: WorkOrder, StartDate: StartDate, EndDate: EndDate, company: window._company, region:true},
                					type: 'POST',
                					dataType: 'json',
                					error: function() {
                					callback({});
                				},
                				success: function(data) {
                					callback(data);
                				}
                			});
                		},
                		select:
                			function(event, ui) {
                				$("#RegionPopupInput").val(ui.item.value);
                				$("#RegionPopupInput").blur();
                			}
                		});

                	$( "#RoutePopupInput" ).autocomplete({
                		source: function (request, callback) {	     
	                		var Projects = [];
	                		$.each($("input[name='allProjectsCheck']:checked"), function(){
	                			if($(this).val() != "All")Projects.push($(this).val());
	                    	});	
	    					Projects = Projects.join(",");
	                		var WorkOrder = $("#woStage").val();
	                		var StartDate = $("#thisWeekPopup input[name='startDate']").val();
	                		var EndDate = $("#thisWeekPopup  input[name='endDate']").val();    
	                		       		
                			$.ajax({
                					url: '/widgets/dashboard/client/region-bundle-search',
                					data: {Project: Projects, WorkOrder: WorkOrder, StartDate: StartDate, EndDate: EndDate, company: window._company, route:true},
                					type: 'POST',
                					dataType: 'json',
                					error: function() {
                					callback({});
                				},
                				success: function(data) {
                					callback(data);
                				}
                			});
                		},
                		select:
                			function(event, ui) {
                				$("#RoutePopupInput").val(ui.item.value);
                				$("#RoutePopupInput").blur();
                			}
                		});
                });	

				$("#dlCallList").click(function(event) {
                    roll.autohide(false);
                    var opt = {
                        popup       : 'dlCallList',
                        title       : 'Download Options',
                        width       : 355,
                        height		: 385,
                        body     	: techMapper.dlCallListPopupHtml(),
                        delay       : 0
                    }
                    roll.showNotAjax(this,event,opt);
                });	

                $("#PrintPage").click(function(){
                    qs = $("body").data("currentQueryString");
                    
                    if(qs != "" && qs != null)
                    	location = "/clients/techMapperPrint.php?"+$("body").data("currentQueryString");
                });

                $("#btnProjectMapping").click(function(event){
                	roll.autohide(false);
                    var opt = {
                        popup       : 'btnProjectMapping',
                        title       : 'Project Mapping',
                        width       : 260,
                        height		: 170,
                        body     	: techMapper.projectMappingPopupHtml(),
                        delay       : 0
                    }
                    roll.showNotAjax(this,event,opt); 
                });   

                $("#cmdFindMapTechs").click(function(){
                	$("#findMapTechs .mapHeaderLink").click();
                });   

                $("#btnProjMappingHighlight").click(function(){
                	$("#highlightArea .mapHeaderLink").click();
                });        

				if( (dashboardWos != "" && dashboardWos != null) || fromNav === '1')$("#fsMapper").click();                			
	    });
    function emcshowValue(val)
    {
        $("#li"+val).css('display','');
        $("#"+val).attr('checked','checked');
        $("#cbxemccert option:selected").remove();
        $("#cbxemccert option[value='0']").text('Select another EMC Certification');
        runSearch();
    }
    function basicToolsShowValue(val)
    {
        $("#li"+val).css('display','');
        $("#"+val).attr('checked','checked');
        $("#cbxbasictools option:selected").remove();
        $("#cbxbasictools option[value='0']").text('Select a Tool');
        runSearch();
    }
    function TelephonyToolsShowValue(val)
    {
        $("#li"+val).css('display','');
        $("#"+val).attr('checked','checked');
        $("#cbxTelephonyTools option:selected").remove();
        $("#cbxTelephonyTools option[value='0']").text('Select a Tool');
        runSearch();
    }
    function CablingExperienceShowValue(val)
    {
        $("#li"+val).css('display','');
        $("#"+val).attr('checked','checked');
        $("#cbxCablingExperiencetools option:selected").remove();
        $("#cbxCablingExperiencetools option[value='0']").text('Select Cabling Experience');
        runSearch();
    }
    function TelephonyExperienceShowValue(val)
    {
        $("#li"+val).css('display','');
        $("#"+val).attr('checked','checked');
        $("#cbxTelephonyExperience option:selected").remove();
        $("#cbxTelephonyExperience option[value='0']").text('Select Telephony Experience');
        runSearch();
    }
</script>
<form id="searchForm" name="searchForm" method="POST">
  <div>
    <div id="searchTabs" style="left: 1px; width: 264px; height:38px;">
      <ul style="border: 0px;" class="tabs">
        <li style="width:133px;" class="currenttab"><a id="listTechs" style="width: 111px; font-size: 12px;" href="javascript:void(0)" class="techTabs current">Find Techs</a></li>
        <li style="width:100px;"><a id="fsMapper" style="width: 111px;line-height: 12px; font-size: 12px;" href="javascript:void(0)" class="techTabs">FS-Mapper&#8482;<br />
          <span style="font-size: 9px">BETA</span></a></li>
      </ul>
      <div style="border: 0px;" class="tabsbottom"></div>
    </div>
    <div id="searchcol">
      <dl id="searchcat">
				<!--  input type="button" style="height: 22px;" class="link_button_sm" name="PrintPage" id="PrintPage" value="Print" -->
				<div id="topSearchButtons" style="margin: 0 8px 4px;" >
					<input type="button" style="height: 22px;" class="link_button_sm" name="clearSearch" id="clearSearch" value="Clear Filters">
					<input type="button" style="height: 22px;" class="link_button_sm" name="Blast" id="Blast" value="Send Email" rel="">
					<input type="button" class="btnDisabledGreySmall" name="Download" id="dlCallList" value="Download"  style="margin-left:60px;margin-top:5px;display:none;" disabled="disabled" >
				</div>
        <input name="emailList" id="emailList" type="hidden" value="" />
        <input id="v" name="v" value="<?=$_GET['v']?>" type="hidden" />
        <input id="doSearch" name="doSearch" value="1" type="hidden" />
        <li>
          <dt class="sechead" id="locationSection">&#43;&nbsp; Location</dt>
          <dd class="options">
            <table>
              <tr>
                <td><table>
                    <tr>
						 <!--14041-->  
                       <td>
                        <!-- style="width:75px;" -->
                        <label for="ProximityZipCode" style="width: 116px; margin-right: 0px;">Zip/Postal Code<span id="reqStar">*</span>
                         <span style="color: red; padding-left: 13px;" class="colState">OR</span>
                        </label>
                        <input type="text" name="ProximityZipCode" id="ProximityZipCode" class="onChange" style="width: 100px;" />
                      </td>
                      <td> 
                           <span class="colState">  
                              <label for="State" style="padding-left: 2px;">State<span id="reqStar">*</span></label>
                              <br />
                              <select id="state" name="state" class="onChange" style="width: 130px;">
                                <?=$usStatesHtml?>
                              </select>
                            </span>
                      </td>
					  <!--End 14041-->
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td><table>
                    <tr>
                      <td><label for="Country" style="width: 115px; margin-right: 0px;">Country</label>
                        <br />
                        <select id="Country" name="Country" class="onChange">
                          <?=$countriesHtml?>
                        </select></td>
                      <td><label for="CityS" style="padding-left: 2px;">City</label>
                        <br />
                        <input type="text" id="City" name="City" class="onChange" style="width: 130px;"/></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td><!-- style="width:100px;float:right;" -->
                  <label for="ProximityDistance" >Max Distance to Site</label>
                  <select id="ProximityDistance" name="ProximityDistance">
                    <option value="10">10 miles</option>
                    <option value="25">25 miles</option>
                    <option value="50" selected>50 miles</option>
                    <option value="75">75 miles</option>
                    <option value="100">100 miles</option>
                    <option value="150">150 miles</option>
                    <option value="250">250 miles</option>
                    <!--  option value="300">300 miles</option>
										<option value="500">500 miles</option -->
                  </select></td>
              </tr>
            </table>
            <input name="ProximityLat" id="ProximityLat" type="hidden" value="<?php echo getSubmittedOrDefaultField("ProximityLat")?>" />
            <input name="ProximityLng" id="ProximityLng" type="hidden" value="<?php echo getSubmittedOrDefaultField("ProximityLng")?>" />
            <div id="map_canvas" style="width: 100%; height: 300px; display: none"></div>
          </dd>
        </li>
        <li>
          <dt class="sechead" id="technicianSection">&#43;&nbsp; Technician</dt>
          <dd class="options">
            <table>
              <tr>
                <td><div style="width: 200px;">
					 <!--14007-->
                    <label for="techID" style="width:70px;float:left;">Technician&nbsp;ID#</label>
                    <input type="text" id="TechID" name="TechID" style="float:right; width: 120px;" class="onChange" />
                  </div></td>
              </tr>
              <?php
						// Company Specific Criteria
						if ($companyID == "FLS") {
					?>
              <tr>
                <td><div style="width: 200px;">
					 <!--14007-->
                     <label for="FLSID" style="width:70px;float:left;" value="<?php echo getSubmittedOrDefaultField("FLSID")?>">FLS&nbsp;ID</label>
                    <input type="text" id="FLSID" name="FLSID" style="float:right; width: 120px" value="<?php echo getSubmittedOrDefaultField("FLSID")?>"  class="onChange"/>
                  </div></td>
              </tr>
              <?php
						}
					?>
              <tr>
                <td><div style="width: 200px;">
                    <label for="FirstName" style="width:70px;float:left;">First Name</label>
                    <input type="text" id="FirstName" name="FirstName" style="float:right; width: 120px;" class="onChange" />
                  </div></td>
              </tr>
              <tr>
                <td><div style="width: 200px;">
                    <label for="LastName" style="width:70px;float:left;">Last Name</label>
                    <input type="text" id="LastName" name="LastName" style="float:right; width: 120px;" class="onChange" />
                  </div></td>
              </tr>
              <tr>
                <td><div style="width: 200px;">
                    <label for="PrimaryPhone" style="width:70px;float:left;">Phone #</label>
                    <input type="text" id="PrimaryPhone" name="PrimaryPhone" style="float:right; width: 120px;" class="onChange" />
                  </div></td>
              </tr>
              <tr>
                <td><div style="width: 200px;">
                    <label for="PrimaryEmail" style="width:70px;float:left;">Email</label>
                    <input type="text" id="PrimaryEmail" name="PrimaryEmail" style="float:right; width: 120px;" class="onChange" />
                  </div></td>
              </tr>
              <!--735-->
              <?php
              // Company Specific Criteria
              if ($companyID == "FLS")
              {
                  ?>
              <tr>
                      <td>
                          <input name="FLSIDExists" id="FLSIDExists" type="checkbox" class="checkboxOnChange" value="1" <?php echo (getSubmittedOrDefaultField("FLSIDExists", 1) == "1" ? "checked=\"checked\"" : "") ?>/>
                          <label for="prefonly">Show Fujitsu FLS Only</label>
                      </td>
                  </tr>
                  <?php
              }
              ?>
              <!--end 735-->
			  </table>
			</dd>
			</li>
			 <li>
			  <dt class="sechead" id="techPerformanceSection">&#43;&nbsp; Tech Performance</dt>
			  <dd class="options">
				<table>
              <tr>
                <td><label for="SATPerformanceAvg" style="margin-top:7px;">Performance &#37; Favorable Rating</label>
                  <select id="SATPerformanceAvg" name="SATPerformanceAvg" class="onChange">
                    <option value="0">0</option>
                    <option value="75">75%</option>
                    <option value="80">80%</option>
                    <option value="90">90%</option>
                    <option value="100">100%</option>
                  </select></td>
              </tr>
              <tr>
                <td><label for="SATRecommendedAvg">Preference &#37; Favorable Rating</label>
                  <select id="SATRecommendedAvg" name="SATRecommendedAvg" class="onChange">
                    <option value="0">0</option>
                    <option value="75">75%</option>
                    <option value="80">80%</option>
                    <option value="90">90%</option>
                    <option value="100">100%</option>
                  </select></td>
              </tr>
              <tr>
                <td><label for="Qty_IMAC_Calls">Completed Calls Greater Than</label>
                  <select id="Qty_IMAC_Calls" name="Qty_IMAC_Calls" class="onChange">
                    <option value="0">0</option>
                    <option value="10">10+</option>
                    <option value="25">25+</option>
                    <option value="75">75+</option>
                    <option value="100">100+</option>
                    <option value="150">150+</option>
                  </select></td>
              </tr>
              <tr>
                <td><label for="No_Shows">No Shows Less Than</label>
                  <select id="No_Shows" name="No_Shows" class="onChange">
                    <option value="0">0</option>
                    <option value="5">5</option>
                    <option value="10">10</option>
                    <option value="15">15</option>
                    <option value="25">25</option>
                  </select></td>
              </tr>
              <tr>
                <td><label for="Back_Outs">Back outs Less Than</label>
                  <select id="Back_Outs" name="Back_Outs" class="onChange">
                    <option value="0">0</option>
                    <option value="5">5</option>
                    <option value="10">10</option>
                    <option value="15">15</option>
                    <option value="25">25</option>
                  </select></td>
              </tr>
					</table>
					</dd>
				</li>
				<li>
                                    <!--735-->
					<dt class="sechead" id="technicianSection">&#43;&nbsp; FS-TechTags&trade;</dt>
                                        <!--end 735-->
					<dd class="options">
					<table>
					<tr>
                <td><input id="preferredOnly" name="preferredOnly" type="checkbox" value="1" class="checkboxOnChange"  />
                  <label for="prefonly">Show Preferred Only</label></td>
					</tr>
                                        <!--735-->
<!--					<tr>
                <td><input id="FStagBlueRibbonTechnician" name="FStagBlueRibbonTechnician" type="checkbox" value="1" class="checkboxOnChange"  />
                  <label for="FStagBlueRibbonTechnician">Blue Ribbon Technician</label></td>
				</tr>
				<tr>
					<td><input id="FStagBootCampCertified" name="FStagBootCampCertified" type="checkbox" value="1" class="checkboxOnChange"  />
					  <label for="FStagBootCampCertified">Boot Camp Certified</label></td>
				</tr>
				<tr>
					<td>
						<input id="FStagBootCampPlusCertified"  type="checkbox" value="43" class="checkboxOnChange"  name="certifications[]"/> <label for="FStagBootCampPlusCertified">Boot Camp Plus</label>
					</td>
				</tr>-->
                                <!--735-->
				<tr>
                <td><input id="FS_Mobile_Date" name="FS_Mobile_Date" type="checkbox" value="1" class="checkboxOnChange"  />
                  <label for="prefonly">FS-Mobile</label></td>
					</tr>
					<tr>
                <td><input type="checkbox" id="DellMRACompliant" name="DellMRACompliant" class="checkboxOnChange" <?php echo (getSubmittedOrDefaultField("DellMRACompliant") == "1" ? "checked=\"checked\"" : "")?> />
                  <label for="DellMRACompliant">Dell MRA Compliant</label></td>
					</tr>
					<tr>
                <td><span class="indentFilter">
                  <input type="checkbox" id="DellMRALapse" name="DellMRALapse" class="checkboxOnChange" <?php echo (getSubmittedOrDefaultField("DellMRACompliant") == "1" ? "checked=\"checked\"" : "")?> />
                  <label for="DellMRALapse">MRA Lapsed</label>
                  </span></td>
					</tr>  
                                        <tr><!--14071-->
                <td><input id="CLCLodgingDiscountProgram" name="certificationsnew[]" type="checkbox" value="164" class="checkboxOnChange"  />
                  <label for="FS_CheckINN">FieldSolutions CheckINN Hotel Savings Card</label></td>
					</tr><!--14071-->
					<tr>
                <td><input id="DevryShow" name="DevryShow" type="checkbox" value="2" class="checkboxOnChange"  />
                  <label for="DevryShow">DeVry Students</label></td>
					</tr>
                                        <!--735-->
<!--					<tr>
                <td><input type="checkbox" name="USAuthorizedDummy" id="USAuthorizedDummy" value="1" class="checkboxOnChange"  />
                  <label for="USAuthorizedDummy">US Verified</label></td>
					</tr>-->
                                        <!--/735-->
                                        <tr>
                <td><input type="checkbox" name="VerifiedSignLanguageCompetency" id="VerifiedSignLanguageCompetency" value="1" class="checkboxOnChange"  />
                  <label for="VerifiedSignLanguageCompetency">Verified Sign Language Competency</label></td>
					</tr>
                                        <!--735-->
                  	<?php 
					$dbInfo = Zend_Registry::get("DB_Info");
					if ($dbInfo["dbname"] !== "gbailey_technicianbureau"):?>
<!--					<tr>
                <td><input id="TBCShow" name="TBCShow" type="checkbox" value="3" class="checkboxOnChange"  />
                  <label for="TBCShow">Technician Boot Camp</label></td>
					</tr>-->
                                        
<!--					<tr>
                <td><input id="BlueRibbonTechnicianShow" name="BlueRibbonTechnicianShow" type="checkbox" value="4" class="checkboxOnChange"  />
                  <label for="BlueRibbionTechnicianShow">Blue Ribbon Technician</label></td>
					</tr>-->
                    <?php endif;?>
					<tr>
                <td><input id="hideisothech" name="hideisothech" type="checkbox" value="1" class="checkboxOnChange" />
                  <label for="hideisothech">Hide ISO Techs</label></td>
					</tr>
                                        <!--end 735-->
					<tr>
                <td><input id="hideBanned" name="hideBanned" type="checkbox" value="1" class="checkboxOnChange" />
                  <label for="hideBanned">Show Denied Techs</label></td>
					</tr>
                                        
					</table>
					</dd>
				</li>
				<li>
					<dt class="sechead">&#43;&nbsp; Public Credentials</dt>
					<dd class="options">
					<ul class="formlist">
              <li>
                <input type="checkbox" id="Bg_Test_Pass_Lite" name="Bg_Test_Pass_Lite" value="Pass" class="checkboxOnChange"  />
                <label for="Bg_Test_Pass_Lite">Basic Background Check</label>
              </li>
              <li>
                <input type="checkbox" id="Bg_Test_Pass_Full" name="Bg_Test_Pass_Full" value="Pass" class="checkboxOnChange"  />
                <label for="Bg_Test_Pass_Full">Comprehensive Background Check</label>
              </li>
              <li>
                <input type="checkbox" name="DrugPassed" id="DrugPassed" value="1" class="checkboxOnChange"  />
                <label for="DrugPassed">Gold Drug Test (9 Panel) </label>
              </li>
               <li>
                <input type="checkbox" name="SilverDrugPassed" id="SilverDrugPassed" value="1" class="checkboxOnChange"  />
                <label for="SilverDrugPassed">Silver Drug Test (5 Panel)</label>
              </li>
              <!--End 1370-->
              <li>
                <input type="checkbox" name="BlueRibbonTechnician" id="BlueRibbonTechnician" value="1" class="checkboxOnChange"  />
                <label for="DrugPassed">Blue Ribbon Technician</label>
              </li>
              <li>
                <input type="checkbox" name="BootCampCertified" id="BootCampCertified" value="1" class="checkboxOnChange"  />
                <label for="DrugPassed">Boot Camp Certified</label>
              </li>
                        <!--<li><input type="checkbox" name="i9Verified" id="i9Verified" value="1" class="checkboxOnChange"  /> <label for="i9Verified">I-9 Verified</label></li>
                        <li><input type="checkbox" name="i9Pending" id="i9Pending" value="1" class="checkboxOnChange"  /> <label for="i9Pending">I-9 Pending</label></li>-->
              <!--735-->
              <li>
                <input id="FStagBootCampPlusCertified"  type="checkbox" value="43" class="checkboxOnChange"  name="certifications[]"/>
                <label for="FStagBootCampPlusCertified">Boot Camp Plus</label>
              </li>
              <!--end 735-->
              <li>
                <input type="checkbox" name="USAuthorized" id="USAuthorized" value="1" class="checkboxOnChange"  />
                <label for="USAuthorized">US Verified</label>
              </li>
			   <!-- 14056-->
              <li>
                <input type="checkbox" id="NACI" name="NACI" value="1" class="checkboxOnChange"  />
                <label for="NACI"> NACI Gov. Security Clearance</label>
                <ul class="indentFilter">
                    <li>
                        <input type="checkbox" name="NACILapsed" id="NACILapsed" value="" class="checkboxOnChange"  />
                        <!--14073--><label for="NACILapsed">Verified Only</label><!--End 14073-->
                    </li>
                </ul>
              </li>
              <li>
                <input type="checkbox" id="InterimSecClear" name="InterimSecClear" value="1" class="checkboxOnChange"  />
                <label for="InterimSecClear"> Interim Gov. Security Clearance</label>
                <ul class="indentFilter">
                    <li>
                        <input type="checkbox" name="InterimSecClearLapsed" id="InterimSecClearLapsed" value="" class="checkboxOnChange"  />
                        <!--14073--><label for="InterimSecClearLapsed">Verified Only</label><!--End 14073-->
                    </li>
                </ul>
              </li>
              <li>
                <input type="checkbox" id="FullSecClear" name="FullSecClear" value="1" class="checkboxOnChange"  />
                <label for="FullSecClear" >Secret Gov. Security Clearance</label>
                <ul class="indentFilter">
                    <li>
                        <input type="checkbox" name="FullSecClearLapsed" id="FullSecClearLapsed" value="" class="checkboxOnChange"  />
                        <!--14073--><label for="FullSecClearLapsed">Verified Only</label><!--End 14073-->
                    </li>
                </ul>
              </li>
              <li>
                <input type="checkbox" id="TopSecClear" name="TopSecClear" value="1" class="checkboxOnChange"  />
                <label for="TopSecClear">Top Secret Gov. Security Clearance</label>
                <ul class="indentFilter">
                    <li>
                        <input type="checkbox" name="TopSecClearLapsed" id="TopSecClearLapsed" value="" class="checkboxOnChange"  />
                        <!--14073--><label for="TopSecClearLapsed">Verified Only</label><!--End 14073-->
                    </li>
                </ul>
              </li>
              <li style="width:350px;">
                <input type="checkbox" id="TopSecSCIClear" name="TopSecSCIClear" value="1" class="checkboxOnChange"  />
                <label for="TopSecSCIClear" style="width:320px;">Top Secret SCI Gov. Security Clearance</label>
                <ul class="indentFilter">
                    <li>
                        <input type="checkbox" name="TopSecSCIClearLapsed" id="TopSecSCIClearLapsed" value="" class="checkboxOnChange"  />
                        <!--14073--><label for="TopSecSCIClearLapsed">Verified Only</label><!--End 14073-->
                    </li>
                </ul>
              </li> <!--end 14056-->
            </ul>
          </dd>
        </li>
        <li>
          <dt class="sechead">&#43;&nbsp; Skills &amp; FS-Experts&#8482;</dt>
          <dd class="options" style="width:257px;">
            <ul class="formlist formlistUlSkill" id="srskills" style="padding-left: 0px;width: 100%;">
                <?php
                foreach ($categories as $category) :
                    if ($category['Category'] == 'General')
                        continue;
			?>
                    <li id="li<?= $category['TechSelfRatingColumn'] ?>" style="display:none;">
                        <label for="<?= $category['TechSelfRatingColumn'] ?>Lable" style="width:230px;"><?= $category['Category'] ?></label>
                        <label class="selfRatingToolLable" style="padding-left: 10px;" for="<?= $category['TechSelfRatingColumn'] ?>">Self-Rating (5=Expert)</label>
                        <label class="selfRatingToolLable" style="padding-left: 10px;" for="<?= $category['TechSelfRatingColumn'] ?>NumOfWO"># of Work Orders</label>
                    <!--735-->
                        <?
                        if ($category['FSExpertEnable'] != 0)
                        {
                            ?>
                            <div>
                                <label class="selfRatingToolLable" style="padding-left: 10px;padding-top:0px;width:117px;" for="feexpert<?= $category['TechSelfRatingColumn'] ?>">FS-Expert&trade; <span style="font:bold 10px/10px Verdana,Arial,Helvetica,sans-serif">(<?= $category['Abbr'] ?>)</span></label>
                                <input id="feexpert<?= $category['TechSelfRatingColumn'] ?>" 
                                       name="<?= $category['Abbr'] ?>FSExpertCode<?= $category['Category_ID'] ?>" 
                                       type="checkbox" class="checkboxOnChange" 
                                       >
                            </div>
                            <div style="clear:both;"></div>
                            <?
                        }
                        ?>
                        <!--end 735-->
                        <select id="<?= $category['TechSelfRatingColumn'] ?>" name="<?= $category['TechSelfRatingColumn'] ?>" class="selfRatingTool">
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                    </select>


                    <!-- 13945 -->                        
                    <select id="<?= $category['TechSelfRatingColumn'] ?>NumOfWO" name="<?= $category['TechSelfRatingColumn'] ?>NumOfWO<?= $category['Category_ID'] ?>" class="selfRatingTool">
                       <option value="0">0</option>
                       <option value="1">1</option>
                       <option value="5">5</option>                            
                       <option value="10">10</option>
                       <option value="25">25</option>
                       <option value="50">50</option>
                    </select>
                    <!-- end 13945 -->
		</li>
		<?php endforeach; ?>
              <li class="cbxSkill">
				<select id="cbxselectSkill" style="width:220px;">
                	<option sort="" value=''>Select A Skill Category</option>
                        <?php foreach ($categories as $category) : 
							if ($category['Category'] == 'General') continue;
						?>
                        	<option sort="<?= $category['Category'] ?>" value='#li<?=$category['TechSelfRatingColumn']?>' wocatid='<?=$category['Category_ID']?>'><?=$category['Category']?></option>
                        <?php endforeach; ?>
                </select>
              </li>
              <style>
                                                        .formlistUlSkill label 
                                                        {
                                                            
                                                            float: left;
                                                        }
                                                        .formlistUlSkill .selfRatingToolLable
                                                        {
                                                            width:115px;
                                                            padding-top: 5px;
                                                            margin-right: 0px;
                                                            float: left;
                                                        }
                                                        .formlistUlSkill li{
                                                            padding-bottom: 4px;
                                                        }
                                                        .formlistUlSkill{
                                                            padding-top: 4px;
                                                        }
                                                        .formlistUlSkill .imageRemove 
                                                        {
                                                            float: left;
                                                        }
                                                    </style>
            </ul>
            <ul>
              <li id="liLanguage" style="display:none;">
                <input type="checkbox" id="chklanguage" name="chklanguage">
                <label for="chklanguage">Wireless Networking</label>
                <input type="checkbox" id="chklanguagefluent" name="chklanguagefluent">
              </li>
              <li class="cbxLanguage" style="padding-bottom:4px;">
                <?
                                                    $Languages = new Core_TechLanguages();
                                                    $LanguagesList = $Languages->getLanguages();
                                                ?>
                <select id="cbxselectLanguage" style="width:220px;" class="checkboxOnChange" >
                  <option sort="" value=''>Select A Language</option>
                  <?

                                                        if (!empty($LanguagesList))
                                                        {
                                                            foreach($LanguagesList as $Language)
                                                            {
                                                                ?>
                  <option value='<?=$Language["id"]?>'>
                  <?=$Language["name"]?>
                  </option>
                  <?
                                                            }
                                                        }
                                                    ?>
                </select>
              </li>
            </ul>
          </dd>
        </li>
        <li>
          <dt class="sechead">&#43;&nbsp; Client Credentials</dt>
            <?php
            $FSTagClass = new Core_Api_FSTagClass();
            $ClientCredentials = $FSTagClass->getFSTagList_ForCompany($isPM,$companyID,1,1,1);//14071
			$ClientCredentialsBlackBox =  false;
            $BBCompanies = split(",","BBAL,BBGC,BBGE,BBCN,BCSST,BBGN,BBJC,BBLC,BBNSG,BBNC,BBS,BB");
            if($isGPM){
                $ClientCredentialsBlackBox = true;
            } else if(in_array($companyID,$BBCompanies)){
                $ClientCredentialsBlackBox = true;
            }
                            ?>
        <dd class="options">
            <ul class="formlist">
                  <?php
                  //931
                if (!empty($ClientCredentials))
                {
                    foreach ($ClientCredentials as $clientCredential)
                    {
                        //839
                        if ($clientCredential["Name"] == "BlackBoxTelecom")
                        {
                            if(!empty($ClientCredentialsBlackBox))
                            {
                                ?>
                                <li>
                                    <input type="checkbox" name="BlackBoxTelecomHelper" id="BlackBoxTelecomHelper" value="" class="checkboxOnChange"  />
                                    <label for="BlackBoxTelecomHelper">Black Box Telecom - Helper (T1)</label>
                                    <ul class="indentFilter">
                                        <li>
                                            <input type="checkbox" name="BBTelecomHelperIncludePendingRequests" id="BBTelecomHelperIncludePendingRequests" value="" class="checkboxOnChange"  />
                                            <label for="BBTelecomHelperIncludePendingRequests">Include Pending Requests</label>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <input type="checkbox" name="BlackBoxTelecomAdvanced" id="BlackBoxTelecomAdvanced" value="" class="checkboxOnChange"  />
                                    <label for="BlackBoxTelecomAdvanced">Black Box Telecom - Advanced (T2)</label>
                                    <ul class="indentFilter">
                                        <li>
                                            <input type="checkbox" name="BBTelecomAdvancedIncludePendingRequests" id="BBTelecomAdvancedIncludePendingRequests" value="" class="checkboxOnChange"  />
                                            <label for="BBTelecomAdvancedIncludePendingRequests">Include Pending Requests</label>
                                        </li>
                                    </ul>
                                </li> 
                            <?php
                            }
                        }
                        else if($clientCredential["Name"] == "BlackBoxCabling")
                        {
                           if(!empty($ClientCredentialsBlackBox))
                            {
                                ?>
                                <li>
                                    <input type="checkbox" id="BlackBoxCablingHelper" name="BlackBoxCablingHelper" value="" class="checkboxOnChange"  />
                                    <label for="BlackBoxCablingHelper">Black Box Cabling - Helper (C1)</label>
                                    <ul class="indentFilter">
                                        <li>
                                            <input type="checkbox" name="BBCablingHelperIncludePendingRequests" id="BBCablingHelperIncludePendingRequests" value="" class="checkboxOnChange"  />
                                            <label for="BBCablingHelperIncludePendingRequests">Include Pending Requests</label>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <input type="checkbox" id="BlackBoxCablingAdvanced" name="BlackBoxCablingAdvanced" value="" class="checkboxOnChange"  />
                                    <label for="BlackBoxCablingAdvanced">Black Box Cabling - Advanced (C2)</label>
                                        <ul class="indentFilter">
                                            <li>
                                                <input type="checkbox" name="BBCablingAdvancedIncludePendingRequests" id="BBCablingAdvancedIncludePendingRequests" value="" class="checkboxOnChange"  />
                                                <label for="BBCablingAdvancedIncludePendingRequests">Include Pending Requests</label>
                                            </li>
                                        </ul>
                                </li>
                                <li>
                                    <input type="checkbox" id="lackBoxCablingLead" name="BlackBoxCablingLead" value="" class="checkboxOnChange"  />
                                    <label for="lackBoxCablingLead">Black Box Cabling - Lead (C3)</label>
                                    <ul class="indentFilter">
                                        <li>
                                            <input type="checkbox" name="BBCablingLeadIncludePendingRequests" id="BBCablingLeadIncludePendingRequests" value="" class="checkboxOnChange"  />
                                            <label for="IncludePendingRequests2">Include Pending Requests</label>
                                        </li>
                                    </ul>
                                </li>
                            <?php
                            } 
                        }
                        else
                        {
                            ?>
                            <li>
                                <input type="checkbox" id="<?= $clientCredential["Name"] ?>" name="certificationsnew[]" value="<?= $clientCredential["id"] ?>" class="checkboxOnChange" /> <label for="<?= $clientCredential["Name"] ?>"><?= (!empty($clientCredential['TagLevelsData']) ? "All &ndash; " : "") . $clientCredential["Title"] ?></label>
                                <?
                                if (!empty($clientCredential['TagLevelsData']))
                                {
                                    ?>
                                    <ul class="indentFilter">
                                    <?
                                        $levelorder = 0;
                                        foreach ($clientCredential['TagLevelsData'] as $TagLevels)
                                        {
                                            $levelorder++;
                                            ?>
                                                <li>
                                                    <input type="checkbox" id="<?= $TagLevels["Title"] ?>" name="certificationsnew[]" value="<?= $clientCredential["id"] . "_" . $TagLevels["id"] . "_" . $TagLevels["DisplayOrder"] ?>" class="checkboxOnChange" /> <label for="<?= $TagLevels["Title"] ?>"><?= "Level " . $levelorder . " &ndash; " . $TagLevels["Title"] ?></label>
                                                </li>
                                            <?
                                        }
                                    ?>
                                    </ul>
                                <?
                                }
                                ?>
                            </li>
                        <?php
                        }
                    }
                    //end 839
                }
                //931
                ?>
            </ul>
          </dd>
        </li>
        <li>
          <dt class="sechead">&#43;&nbsp; Product & Program E-Learning</dt>
          <dd class="options">
            <ul class="formlist">
              <?php
							// Company Specific Criteria
							if ($companyID == "FLS") {
						?>
              <li>
                <input type="checkbox" class="checkboxOnChange" id="Cert_Hallmark_POS" name="Cert_Hallmark_POS"<?php echo (getSubmittedOrDefaultField("Cert_Hallmark_POS") == "1" ? "checked=\"checked\"" : "")?> />
                <label for="Cert_Hallmark_POS">Cert Hallmark POS</label>
              </li>
              <li>
                <input type="checkbox" class="checkboxOnChange" id="Starbucks_Cert" name="Starbucks_Cert" <?php echo (getSubmittedOrDefaultField("Starbucks_Cert") == "1" ? "checked=\"checked\"" : "")?> />
                <label for="Starbucks_Cert">Starbucks Certification</label>
              </li>
              <li>
                <input type="checkbox" class="checkboxOnChange" id="Cert_Maurices_POS" name="Cert_Maurices_POS" <?php echo (getSubmittedOrDefaultField("Cert_Maurices_POS") == "1" ? "checked=\"checked\"" : "")?> />
                <label for="Cert_Maurices_POS">Maurices Certification</label>
              </li>
              <?php
							}
						?>
              <li>
                <input type="checkbox" id="ServRight_ElectroMech_Cert" name="ServRight_ElectroMech_Cert" class="checkboxOnChange" value="1" />
                <label for="ServRight_ElectroMech_Cert">Electro- Mechanical Certification</label>
              </li>
              <li>
                <input type="checkbox" id="Copier_Skills_Assessment" name="Copier_Skills_Assessment" class="checkboxOnChange" value="1" />
                <label for="Copier_Skills_Assessment">Copier Skills Assessment</label>
              </li>
            </ul>
          </dd>
        </li>
        <li>
          <dt class="sechead">&#43;&nbsp; Industry & Product Certifications</dt>
          <dd class="options">
            <ul class="formlist">
              <li>
                <input type="checkbox" class="checkboxOnChange" id="MCSE" name="MCSE" value="1"/>
                <label for="MCSE" style="width:215px;">Microsoft Certified Systems Engineer (MCSE)</label>
              </li>
              <li>
                <input type="checkbox" class="checkboxOnChange" id="MCSA" name="MCSA" value="1"/>
                <label for="MCSA" style="width:215px;">Microsoft Certified Solutions Associate (MCSA)</label>
              </li>
              <li>
                <input type="checkbox" class="checkboxOnChange" id="MCP" name="MCP" value="1"/>
                <label for="MCP" style="width:215px;">Microsoft Certified Professional (MCP)</label>
              </li>
              <li>
                <input type="checkbox" class="checkboxOnChange" id="CCNA" name="CCNA" value="1"/>
                <label for="CCNA" style="width:215px;">Cisco Certified Network Associate (CCNA)</label>
              </li>
              <li>
                <input type="checkbox" class="checkboxOnChange" id="DellCert" name="DellCert" value="1"/>
                <label for="DellCert">Dell Certification</label>
              </li>
              <li>
                <input type="checkbox" class="checkboxOnChange" id="HP_CertNum" name="HP_CertNum" value="1"/>
                <label for="HP_CertNum">HP Certification</label>
              </li>
              <li>
                <input type="checkbox" class="checkboxOnChange" id="CompTIA"  name="CompTIA" value="1"/>
                <label for="CompTIA">CompTIA Registered</label>
              </li>
              <li>
                <input type="checkbox" class="checkboxOnChange" id="APlus"  name="APlus" value="1" />
                <label for="APlus">CompTIA A+</label>
              </li>
              <li>
                <input type="checkbox" class="checkboxOnChange" id="CompTIA_PDI_PLUS"  name="certifications[]" value="36" />
                <label for="CompTIA_PDI_PLUS">CompTIA PDI+</label>
              </li>
              <li>
                <input type="checkbox" class="checkboxOnChange" id="CompTIA_Server_PLUS"  name="certifications[]" value="37" />
                <label for="CompTIA_Server_PLUS">CompTIA Server+</label>
              </li>
              <li>
                <input type="checkbox" class="checkboxOnChange" id="CompTIA_Network_PLUS"  name="certifications[]" value="38" />
                <label for="CompTIA_Network_PLUS">CompTIA Network+</label>
              </li>
              <li>
                <input type="checkbox" class="checkboxOnChange" id="BICSI" name="BICSI" value="1"/>
                <label for="BICSI">BICSI Certification</label>
              </li>
              <?
                                                $apiEMC = new Core_Api_TechClass();
                                                $emcitems = $apiEMC->getEMCList();
                                                if(count($emcitems)>0)
                                                {
                                                foreach ($emcitems as $item)
                                                {
                                            ?>
              <li id="li<?=$item['name']?>"  style="display:none;">
                <input class="checkboxOnChange EMCcheckbox" type="checkbox" id="<?=$item['name']?>" name="<?=$item['name']?>"/>
                <label for="<?=$item['name']?>">
                  <?=$item['label']?>
                </label>
              </li>
              <?
                                                }
                                            ?>
              <li>
                <select id="cbxemccert" style="width:220px;" onchange="emcshowValue(this.value)">
                  <option sort="" value='0'>Select an EMC Certification</option>
                  <?
                                                            foreach ($emcitems as $item)
                                                            {
                                                        ?>
                  <option value='<?=$item['name']?>' >
                  <?=$item['label']?>
                  </option>
                  <?
                                                            }
                                                        ?>
                </select>
              </li>
              <?
                                                }
                                            ?>
            </ul>
          </dd>
        </li>
        <li>
          <dt class="sechead">&#43;&nbsp; Basic Tools</dt>
          <dd class="options" style="width:257px;">
            <ul class="formlist frmbasictools" style="padding-left: 0px;width: 100%;">              
              <?
                  $api = new Core_Api_TechClass();
                  $items = $api->getBasicToolsList();
                  if(count($items)>0)
                  {
                  foreach ($items as $item)
                  {
              ?>
              <li id="li<?=$item['name']?>"  style="display:none;">
                <input class="checkboxOnChange BasicToolscheckbox" type="checkbox" id="<?=$item['name']?>" name="<?=$item['name']?>"/>
                <label for="<?=$item['name']?>" style="width:220px;" >
                  <?=$item['label_ext']?>
                </label>
              </li>
              <?
                }
              ?>
              <li>
                <select id="cbxbasictools" style="width:220px;" onchange="basicToolsShowValue(this.value)">
                  <option sort="" value='0'>Select a Tool</option>
                  <?
                     foreach ($items as $item)
                     {
                  ?>
                  <option value='<?=$item['name']?>' >
                  <?=$item['label_ext']?>
                  </option>
                  <?
                   }
                  ?>
                </select>
              </li>
              <?
               }
              ?>
            </ul>
          </dd>
        </li>
        <li>
          <dt class="sechead">&#43;&nbsp; Cabling Experience</dt>
          <dd class="options" style="width:257px;">
            <ul class="formlist frmCablingExperience" style="padding-left: 0px;width: 100%;">              
              <?
                  $api = new Core_Api_TechClass();
                  $items = $api->getCablingExperienceList();
                  if(count($items)>0)
                  {
                  foreach ($items as $item)
                  {
              ?>
              <li id="li<?=$item['name']?>"  style="display:none;">
                <input class="checkboxOnChange CablingExperiencecheckbox" type="checkbox" id="<?=$item['name']?>" name="<?=$item['name']?>"/>
                <label for="<?=$item['name']?>" style="width:220px;" >   
                  <?=$item['label_ext']?>
                </label>
              </li>
              <?
                }
              ?>
              <li>
                <select id="cbxCablingExperiencetools" style="width:220px;" onchange="CablingExperienceShowValue(this.value)">
                  <option sort="" value='0'>Select Cabling Experience</option>
                  <?
                     foreach ($items as $item)
                     {
                  ?>
                  <option value='<?=$item['name']?>' >
                  <?=$item['label_ext']?>
                  </option>
                  <?
                   }
                  ?>
                </select>
              </li>
              <?
               }
              ?>
            </ul>
          </dd>
        </li>
        <li>
          <dt class="sechead">&#43;&nbsp; Cabling & Telephony Tools</dt>
          <dd class="options" style="width:257px;">
            <ul class="formlist frmCablingTelephonyTools" style="padding-left: 0px;width: 100%;">              
              <?
                  $api = new Core_Api_TechClass();
                  $items = $api->getTelephonyToolsList();
                  if(count($items)>0)
                  {
                  foreach ($items as $item)
                  {
              ?>
              <li id="li<?=$item['name']?>"  style="display:none;">
                <input class="checkboxOnChange TelephonyToolscheckbox" type="checkbox" id="<?=$item['name']?>" name="<?=$item['name']?>"/>
                <label for="<?=$item['name']?>" style="width:220px;" >   
                  <?=$item['label_ext']?>
                </label>
              </li>
              <?
                }
              ?>
              <li>
                <select id="cbxTelephonyTools" style="width:220px;" onchange="TelephonyToolsShowValue(this.value)">
                  <option sort="" value='0'>Select a Tool </option>
                  <?
                     foreach ($items as $item)
                     {
                  ?>
                  <option value='<?=$item['name']?>' >
                  <?=$item['label_ext']?>
                  </option>
                  <?
                   }
                  ?>
                </select>
              </li>
              <?
               }
              ?>
            </ul>
           
          </dd>
        </li>
        <li>
          <dt class="sechead">&#43;&nbsp; Telephony Experience</dt>
          <dd class="options" style="width:257px;">
            <ul class="formlist frmTelephonyExperience" style="padding-left: 0px;width: 100%;">              
              <?
                  $api = new Core_Api_TechClass();
                  $items = $api->getTelephonyExperienceList();
                  if(count($items)>0)
                  {
                  foreach ($items as $item)
                  {
              ?>
              <li id="li<?=$item['name']?>"  style="display:none;">
                <input class="checkboxOnChange TelephonyExperiencecheckbox" type="checkbox" id="<?=$item['name']?>" name="<?=$item['name']?>"/>
                <label for="<?=$item['name']?>" style="width:220px;" >   
                  <?=$item['label_ext']?>
                </label>
              </li>
              <?
                }
              ?>
              <li>
                <select id="cbxTelephonyExperience" style="width:220px;" onchange="TelephonyExperienceShowValue(this.value)">
                  <option sort="" value='0'>Select Telephony Experience </option>
                  <?
                     foreach ($items as $item)
                     {
                  ?>
                  <option value='<?=$item['name']?>' >
                  <?=$item['label_ext']?>
                  </option>
                  <?
                   }
                  ?>
                </select>
              </li>
              <?
               }
              ?>
            </ul>
          </dd>
        </li>
      </dl>
    </div>
  </div>
</form>
<div id="welcomeContent" style="display:none;">
  <div id="navBarWelcomeText" style="display:;text-align:justify;border: 3px solid #4790D4;margin-left: 5px;padding: 8px; margin-top: 11px;">
    <span style="color: #10085A; font-size:13px; font-weight: bold">It&#8217;s as Simple as &#8220;AND&#8221;:<br />
      <br />
      Only Preferred Techs coupled with FS-TechTags&trade; &amp; Credentials &amp; Special Skill Tech Groups, enables large Enterprises to deliver the right tech to the right assignment.</span><br />
      <br />
      The combination of &#8220;Preferred Providers&#8221; &amp; Skills &amp; Credentials made possible through FS-TechTags allows the greatest ability to find and assign work to qualified, preferred technicians for Large Enterprises. On any given day you may need:<br /><br />
    <ul style="list-style-image:url(/widgets/images/triangle.gif); font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; margin-left: 40px;">
      <li>a Preferred tech with Printer skills and Background Check, or</li>
      <li>a Preferred tech with cabling skills, or</li>
      <li>a Preferred tech that has passed your training in Flat Panel repair and speaks Spanish</li>
    </ul>
    <p>Only FieldSolutions couples Preferred tech &amp; FS-TechTags to make getting &#8220;the right tech to the right job&#8221; this simple. We call it &#8220;multidimensional searching &amp; assigning&#8221; and only FieldSolutions has the flexibility to find, filter, and then even automatically assign and compensate properly with custom configured structures to match all your field service technology programs. <br />
      </p>
      <span style="color: #10085A; font-size:13px; font-weight: bold">Truly Vetted Credentials</span></br>
    <p><u>Blue Ribbon Techs for Peace of Mind</u><br />
      FieldSolutions knows that without correct adjudication of background checks and drug tests, and current review dates your insurances (and ours!) may be out of force. That's a risk FieldSolutions is not prepared to take, so we've hired the leader, BlueRibbon Techs (<a href="http://www.blueribbontechs.com">www.blueribbontechs.com</a>). We can sleep at night knowing that the adjudication process is complete and accurate.</p>
    <p><u>US-Authorized Technicians - I-9 Verified</u> <br />
      FieldSolutions now holds US- I-9 Verifications for Technicians authorized to perform services in the United States, including physical proof of identity. The US-Verified filter can be found in the FS-TechTags' and Public Credentials sections of the left-hand sidebar of Find Techs.</p>
    <span style="color: #10085A; font-size:13px; font-weight: bold">Special Skill Groups Service Specific Client Requirements</span>
    <p><u>Dell MRA Compliant</u> <br />
      All Dell service partners can take advantage of the Dell MRA Compliant technicians &#8220;Dell MRA&#8221; Tech Tag. These techs have a verified Blue Ribbon Techs Basic Background Check, Comprehensive Background Check, and Drug Test within the past year. Compliance has 'lapsed' if any one of the three tests is over one year old. The filter for this tag is located in the FS-Tags' section of the left-hand sidebar of Find Techs.</p>
    <p><u>Sign Language and Non-English Language Speaking TechTags</u><br />
      Our clients are demanding special personal skills, including American Sign Language, Spanish speaking and French speaking, so we've added these categories and started recruiting techs with these skills North American-wide.</p>
    <p><u>DeVry Intern Technicians</u> <br />
      FieldSolutions has been selected as a partner, within the curriculum of DeVry University and is committed to investing in American Skills. A DeVry Intern TechTag indicates an intern trained and monitored by DeVry University. These technicians can accept work from any FieldSolutions client, often at a reduced rate, in order to meet their graduation requirements. Not available in all geographies and for all technologies. The filter for DeVry Intern Technicians is located in the FS-Tags' section of the left-hand sidebar of Find Techs.<br />
      <br />
      Do you need a special tag or assistance building tagged tech lists? Please contact your FieldSolutions operations support contact or Account Manager.</p>
  </div>
</div>
<div class="techPopupTemplate" style="display:none;">
  <table>
    <tr>
      <td><h1 style="text-align: left;">At-A-Glance</h1></td>
    </tr>
    <tr>
      <td>FS-Tech ID#: <span id="TechIDLabel"></span></td>
    </tr>
    <?php if ($companyID == "FLS") { ?>
    <tr>
      <td>FLS ID#: <span id="FLSIDLabel"></span> / FLS Status: <span id="FLSstatusLabel"></span></td>
    </tr>
    <?php } ?>
  </table>
  <table style="line-height: 19px;">
    <tr>
      <td><br /></td>
    </tr>
    <tr>
      <td colspan="3"><h1 style=" text-align: center;">Work History</h1></td>
    </tr>
    <tr>
      <td></td>
      <td><h2 style="text-decoration: underline;">12 Months</h2></td>
      <td><h2 style="text-decoration: underline;">Lifetime</h2></td>
    </tr>
    <tr>
      <td>Total Calls:</td>
      <td id="total12Months"></td>
      <td id="totalLifetime"></td>
    </tr>
    <tr>
      <td>Back Outs:</td>
      <td id="backOuts12Months"></td>
      <td id="backOutsLifetime"></td>
    </tr>
    <tr>
      <td>No Shows:</td>
      <td id="noShows12Months"></td>
      <td id="noShowsLifetime"></td>
    </tr>
    <tr>
      <td>Preference:</td>
      <td id="preference12Months"></td>
      <td id="preferenceLifetime"></td>
    </tr>
    <tr>
      <td>Performance:</td>
      <td id="performance12Months"></td>
      <td id="performanceLifetime"></td>
    </tr>
    <!--  tr>
		      		<td>Comments:</td><td id="comments12Months"></td><td id="commentsLifetime"></td>
		      	</tr -->
    <tr>
      <td colspan="3"><table>
          <tr>
            <td><img src="/widgets/images/thumbs-up.png" width="17" height="22" /></td>
            <td>Likes <span id="numLikes"></span><span id="likeButton"></span></td>
            <td><img src="/widgets/images/thought-bubble.png" width="17" height="22" /></td>
            <td><a href="javascript:techProfile.addComments()">Comment on Tech</a></td>
          </tr>
        </table></td>
    </tr>
  </table>
</div>
<div id="mapperLegend" style="display:none;">
  <table>
  	<tr>
  		<td><a href="/clients/FS-Mapper_V2_User_Instructions-2012-12-11.pdf" style="text-decoration:underline;" target="_blank">Click Here</a> for detailed FS-Mapper User Instructions.</td>
  	</tr>
  	<tr><td><br /></td></tr>
  	<tr>
  		<td style="font: 13px Helvetica,Sans-Serif;">Map Legend</td>
  	</tr>
    <tr>
      <td>Techs:</td>
    </tr>
    <tr>
      <td><img width="19" height="30" src="../../images/map/tech_marker_green_one.png">&nbsp; One tech at this location</td>
    </tr>
    <!--  tr>
		        	<td><img width="19" height="30" src="../../images/map/tech_marker_blue_multi.png">&nbsp; Multiple techs at this location</td>
		        </tr -->
    <tr>
      <td><img width="19" height="30" src="../../images/map/wo_cluster_green.png">&nbsp; Zoom in to see more detail</td>
    </tr>
    <tr>
      <td>Work Orders:</td>
    </tr>
    <tr>
      <td><img width="19" height="30" src="../../images/map/wo_marker_blue_one.png">&nbsp; One work order at this location</td>
    </tr>
    <!-- tr>
	        		<td><img width="19" height="30" src="../../images/map/wo_marker_blue_multi.png">&nbsp; Multiple work orders at this location</td>
	        	</tr -->
    <tr>
      <td><img width="19" height="30" src="../../images/map/wo_cluster_blue.png">&nbsp; Zoom in to see more detail</td>
    </tr>
  </table>
</div>
<style type="text/css">
 #mapper{
 	border: 2px solid #4790D4;
 	background-color: #C6D9F1;
 	margin-top: 5px;
 	width: 890px;
 	/*color: #174065;*/
 }

ul.tabs a{
	color: #FFF;
}
ul.tabs a.current{
	border-bottom: 3px solid #F69630;
	color: #FFF;
}
ul.tabs a:hover{
	color: #000;
}
ul.tabs a:hover {
	text-decoration: underline;
	background-color: #FFC24B;
}
ul.tabs a.current {
	background-color: #F69630;
    border-bottom: 1px solid #F69630;
    color: #ffffff;
    cursor: default;
    padding-top: 5px;
    height: 22px;
    top: 0;
}
ul.tabs a {
    padding: 5px 10px 6px;
    border-width: 0;
}
div.tabsbottom {
    height: 4px;
}

.mapHeaderLink{
	padding-right:5px;
}
.mapHeaderLink a{
	color: #10085A;
}

.mapHeaderLink a:hover{
	color: #0000FF;
	text-decoration:underline;
}

.currentMapLink{
	background: #10085A; 
	margin-top: -5px;
	color: #FFFFFF;
}

.currentMapLink a{
	color: #FFFFFF;
}

.currentMapLink a:hover{
	color: #FFFFFF;
}

#topNavLinks{
    height: 25px;
    list-style-type: none;
    margin-bottom: 0;
    margin-left: 0;
    padding-top: 0;
}

#topNavLinks li{
	display: inline;
	line-height: 2;
}

#topNavLinks li span{
	display: inline-block;
	height: 28px;
	vertical-align: middle;
}
.chevron{
	position: relative; 
	top: 3px;
	height: 17px;
}
</style>
<div id="results">
  <div id="resultsWelcome"> </div>
  <div id="mapperView" style="display:none;">
  <form name="mapperForm" id="mapperForm">
    <div id="mapper">
      <ul id="topNavLinks" style="border-bottom: 3px solid #4790D4;font: 11px/10px Verdana,Arial,Helvetica,sans-serif;background-color: #FFF;">
				<li id="mapWorkOrders">
					<a href="javascript:void(0);" class="mapHeaderLink" id="mapperLeftArrow" style="cursor:default;">
						<img class="chevron" src="/widgets/images/arrow_grey_left.png" alt="Go Back a Step" title="Go Back a Step"/>
					</a>
					<span class="mapHeaderLink currentMapLink">
						1. <a href="#" >Select Work Orders</a>
					</span>
					<img style="position: relative; top: 2px;" src="/widgets/images/dbl_triangle.gif">
				</li>
				
				
				<li id="findMapTechs">
					<span class="mapHeaderLink">
						2. <a href="#">Find &amp; Map Techs</a>
					</span>
					<img style="position: relative; top: 2px;" src="/widgets/images/dbl_triangle.gif">
				</li>
				
				<li id="highlightArea">
					<span class="mapHeaderLink">
						3. <a href="#">Highlight an Area</a>
					</span>
					<img style="position: relative; top: 2px;" src="/widgets/images/dbl_triangle.gif">
				</li>
				
				<li id="viewAssign" style="margin-right: 3px;padding-right: 1px;">
					<span class="mapHeaderLink">
						4. <a href="#">Bundle &amp; Assign</a>
					</span>
					<a href="javascript:void(0);" class="mapHeaderLink" id="mapperRightArrow" >
						<img class="chevron"  src="/widgets/images/arrow_orange_right.png" alt="Advance a Step" title="Advance a Step" />
					</a>
				</li>				
				<li id="mapLegend" style="margin-right: 1px;padding-right: 5px;">
						<input type='button' class='link_button_sm' value='Instructions' id='mapLegendBtn'>
        </li>
				<li id="clear" style="margin-right: 8px;">
					<input type='button' class='link_button_sm' value='Start Over' id='clearMapper' onclick="techMapper.clearAll();">
        </li>
      </ul>
			<div id="bottomOptions" style=" width: 100%;font: 11px/10px Verdana,Arial,Helvetica,sans-serif;">
				<div id="mapWorkOrdersContent" class="mapHeaderContent">
					<span style="text-align:center; float:left;border-right: 5px solid #4790D4;padding-right: 5px;height: 53px;">
						<input type='button' class='link_button middle2_button' value='Select Work Orders' id='mapWoSubmit' style="margin: 10px 0 0 5px;">
          </span>
					
          <table style="table-layout:fixed;margin-left: 5px;width: 80%; line-height:15px;">
						<span id="allProjects"><input type="hidden" name="Project" id="Project" value="<?=$_REQUEST['projects']?>" /></span>
						<span id="regionInput"><input type="hidden" name="Region" id="Region" value="" /></span>
						<span id="regionInput"><input type="hidden" name="TB_UNID" id="WinNum" value="" /></span>
						<span id="regionInput"><input type="hidden" name="WO_ID" id="WO_ID" value="" /></span>
						<span id="workOrdersStage"><input type="hidden" name="WorkOrder" id="WorkOrder" value="<?php if($_REQUEST['tab'] != "" && $_REQUEST['tab'] != "workdone"){ echo $_REQUEST['tab'];}else{ ?>Published<?php }?>" /></span>
						<span id="thisWeek"><input type="hidden" name="StartDate" id="StartDate" value="<? echo str_replace("_", "/",$_REQUEST['start_date']);?>" /><input type="hidden" name="EndDate" id="EndDate" value="<? echo str_replace("_", "/",$_REQUEST['end_date']);?>" /></span>
						<span id="routeInput"><input type="hidden" name="Route" id="Route" value="" /></span>
            <tr>
							<td style="padding-top:3px;">Next Step: 
								<input type="button" id="cmdFindMapTechs" class="btnDisabledGreySmall" name="Find & Map Techs" value="Find & Map Techs"  style="margin-left:6px;margin-top:5px;" disabled="disabled" >
							</td>
            </tr>
					</table>
				</div>
				<div id="findMapTechsContent" style="display: none;line-height: 15px;">
					
					<div id="findMapTechsOneHundred">
						<table style="margin: 5px; float:left;">
						<col width="570">
							<tr><td>For a single Work Order: Use Find Techs in the left sidebar or click on a Work Order pin and select the "Use Zip Code" button.</td></tr>
							<tr>
								<td>For multiple Work Orders: Map Techs and/or Bidders using the "Project Mapping" button</td>
								
							</tr>
							<tr><td>
								<div id="projectMappingSeeMore" class="seeMore" style="display:none;">
									FS-Mapper is displaying 
									<span id="techsShowing"></span> of 
									<span id="techsTotal"></span> Technicians that meet your criteria. 
									<span id="seeMoreTechs" style="display:none;">
										(<a href="javascript:void(0);"  onclick="techMapper.loadMoreTechs();" style="text-decoration:underline;">see more</a>)
									</span>
									<span id="removeTechs">
										(<a href="javascript:void(0);"  onclick="techMapper.clearTechMarkers();$('#projectMappingSeeMore').hide();" style="text-decoration:underline;">remove techs</a>)
								</span>
									</span>
								</div>
								<div id="projectMappingMultWos" class="seeMore" style="display:none;">
									FS-Mapper is displaying 
									<span id="techsShowing"></span> Technicians for each Work Order on the map. 
									<span id="seeMoreTechs">
										(<a href="javascript:void(0);"  onclick="techMapper.mapTechsAroundWos(true);" style="text-decoration:underline;">see more</a>)
									</span>
									<span id="removeTechs">
										(<a href="javascript:void(0);"  onclick="techMapper.clearTechMarkers();" style="text-decoration:underline;">remove techs</a>)
									</span>
								</div>
								<div id="projectMappingWoAssigned" class="seeMore" style="display:none;">
									FS-Mapper is displaying Work Order bidders only.
									<span id="removeTechs">
										(<a href="javascript:void(0);"  onclick="techMapper.clearTechMarkers();" style="text-decoration:underline;">remove techs</a>)
									</span>
								</div>
								<div id="projectMappingWoBidders" class="seeMore" style="display:none;">
									FS-Mapper is displaying Work Order bidders only.
									<span id="removeTechs">
										(<a href="javascript:void(0);"  onclick="techMapper.clearTechMarkers();" style="text-decoration:underline;">remove techs</a>)
									</span>
								</div>
								<div id="projectMappingProjBidders" class="seeMore" style="display:none;">
									FS-Mapper is displaying all Project Bidders.
									<span id="removeTechs">
										(<a href="javascript:void(0);"  onclick="techMapper.clearTechMarkers();" style="text-decoration:underline;">remove techs</a>)
									</span>
								</div>
							</td></tr>
							<tr>
								<td></td>
								
							</tr>
						</table>
						<div style="float:right; border-left: 3px solid #4790D4; height: 80px;">
							<div style="margin: 15px 5px 0 5px;text-align:center;">Next Step<br /><input type="button" id="btnProjMappingHighlight" class="btnDisabledGreySmall" name="Highlight an Area" value="Highlight an Area"  disabled="disabled" >
							</div>
						</div>
						<div style="float:right; border-left: 3px solid #4790D4; height: 80px;">
							<div style="margin: 15px 5px 0 5px;text-align:center;">FS-Mapper Tools<br /><input type='button' class='link_button_sm' value='Project Mapping' id='btnProjectMapping' ></div>
						</div>
					</div>
				</div>
			
        <div id="highlightAreaContent" style="display: none;line-height:15px;">
					<table style="margin: 5px; float:left;">
					<col width="550">
						<tr><td>1. Click on the map to highlight (lasso) the area around the Work Orders and Technicians you wish 
      to consider for assignment. (<a href="javascript:void(0);" style="text-decoration:underline" onclick="techMapper.clearPolygon();techMapper.initPolygon();">Try again?</a>)</td></tr>
						<tr>
							<td>2. Bundle Work Orders by highlighting an area around them and clicking 'Bundle Work Orders'</td>
							
						</tr>
						<tr><td>3. Click 'Bundle & Invite' to ask Techs  in the highlighted area to bid on these Work Orders.</td></tr>
						<tr>
							<td></td>
							
						</tr>
					</table>
					<div style="float:right; border-left: 3px solid #4790D4; height: 80px;">
						<div style="margin: 15px 5px 0 5px;text-align:center;">Next Step<br /><input type='button' class='link_button_sm bundleAssignBtn' value='Assign' onclick='$("#viewAssign .mapHeaderLink").click();'></div>
        </div>
					<div style="float:right; border-left: 3px solid #4790D4; height: 80px;">
						<div style="margin: 15px 5px 0 5px;text-align:center;">FS-Mapper Tools<br /><input type='button' class='link_button_sm bundleInviteBtn' value='Bundle &amp; Invite' ></div>
					</div>                
				</div>
        <div id="viewAssignContent" style="display: none; line-height:15px;">
					<table style="margin: 5px; float:left;width: 590px;">
					<col width="550">
						<tr><td>1. Select the Work Orders you wish to Assign and/or Bundle.</td></tr>
						<tr>
							<td style="padding-left: 1em">i. If you are Assigning now, select a Technician and click the 'Assign' button.</td>
							
						</tr>
						<tr>
                        	<td style="padding-left: 1em">ii. If you are Bundling Work Orders only, click the 'Bundle &amp; Invite' button</td>
						</tr>
						<tr>
							<td></td>
						</tr>
					</table>
					<div style="float:right; border-left: 3px solid #4790D4; height: 80px;">
						<div style="margin: 15px 5px 0 5px;text-align:center;">Previous Step<br/><input type='button' class='link_button_sm' value='Highlight an Area' onclick="$('#highlightArea .mapHeaderLink').click();"><br /></div>
					</div>
					<div style="float:right; border-left: 3px solid #4790D4; height: 80px;">
						<div style="margin: 15px 5px 0 5px;text-align:center;">FS-Mapper Tools<br /><input type='button' class='link_button_sm bundleAssignBtn' value='Assign'><br/><input type='button' class='link_button_sm bundleInviteBtn bundleInviteHightlight' value='Bundle &amp; Invite' ></div>
					</div>                 
				</div>
			</div>
			<div id="googleMap" style="width:100%; height: 500px; border-top: 3px solid #4790D4;"></div>
			<div id="viewAssignView" style="background-color:#FFF; border-top: 3px solid #4790D4;"></div>
		</div>
        
    </div>
  </form>
  <div id="listView"> </div>
</div>
<script>
	jQuery("#resultsWelcome").html(jQuery("#welcomeContent").html());
</script>
<?php echo $resultsHtml;?>
</div>
<?php

require ("../footer.php");
?>