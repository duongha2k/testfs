<?php $page = "clients"; ?>
<?php $option = "reports"; ?>
<?php $selected = 'MetricsReport'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php require ("includes/adminCheck.php"); ?>
<?php
$displayPMTools = true;
//require_once("includes/PMCheck.php");
require_once("classActivityReport.php");
$companyID = $_GET["v"];
$Params=$_GET['Params'];

  

$Today = date("m/d/Y");
$Tomorrow  = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d")+1, date("Y")));

// This Week (Mon = 1 - Sun = 7)
$BOW = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d")-getDay(date("N"))+1, date("Y")));
$EOW = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d")-getDay(date("N"))+7, date("Y")));

// last Week (Mon = 1 - Sun = 7)
$BOLW = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d")-getDay(date("N"))-6, date("Y")));
$EOLW = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d")-getDay(date("N")), date("Y")));

// This Month
$BOM = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d")-date("d")+1, date("Y")));
$EOM = date("m/d/Y", mktime(0, 0, 0, date("m")+1, date("d")-date("d"), date("Y")));

// Last Month
$PrevBOM = date("m/d/Y", mktime(0, 0, 0, date("m")-1, date("d")-date("d")+1, date("Y")));
$PrevEOM = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d")-date("d"), date("Y")));

// Q1
$Q1Start = date("m/d/Y", mktime(0, 0, 0, 1, 1, date("Y")));
$Q1End = date("m/d/Y", mktime(0, 0, 0, 4, date("d")-date("d"), date("Y")));

// Q2
$Q2Start = date("m/d/Y", mktime(0, 0, 0, 4, 1, date("Y")));
$Q2End = date("m/d/Y", mktime(0, 0, 0, 7, date("d")-date("d"), date("Y")));

// Q3
$Q3Start = date("m/d/Y", mktime(0, 0, 0, 7, 1, date("Y")));
$Q3End = date("m/d/Y", mktime(0, 0, 0, 10, date("d")-date("d"), date("Y")));

// Q4
$Q4Start = date("m/d/Y", mktime(0, 0, 0, 10, 1, date("Y")));
$Q4End = date("m/d/Y", mktime(0, 0, 0, 13, date("d")-date("d"), date("Y")));

// H1
$H1Start = date("m/d/Y", mktime(0, 0, 0, 1, 1, date("Y")));
$H1End = date("m/d/Y", mktime(0, 0, 0, 7, date("d")-date("d"), date("Y")));

// H2
$H2Start = date("m/d/Y", mktime(0, 0, 0, 7, 1, date("Y")));
$H2End = date("m/d/Y", mktime(0, 0, 0, 13, date("d")-date("d"), date("Y")));

// YTD
$YTDStart = date("m/d/Y", mktime(0, 0, 0, 1, 1, date("Y")));
$YTDEnd = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d"), date("Y")));


?>
<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
<link rel="stylesheet" href="../../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript" src="../../library/js/dateDropDownOption.js"></script>
<script type="text/javascript" src="../../library/js/jquery.watermark.js"></script>
<script type="text/javascript" src="js/CreateUpdateRequestReport.js"></script>
<script type="text/javascript">
    var currYr = " <?= date("Y") ?>";
    var Today = <?php echo "\"" . $Today . "\""; ?>;
    var BOW = <?php echo "\"" . $BOW . "\""; ?>;
    var EOW = <?php echo "\"" . $EOW . "\""; ?>;
    var BOLW = <?php echo "\"" . $BOLW . "\""; ?>;
    var EOLW = <?php echo "\"" . $EOLW . "\""; ?>;
    var BOM = <?php echo "\"" . $BOM . "\""; ?>;
    var EOM = <?php echo "\"" . $EOM . "\""; ?>;
    var PrevBOM = <?php echo "\"" . $PrevBOM . "\""; ?>;
    var PrevEOM = <?php echo "\"" . $PrevEOM . "\""; ?>;
    var Q1Start = <?php echo "\"" . $Q1Start . "\""; ?>;
    var Q1End = <?php echo "\"" . $Q1End . "\""; ?>;
    var Q2Start = <?php echo "\"" . $Q2Start . "\""; ?>;
    var Q2End = <?php echo "\"" . $Q2End . "\""; ?>;
    var Q3Start = <?php echo "\"" . $Q3Start . "\""; ?>;
    var Q3End = <?php echo "\"" . $Q3End . "\""; ?>;
    var Q4Start = <?php echo "\"" . $Q4Start . "\""; ?>;
    var Q4End = <?php echo "\"" . $Q4End . "\""; ?>;
    var H1Start = <?php echo "\"" . $H1Start . "\""; ?>;
    var H1End = <?php echo "\"" . $H1End . "\""; ?>;
    var H2Start = <?php echo "\"" . $H2Start . "\""; ?>;
    var H2End = <?php echo "\"" . $H2End . "\""; ?>;
    var YTDStart = <?php echo "\"" . $YTDStart . "\""; ?>;
    var YTDEnd = <?php echo "\"" . $YTDEnd . "\""; ?>;   	
       
</script>
<?php
$id = $_GET["v"];
?>
<div id="ContainerID"></div>
<script>
    var _CreateUpdateRequestReport = CreateUpdateRequestReport.CreateObject({
        id:'_CreateUpdateRequestReport',
        ContainerID:'#ContainerID',
        Params:'<?=$Params?>',
        CompanyID:'<?=$companyID?>'
    });
</script>
<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
