<?php 
$page = 'clients';
$option = 'users';
$selected = 'usersCreate';
require_once '../headerStartSession.php';

if (strtoupper($_SESSION['tbAdminLevel']) != 'YES') { header("Location: ./"); die();}

$toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
if(!$isFLSManager) {
    $toolButtons[] = 'QuickAssign';
    $toolButtons[] = 'QuickApprove';
}
require ("../header.php");
require ("../navBar.php");
require ("includes/adminCheck.php");

$displayPMTools = true;
require_once("includes/PMCheck.php");
$company = ( !empty($_GET['v']) ) ? $_GET['v'] : $_SESSION['Company_ID'];
?>

<script type="text/javascript" src="/widgets/js/FSWidgetClientDetails.js"></script>
<script type="text/javascript">
var roll;
var detailsWidget;

$('document').ready(function(){
    roll = new FSPopupRoll();
    detailsWidget = new FSWidgetClientDetails({container:'detailsContainer',tab:'create'},roll);
    detailsWidget.setParams({Company_ID:'<?=$company?>'});
    detailsWidget.show();
});
</script>

<div align="center">
<br/>
<div id="detailsContainer"></div>
</div>


<?php require ("../footer.php"); ?>