//-------------------------------------------------------------
//-------------------------------------------------------------
wosDetails.Instances = null;
//-------------------------------------------------------------
wosDetails.CreateObject = function(config) {
    var obj = null;
    if (wosDetails.Instances != null) {
        obj = wosDetails.Instances[config.id];
    }
    if (obj == null) {
        if (wosDetails.Instances == null) {
            wosDetails.Instances = new Object();
        }
        obj = new wosDetails(config);
        wosDetails.Instances[config.id] = obj;
    }
    obj.init();

    return obj;
}
//-------------------------------------------------------------
function wosDetails(config) {
    var Me = this;
    this.id = config.id;   
    this.detailsWidget=config.detailsWidget;
    this.params = config.params;
    this.roll= new FSPopupRoll();
    this.rollMsg= new FSPopupRoll();
    this._redirect = false;
    this.fb = true;
    this.firstbuildEditor = false;
    this.ProjectID = 0;
    //------------------------------------------------------------
    this.LoadingAjax = function(ContentId){
        jQuery(ContentId).html("<center><img src='/widgets/images/wait.gif' border='0' /></center>");
    } 
    //------------------------------------------------------------
    this.ClosePopup=function(){      
        Me.roll.hide();
    }
    //------------------------------------------------------------
    this.showPopup = function(width,height,title,el){
        var content = "<div id='comtantPopupID'><div style='text-align:center;height:150px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
        var opt = {
            width       : width,
            height      : height,
            position    : 'middle',
            title       : title,
            body        : content
        };
        Me.roll.showNotAjax(el, el, opt);
    }
    //------------------------------------------------------------
    this.viewTechBtnHidden_onclick=function(){
        Me.params.siteNumber = jQuery("#edit_SiteNumber").val();
        Me.params.Project_ID = jQuery("#edit_Project_ID  option:selected").val();
        $.ajax({
            url : "/widgets/dashboard/details/list-tech-in-site",
            data : Me.params,
            cache : false,
            type : 'POST',
            dataType : 'html',
            context : this,
            error : function (xhr, status, error) {
                $('body').css('cursor', '');
                if ( xhr.responseText === "authentication required" ) {
                    document.location = "/clients/logIn.php";
                } else {
                    FSWidget.fillContainer("Sorry, but an error occured. No results found.", this);
                }
                this.tabOpenXhrRequest = null;
            },
            success : function(data, status, xhr) {
                if ( xhr.responseText === "data empty xml" ) {
                    jQuery(".viewTechBtnHidden").hide();
                    jQuery(".viewTechBtnHiddenView").show();
                    return false;
                }
                Me.showPopup(600, 230,'Select a Technician for assignment:',this );
                jQuery("#comtantPopupID").html(data);
                jQuery("#closePopup").unbind("click",Me.ClosePopup)
                .bind("click",Me.ClosePopup);
                jQuery(".openTechProfile").unbind("click",Me.openTechProfile)
                .bind("click",Me.openTechProfile);
                jQuery("#cmdAssign").unbind("click",Me.cmdAssign)
                .bind("click",Me.cmdAssign);
            }
        });
    }
    //-----------------------------------------------------------
    this.showMessages = function(data,el) {
        var html = '';
        if (Me._redirect && data.success) { //always refresh page on success
            window.location.reload();
        }
        if (data.success) {
            if(typeof(data.msg) == 'undefined') '<div class="success"><ul><li>Success!</li></ul></div>';
            html += data.msg
            if (data.success == 1)
                html+='<div class="tCenter"><input type="button" class="link_button_popup" id="closePopup" value="Ok" /></div>';
            else if (data.success == 2){
                html+= data.button;
            }
        }else {
            html+='<div class="errors"><ul>';
            for (i=0;i<data.errors.length;i++) {
                if(typeof data.errors[i] === 'object')
                    html+='<li>'+data.errors[i].message+'</li>';
                else
                    html+='<li>'+data.errors[i]+'</li>';
            }
            html+='</ul></div>';
            html+='<div class="tCenter"><input type="button" class="link_button_popup" id="closePopup" value="Ok" /></div>';
        }
        jQuery("#closePopup").unbind("click",Me.ClosePopup)
        .bind("click",Me.ClosePopup);
        Me.roll.autohide(false);
        var opt = {
            width       : 400,
            height      : '',
            position    : 'middle',
            title       : '',
            body        : html
        };
        if (data.success) {
            opt.width = 300;
        }
        return Me.roll.showNotAjax(el, el, opt);
    }
    //------------------------------------------------------------
    this.showMsgErorr = function(err){
        jQuery("#divError").html(err);
        jQuery("#divError").show();
        setTimeout(function(){
            jQuery("#divError").hide();
        },10000);
    }
    //------------------------------------------------------------
    this.cmdAssign = function(){
        var techid = "";
        jQuery(".AssignTech").each(function(){
            if(jQuery(this).is(":checked"))
            {
                techid = jQuery(this).val();
                return false;
            }    
        })
        if(techid == ""){
            Me.showMsgErorr("Please select a Technician for assignment");
            return false;
        }
        Me.params.techid = techid;
        if(typeof(created) != 'undefined'){
            if(created){
                jQuery("#edit_Tech_ID").val(techid);
                Me.detailsWidget.autofillTechInfo(techid);
                var siblingSpans = $('#updateDetailsForm > span'); 
                for(var i=0;i<siblingSpans.length;i++) 
                { 
                    if(siblingSpans[i].className=='required_tech') { 
                        if(this.value!='') { 
                            siblingSpans[i].style.display='inline'; 
                        } 
                        else { 
                            siblingSpans[i].style.display='none';
                        }
                    }
                }
                Me.roll.hide();
                return false;
            }
        }
        $.ajax({
            url : "/widgets/dashboard/details/do-tech-in-site",
            data : Me.params,
            cache : false,
            type : 'POST',
            context : this,
            error : function (xhr, status, error) {
                $('body').css('cursor', '');
                if ( xhr.responseText === "authentication required" ) {
                    document.location = "/clients/logIn.php";
                } else {
                    FSWidget.fillContainer("Sorry, but an error occured. No results found.", this);
                }
                this.tabOpenXhrRequest = null;
            },
            success : function(data) {
                if(data.success == 1){
                    Me.saveApprove();
                }
                else if(data.success == 2){
                    data.msg = "<div class='displayNone colorRed errormsg' id='divError'></div><div>Note: This Work Order is already assigned.</div>";
                    data.button = '<div class="tCenter"><input type="button" class="link_button_popup" id="closePopup" value="Cancel" />&nbsp;&nbsp;&nbsp;<input type="button" class="link_button_popup" id="saveApprove" value="Re-Assign" /></div>';
                    Me.showMessages(data,null);
                    jQuery("#saveApprove").unbind("click",Me.saveApprove)
                    .bind("click",Me.saveApprove);
                    jQuery("#closePopup").unbind("click",Me.ClosePopup)
                    .bind("click",Me.ClosePopup);
                }
            }
        });
    }
    //------------------------------------------------------------
    this.saveApprove = function(){

        $.ajax({
            url : "/widgets/dashboard/do/assign?"
            +"techs["+Me.params.win+"]=" +Me.params.techid
            +"&assignClientDeniedTech=true&"
            +"&agreeds["+Me.params.win+"]="+jQuery("#edit_PayMax").val()
            +"&qtydevices["+Me.params.win+"]="+jQuery("#edit_Qty_Devices").val()
            +"&amountsrep["+Me.params.win+"]="+jQuery("#edit_Amount_Per").val(),
            data : Me.params,
            cache : false,
            type : 'POST',
            context : this,
            error : function (xhr, status, error) {
                $('body').css('cursor', '');
                if ( xhr.responseText === "authentication required" ) {
                    document.location = "/clients/logIn.php";
                } else {
                    FSWidget.fillContainer("Sorry, but an error occured. No results found.", this);
                }
                this.tabOpenXhrRequest = null;
            },
            success : function(data) {
                if(data.error == 0){
                    data.success = 1;
                    Me._redirect = true;
                    Me.showMessages(data,null);
                }
                else{
                    Me.showMsgErorr(data.msg);
                }
            }
        });
    }
    //------------------------------------------------------------
    this.openTechProfile = function(){
        var TechID = jQuery(this).attr("TechID");
        openLightBoxPage('/clients/wosViewTechProfile.php?simple=0&TechID='+TechID+'&v='+Me.params.company+'&popup=1',1000,600);
    }
    //------------------------------------------------------------  
    this.saveTechToSiteImg_onclick=function(){    
        if(jQuery.trim(jQuery(this).attr("src")) == "/widgets/css/images/Btn_SaveTech_g.png") {
            return false;
        }
        var sitenumber=jQuery("#edit_SiteNumber").val();
        var Tech_ID=$("#edit_Tech_ID").val();
        if(typeof(Me.roll)=="undefined")
        {
            Me.roll= new FSPopupRoll();
        }
        
        Me.roll.autohide(false);
        var msg = "";
        if(jQuery.trim(sitenumber) == ''){
            msg = "Site Number required to save";
        }else if(jQuery.trim(Tech_ID) == ''){
            msg = "Assign FS Tech# required to save";
        }
        var html = '<div>'+msg+'</div><div class="tCenter"><input type="button" class="link_button_popup" id="closePopup" value="Ok" /></div>';
        var opt = {
            width       : 300,
            height      : '',
            position    : 'middle',
            title       : '',
            body        : html
        };
        if (jQuery.trim(sitenumber) == '' || jQuery.trim(Tech_ID) == "") {
            Me.roll.showNotAjax(null, null, opt);
            jQuery("#closePopup").unbind("click",Me.ClosePopup)
            .bind("click",Me.ClosePopup);
            return false;
        }
        $.ajax({
            url         : '/widgets/dashboard/sitelist/save-tech-to-site',
            dataType    : 'json',
            data        : {
                SiteNumber: $("#edit_SiteNumber").val(),
                SiteName: $("#edit_SiteName").val(),
                Site_Contact_Name: $("#edit_Site_Contact_Name").val(),
                Address: $("#edit_Address").val(),
                City: $("#edit_City").val(),
                State: $("#edit_State").val(),
                Zipcode: $("#edit_Zipcode").val(),
                Country: $("#edit_Country").val(),
                SitePhone: $("#edit_SitePhone").val(),
                SiteFax: $("#edit_SiteFax").val(),
                SiteEmail: $("#edit_SiteEmail").val(),
                Project_ID: $("#edit_Project_ID option:selected").val(),
                Tech_ID:$("#edit_Tech_ID").val()
            },
            cache       : false,
            type        : 'POST',
            context     : this,
            success     : function (data, status, xhr) {
                if (data) {
                    
                    var html = '<div>Site saved to project site list</div><div class="tCenter"><input type="button" class="link_button_popup" id="closePopup" value="Ok" /></div>';
                    var opt = {
                        width       : 300,
                        height      : '',
                        position    : 'middle',
                        title       : '',
                        body        : html
                    };
                    Me.roll.showNotAjax(null, null, opt);
                    jQuery("#closePopup").unbind("click",Me.ClosePopup)
                    .bind("click",Me.ClosePopup);    
                    jQuery(".viewTechBtnHidden").show();
                    jQuery(".viewTechBtnHiddenView").hide();
                }
            }
        });	
       
    }  
    this.cmdsaveSite = function(){
        var SaveTechToSite = jQuery(this).attr("SaveTechToSite");
        var techid = jQuery(this).attr("techid");
        var fname = jQuery(this).attr("fname");
        var lname = jQuery(this).attr("lname");
        if(SaveTechToSite == '1'){
            data.success = 2;
            data.msg = "<div style='margin-top:-22px;'>Would you like to save FS Tech ID# "+techid+" "+fname+" "+lname+" along with this Site?</div>";
            data.button = '<div style="padding-top:10px;" class="tCenter"><input type="button" class="link_button_sm" id="cmdSaveTechtoSite" value="Save Tech to Site" />&nbsp;&nbsp;&nbsp;<input type="button" class="link_button_sm" id="cmdSaveSiteOnly" value="Save Site Only" /></div>';
            Me.showMessages(data,this);
            jQuery("#cmdSaveTechtoSite").unbind("click",Me.cmdSaveTechtoSite)
            .bind("click",Me.cmdSaveTechtoSite);
            jQuery("#cmdSaveSiteOnly").unbind("click",Me.cmdSaveSiteOnly)
            .bind("click",Me.cmdSaveSiteOnly);
        }else
            detailsWidget.saveSite();
    }
    this.cmdSaveSiteOnly = function(){
        Me.roll.hide();
        detailsWidget.saveSite();
    }
    this.cmdSaveTechtoSite = function(){
        Me.roll.hide();
        detailsWidget.saveSite(Me.saveTechToSiteImg_onclick);
    }
    this.updateButton = function(){
        var event = this;
        detailObject.onInit.ConfirmApproveWithFinalTotalTechByZero(function(){
            var techid = jQuery.trim(jQuery("#updateDetailsForm #edit_Tech_ID").val());
            var win = jQuery('#updateDetailsForm #WIN_NUM').val()+";"
            + Base64.encode(jQuery('#updateDetailsForm #edit_Headline').val()) + ";"
            + Base64.encode(jQuery('#updateDetailsForm #edit_City').val()) + ";"
            + Base64.encode(jQuery('#updateDetailsForm #edit_State option:selected').val()) + ";"
            + Base64.encode(jQuery('#updateDetailsForm #edit_Zipcode').val()) + ";"
            + ";"
            + ";"
            + ";"
            + "";
            detailsWidget.ServiceSchedule = Me.getDataServiceSchedule();
            detailsWidget.AllWOVisitList = Me.getAllWOVisitList();
            Me.setWoDataFromFirstVisitData();
//            detailObject.onInit.warningAssignWO(
//                null,
//                win,
//                techid,
//                function(){ 
            //if(Me._OpenAddWintags){
            //    Me.openAddWintags();
            //}else{
                    detailsWidget.updateDetails();
            //}
                    
//                },
//                5);
        },
        jQuery("#edit_PayAmount").val(),
        jQuery("#edit_Approved").is(":checked"));
        
    }
    this.createButton = function(){
        var techid = jQuery.trim(jQuery("#createDetailsForm #edit_Tech_ID").val());
        var Headline = jQuery('#createDetailsForm #edit_Headline').val();
        var City = jQuery('#createDetailsForm #edit_City').val();
        var State = jQuery('#createDetailsForm #edit_State option:selected').val();
        var Zipcode = jQuery('#createDetailsForm #edit_Zipcode').val();
        if(jQuery.trim(Headline) == "" 
            || jQuery.trim(City) == "" 
            || jQuery.trim(State) == "" 
            || jQuery.trim(Zipcode) == "" ){
            var win = "";   
        }else
            var win = ";"
            + Base64.encode(Headline) + ";"
            + Base64.encode(City) + ";"
            + Base64.encode(State) + ";"
            + Base64.encode(Zipcode) + ";"
            + ";"
            + ";"
            + ";"
            + "";
        detailsWidget.ServiceSchedule = Me.getDataServiceSchedule();
        Me.setWoDataFromFirstVisitData();
//        detailObject.onInit.warningAssignWO(
//            null,
//            win,
//            techid,
//            function(){ 
                detailsWidget.createWO();
//            },
//            2);
    }
    //------------------------------------------------------------
    //------------------------------------------------------------
    this.jCustomSelect = function(cont){  
        var selectors = jQuery('.customSelect');
        if(cont){
            selectors = cont.find('.customSelect');
        }
        if (selectors.length > 0) {
            selectors.each(function(index,selector) {
                runCustomSelect(selector);
            });
        }
	
        function runCustomSelect(sel,afterOnClick) {		
            var selectorObj = jQuery(sel);
            selectorObj[0].csValue = selectorObj.find('.csValue')[0];		
            selectorObj[0].csText = selectorObj.find('.csText')[0];		
            if(!selectorObj.find('select')[0]) return;
            if(selectorObj.find('.csIcon').attr("disabled") == "true"){
                selectorObj.find('.csIcon').unbind("click");
                return;
            }
		
            var strLi = '';
            selectorObj.find('select option').each(function(index, opt){
                if(index==0){
                    strLi = '<li class="first" id="' + jQuery(opt).val() + '">'+ jQuery(opt).html() +'</li>';				
                }else{
                    strLi += '<li id="' + jQuery(opt).val() + '">'+ jQuery(opt).html() +'</li>';
                }
                if(jQuery(opt).attr('selected')){
                    if(typeof( selectorObj[0].csValue) != 'undefined') selectorObj[0].csValue.value = jQuery(opt).val();
                    selectorObj[0].csText.value = jQuery(opt).html();	
                }
            });
            if(typeof(selectorObj[0].csText.csLayer)!='undefined'){
                selectorObj[0].csText.csLayer.remove();
            }
            selectorObj[0].csText.csLayer = jQuery('<div class="customOption"><div class="csLayer"><ul>' + strLi + '</ul></div></div>');		
            selectorObj[0].csText.csLayer.appendTo(document.body);
            selectorObj[0].csText.csLayer.data('opened',false);
            selectorObj.find('.csIcon').unbind().click(function(e){
                e.preventDefault();
                var liHeight = jQuery(selectorObj[0].csText.csLayer.find('li')[0]).outerHeight();
                if(selectorObj[0].csText.csLayer.find('li').length > 4 ){
                    var _height = Math.min(liHeight * 4, ((selectorObj[0].csText.csLayer.find('li').length) * liHeight));
                    _height = 182;
                }else {
                    var _height = (selectorObj[0].csText.csLayer.find('li').length * liHeight);
                }
                var _width = selectorObj.outerWidth();
                var coords = selectorObj.offset();
                var coords2 = selectorObj.find('.csIcon').offset();
                
                if(!selectorObj[0].csText.csLayer.data('opened')){
                    selectorObj[0].csText.csLayer.addClass('smScrollContent').css({
                        'z-index': 9999,
                        'overflow': 'hidden',
                        'height': _height,
                        'width': _width + 6,
                        'position': 'absolute',
                        'top': coords.top + selectorObj.outerHeight(),
                        'left': coords.left - 3
                    });                
                    selectorObj[0].csText.csLayer.data('opened',true);
                }else{
                    close();
                }
            
                selectorObj[0].csText.csLayer.find(':first').css('height', _height);
                if(jQuery(selectorObj[0].csText.csLayer.find('li').length > 4)){
                    selectorObj[0].csText.csLayer.find(':first').css({
                        'overflow':'hidden',
                        'overflow-y': 'auto'
                    });
                }
                selectorObj[0].csText.csLayer.find(".csLayer").scrollTo(selectorObj[0].csText.csLayer.find(".csLayer li[id='9:00 AM']"));
            });
				
            selectorObj[0].csText.csLayer.find('li').each(function (ind, li) {
                jQuery(li).css('cursor', 'pointer');
                jQuery(li).unbind().bind({
                    'click': function(e){
                        e.preventDefault();
                        jQuery(selectorObj[0].csText).focus();
                        var valueCbx = "";
                        if(typeof( selectorObj[0].csValue) != 'undefined'){
                            valueCbx = jQuery(this).attr('id');
                            selectorObj[0].csValue.value = valueCbx;
                        }else{
                            valueCbx = jQuery(this).text();
                        }
                        selectorObj[0].csText.value = jQuery(this).text();				
                        //callback
                        callback(valueCbx);
                        //end callback	
                        close();
                    },
                    'mouseenter': function(){
                        selectorObj[0].csText.csLayer.find('li').removeClass('current');
                        jQuery(this).addClass('current');					
                    }
                });
            });
            function callback(value){
                if(selectorObj.find(".texttimeChooser").length > 0){
                    Me.setDataEstimatedDuration(null,selectorObj);
                }
                if(selectorObj.find(".TechCheckCheckInTime").length > 0 ||
                    selectorObj.find(".TechCheckCheckOutTime").length > 0){
                    Me.TechCheckCheckChange_change(null,selectorObj);
                }
                if(Me.detailsWidget.currentTab != "create" && (jQuery("#showWinTags").val() == "1"
                || jQuery("#showWinTags").val() == 1)
                   && selectorObj.find(".StartTimeFirst").length > 0 ){
                    Me._OpenAddWintags = true;
            }
            }
            function close(layer){
                if(typeof(layer)!='undefined'){
                    layer.css('top', -15000);
                    layer.data('opened',false);
                }else{
                    selectorObj[0].csText.csLayer.css('top', -15000);
                    selectorObj[0].csText.csLayer.data('opened',false);
                }
            }
        }
        jQuery(document).unbind('mousedown').mousedown(function(e){				
            if(!jQuery(e.target).hasClass('customOption') && jQuery(e.target).parents('.customOption').length == 0 /*&& !jQuery(e.target).hasClass('smScroller') && jQuery(e.target).parents('.smScroller').length == 0*/){
                jQuery('.customOption').css('top', -15000);
                jQuery('.customOption').each(function(index, sel){				
                    jQuery(sel).data('opened',false);
                });
            }
        });

    }
    //------------------------------------------------------------
    this.buildGUIVisit = function(html,lengthServiceSchedule,showbottom, section){
        var bottom = '';
        if(showbottom){
            bottom = '<div  id="divAddAVisit">\
                        <img src="/widgets/images/btn_orange_addavisit.png" style="cursor: pointer;" id="cmdAddAVisit" />\
                    </div>';
        }
        var contenthtml = '\
                            <tr>\
                             <td class="subheaderLeft" valign="top" style="margin: 0px;vertical-align: top;padding-top:10px">\
                               Visit #'+lengthServiceSchedule+'<br><div style="padding-top:15px;" >Cancel Visit <input type="checkbox" class="removeVisit" /></div>'+bottom+'\
                             </td>\
                             <td colspan="5"><table cellspacing="0" cellpadding="0"   itemid="0"  class="tableServiceSchedule tableServiceSchedule'+lengthServiceSchedule+'">\
                                <tr><td colspan="4" align="center"><hr class="visitLine" /><input type="hidden" id="addedbysection" name="addedbysection" value="' + section + '"/></td></tr>'+html+'\
                             </table></td>\
                            </tr>';
        return contenthtml;
    }
    //------------------------------------------------------------
    this.disabledImageFAQi = function(lengthServiceSchedule){
        jQuery(".tableServiceSchedule"+lengthServiceSchedule+" .cmdhelpFAQi").hide();
    }
    //------------------------------------------------------------
    this.addVisitSection1 = function(section){
        var html = jQuery("#tableServiceSchedule").html();
        var lengthServiceSchedule = jQuery(".tableServiceSchedule").length;
        if(typeof(lengthServiceSchedule) == 'undefined') lengthServiceSchedule = 0;
        lengthServiceSchedule++;
        var contenthtml = Me.buildGUIVisit(html,lengthServiceSchedule,true, section);
        jQuery("#cmdAddAVisit").remove();
        jQuery("#ContentServiceSchedule").append(contenthtml);
        Me.disabledImageFAQi(lengthServiceSchedule);
        //remove value
        jQuery('.tableServiceSchedule'+lengthServiceSchedule+" input:text").val("");
        jQuery('.tableServiceSchedule'+lengthServiceSchedule+" select").val("");
        Me.jCustomSelect(jQuery('.tableServiceSchedule'+lengthServiceSchedule));
        Me.eventServiceSchedule();
        //Me.updateOfVisits();

		if (typeof paytool_ctrl != "undefined" && paytool_ctrl != null) {
			var elements = jQuery(".EstimatedDuration");
			var element = elements[elements.length - 1];

			paytool_ctrl.addDuration (element);
		}
	}
    //------------------------------------------------------------
    this.cmdAddAVisit = function(){
        Me.addVisitSection6(1);
        Me.addVisitSection1(1);
    }
    //------------------------------------------------------------
    this.TimeInput_blur = function(event,el){
        if(typeof(el) != 'undefined') timeObject = el;
        else timeObject = this;
        var r1 = /^(1[0-2]|0?\d)\s*(am|pm)$/i;
        var r2 = /^(1[0-2]|0?\d)\:([0-5]\d)\s*(am|pm)$/i;
        var result;
        var time = $.trim($(timeObject).val()).toUpperCase();
        //if(time.match(/(0?\d|1[0-2]):(0\d|[0-5]\d) (AM|PM)/i) == null)alert("HERRREE");
        if ( time && (result = time.match(r1)) != null ) {
            $(timeObject).val(parseInt(result[1],10).toString() + ":00 " + result[2]);
        } else if ( time && (result = time.match(r2)) != null ) {
            $(timeObject).val(parseInt(result[1],10).toString() + ":" + result[2] + " " + result[3]);
        } else {
            $(timeObject).val(time);
        }
    }
    //------------------------------------------------------------
    //------------------------------------------------------------
    this.setWoDataFromFirstVisitData = function(){
        jQuery("#edit_StartDate").val(jQuery("#tableServiceSchedule .StartDate").val());
        jQuery("#edit_StartTime").val(jQuery("#tableServiceSchedule .StartTime").val());
        jQuery("#edit_EndDate").val(jQuery("#tableServiceSchedule .EndDate").val());
        jQuery("#edit_EndTime").val(jQuery("#tableServiceSchedule .EndTime").val());
        jQuery("#edit_Duration").val(jQuery("#tableServiceSchedule .EstimatedDuration option:selected").val());
    }
    //------------------------------------------------------------
    this.setDataEstimatedDurationForTime_onChange = function(){
        Me.setDataEstimatedDuration(null,jQuery(this).parent().parent().parent());
    }
    //------------------------------------------------------------
    this.setDataEstimatedDuration = function(ev, event){
        if(typeof(event) == 'undefined') event = this;
        event = jQuery(event).parent().parent().parent();
        //console.log("EST DUR HERE");
        Me.TimeInput_blur(null,jQuery(event).find(".StartTime"));
        Me.TimeInput_blur(null,jQuery(event).find(".EndTime"));
        
        var TechCheckCheckInDate = jQuery.trim(jQuery(event).find(".StartDate").val());
        var TechCheckCheckInTime = jQuery.trim(jQuery(event).find(".StartTime").val());
        var TechCheckCheckOutDate = jQuery.trim(jQuery(event).find(".EndDate").val());
        var TechCheckCheckOutTime = jQuery.trim(jQuery(event).find(".EndTime").val());
        var classStartType = jQuery(event).parent().find(".classStartType").val();
        var dataempty = false;
        if(TechCheckCheckInDate == "") dataempty = true;
        if(TechCheckCheckInTime == "") dataempty = true;
        if(TechCheckCheckOutDate == "") dataempty = true;
        if(TechCheckCheckOutTime == "") dataempty = true;
        if(dataempty || classStartType == 2){
            return false;
        }
        var data = 'CORCheckInDate='+ TechCheckCheckInDate 
        +'&CORCheckInTime='+ TechCheckCheckInTime 
        +'&CORCheckOutDate='+ TechCheckCheckOutDate 
        +'&CORCheckOutTime='+ TechCheckCheckOutTime; 
        $.ajax({
            url : "/widgets/dashboard/details/get-total-hours",
            data : data,
            cache : false,
            type : 'POST',
            success : function(data) {
                if(data.total > 0){
                    var org = data.total/3600;
                    var time = Math.round(org/0.25,0)*0.25;
                    jQuery(event).parent().find(".EstimatedDuration").val(time);
					if (typeof paytool_ctrl != "undefined" && paytool_ctrl != null) paytool_ctrl.update ();
                }
            }
        });     
    }
    //------------------------------------------------------------
    this.addVisitSection6 = function(section){
        var countVisit = jQuery("#tableVisitsSecction6 .contentTechCheck").length;
        countVisit++;
        var content = jQuery("#tableVisitsSecction6 .contentTechCheck").html();
        content = "<tr class='contentTechCheck contentTechCheck_"+countVisit+"' woVisitID='0' typetc='3'>"+content+"</tr>";
        content = content.replace(/TechCheckVisit1/g,'TechCheckVisit'+countVisit);
        jQuery("#tableVisitsSecction6").append(content);
        var event = jQuery(".contentTechCheck_"+countVisit);
        jQuery(event).find(".contentTechCheckCol1").html("<div><div style='width:17px;float:left;'><img style='cursor:pointer;' src='/widgets/css/images/removedoc.png' height='15px'  title='Disable Visit' class='cmdDeleteVisitSection6' />&nbsp;<input type='hidden' id='addedbysection' name='addedbysection' value='" + section + "'/></div><div style='float:left;'>"+countVisit+". Client Added</div></div><div style='padding-left:15px;'>Visit <span id='EnableLink' style=' display: none;'>(<a onclick='detailsWidget._wosDetails.cmdEnableVisitSection6(this);' style='cursor: pointer; text-decoration: underline;' visitindex='" + countVisit + "'>re-enable</a>)</span></div>");
        jQuery(event).find(".OptionCheckInOutTech").attr("checked","true");
        jQuery(event).find("input").val("");
        jQuery(event).find("#addedbysection").val(section);
        jQuery(event).find("input").removeAttr("disabled");
        jQuery(event).find(".csIcon").removeAttr("disabled");
        jQuery(event).find(".TechCheckDisplaytotal").html("&nbsp;");
        jQuery(event).find(".TechCheckDisplay").html("&nbsp;");
        jQuery(event).find(".TechCheckDisplay").attr("val","");
        jQuery(event).find(".TechCheckDisplaytotal").attr("total","");
        jQuery(event).find(".DisplayCORTotalHours").css ("color", "#333");
        Me.jCustomSelect(jQuery(event));
        Me.eventServiceSchedule();
        var edit_Qty_Visits = parseInt (jQuery("#edit_Qty_Visits").val());
        edit_Qty_Visits = (edit_Qty_Visits ? edit_Qty_Visits : 1) + 1;
        jQuery("#NumQtyVisits").val(edit_Qty_Visits);
        jQuery("#edit_Qty_Visits").val(edit_Qty_Visits);
        jQuery("#edit_Qty_Visits_Display").val(edit_Qty_Visits);
        detailsWidget.updateFinalPay("section6");
    }
    //------------------------------------------------------------
    this.cmdAddVisitSeccion6 = function(){
        Me.addVisitSection6(6);
        Me.addVisitSection1(6);
    }
    //------------------------------------------------------------
    this.cmdDeleteVisitSection6 = function(){
        var event = jQuery(this).parent().parent().parent().parent();
        jQuery(event).find(".csIcon").attr("disabled","true");
        jQuery(event).find("#EnableLink").css("display","inline");
        Me.jCustomSelect(jQuery(event));
        jQuery(this).css("display","none");
        jQuery(event).attr("disable","1");
        jQuery(event).find("input").attr("disabled","disabled");
       
        var edit_Qty_Visits = parseInt(jQuery("#edit_Qty_Visits").val())-1;
        jQuery("#edit_Qty_Visits").val(edit_Qty_Visits);
        jQuery("#NumQtyVisits").val(edit_Qty_Visits);
        jQuery("#edit_Qty_Visits_Display").val(edit_Qty_Visits).trigger("blur");
        detailsWidget.updateFinalPay("section6");
        Me.TechCheckCheckChange_change (null, jQuery(this).parent());
    }
    //------------------------------------------------------------
    this.cmdEnableVisitSection6 = function(link){
    	//TODO: The following line fails in IE when re-enabling a visit
        //event = jQuery(link).parent().parent().parent().parent();
    	var event = jQuery(".contentTechCheck");
    	event = jQuery(event[parseInt (jQuery(link).attr("visitindex")) - 1]);
        jQuery(event).find(".csIcon").attr("disabled","false");
        Me.jCustomSelect(jQuery(event));
        jQuery(jQuery(link).parent()).css("display","none");
        jQuery(event).find(".cmdDeleteVisitSection6").css ("display", "inline-block");
        jQuery(event).attr("disable","0");
        jQuery(event).find("input").attr("disabled","");
        
        var edit_Qty_Visits = parseInt(jQuery("#edit_Qty_Visits").val())+1;
        jQuery("#edit_Qty_Visits").val(edit_Qty_Visits);
        jQuery("#NumQtyVisits").val(edit_Qty_Visits);
        jQuery("#edit_Qty_Visits_Display").val(edit_Qty_Visits);
        detailsWidget.updateFinalPay("section6");
        Me.TechCheckCheckChange_change (null, jQuery(link).parent());
    }
    //------------------------------------------------------------
    //Same as last function.  Almost.
    this.cmdEnableVisitSection6_type2 = function(link){
        var event = jQuery(link).parent().parent().parent().parent().parent();
        jQuery(event).find(".csIcon").attr("disabled","false");
        Me.jCustomSelect(jQuery(event));
        jQuery(jQuery(link).parent().parent()).css("display","none");
        jQuery(event).find(".cmdDeleteVisitSection6").css ("display", "inline-block");
        jQuery(event).attr("disable","0");
        jQuery(event).find("input").attr("disabled","");
        
        var edit_Qty_Visits = parseInt(jQuery("#edit_Qty_Visits").val())+1;
        jQuery("#edit_Qty_Visits").val(edit_Qty_Visits);
        jQuery("#edit_Qty_Visits_Display").val(edit_Qty_Visits);
        Me.TechCheckCheckChange_change (null, jQuery(link).parent().parent());
    }
    //------------------------------------------------------------
    this.eventServiceSchedule = function(){
        $("#ContentServiceSchedule .classStartType").unbind("click",Me.classStartType_onChange)
        .bind("click",Me.classStartType_onChange);
        $("#ContentServiceSchedule .removeVisit").unbind("click",Me.removeVisit_onClick)
        .bind("click",Me.removeVisit_onClick);  
        
        detailObject.onInit.watermark();
        
        $('.CalendarInput').calendar({
            dateFormat:'MDY/'
        }); 
        jQuery(".TimeInput")
        .unbind("blur",Me.TimeInput_blur)
        .bind("blur",Me.TimeInput_blur);
       
        
        // build event set data Estimated Duration 
        $("#ContentServiceSchedule .classStartType").unbind("change",Me.setDataEstimatedDuration)
        .bind("change",Me.setDataEstimatedDuration);
        $("#ContentServiceSchedule .StartDate").unbind("change",Me.setDataEstimatedDuration)
        .bind("change",Me.setDataEstimatedDuration);
        $("#ContentServiceSchedule .StartTime").unbind("change",Me.setDataEstimatedDurationForTime_onChange)
        .bind("change",Me.setDataEstimatedDurationForTime_onChange);
        $("#ContentServiceSchedule .EndDate").unbind("change",Me.setDataEstimatedDuration)
        .bind("change",Me.setDataEstimatedDuration);
        $("#ContentServiceSchedule .EndTime").unbind("change",Me.setDataEstimatedDurationForTime_onChange)
        .bind("change",Me.setDataEstimatedDurationForTime_onChange);
        $("#cmdAddAVisit").unbind("click",Me.cmdAddAVisit)
        .bind("click",Me.cmdAddAVisit);
        $("#cmdAddVisitSeccion6").unbind("click",Me.cmdAddVisitSeccion6)
        .bind("click",Me.cmdAddVisitSeccion6);
        jQuery(".TechCheckCheckChange").unbind("change",Me.TechCheckCheckChange_change)
        .bind("change",Me.TechCheckCheckChange_change);
        $(".OptionCheckInOutTech").unbind("click",Me.TotalDuration)
        .bind("click",Me.TotalDuration);
        $(".OptionCheckInOutClient").unbind("click",Me.TotalDuration)
        .bind("click",Me.TotalDuration);
            
        $(".OptionCheckInOutTech").unbind("click",Me.OptionCheckInOutTech_click)
        .bind("click",Me.OptionCheckInOutTech_click);
        $(".OptionCheckInOutClient").unbind("click",Me.OptionCheckInOutClient_click)
        .bind("click",Me.OptionCheckInOutClient_click);
        $(".cmdDeleteVisitSection6").unbind("click",Me.cmdDeleteVisitSection6)
        .bind("click",Me.cmdDeleteVisitSection6);
    }
    //------------------------------------------------------------
    this.getDataServiceSchedule = function(){
        var data = "";
        jQuery("#ContentServiceSchedule .tableServiceSchedule").each(function() {
            var itemid = jQuery(this).attr("itemid");
            var isRemoveVisitAddnew = (itemid == "0" && jQuery(this).parent().parent().find(".removeVisit").is(":checked"));
            isRemoveVisitAddnew = false;
            if(!isRemoveVisitAddnew){
                var isRemoveVisit = jQuery(this).parent().parent().find(".removeVisit").is(":checked");
                var EstimatedDuration = jQuery(this).find(".EstimatedDuration option:selected").val();
                EstimatedDuration = (typeof(EstimatedDuration) == 'undefined')?"":EstimatedDuration;
                
                var StartDate = jQuery.trim(jQuery(this).find(".StartDate").val()) == "M/D/YYYY"?"":jQuery(this).find(".StartDate").val();
                var StartTime = jQuery.trim(jQuery(this).find(".StartTime").val()) == "H:MM AM/PM"?"":jQuery(this).find(".StartTime").val();
                var EndDate = jQuery.trim(jQuery(this).find(".EndDate").val()) == "M/D/YYYY"?"":jQuery(this).find(".EndDate").val();
                var EndTime = jQuery.trim(jQuery(this).find(".EndTime").val()) == "H:MM AM/PM"?"":jQuery(this).find(".EndTime").val();
                var StartTypeID = jQuery(this).find(".classStartType option:selected").val();
                var AddedBySection = jQuery(this).find("#addedbysection").val();
                if(typeof(StartTypeID) == 'undefined') StartTypeID = 1;
                if(typeof(itemid) == 'undefined') itemid = 0;
                
                data += ',{'
                +'"WoVisitID":"'+itemid+'"'
                +',"Remove":"'+isRemoveVisit+'"'
                +',"StartTypeID":"'+StartTypeID+'"'
                +',"StartDate":"'+ StartDate +'"'
                +',"StartTime":"'+ StartTime +'"'
                +',"EndDate":"'+ EndDate +'"'
                +',"EndTime":"'+ EndTime +'"'
                +',"AddedBySection":"'+ AddedBySection +'"'
                +',"EstimatedDuration":"'+EstimatedDuration+'"'
                +',"TechArrivalInstructions":"'+ jQuery(this).find(".TechArrivalInstructions").val() +'"'
                +'}';
            }
        });
        if(data != ""){
            data = data.substring(1,data.length);
        }
        return data;
    }
    //------------------------------------------------------------
    this.getAllWOVisitList = function(){
        var data = "";
        jQuery(".contentTechCheck").each(function(){
            var OptionCheckInOut = jQuery(this).find(".OptionCheckInOutTech").is(":checked")?1:2;
            var TechCheckCheckInDate = jQuery.trim(jQuery(this).find(".TechCheckCheckInDate").val()) == "MM/DD/YYYY"?"":jQuery(this).find(".TechCheckCheckInDate").val();
            var TechCheckCheckInTime = jQuery.trim(jQuery(this).find(".TechCheckCheckInTime").val()) == "H:MM AM/PM"?"":jQuery(this).find(".TechCheckCheckInTime").val();
            var TechCheckCheckOutDate = jQuery.trim(jQuery(this).find(".TechCheckCheckOutDate").val()) == "MM/DD/YYYY"?"":jQuery(this).find(".TechCheckCheckOutDate").val();
            var TechCheckCheckOutTime = jQuery.trim(jQuery(this).find(".TechCheckCheckOutTime").val()) == "H:MM AM/PM"?"":jQuery(this).find(".TechCheckCheckOutTime").val();
            var AddedBySection = jQuery(this).find("#addedbysection").val();
            data += ',{'
            +'"OptionCheckInOut":"'+OptionCheckInOut+'"'
            +',"type":"'+jQuery(this).attr("typetc")+'"'
            +',"CORCheckInDate":"'+ TechCheckCheckInDate +'"'
            +',"CORCheckInTime":"'+ TechCheckCheckInTime +'"'
            +',"CORCheckOutDate":"'+ TechCheckCheckOutDate +'"'
            +',"CORCheckOutTime":"'+ TechCheckCheckOutTime +'"'
            +',"AddedBySection":"'+ AddedBySection +'"'
            +',"woVisitID":"'+ jQuery(this).attr("woVisitID") +'"'
            +',"disable":"'+ jQuery(this).attr("disable") +'"'
            +'}';
        });
        if(data != ""){
            data = data.substring(1,data.length);
        }
        return data;
    }
    //------------------------------------------------------------
    this.TechCheckCheckChange_change = function(ev, event){
    	var is_disabled;
    	
    	if(typeof(event) == 'undefined') event = this;
        event = jQuery(event).parent().parent().parent();
        if($(event).attr("class") === "customSelect")
        	event = jQuery(event).parent().parent().parent();
       
        is_disabled = jQuery(event).attr ("disable");
        
        if (is_disabled != "1") {
	        Me.TimeInput_blur(null,jQuery(event).find(".TechCheckCheckInTime"));
	        Me.TimeInput_blur(null,jQuery(event).find(".TechCheckCheckOutTime"));
	        
	        var TechCheckCheckInDate = jQuery.trim(jQuery(event).find(".TechCheckCheckInDate").val()) == "MM/DD/YYYY"?"":jQuery(event).find(".TechCheckCheckInDate").val();
	        var TechCheckCheckInTime = jQuery.trim(jQuery(event).find(".TechCheckCheckInTime").val()) == "H:MM AM/PM"?"":jQuery(event).find(".TechCheckCheckInTime").val();
	        var TechCheckCheckOutDate = jQuery.trim(jQuery(event).find(".TechCheckCheckOutDate").val()) == "MM/DD/YYYY"?"":jQuery(event).find(".TechCheckCheckOutDate").val();
	        var TechCheckCheckOutTime = jQuery.trim(jQuery(event).find(".TechCheckCheckOutTime").val()) == "H:MM AM/PM"?"":jQuery(event).find(".TechCheckCheckOutTime").val();
	        var dataempty = false;
	        
	        if(TechCheckCheckInDate == "") dataempty = true;
	        if(TechCheckCheckInTime == "") dataempty = true;
	        if(TechCheckCheckOutDate == "") dataempty = true;
	        if(TechCheckCheckOutTime == "") dataempty = true;
	        if(dataempty){
	            jQuery(event).find(".DisplayCORTotalHours").html("");
	            jQuery(event).find(".DisplayCORTotalHours").attr("total",0);
	            Me.TotalDuration();
	            return false;
	        }
	        var data = 'CORCheckInDate='+ TechCheckCheckInDate 
	        +'&CORCheckInTime='+ TechCheckCheckInTime 
	        +'&CORCheckOutDate='+ TechCheckCheckOutDate 
	        +'&CORCheckOutTime='+ TechCheckCheckOutTime; 
	        
	        $.ajax({
	            url : "/widgets/dashboard/details/get-total-hours",
	            data : data,
	            cache : false,
	            type : 'POST',
	            success : function(data) {
	                jQuery(event).find(".DisplayCORTotalHours").html(data.result);
	                jQuery(event).find(".DisplayCORTotalHours").attr("total",data.total);
	                Me.TotalDuration();
	            }
	        });
        }
        else {
            jQuery(event).find(".DisplayCORTotalHours").html("");
            jQuery(event).find(".DisplayCORTotalHours").attr("total", 0);
            Me.TotalDuration();
        }
    }
    //------------------------------------------------------------
    this.TotalDuration = function(preserve_totalhrs){
        var total = 0;
        var displayTotalDuration = "";
        var edit_calculatedTechHrs = "";

        preserve_totalhrs = !!preserve_totalhrs;

        jQuery(".contentTechCheck").each(function(){
            if (jQuery(this).attr("disable") != "1" && 
            	jQuery(this).find(".OptionCheckInOutTech").is(":checked")) {
            	
            	var val = parseInt(jQuery(this).find(".DisplayTechTotalHours").attr("total"));
            	total += isNaN(val) != true ? val : "";
            } else{
            	var val = parseInt(jQuery(this).find(".DisplayCORTotalHours").attr("total"));
                total += isNaN(val) != true ? val : "";
            }
        });
       
        if(total > 0){
            var ost = total%3600;
            displayTotalDuration = (!ost) ? (total/3600)
            +((total/3600) == 1?' hour':' hours') : ((total-ost)/3600)
            +(((total-ost)/3600) == 1?' hour ':' hours ')+Math.round(ost/60)+' min';
            edit_calculatedTechHrs = total/3600;
            if (!preserve_totalhrs) {
	            jQuery("#edit_calculatedTechHrs").val(edit_calculatedTechHrs);
	            jQuery("#edit_Qty_Devices_Display").val(edit_calculatedTechHrs);
            }
        }
        
        jQuery("#displayTotalDuration").html(displayTotalDuration);
        //872
        if(!Me.fb){
            if(jQuery("#edit_Amount_Per option:selected").val() == "Hour" 
            || jQuery("#edit_Amount_Per option:selected").val() == "Site" 
            || jQuery("#edit_Amount_Per option:selected").val() == "Visit" 
            || jQuery("#edit_Amount_Per option:selected").val() == "" 
        ){
                $("#edit_baseTechPay").val("");
                Me.detailsWidget.updateFinalPay();
            }
        }
        //end 872
        Me.fb = false;
    }
    //------------------------------------------------------------
    this.classStartType_onChange = function(){
    /*var event = jQuery(this).parent().parent().parent();
        switch (jQuery(this).val()){
            case "1":
                jQuery(event).find(".DateTypeStart").html("Start Date / Time");
                jQuery(event).find(".DateTypeEnd").html("End Date / Time");
                break;
            case "2":
                jQuery(event).find(".DateTypeStart").html("Arrival Window Starts");
                jQuery(event).find(".DateTypeEnd").html("Arrival Window Ends");
                break;     
        }*/
    }
    //------------------------------------------------------------
    this.TechACSReminderView_onclick = function(){
        var content = '<div id="popupACSServices">'+jQuery("#contentACSServices").html()+"</div>";
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :true,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'content': content
        }
        ).trigger('click');
        jQuery("#contentACSServices .ACSServices").each(function(i){
            if(jQuery(this).is(":checked")){
                jQuery(jQuery("#popupACSServices .ACSServices")[i]).attr('checked',"checked");
            }
        });
    }
    //------------------------------------------------------------
    this.ClientEmailNotification_onclick = function(){
        var content = '<div id="popupClientEmailNotification">'+jQuery("#contentClientEmailNotification").html()+"</div>";
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :true,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'content': content
        }
        ).trigger('click');
            
        jQuery("#contentClientEmailNotification .ClientEmailNotification").each(function(i){
            if(jQuery(this).is(":checked")){
                jQuery(jQuery("#popupClientEmailNotification .ClientEmailNotification")[i]).attr('checked',"checked");
            }
        });
    }

    //------------------------------------------------------------
    this.OptionCheckInOutTech_click = function(){
        var event = jQuery(this).parent().parent().parent();
        jQuery(event).find(".TechCheckCheckInDate").attr("disabled","disabled");
        jQuery(event).find(".TechCheckCheckInTime").attr("disabled","disabled");
        jQuery(event).find(".TechCheckCheckOutDate").attr("disabled","disabled");
        jQuery(event).find(".TechCheckCheckOutTime").attr("disabled","disabled");
        jQuery(event).find(".DisplayTechTotalHours").css("color","black");
        jQuery(event).find(".DisplayCORTotalHours").css("color","gray");
        jQuery(event).find(".csIcon").unbind("click");
        jQuery(event).find(".csIcon").attr("disabled","true");
    }
    this.OptionCheckInOutClient_click = function(){
        var event = jQuery(this).parent().parent().parent();
        var TechCheckCheckInDate = jQuery.trim(jQuery(event).find(".TechCheckCheckInDate").val()) == "MM/DD/YYYY"?"":jQuery(event).find(".TechCheckCheckInDate").val();
        var TechCheckCheckInTime = jQuery.trim(jQuery(event).find(".TechCheckCheckInTime").val()) == "H:MM AM/PM"?"":jQuery(event).find(".TechCheckCheckInTime").val();
        var TechCheckCheckOutDate = jQuery.trim(jQuery(event).find(".TechCheckCheckOutDate").val()) == "MM/DD/YYYY"?"":jQuery(event).find(".TechCheckCheckOutDate").val();
        var TechCheckCheckOutTime = jQuery.trim(jQuery(event).find(".TechCheckCheckOutTime").val()) == "H:MM AM/PM"?"":jQuery(event).find(".TechCheckCheckOutTime").val();
        
        jQuery(event).find(".TechCheckCheckInDate").removeAttr("disabled");
        jQuery(event).find(".TechCheckCheckInTime").removeAttr("disabled");
        jQuery(event).find(".TechCheckCheckOutDate").removeAttr("disabled");
        jQuery(event).find(".TechCheckCheckOutTime").removeAttr("disabled");
        jQuery(event).find(".DisplayTechTotalHours").css("color","gray");
        jQuery(event).find(".DisplayCORTotalHours").css("color","black");
        jQuery(event).find(".csIcon").removeAttr("disabled");
        Me.jCustomSelect(event);
        if(TechCheckCheckInDate == ""
            && TechCheckCheckInTime == ""
            && TechCheckCheckOutDate == ""
            && TechCheckCheckOutTime == ""){
            var TechCheckInDateDisplay = jQuery.trim(jQuery(event).find(".TechCheckInDateDisplay").attr("val"));
            var TechCheckInTimeDisplay = jQuery.trim(jQuery(event).find(".TechCheckInTimeDisplay").attr("val"));
            var TechCheckOutDateDisplay = jQuery.trim(jQuery(event).find(".TechCheckOutDateDisplay").attr("val"));
            var TechCheckOutTimeDisplay = jQuery.trim(jQuery(event).find(".TechCheckOutTimeDisplay").attr("val"));
            
            if( TechCheckInDateDisplay != ""){ 
                jQuery(event).find(".TechCheckCheckInDate").val(TechCheckInDateDisplay);
                jQuery(event).find(".TechCheckCheckInDate").css("background-image", "");
                jQuery(event).find(".TechCheckCheckInDate").css("color","black");
            }
            if(TechCheckInTimeDisplay != ""){
                jQuery(event).find(".TechCheckCheckInTime").val(TechCheckInTimeDisplay);
                jQuery(event).find(".TechCheckCheckInTime").css("background-image", "");
                jQuery(event).find(".TechCheckCheckInTime").css("color","black");
            }
            if(TechCheckOutDateDisplay != ""){ 
                jQuery(event).find(".TechCheckCheckOutDate").val(TechCheckOutDateDisplay);
                jQuery(event).find(".TechCheckCheckOutDate").css("background-image", "");
                jQuery(event).find(".TechCheckCheckOutDate").css("color","black");
            }
            if(TechCheckOutTimeDisplay != "") {
                jQuery(event).find(".TechCheckCheckOutTime").val(TechCheckOutTimeDisplay);
                jQuery(event).find(".TechCheckCheckOutTime").css("background-image", "");
                jQuery(event).find(".TechCheckCheckOutTime").css("color","black");
            }
            jQuery(event).find(".DisplayCORTotalHours").html(jQuery(event).find(".DisplayTechTotalHours").html());
            jQuery(event).find(".DisplayCORTotalHours").attr("total",jQuery(event).find(".DisplayTechTotalHours").attr("total"));
            Me.TotalDuration();  
            if(TechCheckCheckInDate != "") jQuery(event).find(".TechCheckCheckInDate").css("background-image","");
            if(TechCheckCheckInTime != "") jQuery(event).find(".TechCheckCheckInTime").css("background-image","");
            if(TechCheckCheckOutDate != "") jQuery(event).find(".TechCheckCheckOutDate").css("background-image","");
            if(TechCheckCheckOutTime != "") jQuery(event).find(".TechCheckCheckOutTime").css("background-image","");
        }
    }
    //------------------------------------------------------------
    this.removeVisit_onClick = function(){
        //Me.updateOfVisits();
    }
    //------------------------------------------------------------
    this.editQtyDevices_onChange = function(){
        jQuery('#edit_Qty_Visits_Display').val(jQuery("#edit_Qty_Visits").val());
        //var Amount_Per = jQuery("#edit_Amount_Per option:selected").val();
        var value = jQuery("#edit_Qty_Visits").val();
        var numberVisit = jQuery(".tableServiceSchedule").length;
        var checked = jQuery("#ContentServiceSchedule .removeVisit:checked").length;
        var visit = numberVisit - checked;
        if(true){
            value = parseInt(value);
            if(isNaN(value) || value < 1){
                jQuery("#edit_Qty_Visits").val(visit);
                return false;
            }
            // add or remove vitsit Service Schedule
            if(value >= numberVisit){
                // remove checked
                jQuery("#ContentServiceSchedule .removeVisit").removeAttr("checked");
                if(value == numberVisit) return false;
                // add
                var newAdd = value - numberVisit;
                var html = jQuery("#tableServiceSchedule").html();
                var lengthServiceSchedule = jQuery(".tableServiceSchedule").length;
                if(typeof(lengthServiceSchedule) == 'undefined') lengthServiceSchedule = 1;
                var oldlengthServiceSchedule = lengthServiceSchedule + 1;
                var contenthtml = "";
                for(var i = 1;i <= newAdd;i++){
                    lengthServiceSchedule++;
                    var showbuton = (i == newAdd);
                    contenthtml += Me.buildGUIVisit(html,lengthServiceSchedule,showbuton);
                }
                jQuery("#cmdAddAVisit").remove();
                jQuery("#ContentServiceSchedule").append(contenthtml);
                //remove value
                for(var j = oldlengthServiceSchedule;j <= lengthServiceSchedule;j++){
                    jQuery('.tableServiceSchedule'+j+" input:text").val("");
                    jQuery('.tableServiceSchedule'+j+" select").val("");
                    Me.jCustomSelect(jQuery('.tableServiceSchedule'+j));
                    Me.disabledImageFAQi(j);
                }
                Me.eventServiceSchedule();
        
            }else if(value == numberVisit - checked) {
                return false;
            }else if(value > numberVisit - checked) {
//                jQuery("#ContentServiceSchedule .removeVisit").each(function(i){
//                    var j = i+2;
//                    if(j <= value){
//                        jQuery(this).removeAttr("checked");
//                    }
//                });
                    }
            else{
//                jQuery("#ContentServiceSchedule .removeVisit").each(function(i){
//                    var j = i+2;
//                    if(j > value)
//                    {
//                        jQuery(this).attr("checked",true);
//                    }
//                });
                    }
            }
        }
    //------------------------------------------------------------
    this.updateOfVisits = function(){
            var checked = jQuery("#ContentServiceSchedule .removeVisit:checked").length;
            var numberVisit = jQuery(".tableServiceSchedule").length;
            var visit = numberVisit - checked;
        jQuery("#edit_Qty_Visits").val(visit);
       
        }
    //-----------------------------------------------------------
    this.setEditAmountPerContent = function(){
        $('#edit_Amount_Per').attr('title', $('#edit_Amount_Per option:selected').text());
                                                                                                                                                                                                                                                                                                                    
        if($('#edit_Amount_Per option:selected').val()=='Visit')
        {
            $('#perLabel').html('per Visit');
            $('#perLabel2').html('per Visit');
            //$('#DeviceQuantity').html('# of Visits');
            $('#htmlCalculatedTotalHours').html('# of Visits');
            jQuery('#edit_Qty_Visits_Display').val(jQuery('#edit_Qty_Visits').val());
            //jQuery('#edit_Qty_Devices_Display').show();
            jQuery('#edit_calculatedTechHrs').hide();
        }
        else if($('#edit_Amount_Per option:selected').val()=='Device')
        {
            $('#htmlCalculatedTotalHours').html('Device Quantity');
            jQuery('#edit_Qty_Devices_Display').val(jQuery('#edit_Qty_Devices').val());
            jQuery('#edit_Qty_Devices_Display').show();
            jQuery('#edit_calculatedTechHrs').hide();
        }    
        else
        {
            $('#htmlCalculatedTotalHours').html('Calculated Total Hours');
            jQuery('#edit_Qty_Devices_Display').hide();
            jQuery('#edit_calculatedTechHrs').show();
            Me.TotalDuration();
        }    
        if($('#edit_Amount_Per option:selected').val()!='Visit')
        {
            $('#perLabel').html('per '+$('#edit_Amount_Per option:selected').val());
            $('#perLabel2').html('per '+$('#edit_Amount_Per option:selected').val());
            //$('#DeviceQuantity').html('Device Quantity');
            $('#basetechpay1').html('Base Tech Pay:');
        }
                                                                                                                                                                                                                                                                    
        if ($('#edit_Amount_Per option:selected').val() == 'Device' || $('#edit_Amount_Per option:selected').val() == 'Visit') {
            $('#edit_Qty_Devices').removeAttr('readonly');
        }else{
            $('#edit_Qty_Devices').attr('readonly','readonly').val('');
        }
    }
    //------------------------------------------------------------
    this.EditAmountPer_onChange = function(){
        Me.setEditAmountPerContent();
        //Me.updateOfVisits();
    }
    //-----------------------------------------------------------
    this.buildEaventWidgetWODetails = function(){
        jQuery("#edit_Amount_Per").unbind("change",Me.EditAmountPer_onChange)
        .bind("change",Me.EditAmountPer_onChange);
        jQuery("#edit_Qty_Visits").unbind("change",Me.editQtyDevices_onChange)
        .bind("change",Me.editQtyDevices_onChange);
        jQuery("#edit_Qty_Visits_Display").unbind("change",Me.editQtyDevices_onChange)
        .bind("change",Me.editQtyDevices_onChange);
        //Me.updateOfVisits();
    }
    //------------------------------------------------------------
    this.autoFillProjectVisit = function(data){
        if(data["StartType"] == null) data["StartType"] = "";
        if(data["WorkStartTime"] == null) data["WorkStartTime"] = "";
        if(data["WorkEndTime"] == null) data["WorkEndTime"] = "";
        //if(data["EstimatedDuration"] != null)  data["EstimatedDuration"] = Math.round(data["EstimatedDuration"]*100)/100;
        //if(data["TechArrivalInstructions"] == null) data["TechArrivalInstructions"] = "";
        jQuery("#tableServiceSchedule .classStartType").val(data["StartType"]);
        
        if(data["EstimatedDuration"] != null)
        	jQuery("#tableServiceSchedule .EstimatedDuration").val(Math.round(data["EstimatedDuration"]*100)/100);
        if(data["TechArrivalInstructions"] != null)
        jQuery("#tableServiceSchedule .TechArrivalInstructions").val(data["TechArrivalInstructions"]);
        if(jQuery.trim(data["WorkEndTime"]) != ""){
	        jQuery("#tableServiceSchedule .EndTime").val(data["WorkEndTime"]);
            jQuery("#tableServiceSchedule .EndTime").css("background-image","");
        }
        if(jQuery.trim(data["WorkStartTime"]) != ""){
	        jQuery("#tableServiceSchedule .StartTime").val(data["WorkStartTime"]);
            jQuery("#tableServiceSchedule .StartTime").css("background-image","");
        }
        if(jQuery.trim(data["WorkEndTime"]) == "" ||
            jQuery.trim(data["WorkStartTime"]) == ""){
            detailObject.onInit.watermark();
        }
    }
    //------------------------------------------------------------
    this.FSAutoFillFLS_onChange = function(){
        var techid = jQuery(this).val();
        var totextid = jQuery(this).attr("totextid");
        $.ajax({
            url: '/widgets/dashboard/do/get-techid-fls/',
            data:"techid="+techid,
            type: 'post',
            success     : function (data) {
                jQuery("#"+totextid).val(data.techflsid);
            }
        });
    }
    //------------------------------------------------------------
    this.edit_FLS_ID_onChange = function(){
        if ($(this).val() != '') {
            var techId = $(this).val();
            $.ajax({
                url:"/widgets/dashboard/popup/get-tech-fls-info?tech="+techId, 
                success: function(data){
                    if(data['error'] == 0 ) {
                        jQuery(".cmdtechScheduleImg").attr("src","/widgets/css/images/Btn_ViewTechSchedule.png");
                        jQuery(".cmdtechScheduleImg").unbind("click").bind("click",showTechSched);
                        jQuery(".cmdtechScheduleImg").css("cursor","pointer");
                        jQuery(".saveTechToSiteImg").css("cursor","pointer");
                        jQuery(".saveTechToSiteImg").attr("src","/widgets/css/images/Btn_SaveTech.png");
                        jQuery(".saveTechToSiteImg").unbind("click").bind("click",_wosDetails.saveTechToSiteImg_onclick);
                        $("#edit_Tech_FName").val(data['techInfo']['fName']);
                        $("#edit_Tech_ID").val(data['techInfo']['TechID']);
                        $("#edit_Tech_LName").val(data['techInfo']['lName']);
                        $("#edit_TechEmail").val(data['techInfo']['email']);
                        $("#edit_FLS_ID").val(data['techInfo']['FLSID']);
                        detailObject.onInit.RequitePhone("#edit_TechPhone"
                            ,data['techInfo']['phone']
                            ,data['techInfo']['TechID']
                            ,data['techInfo']['PrimaryPhoneStatus']
                            ,1
                            ,'text'
                            ,Base64.encode(data['techInfo']['fName']+' '+data['techInfo']['lName']));
                        $("#techScheduleImg").css('visibility','visible');
                        if (typeof(data['bidInfo']) != 'undefined') {
                            if ($("#edit_bid_comments").val() == '') {
                                $("#edit_bid_comments").val(data['bidInfo']['comments']);
                            }
                        }
                        if (data['techInfo']['isDenied']) {
                            detailsWidget._redirect = 0;
                            detailsWidget.showMessages({
                                success:0,
                                errors:['You are about to assign work to a technician that has been "denied access" by your company. Please re-confirm your choice of technician. Tech ID: '+techId+' ('+data['techInfo']['fName']+' '+data['techInfo']['lName']+')']
                            });
                            if( $("#edit_DenyTechV").attr("type") == "checkbox"){ 
                                $("#edit_DenyTechV").attr("checked","checked");
                            }else{
                                $("#edit_DenyTechV").val("1");
                            }
                        }else{
                            if( $("#edit_DenyTechV").attr("type") == "checkbox"){ 
                                $("#edit_DenyTechV").attr("checked",false);
                            }else{
                                $("#edit_DenyTechV").val("");
                            }
                        }
                        if(data['techInfo']['isPreferred'] == true) {
                            if( $("#edit_PreferTechV").attr("type") == "checkbox"){ 
                                $("#edit_PreferTechV").attr("checked","checked");
                            }else{
                                $("#edit_PreferTechV").val("1");
                            }
                        }else{
                            if( $("#edit_PreferTechV").attr("type") == "checkbox"){ 
                                $("#edit_PreferTechV").attr("checked",false);
                            }else{
                                $("#edit_PreferTechV").val("");
                            }
                        }
                    }else{
                        jQuery(".cmdtechScheduleImg").css("cursor","default");
                        jQuery(".saveTechToSiteImg").css("cursor","default");
                        jQuery(".cmdtechScheduleImg").unbind("click");
                        jQuery(".cmdtechScheduleImg").attr("src","/widgets/css/images/Btn_ViewTechSchedule_g.png");
                        jQuery(".saveTechToSiteImg").attr("src","/widgets/css/images/Btn_SaveTech_g.png");
                        jQuery(".saveTechToSiteImg").unbind("click");
                        $("#edit_Tech_ID").val('');
                        $("#edit_Tech_FName").val('');
                        $("#edit_Tech_LName").val('');
                        $("#edit_TechEmail").val('');
                        $("#edit_TechPhone").val('');
                        $("#edit_FLS_ID").val('');
                        jQuery("#edit_TechPhone").parent().children(".imgClassReQuitePhone").remove();
                    }
                }
            });
        } else {
            jQuery(".cmdtechScheduleImg").css("cursor","default");
            jQuery(".saveTechToSiteImg").css("cursor","default");
            jQuery(".cmdtechScheduleImg").unbind("click");
            jQuery(".cmdtechScheduleImg").attr("src","/widgets/css/images/Btn_ViewTechSchedule_g.png");
            jQuery(".saveTechToSiteImg").attr("src","/widgets/css/images/Btn_SaveTech_g.png");
            jQuery(".saveTechToSiteImg").unbind("click");
                                                                                                                                                                                                                                                                                                                                    
            $("#edit_Tech_ID").val('');
            $("#edit_Tech_FName").val('');
            $("#edit_Tech_LName").val('');
            $("#edit_TechEmail").val('');
            $("#edit_TechPhone").val('');
            $("#edit_FLS_ID").val('');
            jQuery("#edit_TechPhone").parent().children(".imgClassReQuitePhone").remove();
                       
        }
    }
    //------------------------------------------------------------
    this.Backup_FLS_ID_onChange = function(){
        var totextid = jQuery(this).attr("totextid");
        var event = this;
        if ($(this).val() != '') {
            var techId = $(this).val();
            $.ajax({
                url:"/widgets/dashboard/popup/get-tech-fls-info?tech="+techId, 
                success: function(data){
                    if(data['error'] == 0 ) {
                        jQuery("#"+totextid).val(data['techInfo']['TechID']);
                    }else{
                        jQuery(event).val("");
                        jQuery("#"+totextid).val("");
                    }
                }
            });
        } else {
            jQuery(event).val("");
            jQuery("#totextid").val("");
        }
    }
    //-----------------------------------------------------------
    this.tinyMCEEditor = function(_class){
        var tool = "bold,italic,underline,|,bullist,numlist,|,removeformat";
        if(_class == "txtarea_Ship_Contact_Info"){
            tool = "bold,italic,underline,link,|,bullist,numlist,|,removeformat";
        }
        tinyMCE.init({
            // General options
			valid_elements : "@[id|class|style|title|dir<ltr?rtl|lang|xml::lang|onclick|ondblclick|"
				+ "onmousedown|onmouseup|onmouseover|onmousemove|onmouseout|onkeypress|"
				+ "onkeydown|onkeyup],strong/b,em/i,strike,u,"
				+ "#p,-ol[type|compact],-ul[type|compact],-li,br,img[longdesc|usemap|"
				+ "src|border|alt=|title|hspace|vspace|width|height|align],-sub,-sup,"
				+ "-blockquote,-table[border=0|cellspacing|cellpadding|width|frame|rules|"
				+ "height|align|summary|bgcolor|background|bordercolor],-tr[rowspan|width|"
				+ "height|align|valign|bgcolor|background|bordercolor],tbody,thead,tfoot,"
				+ "#td[colspan|rowspan|width|height|align|valign|bgcolor|background|bordercolor"
				+ "|scope],#th[colspan|rowspan|width|height|align|valign|scope],caption,-div,"
				+ "-span,-code,-pre,address,-h1,-h2,-h3,-h4,-h5,-h6,hr[size|noshade],-font[face"
				+ "|size|color],dd,dl,dt,cite,abbr,acronym,del[datetime|cite],ins[datetime|cite],"
				+ "object[classid|width|height|codebase|*],param[name|value|_value],embed[type|width"
				+ "|height|src|*],script[src|type],map[name],area[shape|coords|href|alt|target],bdo,"
				+ "button,col[align|char|charoff|span|valign|width],colgroup[align|char|charoff|span|"
				+ "valign|width],dfn,fieldset,form[action|accept|accept-charset|enctype|method],"
				+ "input[accept|alt|checked|disabled|maxlength|name|readonly|size|src|type|value],"
				+ "kbd,label[for],legend,noscript,optgroup[label|disabled],option[disabled|label|selected|value],"
				+ "q[cite],samp,select[disabled|multiple|name|size],small,"
				+ "textarea[cols|rows|disabled|name|readonly],tt,var,big",
        	mode : "specific_textareas",
            editor_selector :  /(txtarea_Description)/,
            theme : "advanced",
            plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
            // Theme options
            theme_advanced_buttons1 : tool,
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,
            theme_advanced_resize_horizontal : false,
            width : "638",
            height : "100",
            theme_advanced_path : false,
            convert_urls: false,
            relative_urls : false,
            // Skin options
            skin : "o2k7",
            skin_variant : "silver"
        });
    }
    //------------------------------------------------------------
    this.showLoader = function(width,height,showCloseButton)
    {
        var content = "<div style='width:"+width+"px;' id='comtantPopupID'><div style='text-align:center;height:"+height+"px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :showCloseButton,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'content': content
        }
        ).trigger('click');
        

    }
    //------------------------------------------------------------
    this.hideLoader = function()
    {
        $.fancybox.close();
    }
    //------------------------------------------------------------
    this.cmdCancelPopup_click = function(){
        Me.hideLoader();
    }
    //------------------------------------------------------------
    this.edit_Project_ID_onChange = function(){ 
        Me.showLoader(350,60,true);
        var msg = "Do you want the new project to overwrite existing Work Order information?";
        if(window._company == "FLS"){
            msg = "Would you like to overwrite all project data?";
        }
        var html = '<div>\
                        <div>'+msg+'</div>\
                        <div style="color:red;padding-left:10px;" id="divError"></div><div style="text-align:center;padding-top:5px;">\
                            <input type="button" id="cmdCancelPopup" class="link_button_popup" value="No"  />\
                            &nbsp;&nbsp;&nbsp;&nbsp;\
                            <input type="button" id="cmdProjectOk" class="link_button_popup" value="Yes"/>\
                        </div>\
                    </div>';
        jQuery("#comtantPopupID").html(html);
        jQuery("#comtantPopupID #cmdCancelPopup")
        .unbind("click",Me.cmdCancelPopup_click)
        .bind("click",Me.cmdCancelPopup_click);
        jQuery("#comtantPopupID #cmdProjectOk")
        .unbind("click",Me.ProjectAppect_click)
        .bind("click",Me.ProjectAppect_click);
    }

    //------------------------------------------------------------
    this.ProjectAppect_click = function() {
        Me.ShowWaitingSave();
        jQuery("#comtantPopupID #divError").html("");
        clearDefaultProjectFields();
        $('#edit_Project_ID').attr('title', $('#edit_Project_ID option:selected').text());
        Me.detailsWidget.autofillProjectInfo($('#edit_Project_ID option:selected').val(),null,Me.updateProjectWorkorder);
    }
    //------------------------------------------------------------
    this.ShowWaitingSave = function(){
        if(jQuery("#comtantPopupID #WaitingSave").length <= 0){
            jQuery("#comtantPopupID").append('<div id="WaitingSave" style="position: absolute; width: 100%; text-align: center; left: 0px; height: 100%; top: 0px;display:none;"><div style="height:100%;background:url(\'/widgets/images/loader.gif\') no-repeat scroll center center transparent;">&nbsp;</div></div>');
        }
        jQuery("#comtantPopupID #WaitingSave").show();
    }
    //------------------------------------------------------------
    this.HideWaitingSave = function(){
        jQuery("#comtantPopupID #WaitingSave").hide();
    }
    //------------------------------------------------------------
    this.updateProjectWorkorder = function(){
        var ServiceSchedule = Me.getDataServiceSchedule();
        var AllWOVisitList = Me.getAllWOVisitList();
        Me.setWoDataFromFirstVisitData();
        var params = $('#updateDetailsForm').serialize();
        params.company = window._company;
        params += "&Description="+encodeURIComponent(tinyMCE.get('edit_Description').getContent());
        params += "&Requirements="+encodeURIComponent(tinyMCE.get('edit_Requirements').getContent());
        params += "&SpecialInstructions="+encodeURIComponent(tinyMCE.get('edit_SpecialInstructions').getContent());
        params += "&Ship_Contact_Info="+encodeURIComponent(tinyMCE.get('edit_Ship_Contact_Info').getContent());
        params += ServiceSchedule != ""?"&ServiceSchedule="+Base64.encode(ServiceSchedule):"";
        params += AllWOVisitList != ""?"&AllWOVisitList="+Base64.encode(AllWOVisitList):"";
        params += "&isProjectAutoAssign="+(jQuery("#edit_isProjectAutoAssign").is(":checked")?1:0);
        params += "&notP2TPreferredOnly="+(jQuery("# edit_notP2TPreferredOnly").is(":checked")?1:0);
        params += "&isWorkOrdersFirstBidder="+(jQuery("#edit_isWorkOrdersFirstBidder").is(":checked")?1:0);
        params += "&P2TPreferredOnly="+(jQuery("#edit_P2TPreferredOnly").is(":checked")?1:0);
        params += "&isnotWorkOrdersFirstBidder="+(jQuery("#edit_isnotWorkOrdersFirstBidder").is(":checked")?1:0);
        params += "&NotifyOnAutoAssign="+(jQuery("#edit_NotifyOnAutoAssign").is(":checked")?1:0);
        $.ajax({
            url         : '/widgets/dashboard/details/update-project-workorder/',
            dataType    : 'json',
            data        : params,
            cache       : false,
            type        : 'post',
            error : function (xhr, status, error) {
                alert(xhr);
                Me.HideWaitingSave();
            },
            success     : function (data, status, xhr) {
                Me.HideWaitingSave();
                if (data.success) {
                    window.location.reload();
                }else{
                    for(var i = 0; i < data.errors.length;i++){
                        jQuery("#comtantPopupID #divError").append("<div>"+data.errors[i]+"</div>");
                    }
                }
            }
        });
    }
    //------------------------------------------------------------
    this.CalendarInputStartDate_onChange = function(){
        /* Begin Sourceby date code - CM  */
        var startDate = new Date($(this).val());
        if (!startDate)
            return;

        var today = new Date();
        var dayNames = new Array(1,2,3,4,5,6,7);
        var checked = false;
        var i = 0;
        var day = today.getDate();
        var month = today.getMonth();
        var year = today.getFullYear();
        var dayInWeek = dayNames[today.getDay()];
        var startday = day - (dayInWeek - 1);
        while ( i < 7){
            var _startday = startday + i;
            var _date = new Date(year, month, _startday);
            if(startDate.valueOf() == _date.valueOf()){
                checked = true;
                break;
            }
            i++;
        }
        if(checked) {
            $("#edit_ShortNotice").attr("checked","checked");
        }else{
            $("#edit_ShortNotice").removeAttr("checked");
        }   
    }
    this.edit_ShowTechs_click = function(){
        var event = this;
        _global.accessControlClient(2,null,null,function(){
            jQuery(event).attr("checked",Me.ShowTechs);
        });
    }
    this.ShowTechs = false;
    //------------------------------------------------------------
    this.buildEvent = function(){
        Me.fb = true;
        jQuery(".saveTechToSiteImg")
        .unbind("click",Me.saveTechToSiteImg_onclick)
        .bind("click",Me.saveTechToSiteImg_onclick);
        jQuery(".viewTechBtnHidden")
        .unbind("click",Me.viewTechBtnHidden_onclick)
        .bind("click",Me.viewTechBtnHidden_onclick); 
        jQuery(".cmdsaveSite")
        .unbind("click",Me.cmdsaveSite)
        .bind("click",Me.cmdsaveSite);
        jQuery("#updateDetailsForm .updateButton")
        .unbind("click",Me.updateButton)
        .bind("click",Me.updateButton);
        jQuery("#createDetailsForm .updateButton")
        .unbind("click",Me.createButton)
        .bind("click",Me.createButton);
       
        $("#TechACSReminderView").unbind("click",Me.TechACSReminderView_onclick)
        .bind("click",Me.TechACSReminderView_onclick);
        $("#ClientEmailNotification").unbind("click",Me.ClientEmailNotification_onclick)
        .bind("click",Me.ClientEmailNotification_onclick);
        $("#divcreateDetailsForm #CalendarInput").unbind("change",Me.CalendarInputStartDate_onChange)
        .bind("change",Me.CalendarInputStartDate_onChange); 
        
        Me.eventServiceSchedule();
        Me.jCustomSelect();
            
        $(".FSAutoFillFLS").unbind("change",Me.FSAutoFillFLS_onChange)
        .bind("change",Me.FSAutoFillFLS_onChange);    
        $("#edit_FLS_ID").unbind("change",Me.edit_FLS_ID_onChange)
        .bind("change",Me.edit_FLS_ID_onChange);   
        $(".Backup_FLS_ID").unbind("change",Me.Backup_FLS_ID_onChange)
        .bind("change",Me.Backup_FLS_ID_onChange); 
        Me.TotalDuration(true);
        Me.buildEaventWidgetWODetails();

        //Me.buildEditor();
        if(Me.detailsWidget.currentTab == "index"){
            jQuery("#edit_Project_ID").unbind("change",Me.edit_Project_ID_onChange)
            .bind("change",Me.edit_Project_ID_onChange);
            Me.ProjectID = jQuery("#edit_Project_ID option:selected").val();
        }
        jQuery("#edit_ShowTechs").unbind("click",Me.edit_ShowTechs_click)
        .bind("click",Me.edit_ShowTechs_click);
        Me.ShowTechs = jQuery("#edit_ShowTechs").is(":checked");
         Me._wintags = wintags.CreateObject({id:"_wintags",reload:true});
//       if(Me.detailsWidget.currentTab == "index" && jQuery("#showWinTags").val() == "1"){
//           Me.openAddWintags();
//       }
         
         jQuery("#edit_TechMarkedComplete").unbind("click", Me.updateBaseTechPayClick)
         .bind("click", Me.updateBaseTechPayClick);
    }
    this._OpenAddWintags = false;
    //------------------------------------------------------------
    this.cbxContentWintagsNew = function(){
        Me._wintags.dataCompletionStatus("#divContentWintagsNew","PopupNew",this);
    }
    this.cmdDeleteWinTagsCompletionStatusNew = function(){
        Me._wintags.DeleteWinTagsCompletionStatus("#cbxContentWintagsNew",this);
    }
    this.cmdSaveWintagsNew = function(){
        var win = jQuery("#WIN_NUM").val();
        _global.data = jQuery("#frmWintagsNew").serialize()+"&win="+win;
        _global.url = "/widgets/dashboard/details/win-tags";

        _global.onSuccess = function(data){
            _global.hideLoader();
            //_global.showMessages(data);
            detailsWidget.updateDetails();
        };
        _global.show(this);
    }
    //------------------------------------------------------------
    this.openAddWintags = function(){
        var win = jQuery("#WIN_NUM").val();
        _global.data = "win="+win;
        _global.url = "/widgets/dashboard/details/win-tags";
        var _onSuccess = function(data){
            jQuery("#comtantPopupID").html(data); 
            jQuery("#cbxContentWintagsNew").unbind("change",Me.cbxContentWintagsNew).bind("change",Me.cbxContentWintagsNew);
            jQuery(".cmdDeleteWinTagsCompletionStatusPopupNew").unbind("click").bind("click",Me.cmdDeleteWinTagsCompletionStatusNew);
            jQuery("#cmdCancelWintagsNew").click(function(){
                _global.hideLoader();
            });
            jQuery("#frmWintagsNew #cmdSaveWintagsNew").unbind("click").bind("click",Me.cmdSaveWintagsNew);
        }
        _global.onSuccess = _onSuccess;
        _global.showLoader(500,"",true);
        _global.show();
    }
    //------------------------------------------------------------
    this.buildEditor = function(){
        //if(jQuery("#edit_Description").parent().find("iframe").length > 0) return;
        Me.tinyMCEEditor('txtarea_Description');
        Me.tinyMCEEditor('txtarea_Requirements');
        Me.tinyMCEEditor('txtarea_SpecialInstructions');
        Me.tinyMCEEditor('txtarea_Ship_Contact_Info');
    }
    this._wintags = null;
    //------------------------------------------------------------
    this.buildEventext = function(){
        detailObject.onInit.watermark();
        $('.CalendarInput').calendar({
            dateFormat:'MDY/'
        }); 
        jQuery(".TimeInput")
        .unbind("blur",Me.TimeInput_blur)
        .bind("blur",Me.TimeInput_blur);
        Me.jCustomSelect();
       
    }
    
    //------------------------------------------------------------
    this.updateBaseTechPayClick = function(){
        var el = jQuery("#edit_TechMarkedComplete");
        if(el.is(":checked") == true){
        	 var mult = $("#edit_Amount_Per option:selected").val();
        	    if ( mult == "Device")
        	    {
        	        mult = parseFloat($("#edit_Qty_Devices").val());
        	    }
        	    else if (mult == "Visit") {
        	        mult = parseFloat($("#edit_Qty_Visits_Display").val());
        	    }
        	    else
        	    {
        	        if ( mult == "Hour" )
        	        {
        	            mult = parseFloat(($("#edit_calculatedTechHrs").val()=="")?0:$("#edit_calculatedTechHrs").val());
        	        }
        	        else {mult = 1;}
        	    }
        	$("#edit_baseTechPay").val(mult*$("#edit_Tech_Bid_Amount").val());
            $("#edit_baseTechPay").formatCurrency({groupDigits:false, symbol:'', negativeFormat: '-%n'});
        }else{
        	// reset base tech pay
        	$("#edit_baseTechPay").val("");
        }
    }
    //------------------------------------------------------------
    this.init = function() {
 

    }
}