<?php $page = clients; ?>
<?php $option = projects; ?>
<?php $selected = 'projectsView'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php require ("includes/adminCheck.php"); ?>
<?php
	$displayPMTools = true;
	require_once("includes/PMCheck.php");

	require_once("../library/caspioAPI.php");
	$customerList = caspioSelectAdv("Customers", "UNID, Customer_Name", "Company_ID = '" . caspioEscape($_GET["v"]) . "'", "Customer_Name ASC", false, "\"", "^");
	$customerList = str_replace("\"", "", implode("|", $customerList));
	require_once("../library/recruitmentEmailLib.php");

	$activeStatus = $_GET["Active"];
	echo "<script type=\"text/javascript\"> Active = " . $activeStatus . ";</script>";
?>
<!-- Add Content Here -->
<script type="text/javascript" src="../library/jquery/jquery.simplemodal-1.3.min.js"></script>
<style type="text/css">
	.optionBtns {
		width: 90%;
		text-align: center;
		margin: 0px auto;
	}
	#optionMessage {
		margin: 15px 0px 20px 0px;
		font-size: 15px;
		font-weight: bold;		
	}
	.optionBtns div {
		margin: 5px 0px;
	}
	
	.cbButton
	{
		/*Back Button Attributes*/
		color: #ffffff;
		font-size: 12px;
		font-family: Verdana;
		font-style: normal;
		font-weight: bold;
		text-align: center;
		vertical-align: middle;
		border-color: #5496C6;
		border-style: solid;
		border-width: 1px;
		background-color: #032D5F;
		/* Forced by default to add space between buttons */
		width: auto;
		height: auto;
		margin: 0 3px;
	}
	
	.cbButton:hover {
		background-color: #234D7F;
	}
</style>


<div align="center">

<br /><br />

<!-- update_projects_admin.js -->
<script type="text/javascript">
amount_per_selector_field_name = "select#EditRecordAmount_Per";
device_quantity_field_name = "input#EditRecordQty_Devices";
</script>

<script type="text/javascript" src="/update_projects_admin.js">
</script>
<!-- end update_projects_admin.js -->


<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b0000905b079cab8f42b6898b","https:");}catch(v_e){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000905b079cab8f42b6898b">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>
</div>

<script type="text/javascript">
try {
	if (isPM) {
		var customerList = "<?=$customerList?>";
		/* build dropdown using project list */
		var element_dropdown = "EditRecordClient_ID";
		
		/* build dropdown using project list */
		$("#" + element_dropdown).empty();
		index = 1;
		selIndex = 0;
		$("#" + element_dropdown).append("<option value=\"\">Select Company</option>");
		
		var customers = customerList.split("|"); 
		for (i = 0; i < customers.length; i++) {  
			var parts = customers[i].split("^");  
			var customer_id = parts[0];  
			var customer_name = parts[1];  
			htmlEle = "<option value=\"" + customer_id + "\">" + customer_name + "</option>";
			$("#" + element_dropdown).append(htmlEle);
			if (selectedCID == customer_id)
				selIndex = index;
			index++;
		}
		document.getElementById(element_dropdown).selectedIndex = selIndex;
	}
}
catch (e) {}

</script>

<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->