<?php 

require_once ("../headerStartSession.php");
require_once("../library/googleMapAPI.php");
require '../header_clients.php';

$qs = $_SERVER['QUERY_STRING'];

?>
<script src="<?=empty($_SERVER['HTTPS']) ? 'http' : 'https'?>://maps.googleapis.com/maps/api/js?client=<?=$googleClientId?>&sensor=false&v=3.7" type="text/javascript"></script>
<script src="<?=empty($_SERVER['HTTPS']) ? 'http' : 'https'?>://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<link rel="stylesheet" href="<?=empty($_SERVER['HTTPS']) ? 'http' : 'https'?>://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/ui-lightness/jquery-ui.css" type="text/css" />

<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetDashboard.js"></script>
<script type="text/javascript" src="/widgets/js/FSTechMapper.js"></script>
<script type="text/javascript" src="/clients/mapping/js/MarkerClusterer.js"></script>
<script type="text/javascript" src="/clients/mapping/js/PolygonPointDetect.js"></script>
<script type="text/javascript" src="/clients/mapping/js/infobubble.js"></script>

<style type="text/css">
#mainContainer{
	width: 950px;
}
#fsMapperHeader{
	margin-left:auto;
	margin-right:auto;
	text-align:center;
	font-size: 18px;
	text-decoration:underline;
	margin-bottom: 8px;
	font-weight:bold;
}
#googleMap{
	width: 815px; 
	height: 400px; 
	border-top: 3px solid #4790D4;
}
#woSectionHeader{
	font-size: 14px;
	text-decoration:underline;
}
#mapperFooter{
	width: 815px;
}
#woSection{
	width: 815px;
	margin-bottom: 20px;
}
#mapperFooterBorder{
	width: 815px;
	border-top: 2px solid #2A497D;
	margin-bottom: 8px;
}
#topNavBar{
	visibility:hidden;
}
</style>

<script type="text/javascript">

var mapLoaded = false;
currTab = "map";

$("body").data("currentQueryString", '<?= $qs?>');

function runSearch(){
	var qs = '<?= $qs?>';

	techMapper._loadMoreTechsShowing = "";
	techMapper._loadMoreTechsPage = "";


		//showLoader();
		
		$.ajax({
			type: "POST",
			url: "/widgets/dashboard/client/client-tech-search/",
        	dataType    : 'json',
       	 	cache       : false,
			data: qs,
			success: function (data) {

				if(data.mapView != null && data.mapView != ""){
					
					techMapper.setTechPopups(data.mapView);
												
				}
			}
		});
}


$(document).ready(function() {
	
	techMapper = new FSTechMapper({mapContainer: "googleMap", companyID: "BW"});
	techMapper.initMap();
	runSearch();
});
</script>

<div id="mainContainer">
	<div id="fsMapperHeader">FS-Mapper&trade;</div>
	<div id="googleMap"></div>
	<div id="woSection">
		<table>
			<tr>
				<td id="woSectionHeader">Work Order Section</td>
			</tr>
			<tr>
				<td>Projects(s):</td>
				<td>McDonaldsDigital Signage Install<br />BK Monitor Swap<br />Wendy's POS Install</td>
			</tr>
			<tr>
				<td>Work Order Stage:</td>
				<td>Published</td>
			</tr>
			<tr>
				<td>Start Date:</td>
				<td>Published</td>
			</tr>
			<tr>
				<td>Region:</td>
				<td>southwest</td>
			</tr>
			<tr>
				<td>Bundle:</td>
				<td>bundle1</td>
			</tr>
		</table>
	</div>
	<div id="mapperFooterBorder"></div>
	<div id="mapperFooter">
		<span style="float:left;">Field Solutions, Inc. Confidential</span>
		<span style="float:right;">7/6/2012</span>
	</div>

</div>