<?php
// Created on: 11-26-07 
// Author: CM
// Description: If the workorder is changed the tech and call coordinator are notified of the change


try {

require_once("../library/caspioAPI.php");
require_once("../library/smtpMail.php");

$TB_UNID = $_REQUEST['TB_UNID'];
$techID = $_REQUEST['techID'];
$ProjectNo = $_REQUEST['ProjectNo'];
$missingInfo = $_REQUEST['missingInfo'];

$StartDate = $_REQUEST['StartDate'];
$StartTime = $_REQUEST['StartTime'];
$SiteAddress = $_REQUEST['SiteAddress'];
$SiteCity = $_REQUEST['SiteCity'];
$SiteState = $_REQUEST['SiteState'];
$SiteZip = $_REQUEST['SiteZip'];
$PayAmount = $_REQUEST['PayAmount'];
$SpecialInstructions = $_REQUEST['SpecialInstructions'];


$BaseTechPay = $_REQUEST['BaseTechPay'];
$OutOfScope = $_REQUEST['OutOfScope'];
$TripCharge = $_REQUEST['TripCharge'];
$MileageCharge = $_REQUEST['MileageCharge'];
$MaterialsCharge = $_REQUEST['MaterialsCharge'];
$AbortFeeAmount = $_REQUEST['AbortFeeAmount'];
$Additional_Pay_Amount = $_REQUEST['Additional_Pay_Amount'];


$Add_Pay_Reason = $_REQUEST['Add_Pay_Reason'];
$lastModBy = $_REQUEST['lastModBy'];

$approved = ($_REQUEST["Approved"] == 1);
$whatChanged = "";
$whatChangedTech = "";

// Process incoming date

if ($StartDate != ""){
	$StartDate = "Start Date: $StartDate";
	$whatChanged = "$whatChanged<br> $StartDate";
	$whatChangedTech = "$whatChangedTech<br> $StartDate";
	}
if ($StartTime != ""){
	$StartTime = "Start Time: $StartTime";
	$whatChanged = "$whatChanged<br> $StartTime";
	$whatChangedTech = "$whatChangedTech<br> $StartTime";
	}
if ($SiteAddress != ""){
	$SiteAddress = "Site Address: $SiteAddress";
	$whatChanged = "$whatChanged<br> $SiteAddress";
	$whatChangedTech = "$whatChangedTech<br> $SiteAddress";
	}	
if ($SiteCity != ""){
	$SiteCity = "City: $SiteCity";
	$whatChanged = "$whatChanged<br> $SiteCity";
	$whatChangedTech = "$whatChangedTech<br> $SiteCity";
	}	
if ($SiteState != ""){
	$SiteState = "State: $SiteState";
	$whatChanged = "$whatChanged<br> $SiteState";
	$whatChangedTech = "$whatChangedTech<br> $SiteState";
	}
if ($SiteZip != ""){
	$SiteZip = "Zipcode: $SiteZip";
	$whatChanged = "$whatChanged<br> $SiteZip";
	$whatChangedTech = "$whatChangedTech<br> $SiteZip";
	}
if ($PayAmount != ""){
	$PayAmount = "Pay Amount: $PayAmount";
	$whatChanged = "$whatChanged<br> $PayAmount";
	if ($approved)
		$whatChangedTech = "$whatChangedTech<br> $PayAmount";
	}	
if ($SpecialInstructions != ""){
	$SpecialInstructions = "Special Instructions: $SpecialInstructions";
	$whatChanged = "$whatChanged<br> $SpecialInstructions";
	$whatChangedTech = "$whatChangedTech<br> $SpecialInstructions";
	}	
	
	
if ($BaseTechPay != ""){
	$BaseTechPay = "Base Tech Pay: $BaseTechPay";
	$whatChanged = "$whatChanged<br> $BaseTechPay";
	if ($approved)
		$whatChangedTech = "$whatChangedTech<br> $BaseTechPay";
	}

if ($OutOfScope != ""){
	$OutOfScope = "Out of Scope Amount: $OutOfScope";
	$whatChanged = "$whatChanged<br> $OutOfScope";
	if ($approved)
		$whatChangedTech = "$whatChangedTech<br> $OutOfScope";
	}

if ($TripCharge != ""){
	$TripCharge = "Trip Charge: $TripCharge";
	$whatChanged = "$whatChanged<br> $TripCharge";
	if ($approved)
		$whatChangedTech = "$whatChangedTech<br> $TripCharge";
	}

if ($MileageCharge != ""){
	$MileageCharge = "Mileage Charge: $MileageCharge";
	$whatChanged = "$whatChanged<br> $MileageCharge";
	if ($approved)
		$whatChangedTech = "$whatChangedTech<br> $MileageCharge";
	}

if ($MaterialsCharge != ""){
	$MaterialsCharge = "Materials Charge: $MaterialsCharge";
	$whatChanged = "$whatChanged<br> $MaterialsCharge";
	if ($approved)
		$whatChangedTech = "$whatChangedTech<br> $MaterialsCharge";
	}

if ($AbortFeeAmount != ""){
	$AbortFeeAmount= "Abort Fee: $AbortFeeAmount";
	$whatChanged = "$whatChanged<br> $AbortFeeAmount";
	if ($approved)
		$whatChangedTech = "$whatChangedTech<br> $AbortFeeAmount";
	}
	
if ($Additional_Pay_Amount != ""){
	$Additional_Pay_Amount = "Other Adjustments: $Additional_Pay_Amount";
	$whatChanged = "$whatChanged<br> $Additional_Pay_Amount";
	if ($approved)
		$whatChangedTech = "$whatChangedTech<br> $Additional_Pay_Amount";
	}
	
	
	
if ($Add_Pay_Reason != ""){
	$Add_Pay_Reason = "Other Pay Field Explanation: $Add_Pay_Reason";
	$whatChanged = "$whatChanged<br> $Add_Pay_Reason";
	if ($approved)
		$whatChangedTech = "$whatChangedTech<br> $Add_Pay_Reason";
	}
if ($lastModBy != ""){
	$lastModBy = "Modified By: $lastModBy";
	$whatChanged = "$whatChanged<br> $lastModBy";
	if ($approved && $whatChangedTech != "")
		$whatChangedTech = "$whatChangedTech<br> $lastModBy";
	}

//14024
 $techClass = new Core_Api_TechClass();
if ($techID != "") {$isTechHideBid = $techClass->isTechHideBids($techID);}   
// If Tech id and not reviewed
if ($techID != "" && !$isTechHideBid ){
$result = "Success"; //Value required by our library
$calledFrom = (empty($_SERVER['HTTP_REFERER']) ? "Unknown" : $_SERVER['HTTP_REFERER']);// library needs this

// Get Work Order
$workOrder = caspioSelectAdv("Work_Orders", "SiteName, Address, City, State, Zipcode, TB_UNID, WO_ID", "TB_UNID = $TB_UNID", "TB_UNID", false, "'", "|");

function getdelimitedFields($row) {
	/* Takes a single row returned by a select and returns an array of Fields */
		$row = str_replace("'", "", $row); // gets rid of quotes
		return explode("|", $row);
	}

// Populate array with WO info
foreach ($workOrder as $order) {
	$workOrderFields = getdelimitedFields($order);
	
	$SiteName = $workOrderFields[0];
	$Address1 = $workOrderFields[1];
	$City = $workOrderFields[2];
	$State = $workOrderFields[3];
	$Zip = $workOrderFields[4];
	$TB_UNID = $workOrderFields[5];
	$WorkOrderNo = $workOrderFields[6];

}

// Get Project info
$project = caspioSelect("TR_Client_Projects", "Project_ID,Project_Name,Project_Manager,From_Email,Project_Manager_Phone, To_Email", "Project_ID = '$ProjectNo'", false);

// Populate array with project info
foreach ($project as $order) {
	
	$projectFields = getFields($order);
	
	$projectID = $projectFields[0];		// Project No
	$projectName = $projectFields[1];
	$ccName = str_replace(".", "", $projectFields[2]);
	$vFromName = str_replace(".", "", $projectFields[2]);
	$vFromEmail = $projectFields[3];
	$projectCCphone = $projectFields[4];
	$To_Email = str_replace(";", ",", $projectFields[5]);
	
	// if project from email is blank
	if ($vFromEmail == ""){
		$vFromEmail = "projects@fieldsolutions.com";
	}
}
	
// Get Tech Contact info
$records = caspioSelect("TR_Master_List", "techID, FirstName, LastName, PrimaryEmail", "TechID = '$techID'", false);

// Counts # of records returned
$count = sizeof($records);

// Populate array with Tech info
foreach ($records as $order) {
	
	$fields = getFields($order);
	
	// Get data from array and set values for email
	$techID = $fields[0];
	$FirstName = $fields[1];	
	$LastName = $fields[2];
	$sendToEmail = $fields[3];
	
		
// Send Email to the Tech
$eList = "$sendToEmail";
$vSubject = "Project: $projectName - Work Order# $WorkOrderNo has been changed";
@$caller = "wosEmailTechWhenWoChanges";
$message = "		
Dear $FirstName $LastName,
	
Workorder $WorkOrderNo for Project: $projectName has been changed. Below are the changes:

$whatChangedTech
		
<a href='https://www.fieldsolutions.com/techs/wosDetailsNonTab.php?id=$TB_UNID'>View your workorder</a>

Coordinator: $vFromName
Phone: $projectCCphone

Thank you for your support,
Field Solutions Management";
		
$htmlmessage = nl2br($message);


// Emails Tech
if ($whatChangedTech != "")
	smtpMail($vFromName, $vFromEmail, $eList, $vSubject, $message, $htmlmessage, $caller);



// Send Email to the Coordinator
$eList = "$To_Email";
$vFromEmail = "projects@fieldsolutions.com";
$vFromName = "Field Solutions";
$message = "		
Dear $ccName,
	
Workorder $WorkOrderNo for Project: $projectName has been changed. Below are the changes:

$whatChanged
		
Thank you,
Field Solutions Management";
		
$htmlmessage = nl2br($message);

smtpMail($vFromName, $vFromEmail, $eList, $vSubject, $message, $htmlmessage, $caller);

}

}// Ends TechID check	


// BEGIN CATCH	
}catch (SoapFault $fault) {

smtpMail("Email When WO Changes", "nobody@fieldsolutions.com", "collin.mcgarry@fieldsolutions.com", "Email When WO Changes Script Error", "$fault", "$fault", "wosEmailWhenWoChanges.php");

}// END CATCH


?>


<SCRIPT LANGUAGE="JavaScript"> 
	setTimeout('close()',100); 
</SCRIPT>

