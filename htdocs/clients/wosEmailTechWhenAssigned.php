<?php if ($_REQUEST['type'] == "create"){ ?>
<?php $page = "clients"; ?>
<?php $option = "wos"; ?>
<?php 
//require ("../header.php"); ?>
<?php //require ("../navBar.php"); ?>
<?php //require ("includes/adminCheck.php"); ?>
<?php
}
// Created on: 10-10-07 
// Author: CM
// Description: Receives Workorder info from datapage, retrieves techs email and notifies of assigned workorder.  Notifies all other technicians that they were outbid.


// DONE: When client creates WO with "Show_Techs" turned on, send email blast

// DONE: When client updates a WO with "Show_Techs" set from off to ON, send email blast, as long as there's no technician assigned to it


try {
require_once("autoassign/includes/common.php");
require_once("../library/timeStamps2.php");
require_once("../library/caspioAPI.php");
require_once("../library/smtpMail.php");
require_once("../includes/SMS_Send.php");
require_once("../library/woContactLog.php");
require_once("autoassign/includes/AutoAssign.php");

require_once("../library/woACSTimeStamps.php");

$tbUNID = $_REQUEST['tbUNID'];
$techID = $_REQUEST['techID'];
$projectID = $_REQUEST['projectID'];
$type = $_REQUEST['type'];
$company = $_REQUEST['company'];
$companyID = $_REQUEST['CompanyID'];
$getShowTechs = $_REQUEST['showTechs'];

// List of companies (use Company_ID from the database) that Michael will be notified when they create a new WO
// NOTE JC 10/2/08: NO LONGER NOTIFY MICHAEL BASED ON SPECIFIC COMPANIES

// $notifyOnCreateWOList = array("NCR" => 1, "RTG" => 1, "POS" => 1, "AST" => 1, "DS" => 1, "RUO" => 1, "NF" => 1, "TS" => 1, "GAS " => 1);

// FOR TESTING PURPOSES
/*
$tbUNID = "8076";
$techID = "30349";
$projectID = "6";
$type = "create";
$company = "test";
$projectName = "My Pet Project";
*/
/*
echo "tbUNID: $tbUNID<br>";
echo "Tech ID: $techID<br>";
echo "Project ID: $projectID<br>"; 
echo "Type: $type<br>";
echo "Company: $company<br>"; 
*/

/* ----------------------------------------------------- */
/* NOTIFICATION OF PROJECT MANAGERS */
/* ------------------------------------------------------*/

/* Queries database for Work Order details */


	$woInfo = caspioSelectAdv(TABLE_WORK_ORDERS, "TOP 1 WO_ID, Username, CONVERT(VARCHAR, StartDate, 101), StartTime, Project_Name, Address, City, State, Zipcode, Description, PayMax, SpecialInstructions, Amount_Per, Qty_Devices, Company_Name, ShowTechs, isProjectAutoAssign, ProjectManagerName, ProjectManagerEmail, PcntDeduct, CONVERT(VARCHAR, EndDate, 101), ISNULL(EndTime, ''), ISNULL(StartRange,''), ISNULL(Duration,''), ISNULL(Qty_Devices,''), ProjectManagerPhone, PcntDeductPercent", "TB_UNID = '$tbUNID'", "", false, "`", "|^");
	
	if (sizeof($woInfo) == 1 && $woInfo[0] != "") {
		$info = explode("|^", $woInfo[0]);
		$workOrderID = trim($info[0], "`");
		$username = trim($info[1], "`");
		$startDate = trim($info[2], "`");
		$startTime = trim($info[3], "`");
		$projectName = trim($info[4], "`");
		$address = trim($info[5], "`");
		$city = trim($info[6], "`");
		$state = trim($info[7], "`");
		$zipcode = trim($info[8], "`");
		$description = trim($info[9], "`");
		$payMax = trim($info[10], "`");
		$specialInstr = trim($info[11], "`");
		$amountPer = trim($info[12], "`");
		$numDevices = trim($info[13], "`");
		$companyname = trim($info[14], "`");
		$showTechs = trim($info[15], "`");
		$isWOAssign = trim($info[16], "`");
	
		$projectManager = trim($info[17], "`");
		$projectManagerEmail = trim($info[18], "`");
		
		$PcntDeduct = trim($info[19], "`");

                $PcntDeductPercent=trim($info[25], "`");
		
		$endDate = trim($info[20], "`");
		$endTime = trim($info[21], "`");

		$startRange = trim($info[22], "`");
		$duration = trim($info[23], "`");
		
		$qtyDevices = trim($info[24], "`");
		$qtyDevices = ($amountPer == "Device" ? "($qtyDevices device(s))" : "");

		$projectManagerPhone = trim($info[25], "`");

		if ($type == "create") {
			//$woInfo = caspioSelectAdv("Work_Orders", "TOP 1 WO_ID, Username, CONVERT(VARCHAR, StartDate, 101), StartTime, Project_Name, Address, City, State, Zipcode, Description, PayMax, SpecialInstructions, Amount_Per, Qty_Devices, Company_Name, ShowTechs, isProjectAutoAssign", "TB_UNID = '$tbUNID'", "", false, "`", "|");
			
			
			Create_TimeStamp($tbUNID, $username, "Work Order Created", $companyID, $companyname, $projectName, "", "", "");
				
			createWOACSTimeStamp($tbUNID);
			
			if ($showTechs == "True"){
				Create_TimeStamp($tbUNID, $username, "Work Order Published", $companyID, $companyname, $projectName, "", "", "");
                                $NoticeClass = new Core_Api_NoticeClass();
                                $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_PUBLISHED);
                        }      
				
			if (!empty($_SESSION["UserType"]))
				Create_TimeStamp($tbUNID, $username, "Work Order Update Requested", $companyID, $companyname, $projectName, "", "", "");
				
			$devicesText = "";
				
			if ($amountPer == "Device") {
				$devicesText = "
				Devices: $numDevices";
			}
		
			$message = "		
			WorkOrderID: $workOrderID
			Username: $username
			Max Pay: $payMax
			Amount Per: $amountPer $devicesText		
			Start Date: $startDate
			Start Time: $startTime
			Project Name: $projectName
			Address: $address
			City: $city
			State: $state
			Zip: $zipcode
				
			Description:
				
			$description
				
			Special Instructions:
				
			$specialInstr";
	
				
			$htmlmessage = nl2br($message);
			smtpMailLogReceived("Work Order Created", "nobody@fieldsolutions.com", "NewWOsNote@fieldsolutions.com", "New $company WO - $projectName", $message, $htmlmessage, "New Orders");
			//if ($showTechs == "True" && $isWOAssign == "True") {
			if (($isWOAssign == "True") && ($showTechs == "True") && (empty($_SESSION['wo_insert'][$tbUNID]) || ($_SESSION['wo_insert'][$tbUNID]-time()) > 600)) {
				AutoAssign::$baseDir = dirname(__FILE__) . '/autoassign';
				AutoAssign::proccessAutoAssign($tbUNID);
				$_SESSION['wo_insert'][$tbUNID] = time();
			}
				
			//smtpMailLogReceived("Work Order Created", "nobody@fieldsolutions.com", "mbergeron@fieldsolutions.com,gbailey@fieldsolutions.com", "New $company WO - $projectName", $message, $htmlmessage, "New Orders");
		}
	
	/* ----------------------------------------------------- */
	/* CONFIRMATION EMAIL TO ASSIGNED TECHNICIAN */
	/* ------------------------------------------------------*/
	
	// If WO has a Tech ID, send email
	//
	if ($techID != ""){
		if ($type == "create"){
			Create_TimeStamp($tbUNID, $username, "Work Order Assigned", $companyID, $companyname, $projectName, "", "", "");
                        $NoticeClass = new Core_Api_NoticeClass();
                        $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_ASSIGNED);
                }
		$result = "Success"; //Value required by our library, use of try should negate this
		$calledFrom = (empty($_SERVER['HTTP_REFERER']) ? "Unknown" : $_SERVER['HTTP_REFERER']);// library needs this
		
		// Get Project info
		$project = caspioSelect("TR_Client_Projects", "From_Email, Project_Manager, Project_Name, P2TNotificationEmail, AssignmentNotificationEmail, NotifyOnAssign, NotifyOnAutoAssign", "Project_ID = '$projectID'", false);
		
		// Populate array with project info
		foreach ($project as $order) {
			
			$projectFields = getFields($order);
			
			$vFromEmail = $projectFields[0];	// From Email address
			$vFromName = $projectFields[1];		// From Name
			$projectName = $projectFields[2];   // Project Name
			
			if (empty($vFromEmail)) {
				$vFromEmail = "no-replies@fieldsolutions.us";
			}
		
			$P2TNotificationEmail = $projectFields[3];
			$AssignmentNotificationEmail = $projectFields[4];
			$NotifyOnAssign = $projectFields[5];
			$NotifyOnAutoAssign = $projectFields[6];

			$vSubject = "Project Assigned: $projectName"; // Subject Line
		}
	
		
		// Get Tech Contact info
		$records = caspioSelect("TR_Master_List", "techID, FirstName, LastName, PrimaryEmail, PrimaryPhone, ISNULL(SecondaryPhone, ''), Address1, Address2, City, State, ZipCode, ISNULL(PrimPhoneType, ''), ISNULL(SecondPhoneType, ''), ISNULL(ISO_Affiliation_ID, 0), W9", "TechID = '$techID' ", false);
		
		$woInfo = caspioSelectAdv("Work_Orders", "TOP 1 CONVERT(VARCHAR, StartDate, 101), ISNULL(StartTime, ''), City, State, Headline, ISNULL(SiteName, ''), ISNULL(Address, ''), ZipCode", "TB_UNID = '$tbUNID'", "", false, "`", "|");
		
		$startDate = "";
		$startTime = "";
		$city = "";
		$headline = "";
		if (sizeof($woInfo) == 1 && $woInfo[0] != "") {
			$info = explode("|", $woInfo[0]);
			$startDate = trim($info[0], "`");
			$startTime = trim($info[1], "`");
			$city = trim($info[2], "`");
			$state = trim($info[3], "`");
			$headline = trim($info[4], "`");
			$siteName = trim($info[5], "`");
			$siteAddress = trim($info[6], "`");
			$siteZip = trim($info[7], "`");
		}
		
		// Counts # of records returned
		$count = sizeof($records);
		
		// Populate array with workorders
		foreach ($records as $order) {
			
			$fields = getFields($order);
			
			// Get data from array and set values for email
			$techID = $fields[0];
			$FirstName = $fields[1];	
			$LastName = $fields[2];
			$sendToEmail = $fields[3];
			$primaryPhone = $fields[4];
			$secondaryPhone = $fields[5];
			$ContactAddress1 = $fields[6];
			$ContactAddress2 = $fields[7];
			$ContactCity = $fields[8];
			$ContactState = $fields[9];
			$ContactZipCode = $fields[10];
			$PrimPhoneType = $fields[11];
			$SecondPhoneType = $fields[12];
			$isoID = $fields[13];
			$W9 = $fields[14];
			
			$eList = $sendToEmail;
			//$eList = "jcoleman@fieldsolutions.com";
			@$caller = "wosEmailTechWhenAssigned";
			
			//$deductMsg = $PcntDeduct == "True" ? "A 10% service fee will be deducted from your final total payment amount from this work order." : "NO service fee is being deducted from this work order.";
                        if(empty($PcntDeductPercent) || $PcntDeductPercent == 0)
                        {
                            $PcntDeductPercentMsg = "No service fee will be deducted from your final total payment amount from this work order.";
                        }
                        else if($PcntDeductPercent == 1)
                        {
                            $PcntDeductPercentMsg = "You will be paid outside of the FieldSolutions Platform for this work order.";
                        }
                        else
                        {
                            $PcntDeductPercentMsg = "A ".($PcntDeductPercent * 100)."% service fee will be deducted from your final total payment amount from this work order.";
                        }
						
			$message = "
		Dear $FirstName $LastName,
		 
		Congratulations, your bid was accepted and you are assigned (WIN# $tbUNID for $headline) that is scheduled for $startDate.
		
		Location: $city, $state $siteZip
		Date: $startDate
		Start Time: $startTime 
		End Time: $endTime
		Est On Site Arrival Time: $startRange
		Duration: $duration
		Pay: $payMax PER $amountPer $qtyDevices
		
		$PcntDeductPercentMsg

		THIS IS A COMMITTED ASSIGNMENT OF WORK TO YOU.
		
		We appreciate your commitment to complete this work order on time.  If for any reason you are no longer available, please contact $projectManager (phone: $projectManagerPhone email: $projectManagerEmail) immediately.    Your professionalism in managing your commitments will build client loyalty and your reputation.
		
		CONFIRM YOUR ACCEPTANCE OF THE WORK ORDER TODAY.
		Click this link to <a href=\"https://www.fieldsolutions.com/techs/wosWorkOrderReviewed.php?tbUNID=$tbUNID&techID=$techID\">CONFIRM YOUR ASSIGNMENT</a> to WIN# $tbUNID. You may be required to login to your account if you are not already.
				
		Step 1: Be sure to CHECK the BOX in SECTION 5 of the work order: \"Tech has reviewed all Work Order Details\"
		Step 2: Review the work order info and project documentation.  
		Step 3: Across the top of the work order is a blue bar with \"Print\" and \"Project Documents\" buttons.  
		Step 4: Click on Project Documents to download and print the detailed documents for your work order.
		
		We appreciate your professional commitment to quality work for our clients. Thank you for building your independent contractor business through Field Solutions.  

		Field Solutions Management

		Check and Update Your Contact Information:
		
		$ContactAddress1 $ContactAddress2
		$ContactCity, $ContactState  $ContactZipCode
		
		Primary Phone: $primaryPhone  $PrimPhoneType
		Secondary Phone: $secondaryPhone  $SecondPhoneType
		Email: $sendToEmail
		
		<a href='https://www.fieldsolutions.com/techs/profile.php'>Update Contact Information</a>
		";
			
		$htmlmessage = nl2br($message);
		
		// Emails Tech
		smtpMailLogReceived($vFromName, $vFromEmail, $eList, $vSubject, $message, $htmlmessage, $caller);
		
		if ($isWOAssign == "True") {
			$smsRecipientList = implode(",", getSMSEmailFromTechID(array($techID)));
						
			$smsMsg = "GoP2T: You have been awarded WIN# $tbUNID $startTime $startDate $siteAddress $city $siteZip Go online to accept.";
			
//			smtpMail("Field Solutions", $vFromEmail, "tngo@fieldsolutions.com", "GoP2T: You have been awarded WIN# $tbUNID", $smsMsg . " $techID, $smsRecipientList", $smsMsg . " $techID, $smsRecipientList", $caller);

			if ($smsRecipientList != "")
//				smtpMail("Call Coordinator", $fromEmail, "9522707290@txt.att.net", $subjectLine, $sms_message, $sms_htmlmessage, "Recruitment Email");
				smtpMail("Field Solutions", $vFromEmail, $smsRecipientList, "GoP2T: You have been awarded WIN# $tbUNID", $smsMsg, $smsMsg, $caller);
		}
		
		// Notification Email on Assign (P2T and Normal)
//		if ($companyID == "CBD" || $companyID == "suss" || $companyID == "BW" || $companyID == "FS") {
		if ($NotifyOnAutoAssign == "True" || $NotifyOnAssign == "True") {
			$message = "This note to is to notify you that work order ($workOrderID / WIN# $tbUNID) has been assigned to ($techID / $FirstName $LastName).  The event is scheduled for $startDate at $startTime.";
			$htmlmessage = nl2br($message);
			$eList1 = $NotifyOnAssign == "True" ? $AssignmentNotificationEmail : "";
			$eList2 = $NotifyOnAutoAssign == "True" ? $P2TNotificationEmail : "";
			$recpList = array();
			if ($eList1 != "")
				$recpList[] = $eList1;
			if ($eList2 != "")
				$recpList[] = $eList2;
				
			$recp = implode(",", $recpList);
			if ($recp != "") {
				$P2TLabel = $isWOAssign == "True" ? "P2T:" : "";
				smtpMailLogReceived("FieldSolutions", "nobody@fieldsolutions.com", $recp, "$P2TLabel Work Order ($workOrderID / WIN# $tbUNID) has been Assigned", $message, $htmlmessage, $caller);
			}
		}
//		}
		
		// Log in WO Contact Logs
		logEmailContact("Automated Email When Tech Assigned WO", $companyID, $tbUNID, $eList, $vSubject, $message);
				
		/* ------------------------------------------------------ */
		/* W9 Reminder                                            */
		/* ------------------------------------------------------ */
		
		if ($W9 == "False" && $isoID == 0) {
			$message = "Dear Technician,
		
		Congratulations on winning a work order and getting it assigned to you.
		
		Please remember that Field Solutions needs your W-9.
		
		Instructions for submitting your W-9 can be found by clicking on the HELP tab after logging in to the site, or clicking https://www.fieldsolutions.com/techs/w9.php.  
		
		If you are submitting your W-9 in a company name, please include your name and/or your technician ID somewhere on the W-9.
		
		Fax your completed W-9 to 888-258-1656, or email it to accounting@fieldsolutions.com.  W-9's are recorded as received in your profile within 1 business day.  An email will be sent to you acknowledging receipt.
		
		Thank you for your prompt attention to this matter.
		
		Sincerely,
		
		Accounting
		Field Solutions";
				
			$htmlmessage = nl2br($message);
			
			// Emails Tech
			smtpMailLogReceived("Field Solutions Accounting", "accounting@fieldsolutions.com", $eList, "W-9 not yet received by Field Solutions", $message, $htmlmessage, $caller);	
		}
		
		/* ----------------------------------------------------- */
		/* NOTIFICATION TO OUTBID TECHNICIANS  */
		/* ------------------------------------------------------*/
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('*'));
        $query->where('WorkOrderID = ?', $tbUNID);
        $query->where('TechID <> ?', $techId);
        $result = Core_Database::fetchAll($query);
        if ( $result && sizeof($result) != 0 ) {
            $vFromName = "Field Solutions";
            $vFromEmail = "no-replies@fieldsolutions.us";
            $caller = "wosEmailTechWhenAssigned";
            $vSubject = "Thanks for bidding; FS Work Order Assigned to Another Tech"; // Subject Line

            foreach ( $result as $bid ) {
				$techId = $bid['TechID'];
				$FirstName = $bid['Tech_FName'];
				$LastName = $bid['Tech_LName'];				
                $bidDate = new Zend_Date($bid['Bid_Date'], 'yyyy-MM-dd hh:mm:ss');
				$bidDate = $bidDate->toString('MM/dd/yyyy h:mm:ss a');
				$bidAmount = sprintf("%01.2f", $bid['BidAmount']);

                $eList = null;
                $tech = Core_Caspio::caspioSelectAdv('TR_Master_List', 'PrimaryEmail', "TechID='".$techId."'",'');
                if ( !empty($tech) ) $eList = $tech[0]['PrimaryEmail'];
                if ( $eList ) {
                    $message = "
                        Dear $FirstName $LastName,

                        Thank you for your bid on WIN# $tbUNID for $headline that was scheduled for $startDate $startTime located $city, $state. This work order has been assigned to another technician. (Important: the name ID# for the work order has been changed to WIN#)

                        Three things you can do to win more work:
                            1.  Add more details to your profile :  add more skills, certifications and a resume to your profile
                            2.  Build your track record in Field Solutions; bid a few jobs at a  lower  price to get some work history and strong ratings
                            3.  Bid a lower price:  think about your bids, maybe bid a few $ lower than the client's \"max offer\" to get noticed.

                        Also, please enroll in the Fujitsu FLS program through FieldSolutions to see more work orders.

                        Thank you and good hunting!
                        Field Solutions Technician Support Team at support@fieldsolutions.com";
                
                    $htmlmessage = nl2br($message);
                    // send email
                    smtpMailLogReceived($vFromName, $vFromEmail, $eList, $vSubject, $message, $htmlmessage, $caller);
                    // log email
                    logEmailContact("Email Notification To Outbid Tech", $companyID, $tbUNID, $vFromEmail, $vSubject, $message);
                }
            }
        }
        /* end SEND EMAIL TO OUTBID TECHS */
        /* ----------------------------------------------------*/
	}// Ends TechID check	
	
	}
	
	
	// BEGIN CATCH	
		}
	}catch (SoapFault $fault) {
	
	smtpMail("WOS Email Tech When Assigned", "nobody@fieldsolutions.com", "gbailey@fieldsolutions.com", "WOS Email Tech When Assigned Script Error", "$fault", "$fault", "wosEmailTechWhenAssigned.php");
		
	//smtpMail("Tech Assigned Workorder", "nobody@fieldsolutions.com", "collin.mcgarry@fieldsolutions.com,gerald.bailey@fieldsolutions.com", "Tech Assigned Work Order Script Error", "$fault", "$fault", "getTechAndEmailAssignedWO.php");
	}// END CATCH


/* ----------------------------------------------------- */
/* EMAIL BLAST TO TECHS WITHIN 50M RADIUS   */
/* ------------------------------------------------------*/

// Created on: 6/24/2008 
// Author: JC
// Description: When a work order is created, send an email blast to techs in a 50 mile radius

//ini_set("display_errors", 1);

require_once("../library/caspioAPI.php");
require_once("../library/smtpMail.php");

/*

6/24/2008:

DONE: Send email to all techs in 50 mile radius with WO details
DONE: Uncomment $TB_UNID line below BEFORE DEPLOYING!
DONE: Comment out any "echo" statements
DONE: Uncomment Javascript close statement at bottom

*/

// $TB_UNID = $_GET['tbUNID'];

//$TB_UNID = 7036;


// FUNCTIONS

// FUNCTION process
// 
// for use when processing an array returned by caspioSelectAdv.
// trims the quotes off and filters "NULL" into ""
//
function process($str) {
	$output = trim($str, "`");
	switch ($str) {
		case "NULL":
			$output = "";
			break;

		case "True":
		case "Yes":
			$output = "Y";
			break;
		case "False":
		case "No":
			$output = "N";
			break;
	}
	return $output;
}


// FUNCTION queryTechs(fields, conditions, order)
//
// queries TR_Master_List for data

function queryTechs($fields, $conditions, $order="TechID ASC", $zipcode="", $distance="1") {
	if (false) {
		//echo "<p>QUERY: caspioSelectAdv(\"TR_Master_List\", \"$fields\", \"$conditions\", \"$order\", ...)</p>";
	}
	switch ($distance) {
		case "1":
			$result = caspioSelectAdv("TR_Master_List", $fields, $conditions . "AND Deactivated != '1'", $order, FALSE, "`", "|");
			break;
		default:
			$result = proximityTechSearch($zipcode, $distance, $fields, " and $conditions AND Deactivated != '1'", $order);			
	}
	return $result;
}

function queryTechsFromList($fields, $list, $order="TechID ASC") {
	$output = array();
	if (sizeof($list) != 0) {
		$list_string = format_list($list);
		$output = queryTechs($fields, "TechID in ($list_string)", $order);
	}
	return $output;
}


// function proximityTechSearch
//
// returns list of results that meet search criteria including
// proximity search
//
function proximityTechSearch($zipcode, $distance, $fields, $conditions, $order="TechID ASC") {
	$result = array();
	
	$zipcode = caspioEscape($zipcode);
	$distance = caspioEscape($distance);
	
	$center = caspioSelectAdv("USA_ZipCodes", "TOP 1 Latitude, Longitude", "ZIPCode = '$zipcode'", "", false, "`", "|", false);

	if (sizeof($center) == 1 && $center[0] != "") {
		// valid zip, do quick proximity search
		$center = explode("|", $center[0]);
		$latitude = $center[0];
		$longitude = $center[1];
		
		$result = caspioProximitySearchByReferenceRaw("TR_Master_List", false, $latitude, $longitude, "Zipcode", "USA_ZipCodes", false, "ZIPCode", "Latitude", "Longitude", "<= $distance", "3", $fields, $conditions, "", false, "|", "`", false);
	}	
	return $result;
}

// END FUNCTIONS


// GET INFO ABOUT WORK ORDER

// if WO unique ID is not blank, and techID IS blank,
// send email to techs in 50m radius


//echo "<p>About to send emails to technicians...</p>";
//echo "<p>techID: $techID</p>";

if ($tbUNID != "" && $techID == "")
{
	require_once("../library/recruitmentEmailLib.php");
	
	$woInfo = NULL;
	if ($getShowTechs == "true") {
		$woInfo = FALSE;
		queueRecruitmentEmail($tbUNID, $companyID);
	}
	else if ($type == "create")
		// created work order, so check that showtechs is set before sending
		$woInfo = getWOForRecruitmentEmailById(array($tbUNID));
	if ($woInfo)
		blastRecruitmentEmailMainSite($woInfo, $companyID, "", 50, TRUE);

	//echo "<p>Sending emails to technicians...</p>";

	// Get Work Order
/*	$woInfo = caspioSelectAdv("Work_Orders", "ISNULL(Description, ''), ISNULL(City, ''), ISNULL(State, ''), ISNULL(Zipcode, ''), ISNULL(CONVERT(VARCHAR, StartDate, 101), ''), ISNULL(StartTime, ''), ISNULL(EndTime, ''), ISNULL(PayMax, ''), ISNULL(SpecialInstructions, ''), Amount_Per, Qty_Devices, ISNULL(StartRange, ''), ISNULL(Duration, ''), ISNULL(Project_ID, ''), ShowTechs, TB_UNID", "TB_UNID = $tbUNID", "TB_UNID ASC", false, "`", "|");
	
	if (sizeof($woInfo) == 1 && $woInfo[0] != "") {
			$info = explode("|", $woInfo[0]);
			$description = process($info[0]);
			$city = process($info[1]);
			$state = process($info[2]);
			$zipCode = process($info[3]);
			$startDate = process($info[4]);
			$startTime = process($info[5]);
			$endTime = process($info[6]);
			$payMax = process($info[7]);
			$specialInstructions = process($info[8]);
			$amountPer = process($info[9]);
			$qtyDevices = process($info[10]);
			$startRange = process($info[11]);
			$duration = process($info[12]);
			$projectID = process($info[13]);
			$showTechs = process($info[14]);
			$RecID = process($info[15]);
	}
	
	//echo "<p>ProjectID: $projectID</p>";
	
	//echo "<p>city: $city</p>";
	//echo "<p>duration: $curation</p>";
	//echo "<p>showTechs: $showTechs";
		
	if ($showTechs == "Y" || $getShowTechs == "true") {
		// echo "<p>Creating list of technicians...</p>";
		
		// Get Project
		$woInfo = caspioSelectAdv("TR_Client_Projects", "From_Email, Project_Manager, Client_Email, Client_Name", "Project_ID = '$projectID'", "Project_ID ASC", false, "`", "|");

		
		if (sizeof($woInfo) == 1 && $woInfo[0] != "") {
				$info = explode("|", $woInfo[0]);
				$vFromEmail = process($info[0]);
				$vFromName = process($info[1]);
				$clientEmail = process($info[2]);
				$clientName = process($info[3]);
		}
		
		
		// GET ARRAY OF TECHS WITHIN 50 MILES OF WORK ORDER
		
		$techsResult = proximityTechSearch($zipCode, "50", "PrimaryEmail", "");
		
		if (sizeof($techsResult) > 0 && $techsResult[0] != "") {
			$techsResult = array_unique($techsResult);
			$eList = implode(", ", $techsResult);
			$eList = str_replace("`", "", $eList);
			$eList = str_replace(", ,", ", ", $eList);
			
			
			//echo "<p>EMAILS: " . $eList . "</p>";
		
			// Send Email to the Tech
			if ($vFromEmail != "") {
				//$eList = "progressions@gmail.com, gbailey@fieldsolutions.com";
				$eList = $eList . ", gbailey@fieldsolutions.com";
				
				$vSubject = "Work Available in Your Area";
				@$caller = "wosEmailWhenAssigned";
				$message = "-----
		ID: $RecID
		
		Description: 
							
		$description
		
		Special Instructions:
		
		$specialInstructions
							
		Location: $city, $state, $zipCode
		Date: $startDate
		Start Time: $startTime 
		End Time: $endTime
		Est On Site Arrival Time: $startRange
		Duration: $duration
		Required: Ability to lift 75lbs, Cell Phone
		Tools: PC Tool Kit
		Pay: $payMax PER $amountPer $qtyDevices
		";
				
				$message .= "\n\nTo apply for this and other opportunities, log in to your technician profile at <a href='https://www.fieldsolutions.com'>www.fieldsolutions.com</a>";
				
				$message .= "\nNew to FieldSolutions? Create your technician profile, it only takes a few minutes and will be saved to our data base of active technicians. Check out available work orders in your area by logging into your technician profile and selecting work orders.";
				
				$mesage .= "\n\nNeed additional help? <a href='https://www.fieldsolutions.com/techs/Training/FS_AvailableWork.php'>https://www.fieldsolutions.com/techs/Training/FS_AvailableWork.php.</a>";
				

				$message .= "\n\nHave you visited the new Tech Community website? Click <a href='http://www.mytechnicianspace.com/'>here</a> for fun and information on the tech community";
					
				$message .= "\n\nYou are receiving this email as a registered technician on www.fieldsolutions.com.\nClick <a href='http://www.fieldsolutions.com/unsubscribe/'>here</a> to stop receiving future e-mails from us.";
				
				//echo "<p>MESSAGE:</p>";
				//echo $message;
						
				$htmlmessage = nl2br($message);
				
		 		// Emails Tech
			    //	smtpMail($vFromName, $vFromEmail, $eList, $vSubject, $message, $htmlmessage, $caller);
				
				$count = sizeof($techsResult);
				//echo "<p>Sent email blast to $sizeof technicians.</p>";

			}
		}
	}*/
}



?>
<?php if ($type == "create"){ 

//header("Location: createWO.php");
header("Location: wosDetailsNonTabMagnifier.php?v=$companyID&id=$tbUNID");

?>
<!-- Add Content Here -->

<div align="center">
<br><br>

<br />
<p>
Your Work Order has been posted. Click <a href="createWO.php"><b>here</b></a> to return to the Work Order entry screen.
</p>


<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->

<?php }else{ ?>


<SCRIPT LANGUAGE="JavaScript"> 

		setTimeout('close()',100); 

</SCRIPT>

<? } ?>