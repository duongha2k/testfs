<?php
    $page   = 'clients';
    $option = 'dashboard';
    require ("../headerStartSession.php");

    $siteTemplate = ( !empty($_SESSION['template']) ) ? $_SESSION['template'] : NULL;
    if ($siteTemplate == "ruo") {
        require '../templates/ruo/includes/header.php';
    } else {
        require '../header.php';
    }

    $_SERVER['HTTPS'] = ( !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ) ? 'on' : 'off';
    $loggedIn   = ( !empty($_SESSION['loggedIn']) ) ? $_SESSION['loggedIn'] : NULL ;
    $Company_ID = ( !empty($_SESSION['Company_ID']) ) ? $_SESSION['Company_ID'] : NULL;

    if($loggedIn!="yes"){
        header( 'Location: http'.($_SERVER['HTTPS']=='on' ? 's':'').'://'.$_SERVER['HTTP_HOST'].'/clients/logIn.php' ); // Redirect to logIn.php
    }

    //$toolButtons = array('FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
    require '../navBar.php';
    require 'includes/adminCheck.php';
    require 'includes/PMCheck.php';

    $navBarState = $_COOKIE['wosDetailsNavBarState'];
    if (!$navBarState) {
        $navBarState = 'closed';
    }    
?>

<script type="text/javascript">
    window._company = '<?=$_GET['v']?>';
    //  use for load images and save it in browser cache only!!!
    var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
    var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";
</script>

<!--<script type="text/javascript" src="/widgets/js/jquery.min.js"></script>-->
<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetWODetails.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
<script type="text/javascript" src="/widgets/js/functions.js"></script>
<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<link rel="stylesheet" href="/widgets/css/popup.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript">
    function gotoTab(name) {
        location = location.toString().replace('<?=$_SERVER['SCRIPT_NAME']?>', '/clients/wos.php') + '&tab=' + name;
        return false;
    }
    function toggleNavBar( btn )
    {
        var bar = $('#leftVerticalContainer');
        var content = $('#detailsContainer');
        btn = $(btn);

        switch( btn.text() ) {
            case '<<' :
                btn.text('>>');
                btn.parent().css('width', '100%');
                bar.css('display', 'none');
                bar.parent().css('width', '2%');
                bar.parent().css('background', '');
                content.css('width', '97%');
                setCookie('wosDetailsNavBarState', 'closed', 30);
                break;
            case '>>' :
                btn.text('<<');
                btn.parent().css('width', '18%');
                bar.css('display', '');
                bar.parent().css('width', '18%');
                bar.parent().css('background', 'transparent url(/widgets/images/border_right.png) no-repeat scroll right top');
                content.css('width', '81%');
                setCookie('wosDetailsNavBarState', 'opened', 30);
                break;
        }
        return false;
    }
    /**
     *  calendarInit
     *
     *  Display filter post handler
     */
    function calendarInit() {
        $.map(['edit_StartDate', 'edit_EndDate','edit_DateNotified'], function( id ) {
            //$('#' + roll.container().id + ' #' + id).focus( function() {
                //$('#calendar_div').css('z-index', ++roll.zindex);
                $('#'+id).calendar({dateFormat:'MDY/'});
            //});
        });
    }
    /**
     * calendarMove
     *
     * @param event  event
     * @access public
     * @return void
     */
    function calendarMove( input ) {
        $('#calendar_div').css('top', $(input).offset().top + $(input).height() + 6 + 'px');
        $('#calendar_div').css('left', $(input).offset().left + 'px');
        popUpCal.showFor(input);
    }
    var roll;
    var detailsWidget;
    var navWidget;
    $(document).ready(function() {
        roll = new FSPopupRoll();
        detailsWidget = new FSWidgetWODetails({container:'detailsContainer',tab:'create'},roll);
        detailsWidget.show({tab:'create',params:{company:window._company,container:'detailsContainer',tab:"new"}});

        navWidget = new FSWidgetWODetails({container:'leftVerticalContainer',tab:'navigation',customfunctions:{showPreloader:function(){}}},null);
        navWidget.show({tab:'navigation',params:{company:window._company,container:'leftVerticalContainer'}});
        <?php if ($navBarState == 'closed'): ?>
            var btn = $('#navBarToggle');
            var bar = $('#leftVerticalContainer');
            var content = $('#detailsContainer');        
            btn.text('>>');
            btn.parent().css('width', '100%');
            bar.css('display', 'none');
            bar.parent().css('width', '2%');
            bar.parent().css('background', '');
            content.css('width', '97%');
        <?php endif; ?>            
    });
</script>
<!-- Main content -->
<div class="inner10 clr indentTop">
<div class="left_col">
    <div style="width: 80%;" class="fl" id="leftVerticalContainer"></div>
    <div style="width: 18%;" class="fr"><button onclick="return toggleNavBar(this);" id="navBarToggle">&lt;&lt;</button></div>
	</div>
    <div id="detailsContainer" class="right_col">
    </div>
</div>


<?php
/*** End Content ***/
require ("../footer.php");
?>
