<?php $page = clients; ?>
<?php $option = dashboard; ?>
<?php 	require ("../headerStartSession.php"); ?>

<?php

$siteTemplate = $_SESSION['template'];
if ($siteTemplate == "ruo") {
	require ("../templates/ruo/includes/header.php");
} else {
	require ("../header.php");
 }
?>




<?php 
$loggedIn = $_SESSION['loggedIn'];
$Company_ID = $_SESSION['Company_ID'];

if($loggedIn!="yes"){ 
// Redirect to logIn.php
header( 'Location: https://'.$siteTemplate.'.fieldsolutions.com/clients/logIn.php' ) ;
}
?>
<?php require ("../navBar.php");
require ("includes/adminCheck.php");
?>
<?php 
//$loggedIn = $_SESSION['loggedIn'];
//echo "$loggedIn";
?>


<!-- Add Content Here -->
<?php

if ($siteTemplate == "ruo") {
	require ("../templates/ruo/includes/client_dashboardContent_t2.php");
} else {
	require ("dashboardContent_t2.php");
}
?>


<!--- End Content --->
<?php require ("../footer.php"); ?>
