<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<title>Technician Bureau</title>
<meta name="keywords" content="POS, Computer, Computer Repair, Technicians, Field Service, Retail Technicians, Home Computer Repair Technicians, Troubleshooting, PC Technicians" />
<meta name="description" content="Our focus is matching technicians seeking work with clients seeking technicians" />
<meta http-equiv="distribution" content="global" />
<meta http-equiv="copyright" content=" -2008" />
<meta http-equiv="url" content="http://www.fieldsolutions.com" />
<meta name="author" content="Gerald Bailey" />
<meta name="author" content="Collin McGarry" />
<meta name="author" content="Trung Ngo" />

<script type="text/javascript" src="../library/jquery/jquery-1.2.1.pack.js"></script>

<script type="text/javascript">
	$(document).ready(function(){
		parent.resizeTabFrame(document.body.scrollWidth, document.body.scrollHeight);
	});
</script>

</head>
