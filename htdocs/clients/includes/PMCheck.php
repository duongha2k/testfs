<?php
	require_once($_SERVER['DOCUMENT_ROOT'] . "/library/caspioAPI.php");
	if (!isset($displayPMTools)) {
		$displayPMTools = true;
	}

	$db = Zend_Registry::get('DB');
	$select = $db->select();
	$select->from(Core_Database::TABLE_CLIENTS, "ProjectManager")
	        ->where("UserName = " . $db->quoteInto("?", $_SESSION['UserName']));
	$result = $db->fetchCol($select);
	if ($result === false)
		$isPM = FALSE; // username not found
	else
		$isPM = $result[0] == 1;

//	$isPM = false;
	$redirect = false;
	if ((!$isPM && ($_SESSION["Company_ID"] != $_GET['v'] || $_SESSION["Company_ID"] == "")) || $_GET["v"] == "") {
		if ($_SESSION["Company_ID"] == "") $_SESSION["Company_ID"] = "BLANKID"; // shouldn't happen but should be fail safe
		if ($_GET["v"] != "BLANKID")
			$redirect = true;
	}


//	echo "CID: " . $_SESSION["Company_ID"] . "<br/>v: " . $_GET["v"] . "<br/>";
//	echo "isPM: " . ($isPM ? "true" : "false");

	if ($isPM && isset($_GET['v'])) {
		$_SESSION['PM_Company_ID'] = $_GET['v'];
	} else {
		unset($_SESSION['PM_Company_ID']);
	}

    /**
     * if company has been changed - warm user
     * @author Artem Sukharev
     */
    $oldCampany  = isset($_GET['v1']) && !empty($_GET['v1']) ? $_GET['v1'] : '';
    $needWarm = isset($_GET['v1']) && !empty($_GET['v1']) ? true : false;
	if ($isPM && $displayPMTools) {
		$selectedCompany = $_GET["v"];
		$select = $db->select();
		$select->from(Core_Database::TABLE_CLIENTS, array("Company_ID", "CompanyName"))
			->distinct()
			->order(array("CompanyName"));
		$companyList = $db->fetchPairs($select);

//		$companyHtml = "<option value=\"\" " . ($_GET["v"] == "" ? "selected=\"selected\"" : "") . ">All Company</option>";
		$companyHtml = "";
		foreach ($companyList as $id=> $name )
                {
                    $companyHtml .= "<option value=\"$id\" " . ($_GET["v"] == $id ? "selected=\"selected\"" : "") . ">$name - $id</option>";
		}
?>
<div id="companySelect">
Select Company:
<select id="PMCompany" onchange="reloadPage(document.getElementById('PMCompany').value)">
    <?=$companyHtml?>
</select>
<?php if ( $needWarm ) : ?><span class="red">Company has been changed from "<?= $oldCampany ?>" to "<?= $_GET["v"] ?>".</span><?php endif; ?>
</div>
<?php }else{
	print "<br/>";
}
?>
<script type="text/javascript">
    //743
    var isPM = <?= ($isPM ? "true" : "false") ?>;
    //end 734
    
    function reloadPage(company)
    {
        //743
        $.ajax({
            url     : '/widgets/dashboard/client/is-restricted/',
            data    : 'v='+company,
            cache   : false,
            type    : 'POST',
            success : function (data, status, xhr) {
                if(data['restrict'] && isPM)
				{
                    var FSCSDName = "";
                    var FSAMName = "";

                    if(data['csd']==null || data['csd']=="")
                    {
                        FSCSDName="CSD";
                    }
                    else
                    {
                        FSCSDName = "CSD ("+data['csd']+")";
                    }
            
                    if(data['am']==null || data['am']=="")
                    {
                        FSAMName="AM";
                    }
                    else
                    {
                        FSAMName = "Account Manager ("+data['am']+")";
                    }
                    
                    centerPopup("This client&rsquo;s access has been restricted. Please contact your "+FSCSDName+" or "+FSAMName+" before creating or managing Work Orders.");
            //load popup
            loadPopup();
            //CLOSING POPUP
            //Click the x event!
            $("#popupContactClose").click(function(){
                disablePopup();
                //14100
                var str = "<?php echo $_SERVER['PHP_SELF'] ?>";
                str = str.split("/");
                if(str[1]==="reports_test")
                {
                    url = "/reports_test/custom_v2/reportlist.php?v=" + company;
                }
                else
                {
                        url = "<?php echo $_SERVER['PHP_SELF'] ?>?v=" + company;
                }
                //end 14100
                        if (typeof(wd) == "object")
                        {
                            url +='&tab='+wd.currentTab;
                        }
                        document.location.replace(url);
            });
            //Press Escape event!
            $(document).keypress(function(e){
                if(e.keyCode==27 && popupStatus==1){
                    disablePopup();
                }
            }); 
        }
        else
        {
            if (company == "BLANKID")
            {
            url = "./logIn.php";
            }
            else
            {
                //14100
                var str = "<?php echo $_SERVER['PHP_SELF'] ?>";
                str= str.split("/");
                if(str[1]==="reports_test")
                {
                    url = "/reports_test/custom_v2/reportlist.php?v=" + company;
                }
                else
                {
                url = "<?php echo $_SERVER['PHP_SELF'] ?>?v=" + company;
                }
                //end 14100
                if (typeof(wd) == "object")
                {
                    url +='&tab='+wd.currentTab;
                }
            }
            document.location.replace(url);
		}
		},
            error       : function ( xhr, status, error ) {
                //detailObject.onInit._roll.hide();
			}
        });
    }
    //end 734
	<?php if ($redirect) : ?>
reloadPage("<?php echo $_SESSION["Company_ID"] ?>");
<?php endif; ?>
        
</script>
