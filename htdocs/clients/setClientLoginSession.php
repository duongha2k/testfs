<?php
//This page is will log all clients that login by inserting into Caspio using SOAP.
//require_once("../library/smtpMail.php");
require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');


try {
// Retrieve values from login
$UserName = $_REQUEST['UserName'];
$Password = $_REQUEST['Password'];

$authData = array('login'=>$UserName, 'password'=>$Password);

$user = new Core_Api_User();
$user->checkAuthentication($authData);

if (!$user->isAuthenticate()){
	$redirectUrl = empty($_REQUEST['r']) ? '' : "&r=" . urlencode($_REQUEST['r']);
	if($_REQUEST['page'] == "home"){
		header("Location: /wp/?login=client&value=false" . $redirectUrl);
	}else{
		header("Location: /clients/logIn.php?login=false" . $redirectUrl);
	}
	return API_Response::fail();
}
$Company_ID = $user->getCompanyID();
$UserType = $user->getUserType();

//$Company_ID = $_REQUEST['Company_ID'];
if($user->getUserType() == "Admin"){
	$admin = "YES";
}
//$UserType = $_REQUEST["UserType"];

// Set timestamp
$DateTime = date("m/d/Y G:i");

//echo "$UserName";
//echo "$DateTime";


// Insert Login History into TR_Client_Login_History
//$insertWorkorders = caspioInsert("TR_Client_Login_History", "UserName, DateTime", "'$UserName', '$DateTime'");
require_once("../headerStartSession.php");

//Set logged in session
/*$cookie_path = "/";
$cookie_timeout = "0";
$cookie_domain = ".fieldsolutions.com";
session_set_cookie_params($cookie_timeout, $cookie_path, $cookie_domain);
session_name(fsTemplate);
session_start();*/

$_SESSION['loggedIn']="yes";
$_SESSION['loggedInAs']="client";
$_SESSION['Company_ID']=$Company_ID;
$_SESSION['UserName']=$UserName;
$_SESSION['Password'] = $Password;
$_SESSION['UserPassword'] = $Password;
$_SESSION['ClientID'] = $user->getClientId();

$_COOKIE["tbAdminLevel"]=$admin;
$_SESSION["tbAdminLevel"]=$admin;
$_SESSION["UserType"] = $UserType;
$_SESSION['ContactName'] = $user->getContactName();

// Library calls

require_once("../library/caspioAPI.php");
/*
$result = caspioSelectAdv("TR_Client_List", "Password", "UserName = '{$_SESSION["UserName"]}'", "", false, "`", "^|%", false);
$_SESSION["UserPassword"] = trim($result[0], "`");
*/

// Insert Login History into Login_History
//$insertWorkorders = caspioInsert("Login_History", "UserName, DateTime, UserType", "'$UserName', GETDATE(), 'Client'",false);
$user->loginHistory($UserName);
$user->updateLastLoginDate($UserName);

//Handling for "Remember Me" checkbox in login form
$login_key = $_REQUEST["client_key"];
$login_data = $_REQUEST["client_data"];

if (!empty ($login_key)) {
	$db = Core_Database::getInstance();

	if (!empty ($login_data)) {
		$select = $db->select ();
		$select->from ("client_login")
			->where ("login_key = ?", $login_key);

		$records = $db->fetchAll ($select);

		if (empty ($records)) {
			$db->insert ("client_login", array (
				"client_id" => $user->getClientId(), 
				"login_key" => $login_key,
				"login_data" => $login_data
			));
		}
		else {
			$record = $records[0];

			$db->update ("client_login", array (
				"client_id" => $user->getClientId(), 
				"login_data" => $login_data
			), $db->quoteInto("id = ?", $record["id"]));
		}

		//$db->delete ("client_login", $db->quoteInto("client_id = ?", $user->getClientId()) . " AND " . $db->quoteInto("login_key <> ?", $login_key));
	}
	else {
		$db->delete ("client_login", $db->quoteInto("login_key = ?", $login_key));
	}
}

// Get Subdomain
$urlParts = explode('.', $_SERVER['HTTP_HOST']);
$template = $urlParts[0];
$siteTemplate = $_SESSION['template'];

$checkIndexDisplay = $template;

if($siteTemplate == ""){
	$_SESSION['template']=$template;
	$siteTemplate = $_SESSION['template'];
}else if($siteTemplate != $template){
	$_SESSION['template']=$template;
	$siteTemplate = $_SESSION['template'];
}

}catch (SoapFault $fault){
//FTP Failure code

//smtpMail("getClientLoginHistory", "admin@fieldsolutions.com", "cmcgarry@fieldsolutions.com,gbailey@fieldsolutions.com", "getClientLoginHistory Error", "$fault", "$fault", "getClientLoginHistory.php");
}
//13999 
$Core_Api_User = new Core_Api_User();
//743
$isFullRestricted_old = $Core_Api_User->AreFullRestriction($Company_ID);
$isPartialRestricted_old = $Core_Api_User->ArePartialRestriction($Company_ID);
$isFullOpened_old = $Core_Api_User->AreFullorPartialRestriction($Company_ID);
//end 743
$Core_Api_User->setAllAccessRestricted_InNDaysNotAssignedRule($Company_ID);
//743

$isFullRestricted = $Core_Api_User->AreFullRestriction($Company_ID);
$isPartialRestricted = $Core_Api_User->ArePartialRestriction($Company_ID);
$isFullOpened=$Core_Api_User->AreFullorPartialRestriction($Company_ID);

$issendmail = false;
if(($isFullRestricted != $isFullRestricted_old)
        || ($isPartialRestricted != $isPartialRestricted_old)
        || ($isFullOpened!=$isFullOpened_old))
{
    $issendmail = true;
}

if($issendmail)
{
    $restrictedtype = 3;    
    $repClientID = $Core_Api_User->getRepClientIDforCompany($Company_ID);
    $repClientExtInfo = $Core_Api_User->getClientExt($repClientID);    
    if(!empty($repClientExtInfo)){
        $CSD = $clientExtInfo[0]['FSClientServiceDirectorEmail'];
        $AM = $clientExtInfo[0]['FSClientServiceDirectorEmail'];
        $CompanyName = $clientExtInfo[0]['CompanyName'];
        $emailTo = "";
        if(!empty($AM)){
            $emailTo .= $AM;
        }
        if(!empty($CSD)){
            $emailTo .= empty($emailTo) ? $CSD : ','.$CSD;
        }
    }

    if(!empty($emailTo)){
        $apiemailrestricted = new Core_EmailForRestricted();
        $apiemailrestricted->setCompanyId($Company_ID);
        $apiemailrestricted->setCompanyName($CompanyName);
        $apiemailrestricted->settoMail($emailTo);
        $apiemailrestricted->setusername($this->_login);
        $apiemailrestricted->setrestrictedtype($restrictedtype);
        $apiemailrestricted->sendMail();        
    }
}
//end 743
$forceSSL = $siteTemplate != "dev";
if (!empty($_SESSION['redirectURL']) ) {
    $redirectTo = $_SESSION['redirectURL'];
    $_SESSION['_prev_redirectURL'] = $_SESSION['redirectURL'];
    unset($_SESSION['redirectURL']);
    header('Location: http'.((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || $forceSSL ? 's' : '').'://'.$_SERVER['HTTP_HOST']. $redirectTo);    
} else {
	if (!empty($_REQUEST['r'])) {
		header("Location: {$_REQUEST['r']}");
		exit;
	}
    header('Location: http'.((isset($_SERVER['HTTPS'])&&$_SERVER['HTTPS'] == 'on') || $forceSSL ? 's' : '').'://'.$_SERVER['HTTP_HOST'].'/clients/dashboard.php?v=' . $Company_ID);
}
/*
$clientLoginMessageViewed = $_COOKIE["clientLoginMessageViewed"];
if($clientLoginMessageViewed=="yes"){
// Redirect to dashboard.php
//header('Location: https://www.fieldsolutions.com/clients/dashboard.php' );
header('Location: https://'.$siteTemplate.'.fieldsolutions.com/clients/dashboard.php' );
} else if($siteTemplate == "ruo"){
header('Location: https://ruo.fieldsolutions.com/clients/ruo_loginMessage.php' );
}else{
header('Location: https://www.fieldsolutions.com/clients/loginMessage.php' );
}
*/

?>