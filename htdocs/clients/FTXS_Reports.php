<?php $page = "clients"; ?>
<?php $option = "reports"; ?>
<?php $selected = 'FTXSReport'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php require ("includes/adminCheck.php"); ?>
<?php
	$displayPMTools = true;
	require_once("includes/PMCheck.php");
?>
<?php

	$id = $_GET["v"];

?>

<!-- Add Content Here -->
<br /><br />

<div align="center">
<br /><br />
<table width="400" border="1" cellspacing="10" cellpadding="0">
  <tr>
    <th nowrap="nowrap" scope="col">Work Orders</th>
    <th nowrap="nowrap" scope="col">Techs</th>
    <th nowrap="nowrap" scope="col">Tests</th>
  </tr>
  <tr>
    <td align="center" nowrap="nowrap"><div align="center"><a href="FTXS_Metrics_Reports.php?v=<?php echo $id; ?>">Metrics Reports</a><!--<div align="center"><a href="workorderfinancials.php">Work Order Financials</a></div> --></td>
    <td align="center" nowrap="nowrap"><div align="center"><a href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=FLSTechsTrainedNotBadged.prpt&autoSubmit=true')">Trained Techs w/o Badges</a></div></td>
    <td align="center" nowrap="nowrap"><div align="center"><a href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=FLSTestResults.prpt')">View Tech Test Results</a></div></td>
  </tr>
  <tr>
	<td align="center" nowrap="nowrap"><div align="center"><a href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=FAI_Out_of_Scope_Report.prpt')">Out of Scope Work Orders</a></div></td>    
    <td align="center" nowrap="nowrap"><div align="center"><!--a href="../clients/FLS/newTechAssignedToProject.php?v=<?php echo $id; ?>">New Tech Assigned to Project Work</a-->
      <a href="../clients/FLS/TechTrainedRegistered.php?v=<?php echo $id; ?>">Techs Trained and Registered</a></div></td>
    <td align="center" nowrap="nowrap"><div align="center"><a href="../clients/FLS/Hallmark_TestResults.php?v=<?php echo $id; ?>">Hallmark POS Test Results</a></div></td>
  </tr>
  <tr>
    <td align="center" nowrap="nowrap"><div align="center"><a href="FLS/FTXS_Tech_ConfirmReport.php?v=<?php echo $id; ?>">Tech Review and Confirm</a></div><!--<div align="center"><a href="approveworkorders.php">Work Orders: Tech Complete, Not Approved</a></div> --></td>
    <td align="center" nowrap="nowrap"><div align="center"></div></td>
    <td align="center" nowrap="nowrap"><div align="center"><a href="../clients/FLS/Maurices_TestResults.php?v=<?php echo $id; ?>">Maurices Test Results</a></div></td>
  </tr>
  <tr>
    <!--td nowrap="nowrap"><div align="center"><a href="FLS/Financials.php?v=<?php echo $id; ?>">Work Order Financials</a><div align="center"><a href="openWorkOrders.php">Open Work Order Report</a></div></td-->
    <td align="center" nowrap="nowrap"><div align="center"><a href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=FAI_Short_Notice_Report.prpt&autoSubmit=false')">Short Notice Report</a></div></td>
    <td align="center" nowrap="nowrap"><div align="center"><!--a href="../clients/FLS/techmapexport.php?v=<?php echo $id; ?>">Tech Map Export</a--></div></td>
    <td align="center" nowrap="nowrap"><div align="center"></div></td>
  </tr>
  <tr>
    <td align="center" nowrap="nowrap"><div align="center"><a href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=FAI_Technician_SLA_Compliance.prpt&autoSubmit=false')">Technician SLA Compliance</a></div></td>
    <td align="center" nowrap="nowrap"><div align="center"></div></td>
    <td align="center" nowrap="nowrap"><div align="center"></div></td>
  </tr>
  <tr>
    <td align="center" nowrap="nowrap"><div align="center"><a href="/clients/createUpdateRequestReport.php?v=<?php echo $_GET["v"]?>">Update Requests</a></div></td>
    <td align="center" nowrap="nowrap"><div align="center"></div></td>
    <td align="center" nowrap="nowrap"><div align="center"></div></td>
  </tr>  
  <tr>
    <td align="center" nowrap="nowrap"><div align="center"><a href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=MonthlyFinancialReport.prpt&autoSubmit=false')">Monthly Financial Report</a></div></td>
    <td align="center" nowrap="nowrap"><div align="center"></div></td>
    <td align="center" nowrap="nowrap"><div align="center"></div></td>
  </tr>
  <tr>
    <td align="center" nowrap="nowrap"><div align="center"><a href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=MonthlyUtilizationReport.prpt&autoSubmit=false')">Monthly Utilization Report</a></div></td>
    <td align="center" nowrap="nowrap"><div align="center"></div></td>
    <td align="center" nowrap="nowrap"><div align="center"></div></td>
  </tr>
  <tr>
    <td align="center"><div align="center"></div></td>
    <td align="center"><div align="center"></div></td>
    <td align="center"><div align="center"></div></td>
  </tr>
  <tr>
    <td align="center"><div align="center"></div></td>
    <td align="center"><div align="center"></div></td>
    <td align="center"><div align="center"></div></td>
  </tr>
  <tr>
    <td align="center"><div align="center"></div></td>
    <td align="center"><div align="center"></div></td>
    <td align="center"><div align="center"></div></td>
  </tr>
</table>



</div>


<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->