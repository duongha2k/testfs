<?php
$page = 'clients';
$option = 'projects';
$selected = 'projectsCreate';

require ("../header.php");
require ("../navBar.php");
require ("includes/adminCheck.php");

$displayPMTools = true;
require_once("includes/PMCheck.php");
require_once("../library/recruitmentEmailLib.php");
?>
<!-- Add Content Here -->

<br /><br />

<div align="center">

<br /><br />

<!-- update_projects_admin.js -->
<script type="text/javascript">
amount_per_selector_field_name = "select#InsertRecordAmount_Per";
device_quantity_field_name = "input#InsertRecordQty_Devices";
</script>

<script type="text/javascript" src="/update_projects_admin.js"></script>
<!-- end update_projects_admin.js -->
<!-- inherit ShowOnReports value from client -->
<script type="text/javascript">

function clientShowField() {
	return $("input#cbParamVirtual2");
}

function projectShowField() {
	return $("input#InsertRecordShowOnReports");
}

$('document').ready(function(){
	var value = "N";
	if (clientShowField().attr("value") == "Yes")
	{
		value = "Y";
	}
	projectShowField().attr("value",value);
		
});

</script>


<!-- Old one: 193b000000947a970df44b119eb7 -->
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b0000f10cd9abeb6e419da0a3","https:");}catch(v_e){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000f10cd9abeb6e419da0a3">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>
</div>


<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
