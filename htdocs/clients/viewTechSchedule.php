<?php
    /*
     * View Tech Shedule
     * 
     * @author Alex Che
     */

class Core_Temp {
 public function getWorkOrders($user, $pass, $fields, $timeStampFrom, $timeStampTo, $timeStampType, $filters, $offset = 0, $limit = 0, &$countRows = NULL, $sort = "")
        {
/*        $logger = Core_Api_Logger::getInstance();
                $logger->setUrl('getWorkOrders');
        $logger->setLogin($user);
                $logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');

                $authData = array('login'=>$user, 'password'=>$pass);*/
                $errors = Core_Api_Error::getInstance();

/*                $user = new Core_Api_AdminUser();
        $user->checkAuthentication($authData);

*/
/*      $v = $user->isAuthenticate();
               if (!$user->isAuthenticate())
                        return API_Response::fail();*/

                $countRows = 0;

                try {
                        $result = API_AdminWorkOrderFilter::filter(null,
                                    $fields,
                                    $timeStampFrom,
                                    $timeStampTo,
                                    $timeStampType,
                                    $filters, $sort, $limit, $offset, $countRows);


                        $woArray = API_AdminWorkOrder::makeWorkOrderArray($result->toArray(), new API_AdminWorkOrder(NULL));
                } catch (Exception $e) {
                        $errors->addError(7, 'SQL Error: '. $e);
                        error_log($e);
                }
                $err = $errors->getErrors();
                if (!empty($err))
                        return API_Response::fail();

                return API_Response::success($woArray);
        }
}

    $techData = null;
    $woData = null; 
    
    require ("../headerSimple.php"); 
    
    if ( !empty($_REQUEST['techID']) ) {
        $api = new Core_Api_Class();
        
        $techID = $_REQUEST['techID'];
        
        $tech = $api->techLookup($_SESSION['UserName'], $_SESSION["UserPassword"], $techID);
        if ($tech->success && !empty($tech->data[0])) {
            $techData = $tech->data[0];
        }
        $woFilter = new API_AdminWorkOrderFilter();
        //$woFilter->StartDateFrom = date('m/d/Y', strtotime('now')); //2012-03-06
        $woFilter->Tech_ID = $techID;

        //echo("<pre>");print_r($woFilter);//test
        $count = null;
	    $apit = new Core_Temp;
        $wo = $apit->getWorkOrders($_SESSION['UserName'], $_SESSION["UserPassword"], null, null, null, null
                        , $woFilter, 0, 0, $count
                        , 'StartDate desc');
        
        if ($wo->success && !empty($wo->data[0])) {
            $woData = $wo->data;
        }
    }
    
    // start template part
?>

<link rel="stylesheet" href="../css/tablesorter.css" type="text/css" />

<script type="text/javascript">
	var httpReferer = "<?php echo $_SERVER['HTTP_REFERER']?>";
</script>

<table cellpadding="0" cellspacing="0" border="0" align="center" style="border:solid thin #2a497d; margin: 0 auto;">
	<tr>
<td>
<table id="wos" class="tablesorter" cellspacing="0" cellpadding="0">             
<thead>
    <tr>
<th colspan="7" align="center">
<div style="color:#000000; font-weight:bold; font-size:16px;" id='divtechName'><?php if (!empty($techData)) :?><?=$techData->Firstname . ' ' . $techData->Lastname?>'s <?php endif;?>Schedule</div>
</th>
	</tr>
    <tr> 
<th><nobr>Start Date&nbsp;&nbsp;&nbsp;&nbsp;</nobr></th> 
<th><nobr>Start Time&nbsp;&nbsp;&nbsp;&nbsp;</nobr></th> 
<th><nobr>End Date&nbsp;&nbsp;&nbsp;&nbsp;</nobr></th> 
<th><nobr>End Time&nbsp;&nbsp;&nbsp;&nbsp;</nobr></th>
<th>City&nbsp;&nbsp;&nbsp;&nbsp;</th>
<th>State&nbsp;&nbsp;&nbsp;&nbsp;</th>
<th>Zip&nbsp;&nbsp;&nbsp;&nbsp;</th>
    </tr> 
</thead> 
<tbody> 

<?php if (empty($woData)) : ?>
<tr class='wosRow'><td colspan='7' align='center'>None found</td></tr>
<?php else: ?>
    <?php foreach ($woData as $woItem):?>
    <?php 
        $startDate = new Zend_Date($woItem->StartDate, 'yyyy-MM-dd');
        $endDate = new Zend_Date($woItem->EndDate, 'yyyy-MM-dd');
    ?>
    <tr class="wosRow">
        <td><?=(empty($woItem->StartDate) ? '' : $startDate->toString('MM/dd/Y')); ?></td>
        <td><?=htmlspecialchars($woItem->StartTime) ?></td>
        <td><?=(empty($woItem->EndDate) ? '' : $endDate->toString('MM/dd/Y')); ?></td>
        <td><?=htmlspecialchars($woItem->EndTime) ?></td>
        <td><nobr><?=htmlspecialchars($woItem->City) ?></nobr></td>
        <td><?=htmlspecialchars($woItem->State) ?></td>
        <td><?=htmlspecialchars($woItem->Zipcode) ?></td>
    </tr>
    <?php endforeach; ?>
<?php endif;?>


<tfoot>
<th colspan="7" align="right">&nbsp;</th>
</tfoot>
</tbody>
</table>
</td></tr></table>

<script type="text/javascript">
$(document).ready(function(){
$("tr.wosRow").mouseover(function() {$(this).addClass("over");}).mouseout(function() {$(this).removeClass("over");});
});
</script>
<div style="width: 100%; text-align: center">
	<div id="Mod0EditRecord"></div>
</div>
<script type="text/javascript" src="js/backButton.js"></script>
