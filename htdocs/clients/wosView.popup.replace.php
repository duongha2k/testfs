<?php $page     = 'clients'; ?>
<?php $option   = 'wos'; ?>
<?php $selected = 'wosView'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php require ("includes/adminCheck.php"); ?>
<?php require_once ("includes/PMCheck.php"); ?>

<!-- Add Content Here -->

<?php
	require_once("../library/caspioAPI.php");
	$result = caspioSelectAdv('Client_Active_Projects', "TR_Client_Projects_Project_Name, TR_Client_Projects_Project_ID, TR_Client_Projects_Project_Company_ID", "", "TR_Client_Projects_Project_Name ASC", TRUE, "\"", "^");
	$projectList = str_replace("\"", "", implode("|", $result));

	$result = caspioSelectAdv('TR_Client_Projects', "Project_Name, Project_ID, Project_Company_ID", "", "Project_Name ASC", FALSE, "\"", "^");
	$allProjectList = str_replace("\"", "", implode("|", $result));
?>

<script type="text/javascript">
	var projectList = "<?=$projectList?>";
	var allProjectList = "<?=$allProjectList?>";
	// requires for date filter dropdown outside the WO dashboard
	var v = "<?php echo $_GET["v"] ?>";
	var pageName = "wosView";
	function filterDate() {
		dropdown = document.getElementById('WOoptions');
		document.location = "wos.php?v=" + v + "&show=" + dropdown.selectedIndex;
	}
</script>

<br /><br />

<div align="center">

<table border="0" cellpadding="0" cellspacing="0" width="500">
	<tr>
<td align="center">

<h1>View Workorder</h1>
<br />

<!-- View Work Order 
- AJAX for tech lookup
- Email tech when assigned 10/17/07
- Added Complete Unchecked email 11/26/07
- Added Email tech when approved 11/27/07
- Added When WO changes email coordinator 12/10/07
-->

<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b0000b3j2f5f8f2i9f1f1h2g1","https:");}catch(v_e){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000b3j2f5f8f2i9f1f1h2g1">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>
</td>
	</tr>
</table>
</div>

<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
