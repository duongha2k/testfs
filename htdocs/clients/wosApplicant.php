<?php
include("../headerSimple.php");
require_once("../library/caspioAPI.php");
//    $denyList = caspioSelect("Deactivated_Techs_By_Clients", "TechID", "Company_ID = '$companyID'", "");
$displayPMTools = false;
include("includes/PMCheck.php");
//    ini_set("display_errors", 1);
// check login
if ($_SESSION['loggedIn'] != "yes" || $_SESSION['loggedInAs'] != "client")
    header("Location: ./");

// get variables
$winNum = caspioEscape($_GET["WorkOrderID"]);
if (!is_numeric($winNum))
    die();

if (!empty($_GET['back_url']))
    $_SESSION['BID_BACK_URL'] = $_GET['back_url'];

$companyID = (isset($_GET["v"]) ? caspioEscape($_GET["v"]) : "BLANKID");
$showBanned = (isset($_GET["sb"]) && $_GET["sb"] == 1) ? true : false;
$showFlsOnly = (isset($_GET["showFlsOnly"]) && $companyID == "FLS" ? $_GET["showFlsOnly"] == 1 : $companyID == "FLS");
//735
$sort = (isset($_GET["sort"]) ? $_GET["sort"] : "");
//end 735
$order = (isset($_GET["order"]) ? $_GET["order"] : 1);


error_reporting(E_ALL);

// result page configuration
$resultsPerPage = 50;
if (isset($_GET["page"]) && is_numeric($_GET["page"]))
    $currentResultPage = $_GET["page"];
else
    $currentResultPage = (isset($_SESSION["currentResultPage"]) ? $_SESSION["currentResultPage"] : 1);

/**
 *
 * 
 */
 //13925
$hide = '';
if(isset($_GET["bid"]) && !empty($_GET["bid"])){
		if(!isset($_SESSION['Hide'])){
					$valHide = 1;
					$hide = 'Un-Hide';
					$_SESSION['Hide']=1;
					
    $bidID = $_GET["bid"];
    $db = Core_Database::getInstance();
    $query = $db->select();
    $query->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('id'));
    $query->where('id = ?', $bidID);
    $result = Core_Database::fetchAll($query);
    if ($result) {
						$data = array('Hide' =>$valHide);
        Core_Database::update(Core_Database::TABLE_WORK_ORDER_BIDS, $data, "id = '" . $bidID . "'");
    }
			
			}else{
					$valHide = 0;
					$hide = 'Hide';
					if(isset($_SESSION['Hide'])){
						unset($_SESSION['Hide']);
                                        }
					$bidID = $_GET["bid"];
					$db = Core_Database::getInstance();
					$query = $db->select();
					$query->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('id'));
					$query->where('id = ?', $bidID);
					$result = Core_Database::fetchAll($query);
					if ($result) {
						$data = array('Hide' =>$valHide);
						Core_Database::update(Core_Database::TABLE_WORK_ORDER_BIDS, $data, "id = '" . $bidID . "'");
					}
		}
	}else{
			if($_GET['sb']==1){
				$hide = 'Un-Hide';
			}else{
				$hide = 'Hide';
			}
			
	}
//End 13925
//Simple sorts are handle in SQL while special sorts are handled by PHP after 
//the record set is returned.
$simpleSort = array("Tech_FName", "Tech_LName", "TechID", "BidAmount", "Comments", "Bid_Date");
//735
$specialSort = array("FLSID", "ContactInformation", "PreferLevel_USAuthorized", "SATRecommendedAvg", "SATPerformanceAvg", "DrugPassed", "PreferencePercent", "No_Shows", "CityStateZip", "SpecialDistance", "TotalWOs", "ISO", "FSExpert", "PublicCredentials", "ClientCredentials");

function getOrder($sortColumn, $order, $simpleSort, $specialSort)
{
    if ($sortColumn == "Bid_Date")
        $order = ($order == 1 ? "ASC" : "DESC");
    else
        $order = ($order == 1 ? "DESC" : "ASC");

    if (in_array($sortColumn, $simpleSort))
    {
        return $sortColumn . " " . $order;
    } elseif (!in_array($sortColumn, $specialSort))
    {
        return "BidAmount " . $order;
    }
    //return 'BidAmount' . " " . $order;
    return null;
}
//end 735
//Handler functions for usort() call below.
function FLSIDASC($bid1, $bid2) {
    return $bid1['FLSID'] < $bid2['FLSID'];
}

function FLSIDDESC($bid1, $bid2) {
    return $bid1['FLSID'] > $bid2['FLSID'];
}

function ContactInformationASC($bid1, $bid2) {
	return $bid1['Tech_LName'].$bid1['Tech_FName'] < $bid2['Tech_LName'].$bid2['Tech_FName'];
}

function ContactInformationDESC($bid1, $bid2) {
	return $bid1['Tech_LName'].$bid1['Tech_FName'] > $bid2['Tech_LName'].$bid2['Tech_FName'];
}

function CityStateZipASC ($bid1, $bid2) {
	if ($bid1["State"] == $bid2["State"]) {
		return strtoupper ($bid1["City"]) < strtoupper ($bid2["City"]);
	}
	return $bid1["State"] < $bid2["State"];
}

function CityStateZipDESC ($bid1, $bid2) {
	if ($bid1["State"] == $bid2["State"]) {
		return strtoupper ($bid1["City"]) > strtoupper ($bid2["City"]);
	}
	return $bid1["State"] > $bid2["State"];
}

function SpecialDistanceASC ($bid1, $bid2) {
	return $bid1["Distance"] < $bid2["Distance"];
}

function SpecialDistanceDESC ($bid1, $bid2) {
	return $bid1["Distance"] > $bid2["Distance"];
}

function PreferLevel_USAuthorizedASC($bid1, $bid2) {
	if ($bid1["DellMRACompliant"] == $bid2["DellMRACompliant"]) {
		return intval ($bid1["TotalWOs"]) < intval ($bid2["TotalWOs"]);
	}
	return $bid1['DellMRACompliant'] < $bid2['DellMRACompliant'];
}

function PreferLevel_USAuthorizedDESC($bid1, $bid2) {
	if ($bid1["DellMRACompliant"] == $bid2["DellMRACompliant"]) {
		return intval ($bid1["TotalWOs"]) > intval ($bid2["TotalWOs"]);
	}
	return $bid1['DellMRACompliant'] > $bid2['DellMRACompliant'];
}

function SATRecommendedAvgASC($bid1, $bid2) {
    return $bid1['SATRecommendedAvg'] < $bid2['SATRecommendedAvg'];
}

function SATRecommendedAvgDESC($bid1, $bid2) {
    return $bid1['SATRecommendedAvg'] > $bid2['SATRecommendedAvg'];
}

function SATPerformanceAvgASC($bid1, $bid2) {
    return $bid1['SATPerformanceAvg'] < $bid2['SATPerformanceAvg'];
}

function SATPerformanceAvgDESC($bid1, $bid2) {
    return $bid1['SATPerformanceAvg'] > $bid2['SATPerformanceAvg'];
}

function Bg_Test_Pass_LiteASC($bid1, $bid2) {
    return $bid1['Bg_Test_Pass_Lite'] < $bid2['Bg_Test_Pass_Lite'];
}

function Bg_Test_Pass_LiteDESC($bid1, $bid2) {
    return $bid1['Bg_Test_Pass_Lite'] > $bid2['Bg_Test_Pass_Lite'];
}

function PreferencePercentASC($bid1, $bid2) {
	$value1 = intval(trim($bid1["PerformancePercent"], "`"));
	$value2 = intval(trim($bid2["PerformancePercent"], "`"));

	return $value1 < $value2;
}

function PreferencePercentDESC($bid1, $bid2) {
	$value1 = intval(trim($bid1["PerformancePercent"], "`"));
	$value2 = intval(trim($bid2["PerformancePercent"], "`"));

	return $value1 > $value2;
}

function DrugPassedASC($bid1, $bid2) {
	$value1 = ($bid1['Bg_Test_Pass_Lite'] == "Pass") ? 1 : 0;
	$value1 += ($bid1['Bg_Test_Pass_Full'] == "Pass") ? 1 : 0;
	$value1 += ($bid1['DrugPassed'] == 1) ? 1 : 0;

	$value2 = ($bid2['Bg_Test_Pass_Lite'] == "Pass") ? 1 : 0;
	$value2 += ($bid2['Bg_Test_Pass_Full'] == "Pass") ? 1 : 0;
	$value2 += ($bid2['DrugPassed'] == 1) ? 1 : 0;

	return $value1 > $value2;

}

function DrugPassedDESC($bid1, $bid2) {
	$value1 = ($bid1['Bg_Test_Pass_Lite'] == "Pass") ? 1 : 0;
	$value1 += ($bid1['Bg_Test_Pass_Full'] == "Pass") ? 1 : 0;
	$value1 += ($bid1['DrugPassed'] == 1) ? 1 : 0;

	$value2 = ($bid2['Bg_Test_Pass_Lite'] == "Pass") ? 1 : 0;
	$value2 += ($bid2['Bg_Test_Pass_Full'] == "Pass") ? 1 : 0;
	$value2 += ($bid2['DrugPassed'] == 1) ? 1 : 0;

	return $value1 < $value2;
}

function No_ShowsASC ($bid1, $bid2) {
	return $bid1["No_Shows"] < $bid2["No_Shows"];
}

function No_ShowsDESC ($bid1, $bid2) {
	return $bid1["No_Shows"] > $bid2["No_Shows"];
}

//735
function FSExpertDESC($expert1, $expert2) {
    return $expert1['FSExpert'] > $expert2['FSExpert'];
}

function FSExpertASC($expert1, $expert2) {
    return $expert1['FSExpert'] < $expert2['FSExpert'];
}

function PublicCredentialsDESC($PubCre1, $PubCre2) {
    return $PubCre1['PublicCredentials'] > $PubCre2['PublicCredentials'];
}

function PublicCredentialsASC($PubCre1, $PubCre2) {
    return $PubCre1['PublicCredentials'] < $PubCre2['PublicCredentials'];
}

function ClientCredentialsDESC($ClientCre1, $ClientCre2) {
    return $ClientCre1['ClientCredentials'] > $ClientCre2['ClientCredentials'];
}

function ClientCredentialsASC($ClientCre1, $ClientCre2) {
    return $ClientCre1['ClientCredentials'] < $ClientCre2['ClientCredentials'];
}

//
//end 735
$db = Core_Database::getInstance();
$query = $db->select();
$query->from(Core_Database::TABLE_WORK_ORDERS, array( 'PayMax', 'WO_ID', 'Latitude', 'Longitude', 'Project_ID'));
$query->where('WIN_NUM = ?', $winNum);
$query->joinLeft(Core_Database::TABLE_PROJECTS, Core_Database::TABLE_PROJECTS . '.Project_ID = ' . Core_Database::TABLE_WORK_ORDERS . '.Project_ID', array('FixedBid'));
$woInfo = Core_Database::fetchAll($query);
if (isset ($woInfo[0])) {
	$woInfo = $woInfo[0];

	$WO_ID = $woInfo['WO_ID'];
	$woLat = $woInfo['Latitude'];
	$woLong = $woInfo['Longitude'];
}
else {
	$WO_ID = 0;
	$woLat = 0;
	$woLong = 0;
}
$resultsHtml = "";
//$resultColumnMap = array("First Name" => "Tech_FName|1", "Last Name" => "Tech_LName|1", "Tech ID" => "TechID|1", "FLSID" => "FLSID|1", "Bid" => "BidAmount|1", "Bid Comments" => "Comments|1", "Agreed $" => "AssignLnk|0", "Bid Date" => "Bid_Date|1", "Tags" => "PreferLevel|1", "Recommended<br/>(# rates)" => "SATRecommendedAvg|1", "Performance<br/>(# rates)" => "SATPerformanceAvg|1", "Bkgd. Check" => "Bg_Test_Pass_Lite|1", "Schedule" => "ScheduleLnk|0", "Remove" => "RemoveLnk|0");
//$resultColumnMap = array("First Name" => "Tech_FName|1", "Last Name" => "Tech_LName|1", "Tech ID" => "TechID|1", "FLSID" => "FLSID|1", "Bid" => "BidAmount|1", "Bid Comments" => "Comments|1", "Agreed $" => "AssignLnk|0", "Bid Date" => "Bid_Date|1", "Preferred" => "PreferLevel|1", "Recommended<br/>(# rates)" => "SATRecommendedAvg|1", "Performance<br/>(# rates)" => "SATPerformanceAvg|1", "Bkgd. Check" => "Bg_Test_Pass_Lite|1", "Schedule" => "ScheduleLnk|0", "Remove" => "RemoveLnk|0");
//$resultColumnMap = array("" => "RatingBox|0", "FS-Tech ID#" => "TechID|1", "Name" => "LastName|1", "Contact Information" => "PrimaryPhone|1", "City, State<br />Zip" => "City|1", "Miles" => "SpecialDistance|1","Bid" => 'Bid|1', "Bid Comments" => 'BidComments|1', "Agreed $" => 'Agreed|1', "Bid Date" => 'Bid Date|1', "Preference<hr class='headerHr'>Performance" => "PreferencePercent|1", "Total<br/>Work Orders" => "TotalWOs|1", "No&nbsp;Shows<hr class='headerHr'>Back Outs" => "No_Shows|1", "Preferred" => "PreferLevel|1", "Tags" => "USAuthorized|1", "Background<hr class='headerHr'>Drug Test" => "Bg_Test_Pass_Lite|1", "ISO" => "ISO|1", "Remove" => "Remove|1");

//Arrays matching the column titles with the column names in the database.
//The numbers after the pipe (|) represent true/false values and control
//whether the column is rendered as sortable.

//735
if ($companyID != "FLS")
    $resultColumnMap = array("Snap<br/>Shot" => "RatingBox|0",
        "FS-Tech<br/>ID#" => "TechID|1",
        "Name<hr class='thin_white_hr'>Contact Information" => "ContactInformation|1",
        "City,<br/>State Zip" => "CityStateZip|1",
        "Miles" => "SpecialDistance|1",
        "Bid $" => "BidAmount|1",
        "Bid Comments" => "Comments|1",
        "Agreed $" => "AssignLnk|0",
        "Bid Date" => "Bid_Date|1",
        "Preference<hr class='headerHr'>Performance" => "PreferencePercent|1",
        "Work<br/>Orders" => "TotalWOs|1",
        "No&nbsp;Shows<hr class='headerHr'>Back Outs" => "No_Shows|1",
        //"Tags" => "PreferLevel|1",
        "Client<br/>Credentials" => "ClientCredentials|1",
        "Public<br/>Credentials" => "PublicCredentials|1",
        "FS&ndash;Experts&#8482; &ndash; Skill Categories&nbsp;<a id=\"expertinfocontrols\" href=\"javascript:void(0)\"><img style=\"vertical-align: middle;height:20px;\" src=\"/widgets/images/get_info.png\"></a>" => "FSExpert|1",
		//13925
         "$hide" => "RemoveLnk|0");
		 //End 13925
else
    $resultColumnMap = array("Snap<br/>Shot" => "RatingBox|0",
        "FS-Tech<br/>ID#" => "TechID|1",
        "Name<hr class='thin_white_hr'>Contact Information" => "ContactInformation|1",
        "City,<br/>State Zip" => "CityStateZip|1",
        "Miles" => "SpecialDistance|1",
        "FLSID" => "FLSID|1",
        "Bid" => "BidAmount|1",
        "Bid Comments" => "Comments|1",
        "Agreed $" => "AssignLnk|0",
        "Bid Date" => "Bid_Date|1",
        "Preference<hr class='headerHr'>Performance" => "PreferencePercent|1",
        "Work<br/>Orders" => "TotalWOs|1",
        "No&nbsp;Shows<hr class='headerHr'>Back Outs" => "No_Shows|1",
        "Client<br/>Credentials" => "ClientCredentials|1",
        "Public<br/>Credentials" => "PublicCredentials|1",
        "FS&ndash;Experts&#8482; &ndash; Skill Categories&nbsp;<a id=\"expertinfocontrols\" href=\"javascript:void(0)\"><img style=\"vertical-align: middle;height:20px;\" src=\"/widgets/images/get_info.png\"></a>" => "FSExpert|1",
		//13925
         "$hide"  => "RemoveLnk|0",
		//End 13925


        //910
        "NumComments");
        //end 910
//end 735
$db = Core_Database::getInstance();
$query = $db->select();
$query->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('*'));
$query->where('WorkOrderID = ?', $winNum);

if ($sort == "Comments") $query->order ("!ASCII(Comments)");	//Forces empty comments to always be displayed last

$query->order(getOrder($sort, $order, $simpleSort, $specialSort));

if (!$showBanned)
    $query->where('Hide = ?', 0);

$result = Core_Database::fetchAll($query);

$numResult = sizeof($result);
$preferredList = Core_Tech::getClientPreferredTechsArray($companyID);

if (empty($preferredList))
	$preferredList = array(0);

for ($i = 0; $i < count ($preferredList); $i++) {
	$preferredList[$i] = "'{$preferredList[$i]}'";
}

$thechsInfo = array();
$bidListNumeric = array();
$bidList = array();
$th = array();
if (!empty ($result)) {
	foreach ($result as $bid) {
		$bidListNumeric[] = $bid['TechID'];
                $th[] = $bid['TechID'];
	    $bidList[] = "'{$bid['TechID']}'";
	}
}
//735

        $clientID = $_SESSION['ClientID'];
        $apiGPM = new Core_Api_Class();
        $isGPM = $apiGPM->isGPM($clientID);
        $BBCabling="";
        $ClientCre="";
        if($isGPM || in_array($companyID,array('BBAL','BBGC','BBGE','BBCN','BCSST','BBGN','BBJC','BBLC','BBNSG','BBNC','BBS','BB')))
        {
            $BBCabling = " 
                (SELECT Count(*) AS BBCablingCount FROM tech_certification  WHERE tech_certification.certification_id IN (46,47,48) AND tech_certification.TechID=`TechBankInfo`.`TechID`)
              + (SELECT Count(*) AS BBTelecomCount FROM tech_certification WHERE tech_certification.certification_id IN (49,50) AND tech_certification.TechID=`TechBankInfo`.`TechID`) " ;
        }
        if($isGPM)
        {
            $ClientCre = " 
                (SELECT Count(*) AS FSTagCount 
                FROM fstag_techs tagte 
                    INNER JOIN fstags tag ON tag.id = tagte.FSTagId 
                WHERE tagte.TechId=`TechBankInfo`.`TechID` AND tag.Active = 1 AND tag.DisplayOnClientCredentials = 1
                        AND tag.VisibilityId = 1 AND  tag.Title NOT LIKE '%Private Tech%') " ;
        }
        else
        {
            $ClientCre = " 
                (SELECT Count(*) AS FSTagCount 
                FROM fstag_techs tagte 
                    INNER JOIN fstags tag ON tag.id = tagte.FSTagId 
                WHERE tagte.TechId=`TechBankInfo`.`TechID` AND tag.Active = 1 AND tag.DisplayOnClientCredentials = 1
                    AND tag.VisibilityId = 1  AND  tag.Title NOT LIKE '%Private Tech%' 
                    AND tagte.FSTagId IN (SELECT FSTagId FROM fstag_clients WHERE CompanyId='$companyID')) " ;
        }
        $ClientCredentials = "(".$BBCabling." + ".$ClientCre.")";
$specialColumns = array("ISO" => "(CASE WHEN IFNULL(ISO_Affiliation, '') = '' THEN 'No' ELSE 'Yes' END)",
   	"PreferLevel" => "(CASE WHEN (" . ($preferredList && $preferredList[0] != '' ? 'TechID IN (' . implode(",", $preferredList) . ')' : '1 = 0') . ") THEN 'Yes' ELSE 'No' END)",
   	"SpecialDistance" => "Distance",
   	"TotalWOs" => "(IFNULL(Qty_FLS_Service_Calls,0) + IFNULL(Qty_IMAC_Calls,0))",
   	"DellMRACompliant" => "(CASE WHEN (Bg_Test_Pass_Full = 'Pass' AND DrugPassed = '1' AND Bg_Test_ResultsDate_Full >= DATE_SUB(NOW(), INTERVAL 12 MONTH) AND DatePassDrug >= DATE_SUB(NOW(), INTERVAL 12 MONTH)) THEN 'Passed' WHEN (Bg_Test_Pass_Full = 'Pass' AND DrugPassed = '1' AND (Bg_Test_ResultsDate_Full < DATE_SUB(NOW(), INTERVAL 12 MONTH) OR DatePassDrug < DATE_SUB(NOW(), INTERVAL 12 MONTH))) THEN 'Lapsed' ELSE '' END)", "LapsedDate" => "DATE_ADD((CASE WHEN Bg_Test_ResultsDate_Full > DatePassDrug THEN DatePassDrug ELSE Bg_Test_ResultsDate_Full END), INTERVAL 12 MONTH)",
        "FSExpert" => "(select count(`fsexpert_techs`.`FSExpertId`) from `fsexpert_techs` where `TechBankInfo`.`TechID` = `fsexpert_techs`.`TechId`)",
        "PublicCredentials" => "(
                                        (case when (Bg_Test_Pass_Lite = 'Pass' and Bg_Test_Pass_Full = 'Pass') or (Bg_Test_Pass_Lite = 'Pass' and  Bg_Test_Pass_Full <> 'Pass') then 1 else 0 end)
                                        +
                                        (case when DrugPassed = '1' or SilverDrugPassed='1' then 1 else 0 end )
                                        +
                                        (case when (CASE WHEN (Bg_Test_Pass_Full = 'Pass' AND DrugPassed = '1' AND Bg_Test_ResultsDate_Full >= DATE_SUB(NOW(), INTERVAL 12 MONTH) AND DatePassDrug >= DATE_SUB(NOW(), INTERVAL 12 MONTH)) THEN 'Passed' WHEN (Bg_Test_Pass_Full = 'Pass' AND DrugPassed = '1' AND (Bg_Test_ResultsDate_Full < DATE_SUB(NOW(), INTERVAL 12 MONTH) OR DatePassDrug < DATE_SUB(NOW(), INTERVAL 12 MONTH))) THEN 'Lapsed' ELSE '' END) <>'' then 1 else 0 end)
                                        +
                                        (case when isnull(`TechBankInfo`.`FS_Mobile_Date`) then 0 else 1 end)
                                        +
                                        (select count(`tech_certification`.`certification_id`) from `tech_certification` where `TechBankInfo`.`TechID` = `tech_certification`.`TechId` and `tech_certification`.`certification_id`=4)
                                        +
                                        (select count(`tech_certification`.`certification_id`) from `tech_certification` where `TechBankInfo`.`TechID` = `tech_certification`.`TechId` and `tech_certification`.`certification_id` in(3, 43))
                                        +
                                        (select count(`tech_ext`.`I9Status`) from `tech_ext` where `TechBankInfo`.`TechID` = `tech_ext`.`TechId` and `tech_ext`.`I9Status` <> 0)
                                        +
                                        (case when (CASE WHEN IFNULL(ISO_Affiliation, '') = '' THEN 'No' ELSE 'Yes' END)='yes' then 1 else 0 end)
                                    )",
        "ClientCredentials" => "$ClientCredentials"
        //943
        //,"TotalWosCompany"=>"(SELECT Count(WIN_NUM) FROM `work_orders` WHERE `work_orders`.Tech_ID=`TechBankInfo`.`TechID` AND Company_ID='".$companyID."' AND `work_orders`.Approved=1 AND (`work_orders`.Type_ID = 1 OR `work_orders`.Type_ID =2))"
        ,"TotalWosCompany"=>"(SELECT TotalWosCompany FROM `tech_company_wo_count` WHERE `tech_company_wo_count`.Tech_ID=`TechBankInfo`.`TechID` AND Company_ID='$companyID')"        
        //end 943
    );

$fieldListMap = array();
$fieldListMap = array("TechID" => "TechID", "LastName" => "LastName"
           , "FirstName" => "FirstName", "ISO" => "{$specialColumns["ISO"]}"
           , "PrimaryPhone" => "PrimaryPhone", "PrimPhoneType" => "PrimPhoneType"
           , "PrimaryEmail" => "PrimaryEmail", "City" => "City"
           , "State" => "State", "PreferencePercent" => "PreferencePercent"
           , "Qty_IMAC_Calls" => "Qty_IMAC_Calls", "Qty_FLS_Service_Calls" => "Qty_FLS_Service_Calls", "No_Shows" => "No_Shows", "Back_Outs" => "Back_Outs", "PreferLevel" => "{$specialColumns["PreferLevel"]}", "FS_Cert_Test_Pass" => "FS_Cert_Test_Pass", "Bg_Test_Pass_Lite" => "Bg_Test_Pass_Lite", "Bg_Test_Pass_Full" => "Bg_Test_Pass_Full", "FLSCSP_Rec" => "FLSCSP_Rec", "FLSID" => "FLSID", "BG_Test_ResultsDate_Lite" => "BG_Test_ResultsDate_Lite", "BG_Test_ResultsDate_Full" => "BG_Test_ResultsDate_Full", "DatePassDrug" => "DatePassDrug", "PerformancePercent" => "PerformancePercent", "SATPerformanceTotal" => "SATPerformanceTotal", "DrugPassed" => "DrugPassed", "Zip" => "ZipCode", "TotalWOs" => "{$specialColumns["TotalWOs"]}", "DellMRACompliant" => $specialColumns["DellMRACompliant"], "LapsedDate" => $specialColumns["LapsedDate"]);
$fieldListMap['SATRecommendedAvg'] = 'SATRecommendedAvg';
$fieldListMap['SATRecommendedTotal'] = 'IFNULL(SATRecommendedTotal,0)';
$fieldListMap['SATPerformanceAvg'] = 'SATPerformanceAvg';
$fieldListMap['SATPerformanceTotal'] = 'IFNULL(SATPerformanceTotal,0)';
$fieldListMap['Bg_Test_Pass_Lite'] = 'Bg_Test_Pass_Lite';
$fieldListMap['BG_Test_ResultsDate_Lite'] = 'BG_Test_ResultsDate_Lite';
$fieldListMap["FS_Mobile_Date"] = 'FS_Mobile_Date';
$fieldListMap['TechID'] = 'TechID';
$fieldListMap['FLSID'] = 'FLSID';
$fieldListMap['WMID'] = 'WMID';
$fieldListMap['FSExpert'] = $specialColumns['FSExpert'];
$fieldListMap['PublicCredentials'] = $specialColumns['PublicCredentials'];
$fieldListMap['ClientCredentials'] = $specialColumns['ClientCredentials'];
//943
$fieldListMap['TotalWosCompany'] = $specialColumns['TotalWosCompany'];
//end 943
//910
$fieldListMap['NumComments'] = 'NumComments';
//end 910

//$fieldListMap["DellMRACompliant"] = "(CASE WHEN (Bg_Test_Pass_Full = 'Pass' AND DrugPassed = '1' AND Bg_Test_ResultsDate_Full >= DATE_SUB(NOW(), INTERVAL 12 MONTH) AND DatePassDrug >= DATE_SUB(NOW(), INTERVAL 12 MONTH)) THEN 'Passed' WHEN (Bg_Test_Pass_Full = 'Pass' AND DrugPassed = '1' AND (Bg_Test_ResultsDate_Full < DATE_SUB(NOW(), INTERVAL 12 MONTH) OR DatePassDrug < DATE_SUB(NOW(), INTERVAL 12 MONTH))) THEN 'Lapsed' ELSE '' END)";
//$fieldListMap["LapsedDate"] = "DATE_ADD((CASE WHEN Bg_Test_ResultsDate_Full > DatePassDrug THEN DatePassDrug ELSE Bg_Test_ResultsDate_Full END), INTERVAL 12 MONTH)";
$fieldListMap['PreferLevel'] = "(CASE WHEN TechID IN (" . implode(",", $preferredList) . ") THEN '1' ELSE NULL END)";
if (empty($bidList))
    $bidList = array(0);

$criteria = "TechID IN (" . implode(",", $bidList) . ")";
$criteria .= " AND Deactivated = '0' AND (AcceptTerms = 'Yes' OR AcceptTerms = 'WM')";
$criteria .= $showFlsOnly ? " AND IFNULL(FLSID, '') != ''" : "";
//018
$criteria .= " AND HideBids != '1' ";
//end 018

$certSearch = new Core_TechCertifications;
$certResult = $certSearch->getTechCertification($th);

if (!$showBanned) {
	$deniedList = Core_Tech::getClientDeniedTechsArray($companyID);

if (!empty($deniedList)) {
	for ($i = 0; $i < count ($deniedList); $i++) {
		$deniedList[$i] = "'{$deniedList[$i]}'";
	}

	$criteria .= ' AND TechID NOT IN (' . implode(',', $deniedList) . ')';
}
}

$query = $db->select();
$query->from(Core_Database::TABLE_TECH_BANK_INFO, $fieldListMap)
        ->where($criteria);
$query = Core_Database::whereProximity($query, $woLat, $woLong, "Latitude", "Longitude", "999999", "Distance", false, true);
$info = $db->fetchAll($query);

foreach ($info as $val) {
    if (empty($val['TechID']))
        continue;
    $thechsInfo[$val['TechID']] = $val;
}
//    $thechInfo = Core_Caspio::caspioSelectWithFieldListMap(TABLE_MASTER_LIST, $fieldListMap, $criteria, '');

if ($result && sizeof($result) != 0) {
    foreach ($result as $key => &$bid) {
    	/* load tech information */
        if (isset($thechsInfo[$bid['TechID']])) {
	        foreach ($thechsInfo[$bid['TechID']] as $techFiedName => $techFiedValue)
	            $bid[$techFiedName] = $techFiedValue;
	        foreach ($woInfo as $techFiedName => $techFiedValue)
	            $bid[$techFiedName] = $techFiedValue;
        }
        else {
        	unset ($result[$key]);
    		$numResult = sizeof($result);
        }
    }

    if ($showFlsOnly || !$showBanned) {
    	foreach ($result as $k => $v) {
            if (!isset($thechsInfo[$result[$k]['TechID']]))
                unset($result[$k]);
        }
        $numResult = sizeof($result);
    }
}
//943
$user = new Core_Api_User();
$clientInfo = $user->getRepClientforCompany($companyID);
$CompanyName = $clientInfo[0]["CompanyName"];
//end 943

if ($result && sizeof($result) != 0) {
    $ratingSearch = new Core_Api_Class;
    $rangeStats = $ratingSearch->get12MonthsCallStats($_SESSION['Auth_User']['login'], $_SESSION['Auth_User']['password'], $bidListNumeric);
	$lifetimeStats =  $ratingSearch->getLifeTimeCallStats($_SESSION['Auth_User']['login'], $_SESSION['Auth_User']['password'], $bidListNumeric);

    /* do special sorting */
    if (null === getOrder($sort, $order, $simpleSort, $specialSort)) {
        $specialSort = array("FLSID", "ContactInformation", "PreferLevel_USAuthorized", "SATRecommendedAvg", "SATPerformanceAvg", "Bg_Test_Pass_Lite", "PreferencePercent", "DrugPassed", "No_Shows", "CityStateZip", "SpecialDistance", "FSExpert", "PublicCredentials", "ClientCredentials");
        if (in_array($sort, $specialSort))
        {
             if($sort == "FSExpert" || $sort == "PublicCredentials" || $sort == "ClientCredentials")
             {
                 $o = ($order == 0 ? "DESC" : "ASC");
             }
             else
             {
                 $o = ($order == 1 ? "ASC" : "DESC");
             }
            
            usort($result, $sort.$o);
        }
    }
    $offset = ($resultsPerPage * ($currentResultPage - 1));
    $searchResult = array_slice($result, $offset, $resultsPerPage);
    if (!empty($searchResult)) {
        if ($sort == 'Tech_Name' || $sort == 'TotalWOs' 
        	|| $sort == 'USAuthorized' || $sort == 'ISO') {

            // Obtain a list of columns
            foreach ($searchResult as $key => $row) {
           		$volume[$key] = $row["$sort"];
            }
            if ($order == 0)
                array_multisort($volume, SORT_DESC, $searchResult);
            else
                array_multisort($volume, SORT_ASC, $searchResult);
        }
    }

    /**
     * Build HTML
     */
    $resultsHtml = '<table id="appTable" class="resultsTable"><colgroup>
	<col width="60px"/>
	<col width="*"/>
	<col width="*"/>
	<col width="*"/>
	<col width="*"/>
	<col width="30px"/>
	<col width="30px"/>
	<col width="*"/>
	<col width="*"/>
	<col width="*"/>
	<col width="*"/>
	<col width="*"/>
	<col width="*"/>
	<col width="*"/>
	<col width="182px"/>
	<col width="*"/>
	</colgroup><thead><tr>';
    foreach ($resultColumnMap as $label => $map) {
        list($sortField, $allowSorting) = explode("|", $map);
        $myClass = ($sort == $sortField ? "sortAbleSelected" : "sortAble");
        $orderImg = $order == 0 ? "https://bridge.caspio.net/images/set5_ascending.gif" : "https://bridge.caspio.net/images/set5_descending.gif";
        $additionalClass = "";

        if ($sortField == "Comments")
            $additionalClass = " bidComments";
        else if ($sortField == "Bid_Date")
            $additionalClass = " bidDate";
        
        if($sortField=="FSExpert")
        {
            $sortCode = ($allowSorting == 1 ? "class=\"$myClass$additionalClass\" " : "class=\"nonSortAble$additionalClass\"");
            $orderImgTag = $sort == $sortField ? "<img src=\"$orderImg\" />" : "";
            $resultsHtml .= "<td $sortCode><a href=\"#\" onclick=\"sortBy('{$sortField}')\">$label</a> $orderImgTag</td>";
        }
        else
        {
            $sortCode = ($allowSorting == 1 ? "class=\"$myClass$additionalClass\" onclick=\"sortBy('{$sortField}')\"" : "class=\"nonSortAble$additionalClass\"");
            $orderImgTag = $sort == $sortField ? "<img src=\"$orderImg\" />" : "";
            $resultsHtml .= "<td $sortCode><a href=\"#\">$label</a> $orderImgTag</td>";
        }
    }
    $resultsHtml .= "</tr></thead><tbody>";
    //735
    $apiexts = new Core_Api_TechClass;
    // end 
    foreach ($searchResult as $rowIndex => $row) {
        $rowColor = (($rowIndex + 1) % 2 == 0 ? "evenRow" : "oddRow");
        $rowColor = $row['PreferLevel'] == 1 ? 'preferred' : $rowColor;

        // grab commonly used fields
        $techID = $row['TechID'];
        $bidAmount = $row['BidAmount'];
        $bidID = $row['id'];
        $comments = trim($row['Comments'], "`'");
        $fixedBid = (isset($row['FixedBid']) && $row['FixedBid']) ? true : false;
        $resultsHtml .= "<tr class=\"$rowColor\">";
        // Add new Column
        /**
         * Add rating 
         */
		 
		if ($row["TotalWOs"] < ($lifetimeStats[$techID]['imacsMain'] + $lifetimeStats[$techID]['serviceMain'])) {
			$row["TotalWOs"] = $lifetimeStats[$techID]['imacsMain'] + $lifetimeStats[$techID]['serviceMain'];
			$row["PreferencePercent"] = $lifetimeStats[$techID]['recommendedAvg'];
			$row["SATRecommendedTotal"] = $lifetimeStats[$techID]['recommendedTotal'];
			$row["PerformancePercent"] = $lifetimeStats[$techID]['performanceAvg'];
			$row["SATPerformanceTotal"] = $lifetimeStats[$techID]['performanceTotal'];
			$row["Back_Outs"] = (empty($lifetimeStats[$techID]['backOutMain']) ? "0" : (string)$lifetimeStats[$techID]['backOutMain']);
			$row["No_Shows"] = empty($lifetimeStats[$techID]['noShowMain']) ? "0" : (string)$lifetimeStats[$techID]['noShowMain'];
		}

        $techRangeStats = $rangeStats[$techID];
        $satRecTotal = trim($row["SATRecommendedTotal"], "`");
        $prefPercent = $satRecTotal == NULL ? "0% (0)" : number_format($row["PreferencePercent"], 0) . "% ($satRecTotal)";
        $ratings = $prefPercent;
        $ratings .= "<hr>";
        $satPerfTotal = trim($row["SATPerformanceTotal"], "`");
        $perfPercent = $satPerfTotal == NULL ? "0% (0)" : number_format(trim($row["PerformancePercent"], "`"), 0) . "% ($satPerfTotal)";
        $ratings .= $perfPercent;
//        print $ratings;
        
//        $satRecTotal = trim($row["SATRecommendedTotal"], "`");
//        //$ratings = $row["PreferencePercent"];
//        $prefPercent = $satRecTotal == NULL ? "0% (0)" : number_format($row["PreferencePercent"], 0) . "% ($satRecTotal)";
//        $ratings = $prefPercent;
//        $ratings .= "<hr>";
//        $satPerfTotal = trim($row["SATPerformanceTotal"], "`");
//        $perfPercent = $satPerfTotal == NULL ? "0% (0)" : number_format(trim($row["PerformancePercent"], "`"), 0) . "% ($satPerfTotal)";
//        //$ratings .= $row["PerformancePercent"];
//        $ratings .= $perfPercent;
//        print $ratings;
        
        
        /**
         *  get TechInfo
         */
        //$companyID = $_GET["v"];


        $techName = $row["Tech_FName"];
        $techName .= "&nbsp;";
        $techName .= $row["Tech_LName"];
        $PrimaryPhoneStatus = "1";
        $SecondaryPhoneStatus = "1";

       if (!empty($techID)) {
            $Core_Api_TechClass = new Core_Api_TechClass;
            $result = $Core_Api_TechClass->getExts($techID);
            if (!empty($result['TechID'])) {
                $PrimaryPhoneStatus = $result['PrimaryPhoneStatus'];
                $SecondaryPhoneStatus = $result['PrimaryPhoneStatus'];
            }
        }
        $contactInfo = $row["PrimaryPhone"];
        if($PrimaryPhoneStatus == "1" && trim($row["PrimaryPhone"]) != "")
        {
            $contactInfo .= "&nbsp;<img class=\"imgClassReQuitePhone\" align=\"absmiddle\" style=\"display:\"";
            $contactInfo .= " src=\"" . (($PrimaryPhoneStatus == "1") ? '/widgets/images/ico_phonex16.png' : '/widgets/images/ico_Notphonex16.png') . '"';
        $contactInfo .= " title=\"" . (($PrimaryPhoneStatus == "1") ? 'Click to notify ' . $techName . ' that this Phone # is Invalid' : 'Invalid Phone # Reported') . '"';
            $contactInfo .= ($PrimaryPhoneStatus == "1") ? " onclick=\"javascript:detailObject.onInit.RequitePhoneClick(this,'" . $techID . "' , 1,'" .base64_encode($techName) . "','".$row["PrimaryPhone"]."');\"" : " ";

        $contactInfo .= " />";
        }
        $contactInfo .= "<br/>";
        $contactInfo .= $row["PrimaryEmail"];

        // fields pulled from DB
        //910
        $client = new Core_Api_Class();
        $clientID = $_SESSION['ClientID'];
        $apiGPM = new Core_Api_Class();
        $isGPM = $apiGPM->isGPM($clientID);
        //end 910
        foreach ($resultColumnMap as $label => $mapping) {
            // Processes each column returned and outputs result. By default, column is outputed as it is formated by the the SQL query
            $mapping = explode("|", $mapping);
            $mapping = $mapping[0];
            $style = "";
            //735
            switch ($mapping) {
                case "USAuthorized":
                    $style = "min-width: 60px";

                    $extsUSAuth = $apiexts->getExts($techID);
                    if ($extsUSAuth["I9Status"] == 2) {
                        $field = "<span style='height: 19px;display:block;'><img src=\"/widgets/images/USAuthorized_16.png\" class=\"hoverText\" title=\"Authorized to perform services in the United States: 03/05/2012\" /></span>";
                    } elseif ($extsUSAuth["I9Status"] == 1) {
                        $field = "<span style='height: 19px;display:block;'><img src=\"/widgets/images/USPending_19.png\" class=\"hoverText\" title=\"Authorization to perform services in the United States is pending\" /></span>";
                    } else {
                        $field = "<span style='height: 19px;display:block;'></span>";
                    }
                    break;
                case "No_Shows":
                    if ($row['No_Shows'] != "") {
                        $field = "<span style='margin-left: 23px;'>" . $row['No_Shows'] . "</span>";
                    } else {
                        $field = "<span style='height: 19px;display:block;'></span>";
                    }
                    $field .= "<hr>";
                    if ($row['Back_Outs'] != "") {
                        $field .= "<span style='margin-left: 23px;'>" . $row['Back_Outs'] . "</span>";
                    } else {
                        $field = "<span style='height: 19px;display:block;'></span>";
                    }
                    break;
                case "PreferencePercent":
                    $style = "text-align:center";
                    $field = $ratings;
                    break;
                case "SpecialDistance":
			$field = (!empty($row["Distance"]) || $row["Distance"] == 0 ? round($row["Distance"], 0) : "-");
                    break;
                case "TotalWOs";
                    //943
                    $style = "text-align:center";
                    $field = "<span title='All FieldSolutions Work Orders'>".$row["TotalWOs"]."</span>";
                    $field .= "<hr/>";
                    $field .= "<span title='All ". $CompanyName." Work Orders'>".$row["TotalWosCompany"]."</span>";
                    //end 943
                    break;
                case "Back_Outs":
                    if ($row['No_Shows'] != "") {
                        $field = "<span style='margin-left: 23px;'>" . $row['No_Shows'] . "</span>";
                    } else {
                        $field = "<span style='height: 19px;display:block;'></span>";
                    }
                    $field .= "<hr>";
                    if ($row['Back_Outs'] != "") {
                        $field .= "<span style='margin-left: 23px;'>" . $row['Back_Outs'] . "</span>";
                    } else {
                        $field = "<span style='height: 19px;display:block;'></span>";
                    }
                    break;
                case 'CityStateZip':
                    $location = $row["City"];
                    $location .= ",<br/>";
                    $location .= $row["State"] . "&nbsp;";
                    $location .= $row["Zip"];
                    $field = $location;
                    break;
                case "RatingBox":
                    //910
                    $style="text-align: left;padding: 0pt 5px;";
                    $rating = $techRangeStats['recommendedAvg'];
                    $numberRankings = $techRangeStats['recommendedTotal'];
                    if ($rating == NULL)
                    {
                        $rating = "";
                        $numberRankings = 0;
                    }

                    $color = "#AAAAAA";

                    $callsSumm = $techRangeStats['imacsMain'] + $techRangeStats['serviceMain'];
                    if ($callsSumm == 0)
                    {
                        $color = "#AAAAAA";
                    }

                    if ($callsSumm >= 5)
                    {
                        $color = "#FFFF00";
                    }

                    if ($techRangeStats['noShowMain'] >= 2 || ($callsSumm > 0 && !empty($techRangeStats['recommendedAvg']) && $techRangeStats['recommendedAvg'] <= 90 && !empty($techRangeStats['performanceAvg']) && $techRangeStats['performanceAvg'] <= 90))
                    {
                        $color = "#FF0000";
                    }
                    else if ($callsSumm >= 10 && !empty($techRangeStats['recommendedAvg']) && $techRangeStats['recommendedAvg'] >= 95 && !empty($techRangeStats['performanceAvg']) && $techRangeStats['performanceAvg'] >= 95 && $techRangeStats['backOutMain'] < 6 && $techRangeStats['noShowMain'] < 3)
                    {
                        $color = "#00FF00";
                    }
                    
                    $TechNetLikes = Core_Tech::getTechNetLikes($techID);
                    $numNetLikes = $TechNetLikes['numNetLikes'];
                    $numComments = 0;
                    if($isGPM)
                    {
                        $numComments = $row['NumComments'];                        
                    }
                    else
                    {
                        $numComments = Core_Comments::getNumComments_ForTech_andNonGPMClient($techID,$clientID,$companyID);
                    }
                    
                    $NetLikescolor = '';
                    $imageThumbs = '/widgets/images/thumbs-up.png';//14027
                    if($numNetLikes < 0)
                    {
                        $NetLikescolor = 'color:red';
                        $imageThumbs = '/widgets/images/thumbs-down.png';//14027
                    }
                    
                    $field ="<table width='50px'>
                                <tr valign='middle' height='17' style='text-align:center;border-right:0px'>
                                    <td style='text-align:center;padding:0px;width:15px;'>
                                        <div id=\"myRating$techID\" class=\"ratingBox\" style=\"background-color: $color; width: 10px; height: 10px;margin-left:4px;\"></div>
                                        <script type=\"text/javascript\">colorCode = document.getElementById(\"myRating$techID\");colorCode.rating = \"$rating\";colorCode.numRating = \"$numberRankings\";</script>
                                    </td>
                                    <td style='text-align:center;padding:0px;'></td>
                                </tr>
                                <tr valign='middle' height='17' style='text-align:center;'>
                                    <td style='text-align:center;padding:0px;width:15px;'>
                                        <a class='cmdOpenTechProfileShow' href='wosViewTechProfile.php?simple=0&TechID=$techID&v=$companyID&cl=1' title='Add/View Comments'>
                                            <img src='$imageThumbs' height='16' />         
                                        </a>
                                    </td>
                                    <td style='text-align:center;padding:0px;'>
                                        <a style='$NetLikescolor' class='cmdOpenTechProfileShow' href='wosViewTechProfile.php?simple=0&TechID=$techID&v=$companyID&cl=1' title='Add/View Comments'>
                                            ".$numNetLikes."
                                        </a>
                                    </td>
                                </tr>
                                <tr valign='middle' height='17' style='text-align:center;'>
                                    <td style='text-align:center;padding:0px;width:15px;'>
                                        <a class='cmdOpenTechProfileShow' href='wosViewTechProfile.php?simple=0&TechID=$techID&v=$companyID&cl=1' title='Add/View Comments'>
                                            <img src='/widgets/images/Commentbubble_icon.png?t=1' />
                                        </a>
                                    </td>
                                    <td style='text-align:center;padding:0px;'>
                                        <a class='cmdOpenTechProfileShow' href='wosViewTechProfile.php?simple=0&TechID=$techID&v=$companyID&cl=1' title='Add/View Comments'>
                                            ".$numComments."
                                        </a>
                                    </td>
                                </tr>
                            </table>";
                    //end 13910
                    break;
                case "TechID":
                    $field = "<a class='cmdOpenTechProfileShow' href=\"wosViewTechProfile.php?TechID=$techID&back=1&v=" . $_GET["v"] . "\">$techID</a>";
                    $field .= "<br/><a class='BidSchedule' id='techScheduleLnk' href=\"javascript:void(0);\" techid=$techID >Schedule</a>";
                    break;
                case "Tech_Name":
                    $field = $techName;
                    break;
                case "ContactInformation":
                    $field = "<b>".$techName . '</b>';
					if (!empty ($row["WMID"])) $field .= "<img src='/widgets/images/wm32.png' style='vertical-align: middle;' alt='Work Order sourced through Work Market' />";
					$field .= '<br/>';
                    $field .= $contactInfo;
                    break;
                case "SATRecommendedAvg":
                    $total = $row['SATRecommendedTotal'];
                    $field = $total == 0 ? "0.0 (0)" : number_format($row['SATRecommendedAvg'], 1) . " ($total)";
                    break;
                case "SATPerformanceAvg":
                    $total = $row['SATPerformanceTotal'];
                    $field = $total == 0 ? "0.0 (0)" : number_format($row['SATRecommendedAvg'], 1) . " ($total)";
                    break;
                case "AssignLnk":
                    $defaultBid = $fixedBid ? $row['PayMax'] : '';
                    $readonly = $fixedBid ? "disabled='disabled'" : "";
                    $field = "<div style='text-align: center'><input id='agreedTechPay$bidID' name='agreedTechPay$bidID' type='text' class='agreedTechPayBox' size='4' value = '$defaultBid' $readonly />
                        <input type='hidden' id='bidComments$bidID' name='bidComments$bidID' value='$comments'/><br/><a id='assignBtn$bidID' name='assignBtn$bidID' href='javascript:void(0)' class='assignBtn'>Assign</a><input id='techID$bidID' name='techID$bidID' type='hidden' value='$techID' /></div>";
                    break;
                case "RemoveLnk":
                    //$field = "<a href=\"javascript:remove_tech('$bidID');\">Remove</a>";
                    $url = '/clients/wosApplicant.php?sort=' . $sort . '&order=' . $order . '&sb=' . ($showBanned ? 1 : 0) . '&v=' . $companyID . '&WorkOrderID=' . $winNum . '&page=' . $currentResultPage . '&bid=' . $bidID;
                    $field = "<a href=\"" . $url . "\">$hide</a>";
                    break;
                case "Bid_Date":
                    $objDate = new Zend_Date($row['Bid_Date'], 'yyyy-MM-dd hh:mm:ss');
                    $field = $objDate->toString('MM/dd/yyyy') . '<br/>' . $objDate->toString('h:mm:ss') . "&nbsp;" .  $objDate->toString('a');
                    break;
                case "Bg_Test_Pass_Lite":
                    if ($row['Bg_Test_Pass_Lite'] == "Pass") {
                        $field = "<img src=\"/widgets/images/check_green.gif\" align=\"absmiddle\" style=\"margin-left: 23px;\">";
                    }
                    else
                        $field = "No";
                    break;
                case 'PreferLevel' :
                    $field = $row['PreferLevel'] == 1 ? 'Yes' : 'No';
                    /* $style = "min-width: 60px";
                      $field = "<span style='white-space:nowrap; position: relative; top: -3px'>";
                      if ($row['DellMRACompliant'] == 'Passed')
                      $color = "<span><img src='/widgets/images/dellMRAPassed.png' style='position: relative; top: -1px' title='Dell MRA Compliant' /></span>";
                      else if ($row['DellMRACompliant'] == 'Lapsed')  {
                      $date = new Zend_Date($row['LapsedDate'],'YYYY-mm-dd');
                      if (!empty($date))
                      $date = $date->toString('mm/dd/YYYY');
                      else $date = "";
                      $color = "<span><img src='/widgets/images/dellMRALapsed.png' style='position: relative; top: -1px' title='Dell MRA Compliance Lapsed: $date'/></span>";
                      }
                      if (!empty($row['DellMRACompliant']))
                      $field .= $color;
                      else $field = "<span style='white-space:nowrap'>&nbsp;";
                      $field .= "<hr>";
                      @$cert = $certResult[$techID];
                      if (sizeof($cert) > 0 && array_key_exists(2, $cert))
                      $field .= "<span style='white-space:nowrap'><img src='/widgets/images/DeVry.png' style='vertical-align: text-bottom'/></span>";
                      else $field .= "&nbsp;";
                      if (sizeof($cert) > 0 && array_key_exists(3, $cert))
                      $field .= "<span style='margin-left:15px'>TBC</span>";
                      $field .= "</span>"; */
                    break;
                case 'BidAmount': 
                	 $WO = $apiexts->getProjectIdByWinNum($winNum);
                	if($companyID == "FLS"){
                		if (empty ($_GET['projects']))
                			$project_id = $_GET['projects'];
                		else
                			$project_id = $woInfo["Project_ID"];
                		$WOs = $apiexts->getWOs_BidsByTechAndCompanyAndProject($techID,$companyID,$project_id);
                		$numbid = count(explode(",",$WOs));
                	}else{ 
                      $numbid = $apiexts->getNumOfBidsByTechAndCompanyAndProject($techID,
                            $companyID,$WO);
                	}     
                        if(trim($numbid) == "") $numbid = 0;
                        $field = $bidAmount;
                        $field .= '<br><a href="#" onclick="javascript:
                                if( typeof (window.parent) != \'undefined\') 
                                    { window.parent.location = \'/clients/wos.php?v='.$companyID.'&tab=published&techid='.$techID.'&projectid='.$WO.'\' } 
                                else { window.location = \'/clients/wos.php?v='.$companyID.'&tab=published&techid='.$techID.'&projectid='.$WO.'\' }">
                             Bids('.$numbid.')</a>';
                 break;
                case "ClientCredentials":
                    $colnum = $isGPM?3:2;
                    $rownum = 0;
                    $colspan = $isGPM?2:1;
                    $isviewmore = false;

                    $BlackBoxCablingStatus = $Core_Api_TechClass->GetBlackBoxCablingStatus($techID);
                    $BlackBoxTelecomStatus = $Core_Api_TechClass->getBlackBoxTelecomStatus($techID);
                    $isdisplayBlackBoxCabling = !empty($BlackBoxCablingStatus)?1:0;
                    $isdisplayBlackBoxTelecom = !empty($BlackBoxTelecomStatus)?1:0;
                    $isdisplayblackbox =0;
                    if($isGPM || in_array($companyID,array('BBAL','BBGC','BBGE','BBCN','BCSST','BBGN','BBJC','BBLC','BBNSG','BBNC','BBS','BB')))
                    {
                        $isdisplayblackbox = 1;
                    }
                    //13912
                    $extFilters = array();
                    if($isGPM)
                    {
                        $isOtherCompanyTag = 1;
                        $callerIsGPM = 1;
                        $extFilters['VisibilityId']=4;
                    }
                    else
                    {
                        $isOtherCompanyTag = 0;
                        $callerIsGPM = 0;
                        $extFilters['VisibilityId']=1;
                    }
                    //931
                    $extFilters['SortByTitle']=1;
                    //end 931
                    $FSTagClass = new Core_Api_FSTagClass();
                    //839
                    $getFSTagList = $FSTagClass->getFSTagList_forTech_forCompany($techID, $companyID, $isOtherCompanyTag, 1, $callerIsGPM, null, 1, $extFilters);
                    $getFSTagListNew = array();
                    foreach($getFSTagList as $elementKey => $element)
                    {
                        if($getFSTagList[$elementKey]['Name']=="BlackBoxCabling")
                        {
                            if($isdisplayblackbox==1)
                            {
                                if($isdisplayBlackBoxCabling==1)
                                {
                                    $getFSTagListNew[] = $getFSTagList[$elementKey];
                                }
                            }
                        }
                        else if($getFSTagList[$elementKey]['Name']=="BlackBoxTelecom")
                        {
                            if($isdisplayblackbox==1)
                            {
                                if($isdisplayBlackBoxTelecom==1 )
                                {
                                    $getFSTagListNew[] = $getFSTagList[$elementKey];
                                }
                            }
                        }
                        else if(!empty($getFSTagList[$elementKey]['TechId']))
                        {
                            $getFSTagListNew[] = $getFSTagList[$elementKey];
                        }
                    }
                    $FSTagNo = count($getFSTagListNew);
                    $rownum = 4;
                    if($FSTagNo>12)
                    {
                        $isviewmore = true;
                    }
                    $field = "";
                    
                    if($isGPM)
                    {
                        $field .= "<table cellspacing='0' style=\"text-align:left;\" width=\"111px\">";
                        $field .= "<colgroup>";
                        $field .= "<col width=\"33%\">";
                        $field .= "<col width=\"33%\">";
                        $field .= "<col width=\"33%\">";
                        $field .= "</colgroup>";
                    }
                    else
                    {
                        $field .= "<table cellspacing='0' style=\"text-align:left;\" width=\"74px\">";
                        $field .= "<colgroup>";
                        $field .= "<col width=\"50%\">";

                        $field .= "<col width=\"50%\">";
                        $field .= "</colgroup>";
                    }
                    $k=0;
                    //912
                    $defaultnotedit = array('ErgoMotionCertifiedTechnician'=>'ErgoMotionCertifiedTechnician',
                        'TechForceICAgreement'=>'TechForceICAgreement',
                        'Dell_MRA_Compliant'=>'Dell_MRA_Compliant',
                        'BootCampCertified'=>'BootCampCertified',
                        'FSMobile'=>'FSMobile');
                    //end 912
                    for($i=0;$i<$rownum;$i++)
                    {
                        $field .= "<tr style='text-align:center;'>";
                        for($j=0;$j<$colnum;$j++)
                        {
                            if($i==$rownum-1 && $isviewmore && $j>0)
                            {
                                $field .= "<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;' colspan='$colspan'>";
                                $field .= "<a href='wosViewTechProfile.php?simple=0&TechID=$techID&v=$companyID&C=1' style='text-decoration:underline;' class='cmdOpenTechProfileShow'>See More...</a>";
                                $field .= "</td>";
                                break;
                            }
                            $field .= "<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
                            if(empty($getFSTagListNew[$k]))
                            {
                                $field .= "<div style='height:19px;'>&nbsp;</div>";
                            }
                            else
                            {
                                if($getFSTagListNew[$k]['Name']=="BlackBoxTelecom")
                                {
                                    $BlackBoxdate = $BlackBoxTelecomStatus['Date'];
                                    if (!empty($BlackBoxdate))
                                    {
                                        $strBlackBoxDate = date_format(new DateTime($BlackBoxdate), "m/d/Y");
                                    } else
                                    {
                                        $strBlackBoxDate = "";
                                    }

                                    if ($BlackBoxTelecomStatus['CertID'] == "49")
                                    {
                                        $BlackBoximg = "BTT1.png";
                                        $BlackBoxStatus = "Black Box Telecom - " . $BlackBoxTelecomStatus['LevelText'] . ": " . $strBlackBoxDate;
                                    } else if ($BlackBoxTelecomStatus['CertID'] == "50")
                                    {
                                        $BlackBoximg = "BTT2.png";
                                        $BlackBoxStatus = "Black Box Telecom - " . $BlackBoxTelecomStatus['LevelText'] . ": " . $strBlackBoxDate;
                                    }

                                    $field .= "<img id='BlackBoxCablingimgtag' style='" . ($BlackBoxTelecomStatus['Status'] != 'Pending' ? "display:'';" : "display:none;") . "' height='19px' bt-xtitle='" . $BlackBoxStatus . "' class='hoverText' src='/widgets/images/" . $BlackBoximg . "' title=''/>";
                                }
                                else if($getFSTagListNew[$k]['Name']=="BlackBoxCabling" )
                                {
                                    $BlackBoxdate = $BlackBoxCablingStatus['Date'];
                                    if (!empty($BlackBoxdate))
                                    {
                                        $strBlackBoxDate = date_format(new DateTime($BlackBoxdate), "m/d/Y");
                                    } else
                                    {
                                        $strBlackBoxDate = "";
                                    }
                                    if ($BlackBoxCablingStatus['CertID'] == "46")
                                    {
                                        $BlackBoximg = "BTC1.png";
                                        $BlackBoxStatus = "Black Box Cabling - " . $BlackBoxCablingStatus['LevelText'] . ": " . $strBlackBoxDate;
                                    } else if ($BlackBoxCablingStatus['CertID'] == "47")
                                    {
                                        $BlackBoximg = "BTC2.png";
                                        $BlackBoxStatus = "Black Box Cabling - " . $BlackBoxCablingStatus['LevelText'] . ": " . $strBlackBoxDate;
                                    } else if ($BlackBoxCablingStatus['CertID'] == "48")
                                    {
                                        $BlackBoximg = "BTC3.png";
                                        $BlackBoxStatus = "Black Box Cabling - " . $BlackBoxCablingStatus['LevelText'] . ": " . $strBlackBoxDate;
                                    } 

                                    $field .= "<img id='BlackBoxCablingimgtag' style='" . ($BlackBoxCablingStatus['Status'] != 'Pending' ? "display:'';" : "display:none;") . "' height='19px' bt-xtitle='" . $BlackBoxStatus . "' class='hoverText' src='/widgets/images/" . $BlackBoximg . "' title=''/>";
                                }
                                else
                                {
                                    if(empty($getFSTagListNew[$k]['Date']))
                                    {
                                        $date = "";
                                    }
                                    else
                                    {
                                        $date = new Zend_Date($getFSTagListNew[$k]['Date'], 'YYYY-mm-dd'); 
                                    }

                                    if(!empty($getFSTagListNew[$k]['TagArt']))
                                    {
                                        $TagArt = "https://s3.amazonaws.com/wos-docs/tags/". str_replace ("|", "", $getFSTagListNew[$k]['TagArt']);
                                    }
                                    else
                                    {
                                        $TagArt = "/widgets/images/check_green.gif";
                                    }

                                    if(empty($getFSTagListNew[$k]['ExistingImagePath']))
                                    {
                                        $ExistingImagePath = $TagArt;
                                    }
                                    else
                                    {
                                        $ExistingImagePath = $getFSTagListNew[$k]['ExistingImagePath'];
                                    }
                                    //931
                                    $HasMultiTagLevels="";
                                    if($getFSTagListNew[$k]['HasMultiTagLevels']==1 && !empty($getFSTagListNew[$k]['TagLevelsData']))
                                    {      
                                        $HasMultiTagLevels = ": L".$getFSTagListNew[$k]['LevelOrder']."&nbsp;-&nbsp;".$getFSTagListNew[$k]['LevelTitle'];
                                    }
                                    $Title = $getFSTagListNew[$k]['Title'];
                                    if (!empty($date))
                                    {
                                        $date = ": ".$date->toString('mm/dd/YYYY');
                                    }
                                    else
                                    {
                                        $date = "";
                                    }
                                    $field .= "<img src='$ExistingImagePath' class='hoverText' bt-xtitle='$Title$HasMultiTagLevels$date' height='19px' />";
                                    //end 931
                                }
                            }
                            $field .= "</td>";
                            $k++;
                        }
                        $field .= "</tr>";
                    }
                    $field .= "</table>";
                    break;
                    //end 839
                case "PublicCredentials":
                    $cert = $certResult[$techID];
                    $apiexts = new Core_Api_TechClass;
                    //background check
                    $field ="<table cellspacing='0' cellpadding='0' width='74px'>";
                    $field .="<colgroup><col width=\"50%\"><col width=\"50%\"></colgroup>";
                    $field .="<tr style=\"text-align:center;\">";
                    //Background Check
                    $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
                    if ($row["Bg_Test_Pass_Lite"] == "Pass" && $row["Bg_Test_Pass_Full"] == "Pass") {
                        $basicDate = strtotime($row['BG_Test_ResultsDate_Lite']);
                        $basicDate = date("m/d/Y", $basicDate);
                        $compDate = strtotime($row['BG_Test_ResultsDate_Full']);
                        $compDate = date("m/d/Y", $compDate);
                        $field .= "<img src=\"/widgets/images/BCPlus.png\" height=\"19px\" class=\"hoverText\" bt-xtitle=\"Comprehensive Background Check - {$compDate}\" style=\"margin-left: 2px;\"/>";
                    } elseif ($row["Bg_Test_Pass_Lite"] == "Pass" && $row["Bg_Test_Pass_Full"] != "Pass") {
                        $basicDate = strtotime($row['BG_Test_ResultsDate_Lite']);
                        $basicDate = date("m/d/Y", $basicDate);
                        $field .= "<img src=\"/widgets/images/BC.png\" height=\"19px\" class=\"hoverText\" bt-xtitle=\"Basic Background Check - {$basicDate}\" />";
                    } else {
                        $field.="<div style='height:19px;'>&nbsp;</div>";
                    }
                    $field .="</td>";

                    //gold drug test
                    $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
                    $img="&nbsp;";
                    if ($row["DrugPassed"] == 1)
                    {
                        $Date = strtotime($row["DatePassDrug"]);
                        $Date = date("m/d/Y", $Date);
                        $img = "dt9.png";
                        $title ="Gold Drug Test (9 Panel)";
                    }
                    else if ($row["SilverDrugPassed"] == 1)
                    {
                        $Date = strtotime($row["DatePassedSilverDrug"]);
                        $Date = date("m/d/Y", $Date);
                        $img = "dt5.png";
                        $title ="Silver Drug Test (5 Panel)";
                    }
                    else
                    {
                        $Date=null;
                        $title=null;
                    }

                    if($img!="&nbsp;")
                    {
                        $field .="<img src='/widgets/images/$img' height='19px' class='hoverText' bt-xtitle='$title &ndash; {$Date}' />";
                    }
                    else
                    {
                        $field.="<div style='height:19px;'>&nbsp;</div>";
                    }
                    $field .="</td>";
                    $field .="</tr>";
                    $field .="<tr style=\"text-align:center;\">";
                    //dell
                    $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
                    if(!empty($row['DellMRACompliant']))
                    {
                        if ($row['DellMRACompliant'] == 'Passed')
                        {
                            $field .= "<img src='/widgets/images/dellMRAPassed.png' height='19px' class='hoverText' bt-xtitle='Dell MRA Compliant' />";
                        }
                        else if ($row['DellMRACompliant'] == 'Lapsed')
                        {
                            $date = new Zend_Date($row['LapsedDate'], 'YYYY-mm-dd');
                            if (!empty($date))
                            {
                                $date = ": ".$date->toString('mm/dd/YYYY');
                            }
                            else
                            {
                                $date = "";
                            }

                            $field .= "<img src='/widgets/images/dellMRAPassed.png' height='19px' class='hoverText' bt-xtitle='Dell MRA Compliance Lapsed $date'/>";
                        }
                    }
                    else
                    {
                        $field.="<div style='height:19px;'>&nbsp;</div>";
                    }
                    $field .="</td>";
                    //FS-mobile
                    $FSmobilimg="&nbsp;";
                    $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
                    if(!empty($row['FS_Mobile_Date']))
                    {
                        $Date = strtotime($row['FS_Mobile_Date']);
                        $Date = date("m/d/Y", $Date);
                        $FSmobilimg = "fs_mobile_36x32.png";
                        $title ="I Have FS-Mobile";
                    }
                    else
                    {
                        $Date=null;
                        $title=null;
                    }
                    if($FSmobilimg!="&nbsp;")
                    {
                        $field .="<img src='/widgets/images/$FSmobilimg' height='19px' class='hoverText' bt-xtitle='$title &ndash; {$Date}' />";
                    }
                    else
                    {
                        $field.="<div style='height:19px;'>&nbsp;</div>";
                    }
                    $field .="</td>";
                    $field .="</tr>";
                    $field .="<tr style=\"text-align:center;\">";
                    //Blue Ribbon Technician
                    $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
                    if (sizeof($cert) > 0 && array_key_exists(4, $cert))
                    {
                        $TBCCertInfo = $Core_Api_TechClass->getTechCertInfo(4,$row["TechID"]);
                        if(!empty($TBCCertInfo['date']))
                        {
                            $Date = split('-',$TBCCertInfo['date']);
                            $Datestr = ": ".$Date[1]."/".$Date[2]."/".$Date[0];
                        }
                        else
                        {
                            $Datestr="";
                        }

                        $field .= "<img src=\"/widgets/images/BRT_Tag.png\" height='19px' class=\"hoverText\" bt-xtitle=\"Blue Ribbon Technician$Datestr\" />";
                    }
                    else
                    { 
                        $field.="<div style='height:19px;'>&nbsp;</div>";
                    }
                    $field .="</td>";

                    //boot camp
                    $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
                    if (sizeof($cert) > 0 && array_key_exists(3, $cert))
                    {
                        $BootCampCertInfo = $Core_Api_TechClass->getTechCertInfo(3,$row["TechID"]);
                        if(!empty($BootCampCertInfo['date']))
                        {
                            $BootCampdate = split('-',$BootCampCertInfo['date']);
                            $BootCampdatestr = ": ".$BootCampdate[1]."/".$BootCampdate[2]."/".$BootCampdate[0];
                        }
                        else
                        {
                            $$TBCCertInfodatestr="";
                        }
                        $field .= "<img src=\"/widgets/images/Boot_Tag.png\" height='19px' class=\"hoverText\" bt-xtitle=\"Boot Camp Certified$BootCampdatestr\" />";
                    }
                    else if (sizeof($cert) > 0 && array_key_exists(43, $cert))
                    {
                        $BootCampplusCertInfo = $Core_Api_TechClass->getTechCertInfo(43,$row["TechID"]);
                        if(!empty($BootCampplusCertInfo['date']))
                        {
                            $BootCampplusdate = split('-',$BootCampplusCertInfo['date']);
                            $BootCampplusdatestr = ": ".$BootCampplusdate[1]."/".$BootCampplusdate[2]."/".$BootCampplusdate[0];
                        }
                        else
                        {
                            $BootCampplusdatestr="";
                        }

                        $field .= "<img src=\"/widgets/images/Boot_Tag_PLUS.png\" height='19px' class=\"hoverText\" bt-xtitle=\"Boot Camp Plus$BootCampplusdatestr\" />";
                    }
                    else
                    { 
                        $field.="<div style='height:19px;'>&nbsp;</div>";
                    }
                    $field .="</td>";
                    $field .="</tr>";
                    $field .="<tr style=\"text-align:center;\">";
                    //I9
                    $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
                    $extsUSAuth = $Core_Api_TechClass->getExts($techID);
                    $i9img="&nbsp;";
                    if ($extsUSAuth["I9Status"] == 2)
                    {
                        $title="Authorized to perform services in the United States";
                        //$Date = $extsUSAuth['I9Date'];
                        $Date = strtotime($extsUSAuth['I9Date']);
                        $Date = date("m/d/Y", $Date);
                        $i9img="VerifiedTall2.png";
                    }
                    else if($extsUSAuth["I9Status"] == 1)
                    {
                        $title="Authorization to perform services in the United States is pending";
                        $Date = "";
                        $i9img="PendingTall2.png";
                    }
                    
                    if($i9img!="&nbsp;")
                    {
                        $field .="<img src='/widgets/images/$i9img' height='19px' class='hoverText' bt-xtitle='$title &ndash; $Date' />";
                    }
                    else
                    {
                        $field.="<div style='height:19px;'>&nbsp;</div>";
                    }
                    $field .="</td>";

                    //ISO
                    $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
                    if($row["ISO"]=="Yes")
                    {
                        $field .="<img src='/widgets/images/iso-2.png' height='19px' class='hoverText' bt-xtitle='This technician is a member of an Independent Service Organization &#40;ISO&#41;' />";
                    }
                    else
                    {
                        $field.="<div style='height:19px;'>&nbsp;</div>";
                    }
                    $field .="</td>";
                    $field .="</tr>";
                    $field .="</table>";
                    break;
                case "FSExpert":
                    //FS-Experts 
                    $apiFSExpert = new Core_Api_FSExpertClass();
                    $FSExperts = $apiFSExpert->getFSExpertListForTech($row["TechID"]);

                    $field ="<table cellspacing='0' cellpadding='0' width='220px'>";
                    $field .="<colgroup>";
                    $field .="<col width=\"20%\">";
                    $field .="<col width=\"20%\">";
                    $field .="<col width=\"20%\">";
                    $field .="<col width=\"20%\">";
                    $field .="<col width=\"20%\">";
                    $field .="</colgroup>";
                    $k=1;
                    for ($i = 0; $i < 4; $i++)
                    {
                        $field .="<tr>";
                        for ($j = 0; $j < 6; $j++)
                        {
                            if($j==5)
                            {
                                $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD; display:none;'>";
                            }
                            else
                            {
                                $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
                            }
                            if (sizeof($FSExperts) > 0 && array_key_exists($k, $FSExperts))
                            {
                                if (!empty($FSExperts[$k]["Title"]))
                                {
                                    $title = "FS-Expert: <b>".$FSExperts[$k]["Title"]."</b><div style=\"padding-left:15px;padding-top:10px;\"><ul><li style=\"list-style:disc outside none;\">".$FSExperts[$k]["Content"]."</li></ul></div>";
                                }
                                else
                                {
                                    $title = "" ;
                                }
                                $field .="<img src='/widgets/images/expert/".$FSExperts[$k]["Abbreviation"].".png' height='19px' k='$k' class='hoverTextnew'  bt-xtitle='$title' />";
                            } else
                            {
                                $field.="<div style='height:19px;width:30px;'>&nbsp;</div>";
                            }
                            $field .= "</td>";
                            $k++;
                        }
                        $field .= "</tr>";
                    }
                    $field .="</table>";
                    break;
                default:
                    if (isset($row[$mapping]))
                        $field = $row[$mapping];
                    else
                        $field = '';
            }
            //end 735
            //$resultsHtml .= "<td>$field</td>";
            $resultsHtml .= "<td style='$style'>$field</td>";
        }
        $resultsHtml .= "</tr>";
    }

    $resultsHtml .= "</tbody>";

    // construct results page footer
    $prevPageBtn = ($currentResultPage > 1 ? "<span id=\"prevPageBtn\" onclick=\"prevPage()\">&lt;</span>" : "");
    $numPages = ceil($numResult / $resultsPerPage);
    $nextPageBtn = ($currentResultPage < $numPages ? "<span id=\"nextPageBtn\" onclick=\"nextPage()\">&gt;</span>" : "");
    $pageSelect = "<select id='pageSelect' name='pageSelect' style='width: 5em; text-align: center' onChange='gotoPage(this.selectedIndex + 1);'>";
    for ($i = 1; $i <= $numPages; $i++) {
        $start = 1 + (($i - 1) * $resultsPerPage);
        $pageSelect .= "<option value='$i'" . ($currentResultPage == $i ? " selected='selected' " : "") . ">" . $start . " - " . min($start + $resultsPerPage, $numResult) . "</option>";
    }
    $pageSelect .= "</select>";
    $resultsHtml .= "<tfoot><tr><td colspan=\"" . sizeof($resultColumnMap) . "\">$prevPageBtn Records " . $pageSelect . " of $numResult $nextPageBtn</td></tr></tfoot></table>";
} else {
    $resultsHtml = "<div class=\"noResultsDiv\">There are currently no applicants for this Work Order.</div>";
}

if (isset($_GET["sb"]) || isset($_GET["snp"]) || isset($_GET["sort"]) || isset($_GET["order"]) || isset($_GET["page"])) {
    // parse querystring
    $arr = Array();
    $ignoreGet = array("sort" => 1, "order" => 1, "page" => 1);
    foreach ($_GET as $key => $val) {
        if (array_key_exists($key, $ignoreGet))
            continue;
        if (is_array ($val)) {
	        $arr[] = ($key . "=[" . implode (",", $val) . "]");
        }
        else {
	        $arr[] = "$key=$val";
        }
    }
    $qs = implode("&", $arr);
}
else
    $qs = $_SERVER['QUERY_STRING'];
?>

<style type="text/css">
    #emailBlast {
        text-align: center;
    }
    .verdana2 {
        font-family:Verdana, Arial, Helvetica, sans-serif;
        font-size: small;
        clear: both;
    }

    .verdana2bold {
        font-family:Verdana, Arial, Helvetica, sans-serif;
        font-size: small;
        font-weight: bold;
        clear: both;
    }

    .fieldGroup {
        padding-bottom: 5px;
        border-bottom: 2px solid #2A497D;
    }

    .formBox, .formBoxFooter {
        margin: 15px auto 0px auto;
        padding: 15px;
        width: 920px;
        color: #2A497D;
        font-size: 12px;
        border: 2px solid #2A497D;
    }

    .formBox input {
        font-size: 12px;
        height: 14px;
    }

    .formBoxFooter {
        background-color: #BACBDF;
        text-align: center;
        margin: 0px auto 10px auto;
        padding: 5px 15px;
        border-top: none;
    }
    .formRow3, .formRow4, .formRow5, .formRow6 {
        width: 100%;
        clear: both;
    }
    .formRow3 div {
        width: 33%;
        float: left;
        padding: 0px;
        margin: 0px 0px 5px 0px;
    }

    .formRow4 div {
        width: 25%;
        float: left;
        padding: 0px;
        margin: 0px 0px 5px 0px;
    }

    .formRow5 div {
        width: 20%;
        float: left;
        margin-bottom: 5px;
    }

    .formRow6 div {
        width: 16%;
        float: left;
        margin-bottom: 5px;
    }

    #searchControls {
        text-align:right;
    }

    .resultsTable {
        /*        width: 80%;*/
        margin: 1px auto 0px auto;
        border: 2px solid #032D5F;
    }

    .resultsTable thead {
        text-align: center;
        color: #FFF;
        border: 1px solid #2A497D;
        background-color: #5091CB;
        cursor: default;
    }

    .bidComments {
        min-width: 50px;
    }

    .resultsTable thead td {
        padding: 3px 3px;
    }

    .sortAble, #nextPageBtn, #prevPageBtn {
        cursor: pointer;
    }

    .sortAble a, .sortAbleSelected a, .nonSortAble a {
        text-decoration: none;
        color: #fff;
    }

    .sortAble a:hover, .sortAbleSelected a:hover {
        text-decoration: underline;
        color: #00f;
    }

    .sortAbleSelected {
        cursor: pointer;
        background-color: #FF9900;
        color: #FFFFFF;
        font-weight: bold;
    }

    .resultsTable tfoot {
        text-align: center;
        color: #000;
        border: 1px solid #2A497D;
        background-color: #5091CB;
    }

    .resultsTable tfoot td {
        padding: 7px 10px;
    }

    .resultsTable td {
        padding: 3px 5px;
    }

    .resultsTable .evenRow {
        background-color: #E9EEF8;
    }

    .resultsTable .oddRow {
        background-color: #FBFCFD;
    }

    .resultsTable .preferred {
        background-color: #FFFF8C;
    }

    .searchControls {
        margin-top: 10px;
        text-align: center;
    }

    .noResultsDiv {
        margin-top: 10px;
        text-align: center;
    }

    .agreedTechPayBox {
        text-align: right;
    }

    hr {
        height: 1px;
    }
</style>

<script type="text/javascript" src="/widgets/js/fancybox/jquery.easing.pack.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-mod.js"></script>
<link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" media="all" />
<script type="text/javascript">
    var qs = "<?= $qs ?>";
    var myName = "<?php echo $_SERVER['PHP_SELF'] ?>";
    var currentResultPage = "<?= $currentResultPage ?>";
    var currSort = "<?= $sort ?>";
    var currOrder = "<?= $order ?>";
    var closeTimeout = null;
    var closeOK = false;

  
    
    function checkCloseOK() {
        if (closeOK) {
            window.close();
        }
        else
            queueCheckCloseOK();
    }
    
    function clearCheckCloseOK() {
        if (closeTimeout != null) {
            try {
                clearTimeout(closeTimeout);
            } catch (e) {}
        }
        closeTimeout = null;
    }
    
    function queueCheckCloseOK() {
        clearCheckCloseOK();
        closeTimeout = setTimeout("checkCloseOK()", 500);
    }
    
    function assignClicked() {
        assignBtnId = $(this).attr("id");
        bidID = assignBtnId.split("assignBtn", 2);
        bidID = bidID[1];
        techID = $("#techID" + bidID).val();
        agreed = $("#agreedTechPay" + bidID).val();
        comments = $("#bidComments" + bidID).val();
        $("#assignedMsg").html("Assigning Win #<?= $_GET["WorkOrderID"] ?> to Tech ID:" + techID + " at $" + agreed + " ...").css("color", "#000000").show();
        closeOK = false;
        //        queueCheckCloseOK();
        $.ajax({
            type: "POST",
            url: "ajax/directAssign.php",
            data: {
                winNum: "<?= $winNum ?>",
                companyId: "<?= $companyID ?>",
                techId: techID,
                agreed: agreed,
                bidComments: comments
            },
			error: function (x, y, z) {
				//console.log (x, y, z);
			},
            success: function (data) {
                if(window.parent.location.pathname == "/clients/techs.php"){
                    window.parent.assignApplicantFromBidders();
                    window.close();
                }else{
	                try {
	                    eval("result = " + data);
	                    if (result["success"] == 0) {
	                        $("#assignedMsg").html(result["errors"][0]).css("color", "#990000").show();
	                        closeOK = false;
	                        //                        clearCheckCloseOK();
	                    }
	                    else {
	                        $("#assignedMsg").html("Work order was successfully assigned.").css("color", "#009900").show();
	                        closeOK = true;
	                        try {
	                            try {
	                                // viewing bids from details page
	<?php if (!empty($_SESSION['BID_BACK_URL'])) : ?>
	                                    opener.opener.location = '<?= $_SESSION['BID_BACK_URL']; ?>';
	<?php else : ?>
	                                    opener.opener.location.reload();
	<?php endif; ?>
	                                opener.close();
	                            } catch (e) {
	<?php if (!empty($_SESSION['BID_BACK_URL'])) : ?>
	                                    opener.location = '<?= $_SESSION['BID_BACK_URL']; ?>';
	<?php else : ?>
	                                    opener.location.reload();
	<?php endif; ?>
	                            }
	                        } catch (e) {}
	                        window.close();
	                        try {
	                            parent.location.reload();
	                        } catch (e) {}
	                    }
	                } catch (e) {}
                }
            }
        });
    }
    
    function agreedTechPayFocus() {
        if ($(this).val() == "0.00") $(this).attr("value", "");
    }

    function agreedTechPayChange() {
        $(this).attr("value", massageMoney($(this).val()));
        agreedTechPayBlur();
    }

    function agreedTechPayBlur() {
        //        if ($(this).val() == "") $(this).attr("value", "");
    }
    
    function BidSchedule_Click()
    {
        if(typeof(window.parent) != 'undefined')
        {
            schedule = new window.parent.FSTechSchedule({width: 840, height: 450});
        }
        else
        {
            schedule = new FSTechSchedule({width: 840, height: 450});
        }
        var techid= jQuery(this).attr("techid");
        schedule.show($(this), null, {
            data:{tech_ID:techid,
                date:'',
                date_interval:'all',
                display_calendar:1,
                display_date:1,
                sort:'starttime',
                sort_dir:'desc'}
        });
    }

    $(document).ready(function() {
	try {
		b = opener;
		if (!b) {
			$("#showInPopup").show();

			$("#showInPopup").click(function() {
				parent.$("<div></div>").fancybox(
            							{
                                                                        'autoDimensions' : false,
                                                                        'width' : 1300,
                                                                        'height' : 600,
                                                                        'transitionIn': 'none',
                                                                        'transitionOut' : 'none',
                                                                        'type': 'iframe',
                                                                        'href': "<?= $_SERVER['PHP_SELF'] ?>?<?=$_SERVER['QUERY_STRING']?>&showback=1"
                                                                }
                                                        ).trigger('click');
			});
		}
	} catch (e) {
	}
        w = $("#appTable").width();

        $(".BidSchedule").unbind('click', BidSchedule_Click).bind('click', BidSchedule_Click);
        $(".breaklineFooter").css('display', 'none');
        jQuery(".hoverText").bt({
                                            titleSelector: "attr('bt-xtitle')",
                                            fill: 'white',
                                            cssStyles: {color: 'black', fontWeight: 'bold', width: 'auto'},
                                            animate:true
                                    });
        //735
        jQuery(".hoverTextnew").bt({
            titleSelector: "attr('bt-xtitle')",
            fill: 'white',
            cssStyles: {color: 'black', width: '300px'},
            animate:true
        });
        $("#expertinfocontrols").unbind('click').bind('click', function(){
   var content = "<div id='comtantPopupID'><div style='text-align:center;height:380px;width:900px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
        $("<div></div>").fancybox(
        {
            'showCloseButton' :true,
            'width' : 800,
            'height' : "auto",
            'content': content
        }
        ).trigger('click');

        $.ajax({
        type: "POST",
        url: "/widgets/dashboard/popup/expert-list",
        data: "",
        success:function( html ) {
            var content = html;
            $("#comtantPopupID").html(content);
        }
    });
});
        //end 735
        if (w < 50) w = 500;
        $("#appHeadTable").width(w + 5);
        $(".assignBtn").click(assignClicked);
        $(".agreedTechPayBox").focus(agreedTechPayFocus);
        $(".agreedTechPayBox").change(agreedTechPayChange);
        $(".agreedTechPayBox").blur(agreedTechPayBlur);

        var ref = document.referrer.split('?')[0];
        if (ref = 'wosDetails.php'){
            $('body').css('width', '800px');
            $('body').css('margin', '0');
            $('#headerSlider').css('display', 'none');
            $('#maintab').css('display', 'none');
            $('#subSectionLabel').css('display', 'none');
            $('#PMCompany').css('display', 'none');
            $('table.form_table').css('margin', '0');
            $('table.form_table').css('padding', '0');
            $('table.form_table').css('width', '400px');
            $('div.footer_menu').css('display','none');
            $('#content').css('margin', '0 0');
            $('#appHeadTable').css('width', '800px');
            $('#appTable').css('width', '800px');
        }
        if(typeof(window.parent) != 'undefined')
        {
//            window.parent._global.autoResize(null,jQuery('body').height());
        }
    });

    try {
        currentResultPage = parseInt(currentResultPage, 10);
    }
    catch (e) {}

    /*    function viewSchedule(url)
{
newwindow=window.open(url,'name','height=500, width=500, left=100, top=100, resizable=yes, scrollbars=yes, toolbar=no,status=no');
if (window.focus) {newwindow.focus()}
}

function techRatings(url)
{
newwindow=window.open(url,'name','height=500, width=650, left=100, top=100, resizable=yes, scrollbars=yes, toolbar=no,status=no');
if (window.focus) {newwindow.focus()}
}*/


    function updateSearchUrl() {
        setting = (currSort != "" ? "sort=" + currSort + "&order=" + currOrder : "");
        return myName + "?" + setting + "&" + qs;
    }

    function updateSearch() {
        frm = document.getElementById("searchForm");
        hideBanned = document.getElementById("hideBanned").checked;
        preferredOnly = document.getElementById("preferredOnly").checked;
        if (!hideBanned  || !preferredOnly) {
            settings = (hideBanned ? "" : "&sb=1");
            settings = settings + (preferredOnly ? "" : "&snp=1");
            //            alert(updateSearchUrl() + settings);
            frm.action = updateSearchUrl() + settings;
        }
        else
            frm.action = updateSearchUrl();
    }

    /*    function newSearch() {
frm = document.getElementById("searchForm");
document.location.href = myName + "?newSearch=1&" + qs;
}*/
    //735
    function sortBy(column) {
        if (column == "<?= $sort ?>")
        {
            order = (<?= $order ?> == 0 ? 1 : 0);
        }
        else
        {
            if(column =="PublicCredentials" || column =="ClientCredentials" || column =="FSExpert")
            {
                order = 1;
            }
            else
            {
                order = 0;
            }
        }
        document.location.href = myName = "?sort=" + column + "&order=" + order + "&" + qs;
    }
    //end 735
    function gotoPage(num) {
        document.location.href = updateSearchUrl() + "&page=" + num;
    }

    function prevPage() {
        gotoPage(currentResultPage - 1);
    }

    function nextPage() {
        gotoPage(currentResultPage + 1);
    }

    function remove_tech(id) {
        window.open("removeApplicant.php?id=" + id + "&v=<?php echo $_GET["v"] ?>", "mywindow", "status=1, width=650, height=350, scrollbars=1, resizable=1");
    }    

    function assignTech(woID, techID, bid) {
        try {
            switch (opener.pageName) {
                case "wosDetails":
                    opener.location = "wosDetails.php?v=<?php echo $_GET["v"] ?>&id=" + woID + "&assignTech=" + techID + "&assignBid=" + bid + "&referer=" + encodeURIComponent(opener.httpReferer);
                    break;
                case "wosDetailsNonTab":
                    opener.location = "wosDetailsNonTab.php?v=<?php echo $_GET["v"] ?>&id=" + woID + "&assignTech=" + techID + "&assignBid=" + bid + "&referer=" + encodeURIComponent(opener.httpReferer);
                    break;
                case "wosView":
                    opener.location = "wosDetailsNonTab.php?v=<?php echo $_GET["v"] ?>&id=" + woID + "&assignTech=" + techID + "&assignBid=" + bid + "&referer=" + encodeURIComponent(opener.location);
                    break;
                default:
                    opener.location = "wosDetails.php?v=<?php echo $_GET["v"] ?>&id=" + woID + "&assignTech=" + techID + "&assignBid=" + bid + "&referer=" + encodeURIComponent(opener.location);
                    break;
                }
                opener.focus();
                window.close();
            }
            catch (e) {
            }
        }

        function viewSchedule(url) {
            newwindow=window.open(url,'name','height=500, width=500, left=100, top=100, resizable=yes, scrollbars=yes, toolbar=no,status=no');

            if (window.focus) {newwindow.focus()}
        }

        function changeFlsOnly() {
            showFlsOnly = $("#flsOnly").attr("checked");
            document.location.href = "<?= $_SERVER['PHP_SELF'] ?>?sb=<?= $showBanned ? 1 : 0 ?>&v=<?= $_GET["v"] ?>&showFlsOnly=" + (showFlsOnly ? 1 : 0) + "&WorkOrderID=<?= $winNum ?>";
        }

	function openTechProfile (techid, company) {
		if (window == window.top) {
			window.location = "wosViewTechProfileSimple.php?TechID=" + techid + "&v=" + company + "&simple=1&back=1";
		}
		else {
			parent.$("<div></div>").fancybox({
				"autoDimensions": false,
				"width": 1015,
				"height": "90%",
				"transitionIn": "none",
				"transitionOut": "none",
				"type": "iframe",
				"href": "wosViewTechProfileSimple.php?TechID=" + techid + "&v=" + company + "&simple=1&back=1&popup=1"
			}).trigger("click");
		}
	}
        jQuery(window).ready(function(){
            _global.showback = '<?= (isset ($_REQUEST["showback"]) ? $_REQUEST["showback"] : "0") ?>';
           _global.eventOpenProfileTech(); 
           $("#fancybox-wrap").css('display','none');
        });
        
</script>
<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
<script type="text/javascript" src="/widgets/js/FSTechSchedule.js"></script>
<script type="text/javascript" src="/widgets/js/functions.js"></script>
<script type="text/javascript" src="/widgets/js/base64.js"></script>
<script type="text/javascript" src="../library/techCallStats_SAT.js"></script>
<script type="text/javascript" src="../library/jquery/jquery.bt.min.js"></script>
<link href="../library/ratingSourceBreakdown.css" rel="stylesheet" />
<!--735-->
<style type="text/css">
.thin_white_hr {
    /* border: 0px; */
    height: 1px;
        color: white;
    background: none repeat scroll 0 0 white;
}
</style>
<!--735-->
<div id="content">
    <div id="assignedMsg" style="display:none"></div>
         <?php if(!isset ($_GET["backNotices"]) && !empty($_GET["backNotices"])){ ?>
        <input value="Back" class="link_button_popup" type="button" onclick="<?= base64_decode($_GET['backclick']) ?>" />
        <?php } ?>
         <?php if(empty($_GET["simple"])){ ?>
	<div><a href="javascript:void(0)" id="showInPopup" style="display: none">Show Applicants in a pop-up</a></div>
        <?php } ?>

    <table id="appHeadTable" style="width: 90%">
        <tr>
            <td><span style="font-weight: bold">WIN#</span> <?= $winNum ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-weight: bold">Client Work Order #</span> <?= $WO_ID ?></td>
            <td id="searchControls"><span id="flsOnlyControl" style="<?= $companyID == "FLS" ? "" : "display:none" ?>" ><input type="checkbox" id="flsOnly" name="flsOnly" <?= $showFlsOnly ? "checked=\"checked\"" : "" ?> onclick="changeFlsOnly()" /> FLS Tech Only</span> 
                <a href="<?= $_SERVER['PHP_SELF'] ?>?backNotices=<?= isset ($_GET["backNotices"]) ? $_GET["backNotices"] : 0 ?>&backclick=<?=isset ($_GET["backclick"]) ? $_GET["backclick"] : ""?>&sb=<?= $showBanned ? 0 : 1 ?>&v=<?= $_GET["v"] ?><?= $companyID == "FLS" ? "&showFlsOnly=" . ($showFlsOnly ? 1 : 0) : "" ?>&WorkOrderID=<?= $winNum ?>&projects=<?= isset ($_GET["projects"]) ? $_GET['projects'] : ""?>"><?= $showBanned ? "Hide Admin Denied Techs and Removed Bids" : "Show All Applicants" ?></a></td>
        </tr>
    </table>
    <div>
<?= $resultsHtml ?>
    </div>
</div>

<?php
//require_once("../footer.php");
?>
