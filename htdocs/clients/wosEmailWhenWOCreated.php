<?php
// Created on: 6/24/2008 
// Author: JC
// Description: When a work order is created, send an email blast to techs in a 50 mile radius

//ini_set("display_errors", "stdout");

require_once("../library/caspioAPI.php");
require_once("../library/smtpMail.php");
require_once("../library/contactSettings.php");

/*

6/24/2008:

DONE: Send email to all techs in 50 mile radius with WO details
TODO: Uncomment $TB_UNID line below BEFORE DEPLOYING!
TODO: Comment out any "echo" statements
TODO: Uncomment Javascript close statement at bottom

*/

$TB_UNID = $_GET['TB_UNID'];

//$TB_UNID = 7036;


// FUNCTIONS

// FUNCTION process
// 
// for use when processing an array returned by caspioSelectAdv.
// trims the quotes off and filters "NULL" into ""
//
function process($str) {
	$output = trim($str, "`");
	switch ($str) {
		case "NULL":
			$output = "";
			break;
		case "True":
		case "Yes":
			$output = "Y";
			break;
		case "False":
		case "No":
			$output = "N";
			break;
	}
	return $output;
}


// FUNCTION queryTechs(fields, conditions, order)
//
// queries TR_Master_List for data

function queryTechs($fields, $conditions, $order="TechID ASC", $zipcode="", $distance="1") {
	if (false) {
		echo "<p>QUERY: caspioSelectAdv(\"TR_Master_List\", \"$fields\", \"$conditions\", \"$order\", ...)</p>";
	}
	switch ($distance) {
		case "1":
			$result = caspioSelectAdv("TR_Master_List", $fields, $conditions, $order, FALSE, "`", "|");
			break;
		default:
			$result = proximityTechSearch($zipcode, $distance, $fields, " and $conditions", $order);			
	}
	return $result;
}

function queryTechsFromList($fields, $list, $order="TechID ASC") {
	$output = array();
	if (sizeof($list) != 0) {
		$list_string = format_list($list);
		$output = queryTechs($fields, "TechID in ($list_string)", $order);
	}
	return $output;
}


// function proximityTechSearch
//
// returns list of results that meet search criteria including
// proximity search
//
function proximityTechSearch($zipcode, $distance, $fields, $conditions, $order="TechID ASC") {
	$result = array();
	
	$zipcode = caspioEscape($zipcode);
	$distance = caspioEscape($distance);
	
	$center = caspioSelectAdv("USA_ZipCodes", "TOP 1 Latitude, Longitude", "ZIPCode = '$zipcode'", "", false, "`", "|", false);

	if (sizeof($center) == 1 && $center[0] != "") {
		// valid zip, do quick proximity search
		$center = explode("|", $center[0]);
		$latitude = $center[0];
		$longitude = $center[1];
		
		$result = caspioProximitySearchByReferenceRaw("TR_Master_List", false, $latitude, $longitude, "Zipcode", "USA_ZipCodes", false, "ZIPCode", "Latitude", "Longitude", "<= $distance", "3", $fields, $conditions, "", false, "|", "`", false);
	}	
	return $result;
}

// END FUNCTIONS


// GET INFO ABOUT WORK ORDER

if ($TB_UNID != "")
{

	// Get Work Order
	$woInfo = caspioSelectAdv("Work_Orders", "ISNULL(Description, ''), ISNULL(City, ''), ISNULL(State, ''), ISNULL(Zipcode, ''), ISNULL(CONVERT(VARCHAR, StartDate, 101), ''), ISNULL(StartTime, ''), ISNULL(EndTime, ''), ISNULL(PayMax, ''), ISNULL(SpecialInstructions, ''), Amount_Per, Qty_Devices, ISNULL(StartRange, ''), ISNULL(Duration, ''), ISNULL(Project_ID, '')", "TB_UNID = $TB_UNID", "TB_UNID ASC", false, "`", "|");
	
	if (sizeof($woInfo) == 1 && $woInfo[0] != "") {
			$info = explode("|", $woInfo[0]);
			$description = process($info[0]);
			$city = process($info[1]);
			$state = process($info[2]);
			$zipCode = process($info[3]);
			$startDate = process($info[4]);
			$startTime = process($info[5]);
			$endTime = process($info[6]);
			$payMax = process($info[7]);
			$specialInstructions = process($info[8]);
			$amountPer = process($info[9]);
			$qtyDevices = process($info[10]);
			$startRange = process($info[11]);
			$duration = process($info[12]);
			$projectID = process($info[13]);
	}
	
	//echo "<p>ProjectID: $projectID</p>";
		
		
	
	// Get Project
	$woInfo = caspioSelectAdv("TR_Client_Projects", "From_Email, Project_Manager, Client_Email, Client_Name", "Project_ID = '$projectID'", "Project_ID ASC", false, "`", "|");
	
	if (sizeof($woInfo) == 1 && $woInfo[0] != "") {
			$info = explode("|", $woInfo[0]);
			$vFromEmail = process($info[0]);
			$vFromName = process($info[1]);
			$clientEmail = process($info[2]);
			$clientName = process($info[3]);
	}
	
	// GET ARRAY OF TECHS WITHIN 50 MILES OF WORK ORDER
	
	$techsResult = proximityTechSearch($zipCode, "50", "PrimaryEmail", "");
	
	if (sizeof($techsResult) > 0 && $techsResult[0] != "") {
		$techsResult = array_unique($techsResult);
		$eList = implode(", ", $techsResult);
		$eList = str_replace("`", "", $eList);
		$eList = str_replace(", ,", ", ", $eList);
		
		// vince debugging
        $contact = new ContactSettings();
        $doNotEmail = $contact->getEmailList();
        $eList = $contact->filterEmail($eList);
		
		echo "<p>EMAILS: " . $eList . "</p>";
	
		// Send Email to the Tech
		if ($clientEmail != "") {			
			//$eList = "progressions@gmail.com";
			
			$vSubject = "Work Available in Your Area";
			@$caller = "wosEmailWhenWOCreated";
			$message = "-----
	Description: 
						
	$description
	
	Special Instructions:
	
	$specialInstructions
						
	Location: $city, $state, $zipCode
	Date: $startDate
	Start Time: $startTime 
	End Time: $endTime
	Est On Site Arrival Time: $startRange
	Duration: $duration
	Required: Ability to lift 75lbs, Cell Phone
	Tools: PC Tool Kit
	Pay: $payMax PER $amountPer $qtyDevices
	";
			
			$message .= "\n\nHave you visited the new Tech Community website? Click <a href='http://groups.google.com/group/mytechnicianspace/'>here</a> for fun and information on the tech community";
				
			$message .= "\n\nYou are receiving this email as a registered technician on www.fieldsolutions.com.\nClick <a href='http://www.fieldsolutions.com/unsubscribe/'>here</a> to stop receiving future e-mails from us.";
			
			//echo "<p>MESSAGE:</p>";
			//echo $message;
					
			$htmlmessage = nl2br($message);
			
			// Emails Tech
			smtpMail($vFromName, $vFromEmail, $eList, $vSubject, $message, $htmlmessage, $caller);
		}
	
	}



}


?>

<SCRIPT LANGUAGE="JavaScript"> 

	setTimeout('close()',100); 

</SCRIPT>