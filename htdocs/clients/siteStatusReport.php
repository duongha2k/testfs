<?php $page = "clients"; ?> 
<?php $option = "reports"; ?>
<?php $selected = 'MetricsReport'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php require ("includes/adminCheck.php"); ?>
<?php
$displayPMTools = true;

?>
<?php
$id = $_GET["v"];
?>
<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
<link rel="stylesheet" href="../../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript" src="../../library/js/dateDropDownOption.js"></script>
<script type="text/javascript" src="../../library/js/jquery.watermark.js"></script>
<?php
$displayPMTools = true;
//require_once("includes/PMCheck.php");
require_once("classActivityReport.php");
$companyID = $_GET["v"];
$Params=$_GET['Params'];

$Today = date("m/d/Y");
$Tomorrow  = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d")+1, date("Y")));

// This Week (Mon = 1 - Sun = 7)
$BOW = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d")-getDay(date("N"))+1, date("Y")));
$EOW = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d")-getDay(date("N"))+7, date("Y")));

// last Week (Mon = 1 - Sun = 7)
$BOLW = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d")-getDay(date("N"))-6, date("Y")));
$EOLW = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d")-getDay(date("N")), date("Y")));

// This Month
$BOM = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d")-date("d")+1, date("Y")));
$EOM = date("m/d/Y", mktime(0, 0, 0, date("m")+1, date("d")-date("d"), date("Y")));

// Last Month
$PrevBOM = date("m/d/Y", mktime(0, 0, 0, date("m")-1, date("d")-date("d")+1, date("Y")));
$PrevEOM = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d")-date("d"), date("Y")));

// Q1
$Q1Start = date("m/d/Y", mktime(0, 0, 0, 1, 1, date("Y")));
$Q1End = date("m/d/Y", mktime(0, 0, 0, 4, date("d")-date("d"), date("Y")));

// Q2
$Q2Start = date("m/d/Y", mktime(0, 0, 0, 4, 1, date("Y")));
$Q2End = date("m/d/Y", mktime(0, 0, 0, 7, date("d")-date("d"), date("Y")));

// Q3
$Q3Start = date("m/d/Y", mktime(0, 0, 0, 7, 1, date("Y")));
$Q3End = date("m/d/Y", mktime(0, 0, 0, 10, date("d")-date("d"), date("Y")));

// Q4
$Q4Start = date("m/d/Y", mktime(0, 0, 0, 10, 1, date("Y")));
$Q4End = date("m/d/Y", mktime(0, 0, 0, 13, date("d")-date("d"), date("Y")));

// H1
$H1Start = date("m/d/Y", mktime(0, 0, 0, 1, 1, date("Y")));
$H1End = date("m/d/Y", mktime(0, 0, 0, 7, date("d")-date("d"), date("Y")));

// H2
$H2Start = date("m/d/Y", mktime(0, 0, 0, 7, 1, date("Y")));
$H2End = date("m/d/Y", mktime(0, 0, 0, 13, date("d")-date("d"), date("Y")));

// YTD
$YTDStart = date("m/d/Y", mktime(0, 0, 0, 1, 1, date("Y")));
$YTDEnd = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d"), date("Y")));


?>
<style type="text/css">
div#loader {
    width:100px;
    height: 100px;
    background:url(/widgets/images/loader.gif) no-repeat center #fff;
    text-align:center;
    padding:10px;
    font:normal 16px Tahoma, Geneva, sans-serif;
}
</style>
<script type="text/javascript">
    var currYr = " <?= date("Y") ?>";
    var Today = <?php echo "\"" . $Today . "\""; ?>;
    var BOW = <?php echo "\"" . $BOW . "\""; ?>;
    var EOW = <?php echo "\"" . $EOW . "\""; ?>;
    var BOLW = <?php echo "\"" . $BOLW . "\""; ?>;
    var EOLW = <?php echo "\"" . $EOLW . "\""; ?>;
    var BOM = <?php echo "\"" . $BOM . "\""; ?>;
    var EOM = <?php echo "\"" . $EOM . "\""; ?>;
    var PrevBOM = <?php echo "\"" . $PrevBOM . "\""; ?>;
    var PrevEOM = <?php echo "\"" . $PrevEOM . "\""; ?>;
    var Q1Start = <?php echo "\"" . $Q1Start . "\""; ?>;
    var Q1End = <?php echo "\"" . $Q1End . "\""; ?>;
    var Q2Start = <?php echo "\"" . $Q2Start . "\""; ?>;
    var Q2End = <?php echo "\"" . $Q2End . "\""; ?>;
    var Q3Start = <?php echo "\"" . $Q3Start . "\""; ?>;
    var Q3End = <?php echo "\"" . $Q3End . "\""; ?>;
    var Q4Start = <?php echo "\"" . $Q4Start . "\""; ?>;
    var Q4End = <?php echo "\"" . $Q4End . "\""; ?>;
    var H1Start = <?php echo "\"" . $H1Start . "\""; ?>;
    var H1End = <?php echo "\"" . $H1End . "\""; ?>;
    var H2Start = <?php echo "\"" . $H2Start . "\""; ?>;
    var H2End = <?php echo "\"" . $H2End . "\""; ?>;
    var YTDStart = <?php echo "\"" . $YTDStart . "\""; ?>;
    var YTDEnd = <?php echo "\"" . $YTDEnd . "\""; ?>;   	
       
</script>
<!--- Add Content Here --->
<br /><br />
<!--896-->
<div align="center" style="color: #174065;"><strong>SLA Report</strong></div>
<!--end 896-->
<div align="center">
    <div>
        <iframe id="ifrmDownloadSiteStatus" name="ifrmDownloadSiteStatus" style="height: 0px;width:0px;" ></iframe>
        <form method="post" enctype="multipart/form-data" target="ifrmDownloadSiteStatus" id="frmDownload"  name="frmDownload"
              action="/widgets/dashboard/reports/download-site-status">
            <table>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        <!--871-->
                        Select Projects:
                        <!--end 871-->
                    </td>
                    <td style="padding-left: 10px;">
                        <!--857--> 
                        <div style="min-height:50px;max-height:160px;overflow-y: auto;overflow-x: hidden; width:380px;">
                        <?php      
                            $apiProjectClass = new Core_Api_ProjectClass();
                            $filter = array('Company_ID'=>$_GET["v"]);
                            $ArrayProjectList = $apiProjectClass->getAllProjects($_SESSION['Auth_User']['login'], $_SESSION['Auth_User']['password'],
                                                    null,$filter);  
                            $ArrayProjectList = $ArrayProjectList->data;
                            $numProject=count($ArrayProjectList);
                            for($i=0;$i<$numProject;$i++)
                            {
                                echo '<div style="padding-bottom: 3px;">';
                                echo'<input type="checkbox" id="chk'.$ArrayProjectList[$i]->Project_ID.'" value="'.$ArrayProjectList[$i]->Project_ID.'"  name="Project_ID[]" class="classProjectItems">';
                                echo'&nbsp;<label for="chk'.$ArrayProjectList[$i]->Project_ID.'">'.$ArrayProjectList[$i]->Project_Name.'</label>';                                
                                echo '</div>';
                            }
                        ?>
                        <!--end 857-->
                    </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <?php
                    $DateRangeHtml = "<option value=\"showAll\" selected=\"selected\">Show All</option>";
                    $DateRangeHtml .= "<option value=\"showToday\">Today</option>";
                    $DateRangeHtml .= "<option value=\"showWeek\">This Week</option>";
                    $DateRangeHtml .= "<option value=\"showLastWeek\">Last Week</option>";
                    $DateRangeHtml .= "<option value=\"showMonth\">This Month</option>";
                    $DateRangeHtml .= "<option value=\"lastMonth\">Last Month</option>";
                    $DateRangeHtml .= "<option value=\"5\">Q1 " . date("Y") . "</option>";
                    $DateRangeHtml .= "<option value=\"6\">Q2 " . date("Y") . "</option>";
                    $DateRangeHtml .= "<option value=\"7\">Q3 " . date("Y") . "</option>";
                    $DateRangeHtml .= "<option value=\"8\">Q4 " . date("Y") . "</option>";
                    $DateRangeHtml .= "<option value=\"9\">H1 " . date("Y") . "</option>";
                    $DateRangeHtml .= "<option value=\"10\">H2 " . date("Y") . "</option>";
                    $DateRangeHtml .= "<option value=\"11\">YTD " . date("Y") . "</option>";
                    ?>
                    <td>Start Date</td>
                    <td style="padding-left: 10px;">
                        <select name="selectStartDate" id="selectStartDate" style="width:110px;">
                            <?= $DateRangeHtml ?>
                        </select>
                        &nbsp;&nbsp;From:&nbsp;
                        <input type="text" size="10" placeholder="MM/DD/YYYY" title="MM/DD/YYYY" class="CalendarInput inputwatermark" name="fromStartDate" id="fromStartDate"/>
                        &nbsp;&nbsp;To:&nbsp;
                        <input type="text" size="10" placeholder="MM/DD/YYYY" title="MM/DD/YYYY" class="CalendarInput inputwatermark" name="toStartDate"  id="toStartDate"/>
                    </td>
                </tr>
                <tr>
                    <td>Date Tech Marked Complete</td>
                    <td style="padding-left: 10px;">
                        <select name="selectMarkedComplete" id="selectMarkedComplete" style="width:110px;">
                            <?= $DateRangeHtml ?>
                        </select>
                        &nbsp;&nbsp;From:&nbsp;
                        <input type="text" size="10" placeholder="MM/DD/YYYY" title="MM/DD/YYYY" class="CalendarInput inputwatermark" name="fromMarkedComplete" id="fromMarkedComplete"/>
                        &nbsp;&nbsp;To:&nbsp;
                        <input type="text" size="10" placeholder="MM/DD/YYYY" title="MM/DD/YYYY" class="CalendarInput inputwatermark" name="toMarkedComplete"  id="toMarkedComplete"/>
                    </td>
                </tr>
                
                <tr>
                    <td>Date Approved</td>
                    <td style="padding-left: 10px;">
                        <select name="selectDateApproved" id="selectDateApproved" style="width:110px;">
                            <?= $DateRangeHtml ?>
                        </select>
                        &nbsp;&nbsp;From:&nbsp;
                        <input type="text" size="10" placeholder="MM/DD/YYYY" title="MM/DD/YYYY" class="CalendarInput inputwatermark" name="fromDateApproved" id="fromDateApproved"/>
                        &nbsp;&nbsp;To:&nbsp;
                        <input type="text" size="10" placeholder="MM/DD/YYYY" title="MM/DD/YYYY" class="CalendarInput inputwatermark" name="toDateApproved"  id="toDateApproved"/>
                    </td>
                </tr>
                
                <tr>
                    <td>Date Invoiced</td>
                    <td style="padding-left: 10px;">
                        <select name="selectDateInvoiced" id="selectDateInvoiced" style="width:110px;">
                            <?= $DateRangeHtml ?>
                        </select>
                        &nbsp;&nbsp;From:&nbsp;
                        <input type="text" size="10" placeholder="MM/DD/YYYY" title="MM/DD/YYYY" class="CalendarInput inputwatermark" name="fromDateInvoiced" id="fromDateInvoiced"/>
                        &nbsp;&nbsp;To:&nbsp;
                        <input type="text" size="10" placeholder="MM/DD/YYYY" title="MM/DD/YYYY" class="CalendarInput inputwatermark" name="toDateInvoiced"  id="toDateInvoiced"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td valign="top">
                        Select Report Type:
                    </td>
                    <td style="padding-left: 10px;">
                        <!--896-->
                        <div>
                            <div><input checked value="2" type='radio' name='downloadSiteStatus' />&nbsp;SLA Status Report</div>
                            <div><input value="3" type='radio' name='downloadSiteStatus' />&nbsp;Site Status Report</div>
                        </div>
                        <!--end 896-->
                    </td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        
                    </td>
                    <td>
                        <input type="button" onclick="javascript:download();" class="link_button download_button" value="Download">
                    </td>
                </tr>
            </table>
        </form>
    </div>



</div>
<script>
    jQuery(window).ready(function(){
        //detailObject.onInit.watermark();
        
        $('.CalendarInput').calendar({
            dateFormat:'MDY/'
        }); 
        $("#selectStartDate").change(function(){ 
            filterDate("#selectStartDate",'#fromStartDate','#toStartDate');
        })
        $("#selectMarkedComplete").change(function(){ 
            filterDate("#selectMarkedComplete",'#fromMarkedComplete','#toMarkedComplete');
        })
        $("#selectDateApproved").change(function(){ 
            filterDate("#selectDateApproved",'#fromDateApproved','#toDateApproved');
        })
        $("#selectDateInvoiced").change(function(){ 
            filterDate("#selectDateInvoiced",'#fromDateInvoiced','#toDateInvoiced');
        })
    });
    var loaderVisible = false;
    function download()
    {
        showLoader();
        document.forms["frmDownload"].submit();
        setTimeout('getstatus()', 2000);        
    }
    
    function getstatus(){
      $.ajax({
          url: "/widgets/dashboard/reports/getdownloadstatus",
          type: "POST",
          dataType: 'json',
          success: function(data) {
            if(data.status=="pending")
              setTimeout('getstatus()', 2000);
            else
              hideLoader();
          }
      });
    }
    function showLoader(){
        if(!loaderVisible){
            loaderVisible = true;
            var content = "<div id='loader'>Loading...</div>";
            $("<div></div>").fancybox(
                {
                    'autoDimensions' : true,
                    'showCloseButton' :false,
                    'hideOnOverlayClick' : false,
                    'content': content
                }
            ).trigger('click');
        }
    }
    
    function hideLoader(){
         setTimeout (jQuery.fancybox.close, 500);
          loaderVisible = false;
    }
</script>

<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
