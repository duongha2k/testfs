<?php
set_time_limit(300);
require_once("../headerStartSession.php");
require_once("../library/caspioAPI.php");
require_once("../library/projectTimeStamps.php");
require_once("../library/woACSTimeStamps.php");
$id = $_GET["id"];

require_once("{$_SERVER['DOCUMENT_ROOT']}/library/bootstrapAPI.php");
Core_Database_CaspioSync::sync(TABLE_CLIENT_PROJECTS, "Project_ID", array($id));

$user =isset($_SESSION['UserName'])?$_SESSION['UserName']:null;
if ($_SESSION['Auth_User']['isPM']) $user .= '|pmContext=' . $_SESSION['PM_Company_ID'];
$choice = 0;
$numUpdated = 0;
if (isset($_GET["choice"]))
    $choice = $_GET["choice"];
if (is_numeric($id) && is_numeric($choice)) {
    $projectInfo = createProjectTimeStamp($id, $user);
    if ($choice != 3) {
        // update ACS changes in project WOs
        $reminderAll = $projectInfo["reminderAll"];
        $reminderAcceptance = $projectInfo["reminderAcceptance"];
        $reminder24Hr = $projectInfo["reminder24Hr"];
        $reminder1Hr = $projectInfo["reminder1Hr"];
        $checkInCall = $projectInfo["checkInCall"];
        $checkOutCall = $projectInfo["checkOutCall"];
        $reminderNotMarkComplete = $projectInfo["reminderNotMarkComplete"];
        $reminderIncomplete = $projectInfo["reminderIncomplete"];
        $SMSBlast = $projectInfo["SMSBlast"];
        $applyTo = $projectInfo["applyTo"];
        $applyTo = trim($applyTo, "'");
        $applyTo = $applyTo == "DEFAULT" ? "" : $applyTo;
        if ($choice != $applyTo) die();

        $criteria = "Project_ID = '$id' AND TechMarkedComplete = '0' AND ACSInvoiced = '0' AND Approved = '0' AND Deactivated = '0'";

        if ($choice == 2)
            $criteria = "IFNULL(Tech_ID, '') = '' AND " . $criteria;

        $db = Zend_Registry::get('DB');
        $sql = "SELECT WIN_NUM FROM work_orders WHERE $criteria";
        $woList = $db->fetchAll($sql, array(), Zend_Db::FETCH_NUM);

        if (count($woList) > 0) {
            $fields = array("ReminderAll" => $reminderAll,
                            "ReminderAcceptance" => $reminderAcceptance,
                            "Reminder24Hr" => reminder24Hr,
                            "Reminder1Hr" => $reminder1Hr,
                            "CheckInCall" => $checkInCall,
                            "CheckOutCall" => $checkOutCall,
                            "ReminderNotMarkComplete" => $reminderNotMarkComplete,
                            "ReminderIncomplete" => $reminderIncomplete,
                            "SMSBlast" => $SMSBlast );
            $numUpdated = $db->update('work_orders', $fields, $criteria);

            foreach ($woList as $unid) {
                if ($unid == "") continue;
                createWOACSTimeStamp($unid);
            }
        }
    }
}
?>