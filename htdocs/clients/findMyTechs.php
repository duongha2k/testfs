<?php

    $includeStyles = array(
        '/library/ratingSourceBreakdown.css'
    );

    require ("../headerSimple.php");
    require_once("../library/caspioAPI.php");
    
    if (empty($_GET["v"])) {
        $_GET["v"] = '';
    }
    $companyID = (isset($_GET["v"]) ? caspioEscape($_GET["v"]) : "BLANKID");
    $params = $_REQUEST;
    
    $client = new Core_Api_Class();
    //14023
    $searchParams = array(
        'ProximityDistance' => $params['distance'],
        'WithCoordinates' => true,
        'hideBanned' => true,
        'hideBids' => 0,
        'HelpingHands' => 0
    );
    //735
    $skillcategory = "";
    if (!empty($params['zip'])) {
    
        // get zip coordinates
    	$geocode = new Core_Geocoding_Google();
    	$geocode->setZip($params['zip']);
    	$updatedFields['Latitude'] = $geocode->getLat();
    	$updatedFields['Longitude'] = $geocode->getLon();
    	
        $searchParams['ProximityZipCode'] = $params['zip'];
        $searchParams['ProximityLat'] = $geocode->getLat();
        $searchParams['ProximityLng'] = $geocode->getLon();
    	
    } else if (!empty($params['clientWO']) || !empty($params['win'])) {

        if (!empty($params['clientWO']) && empty($params['win']) ) {
            $res = $client->getWinNum(
                $_SESSION['UserName'].'|pmContext='.$params['v'],
                $_SESSION['Password'], 
                $params['clientWO']
            );
            if ($res->success && !empty($res->data[0])) {
                $params['win'] = $res->data[0]->WIN_NUM;
                $skillcategory = $res->data[0]->WO_Category_ID;
            }
        }
        
        if (!empty($params['win']) ) { 
            $wo = $client->getWorkOrder($_SESSION['UserName'].'|pmContext='.$params['v'],
            $_SESSION['Password'], $params['win']);
    
            if ($wo->success && !empty($wo->data[0])) {
                $searchParams['ProximityZipCode'] = $wo->data[0]->Zipcode;
                $searchParams['ProximityLat'] = $wo->data[0]->Latitude;
                $searchParams['ProximityLng'] = $wo->data[0]->Longitude;
                $skillcategory = $wo->data[0]->WO_Category_ID;
        	}
    	} 
    } 
    
    $Categoryname = "";
    if($skillcategory != "")
    {
        $apiexpert = new Core_Api_FSExpertClass();
        $FSExpertinfo = $apiexpert->getFSExpert_ByCatID($skillcategory);
        $Categoryname=$FSExpertinfo['Category'];
    }
    
    //end 735
    if (!isset($searchParams['ProximityLat']) || !isset($searchParams['ProximityLng']) || empty($searchParams['ProximityDistance']) ) {
        echo 'Location is not calculated. Enter correct Zip Code or WO.';
        exit;
    }
    //910
    try
    {
        // new search. get 5 results by the search paramaters.
        $new_resultMyRecent = $client->getTechs($_SESSION['UserName'].'|pmContext='.$params['v'], $_SESSION['Password'], $searchParams, "myRecent", NULL, 5);
        }
    catch( Exception $e)
    {

    }
        try
    {
        $new_resultMyPreferred = $client->getTechs($_SESSION['UserName'].'|pmContext='.$params['v'], $_SESSION['Password'], $searchParams, "myPreferred", NULL, 5);
        }
    catch( Exception $e)
    {

    }
        //735
        //FSExpertForCatID
    try
    {
        $new_resultRecent = $client->getTechs($_SESSION['UserName'].'|pmContext='.$params['v'], $_SESSION['Password'], $searchParams, "recent", NULL, 5);
    }
    catch( Exception $e)
    {

    }
    //end 910
    if($Categoryname!="")
    {
        $searchParams['FSExpertForCatID']=$skillcategory;
        $new_resultRecentExpert = $client->getTechs($_SESSION['UserName'].'|pmContext='.$params['v'], $_SESSION['Password'], $searchParams, "recent", NULL, 5);
        if(count($new_resultRecentExpert->data)>0)
        {
            $new_resultRecent=$new_resultRecentExpert;
        }
    }

    $view = array();
    $view['resultMyRecent'] = $new_resultMyRecent->data;
    $view['resultMyPrefered'] = $new_resultMyPreferred->data;
    $view['resultRecent'] = $new_resultRecent->data;
    $clientID = $_SESSION['ClientID'];
    $apiGPM = new  Core_Api_Class();
    $isGPM = $apiGPM -> isGPM($clientID);
    //943
    $user = new Core_Api_User();
    $clientInfo = $user->getRepClientforCompany($companyID);
    $CompanyName = $clientInfo[0]["CompanyName"];
    //end 943
?>

<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetWODetails.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
<script type="text/javascript" src="/widgets/js/functions.js"></script>
<script type="text/javascript" src="/widgets/js/FSTechSchedule.js"></script>
<script type="text/javascript" src="js/backButton.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.easing.pack.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-mod.js"></script>
<link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" media="all" />

<script type="text/javascript" src="/library/techCallStats_SAT.js"></script>

<script type="text/javascript">
    var schedule = new FSTechSchedule({width: 940, height: 450});
</script>

<style type="text/css">
.thin_white_hr {
    /* border: 0px; */
    height: 1px;
        color: white;
    background: none repeat scroll 0 0 white;
}

.thin_hr {
    border: 0px;
    height: 1px;
}

.sortable{
    font-weight: normal !important;
        background: #5091CB;
        color: white;
}
</style>

<script type="text/javascript" src="/widgets/js/jquery.tinysort.js"></script>

<!-- Main content -->
<!--<div class="inner10 indentTop" id="content">-->
<? if (!in_array($params['c'], array( "detailsContainer", "projectSearch"))){ ?>
<div class="inner10" id="content">
<? } else { ?>
<div class="inner10" style="margin-top: -10px;" id="content">
<? } ?>

<? //if ($params['c'] != "projectSearch"){ ?>
<? //if (in_array($params['c'], array( "detailsContainer", "projectSearch"))){ ?>
<? if (0){ ?>
<div style="padding-bottom: 5px; float: left; margin-top: 0px;"><div class="link_button" style="text-align: center;" onclick="goBack();">Go Back</div></div>
<? } ?>

<table width="100%" class="gradBox_table_inner" id="sortTable" style="border-left: 1px solid #CEE0EC">
    <colgroup>
        <!--910-->
        <col width="50px">
        <!--end 910-->
        <col width="70px">
        <col width="*">
        <col width="*">
        <col width="*">
        <col width="*">
        <col width="*">
        <col width="*">
        <col width="<?=$isGPM?"111px":"74px"?>">
        <col width="74px">
        <col width="182px">
    </colgroup>
<thead>
    <!--735-->
    <tr class="table_head" id="sort-line">
        <th><nobr>Snap<br />Shot</nobr></th>
        <th class="sortable" special="" >FS-Tech<br/>ID#</th>
        <th class="sortable" special="" >Name<hr class="thin_white_hr">Contact Information</th>
        <th class="sortable" special="" >City, State<br/>Zip</th>
        <th class="sortable" special="" >Miles</th>
        <th class="sortable" special="" >Preference<hr class="thin_white_hr">Performance</th>
        <th class="sortable" special="" ><nobr>Work<br/>Orders</nobr></th>
        <th class="sortable" special="" ><nobr>No Shows<hr class="thin_white_hr">Back Outs</nobr></th>
        <th class="sortable" special=""  style="display:none;"></th>
        <th class="sortable" special=""  style="display:none;"></th>
        <th class="sortable" special=""  style="display:none;"></th>
        <th class="sortable" special="ClientCredentials" >Client<br/>Credentials</th>
        <th class="sortable" special="PublicCredentials" >Public<br/>Credentials</th>
        <th class="sortable" special="fsexpert" ><a href="javascript:;" id="fsexpert">FS&ndash;Experts&#8482; &ndash; Skill Categories</a><span><a id="expertinfocontrols" href="javascript:void(0)"><img style="vertical-align: middle;height:20px;" src="/widgets/images/get_info.png"></a></span></th>
    </tr>
    <!--end 735-->
</thead>
<?php
//735
$lists = array("resultMyRecent" => "My Recent Techs", "resultMyPrefered" => "My Preferred Techs",  "resultRecent" => "FS-Experts: $Categoryname");
if($Categoryname=="" || empty($new_resultRecentExpert->data))
{
    $lists['resultRecent']="FS Recent Techs";
}
//end 735

$Core_Api_TechClass = new Core_Api_TechClass;
$preferredTechs = Core_Tech::getClientPreferredTechsArray($params['v']);

$ratingSearch = new Core_Api_Class;

foreach ($lists as $lvar => $lTitle) {
$lineIndex = 0;
?>
<thead class="sub-header">
        <tr>
                <th colspan="219" class="tbpadding"><?=$lTitle?> &nbsp;
                <a href="/clients/techs.php?v=<?=$params['v'];?>&newSearch=1&defaultZip=<?=$searchParams['ProximityZipCode']?><?php if ($lvar != 'resultMyPrefered') : ?>&snp=1<?php endif;?>" target="_blank" >find more</a>
                </th>
        </tr>
</thead>

<?php if (empty($view[$lvar]) ) { ?>
    <tbody class="sortin border_td">
        <tr><td colspan="19" class="tbpadding">No techs</td></tr>
    </tbody>
<?php } else {  ?>

    <tbody class="sortin border_td">
        <?
        //735
        $certSearch = new Core_TechCertifications;
        $apiexts = new Core_Api_TechClass;
        $tl = array();
        foreach ($view[$lvar] as $key => $techs ) {
            $tl[] = $techs->TechID;
        }
         $techCertification = $certSearch->getTechCertification($tl);
         //end 735
		$rangeStats = $ratingSearch->get12MonthsCallStats($_SESSION['UserName'].'|pmContext='.$params['v'], $_SESSION['Password'], $tl);
		$lifetimeStats =  $ratingSearch->getLifeTimeCallStats($_SESSION['UserName'].'|pmContext='.$params['v'], $_SESSION['Password'], $tl);
        ?>
    <?php foreach( $view[$lvar] as $key => $techs ) { ?>
        <tr class="border_td <?php if ($lineIndex % 2) {?>even_tr<?php }else { ?>odd_tr<?php } //endif;?><?php if ($techs->isPreferred || in_array($techs->TechID, $preferredTechs)) { ?> gold<?php } //endif;?>">
        <td>
            <!--910-->
            <?php
                $rangeStats = $ratingSearch->get12MonthsCallStats($_SESSION['UserName'].'|pmContext='.$params['v'], $_SESSION['Password'], $techs->TechID);
                $techRangeStats = $rangeStats[$techs->TechID];
                $rating = $techRangeStats['recommendedAvg'];
                $numberRankings = $techRangeStats['recommendedTotal'];
                if ($rating == NULL)
                {
                    $rating = "";
                    $numberRankings = 0;
                }

                $color = "#AAAAAA";
                $callsSumm = $techRangeStats['imacsMain'] + $techRangeStats['serviceMain'];
                if ($callsSumm == 0)
                {
                    $color = "#AAAAAA";
                }

                if ($callsSumm >= 5)
                {
                    $color = "#FFFF00";
                }

                if ($techRangeStats['noShowMain'] >= 2 || ($callsSumm > 0 && !empty($techRangeStats['recommendedAvg']) && $techRangeStats['recommendedAvg'] <= 90 && !empty($techRangeStats['performanceAvg']) && $techRangeStats['performanceAvg'] <= 90))
                {
                    $color = "#FF0000";
                }
                else if ($callsSumm >= 10 && !empty($techRangeStats['recommendedAvg']) && $techRangeStats['recommendedAvg'] >= 95 && !empty($techRangeStats['performanceAvg']) && $techRangeStats['performanceAvg'] >= 95 && $techRangeStats['backOutMain'] < 6 && $techRangeStats['noShowMain'] < 3)
                {
                    $color = "#00FF00";
                }

                $techID = $techs->TechID;
                $clientID = $_SESSION['ClientID'];
                $TechNetLikes = Core_Tech::getTechNetLikes($techID);
                $numNetLikes = $TechNetLikes['numNetLikes'];
                $numComments = 0;
                if($isGPM)
                {
                    $Core_Comments = new Core_Comments();
                    $numComments = $Core_Comments->getNumAllComments_forTech($techID);
                }
                else
                {
                    $numComments = Core_Comments::getNumComments_ForTech_andNonGPMClient($techID,$clientID,$companyID);
                }
                $colorLike = '';
                $imageThumbs = '/widgets/images/thumbs-up.png';//14027
                if($numNetLikes < 0)
                {
                    $colorLike = 'color:red';
                    $imageThumbs = '/widgets/images/thumbs-down.png';//14027
                }
            ?>
            <table border="0" width="50px">
                <tr style="height:17px;" valign="middle">
                    <td style="text-align:center;padding:0px;width:15px;border-right:0px;">
                        <div id="myRating<?=$techs->TechID?>" class="ratingBox" style="margin-left:0px;background-color: <?=$color?>;width: 10px; height: 10px;margin-left:3px;"></div>
                    </td>
                    <td style="border-right:0px"></td>
                </tr>
                <tr style="height:17px;" valign="middle">
                    <td style="text-align:center;padding:0px;width:15px;border-right:0px;">
                        <a class='cmdOpenTechProfileShow' href='wosViewTechProfile.php?simple=0&TechID=<?=$techID?>&v=<?=$companyID?>&cl=1' title='Add/View Comments' >
                            <img src='<?=$imageThumbs?>' height='16'/><!--14027-->
                        </a>
                    </td>
                    <td style="border-right:0px">
                        <a class='cmdOpenTechProfileShow' href='wosViewTechProfile.php?simple=0&TechID=<?=$techID?>&v=<?=$companyID?>&cl=1' title='Add/View Comments' >
                            <span style='<?=$colorLike?>'> <?=$numNetLikes?> </span>
                        </a>
                    </td>
                </tr>
                <tr style="height:17px;" valign="middle">
                    <td style="text-align:center;padding:0px;width:15px;border-right:0px;" >
                        <a class='cmdOpenTechProfileShow' href='wosViewTechProfile.php?simple=0&TechID=<?=$techID?>&v=<?=$companyID?>&cl=1' title='Add/View Comments' >
                            <img src='/widgets/images/Commentbubble_icon.png' />
                        </a>
                    </td>
                    <td style="border-right:0px">
                        <a class='cmdOpenTechProfileShow' href='wosViewTechProfile.php?simple=0&TechID=<?=$techID?>&v=<?=$companyID?>&cl=1' title='Add/View Comments' >
                            <?=$numComments?>
                        </a>
                    </td>
                </tr>
            </table>


<script type="text/javascript">   
        colorCode = document.getElementById("myRating<?=$techs->TechID?>");
        colorCode.rating = "<?=$rating?>";
        colorCode.numRating = "<?=$numberRankings?>";
</script>
<!--end 910-->
    </td>
    <td title="<?=$techs->TechID?>">
        <?
        if (!in_array($params['c'], array( "detailsContainer", "projectSearch", "undefined"))){ ?>
            <a class="cmdOpenTechProfileShow" href="/clients/wosViewTechProfile.php?backtolist=1&TechID=<?=$techs->TechID?>&v=<?=$_GET["v"]?>"><?=$techs->TechID?></a>
        <? } else { ?>
            <a class="cmdOpenTechProfileShow" href="/clients/wosViewTechProfile.php?backtolist=1&TechID=<?=$techs->TechID?>&v=<?=$_GET["v"]?>&fFrame=1&prev=findMyTechs.php&win=<?=$params['win']?>&clientWO=<?=$params['clientWO']?>&zip=<?=$params['zip']?>&distance=<?=$params['distance']?>&c=<?=$params['c']?>"><?=$techs->TechID?></a>
        <? }
        ?>
        <br />
        <a href="javascript:<? if(!in_array($params['c'], array( "detailsContainer", "projectSearch"))){ ?>window.parent.<? } ?>schedule.show($(this), null, {data:{'tech_ID' : '<?=$techs->TechID?>','date':'','date_interval':'all','display_calendar':'1','display_date':'1','sort':'starttime','sort_dir':'desc'}});">Schedule</span></a>
    </td>
    <td>
        <!--735-->
        <span  style='font-weight:bold;'><?= $techs->Firstname ?> <?= $techs->Lastname ?></span><br />
        <?php
        $PrimaryPhoneStatus = 1;
        $SecondaryPhoneStatus = 1;
        $contactInfo = $techs->PrimaryPhone;
        if (!empty($techs->TechID))
        {
            $result = $Core_Api_TechClass->getExts($techs->TechID);
            if (!empty($result['TechID']))
            {
                $PrimaryPhoneStatus = $result['PrimaryPhoneStatus'];
                $SecondaryPhoneStatus = $result['PrimaryPhoneStatus'];
            }
        }
        if ($PrimaryPhoneStatus == "1" && $techs->PrimaryPhone != "")
        {
            $contactInfo .= "&nbsp;<img class=\"imgClassReQuitePhone\" align=\"absmiddle\" ";
            $contactInfo .= " src=\"" . (($PrimaryPhoneStatus == "1") ? '/widgets/images/ico_phonex16.png' : '/widgets/images/ico_Notphonex16.jpg') . '"';
            $contactInfo .= " title=\"" . (($PrimaryPhoneStatus == "1") ? 'Click to notify ' . $techs->Firstname . " " . $techs->Lastname . ' that this Phone # is Invalid' : 'Invalid Phone # Reported') . '"';
            $contactInfo .= ($PrimaryPhoneStatus == "1") ? " onclick=\"javascript:detailObject.onInit.RequitePhoneClick(this,'" . $techs->TechID . "' , 1,'" . base64_encode($techs->Firstname . " " . $techs->Lastname) . "','" . $techs->PrimaryPhone . "');\"" : " ";
            $contactInfo .= " />";
        }
        ?>
        <?= $contactInfo ?><br />
        <span><?= $techs->PrimaryEmail ?></span><br />
        <!--end 735-->
    </td>
    <td>
        <nobr><?=$techs->City?>
        <?=$techs->State?></nobr><br />
        <span><?=$techs->Zipcode?></span><br />
    </td>
<?php
    $distance = 'n/a';
    $distance = Core_Geocoding_Google::distance($techs->Latitude, $techs->Longitude, $searchParams['ProximityLat'], $searchParams['ProximityLng']);

	$TotalWOs = (int)$techs->Qty_IMAC_Calls + (int)$techs->Qty_FLS_Service_Calls;
	$techID = $techs->TechID;
	if ($TotalWOs < ($lifetimeStats[$techID]['imacsMain'] + $lifetimeStats[$techID]['serviceMain'])) {
		$techs->Qty_IMAC_Calls = $lifetimeStats[$techID]['imacsMain'];
		$techs->Qty_FLS_Service_Calls = $lifetimeStats[$techID]['serviceMain'];
		$techs->PreferencePercent = $lifetimeStats[$techID]['recommendedAvg'];
		$techs->SATRecommendedTotal = $lifetimeStats[$techID]['recommendedTotal'];
		$techs->PerformancePercent = $lifetimeStats[$techID]['performanceAvg'];
		$techs->SATPerformanceTotal = $lifetimeStats[$techID]['performanceTotal'];
		$techs->Back_Outs = $lifetimeStats[$techID]['backOutMain'];
		$techs->No_Shows = $lifetimeStats[$techID]['noShowMain'];
	}
					
	if($techs->SATRecommendedTotal == NULL){
        $preferencePercent = "0% (0)";
    } else {
        $preferencePercent = number_format($techs->PreferencePercent, 0) . "% (" . $techs->SATRecommendedTotal . ")";
    }

    if($techs->SATPerformanceTotal == NULL){
        $performancePercent = "0% (0)";
    } else {
        $performancePercent = number_format($techs->PerformancePercent, 0) . "% (" . $techs->SATPerformanceTotal . ")";
    }

?>
    <td style="text-align: center;">
        <span><?=round($distance)?></span><br />
    </td>
    <td style="text-align: center;">
        <nobr><?=$preferencePercent?></nobr><hr class="thin_hr">
        <nobr><?=$performancePercent?></nobr>
    </td>
    <!--943-->
    <td style="text-align: center;">
        <?
        $core = new Core_Api_WosClass();
        $result = $core->TotalWos_byTechandCompany($techs->TechID,$companyID);
        ?>
        <span title="All FieldSolutions Work Orders"><?=((int)$techs->Qty_IMAC_Calls + (int)$techs->Qty_FLS_Service_Calls)?></span>
        <hr  class="thin_hr"/>
        <span title="All <?=$CompanyName?> Work Orders"><?=$result?></span>
    </td>
    <!--end 943-->
    <td style="text-align: center;">
        <span><?=$techs->No_Shows?></span><hr class="thin_hr">
        <span><?=$techs->Back_Outs?></span>
    </td>
    <?
    //735
    //client Client Credentials 
    $apiTechClass = new Core_Api_TechClass();

    $colnum = $isGPM?3:2;
    $rownum = 0;
    $colspan = $isGPM?2:1;
    $isviewmore = false;

    $BlackBoxCablingStatus = $apiTechClass->GetBlackBoxCablingStatus($techs->TechID);
    $BlackBoxTelecomStatus = $apiTechClass->getBlackBoxTelecomStatus($techs->TechID);
    $isdisplayBlackBoxCabling = !empty($BlackBoxCablingStatus) ? 1 : 0;
    $isdisplayBlackBoxTelecom = !empty($BlackBoxTelecomStatus) ? 1 : 0;
    $isdisplayblackbox = 0;
    if ($isGPM || in_array($companyID, array('BBAL', 'BBGC', 'BBGE', 'BBCN', 'BCSST', 'BBGN', 'BBJC', 'BBLC', 'BBNSG', 'BBNC', 'BBS', 'BB')))
    {
        $isdisplayblackbox = 1;
    }

    //13912
    $extFilters = array();
    if ($isGPM)
    {
        $isOtherCompanyTag = 1;
        $callerIsGPM=1;
        $extFilters['VisibilityId']=4;    
    } else
    {
        $isOtherCompanyTag = 0;
        $callerIsGPM=0;
        $extFilters['VisibilityId']=1;    
    }
    //931
    $extFilters['SortByTitle']=1;
    //end 931
    $FSTagClass = new Core_Api_FSTagClass();
    //839
    $getFSTagList = $FSTagClass->getFSTagList_forTech_forCompany($techs->TechID, $companyID, $isOtherCompanyTag, 1, $callerIsGPM, null, 1, $extFilters);
    $getFSTagListNew = array();
    foreach($getFSTagList as $elementKey => $element)
    {
        if($getFSTagList[$elementKey]['Name']=="BlackBoxCabling")
        {
            if($isdisplayblackbox==1)
            {
                if($isdisplayBlackBoxCabling==1 )
                {
                    $getFSTagListNew[] = $getFSTagList[$elementKey];
                }
            }
        }
        else if($getFSTagList[$elementKey]['Name']=="BlackBoxTelecom")
        {
            if($isdisplayblackbox==1)
            {
                if($isdisplayBlackBoxTelecom==1 )
                {
                    $getFSTagListNew[] = $getFSTagList[$elementKey];
                }
            }
        }
        else if(!empty($getFSTagList[$elementKey]['TechId']))
        {
            $getFSTagListNew[] = $getFSTagList[$elementKey];
        }
    }
    $FSTagNo = count($getFSTagListNew);

    $rownum = 4;
    if($FSTagNo>12)
    {
        $isviewmore = true;
    }
    $field = "";
    if($isGPM)
    {
        $field .= "<table cellspacing='0' style=\"text-align:left;\" width=\"111px\">";
        $field .= "<colgroup>";
        $field .= "<col width=\"33%\">";
        $field .= "<col width=\"33%\">";
        $field .= "<col width=\"33%\">";
        $field .= "</colgroup>";
    }
    else
    {
        $field .= "<table cellspacing='0' style=\"text-align:left;\" width=\"74px\">";
        $field .= "<colgroup>";
        $field .= "<col width=\"50%\">";

        $field .= "<col width=\"50%\">";
        $field .= "</colgroup>";
    }
    $k=0;
    //912
    $defaultnotedit = array('ErgoMotionCertifiedTechnician'=>'ErgoMotionCertifiedTechnician',
        'TechForceICAgreement'=>'TechForceICAgreement',
        'Dell_MRA_Compliant'=>'Dell_MRA_Compliant',
        'BootCampCertified'=>'BootCampCertified',
        'FSMobile'=>'FSMobile');
    //end 912
    for($i=0;$i<$rownum;$i++)
    {
        $field .= "<tr style='text-align:center;'>";
        for($j=0;$j<$colnum;$j++)
        {
            if($i==$rownum-1 && $isviewmore && $j>0)
            {
                $field .= "<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;' colspan='$colspan'>";
                $field .= "<a href='wosViewTechProfile.php?simple=0&TechID=$techs->TechID&v=$companyID&C=1' style='text-decoration:underline;' class='cmdOpenTechProfileShow'>See More...</a>";
                $field .= "</td>";
                break;
            }
            $field .= "<td style='padding: 2px 2px; border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
            if(empty($getFSTagListNew[$k]))
            {
                $field .= "<div style='height:19px;'>&nbsp;</div>";
            }
            else
            {
                if($getFSTagListNew[$k]['Name']=="BlackBoxTelecom")
                {
                    $BlackBoxdate = $BlackBoxTelecomStatus['Date'];
                    if (!empty($BlackBoxdate))
                    {
                        $strBlackBoxDate = date_format(new DateTime($BlackBoxdate), "m/d/Y");
                    } else
                    {
                        $strBlackBoxDate = "";
                    }

                    if ($BlackBoxTelecomStatus['CertID'] == "49")
                    {
                        $BlackBoximg = "BTT1.png";
                        $BlackBoxStatus = "Black Box Telecom - " . $BlackBoxTelecomStatus['LevelText'] . ": " . $strBlackBoxDate;
                    } else if ($BlackBoxTelecomStatus['CertID'] == "50")
                    {
                        $BlackBoximg = "BTT2.png";
                        $BlackBoxStatus = "Black Box Telecom - " . $BlackBoxTelecomStatus['LevelText'] . ": " . $strBlackBoxDate;
                    }

                    $field .= "<img id='BlackBoxCablingimgtag' style='" . ($BlackBoxTelecomStatus['Status'] != 'Pending' ? "display:'';" : "display:none;") . "' height='19px' bt-xtitle='" . $BlackBoxStatus . "' class='hoverText' src='/widgets/images/" . $BlackBoximg . "' title=''/>";
                }
                else if($getFSTagListNew[$k]['Name']=="BlackBoxCabling" )
                {
                    $BlackBoxdate = $BlackBoxCablingStatus['Date'];
                    if (!empty($BlackBoxdate))
                    {
                        $strBlackBoxDate = date_format(new DateTime($BlackBoxdate), "m/d/Y");
                    } else
                    {
                        $strBlackBoxDate = "";
                    }
                    if ($BlackBoxTelecomStatus['CertID'] == "46")
                    {
                        $BlackBoximg = "BTC1.png";
                        $BlackBoxStatus = "Black Box Cabling - " . $BlackBoxCablingStatus['LevelText'] . ": " . $strBlackBoxDate;
                    } else if ($BlackBoxCablingStatus['CertID'] == "47")
                    {
                        $BlackBoximg = "BTC2.png";
                        $BlackBoxStatus = "Black Box Cabling - " . $BlackBoxCablingStatus['LevelText'] . ": " . $strBlackBoxDate;
                    } else if ($BlackBoxCablingStatus['CertID'] == "48")
                    {
                        $BlackBoximg = "BTC3.png";
                        $BlackBoxStatus = "Black Box Cabling - " . $BlackBoxCablingStatus['LevelText'] . ": " . $strBlackBoxDate;
                    } 

                    $field .= "<img id='BlackBoxCablingimgtag' style='" . ($BlackBoxCablingStatus['Status'] != 'Pending' ? "display:'';" : "display:none;") . "' height='19px' bt-xtitle='" . $BlackBoxStatus . "' class='hoverText' src='/widgets/images/" . $BlackBoximg . "' title=''/>";
                }
                else
                {
                    if(empty($getFSTagListNew[$k]['Date']))
                    {
                        $date = "";
                    }
                    else
                    {
                        $date = new Zend_Date($getFSTagListNew[$k]['Date'], 'YYYY-mm-dd'); 
                    }

                    if(!empty($getFSTagListNew[$k]['TagArt']))
                    {
                        $TagArt = "https://s3.amazonaws.com/wos-docs/tags/". str_replace ("|", "", $getFSTagListNew[$k]['TagArt']);
                    }
                    else
                    {
                        $TagArt = "/widgets/images/check_green.gif";
                    }

                    if(empty($getFSTagListNew[$k]['ExistingImagePath']))
                    {
                        $ExistingImagePath = $TagArt;
                    }
                    else
                    {
                        $ExistingImagePath = $getFSTagListNew[$k]['ExistingImagePath'];
                    }
                    //931
                    $HasMultiTagLevels="";
                    if($getFSTagListNew[$k]['HasMultiTagLevels']==1 && !empty($getFSTagListNew[$k]['TagLevelsData']))
                    {      
                        $HasMultiTagLevels = ": L".$getFSTagListNew[$k]['LevelOrder']."&nbsp;-&nbsp;".$getFSTagListNew[$k]['LevelTitle'];
                    }
                    $Title = $getFSTagListNew[$k]['Title'];
                    if (!empty($date))
                    {
                        $date = ": ".$date->toString('mm/dd/YYYY');
                    }
                    else
                    {
                        $date = "";
                    }
                    $field .= "<img src='$ExistingImagePath' class='hoverText' bt-xtitle='$Title$HasMultiTagLevels$date' height='19px' />";
                    //end 931
                }
            }
            $field .= "</td>";
            $k++;
        }
        $field .= "</tr>";
    }
    //end 839
    $field .= "</table>";
    $ClientCredentials = $field;
    //end Client Credentials 
    
    //puplic Credentials 
    $cert = $techCertification[$techs->TechID];
    $TBCCertInfo = $apiexts->getTechCertInfo(4,$techs->TechID);
    $puplicNo = 0;
    //background check
    $field ="<table cellspacing='0' cellpadding='0' width='74px'>";
    $field .="<colgroup><col width=\"50%\"><col width=\"50%\"></colgroup>";
    $field .="<tr style=\"text-align:center;\">";
    //Background Check
    $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
    if ($techs->Bg_Test_Pass_Lite  == "Pass" && $techs->Bg_Test_Pass_Full  == "Pass") {
        $basicDate = strtotime($techs->BG_Test_ResultsDate_Lite);
        $basicDate = date("m/d/Y", $basicDate);
        $compDate = strtotime($techs->BG_Test_ResultsDate_Full);
        $compDate = date("m/d/Y", $compDate);
        $field .= "<img src=\"/widgets/images/BCPlus.png\" height=\"19px\" class=\"hoverText\" bt-xtitle=\"Comprehensive Background Check - {$compDate}\" style=\"margin-left: 2px;\"/>";
        $puplicNo++;
    } elseif ($techs->Bg_Test_Pass_Lite == "Pass" && $techs->Bg_Test_Pass_Full != "Pass") {
        $basicDate = strtotime($techs->BG_Test_ResultsDate_Lite);
        $basicDate = date("m/d/Y", $basicDate);
        $field .= "<img src=\"/widgets/images/BC.png\" height=\"19px\" class=\"hoverText\" bt-xtitle=\"Basic Background Check - {$basicDate}\" />";
        $puplicNo++;
    } else {
        $field .= "<div style='height:19px;'>&nbsp;</div>";
    }
    $field .="</td>";

    //gold drug test
    $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
    $img="&nbsp;";
    if ($techs->DrugPassed  == 1)
    {
         //14108   
        $Date = strtotime($techs->DatePassDrug);
        if(!empty($Date) && $Date > 0){    
        $Date = date("m/d/Y", $Date);
        }else{
          $Date = '';  
        }
          //End 14108
        $img = "dt9.png";
        $title ="Gold Drug Test (9 Panel)";
        $puplicNo++;
    }
    else if ($techs->SilverDrugPassed == 1)
    {
        //14108 
        $Date = strtotime($techs->DatePassedSilverDrug);
         if(!empty($Date) && $Date > 0){           
        $Date = date("m/d/Y", $Date);
        }else{
          $Date = '';  
        }
        //End 14108
        $img = "dt5.png";
        $title ="Silver Drug Test (5 Panel)";
        $puplicNo++;
    }
    else
    {
        $Date=null;
        $title=null;
    }

    if($img!="&nbsp;")
    {
        $field .="<img src='/widgets/images/$img' height='19px' class='hoverText' bt-xtitle='$title &ndash; {$Date}' />";
        $puplicNo++;
    }
    else
    {
        $field.="<div style='height:19px;'>&nbsp;</div>";
    }
    $field .="</td>";
    $field .="</tr>";
    $field .="<tr style=\"text-align:center;\">";

    //dell
    $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
    $DellMRACompliant = "";
    $LapsedDate='';
    $DatePassDrug = new Zend_Date($techs->DatePassDrug, 'YYYY-mm-dd');
    $Bg_Test_ResultsDate_Full = new Zend_Date($techs->Bg_Test_ResultsDate_Full, 'YYYY-mm-dd');

    if($techs->Bg_Test_Pass_Full == "Pass" && $techs->DrugPassed == '1' 
            && $Bg_Test_ResultsDate_Full >= date("YYYY-mm-dd", strtotime("-12 months")) 
            && $DatePassDrug >= date("YYYY-mm-dd", strtotime("-12 months")) )
    {
        $DellMRACompliant ='Passed';
    }
    else if($techs->Bg_Test_Pass_Full = 'Pass' && $techs->DrugPassed = '1' 
            && ($Bg_Test_ResultsDate_Full < date("YYYY-mm-dd", strtotime("-12 months")) 
                || $DatePassDrug < date("YYYY-mm-dd", strtotime("-12 months"))))
    {
        $DellMRACompliant ='Lapsed';
    }


    if($Bg_Test_ResultsDate_Full > $DatePassDrug)
    {
        $LapsedDate= date($DatePassDrug, strtotime("+12 months"));
    }
    else
    {
        $LapsedDate= date($Bg_Test_ResultsDate_Full, strtotime("+12 months"));
    }
    if(!empty($DellMRACompliant ))
    {
        if ($DellMRACompliant == 'Passed')
        {
            $field .= "<img src='/widgets/images/dellMRAPassed.png' height='19px' class='hoverText' bt-xtitle='Dell MRA Compliant' />";
        }
        else if ($DellMRACompliant == 'Lapsed')
        {
            $date = new Zend_Date($LapsedDate, 'YYYY-mm-dd');
            if (!empty($date))
            {
                $date = ": ".$date->toString('mm/dd/YYYY');
            }
            else
            {
                $date = "";
            }

            $field .= "<img src='/widgets/images/dellMRAPassed.png' height='19px' class='hoverText' bt-xtitle='Dell MRA Compliance Lapsed $date'/>";
        }
        $puplicNo++;
    }
    else
    {
        $field.="<div style='height:19px;'>&nbsp;</div>";
    }
    $field .="</td>";

    //FS-mobil
    $FSmobilimg="&nbsp;";
    $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
    if(!empty($techs->FS_Mobile_Date))
    {
        $Date = strtotime($techs->FS_Mobile_Date);
        $Date = date("m/d/Y", $Date);
        $FSmobilimg = "fs_mobile_36x32.png";
        $title ="I Have FS-Mobile";
    }
    else
    {
        $Date=null;
        $title=null;
    }
    if($FSmobilimg!="<div style='height:19px;'>&nbsp;</div>")
    {
        $field .="<img src='/widgets/images/$FSmobilimg' height='19px' class='hoverText' bt-xtitle='$title &ndash; {$Date}' />";
        $puplicNo++;
    }
    else
    {
        $field.="<div style='height:19px;'>&nbsp;</div>";
    }
    $field .="</td>";
    $field .="</tr>";
    $field .="<tr style=\"text-align:center;\">";
    //Blue Ribbon Technician
    $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
    if (sizeof($cert) > 0 && array_key_exists(4, $cert))
    {
        if(!empty($TBCCertInfo['date']))
        {
            $Date = split('-',$TBCCertInfo['date']);
            $Datestr = ": ".$Date[1]."/".$Date[2]."/".$Date[0];
        }
        else
        {
            $Datestr="";
        }

        $field .= "<img src=\"/widgets/images/BRT_Tag.png\" height='19px' class=\"hoverText\" bt-xtitle=\"Blue Ribbon Technician$Datestr\" />";
        $puplicNo++;
    }
    else
    { 
        $field.="<div style='height:19px;'>&nbsp;</div>";
    }
    $field .="</td>";

    //boot camp
    $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
    if (sizeof($cert) > 0 && array_key_exists(3, $cert))
    {
        $BootCampCertInfo = $apiexts->getTechCertInfo(3,$techs->TechID);
        if(!empty($BootCampCertInfo['date']))
        {
            $BootCampdate = split('-',$BootCampCertInfo['date']);
            $BootCampdatestr = ": ".$BootCampdate[1]."/".$BootCampdate[2]."/".$BootCampdate[0];
        }
        else
        {
            $$TBCCertInfodatestr="";
        }
        $field .= "<img src=\"/widgets/images/Boot_Tag.png\" height='19px' class=\"hoverText\" bt-xtitle=\"Boot Camp Certified$BootCampdatestr\" />";
        $puplicNo++;
    }
    else if (sizeof($cert) > 0 && array_key_exists(43, $cert))
    {
        $BootCampplusCertInfo = $apiexts->getTechCertInfo(43,$techs->TechID);
        if(!empty($BootCampplusCertInfo['date']))
        {
            $BootCampplusdate = split('-',$BootCampplusCertInfo['date']);
            $BootCampplusdatestr = ": ".$BootCampplusdate[1]."/".$BootCampplusdate[2]."/".$BootCampplusdate[0];
        }
        else
        {
            $BootCampplusdatestr="";
        }

        $field .= "<img src=\"/widgets/images/Boot_Tag_PLUS.png\" height='19px' class=\"hoverText\" bt-xtitle=\"Boot Camp Plus$BootCampplusdatestr\" />";
        $puplicNo++;
    }
    else
    { 
        $field.="<div style='height:19px;'>&nbsp;</div>";
    }
    $field .="</td>";
    $field .="</tr>";
    $field .="<tr style=\"text-align:center;\">";
    //I9
    $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
    $apiTechClass = new Core_Api_TechClass();
    $extsUSAuth = $apiTechClass->getExts($techs->TechID);
    $i9img="&nbsp;";
    if ($extsUSAuth["I9Status"] == 2)
    {
        $title="Authorized to perform services in the United States";
        $date = new Zend_Date($extsUSAuth['I9Date'], 'YYYY-mm-dd');
        $i9datestr = " &ndash; ".$date->toString('mm/dd/YYYY');
        $i9img="VerifiedTall2.png";
    }
    else if($extsUSAuth["I9Status"] == 1)
    {
        $title="Authorization to perform services in the United States is pending";
        $i9datestr = "";
        $i9img="PendingTall2.png";
    }

    if($i9img!="&nbsp;")
    {
        $field .="<img src='/widgets/images/$i9img' height='19px' class='hoverText' bt-xtitle='$title $i9datestr' />";
        $puplicNo++;
    }
    else
    {
        $field.="<div style='height:19px;'>&nbsp;</div>";
    }
    $field .="</td>";

    //ISO
    $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
    if(!empty($techs->ISO_Affiliation_ID))
    {
        $field .="<img src='/widgets/images/iso-2.png' height='19px' class='hoverText' bt-xtitle='This technician is a member of an Independent Service Organization &#40;ISO&#41;' />";
        $puplicNo++;
    }
    else
    {
        $field.="<div style='height:19px;'>&nbsp;</div>";
    }
    $field .="</td>";
    $field .="</tr>";
    $field .="</table>";
    $puplicCredentials = $field;
    //end puplic Credentials
    
    //Expert
    //FS-Experts 
    $apiFSExpert = new Core_Api_FSExpertClass();
    $FSExperts = $apiFSExpert->getFSExpertListForTech($techs->TechID);
    $FSExpertsno = count($FSExperts);

    $field ="<table cellspacing='0' cellpadding='0' width='182px'>";
    $field .="<colgroup>";
    $field .="<col width=\"20%\">";
    $field .="<col width=\"20%\">";
    $field .="<col width=\"20%\">";
    $field .="<col width=\"20%\">";
    $field .="<col width=\"20%\">";
    $field .="</colgroup>";
    $k=1;
    for ($i = 0; $i < 4; $i++)
    {
        $field .="<tr>";
        for ($j = 0; $j < 6; $j++)
        {
            if($j==5)
            {
                $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD; display:none;'>";
            }
            else
            {
                $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
            }
            if (sizeof($FSExperts) > 0 && array_key_exists($k, $FSExperts))
            {
                if (!empty($FSExperts[$k]["Title"]))
                {
                    $title = "FS-Expert: <b>".$FSExperts[$k]["Title"]."</b><div style=\"padding-left:15px;padding-top:10px;\"><ul><li style=\"list-style:disc outside none;\">".$FSExperts[$k]["Content"]."</li></ul></div>";
                }
                else
                {
                    $title = "" ;
                }
                $field .="<img src='/widgets/images/expert/".$FSExperts[$k]["Abbreviation"].".png' height='19px' k='$k' class='hoverTextnew'  bt-xtitle='$title' />";
            } else
            {
                $field.="<div style='height:19px;width:30px;'>&nbsp;</div>";
            }
            $field .= "</td>";
            $k++;
        }
        $field .= "</tr>";
    }
    $field .="</table>";
    $Expertfield = $field;
    //end Expert
    ?>
    <td style="display:none;"><span><?=$FSTagNo;?></span></td>
    <td style="display:none;"><span><?=$puplicNo;?></span></td>
    <td style="display:none;"><span><?=$FSExpertsno;?></span></td>
    <td style="text-align: center; width: <?= $isGPM ? "111px" : "74px" ?>;">
        <?
        echo($ClientCredentials);
        ?>
    </td>
    <td>
        <?php
        echo($puplicCredentials);
        ?>
    </td>
    <td>
        <?
        echo($Expertfield);
        ?>
    </td>
    <?php
    $lineIndex++;
    } // end foreach
    ?>
    </tbody>
<?php } //endif?>

<?php } //end foreach;?>
</table>


</div>

<script type="text/javascript" src="../library/jquery/jquery.bt.min.js"></script>
<script>
var headIndex = 1;

function sortDefault()
{
	$("#sortTable>tbody>tr").tsort("td:eq(0)[attr]",{order:'desc', attr : 'attr'});
}

$(document).ready(function() { 
    $('#sort-line th.sortable').each(function()
    {
        if($(this).attr("special")!="")
        {
            
            if($(this).attr("special")=='fsexpert')
            {
                $("#fsexpert").attr('index', headIndex-3);
                $("#fsexpert").click(function(){
                    sortTable($(this).attr('index'));
                });
                headIndex++;
            }
            else
            {
                $(this).attr('index', headIndex-3);
                $(this).click(function(){
                    sortTable($(this).attr('index'));
                });
                headIndex++;
            }
        }
        else
        {
            $(this).attr('index', headIndex++);
            $(this).click(function(){
                sortTable($(this).attr('index'));
            });
        }
    });
    sortDefault();
}
);


function cleanupHeaders()
{
	$('#sort-line th.sortable').each(function() {
		$(this).removeClass('sort-asc').removeClass('sort-desc');
	});
}


var aAsc = [];
function sortTable(nr) {
    cleanupHeaders();
    //735
    if(nr==8 ||nr==9 || nr==10)
    {
        if(typeof(aAsc[nr])=="undefined")
        {
            aAsc[nr] = "asc";
        }
    }
    //end 735
    aAsc[nr] = aAsc[nr]=="asc"?"desc":"asc";
    
    if(nr==8 ||nr==9)
    {
        $('#sort-line th.sortable[index=' + (nr+3) + ']').addClass('sort-' + aAsc[nr]);
    }
    else if( nr==10 )
    {
        $(this).parent().addClass('sort-' + aAsc[nr]);
    }
    else
    {
        $('#sort-line th.sortable[index=' + nr + ']').addClass('sort-' + aAsc[nr]);
    }

    if (nr == 1) {
        $("#sortTable>tbody>tr").tsort("td:eq("+nr+")",{order:aAsc[nr], attr : 'title'});
    }
    else {
        $("#sortTable>tbody>tr").tsort("td:eq("+nr+") span",{order:aAsc[nr]});
    }
}

jQuery(".hoverText").bt({
    titleSelector: "attr('bt-xtitle')",
    fill: 'white',
    cssStyles: {color: 'black', fontWeight: 'bold', width: 'auto'},
    animate:true
});
//735
jQuery(".hoverTextnew").bt({
    titleSelector: "attr('bt-xtitle')",
    fill: 'white',
    cssStyles: {color: 'black', width: '300px'},
    animate:true
});
$("#expertinfocontrols").unbind('click').bind('click', function(){
   var content = "<div id='comtantPopupID'><div style='text-align:center;height:380px;width:900px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
        $("<div></div>").fancybox(
        {
            'showCloseButton' :true,
            'width' : 800,
            'height' : "auto",
            'content': content
        }
        ).trigger('click');

        $.ajax({
        type: "POST",
        url: "/widgets/dashboard/popup/expert-list",
        data: "",
        success:function( html ) {
            var content = html;
            $("#comtantPopupID").html(content);
        }
    });
});
//end 735

jQuery(window).ready(function(){
           _global.eventOpenProfileTech(); 
        });
</script>

<?php
/*** End Content ***/
//require ("../footer.php");
?> 