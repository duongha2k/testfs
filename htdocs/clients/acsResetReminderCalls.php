<?php
	require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');

	require_once("../headerStartSession.php");
	require_once("../library/timeStamps2.php");
	require_once("../library/woACSTimeStamps.php");
//	ini_set("display_errors",1);
	$id = $_GET["id"];
	$userName = $_GET["userName"];
	$projectName = $_GET["projectName"];
	$cID = $_GET["cID"];
	$cName = $_GET["cName"];
	if (is_numeric($id)) {
		Core_IVR_Redial::resetCalls($id);
		Core_IVR_Log::resetCalls($id);
		Create_TimeStamp($id, $userName, "Reset ACS Calls", $cID, $cName, $projectName, "", "", "");
		createWOACSTimeStamp($id);
	}
//		queueCreateTimeStamp(woID, "Reset ACS Calls", Company_ID, clientName, projectName, "", "", "");
//		window.open("acsResetReminderCalls.php?id=" + document.forms.caspioform.GetTB_UNID.value);

	$httpReferer = (isset($_GET['referer']) ? urldecode($_GET['referer']) : $_SERVER["HTTP_REFERER"]);
	$saveReferer = $_SESSION["saveReferer"];
	if ($_SESSION["saveReferer"] != $httpReferer)
		$_SESSION["saveReferer"] = $httpReferer;

	if (isset($_GET["saveReferer"])) {
		$_SESSION["saveReferer"] = $_GET["saveReferer"];
		$saveReferer = $_SESSION["saveReferer"];
	}
?>

<script type="text/javascript">
	var saveReferer = "<?=$saveReferer?>";
</script>

<script type="text/javascript">
	var selectedPID = "";
	try {
		parent.reloadTabFrame();
	}
	catch (e) {
		if (saveReferer != "") {
			document.location = saveReferer;
		}
	}
</script>
