<?php 
header('Content-Type:text/html; charset=UTF-8');
require ("../headerSimple.php"); 
?>

<?php

$fields = array(
    "DateChanged" => "DateChanged",
    "Project_ID" => "ProjectId",
    "Project_Name" => "ProjectName",
    "UserName" => "UserName",
    "ReminderAll" => "ReminderAll",
    "ReminderAcceptance" => "ReminderAcceptance",
    "SMSBlast" => "SMSBlast",
    "Reminder24Hr" => "Reminder24Hr",
    "Reminder1Hr" => "Reminder1Hr",
    "CheckInCall" => "CheckInCall",
    "CheckOutCall" => "CheckOutCall",
    "ReminderNotMarkComplete" => "ReminderNotMarkComplete",
    "ReminderIncomplete" => "ReminderIncomplete",
    "ApplyTo" => "Change Applied To (1 = ALL, 2 = Unassigned, 3 = New)",
    "PcntDeduct" => "PcntDeduct"
);

	$displayPMTools = false;
	require ("includes/PMCheck.php"); 

	require_once realpath(dirname(__FILE__) . '/../') . '/includes/_init.service.php';
	
	$result = null;
	
	if (empty($_GET['sortDir']) || !in_array($_GET['sortDir'], array('asc', 'desc')) ) {
	    $_GET['sortDir'] = 'desc';
	}
	
	if (empty($_GET['sortField']) || !key_exists($_GET['sortField'], $fields)) {
	    $_GET['sortField'] = 'DateChanged';
	}

	if ( !empty($_GET['id']) && !empty($_GET['v']) ) {
	    $api = new Core_Api_Class();
        $res = $api->getProjectTimestampsByProject($_SESSION['UserName'].'|pmContext='.$_GET['v'], $_SESSION["Password"], $_GET['id'], 0, 0, "{$_GET['sortField']} {$_GET['sortDir']}");
	    if ($res->success) $result = $res->data;
	}
	
	
	function makeURL($sortField) 
	{
	    
	    $params = array(
	        'v' => $_GET['v'],
	        'id' => $_GET['id'],
	        'sortField' => $sortField,
	        'sortDir' => ($_GET['sortField'] == $sortField && $_GET['sortDir'] == 'asc') ? 'desc' : 'asc'
	    );
	    
	    $pParams = array();
	    foreach ($params as $key => $value) {
	        $pParams[] = "{$key}={$value}";
	    } 
	    
	    return "acsProjectChangeLog.php?" . implode('&', $pParams);
	}
	
	function getSortClass($sortField) 
	{
        if ($sortField == $_GET['sortField']) {
            if ($_GET['sortDir'] == 'asc') {
                return ' sort-asc';
            } else {
                return ' sort-desc';
            }
        }
        return '';
	}
	

	if (is_null($result)) :
?>
<br/><center><b>Wrong ID</b></center>
<?php 
    elseif (empty($result)):
?>
<br/><center><b>There aren't records to display</b></center>
<?php 
    else:
?>
<table width="100%" class="gradBox_table_inner">
    <tr class="table_head">
    <?php foreach ($fields as $fieldName => $fieldValue) : ?>
<th class="sortable<?=getSortClass($fieldName)?>"><a href="<?=makeURL($fieldName)?>" ><?=$fieldValue?></a></th>
    <?php endforeach; ?>
    </tr>
<?php   
        $tr = 1; 
        foreach ($result as $item) :
            $dateTime = new Zend_Date($item->DateChanged, 'yyyy-MM-dd HH:mm:ss');
        ?>
        <tr class="<?=($tr % 2)?'border_td odd_tr':'border_td even_tr'?>">
        	<td><br/><nobr><?= (empty($item->DateChanged) ? '' : $dateTime->toString("MM/dd/Y HH:mm a")) ?></nobr><br/></td>
        	<td><?= htmlspecialchars($item->Project_ID) ?></td>
        	<td><?= htmlspecialchars($item->Project_Name) ?></td>
        	<td><?= htmlspecialchars($item->UserName) ?></td>
        	<td align="center"><?= htmlspecialchars($item->ReminderAll) ?></td>
        	<td align="center"><?= htmlspecialchars($item->ReminderAcceptance) ?></td>
        	<td align="center"><?= htmlspecialchars($item->SMSBlast) ?></td>
        	<td align="center"><?= htmlspecialchars($item->Reminder24Hr) ?></td>
        	<td align="center"><?= htmlspecialchars($item->Reminder1Hr) ?></td>
        	<td align="center"><?= htmlspecialchars($item->CheckInCall) ?></td>
        	<td align="center"><?= htmlspecialchars($item->CheckOutCall) ?></td>
        	<td align="center"><?= htmlspecialchars($item->ReminderNotMarkComplete) ?></td>
        	<td align="center"><?= htmlspecialchars($item->ReminderIncomplete) ?></td>
        	<td align="center"><?= htmlspecialchars($item->ApplyTo) ?></td>
        	<td align="center"><?= htmlspecialchars($item->PcntDeduct) ?></td>
        </tr>
        <?php
        endforeach;
?>
</table>
<?php 
    endif;
?>

</body>
</html>
