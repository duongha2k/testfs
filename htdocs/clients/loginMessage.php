<?php
//Set logged in session
setcookie("clientLoginMessageViewed", "yes",  time()+60*60*24*365*10, "/", ".fieldsolutions.com");
?>
<?php $page = 'none'; ?>
<?php $option = 'none'; ?>
<?php $displayHome = 'no'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>


<!-- Add Content Here -->
<style>
p.textContent{margin:0px;}
</style>
<br /><br />

<div align="center">

<table border="0" cellpadding="0" cellspacing="0">
	<tr>
<td width="700" align="left">
<h1 style="font-size:18px">Welcome to the New Field Solutions Web Site!</h1>
<br />
<p class="textContent">
<h2 style="font-size:14px;">What's New:</h2>
New Navigation with fewer clicks from log-on throughout the work order process
</p>
<p class="textContent">
<b>The New Field Solutions Work Order</b>
</p>
<p class="textContent">
New navigation - find/search for a tech on top, easy to find and go to<br />
New work order flow with 8 sections in the sequence of work so it is easier to see what has been accomplished<br />
New fields for controlling out of scope and materials costs, with less time<br />
More structured content - phone numbers, dates and times now in standard formats for better accuracy
</p>
<p class="textContent">
<b>New Dashboards</b>
Real time visibility to every work order based on searches and stage of delivery
Look up and find work orders faster, with project level and date search to populate dashboards
Sort work orders in any sequence for better tracking and visibility
</p>

<p class="textContent">
<b>New Reporting</b>
All dashboards, and one-click report export into Excel spreadsheet
</p>

<p class="textContent">
<h2 style="font-size:14px;">What's The Same:</h2>
All the same data<br />
All technician and project information is still there<br />
Same administrator controls<br />
</p>

<p class="textContent">
<h2 style="font-size:14px;">Other Changes You Should Know About:</h2>
We've added a lot of services for the technicians. For example we now track background checks in the technician profiles
We also now offer direct deposit technician payment, delivering on our commitment to pay the techs faster. 
</p>
<p align="center" style="font-size:14px;">
<a href="dashboard.php">Continue</a>
</p>
</td>
	</tr>
</table>

</div>


<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
