<?php
// Created on: 6/20/2008 
// Author: JC
// Description: If the workorder is deactivated, the tech is notified of the change

//ini_set("display_errors", "stdout");

require_once("../library/caspioAPI.php");
require_once("../library/smtpMail.php");

/*

6/23/2008:

DONE: Send email to tech assigned to WO
DONE: Send email to all techs who bid on WO
TODO: Uncomment $techID line below BEFORE DEPLOYING!

1. email tech attached to work order
2. email all techs who bid on work order

*/

$TB_UNID = $_GET['ID'];
$techID = $_GET['techID'];



// FUNCTIONS

// FUNCTION process
// 
// for use when processing an array returned by caspioSelectAdv.
// trims the quotes off and filters "NULL" into ""
//
function process($str) {
	$output = trim($str, "`");
	switch ($str) {
		case "NULL":
			$output = "";
			break;
		case "True":
		case "Yes":
			$output = "Y";
			break;
		case "False":
		case "No":
			$output = "N";
			break;
	}
	return $output;
}


function techSubQuery($field) {
	return "(select top 1 $field from TR_Master_List where TR_Master_List.TechID = Work_Order_Applicants.TechID)";
}

// END FUNCTIONS


// GET INFO ABOUT WORK ORDER

if ($techID != "")
{
	
	// Get Work Order
	$woInfo = caspioSelectAdv("Work_Orders", "WO_ID, convert(varchar,StartDate,101), Project_Name, Project_ID, SiteName, StartTime, SiteNumber, City, State, Zipcode, Headline, ResourceCoordinatorName, ResourceCoordinatorPhone", "TB_UNID = $TB_UNID", "TB_UNID ASC", false, "`", "|");
	
	if (sizeof($woInfo) == 1 && $woInfo[0] != "") {
			$info = explode("|", $woInfo[0]);
			$woID = process($info[0]);
			$startDate = process($info[1]);
			$ProjectName = process($info[2]);
			$ProjectID = process($info[3]);
			$siteName = process($info[4]);
			$startTime = process($info[5]);
			$siteNum = process($info[6]);
			$city = process($info[7]);
			$state = process($info[8]);
			$zip = process($info[9]);
			$headline = process($info[10]);
			$rc = process($info[11]);
			$rcPhone = process($info[12]);
	}
	
//	echo "<p>ProjectID: $ProjectID</p>";
		
	// Get Project
	$woInfo = caspioSelectAdv("TR_Client_Projects", "From_Email, Project_Manager, Client_Email, Client_Name, Project_Manager, Project_Manager_Phone", "Project_ID = $ProjectID", "Project_ID ASC", false, "`", "|");
	
	if (sizeof($woInfo) == 1 && $woInfo[0] != "") {
			$info = explode("|", $woInfo[0]);
			$vFromEmail = process($info[0]);
			$vFromName = process($info[1]);
			$clientEmail = process($info[2]);
			$clientName = process($info[3]);
			$projectRC = process($info[4]);
			$projectRCphone = process($info[5]);
			
			if ($rc == ""){
				$rc = $projectRC;
				$rcPhone = $projectRCphone;
			}

	}
	
	// Get Tech Info
	$techInfo = caspioSelectAdv("TR_Master_List", "FirstName, LastName, PrimaryEmail", "TechID = $techID", "TechID", false, "`", "|");
	
	if (sizeof($techInfo) == 1 && $techInfo[0] != "") {
		$info = explode("|", $techInfo[0]);
		$firstName = process($info[0]);
		$lastName = process($info[1]);
		$primaryEmail = process($info[2]);
	}

			
	// Send Email to the Tech
	if ($clientEmail != "") {
	
		
		//$vFromName = "Field Solutions";
		//$vFromEmail = "jcoleman@fieldsolutions.com";
		
		$eList = $primaryEmail;
		
		$eList = "gbailey@fieldsolutions.com";
		
		$vSubject = "WIN# $TB_UNID Deactivated By Client";
		@$caller = "wosEmailWhenWODeactivated";
		$message = "
	This is notification that the below work order to which you had been assigned has been deactivated by the client:
	 
	Win#: $TB_UNID
	Client Work Order #: $woID
	Start Date: $startDate
	Site Name: $siteName
	Site Number: $siteNum
	City, State, ZipCode: $city, $state, $zip
	Headline: $headline
	 
	Client Contact Info:
	Coordinator:  $rc
	Telephone:  $rcPhone
	 
	This work order is no longer available to be viewed in the Field Solutions system. If you have any concerns, please call the client directly.
	
	Thank you,
	The Field Solutions Team";
				
		$htmlmessage = nl2br($message);
		
		// Emails Tech
		smtpMail($vFromName, $vFromEmail, $eList, $vSubject, $message, $htmlmessage, $caller);

//		echo "Email sent.";
	}
}


?>

<SCRIPT LANGUAGE="JavaScript"> 

	setTimeout('close()',100); 

</SCRIPT>