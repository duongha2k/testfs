<?php $page = login; ?>

<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>

<!-- Add Content Here -->

<script type="text/javascript">
	setTimeout("document.location = 'index.php'", 5000);
</script>

<br /><br />

<div style="width: 500px; margin: 0px auto 0px auto">
Thank you for your payment. Your transaction has been completed, and a receipt for your purchase has been emailed to you. You may log into your account at <a href="http://www.paypal.com/us" target="_blank">www.paypal.com/us</a> to view details of this transaction. You will be redirected to the login page shortly. Click <a href="index.php">here</a> to proceed to the login page.
</div>


<!--- End Content --->
<?php require ("../footer.php"); ?>