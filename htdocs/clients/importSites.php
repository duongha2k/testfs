<?php
header('Content-Type:text/html; charset=UTF-8');
	set_time_limit(9000);
	$page = "clients";
	$option = "wos";
	$selected = "importWO";
	$errorCount = 0;
	
	require_once("../header.php");
	require_once("../navBar.php");
	require_once("includes/PMCheck.php");
	require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');
//	require_once("../library/caspioAPI.php");
	$companyID = $_GET["v"];
//ini_set('display_errors',1);
	
?>

<style type="text/css">
	td { border:#385C7E solid medium; padding: 0px 5px; text-align: center; font-size:xx-small; }
	.errorCell { background-color: #FF8800; cursor: default; }
	#grayOutLoading {
		background-color: #999999;
		width: 100%;
		height: 100%;
		position: absolute;
		top: 0px;
		left: 0px;
		filter: alpha(opacity=50);
		opacity: 0.5;
		display: none;
	}
	#loadingMsg {
		margin: 25% auto;
		font-size: 20px;
		color: #FFFFFF;
	}
</style>

<div id="errorDiv" style="text-align: center; background-color: #FF0000; color: #FFFFFF; display: none; margin: 10px 0px; font-weight: bold;">Error</div>
<div style="text-align: center">
	<a href="importDocs/site_import_template.xls">Download Import Template</a>
    <form id="uploadImport" name="uploadImport" method="post" onsubmit="showLoadingMsg()" action="<?php echo $_SERVER['PHP_SELF']?>?v=<?=$_GET["v"]?>" enctype="multipart/form-data">
<?php
	$search = new Core_Api_Class;
	$projects = $search->getAllProjects(
		$_SESSION['UserName'].'|pmContext='.$companyID,
		$_SESSION['UserPassword']
	);
	$projects = $projects->data;
?>

        Select a project and upload your site list<br/>
	Project:
	<select title="" type="text" id="edit_Project_ID" name="Project_ID">
	<?php foreach( $projects as $prj ) :?>
    <?php if (!$prj->Active) continue;?>
		<option title="<?=$prj->Project_Name?>" value="<?=$prj->Project_ID?>" <?php if (isset($_REQUEST['Project_ID']) && $prj->Project_ID == $_REQUEST['Project_ID']) echo 'selected'; ?>><?=$prj->Project_Name?></option>
	<?php endforeach;?>
	</select>

        <input name="importFile" type="file" size="50" /><br /><br />
        <input name="submit" type="submit" value="Submit" /><br /><br />
    </form>
    <div id="grayOutLoading">
    	<div id="loadingMsg">
        	Loading ...
        </div>
    </div>
</div>
<?php
//	ini_set("display_errors", 1);
//	require_once("../library/importWOInclude_t2.php");
	
//	parseWOImport();


function processUpload() {
	$api = new Core_Api_Class;
	if (!isset($_POST['submit'])) return;
	if (!isset($_FILES["importFile"]["tmp_name"]) || !file_exists($_FILES["importFile"]["tmp_name"])) {
		echo "No file uploaded";
		return;
	}

	$inputFileName = $_FILES["importFile"]["tmp_name"];;
	$inputFileType = PHPExcel_IOFactory::identify($inputFileName);

	$objReader = PHPExcel_IOFactory::createReader($inputFileType);
	$objPHPExcel = $objReader->load($inputFileName);

	$row = $objPHPExcel->getActiveSheet()->getRowIterator();

	$columnMap = array(
		"Site Number" => "SiteNumber",
		"Site Name" => "SiteName",
		"Contact Name" => "Site_Contact_Name",
		"Address" => "Address",
		"City" => "City",
		"State" => "State",
		"Zipcode" => "Zipcode",
		"Country" => "Country",
		"Site Phone" => "SitePhone",
		"Site Fax" => "SiteFax",
		"Site Email" => "SiteEmail"
	);
	$indexRow = 0;
	$indexCol = 0;
	$columns = array();
	do {
	        $cell = $row->current()->getCellIterator();
		$cell->setIterateOnlyExistingCells(false);
		$site = new Core_Site();
		$indexCol = 0;
	        do {
			$cellValue = $cell->current()->getValue();
//			echo "$cellValue<br/>";
			if ($indexRow == 0 && array_key_exists($cellValue, $columnMap)) {
				$columns[$indexCol++] = $columnMap[$cellValue];
			}
			else if ($indexRow > 0) {
				$field = $columns[$indexCol];
				$site->$field = $cellValue;
				++$indexCol;
			}
	                $cell->next();
	        } while ($cell->valid());
		$site->Project_ID = $_POST['Project_ID'];
		if ($indexRow > 0 && trim($site->SiteNumber) == '') {
			echo "Skipping: No site number in row " . ($indexRow+1) . "<br/>";
		}
		else if ($indexRow > 0) {
			echo "Saving site: {$site->SiteNumber} ... ";
			try {
				$result = $api->updateSite(
					$_SESSION['UserName'].'|pmContext='.$_GET['v'],
					$_SESSION['UserPassword'],
					$site->toArray()
				);
			} catch (Exception $e) { //echo $e;
			}
			if ($result->success)
			echo "Done<br/>";
			else {
				echo "Error: ";
				foreach ($result->errors as $error) {
					echo $error->message . "<br/>";
				}
			}
			ob_flush();
			flush();
		}
//		if ($indexRow > 0) var_dump($site);
		++$indexRow;
	        $row->next();
	} while ($row->valid());
}

processUpload();

?>

<script type="text/javascript">
	function showLoadingMsg() {
		$("#grayOutLoading").show();
	}
	if (<?=$errorCount?> > 0)
		document.getElementById("errorDiv").style.display = "";
</script>
