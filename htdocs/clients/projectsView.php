<?php $page = clients; ?>
<?php $option = projects; ?>
<?php $selected = 'projectsView'; ?>
<?php require ("../header.php"); ?>

<?php 
    $Company_ID   = ( !empty($_GET['v']) ) ? $_GET['v'] : $_SESSION['Company_ID'];
    $UserType     = ( !empty($_SESSION['UserType']) ) ? $_SESSION['UserType'] : NULL;
    $isFLSManager = (strtolower($Company_ID) == 'fls' && (strtolower($UserType)==='install desk' || strtolower($UserType)==='manager'));
    if($isFLSManager){
        $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
    } else {
        $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
    }
?>
<?php require ("../navBar.php"); ?>
<?php require ("includes/adminCheck.php"); ?>
<?php
	$displayPMTools = true;
	require_once("includes/PMCheck.php");
/*
	require_once("../library/caspioAPI.php");
	$customerList = caspioSelectAdv("Customers", "UNID, Customer_Name", "Company_ID = '" . caspioEscape($_GET["v"]) . "'", "Customer_Name ASC", false, "\"", "^");
	$customerList = str_replace("\"", "", implode("|", $customerList));
	//require_once("../library/recruitmentEmailLib.php");

	$activeStatus = $_GET["Active"];
	echo "<script type=\"text/javascript\"> Active = " . $activeStatus . ";</script>";
*/	
?>
<!-- Add Content Here -->
<script type="text/javascript" src="../library/jquery/jquery.simplemodal-1.3.min.js"></script>
<style type="text/css">
	.optionBtns {
		width: 90%;
		text-align: center;
		margin: 0px auto;
	}
	#optionMessage {
		margin: 15px 0px 20px 0px;
		font-size: 15px;
		font-weight: bold;		
	}
	.optionBtns div {
		margin: 5px 0px;
	}
	
	.cbButton
	{
		/*Back Button Attributes*/
		color: #ffffff;
		font-size: 12px;
		font-family: Verdana;
		font-style: normal;
		font-weight: bold;
		text-align: center;
		vertical-align: middle;
		border-color: #5496C6;
		border-style: solid;
		border-width: 1px;
		background-color: #032D5F;
		/* Forced by default to add space between buttons */
		width: auto;
		height: auto;
		margin: 0 3px;
	}
	
	.cbButton:hover {
		background-color: #234D7F;
	}
</style>


<div align="center">

<br /><br />

<!-- update_projects_admin.js -->
<script type="text/javascript">
amount_per_selector_field_name = "select#EditRecordAmount_Per";
device_quantity_field_name = "input#EditRecordQty_Devices";
</script>

<!--<script type="text/javascript" src="/update_projects_admin.js"></script>-->
<?php script_nocache ("/update_projects_admin.js"); ?>
<!-- end update_projects_admin.js -->

<!--<script type="text/javascript" src="/widgets/js/FSWidgetManager.js"></script>-->
<?php script_nocache ("/widgets/js/FSWidgetManager.js"); ?>
<script type="text/javascript">
var wManager = new FSWidgetManager();
</script>

<!--<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>-->
<?php script_nocache ("/widgets/js/FSPopupAbstract.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>-->
<?php script_nocache ("/widgets/js/FSPopupRoll.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/ProgressBar.js"></script>-->
<?php script_nocache ("/widgets/js/ProgressBar.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/ProgressBarAdapterAbstract.js"></script>-->
<?php script_nocache ("/widgets/js/ProgressBarAdapterAbstract.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/ProgressBarAdapterPopupSpin.js"></script>-->
<?php script_nocache ("/widgets/js/ProgressBarAdapterPopupSpin.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>-->
<?php script_nocache ("/widgets/js/FSWidget.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/FSWidgetProject.js?04082011"></script>-->
<?php script_nocache ("/widgets/js/FSWidgetProject.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/FSWidgetSortTool.js"></script>-->
<?php script_nocache ("/widgets/js/FSWidgetSortTool.js"); ?>









<script type="text/javascript">
try {
	if (isPM) {
		var customerList = "<?=$customerList?>";
		/* build dropdown using project list */
		var element_dropdown = "EditRecordClient_ID";
		
		/* build dropdown using project list */
		$("#" + element_dropdown).empty();
		index = 1;
		selIndex = 0;
		$("#" + element_dropdown).append("<option value=\"\">Select Company</option>");
		
		var customers = customerList.split("|"); 
		for (i = 0; i < customers.length; i++) {  
			var parts = customers[i].split("^");  
			var customer_id = parts[0];  
			var customer_name = parts[1];  
			htmlEle = "<option value=\"" + customer_id + "\">" + customer_name + "</option>";
			$("#" + element_dropdown).append(htmlEle);
			if (selectedCID == customer_id)
				selIndex = index;
			index++;
		}
		document.getElementById(element_dropdown).selectedIndex = selIndex;
	}
}
catch (e) {}

</script>


<script>

var proj;           //  Widgets object
var roll;           //  Rollover object
var popup;          //  Rollover object
var progressBar;    //  Progress bar object


$(document).ready( function() {
	proj = new FSWidgetProject({container:'projectContainer',dbVersion:'<?=$dbVersion?>'});
    /* Find tag in URL params */

    //proj 		= new FSWidgetProject();
    roll        = new FSPopupRoll();
    progressBar = new ProgressBar();

    //proj.sortTool(new FSWidgetSortTool('sortSelect', 'directionSelect', proj, roll, 'quick'));

    /*  Load tab by parameters in URL */
    proj.setParams(/*proj.parseUrl()*/ {hideInactive: 'true', company: '<?=$_REQUEST['v'];?>'} );
    
    /*  Load tab by parameters in URL */
    progressBar.key('approve');
    progressBar.adapter(new ProgressBarAdapterPopupSpin(progressBar, null));

    proj.show({params: proj.getParams()});

    // add to manager
    wManager.add(proj, 'mainContainer');
    wManager._widgets.mainContainer = proj;

    $("#downloadButton").click(function(event){
        var opt = proj.getParams();
        opt['download']='csv';
        proj.open({
            tab     : 'list',
            params  : opt
        });
    });
});


function setStatus () {
    activeStatus = $("#SetActive").attr("checked");
    if (activeStatus != true)
	    activeStatus = "false";

	var opt=proj.getParams();
	opt.hideInactive = activeStatus;
	opt.page = 1;
	proj.setParams(opt);
	proj.show({tab:'list',params:opt});
}


</script>

<table border="0" cellpadding="3" cellspacing="3" width="500">
  <tbody><tr>
    <td nowrap="nowrap"><form id="ClientProjects" name="ClientProjects" method="post">
    <input name="SetActive" id="SetActive" onclick="setStatus()" type="checkbox" checked="checked">
    <label> Hide Inactive</label></form></td>
    <td nowrap="nowrap" width="5px">&nbsp;&nbsp;</td>

    <td nowrap="nowrap"><a href="" onclick="var opt=proj.getParams();opt.letter='';proj.setParams(opt);proj.show({tab:'list',params:opt});return false;">Show All (A-Z)</a></td>
    <td nowrap="nowrap" width="5px">&nbsp;&nbsp;</td>
    <td nowrap="nowrap" width="5px">Projects that start with:</td>
    <?php   foreach (range('A', 'Z') as $letter) :  ?>
        <td nowrap="nowrap"><a href="" onclick="var opt=proj.getParams();opt.letter='<?=$letter?>';opt.page = 1;proj.setParams(opt);proj.show({tab:'list',params:opt});return false;"><?=$letter?></a></td>
    <?php   endforeach;  ?>
	<td nowrap="nowrap"><a href="" onclick="var opt=proj.getParams();opt.letter='#';opt.page = 1;proj.setParams(opt);proj.show({tab:'list',params:opt});return false;">#</a></td>    
  </tr>
</tbody></table>



<script>


</script>
<input type="button" class="link_button download_button" value="Download" id="downloadButton" style="float: left;">
<br class="clear" />
<div id="tabbedMenu" style="height:16px;">
<table cellspacing='0' cellpadding='0' class="paginator_top"><tr><td class="tCenter" id="toppaginatorContainer"></td></tr></table>
</div>
<br class="clear" />

<div id="projectContainer" style="margin-left:auto; margin-right:auto; width:100%; height:100%;"></div>



<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
