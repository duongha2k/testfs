<?php
set_time_limit(600);
require_once("../headerStartSession.php");
require_once("../library/caspioAPI.php");
require_once("../library/projectTimeStamps.php");
require_once("../library/acsUpdateChanges.php");
require ("autoassign/includes/AutoAssign.php");
AutoAssign::$baseDir = dirname(__FILE__) . '/autoassign';

$id = $_GET["id"];
$user =isset($_SESSION['UserName'])?$_SESSION['UserName']:null;

require_once("{$_SERVER['DOCUMENT_ROOT']}/library/bootstrapAPI.php");
Core_Database_CaspioSync::sync(TABLE_CLIENT_PROJECTS, "Project_ID", array($id));

$choice = 0;
$numUpdated = 0;
if (isset($_GET["choice"])) $choice = $_GET["choice"];

if (is_numeric($id) && is_numeric($choice)) {
    if ($choice == 1) {
        //Projects should be pulled from MySQL
        $login = isset($_SESSION['UserName'])?$_SESSION['UserName']:null;
	if ($_SESSION['Auth_User']['isPM']) $login .= '|pmContext=' . $_SESSION['PM_Company_ID'];
        $password = isset($_SESSION['UserPassword'])?$_SESSION['UserPassword']:null;

        if (!$login ||!$password) throw new Exception('Auth required');
        $apiClass = new Core_Api_Class();
        $projectInfo = $apiClass->getProjects($login,$password,array($id),'');
        if ($projectInfo->success && !empty($projectInfo->data[0])) {
            $projectInfo = $projectInfo->data[0];
            //UPDATE WORK ORDERS
            $db = Zend_Registry::get('DB');
            $criteria = "Project_ID = '".$projectInfo->Project_ID."' AND IFNULL(Tech_ID, '') = '' AND Deactivated = '0'";
            $sql = "SELECT WIN_NUM FROM work_orders WHERE $criteria AND ShowTechs = 1";
            $woList = $db->fetchAll($sql, array(), Zend_Db::FETCH_NUM);
            $numUpdated = 0;
            if ($woList && $woList[0] != "") {
                $fields = array("isProjectAutoAssign" => (bool)$projectInfo->isProjectAutoAssign,
                                "isWorkOrdersFirstBidder" => (bool)$projectInfo->isWorkOrdersFirstBidder,
                                "P2TPreferredOnly" => (bool)$projectInfo->P2TPreferredOnly,
                                "MinutesRemainPublished" => $projectInfo->MinutesRemainPublished,
                                "MinimumSelfRating" => $projectInfo->MinimumSelfRating,
                                "MaximumAllowableDistance" => $projectInfo->MaximumAllowableDistance);
                $numUpdated = $db->update('work_orders', $fields, $criteria);
				$pcs = new Core_ProjectCertifications();
				$certs = $pcs->getProjectCertification(array($id));
                foreach ($woList as $unid) {
                    if (empty($unid)) continue;
					$unid = $unid[0];

					$pws = new Core_WorkOrderCertifications();
					$certswo = $pws->getWorkOrderCertification($unid);
			
					if (!empty($certswo[$unid])) {
						// delete certifications removed by update
						$diff = array_diff($certs[$id], $certswo[$unid]);
						foreach ($diff as $cid) {
							$pc = new Core_WorkOrderCertification($unid, $cid);
							$pc->delete();
						}
					}

					if (!empty($certs[$id])) {
						foreach ($certs[$id] as $v) {
							$pc = new Core_WorkOrderCertification($unid, $v);
							$pc->save();
						}
					}

                    createWOACSTimeStamp($unid);
                    if ((bool)$projectInfo->isProjectAutoAssign)
                        AutoAssign::proccessAutoAssign($unid);
                    else
                        AutoAssign::removeShellFileIfExist($unid);
                }
            }
        }
    }
    $numUpdatedACS = acsUpdateChanges($id, $user, false, $choice);
}
?>
