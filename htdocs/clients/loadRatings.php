<?php	
	ini_set("auto_detect_line_endings", 1); // Detect Mac line endings
	require_once("../library/caspioAPI.php");
	require_once("../importInclude.php");
	
	$requiredFields = array();
	
	$requiredFields["TechID"] = false;
	$requiredFields["FName"] = false;
	$requiredFields["LName"] = false;
	$requiredFields["RankingSource"] = false;
	$requiredFields["ContactName"] = false;
	$requiredFields["OverallRanking"] = false;

	$columns = caspioGetTableDesign("TR_Tech_Rankings", FALSE);

	$tableColumns = array();
	foreach ($columns as $info) {
		$fieldInfo = explode(",",$info);
		$fieldName = $fieldInfo[0];
		$fieldType = $fieldInfo[1];
		$fieldUnique = $fieldInfo[2];
		$tableColumns[$fieldName] = new caspioFieldProperties($fieldType, $fieldUnique);
	}

	if (isset($_GET["template"])) {
		// download template
		header("Content-disposition: attachment; filename=TechRatingTemplate.csv");
		echo "TechID, FName, LName, RankingSource, ContactName, GeneralComments, OverallRanking";
		foreach ($tableColumns as $key=>$val) {
			if (array_key_exists($key, $requiredFields) || $key == "RankingDate" || $key == "GeneralComments") continue;
			echo ", $key";
		}
		echo "\n";
		die();
	}
?>
<?php $page = clients; ?>
<?php $option = techs; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<style type="text/css">
	td { border:#385C7E solid medium; padding: 0px 5px; text-align: center; font-size:xx-small; }
	.errorCell { background-color: #FF8800; cursor: default; }
</style>

<!--<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script> <script type="text/javascript" language="javascript">try{f_cbload("193B0000D3D2C0H0E2D3D2C0H0E2","https:");}catch(li){;}</script> <div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193B0000D3D2C0H0E2D3D2C0H0E2">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>

<script type="text/javascript">
	try {
		var test = clientID;
	} catch (e) {
		document.location.replace("./");
	}
</script>-->

<?php

	if (isset($_POST["submit"])) {		
		if (!isset($_FILES["importFile"]["tmp_name"]) || !file_exists($_FILES["importFile"]["tmp_name"])) {
			echo "No file Uploaded";
			die();
		}
	
		$columns = caspioGetTableDesign("TR_Tech_Rankings", FALSE);

		$tableColumns = array();
		foreach ($columns as $info) {
			$fieldInfo = explode(",",$info);
			$fieldName = $fieldInfo[0];
			$fieldType = $fieldInfo[1];
			$fieldUnique = $fieldInfo[2];
			$tableColumns[$fieldName] = new caspioFieldProperties($fieldType, $fieldUnique);
		}
		
		if ($_FILES["importFile"]["size"] > 150000) {
			// file too large
			
		}
			
		$handle = fopen($_FILES["importFile"]["tmp_name"], "r");
		$importColumns = fgetcsv($handle);
		
		$importColumns[] = "RankingDate";
		
		$importColumnNum = sizeof($importColumns);
		$templateError = false;
		
		$msg = "";
		
		for ($i = 0; $i < $importColumnNum; $i++) {
			// match import file columns to table columns and clean-up import column names
			$importColumns[$i] = trim($importColumns[$i]);
			$name = $importColumns[$i];
			
			if (array_key_exists($name, $requiredFields)) {
				$requiredFields[$name] = 1;
			}
			if (!array_key_exists($name, $tableColumns)) {
				// unknown column
				$msg = "Unrecognized column: '$name'<br/>";
				$templateError = true;
				break;
			}
		}
		
		if (!$templateError) {
			foreach ($requiredFields as $key => $found) {
				// check for missing required columns
				if ($found == 1) continue;
				$msg = "Required column missing: '$key'<br/>";
				$templateError = true;
				break;
			}
		}
				
		if (!$templateError) {
			// begin check of data
			$today = date("m/d/Y");
			$currRow = 1;
			$importTableByColumn = array();
			while (!feof($handle)) {
				// parse values
				if ($msg != "") break; // exit if there's an error

				$importValues = fgetcsv($handle);
				if (sizeof($importValues) == 1) continue; // ignore blank row

				$importValuesNum = sizeof($importValues);
				
				$row = array();
				for ($i = 0; $i < $importColumnNum; $i++) {
					if ($i >= $importValuesNum) continue; // number columns more then number of values
					$column = $tableColumns[$importColumns[$i]];
					if (!$column->checkValue($importValues[$i])) {
						// verify data type correctness
						$msg = "Row $currRow: '{$importValues[$i]}' is not a valid value for the '{$importColumns[$i]}' column. This column requires a " . $column->getType() . " value.<br/>";
						break;
					}
					if (array_key_exists($importColumns[$i], $requiredFields) && trim($importValues[$i]) == "") {
						// check for blank required fields
						$msg = "Line $currRow: The '{$importColumns[$i]}' field is required.<br/>";
						break;
					}
					$row[$importColumns[$i]] = $importValues[$i];
				}
				$row["RankingDate"] = $today;
				$importTableByColumn[] = $row;
				$currRow++;	
			}
			
			if ($msg == "") {
				$numRows = sizeof($importTableByColumn);
				if ($numRows == 0)
					$msg = "The csv file doesn't contain any data to load.<br/>";
			}
						
			if ($msg == "") {
				// build list of TechID and check for multiple names for identical TechID
				$techIDMap = array();
	
				for ($i = 0; $i < $numRows; $i++) {
					$techID = $importTableByColumn[$i]["TechID"];
					$fName = $importTableByColumn[$i]["FName"];
					$lName = $importTableByColumn[$i]["LName"];
					if (!array_key_exists($techID, $techIDMap))
						$techIDMap[$techID] = array($fName, $lName);
					else {
						if ($fName != $techIDMap[$techID][0] || $lName != $techIDMap[$techID][1]) {
							$msg = "The csv file contains multiple names for TechID '$techID'<br/>";
							break;
						}
					}
				}
			}
						
			if ($msg == "") {
				// make sure First and Last names for tech matches what's in the database.
				
				$techList = "";
				foreach ($techIDMap as $techID=>$val) {
					$techList .= ($techList != "" ? ", " : "") . "'$techID'";
				}
				
//				echo "SELECT TechID, FirstName, LastName FROM TR_Master_List WHERE TechID IN ($techList)<br/>";
							
				$list = caspioSelectAdv("TR_Master_List", "TechID, FirstName, LastName", "TechID IN ($techList)", "", false, "`", "|");
				
				$numValidTechID = sizeof($list);
				
				if (sizeof($info) == 1 && $info[0] == "") {
					// no matching IDs returned
					$msg = "Unable to match any TechID from the csv file.<br/>";
				}
				else {
					foreach ($list as $info) {
						$parts = explode("|", $info);
						$techID = trim($parts[0], "`");
						$fName = trim($parts[1], "`");
						$lName = trim($parts[2], "`");
						if ($techIDMap[$techID][0] != $fName || $techIDMap[$techID][1] != $lName) {
							$msg .= "The tech first and last name for TechID '$techID' ({$techIDMap[$techID][0]}, {$techIDMap[$techID][1]}) doesn't match the tech's record from the database. ($fName, $lName)<br/>";
							break;
						}
					}
				}
			}
			
			if ($msg == "") {
				// insert ratings
				$insertValues = array();
				for ($i = 0; $i < $numRows; $i++) {
					$insertRow = "";
					foreach ($importColumns as $column) {
						$type = $tableColumns[$column]->getType();
						$value = ($type == "ShortText" || $type == "LongText" ? caspioEscape($importTableByColumn[$i][$column]) : $importTableByColumn[$i][$column]);
						$insertRow .= ($insertRow != "" ? ", " : "") . (!array_key_exists($column, $importTableByColumn[$i]) ? "''" : "'$value'");
					}
					$insertValues[] = $insertRow;
				}
				
				$columns = implode(", ", $importColumns);
				foreach ($insertValues as $values) {
//					echo "INSERT INTO TR_Tech_Rankings (" . $columns . ") VALUES ($values)<br/>";
					caspioInsert("TR_Tech_Rankings_TN", $columns, $values);
				}
				$msg = sizeof($insertValues) . " ratings added.<br/>";
			}
		}		
	}
	
?>

<form name="uploadImport" method="post" action="<?php echo $_SERVER['PHP_SELF']?>" enctype="multipart/form-data">
	<div style="text-align: center;">
		<br/>
		<?=$msg?>
		<?php 
			echo "Click <a href=\"{$_SERVER['PHP_SELF']}?template=1\">here</a> to download the import template.<br/>";
		?>
		<br/>
	</div>
	<div style="text-align: center;">
		Ratings File (csv format):
		<input name="importFile" type="file" /><br/> <br/>
		<input name="submit" type="submit" value="Submit" />
	</div>
</form>

<!--- End Content --->
<?php require ("../footer.php"); ?>
