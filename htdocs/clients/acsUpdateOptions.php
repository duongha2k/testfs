<?php
	require_once("../headerSimple.php");
?>
<style type="text/css">
	#optionBox, #waitMessageBox {
		width: 90%;
		text-align: center;
		margin: 0px auto;
	}
	#optionMessage, #waitMessage {
		margin: 90px 0px 20px 0px !important;
		font-size: 15px;
		font-weight: bold;		
	}
	#optionBox div, #waitMessageBox div {
		margin: 5px 0px;
	}
	
	.cbButton
	{
		/*Back Button Attributes*/
		color: #ffffff;
		font-size: 12px;
		font-family: Verdana;
		font-style: normal;
		font-weight: bold;
		text-align: center;
		vertical-align: middle;
		border-color: #5496C6;
		border-style: solid;
		border-width: 1px;
		background-color: #032D5F;
		/* Forced by default to add space between buttons */
		width: auto;
		height: auto;
		margin: 0 3px;
	}
	
	.cbButton:hover {
		background-color: #234D7F;
	}
</style>
<script type="text/javascript">
	var waitingForUpdate = false;
	var choice = 0;
	var checkUpdateTimer = null;
	var oldSDT = opener.serverDateTime;
	$(document).ready(function() {
		focus();
	});
	function submitProjectChanges() {
		try {
			waitingForUpdate = true;
			opener.focus();
			opener.allowSubmit = true;
			opener.$("#caspioform").submit();
		}
		catch (e) {
			alert(e.message);
			window.close();
		}
	}
	function chooseOption(option) {
		choice = option;
		opener.$("#EditRecordACSApplyTo").attr("value", choice);
		submitProjectChanges();
		$("#optionBox").hide();
		$("#waitMessageBox").show();
		checkUpdateTimer = setInterval("checkUpdateFinished()", 50);
	}
	function closeIfNotSubmitting() {
		if (!waitingForUpdate)
			window.close();
	}
	function checkUpdateFinished() {
		try {
			if (oldSDT == opener.serverDateTime) return; // havan't loaded new page yet
			if (!(test = opener.projectViewResults))
				if (test = opener.projectViewDetails) {
					// form error
					window.close();
					return;
				}
				else
					return;
			clearInterval(checkUpdateTimer);
		} catch (e) {
		}
		updateFinished();
	}
	function updateFinished() {
		window.location.replace("acsUpdateProjectWOs.php?id=<?=$_GET["id"]?>&choice=" + choice + "&user=<?=$_GET["user"]?>");
	}
</script>
    <body>
        <div id="optionBox">
            <div id="optionMessage">How would you like to apply ACS options changes?</div>
			<div><input type="button" value="All Work Orders" onClick="chooseOption(1)" class="cbButton"/></div>
			<div><input type="button" value="Unassigned and New Work Orders" onClick="chooseOption(2)"  class="cbButton"/></div>
			<div><input type="button" value="New Work Orders Only" onClick="chooseOption(3)" class="cbButton"/></div>
			<div><input type="button" value="Cancel" onClick="window.close()" class="cbButton"/></div>
	    </div>
        <div id="waitMessageBox" style="display:none">
            <div id="waitMessage">
            	Updating ... please wait.<br/><br/>
                <img src="../images/loading.gif"/>
            </div>
        </div>
    </body>
</html>