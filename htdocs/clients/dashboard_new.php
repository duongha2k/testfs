<?php 
$page   = 'clients';
$option = 'dashboard';
require ("../headerStartSession.php");

$siteTemplate = $_SESSION['template'];
if ($siteTemplate == "ruo") {
    require ("../templates/ruo/includes/header.php");
} else {
    require ("../header.php");
}

$loggedIn   = $_SESSION['loggedIn'];
$Company_ID = $_SESSION['Company_ID'];

if($loggedIn!="yes"){
// Redirect to logIn.php
header( 'Location: /clients/logIn.php' ) ;
}

$toolButtons = array('FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
require '../navBar.php';
require 'includes/adminCheck.php';
require 'includes/PMCheck.php';

/*** Add Content Here ***/
//if ($siteTemplate == "ruo") {
    //require ("../templates/ruo/includes/client_dashboardContent.php");
//} else {
    //require ("dashboardContent.php");
//}
?>

<link rel=stylesheet type="text/css" href="/widgets/css/main.css" />
<link rel="stylesheet" type="text/css" href="/widgets/css/popup.css" />
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script> <script type="text/javascript" language="javascript">try{f_cbload("193B0000D3D2C0H0E2D3D2C0H0E2","https:");}catch(li){;}</script> <div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193B0000D3D2C0H0E2D3D2C0H0E2">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>
<!--<script type="text/javascript" src="/widgets/js/jquery.min.js"></script>-->
<script type="text/javascript" src="/widgets/js/jquery.form.js"></script>

<script type="text/javascript">
    window._company = '<?=$_GET['v']?>';
    //  use for load images and save it in browser cache only!!!
    var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
    var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";
</script>

<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetDashboard.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetDMain.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetManager.js"></script>
<script type="text/javascript" src="/widgets/js/FSFindWorkOrder.js"></script>
<script type="text/javascript" src="/widgets/js/FSQuickAssignTool.js"></script>
<script type="text/javascript" src="/widgets/js/FSTechPayUtils.js"></script>
<script type="text/javascript" src="/widgets/js/ProgressBar.js"></script>
<script type="text/javascript" src="/widgets/js/ProgressBarAdapterAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/ProgressBarAdapterPopupSpin.js"></script>

<script type="text/javascript">
    var pmContext = "<?=$_GET["v"]?>";
    var roll, wd;
    var wManager;                           //  Widget's manager
    var findWorkOrder;                      //  Find WO Popup
    var progressBar;                        //  Progress bar object
    var assignTool;                         //  Assign Popup
    var topInnerRight, middleInnerRight, leftInnerBottomRight, rightInnerBottonRight, leftVerticalContainer;

    
    $(document).ready(function() {
        wManager                = new FSWidgetManager();
        topInnerRight           = new FSWidgetDMain({container:'topInnerRight',         tab:'published'});
        middleInnerRight        = new FSWidgetDMain({container:'middleInnerRight',      tab:'assigned'});
        leftInnerBottomRight    = new FSWidgetDMain({container:'leftInnerBottomRight',  tab:'workdonesmall'});
        rightInnerBottonRight   = new FSWidgetDMain({container:'rightInnerBottonRight', tab:'alertemails'});
        leftVerticalContainer   = new FSWidgetDMain({container:'leftVerticalContainer', tab:'navigation'});

        wManager.add(topInnerRight,         'published');
        wManager.add(middleInnerRight,      'assigned');
        wManager.add(leftInnerBottomRight,  'workdone');
        wManager.add(rightInnerBottonRight);
        wManager.add(leftVerticalContainer);

        topInnerRight.show();
        middleInnerRight.show();
        leftInnerBottomRight.show();
        rightInnerBottonRight.show();
        leftVerticalContainer.show();

        //alias for create wd, simply it is any of FSWidget child
        wd              = new FSWidgetDashboard({container:'widgetContainer',tab:'workdone'});  //  Using old url routes
        roll            = new FSPopupRoll();

        findWorkOrder   = new FSFindWorkOrder(wd, roll);
        assignTool      = new FSQuickAssignTool( wd );
        progressBar     = new ProgressBar();

        findWorkOrder.buildHtml();

        progressBar.key('approve');
        progressBar.adapter(new ProgressBarAdapterPopupSpin(progressBar, null));

        /* Make buttons functionality */
        $("#FindWorkOrderBtn").click(function(event) {
            roll.autohide(false);
            var opt = {
                width       : 325,
                height      : '',
                title       : 'Find a Work Order',
                handler		: findWorkOrder.prepare,
                context     : wd,
                body        : $(findWorkOrder.container()).html()
            };
            roll.showNotAjax(this, event, opt);
        });
        $("#CreateWorkOrderBtn").click(function(event) {
        	openPopup('/clients/createWO.php?v=<?php echo $_GET["v"]?>');
        });
        $("#QuickAssignBtn").click(function(event) {
            roll.autohide(false);
            var opt = {
                popup       : 'quickassigntool',
                title       : 'Quick Assign',
                width       : 400,
                preHandler  : assignTool.getWidgetHtml,
                postHandler : assignTool.initEvents,
                context     : assignTool,
                delay       : 0
            }
            roll.show(this,event,opt);

        });
        $("#QuickApproveBtn").click(function(event) {
            roll.autohide(false);
            var opt = {
                popup       : 'quickapprove',
                title       : 'Quick Approve',
                width       : 400,
                context     : wd,
                preHandler  : wd.getWidgetHtml,
                delay       : 0,
                postHandler : function() {
                	$("#Approve_approveForm").ajaxForm({dataType: 'json', success: function(data) {
                        if (data.error == 0) {
                            roll.hide();
                            topInnerRight.show();
                            middleInnerRight.show();
                            leftInnerBottomRight.show();
                        } else {
                            $('#woErrorMsg').html(data.msg);
                            $('#woErrorMsg').show('fast',function(){
                                setTimeout(function () {  $('#woErrorMsg').hide(500); }, 3000);
                            });
                        }
    				}, beforeSubmit : function() {
    			        progressBar.key('updatepayfinan');
    			        progressBar.run();
    			        return true;
    				}});
    			}
            }
            roll.show(this,event,opt);
        });
        /* End buttons */
    });

    function gotoTab(name) {
        location = location.toString().replace('dashboard.php', 'wos.php') + '&tab=' + name;
        return false;
    }
    function toggleNavBar( btn )
    {
        var bar = $('#leftVerticalContainer');
        var content = $('#rightVerticalContainer');
        btn = $(btn);

        switch( btn.text() ) {
            case '<<' :
                btn.text('>>');
                btn.parent().css('width', '100%');
                bar.css('display', 'none');
                bar.parent().css('width', '2%');
                bar.parent().css('background', '');
                content.css('width', '97%');
                break;
            case '>>' :
                btn.text('<<');
                btn.parent().css('width', '18%');
                bar.css('display', '');
                bar.parent().css('width', '18%');
                bar.parent().css('background', 'transparent url(/widgets/images/border_right.png) no-repeat scroll right top');
                content.css('width', '81%');
                break;
        }
        return false;
    }
    function applyProgectFilter(select)
    {
        wManager.get('published').filters().setProject(select.value);
        wManager.get('assigned').filters().setProject(select.value);
        wManager.get('workdone').filters().setProject(select.value);
        wManager.get('published').show();
        wManager.get('assigned').show();
        wManager.get('workdone').show();
        return false;
    }
</script>

<!-- Main content -->
<div class="inner10 clr indentTop landing">
    <div class="left_col">
		<div id="leftVerticalContainer" class="fl" style="width: 80%"></div>
		<div class="fr" style="width: 18%"><button onclick="return toggleNavBar(this);">&lt;&lt;</button></div>
	</div>
    <div id="rightVerticalContainer" class="right_col">
        <div class="gray_bg">
            <h3 class="pseudolink" onclick="return gotoTab('published');">My Published Work Orders (Not Yet Assigned)</h3>
        </div>
        <div id="topInnerRight"></div>
        <div class="blue_bg">
            <h3 class="pseudolink" onclick="return gotoTab('assigned');">My Assigned Work Orders</h3>
        </div>
        <div id="middleInnerRight"></div>
        <div>
		<table class="per50" cellspacing=0 cellpadding=0 width="100%">
			<col width=50% />
			<col width=50% />
            <thead>
                <tr>
                    <td class="borright gray_bg">
                        <h3 class="pseudolink" onclick="return gotoTab('workdone');">Waiting For Approval</h3>
                        (needs action: approve to pay tech)
                    </td>
				    <td class="gray_bg">
                        <h3 class="pseudolink" onclick="">Alert Emails</h3>
                    </td>
                </tr>
            </thead>
			<tr>
                <td class="borright">
                    <div id="leftInnerBottomRight"></div>
                </td>
                <td>
                    <div id="rightInnerBottonRight"></div>
                </td>
			</tr>
		</table>
        </div>
    </div>
</div>
<!-- Hidden container for wd; wd role is a transport html tool ONLY -->
<div id="widgetContainer" style="display:none;"></div>

<!-- End Main content -->


<?php
/*** End Content ***/
require ("../footer.php"); 
?>

