<?php
set_time_limit(300);
require_once("../headerStartSession.php");
require_once("../library/caspioAPI.php");
require_once("../library/projectTimeStamps.php");
require_once("../library/woACSTimeStamps.php");
require ("autoassign/includes/AutoAssign.php");
AutoAssign::$baseDir = dirname(__FILE__) . '/autoassign';

$id = $_REQUEST["id"];
$prjoldInfo = $_REQUEST["prj"];
$prjoldInfo = json_decode($prjoldInfo);
$prjoldInfo = (object)$prjoldInfo;


$prjexpenInfo = $_REQUEST["expen"];
$prjexpenInfo = json_decode($prjexpenInfo);
$prjexpenInfo = (object)$prjexpenInfo;

$user = isset($_SESSION['UserName']) ? $_SESSION['UserName'] : null;

require_once("{$_SERVER['DOCUMENT_ROOT']}/library/bootstrapAPI.php");
Core_Database_CaspioSync::sync(TABLE_CLIENT_PROJECTS, "Project_ID", array($id));

$user = isset($_SESSION['UserName']) ? $_SESSION['UserName'] : null;
if ($_SESSION['Auth_User']['isPM'])
    $user .= '|pmContext=' . $_SESSION['PM_Company_ID'];
$password = isset($_SESSION['UserPassword']) ? $_SESSION['UserPassword'] : $_SESSION['Password'];
if (empty($password)) $password = null;

if (!$user || !$password)
    throw new Exception('Auth required');

$apiClass = new Core_Api_Class();
try {
	$projectInfo = $apiClass->getProjects($user, $password, array($id), '');
} catch (Exception $e) {
	// unable to get project info, stop the update
	die();
}

if ($projectInfo->success && !empty($projectInfo->data[0]))
    $projectInfo = $projectInfo->data[0];
    

$Core_Api_ProjectClass  = new Core_Api_ProjectClass();
$ExpenseReporting = $Core_Api_ProjectClass->getProjectBCats_insertFromClientIfNotExists($projectInfo->Project_ID);

$choice = 0;
$numUpdated = 0;

if (isset($_REQUEST["choice"]))
    $choice = $_REQUEST["choice"];

if (is_numeric($id) && is_numeric($choice))
{
    if(!empty($prjoldInfo))
    {
		$prjOldInfo["SignOff_Disabled"] = !isset($prjOldInfo["SignOff_Disabled"]);

        //p2t
        $numUpdatedP2T = p2tUpdateChanges($id, $user, $password, $choice, $projectInfo, $prjoldInfo);
        //other
        $numUpdatedother = otherUpdateChanges($id, $user, $choice, $projectInfo, $prjoldInfo, $ExpenseReporting, $prjexpenInfo);
        //asc
        $numUpdatedACS = acsUpdateChanges($id, $user, $choice, $prjoldInfo);
    }

}

function p2tUpdateChanges($id, $user, $password, $choice, $projectInfo, $projectoldInfo)
{
    if ($choice != 3)
    {
        //UPDATE WORK ORDERS
        $db = Zend_Registry::get('DB');
        $criteria = makecriteria($id, $choice, 1);
        $sql = "SELECT WIN_NUM FROM work_orders WHERE $criteria";
        $woList = $db->fetchAll($sql, array(), Zend_Db::FETCH_NUM);
        $numUpdated = 0;
        
        if ($woList && $woList[0] != "")
        {
			$Core_Api_ProjectClass  = new Core_Api_ProjectClass();
			$cert_requirements = $Core_Api_ProjectClass->loadP2TCertRequirements ($id);

			if (Core_Api_ProjectClass::countP2TCertRequirements($cert_requirements) > 0) {
				foreach ($woList as $win_num) {
					$Core_Api_ProjectClass->saveP2TCertRequirements ($win_num[0], $cert_requirements, true, false);
				}
			}

            if($projectoldInfo->isProjectAutoAssign != $projectInfo->isProjectAutoAssign ||
                $projectoldInfo->isWorkOrdersFirstBidder != $projectInfo->isWorkOrdersFirstBidder ||
                $projectoldInfo->P2TPreferredOnly != $projectInfo->P2TPreferredOnly ||
                $projectoldInfo->MinutesRemainPublished != $projectInfo->MinutesRemainPublished ||
                $projectoldInfo->MinimumSelfRating != $projectInfo->MinimumSelfRating ||
                $projectoldInfo->MaximumAllowableDistance != $projectInfo->MaximumAllowableDistance)
            {
                $fields = array("isProjectAutoAssign" => (bool) $projectInfo->isProjectAutoAssign,
                    "isWorkOrdersFirstBidder" => (bool) $projectInfo->isWorkOrdersFirstBidder,
                    "P2TPreferredOnly" => (bool) $projectInfo->P2TPreferredOnly,
                    "MinutesRemainPublished" => $projectInfo->MinutesRemainPublished,
                    "MinimumSelfRating" => $projectInfo->MinimumSelfRating,
                    "MaximumAllowableDistance" => $projectInfo->MaximumAllowableDistance);
                $numUpdated = $db->update('work_orders', $fields, $criteria);

                $pcs = new Core_ProjectCertifications();
                $certs = $pcs->getProjectCertification(array($id));
                foreach ($woList as $unid)
                {
                    if (empty($unid))
                        continue;
                    $unid = $unid[0];

                    $pws = new Core_WorkOrderCertifications();
                    $certswo = $pws->getWorkOrderCertification($unid);

                    if (!empty($certswo[$unid]))
                    {
                        // delete certifications removed by update
                        $diff = array_diff($certs[$id], $certswo[$unid]);
                        foreach ($diff as $cid)
                        {
                            $pc = new Core_WorkOrderCertification($unid, $cid);
                            $pc->delete();
                        }
                    }

                    if (!empty($certs[$id]))
                    {
                        foreach ($certs[$id] as $v)
                        {
                            $pc = new Core_WorkOrderCertification($unid, $v);
                            $pc->save();
                        }
                    }

                    createWOACSTimeStamp($unid);
                    
                    if ((bool) $projectInfo->isProjectAutoAssign)
                        AutoAssign::proccessAutoAssign($unid);
                    else
                        AutoAssign::removeShellFileIfExist($unid);
                    
                }
			}
        }
    }
}

//update asc
function acsUpdateChanges($id, $user, $choice, $projectoldInfo)
{
    if ($choice != 3)
    {
        $projectInfo = createProjectTimeStamp($id, $user);
        if(!empty($projectInfo))
        {
            if($projectoldInfo->ReminderAll != $projectInfo["reminderAll"] ||
                    $projectoldInfo->ReminderAcceptance != $projectInfo["reminderAcceptance"] ||
                    $projectoldInfo->Reminder24Hr != $projectInfo["reminder24Hr"] ||
                    $projectoldInfo->Reminder1Hr != $projectInfo["reminder1Hr"] ||
                    $projectoldInfo->CheckInCall != $projectInfo["checkInCall"] ||
                    $projectoldInfo->CheckOutCall != $projectInfo["checkOutCall"] ||
                    $projectoldInfo->ReminderNotMarkComplete != $projectInfo["reminderNotMarkComplete"] ||
                    $projectoldInfo->ReminderIncomplete != $projectInfo["reminderIncomplete"] ||
                    $projectoldInfo->AutoSMSOnPublish != $projectInfo["SMSBlast"])
            {
                $reminderAll = $projectInfo["reminderAll"];
                $reminderAcceptance = $projectInfo["reminderAcceptance"];
                $reminder24Hr = $projectInfo["reminder24Hr"];
                $reminder1Hr = $projectInfo["reminder1Hr"];
                $checkInCall = $projectInfo["checkInCall"];
                $checkOutCall = $projectInfo["checkOutCall"];
                $reminderNotMarkComplete = $projectInfo["reminderNotMarkComplete"];
                $reminderIncomplete = $projectInfo["reminderIncomplete"];
                $SMSBlast = $projectInfo["SMSBlast"];
                $applyTo = $projectInfo["applyTo"];
                $applyTo = trim($applyTo, "'");
                $applyTo = $applyTo == "DEFAULT" ? "" : $applyTo;
                
                if ($choice != $applyTo)
                    die();

                $criteria = makecriteria($id, $choice, 2);

                $db = Zend_Registry::get('DB');
                $sql = "SELECT WIN_NUM FROM work_orders WHERE $criteria";

                $woList = $db->fetchAll($sql, array(), Zend_Db::FETCH_NUM);
                if (count($woList) > 0)
                {
                    $fields = array("ReminderAll" => $reminderAll,
                        "ReminderAcceptance" => $reminderAcceptance,
                        "Reminder24Hr" => $reminder24Hr,
                        "Reminder1Hr" => $reminder1Hr,
                        "CheckInCall" => $checkInCall,
                        "CheckOutCall" => $checkOutCall,
                        "ReminderNotMarkComplete" => $reminderNotMarkComplete,
                        "ReminderIncomplete" => $reminderIncomplete,
                        "SMSBlast" => $SMSBlast);

                    $numUpdated = $db->update('work_orders', $fields, $criteria);

                    foreach ($woList as $wo)
                    {
                        $unid = $wo[0];
                        if ($unid == "")
                            continue;
                        createWOACSTimeStamp($unid);
                    }
                }
            }
        }
    }
    return $numUpdated;
}

function otherUpdateChanges($id, $user, $choice, $projectInfo, $projectoldInfo, $ExpenseReporting, $prjexpenInfo)
{
    if ($choice != 3)
    {
    	$db = Zend_Registry::get('DB');
    	
        $criteria = makecriteria($id, $choice, 3);
        $sql = "SELECT WIN_NUM FROM work_orders WHERE $criteria"; 
        $woList = $db->fetchAll($sql, array(), Zend_Db::FETCH_NUM);
         
         
        if (count($woList) > 0)
        {
			$updateSql = "UPDATE work_orders AS w LEFT JOIN future_wo_info AS f ON (1=" . $db->quoteInto("?", $projectInfo->AllowCustomCommSettings) . " AND w.WorkOrderOwner = f.ClientUserName) SET ";
            
        	$updateSets = array();
            $fields = array();
            if($projectoldInfo->PO != $projectInfo->PO) $updateSets[] = "w.PO = '".$projectInfo->PO."'"; //$fields["PO"] = $projectInfo->PO;
            if($projectoldInfo->Project_Manager != $projectInfo->Project_Manager) $updateSets[] = "w.ProjectManagerName = (CASE WHEN f.ProjectManagerName IS NULL THEN '".$projectInfo->Project_Manager."' ELSE w.ProjectManagerName END) "; //$fields["ProjectManagerName"] = $projectInfo->Project_Manager;
            if($projectoldInfo->Project_Manager_Email != $projectInfo->Project_Manager_Email) $updateSets[] = "w.ProjectManagerEmail = (CASE WHEN f.ProjectManagerEmail IS NULL THEN '".$projectInfo->Project_Manager_Email."' ELSE w.ProjectManagerEmail END) "; //$fields["ProjectManagerEmail"] = $projectInfo->Project_Manager_Email;
            if($projectoldInfo->Project_Manager_Phone != $projectInfo->Project_Manager_Phone) $updateSets[] = "w.ProjectManagerPhone = (CASE WHEN f.ProjectManagerName IS NULL THEN '".$projectInfo->Project_Manager_Phone."' ELSE w.ProjectManagerPhone END) "; //$fields["ProjectManagerPhone"] = $projectInfo->Project_Manager_Phone;
            if($projectoldInfo->Resource_Coordinator != $projectInfo->Resource_Coordinator) $updateSets[] = "w.ResourceCoordinatorName = (CASE WHEN f.ResourceCoordinatorName IS NULL THEN '".$projectInfo->Resource_Coordinator."' ELSE w.ResourceCoordinatorName END) "; //$fields["ResourceCoordinatorName"] = $projectInfo->Resource_Coordinator;
            if($projectoldInfo->Resource_Coord_Email != $projectInfo->Resource_Coord_Email) $updateSets[] = "w.ResourceCoordinatorEmail = (CASE WHEN f.ResourceCoordinatorEmail IS NULL THEN '".$projectInfo->Resource_Coord_Email."' ELSE w.ResourceCoordinatorEmail END) "; //$fields["ResourceCoordinatorEmail"] = $projectInfo->Resource_Coord_Email;
            if($projectoldInfo->Resource_Coord_Phone != $projectInfo->Resource_Coord_Phone) $updateSets[] = "w.ResourceCoordinatorPhone = (CASE WHEN f.ResourceCoordinatorPhone IS NULL THEN '".$projectInfo->Resource_Coord_Phone."' ELSE w.ResourceCoordinatorPhone END) "; //$fields["ResourceCoordinatorPhone"] = $projectInfo->Resource_Coord_Phone;
            if($projectoldInfo->Default_Amount != $projectInfo->Default_Amount) $updateSets[] = "w.PayMax = '".$projectInfo->Default_Amount."' "; //$fields["PayMax"] = $projectInfo->Default_Amount;
            if($projectoldInfo->Qty_DevicesPO != $projectInfo->Qty_Devices) $updateSets[] = "w.Qty_Devices = '".$projectInfo->Qty_Devices."' "; //$fields["Qty_Devices"] = $projectInfo->Qty_Devices;
            if($projectoldInfo->Amount_Per != $projectInfo->Amount_Per) $updateSets[] = "w.Amount_Per = '".$projectInfo->Amount_Per."' ";  //$fields["Amount_Per"] = $projectInfo->Amount_Per;
            if($projectoldInfo->Description != $projectInfo->Description) $updateSets[] = "w.Description = '".mysql_real_escape_string($projectInfo->Description)."' ";//$fields["Description"] = $projectInfo->Description;
            if($projectoldInfo->Requirements != $projectInfo->Requirements) $updateSets[] = "w.Requirements = '".mysql_real_escape_string($projectInfo->Requirements)."' "; //$fields["Requirements"] = $projectInfo->Requirements;
            if($projectoldInfo->Tools != $projectInfo->Tools) $updateSets[] = "w.Tools = '".mysql_real_escape_string($projectInfo->Tools)."' "; //$fields["Tools"] = $projectInfo->Tools;
            if($projectoldInfo->SpecialInstructions != $projectInfo->SpecialInstructions) $updateSets[] = "w.SpecialInstructions = '".mysql_real_escape_string($projectInfo->SpecialInstructions)."' "; //$fields["SpecialInstructions"] = $projectInfo->SpecialInstructions;
            if($projectoldInfo->SpecialInstructions_AssignedTech != $projectInfo->SpecialInstructions_AssignedTech) $updateSets[] = "w.Ship_Contact_Info = '".mysql_real_escape_string($projectInfo->SpecialInstructions_AssignedTech)."' "; //$fields["Ship_Contact_Info"] = $projectInfo->SpecialInstructions_AssignedTech;
            if($projectoldInfo->UseMyRecruitEmailSettings != $projectInfo->UseMyRecruitEmailSettings) $updateSets[] = "w.UseMyRecruitEmailSettings = '".$projectInfo->UseMyRecruitEmailSettings."' "; //$fields["UseMyRecruitEmailSettings"] = $projectInfo->UseMyRecruitEmailSettings;
            if($projectoldInfo->AutoBlastOnPublish != $projectInfo->AutoBlastOnPublish) $updateSets[] = "w.AutoBlastOnPublish = (CASE WHEN f.SystemGeneratedEmailFrom IS NULL THEN '".$projectInfo->AutoBlastOnPublish."' ELSE w.AutoBlastOnPublish END) "; //$fields["AutoBlastOnPublish"] = $projectInfo->AutoBlastOnPublish;
            if($projectoldInfo->RecruitmentFromEmail != $projectInfo->RecruitmentFromEmail) $updateSets[] = "w.RecruitmentFromEmail = '".$projectInfo->RecruitmentFromEmail."' "; //$fields["RecruitmentFromEmail"] = $projectInfo->RecruitmentFromEmail;
            if($projectoldInfo->PricingRuleID != $projectInfo->PricingRuleID) $updateSets[] = "w.PricingRuleID = '".$projectInfo->PricingRuleID."' "; //$fields["PricingRuleID"] = $projectInfo->PricingRuleID;
            if($projectoldInfo->WO_Category_ID != $projectInfo->WO_Category_ID) $updateSets[] = "w.WO_Category_ID = '".$projectInfo->WO_Category_ID."' "; //$fields["WO_Category_ID"] = $projectInfo->WO_Category_ID;
            if($projectoldInfo->General1 != $projectInfo->General1) $updateSets[] = "w.General1 = '".$projectInfo->General1."' "; //$fields["General1"] = $projectInfo->General1;
            if($projectoldInfo->General2 != $projectInfo->General2) $updateSets[] = "w.General2 = '".$projectInfo->General2."' ";//$fields["General2"] = $projectInfo->General2;
            if($projectoldInfo->General3 != $projectInfo->General3) $updateSets[] = "w.General3 = '".$projectInfo->General3."' ";//$fields["General3"] = $projectInfo->General3;
            if($projectoldInfo->General4 != $projectInfo->General4) $updateSets[] = "w.General4 = '".$projectInfo->General4."' ";//$fields["General4"] = $projectInfo->General4;
            if($projectoldInfo->General5 != $projectInfo->General5) $updateSets[] = "w.General5 = '".$projectInfo->General5."' ";//$fields["General5"] = $projectInfo->General5;
            if($projectoldInfo->General6 != $projectInfo->General6) $updateSets[] = "w.General6 = '".$projectInfo->General6."' ";//$fields["General6"] = $projectInfo->General6;
            if($projectoldInfo->General7 != $projectInfo->General7) $updateSets[] = "w.General7 = '".$projectInfo->General7."' ";//$fields["General7"] = $projectInfo->General7;
            if($projectoldInfo->General8 != $projectInfo->General8) $updateSets[] = "w.General8 = '".$projectInfo->General8."' ";//$fields["General8"] = $projectInfo->General8;
            if($projectoldInfo->General9 != $projectInfo->General9) $updateSets[] = "w.General9 = '".$projectInfo->General9."' ";//$fields["General9"] = $projectInfo->General9;
            if($projectoldInfo->General10 != $projectInfo->General10) $updateSets[] = "w.General10 = '".$projectInfo->General10."' ";//$fields["General10"] = $projectInfo->General10;
            if($projectoldInfo->Headline != $projectInfo->Headline) $updateSets[] = "w.Headline = '".$projectInfo->Headline."' ";//$fields["Headline"] = $projectInfo->Headline;
            if($projectoldInfo->CheckInOutEmail != $projectInfo->CheckInOutEmail) $updateSets[] = "w.CheckInOutEmail = '".$projectInfo->CheckInOutEmail."' ";//$fields["CheckInOutEmail"] = $projectInfo->CheckInOutEmail;
            if($projectoldInfo->EmergencyName != $projectInfo->EmergencyName) $updateSets[] = "w.EmergencyName = (CASE WHEN f.EmergencyName IS NULL THEN '".$projectInfo->EmergencyName."' ELSE w.EmergencyName END) ";//$fields["EmergencyName"] = $projectInfo->EmergencyName;
            if($projectoldInfo->EmergencyPhone != $projectInfo->EmergencyPhone) $updateSets[] = "w.EmergencyPhone = (CASE WHEN f.EmergencyPhone IS NULL THEN '".$projectInfo->EmergencyPhone."' ELSE w.EmergencyPhone END) ";//$fields["EmergencyPhone"] = $projectInfo->EmergencyPhone;
            if($projectoldInfo->EmergencyEmail != $projectInfo->EmergencyEmail) $updateSets[] = "w.EmergencyEmail = (CASE WHEN f.EmergencyEmail IS NULL THEN '".$projectInfo->EmergencyEmail."' ELSE w.EmergencyEmail END) ";//$fields["EmergencyEmail"] = $projectInfo->EmergencyEmail;
            if($projectoldInfo->TechnicalSupportName != $projectInfo->TechnicalSupportName) $updateSets[] = "w.TechnicalSupportName = (CASE WHEN f.TechnicalSupportName IS NULL THEN '".$projectInfo->TechnicalSupportName."' ELSE w.TechnicalSupportName END) ";//$fields["TechnicalSupportName"] = $projectInfo->TechnicalSupportName;
            if($projectoldInfo->TechnicalSupportEmail != $projectInfo->TechnicalSupportEmail) $updateSets[] = "w.TechnicalSupportEmail = (CASE WHEN f.TechnicalSupportEmail IS NULL THEN '".$projectInfo->TechnicalSupportEmail."' ELSE w.TechnicalSupportEmail END) ";//$fields["TechnicalSupportEmail"] = $projectInfo->TechnicalSupportEmail;
            if($projectoldInfo->TechnicalSupportPhone != $projectInfo->TechnicalSupportPhone) $updateSets[] = "w.TechnicalSupportPhone = (CASE WHEN f.TechnicalSupportPhone IS NULL THEN '".$projectInfo->TechnicalSupportPhone."' ELSE w.TechnicalSupportPhone END) ";//$fields["TechnicalSupportPhone"] = $projectInfo->TechnicalSupportPhone;
            if($projectoldInfo->CheckInOutName != $projectInfo->CheckInOutName) $updateSets[] = "w.CheckInOutName = '".$projectInfo->CheckInOutName."' ";//$fields["CheckInOutName"] = $projectInfo->CheckInOutName;
            if($projectoldInfo->CheckInOutNumber != $projectInfo->CheckInOutNumber) $updateSets[] = "w.CheckInOutNumber = '".$projectInfo->CheckInOutNumber."' ";//$fields["CheckInOutNumber"] = $projectInfo->CheckInOutNumber;
            if($projectoldInfo->SignOffSheet_Required != $projectInfo->SignOffSheet_Required) $updateSets[] = "w.SignOffSheet_Required = (CASE WHEN f.SignOffSheet_Required IS NULL THEN '".$projectInfo->SignOffSheet_Required."' ELSE w.SignOffSheet_Required END) ";//$fields["SignOffSheet_Required"] = $projectInfo->SignOffSheet_Required;
            //if($projectoldInfo->FixedBid != $projectInfo->FixedBid) $fields["FixedBid"] = $projectInfo->FixedBid;
            if($projectoldInfo->StartDate != $projectInfo->StartDate) $updateSets[] = "w.StartDate = '".$projectInfo->StartDate."' ";//$fields["StartDate"] = $projectInfo->StartDate;
            if($projectoldInfo->StartTime != $projectInfo->StartTime) $updateSets[] = "w.StartTime = '".$projectInfo->StartTime."' ";//$fields["StartTime"] = $projectInfo->StartTime;
            if($projectoldInfo->EndDate != $projectInfo->EndDate) $updateSets[] = "w.EndDate = '".$projectInfo->EndDate."' ";//$fields["EndDate"] = $projectInfo->EndDate;
            if($projectoldInfo->EndTime != $projectInfo->EndTime) $updateSets[] = "w.EndTime = '".$projectInfo->EndTime."' ";//$fields["EndTime"] = $projectInfo->EndTime;
            if($projectoldInfo->StartRange != $projectInfo->StartRange) $updateSets[] = "w.StartRange = '".$projectInfo->StartRange."' "; //$fields["StartRange"] = $projectInfo->StartRange;
            if($projectoldInfo->Duration != $projectInfo->Duration) $updateSets[] = "w.Duration = '".$projectInfo->Duration."' ";//$fields["Duration"] = $projectInfo->Duration;
            if($projectoldInfo->CredentialCertificationId != $projectInfo->CredentialCertificationId) $updateSets[] = "w.CredentialCertificationId = '".$projectInfo->CredentialCertificationId."' ";//$fields["CredentialCertificationId"] = $projectInfo->CredentialCertificationId;
            if($projectoldInfo->PartManagerUpdate_Required != $projectInfo->PartManagerUpdate_Required) $updateSets[] = "w.PartManagerUpdate_Required = '".$projectInfo->PartManagerUpdate_Required."' ";//$fields["PartManagerUpdate_Required"] = $projectInfo->PartManagerUpdate_Required;
            if($projectoldInfo->Type_ID != $projectInfo->Type_ID) $updateSets[] = "w.Type_ID = '".$projectInfo->Type_ID."' ";//$fields["Type_ID"] = $projectInfo->Type_ID;

            if($projectoldInfo->CustomSignOff_Enabled != $projectInfo->CustomSignOff_Enabled) $updateSets[] = "w.CustomSignOff_Enabled = '".$projectInfo->CustomSignOff_Enabled."' ";
            if($projectoldInfo->SignOff_Disabled != $projectInfo->SignOff_Disabled) $updateSets[] = "w.SignOff_Disabled = '".$projectInfo->SignOff_Disabled."' ";

			$woArr = array();
            foreach($woList as $k=>$v){
            	$woArr[] = $v[0];
            }
            
            $criteria = makecriteria($id, $choice, 3, $woArr);
            $updateSql = $updateSql.implode(",",$updateSets)."WHERE ".$criteria;

            //$projectInfo, $projectoldInfo
            //echo("<br/>projectInfo: ");print_r($projectInfo);
            //echo('<br/>updateSql: '.$updateSql);
            //echo("<br/>projectoldInfo: ");print_r($projectoldInfo);
            //die();//test
            
            if(count($updateSets) > 0 && $updateSql != ""){
            	$numUpdated = $db->getConnection()->exec($updateSql);
                //$numUpdated = $db->update('work_orders', $fields, $criteria);
            }
            
            $fields1 = array();
            if($projectoldInfo->PcntDeduct != $projectInfo->PcntDeduct) $fields1["PcntDeduct"] = $projectInfo->PcntDeduct;
            if($projectoldInfo->PcntDeductPercent != $projectInfo->PcntDeductPercent) $fields1["PcntDeductPercent"] = $projectInfo->PcntDeductPercent;
            $criteria = " (Tech_ID IS NULL OR Tech_ID = '') AND Project_ID = '" . $id . "' AND Deactivated = '0' AND (Status = 'created' OR Status = 'published')";
            if(!empty($fields1))
                $numUpdated = $db->update('work_orders', $fields1, $criteria);
            
			$apipro =  new Core_Api_ProjectClass();
                
			if($choice==1)
			{
				$apipro->updateAllWOs_BillableBcats_ByProject($id);
			}
			else if($choice==2)
			{
				$apipro->updateUnassignedWOs_BillableBcats_ByProject($id);
			}
        }
    }
    return $numUpdated;
}

/*
 * $id:     projectid
 * $choice: 1: all wo
 *          2: unassigned and new wo
 * $type:   1: p2t
 *          2: asc
 *          3: other
 */

function makecriteria($id, $choice, $type, $woList='')
{
   $criteria = ($woList != "") ? "WIN_NUM IN (" . implode(",",$woList) . ") AND Deactivated = '0' " : "Project_ID = '" . $id . "' AND Deactivated = '0' ";
   //$criteria = "Project_ID = '" . $id . "' AND Deactivated = '0' ";
   //$criteria = "WIN_NUM IN '" . implode(",",$woList) . "' AND Deactivated = '0' ";
    if ($type == 1)
    {
//        $criteria .= "AND IFNULL(Tech_ID, '') = '' AND ShowTechs = 1 ";
        if ($choice == 2 || $choice == 1)
        {
            $criteria .= "AND (Tech_ID IS NULL OR Tech_ID = '')";
        }
        
    } else if ($type == 2)
    {
        if ($choice == 1)
        {
            $criteria .= "AND TechMarkedComplete = '0' AND ACSInvoiced = '0' AND Approved = '0' ";
        } 
        else if($choice == 2)
        {
            //$criteria .= "AND IFNULL(Tech_ID, '') = '' AND TechMarkedComplete = '0'  AND ACSInvoiced = '0' AND Approved = '0' ";
            $criteria .= "AND (Tech_ID IS NULL OR Tech_ID = '') AND TechMarkedComplete = '0'  AND ACSInvoiced = '0' AND Approved = '0' AND  (Status = 'created' OR Status = 'published') ";
        }
    } else if ($type == 3)
    {
        if ($choice == 1)
        {
            $criteria .= "AND TechMarkedComplete = '0' AND Approved = '0' ";
        } 
        else if($choice == 2)
        {
            //$criteria .= "AND IFNULL(Tech_ID, '') = '' AND TechMarkedComplete = '0' AND Approved = '0' ";
            $criteria .= "AND (Tech_ID IS NULL OR Tech_ID = '') AND TechMarkedComplete = '0' AND Approved = '0' AND  (Status = 'created' OR Status = 'published') ";
        }
    }
    return $criteria;
}

?>