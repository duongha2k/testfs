<?php
/**
 * @author Sergey Petkevich
 */
require ("../../headerStartSession.php");
if (!empty($_POST['enable']) && $_POST['enable'] == 'true') {    $_SESSION['mapEnableOverlay'] = true;

    if (!empty($_POST['techId'])) {
        if (!isset($_SESSION['mapOverlayTechs']) || !is_array($_SESSION['mapOverlayTechs'])) {            $_SESSION['mapOverlayTechs'] = explode(",", $_POST['techId']);
        } else {            $_SESSION['mapOverlayTechs'] = array_merge($_SESSION['mapOverlayTechs'], explode(",", $_POST['techId']));
        }
    }
} else {    unset($_SESSION['mapEnableOverlay']);
    unset($_SESSION['mapOverlayTechs']);
}

?>