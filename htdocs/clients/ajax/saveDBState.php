<?php
require_once("../../library/caspioAPI.php");
require_once '../../headerStartSession.php';

$Core_Api_User = new Core_Api_User();
$authData = array('login'=>$_SESSION['UserName'], 'password'=>$_SESSION['Password']);
$Core_Api_User->load($authData);
$client_id = $Core_Api_User->getClientId();
$settings = Core_DashboardSettings::getSettings($client_id);
$dashboardSettingsSavedState = json_encode("");
if ($settings['sort'] == Core_DashboardSettings::SORT_RECENT) {
	if (isset($_COOKIE['FSWidgetDashboardState']))
		Core_DashboardSettings::saveSettings($client_id, array('dbState' => $_COOKIE['FSWidgetDashboardState']));
}
?>