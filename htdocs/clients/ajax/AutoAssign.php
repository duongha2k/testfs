<?php
if ($_SERVER['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest') {
	if (!empty($_GET['id'])) {
		require ("../autoassign/includes/common.php");
		require_once("../../library/timeStamps2.php");
		require_once("../../library/caspioAPI.php");
		require_once("../../library/smtpMail.php");
		require_once("../../includes/SMS_Send.php");
	    require ("../autoassign/includes/AutoAssign.php");
	    
	    AutoAssign::$baseDir = realpath(dirname(__FILE__) . "/../../") . '/clients/autoassign';
		if (!empty($_GET['isAssign']) && !empty($_GET['showTechs']) && ($_GET['showTechsOld'] == 'No' || $_GET['isAssignOld'] == 'No')) {
			AutoAssign::proccessAutoAssign($_GET['id']);
		} elseif(empty($_GET['isAssign']) || empty($_GET['showTechs'])) {
		    AutoAssign::removeShellFileIfExist((int)$_GET['id']);
		}
	}
	exit();
}
