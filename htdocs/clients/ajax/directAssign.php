<?php
if ($_SERVER['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest') {
	require_once("{$_SERVER['DOCUMENT_ROOT']}/library/woapi.php");
    require ("{$_SERVER['DOCUMENT_ROOT']}/headerStartSession.php");
	class Auth
	{
		private $login = '';
		private $isAuht = false;
		
		function __construct() {
		}
		
		public function setLogin($val) {
			$this->login = $val;
		}
		
		public function getAuthData() {
			if (empty($this->login)) return null;

            $db = Core_Database::getInstance();
            $query = $db->select();
            $query->from(Core_Database::TABLE_CLIENTS, 'Password');
            $query->where('UserName = ?', $this->login);
            $result = Core_Database::fetchAll($query);
            if ( !$result || sizeof($result) == 0 ) return null;
            return array('login'=>$this->login, 'password'=>$result[0]['Password']);
		}
	}
    require_once("{$_SERVER['DOCUMENT_ROOT']}/library/caspioAPI.php");

	$authData = getAuthData();
	$login = $authData['login'];
	$password = $authData['password'];

    $companyID = trim($_POST['companyId'], "'`");
    $success = 0;
    $errors = array();

    $techId     = isset($_POST['techId']) && !empty($_POST['techId']) ? (int)trim($_POST['techId'], "'`")   : null;
    $techPay    = isset($_POST['agreed']) && !empty($_POST['agreed']) ? trim($_POST['agreed'], "'`")        : null;
    $bidComments= isset($_POST['bidComments']) && !empty($_POST['bidComments']) ? trim($_POST['bidComments'], "'`"): null;
    $winNum     = isset($_POST['winNum']) && !empty($_POST['winNum']) ? (int)trim($_POST['winNum'], "'`")   : null;

    if ( $techId && $techPay ) {
	    $techId  = (int)trim($_POST['techId'], "'`");
        $techPay = trim($_POST['agreed'], "'`");
        if ( !empty($winNum) && !empty($companyID) && !empty($techId) && !empty($techPay) && !empty($login) && !empty($password) && $techPay != '0.00') {
			$password = urlencode ($password);
            $url = WOAPI_URL . "client/rest/wos/$winNum?user=$login|pmContext=$companyID&pass=$password";
error_log ("Test: " . $url);
            $result = execCurl($url);

			try {
				$xml = simplexml_load_string($result);

				if (empty($xml->error)) {
					if ($xml->Deactivated != 1 && empty($xml->Tech_ID)) {
						$url = WOAPI_URL . "client/rest/wos/$winNum/assign?techID=$techId&bidAmount=$techPay&bidComments=".urlencode($bidComments)."&user=$login|pmContext=$companyID&pass=$password&amountPer={$xml->Amount_Per}";
						$result = execCurl($url);
						$xml = simplexml_load_string($result);

						if (!empty($xml->error)) {
							foreach ($xml as $er) {
								$errors[] = '<br/>Error (WIN #'.$winNum.') : '.$er->description;
							}
						} elseif (!empty($xml->WIN_NUM)) {
							$success = $winNum;
						} else {
							$errors[] = 'Server error (WIN #'.$winNum.')';
						}
					} else {
						$errors[] = 'Error : WIN #'.$winNum.' has already been assigned.';
					}
				}
				else {
					$nodes = $xml->error->children();
					$errors[] = "Error : " . $nodes[1]->saveXML();
				}
			}
			catch (Exception $e) {
				$errors[] = "Error : " . $e->getMessage ();
			}
        }
	}
    echo json_encode(array('errors'=>$errors, 'wo'=>$winNum, 'success'=>$success));
    exit();
}else{
    die();
}

function execCurl($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_FAILONERROR, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
    curl_setopt($ch, CURLOPT_TIMEOUT, 10); // times out after 4s
    $result = curl_exec($ch); // run the whole process
    curl_close($ch);
    return $result;
}

function getAuthData() {
	if (empty ($_SESSION["Password"])) {
		$auth = new Auth();
		$auth->setLogin($_SESSION['UserName']);
		return $auth->getAuthData();
	}
	else {
		return array("login" => $_SESSION['UserName'], "password" => $_SESSION["Password"]);
	}
}

