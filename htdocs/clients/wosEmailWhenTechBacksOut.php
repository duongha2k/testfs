<?php
// Created on: 11-26-07 
// Author: CM
// Description: If the workorder is changed the tech and call coordinator are notified of the change

//ini_set("display_errors", "stdout");

$TB_UNID = $_REQUEST['TB_UNID'];
$techID = $_REQUEST['techID'];
//$techID = 30349;
?>

<?php
require_once("../library/caspioAPI.php");
require_once("../library/smtpMail.php");

// If Tech id and not reviewed
if ($techID != "")
{
	
	// Get Work Order
	$woInfo = caspioSelectAdv("Work_Orders", "WO_ID, StartDate, Project_Name, Project_ID", "TB_UNID = $TB_UNID", "TB_UNID", false, "`", "|");
	
	if (sizeof($woInfo) == 1 && $woInfo[0] != "") {
			$info = explode("|", $woInfo[0]);
			$woID = trim($info[0], "`");
			$StartDate = trim($info[1], "`");
			$ProjectName = trim($info[2], "`");
			$ProjectID = trim($info[3], "`");
	}
		
	// Get Project
	$woInfo = caspioSelectAdv("TR_Client_Projects", "From_Email, Project_Manager, Client_Email, Client_Name", "Project_ID = $ProjectID", "Project_ID", false, "`", "|");
	
	if (sizeof($woInfo) == 1 && $woInfo[0] != "") {
			$info = explode("|", $woInfo[0]);
			$vFromEmail = trim($info[0], "`");
			$vFromName = trim($info[1], "`");
			$clientEmail = trim($info[2], "`");
			$clientName = trim($info[3], "`");
	}
	
	if ($clientEmail != "")
	{
	
		// Get Tech Info
		$techInfo = caspioSelectAdv("TR_Master_List", "FirstName, LastName", "TechID = $techID", "TechID", false, "`", "|");
		
		if (sizeof($techInfo) == 1 && $techInfo[0] != "") {
			$info = explode("|", $techInfo[0]);
			$FirstName = trim($info[0], "`");
			$LastName = trim($info[1], "`");
		}
	
				
		// Send Email to the Tech
		//$vFromName = "Field Solutions";
		//$vFromEmail = "jcoleman@fieldsolutions.com";
		$eList = $clientEmail;
		
		//$eList = "progressions@gmail.com";
		
		$vSubject = "Technician $FirstName $LastName has backed out of WO $woID";
		@$caller = "wosEmailWhenTechBacksOut";
		$message = "
		Technician $FirstName $LastName has backed out of WO $woID for $StartDate on Project $ProjectName.
		
		Thank you,
		Field Solutions Management";
				
		$htmlmessage = nl2br($message);
		
		// Emails Tech
		smtpMail($vFromName, $vFromEmail, $eList, $vSubject, $message, $htmlmessage, $caller);
		
		//echo "<p>$eList</p>";
		//echo $message;
	}
	else 
	{
		//echo "No client email";
	}
}

?>

<SCRIPT LANGUAGE="JavaScript"> 

	setTimeout('close()',100); 

</SCRIPT>