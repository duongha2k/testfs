<?php $page = 'clients'; ?>
<?php $option = 'reports'; ?>
<?php $selected = 'techsPreferredEdit'; ?>
<?php  require ("../header.php");?>
<?php require ("../navBar.php"); ?>
<?php require ("includes/adminCheck.php"); ?>
<?php require ("includes/PMCheck.php"); ?>
<?php 
$companyID = (isset($_GET["v"]) ? caspioEscape($_GET["v"]) : "BLANKID");
?>
<!-- Add Content Here -->
<style type="text/css" title="currentStyle">
			@import "/widgets/js/datatables/css/demo_page.css";
			@import "/widgets/js/datatables/css/demo_table.css";
			@import "/widgets/js/datatables/css/datatable_theme.css";
		</style>

<script  src="/widgets/js/datatables/js/jquery.dataTables.js"></script>
<br />
<script language="javascript" type="text/javascript"> 
function cbLinkHover(obj,css) 
{
		try { 
			obj.style.cssText=css; 
		} catch (e) {}
} 

function cbButtonHover(obj,css) 
{
		try { 
			obj.style.cssText=css; 
		} catch (e) {}
}


var techTable;
var aPos;
var aData;

function removePrefTech(techId){
	
	$.ajax({
		type: "POST",
		url: "ajax/removePreferredTech.php",
        dataType    : 'json',
        cache       : false,
		data: {
			companyId: "<?=$companyID?>",
			techId: techId
		},
		success: function (data) {
			
			try {
				if (data.success == 0) {
					$("#mainContent").html("<h1>NO RECORDS FOUND</h1>").show();
				}
				else {
					techTable.fnDeleteRow(aPos[0]);
				}
			} catch (e) {}
		}
	});
}

function updatePosition(obj){
	aPos = techTable.fnGetPosition( obj );         
	//Get the data array for this row 
	aData = techTable.fnGetData( aPos[0] );
}

$(document).ready(function() {
	$("#tableLabel").hide();
	
	$('#clientPreferredForm').submit(function(event) {
		event.preventDefault();

		var techID = $("#techID").val();
		var fName = $("#fName").val();
		var lName = $("#lName").val();
		
		$.ajax({
			type: "POST",
			url: "ajax/getPreferredTechs.php",
	        dataType    : 'json',
	        cache       : false,
			data: {
				companyId: "<?=$companyID?>",
				techId: techID,
				fName: fName,
	            lName: lName
			},
			success: function (data) {
				
				try {
					if (data.success == 0) {
						$("#mainContent").html("<h1>NO RECORDS FOUND</h1>").show();
					}
					else {
							var html = "";

							html +="<table cellspacing='1' id='techPreferredTable' class='tablesorter'><thead><tr><th>Tech ID</th><th>First Name</th><th>Last Name </th><th>Primary Phone</th><th>Primary Email</th><th>Recommended</th><th>Performance</th><th></th></tr></thead><tbody>";
							
							for(var i = 0; i < data.techs.length; i++){
									html += "<tr>";
									html += "<td>"+data.techs[i].TechID+"</td>";
									html += "<td>"+data.techs[i].FirstName+"</td>";
									html += "<td>"+data.techs[i].LastName+"</td>";
									html += "<td>"+data.techs[i].PrimaryPhone+"</td>";
									html += "<td>"+data.techs[i].PrimaryEmail+"</td>";
									html += "<td>"+ ((data.techs[i].SATRecommendedAvg == null) ? "0" : data.techs[i].SATRecommendedAvg) +"("+ ((data.techs[i].SATRecommendedTotal == null) ? "0" : data.techs[i].SATRecommendedTotal) +")"+"</td>";
									html += "<td>"+ ((data.techs[i].SATPerformanceAvg == null) ? "0" : data.techs[i].SATPerformanceAvg) +"("+ ((data.techs[i].SATPerformanceTotal == null) ? "0" : data.techs[i].SATPerformanceTotal) +")"+"</td>";
									html += "<td onclick='updatePosition(this);'><a href='javascript:void(0);' onClick='javascript:removePrefTech("+data.techs[i].TechID+");'>Remove Tech</a></td>";
									html += "</tr>";
								}
							html += "</tbody></table>";
							
						
						
						$("#mainContent").html(html).show();
						$("#tableLabel").html("<h1>Techs Client Preferred</h1>").show();

						
						techTable = $('#techPreferredTable').dataTable({
													        "aoColumnDefs": [{ "bSortable": false, "aTargets": [ 7 ]}]
        													});	
						/*
						$("#techPreferredTable tbody td").click(function() {
							aPos = techTable.fnGetPosition( this );         
							//Get the data array for this row 
							aData = techTable.fnGetData( aPos[0] );
						});
						*/
					}
				} catch (e) {}
			}
		});
	});
});

</script>
	<div id="tableLabel">
	</div>

	<div id="mainContent" align="center">		
		<form id="clientPreferredForm" name="clientPreferredForm"  method="post" action="" style="margin: 0px;">
			<input type="hidden" value="193b0000f968d7a8a1e04510bc4c" name="AppKey">
			<table cellspacing="0" style="border: 2px solid rgb(56, 92, 126);">
			<tbody>
				<tr>
					<td align="center">
						<h1>Techs Client Preferred</h1>
					</td>
				</tr>
				<tr style="background-color: rgb(255, 255, 255); padding: 7px;">
					<td style="text-align: left; vertical-align: top; width: auto; white-space: nowrap; padding: 2px 5px; color: rgb(56, 92, 126); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal;">
						<label for="techID">Tech ID</label>
					</td>
					<td style="text-align: left; vertical-align: top; width: auto; padding: 5px 10px; white-space: nowrap;">
						<input type="text" value="" id="techID" name="techID" maxlength="255" size="25" style="color: rgb(0, 0, 0); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal; border: 1px solid rgb(0, 0, 0); padding: 1px;">
					</td>
				</tr>
				<tr style="background-color: rgb(255, 255, 255);">
					<td style="text-align: left; vertical-align: top; width: auto; white-space: nowrap; padding: 2px 5px; color: rgb(56, 92, 126); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal;">
						<label for="fName">First Name (contains)</label>
					</td>
					<td style="text-align: left; vertical-align: top; width: auto; padding: 5px 10px; white-space: nowrap;">
						<input type="text" value="" id="fName" name="fName" maxlength="255" size="25" style="color: rgb(0, 0, 0); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal; border: 1px solid rgb(0, 0, 0); padding: 1px;">
					</td>
				</tr>
				<tr style="background-color: rgb(255, 255, 255); padding: 7px;">
					<td style="text-align: left; vertical-align: top; width: auto; white-space: nowrap; padding: 2px 5px; color: rgb(56, 92, 126); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal;">
						<label for="lName">Last Name (contains)</label>
					</td>
					<td style="text-align: left; vertical-align: top; width: auto; padding: 5px 10px; white-space: nowrap;">
						<input type="text" value="" id="lName" name="lName" maxlength="255" size="25" style="color: rgb(0, 0, 0); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal; border: 1px solid rgb(0, 0, 0); padding: 1px;">
					</td>
				</tr>
				<tr style="background-color: rgb(255, 255, 255);">
					<td style="padding: 5px 0px; text-align: center; vertical-align: middle; border: 2px solid rgb(42, 73, 125); background-color: rgb(186, 203, 223);" colspan="2">
						<input type="submit" value="Search" onmouseout="cbButtonHover(this,'color: #ffffff;font-size: 12px;font-family: Verdana;font-style: normal;font-weight: bold;text-align: center;vertical-align: middle;border-color: #174065;border-style: solid;border-width: 1px;background-color: #385c7e;width: auto;height: auto;margin: 0 3px;');" onmouseover="cbButtonHover(this,'color: #FFFFFF;font-size: 12px;font-family: Verdana;font-style: normal;font-weight: bold;text-align: center;vertical-align: middle;border-style:solid;border-width: 1px;border-color: #405c8b;background-color: #6373B5;width: auto;height: auto;margin: 0 3px;');" style="color: rgb(255, 255, 255); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold; text-align: center; vertical-align: middle; border: 1px solid rgb(23, 64, 101); background-color: rgb(56, 92, 126); width: auto; height: auto; margin: 0pt 3px;" id="submitBtn">
					</td>
				</tr>
			</tbody>
			</table>

		</form>
		
		
		
		<!-- table id='techPreferredTable' class='tablesorter'>
		<thead><tr>
		<th>Tech ID</th><th>First Name</th><th>Last Name </th><th>Primary Phone</th><th>Primary Email</th><th>Recommended</th><th>Performance</th><th></th>
		</tr></thead>
		<tbody>
		</tbody>
		</table -->
		
	</div>
	
	
<!-- div id="cxkg"><a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000f968d7a8a1e04510bc4c">Click here</a> to load this Caspio <a title="Online Database" href="http://www.caspio.com">Online Database</a>.</div>
<div id="cb193b0000f968d7a8a1e04510bc4c"><a href="http://www.caspio.com" target="_blank">Online Database</a> by Caspio</div>
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b0000f968d7a8a1e04510bc4c","https:");}catch(v_e){;}</script>
<div id="cxkg"><a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000f968d7a8a1e04510bc4c">Click here</a> to load this Caspio <a href="http://www.caspio.com" title="Online Database">Online Database</a>.</div -->


<script type="text/javascript">
function formatSATScore(index) {
	if (index == 0) return;
	if ($(this).next().html() == "&nbsp;")
		$(this).html("0 (0)");
	else
		$(this).html($(this).html() + " (" + $(this).next().html() + ")");
}

$("#caspioform table:first tr > td:nth-child(7)").hide();
$("#caspioform table:first tr > td:nth-child(9)").hide();

$("#caspioform table:first tr > td:nth-child(6)").each(formatSATScore);
$("#caspioform table:first tr > td:nth-child(8)").each(formatSATScore);

</script>

<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
