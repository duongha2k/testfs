<?php
    /**
     * DEPRICATED:
     * new script SATFrom_beta.php is used
     * @author Artem Sukharev
     */
?>
<?php
require_once("../headerSimple.php");
$displayPMTools = false;
require_once("includes/PMCheck.php");
?>
<style type="text/css">
	.tableRadioBtn {
		width: 120px;
	}
	.tableRadioBtn label {
		padding-left: 5px;
	}
	<?php
		if ($_GET["embedded"] == 1):
	?>
	.SATLabel {
		position: relative;
		left: -15px;
		font-weight: bold;
		color: #032D5F;
	}
	<?php endif;?>
</style>
<script type="text/javascript">
	
	function SATEmbedRelayRadioBtn() {
		SATValue = $(this).val();
		SATName = $(this).attr("name");
//		alert(SATValue);
		$("#" + SATName, parent.document).attr("value", SATValue);
	}
	
	function attachRelayRadioBtn() {
		$(this).click(SATEmbedRelayRadioBtn);
	}
	
	function tablizeRadioBtns(firstChoice, numChoices, prefix) {
		if (numChoices == null) numChoices = 4;
		if (prefix == null) prefix = "#EditRecord";
		span = $(prefix + firstChoice).parent();
		$(prefix + firstChoice).parent().parent().append("<table style=\"width:100%\"><tr id=\"" + firstChoice + "Row\"></tr></table>");
		choices = $(prefix + firstChoice).parent().children();
		input = choices.filter("input");
		label = choices.filter("label");

		for (i = 0; i < numChoices; ++i) {
			choice = firstChoice + i;
			$("#" + firstChoice + "Row").append("<td id=\"" + choice + "\" class=\"tableRadioBtn\" />");
			$(input[i]).appendTo("#" + choice);
			$(label[i]).appendTo("#" + choice);
		}
		for (i = 0; i < 4 - numChoices; ++i) {
			$("#" + firstChoice + "Row").append("<td/>");
		}
		span.remove();
	}
	
	$(document).ready(function() {
		tablizeRadioBtns("SATRecommended0");
		tablizeRadioBtns("SATPerformance0");
		tablizeRadioBtns("cbParamVirtual10", 3, "#");
		$("#EditRecordSATRecommended0").parent().parent().css({"font-size": "11px", "left": "7px", "position" : "relative"});		
		$("#EditRecordSATPerformance0").parent().parent().css({"font-size": "11px", "left": "7px", "position" : "relative"});

		if ("<?=$_GET["embedded"]?>" == 1) {
			$("#winnum").parent().parent().hide();
			$("table").attr("style", "width: 100%");
			$("#Mod0EditRecord").parent().parent().hide();
						
			$("input[name='EditRecordSATRecommended']").each(attachRelayRadioBtn);
			$("input[name='EditRecordSATPerformance']").each(attachRelayRadioBtn);
			$("#SATFrame", parent.document).css("height", $(window).height() + 30 + "px");
		}
		else {							
			$("#winnum").parent().css("padding", "10px 2px").next().css("padding", "10px 2px").next().css("padding", "10px 2px").next().css("padding", "10px 2px");
		}
		
		
		techID = "<?=$_GET["techID"]?>";
		companyID = "<?=$_GET["v"]?>";
		if (techID != "" && companyID != "") {
			$.post("ajax/techStatus.php", {"techID" : techID, "companyID" : companyID}, 
				function(data) {
					try {
						data = eval("data = " + data + ";");
					}
					catch (e) { data["status"] = 0 }
					switch (data["status"]) {
						case 0:
							$("#cbParamVirtual12").click();
							break;
						case 1:
							$("#cbParamVirtual10").click();
							break;
						case 2:
							$("#cbParamVirtual11").click();
							break;
					}
					
				}
			);
		}
		
		$("#cbParamVirtual12").click(setTechStatusNone);
		$("#cbParamVirtual10").click(setTechStatusPreferred);
		$("#cbParamVirtual11").click(setTechStatusDenied);
	});
	
	function setTechStatus(status) {
		techID = "<?=$_GET["techID"]?>";
		companyID = "<?=$_GET["v"]?>";
		$.post("ajax/techStatus.php", {"techID" : techID, "companyID" : companyID, "status" : status}, 
			function(data) {}
		);
	}
	
	function setTechStatusNone() {
		setTechStatus(0);
	}

	function setTechStatusPreferred() {
		setTechStatus(1);
	}

	function setTechStatusDenied() {
		setTechStatus(2);
	}
</script>
<div style="margin: 15px">
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b0000920e8c79e52748c1b68b","https:");}catch(v_e){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000920e8c79e52748c1b68b">here</a> to load this Caspio <a href="http://www.caspio.com" title="Online Database">Online Database</a> app.</div>