<?php
	set_time_limit(300);
	require_once("../headerStartSession.php");
	require_once("../library/caspioAPI.php");
	require_once("../library/projectTimeStamps.php");
	require_once("../library/woACSTimeStamps.php");
//	ini_set("display_errors",1);
	$id = $_GET["id"];

	require_once("{$_SERVER['DOCUMENT_ROOT']}/library/bootstrapAPI.php");
	Core_Database_CaspioSync::sync(TABLE_CLIENT_PROJECTS, "Project_ID", array($id));

	$user = $_GET["user"];
	$create = isset($_GET["create"]);
	$choice = 0;
	$numUpdated = 0;
	if (isset($_GET["choice"]))
		$choice = $_GET["choice"];
	if (is_numeric($id) && is_numeric($choice)) {
		$projectInfo = createProjectTimeStamp($id, $user);
		if (!$create && $choice != 3) {
			// update ACS changes in project WOs
			$reminderAll = $projectInfo["reminderAll"];
			$reminderAcceptance = $projectInfo["reminderAcceptance"];
			$reminder24Hr = $projectInfo["reminder24Hr"];
			$reminder1Hr = $projectInfo["reminder1Hr"];
			$checkInCall = $projectInfo["checkInCall"];
			$checkOutCall = $projectInfo["checkOutCall"];
			$reminderNotMarkComplete = $projectInfo["reminderNotMarkComplete"];
			$reminderIncomplete = $projectInfo["reminderIncomplete"];
			$SMSBlast = $projectInfo["SMSBlast"];
			$applyTo = $projectInfo["applyTo"];
			$applyTo = trim($applyTo, "'");
			$applyTo = $applyTo == "DEFAULT" ? "" : $applyTo;
			if ($choice != $applyTo) die();

			$criteria = "Project_ID = '$id' AND TechMarkedComplete = '0' AND ACSInvoiced = '0' AND Approved = '0' AND Deactivated = '0'";
			
			if ($choice == 2) {
				// update only currently unassigned
				$criteria = "IFNULL(Tech_ID, '') = '' AND " . $criteria;
			}
			
//			echo "$criteria";
//			$woList = caspioSelectAdv("Work_Orders", "TB_UNID", $criteria, "", false, "`", "|", false);
			
			$db = Zend_Registry::get('DB');
			$sql = "SELECT WIN_NUM FROM work_orders WHERE $criteria";
			$woList = $db->fetchAll($sql, array(), Zend_Db::FETCH_NUM);
			
			if (count($woList) > 0) {
//				$numUpdated = caspioUpdate("Work_Orders", "ReminderAll, ReminderAcceptance, Reminder24Hr, Reminder1Hr, CheckInCall, CheckOutCall, ReminderNotMarkComplete, ReminderIncomplete, SMSBlast", "'$reminderAll', '$reminderAcceptance', '$reminder24Hr', '$reminder1Hr', '$checkInCall', '$checkOutCall', '$reminderNotMarkComplete', '$reminderIncomplete', '$SMSBlast'", $criteria, false);
				$fields = array("ReminderAll" => $reminderAll, 
								"ReminderAcceptance" => $reminderAcceptance, 
								"Reminder24Hr" => $reminder24Hr, 
								"Reminder1Hr" => $reminder1Hr, 
								"CheckInCall" => $checkInCall, 
								"CheckOutCall" => $checkOutCall, 
								"ReminderNotMarkComplete" => $reminderNotMarkComplete, 
								"ReminderIncomplete" => $reminderIncomplete, 
								"SMSBlast" => $SMSBlast );
				$numUpdated = $db->update('work_orders', $fields, $criteria);
								
				foreach ($woList as $unid) {
					if ($unid == "") continue;
					createWOACSTimeStamp($unid);
				}
			}
		}
	}	
?>
<?php
if (!$create):
	if ($numUpdated > 0):
?>
<script type="text/javascript">
	window.focus();
	setTimeout("window.close();", 5000);
</script>

<div style="text-align:center">
Updated <?=$numUpdated?> work orders with new ACS settings <br/><br/>
<input type="button" onclick="window.close()" value="Close"/>
</div>
<?php
	else:
?>
<script type="text/javascript">
	window.close();
</script>
<?php
	endif;
else:
?>
Your submission was successful.
<?php
endif;
?>
