<?php
$page = "clients";
require_once("../headerStartSession.php");
require_once("../library/caspioAPI.php");
require_once("../library/smtpMail.php");
require_once("../library/recruitmentEmailLib.php");
require_once("../library/woContactLog.php");
require_once("../../wdata/application/modules/dashboard/views/helpers/DisplayDate.php");

$id = $_GET['id'];
$companyID = $_GET['v'];
if (!is_numeric($id)) die();
$EmailBlastRadius = isset($_GET['EmailBlastRadius'])?empty($_GET['EmailBlastRadius'])?50:$_GET['EmailBlastRadius']:50;
$EmailBlastRadiusPreferredOnly = isset($_GET['EmailBlastRadiusPreferredOnly'])?empty($_GET['EmailBlastRadiusPreferredOnly'])?0:$_GET['EmailBlastRadiusPreferredOnly']:0;
/*$info = getBlastSettingById($_GET["id"]);

echo "Blast Level: {$info["ActiveBlastLevel"]}<br/>";
echo "&nbsp;&nbsp;Auto Blast On Publish: " . ($info["AutoBlastOnPublish"] ? "Yes" : "No") . "<br/>";
echo "&nbsp;&nbsp;From Email: {$info["FromEmail"]}<br/>";*/

$wo = getWOForRecruitmentEmailById(array($id), ID_TYPE_TB_UNID, FALSE, FALSE);
$emailsSent = blastRecruitmentEmailMainSite($wo, $companyID, "", $EmailBlastRadius, TRUE, TRUE,"","","",$EmailBlastRadiusPreferredOnly);
//build email message

if(!empty($wo))
{
    if (!empty($_SESSION)) $user = $_SESSION["UserName"];
    $autoAssign = $wo[0][21] == 1;
    $subjectLine = ($autoAssign ? "P2T: " : "WIN#: {$wo[0][17]} NEW ");
    $qtyDevices = ($wo[0][9] == "Device" ? "(" . $wo[0][10] . " device(s))" : "");

    $PcntDeduct = $wo[0][26] == 1;
    $PcntDeductPercent = $wo[0][27];
    if(empty($PcntDeductPercent) || $PcntDeductPercent == 0)
    {
        $PcntDeductPercentMsg = "No service fee will be deducted from your final total payment amount from this work order.";
    }
    else if($PcntDeductPercent == 1)
    {
        $PcntDeductPercentMsg = "You will be paid outside of the FieldSolutions Platform for this work order.";
    }
    else
    {
        $PcntDeductPercentMsg = "A ".($PcntDeductPercent * 100)."% service fee will be deducted from your final total payment amount from this work order.";
    }

	$details = new Core_Api_WosClass ();
	$visits = $details->GetWOVisitList ($id);
	$date_helper = new Dashboard_View_Helper_DisplayDate ();

    //$deductMsg = $PcntDeduct ? "A 10% service fee will be deducted from your final total payment amount from this work order." : "NO service fee will be deducted from this work order";
    $message = "WIN#: ".$wo[0][17]." Go to <a href='https://www.fieldsolutions.com/techs/wosDetails.php?id=".$wo[0][17]."'>https://www.fieldsolutions.com/techs/wosDetails.php?id=".$wo[0][17]."</a> to apply!

    Multiple Work Orders in your area are available within the same project. <a href='https://www.fieldsolutions.com/techs/wos.php?tab=techavailable&date_start=" . date("m/d/Y") . "'>Click here</a> to view all of your Available Work.

    $subjectLine
	Location: ".$wo[0][1].", ".$wo[0][2].", ".$wo[0][3];

    $index = 1;
	foreach ($visits as $visit) {
		$message .= "\n\nVisit #$index:";
		$message .= "\nStart Type: " . ($visit["StartTypeID"] == 1 ? "Firm Start Time" : "Start Time Window");
		$message .= "\nWork Start: " . $date_helper->displayDate ('n/j/Y', $visit["StartDate"]) . " " . $visit["StartTime"];
		$message .= "\nWork End: " . $date_helper->displayDate ('n/j/Y', $visit["EndDate"]) . " " . $visit["EndTime"];
		$message .= "\nDuration: " . $details->getDurationDesc ($visit["EstimatedDuration"]);
		$message .= "\nArrival Instructions: " . $visit["TechArrivalInstructions"];

		$index++;
	}

    $message .= "\n\nPay: ".$wo[0][7]." PER ".$wo[0][9]." $qtyDevices
	$PcntDeductPercentMsg

    Description:
    
    ".strip_tags ($wo[0][0])."

    Required Tools:
    
    ".strip_tags ($wo[0][22])."

    Special Instructions:
    
    ".strip_tags ($wo[0][8])."

    ";

   Core_Database::insert("WO_Contact_Log",
                            array("Created_Date"=>date("Y-m-d g:i:s"),
                                "Created_By"=>$user,
                                "Type"=>"Email",
                                "Contact"=>"Multiple Techs",
                                "Subject"=>"Available Work",
                                "Message"=>$message,
                                "Company_ID"=>$companyID,
                                "WorkOrder_ID"=>$wo[0][17]
                                )
                            );
    if($wo[0][20])
    {
        $locationLine = "";
        
        if ($wo[0][1] == "" && $wo[0][3] == "")
            $locationLine = "";
        else if ($wo[0][1] == "")
            $locationLine = $wo[0][3]." - ";
        else if ($wo[0][3] == "")
            $locationLine = $wo[0][1]." - ";
        else
            $locationLine = $wo[0][1]." ".$wo[0][3]." - ";

        $sms_message = "View more WOs from same project online or on FS-Mobile - WIN# ".$wo[0][17]."- ".$wo[0][19]."- $locationLine ".$wo[0][18]."";
        Core_Database::insert("WO_Contact_Log",
                            array("Created_Date"=>date("Y-m-d g:i:s"),
                                "Created_By"=>$user,
                                "Type"=>"SMS Text Message",
                                "Contact"=>"Multiple Techs",
                                "Subject"=>"Available Work",
                                "Message"=>$sms_message,
                                "Company_ID"=>$companyID,
                                "WorkOrder_ID"=>$wo[0][17]
                                )
                            );
    }
}

// Log in WO Contact Logs
echo "Recruitment Emails Sent: $emailsSent<br/><br/>";

?>  

<input type="button" onclick="window.close()" value="Close Window" />