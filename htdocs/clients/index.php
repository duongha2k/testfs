<?php ob_start();?>
<?php $page = "clients"; ?>
<?php $option = "home"; ?>
<?php require ("../header.php"); ?>

<?php
// Check whether client is logged in
$loggedInAs = $_SESSION['loggedInAs'];

if($loggedIn == "yes" && $loggedInAs == "client"){
    // Redirect to dashboard.php
    ob_clean();
    header("Location: http://" . $_SERVER['HTTP_HOST'] . "/clients/dashboard.php") ;
}else{
    // Redirect to logIn.php
    ob_clean();
    header("Location: http://" . $_SERVER['HTTP_HOST'] . "/clients/logIn.php" . (empty($_GET['r']) ? '' : '?r=' . urlencode($_GET['r']))) ;
} 
?>
