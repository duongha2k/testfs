<?php $page = clients; ?>
<?php $option = dashboard; ?>
<?php 	require ("../headerStartSession.php"); ?>

<?php

$siteTemplate = $_SESSION['template'];
if ($siteTemplate == "ruo") {
	require ("../templates/ruo/includes/header.php");
} else {
	require ("../header.php");
 }
?>


<?php 
$loggedIn = $_SESSION['loggedIn'];
$Company_ID = $_SESSION['Company_ID'];

if($loggedIn!="yes"){ 
// Redirect to logIn.php
header( 'Location: https://'.$siteTemplate.'.fieldsolutions.com/clients/logIn.php' ) ;
}
?>
<?php require ("../navBar2.php"); ?>
<?php 
//$loggedIn = $_SESSION['loggedIn'];
//echo "$loggedIn";
?>


<!-- Add Content Here -->
<?php

if ($siteTemplate == "ruo") {
	require ("../templates/ruo/includes/client_dashboardContent.php");
} else {
	require ("dashboardContent2.php");
}
?>


<!--- End Content --->
<?php require ("../footer.php"); ?>
