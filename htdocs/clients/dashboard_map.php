<?php
/**
 * @author Sergey Petkevich
 */
require ("../headerTab.php");
require_once("../library/caspioAPI.php");
if (!empty($_SESSION['Company_ID'])) {
    $companyId = mysql_escape_string($_SESSION['Company_ID']);

    $sqlFilter = '';

    if (!empty($_GET['projid'])) {
        $sqlFilter .= ' AND (Project_ID = \'' . (int)$_GET['projid'] . '\')';
    }

    if (!empty($_GET['startdate'])) {
        $sqlFilter .= ' AND StartDate >= \'' . mysql_escape_string($_GET['startdate']) . '\'';
    }

    if (!empty($_GET['enddate'])) {
        $sqlFilter .= ' AND StartDate <= \'' . mysql_escape_string($_GET['enddate']) . '\'';
    }

    if (!empty($_GET['category'])) {
        $sqlFilter .= ' AND WO_Category_ID = \'' . (int)$_GET['category'] . '\'';
    }

    // Do not dispay deactivated
    //$sqlFilter .= ' AND Deactivated = \'False\'';


    $fieldList = 'TB_UNID,Deactivated,Latitude,Longitude,Tech_ID,TechMarkedComplete,WO_ID,StartDate,Project_ID,SiteName,Address,City,State,Zipcode,Tech_Bid_Amount,ShowTechs';

    $workOrders = caspioSelectAdv("Work_Orders", $fieldList, "Company_ID LIKE '" . $companyId . "'" . $sqlFilter, "", false, "`", "|");

    $arrWorkOrders            = array();
    $arrWoLocations           = array();
    $arrWorkOrdersPublished = array();
    $arrWorkOrderAssigned     = array();
    $arrWorkOrderComplete     = array();
    $arrWoInfo                = array();
    $arrProjectsIds           = array();
    $arrTechsAssigned         = array();

    foreach ($workOrders as $keyRecord => &$record) {
        $workRecord = explode("|", $record);
        $woId = (int)trim($workRecord[0], "`");
        $arrWorkOrders[$woId] = $woId;

        $arrWoLocations[$woId]['Latitude'] = trim($workRecord[2], "`");
        $arrWoLocations[$woId]['Longitude'] = trim($workRecord[3], "`");

        $woName    = trim($workRecord[6], "`");
        $startTime = trim($workRecord[7], "`");
        $projectId = trim($workRecord[8], "`");
        $site      = trim($workRecord[9], "`");
        $address   = trim($workRecord[10], "`");
        $city      = trim($workRecord[11], "`");
        $state     = trim($workRecord[12], "`");
        $zip       = trim($workRecord[13], "`");
        $location  = '';
        $bidAmount = trim($workRecord[14], "`");
        $techId    = trim($workRecord[4], "`");
        $published = trim($workRecord[15], "`");

        if (!empty($address)) {
            $location .= $address . ', ';
        }

         if (!empty($city)) {
            $location .= $city;
        }

        if (!empty($zip)) {
            $location .= ' ' . $zip;
        }

        if (!empty($state)) {
            $location .= ', ' . $state;
        }

        // Get the project details
        if (!isset($arrProjectsIds[$projectId]) && $projectId > 0) {
            $projectDetails = caspioSelectAdv("TR_Client_Projects", 'Project_Name', "Project_ID =" . (int)$projectId, "", false, "`", "|");
            if (isset($projectDetails[0])) {
                $arrProjectsIds[$projectId]['Project_Name'] = trim($projectDetails[0], "`");
            }
        }

        // Get the assigned Tech details
        if ($techId > 0 && !isset($arrTechsAssigned[$techId])) {
            $techDetails = caspioSelectAdv("TR_Master_List", 'FirstName,LastName', "TechID =" . (int)$techId, "", false, "`", "|");
            if (isset($techDetails[0])) {
                $arrTechName = explode('|', $techDetails[0]);
                $arrTechsAssigned[$techId]['FirstName'] = trim($arrTechName[0], "`");
                if ($arrTechName[1]) {
                    $arrTechsAssigned[$techId]['LastName'] = trim($arrTechName[1], "`");
                }
            }


        }

        $arrWoInfo[$woId] = 'ID: <a href=\"wosDetails.php?v=' . $companyId . '&id=' . $woId . '\" target=\"_blank\">' . $woId . '</a><br />';
        if ($startTime != 'NULL') {
            $arrWoInfo[$woId] .= 'Start date/time: ' . addslashes(strip_tags($startTime)) . '<br />';
        }

        $arrWoInfo[$woId] .= addslashes(strip_tags($woName)) . '<br />';
        if (isset($arrProjectsIds[$projectId])) {
        	$arrProjectsIds[$projectId]['Project_Name'];
            $arrWoInfo[$woId] .= 'Project: ' . addslashes(strip_tags($arrProjectsIds[$projectId]['Project_Name'])) . '<br />';
        }

        if (!empty($site) && $site != 'NULL') {
            $arrWoInfo[$woId] .= 'Site: ' . addslashes(strip_tags($site)) . '<br />';
        }

        $arrWoInfo[$woId] .= 'Location: ' . $location . '<br />';

        if (!empty($bidAmount) && $bidAmount != 'NULL') {
            $arrWoInfo[$woId] .= 'Bid amount: $' . $bidAmount . '<br />';
        }

        if (!empty($arrTechsAssigned[$techId])) {
            $arrWoInfo[$woId] .= 'Assigned Tech: ' . $arrTechsAssigned[$techId]['FirstName'] . ' ' . $arrTechsAssigned[$techId]['LastName'] . '<br />';
        }

        //Work Done
        if ($complete != 'False') {
            $arrWorkOrderComplete[$woId] = $woId;
            unset($workOrders[$keyRecord]);
            continue;
        }

        $assignedTo = trim($workRecord[4], "`");
        // Assigned
        if ($assignedTo > 0) {
            $arrWorkOrderAssigned[$woId] = $woId;
            unset($workOrders[$keyRecord]);
            continue;
        }

        $complete = trim($workRecord[5], "`");


        $deactevated = trim($workRecord[1], "`");
        // Published
        if ($published == 'True') {
            $arrWorkOrdersPublished[$woId] = $woId;
            unset($workOrders[$keyRecord]);
            continue;
        }
    }
}

$cfgSite = Zend_Registry::get('CFG_SITE');

$googleKey = $cfgSite->get("site")->get("google_map_key");
$googleClientId = $cfgSite->get("site")->get("google_map_client_id");
?>
<script src="https://maps-api-ssl.google.com/maps?file=api&amp;v=2&amp;client=<?=$googleClientId?>&amp;sensor=false"
            type="text/javascript"></script>
<script src="js/map/google.js" type="text/javascript"></script>

<script type="text/javascript">
function doActionForSelected(thisMarkers)
{
    var emailList = Array();
    for (i=0; i < thisMarkers.length; i++) {
        emailList[emailList.length] = thisMarkers[i].email;
    }

    document.getElementById('emailList').value = emailList.join(',');
    document.getElementById('goToEmail').submit();
}

function initialize() {
    if (GBrowserIsCompatible()) {
        map = new GMap2(document.getElementById("gmap"));
        map.addControl(new GSmallMapControl());
        map.addControl(new GMapTypeControl());
        map.setCenter(new GLatLng(37.4419, -122.1419), 3);

        var lassoIcon = new GIcon(G_DEFAULT_ICON);
        lassoIcon.image = "../images/map/crosshairs.gif";
        lassoIcon.iconSize = new GSize(10,10);
        lassoIcon.iconAnchor = new GPoint(5,5);
        lassoIcon.shadow = "";

        GEvent.addListener(map, "click", function(overlay,latlng) {
        if (latlng && lassoUse == true && lassoPoints.length < 50) {
            lassoPoints[lassoPoints.length] = latlng;
            var point = new GPoint(latlng.x, latlng.y);
            var lassoId = lassoMarkers.length;
            lassoMarkers[lassoId] = new GMarker(point, lassoIcon);
            map.addOverlay(lassoMarkers[lassoId]);
        }
        });
    }
}

$(document).ready(function(){
    initialize();
    initMarkers();
  });

  window.onunload = function() {
                        GUnload();
                    }
    </script>
<style>
#gmap.edit div img { cursor: crosshair; }
</style>
<div style="width:1000px;">
    <div id="gmap" style="width:700px; height:500px; float:left;"></div>
    <div style="float:right; width:290px;">
    </div>
</div>

<script type="text/javascript">
function initMarkers()
{
    if (GBrowserIsCompatible()) {
    var greenIcon = new GIcon(G_DEFAULT_ICON);
    greenIcon.image = "../images/map/marker_green.png";

    var yellowIcon = new GIcon(G_DEFAULT_ICON);
    yellowIcon.image = "../images/map/marker_yellow.png";

    var greyIcon = new GIcon(G_DEFAULT_ICON);
    greyIcon.image = "../images/map/marker_grey.png";

    var blueIcon = new GIcon(G_DEFAULT_ICON);
    blueIcon.image = "../images/map/marker_blue_sky.png";

<?php
$i = 0;
// Assigned
foreach ($arrWorkOrderAssigned as $key=>&$val) {
	if ($arrWoLocations[$key]['Latitude'] == 'NULL') {
        continue;
	}

	$markerName = 'marker' .  $i;

    echo "var point = new GLatLng('" . $arrWoLocations[$key]['Latitude'] . "', '" . $arrWoLocations[$key]['Longitude'] . "');";
    echo "var " . $markerName . " = new GMarker(point, greenIcon);";
    echo "GEvent.addListener(" . $markerName . ",'click', function(){
                " . $markerName . ".openInfoWindow(\"" . $arrWoInfo[$key] . "\");
              });
    map.addOverlay(" . $markerName . ");";
    $i++;
}

// Published
foreach ($arrWorkOrdersPublished as $key=>&$val) {
	if ($arrWoLocations[$key]['Latitude'] == 'NULL') {
        continue;
	}

	$markerName = 'marker' .  $i;
    echo "var point = new GLatLng('" . $arrWoLocations[$key]['Latitude'] . "', '" . $arrWoLocations[$key]['Longitude'] . "');";
    echo "var " . $markerName . " = new GMarker(point);";
    echo "GEvent.addListener(" . $markerName . ",'click', function(){
                " . $markerName . ".openInfoWindow(\"" . $arrWoInfo[$key] . "\");
              });
    map.addOverlay(" . $markerName . ");";
    $i++;
}

// Complete
foreach ($arrWorkOrderComplete as $key=>&$val) {
	if ($arrWoLocations[$key]['Latitude'] == 'NULL') {
        continue;
	}

	$markerName = 'marker' .  $i;
    echo "var point = new GLatLng('" . $arrWoLocations[$key]['Latitude'] . "', '" . $arrWoLocations[$key]['Longitude'] . "');";
    echo "var " . $markerName . " = new GMarker(point, blueIcon);";
    echo "GEvent.addListener(" . $markerName . ",'click', function(){
                " . $markerName . ".openInfoWindow(\"" . $arrWoInfo[$key] . "\");
              });
    map.addOverlay(" . $markerName . ");";
    $i++;
}


// Created Work Orders
foreach ($arrWorkOrders as $key=>&$val) {
	if ($arrWoLocations[$key]['Latitude'] == 'NULL') {
        continue;
	}

	$markerName = 'marker' .  $i;
    echo "var point = new GLatLng('" . $arrWoLocations[$key]['Latitude'] . "', '" . $arrWoLocations[$key]['Longitude'] . "');";
    echo "var " . $markerName . " = new GMarker(point, yellowIcon);";
    echo "GEvent.addListener(" . $markerName . ",'click', function(){
                " . $markerName . ".openInfoWindow(\"" . $arrWoInfo[$key] . "\");
              });
    map.addOverlay(" . $markerName . ");";
    $i++;
}

?>
}
}
</script>
</body>
</html>