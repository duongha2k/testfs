<?php
require_once("../headerStartSession.php");
require_once("../library/FSSession.php");
//FSSession::validateSessionParams(array("UserName" => "UserName"));
require_once("../headerSimple.php");
$displayPMTools = false;
require_once("includes/PMCheck.php");
?>
<html>
<head>
<?php 
if(stristr($_SERVER['HTTP_REFERER'], "wosDetails.php")) {
echo"
<style>
#headerSlider{
	display:hidden;
	}
#maintab{
	display:hidden;
	}
#subSectionLabel{
	display:hidden;
	}
#PMCompany{
	display:hidden;
	}
.form_table{
	margin:0;
	padding:0;
	width:400px;
	}

</style>";
}?>
<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetClientDetails.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
<script type="text/javascript">
var roll;
var detailsWidget;
$('document').ready(function(){
    roll = new FSPopupRoll();
    detailsWidget = new FSWidgetClientDetails({container:'detailsContainer',tab:'addContact'},roll);
    params = {
        'WorkOrder_ID':'<?=$_GET["WO_ID"]?>',
        'Company_ID':'<?=$_GET["v"]?>',
        'email' : '<?=$_GET["email"]?>',
        'techName' : '<?=$_GET["Tech_FName"]?> <?=$_GET["Tech_LName"]?>',
        'Created_By' : '<?=$_GET["fromName"]?>',
        'fromEmail' : '<?=$_GET["fromEmail"]?>',
	'isLB' : '<?=$_GET["isLB"]?>'

    }
    detailsWidget.show({params:params});
	
	var ref = document.referrer.split('?')[0];
	if (ref = 'wosDetails.php'){
		$('body').css('width', '800px');
		$('body').css('margin', '0');
		$('#headerSlider').css('display', 'none');
		$('#maintab').css('display', 'none');
		$('#subSectionLabel').css('display', 'none');
		$('#PMCompany').css('display', 'none');
		$('table.form_table').css('margin', '0');
		$('table.form_table').css('padding', '0');
		$('table.form_table').css('width', '400px');
		$('div.footer_menu').css('display','none');
		$('#content').css('margin', '0 0');
		$('#appHeadTable').css('width', '800px');
		$('#appTable').css('width', '800px');
	}
	

});
</script>
</head>
<body>
<div align="center"><br/><div id="detailsContainer"></div></div>
</body>
</html>