<?php
$page = 'clients';
$option = 'customers';
$selected = 'customersList';
require_once '../headerStartSession.php';

if (strtoupper($_SESSION['tbAdminLevel']) != 'YES') { header("Location: ./"); die();}

$toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
if(!$isFLSManager) {
    $toolButtons[] = 'QuickAssign';
    $toolButtons[] = 'QuickApprove';
}
require ("../header.php");
require ("../navBar.php");
require ("includes/adminCheck.php");

$displayPMTools = true;
require_once("includes/PMCheck.php");
$company = ( !empty($_GET['v']) ) ? $_GET['v'] : $_SESSION['Company_ID'];
?>
<script type="text/javascript">companyID = window._company = '<?=$company?>';</script>
<!--<script type="text/javascript" src="/widgets/js/jquery.min.js"></script>-->
<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetCustomerDetails.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
<script type="text/javascript" src="/widgets/js/functions.js"></script>
<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
<link rel="stylesheet" href="/widgets/css/popup.css" type="text/css" />
<script type="text/javascript" src="/widgets/js/jquery.formatCurrency-1.4.0.js"></script>
<script type="text/javascript">
function deactivate() {
var res = confirm("Do you really wish to delete this customer?");

if(res){
UpdateForm.Active.value="0";
return true;}
else
return false;
}

}
</script>
<script type="text/javascript">
var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";

var roll;
var detailsWidget;

$('document').ready(function(){
    roll = new FSPopupRoll();
    detailsWidget = new FSWidgetCustomerDetails({container:'detailsContainer',tab:'update'},roll);
    detailsWidget.show({params:{id:'<?php echo $_GET['id'];?>', Company_ID:'<?=$company?>'}});
});
</script>
<style>
#h3
</style>
<div id="deletecust" align="right">
<h3><a href="#" onclick="deactivate()">Delete Customer</a></h3>
</div>
<div align="center">

<br/>
<div id="detailsContainer"></div>
</div>

<?php require ("../footer.php"); ?>
