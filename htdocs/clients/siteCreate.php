<?php 
$page = 'clients';
$option = 'users';
$selected = 'usersCreate';
require_once '../headerStartSession.php';

//if (strtoupper($_SESSION['tbAdminLevel']) != 'YES') { header("Location: ./"); die();}

$toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
if(!$isFLSManager) {
    $toolButtons[] = 'QuickAssign';
    $toolButtons[] = 'QuickApprove';
}
require ("../header.php");
require ("../navBar.php");
require ("includes/adminCheck.php");

$displayPMTools = true;
require_once("includes/PMCheck.php");
$company = ( !empty($_GET['v']) ) ? $_GET['v'] : $_SESSION['Company_ID'];
$project_id = $_GET['project_id'];
?>

<script type="text/javascript" src="/widgets/js/FSWidgetSitelistDetails.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetTechSite.js"></script>
<script type="text/javascript">
var roll;
var detailsWidget;
var techSiteWidget;

$('document').ready(function(){
    roll = new FSPopupRoll();
    detailsWidget = new FSWidgetSitelistDetails({container:'detailsContainer',tab:'create'},roll);
    detailsWidget.setParams({Company_ID:'<?=$company?>',Project_ID:'<?=$project_id?>'});
    detailsWidget.show();
});

function showTechSite(element,techIDs)
{    
    var companyID = '<?=$company?>';
    techSiteWidget = new FSWidgetTechSite({TechIDs:techIDs,CompanyID:companyID},roll,element);
    var width = 600;
    if(companyID=='FLS') width=700
    var opt = {};
        opt.width    = width;
        opt.height   = "";
        opt.paddingleft   = 0;
        opt.paddingTop   = 0;
        opt.title    = 'Technician for New Site';
        opt.body     = "<div id='divShowTechsInSite'></div>";
    roll.showNotAjax(element, null, opt);
    techSiteWidget.doRequest();        
}

</script>

<div align="center">
<br/>
<div id="detailsContainer"></div>
</div>


<?php require ("../footer.php"); ?>