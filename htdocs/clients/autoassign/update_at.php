<?php
require_once('includes/AutoAssign.php');
$path = dirname(__FILE__) . '/shells/';
AutoAssign::$baseDir = dirname(__FILE__);

if ($handle = opendir($path)) {
    while (false !== ($file = readdir($handle))) {
        if ($file != "." && $file != ".." && preg_match("/\.sh$/", $file)) {
        	$fileInfo = AutoAssign::getDetailsFromShFile($file);

            if (!empty($fileInfo['woid']) && !empty($fileInfo['time'])) {
                $command = 'at -f '.$path.'do-auto-'.$fileInfo['woid'].'.sh -t '.$fileInfo['time'];
                shell_exec($command);
            }
        }
    }

    echo "<pre>";
    print_r($content);

    closedir($handle);
}