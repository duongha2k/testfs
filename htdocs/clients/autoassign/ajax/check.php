<?php
session_start();
$argc = true;
require_once('../../../library/caspioAPI.php');
require_once('../includes/Categories.php');
require_once('../includes/AutoAssign.php');
require_once('../includes/AutoAssignTotal.php');


$autoAssignTotal = new AutoAssignTotal;

if (!empty($_POST['minrating'])) {
    $autoAssignTotal->setMinSelfRating($_POST['minrating']);
}
if (!empty($_POST['maxdistance'])) {
    $autoAssignTotal->setMaxDistance($_POST['maxdistance']);
}
if (!empty($_POST['category'])) {
    $autoAssignTotal->setCategoryId($_POST['category']);
}
if (!empty($_POST['lat'])) {
    $autoAssignTotal->setLat($_POST['lat']);
}
if (!empty($_POST['long'])) {
    $autoAssignTotal->setLong($_POST['long']);
}
if (!empty($_POST['v'])) {
    $autoAssignTotal->setCompanyId($_POST['v']);
}

if (!empty($_POST['startdate'])) {
    $autoAssignTotal->setStartDate($_POST['startdate']);
}

if (!empty($_POST['starttime'])) {
    $autoAssignTotal->setStartTime($_POST['starttime']);
}

if (!empty($_POST['enddate'])) {
    $autoAssignTotal->setEndDate($_POST['enddate']);
}

if (!empty($_POST['endtime'])) {
    $autoAssignTotal->setEndTime($_POST['endtime']);
}

if (!empty($_POST['preferredonly']) && ($_POST['preferredonly'] == 'true')) {
    $autoAssignTotal->setPreferredOnly(true);
}

echo $autoAssignTotal->getTotal();