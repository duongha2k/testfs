<?php
//include_once("../library/caspioAPI.php");

class Categories
{
    /**
     * Get field name in TR_Master_List by category ID
     */
    public static function getFieldNambeById($categoryId)
    {
        $result = caspioSelectAdv(TABLE_WORK_ORDER_CATEGORIES, 'TechSelfRatingColumn', 'Category_ID = '.$categoryId, "", false, "`", "|");
        if ($result) {
        	if (!empty($result[0])) {
        		return trim($result[0],'`');
        	}
        }    /*
        $categories = array();
        $categories[1]  = 'cablingSelfRating';
        $categories[2]  = 'electronicsSelfRating';
        $categories[3]  = 'constructionSelfRating';
        $categories[4]  = 'desktopSelfRating';
        $categories[5]  = 'telephonySelfRating';
        $categories[6]  = 'networkingSelfRating';
        $categories[7]  = 'routersSelfRating';
        $categories[8]  = 'electricalSelfRating';
        $categories[9]  = 'posSelfRating';
        $categories[10] = 'CCTVSelfRating';
        $categories[11] = 'PrintersSelfRating';
        $categories[12] = 'ServersSelfRating';
        $categories[13] = 'atmSelfRating';
        $categories[14] = 'DigitalSignageSelfRating';
        $categories[15] = 'DslSelfRating';
        $categories[16] = 'FiberCablingSelfRating';
        $categories[17] = 'GeneralWiringSelfRating';
        $categories[18] = 'KioskSelfRating';
        $categories[19] = 'LowVoltageSelfRating';
        $categories[20] = 'RFIDSelfRating';
        $categories[21] = 'SatelliteSelfRating';
        $categories[22] = 'ServerSoftwareSelfRating';
        $categories[23] = 'SiteSurveySelfRating';
        $categories[24] = 'TelephonyVoIPSelfRating';
        $categories[25] = 'WirelessSelfRating';

        if (!empty($categories[$categoryId])) {
            return $categories[$categoryId];
        }*/
    }
    
    public static function test($categoryId)
    {
        $result = caspioSelectAdv(TABLE_WORK_ORDER_CATEGORIES, 'TechSelfRatingColumn', 'Category_ID = '.$categoryId, "", false, "`", "|");
        if ($result) {
        	if (!empty($result[0])) {
        		return trim($result[0],'`');
        	}
        }    
    }
    

	/**
     * Get category name in WO_Categories by category ID
     */
    public static function getCategoryNambeById($categoryId)
    {
        $result = caspioSelectAdv("WO_Categories", 'Category', 'Category_ID = '.$categoryId, "", false, "`", "|");

        if ($result) {
        	if (!empty($result[0])) {
        		return trim($result[0],'`');
        	}
        }
    }

}