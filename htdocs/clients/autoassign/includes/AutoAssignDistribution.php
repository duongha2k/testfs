<?php
include_once("Categories.php");
include_once(dirname(__FILE__)."/../../../library/smtpMail.php");
class AutoAssignDistribution
{
	private $sms   = '';
	private $email = array();

	function __construct(){}

	public function createSMSFor20FirstTechs($details)
	{
		$catName = Categories::getCategoryNambeById($details['categoryId']);

		$msg = "#".substr($details['woId'],0,6)." \n\n
               ".substr($catName."-".$details['Headline'], 0, 48)." \n\n
               ".substr($details['City'], 0, 15).", ".substr($details['State'],0,2).", ".substr($details['Zip'], 0, 5)." \n\n
               ".$details['DateEntered'];
        if (!empty($details['Duration'])) {
        	$msg .=	", DUR-".substr($details['Duration'], 0, 3)." Hrs";
        }

        $msg .= "\n\n $".substr($details['PayMax'],0,6);

        if (!empty($details['Amount_Per'])) {
        	$msg .=	" Per ".$details['Amount_Per'];
        }

		$this->sms = $msg;
	}

	public function createSMSForAssignedTech($details)
	{
		$dur = $details['Duration']=='' ? '' : ", DUR ".substr($details['Duration'], 0, 3)." Hrs";

		$msg = substr("You are Awarded WO ".$details['woName']." \n\n",0,29).
		       "#".substr($details['woId'],0,6).", ".$details['DateEntered'].", $".substr($details['PayMax'],0,6)." $dur \n\n
		       Please login to FieldSolutions.com and accept this WO";
		$this->sms = $msg;
	}

	public function createEmailForClientIfAutoAssignFails($details)
	{
		$this->email['fromName']  = "FieldSolutions.com";
		$this->email['fromEmail'] = "nobody@fieldsolutions.com";

		$coreClass = new Core_Api_Class;
		$additionalFields = $coreClass->getWorkOrderAdditionalFields($details['woId']);
			
		$clientEmail = $additionalFields['ACSNotifiPOC_Email'];
				
/*		if ($details['companyId'] == "CBD" || $details['companyId'] == "suss" || $details['companyId'] == "BW" || $details['companyId'] == "FS") {
		$results = caspioSelectAdv("TR_Client_Projects", "P2TNotificationEmail", "Project_ID = '{$details['Project_ID']}'", "", FALSE, "`", "|" );
		if (!empty($results[0]) && trim($results[0], "`") != "") {
			$clientEmail = trim($results[0], "`");
		}
		}*/
		
		$this->email['eList']     = "tngo@fieldsolutions.com," . $clientEmail;
		$this->email['subject']   = "There were no qualifying bids for Auto-Assign";

		$link = "https://www.fieldsolutions.com/clients/wosDetailsNonTab.php?v=".$details['companyId']."&id=".$details['woId'];

		$this->email['txt']  = "Dear Representative, \n
		                     WIN#".$details['woId']." was not successfully auto-assigned based on your criteria.
		                     The work order has returned to a Non-Auto-Assigned status and remains in the Published tab.
		                     If you wish to review these bids now click this link. $link \n Thank you, \n Field Solutions";

		$this->email['html'] = nl2br($this->email['txt']);
		$this->email['caller'] = "Auto-Assign";
	}

	public function createEmailForClientIfEmptyPayMax($details)
	{
		$this->email['fromName']  = "FieldSolutions.com";
		$this->email['fromEmail'] = "nobody@fieldsolutions.com";
		$this->email['eList']     = $details['SiteEmail'];
		$this->email['subject']   = "Client Offer Information is missing";

		$this->email['txt']       = "Dear ".$details['Site_Contact_name'].",\n"
		                          . "Client Offer Information is missing. Auot-Assign will not be proccessed. \n"
		                          . "Thank you, \n Field Solutions";

		$this->email['html']      = nl2br($this->email['txt']);
		$this->email['caller']    = "Auto-Assign";
	}

	public function sendEmailForOtherBidders($woId, $techId)
	{
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('*'));
        $query->where('WorkOrderID = ?', $woId);
        $query->where('TechID <> ?', $techId);
        $result = Core_Database::fetchAll($query);
        if ( $result && sizeof($result) != 0 ) {
            $vFromName = "Field Solutions";
            $vFromEmail = "admin@fieldsolutions.us";
            $caller = "wosEmailTechWhenAssigned";
            $vSubject = "Thanks for bidding; FS Work Order Assigned to Another Tech"; // Subject Line

            foreach ( $result as $bid ) {
				$techId = $bid['TechID'];
				$FirstName = $bid['Tech_FName'];
				$LastName = $bid['Tech_LName'];				
                $bidDate = new Zend_Date($bid['Bid_Date'], 'yyyy-MM-dd hh:mm:ss');
				$bidDate = $bidDate->toString('MM/dd/yyyy h:mm:ss a');
				$bidAmount = sprintf("%01.2f", $bid['BidAmount']);

                $eList = null;
                $tech = Core_Caspio::caspioSelectAdv('TR_Master_List', 'PrimaryEmail', "TechID='".$techId."'",'');
                if ( !empty($tech) ) $eList = $tech[0]['PrimaryEmail'];
                if ( $eList ) {
                    $message = "
                        Dear $FirstName $LastName,

                        Thank you for your bid of \$$bidAmount on $bidDate in $city, $state, $zipcode on $startDate. This work order has been assigned to another technician.

                        Three things you can do to win more work:
                            1.  Add more details to your profile :  add more skills, certifications and a resume to your profile
                            2.  Build your track record in Field Solutions; bid a few jobs at a  lower  price to get some work history and strong ratings
                            3.  Bid a lower price:  think about your bids, maybe bid a few $ lower than the client's \"offer\" to get noticed.

                        Also, please enroll in the Fujitsu FLS program through FieldSolutions to see more work orders.

                        Thank you and good hunting!
                        Field Solutions Technician Support Team at support@fieldsolutions.com";
                    $htmlmessage = nl2br($message);
                    // send email
                    smtpMailLogReceived($vFromName, $vFromEmail, $eList, $vSubject, $message, $htmlmessage, $caller);
                    // log email
                    //logEmailContact("Email Notification To Outbid Tech", $companyID, $woId, $vFromEmail, $vSubject, $message);
                }
            }
        }
        return true;
	}

	public function sendSMS($numbers)
	{
		/*foreach ($numbers as $number) {
			if ($number) {
				//sened_sms(str_replace('-', '', $number), $this->sms);
			}
		}*/
		return true;
	}

	public function sendEmail()
	{
		//smtpMailLogReceived("no-replies@fieldsolutions.us", "no-replies@fieldsolutions.us", $this->email['eList'], $this->email['subject'], $this->email['txt'], $this->email['html'], $this->email['caller']);
		smtpMailLogReceived("Field Solutions", "no-replies@fieldsolutions.us", $this->email['eList'], $this->email['subject'], $this->email['txt'], $this->email['html'], $this->email['caller']);
		return true;
	}
}
