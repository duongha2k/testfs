<?php
/**
 * Get the total of Techs who mathes the criteria
 * @author Sergey Petkevich
 */

class AutoAssignTotal
{
    private $minSelfRating;
    private $maxDistance;
    private $categoryId;
    private $lat;
    private $long;
    private $companyId;

    private $startDate;
    private $startTime;
    private $endDate;
    private $endTime;

    private $preferredOnly = false;

    public function setMinSelfRating($val)
    {
        $this->minSelfRating = (int)$val;
    }

    public function setMaxDistance($val)
    {
        $this->maxDistance = (int)$val;
    }

    public function setCategoryId($val)
    {
        $this->categoryId = (int)$val;
    }

    public function setLat($val)
    {
        $this->lat = (float)$val;
    }

    public function setLong($val)
    {
        $this->long = (float)$val;
    }

    public function setCompanyId($val)
    {

        $this->companyId = mysql_escape_string($val);
    }

    public function setStartDate($val)
    {
        if (preg_match('#^\d{1,2}\/\d{1,2}\/\d{4}$#', $val)) {
            $this->startDate = $val;
        }
    }

    public function getStartDate()
    {
        return $this->startDate;
    }

    public function setStartTime($val)
    {
        if (preg_match('#^\d{1,2}:\d{1,2}\s?((AM)|(PM))$#i', $val)) {
            $this->startTime = $val;
        }
    }

    public function getStartTime()
    {
        return $this->startTime;
    }

    public function setEndDate($val)
    {
        if (preg_match('#^\d{1,2}\/\d{1,2}\/\d{4}$#', $val)) {
            $this->endDate = $val;
        }
    }

    public function getEndDate()
    {
        return $this->endDate;
    }

    public function setEndTime($val)
    {
        if (preg_match('#^\d{1,2}:\d{1,2}\s?((AM)|(PM)$)#i', $val)) {
            $this->endTime = $val;
        }
    }

    public function getEndTime()
    {
        return $this->endTime;
    }

    public function setPreferredOnly($val)
    {
        $this->preferredOnly = (bool)$val;
    }

    public function getTotal()
    {
        $ratingFieldName = Categories::getFieldNambeById($this->categoryId);

        //$fieldList = 'COUNT(TechID)';
        $fieldList = 'TechID';
        $sqlFilter = '';
        $sqlFilter = ' AcceptTerms = \'Yes\' AND Deactivated = \'0\' AND';

        if (!empty($this->preferredOnly)) {
        	$preferredList = Core_Tech::getClientPreferredTechsArray($wo['companyId']);
        	(!empty($preferredList)) ? $sqlFilter .=  ' TechID IN ('.implode(",",$preferredList). ')' : $sqlFilter .= "";
	    }

		$deniedList = Core_Tech::getClientDeniedTechsArray($this->companyId);
		if (!empty($deniedList))
		    $sqlFilter .= ' TechID NOT IN (' . implode(',',$deniedList) . ')';
		else
			$sqlFilter .= ' TechID NOT IN (0)';

	    $sqlFilter .= ' AND AllowText = 1';

		// Category is chosen and minimum rating is set
		if (!empty($this->minSelfRating) && ($this->minSelfRating > 0) && ($this->categoryId > 0)) {
			$sqlFilter .= ' AND '.$ratingFieldName.' >= ' . $this->minSelfRating;
		} elseif ($this->categoryId > 0) { // Category is chosen (without rating)
            $sqlFilter .= ' AND '.$ratingFieldName.' > 0';
		}
		if (!empty($this->maxDistance) && ($this->maxDistance > 0) && !empty($this->lat) && !empty($this->long)) {
	    	$sqlFilter = ' AND'. $sqlFilter;
			$result = caspioProximitySearchByCoordinatesRaw("TR_Master_List", false, $this->lat, $this->long, "Latitude", "Longitude", "<= ".$this->maxDistance, "3", $fieldList, $sqlFilter, "", false, "|", "`", true);
		} else {
			$result = caspioSelectAdv("TR_Master_List", $fieldList, $sqlFilter, "", false, "`", "|");
		}

        $arrTechs = array();

        foreach ($result as &$tech) {
            $arrTechs[$tech]['id'] = $tech;
        }

        $startDate = $this->getStartDate();
        $strartTime = $this->getStartTime();

        $endDate = $this->getEndDate();
        $endTime = $this->getEndTime();

        // Check if the Techs are not busy
        if ((count($arrTechs) > 0) && !empty($startDate) && !empty($strartTime) && !empty($endDate) && !empty($endTime)) {
            $freeTechs = AutoAssign::isTechFree($arrTechs, $startDate, $strartTime, $endDate, $endTime);

        } else { // Start/End Datetime is not set
            $freeTechs = $result;
        }
        return count($freeTechs);
    }


}