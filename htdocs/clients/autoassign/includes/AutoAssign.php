<?php
$thisDir = dirname(__FILE__);
require_once(dirname(__FILE__) . '/../../../library/caspioAPI.php');
include_once($thisDir."/AutoAssignDistribution.php");
include_once($thisDir."/Categories.php");
include_once($thisDir."/common.php");


class AutoAssign
{
	const API_USER = "auto-assign";
	const API_PASSWORD = "aut0assign210";
	public static $baseDir;

	function __construct(){}


	/*****************************************************************/

	public static function proccessAutoAssign($woId)
	{
		$wo = self::getWorkOrderDetails($woId);
		if (!empty($wo)) {
			$distribution = new AutoAssignDistribution();
			if ($wo['PayMax'] <= 0) {
				$distribution->createEmailForClientIfEmptyPayMax($wo);
				$distribution->sendEmail();
				return false;
			}
			$techsNumbers = self::getFirst20TechsNumbers($wo);
			/*if ($techsNumbers) {
				$distribution->createSMSFor20FirstTechs($wo);
				$distribution->sendSMS(self::getFirst20TechsNumbers($wo));
			} */
			if (empty($wo['MinutesRemainPublished']) || ($wo['MinutesRemainPublished'] < 1)) {
				$wo['MinutesRemainPublished'] = 60;
			}
			self::makeAutoAssignShellFile($woId,$wo['MinutesRemainPublished']);
			return true;
		}
		return false;
	}

	public static function executeAutoAssign($woId){
		$fileName = self::$baseDir.'/shells/do-auto-'.$woId.'.sh';

		if (file_exists($fileName)) {
			unlink($fileName);
		}
		$woDetails = self::getWorkOrderDetails($woId);

		if (!empty($woDetails) && !empty($woDetails['woId'])) {
			$distr = new AutoAssignDistribution();
			$tech = self::getTechForAssign($woDetails);
			$wm = new Core_WorkMarket($woId);
			if ($tech && !$wm->hasOffersWithCounters() && self::doesTechHaveRequiredCertificationForWO($woId, $tech['id'])) {
				if(self::assignTech($woDetails,$tech['id'],$tech['FirstName'],$tech['LastName'],$tech['BidAmount'],$tech['PrimaryEmail'],$tech['PrimaryPhone'],$tech['PrimaryPhoneExt'])) {
					if ($tech['AllowText'] == 'True') {
//						$distr->createSMSForAssignedTech($woDetails);
//						$numbers[0] = $tech['SMSNumber'];
//						$distr->sendSMS($numbers);
					}
//					$distr->sendEmailForOtherBidders($woId, $tech['id']);
					return true;
				}
        	}
			// autoassign failed
			$wm->stopWMAssignment();
        	$distr->createEmailForClientIfAutoAssignFails($woDetails);
            $distr->sendEmail();
		}
		return false;
	}

	/**
	 * Assign Tech who made first bid
	 *
	 * @param int $woId
	 * @param int $techId
	 * @return bool
	 */
	//public static function assignFirstBidTech($woId, $techId, $firstName, $lastName, $bidAmount, $smsNum)
	public static function assignFirstBidTech($woId, $techId, $bidAmount, $bidComments=null)
	{

		$techDetails = self::getTechDetails($techId);
		$woDetails   = self::getWorkOrderDetails($woId);

		if ($woDetails) {
            $isProjectAutoAssign = $woDetails['isProjectAutoAssign'];
            $isWorkOrdersFirstBidder = $woDetails['isWorkOrdersFirstBidder'];
            
			if ($isProjectAutoAssign==1 && $isWorkOrdersFirstBidder == 1) {
                                
                $isTechAppropriateForAutoAssign = self::isTechAppropriateForAutoAssign($woDetails, $techId);

                $doesTechHaveRequiredCertForWO = self::doesTechHaveRequiredCertificationForWO($woId, $techId);
                
                $apiWosClass = new Core_Api_WosClass();
                $doesTechHaveMatchedCOMPUCertForWO = $apiWosClass->doesTechHaveMatchedCOMPUCertForWO($woId, $techId);
                
                $isAutoAssign = 0;
                if($isTechAppropriateForAutoAssign && $doesTechHaveRequiredCertForWO) $isAutoAssign=1;
                if($isAutoAssign==0)
                {
                    if($isTechAppropriateForAutoAssign && $doesTechHaveMatchedCOMPUCertForWO) $isAutoAssign=1;
                }
                
				if ($isAutoAssign==1) 
                {
					$wm = new Core_WorkMarket($woId);
					if(!$wm->hasOffersWithCounters() && self::assignTech($woDetails, $techId, $techDetails['FirstName'], $techDetails['LastName'], $bidAmount, $techDetails['PrimaryEmail'], $techDetails['PrimaryPhone'], $techDetails['PrimaryPhoneExt'],$bidComments)) {
						Core_AutoAssign::clearTimer((int)$woId);
/*						$fileName = dirname( __FILE__ ).'/../shells/do-auto-'.(int)$woId.'.sh';
						if (file_exists($fileName)) {
							$str = self::getDetailsFromShFile('do-auto-'.(int)$woId.'.sh', 'atid');
							unlink($fileName);
							$str = (int)$str;
							echo `atrm $str`;
						}*/
						if (!empty($smsNum)) {
							$distr = new AutoAssignDistribution();
							$sms_numbers[0] = $smsNum;
							$distr->createSMSForAssignedTech($woDetails);
							$distr->sendSMS($sms_numbers);
						}
						return true;
					}
				}
			}
		}
		return false;
	}

	static public function getTechDetails($techId)
	{
		$tehDetails = array();

		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_TECH_BANK_INFO, array('TechID','SMS_Number','AllowText','PrimaryPhone','PrimaryEmail','FirstName','LastName','PrimaryPhoneExt'))
			->where('TechID = ?', $techId);
		$results = $db->fetchRow($select);
		if (!empty($results)) {
    		foreach ($results as $key => $val) {
                $tehDetails[$key] = $val;
    		}
		}
        return $tehDetails;
	}


	/*****************************************************************/





	/**
	 * assign tech by id to work order by id
	 *
	 * @param int $techId
	 * @param int $woId
	 */
	private static function assignTech($wo, $techId, $firstName, $lastName, $bidAmount, $email = null, $phone = null, $phoneExt = null,$bidComments=null)
	{
		/*$valueList = '\''.mysql_escape_string($techId).'\',\''.mysql_escape_string(trim($firstName, '`')).'\',\''.mysql_escape_string(trim($lastName,'`')).'\',\''.mysql_escape_string(trim($bidAmount, '`')).'\','.
		              '\''.mysql_escape_string(trim($email, '`')).'\',\''.mysql_escape_string(trim($phone, '`')).'\',\''.mysql_escape_string(trim($phoneExt, '`')).'\'';

        */
		/*$valueList = '"'.$techId.'","'.trim($firstName, '`').'","'.trim($lastName,'`').'","'.trim($bidAmount, '`').'",'.
		              '"'.trim($email, '`').'","'.trim($phone, '`').'","'.trim($phoneExt, '`').'"';
        */

        if ($techId == 'NULL') {
            $techId = '';
        }

        if ($firstName == 'NULL') {
            $firstName = '';
        }

        if ($lastName == 'NULL') {
            $lastName = '';
        }

        if ($bidAmount == 'NULL') {
            $bidAmount = '';
        }

        if ($email == 'NULL') {
            $email = '';
        }

        if ($phone == 'NULL') {
            $phone = '';
        }

		$api = new Core_Api_AdminClass;

		$apiwo = new API_WorkOrder;
		$apiwo->Tech_ID = $techId;
		$apiwo->Tech_Bid_Amount = $bidAmount;
        $apiwo->bid_comments = $bidComments;
		
		$r = $api->updateWorkOrder(self::API_USER, self::API_PASSWORD, $wo['woId'], $apiwo);

//		print_r($r);
//		print_r($apiwo);


//        self::afterAssign($techId, $wo['woId'], $wo['Project_ID'], $wo['companyName'], $wo['companyId']);
		return true;
	}

	/**
	 * make Auto Assign Shell File
	 *
	 * @param int $woId
	 * @return bool
	 */
	private static function makeAutoAssignShellFile($woId,$delay)
	{
		Core_AutoAssign::setTimer($woId, $delay);
/*		$time = date('mdHi', (($delay * 60) + time()));
		$docPath = self::$baseDir;
		$fileName = $docPath.'/shells/do-auto-'.$woId.'.sh';
		
		if (file_exists($fileName)) {
			self::removeShellFileIfExist($woId);
		}

		$fileText .= 'php -f '.$docPath.'/includes/executeAutoAssign.php '.$woId;//.' '.$atId;
		$f = fopen($fileName, "w");
		fwrite($f, $fileText);
		fclose($f);

		echo `at -f $docPath/shells/do-auto-$woId.sh -t $time`;

		$atq = shell_exec("atq");

		echo "at -f $docPath/shells/do-auto-$woId.sh -t $time";

		if (strlen($atq)) {
			$res = explode('a apache', $atq);
			$result = array();
			foreach ($res as $val) {
				preg_match("/[0-9]+/", $val, $buf);
				$result[] = (int)$buf[0];
			}
			sort($result);
			$atId = end($result);
			$f = fopen($fileName, "a+");
			fwrite($f, "\n#atid:".$atId);
			fwrite($f, "\n#time:".$time);
			fclose($f);
		}*/
	}

	/**
	 * get WO details
	 *
	 * @param int $woId
	 * @return mixed
	 */
	public static function getWorkOrderDetails($woId)
	{
        $woTech = new Core_Api_AdminClass();
        
        $filters = new API_AdminWorkOrderFilter;
        $filters->TB_UNID = (int)$woId;
        $filters->Deactivated = false;
        $filters->Tech_ID_IS_NULL =  true;
        $filters->isProjectAutoAssign = true;

        $result = null;

		$fieldList = 'WIN_NUM,WO_ID,Deactivated,Latitude,Longitude,Company_ID,'
		           . 'isWorkOrdersFirstBidder,MinutesRemainPublished,MinimumSelfRating,MaximumAllowableDistance,'
		           . 'WO_Category_ID,PayMax,isProjectAutoAssign,StartDate,EndDate,StartTime,EndTime,'
		           . 'Headline,City,State,Zipcode,DateEntered,Duration,Username,SiteEmail,Site_Contact_Name,Amount_Per,'
		           . 'Company_Name,Project_Name,P2TPreferredOnly,Project_ID,EmergencyEmail,ResourceCoordinatorEmail';
        // get list of orders
        $woRes = $woTech->getWorkOrders(
            self::API_USER, 
            self::API_PASSWORD,
            $fieldList,
			"",
			"",
			"",
            $filters
        );
        
		$result = array();
         
        
        if ( $woRes->success && !empty($woRes->data) ) {
            $result = (array)$woRes->data[0];

	    $result['woId']   = $result['WIN_NUM'];
	    $result['woName']   = $result['WO_ID'];
	    $result['arrWoLocations']['Latitude']  = $result['Latitude'];
	    $result['arrWoLocations']['Longitude'] = $result['Longitude'];

	    $result['companyId']                = $result['Company_ID'];
	    $result['companyName']              = $result['Company_Name'];
	    $result['projectName']              = $result['Project_Name'];
	    $result['categoryId']               = (int)$result['WO_Category_ID'];
	    $result['Site_Contact_name']        = $result['Site_Contact_Name'];

//		print_r($result);
	}
	return $result;
	}
	
	/**
	 * returns true if tech has certifications needed for the work order
	 *
	 * @param array $wo
	 * @return mixed
	 */
	public static function doesTechHaveRequiredCertificationForWO($win, $techid, $is_debug = false) {
        $db = Core_Database::getInstance();
		$brt_tech = false;
		$boot_camp_tech = false;

		//---------------------------------------------------------------------
		//Filter functions
		//$wcs = new Core_WorkOrderCertifications();
		//$certswo = $wcs->getWorkOrderCertification($win);
		$filter_required = function ($x) { return $x == 2; };
		$filter_integer = function (&$value) { $value = intval ($value); };
		$filter_keys = function ($array) {
			return !empty($array) ? array_keys($array) : array();
		};
		$debug = function ($heading, $data) { echo ($heading.":<br/>".var_export($data, true)."<br/><br/>"); };

		//---------------------------------------------------------------------
		//Load work order cert requirements
		$project = new Core_Api_ProjectClass ();
		$certs = $project->loadP2TCertRequirements ($win, false);

		//---------------------------------------------------------------------
		//Load all tech data (profile, certs, tags and expertise)
		//$search  = new Core_Tech;
		//$techInfo = $search->getProfile($techid);
        $query = $db->select();
        $query->from (array ('t'=>"TechBankInfo"), array (
			"MCSE",
			"CCNA",
			"DellCert",
			"HP_CertNum",
			"BICSI",
			"CompTIA",
			"Bg_Test_Pass_Lite",
			"Bg_Test_Pass_Full",
			"DrugPassed",
			"SilverDrugPassed",
			"NACI",
			"FS_Mobile_Date",
			"InterimSecClear",
			"FullSecClear",
			"TopSecClear"
		));
        $query->joinLeft (array ("tx"=>"tech_ext"), "t.TechID = tx.TechID", array (
			"I9Status"
		));
        $query->where ("t.TechID = ?", $techid);
        $techInfo = Core_Database::fetchAll ($query);
		$techInfo = $techInfo[0];
		if ($is_debug) $debug ("Tech Info", $techInfo);

		$ind_certs_select = $db->select();
		$ind_certs_select->from(Core_Database::TABLE_CERTIFICATIONS, array ("name", "label"));
		$ind_certs_select->where("hide = 0");
		$ind_certs_results = Core_Database::fetchAll($ind_certs_select);

        $ind_certs_select = $db->select();
        $ind_certs_select->from (array ('c'=>"certifications"), array (
			"name",
			"label"
		));
        $ind_certs_select->joinLeft (array ("tc" => "tech_cartification"), "c.id = tc.certification_id");
        $ind_certs_select->where ("tc.TechID = ?", $techid);
        $ind_certs_results = Core_Database::fetchAll ($ind_certs_select);
		$tech_industry = array ();
		foreach ($ind_certs_results as $record) {
			$tech_industry[] = $record["name"];
			if ($record["name"] == "BlueRibbonTechnician") $brt_tech = true;
			if ($record["name"] == "TBC") $boot_camp_tech = true;
		}
		if ($techInfo["MCSE"] == 1) $tech_industry[] = "MCSE";
		if ($techInfo["CCNA"] == 1) $tech_industry[] = "CCNA";
		if (!empty ($techInfo["DellCert"]) && $techInfo["DellCert"] != "None") $tech_industry[] = "DellCert";
		if (!empty ($techInfo["HP_CertNum"])) $tech_industry[] = "HP_CertNum";
		if ($techInfo["BICSI"] == 1) $tech_industry[] = "BICSI";
		if (!empty ($techInfo["CompTIA"])) $tech_industry[] = "CompTIA";
		if ($is_debug) $debug ("Tech Industry Credentials", $tech_industry);

		$FSTagClass = new Core_Api_FSTagClass();
		$certstech_results = $FSTagClass->getFSTagList_forTech ($techid);
		$certstech = array ();
		foreach ($certstech_results as $record) {
			if ($record["TechId"] == $techid)
				$certstech[] = $record["FSTagId"];
		}
		if ($is_debug) $debug ("Tech Client Certifications", $certstech);

		$FSExperts = new Core_Api_FSExpertClass ();
		$tech_expert_all = $FSExperts->getFSExpertListForTech ($techid);
		$tech_expert = array ();
		foreach ($tech_expert_all as $record) {
			array_push ($tech_expert, $record["FSExpertId"]);
		}
		if ($is_debug) $debug ("Tech Expertise", $tech_expert);

		//---------------------------------------------------------------------
		//Public certifications
		$wo_public = $filter_keys (array_filter ($certs["public"], $filter_required));
		if ($is_debug) $debug ("Checking Public Certifications", $wo_public);

		$client_cred_extras = array ();

		$diff_public = true;
		foreach ($wo_public as $cert) {
			switch ($cert) {
				case "Bg_Test_Pass_Lite":
					if ($techInfo["Bg_Test_Pass_Lite"] != "Pass") {
						$diff_public = false;
						if ($is_debug) $debug ("Bg_Test_Pass_Lite", "Failed");
					}
					break;
				case "Bg_Test_Pass_Full":
					if ($techInfo["Bg_Test_Pass_Full"] != "Pass") {
						$diff_public = false;
						if ($is_debug) $debug ("Bg_Test_Pass_Full", "Failed");
					}
					break;
				case "DrugPassed":
					if ($techInfo["DrugPassed"] != "1") {
						$diff_public = false;
						if ($is_debug) $debug ("DrugPassed", "Failed");
					}
					break;
				case "SilverDrugPassed":
					if ($techInfo["SilverDrugPassed"] != "1") {
						$diff_public = false;
						if ($is_debug) $debug ("SilverDrugPassed", "Failed");
					}
					break;
				case "USAuthorized":
					if ($techInfo["I9Status"] == "0" || empty ($techInfo["I9Status"])) {
						$diff_public = false;
						if ($is_debug) $debug ("I9Status", "Failed");
					}
					break;
				case "BRT":
					if (!$brt_tech) $diff_public = false;
					if ($is_debug) $debug ("BRT", "Failed");
					break;
				case "TBC":
					if (!$boot_camp_tech) $diff_public = false;
					if ($is_debug) $debug ("TBC", "Failed");
					break;
				case "NACI":
					if ($techInfo["NACI"] != "1") {
						$diff_public = false;
						if ($is_debug) $debug ("NACI", "Failed");
					}
					break;
				case "FS_Mobile_Date":
					if (empty ($techInfo["FS_Mobile_Date"])) {
						$diff_public = false;
						if ($is_debug) $debug ("FS_Mobile_Date", "Failed");
					}
					break;
				case "InterimSecClear":
					if ($techInfo["InterimSecClear"] != "1") {
						$diff_public = false;
						if ($is_debug) $debug ("InterimSecClear", "Failed");
					}
					break;
				case "FullSecClear":
					if ($techInfo["FullSecClear"] != "1") {
						$diff_public = false;
						if ($is_debug) $debug ("FullSecClear", "Failed");
					}
					break;
				case "TopSecClear":
					if ($techInfo["TopSecClear"] != "1") {
						$diff_public = false;
						if ($is_debug) $debug ("TopSecClear", "Failed");
					}
					break;
				case "2":
				case "6":
				case "64":
					$client_cred_extras[] = $cert;
			}
		}

		//---------------------------------------------------------------------
		//Client credentials
		$certswo = array_filter ($certs["client"], $filter_required);
		$certswo = $filter_keys ($certswo);
		if ($is_debug) $debug ("Checking Client Credentials", $certswo);

		//Some tags are selected as public certs and are added here...
		if (!empty ($client_cred_extras))
			$certswo = array_merge ($certswo, $client_cred_extras);
		array_walk ($certstech, $filter_integer);

		$diff_certs = array_diff($certswo, $certstech);
		if ($is_debug)
			foreach ($diff_certs as $cert)
				$debug ($cert, "Failed");


		//---------------------------------------------------------------------
		//Expert tags
		$wo_expert = $filter_keys (array_filter ($certs["expert"], $filter_required));
		if ($is_debug) $debug ("Checking Expert tags", $wo_expert);

		$diff_expert = array_diff($wo_expert, $tech_expert);
		if ($is_debug)
			foreach ($diff_expert as $cert)
				$debug ($cert, "Failed");

		//---------------------------------------------------------------------
		//Industry certifications
		$wo_industry = $filter_keys (array_filter ($certs["industry"], $filter_required));
		if ($is_debug) $debug ("Checking Industry Certifications", $wo_industry);

		$diff_industry = array_diff($wo_industry, $tech_industry);
		if ($is_debug)
			foreach ($diff_industry as $cert)
				$debug ($cert, "Failed");

		return count($diff_certs) == 0 && 
			$diff_public == true && 
			count($diff_expert) == 0 && 
			count($diff_industry) == 0;
	}

	/**
	 * get tech id for assign
	 *
	 * @param array $wo
	 * @return mixed
	 */
	public static function getTechForAssign($wo)
	{
		$ratingFieldName = Categories::getFieldNambeById($wo['categoryId']);

		$fieldList = array('TechID','SMS_Number','AllowText','PrimaryPhone','PrimaryPhoneExt', 'PrimaryEmail');
		
		$payMax = (float)$wo['PayMax'];
		
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('TechID'));
        $query->where('Hide = 0 AND WorkOrderID = ?', $wo['woId']);
        $query->where('BidAmount > 1.00 AND BidAmount <= ?', $payMax);
        $result = Core_Database::fetchAll($query);
        $techIds = array();
        if ( $result ) {
            foreach ( $result as $res ) $techIds[] = $res['TechID'];
        }
        $techIdsFilter = (sizeof($techIds) != 0) ? implode(',', $techIds) : '-1';

		$sqlFilter = '';
        $sqlFilter .= ' AcceptTerms = \'Yes\' AND Deactivated = \'0\' AND';
		$sqlFilter .= ' TechID IN ('.$techIdsFilter.')';

		// Preferred only
		if ($wo['P2TPreferredOnly'] == 'True') {
			$preferredList = Core_Tech::getClientPreferredTechsArray($wo['companyId']);
        	(!empty($preferredList)) ? $sqlFilter .=  ' AND TechID IN ('.implode(",",$preferredList). ')' : $sqlFilter .= "";
	    }

		$deniedList = Core_Tech::getClientDeniedTechsArray($wo['companyId']);
		if (!empty($deniedList))
		    $sqlFilter .= ' AND TechID NOT IN (' . implode(',',$deniedList) . ')';

		if (!empty($wo['MinimumSelfRating']) && ($wo['MinimumSelfRating'] > 0) && !empty($wo['categoryId']) && ($wo['categoryId'] > 0)) {
			$sqlFilter .= ' AND '.$ratingFieldName.' >= '.$wo['MinimumSelfRating'];
		} elseif (!empty($wo['categoryId'])) {
			// ignore self rating if min self rating is NULL or 0
			//$sqlFilter .= ' AND '.$ratingFieldName.' > 0 ';
		}

		if ($wo['MaximumAllowableDistance'] != 'NULL' && $wo['arrWoLocations']['Latitude'] !='NULL' && $wo['arrWoLocations']['Longitude'] != 'NULL') {
			$sqlFilter = ' AND'. $sqlFilter;
			$result = caspioProximitySearchByCoordinatesRaw ("TR_Master_List", false, $wo['arrWoLocations']['Latitude'], $wo['arrWoLocations']['Longitude'], "Latitude", "Longitude", "<= ".$wo['MaximumAllowableDistance'], "3", $fieldList, $sqlFilter, "calculated_distance ASC", true, "|", "`", true);
		} else {
			$result = caspioSelectAdv("TR_Master_List", $fieldList, $sqlFilter, "", false, "`", "|");
		}


		if ($result) {
			if (empty($result[0])) {
				return false;
			}

			$techs = array();
			$freeTechs = array();

			foreach ($result as $key=>$val) {
				if (is_array($val)) {
					$record = array_values($val);
					$id = $record[0];
				}
				else {
					$record = explode("|", $val);
					$id = trim($record[0], "`");
				}
				$freeTechs[] = $id;
				$techs[$key]['id'] = $id;
				$techInfo[$id]['num'] = trim($record[1], "`");
				$techInfo[$id]['AllowText'] = trim($record[2], "`");

				$techInfo[$id]['PrimaryPhone'] = trim($record[3], "`");
				$techInfo[$id]['PrimaryPhoneExt'] = trim($record[4], "`");
				$techInfo[$id]['PrimaryEmail'] = trim($record[5], "`");
			}

			if (!empty($wo['StartDate']) && !empty($wo['EndDate']) && !empty($wo['StartTime']) && !empty($wo['EndTime'])) {
				$freeTechs = self::isTechFree($techs, $wo['StartDate'],$wo['StartTime'],$wo['EndDate'],$wo['EndTime']);
			}
			if (empty($freeTechs)) {
				return false;
			}
			if ($wo['isWorkOrdersFirstBidder'] && !empty($ids)) { // Not in use
                $db = Core_Database::getInstance();
                $query = $db->select();
                $query->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('TechID'));
                $query->where('Hide = 0 AND WorkOrderID = ?', $wo['woId']);
                $query->where('TechID IN (?)', $ids);
                $query->order('Bid_Date');
                $result = Core_Database::fetchAll($query);
                if ( $result && sizeof($result) != 0 ) return $result[0];
			} else {
                $db = Core_Database::getInstance();
                $query = $db->select();
                $query->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('id','TechID','BidAmount','Tech_FName','Tech_LName','Bid_Date'));
                $query->where('Hide = 0 AND WorkOrderID = ?', $wo['woId']);
                $query->where('TechID IN (?)', $freeTechs);
                $query->order('id');
                $techIds = Core_Database::fetchAll($query);
                
				if ($techIds) {
					if (empty($techIds[0])) {
						return false;
					}
					$minBid = $techIds[0];
					$minBidTechId = (int)trim($minBid['TechID'], "`");

					$minBidTech['id'] = $minBidTechId;
					$minBidTech['FirstName'] = trim($minBid['Tech_FName'], "`");
					$minBidTech['LastName']  = trim($minBid['Tech_LName'], "`");
					$minBidTech['BidAmount'] = trim($minBid['BidAmount'], "`");
					$minBidTech['SMSNumber'] = $techInfo[$minBidTechId]['num'];
					$minBidTech['AllowText'] = $techInfo[$minBidTechId]['AllowText'];

					$minBidTech['PrimaryPhone'] = $techInfo[$minBidTechId]['PrimaryPhone'];
					$minBidTech['PrimaryPhoneExt'] = $techInfo[$minBidTechId]['PrimaryPhoneExt'];
					$minBidTech['PrimaryEmail'] = $techInfo[$minBidTechId]['PrimaryEmail'];

					foreach ($techIds as $val) {
						$record = $val;
						$curBid =trim($record['BidAmount'], "`");
						$curTechId = trim($record['TechID'], "`");
						if ($curBid < $minBid && self::doesTechHaveRequiredCertificationForWO($wo['woId'], $curTechId)) {
							$minBid = $curBid;
							$minBidTech['id'] = $curTechId;
							$minBidTech['FirstName'] = trim($record['Tech_FName'], "`");
							$minBidTech['LastName'] = trim($record['Tech_LName'], "`");
							$minBidTech['BidAmount'] = $curBid;
							$minBidTech['SMSNumber'] = $techInfo[$curTechId]['num'];
							$minBidTech['AllowText'] = $techInfo[$curTechId]['AllowText'];

							$minBidTech['PrimaryPhone'] = $techInfo[$minBidTechId]['PrimaryPhone'];
        					$minBidTech['PrimaryPhoneExt'] = $techInfo[$minBidTechId]['PrimaryPhoneExt'];
        					$minBidTech['PrimaryEmail'] = $techInfo[$minBidTechId]['PrimaryEmail'];
						}
					}
					return $minBidTech;
				}
			}
		}
		return false;
	}

	/**
	 * check Tech for auto assign
	 *
	 * @param array $wo
	 * @param int $techId
	 * @return bool
	 */
	private static function isTechAppropriateForAutoAssign($wo, $techId)
	{
		$ratingFieldName = Categories::getFieldNambeById($wo['categoryId']);

		$fieldList = 'TechID';

        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('TechID'));
        $query->where('Hide = 0 AND WorkOrderID = ?', $wo['woId']);
        $query->where('BidAmount > 1.00 AND BidAmount <= ?', (float)$wo['PayMax']);
        $query->where('TechID = ?', $techId);
        $result = Core_Database::fetchAll($query);
        $techIds = array();
        if ( $result ) {
            foreach ( $result as $res ) $techIds[] = $res['TechID'];
        }
        $techIdsFilter = (sizeof($techIds) != 0) ? implode(',', $techIds) : '-1';

		$sqlFilter = '';
		$sqlFilter .= ' AcceptTerms = \'Yes\' AND Deactivated = \'0\' AND';
        $sqlFilter .= ' TechID = '.$techId;
        $sqlFilter .= ' AND TechID IN ('.$techIdsFilter.')';

         if ($wo['P2TPreferredOnly'] == 'True') {
            $preferredList = Core_Tech::getClientPreferredTechsArray($wo['companyId']);
        	(!empty($preferredList)) ? $sqlFilter .=  ' AND TechID IN ('.implode(",",$preferredList). ')' : $sqlFilter .= "";
	   
         }

		$deniedList = Core_Tech::getClientDeniedTechsArray($wo['companyId']);
		if (!empty($deniedList))
		    $sqlFilter .= ' AND TechID NOT IN (' . implode(',',$deniedList) . ')';

		if (!empty($wo['MinimumSelfRating']) && $wo['categoryId']) {
			$sqlFilter .= ' AND '.$ratingFieldName.' >= ' . $wo['MinimumSelfRating'];
		} elseif ($wo['categoryId']) {
			// ignore self rating if min self rating is NULL or 0
			//$sqlFilter .= ' AND '.$ratingFieldName.' > 0 ';
		}

		if ($wo['MaximumAllowableDistance'] != 'NULL'&& $wo['arrWoLocations']['Latitude'] != 'NULL' && $wo['arrWoLocations']['Longitude'] !='NULL') {
	    	$sqlFilter = ' AND'. $sqlFilter;
			$result = caspioProximitySearchByCoordinatesRaw ("TR_Master_List", false, $wo['arrWoLocations']['Latitude'], $wo['arrWoLocations']['Longitude'], "Latitude", "Longitude", "<= ".$wo['MaximumAllowableDistance'], "3", $fieldList, $sqlFilter, "calculated_distance ASC", true, "|", "`", true);
		} else {
			$result = caspioSelectAdv("TR_Master_List", $fieldList, $sqlFilter, "", false, "`", "|");
		}

		if ($result) {
			if (!empty($result[0])) {
				if (is_array($result[0])) {
					$result = array_values($result[0]);
				}
				else
					$result = explode('|', $result[0]);
				$tech[0]['id'] = $result[0];
				if (!empty($wo['StartDate']) && !empty($wo['EndDate']) && !empty($wo['StartTime']) && !empty($wo['EndTime'])) {
					$freeTechs = self::isTechFree($tech,$wo['StartDate'],$wo['StartTime'],$wo['EndDate'],$wo['EndTime']);
					if (!empty($freeTechs)) {
						return true;
					}
				}else{
					return true;
				}

			}
		}
		return false;
	}

	/**
	 * Function for logical comperation of time when Tech is busy
	 *
	 * @param array $techs
	 * @param str $startDate
	 * @param str $startTime
	 * @param str $endDate
	 * @param str $endTime
	 * @return array
	 */
	 public static function isTechFree($techs,$startDate,$startTime,$endDate,$endTime)
	{
		$startDate = explode(' ', $startDate);
		$endDate = explode(' ', $endDate);

		$startTotalTime = strtotime($startDate[0].' '.$startTime) - 60*60;
		$endTotalTime   = strtotime($endDate[0].' '.$endTime) + 60*60;

		$bufStartTime = explode('|',date('m/d/Y|h:i A', $startTotalTime));
		$bufEndTime   = explode('|',date('m/d/Y|h:i A', $endTotalTime));

		$startDate = $bufStartTime[0];
		$startTime = $bufStartTime[1];
		$endDate   = $bufEndTime[0];
		$endTime   = $bufEndTime[1];

		$result = array();
		$res    = array();
        $techsId = array();

		foreach ($techs as &$tech) {
            $techsId[] = (int)$tech['id'];
            $result[$tech['id']] = $tech['id'];
		}

		if (count($techsId) > 0) {
			$api = new Core_Api_AdminClass;
			$filters = new API_AdminWorkOrderFilter;
			$filters->Tech_ID_List = $techsId;
			$filters->WO_State = WO_STATE_ASSIGNED;
            $fieldList = 'WIN_NUM,StartDate,StartTime,EndDate,EndTime,Tech_ID';
			
			$woRes = $api->getWorkOrders(
	            self::API_USER, 
	            self::API_PASSWORD,
	            $fieldList,
				"",
				"",
				"",
	            $filters
	        );
			
	        if ( $woRes->success && !empty($woRes->data) ) {
	            $res = (array)$woRes->data;
			}
		}
		foreach ($res as $record) {
			$thisWoId = $record->WIN_NUM;
			$thisWoStartDate = $record->StartDate;
			$thisStartTime = $record->StartTime;
			$thisEndDate = $record->EndDate;
			$thisEndTime = $record->EndTime;
			$thisTechId = $record->Tech_ID;

//			$thisWoStartDate = explode(" ", $thisWoStartDate);
//            $thisEndDate = explode(" ", $thisEndDate);

            $thisWoStartDateTime = strtotime($thisWoStartDate.' '.$thisStartTime);
			if (empty($thisEndDate)) $thisWoEndDateTime = $thisWoStartDateTime + (60 * 60 * 8); // make end time 8 hrs from start if no end date / time entered
			else
	            $thisWoEndDateTime = strtotime($thisEndDate.' '.$thisEndTime);

            if (!empty($thisWoStartDateTime)) {
				if (($thisWoStartDateTime == $startTotalTime) || ($thisWoStartDateTime > $startTotalTime ? $thisWoStartDateTime <= $endTotalTime : $startTotalTime <= $thisWoEndDateTime)) {
					//echo "$thisWoId ---- $thisTechId --- " . date("m/d/Y H:i", $thisWoStartDateTime) . " -- " . date("m/d/Y H:i", $thisWoEndDateTime) . " -- " . date("m/d/Y H:i", $startTotalTime) . " -- " . date("m/d/Y H:i", $endTotalTime) . "\n\n";
					// check for time range interest
	                unset($result[$thisTechId]);
	            }
		    }
		}
		return $result;
	}


	/**
	 * get SMS numbers of 20 first techs for auto assign distribution
	 *
	 * @param array $wo
	 * @return array
	 */
	private static function getFirst20TechsNumbers($wo)
	{
		$ratingFieldName = Categories::getFieldNambeById($wo['categoryId']);

		$fieldList = 'TechID';
		$sqlFilter = ' AcceptTerms = \'Yes\' AND Deactivated = \'0\'';

		if ($wo['P2TPreferredOnly'] == 'True') {
			$preferredList = Core_Tech::getClientPreferredTechsArray($wo['companyId']);
        	(!empty($preferredList)) ? $sqlFilter .=  ' AND TechID IN ('.implode(",",$preferredList). ')' : $sqlFilter .= "";
	   	    }

		$deniedList = Core_Tech::getClientDeniedTechsArray($wo['companyId']);
		if (!empty($deniedList))
		    $sqlFilter .= ' AND TechID NOT IN (' . implode(',',$deniedList) . ')';
			
	    $sqlFilter .= ' AND SMS_Number IS NOT NULL';
	    $sqlFilter .= ' AND AllowText = 1';

		if (!empty($wo['MinimumSelfRating']) && $wo['MinimumSelfRating'] >0 && $wo['categoryId'] >0) {
			$sqlFilter .= ' AND '.$ratingFieldName.' >= ' . $wo['MinimumSelfRating'];
		} elseif ($wo['categoryId'] > 0) {
			// ignore self rating if min self rating is NULL or 0
			//$sqlFilter .= ' AND '.$ratingFieldName.' > 0 ';
		}

		if ($wo['MaximumAllowableDistance'] != 'NULL' && $wo['arrWoLocations']['Latitude'] != 'NULL' && $wo['arrWoLocations']['Longitude'] != 'NULL') {
			$sqlFilter = ' AND'. $sqlFilter;
			$result = caspioProximitySearchByCoordinatesRaw ("TR_Master_List", false, $wo['arrWoLocations']['Latitude'], $wo['arrWoLocations']['Longitude'], "Latitude", "Longitude", "<= ".$wo['MaximumAllowableDistance'], "3", $fieldList, $sqlFilter, "calculated_distance ASC", true, "|", "`", true);

		} else {
			$result = caspioSelectAdv("TR_Master_List", $fieldList, $sqlFilter, "", false, "`", "|");

		}
		if ($result) {
			if (!empty($result[0])) {
				$techs = array();
				$freeTechs = array();
				foreach ($result as $key=>$val) {
					if (is_array($val)) {
						$record = array_values($val);
					}
					else {
						$record = explode("|", $val);
					}
					$techs[$key]['id'] = (int)trim($record[0],'`');
					$freeTechs[] = (int)trim($record[0],'`');
					if ($key == 19) break;
				}

				if (!empty($wo['StartDate']) && !empty($wo['EndDate']) && !empty($wo['StartTime']) && !empty($wo['EndTime'])) {
					$freeTechs = self::isTechFree($techs, $wo['StartDate'],$wo['StartTime'],$wo['EndDate'],$wo['EndTime']);
				}
				if (!empty($freeTechs)) {
					$ids = '';
					foreach ($freeTechs as $key=>$tech) {
						$ids .= $key == 0 ? $tech : ','.$tech;
					}
					if (!empty($ids)) {
						$sqlFilter = 'TechID IN ('.$ids.')';
					} else {
						$sqlFilter = '1=2';
					}
					$smsNumbers = caspioSelectAdv("TR_Master_List", 'SMS_Number', $sqlFilter, "", false, "`", "|");
					foreach ($smsNumbers as &$num) {
						$num = trim($num,'`');
					}

					if ($smsNumbers) {
						if (!empty($smsNumbers[0])) {
							return $smsNumbers;
						}
					}
				}
			}
		}
		return false;
	}

	private static function getWorkOrderBids($woId)
	{
        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('id'));
        $query->where('WorkOrderID = ? AND Hide = 0', $woId);
        $result = Core_Database::fetchAll($query);
        if ( !$result || sizeof($result) == 0 ) return false;
        else true;
	}


	public function getDetailsFromShFile($file, $info = null)
    {
        $shellPath = self::$baseDir.'/shells';
        $content = file_get_contents($shellPath.'/'.$file);
        $result = array();

        preg_match('/atid:(\d+)/i', $content, $atInfo);

        if (!empty($atInfo[1])) {
            $result['atid'] = $atInfo[1];
        } else {
            $result['atid'] = NULL;
        }

        preg_match('/time:(\d+)/i', $content, $timeInfo);

        if (!empty($timeInfo[1])) {
            $result['time'] = $timeInfo[1];
        } else {
            $result['time'] = NULL;
        }

        preg_match('/do-auto-(\d+)/', $file, $woId);

        if (!empty($woId[1])) {
            $result['woid'] = $woId[1];
        } else {
            $result['woid'] = 0;
        }

        if ($info !== null && array_key_exists($info, $result)) {
            return $result[$info];
        }

        return $result;
    }

    static public function afterAssign($techID, $tbUNID, $projectID, $companyName, $companyID, $type='update')
    {
/*        $url = "https://www.fieldsolutions.com/clients/wosAdapterAssigned.php?techID=".urlencode($techID)."&tbUNID=".urlencode($tbUNID)."&projectID=".urlencode($projectID)."&company=".urlencode($companyName)."&CompanyID=".urlencode($companyID)."&type=update";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url); // set url to post to
        curl_setopt($ch, CURLOPT_FAILONERROR, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);// allow redirects
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
        curl_setopt($ch, CURLOPT_TIMEOUT, 20); // times out after 4s
        curl_setopt($ch, CURLOPT_POST, 0); // set POST method
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);


        $result = curl_exec($ch); // run the whole process
        curl_close($ch);*/
    }
    
    
    static public function removeShellFileIfExist($woId) {
		Core_AutoAssign::clearTimer($woId);
/*        $fileName = dirname( __FILE__ ).'/../shells/do-auto-'.$woId.'.sh';
		
        if (file_exists($fileName)) {
            $str = self::getDetailsFromShFile('do-auto-'.$woId.'.sh', 'atid');
            unlink($fileName);
            $str = (int)$str;
            echo `atrm $str`;
        }*/
    }
}
