<?php

$argc =true;
$rootPath = realpath(dirname(__FILE__) . "/../../../");
include_once($rootPath . '/clients/autoassign/includes/common.php');
include_once($rootPath . '/clients/autoassign/includes/AutoAssign.php');
include_once($rootPath . "/library/caspioAPI.php");
include_once($rootPath . "/library/timeStamps2.php");


if (!empty($_SERVER['argv'][1])) {
	if ($_SERVER['argv'][1] != 'batch')
		$wins = array($_SERVER['argv'][1]);
	else {
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_P2T_TIMER, array('WIN_NUM'))
			->where('BatchId = ?', $_SERVER['argv'][2]);
		$wins = $db->fetchCol($select);
		if (empty($wins)) die();
	}
	AutoAssign::$baseDir = $rootPath . '/clients/autoassign';
	foreach ($wins as $win) {
		$res = AutoAssign::executeAutoAssign($win);
	}
}