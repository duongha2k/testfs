<?php
header("Content-Type: text/html; charset=utf-8");
$page = 'clients';
$option = 'email';
$selected = 'recruitmentemail';

if ($isFLSManager)
{
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
} else
{
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
}
$isLB = !empty($_GET['isLB']);
?>
<?php require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php'); ?>
<?php
if (!$isLB)
{
    require ("../header.php");
    require_once("../navBar.php");
}
else
    require ("../headerSimple.php");
?>
<?php require ("includes/adminCheck.php"); ?>
<?php
if ($isLB)
    $displayPMTools = false;

require ("includes/PMCheck.php");
?>
<?php require_once("../library/googleMapAPI.php"); ?>
<!--- Add Content Here --->
<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.easing.pack.js"></script>
<link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" />
<?php
//	ini_set("display_erorrs", 1);
require_once("../library/caspioAPI.php");
require_once("../library/smtpMail.php");
require_once("../library/recruitmentEmailLib.php");

function blastRecruitmentEmailMainSiteBGWithCriteria($criteria, $companyID, $comments = "", $distance = "50", $copySelf = FALSE, $manualBlast = FALSE, $returnBadZip = FALSE, $alternateFromEmail = "")
{
    $copySelf = serialize($copySelf);
    $manualBlast = serialize($manualBlast);
    $returnBadZip = serialize($returnBadZip);

    exec("(/usr/bin/php -f " . realpath(dirname(__FILE__) . "/../") . "/library/blastRecruitmentEmailMainSiteBGWithCriteria.php "
            . escapeshellarg($criteria) . " " . escapeshellarg($companyID) . " " . escapeshellarg($comments) . " " . escapeshellarg($distance) . " "
            . escapeshellarg($copySelf) . " " . escapeshellarg($manualBlast) . " " . escapeshellarg($returnBadZip) . " " . escapeshellarg($alternateFromEmail) . ") > /dev/null 2>&1 &");
    
}

if (isset($_POST["send"]))
{
    if (empty($_POST["Project"]) || !is_numeric($_POST["Project"]))
        die();
    $companyID = $_GET["v"];

    // get form info
    $projectID = $_POST["Project"];
    $distance = floatval($_POST["Distance"]);

    // find work orders in project
    $zipCodeLongLatMap = array();
    $proximityZipMap = array();
    $techEmailMap = array();

    $params = array();
    $where = array();
    //14146
    if (!empty($_POST['State']))
    {
        $where[] = 'State=?';
        $params[] = $_POST['State'];
    }
    //End 14146
    $where[] = 'Project_ID=?';
    $params[] = $projectID;
    if (!empty($_POST["StartDate"]))
    {
        $date = new Zend_Date($_POST["StartDate"], 'M/d/yyyy');
        $where[] = 'StartDate >= ?';
        $params[] = $date->toString('yyyy-MM-dd');
    }
    if (!empty($_POST['EndDate']))
    {
        $date = new Zend_Date($_POST["EndDate"], 'M/d/yyyy');
        $where[] = 'StartDate <= ?';
        $params[] = $date->toString('yyyy-MM-dd');
    }

    $where[] = "IFNULL(Tech_ID,'') = '' AND Invoiced = 0 AND Deactivated = 0 AND ShowTechs = 1";
    $params[] = "";
    $where[] = 'Company_ID=?';
    $params[] = $companyID;


    $db = Zend_Registry::get('DB');
    foreach ($where as $k => $w)
    {
        $where[$k] = $db->quoteInto($w, $params[$k]);
    }
    $criteria = implode(' AND ', $where);
    $sql = "SELECT WIN_NUM FROM work_orders WHERE " . $criteria;
    $woExist = $db->fetchOne($sql);
    //$woList = caspioSelectAdv("Work_Orders", "TOP 1 TB_UNID", $criteria, "", false, "`", "|", false);
//		print_r($woList); die();
//		print_r($woList);
//		die();
//		$woList = caspioSelectAdv("Work_Orders", RECRUITMENT_EMAIL_FIELDS, "TB_UNID IN ('7103', '26282')", "", false, "`", "|", false);
//    print_r($sql);die;
    if (!$woExist)
    {
        // No work orders found
        echo "<div align=\"center\"><br/>No work orders found.<br/></div>";
        //14146
        if ($isLB)
        {
            echo "<script>
			$(\"body\").css(\"width\", \"800px\");
			$(\"body\").css(\"margin\", \"0\");
			$(\"body\").css(\"overflow\", \"hidden\");
			$(\"#headerSlider\").css(\"display\", \"none\");
			$(\"#maintab\").css(\"display\", \"none\");
			$(\"#subSectionLabel\").css(\"display\", \"none\");
			$(\"#PMCompany\").css(\"display\", \"none\");
			$(\"table.form_table\").css(\"margin\", \"0\");
			$(\"table.form_table\").css(\"padding\", \"0\");
			$(\"table.form_table\").css(\"width\", \"400px\");
			$(\"div.footer_menu\").css(\"display\",\"none\");
			$(\"#content\").css(\"margin\", \"0\");
			$(\"#tabcontent\").css(\"display\", \"none\");
			$(\"#searchForm\").css(\"width\", \"800px\");
			$(\"#mainContainer\").css(\"width\", \"800px\");
			$(\"#mainContainer\").css(\"margin\", \"0\");
                    </script>
			";
        }
        //end 14146
    } else
    {
        $comments = $_POST["Comments"];
        $copySelf = isset($_POST["copySelf"]);
        $badZip = array();
        $emailSent = 0;

//			smtpMail("Recruitment Email Form", "no-replies@fieldsolutions.us", "tngo@fieldsolutions.com", "Recruitment Email Form Ran", "$companyID, $projectID", "", "Recruitment Email");
//			$sql = "SELECT from_email FROM projects WHERE Project_ID=?";
//			$fromEmail = $db->fetchOne($sql, array($projectID));
        $authData = array('login' => $_SESSION['UserName'], 'password' => $_SESSION['Password']);
        $user = new Core_Api_User();
        $user->checkAuthentication($authData);
        $fromEmail = $user->getEmailPrimary();

        $info = blastRecruitmentEmailMainSiteBGWithCriteria($criteria, $companyID, $comments, $distance, $copySelf, TRUE, TRUE, $fromEmail);

//			$msg = "# WO - " . sizeof($woList) . "\nCompany - $companyID\nProjectID - $projectID\nUsername - " . $_SESSION["Username"] . "\nWIN INFO - " . print_r($woList, true);
        $msg = "Criteria - $criteria";

        smtpMail("FS Recruitment Email", "no-replies@fieldsolutions.us", "tngo@fieldsolutions.com,gbailey@fieldsolutions.com", "Recruitment Emails Started", $msg, nl2br($msg), "Recruitment Email");

        echo "<div align=\"center\"><br/>";
        //echo "Sending in progress. An email will be sent to $fromEmail when sending is complete.";
        //14146
        if ($isLB)
        {
            echo "<script>
                    $(document).ready(function() {
			$(\"body\").css(\"width\", \"800px\");
			$(\"body\").css(\"margin\", \"0\");
			$(\"body\").css(\"overflow\", \"hidden\");
			$(\"#headerSlider\").css(\"display\", \"none\");
			$(\"#maintab\").css(\"display\", \"none\");
			$(\"#subSectionLabel\").css(\"display\", \"none\");
			$(\"#PMCompany\").css(\"display\", \"none\");
			$(\"table.form_table\").css(\"margin\", \"0\");
			$(\"table.form_table\").css(\"padding\", \"0\");
			$(\"table.form_table\").css(\"width\", \"400px\");
			$(\"div.footer_menu\").css(\"display\",\"none\");
			$(\"#content\").css(\"margin\", \"0\");
			$(\"#tabcontent\").css(\"display\", \"none\");
			$(\"#searchForm\").css(\"width\", \"800px\");
			$(\"#mainContainer\").css(\"width\", \"800px\");
			$(\"#mainContainer\").css(\"margin\", \"0\");
			try {
				parent.recruitmentEmailDone();
			} catch (e) {
				$('<div></div>').fancybox(
	                                {
	                                        'autoDimensions' : false,
	                                        'width' : 300,
	                                        'height' : 100,
	                                        'transitionIn': 'none',
	                                        'transitionOut' : 'none',
	                                        'content' : \"<div style='text-align:center'>Success!</div><br/>Your Project Recruitment Emails have been sent! Please note that multiple sites in the same vicinity may be combined in messaging to individual technicians.\"
	                                }
	                        ).trigger('click');
			}
			});
			</script>
			";
        }
        else
        {
            echo "<script>
                    $(document).ready(function() {
                        $('<div></div>').fancybox(
	                {
                            'autoDimensions' : false,
	                    'width' : 300,
	                    'height' : 100,
	                    'transitionIn': 'none',
	                    'transitionOut' : 'none',
	                    'content' : \"<div style='text-align:center'>Success!</div><br/>Your Project Recruitment Emails have been sent! Please note that multiple sites in the same vicinity may be combined in messaging to individual technicians.\"
                        }).trigger('click');
                    });
                </script>";
        }
        //end 14146
    }
}

if (isset($_GET["v"]))
{
    $companyID = caspioEscape($_GET["v"]);
    $statesOptions = Core_State::getByCode('US');
    $statesHtml = "<option value=\"\">All States</option>";
    foreach ($statesOptions as $code => $name)
    {
        $statesHtml .= "<option value=\"$code\">$name</option>";
    }
    $db = Zend_Registry::get('DB');
    $sql = "SELECT Project_Name, Project_ID FROM projects WHERE Active = 1 AND Project_Company_ID = ? ORDER BY Project_Name ASC";
    $projectList = $db->fetchAll($sql, array($companyID), Zend_Db::FETCH_NUM);
//		$projectList = caspioSelectAdv("TR_Client_Projects", "Project_Name, Project_ID", "active = '1' AND Project_Company_ID = '$companyID'", "Project_Name ASC", false, "`", "|");
    $projectHtml = "<option value=\"None\" selected=\"selected\">Select Project</option>";
    foreach ($projectList as $project)
    {
//			$project = explode("|", $project);
        $name = $project[0];
        $id = $project[1];
        $projectHtml .= "<option value=\"$id\">$name</option>";
//			$projectHtml .= "<option value=\"$id\" " . ($_POST["Project"] == $id ? "selected=\"selected\"" : "") . ">$name</option>";
    }
}
?>
<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#StartDate').calendar({dateFormat: 'MDY/'});
        $('#EndDate').calendar({dateFormat: 'MDY/'});


    });
    $('document').ready(function() {
        var ref = document.referrer.split("?")[0];
        //14146
        var isLB="<?=$isLB?>";
        if(isLB)
        {
            if (ref.match("wosDetails.php"))
            {
                $("body").css("width", "800px");
                $("body").css("margin", "0");
                $("body").css("overflow", "hidden");
                $("#headerSlider").css("display", "none");
                $("#maintab").css("display", "none");
                $("#subSectionLabel").css("display", "none");
                $("#PMCompany").css("display", "none");
                $("table.form_table").css("margin", "0");
                $("table.form_table").css("padding", "0");
                $("table.form_table").css("width", "400px");
                $("div.footer_menu").css("display", "none");
                $("#content").css("margin", "0");
                $("#tabcontent").css("display", "none");
                $("#searchForm").css("width", "800px");
                $("#mainContainer").css("width", "800px");
                $("#mainContainer").css("margin", "0");

            }
        }
        //end 14146
    });
</script>

<script type="text/javascript">
    function saveUserInfo()
    {
        if (document.forms.recruit.Project.selectedIndex == 0) {
            alert("Please select a project");
            return false;
        }

        var expireDate = new Date;
        expireDate.setMonth(expireDate.getMonth() + 6);

        var eCopySelf = (document.forms.recruit.copySelf.checked ? "1" : "0");

        document.cookie = "blast_CopySelf=" + eCopySelf + ";expires=" + expireDate.toGMTString() + ";path=/";
    }

    function get_cookie(theCookie)
    {
        var search = theCookie + "="
        var returnvalue = "";
        if (document.cookie.length > 0)
        {
            offset = document.cookie.indexOf(search);
            // if cookie exists
            if (offset != -1)
            {
                offset += search.length;
                // set index of beginning of value
                end = document.cookie.indexOf(";", offset);
                // set index of end of cookie value
                if (end == -1)
                    end = document.cookie.length;
                returnvalue = unescape(document.cookie.substring(offset, end));
            }
        }
        return returnvalue;
    }
</script>

<style  TYPE="text/css">
    .label{ font-size:12px; color:#385C7E;}
</style>

<div align="center">
    <h1>Send recruitment email for all work orders in this project</h1>
    <!--14146-->
    <form id="recruit" name="recruit" action="<?= $_SERVER['PHP_SELF'] ?>?v=<?= $_GET["v"] ?>&isLB=<?=$isLB?>" method="post">
    <!--end 14146-->
        <table cellpadding="0" cellspacing="0" border="0" style="border:#385C7E solid medium;">
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="5">
                        <tr>
                            <td class="label">Project</td>
                            <td>
                                <select id="Project" name="Project">
                                    <?= $projectHtml ?>
                                </select>
                                &nbsp;<input name="copySelf" type="checkbox" checked="<?php echo (isset($_POST["copySelf"]) ? "checked" : ""); ?>"/>&nbsp;Copy&nbsp;to&nbsp;Self
                            </td>
                        </tr>
                        <tr>
                            <td class="label">Start Date (>=)</td>
                            <td>
                                <input name="StartDate" type="text" id="StartDate" size="15"> <span style="color:#ff0000; font-weight:bold"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">Start Date (<=)</td>
                            <td>
                                <input name="EndDate" type="text" id="EndDate" size="15"> <span style="color:#ff0000; font-weight:bold"></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">State</td>
                            <td>
                                <select id="State" name="State">
                                    <?= $statesHtml ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="label">Distance</td>
                            <td>
                                <select id="Distance" name="Distance">
                                    <option value="50" <?php echo ($_POST["Distance"] == "50" ? "selected=\"selected\"" : ""); ?>>50</option>
                                    <option value="75" <?php echo ($_POST["Distance"] == "75" ? "selected=\"selected\"" : ""); ?>>75</option>
                                    <option value="100" <?php echo ($_POST["Distance"] == "100" ? "selected=\"selected\"" : ""); ?>>100</option>
                                    <option value="200" <?php echo ($_POST["Distance"] == "200" ? "selected=\"selected\"" : ""); ?>>200</option>
                                </select> Miles
                            </td>
                        </tr>
                        <tr>
                            <td class="label">Comments</td>
                            <td>
                                <textarea id="Comments" name="Comments" style="width: 30em; height: 10em"></textarea>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="text-align:center; background-color:#BACBDF; border-top:#385C7E solid medium; padding:5px;">
                    <input id="send" name="send" type="submit" value="Send" style="background-color:#385C7E; color:#FFFFFF; font-size:12px; font-weight:bold;" />
                </td>
            </tr>
        </table>
    </form>

    <script type="text/javascript">
        document.forms.recruit.copySelf.checked = (get_cookie("blast_CopySelf") == "1");
        document.forms.recruit.onsubmit = saveUserInfo;
    </script>

    <!--- End Content --->
    <?php if (!$isLB) require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
