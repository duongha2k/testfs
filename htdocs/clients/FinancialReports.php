<?php $page = "clients"; ?>
<?php $option = "reports"; ?>
<?php $selected = 'MetricsReport'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php require ("includes/adminCheck.php"); ?>
<?php
$displayPMTools = true;
require_once("includes/PMCheck.php");
?>
<?php
$id = $_GET["v"];
?>

<!--- Add Content Here --->
<br /><br />

<div align="center">
    <br /><br />
    <table width="400" border="1" cellspacing="5" cellpadding="0">
        <tr>
            <th nowrap="nowrap" scope="col" align="left">Financial Reports</th>
        </tr>
        <tr>
            <td nowrap="nowrap">&nbsp;</td>
        </tr>
        <tr>
            <td nowrap="nowrap"><!--<div align="center"><a href="workOrdersOOS.php">Out of Scope Work Orders</a></div> -->
                <a href="javascript:openPopup('http://<?= $_SERVER["SERVER_NAME"] ?>/reports/reportviewer/report.html?name=InvoiceDetailAndBackupReport.prpt&autoSubmit=false')">Invoice Detail Report</a></td>
        </tr>
        <tr>
            <td nowrap="nowrap">
                <a href="CreateExpenseReimbursementReport.php?v=<?php echo $id; ?>">Expense Reimbursement Report</a></td>
        </tr>
    </table>
</div>
<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
