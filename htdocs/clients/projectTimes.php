<?php $page = "clients"; ?>
<?php $option = "reports"; ?>
<?php $selected = 'projectTimesReport'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php require ("includes/adminCheck.php"); ?>
<?php
	$displayPMTools = true;
	require_once("includes/PMCheck.php");
?>
<?php
	require_once("../library/caspioAPI.php");
	$result = caspioSelectAdv(Client_Active_Projects, "TR_Client_Projects_Project_Name, TR_Client_Projects_Project_ID, TR_Client_Projects_Project_Company_ID", "", "TR_Client_Projects_Project_Name ASC", TRUE, "\"", "^");
	$projectList = str_replace("\"", "", implode("|", $result));
	$result = caspioSelectAdv(TR_Client_Projects, "Project_Name, Project_ID, Project_Company_ID", "", "Project_Name ASC", FALSE, "\"", "^");
	$allProjectList = str_replace("\"", "", implode("|", $result));
?>

<script type="text/javascript">
	var projectList = "<?=$projectList?>";
	var allProjectList = "<?=$allProjectList?>";
</script>
<!-- Add Content Here -->
<script type="text/javascript" src="../library/jquery/jquery.simplemodal-1.3.min.js"></script>
<style type="text/css">
	.optionBtns {
		width: 90%;
		text-align: center;
		margin: 0px auto;
	}
	#optionMessage {
		margin: 15px 0px 20px 0px;
		font-size: 15px;
		font-weight: bold;		
	}
	.optionBtns div {
		margin: 5px 0px;
	}
	
	.cbButton
	{
		/*Back Button Attributes*/
		color: #ffffff;
		font-size: 12px;
		font-family: Verdana;
		font-style: normal;
		font-weight: bold;
		text-align: center;
		vertical-align: middle;
		border-color: #5496C6;
		border-style: solid;
		border-width: 1px;
		background-color: #032D5F;
		/* Forced by default to add space between buttons */
		width: auto;
		height: auto;
		margin: 0 3px;
	}
	
	.cbButton:hover {
		background-color: #234D7F;
	}
</style>
<br /><br />

<div align="center">

<br /><br />

<!-- update_projects_admin.js -->
<script type="text/javascript">
amount_per_selector_field_name = "select#EditRecordAmount_Per";
device_quantity_field_name = "input#EditRecordQty_Devices";
</script>

<script type="text/javascript" src="/update_projects_admin.js">
</script>

<!-- end update_projects_admin.js -->

<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b0000g3e8g4b8j7b5d1c8f5i2","https:");}catch(v_e){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000g3e8g4b8j7b5d1c8f5i2">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>
</div>

<script type="text/javascript">
	var v = "<?=$_GET["v"]?>";
	if (isPM) {
		/* build dropdown using project list */
//		var element_dropdown = "Value2_1";
		
		/* build dropdown using project list */
		$("#" + element_dropdown).empty();
		$("#" + element_dropdown).append("<option value=\"\" selected=\"selected\">Select Project</option>");
		
		var projects = projectList.split("|"); 
		for (i = 0; i < projects.length; i++) {  
			var parts = projects[i].split("^");  
			var project_name = parts[0];  
			var project_id = parts[1];  
			var company = parts[2];
			if (v != '' && company != v) continue;
			htmlEle = "<option value=\"" + project_id + "\">" + project_name + "</option>";
			$("#" + element_dropdown).append(htmlEle);
		}
		document.getElementById(element_dropdown).selectedIndex = 0;	
	}
</script>

<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->