<?php $page = "clients"; ?>
<?php require_once("../library/timeStamps2.php"); ?>
<?php require_once("../library/timeStampsCentralized.php"); ?>
<?php require ("../headerSimple.php"); ?>
<!--?php require ("../navBar.php"); ?-->
<?php require ("includes/adminCheck.php"); ?>
<?php
	$displayPMTools = false;
	require_once ("includes/PMCheck.php");
	require_once("../library/recruitmentEmailLib.php");
	processQueuedRecruitmentEmail();	
?>
<?php require_once ("../library/googleMapAPI.php"); ?>
<!-- Add Content Here -->
<script type="text/javascript">
	var TSType = "Edit";
function approvalDate() {
	return $("input#EditRecordDateApproved");
}

function approvalCheck() {
	return $("input#EditRecordApproved");
}

	function onReady() {
		approvalCheck().change(function() {	
			//alert(approvalCheck().attr("checked"));
		
			if (approvalCheck().attr("checked") != true) {
				approvalDate().attr("value","");
//				alert("Approval Date: " + approvalDate().attr("value"));
			}
		});
		initialize();
		$("#EditRecordZipcode").change(mapShowAddress);
		onPointReturned = saveGeocode;
		document.getElementById("map_canvas").style.display = "none";
	}
	
	function saveGeocode(point) {
		$("#EditRecordLatitude").attr("value", point.lat());
		$("#EditRecordLongitude").attr("value", point.lng());
	};
</script>
<?php
	googleMapAPIJS("onReady");
	initializeTimeStampsJS();
	initializeTimeStampsJSCentralized();	
?>


<!-- blank approval date if client un-approves WO -->
<script type="text/javascript">


function approvalDate() {
	return $("input#EditRecordDateApproved");
}

function approvalCheck() {
	return $("input#EditRecordApproved");
}

/*$(document).ready(function() {
	//alert("Hello");
	
	approvalCheck().change(function() {	
		//alert(approvalCheck().attr("checked"));
	
		if (approvalCheck().attr("checked") != true) {
			approvalDate().attr("value","");
			alert("Approval Date: " + approvalDate().attr("value"));
		}
	});

});*/


</script>
<!-- end -->


<?php
	require_once("../library/caspioAPI.php");
	$result = caspioSelectAdv(Client_Active_Projects, "TR_Client_Projects_Project_Name, TR_Client_Projects_Project_ID, TR_Client_Projects_Project_Company_ID", "", "TR_Client_Projects_Project_Name ASC", TRUE, "\"", "^");
	$projectList = str_replace("\"", "", implode("|", $result));

	$result = caspioSelectAdv(TR_Client_Projects, "Project_Name, Project_ID, Project_Company_ID", "", "Project_Name ASC", FALSE, "\"", "^");
	$allProjectList = str_replace("\"", "", implode("|", $result));
	$httpReferer = (isset($_GET['referer']) ? urldecode($_GET['referer']) : $_SERVER["HTTP_REFERER"]);
	$saveReferer = $_SESSION["saveReferer"];
	if ($_SESSION["saveReferer"] != $httpReferer)
		$_SESSION["saveReferer"] = $httpReferer;
?>


<script type="text/javascript">
	var projectList = "<?=$projectList?>";
	var allProjectList = "<?=$allProjectList?>";
	var v = "<?php echo $_GET["v"] ?>";
	var httpReferer = "<?=$httpReferer?>";
	var saveReferer = "<?=$saveReferer?>";
	var pageName = "<?=basename($_SERVER['SCRIPT_FILENAME'], ".php")?>";
</script>


<!-- setup code for update_projects.php -->
<!--<script type="text/javascript">
	project_selector_field_name = "select#EditRecordProject_ID";
	default_amount_field_name = "input#EditRecordPayMax";
	amount_per_field_name = "select#EditRecordAmount_Per";
	qty_devices_field_name = "input#EditRecordQty_Devices";
</script> -->

<?php
/*	$projectTable = "TR_Client_Projects";
	$projectID = "Project_ID";
	$defaultAmount = "Default_Amount";
	$amountPer = "Amount_Per";
	$qtyDevices = "Qty_Devices";*/
?>

<?php //	require("../update_projects.php"); ?>
<!-- end setup code for update_projects.php -->




<style type="text/css">
.cbBackButton
{
/*Back Button Attributes*/
color: #ffffff;
font-size: 12px;
font-family: Verdana;
font-style: normal;
font-weight: bold;
text-align: center;
vertical-align: middle;
border-color: #5496C6;
border-style: solid;
border-width: 1px;
background-color: #032D5F;
/* Forced by default to add space between buttons */
width: auto;
height: auto;
margin: 0 3px;
}
</style>

<!-- View Work Order 
- AJAX for tech lookup
- Email tech when assigned 10/17/07
- Added Complete Unchecked email 11/26/07
- Added Email tech when approved 11/27/07
- Added When WO changes email coordinator 12/10/07
-->

<!-- old: 193b0000341163aa00d64cf29399 -->

<div style="width: 500px">
<div id="map_canvas"></div>
<?php // First one is non-FLS
if (empty($_SESSION["UserType"])): ?>
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b0000d99b804e041c47e1b271","https:");}catch(v_e){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000d99b804e041c47e1b271">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>
<?php
else:?>
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b000066398d492b7e49e0a1f9","https:");}catch(v_e){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b000066398d492b7e49e0a1f9">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>
<?php
endif;
?>
</div>
<!--- End Content --->
<!--?php require ("../footer.php"); ?--><!-- ../ only if in sub-dir -->
