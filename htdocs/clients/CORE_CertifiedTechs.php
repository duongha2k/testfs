<?php 
//	ini_set("display_errors", 1);

if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE')) {
	session_cache_limiter("public");
	header("Cache-Control: must-revalidate, max-age=10");
}
set_time_limit(1600);

$companyID = $_GET["v"];
	
class CORE_TestReport {

	var $StartDate; // = "01/01/1901";
	var $EndDate; // = "12/31/2099";
}
// $this->totalWOs = 0;

function getHeader($Heading, $companyID)
{

	$cols = 3;
	echo "<br><div id=\"results\"><form id=\"formattedFields\" name=\"formattedFields\" action=\"" . $_SERVER['PHP_SELF'] . "?v=" . $_GET["v"] . "\" method=\"post\">
	<table class=\"resultsTable\"><thead>";
	
	echo "	<tr class=\"resultTop\"><td colspan=\"";
	echo $cols-0;
	echo "\" valign=\"left\"><input type=\"image\" src=\"https://bridge.caspio.net/images/box_blue.gif\" name=\"image\" align=\"left\" border=\"0\"/></td></tr><tr class=\"resultHeader\">";
	
    echo "<td scope=\"col\" valign=\"bottom\">Tech ID</th>
    <td scope=\"col\" valign=\"bottom\">Tech Name</th>
    <td scope=\"col\" valign=\"bottom\">Zip</th>";

	echo "</tr></thead><tbody>";
}

function getFooter($formattedFields)
{
	echo "</tbody></table>  <textarea name=\"formattedFields\" id=\"formattedFields\" style=\"display:none\">" . $formattedFields . "</textarea></div>";
}

function getHeaderCSV($Heading, $companyID)
{
	if ($Heading != "") {
		echo $Heading . "\n";
	} else {
		echo "Tech ID,Tech Name, Zip \n";
	}
}

function escapeCSVField($value) {
	$delimiter = ",";
	$enclosure = "\"";
	if (strpos($value, ' ') !== false ||
		strpos($value, $delimiter) !== false ||
		strpos($value, $enclosure) !== false ||
		strpos($value, "\n") !== false ||
		strpos($value, "\r") !== false ||
		strpos($value, "\t") !== false)
	{
		$str2 = $enclosure;
		$escaped = 0;
		$len = strlen($value);
		for ($i=0;$i<$len;$i++)
		{
			if (!$escaped && $value[$i] == $enclosure)
				$str2 .= $enclosure;
			$str2 .= $value[$i];
		}
		$str2 .= $enclosure;
		$value = $str2;
	}
	return $value;
}


if (isset($_POST["formattedFields"])) {
	header("Content-disposition: attachment; filename=CORE_CertifiedTechs_Report.csv");
	getHeaderCSV("", $companyID);	
	$formattedFields = $_POST["formattedFields"];
		

//	echo $_POST['formattedFields'];
	echo $formattedFields;
	
	die();
} 
	$page =  "clients"; 
	$option = "reports";
	require ("../header.php");
	require ("../navBar.php");
	require ("includes/adminCheck.php");
	require ("includes/PMCheck.php");

?>

<style type="text/css">
	#emailBlast {
		text-align: center;
	}
	.verdana2 {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		clear: both;
	}

	.verdana2bold {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		font-weight: bold;
		clear: both;
	}

	.fieldGroup {
		padding-bottom: 5px;
		border-bottom: 2px solid #2A497D;
	}

	.formBox, .formBoxFooter {
		margin: 15px auto 0px auto;
		padding: 15px;
		width: 920px;
		color: #2A497D;
		font-size: 12px;
		border: 2px solid #2A497D;
	}

	.formBox input {
		font-size: 12px;
		height: 14px;
	}

	.formBoxFooter {
		background-color: #BACBDF;
		text-align: center;
		margin: 0px auto 10px auto;
		padding: 5px 15px;
		border-top: none;
	}
	.formRow3, .formRow4, .formRow5, .formRow6 {
		width: 100%;
		clear: both;
	}
	.formRow3 div {
		width: 33%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow4 div {
		width: 25%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow5 div {
		width: 20%;
		float: left;
		margin-bottom: 5px;
	}

	.formRow6 div {
		width: 16%;
		float: left;
		margin-bottom: 5px;
	}

	.resultsTable {
		width: 50%;
		margin: 10px auto 0px auto;
	}

	.resultsTable thead {
		text-align: center;
		color: #2A497D;
		border: 1px solid #2A497D;
		background-color: #A0A8AA;
		cursor: default;
	}

	.sortAble, #nextPageBtn, #prevPageBtn {
		cursor: pointer;
	}

	.sortAbleSelected {
		cursor: pointer;
		background-color: #FF9900;
		color: #FFFFFF;
		font-weight: bold;
	}

	.resultsTable tfoot {
		text-align: center;
		color: #000000;
		border: 1px solid #2A497D;
		background-color: #A0A8AA;
	}

	.resultsTable td {
		padding: 3px 5px;
	}

	.resultsTable .evenRow {
		background-color: #E9EEF8;
	}

	.resultsTable .oddRow {
		background-color: #FBFCFD;
	}

	.resultsTable .preferred {
		background-color: #FFFF8C;
	}

	.searchControls {
		margin-top: 10px;
		text-align: center;
	}

	.noResultsDiv {
		margin-top: 10px;
		text-align: center;
	}

	#grayOutLoading {
		background-color: #999999;
		width: 100%;
		height: 100%;
		position: absolute;
		top: 0px;
		left: 0px;
		filter: alpha(opacity=50);
		opacity: 0.5;
		<?=(isset($_POST["search"]) ? "" : "display: none;")?>
	}
	#loadingMsg {
		margin: 25% auto;
		font-size: 20px;
		color: #FFFFFF;
		text-align: center;
	}

</style>

    <div id="grayOutLoading">
    	<div id="loadingMsg">
        	<img src="/images/loading.gif" /> Loading ...
        </div>
    </div>
    
<script type="text/javascript">
	$(document).ready(function() {
		$("#grayOutLoading").hide();
	});
</script>

<?php

if ($companyID == "CORE") {

	echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
   <tr>
    <td colspan=\"2\" align=\"center\"><h2>CORE Certified Techs Report</h2></td>
  </tr>
</table>";

	echo "<br><br><a href='CORE_CertifiedTechs.php?v=$companyID'>Create New Report</a>";
	
	getHeader("", $companyID);
		
	$rowNumber = 1;
	$rowColor = ($rowNumber % 2 == 0 ? "evenRow" : "oddRow");
	$formattedFields = "";

	$deniedList = Core_Tech::getClientDeniedTechsArray('CORE');
	$deniedFilter = "";
	if (!empty($deniedList))
	    $deniedFilter .= ' AND TechID NOT IN (' . implode(',',$deniedList) . ')';
	
	$records = caspioSelectAdv("TR_Master_List", "TechID, FirstName, LastName, ZipCode", "ISNULL(CORE_Cert, '') != '' AND ISNULL(Deactivated, '') = '' $deniedFilter","TechID ASC", false, "`", "|^",false);
	
	foreach ($records as $tech) {
		$fields = explode("|^", $tech);
		
		$techID = trim($fields[0], "`"); // Remove quotes	
		$techName = trim($fields[1], "`") . " " . trim($fields[2], "`"); // Remove quotes	
		$zip = trim($fields[3], "`"); // Remove quotes	

		echo "<tr class=\"" . $rowColor."\">
		<td nowrap=\"nowrap\" align=\"left\">" .  $techID . "</td>" . "
		<td nowrap=\"nowrap\" align=\"left\">" . $techName . "</td>" . "
		<td nowrap=\"nowrap\" align=\"left\">" . $zip . "</td>";

		echo "</tr>";
		
		$formattedFields .=  escapeCSVField($techID) . "," . escapeCSVField($techName) . "," . escapeCSVField($zip) . "\n";
		$rowNumber ++;
		$rowColor = ($rowNumber % 2 == 0 ? "evenRow" : "oddRow");
		
		ob_flush();
		flush();  
	
	}
	echo "<tr><td colspan=\"3\" align=\"center\"><h2>Passed Techs: ". sizeof($records) . "</h2></td></tr>";


	getFooter($formattedFields);

} else {	
	echo "<h2>Access Denied!</h2>";
}


?>