<?php 
if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE')) {
	session_cache_limiter("public");
	header("Cache-Control: must-revalidate, max-age=10");
}

$companyID = $_GET["v"];

require_once("classActivityReport.php");

//ini_set("display_errors", 1);
set_time_limit(1600);

$projectReport = new flsActivityReport($companyID);
$projectReport->getSourcedBy($companyID);
$projectReport->getProjects($companyID);
$projectReport->getStates();

if (isset($_POST["formattedFields"])) {

	// download csv
    header('Content-type: application/csv');//14060  
	header("Content-disposition: attachment; filename=Geographic_Report.csv");

	getHeaderCSV("State", $companyID);	
	$formattedFields = $_POST["formattedFields"];
	echo $formattedFields;

	die();
} 

	$page = "clients"; 
	$option = "reports";
	require ("../header.php");
	require ("../navBar.php");
	require ("includes/adminCheck.php");
	require ("includes/PMCheck.php");

?>

<style type="text/css">
	#emailBlast {
		text-align: center;
	}
	.verdana2 {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		clear: both;
	}

	.verdana2bold {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		font-weight: bold;
		clear: both;
	}

	.fieldGroup {
		padding-bottom: 5px;
		border-bottom: 2px solid #2A497D;
	}

	.formBox, .formBoxFooter {
		margin: 15px auto 0px auto;
		padding: 15px;
		width: 920px;
		color: #2A497D;
		font-size: 12px;
		border: 2px solid #2A497D;
	}

	.formBox input {
		font-size: 12px;
		height: 14px;
	}

	.formBoxFooter {
		background-color: #BACBDF;
		text-align: center;
		margin: 0px auto 10px auto;
		padding: 5px 15px;
		border-top: none;
	}
	.formRow3, .formRow4, .formRow5, .formRow6 {
		width: 100%;
		clear: both;
	}
	.formRow3 div {
		width: 33%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow4 div {
		width: 25%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow5 div {
		width: 20%;
		float: left;
		margin-bottom: 5px;
	}

	.formRow6 div {
		width: 16%;
		float: left;
		margin-bottom: 5px;
	}

	.resultsTable {
		width: 80%;
		margin: 10px auto 0px auto;
	}

	.resultsTable thead {
		text-align: center;
		color: #2A497D;
		border: 1px solid #2A497D;
		background-color: #A0A8AA;
		cursor: default;
	}

	.sortAble, #nextPageBtn, #prevPageBtn {
		cursor: pointer;
	}

	.sortAbleSelected {
		cursor: pointer;
		background-color: #FF9900;
		color: #FFFFFF;
		font-weight: bold;
	}

	.resultsTable tfoot {
		text-align: center;
		color: #000000;
		border: 1px solid #2A497D;
		background-color: #A0A8AA;
	}

	.resultsTable td {
		padding: 3px 5px;
	}

	.resultsTable .evenRow {
		background-color: #E9EEF8;
	}

	.resultsTable .oddRow {
		background-color: #FBFCFD;
	}

	.resultsTable .preferred {
		background-color: #FFFF8C;
	}

	.searchControls {
		margin-top: 10px;
		text-align: center;
	}

	.noResultsDiv {
		margin-top: 10px;
		text-align: center;
	}

	#grayOutLoading {
		background-color: #999999;
		width: 100%;
		height: 100%;
		position: absolute;
		top: 0px;
		left: 0px;
		filter: alpha(opacity=50);
		opacity: 0.5;
		<?=(isset($_POST["search"]) ? "" : "display: none;")?>
	}
	#loadingMsg {
		margin: 25% auto;
		font-size: 20px;
		color: #FFFFFF;
		text-align: center;
	}

</style>

    <div id="grayOutLoading">
    	<div id="loadingMsg">
        	<img src="/images/loading.gif" /> Loading ...
        </div>
    </div>
    
<script type="text/javascript">
	$(document).ready(function() {
		$("#grayOutLoading").hide();
	});
</script>

<?php
if (!isset($_POST["search"])) {

	$DateRangeHtml = "<option value=\"showAll\" selected=\"selected\">Show All</option>";
	$DateRangeHtml .= "<option value=\"showToday\">Today</option>";
	$DateRangeHtml .= "<option value=\"showWeek\">This Week</option>";
	$DateRangeHtml .= "<option value=\"showLastWeek\">Last Week</option>";
	$DateRangeHtml .= "<option value=\"showMonth\">This Month</option>";
	$DateRangeHtml .= "<option value=\"lastMonth\">Last Month</option>";
	$DateRangeHtml .= "<option value=\"5\">Q1 " . date("Y") . "</option>";
	$DateRangeHtml .= "<option value=\"6\">Q2 " . date("Y") . "</option>";
	$DateRangeHtml .= "<option value=\"7\">Q3 " . date("Y") . "</option>";
	$DateRangeHtml .= "<option value=\"8\">Q4 " . date("Y") . "</option>";
	$DateRangeHtml .= "<option value=\"9\">H1 " . date("Y") . "</option>";
	$DateRangeHtml .= "<option value=\"10\">H2 " . date("Y") . "</option>";
	$DateRangeHtml .= "<option value=\"11\">YTD " . date("Y") . "</option>";
	
/*	$projectCriteria = new flsActivityReport();
	//$ProjectList = array();
	$ProjectList = $projectCriteria->getProjects();

	$projectHtml = "<option value=\"0\" selected=\"selected\">Select Project</option>";
	foreach ($ProjectList as $projectKey => $project) {
		$name = $project;
		$id = $projectKey;
		$projectHtml .= "<option value=\"$id\">$name</option>";
		//echo "Key: " . $id . " : Value: " . $name . "<br>";
	}
*/

?>
<br /><br />

<div align="center">
<form id="ActivityReport" name="ActivityReport" onsubmit="showLoadingMsg()" action="<?= $_SERVER['PHP_SELF']?>?v=<?= $_GET["v"] ?>" method="post">
<table width="400" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td colspan="5" align="center"><h2>Geographic Report</h2></td>
  </tr>
   <tr>
     <td colspan="5" align="left">&nbsp;</td>
   </tr>
   <tr>
    <td colspan="5" align="left"><label><b>Select Date Range:</b> </label><select id="DateRange" name="DateRange"><?=$DateRangeHtml?></select></td>
  </tr>
   <tr>
    <td colspan="5" align="center">&nbsp;</td>
  </tr>
 <tr>
    <td nowrap="nowrap">
   	<label><b>Start Date >=</b> </label><input id="StartDate" name="StartDate" type="text" size="13" />    </td>
    <td nowrap="nowrap">&nbsp;</td>
    <td nowrap="nowrap">
    <label><b>End Date <=</b> </label><input id="EndDate" name="EndDate" type="text" size="13" />    </td>
    <td nowrap="nowrap"><input id="v" name="v" type="hidden" value="<?=$_GET["v"]?>" /> &nbsp;</td>
    <td nowrap="nowrap">&nbsp;</td>
    <td nowrap="nowrap">
    <input id="search" name="search" type="submit" value="Run Report" style="background-color:#385C7E; color:#FFFFFF; font-size:12px; font-weight:bold;" />	</td>
   <tr>
    <td colspan="5">
    <!--select name="ProjectList" multiple><? echo $projectHtml ?> </select-->  &nbsp;  </td>
    <td>&nbsp;</td>
  </tr>
</tr>
 <tr><td colspan="5"><br /><i>Note: Excludes work orders that have no start and completion dates.</i></td></tr>
</table>
</form>

</div>

<link rel="stylesheet" href="../../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript" src="../../library/js/dateDropDown.js"></script>
<script type="text/javascript">
	var currYr = " <?=date("Y")?>";
	var Today = <?php echo "\"".$Today."\""; ?>;
	var BOW = <?php echo "\"".$BOW."\""; ?>;
	var EOW = <?php echo "\"".$EOW."\""; ?>;
	var BOLW = <?php echo "\"".$BOLW."\""; ?>;
	var EOLW = <?php echo "\"".$EOLW."\""; ?>;
	var BOM = <?php echo "\"".$BOM."\""; ?>;
	var EOM = <?php echo "\"".$EOM."\""; ?>;
	var PrevBOM = <?php echo "\"".$PrevBOM."\""; ?>;
	var PrevEOM = <?php echo "\"".$PrevEOM."\""; ?>;
	var Q1Start = <?php echo "\"".$Q1Start."\""; ?>;
	var Q1End = <?php echo "\"".$Q1End."\""; ?>;
	var Q2Start = <?php echo "\"".$Q2Start."\""; ?>;
	var Q2End = <?php echo "\"".$Q2End."\""; ?>;
	var Q3Start = <?php echo "\"".$Q3Start."\""; ?>;
	var Q3End = <?php echo "\"".$Q3End."\""; ?>;
	var Q4Start = <?php echo "\"".$Q4Start."\""; ?>;
	var Q4End = <?php echo "\"".$Q4End."\""; ?>;
	var H1Start = <?php echo "\"".$H1Start."\""; ?>;
	var H1End = <?php echo "\"".$H1End."\""; ?>;
	var H2Start = <?php echo "\"".$H2Start."\""; ?>;
	var H2End = <?php echo "\"".$H2End."\""; ?>;
	var YTDStart = <?php echo "\"".$YTDStart."\""; ?>;
	var YTDEnd = <?php echo "\"".$YTDEnd."\""; ?>;

	function showLoadingMsg() {
		$("#grayOutLoading").show();
	}
	
	$(document).ready(function(){
		$('#StartDate').calendar({dateFormat: 'MDY-'});
		$('#EndDate').calendar({dateFormat: 'MDY-'});
		$("#DateRange").change(filterDate);
	});
</script>


<?php

} else {

	$Start =  $_POST['StartDate'] != '' ? $_POST['StartDate'] : "01/01/1907";
	$End =  $_POST['EndDate'] != '' ? $_POST['EndDate'] : "12/31/2099";

	echo "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">
   <tr>
    <td colspan=\"5\" align=\"center\"><h2>Metrics by State</h2></td>
  </tr>
</table>";

	echo "<br><br><a href='report_ActivityByState.php?v=$companyID'>Create New Report</a> | <a href='Metrics_Reports.php?v=$companyID'>FS-Metrics Reports</a>";

	getHeader("State", $companyID);

	$rowNumber = 1;
	$rowColor = ($rowNumber % 2 == 0 ? "evenRow" : "oddRow");

	$projectCriteria = new flsActivityReport($companyID);
	$projectCriteria->setDateRange( $Start,  $End);
	$projectCriteria->getSourcedBy($companyID);
	$projectCriteria->getProjects($companyID);
	$statesList = $projectCriteria->getStates();
	$formattedFields = "";

	foreach ($statesList as $stateKey => $state) {
	
		$rowNumber ++;
		$rowColor = ($rowNumber % 2 == 0 ? "evenRow" : "oddRow");

		
		$id = $stateKey;
		$thisState = array($id => $name);
		$projectCriteria->setState($thisState);
		$projectCriteria->getWorkOrders($companyID);

		if($state == "------------"){
			$name = "BLANK";
		} else {
			$name = $state;
		}


		if($projectCriteria->totalWOs >0)
		{
	
			echo "<tr class=\"" . $rowColor."\">
			<td nowrap=\"nowrap\" class=\"leftRow\">" .  $name . "</td>" . "
			<td nowrap=\"nowrap\">" .  $Start . "</td>" . "
			<td nowrap=\"nowrap\">" .  $End . "</td>" . "
			<td nowrap=\"nowrap\" align=\"right\">" .  $projectCriteria->totalWOs . "</td>" . "
			<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->sourced . "</td>" . "
			<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->pcntSourced . "</td>" . "
			<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->unSourced . "</td>" . "
			<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->pcntUnsourced . "</td>" . "
			<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->complete . "</td>" . "
			<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->pcntComplete . "</td>" . "
			<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->Incomplete . "</td>" . "
			<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->pcntIncomplete . "</td>";
			
		    if ($companyID == "FTXS") {
			echo "<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->kickBack . "</td>" . "
			<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->pcntKickedBack . "</td>" . "
			<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->declined . "</td>" . "
			<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->pcntDeclined . "</td>";
			}
			
			echo "<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->deactivated . "</td>" . "
			<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->pcntDeactivated . "</td>" . "
			<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->late . "</td>" . "
			<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->noshow . "</td>";
			
			if ($companyID == "FTXS") {
			echo "<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->shortNotice . "</td>" . "
			<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->notSourced14 . "</td>" . "
			<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->newTechsAdded . "</td>";
			}
			
			echo "<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->totalPaid . "</td>" . "
			<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->totalPay . "</td>" . "
			<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->avgPay . "</td>" . "
			<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->totalHours . "</td>" . "
			<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->avgHours . "</td>";
			
			if ($companyID == "FTXS") {
			echo "<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->totalOOS . "</td>" . "
			<td nowrap=\"nowrap\" align=\"right\">" . $projectCriteria->pcntOOS . "</td>";
			}
			
			echo "</tr>";

			if ($companyID == "FTXS") {

				$formattedFields .=  escapeCSVField($name) . "," . escapeCSVField($Start) . ",". escapeCSVField($End) . "," . escapeCSVField($projectCriteria->totalWOs) . "," . escapeCSVField($projectCriteria->sourced) . "," . escapeCSVField($projectCriteria->pcntSourced) . "," . escapeCSVField($projectCriteria->unSourced) . ", " . escapeCSVField($projectCriteria->pcntUnsourced) . "," . escapeCSVField($projectCriteria->complete) . "," . escapeCSVField($projectCriteria->pcntComplete) . "," . escapeCSVField($projectCriteria->Incomplete) . "," . escapeCSVField($projectCriteria->pcntIncomplete) . "," . escapeCSVField($projectCriteria->kickBack) . "," . escapeCSVField($projectCriteria->pcntKickedBack) . "," . escapeCSVField($projectCriteria->declined) . "," . escapeCSVField($projectCriteria->pcntDeclined) . "," . escapeCSVField($projectCriteria->deactivated) . "," . escapeCSVField($projectCriteria->pcntDeactivated) . "," . escapeCSVField($projectCriteria->late) . "," . escapeCSVField($projectCriteria->noshow) . "," . escapeCSVField($projectCriteria->shortNotice) . "," . escapeCSVField($projectCriteria->notSourced14) . "," . escapeCSVField($projectCriteria->newTechsAdded) . "," . escapeCSVField($projectCriteria->totalPaid) . "," . escapeCSVField($projectCriteria->totalPay) . "," . escapeCSVField($projectCriteria->avgPay) . "," . escapeCSVField($projectCriteria->totalHours) . "," . escapeCSVField($projectCriteria->avgHours) . "," . escapeCSVField($projectCriteria->totalOOS) . "," . escapeCSVField($projectCriteria->pcntOOS) . "\n";

			} else {
				
				$formattedFields .=  escapeCSVField($name) . "," . escapeCSVField($Start) . ",". escapeCSVField($End) . "," . escapeCSVField($projectCriteria->totalWOs) . "," . escapeCSVField($projectCriteria->sourced) . "," . escapeCSVField($projectCriteria->pcntSourced) . "," . escapeCSVField($projectCriteria->unSourced) . ", " . escapeCSVField($projectCriteria->pcntUnsourced) . "," . escapeCSVField($projectCriteria->complete) . "," . escapeCSVField($projectCriteria->pcntComplete) . "," . escapeCSVField($projectCriteria->Incomplete) . "," . escapeCSVField($projectCriteria->pcntIncomplete) . "," . escapeCSVField($projectCriteria->deactivated) . "," . escapeCSVField($projectCriteria->pcntDeactivated) . "," . escapeCSVField($projectCriteria->late) . "," . escapeCSVField($projectCriteria->noshow) . "," . escapeCSVField($projectCriteria->totalPaid) . "," . escapeCSVField($projectCriteria->totalPay) . "," . escapeCSVField($projectCriteria->avgPay) . "," . escapeCSVField($projectCriteria->totalHours) . "," . escapeCSVField($projectCriteria->avgHours) . "\n";
		
			}
			
			$rowNumber ++;
			$rowColor = ($rowNumber % 2 == 0 ? "evenRow" : "oddRow");

		}	
		ob_flush();
		flush();
	}

	$projectReport->setDateRange( $Start,  $End);
	$projectReport->setState($statesList);
	$projectReport->getWorkOrders($companyID);

	echo "<b><tfoot><tr>
	<td>Totals:</td>
	<td nowrap=\"nowrap\">" .  $Start . "</td>" . "
	<td nowrap=\"nowrap\">" .  $End . "</td>" . "
	<td align=\"right\">" .  $projectReport->totalWOs . "</td>" . "
	<td align=\"right\">" . $projectReport->sourced . "</td>" . "
	<td align=\"right\">" . $projectReport->pcntSourced . "</td>" . "
	<td align=\"right\">" . $projectReport->unSourced . "</td>" . "
	<td align=\"right\">" . $projectReport->pcntUnsourced . "</td>" . "
	<td align=\"right\">" . $projectReport->complete . "</td>" . "
	<td align=\"right\">" . $projectReport->pcntComplete . "</td>" . "
	<td align=\"right\">" . $projectReport->Incomplete . "</td>" . "
	<td align=\"right\">" . $projectReport->pcntIncomplete . "</td>";
	
	if ($companyID == "FTXS") {
	echo "<td align=\"right\">" . $projectReport->kickBack . "</td>" . "
	<td align=\"right\">" . $projectReport->pcntKickedBack . "</td>" . "
	<td align=\"right\">" . $projectReport->declined . "</td>" . "
	<td align=\"right\">" . $projectReport->pcntDeclined . "</td>";
	}
	
	echo "<td align=\"right\">" . $projectReport->deactivated . "</td>" . "
	<td align=\"right\">" . $projectReport->pcntDeactivated . "</td>" . "
	<td align=\"right\">" . $projectReport->late . "</td>" . "
	<td align=\"right\">" . $projectReport->noshow . "</td>";
	
	if ($companyID == "FTXS") {
	echo "<td align=\"right\">" . $projectReport->shortNotice . "</td>" . "
	<td align=\"right\">" . $projectReport->notSourced14 . "</td>" . "
	<td align=\"right\">" . $projectReport->newTechsAdded . "</td>";
	}
	
	echo "<td align=\"right\">" . $projectReport->totalPaid . "</td>" . "
	<td align=\"right\">" . $projectReport->totalPay . "</td>" . "
	<td align=\"right\">" . $projectReport->avgPay . "</td>" . "
	<td align=\"right\">" . $projectReport->totalHours . "</td>" . "
	<td align=\"right\">" . $projectReport->avgHours . "</td>";

	if ($companyID == "FTXS") {
	echo "<td align=\"right\">" . $projectReport->totalOOS . "</td>" . "
	<td align=\"right\">" . $projectReport->pcntOOS . "</td>";
	}
	
	echo "</tr></tfoot><b>";

	if ($companyID == "FTXS") {
	
		$formattedFields .= "Totals:," . escapeCSVField($Start) . ",". escapeCSVField($End) . "," . escapeCSVField($projectReport->totalWOs) . "," . escapeCSVField($projectReport->sourced) . "," . escapeCSVField($projectReport->pcntSourced) . "," . escapeCSVField($projectReport->unSourced) . ", " . escapeCSVField($projectReport->pcntUnsourced) . "," . escapeCSVField($projectReport->complete)  . "," . escapeCSVField($projectReport->pcntComplete) . "," . escapeCSVField($projectReport->Incomplete) . "," . escapeCSVField($projectReport->pcntIncomplete) . "," . escapeCSVField($projectReport->kickBack) . "," . escapeCSVField($projectReport->pcntKickedBack) . "," . escapeCSVField($projectReport->declined) . "," . escapeCSVField($projectReport->pcntDeclined) . "," . escapeCSVField($projectReport->deactivated) . "," . escapeCSVField($projectReport->pcntDeactivated) . "," . escapeCSVField($projectReport->late) . "," . escapeCSVField($projectReport->noshow) . "," . escapeCSVField($projectReport->shortNotice) . "," . escapeCSVField($projectReport->notSourced14) . "," . escapeCSVField($projectReport->newTechsAdded) . "," . escapeCSVField($projectReport->totalPaid) . "," . escapeCSVField($projectReport->totalPay) . "," . escapeCSVField($projectReport->avgPay) . "," . escapeCSVField($projectReport->totalHours) . "," . escapeCSVField($projectReport->avgHours) . "," . escapeCSVField($projectReport->totalOOS) . "," . escapeCSVField($projectReport->pcntOOS) . "\n";

	} else {
	
	$formattedFields .= "Totals:," . escapeCSVField($Start) . ",". escapeCSVField($End) . "," . escapeCSVField($projectReport->totalWOs) . "," . escapeCSVField($projectReport->sourced) . "," . escapeCSVField($projectReport->pcntSourced) . "," . escapeCSVField($projectReport->unSourced) . ", " . escapeCSVField($projectReport->pcntUnsourced) . "," . escapeCSVField($projectReport->complete)  . "," . escapeCSVField($projectReport->pcntComplete) . "," . escapeCSVField($projectReport->Incomplete) . "," . escapeCSVField($projectReport->pcntIncomplete) . "," . escapeCSVField($projectReport->deactivated) . "," . escapeCSVField($projectReport->pcntDeactivated) . "," . escapeCSVField($projectReport->late) . "," . escapeCSVField($projectReport->noshow) . "," . escapeCSVField($projectReport->totalPaid) . "," . escapeCSVField($projectReport->totalPay) . "," . escapeCSVField($projectReport->avgPay) . "," . escapeCSVField($projectReport->totalHours) . "," . escapeCSVField($projectReport->avgHours) . "\n";

	}
		 
	getFooter($formattedFields);
}

?>
<!--- End Content --->
<?php require ("../footer.php"); ?>
