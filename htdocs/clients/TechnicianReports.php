<?php $page = "clients"; ?>
<?php $option = "reports"; ?>
<?php $selected = 'MetricsReport'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php require ("includes/adminCheck.php"); ?>
<?php
$displayPMTools = true;
require_once("includes/PMCheck.php");
?>
<?php
$id = $_GET["v"];
require_once("../library/caspioAPI.php");
$db = Zend_Registry::get('DB');
$select = $db->select();
$select->from(Core_Database::TABLE_CLIENTS, "ProjectManager")
        ->where("UserName = " . $db->quoteInto("?", $_SESSION['UserName']));
$result = $db->fetchCol($select);
if ($result === false)
    $isPM = FALSE; // username not found
else
    $isPM = $result[0] == 1;
?>

<!--- Add Content Here --->
<br /><br />

<div align="center">
    <br /><br />
    <table width="400" border="1" cellspacing="5" cellpadding="0">
        <tr>
            <th nowrap="nowrap" scope="col" align="center">Tech Analysis & Reports</th>
        </tr>
        <tr>
            <td nowrap="nowrap">&nbsp;</td>
        </tr>
        <tr>
            <td align="center">
            <a href="/clients/createTechUtilReport.php?v=<?php echo $_GET["v"]?>">Tech Utilization Report</a>
            </td>
        </tr>
        <tr>
            <td align="center"><a attribid="31" href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=Technician_SLA_Compliance.prpt&ignoreDefaultDates=true&ignoreDefaultDates=true&autoSubmit=false')">Technician SLA Compliance</a>
            </td>
        </tr>        
        <tr>
            <td align="center">
                <a attribid="46" href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=TechSiteSatisfaction.prpt&ignoreDefaultDates=true&autoSubmit=false')">Tech Site Satisfaction Survey List</a>
            </td>
        </tr>
        <?php if ($isPM): ?>
            <tr>
            <td nowrap="nowrap" align="center">
                    <a attribid="33" href="/clients/techsDeniedEdit.php?v=<?php echo $_GET["v"] ?>">
                        Client Denied Techs
                    </a>


                </td>
            </tr>
             <!--14089-->
        <tr>
            <td nowrap="nowrap" align="center">
                <a attribid="34" href="/clients/techsClientPreferredEdit.php?v=<?php echo $_GET["v"] ?>">Client Preferred Techs</a></td>
        </tr>
        <?php endif; ?>  
        <tr>
         <td align="center"><a attribid="32" href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=Tech_Review_and_Confirm.prpt&ignoreDefaultDates=true&autoSubmit=false')">Tech Review and Confirm</a></td>
         </tr>
        
        <tr>
            <td nowrap="nowrap" align="center">
                <a attribid="35" href="javascript:openPopup('http://<?= $_SERVER["SERVER_NAME"] ?>/reports/reportviewer/report.html?name=Tech_Called_Report.prpt&ignoreDefaultDates=true&autoSubmit=false')">Tech Called Report</a></td>
        </tr>
        <!--14089-->
        <?php if ($isPM): ?> 
        <tr>
            <td nowrap="nowrap" align="center"><!--<div align="center"><a href="workOrdersOOS.php">Out of Scope Work Orders</a></div> -->
                <a attribid="37" href="javascript:openPopup('http://<?= $_SERVER["SERVER_NAME"] ?>/reports/reportviewer/report.html?name=Tech_WO_Address_Report.prpt&amp;ignoreDefaultDates=true&amp;autoSubmit=false')">Tech WO Address Report</a></td>
        </tr>    
        <?php endif; ?>   
        <!--end 14089-->

    </table>
</div>


<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
