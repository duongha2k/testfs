<?php require ("../headerSimple.php"); ?>

<?php
	$displayPMTools = false;
	require ("includes/PMCheck.php"); 

	require_once realpath(dirname(__FILE__) . '/../') . '/includes/_init.service.php';
	
	$result = null;
	
	if ( !empty($_GET['WO_ID']) && !empty($_GET['v']) ) {
	    $api = new Core_Api_Class();
        $res = $api->getTimestampsWithWinNum($_SESSION['UserName'].'|pmContext='.$_GET['v'], $_SESSION["Password"], $_GET['WO_ID']);
	    if ($res->success) $result = $res->data;
	}

	if (is_null($result)) :
?>
<br/><center><b>Wrong WO_ID</b></center>
<?php 
    elseif (empty($result)):
?>
<br/><center><b>There aren't records to display</b></center>
<?php 
    else:
?>
<table width="100%" class="gradBox_table_inner">
    <col width="200px" />
    <col width="120px" />
    <col width="*" />
    <col width="220px" />
    <col width="220px" />
    <col width="220px" />
    <col width="80px" />
    <col width="120px" />
    <tr class="table_head">
        <th>Date &amp; Time (CST)</th>
        <th>Username</th>
        <th>Description</th>
        <th>Client</th>
        <th>Project</th>
        <th>Customer</th>
        <th>TechId</th>
        <th>Tech</th>
    </tr>
<?php   
        $tr = 1; 
        foreach ($result as $item) :
            $dateTime = new Zend_Date($item->dateTime, 'yyyy-MM-dd HH:mm:ss');
        ?>
        <tr class="<?=($tr % 2)?'border_td odd_tr':'border_td even_tr'?>">
        	<td><br/><?= (empty($item->dateTime) ? '' : $dateTime->toString("MM/dd/Y HH:mm a")) ?><br/><br/></td>
        	<td><?= htmlspecialchars($item->username) ?></td>
        	<td><?= htmlspecialchars($item->description) ?></td>
        	<td><?= htmlspecialchars($item->clientName) ?></td>
        	<td><?= htmlspecialchars($item->projectName) ?></td>
        	<td><?= htmlspecialchars($item->customerName) ?></td>
        	<td><?= htmlspecialchars(empty($item->techId) ? '' : $item->techId) ?></td>
        	<td><?= htmlspecialchars($item->techName) ?></td>
        </tr>
        <?php
        endforeach;
?>
</table>
<?php 
    endif;
?>

</body>
</html>
