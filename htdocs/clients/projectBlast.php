<?php 
    header("Content-Type: text/html; charset=utf-8");
    $page = 'clients';
    $option = 'email';
    $selected = 'emailBlast';
    
    require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');

    if($isFLSManager){
        $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
    } else {
        $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
    }
    
?>
<?php 
$isLB = !empty($_GET['isLB']);
if (!$isLB) 
require ("../header.php"); 
else
require ("../headerSimple.php"); 
?>
<?php 
if (!$isLB)
	    require '../navBar.php';
	else
		$displayPMTools = false;
?>
<?php require ("includes/adminCheck.php"); ?>
<?php require ("includes/PMCheck.php"); ?>
<!-- Add Content Here -->

<?php
//	ini_set("display_erorrs", 1);
	require_once("../library/caspioAPI.php");
	require_once("../library/smtpMail.php");
	if (isset($_POST["send"])) {
		if (!is_numeric($_POST["Project"])) die();
		if ($_POST["Subject"] == "") die();
		$companyID = $_GET["v"];
		$startDate = $_POST["StartDate"];
		$endDate = $_POST["EndDate"];

		if (!empty($startDate)) {
			$date = new Zend_Date($startDate, 'M/d/yyyy');
			$startDate = $date->toString('yyyy-MM-dd');
		}
		if (!empty($endDate)) {
			$date = new Zend_Date($endDate, 'M/d/yyyy');
			$endDate = $date->toString('yyyy-MM-dd');
		}

		$criteriaDate = "";
		if (!empty($startDate)) {
			$criteriaDate .= " AND StartDate >= '$startDate'";
		}
		if (!empty($endDate)) {
			$criteriaDate .= " AND StartDate <= '$endDate'";
		}

		// get form info
		$projectID = $_POST["Project"];

		$db = Zend_Registry::get('DB');
		$sql = "SELECT DISTINCT Tech_ID FROM work_orders WHERE Project_ID = ? $criteriaDate AND IFNULL(Tech_ID, '') != '' AND Invoiced = 0 AND Deactivated = 0 AND TechMarkedComplete = 0 AND Company_ID = ?";
		$techIds = $db->fetchCol($sql, array($projectID, $companyID));

		$woList = array();
		if (count($techIds)) {
			$woList = caspioSelectAdv("TR_Master_List", "PrimaryEmail", 'TechID IN (' . implode(',', $techIds) . ')', "", false, " ", "|");
		}

		if (sizeof($woList) == 1 && $woList[0] == "") {
			// No work orders found
			echo "<div align=\"center\"><br/>No work orders found.<br/></div>";
			
			echo "
					<script>
			$(\"body\").css(\"width\", \"800px\");
			$(\"body\").css(\"margin\", \"0\");
			$(\"body\").css(\"overflow\", \"hidden\");
			$(\"#headerSlider\").css(\"display\", \"none\");
			$(\"#maintab\").css(\"display\", \"none\");
			$(\"#subSectionLabel\").css(\"display\", \"none\");
			$(\"#PMCompany\").css(\"display\", \"none\");
			$(\"table.form_table\").css(\"margin\", \"0\");
			$(\"table.form_table\").css(\"padding\", \"0\");
			$(\"table.form_table\").css(\"width\", \"400px\");
			$(\"div.footer_menu\").css(\"display\",\"none\");
			$(\"#content\").css(\"margin\", \"0\");
			$(\"#tabcontent\").css(\"display\", \"none\");
			$(\"#searchForm\").css(\"width\", \"800px\");
			$(\"#mainContainer\").css(\"width\", \"800px\");
			$(\"#mainContainer\").css(\"margin\", \"0\");
			</script>
			";
		}
		else {

			$subject = $_POST["Subject"];
			$comments = $_POST["Comments"];
			$copySelf = isset($_POST["copySelf"]);
			$emailSent = 0;

//			smtpMail("Project Email Blast Form", "no-replies@fieldsolutions.us", "gbailey@fieldsolutions.com", "Project Email Blast Form Ran", "Company ID: $companyID, Project ID: $projectID", "", "Recruitment Email");

			$db = Zend_Registry::get('DB');
			$sql = "SELECT from_email FROM projects WHERE Project_ID=?";
			$fromEmail = $db->fetchOne($sql, array($projectID));
			if($copySelf) {
				$woList[sizeof($woList)+1] = $fromEmail;
			}

			$info = smtpMailLogReceivedBG("Field Solutions", $fromEmail, $woList, $subject, $comments, nl2br($comments), "projectBlast.php", ",");

			echo "<div align=\"center\"><br/>";
			if ($copySelf) {
				echo "Copy sent to: $fromEmail<br/>";
			}
			echo "Emails sent: " . sizeof($woList) . "<br/>";
			echo "</div>";
			
			echo "
					<script>
			$(\"body\").css(\"width\", \"800px\");
			$(\"body\").css(\"margin\", \"0\");
			$(\"body\").css(\"overflow\", \"hidden\");
			$(\"#headerSlider\").css(\"display\", \"none\");
			$(\"#maintab\").css(\"display\", \"none\");
			$(\"#subSectionLabel\").css(\"display\", \"none\");
			$(\"#PMCompany\").css(\"display\", \"none\");
			$(\"table.form_table\").css(\"margin\", \"0\");
			$(\"table.form_table\").css(\"padding\", \"0\");
			$(\"table.form_table\").css(\"width\", \"400px\");
			$(\"div.footer_menu\").css(\"display\",\"none\");
			$(\"#content\").css(\"margin\", \"0\");
			$(\"#tabcontent\").css(\"display\", \"none\");
			$(\"#searchForm\").css(\"width\", \"800px\");
			$(\"#mainContainer\").css(\"width\", \"800px\");
			$(\"#mainContainer\").css(\"margin\", \"0\");
			</script>
			";
		}
	}

	if (isset($_GET["v"])) {
		$companyID = $_GET["v"];
		$db = Zend_Registry::get('DB');
		if ($db instanceof Zend_Db_Adapter_Abstract) {}
		$sql = "SELECT Project_ID,Project_Name from projects WHERE active=1 AND Project_Company_ID=? ORDER BY Project_Name ASC";
		$projectOptions = $db->fetchPairs($sql, array($companyID));
		$projectHtml = "<option value=\"None\" selected=\"selected\">Select Project</option>";
		foreach ($projectOptions as $id => $name) {
			$projectHtml .= "<option value=\"$id\">$name</option>";
		}
	}
?>

<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#StartDate').calendar({dateFormat: 'MDY/'});
		$('#EndDate').calendar({dateFormat: 'MDY/'});
		
	});
	$('document').ready(function(){
	var ref = document.referrer.split("?")[0];
		if (ref.match("wosDetails.php")){
			$("body").css("width", "800px");
			$("body").css("margin", "0");
			$("body").css("overflow", "hidden");
			$("#headerSlider").css("display", "none");
			$("#maintab").css("display", "none");
			$("#subSectionLabel").css("display", "none");
			$("#PMCompany").css("display", "none");
			$("table.form_table").css("margin", "0");
			$("table.form_table").css("padding", "0");
			$("table.form_table").css("width", "400px");
			$("div.footer_menu").css("display","none");
			$("#content").css("margin", "0");
			$("#tabcontent").css("display", "none");
			$("#searchForm").css("width", "800px");
			$("#mainContainer").css("width", "800px");
			$("#mainContainer").css("margin", "0");
			
		}
});
</script>

<script type="text/javascript">
function saveUserInfo()
{
	if (document.forms.recruit.Project.selectedIndex == 0) {
		alert("Please select a project");
		return false;
	}

	if (document.forms.recruit.Subject.value == "") {
		alert("Please enter a subject for this message");
		return false;
	}

	var expireDate = new Date;
	expireDate.setMonth(expireDate.getMonth()+6);

	var eCopySelf = (document.forms.recruit.copySelf.checked ? "1" : "0");

	document.cookie = "blast_CopySelf=" + eCopySelf + ";expires=" + expireDate.toGMTString() + ";path=/";
}

function get_cookie(theCookie)
{
	var search = theCookie + "="
	var returnvalue = "";
	if (document.cookie.length > 0)
	{
		offset = document.cookie.indexOf(search);
		// if cookie exists
		if (offset != -1)
		{
			offset += search.length;
			// set index of beginning of value
			end = document.cookie.indexOf(";", offset);
			// set index of end of cookie value
			if (end == -1) end = document.cookie.length;
			returnvalue=unescape(document.cookie.substring(offset, end));
		}
	}
	return returnvalue;
}
</script>

<style  TYPE="text/css">
.label{ font-size:12px; color:#385C7E;}
</style>

<div align="center" style="margin-top:20px;">
<h1>Email all assigned technicians to this project</h1>
<form id="recruit" name="recruit" action="<?=$_SERVER['PHP_SELF']?>?v=<?=$_GET["v"]?>" method="post">
	<table cellpadding="0" cellspacing="0" border="0" style="border:#385C7E solid medium;">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="5">
					<tr>
						<td class="label">Project</td>
						<td>
							<select id="Project" name="Project">
								<?=$projectHtml?>
							</select>
							&nbsp;<input name="copySelf" type="checkbox" checked="<?php echo (isset($_POST["copySelf"]) ? "checked" : "");?>"/>&nbsp;Copy&nbsp;to&nbsp;Self						</td>
					</tr>
					<tr>
						<td class="label">Start Date (>=)</td>
						<td>
							<input name="StartDate" type="text" id="StartDate" size="15"> <span style="color:#ff0000; font-weight:bold">* Required</span>
						</td>
					</tr>
					<tr>
						<td class="label">Start Date (<=)</td>
						<td>
							<input name="EndDate" type="text" id="EndDate" size="15"> <span style="color:#ff0000; font-weight:bold">* Required</span>
						</td>
					</tr>
					<tr>
						<td class="label">Subject</td>
						<td>
							<textarea id="Subject" name="Subject" style="width: 30em; height: 1em"></textarea>						</td>
					</tr>
					<tr>
						<td class="label">Comments</td>
						<td>
							<textarea id="Comments" name="Comments" style="width: 30em; height: 10em"></textarea>						</td>
					</tr>
				</table>
		  </td>
		</tr>
		<tr>
			<td style="text-align:center; background-color:#BACBDF; border-top:#385C7E solid medium; padding:5px;">
			<input id="send" name="send" type="submit" value="Send" style="background-color:#385C7E; color:#FFFFFF; font-size:12px; font-weight:bold;" />
			</td>
		</tr>
	</table>
</form>

<script type="text/javascript">
	document.forms.recruit.copySelf.checked = (get_cookie("blast_CopySelf") == "1");
	document.forms.recruit.onsubmit = saveUserInfo;
</script>

<!--- End Content --->
<?php 
if (!$isLB) require ("../footer.php");
?><!-- ../ only if in sub-dir -->
