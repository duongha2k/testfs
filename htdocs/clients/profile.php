<?php
$page = 'clients';
if($_GET['tabid']=='clientQuickTickets')
    $option = 'quickview';
elseif($_GET['tabid']=='clientMySettings')
	$option = 'clientMySettings';
elseif($_GET['tabid']=='clientUsers')
	$option = 'clientUsers';
else
    $option = 'profile';
require_once("../headerStartSession.php");
//require_once("../library/FSSession.php");
//FSSession::validateSessionParams(array("UserName" => "UserName"));

$company = ( isset($_GET['v']) ) ? $_GET['v'] : $_SESSION['Company_ID'];
$isFLSManager = (strtolower($company) == 'fls' && (strtolower($_SESSION['UserType']) === 'install desk' || strtolower($_SESSION['UserType']) === 'manager'));

if ($isFLSManager) {
    $toolButtons = array(/* 'MyTechs', */ 'FindWorkOrder', 'CreateWorkOrder');
} else {
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
}
if (empty($_GET["simple"])){
require ("../header.php");
require ("../navBar.php");
//require ("includes/adminCheck.php");
if($_SESSION['Auth_User']['isPM'] ==  1)
{
    require 'includes/PMCheck.php';
}
}
else {
	require ("../headerSimple.php");
?>      
	<body>
<?php
}
$isAdmin = $_SESSION['UserObj'][0]['Admin'];
//13777
$db = Zend_Registry::get('DB');
$select = $db->select();
$select->from(Core_Database::TABLE_CLIENTS, "ProjectManager")
        ->where("UserName = " . $db->quoteInto("?", $_SESSION['UserName']));
$result = $db->fetchCol($select);
if ($result === false) {
    $isPM = FALSE; // username not found    
} else {
    $isPM = $result[0] == 1;    
}
//end 13777 
$Core_Api_User = new Core_Api_User();
//743
$isFullRestricted_old = $Core_Api_User->AreFullRestriction($company);
$isPartialRestricted_old = $Core_Api_User->ArePartialRestriction($company);
$isFullOpened_old = $Core_Api_User->AreFullorPartialRestriction($company);
//end 743
$Core_Api_User->setAllAccessRestricted_InNDaysNotAssignedRule($company);
//743
$isFullRestricted = $Core_Api_User->AreFullRestriction($company);
$isPartialRestricted = $Core_Api_User->ArePartialRestriction($company);
$isFullOpened=$Core_Api_User->AreFullorPartialRestriction($company);


$issendmail = false;
if(($isFullRestricted != $isFullRestricted_old)
        || ($isPartialRestricted != $isPartialRestricted_old)
        || ($isFullOpened!=$isFullOpened_old))
{
    $issendmail = true;
}

if($issendmail)
{
    $restrictedtype = 3;    
    $repClientID = $Core_Api_User->getRepClientIDforCompany($company);
    $repClientExtInfo = $Core_Api_User->getClientExt($repClientID);    
    if(!empty($repClientExtInfo)){
        $CSD = $clientExtInfo[0]['FSClientServiceDirectorEmail'];
        $AM = $clientExtInfo[0]['FSClientServiceDirectorEmail'];
        $CompanyName = $clientExtInfo[0]['CompanyName'];
        $emailTo = "";
        if(!empty($AM)){
            $emailTo .= $AM;
        }
        if(!empty($CSD)){
            $emailTo .= empty($emailTo) ? $CSD : ','.$CSD;
        }
    }

    if(!empty($emailTo)){
        $apiemailrestricted = new Core_EmailForRestricted();
        $apiemailrestricted->setCompanyId($Company_ID);
        $apiemailrestricted->setCompanyName($CompanyName);
        $apiemailrestricted->settoMail($emailTo);
        $apiemailrestricted->setusername($this->_login);
        $apiemailrestricted->setrestrictedtype($restrictedtype);
        $apiemailrestricted->sendMail();        
    }
}
//end 743
?>
<script type="text/javascript" src="/library/jquery/jquery.bt.min.js"></script>
<!--<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>-->
<?php script_nocache ("/widgets/js/ajaxfileupload.js"); ?>
<!--<script type="text/javascript" src="js/profile.js"></script>-->
<?php script_nocache ("/clients/js/profile.js"); ?>
<!--<script type="text/javascript" src="/widgets/js/FSWidgetManager.js"></script>-->
<?php script_nocache ("/widgets/js/FSWidgetManager.js"); ?>
<script type="text/javascript">
var wManager = new FSWidgetManager();
</script>
<!--<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>-->
<?php script_nocache ("/widgets/js/FSPopupRoll.js"); ?>
<script>function calendarInit() {}</script>
<!-- Add Content Here -->
<?php if (empty($_GET["simple"])){
   echo '<br /><br />'; 
} ?>
<style>
    .contentBodyTab
    {
        margin-left:auto; margin-right:auto; width:100%; 
        height:100%; clear: left;
    }
    #clientProfile .form_table
    {
        float: left;
    }
    .borderStyleDiv
    {

       /* width:70%; */
    }
    .form_tableV2 td {
        padding: 2px 5px;
        vertical-align: top;
    }
    #clientProfile .form_tableV2 {
        float: left;
    }
    .popup_form_holder .form_tableV2 {
        margin: 0 auto;
        text-align: left;
    }
    .form_tableV2 {
        font-size: 12px;
        border-bottom:1px solid #F69630;
        border-left:1px solid #F69630;
        border-right:1px solid #F69630;
    }
    .form_tableV2 .tbPt {
        border-color: #666666;
        border-style: solid;
        border-width: 0px 0px 1px 0px;
    }
    ul.tabs a:hover {
      text-decoration: underline;
      background-color: #FFC24B;
    }
    ul.tabs a.current {
        background-color: #F69630;
        border-bottom: 1px solid #F69630;
        color: #ffffff;
        cursor: default;
        padding-top: 5px;
        height: 22px;
        top: 0;
    }
    ul.tabs a {
        color:#385C7E;
        padding: 5px 10px 6px;
        border-width: 0;
    }
    div.tabsbottom {
        height: 4px;
    }
    ul.tabWorkOrderSettings {
    border-bottom: 1px solid #666666;
    height: 34px;
    margin: 0 !important;
    padding: 5px 0 0;
}
ul.tabWorkOrderSettings li {
    float: left;
    height: 100%;
    list-style-type: none;
    margin: 0;
    padding: 0;
}
ul.tabWorkOrderSettings a {
    -moz-border-bottom-colors: none;
    -moz-border-image: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: #BFBFBF;
    border-color: #666666 #666666 -moz-use-text-color;
    border-radius: 4px 4px 0 0;
    border-style: solid solid none;
    border-width: 1px 1px 0;
    color: #012A5E;
    display: block;
    float: left;
    font-size: 13px;
    height: 18px;
    margin-right: 2px;
    outline: 0 none;
    padding: 5px 10px;
    position: relative;
    text-align: center;
    text-decoration: none;
    top: 5px;
    vertical-align: bottom;
}
ul.tabWorkOrderSettings a:hover {
    background-color: #F7F7F7;
    color: #333333;
}
ul.tabWorkOrderSettings a.current {
    background-color: #F69630;
    border-bottom: 1px solid #F69630;
    color: #012A5E;
    cursor: default;
    height: 22px;
    top: 0;
}
    ul.tabWorkOrderSettings a:hover {
        text-decoration: underline;
        background-color: #BFBFB9;
    }
    ul.tabWorkOrderSettings a.current {
        background-color: #F69630;
        border-bottom: 1px solid #F69630;
        color: #ffffff;
        cursor: default;
        padding-top: 5px;
        height: 22px;
        top: 0;
    }
    ul.tabWorkOrderSettings a {
        color:#385C7E;
        padding: 5px 10px 6px;
        border-width: 0;
    }
    .tableContentWDS td
    {
        padding: 0px;
    }
    .label_field{
    	color: #000;
    }
    
    .even_tr td{
    	border:none;
    }
    .odd_tr td{
    	border:none;
    }
</style>
<!--<script src="/widgets/js/FSWidgetClientDetails.js?042620111"></script>-->
<?php script_nocache ("/widgets/js/FSWidgetClientDetails.js"); ?>

<div id="clientProfile" style="<?= empty($_GET["simple"])?'width:70%;':"" ?>margin-left:auto;margin-right:auto;">

    <div style="<?= empty($_GET["simple"])?'height:38px;':"" ?>" id="tabbedMenuNew">
<?php if (empty($_GET["simple"])){ ?>
            <ul  class="tabs" style="width:808px;border: 0px;">
                <li tabid="clientMySettings" style="width:120px;"><a class="current" href="javascript:void(0)" style="width:99px; font-size: 12px;">My Settings</a></li>
                <li class="currenttab" tabid="detailsContainer" style="width:120px;"><a class="current" href="javascript:void(0)" style="width:99px; font-size: 12px;">Client Profile</a></li>
                <!-- 13777 -->
                <?php if($isAdmin || $isPM) { ?>
                    <li tabid="clientWorkOrderSettings" style="width:145px;"><a style="width:124px; font-size: 12px;" href="javascript:void(0)">Customize Work Order</a></li>
                <?php } ?>
                <!-- end 13777 -->                
                <li tabid="clientQuickTickets" style="width:120px;"><a style="width:99px; font-size: 12px;" href="javascript:void(0)">QuickTicket</a></li>
                <?php if ((isset($_SESSION['PM_Company_ID']) || strtoupper($_SESSION["tbAdminLevel"]) == 'YES') && !$isFLSManager):?>
                <li tabid="clientUsers" style="width:120px;"><a style="width:99px; font-size: 12px;" href="javascript:void(0)">Users</a></li>
                <?php endif;?>
                <li tabid="clientAccessSettings" style="width:120px;"><a href="javascript:void(0)" style="width:99px; font-size: 12px;">Access Settings</a></li>
                <li tabid="clientProjectSettings" style="width:110px;display: none;"><a href="javascript:void(0)">Project Settings</a></li>
                <li tabid="clientReportSettings" style="width:110px;display: none;"><a href="javascript:void(0)">Report Settings</a></li>
                <li tabid="clientFinancialStatements" style="width:120px;display: none;"><a href="javascript:void(0)">FinancialStatements</a></li>
            </ul>
        <?php } ?>
        <?php if(!empty($_GET["backNotices"])){ ?>
        <input style="cursor: pointer;" value="Back" class="link_button_popup" type="button" onclick="<?= base64_decode($_GET['backclick']) ?>" />
        <?php } ?>
            <div class="tabsbottom borderStyleDiv" style="border: 0px;<?= !empty($_GET["notice"])?"width:99%;":"" ?>"></div>

    </div>

    <div id="detailsContainer"  class="contentBodyTab borderStyleDiv"></div>    
    <div id="clientUsers"  class="contentBodyTab borderStyleDiv" style="display:none;">
    </div>  
    <div id="clientMySettings"  class="contentBodyTab borderStyleDiv" style="display:none;">
    </div>  
    <div id="clientQuickTickets"  class="contentBodyTab borderStyleDiv" style="display:none;">

    </div>  
    <div id="clientAccessSettings"  class="contentBodyTab borderStyleDiv" style="display:none;">

    </div>    
    <div id="clientWorkOrderSettings"  class="contentBodyTab borderStyleDiv" style="display:none;">

    </div>  
    <div id="clientProjectSettings" class="contentBodyTab" style="display:none;">
        <table class="form_table" style="margin-top: 0px;height:200px;" width="65%">
            <tr>
                <td align="center">
                    Project Settings
                </td>
            </tr>
        </table>
    </div>  
    <div id="clientReportSettings" class="contentBodyTab" style="display:none;">
        <table class="form_table" style="margin-top: 0px;height:200px;" width="65%">
            <tr>
                <td align="center">
                    Report Settings
                </td>
            </tr>
        </table>
    </div>  
    <div id="clientFinancialStatements" class="contentBodyTab" style="display:none;">
        <table class="form_table" style="margin-top: 0px;height:200px;" width="65%">
            <tr>
                <td align="center">
                    Financial Statements
                </td>
            </tr>
        </table>
    </div>  
    <div style="clear: both;">

    </div>
</div>

<script>
var _profile;
$(document).ready(function() {
        _profile = profile.CreateObject({
        id:'_profile',
        win:"<?= $_GET['id'] ?>",
        tab:"<?= $_GET['tabid'] ?>",
        companyName : "<?= $company?>",
        notice : "<?= $_GET['notice'] ?>"
    });
});
</script>
<!--- End Content --->
<?php if (empty($_GET["simple"])){ require ("../footer.php"); } ?>
		
