<?php
// Created on: 11-26-07 
// Author: CM
// Description: If the client unchecks the Tech Complete checkbox, send an email back to the tech automatically notifiying them of this and include the Missing Comments field within the email.


try {

require_once("../library/caspioAPI.php");
require_once("../library/smtpMail.php");

$TB_UNID = $_REQUEST['TB_UNID'];
$techID = $_REQUEST['techID'];
$ProjectNo = $_REQUEST['ProjectNo'];
$missingInfo = $_REQUEST['missingInfo'];


// If Tech id and not reviewed
if ($techID != ""){
$result = "Success"; //Value required by our library
$calledFrom = (empty($_SERVER['HTTP_REFERER']) ? "Unknown" : $_SERVER['HTTP_REFERER']);// library needs this

// Get Work Order
$workOrder = caspioSelectAdv("Work_Orders", "SiteName, Address, City, State, Zipcode, TB_UNID, WO_ID", "TB_UNID = $TB_UNID", "TB_UNID", false, "'", "|");

function getdelimitedFields($row) {
	/* Takes a single row returned by a select and returns an array of Fields */
		$row = str_replace("'", "", $row); // gets rid of quotes
		return explode("|", $row);
	}

// Populate array with WO info
foreach ($workOrder as $order) {
	$workOrderFields = getdelimitedFields($order);
	
	$SiteName = $workOrderFields[0];
	$Address1 = $workOrderFields[1];
	$City = $workOrderFields[2];
	$State = $workOrderFields[3];
	$Zip = $workOrderFields[4];
	$TB_UNID = $workOrderFields[5];
	$WorkOrderNo = $workOrderFields[6];

}

// Get Project info
$project = caspioSelect("TR_Client_Projects", "Project_ID,Project_Name,Project_Manager,From_Email,Project_Manager_Phone", "Project_ID = '$ProjectNo'", false);

// Populate array with project info
foreach ($project as $order) {
	
	$projectFields = getFields($order);
	
	$projectID = $projectFields[0];		// Project No
	$projectName = $projectFields[1];
	$vFromName = str_replace(".", "", $projectFields[2]);
	$vFromEmail = $projectFields[3];
	$vSubject = "Payment Delayed - Work Order# $WorkOrderNo not updated on FS website.";
	$projectCCphone = $projectFields[4];
	
	// if project from email is blank
	if ($vFromEmail == ""){
		$vFromEmail = "projects@fieldsolutions.com";
	}
}
	
// Get Tech Contact info
$records = caspioSelect("TR_Master_List", "techID, FirstName, LastName, PrimaryEmail", "TechID = '$techID'", false);

// Counts # of records returned
$count = sizeof($records);

// Populate array with Tech info
foreach ($records as $order) {
	
	$fields = getFields($order);
	
	// Get data from array and set values for email
	$techID = $fields[0];
	$FirstName = $fields[1];	
	$LastName = $fields[2];
	$sendToEmail = $fields[3];
	

	$eList = $sendToEmail;
	//$eList = "collin.mcgarry@fieldsolutions.com";
	//$eList = "collin.mcgarry@fieldsolutions.com,gerald.bailey@fieldsolutions.com";
	@$caller = "wosEmailTechWhenCompleteUnchecked";
		
		
$vMessage = "Dear FS Technician,/r/r Work Order No: $WorkOrderNo needs to be updated on Field Solutions website.  Payment cannot be approved until these steps are taken./r/r Comments: $missingInfo/r/r Follow this link to update the work order: https://www.fieldsolutions.com/techs/wosDetailsNonTab.php?id=$TB_UNID/r/r Coordinator: $vFromName/r Email: $vFromEmail/r Phone: $projectCCphone/r/r Thank you./r/r Regards,/r/r Field Solutions/r Web: www.fieldsolutions.com";

$html_message = "<p>Dear FS Technician,</p><p>Work Order No: $WorkOrderNo needs to be updated on Field Solutions website.  Payment cannot be approved until these steps are taken.</p><p>Missing Info / Comments: $missingInfo</p><p><a href='https://www.fieldsolutions.com/techs/wosDetailsNonTab.php?id=$TB_UNID'>Click here to update the workorder</a></p><p>Coordinator: $vFromName<br>Email: $vFromEmail<br>Phone: $projectCCphone</p><p>Thank you.</p><p>Regards,</p><p>Field Solutions<br>Web: <a href='http://www.fieldsolutions.com'>www.fieldsolutions.com</a></p>";
}
	
// Emails Tech
smtpMail($vFromName, $vFromEmail, $eList, $vSubject, $vMessage, $html_message, $caller);


}// Ends TechID check	


// BEGIN CATCH	
}catch (SoapFault $fault) {

smtpMail("WOS When Complete Unchecked", "nobody@fieldsolutions.com", "collin.mcgarry@fieldsolutions.com", "WOS When Complete Unchecked Script Error", "$fault", "$fault", "wosEmailTechWhenCompleteUnchecked.php");
	
//smtpMail("Tech Assigned Workorder", "nobody@fieldsolutions.com", "collin.mcgarry@fieldsolutions.com,gerald.bailey@fieldsolutions.com", "Tech Assigned Work Order Script Error", "$fault", "$fault", "getTechAndEmailAssignedWO.php");
}// END CATCH


?>
 
<SCRIPT LANGUAGE="JavaScript"> 
	setTimeout('close()',100); 
</SCRIPT>

