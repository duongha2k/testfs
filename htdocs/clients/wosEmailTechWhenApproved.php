<?php
// Created on: 11-27-07 
// Author: CM
// Description: If the client approves the workorder send email to tech with a notification

try {
require_once("../library/caspioAPI.php");
require_once("../library/smtpMail.php");


$TB_UNID = $_REQUEST['TB_UNID'];
$techID = $_REQUEST['techID'];
$ProjectNo = $_REQUEST['ProjectNo'];
//ini_set("display_errors",1);
//require_once("../library/pricing.php");
//queuePricingCalculation($TB_UNID);
// FOR TESTING PURPOSES
//$TB_UNID = "5525";
//$techID ="18510";
//$ProjectNo = "8";


// If Tech id and not reviewed
if ($techID != ""){
$result = "Success"; //Value required by our library
$calledFrom = (empty($_SERVER['HTTP_REFERER']) ? "Unknown" : $_SERVER['HTTP_REFERER']);// library needs this

// Get Work Order
$workOrder = caspioSelect("Work_Orders", "SiteName, TB_UNID, WO_ID, DateApproved, PcntDeduct, PcntDeductPercent", "TB_UNID = '$TB_UNID'", false);

// Populate array with WO info
foreach ($workOrder as $order) {
	$workOrderFields = getFields($order);
	
	$SiteName = $workOrderFields[0];
	$TB_UNID = $workOrderFields[1];
	$WorkOrderNo = $workOrderFields[2];
	$DateApproved = $workOrderFields[3];
	$PcntDeduct = $workOrderFields[4];
        $PcntDeductPercent=$workOrderFields[5];
}

// Get Project info
$project = caspioSelect("TR_Client_Projects", "Project_ID,Project_Name,Project_Manager,From_Email,Project_Manager_Phone", "Project_ID = '$ProjectNo'", false);

// Populate array with project info
foreach ($project as $order) {
	
	$projectFields = getFields($order);
	
	$projectID = $projectFields[0];		// Project No
	$projectName = $projectFields[1];
	$vFromName = str_replace(".", "", $projectFields[2]);
	//$vFromEmail = $projectFields[3];
	$projectCCphone = $projectFields[4];
	

	$vFromEmail = "support@fieldsolutions.com";
	
	// if project from email is blank
	if ($vFromEmail == ""){
		$vFromEmail = "projects@fieldsolutions.com";
	}
}
	
// Get Tech Contact info
$records = caspioSelect("TR_Master_List", "techID, FirstName, LastName, PrimaryEmail, ISNULL(ISO_Affiliation_ID, 0), W9", "TechID = '$techID'", false);

// Counts # of records returned
$count = sizeof($records);

// Populate array with Tech info
foreach ($records as $order) {
	
	$fields = getFields($order);
	
	// Get data from array and set values for email
	$techID = $fields[0];
	$FirstName = $fields[1];	
	$LastName = $fields[2];
	$sendToEmail = $fields[3];
	$isoID = $fields[4];
	$W9 = $fields[5];	

	$eList = $sendToEmail;
	@$caller = "wosEmailTechWhenApproved";
	$vSubject = "Work Order# $WorkOrderNo has been approved.";	

	//334			
        //$deductMsg = $PcntDeduct == "True" ? "A 10% service fee will be deducted from your final total payment amount from this work order." : "NO service fee is being deducted from this work order.";
        if(empty($PcntDeductPercent) || $PcntDeductPercent == 0)
        {
            $PcntDeductPercentMsg = "No service fee will be deducted from your final total payment amount from this work order.";
        }
        else if($PcntDeductPercent == 1)
        {
            $PcntDeductPercentMsg = "You will be paid outside of the FieldSolutions Platform for this work order.";
        }
        else
        {
            $PcntDeductPercentMsg = "A ".($PcntDeductPercent * 100)."% service fee will be deducted from your final total payment amount from this work order.";
        }
// Send Email to the Tech
	$message = "		
Dear $FirstName $LastName,

Work Order No: $WorkOrderNo has been approved. 

$PcntDeductPercent

Your payment via direct deposit (or check if you have not signed up for direct deposit) is expected within 14 business days.

Coordinator: $vFromName
Email: $vFromEmail
Phone: $projectCCphone

<a href='https://www.fieldsolutions.com/techs/Training/FS_PaymentPolicy.php'>Payment Policy</a>

Thank you.

Regards,
Field Solutions
Web: <a href='http://www.fieldsolutions.com'>www.fieldsolutions.com</a>";
		
$htmlmessage = nl2br($message);
		
}

// Emails Tech
smtpMail($vFromName, $vFromEmail, $eList, $vSubject, $message, $htmlmessage, $caller);

	/* ------------------------------------------------------ */
	/* W9 Reminder                                            */
	/* ------------------------------------------------------ */
	if ($W9 == "False" && $isoID == 0) {
		$message = "You are in jeopardy of having payment for your recently completed work order(s) being delayed.  We cannot pay you until we have a W-9 on record for you.

Instructions for submitting your W-9 can be found by clicking within the HELP tab after logging in to the site, or by clicking https://www.fieldsolutions.com/techs/w9.php.  

If you are submitting your W-9 in a company name, please include your name and/or your technician ID somewhere on the W-9.

Fax your completed W-9 to 888-258-1656, or email it to accounting@fieldsolutions.com.  W-9's are recorded as received in your profile within 1 business day.  An email will be sent to you acknowledging receipt.

Thank you for your prompt attention to this matter.

Sincerely,

Accounting
Field Solutions";
			
		$htmlmessage = nl2br($message);
		
		// Emails Tech
		smtpMailLogReceived("Field Solutions Accounting", "accounting@fieldsolutions.com", $eList, "Payment could be delayed if your W-9 is not received", $message, $htmlmessage, $caller);	
	}
}// Ends TechID check	


// BEGIN CATCH	
}catch (SoapFault $fault) {

smtpMail("WOS When Complete Unchecked", "nobody@fieldsolutions.com", "collin.mcgarry@fieldsolutions.com", "WOS When Complete Unchecked Script Error", "$fault", "$fault", "wosEmailTechWhenApproved.php");
	
}// END CATCH


?>
 
<SCRIPT LANGUAGE="JavaScript"> 
	setTimeout('close()',100); 
</SCRIPT>

