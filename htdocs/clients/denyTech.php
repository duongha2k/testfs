<?php
	require_once("../headerStartSession.php");
	require_once("../library/FSSession.php");
	FSSession::validateSessionParams(array("UserName" => "ClientID"));
?>
<?php require_once("../headerSimple.php"); ?>
<?php 
	$displayPMTools = false;
	require_once("includes/PMCheck.php"); 
?>
<script type="text/javascript">
$(document).ready(function() {
	$.ajax({
		url: '/widgets/dashboard/client-denied/deny',
		data: {
			TechID: "<?=$_GET['TechID']?>",
			FName: "<?=$_GET['FName']?>",
			LName: "<?=$_GET['LName']?>",
			Company_ID: "<?=$_GET['v']?>"			
		},
		type: 'post',
		success: function (data) {
			$("#denyForm").html(data);
		}
	});
});
</script>

<div id="denyForm"></div>