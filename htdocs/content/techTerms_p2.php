<?php $page = login; ?>
<?php $option = tech; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<p>
  <!-- Add Content Here -->
  
  <br />
  <br />
</p>
<div class="techTerms" style="width:760px">
  <h3 align="center"><b>ELECTRONIC INDEPENDENT CONTRACTOR AGREEMENT</b></h3>
  <br />
  <br />
  <p><span style="font-weight: bold">1. INFORMATION ABOUT FIELD SOLUTIONS.</span>  Field Solutions is engaged in the &ldquo;Business&rdquo; of installing, troubleshooting, servicing, and repairing the personal computers, workstations, printers, point-of-sale/POS terminals, and associated devices and software of its clients, and on occasion, the customers of its clients (both the client and a customer of a client are defined in this Agreement as a &ldquo;Client&rdquo;) (and all and each of these offerings are defined as the &ldquo;Services&rdquo;).</p>
  <p><br />
    <span style="font-weight: bold">2. INFORMATION ABOUT YOU.</span>  To be eligible to receive work order offers from Field Solutions, answer the questions below and select &ldquo;Next&rdquo; to continue.  By providing this information, you certify that you are the person identified by these items and that you are a qualified technician who possesses the necessary skill, knowledge, training, ability, and experience to perform some or all of the Services for Field Solutions&rsquo; Clients.</p>
  <p><span style="font-weight: bold">3. ACCEPTANCE OF WORK ORDERS. </span>You will be eligible to receive offers from time-to-time from Field Solutions to provide Services to Clients as set forth in separate work orders for each job only after you provide the information requested by Field Solutions and accept the terms and conditions of this Agreement.</p>
  <p>A. Each Work Order will describe the Services to be provided, the fee for the Services, any special requirements or conditions that the Client imposes such as, without limitation, satisfactory results on a background investigation and drug and/or alcohol testing, and any other terms and conditions governing the Services. Field Solutions will deliver each such Work Order to the e-mail address that you provide.  You must confirm your acceptance of the Work Order before performing any Services.  At the request of Field Solutions, you also must complete and return a tax form W-4 as a condition of an offer of a Work Order. </p>
  <p>B. You agree to provide the Services diligently and professionally and to conduct yourself in accordance with Field Solutions&rsquo; Code of Conduct for our members (which you can find at:  <a href="https://www.fieldsolutions.com/content/Members_Code_of_Conduct.pdf" target="_blank">https://www.fieldsolutions.com/content/Members_Code_of_Conduct.pdf</a> at all times while engaged on a Work Order and any rules and regulations that a Client establishes for visitors to its premises.</p>
  <p>C. You are not obligated to accept a fixed, or minimum, number of Work Orders.  You maintain sole discretion to reject an offer of a Work Order.  However, once you accept Field Solutions&rsquo; offer of a Work Order, you are responsible for rendering the Services; or you may secure a substitute contractor, or assign your own agents or employees to render the Services, provided that any such person other than you must be acceptable to Field Solutions and the Client for assuring the quality of the completion of the Work Order. </p>
  <p>D. Field Solutions has no obligation to offer you a minimum number of Work Orders.</p>
  <div class="agreeButtons">
  <ul>
    <li style="width:130px; bgcolor:#032D5F; display:inline; padding: 1px 15px; font-family:Verdana, Arial, Helvetica, sans-serif;
">
    <a href="../logOut.php" style="color:#FFFFFF; background-color:#032D5F; font-weight:bold; text-decoration:none" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F'">I Do Not Agree</a>    </li>
    <li style="width:130px; bgcolor:#032D5F; display:inline; padding: 1px 15px">
    <a href="techTerms_p3.php" style="color:#FFFFFF; background-color:#032D5F; font-weight:bold; text-decoration:none" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F'">I Agree</a>    </li>
  </ul>
  </div>
  <!--<div class="agreeButtons" style="width:130px"; bgcolor="#032D5F" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F';"><span style="color: #FFFFFF; font-weight: bold">I Agree</span></div> -->

</div>
<h1><!--- End Content --->
</h1>

<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
