<?php
	$page = "login";
	require_once("../header.php");
	require_once("../navBar.php");
?>
<style>
	h2, h2 {
		text-align: left;
	}
	h2 {
		margin-top: 10px;
		font-size: 12px;
	}
</style>
	<div style="width: 40%; margin: 0px auto">
    	<h1>Work Order Import Common Errors Messages</h1>
                
        <h2>No file uploaded</h2>
        <a name="noFile"></a>
        <div>
        	Use the browse button to locate and upload a CSV file of work orders to import. The 'Download Import Template' link will provide a template for what your CSV file should look like.
        </div>
        
		<h2>File too large (limit 1.5 MB)</h2>
        <a name="fileTooLarge"></a>
        <div>
        	The CSV file is too large. Split the upload into two or more CSV file.
        </div>        
        
        <h2>Unrecognized column: Column</h2>
        <a name="unknonwnColumn"></a>
        <div>
        	Your CSV file contains column names that are unrecognized and/or misspelled. Correct the column name or use the 'Download Import Template' link on the import work order page.
        </div>

        <h2>Duplicate column: Column</h2>
        <a name="duplicateColumn"></a>
        <div>
        	A column name appears more than once. Remove this duplicate column.
        </div>
        
        <h2>Row #: Row is blank</h2>
        <a name="blankRow"></a>
        <div>
        	A row in your CSV file is blank. Remove this row.
        </div>
        
        <h2>Row #: Column must be in this format: format requirement</h2>
        <a name="badFormat"></a>
        <div>
        	A value for a column is not formatted as required. For example, values for time typically need to be formatted as H:MM AM/PM. 3:00 AM meets the requirement while 3:00 am would not. Specify how you want your spreadsheet application to format these values so that they are saved in the CSV file correctly.
        </div>

        <h2>Row #: Column must equal Value1 OR Value2 ...</h2>
        <a name="mustEqual"></a>
        <div>
        	A value for a column must equal one of the choices listed.
        </div>
        
        <h2>Row #: Column datatype mismatch (Expected Type)</h2>
        <a name="dataTypeMismatch"></a>
        <div>
        	A value for a column is not the correct type. For example, puting a value of 100.00 in the StartDate column is a type mismatch since 100.00 is a number and the StartDate column is expecting a date value.
        </div>
        
        <h2>Row #: Values for Column1 and Column2 do not match (Column1Value / Column2Value)</h2>
        <a name="twoField"></a>
        <div>
        	Column1 and Column2 values should be related. For example, Project_ID and Project_Name are related. You will get this error if the project ID and Name do not match.
        </div>

        <h2>Row #: WO_ID found more than once (WO_ID)</h2>
        <a name="woidDupCSV"></a>
        <div>
        	One or more WO_ID were duplicated in your CSV file. WO_IDs must be unique. Create a unique id for every work order.
        </div>
        
        <h2>WO_IDs already exist: List of Duplicates</h2>
        <a name="woidDupDB"></a>
        <div>
        	One or more WO_ID in your CSV file already exists within Field Solutions. WO_IDs must be unique. Create a unique id for every work order.
        </div>
        
	</div>
<?php
	require_once("../footer.php");
?>