<?php $page = login; ?>

<?php 
$page = "techs"; 
?>
<?php $option = "training"; ?>
<?php $selected = "support"; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>



<!-- Add Content Here -->

<br />

<div id="leftcontent">

<h1>Self-Help</h1>


<p>
<b>FLS/Service Call program Support</b>
</p>

<div id="workOrders" align="center" style="padding="10px">
	<script type="text/javascript" language="JavaScript">
	<!-- 
	function loadPage(list) {location.href=list.options[list.selectedIndex].value}
	-->
	</script>

	<form name="workOrders">
	<select name="file" size="1"
  		onchange="loadPage(this.form.elements[0])"
  		target="_parent._blank"
  		onmouseclick="this.focus()">
	<option value="">-------Select One-------</option>
	<option value="http://www.flsupport.com/3.html">FLS Online Training</option>
	<option value="http://www.flsupport.com/21.html">FLS Support</option>
	<option value="http://www.flsupport.com/24.html">FLS FAQ</option>
	<option value="http://flsupport.com/20.html">FLS Work Order Help</option>
	<option value="http://flsupport.com/15.html">FLS Badge/ID help</option>
	<option value="https://www.fieldsolutions.com/techs/ttb-payment.php">FLS/TB Payment Policy</option>
	</select>
	</form>

</div>

<br /><br />


<p>
<b>Questions regarding how Field Solutions works?</b>
</p>

<p><a href="/techs/Training/FS_Training.php">How 
Field Solutions works for the Technician</a></p>
<p><a href="/techs/Training/FS_PaymentPolicy.php">Field Solutions Payment Policy</a></p>

<hr>

<p><b>Login problems?</b></p>

<p><a href="/misc/lostPassword.php">Password Recovery</a></p>


<hr noshade size="1">

<p><b>W-9 questions/problems?</b></p>

<p><a href="/techs/w9.php">Submit Your W-9 Form</a></p>


<hr noshade size="1">

<p><b>Background Check questions/problems?</b></p>

<p>Check out our <a href="/techs/background_FAQ.php">FAQ</a></p>


<br />
<br />
<br />
<br />
<br />
<br />
</div>


<div id="rightcontent">

<div id="contactUs" style="padding="10px"> 

<h1>Contact Us</h1>

<?PHP include '../techs/FAQs/_contactUs.php'; ?>
</div>

<!--- End Content --->
<?php require ("../footer.php"); ?>
		
