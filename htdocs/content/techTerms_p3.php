<?php $page = login; ?>
<?php $option = tech; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<p>
  <!-- Add Content Here -->
  
  <br />
  <br />
</p>
<div class="techTerms" style="width:760px">
  <h3 align="center"><b>ELECTRONIC INDEPENDENT CONTRACTOR AGREEMENT</b></h3>
  <br />
  <br />
  <p style="font-weight: bold">4. YOUR DUTIES AS AN INDEPENDENT CONTRACTOR</p>
  <p>A. You will render the Services as an independent contractor.  You will pay any and all payroll, self-employment, or other taxes of any nature imposed upon the compensation paid by Field Solutions to You for the rendering of the Services. </p>
  <p>B. You will obtain workers compensation insurance if required by law, and keep such records, pay such other taxes or governmental fees, comply with applicable law and maintain such licenses as are required to perform the Services.  You will be solely responsible for obtaining and purchasing liability insurance in such amounts and coverage as you select.  Field Solutions will not provide any contributions to unemployment insurance or workers compensation funds.  Certain Clients may require specific insurance coverages.  You will carry such insurance as may be required by an applicable Work Order.  Field Solutions recommends that You carry the following minimum insurance coverage: (i) workers compensation insurance as prescribed by the law of the state in which Consultant renders Services; (ii) employer liability insurance with limits of at least $1,000,000 per occurrence; (iii) comprehensive automobile liability insurance with a combined single limit per occurrence of at least $1,000,000 for bodily injury and property damage; (iv) comprehensive general liability insurance, including, broad form/property damage, bodily injury, products and completed operations, with a combined single limit per occurrence of at least $1,000,000; and (v) professional liability insurance with a minimum limit of $1,000,000 per occurrence.  You will provide proof of required insurance coverage upon request by Field Solutions. </p>
  <p>C. Field Solutions will not, and declines to, specify or control the means or methods of how you will render the Services.  Field Solutions is relying on your skills, knowledge, training, ability, and experience in the Services to enable you to determine and control how to render the Services in a Work Order in order to satisfy the objective of the Client and without Field Solutions supervision.</p>
  <p>D. Except for the purpose of complying with a Work Order, Field Solutions has not provided, and will not provide, you with training or individual instruction with respect to how you render the Services to a Client&rsquo;s satisfaction.  Except for reading manuals associated with a Work Order, you will obtain the necessary technical proficiency to render the Services at your own expense, or at the expense of another person or entity, prior to becoming a contractor to Field Solutions. </p>
  <p>E. Field Solutions will not provide any tools, equipment or office space to you.  You will set your hours and work schedule as you deem appropriate to render the Services.</p>
  <p>F. You will not be entitled to any rights or benefits provided to employees of Field Solutions including, without limitation, health insurance benefits, vacation or other time off, and other employee benefits.  You are responsible for any expenses incurred to perform the Services.</p>
  <p>G. You have no obligation to keep reports or logs of any type to justify the time spent rendering Services on a Work Order, except that you will record your hours on Field Solutions&rsquo; web site in order to verify the fee due and owing to you on a given Work Order and to permit Field Solutions to estimate the cost of similar Services in the future.</p>
  <p>H. The nature of the Services is such that this Agreement does not contemplate that you will have assistants to render the Services.  You will notify Field Solutions in writing if you desire to retain such assistants, and the terms and conditions of the use of such individuals will be determined in discussions between you and Field Solutions.</p>
  <p>I. The nature of the Services is also such that you will render them on the location of the Client, which may be either a residence or commercial establishment.  You will at all times comply with the applicable rules, policies and procedures that the Client establishes for visitors to the Client&rsquo;s premises. </p>
  <div class="agreeButtons">
  <ul>
    <li style="width:130px; bgcolor:#032D5F; display:inline; padding: 1px 15px; font-family:Verdana, Arial, Helvetica, sans-serif;
">
    <a href="../logOut.php" style="color:#FFFFFF; background-color:#032D5F; font-weight:bold; text-decoration:none" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F'">I Do Not Agree</a>    </li>
    <li style="width:130px; bgcolor:#032D5F; display:inline; padding: 1px 15px">
    <a href="techTerms_p4.php" style="color:#FFFFFF; background-color:#032D5F; font-weight:bold; text-decoration:none" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F'">I Agree</a>    </li>
  </ul>
  </div>
  <!--<div class="agreeButtons" style="width:130px"; bgcolor="#032D5F" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F';"><span style="color: #FFFFFF; font-weight: bold">I Agree</span></div> -->

</div>
<h1><!--- End Content --->
</h1>

<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
