<?php $page = login; ?>
<?php $option = tech; ?>
<?php $displayHome = 'no'; ?>
<?php 
require_once("../library/caspioAPI.php");
if($_POST['agreeICA'] == "agreeICAYes"){
		$date = date("Y-m-d H:i:s");
		$result = Core_Caspio::caspioUpdate("TR_Master_List", array("AcceptTerms","Date_TermsAccepted"), array("Yes", $date), "TechID = '".$_SESSION['TechID']."'");
		if($result){
			$_SESSION["acceptedICA"] = "Yes";
		}
		
		header("Location: /techs/dashboard.php");
		exit;
	}

?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>

<p>
  <!-- Add Content Here -->
  
  <br />
  <br />
</p>
<script type="text/javascript">
	var warnedICA = false;
	function openPopup(url) {
		newwindow=window.open(url,'techTerms','height=500, width=740, left=100, top=100, resizable=yes, scrollbars=yes, toolbar=no,status=no');
		if (window.focus) {newwindow.focus()}
	}
	function agreeICA() {
		$("#agreeICAYes").attr({"disabled" : false, "checked" : true});
	}
	function notAgreeICA() {
		$("#agreeICANo").attr({"checked" : true});
	}
	function checkForm() {
		if (!$("#agreeICAYes").attr("checked") && !warnedICA) {
			alert("You cannot apply or bid on work orders until the Independent Contractor Agreement is agreed to.");
			warnedICA = true;
			return false;
		}
		if (!$("#agreeICAYes").attr("checked") && warnedICA) {
			$("#agreementForm").attr("action", "/techs/dashboard.php");
		}
	}
	$(document).ready(function () {
		$("#agreementForm").submit(checkForm);
	});
</script>
<style type="text/css">
	#agreeDiv {
		width: 600px;
		margin: 50px auto;
	}
	#agreeDiv ul {
		width: 90%;
		margin: 0px auto;
	}
	#agreeDiv a {
		text-decoration: none;
		font-weight: bold;
	}
	#agreeDiv a:hover {
		text-decoration: underline;
	}
	#agreeDiv div {
		width: 100%;
		text-align: center;
	}
</style>
<div id="agreeDiv">
	You haven't agreed to the Independent Contractor Agreement yet. You must agree in order to bid on or apply for work orders:<br/><br/>
	<form id="agreementForm" name="agreementForm" method="post" action="<?=$_SERVER['PHP_SELF']?>">
    	<ul>
			<li><a href="javascript:openPopup('techTerms_combined.php')">Independent Contractor Agreement</a><br/><input id="agreeICAYes" name="agreeICA" type="radio" disabled="disabled" value="agreeICAYes" />&nbsp;I&nbsp;Agree&nbsp;&nbsp;&nbsp;<input id="agreeICANo" name="agreeICA" type="radio" checked="checked"  value="agreeICANo" />&nbsp;I&nbsp;Do&nbsp;Not&nbsp;Agree&nbsp;(Must <a href="javascript:openPopup('techTerms_combined.php')"><u>view</u></a> and scroll to bottom of page to agree)</li>
		</ul>
        <br/><br/>
	    <div><input id="agreeSubmit" name="agreeSubmit" type="submit" value="Submit"></div>
	</form>
</div>

</body>
</html>
		
