<?php $page = 'aboutus'; ?>
<?php $option = 'ourMembers'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>

<div id="container">


<div id="header">

    <div id="wrapper">
        <div id="content">
       
<div id="aboutUsWrapper"> 
<div id="aboutUsHdr">Our Member - Technicians</div>
    
<!-- Add Content Here -->

<p id="aboutUsText">
FieldSolutions<sup><font size="1">TM</font></sup> members are just that: Members. Not contractors, not substitutes. FieldSolutions<sup><font size="1">TM</font></sup> works tirelessly to help you be successful.
</p>

<p id="aboutUsText">
FieldSolutions<sup><font size="1">TM</font></sup> members are technology hardware, software, field service and quality assurance people wanting to work on their own terms. Expertise of interest include: personal computers, point of sale systems, WiFi hubs and routers, telephone systems, and other technologies (register now to check out the whole list).
</p>

<p id="aboutUsText">
FieldSolutions<sup><font size="1">TM</font></sup> members are hardworking with a focus on delivering quality. Because we have a steady stream of work, you will have a steady stream of work. Because we follow up with our clients and the install location managers, we ensure world-class client satisfaction. 
</p>

<p id="aboutUsText">
By being the best at what we do, we all get more jobs to deliver.
</p>

<div id="aboutUsSubHdr">
What our members seek in working with FieldSolutions<sup><font size="1">TM</font></sup>:
</div>

<p>
<ul id="aboutUsList">
<li>Work and income opportunities within the flexible life style that they have chosen </li>
<li>Work with a staffing program that is reputable, trustworthy and transparent </li>
<li>Work with the world's leading equipment providers and service companies </li>
<li>Work with a company that is committed to quality </li>
</ul>
</p>



<br /><br />
</div>

</div></div>

<!--- End Content --->
<?php require ("../footer.php"); ?>
		
