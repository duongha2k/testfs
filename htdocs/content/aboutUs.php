<?php $page = 'aboutus'; ?>
<?php $option = 'aboutus'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>

<div id="container">
	<div id="header">
		<div id="wrapper">
			<div id="content">
				<div id="aboutUsWrapper"> 
				<div id="aboutUsHdr">Our Business</div>
    
<!-- Add Content Here -->
<table width="100%" cellspacing="0" cellpadding="0">
  <tr>
    <td><div id="aboutUsSubHdr">
Quality Field Sourcing delivers all our clients' field technician requirements 
		</div>

<p>
<ul id="aboutUsList">
<li>Electronics equipment providers come to FieldSolutions<sup><font size="1">TM</font></sup> to achieve a breakthrough in performance and total cost of their Field Service Technical Operations by deploying our certified contractors as a supplemental field service resource.</li>
<li>FieldSolutions<sup><font size="1">TM</font></sup> North American network of technicians provide single order break/fix and national technology deployment for national field programs.</li>
<li>By using Field Solutions<sup><font size="1">TM</font></sup> as their single source for field technician contractors across North America our clients achieve cost savings of up to 70% compared to full time field staff employees or the cost of multiple layers of service companies and sub-contractors. </li>
</ul>
</p>

<div id="aboutUsSubHdr">
Custom FieldSolutions<sup><font size="1">TM</font></sup> programs support all Field technician needs of our clients, including:
</div>

<p>
<ul id="aboutUsList">
<li>Lowest cost client self service Web-based work order fulfillment</li>
<li>Full Service program management support for complete dispatch and service outsourcing </li>
<li>Level 1 telephonic or web based service call handling </li>
</ul>
</p>

<div id="aboutUsSubHdr">
FieldSolutions<sup><font size="1">TM</font></sup> is a leader in providing field technicians efficiently and effectively</div>

<p>
<ul id="aboutUsList">
<li>FieldSolutions<sup><font size="1">TM</font></sup> database of over 17,000 hard working individuals provide a cost effective, perfectly utilized field technician resource. </li>
<li>FieldSolutions<sup><font size="1">TM</font></sup> technician-contractor skills, certifications and individual performance histories enable us to identify those contractors that "fit the bill" for our clients, yielding the best field operations results. </li>
<li>FieldSolutions<sup><font size="1">TM</font></sup> unique reverse bidding process allows us to apply our clients' cost strategy and achieve the lowest total cost, while maintaining technician selection control for quality assurance. </li>
</ul>
</p>
</td>
    <td align="top" valign="top">
    <br />
    
<div id="none" style="border:thick; border-color:#0000FF;">
</div>

      <style type="text/css">
		table#associations {border: 1px solid #174065;
		margin-left:5px;
			/*margin-bottom: 15px;
			padding: 10px;*/}
		
		div#associationsTitle {font-family:Verdana, Arial, Helvetica, sans-serif;font-size:14px; color:#012A5E; font-weight:bold;}
	</style>
    
<table id="associations" width="215">
  <tr>
    <td align="center" valign="top"><div id="associationsTitle">Partners & Memberships</div></td>
  </tr>
  <tr>
    <td align="center" valign="top"><a href="http://www.goRSPA.org/" target="_blank" style="text-decoration:none; margin-left:20px"><img src="images/RSPA.jpg" /></a></td>
  </tr>
  <tr>
    <td align="center" valign="top"><a href="http://www.goRSPA.org/" target="_blank" style="text-decoration:none">www.goRSPA.org</a><br /><br /></td>
  </tr>
  <tr>
    <td align="center" valign="top"><a href="http://www.NRF.org" target="_blank" style="text-decoration:none; margin-left:20px"><img src="images/NRF.jpg" /></a></td>
  </tr>
  <tr>
    <td align="center" valign="top"><a href="http://www.NRF.org" target="_blank" style="text-decoration:none">www.NRF.org</a><br /><br /></td>
  </tr>
  <tr>
    <td align="center" valign="top"><a href="http://www.CompTIA.org" target="_blank" style="text-decoration:none; margin-left:20px"><img src="images/CompTIA.jpg" /></a></td>
  </tr>
  <tr>
    <td align="center" valign="top"><a href="http://www.CompTIA.org" target="_blank" style="text-decoration:none">www.CompTIA.org</a></td>
  </tr>
</table>
</td>
  </tr>
</table>




<br /><br />

</div>
</div>
</div></div>

<!--- End Content --->
<?php require ("../footer.php"); ?>
		
