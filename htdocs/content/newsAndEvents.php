<?php $page = 'aboutus'; ?>
<?php $option = 'newsAndEvents'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>

<!--- Add Content Here--->


<style type="text/css">

#body {
	margin: 0 50px;
}

#body h1 {
	font-size: 2em;
}

#body h2 {
	font-size: 1.5em;
}

#body h3 {
	font-size: 1.25em;
	text-align: left;
	padding: 15px 0 0 0;
}

#body dl {
	margin: 10px 0 20px 20px;
}

#body dt {
	font-weight: bold;
	float: left;
	margin-bottom: 5px;
	width: 200px;
}

#body dd {
	margin-bottom: 5px;
	margin-left: 200px;
}

#body dd a {
	font-size: 0.8em;
}

</style>

<div id="body">
<h1>News and Events</h1>
    
<div class="news">
  <h2>News</h2>
  <dl>
	<dt>Apr 2, 2010</dt>
    <dd>Field Solutions announces new Performance & Preference&trade; provider ratings system combining ratings of technician preference and service event performance <a href="Field Solutions New Preference & Performance ratings - Go Live Apr 2 2010.pdf" target="_blank">Read more</a></dd>
  	<dt>Feb 2, 2010</dt>
    <dd>Field Solutions releases FS-Parts Manager within Version 1.5 of its leading Field Solutions Work Order Management System... <a href="Field Solutions New FS-Parts Manager Module.pdf" target="_blank">Read more</a></dd>  
  	<dt>Jan 20, 2010</dt>
    <dd>Field Solutions 2009 Annual Recap: 38.8% Annual Growth <a href="Field Solutions Annual Recap - News Release 01_19_2010.pdf" target="_blank">Read more</a></dd>  
  	<dt>Aug 4, 2009</dt>
    <dd>Field Solutions announces Strategic Alliance with People To GO in Canada <a href="Field Solutions and People To Go Alliance - News Release August 4 2009.pdf" target="_blank">Read more</a></dd>  
  	<dt>Aug 1, 2009</dt>
    <dd>Field Solutions announces Push2Tech;&trade;: a Breakthrough in SLA management for technology service companies <a href="Field Solutions Breakthrough - Push2Tech(tm) - Go Live August 1 2009.pdf" target="_blank">Read more</a></dd>  
  	<dt>July 27, 2009</dt>
    <dd>Field Solutions Reports 89.6% First Half 2009 Growth <a href="Field Solutions Reports Strong 1H2009 Revenue Growth Continues - News Release July 27 2009.pdf" target="_blank">Read more</a></dd>  
  	<dt>July 8, 2009</dt>
    <dd>Field Solutions Announces New Automated Contact Services <a href="Field Solutions Speed and Cost Savings Tools - ACS.pdf" target="_blank">Read more</a></dd>
  	<dt>May 13, 2009</dt>
    <dd>Field Solutions Announces announces new system enhancements <a href="Field Solutions System Enhancements Announced for May 13 2009.pdf" target="_blank">Read more</a></dd>

  	<dt>March 19, 2009</dt>
    <dd>Field Solutions Announces New National Helping Hands resource <a href="Field Solutions Announces New National Helping Hands Resource - March 19 2009 Final.pdf" target="_blank">Read more</a>&nbsp;&nbsp;<a href="National Helping Hands FAQ March 2009.pdf" target="_blank">National Helping Hands FAQ</a></dd>
  	<dt>March 10, 2009</dt>
    <dd>Field Solutions Announces 
New Telephony and Cable/Wiring "TCW" Sourcing Division, and Additions to Client Development Team <a href="Field Solutions Announces New Telephony-Cabling-Wiring Division and  Client Services Additions - March 10 2009 Final.pdf" target="_blank">Read more</a></dd>
  	<dt>November 6, 2008</dt>
    <dd>Field Solutions Announces Accelerating Growth and new "Technician Resource Center" ("TRC") <a href="FieldSolutionsAnnouncesAcceleratingGrowthandNewTRCFunction-Nov52008.pdf" target="_blank">Read more</a></dd>
  	<dt>July 28, 2008</dt>
    <dd>Field Solutions Announces 2nd Quarter results: Rapid Growth and New Services <a href="Rapid Growth and New Services.pdf" target="_blank">Read more</a></dd>
  	<dt>May 8, 2008</dt>
    <dd>Field Solutions Announces New Clients and Resource Additions <a href="Field Solutions Announces Growth and Expansion - May 8 2008.pdf" target="_blank">Read more</a></dd>
	<dt>April 21, 2008</dt>
    <dd>Field Solutions Introduces  FieldStaff&#8482; Employee Leasing Services for Field Service Operations &hellip;<a href="Launch%20of%20FieldStaff.pdf" target="_blank">Read more</a></dd>
    <dt>March 5, 2008</dt>
    <dd>Field Solutions Announces Launch of Upgraded Operating Website &hellip; <a href="Field Solutions Redesigned WebSite.pdf" target="_blank">Read more</a></dd>
    <dt>December 14, 2007</dt>
    <dd>FieldSolutions and TechnicianBureau merger announced... <a href="FieldMart_and_Technician_Bureau_Announcement.pdf" target="_blank">Read more</a></dd>
      <dt>December 10, 2007</dt>
      <dd>FieldSolutions Joins the National Retail Federation</dd>
      <dt>July 15, 2007</dt>
      <dd>FieldSolutions releases White Paper: The Competitive Power of Dynamic Field Sourcing-&#169;2007... <a href="whitepaperRequest.php">Request whitepaper</a></dd>
      <dt>July 15, 2007</dt>
      <dd>FieldSolutions joins RSPA, Retail Solutions Provider Association</dd>
  </dl>
</div> <!-- end DIV "news" -->

<div class="events">
  <h2>Events</h2>
  <h3>Upcoming Events</h3>
  <dl>
    <dt>April 16-18, 2008</dt>
    <dd>KioskCom, Las Vegas, NV <a href="mailto:mreader@fieldsolutions.com?subject=Request a meeting at KioskCom">Request a meeting</a></dd>
    <dt>April 16-19, 2008</dt>
    <dd>FSTec, Grapevine, TX <a href="mailto:mreader@fieldsolutions.com?subject=Request a meeting at FSTec">Request a meeting</a></dd>
    <dt>April 23-5, 20008</dt>
    <dd>Hotel Technology Forum, Las Vegas, NV <a href="mailto:mreader@fieldsolutions.com?subject=Request a meeting at Hotel Technology Forum">Request a meeting</a></dd>
    <dt>Apr 27-May 2, 2008</dt>
    <dd>InterOp, Las Vegas, NV <a href="mailto:mreader@fieldsolutions.com?subject=Request a meeting at KioskCom">Request a meeting</a></dd>
    <dt>June 2-3, 2008</dt>
    <dd>CompTIA Services Section, Montreal, Ont <a href="mailto:mreader@fieldsolutions.com?subject=Request a meeting at CompTIA Services Section">Request a meeting</a></dd>
    <dt>July 12-17, 2008</dt>
    <dd>RSPA RetailNow Convention, Las Vegas, NV <a href="mailto:mreader@fieldsolutions.com?subject=Request a meeting at RSPA RetailNow Convention">Request a meeting</a></dd>
    <dt>August 5-7, 2008</dt>
    <dd>CompTIA "BreakAway" Conference, Orlando, FL <a href="mailto:mreader@fieldsolutions.com?subject=Request a meeting at CompTIA 'BreakAway' Conference">Request a meeting</a></dd>
    <dt>Aug. 10-12, 2008</dt>
    <dd>NRF Tech, IT Leadership Conference, Boulder, CO <a href="mailto:mreader@fieldsolutions.com?subject=Request a meeting at NRF Tech, IT Leadership Conference">Request a meeting</a></dd>
  </dl>
  
  
  <h3>Past Events</h3>
  <dl>
    <dt>February 2-7. 2008</dt>
    <dd>FieldSolutions attends RSPA Inspire Conference, St. Thomas, USVI...</dd>
    <dt>January 14-17, 2008</dt>
    <dd>FieldSolutions attends National Retail Federation "Big Show", New York City, NY... </dd>
    <dt>November 7-9, 2007</dt>
    <dd>FieldSolutions attends The NACS show, Atlanta, GA... </dd>
    <dt>October 11-13, 2007</dt>
    <dd>FieldSolutions attends FS/Tech, Atlanta, GA... </dd>
    <dt>September 20-23, 2007</dt>
    <dd>FieldSolutions attends BlueStar VarTech, Nashville, TN... </dd>
    <dt>July 15-19, 2007</dt>
    <dd>FieldSolutions attends  RSPA RetailNow conference, Las Vegas Nevada</dd>
  </dl>
</div> <!-- end DIV "events" -->

</div> <!-- end DIV "body"-->
<!--- End Content --->
<?php require ("../footer.php"); ?>
		
