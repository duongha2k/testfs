<?php $page = 'aboutus'; ?>
<?php $option = 'ourClients'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>

<div id="container">


<div id="header">

    <div id="wrapper">
        <div id="content">
       
<div id="aboutUsWrapper"> 
<div id="aboutUsHdr">Our Clients</div>
    
<!-- Add Content Here -->

<p id="aboutUsText">FieldSolutions<sup><font size="1">TM</font></sup> clients are just that: clients. By this we mean that we will take care of all of your field service technician sourcing needs. We are committed to being the most complete, and easiest to use field sourcing provider. By taking care of you, our client, we are confident that you will come back to us time and again. You are our clients, not just a customer placing a work order.
</p>

<p id="aboutUsText">FieldSolutions<sup><font size="1">TM</font></sup> clients are technology companies: hardware and software product companies; manufacturers, dealers, resellers, installers and service providers seeking a one-stop shop for quality field support across all of North America.
</p>

<p id="aboutUsText">
We leverage our database with the leading job order management technology, a global service team, and comprehensive Quality Field Sourcing<sup><font size="1">TM</font></sup> services.
</p>

<div id="aboutUsSubHdr">
Our Clients Business include:
</div>

<p>
<ul id="aboutUsList">
<li>Original Equipment Manufacturers </li>
<li>Distributors </li>
<li>Value Added Resellers </li>
<li>Dealers </li>
<li>Installation and Repair Service Companies </li>
</ul>
</p>


<div id="aboutUsSubHdr">
Our clients strategic goals for working with FieldSolutions include:
</div>

<p>
<ul id="aboutUsList">
<li>Reduced total cost of field service - installation, break/fix or warrantee maintenance </li>
<li>Expanded geographic field service coverage  </li>
<li>Increased peak-load service staffing performance  </li>
<li>Increased roll-out and program implementation capacity and speed  </li>
<li>Improved quality and time to serve end-user customers  </li>
<li>Reduced investment in deploying a North American-wide field service organization  </li>
</ul>
</p>

<br /><br />
</div>

</div></div>

<!--- End Content --->
<?php require ("../footer.php"); ?>
		
