<html>
	<head>
    	<title>ACS Statement of Fees</title>
		<script type="text/javascript" src="https://www.fieldsolutions.com/library/jquery/jquery-1.2.1.pack.js"></script>
        <script type="text/javascript">
			function ACSAuthorize() {
				try {
					var prefix = opener.prefix;
					if (opener.didACSChange()) {
						opener.$(prefix + "ACSAuthBy").attr("value", opener.currentUser);
						opener.$(prefix + "ACSAuthDate").attr("value", opener.serverDateTime);
						opener.$("#acsAuthBy").html(opener.$(prefix + "ACSAuthBy").val());
						if (opener.autoSubmitAfterACSAuthorize)
							opener.$("#caspioform").submit();
					}
					window.close();
				}
				catch (e) {}
			}
			
			$(document).ready(function() {
				try {
					if (opener.didACSChange())
						$("#closeBtn").attr("value", "I Accept");
				}
				catch (e) {}
				$(window).blur(function() {
//					window.close();
				});
			});
		</script>
	</head>
<body>
	<div>
    	ACS Fees and Purchase Terms
	</div>
    <ul>
	    <li style="font-weight: bold">
			Automated Contact Services Fees: Each call service item will incur a fee of $.25 per approved or deactivated work order. If the bundle of all 8 contact services is selected, a fee of $1.50 per approved or deactivated work order  will be invoiced.
		</li>
        <li>
			All ACS services fees are extra and will be in addition to all other self service and assisted self service fees. ACS service fees for full service and fixed price projects will be established and quoted separately.
		</li>
        <li>
			ACS fees will be an integral part of, and included within, the receivables for the invoice in which they are billed. 
ACS purchase selection cannot be changed after a work order is assigned. 
		</li>
	</ul>
    
   <div style="text-align:center">
		<input id="closeBtn" type="button" onClick="ACSAuthorize()" value="Close"/>
   </div>
</body>
</html>