<?php $page = login; ?>
<?php $option = tech; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<p>
  <!-- Add Content Here -->
  
  <br />
  <br />
</p>
<div class="techTerms" style="width:760px">
  <h3 align="center"><b>ELECTRONIC INDEPENDENT CONTRACTOR AGREEMENT</b></h3>
  <br />
  <span style="font-weight: bold"><br />
  </span>
  <p><span style="font-weight: bold">5. LIMITATION OF AUTHORITY.</span>  You will not have any authority to:  (a) bind or obligate Field Solutions or subject Field Solutions to any liability except as expressly provided in this Agreement, or (b) do any other act on behalf of Field Solutions that is not expressly authorized in this Agreement.</p>
  <p><span style="font-weight: bold">6. WARRANTY. </span>You warrant that the Services that you will render will be performed in a timely and professional manner.  You also warrant that you will comply with all applicable laws, rules and regulations in the performance of the Services.</p>
  <p><span style="font-weight: bold">7. COMPENSATION. </span>All compensation paid to you is to be considered compensation for the proper handling by you of all phases of rendering the Services.  Field Solutions will pay you the fee specified in a given Work Order for your rendering of Services, and on the terms set forth in the Work Order.  You will receive no other compensation other than as set forth in the Work Order.  You will be paid within 30 days after satisfactory completion of the Services.   Field Solutions will report payments to you for tax purposes on a Form 1099. </p>
  <p><span style="font-weight: bold">8.
    CLIENT DISPUTES.</span>  If any Client is involved in a dispute with you arising from or related to your performance of the Services, Field Solutions may, in its sole discretion, assist you and such Client in resolving the dispute; provided, however, that Field Solutions will be solely responsible for all Client fee, billing, and payment matters.  In the event of any dispute between you and a Client related to the Services that you provide, you will notify Field Solutions in writing of such dispute within one business day of becoming aware of such dispute.  You will use your best efforts to resolve any such disputes within two business days after receiving such notice from a Client.  Upon notice of any dispute, Field Solutions reserves the right to offer the applicable Work Order to another Field Solutions contractor with no payment obligation due or owing to you.</p>
  <p><br />
    <span style="font-weight: bold">9. INDEMNIFICATION.</span>  You will indemnify, defend and hold Field Solutions harmless from any and all liability, obligation, loss, damage, injury, penalty, action, judgment, suit, claim, cost, expense or disbursement of any kind or nature that may be imposed on, incurred by or served against Field Solutions by any Client or other person or entity arising out of, or in connection with the Services, or any alleged act or neglect of you.  You will provide Field Solutions with immediate notification of any claim received that names, or in any way involves, Field Solutions.  Nothing in this Section affects your status as an independent contractor. </p>
  <p><span style="font-weight: bold">10. 
    TERMINATION.</span>  This Agreement and your Services may be terminated by either party without advance notice.</p>
  <div class="agreeButtons">
  <ul>
    <li style="width:130px; bgcolor:#032D5F; display:inline; padding: 1px 15px; font-family:Verdana, Arial, Helvetica, sans-serif;
">
    <a href="../logOut.php" style="color:#FFFFFF; background-color:#032D5F; font-weight:bold; text-decoration:none" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F'">I Do Not Agree</a>    </li>
    <li style="width:130px; bgcolor:#032D5F; display:inline; padding: 1px 15px">
    <a href="techTerms_p5.php" style="color:#FFFFFF; background-color:#032D5F; font-weight:bold; text-decoration:none" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F'">I Agree</a>    </li>
  </ul>
  </div>
  <!--<div class="agreeButtons" style="width:130px"; bgcolor="#032D5F" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F';"><span style="color: #FFFFFF; font-weight: bold">I Agree</span></div> -->
</div>
<h1><!--- End Content --->
</h1>

<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
