<html>
	<head>
    	<title>ACS Statement of Fees</title>
		<script type="text/javascript" src="https://www.fieldsolutions.com/library/jquery/jquery-1.2.1.pack.js"></script>
        <script type="text/javascript">
			function P2TAuthorize() {
				try {
					if (opener.didP2TChange()) {
						opener.acceptedP2T = true;
						if (opener.autoSubmitAfterP2TAuthorize)
								opener.$("#caspioform").submit();
					}
					window.close();
				}
				catch (e) {}
			}
			
			$(document).ready(function() {
				try {
					if (opener.didP2TChange())
						$("#closeBtn").attr("value", "I Accept");
				}
				catch (e) {}
				$(window).blur(function() {
//					window.close();
				});
			});
		</script>
	</head>
<body>
	<div>
    	Push2Tech&trade; Fees and Usage Terms:
	</div>
    <ul>
	    <li>
			In selecting the Push2Tech&trade; auto-assignment feature, you are also selecting the ACS 8- contact actions bundle.
		</li>
        <li>
			Each work order linked to this project will carry the Push2Tech&trade; auto-assignment business rules selected with this project.
		</li>
        <li>
			Each work order completed or deactivated will incur the ACS 8-action bundle fee of $1.50 in addition to any other self service  and assisted service fees.
		</li>
        <li>
	        All fees and purchase terms of the ACS Automated Contact services will apply.
        </li>
	</ul>
    
   <div style="text-align:center">
		<input id="closeBtn" type="button" onClick="P2TAuthorize()" value="Close"/>
   </div>
</body>
</html>