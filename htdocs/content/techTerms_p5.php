<?php $page = login; ?>
<?php $option = tech; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<p>
  <!-- Add Content Here -->
  
  <br />
  <br />
</p>
<div class="techTerms" style="width:760px">
  <h3 align="center"><b>ELECTRONIC INDEPENDENT CONTRACTOR AGREEMENT</b></h3>
  <br />
  <span style="font-weight: bold"><br />
  </span>
  <p><span style="font-weight: bold">11. CONFIDENTIALITY.</span>  You will at all times maintain in strict confidence all personal information of Field Solutions&rsquo; Clients that you receive in the course of providing Services, including all Work Orders, Client addresses, information on Client premises to which you have access or obtain control.  Such information will remain the exclusive property of Field Solutions and its Clients.  You will not retain the originals or any copies of such information without Field Solutions&rsquo; prior written permission. </p>
  <p><span style="font-weight: bold">12. COVENANTS.</span>  Field Solutions makes considerable efforts and incurs substantial expense to market to prospective Clients requiring information about the Business.  Accordingly, while engaged to render Services on any Work Order, you will not knowingly, directly or indirectly, solicit any Client or prospective Client who is a Client or prospective Client of Field Solutions or any of Field Solutions&rsquo; affiliates for the purpose of providing services that are competitive with the Business of Field Solutions. </p>
  <p><span style="font-weight: bold">13. 
    REMEDIES.</span>  Any breach of any of the covenants or agreements set forth in Sections 11, 12 and 13 of this Agreement will cause Field Solutions irreparable harm for which there is no adequate remedy at law, and, without limiting whatever other rights and remedies Field Solutions may have under this paragraph, you consent to the issuance of an injunction in favor of Field Solutions enjoining the breach of any of the preceding covenants or agreements by any court of competent jurisdiction. </p>
  <p><span style="font-weight: bold">14. 
    SURVIVING RIGHTS.</span>  Notwithstanding the termination of this Agreement, the parties will be required to carry out any provisions of this Agreement that contemplate performance subsequent to such termination; and such termination will not affect any liability or other obligation that will have accrued prior to such termination including, but not limited to, any liability for loss or damage on account of a prior default.</p>
  <div class="agreeButtons">
  <ul>
    <li style="width:130px; bgcolor:#032D5F; display:inline; padding: 1px 15px; font-family:Verdana, Arial, Helvetica, sans-serif;
">
    <a href="../logOut.php" style="color:#FFFFFF; background-color:#032D5F; font-weight:bold; text-decoration:none" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F'">I Do Not Agree</a>    </li>
    <li style="width:130px; bgcolor:#032D5F; display:inline; padding: 1px 15px">
    <a href="techTerms_p6.php" style="color:#FFFFFF; background-color:#032D5F; font-weight:bold; text-decoration:none" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F'">I Agree</a>    </li>
  </ul>
  </div>
  <!--<div class="agreeButtons" style="width:130px"; bgcolor="#032D5F" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F';"><span style="color: #FFFFFF; font-weight: bold">I Agree</span></div> -->
</div>
<h1><!--- End Content --->
</h1>

<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
