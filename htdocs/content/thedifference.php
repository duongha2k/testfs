<?php $page = 'aboutus'; ?>
<?php $option = 'thedifference'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>

<div id="container">


<div id="header">

    <div id="wrapper">
        <div id="content">
       
<div id="aboutUsWrapper"> 
<div id="aboutUsHdr">FieldSolutions<sup><font size="1">TM</font></sup> Difference for Clients</div>
    
<!-- Add Content Here -->
<br /><br />
<p>
FieldSolutions<sup><font size="1">TM</font></sup> is much more than a web-enabled job board, we are a service company that has embraced the internet as a way to deliver a breakthrough strategic advantage to our clients:
</p>

<div id="aboutUsSubHdr">
FieldSolutions<sup><font size="1">TM</font></sup> Automation Advantages
</div>


<p>
<ul id="aboutUsList">
<li>Superior database and knowledge of our contractors </li>
<li>FieldSolutions technician performance measurements </li>
<li>Innovative reverse bid process enables breakthrough work order cost control and performance </li>
<li>Advanced national service program and ad hoc on line work order uploading capabilities </li>
<li>Integration with parts management and reverse logistics programs </li>
</ul>
</p>


<div id="aboutUsSubHdr">
FieldSolutions<sup><font size="1">TM</font></sup> Program Management Advantages
</div>


<p>
<ul id="aboutUsList">
<li>Full Service program management for large one time, or on-going client requirements provides a field management resource with out the cost burden of adding full time staff  </li>
<li>Complete client reporting options  </li>
<li>Automated job open/job completion feedback systems  </li>
<li>Custom banking options </li>
</ul>
</p>

<div id="aboutUsSubHdr">
FieldSolutions<sup><font size="1">TM</font></sup> Quality Assurance Advantages
</div>


<p>
<ul id="aboutUsList">
<li>Full Service program management for large one time, or on-going client requirements provides a field management resource with out the cost burden of adding full time staff  </li>
<li>Complete client reporting options  </li>
<li>Automated job open/job completion feedback systems  </li>
<li>Custom banking options </li>
</ul>
</p>

<br /><br />
</div>

</div></div>

<!--- End Content --->
<?php require ("../footer.php"); ?>
		
