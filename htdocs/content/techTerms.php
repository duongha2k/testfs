<?php $page = login; ?>
<?php $option = tech; ?>
<?php $displayHome = 'no'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>

<p>
  <!-- Add Content Here -->
  
  <br />
  <br />
</p>
<div class="techTerms" style="width:760px">
  <h3 align="center"><b>ELECTRONIC INDEPENDENT CONTRACTOR AGREEMENT</b></h3>
  <br />
  <br />
  <p><b>Introduction</b></p>
  <p>This is an AGREEMENT between you and Field Solutions, LLC (&ldquo;Field Solutions&rdquo;) that defines the terms and conditions for Field Solutions&rsquo; to engage you to provide services to our customers as described in this agreement (&ldquo;Agreement&rdquo;).<br>
    <br>
    By clicking the &ldquo;I ACCEPT&rdquo; button below, you agree that:
  <p>
  <ol style="margin-left:50px">
    <li>You are creating a contract with Field Solutions that is legally binding, and that you expect to be enforced.<br />
        <br />
    </li>
    <li>Your acceptance of this agreement by electronic means will not affect its enforcement, and that you waive the use of traditional paper documents signed in writing.</li>
    </ol>
  <p>This Agreement will not be effective unless and until you accept its terms by clicking the &ldquo;I ACCEPT&rdquo; button on each screen.  You may obtain a copy of the electronic record of this Agreement, or a copy in a medium that is retrievable, by a written request sent via email transmission to Field Solutions e-mailbox whose address Field Solutions will provide. </p>

<div class="agreeButtons">
  <ul>
    <li style="width:130px; bgcolor:#032D5F; display:inline; padding: 1px 15px; font-family:Verdana, Arial, Helvetica, sans-serif;
">
    <a href="../logOut.php" style="color:#FFFFFF; background-color:#032D5F; font-weight:bold; text-decoration:none" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F'">I Do Not Agree</a>    </li>
    <li style="width:130px; bgcolor:#032D5F; display:inline; padding: 1px 15px">
    <a href="techTerms_p2.php" style="color:#FFFFFF; background-color:#032D5F; font-weight:bold; text-decoration:none" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F'">I Agree</a>
    </li>
  </ul>
  </div>
  <!--<div class="agreeButtons" style="width:130px"; bgcolor="#032D5F" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F';"><span style="color: #FFFFFF; font-weight: bold">I Agree</span></div> -->

</div>
<h1><!--- End Content --->
</h1>

<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
