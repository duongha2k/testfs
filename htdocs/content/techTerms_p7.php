<?php $page = login; ?>
<?php $option = tech; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<p>
  <!-- Add Content Here -->
  
  <br />
  <br />
</p>
<div class="techTerms" style="width:760px">
  <h3 align="center"><b>ELECTRONIC INDEPENDENT CONTRACTOR AGREEMENT</b></h3>
  <br />
  <span style="font-weight: bold"><br />
  </span>
  <p><span style="font-weight: bold">22. FORUM SELECTION.</span>  Because (a) Field Solutions is a Minnesota company with its principal place of business in Hennepin County, Minnesota; (b) the Company&rsquo;s significant contracts are governed by Minnesota law; and (c)&nbsp;it is mutually agreed that it is in the best interests of Field Solutions Clients, vendors, contactors, and employees that a uniform body of law consistently interpreted be applied to the relationships that Field Solutions has with such persons and entities, this Agreement is deemed entered into in the State of Minnesota between  Field Solutions and you.  The State of Minnesota, Hennepin County District Court, will have exclusive jurisdiction and venue over any disputes between Field Solutions and you for the purposes of any action arising out of or related to the their respective obligations hereunder, including specifically (but without limiting the generality of the foregoing), actions for temporary equitable relief and permanent equitable relief.  You hereby (a) waive any objection that you might have now or hereafter to the foregoing jurisdiction and venue of any such litigation, action or proceeding, (b) irrevocably submits to the exclusive jurisdiction of any such court set forth above in any such litigation, action or proceeding, and (c) waives any claim or defense of inconvenient forum.  Each party hereby consents to service of process by registered mail, return receipt requested, at such party&rsquo;s last known address (as modified by written notice of a party from time to time) and expressly waives the benefit of any contrary provision of law.</p>
  <p>IN WITNESS WHEREOF, the parties have executed this Agreement. </p>
  <p>&nbsp;</p>
  <div class="agreeButtons">
  <ul>
    <li style="width:130px; bgcolor:#032D5F; display:inline; padding: 1px 15px; font-family:Verdana, Arial, Helvetica, sans-serif;
">
    <a href="../logOut.php" style="color:#FFFFFF; background-color:#032D5F; font-weight:bold; text-decoration:none" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F'">I Do Not Agree</a>    </li>
    <li style="width:130px; bgcolor:#032D5F; display:inline; padding: 1px 15px">
    <a href="Privacy_Policy.php" style="color:#FFFFFF; background-color:#032D5F; font-weight:bold; text-decoration:none" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F'">I Agree</a>    </li>
  </ul>
  </div>
  <!--<div class="agreeButtons" style="width:130px"; bgcolor="#032D5F" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F';"><span style="color: #FFFFFF; font-weight: bold">I Agree</span></div> -->
</div>
<h1><!--- End Content --->
</h1>

<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
