<?php $page = login; ?>
<?php $option = tech; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<p>
  <!-- Add Content Here -->
  
  <br />
  <br />
</p>
<div class="techTerms" style="width:760px">
  <h3 align="center"><b>ELECTRONIC INDEPENDENT CONTRACTOR AGREEMENT</b></h3>
  <br />
  <span style="font-weight: bold"><br />
  </span>
  <p><span style="font-weight: bold">15. ASSIGNMENT.</span> Field Solutions may assign its rights under this Agreement to an affiliated or successor corporation or business entity.</p>
  <p><span style="font-weight: bold">16. NOTICES.</span>  All notices, requests, and other communications will be in writing and delivered electronically.  Field Solutions will send any such communications to the last known e-mail address that you provide, and you will send any such communications to the Field Solutions e-mail address that it provides.</p>
  <p><span style="font-weight: bold">17. 
    WAIVER, MODIFICATION OR AMENDMENT.</span>  No waiver, modification or amendment of any term, condition or provision of this Agreement will be valid or of any effect unless made in writing, signed by the party to be bound or its duly authorized representative and specifying with particularity the nature and extent of such waiver, modification or amendment.  Any waiver by any part of any default of the other will not affect or impair any right arising from any subsequent default.  Nothing in this Agreement limits the rights and remedies of the parties under and pursuant to this Agreement, except as set forth above.</p>
  <p><span style="font-weight: bold">18. ENTIRE AGREEMENT.</span>  This Agreement contains the entire agreement between Field Solutions and you and supersedes and cancels any and all other agreements, whether oral or in writing, between Field Solutions and you with respect to the matters referred to in this Agreement.</p>
  <p><span style="font-weight: bold">19. NTERPRETATION.</span>  The provisions of this Agreement will be applied and interpreted in a manner consistent with each other so as to carry out the purposes and intent of the parties to this Agreement, which is to create an independent contractor relationship. </p>
  <p><span style="font-weight: bold">20. BLUE PENCIL DOCTRINE.</span>  In the event that any provision of this Agreement is unenforceable under applicable law, the validity or enforceability of the remaining provisions will not be affected.  To the extent any provision of this Agreement is judicially determined to be unenforceable, a court of competent jurisdiction may reform any such provision to make it enforceable.  The provisions of this Agreement will, where possible, be interpreted so as to sustain their legality and enforceability.</p>
  <p><span style="font-weight: bold">21. GOVERNING LAW.</span>  The validity, enforceability, construction, and interpretation of this Agreement will be governed by the laws of the State of Minnesota, without regard to any conflict-of-law or choice-of-law rules.  You irrevocably waive your right, if any, to have the laws other than the State of Minnesota apply to this Agreement. </p>
  <div class="agreeButtons">
  <ul>
    <li style="width:130px; bgcolor:#032D5F; display:inline; padding: 1px 15px; font-family:Verdana, Arial, Helvetica, sans-serif;
">
    <a href="../logOut.php" style="color:#FFFFFF; background-color:#032D5F; font-weight:bold; text-decoration:none" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F'">I Do Not Agree</a>    </li>
    <li style="width:130px; bgcolor:#032D5F; display:inline; padding: 1px 15px">
    <a href="techTerms_p7.php" style="color:#FFFFFF; background-color:#032D5F; font-weight:bold; text-decoration:none" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F'">I Agree</a>    </li>
  </ul>
  </div>
  <!--<div class="agreeButtons" style="width:130px"; bgcolor="#032D5F" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F';"><span style="color: #FFFFFF; font-weight: bold">I Agree</span></div> -->
</div>
<h1><!--- End Content --->
</h1>

<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
