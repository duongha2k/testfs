<?php require ("../headerSimple.php"); ?>
<script type="text/javascript">
	function agreeICA() {
		try {
			opener.agreeICA();
			window.close();
		}
		catch (e) {
		}
	}
	function notAgreeICA() {
		try {
			opener.notAgreeICA();
			window.close();
		}
		catch (e) {
		}
	}
</script>

<p>
  <!-- Add Content Here -->
  
  <br />
  <br />
</p>
<div class="techTerms" style="width:760px">
  <h3 align="center"><b>ELECTRONIC INDEPENDENT CONTRACTOR AGREEMENT</b></h3>
  <br />
  <br />
  <p><b>Introduction</b></p>
  <p>This is an AGREEMENT between you and Field Solutions Inc. (&ldquo;Field Solutions&rdquo;) (&ldquo;Agreement&rdquo;) that defines the terms and conditions for Field Solutions&rsquo; to engage you to provide services to our Clients as described in this Agreement.   As used in this Agreement, the words &ldquo;You,&rdquo;  "you" and "your" mean the Independent Contractor (&ldquo;Technician&rdquo;) entering into this Agreement as a condition of becoming eligible to receive Work Order offers from Field Solutions.<br>
    <br>
    By clicking the &ldquo;I ACCEPT&rdquo; button below, you agree that:
  <ol style="margin-left:50px">
    <li>You are creating a contract with Field Solutions that is legally binding, and that you expect to be enforced.<br />
        <br />
    </li>
    <li>Your acceptance of this agreement by electronic means will not affect its enforcement, and that you waive the use of traditional paper documents signed in writing.</li>
  </ol>
  <p>This Agreement will not be effective unless and until you accept its terms by clicking the &ldquo;I ACCEPT&rdquo; button at the end of this document.  You may obtain a copy of the electronic record of this Agreement, or a copy in a medium that is retrievable, by a written request sent via email transmission to Field Solutions e-mailbox address which is shown on this site.</p>
  <hr>
  <p><span style="font-weight: bold">1. INFORMATION ABOUT FIELD SOLUTIONS.</span>  Field Solutions provides on-line software system and data base of information about you and other Independent Contractor Technicians, for use by Field Solutions&rsquo; Clients. These Clients are engaged in the &ldquo;Business&rdquo; of installing, troubleshooting, servicing, and repairing the personal computers, workstations, printers, point-of-sale/POS terminals, networks and associated devices and software at their locations, and at the locations of the customers of these Clients (both the Client and a customer of a Client are defined in this Agreement as a &ldquo;Client&rdquo;) (and all and each of these offerings are defined as the &ldquo;Services&rdquo;).</p>
  <p><br />
    <span style="font-weight: bold">2. INFORMATION ABOUT YOU.</span>  To be eligible to receive &ldquo;Work Order&rdquo; offers from Field Solutions, you must certify that you are the person entering into  this Agreement and that you are a qualified technician who possesses the necessary skill, knowledge, training, ability, and experience to perform some or all of the Services for Field Solutions&rsquo; Clients.</p>
  <p><span style="font-weight: bold">3. ACCEPTANCE OF WORK ORDERS. </span>You will be eligible to receive offers from time-to-time from Field Solutions&rsquo; online system to provide Services to Clients as set forth in separate Work Orders for each job only after you provide the information requested by Field Solutions and accept the terms and conditions of this Agreement.</p>
  <p>A. Each Work Order will describe the Services to be provided, the fee for the Services, any special requirements or conditions that the Client imposes such as, without limitation, satisfactory results on a background investigation and drug and/or alcohol testing, and any other terms and conditions governing the Services. Field Solutions will deliver each such Work Order to the e-mail address that you provide.  You must confirm your acceptance of the Work Order before performing any Services.  At the request of Field Solutions, you also must complete the online W-9 tax form as a condition of an offer of a Work Order.</p>
  <p>B. You agree to provide the Services in a timely manner, competently, diligently and professionally, and to conduct yourself in accordance with Work Order descriptions and requirements for job conduct that a Client establishes for visitors to its premises, including, but not limited to issues of on-time arrival, job completion as described in the work order, appearance, workplace security, workplace conduct, and Client confidentiality.</p>
  <p>C. You are not obligated to accept a fixed, or minimum, number of Work Orders.  You maintain sole discretion to reject an offer of a Work Order.  However, once you accept the Client&rsquo;s offer of a Work Order, you are responsible for rendering the Services; or you may secure a substitute contractor, or assign your own agents or employees to render the Services, provided that (i) any such person other than you must be determined by the Client to be technically capable and meet other Client requirements in order to achieve the results sought through the Work Order, (ii)  you agree to continue to be bound by the terms of the Work Order for assuring the quality of the completion of the Work Order, and (iii) such other person rendering the Services will do so diligently and professionally. </p>
  <p>D. Field Solutions has no obligation to offer you a minimum number of Work Orders.</p>
  
  <p style="font-weight: bold">4. YOUR DUTIES AS AN INDEPENDENT CONTRACTOR</p>
  <p>A. You will render the Services as an independent contractor.  You will pay any and all payroll, self-employment, or other taxes of any nature imposed upon the compensation paid by Field Solutions to You for the rendering of the Services. </p>
  <p>B. You will obtain workers compensation insurance if required by law, and keep such records, pay such other taxes or governmental fees, comply with applicable law and maintain such licenses as are required to perform the Services.  You will be solely responsible for obtaining and either self-insuring or purchasing liability insurance in such amounts and coverage as you select.  You understand that issues of liability that arise as a result of your negligence in the execution of a Work Order will be your responsibility.  Field Solutions will not provide any contributions to unemployment insurance or workers compensation funds.  Certain Clients may require specific insurance coverages.  You will carry such insurance as may be required by an applicable Work Order.  Field Solutions recommends that You carry the following minimum insurance coverage: (i) workers compensation insurance as prescribed by the law of the state in which you  render Services; (ii) employer liability insurance with limits of at least $1,000,000 per occurrence; (iii) comprehensive automobile liability insurance with a combined single limit per occurrence of at least $1,000,000 for bodily injury and property damage; (iv) comprehensive general liability insurance, including, broad form/property damage, bodily injury, products and completed operations, with a combined single limit per occurrence of at least $1,000,000; and (v) professional liability insurance with a minimum limit of $1,000,000 per occurrence.  You will provide proof of required insurance coverage upon request by Field Solutions. </p>
  <p>C. Field Solutions will not, and declines to, specify or control the means or methods of how you will render the Services.  Field Solutions is relying on your skills, knowledge, training, ability, and experience in the Services to enable you to determine and control how to render the Services in a Work Order in order to satisfy the objective of the Client and without Field Solutions supervision. </p>
  <p>D. Except for the purpose of explaining how to use our on-line system and otherwise  responding to any questions that you have concerning  a Work Order, Field Solutions has not provided, and will not provide, you with training or individual instruction with respect to how you render the Services to a Client&rsquo;s satisfaction.  Except for reading manuals associated with a Work Order, you will obtain the necessary technical proficiency to render the Services at your own expense, or at the expense of another person or entity, prior to becoming a contractor to Field Solutions. </p>
  <p>E. Except for the purpose of explaining how to use our on-line system and otherwise  responding to any questions that you have concerning  a Work Order, Field Solutions has not provided, and will not provide, you with training or individual instruction with respect to how you render the Services to a Client&rsquo;s satisfaction.  Except for reading manuals associated with a Work Order, you will obtain the necessary technical proficiency to render the Services at your own expense, or at the expense of another person or entity, prior to becoming a contractor to Field Solutions.  </p>
  <p>F. You will not be entitled to any rights or benefits provided to employees of either Field Solutions or our Clients including, without limitation, health insurance benefits, vacation or other time off, and other employee benefits.  You are responsible for any expenses incurred to perform the Services. </p>
  <p>G. You have no obligation to keep reports or logs of any type to justify the time spent rendering Services on a Work Order, except that you will record your hours on Field Solutions&rsquo; web site in order to verify the fee due and owing to you on a given Work Order and to permit Field Solutions to estimate the cost of similar Services in the future. </p>
  <p>H. The nature of the Services is also such that you will render them on the location of the Client, which may be either a residence or commercial establishment.</p>
  
<p><span style="font-weight: bold">5. LIMITATION OF AUTHORITY.</span>  You will not have any authority to:  (a) bind or obligate Field Solutions or subject Field Solutions to any liability except as expressly provided in this Agreement, or (b) do any other act on behalf of Field Solutions that is not expressly authorized in this Agreement.</p>
  <p><span style="font-weight: bold">6. WARRANTY. </span>You warrant that the Services that you will render will be performed in a timely, professional, diligent and competent manner.  Any defects in your performance of the Services  or other Client dissatisfaction with you will be remedied by you in a manner that is satisfactory to the Client.  You also warrant that you will comply with all applicable laws, rules and regulations in the performance of the Services.</p>
  <p><span style="font-weight: bold">7. COMPENSATION. </span>All compensation paid to you is to be considered compensation for the proper handling by you of all phases of rendering the Services.  Field Solutions will pay you the fee specified in a given Work Order, and authorized by the Client,  for your rendering of Services, and on the terms set forth in the Work Order.  You will receive no other compensation other than as set forth in the Work Order.  You will be paid within 30 days after satisfactory completion of the Services.   Field Solutions will report payments to you for tax purposes on a Form 1099. </p>
  <p><span style="font-weight: bold">8.
    CLIENT DISPUTES.</span>  If any Client is involved in a dispute with you arising from or related to your performance of the Services, Field Solutions may, in its sole discretion, assist you and such Client in resolving the dispute; provided, however, that Field Solutions will be solely responsible for all Client fee, billing, and payment matters.  In the event of any dispute between you and a Client related to the Services that you provide, you will notify Field Solutions in writing of such dispute within one business day of becoming aware of such dispute.  You will use your best efforts to resolve any such disputes within two business days after receiving such notice from a Client.  Upon notice of any dispute, Field Solutions reserves the right to offer the applicable Work Order to another Field Solutions contractor with no payment obligation due or owing to you.</p>
  <p><span style="font-weight: bold">9. INDEMNIFICATION.</span>  You will indemnify, defend and hold Field Solutions harmless from any and all liability, obligation, loss, damage, injury, penalty, action, judgment, suit, claim, cost, expense or disbursement of any kind or nature that may be imposed on, incurred by or served against Field Solutions by any Client or other person or entity arising out of, or in connection with the Services, or any alleged act or neglect of you.  You will provide Field Solutions with immediate notification of any claim received that names, or in any way involves, Field Solutions.  Nothing in this Section affects your status as an independent contractor. </p>
  <p><span style="font-weight: bold">10. 
    TERMINATION.</span>  This Agreement and your Services may be terminated by either party without advance notice.</p>
    
  <p><span style="font-weight: bold">11. CONFIDENTIALITY.</span>  You will at all times maintain in strict confidence all personal and proprietary information of Field Solutions&rsquo; Clients that you receive in the course of providing Services, including all Work Orders, Client addresses, and information on Client premises to which you have access or obtain control.  Such information will remain the exclusive property of Field Solutions and its Clients.  You will not retain the originals or any copies of such information without Field Solutions&rsquo; prior written permission. </p>
  <p><span style="font-weight: bold">12. COVENANTS.</span>  Field Solutions makes considerable efforts and incurs substantial expense to market to prospective Clients requiring information about the Business.  Accordingly, while engaged to render Services on any Work Order, you will not knowingly, directly or indirectly, solicit any Client or prospective Client who is a Client or prospective Client of Field Solutions or any of Field Solutions&rsquo; affiliates for the purpose of providing services that are competitive with the Business of Field Solutions. </p>
  <p><span style="font-weight: bold">13. 
    REMEDIES.</span>  Any breach of any of the covenants or agreements set forth in Sections 11 and 12  of this Agreement will cause Field Solutions irreparable harm for which there is no adequate remedy at law, and, without limiting whatever other rights and remedies Field Solutions may have under this paragraph, you consent to the issuance of an injunction in favor of Field Solutions enjoining the breach of any of the preceding covenants or agreements by any court of competent jurisdiction. </p>
  <p><span style="font-weight: bold">14. 
    SURVIVING RIGHTS.</span>  Notwithstanding the termination of this Agreement, the parties will be required to carry out any provisions of this Agreement that contemplate performance subsequent to such termination; and such termination will not affect any liability or other obligation that will have accrued prior to such termination including, but not limited to, any liability for loss or damage on account of a prior default.</p>

  <p><span style="font-weight: bold">15. ASSIGNMENT.</span> Field Solutions may assign its rights under this Agreement to an affiliated or successor corporation or business entity.</p>
  <p><span style="font-weight: bold">16. NOTICES.</span>  All notices, requests, and other communications will be in writing and delivered electronically.  Field Solutions will send any such communications to the last known e-mail address that you provide, and you will send any such communications to the Field Solutions e-mail address that it provides.</p>
  <p><span style="font-weight: bold">17. 
    WAIVER, MODIFICATION OR AMENDMENT.</span>  No waiver, modification or amendment of any term, condition or provision of this Agreement will be valid or of any effect unless made in writing, signed by the party to be bound or its duly authorized representative and specifying with particularity the nature and extent of such waiver, modification or amendment.  Any waiver by any part of any default of the other will not affect or impair any right arising from any subsequent default.  Nothing in this Agreement limits the rights and remedies of the parties under and pursuant to this Agreement, except as set forth above.</p>
  <p><span style="font-weight: bold">18. ENTIRE AGREEMENT.</span>  This Agreement contains the entire agreement between Field Solutions and you and supersedes and cancels any and all other agreements, whether oral or in writing, between Field Solutions and you with respect to the matters referred to in this Agreement.</p>
  <p><span style="font-weight: bold">19. INTERPRETATION.</span>  The provisions of this Agreement will be applied and interpreted in a manner consistent with each other so as to carry out the purposes and intent of the parties to this Agreement, which is to create an independent contractor relationship. </p>
  <p><span style="font-weight: bold">20. BLUE PENCIL DOCTRINE.</span>  In the event that any provision of this Agreement is unenforceable under applicable law, the validity or enforceability of the remaining provisions will not be affected.  To the extent any provision of this Agreement is judicially determined to be unenforceable, a court of competent jurisdiction may reform any such provision to make it enforceable.  The provisions of this Agreement will, where possible, be interpreted so as to sustain their legality and enforceability.</p>
  <p><span style="font-weight: bold">21. GOVERNING LAW.</span>  The validity, enforceability, construction, and interpretation of this Agreement will be governed by the laws of the State of Minnesota, without regard to any conflict-of-law or choice-of-law rules.  You irrevocably waive your right, if any, to have the laws other than the State of Minnesota apply to this Agreement. </p>
    
  <p><span style="font-weight: bold">22. FORUM SELECTION.</span>  Because (a) Field Solutions is a Minnesota company with its principal place of business in Hennepin County, Minnesota; (b) the Company&rsquo;s significant contracts are governed by Minnesota law; and c) it is mutually agreed that it is in the best interests of Field Solutions Clients, vendors, contactors, and employees that a uniform body of law consistently interpreted be applied to the relationships that Field Solutions has with such persons and entities, this Agreement is deemed entered into in the State of Minnesota between  Field Solutions and you.  The State of Minnesota, Hennepin County District Court, will have exclusive jurisdiction and venue over any disputes between Field Solutions and you for the purposes of any action arising out of or related to the their respective obligations hereunder, including specifically (but without limiting the generality of the foregoing), actions for temporary equitable relief and permanent equitable relief.  You hereby (a) waive any objection that you might have now or hereafter to the foregoing jurisdiction and venue of any such litigation, action or proceeding, (b) irrevocably submits to the exclusive jurisdiction of any such court set forth above in any such litigation, action or proceeding, and (c) waives any claim or defense of inconvenient forum.  Each party hereby consents to service of process by registered mail, return receipt requested, at such party&rsquo;s last known address (as modified by written notice of a party from time to time) and expressly waives the benefit of any contrary provision of law.</p>
  
       <br/><br/>
	IN WITNESS WHEREOF, the parties have executed this Agreement.<br/><br/>
<div class="agreeButtons">
  <ul>
    <li style="width:130px; bgcolor:#032D5F; display:inline; padding: 1px 15px; font-family:Verdana, Arial, Helvetica, sans-serif;
">
    <a href="javascript:notAgreeICA()" style="color:#FFFFFF; background-color:#032D5F; font-weight:bold; text-decoration:none" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F'">I Do Not Accept</a>    </li>
    <li style="width:130px; bgcolor:#032D5F; display:inline; padding: 1px 15px">
    <a href="javascript:agreeICA()" style="color:#FFFFFF; background-color:#032D5F; font-weight:bold; text-decoration:none" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F'">I Accept</a>    </li>
  </ul>
  </div>

</div>
<h1><!--- End Content --->
</h1>
<span id="bottomPage"></span>
</body>
</html>
		
