<?php $page = 'aboutus'; ?>
<?php $option = 'management'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>

<div id="container">


<div id="header">

    <div id="wrapper">
        <div id="content">
       
<div id="aboutUsWrapper"> 
<div id="aboutUsHdr">Our Team</div>
    
<!-- Add Content Here -->
<br /><br />



<div id="aboutUsSubHdr">
Mac Lewis - CEO
</div>
<p>
Mac has&nbsp;over&nbsp;35 years of executive management experience in technology based products and services companies. He was President and CEO of Computer Network Technology (CNT) for ten years during which time it went from a pre-revenue start-up to a NASDAQ-traded public company, with products and services for large enterprise data networking clients.&nbsp;Prior to CNT, he was founder and CEO of The Systems Center - a mainframe oriented software product company, and also had 14 years with IBM in management, sales, systems engineering and product management.&nbsp;Prior to co-founding FieldSolutions, &nbsp;Mac was founder and a partner of Sherpa Partners, a technology investment firm. He serves&nbsp;as a Board Member of several early stage private companies&nbsp;as well as several non-profit organizations.&nbsp;He is a graduate of Princeton University.</p>

<div id="aboutUsSubHdr">
Mike Kraemer - EVP Business Development
</div>
<p>
Mike joins Field Solutions through the merger with TechnicanBureau, a company he founded. For the prior ten years Mike ran successful  businesses providing field technician services. With client relationships throughout the technology arena, Mike is a recognized industry leader in technician contractor sourcing and management. Mike&rsquo;s background includes software sales and programming with Datatec Systems, Compuware, Roll-outs, and Fingerhut.</p>

<div id="aboutUsSubHdr">
Marty Reader - EVP Sales and Marketing
</div>

<p>Marty joins Field Solutions with over 25 years of experience in business services leadership roles in field services companies. Marty was&nbsp;the executive responsible for Workforce Productivity Analytics and Marketing&nbsp;at Reed Group, a leading occupational health services provider. Previously Marty led the analytics consulting practice for the Cedar Group, overseeing Cedar Workforce Technologies Research and managing the service analytics consulting practice technology providers including Oracle, PeopleSoft and ADP.&nbsp;Marty has held senior management positions with global&nbsp;leaders including ADP, Marriott, Ecolab,&nbsp;G&amp;K Services and IBM. Marty earned his MBA at Cornell University, and his BA from Colby College. </p>

<div id="aboutUsSubHdr">Joseph Rubin - TCW Sourcing Division President</div>

<p>Joseph has over 25 years experience in high tech and field service industry with over 20 years general management and senior executive experience. Joseph worked for Norstan, Inc. a $500M US/Canada Voice, Data and Video Systems Integrator and held the positions of EVP Communication Systems/Corporate Officer and President Norstan Canada, Inc. He was also, CEO and co-founder of Sagess, Inc. a provider of Voice &amp; Data Infrastructure Outsourcing Solutions; President &amp; CEO One Call Telecom, Inc., a Competitive Local Exchange Carrier (CLEC).</p>

<div id="aboutUsSubHdr">
Jeff Sussna - Chief Technology Officer
</div>
<p>
Jeff has over 20 years of experience building advanced software systems and applications. He has led the development and deployment of a range of high-performance online systems, including distributed operating systems, content management systems, and live-coverage web sites for major sporting events. He has held senior technical positions with technology leaders including Apple and Oracle, and has expertise in all aspects of system development, quality assurance, and operations. Most recently Jeff was a Chief Architect with Internet Broadcasting, the leading web publishing systems provider for the broadcasting industry. Jeff is a graduate of Hampshire College.</p>

<div id="aboutUsSubHdr">
John Sinning - Director of Finance and Administration
</div>
<p>
John comes to Field Solutions with over 20 years of experience in technology services operations and finance.&nbsp;Most recently he led Operations for a field service deployment company. Previously&nbsp;he was Director of Global Technology Planning and Governance for Thomson Corporation. John&rsquo;s prior experience includes finance and planning roles with USBank Corporation. John earned his MBA from the University of St. Thomas, and his BS from the Carlson School, University of Minnesota.</p>

<br /><br />
</div>

</div></div>

<!--- End Content --->
<?php require ("../footer.php"); ?>
		
