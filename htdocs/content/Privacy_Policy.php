<?php $page = login; ?>
<?php $option = tech; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<p>
  <!-- Add Content Here -->
  
  <br />
  <br />
</p>
<div class="techTerms" style="width:760px">
  <h3 align="center"><b>Privacy Statement</b></h3>
  <br />
  <span style="font-weight: bold"><br />
  </span>
  <p><span style="font-weight: bold">Overview</span></p>
  <p>Thank you for visiting www.FieldSolutions.com. Field Solutions, LLC (&ldquo;Field Solutions&rdquo;), and our affiliates, are committed to respecting the privacy of our users.  We strive to provide a safe, secure user experience.  This Privacy Statement sets forth the online data collection and usage policies and practices that apply to www.FieldSolutions.com and other related Field Solutions Web sites (collectively referred to herein as the &quot;Field Solutions Sites&quot;) and does not apply to information we collect in any other fashion.  The information we gather on the Field Solutions Sites may be shared with our affiliates to provide Field Solutions products and services to our customers.</p>
  <p>The Field Solutions Sites contain links to other Web sites.  We do not own, control or operate the links or their Web sites, and we are not responsible for the privacy policies, practices or contents of such Web sites.  We encourage you to review the privacy policies of those other Web sites so you can understand how they collect, use and share your information.  The inclusion of any link does not imply endorsement of the Web site(s).</p>
  <p><span style="font-weight: bold">Collection and Use of Information by Field Solutions</span></p>
  <p>In some areas of the Field Solutions Sites, we request or may request that you provide personal information, including your name, address, e-mail address, telephone number, social security number, contact information, and certain other information from which your identity is discernible.  In other areas, we collect or may collect demographic information that is not unique to you such as your ZIP code, age, preferences, gender, and interests.  Sometimes we collect or may collect a combination of the two types of information.  Examples of areas on the Field Solutions Sites where we collect personal or combined personal and demographic data are pages where you can create a Field Solutions profile, sign up to use a service, or post a resume or profile. </p>
  <p>Moreover, there is information about your computer hardware and software that is or may be collected by us.  This information may include, among other things, the IP address of the computer you are using, the browser and operating system you are using, domain names, access times and referring Web site addresses, but is not linked to your personal information. <br />
    </p>
  <p>We may sometimes afford you the opportunity to provide descriptive, cultural, behavioral, and preferential information about yourself, but it is solely up to you whether you furnish such information.  If you do provide such information, you are thereby consenting to the use of that information in accordance with the policies and practices described in this Privacy Statement. <br />
    </p>
  <p>If you choose to use our referral service to tell a friend about the Field Solutions Sites or refer a work order to a friend, we will ask you for your friend's name and e-mail address.  We will automatically send your friend a one-time e-mail inviting him or her to visit the Field Solutions Sites or view the work order.  We do not store this information.<br />
    </p>
  <p style="font-weight: bold">Our Use of Your Information<br />
    </p>
  <p>We use the information we gather on the Field Solutions Sites, whether personal, demographic, collective or technical, for the purpose of operating and improving the Field Solutions Sites, fostering a positive user experience, and delivering the products and services that we offer. <br />
    </p>
  <p>If you have provided consent for us to do so, we may also use the information we gather to inform you of other products or services available from us or our affiliated companies. <br />
    </p>
  <p>We may use your contact information in order to send you e-mail or other communications regarding updates at the Field Solutions Sites, and to contact you about your opinion of our current products and services or potential new products and services that may be offered.  We may also provide additional communications, promotions and information on new Field Solutions opportunities and additional work orders, which may be of interest to you.  The nature and frequency of these messages will vary depending upon the information we have about you. <br />
    </p>
  <p>We have areas on the Field Solutions Sites where you can submit feedback.  Any feedback you submit in these areas becomes our property, and we can use such feedback (such as success stories) for marketing purposes or to contact you for further information. <br />
    </p>
  <p style="font-weight: bold">Choices Regarding the Disclosure of Personal Information to Others <br />
    </p>
  <p>We do not disclose your personal information to third parties, or your combined personal and demographic information or information about your use of the Field Solutions Sites (such as the areas you visit or the services you access), except as set forth below.</p>
  <ol style="margin-left:50px">
    <li>We disclose information to companies and individuals we employ to perform functions on our behalf.  Examples include hosting our Web servers, analyzing data, providing marketing assistance, and providing customer service.  These companies and individuals will have access to your personal information as necessary to perform their functions, but they may not share that information with any other third party.<br />
        <br />
    </li>
    <li>We disclose information if legally required to do so, if requested to do so by a governmental entity, or if we believe in good faith that such action is necessary to: (a) conform to legal requirements or comply with legal process; (b) protect our rights or property or our affiliated companies; (c) prevent a crime or protect national security; or (d) protect the personal safety of users or the public.</li>
    <br />
    <li>We will disclose and transfer information to a third party who acquires all or a substantial portion of our business, whether such acquisition is by way of merger, consolidation or purchase of all or a substantial portion of our assets.  In addition, in the event we become the subject of a bankruptcy proceeding, whether voluntary or involuntary, we or our trustee in bankruptcy may sell, license or otherwise dispose of such information in a transaction approved by the bankruptcy court.  You will be notified of sale of all or a substantial portion of our business to a third party via e-mail or through a prominent notice posted on the Field Solutions Sites.</li>
  </ol>
  <p style="font-weight: bold">Other Uses of Information</p>
  <p>We may aggregate on an anonymous basis data regarding job qualifications, schooling, age, experience level or other information relevant to work orders.  Such data may be made available to our affiliates in providing our products and services and various contracting parties.</p>
  <p>If you register on the Field Solutions Sites to become eligible to accept a work order(s), you may be asked to provide information on your gender, race or other protected status where permitted by applicable law.  This information may be gathered by contracting parties, as required by law, for reporting and record-keeping requirements.  You should understand that if provided, this information will be used by such contracting parties only in accordance with applicable law and will not be used in making any negative employment decisions.  All information provided will be kept separate from your expression of interest in any work order.  Providing this information is strictly voluntary and you will not be subject to any adverse action or treatment if you choose not to provide this information.</p>
  <p style="font-weight: bold">Personal Information and Profiles<br />
    </p>
  <p>Since the Field Solutions Sites offer opportunities for potentially accepting work orders, once you upload your contact information and qualifications into the database, all contracting parties with access to our searchable resume database (or copies thereof) will have access to your contact information and qualifications.</p>
  <p>We attempt to limit access to our searchable database (or copies thereof) only to paying contracting parties, as well as law enforcement and national security agencies, but cannot guarantee that other parties will not gain access to this database.  We are not responsible for the use made of resumes by third parties who access such resumes while they are in our searchable database.  However, the portions of your searchable information that are automatically made public in your Field Solutions profile are accessible to anyone who uses the Field Solutions service.  You may turn off access to your information on Field Solutions or at anytime, but contracting parties that have paid for access to the Field Solutions database or to obtain a copy of that database or have paid for access to the Field Solutions services, as well as parties who have otherwise gained access, may have retained a copy of your information or Field Solutions profile in their own files or databases.  We are not responsible for the retention, use or privacy of information or profiles in these instances.</p>
  <p style="font-weight: bold">Posting to Public Areas of Field Solutions</p>
  <p>Please remember that if you post any of your personal information in public areas of the Field Solutions Sites, or in the Field Solutions searchable resume database, such information may be collected and used by others over whom we have no control.  We are not responsible for the use made by third parties of information you post or otherwise make available in public areas of the Field Solutions Sites.</p>
  <p style="font-weight: bold">Use of Cookies</p>
  <p>We use &quot;cookies&quot; to help personalize and maximize your online experience and time online.  A cookie is a text file that is placed on your hard drive by a Web page server.  Cookies are not used to run programs or deliver viruses to your computer.  Cookies are uniquely assigned to your computer, and can only be read by a Web server in the domain that issued the cookie to you.</p>
  <p>One of the primary purposes of cookies is to provide a convenience feature to save you time.  The purpose of a cookie is to tell the Web server that you have returned to a specific page.  For example, if you personalize Field Solutions pages, or register for services, a cookie helps us to recall your specific information (such as user name, password and preferences).  Because of our use of cookies, we can deliver faster and more accurate results and a more personalized site experience.  When you return to a Field Solutions Site, the information you previously provided can be retrieved, so you can easily use the features that you customized.  We also use cookies to track click streams and for load balancing.</p>
  <p>You may have the ability to accept or decline cookies.  Most Web browsers automatically accept cookies, but you can usually modify your browser setting to decline all cookies if you prefer.  Alternatively, you may be able to modify your browser setting to notify you each time a cookie is tendered and permit you to accept or decline cookies on an individual basis.  If you choose to decline cookies, however, that may hinder performance and negatively impact your experience on the Field Solutions Sites.</p>
  <p style="font-weight: bold">Use of Web Beacons</p>
  <p>The Field Solutions Sites may contain electronic images known as Web beacons (sometimes called single-pixel gifs) that allow us to count users who have visited those pages and to deliver co-branded services.  Web beacons are not used to access your personally identifiable information; they are a technique we use to compile aggregated statistics about our Web site usage.  Web beacons collect only a limited set of information including a cookie number, time and date of a page view, and a description of the page on which the Web beacon resides.</p>
  <p>You may not decline Web beacons, however, they can be rendered ineffective by declining all cookies or modifying your browser setting to notify you each time a cookie is tendered and permit you to accept or decline cookies on an individual basis.<br />
    </p>
  <p style="font-weight: bold">Access to and Modification of Your Information<br />
    </p>
  <p>You may review, correct, update or change your Field Solutions account and/or Field Solutions profile at any time.  To change your Field Solutions profile, simply log into your account, click edit inside your profile or choose a link under Manage Your Profile. <br />
    </p>
  <p>We retain indefinitely all the information we gather about you in an effort to make your repeat use of the Field Solutions Sites more efficient, practical, and relevant.  Of course, you can close your Field Solutions account at any time, and delete your Field Solutions profile, in which event your profile and personal information will not be able to be reviewed by others. Information in a closed account may remain in the system, and be viewable by FieldSolution admistrative users, indefinitely. When or if account information is deleted from the system, we will remove all our copies of your resume from the Field Solutions Sites, your Field Solutions profile and Field Solutions account information, except for an archival copy which is not accessible on the Internet.<br />
    </p>
  <p style="font-weight: bold">Security of the Personal Information<br />
    </p>
  <p>We have implemented commercially reasonable technical and organizational measures designed to secure your personal information from accidental loss and from unauthorized access, use, alteration or disclosure.  However, we cannot guarantee that unauthorized third parties will never be able to defeat those measures or use your personal information for improper purposes.</p>
  <p style="font-weight: bold">Children</p>
  <p>The Field Solutions Sites are not intended for children under 13 years of age.  That is why, consistent with the <a href="http://www.ftc.gov/ogc/coppa1.htm" target="_blank">Children&rsquo;s Online Privacy Protection Act</a>, we will not knowingly collect personal information from children under 13.  For more information about protecting the privacy and safety of minors online, we recommend that you visit the <a href="http://www.ftc.gov/" target="_blank">Federal Trade Commission</a> and <a href="http://www.truste.org/" target="_blank">TRUSTe</a> Web sites. <br />
    </p>
  <p style="font-weight: bold">Safe Harbor<br />
    </p>
  <p>We participate in the Department of Commerce's Safe Harbor Framework arising out of the U.S. implementation of the European Union Data Protection Directive.  We have certified adherence to the Safe Harbor Privacy Principles for each of the Field Solutions Sites.  For more information about the Safe Harbor Privacy Principles and to view our certification, visit the<a href="http://www.export.gov/safeharbor/" target="_blank"> U.S. Department of Commerce's Safe Harbor Web site</a>.  If you would like to contact us directly about the Safe Harbor program, please send an e-mail to <a href="mailto:info@FieldSolutions.com">info@FieldSolutions.com</a>.<br />
    </p>
  <p style="font-weight: bold">Changes to Privacy Statement<br />
    </p>
  <p>We may change this Privacy Policy from time to time.  If we decide to materially change our Privacy Statement with respect to any of the Field Solutions Sites, we will post those changes through a prominent notice on each Field Solutions Site so that you will always know what information we gather, how we might use that information, and to whom we will disclose it.<br />
    </p>
  <p>If at any time, you have questions or concerns about this Privacy Statement or believe that we have not adhered to this Privacy Statement, please feel free to e-mail us at <a href="mailto:info@FieldSolutions.com">info@FieldSolutions.com</a> or call us and speak to one of our representatives.  We will use commercially reasonable efforts to promptly answer your question or resolve your problem. <br />
    </p>
  <p>Contact Information<br />
    </p>
  <p>Field Solutions, LLC is a Minnesota limited liability company.  All written inquiries may be directed to Field Solutions, LLC, P.O. Box 390108, Minneapolis, MN 55439. </p>
  <p>Thank you for using Field Solutions.<br />
    </p>
  <p>Update effective March 3rd, 2008.</p>
  <p>&nbsp;</p>
  <div class="agreeButtons">
  <ul>
    <li style="width:130px; bgcolor:#032D5F; display:inline; padding: 1px 15px; font-family:Verdana, Arial, Helvetica, sans-serif;
">
    <a href="../logOut.php" style="color:#FFFFFF; background-color:#032D5F; font-weight:bold; text-decoration:none" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F'">I Do Not Agree</a>    </li>
    <li style="width:130px; bgcolor:#032D5F; display:inline; padding: 1px 15px">
    <a href="Terms_Use.php" style="color:#FFFFFF; background-color:#032D5F; font-weight:bold; text-decoration:none" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F'">I Agree</a>    </li>
  </ul>
  </div>
  <!--<div class="agreeButtons" style="width:130px"; bgcolor="#032D5F" onMouseover="this.style.backgroundColor='#0000FF';" onMouseout="this.style.backgroundColor='#032D5F';"><span style="color: #FFFFFF; font-weight: bold">I Agree</span></div> -->
</div>
<h1><!--- End Content --->
</h1>

<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
