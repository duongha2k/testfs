<?php $page = 'aboutus'; ?>
<?php $option = 'missionvision'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>

<div id="container">


<div id="header">

    <div id="wrapper">
        <div id="content">
       
<div id="aboutUsWrapper"> 
<div id="aboutUsHdr">Our Mission and Our Values</div>
    
<!-- Add Content Here -->

<div id="aboutUsSubHdr">
Our Mission Statement
</div>

<p id="aboutUsText">
FieldSolutions<sup><font size="1">TM</font></sup> is a service company. Our mission is to deliver the highest quality field service results, create extraordinary value, and continually enhance our clients' and members' field service experience.
</p>
<p id="aboutUsText">
For our technician members, we provide the best opportunity for success in delivering quality field services.
</p>
<p id="aboutUsText">
For our clients, we commit to the highest satisfaction through consistently delivering the most efficient, effective, and highest quality field service solutions.
</p>
<p id="aboutUsText">
As a community, we are proud to be passionate, results-oriented, and respectful.
</p>

<div id="aboutUsSubHdr">
Our Values
</div>

<p id="aboutUsText">
Field Solutions<sup><font size="1">TM</font></sup> leads by successfully competing, while maintaining our values of respect for all members of our community.
</p>

<p id="aboutUsText">
We lead our industry by continually innovating to exceed expectations. We strive to always build the loyalty of our clients, members and co-workers.
</p>

<p id="aboutUsText">
We deliver on our commitments by clearly communicating expectations, and then passionately pursuing results. We strive for the highest quality in every action.
</p>

<p id="aboutUsText">
We create extraordinary value in our clients' and members' experience by working to always be the lowest total cost solution available.
</p>

<br /><br />

</div>

</div></div>

<!--- End Content --->
<?php require ("../footer.php"); ?>
		
