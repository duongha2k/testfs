<?php
	require_once("library/caspioAPI.php");
	require_once("library/smtpMail.php");
	ini_set("display_errors", 1);
	set_time_limit(400);

	$googleMapApiKey = "ABQIAAAAevr5CfqTkff7d66yhM00ehQiHFTdAgR5rsznpZDJ3kjax64MHRQ7l70yM4_gYxQyarVLdEWneUeMZw";
	function geocodeAddress($address) {
		global $googleMapApiKey;
		// create a new cURL resource
		$ch = curl_init();
		$address = urlencode($address);
		// set URL and other appropriate options
		$options = array(CURLOPT_URL => "http://maps.google.com/maps/geo?q=$address&key=$googleMapApiKey&output=csv",
						 CURLOPT_HEADER => false,
						 CURLOPT_RETURNTRANSFER => true
						);
		
		curl_setopt_array($ch, $options);
		
		// grab URL and pass it to the browser
		$response = curl_exec($ch);
		
		// close cURL resource, and free up system resources
		curl_close($ch);
		
		if ($response) {
			$info = explode(",", $response);
			if ($info[0] == "200") {
				$lat = $info[2];
				$long = $info[3];
				return array($lat, $long);
			}
		}		
		return false;
	}

	$table = "TR_Master_List";
	$diffLat = "ABS((SELECT TOP 1 USA_ZIPCodes.Latitude FROM USA_ZIPCodes WHERE USA_ZIPCodes.ZIPCode = $table.Zipcode) - Latitude)";
	$diffLong = "ABS((SELECT TOP 1 USA_ZIPCodes.Longitude FROM USA_ZIPCodes WHERE USA_ZIPCodes.ZIPCode = $table.Zipcode) - Longitude)";

	$ignoreList = array(13527, 14900, 16613, 16846, 16913, 17480, 17659, 18110, 18360, 18631, 19066, 19189, 20212, 20212, 20279,20350,20427,20621,20955,21392,21860,22264,22377,22461,22774,22781,22787,22995,23133,23213,23392,23411,23908,24453,25269, 25352, 25379, 25457, 25475, 25502, 25956, 26251, 26298, 27025, 27087, 27535, 27772, 27824, 28062, 28155, 28353, 28419,30895, 31300, 31303, 31304, 31355, 31405, 31533, 31771, 31843, 31849, 31859, 32217, 32341, 32343,32349, 32393, 32421, 32475, 32485, 32518, 32532, 32797, 33340, 33438, 33910, 33919, 33996, 34253, 34334, 34668, 34679, 35121, 36380);

	$techList = caspioSelectAdv($table, "(SELECT TOP 1 USA_ZIPCodes.Latitude FROM USA_ZIPCodes WHERE USA_ZIPCodes.ZIPCode = $table.Zipcode), (SELECT TOP 1 USA_ZIPCodes.Longitude FROM USA_ZIPCodes WHERE USA_ZIPCodes.ZIPCode = $table.Zipcode), TechID, Latitude, Longitude, State, Zipcode, Address1", "ISNULL(Zipcode,'') != '' AND LEN(Zipcode) >= 5 AND ((ISNULL(Latitude,'') = '' OR ISNULL(Longitude, '') = '') OR ($diffLat >= 1 OR $diffLong >= 1)) AND TechID NOT IN(" . implode(",", $ignoreList) . ")","", false, "`", "|", false);
		
	$unable = array();
	
	foreach ($techList as $wo) {
		if ($wo == "") continue;
		$info = explode("|", $wo);
		$newLat = trim($info[0], "`");
		$newLong = trim($info[1], "`");
		$techID = trim($info[2], "`");
		
//		if (array_key_exists($techID,$ignoreList)) continue;
		
		$zipcode = trim($info[6], "`");
		$address = trim($info[7], "`");		
		if ($newLat == "NULL" || $newLong == "NULL") {
			$location = "$address $zipcode";
			sleep(1);
			$coord = geocodeAddress($location);
			$newLat = $coord[0];
			$newLong = $coord[1];
/*			if ($newLat == "" || $newLong == "") {
				$location = "$zipcode";
				sleep(1);
				$coord = geocodeAddress($location);
				$newLat = $coord[0];
				$newLong = $coord[1];
			}*/
		}
		flush();
		ob_flush();
		if ($newLat == "" || $newLong == "") {
			echo "UNABLE: $newLat, $newLong WHERE TechID = '$techID' {$info[5]} $address $zipcode<br/>";
			$unable[] = $techID;
			smtpMail("Geocode Tech CRON", "no-replies@fieldsolutions.us", "tngo@fieldsolutions.com", "Unable to geocode TechID: $techID", "TechID: $techID", "TechID: $techID", "Geocode Tech CRON");
			continue;
		}
		echo "Update $newLat, $newLong WHERE TechID = '$techID' {$info[5]} $address $zipcode ... ";
		print_r(caspioUpdate("$table", "Latitude, Longitude", "'$newLat', '$newLong'", "TechID = '$techID'", false));
		echo "<br/>";
	}
	
	echo implode(", ", $unable);
?>