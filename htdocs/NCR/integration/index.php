<?php
ini_set('memory_limit', '1024M');
error_reporting(E_ERROR);
set_time_limit(0);
require_once('nusoap/lib/nusoap.php');
include_once("qb/default.php");
include_once("qb/mcf.php");
include_once("qb/csv.php");
include_once("config.php");
$timeStampFrom  = '';
$token = isset($_GET['token']) ? $_GET['token'] : '';
$successNums = 0;
$failNums = 0;
if(!file_exists('lastRunTime.txt')){
	file_put_contents('lastRunTime.txt',"");
	$timeStampFrom = "";
}else{
	$timeStampFrom = file_get_contents('lastRunTime.txt');
}
$client = new nusoap_client("https://api.fieldsolutions.com/api/client/wsdl/3",true);
$client->soap_defencoding = 'UTF-8';
$param = array(
'user'=> FS_USERNAME,
'pass' => FS_PASSWORD,
'fields'=>"",
'timeStampFrom'=> /*$timeStampFrom == "" ? "" :*/ date('m/d/Y') /*date('m/d/Y H:m',strtotime("-8 hour"))*/,
'timeStampTo' => '',
'timeStampType' => '',
'filters' => array('QBIDIsNotEmpty'=>true,'Parts_Presents'=>false,'Docs_Presents'=>false)
);
$result = $client->call('getWorkOrders',$param,'ns1');

if($result['success'] == 1 && count($result['data']) !=0){
	$updateArr = array();
	foreach($result['data'] as $rec){
		$data = split('-',$rec['QBID']);
		$fs = array();
		if(count($data)>1 && preg_match("/QBID-(.*)-\d{0,20}$/i",$rec['QBID'])){
			$table = $data[1];
			$tid = $data[2];
			$qb = new qb_mcf(QB_USERNAME,QB_PASSWORD,true,'',$token,QB_REALM);
			$qb->set_database_table("bgttzk9fp");
			try {
			$jobFieldsId = $qb->modelData($qb->do_query('{8.EX.'.$table.'}',0,0,'3.8.10.13.14.17.21.23'),true);
			} catch (Exception $e) {
				echo "{8.EX.'.$table.'} -- $e"; die();
			}
			$integration  = $jobFieldsId;
			if(empty($jobFieldsId[0][8])){continue;}
			if($jobFieldsId[0][10]!=0){continue;}
			//get realm and password
			$realm = $jobFieldsId[0][17];
			$password = $jobFieldsId[0][21];
			if($realm == ""){
					$realm = "www";
					$password = "payday123";
			}
			$auth_qb = new qb_mcf(QB_USERNAME,$password,true,'',$jobFieldsId[0]['App Token'],$realm); 
			$tableId = $jobFieldsId[0][8];
			if(isset($jobFieldsId[0]['QBID Validation']) && $jobFieldsId[0]['QBID Validation'] == 0){
				$fs = array(array('fid'=>6,'value'=> $jobFieldsId[0][8]),array('fid'=>8,'value'=>'Table Id:'.$jobFieldsId[0][8].' validation fail!'),array("fid"=>9,'value'=>date("m-d-Y h:i:s A")));
				echo '<font color="red">Integration Table Id:'.$jobFieldsId[0][8].' validation fail!</font><br/>';
				$qb->set_database_table("bgv59mqhn");
				$isHadLog = $qb->modelData($qb->do_query('{6.EX.'.$fs[0]['value'].'}',0,0,'3.8'),true);
					if(isset($isHadLog[0][8])){
						$fs[1]['value'] = $isHadLog[0][8]." , ".$fs[1]['value'];
						$qb->edit_record($isHadLog[0][3],$fs);
					}else{
						$qb->add_record($fs);
					}
				$failNums++;
				continue;
			}
			$jobFieldsId = $jobFieldsId[0][3];
			$qb->set_database_table("bgttzk9i6");
			$relateFields = $qb->modelData($qb->do_query('{6.EX.'.$jobFieldsId.'}',0,0,'11.12.14'),true);
			if(is_array($relateFields) && count($relateFields)>0){
				$insertStr = array();
				$qdbFieldId = array();
				if($tid!=''){
					$pKey = $integration[0][23] == "" ? 3 : (int)$integration[0][23];
					$qdbFieldId[] = $pKey;
					$insertStr[] = $tid;
				}
				$auth_qb->set_database_table($table);
				$fIds = getFieldsIds($auth_qb);
				$insertedFieldIds = array();
				foreach($relateFields as $field){  
						if(in_array($field[11],$fIds)){
								if(!in_array($field[11],$insertedFieldIds)){
										$insertStr[] = '"'.str_replace('"','\"',$rec[$field[12]]).'"';
										$qdbFieldId[] = $field[11];
								}
						}else{
								$fs = array(array('fid'=>6,'value'=> $table),array('fid'=>8,'value'=>'Field '.$field[12].' is not exist.'),array("fid"=>9,'value'=> date("m-d-Y h:i:s A")));
								$qb->set_database_table("bgv59mqhn");
								$isHadLog = $qb->modelData($qb->do_query('{6.EX.'.$table.'}',0,0,'3.8'),true);
									if(isset($isHadLog[0][8])){
										$fs[1]['value'] = $isHadLog[0][8]." , ".$fs[1]['value'];
										$qb->edit_record($isHadLog[0][3],$fs);
									}else{
										$qb->add_record($fs);
									}
						}
						$insertedFieldIds[] = $field[11];
				}
				if($response = $auth_qb->import_from_csv(implode(',',$insertStr),implode('.',$qdbFieldId))){
					if($tid){
						if($response->rids->rid!=""){
							$successNums++;
							echo 'Integration Table Id:'.$tableId.' Record Id:'.$tid.' updated<br>';
							$fs = array(array('fid'=>6,'value'=>$table),array('fid'=>8,'value'=>'Record Id:'.$tid.' updated'),array("fid"=>9,'value'=>date("m-d-Y h:i:s A")));
						}else{
							echo '<font color="red">Integration Table Id:'.$tableId.' not exist Record Id '.$tid.' .Updated fail!</font><br>';
							$fs = array(array('fid'=>6,'value'=>$table),array('fid'=>8,'value'=>'not exist Record Id '.$tid),array("fid"=>9,'value'=>date("m-d-Y h:i:s A")));
							$failNums++;
						}
					}
				}else{
					$failNums++;
				}
				$qb->set_database_table("bgv59mqhn");
				$isHadLog = $qb->modelData($qb->do_query('{6.EX.'.$fs[0]['value'].'}',0,0,'3.8'),true);
					if(isset($isHadLog[0][8])){
						$fs[1]['value'] = $isHadLog[0][8]." , ".$fs[1]['value'];
						$qb->edit_record($isHadLog[0][3],$fs);
					}else{
						$qb->add_record($fs);
					}
			}
		}
	}
}else{
	echo $client->getError();
}


function getFieldsIds($handler){
		$fields = $handler->getFields();
		$fieldIds = array();
		foreach($fields as $key => $val){
				$fieldIds[] = $key;
		}
		return $fieldIds;
}
//file_put_contents('lastRunTime.txt',time());
echo "Total Records Num:".($successNums+$failNums)."<br/>Success Import Num:".$successNums."<br/>Fail Import Num:".$failNums;
?>
