<?php
	/**
	 * Extend the Quickbase class adding additional functionality for ease of use
	 * @author Timz0r
	 * @copyright 2009 MCF Technologies
	 */
	final class qb_mcf extends qb_default {
		
		/**
		 * Storage for records that will be inserted using import csv method
		 *
		 * @var array
		 */
		private $records = array();
		
		
		
		/**
		 * Hold cookie jar file locations
		 *
		 * @var string
		 * @var string
		 */
		private $ckfile;
		private $cjFile;
		
		
		/**
		 * Constructor
		 *
		 */
		public function __construct($un = '', $pw = '', $usexml = true, $db = '', $token = '', $realm = '', $hours = '') {
			
			parent::__construct($un, $pw, $usexml, $db, $token, $realm, $hours);
			
			$this->ckfile = tempnam("/tmp", "CURLCOOKIE");
			$this->cjFile = tempnam("/tmp", "CURLCJ");
		}
		
		
		
		/**
		 * Destructor
		 *
		 */
		public function __destruct() {
			
			parent::__destruct();
			
			unlink($this->cjFile);
			unlink($this->ckfile);
		}
		
		
		
		/**
		 * Helper function to model data
		 *
		 * @param simpleXML $results
		 * @param bool $num (If true, we get a numeric array using Field IDs for keys)
		 * @return array
		 */
		public static function modelData($results, $num = NULL) {
				
				$dom = new DOMDocument("1.0", "utf-8");
				$dom->loadXML($results->asXML());
				
				// Report errors
				if($results->errcode != 0) {
					die("The following error has been returned from QuickBase: \"" . $results->errtext . "\"");
				}
				
//				print_r($results->asXML());
//				print_r($dom->saveXML());
//				exit();
				
				// Get fields for find and replace
				$out['fields'] = array();
				if ($dom == null) return false;
				if ($dom->getElementsByTagName("fields") == null) return false;
				if ($dom->getElementsByTagName("fields")->item(0) == null) return false;
				try 
				{
					$fNodes = $dom->getElementsByTagName("fields")->item(0)->getElementsByTagName("field");
				} 
				catch(Exception $e)
				{
					return false;
				}
				
				
				foreach($fNodes as $field) {
					
					$id = $field->getAttribute("id");
					
					$label = $field->getElementsByTagName("label")->item(0)->nodeValue;
					$out['fields'][$id]['label'] = $label;
					$out['fields'][$id]['field_type'] = $field->getAttribute("field_type");
				}
				
				
				
				// Model result data
				$records = $dom->getElementsByTagName("records")->item(0)->getElementsByTagName("record");
				
				// If we have records to process
				if(!$records->length) {
					return false;
				}
	
				// Get records in array
				$i = 0;
				foreach($records as $record) {
					
					$values = $record->getElementsByTagName("f");
					foreach($values as $value) {
						
						$fid = $value->getAttribute("id");
						
						// We have the option to get an associative array, or one that uses field IDs as the key pairs
						if($num === true) {
							$key = $fid;
						}
						else {
							$key = $out['fields'][$fid]['label'];
						}
						
						// Process date
						if($out['fields'][$fid]['field_type'] == "date") {
							if (empty($value->nodeValue))
								$out['records'][$i][$key] = null;
							else
								$out['records'][$i][$key] = date("F j, Y", ($value->nodeValue / 1000));
							continue;
						}						
						if($out['fields'][$fid]['field_type']== "timestamp") {
							if (empty($value->nodeValue))
								$out['records'][$i][$key] = null;
							else
								$out['records'][$i][$key] = date("m-d-Y H:i:s", ($value->nodeValue / 1000));							
							continue;
						}
					
						$out['records'][$i][$key] = $value->nodeValue;
					}
					$i++;
				}
				
				return $out['records'];
		}
		
		/**
		 * Helper function to model data
		 *
		 * @param simpleXML $results
		 * @param bool $num (If true, we get a numeric array using Field IDs for keys)
		 * @return array
		 */
		public static function modelData2($results, $num = NULL) {
				
				$dom = new DOMDocument("1.0", "utf-8");
				$dom->loadXML($results->asXML());
				
				// Report errors
				if($results->errcode != 0) {
					die("The following error has been returned from QuickBase: \"" . $results->errtext . "\"");
				}
				
//				print_r($results->asXML());
//				print_r($dom->saveXML());
//				exit();
				
				// Get fields for find and replace
				$out['fields'] = array();
				if ($dom == null) return false;
				if ($dom->getElementsByTagName("fields") == null) return false;
				if ($dom->getElementsByTagName("fields")->item(0) == null) return false;
				try 
				{
					$fNodes = $dom->getElementsByTagName("fields")->item(0)->getElementsByTagName("field");
				} 
				catch(Exception $e)
				{
					return false;
				}
				
				
				foreach($fNodes as $field) {
					
					$id = $field->getAttribute("id");
					
					$label = $field->getElementsByTagName("label")->item(0)->nodeValue;
					$out['fields'][$id]['label'] = $label;
					$out['fields'][$id]['field_type'] = $field->getAttribute("field_type");
				}
				
				
				
				// Model result data
				$records = $dom->getElementsByTagName("records")->item(0)->getElementsByTagName("record");
				
				// If we have records to process
				if(!$records->length) {
					return false;
				}
	
				// Get records in array
				$i = 0;
				foreach($records as $record) {
					
					$values = $record->getElementsByTagName("f");
					foreach($values as $value) {
						
						$fid = $value->getAttribute("id");
						
						// We have the option to get an associative array, or one that uses field IDs as the key pairs
						if($num === true) {
							$key = $fid;
						}
						else {
							$key = $out['fields'][$fid]['label'];
						}
						
						// Process date
						if($out['fields'][$fid]['field_type'] == "date") {
							if (empty($value->nodeValue))
								$out['records'][$i][$key] = null;
							else
								$out['records'][$i][$key] = date("m-d-Y", ($value->nodeValue / 1000));
							continue;
						}						
						if($out['fields'][$fid]['field_type']== "timestamp") {
							if (empty($value->nodeValue))
								$out['records'][$i][$key] = null;
							else
								$out['records'][$i][$key] = date("m-d-Y H:i:s", ($value->nodeValue / 1000));							
							continue;
						}
					
						$out['records'][$i][$key] = $value->nodeValue;
					}
					$i++;
				}
				
				return $out['records'];
		}
		
		/**
		 * Gets the last inserted ID
		 *
		 * @param simpleXML $data
		 * @return int
		 */
		public static function getInsertId($data) {
			$doc = new DOMDocument("1.0", "UTF-8");
			$doc->loadXML($data->asXML());
			return $doc->getElementsByTagName("rid")->item(0)->nodeValue;
		}
		
		
		
		/**
		 * Gets all inserted IDs (For use with response from Import CSV)
		 *
		 * @param simpleXML $data
		 * @return array of IDs
		 */
		public function getInsertIds($data) {
			
			$doc = new DOMDocument("1.0", "UTF-8");
			$doc->loadXML($data->asXML());
			
			$out = array();
			$nodes = $doc->getElementsByTagName("rid");
			foreach ($nodes as $node) {
				$out[] = $node->nodeValue;
			}
			
			return $out;
		}
		
		
		
		/**
		 * Returns field IDs as key and labels as value for the given table
		 *
		 * @return array
		 */
		public function getFields() {
			
			$results = $this->get_schema();
			
			$dom = new DOMDocument("1.0", "UTF-8");
			$dom->loadXML($results->asXML());
			
			// Report errors
			if($results->errcode != 0) {
				die("The following error has been returned from QuickBase: \"" . $results->errtext . "\"");
			}
			
			
		//	print_r($results->asXML());
		//	exit();
			
			// Model data
			$out = array();
			$fNodes = $dom->getElementsByTagName("fields")->item(0)->getElementsByTagName("field");
			foreach($fNodes as $field) {
				
				$label = $field->getElementsByTagName("label")->item(0)->nodeValue;
				$out[$field->getAttribute("id")] = $label;
			}
			
			
			return $out;
		}
		
		
		
		/**
		 * Returns all kinds of meta data about the current table
		 *
		 * @return 
		 */
		public function getFieldData($ids = false) {
			
			
			$results = $this->get_schema();
			
			$dom = new DOMDocument("1.0", "UTF-8");
			$dom->loadXML($results->asXML());
			
			if($results->errcode != 0) {
				die("The following error has been returned from QuickBase: \"" . $results->errtext . "\"");
			}
			
			$out = array();
			$fNodes = $dom->getElementsByTagName("fields")->item(0)->getElementsByTagName("field");
			foreach($fNodes as $field) {
				
				$label	= $field->getElementsByTagName("label")->item(0)->nodeValue;
				$id		= $field->getAttribute("id");
				
				if($ids !== false) {
					$key = $id;
				}
				else {
					$key = $label;
				}
				
				$out[$key]['field_type'] = $field->getAttribute("field_type");
				$out[$key]['id'] = $id;
				$out[$key]['label'] = $label;
			}
			
			return $out;
			
		}
		
		
		
		/**
		 * Fixes fields of type "Percentage", aparantly this is an issue with the Quickbase API...
		 *
		 * @param array $data
		 * @param bool $method
		 * @return array
		 */
		public function fixPercents($data, $method = false) {
			
			if(!count($data) || !is_array($data)) {
				return false;
			}
			
			$fData = $this->getFieldData($method);
			foreach($data as $key => $row) {
				foreach($row as $field => $value)
				
			//	print($field . "=" . $value . "\n");
			//	print_r($fData[$field]['field_type']);
				if($fData[$field]['field_type'] == "percent") {
					$data[$key][$field] = $value * 100;
				}
			}
			
		//	exit();
			
			return $data;
		}
		
		
		
		/**
		 * Returns record IDs where a certain field equals a certain value
		 *
		 * @param int $fid
		 * @param string $value
		 * @param int $ridFID
		 * @return array (Field IDs matching criteria)
		 */
		public function getRIDsByField($fid, $value, $ridFID = 3) {
			
			if(empty($fid)) {
				return false;
			}
			
			$query = "{" . $fid . ".EX.'" . $value . "'}";
			$results = $this->modelData($this->do_query($query, 0, 0, $ridFID), true);
			
			$items = array();
			foreach($results as $result) {
				$items[] = $result[$ridFID];
			}
			
			return $items;
		}
		
		
		
		
		public function getTables() {
			
			// Store the starting table
			$startTbl = $this->db_id;
			
			// Get the tables schema so we can get the Application ID
			$results = $this->get_schema();
			if($results->errcode != 0) {
				die("The following error has been returned from QuickBase: \"" . $results->errtext . "\"");
			}
			
			
			$dom = new DOMDocument("1.0", "UTF-8");
			$dom->loadXML($results->asXML());
			
			// Change the table, get the results, then change it back to whatever it was before
			$this->set_database_table($dom->getElementsByTagName("app_id")->item(0)->nodeValue);
			$results = $this->get_schema();
			$this->set_database_table($startTbl);
			
			$dom = new DOMDocument("1.0", "UTF-8");
			$dom->loadXML($results->asXML());
			
			$i = 0;
			$out = array();
			$nodes = $dom->getElementsByTagName("chdbid");
			foreach($nodes as $node) {
				
				$out[$i]['label']	= ucwords(str_replace("_", " ", ltrim($node->getAttribute("name"), "_dbid_")));
				$out[$i]['id']		= $node->nodeValue;
				
				$i++;
			}
			
			
			// Return results
			if(count($out)) {
				return $out;
			}
			
			return false;
		}
		
		
		
		/**
		 * Login to QuickBase in the same manner that a user would, this is usefull for file downloads
		 *
		 * @return bool true on success, false on failure
		 */
		public function loginToQb() {
			
			$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_URL, "https://www.quickbase.com/db/main?a=signin");
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cjFile);
			curl_setopt($ch, CURLOPT_COOKIEFILE, $this->ckfile);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "loginid=" . urlencode($this->user_name) . "&password=" . urlencode($this->passwd));
			
			$r = curl_exec($ch);
			
		//	print_r($r);exit;
			
			if(strstr($r, "<title>My QuickBase</title>") === false) {
				return false;
			}
			
			return true;
		}
		
		
		
		/**
		 * Retrieve a file from QuickBase
		 *
		 * @param unknown_type $url
		 */
		public function retrieveFile($url) {
			
			
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cjFile);
			curl_setopt($ch, CURLOPT_COOKIEFILE, $this->ckfile);
		//	curl_setopt($ch, CURLOPT_HEADER, true);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
			curl_setopt($ch, CURLOPT_COOKIE, "TICKET=" . urlencode($this->ticket));
			
			$r = curl_exec($ch);
			
			return $r;
		}
	}