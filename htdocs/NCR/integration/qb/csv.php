<?php
	/**
	 * Assists with the import csv functionality of QuickBase
	 * @author Timz0r
	 * @copyright 2009 MCF Web Services
	 *
	 */
	final class qb_csv {
		
		// CSV Fields, (Represented by FIDs)
		private $fields	= array();
		
		// Row data
		private $rows	= array();
		
		// Used to store fid / value arrays for one row
		private $row	= array();
		
		
		
		/**
		 * Add an item to current row
		 *
		 * @param unknown_type $value
		 */
		public function addItem($value) {
			$this->row[] = $value;
		}
		
		
		
		/**
		 * Advance to a new row
		 *
		 */
		public function nextRow() {
			$this->rows[] = $this->row;
			$this->row = array();
		}
		
		
		
		/**
		 * Return the clist for the import
		 *
		 * @return unknown
		 */
		public function getClist() {
			return implode(".", $this->fields);
		}
		
		
		
		/**
		 * Returns CSV String for QuickBase csv import
		 *
		 * @return string / bool false on fail
		 */
		public function getCSV() {
			
			// Check if there are rows
			if(!is_array($this->rows) || !count($this->rows)) {
				return false;
			}
			
			
			// Gather all field IDs from the first record to use as a clist, all subsequent rows should have the same clist or this will not work
			$this->fields = array();
			foreach ($this->rows[0] as $item) {
				$this->fields[] = $item['fid'];
			}
			
			
			// Iterate through all rows
			$csv = "";
			foreach ($this->rows as $row) {
				
				// If this is an extra or empty one, skip it and continue
				if(count($row) == 0) {
					continue;
				}
				
				// Make sure we have the same number of fields in each row
				if(count($row) != count($this->fields)) {
					die("Error, the number of fields for the current row (" . print_r($row) . ") is not equal to the number of fields in the generated list (" . print_r($this->fields) . ").");
				}
				
				// Iterate through each "field => value" pair in the current row
				foreach($row as $key => $value) {
					
					// Make sure that the order is the same as the pre-generated list
					if($this->fields[$key] != $value['fid']) {
						die("Error, field IDs are not in the proper order in the current row, (position: " . $key . "current fid: " . $value['fid'] . ", (it should be " . $this->fields[$key] .", according to the pre-generated list) value: " . $value['value'] . ")");
					}
					
					// If we have a comma and a quote in our value, we need to replace the double quotes with a single quote (a QuickBase Problem)
					if((strstr($value['value'], ',') !== false) && (strstr($value['value'], '"') !== false)) {
						$value['value'] = str_replace('"', "'", $value['value']);
					}
					
					// If we have just commas, then we need to add quotes around the current entry
					if(strstr($value['value'], ',') !== false) {
						$value['value'] = '"' . $value['value'] . '"';
					}
					
					// Replace "&" with "and", "\n" with "", trim white space, add a comma, and add the value to the list
					$csv .= trim(str_replace(array("&", "\n"), array(" and ", ""), $value['value'])) . ",";
				}
				
				// Remove the trailing comma, and add a line break to signify the end of this record
				$csv = rtrim($csv, ",");
				$csv .= "\n";
			}
			
			// Return csv list without ending line break
			return rtrim($csv, "\n");
		}
		
		
		
		/**
		 * Converts a CSV string to an array
		 *
		 * @param csv string $input
		 * @param string $delimiter
		 * @param  string $enclosure
		 * @return array
		 */
		public static function csvToArray($csv, $delimiter = ',', $enclosure = '"', $escape = '\\', $terminator = "\n") {
			
			$out = array();
			$rows = explode($terminator, $csv);
			foreach($rows as $row) {
				$row = trim($row);
				$values = str_getcsv($row, $delimiter, $enclosure, $escape);
				$out[] = $values;
			}
			
			return $out;
		}
	}