<?php
error_reporting(E_ERROR);
set_time_limit(0);
require_once('nusoap/lib/nusoap.php');
include_once("qb/default.php");
include_once("qb/mcf.php");
include_once("qb/csv.php");
$timeStampFrom  = '';
/*
$countRows = 2; //Every http request get rows.don't set this param too large.
$pageRefreshTime = 1; //Page will be refreshed after  5 seconds.
$offset = isset($_GET['offset']) ? $_GET['offset'] : 0; //which record will be start.
*/
$successNums = 0;
$failNums = 0;
if(!file_exists('lastRunTime.txt')){
	file_put_contents('lastRunTime.txt',"");
	$timeStampFrom = "";
}else{
	$timeStampFrom = file_get_contents('lastRunTime.txt');
}
$client = new nusoap_client("https://api.fieldsolutions.com/api/client/wsdl/3",true);
$client->soap_defencoding = 'UTF-8';
$param = array(
'user'=>'ncrapi',
'pass' => 'ncrapi20111212',
'fields'=>"",
'timeStampFrom'=> $timeStampFrom == "" ? "" : date('m/d/Y',$timeStampFrom),
'timeStampTo' => '',
'timeStampType' => '',
'filters' => array('QBIDIsNotEmpty'=>true,'Parts_Presents'=>false,'Docs_Presents'=>false)
);
$result = $client->call('getWorkOrders',$param,'ns1');
if($result['success'] == 1 && count($result['data']) !=0){
	$updateArr = array();
	foreach($result['data'] as $rec){
		$data = split('-',$rec['QBID']);
		if(count($data)>1 && preg_match("/QBID-(.*)-\d{0,20}$/i",$rec['QBID'])){
			$table = $data[1];
			$tid = $data[2];
			$qb = new qb_mcf();
			$qb->set_database_table("bgttzk9fp");
			$jobFieldsId = $qb->modelData($qb->do_query('{8.EX.'.$table.'}',0,0,'3.8.13'));
			if(isset($jobFieldsId[0]['QBID Validation']) && $jobFieldsId[0]['QBID Validation'] == 0){
				$fs = array(array('fid'=>6,'value'=>$jobFieldsId[0]['Table ID']),array('fid'=>8,'value'=>'Integration QBID is Required.  Please add the QBID to the list of Integration Fields for this Record'),array("fid"=>9,'value'=>date("m-d-Y")));
				$qb->set_database_table("bgv59mqhn");
				$qb->add_record($fs);
				exit("<script type='text/javascript'>alert('Integration QBID is Required.  Please add the QBID to the list of Integration Fields for this Record')</script>");
			}
			$jobFieldsId = $jobFieldsId[0]['Record ID#'];
			$qb->set_database_table("bgttzk9i6");
			$relateFields = $qb->modelData($qb->do_query('{6.EX.'.$jobFieldsId.'}',0,0,'11.12'));
			if(is_array($relateFields) && count($relateFields)>0){
				$insertStr = array();
				$qdbFieldId = array();
				if($tid!=''){
					$qdbFieldId[] = 3;
					$insertStr[] = $tid;
				}
				foreach($relateFields as $field){
					$insertStr[] = '"'.str_replace('"','\"',$rec[$field['Field Name']]).'"';
					$qdbFieldId[] = $field['Field ID'];
				}
				$qb->set_database_table($table);
				if($qb->import_from_csv(implode(',',$insertStr),implode('.',$qdbFieldId))){
					$successNums++;
				}else{
					$failNums++;
				}
			}
		}
	}
}else{
	echo $client->getError();
}
file_put_contents('lastRunTime.txt',time());
echo "Total Records Num:".($successNums+$failNums)."<br/>Success Import Num:".$successNums."<br/>Fail Import Num:".$failNums;
/*
$offset = $offset+$countRows;
echo '<script type="text/javascript">function timeout(){location.href="http://'.$_SERVER['HTTP_HOST'].$_SERVER['SCRIPT_NAME'].'?offset='.$offset.'";}setTimeout("timeout()",'.($pageRefreshTime*1000).');</script>';
				*/
?>
