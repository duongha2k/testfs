<?php $page = techs; ?>
<?php $option = more; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->

<br /><br />

<div align="center">
  <table border="0" cellpadding="0" cellspacing="0" width="450">
    <tbody>
      <tr>
        <td><div align="center"><img src="/NCR/NCR_Logo.jpg" alt="NCR Logo" width="277" height="47" /></div></td>
      </tr>
      <tr>
        <td bgcolor="#FFFBEF"><div align="center">&nbsp;</div></td>
      </tr>
      
      <tr>
        <td bgcolor="#FFFBEF"><div align="center"><font color="#174065" face="Verdana"><b>NCR Papa John's Files</b></font></div></td>
      </tr>
      <tr>
        <td bgcolor="#FFFBEF">&nbsp;</td>
      </tr>
      
      <tr>
      <td bgcolor="#FFFBEF"><div align="left"><strong>Instructions:</strong></div></td>
    </tr>
      <tr>
        <td bgcolor="#FFFBEF">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#FFFBEF"><div align="left">
          <p>1. Download the files below.</p>
          <p>2. Burn all the files  to a CD.<br>
            <br>
            <strong><font color="#FF0000">NOTE:</font></strong> When saving the files, be sure they <strong>DO NOT</strong> end with .zip in the file name! Each file should end with 3 digits (ie: 001, 002, etc)</p>
          </div></td>
      </tr>
      <tr>
        <td bgcolor="#FFFBEF">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#FFFBEF"><table width="100%" border="0" cellpadding="2">
          <tr>
            <td><a href="PapaJohns/pkg2003.FC5_V621R4.001">Fedora V621.R4.001</a></td>
          </tr>
          <tr>
            <td><a href="PapaJohns/pkg2003.FC5_V621R5.002">Fedora V621.R5.002</a></td>
          </tr>
          <tr>
            <td><a href="PapaJohns/pkg2003.FC5_V621R6.003">Fedora V621.R6.003</a></td>
          </tr>
          <tr>
            <td><a href="PapaJohns/pkg2003.FC5_V622R0.004">Fedora V622.R0.004</a></td>
          </tr>
          <tr>
            <td><a href="PapaJohns/pkg2003.FC5_V622R1.005">Fedora V622.R1.005</a></td>
          </tr>
          <tr>
            <td><a href="PapaJohns/pkg2003.FC5_V622R2.006">Fedora V622.R2.006</a></td>
          </tr>
        </table></td>
      </tr>
  </tbody></table>
</div>

<!--- End Content --->
<?php require ("../footer.php"); ?>
		
