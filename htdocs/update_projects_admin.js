

function amountPerSelector() {
	return $(amount_per_selector_field_name);
}

function deviceQuantityField() {
	return $(device_quantity_field_name);
}


$(document).ready(function() {
	deviceQuantityField().attr("disabled",true);

	//alert("hello");

	amountPerSelector().change(function() {
		var toggle = (amountPerSelector().attr("value") != "Device");
		deviceQuantityField().attr("disabled",toggle);
		if (!toggle)
			deviceQuantityField().focus();
	});

});

