<?php
// Set site URL
if (!defined("FS_SESSION_STARTED")):
define("FS_SESSION_STARTED", true);

//if ($_SERVER["HTTP_HOST"] === "local-dev:8880") $_SERVER["HTTP_HOST"] = "dev.widgets.warecorp.com";

//$siteUrl = "https://www.fieldsolutions.com"; 
ini_set("session.gc_maxlifetime", 84600);
$siteUrl = "http".((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on') ? 's':'')."://". $_SERVER['HTTP_HOST'];
$cookie_path = "/";
$cookie_timeout = "0";
//$cookie_domain = ".fieldsolutions.com";

if (strpos($_SERVER['HTTP_HOST'], ".com") === false)
	$cookie_domain = $_SERVER['HTTP_HOST']; // number ip
else
	$cookie_domain = '.fieldsolutions.com';

require_once("library/PHPSessionHandler.php");
require_once("library/script_caching.php");
$phpSessionHandler = new Core_PHPSessionHandler();

if ( !session_id() || !isset($_SESSION)) {
    session_set_cookie_params($cookie_timeout, $cookie_path, $cookie_domain);
    session_name('fsTemplate');
    session_start();
}

// Get Subdomain
$urlParts = explode('.', $_SERVER['HTTP_HOST']);
$template = $urlParts[0];
//894 
$v= $_REQUEST['v'];
//$template='pitneybowes';
if ($template === "dev" || ($template === "test" && $v=='PITBC')) $template = "www";
//end 894
if ( isset($_SESSION['template']) ) {
    $siteTemplate = $_SESSION['template'];
} else {
    $siteTemplate = null;
}

$checkIndexDisplay = $template;

if($siteTemplate == ""){
	$_SESSION['template']=$template;
	$siteTemplate = $_SESSION['template'];
}else if($siteTemplate != $template){
	$_SESSION['template']=$template;
	$siteTemplate = $_SESSION['template'];
}

// GLOBAL VARIABLES
$loggedIn = (  empty($_SESSION['loggedIn']) ) ? '' : $_SESSION['loggedIn'];
if($loggedIn==""){$loggedIn="no";}
if ( isset($_SESSION['loggedInAs']) ) {
    $loggedInAs = $_SESSION['loggedInAs'];
} else {
    $loggedInAs = "";
}
if($loggedInAs==""){$loggedInAs="no";}

$serverDateTime = date("m/d/Y H:i:s");

require_once("library/loginSessionChecks.php");


$ignorePageList = array("none" => 1,"login" => 1, "adminlogin" => 1, "contactus" => 1, "aboutus" => 1);
if (isset($page) && (!isset($option) || $option != "home")) {
	if (!array_key_exists($page, $ignorePageList)) {
		// find calling script's path
		$myScriptPath = explode(dirname(__FILE__), pathinfo($_SERVER['SCRIPT_FILENAME'], PATHINFO_DIRNAME));
		if (array_key_exists(1, $myScriptPath))
			$myScriptPath = $myScriptPath[1];
		else 
			$myScriptPath = "";
						
		// check login by directory
		if (strpos($myScriptPath, "fls") === FALSE) {
			// main site
			switch ($page) {
				case "admin":
					checkAdminLogin2();
					break;
				case "clients":
					checkClientLogin();
					break;
				case "techs":
					checkTechLogin();
					break;
				case "accounting":
					checkAccountingLogin();
					break;
				case "staff":
					checkStaffLogin();
					break;
				case "customers":
					checkCustomerLogin();
					break;
				default:
					header("Location: /pageUnavailable.php?r=" . urlencode(base64_encode($_SERVER['SCRIPT_FILENAME'])) );
					break;
			}
		}
		else {
			// fls site
			switch ($page) {
				case "admin":
					checkFLsAdminLogin();
					break;
				case "cc":
					checkFLSCCLogin();
					break;
				case "manager":
					checkFLSManagerLogin();
					break;
				case "installdesk":
					checkFLSInstallDeskLogin();
					break;
				default:
					header("Location: /pageUnavailable.php?r=" . urlencode(base64_encode($_SERVER['SCRIPT_FILENAME'])) );
					break;
			}
		}
	}
}
endif;

?>
