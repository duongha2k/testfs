<?php
// Updates ISO and ISO tech information
require_once("headerStartSession.php");
if (!isset($_GET["TechID"]) && !isset($_GET["ISOID"])) die();
// queue the update
if (isset($_GET["TechID"]))
	$_SESSION["MysqlUpdateTech"] = $_GET["TechID"];
else
	$_SESSION["MysqlUpdateISO"] = $_GET["ISOID"];
?>
<script type="text/javascript">
	setTimeout('close()',100);
</script>