<?php
	try {
		require_once("library/caspioAPI.php");
		require_once("library/smtpMail.php");
		$mainBackOutsCount = caspioSelect("Work_Orders", "BackOut_Tech, Count(BackOut_Tech)", "BackOut_Tech <> '' GROUP BY BackOut_Tech", "BackOut_Tech ASC", false);
//		$flsBackOutsCount = caspioSelect("FLS_Work_Orders", "BackOut_Tech, Count(BackOut_Tech)", "BackOut_Tech <> '' GROUP BY BackOut_Tech", "BackOut_Tech ASC", false);
		
		$countByID = array();
		foreach ($mainBackOutsCount as $nCnt) {
			// combining counts from fls and main site work orders
			if ($nCnt == "") continue;
			$pair = explode(",", $nCnt);
			$id = $pair[0];
			$count = $pair[1];
			$countByID[$id] = $count;
		}
/*		foreach ($flsBackOutsCount as $nCnt) {
			// combining counts from fls and main site work orders
			if ($nCnt == "") continue;
			$pair = explode(",", $nCnt);
			$id = $pair[0];
			$count = $pair[1];
			if (!isset($countByID[$id]))
				$countByID[$id] = $count;
			else
				$countByID[$id] += $count;						
		}*/
		
		$IDsByCount = array();
		foreach ($countByID as $id => $count) {
			// group BackOut_Tech by number of no shows
			if (!isset($IDsByCount[$count]))
				$IDsByCount["$count"] = $id;
			else
				$IDsByCount["$count"] .= "," . $id;
		}
		
		// clears no shows
		caspioUpdate("TR_Master_List", "Back_Outs", "'0'", "", false);
		
		foreach ($IDsByCount as $count => $ids) {
			// update tech's record with no show count
				caspioUpdate("TR_Master_List", "Back_Outs", "'$count'", "TechID IN ($ids)", false);
		}			
	}
	catch (SoapFault $fault) {
		smtpMail("BackOut Count Script", "nobody@fieldsolutions.com", "codem01@gmail.com", "BackOut Count Script Error", "$fault", "$fault", "BackOutCount.php");
	}	
?>
