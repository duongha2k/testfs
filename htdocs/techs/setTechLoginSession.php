<?php
//This page is will log all clients that login by inserting into Caspio using SOAP.
require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');

try {

// Retrieve values from login
$UserName = $_REQUEST['UserName'];
$Password = $_REQUEST['Password'];



// Insert Login History into TR_Client_Login_History
//$insertWorkorders = caspioInsert("TR_Client_Login_History", "UserName, DateTime", "'$UserName', '$DateTime'");

$authData = array('login'=>$UserName, 'password'=>$Password);
//$errors = Core_Api_Error::getInstance();

$user = new Core_Api_TechUser();
$user->checkAuthentication($authData);

/*
if (!$user->isAuthenticate() || !$user->isActivated()){
	$redirectUrl = empty($_REQUEST['r']) ? '' : "&r=" . urlencode($_REQUEST['r']);
	if($_REQUEST['page'] == "home" ){
		if(!$user->isActivated()){
			header("Location: /wp/?login=tech&value=notactivated&email=".$user->getEmail() . $redirectUrl);
		}else{
			header("Location: /wp/?login=tech&value=false" . $redirectUrl);
		}
	}elseif($_REQUEST['flsupport'] == "true"){
		header("Location: /techs/logIn.php?login=false&flsupport=true" . $redirectUrl);
	}else{
		if(!$user->isActivated()){
			header("Location: /techs/logIn.php?login=notactivated&email=".$user->getEmail() . $redirectUrl);
		}else{
			header("Location: /techs/logIn.php?login=false" . $redirectUrl);
		}
	}
	return API_Response::fail();
} 
*/

if (!$user->isAuthenticate()){
	$redirectUrl = empty($_REQUEST['r']) ? '' : "&r=" . urlencode($_REQUEST['r']);
        if($_REQUEST['devry'] == "1" || !empty($_REQUEST['devry'])){
            header("Location: /devry/login.php?value=false" . $redirectUrl);
        }
	else if($_REQUEST['page'] == "home" ){
		header("Location: /wp/?login=tech&value=false" . $redirectUrl);
	}elseif($_REQUEST['flsupport'] == "true"){
		header("Location: /techs/logIn.php?login=false&flsupport=true" . $redirectUrl);
	}else{
		
		header("Location: /techs/logIn.php?login=false" . $redirectUrl);
	}
	return API_Response::fail();
}

$TechID = $user->getTechID();
$Date_TermsAccepted = strtotime($user->getDateAccepted());
//if ($Date_TermsAccepted != ""){
//$Date_TermsAccepted = date("m/d/Y", strtotime($Date_TermsAccepted));
//}
$FLSID = $user->getFlsID();
$FLSCSP_Rec= $user->getFlsCpRec();
$ISO_ID= $_REQUEST['ISO_ID'];

$Refresh= $_REQUEST['Refresh'];

require_once("../headerStartSession.php");

//Set logged in session
/*$cookie_path = "/";
$cookie_timeout = "0";
#$cookie_domain = ".fieldsolutions.com";
//$cookie_domain = str_replace('www.', '', strtolower('.'.$_SERVER['HTTP_HOST']));
$cookie_domain = ".widgets.warecorp.com";

session_set_cookie_params($cookie_timeout, $cookie_path, $cookie_domain);
session_name('fsTemplate');
session_start();*/

//$techLoginMessageViewed = $_SESSION['techLoginMessageViewed'];
//$techLoginMessageViewed = isset($_SESSION['techLoginMessageViewed']) ? $_SESSION['techLoginMessageViewed'] : null;
$_SESSION['loggedIn']="yes";
$_SESSION['loggedInAs']="tech";
$_SESSION['TechID']=$TechID;
$_SESSION['FLSID']=$FLSID;
$_SESSION['FLSCSP_Rec']=$FLSCSP_Rec;
$_SESSION["FSCert"] = "";
$_SESSION['ISO_ID']=$ISO_ID;
$_SESSION["UserName"]=$UserName;
$_SESSION["LoginPage"] = isset ($_REQUEST["login_page"]) ? $_REQUEST["login_page"] : NULL;

// Library calls
require_once("../library/caspioAPI.php");

$user = new Core_Api_TechUser();
$user->load(array('login'=> $_SESSION["UserName"]));
$_SESSION["UserPassword"] = $user->getPassword();

// Insert Login History into Login_History
//$insertWorkorders = caspioInsert("Login_History", "UserName, DateTime, UserType", "'$UserName', GETDATE(), 'Tech'",false);
$user->loginHistory($UserName);

//Handling for "Remember Me" checkbox in login form
$login_key = $_REQUEST["tech_key"];
$login_data = $_REQUEST["tech_data"];

if (!empty ($login_key)) {
	$db = Core_Database::getInstance();

	if (!empty ($login_data)) {
		$select = $db->select ();
		$select->from ("tech_login")
			->where ("login_key = ?", $login_key);

		$records = $db->fetchAll ($select);

		if (empty ($records)) {
			$db->insert ("tech_login", array (
				"tech_id" => $TechID, 
				"login_key" => $login_key,
				"login_data" => $login_data
			));
		}
		else {
			$record = $records[0];

			$db->update ("tech_login", array (
				"tech_id" => $TechID, 
				"login_data" => $login_data
			), $db->quoteInto("id = ?", $record["id"]));
		}

		//$db->delete ("tech_login", $db->quoteInto("tech_id = ?", $TechID) . " AND " . $db->quoteInto("login_key <> ?", $login_key));
	}
	else {
		$db->delete ("tech_login", $db->quoteInto("login_key = ?", $login_key));
	}
}

// Get Subdomain
$urlParts = explode('.', $_SERVER['HTTP_HOST']);
$template = $urlParts[0];
$siteTemplate = $_SESSION['template'];

$checkIndexDisplay = $template;

if($siteTemplate == ""){
	$_SESSION['template']=$template;
	$siteTemplate = $_SESSION['template'];
}else if($siteTemplate != $template){
	$_SESSION['template']=$template;
	$siteTemplate = $_SESSION['template'];
}

}catch (SoapFault $fault){
//FTP Failure code

//smtpMail("getClientLoginHistory", "admin@fieldsolutions.com", "cmcgarry@fieldsolutions.com,gbailey@fieldsolutions.com", "getClientLoginHistory Error", "$fault", "$fault", "getClientLoginHistory.php");
}


// LOGIN REDIRECTS
//$techLoginMessageViewed = $_COOKIE["techLoginMessageViewed"];
//$techLoginMessageViewed = isset($_COOKIE["techLoginMessageViewed"]) ? $_COOKIE["techLoginMessageViewed"] : null;

// MM/DD/YYYY HH:MM:SS

$_SESSION["acceptedICA"] = ($Date_TermsAccepted == "" || $Date_TermsAccepted < strtotime('03/03/2008 10:00:00 PM') ? "No" : "Yes");

if($Date_TermsAccepted == "" || $Date_TermsAccepted < strtotime('03/03/2008 10:00:00 PM')){
// 1/9/2009 4:50:35 PM
//	if($techLoginMessageViewed=="yes"){
	// Redirect to techTerms.php if terms have not been accepted
	header('Location: https://'.$_SERVER['HTTP_HOST'].'/content/techTerms_form.php');
/*	}else{
	// Redirect to login message
	header('Location: http://'.$_SERVER['HTTP_HOST'].'/techs/loginMessage_techTerms.php');
	}
*/

}else if ($Refresh == "No"){
	header('Location: http://'.$_SERVER['HTTP_HOST'].'/techs/TechRefresh.php');

}else{

//	if($techLoginMessageViewed=="yes"){
	// Redirect to dashboard.php

	/**
     * Modified by Sergey Petkevich (Warecorp)
     */
	
	
	 
	$forceSSL = $siteTemplate != "dev";
	if($user->getAcceptTerms() != "Yes") $acceptTermsVar = "acceptTerms=No";
	
	if($_REQUEST['flsupport'] == "true"){
		$redir = $redir = '/techs/Training/FLS/flsupportdocs.php';
	}else if($_REQUEST['activateRegistration'] == "true"){
		$redir = 'http'.($_SERVER['HTTPS'] == 'on' || $forceSSL ? 's' : '').'://'.$_SERVER['HTTP_HOST'].'/techs/updateProfile.php';
	}else{
		if(!$user->isActivated()){
			$redir = 'http'.($_SERVER['HTTPS'] == 'on' || $forceSSL ? 's' : '').'://'.$_SERVER['HTTP_HOST'].'/techs/dashboard.php?activated=false&'.$acceptTermsVar;
		}else{
			$redir = 'http'.($_SERVER['HTTPS'] == 'on' || $forceSSL ? 's' : '').'://'.$_SERVER['HTTP_HOST'].'/techs/dashboard.php?'.$acceptTermsVar;
		}
	}

	if ($user->getDeactivated() != 1) {
		if ( !empty($_SESSION['redirectURL']) ) {
			$redir = $_SESSION['redirectURL'];
			unset($_SESSION['redirectURL']);
		}
		else if (!empty($_REQUEST['r'])) {
			$redir = $_REQUEST['r'];
		}
	}

     header( 'Location: ' . $redir ) ;
	//header('Location: http://'.$siteTemplate.'.fieldsolutions.com/techs/dashboard.php');
	
    /*} else if($siteTemplate == "ruo"){
    }
	// Redirect to RUO login message
	header('Location: http://'.$_SERVER['HTTP_HOST'].'/techs/ruo_tech_loginMessage.php');
	} else {
	// Redirect to login message
	header('Location: http://'.$_SERVER['HTTP_HOST'].'/techs/loginMessage.php');
	}
*/
}
?>

