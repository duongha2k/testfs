<?php $page = techs; ?>
<?php $option = more; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->


<br /><br />
<script type="text/javascript">

$(document).ready(function(){
	$("body").css("width", "850px").css("min-width", "850px");
	
});

</script>

<style type="text/css">
#mainContainer{
	min-width: 800px;
	width: 850px;
}
	div#body {
		width: 600px;
		margin: auto;
		margin-top: -15px;
	}
	
	h5 {
		font-size: 1em;
		margin-top: 15px;
	}
</style>

<div id="body">
	<h5>FLS &ndash; Computing Security Procedures (FLS-CSP) Policy</h5>
	<p>
	As of March 1 2008, Fujitsu Transactions Services (FTXS)  is requiring all First Line Support (FLS) technicians to review, sign and submit the FLS-CSP form in order to be eligible to run calls or work projects/installs for FLS. Effective immediately, all new FLS technicians will be required to complete the FLS-CSP.</p>
	<p>The FLS-CSP form helps assure FTXS that FLS technicians working for them understand the following:</p>
	<p>As a contracted technician working for FLS, you agree to adhere to customer policies, regulations and procedures governing the security of their computers and associated data that may be provided to you by the customer.  You will be acknowledging and agreeing to not access information for any reason other than what is necessary to complete the job/task.</p>
	<p><b>Note:</b> The FLS-CSP form that you need to download below is in PDF. Meaning, if you do not already have it, you will need the <a href="http://www.adobe.com/products/acrobat/readstep2.html" target=_blank>Adobe Reader</a> software to view and print out the form for faxing. You can download the required Adobe Reader software for free by <a href="http://www.adobe.com/products/acrobat/readstep2.html" target=_blank>
		clicking here</a>.
	<p><img height=23 alt="" src="images/pdf.jpg" width=23 border=0> <b>FLS-CSP Form</b> / <a href="forms/FLSCSP.pdf">Click here</a> to download</p>

		  <h5>Fax your completed and signed FLS-CSP form to: 888-234-6977</h5>
		<p>We are accepting only faxes of the Disclosure form.  Do not email it.</p>
    <p>Once Field Solutions has received your signed FLS-CSP form and it has been entered as such in your tech profile (account), there will be a "Yes" next to "Received?" under "FLS Computer Security Policy" in the FLS portion of your technician profile (account). Access the FLS portion of your tech account by clicking on the "More" link (far right in the top blue menu) then clicking where it reads, "Click here to update your profile with information for FLS"</p>
    <p>Questions? Please contact Field Solutions support at: <a href="mailto:support@fieldsolutions.com">support@fieldsolutions.com</a></p>
    <p>&nbsp; </p></td>
	</tr>	
</table>	

</div>

<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
