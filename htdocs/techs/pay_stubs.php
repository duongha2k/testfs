<?php
$page = 'techs';
$option = 'pay_stubs';
require '../header.php';
 /* Check authentication */
$authData = array('login' => $_SESSION["UserName"], 'password' => $_SESSION["UserPassword"]);
$tech = new Core_Api_TechUser;
$tech->checkAuthentication($authData);

if (!$tech->isAuthenticate()) {
   return API_Response::fail();
}
$techFirstName = $tech->getFirstName(); 
$techLastName = $tech->getLastName(); 
$techPaymentMethod = $tech->getPaymentMethod();
?>
<script>
$(document).ready(function() {
<?php if ($tech->getDeactivated() == 1): ?>
		disableLinks($("div.left_col a:not(.allowLinkTD)"));
<?php endif; ?>

if (typeof(startup) == 'function') startup()});
    window._techId = '<?= $_SESSION['TechID'] ?>';
var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";
</script>
<?php
require '../navBar.php';
$search = new Core_Api_TechClass();
$counters = array();
$search->getWorkOrdersCountByStatus($_SESSION["UserName"], $_SESSION["UserPassword"], $counters);

$pointsSummary = Core_FSPlusOne::getTechPointSummary($_SESSION['TechID']);
if (!$pointsSummary) {
    $pointsSummary['allPoints'] = "0";
    $pointsSummary['currentPoints'] = "0";
}
$isEligible = Core_FSPlusOne::isEligible($_SESSION['TechID']);  
$periodList = Get3Periods();
$y = date("Y");
$netPayYearToDate = $search->getNetPayForYear($_SESSION["UserName"], $_SESSION["UserPassword"], $y);

/* test * */
$startDate = "2007-01-01";
$endDate = "2010-09-27";
$netPayForPeriod = $search->getNetPayForPeriod($_SESSION["UserName"], $_SESSION["UserPassword"], $startDate, $endDate);
/* test * */

function Get3Periods() {
    $y = date("Y", time());
    $d = date("d", time());
    $m = date("m", time());
    $date_array = array();
    
    $date_start = getdate(mktime(0, 0, 0, $m, $d, $y));
    if ($date_start['wday'] <= 4) {
        $Thursday = $date_start['wday'] + 3;
    } else {
        $Thursday = $date_start['wday'] - 4; 
    }
    $date_start = mktime(0, 0, 0, $m, $d - $Thursday, $y);
    $y = date("Y", $date_start);
    $d = date("d", $date_start);
    $m = date("m", $date_start);
    
    for ($i = 0; $i < 3; $i++) {
        $date_start = mktime(0, 0, 0, $m, $d - 6, $y);
        $date_end = mktime(0, 0, 0, $m, $d, $y);
        
        $date_array[$i][0] = date("m/d/y", $date_start) . ' &minus; ' . date("m/d/y", $date_end);
        $date_array[$i][1] = date("Y-m-d", $date_start) . ',' . date("Y-m-d", $date_end);
        
        $_date = mktime(0, 0, 0, $m, $d - 7, $y);
        
        $y = date("Y", $_date);
        $d = date("d", $_date);
        $m = date("m", $_date);
    }
    return $date_array;   
}
?>
<?php

function GetPeriodsToRegister($registerCreated) {
    $registerCreated = new DateTime($registerCreated);
    $y = date("Y", time());
    $d = date("d", time());
    $m = date("m", time());

    $date_array = array();

    $date_start = getdate(mktime(0, 0, 0, $m, $d, $y));
    if ($date_start['wday'] <= 4) {
        $Thuday = $date_start['wday'] + 3;
    } else {
        $Thuday = $date_start['wday'] - 4;
    }
//var_dump ($date_start, $Thuday);
//echo ("<br/>");
    $date_start = mktime(0, 0, 0, $m, $d - $Thuday + 7, $y);
    $y = date("Y", $date_start);
    $d = date("d", $date_start);
    $m = date("m", $date_start);
    $i = 0;
    $_date_start = new DateTime($y . '-' . $m . '-' . $d);
//var_dump ($date_start, $_date_start, $y, $m, $d);
//echo ("<br/>");
//exit (0);
    while ($_date_start >= $registerCreated) {
        
        $date_start = mktime(0, 0, 0, $m, $d - 6, $y);
        $date_end = mktime(0, 0, 0, $m, $d, $y);

        $_y = date("Y", $date_start);
        $_d = date("d", $date_start);
        $_m = date("m", $date_start);
        $_ey = date("Y", $date_end);
        
        $_date_start = new DateTime($_y . '-' . $_m . '-' . $_d);
        if ($_date_start < $registerCreated) {
            break;
        }
        $date_array[$i][0] = $_ey;
        $date_array[$i][1] = date("m/d/Y", $date_start);
        $date_array[$i][2] = date("m/d/Y", $date_end);
        $date_array[$i][3] = date("Y-m-d", $date_start);
        $date_array[$i][4] = date("Y-m-d", $date_end);
        $i++;

        $_date = mktime(0, 0, 0, $m, $d - 7, $y);

        $y = date("Y", $_date);
        $d = date("d", $_date);
        $m = date("m", $_date);
    }
    return $date_array;
}

$search = new Core_Api_TechClass();
//$registerCreated = '2011-10-21';
$registerCreated = $tech->getRegDate();
$aYear4Week = GetPeriodsToRegister($registerCreated);
//print_r($aYear4Week);die();//test
?>
<script type="text/javascript" src="/library/jquery/jquery.bgiframe.min.js"></script>
<script type="text/javascript" src="/library/js/excanvas.js"></script>
<script type="text/javascript" src="/library/jquery/jquery.bt.min.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.client.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.easing.pack.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-mod.js"></script>
<link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" media="all" />
<style>
    .tableTn_head th {
        border-bottom: 1px solid #000000;
        border-right: 1px solid #FFFFFF;
        border-top: 1px solid #000000;
        padding: 2px 3px;
        text-align: center;
    }
    .tableTn_head th, .table_head th a {
        background: none repeat scroll 0 0 #5091CB;
        color: #FFFFFF;
        font-weight: normal;
    }
    div#loader{
            width:100px;
            height: 100px;
            background:url(/widgets/images/loader.gif) no-repeat center #fff;
            text-align:center;
            padding:10px;
            font:normal 16px Tahoma, Geneva, sans-serif;
    }
</style>
<script>
$(document).ready(function(){
    if($.client.os == 'Windows'){
        if($.client.browser == 'Firefox'){
            $('#pointsSummary').css('top', '120px');
            
        }
        if($.client.browser == 'Explorer'){
        
            $('#pointsSummary').css('top', '121px');
        }
    }

    $(".certForms").click(function(){
        if($(this).attr('rel') == "insurance"){
            var messageHtml = "<h2>FieldSolutions Insurance Coverage</h2>";
                messageHtml += "<p>FieldSolutions insures our clients against risk of work on-site. As such, we never put you through the hassle of producing your insurance overages, we don�t verify whether your payments are current; and most important we don�t charge you a fee like other systems for coverages. Insurance coverage is just one reason FieldSolutions is the most preferred place to get technology field work. Remind your clients that you want to get your work and paid through FieldSolutions.</p>";

            $("<div></div>").fancybox(
                    {
                        'autoDimensions' : false,
                        'width'    : 420,
                        'height' : 225,
                        'transitionIn': 'none',
                        'transitionOut' : 'none',
                        'content' : messageHtml
                    }
                ).trigger('click');
    
        }else{
        
            $("<div></div>").fancybox(
                    {
                        'autoDimensions' : false,
                        'width'    : 900,
                        'height' : 850,
                        'transitionIn': 'none',
                        'transitionOut' : 'none',
                        'type': 'iframe',
                        'href': $(this).attr('rel')
                    }
                ).trigger('click');
        }
    
    });
});

function updatePaymentInfo(){
    document.location = "/techs/updateProfile.php?data=showPaymentInfo";
}
</script>

<script>
$(document).ready(function(){ 
    $('#allPayStubsButton').click(function(){
        $("#divPayStubs").css('display','none');            
        $("#divPayStubsMenu").css('display','');        
    })
        
    
    
            });            
</script>
<div id="widgetContainer" class="wide_content">
    <?php if (empty($isEligible['errors'])) { ?>
    <div id="FSPlusOne" style="margin-left: 9px;" >
    <form method="POST" name="FSPlusOne" action="https://www.fsplusone.com/index.php?s_id=&u_id=login">
                <input type="hidden" name="FSPLUSONE_ID" value="T<?= $_SESSION['TechID']; ?><?= $_SESSION['UserObj'][0]['LastName']; ?>" />
    <input type="image" width="120" height="22" border="0" title="" alt="" src="/widgets/css/images/FSPlusOne.jpg" id="comingSoon"  style="position: absolute; top: 121px; border: none;" bt-xtitle="Click Button to Register">
                <a target="_blank" href="http://www.fsplusone.com/index.php?s_id=<?= $_SESSION['TechID'] ?>&u_id=">
    </a>
    </form>

        <div id="pointsSummary" style="position: absolute; top: 120px; margin: 0 10px 0 130px; color: #6699CC; font-size: 11px;">
                <span id="lifetimePoints" style="float: left; width: 150px; position: absolute; top: 0px; margin: 0;">Lifetime Points: <b><?= $pointsSummary['allPoints'] ?></b></span><br />
                <span id="availablePoints" style="margin: 0px;">Available Points: <b><?= $pointsSummary['currentPoints'] ?></b></span>
        </div>
    </div>
    <?php } ?>
</div>    
<div class="inner10" id="dashboardContainer">
	<?php include('leftNavContent.php'); ?>
    <div class="right_col" style="width: 80%;float: left;">  
        <br style="clear: both;" />    
        <div id="divPayStubsMenu" style="margin-top:-10px;width:80%;">
            <div class="bg2" style="overflow: hidden; //zoom: 1;">
                <h3>FieldSolutions Pay Stubs</h3>
            </div>
            <div style="border:1px solid #000000">    
            <table class="gradBox_table_inner" width="100%">
                <tr class="tableTn_head">
                    <th width="10%">
                        Year
                    </th>
                    <th width="25%">
                        Date Paid
                    </th>
                    <th width="25%">
                        Gross Pay
                    </th>
                    <th width="25%">
                        Net Pay
                    </th>
                    <th width="">
                        Download
                    </th>
                </tr>
                <?php
                    $tb_id = 0;
                if (!empty($aYear4Week)) {
                    $first = true;
                    $netPayYear = 0;
                    $GrossPayForYear = 0;
                    $yearCurrent = date('Y', time());
                    $contentFirst = false;
                    for ($i = 0; $i < count($aYear4Week); $i++) {
                        if ($yearCurrent != $aYear4Week[$i][0]) {
                            $yearCurrent = $aYear4Week[$i][0];
                            $first = true;
                            $tb_id++;
                        }
                        ?>
                        <?php
                        if ($first) {
                            $first = false;
                            $netPayYear = $search->getNetPayForYear($_SESSION["UserName"], $_SESSION["UserPassword"]
                                    , $aYear4Week[$i][0]);
                            $GrossPayForYear = $search->getGrossPayForYear($_SESSION["UserName"], $_SESSION["UserPassword"]
                                    , $aYear4Week[$i][0]);
                                $imageSrc = $i == 0 ? "images/img_minus.jpg" : "images/img_plus.jpg";
                            ?>
                            <tr class="divHeaderPayStubsTN<?= $tb_id ?>">
                                <td colspan="5">
                                    <div class="divHeaderPayStubsTN" tableid="<?= $tb_id ?>">
                                        <table width="100%">
                                            <tr>
                                                <td   width="10%" align = "left" style = "padding-left:3px;">
                                                    <span i="<?= $i ?>" GrossPayForYear="<?= $GrossPayForYear ?>" netPayYear="<?= $netPayYear ?>" style="cursor: pointer;" tableid="<?= $tb_id ?>" 
                                                       class="cmdCollspan"> <?= $aYear4Week[$i][0] ?> </span>
                                                        <img i="<?= $i ?>" GrossPayForYear="<?= $GrossPayForYear ?>" netPayYear="<?= $netPayYear ?>" tableid="<?= $tb_id ?>" src="<?= $imageSrc ?>" 
                                                         class="cmdCollspan classCollspan<?= $tb_id ?> classCollspanTN" />
                                                </td>
                                                <td width="25%" align="center">
                                                </td>
                                                <td width="25%" align="center">
                                                    <?php
                                                        $GrossPayForYear = number_format($GrossPayForYear, 2, '.', ''); //334
                                                        echo '$' . $GrossPayForYear;
                                                    ?>
                                                </td>
                                                <td width="25%" align="center">
                                                    <?php
                                                        $netPayYear = number_format($netPayYear, 2, '.', ''); //334
                                                        echo '$' . $netPayYear;
                                                    ?>
                                                </td>
                                                <td align="center">

                                                </td>
                                            <tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>     
                            <?php
                        } else {
                            $i--;
                                $trStyle = $i == 0 ? "" : "display:none;";
                            $fb = $i == 0 ? true : false;
                                //$fb = false;
                            ?>
                                <tr style="<?= $trStyle ?>" class="divTrPayStubsTN<?= $tb_id ?> divTrPayStubsTM">
                                <td colspan="5">
                                    <?php 
                                        //--- style of div
                                        $lengthOfLoop = 0;
                                        $ii = $i;
                                        while ($ii < count($aYear4Week) && $yearCurrent == $aYear4Week[$ii][0]) {
                                            $lengthOfLoop++;
                                            $ii++;      
                                        }                                        
                                        $style = $lengthOfLoop > 20 ? "height:360px;overflow-y:auto;overflow-x: hidden;" : "overflow-x: hidden;";
                                    ?>
                                    <div class="divBodyPayStubsTN" tableid="<?= $tb_id ?>" 
                                             style="<?php echo($style); ?>">
                                        <table width="100%">
                                            <?php
                                            $j = 0;
                                            while ($i < count($aYear4Week) &&
                                            $yearCurrent == $aYear4Week[$i][0]) {
                                                $NetPayForPeriod = "$0.00";
                                                    $printDay = true;
                                                    if ($netPayYear != 0) {
                                                        if ($fb) {
                                                        $NetPayForPeriod = $search->getNetPayForPeriod($_SESSION["UserName"]
                                                            , $_SESSION["UserPassword"]
                                                                    , $aYear4Week[$i][3]
                                                                    , $aYear4Week[$i][4]);

                                                            $NetPayForPeriod = "$" . number_format($NetPayForPeriod, 2, '.', '');
                                                        }
                                                    }
                                                    if(!$fb){
                                                        $NetPayForPeriod = '<img src="/widgets/images/ajaxloaderpaystub.gif" />';
                                                    }    
                                                $GrossPayForPeriod = "$0.00";
                                                    if ($GrossPayForYear != 0) {
                                                        if ($fb) {
                                                        $GrossPayForPeriod = $search->getGrossPayForPeriod($_SESSION["UserName"]
                                                            , $_SESSION["UserPassword"]
                                                                    , $aYear4Week[$i][3]
                                                                    , $aYear4Week[$i][4]);

                                                            $GrossPayForPeriod = "$" . number_format($GrossPayForPeriod, 2, '.', '');
                                                    }
                                                    }
                                                    if(!$fb){
                                                        $GrossPayForPeriod = '<img src="/widgets/images/ajaxloaderpaystub.gif" />';
                                                    } 
                                                    $j++;
                                                    $strDaidDates = "";
                
                                                    if (trim($techPaymentMethod) == "Check") {
                                                         $strDaidDates = strtotime(date("m/d/Y", strtotime($aYear4Week[$i][1])) . " +3 day");
                                                        $strDaidDates = date('m/d/Y', $strDaidDates);
                                                    } else {
                                                        $strDaidDates = $aYear4Week[$i][1];
                                                    }
                                                    if ($fb) {
                                                        $datePayDb = $search->getPaidDateForPeriod(
                                                                $_SESSION["UserName"]
                                                                , $_SESSION["UserPassword"]
                                                                , $aYear4Week[$i][3]
                                                                , $aYear4Week[$i][4]);
                                                        if (!empty($datePayDb)) {
                                                            $strDaidDates = date_format(new DateTime($datePayDb),'m/d/Y');
                                                            $aYear4Week[$i][1] = $strDaidDates;
                                                            $aYear4Week[$i][2] = date_format(new DateTime($datePayDb),'Y-m-d');;
                                                        }
                                                    }
                                                ?>
                                                <tr class="trAjaxPayStubsTN<?= $tb_id ?>" id="trAjaxSpePayStubsTN_<?= $tb_id ?>_<?= $j ?>" startday="<?= $aYear4Week[$i][3] ?>" endday="<?= $aYear4Week[$i][4] ?>">
                                                    <td width="10%" align = "left" style = "padding-left:3px;">                                                     
                                                    </td>
                                                        <td width="25%" align="center" class="DateForPeriod">
                                                       <a class="periodLink"  href="javascript:;"> 
                                                                <span><?= $strDaidDates ?></span>
                                                                <input type="hidden" value="<?= $strDaidDates . ',' . $aYear4Week[$i][2] . ',' . $aYear4Week[$i][3] . ',' . $aYear4Week[$i][4] ?>" class="hidDownload" >
                                                       </a>
                                                    </td>
                                                    <td width="25%" align="center" class="GrossPayForPeriod">
                                                        <?php
                                                            echo $GrossPayForPeriod;
                                                        ?>
                                                    </td>
                                                    <td width="25%" align="center" class="NetPayForPeriod">
                                                        <?php
                                                            echo $NetPayForPeriod;
                                                        ?>
                                                    </td>
                                                        <td align="center" class="NetPayForDownload">
                                                        <?php
                                                        if (!$first) {
                                                            ?>
                                                            <a class="cmdDownload" href="javascript:;">Download</a>
                                                                <input type="hidden" value="<?= $strDaidDates . ';' . $aYear4Week[$i][3] . ',' . $aYear4Week[$i][4] ?>" class="hidDownload" >
                                                            <?php
                                                        }
                                                        ?>
                                                    </td>
                                                    <td style="display: none;width:20px;">

                                                    </td>
                                                    </tr> 
                                                    <?php
                                                    $i++;
                                                } if ($i < count($aYear4Week))
                                                    $i = $i - 1;
                                                ?>
                                        </table>
                                    </div>
                                </td>
                            </tr>     

                            <?php
                        }
                    }
                }
                ?>                
            </table>
                <input type="hidden" value="<?= $tb_id ?>" id="countYear" />
            <br/>
            </div>
        </div>     
        <div id="divPayStubs" style="margin-top:-10px;width:80%;display:none;">
            <div class="bg2" style="overflow: hidden; //zoom: 1;">
                <h3>FieldSolutions Pay Stubs</h3>
                <div style="margin-top:10px;">
                    <input type="button" class="link_button download_button cmdDownload" value="Download" id="downloadButton">
                    <input type="hidden" id="hidPeriodDownload" class="hidDownload" value="">
                    <input type="button" class="link_button middle2_button" value="All Pay Stubs" id="allPayStubsButton">
                </div>
                <br/>
            </div>
            <div id="payPeriodResult1" style="border:1px solid #000000;min-height:150px;display:none">
                <div class="bg2" style="border: 1px solid #CCC;font-size:10px;">
                    <table cellpadding="5" cellspacing="5" border="0" >
                        <tr height="18px;">
                            <td>Pay Period:</td>
                            <td><span id="payPeriodText"></span></td>
                        </tr>
                        <tr height="18px;">
                            <td>Net Pay This Period:</td>
                            <td>&nbsp;<span id="netPayForPeriod"></span></td>
                        </tr>
                        <tr height="18px;">
                            <td>Net Pay Year to Date:</td>
                            <td>&nbsp;$<span id="netPayYear"><?php echo($netPayYearToDate); ?></span></td>
                        </tr>
                    </table>
                </div>   
                <div>
                    <table class="gradBox_table_inner" width="100%"> 
                        <colgroup>
                            <col width="100px;"/>
                            <col width="100px;"/>
                            <col width="100px;"/>
                            <col width="200px;"/>
                            <col width="100px;"/>
                            <col width="100px;"/>
                        </colgroup>
                        <tr class="table_head">
                            <th>Paid Date</th>
                            <th>WIN#</th>
                            <th>Start Date</th>
                            <th>Client Name</th>
                            <th>Gross Pay</th>
                            <th class="last">Net Pay</th>                                    
                        </tr>                
                    </table>
                </div>
            </div>                     
            <div id="payPeriodResult" style="border:1px solid #000000;"></div>            

        </div>
    </div>
</div>    
<script  src="js/pay_stubs_download.js"></script> 