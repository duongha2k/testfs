<?php

require_once ("../../../headerStartSession.php");
$loggedIn = (!empty($_SESSION['loggedIn']) ) ? $_SESSION['loggedIn'] : 'no';
$msg = "";
$result = array("success" => "0", "msg" => "");
if ($loggedIn !== 'yes') {
    $msg = 'http' . ($_SERVER['HTTPS'] == 'on' ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . '/techs/logIn.php?flsupport=true';
    echo json_encode($result);
    die;
}
require ("../../../library/mySQL.php");
$q1 = $_POST['question1'];
$q2 = $_POST['question2'];
$q3 = $_POST['question3'];
$q4 = $_POST['question4'];
$q5 = $_POST['question5'];
$q6 = $_POST['question6'];
$q7 = $_POST['question7'];
$q8 = $_POST['question8'];
$q9 = $_POST['question9'];
$q10 = $_POST['question10'];
$q11 = $_POST['question11'];
$q12 = $_POST['question12'];
$q13 = $_POST['question13'];
$q14 = $_POST['question14'];
$q15 = $_POST['question15'];

// These are the correct answers to key off of:

$a1 = 3;
$a2 = 2;
$a3 = 1;
$a4 = 3;
$a5 = 3;
$a6 = 2;
$a7 = 1;
$a8 = 2;
$a9 = 3;
$a10 = 1;
$a11 = 2;
$a12 = 1;
$a13 = 3;
$a14 = 1;
$a15 = 4;

// Tally Score:
$points = 0;
$Wrong = '';

$points = ($q1 == $a1) ? ++$points : $points;
$points = ($q2 == $a2) ? ++$points : $points;
$points = ($q3 == $a3) ? ++$points : $points;
$points = ($q4 == $a4) ? ++$points : $points;
$points = ($q5 == $a5) ? ++$points : $points;
$points = ($q6 == $a6) ? ++$points : $points;
$points = ($q7 == $a7) ? ++$points : $points;
$points = ($q8 == $a8) ? ++$points : $points;
$points = ($q9 == $a9) ? ++$points : $points;
$points = ($q10 == $a10) ? ++$points : $points;
$points = ($q11 == $a11) ? ++$points : $points;
$points = ($q12 == $a12) ? ++$points : $points;
$points = ($q13 == $a13) ? ++$points : $points;
$points = ($q14 == $a14) ? ++$points : $points;
$points = ($q15 == $a15) ? ++$points : $points;

$Wrong = ($q1 != $a1) ? $Wrong . "#1<br />" : $Wrong;
$Wrong = ($q2 != $a2) ? $Wrong . "#2<br />" : $Wrong;
$Wrong = ($q3 != $a3) ? $Wrong . "#3<br />" : $Wrong;
$Wrong = ($q4 != $a4) ? $Wrong . "#4<br />" : $Wrong;
$Wrong = ($q5 != $a5) ? $Wrong . "#5<br />" : $Wrong;
$Wrong = ($q6 != $a6) ? $Wrong . "#6<br />" : $Wrong;
$Wrong = ($q7 != $a7) ? $Wrong . "#7<br />" : $Wrong;
$Wrong = ($q8 != $a8) ? $Wrong . "#8<br />" : $Wrong;
$Wrong = ($q9 != $a9) ? $Wrong . "#9<br />" : $Wrong;
$Wrong = ($q10 != $a10) ? $Wrong . "#10<br />" : $Wrong;
$Wrong = ($q11 != $a11) ? $Wrong . "#11<br />" : $Wrong;
$Wrong = ($q12 != $a12) ? $Wrong . "#12<br />" : $Wrong;
$Wrong = ($q13 != $a13) ? $Wrong . "#13<br />" : $Wrong;
$Wrong = ($q14 != $a14) ? $Wrong . "#14<br />" : $Wrong;
$Wrong = ($q15 != $a15) ? $Wrong . "#15<br />" : $Wrong;

$questions = 15;

$score = round(($points / $questions) * 100);

$TechID = $_SESSION['TechID'];
$DateTime = date("Y-m-d H:i:s");

mysqlQuery("INSERT INTO ComputersPlus_Test (tech_id, date_taken, score, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15) VALUES ('$TechID', '$DateTime', '$score', '$q1', '$q2', '$q3', '$q4', '$q5', '$q6', '$q7', '$q8', '$q9', '$q10', '$q11', '$q12', '$q13', '$q14', '$q15')");



if ($score == 100) {
    mysqlQuery("DELETE FROM tech_certification WHERE TechID='$TechID' AND certification_id='24'");
    mysqlQuery("INSERT INTO tech_certification (TechID,certification_id,date) VALUES ('$TechID','24','$DateTime')");
    $result["success"] = "1";
} else {
    $result["success"] = "2";
    $result["msg"] = $score."%";
}
echo json_encode($result);
die;
?>
