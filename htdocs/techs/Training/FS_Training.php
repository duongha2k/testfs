<?php $page = "techs"; ?>
<?php $option = "training"; ?>
<?php $selected = "fs" ?>
<?php $display = "fs" ?>
<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>

<!-- Add Content Here -->

<br /><br />

<div align="center">

<TABLE cellSpacing=0 cellPadding=0 width=700 border=0 style="text-align:left">
<TBODY>
<TR>
<TD colSpan=2><BR><FONT face=Arial size=4><B>How Field Solutions Works</B></FONT> 
<BR>
<FONT face=Arial size=3>What each Tech needs to know</FONT> 
<P>
<FONT face=Arial size=2>
<B>Note:</B> FLS is a major client of Field Solutions and handles things a bit differently and separately from all other clients and the main site. Therefore, please pay attention to the differences between "FLS" How-To instructions and "FIELDSOLUTIONS" (main site) How-To instructions. 
<P>
<HR>
<P>
All Techs are required to go through each section listed below so that you fully understand how Field Solutions works. Eventually there will be a 12-question that you will be required to pass and become fully registered with Field Solutions. Prior to the 12-question test being implemented covering these instructions, techs will be eligible to perform work for clients via Field Solutions once they have fully registered and have agreed to all Field Solutions terms.

<P>
<FONT face=Arial size=3><B>How-To Instructions</B> </FONT> 
<P>
<B>1</B>) <A href="FS_Works.php" target="_blank">How Field Solutions works</A>
<P>
<B>2</B>) <a href="FS_AvailableWork.php" target="_blank">AVAILABLE WORK - How to apply for/bid on open work orders</a>
<P>
<B>3</B>) <a href="FS_AssignedWork.php" target="_blank">ASSIGNED WORK - How to view/search for work orders assigned to you </A>
<P>
<B>4</B>) <a href="FS_AppliedWork.php" target="_blank">  MY APPLIED WOS - How to view/search/modify work orders you have applied for/bid on
</a>
<P>
<B>5</B>) <a href="FS_CompleteWork.php" target="_blank">How to complete your work orders online - The work order completion, approval, payment  process</A>
<P>
<B>6</B>) <a href="FS_PaymentPolicy.php" target="_blank">Field Solutions Payment Policy</A>
<P>
<B>7</B>) <a href="FS_DirectDeposit.php" target="_blank">Direct Deposit</A>
<P>
<B>8</B>) <a href="FS_AutoEmails.php" target="_blank">Auto-emails from Field Solutions</A>
<P>
<B>9</B>) <a href="FS_FLS_Program.php" target="_blank">The FLS Program/Client</a>
<P>
<B>10</B>) <a href="FS_TechnicianSupport.php" target="_blank">Field Solutions Technician Support</A>
<P>
<B>11)</B> <a href="How to Accept Work Orders.pdf" target="_blank">How to Accept Work Orders</a>
<P>
<B>12)</B> <a href="How to Confirm Work Orders.pdf" target="_blank">How to Confirm Work Orders</a>
<P>
<B>13)</B> <a href="How to Modify Bids Applied Work Orders.pdf" target="_blank">How to Modify Bids of Applied Work Orders
</a>
<P>
</FONT></TD></TR></TBODY></TABLE>



</div>

<!--- End Content --->
<?php require ("../../footer.php"); ?>
		
