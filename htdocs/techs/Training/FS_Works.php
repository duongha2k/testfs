<?php $page = "techs"; ?>
<?php $option = "training"; ?>
<?php $selected = "fs" ?>
<?php $display = "fs" ?>
<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>

<!-- Add Content Here -->

<br /><br />
<div align="center">

<TABLE cellSpacing=0 cellPadding=0 width=700 border=0 style="text-align:left">
<TBODY>
<TR>
<TD colSpan=2><BR><FONT face=Arial size=4><B>How Field Solutions works for the technician</B></FONT> 
<P>
<FONT face=Arial size=2>
The main entities (companies/individuals) involved with Field Solutions:
<P>
1) Several clients (companies offering install/work opportunities)   
<P>
2) You, the Technician
<P>
3) Field Solutions staff
<P>
Field Solutions alone does not offer install/work opportunities. It is the individual clients affiliated with Field Solutions who have the need for field service technicians, which they access via www.fieldsolutions.com. 
<P>

<FONT face=Arial size=3><B>Getting involved with install/work opportunities via Field Solutions</B></FONT>
<P>
<FONT face=Arial size=2>
It all starts with you, the technician, registering with Field Solutions and going through all of the online How-To instructions, which you are doing (possibly in part) right now. 
<P>
Once registered with Field Solutions, you are considered an independent technician (contractor) and not an employee of Field Solutions or any clients of Field Solutions.
<P>
There are basically two different ways for field service technicians to utilize the services of Field Solutions to help you find install/work opportunities - 
<P>
<B> 1) </B> Techs are encouraged to proactively view, apply for/bid on work orders. To do so, login to your Field Solutions account/profile and click on the "Work Orders" tab towards the top of the page. See other training sections for more detail on how to search and process work orders. 
<P>
<B> 2) </B>  If/when your services are needed, Field Solutions clients and/or staff will contact you directly (usually via email) after accessing your Field Solutions profile as a result of you registering with Field Solutions.

<P>
There is no downside to being registered with Field Solutions. As long as you possess technical installation and/or technical service (break-fix) experience and/or the appropriate skills, you are encouraged to register no matter what your situation/availability is. You can always turn down work if/when a client and/or Field Solutions staff contacts you.
</FONT><P class="nextButton" align="center"><a href="FS_AvailableWork.php">Next</a></TD></TR></TBODY></TABLE>




</div>

<!--- End Content --->
<?php require ("../../footer.php"); ?>
		
