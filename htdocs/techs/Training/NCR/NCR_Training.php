<?php 
	$page = techs;
	//$option = training;
	require ("../../../header.php"); 
	//require ("../../../navBar.php"); 

?>
<script type="text/javascript">

$(document).ready(function(){
	$("body").css("width", "850px").css("min-width", "850px");
});

</script>
<style type="text/css">
#mainContainer{
	min-width: 800px;
	width: 850px;
}
</style>

<div id="NCRTraining" style="width: 700px;">
<br /><br />
<table border="0" width="700">
	<tr>
<td><div><span class="topHeading">NCR - Over a Century</span></div>
  <p><img src="NCR_Century.jpg" width="734" height="422" /></p></td>
	</tr>
	<tr>
	  <td><p>NCR is a publicly held global corporation headquartered in Peachtree City, Georgia U.S.A.</p>
	    <ul>
	      <li>We do business in approximately 130 countries &ndash;  regions
	        <ul>
	          <li> Americas, Europe/Middle East/Africa (EMEA), APC(Asia Pacific) and Japan<br />  
                <br />
                </li>
            </ul>
	      </li>
          <li>Our annual revenue is over 6.1 billion dollars (2006).<br />
            <br />
          </li>
	      <li>We employ 28-29,000 employees around the world.<br />
	        <br />
	      </li>
          <li>NCR's Field Service Delivery workforce is made up of:
            <ul>
              <li>Approx. 6,000 CEs in 130 countries reporting to Territory Managers (TMs)</li>
              <li>Service partners - NCR's subcontractors, who provide on-site service <br />
                <br />
                </li>
            </ul>
          </li>
        </ul>
	    <div><span class="topHeading">Products and Services</span></div>
	    <p><span class="subHeading">Financial Self-Service - </span>Delivering best-in-class self-service solutions to the world&rsquo;s leading financial institutions, with an unrivalled portfolio to suit all transaction volumes and locations. <br />
	      <span class="subHeading">Payment and Imaging Solutions - </span>Designed to help financial institutions and corporations deliver innovative products and services in an ever-changing payments landscape. <br />
	      <span class="subHeading">Point of Sale and Kiosk Solutions - </span>NCR offers self-checkout, self-service kiosks, point-of-sale workstations, scanners, software and services that minimize downtime, maximize productivity and improve customer service. <br />
	      <span class="subHeading">Supplies for the Banking and Retail Industries</span>  - Business consumables for all print technologies and related services to complement NCR's solutions for retail, financial and other industries. <br />
      <span class="subHeading">Services and Support</span> - From ATMs and self-service kiosks to servers and networks, NCR provides a complete portfolio of services fueled by actionable IT intelligence. </p></td>
    </tr>
	<tr>
	  <td><div class="topHeading">Quality Customer Service</div>
      <p><strong>NCR expects their Customer Engineers and Contractors to provide Quality Customer Service - service that is timely, appropriate, and professional. </strong><br />
        <br />
      </p></td>
    </tr>
	<tr>
	  <td class="blockCenter"><img src="Trust_Loyalty.jpg" width="574" height="429" /></td>
    </tr>
	<tr>
	  <td><div><span class="subHeading2">Quality Customer Service consists of:</span></div>
	    <ul>
	      <li>Consistently and constantly giving our customers what they want and need</li>
          <li>Providing appropriate services in a timely manner</li>
	      <li>Maintaining a professional demeanor during customer interactions<br />
	      </li>
	    </ul>
	    <p align="center"><strong>In other words &ndash; being Customer Centric</strong></p>
	    <div><span class="subHeading2">Being Customer Centric means:</span></div>
	    <ul>
	      <li>Doing everything you can to please the Customer. (while staying within recommended guidelines.)</li>
          <li>Presenting yourself as a professional &ndash; all the time </li>
          <li>Being the kind of person the Customer enjoys doing business with.</li>
          <li>Realizing that everything you do or say affects the Customer&rsquo;s perception of the company. Make sure your words and actions provide a positive professional image of both you and NCR.</li>
          <li>Giving the Customer personal attention, and considering the Customer&rsquo;s problem as your own.</li>
          <li>Being able to view the situation from the Customer&rsquo;s perspective.</li>
          <li>Being knowledgeable, and when necessary, knowing when to call upon someone else or find another approach to solve the problem.</li>
          <li>Demonstrating commitment and respect for both the Customer&rsquo;s situation and for NCR (as a company).</li>
          <li>Demonstrating a sense of urgency and understanding, getting the situation resolved in a timely manner.</li>
      </ul></td>
    </tr>
	<tr>
	  <td><p style="font-weight: bold">On site, you are NCR to our Customer and you should always act in a professional manner.</p>
        <table id="posNegTable" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top" style="background-color: #DDDDDD"><img style="width: auto; height: 40px" src="thumbsup.gif" /><span style="font-weight: bold">Positive:</span></td>
            <td valign="top" style="background-color: #DDDDDD"><img style="width: auto; height: 40px" src="thumbsdown.gif" /><span style="font-weight: bold">Negative:</span></td>
          </tr>
          <tr>
            <td valign="top"><ul>
              <li>Upon arrival at the site, find the manager (manager on-duty, CSM, etc.) and introduce yourself.  Shake their hand if appropriate, and tell them you are there to work on the equipment. </li>
            </ul></td>
            <td valign="top"><ul>
              <li>Upon arrival at the site, ask the employees you encounter what they think must be done and begin working on the equipment.</li>
            </ul></td>
          </tr>
          <tr>
            <td valign="top"><ul>
              <li>Wear clothes that are neat, free of stains, rips, or wrinkles.  (see the attached Dress Code for more details)</li>
            </ul></td>
            <td valign="top"><ul>
              <li>Dress sloppily and have unkempt hair. </li>
            </ul></td>
          </tr>
          <tr>
            <td valign="top"><ul>
              <li>Keep your work area neat (tools not lying all around the work area; wipe down the equipment after working on it). </li>
            </ul></td>
            <td valign="top"><ul>
              <li>Have tools strewn all around the work area.</li>
            </ul></td>
          </tr>
          <tr>
            <td valign="top"><ul>
              <li>Notify the Customer Manager if you notice any hazards at the site (if you see a water spill or something where a person could get hurt, courteously make them aware of the situation).</li>
            </ul></td>
            <td valign="top"><ul>
              <li>If asked a question that you do not know, respond by saying &ldquo;I&rsquo;m not trained on this.&rdquo;  &ldquo;I don't know how to fix it.&rdquo;</li>
            </ul></td>
          </tr>
          <tr>
            <td valign="top"><ul>
              <li>Answer questions confidently, even if you don't know the answer; respond in a confident way, &ldquo;I&rsquo;ll call my support team for additional guidance on this issue.&rdquo;  (DO NOT SAY &ndash; &ldquo;I&rsquo;m not trained on this.&rdquo;  &ldquo;I don't know how to fix it.&rdquo;)</li>
            </ul></td>
            <td valign="top" class="blockCenter"><img src="toolbox.png" style="height: 100px; width: auto"/></td>
          </tr>
          <tr>
            <td valign="top">&nbsp;</td>
            <td valign="top"><ul>
              <li>Discussions concerning pay are strictly prohibited while at the customer site.  All questions concerning pay need to be directed to your Field Solutions Contact.</li>
            </ul></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>        <p>&nbsp;</p></td>
    </tr>
	<tr>
	  <td><p align="center" style="font-weight: bold">The Dress Code<br />
	  </p>      </td>
    </tr>
	<tr>
	  <td><span style="font-weight: bold">A key element of a professional image is appearance.  Because your job brings you into daily contact with our Customers, you are expected to follow &ldquo;business casual&rdquo; guidelines.  NCR Customer Services defines business casual this way:<br />
	    <br />
	  </span></td>
    </tr>
	<tr>
	  <td><p style="font-weight: bold">Male employees</p>
	    <ul>
	      <li> Wear casual business dress slacks and dress or polo shirts, with dress or business casual shoes. All shirts must have a collar. </li>
          <li>Shoes should be brushed or polished as required.</li>
          <li>Boots may be worn if they are acceptable to local custom, are designed as dress boots, and are polished. </li>
          <li>Shirts should be kept tucked into the pants.</li>
          <li>Socks and belts are required as part of business casual attire.</li>
        </ul>
	    <p style="font-weight: bold">Female employees </p>
	    <ul>
	      <li>Wear business casual dress slacks, skirts, suits, or dresses, with business casual shirts/blouses and shoes appropriate for business attire.</li>
          <li>Shirts not designed to be worn externally should be kept tucked in. </li>
	      <li>Shoes should be brushed or polished as required.</li>
        </ul>
	    <p style="font-weight: bold">Male and Female employees</p>
	    <ul>
	      <li>Clothing must be free of stains, tears, or other blemishes, and should be ironed for a neat appearance.</li>
          <li>Hats should be removed while indoors.</li>
	      <li>Clothing items considered inappropriate include: </li>
        
	      <ul>
	        <li>Denim (blue jeans, jackets, shirts) </li>
            <li>Sweatpants, sweatshirts, T-shirts and sweat suits</li>
	        <li>Tank tops, halters, or sun dresses</li>
	        <li>Shorts, mini-skirts</li>
	        <li>Athletic shoes, casual sandals<br />
	          <br />
	        </li>
          </ul>
	      <li>All clothing must be free of competitor logos or company names (i.e. IBM, HP, Fujitsu, Diebold, etc.)</li>
	      <li>Any item of clothing which causes undue attention or is a distraction and does not meet the dress standards of our customers should not be worn.</li>
	      <li>Any clothing articles with markings that may be considered offensive are prohibited. </li>
	      <li>Items of jewelry that cause undue attention or a distraction should not be worn. </li>
	      <li>Earrings worn on body parts other than the ears are not acceptable business attire.</li>
	      <li>Hair styles which cause distractions in a customer or business environment are not acceptable.<br />
	        <br />
	        <span style="font-weight: bold">Safety Considerations	        </span>
	        <ul>
	          <li>Jewelry which would subject the wearer to potential safety risks is prohibited. </li>
              <li>Hair of sufficient length should be kept pulled back and tied to prevent injury or entanglement when working</li>
            </ul>
          </li>
	    </ul>	    </td>
    </tr>
	<tr>
	  <td>&nbsp;</td>
    </tr>
	<tr>
	  <td><div align="center"><span style="font-weight: bold">Procedures for Installation Activity</span></div></td>
    </tr>
	<tr>
	  <td><ul>
	    <li>Relevant installation instructions will be provided in the Work Order.  Supplemental installation instruction will be provided to you via email.</li>
        <li>Upon assignment technicians must go to their work order on the Field Solutions&rsquo; web site and review all work order details and documentation.  Please be sure to check the box that shows you have reviewed. </li>
	    <li>Technicians must go to their work order on the Field Solutions&rsquo; web site and confirm assignment 48 hours prior to install date.  Failure to do this may result in the work order being reassigned. Please be sure to check the box that shows you confirm assignment. </li>
	    <li>	      Each technician must take a PRINTED copy of the work order to the customer site.</li>
	    <li>Work order instructions and any additional installation instructions must be reviewed prior to arriving at the customer site.</li>
	    <li>	      Technicians need to carry with them a standard PC toolset to all installations.  Additional tools may be required for specific projects.  It is the responsibility of the technician to provide these tools unless specifically noted.</li>
	    <li>All Technicians are expected to have a working cell phone with them at the installation to enable them to contact support numbers and report status information.</li>
	    <li>All technicians must arrive on-site as scheduled.  Any expected delays must be communicated to the Project Coordinator listed on your work order.</li>
	    <li>Work Instructions/Installation Instructions must be followed including any requirements to check in/check out with the project team or customer help desk.</li>
	    <li>Once the work has been completed, the technician must get the site contact to sign-off on the work order.</li>
	    <li>Work orders need to be closed within 24 hours of completing the install. All site installation deliverables required from the installation, including the signed work order, must be submitted otherwise pay approval may be delayed. </li>
	    <li>If you have any questions on how to complete work orders, please refer back to Field Solutions&rsquo; Web Site help menu for FAQ&rsquo;s. </li>
	  </ul></td>
    </tr>
</table>

</div>

<!-- Add Content Here -->

<style type="text/css">
.topHeading {
	font-size: 14px;
	font-weight: bolder;
	color: #000000;
	font: Arial;
}
.subHeading {
	font-size: 12px;
	font-weight: bolder;
	color: #000000;
	font: Arial;
	text-decoration: underline;
}
.subHeading2 {
	font-size: 12px;
	font-weight: bolder;
	color: #000000;
	font: Arial;
}
.subHeading3 {
	font-size: 12px;
	font-weight: bolder;
	color: #032d5f;
	font: Arial;
	border:thin;
	border-color:#032d5f;
}
#NCRTraining {
	width: 700px;
	margin: 0px auto;
}
#NCRTraining ul, #NCRTraining ul ul {
	margin: 0px;
	padding: 0px 0px 0px 40px;
	margin-bottom: 0px;
}
.blockCenter {
	text-align: center;
}
#NCRTraining li {
}
#posNegTable ul {
	margin-bottom: 5px;
}

</style>

<br /><br />

<div id="NCRTraining">

<table border="0" width="700">
	<tr>
	  <td><span class="subHeading3">This is an open book test for the NCR Basic Certification. You have to take the NCR Basic Certification Test to be certified to do future NCR installations. You can use this document to assist you in taking this test.<br />
	      <br /><hr /></span></td>
	</tr>
	<tr>
<td><div><span class="topHeading">NCR - Over a Century</span></div>
  <p><img src="NCR_Century.jpg" width="734" height="422" /></p></td>
	</tr>
	<tr>
	  <td><p>NCR is a publicly held global corporation headquartered in Peachtree, Georgia, U.S.A.</p>
	    <ul>
	      <li>We do business in approximately 130 countries &ndash;  regions
	        <ul>
	          <li> Americas, Europe/Middle East/Africa (EMEA), APC(Asia Pacific) and Japan<br />  
                <br />
                </li>
            </ul>
	      </li>
          <li>Our annual revenue is over 6.1 billion dollars (2006).<br />
            <br />
          </li>
	      <li>We employ 28-29,000 employees around the world.<br />
	        <br />
	      </li>
          <li>NCR's Field Service Delivery workforce is made up of:
            <ul>
              <li>Approx. 6,000 CEs in 130 countries reporting to Territory Managers (TMs)</li>
              <li>Service partners - NCR's subcontractors, who provide on-site service <br />
                <br />
                </li>
            </ul>
          </li>
        </ul>
	    <div><span class="topHeading">Products and Services</span></div>
	    <p><span class="subHeading">Financial Self-Service - </span>Delivering best-in-class self-service solutions to the world&rsquo;s leading financial institutions, with an unrivalled portfolio to suit all transaction volumes and locations. <br />
	      <span class="subHeading">Payment and Imaging Solutions - </span>Designed to help financial institutions and corporations deliver innovative products and services in an ever-changing payments landscape. <br />
	      <span class="subHeading">Point of Sale and Kiosk Solutions - </span>NCR offers self-checkout, self-service kiosks, point-of-sale workstations, scanners, software and services that minimize downtime, maximize productivity and improve customer service. <br />
	      <span class="subHeading">Supplies for the Banking and Retail Industries</span>  - Business consumables for all print technologies and related services to complement NCR's solutions for retail, financial and other industries. <br />
      <span class="subHeading">Services and Support</span> - From ATMs and self-service kiosks to servers and networks, NCR provides a complete portfolio of services fueled by actionable IT intelligence. </p></td>
    </tr>
	<tr>
	  <td><div class="topHeading">Quality Customer Service</div>
      <p><strong>NCR expects their Customer Engineers and Contractors to provide Quality Customer Service - service that is timely, appropriate, and professional. </strong><br />
        <br />
      </p></td>
    </tr>
	<tr>
	  <td class="blockCenter"><img src="Trust_Loyalty.jpg" width="574" height="429" /></td>
    </tr>
	<tr>
	  <td><div><span class="subHeading2">Quality Customer Service consists of:</span></div>
	    <ul>
	      <li>Consistently and constantly giving our customers what they want and need</li>
          <li>Providing appropriate services in a timely manner</li>
	      <li>Maintaining a professional demeanor during customer interactions<br />
	      </li>
	    </ul>
	    <p align="center"><strong>In other words &ndash; being Customer Centric</strong></p>
	    <div><span class="subHeading2">Being Customer Centric means:</span></div>
	    <ul>
	      <li>Doing everything you can to please the Customer. (while staying within recommended guidelines.)</li>
          <li>Presenting yourself as a professional &ndash; all the time </li>
          <li>Being the kind of person the Customer enjoys doing business with.</li>
          <li>Realizing that everything you do or say affects the Customer&rsquo;s perception of the company. Make sure your words and actions provide a positive professional image of both you and NCR.</li>
          <li>Giving the Customer personal attention, and considering the Customer&rsquo;s problem as your own.</li>
          <li>Being able to view the situation from the Customer&rsquo;s perspective.</li>
          <li>Being knowledgeable, and when necessary, knowing when to call upon someone else or find another approach to solve the problem.</li>
          <li>Demonstrating commitment and respect for both the Customer&rsquo;s situation and for NCR (as a company).</li>
          <li>Demonstrating a sense of urgency and understanding, getting the situation resolved in a timely manner.</li>
      </ul></td>
    </tr>
	<tr>
	  <td><p style="font-weight: bold">On site, you are NCR to our Customer and you should always act in a professional manner.</p>
        <table id="posNegTable" width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td valign="top" style="background-color: #DDDDDD"><img style="width: auto; height: 40px" src="thumbsup.gif" /><span style="font-weight: bold">Positive:</span></td>
            <td valign="top" style="background-color: #DDDDDD"><img style="width: auto; height: 40px" src="thumbsdown.gif" /><span style="font-weight: bold">Negative:</span></td>
          </tr>
          <tr>
            <td valign="top"><ul>
              <li>Upon arrival at the site, find the manager (manager on-duty, CSM, etc.) and introduce yourself.  Shake their hand if appropriate, and tell them you are there to work on the equipment. </li>
            </ul></td>
            <td valign="top"><ul>
              <li>Upon arrival at the site, ask the employees you encounter what they think must be done and begin working on the equipment.</li>
            </ul></td>
          </tr>
          <tr>
            <td valign="top"><ul>
              <li>Wear clothes that are neat, free of stains, rips, or wrinkles.  (see the attached Dress Code for more details)</li>
            </ul></td>
            <td valign="top"><ul>
              <li>Dress sloppily and have unkempt hair. </li>
            </ul></td>
          </tr>
          <tr>
            <td valign="top"><ul>
              <li>Keep your work area neat (tools not lying all around the work area; wipe down the equipment after working on it). </li>
            </ul></td>
            <td valign="top"><ul>
              <li>Have tools strewn all around the work area.</li>
            </ul></td>
          </tr>
          <tr>
            <td valign="top"><ul>
              <li>Notify the Customer Manager if you notice any hazards at the site (if you see a water spill or something where a person could get hurt, courteously make them aware of the situation).</li>
            </ul></td>
            <td valign="top"><ul>
              <li>If asked a question that you do not know, respond by saying &ldquo;I&rsquo;m not trained on this.&rdquo;  &ldquo;I don't know how to fix it.&rdquo;</li>
            </ul></td>
          </tr>
          <tr>
            <td valign="top"><ul>
              <li>Answer questions confidently, even if you don't know the answer; respond in a confident way, &ldquo;I&rsquo;ll call my support team for additional guidance on this issue.&rdquo;  (DO NOT SAY &ndash; &ldquo;I&rsquo;m not trained on this.&rdquo;  &ldquo;I don't know how to fix it.&rdquo;)</li>
            </ul></td>
            <td valign="top" class="blockCenter"><img src="toolbox.png" style="height: 100px; width: auto"/></td>
          </tr>
          <tr>
            <td valign="top">&nbsp;</td>
            <td valign="top"><ul>
              <li>Discussions concerning pay are strictly prohibited while at the customer site.  All questions concerning pay need to be directed to your Field Solutions Contact.</li>
            </ul></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>        <p>&nbsp;</p></td>
    </tr>
	<tr>
	  <td><p align="center" style="font-weight: bold">The Dress Code<br />
	  </p>      </td>
    </tr>
	<tr>
	  <td><span style="font-weight: bold">A key element of a professional image is appearance.  Because your job brings you into daily contact with our Customers, you are expected to follow &ldquo;business casual&rdquo; guidelines.  NCR Customer Services defines business casual this way:<br />
	    <br />
	  </span></td>
    </tr>
	<tr>
	  <td><p style="font-weight: bold">Male employees</p>
	    <ul>
	      <li> Wear casual business dress slacks and dress or polo shirts, with dress or business casual shoes. All shirts must have a collar. </li>
          <li>Shoes should be brushed or polished as required.</li>
          <li>Boots may be worn if they are acceptable to local custom, are designed as dress boots, and are polished. </li>
          <li>Shirts should be kept tucked into the pants.</li>
          <li>Socks and belts are required as part of business casual attire.</li>
        </ul>
	    <p style="font-weight: bold">Female employees </p>
	    <ul>
	      <li>Wear business casual dress slacks, skirts, suits, or dresses, with business casual shirts/blouses and shoes appropriate for business attire.</li>
          <li>Shirts not designed to be worn externally should be kept tucked in. </li>
	      <li>Shoes should be brushed or polished as required.</li>
        </ul>
	    <p style="font-weight: bold">Male and Female employees</p>
	    <ul>
	      <li>Clothing must be free of stains, tears, or other blemishes, and should be ironed for a neat appearance.</li>
          <li>Hats should be removed while indoors.</li>
	      <li>Clothing items considered inappropriate include: </li>
        
	      <ul>
	        <li>Denim (blue jeans, jackets, shirts) </li>
            <li>Sweatpants, sweatshirts, T-shirts and sweat suits</li>
	        <li>Tank tops, halters, or sun dresses</li>
	        <li>Shorts, mini-skirts</li>
	        <li>Athletic shoes, casual sandals<br />
	          <br />
	        </li>
          </ul>
	      <li>All clothing must be free of competitor logos or company names (i.e. IBM, HP, Fujitsu, Diebold, etc.)</li>
	      <li>Any item of clothing which causes undue attention or is a distraction and does not meet the dress standards of our customers should not be worn.</li>
	      <li>Any clothing articles with markings that may be considered offensive are prohibited. </li>
	      <li>Items of jewelry that cause undue attention or a distraction should not be worn. </li>
	      <li>Earrings worn on body parts other than the ears are not acceptable business attire.</li>
	      <li>Hair styles which cause distractions in a customer or business environment are not acceptable.<br />
	        <br />
	        <span style="font-weight: bold">Safety Considerations	        </span>
	        <ul>
	          <li>Jewelry which would subject the wearer to potential safety risks is prohibited. </li>
              <li>Hair of sufficient length should be kept pulled back and tied to prevent injury or entanglement when working</li>
            </ul>
          </li>
	    </ul>	    </td>
    </tr>
	<tr>
	  <td>&nbsp;</td>
    </tr>
	<tr>
	  <td><div align="center"><span style="font-weight: bold">Procedures for Installation Activity</span></div></td>
    </tr>
	<tr>
	  <td><ul>
	    <li>Relevant installation instructions will be provided in the Work Order.  Supplemental installation instruction will be provided to you via email.</li>
        <li>Upon assignment technicians must go to their work order on the Field Solutions&rsquo; web site and review all work order details and documentation.  Please be sure to check the box that shows you have reviewed. </li>
	    <li>Technicians must go to their work order on the Field Solutions&rsquo; web site and confirm assignment 48 hours prior to install date.  Failure to do this may result in the work order being reassigned. Please be sure to check the box that shows you confirm assignment. </li>
	    <li>	      Each technician must take a PRINTED copy of the work order to the customer site.</li>
	    <li>Work order instructions and any additional installation instructions must be reviewed prior to arriving at the customer site.</li>
	    <li>	      Technicians need to carry with them a standard PC toolset to all installations.  Additional tools may be required for specific projects.  It is the responsibility of the technician to provide these tools unless specifically noted.</li>
	    <li>All Technicians are expected to have a working cell phone with them at the installation to enable them to contact support numbers and report status information.</li>
	    <li>All technicians must arrive on-site as scheduled.  Any expected delays must be communicated to the Project Coordinator listed on your work order.</li>
	    <li>Work Instructions/Installation Instructions must be followed including any requirements to check in/check out with the project team or customer help desk.</li>
	    <li>Once the work has been completed, the technician must get the site contact to sign-off on the work order.</li>
	    <li>Work orders need to be closed as soon as the installation is complete. All site installation deliverables required from the installation, including the signed work order, must be submitted otherwise pay approval may be delayed. </li>
	    <li>If you have any questions on how to complete work orders, please refer back to Field Solutions&rsquo; Web Site help menu for FAQ&rsquo;s. </li>
	  </ul></td>
    </tr>
    <tr><td>&nbsp;</td></tr>
	<tr>
	  <td align="center"><a href="NCR_Basic_Certification.php">Click here to take the NCR Basic Certification Exam</a></td>
    </tr>
    <tr><td>&nbsp;</td></tr>
</table>

</div>


<!--- End Content --->
<?php /* require ("../../footer.php");*/ ?><!-- ../ only if in sub-dir -->
		
