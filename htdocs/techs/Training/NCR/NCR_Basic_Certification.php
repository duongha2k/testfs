<?php
	ini_set("display_errors", 1); 
?>

<?php $page = 'techs'; ?>
<?php $option = 'home'; ?>
<?php 
	require_once ("../../../headerStartSession.php");

	$loggedIn   = ( !empty($_SESSION['loggedIn']) ) ? $_SESSION['loggedIn'] : 'no';

    if($loggedIn !== 'yes'){
        header( 'Location: http'.($_SERVER['HTTPS']=='on' ? 's':'').'://'.$_SERVER['HTTP_HOST'].'/techs/logIn.php?flsupport=true' ); // Redirect to logIn.php
   		exit;
    }
?>

<?php  require ("../../../header.php"); ?>
<?php  /*require ("../../../navBar.php");*/  ?>
<?php  require ("../../../library/mySQL.php"); ?>


<style type="text/css">
	#emailBlast {
		text-align: center;
	}
	.verdana2 {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		clear: both;
	}

	.verdana2bold {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		font-weight: bold;
		clear: both;
	}

	.fieldGroup {
		padding-bottom: 5px;
		border-bottom: 2px solid #2A497D;
	}

	.formBox, .formBoxFooter {
		margin: 15px auto 0px auto;
		padding: 15px;
		width: 920px;
		color: #2A497D;
		font-size: 12px;
		border: 2px solid #2A497D;
	}

	.formBox input {
		font-size: 12px;
		height: 14px;
	}

	.formBoxFooter {
		background-color: #BACBDF;
		text-align: center;
		margin: 0px auto 10px auto;
		padding: 5px 15px;
		border-top: none;
	}
	.formRow3, .formRow4, .formRow5, .formRow6 {
		width: 100%;
		clear: both;
	}
	.formRow3 div {
		width: 33%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow4 div {
		width: 25%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow5 div {
		width: 20%;
		float: left;
		margin-bottom: 5px;
	}

	.formRow6 div {
		width: 16%;
		float: left;
		margin-bottom: 5px;
	}

	.resultsTable {
		width: 80%;
		margin: 10px auto 0px auto;
	}

	.resultsTable thead {
		text-align: center;
		color: #2A497D;
		border: 1px solid #2A497D;
		background-color: #A0A8AA;
		cursor: default;
	}

	.sortAble, #nextPageBtn, #prevPageBtn {
		cursor: pointer;
	}

	.sortAbleSelected {
		cursor: pointer;
		background-color: #FF9900;
		color: #FFFFFF;
		font-weight: bold;
	}

	.resultsTable tfoot {
		text-align: center;
		color: #000000;
		border: 1px solid #2A497D;
		background-color: #A0A8AA;
	}

	.resultsTable td {
		padding: 3px 5px;
	}

	.resultsTable .evenRow {
		background-color: #E9EEF8;
	}

	.resultsTable .oddRow {
		background-color: #FBFCFD;
	}

	.resultsTable .preferred {
		background-color: #FFFF8C;
	}
    .resultsTable .techGrey {
		background-color: #DEDEDE;
	}
    .resultsTable .techYellow {
		background-color: #FFFF8C;
	}
    .resultsTable .techRed {
		background-color: #FFBBBB;
	}
    .resultsTable .techGreen {
		background-color: #89FF89;
	}

	.searchControls {
		margin-top: 10px;
		text-align: center;
	}

	.noResultsDiv {
		margin-top: 10px;
		text-align: center;
	}
	
	label input {margin-right:10px;}
	.link_button { width: 140px; border:none; background: url(/widgets/images/button_middle.png) left top no-repeat; font: normal 10px/10px Verdana, Arial, Helvetica, sans-serif; padding: 4px 0 8px; color: #385c7e; cursor: pointer; }
</style>

<?php

if (!isset($_POST["sendTest"])) {

?>


<form id="testForm" name="testForm" action="<?php echo $_SERVER['PHP_SELF'] . "?v=" . $_GET["v"]?>&snp=1" method="post">
<div class="formBox">
<div class="verdana2bold" align="center">NCR Basic Certification Exam</div><br />
<div class="formRow1">This is an open book exam for the NCR Basic Certification. You have to take the NCR Basic Certification Exam to be certified to do future NCR installations. You can use this document to assist you in taking this exam. </div><br />
<div class="formRow1"><strong>Note: </strong>You must have a score of 100% to be activated as an NCR Certified Tech.</div><br />
  <div class="formRow1">
    <ul>
      <li><a href="NCR_Training.php">NCR Training Material</a></li>
    </ul>
    <hr /><br />
    <style type="text/css">  
ul {   margin-bottom: 25px;   margin-left: 50px;  }  li a {    color: red; } 
</style>
</div>
    <div class="verdana2bold">1) When faced with a Customer problem that you do not have an answer for, your response would be?</div><br />
    <div class="formRow1">
        <div><label><input id="question1" name="question1" value="1"  type="radio">I'm not trained on this</label></div>
        <div><label><input id="question1" name="question1" value="2"  type="radio">I'll call my support team for some additional help on this issue</label></div>
        <div><label><input id="question1" name="question1" value="3"  type="radio">I don't know how to fix it</label></div>
    </div>
    <br />
    <div class="verdana2bold">2) NCR World Headquarters is located in?</div><br />
    <div class="formRow2">        
        <div><label><input id="question2" name="question2" value="1"  type="radio">Canton, OH</label></div>
        <div><label><input id="question2" name="question2" value="2"  type="radio">Buffalo, NY</label></div>
        <div><label><input id="question2" name="question2" value="3"  type="radio">Peachtree, GA</label></div>
        <div><label><input id="question2" name="question2" value="4"  type="radio">St. Luis, MI</label></div>
    </div>
    <br />
   <div class="verdana2bold">3) Where will you find the Work Order for the activities that you will perform?</div><br />
   <div class="formRow2">        
        <div><label><input id="question3" name="question3" value="1"  type="radio">It is mailed to you - USPS</label></div>
        <div><label><input id="question3" name="question3" value="2"  type="radio">At the Field Solutions local office</label></div>
        <div><label><input id="question3" name="question3" value="3"  type="radio">On the Field Solutions Web Site</label></div>
        <div><label><input id="question3" name="question3" value="4"  type="radio">It is sent to you via UPS</label></div>
	</div>
    <br />
   <div class="verdana2bold">4) How soon after the work is performed must you close/update the Work Order � in order to avoid a delay in pay approval?</div><br />
    <div class="formRow2">        
        <div><label><input id="question4" name="question4" value="1"  type="radio">As soon as the installation is complete</label></div>
        <div><label><input id="question4" name="question4" value="2"  type="radio">Within 24 hours of completing the install</label></div>
        <div><label><input id="question4" name="question4" value="3"  type="radio">Within 2 days of completing the install</label></div>
        <div><label><input id="question4" name="question4" value="4"  type="radio">Within 3 days of completing the install</label></div>
	</div>
    <br />
   <div class="verdana2bold">5) Is it appropriate to wear a shirt that carries an IBM logo on the pocket to an NCR customer's install?</div><br />
    <div class="formRow2">        
        <div><label><input id="question5" name="question5" value="1"  type="radio">Yes</label></div>
        <div><label><input id="question5" name="question5" value="2"  type="radio">No</label></div>
	</div>
    <br />
   <div class="verdana2bold">6) If you have questions concerning how to complete a work order you should?</div><br />
    <div class="formRow2">        
        <div><label><input id="question6" name="question6" value="1"  type="radio">Call 1-800-Help </label></div>
        <div><label><input id="question6" name="question6" value="2"  type="radio">Call the local NCR Office</label></div>
        <div><label><input id="question6" name="question6" value="3"  type="radio">Refer to the FAQ on the Field Solutions Web Site</label></div>
        <div><label><input id="question6" name="question6" value="4"  type="radio">Call a local NCR Customer Engineer</label></div>
	</div>
    <br />
   <div class="verdana2bold">7) Which describes being "Customer Centric"?</div><br />
    <div class="formRow2">        
        <div><label><input id="question7" name="question7" value="1"  type="radio">Giving the Customer personal attention</label></div>
        <div><label><input id="question7" name="question7" value="2"  type="radio">Demonstrating a sense of urgency</label></div>
        <div><label><input id="question7" name="question7" value="3"  type="radio">Presenting yourself in a professional manner</label></div>
        <div><label><input id="question7" name="question7" value="4"  type="radio">All of the above</label></div>
	</div>
    <br />
   <div class="verdana2bold">8) You are performing an installation and an NCR Customer Engineer is there on the same site. Is it OK to discuss pay together?</div><br />
    <div class="formRow2">        
        <div><label><input id="question8" name="question8" value="1"  type="radio">Yes</label></div>
        <div><label><input id="question8" name="question8" value="2"  type="radio">No</label></div>
	</div>
    <br />
   <div class="verdana2bold">9) Are you required to bring a printed copy of the Work Order with you to the site?</div><br />
    <div class="formRow2">        
        <div><label><input id="question9" name="question9" value="1"  type="radio">Yes</label></div>
        <div><label><input id="question9" name="question9" value="2"  type="radio">No</label></div>
	</div>
    <br />
	<div class="verdana2bold">10) If there has been an expected delay in your timely arrival on site, who do you contact?</div><br />
    <div class="formRow2">        
        <div><label><input id="question10" name="question10" value="1"  type="radio">The Customer</label></div>
        <div><label><input id="question10" name="question10" value="2"  type="radio">The Project Coordinator listed on the Work Order</label></div>
        <div><label><input id="question10" name="question10" value="3"  type="radio">FieldSolutions office via email</label></div>
        <div><label><input id="question10" name="question10" value="4"  type="radio">Local NCR Office</label></div>
	</div>
    <br />
	<div class="verdana2bold">11) What tools and/or materials are you expected to have with you when you are performing an install for NCR?</div><br />
    <div class="formRow2">        
        <div><label><input id="question11" name="question11" value="1"  type="radio">Standard PC Toolkit</label></div>
        <div><label><input id="question11" name="question11" value="2"  type="radio">Project Specific Install Guide</label></div>
        <div><label><input id="question11" name="question11" value="3"  type="radio">Printed work order</label></div>
        <div><label><input id="question11" name="question11" value="4"  type="radio">Cell phone</label></div>
        <div><label><input id="question11" name="question11" value="5"  type="radio">ID that matches your name on the Work Order</label></div>
        <div><label><input id="question11" name="question11" value="6"  type="radio">All of the above</label></div>
	</div>
    <br />
	<div class="verdana2bold">12) Which of these is NOT part of the NCR's product offering?</div><br />
    <div class="formRow2">        
        <div><label><input id="question12" name="question12" value="1"  type="radio">ATMs</label></div>
        <div><label><input id="question12" name="question12" value="2"  type="radio">Self-checkout systems</label></div>
        <div><label><input id="question12" name="question12" value="3"  type="radio">Kiosks</label></div>
        <div><label><input id="question12" name="question12" value="4"  type="radio">Payment and Imaging solutions</label></div>
        <div><label><input id="question12" name="question12" value="5"  type="radio">Point-of-sale workstations</label></div>
        <div><label><input id="question12" name="question12" value="6"  type="radio">None of the above</label></div>
	</div>
	</div>      
    <div class="formBoxFooter">
    <div><input id="sendTest" name="sendTest" value="Submit Test" class="link_button" type="submit" /></div>
    </div>
 </form>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>

<?php

} else {
	$q1 =  $_POST['question1'];
	$q2 =  $_POST['question2'];
	$q3 =  $_POST['question3'];
	$q4 =  $_POST['question4'];
	$q5 =  $_POST['question5'];
	$q6 =  $_POST['question6'];
	$q7 =  $_POST['question7'];
	$q8 =  $_POST['question8'];
	$q9 =  $_POST['question9'];
	$q10 =  $_POST['question10'];
	$q11 =  $_POST['question11'];
	$q12 =  $_POST['question12'];

	$a1 = 2;
	$a2 = 3;
	$a3 = 3;
	$a4 = 1;
	$a5 = 2;
	$a6 = 3;
	$a7 = 4;
	$a8 = 2;
	$a9 = 1;
	$a10 = 2;
	$a11 = 6;
	$a12 = 6;

	
$points = 0;

$points = ($q1 == $a1) ? ++$points : $points;
$points = ($q2 == $a2) ? ++$points : $points;
$points = ($q3 == $a3) ? ++$points : $points;
$points = ($q4 == $a4) ? ++$points : $points;
$points = ($q5 == $a5) ? ++$points : $points;
$points = ($q6 == $a6) ? ++$points : $points;
$points = ($q7 == $a7) ? ++$points : $points;
$points = ($q8 == $a8) ? ++$points : $points;
$points = ($q9 == $a9) ? ++$points : $points;
$points = ($q10 == $a10) ? ++$points : $points;
$points = ($q11 == $a11) ? ++$points : $points;
$points = ($q12 == $a12) ? ++$points : $points;


$questions = 12;

$score = round(($points / $questions) *100);

$TechID = $_SESSION['TechID'];
$DateTime = date("Y-m-d H:i:s");

mysqlQuery("INSERT INTO NCR_Test (tech_id, date_taken, score, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12) VALUES ('$TechID', '$DateTime', '$score', '$q1', '$q2', '$q3', '$q4', '$q5', '$q6', '$q7', '$q8', '$q9', '$q10', '$q11', '$q12')");

echo "<div class=\"formBox\">";

if ($score == 100) {
	mysqlQuery("UPDATE TechBankInfo SET NCR_Basic_Cert = '1' WHERE TechID = '$TechID'");
	Core_Database_MysqlSync::sync(Core_Database::TABLE_TECH_BANK_INFO, "TechID", $TechID);
	echo "<div class=\"verdana2bold\" style=\"color:green\">Contratulations, you passed the NCR Basic Certification Exam!</div><div class=\"formRow1\"></div><br />";
	echo "<div class=\"verdana2bold\">SCORE: $score %</div>";
	
	Send_Emails($TechID);

} else {
	echo "<div class=\"verdana2bold\" style=\"color:#F00\">Sorry, you do not have a passing score</div><br />";	
	echo "<div class=\"formRow2\">You will need to re-take the exam and will not be activated for the NCR Program until you have scored 100%.</div>";
	echo "<div class=\"formRow2\">Please Click <a href=\"NCR_Basic_Certification.php\">here</a> to re-take the test.</div><br />";
	echo "<div class=\"verdana2bold\">SCORE: $score %</div>";
	echo "</div></div></div></div>";
}
echo "</div></div></div></div>";

} ?>

<p>
  <?php /*( require ("../../../footer.php");*/ ?>
</p>