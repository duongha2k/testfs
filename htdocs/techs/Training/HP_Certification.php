<?php
$page = "techs"; //Main section in navbar
$option = "certifications"; //Sub-section in navbar
$selected = "HPCert";
require_once("../../headerStartSession.php");
require_once("../../library/FSSession.php");
FSSession::validateSessionParams(array("TechID" => "TechID"));
require ("../../header.php");
//require ("../../navBar.php");
?>
<!-- Add Content Here -->

<br /><br />

<div align="center">

<table border="0" width="500">
	<tr>
	  <td><h2>Hewlett &ndash; Packard / HP Cert</h2>
	    <p>Proof of an HP (Hewlett-Packard) Certificate (the actual document, uploaded below) is needed to be eligible for ALL Warranty Service Work Orders through Field Solutions.&nbsp; Update the areas below with the certification information that&nbsp; reflects&nbsp; the certification you have.&nbsp;</p>
<p>PLEASE READ:&nbsp; On July 1, 2010: Exam HP2-H08 will be required for all new technicians the want to become APS Desktop, Notebook and Workstation certified.&nbsp; This test replaces the HP2-Q01.&nbsp;</p>
<p> Technicians that passed the HP2-Q01 prior to July 1st, do NOT need to take the new exam until notified by HP to update their certification.&nbsp;</p>

<div id="cb193b00002ea6d5ff5afb4712ac39"><a href="http://www.caspio.com" target="_blank">Online Database</a> by Caspio</div>
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b00002ea6d5ff5afb4712ac39","https:");}catch(v_e){;}</script>
<div id="cxkg"><a href="https://bridge.caspio.net/dp.asp?AppKey=193b00002ea6d5ff5afb4712ac39">Click here</a> to load this Caspio <a href="http://www.caspio.com" title="Online Database">Online Database</a>.</div>

      <p><strong>Step 2:</strong>&nbsp; <a href="/techs/Training/HP_Learning_Center.pdf" target="_BLANK">Download HP Training Instructions</a><br />
        <em><strong>Note: HP Charges $75.00 to buy this certificate. This is NOT paid to FieldSolutions. It is paid by credit card on the HP training site.</strong></em><strong></strong></p>
<p><strong>Step 3:</strong>&nbsp; Complete Certification Steps&#13;</p>
<p><strong>Step 4:</strong>&nbsp; Enter your certification information in the box above <strong>(For HP Certifications obtained after July 1, 2010&nbsp; (HP Cert # HP2-HO8 )</strong></p></td>
	  </tr>
</table>

</div>
	


<!--- End Content --->
<?php /// require ("../../footer.php");
