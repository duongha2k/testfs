<?php $page = "techs"; ?>
<?php $option = "training"; ?>
<?php $selected = "fs" ?>
<?php $display = "fs" ?>
<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>

<!-- Add Content Here -->

<br /><br />

<div align="center">

<TABLE cellSpacing=0 cellPadding=0 width=700 border=0 style="text-align:left">
<TBODY>
<TR>
<TD colSpan=2><BR><FONT face=Arial size=4><B>Direct Deposit</B></FONT> 
<P>
<HR>
<P>
<FONT face=Arial size=2>
Field Solutions offers Direct Deposit, which means you can request to have payment for work performed, deposited directly into your checking or savings account instead of Field Solutions mailing you a paper check. 
<P>
To request Direct Deposits of all payments to you, login to your tech account/profile and click on the "Banking" link in the blue menu bar.
<P>
Please complete and submit the Banking form - 
<P>
Select the type of account you want your payment deposited in ("Account Type")
<BR>

Enter the name of your bank ("Bank Name")
<BR>
Enter bank's address, city, state and zip ("Address 1, 2 ", "City", "State", "Zip", "County")  
<BR>
Enter the name on your bank account ("Name on Account")
<BR>
Enter the routing number of your checking or savings account ("Routing#")
<BR>
Enter the account number of your checking or savings account ("Account#")
<P>
After you are sure that you have accurately completed the form, click on the "Update" button.
<P>
<B>Important Note:</B> Be sure to submit the correct routing and account numbers that go with your checking or savings account. When in doubt, call your bank and ask them. 
<p>Please see the image below for an example of where to look for the routing and account numbers on a check tied to your checking account if that is where you want payment deposited into. For savings accounts, you will need to contact your bank for the routing number associated with your savings account. It is NOT the routing number on your deposit ticket.<p>
<!--<P>
Please see the image below for an example of where to look for the routing and account numbers on a check tied to your checking account if that is where you want payment deposited into. For savings accounts, the routing number should be on a deposit ticket tied to the account. Same thing here as with your checking, be sure not confuse the savings routing number with the accounting number. If in doubt, you should check with your bank. 
-->
<P>
<IMG SRC="../../content/images/ran-example.JPG" WIDTH=348 HEIGHT=226 BORDER=0  ALT="">
<P class="nextButton" align="center"><a href="FS_PaymentPolicy.php">Previous</a>
&nbsp; | &nbsp;<a href="FS_AutoEmails.php">Next</a>
</TD></TR></TBODY></TABLE>















</div>

<!--- End Content --->
<?php require ("../../footer.php"); ?>
		
