<?php $page = "techs"; ?>
<?php $option = "training"; ?>
<?php $selected = "fs" ?>
<?php $display = "fs" ?>
<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>

<!-- Add Content Here -->

<br /><br />

<div align="center">

<TABLE cellSpacing=0 cellPadding=0 width=700 border=0 style="text-align:left">
<TBODY>
<TR>
<TD colSpan=2><BR><FONT face=Arial size=4><B>Auto-emails from Field Solutions</B></FONT> 
<P>
<HR>
<P>
<FONT face=Arial size=2>
Once you are fully registered as a technician with Field Solutions, you will begin to receive auto emails confirming things you have done, asking you to confirm something, email containing special instructions, etc. The type of auto-emails you receive, the content of those emails plus how often and/or how many you receive, will vary depending on your involvement/status. 
<P>
Here are examples of the types of auto-emails that you may receive as a registered technician with Field Solutions depending on your status: 
<P>
- Once you become fully registered as a technician with Field Solutions, you will receive an auto-email welcoming you.
<P>
- Once you have submitted the FLS training test (regarding process/protocol) with a passing score and have been activated for the FLS program/client, you will receive an auto-email welcoming you to the FLS program.
<P>

- When you are first assigned a work order, you will receive an auto-email containing a link that you will need to click on in order to verify that you have reviewed the work order. 
<P>
- 48 hours prior to the start date/time of a particular install you were assigned to, you will receive an auto-email containing a link that you will need to click on in order to confirm that you will be showing up at the install on the start date/time listed. 
<P>
- 24 hours past the start date/time, you will receive an auto-email if/when you have not completed the work order online per instructions.
<P>
- Whenever FLS work orders within 50 miles of your zip code are imported into the system, you will receive an auto-email alert.  
<P>
- Whenever a work order of yours online is approved by the client, you will receive an auto-email alert. 
<P>
- If/when you are assigned to a work order and there is a change of address, a date change or other critical information is modified, you will receive auto-email alerting you of the fact.  
<P> 
- While not technically an "auto-email" in nature, you may receive occasional email from clients and/or Field Solutions staff related to install opportunities and/or special instructions. 
<P class="nextButton" align="center"><a href="FS_DirectDeposit.php">Previous</a>
&nbsp; | &nbsp;<a href="FS_FLS_Program.php">Next</a>
</TD></TR></TBODY></TABLE>

















</div>

<!--- End Content --->
<?php require ("../../footer.php"); ?>
		
