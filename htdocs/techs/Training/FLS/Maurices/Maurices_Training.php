<?php
	ini_set("display_errors", 1); 
?>

<?php $page = 'techs'; ?>
<?php $option = 'home'; ?>
<?php 
	require_once ("../../../../headerStartSession.php");

	$loggedIn   = ( !empty($_SESSION['loggedIn']) ) ? $_SESSION['loggedIn'] : 'no';

    if($loggedIn !== 'yes'){
        header( 'Location: http'.($_SERVER['HTTPS']=='on' ? 's':'').'://'.$_SERVER['HTTP_HOST'].'/techs/logIn.php?flsupport=true' ); // Redirect to logIn.php
   		exit;
    }
?>

<?php  require ("../../../../header.php"); ?>
<?php  require ("../../../../navBar.php");  ?>
<?php  require ("../../../../library/mySQL.php"); ?>

<script type="text/javascript">

$(document).ready(function(){
	$("body").css("width", "850px").css("min-width", "850px");
	
});

</script>
<style type="text/css">
	#emailBlast {
		text-align: center;
	}
	.verdana2 {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		clear: both;
	}

	.verdana2bold {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		font-weight: bold;
		clear: both;
	}

	.fieldGroup {
		padding-bottom: 5px;
		border-bottom: 2px solid #2A497D;
	}

	.formBox, .formBoxFooter {
		margin: 15px auto 0px auto;
		padding: 15px;
		width: 920px;
		color: #2A497D;
		font-size: 12px;
		border: 2px solid #2A497D;
	}

	.formBox input {
		font-size: 12px;
		height: 14px;
	}

	.formBoxFooter {
		background-color: #BACBDF;
		text-align: center;
		margin: 0px auto 10px auto;
		padding: 5px 15px;
		border-top: none;
	}
	.formRow3, .formRow4, .formRow5, .formRow6 {
		width: 100%;
		clear: both;
	}
	.formRow3 div {
		width: 33%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow4 div {
		width: 25%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow5 div {
		width: 20%;
		float: left;
		margin-bottom: 5px;
	}

	.formRow6 div {
		width: 16%;
		float: left;
		margin-bottom: 5px;
	}

	.resultsTable {
		width: 80%;
		margin: 10px auto 0px auto;
	}

	.resultsTable thead {
		text-align: center;
		color: #2A497D;
		border: 1px solid #2A497D;
		background-color: #A0A8AA;
		cursor: default;
	}

	.sortAble, #nextPageBtn, #prevPageBtn {
		cursor: pointer;
	}

	.sortAbleSelected {
		cursor: pointer;
		background-color: #FF9900;
		color: #FFFFFF;
		font-weight: bold;
	}

	.resultsTable tfoot {
		text-align: center;
		color: #000000;
		border: 1px solid #2A497D;
		background-color: #A0A8AA;
	}

	.resultsTable td {
		padding: 3px 5px;
	}

	.resultsTable .evenRow {
		background-color: #E9EEF8;
	}

	.resultsTable .oddRow {
		background-color: #FBFCFD;
	}

	.resultsTable .preferred {
		background-color: #FFFF8C;
	}
    .resultsTable .techGrey {
		background-color: #DEDEDE;
	}
    .resultsTable .techYellow {
		background-color: #FFFF8C;
	}
    .resultsTable .techRed {
		background-color: #FFBBBB;
	}
    .resultsTable .techGreen {
		background-color: #89FF89;
	}

	.searchControls {
		margin-top: 10px;
		text-align: center;
	}

	.noResultsDiv {
		margin-top: 10px;
		text-align: center;
	}
	
	label input {margin-right:10px;}
	.link_button { width: 140px; border:none; background: url(/widgets/images/button_middle.png) left top no-repeat; font: normal 10px/10px Verdana, Arial, Helvetica, sans-serif; padding: 4px 0 8px; color: #385c7e; cursor: pointer; }
</style>

<?php

if (!isset($_POST["sendTest"])) {

?>


<form id="testForm" name="testForm" action="<?php echo $_SERVER['PHP_SELF'] . "?v=" . $_GET["v"]?>&snp=1" method="post">
<div class="formBox">
<div class="verdana2bold" align="center">FLS Maurices Certification Exam</div><br />
<div class="formRow1">This is an open book exam for the FLS Maurices Certification. You have to take the FLS Maurices Certification Exam to be certified to do future FLS Maurices installations. You can use this document to assist you in taking this exam. </div><br />
<div class="formRow1"><strong>Note: </strong>You must have a score of 100% to be activated for the FLS Maurices Program.</div><br />
  <div class="formRow1">
    <ul>
      <li><a href="/techs/Training/Maurices/Maurices_Install_Guide_rev2_6.doc">Maurices Customer Installation Document</a></li>
    </ul>
    <hr /><br />
    <style type="text/css">  
ul {   margin-bottom: 25px;   margin-left: 50px;  }  li a {    color: red; } 
</style>
</div>
    <div class="verdana2bold">1) True or False? Techs should plan to arrive onsite 15 minutes early?</div><br />
    <div class="formRow1">
        <div><label><input id="question1" name="question1" value="1"  type="radio">True</label></div>
        <div><label><input id="question1" name="question1" value="2"  type="radio">False</label></div>
    </div>
    <br />
    <div class="verdana2bold">2) True or False? Techs do not need to review the installation documentation prior to arriving onsite?</div><br />
    <div class="formRow2">        
        <div><label><input id="question2" name="question2" value="1"  type="radio">True</label></div>
        <div><label><input id="question2" name="question2" value="2"  type="radio">False</label></div>
    </div>
    <br />
   <div class="verdana2bold">3) True or False? Upon arrival at site, tech should introduce themselves to the site personnel, and then call the Command Center to report they are ready to begin?</div><br />
   <div class="formRow2">        
        <div><label><input id="question3" name="question3" value="1"  type="radio">True</label></div>
        <div><label><input id="question3" name="question3" value="2"  type="radio">False</label></div>
	</div>
    <br />
   <div class="verdana2bold">4) While on site you should practice proper business etiquette at all times?</div><br />
    <div class="formRow2">        
        <div><label><input id="question4" name="question4" value="1"  type="radio">True</label></div>
        <div><label><input id="question4" name="question4" value="2"  type="radio">False</label></div>
	</div>
    <br />
   <div class="verdana2bold">5) While doing the inventory onsite you notice that a piece of equipment is missing, who do you need to report this to?</div><br />
    <div class="formRow2">        
        <div><label><input id="question5" name="question5" value="1"  type="radio">Manager on Duty </label></div>
        <div><label><input id="question5" name="question5" value="2"  type="radio">FLS Coordinator</label></div>
        <div><label><input id="question5" name="question5" value="3"  type="radio">Fujitsu Dispatch</label></div>
        <div><label><input id="question5" name="question5" value="4"  type="radio">Project Manager</label></div>
        <div><label><input id="question5" name="question5" value="5"  type="radio">Command Center</label></div>
	</div>
    <br />
   <div class="verdana2bold">6) What is the difference between setting the registers up in a Stacked/Integrated Configuration versus a Dispersed/Distributed Configuration?</div><br />
    <div class="formRow2">        
        <div><label><input id="question6" name="question6" value="1"  type="radio">Dispersed/Distributed means the registers are installed on carts that can be wheeled around or dispersed throughout the store</label></div>
        <div><label><input id="question6" name="question6" value="2"  type="radio">Stacked/Integrated configurations mean the equipment is stacked in a single grouping. Monitor, keyboard and printer sit on top of terminal which sits on top of cash drawer and all sit on the counter top</label></div>
        <div><label><input id="question6" name="question6" value="3"  type="radio">Dispersed/Distributed Configuration means the equipment is dispersed within the counter/cabinetry and cables are routed within that cabinetry to allow connectivity</label></div>
        <div><label><input id="question6" name="question6" value="4"  type="radio">Both answers 1 & 2 are valid</label></div>
        <div><label><input id="question6" name="question6" value="5"  type="radio">Both answers 2 & 3 are valid</label></div>
	</div>
    <br />
   <div class="verdana2bold">7) Techs are required to work on one register at a time, de-installing the old and installing the new register in its place. As you complete each new register installation, what should you do?</div><br />
    <div class="formRow2">        
        <div><label><input id="question7" name="question7" value="1"  type="radio">Box the old register and prep it for shipping</label></div>
        <div><label><input id="question7" name="question7" value="2"  type="radio">Ask the manager on duty to run test transactions as you start the next register</label></div>
        <div><label><input id="question7" name="question7" value="3"  type="radio">Contact the Command Center and report that register # is up before proceeding</label></div>
        <div><label><input id="question7" name="question7" value="4"  type="radio">Immediately start on the next register de-installation</label></div>
	</div>
    <br />
   <div class="verdana2bold">8) The VeriFone credit card terminal requires an external power supply?</div><br />
    <div class="formRow2">        
        <div><label><input id="question8" name="question8" value="1"  type="radio">True</label></div>
        <div><label><input id="question8" name="question8" value="2"  type="radio">False</label></div>
	</div>
    <br />
   <div class="verdana2bold">9) As you are de-installing each register, is it required to have a store manager/store personnel present to verify if there is any cash in the cash drawer?</div><br />
    <div class="formRow2">        
        <div><label><input id="question9" name="question9" value="1"  type="radio">Yes</label></div>
        <div><label><input id="question9" name="question9" value="2"  type="radio">No</label></div>
	</div>
    <br />
	<div class="verdana2bold">10) If you are the clerk standing behind the counter at the register 1 position, what to which side is register 2?</div><br />
    <div class="formRow2">        
        <div><label><input id="question10" name="question10" value="1"  type="radio">Right</label></div>
        <div><label><input id="question10" name="question10" value="2"  type="radio">Left</label></div>
        <div><label><input id="question10" name="question10" value="3"  type="radio">Directly Behind</label></div>
	</div>
    <br />
	<div class="verdana2bold">11) True or False? When installing registers 2 and 3, they should plug into the associated port on the switch. Example: Register 2 plugs into port 2x and register 3 plugs into port 3x?</div><br />
    <div class="formRow2">        
        <div><label><input id="question11" name="question11" value="1"  type="radio">True</label></div>
        <div><label><input id="question11" name="question11" value="2"  type="radio">False</label></div>
	</div>
    <br />
	<div class="verdana2bold">12) According to the documentation, who runs the test scripts on the registers?</div><br />
    <div class="formRow2">        
        <div><label><input id="question12" name="question12" value="1"  type="radio">Technician</label></div>
        <div><label><input id="question12" name="question12" value="2"  type="radio">Store Associate</label></div>
	</div>
    <br />
	<div class="verdana2bold">13) Once the installation is complete and before leaving the site, what is the tech required to do?</div><br />
    <div class="formRow2">        
        <div><label><input id="question13" name="question13" value="1"  type="radio">Tech must contact Command Center to verify all terminals are properly configured</label></div>
        <div><label><input id="question13" name="question13" value="2"  type="radio">Tech must verbally checkout with the Command Center</label></div>
        <div><label><input id="question13" name="question13" value="3"  type="radio">Gather up and dispose of excess trash and boxes</label></div>
        <div><label><input id="question13" name="question13" value="4"  type="radio">Hand cash drawer keys over to the manager on duty or store trainer</label></div>
        <div><label><input id="question13" name="question13" value="5"  type="radio">All of the above</label></div>
	</div>
    <br />
	<div class="verdana2bold">14) True or False? When powering on the register the LAN cable must be plugged in?</div><br />
    <div class="formRow2">        
        <div><label><input id="question14" name="question14" value="1"  type="radio">True</label></div>
        <div><label><input id="question14" name="question14" value="2"  type="radio">False</label></div>
	</div>
    <br />
	<div class="verdana2bold">15) What section will you find the Scanner program sheet?</div><br />
    <div class="formRow2">        
        <div><label><input id="question15" name="question15" value="1"  type="radio">5.8</label></div>
        <div><label><input id="question15" name="question15" value="2"  type="radio">Appendix A</label></div>
        <div><label><input id="question15" name="question15" value="3"  type="radio">Appendix F</label></div>
        <div><label><input id="question15" name="question15" value="4"  type="radio">Store Manager</label></div>
	</div>
    <br />
	<div class="verdana2bold">16) When programming the scanner how many beeps do you wait for after scanning default barcode?</div><br />
    <div class="formRow2">        
        <div><label><input id="question16" name="question16" value="1"  type="radio">Wait for the scanner to beep 1 time then wait 2 more seconds for additional beep.</label></div>
        <div><label><input id="question16" name="question16" value="2"  type="radio">Wait for the scanner to beep 3 times then wait 10 more seconds for additional 3 beeps</label></div>
        <div><label><input id="question16" name="question16" value="3"  type="radio">Do not need to wait move on to next barcode</label></div>
	</div>
    <br />
	<div class="verdana2bold">17) True or False? When running store personalization you have the store associate confirm information entered?</div><br />
    <div class="formRow2">        
        <div><label><input id="question17" name="question17" value="1"  type="radio">True</label></div>
        <div><label><input id="question17" name="question17" value="2"  type="radio">False</label></div>
	</div>
    <br />
	<div class="verdana2bold">18) The external power supply for the VeriFone credit card terminal plugs into?</div><br />
    <div class="formRow2">        
        <div><label><input id="question18" name="question18" value="1"  type="radio">The VeriFone terminal</label></div>
        <div><label><input id="question18" name="question18" value="2"  type="radio">The multiport connection on the VeriFone RS 232 cable</label></div>
        <div><label><input id="question18" name="question18" value="3"  type="radio">The right side of the VeriFone RS 232 9 pin connector at the register</label></div>
        <div><label><input id="question18" name="question18" value="4"  type="radio">The VeriFone does not use an external power supply</label></div>
	</div>
    <br />
	<div class="verdana2bold">19) The VeriFone credit card terminal plugs into what port</div><br />
    <div class="formRow2">        
        <div><label><input id="question19" name="question19" value="1"  type="radio">COM1</label></div>
        <div><label><input id="question19" name="question19" value="2"  type="radio">COM2</label></div>
        <div><label><input id="question19" name="question19" value="3"  type="radio">Any USB Port</label></div>
	</div>
    </div>      
    <div class="formBoxFooter">
    <div><input id="sendTest" name="sendTest" value="Submit Test" class="link_button" type="submit" /></div>
    </div>
 </form>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>

<?php

} else {
	$q1 =  $_POST['question1'];
	$q2 =  $_POST['question2'];
	$q3 =  $_POST['question3'];
	$q4 =  $_POST['question4'];
	$q5 =  $_POST['question5'];
	$q6 =  $_POST['question6'];
	$q7 =  $_POST['question7'];
	$q8 =  $_POST['question8'];
	$q9 =  $_POST['question9'];
	$q10 =  $_POST['question10'];
	$q11 =  $_POST['question11'];
	$q12 =  $_POST['question12'];
	$q13 =  $_POST['question13'];
	$q14 =  $_POST['question14'];
	$q15 =  $_POST['question15'];
	$q16 =  $_POST['question16'];
	$q17 =  $_POST['question17'];
	$q18 =  $_POST['question18'];
	$q19 =  $_POST['question19'];

	$a1 = 1;
	$a2 = 2;
	$a3 = 1;
	$a4 = 1;
	$a5 = 5;
	$a6 = 5;
	$a7 = 3;
	$a8 = 1;
	$a9 = 1;
	$a10 = 2;
	$a11 = 1;
	$a12 = 2;
	$a13 = 5;
	$a14 = 2;
	$a15 = 3;
	$a16 = 2;
	$a17 = 1;
	$a18 = 3;
	$a19 = 2;
 
$points = 0;

$points = ($q1 == $a1) ? ++$points : $points;
$points = ($q2 == $a2) ? ++$points : $points;
$points = ($q3 == $a3) ? ++$points : $points;
$points = ($q4 == $a4) ? ++$points : $points;
$points = ($q5 == $a5) ? ++$points : $points;
$points = ($q6 == $a6) ? ++$points : $points;
$points = ($q7 == $a7) ? ++$points : $points;
$points = ($q8 == $a8) ? ++$points : $points;
$points = ($q9 == $a9) ? ++$points : $points;
$points = ($q10 == $a10) ? ++$points : $points;
$points = ($q11 == $a11) ? ++$points : $points;
$points = ($q12 == $a12) ? ++$points : $points;
$points = ($q13 == $a13) ? ++$points : $points;
$points = ($q14 == $a14) ? ++$points : $points;
$points = ($q15 == $a15) ? ++$points : $points;
$points = ($q16 == $a16) ? ++$points : $points;
$points = ($q17 == $a17) ? ++$points : $points;
$points = ($q18 == $a18) ? ++$points : $points;
$points = ($q19 == $a19) ? ++$points : $points;

$questions = 19;

$score = round(($points / $questions) *100);

$TechID = $_SESSION['TechID'];
$DateTime = date("Y-m-d H:i:s");

mysqlQuery("INSERT INTO Maurices_Test (tech_id, date_taken, score, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19) VALUES ('$TechID', '$DateTime', '$score', '$q1', '$q2', '$q3', '$q4', '$q5', '$q6', '$q7', '$q8', '$q9', '$q10', '$q11', '$q12', '$q13', '$q14', '$q15', '$q16', '$q17', '$q18', '$q19')");

echo "<div class=\"formBox\">";

if ($score == 100) {
	mysqlQuery("UPDATE TechBankInfo SET Cert_Maurices_POS = '1' WHERE TechID = '$TechID'");
	Core_Database_MysqlSync::sync(Core_Database::TABLE_TECH_BANK_INFO, "TechID", $TechID);
	echo "<div class=\"verdana2bold\" style=\"color:green\">Contratulations, you passed the FLS Maurices Certification Exam!</div><div class=\"formRow1\"></div><br />";
	echo "<div class=\"verdana2bold\">SCORE: $score %</div>";
	
	Send_Emails($TechID);

} else {
	echo "<div class=\"verdana2bold\" style=\"color:#F00\">Sorry, you do not have a passing score</div><br />";	
	echo "<div class=\"formRow2\">You will need to re-take the exam and will not be activated for the FLS Maurices Program until you have scored 100%.</div>";
	echo "<div class=\"formRow2\">Please Click <a href=\"Maurices_Training.php\">here</a> to re-take the test.</div><br />";
	echo "<div class=\"verdana2bold\">SCORE: $score %</div>";
	echo "</div></div></div></div>";
}
echo "</div></div></div></div>";

} ?>

<p>
  <?php require ("../../../../footer.php"); ?>
</p>