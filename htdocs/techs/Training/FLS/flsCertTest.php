<?php 
	require_once ("../../../headerSimple.php");
?>
<div>
    <h1>Fujitsu FLS Certification Instructions</h1>
    <p style="margin-top: 20px">Fujitsu FLS is a major FieldSolutions client focused on performing high-volume hardware and software IMAC and Service (break/fix) calls in retail, grocery and office environments. Fujitsu ONLY uses sub-contractors that have completed their FREE Orientation Training that evaluates technicians on the following skills:</p>
	    <ul style="list-style: disc inside none; margin-left: 50px">
	    	<li>Good customer service skills</li>
			<li>Basic information technology skills</li>
			<li>Ability to closely follow and understand written and verbal instructions</li>
			<li>Ability to communicate effectively</li>
        </ul>
	<p>In order to accept  an assignment with Fujitsu, you will first need to complete their FREE training module and score 100% on the open book test hosted on the Blue Ribbon Techs site.</p>
	<p>FieldSolutions contracts with Blue Ribbon Techs (<a href="http://www.blueribbontechs.com" target="_blank">www.blueribbontechs.com</a>), the "One-And-Done" shop for online Technician Credentials, to conduct and maintain our technicians' Background Checks, Drug Tests, and other certifications. Blue Ribbon Techs is a new company acting in the interest of independent contractor technicians. Their mission is to serve you and address all of your portal and customer credential needs.</p>
    <p>We also encourage all techs to complete the  Blue Ribbon Techs Boot Camp professional training program. It helps prepare techs for doing contract work and sharpen soft skills which are important to FieldSolutions and other clients.</p>
    <p style="font-weight: bold">To add your credentials from Blue Ribbon Techs to your FieldSolutions profile you will want to add us as an authorized Client. To do this log in to Blue Ribbon Techs, click on Profile, then the clients tab, add our company name (FieldSolutions), and your FieldSolutions Tech ID# (NOT your FLS ID#).</p>
	<p>To get started, simply click the button below, or go directly to <a href="http://www.blueribbontechs.com" target="_blank">www.blueribbontechs.com</a> and register with Blue Ribbon Techs. Once you are registered you can start the Training.</p>
	<p>Start growing your business today!</p>
    
    <div style="width: 120px; margin: 5px auto"><input type="button" value="Start FLS Training" class="link_button_sm" onclick="window.open('http://www.blueribbontechs.com')"></div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	parent.$("#fancybox-content").css('height', 530);
//	$.fancybox.resize();
});
</script>