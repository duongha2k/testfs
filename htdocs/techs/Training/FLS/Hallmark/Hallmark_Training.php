<?php
	ini_set("display_errors", 1); 
?>

<?php $page = 'techs'; ?>
<?php $option = 'home'; ?>
<?php 
	require_once ("../../../../headerStartSession.php");

	$loggedIn   = ( !empty($_SESSION['loggedIn']) ) ? $_SESSION['loggedIn'] : 'no';

    if($loggedIn !== 'yes'){
        header( 'Location: http'.($_SERVER['HTTPS']=='on' ? 's':'').'://'.$_SERVER['HTTP_HOST'].'/techs/logIn.php?flsupport=true' ); // Redirect to logIn.php
   		exit;
    }
?>

<?php  require ("../../../../header.php"); ?>
<?php  require ("../../../../navBar.php");  ?>
<?php  require ("../../../../library/mySQL.php"); ?>
<script type="text/javascript">

$(document).ready(function(){
	$("body").css("width", "850px").css("min-width", "850px");
	
});

</script>

<style type="text/css">
	#emailBlast {
		text-align: center;
	}
	.verdana2 {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		clear: both;
	}

	.verdana2bold {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		font-weight: bold;
		clear: both;
	}

	.fieldGroup {
		padding-bottom: 5px;
		border-bottom: 2px solid #2A497D;
	}

	.formBox, .formBoxFooter {
		margin: 15px auto 0px auto;
		padding: 15px;
		width: 920px;
		color: #2A497D;
		font-size: 12px;
		border: 2px solid #2A497D;
	}

	.formBox input {
		font-size: 12px;
		height: 14px;
	}

	.formBoxFooter {
		background-color: #BACBDF;
		text-align: center;
		margin: 0px auto 10px auto;
		padding: 5px 15px;
		border-top: none;
	}
	.formRow3, .formRow4, .formRow5, .formRow6 {
		width: 100%;
		clear: both;
	}
	.formRow3 div {
		width: 33%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow4 div {
		width: 25%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow5 div {
		width: 20%;
		float: left;
		margin-bottom: 5px;
	}

	.formRow6 div {
		width: 16%;
		float: left;
		margin-bottom: 5px;
	}

	.resultsTable {
		width: 80%;
		margin: 10px auto 0px auto;
	}

	.resultsTable thead {
		text-align: center;
		color: #2A497D;
		border: 1px solid #2A497D;
		background-color: #A0A8AA;
		cursor: default;
	}

	.sortAble, #nextPageBtn, #prevPageBtn {
		cursor: pointer;
	}

	.sortAbleSelected {
		cursor: pointer;
		background-color: #FF9900;
		color: #FFFFFF;
		font-weight: bold;
	}

	.resultsTable tfoot {
		text-align: center;
		color: #000000;
		border: 1px solid #2A497D;
		background-color: #A0A8AA;
	}

	.resultsTable td {
		padding: 3px 5px;
	}

	.resultsTable .evenRow {
		background-color: #E9EEF8;
	}

	.resultsTable .oddRow {
		background-color: #FBFCFD;
	}

	.resultsTable .preferred {
		background-color: #FFFF8C;
	}
    .resultsTable .techGrey {
		background-color: #DEDEDE;
	}
    .resultsTable .techYellow {
		background-color: #FFFF8C;
	}
    .resultsTable .techRed {
		background-color: #FFBBBB;
	}
    .resultsTable .techGreen {
		background-color: #89FF89;
	}

	.searchControls {
		margin-top: 10px;
		text-align: center;
	}

	.noResultsDiv {
		margin-top: 10px;
		text-align: center;
	}
	
	label input {margin-right:10px;}
	.link_button { width: 140px; border:none; background: url(/widgets/images/button_middle.png) left top no-repeat; font: normal 10px/10px Verdana, Arial, Helvetica, sans-serif; padding: 4px 0 8px; color: #385c7e; cursor: pointer; }
</style>

<?php

if (!isset($_POST["sendTest"])) {

?>


<form id="testForm" name="testForm" action="<?php echo $_SERVER['PHP_SELF'] . "?v=" . $_GET["v"]?>&snp=1" method="post">
<div class="formBox">
<div class="verdana2bold" align="center">FLS Hallmark POS Training Exam</div><br />
<div class="formRow1">This is an open book exam for the Hallmark project for FLS. You have to take the Hallmark test to be certified to do future Hallmark installations. You can use both documents to assist you in taking this test. </div><br />
<div class="formRow1"><strong>Note: </strong>You must have a score of 100% to be activated as an FLS Hallmark Tech.</div><br />
  <div class="formRow1">
    <ul>
      <li><a href="hallmark_tp2k_conversion_guide V9.0.pdf">Hallmark POS Installation Guide</a></li>
      <li><a href="hallmark_new_store_installation_guide.doc">Hallmark New Store Installation Guide</a></li>
      <li>&nbsp;</li>
      <li><a href="ftp://repair:2kGYRetr@ftp.ftxs.fujitsu.com/repair/Hallmark%20POS%20Refresh_R3.wmv" target="_blank">Hallmark Installation Video</a><br>
      </li>
      (Must be using Internet Explorer)
    </ul>
    <hr /><br />
    <style type="text/css">  
ul {   margin-bottom: 25px;   margin-left: 50px;  }  li a {    color: red; } 
</style>
</div>
    <div class="verdana2bold">1) Upon arrival, after you introduce yourself to the store manager and have spoken to the Hallmark Support Desk, what is the first task you do?</div><br />
    <div class="formRow1">
        <div><label><input id="question1" name="question1" value="1"  type="radio">Find the Accessory Box, remove the UPS and plug it into any available wall outlet. Refer to the indicator lights to ensure the UPS battery is charging</label></div>
        <div><label><input id="question1" name="question1" value="2"  type="radio">Take a break</label></div>
        <div><label><input id="question1" name="question1" value="3"  type="radio">Shut down all registers, and printers that are to be replaced. Fill out and fax the completed sign off sheet</label></div>
    </div>
    <br />
    <div class="verdana2bold">2) On the register screen, what action should you do if "NoDB" is displayed after you've swapped the switch or switches?</div><br />
    <div class="formRow2">        
        <div><label><input id="question2" name="question2" value="1"  type="radio">"NoDB" means the backup routine between the register and PC Server has finished, continue to the next step</label></div>
        <div><label><input id="question2" name="question2" value="2"  type="radio">"NoDB" should be expected as the DB does not reside on that terminal, locate the terminal with the correct DB and try the step again</label></div>
        <div><label><input id="question2" name="question2" value="3"  type="radio">"NoDB" means the register and the PC Server data communications is not functioning. Troubleshoot or call Hallmark Support Desk for next action</label></div>
    </div>
    <br />
   <div class="verdana2bold">3) What does the "QUICKNTLY" command do?</div><br />
   <div class="formRow2">        
        <div><label><input id="question3" name="question3" value="1"  type="radio">Backups the database from the PC Server to two units; the Backup Server Register and to Satellite # 2 Register</label></div>
        <div><label><input id="question3" name="question3" value="2"  type="radio">This is the command the store associates will run to perform the store close. Once that step is complete you may continue to the next step</label></div>
        <div><label><input id="question3" name="question3" value="3"  type="radio">This is the system shutdown command that must be run prior to swapping the equipment</label></div>
	</div>
    <br />
   <div class="verdana2bold">4) On the old system during store close, what report should you make sure the Store Mgr. prints out for later reference?</div><br />
    <div class="formRow2">        
        <div><label><input id="question4" name="question4" value="1"  type="radio">Inventory Count Report, Appendix Z</label></div>
        <div><label><input id="question4" name="question4" value="2"  type="radio">Store Balancing Report, Step 2.1 relates to Step 14.7b</label></div>
        <div><label><input id="question4" name="question4" value="3"  type="radio">System Status Report, Help Desk will guide Store Manager through this step</label></div>
	</div>
    <br />
   <div class="verdana2bold">5) Before removing the telephone cables to the modems in the old IBM Server, what action does the manual ask you to do?</div><br />
    <div class="formRow2">        
        <div><label><input id="question5" name="question5" value="1"  type="radio">Tie wrap the cables for shipping after the installation</label></div>
        <div><label><input id="question5" name="question5" value="2"  type="radio">Label the Inbound and Outbound telco cables before removing them</label></div>
        <div><label><input id="question5" name="question5" value="3"  type="radio">Test the lines to make sure they are functioning properly prior to work being done</label></div>
	</div>
    <br />
   <div class="verdana2bold">6) What equipment should Not be plugged into the Battery, Surge and Noise protector side of the UPS?</div><br />
    <div class="formRow2">        
        <div><label><input id="question6" name="question6" value="1"  type="radio">Printers </label></div>
        <div><label><input id="question6" name="question6" value="2"  type="radio">Modems</label></div>
        <div><label><input id="question6" name="question6" value="3"  type="radio">Both</label></div>
	</div>
    <br />
   <div class="verdana2bold">7) During a normal installation, is the technician asked to program the scanners to make them operational?</div><br />
    <div class="formRow2">        
        <div><label><input id="question7" name="question7" value="1"  type="radio">Yes. This is part of the standard installations procedures and there are instructions in Appendix Q </label></div>
        <div><label><input id="question7" name="question7" value="2"  type="radio">No. The barcodes for programming the scanners are included for reference only in the manual. The scanners were previously tested in the staging facility in CA</label></div>
	</div>
    <br />
   <div class="verdana2bold">8) "QUICKNTLY" backups the database. What command is used to restore the database to the new PC Server from the old register?</div><br />
    <div class="formRow2">        
        <div><label><input id="question8" name="question8" value="1"  type="radio">REFRESH.CMD</label></div>
        <div><label><input id="question8" name="question8" value="2"  type="radio">FORMAT.CMD</label></div>
        <div><label><input id="question8" name="question8" value="3"  type="radio">IPBACKUP.CMD</label></div>
	</div>
    <br />
   <div class="verdana2bold">9) If the shelf to support the new register is less than eighteen inches from front to back, what preparatory action is required by the tech?</div><br />
    <div class="formRow2">        
        <div><label><input id="question9" name="question9" value="1"  type="radio">Tech must raise the shelf to accommodate the new register</label></div>
        <div><label><input id="question9" name="question9" value="2"  type="radio">Tech must remove the pedestal feet from the bottom of the cash drawer assembly, otherwise the POS could fall off the front edge of the shelf</label></div>
        <div><label><input id="question9" name="question9" value="3"  type="radio">Tech should politely inform the Store Manager that they need to notify maintenance to raise the shelf. The installation will be continued once the shelf is raised</label></div>
	</div>
    <br />
	<div class="verdana2bold">10) When testing each new register, why is the FE asked to type in "9000"?</div><br />
    <div class="formRow2">        
        <div><label><input id="question10" name="question10" value="1"  type="radio">To test whether the Backspace key is properly installed</label></div>
        <div><label><input id="question10" name="question10" value="2"  type="radio">To test the sticky keys feature of the software after repeated presses of the 0 key</label></div>
        <div><label><input id="question10" name="question10" value="3"  type="radio">To test the software recognition of reserved ID# 9000</label></div>
	</div>
    <br />
	<div class="verdana2bold">11) What is different for the operator when swiping a credit card on Fujitsu POS compared to the IBM registers?</div><br />
    <div class="formRow2">        
        <div><label><input id="question11" name="question11" value="1"  type="radio">There are no differences, the units are identical and interchangeable</label></div>
        <div><label><input id="question11" name="question11" value="2"  type="radio">The only differences are cosmetic in the shape and color of the units</label></div>
        <div><label><input id="question11" name="question11" value="3"  type="radio">Magnetic stripe must face AWAY from operator when swiping it thru the Fujitsu magnetic stripe reader</label></div>
	</div>
    <br />
	<div class="verdana2bold">12) At Step 24, after the "finish" command is entered, does the FE wait for the process to complete?</div><br />
    <div class="formRow2">        
        <div><label><input id="question12" name="question12" value="1"  type="radio">Yes. Do not proceed until you are cleared by the help desk or data loss may occur</label></div>
        <div><label><input id="question12" name="question12" value="2"  type="radio">No. Continue to complete the data wipe hard drive exchange on the old equipment and prepare to leave the site</label></div>
	</div>
    <br />
	<div class="verdana2bold">13) During the PCI Security Procedure, are the drives removed packaged into one box and labeled and removed from the store?</div><br />
    <div class="formRow2">        
        <div><label><input id="question13" name="question13" value="1"  type="radio">Not quite. Drives are removed and packaged into one box and labeled with the preprinted label found in the Open-Me-First envelope. The package is left at the site for FedEx to pick up the following day. If the store doesn't normally have FedEx Express pick-up, then the FE needs to call FedEx to arrange a pickup.</label></div>
        <div><label><input id="question13" name="question13" value="2"  type="radio">Yes, they should not be left for the store to discard</label></div>
	</div>
    <br />
	<div class="verdana2bold">14) If a Cisco router and security network equipment are found in the backoffice, does the FE remove any of this equipment to perform the POS installation?</div><br />
    <div class="formRow2">        
        <div><label><input id="question14" name="question14" value="1"  type="radio">No. A good rule of thumb is to touch only cables and equipment mentioned in the manual and leave all other equipment alone</label></div>
        <div><label><input id="question14" name="question14" value="2"  type="radio">Yes. That old equipment is incompatible with the new systems</label></div>
	</div>
    <br />
	<div class="verdana2bold">15) If the multifunction printer works for faxing, can the FE fax the signed completed Acceptance Signoff to Fujitsu that evening?</div><br />
    <div class="formRow2">        
        <div><label><input id="question15" name="question15" value="1"  type="radio">Yes</label></div>
        <div><label><input id="question15" name="question15" value="2"  type="radio">No</label></div>
	</div>
    <br />
	</div>      
    <div class="formBoxFooter">
    <div><input id="sendTest" name="sendTest" value="Submit Test" class="link_button" type="submit" /></div>
    </div>
 </form>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>

<?php

} else {
	$q1 =  $_POST['question1'];
	$q2 =  $_POST['question2'];
	$q3 =  $_POST['question3'];
	$q4 =  $_POST['question4'];
	$q5 =  $_POST['question5'];
	$q6 =  $_POST['question6'];
	$q7 =  $_POST['question7'];
	$q8 =  $_POST['question8'];
	$q9 =  $_POST['question9'];
	$q10 =  $_POST['question10'];
	$q11 =  $_POST['question11'];
	$q12 =  $_POST['question12'];
	$q13 =  $_POST['question13'];
	$q14 =  $_POST['question14'];
	$q15 =  $_POST['question15'];


	$a1 = 1;
	$a2 = 3;
	$a3 = 1;
	$a4 = 2;
	$a5 = 2;
	$a6 = 1;
	$a7 = 2;
	$a8 = 1;
	$a9 = 2;
	$a10 = 1;
	$a11 = 3;
	$a12 = 2;
	$a13 = 1;
	$a14 = 1;
	$a15 = 1;
	
$points = 0;

$points = ($q1 == $a1) ? ++$points : $points;
$points = ($q2 == $a2) ? ++$points : $points;
$points = ($q3 == $a3) ? ++$points : $points;
$points = ($q4 == $a4) ? ++$points : $points;
$points = ($q5 == $a5) ? ++$points : $points;
$points = ($q6 == $a6) ? ++$points : $points;
$points = ($q7 == $a7) ? ++$points : $points;
$points = ($q8 == $a8) ? ++$points : $points;
$points = ($q9 == $a9) ? ++$points : $points;
$points = ($q10 == $a10) ? ++$points : $points;
$points = ($q11 == $a11) ? ++$points : $points;
$points = ($q12 == $a12) ? ++$points : $points;
$points = ($q13 == $a13) ? ++$points : $points;
$points = ($q14 == $a14) ? ++$points : $points;
$points = ($q15 == $a15) ? ++$points : $points;


$questions = 15;

$score = round(($points / $questions) *100);

$TechID = $_SESSION['TechID'];
$DateTime = date("Y-m-d H:i:s");

mysqlQuery("INSERT INTO Hallmark_Test (tech_id, date_taken, score, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15) VALUES ('$TechID', '$DateTime', '$score', '$q1', '$q2', '$q3', '$q4', '$q5', '$q6', '$q7', '$q8', '$q9', '$q10', '$q11', '$q12', '$q13', '$q14', '$q15')");

echo "<div class=\"formBox\">";

if ($score == 100) {
	mysqlQuery("UPDATE TechBankInfo SET Cert_Hallmark_POS = '1' WHERE TechID = '$TechID'");
	Core_Database_MysqlSync::sync(Core_Database::TABLE_TECH_BANK_INFO, "TechID", $TechID);
	echo "<div class=\"verdana2bold\" style=\"color:green\">Contratulations, you passed the FLS Hallmark POS Training Exam!</div><div class=\"formRow1\"></div><br />";
	echo "<div class=\"verdana2bold\">SCORE: $score %</div>";
	
	Send_Emails($TechID);

} else {
	echo "<div class=\"verdana2bold\" style=\"color:#F00\">Sorry, you do not have a passing score</div><br />";	
	echo "<div class=\"formRow2\">You will need to re-take the exam and will not be activated for the FLS Hallmark Program until you have scored 100%.</div>";
	echo "<div class=\"formRow2\">Please Click <a href=\"Hallmark_Training.php\">here</a> to re-take the test.</div><br />";
	echo "<div class=\"verdana2bold\">SCORE: $score %</div>";
	echo "</div></div></div></div>";
}
echo "</div></div></div></div>";

} ?>

<p>
  <?php require ("../../../../footer.php"); ?>
</p>