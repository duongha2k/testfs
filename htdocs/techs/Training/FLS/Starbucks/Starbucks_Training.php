<?php
	ini_set("display_errors", 1); 
?>

<?php $page = 'techs'; ?>
<?php $option = 'home'; ?>
<?php 
	require_once ("../../../../headerStartSession.php");

	$loggedIn   = ( !empty($_SESSION['loggedIn']) ) ? $_SESSION['loggedIn'] : 'no';

    if($loggedIn !== 'yes'){
        header( 'Location: http'.($_SERVER['HTTPS']=='on' ? 's':'').'://'.$_SERVER['HTTP_HOST'].'/techs/logIn.php?flsupport=true' ); // Redirect to logIn.php
   		exit;
    }
?>

<?php  require ("../../../../header.php"); ?>
<?php  require ("../../../../navBar.php");  ?>
<?php  require ("../../../../library/mySQL.php"); ?>

<script type="text/javascript">

$(document).ready(function(){
	$("body").css("width", "850px").css("min-width", "850px");
	
});

</script>
<style type="text/css">
	#emailBlast {
		text-align: center;
	}
	.verdana2 {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		clear: both;
	}

	.verdana2bold {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		font-weight: bold;
		clear: both;
	}

	.fieldGroup {
		padding-bottom: 5px;
		border-bottom: 2px solid #2A497D;
	}

	.formBox, .formBoxFooter {
		margin: 15px auto 0px auto;
		padding: 15px;
		width: 920px;
		color: #2A497D;
		font-size: 12px;
		border: 2px solid #2A497D;
	}

	.formBox input {
		font-size: 12px;
		height: 14px;
	}

	.formBoxFooter {
		background-color: #BACBDF;
		text-align: center;
		margin: 0px auto 10px auto;
		padding: 5px 15px;
		border-top: none;
	}
	.formRow3, .formRow4, .formRow5, .formRow6 {
		width: 100%;
		clear: both;
	}
	.formRow3 div {
		width: 33%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow4 div {
		width: 25%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow5 div {
		width: 20%;
		float: left;
		margin-bottom: 5px;
	}

	.formRow6 div {
		width: 16%;
		float: left;
		margin-bottom: 5px;
	}

	.resultsTable {
		width: 80%;
		margin: 10px auto 0px auto;
	}

	.resultsTable thead {
		text-align: center;
		color: #2A497D;
		border: 1px solid #2A497D;
		background-color: #A0A8AA;
		cursor: default;
	}

	.sortAble, #nextPageBtn, #prevPageBtn {
		cursor: pointer;
	}

	.sortAbleSelected {
		cursor: pointer;
		background-color: #FF9900;
		color: #FFFFFF;
		font-weight: bold;
	}

	.resultsTable tfoot {
		text-align: center;
		color: #000000;
		border: 1px solid #2A497D;
		background-color: #A0A8AA;
	}

	.resultsTable td {
		padding: 3px 5px;
	}

	.resultsTable .evenRow {
		background-color: #E9EEF8;
	}

	.resultsTable .oddRow {
		background-color: #FBFCFD;
	}

	.resultsTable .preferred {
		background-color: #FFFF8C;
	}
    .resultsTable .techGrey {
		background-color: #DEDEDE;
	}
    .resultsTable .techYellow {
		background-color: #FFFF8C;
	}
    .resultsTable .techRed {
		background-color: #FFBBBB;
	}
    .resultsTable .techGreen {
		background-color: #89FF89;
	}

	.searchControls {
		margin-top: 10px;
		text-align: center;
	}

	.noResultsDiv {
		margin-top: 10px;
		text-align: center;
	}
	
	label input {margin-right:10px;}
	.link_button { width: 140px; border:none; background: url(/widgets/images/button_middle.png) left top no-repeat; font: normal 10px/10px Verdana, Arial, Helvetica, sans-serif; padding: 4px 0 8px; color: #385c7e; cursor: pointer; }
</style>

<?php

if (!isset($_POST["sendTest"])) {

?>


<form id="testForm" name="testForm" action="<?php echo $_SERVER['PHP_SELF'] . "?v=" . $_GET["v"]?>&snp=1" method="post">
<div class="formBox">
<div class="verdana2bold" align="center">FLS Starbucks Certification Exam</div><br />
<div class="formRow1">This is an open book exam for the FLS Starbucks Certification. You have to take the FLS Starbucks Certification Exam to be certified to do future FLS Starbucks installations. You can use this document to assist you in taking this exam. </div><br />
<div class="formRow1"><strong>Note: </strong>You must have a score of 100% to be activated for the FLS Starbucks Program.</div><br />
  <div class="formRow1">
    <ul>
      <li><a href="Starbucks%20Technology%20Refresh%20Installation%20Guide%20Ver2.19.pdf">Starbucks Training Material</a></li>
      <li>&nbsp;</li>
      <li><a href="ftp://repair:2kGYRetr@ftp.ftxs.fujitsu.com/repair/Starbucks_Video_02_09v1.wmv" target="_blank">Starbucks Training Video</a><br>
      </li>
      (Must be using Internet Explorer)
    </ul>
    <hr /><br />
    <style type="text/css">  
ul {   margin-bottom: 25px;   margin-left: 50px;  }  li a {    color: red; } 
</style>
</div>
    <div class="verdana2bold">1) What type of equipment will be replaced during this project?</div><br />
    <div class="formRow1">
        <div><label><input id="question1" name="question1" value="1"  type="radio">Bean Scales and miscellaneous peripheral devices</label></div>
        <div><label><input id="question1" name="question1" value="2"  type="radio">Point of Sale and/or back office equipment</label></div>
        <div><label><input id="question1" name="question1" value="3"  type="radio">Network and/or telecom equipment</label></div>
        <div><label><input id="question1" name="question1" value="4"  type="radio">Video surveillance equipment</label></div>
    </div>
    <br />
    <div class="verdana2bold">2) Upon onsite arrival tech should introduce self to manager on duty (MOD), what is the preferred strategy to accomplish this if the store is busy?</div><br />
    <div class="formRow2">        
        <div><label><input id="question2" name="question2" value="1"  type="radio">Call the site ahead and inform them you are arriving</label></div>
        <div><label><input id="question2" name="question2" value="2"  type="radio">Enter the store and attempt to flag down the MOD</label></div>
        <div><label><input id="question2" name="question2" value="3"  type="radio">Go directly to the back room and look for the MOD in the office</label></div>
        <div><label><input id="question2" name="question2" value="4"  type="radio">Wait in queue in the customer line for assistance rather than trying to flag down the MOD</label></div>
    </div>
    <br />
   <div class="verdana2bold">3) Part of the cleanup on this project involves organizing/tidying cables, which cables need attention?</div><br />
   <div class="formRow2">        
        <div><label><input id="question3" name="question3" value="1"  type="radio">Front end register cables only</label></div>
        <div><label><input id="question3" name="question3" value="2"  type="radio">Back office cables only.</label></div>
        <div><label><input id="question3" name="question3" value="3"  type="radio">Network cables only</label></div>
        <div><label><input id="question3" name="question3" value="4"  type="radio">All of the above</label></div>
	</div>
    <br />
   <div class="verdana2bold">4) During the cable cleanup steps, how much time should be spent?</div><br />
    <div class="formRow2">        
        <div><label><input id="question4" name="question4" value="1"  type="radio">Try to limit the time to a reasonable time frame, it doesn't need to be perfect</label></div>
        <div><label><input id="question4" name="question4" value="2"  type="radio">As long as it takes until the MOD approves of the work</label></div>
        <div><label><input id="question4" name="question4" value="3"  type="radio">Spend no more than 45 minutes to 1 hour total on cabling</label></div>
        <div><label><input id="question4" name="question4" value="4"  type="radio">No additional time should be spent on cabling</label></div>
	</div>
    <br />
   <div class="verdana2bold">5) There is a page in the manual with an overview of Retrofit steps that can be used as a quick guide to ensure all steps have been completed?</div><br />
    <div class="formRow2">        
        <div><label><input id="question5" name="question5" value="1"  type="radio">True</label></div>
        <div><label><input id="question5" name="question5" value="2"  type="radio">False</label></div>
	</div>
    <br />
   <div class="verdana2bold">6) There are check points listed throughout the document. What is the purpose of these check points?</div><br />
    <div class="formRow2">        
        <div><label><input id="question6" name="question6" value="1"  type="radio">These are the specific points in which the MOD will test that specific device's functionality</label></div>
        <div><label><input id="question6" name="question6" value="2"  type="radio">These are the specified break times for techs</label></div>
        <div><label><input id="question6" name="question6" value="3"  type="radio">These are meant to be guides that will help direct you on where to skip to in the documentation if a specific section or change is not applicable at a certain store</label></div>
        <div><label><input id="question6" name="question6" value="4"  type="radio">These are in place for the help desk only and have no meaning to the onsite technician</label></div>
	</div>
    <br />
   <div class="verdana2bold">7) Why is it important to note if the old Manager's Work Station (MWS) has a Matrox Video Card?</div><br />
    <div class="formRow2">        
        <div><label><input id="question7" name="question7" value="1"  type="radio">The Matrox video card is no longer compatible and will need to be replaced with a different video card</label></div>
        <div><label><input id="question7" name="question7" value="2"  type="radio">Matrox video card will need to be removed from the old MWS and installed in the new MWS, drivers/software will also need to be loaded</label></div>
        <div><label><input id="question7" name="question7" value="3"  type="radio">If the old MWS already has a Matrox video card then that is already the newest MWS and it does not need to be replaced</label></div>
        <div><label><input id="question7" name="question7" value="4"  type="radio">If a Matrox video card is present tech will need to install the additional video RAM on the video card</label></div>
	</div>
    <br />
   <div class="verdana2bold">8) What is the purpose of step 5.20?</div><br />
    <div class="formRow2">        
        <div><label><input id="question8" name="question8" value="1"  type="radio">This step verifies the integrity of the data from the MWS transfer</label></div>
        <div><label><input id="question8" name="question8" value="2"  type="radio">This step configures the network settings for the registers, without this step the registers will be stuck in offline mode</label></div>
        <div><label><input id="question8" name="question8" value="3"  type="radio">This step tests the functionality of the Matrox video card, if it exists, to ensure that it is properly configured for full video support</label></div>
        <div><label><input id="question8" name="question8" value="4"  type="radio">This is the final checkout with the Command Center before leaving site</label></div>
	</div>
    <br />
   <div class="verdana2bold">9) During the Final Walkthrough, which of the below steps are required?</div><br />
    <div class="formRow2">        
        <div><label><input id="question9" name="question9" value="1"  type="radio">Make sure all items for disposal are boxed, labeled, and stacked for pickup</label></div>
        <div><label><input id="question9" name="question9" value="2"  type="radio">Make sure all work areas are clean</label></div>
        <div><label><input id="question9" name="question9" value="3"  type="radio">Make sure all trash is disposed in the outside trash dumpster</label></div>
        <div><label><input id="question9" name="question9" value="4"  type="radio">All of the above</label></div>
	</div>
    <br />
	<div class="verdana2bold">10) The sign off sheet is optional and faxing of that sheet to the specified fax number is not important upon completion of the job?</div><br />
    <div class="formRow2">        
        <div><label><input id="question10" name="question10" value="1"  type="radio">True</label></div>
        <div><label><input id="question10" name="question10" value="2"  type="radio">False</label></div>
	</div>
	</div>      
    <div class="formBoxFooter">
    <div><input id="sendTest" name="sendTest" value="Submit Test" class="link_button" type="submit" /></div>
    </div>
 </form>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>

<?php

} else {
	$q1 =  $_POST['question1'];
	$q2 =  $_POST['question2'];
	$q3 =  $_POST['question3'];
	$q4 =  $_POST['question4'];
	$q5 =  $_POST['question5'];
	$q6 =  $_POST['question6'];
	$q7 =  $_POST['question7'];
	$q8 =  $_POST['question8'];
	$q9 =  $_POST['question9'];
	$q10 =  $_POST['question10'];

	$a1 = 2;
	$a2 = 4;
	$a3 = 2;
	$a4 = 1;
	$a5 = 1;
	$a6 = 3;
	$a7 = 2;
	$a8 = 1;
	$a9 = 4;
	$a10 = 2;
	
$points = 0;

$points = ($q1 == $a1) ? ++$points : $points;
$points = ($q2 == $a2) ? ++$points : $points;
$points = ($q3 == $a3) ? ++$points : $points;
$points = ($q4 == $a4) ? ++$points : $points;
$points = ($q5 == $a5) ? ++$points : $points;
$points = ($q6 == $a6) ? ++$points : $points;
$points = ($q7 == $a7) ? ++$points : $points;
$points = ($q8 == $a8) ? ++$points : $points;
$points = ($q9 == $a9) ? ++$points : $points;
$points = ($q10 == $a10) ? ++$points : $points;

$questions = 10;

$score = round(($points / $questions) *100);

$TechID = $_SESSION['TechID'];
$DateTime = date("Y-m-d H:i:s");

mysqlQuery("INSERT INTO Starbucks_Test (tech_id, date_taken, score, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10) VALUES ('$TechID', '$DateTime', '$score', '$q1', '$q2', '$q3', '$q4', '$q5', '$q6', '$q7', '$q8', '$q9', '$q10')");

echo "<div class=\"formBox\">";

if ($score == 100) {
	mysqlQuery("UPDATE TechBankInfo SET Starbucks_Cert = '1' WHERE TechID = '$TechID'");
	Core_Database_MysqlSync::sync(Core_Database::TABLE_TECH_BANK_INFO, "TechID", $TechID);
	echo "<div class=\"verdana2bold\" style=\"color:green\">Contratulations, you passed the FLS Starbucks Certification Exam!</div><div class=\"formRow1\"></div><br />";
	echo "<div class=\"verdana2bold\">SCORE: $score %</div>";
	
	Send_Emails($TechID);

} else {
	echo "<div class=\"verdana2bold\" style=\"color:#F00\">Sorry, you do not have a passing score</div><br />";	
	echo "<div class=\"formRow2\">You will need to re-take the exam and will not be activated for the FLS Starbucks Program until you have scored 100%.</div>";
	echo "<div class=\"formRow2\">Please Click <a href=\"Starbucks_Training.php\">here</a> to re-take the test.</div><br />";
	echo "<div class=\"verdana2bold\">SCORE: $score %</div>";
	echo "</div></div></div></div>";
}
echo "</div></div></div></div>";

} ?>

<p>
  <?php require ("../../../../footer.php"); ?>
</p>