<?php



?>
<script src="/widgets/js/jquery.min.js"></script>

<script type="text/javascript">
$(document).ready(function () {
	
	$('#techLoginForm').submit(function(event) {
		event.preventDefault();
		
	  	var formData = $(this).serializeArray();
	   
	   var idx;
	   var query = "";
	   
	   for ( idx in formData ) {
           if ( formData[idx].value ) {
               query += '&' + formData[idx].name + '=' + encodeURIComponent(formData[idx].value).replace(/\~/g, '%7E').replace(/\!/g, '%21').replace(/\'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29');
           }
       }

	    l  = location.protocol + '//' + location.host;
	    l += '/techs/setTechLoginSession.php?v=true';
        l += query;
	   
	    location = l; 
	});

	var reqLogin = '<?=$_REQUEST['login']?>';
	
	if(reqLogin == "false"){
		$("#authFailed").show();
	}else{
		$("#authFailed").hide();
	}
	
});
</script>

<p id="authFailed">
Authentication failed. The information you have provided cannot be authenticated. Check your login information and try again.
</p>
<div id="content" style="width: 586px; height: 273px; margin-right:auto; margin-left: auto;">
<form id="techLoginForm" method="post"  style="margin: 0px;">
	<input type="hidden" value="C4J9D4I2C2E9J3E8C2" name="appKey"><input type="hidden" value="http://flsupport.com/26.html" name="pathname">
	<input type="hidden" value="0" name="PrevPageID">
	<input type="hidden" value="1" name="flsupport" />
	<table cellspacing="0" style="border: 2px solid rgb(42, 73, 125); border-collapse: collapse;">
	<tbody>
	<tr style="background-color: rgb(255, 255, 255); padding: 7px;">
		<td style="text-align: left; vertical-align: top; padding: 5px 10px; color: rgb(42, 73, 125); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal;" colspan="2"><font size="2" face="Arial"> Here is where you access the unedited documents needed for Service Calls, documents such as: The checklist, CSR form, PRF, Parts Processing Guide, installation manuals, etc.  </font>
			<p><font size="2" face="Arial"> Login below by entering the same User &amp; Pass information used to login to your Field Solutions account. Don't remember your login? <a target="_blank&quot;" href="http://www.fieldsolutions.com/51.html"><font size="2" face="Arial" color="#0062c4">Click here</font></a> </font></p><p><font size="2" face="Arial"> <b>Unedited Docs Login:</b> </font></p><p></p>
		</td>
	</tr>
	<tr style="background-color: rgb(255, 255, 255);">
		<td style="text-align: left; vertical-align: top; width: auto; white-space: normal; padding: 2px 5px; color: rgb(42, 73, 125); font-size: 10px; font-family: Verdana; font-style: normal; font-weight: normal;">
			<label for="xip_UserName">Tech User Name:</label>
		</td>
		<td style="text-align: left; vertical-align: top; padding: 2px 5px; white-space: nowrap;">
			<input type="text" value="" maxlength="100" size="20" id="UserName" name="UserName" style="color: rgb(0, 0, 0); font-size: 10px; font-family: Verdana; font-style: normal; font-weight: normal;">
		</td>
	</tr>
	<tr style="background-color: rgb(255, 255, 255); padding: 7px;">
		<td style="text-align: left; vertical-align: top; width: auto; white-space: normal; padding: 2px 5px; color: rgb(42, 73, 125); font-size: 10px; font-family: Verdana; font-style: normal; font-weight: normal;">
			<label for="xip_TR_Master_List_Password">Tech Password:</label>
		</td>
		<td style="text-align: left; vertical-align: top; padding: 2px 5px; white-space: nowrap;">
			<input type="password" value="" maxlength="20" size="20" id="Password" name="Password" style="color: rgb(0, 0, 0); font-size: 10px; font-family: Verdana; font-style: normal; font-weight: normal; padding: 1px;"></td>
	</tr>
	<tr style="background-color: rgb(255, 255, 255);">
		<td style="padding: 5px 0px; text-align: center; vertical-align: middle; border: 2px solid rgb(42, 73, 125); background-color: rgb(186, 203, 223);" colspan="2">
			<input type="submit" id="xip_datasrc_Unedited_Docs" value="Login" name="xip_datasrc_Unedited_Docs" style="color: rgb(255, 255, 255); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold; text-align: center; vertical-align: middle; border: 1px solid rgb(42, 73, 125); background-color: rgb(42, 73, 125); width: auto; height: auto; margin: 0pt 3px;">
		</td>
	</tr>
	</tbody>
	</table>
</form>
</div>