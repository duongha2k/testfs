<?php $page = 'techs'; ?>
<?php $option = 'home'; ?>
<?php 
require_once ("../../../headerStartSession.php");

$loggedIn   = ( !empty($_SESSION['loggedIn']) ) ? $_SESSION['loggedIn'] : 'no';

    if($loggedIn !== 'yes'){
        header( 'Location: http'.($_SERVER['HTTPS']=='on' ? 's':'').'://'.$_SERVER['HTTP_HOST'].'/techs/logIn.php?flsupport=true' ); // Redirect to logIn.php
   		exit;
    }
?>
<?php  require ("../../../header.php"); ?>
<?php  require ("../../../navBar.php");  ?>

<br /><br />
<TABLE cellSpacing=0 cellPadding=0 width=525 border=0>
<TBODY>
<TR>
<TD colSpan=2>
<FONT face=Arial size=2> It is very important that you review and print many of the documents on this page. You should put them in a folder and bring them with you on all Service Calls that you run. Please discard similar edited documents that you may have accessed prior to being activated and being granted access the UNEDITED documents in this section. <P>
<B>Note:</B> Most documents on the FLS web site are in PDF. Meaning, if you do not already have it, you will need the <A HREF="http://www.adobe.com/products/acrobat/readstep2.html"target="_blank"></B><FONT SIZE="2" FACE="Arial" COLOR="#0062c4">Adobe Reader</FONT></A> software whenever you see the red icon next to the document you are attempting to view and/or print. You can download the required Adobe Reader software for free by <A HREF="http://www.adobe.com/products/acrobat/readstep2.html"target="_blank"></B><FONT SIZE="2" FACE="Arial" COLOR="#0062c4">clicking here</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/excel.jpg" WIDTH=22 HEIGHT=24 BORDER=0 ALT=""> The Excel icon next to any documents means that you will also need MS Excel to view/print that particular document.<BR>
<BR>
<B>Review, print and make copies</B>

<P> Review and print the documentation below. Make several copies of each document as you will need to take one of each on every call that you run. <P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/doc/FLS_Call_Checklist.doc"target=_blank"><FONT face=Arial color=#0062c4 size=2>Service Call Checklist</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/csr.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>CSR (Customer Service Report)</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/doc/PDF_Sample.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>PRF (Part Return Form)</FONT></A>
<P>
<B>Instructions to review, understand, print and bring with you on all calls - </B>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/doc/Part_Processing_Guide.doc"target=_blank"><FONT face=Arial color=#0062c4 size=2>Parts Processing Guide</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/doc/Calling_into_the_Fujitsu_IVR_System.doc"target=_blank"><FONT face=Arial color=#0062c4 size=2>IVR Telephone System Instructions</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/csrformguide.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>Guide to Filling out the CSR Form</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/scscript.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>Service Call Script</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/excel.jpg" WIDTH=22 HEIGHT=24 BORDER=0 ALT=""> <A href="http://flsupport.com/doc/work_order_closing_codes.xls"target=_blank"><FONT face=Arial color=#0062c4 size=2>Code Reference Guide</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/excel.jpg" WIDTH=22 HEIGHT=24 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/retpartsguide.xls"target=_blank"><FONT face=Arial color=#0062c4 size=2>FTXS Retail Parts Guide</FONT></A>
<P>
<B>Equipment Manuals/Instructions </B>
<P> The documentation (manuals) below, are instructions for servicing certain types of equipment. You do not necessarily need to print them all out right away but should at least review them and make a note that they are here in the event you are ever dispatched on a call involving any of the devices. If/when you are dispatched on a call involving any of the devices below, be sure to print out the instructions and bring them with you on the call. <P>
<HR> The link below is to a a video that may require Windows Media. <P>
<IMG SRC="http://flsupport.com/images/wmp.jpg" WIDTH=75 HEIGHT=56 BORDER=0 ALT="">
<A href="http://tinyurl.com/ywzcdg"target=_blank"><FONT face=Arial color=#0062c4 size=2></FONT><FONT face=Arial color=#0062c4 size=2>Staples LogoWorks Project</FONT></A>
<HR> <P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/ROSS_ TP2000_ Keyboards.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>ROSS TP2000 Keyboards</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/Keyboard_Instructions.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>TP2000 Keyboard Programming Instructions</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/Everest_EFT_download.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>Everest EFT Download</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/Everest_reset.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>Everest Terminal Reset</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/Installing_Gold_Disk_in_TP2K.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>Installing Gold Disk in TP2K</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/NCR_POS-DynaKey_Keyboard_Replacement.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>NCR POS - DynaKey Keyboard Replacement</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/DIEBOLD_CASSETTES_FIELD_PROCESS.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>Diebold Casettes - Field Process</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/Phaser_ 8400.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>Phaser 8400 Color Printer Instructions</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/LockRemoval.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>Lock Removal</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/verifone-omni-470-install-download.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>Verifone Omni 470 Install/Download Procedures</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/lexmark-optra-r-printers.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>Lexmark Optra-R Printers - Reset Instructions</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/VerifoneMX870.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>Verifone MX870</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/LS4004-hand-scanner-TJ.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>LS-4004 Hand Scanner - Trader Joe's</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/V4symbolLS4071P370scan-1.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>Symbol LS-4071 Hand Scanner Service Guide</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/hm_scnr_barcodes.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>Scanner Bar Codes</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/LS-6005_6005i_ibm_4683_4694.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>LS-LS-6005 / LS-6005i Hand Scanners on IBM 4683/4694</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/Symbol_Cyclone_M2000_programming.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>Symbol Cyclone M2000 Hand Scanner on IBM 4683/4694</FONT></A>
<P>
<B>Fujitsu F53 Instructions</B>
<P> Below, are links to instructions (Part 1 & 2) explaining how to perform the double-detect adjustment on a UScan F53. This can eliminate a whole unit swap. It is IMPERATIVE that you have the Thickness Gauge Tool, USA0204711, in order to perform this adjustment, which can be found for less than $3 at any auto or hardware store. <P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/F53_1.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>UScan F53 (Part 1)</FONT></A>
<P>
<IMG SRC="http://flsupport.com/images/pdf.JPG" WIDTH=23 HEIGHT=23 BORDER=0 ALT=""> <A href="http://flsupport.com/uedocs2018/F53-2.pdf"target=_blank"><FONT face=Arial color=#0062c4 size=2>UScan F53 (Part 2)</FONT></A>
<P>
<B>Technical Documentation Via Fujitsu</B>
<P> Listed below are two links to Fujitsu's server where you are able to access Technical instructions for different types of equipment. There is really nothing "organized" or anything specific we would like to point out. Rather, a collection of documents and instructions that you can browse through, print out and create your own technical manual(s) if you would be more comfortable running calls with this information or need some information for a project you are working on. <P> You will also be provided with a name and number of a full-time Fujitsu Tech when you are dispatched on a call. You are required to call that Technician any time you have technical questions or problems. Therefore, it is NOT required or necessary that you review, download and/or print anything in this section. <P> Important note on accessing documents of the FTXS server (link) directly below: After attempting to login, it is possible that you will get an error box. If you do, click on the "Okay" button and look for the "ftp.ftxs.fujitsu.com" link in the upper left and click on that. You will then be able to access all of the documentation. <P>
<A href="ftp://ftp.ftxs.fujitsu.com/repair"target=_blank"><FONT face=Arial color=#0062c4 size=2>ftp://ftp.ftxs.fujitsu.com/repair</FONT></A> - Login required, see user/pass info. below. <P> Username: repair <BR> Password: 2kGYRetr <P> Here's another good source of technical documentation/instructions. You do not need a login (user/pass) for this link, just click on the link below for direct access. <P>
<A href="ftp://repair:2kGYRetr@ftp.ftxs.fujitsu.com/repair/Index.htm"target=_blank"><FONT face=Arial color=#0062c4 size=2>ftp://repair:2kGYRetr@ftp.ftxs.fujitsu.com/repair/Index.htm</FONT></A>
</FONT></TD></TR></TBODY></TABLE>
<?php require ("../../../footer.php"); 