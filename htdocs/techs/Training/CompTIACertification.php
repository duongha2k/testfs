<?php $page = "techs"; ?>
<?php $option = "training"; ?>
<?php $selected = "fs" ?>
<?php $display = "fs" ?>
<?php require ("../../headerSimple.php"); ?>

<!-- Add Content Here -->
<style>
    .CompTIA_BG
    {
        background-image:url('/widgets/images/CompTIA_BG.jpg');
        background-repeat:no-repeat;
        height:90px;
    }
    .CompTIA_BorderBox_L
    {
        background-image:url('/widgets/images/CompTIA_BorderBox_L.png');
        background-repeat:no-repeat;
        padding-left: 10px;
        padding-top: 5px;
    }
    .CompTIA_BorderBox_R
    {
        background-image:url('/widgets/images/CompTIA_BorderBox_R.png');
        background-repeat:no-repeat;
        font-size:15px;
    }
    .CompTIA_header
    {
        font-size:15px;
        font-weight:bold;
        height:10px; 
        padding-bottom: 10px;
    }
</style>
<br/>
<div style="width:900px;height:500px;">
    <table cellSpacing="0" cellPadding="0" width="900px" border="0" style="text-align:left;">
        <tr>
            <td>
                <div id="headerLogo"></div>
            </td>
            <td width="10px"></td>
            <td style="text-align:right;">
                <img src="/widgets/images/CompTIA_Logo.png" alt=""/>
            </td>
        </tr>
        <tr class="CompTIA_BG">
            <td colSpan="3" style="padding-left:10px;padding-bottom:20px;color:white;">
                <span style="font-size: 18px;">Welcome to the </span><br/>
                <span style="font-size: 18px;font-weight: bold;">FieldSolutions - CompTIA Certification Program</span>
            </td>
        </tr>
        <tr style="vertical-align:top;">
            <td style="padding-left:5px;">
                <table cellSpacing="0" cellPadding="0" width="610px" border="0" style="text-align:left;">
                    <tr>
                        <td colspan="5" style="font-size:15px;padding-bottom:10px;">
                            As a leader in the electronics industry, FieldSolutions is committed to optimizing field service performance 
                            and expanding the opportunities available to independent technicians. With that in mind, we have partnered 
                            with CompTIA to offer discounts on training materials and exams for FieldSolutions technicians.
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" class="CompTIA_header">
                            Select Below for Great Saving<br/>
                            on the Industry Leading CompTIA Training and Certification
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="CompTIA_BorderBox_L">
                            <table cellSpacing="0" cellPadding="0" width="303px" border="0" style="text-align:left;">
                                <tr>
                                    <td class="CompTIA_header">
                                        Get 15% off<br/>
                                        CompTIA exam vouchers.
                                    </td>
                                    <td rowspan="3">
                                        <a target="_blank" href="https://affiliate.comptiastore.com/SearchResults.asp?Cat=188"><img src="/widgets/images/CompTIA_Save15.png" alt=""/></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Enter coupon code: VC22F304919
                                    </td>
                                <tr>
                                    <td style="padding-top:5px;padding-bottom:15px;">
                                        <a target="_blank" href="https://affiliate.comptiastore.com/SearchResults.asp?Cat=188"><img src="/widgets/images/CompTIA_ShopNow.png" alt=""/></a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="5px"></td>
                        <td colspan="2" class="CompTIA_BorderBox_L">
                            <table cellSpacing="0" cellPadding="0" width="303px" border="0" style="text-align:left;">
                                <tr>
                                    <td class="CompTIA_header">
                                        Get 10% off <br/>
                                        CompTIA training material.
                                    </td>
                                    <td rowspan="3">
                                        <a target="_blank" href="https://affiliate.comptiastore.com/SearchResults.asp?Cat=188"><img src="/widgets/images/CompTIA_Save10.png" alt=""/></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Enter coupon code: CC22F735210
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top:5px;padding-bottom:15px;">
                                        <a target="_blank" href="https://affiliate.comptiastore.com/SearchResults.asp?Cat=188"><img src="/widgets/images/CompTIA_ShopNow.png" alt=""/></a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" class="CompTIA_header">
                            Recommended certifications include:  
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <table cellSpacing="0" cellPadding="0" width="100%" border="0" style="text-align:left;">
                                <col width="25%">
                                <col width="25%">
                                <col width="25%">
                                <col width="25%">
                                <tr style="vertical-align: top; font-size:10px;">
                                    <td>
                                        <table cellSpacing="0" cellPadding="0" width="100%" border="0" style="text-align:left;">
                                            <tr>
                                                <td style="vertical-align: bottom; height:50px;">
                                                    <img height="50px" src="/widgets/images/CompTIA_A.png" alt=""/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left:5px;padding-right:5px;">
                                                    <a height="50px" target="_blank" href="http://certification.comptia.org/getCertified/certifications/a.aspx" >CompTIA A+</a>: Preventative maintenance, basic networking, installation, troubleshooting and communication skills
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table cellSpacing="0" cellPadding="0" width="100%" border="0" style="text-align:left;">
                                            <tr>
                                                <td style="vertical-align: bottom; height:50px;">
                                                    <img height="50px" src="/widgets/images/CompTIA_PDI.png" alt=""/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left:5px;padding-right:5px;">
                                                    <a target="_blank" href="http://certification.comptia.org/getCertified/certifications/pdi.aspx" >CompTIA PDI+</a>: Basic electromechanical components and tools, print engine and scan processes, color theory and networking
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table cellSpacing="0" cellPadding="0" width="100%" border="0" style="text-align:left;">
                                            <tr>
                                                <td style="vertical-align: bottom; height:50px;">
                                                    <img height="33px" src="/widgets/images/CompTIA_Server.png" alt=""/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left:5px;padding-right:5px;">
                                                    <a target="_blank" href="http://certification.comptia.org/getCertified/certifications/server.aspx" >CompTIA Server+</a>: Building, maintaining and troubleshooting server hardware and software
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table cellSpacing="0" cellPadding="0" width="100%" border="0" style="text-align:left;">
                                            <tr>
                                                <td style="vertical-align: bottom; height:50px;">
                                                    <img height="33px" src="/widgets/images/CompTIA_Network.gif" alt=""/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left:5px;padding-right:5px;">
                                                    <a target="_blank" href="http://certification.comptia.org/getCertified/certifications/network.aspx" >CompTIA Network+</a>: Network technologies, installation and configuration, media and topologies, management, and security
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                </table>
            </td>
            <td width="10px"></td>
            <td class="CompTIA_BorderBox_R">
                <div style="padding-left:14px;padding-top:10px;padding-bottom:5px;padding-right:10px;">
                    <span style="color: red; font-size:20px;">CompTIA Training and Certification</span><br/><div style="width:100%;height:10px;"></div>
                    As the global IT industry association, CompTIA builds vendor-neutral certifications that measure foundational skills.<br/><div style="width:100%;height:10px;"></div>
                    <span style="font-weight: bold;">You learn,</span> gaining critical knowledge and skills.<br/><div style="width:100%;height:10px;"></div>
                    <span style="font-weight: bold;">You certify,</span> by passing the test and gaining the credential that proves you have the knowledge and skills to meet the increasing complexities and demands of the industry.<br/><div style="width:100%;height:10px;"></div>
                    <span style="font-weight: bold;">You perform</span> at an optimal level.<br/><div style="width:100%;height:10px;"></div>
                    <span style="font-size: 10px;">For more information on this  program, contact FieldSolutions at: <a href="mailto:support@FieldSolutions.com">support@FieldSolutions.com</a>, 952-288-2500, option 1</span>
                </div>
            </td>
        </tr>
    </table >
</div>