<?php
	ini_set("display_errors", 1); 
?>
<?php $page = 'techs'; ?>
<?php $option = 'home'; ?>
<?php 
	require_once ("../../../headerStartSession.php");

	$loggedIn   = ( !empty($_SESSION['loggedIn']) ) ? $_SESSION['loggedIn'] : 'no';

    if($loggedIn !== 'yes'){
        header( 'Location: http'.($_SERVER['HTTPS']=='on' ? 's':'').'://'.$_SERVER['HTTP_HOST'].'/techs/logIn.php?' ); // Redirect to logIn.php
   		exit;
    }
?>
<?php //require_once("../../../clients/function.debug.php"); __debug(true); ?>
<?php  require ("../../../header.php"); ?>
<?php  require ("../../../navBar.php");  ?>
<?php  require ("../../../library/mySQL.php"); ?>
<script type="text/javascript">

$(document).ready(function(){
	$("body").css("width", "850px").css("min-width", "850px");
	
});

</script>
<style type="text/css">
#mainContainer {
	min-width: 800px;
	width: 850px;
}
</style>
<style type="text/css">
#emailBlast {
	text-align: center;
}
.verdana2 {
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size: small;
	clear: both;
}
.verdana2bold {
	font-family:Verdana, Arial, Helvetica, sans-serif;
	font-size: small;
	font-weight: bold;
	clear: both;
}
.fieldGroup {
	padding-bottom: 5px;
	border-bottom: 2px solid #2A497D;
}
.formBox, .formBoxFooter {
	margin: 15px auto 0px auto;
	padding: 15px;
	width: 920px;
	color: #2A497D;
	font-size: 12px;
	border: 2px solid #2A497D;
}
.formBox input {
	font-size: 12px;
	height: 14px;
}
.formBoxFooter {
	background-color: #BACBDF;
	text-align: center;
	margin: 0px auto 10px auto;
	padding: 5px 15px;
	border-top: none;
}
.formRow3, .formRow4, .formRow5, .formRow6 {
	width: 100%;
	clear: both;
}
.formRow3 div {
	width: 33%;
	float: left;
	padding: 0px;
	margin: 0px 0px 5px 0px;
}
.formRow4 div {
	width: 25%;
	float: left;
	padding: 0px;
	margin: 0px 0px 5px 0px;
}
.formRow5 div {
	width: 20%;
	float: left;
	margin-bottom: 5px;
}
.formRow6 div {
	width: 16%;
	float: left;
	margin-bottom: 5px;
}
.resultsTable {
	width: 80%;
	margin: 10px auto 0px auto;
}
.resultsTable thead {
	text-align: center;
	color: #2A497D;
	border: 1px solid #2A497D;
	background-color: #A0A8AA;
	cursor: default;
}
.sortAble, #nextPageBtn, #prevPageBtn {
	cursor: pointer;
}
.sortAbleSelected {
	cursor: pointer;
	background-color: #FF9900;
	color: #FFFFFF;
	font-weight: bold;
}
.resultsTable tfoot {
	text-align: center;
	color: #000000;
	border: 1px solid #2A497D;
	background-color: #A0A8AA;
}
.resultsTable td {
	padding: 3px 5px;
}
.resultsTable .evenRow {
	background-color: #E9EEF8;
}
.resultsTable .oddRow {
	background-color: #FBFCFD;
}
.resultsTable .preferred {
	background-color: #FFFF8C;
}
.resultsTable .techGrey {
	background-color: #DEDEDE;
}
.resultsTable .techYellow {
	background-color: #FFFF8C;
}
.resultsTable .techRed {
	background-color: #FFBBBB;
}
.resultsTable .techGreen {
	background-color: #89FF89;
}
.searchControls {
	margin-top: 10px;
	text-align: center;
}
.noResultsDiv {
	margin-top: 10px;
	text-align: center;
}
label input {
	margin-right:10px;
}
.link_button {
	width: 140px;
	border:none;
	background: url(/widgets/images/button_middle.png) left top no-repeat;
	font: normal 10px/10px Verdana, Arial, Helvetica, sans-serif;
	padding: 4px 0 8px;
	color: #385c7e;
	cursor: pointer;
}
</style>
<?php

$TechID = $_SESSION['TechID'];

$q1 =  $_POST['question1'];
$q2 =  $_POST['question2'];
$q3 =  $_POST['question3'];
$q4 =  $_POST['question4'];

$q5 =  $_POST['question5'];
$q6 =  $_POST['question6'];
$q7 =  $_POST['question7'];
$q8 =  $_POST['question8'];
$q9 =  $_POST['question9'];
$q10 =  $_POST['question10'];
$q11 =  $_POST['question11'];
$q12 =  $_POST['question12'];
$q13 =  $_POST['question13'];


if (!isset($_POST["doSubmit"])) {

	if (isset($_POST["doSave"])) {
		$testID = mysqlFetchAssoc(mysqlQuery("SELECT id FROM Copier_Assessment WHERE tech_id = '".$TechID."'"));
		if ($testID) {
			$testID = $testID[0];
			$emTestID = $testID[id];
	
			mysqlQuery("UPDATE ServRight_ElectroMechTest SET a1 = '$q1', a2 = '$q2', a3 = '$q3', a4 = '$q4', a5 = '$q5', a6 = '$q6', a7 = '$q7', a8 = '$q8', a9 = '$q9', a10 = '$q10',
					   a11 = '$q11', a12 = '$q12', a13 = '$q13' WHERE id = " . $emTestID);
					   
		} else {
			$DateTime = date("Y-m-d H:i:s");
			mysqlQuery("INSERT INTO Copier_Assessment (tech_id, date_taken, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13)
			   VALUES ('$TechID', '$DateTime', '$q1', '$q2', '$q3', '$q4', '$q5', '$q6', '$q7', '$q8', '$q9', '$q10', '$q11', '$q12', '$q13'");

		}

	}
	
	
	$testID = mysqlFetchAssoc(mysqlQuery("SELECT id, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13 FROM Copier_Assessment WHERE tech_id = '".$TechID."'"));
	if ($testID) {
		$testID = $testID[0];
		$emTestID = $testID[id];
		$q1 = $testID[a1];
		$q2 = $testID[a2];
		$q3 = $testID[a3];
		$q4 = $testID[a4];
		$q5 = $testID[a5];
		$q6 = $testID[a6];
		$q7 = $testID[a7];
		$q8 = $testID[a8];
		$q9 = $testID[a9];
		$q10 = $testID[a10];
		$q11 = $testID[a11];
		$q12 = $testID[a12];
		$q13 = $testID[a13];

//if($q1 == "1"){echo "<p>Answer1: ". $q1 . "</p>";}
	}

?>
<form id="testForm" name="testForm" action="<?php echo $_SERVER['PHP_SELF'] . "?v=" . $_GET["v"]?>&snp=1" method="post">
  <div class="formBox">
    <div class="verdana2bold" align="center">Copier Skills Assessment</div>
    <br />
    <div class="formRow1">Completing this skills assessment will  make you more marketable to FieldSolutions’ clients looking for technicians with Copier service experience. The first four questions are designed to gauge your level and range of experience, and the next nine questions are a basic assessment of your copier service knowledge. A passing score is 75%. If you do not pass, you will be able to re-take this assessment twice.</div>
    <br />
  </div>
  <div class="formBox">
	<div class="verdana2bold">1) Select the type of Copiers you have worked on.</div>
    <br />
    <div class="formRow1">
      <div>
        <label>
          <input id="question1" name="question1" value="1"  type="radio"<?php if($q1 == 1){print " checked=\"checked\"";} ?>>
          Digital</label>
      </div>
      <div>
        <label>
          <input id="question1" name="question1" value="2"  type="radio"<?php if($q1 == 2){print " checked=\"checked\"";} ?>>
          Analog</label>
      </div>
      <div>
        <label>
          <input id="question1" name="question1" value="3"  type="radio"<?php if($q1 == 3){print " checked=\"checked\"";} ?>>
          Digital & Analog</label>
      </div>
    </div>
    <br />
    <div class="verdana2bold">2) Select ALL Brands of Copiers you have experience repairing.</div>
    <br />
    <div class="formRow2">
      <div>
        <p>
          <label>
            <input type="checkbox" name="Brands" value="Canon" id="Brands_0">
            Canon</label>
          <br>
          <label>
            <input type="checkbox" name="Brands" value="Richo" id="Brands_1">
            Ricoh/Savin/Lanier</label>
          <br>
          <label>
            <input type="checkbox" name="Brands" value="Konica Minolta" id="Brands_2">
            Konica Minolta</label>
          <br>
          <label>
            <input type="checkbox" name="Brands" value="Xerox" id="Brands_3">
            Xerox</label>
          <br>
          <label>
            <input type="checkbox" name="Brands" value="OCE" id="Brands_4">
            OCE</label>
          <br>
          <label>
            <input type="checkbox" name="Brands" value="Kyocera/Mita" id="Brands_5">
            Kyocera/Mita</label>
          <br>
          <label>
            <input type="checkbox" name="Brands" value="Gestetner" id="Brands_6">
            Gestetner</label>
          <br>
          <label>
            <input type="checkbox" name="Brands" value="Toshiba" id="Brands_7">
            Toshiba</label>
          <br>
          <label>
            <input type="checkbox" name="Brands" value="Pitney Bowes" id="Brands_8">
            Pitney Bowes</label>
          <br>
          <label>
            <input type="checkbox" name="Brands" value="Sharp" id="Brands_9">
            Sharp</label>
          <br>
          <label>
            <input type="checkbox" name="Brands" value="Panasonic" id="Brands_10">
            Panasonic</label>
          <br>
        </p>
    </div>
    <br />
    <div class="verdana2bold">3) How many Years of experience do you have working on Copiers?</div>
    <br />
    <div class="formRow2">
      <div>
        <label>
          <input id="question3" name="question3" value="1"  type="radio"<?php if($q3 == 1){print " checked=\"checked\"";} ?>>
          0-1 Year</label>
      </div>
      <div>
        <label>
          <input id="question3" name="question3" value="2"  type="radio"<?php if($q3 == 2){print " checked=\"checked\"";} ?>>
          1-4 Years</label>
      </div>
      <div>
        <label>
          <input id="question3" name="question3" value="3"  type="radio"<?php if($q3 == 3){print " checked=\"checked\"";} ?>>
          5-10 Years</label>
      </div>
      <div>
        <label>
          <input id="question3" name="question3" value="4"  type="radio"<?php if($q3 == 4){print " checked=\"checked\"";} ?>>
          10+ Years</label>
      </div>
        </div>
    <br />
      <div class="verdana2bold">4) Select ALL Brands of Copiers you have experience repairing.</div>
    <br />
    <div class="formRow2">
      <div>
        <p>
          <label>
            <input type="checkbox" name="Repairs" value="Canon" id="Repairs_0">
            Cleaning (rollers, doc feeder, sorters, trays, transport deck, paper path)</label>
          <br>
          <label>
            <input type="checkbox" name="Repairs" value="Developer Replacement" id="Repairs_1">
            Developer Replacement</label>
          <br>
          <label>
            <input type="checkbox" name="Repairs" value="Drum/Cleaning Blade Replacement" id="Repairs_2">
            Drum/Cleaning Blade Replacement</label>
          <br>
          <label>
            <input type="checkbox" name="Repairs" value="Fuser Roller Replacement / Fuser Rebuild" id="Repairs_3">
            Fuser Roller Replacement / Fuser Rebuild</label>
          <br>
          <label>
            <input type="checkbox" name="Repairs" value="Paper Tray Roller Replacement" id="Repairs_4">
            Paper Tray Roller Replacement</label>
          <br>
          <label>
            <input type="checkbox" name="Repairs" value="Board Replacement" id="Repairs_5">
            Board Replacement</label>
          <br>
          <label>
            <input type="checkbox" name="Repairs" value="Firmware Updates" id="Repairs_6">
            Firmware Updates</label>
          <br>
          <label>
            <input type="checkbox" name="Repairs" value="Networking Troubleshooting" id="Repairs_7">
            Networking Troubleshooting</label>
          <br>
          <label>
            <input type="checkbox" name="Repairs" value="Scanning Troubleshooting" id="Repairs_8">
            Scanning Troubleshooting</label>
          <br>
          <label>
            <input type="checkbox" name="Repairs" value="Fax Troubleshooting" id="Repairs_9">
            Fax Troubleshooting</label>
          <br>
        </p>
    </div>
    <br />
    <div class="verdana2bold">5) What is the order of the "Copy Process"? Select the proper order from the answers below?</div>
    <br />
    <div class="formRow2">
      <div>
        <label>
          <input id="question5" name="question5" value="1"  type="radio"<?php if($q5 == 1){print " checked=\"checked\"";} ?>>
          Image Exposure, Charging, Developing, Fixing, Transfer, Drum Cleaning</label>
      </div>
      <div>
        <label>
          <input id="question5" name="question5" value="2"  type="radio"<?php if($q5 == 2){print " checked=\"checked\"";} ?>>
          Charging, Image Exposure, Developing, Transfer, Fixing, Drum Cleaning</label>
      </div>
      <div>
        <label>
          <input id="question5" name="question5" value="3"  type="radio"<?php if($q5 == 3){print " checked=\"checked\"";} ?>>
          Charging, Developing, Image Exposure, Fixing, Transfer, Drum Cleaning</label>
      </div>
      <div>
        <label>
          <input id="question5" name="question5" value="4"  type="radio"<?php if($q5 == 4){print " checked=\"checked\"";} ?>>
          Charging, Developing, Image Exposure, Transfer, Drum Cleaning, Fixing</label>
      </div>
    </div>
    <br />
    <div class="verdana2bold">6) What is the main difference between a printer and Digital Copier?</div>
    <br />
    <div class="formRow2">
      <div>
        <label>
          <input id="question6" name="question6" value="1"  type="radio"<?php if($q6 == 1){print " checked=\"checked\"";} ?>>
          Copiers are all multifunction devices and printers may not be</label>
      </div>
      <div>
        <label>
          <input id="question6" name="question6" value="2"  type="radio"<?php if($q6 == 2){print " checked=\"checked\"";} ?>>
          Copiers use more paper</label>
      </div>
      <div>
        <label>
          <input id="question6" name="question6" value="3"  type="radio"<?php if($q6 == 3){print " checked=\"checked\"";} ?>>
          There is no real difference</label>
      </div>
    </div>
    <br />
    <div class="verdana2bold">7) What would you check if a Digital Copier is making lines on copies when using the document feeder (handler) on a digital copier, but not on the prints from the machine?</div>
    <br />
    <div class="formRow2">
      <div>
        <label>
          <input id="question7" name="question7" value="1"  type="radio"<?php if($q7 == 1){print " checked=\"checked\"";} ?>>
          Scan Glass</label>
      </div>
      <div>
        <label>
          <input id="question7" name="question7" value="2"  type="radio"<?php if($q7 == 2){print " checked=\"checked\"";} ?>>
          Mirrors</label>
      </div>
      <div>
        <label>
          <input id="question7" name="question7" value="3"  type="radio"<?php if($q7 == 3){print " checked=\"checked\"";} ?>>
          Drum</label>
      </div>
      <div>
        <label>
          <input id="question7" name="question7" value="4"  type="radio"<?php if($q7 == 4){print " checked=\"checked\"";} ?>>
          Fuser</label>
      </div>
      <div>
        <label>
          <input id="question7" name="question7" value="5"  type="radio"<?php if($q7 == 5){print " checked=\"checked\"";} ?>>
          A and C</label>
      </div>
    </div>
    <br />
    <div class="verdana2bold">8) How would you determine what is causing a black line without taking the drum unit or the fusing unit out of the machine?</div>
    <br />
    <div class="formRow2">
      <div>
        <label>
          <input id="question8" name="question8" value="1"  type="radio"<?php if($q8 == 1){print " checked=\"checked\"";} ?>>
          Run a test copy, review the line on the paper, and install a new fusing unit</label>
      </div>
      <div>
        <label>
          <input id="question8" name="question8" value="2"  type="radio"<?php if($q8 == 2){print " checked=\"checked\"";} ?>>
          Run a test copy without the toner cartridge installed</label>
      </div>
      <div>
        <label>
          <input id="question8" name="question8" value="3"  type="radio"<?php if($q8 == 3){print " checked=\"checked\"";} ?>>
          Run a test copy, stop the copy as it is passing the drum unit, and visually inspect drum & fusing roller</label>
      </div>
      <div>
        <label>
          <input id="question8" name="question8" value="4"  type="radio"<?php if($q8 == 4){print " checked=\"checked\"";} ?>>
          Run a test copy and ask the End User for help</label>
      </div>
     </div>
    <br />
    <div class="verdana2bold">9) What would you clean if a Digital Copier was making light copies when placed on the original scan glass but not when printing?</div>
    <br />
    <div class="formRow2">
      <div>
        <label>
          <input id="question9" name="question9" value="1"  type="radio"<?php if($q9 == 1){print " checked=\"checked\"";} ?>>
          A.	Feed Rollers</label>
      </div>
      <div>
        <label>
          <input id="question9" name="question9" value="2"  type="radio"<?php if($q9 == 2){print " checked=\"checked\"";} ?>>
          Drum</label>
      </div>
      <div>
        <label>
          <input id="question9" name="question9" value="3"  type="radio"<?php if($q9 == 3){print " checked=\"checked\"";} ?>>
          Orignal Glass</label>
      </div>
      <div>
        <label>
          <input id="question9" name="question9" value="4"  type="radio"<?php if($q9 == 4){print " checked=\"checked\"";} ?>>
          Mirrors</label>
      </div>
      <div>
        <label>
          <input id="question9" name="question9" value="5"  type="radio"<?php if($q9 == 5){print " checked=\"checked\"";} ?>>
          C and D</label>
      </div>
    </div>
    <br />
    <div class="verdana2bold">10) What would you clean if a Digital Copier was making light copies when printing?</div>
    <br />
    <div class="formRow2">
      <div>
        <label>
          <input id="question10" name="question10" value="1"  type="radio"<?php if($q10 == 1){print " checked=\"checked\"";} ?>>
          Drum</label>
      </div>
      <div>
        <label>
          <input id="question10" name="question10" value="2"  type="radio"<?php if($q10 == 2){print " checked=\"checked\"";} ?>>
          Feed Rollers</label>
      </div>
      <div>
        <label>
          <input id="question10" name="question10" value="3"  type="radio"<?php if($q10 == 3){print " checked=\"checked\"";} ?>>
          Original Glass</label>
      </div>
      <div>
        <label>
          <input id="question10" name="question10" value="4"  type="radio"<?php if($q10 == 4){print " checked=\"checked\"";} ?>>
          Transfer / Separation Corona Assembly</label>
      </div>
      <div>
        <label>
          <input id="question10" name="question10" value="5"  type="radio"<?php if($q10 == 5){print " checked=\"checked\"";} ?>>
          Cleaning Blade</label>
      </div>
      <div>
        <label>
          <input id="question10" name="question10" value="6"  type="radio"<?php if($q10 == 6){print " checked=\"checked\"";} ?>>
          Developer</label>
      </div>
      <div>
        <label>
          <input id="question10" name="question10" value="7"  type="radio"<?php if($q10 == 7){print " checked=\"checked\"";} ?>>
          Main Charge Corona Assembly</label>
      </div>
      <div>
        <label>
          <input id="question10" name="question10" value="8"  type="radio"<?php if($q10 == 8){print " checked=\"checked\"";} ?>>
          D and G</label>
      </div>
      <div>
        <label>
          <input id="question10" name="question10" value="9"  type="radio"<?php if($q10 == 9){print " checked=\"checked\"";} ?>>
          All of the above</label>
      </div>
    </div>
  </div>
     <div class="verdana2bold">11) The Digital Copier is jamming out of the third tray, but not out of tray one or tray two.  What would you check to resolve the issue?</div>
    <br />
    <div class="formRow1">
      <div>
        <label>
          <input id="question11" name="question11" value="1"  type="radio"<?php if($q11== 1){print " checked=\"checked\"";} ?>>
          A.	Review tray 1 and tray 2 to see why they are performing properly</label>
      </div>
      <div>
        <label>
          <input id="question11" name="question11" value="2"  type="radio"<?php if($q11== 2){print " checked=\"checked\"";} ?>>
          B.	Inspect the pick-off fingers in the fusing unit</label>
      </div>
      <div>
        <label>
          <input id="question11" name="question11" value="3"  type="radio"<?php if($q11== 3){print " checked=\"checked\"";} ?>>
          C.	Jig the door, and visually inspect paper exiting tray three</label>
      </div>
      <div>
        <label>
          <input id="question11" name="question11" value="4"  type="radio"<?php if($q11== 4){print " checked=\"checked\"";} ?>>
          D.	Visually inspect paper is loaded properly in tray 3</label>
      </div>
      <div>
        <label>
          <input id="question11" name="question11" value="5"  type="radio"<?php if($q11== 5){print " checked=\"checked\"";} ?>>
          E.	C and D</label>
      </div>
     <div>
        <label>
          <input id="question11" name="question11" value="6"  type="radio"<?php if($q11== 6){print " checked=\"checked\"";} ?>>
          All of the Above</label>
      </div>
    </div>
    <br />
    <div class="verdana2bold">12) The Fax on a Digital Copier is not working.  What would you check to resolve the issue?</div>
    <br />
    <div class="formRow2">
      <div>
        <label>
          <input id="question12" name="question12" value="1"  type="radio"<?php if($q12== 1){print " checked=\"checked\"";} ?>>
          Check to see if the line has a dial tone</label>
      </div>
      <div>
        <label>
          <input id="question12" name="question12" value="2"  type="radio"<?php if($q12== 2){print " checked=\"checked\"";} ?>>
          Send a test fax to another working fax device</label>
      </div>
      <div>
        <label>
          <input id="question12" name="question12" value="3"  type="radio"<?php if($q12== 3){print " checked=\"checked\"";} ?>>
          Call the fax number to verify it is a valid line</label>
      </div>
      <div>
        <label>
          <input id="question12" name="question12" value="4"  type="radio"<?php if($q12== 4){print " checked=\"checked\"";} ?>>
          Check to see if the line is plugged into the correct port</label>
      </div>
      <div>
        <label>
          <input id="question12" name="question12" value="5"  type="radio"<?php if($q12== 5){print " checked=\"checked\"";} ?>>
          Use an analog telephone to dial out on the line</label>
      </div>
     <div>
        <label>
          <input id="question12" name="question12" value="6"  type="radio"<?php if($q12== 6){print " checked=\"checked\"";} ?>>
          All of the above</label>
    </div>
    </div>
    <br />
    <div class="verdana2bold">13) If the document feeder (handler) is jamming, what is one of the first things you would do upon arriving onsite with the troubled Digital Copier?</div>
    <br />
    <div class="formRow2">
      <div>
        <label>
          <input id="question13" name="question13" value="1"  type="radio"<?php if($q13== 1){print " checked=\"checked\"";} ?>>
          Check the transport belt</label>
      </div>
      <div>
        <label>
          <input id="question13" name="question13" value="2"  type="radio"<?php if($q13== 2){print " checked=\"checked\"";} ?>>
          Clean tray 1 feed rollers</label>
      </div>
      <div>
        <label>
          <input id="question13" name="question13" value="3"  type="radio"<?php if($q13== 3){print " checked=\"checked\"";} ?>>
          Run a test page through the document feeder</label>
      </div>
      <div>
        <label>
          <input id="question13" name="question13" value="4"  type="radio"<?php if($q13== 4){print " checked=\"checked\"";} ?>>
          Disconnect the sorter</label>
      </div>
    </div>
    <br />
   </div>
  </div>
  <div class="formBoxFooter">
    <div>
      <input id="doSubmit" name="doSubmit" value="Submit Test" type="submit" />
    </div>
  </div>
</form>
</div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<?php

} else {
	
	$q1 =  $_POST['question1'];
	$q2 =  $_POST['question2'];
	$q3 =  $_POST['question3'];
	$q4 =  $_POST['question4'];
	$q5 =  $_POST['question5'];
	$q6 =  $_POST['question6'];
	$q7 =  $_POST['question7'];
	$q8 =  $_POST['question8'];
	$q9 =  $_POST['question9'];
	$q10 =  $_POST['question10'];

	$q11 =  $_POST['question11'];
	$q12 =  $_POST['question12'];
	$q13 =  $_POST['question13'];
	$q14 =  $_POST['question14'];
	$q15 =  $_POST['question15'];
	$q16 =  $_POST['question16'];
	$q17 =  $_POST['question17'];
	$q18 =  $_POST['question18'];
	$q19 =  $_POST['question19'];
	$q20 =  $_POST['question20'];

	$q21 =  $_POST['question21'];
	$q22 =  $_POST['question22'];
	$q23 =  $_POST['question23'];
	$q24 =  $_POST['question24'];
	$q25 =  $_POST['question25'];
	$q26 =  $_POST['question26'];
	$q27 =  $_POST['question27'];
	$q28 =  $_POST['question28'];
	$q29 =  $_POST['question29'];
	$q30 =  $_POST['question30'];

	$q31 =  $_POST['question31'];
	$q32 =  $_POST['question32'];
	$q33 =  $_POST['question33'];
	$q34 =  $_POST['question34'];
	$q35 =  $_POST['question35'];
	$q36 =  $_POST['question36'];
	$q37 =  $_POST['question37'];
	$q38 =  $_POST['question38'];
	$q39 =  $_POST['question39'];
	$q40 =  $_POST['question40'];

	$q41 =  $_POST['question41'];
	$q42 =  $_POST['question42'];
	$q43 =  $_POST['question43'];
	$q44 =  $_POST['question44'];
	$q45 =  $_POST['question45'];
	$q46 =  $_POST['question46'];
	$q47 =  $_POST['question47'];
	$q48 =  $_POST['question48'];
	$q49 =  $_POST['question49'];
	$q50 =  $_POST['question50'];
	

// echo "Question 1: " . $q1."<br>";
// echo "Question 2: " . $q2."<br>";
// echo "Question 3: " . $q3."<br>";
// echo "Question 4: " . $q4."<br>";
// echo "Question 5: " . $q5."<br>";
// echo "Question 6: " . $q6."<br>";
// echo "Question 7: " . $q7."<br>";
// echo "Question 8: " . $q8."<br>";
// echo "Question 9: " . $q9."<br>";
// echo "Question 10: " . $q10."<br>";

	$a1 = 1;
	$a2 = 3;
	$a3 = 2;
	$a4 = 4;
	$a5 = 5;
	$a6 = 2;
	$a7 = 5;
	$a8 = 2;
	$a9 = 3;
	$a10 = 1;
	
	$a11 = 3;
	$a12 = 3;
	$a13 = 5;
	$a14 = 4;
	$a15 = 1;
	$a16 = 3;
	$a17 = 2;
	$a18 = 5;
	$a19 = 2;
	$a20 = 4;
	
	$a21 = 3;
	$a22 = 4;
	$a23 = 2;
	$a24 = 3;
	$a25 = 4;
	$a26 = 5;
	$a27 = 4;
	$a28 = 3;
	$a29 = 5;
	$a30 = 4;
	
	$a31 = 4;
	$a32 = 4;
	$a33 = 4;
	$a34 = 3;
	$a35 = 4;
	$a36 = 4;
	$a37 = 1;
	$a38 = 5;
	$a39 = 3;
	$a40 = 4;
	
	$a41 = 3;
	$a42 = 5;
	$a43 = 3;
	$a44 = 4;
	$a45 = 4;
	$a46 = 3;
	$a47 = 5;
	$a48 = 5;
	$a49 = 1;
	$a50 = 2;


$points = 0;
$section1 = 0;
$section2 = 0;
$section3 = 0;
$section4 = 0;
$section5 = 0;

$points = ($q1 == $a1) ? ++$points : $points;
//echo "<p>Points: " . $points . "</p>";
$points = ($q2 == $a2) ? ++$points : $points;
//echo "<p>Points: " . $points . "</p>";
$points = ($q3 == $a3) ? ++$points : $points;
//echo "<p>Points: " . $points . "</p>";
$points = ($q4 == $a4) ? ++$points : $points;
//echo "<p>Points: " . $points . "</p>";
$points = ($q5 == $a5) ? ++$points : $points;
//echo "<p>Points: " . $points . "</p>";
$points = ($q6 == $a6) ? ++$points : $points;
//echo "<p>Points: " . $points . "</p>";
$points = ($q7 == $a7) ? ++$points : $points;
//echo "<p>Points: " . $points . "</p>";
$points = ($q8 == $a8) ? ++$points : $points;
//echo "<p>Q8: " . $q8 . " A8: " . $a8 . "</p>";
//echo "<p>Points: " . $points . "</p>";
$points = ($q9 == $a9) ? ++$points : $points;
//echo "<p>Points: " . $points . "</p>";
$points = ($q10 == $a10) ? ++$points : $points;
//echo "<p>Points: " . $points . "</p>";
$section1 = $points;
//echo "<p>Section Points: " . $section1 . "</p>";

$points = ($q11 == $a11) ? ++$points : $points;
$points = ($q12 == $a12) ? ++$points : $points;
$points = ($q13 == $a13) ? ++$points : $points;
$points = ($q14 == $a14) ? ++$points : $points;
$points = ($q15 == $a15) ? ++$points : $points;
$points = ($q16 == $a16) ? ++$points : $points;
$points = ($q17 == $a17) ? ++$points : $points;
$points = ($q18 == $a18) ? ++$points : $points;
$points = ($q19 == $a19) ? ++$points : $points;
$points = ($q20 == $a20) ? ++$points : $points;
$section2 = $points - $section1;

$points = ($q21 == $a21) ? ++$points : $points;
$points = ($q22 == $a22) ? ++$points : $points;
$points = ($q23 == $a23) ? ++$points : $points;
$points = ($q24 == $a24) ? ++$points : $points;
$points = ($q25 == $a25) ? ++$points : $points;
$points = ($q26 == $a26) ? ++$points : $points;
$points = ($q27 == $a27) ? ++$points : $points;
$points = ($q28 == $a28) ? ++$points : $points;
$points = ($q29 == $a29) ? ++$points : $points;
$points = ($q30 == $a30) ? ++$points : $points;
$section3 = $points - $section1 - $section2;

$points = ($q31 == $a31) ? ++$points : $points;
$points = ($q32 == $a32) ? ++$points : $points;
$points = ($q33 == $a33) ? ++$points : $points;
$points = ($q34 == $a34) ? ++$points : $points;
$points = ($q35 == $a35) ? ++$points : $points;
$points = ($q36 == $a36) ? ++$points : $points;
$points = ($q37 == $a37) ? ++$points : $points;
$points = ($q38 == $a38) ? ++$points : $points;
$points = ($q39 == $a39) ? ++$points : $points;
$points = ($q40 == $a40) ? ++$points : $points;
$section4 = $points - $section1 - $section2 - $section3;

$points = ($q41 == $a41) ? ++$points : $points;
$points = ($q42 == $a42) ? ++$points : $points;
$points = ($q43 == $a43) ? ++$points : $points;
$points = ($q44 == $a44) ? ++$points : $points;
$points = ($q45 == $a45) ? ++$points : $points;
$points = ($q46 == $a46) ? ++$points : $points;
$points = ($q47 == $a47) ? ++$points : $points;
$points = ($q48 == $a48) ? ++$points : $points;
$points = ($q49 == $a49) ? ++$points : $points;
$points = ($q50 == $a50) ? ++$points : $points;
$section5 = $points - $section1 - $section2 - $section3 - $section4;

$score = round(($points / 50) *100);

$TechID = $_SESSION['TechID'];
$DateTime = date("Y-m-d H:i:s");

$testID = mysqlFetchAssoc(mysqlQuery("SELECT id FROM ServRight_ElectroMechTest WHERE tech_id = '".$TechID."'"));
if ($testID) {
	$testID = $testID[0];
	$emTestID = $testID[id];

	mysqlQuery("UPDATE ServRight_ElectroMechTest SET a1 = '$q1', a2 = '$q2', a3 = '$q3', a4 = '$q4', a5 = '$q5', a6 = '$q6', a7 = '$q7', a8 = '$q8', a9 = '$q9', a10 = '$q10',
			   a11 = '$q11', a12 = '$q12', a13 = '$q13', a14 = '$q14', a15 = '$q15', a16 = '$q16', a17 = '$q17', a18 = '$q18', a19 = '$q19', a20 = '$q20',
				a21 = '$q21', a22 = '$q22', a23 = '$q23', a24 = '$q24', a25 = '$q25', a26 = '$q26', a27 = '$q27', a28 = '$q28', a29 = '$q29', a30 = '$q30',
				a31 = '$q31', a32 = '$q32', a33 = '$q33', a34 = '$q34', a35 = '$q35', a36 = '$q36', a37 = '$q37', a38 = '$q38', a39 = '$q39',a40 = '$q40',
				a41 = '$q41', a42 = '$q42', a43 = '$q43', a44 = '$q44', a45 = '$q45', a46 = '$q46', a47 = '$q47', a48 = '$q48', a49 = '$q49', a50 = '$q50', score = $score, Section1Score = $section1, Section2Score = $section2, Section3Score = $section3, Section4Score = $section4, Section5Score = $section5 WHERE id = " . $emTestID);
			   
} else {
	$DateTime = date("Y-m-d H:i:s");
	mysqlQuery("INSERT INTO ServRight_ElectroMechTest (tech_id, date_taken, score, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20,
												   a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47, a48, a49, a50, Section1Score, Section2Score, Section3Score, Section4Score, Section5Score)
		   VALUES ('$TechID', '$DateTime', '$score', '$q1', '$q2', '$q3', '$q4', '$q5', '$q6', '$q7', '$q8', '$q9', '$q10', '$q11', '$q12', '$q13', '$q14', '$q15', '$q16', '$q17', '$q18', '$q19', '$q20'
				   , '$q21', '$q22', '$q23', '$q24', '$q25', '$q26', '$q27', '$q28', '$q29', '$q30', '$q31', '$q32', '$q33', '$q34', '$q35', '$q36', '$q37', '$q38', '$q39', '$q40', '$q41', '$q42', '$q43', '$q44', '$q45', '$q46', '$q47', '$q48', '$q49', '$q50', '$section1', '$section2', '$section3', '$section4', '$section5')");
}
		


echo "<div class=\"formBox\">";

if ($score >= 80) {
	mysqlQuery("UPDATE TechBankInfo SET ServRight_ElectroMech_Cert = '1' WHERE TechID = '$TechID'");
	Core_Database_MysqlSync::sync(Core_Database::TABLE_TECH_BANK_INFO, "TechID", $TechID);
	echo "<div class=\"verdana2bold\" style=\"color:green\">Contratulations, you passed!</div><div class=\"formRow1\"></div><br />";
	echo "<div class=\"verdana2bold\">SCORE: $score %</div>";
} else {
	echo "<div class=\"verdana2bold\" style=\"color:#F00\">Sorry, you do not have a passing score</div><br />";
	echo "<div class=\"formRow2\">You will need to re-take the test. You will not be activated for the ServRight Electro Mechanical program until you have scored 100%.</div>";
	echo "<div class=\"formRow2\">Please Click <a href=\"electroMechTest.php\">here</a> to re-take the test.</div><br />";
	echo "<div class=\"verdana2bold\">SCORE: $score %</div>";
	echo "</div></div></div></div>";
}
echo "</div></div></div></div>";


//	echo "<br>Total Points: " . $points . "<br>";
//	echo "Score: " . $score . "%<br>";
} ?>
<p>
  <?php require ("../footer.php"); ?>
</p>
