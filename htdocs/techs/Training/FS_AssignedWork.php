<?php $page = "techs"; ?>
<?php $option = "training"; ?>
<?php $selected = "fs" ?>
<?php $display = "fs" ?>
<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>

<!-- Add Content Here -->

<br /><br />

<div align="center">

<TABLE cellSpacing=0 cellPadding=0 width=700 border=0 style="text-align:left">
<TBODY>
<TR>
<TD colSpan=2>
<BR><FONT face=Arial size=4><B>ASSIGNED WORK</B></FONT> 
<BR>
<FONT face=Arial size=3>How to view/search for work orders assigned to you</FONT> 
<P>
<FONT face=Arial size=2>
<B>Note:</B> FLS is a major client of Field Solutions and handles things a bit differently and separately from all other clients and the main site. Therefore, please pay attention to the differences in the work order instructions for "FLS" versus "FIELDSOLUTIONS" (main site) work order instructions. 
<P>
<HR>
<P>

<B>How to search/view FLS work orders assigned to you </B>
<P>
Once logged into your Field Solutions account, click on the "Work Orders" link in the blue menu bar at the top. When first arriving in the work order section, note the four links showing at the top just under the blue menu bar - "ASSIGNED WORK", "AVAILABLE WORK", "MY APPLIED WOS" and "FLSWOS". The work order section defaults to show main (Non FLS) work orders, meaning it will show you all work orders under all clients but FLS upon first coming into the work order section. To view/search for FLS work orders assigned to you, click on the "FLSWOS" link. Note: You can toggle back/forth between main work orders (all clients but FLS) and FSL work orders.   
<P>
After clicking on "FLSWOS", click on the "ASSIGNED WORK" link where you can search all FLS work orders that have been assigned to you or by week/month via the drop down menu (top, left). You can also search for work orders within a specific date range directly below the dropdown menu.  
<P>
<B>How to search/view main Field Solutions (non-FLS) work orders assigned to you </B>
<P>
Once logged into your Field Solutions account, click on the "Work Orders" link in the blue menu bar at the top. When first arriving in the work order section, note the four links showing at the top just under the blue menu bar - "ASSIGNED WORK", "AVAILABLE WORK", "MY APPLIED WOS" and "FLSWOS". The work order section defaults to show main (non FLS) work orders. Meaning, you search for non-FLS work orders assigned to you upon first entering the work order section.    
<P>
Next, click the "ASSIGNED WORK" link where you can search all work orders that have been assigned to you or by week/month via the drop down menu (top, left). You can also search for work orders by mile radius and/or a specific date range directly below the dropdown menu.  
<P>
<B>Special note regarding work orders assigned to you</B>
<P>

If for some reason a work order assigned to you is canceled ("kicked back") by the client, it will be removed from your "ASSIGNED WORK" section.   
<P>
<HR>
<P>
<B>What the following tabs are for in each work order section - 
<BR>
"Assigned" - "Work Done" - " Approved" -  "In Accounting" - "Paid" - "Incomplete" - "All" </B>
<P>
These tabs in each work order section are for work order processing. You should keep track of each step to make sure that you are on target to get paid for work performed. 
<P>
A brief explanation of each tab - 
<P>
<B>Assigned</B> - A listing of all your work orders officially assigned to you by clients/staff.
<P>

<B>Work Done</B> - A listing of all work orders covering work you have performed that the client has entered into the system, which needs to be completed by you online in order to be approved and paid.  
<P>
<B>Approved</B> - A listing of all your work orders that have been approved by the client(s). Note: Clients approve work orders, not Field Solutions. You cannot be paid for work orders until they have been approved by the client.
<P>
<B>In Accounting</B> - A listing of all your work orders that have been approved by the client(s) and are in the queue to be paid by Field Solutions. 
<P>
<B>Paid</B> - A listing of all your work orders that have been paid to include the date a check was cut and mailed or the direct deposit process began.   
<P>
<B>Incomplete</B> - A listing of all/any of your work orders that you have not (properly) completed to include updating your work order(s) online per instructions.

<P>
<B>All</B> - A listing of all your work orders

<P class="nextButton" align="center"><a href="FS_AvailableWork.php">Previous</a>
&nbsp; | &nbsp;<a href="FS_AppliedWork.php">Next</a>
</TD></TR></TBODY></TABLE>









</div>

<!--- End Content --->
<?php require ("../../footer.php"); ?>
		
