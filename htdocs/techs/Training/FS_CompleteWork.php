<?php $page = "techs"; ?>
<?php $option = "training"; ?>
<?php $selected = "fs" ?>
<?php $display = "fs" ?>
<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>

<!-- Add Content Here -->

<br /><br />

<div align="center">

<TABLE cellSpacing=0 cellPadding=0 width=700 border=0 style="text-align:left">
<TBODY>
<TR>
<TD colSpan=2>
<BR><FONT face=Arial size=4><B>How to complete your work orders online </B></FONT> 
<BR>
<FONT face=Arial size=3>The work order completion, approval, payment  process</FONT> 
<P>
<FONT face=Arial size=2>
<B>Note:</B> FLS is a major client of Field Solutions and handles things a bit differently and separately from all other clients and the main site. Therefore, please pay attention to the differences in the work order instructions for "FLS" versus "FIELDSOLUTIONS" (main site) work order instructions. 
<P>
<HR>
<P>

In addition to following client protocol for closing out your jobs on-site after you complete them, techs must also complete their work orders online per instructions in order to have them approved by the client and paid by Field Solutions.  
<P>
Completing a work order from start to finish (The "In a nutshell" version):
<P>
1) The tech performs the work and closes the work order (job/install) on-site per client instructions
<BR>
2) The tech closes out their work order online per instructions
<BR>
3) The client approves the tech's work orders online
<BR>
4) Field Solutions processes payment on the tech's work order
<BR>
5) The tech is sent a check or has their payment deposited directly into their bank account
<P>
<B>How to complete FLS work orders online </B> (Step-by-step instructions)

<P>
Note: The link below is associated with the FLS program/client and how you must complete your work orders online in order to get them approved by the FLS client and paid by Field Solutions. If you have not yet been activated for the FLS program, disregard this section. 
<P>
If you are an active tech with FLS and looking for instructions on how to properly  complete your FLS work orders online <A href="http://flsupport.com/20.html"target=_blank"><FONT face=Arial color=#0062c4 size=2>click here</FONT></A> for detailed instructions.
<P>
<B>How to complete main Field Solutions (non-FLS) work orders online</B> (Step-by-step instructions)
<P>
1) Once logged into your Field Solutions account, click on the "Work Orders" link in the blue menu bar at the top. By default you will enter the "ASSIGNED WORK" section after clicking on the "Work Orders" link, which is where you need to be to complete your work orders online in order to get paid. 
<P>
<B>Note:</B> The work order section defaults to show main Field Solutions (non-FLS) work orders, meaning it will show you all work orders under all clients but FLS upon first coming into the work order section. To complete FLS work orders, you will need to click on the "FLSWOS" link. You are able to toggle back and forth between main work orders (all clients but FLS) and FSL work orders.   

<P>
2) Next, click on the "Work Done" tab below the main search. This will bring up all work orders covering work you have performed that the client has entered into the system. 
<P>
3) Next, you need to click on the work order number (link) to open it and complete per instructions below.
<P>
A) Mark the work order complete by checking the tech complete box.
<P>
B) Enter your time in (time arrived on-site) and time out (time you left the site).
<P>
C) Enter comments. 
<P>
D) If the client is looking for deliverables (documentation), use one or more of the "Tech File 1, 2, 3" fields to upload those documents. If/when you have more than three files to attached, zip one or more files together.
<P>
E) Be sure to click the submit or update button to save.
<P>
Once you have updated your work orders online as instructed above, the client will be alerted so they can approve it to be paid by Field Solutions.
<P class="nextButton" align="center"><a href="FS_AppliedWork.php">Previous</a>
&nbsp; | &nbsp;<a href="FS_PaymentPolicy.php">Next</a>
</TD></TR></TBODY></TABLE>













</div>

<!--- End Content --->
<?php require ("../../footer.php"); ?>
		
