<?php
	ini_set("display_errors", 1); 
?>

<?php $page = 'techs'; ?>
<?php $option = 'home'; ?>
<?php 
	require_once ("../../../headerStartSession.php");

	$loggedIn   = ( !empty($_SESSION['loggedIn']) ) ? $_SESSION['loggedIn'] : 'no';

    if($loggedIn !== 'yes'){
        header( 'Location: http'.($_SERVER['HTTPS']=='on' ? 's':'').'://'.$_SERVER['HTTP_HOST'].'/techs/logIn.php?flsupport=true' ); // Redirect to logIn.php
   		exit;
    }
?>
<?php //require_once("../../../clients/function.debug.php"); __debug(true); ?>

<?php  require ("../../../header.php"); ?>
<?php  require ("../../../navBar.php");  ?>
<?php  require ("../../../library/mySQL.php"); ?>
<script type="text/javascript">

$(document).ready(function(){
	$("body").css("width", "850px").css("min-width", "850px");
	
});

</script>
<style type="text/css">
#mainContainer{
	min-width: 800px;
	width: 850px;
}
</style>

<style type="text/css">
	#emailBlast {
		text-align: center;
	}
	.verdana2 {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		clear: both;
	}

	.verdana2bold {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		font-weight: bold;
		clear: both;
	}

	.fieldGroup {
		padding-bottom: 5px;
		border-bottom: 2px solid #2A497D;
	}

	.formBox, .formBoxFooter {
		margin: 15px auto 0px auto;
		padding: 15px;
		width: 920px;
		color: #2A497D;
		font-size: 12px;
		border: 2px solid #2A497D;
	}

	.formBox input {
		font-size: 12px;
		height: 14px;
	}

	.formBoxFooter {
		background-color: #BACBDF;
		text-align: center;
		margin: 0px auto 10px auto;
		padding: 5px 15px;
		border-top: none;
	}
	.formRow3, .formRow4, .formRow5, .formRow6 {
		width: 100%;
		clear: both;
	}
	.formRow3 div {
		width: 33%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow4 div {
		width: 25%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow5 div {
		width: 20%;
		float: left;
		margin-bottom: 5px;
	}

	.formRow6 div {
		width: 16%;
		float: left;
		margin-bottom: 5px;
	}

	.resultsTable {
		width: 80%;
		margin: 10px auto 0px auto;
	}

	.resultsTable thead {
		text-align: center;
		color: #2A497D;
		border: 1px solid #2A497D;
		background-color: #A0A8AA;
		cursor: default;
	}

	.sortAble, #nextPageBtn, #prevPageBtn {
		cursor: pointer;
	}

	.sortAbleSelected {
		cursor: pointer;
		background-color: #FF9900;
		color: #FFFFFF;
		font-weight: bold;
	}

	.resultsTable tfoot {
		text-align: center;
		color: #000000;
		border: 1px solid #2A497D;
		background-color: #A0A8AA;
	}

	.resultsTable td {
		padding: 3px 5px;
	}

	.resultsTable .evenRow {
		background-color: #E9EEF8;
	}

	.resultsTable .oddRow {
		background-color: #FBFCFD;
	}

	.resultsTable .preferred {
		background-color: #FFFF8C;
	}
    .resultsTable .techGrey {
		background-color: #DEDEDE;
	}
    .resultsTable .techYellow {
		background-color: #FFFF8C;
	}
    .resultsTable .techRed {
		background-color: #FFBBBB;
	}
    .resultsTable .techGreen {
		background-color: #89FF89;
	}

	.searchControls {
		margin-top: 10px;
		text-align: center;
	}

	.noResultsDiv {
		margin-top: 10px;
		text-align: center;
	}
	
	label input {margin-right:10px;}
	.link_button { width: 140px; border:none; background: url(/widgets/images/button_middle.png) left top no-repeat; font: normal 10px/10px Verdana, Arial, Helvetica, sans-serif; padding: 4px 0 8px; color: #385c7e; cursor: pointer; }
</style>

<?php

$TechID = $_SESSION['TechID'];

$q1 =  $_POST['question1'];
$q2 =  $_POST['question2'];
$q3 =  $_POST['question3'];
$q4 =  $_POST['question4'];
$q5 =  $_POST['question5'];
$q6 =  $_POST['question6'];
$q7 =  $_POST['question7'];
$q8 =  $_POST['question8'];
$q9 =  $_POST['question9'];
$q10 =  $_POST['question10'];

$q11 =  $_POST['question11'];
$q12 =  $_POST['question12'];
$q13 =  $_POST['question13'];
$q14 =  $_POST['question14'];
$q15 =  $_POST['question15'];
$q16 =  $_POST['question16'];
$q17 =  $_POST['question17'];
$q18 =  $_POST['question18'];
$q19 =  $_POST['question19'];
$q20 =  $_POST['question20'];

$q21 =  $_POST['question21'];
$q22 =  $_POST['question22'];
$q23 =  $_POST['question23'];
$q24 =  $_POST['question24'];
$q25 =  $_POST['question25'];
$q26 =  $_POST['question26'];
$q27 =  $_POST['question27'];
$q28 =  $_POST['question28'];
$q29 =  $_POST['question29'];
$q30 =  $_POST['question30'];

$q31 =  $_POST['question31'];
$q32 =  $_POST['question32'];
$q33 =  $_POST['question33'];
$q34 =  $_POST['question34'];
$q35 =  $_POST['question35'];
$q36 =  $_POST['question36'];
$q37 =  $_POST['question37'];
$q38 =  $_POST['question38'];
$q39 =  $_POST['question39'];
$q40 =  $_POST['question40'];

$q41 =  $_POST['question41'];
$q42 =  $_POST['question42'];
$q43 =  $_POST['question43'];
$q44 =  $_POST['question44'];
$q45 =  $_POST['question45'];
$q46 =  $_POST['question46'];
$q47 =  $_POST['question47'];
$q48 =  $_POST['question48'];
$q49 =  $_POST['question49'];
$q50 =  $_POST['question50'];
	


if (!isset($_POST["doSubmit"])) {

	if (isset($_POST["doSave"])) {
		$testID = mysqlFetchAssoc(mysqlQuery("SELECT id FROM ServRight_ElectroMechTest WHERE tech_id = '".$TechID."'"));
		if ($testID) {
			$testID = $testID[0];
			$emTestID = $testID[id];
	
			mysqlQuery("UPDATE ServRight_ElectroMechTest SET a1 = '$q1', a2 = '$q2', a3 = '$q3', a4 = '$q4', a5 = '$q5', a6 = '$q6', a7 = '$q7', a8 = '$q8', a9 = '$q9', a10 = '$q10',
					   a11 = '$q11', a12 = '$q12', a13 = '$q13', a14 = '$q14', a15 = '$q15', a16 = '$q16', a17 = '$q17', a18 = '$q18', a19 = '$q19', a20 = '$q20',
						a21 = '$q21', a22 = '$q22', a23 = '$q23', a24 = '$q24', a25 = '$q25', a26 = '$q26', a27 = '$q27', a28 = '$q28', a29 = '$q29', a30 = '$q30',
						a31 = '$q31', a32 = '$q32', a33 = '$q33', a34 = '$q34', a35 = '$q35', a36 = '$q36', a37 = '$q37', a38 = '$q38', a39 = '$q39',a40 = '$q40',
						a41 = '$q41', a42 = '$q42', a43 = '$q43', a44 = '$q44', a45 = '$q45', a46 = '$q46', a47 = '$q47', a48 = '$q48', a49 = '$q49', a50 = '$q50' WHERE id = " . $emTestID);
					   
		} else {
			$DateTime = date("Y-m-d H:i:s");
			mysqlQuery("INSERT INTO ServRight_ElectroMechTest (tech_id, date_taken, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20,
													   a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47, a48, a49, a50)
			   VALUES ('$TechID', '$DateTime', '$q1', '$q2', '$q3', '$q4', '$q5', '$q6', '$q7', '$q8', '$q9', '$q10', '$q11', '$q12', '$q13', '$q14', '$q15', '$q16', '$q17', '$q18', '$q19', '$q20'
					   , '$q21', '$q22', '$q23', '$q24', '$q25', '$q26', '$q27', '$q28', '$q29', '$q30', '$q31', '$q32', '$q33', '$q34', '$q35', '$q36', '$q37', '$q38', '$q39', '$q40', '$q41', '$q42', '$q43', '$q44', '$q45', '$q46', '$q47', '$q48', '$q49', '$q50')");

		}

	}
	
	
	$testID = mysqlFetchAssoc(mysqlQuery("SELECT id, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20,
													   a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40,
													   a41, a42, a43, a44, a45, a46, a47, a48, a49, a50 FROM ServRight_ElectroMechTest WHERE tech_id = '".$TechID."'"));
	if ($testID) {
		$testID = $testID[0];
		$emTestID = $testID[id];
		$q1 = $testID[a1];
		$q2 = $testID[a2];
		$q3 = $testID[a3];
		$q4 = $testID[a4];
		$q5 = $testID[a5];
		$q6 = $testID[a6];
		$q7 = $testID[a7];
		$q8 = $testID[a8];
		$q9 = $testID[a9];
		$q10 = $testID[a10];

		$q11 = $testID[a11];
		$q12 = $testID[a12];
		$q13 = $testID[a13];
		$q14 = $testID[a14];
		$q15 = $testID[a15];
		$q16 = $testID[a16];
		$q17 = $testID[a17];
		$q18 = $testID[a18];
		$q19 = $testID[a19];
		$q20 = $testID[a20];

		$q21 = $testID[a21];
		$q22 = $testID[a22];
		$q23 = $testID[a23];
		$q24 = $testID[a24];
		$q25 = $testID[a25];
		$q26 = $testID[a26];
		$q27 = $testID[a27];
		$q28 = $testID[a28];
		$q29 = $testID[a29];
		$q30 = $testID[a30];

		$q31 = $testID[a31];
		$q32 = $testID[a32];
		$q33 = $testID[a33];
		$q34 = $testID[a34];
		$q35 = $testID[a35];
		$q36 = $testID[a36];
		$q37 = $testID[a37];
		$q38 = $testID[a38];
		$q39 = $testID[a39];
		$q40 = $testID[a40];

		$q41 = $testID[a41];
		$q42 = $testID[a42];
		$q43 = $testID[a43];
		$q44 = $testID[a44];
		$q45 = $testID[a45];
		$q46 = $testID[a46];
		$q47 = $testID[a47];
		$q48 = $testID[a48];
		$q49 = $testID[a49];
		$q50 = $testID[a50];

//if($q1 == "1"){echo "<p>Answer1: ". $q1 . "</p>";}
	}

?>


<form id="testForm" name="testForm" action="<?php echo $_SERVER['PHP_SELF'] . "?v=" . $_GET["v"]?>&snp=1" method="post">
<div class="formBox">
    <div class="verdana2bold" align="center">Electro-Mechanical Certification Test</div><br />
    <div class="formRow1">This is an open book exam for the Electro-Mechanical Certification. You must take the Electro-Mechanical Certification Test to be certified to do future Electro-Mechanical work. You can use the included documents to assist you in taking this exam. </div><br />
    <div class="formRow1"><strong>Note: </strong>You must have a score of 80% to be activated for the Electro-Mechanical Certification program.</div><br />
</div>

<div class="formBox">
    <div class="verdana2bold" align="left">Section 1 - Challenge Spartan 150 Paper Cutter</div><br />
	<div class="formRow1">
    <ul>
      <li><a href="Challenge%20Spartan%20150%20Training%20Course.pps">Challenge Spartan 150 Training Course</a></li>
      <li><a href="Challenge%20Spartan%20150%20Service%20Manual.pdf" target="_blank">Challenge Spartan 150 Service Manual</a></li>
      <li><a href="Challenge%20Spartan%20150%20Operators%20Manual.pdf" target="_blank">Challenge Spartan 150 Operators Manual</a></li>
    </ul><br />
    <hr /><br />
</div>    <div class="verdana2bold">1) The control board determines upper and lower cut limits by sensing the blade position via?</div><br />
    <div class="formRow1">
        <div><label><input id="question1" name="question1" value="1"  type="radio"<?php if($q1 == 1){print " checked=\"checked\"";} ?>>limit switch</label></div>
        <div><label><input id="question1" name="question1" value="2"  type="radio"<?php if($q1 == 2){print " checked=\"checked\"";} ?>>microprocessor logic</label></div>
        <div><label><input id="question1" name="question1" value="3"  type="radio"<?php if($q1 == 3){print " checked=\"checked\"";} ?>>optical sensors</label></div>
        <div><label><input id="question1" name="question1" value="4"  type="radio"<?php if($q1 == 4){print " checked=\"checked\"";} ?>>relays</label></div>
        <div><label><input id="question1" name="question1" value="5"  type="radio"<?php if($q1 == 5){print " checked=\"checked\"";} ?>>optical beam and pickup sensors</label></div>
    </div>
    <br />
    <div class="verdana2bold">2) The LED display counter works via an optical encorder driven by?</div><br />
    <div class="formRow2">        
        <div><label><input id="question2" name="question2" value="1"  type="radio"<?php if($q2 == 1){print " checked=\"checked\"";} ?>>a computer counter on the display</label></div>
        <div><label><input id="question2" name="question2" value="2"  type="radio"<?php if($q2 == 2){print " checked=\"checked\"";} ?>>a threaded shaft</label></div>
        <div><label><input id="question2" name="question2" value="3"  type="radio"<?php if($q2 == 3){print " checked=\"checked\"";} ?>>an encoder wheel on a stationary toothed belt</label></div>
        <div><label><input id="question2" name="question2" value="4"  type="radio"<?php if($q2 == 4){print " checked=\"checked\"";} ?>>magnetic strip reader</label></div>
        <div><label><input id="question2" name="question2" value="5"  type="radio"<?php if($q2 == 5){print " checked=\"checked\"";} ?>>optical encorder strip</label></div>
    </div>
    <br />
   <div class="verdana2bold">3) Back gauge straightness can be adjusted via?</div><br />
   <div class="formRow2">        
        <div><label><input id="question3" name="question3" value="1"  type="radio"<?php if($q3 == 1){print " checked=\"checked\"";} ?>>the crank handle</label></div>
        <div><label><input id="question3" name="question3" value="2"  type="radio"<?php if($q3 == 2){print " checked=\"checked\"";} ?>>set screws on the shuttle block</label></div>
        <div><label><input id="question3" name="question3" value="3"  type="radio"<?php if($q3 == 3){print " checked=\"checked\"";} ?>>threaded rod adjustment</label></div>
        <div><label><input id="question3" name="question3" value="4"  type="radio"<?php if($q3 == 4){print " checked=\"checked\"";} ?>>firmware setting</label></div>
        <div><label><input id="question3" name="question3" value="5"  type="radio"<?php if($q3 == 5){print " checked=\"checked\"";} ?>>limit stoppers on the sides</label></div>
	</div>
    <br />
   <div class="verdana2bold">4) The illuminated cut line is ?</div><br />
    <div class="formRow2">        
        <div><label><input id="question4" name="question4" value="1"  type="radio"<?php if($q4 == 1){print " checked=\"checked\"";} ?>>non-adjustable</label></div>
        <div><label><input id="question4" name="question4" value="2"  type="radio"<?php if($q4 == 2){print " checked=\"checked\"";} ?>>a computer controlled measuring device</label></div>
        <div><label><input id="question4" name="question4" value="3"  type="radio"<?php if($q4 == 3){print " checked=\"checked\"";} ?>>dangerous to stare at</label></div>
        <div><label><input id="question4" name="question4" value="4"  type="radio"<?php if($q4 == 4){print " checked=\"checked\"";} ?>>a series of LED lights</label></div>
        <div><label><input id="question4" name="question4" value="5"  type="radio"<?php if($q4 == 5){print " checked=\"checked\"";} ?>>a red laser beam</label></div>
	</div>
    <br />
   <div class="verdana2bold">5) The motor brake can be disengaged or engaged by?</div><br />
    <div class="formRow2">        
        <div><label><input id="question5" name="question5" value="1"  type="radio"<?php if($q5 == 1){print " checked=\"checked\"";} ?>>setting a jumper on the main board</label></div>
        <div><label><input id="question5" name="question5" value="2"  type="radio"<?php if($q5 == 2){print " checked=\"checked\"";} ?>>removing the lock pin</label></div>
        <div><label><input id="question5" name="question5" value="3"  type="radio"<?php if($q5 == 3){print " checked=\"checked\"";} ?>>removing the power cord</label></div>
        <div><label><input id="question5" name="question5" value="4"  type="radio"<?php if($q5 == 4){print " checked=\"checked\"";} ?>>engaging the motor in test mode</label></div>
        <div><label><input id="question5" name="question5" value="5"  type="radio"<?php if($q5 == 5){print " checked=\"checked\"";} ?>>moving the lever on the back of the motor</label></div>
	</div>
    <br />
   <div class="verdana2bold">6) Changing the cutting blade requires a?</div><br />
    <div class="formRow2">        
        <div><label><input id="question6" name="question6" value="1"  type="radio"<?php if($q6 == 1){print " checked=\"checked\"";} ?>>In-lbs torque wrench</label></div>
        <div><label><input id="question6" name="question6" value="2"  type="radio"<?php if($q6 == 2){print " checked=\"checked\"";} ?>>knife lifter assembly</label></div>
        <div><label><input id="question6" name="question6" value="3"  type="radio"<?php if($q6 == 3){print " checked=\"checked\"";} ?>>ratchet wrench</label></div>
        <div><label><input id="question6" name="question6" value="4"  type="radio"<?php if($q6 == 4){print " checked=\"checked\"";} ?>>14mm 3/8 inch drive 6 point socket</label></div>
        <div><label><input id="question6" name="question6" value="5"  type="radio"<?php if($q6 == 5){print " checked=\"checked\"";} ?>>5/16" allen wrench</label></div>
	</div>
    <br />
   <div class="verdana2bold">7) The main motor gearbox assembly drives the blade movement via?</div><br />
    <div class="formRow2">        
        <div><label><input id="question7" name="question7" value="1"  type="radio"<?php if($q7 == 1){print " checked=\"checked\"";} ?>>two gears and a chain</label></div>
        <div><label><input id="question7" name="question7" value="2"  type="radio"<?php if($q7 == 2){print " checked=\"checked\"";} ?>>a friction clutch mechanism</label></div>
        <div><label><input id="question7" name="question7" value="3"  type="radio"<?php if($q7 == 3){print " checked=\"checked\"";} ?>>a train of 5 gears</label></div>
        <div><label><input id="question7" name="question7" value="4"  type="radio"<?php if($q7 == 4){print " checked=\"checked\"";} ?>>a hydraulic pump</label></div>
        <div><label><input id="question7" name="question7" value="5"  type="radio"<?php if($q7 == 5){print " checked=\"checked\"";} ?>>a sector gear and a connecting rod</label></div>
	</div>
    <br />
   <div class="verdana2bold">8) The cutting depth adjusting is located?</div><br />
    <div class="formRow2">        
        <div><label><input id="question8" name="question8" value="1"  type="radio"<?php if($q8 == 1){print " checked=\"checked\"";} ?>>on the control board</label></div>
        <div><label><input id="question8" name="question8" value="2"  type="radio"<?php if($q8 == 2){print " checked=\"checked\"";} ?>>on the right side of the unit</label></div>
        <div><label><input id="question8" name="question8" value="3"  type="radio"<?php if($q8 == 3){print " checked=\"checked\"";} ?>>next to the power switch</label></div>
        <div><label><input id="question8" name="question8" value="4"  type="radio"<?php if($q8 == 4){print " checked=\"checked\"";} ?>>on the front panel of the unit</label></div>
        <div><label><input id="question8" name="question8" value="5"  type="radio"<?php if($q8 == 5){print " checked=\"checked\"";} ?>>on the right rear side of the unit</label></div>
	</div>
    <br />
   <div class="verdana2bold">9) The part number for the Safety Shield Magnet is?</div><br />
    <div class="formRow2">        
        <div><label><input id="question9" name="question9" value="1"  type="radio"<?php if($q9 == 1){print " checked=\"checked\"";} ?>>EE-3065</label></div>
        <div><label><input id="question9" name="question9" value="2"  type="radio"<?php if($q9 == 2){print " checked=\"checked\"";} ?>>1688-MAG</label></div>
        <div><label><input id="question9" name="question9" value="3"  type="radio"<?php if($q9 == 3){print " checked=\"checked\"";} ?>>60053</label></div>
        <div><label><input id="question9" name="question9" value="4"  type="radio"<?php if($q9 == 4){print " checked=\"checked\"";} ?>>1515-4</label></div>
        <div><label><input id="question9" name="question9" value="5"  type="radio"<?php if($q9 == 5){print " checked=\"checked\"";} ?>>1232-2</label></div>
	</div>
    <br />
	<div class="verdana2bold">10) The paper clamp operates via a?</div><br />
    <div class="formRow2">        
        <div><label><input id="question10" name="question10" value="1"  type="radio"<?php if($q10 == 1){print " checked=\"checked\"";} ?>>screw and nut mechanism</label></div>
        <div><label><input id="question10" name="question10" value="2"  type="radio"<?php if($q10 == 2){print " checked=\"checked\"";} ?>>cam and lever mechanism</label></div>
        <div><label><input id="question10" name="question10" value="3"  type="radio"<?php if($q10 == 3){print " checked=\"checked\"";} ?>>lever and interlock mechanism</label></div>
        <div><label><input id="question10" name="question10" value="4"  type="radio"<?php if($q10 == 4){print " checked=\"checked\"";} ?>>hydraulic cylinder mechanism</label></div>
        <div><label><input id="question10" name="question10" value="5"  type="radio"<?php if($q10 == 5){print " checked=\"checked\"";} ?>>air pressure servo mechanism</label></div>
	</div></div>
  <div class="formBoxFooter">
	<div><input id="doSave" name="doSave" value="Save" type="submit" /></div>
 </div>
</div>

<div class="formBox">
    <div class="verdana2bold" align="left">Section 2 - BC10 Card Cutter</div><br />
	<div class="formRow1">
    <ul>
      <li><a href="BC-10%20Card%20Cutter%20Service%20and%20Training CBT.pps">BC-10 Card Cutter Service and Training CBT</a></li>
      <li><a href="BC-10%20Card%20Cutter%20Service%20Manual.pdf" target="_blank">BC-10 Card Cutter Service Manual</a></li>
      <li><a href="BC-10%20Card%20Cutter%20Operator%20Manual.pdf" target="_blank">BC-10 Card Cutter Operator Manual</a></li>
      <li><a href="BC%2010%20Card%20Cutter%20Parts%20Manual.pdf" target="_blank">BC 10 Card Cutter Parts Manual</a></li>
    </ul><br />
    <hr /><br />
</div>    <div class="verdana2bold">11) The power supply is located?</div><br />
    <div class="formRow1">
        <div><label><input id="question11" name="question11" value="1"  type="radio"<?php if($q11== 1){print " checked=\"checked\"";} ?>>behind the back cover</label></div>
        <div><label><input id="question11" name="question11" value="2"  type="radio"<?php if($q11== 2){print " checked=\"checked\"";} ?>>behind the front cover</label></div>
        <div><label><input id="question11" name="question11" value="3"  type="radio"<?php if($q11== 3){print " checked=\"checked\"";} ?>>behind the right side cover</label></div>
        <div><label><input id="question11" name="question11" value="4"  type="radio"<?php if($q11== 4){print " checked=\"checked\"";} ?>>behind the left side cover</label></div>
        <div><label><input id="question11" name="question11" value="5"  type="radio"<?php if($q11== 5){print " checked=\"checked\"";} ?>>in an external enclosure</label></div>
    </div>
    <br />
    <div class="verdana2bold">12) The pick-up roller (or pick wheel) is driven by?</div><br />
    <div class="formRow2">        
        <div><label><input id="question12" name="question12" value="1"  type="radio"<?php if($q12== 1){print " checked=\"checked\"";} ?>>the paper motor</label></div>
        <div><label><input id="question12" name="question12" value="2"  type="radio"<?php if($q12== 2){print " checked=\"checked\"";} ?>>a gear train</label></div>
        <div><label><input id="question12" name="question12" value="3"  type="radio"<?php if($q12== 3){print " checked=\"checked\"";} ?>>the pick motor via a belt</label></div>
        <div><label><input id="question12" name="question12" value="4"  type="radio"<?php if($q12== 4){print " checked=\"checked\"";} ?>>the pick clutch</label></div>
        <div><label><input id="question12" name="question12" value="5"  type="radio"<?php if($q12== 5){print " checked=\"checked\"";} ?>>friction with another roller</label></div>
    </div>
    <br />
   <div class="verdana2bold">13) The S3 sensor is located?</div><br />
   <div class="formRow2">        
        <div><label><input id="question13" name="question13" value="1"  type="radio"<?php if($q13== 1){print " checked=\"checked\"";} ?>>In the waste bin</label></div>
        <div><label><input id="question13" name="question13" value="2"  type="radio"<?php if($q13== 2){print " checked=\"checked\"";} ?>>on the paper tray</label></div>
        <div><label><input id="question13" name="question13" value="3"  type="radio"<?php if($q13== 3){print " checked=\"checked\"";} ?>>inside the slitter assembly</label></div>
        <div><label><input id="question13" name="question13" value="4"  type="radio"<?php if($q13== 4){print " checked=\"checked\"";} ?>>next to the S1 sensor under the pick assembly</label></div>
        <div><label><input id="question13" name="question13" value="5"  type="radio"<?php if($q13== 5){print " checked=\"checked\"";} ?>>beneath the pressure roller unit</label></div>
	</div>
    <br />
   <div class="verdana2bold">14) The cutter unit must be aligned to the chassis when?</div><br />
    <div class="formRow2">        
        <div><label><input id="question14" name="question14" value="1"  type="radio"<?php if($q14== 1){print " checked=\"checked\"";} ?>>it is run in reverse</label></div>
        <div><label><input id="question14" name="question14" value="2"  type="radio"<?php if($q14== 2){print " checked=\"checked\"";} ?>>the blades are sharpened</label></div>
        <div><label><input id="question14" name="question14" value="3"  type="radio"<?php if($q14== 3){print " checked=\"checked\"";} ?>>the card layout changes</label></div>
        <div><label><input id="question14" name="question14" value="4"  type="radio"<?php if($q14== 4){print " checked=\"checked\"";} ?>>it is removed or replaced</label></div>
        <div><label><input id="question14" name="question14" value="5"  type="radio"<?php if($q14== 5){print " checked=\"checked\"";} ?>>the slitter is removed</label></div>
	</div>
    <br />
   <div class="verdana2bold">15) The main board is located?</div><br />
    <div class="formRow2">        
        <div><label><input id="question15" name="question15" value="1"  type="radio"<?php if($q15== 1){print " checked=\"checked\"";} ?>>behind the front cover</label></div>
        <div><label><input id="question15" name="question15" value="2"  type="radio"<?php if($q15== 2){print " checked=\"checked\"";} ?>>behind the rear cover</label></div>
        <div><label><input id="question15" name="question15" value="3"  type="radio"<?php if($q15== 3){print " checked=\"checked\"";} ?>>behind the right side cover</label></div>
        <div><label><input id="question15" name="question15" value="4"  type="radio"<?php if($q15== 4){print " checked=\"checked\"";} ?>>behind the bottom cover</label></div>
        <div><label><input id="question15" name="question15" value="5"  type="radio"<?php if($q15== 5){print " checked=\"checked\"";} ?>>behind the left cover</label></div>
	</div>
    <br />
   <div class="verdana2bold">16) The S2 sensor detects?</div><br />
    <div class="formRow2">        
        <div><label><input id="question16" name="question16" value="1"  type="radio"<?php if($q16== 1){print " checked=\"checked\"";} ?>>the margins</label></div>
        <div><label><input id="question16" name="question16" value="2"  type="radio"<?php if($q16== 2){print " checked=\"checked\"";} ?>>the paper size</label></div>
        <div><label><input id="question16" name="question16" value="3"  type="radio"<?php if($q16== 3){print " checked=\"checked\"";} ?>>the leading edge of the paper or a cut mark</label></div>
        <div><label><input id="question16" name="question16" value="4"  type="radio"<?php if($q16== 4){print " checked=\"checked\"";} ?>>humidity</label></div>
        <div><label><input id="question16" name="question16" value="5"  type="radio"<?php if($q16== 5){print " checked=\"checked\"";} ?>>the card size</label></div>
	</div>
    <br />
   <div class="verdana2bold">17) The single cut layout for letter-sized paper on a sheet of business cards has left and right margins that measure?</div><br />
    <div class="formRow2">        
        <div><label><input id="question17" name="question17" value="1"  type="radio"<?php if($q17== 1){print " checked=\"checked\"";} ?>>1/4"</label></div>
        <div><label><input id="question17" name="question17" value="2"  type="radio"<?php if($q17== 2){print " checked=\"checked\"";} ?>>1/2"</label></div>
        <div><label><input id="question17" name="question17" value="3"  type="radio"<?php if($q17== 3){print " checked=\"checked\"";} ?>>1 Inch</label></div>
        <div><label><input id="question17" name="question17" value="4"  type="radio"<?php if($q17== 4){print " checked=\"checked\"";} ?>>3/4"</label></div>
        <div><label><input id="question17" name="question17" value="5"  type="radio"<?php if($q17== 5){print " checked=\"checked\"";} ?>>3/4" and 1/4"</label></div>
	</div>
    <br />
   <div class="verdana2bold">18) An E002 error indicates?</div><br />
    <div class="formRow2">        
        <div><label><input id="question18" name="question18" value="1"  type="radio"<?php if($q18== 1){print " checked=\"checked\"";} ?>>the waste bin is full</label></div>
        <div><label><input id="question18" name="question18" value="2"  type="radio"<?php if($q18== 2){print " checked=\"checked\"";} ?>>a wrong card size</label></div>
        <div><label><input id="question18" name="question18" value="3"  type="radio"<?php if($q18== 3){print " checked=\"checked\"";} ?>>the cut mark is in the wrong place</label></div>
        <div><label><input id="question18" name="question18" value="4"  type="radio"<?php if($q18== 4){print " checked=\"checked\"";} ?>>miss-pick in the paper tray</label></div>
        <div><label><input id="question18" name="question18" value="5"  type="radio"<?php if($q18== 5){print " checked=\"checked\"";} ?>>the cutter assembly failed to cut</label></div>
	</div>
    <br />
   <div class="verdana2bold">19) An E007 error indicates?</div><br />
    <div class="formRow2">        
        <div><label><input id="question19" name="question19" value="1"  type="radio"<?php if($q19== 1){print " checked=\"checked\"";} ?>>the slitter is not seated correctly</label></div>
        <div><label><input id="question19" name="question19" value="2"  type="radio"<?php if($q19== 2){print " checked=\"checked\"";} ?>>the sheet did not reach the S3 sensor in time</label></div>
        <div><label><input id="question19" name="question19" value="3"  type="radio"<?php if($q19== 3){print " checked=\"checked\"";} ?>>the pressure roller unit is not installed</label></div>
        <div><label><input id="question19" name="question19" value="4"  type="radio"<?php if($q19== 4){print " checked=\"checked\"";} ?>>there is paper stuck under the S2 sensor</label></div>
        <div><label><input id="question19" name="question19" value="5"  type="radio"<?php if($q19== 5){print " checked=\"checked\"";} ?>>paper skew is detected when feeding</label></div>
	</div>
    <br />
	<div class="verdana2bold">20) The slitter unit is?</div><br />
    <div class="formRow2">        
        <div><label><input id="question20" name="question20" value="1"  type="radio"<?php if($q20== 1){print " checked=\"checked\"";} ?>>scissor like cutting blades</label></div>
        <div><label><input id="question20" name="question20" value="2"  type="radio"<?php if($q20== 2){print " checked=\"checked\"";} ?>>non-replacable</label></div>
        <div><label><input id="question20" name="question20" value="3"  type="radio"<?php if($q20== 3){print " checked=\"checked\"";} ?>>diagonal cutting blades</label></div>
        <div><label><input id="question20" name="question20" value="4"  type="radio"<?php if($q20== 4){print " checked=\"checked\"";} ?>>an easy to remove modular unit</label></div>
        <div><label><input id="question20" name="question20" value="5"  type="radio"<?php if($q20== 5){print " checked=\"checked\"";} ?>>easy to resharpen</label></div>
	</div></div>
  <div class="formBoxFooter">
	<div><input id="doSave" name="doSave" value="Save" type="submit" /></div>
 </div>
</div>

<div class="formBox">
    <div class="verdana2bold" align="left">Section 3 - GBC Catena 65 Laminator</div><br />
	<div class="formRow1">
    <ul>
      <li><a href="GBC%20Catena%2065R%20Training%20and%20Service%20PowerPoint.pps" target="_blank">GBC Catena 65R Training and Service Training</a></li>
      <li><a href="GBC%20Catena%20Service%20Manual.pdf" target="_blank">GBC Catena Service Manual</a></li>
      <li><a href="GBC%20CATENA%20PARTS%20manual.pdf" target="_blank">GBC CATENA PARTS manual</a></li>
    </ul><br />
    <hr /><br />
</div>    <div class="verdana2bold">21) There are ________ holding the left side cover on?</div><br />
    <div class="formRow1">
        <div><label><input id="question21" name="question21" value="1"  type="radio"<?php if($q21== 1){print " checked=\"checked\"";} ?>>2 clips and 2 screws</label></div>
        <div><label><input id="question21" name="question21" value="2"  type="radio"<?php if($q21== 2){print " checked=\"checked\"";} ?>>6 screws</label></div>
        <div><label><input id="question21" name="question21" value="3"  type="radio"<?php if($q21== 3){print " checked=\"checked\"";} ?>>4 screws</label></div>
        <div><label><input id="question21" name="question21" value="4"  type="radio"<?php if($q21== 4){print " checked=\"checked\"";} ?>>2 wing nuts</label></div>
        <div><label><input id="question21" name="question21" value="5"  type="radio"<?php if($q21== 5){print " checked=\"checked\"";} ?>>2 brackets</label></div>
    </div>
    <br />
    <div class="verdana2bold">22) There are _______ for the film tension adjustments on the Catena 65 laminator?</div><br />
    <div class="formRow2">        
        <div><label><input id="question22" name="question22" value="1"  type="radio"<?php if($q22== 1){print " checked=\"checked\"";} ?>>electro-magnetic clutches</label></div>
        <div><label><input id="question22" name="question22" value="2"  type="radio"<?php if($q22== 2){print " checked=\"checked\"";} ?>>back tension levers</label></div>
        <div><label><input id="question22" name="question22" value="3"  type="radio"<?php if($q22== 3){print " checked=\"checked\"";} ?>>automatic clutches</label></div>
        <div><label><input id="question22" name="question22" value="4"  type="radio"<?php if($q22== 4){print " checked=\"checked\"";} ?>>two adjuster knobs</label></div>
        <div><label><input id="question22" name="question22" value="5"  type="radio"<?php if($q22== 5){print " checked=\"checked\"";} ?>>4 adjuster controls</label></div>
    </div>
    <br />
   <div class="verdana2bold">23) The part number for the DC geared is?</div><br />
   <div class="formRow2">        
        <div><label><input id="question23" name="question23" value="1"  type="radio"<?php if($q23== 1){print " checked=\"checked\"";} ?>>6090255</label></div>
        <div><label><input id="question23" name="question23" value="2"  type="radio"<?php if($q23== 2){print " checked=\"checked\"";} ?>>6090202</label></div>
        <div><label><input id="question23" name="question23" value="3"  type="radio"<?php if($q23== 3){print " checked=\"checked\"";} ?>>22-29-4754</label></div>
        <div><label><input id="question23" name="question23" value="4"  type="radio"<?php if($q23== 4){print " checked=\"checked\"";} ?>>15447-M</label></div>
        <div><label><input id="question23" name="question23" value="5"  type="radio"<?php if($q23== 5){print " checked=\"checked\"";} ?>>6095871</label></div>
	</div>
    <br />
   <div class="verdana2bold">24) The latest version of the main board requires?</div><br />
    <div class="formRow2">        
        <div><label><input id="question24" name="question24" value="1"  type="radio"<?php if($q24== 1){print " checked=\"checked\"";} ?>>resetting the firmware in the display board</label></div>
        <div><label><input id="question24" name="question24" value="2"  type="radio"<?php if($q24== 2){print " checked=\"checked\"";} ?>>a different power transformer</label></div>
        <div><label><input id="question24" name="question24" value="3"  type="radio"<?php if($q24== 3){print " checked=\"checked\"";} ?>>a new version IR sensor</label></div>
        <div><label><input id="question24" name="question24" value="4"  type="radio"<?php if($q24== 4){print " checked=\"checked\"";} ?>>a new version of the bi-metal temp sensors</label></div>
        <div><label><input id="question24" name="question24" value="5"  type="radio"<?php if($q24== 5){print " checked=\"checked\"";} ?>>a special heat shield be installed which comes with the unit</label></div>
	</div>
    <br />
   <div class="verdana2bold">25) The bi-metal thermostat for the upper heat roller has _______________ terminals?</div><br />
    <div class="formRow2">        
        <div><label><input id="question25" name="question25" value="1"  type="radio"<?php if($q25== 1){print " checked=\"checked\"";} ?>>solder on</label></div>
        <div><label><input id="question25" name="question25" value="2"  type="radio"<?php if($q25== 2){print " checked=\"checked\"";} ?>>3</label></div>
        <div><label><input id="question25" name="question25" value="3"  type="radio"<?php if($q25== 3){print " checked=\"checked\"";} ?>>4</label></div>
        <div><label><input id="question25" name="question25" value="4"  type="radio"<?php if($q25== 4){print " checked=\"checked\"";} ?>>2</label></div>
        <div><label><input id="question25" name="question25" value="5"  type="radio"<?php if($q25== 5){print " checked=\"checked\"";} ?>>1 ground wire and 2</label></div>
	</div>
    <br />
   <div class="verdana2bold">26) ________________ are needed to remove the heat rollers for replacement?</div><br />
    <div class="formRow2">        
        <div><label><input id="question26" name="question26" value="1"  type="radio"<?php if($q26== 1){print " checked=\"checked\"";} ?>>7mm socket</label></div>
        <div><label><input id="question26" name="question26" value="2"  type="radio"<?php if($q26== 2){print " checked=\"checked\"";} ?>>13mm wrench</label></div>
        <div><label><input id="question26" name="question26" value="3"  type="radio"<?php if($q26== 3){print " checked=\"checked\"";} ?>>GBC spanner tool</label></div>
        <div><label><input id="question26" name="question26" value="4"  type="radio"<?php if($q26== 4){print " checked=\"checked\"";} ?>>2 small flat blade screwdrivers</label></div>
        <div><label><input id="question26" name="question26" value="5"  type="radio"<?php if($q26== 5){print " checked=\"checked\"";} ?>>Snap ring pliers</label></div>
	</div>
    <br />
   <div class="verdana2bold">27) To remove the grey pull rollers you need ______________ to remove the sprocket gear.</div><br />
    <div class="formRow2">        
        <div><label><input id="question27" name="question27" value="1"  type="radio"<?php if($q27== 1){print " checked=\"checked\"";} ?>>an E-clip tool</label></div>
        <div><label><input id="question27" name="question27" value="2"  type="radio"<?php if($q27== 2){print " checked=\"checked\"";} ?>>snap ring pliers</label></div>
        <div><label><input id="question27" name="question27" value="3"  type="radio"<?php if($q27== 3){print " checked=\"checked\"";} ?>>a 4mm allen wrench</label></div>
        <div><label><input id="question27" name="question27" value="4"  type="radio"<?php if($q27== 4){print " checked=\"checked\"";} ?>>a 2.5mm allen wrench</label></div>
        <div><label><input id="question27" name="question27" value="5"  type="radio"<?php if($q27== 5){print " checked=\"checked\"";} ?>>a 7mm socket</label></div>
	</div>
    <br />
   <div class="verdana2bold">28) If the temp display reads in celcius this indicates?</div><br />
    <div class="formRow2">        
        <div><label><input id="question28" name="question28" value="1"  type="radio"<?php if($q28== 1){print " checked=\"checked\"";} ?>>the bi-metal thermostats are plugged into wrong locations</label></div>
        <div><label><input id="question28" name="question28" value="2"  type="radio"<?php if($q28== 2){print " checked=\"checked\"";} ?>>the European version of the main board is installed</label></div>
        <div><label><input id="question28" name="question28" value="3"  type="radio"<?php if($q28== 3){print " checked=\"checked\"";} ?>>fahrenheit has not been selected on the main board</label></div>
        <div><label><input id="question28" name="question28" value="4"  type="radio"<?php if($q28== 4){print " checked=\"checked\"";} ?>>the CPU controllers firmware is the wrong version</label></div>
        <div><label><input id="question28" name="question28" value="5"  type="radio"<?php if($q28== 5){print " checked=\"checked\"";} ?>>the wrong version temp sensor is installed</label></div>
	</div>
    <br />
   <div class="verdana2bold">29) When you have a failed main board you need to ________________ before ordering a replacement board.</div><br />
    <div class="formRow2">        
        <div><label><input id="question29" name="question29" value="1"  type="radio"<?php if($q29== 1){print " checked=\"checked\"";} ?>>determine what temperature setting the board has</label></div>
        <div><label><input id="question29" name="question29" value="2"  type="radio"<?php if($q29== 2){print " checked=\"checked\"";} ?>>determine what output wattage the board is</label></div>
        <div><label><input id="question29" name="question29" value="3"  type="radio"<?php if($q29== 3){print " checked=\"checked\"";} ?>>determine what firmware version is installed</label></div>
        <div><label><input id="question29" name="question29" value="4"  type="radio"<?php if($q29== 4){print " checked=\"checked\"";} ?>>count how many connectors to determine which version</label></div>
        <div><label><input id="question29" name="question29" value="5"  type="radio"<?php if($q29== 5){print " checked=\"checked\"";} ?>>determine which version via the serial number of the unit</label></div>
	</div>
    <br />
	<div class="verdana2bold">30) The output voltage of the power transformer are?</div><br />
    <div class="formRow2">        
        <div><label><input id="question30" name="question30" value="1"  type="radio"<?php if($q30== 1){print " checked=\"checked\"";} ?>>12V DC and 5V AC</label></div>
        <div><label><input id="question30" name="question30" value="2"  type="radio"<?php if($q30== 2){print " checked=\"checked\"";} ?>>53V AC and 17V AC</label></div>
        <div><label><input id="question30" name="question30" value="3"  type="radio"<?php if($q30== 3){print " checked=\"checked\"";} ?>>20V AC and 50V AC for the heaters</label></div>
        <div><label><input id="question30" name="question30" value="4"  type="radio"<?php if($q30== 4){print " checked=\"checked\"";} ?>>17V AC and 36V AC</label></div>
        <div><label><input id="question30" name="question30" value="5"  type="radio"<?php if($q30== 5){print " checked=\"checked\"";} ?>>20V AC and 10V AC</label></div>
	</div></div>
  <div class="formBoxFooter">
	<div><input id="doSave" name="doSave" value="Save" type="submit" /></div>
 </div>
</div>

<div class="formBox">
    <div class="verdana2bold" align="left">Section 4 - Rhin-O-Tuff Binding Punch </div><br />
	<div class="formRow1">
    <ul>
      <li><a href="Rhin-O-Tuff%20PDI%207000%20Training Course.pps" target="_blank">Rhin-O-Tuff PDI 7000 Training Course</a></li>
      <li><a href="Rhin-O-Tuff%20PDI%207000%20Parts Manual.pdf" target="_blank">Rhin-O-Tuff PDI 7000 Parts Manual</a></li>
    </ul><br />
    <hr /><br />
</div>    <div class="verdana2bold">31) To remove a stuck die you can?</div><br />
    <div class="formRow1">
        <div><label><input id="question31" name="question31" value="1"  type="radio"<?php if($q31== 1){print " checked=\"checked\"";} ?>>open top lid and remove the die</label></div>
        <div><label><input id="question31" name="question31" value="2"  type="radio"<?php if($q31== 2){print " checked=\"checked\"";} ?>>remove all paper from the punch</label></div>
        <div><label><input id="question31" name="question31" value="3"  type="radio"<?php if($q31== 3){print " checked=\"checked\"";} ?>>switch punch into reverse</label></div>
        <div><label><input id="question31" name="question31" value="4"  type="radio"<?php if($q31== 4){print " checked=\"checked\"";} ?>>use the special reverse tool to put the clamp in full back position</label></div>
        <div><label><input id="question31" name="question31" value="5"  type="radio"<?php if($q31== 5){print " checked=\"checked\"";} ?>>unlock the die by removing the guard pins</label></div>
    </div>
    <br />
    <div class="verdana2bold">32) The black levers on the left and right sides of the punch are to?</div><br />
    <div class="formRow2">        
        <div><label><input id="question32" name="question32" value="1"  type="radio"<?php if($q32== 1){print " checked=\"checked\"";} ?>>adjust the punch skew</label></div>
        <div><label><input id="question32" name="question32" value="2"  type="radio"<?php if($q32== 2){print " checked=\"checked\"";} ?>>manually drive the punch mechanism</label></div>
        <div><label><input id="question32" name="question32" value="3"  type="radio"<?php if($q32== 3){print " checked=\"checked\"";} ?>>eject the waste paper bin</label></div>
        <div><label><input id="question32" name="question32" value="4"  type="radio"<?php if($q32== 4){print " checked=\"checked\"";} ?>>lock or unlock the die</label></div>
        <div><label><input id="question32" name="question32" value="5"  type="radio"<?php if($q32== 5){print " checked=\"checked\"";} ?>>clear debris from the punch</label></div>
    </div>
    <br />
   <div class="verdana2bold">33) Punching more than 4 or 5 sheets of clear plastic covers will?</div><br />
   <div class="formRow2">        
        <div><label><input id="question33" name="question33" value="1"  type="radio"<?php if($q33== 1){print " checked=\"checked\"";} ?>>throw off the punch settings</label></div>
        <div><label><input id="question33" name="question33" value="2"  type="radio"<?php if($q33== 2){print " checked=\"checked\"";} ?>>cause the die to unlock</label></div>
        <div><label><input id="question33" name="question33" value="3"  type="radio"<?php if($q33== 3){print " checked=\"checked\"";} ?>>punch the wrong number of holes</label></div>
        <div><label><input id="question33" name="question33" value="4"  type="radio"<?php if($q33== 4){print " checked=\"checked\"";} ?>>possibly severely jam the die</label></div>
        <div><label><input id="question33" name="question33" value="5"  type="radio"<?php if($q33== 5){print " checked=\"checked\"";} ?>>confuse the sensors and cause an error</label></div>
	</div>
    <br />
   <div class="verdana2bold">34) If the manual reverse tool cannot be found you can use?</div><br />
    <div class="formRow2">        
        <div><label><input id="question34" name="question34" value="1"  type="radio"<?php if($q34== 1){print " checked=\"checked\"";} ?>>a pair of needle nose pliers</label></div>
        <div><label><input id="question34" name="question34" value="2"  type="radio"<?php if($q34== 2){print " checked=\"checked\"";} ?>>a Phillips screwdriver</label></div>
        <div><label><input id="question34" name="question34" value="3"  type="radio"<?php if($q34== 3){print " checked=\"checked\"";} ?>>a large flat blade screwdriver</label></div>
        <div><label><input id="question34" name="question34" value="4"  type="radio"<?php if($q34== 4){print " checked=\"checked\"";} ?>>a wooden dowel</label></div>
        <div><label><input id="question34" name="question34" value="5"  type="radio"<?php if($q34== 5){print " checked=\"checked\"";} ?>>a 8mm allen wrench</label></div>
	</div>
    <br />
   <div class="verdana2bold">35) The interlock safety switches on the punch are activated by?</div><br />
    <div class="formRow2">        
        <div><label><input id="question35" name="question35" value="1"  type="radio"<?php if($q35== 1){print " checked=\"checked\"";} ?>>the brake interlock switch mechanism</label></div>
        <div><label><input id="question35" name="question35" value="2"  type="radio"<?php if($q35== 2){print " checked=\"checked\"";} ?>>the left and right die position switches</label></div>
        <div><label><input id="question35" name="question35" value="3"  type="radio"<?php if($q35== 3){print " checked=\"checked\"";} ?>>the clamp when in full reverse position</label></div>
        <div><label><input id="question35" name="question35" value="4"  type="radio"<?php if($q35== 4){print " checked=\"checked\"";} ?>>the left and right lock levers and top cover</label></div>
        <div><label><input id="question35" name="question35" value="5"  type="radio"<?php if($q35== 5){print " checked=\"checked\"";} ?>>the main motor</label></div>
	</div>
    <br />
   <div class="verdana2bold">36) ____________ may prevent the left or right lock levers from engaging fully and locking.</div><br />
    <div class="formRow2">        
        <div><label><input id="question36" name="question36" value="1"  type="radio"<?php if($q36== 1){print " checked=\"checked\"";} ?>>A broken electrical connector on the die</label></div>
        <div><label><input id="question36" name="question36" value="2"  type="radio"<?php if($q36== 2){print " checked=\"checked\"";} ?>>Die 1 not being installed correctly before installing the 2nd die</label></div>
        <div><label><input id="question36" name="question36" value="3"  type="radio"<?php if($q36== 3){print " checked=\"checked\"";} ?>>Incorrect job settings on the punch</label></div>
        <div><label><input id="question36" name="question36" value="4"  type="radio"<?php if($q36== 4){print " checked=\"checked\"";} ?>>Waste paper, dirt and debris</label></div>
        <div><label><input id="question36" name="question36" value="5"  type="radio"<?php if($q36== 5){print " checked=\"checked\"";} ?>>A die that is inserted backwards</label></div>
	</div>
    <br />
   <div class="verdana2bold">37) There are _________ holding the bottom cover to the punch.</div><br />
    <div class="formRow2">        
        <div><label><input id="question37" name="question37" value="1"  type="radio"<?php if($q37== 1){print " checked=\"checked\"";} ?>>8 screws</label></div>
        <div><label><input id="question37" name="question37" value="2"  type="radio"<?php if($q37== 2){print " checked=\"checked\"";} ?>>4 thumb screws</label></div>
        <div><label><input id="question37" name="question37" value="3"  type="radio"<?php if($q37== 3){print " checked=\"checked\"";} ?>>2 slide rails</label></div>
        <div><label><input id="question37" name="question37" value="4"  type="radio"<?php if($q37== 4){print " checked=\"checked\"";} ?>>4 screws in the rubber feet</label></div>
        <div><label><input id="question37" name="question37" value="5"  type="radio"<?php if($q37== 5){print " checked=\"checked\"";} ?>>4 screws</label></div>
	</div>
    <br />
   <div class="verdana2bold">38) The precise motion of the punch mechanism is controlled by a?</div><br />
    <div class="formRow2">        
        <div><label><input id="question38" name="question38" value="1"  type="radio"<?php if($q38== 1){print " checked=\"checked\"";} ?>>the bi-metal thermostats are plugged into wrong locations</label></div>
        <div><label><input id="question38" name="question38" value="2"  type="radio"<?php if($q38== 2){print " checked=\"checked\"";} ?>>flywheel</label></div>
        <div><label><input id="question38" name="question38" value="3"  type="radio"<?php if($q38== 3){print " checked=\"checked\"";} ?>>stepper motor</label></div>
        <div><label><input id="question38" name="question38" value="4"  type="radio"<?php if($q38== 4){print " checked=\"checked\"";} ?>>DC servo feedback loop</label></div>
        <div><label><input id="question38" name="question38" value="5"  type="radio"<?php if($q38== 5){print " checked=\"checked\"";} ?>>electric friction clutch on the motor</label></div>
	</div>
    <br />
   <div class="verdana2bold">39) A ____________ tool is needed to remove the levers from the lock mechanism.?</div><br />
    <div class="formRow2">        
        <div><label><input id="question39" name="question39" value="1"  type="radio"<?php if($q39== 1){print " checked=\"checked\"";} ?>>6mm socket</label></div>
        <div><label><input id="question39" name="question39" value="2"  type="radio"<?php if($q39== 2){print " checked=\"checked\"";} ?>>screwdriver</label></div>
        <div><label><input id="question39" name="question39" value="3"  type="radio"<?php if($q39== 3){print " checked=\"checked\"";} ?>>punch</label></div>
        <div><label><input id="question39" name="question39" value="4"  type="radio"<?php if($q39== 4){print " checked=\"checked\"";} ?>>allen wrench</label></div>
        <div><label><input id="question39" name="question39" value="5"  type="radio"<?php if($q39== 5){print " checked=\"checked\"";} ?>>snap ring</label></div>
	</div>
    <br />
	<div class="verdana2bold">40) The part number for the foot pedal assembly is?</div><br />
    <div class="formRow2">        
        <div><label><input id="question40" name="question40" value="1"  type="radio"<?php if($q40== 1){print " checked=\"checked\"";} ?>>007441</label></div>
        <div><label><input id="question40" name="question40" value="2"  type="radio"<?php if($q40== 2){print " checked=\"checked\"";} ?>>05455</label></div>
        <div><label><input id="question40" name="question40" value="3"  type="radio"<?php if($q40== 3){print " checked=\"checked\"";} ?>>0055421</label></div>
        <div><label><input id="question40" name="question40" value="4"  type="radio"<?php if($q40== 4){print " checked=\"checked\"";} ?>>007031</label></div>
        <div><label><input id="question40" name="question40" value="5"  type="radio"<?php if($q40== 5){print " checked=\"checked\"";} ?>>6695-88</label></div>
	</div></div>
  <div class="formBoxFooter">
	<div><input id="doSave" name="doSave" value="Save" type="submit" /></div>
 </div>
</div>


<div class="formBox">
    <div class="verdana2bold" align="left">Section 5 - Standard Horizon Folder </div><br />
	<div class="formRow1">
    <ul>
      <li><a href="PF-P310%20Standard%20Folder%20training.pps" target="_blank">PF-P310 Standard Folder training</a></li>
      <li><a href="PF-P310%20Standard%20Folder%20Parts%20Manual.pdf" target="_blank">PF-P310 Standard Folder Parts Manual</a></li>
      <li><a href="PF-P310%20Standard%20Folder%20Maintenance%20Manual.pdf" target="_blank">PF-P310 Standard Folder Maintenance Manual</a></li>
      <li><a href="PF-P310%20Standard%20Folder%20Operators%20Manual.pdf" target="_blank">PF-P310 Standard Folder Operators Manual</a></li>
      <li><a href="PF-P310 Standard%20Folder%20-%20Making%20a%20V%20Fold.pps" target="_blank">PF-P310 Standard Folder - Making a V Fold</a></li>
      <li><a href="PF-P310%20Standard%20Folder%20-%20Making%20a%20C%20Fold.pps" target="_blank">PF-P310 Standard Folder - Making a C Fold</a></li>
    </ul><br />
    <hr /><br />
</div>    <div class="verdana2bold">41) The 2 paper stop knobs on the front of the folder adjust?</div><br />
    <div class="formRow1">
        <div><label><input id="question41" name="question41" value="1"  type="radio"<?php if($q41== 1){print " checked=\"checked\"";} ?>>the fold plate angles</label></div>
        <div><label><input id="question41" name="question41" value="2"  type="radio"<?php if($q41== 2){print " checked=\"checked\"";} ?>>the backstop timing</label></div>
        <div><label><input id="question41" name="question41" value="3"  type="radio"<?php if($q41== 3){print " checked=\"checked\"";} ?>>the position of the folds on the paper</label></div>
        <div><label><input id="question41" name="question41" value="4"  type="radio"<?php if($q41== 4){print " checked=\"checked\"";} ?>>the paper exit setting</label></div>
        <div><label><input id="question41" name="question41" value="5"  type="radio"<?php if($q41== 5){print " checked=\"checked\"";} ?>>the paper size settings</label></div>
    </div>
    <br />
    <div class="verdana2bold">42) The part number for the feed solenoid is?</div><br />
    <div class="formRow2">        
        <div><label><input id="question42" name="question42" value="1"  type="radio"<?php if($q42== 1){print " checked=\"checked\"";} ?>>44.95.995</label></div>
        <div><label><input id="question42" name="question42" value="2"  type="radio"<?php if($q42== 2){print " checked=\"checked\"";} ?>>4009284-00</label></div>
        <div><label><input id="question42" name="question42" value="3"  type="radio"<?php if($q42== 3){print " checked=\"checked\"";} ?>>5625-9</label></div>
        <div><label><input id="question42" name="question42" value="4"  type="radio"<?php if($q42== 4){print " checked=\"checked\"";} ?>>400252-01</label></div>
        <div><label><input id="question42" name="question42" value="5"  type="radio"<?php if($q42== 5){print " checked=\"checked\"";} ?>>4004603-00</label></div>
    </div>
    <br />
   <div class="verdana2bold">43) To remove the feed roller shaft, it is necessary to?</div><br />
   <div class="formRow2">        
        <div><label><input id="question43" name="question43" value="1"  type="radio"<?php if($q43== 1){print " checked=\"checked\"";} ?>>unlock the shaft from the retainers and slide it out</label></div>
        <div><label><input id="question43" name="question43" value="2"  type="radio"<?php if($q43== 2){print " checked=\"checked\"";} ?>>remove the snap rings and slide the shaft out</label></div>
        <div><label><input id="question43" name="question43" value="3"  type="radio"<?php if($q43== 3){print " checked=\"checked\"";} ?>>push the shaft toward the front of the unit and pull the shaft assembly up and out</label></div>
        <div><label><input id="question43" name="question43" value="4"  type="radio"<?php if($q43== 4){print " checked=\"checked\"";} ?>>remove the front and back covers first</label></div>
        <div><label><input id="question43" name="question43" value="5"  type="radio"<?php if($q43== 5){print " checked=\"checked\"";} ?>>disassemble the pick clutch assembly first</label></div>
	</div>
    <br />
   <div class="verdana2bold">44) The part number for the 24V power supply is?</div><br />
    <div class="formRow2">        
        <div><label><input id="question44" name="question44" value="1"  type="radio"<?php if($q44== 1){print " checked=\"checked\"";} ?>>24vPS-1586</label></div>
        <div><label><input id="question44" name="question44" value="2"  type="radio"<?php if($q44== 2){print " checked=\"checked\"";} ?>>32877</label></div>
        <div><label><input id="question44" name="question44" value="3"  type="radio"<?php if($q44== 3){print " checked=\"checked\"";} ?>>1695-88</label></div>
        <div><label><input id="question44" name="question44" value="4"  type="radio"<?php if($q44== 4){print " checked=\"checked\"";} ?>>4009275-00</label></div>
        <div><label><input id="question44" name="question44" value="5"  type="radio"<?php if($q44== 5){print " checked=\"checked\"";} ?>>4015864-00</label></div>
	</div>
    <br />
   <div class="verdana2bold">45) To engage the feed table you must?</div><br />
    <div class="formRow2">        
        <div><label><input id="question45" name="question45" value="1"  type="radio"<?php if($q45== 1){print " checked=\"checked\"";} ?>>lower the feed tray lever</label></div>
        <div><label><input id="question45" name="question45" value="2"  type="radio"<?php if($q45== 2){print " checked=\"checked\"";} ?>>adjust the fold position</label></div>
        <div><label><input id="question45" name="question45" value="3"  type="radio"<?php if($q45== 3){print " checked=\"checked\"";} ?>>press the start button to start the feed motor</label></div>
        <div><label><input id="question45" name="question45" value="4"  type="radio"<?php if($q45== 4){print " checked=\"checked\"";} ?>>raise the feed tray lever</label></div>
        <div><label><input id="question45" name="question45" value="5"  type="radio"<?php if($q45== 5){print " checked=\"checked\"";} ?>>select paper fold</label></div>
	</div>
    <br />
   <div class="verdana2bold">46) The paper stoppers are movable via?</div><br />
    <div class="formRow2">        
        <div><label><input id="question46" name="question46" value="1"  type="radio"<?php if($q46== 1){print " checked=\"checked\"";} ?>>the fold rollers</label></div>
        <div><label><input id="question46" name="question46" value="2"  type="radio"<?php if($q46== 2){print " checked=\"checked\"";} ?>>the main motor</label></div>
        <div><label><input id="question46" name="question46" value="3"  type="radio"<?php if($q46== 3){print " checked=\"checked\"";} ?>>toothed belts driven via the stop knobs</label></div>
        <div><label><input id="question46" name="question46" value="4"  type="radio"<?php if($q46== 4){print " checked=\"checked\"";} ?>>threaded shafts</label></div>
        <div><label><input id="question46" name="question46" value="5"  type="radio"<?php if($q46== 5){print " checked=\"checked\"";} ?>>the RUN lever</label></div>
	</div>
    <br />
   <div class="verdana2bold">47) There are 2 __________ that activate the page deflector.</div><br />
    <div class="formRow2">        
        <div><label><input id="question47" name="question47" value="1"  type="radio"<?php if($q47== 1){print " checked=\"checked\"";} ?>>page limiters</label></div>
        <div><label><input id="question47" name="question47" value="2"  type="radio"<?php if($q47== 2){print " checked=\"checked\"";} ?>>paper chutes</label></div>
        <div><label><input id="question47" name="question47" value="3"  type="radio"<?php if($q47== 3){print " checked=\"checked\"";} ?>>micro switches</label></div>
        <div><label><input id="question47" name="question47" value="4"  type="radio"<?php if($q47== 4){print " checked=\"checked\"";} ?>>levers</label></div>
        <div><label><input id="question47" name="question47" value="5"  type="radio"<?php if($q47== 5){print " checked=\"checked\"";} ?>>solenoids</label></div>
	</div>
    <br />
   <div class="verdana2bold">48) The deflector gate is accessible when you?</div><br />
    <div class="formRow2">        
        <div><label><input id="question48" name="question48" value="1"  type="radio"<?php if($q48== 1){print " checked=\"checked\"";} ?>>remove the right side cover</label></div>
        <div><label><input id="question48" name="question48" value="2"  type="radio"<?php if($q48== 2){print " checked=\"checked\"";} ?>>remove the rear cover</label></div>
        <div><label><input id="question48" name="question48" value="3"  type="radio"<?php if($q48== 3){print " checked=\"checked\"";} ?>>remove the front cover</label></div>
        <div><label><input id="question48" name="question48" value="4"  type="radio"<?php if($q48== 4){print " checked=\"checked\"";} ?>>remove the feed table</label></div>
        <div><label><input id="question48" name="question48" value="5"  type="radio"<?php if($q48== 5){print " checked=\"checked\"";} ?>>open the exit cover and lower the roller bracket</label></div>
	</div>
    <br />
   <div class="verdana2bold">49) The timing belt tension is adjustable by?</div><br />
    <div class="formRow2">        
        <div><label><input id="question49" name="question49" value="1"  type="radio"<?php if($q49== 1){print " checked=\"checked\"";} ?>>adjusting the motor position</label></div>
        <div><label><input id="question49" name="question49" value="2"  type="radio"<?php if($q49== 2){print " checked=\"checked\"";} ?>>the top pulley position adjustment</label></div>
        <div><label><input id="question49" name="question49" value="3"  type="radio"<?php if($q49== 3){print " checked=\"checked\"";} ?>>a spring loaded tensioner</label></div>
        <div><label><input id="question49" name="question49" value="4"  type="radio"<?php if($q49== 4){print " checked=\"checked\"";} ?>>adjusting the timing belt tensioner slider</label></div>
        <div><label><input id="question49" name="question49" value="5"  type="radio"<?php if($q49== 5){print " checked=\"checked\"";} ?>>a tensioner pulley</label></div>
	</div>
    <br />
	<div class="verdana2bold">50) The paper skew adjustment handle adjusts?</div><br />
    <div class="formRow2">        
        <div><label><input id="question50" name="question50" value="1"  type="radio"<?php if($q50== 1){print " checked=\"checked\"";} ?>>the pick roller angle</label></div>
        <div><label><input id="question50" name="question50" value="2"  type="radio"<?php if($q50== 2){print " checked=\"checked\"";} ?>>the paper side guide angle</label></div>
        <div><label><input id="question50" name="question50" value="3"  type="radio"<?php if($q50== 3){print " checked=\"checked\"";} ?>>the fold plate skew</label></div>
        <div><label><input id="question50" name="question50" value="4"  type="radio"<?php if($q50== 4){print " checked=\"checked\"";} ?>>the backstop skew</label></div>
        <div><label><input id="question50" name="question50" value="5"  type="radio"<?php if($q50== 5){print " checked=\"checked\"";} ?>>the feed table angle</label></div>
	</div></div>
</div>
  <div class="formBoxFooter">
	<div><input id="doSubmit" name="doSubmit" value="Submit Test" type="submit" /></div>
 </div>
 </form>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>

<?php

} else {
	
	$q1 =  $_POST['question1'];
	$q2 =  $_POST['question2'];
	$q3 =  $_POST['question3'];
	$q4 =  $_POST['question4'];
	$q5 =  $_POST['question5'];
	$q6 =  $_POST['question6'];
	$q7 =  $_POST['question7'];
	$q8 =  $_POST['question8'];
	$q9 =  $_POST['question9'];
	$q10 =  $_POST['question10'];

	$q11 =  $_POST['question11'];
	$q12 =  $_POST['question12'];
	$q13 =  $_POST['question13'];
	$q14 =  $_POST['question14'];
	$q15 =  $_POST['question15'];
	$q16 =  $_POST['question16'];
	$q17 =  $_POST['question17'];
	$q18 =  $_POST['question18'];
	$q19 =  $_POST['question19'];
	$q20 =  $_POST['question20'];

	$q21 =  $_POST['question21'];
	$q22 =  $_POST['question22'];
	$q23 =  $_POST['question23'];
	$q24 =  $_POST['question24'];
	$q25 =  $_POST['question25'];
	$q26 =  $_POST['question26'];
	$q27 =  $_POST['question27'];
	$q28 =  $_POST['question28'];
	$q29 =  $_POST['question29'];
	$q30 =  $_POST['question30'];

	$q31 =  $_POST['question31'];
	$q32 =  $_POST['question32'];
	$q33 =  $_POST['question33'];
	$q34 =  $_POST['question34'];
	$q35 =  $_POST['question35'];
	$q36 =  $_POST['question36'];
	$q37 =  $_POST['question37'];
	$q38 =  $_POST['question38'];
	$q39 =  $_POST['question39'];
	$q40 =  $_POST['question40'];

	$q41 =  $_POST['question41'];
	$q42 =  $_POST['question42'];
	$q43 =  $_POST['question43'];
	$q44 =  $_POST['question44'];
	$q45 =  $_POST['question45'];
	$q46 =  $_POST['question46'];
	$q47 =  $_POST['question47'];
	$q48 =  $_POST['question48'];
	$q49 =  $_POST['question49'];
	$q50 =  $_POST['question50'];
	

// echo "Question 1: " . $q1."<br>";
// echo "Question 2: " . $q2."<br>";
// echo "Question 3: " . $q3."<br>";
// echo "Question 4: " . $q4."<br>";
// echo "Question 5: " . $q5."<br>";
// echo "Question 6: " . $q6."<br>";
// echo "Question 7: " . $q7."<br>";
// echo "Question 8: " . $q8."<br>";
// echo "Question 9: " . $q9."<br>";
// echo "Question 10: " . $q10."<br>";

	$a1 = 1;
	$a2 = 3;
	$a3 = 2;
	$a4 = 4;
	$a5 = 5;
	$a6 = 2;
	$a7 = 5;
	$a8 = 2;
	$a9 = 3;
	$a10 = 1;
	
	$a11 = 3;
	$a12 = 3;
	$a13 = 5;
	$a14 = 4;
	$a15 = 1;
	$a16 = 3;
	$a17 = 2;
	$a18 = 5;
	$a19 = 2;
	$a20 = 4;
	
	$a21 = 3;
	$a22 = 4;
	$a23 = 2;
	$a24 = 3;
	$a25 = 4;
	$a26 = 5;
	$a27 = 4;
	$a28 = 3;
	$a29 = 5;
	$a30 = 4;
	
	$a31 = 4;
	$a32 = 4;
	$a33 = 4;
	$a34 = 3;
	$a35 = 4;
	$a36 = 4;
	$a37 = 1;
	$a38 = 5;
	$a39 = 3;
	$a40 = 4;
	
	$a41 = 3;
	$a42 = 5;
	$a43 = 3;
	$a44 = 4;
	$a45 = 4;
	$a46 = 3;
	$a47 = 5;
	$a48 = 5;
	$a49 = 1;
	$a50 = 2;


$points = 0;
$section1 = 0;
$section2 = 0;
$section3 = 0;
$section4 = 0;
$section5 = 0;

$points = ($q1 == $a1) ? ++$points : $points;
//echo "<p>Points: " . $points . "</p>";
$points = ($q2 == $a2) ? ++$points : $points;
//echo "<p>Points: " . $points . "</p>";
$points = ($q3 == $a3) ? ++$points : $points;
//echo "<p>Points: " . $points . "</p>";
$points = ($q4 == $a4) ? ++$points : $points;
//echo "<p>Points: " . $points . "</p>";
$points = ($q5 == $a5) ? ++$points : $points;
//echo "<p>Points: " . $points . "</p>";
$points = ($q6 == $a6) ? ++$points : $points;
//echo "<p>Points: " . $points . "</p>";
$points = ($q7 == $a7) ? ++$points : $points;
//echo "<p>Points: " . $points . "</p>";
$points = ($q8 == $a8) ? ++$points : $points;
//echo "<p>Q8: " . $q8 . " A8: " . $a8 . "</p>";
//echo "<p>Points: " . $points . "</p>";
$points = ($q9 == $a9) ? ++$points : $points;
//echo "<p>Points: " . $points . "</p>";
$points = ($q10 == $a10) ? ++$points : $points;
//echo "<p>Points: " . $points . "</p>";
$section1 = $points;
//echo "<p>Section Points: " . $section1 . "</p>";

$points = ($q11 == $a11) ? ++$points : $points;
$points = ($q12 == $a12) ? ++$points : $points;
$points = ($q13 == $a13) ? ++$points : $points;
$points = ($q14 == $a14) ? ++$points : $points;
$points = ($q15 == $a15) ? ++$points : $points;
$points = ($q16 == $a16) ? ++$points : $points;
$points = ($q17 == $a17) ? ++$points : $points;
$points = ($q18 == $a18) ? ++$points : $points;
$points = ($q19 == $a19) ? ++$points : $points;
$points = ($q20 == $a20) ? ++$points : $points;
$section2 = $points - $section1;

$points = ($q21 == $a21) ? ++$points : $points;
$points = ($q22 == $a22) ? ++$points : $points;
$points = ($q23 == $a23) ? ++$points : $points;
$points = ($q24 == $a24) ? ++$points : $points;
$points = ($q25 == $a25) ? ++$points : $points;
$points = ($q26 == $a26) ? ++$points : $points;
$points = ($q27 == $a27) ? ++$points : $points;
$points = ($q28 == $a28) ? ++$points : $points;
$points = ($q29 == $a29) ? ++$points : $points;
$points = ($q30 == $a30) ? ++$points : $points;
$section3 = $points - $section1 - $section2;

$points = ($q31 == $a31) ? ++$points : $points;
$points = ($q32 == $a32) ? ++$points : $points;
$points = ($q33 == $a33) ? ++$points : $points;
$points = ($q34 == $a34) ? ++$points : $points;
$points = ($q35 == $a35) ? ++$points : $points;
$points = ($q36 == $a36) ? ++$points : $points;
$points = ($q37 == $a37) ? ++$points : $points;
$points = ($q38 == $a38) ? ++$points : $points;
$points = ($q39 == $a39) ? ++$points : $points;
$points = ($q40 == $a40) ? ++$points : $points;
$section4 = $points - $section1 - $section2 - $section3;

$points = ($q41 == $a41) ? ++$points : $points;
$points = ($q42 == $a42) ? ++$points : $points;
$points = ($q43 == $a43) ? ++$points : $points;
$points = ($q44 == $a44) ? ++$points : $points;
$points = ($q45 == $a45) ? ++$points : $points;
$points = ($q46 == $a46) ? ++$points : $points;
$points = ($q47 == $a47) ? ++$points : $points;
$points = ($q48 == $a48) ? ++$points : $points;
$points = ($q49 == $a49) ? ++$points : $points;
$points = ($q50 == $a50) ? ++$points : $points;
$section5 = $points - $section1 - $section2 - $section3 - $section4;

$score = round(($points / 50) *100);

$TechID = $_SESSION['TechID'];
$DateTime = date("Y-m-d H:i:s");

$testID = mysqlFetchAssoc(mysqlQuery("SELECT id FROM ServRight_ElectroMechTest WHERE tech_id = '".$TechID."'"));
if ($testID) {
	$testID = $testID[0];
	$emTestID = $testID[id];

	mysqlQuery("UPDATE ServRight_ElectroMechTest SET a1 = '$q1', a2 = '$q2', a3 = '$q3', a4 = '$q4', a5 = '$q5', a6 = '$q6', a7 = '$q7', a8 = '$q8', a9 = '$q9', a10 = '$q10',
			   a11 = '$q11', a12 = '$q12', a13 = '$q13', a14 = '$q14', a15 = '$q15', a16 = '$q16', a17 = '$q17', a18 = '$q18', a19 = '$q19', a20 = '$q20',
				a21 = '$q21', a22 = '$q22', a23 = '$q23', a24 = '$q24', a25 = '$q25', a26 = '$q26', a27 = '$q27', a28 = '$q28', a29 = '$q29', a30 = '$q30',
				a31 = '$q31', a32 = '$q32', a33 = '$q33', a34 = '$q34', a35 = '$q35', a36 = '$q36', a37 = '$q37', a38 = '$q38', a39 = '$q39',a40 = '$q40',
				a41 = '$q41', a42 = '$q42', a43 = '$q43', a44 = '$q44', a45 = '$q45', a46 = '$q46', a47 = '$q47', a48 = '$q48', a49 = '$q49', a50 = '$q50', score = $score, Section1Score = $section1, Section2Score = $section2, Section3Score = $section3, Section4Score = $section4, Section5Score = $section5 WHERE id = " . $emTestID);
			   
} else {
	$DateTime = date("Y-m-d H:i:s");
	mysqlQuery("INSERT INTO ServRight_ElectroMechTest (tech_id, date_taken, score, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20,
												   a21, a22, a23, a24, a25, a26, a27, a28, a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41, a42, a43, a44, a45, a46, a47, a48, a49, a50, Section1Score, Section2Score, Section3Score, Section4Score, Section5Score)
		   VALUES ('$TechID', '$DateTime', '$score', '$q1', '$q2', '$q3', '$q4', '$q5', '$q6', '$q7', '$q8', '$q9', '$q10', '$q11', '$q12', '$q13', '$q14', '$q15', '$q16', '$q17', '$q18', '$q19', '$q20'
				   , '$q21', '$q22', '$q23', '$q24', '$q25', '$q26', '$q27', '$q28', '$q29', '$q30', '$q31', '$q32', '$q33', '$q34', '$q35', '$q36', '$q37', '$q38', '$q39', '$q40', '$q41', '$q42', '$q43', '$q44', '$q45', '$q46', '$q47', '$q48', '$q49', '$q50', '$section1', '$section2', '$section3', '$section4', '$section5')");
}
		


echo "<div class=\"formBox\">";

if ($score >= 80) {
	mysqlQuery("UPDATE TechBankInfo SET ServRight_ElectroMech_Cert = '1' WHERE TechID = '$TechID'");
	Core_Database_MysqlSync::sync(Core_Database::TABLE_TECH_BANK_INFO, "TechID", $TechID);
	echo "<div class=\"verdana2bold\" style=\"color:green\">Contratulations, you passed!</div><div class=\"formRow1\"></div><br />";
	echo "<div class=\"verdana2bold\">SCORE: $score %</div>";
} else {
	echo "<div class=\"verdana2bold\" style=\"color:#F00\">Sorry, you do not have a passing score</div><br />";
	echo "<div class=\"formRow2\">You will need to re-take the test. You will not be activated for the ServRight Electro Mechanical program until you have scored 100%.</div>";
	echo "<div class=\"formRow2\">Please Click <a href=\"electroMechTest.php\">here</a> to re-take the test.</div><br />";
	echo "<div class=\"verdana2bold\">SCORE: $score %</div>";
	echo "</div></div></div></div>";
}
echo "</div></div></div></div>";


//	echo "<br>Total Points: " . $points . "<br>";
//	echo "Score: " . $score . "%<br>";
} ?>

<p>
  <?php require ("../footer.php"); ?>
</p>