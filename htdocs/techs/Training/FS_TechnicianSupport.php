<?php $page = "techs"; ?>
<?php $option = "training"; ?>
<?php $selected = "fs" ?>
<?php $display = "fs" ?>
<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>

<!-- Add Content Here -->

<br /><br />

<div align="center">

<TABLE cellSpacing=0 cellPadding=0 width=700 border=0 style="text-align:left">
<TBODY>
<TR>
<TD colSpan=2><BR><FONT face=Arial size=4><B>Field Solutions Technician Support</B></FONT> 
<P>
<HR>
<P>
<FONT face=Arial size=2>
If you have any questions regarding how to:
<P>
- Navigate the Field Solutions web site
<BR>
- Find/search for open work orders
<BR>
- Find/search for work orders belonging to you (applied for, assigned, completed, approved. paid)
<BR>

- Get assistance regarding payment/contract issues
<BR>
......or any other questions or technician focused issues, be sure you have gone through all How-To sections. If you still have questions, please contact Field Solutions Technician Support via the link below. 
<P>
<A href="http://www.fieldsolutions.com/content/contactUs.php"target=_blank"><FONT face=Arial color=#0062c4 size=2>Field Solutions Technician Support</FONT></A>

<P class="nextButton" align="center"><a href="FS_FLS_Program.php">Previous</a>


</TD></TR></TBODY></TABLE>







</div>

<!--- End Content --->
<?php require ("../../footer.php"); ?>
		
