<?php $page = "techs"; ?><!-- Main section in navbar -->
<?php $option = "training"; ?><!-- Sub-section in navbar -->
<?php $selected = "FSTraining"; ?>
<?php require ("../../../header.php"); ?><!-- ../ only if in sub-dir -->
<?php require ("../../../navBar.php"); ?><!-- ../ only if in sub-dir -->
<!-- Add Content Here -->
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193B0000E9C6D4G0I3E9C6D4G0I3","https:");}catch(ilolli){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193B0000E9C6D4G0I3E9C6D4G0I3">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>
<!--<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b0000e8b4c0a9h0c2c2f4g8a0","https:");}catch(v_e){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000e8b4c0a9h0c2c2f4g8a0">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>-->

<!-- Check if logged in, if not redirect -->
<script type="text/javascript">
	try {
		var test = v;
		var techZip = z;
		if (v != "<?=$_GET["v"]?>")
			window.location.replace("<?=$_SERVER['PHP_SELF']?>?v=" + v);
	}
	catch (e) {
		window.location.replace("/techs/");
	}
</script>

<br /><br />

<div align="center">

<table border="0" width="720">
	<tr>
	  <td><h2>Field Solutions Video Online Tutorial</h2>
	    <p>Watch the Online Tutorial as a first step toward Field Solutions Certification.  Why become Field Solutions Certified?  Our clients want to know that our Technicians are properly prepared to run work orders for them.</p>
	    <p>Certified Technicians will get a special notation in their profile indicating &quot;Field Solutions Certified&quot; next to their name.  You'll be preferred by clients if you're certified.  So watch the tutorial and complete the Field Solutions Certification, and you'll be ready to Get To Work!!!</p>
	    <p>Watch all the tutorial segments in succession and then complete the <a href="FS_Certification.php">Field Solutions Certification review</a>.  If you need a refresher on a single topic, just play that segment.  Total playing time of the tutorials is less than 30 minutes.</p>
	    <p>The tutorials are in Flash format, so you'll need to have the Flash plugin installed on your system in order to view them.</p>
	    <p>If you have trouble with the tutorials, please contact our support team at <a href="mailto:support@fieldsolutions.com">support@fieldsolutions.com</a></p>
	    <p>&nbsp;</p></td>
	  </tr>
	<tr>
	  <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="60%" valign="top" bgcolor="#CCCCCC">&nbsp;</td>
        </tr>
        
        <tr>
          <td valign="top">&nbsp;</td>
        </tr>
        <tr>
          <td valign="top"><a href="Videos/intro/ppI.htm" target="_blank">Introduction</a></td>
        </tr>
        <tr>
          <td valign="top"><a href="Videos/p1/pp1.htm" target="_blank">Part 1 - Logging In &amp; Home Page</a></td>
        </tr>
        <tr>
          <td valign="top"><a href="Videos/p2/pp2.htm" target="_blank">Part 2 - Technician Information</a></td>
        </tr>
        <tr>
          <td valign="top"><a href="Videos/p3/pp3.htm" target="_blank">Part 3 - Field Solutions Policies, Processes &amp; Help<br />
          </a><span style="margin-left:5px">How To's, Field Solutions Certification Test, W-9 &amp; Getting Paid            </td>
        </tr>
        <tr>
          <td valign="top"><a href="Videos/p4/pp4.htm" target="_blank">Part 4 - Client Requirements</a></td>
        </tr>
        <tr>
          <td valign="top"><a href="Videos/p5/pp5.htm" target="_blank">Part 5 - Work Orders I<br />
          </a><span style="margin-left:5px">Finding &amp; Bidding on Work Orders</span></td>
        </tr>
        <tr>
          <td valign="top"><a href="Videos/p6/pp6.htm" target="_blank">Part 6 - Work Orders II<br />
          </a><span style="margin-left:5px">Running &amp; Closing out a Work Order, Technician Dashboard</span></td>
        </tr>
        <tr>
          <td valign="top"><a href="Videos/p7/pp7.htm" target="_blank">Part 7 - Technician Forum</a></td>
        </tr>
      </table></td>
	  </tr>
	<tr>
<td><br />
<A href="http://www.fieldsolutions.com/techs/Training/FieldSolutions/Videos/FieldSolutionsTraining.wvx"></A></td>
	</tr>
	<tr>
	  <td bgcolor="#CCCCCC"><div align="center">Remember, after watching the tutorial, take the <a href="FS_Certification.php">Field Solutions Certification review</a></div></td>
	  </tr>
	<tr>
	  <td>&nbsp;</td>
	  </tr>
</table>

</div>
	


<!--- End Content --->
<?php require ("../../../footer.php"); ?><!-- ../ only if in sub-dir -->
		
