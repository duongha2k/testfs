<?php $page = login; ?><!-- Main section in navbar -->
<?php $option = profile; ?><!-- Sub-section in navbar -->
<?php require ("../../../header.php"); ?><!-- ../ only if in sub-dir -->
<?php require ("../../../navBar.php"); ?><!-- ../ only if in sub-dir -->
<!-- Add Content Here -->

<?php
require_once("../../../library/smtpMail.php");
require_once("../../../library/caspioAPI.php");

function Update_And_Mail($TechID, $Score)
{
	$wsdl = "https://bridge.caspio.net/ws/api.asmx?wsdl";
	$tableName = "TR_Master_List";
	$name = "mgkassoc";
	$profile = "EmailBlaster";
	$password = "DevTeam2007$";
	# ------  Add code below here  ------
	try {	
			
		$client = new SoapClient($wsdl);
		$criteria = "TechID = '$TechID' ";		
		$fieldList = "FirstName, LastName, PrimaryEmail, Address1, Address2, City, State, ZipCode, PrimaryPhone, SecondaryPhone, FLSRegion";
		$records = $client->SelectDataRaw($name, $profile, $password, $tableName, false, $fieldList, $criteria, "", "", "");
		//print_r($records);
		//echo "<br>";
		//echo sizeof($records);
		
		if(sizeof($records) == 1) {
			//echo sizeof($records);
			//echo "<br>";
			$arr = explode(",", $records[0]);
			//echo sizeof($arr);
			//echo "<br>";
			//print_r($arr);
			//echo "<br>";
			if(sizeof($arr) >= 11) {
			
				$Name = $arr[0]." ".$arr[1];
				$Name = str_replace("'", "", $Name);
				
				$Email = $arr[2];
				$Email = str_replace("'", "", $Email);
				
				$Address1 = $arr[3];
				$Address1 = str_replace("'", "", $Address1);
	
				$Address2 = $arr[4];
				$Address2 = str_replace("'", "", $Address2);
				
				$City = $arr[5];
				$City = str_replace("'", "", $City);

				$State = $arr[6];
				$State = str_replace("'", "", $State);

				$Zip = $arr[7];
				$Zip = str_replace("'", "", $Zip);
				
				$Phone1 = $arr[8];
				$Phone1 = str_replace("'", "", $Phone1);

				$Phone2 = $arr[9];
				$Phone2 = str_replace("'", "", $Phone2);
				
				$Region = $arr[10];
				$Region = str_replace("'", "", $Region);
				
			}
			if(sizeof($arr) == 12){
				$Region .= ", " . $arr[11];
				$Region = str_replace("'", "", $Region);
				//echo "Worked!";
			}
			
			$fromName = 'FLSupport';
			$fromEmail = 'support@fieldsolutions.com';
			$subject = 'PASSED TEST [' . $TechID . '] - IDs/Badge/Waybills Needed';
			$html_message = 'The following tech has passed the FLS training test<br>and needs IDs created plus a Badge and Waybills<br>shipped.<br><br>';
			$html_message .= 'Name: ' . $Name . '<br>';
			$html_message .= 'Address: ' . $Address1 . '<br>';
			if($Address2 != ""){
				$html_message .= '          ' . $Address2 . '<br>';			
			}
			
			$html_message .= 'City: ' . $City . '<br>';
			$html_message .= 'State: ' . $State . '<br>';
			$html_message .= 'Zip: ' . $Zip . '<br>';
			$html_message .= 'Telephone 1: ' . $Phone1 . '<br>';
			$html_message .= 'Telephone 2: ' . $Phone2 . '<br>';
			$html_message .= 'FLS Region: ' . $Region . '<br>';

			$message = str_replace('<br/>', chr(13) . chr(10), $html_message); // Plain text version of message
			$message = str_replace('<br>', chr(13) . chr(10), $message); // Plain text version of message
/*			$toGroups = "";
			$toGroupsFilter = "";


			$webDB = "";*/
			//$emailList = "gerald.bailey@technicianbureau.net";
			$emailList = "support@fieldsolutions.com, Wbarrios@ftxs.fujitsu.com, skimberlin@fieldsolutions.com";
//			$emailList = "codem01@gmail.com";
			smtpMailLogReceived($fromName, $fromEmail, $emailList, $subject, $message, $html_message, "t-test-submitted.php");


			// Email Notice to Techs
			$fromName = "FLS Training";
			$fromEmail = "support@fieldsolutions.com";
			$emailList = $Email;
			$subject = "Welcome to FLS";
			
			$message = "Dear TB Technician,\n\n
			
Welcome to FLS!\n\n
 
Congratulations on passing the FLS online training test. You have been
activated as a Tech for the FLS program and associated client.\n\n
 
There are a couple of important things for you to do while you wait for your
FLS ID numbers to be created as well as your Badge & Waybills to be
shipped to you via FedEx.\n\n
 
1) It is important for you access the UNEDITED documentation in the
\"Unedited Docs\" section, print those documents and have them with you
on all FLS calls that you run. You will need to make multiple copies of
some of the documents such as the Checklist, CSR form and PRF as
you will need one for each call/install. \n\n
 
\"Unedited Docs\" section: http://www.flsupport.com/26.html\n\n
 
2) If you haven't already done so, please be sure to Submit your W-9
form per instructions here: https://www.fieldsolutions.com/techs/w9.php.
If you are a Canadian Tech, please disregard this step as it  is not
necessary for you to submit a W-9 or any other form.\n\n
 
Two FLS ID numbers will be issued in your name and be accessible via
the FLS portion of your TB account/profile per instructions here:
http://flsupport.com/15.html. Please note that it generally takes 4-6
business days from the time you receive this email, for your FLS ID
numbers to show up in your account.\n\n
 
A Badge & Waybills (pre-paid shipping labels) will also be shipped to
you via FedEx to the address indicated in your profile. If that address is
a PO Box, we CANNOT ship as FedEx will NOT deliver to a PO Box.
Therefore, be sure that you have a physical street address listed in your
TB account and NOT a PO Box for your primary address. Note: Your
Badge & Waybills will likely show up several days before your FLS ID
numbers become available in your TB account as that is how things
are processed. You cannot run calls until your have your FLS ID
numbers.\n\n
 
If you have any questions, we ask that you first review the FAQ section
here: http://flsupport.com/24.html. If you cannot find the answer to your
question(s) in the FAQ section please contact the appropriate support
staff at the following link: http://flsupport.com/21.html\n\n
 
Thank you.\n\n
 
Regards,\n\n
 
FLS/TB Support\n
Field Solutions\n
FLS Training: www.flsupport.com\n
Email: support@fieldsolutions.com\n
TB Tech Login:\n
https://www.fieldsolutions.com/techs/index.php\n";

			$html_message = "Dear TB Technician,<br/><br/>
			
Welcome to FLS!<br/><br/>
 
Congratulations on passing the FLS online training test. You have been
activated as a Tech for the FLS program and associated client.<br/><br/>
 
There are a couple of important things for you to do while you wait for your
FLS ID numbers to be created as well as your Badge & Waybills to be
shipped to you via FedEx.<br/><br/>
 
1) It is important for you access the UNEDITED documentation in the
\"Unedited Docs\" section, print those documents and have them with you
on all FLS calls that you run. You will need to make multiple copies of
some of the documents such as the Checklist, CSR form and PRF as
you will need one for each call/install. <br/><br/>
 
\"Unedited Docs\" section: <a href=\"http://www.flsupport.com/26.html\">http://www.flsupport.com/26.html</a><br/><br/>
 
2) If you haven't already done so, please be sure to Submit your W-9
form per instructions here: <a href=\"https://www.fieldsolutions.com/techs/w9.php\">https://www.fieldsolutions.com/techs/w9.php</a>.
If you are a Canadian Tech, please disregard this step as it  is not
necessary for you to submit a W-9 or any other form.<br/><br/>
 
Two FLS ID numbers will be issued in your name and be accessible via
the FLS portion of your TB account/profile per instructions here:
<a href=\"http://flsupport.com/15.html\">http://flsupport.com/15.html</a>. Please note that it generally takes 4-6
business days from the time you receive this email, for your FLS ID
numbers to show up in your account.<br/><br/>
 
A Badge & Waybills (pre-paid shipping labels) will also be shipped to
you via FedEx to the address indicated in your profile. If that address is
a PO Box, we CANNOT ship as FedEx will NOT deliver to a PO Box.
Therefore, be sure that you have a physical street address listed in your
TB account and NOT a PO Box for your primary address. Note: Your
Badge & Waybills will likely show up several days before your FLS ID
numbers become available in your TB account as that is how things
are processed. You cannot run calls until your have your FLS ID
numbers.<br/><br/>
 
If you have any questions, we ask that you first review the FAQ section
here: <a href=\"http://flsupport.com/24.html\">http://flsupport.com/24.html</a>. If you cannot find the answer to your
question(s) in the FAQ section please contact the appropriate support
staff at the following link: <a href=\"http://flsupport.com/21.html\">http://flsupport.com/21.html</a><br/><br/>
 
Thank you.<br/><br/>
 
Regards,<br/><br/>
 
FLS/TB Support<br/>
Field Solutions<br/>
FLS Training: <a href=\"www.flsupport.com\">www.flsupport.com</a><br/>
Email: support@fieldsolutions.com<br/>
TB Tech Login:<br/>
<a href=\"https://www.fieldsolutions.com/techs/index.php\">https://www.fieldsolutions.com/techs/index.php</a><br/>";
			
			smtpMailLogReceived($fromName, $fromEmail, $emailList, $subject, $message, $html_message, "t-test-submitted.php");
		}
	} catch (SoapFault $fault) {
			//SOAP fault handling
			$str = "<h1><b>Fault:</b> faultcode: {$fault->faultcode}, <b>faultstring:</b> {$fault->faultstring}</h1>";
	}

}


$TechID=$_GET["TechID"];
$DateTaken = $_GET["DateTaken"];
		
$ExpirationDate = date('09/09/2099');
	
caspioUpdate("TR_Master_List", "FS_Cert_Test_Pass, FS_Cert_Expiration_Date, FS_Cert_Date_Taken", "'1', '$ExpirationDate', '$DateTaken'", "TechID = '$TechID'");

	//Update_And_Mail($TechID, $Score);
	
print("<h3>Congratulations, you are now Field Solutions Certified!<br>Click <a href=\"https://www.fieldsolutions.com/techs/dashboard.php\">here</a> to return to the home page.</h3>");		
		
?>

<!--- End Content --->
<?php require ("../../../footer.php"); ?><!-- ../ only if in sub-dir -->
