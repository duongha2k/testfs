<?php $page = "techs"; ?>
<?php $option = "training"; ?>
<?php $selected = "fs" ?>
<?php $display = "fs" ?>
<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>

<!-- Add Content Here -->

<br /><br />

<div align="center">

<TABLE cellSpacing=0 cellPadding=0 width=700 border=0 style="text-align:left">
<TBODY>
<TR>
<TD colSpan=2>
<BR><FONT face=Arial size=4><B>AVAILABLE WORK</B></FONT> 
<BR>
<FONT face=Arial size=3>How to apply for/bid on open work orders</FONT> 
<P>
<FONT face=Arial size=2>
<B>Note:</B> FLS is a major client of Field Solutions and handles things a bit differently and separately from all other clients and the main site. Therefore, please pay attention to the differences in the work order instructions for "FLS" versus "FIELDSOLUTIONS" (main site) work order instructions. 
<P>
<HR>
<P>

<FONT face=Arial size=2>
<P>
<B>How to apply for open FLS work orders</B>
<P>
Once logged into your Field Solutions account, click on the "Work Orders" link in the blue menu bar at the top. When first arriving in the work order section, note the four links showing at the top just under the blue menu bar - "ASSIGNED WORK", "AVAILABLE WORK", "MY APPLIED WOS" and "FLSWOS". The work order section defaults to show main (Non FLS) work orders, meaning it will show you all work orders under all clients but FLS upon first coming into the work order section. To search/apply for FLS open work orders, you need to click on the "FLSWOS" link. Note: You can toggle back/forth between main work orders (all clients but FLS) and FSL work orders.   
<P>
After clicking on "FLSWOS", click on the "AVAILABLE WORK" link where you can search all FLS work orders available to apply for or by week/month via the drop down menu (top, left). You can also search for work orders within a specific date range directly below the dropdown menu.  
<P>
<B>How to bid on/apply for main Field Solutions (non-FLS) open work orders</B>
<P>
Once logged into your Field Solutions account, click on the "Work Orders" link in the blue menu bar at the top. When first arriving in the work order section, note the four links showing at the top just under the blue menu bar - "ASSIGNED WORK", "AVAILABLE WORK", "MY APPLIED WOS" and "FLSWOS". The work order section defaults to show main (non FLS) work orders. Meaning, you search for non-FLS work orders to bid on/apply for upon first entering the work order section.    
<P>
Next, click the "AVAILABLE WORK" link where you can search all work orders available to bid on/apply for or by week/month via the drop down menu (top, left). You can also search for work orders by mile radius and/or a specific date range directly below the dropdown menu. 
<P>
To conduct an advanced search, select �AVAILABLE WORK�. Once you are routed to the search screen, select the pull down menu for a �Distance� search. Select the option �advanced search�. This option will open up a new screen with several search options. The first option is search by ID, if you have the ID number of the work order you are interested in, enter it here.
<P>
Please note, when searching for a work order, if it is has been deactivated or assigned to another technician your search for an ID will say �no records found�. 
 
<P>
<B>Special note regarding work orders you have applied for</B>

<P>
It is common for work orders to be entered by clients a few days/weeks in advance of the work, which means it may take a while before clients/staff get to your work order application/bid, so please be patient. Keep in mind that there may be many Techs applying for/bidding on the same work orders. Once a work order is awarded to a tech, it will be removed from the "AVAILABLE WORK" work order listing and from your queue under  "MY APPLIED WOS" if you have applied for it. 
<P>
<HR>
<P>
<B>What the following tabs are for in each work order section - 
<BR>
"Assigned" - "Work Done" - " Approved" -  "In Accounting" - "Paid" - "Incomplete" - "All" </B>
<P>
These tabs in each work order section are for work order processing. You should keep track of each step to make sure that you are on target to get paid for work performed. 
<P>
A brief explanation of each tab - 
<P>
<B>Assigned</B> - A listing of all your work orders officially assigned to you by clients/staff.

<P>
<B>Work Done</B> - A listing of all work orders covering work you have performed that the client has entered into the system, which needs to be completed by you online in order to be approved and paid.  
<P>
<B>Approved</B> - A listing of all your work orders that have been approved by the client(s). Note: Clients approve work orders, not Field Solutions. You cannot be paid for work orders until they have been approved by the client.
<P>
<B>In Accounting</B> - A listing of all your work orders that have been approved by the client(s) and are in the queue to be paid by Field Solutions. 
<P>
<B>Paid</B> - A listing of all your work orders that have been paid to include the date a check was cut and mailed or the direct deposit process began.   
<P>

<B>Incomplete</B> - A listing of all/any of your work orders that you have not (properly) completed to include updating your work order(s) online per instructions.
<P>
<B>All</B> - A listing of all your work orders
<P class="nextButton" align="center"><a href="FS_Works.php">Previous</a>
&nbsp; | &nbsp;<a href="FS_AssignedWork.php">Next</a>
</TD></TR></TBODY></TABLE>







</div>

<!--- End Content --->
<?php require ("../../footer.php"); ?>
		
