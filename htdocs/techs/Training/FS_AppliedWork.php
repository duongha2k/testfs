<?php $page = "techs"; ?>
<?php $option = "training"; ?>
<?php $selected = "fs" ?>
<?php $display = "fs" ?>
<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>

<!-- Add Content Here -->

<br /><br />

<div align="center">

<TABLE cellSpacing=0 cellPadding=0 width=700 border=0 style="text-align:left">
<TBODY>
<TR>
<TD colSpan=2>
<BR><FONT face=Arial size=4><B>MY APPLIED WOS</B></FONT> 
<BR>
<FONT face=Arial size=3>How to view/search/modify work orders you have applied for/bid on</FONT> 
<P>
<FONT face=Arial size=2>
<B>Note:</B> FLS is a major client of Field Solutions and handles things a bit differently and separately from all other clients and the main site. Therefore, please pay attention to the differences in the work order instructions for "FLS" versus "FIELDSOLUTIONS" (main site) work order instructions. 
<P>
<HR>
<P>

<B>How to view/search FLS work orders you have applied for </B>
<P>
Once logged into your Field Solutions account, click on the "Work Orders" link in the blue menu bar at the top. When first arriving in the work order section, note the four links showing at the top just under the blue menu bar - "ASSIGNED WORK", "AVAILABLE WORK", "MY APPLIED WOS" and "FLSWOS". The work order section defaults to show main (Non FLS) work orders, meaning it will show you all work orders under all clients but FLS upon first coming into the work order section. To view/search for FLS work orders that you have applied for, click on the "FLSWOS" link. Note: You can toggle back/forth between main work orders (all clients but FLS) and FSL work orders.   
<P>
After clicking on "FLSWOS", click on the "MY APPLIED WOS" link where you can search FLS work orders that you have applied for. <B>Note:</B> You cannot edit your application for FLS work orders. 
<P>
<B>How to view/search/modify main Field Solutions (non-FLS) work orders you have applied for </B>
<P>
Once logged into your Field Solutions account, click on the "Work Orders" link in the blue menu bar at the top. When first arriving in the work order section, note the four links showing at the top just under the blue menu bar - "ASSIGNED WORK", "AVAILABLE WORK", "MY APPLIED WOS" and "FLSWOS". The work order section defaults to show main (non FLS) work orders. Meaning, you search for non-FLS work orders that you have bid on/applied for upon first entering the work order section.    
<P>
Next, click on the "MY APPLIED WOS" link where you can search and/or modify work orders that you have applied for by "Bid Date", "Start Date" or by just clicking the "Search" button without entering any search parameters to view the entire list of work orders that you have applied for.
<P>

<B>Special note regarding work orders you have applied for</B>
<P>
It is common for work orders to be entered by clients a few days/weeks out, which means it may take a while before clients/staff get to your work order application/bid, so please be patient. Keep in mind that there may be many Techs applying for/bidding on the same work orders. 
<P>
If/when a work order that you applied for/bid on is awarded to another tech, it will be removed from your queue under  "MY APPLIED WOS". That way you are aware when a work order you applied for, is assigned to another tech.  
<P>
<HR>
<P>
<B>What the following tabs are for in each work order section - 
<BR>
"Assigned" - "Work Done" - " Approved" -  "In Accounting" - "Paid" - "Incomplete" - "All" </B>
<P>
These tabs in each work order section are for work order processing. You should keep track of each step to make sure that you are on target to get paid for work performed. 
<P>
A brief explanation of each tab - 

<P>
<B>Assigned</B> - A listing of all your work orders officially assigned to you by clients/staff. 
<P>
<B>Work Done</B> - A listing of all work orders covering work you have performed that the client has entered into the system, which needs to be completed by you online in order to be approved and paid.  
<P>
<B>Approved</B> - A listing of all your work orders that have been approved by the client(s). Note: Clients approve work orders, not Field Solutions. You cannot be paid for work orders until they have been approved by the client.
<P>
<B>In Accounting</B> - A listing of all your work orders that have been approved by the client(s) and are in the queue to be paid by Field Solutions. 
<P>

<B>Paid</B> - A listing of all your work orders that have been paid to include the date a check was cut and mailed or the direct deposit process began.   
<P>
<B>Incomplete</B> - A listing of all/any of your work orders that you have not (properly) completed to include updating your work order(s) online per instructions.
<P>
<B>All</B> - A listing of all your work orders


<P class="nextButton" align="center"><a href="FS_AssignedWork.php">Previous</a>
&nbsp; | &nbsp;<a href="FS_CompleteWork.php">Next</a>
</TD></TR></TBODY></TABLE>











</div>

<!--- End Content --->
<?php require ("../../footer.php"); ?>
		
