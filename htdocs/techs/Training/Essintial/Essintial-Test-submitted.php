<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Essintial Test Submission</title>
</head>

<body>
<?php
require_once("../../../library/smtpMail.php");
require_once("../../../library/caspioAPI.php");

$TechID=$_GET["TechID"];
$Score=$_GET["Grade"];
$DateTaken = $_GET["DateTaken"];
		
if($Score >= 86) {
		
	caspioUpdate("TR_Master_List", "EssintialCert", "'1'", "TechID = '$TechID'");
	
	$records = caspioSelectAdv("TR_Master_List", "FirstName, LastName", "TechID = '$TechID'", "", false, "`", "|^",false);
	foreach ($records as $tech) {
		$fields = explode("|^", $tech);
		$techFirst = caspioEscape(trim($fields[0], "`"));
		$techLast = caspioEscape(trim($fields[1], "`"));
	}
	
	$fromName = 'Field Solutions';
	$fromEmail = 'no-replies@fieldsolutions.com';
	$subject = 'Essintial Professionalism Training Test - Tech Passed: ' . $TechID;
	$html_message = 'The following tech has passed the Essintial Professionalism Training:<br><br>';
	$html_message .= 'Tech ID: ' . $TechID . '<br>';
	$html_message .= 'Tech Name: ' . $techFirst . " " . $techLast . '<br><br>';
	$html_message .= 'Thank you,<br><br>';
	$html_message .= 'Your FieldSolutions Team';

	$message = str_replace('<br/>', chr(13) . chr(10), $html_message); // Plain text version of message
	//$emailList = "gbailey@fieldsolutions.com";
	$emailList = "dfigueroa@essintial.com"; //essintialservicesolutions@yahoo.com";
	smtpMailLogReceived($fromName, $fromEmail, $emailList, $subject, $message, $html_message, "Essintial-test-submitted.php");
	
	
}

echo "<script type=\"text/javascript\">
		window.location = \"Essintial-Test Results.php?TechID=$TechID&Grade=$Score&DateTaken=$DateTaken\";
	</script>";

?>

</body>
</html>
