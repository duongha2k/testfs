<?php
	ini_set("display_errors", 1); 
?>

<?php $page = 'techs'; ?>
<?php $option = 'home'; ?>
<?php 
	require_once ("../../../headerStartSession.php");

	$loggedIn   = ( !empty($_SESSION['loggedIn']) ) ? $_SESSION['loggedIn'] : 'no';

    if($loggedIn !== 'yes'){
        header( 'Location: http'.($_SERVER['HTTPS']=='on' ? 's':'').'://'.$_SERVER['HTTP_HOST'].'/techs/logIn.php?flsupport=true' ); // Redirect to logIn.php
   		exit;
    }
?>

<?php  require ("../../../header.php"); ?>
<?php  require ("../../../navBar.php");  ?>
<?php  require ("../../../library/mySQL.php"); ?>
<script type="text/javascript">

$(document).ready(function(){
	$("body").css("width", "850px").css("min-width", "850px");
	
});

</script>

<style type="text/css">
	#emailBlast {
		text-align: center;
	}
	.verdana2 {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		clear: both;
	}

	.verdana2bold {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		font-weight: bold;
		clear: both;
	}

	.fieldGroup {
		padding-bottom: 5px;
		border-bottom: 2px solid #2A497D;
	}

	.formBox, .formBoxFooter {
		margin: 15px auto 0px auto;
		padding: 15px;
		width: 920px;
		color: #2A497D;
		font-size: 12px;
		border: 2px solid #2A497D;
	}

	.formBox input {
		font-size: 12px;
		height: 14px;
	}

	.formBoxFooter {
		background-color: #BACBDF;
		text-align: center;
		margin: 0px auto 10px auto;
		padding: 5px 15px;
		border-top: none;
	}
	.formRow3, .formRow4, .formRow5, .formRow6 {
		width: 100%;
		clear: both;
	}
	.formRow3 div {
		width: 33%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow4 div {
		width: 25%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow5 div {
		width: 20%;
		float: left;
		margin-bottom: 5px;
	}

	.formRow6 div {
		width: 16%;
		float: left;
		margin-bottom: 5px;
	}

	.resultsTable {
		width: 80%;
		margin: 10px auto 0px auto;
	}

	.resultsTable thead {
		text-align: center;
		color: #2A497D;
		border: 1px solid #2A497D;
		background-color: #A0A8AA;
		cursor: default;
	}

	.sortAble, #nextPageBtn, #prevPageBtn {
		cursor: pointer;
	}

	.sortAbleSelected {
		cursor: pointer;
		background-color: #FF9900;
		color: #FFFFFF;
		font-weight: bold;
	}

	.resultsTable tfoot {
		text-align: center;
		color: #000000;
		border: 1px solid #2A497D;
		background-color: #A0A8AA;
	}

	.resultsTable td {
		padding: 3px 5px;
	}

	.resultsTable .evenRow {
		background-color: #E9EEF8;
	}

	.resultsTable .oddRow {
		background-color: #FBFCFD;
	}

	.resultsTable .preferred {
		background-color: #FFFF8C;
	}
    .resultsTable .techGrey {
		background-color: #DEDEDE;
	}
    .resultsTable .techYellow {
		background-color: #FFFF8C;
	}
    .resultsTable .techRed {
		background-color: #FFBBBB;
	}
    .resultsTable .techGreen {
		background-color: #89FF89;
	}

	.searchControls {
		margin-top: 10px;
		text-align: center;
	}

	.noResultsDiv {
		margin-top: 10px;
		text-align: center;
	}
	
	label input {margin-right:10px;}
	.link_button { width: 140px; border:none; background: url(/widgets/images/button_middle.png) left top no-repeat; font: normal 10px/10px Verdana, Arial, Helvetica, sans-serif; padding: 4px 0 8px; color: #385c7e; cursor: pointer; }
</style>

<?php

if (!isset($_POST["sendTest"])) {

?>


<form id="testForm" name="testForm" action="<?php echo $_SERVER['PHP_SELF'] . "?v=" . $_GET["v"]?>&snp=1" method="post">
<div class="formBox">
<div class="verdana2bold" align="center">Essintial Professionalism Training Exam</div><br />
<div class="formRow1">This is an open book exam for Essential. You have to take the Essintial Professionalism Certification Exam to be certified to do future Essintial installations or break/fix work. You can use this document to assist you in taking this exam. </div><br />
<div class="formRow1"><strong>Note: </strong>You must have a score of 100% to be activated with Essintial.</div><br />
  <div class="formRow1">
    <ul>
      <li><a href="Essintial%20Professionalism%20Training%20Presentation.pptx">Essintial Professionalism for Technician</a></li>
    </ul>
    <hr /><br />
    <style type="text/css">  
ul {   margin-bottom: 25px;   margin-left: 50px;  }  li a {    color: red; } 
</style>
</div>
    <div class="verdana2bold">1) The starting point for effective relationships in business is professionalism.</div><br />
    <div class="formRow1">
        <div><label><input id="question1" name="question1" value="1"  type="radio">True</label></div>
        <div><label><input id="question1" name="question1" value="2"  type="radio">False</label></div>
    </div>
    <br />
    <div class="verdana2bold">2) By learning how to communicate with coworkers effectively, you will gain recognition as a force of positive attitude in the workplace.</div><br />
    <div class="formRow2">        
        <div><label><input id="question2" name="question2" value="1"  type="radio">True</label></div>
        <div><label><input id="question2" name="question2" value="2"  type="radio">False</label></div>
    </div>
    <br />
   <div class="verdana2bold">3) Having a positive attitude in the workplace is only beneficial to the individual.</div><br />
   <div class="formRow2">        
        <div><label><input id="question3" name="question3" value="1"  type="radio">True</label></div>
        <div><label><input id="question3" name="question3" value="2"  type="radio">False</label></div>
	</div>
    <br />
   <div class="verdana2bold">4) Good communication skills are not needed in today�s business world.</div><br />
    <div class="formRow2">        
        <div><label><input id="question4" name="question4" value="1"  type="radio">True</label></div>
        <div><label><input id="question4" name="question4" value="2"  type="radio">False</label></div>
	</div>
    <br />
   <div class="verdana2bold">5) Keep the tone positive and emphasize the positive. This will prevent the audience from becoming defensive.</div><br />
    <div class="formRow2">        
        <div><label><input id="question5" name="question5" value="1"  type="radio">True</label></div>
        <div><label><input id="question5" name="question5" value="2"  type="radio">False</label></div>
	</div>
    <br />
   <div class="verdana2bold">6) Setting goals, using an organizer, and writing key points down are effective tools for time management.</div><br />
    <div class="formRow2">        
        <div><label><input id="question6" name="question6" value="1"  type="radio">True</label></div>
        <div><label><input id="question6" name="question6" value="2"  type="radio">False</label></div>
	</div>
    <br />
   <div class="verdana2bold">7) Customer satisfaction has nothing to do with a healthy business relationship.</div><br />
    <div class="formRow2">        
        <div><label><input id="question7" name="question7" value="1"  type="radio">True</label></div>
        <div><label><input id="question7" name="question7" value="2"  type="radio">False</label></div>
	</div>
    <br />
   <div class="verdana2bold">8) Speaking clearly, positive attitude, and patience are key values that everyone should adhere to.</div><br />
    <div class="formRow2">        
        <div><label><input id="question8" name="question8" value="1"  type="radio">True</label></div>
        <div><label><input id="question8" name="question8" value="2"  type="radio">False</label></div>
	</div>
    <br />
   <div class="verdana2bold">9) Work ethic is a set of values based on laziness and carelessness</div><br />
    <div class="formRow2">        
        <div><label><input id="question9" name="question9" value="1"  type="radio">True</label></div>
        <div><label><input id="question9" name="question9" value="2"  type="radio">False</label></div>
	</div>
    <br />
	<div class="verdana2bold">10) Listening to the customer before responding and formulating your responses accordingly are characteristics of good communication.</div><br />
    <div class="formRow2">        
        <div><label><input id="question10" name="question10" value="1"  type="radio">True</label></div>
        <div><label><input id="question10" name="question10" value="2"  type="radio">False</label></div>
	</div>
    <br />
	<div class="verdana2bold">11) With problem solving, understanding the clients issues is not important.</div><br />
    <div class="formRow2">        
        <div><label><input id="question11" name="question11" value="1"  type="radio">True</label></div>
        <div><label><input id="question11" name="question11" value="2"  type="radio">False</label></div>
	</div>
    <br />
	<div class="verdana2bold">12) Setting small goals and achieve them are ways to boost your self-confidence.</div><br />
    <div class="formRow2">        
        <div><label><input id="question12" name="question12" value="1"  type="radio">True</label></div>
        <div><label><input id="question12" name="question12" value="2"  type="radio">False</label></div>
	</div>
    <br />
	<div class="verdana2bold">13) Wearing jeans, t-shirts, and sneakers are acceptable work attire for the ATM/Wincor project.</div><br />
    <div class="formRow2">        
        <div><label><input id="question13" name="question13" value="1"  type="radio">True</label></div>
        <div><label><input id="question13" name="question13" value="2"  type="radio">False</label></div>
	</div>
    <br />
	<div class="verdana2bold">14) Despite local and Federal laws, it is acceptable to bring a gun to an ATM/Bank call.</div><br />
    <div class="formRow2">        
        <div><label><input id="question14" name="question14" value="1"  type="radio">True</label></div>
        <div><label><input id="question14" name="question14" value="2"  type="radio">False</label></div>
	</div>
    <br />
	<div class="verdana2bold">15) Criticism is not meant to be hurtful or demoralizing but:</div><br />
    <div class="formRow2">        
        <div><label><input id="question15" name="question15" value="1"  type="radio">A way to get back at someone</label></div>
        <div><label><input id="question15" name="question15" value="2"  type="radio">An honest and an excellent opportunity to improve</label></div>
        <div><label><input id="question15" name="question15" value="3"  type="radio">Is useless information about yourself</label></div>
        <div><label><input id="question15" name="question15" value="4"  type="radio">None of the above</label></div>
	</div>
    <br />
	<div class="verdana2bold">16) Effective performers in today's organizations are those who:</div><br />
    <div class="formRow2">        
        <div><label><input id="question16" name="question16" value="1"  type="radio">Complete their job and go home</label></div>
        <div><label><input id="question16" name="question16" value="2"  type="radio">Are quiet in the workplace and cause no issues</label></div>
        <div><label><input id="question16" name="question16" value="3"  type="radio">Anticipate future needs and adapt to changing job requirements</label></div>
        <div><label><input id="question16" name="question16" value="4"  type="radio">Keep projects open to "Look Busy"</label></div>
	</div>
    <br />
	<div class="verdana2bold">17) Which is NOT part of having a plan for working under pressure?</div><br />
    <div class="formRow2">        
        <div><label><input id="question17" name="question17" value="1"  type="radio">Maintain contro</label></div>
        <div><label><input id="question17" name="question17" value="2"  type="radio">Evaluate the issue</label></div>
        <div><label><input id="question17" name="question17" value="3"  type="radio">Pass the work off to someone else</label></div>
        <div><label><input id="question17" name="question17" value="4"  type="radio">Have a plan</label></div>
	</div>
    <br />
	<div class="verdana2bold">18) Being a professional includes all of the following with the exception of?</div><br />
    <div class="formRow2">        
        <div><label><input id="question18" name="question18" value="1"  type="radio">Showing-up on time</label></div>
        <div><label><input id="question18" name="question18" value="2"  type="radio">Being polite and respectful to customers and your peers</label></div>
        <div><label><input id="question18" name="question18" value="3"  type="radio">Being negative and argumentative</label></div>
        <div><label><input id="question18" name="question18" value="4"  type="radio">Being a team player</label></div>
	</div>
    <br />
	<div class="verdana2bold">19) Which answer is NOT part of having Good communication skills?</div><br />
    <div class="formRow2">        
        <div><label><input id="question19" name="question19" value="1"  type="radio">Clarity </label></div>
        <div><label><input id="question19" name="question19" value="2"  type="radio">Patience </label></div>
        <div><label><input id="question19" name="question19" value="3"  type="radio">Panic</label></div>
        <div><label><input id="question19" name="question19" value="4"  type="radio">Understanding</label></div>
	</div>
    <br />
	<div class="verdana2bold">20) Being flexible in the workplace means?</div><br />
    <div class="formRow2">        
        <div><label><input id="question20" name="question20" value="1"  type="radio">The ability to not cramp up while doing your job</label></div>
        <div><label><input id="question20" name="question20" value="2"  type="radio">Do yoga during your break</label></div>
        <div><label><input id="question20" name="question20" value="3"  type="radio">Ability to adjust oneself to different conditions and environments</label></div>
        <div><label><input id="question20" name="question20" value="4"  type="radio">None of the above </label></div>
	</div>
    <br />
	<div class="verdana2bold">21) Being efficient and effective are?</div><br />
    <div class="formRow2">        
        <div><label><input id="question21" name="question21" value="1"  type="radio">Characteristics of a professional </label></div>
        <div><label><input id="question21" name="question21" value="2"  type="radio">Words that have little meaning  </label></div>
        <div><label><input id="question21" name="question21" value="3"  type="radio">Ways to destroy a good business relationship</label></div>
        <div><label><input id="question21" name="question21" value="4"  type="radio">Are unnecessary to do a good job</label></div>
	</div>
    <br />
	<div class="verdana2bold">22) A strong work ethic includes?</div><br />
    <div class="formRow2">        
        <div><label><input id="question22" name="question22" value="1"  type="radio">Being reliable</label></div>
        <div><label><input id="question22" name="question22" value="2"  type="radio">Having initiative</label></div>
        <div><label><input id="question22" name="question22" value="3"  type="radio">Maintaining very good social skills</label></div>
        <div><label><input id="question22" name="question22" value="4"  type="radio">All of the above</label></div>
	</div>
    <br />
	<div class="verdana2bold">23) When calling to close a call real-time please make sure you?</div><br />
    <div class="formRow2">        
        <div><label><input id="question23" name="question23" value="1"  type="radio">Call at the end of the day and close all of your calls together</label></div>
        <div><label><input id="question23" name="question23" value="2"  type="radio">Call in the middle of your service ticket and give us an estimate on when you will be done</label></div>
        <div><label><input id="question23" name="question23" value="3"  type="radio">Are completely done servicing the ATM and it has come back online and is operational with no errors</label></div>
        <div><label><input id="question23" name="question23" value="4"  type="radio">Always use Eastern Standard Time</label></div>
	</div>
    <br />
	<div class="verdana2bold">24) When you arrive to site to find the ATM is fully operational you?</div><br />
    <div class="formRow2">        
        <div><label><input id="question24" name="question24" value="1"  type="radio">Leave and move on to the next call</label></div>
        <div><label><input id="question24" name="question24" value="2"  type="radio">Still need to open it up, place it in Supervisor Mode and perform a reset</label></div>
        <div><label><input id="question24" name="question24" value="3"  type="radio">Use your debit card to verify that it works</label></div>
        <div><label><input id="question24" name="question24" value="4"  type="radio">All of the above</label></div>
	</div>
    <br />
	<div class="verdana2bold">25) Chase, as well as many other banks and Credit Unions, do not like to expose opportunity in the branch. By this we mean letting the public know that there is money somewhere. What should you NOT do when entering a bank?</div><br />
    <div class="formRow2">        
        <div><label><input id="question25" name="question25" value="1"  type="radio">Quietly find the branch manager and advise him you are there to service the ATM</label></div>
        <div><label><input id="question25" name="question25" value="2"  type="radio">Close doors when possible to "closet ATMs"</label></div>
        <div><label><input id="question25" name="question25" value="3"  type="radio">Loudly announce you are there to fix the machine to anyone who is listening</label></div>
        <div><label><input id="question25" name="question25" value="4"  type="radio">Work quickly to reduce the risk</label></div>
	</div>
    <br />
	<div class="verdana2bold">26) Open/Close codes are unique to each lock. Please make sure that when you obtain a code from Wincor you?</div><br />
    <div class="formRow2">        
        <div><label><input id="question26" name="question26" value="1"  type="radio">Are using that code</label></div>
        <div><label><input id="question26" name="question26" value="2"  type="radio">Give it to your best friend</label></div>
        <div><label><input id="question26" name="question26" value="3"  type="radio">Share it with other techs</label></div>
        <div><label><input id="question26" name="question26" value="4"  type="radio">Save it and use an older code from last month</label></div>
	</div>
    <br />
	<div class="verdana2bold">27) If the branch is open, you must enter through?</div><br />
    <div class="formRow2">        
        <div><label><input id="question27" name="question27" value="1"  type="radio">The back door</label></div>
        <div><label><input id="question27" name="question27" value="2"  type="radio">The main entrance</label></div>
        <div><label><input id="question27" name="question27" value="3"  type="radio">The side entrance</label></div>
        <div><label><input id="question27" name="question27" value="4"  type="radio">An open window</label></div>
	</div>
    <br />
	<div class="verdana2bold">28) When is it acceptable to have non-Essintial personnel with you during an ATM call?</div><br />
    <div class="formRow2">        
        <div><label><input id="question28" name="question28" value="1"  type="radio">Never</label></div>
        <div><label><input id="question28" name="question28" value="2"  type="radio">When you feel like having someone to talk to</label></div>
        <div><label><input id="question28" name="question28" value="3"  type="radio">When it rains so that they can hold your umbrella for you</label></div>
        <div><label><input id="question28" name="question28" value="4"  type="radio">Mondays and Wednesday only</label></div>
	</div>
    <br />
	<div class="verdana2bold">29) Stealing money from an ATM will result in?</div><br />
    <div class="formRow2">        
        <div><label><input id="question29" name="question29" value="1"  type="radio">Termination</label></div>
        <div><label><input id="question29" name="question29" value="2"  type="radio">Arrested</label></div>
        <div><label><input id="question29" name="question29" value="3"  type="radio">Prosecuted to the fullest extent of the law</label></div>
        <div><label><input id="question29" name="question29" value="4"  type="radio">All of the above</label></div>
	</div>
    </div>      
    <div class="formBoxFooter">
    <div><input id="sendTest" name="sendTest" value="Submit Test" class="link_button" type="submit" /></div>
    </div>
 </form>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>

<?php

} else {
	$q1 =  $_POST['question1'];
	$q2 =  $_POST['question2'];
	$q3 =  $_POST['question3'];
	$q4 =  $_POST['question4'];
	$q5 =  $_POST['question5'];
	$q6 =  $_POST['question6'];
	$q7 =  $_POST['question7'];
	$q8 =  $_POST['question8'];
	$q9 =  $_POST['question9'];
	$q10 =  $_POST['question10'];
	$q11 =  $_POST['question11'];
	$q12 =  $_POST['question12'];
	$q13 =  $_POST['question13'];
	$q14 =  $_POST['question14'];
	$q15 =  $_POST['question15'];
	$q16 =  $_POST['question16'];
	$q17 =  $_POST['question17'];
	$q18 =  $_POST['question18'];
	$q19 =  $_POST['question19'];
	$q20 =  $_POST['question20'];
	$q21 =  $_POST['question21'];
	$q22 =  $_POST['question22'];
	$q23 =  $_POST['question23'];
	$q24 =  $_POST['question24'];
	$q25 =  $_POST['question25'];
	$q26 =  $_POST['question26'];
	$q27 =  $_POST['question27'];
	$q28 =  $_POST['question28'];
	$q29 =  $_POST['question29'];

	$a1 = 1;
	$a2 = 1;
	$a3 = 2;
	$a4 = 2;
	$a5 = 1;
	$a6 = 1;
	$a7 = 2;
	$a8 = 1;
	$a9 = 2;
	$a10 = 1;
	$a11 = 2;
	$a12 = 1;
	$a13 = 2;
	$a14 = 2;
	$a15 = 2;
	$a16 = 3;
	$a17 = 3;
	$a18 = 3;
	$a19 = 3;
	$a20 = 3;
	$a21 = 1;
	$a22 = 3;
	$a23 = 3;
	$a24 = 2;
	$a25 = 3;
	$a26 = 1;
	$a27 = 2;
	$a28 = 1;
	$a29 = 4;
 
$points = 0;

$points = ($q1 == $a1) ? ++$points : $points;
$points = ($q2 == $a2) ? ++$points : $points;
$points = ($q3 == $a3) ? ++$points : $points;
$points = ($q4 == $a4) ? ++$points : $points;
$points = ($q5 == $a5) ? ++$points : $points;
$points = ($q6 == $a6) ? ++$points : $points;
$points = ($q7 == $a7) ? ++$points : $points;
$points = ($q8 == $a8) ? ++$points : $points;
$points = ($q9 == $a9) ? ++$points : $points;
$points = ($q10 == $a10) ? ++$points : $points;
$points = ($q11 == $a11) ? ++$points : $points;
$points = ($q12 == $a12) ? ++$points : $points;
$points = ($q13 == $a13) ? ++$points : $points;
$points = ($q14 == $a14) ? ++$points : $points;
$points = ($q15 == $a15) ? ++$points : $points;
$points = ($q16 == $a16) ? ++$points : $points;
$points = ($q17 == $a17) ? ++$points : $points;
$points = ($q18 == $a18) ? ++$points : $points;
$points = ($q19 == $a19) ? ++$points : $points;
$points = ($q20 == $a20) ? ++$points : $points;
$points = ($q21 == $a21) ? ++$points : $points;
$points = ($q22 == $a22) ? ++$points : $points;
$points = ($q23 == $a23) ? ++$points : $points;
$points = ($q24 == $a24) ? ++$points : $points;
$points = ($q25 == $a25) ? ++$points : $points;
$points = ($q26 == $a26) ? ++$points : $points;
$points = ($q27 == $a27) ? ++$points : $points;
$points = ($q28 == $a28) ? ++$points : $points;
$points = ($q29 == $a29) ? ++$points : $points;

$questions = 29;

$score = round(($points / $questions) *100);

$TechID = $_SESSION['TechID'];
$DateTime = date("Y-m-d H:i:s");

mysqlQuery("INSERT INTO Essintial_Test (tech_id, date_taken, score, a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15, a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28, a29) VALUES ('$TechID', '$DateTime', '$score', '$q1', '$q2', '$q3', '$q4', '$q5', '$q6', '$q7', '$q8', '$q9', '$q10', '$q11', '$q12', '$q13', '$q14', '$q15', '$q16', '$q17', '$q18', '$q19', '$q20', '$q21', '$q22', '$q23', '$q24', '$q25', '$q26', '$q27', '$q28', '$q29')");

echo "<div class=\"formBox\">";
//86
if ($score == 100) {
	mysqlQuery("UPDATE TechBankInfo SET EssintialCert = '1' WHERE TechID = '$TechID'");
	Core_Database_MysqlSync::sync(Core_Database::TABLE_TECH_BANK_INFO, "TechID", $TechID);
	echo "<div class=\"verdana2bold\" style=\"color:green\">Contratulations, you passed the Essintial Certification Exam!</div><div class=\"formRow1\"></div><br />";
	echo "<div class=\"verdana2bold\">SCORE: $score %</div>";
	
	Send_Emails($TechID);

} else {
	echo "<div class=\"verdana2bold\" style=\"color:#F00\">Sorry, you do not have a passing score</div><br />";	
	echo "<div class=\"formRow2\">You will need to re-take the exam and will not be activated for the Essintial Program until you have scored 100%.</div>";
	echo "<div class=\"formRow2\">Please Click <a href=\"Essintial_Training.php\">here</a> to re-take the test.</div><br />";
	echo "<div class=\"verdana2bold\">SCORE: $score %</div>";
	echo "</div></div></div></div>";
}
echo "</div></div></div></div>";

} ?>

<p>
  <?php require ("../../../footer.php"); ?>
</p>