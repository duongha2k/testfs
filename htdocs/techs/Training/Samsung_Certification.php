<?php
$page = "techs";//Main section in navbar
$option = "certifications"; //Sub-section in navbar
$selected = "SamsungCert";
require_once("../../headerStartSession.php");
//require ("../../header.php");
//require ("../../navBar.php");
?>

<!-- Add Content Here -->

<br />
<script type="text/javascript">

$(document).ready(function(){
	$("body").css("width", "850px").css("min-width", "850px");
	
});

</script>
<style type="text/css">
#mainContainer{
	min-width: 800px;
	width: 850px;
}
</style>

<div align="center" width="600">
	<div align="left">
	<h2>Samsung Certification</h2>
	<p>Greetings FieldSolutions Technicians,</p>
	<p>We at FieldSolutions are proud to announce the start-up of a very exciting new project repairing Samsung Flat Panel televisions in residential homes throughout the United States! The work will consist of warranty work (most jobs will be either replacing the motherboard and/or the power supply).  This project has the potential to provide many techs with a great deal of continual work in your respective locations. In preparation of this work in your area, USSI in partnership with FieldSolutions is proud to offer Samsung Certification training online at no cost to you! Obtaining this certification will not only make your resume look great and qualify you for this project, but it will also make you eligible for Samsung Commercial work as well.</p>
	<p>Please follow the enclosed/attached instructions to partake in this training and become Samsung certified. For your convenience, the instructions have been attached to this email in PDF format. Please perform the actions detailed in the attachment to register for the training. This MUST be done before you can proceed with the instructions in the remainder of this email.</p>
	<p>This training is offered through USSI, not FieldSolutions.  USSI has completed most of the information for you in the registration process.  The only personal information you will provide in this registration should be your First Name, Last Name, and email address.  If you have any questions or are experiencing any problems with this training, please contact USSI immediately:</p>
	<p>Amanda Stevens @ <a href="mailto:samsung_tech@ussi.org">samsung_tech@ussi.org</a><br />
	  (321) 723-5395 x 214</p>
	<p>PLEASE STOP NOW AND CLICK ON THE LINK: <a href="/techs/Training/Samsung_Training_Instructions.pdf">Samsung Training Registration Instructions</a></p>
	<p>Please complete the instructions on this attachment in their entirety. Upon completion, please proceed with the remainder of this email which provides instructions to SIGN IN to the training website and complete the training.</p>
	<hr />
	<p>Visit <a href="https://my.plus1solutions.net/clientPortals/samsung/">https://my.plus1solutions.net/clientPortals/samsung/</a> and click on the &quot;Sign In&quot; button. You will be redirected to the login page and will login using the following information:</p>
	<p>Login: Create Login<br />
	  Password: Create Password</p>
	<p>Once logged in you will register for:</p>
	<ul>
	  <li>2009 Samsung Flat Panel Exam</li>
	  <li>2009 Samsung Hospitality Training Exam</li>
	  <li>2009 Samsung LCD Products Exam</li>
	  <li>2009 Samsung PDP Exam</li>
	  <li>2009 Samsung Picture Adjustments Exam</li>
	  <li>2010 Samsung TV Tech Training Exam</li>
	  <li>2010 Samsung Flat Panel Exam</li>
	</ul>
	<p>To access the training material and tests:</p>
	<ol>
	  <li>Login</li>
	  <li>To Access Training Materials:
	    <ol style="list-style:lower-alpha">
	      <li>On the menu on the left click on Training, this will create 3 other subsections
		    <ol style="list-style:lower-roman">
	          <li>Electronics</li>
	          <li>Appliances</li>
	          <li>Office</li>
	        </ol>
	      </li>
	      <li>Click on Electronics and then On-Demand</li>
	      <li>A list of the following trainings will be available on the screen:
		    <ol style="list-style:lower-roman">
	          <li>Plasma</li>
	          <li>2009 Samsung PDP Products</li>
	          <li>2009 Samsung LCD Products</li>
	          <li>2009 Samsung Flat Panel</li>
	          <li>2009 Samsung Picture Adjustments</li>
	          <li>2009 Samsung Hospitality</li>
	          <li>2009 Samsung Systems Training</li>
	          <li>2010 Samsung TV Tech Training Exam</li>
	          <li>2010 Samsung Flat Panel Exam</li>
	        </ol>
	      </li>
	      <li>The ones in which are highlighted above are the courses which USSI and Samsung need all Service Providers to take</li>
	    </ol>
	  </li>
	  <li>To Access The Test:
	    <ol style="list-style:lower-alpha">
	      <li>Click on the upper right corner where it says &quot;Online Exams &amp; Assessments&quot;</li>
	      <li>This will pop-up in a new window, please turn off all pop-up blockers</li>
	      <li>Take the exams/assessments for the training highlighted above</li>
	    </ol>
	  </li>
	</ol>
	<p>If you have any questions, please feel free to contact USSI Field Services at: (please note this training is offered through USSI. All support questions would need to be directed to USSI) </p>
	<p> Amanda Stevens @ <a href="mailto:samsung_tech@ussi.org">samsung_tech@ussi.org</a><br />
	  (321) 723-5395 x 214</p>
	</div>
</div>
	


<!-- End Content -->
<?php /*require ("../../footer.php");*/ ?><!-- ../ only if in sub-dir -->
		
