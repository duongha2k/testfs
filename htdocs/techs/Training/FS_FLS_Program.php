<?php $page = "techs"; ?>
<?php $option = "training"; ?>
<?php $selected = "fs" ?>
<?php $display = "fs" ?>
<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>

<!-- Add Content Here -->

<br /><br />

<div align="center">

<TABLE cellSpacing=0 cellPadding=0 width=700 border=0 style="text-align:left">
<TBODY>
<TR>
<TD colSpan=2><BR><FONT face=Arial size=4><B>The FLS Program/Client</B></FONT> 
<P>
<HR>
<P>
<FONT face=Arial size=2>
FLS (First Line Support) is a major client of Field Solutions. It is not necessary to go through the FLS online training to become fully registered with Field Solutions and work for clients outside of FLS. However,  if/when you want to make yourself eligible to work for the FLS program/client, it is required that you go through the FLS online training on process beginning at the link below. 
<P>
<A href="http://www.flsupport.com/3.html"target=_blank"><FONT face=Arial color=#0062c4 size=2>FLS Online Training</FONT></A>

<P class="nextButton" align="center"><a href="FS_AutoEmails.php">Previous</a>
&nbsp; | &nbsp;<a href="FS_TechnicianSupport.php">Next</a>
</TD></TR></TBODY></TABLE>





</div>

<!--- End Content --->
<?php require ("../../footer.php"); ?>
		
