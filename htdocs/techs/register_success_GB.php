<?php $page = login; ?>
<?php $option = tech; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->


<div align="center">
<h1>Welcome!  Your registration was a success!</h1>
<table width="600" border="0" cellspacing="0" cellpadding="3">
  <tr>
    <td align="left" valign="top"><span style="font-weight: bold">What Happens Next:</span>
      <ol style="list-style-position:outside; margin-left:30px;">
        <li>You will receive a confirmation e-mail. Remember to add us to your &ldquo;safe sender&rdquo; list.</li>
        <li>Log in now using your username and password</li><br /><br />
      </ol></td>
  </tr>
  <tr>
    <td align="left" valign="top"><span style="font-weight: bold">Once you are logged in:</span></td>
  </tr>
  <tr>
    <td align="left" valign="top">
      <ol style="list-style-position:outside; margin-left:30px;">
<!--      <li>Accept the independent contractor agreement; acceptance is required to receive and view work orders.</li>-->
      <li>Complete your tech profile (click on &ldquo;skills&rdquo;, &ldquo;experience&rdquo;,  etc)</li>
      <li>View and bid on available work orders! (click on &ldquo;work order&rdquo; tab)<br />
        <br />
      </li>
    </ol></td>
  </tr>
  <tr>
    <td align="left" valign="top"><p style="font-family:Verdana, Arial, Helvetica, sans-serif; font-size:12px">Our clients will e-mail you work orders based on your profile, so add to your profile:  the more information you put into your technician profile the more work you will get!  Add your skills, certifications and resume.   Also check your profile often, as new fields/sections are being added on a regular basis.<br />
      <br />
    </p>      </td>
  </tr>
  
  <tr>
    <td align="center" valign="top"> <a style="background-color:#000066; color:#FFFFFF; font-weight:bold; padding-left:3px; padding-right:3px; text-decoration:none" href="https://www.fieldsolutions.com/techs/dashboard.php">Login Now!</a><br /><br /></td>
  </tr>
  <tr>
    <td align="left" valign="top"><div align="center">For assistance, contact:<br /> 
      <a href="mailto:support@fieldsolutions.com">support@fieldsolutions.com</a></div></td>
  </tr>
</table>
<p>&nbsp;</p>
</div>



<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
<!-- <div class="register">
<form action="dashboard.php">
	<input class="login" value="Login Now!" type="submit">
</form>
</div>-->
