<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$TechID = $_GET['TechID'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <style type='text/css'>

            body {
                font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
                margin: 0;
                cursor: auto; overflow: hidden;
            }

            h1 {
                margin: 0;
                padding: 0.5em;
            }

            p.description {
                font-size: 0.8em;
                padding: 1em;
                position: absolute;
                top: 3.2em;
                margin-right: 400px;
            } 

            #message {
                font-size: 0.7em;
                position: absolute;
                top: 1em; 
                right: 1em;
                width: 350px;
                display: none;
                padding: 1em;
                background: #ffc;
                border: 1px solid #dda;
            }
            #fancybox-loading {
                position: fixed;
                top: 0;
                width: 100%;
                height: 100%;
                cursor: pointer;
                overflow: hidden;
                z-index: 1104;
                display: none;
                background: transparent;
            }

            #fancybox-loading div {
                position: absolute;
                width: 40px;
                height: 40px;
                left: 50%;
                top: 50%;
                background-image: url('/widgets/images/wait_small.png');
                background-repeat: no-repeat;
            }
        </style>
        <link rel="stylesheet" href="../library/jquery/weekcalendar/jquery-ui.1.7.2.css" type="text/css" />
        <link rel="stylesheet" href="../library/jquery/weekcalendar/jquery.weekcalendar.css" type="text/css" />
        <script type="text/javascript" src="../library/jquery/weekcalendar/jquery.min.1.3.2.js"></script>
        <script type="text/javascript" src="../library/jquery/weekcalendar/jquery-ui.min.1.7.2.js"></script>
        <script type="text/javascript" src="/widgets/js/functions.js"></script>
        <script src="/widgets/js/FSWidget.js"></script>
        <script src="/widgets/js/FSPopupAbstract.js"></script>
        <script src="/widgets/js/FSPopupRoll.js"></script>
        <script type="text/javascript" src="js/techshedule.js"></script>
        <link rel="stylesheet" href="/widgets/css/main.css" type="text/css" media="screen"/>
        <link rel="stylesheet" href="/widgets/css/popup.css" type="text/css" media="screen"/>
        <script type='text/javascript'>
            var year = new Date().getFullYear();
            var month = new Date().getMonth();
            var day = new Date().getDate();
	   
            $(document).ready(function() {
                function displayMessage(message) {
                    $("#message").html(message).fadeIn();
                }

                $("<div id=\"message\" class=\"ui-corner-all\"></div>").prependTo($("body"));
                var _techshedule = techshedule.CreateObject({
                    id:'_techshedule'
                });
            });
            
        </script>
    </head>
    <body>
        <input id="TechID" type="hidden" value="<?= $TechID ?>">
        <div id='calendar'></div>
        <div id="fancybox-loading" style="display: none;"><div ></div></div>
        <div id="event_edit_container">
        </div>
    </body>
</html>
