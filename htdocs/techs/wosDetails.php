<?php
    $page   = 'techs';
    $option = 'details';
    require_once ("../headerStartSession.php");

    $_SERVER['HTTPS'] = ( !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ) ? 'on' : 'off';

    $loggedIn   = ( !empty($_SESSION['loggedIn']) ) ? $_SESSION['loggedIn'] : 'no';
    if($loggedIn !== 'yes'){
        header( 'Location: http'.($_SERVER['HTTPS']=='on' ? 's':'').'://'.$_SERVER['HTTP_HOST'].'/techs/logIn.php' ); // Redirect to logIn.php
    }

    $_GET['v']  = ( !empty($_GET['v']) )  ? $_GET['v'] + 0 : -1;
    $_GET['id'] = ( !empty($_GET['id']) ) ? $_GET['id']+ 0 : -1;

    /**
     * to allow tech view available WO if there isn't tech in get try load from session
     * @author Artem Sukharev
     */
    $_GET['v'] = $_GET['v'] == -1 && isset($_SESSION['TechID']) && !empty($_SESSION['TechID']) ? $_SESSION['TechID'] : $_GET['v'];

    $ckey = md5(microtime().$_GET['id'].$_GET['v']);
    $siteTemplate = ( !empty($_SESSION['template']) ) ? $_SESSION['template'] : NULL;
    if ($siteTemplate == "ruo") {
        require '../templates/ruo/includes/header.php';
    } else {
        require '../header.php';
    }

    $navBarState = ( isset($_COOKIE['wosDetailsNavBarState']) ) ? $_COOKIE['wosDetailsNavBarState'] : NULL;
    if (!$navBarState) {
        $navBarState = 'closed';
    }

    /**
     * <script type="text/javascript" src="/widgets/js/FSTechSchedule.js"></script>
     * var schedule = new FSTechSchedule();
     */
    require_once '../library/StaticMerger.php';
    $merger = new HTTP_StaticMerger('a-secret-and-constant-string');
    $included_js = $merger->getHtml("/js_css_merge.php/", array(
        "/widgets/js/ajaxfileupload.js",
        //"/widgets/js/jquery.formatCurrency-1.4.0.js",
    	"/widgets/js/FSFormValidator.js",
        "/widgets/js/FSFormFilter.js",
        "/widgets/js/FSTechWODetails.js",
        "/widgets/js/FSTechWorkOrderToolBar.js",
        "/techs/js/wosTechDetails.js",
        "/widgets/js/jquery.scrollTo.js"
    ), false, array('v' => '08022011'));

    echo ('<script type="text/javascript" src="/widgets/js/jquery.formatCurrency-1.4.0.js"></script>');
    /*
    <script type="text/javascript" src="/widgets/js/jquery.formatCurrency-1.4.0.js"></script>
    <script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
    <script type="text/javascript" src="/widgets/js/FSFormValidator.js"></script>
    <script type="text/javascript" src="/widgets/js/FSFormFilter.js"></script>
    <script type="text/javascript" src="/widgets/js/FSTechWODetails.js"></script>
    <script type="text/javascript" src="/widgets/js/FSTechWorkOrderToolBar.js"></script>
     */

    $__jsinit__ = '
        <script type="text/javascript" src="/widgets/dashboard/js/prepare-wo/win/'.$_GET['id'].'/key/'.$ckey.'/"></script>'.
        $included_js.'
        <script type="text/javascript">
            var woDetails;
            var _wosTechDetails = null;
            var wd;
            var roll = new FSPopupRoll();
            var toolBar = new FSTechWorkOrderToolBar(null,roll);

            function formatTime( timeObject )
            {
                var r1 = /^(1[0-2]|0?\d)\s*(am|pm)$/i;
                var r2 = /^(1[0-2]|0?\d)\:([0-5]\d)\s*(am|pm)$/i;
                var result;
                var time = $.trim($(timeObject).val()).toUpperCase();

                if ( time && (result = time.match(r1)) != null ) {
                    $(timeObject).val(parseInt(result[1],10).toString() + ":00 " + result[2]);
                } else if ( time && (result = time.match(r2)) != null ) {
                    $(timeObject).val(parseInt(result[1],10).toString() + ":" + result[2] + " " + result[3]);
                } else {
                    $(timeObject).val(time);
                }
            }
            
            function selectHandler( input, dateStr, updateGreenBox )
            {
                var id = $(input).attr("id");
                var form = document.getElementById("woForm");
                if ( form != null )
                {
                    form[id].value = dateStr;
                    woDetails.updateTechHrsValue(form, $("#TechWODetailsSection3_slider #TechTotalHrs"), updateGreenBox);
                }
            }
            
            function onErrorHandler(xhr, status, error)
            {
                if ( xhr.responseText === "authentication required" )
                {
                    document.location = "/clients/logIn.php";
                } else {
                	$(\'#woWorkPlace\').html(xhr.responseText);
                }
            }

            function toggleTab(index)
            {
                $("#TechWODetailsSection"+index+"_slider").slideToggle("fast");
                if ($("#bulletSection"+index).attr("alt") == "Expand Section")
                {
                    $("#bulletSection"+index).attr("alt","Collapse Section");
                    $("#bulletSection"+index).attr("src","/widgets/css/images/triangle_up.gif");
                }
                else
                {
                    $("#bulletSection"+index).attr("alt","Expand Section");
                    $("#bulletSection"+index).attr("src","/widgets/css/images/triangle_down.gif");
                }

                //CHECK ALL
                for (var i=1;i<7;i++)
                {
                    if ($("#bulletSection"+i).length > 0 && $("#bulletSection"+i).attr("alt") == "Collapse Section")
                    {
                        $("#ExpandAllSections").addClass("displayNone");
                        $("#CollapseAllSections").removeClass("displayNone");
                        return;
                    }
                }
                
                $("#ExpandAllSections").removeClass("displayNone");
                $("#CollapseAllSections").addClass("displayNone");
            }
			
            function openTab(index)
            {
                if ($("#bulletSection"+index).attr("alt")==="Expand Section")
                {
                    toggleTab(index);
			}
            }
			
            function closeTab(index)
            {
                if ($("#bulletSection"+index).attr("alt")!=="Expand Section")
                {
                    toggleTab(index);
			}
            }

            function toggleTabV2(index)
            {
                if($("#TechWODetailsSection"+index+"_slider").css("display") == "none"){ 
                    $("#TechWODetailsSection"+index+"_slider").show();
                }
                else{  
                    $("#TechWODetailsSection"+index+"_slider").hide();
                }
                
                if ($("#bulletSection"+index).attr("alt") == "Expand Section")
                {
                    $("#bulletSection"+index).attr("alt","Collapse Section");
                    $("#bulletSection"+index).attr("src","/widgets/css/images/triangle_up.gif");
                }
                else
                {
                    $("#bulletSection"+index).attr("alt","Expand Section");
                    $("#bulletSection"+index).attr("src","/widgets/css/images/triangle_down.gif");
                }

                
            }
			
            function openTabV2(index)
            {
                if ($("#bulletSection"+index).attr("alt")==="Expand Section")
                {
                    toggleTabV2(index);
                }
            }
			
            function closeTabV2(index)
            {
                if ($("#bulletSection"+index).attr("alt")!=="Expand Section")
                {
                    toggleTabV2(index);
                }
            }
            
            function uparrowbtn(index)
            {
                $("#bulletSection"+index).attr("src","/widgets/css/images/triangle_up.gif");
            }
			
            function downarrowbtn(index)
            {
                $("#bulletSection"+index).attr("src","/widgets/css/images/triangle_down.gif");
            }
            
            function toggleAllTabs()
            {
                var up = $("#ExpandAllSections").hasClass("displayNone");
                for (var i = 1;i<7;i++)
                {
                    if ( $("#bulletSection"+i).length > 0 )
                    {
                        if (up && $("#bulletSection"+i).attr("alt") == "Collapse Section")
                        {
                            toggleTab(i);
                        }
                        else if (!up && $("#bulletSection"+i).attr("alt") != "Collapse Section")
                        {
                            toggleTab(i);
                        }
                    }
                }
            }
            
            $(document).ready(function() {
                woDetails = new FSTechWODetails( '. $_GET['id'] .', '. $_GET['v'] .', "'. $ckey .'" );
                wd = new FSWidgetDashboard({container:"widgetContainer",tab:"workdone"});
                
                $.ajax({
                    url      : "/widgets/dashboard/tech-wo-details/details/",
                    data     : { win : '. $_GET['id'] .', key : "'. $ckey .'" },
                    cache    : false,
                    type     : "POST",
                    dataType : "json",
                    error    : onErrorHandler,
                    success  : function(data, status, xhr) {
                        if ( data.success ) {
                            $("#woWorkPlace").html(data.work_place);
                            //jQuery(".masterReimbursable").unbind("blur",woDetails.warningTechClaim).bind("blur",woDetails.warningTechClaim);//376
                            //jQuery("#techN").html(data.Status);
                            if (typeof(data.Status) != "undefined")
                            {
                                if(data.Status.toLowerCase() == "published")
                                {
                                    toggleTabV2(5);                                    
                                    toggleTabV2(1);
                                    toggleTabV2(2);
                                    toggleTabV2(3);
                                }
                                else if(data.Status.toLowerCase() == "assigned")
                                {
                                    toggleTabV2(5);
                                    toggleTabV2(0);
                                    toggleTabV2(1);
                                    toggleTabV2(2);
                                    uparrowbtn(5); 
                                    uparrowbtn(0); 
                                    uparrowbtn(1); 
                                    uparrowbtn(2); 
                                    downarrowbtn(3); 
                                    downarrowbtn(4);
                            }
                                else if(data.Status.toLowerCase() == "incomplete")
                                {
                                    
                                    toggleTabV2(5);
                                    toggleTabV2(2);
                                    downarrowbtn(0); 
                                    downarrowbtn(1); 
                                    uparrowbtn(5); 
                                    uparrowbtn(2); 
                                    downarrowbtn(3); 
                                    downarrowbtn(4);
                                }
                                else
                                {
                                    toggleTabV2(5);
                                    toggleTabV2(3);
                                    downarrowbtn(0); 
                                    downarrowbtn(1); 
                                    downarrowbtn(2); 
                                    uparrowbtn(5); 
                                    uparrowbtn(3); 
                                    downarrowbtn(4);
                                }
                            }
                            _wosTechDetails = new wosTechDetails.CreateObject({
                                id:"_wosTechDetails",
                                detailsWidget:woDetails,
                                wd:wd,
                                win:"'.$_GET['id'].'",
                                techid:"'.$_SESSION['TechID'].'"
                            });
                        }
                        //019
                        else
                        {
                            $("#woWorkPlace").html(data.error);
                    }
                        //end 019
                    }
                });
                
                /*
                $.ajax({
                    url      : "/widgets/dashboard/tech-wo-details/work-place/",
                    data     : { win : '. $_GET['id'] .', key : "'. $ckey .'" },
                    cache    : false,
                    type     : "POST",
                    dataType : "html",
                    error    : onErrorHandler,
                    success  : function(data, status, xhr) { $("#woWorkPlace").html(data);}
                });
                */

                $.ajax({
                    url      : "/widgets/dashboard/tech-wo-details/navigation/",
                    data     : { win : '. $_GET['id'] .', key : "'. $ckey .'" },
                    cache    : false,
                    type     : "POST",
                    dataType : "html",
                    error    : onErrorHandler,
                    success  : function(data, status, xhr) { $("#leftHandMenuOpend").children(".leftHandMenuOpend_box").html(data);}
                });
                });
        </script>';

    require '../navBar.php';
?>

<!-- Check if logged in, if not redirect -->
<script type="text/javascript">
	try {
        v = window._techId = '<?=$_GET['v']?>';
        var win  = '<?=$_GET['id']?>';
		var test = v;
		if ( v != "<?=$_GET['v']?>" && win ) {
            location.replace("<?=$_SERVER['PHP_SELF']?>?v=" + v + "&id=" + id);
        } else if ( v != "<?=$_GET['v']?>" && !win ) {
            location = "/techs/wos.php?v=" + v;
        }
	}
	catch (e) {
		window.location.replace("./");
	}
    var __image__    = new Image(); __image__.src    = "/widgets/images/wait.gif";
    var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";

</script>
<!-- Main content -->
<div style="width:100%;height:300px;">
    <div id="leftHandMenuOpend" style="width: 15%; float:left; <?php if ($navBarState == 'closed') echo 'display:none'?>">
        <div class="leftHandMenuOpend_box">&nbsp;</div>
    </div>
    <div id="leftHandMenuHided" style="width: 3%; float:left;<?php if ($navBarState == 'opened') { echo 'display:none;';} else {echo 'display: block;';} ?>">
        <span class="ajaxLink" onclick="$('#leftHandMenuHided').hide();$('#leftHandMenuOpend').show();$('#woWorkPlace').css('width','84%');setCookie('wosDetailsNavBarState', 'opened', 30);"><img src="/widgets/images/right_arrow.gif" width="23" height="20" border="none" alt="" align="right" /></span><br />
    </div>
    <div id="woWorkPlace" style="margin-top:-30px;float: right;<?php if ($navBarState == 'opened') { echo 'width: 84%;'; } else {echo 'width: 96%;';} ?>"><img style='margin:10% 40%;' alt='Wait...' src='/widgets/images/progress.gif' /></div>
    
</div>
<br class="clear" />


<!-- Main content -->
<?php require ("../footer.php"); ?>

