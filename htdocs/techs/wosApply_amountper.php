<?php
    /**
     * DEPRICATED
     * now used techs/wosDetails.php
     * @autor Artem Sukharev
     */

	if (isset($_GET["nontab"])) {
		$page = techs;
		require ("../header.php");
		require ("../navBar.php");
	}
	else {
		require("../headerTab.php");
	}
	if (!isset($_SESSION["acceptedICA"]) || $_SESSION["acceptedICA"] != "Yes") {
		echo "Please agree to the <a href=\"../content/techTerms_form.php\" target=\"_parent\">Independent Contractor Agreement</a> before applying or bidding for work.";
	}
	else {

require_once("../library/caspioAPI.php");
$workOrderId = (int)$_GET['TB_UNID'];

$query = caspioSelectAdv("Work_Orders", "Project_ID, PayMax, ISNULL(PcntDeduct, '')", "TB_UNID = '$workOrderId'", "", false, "`", "|");
//$projectId = str_replace("`", "", implode("", $query));
//$projectId = (int)$projectId;
foreach ($query as $order) {
	$fields = explode("|", $order);
	$projectId = (int) trim($fields[0], "`");
	$OfferAmount = trim($fields[1], "`");
	$PcntDeduct = trim($fields[2], "`");
}


$query = caspioSelectAdv("TR_Client_Projects", "AutoSMSOnPublish, ISNULL(FixedBid, '')", "Project_ID = '$projectId'", "", false, "`", "|");
//$projectP2T = str_replace("`", "", implode("", $query));
foreach ($query as $project) {
	$fields = explode("|", $project);
	$projectP2T = trim($fields[0], "`");
	$FixedBid = trim($fields[1], "`");
	echo "<script type=\"text/javascript\">var FixedBid = '".$FixedBid."';var OfferAmount = '".$OfferAmount."';var PcntDeduct = '".$PcntDeduct."';</script>";
}

/*if ($projectP2T != 'True') {
    $redirUrl = 'wosApply_amountper.php?' . $_SERVER['QUERY_STRING'];
    header("Location: $redirUrl\n\n");
    exit;
}*/

// Get the e-mail address
$creatorId = $_GET['CreatedBy'];
$client = caspioSelectAdv("TR_Client_List", "Email1", "UserName = '$creatorId'", "", false, "`", "|");
$clientEmail = str_replace("`", "", implode("", $client));
?>
<!-- Add Content Here -->

<script type="text/javascript">
	$(document).ready(function () {

        isProjectAutoAssign = '<?php echo $_GET['isAutoAssign']?>';
        if (isProjectAutoAssign == '') {
            isProjectAutoAssign = $('#cbParamVirtual4').attr('value');
        }
        if(isProjectAutoAssign == "Yes") {
            $('#cbParamVirtual4').attr('value', 'Yes');
            //document.getElementById('comments').style.display = 'none';
            //document.getElementById('InsertRecordComments').style.display = 'none';
            $('#comments').attr('style', 'display:none');
            $('#InsertRecordComments').attr('style', 'display:none');
        } else {
            $('#cbParamVirtual4').attr('value', 'No');
	        $('#P2TbidText').attr('style', 'display:none');
            $('#cbParamVirtual5').attr('style', 'display:none');

        }
		$("#AmountPer").html("Per <?php echo ($_GET["per"] == "" ? "Site" : $_GET["per"])?>");
		if (FixedBid == "True") {
			var fixedBidNotice = "FIXED TOTAL PAY WORK ORDER: this work order is a total pre-set fixed pay work order. No extra $ or hourly rates or bids will<br>be accepted. No added comments will be considered or accepted.<br><br>";
			$("#FixedBid").attr("innerHTML", fixedBidNotice);
			$("#InsertRecordBidAmount").attr("value", OfferAmount);
			$("#InsertRecordBidAmount").attr("readonly", 'true');
		}
		if (PcntDeduct == "True") {
			var deductNotice = "** A 10% service fee will be deducted from your final total payment amount from this work order.";
			$("#deductNotice").attr("innerHTML", deductNotice);
		}
	});
</script>

<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b000013f65d2a5ff04756b669","https:");}catch(v_e){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b000013f65d2a5ff04756b669">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>

<script type="text/javascript">
function validate() {
	bid = document.forms.caspioform.InsertRecordBidAmount.value;
	if (bid == "") {
		document.forms.caspioform.InsertRecordBidAmount.focus();
		alert("Please enter a bid amount");
		return false;
	}
	if (!isValidMoney(bid)) {
		document.forms.caspioform.InsertRecordBidAmount.focus();
		alert("Bid must be formated like 0.00");
		return false;
	}
}

function massageMyMoney() {
	this.value = massageMoney(this.value);
}

try {
	document.forms.caspioform.InsertRecordBidAmount.onblur = massageMyMoney;

	document.forms.caspioform.onsubmit = validate;
}
catch (e) {
}
//$(document).ready(function() {
//    document.getElementById('InsertRecordClientEmail').value = '<?=$clientEmail?>';
//});

</script>

<?php
	}
	if (isset($_GET["nontab"])) {
		require ("../footer.php");
	} else {
	    echo '</body>';
	}
?><!-- ../ only if in sub-dir -->
