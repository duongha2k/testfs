<?php $page = techs; ?>
<?php $option = profile; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->


<br /><br />

<div align="center">
<table cellspacing="0" cellpadding="0" width="500" border="0">
	<tr>
<td>
	
	<p align="center">
	<b>Submitting Your W-9 Form</b>
	</p>
	<p align="left">As an Independent Contractor, Field Solutions must have a W-9 form on file for you in order to issue you a 1099 at year end.</p>
    <p>Please download the W-9 below, fill it out, sign it and fax to: 888-258-1656.  You can also submit it to us via email at <a href="mailto:W9@fieldsolutions.com">W9@fieldsolutions.com</a>.  An email will be sent to you acknowledging receipt.</p>
    <p>If you are submitting your W-9 in a company name, please include your technician registered name and technician ID somewhere on the W-9.</p>
    <p>If you are a Canadian citizen, submit your SIN in place of the U.S. SSN or Business EIN.</p>
	<p align="left"><b>Note:</b> The W-9 form that you need to download below is in PDF.   You can download the Adobe PDF Reader software to view and print out the form by <a href="http://www.adobe.com/products/acrobat/readstep2.html" target=_blank>clicking here</a>.</p>
	<p align="center">Open the W-9 form by <a href="forms/W9.pdf">clicking here</a><br>
		<br>
		<br>
	</p>
</td>
	</tr>	
</table>	

</div>

	


<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
