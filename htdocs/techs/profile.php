<?php $page = 'techs'; ?>
<?php $option = 'profile'; ?>
<?php
	require_once("../headerStartSession.php");
	require_once("../library/FSSession.php");
	FSSession::validateSessionParams(array("TechID" => "TechID"));
?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php require ("../library/googleMapAPI.php"); ?>

<?PHP $ISO_ID= $_SESSION['ISO_ID']; ?>
<!-- Add Content Here -->

<br /><br />

<div align="center">

<table border="0" cellpadding="0" cellspacing="5" width="700">
	<tr valign="top">
<td>
<?php if($ISO_ID==" " || $ISO_ID==""){ ?>
<script type="text/javascript" src="../library/jquery/jquery.autotab.js"></script>
<script type="text/javascript">
	function onCountryChange() {
		$.ajax({
			url         : '/widgets/dashboard/js/state-info/',
			dataType    : 'json',
			data        : {country: $("#EditRecordCountry").val()},
			cache       : false,
			type        : 'post',
			context     : this,
			success     : function (data, status, xhr) {
				if (data) {
					//AUTOFILL
					var option = '';
					option += '<option selected="selected" value="">Select State</option>';
					for(index in data) {
						option += '<option value="' + index + '">' + data[index] + '</option>';
					}
					$("#EditRecordState").html(option);
				}
			}
		});	
	}
	function updateGeocode() {
		zip = $("#EditRecordZipCode").val();
		state = $("#EditRecordState").val();
		address = $("#EditRecordAddress1").val();
		processAddress(address + " " + state + ", " + zip);
	}
	function onReady() {
		initialize();
		//onCountryChange();
		$("#EditRecordZipCode").change(updateGeocode);
		$("#EditRecordState").change(updateGeocode);
		$("#EditRecordAddress1").change(updateGeocode);
		$("#EditRecordCountry").change(onCountryChange);		
		onPointReturned = saveGeocode;
		document.getElementById("map_canvas").style.display = "none";
		loadPhoneNumbers();
		$("#PrimaryAreaCode").autotab({ target: "PrimaryExch", format: "numeric" });
		$("#PrimaryExch").autotab({ target: "PrimaryLastFour", format: "numeric", previous: "PrimaryAreaCode" });
		$("#PrimaryLastFour").autotab({ target: "PrimaryExtension", format: "numeric", previous: "PrimaryExch" });
		$("#PrimaryExtension").autotab({ format: "numeric", previous: "PrimaryLastFour" });

		$("#SecondaryAreaCode").autotab({ target: "SecondaryExch", format: "numeric" });
		$("#SecondaryExch").autotab({ target: "SecondaryLastFour", format: "numeric", previous: "SecondaryAreaCode" });
		$("#SecondaryLastFour").autotab({ target: "SecondaryExtension", format: "numeric", previous: "SecondaryExch" });
		$("#SecondaryExtension").autotab({ format: "numeric", previous: "SecondaryLastFour" });

		$("#SmsAreaCode").autotab({ target: "SmsExch", format: "numeric" });
		$("#SmsExch").autotab({ target: "SmsLastFour", format: "numeric", previous: "SmsAreaCode" });
		$("#SmsLastFour").autotab({ format: "numeric", previous: "SmsExch" });

		$("#caspioform").submit(profileSubmit);

		$("#EditRecordAllowText").change(
		    function(objEvent){
                changeAgreeDate();
	        }
        );
	}

	function saveGeocode(point) {
		$("#EditRecordLatitude").attr("value", point.lat());
		$("#EditRecordLongitude").attr("value", point.lng());
	};

	function loadPhoneNumbers() {
		var primary = $("#EditRecordPrimaryPhone").val();
		var primaryExt = $("#EditRecordPrimaryPhoneExt").val();
		var secondary = $("#EditRecordSecondaryPhone").val();
		var secondaryExt = $("#EditRecordSecondaryPhoneExt").val();
		var smsNumb = $("#EditRecordSMS_Number").val();

		if (primary != "") {
			var phone = primary.split("-");
			$("#PrimaryAreaCode").val(phone[0]);
			$("#PrimaryExch").val(phone[1]);
			$("#PrimaryLastFour").val(phone[2]);
			$("#PrimaryExtension").val(primaryExt);
		}
		if (secondary != "") {
			var phone = secondary.split("-");
			$("#SecondaryAreaCode").val(phone[0]);
			$("#SecondaryExch").val(phone[1]);
			$("#SecondaryLastFour").val(phone[2]);
			$("#SecondaryExtension").val(secondaryExt);
		}
		if (smsNumb != "") {
			var phone = smsNumb.split("-");
			$("#SmsAreaCode").val(phone[0]);
			$("#SmsExch").val(phone[1]);
			$("#SmsLastFour").val(phone[2]);
		}
	}

	function profileSubmit() {
		var areaCode = $("#PrimaryAreaCode").val();
		var exch = $("#PrimaryExch").val();
		var lastFour = $("#PrimaryLastFour").val();
		if (areaCode != "" && exch != "" && lastFour != "") {
			if (areaCode.length != 3 || exch.length != 3 || lastFour.length != 4) {
				alert("Make sure your primary phone number is in this format: ###-###-####");
				return false;
			}
			$("#EditRecordPrimaryPhone").val(areaCode + "-" + exch + "-" + lastFour);
		}
		else {
			alert("Please enter a Primary Phone Number");
			$("#PrimaryAreaCode").focus();
			return false;
		}
		areaCode = $("#SecondaryAreaCode").val();
		exch = $("#SecondaryExch").val();
		lastFour = $("#SecondaryLastFour").val();
		if (areaCode != "" && exch != "" && lastFour != "") {
			if (areaCode.length != 3 || exch.length != 3 || lastFour.length != 4) {
				alert("Make sure your secondary phone number is in this format: ###-###-####");
				return false;
			}
			$("#EditRecordSecondaryPhone").val(areaCode + "-" + exch + "-" + lastFour);
		}
		else {
			if (areaCode != "" || exch != "" || lastFour != "") {
				alert("Please fill in all secondary phone fields");
				return false;
			}
			else
				$("#EditRecordSecondaryPhone").val("");
		}

		areaCode = $("#SmsAreaCode").val();
		exch = $("#SmsExch").val();
		lastFour = $("#SmsLastFour").val();
		if (areaCode != "" && exch != "" && lastFour != "") {
			if (areaCode.length != 3 || exch.length != 3 || lastFour.length != 4) {
				alert("Make sure your SMS phone number is in this format: ###-###-####");
				return false;
			}
			$("#EditRecordSMS_Number").val(areaCode + "-" + exch + "-" + lastFour);
		}
		else {
			if (areaCode != "" || exch != "" || lastFour != "") {
				alert("Please fill in all phone number to receive SMS fields");
				return false;
			}
			else
				$("#EditRecordSMS_Number").val("");
		}

		$("#EditRecordPrimaryPhoneExt").val($("#PrimaryExtension").val());
		$("#EditRecordSecondaryPhoneExt").val($("#SecondaryExtension").val());

		if ($("#EditRecordPrimPhoneType").val() == "")
			$("#EditRecordPrimPhoneType").val("Office");
		if ($("#SecondaryLastFour").val() != "" && $("#EditRecordSecondPhoneType").val() == "")
			$("#EditRecordSecondPhoneType").val("Office");
			
		if ($("#EditRecordAllowText").attr("checked") && $("#EditRecordSMS_Number").val() == "") {
			alert("Please enter a SMS phone number");
			return false;
		}
		if ($("#EditRecordAllowText").attr("checked") && $("#EditRecordCellProvider").val() == "") {
			alert("Please select a cell provider");
			return false;
		}			
	}

function termsPopUp()
{
    termsWindow=window.open('sms_terms.php','Terms','height=300,width=400');
	termsWindow.focus();
}

function changeAgreeDate()
{
    $("#EditRecordSMS_AgreeDate").val('<?php echo date('m/d/Y H:i:s');?>');
}
</script>
<?php
	googleMapAPIJS("onReady");
?>
<div id="map_canvas"></div>
<div id="cb193b000035293d3fb7234e518925"><a href="http://www.caspio.com" target="_blank">Online Database</a> by Caspio</div>
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b000035293d3fb7234e518925","https:");}catch(v_e){;}</script>
<div id="cxkg"><a href="https://bridge.caspio.net/dp.asp?AppKey=193b000035293d3fb7234e518925">Click here</a> to load this Caspio <a href="http://www.caspio.com" title="Online Database">Online Database</a>.</div>
<?php } else { ?>
<div id="cb193b0000e57fa454b28e4bf99375"><a href="http://www.caspio.com" target="_blank">Online Database</a> by Caspio</div>
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b0000e57fa454b28e4bf99375","https:");}catch(v_e){;}</script>
<div id="cxkg"><a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000e57fa454b28e4bf99375">Click here</a> to load this Caspio <a href="http://www.caspio.com" title="Online Database">Online Database</a>.</div>
<?php } ?>

</td>
	</tr>
</table>

</div>

<!--- End Content --->
<?php require ("../footer.php"); ?>

<!--
10-17-2008 GB:
	Old appkey: 193b0000i1g5c8d9i4g7g2f5c9e3
    Became:		193b0000g8d0f6d1e0g9i0g7d9g3
-->
