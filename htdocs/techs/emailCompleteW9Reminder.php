<?php
	require_once("../library/caspioAPI.php");
	require_once("../library/smtpMail.php");
//	ini_set("display_errors",1);
	if (!isset($_GET["techID"]) || !is_numeric($_GET["techID"])) die();
	$techID = $_GET["techID"];
	
	$records = caspioSelectAdv("TR_Master_List", "PrimaryEmail", "TechID = '$techID' AND ISNULL(ISO_Affiliation_ID, 0) = 0 AND W9 = '0'", "", false, "`", "|", false);
	
	if ($records[0] != "") {
		$message = "Thank you for your recent completion of a Field Solutions work order.

We haven't received your W-9 yet.  We will need to receive your W-9 prior to paying you for the completed work order.

Instructions for submitting your W-9 can be found by clicking within the HELP tab after logging in to the site, or by clicking https://www.fieldsolutions.com/techs/w9.php.  

If you are submitting your W-9 in a company name, please include your name and/or your technician ID somewhere on the W-9.

Fax your completed W-9 to 888-258-1656, or email it to accounting@fieldsolutions.com.  W-9's are recorded as received in your profile within 1 business day.  An email will be sent to you acknowledging receipt.

Thank you for your prompt attention to this matter.

Sincerely,

Accounting
Field Solutions";
	
	
		smtpMailLogReceived("Field Solutions Accounting", "accounting@fieldsolutions.com", trim($records[0],"`"), "W-9 not yet received by Field Solutions", $message, nl2br($message), "Tech Mark Complete W9 Reminder");
	}
?>
<script type="text/javascript">
	window.close();
</script>