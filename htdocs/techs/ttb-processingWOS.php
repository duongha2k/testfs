<?php $page = login; ?>
<?php $option = tech; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->
<script type="text/javascript" src="../library/jquery/jquery-packed-1.1.4.js"></script>
<script type="text/javascript">
$(document).ready(function(){
  });
</script>
<TABLE cellSpacing=0 cellPadding=0 width=525 border=0>
<TBODY>
<TR>
<TD colSpan=2>
<BR> 
<FONT face="Arial" size="3"><B>Processing Work Orders</B></FONT>
<P>
<HR>
</P>
<FONT face="Arial" size="2">
<B><a name="bidFLS">How to search and apply for open FLS Work orders</a></B>
<P>
Search and apply for open FLS work orders by clicking on the "------Work Orders-----" drop down list (upper right) and selecting, "View Open FLS Work Orders". The FLS work order search screen will appear. You can then simply click the "Search" button to list ALL FLS work orders and sort them by clicking on the column headers in the search results list. You can also fill in and/or select certain search criteria on the search screen to only list those particular work orders, if any actually exist.</P>

<B><a name="bidMain">How to search and bid on non-FLS open work orders</a></B>
<P>
Search work orders for clients other than FLS to bid on them by clicking on the "------Work Orders-----" drop down list (upper right) and selecting, "View Open Work Orders" The work order search 
screen will appear. You can then simply click the "Search" button to list ALL work orders and sort them by clicking on the column headers in the search results list. You can also fill in and/or select certain search criteria on the search screen to only list those particular work orders, if any actually exist. 
</P>

<B><a name="assignedFLS">How to search FLS work orders assigned to you</a></B>
<P>
To access FLS work orders assigned to you, click on the "-----Work Orders-------" drop down list and select "View My FLS Work Orders". The FLS work order search screen will appear. You can then simply click the "Search" button to list ALL of your FLS work orders and sort them by clicking on the column headers in the search results list. You can also fill in and/or select certain search criteria on the search screen and only bring up those particular work orders. 
</P>

<B><a name="assignedMain">How to search non-FLS work orders assigned to you</a></B>
<P>
To access non-FLS work orders assigned to you, click on the "-----Work Orders-------" drop down list and select "View My Work Orders". The work order search screen will appear. You can then simply click the "Search" button to list ALL of your work orders and sort them by clicking on the column headers in the search results list. You can also fill in and/or select certain search criteria on the search screen and only bring up those particular work orders. 
</P>

<B><a name="completedFLS">How to process FLS work orders online once completed</a></B>
<P>
Click <a href="http://flsupport.com/20.html">here</a> for FLS work order help.
</P>

<B><a name="completedMain">How to process non-FLS work orders online once completed</a></B>
<P>
1) Login <a href="https://www.fieldsolutions.com/techs/index.php">here</a>.<br/>
<br/>

2) Click on the "-----Work Orders-------" drop down list and select "View My Work Orders". The work order search screen will appear. You can then simply click the "Search" button to list ALL of your work orders and sort them by clicking on the column headers in the search results list. You can also fill in and/or select certain search criteria on the search screen and only bring up those particular work orders.<br/><br/>

3) Click on the work order number link in the search results list to open the work order.<br/><br/>

4) Mark your work order(s) complete by checking the box.<br/><br/>

5) Enter any comments that you have. <br/><br/>

6) If/when the client is looking for deliverables (documentation) you will need to attach that where indicated. <br/><br/>

Note: If/when you have more than three files to attached, zip one or more files together.
</P>

<B><a name="printWOS">Printing work orders</a></B>
<P>
You can and should print out all work orders assigned to you. Do so by pulling up the details of the work order and clicking on the "Print" link in the upper-left corner of the work order. <br/><br/>

Note: There is no print feature for FLS work orders, only non-FLS work orders. 
</P>

<B><a name="printWOS">TB payment policy</a></B>
<P>
Click <a href="ttb-payment.php">here</a> for TB payment policy. 
</P>
</FONT>
</TD></TR></TBODY></TABLE>
<pre>



















































</pre>
<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
