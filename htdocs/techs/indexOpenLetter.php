<?php $page = techs; ?>
<?php $option = home; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->

<br /><br /><br />

<div id="leftcontent">
<div id="openLetter" style="margin-left:10px; margin-right:10px; margin-bottom:10px; background-color:#D9D9D9; border: 1px solid #000000;">
<p align="center">
<b>An Open Letter</b>
</p>
<br />
<p>
To All Technicians:
</p>
<p>
As you are aware, there was a problem with accessing our website on Sunday. Although our email and web servers were working fine, people attempting to access our servers had problems doing so. The problem was due to an unauthorized change done to our account at our registrar (the people who host our domain of fieldsolutions.com).
</p>
<p>
We at Field Solutions find this totally unacceptable and inexcusable. Therefore we will be switching registrars to a company that we feel will be more reliable and have a better support plan in place. We will of course keep you posted as to any changes that will effect you during this process.
</p>
<p>
Regards,
</p>
<p>
Field Solutions, Inc.
</p>
</div>

</div>

<div id="rightcontent">
<br /><br />

<div align="center">
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script> <script type="text/javascript" language="javascript">try{f_cbload("F2H4J2B7E3F2H4J2B7E3","https:");}catch(li){;}</script> <div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=F2H4J2B7E3F2H4J2B7E3">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>


<br /><br />
<p><a href="../misc/lostPassword.php">Forgot User Name and/or Password?</a></p>


<p>
<a href="../techs/signup.php">Don't Have an Account?</a>
</p>
</div>
</div>
<!--- End Content --->
<?php require ("../footer.php"); ?>
		
