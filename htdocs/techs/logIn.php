<?php $page = 'techs'; ?>
<?php $option = 'home'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->

<script type="text/javascript">
$(document).ready(function () {
	
	$('#techLoginForm').submit(function(event) {
		event.preventDefault();
		
	  	var formData = $(this).serializeArray();
	   
	   var idx;
	   var query = "";
	   
	   for ( idx in formData ) {
           if ( formData[idx].value ) {
               query += '&' + formData[idx].name + '=' + encodeURIComponent(formData[idx].value).replace(/\~/g, '%7E').replace(/\!/g, '%21').replace(/\'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29');
           }
       }

	   l  = location.protocol + '//' + location.host;
	    l += '/techs/setTechLoginSession.php?v=true';
        l += query;
	 
	    location = l; 
	});

	var reqLogin = '<?=$_REQUEST['login']?>';
	
	if(reqLogin == "false"){
		$("#authFailed").show();
	}else if(reqLogin == "notactivated"){
		$("#notActivated").show();
	}	
	else{
		$("#authFailed").hide();
		$("#notActivated").hide();
	}
	
});

function sendActivateEmail(email){
	jQuery.ajax({
		type: "POST",
		url: "/techs/ajax/sendActivateEmail.php",
		dataType    : 'json',
		cache       : false,
		data: {email: email},
		success: function (data) {
				
		jQuery("#notActivated").html("An email has been sent to your primary email address.");
				
				
		}
	});
}
</script>

<br /><br /><br />
<!--
<div id="leftcontent">
<div id="openLetter" style="margin-left:10px; margin-right:10px; margin-bottom:10px; background-color:#D9D9D9; border: 1px solid #000000;">
<p align="center">
<b>Open Letter</b>
</p>
<br />
<p>
To All Technicians:
</p>

<p>
Over the past few months, we've had some complaints of checks taking too long to get to you once marked Paid on the work orders. We have taken measures that should reduce that window. We have also made some adjustments in how we process your approved work orders to help you with your personal financial planning. Your work order search and results screens now indicate "Pay Scheduled?" and "Pay Date". Your check will go in the mail on the "Pay Date" indicated. 
</p>
<p>
Our goal is to give you as much advance notice as possible. Per our commitment to you, payment for approved work orders will be mailed within 14 business days of approval. 
</p>
<p>
Please understand that Field Solutions has no control over the "Approval Date". If you are having difficulty getting work orders approved, you must work that out with the client that dispatched you to perform the work.
</p>

<p>
Regards,
</p>
<p>
Field Solutions, Inc.
</p>
</div>

</div>

<div id="rightcontent">
<br /><br />
-->

<div align="center">

<!--  
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b0000i8g9d0e1d0b9c5i2c3b1","https:");}catch(ilolli){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000i8g9d0e1d0b9c5i2c3b1">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>
-->
<style type="text/css">
table#techLogin{
	border: 2px solid rgb(56, 92, 126); 
	border-collapse: collapse;
}
table#techLogin tr{
	background-color: rgb(255, 255, 255); 
	padding: 7px;
}
table#techLogin td{
	text-align: left; 
	vertical-align: top; 
	width: auto; 
	white-space: nowrap; 
	padding: 2px 5px; 
	color: rgb(56, 92, 126); 
	font-size: 12px; 
	font-family: Verdana; 
	font-style: normal; 
	font-weight: normal;
}

.techFormInput{
	color: rgb(0, 0, 0); 
	font-size: 12px; 
	font-family: Verdana; 
	font-style: normal; 
	font-weight: normal; 
	border: 1px solid rgb(0, 0, 0); 
	padding: 1px;
}

.error{
	color: rgb(255, 0, 0); 
	font-size: 12px; 
	font-family: Verdana; 
	font-style: normal; 
	font-weight: bold; 
	text-align: left; 
	vertical-align: middle; 
	margin-left: 5px;
	display: none;
	text-align: center;
}
</style>

<p id="authFailed" class="error">
Authentication failed. The information you have provided cannot be authenticated. Check your login information and try again.
</p>

<p id="notActivated" class="error">Your login information is incorrect or your account is not active. Please try re-entering your user name and password. New registrants <a href="javascript:void(0);" onclick="sendActivateEmail('<?=$_REQUEST['email']?>');">Click Here</a> to have the verification email sent again. If you need assistance, please contact Support at
        952-288-2500 or support@fieldsolutions.com.</p>

<form id="techLoginForm" name="techLoginForm" method="post" action="" style="margin: 0px;">
	<input type="hidden" value="193b0000i8g9d0e1d0b9c5i2c3b1" name="appKey">
	<input type="hidden" value="<?=$_REQUEST['flsupport']?>" name="flsupport">
	<input type="hidden" value="0" name="PrevPageID">
	<input type="hidden" name="r" value="<?=htmlentities($_GET['r'])?>">
	<table id="techLogin" cellspacing="0" style="">
	<tbody>
		<tr>
			<td>
				<label for="xip_UserName">Tech User Name</label>
			</td>
			<td>
				<input class="techFormInput" type="text" value="" maxlength="255" size="15" id="UserName" name="UserName">
			</td>
		</tr>
		<tr>
			<td>
				<label for="xip_Password">Password</label>
			</td>
			<td>
				<input class="techFormInput" type="password" value="" maxlength="255" size="15" id="Password" name="Password" >
			</td>
		</tr>
		<tr style="background-color: rgb(255, 255, 255); padding: 7px;">
			<td style="padding: 5px 0px; text-align: center; vertical-align: middle; border: 2px solid rgb(42, 73, 125); background-color: rgb(186, 203, 223);" colspan="2">
				<input type="submit" id="xip_datasrc_TR_Master_List"  value="Login" name="xip_datasrc_TR_Master_List">
			</td>
		</tr>
	</tbody>
	</table>
</form>

Username and Password are case sensitive 

<br />
<p><a href="../misc/lostPassword.php">Forgot User Name and/or Password?</a></p>


<p>
<a href="../techs/signup.php">Don't Have an Account?</a>
</p>
</div>
<!--</div>-->
<!--- End Content --->
<?php require ("../footer.php"); ?>
		
