<?php
    $page   = 'techs';
    $option = 'details';
    require ("../headerStartSession.php");

    $_SERVER['HTTPS'] = ( !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ) ? 'on' : 'off';

    $loggedIn   = ( !empty($_SESSION['loggedIn']) ) ? $_SESSION['loggedIn'] : 'no';
    if($loggedIn !== 'yes'){
        header( 'Location: http'.($_SERVER['HTTPS']=='on' ? 's':'').'://'.$_SERVER['HTTP_HOST'].'/techs/logIn.php' ); // Redirect to logIn.php
    }

    $_GET['v']  = ( !empty($_GET['v']) )  ? $_GET['v'] + 0 : -1;
    $_GET['id'] = ( !empty($_GET['id']) ) ? $_GET['id']+ 0 : -1;

    $ckey = md5(microtime().$_GET['id'].$_GET['v']);
    $siteTemplate = ( !empty($_SESSION['template']) ) ? $_SESSION['template'] : NULL;
    if ($siteTemplate == "ruo") {
        require '../templates/ruo/includes/header.php';
    } else {
        require '../header.php';
    }
    
    $navBarState = ( isset($_COOKIE['wosDetailsNavBarState']) ) ? $_COOKIE['wosDetailsNavBarState'] : NULL;
    if (!$navBarState) {
        $navBarState = 'closed';
    }

    $__jsinit__ = '
		<link rel="stylesheet" href="/library/jquery/calendar/jquery-calendar.css" type="text/css" />
        <script type="text/javascript" src="/library/jquery/calendar/jquery-calendar.js"></script>
        <script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
        <script type="text/javascript" src="/widgets/js/FSWidgetDashboard.js"></script>
        <script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
        <script type="text/javascript" src="/widgets/js/FSFormValidator.js"></script>
        <script type="text/javascript" src="/widgets/js/FSFormFilter.js"></script>
        <script type="text/javascript" src="/widgets/dashboard/js/prepare-wo/win/'.$_GET['id'].'/key/'.$ckey.'/"></script>
        <script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
        <script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
        <script type="text/javascript" src="/widgets/js/FSTechWODetails.js"></script>
        <script type="text/javascript" src="/widgets/js/FSTechWorkOrderToolBar.js"></script>
        <script type="text/javascript">
            var woDetails;
            var wd;
            var roll = new FSPopupRoll();
            var toolBar = new FSTechWorkOrderToolBar(null,roll);

            function formatTime( timeObject )
            {
                var r1 = /^(1[0-2]|0?\d)\s*(am|pm)$/i;
                var r2 = /^(1[0-2]|0?\d)\:([0-5]\d)\s*(am|pm)$/i;
                var result;
                var time = $.trim($(timeObject).val()).toUpperCase();

                if ( time && (result = time.match(r1)) != null ) {
                    $(timeObject).val(parseInt(result[1],10).toString() + ":00 " + result[2]);
                } else if ( time && (result = time.match(r2)) != null ) {
                    $(timeObject).val(parseInt(result[1],10).toString() + ":" + result[2] + " " + result[3]);
                } else {
                    $(timeObject).val(time);
                }
            }
            function selectHandler( input, dateStr, updateGreenBox ) {
                var id = $(input).attr("id");
                var form = document.getElementById("woForm");
                if ( form != null ) {
                    form[id].value = dateStr;
                    woDetails.updateTechHrsValue(form, $("#TechWODetailsSection3_slider #TechTotalHrs"), updateGreenBox);
                }
            }
            function onErrorHandler(xhr, status, error) {
                if ( xhr.responseText === "authentication required" ) {
                    document.location = "/clients/logIn.php";
                } else {
                    $(\'#woWorkPlace\').html(xhr.responseText);
                }
            }
            function toggleBullet( img, direction ) {
                if ( typeof(img) == "string" ) {
                    img = $.trim(img);
                    if      ( img == "" )                return;
                    else if ( img.search(/^#/) == -1 )   img = $("#" + img);
                    else                                 img = $(img);
                }
                direction = direction || null;
                if ( null !== direction && typeof(direction) == "string" ) {
                    direction = direction.toLowerCase();
                    if ( direction !== "up" && direction !== "down" ) {
                        direction = null;
                    }
                }
                if ( $(img).attr("src").search(/plus\.png/) !== -1 && (direction === "down" || direction === null) ) {
                    $(img).attr("src", "/widgets/images/bullet_toggle_minus.png");
                    $(img).attr("alt", "Collapse Section");
                } else if ( $(img).attr("src").search(/minus\.png/) !== -1 && (direction === "up" || direction === null) ) {
                    $(img).attr("src", "/widgets/images/bullet_toggle_plus.png");
                    $(img).attr("alt", "Expand Section");
                }

                var idx;
                var sliders = [
                    "#TechWODetailsSection1_slider",
                    "#TechWODetailsSection2_slider",
                    "#TechWODetailsSection3_slider",
                    "#TechWODetailsSection4_slider",
                    "#TechWODetailsSection5_slider",
                    "#TechWODetailsSection6_slider"
                    ];
                for ( idx in sliders ) {
                    if ( $(sliders[idx]).css("display") !== "none" ) {
                        $("#ExpandAllSections").hide();
                        $("#CollapseAllSections").show();
                        return;
                    }
                }

                $("#ExpandAllSections").show();
                $("#CollapseAllSections").hide();
            }
            function toggleBulletAll( direction ) {
                direction = direction || null;
                if ( null !== direction && typeof direction === "string" ) {
                    direction = direction.toLowerCase();
                    if ( direction !== "up" && direction !== "down" ) {
                        direction = null;
                    }
                }
                $.each([
                    "#TechWODetailsSection1_slider",
                    "#TechWODetailsSection2_slider",
                    "#TechWODetailsSection3_slider",
                    "#TechWODetailsSection4_slider",
                    "#TechWODetailsSection5_slider",
                    "#TechWODetailsSection6_slider"
                ], function() {
                    var result;
                    if ( (result = this.toString().match(/(\d+)/)) != null ) {
                        var id = result[1];
                        if ( direction === null ) {
                            $(this.toString()).slideDown("fast",function(){toggleBullet($("#bulletSection" + id), direction)});
                        } else if ( direction === "down" ) {
                            $(this.toString()).slideDown("fast",function(){toggleBullet($("#bulletSection" + id), direction)});
                        } else if ( direction === "up" ) {
                            $(this.toString()).slideUp("fast",function(){toggleBullet($("#bulletSection" + id), direction)});
                        }
                    }
                });
            }
            $(document).ready(function() {
                woDetails = new FSTechWODetails( '. $_GET['id'] .', '. $_GET['v'] .', "'. $ckey .'" );
                wd = new FSWidgetDashboard({container:"widgetContainer",tab:"workdone"});
                $.ajax({
                    url      : "/widgets/dashboard/tech-wo-details/work-place/",
                    data     : { win : '. $_GET['id'] .', key : "'. $ckey .'" },
                    cache    : false,
                    type     : "POST",
                    dataType : "html",
                    error    : onErrorHandler,
                    success  : function(data, status, xhr) { $("#woWorkPlace").html(data);}
                });
                $.ajax({
                    url      : "/widgets/dashboard/tech-wo-details/sections/",
                    data     : { win : '. $_GET['id'] .', key : "'. $ckey .'" },
                    cache    : false,
                    type     : "POST",
                    dataType : "html",
                    error    : onErrorHandler,
                    success  : function(data, status, xhr) { 
                        $("#woSections").html(data); 
                        if (woDetails._wo != "" && woDetails._wo != null && woDetails._wo.Status != "undefined") {
                            if (!woDetails._wo.WorkOrderReviewed) {
                                $("#TechWODetailsSection1_slider").slideDown("fast",function(){toggleBullet($("#bulletSection1"))});
                                $("#TechWODetailsSection3_slider").slideDown("fast",function(){toggleBullet($("#bulletSection3"))});
                            } else if (!woDetails._wo.TechCheckedIn_24hrs) {
                                $("#TechWODetailsSection1_slider").slideDown("fast",function(){toggleBullet($("#bulletSection1"))});
                                $("#TechWODetailsSection3_slider").slideDown("fast",function(){toggleBullet($("#bulletSection3"))});
                            } else if (!woDetails._wo.TechMarkedComplete && woDetails._wo.Status.toLowerCase() != "incomplete" ) {
                                $("#TechWODetailsSection2_slider").slideDown("fast",function(){toggleBullet($("#bulletSection2"))});
                                $("#TechWODetailsSection3_slider").slideDown("fast",function(){toggleBullet($("#bulletSection3"))});
                                $("#TechWODetailsSection4_slider").slideDown("fast",function(){toggleBullet($("#bulletSection4"))});
                            } else if (woDetails._wo.TechMarkedComplete && woDetails._wo.Status.toLowerCase() != "incomplete" && woDetails._wo.Status.toLowerCase() != "approved" ) {
                                $("#TechWODetailsSection1_slider").slideDown("fast",function(){toggleBullet($("#bulletSection1"))});
                                $("#TechWODetailsSection3_slider").slideDown("fast",function(){toggleBullet($("#bulletSection3"))});
                            } else if (woDetails._wo.Status.toLowerCase() == "incomplete") {
                                $("#TechWODetailsSection4_slider").slideDown("fast",function(){toggleBullet($("#bulletSection4"))});
                                $("#TechWODetailsSection5_slider").slideDown("fast",function(){toggleBullet($("#bulletSection5"))});
                            } else if (woDetails._wo.Status.toLowerCase() == "approved") {
                                $("#TechWODetailsSection1_slider").slideDown("fast",function(){toggleBullet($("#bulletSection1"))});
                                $("#TechWODetailsSection3_slider").slideDown("fast",function(){toggleBullet($("#bulletSection3"))});
                            }
                        }
                    }
                });
            });
        </script>';

    require '../navBar.php';
?>
<!-- Add Content Here -->
<link rel=stylesheet type="text/css" href="/widgets/css/main.css" />
<link rel="stylesheet" type="text/css" href="/widgets/css/popup.css" />

<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193B0000E9C6D4G0I3E9C6D4G0I3","https:");}catch(ilolli){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193B0000E9C6D4G0I3E9C6D4G0I3">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>

<!-- Check if logged in, if not redirect -->
<script type="text/javascript">
	try {
        window._techId = '<?=$_GET['v']?>';
        var win  = '<?=$_GET['id']?>';
		var test = v;
		var techZip = z;
		if ( v != "<?=$_GET['v']?>" && win ) {
            location.replace("<?=$_SERVER['PHP_SELF']?>?v=" + v + "&id=" + id);
        } else if ( v != "<?=$_GET['v']?>" && !win ) {
            location = "/techs/wos.php?v=" + v;
        }
	}
	catch (e) {
		window.location.replace("./");
	}
    var __image__    = new Image(); __image__.src    = "/widgets/images/wait.gif";
    var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";

</script>
<!-- Main content -->

<div>
    <div id="leftHandMenuOpend" style="width: 15%; float:left; <?php if ($navBarState == 'closed') echo 'display:none'?>">
		<div class="leftHandMenuOpend_box">
			<span class="ajaxLink" onclick="$('#leftHandMenuOpend').hide();$('#leftHandMenuHided').show();$('#woWorkPlace').css('width','96%');setCookie('wosDetailsNavBarState', 'closed', 30);"><img src="/widgets/images/left_arrow.gif" width="23" height="20" border="none" alt="" align="right" /></span><br />
			<span onclick="window.close()" class="ajaxLink">&lt; Back</span><br />
            <?php /* <input type="button" id="MyScheduleBtn" value="View My Schedule" class="link_button middle_button"/><br /> */ ?>
			<input type="button" id="FullWOView" value="View Full Work Order" style="margin-bottom:5px;" class="link_button middle_button indentTop" onclick="location = location.toString().replace(/(\#.*)?$/, '#section1'); toggleBulletAll('down');"/><br />
			<br />
			<span><b>View Work Order Details by Section</b></span><br />
			<a class="ajaxLink" href="#section1" onclick="$('#TechWODetailsSection1_slider').slideDown('fast',function(){toggleBullet($('#bulletSection1'),'down')})">1. Work Order Info</a><br />
			<a class="ajaxLink" href="#section2" onclick="$('#TechWODetailsSection2_slider').slideDown('fast',function(){toggleBullet($('#bulletSection2'),'down')})">2. Documents &amp; Parts</a><br />
			<a class="ajaxLink" href="#section3" onclick="$('#TechWODetailsSection3_slider').slideDown('fast',function(){toggleBullet($('#bulletSection3'),'down')})">3. Contact &amp; Check In Info</a><br />
			<a class="ajaxLink" href="#section4" onclick="$('#TechWODetailsSection4_slider').slideDown('fast',function(){toggleBullet($('#bulletSection4'),'down')})">4. Complete Work Order</a><br />
			<a class="ajaxLink" href="#section5" onclick="$('#TechWODetailsSection5_slider').slideDown('fast',function(){toggleBullet($('#bulletSection5'),'down')})">5. Tech Pay</a><br />
			<a class="ajaxLink" href="#section6" onclick="$('#TechWODetailsSection6_slider').slideDown('fast',function(){toggleBullet($('#bulletSection6'),'down')})">6. Other Info</a><br />
			<br />
			<span><b>My Work Orders</b></span><br />
			<a href="wos.php?v=<?=$_GET['v']?>&tab=available" id="orders_link">Available Work</a><br />
			<a href="wos.php?v=<?=$_GET['v']?>&tab=assigned" id="orders_link">Assigned Work</a><br />
			<a href="wos.php?v=<?=$_GET['v']?>&tab=available" id="orders_link">Available Work</a><br />
			<br /> 
			<a href="dashboard.php?v=<?=$_GET['v']?>" id="orders_link">Control Panel</a><br />
			<br />
		</div>
    </div>
    <div id="leftHandMenuHided" style="width: 3%; float:left;<?php if ($navBarState == 'opened') { echo 'display:none;';} else {echo 'display: block;';} ?>">
        <span class="ajaxLink" onclick="$('#leftHandMenuHided').hide();$('#leftHandMenuOpend').show();$('#woWorkPlace').css('width','84%');setCookie('wosDetailsNavBarState', 'opened', 30);"><img src="/widgets/images/right_arrow.gif" width="23" height="20" border="none" alt="" align="right" /></span><br />
    </div>
    <div id="woWorkPlace" style="float: right;<?php if ($navBarState == 'opened') { echo 'width: 84%;'; } else {echo 'width: 96%;';} ?>"><img style='margin:10% 40%;' alt='Wait...' src='/widgets/images/progress.gif' /></div>
</div>
<br class="clear" />
<div id="woSections"></div>

<!-- Main content -->
<?php require ("../footer.php"); ?>

