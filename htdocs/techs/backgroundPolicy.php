<?php $page = 'techs'; ?>
<?php $option = 'backgroundChecks'; ?>
<?php $display = 'yes'; ?>
<?php $selected = 'backgroundPolicy'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->



<br />

	  <h3 align="center"><b>Field Solutions - Background Check Policy</b>	</h3>
	  <p>Field Solutions has adopted a policy on background checks to facilitate technicians in growing their independent contractor business by gaining eligibility and access to clients that have background checks embedded in their work requirements. For clients that have such a requirement, your background check must be conducted via Field Solutions. Previously administered background checks and/or Government clearances are not valid. 
	<p>Technicians are encouraged but not required to have the &ldquo;<span style="font-weight: bold">Basic Background Check</span>&rdquo; conducted for Field Solutions. There are more detailed background checks that some clients may require in the future but not needed at this time. If/when a more detailed background check becomes a requirement for any clients, technicians will be notified and supplied with instructions plus additional fee information (if applicable).</p>
	<p>Field Solutions has contracted with an independent processor to perform the background check. The process is 100% handled by the processor with only the results of the background check being presented to Field Solutions. For the Basic Background Check, the processor indicates pass/fail based on the criteria of whether or not the individual being tested has a conviction for a Criminal Misdemeanor or Felony, or more than one DUI/DWI.</p>
	<p>Field Solutions has negotiated a very good price for the Basic Background Check and as such, is able to pass along the savings to you. The fee to have a Basic Background Check performed is $19.00. The fee is non-refundable. You should consider the fee for a background check part of the investment you need to make to grow your business as a self-employed, independent contractor.  </p>
	<div id="specialNote" class="specialNote" style="margin-left:50px"><span style="font-weight: bold">Special Note:</span> New York residents are assessed $55 more for background checks due to New York State legislative requirements. Field Solutions will cover the extra costs for NY residents but this is subject to change at any time without notice. </div>
	<p>Complete instructions for having a background check performed for Field Solutions, can be found on your Technician Dashboard after logging into your account at the Field Solutions web site. Upon submitting the Disclosure & Authorization form and submitting payment, you will be sent a link allowing you access to secure website where you will submit your application in order to have your background check conducted. </p>
	<p>On average, it takes about 3-5 business days for background check results to come back and for a pass or fail to be indicated in your technician profile/account with Field Solutions.</p>
	<p>Please see the <a href="background_FAQ.php">FAQ section</a> for more detailed information/instructions.</p></td>
  </tr>
</table>
	
	


	


<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
