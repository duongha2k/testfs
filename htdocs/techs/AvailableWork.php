<?php
//https://www.fieldsolutions.com/techs/AvailableWork.php?Zip=78684&Radius=50
	ini_set("display_errors", 1);
	require_once("../library/caspioAPI.php");
	
	require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');

	function locateAddress($address) {
	    
	    static $googleMapApiKey = null;
	    static $googleMapApiClientId = null;
	    
	    if (empty($googleMapApiKey)) {
	        $cfgSite = Zend_Registry::get('CFG_SITE');
            $googleMapApiKey = $cfgSite->get("site")->get("google_map_key");
            $googleMapClientId = $cfgSite->get("site")->get("google_map_client_id");
	    }
	    
		// create a new cURL resource
		$ch = curl_init();
		$address = urlencode($address);
		// set URL and other appropriate options
		$options = array(CURLOPT_URL => "http://maps.google.com/maps/geo?q=$address&sensor=false&client=$googleMapClientId&client=g&output=csv",
						 CURLOPT_HEADER => false,
						 CURLOPT_RETURNTRANSFER => true
						);
			
		curl_setopt_array($ch, $options);
			
		// grab URL and pass it to the browser
		$response = curl_exec($ch);
			
		// close cURL resource, and free up system resources
		curl_close($ch);
			
		$info = explode(",", $response);
		if ($info[0] == "200")
			return array($info[2], $info[3]);
		return false;
	}

	$Zip = locateAddress($_GET["Zip"]);
	$Radius = caspioEscape($_GET["Radius"]);
		
//	if (!$Zip) die();

echo '<?xml version="1.0" encoding="utf-8"?>
			<rss version="2.0">
			<channel>

<title>Field Solutions Available Work Orders</title>
<link>http://www.fieldsolutions.com/</link>
<description>This feed contains available work orders by zip code.</description>

<lastBuildDate>' . date("r") . '</lastBuildDate><language>en-us</language>';
		
//	$result = caspioSelectAdv("Work_Orders_Geocode", "Latitude, Longitude, TB_UNID, StartDate, StartTime, City, State, Zipcode, PayMax, Amount_Per, Description, Requirements, Tools, SpecialInstructions", "Zipcode = '78664' AND IsNull(Tech_ID, '') = '' AND IsNull(Deactivated, '') = '' AND ShowTechs = '1' ", "", false, "`", "|", false);
	

	$result = caspioProximitySearchByCoordinatesRaw("Work_Orders_Geocode", false, $Zip[0], $Zip[1], "Latitude", "Longitude", "<= $Radius.49", "3", "TB_UNID, StartDate, StartTime, City, State, Zipcode, PayMax, Amount_Per, Description, Requirements, Tools, SpecialInstructions", "AND IsNull(Tech_ID, '') = '' AND IsNull(Deactivated, '') = '' AND ShowTechs = '1'", "", false, "|", "`", false);
	
	if (sizeof($result) == 1 && $result[0] == "") $result = array();

	foreach ($result as $row) {
		
//		$row = str_replace("'", "", $row); // gets rid of quotes
		$workOrder = explode("|", $row);

		echo '<item><title>New Work Order - ' . htmlentities(trim($workOrder[3], "`")) . '</title>';
		echo '<link>https://www.fieldsolutions.com/techs/wosDetails.php?id=' . trim($workOrder[0], "`") . '</link>';
		echo '<guid>https://www.fieldsolutions.com/techs/wosDetails.php?id=' . trim($workOrder[0], "`") . '</guid>';
		echo '<pubDate>' .  date("r") . '</pubDate>';
		echo '<description>';
		echo '<b>Work Description:</b><br /> ' . htmlentities(trim($workOrder[8], "`")) . '<br /><br />';
		echo '<b>Work Requirements:</b><br /> ' . htmlentities(trim($workOrder[9], "`")) . '<br /><br />';
		echo '<b>Work Tools:</b><br /> ' . html_entity_decode(trim($workOrder[10], "`")) . '<br /><br />';
		echo '<b>Work Special Instructions:</b><br /> ' . html_entity_decode(trim($workOrder[11], "`"));
		
		echo '</description>';
		echo '</item>';
			
	}

echo '</channel></rss>';


?>