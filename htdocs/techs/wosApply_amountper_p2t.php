<?php
    /**
     * DEPRICATED
     * now used techs/wosDetails.php
     * @autor Artem Sukharev
     */

	if (isset($_GET["nontab"])) {
		$page = techs;
		require ("../header.php");
		require ("../navBar.php");
	}
	else {
		require("../headerTab.php");
	}
	if (!isset($_SESSION["acceptedICA"]) || $_SESSION["acceptedICA"] != "Yes") {
		echo "Please agree to the <a href=\"../content/techTerms_form.php\" target=\"_parent\">Independent Contractor Agreement</a> before applying or bidding for work.";
	}
	else {

require_once("../library/caspioAPI.php");
$workOrderId = (int)$_GET['TB_UNID'];

$query = caspioSelectAdv("Work_Orders", "Project_ID", "TB_UNID = '$workOrderId'", "", false, "`", "|");
$projectId = str_replace("`", "", implode("", $query));
$projectId = (int)$projectId;

$query = caspioSelectAdv("TR_Client_Projects", "AutoSMSOnPublish", "Project_ID = '$projectId'", "", false, "`", "|");
$projectP2T = str_replace("`", "", implode("", $query));

if ($projectP2T != 'True') {
    $redirUrl = 'wosApply_amountper.php?' . $_SERVER['QUERY_STRING'];
    header("Location: $redirUrl\n\n");
    exit;
}

// Get the e-mail address
$creatorId = $_GET['CreatedBy'];
$client = caspioSelectAdv("TR_Client_List", "Email1", "UserName = '$creatorId'", "", false, "`", "|");
$clientEmail = str_replace("`", "", implode("", $client));

?>
<!-- Add Content Here -->

<script type="text/javascript">
	$(document).ready(function () {
		$("#AmountPer").html("Per <?php echo ($_GET["per"] == "" ? "Site" : $_GET["per"])?>");
	});
</script>

<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b0000e5h9c4i8d2e7d0c2a9i5","https:");}catch(v_e){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000e5h9c4i8d2e7d0c2a9i5">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>

<script type="text/javascript">
function validate() {
	bid = document.forms.caspioform.InsertRecordBidAmount.value;
	if (bid == "") {
		document.forms.caspioform.InsertRecordBidAmount.focus();
		alert("Please enter a bid amount");
		return false;
	}
	if (!isValidMoney(bid)) {
		document.forms.caspioform.InsertRecordBidAmount.focus();
		alert("Bid must be formated like 0.00");
		return false;
	}
}

function massageMyMoney() {
	this.value = massageMoney(this.value);
}

try {
	document.forms.caspioform.InsertRecordBidAmount.onblur = massageMyMoney;

	document.forms.caspioform.onsubmit = validate;
}
catch (e) {
}
//$(document).ready(function() {
    document.getElementById('InsertRecordClientEmail').value = '<?=$clientEmail?>';
//});

</script>

<?php
	}
	if (isset($_GET["nontab"])) {
		require ("../footer.php");
	} else {
	    echo '</body>';
	}
?><!-- ../ only if in sub-dir -->
