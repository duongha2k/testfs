<?php

require_once ("../../../headerStartSession.php");
$loggedIn = (!empty($_SESSION['loggedIn']) ) ? $_SESSION['loggedIn'] : 'no';
$msg = "";
$result = array("success" => "0", "msg" => "");
if ($loggedIn !== 'yes') {
    $msg = 'http' . ($_SERVER['HTTPS'] == 'on' ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . '/techs/logIn.php?flsupport=true';
    echo json_encode($result);
    die;
}
require ("../../../library/mySQL.php");
$level = 0;
$result["success"] = "";
$TechID = $_SESSION['TechID'];
$DateTime = date("Y-m-d H:i:s");

if(isset($_POST)){
    $level = $_POST['CRT'];
    $c1 = isset($_POST['C1RT_1'])?1:0;
    $c2 = isset($_POST['C1RT_2'])?1:0;
    $c3 = isset($_POST['C1RT_3'])?1:0;
    $c4 = isset($_POST['C1RT_4'])?1:0;
    $c5 = isset($_POST['C1RT_5'])?1:0;
    $c6 = isset($_POST['C1RE_1'])?1:0;
    $c7 = isset($_POST['C1RE_2'])?1:0;
    $c8 = isset($_POST['C2RT_1'])?1:0;
    $c9 = isset($_POST['C2RT_2'])?1:0;
    $c10 = isset($_POST['C2RT_3'])?1:0;
    $c11 = isset($_POST['C2RT_4'])?1:0;
    $c12 = isset($_POST['C2RT_5'])?1:0;
    $c13 = isset($_POST['C2RT_6'])?1:0;
    $c14 = isset($_POST['C2RT_7'])?1:0;
    $c15 = isset($_POST['C2RT_8'])?1:0;
    $c16 = isset($_POST['C2RT_9'])?1:0;
    $c17 = isset($_POST['C2RT_10'])?1:0;
    $c18 = isset($_POST['C2RT_11'])?1:0;
    $c19 = isset($_POST['C2RE_1'])?1:0;
    $c20 = isset($_POST['C2RE_2'])?1:0;
    $c21 = isset($_POST['C2RE_3'])?1:0;
    $c22 = isset($_POST['C2RE_4'])?1:0;
    $c23 = isset($_POST['C3RT_1'])?1:0;
    $c24 = isset($_POST['C3RT_2'])?1:0;
    $c25 = isset($_POST['C3RT_3'])?1:0;
    $c26 = isset($_POST['C3RT_4'])?1:0;
    $c27 = isset($_POST['C3RE_1'])?1:0;
    $c28 = isset($_POST['C3RE_2'])?1:0;
    $c29 = isset($_POST['C3RE_3'])?1:0;
    $insCabling = mysqlQuery("DELETE FROM BlackBoxCabling_Test WHERE tech_id='$TechID'");
    if ($insCabling > 0) {
        mysqlQuery("INSERT INTO BlackBoxCabling_Test (tech_id, date_taken, Level, C1RT_1,C1RT_2,C1RT_3,C1RT_4,C1RT_5,C1RE_1,C1RE_2, C2RT_1,C2RT_2,C2RT_3,C2RT_4, C2RT_5,C2RT_6,C2RT_7,C2RT_8,C2RT_9,C2RT_10,C2RT_11, C2RE_1, C2RE_2, C2RE_3, C2RE_4, C3RT_1,C3RT_2,C3RT_3,C3RT_4, C3RE_1,C3RE_2,C3RE_3)
                    VALUES ('$TechID','$DateTime',$level, $c1,  $c2, $c3, $c4, $c5,$c6,$c7,$c8,$c9,$c10,$c11,$c12,$c13,$c14,$c15,$c16,$c17,$c18,$c19,$c20,$c21,$c22,$c23,$c24,$c25,$c26,$c27,$c28,$c29)"
                    );
        $result["success"] = "1";
    } else {
        mysqlQuery("INSERT INTO BlackBoxCabling_Test (tech_id, date_taken, Level, C1RT_1,C1RT_2,C1RT_3,C1RT_4,C1RT_5,C1RE_1,C1RE_2, C2RT_1,C2RT_2,C2RT_3,C2RT_4, C2RT_5,C2RT_6,C2RT_7,C2RT_8,C2RT_9,C2RT_10,C2RT_11, C2RE_1, C2RE_2, C2RE_3, C2RE_4, C3RT_1,C3RT_2,C3RT_3,C3RT_4, C3RE_1,C3RE_2,C3RE_3)
                    VALUES ('$TechID','$DateTime',$level, $c1,  $c2, $c3, $c4, $c5,$c6,$c7,$c8,$c9,$c10,$c11,$c12,$c13,$c14,$c15,$c16,$c17,$c18,$c19,$c20,$c21,$c22,$c23,$c24,$c25,$c26,$c27,$c28,$c29)"
                    );
        $result["success"] = "2";
    }
}
echo json_encode($result);
die;
?>
