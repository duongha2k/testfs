<?php $page = 'techs'; ?>
<?php $option = 'wos'; ?>
<?php $selected = 'wosAssigned'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193B0000E9C6D4G0I3E9C6D4G0I3","https:");}catch(ilolli){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193B0000E9C6D4G0I3E9C6D4G0I3">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>
<!--<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b0000e8b4c0a9h0c2c2f4g8a0","https:");}catch(v_e){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000e8b4c0a9h0c2c2f4g8a0">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>-->

<!-- Check if logged in, if not redirect -->
<script type="text/javascript">
	try {
		var test = v;
		var techZip = z;
		if (v != "<?=$_GET["v"]?>")
			window.location.replace("<?=$_SERVER['PHP_SELF']?>?v=" + v + "&tab="+"<?=$_GET['tab']?>");
	}
	catch (e) {
		window.location.replace("./");
	}
</script>
<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript">
	<?php require("includes/getWOCount.php"); ?>

	var startWeek = "<?=$startWeek?>";
	var endWeek = "<?=$endWeek?>";
	var startMonth = "<?=$startMonth?>";
	var endMonth = "<?=$endMonth?>";

	var currentTab = "";
	var oldCurrentTab = "";
	var mode = 0;
	
	var currentPage = 0;
	var lastPage = 0;
	
	var currentSortType = "";
	var lastSortType = "";
	
	var currentOrderBy = "";
	var lastOrderBy = "";

	$(document).ready(function(){
		$('#StartDate').calendar({dateFormat: 'MDY/'});
		$('#EndDate').calendar({dateFormat: 'MDY/'});
		/*if (v2 != "" && fstatus == "Trained") {
			me = document.getElementById("dbTypeLink");
			me.href = "wosFLSAssigned.php?v=" + v2;
			me.style.display = "inline";
		}*/
		dropdownM = document.getElementById('WOAssignedOptions');
		dropdownA = document.getElementById('WOAvailableOptions');
		<?php
			if (isset($_GET["showM"]) && is_numeric($_GET["showM"]))
				echo "dropdownM.selectedIndex = " . $_GET["showM"] . "; filterDateAssigned();\n";
	
			if (isset($_GET["showA"]) && is_numeric($_GET["showA"]))
				echo "dropdownA.selectedIndex = " . $_GET["showA"] . "; filterDateAvailable();\n";
		?>		
	});

	function openTab(tab) {
		try {
			document.getElementById(currentTab).className = "";
		}
		catch (e) {
		}
		try {
			document.getElementById(tab).className = "current";
			currentTab = tab;
		}
		catch(e) {
			currentTab = "all";
		}
		currentOrderBy = "";
		currentSortType = "";
		lastOrderBy = "";
		lastSortType = "";
		reloadTabFrame();
	}
	
	function hideAvailableControls() {
		document.getElementById("tabbedMenu").style.display = ""; // shows tabs
		document.getElementById("distanceSpan").style.display = "none"; // hides distance
	}
	
	function showAvailableControls() {
		document.getElementById("tabbedMenu").style.display = "none"; // hides tabs
		document.getElementById("distanceSpan").style.display = "inline"; // shows distance
	}

	function openMain() {
		if (mode == 1) {
			hideAvailableControls();
			currentTab = oldCurrentTab;
			mode = 0;
		}
		reloadTabFrame();
	}
	
	function openAvailable() {
		if (mode == 0) {
			mode = 1;
			showAvailableControls();
			oldCurrentTab = currentTab;
			currentTab = "available";
		}
		reloadTabFrame();
	}
	
	function reloadTabFrame() {
		// frames['tabFrame'] - accessing object
		// document.getElementById("tabFrame") - accessing html element
		url = "";
//		projectId = document.forms.dashFilter.Project.value;
//		if (projectId == "None") projectId = "";
		startDate = document.forms.dashFilter.StartDate.value;
		endDate = document.forms.dashFilter.EndDate.value;
		distance = document.forms.dashFilter.Distance.value;
		switch (currentTab) {
			case "assigned":
				url = "assignedTab.php";
				break;
			case "workdone":
				url = "workDoneTab.php";
				break;
			case "approved":
				url = "approvedTab.php";
				break;
			case "inaccounting":
				url = "inAccountingTab.php";
				break;
			case "paid":
				url = "paidTab.php";
				break;
			case "incomplete":
				url = "incompleteTab.php";
				break;
			case "all":
				url = "allTab.php";
				break;
			case "available":
				url = "availableTab.php";
				break;
			default:
				openTab("all");
				return;
				break;				
		}

		document.getElementById("tabFrame").src = url + "?cbResetParam=1&CPIpage=" + lastPage + "&CPIsortType=" + currentSortType + "&CPIorderBy=" + currentOrderBy + "&StartDate=" + startDate + "&EndDate=" + endDate + "&TechZip=" + techZip + "&Distance=" + distance;
		lastPage = 0;
		lastSortType = "";	
		lastOrderBy = "";
	}
	
	function resizeTabFrame(width, height, me) {
		if (lastPage == 0) {
			// Remember results page info
			try {
				currentPage = me.document.forms.caspioform.CPIpage.value;
				currentSortType = me.document.forms.caspioform.CPIsortType.value;	
				currentOrderBy = me.document.forms.caspioform.CPIorderBy.value;
			} catch (e) {
				lastPage = currentPage;
				lastSortType = currentSortType;	
				lastOrderBy = currentOrderBy;
			}
		}
		tabFrame = document.getElementById("tabFrame");
		myParent = tabFrame.parentNode;
		try {
			// FF version
			tabFrame.style.width = width + "px";
			tabFrame.style.height = height + "px";
		}
		catch (e) {
			// IE version
			try {
				document.all.tabFrame.style.width = width + "px";
				document.all.tabFrame.style.height = height + "px";
			}
			catch (e) {
			}
		}
		window.scrollTo(0,0);
	}
	
	function filterDateAssigned() {
		dropdown = document.getElementById('WOAssignedOptions');
		switch (dropdown.options[dropdown.selectedIndex].value) {
			case "showWeek":
				showWeek();
				break;
			case "showMonth":
				showMonth();
				break;
			case "showAllOpen":
				showAllOpen();
				break;
		}
	}
	
	function filterDateAvailable() {
		dropdown = document.getElementById('WOAvailableOptions');
		switch (dropdown.options[dropdown.selectedIndex].value) {
			case "showWeekAvailable":
				showWeekAvailable();
				break;
			case "showMonthAvailable":
				showMonthAvailable();
				break;
			case "showAllOpenAvailable":
				showAllOpenAvailable();
				break;
		}
	}
	
	function showWeek() {
		document.getElementById("StartDate").value = startWeek;
		document.getElementById("EndDate").value = endWeek;
		openMain();
	}
	
	function showMonth() {
		document.getElementById("StartDate").value = startMonth;
		document.getElementById("EndDate").value = endMonth;
		openMain();
	}
	
	function showAllOpen() {
		document.getElementById("StartDate").value = "";
		document.getElementById("EndDate").value = "";
		openMain();
	}

	function showWeekAvailable() {
		document.getElementById("StartDate").value = startWeek;
		document.getElementById("EndDate").value = endWeek;
		openAvailable();
	}
	
	function showMonthAvailable() {
		document.getElementById("StartDate").value = startMonth;
		document.getElementById("EndDate").value = endMonth;
		openAvailable();
	}
	
	function showAllOpenAvailable() {
		document.getElementById("StartDate").value = "";
		document.getElementById("EndDate").value = "";
		openAvailable();
	}
</script>

<div id="container">
<div id="header">

    <div id="wrapper">
        <div id="content" align="center"></div>
    </div>


<div style="margin-left:10px; margin-top:25px;">
<select style="width:190px;" id="WOAssignedOptions" name="WOAssignedOptions" size="1" onChange="filterDateAssigned()">
<option value="">Select</option>
<option value="">-----------------------------------</option>
<option value="showWeek">This Week's WOs&nbsp;&nbsp;(<?=$woThisWeek?>)</option>
<option value="showMonth">This Month's WOs&nbsp;&nbsp;(<?=$woThisMonth?>)</option>
<option value="showAllOpen">All WOs&nbsp;&nbsp;(<?=$woAllOpen?>)</option>
</select>
</div>

<!--- DASHBOARD FILTER --->
<div id="projectSearch">
<form id="dashFilter" name="dashFilter" action="<?=$_SERVER['PHP_SELF']?>?v=<?=$_GET["v"]?>" method="post">
<span id="distanceSpan" style="display: none">
	<span style="font-weight: bold; margin-left:10px;">Distance:</span>
	<select id="Distance" name="Distance">
	<option value="50">50 miles</option>
	<option value="75">75 miles</option>
	<option value="100">100 miles</option>
	<option value="150">150 miles</option>
	</select> 
</span>

<span style="font-weight: bold; margin-left:10px;"">Between</span>
<input type="text" name="StartDate" id="StartDate">

<span style="font-weight: bold">And</span>

<input type="text" name="EndDate" id="EndDate">
<input type="button" name="ReloadTabFrame" onClick="reloadTabFrame()" value="Update">
</form>
</div>
    
<div id="noSubstitute" width="500" style="font-weight: bold; margin-top: 10px; margin-bottom: 0px;"><b>Sub-contractors and substitutions are not permitted without prior client approval.<br />Without such approval the client has the sole discretion to deny access to the work site and may not pay the technician.</b></div>  

<div id="tabbedMenu">
<ul>
<li id="assigned"><a href="javascript:openTab('assigned')">Assigned</a></li>
<li id="workdone"><a href="javascript:openTab('workdone')">Work Done</a></li>
<li id="approved"><a href="javascript:openTab('approved')">Approved</a></li>
<li id="inaccounting"><a href="javascript:openTab('inaccounting')">In Accounting</a></li>
<li id="paid"><a href="javascript:openTab('paid')">Paid</a></li>
<li id="incomplete"><a href="javascript:openTab('incomplete')">Incomplete</a></li>
<li id="all"><a href="javascript:openTab('all')">All</a></li>
</ul>
</div>   

<br />

<div class="formHeader" align="center" style="">
	<iframe id="tabFrame" name="tabFrame" class="tabFrame" src="../blank.html" style="width: 100%; height: 100%"></iframe>
</div>    
   
     

<br />

</div>
<script type="text/javascript">
$(document).ready(function(){
      openTab('<?=(isset($_GET['tab'])&&($_GET['tab']!=''))?$_GET['tab']:'assigned';?>');
});
</script>
<!--- End Content --->

<?php require ("../footer2.php"); ?>

<!--
<p align="center">
Copyright &#169;  2008, FieldSolutions, LLC  All rights reserved.
</p>
</body>
</html>-->

