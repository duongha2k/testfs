<?php
    $displayPMTools = false;
    //require '../headerTab.php';
?>

<script type="text/javascript" src="/widgets/js/jquery.min.js"></script>
<link href="/widgets/css/main.css" type="text/css" rel="stylesheet">
<script type="text/javascript">
    //  use for load images and save it in browser cache only!!!
    var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
    var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";
</script>


<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetDashboard.js"></script>
<script type="text/javascript" src="/widgets/js/FSTechPayStatus.js"></script>

<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript">
    
    var statusWidget;
    $(document).ready(function() {
        var conf = {};
        statusWidget = new FSTechPayStatus({container:'mainContainer'},null);
        <?php if ( !empty($_GET['win']) ) :?>
                conf.win = '<?=$_GET['win']?>';
        <?php endif;?>
        <?php if ( !empty($_GET['StartDateFrom']) ) :?>
                conf.StartDateFrom = '<?=$_GET['StartDateFrom']?>';
        <?php endif;?>
        <?php if ( !empty($_GET['StartDateTo']) ) :?>
                conf.StartDateTo = '<?=$_GET['StartDateTo']?>';
        <?php endif;?>
        <?php if ( !empty($_GET['DatePaidFrom']) ) :?>
                conf.DatePaidFrom = '<?=$_GET['DatePaidFrom']?>';
        <?php endif;?>
        <?php if ( !empty($_GET['DatePaidTo']) ) :?>
                conf.DatePaidTo = '<?=$_GET['DatePaidTo']?>';
        <?php endif;?>
        
        statusWidget.show({tab:'index',params:conf});
    });
</script>

<!-- Main content -->
<div>
    <div id="mainContainer" style="width:100%;">
    </div>
</div>
