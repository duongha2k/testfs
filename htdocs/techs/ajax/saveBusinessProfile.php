<?php

if ($_SERVER['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest') {
    require_once("{$_SERVER['DOCUMENT_ROOT']}/library/woapi.php");
    require ("{$_SERVER['DOCUMENT_ROOT']}/headerStartSession.php");
    //($result == "false") ? $status = "false" : $status = "true";

    try {
        $returnArr = array();
        if ($_FILES) {
            $returnArr['uploadResult'] = Core_Tech::uploadTechFileAWS($_FILES, $_GET['techID']);
        }

        if (!empty($_POST)) {
            $returnArr['updateInfoResult'] = Core_Tech::updateTechInfo($_POST);

            //--- update MyWebSite
            $MyWebSite = $_POST['MyWebSite'];
            $techID = $_POST['techID'];
            $Core_Api_TechClass = new Core_Api_TechClass;
            $Core_Api_TechClass->editMyWebSite($techID, $MyWebSite);

            //--- update TMCStatus
            if (isset($_POST['TMCStatusChk'])) {
                $TMCStatus = $_POST['TMCStatusChk'];
                $TMCDate = $_POST['TMCDateTxt'];
                if (!empty($TMCDate))
                    $TMCDate = date_format(new DateTime($TMCDate), 'Y-m-d');

                $Core_Api_TechClass->updateTMC($techID, $TMCStatus, $TMCDate);
                $returnArr['updateTMCStatus'] = $TMCStatus;
            }
            
            //13624, 13931
        /*Array ( [0] => FSTag [1] => BlueRibbonTechnician [2] => Chk )*/                             $Core_Api_FSTagClass = new Core_Api_FSTagClass();
            $FSTagStatusArray = array(); $FSTagDateArray = array();
            foreach($_POST as $k=>$v) {
                $postInfo = split('_',$k);
                if(count($postInfo)==3 && $postInfo[0]=='FSTag'){
                    $fstagName = $postInfo[1];
                    $ext = $postInfo[2];
                    if($ext=='Chk') {
                        $FSTagStatusArray[$fstagName] = $v;
                    } else if($ext=='Datetxt') {
                        $FSTagDateArray[$fstagName] = $v;
                    } else if($ext=='MultiLevelCbx'){
                        if(empty($v)){
                            $FSTagStatusArray[$fstagName] = 0;
                        } else {
                            $FSTagStatusArray[$fstagName] = array('LevelID'=>$v);                            
                        }
                    }
                    
                }
            }
            //echo("<br/>FSTagStatusArray: ");print_r($FSTagStatusArray);die();
            if(count($FSTagStatusArray)>0){
                foreach($FSTagStatusArray as $k=>$v) {
                    if(is_array($v)){
                        if(!empty($v['LevelID'])){
                            //echo("<br/>techID: ");print_r($techID);
                            //echo("<br/>tagName: ");print_r($k);
                            //echo("<br/>Date: ");print_r($FSTagDateArray[$k]);
                            //echo("<br/>LevelID: ");print_r($v['LevelID']);die();
                                $Core_Api_FSTagClass->saveFSTagForTech_FSTagName($techID,$k,'','',$FSTagDateArray[$k],$v['LevelID']);                            
                        }
                    } else if ($v=="1") {
                        $Core_Api_FSTagClass->saveFSTagForTech_FSTagName($techID,$k,'','',$FSTagDateArray[$k]);
                    } else if($v=="0"){
                        $Core_Api_FSTagClass->deleteFSTagForTech_FStagName($techID,$k);
                    } 
                }
            }
            //end 13624, 13931
            
        }

        function isDateValid($str) {
            $stamp = strtotime($str);
            if (!is_numeric($stamp))
                return false;

            //checkdate(month, day, year)
            if (checkdate(date('m', $stamp), date('d', $stamp), date('Y', $stamp))) {
                return true;
            }
            return false;
        }

        function updateCert($params,$_status,$_date,$_num,$id,$type) {
			$error = "";
            
            if (isset($params[$_status]) && isset($params[$_date])) {
				$techId = $params['techID'];
				$status = $params[$_status];
				$date = $params[$_date];
                $num = "";
                if(!empty($_num) && isset($params[$_num])) $num = $params[$_num];
                $valid = 1;
                if (!empty($date)) {
                    if (!isDateValid($date)) {
						$valid = 0;
                        $error = $type . " Status Date is invalid.";
					}    
				}
	
                if ($valid == 1) {
                    if (!empty($date))
                        $date = date_format(new DateTime($date), 'Y-m-d');
                    $techCert = new Core_TechCertification($techId, $id, $num, $date, null);
                    if ($status == 0) {
						$techCert->delete();
					} else {
						$techCert->save();                        
					}
				}
			}
			
			return $error;
		   }        

        updateCert($_POST, 'VonagePlusStatusChkInput', 'VOPDatetxt',"", 18, 'Vonage Plus');
        updateCert($_POST, 'NCRBadgedTechnicianStatusChkInput', 'NCRBadgedTechnicianDatetxt',"", 19, 'NCR Badged Technician');
        updateCert($_POST, 'LMSPreferredTechnicianStatusChkInput', 'LMSPreferredDateInput',"", 20, 'LMS Preferred Technician');
        updateCert($_POST, 'LMSPlusKeyTechnicianStatusChkInput', 'LMSPlusKeyDateInput',"", 21, 'LMS+ Key Contractor');
        // for FlextronicsContractor  
        updateCert($_POST,'FlextronicsScreenedStatusChkInput','FlextronicsScreenedDatetxt',"",23,'Flextronics Screened');
        // for FlextronicsScreened
        updateCert($_POST,'FlextronicsContractorStatusChkInput','FlextronicsContractorDatetxt',"FlextronicsContractorNumStatusInput",22,'Flextronics Contractor');
        updateCert($_POST,'CSCDesksideSupportTechStatusChkInput','CSCDesksideSupportTechDatetxt',"",25,'CSC Deskside Support Tech');
        updateCert($_POST,'ServRightBrotherStatusChkInput','ServRightBrotherDatetxt',"",26,'ServRight ? Brother Certified');

        updateCert($_POST,'FlextronicsRecruitStatusChkInput','FlextronicsRecruitDatetxt',"",30,'Flextronics Recruit');
        updateCert($_POST,'CraftmaticCertifiedStatusChkInput','CraftmaticCertifiedDatetxt',"",31,'Craftmatic Certified');

        $_POST["CompuComCertifiedStatusInputCk1"] = 1;
        $_POST["CompuComCertifiedStatusInputCk0"] = 0;
        updateCert($_POST,"CompuComCertifiedStatusInputCk0",'CompuComCertifiedDateInput',"",33,'CompuCom Certified');
        updateCert($_POST,"CompuComCertifiedStatusInputCk0",'CompuComCertifiedDateInput',"",34,'CompuCom Certified');
        updateCert($_POST,"CompuComCertifiedStatusInputCk0",'CompuComCertifiedDateInput',"",35,'CompuCom Certified');
        updateCert($_POST,"CompuComCertifiedStatusInputCk1",'CompuComCertifiedDateInput',"",$_POST['CompuComCertifiedStatusInput'],'CompuCom Certified');

        $_POST["PurpleTechnicianCertifiedInputCk1"] = 1;
        $_POST["PurpleTechnicianCertifiedInputCk0"] = 0;
        updateCert($_POST,"PurpleTechnicianCertifiedInputCk0",'PurpleTechnicianCertifiedDateInput',"",39,'Purple Technician');
        updateCert($_POST,"PurpleTechnicianCertifiedInputCk0",'PurpleTechnicianCertifiedDateInput',"",40,'Purple Technician');
        updateCert($_POST,"PurpleTechnicianCertifiedInputCk1",'PurpleTechnicianCertifiedDateInput',"",$_POST['PurpleTechnicianCertifiedStatusInput'],'Purple Technician');
        
        Core_Tech::updateCerts($_POST);
        //end 535
        $Core_Api_TechClass = new Core_Api_TechClass;
        if(isset($_POST['CbxGGETechEvaluationFirst']))
        {
            $dateFirst = !empty($_POST['GGETechEvaluationDateInputFirst'])
                ?date_format(new DateTime($_POST['GGETechEvaluationDateInputFirst']), "Y-m-d")
                :(!empty($_POST['CbxGGETechEvaluationFirst'])?date("Y-m-d"):"");
            $Core_Api_TechClass->saveFirstAttemptScoreOrStatus($_POST['techID'],$_POST['CbxGGETechEvaluationFirst'],$dateFirst);
        }
        if(isset($_POST['CbxGGETechEvaluationSecond'])){
            $dateSecond = !empty($_POST['GGETechEvaluationDateInputSecond'])
                ?date_format(new DateTime($_POST['GGETechEvaluationDateInputSecond']), "Y-m-d")
                :(!empty($_POST['CbxGGETechEvaluationSecond'])?date("Y-m-d"):"");
            $Core_Api_TechClass->saveSecondAttemptScoreOrStatus($_POST['techID'],$_POST['CbxGGETechEvaluationSecond'],$dateSecond);
        }
        
        $BlackBoxtechID = $_POST['techID'];
        $BlackBoxCablingid = $_POST['BlackBoxCablingid'];
        $BlackBoxCablingcertifications = $_POST['BlackBoxCablingcertifications'];
        $BlackBoxCablingDate =!empty($_POST['BlackBoxCablingcertDate'])?date_format(new DateTime($_POST['BlackBoxCablingcertDate']), "Y-m-d"):date("Y-m-d");
        $BlackBoxTelecomid = $_POST['BlackBoxTelecomid'];
        $BlackBoxTelecomcertifications = $_POST['BlackBoxTelecomcertifications'];
        $BlackBoxTelecomDate =!empty($_POST['BlackBoxTelecomcertDate'])?date_format(new DateTime($_POST['BlackBoxTelecomcertDate']), "Y-m-d"):date("Y-m-d");

        if(empty($BlackBoxCablingcertifications))
        {
            $Core_Api_TechClass->denyBBCert($BlackBoxCablingid,$BlackBoxtechID);
        }
        else if($BlackBoxCablingcertifications=="pending")
        {
            if(empty($BlackBoxCablingid))
            {
                $BlackBoxCablingid = 46;
            }
            $Core_Api_TechClass->saveBBPending($BlackBoxCablingid,$BlackBoxtechID,$BlackBoxCablingDate);
        }
        else
        {
            $Core_Api_TechClass->saveBBCert($BlackBoxCablingcertifications,$BlackBoxtechID,$BlackBoxCablingDate);
        }

        if(empty($BlackBoxTelecomcertifications))
        {
            $Core_Api_TechClass->denyBBCert($BlackBoxTelecomid,$BlackBoxtechID);
        }
        else if($BlackBoxTelecomcertifications=="pending")
        {
            if(empty($BlackBoxTelecomid))
            {
                $BlackBoxTelecomid = 49;
            }
            $Core_Api_TechClass->saveBBPending($BlackBoxTelecomid,$BlackBoxtechID,$BlackBoxTelecomDate);
        }
        else
        {
            $Core_Api_TechClass->saveBBCert($BlackBoxTelecomcertifications,$BlackBoxtechID,$BlackBoxTelecomDate);
        }
        // send email
        if ($Core_Api_TechClass->isGGEModified ())
        {    
        Core_Api_Class::sendGGETechEvaluation($_POST['techID']);
        }    
        echo json_encode($returnArr);
        exit;
    } catch (Exception $e) {
//		print_r($e);
    }
}
?>