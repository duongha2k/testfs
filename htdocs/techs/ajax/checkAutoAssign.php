<?php
if ($_SERVER['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest') {
	if (!empty($_GET['techId']) && !empty($_GET['woId'])) {
		require ("../../clients/autoassign/includes/common.php");
		require_once("../../library/caspioAPI.php");
		require_once("../../library/timeStamps2.php");
		require ("../../clients/autoassign/includes/AutoAssign.php");
		//AutoAssign::assignFirstBidTech((int)$_GET['woId'],(int)$_GET['techId'],$_GET['firstName'],$_GET['lastName'],(int)$_GET['bidAmount'],$_GET['smsNum']);
		AutoAssign::assignFirstBidTech($_GET['woId'],$_GET['techId'],$_GET['bidAmount']);
		
		require_once("../../library/clientBidNotification.php");

		$winNum = $_GET['woId'];
		$bid = $_GET['bidAmount'];
		$comments = $_GET['comments'];
		$techFName = $_GET['firstName'];
		$techLName = $_GET['lastName'];
//		$info = caspioSelectAdv("Work_Orders", "Company_ID, Project_ID, WO_ID", "TB_UNID = '$winNum'", "", false, "`", "|", false);
		$db = Zend_Registry::get('DB');
		$sql = "SELECT Company_ID, Project_ID, WO_ID, Amount_Per FROM work_orders WHERE WIN_NUM=?";
		$result = $db->fetchRow($sql, array($winNum), Zend_Db::FETCH_NUM);
		$companyID = $result[0];
		$projectID = $result[1];
		$clientWONum = $result[2];
		$amount_per = $result[3];
			
		sendBidNotificationToClient($companyID, $projectID, $winNum, $clientWONum, $techFName, $techLName, $bid, substr($comments, 0, 150), $_GET['techId'], $amount_per);		
	}
}
exit();