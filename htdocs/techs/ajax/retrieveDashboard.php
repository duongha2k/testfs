<?PHP
// Created on: ?
// Author: CM
// Description: Retrieves info to be displayed on tech dashboard

// Updated on: 10-27-07 
// by: CM
// Notes: Updated to use caspiolibrary.php
	
try{

require_once("../../library/caspioAPI.php");
$TechID = $_REQUEST['techID']; 

$records = caspioSelect("TR_Master_List", "techID, FirstName, LastName, PrimaryEmail, skillsComplete, expComplete, certComplete, mediaComplete, equipComplete", "TechID = '$TechID'", false);


// Populate array with workorders
foreach ($records as $order) {
	
	$fields = getFields($order);
	
	// Get data from array and set values for email
	$techID = $fields[0];
	$FirstName = $fields[1];	
	$LastName = $fields[2];
	$PrimaryEmail = $fields[3];
	$skillsComplete = $fields[4];
	$expComplete = $fields[5];
	$certComplete = $fields[6];
	$mediaComplete = $fields[7];
	$equipComplete = $fields[8];
	
	echo "$techID,";
	echo "$FirstName,";
	echo "$LastName,";
	echo "$PrimaryEmail";
	echo "$skillsComplete";
	echo "$expComplete";
	echo "$certComplete";
	echo "$mediaComplete";
	echo "$equipComplete";
	
	}
	
}catch (SoapFault $fault){
	//SOAP fault handling
	$str = "<h1><b>Fault:</b> faultcode: {$fault->faultcode}, <b>faultstring:</b> {$fault->faultstring}</h1>";
	print($str);
}
	
?>

	
