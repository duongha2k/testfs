
<?php

$TechName = "";
$TechID = "";
$_PaymentMethod = "";
 require '../../headerStartSession.php';

  //--- techician info
  $authData = array('login' => $_SESSION["UserName"], 'password' => $_SESSION["UserPassword"]);
  $tech = new Core_Api_TechUser;
  $tech->checkAuthentication($authData);

  if (!$tech->isAuthenticate()) {
  return API_Response::fail();
  }
  $TechName = $techFirstName = $tech->getFirstName() . ' ' . $techLastName = $tech->getLastName();
  $TechID = $tech->getTechId();
  $_PaymentMethod = $tech->getPaymentMethod(); 

//--- get date
$datePayStubValue = trim(urldecode(isset($_REQUEST['datePayStubValue']) ? $_REQUEST['datePayStubValue'] : ''));
$datePayStubText = trim(urldecode($_REQUEST['datePayStubText']));
$netPayForPeriod = "";
$search = new Core_Api_TechClass();
$netPayYear = "";
$startDate = "";
$endDate = "";
$endDateFileName = "";
if ($datePayStubValue != "") {
    //--- start date, end date    
    $date = split(',', $datePayStubValue);
    $startDate = $date[0];
    $endDate = $date[1];
    //* test */
    //$startDate='2007-01-01';
    //* endtest */
    //--- $datePayStubText, $netPayForPeriod, $netPayYear, $result  
    $date0 = new DateTime($startDate);
    $date1 = new DateTime($endDate);
    $endDateFileName = $date1->format('Ymd');
    //$datePayStubText = $date0->format('m/d/Y');
    $netPayForPeriod = $search->getNetPayForPeriod(
            $_SESSION["UserName"], $_SESSION["UserPassword"], $startDate, $endDate);
    $netPayForPeriod = empty($netPayForPeriod) ? '' : '$' . $netPayForPeriod;
    //$ey = date('Y', strtotime($endDate));
    //$netPayYear = $search->getNetPayForYear($_SESSION["UserName"], $_SESSION["UserPassword"], $ey);
    $netPayYear = $search->getNetPayYTD($_SESSION["UserName"], $_SESSION["UserPassword"], $startDate, $endDate);
    $netPayYear = empty($netPayYear) ? '' : '$' . $netPayYear;
    $result = $search->getPayStubsForPeriod($_SESSION["UserName"], $_SESSION["UserPassword"]
            , $startDate, $endDate);
}

// define Font-Path
define('SETAPDF_FORMFILLER_FONT_PATH', 'FormFiller/font/');
// require API
require_once('../../library/pdf/class.ezpdf.php');

$pdf = & new Cezpdf();
$pdf->selectFont('../../library/pdf/fonts/Helvetica.afm');

$image = "../../templates/www/images/fieldSolutionsLogo.jpg";
$pdf->ezSetMargins(0, 0, 0, 0);
$pdf->ezImage($image, 0, 300, "none", "center");
$pdf->ezText("<u>Technician Pay Stub</u>"
        , 14
        , array('justification' => 'center'));
$pdf->ezText("  ");
$pdf->ezText("Paid Date: " . $datePayStubText . ""
        , 10
        , array('justification' => 'center'));
$pdf->ezSetMargins(0, 0, 10, 0);
$dataHerdater = array(
    array("", "", "", "", "")
    , array("Name:", $TechName, "", "", "")
    , array("FS Tech ID#:", $TechID, "", "", "")
    , array("", "", "", "", "")
    , array("Payment Method:", $_PaymentMethod, "", "", "")
    , array("Total Pay:", $netPayForPeriod, "", "", "")
    , array("Year to Date Net Pay:", '' . $netPayYear, "", "", "")
    , array("", "", "", "", "")
);
$pdf->ezTable($dataHerdater, array(0 => '', 1 => '', 2 => '', 3 => '', 4 => '')
        , '', array('showHeadings' => 0
    , 'shaded' => 0
    , 'showLines' => 0
    , 'cols' => array(0 => array('width' => 110)
        , 1 => array('width' => 200)
        , 2 => array('width' => 90)
        , 3 => array('width' => 50)
        , 4 => array('width' => 50))
    , 'width' => '500px'));


$data = array();
if (isset($result) && count($result) > 0) {
    $length = count($result);
    for ($i = 0; $i < $length; $i++) {
        $DateApproved = $result[$i]['DateApproved'];
        $DateApproved = new DateTime($DateApproved);
        $DateApproved = $DateApproved->format("m/d/Y");
        $winNum = $result[$i]['WIN_NUM'];
        $woStartDate = $result[$i]['StartDate'];
        $woStartDate = new DateTime($woStartDate);
        $woStartDate = $woStartDate->format("m/d/Y");
        //$clientName = $result[$i]['SiteName'];
        $clientName = $result[$i]['Headline'];
        //---334,13711   
        $grossPay = "";
        $netPay="";
            $grossPay = $result[$i]['PayAmount'];                 
            $netPay = $result[$i]['PayAmount_Final'];  
            if(empty($netPay)||$netPay == 0)
            {
                $netPay = $grossPay *(1-($result[$i]['PcntDeductPercent']+0));
            }
            $grossPay = number_format($grossPay, 2, '.', '');
            $grossPay = "\$".$grossPay;            
            $netPay = number_format($netPay, 2, '.', '');
            $netPay = "\$".$netPay;
        //---   
        $data[$i]['Start Date'] = $woStartDate;
        $data[$i]['WIN#'] = $winNum;
        $data[$i]['Approved Date'] = $DateApproved;
        $data[$i]['Headline'] = $clientName;
        $data[$i]['Gross Pay'] = $grossPay;
        $data[$i]['Net Pay'] = $netPay;
    }
} else {
    $data = array(
        array("Start Date" => ''
            , "WIN#" => ''
            ,"Approved Date" => ''
            , "Headline" => ''
            , "Gross Pay" => ''
            , "Net Pay" => '')
    );
}

$pdf->ezTable($data, null
        , '', array('showHeadings' => 1
    , 'shaded' => 0
    , 'showLines' => 1
    , 'cols' => array(
        "Start Date" => array('width' => 65)
        , "WIN#" => array('width' => 50)
        , "Approved Date" => array('width' => 85)
        , "Headline" => array('width' => 185)
        , "Gross Pay" => array('width' => 60)
        , "Net Pay" => array('width' => 55))
    , 'width' => '500px'));



$footer = $pdf->openObject();
$pdf->addTextWrap(50, 50, 500, 8, "Field Solutions, Inc. Confidential "
        , 'left'
);
$pdf->addTextWrap(50, 50, 500, 8, date('M d, Y')
        , 'right'
);
$pdf->line(50, 60, 562, 60);
$pdf->closeObject();
$pdf->addObject($footer, "all");

header("Cache-Control: cache, must-revalidate");
header("Pragma: public");
header("Content-Type: application/force-download");
header("Content-Disposition: attachment; filename=\"FSPayStub_".$tech->getLastName().'_'.$endDateFileName.".pdf\"");
//header("Content-Disposition: attachment; filename=\"FSPayStub_1sds_" . $endDateFileName . ".pdf\""); // for test

echo $pdf->output();
exit;
?>
