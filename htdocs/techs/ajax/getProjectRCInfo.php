<?php
/**
 * @author Alex Scherba
 */

//if ($_SERVER['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest'){
	require_once("../../library/caspioAPI.php");
	
	$projectID = (int)$_GET['projectID'];
	$fields = "Resource_Coordinator, Resource_Coord_Phone, Resource_Coord_Email";
	         
	$records = caspioSelectAdv("TR_Client_Projects", $fields , "Project_ID = '".$projectID."'", "", false, "`", "|",false);
	
	if (isset($records[0])) {
		$result = "";
		
	    $projectContactInfo = explode("|", $records[0]);
	 
	    if (is_array($projectContactInfo))
	    {
	        foreach ($projectContactInfo as $key=>$val) {
	        	$projectContactInfo[$key] = trim($val, "`");
	        }
			$result = implode("|", $projectContactInfo);
	    }

	    echo $result;
	}
//}