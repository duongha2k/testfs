<?php
require_once(dirname(__FILE__) . "/../../../headerSimple.php");
?>
<link rel="stylesheet" href="/widgets/css/printW9.css" type="text/css" /> 
<script type="text/javascript">
        $(document).ready(function () {
                $("body").html($("body", opener.document).html());
                $("#w9Info :input",window.opener.document).each(function(){
                    id = $(this).attr("id");
                    
                    if(id != "" && id != null){
                        if($(this).attr("type") == "radio"){
                            val = $(this).val();
                        	$("input[value="+val+"]").attr("checked",true);
                        }else{
                        	$("#"+id).val($(this).val());
                        }
                    }
                });
                $(".hidePrint").hide();
                $("input.required").removeClass("required");
                $("select.required").removeClass("required");
                window.print();
        });
</script>

