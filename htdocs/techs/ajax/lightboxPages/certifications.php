<?php
	ini_set("display_errors", 1); 
?>

<?php $page = 'techs'; ?>
<?php $option = 'home'; ?>
<?php 
	require_once ("../../../headerStartSession.php");

	$loggedIn   = ( !empty($_SESSION['loggedIn']) ) ? $_SESSION['loggedIn'] : 'no';

    if($loggedIn !== 'yes'){
        header( 'Location: http'.($_SERVER['HTTPS']=='on' ? 's':'').'://'.$_SERVER['HTTP_HOST'].'/techs/logIn.php?flsupport=true' ); // Redirect to logIn.php
   		exit;
    }
?>

<?php  require ("../../../header.php"); ?>
<?php  /*require ("../navBar.php");*/  ?>
<?php  require ("../../../library/mySQL.php"); ?>


<style type="text/css">
#mainContainer{
	min-width: 800px;
	width: 850px;
}
	#emailBlast {
		text-align: center;
	}
	.verdana2 {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		clear: both;
	}

	.verdana2bold {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		font-weight: bold;
		clear: both;
	}

	.fieldGroup {
		padding-bottom: 5px;
		border-bottom: 2px solid #2A497D;
	}

	.formBox, .formBoxFooter {
		margin: 15px auto 0px auto;
		padding: 15px;
		width: 500px;
		color: #2A497D;
		font-size: 12px;
		border: 2px solid #2A497D;
	}

	.formBox input {
		font-size: 12px;
		height: 14px;
	}

	.formBoxFooter {
		background-color: #BACBDF;
		text-align: center;
		margin: 0px auto 10px auto;
		padding: 5px 15px;
		border-top: none;
	}
	.formRow3, .formRow4, .formRow5, .formRow6 {
		width: 100%;
		clear: both;
	}
	.formRow3 div {
		width: 33%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow4 div {
		width: 25%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow5 div {
		width: 20%;
		float: left;
		margin-bottom: 5px;
	}

	.formRow6 div {
		width: 16%;
		float: left;
		margin-bottom: 5px;
	}

	.resultsTable {
		width: 80%;
		margin: 10px auto 0px auto;
	}

	.resultsTable thead {
		text-align: center;
		color: #2A497D;
		border: 1px solid #2A497D;
		background-color: #A0A8AA;
		cursor: default;
	}

	.sortAble, #nextPageBtn, #prevPageBtn {
		cursor: pointer;
	}

	.sortAbleSelected {
		cursor: pointer;
		background-color: #FF9900;
		color: #FFFFFF;
		font-weight: bold;
	}

	.resultsTable tfoot {
		text-align: center;
		color: #000000;
		border: 1px solid #2A497D;
		background-color: #A0A8AA;
	}

	.resultsTable td {
		padding: 3px 5px;
	}

	.resultsTable .evenRow {
		background-color: #E9EEF8;
	}

	.resultsTable .oddRow {
		background-color: #FBFCFD;
	}

	.resultsTable .preferred {
		background-color: #FFFF8C;
	}
    .resultsTable .techGrey {
		background-color: #DEDEDE;
	}
    .resultsTable .techYellow {
		background-color: #FFFF8C;
	}
    .resultsTable .techRed {
		background-color: #FFBBBB;
	}
    .resultsTable .techGreen {
		background-color: #89FF89;
	}

	.searchControls {
		margin-top: 10px;
		text-align: center;
	}

	.noResultsDiv {
		margin-top: 10px;
		text-align: center;
	}
	
	label select {margin-right:10px;}
	.link_button { width: 140px; border:none; background: url(/widgets/images/button_middle.png) left top no-repeat; font: normal 10px/10px Verdana, Arial, Helvetica, sans-serif; padding: 4px 0 8px; color: #385c7e; cursor: pointer; }
	
	fieldset {
	  margin: 1.5em 0 0 0;
	  padding: 0;
	}
	legend {
	  margin-left: 1em;
	  color: #000000;
	  font-weight: bold;
	}
	fieldset ol {
	  padding: 1em 1em 0 1em;
	  list-style: none;
	}
	fieldset li {
	  padding-bottom: 1em;
	}	
	fieldset {
	float: left;
	clear: left;
	width: 100%;
	margin: 0 0 1.5em 0; padding: 0;
	}
	label {
  	display: block;
	float: left;
	width: 20em;
	margin-right: 1em;
  	}

</style>

<?php

$TechID = $_SESSION['TechID'];

$certInfo = mysqlFetchAssoc(mysqlQuery("SELECT DellCert, MCSE, CCNA, APlus, CompTIA, FS_Cert_Test_Pass, ServRight_ElectroMech_Cert, EssintialCert, HP_CertNum, NCR_Basic_Cert, SamsungCert, Cert_Hallmark_POS, Cert_Maurices_POS, Starbucks_Cert FROM TechBankInfo WHERE TechID = '".$TechID."'"));
//print_r($certInfo);
//echo sizeof($certInfo);
//print_r $certInfo[0];
if ($certInfo) {
	$certInfo = $certInfo[0];

	$dellCert = $certInfo[DellCert];
	$dellCert = str_replace("'", "", $dellCert);

	$mcse = $certInfo[MCSE];
	$mcse = str_replace("'", "", $mcse);

	$ccna = $certInfo[CCNA];
	$ccna = str_replace("'", "", $ccna);

	$aPlus = $certInfo[APlus];
	$aPlus = str_replace("'", "", $aPlus);

	$compTIA = $certInfo[CompTIA];
	$compTIA = str_replace("'", "", $compTIA);

	$fsCert = $certInfo[FS_Cert_Test_Pass];
	$fsCert = ($fsCert == '1') ? "Yes" : "No";

	$electroMechCert = $certInfo[ServRight_ElectroMech_Cert];
	$electroMechCert = ($electroMechCert == '1') ? "Yes" : "No";

	$essintialCert = $certInfo[EssintialCert];
	$essintialCert = ($essintialCert == '1') ? "Yes" : "No";

	$HPCert = $certInfo[HP_CertNum];
	$HPCert = ($HPCert != '') ? "Yes" : "No";

	$ncrCert = $certInfo[NCR_Basic_Cert];
	$ncrCert = ($ncrCert == '1') ? "Yes" : "No";

	$samsungCert = $certInfo[SamsungCert];
	$samsungCert = ($samsungCert == '1') ? "Yes" : "No";



/*
	if(sizeof($techInfo) == 12){
		$Region .= ", " . $techInfo[FLSRegion];
		$Region = str_replace("'", "", $Region);
	}
*/
}


if (!isset($_POST["updateCert"])) {

?>


<form id="testForm" name="testForm" action="<?php echo $_SERVER['PHP_SELF'] . "?v=" . $_GET["v"]?>&snp=1" method="post">
<div class="formBox">
<fieldset>
<ol>
	<li>
	<label for "dellCert">Dell Certification</label><select name="dellCertType">
   	    <option>Select One</option>
   	    <option value="DCSNP">DCSNP</option>
            <option value="DCSE">DCSE</option>
            <option value="Other">Other</option>
            <option value="None">None</option>
          </select>
	</li>
    <li>
    	<label for "MCSE">MCSE</label>
        <input name="MCSE" type="checkbox" value="" /><span>&nbsp;(check if "Yes")</span>
    </li>
    <li>
    	<label for "MCSE">CCNA</label>
        <input name="MCSE" type="checkbox" value="" />&nbsp;(check if "Yes")
    </li>
    <li>
    	<label for "aPlus">A+</label>
        <input name="aPlus" type="checkbox" value="" />&nbsp;(check if "Yes")
    </li>
    <li>
    	<label for "CompTIA">CompTIA</label>
        <input name="CompTIA" type="text" value=""/>
    </li>
    <!-- 
    <div class="verdana2bold">Click an item below to modify/view it's certification status.</div>
    <br />
    <li>
    	<label for "fsCert">FS Certified?&nbsp;<a href="">Edit</a></label><?php echo "$fsCert"; ?>
    </li>
    <li>
    	<label for "elecSkills">Electro-Mechanical Skills?&nbsp;<a href="Training/ServRight/electroMechTest.php">Edit</a></label><?php echo "$electroMechCert"; ?>
    </li>
    <li>
    	<label for "essintialCert">Essintial Certified?&nbsp;<a href="Training/Essintial/Essintial_Training.php">Edit</a></label><?php echo "$essintialCert"; ?>
	</li>
    <li>
    	<label for "hpCert">HP Certified?&nbsp;<a href="Training/HP_Certification.php">Edit</a></label><?php echo "$HPCert"; ?>
    </li>
    <li>
    	<label for "ncrCert">NCR Certified?&nbsp;<a href="Training/NCR/NCR_Training.php">Edit</a></label><?php echo "$ncrCert"; ?>
    </li>
    <li>
    	<label for "samsungCert">Samsung Certified?&nbsp;<a href="Training/Samsung_Certification.php">Edit</a></label><?php echo "$samsungCert"; ?>
    </li>
    <div class="verdana2bold">Fujitsu FLS Related</div><br />
    <div>The FLS program/Tech opportunity is focused around running service & project calls performing work on hardware and software in the retail and grocery environments for one of our clients. Getting involved with FLS as a Technician requires some technical knowledge, as you will be replacing defective parts dealing with primarily POS equipment in retail stores (keyboards, check readers, monitors, cash registers/drawers, scanners, etc). </div><br . />
    <div>In order to be activated as a Tech with FLS (First Line Support), you will first need to go through the required online training and submit the test with a passing score. Get more information regarding the FLS program and/or begin the online training process <a href="http://www.flsupport.com/2.html">here </a></div><br />
    <li>
	<label for "flsRegion">What FLS Region can you cover?</label><select name="flsRegion">
   		<option>Select One</option>
   	    <option value="DCSNP">No Region w/in 50 miles</option>
        <option value="DCSE">DCSE</option>
        <option value="Other">Other</option>
        <option value="None">None</option>
        </select>
    </li>
    <li>
    	<label for "flsID">FLS Contractor ID</label>999998
    </li>
    <li>
    	<label for "flswhID">FLS Warehouse ID</label>12345
    </li>
    <li>
    	<label for "flsCSP"><b>FLS Computer Security Policy</b></label><br />
    </li>
	<li>
    	<label for "flsCSPrcvd">Received?</label>Yes
    </li>
    <li>
    	<label for "flsCSPdate">Date Entered</label>09/28/2011
    </li>
    <li>
    	<label for "flsCSPby">Entered By</label>gbailey
    </li>
    <li>
	    <div><strong>Click <a href="http://www.flsupport.com/20.html">here</a> for important instructions regarding getting paid for your completed work orders.</strong></div><br />
    </li>
    <li>
    	<label for "hallmarkCert">Cert Hallmark POS?&nbsp;<a href="Training/FLS/Hallmark/Hallmark_Training.php">Edit</a></label>yes
    </li>
    <li>
    	<label for "mauricesCert">Maurices Certification?&nbsp;<a href="Training/FLS/Maurices/Maurices_Training.php">Edit</a></label>yes
    </li>
    <li>
    	<label for "starbucksCert">Starbucks Certification?&nbsp;<a href="Training/FLS/Starbucks/Starbucks_Training.php">Edit</a></label>yes
    </li>
    -->
</ol>

</fieldset>

<div class="verdana2bold" align="center">&nbsp;</div><br />
<br />
</div>      
<div class="formBoxFooter">
<div><input id="cancel" name="cancel" value="Cancel" class="link_button" type="button" /><input id="updateCert" name="updateCert" value="Update" class="link_button" type="submit" /></div>
</div>
 </form>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>

<?php

} else {


} ?>
