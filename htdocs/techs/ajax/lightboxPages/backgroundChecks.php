<?php
require_once(dirname(__FILE__) . "/../../../headerSimple.php");
require_once(dirname(__FILE__) . '/../../../../includes/modules/common.init.php');
$db = Core_Database::getInstance();
$select = $db->select();
$select->from(Core_Database::TABLE_TECH_BANK_INFO, array('TechID', 'Firstname', 'Lastname', 'PrimaryEmail', 'PrimaryPhone', 'Zipcode'));
$select->where('TechID = ?', $_SESSION['TechID']);
$info = $db->fetchRow($select);
if (!$info)
    die();
foreach ($info as $k => $v)
{
	$info[$k] = htmlentities($v);
}
?>
<div style='text-align: center; font-weight: bold'>Background Check / Drug Test Instructions</div><br/>
<div>Did you know that Background Checks are required for 50% of all FieldSolutions work?!</div><br/>
<div>FieldSolutions contracts with Blue Ribbon Techs (<a href="http://www.blueribbontechs.com" target="_blank">www.blueribbontechs.com</a>), the &quot;One-And-Done&quot; shop for online Technician Credentials, to conduct and maintain our technicians' Background Checks, Drug Tests, and other certifications. Blue Ribbon Techs is a new company acting in the interest of independent contractor technicians. Their mission is to serve you and address all of your portal and customer credential needs.</div><br/>
<div>When you go through Blue Ribbon Techs for your Background Checks and Drug Tests, you only pay once, and Blue Ribbon Techs stores your credentials for clients. Because FieldSolutions is Blue Ribbon Techs' client, we will receive and display your credentials in your FieldSolutions Technician Profile once you authorize us to see the results.</div><br/>
<div><b>To add your credentials from Blue Ribbon Techs to your FieldSolutions profile you will want to add us as an authorized Client. To do this log in to Blue Ribbon Techs, click on Profile, then the clients tab, add our company name (FieldSolutions), and your FieldSolutions Tech ID# (NOT your FLS ID#).</b></div><br/>
<div>To get started, simply click the button below, or go directly to <a href="http://www.blueribbontechs.com" target="_blank">www.blueribbontechs.com</a> register with Blue Ribbon Techs. Once you are register you can request a Background Check or other certifications.</div><br/>
<div>Blue Ribbon Techs charges techs the same low rate of $19.00 for a basic Silver Level Background Checks - BUT: you only pay Blue Ribbon Techs once and you're done with expenses for this service.</div><br/>
<div>We also encourage all techs to complete the Blue Ribbon Techs Boot Camp. BRT Boot Camp is a professional training program and it is FREE for a tech to take. It helps prepare techs for doing contract work and sharpen soft skills which are important to FieldSolutions and other clients.</div><br/>
<div>Start growing your business today! </div><br/>
<form id='BRTForm' action='https://blueribbontechs.com/incoming' method='post' target="_blank">
    <div style='text-align:center'><input id='getBGCheckBtn' class='link_button wide_button220' type='submit' value='Get a Background Check or Drug Test' onclick=''></div>
<input type='hidden' id='BRTTechID' name='TechID'
           value='<?= $_SESSION['TechID'] ?>'/>
    <input type='hidden' id='BRTFirstname' name='Firstname' value='<?= $info['Firstname'] ?>'/>
    <input type='hidden' id='BRTLastname' name='Lastname' value='<?= $info['Lastname'] ?>'/>
    <input type='hidden' id='BRTPrimaryEmail' name='PrimaryEmail' value='<?= $info['PrimaryEmail'] ?>'/>
    <input type='hidden' id='BRTPrimaryPhone' name='PrimaryPhone' value='<?= $info['PrimaryPhone'] ?>'/>
    <input type='hidden' id='BRTZipcode' name='Zipcode' value='<?= $info['Zipcode'] ?>'/></form>
<script>
    jQuery("#getBGCheckBtn").click(function(){
        if(typeof(window.parent) != 'undefined'){
            if(typeof(window.parent.closebgCheckLinkbox) == 'function'){
                window.parent.closebgCheckLinkbox("");
            }
        }
    });
</script>
