<?php $page = 'techs'; ?>
<?php $option = 'backgroundChecks'; ?>
<?php $display = 'yes'; ?>
<?php $selected = 'backgroundChecks'; ?>
<?php require ("../../../header.php"); ?>

<style type="text/css">
#mainContainer{
	min-width: 800px;
	width: 850px;
}
</style>

<script type="text/javascript">
function waitTechInfo() {
	try {
		a = techInfo;
		showTechInfo();
	} catch (e) {
		setTimeout("waitTechInfo()", 1000);
	}
}

function showTechInfo() {
	$("#TechID").html(techInfo["TechID"]);
	$("#Firstname").html(techInfo["Firstname"]);
	$("#BgFirstName").val(techInfo["Firstname"]);
	$("#Lastname").html(techInfo["Lastname"]);
	$("#BgLastName").val(techInfo["Lastname"]);
	$("#PrimaryEmail").html(techInfo["PrimaryEmail"]);
	$("#BgPrimaryEmail").val(techInfo["PrimaryEmail"]);
}

$(document).ready(function () {
	waitTechInfo();
	$('#backgroundRequestForm').submit(function(event) {
		event.preventDefault();
		
		var submitData = $(this).serializeArray();
		
		$.ajax({
			type: "POST",
			url: "/techs/ajax/saveBackgroundCheckRequest.php",
	        dataType    : 'json',
	        cache       : false,
			data: submitData,
			success: function (data) {
					var messageHtml = "<h2>Update Complete</h2>";
					messageHtml += "<p>Your request for a background check has been submitted.</p>";
					messageHtml += "<b>Thank you!</b>";
					$("#pageContent").html(messageHtml);
			}
		});
	});
});

</script>

<div id="pageContent">
<table cellspacing="0" style="border: 2px solid rgb(3, 45, 95); border-collapse: collapse; margin-right:auto; margin-left:auto;">
<tbody>
<form id="backgroundRequestForm" name="backgroundRequestForm" method="post" action="" style="margin: 0px;">
	<input type="hidden" name="techID" value="<?=$_SESSION['TechID'] ?>" />
	<input type="hidden" name="Bg_Test_ReqDate_Lite" value="<?php echo Date('Y-m-d H:i:s'); ?>" />
	<input type="hidden" name="BgFirstName" id="BgFirstName" value="<?=$_SESSION["UserObj"][0]["FirstName"]?>" />
	<input type="hidden" name="BgLastName" id="BgLastName" value="<?=$_SESSION["UserObj"][0]["LastName"]?>" />
	<input type="hidden" name="BgPrimaryEmail" id="BgPrimaryEmail" value="<?=$_SESSION["UserObj"][0]["PrimaryEmail"]?>" />

	<tr style="padding: 7px;">
		<td style="text-align: left; vertical-align: top; padding: 1px; color: rgb(42, 73, 125); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold;" colspan="4">
			<div style="font-weight: bold; color: #FFF; font-size: 12px; padding-top: 5px; padding-bottom: 5px; margin-left: 2px; text-align: left; background-color: rgb(16, 8, 90);">Background Check Request</div>
		</td>
	</tr>
	<tr>
		<td style="text-align: left; vertical-align: top; padding: 0px;" colspan="4">
			<table cellspacing="0" style="width: 100%; height: 100%;">
				<tbody>
					<tr>
						<td style="text-align: left; vertical-align: top; width: auto; white-space: nowrap; padding: 2px 5px; color: rgb(3, 45, 95); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal;">FS Tech ID</td>
					</tr>
					<tr>
						<td style="text-align: left; vertical-align: top; width: auto; padding: 5px 10px; white-space: normal;">
							<span style="color: rgb(0, 0, 0); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal;" id="TechID"></span>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr style="padding: 7px;">
		<td style="text-align: left; vertical-align: top; padding: 0px;" colspan="2">
			<table cellspacing="0" style="width: 100%; height: 100%;">
				<tbody>
					<tr>
						<td style="text-align: left; vertical-align: top; width: auto; white-space: nowrap; padding: 2px 5px; color: rgb(3, 45, 95); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal;">First Name</td>
					</tr>
					<tr>
						<td style="text-align: left; vertical-align: top; width: auto; padding: 5px 10px; white-space: normal;"><span style="color: rgb(0, 0, 0); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal;" id="Firstname"></span></td>
					</tr>
				</tbody>
			</table>
		</td>
		<td style="text-align: left; vertical-align: top; padding: 0px;" colspan="2">
			<table cellspacing="0" style="width: 100%; height: 100%;">
				<tbody>
					<tr>
						<td style="text-align: left; vertical-align: top; width: auto; white-space: nowrap; padding: 2px 5px; color: rgb(3, 45, 95); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal;">Last Name</td>
					</tr>
					<tr>
						<td style="text-align: left; vertical-align: top; width: auto; padding: 5px 10px; white-space: normal;">
							<span style="color: rgb(0, 0, 0); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal;" id="Lastname"></span>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
		</tr>
		<tr>
			<td style="text-align: left; vertical-align: top; padding: 1px; color: rgb(42, 73, 125); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold;" colspan="4"><hr></td>
		</tr>
		<tr style="padding: 7px;">
			<td style="text-align: left; vertical-align: top; padding: 0px;" colspan="2">
				<table cellspacing="0" style="width: 100%; height: 100%;">
					<tbody>
						<tr>
							<td style="text-align: left; vertical-align: top; width: auto; white-space: nowrap; padding: 2px 5px; color: rgb(3, 45, 95); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal;">Primary Email</td>
						</tr>
						<tr>
							<td style="text-align: left; vertical-align: top; width: auto; padding: 5px 10px; white-space: normal;">
								<span style="color: rgb(0, 0, 0); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal;" id="PrimaryEmail"></span>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
			<td style="text-align: left; vertical-align: top; padding: 0px;" colspan="2">
				<table cellspacing="0" style="width: 100%; height: 100%;">
					<tbody>
						<tr>
							<td style="text-align: left; vertical-align: top; width: auto; white-space: nowrap; padding: 2px 5px; color: rgb(3, 45, 95); font-size: 10px; font-family: Verdana; font-style: normal; font-weight: bold;">
								<label for="cbParamVirtual1">&nbsp;<span style="color: rgb(255, 0, 0); font-size: 12px; font-family: Verdana; margin-left: 2px;">* Required</span></label>
							</td>
						</tr>
						<tr>
							<td style="text-align: left; vertical-align: top; padding: 2px 5px; white-space: normal;">
								<span style="color: rgb(0, 0, 0); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal;">
									<input type="checkbox" value="Confirmed" name="cbParamVirtual1" id="cbParamVirtual1"> This is the correct email address
									<input type="hidden" value="Confirmed" name="confirmEmail">
								</span>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td style="text-align: left; vertical-align: top; padding: 1px; color: rgb(42, 73, 125); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold;" colspan="4">(Please use this email address when sending your payment via PayPal)   <hr></td>
		</tr>
		<tr style="padding: 7px;">
			<td style="text-align: left; vertical-align: top; width: auto; white-space: nowrap; padding: 2px 5px; color: rgb(3, 45, 95); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal;"><label for="EditRecordBg_Test_Req_Lite">I request a Basic Background Check</label></td>
			<td style="text-align: left; vertical-align: top; padding: 2px 5px; white-space: normal;">
				<span style="color: rgb(0, 0, 0); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal;">
					<input type="checkbox" value="1" name="Bg_Test_Req_Lite" id="Bg_Test_Req_Lite"> 
					<input type="hidden" value="<?=date("Y-m-d H:i:s");?>" name="Bg_Test_ReqDate_Lite">
				</span>
			</td>
			<td style="text-align: left; vertical-align: top; width: auto; white-space: nowrap; padding: 2px 5px; color: rgb(3, 45, 95); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal;">$19 (see instructions if NY)</td>
			<td style="text-align: left; vertical-align: top; width: auto; padding: 5px 10px; white-space: normal;">
				<span style="color: rgb(0, 0, 0); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal;">&nbsp;</span>
			</td>
		</tr>
		<tr>
			<td style="padding: 5px 0px; text-align: center; vertical-align: middle; border: 2px solid rgb(3, 45, 95); background-color: rgb(80, 145, 203);" colspan="4">
				<input type="submit" value="Update" onmouseout="cbButtonHover(this,'color: #ffffff; font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold; text-align: center; vertical-align: middle; border-color: #5091CB; border-style: solid; border-width: 1px; background-color: #032D5F;  width: auto; height: auto; margin: 0 3px;');" onmouseover="cbButtonHover(this,'color: #FFFFFF; font-size: 12px; font-style: normal; font-weight: bold; text-align: center; font-family: Verdana; vertical-align: middle; border-style:solid; border-width: 1px; border-color: #032D5F; background-color: #083194;  width: auto; height: auto; margin: 0 3px;');" style="color: rgb(255, 255, 255); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold; text-align: center; vertical-align: middle; border: 1px solid rgb(80, 145, 203); background-color: rgb(3, 45, 95); width: auto; height: auto; margin: 0pt 3px;" name="Mod0EditRecord" id="Mod0EditRecord">
			</td>
		</tr>
	</form>
	</tbody>
</table>
</div>