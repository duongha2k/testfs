<?php require ("../../../header.php"); ?>
<?php  require_once ("../../../headerStartSession.php"); ?>
<script type="text/javascript" src="/widgets/js/jquery.validate.js"></script>
<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
<script type="text/javascript">
$(document).ready(function(){

	$(document).ready(function(){
		$("body").css("width", "850px").css("min-width", "850px");
		
	});

	jQuery.validator.addMethod("password", function( value, element ) {
		var result = this.optional(element) || value.length >= 6 && /\d/.test(value) && /[a-z]/i.test(value);
		if (!result) {
			element.value = "";
			var validator = this;
			setTimeout(function() {
				validator.blockFocusCleanup = true;
				element.focus();
				validator.blockFocusCleanup = false;
			}, 1);
		}
		return result;
	}, "Your password must be at least 6 characters long and contain at least one number and one character.");
	
	
	jQuery.validator.messages.required = "";
	
	$("#hpCertForm").validate({
		invalidHandler: function(e, validator) {
			var errors = validator.numberOfInvalids();
		
			if (errors) {
				var message = errors == 1
					? 'You missed 1 field. It has been highlighted below'
					: 'You missed ' + errors + ' fields.  They have been highlighted below';
				$("div.error span").html(message);
				$("div.error").show();
			} else {
				$("div.error").hide();
			}
		},
		onkeyup: false,
		submitHandler: function(form) {
			$("div.error").hide();
			showLoader();
			ajaxRequest(form);
		},
		
		errorPlacement: function(error,element){
             error.appendTo(element.parent().parent().next().find("div.status"));
		},
		errorClass: "invalid",
		debug:true
	});

  	function ajaxRequest(form){
		if($("#HP_CertProof").val() != ""){
			
			$.ajaxFileUpload({
		        url             : "/techs/ajax/submitHpCertInfo.php?techID="+<?=$_SESSION['TechID']?>,
		        secureuri       : false,
		        fileElementId   : 'HP_CertProof',
		        dataType        : 'json',
		        cache           : false,
		        success         : function (data, status, xhr) {
					var submitData = $(form).serialize();
					
				  	$.ajax({
						type: "POST",
						url: "/techs/ajax/submitHpCertInfo.php",
				        dataType    : 'json',
				        cache       : false,
						data: submitData,
						success: function (data) {
						
				  			var messageHtml = "<h2>Your Hewlett – Packard/HP Cert Info has been submitted successfully.</h2>";
							messageHtml += "<b>Thank you!</b>";
							$("#hpContent").html(messageHtml);
								
							hideLoader();
						}
					});
		        },
		        error:function(data, status, xhr){;
		        	var messageHtml = "<h2 style='color: #FF0000;'>W9 Error</h2>";
					messageHtml += "<p>There was a problem updating your Hewlett – Packard/HP Cert info. Please close this window and try again.</p> ";
					$("#hpContent").html(messageHtml);
					
					hideLoader();

		        }
		    });

		}else{
	  	var submitData = $(form).serialize();
	  	
			$.ajax({
				type: "POST",
				url: "/techs/ajax/submitHpCertInfo.php",
		        dataType    : 'json',
		        cache       : false,
				data: submitData,
				success: function (data) {
					
					var onClosed = "";
					if(data.success == "success"){
						var messageHtml = "<h2>Your Hewlett – Packard/HP Cert Info has been submitted successfully.</h2>";
						messageHtml += "<b>Thank you!</b>";
						$("#hpContent").html(messageHtml);
						
					}else{
						var messageHtml = "<h2 style='color: #FF0000;'>W9 Error</h2>";
						messageHtml += "<p>There was a problem updating your Hewlett – Packard/HP Cert info. Please close this window and try again.</p> ";
						$("#hpContent").html(messageHtml);
					}
						hideLoader();
				}
			});
  		}
  	}

	 var loaderVisible = false;
	  function showLoader(){
		  if(!loaderVisible){
			  $("div#loader").fadeIn("fast");
			  loaderVisible = true;
		  }
	  }
	  
	  function hideLoader(){
		  if(loaderVisible){
			  var loader = $("div#loader");
			  loader.stop();
			  loader.fadeOut("fast");
			  loaderVisible = false;
		  }
	  }


});
</script>

<style type="text/css">
#mainContainer{
	min-width: 800px;
	width: 850px;
}
.required{
	padding: 0;
}

*{
	margin: 0;
	padding: 0;
	border: none;
	text-decoration: none;
	outline: none;
}
#wr{
	margin: 20px auto;
	width: 800px;
	text-align: left;
}
#acc dt, #acc dd{
	/*width: 100%;*/
}
.sectionHeading{
	cursor: pointer;
	display: block;
	padding: 3px 10px;
	background: #E1E1E1;
	color: #000;
	border: solid 1px #666;
	height: 20px;
	font-weight: bold;
	font-size: 14px;
	text-align:left;
}

#acc dt.act{
	background: #E1E1E!;
}

#acc dd{
	/*display: none;*/
	padding: 3px 10px;
}

.label{
	text-align: right;

}

td.field input.invalid, td.field select.invalid, tr.errorRow td.field input, tr.errorRow td.field select {
    background-color: #FFFFD5;
    border: 2px solid red;
    color: red;
    margin: 0;
}
tr td.field div.formError {
    color: #FF0000;
    display: none;
}
tr.errorRow td.field div.formError {
    display: block;
    font-weight: normal;
}
div.invalid, label.invalid {
    color: red;
}
div.invalid a {
    color: #336699;
    font-size: 12px;
    text-decoration: underline;
}
#section{
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 1px solid #000;
}
#reqStar{
	color: rgb(255, 0, 0); 
	font-size: 12px; 
	font-family: Verdana; 
	margin-left: 2px;
}

.round {
    border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; 
} 


.required{
	color: #000;
}
div.status{
	text-align: right;
	margin-right: 13px;
}


div#loader{
	display: none;
    width:100px;
    height: 100px;
    position: fixed;
    top: 50%;
    left: 50%;
    background:url(/widgets/images/loader.gif) no-repeat center #fff;
    text-align:center;
    padding:10px;
    font:normal 16px Tahoma, Geneva, sans-serif;
    border:1px solid #666;
    margin-left: -50px;
    margin-top: -50px;
    z-index:2;
    overflow: auto;
}

#W9Agreement p{
margin: 0 80px 0 80px;
}

a, form a{
font-size: 12px;
}

#tableMain{
margin-left: auto; 
margin-right: auto; 
line-height: 2;
table-layout: fixed;
}

#tableMain td{
	width: 50%;
}

.sectionHeader{
border-bottom: 1px solid #000; 
margin-bottom: 5px;
}
</style>

<div id="loader">
	Loading...
</div>
<div align="center" id="hpContent">
<table style="width: 500px;">
<tr>
<td><h2>Hewlett &ndash; Packard / HP Cert</h2>
	    <p>Proof of an HP (Hewlett-Packard) Certificate (the actual document, uploaded below) is needed to be eligible for ALL Warranty Service Work Orders through Field Solutions.&nbsp; Update the areas below with the certification information that&nbsp; reflects&nbsp; the certification you have.&nbsp;</p>
<p>PLEASE READ:&nbsp; On July 1, 2010: Exam HP2-H08 will be required for all new technicians the want to become APS Desktop, Notebook and Workstation certified.&nbsp; This test replaces the HP2-Q01.&nbsp;</p>
<p> Technicians that passed the HP2-Q01 prior to July 1st, do NOT need to take the new exam until notified by HP to update their certification.&nbsp;</p>

<form id="hpCertForm" name="hpCertForm">
<input type="hidden" name="techID" value="<?=$_SESSION['TechID']?>">
<table cellspacing="0" style="border: 2px solid rgb(3, 45, 95); border-collapse: collapse;">
<tbody>
	<tr>
		<td colspan=2><div class="error" style="display: none;"><span></span></div></td>
	</tr>
	<tr style="background-color: rgb(255, 255, 255); padding: 7px;">
	<td style="text-align: left; vertical-align: top; padding: 0px;" colspan="2">
		<table cellspacing="0" style="width: 100%; height: 100%;">
		<tbody>
			<tr>
				<td style="text-align: left; vertical-align: top; padding: 1px; color: rgb(42, 73, 125); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold;" colspan="2">HP Certified?</td>
				<td style="text-align: left; vertical-align: top; padding: 2px 5px; white-space: normal;" colspan="2">
					<select style="color: rgb(0, 0, 0); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal;" size="1" name="HP_CertNum" id="HP_CertNum">
						<option selected="selected" value="">Not Certified</option>
						<option value="HP Cert # HP-Q01">HP Cert # HP-Q01</option>
						<option value="HP Cert # HP-H08">HP Cert # HP-H08</option>
					</select>
				</td>
			</tr>
		</tbody>
		</table>
	</td>
	</tr>
	<tr style="background-color: rgb(255, 255, 255);">
	<td style="text-align: left; vertical-align: top; padding: 0px;" colspan="2">
		<table cellspacing="0" style="width: 100%; height: 100%;">
		<tbody>
			<tr>
				<td style="text-align: left; vertical-align: top; padding: 1px; color: rgb(42, 73, 125); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold;" colspan="2">Upload Proof of Certification</td>
				<td style="text-align: left; vertical-align: top; padding: 2px 5px; white-space: normal;" colspan="2">
					<a onmouseout="cbLinkHover(this,'color: #5496C6; font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal; text-decoration: underline;');" onmouseover="cbLinkHover(this,'color: #10085A; font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal; text-decoration: underline;');" style="color: rgb(84, 150, 198); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal; text-decoration: underline;" href="https://bridge.caspio.net/dpImages.aspx?appkey=193b00002ea6d5ff5afb4712ac39&amp;fileID=0"></a>
						<input type="file" style="color: rgb(0, 0, 0); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal; border: 1px solid rgb(0, 0, 0);" name="HP_CertProof" size="20" id="HP_CertProof">
				</td>
			</tr>
		</tbody>
		</table>
	</td>
	</tr>
	<tr style="background-color: rgb(255, 255, 255); padding: 7px;">
		<td style="text-align: left; vertical-align: top; padding: 1px; color: rgb(42, 73, 125); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold;" colspan="2">
			<div style="font-style: normal; font-weight: normal; color: rgb(0, 0, 0);">
				<b>What is Proof of Certification?<br></b>A screen print is required to prove you have obtained valid, up to date certification.  You can upload an image or a document that contains that screenshot using the browse and upload feature below.</div>
				<br>
		</td>
	</tr>
	<tr style="background-color: rgb(255, 255, 255);">
		<td style="text-align: left; vertical-align: top; padding: 1px; color: rgb(42, 73, 125); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold;" colspan="2">
			<div style="font-style: normal; font-weight: normal; color: rgb(0, 0, 0);" id="HP_Cert">
				<b>Don't have your Hewlett Packard/HP Certification?</b><br>
				Get certified by following the steps below:<br>
				<br>
				<b>Step 1:</b> Obtain your learner IDs and enter them:<br>
				<a target="_blank" href="/techs/Training/HP_CertTraining_2010.pdf"><u><b>(Download the Field Solutions Instructions for Training Setup)</b></u></a>)
			</div>
		</td>
	</tr>
	<tr style="background-color: rgb(255, 255, 255); padding: 7px;">
		<td style="text-align: left; vertical-align: top; width: auto; white-space: nowrap; padding: 2px 5px; color: rgb(3, 45, 95); font-size: 10px; font-family: Verdana; font-style: normal; font-weight: bold;">
			<label for="HP_LearnerID">HP Learner ID #<span style="color: rgb(255, 0, 0); font-size: 12px; font-family: Verdana; margin-left: 2px;">* Required</span>
			</label>
		</td>
		<td style="text-align: left; vertical-align: top; padding: 2px 5px; white-space: normal;">
			<input type="text" class="required"  id="HP_LearnerID" name="HP_LearnerID" maxlength="255" size="15" />
		</td>
	</tr>
	<tr style="background-color: rgb(255, 255, 255);">
		<td style="text-align: left; vertical-align: top; width: auto; white-space: nowrap; padding: 2px 5px; color: rgb(3, 45, 95); font-size: 10px; font-family: Verdana; font-style: normal; font-weight: bold;">
			<label for="HP_Prometric_ID">Prometric Learner ID #<span style="color: rgb(255, 0, 0); font-size: 12px; font-family: Verdana; margin-left: 2px;">* Required</span>
			</label>
		</td>
		<td style="text-align: left; vertical-align: top; padding: 2px 5px; white-space: normal;">
			<input type="text" class="required"  id="HP_Prometric_ID" name="HP_Prometric_ID" maxlength="255" size="15"  />
		</td>
	</tr>
	<tr style="background-color: rgb(255, 255, 255); padding: 7px;">
		<td style="padding: 5px 0px; text-align: center; vertical-align: middle; border: 2px solid rgb(3, 45, 95); background-color: rgb(80, 145, 203);" colspan="2">
			<input type="submit" value="Update"  style="color: rgb(255, 255, 255); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold; text-align: center; vertical-align: middle; border: 1px solid rgb(80, 145, 203); background-color: rgb(3, 45, 95); width: auto; height: auto; margin: 0pt 3px;" name="Mod0EditRecord" id="Mod0EditRecord">
		</td>
	</tr>
</tbody>
</table>
</form>
<p><strong>Step 2:</strong>&nbsp; <a target="_BLANK" href="/techs/Training/HP_Learning_Center.pdf">Download HP Training Instructions</a><br>
        <em><strong>Note: HP Charges $75.00 to buy this certificate. This is NOT paid to FieldSolutions. It is paid by credit card on the HP training site.</strong></em><strong></strong></p>
        
<p><strong>Step 3:</strong>&nbsp; Complete Certification Steps
</p>

<p><strong>Step 4:</strong>&nbsp; Enter your certification information in the box above <strong>(For HP Certifications obtained after July 1, 2010&nbsp; (HP Cert # HP2-HO8 )</strong></p>
</td>
</tr>
</table>
</div>