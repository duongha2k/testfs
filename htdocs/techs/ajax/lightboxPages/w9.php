<?php $page = techs; ?>
<?php $option = profile; ?>
<?php   require_once ("../../../headerSimple.php");
require_once dirname(__FILE__).'/../../../../includes/modules/common.init.php';
$common = new Core_Api_CommonClass;
$statesOptions = $common->getStatesArray('US');

$statesHtml = "<option value=\"\">Select State</option>";
foreach ($statesOptions->data as $code => $name) {
        $statesHtml .= "<option value=\"$code\">$name</option>";
}
?>
<!-- Add Content Here -->
<script type="text/javascript" src="/widgets/js/jquery.validate.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.dateFormat-1.0.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.autotab.js"></script>
<link rel="stylesheet" href="/widgets/css/main.css" type="text/css" />
<link rel="stylesheet" href="/templates/www/main.css" type="text/css" />
<link rel="stylesheet" href="/widgets/css/details.css" type="text/css" />
<link rel="stylesheet" href="/widgets/css/blueprint/screen.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="/widgets/css/blueprint/print.css" type="text/css" media="print" /> 
<link rel="stylesheet" href="/widgets/css/printW9.css" type="text/css" /> 
<script type="text/javascript">

$(document).ready(function(){
	$("body").css("width", "850px").css("min-width", "850px");
});

</script>
<script type="text/javascript">
 $(document).ready(function(){
 	
	jQuery.validator.addMethod("numberCheck", function( value, element ) {
		if(value.substring(0,1) != "X" && isNaN(value) == true){
			return false;
		}else{
			return true;
		}
			
	},"This field must contain numbers only");

	jQuery.validator.addMethod("bizNameCheck", function( value, element ) {
		if($("#BusinessName").val() == "" && $("input[name=FTC]:checked").val() != "Individual/Sole Proprietor"){
			
			$("body").data("bizNameAlertCount", $("body").data("bizNameAlertCount") +1);
			if($("body").data("bizNameAlertCount") <= 1){
				alert("Please enter a Business Name/Disregarded entity name");
				return false;
			}else{
				return true; 
			}
		}else{
			return true;
		}
			
	},"This field must contain numbers only");

	jQuery.validator.addMethod("initialCheck", function( value, element ) {
		var firstLastInit = $("#FirstName").val().substring(0,1).toLowerCase() + $("#LastName").val().substring(0,1).toLowerCase();
		if(firstLastInit != $("#DigitalSignature").val().toLowerCase()){
			return false;
		}else{
			return true;
		}
			
	},"Initials do not match the First and Last Name entered. Please reenter.");
	
	$("#w9Info").validate({
		invalidHandler: function(e, validator) {
			var errors = validator.numberOfInvalids();
		
			if (errors) {
				var message = errors == 1
					? 'You missed 1 field. It has been highlighted below'
					: 'You missed ' + errors + ' fields.  They have been highlighted below';
				$("div.error span").html(message);
				$("div.error").show();
			} else {
				$("div.error").hide();
			}
		},
		onkeyup: false,
		submitHandler: function(form) {
			$("div.error").hide();
			showLoader();
			ajaxRequest(form);
		},
		rules: {
			SSN1:{
				numberCheck: true
			},
			SSN2:{
				numberCheck: true
			},
			SSN3:{
				numberCheck: true
			},
			EIN1:{
				numberCheck: true
			},
			EIN2:{
				numberCheck: true
			},
			DigitalSignature: {
				initialCheck: true
			},
			BusinessName: {
				bizNameCheck: true
			}
		},
		errorPlacement: function(error,element){
             error.appendTo(element.parent().parent().parent().next().find("div.status"));
		},
		errorClass: "invalid",
		debug:true
	});

  function ajaxRequest(form){
	  	var submitData = $(form).serialize();
	  	
			$.ajax({
				type: "POST",
				url: "/techs/ajax/submitW9Info.php",
		        dataType    : 'json',
		        cache       : false,
				data: submitData,
				success: function (data) {
					
					var onClosed = "";
					if(data.success == "success"){
						var messageHtml = "<h2>Your W9 Info has been updated successfully.</h2>";
						messageHtml += "<b>Thank you!</b>";
						$("#w9Content").html(messageHtml);
						setTimeout(function(){
							parent.$.fancybox.close(); 
						},1000);
						parent.location.reload();
					}else{
						var messageHtml = "<h2 style='color: #FF0000;'>W9 Error</h2>";
						messageHtml += "<p>There was a problem updating your W9 info. Please close this window and try again.</p> ";
						$("#w9Content").html(messageHtml);
						setTimeout(function(){
							parent.$.fancybox.close(); 
						},1000);
					}
					hideLoader();
				}
			});
  }
 
  $(".resize").vjustify();
  $("div.buttonSubmit").hoverClass("buttonSubmitHover");
  
  if ($.browser.safari) {
    $("body").addClass("safari");
  }
  
  //$("input.primaryPhone").mask("(999) 999-9999");
 // $("input.zipcode").mask("99999");
  


  // toggle optional billing address
  var subTableDiv = $("div.subTableDiv");
  var toggleCheck = $("input.toggleCheck");
  toggleCheck.is(":checked")
  	? subTableDiv.hide()
	: subTableDiv.show();
  $("input.toggleCheck").click(function() {
      if (this.checked == true) {
        subTableDiv.slideUp("medium");
        $("form").valid();
      } else {
        subTableDiv.slideDown("medium");
      }
  });

	
  $("input[name=TIN]").click(function(){
		if($("input[name=TIN]:checked").attr("id") == "SSNRadio"){
			$("#SSN1, #SSN2, #SSN3").addClass("required");
			$("#EIN1, #EIN2").removeClass("required");
			$(".reqSSN").show();
			$(".reqEIN").hide();
			if($("#EIN1").val() != "") $("body").data("EIN1",$("#EIN1").val() );
			if($("#EIN2").val() != "") $("body").data("EIN2",$("#EIN2").val() );

			if($("body").data("SSN1") != "") $("#SSN1").val($("body").data("SSN1"));
			if($("body").data("SSN2") != "") $("#SSN2").val($("body").data("SSN2"));
			if($("body").data("SSN3") != "") $("#SSN3").val($("body").data("SSN3"));
			$("#EIN1, #EIN2").val("");
		}else if($("input[name=TIN]:checked").attr("id") == "EINRadio"){
			$("#EIN1, #EIN2").addClass("required");
			$(".reqSSN").hide();
			$(".reqEIN").show();
			$("#SSN1, #SSN2, #SSN3").removeClass("required");
			if($("#SSN1").val() != "") $("body").data("SSN1",$("#SSN1").val() );
			if($("#SSN2").val() != "") $("body").data("SSN2",$("#SSN2").val() );
			if($("#SSN3").val() != "") $("body").data("SSN3",$("#SSN3").val() );

			if($("body").data("EIN1") != "") $("#EIN1").val($("body").data("EIN1"));
			if($("body").data("EIN2") != "") $("#EIN2").val($("body").data("EIN2"));
			$("#SSN1, #SSN2, #SSN3").val(""); 
		}
	});
	
  $("#SSN1, #EIN1").keyup(function(e){
		$this = $(e.target);
		if($this.attr("id") == "SSN1" && $("input[name=FTC]:checked").val() != "C Corp"){
			$("#SSNRadio").attr("checked", true);
			$("#SSN1, #SSN2, #SSN3").addClass("required");
			$("#EIN1, #EIN2").removeClass("required");
			$("#EINRadio").removeClass("required");
			$("#EIN1, #EIN2").val("");
		}else{
			$("#EINRadio").attr("checked",true);
			$("#EIN1, #EIN2").addClass("required");
			$("#SSN1, #SSN2, #SSN3").removeClass("required");
			$("#SSNRadio").removeClass("required");
			$("#SSN1, #SSN2, #SSN3").val(""); 
		}
	});

  $("input[name=FTC]").click(function(){
		if($("input[name=FTC]:checked").attr("id") == "FTCOther"){
			$("#FTCOtherDisplay").show();
			$("#FTCOtherInput").addClass("required");
		}else{
			$("#FTCOtherDisplay").hide();
			$("#FTCOtherInput").removeClass("required");
		}

		if($("input[name=FTC]:checked").val() == "C Corp"){
			$("#EIN1, #EIN2").addClass("required");
			$("#SSN1, #SSN2, #SSN3").removeClass("required");
			$(".reqSSN").hide();
			$(".reqEIN").show();
			$("#EINRadio").attr("checked","checked");
			$("#SSN1, #SSN2, #SSN3").val("");
		}else if($("input[name=FTC]:checked").val() == "Individual/Sole Proprietor"){
			$("#SSN1, #SSN2, #SSN3").addClass("required");
			$("#EIN1, #EIN2").removeClass("required");
			$(".reqEIN").hide();
			$(".reqSSN").show();
			$("#SSNRadio").attr("checked","checked");
			$("#EIN1, #EIN2").val("");
		}else{
			$("#SSNRadio").attr("checked","checked");
		}

		if($("input[name=FTC]:checked").val() == "LLC"){
			$("#LLCTax").addClass("required");
		}else{
			$("#LLCTax").removeClass("required");
		}
	});
	 var loaderVisible = false;
	  function showLoader(){
		  if(!loaderVisible){
			  $("div#loader").fadeIn("fast");
			  loaderVisible = true;
		  }
	  }
	  
	  function hideLoader(){
		  if(loaderVisible){
			  var loader = $("div#loader");
			  loader.stop();
			  loader.fadeOut("fast");
			  loaderVisible = false;
		  }
	  }

	  var techID = '<?=$_SESSION['TechID'];?>';
	  
		$.ajax({
			type: "POST",
			url: '/techs/ajax/getW9Info.php',
	        dataType    : 'json',
	        cache       : false,
			data: {id:techID},
			success: function (data) {
				for(index in data.w9Info){
					if(index == "FederalTaxClassification"){
						$('.FTC').each(function(){
							if($(this).val() == data.w9Info[index]){
								$(this).attr("checked",true);
								if(data.w9Info[index] == "Other"){
									$("#FTCOther").click();
									$("#FTCOtherInput").val(data.w9Info['FTCOther']);
								}
							}
						});
					}else if(index == "ExemptPayee"){
						if(data.w9Info[index] == "0"){
							$("#ExemptPayee").attr("checked",false);
						}else{
							$("#ExemptPayee").attr("checked",true);
						}
					}else if(index == "MaskedSSN" && data.w9Info[index] != "" && data.w9Info[index] != null){
						var ssn = data.w9Info[index].toString();
						$("#SSN1").val(ssn.substring(0,3));
						$("#SSN2").val(ssn.substring(3,5));
						$("#SSN3").val(ssn.substring(5,9));
						$("#SSNRadio").click();
					}else if(index == "MaskedEIN" && data.w9Info[index] != "" && data.w9Info[index] != null){
						var ein = data.w9Info[index].toString();
						$("#EIN1").val(ein.substring(0,2));
						$("#EIN2").val(ein.substring(3,9));
						$("#EINRadio").click();
					}else if(index == "DateChanged" && data.w9Info[index] != "" && data.w9Info[index] != null){
						$("#W9LastSubmitted").html('Submitted W-9 on ' + $.format.date(data.w9Info[index], 'MM/dd/yyyy'));
					}else{
						$("#"+index).val(data.w9Info[index]);
					}
				}
			 },
		    error:function(data, status, xhr){
			 }
		});

});

$.fn.vjustify = function() {
    var maxHeight=0;
    $(".resize").css("height","auto");
    this.each(function(){
        if (this.offsetHeight > maxHeight) {
          maxHeight = this.offsetHeight;
        }
    });
    this.each(function(){
        $(this).height(maxHeight);
        if (this.offsetHeight > maxHeight) {
            $(this).height((maxHeight-(this.offsetHeight-maxHeight)));
        }
    });
};

$.fn.hoverClass = function(classname) {
	return this.hover(function() {
		$(this).addClass(classname);
	}, function() {
		$(this).removeClass(classname);
	});
};

</script>
<div id="loader">
	Loading...
</div>

<br /><br />

<div align="center" id="w9Content" class="container">
<form name="w9Info" id="w9Info" method="post" >
<input type="hidden" name="techID" value="<?php echo $_SESSION['TechID']; ?>" />
<input type="hidden" name="id" id="id" value="" />
						<table cellspacing="0" cellpadding="0" width="800" border="0" style="text-align: left">
					<tr><td>		
						<input type="hidden" name="TechID" value="<?php echo $_SESSION['TechID']; ?>" />

						<table>
						<tr><td class="divider"><br /></td></tr>
					
						<tr style="border-bottom: 1px solid #000; margin-bottom: 5px;">
							<td colspan=2 class="sectionHeader">W-9 Tax Information (<a href="http://www.irs.gov/pub/irs-pdf/fw9.pdf?portlet=103" target="_blank">click here for instructions</a>)<span style="font-size: 10px; margin-top: 5px; float: right; color: rgb(255, 0, 0);">This information is encrypted during transmission and is stored on a secure site.</span></td>
						</tr>
						<tr><td class="divider"><br /></td></tr>
						<tr>
							<td><span class="label"><span id="reqStar">*</span>Name (as shown on your income tax return):</span></td>
						</tr>
						<tr>
							<td colspan=2>
								
								<span>First Name</span>
								<span class="field"><input class="required"  type="text" size="20" name="FirstName" id="FirstName" /></span>
								
								<span>MI </span>
								<span class="field"><input  type="text" size="1" name="MI" id="MI" /></span>
								
								<span>Last Name</span>
								<span class="field"><input class="required"  type="text" size="20" name="LastName" id="LastName" /></span>
							</td>
						</tr>
						<tr><td colspan=2><br /><hr /></td></tr>
						<tr>
							<td><span class="label"><span id="reqStar">*</span>Business name/disregarded entity name (if different from above):</span></td>
						</tr>
						<tr>
							<td colspan=2>
								
								<span class="label"> </span>
								<span class="field">
									<input type="text" size="75" name="BusinessName" id="BusinessName" />
								</span>
							</td>
						</tr>
						<tr><td colspan=2 ><br /><hr /></td></tr>
						<tr>
							<td><span class="label"><span id="reqStar">*</span>Check appropriate box for federal tax classification:</span></td>
						</tr>

					<!--  tr>
						<td class="label" style="vertical-align: top;">  </td>
					</tr -->
					<tr>
						<td class="field" colspan=2>
							<table id="FedTaxClass" style="line-height: 20px;">
								<tr>
									<td><input class="required FTC" type="radio" name="FTC" value="Individual/Sole Proprietor" /> Individual/Sole Proprietor</td>
								
									<td><input class="required FTC" type="radio" name="FTC" value="C Corp" /> C Corporation</td>
								
									<td><input class="required FTC" type="radio" name="FTC" value="S Corp" /> S Corporation</td>
								
									<td><input class="required FTC" type="radio" name="FTC" value="Partnership" /> Partnership</td>
								
									<td><input class="required FTC" type="radio" name="FTC" value="Trust/Estate" /> Trust/Estate</td>
								</tr>
								<tr>
									<td><input class="required FTC" type="radio" name="FTC" class="FTC" value="LLC" /> Limited Liability Company. </td>
									
									<td colspan=4>Enter the tax classification (C= C corporation, S=S corporation, P=partnership) &rsaquo; <input type="text" size="5" name="LLCTaxClass" id="LLCTaxClass" /></td>
								
									<td style="padding-left: 20px;"><input type="checkbox" name="ExemptPayee" id="ExemptPayee" value="1" checked/> Exempt Payee</td>
								</tr>
								<tr>
									<td colspan=5><input class="required FTC" type="radio" name="FTC" value="Other" id="FTCOther" /> Other (see instructions)
										<span id="FTCOtherDisplay" style="display:none;">
											<input type="text" size="20" name="FTCOther" id="FTCOtherInput" />
										</span>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan=2 ><br /><hr /></td></tr>
						<tr>
							<td style="width: 65%">
								<table>
									<tr>
										<td><span class="label"><span id="reqStar">*</span>Address (number, street, and apt. or suite no.) </span></td>
									</tr>
									<tr>
										<td colspan=2>
											<span class="field">
												<input class="required" type="text" size="75" name="Address1" id="Address1" />
											</span>
										</td>
									</tr>
									
									<tr>
										<td><span class="label"><span id="reqStar">*</span>City, state, and ZIP code</span></td>
									</tr>
									<tr>
										<td colspan=2 class="field">
											 
											<span class="field">
												<input class="required" type="text" size="20" name="City" id="City" />
											</span>
											<span class="field">
												<select class="required" id="State" name="State" ><?=$statesHtml?></select>
											</span>
											<span class="field">
												<input class="required" type="text" size="20" name="ZipCode" id="ZipCode" />
											</span>
                                            
										</td>
									</tr>
									
									<tr>
										<td>List account number(s) here (optional)</td>
									</tr>
									<tr>
										<td colspan=2>
											<span class="label"> </span>
											<span class="field">
												<input
												  type="text" size="75" name="AccountNumbers" id="AccountNumbers" />
											</span>
										</td>
									</tr>
							</table>
						</td>
						<td style="width: 35%; padding-left: 10px;">
							<table>
								<tr>
									<td>Requester's name and address (optional)</td>
								</tr>
								<tr>
									<td>
										<span class="label"> </span>
										<span class="field">
											<table>
												<tr><td>FieldSolutions</td></tr>
												<tr><td>10400 Yellow Circle Dr. Suite 300</td></tr>
												<tr><td>Minnetonka, MN 55343</td></tr> 
											</table>
										</span>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
					
					<table cellspacing="0" cellpadding="0" width="800" border="0">
						<tr><td colspan=5><br /><hr /></td></tr>
						<tr>
							<td style="font-weight: bold;">
								<span style="padding: 0 10px 0 10px; color: #FFF; background-color: #000;">Part I</span>
								<span>Taxpayer Identification Number (TIN)</span>
							</td>
						</tr>
						<tr><td colspan=5><hr /></td></tr>
						<tr>
							<td style="width: 65%;"> 
								Enter your TIN in the appropriate box. The TIN provided must match the name given on the “Name” line
								to avoid backup withholding. For individuals, this is your social security number (SSN). However, for a
								resident alien, sole proprietor, or disregarded entity, see the Part I instructions on page 3. For other
								entities, it is your employer identification number (EIN). If you do not have a number, see How to get a
								TIN on page 3.
							</td>
							<td style="vertical-align: middle; padding-left: 20px;">
								<table>
									<tr>
										<td class="label" style="text-align: left"><span id="reqStar" class="reqSSN">*</span> Social Security Number: </td>
									</tr>
									<tr>
										<td class="field">
											<input type="radio" class="required" name="TIN" value="SSN" id="SSNRadio" /> 
											<span id="SSNInput"> 
												<input  type="text" size="3" maxlength="3" name="SSN1" id="SSN1" /> - 
												<input  type="text" size="2" maxlength="2" name="SSN2" id="SSN2" /> - 
												<input  type="text" size="4" maxlength="4" name="SSN3" id="SSN3" /> 
											</span>  
										</td>
									</tr>
								</table>
							</td>
						</tr>
						
						<tr>
							<td style="width: 65%;"> 
								Note. If the account is in more than one name, see the chart on page 4 for guidelines on whose
								number to enter.
							</td>
							<td style="vertical-align: middle; padding-left: 20px;">
								<table>
									<tr>
										<td class="label"><span id="reqStar" class="reqEIN">*</span> Employer Identification Number: </td>
									</tr>
									<tr>
										<td class="field">
											<input type="radio" class="required" name="TIN" value="EIN" id="EINRadio" />
											<span id="EINInput">
												<input type="text" size="2" maxlength="2" name="EIN1" id="EIN1" /> - 
												<input type="text" size="7" maxlength="7" name="EIN2" id="EIN2" /> 
											</span> 
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					
					<table cellspacing="0" cellpadding="0" width="800" border="0">
						<tr><td colspan=5><br /><hr /></td></tr>
						<tr>
							<td style="font-weight: bold;">
								<span style="padding: 0 10px 0 10px; color: #FFF; background-color: #000;">Part II</span>
								<span>Certification</span>
							</td>
						</tr>
						<tr><td colspan=5><hr /></td></tr>
						<tr>
							<td colspan=5 id="W9Agreement"> 
								Under penalties of perjury, I certify that:<br /><br />
								1. The number shown on this form is my correct taxpayer identification number (or I am waiting for a number to be issued to me), and<br /><br />
								
								2. I am not subject to backup withholding because: (a) I am exempt from backup withholding, or (b) I have not been notified by the Internal Revenue
								Service (IRS) that I am subject to backup withholding as a result of a failure to report all interest or dividends, or (c) the IRS has notified me that I am
								no longer subject to backup withholding, and<br /><br />
								
								3. I am a U.S. citizen or other U.S. person (defined below).<br /><br />
								
								<b>Certification instructions</b> You must cross out item 2 above if you have been notified by the IRS that you are currently subject to backup withholding
								because you have failed to report all interest and dividends on your tax return. For real estate transactions, item 2 does not apply. For mortgage
								interest paid, acquisition or abandonment of secured property, cancellation of debt, contributions to an individual retirement arrangement (IRA), and
								generally, payments other than interest and dividends, you are not required to sign the certification, but you must provide your correct TIN. See the
								instructions on page 4.<br />
							</td>
						</tr>
						<tr>
							<td style="border-top: 1px solid #000; border-bottom:1px solid #000;">
								<span class="label" style="border-right:1px solid #000; border-spacing: 0px; font-weight:bold;" >Enter your initials here to Certify the information contained herein, to be used for your IRS Form W-9: </span>
								<span class="field" style="padding-left: 10px;">
									<input class="required" type="text" name="DigitalSignature" id="DigitalSignature" size="2" maxlength="2" />
								</span>
								<span id="reqStar">*</span>
							</td>
						</tr>
						<tr>
							<td colspan=2 align="right"><div class="status"></div></td>
						</tr>
					</table>
                   					
					<table>
					<tr><td><br /></td></tr>
                    <tr class="hidePrint"><td><a href="javascript:printW9()">Print</a> a copy of your W-9 now for your records.  For your protection future views of your online W-9 will not show your SSN.</td></tr>
					<tr>
						<td><span id="reqStar">* Required Fields</span></td><td><input style="height: 22px;" class="link_button middle2_button hidePrint" type="submit" value="Submit" name="W9Submit" /></td>
					</tr>
					</table>	
                    
					<table>
					<tr><td><br/><span id="W9LastSubmitted"></span><br /></td></tr>
					</table>
</form>
</div>
		
<script type="text/javascript">
	function printW9() {
		w9Print = window.open("w9Print.php", "w9Print");
	}
</script>