<?php $page = techs; ?>
<?php $option = profile; ?>
<?php require ("../../../header.php"); ?>
<?php  require_once ("../../../headerStartSession.php"); 
require_once("../../../library/mySQL.php");

if(isset($_POST['FLSRegion'])){
	mysqlFetchAssoc(mysqlQuery("UPDATE TechBankInfo SET FLSRegion = '{$_POST['FLSRegion']}' WHERE TechID = '{$_SESSION['TechID']}'"));
}

$flsData =  mysqlFetchAssoc(mysqlQuery("SELECT FLSID, FLSWhse, FLSCSP_Rec, FLSCSP_RecDate, FLSCSP_RecBy, FLSRegion FROM TechBankInfo WHERE TechID = '{$_SESSION['TechID']}'"));
$flsData = $flsData[0];
$flsData['FLSCSP_Rec'] == "0" ? $flsData['FLSCSP_Rec'] = "No" : $flsData['FLSCSP_Rec'] = "Yes";
if($flsData['FLSCSP_RecDate'] != "") $flsData['FLSCSP_RecDate'] = date("m-d-Y", strtotime($flsData['FLSCSP_RecDate'])); 
?>

<!-- Add Content Here -->
<script type="text/javascript" src="/widgets/js/jquery.validate.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.autotab.js"></script>

<script type="text/javascript">

$(document).ready(function(){
	$("body").css("width", "850px").css("min-width", "850px");

	$("#FLSRegion").val('<?=$flsData['FLSRegion']?>');
	
});

</script>

<style type="text/css">

#mainContainer{
	min-width: 800px;
	width: 850px;
}
	#emailBlast {
		text-align: center;
	}
	.verdana2 {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		clear: both;
	}

	.verdana2bold {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		font-weight: bold;
		clear: both;
	}

	.fieldGroup {
		padding-bottom: 5px;
		border-bottom: 2px solid #2A497D;
	}

	.formBox, .formBoxFooter {
		margin: 15px auto 0px auto;
		padding: 15px;
		width: 500px;
		color: #2A497D;
		font-size: 12px;
		border: 2px solid #2A497D;
	}

	.formBox input {
		font-size: 12px;
		height: 14px;
	}

	.formBoxFooter {
		background-color: #BACBDF;
		text-align: center;
		margin: 0px auto 10px auto;
		padding: 5px 15px;
		border-top: none;
	}
	.formRow3, .formRow4, .formRow5, .formRow6 {
		width: 100%;
		clear: both;
	}
	.formRow3 div {
		width: 33%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow4 div {
		width: 25%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow5 div {
		width: 20%;
		float: left;
		margin-bottom: 5px;
	}

	.formRow6 div {
		width: 16%;
		float: left;
		margin-bottom: 5px;
	}

	.resultsTable {
		width: 80%;
		margin: 10px auto 0px auto;
	}

	.resultsTable thead {
		text-align: center;
		color: #2A497D;
		border: 1px solid #2A497D;
		background-color: #A0A8AA;
		cursor: default;
	}

	.sortAble, #nextPageBtn, #prevPageBtn {
		cursor: pointer;
	}

	.sortAbleSelected {
		cursor: pointer;
		background-color: #FF9900;
		color: #FFFFFF;
		font-weight: bold;
	}

	.resultsTable tfoot {
		text-align: center;
		color: #000000;
		border: 1px solid #2A497D;
		background-color: #A0A8AA;
	}

	.resultsTable td {
		padding: 3px 5px;
	}

	.resultsTable .evenRow {
		background-color: #E9EEF8;
	}

	.resultsTable .oddRow {
		background-color: #FBFCFD;
	}

	.resultsTable .preferred {
		background-color: #FFFF8C;
	}
    .resultsTable .techGrey {
		background-color: #DEDEDE;
	}
    .resultsTable .techYellow {
		background-color: #FFFF8C;
	}
    .resultsTable .techRed {
		background-color: #FFBBBB;
	}
    .resultsTable .techGreen {
		background-color: #89FF89;
	}

	.searchControls {
		margin-top: 10px;
		text-align: center;
	}

	.noResultsDiv {
		margin-top: 10px;
		text-align: center;
	}
	
	label select {margin-right:10px;}
	.link_button { width: 140px; border:none; background: url(/widgets/images/button_middle.png) left top no-repeat; font: normal 10px/10px Verdana, Arial, Helvetica, sans-serif; padding: 4px 0 8px; color: #385c7e; cursor: pointer; }
	
	fieldset {
	  margin: 1.5em 0 0 0;
	  padding: 0;
	}
	legend {
	  margin-left: 1em;
	  color: #000000;
	  font-weight: bold;
	}
	fieldset ol {
	  padding: 1em 1em 0 1em;
	  list-style: none;
	}
	fieldset li {
	  padding-bottom: 1em;
	}	
	fieldset {
	float: left;
	clear: left;
	width: 100%;
	margin: 0 0 1.5em 0; padding: 0;
	}
	label {
  	display: block;
	float: left;
	width: 20em;
	margin-right: 1em;
  	}

</style>



<form id="flsForm" name="flsForm" action="<?php echo $_SERVER['PHP_SELF'] . "?v=" . $_GET["v"]?>&snp=1" method="post">
<div class="formBox">
<fieldset>
<ol>
	
    <div class="verdana2bold">Fujitsu FLS Related</div><br />
    <div>The FLS program/Tech opportunity is focused around running service & project calls performing work on hardware and software in the retail and grocery environments for one of our clients. Getting involved with FLS as a Technician requires some technical knowledge, as you will be replacing defective parts dealing with primarily POS equipment in retail stores (keyboards, check readers, monitors, cash registers/drawers, scanners, etc). </div><br . />
    <div>In order to be activated as a Tech with FLS (First Line Support), you will first need to go through the required online training and submit the test with a passing score. Get more information regarding the FLS program and/or begin the online training process <a href="http://www.flsupport.com/2.html">here </a></div><br />
    <li>
	<label for "flsRegion">What FLS Region can you cover?</label><select name="FLSRegion" id="FLSRegion">
   		<option>Select One</option>
    	<option value="No Region w/in 50 miles">No Region w/in 50 miles</option>
    	<option value="Albany, NY">Albany, NY</option>
    	<option value="Atlanta, GA">Atlanta, GA</option>
    	<option value="Baltimore, MD">Baltimore, MD</option>
    	<option value="Boston, MA">Boston, MA</option>
    	<option value="Buffalo, NY">Buffalo, NY</option>
    	<option value="Charlotte, NC">Charlotte, NC</option>
    	<option value="Chicago, IL">Chicago, IL</option>
    	<option value="Cincinnati, OH">Cincinnati, OH</option>
    	<option value="Colorado Springs, CO">Colorado Springs, CO</option>
    	<option value="Columbus, OH">Columbus, OH</option>
    	<option value="Dallas, TX">Dallas, TX</option>
    	<option value="Daytona Beach, FL">Daytona Beach, FL</option>
    	<option value="Denver, CO">Denver, CO</option>
    	<option value="Detroit, MI">Detroit, MI</option>
    	<option value="Erie, PA">Erie, PA</option>
    	<option value="Greensboro, NC">Greensboro, NC</option>
    	<option value="Greenville, SC">Greenville, SC</option>
    	<option value="Hartford, CT">Hartford, CT</option>
    	<option value="Houston, TX">Houston, TX</option>
    	<option value="Jackson, MS">Jackson, MS</option>
    	<option value="Las Vegas, NV">Las Vegas, NV</option>
    	<option value="Los Angeles, CA">Los Angeles, CA</option>
    	<option value="Lynchburg, VA">Lynchburg, VA</option>
    	<option value="Manchester, NH">Manchester, NH</option>
    	<option value="New Orleans, LA">New Orleans, LA</option>
    	<option value="New York, NY">New York, NY</option>
    	<option value="Newark, NJ">Newark, NJ</option>
    	<option value="Newburgh, NY">Newburgh, NY</option>
    	<option value="Orlando, FL">Orlando, FL</option>
    	<option value="Ottawa, ON">Ottawa, ON</option>
    	<option value="Philadelphia, PA">Philadelphia, PA</option>
    	<option value="Phoenix, AZ">Phoenix, AZ</option>
    	<option value="Pittsburgh, PA">Pittsburgh, PA</option>
    	<option value="Portland, ME">Portland, ME</option>
    	<option value="Portland, OR">Portland, OR</option>
    	<option value="Providence, RI">Providence, RI</option>
    	<option value="Raleigh, NC">Raleigh, NC</option>
    	<option value="Roanoke, VA">Roanoke, VA</option>
    	<option value="San Francisco, CA">San Francisco, CA</option>
    	<option value="San Juan, PR">San Juan, PR</option>
    	<option value="Seattle, WA">Seattle, WA</option>
    	<option value="Springfield, IL">Springfield, IL</option>
    	<option value="St. Louis, MO">St. Louis, MO</option>
    	<option value="Toledo, OH">Toledo, OH</option>
    	<option value="Washington, DC">Washington, DC</option>
        </select>
    </li>
    <li>
    	<label for "FLSID">FLS Contractor ID</label><span id="FLSID"><?=$flsData['FLSID']?></span>
    </li>
    <li>
    	<label for "FLSWhse">FLS Warehouse ID</label><span id="FLSWhse"><?=$flsData['FLSWhse']?></span>
    </li>
    <li>
    	<label for "flsCSP"><b>FLS Computer Security Policy</b></label><br />
    </li>
	<li>
    	<label for "FLSCSP_Rec">Received?</label><span id="FLSCSP_Rec"><?=$flsData['FLSCSP_Rec']?></span>
    </li>
    <li>
    	<label for "FLSCSP_RecDate">Date Entered</label><span id="FLSCSP_RecDate"><?=$flsData['FLSCSP_RecDate']?></span>
    </li>
    <li>
    	<label for "flsCSPby">Entered By</label><span id="FLSCSP_RecBy"><?=$flsData['FLSCSP_RecBy']?></span>
    </li>
    <li>
	    <div><strong>Click <a href="http://www.flsupport.com/20.html">here</a> for important instructions regarding getting paid for your completed work orders.</strong></div><br />
    </li>
</ol>

</fieldset>

<div class="verdana2bold" align="center">&nbsp;</div><br />
<br />
</div>      
<div class="formBoxFooter">
<div><input id="cancel" name="cancel" value="Cancel" class="link_button" type="button" /><input id="updateCert" name="updateCert" value="Update" class="link_button" type="submit" /></div>
</div>
 </form>
</div>

<p>&nbsp;</p>
<p>&nbsp;</p>
