<?php
/**
 * @author Alex Scherba
 */

if ($_SERVER['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest'){
	require_once("../../library/caspioAPI.php");
	
	$projectID = (int)$_GET['projectID'];
	$fields = "Project_Manager, Project_Manager_Phone, Project_Manager_Email, EmergencyName, "
	         ."EmergencyPhone, EmergencyEmail, TechnicalSupportName, TechnicalSupportPhone, "
	         ."TechnicalSupportEmail, CheckInOutName, CheckInOutNumber, CheckInOutEmail";
	         
	$records = caspioSelectAdv("TR_Client_Projects", $fields , "Project_ID = '".$projectID."'", "", false, "`", "|",false);
	
	if (isset($records[0])) {
		header ("Content-type: text/xml");
	
	    $projectContactInfo = explode("|", $records[0]);
	 
	    $result = '<?xml version="1.0" encoding="utf-8" ?><records>';
	    if (is_array($projectContactInfo))
	    {
	        foreach ($projectContactInfo as $key=>$val) {
	        	$val = trim($val, "`");
	        	$val = strip_tags($val);
	            $result .= '<param_'.$key.'>'. $val . '</param_'.$key.'>';
	        }
	    }
	    $result .= '</records>';
	    echo $result;
	}
}