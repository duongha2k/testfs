<?php
if ($_SERVER['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest') {
	require_once("{$_SERVER['DOCUMENT_ROOT']}/library/woapi.php");
    require ("{$_SERVER['DOCUMENT_ROOT']}/headerStartSession.php");
	
	try{
			if(!empty($_FILES)){
				if(isset($_GET['vehicleDesc']) && $_GET['vehicleDesc'] != "") $_FILES['description'] = $_GET['vehicleDesc'];
           		$result = Core_Tech::uploadTechFileAWS($_FILES, $_GET['techID']);
			}
			
			if(!empty($_POST)){
           		$result = Core_Tech::saveEquipment(json_decode($_POST['data']), $_POST['techID'], $_POST['otherTools'], json_decode($_POST['telephonyData']));
			}
    		
            if($result){
           		$success = "success";
            }else{
                $success = "error";
            }
	}catch(Exception $e){
//		print_r($e);
	}
	
    echo json_encode(array('success'=>$success));
   
}
?>