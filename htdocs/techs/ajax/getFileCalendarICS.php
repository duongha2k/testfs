<?php

require '../../headerStartSession.php';
$authData = array('login' => $_SESSION["UserName"], 'password' => $_SESSION["UserPassword"]);
$tech = new Core_Api_TechUser;
$tech->checkAuthentication($authData);

if (!$tech->isAuthenticate()) {
    return API_Response::fail();
}
$TechID = $tech->getTechId();
header("Cache-Control: cache, must-revalidate");
header("Pragma: public");
header('Content-type: text/calendar; charset=utf-8');
header("Content-Disposition: attachment; filename=\"".$_GET['filename']."_".$TechID."_".  uniqid().".ics\"");
$TechClass = new Core_Api_TechClass();
$techids = $_POST['techids'];
if(empty($techids)) die;
$items = explode(',', $techids);
$itemWo = array();
$i = 0;
foreach ($items as $item)
{
    if(empty($item))  continue;
    $arr = explode('-', $item);
    $itemWo[$i]['woid'] = $arr[0];
    $itemWo[$i]['wostatus'] = $arr[1];
    $i++;
}   

function formatTimeValue($_date,$_time,$_timezone)
{
    // for time
    $StartTime = $_time;
    $time = array();
    if(strrpos(strtolower($_time),'am') > 0)
    {
        $StartTime = str_replace(' am', "" ,strtolower($_time));
        
    }  
    $time = explode(":", $StartTime);
    if(strrpos(strtolower($_time),'pm')  > 0)
    {
        $StartTime = str_replace(' pm', "" ,strtolower($_time));
        $time = explode(":", $StartTime);
        if((int)($time[0]) < 12)
            $time[0] = $time[0] + 12;
    }     
    $time[0] = (int)$time[0] + $_timezone;
    $time[1] = (int)$time[1];
    if(count($time) == 2) $time[2] = 0;
    // date
    $date = explode("-", $_date);
    $year = $date[0];
    $mon = $date[1];
    $day = $date[2];
    $date  = mktime($time[0], $time[1], $time[2], $mon  , $day, $year);
    $StartTime = date( "Ymd His",$date);
    $StartTime = str_replace(" ", "T", $StartTime);
    return $StartTime;
}
$ics = <<<EOT
BEGIN:VCALENDAR
METHOD:PUBLISH
X-WR-TIMEZONE:America/Chicago
CALSCALE:GREGORIAN
X-WR-CALNAME:FieldSolutions
VERSION:2.0
X-WR-RELCALID:F1B33276-2C67-473C-B2C5-FFEC990B7C15
X-APPLE-CALENDAR-COLOR:#E51717
BEGIN:VTIMEZONE
TZID:America/Chicago
BEGIN:DAYLIGHT
TZOFFSETFROM:-0600
TZOFFSETTO:-0500
DTSTART:20070311T020000
RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=2SU
TZNAME:CDT
END:DAYLIGHT
BEGIN:STANDARD
TZOFFSETFROM:-0500
TZOFFSETTO:-0600
DTSTART:20071104T020000
RRULE:FREQ=YEARLY;BYMONTH=11;BYDAY=1SU
TZNAME:CST
END:STANDARD
END:VTIMEZONE
EOT;
if (!empty($itemWo)) {
    $timezone = 5;
    foreach ($itemWo as $item) {
        $wos = $TechClass->getWos($item['woid']);
        if(empty($wos)) continue;
        $wo = $wos[0];
        $win = $wo['WIN_NUM'];
        $wo_id = $wo['WO_ID'];
        $Headline = $wo['Headline'];
        $SiteName = $wo['SiteName'];
        $City = $wo['City'];
        $Zipcode = $wo['Zipcode'];
        $PayMax= $wo['PayMax'];
        $State = $wo['State'];
        $Address = $wo['Address'];
        $WO_Category = $wo['WO_Category'];
        $Amount_Per = $wo['Amount_Per'];
        if(empty($wo['StartDate']) || empty($wo['StartTime']) || empty($wo['EndDate']) || empty($wo['EndTime']))  continue;
        $StartDate = $wo['StartDate'];
        $StartTime = $wo['StartTime'];
        $EndDate = $wo['EndDate'];
        $EndTime = $wo['EndTime'];
        $charn = nl2br('\n');
        $Status = $item['wostatus'] == 'applied'?"You have applied for this Work Order $charn":'';
        $Subject = $item['wostatus'] == 'applied'?"Applied: ":"";
        $DTSTART = formatTimeValue($StartDate,$StartTime,0);
        $DTSTARTZ = formatTimeValue($StartDate,$StartTime,$timezone)."Z";
        $DTEND = formatTimeValue($EndDate,$EndTime,0);
        $UID = $win.$TechID;
        $ics.= <<<EOT
\nBEGIN:VEVENT
SEQUENCE:2
TRANSP:OPAQUE
UID:$UID
DTSTART;TZID=America/Chicago:$DTSTART
DTSTAMP:$DTSTARTZ
SUMMARY:$Subject WIN#$win - $Headline � $City, $State
CREATED:$DTSTARTZ
CATEGORIES:FieldSolutions
DESCRIPTION:$Status WIN#:$win $charn Client WO #: $wo_id $charn $WO_Category - $Headline $charn $SiteName $charn $Address $charn $City, $State $Zipcode $charn Pay: $PayMax PER $Amount_Per $charn
DTEND;TZID=America/Chicago:$DTEND
END:VEVENT
EOT;
    }
}
$ics .=<<<EOT
\nEND:VCALENDAR
EOT;

echo $ics;
exit;
?>