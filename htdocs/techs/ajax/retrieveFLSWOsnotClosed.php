<link rel="stylesheet" href="../../css/tablesorter.css" type="text/css" />

<script type="text/javascript" src="../../library/jquery/jquery-packed-1.1.4.js"></script>
<script type="text/JavaScript" src="../../library/jquery/tablesorter/jquery.tablesorter.pack.js"></script>


<table id="wos" class="tablesorter" cellspacing="1">             
<thead>
    <tr>
<th colspan="4" align="center">
<b>FLS Work Orders Not Marked Complete</b><br />

<div style="color:#000000" id='amountDiv'>Amount Pending Completion: $</div>
</th>
	</tr>
    <tr> 
<th>Days Open</th> 
<th>Workorder</th> 
<th>Date</th>
<th>Pay Amt</th>
    </tr> 
</thead> 
<tbody> 
	
		

<?PHP
require_once ("../../library/caspioAPI.php");


// THIS IS USED FOR THE LOOKUP OF FLS WORKORDERS NOT CLOSED - CM
//-----------------------------------------------
	
// Begin SOAP Calls here...
// ----------------------------------
$FLSTechID = $_REQUEST['FLSTechID']; 
$yesterday = date('m-d-Y', strtotime('-1 days'));
$countBack = date('m-d-Y', strtotime('-42 days')); //6-weeks
// Test FLS ID
//$FLSTechID = 778419; 

$records = caspioSelect("FLS_Work_Orders", "WorkOrderID, WorkOrderNo, PjctStartDate, PayAmt", "FLSTechID='$FLSTechID' AND PjctStartDate <= '$yesterday' AND PjctStartDate >= '$countBack' AND TechComplete = '0' AND Kickback = '0' AND IsNull(Deactivated, '') = '' ", "PjctStartDate DESC", false);

$count = sizeof($records);
$todaysDate =  date("m/d/Y");
$total = "0";
foreach ($records as $order) {
	
$fields = getFields($order);
$WorkOrderID = $fields[0];
$workOrderNo = $fields[1];
if ($count=1 && empty($WorkOrderID)){
  echo "<script language='JavaScript'>parent.document.getElementById('viewIframe').style.display='none';</script>";
		die();
	}
$startDate = $fields[2];
$startDate = date("m/d/Y", strtotime($startDate));
$PayAmt= $fields[3];
$PayAmt = str_replace("$", "", $PayAmt);
$total = $total + $PayAmt;

echo "<tr class='wosRow'><td>" .days_elapsed($startDate) . "</td>";
echo "<td><a href='../flsOpenWOS.php?WOS=$WorkOrderID&getFLSTechID=$FLSTechID' target='_top'>$workOrderNo</a></td>";
//echo "<td><a href='../wosDetailsFLS.php?id=$WorkOrderID' target='_top'>$workOrderNo</a></td>";
echo "<td>$startDate</td>";
echo "<td>$ $PayAmt</td></tr>";
}


// Counts days elapsed
function days_elapsed($time)
{
if (!(is_int($time) && strlen($time) == 11))
{ 
$time = strtotime($time);
}
$diff_date = time() - $time;
$more_than_year = date('Y', $diff_date) - 1970; // Hack for non negative timestamps.
$years = ($more_than_year >= 0) ? $more_than_year : '';
return $years * 365 + date('z', $diff_date);
}
?>

<tfoot>
<th colspan="4" align="right">&nbsp;</th>
</tfoot>
</tbody>
</table>


<script language="JavaScript">

var amountDisplay = document.getElementById('amountDiv');
amountDisplay.innerHTML = amountDisplay.innerHTML + <?php echo $total; ?>;
			
$(document).ready(function() 
    { 
        $('#wos').tablesorter({widgets: ['zebra']}); 
		
	} 
); 
</script> 

<script type="text/javascript">
$(document).ready(function(){
$(".wosRow tr").mouseover(function() {$(this).addClass("over");}).mouseout(function() {$(this).removeClass("over");});
//$(".wosRow tr:even").addClass("alt");
});
</script>
