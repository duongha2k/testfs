<?php

if ($_SERVER['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest') {
	require_once("{$_SERVER['DOCUMENT_ROOT']}/library/woapi.php");
    require ("{$_SERVER['DOCUMENT_ROOT']}/headerStartSession.php");
    
    try{
    	$sendArray = $_REQUEST;
    	$result = Core_Tech::deleteTechFileAWS($sendArray);
    	if($result == true){
    		$success = "success";
    	}else{
    		$success = "error";
    	}
    }catch(Exception $e){
    	error_log($e->getMessage());
    }
    
    echo json_encode(array('success'=>$success));
    
}

?>