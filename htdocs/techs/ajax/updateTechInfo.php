<?php

if ($_SERVER['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest')
{
    require_once("{$_SERVER['DOCUMENT_ROOT']}/library/woapi.php");
    require ("{$_SERVER['DOCUMENT_ROOT']}/headerStartSession.php");
    //($result == "false") ? $status = "false" : $status = "true";

    try
    {
        $authData = array('login' => $_SESSION["UserName"], 'password' => $_SESSION["UserPassword"]);

        $user = new Core_Api_TechUser();
        $user->checkAuthentication($authData);
        $techInfoBerfore = Core_Tech::getProfile($_REQUEST['techID'], true, API_Tech::MODE_TECH, "");
        if ($user->getDeactivated() != 1)
        {
            $result = Core_Tech::updateTechInfo($_REQUEST);
            $params = $_POST;
            if(isset($params['MCSA_Cert_Number'])&&empty($params['MCSA_Cert_Number']))
                    $params['MCSA_CertChkInput']=0;
            if(isset($params['MCP_Cert_Number'])&&empty($params['MCP_Cert_Number']))
                    $params['MCP_CertChkInput']=0;
        
            Core_Tech::updateCert($params,'MCSA_CertChkInput','MCSA_Date',"MCSA_Cert_Number",27,'Microsoft Certified Solutions Associate');
            Core_Tech::updateCert($params,'MCP_CertChkInput','MCP_Date',"MCP_Cert_Number",28,'Microsoft Certified Professional');
            Core_Tech::updateCerts($params);
            
            //13931
            $certificationsNew = $params["certificationsNew"];
            $FSTagClass = new  Core_Api_FSTagClass();
            if(!empty($certificationsNew)){
                foreach($certificationsNew as $_certificationid){
                    $Date = (!empty($params['certificationsNewDate_'.$_certificationid])?date_format(new DateTime($params['certificationsNewDate_'.$_certificationid]), "Y-m-d"):date("Y-m-d"));
                    if(!empty($params['certificationsNewStatus_'.$_certificationid])){
                        $FSTagClass->saveFSTagForTech(
                                $params['techID'],
                                $_certificationid,
                                $params['certificationsNewNumber_'.$_certificationid]
                                ,""
                                ,$Date);
                    } else if(!empty($params['FSTag_'.$_certificationid.'_MultiLevelCbx'])) {
                         $FSTagClass->saveFSTagForTech(
                                $params['techID'],
                                $_certificationid,
                                $params['certificationsNewNumber_'.$_certificationid]
                                ,""
                                ,$Date,$params['FSTag_'.$_certificationid.'_MultiLevelCbx']);
                                
                    }else{
                        $FSTagClass->deleteFSTagForTech($params['techID'],$_certificationid);
                    }
                }
            }
            //end 13931
            $TechExts = array();
            $ReceAvaiWorkNotiEmaiAndSMSMess_forGroupWOonly = isset($params['ReceAvaiWorkNotiEmaiAndSMSMess_forGroupWOonly'])? 1:0;
            $ViewOnlyGroupWOonMyDash  = isset($params['ViewOnlyGroupWOonMyDash'])? 1:0;
            $TechExts = array("ReceAvaiWorkNotiEmaiAndSMSMess_forGroupWOonly" => $ReceAvaiWorkNotiEmaiAndSMSMess_forGroupWOonly,
            "ViewOnlyGroupWOonMyDash" => $ViewOnlyGroupWOonMyDash);
            
            $apiTechClass = new Core_Api_TechClass();
            $errnew = $apiTechClass->saveTechExts($_REQUEST['techID'], $TechExts);
            $db = Core_Database::getInstance();
            
            if($_REQUEST['w9'] === 0 || $_REQUEST['w9'] === "0")
            {
                $result = $db->update(
                        Core_Database::TABLE_TECH_BANK_INFO,
                        array("W9"=>$_REQUEST['w9']),
                        array("TechID = ?"=>$_REQUEST['techID'])
                        );
              
                $json = $_REQUEST['EMC'];
                $techid = $_REQUEST['techID'];
                $Core_Api_TechClass = new Core_Api_TechClass;
                $EMCListForTechnician = $Core_Api_TechClass->getEMCListForTechnician($techid);
                if (trim($json) != "" && isset($_REQUEST['EMC'])) {
                    $data = json_decode( $json);
                    if (!empty($data)) {
                        $selectedEMCNames="";
                        // get all
                        foreach ($data as $item) {
                            $EMCName = $item->key;
                            $selectedEMCNames .= empty($selectedEMCNames)?"'".$EMCName."'":  ",'".$EMCName."'";
                            $date = $item->value;
                            $Core_Api_TechClass->saveTechCertification(
                                    $techid,$item->key, (trim($date) != "") ? date_format(new DateTime($date), 'Y-m-d') : ''
                            );
                        }
                        //remove spam
                        $Core_Api_TechClass->selectOnlyEMCNames($techid,$selectedEMCNames);
                    }
                    if(!empty($EMCListForTechnician))
                    {
                        foreach($EMCListForTechnician as $emcs)
                        {
                            $delete = true;
                            if (!empty($data)) {
                                foreach ($data as $item) {
                                    if($emcs["name"] == $item->key)
                                    {
                                        $delete = false;
                                        break;
                }
                                }  
                            }
                            if($delete)
                            {
                                $Core_Api_TechClass->deleteCertificationByName($techid,$emcs["name"]);
                            }    
                        } 
                    }    
                       
                }
                $mail = new Core_Mail();
                //Send mail to tech
                $message = "

You have changed your name in your FieldSolutions Technician Profile from ".$techInfoBerfore['Firstname']." ".$techInfoBerfore['Lastname']." to ".$_REQUEST['FirstName']." ".$_REQUEST['LastName'].". As required by law, you must also update your W-9 on file with FieldSolutions. You can securely update your W-9 online by visiting Section 2: Payment Information within your Technician Profile.

If you have any questions regarding your W-9, please contact <a href='mailto:accounting@fieldsolutions.com'>accounting@fieldsolutions.com</a>.

Thank you for your prompt attention to this matter.

Sincerely, Accounting Field Solutions

";
                
                $mail->setBodyText($message);
                $mail->setFromName("FieldSolutions");
                $mail->setFromEmail('no-replies@fieldsolutions.com');
                $mail->setToEmail($_REQUEST['PrimaryEmail']);
                $mail->setSubject("Field Solutions W-9");
                $mail->send();

                //send mail to accounting
                
//                $apistaff = new Core_Api_CommonClass();
//                $staff = $apistaff ->getOpeStaffById(1);
//                   
                
//                if(!empty($staff))
//                {
                    $message = "Accounting,

Tech ID# ".$_REQUEST['techID']." has changed names from ".$techInfoBerfore['Firstname']." ".$techInfoBerfore['Lastname']." to ".$_REQUEST['FirstName']." ".$_REQUEST['LastName'].". This technician has been notified to make the necessary updates to their W-9.

Thank you
";
                    $mail->setBodyText($message);
                    $mail->setFromName("FieldSolutions");
                    $mail->setFromEmail('support@fieldsolutions.com');
                    //$mail->setToEmail($staff['email']);
                    $mail->setToEmail('W9@fieldsolutions.com');
                    
                    $mail->setSubject("Name Change: Tech ID# ".$_REQUEST['techID']);
                    $mail->send();
//                }
            }
        }   
        else
            $result = false;
        if ($result)
        {
            $techInfoAfter = Core_Tech::getProfile($_REQUEST['techID'], true, API_Tech::MODE_TECH, "");
            $Core_Api_TechClass = new Core_Api_TechClass;
            if (!empty($techInfoBerfore))
            {
                if ($techInfoBerfore['PrimaryPhone'] != $techInfoAfter['PrimaryPhone'])
                {
                    $aClient = $Core_Api_TechClass->setPrimaryPhoneValid($_REQUEST['techID'], 2, $_SESSION["UserName"]);
                }
                if ($techInfoBerfore['SecondaryPhone'] != $techInfoAfter['SecondaryPhone'])
                {
                    $aClient = $Core_Api_TechClass->setSecondaryPhoneValid($_REQUEST['techID'], 2, $_SESSION["UserName"]);
                }
            }
            $success = "success";
        } else
        {
            $success = "error";
        }
    } catch (Exception $e)
    {
//		print_r($e);
    }

    echo json_encode(array('success' => $success));
}
?>