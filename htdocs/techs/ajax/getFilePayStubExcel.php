<?php

require '../../headerStartSession.php';

$authData = array('login' => $_SESSION["UserName"], 'password' => $_SESSION["UserPassword"]);
$tech = new Core_Api_TechUser;
$tech->checkAuthentication($authData);

if (!$tech->isAuthenticate()) {
    return API_Response::fail();
}
$TechName = $techFirstName = $tech->getFirstName() . ' ' . $techLastName = $tech->getLastName();
$TechID = $tech->getTechId();
$_PaymentMethod = $tech->getPaymentMethod();

$datePayStubValue = trim(urldecode($_REQUEST['datePayStubValue']));
$datePayStubText = trim(urldecode($_REQUEST['datePayStubText']));
$netPayForPeriod = "";
$search = new Core_Api_TechClass();
$netPayYear = "";

$startDate = "";
$endDate = "";
$endDateFileName = "";
if ($datePayStubValue != "") {
    $date = split(',', $datePayStubValue);
    $startDate = $date[0];
    $endDate = $date[1];
    /* test */
    //$startDate='2007-01-01';
    /* end of  test */
    $date0 = new DateTime($startDate);
    $date1 = new DateTime($endDate);
    $endDateFileName = $date1->format('Ymd');
    //$datePayStubText = $date0->format('m/d/Y');
    $netPayForPeriod = $search->getNetPayForPeriod(
                    $_SESSION["UserName"], $_SESSION["UserPassword"], $startDate, $endDate);
    
    $netPayForPeriod = empty($netPayForPeriod)?"":"\$".$netPayForPeriod;
    
    //$netPayYear = $search->getNetPayForYear($_SESSION["UserName"], $_SESSION["UserPassword"], date('Y', strtotime($endDate)));
    $netPayYear = $search->getNetPayYTD($_SESSION["UserName"], $_SESSION["UserPassword"], 
                                        $startDate, $endDate);
    
    $netPayYear = empty($netPayYear)?"":"\$".$netPayYear;
    
    $result = $search->getPayStubsForPeriod($_SESSION["UserName"]
            , $_SESSION["UserPassword"], $startDate, $endDate);
}
$csv_filename = "FSPayStub_" . $tech->getLastName() . '_' . $endDateFileName . ".csv";
header("Content-disposition: attachment; filename=$csv_filename");


echo "\"FieldSolutions Pay Stub\",,,,,\"Date Created: " . date('M d, Y') . "\" \n";
echo " \n";
echo "\"Name: " . $TechName . "\",,,,,\"Paid Date: " . $datePayStubText . "\" \n";
echo "\"Tech ID#: " . $TechID . "\",,,,,\"Net Pay This Period: " . $netPayForPeriod . "\" \n";
echo "\"Payment Method:  " . $_PaymentMethod . "\",,,,,\"Net Pay Year to Date: " . $netPayYear . "\" \n";
echo " \n";
echo "\"Start Date\",\"WIN#\",\"Approved Date\",\"Headline\",\"Gross Pay\",\"Net Pay\" \n";
//echo "\"               \",\"\",\"               \",\"                       \",\"\",\"\" \n"; //for test


if (isset($result) && count($result) > 0) {
    $length = count($result);
    for ($i = 0; $i < $length; $i++) {
        $DateApproved = $result[$i]['DateApproved'];
        $DateApproved = new DateTime($DateApproved);
        $DateApproved = $DateApproved->format("m/d/Y");
        $winNum = $result[$i]['WIN_NUM'];
        $woStartDate = $result[$i]['StartDate'];
        $woStartDate = new DateTime($woStartDate);
        $woStartDate = $woStartDate->format("m/d/Y");
        //$clientName = $result[$i]['SiteName'];
        $clientName = $result[$i]['Headline'];
        //---334,13711   
        $grossPay = "";
        $netPay="";
            $grossPay = $result[$i]['PayAmount'];                 
            $netPay = $result[$i]['PayAmount_Final'];  
            if(empty($netPay)||$netPay == 0)
            {
                $netPay = $grossPay *(1-($result[$i]['PcntDeductPercent']+0));
            }
            $grossPay = number_format($grossPay, 2, '.', '');            
            $grossPay = "\$".$grossPay;            
            $netPay = number_format($netPay, 2, '.', '');            
            $netPay = "\$".$netPay;
        //---
        $csv = "\"".$woStartDate."\",\"" 
                . $winNum . "\",\"" 
                . $DateApproved. "\",\"" 
                . $clientName . "\",\"" 
                . $grossPay . "\",\"" 
                . $netPay . "\" \n";
        echo($csv);
    }
}
?>