<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Profile Changed</title>
</head>

<body>

<?php
ini_set("display_errors",1);
require_once("../library/quickbooks.php");
require_once("../techs/updateWOs_Tech_Info.php");

$TechID=$_GET["TechID"];		

function UpdateMySQL($TechID)
{
	copyTechProfile(array($TechID));
	copyTechBankInfo(array($TechID));
	
	require_once("{$_SERVER['DOCUMENT_ROOT']}/library/bootstrapAPI.php");
	Core_Database_CaspioSync::sync(TABLE_MASTER_LIST, "TechID", array($_GET["TechID"]));
}


UpdateMySQL($TechID);
UpdateWOs_Tech_Info($TechID);

print("
	<script type=\"text/javascript\">
		window.location = \"../techs/dashboard.php\";
	</script>
");		

echo "Done";

?>


</body>
</html>
