<br /><br />

<div align="center">
<table cellspacing="0" cellpadding="0" width="600" border="0">
	<tr>
<td>
	
	<p align="center">
	<b>FLS &ndash; Computing Security Procedures (FLS-CSP) Policy</b>	</p>
	<p>
	As of March 1 2008, Fujitsu Transactions Services (FTXS)  is requiring all First Line Support (FLS) technicians to review, sign and submit the FLS-CSP form in order to be eligible to run calls or work projects/installs for FLS. Effective immediately, all new FLS technicians will be required to complete the FLS-CSP.<br>
	<br>
	The FLS-CSP form helps assure FTXS that FLS technicians working for them understand the following:<br>
	<br>
	As a contracted technician working for FLS, you agree to adhere to customer policies, regulations and procedures governing the security of their computers and associated data that may be provided to you by the customer.  You will be acknowledging and agreeing to not access information for any reason other than what is necessary to complete the job/task.
	<p><b>Note:</b> The FLS-CSP form that you need to download below is in PDF. Meaning, if you do not already have it, you will need the <a href="http://www.adobe.com/products/acrobat/readstep2.html" target=_blank>Adobe Reader</a> software to view and print out the form for faxing. You can download the required Adobe Reader software for free by <a href="http://www.adobe.com/products/acrobat/readstep2.html" target=_blank>
		clicking here</a>.
	<p align="center"><img height=23 alt="" src="images/pdf.jpg" width=23 border=0> <b>FLS-CSP Form</b> / <a href="forms/FLSCSP.pdf">Click here</a> to download <br>	</p>
<div align="center">
		<ul>
		  <li>Fax your completed and signed FLS-CSP form to: 888-870-1182		  </li>
		  </ul></div>
		<p>We are accepting only faxes of the Disclosure form.  Do not email it.</p>
    <p>Once we have received your signed FLS-CSP form, we will check your profile as having successfully received the form, within 3 business days.</p>
    <p>Questions? Please contact Field Solutions support at: <a href="mailto:support@fieldsolutions.com">support@fieldsolutions.com</a></p>
    <p align="center">&nbsp; </p></td>
	</tr>	
</table>	

</div>

	
