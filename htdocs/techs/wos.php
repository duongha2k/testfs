<?php 
$page = 'techs';
$option = 'wos';
require '../header.php';
?>
<script>
$(document).ready(function() {if (typeof(startup) == 'function') startup()});
window._techId = '<?=$_SESSION['TechID']?>';
var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";
</script>
<?php
require '../navBar.php';
$search = new Core_Api_TechClass();
$counters = array();
$search->getWorkOrdersCountByStatus($_SESSION["UserName"], $_SESSION["UserPassword"], $counters);
//075
$techinfo = new Core_Tech($_SESSION['TechID']);
$country = $techinfo->getCountry();
unset($techinfo);
//end 075
?>
<script type="text/javascript" src="/widgets/js/jquery.form.js"></script>
<script type="text/javascript" src="/widgets/js/FSTechDashboard.js?16082011"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetActivityLog.js"></script>
<script type="text/javascript" src="/widgets/js/ProgressBar.js"></script>
<script type="text/javascript" src="/widgets/js/ProgressBarAdapterAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/ProgressBarAdapterPopupSpin.js"></script>
<script type="text/javascript" src="/widgets/js/FSTechPayUtils.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetSortTool.js"></script>
<script type="text/javascript" src="/widgets/js/livevalidation.js"></script>
<script type="text/javascript" src="/library/jquery/jquery.bt.min.js"></script>
<script type="text/javascript" src="/techs/js/wosTechDetails.js"></script>
<link rel="stylesheet" href="/widgets/css/resizable.css" type="text/css" />

<script type="text/javascript">
var LVDefaultFunction = function(){this.addFieldClass();}
LVDefaults = {
    onValid : LVDefaultFunction,
    onInvalid : LVDefaultFunction
}

function reloadTabFrame() {
	// old dashboard reload tab adapter
	try {
	        wd.show({tab:wd.currentTab,params:wd.getParams()});
	} catch (e) {}
}

/**
 * toggleTab
 *
 * Switch Tabs
 *
 * @param string name Name of currect Tab
 * @param object link Tab's link object
 * @param object td td object
 * @return false always
 */
function toggleTab( name, link, td, show ) {
    if (td.currentTab != name) {
        jQuery("#MyscheduleDownloadAssigned").attr("tab",name);
        if(name == 'techassigned') jQuery("#MyscheduleDownloadAssigned").show(); else jQuery("#MyscheduleDownloadAssigned").hide();
        td.resetFilters();
        td.currentTab = name;
        td.sortTool().initSortTool();

        if(name == 'techavailable'){
            td.filters().distance('50');
            d = new Date();
            mm = d.getMonth()+1;
            td.filters().dateStart(''+mm+'/'+d.getDate()+'/'+d.getFullYear()+'');
        }
        
       /*
        if(name == 'techall'){
            d = new Date();
            mm = d.getMonth()+1;
            td.filters().dateStart(''+mm+'/'+d.getDate()+'/'+d.getFullYear()+'');
        }
	*/
    }

    if(show == "showall" && (name == "techavailable")){
        td.filters().dateStart('01/01/1960');
    }else if(show == "showcurrent" && (name == "techavailable")){
    	d = new Date();
	mm = d.getMonth()+1;
	td.filters().dateStart(''+mm+'/'+d.getDate()+'/'+d.getFullYear()+'');
    }
    //075
    if(name == "techavailable" && td.filters()._country=="")
    {
        td.filters().country('<?=$country?>');
    }
    //end 075
    td.show({tab:td.currentTab,params:td.getParams()});

    link.blur();
    $("#toppaginatorContainer").html('');

    if(td.currentTab == "techavailable"){
        showLink = $('#showAll a');
        if(td.currentTab == "techavailable"){
            if(show == "showall"){
                var showCurrent = "Show Current (" + '<?=$counters['published']?>' +")";
                showLink.text(showCurrent);
            }else{
                var showAll = "Show All ("+ '<?=$counters['published_all'] ?>' +")";
                showLink.text(showAll);
            }
        }else if(td.currentTab == "techall"){
            if(show == "showall"){
                var showCurrent = "Show Current ("+ '<?=$counters['all_current']?>' +")";
                showLink.text(showCurrent);
            }else{
                var showAll = "Show All ("+ '<?=$counters['all'] ?>' +")";
                showLink.text(showAll);
            }
        }
            $("#showAll").show();
    }else{
            $("#showAll").hide();
    }

}

var td;             //  Widgets object
var wd;
var progressBar;    //  Progress bar object
var sch;

function startup() 
{
    wd = td  = new FSTechDashboard({container:'widgetContainer',tab:'techassigned'});
    // Find tag in URL params
    var tab = '<?= (!empty($_GET['tab'])) ? strtolower($_GET['tab']) : '__invalid__'; ?>';
    if ( td._isValidTab(tab) ) { td.currentTab = tab; }
    // Find tab in URL params
    td.htmlFilters();

    td.sortTool(new FSWidgetSortTool('sortSelect', 'directionSelect', td, roll, 'quick'));

//    td.setParams(td.parseUrl());

    //  Load tab by parameters in URL
    if(td.currentTab == 'techavailable'){
        td.filters().distance('50');
        d = new Date();
        mm = d.getMonth()+1;
        td.filters().dateStart(''+mm+'/'+d.getDate()+'/'+d.getFullYear()+'');
    }
    td.setParams(td.parseUrl());
    toggleTab(td.currentTab, $('#'+td.currentTab), td);
    //  Load tab by parameters in URL

    $("#viewFiltersButton").click(function(event){
        roll.autohide(false);
        var opt = {
            width       : 325,
            height      : 230,
            title       : 'Filter By',
            context     : td,
            handler     : td.prepareFilters,
            postHandler : calendarInit,
            body        : $(td.filters().container()).html()
        };
        roll.showNotAjax(this, event, opt);
    });
    $("#downloadButton").click(function(event) {
        var opt = td.getParams();
        opt['download']='csv';
        td.open({
            tab     : td.currentTab,
            params  : opt
        });
    });
    $("#sortButton").click(function(event){
        td.sortTool().switchSortTools('full');
        roll.autohide(false);
        var opt = {
            width   : 325,
            height  : '',
            title   : 'Sort Work Orders...',
            context : td.sortTool(),
            handler : td.sortTool().initSortPopup,
            body    : td.sortTool().makeFullSortPopup()
        };
        roll.showNotAjax(this,event,opt);
    });
};

function switchShowAll(obj){
    var showAll = "Show All ("+ '<?=$counters['published_all'] ?>' +")";
    var showCurrent = "Show Current (" + '<?=$counters['published']?>' +")";

    var showLink = $(obj);
    
    if(showLink.text().substring(0,8) == 'Show All') {
        showLink.text(showCurrent);
        showLink.css("display", "inline");
        toggleTab(wd.currentTab, showLink, wd, 'showall');
    } else {
        showLink.text(showAll);
        showLink.css("display", "inline");
        toggleTab(wd.currentTab, showLink, wd, 'showcurrent');
    }
    return false;
}
</script>

<div id="container">
<!--  div class="toolGroup" -->
    <div class="toolGroupLeft">
		<div class="toolBtn">
			<input id="viewFiltersButton" type="button" value="Filter" class="link_button" />
		</div>
        <div class="toolBtn">
			<input id="sortButton" type="button" value="Sort" class="link_button" />
		</div>
		<div class="toolBtn">
			<input id="downloadButton" type="button" value="Download" class="link_button download_button" />
        </div>
        <div class="toolBtn">
            <img style="cursor: pointer;<?= $_GET['tab'] =="techassigned"||$_GET['tab'] =="techavailable"?'':'display:none;' ?>" tab="<?= $_GET['tab'] ?>"  id="MyscheduleDownloadAssigned" width="25" height="25" border="0" title="" onclick="detailObject.onInit.showMyscheduleDownload(this);" src="/images/CalendarIcon.gif" alt="" title="">
	</div>
	</div>
	<div class="toolGroupRight" style="">
		<div class="toolBtn topSm">
			Quick Sort
		</div>
		<div class="toolBtn">
			<select id="sortSelect" onchange="return td.sortTool().applySort('quick');"></select>
		</div>
		<div class="toolBtn">
			<select id="directionSelect" onchange="return td.sortTool().applySort('quick');"></select>
		</div><br /><br />
		
		<div class="toolBtn" id="showAll">
			<a href="javascript:void(0);" onclick="switchShowAll(this);">Show All (<?= $counters['published_all']?>)</a>
		</div>
	</div>
<!-- /div -->
<br/>
<div id="header">
    <div id="wrapper"><div id="content" align="center"></div></div>
    <div id="tabbedMenu" style="height:16px;">
    <ul>
        <li id="techavailable"><a href="javascript:void(0)" onclick="toggleTab('techavailable', this, td);">Available</a></li>
        <li id="techassigned"><a href="javascript:void(0)" onclick="toggleTab('techassigned', this, td);">Assigned</a></li>
        <li id="techworkdone"><a href="javascript:void(0)" onclick="toggleTab('techworkdone', this, td);">Work Done</a></li>
        <li id="techincomplete"><a href="javascript:void(0)" onclick="toggleTab('techincomplete', this, td);">Incomplete</a></li>
        <li id="techapproved"><a href="javascript:void(0)" onclick="toggleTab('techapproved', this, td);">Approved</a></li>
        <li id="techpaid"><a href="javascript:void(0)" onclick="toggleTab('techpaid', this, td);">Paid</a></li>
        <li id="techall"><a href="javascript:void(0)" onclick="toggleTab('techall', this, td);">All</a></li>
    </ul>
    <script>
    function onChangePageSize()
    {
    	reloadTabFrame();
    }
    </script>
    <div id="toppaginatorSizeContainer"></div>
    <table cellspacing='0' cellpadding='0' class="paginator_top"><nobr><tr><td class="tCenter" id="toppaginatorContainer"></td></tr></nobr></table>
    </div>
    <br/>

    <div id="widgetContainer" style="margin-left:auto; margin-right:auto; width:100%; height:100%; clear: left;"></div>
</div>
</div>
<!-- End Content -->
<?php require ("../footer.php"); ?>
