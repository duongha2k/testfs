<?php
$page = 'techs';
$option = 'survey';
require_once ("../headerStartSession.php");
//
//$_SERVER['HTTPS'] = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ) ? 'on' : 'off';
//
//$loggedIn = (!empty($_SESSION['loggedIn']) ) ? $_SESSION['loggedIn'] : 'no';
//if ($loggedIn !== 'yes')
//{
//    header('Location: http' . ($_SERVER['HTTPS'] == 'on' ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . '/techs/logIn.php'); // Redirect to logIn.php
//}
//
//$_GET['v'] = (!empty($_GET['v']) ) ? $_GET['v'] + 0 : -1;
//$_GET['id'] = (!empty($_GET['id']) ) ? $_GET['id'] + 0 : -1;
//
///**
// * to allow tech view available WO if there isn't tech in get try load from session
// * @author Artem Sukharev
// */
//$_GET['v'] = $_GET['v'] == -1 && isset($_SESSION['TechID']) && !empty($_SESSION['TechID']) ? $_SESSION['TechID'] : $_GET['v'];
//
//$ckey = md5(microtime() . $_GET['id'] . $_GET['v']);
$siteTemplate = (!empty($_SESSION['template']) ) ? $_SESSION['template'] : NULL;
if ($siteTemplate == "ruo")
{
    require '../templates/ruo/includes/header.php';
} else
{
    require '../headerSimple.php';
}
?>
<link rel="stylesheet" href="../../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-mod.js"></script>

<link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" media="all" />
<script type="text/javascript" src="../../library/js/jquery.watermark.js"></script>
<script type="text/javascript" src="js/techsurvey.js"></script>
<script type="text/javascript">
    
    $(document).ready(function() {
        var _techsurvey = techsurvey.CreateObject({
            id: '_techsurvey',
            win:'<?= $_GET['win'] ?>'
        });
    });
</script>
<!-- Main content -->
<style>
    .backgroundlogo
    {
        background: url('/widgets/images/techsurvey/flex_logo.png') no-repeat scroll 0px 0px transparent;
        height: 90px;
        width: 100%;
        float: left;
    }
    .flextronicssurveylogo
    {
        background: url('/widgets/images/techsurvey/flextronicssurveylogo.png') no-repeat scroll 0px 0px transparent;
        height: 90px;
        width: 90px;
        float: right;
    }
    #surveyPlace table tr td
    {
        vertical-align: top;
    }
    #surveyPlace table .Section
    {
        color: #3399ff; 
        font-weight: bold;
        font-size: 13px;
        padding: 5px 0;
        margin:0;
        padding-left:5px;
    }

</style>
<div class="backgroundlogo">
    <div class="flextronicssurveylogo"></div>
</div>

<div id="surveyPlace" style="float:left;width:100%;">
    <img style='margin:10% 40%;' alt='Wait...' src='/widgets/images/progress.gif' />
</div><br/>
<div style="text-align:center;margin: 30px 0" id="divcmd">
    <input type="button" value="Back" class="link_button" id="cmdBack" name="cmdBack" style="width: 96px; background: url('/widgets/images/button.png') no-repeat scroll 0px 0px transparent; display:none;">
    <input type="button" value="Next" class="link_button" id="cmdNext" name="cmdNext" style="width: 96px; background: url('/widgets/images/button.png') no-repeat scroll 0px 0px transparent;">
    <input type="button" value="Submit" class="link_button" id="cmdSubmit" name="cmdSubmit" style="width: 96px; background: url('/widgets/images/button.png') no-repeat scroll 0px 0px transparent; display:none;">
</div>
