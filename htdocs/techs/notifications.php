<?php 
$page = 'techs';
$option = 'dashboard';
require_once ("../headerStartSession.php");

if ( !empty($_SESSION['redirectURL'])) {
        $redir = $_SESSION['redirectURL'];
        unset($_SESSION['redirectURL']);
	if ($redir != $_SERVER['REQUEST_URI'] && strpos($redir, "techs") !== FALSE && $redir != "/techs/")
		header( 'Location: ' . $redir ) ;
}

$siteTemplate = $_SESSION['template'];

if ($siteTemplate == "ruo") {
	require ("../templates/ruo/includes/header.php");
} else {
	require ("../header.php");
}
$loggedIn   = $_SESSION['loggedIn'];
$TechID     = $_SESSION['TechID'];
$FLSID      = $_SESSION['FLSID'];
$FLSCSP_Rec = $_SESSION['FLSCSP_Rec'];

if( $loggedIn != "yes" ) { 
    // Redirect to logIn.php
    header( 'Location: https://www.fieldsolutions.com/techs/logIn.php' ) ;
}
?>

<?php require ("../navBar.php"); ?>

<script type="text/javascript" src="../library/jquery/jquery_iframe.js"></script>


<!--script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script> <script type="text/javascript" language="javascript">try{f_cbload("I4H0E7G3A9I4H0E7G3A9","https:");}catch(li){;}</script> <div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=I4H0E7G3A9I4H0E7G3A9">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div-->

<!-- Check if logged in, if not redirect -->
<script language="JavaScript">
try {
	var getUsernameField = document.forms[0].xip_UserName.value;
	setTimeout('window.location.replace("/techs/index.php")');
} catch(e) {} 
</script>

<!--- LEFT COLUMN --->
<div id="leftcontent2">
<div id="" style="padding-left:40px">
<table width="425" border="0" cellpadding="0">
  <tr>
    <td>

    <style type="text/css">
		div#alert {
			border: 1px solid #174065;
			margin-bottom: 15px;
			padding: 10px;
		}
		#alert ol,
		#alert ul {
			margin: 0 25px;
			}
		#alert ul {
			list-style: none;
		}
		#alert h3 {
			color: red;
			margin-top: -20px;
			}
		#alert h4 {
			margin-bottom: 15px;
		}
		#alert li {
			margin-bottom: 10px;
		}
	</style>

<!--
<div style="color: #ff0000; font-weight: bold">Due to the Thanksgiving holiday direct deposit may be late depending
on your banks policy.
We are sorry for any delays this may cause you. Field Solutions Management</div>
-->
<br/>

<div id="alert">
<h3>Attention All Technicians:</h3>
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b0000b7e8e0e4i5c3e9f3f4g7&Section=Tech%20Dashboard","https:");}catch(v_e){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000b7e8e0e4i5c3e9f3f4g7&Section=Tech%20Dashboard">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>
</div>

<br />

</td>
  </tr>
</table>


<table width="425" border="0" cellpadding="0">
  <tr>
    <td>

    <style type="text/css">
		div#alert {
			border: 1px solid #174065;
			margin-bottom: 15px;
			padding: 10px;
		}
		#alert ol,
		#alert ul {
			margin: 0 25px;
			}
		#alert ul {
			list-style: none;
		}
		#alert h3 {
			color: red;
			margin-top: -20px;
			}
		#alert h4 {
			margin-bottom: 15px;
		}
		#alert li {
			margin-bottom: 10px;
		}
	</style>

    <div id="alert">
    <h3>Note for FLS Techs</h3>
 <script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script><!-- 193b0000e2f2g7f8c2h8i8b1a8b6 -->
<script type="text/javascript" language="javascript">try{f_cbload("193b00009528eea2841a4bcfaba9&Section=Tech%20FLS%20Dashboard","https:");}catch(v_e){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b00009528eea2841a4bcfaba9&Section=Tech%20FLS%20Dashboard">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>
   </div>
    <br />
</td>
  </tr>
</table><br />
<br />
</div>

<br><br>

</div>



<?php require ("../footer.php"); ?>

<script type="text/javascript" language="javascript">

// Jquery Ready Function..when everything loaded execute JS code
$(document).ready(function(){ 
<!-- Hide submit button on caspio datapage -->
document.getElementById('Mod0EditRecord').style.display = 'none';

// Verify flsid exists
if(document.forms[0].EditRecordFLSID.value != ""){
$('a.iframe').iframe();
}

}); 
//-->
</script>
