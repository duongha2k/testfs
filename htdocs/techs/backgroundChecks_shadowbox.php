<?php $page = 'techs'; ?>
<?php $option = 'backgroundChecks'; ?>
<?php $display = 'yes'; ?>
<?php $selected = 'backgroundChecks'; ?>
<?php require ("../headerSimple.php"); ?>
<!-- Add Content Here -->
<br />
<script type="text/javascript">
    var userName = '<?= $_SESSION['TechID'] ?>';
    
    
</script>

<h3 align="left">
    <b>Background Check Instructions</b>
</h3>
<p>Some clients of Field Solutions require Technicians to be background checked prior to performing any work for them. It is up to each individual Technician whether he/she has a background check performed or not. If you elect not to get a background check you will not be eligible for work orders that a client indicates as background check required. 
<p>Background checks must be performed via Field Solutions regardless of whether or not you have been background checked by another entity in the past. The cost to have a Basic Background Check performed for Field Solutions is $19.00.  The fee is non-refundable.</p>
<p>If you plan on having a background check performed via Field Solutions, be advised that it <span style="text-decoration:underline">takes up to 5 business days to complete the cycle.</span> Until the background check has been received and logged into your profile, you will not be included or eligible for background check required work orders. </p>
<p>&nbsp;</p>
<hr><br>
<h3 align="left" style="font-weight: bold">Background Check Instructions &ndash; Step-by-Step Process</h3>
<p>PLEASE PRINT THIS PAGE BEFORE STARTING (use your browser button).  Then follow the step-by-step instructions below. To help speed up the process and eliminate confusion, it is very important to follow through with each step in the exact order listed.  </p>
<p><span style="font-weight: bold">STEP ONE)</span> Submit your $19.00 payment via PayPal by clicking <a onclick="window.parent.location='https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=support@fieldsolutions.com&item_name=Background%20Check&item_number=BC2&amount=19.00&no_shipping=0&no_note=0&currency_code=USD&lc=US&bn=PP-BuyNowBF&charset=UTF-8'" href="javascript:;">here</a>. </p>
<table width="600" border="0" cellpadding="0">
    <tr>
        <td width="50">&nbsp;</td>
        <td><b>Important payment notes:</b>
            <br>
            <br>
            When submitting payment via PayPal, please pay attention to the special notes box. We need you to <span style="text-decoration:underline">enter the full <span style="font-weight:bold">LEGAL NAME OF THE PERSON</span> getting the background check</span> (not a business name or nickname) into that box so we can identify you and credit you for payment.<br>
        </td>
    </tr>
    <tr>
        <td width="50">&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td width="50">&nbsp;</td>
        <td>Credit card payments and bank account debits are processed immediately, eChecks take several days to clear. Thus, it may take a business day or two before you'll be able to move to Step 3.</td>
    </tr>
</table>    
<p><span style="font-weight: bold">STEP TW0)</span> Submit your request for a background check by clicking <a onclick="window.parent.location='backgroundCheckRequest.php'" href="javascript:;">here</a>.</p>

<p><b>Note:</b> Only after going to the secure web site in Step 3 below, will your background check be performed. Step 3 cannot and will not be initiated until you have completed Steps 1 &amp; 2 above. </p>
<h3 align="left" style="font-weight: bold">Email correspondence - Confirmation/Instructions:</h3>
<p><span style="font-weight: bold">3)</span> Upon confirmation of Steps 1 & 2, an email will be sent to you containing access information and a link to the secure web site. Once you receive that email:<br>
</p>
<table width="600" border="0" cellpadding="0">
    <tr>
        <td width="50">&nbsp;</td>
        <td>
            <ul>
                <li>Login to the secure web site using the login info that was emailed to you </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td width="50">&nbsp;</td>
        <td>
            <ul>
                <li>Follow the online instructions for entering your information in the required fields. </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td width="50">&nbsp;</td>
        <td>
            <ul>
                <li>The final step of the background check  application is a secure digital signature page. You will be required to agree to the background check terms and digitally sign. </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>Upon submission of your info and you agreeing to the terms and digitally signing, your background check will be conducted.</td>
    </tr>
</table>
<p>&nbsp;</p>
<hr>
<p>&nbsp;</p>
<p>It takes 2-3 business days to process the background check. Allow up to 5 business days from the completion of Step 3 for a pass or fail to be indicated in your technician profile/account with Field Solutions. </p>
<p>If you fail the check, you will be entered in the Field Solutions tech database as having failed the background check. Clients will only see an indicator that you have not taken a background check. </p>
<p>Questions? Please review the <a  onclick="window.parent.location='background_FAQ.php'" href="javascript:;">FAQ</a> section prior to contacting Field Solutions support at: <a href="mailto:support@fieldsolutions.com">support@fieldsolutions.com</a></p></td>
</tr>
</table>