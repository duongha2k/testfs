<?php $page = techs; ?>
<?php $option = media; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>

<!-- Add Content Here -->

<br /><br />

<div align="center">
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script> <script type="text/javascript" language="javascript">try{f_cbload("H4H8F5A4G7E7I5F0E6","https:");}catch(li){;}</script> <div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=H4H8F5A4G7E7I5F0E6">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>
</div>

<script type="text/javascript" language="javascript">
<!--document.forms.caspioform.EditRecordmediaComplete.checked = true;-->
</script>

<table border="0" cellpadding="0" cellspacing="0">
	<tr valign="top">
<td>

<p>
<FONT face=Arial size=4><B>Creating an Audio Resume for Your Tech Profile</B> 
</FONT>
</p>

<P>
As a registered Tech at Field Solutions, you are strongly encouraged to 
submit a picture of yourself as well as an Audio Resume per our simple instructions
 below. 
</P>

<HR>

<p>
More and more companies seeking Technicians, will be visiting Field Solutions
to see which Techs have submitted pictures and audio of themselves Techs who have 
submitted a picture as well as audio, will be far more likely to be selected to 
perform contract work. 
</P>

<P>
You may have your resume/profile on other resume posting web sites have companies contacting you via that site. However, keep in mind, those companies may eventuallybegin visiting Field Solutions to see if you have a picture of yourself and an Audio Resume for them to listen to. If you do not, they will quite likely seek out another Technician who does. 
</P>

<br /><br /><br /><br /><br />
</td>
<td>

<p>
<FONT face=Arial size=4><B>Upload Your Picture</B> 
</FONT>
</p>

<P>
Do you have to submit a picture of yourself as well as audio in order to be part of
our database? No. However, it is certainly in your best interest to do so. Clients 
seeking Techs are going to constantly look at and consider Techs who have submitted
a picture and audio of themselves before Techs who have not. 

<P>
Here is all you need to do in order to have your picture and audio posted along 
with your standard profile: 
</P>

<HR>

<p>
<B>1) Upload your picture</B> - If you haven't already uploaded your picture during the full registration process, it's important that you go to the <a href="profile.php">Profile page</a> and upload your picture prior to creating and submitting your Audio Resume. 
</p>

<HR>

<p>
<B>2) Create your Audio Resume</B> - <FONT color=#000000>Access/print the Audio Resume Script below. The script includes instructions plus questions for you to answer when recording your Audio Resume</FONT><FONT color=#000080>. </FONT>
</p>

<P>
<FONT face=Arial size=1>Note: You will need Adobe Reader to view/print the Audio Resume Script below as it is in PDF. If you do not already have the required Adobe software, you can download it for free by <A href="http://www.adobe.com/products/acrobat/readstep2.html" target=_blank><FONT face=Arial color=#0000a0 size=1><B>clicking here</B></FONT></A> </FONT>
</P>

<P align="center">
<A href="/docs/audioresume-q-a.pdf" target=_blank><FONT face=Arial color=#0000a0 size=2><B>Click here</B></FONT></A> to access/print the Audio Resume Script.
</P>

<br><br><br><br>
</td></tr></table>

<!--- End Content --->
<?php require ("../footer.php"); ?>
		
