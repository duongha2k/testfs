<?php $page = 'techs'; ?>
<?php $option = 'more'; ?>
<?php
	require_once("../headerStartSession.php");
	require_once("../library/FSSession.php");
	FSSession::validateSessionParams(array("TechID" => "TechID"));
	require_once("../library/smtpMail.php");
?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>

<script type="text/javascript" src="/widgets/js/jquery.validate.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.curvycorners.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.easing.pack.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-mod.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.jqEasyCharCounter.js"></script>
<link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" media="all" />
<!-- Add Content Here -->
<style>
#backgroundRequestForm{
margin-top: 40px;
}
#BCRequestTable{
border:3px solid #10085b;
width:500px;
}
#BCRequestTable table{
width:100%;
}
td{
width:50%;
}
#BCRequestTable thead th{
text-align: left; 
vertical-align: top; 
color: #2a497d; 
font-size: 12px; 
font-family: Verdana; 
font-style: normal; 
font-weight: bold;
}
#BCRequestTable tfoot{
height:28px;
}
#BCRequestTable tfoot tr{
background-color:#5091cb;

}
#BCRequestTable input#submitRequest{
background-color:#10085b;
color:white;
font-weight:bold;
border:0;
margin-left:45%;
margin-right:45%;
}
#BCRequestTable .label{
color:#032D5F;
}
#BCRequestTable .info{
padding-left:5px;
}
#BCRequestTable .notes{
color: #2A497D; 
font-size: 12px; 
font-family: Verdana; 
font-style: normal; 
font-weight: bold;
}
.break{
border-top:3px solid #10085b;
}
.required{
color:#FF0000;
font-size: 12px;
font-family:Verdana;
font-weight:bold;
}
#correctEmail, #requestBC{
margin-right: 5px;
}
h1{
font-weight: bold;
color: white;
font-size: 12px;
padding-top: 5px;
padding-bottom: 5px;
margin-left: 2px;
text-align: left;
background-color: #10085A;
}
#breaks{
border-top: 3px solid black;
border-bottom: 3px solid black;
width:100%;
}

</style>
<script type="text/javascript">
function waitTechInfo() {
	try {
		a = techInfo;
		showTechInfo();
	} catch (e) {
		setTimeout("waitTechInfo()", 1000);
	}
}

function showTechInfo() {
	$("#TechID").html(techInfo["TechID"]);
	$("#Firstname").html(techInfo["Firstname"]);
	$("#BgFirstName").val(techInfo["Firstname"]);
	$("#Lastname").html(techInfo["Lastname"]);
	$("#BgLastName").val(techInfo["Lastname"]);
	$("#PrimaryEmail").html(techInfo["PrimaryEmail"]);
	$("#BgPrimaryEmail").val(techInfo["PrimaryEmail"]);
}
$(document).ready(function () {
	waitTechInfo();
		$('#BCRequestForm').submit(function(event) {
			event.preventDefault();
			if( $("#correctEmail").is(":checked") == true && $("#Bg_Test_Req_Lite").is(":checked") == true){
			var submitData = $(this).serializeArray();
			$.ajax({
				type: "POST",
				url: "/techs/ajax/saveBackgroundCheckRequest.php",
	        	dataType    : 'json',
	       	 	cache       : false,
				data: submitData,
				success: function (data) {
					var messageHtml = "<h2>Update Complete</h2>";
					messageHtml += "<p>Your request for a background check has been submitted.</p>";
					messageHtml += "<b>Thank you!</b>";
					$("#backgroundRequestForm").html(messageHtml);
				}
			});
			}
		});

});

</script>


<div id="backgroundRequestForm" align="center">
	
	<form action="" enctype="multi-part/form-data" method="POST" name="BCRequestForm" id="BCRequestForm">
	<input type="hidden" name="techID" value="<?=$_SESSION['TechID'] ?>" />
	<input type="hidden" name="Bg_Test_ReqDate_Lite" value="<?php echo Date('Y-m-d H:i:s'); ?>" />
	<input type="hidden" name="BgFirstName" id="BgFirstName" value="<?=$_SESSION["UserObj"][0]["FirstName"]?>" />
	<input type="hidden" name="BgLastName" id="BgLastName" value="<?=$_SESSION["UserObj"][0]["LastName"]?>" />
	<input type="hidden" name="BgPrimaryEmail" id="BgPrimaryEmail" value="<?=$_SESSION["UserObj"][0]["PrimaryEmail"]?>" />
		<table id="BCRequestTable">
			<thead>
				<tr>
					<th colspan="2"><h1>Background Check Request</h1></th>
				</tr>
			</thead>
			<tfoot>
				<tr class="break">
					<td colspan="2"><input id="submitRequest" type="submit" value="Update"/></td>
				</tr>
			</tfoot>
			<tbody>
				<tr>
					<td><table>
						<tr>
							<td class="label">FS Tech ID</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td class="info"><?php echo $_SESSION["TechID"];?></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td class="label">First Name</td>
							<td class="label">Last Name</td>
						</tr>
						<tr>
							<td class="info" id="Firstname"><?php echo $_SESSION["UserObj"][0]["FirstName"]?></td>
							<td class="info" id="Lastname"><?php echo $_SESSION["UserObj"][0]["LastName"]?></td>
						</tr>
					</table></td>
				</tr>
				<tr><td>
				<table id="breaks">
					<tr class="break">
						<td class="label">Primary Email</td>
						<td class="required">* Required</td>
					</tr>
					<tr>
						<td class="info" id="PrimaryEmail"><?php echo $_SESSION["UserObj"][0]["PrimaryEmail"]?></td>
						<td><input type="checkbox" id="correctEmail" name="correctEmail" value="1" />This is the correct email address.</td>
					</tr>
					<tr>
						<td colspan="2" class="notes">(Please use this email address when sending your payment via PayPal)</td>
					</tr>
				</table>
				</td></tr>
				<tr><td>
				<table>
					<tr class="break">
						<td class="label">I request a Basic Background Check</td>
						<td><input type="checkbox" id="Bg_Test_Req_Lite" name="Bg_Test_Req_Lite" value="1" />$19 (see instructions if NY)</td>
					</tr>
				</table>
				</td></tr>
			</tbody>
		
		</table>
	</form>
</div>

<!--- End Content --->
<?php require ("../footer.php"); ?>
		
