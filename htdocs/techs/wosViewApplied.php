<?php
/**
 * wosViewApplied.php
 * Page for Applied Work Order
 * @author Alex Scherba
 */
$page   = 'techs';
$option = 'wos';

require ("../header.php");
require ("../navBar.php");

if ( !empty($_GET['id']) ) {
    $win = $_GET['id']+0;
?>
<script type="text/javascript" src="/widgets/js/FSWidgetAppliedWO.js"></script>
<script type="text/javascript">
var wd;
$(document).ready( function() {
    wd = new FSWidgetAppliedWO({container:'appliedWO',win:'<?php echo$win?>'});
    wd.show();
});
var nw;
function changeBid(url){ nw=window.open(url,'changeBid','height=325,width=450,left=100,top=100,resizable=yes,scrollbars=yes,toolbar=no,status=no');if (window.focus){nw.focus()}} function goBack(){window.history.go(-1)}
</script>
<div id="appliedWO"></div>
<?php }else{?>
<div id="appliedWO">No results.</div>
<?php }?>
<?php require ("../footer.php"); ?>