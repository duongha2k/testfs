<?php
	$urlParts = explode('.', $_SERVER['HTTP_HOST']);
	$template = $urlParts[0];
	$checkIndexDisplay = $template;
	if ($checkIndexDisplay == "fsplusone"){
	    header("Location: https://www.fieldsolutions.com/techs/dashboard.php");
	    exit;
	}

    $page   = 'techs';
    $option = 'dashboard';
    require      '../header.php';
    $_GET['v']   = ( empty($_GET['v']) ) ? NULL : $_GET['v'];
    require_once("../headerStartSession.php");
    require_once dirname(__FILE__).'/../../includes/modules/common.init.php';
    $search = new Core_Api_TechClass;

	$authData = array('login'=>$_SESSION["UserName"], 'password'=>$_SESSION["UserPassword"]);

	$user = new Core_Api_TechUser();
	$user->checkAuthentication($authData);    
?>
<?php require ("../navBar.php"); 
$pointsSummary = Core_FSPlusOne::getTechPointSummary($_SESSION['TechID']);
if(!$pointsSummary){
	$pointsSummary['allPoints'] = "0";
	$pointsSummary['currentPoints'] = "0";
}
$isEligible = Core_FSPlusOne::isEligible($_SESSION['TechID']);

if($_REQUEST['activated'] == "false"){
	Core_Tech::confirmTechRegistration($user->getEmail(), $user->getActivationCode());
}
//075
$techinfo = new Core_Tech($_SESSION['TechID']);
$country = $techinfo->getCountry();
unset($techinfo);
//end 075
?>
<script type="text/javascript" src="/library/jquery/jquery.bgiframe.min.js"></script>
<script type="text/javascript" src="/library/js/excanvas.js"></script>
<script type="text/javascript" src="/library/jquery/jquery.bt.min.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.client.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.easing.pack.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-mod.js"></script>
<script type="text/javascript" src="/widgets/js/functions.js"></script>
<link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" media="all" />

<script>
$(document).ready(function(){
	if($.client.os == 'Windows'){
		if($.client.browser == 'Firefox'){
			$('#pointsSummary').css('top', '120px');
			
		}
		if($.client.browser == 'Explorer'){
		
			$('#pointsSummary').css('top', '121px');
		}
	}

	var techActivated = '<?=$_REQUEST['activated']?>';
	var techAcceptTerms = '<?=$_REQUEST['acceptTerms']?>';

	if(techActivated == "false"){
		var messageHtml = "<h2>Thanks for registering!</h2><p>Please visit your Technician Profile using the My Profile link on the left sidebar ensure that your contact information is correct. If you have not received a Welcome email, check your email account's Junk Mail folder and add FieldSolutions as a safe sender. We're looking forward to working with you!</br></br> -Your FieldSolutions Team</p>";
		$("<div></div>").fancybox(
				{
					'autoDimensions' : false,
					'width'	: 420,
					'height' : 225,
					'transitionIn': 'none',
					'transitionOut' : 'none',
					'content' : messageHtml
				}
			).trigger('click');
	}

	if(techAcceptTerms == "No"){
		var messageHtml = "<h1 style='text-align:center;color: #FF0000;'>Attention!</h1><p>You must accept Terms of Use to be visible to clients.</br></br><a href='../content/Terms_Use.pdf'>Click Here</a> to view the Terms of Use<br /><a href='javascript:void(0);' onclick='acceptTerms();'>Click Here</a> to accept the Terms of Use (Acceptance without viewing is a binding agreement)</p>";
		
		$("<div></div>").fancybox(
				{
					'autoDimensions' : false,
					'width'	: 420,
					'height' : 175,
					'transitionIn': 'none',
					'transitionOut' : 'none',
					'content' : messageHtml
				}
			).trigger('click');
	}

	$(".certForms").click(function(){
		if($(this).attr('rel') == "insurance"){
			var messageHtml = "<h2>FieldSolutions Insurance Coverage</h2>";
			messageHtml += "<p>FieldSolutions insures our clients against risk of work on-site. As such, we never put you through the hassle of producing your insurance overages, we don’t verify whether your payments are current; and most important we don’t charge you a fee like other systems for coverages. Insurance coverage is just one reason FieldSolutions is the most preferred place to get technology field work. Remind your clients that you want to get your work and paid through FieldSolutions.</p>";

			$("<div></div>").fancybox(
					{
						'autoDimensions' : false,
						'width'	: 420,
						'height' : 225,
						'transitionIn': 'none',
						'transitionOut' : 'none',
						'content' : messageHtml
					}
				).trigger('click');
	
		}else if($(this).attr('CompTIA') == "CompTIA"){
			$("<div></div>").fancybox(
					{
						'autoDimensions' : true,
						'width'	: 900,
						'height' : 580,
						'transitionIn': 'none',
						'transitionOut' : 'none',
						'type': 'iframe',
						'href': $(this).attr('rel'),
                                                'autoScale' : false
					}
				).trigger('click');
	
		}
                else
                {
		
			$("<div></div>").fancybox(
					{
						'autoDimensions' : false,
						'width'	: 900,
						'height' : 850,
						'transitionIn': 'none',
						'transitionOut' : 'none',
						'type': 'iframe',
						'href': $(this).attr('rel')
					}
				).trigger('click');
		}
                
	
	});
});

function acceptTerms(){
	techID = '<?=$_SESSION['TechID']?>';
	showLoader();
	 $.ajax({
         type: "POST",
         url: "/techs/ajax/updateTechInfo.php",
         dataType    : 'json',
         cache       : false,
         data: {AcceptTerms: "Yes",Date_TermsAccepted: "Now", techID: techID},
         success: function (data)
         {
             hideLoader();
             if(window.location.href.indexOf('?') > -1) {
            	  window.location = window.location.href.substr(0, window.location.href.indexOf('?'));
            }
         }
     });
}

function updatePaymentInfo(){
	document.location = "/techs/updateProfile.php?data=showPaymentInfo";
}

var loaderVisible = false;
function showLoader(){
    if(!loaderVisible){
        $("div#loader").fadeIn("fast");
        loaderVisible = true;
    }
}

function hideLoader(){
    if(loaderVisible){
        var loader = $("div#loader");
        loader.stop();
        loader.fadeOut("fast");
        loaderVisible = false;
    }
}
</script>
<style type="text/css">
div#loader{
        display: none;
        width:100px;
        height: 100px;
        position: fixed;
        top: 50%;
        left: 50%;
        background:url(/widgets/images/loader.gif) no-repeat center #fff;
        text-align:center;
        padding:10px;
        font:normal 16px Tahoma, Geneva, sans-serif;
        border:1px solid #666;
        margin-left: -50px;
        margin-top: -50px;
        z-index:200;
        overflow: auto;
    }
</style>
<div id="loader">
    Loading...
</div>
<div id="widgetContainer" class="wide_content">

<?php if(empty($isEligible['errors'])){ ?>
<div id="FSPlusOne" style="margin-left: 9px;" >
<form method="POST" name="FSPlusOne" action="https://www.fsplusone.com/index.php?s_id=&u_id=login">
<input type="hidden" name="FSPLUSONE_ID" value="T<?=$_SESSION['TechID'];?><?=$_SESSION['UserObj'][0]['LastName'];?>" />
<input type="image" width="120" height="22" border="0" title="" alt="" src="/widgets/css/images/FSPlusOne.jpg" id="comingSoon"  style="position: absolute; top: 121px; border: none;" bt-xtitle="Click Button to Register">
<a target="_blank" href="http://www.fsplusone.com/index.php?s_id=<?=$_SESSION['TechID']?>&u_id=">
</a>
</form>

	<div id="pointsSummary" style="position: absolute; top: 120px; margin: 0 10px 0 130px; color: #6699CC; font-size: 11px;">
		<span id="lifetimePoints" style="float: left; width: 150px; position: absolute; top: 0px; margin: 0;">Lifetime Points: <b><?=$pointsSummary['allPoints'] ?></b></span><br />
		<span id="availablePoints" style="margin: 0px;">Available Points: <b><?=$pointsSummary['currentPoints'] ?></b></span>
	</div>
</div>
<?php } ?>
<div class="inner10" id="dashboardContainer">
	<?php include('leftNavContent.php'); ?>
	<div class="right_col">
	<br style="clear: both;" />
		<div id="availableContainer" style="margin-bottom: 10px;">
			<div class="bg2" style="overflow: hidden; //zoom: 1;">
			<h3>Available Work</h3>
            <div class="fr" style="margin-right:-1px;">
                <div class="toolBtn">
                <select id="right_sortSelect" style="width:110px;" onchange="return td_right.sortTool().applySort('quick');"></select>
                </div>
                <div class="toolBtn">
                    <select id="right_directionSelect" onchange="return td_right.sortTool().applySort('quick');"></select>
                </div>
            </div>
            </div>
            <div id="rightWidgetContainer" class="scroll" style="border:1px solid #000000;"></div>
		</div>
        
        <div id="assignedContainer" style="margin-bottom: 10px;">
            <div class="bg2" style="overflow: hidden; //zoom: 1;">
            	<h3>My Assigned Schedule (Start date/time order)<a onclick="detailObject.onInit.showMyscheduleDownload(this);//296" href="javascript:;">
					<img width="25" height="25" border="0" alt="" src="/images/CalendarIcon.gif" title="">
					</a></h3>
				<div class="fr">
                    <div class="toolBtn">
                    <select id="top_sortSelect" style="max-width:110px;" onchange="return td.sortTool().applySort('quick');"></select>
                    </div>
                    <div class="toolBtn">
                        <select id="top_directionSelect" onchange="return td.sortTool().applySort('quick');"></select>
                    </div>
                   <!--  <a onclick="tpm.openPopupWin('assignedWO');" href="javascript:///">Show all</a> -->
                </div>
				Next 10 Work Orders
			</div>
			<div id="topWidgetContainer" class="scroll" style="border:1px solid #000000;"></div>
		</div>

		<div id="incompleteContainer">
			<div class="bg2" style="overflow: hidden; //zoom: 1;">
                <h3>Past Date Work Orders</h3>
				<div class="fr" style="margin-right:-10px;">
					<div class="toolBtn">
					<select id="left_sortSelect" style="width:110px;" onchange="return td_left.sortTool().applySort('quick');"></select>
					</div>
					<div class="toolBtn">
						<select id="left_directionSelect" onchange="return td_left.sortTool().applySort('quick');"></select>
					</div>
				</div>
				<span style="font-size: 13px;">(needs action: mark as complete to be paid)</span>
            </div>
            <div id="leftWidgetContainer" class="scroll" style="border:1px solid #000000;"></div>
		</div>
	</div>
</div>
<?php
	$date = new DateTime();
	$date->modify('-1 day');
	$past_date = str_replace ("-", "s", $date->format('m-d-Y'));
?>
<iframe src="/widgets/dashboard/tech-dashboard/assigned/tab/techassigned/rt/iframe/end_date/<?=$past_date;?>/sort1/start/dir1/asc/version/Full/" onload="document.getElementById('leftWidgetContainer').innerHTML = this.contentWindow.document.body.innerHTML" style="position: absolute; left: -10000px; top: -10000px;"></iframe>
<iframe src="/widgets/dashboard/tech-dashboard/assigned/tab/techassigned/rt/iframe/start_date/<?=date('m');?>s<?=date('d');?>s<?=date('Y');?>/sort1/start/dir1/asc/version/Full/" onload="document.getElementById('topWidgetContainer').innerHTML = this.contentWindow.document.body.innerHTML" style="position: absolute; left: -10000px; top: -10000px;"></iframe>
<!--075-->
<iframe src="/widgets/dashboard/tech-dashboard/available/rt/iframe/distance/50/tab/techavailable/start_date/<?=date('m');?>s<?=date('d');?>s<?=date('Y');?>/sort1/start/dir1/asc/version/Full/country/<?=$country?>" onload="document.getElementById('rightWidgetContainer').innerHTML = this.contentWindow.document.body.innerHTML" style="position: absolute; left: -10000px; top: -10000px;"></iframe>
<!--end 075-->
<script type="text/javascript" src="/widgets/js/FSTechDashboard.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetSortTool.js"></script>
<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>

<?php  //Check if logged in, if not redirect ?> 
<script type="text/javascript">
    var showAllTimer = null;
	try {
        window._techId = '<?=$_GET['v']?>';
		var test = v;
		var techZip = z;
		//if (v != "<?=$_GET["v"]?>")
		//	window.location.replace("<?=$_SERVER['PHP_SELF']?>?v=" + v);
		
		/*
		if (acceptTerms == "No") {
			alert("You must accept terms in your profile before viewing available work");
			document.location.replace("./");
		}
		*/
		
	}
	catch (e) {
		//window.location.replace("./");
	}
    eventApplyWo = function(){
        quickBid._roll = roll;
        quickBid.eventbutton = jQuery("#qaapplybid");
        quickBid.afterBid = function(){
            window.location.reload();
        };
        quickBid.initBidForm();
    }    
    
    
    var td;             // Widgets object
    var td_left;        // Widgets object
    var td_right;       // Widgets object
    var roll;           // Rollover object

    $(document).ready(function(){

	<?php if ($user->getDeactivated() == 1):?>
		disableLinks($("div.left_col a:not(.allowLinkTD)"));
		if ($.cookie('close_deactivated') != 1) {
			showTechDeactivatedMsg(function() {
				$.cookie('close_deactivated', 1);
				$.ajax({
				   url: '/techs/ajax/deactivationAck.php',
				   data: {id: '<?=$_SESSION['TechID']?>'},
				   async: false,
				   cache: false
				});
			});
		}
	<?php endif;?>
        roll          = new FSPopupRoll();
        td            = new FSTechDashboard({container:'topWidgetContainer',tab:'techassigned'});
        d = new Date();
        td.filters().dateStart((d.getMonth()+1)+'/'+d.getDate()+'/'+d.getFullYear());
        td_left       = new FSTechDashboard({container:'leftWidgetContainer',tab:'techassigned'});
        td_left.filters().dateEnd((d.getMonth()+1)+'/'+d.getDate()+'/'+d.getFullYear());
        td_right      = new FSTechDashboard({container:'rightWidgetContainer',tab:'techavailable'});
        td_right.filters().dateStart((d.getMonth()+1)+'/'+d.getDate()+'/'+d.getFullYear());
        td_right.filters().distance('50');
        wManager.add(td,         'assigned');
        wManager.add(td_left,      'past');
        wManager.add(td_right,  'available');
        td.sortTool(new FSWidgetSortTool('top_sortSelect', 'top_directionSelect', td, roll, 'quick'));
        td_left.sortTool(new FSWidgetSortTool('left_sortSelect', 'left_directionSelect', td_left, roll, 'quick'));
        td_right.sortTool(new FSWidgetSortTool('right_sortSelect', 'right_directionSelect', td_right, roll, 'quick'));

        var p = td.getParams();
        p.nonav = true;
        td.sortTool().initSortTool();
        td_left.sortTool().initSortTool();
        td_right.sortTool().initSortTool();

        if (!techInfo.expComplete ) $("#todoExperience").removeClass('displayNone');
        if (!techInfo.certComplete ) $("#todoCertifications").removeClass('displayNone');
        if (!techInfo.equipComplete ) $("#todoEquipment").removeClass('displayNone');
        if (!techInfo.W9) $("#todoW9").removeClass('displayNone');
        if (!techInfo.Cert_Hallmark_POS) $("#todoHallmark").removeClass('displayNone');
        if (!techInfo.NCR_Basic_Cert) $("#todoNCR").removeClass('displayNone');
        if (!techInfo.Starbucks_Cert) $("#todoStarbucks").removeClass('displayNone');
        if (!techInfo.FLSCSP_Rec) $("#todoFLS_CSP").removeClass('displayNone');
        if (!techInfo.Cert_Maurices_POS) $("#todoMarices").removeClass('displayNone');
        if (!techInfo.CORE_Cert) $("#todoCore").removeClass('displayNone');
        $('#comingSoon').bt({
        	  fill: '#ffffff',
        	  strokeStyle: '#000000'
        });
    });
</script>

<!-- Begin Splash Content hack -->
<?php
$records = caspioSelectAdv("TR_Master_List", "FLSID, FLSCSP_Rec, Push2Tech", "TechID = '{$_SESSION['TechID']}'", "", false, "`", "|^",false);
foreach ($records as $tech) {
	$fields = explode("|^", $tech);
	$FLSID = trim($fields[0], "`");
	$FLSCSP_Rec = (trim($fields[1], "`") == 'True') ? 'Yes' : 'No';
	$techRefresh = (trim($fields[1], "`") == 'False') ? 'Yes' : 'No';
	echo "<script type=\"text/javascript\">var techRefresh = '".$techRefresh."';</script>";
}
?>
<? if( intval (date("Ymd")) < 20120913 ): ?>
	<!--div id="window_RED">
		<div id="windowTop_RED">
			<div id="windowTopContent_RED">Techs with Cable Certifier Needed Nationwide!</div>
			<img src="images/window_close.jpg" id="windowClose_Two" />
		</div>
	<div id="windowBottom_RED"><div id="windowBottomContent_RED">&nbsp;</div></div>
	<div id="windowContent_RED">
		<div>
		A large, nationwide rollout project is in need of <span style="font-weight:bold;text-decoration:underline;margin:0px;">technicians who own a cable certifier</span>. CABLE CERTIFIER has been added as a new Equipment Item in the Tools section of your Technician Profile. Check the box for Cable Certifier to <span style="font-weight:bold;margin:0px;">indicate that you have this equipment (in advance of assignment) and are capable of operating it.</span><br/>

		Until September 30, 2012: <span style="font-weight:bold;margin:0px;">Receive 500 FS-PlusOne Bonus Reward Points</span> for updating your profile to indicate if you own this piece of equipment. Points will be awarded following the completion of the bonus period.<br/>

		<a href="/techs/FSPlusOne_Cable_Certifier_Promotion_20120910.pdf" target="_blank">Read More...</a><br/>

		Please contact FieldSolutions if you have any questions, <a href="mailto:support@fieldsolutions.com" style="margin:0px;">support@fieldsolutions.com</a>.<br/>
		</div>
	</div>
	<img src="images/window_resize.gif" id="windowResize_Two" />
	</div-->

<?php endif; ?>
<?php if (intval (date("Ymd")) >= 20130111 && intval (date("Ymd")) < 20130116): ?>
	<!--
        <style type="text/css">
            ul.a {list-style-type:disc;}
        </style>
    <div id="window_CompTIA">
        <div id="windowTop_CompTIA">
            <div id="windowTopContent_CompTIA">Discounted CompTIA Certifications!</div>
                <img src="images/window_close.jpg" id="windowClose_CompTIA" />
            </div>
            <div id="windowBottom_CompTIA">
                <div id="windowBottomContent_CompTIA">&nbsp;</div>
            </div>
            <div id="windowContent_CompTIA">
                <div>
                    Save 15% off Exams and 10% off Training Material &ndash; Only for FieldSolutions Techs<br/>
                    <ul class="a">
                        <li><span style="font-weight:bold;margin:0px;">Click on &ldquo;CompTIA Industry Certifications&rdquo; in the left navigation bar of your technician homepage to receive these exclusive discounted rates.</span></li>
                        <li><span style="text-decoration:underline;margin:0px;">NEW!! CompTIA Certification categories</span> have been added to the FieldSolutions technician profile. Open your technician profile and enter your CompTIA Certification Code (received upon passing) in the appropriate certification category for PDI+, Server+, or Network+.</li>
                        <li><span style="text-decoration:underline;margin:0px;">AND MORE: Receive 500 FS-PlusOne Reward Points</span> for each of the recommended CompTIA certifications: A+, PDI+, Server+, Network+. Register with FS-PlusOne by clicking the FS-PlusOne logo in the top left corner of your technician homepage.</li>
                    </ul>
                    <a href="/techs/CompTIA Cert Discount - Read More PDF.pdf" target="_blank" style="margin-left:0px;">Read More...</a><br/>
                    Sincerely,<br/>
                    Your FieldSolutions Team<br/>
                </div>
            </div>
            <img src="images/window_resize.gif" id="windowResize_CompTIA" />
        </div>
        -->
    <?
endif;
//end 591
?>

<style type="text/css">
#window_BLUE ul{
	list-style-type: disc;
}
#window_BLUE ul li{
 font-size: 12px;
 line-height: 13px;
}

#window_BLUE a{
	margin: 0px;
}
</style>
<?php if (intval (date("Ymd")) <= 20130924): ?>
<div id="window_BLUE">
	<div id="windowTop_BLUE">
		<div id="windowTopContent_BLUE">CCNA Techs Needed! FS-PlusOne Bonus for Profile Update</div>
		<img src="../techs/images/window_close.jpg" id="windowClose" />
	</div>
<div id="windowBottom_BLUE"><div id="windowBottomContent_BLUE">&nbsp;</div></div>
<div id="windowContent_BLUE" style="font-size: 12px;">
	<p style="font-family: Arial, Helvetica, sans-serif; font-weight: bold">Add your CCNA Certification to your Profile to Qualify for Upcoming Work and Earn FS-PlusOne Bonus Rewards</p>
    <p style="font-family: Arial, Helvetica, sans-serif;">
		New clients of FieldSolutions have Cisco network jobs across the country that require CCNA Technicians. To qualify for these upcoming projects and service calls, take 30 seconds to add your CCNA validation number to your FieldSolutions profile. This 16-digit validation number is listed on your CCNA certificate. 
        <ul>
            <li>Click &quot;My Profile&quot; in the left menu bar</li>
            <li>Open Section 7 for Industry Certifications</li>
            <li>Click Edit and enter your CCNA validation number in the open field</li>
            <li>Click the SAVE icon in the upper right corner of that section</li>
        </ul>
	</p>
    <p style="font-family: Arial, Helvetica, sans-serif;"><span style="margin: 0px; font-weight: bold; text-decoration:underline">First 300 Techs to Add their CCNA Validation Code will Win 3,000 FS-PlusOne Rewards Points!</span><br/><span style="margin: 0px">
    You must enter a valid CCNA number and be registered with FS-PlusOne, FieldSolutions' technician rewards program, to qualify for these bonus rewards. To register for the FS-PlusOne rewards program click on the FS-PlusOne logo in the upper left corner of this page.</span>
    </p>
    <p>Please contact FieldSolutions if you have any questions, <a href="mailto:support@FieldSolutions.com">support@FieldSolutions.com</a>.</p>
	<p style="padding: 0px; font-family: Arial, Helvetica, sans-serif;">
		Thank you,<br/>
Your FieldSolutions Team
	</p>
</div>
<img src="images/window_resize.gif" id="windowResize_Two" />

<!--div id="window_BLUE">
	<div id="windowTop_BLUE">
		<div id="windowTopContent_BLUE">FieldSolutions Announces new FS-Rush&trade; Auto-Assignment Tool</div>
		<img src="../techs/images/window_close.jpg" id="windowClose" />
	</div>
<div id="windowBottom_BLUE"><div id="windowBottomContent_BLUE">&nbsp;</div></div>
<div id="windowContent_BLUE" style="font-size: 12px;">
	<p style="font-family: Arial, Helvetica, sans-serif;">September 16, 2013 - FieldSolutions, North America's premier contingent technology field resource provider releases FS-Rush&trade; a new first bidder automatic assignment tool. FS-Rush clients will be publishing work and assigning it to the first matching applicant.</p>
    <p style="font-family: Arial, Helvetica, sans-serif;">
		Additionally, now if you are registered as a provider on Work Market through our new alliance, you MAY also see work orders appearing on that platform via skill specific FieldSolutions 'groups'. This work will always be posted in the FieldSolutions system as well, and with the addition of providers in Work Market speed to bid will be more competitive.  Regardless of whether the application comes through FieldSolutions or from eligible providers on Work Market, the FS-Rush work orders will be auto-assigned to the first matching applicant.
	</p>
    <p style="font-family: Arial, Helvetica, sans-serif;"><span style="font-weight: bold">FS-Rush&trade;: New Word, Faster Response Needed</span>
        <ul>
            <li>Clients engaged in the break/fix and maintenance work depend on fast response times from technicians to meet strict on-site service level agreements (SLAs)</li>
            <li>The expanded provider base, now including Work Market providers, may result in faster work order assignment</li>
            <li>The provider will be paid through the platform they bid on. Pay for work completed through Work Market will be made 30 days after client approval. Pay through FieldSolutions will remain the week after client approval.</li>
            <li>To bid faster and win more work, download FS-Mobile, FieldSolutions mobile smartphone app for iPhone and Android and bid while working.</li>
        </ul>
	</p>
    <p style="font-family: Arial, Helvetica, sans-serif;">Questions? Email: <a href="mailto:Support@FieldSolutions.com">Support@FieldSolutions.com</a>
</p>
	<p style="padding: 0px; font-family: Arial, Helvetica, sans-serif;">
		Thank you,<br/>
Your FieldSolutions Team
	</p>
</div>
<img src="images/window_resize.gif" id="windowResize_Two" /-->
<?php endif;?>
<!--
<div id="window_BLUE">
	<div id="windowTop_BLUE">
		<div id="windowTopContent_BLUE">New Release: Tech-to-Client Q&A within Work Orders</div>
		<img src="../techs/images/window_close.jpg" id="windowClose" />
	</div>
<div id="windowBottom_BLUE"><div id="windowBottomContent_BLUE">&nbsp;</div></div>
<div id="windowContent_BLUE" style="font-size: 12px;">
	<p style="font-family: Arial, Helvetica, sans-serif;">January 9, 2013 - Today FieldSolutions released all new work order Q&A tools designed to streamline tech-to-client communications within the work order.</p>

	<p style="font-family: Arial, Helvetica, sans-serif;">
		Q&A Messaging		
		<ul style="padding: 0px 25px 0px 25px; list-style: disc none;">
			<li>Now, you can send Q&A messages to clients before applying to ask for clarification as needed such as scope of work, travel, reimbursements, etc., before you even submit a bid!</li>
			<li>Now you can message (via Q&A) throughout the life of the work order and the coordinators will see them in their new Notices area! And FieldSolutions' new Q&A Log preserves the messages for your future reference within that work order.</li>
		</ul>
	</p>
	
	<p style="font-family: Arial, Helvetica, sans-serif;">
		NEW SKILLS: Calling all Cabling Techs: We've added a new Cabling skill category, Tools, and Experience Questions to the Technician Profile. Update your profile today!	
		<ul style="padding: 0px 25px 0px 25px; list-style: disc none;">
			<li>Skilled at Central Office Cabling? Rate yourself in Section 4 of your Profile - and don’t forget to check the 'Cabling Experience' questions at the end of Sec. 4</li>
			<li>Do you have specialized cabling tools like a lacing needle or a wire wrap gun? Let clients know by filling out Section 5</li>
		</ul>
	</p>
	<p style="padding: 0px; font-family: Arial, Helvetica, sans-serif;">
		Thank you,<br />
		Your FieldSolutions Team
	</p>
	<br/><br/>
</div>
<img src="images/window_resize.gif" id="windowResize_Two" />
</div>
-->
<?  if( date("Ymd") >= 20130306 && strtotime("now") < strtotime("2013-03-07 03:00:00") ){ ?>
		<div id="window_BLUE" style="display: block">
                <div id="windowTop_BLUE">
                        <div id="windowTopContent_BLUE">Scheduled Site Maintenance</div>
                        <img src="images/window_close.jpg" id="windowClose" />
                </div>
        <div id="windowBottom_BLUE"><div id="windowBottomContent_BLUE">&nbsp;</div></div>
        <div id="windowContent_BLUE">
			<p>FieldSolutions Technicians,</p>
			<p>Tonight from 12:00 AM - 1:00 AM EST, FieldSolutions will be unavailable due to scheduled site maintenance.</p>

			
<p style="clear:both">
			Thank you for your patience,</br>
			Your FieldSolutions Team
		</p>
            
        </div>
        <img src="images/window_resize.gif" id="windowResize" />
</div>
<? }?>

<? if( intval (date("Ymd")) < 20130806 ): ?>
<div id="window_ORANGE" style="display: block">
	<div id="windowTop_ORANGE">
	<div id="windowTopContent_ORANGE">FieldSolutions CheckINN&trade; Hotel Savings Card</div>
		<img src="images/window_close.jpg" id="windowClose" />
	</div>
	<div id="windowBottom_ORANGE"><div id="windowBottomContent_ORANGE">&nbsp;</div></div>
	<div id="windowContent_ORANGE">
		<p>FieldSolutions now offers the FieldSolutions CheckINN&trade; Hotel Discount Card to all technicians for use at over 10,800 participating hotels. Using your personal FieldSolutions CheckINN&trade; Card, you'll receive discounted hotel rates up to 50% less than standard corporate rates.<a href="/techs/documents/FieldSolutionsCheckINNHotelSavingsProgram-OverviewforFieldSolut.pdf" target="_blank">Read more about this exclusive offer and how to register.</a></p>
		
		<p>For all FieldSolutions work that requires overnight travel, preference will be given to technicians who have already registered for their CheckINN&trade; Hotel Savings Card. <a href="javascript:void(0);" onclick="javascript:$('#showCheckInnPopup', window.parent.document).trigger('click');">Click HERE to Enroll</a>. While registering, you MUST enter your FieldSolutions' Tech ID# in the 'Company' field. This lets clients see that you have registered and are able to utilize the FieldSolutions CheckINN&trade; Card's discounted hotel rates.</p>
		
		<p>As promotion of both of these exclusive offerings, earn <b>5,000 FS-PlusOne Rewards points</b> when you complete the following:</p>
		<ul style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; margin-left: 40px;">
			<li style="list-style-type: disc; padding-bottom: 10px;">1) Register for FieldSolutions' CheckINN&trade; Hotel Discount Card</li>
			<li style="list-style-type: disc; padding-bottom: 10px;">2)	Upload a Profile Photo to your technician profile</li>
			<li style="list-style-type: disc; padding-bottom: 10px;">3)	Upload a Resume to your technician profile</li>
			<li style="list-style-type: disc; padding-bottom: 10px;">4)	Complete your profile Skill Self-Ratings (new skill categories have recently been added)</li>
		</ul>
		
		<p>Must complete all four items and be registered with FS-PlusOne to earn these rewards. <u>Bonus ends September 1, 2013.</u></p>
		
		<p>Please contact FieldSolutions if you have any questions, <a style="margin: 0 !important;" href="mailto:support@fieldsolutions.com">support@fieldsolutions.com</a>.</p>

		<p style="clear:both">
			Sincerely,</br>
			Your FieldSolutions Team
		</p>
	</div>
	<img src="images/window_resize.gif" id="windowResize" />
</div>
<? endif; ?>

<script type="text/javascript"> 
$(document).ready(
  function()
  {
	//if ($.cookie('close_blue') != 1) $("#window_BLUE").show();
	//else $("#window_BLUE").hide();
	//if ($.cookie('close_green') != 1) $("#window_GREEN").show();
	//else $("#window_GREEN").hide();
    $('#windowOpen_GREEN').bind(
      'click',
      function() {
        if($('#window_GREEN').css('display') == 'none') {
          $(this).TransferTo(
            {
              to:'window_GREEN',
              className:'transferer_Two',
              duration: 400,
              complete: function()
              {
                $('#window_GREEN').show();
              }
            }
          );
        }
        this.blur();
        return false;
      }
    );
    $('#windowClose_Two').bind(
      'click',
      function()
      {
	  $.cookie('close_green', 1);

        $('#window_GREEN').TransferTo(
          {
            to:'windowOpen_GREEN',
            className:'transferer_GREEN',
            duration: 400
          }
        ).hide();
      }
    );
    $('#windowMin').bind(
      'click',
      function()
      {
        $('#windowContent_GREEN').SlideToggleUp(300);
        $('#windowBottom_GREEN, #windowBottomContent_GREEN').animate({height: 10}, 300);
        $('#window_GREEN').animate({height:40},300).get(0).isMinimized = true;
        $(this).hide();
        $('#windowResize_Two').hide();
        $('#windowMax_GREEN').show();
      }
    );
    $('#windowMax_GREEN').bind(
      'click',
      function()
      {
        var windowSize_GREEN = $.iUtil.getSize(document.getElementById('windowContent_GREEN'));
        $('#windowContent_GREEN').SlideToggleUp(300);
        $('#windowBottom_GREEN, #windowBottomContent_GREEN').animate({height: windowSize.hb + 13}, 300);
        $('#window_GREEN').animate({height:windowSize_GREEN.hb+43}, 300).get(0).isMinimized = false;
        $(this).hide();
        $('#windowMin_GREEN, #windowResize_Two').show();
      }
    );
    $('#window_GREEN').Resizable(
      {
        minWidth: 200,
        minHeight: 60,
        maxWidth: 700,
        maxHeight: 400,
        dragHandle: '#windowTop_GREEN',
        handlers: {
          se: '#windowResize_GREEN'
        },
        onResize : function(size, position) {
          $('#windowBottom_GREEN, #windowBottomContent_GREEN').css('height', size.height-33 + 'px');
          var windowContentEl_GREEN = $('#windowContent_GREEN').css('width', size.width - 25 + 'px');
          if (!document.getElementById('window_GREEN').isMinimized) {
            windowContentEl_GREEN.css('height', size.height - 48 + 'px');
          }
        }
      }
    );
  }
);
</script>

		<script type="text/javascript"> 
		$(document).ready(
		  function()
		  {
		    $('#windowOpen_BLUE').bind(
		      'click',
		      function() {
		        if($('#window_BLUE').css('display') == 'none') {
		          $(this).TransferTo(
		            {
		              to:'window_BLUE',
		              className:'transferer_Two',
		              duration: 400,
		              complete: function()
		              {
		                $('#window_BLUE').show();
		              }
		            }
		          );
		        }
		        this.blur();
		        return false;
		      }
		    );
		    $('#windowClose').bind(
		      'click',
		      function()
		      {
				  $.cookie('close_blue', 1);
		        $('#window_BLUE').TransferTo(
		          {
		            to:'windowOpen_BLUE',
		            className:'transferer_Two',
		            duration: 400
		          }
		        ).hide();
		      }
		    );
		    $('#windowMin').bind(
		      'click',
		      function()
		      {
		        $('#windowContent_BLUE').SlideToggleUp(300);
		        $('#windowBottom_BLUE, #windowBottomContent_BLUE').animate({height: 10}, 300);
		        $('#window_BLUE').animate({height:40},350).get(0).isMinimized = true;
		        $(this).hide();
		        $('#windowResize').hide();
		        $('#windowMax_BLUE').show();
		      }
		    );
		    $('#windowMax_BLUE').bind(
		      'click',
		      function()
		      {
		        var windowSize_BLUE = $.iUtil.getSize(document.getElementById('windowContent_BLUE'));
		        $('#windowContent_BLUE').SlideToggleUp(300);
		        $('#windowBottom_BLUE, #windowBottomContent_BLUE').animate({height: windowSize.hb + 13}, 300);
		        $('#window_BLUE').animate({height:windowSize_BLUE.hb+43}, 300).get(0).isMinimized = false;
		        $(this).hide();
		        $('#windowMin_BLUE, #windowResize').show();
		      }
		    );
		    $('#window_BLUE').Resizable(
		      {
		        minWidth: 500,
		        minHeight: 60,
		        maxWidth: 800,
		        maxHeight: 400,
		        dragHandle: '#windowTop_BLUE',
		        handlers: {
		          se: '#windowResize'
		        },
		        onResize : function(size, position) {
		          $('#windowBottom_BLUE, #windowBottomContent_BLUE').css('height', size.height-33 + 'px');
		          var windowContentEl_BLUE = $('#windowContent_BLUE').css('width', size.width - 25 + 'px');
		          if (!document.getElementById('window_BLUE').isMinimized) {
		            windowContentEl_BLUE.css('height', size.height - 48 + 'px');
		          }
		        }
		      }
		    );
		  }
		);
		</script>

<script type="text/javascript"> 
$(document).ready(
  function()
  {
	//if ($.cookie('close_blue') != 1) $("#window_BLUE").show();
	//else $("#window_BLUE").hide();
	//if ($.cookie('close_green') != 1) $("#window_GREEN").show();
	//else $("#window_GREEN").hide();
    $('#windowOpen_RED').bind(
      'click',
      function() {
        if($('#window_RED').css('display') == 'none') {
          $(this).TransferTo(
            {
              to:'window_RED',
              className:'transferer_Two',
              duration: 400,
              complete: function()
              {
                $('#window_RED').show();
              }
            }
          );
        }
        this.blur();
        return false;
      }
    );
    $('#windowClose_Two').bind(
      'click',
      function()
      {
	  $.cookie('close_RED', 1);

        $('#window_RED').TransferTo(
          {
            to:'windowOpen_RED',
            className:'transferer_RED',
            duration: 400
          }
        ).hide();
      }
    );
    $('#windowMin').bind(
      'click',
      function()
      {
        $('#windowContent_RED').SlideToggleUp(300);
        $('#windowBottom_RED, #windowBottomContent_RED').animate({height: 10}, 300);
        $('#window_RED').animate({height:40},300).get(0).isMinimized = true;
        $(this).hide();
        $('#windowResize_Two').hide();
        $('#windowMax_RED').show();
      }
    );
    $('#windowMax_RED').bind(
      'click',
      function()
      {
        var windowSize_RED = $.iUtil.getSize(document.getElementById('windowContent_RED'));
        $('#windowContent_RED').SlideToggleUp(300);
        $('#windowBottom_RED, #windowBottomContent_RED').animate({height: windowSize.hb + 13}, 300);
        $('#window_RED').animate({height:windowSize_RED.hb+43}, 300).get(0).isMinimized = false;
        $(this).hide();
        $('#windowMin_RED, #windowResize_Two').show();
      }
    );
    $('#window_RED').Resizable(
      {
        minWidth: 200,
        minHeight: 60,
        maxWidth: 700,
        maxHeight: 400,
        dragHandle: '#windowTop_RED',
        handlers: {
          se: '#windowResize_RED'
        },
        onResize : function(size, position) {
          $('#windowBottom_RED, #windowBottomContent_RED').css('height', size.height-33 + 'px');
          var windowContentEl_RED = $('#windowContent_RED').css('width', size.width - 25 + 'px');
          if (!document.getElementById('window_RED').isMinimized) {
            windowContentEl_RED.css('height', size.height - 48 + 'px');
          }
        }
      }
    );

	//if ($.cookie('close_blue') != 1) $("#window_BLUE").show();
	//else $("#window_BLUE").hide();
	//if ($.cookie('close_green') != 1) $("#window_GREEN").show();
	//else $("#window_GREEN").hide();
    $('#windowOpen_ORANGE').bind(
      'click',
      function() {
        if($('#window_ORANGE').css('display') == 'none') {
          $(this).TransferTo(
            {
              to:'window_ORANGE',
              className:'transferer_Two',
              duration: 400,
              complete: function()
              {
                $('#window_ORANGE').show();
              }
            }
          );
        }
        this.blur();
        return false;
      }
    );
    $('#windowClose').bind(
      'click',
      function()
      {
	  $.cookie('close_ORANGE', 1);

        $('#window_ORANGE').TransferTo(
          {
            to:'windowOpen_ORANGE',
            className:'transferer_ORANGE',
            duration: 400
          }
        ).hide();
      }
    );
    $('#windowMin').bind(
      'click',
      function()
      {
        $('#windowContent_ORANGE').SlideToggleUp(300);
        $('#windowBottom_ORANGE, #windowBottomContent_ORANGE').animate({height: 10}, 300);
        $('#window_ORANGE').animate({height:40},300).get(0).isMinimized = true;
        $(this).hide();
        $('#windowResize_Two').hide();
        $('#windowMax_ORANGE').show();
      }
    );
    $('#windowMax_ORANGE').bind(
      'click',
      function()
      {
        var windowSize_ORANGE = $.iUtil.getSize(document.getElementById('windowContent_ORANGE'));
        $('#windowContent_ORANGE').SlideToggleUp(300);
        $('#windowBottom_ORANGE, #windowBottomContent_ORANGE').animate({height: windowSize.hb + 13}, 300);
        $('#window_ORANGE').animate({height:windowSize_ORANGE.hb+43}, 300).get(0).isMinimized = false;
        $(this).hide();
        $('#windowMin_ORANGE, #windowResize_Two').show();
      }
    );
    $('#window_ORANGE').Resizable(
      {
        minWidth: 200,
        minHeight: 60,
        maxWidth: 700,
        maxHeight: 400,
        dragHandle: '#windowTop_ORANGE',
        handlers: {
          se: '#windowResize_ORANGE'
        },
        onResize : function(size, position) {
          $('#windowBottom_ORANGE, #windowBottomContent_ORANGE').css('height', size.height-33 + 'px');
          var windowContentEl_ORANGE = $('#windowContent_ORANGE').css('width', size.width - 25 + 'px');
          if (!document.getElementById('window_ORANGE').isMinimized) {
            windowContentEl_ORANGE.css('height', size.height - 48 + 'px');
          }
        }
      }
    );	//*/
  }
);
</script>
<script type="text/javascript"> 
$(document).ready(function()
{
    $('#windowOpen_CompTIA').bind('click',function()
    {
        if($('#window_CompTIA').css('display') == 'none')
        {
            $(this).TransferTo(
            {
                to:'window_CompTIA',
                className:'transferer_CompTIA',
                duration: 400,
                complete: function()
                {
                    $('#window_CompTIA').show();
                }
            });
        }
        this.blur();
        return false;
    });

    $('#windowClose_CompTIA').bind('click', function()
    {
        $.cookie('close_CompTIA', 1);

        $('#window_CompTIA').TransferTo(
        {
            to:'windowOpen_CompTIA',
            className:'transferer_CompTIA',
            duration: 400
        }).hide();
    });
    
    $('#windowMin').bind('click',function()
    {
        $('#windowContent_CompTIA').SlideToggleUp(300);
        $('#windowBottom_CompTIA, #windowBottomContent_CompTIA').animate({height: 10}, 300);
        $('#window_CompTIA').animate({height:40},300).get(0).isMinimized = true;
        $(this).hide();
        $('#windowResize_CompTIA').hide();
        $('#windowMax_CompTIA').show();
    });
    
    $('#windowMax_CompTIA').bind('click',function()
    {
        var windowSize_CompTIA = $.iUtil.getSize(document.getElementById('windowContent_CompTIA'));
        $('#windowContent_CompTIA').SlideToggleUp(300);
        $('#windowBottom_CompTIA, #windowBottomContent_CompTIA').animate({height: windowSize.hb + 13}, 300);
        $('#window_CompTIA').animate({height:windowSize_CompTIA.hb+43}, 300).get(0).isMinimized = false;
        $(this).hide();
        $('#windowMin_CompTIA, #windowResize_Two').show();
    });
    
    $('#window_CompTIA').Resizable(
    {
        minWidth: 200,
        minHeight: 60,
        maxWidth: 700,
        maxHeight: 400,
        dragHandle: '#windowTop_CompTIA',
        handlers: {
          se: '#windowResize_CompTIA'
        },
        onResize : function(size, position)
        {
            $('#windowBottom_CompTIA, #windowBottomContent_CompTIA').css('height', size.height-33 + 'px');
            var windowContentEl_CompTIA = $('#windowContent_CompTIA').css('width', size.width - 25 + 'px');
            if (!document.getElementById('window_CompTIA').isMinimized)
            {
                windowContentEl_CompTIA.css('height', size.height - 48 + 'px');
            }
        }
    });
});
</script>
<?php require ("../footer.php"); ?>
