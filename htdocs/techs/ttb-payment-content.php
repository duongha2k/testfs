<TABLE cellSpacing=0 cellPadding=0 width=525 border=0 style="text-align:left">
<TBODY>
<TR>
<TD colSpan=2><BR> <FONT face=Arial size=3><B>Field Solutions Payment Policy</B></FONT>
<P>
<HR>
<P>
<FONT face=Arial size=2>
<B>How/when you are paid</B>
<P>
Payment Schedule:
<P>
<a href="2013 Tech Pay Schedule.pdf">2013 Tech Pay Schedule.pdf</a>

<!-- TABLE WIDTH=430 BORDER=1 align="center" CELLPADDING=2 CELLSPACING=0 COLS=3>
	<COL WIDTH=111>
	<COL WIDTH=134>
	<COL WIDTH=107>
	<TR>
		<TD WIDTH=130 HEIGHT=42 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#666666">
			<P ALIGN=CENTER><strong><FONT COLOR="#ffffff">Work Orders<br />
		    approved</FONT></strong></P>		</TD>
		<TD WIDTH=175 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#666666">
			<P ALIGN=CENTER><strong><FONT COLOR="#ffffff">Pay Process Date<br />
			  Check
			Mailed</FONT></strong></P>		</TD>
		<TD WIDTH=125 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#666666">
			<P ALIGN=CENTER><strong><FONT COLOR="#ffffff">Direct Deposit<br />
		    Date</FONT></strong></P>		</TD>
	</TR>
	<TR>
		<TD width="130" HEIGHT=20 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#ffcc99">
			<P ALIGN=CENTER><FONT COLOR="#000000">8/4-8/10</FONT></P>		</TD>
		<TD width="175" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39678" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">08/18/2008</FONT></P>		</TD>
		<TD width="125" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39675" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">08/15/2008</FONT></P>		</TD>
	</TR>
	<TR>
		<TD width="130" HEIGHT=20 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#ffcc99">
			<P ALIGN=CENTER><FONT COLOR="#000000">8/11-8/17</FONT></P>		</TD>
		<TD width="175" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39685" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">08/25/2008</FONT></P>		</TD>
		<TD width="125" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39682" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">08/22/2008</FONT></P>		</TD>
	</TR>
	<TR>
		<TD width="130" HEIGHT=20 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#ffcc99">
			<P ALIGN=CENTER><FONT COLOR="#000000">8/18-8/24</FONT></P>		</TD>
		<TD width="175" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39692" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">09/01/2008</FONT></P>		</TD>
		<TD width="125" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39689" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">08/29/2008</FONT></P>		</TD>
	</TR>
	<TR>
		<TD width="130" HEIGHT=20 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#ffcc99">
			<P ALIGN=CENTER><FONT COLOR="#000000">8/25-8/31</FONT></P>		</TD>
		<TD width="175" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39699" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">09/08/2008</FONT></P>		</TD>
		<TD width="125" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39696" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">09/05/2008</FONT></P>		</TD>
	</TR>
	<TR>
		<TD width="130" HEIGHT=20 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#ffcc99">
			<P ALIGN=CENTER><FONT COLOR="#000000">9/1-9/7</FONT></P>		</TD>
		<TD width="175" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39706" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">09/15/2008</FONT></P>		</TD>
		<TD width="125" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39703" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">09/12/2008</FONT></P>		</TD>
	</TR>
	<TR>
		<TD width="130" HEIGHT=20 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#ffcc99">
			<P ALIGN=CENTER><FONT COLOR="#000000">9/8-9/14</FONT></P>		</TD>
		<TD width="175" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39713" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">09/22/2008</FONT></P>		</TD>
		<TD width="125" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39710" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">09/19/2008</FONT></P>		</TD>
	</TR>
	<TR>
		<TD width="130" HEIGHT=20 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#ffcc99">
			<P ALIGN=CENTER><FONT COLOR="#000000">9/15-9/21</FONT></P>		</TD>
		<TD width="175" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39720" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">09/29/2008</FONT></P>		</TD>
		<TD width="125" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39717" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">09/26/2008</FONT></P>		</TD>
	</TR>
	<TR>
		<TD width="130" HEIGHT=20 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#ffcc99">
			<P ALIGN=CENTER><FONT COLOR="#000000">9/22-9/28</FONT></P>		</TD>
		<TD width="175" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39727" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">10/06/2008</FONT></P>		</TD>
		<TD width="125" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39724" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">10/03/2008</FONT></P>		</TD>
	</TR>
	<TR>
		<TD width="130" HEIGHT=20 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#ffcc99">
			<P ALIGN=CENTER><FONT COLOR="#000000">9/29-10/5</FONT></P>		</TD>
		<TD width="175" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39734" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">10/13/2008</FONT></P>		</TD>
		<TD width="125" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39731" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">10/10/2008</FONT></P>		</TD>
	</TR>
	<TR>
		<TD width="130" HEIGHT=20 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#ffcc99">
			<P ALIGN=CENTER><FONT COLOR="#000000">10/6-10/12</FONT></P>		</TD>
		<TD width="175" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39741" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">10/20/2008</FONT></P>		</TD>
		<TD width="125" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39738" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">10/17/2008</FONT></P>		</TD>
	</TR>
	<TR>
		<TD width="130" HEIGHT=20 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#ffcc99">
			<P ALIGN=CENTER><FONT COLOR="#000000">10/13-10/19</FONT></P>		</TD>
		<TD width="175" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39748" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">10/27/2008</FONT></P>		</TD>
		<TD width="125" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39745" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">10/24/2008</FONT></P>		</TD>
	</TR>
	<TR>
		<TD width="130" HEIGHT=20 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#ffcc99">
			<P ALIGN=CENTER><FONT COLOR="#000000">10/20-10/26</FONT></P>		</TD>
		<TD width="175" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39755" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">11/03/2008</FONT></P>		</TD>
		<TD width="125" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39752" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">10/31/2008</FONT></P>		</TD>
	</TR>
	<TR>
		<TD width="130" HEIGHT=20 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#ffcc99">
			<P ALIGN=CENTER><FONT COLOR="#000000">10/27-11/2</FONT></P>		</TD>
		<TD width="175" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39762" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">11/10/2008</FONT></P>		</TD>
		<TD width="125" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39759" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">11/07/2008</FONT></P>		</TD>
	</TR>
	<TR>
		<TD width="130" HEIGHT=20 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#ffcc99">
			<P ALIGN=CENTER><FONT COLOR="#000000">11/3-11/9</FONT></P>		</TD>
		<TD width="175" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39769" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">11/17/2008</FONT></P>		</TD>
		<TD width="125" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39766" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">11/14/2008</FONT></P>		</TD>
	</TR>
	<TR>
		<TD width="130" HEIGHT=20 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#ffcc99">
			<P ALIGN=CENTER><FONT COLOR="#000000">11/10-11/16</FONT></P>		</TD>
		<TD width="175" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39776" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">11/24/2008</FONT></P>		</TD>
		<TD width="125" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39773" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">11/21/2008</FONT></P>		</TD>
	</TR>
	<TR>
		<TD width="130" HEIGHT=20 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#ffcc99">
			<P ALIGN=CENTER><FONT COLOR="#000000">11/17-11/23</FONT></P>		</TD>
		<TD width="175" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39783" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">12/01/2008</FONT></P>		</TD>
		<TD width="125" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39780" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">11/28/2008</FONT></P>		</TD>
	</TR>
	<TR>
		<TD width="130" HEIGHT=20 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#ffcc99">
			<P ALIGN=CENTER><FONT COLOR="#000000">11/24-11/30</FONT></P>		</TD>
		<TD width="175" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39790" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">12/08/2008</FONT></P>		</TD>
		<TD width="125" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39787" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">12/05/2008</FONT></P>		</TD>
	</TR>
	<TR>
		<TD width="130" HEIGHT=20 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#ffcc99">
			<P ALIGN=CENTER><FONT COLOR="#000000">12/1-12/7</FONT></P>		</TD>
		<TD width="175" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39797" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">12/15/2008</FONT></P>		</TD>
		<TD width="125" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39794" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">12/12/2008</FONT></P>		</TD>
	</TR>
	<TR>
		<TD width="130" HEIGHT=20 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#ffcc99">
			<P ALIGN=CENTER><FONT COLOR="#000000">12/8-12/14</FONT></P>		</TD>
		<TD width="175" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39804" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">12/22/2008</FONT></P>		</TD>
		<TD width="125" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39801" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">12/19/2008</FONT></P>		</TD>
	</TR>
	<TR>
		<TD width="130" HEIGHT=20 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#ffcc99">
			<P ALIGN=CENTER><FONT COLOR="#000000">12/15-12/21</FONT></P>		</TD>
		<TD width="175" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39811" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">12/29/2008</FONT></P>		</TD>
		<TD width="125" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39808" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">12/26/2008</FONT></P>		</TD>
	</TR>
	<TR>
		<TD width="130" HEIGHT=20 nowrap="nowrap" bordercolor="#000000" BGCOLOR="#ffcc99">
			<P ALIGN=CENTER><FONT COLOR="#000000">12/22-12/28</FONT></P>		</TD>
		<TD width="175" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39452" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">01/05/2008</FONT></P>		</TD>
		<TD width="125" nowrap="nowrap" bordercolor="#000000" BGCOLOR="#94bd5e" SDVAL="39449" SDNUM="1033;0;MM/DD/YYYY">
			<P ALIGN=CENTER><FONT COLOR="#000000">01/02/2008</FONT></P>		</TD>
	</TR>
</TABLE-->

<P>Techs performing work for clients via Field Solutions will receive payment in the form of a check from Field Solutions, LLC or via Direct Deposit if you have set this per instructions in your tech profile. Checks are cut and mailed to Techs for completed and approved work orders per the payment schedule above. Please understand - Field Solutions, LLC cannot cut checks for work orders that have not been approved by the client (the company you performed the work for). See further details below.
<P>Once a work order has been approved by the client, a date will eventually show up in the &quot;Pay Process Date&quot; column in your list of work orders online per payment schedule above. The &quot;Pay Process Date&quot; is provided to let you know that your check is being cut and mailed on that date or the very next day at the latest. The &quot;Pay Process Date&quot; also allows you to keep track of which work orders will be paid on what check as it is the same date that will be on your check(s).<br />
  If/when it has been more than 10 days past the &quot;Pay Process Date&quot; and you still have not received your check, please refer to your options below for recovering your check.
<P>1) Continue waiting to see if your check arrives.
<P>2) Contact Field Solutions support to request that your original check be canceled and a new check be issued. Note: It must be 10 or more days past the &quot;Pay Process Date&quot; before accounting will consider re-issuing a check. All re-issued checks are sent Priority Mail, which takes 2-3 days for delivery on average. There is a $25 check cancel/re-issue fee per check. If/when contacting Field Solutions support to request a new check be issued, please be sure to let us know which check(s) you have not received by listing the check date (&quot;Pay Process Date&quot;). We also need your acknowledgment that you will be charged the $25 check cancel/re-issue fee per check.
<P>3) Wait to see if the check is returned to us at which time a new check will be issued and sent Priority Mail with NO check cancel/re-issue fee.
<P>4) Wait 60 days from the date your original check was processed (the &quot;Pay Process Date&quot;) at which time a new check will be issued and sent Priority Mail with NO check cancel/re-issue fee.
<P><strong>Contact Field Solutions with all payment issues</strong>
<P>It is very important for Techs to understand that you are to contact Field Solutions regarding payment issues rather than the client (company you performed the work for) as long as they have approved your work order(s). If/when a client has approved your work order(s), they have nothing to do with payment. You only risk annoying the client to the point they no longer want to use your services.
<P><strong><br />
  Work orders not entered online and/or not approved</strong>
<P>If/when you come find a work order that has NOT been ENTERED for you to view/update online via Field Solutions web site, please contact the client and/or the person who set you up to perform the work and NOT Field Solutions.
<P>If/when you find a work order that has NOT been APPROVED for payment, please contact the client and/or the person who set you up to perform the work and NOT Field Solutions.
<P><strong>If you find a discrepancy in your payment amount, please address it as follows:</strong>
<P>Make sure that you have looked at the date on your check and have cross-referenced it with the date(s) in the &quot;Pay Process Date&quot; column online where you update your work orders. The date(s) in the &quot;Pay Process Date&quot; column online will exactly match the date on your check(s).
<P><strong>Important:</strong> Please do not email and/or call accounting to inform them that your check is not for the correct amount until you have cross-referenced the date on your check with the date(s) in the &quot;Pay Process Date&quot; column online where you update your work orders.
<P><strong>In the event you are not paid the total amount or at all for work performed, it is likely due to one or more of the following reasons:</strong>
<P>1) You have not marked your work order complete online per instructions. If/when you have failed to do something correctly, it will usually be noted within the work order online. Please refer to the &quot;How-To&quot; instructions in your on your tech account.
<P>2) One or more of your work orders have not yet been approved by the client and/or they approved the work order for an incorrect amount. Payment cannot be processed by Field Solutions, LLC for your Work orders until they have been approved by the client (the company you performed the work for). Once a work order is approved by the client, Field Solutions, LLC will process payment for whatever amount the client indicates. If/when you have issues with the client not approving your work order(s) and/or for an incorrect amount, you should log into your Field Solutions tech profile and view your work orders to determine why one or more of them have not been approved and/or approved for an incorrect amount then contact the client (the company you performed the work for) and NOT Field Solutions.
<P>3) Your check is lost in the mail or has been returned to us. See above for your options for recovering your check(s).
<P>We ask that you please read the entire payment policy above and act accordingly prior to contacting Field Solutions Technician Support. When contacting Field Solutions regarding a payment issue, please be sure to list the date of your check(s) (&quot;Pay Process Date&quot;) in question.
</TR></TBODY></TABLE>	