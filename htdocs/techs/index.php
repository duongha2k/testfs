<?php 
    $page = 'techs'; 
    $option = 'home'; 
    require_once ("../headerStartSession.php");
    // Check whether tech is logged in
    $loggedIn = isset($_SESSION['loggedIn']) ? $_SESSION['loggedIn'] : null;
    $loggedInAs = isset($_SESSION['loggedInAs']) ? $_SESSION['loggedInAs'] : null;
    
    /**
     * Modified by Sergey Petkevich (Warecorp)
     */
    
    if($loggedIn == "yes" && $loggedInAs == "tech"){
        // Redirect to dashboard.php
        header( 'Location: /techs/dashboard.php') ;
    }else{
        header('Location: /techs/logIn.php');
    }
    /**
     * #####################################
     */

