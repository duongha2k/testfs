<?php
    $counters = array();
    $search->getWorkOrdersCountByStatus($_SESSION["UserName"], $_SESSION["UserPassword"], $counters);

function getTechSelfRate($expertiseInfo)
{
    $selfRatingCount = 0;
        
    foreach ($expertiseInfo as $selfRating)
    {
        if ($selfRating['selfRating'] == "")
            return 0;
    }
    return 1;
	}

function getEquipmentInfo($equipRes)
{
    //return count($equipRes);
    $equipcount= 0 ;

    if ($equipRes) {
	    foreach ($equipRes as $equip)
	    {
	        if($equip !="ExpInstallSurv" && $equip != "InstallingTelephony"  && $equip != "SiemensHipath")
	        {
	            $equipcount +=1;
	        }
	    }
    }
    
    return $equipcount;
}

$authData = array('login' => $_SESSION["UserName"], 'password' => $_SESSION["UserPassword"]);

$tech = new Core_Api_TechUser();
$tech->checkAuthentication($authData);

if (!$tech->isAuthenticate())
    return API_Response::fail();
$db = Core_Database::getInstance();
$TechID = $tech->getTechId();

$techInfo = Core_Tech::getProfile($TechID, true, API_Tech::MODE_TECH);

//print_r("<pre>");
//print_r($techInfo['expertiseInfo']);

$TechBusinessinfo = 0;
$TechBusinessStatement = 0;
$TechResume = 0;
$TechSkill = getTechSelfRate($techInfo['expertiseInfo']); //
$TechToolKit = getEquipmentInfo($techInfo['equipmentInfo']); //$techInfo['equipmentInfo']
$TechBasicBackgroundCheck = 0;
$resumecount = 0;
$ProfilePiccount = 0;

if ($techInfo['PrimaryPhone'] != "" && $techInfo['SecondaryPhone'] != "")
    $TechBusinessinfo = 1;

if ($techInfo['EquipExperience'] != "" || $techInfo['SpecificExperience'] != "" || $techInfo['SiteRef'] != "")
    $TechBusinessStatement = 1;

$fullRegDate = $techInfo["credentialInfo"]['bgCheckDate'];

if ($fullRegDate != "")
{
    $now = mktime(0, 0, 0, date("m"), date("d") - 365, date("Y"));
    $now = date('Y-m-d', $now);

    $date1 = strtotime($now);
    $date2 = strtotime($fullRegDate);
    if ($date1 > $date2)
    {
        
    } else
    {
        $TechBasicBackgroundCheck = 1;
    }
}

if (!empty($techInfo['techFiles']))
{

    foreach ($techInfo['techFiles'] as $file)
    {
        if ($file['fileType'] == "Resume" && $file['filePath'] != "")
        {
            $resumecount +=1;
        }

        if ($file['fileType'] == "Profile Pic" && $file['filePath'] != "")
        {
            $ProfilePiccount +=1;
        }
    }
}
//tested done
if ($TechBusinessinfo)
{
    $techpercent += 5;
}

if ($ProfilePiccount > 0)
{
    $techpercent += 15;
}
//tested done
if ($TechBusinessStatement)
{
    $techpercent += 15;
}
//tested done
if ($resumecount > 0)
{
    $techpercent += 15;
}

//tested done
if ($TechSkill == 1)
{
    $techpercent += 20;
}

if ($TechToolKit > 0)
{
    $techpercent += 10;
}

if ($TechBasicBackgroundCheck)
{
    $techpercent += 20;
}

?>
<style type="text/css">
        	#clcLogoSection img{
        		margin-right: 15px;
        	}
        	#clcPopupContainer p{
        		font: 11px Helvetica, Sans-Serif;
        	}
            h4.leftNavHeader:hover{
        		cursor:hand;
        		cursor:pointer;
        	}
        	h4.leftNavHeader.closed{
        		background-image: url('/widgets/images/triangle.gif');
        	}
        	h4.leftNavHeader{
        		background-image: url('/widgets/images/triangleBlue_down.gif');
        	}
        </style>
	<div class="left_col" style="width: 20%;">
            <h4 class="leftNavHeader closed">FS-PlusOne</h4>
            <div class="leftNavContent collapse">
	            <ul>
	                <li><a id="FSPlusOneRewards" href="/techs/FS-PlusOne%20Program%20Overview_011613.pdf" target="_blank">FS-PlusOne Rewards</a></li>
	            </ul>
	            <ul>
	                <li><a id="FSPlusOneFAQ" href="/techs/FS_PlusOne_FAQ_011613.pdf" target="_blank">FS-PlusOne FAQ</a></li>
	            </ul>
	            <ul>
	                <li><a id="FSPlusOneFAQ" href="/techs/FS_PlusOne_Earning_Reward_Points_011613.pdf" target="_blank">FS-PlusOne Current Offers</a></li>
	            </ul>
	         </div>
	         
	         <h4 class="leftNavHeader">FS-CheckINN&trade; Hotel Savings<img alt="New" src="/images/new.gif" name="New"></h4>
            <div class="leftNavContent">
	            <ul>
	                <li><a id="showCheckInnPopup" href="javascript:void(0);">Register for FS CheckINN&trade; Savings</a></li>
	            </ul>
	            <ul>
	                <li><a id="checkInnOverview" href="documents/FieldSolutionsCheckINNHotelSavingsProgram-OverviewforFieldSolutionsTechnicians-2013-08-08.pdf" target="_blank">CheckINN&trade; Hotel Savings Overview</a></li>
	            </ul>
	         </div>
	         
	        <h4 class="leftNavHeader closed"><img alt="New" src="/images/FS_Mobile_16x16.png" name="FS_Mobile">&nbsp;FS-Mobile&trade;</h4>
	        <div class="leftNavContent collapse">
	            <ul>
	                <li><a id="FSMobileFAQ" href="/techs/FS-Mobile_FAQ_01202012.pdf" target="_blank">FS-Mobile&trade; FAQ</a></li>
	            </ul>
	            <ul>
	                <li><a id="FSMobileAndroid" href="https://market.android.com/details?id=com.fieldsolutions.android&feature=search_result" target="_blank">FS-Mobile&trade; Android</a></li>
	            </ul>
	            <ul>
	                <li><a id="FSMobileIphone" href="http://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=0CCQQFjAA&url=http%3A%2F%2Fitunes.apple.com%2Fus%2Fapp%2Ffs-mobile%2Fid471390045%3Fmt%3D8&ei=rPAZT9aICaSvsQL0pLjjCw&usg=AFQjCNFXXlRy5XKhPD8mxhQ5XdBKWwJbpw" target="_blank">FS-Mobile&trade; iPhone</a></li>
	            </ul>
	        </div>
            
            <h4>My Work</h4>
            <ul>
                <li>
<!--                    <table cellspadding="0" cellspacing="2" border="0">
                        <tr>
                            <td><a class="certForms"  href="javascript:void(0);" rel="/techs/Training/FLS/flsCertTest.php" id="myscheduleLink" >My Schedule</a></td>
                            <td><img alt="New" src="/images/CalendarIcon.gif" width="18px"/></td>
                            <td><img alt="New" src="/images/new.gif" name="New"></td>
                        </tr>
                    </table>-->
                            <a onclick="detailObject.onInit.showMyscheduleCalendar('/techs/TechSchedule.php?TechID=<?=$TechID?>')"  href="javascript:void(0);" rel="/techs/TechSchedule.php?TechID=<?=$TechID?>" id="myscheduleLink" >My Schedule</a>
                            <a onclick="detailObject.onInit.showMyscheduleDownload(this);" href="javascript:;"> <img alt="New" src="/images/CalendarIcon.gif" width="15px" height="15px"/></a>
                            <img alt="New" src="/images/new.gif" name="New">
                        
                </li><!---->
                <li><a id="availableWorkLink" href="/techs/wos.php?tab=techavailable">Available Work (<?= (isset ($counters['published']) ? $counters['published'] : 0)?>)</a></li>
                <li><a id="assignedWorkLink" href="/techs/wos.php?tab=techassigned">Assigned Work (<?= (isset ($counters['assigned']) ? $counters['assigned'] : 0)?>)</a></li>
                <li><a id="appliedWorkLink" href="/techs/wosViewAppliedAll.php">Applied Work (my bids) (<?= (isset ($counters['applied']) ? $counters['applied'] : 0)?>)</a></li>
            </ul>


            <script type="text/javascript">
                $(document).ready(function(){
                    //Fill my perfomance rating mouseover.
                });
            </script>
            <h4 class="leftNavHeader">My Money</h4>
            <div class="leftNavContent">
	            <ul>
	                <li><a id="payStubs" href="/techs/pay_stubs.php" target="_self" class="allowLinkTD">Pay Stubs</a></li>
	                <li><a id="incompleteWorkOrdersLink" href="/techs/wos.php?tab=techincomplete" target="_self">Incomplete Work Orders (<?= $counters['incomplete']?>)</a></li>
	                <li><a id="siteCompleteLink" href="/techs/wos.php?tab=techworkdone" target="_self">Site Complete Not Yet Approved Work (<?= $counters['work done']?>)</a></li>
	                <li><a id="approvedNotPaidLink" href="/techs/wos.php?tab=techapproved" target="_self">Approved Not Yet Paid Work (<?= $counters['in accounting']?>)</a></li>
	                <li><a id="directDepositEnrollmentLink" href="/techs/updateProfile.php?data=showPaymentInfo">Direct Deposit Enrollment</a></li>
	            </ul>
            </div>

	<h4 class="leftNavHeader"><a id="updateProfileLink" href="/techs/updateProfile.php" target="_self" class="allowLinkTD techpercentcompletion">My Profile</a></h4>

    <table width="96%" cellspacing="0" cellpadding="0" border="0">
        <tr>
            <td width="10%"></td>
            <td width="85%" >
                <div class ="techpercentcompletion" id="container" style="position: relative; border: 1px solid #4F81BD; text-align: left; height: 12px; width: 100%;">
                    <div style="overflow: hidden; position: absolute; top: 0pt; left: 0pt; width: <?= $techpercent; ?>%;">
                        <div style="height: 12px; color: #ffffff; background-color: #4F81BD; font-size: 10px; width: 500%;">
                            <span><? echo $techpercent; ?>% Completion</span>
                        </div>
                    </div>
                    <div style="color: #4F81BD; width: 100%; font-size: 10px; height: 12px;">
                        <span><? echo $techpercent; ?>% Completion</span>
                    </div>
                </div>
            </td>
            <td width="5%"></td>
        </tr>
    </table>

    <div id="techpercentcompletionpopup" style="display: none;" class="popup-container-roll">
        <div id="_popup_title" class="popup-header-title" style="width: 160px;">
            Complete your profile: <br/>
        </div>
        <div id="_popup_button" class="popup-header-button"></div>
        <div id="_popup__body" class="popup-body">
            <?
            if($techpercent==100)
            {
                ?>
                    Your profile was completed.<br/>
                    Click <b><a id="updateProfileLink" href="/techs/updateProfile.php" target="_self" class="allowLinkTD">here</a></b> to view or update Profile.<br/>
                <?
            }
            else
            {
                if (!$TechBusinessinfo)
                {
                    ?>
                    +5% - <a id="updateProfileLink" href="/techs/updateProfile.php?data=showbasicRegistration" target="_self" class="allowLinkTD">Enter both your Business Hours and Evening/Weekend Phone #</a><br/>
                    <?
                }
                if (!($ProfilePiccount > 0))
                {
                    ?>
                    +15% - <a id="updateProfileLink" href="/techs/updateProfile.php?data=showbasicRegistration" target="_self" class="allowLinkTD">Upload your Photo</a><br/>
                    <?
                }
                if (!$TechBusinessStatement)
                {
                    ?>
                    +15% - <a id="updateProfileLink" href="/techs/updateProfile.php?data=showBusinessProfileSection" target="_self" class="allowLinkTD">Fill out your Business Statement</a><br/>
                    <?
                }
                if (!($resumecount > 0))
                {
                    ?>
                    +15% - <a id="updateProfileLink" href="/techs/updateProfile.php?data=showBusinessProfileSection" target="_self" class="allowLinkTD">Upload your Resume</a><br/>
                    <?
                }
                if (!($TechSkill == 1))
                {
                    ?>
                    +20% - <a id="updateProfileLink" href="/techs/updateProfile.php?data=showExpertiseSection" target="_self" class="allowLinkTD">Rate your Skills</a><br/>
                    <?
                }
                if (!($TechToolKit > 0))
                {
                  //14034
                    ?>
                    +10% - <a id="updateProfileLink" href="/techs/updateProfile.php?data=showToolsResourcesSection" target="_self" class="allowLinkTD">What's in your Tool kit?</a><br/>
                    <?
                  //End 14034
                }
                if (!$TechBasicBackgroundCheck)
                {
                    ?>
                    +20% - <a id="updateProfileLink" href="/techs/updateProfile.php?data=showCredentialsSection" target="_self" class="allowLinkTD">Get a Basic Background Check</a><br/>
                    <?
                }
            }
            ?>
        </div>
    </div>
    <script type="text/javascript">
        var timeout = null;
        var _timeoutHover = null;
        var _hover = false;

//        $("#divtechpercentcompletion").hover(function() { 
//            _hover= true; 
//        }, function() { 
//            _hover = false; 
//        })
            
         $(document).ready(function() {
            $(".collapse").hide();
            $(".leftNavHeader").click(function(){
                	thisHeader = $(this);
                	thisHeader.next(".leftNavContent").slideToggle(300,function(){
                    		if($(this).is(":visible") == true){
                        		thisHeader.css("background-image","url(/widgets/images/triangleBlue_down.gif)");
                        		console.log("VISIBLE: "+thisHeader.css('background-image'));
                    		}else{
                    			thisHeader.css("background-image","url(/widgets/images/triangle.gif)");
                    			console.log("NON VISIBLE: "+thisHeader.css('background-image'));
                    		}
                    	});
            });
            $("#showCheckInnPopup").click(function(){
		        $("<div></div>").fancybox({
		 			'autoScale' : false,
		 			'content' : $("#clcPopupContent").html(),
		 			'scrolling' : 'no',
		 			'height' : 520,
	 				'width' : 710,
		 			'autoDimensions' : false,
		 			'centerOnScroll' : true
		 			}).trigger('click');  
            });
         });
			  
        $(".techpercentcompletion").mouseover(function()
        {
            _hover = true; 
            var pos = $(this).offset();
            $("#techpercentcompletionpopup").css({
                    'left': (pos.left) + 100 + 'px',
                    'top':  (pos.top  + $(this).height() - 20) + 'px',
                    'zIndex': '101'
                });
            if(_timeoutHover != null)
            {
                clearTimeout(_timeoutHover);
            }   
            if(timeout!= null)
            {
                clearTimeout(timeout);
            }
           _timeoutHover = setTimeout(function()
            {
                if(_hover)
                {    
                    $('#techpercentcompletionpopup').show();
                }
            },1000); 
        }).mouseout(function(){
               _hover = false; 
               if(timeout!= null)
               {
                    clearTimeout(timeout);
               } 
               timeout = setTimeout("$('#techpercentcompletionpopup').hide();",1000);
        });
        
        
        $("#techpercentcompletionpopup").mouseover(function()
        {
            if(timeout!= null)
            {
                clearTimeout(timeout);
            }
        }).mouseout(function(){
                if(timeout!= null)
                {
                    clearTimeout(timeout);
                }
                timeout = setTimeout("$('#techpercentcompletionpopup').hide();",1000);
        });
        
        $("#_popup_button").click(function(){
            $('#techpercentcompletionpopup').hide();
            if(timeout!= null)
            {
                clearTimeout(timeout);
            }
        });
    </script>
            <!-- h4>My Credentials</h4>
            <ul>
                <li><a id="myBasicProfileLink" href="javascript:///" onclick="tpm.openPopupWin('myProfile');">My Basic  Profile</a>
                </li>
                <li><a id="skillsLink" href="javascript:///" onclick="tpm.openPopupWin('skills');">Skills</a></li>
                <li><a id="experienceLink" href="javascript:///" onclick="tpm.openPopupWin('experience');">Experience</a></li>
                <li><a id="bgcheckLink" href="javascript:///" onclick="tpm.openPopupWin('bg-checks');" >Background Check</a></li>
                  <li><a href="javascript:///" onclick="tpm.openPopupWin('certifications');">Certifications</a>
                    <ul>
                    	<li><a href="javascript:///" onclick="tpm.openPopupWin('todo',{url:'/techs/Training/ServRight/electroMechTest.php'});">Electro-Mechanical Skills</a><img alt="New" src="/images/new.gif" name="New"></li>
                    	<li><a href="javascript:///" onclick="tpm.openPopupWin('FLS');">FLS</a></li>
                    </ul>
                </li>
            </ul -->
            <h4 class="leftNavHeader">Certifications</h4>
            <div class="leftNavContent">
	            <ul style="margin-right:0px;">
	            	<!--
					<li class="displayNone" id="todoExperience"><a href="javascript:///" onclick="tpm.openPopupWin('experience');">Complete Experience</a></li>
					<li class="displayNone" id="todoCertifications"><a href="javascript:///" onclick="tpm.openPopupWin('certifications');">Complete Certifications</a></li>
					<li class="displayNone" id="todoEquipment"><a href="javascript:///" onclick="tpm.openPopupWin('equipment');">Equipment</a></li>
					<li class="displayNone" id="todoW9"><a href="javascript:///" onclick="tpm.openPopupWin('submitW9');">Complete W9</a></li>
					-->
					<li class="displayNone" id="todoNCR"><a class="certForms"  href="javascript:void(0);" rel="/techs/Training/NCR/NCR_Training.php">NCR Basic Certification</a></li>
					<li class="displayNone" id="todoFLS_CSP"><a class="certForms"  href="javascript:void(0);" rel="/techs/Training/FLS/flsCertTest.php">Computing Security Procedures (FLS-CSP) Policy</a></li>
					<!--
					<li class="displayNone" id="todoCore"><a href="javascript:///" onclick="tpm.openPopupWin('todo',{url:'/techs/Training/CORE/CORE_Test.php'});">Core-Techs Certification</a></li>
					<li id="todoSkills"><a href="javascript:///" onclick="tpm.openPopupWin('todo',{url:'/techs/cablingSkills.php'});" >CCTV Skills: Update your Cabling Profile Now for New Clients!</a></li>
					<li id="todoTelephony"><a href="javascript:///" onclick="tpm.openPopupWin('todo',{url:'/techs/telephonySkills.php'});" >Telephony Experience</a></li>
					-->
	                                <li id="todoCompTIA"><a class="certForms"  CompTIA='CompTIA' href="javascript:void(0);" rel="/techs/Training/CompTIACertification.php">CompTIA Industry Certifications</a><img alt="New" src="/images/new.gif" name="New"></li>
					<li id="todoFLS"><a class="certForms"  href="javascript:void(0);" rel="/techs/Training/FLS/flsCertTest.php">Fujitsu FLS</a></li>				
					<li id="todoHP" class=""><a class="certForms"  href="javascript:void(0);" rel="/techs/ajax/lightboxPages/HP_Cert.php">HP Certification</a></li>
	<!--        <li id="todoEssintial" class=""><a class="certForms"  href="javascript:void(0);" rel="/techs/Training/Essintial/Essintial_Training.php">Essintial Certification</a></li>-->
					<li><a class="certForms" href="javascript:void(0);" rel="/techs/Training/ServRight/electroMechTest.php" >Electro-Mechanical Skills</a></li>
			        <li><a href="javascript:;" class="allowLinkTD cmdCopierSkills">Copier Skills</a></li>
	                        <li><a href="javascript:;" class="allowLinkTD cmdComputerPlusCertification">Computer Plus Certification</a><img alt="New" src="/images/new.gif" name="New"></li>
	                        <li><a href="javascript:;" class="allowLinkTD GGETechEvaluation">GGE Tech Evaluation</a><img alt="New" src="/images/new.gif" name="New"></li>
	                        <li><a href="javascript:;" class="allowLinkTD ServRightBrotherCertification">ServRight - Brother Certification</a><img alt="New" src="/images/new.gif" name="New"></li>
	                        <li><a href="javascript:;" class="allowLinkTD TechFORCEContractorAgreement">TechFORCE Contractor Agreement</a><img alt="New" src="/images/new.gif" name="New"></li>
	        	</ul>
        	</div>
    <script type="text/javascript" src="/widgets/js/base64.js"></script>
    <script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <script type="text/javascript" src="/widgets/js/fancybox/jquery.easing.pack.js"></script>
    <script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-mod.js"></script>
    <link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" media="all" />
    <script type="text/javascript" src="js/copier_skills_assessment.js"></script> 
    <script type="text/javascript" src="js/ComputerPlusCertification.js"></script> 
    <script type="text/javascript" src="js/GGETechEvaluation.js"></script> 
    <script type="text/javascript" src="js/ServRightBrotherCertification.js"></script> 
        <script type="text/javascript">
                if (!techInfo.expComplete ) $("#todoExperience").removeClass('displayNone');
                if (!techInfo.certComplete ) $("#todoCertifications").removeClass('displayNone');
                if (!techInfo.equipComplete ) $("#todoEquipment").removeClass('displayNone');
                if (!techInfo.W9) $("#todoW9").removeClass('displayNone');
                if (!techInfo.Cert_Hallmark_POS) $("#todoHallmark").removeClass('displayNone');
                if (!techInfo.NCR_Basic_Cert) $("#todoNCR").removeClass('displayNone');
                if (!techInfo.Starbucks_Cert) $("#todoStarbucks").removeClass('displayNone');
                if (!techInfo.FLSCSP_Rec) $("#todoFLS_CSP").removeClass('displayNone');
                if (!techInfo.Cert_Maurices_POS) $("#todoMarices").removeClass('displayNone');
                if (!techInfo.CORE_Cert) $("#todoCore").removeClass('displayNone');
                if (!techInfo.HP_Cert) $("#todoHP").removeClass('displayNone');
                if (!techInfo.SamsungCert) $("#todoSamsung").removeClass('displayNone');
                if (!techInfo.EssintialCert) $("#todoEssintial").removeClass('displayNone');
        var _copier_skills_assessment = copier_skills_assessment.CreateObject({
                   id:'_copier_skills_assessment' 
                });
        var _ComputerPlusCertification = ComputerPlusCertification.CreateObject({
                   id:'_ComputerPlusCertification' 
                });        
         var _GGETechEvaluation = GGETechEvaluation.CreateObject({
                   id:'_GGETechEvaluation' 
                });        
        var _ServRightBrotherCertification = ServRightBrotherCertification.CreateObject({
                   id:'_ServRightBrotherCertification' 
                });  
           
        </script>
        
        <h4>Contact</h4>
        <ul>
            <li><a href="javascript:///" onclick="tpm.openPopupWin('help');">Help</a></li>
        </ul>
	</div>
	
	<div id="clcPopupContent" style="display:none;">
		<div id="clcPopupContainer" style="width: 710px; height: 550px;">
			<div>
				<img src="images/fs_logo_small.png" width="200"  />
			</div>
			<div>
				<img src="images/clc_header_bg.png" height="100" width="700" />
				<p style="margin: -96px 0 0 10px; color: #FFF;">
					<span style="font-size: 13px;font-weight:bold;">FieldSolutions CheckINN&trade; Hotel Savings for Technicians</span><br />
					<span>Saves FieldSolutions Technicians up to 50% on Nationwide Hotels</span>
				</p>
			</div>
			
			<div style="width: 700px;margin-top: 15px;font-size: 11px;">
				<div style="float:left;width: 450px;">
					<div style="color: #F69630;font-size:11px;">FIELDSOLUTIONS EXCLUSIVELY OFFERS OUR TECHNICIANS THE FIELDSOLUTIONS CHECKINN HOTEL SAVINGS CARD</div>
					<p style="margin-top: 5px; line-height: 15px;">FieldSolutions now offers you the benefits of the CheckINN&trade; hotel savings card for use at over 10,800 
					participating hotels. FieldSolutions' CheckINN&trade; discounted hotel rates are up to 50% less than standard 
					corporate rates. Travelers present their personalized FieldSolutions CheckINN&trade; card at check-in to receive 
					the reduced rates, with direct billing to your pre-authorized personal credit card. 
					</p>
					<div style="color: #F69630;font-size:11px;">CARD SAVINGS FEATURES</div><br />
					<ul style="list-style: disc;margin-left: 20px;line-height: 15px;color: #000;">
						<li>Rates are typically up to 50% less due to the CheckINN&trade; Card's enormous booking volumes</li>
						<li>Free to enroll</li>
						<li>Walk-in availability, no extra costs for last minute bookings, standard hotel cancellation policies</li>
						<li>Avoid extra room night charges with CLC's 24 hour stay policy - for example, check in at 10 pm, check out at 9:59pm the next day</li>
						<li>Easy hotel and rate access via free iPhone&trade; & Android&reg; Hotel Locator Apps</li>
						<li>Stay details and invoices posted to your online account for easy tracking and expense reimbursement</li>
						<li>Nightly $8.95 transaction fee plus taxes are automatically added to all rates, not applied for cancelled reservations</li>
					</ul><br />
					<div style="color: #F69630;font-size:11px;">PARTICIPATING HOTELS INCLUDE</div>
					<div id="clcLogoSection" style="width: 100%">
						<img src="images/hampton_inn_logo.png" style="float:left; width: 50px;"  />
						<img src="images/hilton_logo.png" style="float:left; width: 75px;" />
						<img src="images/hyatt_logo.png" style="float:left; width: 35px;" />
						<img src="images/radisson_logo.png" style="float:left; width: 59px;" />
						<img src="images/bestwest_logo.png" style="float:left; width: 39px;" />
						<img src="images/super8_logo.png" style="float:left; width: 32px;" />
						<img src="images/days_inn_logo.png" style="float:left; width: 50px;" />
					</div>
				</div>
				<div style="float:left; margin-left: 45px;">
					<div style="width: 200px; height: 90px;">
						<a href="https://www.clclodging.com/cid/join/CCacctsignup.cfm?key=FIELDSOLTECH&company=<?=$TechID?>" target="_blank">
							<img src="images/enrollBtn_bg.png" height="60" width="200" style="position: relative;z-index: 5;" />
						</a>
					</div>
					<div style="width: 200px; height: 110px;">
						<img src="images/dropshadow_bg.png" height="200" width="200" />
						<p style="margin-top: -190px; font-size: 10px; padding: 0 20px 0 22px; text-align: center;">For important information regarding the usage of your FieldSolutions CheckINN card, please review:<br /><br />
						<a href="FieldSolutionsCheckINNHotelSavingsProgram-OverviewforFieldSolutionsTechnicians-2013-08-08.pdf" target="_blank">FieldSolutions CheckINN&trade; Program Overview</a></p>
					</div>
					<img src="images/checkinn_card.png" style="width: 200px;" />
				</div>
			</div>
		</div>
	</div>
