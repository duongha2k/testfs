<?php
$page = 'techs';
$option = 'wos';
$selected = 'wosViewApplied';
require ("../header.php");
require ("../navBar.php");

	require_once("../library/caspioAPI.php");
//	ini_set("display_errors",1);

	if (empty($_SESSION['TechID'])) {
		header('Location: /techs/logIn.php');
		exit;
	}

	function parseDate($date) {
		$parts = explode("/", $date);
		if (!is_numeric($parts[0]) || !is_numeric($parts[1]) || !is_numeric($parts[2]))
			return FALSE;

//		return "{$parts[2]}/{$parts[0]}/{$parts[1]}";
        return "{$parts[2]}-{$parts[0]}-{$parts[1]}";
	}

	$techID = ($_SESSION['TechID']);

	if ($techID != "") {
		$userInfo = caspioSelectAdv("TR_Master_List", "Username, Zipcode", "TechID = '$techID'", "", FALSE, "`", "|");

		if (sizeof($userInfo) != 1) die();

		$userInfo = explode("|", $userInfo[0]);
		$userKey = trim($userInfo[0], "`") . trim($userInfo[1], "`");

		$myKey = rand(0, 10000);
		$expected = md5($userKey . $myKey);
	}
?>

<br/>
<div align="center">
<script type="text/javascript">

	function sortBy(num) {
		frm = document.getElementById("appliedWO");
		frm.sortDir.value = (frm.sortCol.value == num ? (frm.sortDir.value == 0 ? 1 : 0) : 0);
		frm.sortCol.value = num;
		frm.submit();
	}
</script>
</div>

<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript" src="js/dropdownDateFilter.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
	$('#BidDateGT').calendar({dateFormat: 'MDY/'});
	$('#BidDateLT').calendar({dateFormat: 'MDY/'});
	$('#StartDateGT').calendar({dateFormat: 'MDY/'});
	$('#StartDateLT').calendar({dateFormat: 'MDY/'});
	if (v2 != "" && fstatus == "Trained") {
		me = document.getElementById("dbTypeLink");
//		me.href = "wosFLS.php?v=" + v2;
//		me.style.display = "inline";
	}
	});
</script>

<style  TYPE="text/css">
.label{ font-size:12px; color:#385C7E;}

.resultTable {
	border-spacing: 0px;
	text-align: center;
	color: #000000;
	padding: 0px;
	border-collapse: collapse;
}

.resultTop {
	background-color: #949CCE;
	border-top: 1px solid #000000;
	border-left: 1px solid #000000;
	border-right: 1px solid #000000;
}

.leftHeader {
	border-left: 2px solid #2A497D;
}

.rightHeader {
	border-right: 2px solid #2A497D;
}

.middleHeader {
	border-left: 1px solid #FFFFFF;
	border-right: 1px solid #FFFFFF;
}

.resultHeader a, .resultRow a:hover {
	color: #2A497D;
}

.resultHeader a:hover, .resultRow a {
	color: #6373B5;
}

.resultHeader td {
	color: #000099;
	text-align: center;
	background-color: #A0A8AA;
	padding: 7px 10px;
	font-weight: bold;
	cursor: pointer;
	border-top: 2px solid #2A497D;
}

.resultRow {
	background-color: #FFFFFF;
}

.resultRow:hover {
	background-color: #EAEFF5;
}

.leftRow {
	border-left: 2px solid #2A497D;
}

.rightRow {
	border-right: 2px solid #2A497D;
}

.resultRow td {
	padding: 0px 10px;
}

.resultBottom {
	background-color: #A0A8AA;
	border: 2px solid #2A497D;
}
</style>

<div align="center">
<h1>My Applied Work Orders</h1>
<?php
if (!isset($_POST["search"])) {
?>
<form id="appliedWO" name="appliedWO" action="<?=$_SERVER['PHP_SELF']?>?v=<?=$techID?>" method="post">
	<table cellpadding="0" cellspacing="0" border="0" style="border:#385C7E solid medium;">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="5">
					<tr>
						<td class="label">Bid Date (>=)</td>
						<td><input id="BidDateGT" name="BidDateGT" type="text" size="13" /></td>
					</tr>
					<tr>
						<td class="label">Bid Date (<=)</td>
						<td><input id="BidDateLT" name="BidDateLT" type="text" size="13" /></td>
					</tr>
					<tr>
						<td class="label">Start Date (>=)</td>
						<td><input id="StartDateGT" name="StartDateGT" type="text" size="13" /></td>
					</tr>
					<tr>
						<td class="label">Start Date (<=)</td>
						<td><input id="StartDateLT" name="StartDateLT" type="text" size="13" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="text-align:center; background-color:#BACBDF; border-top:#385C7E solid medium; padding:5px;">
			<input id="search" name="search" type="submit" value="Search" style="background-color:#385C7E; color:#FFFFFF; font-size:12px; font-weight:bold;" />
			<input id="v" name="v" type="hidden" value="<?=$techID?>" />
			<input name="sort" type="hidden" value="1" />
			<input name="sortDir" type="hidden" value="1" />
			</td>
		</tr>
	</table>
</form>
<?php
}
else {
	if (!isset($_POST["v"]) || !is_numeric($_POST["v"])) die();

	$bidDateGT = parseDate($_POST["BidDateGT"]);
	$bidDateLT = parseDate($_POST["BidDateLT"]);
	$startDateGT = parseDate($_POST["StartDateGT"]);
	$startDateLT = parseDate($_POST["StartDateLT"]);

?>

<form id="appliedWO" name="appliedWO" action="<?=$_SERVER['PHP_SELF']?>?v=<?=$techID?>" method="post">
	<input id="BidDateGT" name="BidDateGT" type="hidden" value="<?=$_POST["BidDateGT"]?>" />
	<input id="BidDateLT" name="BidDateLT" type="hidden" value="<?=$_POST["BidDateLT"]?>" />
	<input id="StartDateGT" name="StartDateGT" type="hidden" value="<?=$_POST["StartDateGT"]?>" />
	<input id="StartDateLT" name="StartDateLT" type="hidden" value="<?=$_POST["StartDateLT"]?>" />
	<input id="v" name="v" type="hidden" value="<?=$techID?>" />
	<input name="search" type="hidden" value="Search" />
	<input name="sortCol" type="hidden" value="<?php echo $_POST["sortCol"] ?>" />
	<input name="sortDir" type="hidden" value="<?php echo $_POST["sortDir"] ?>" />
</form>

<?php
	$sort = !empty($_POST["sortCol"])? $_POST["sortCol"] : '';
	$dir = !empty($_POST["sortDir"])? $_POST["sortDir"] : 0;
	$sortQueryWA = "";
	$sortQuery = "";
	switch ($sort) {
		case 0:
			$sortQueryWA = "Bid_Date";
			break;
		//case 1:
			//$sortQuery = "type";
			//break;
		case 2:
			$sortQuery = "Headline";
			break;
		case 3:
			$sortQuery = "StartDate";
			break;
		case 4:
			$sortQuery = "StartTime";
			break;
		case 5:
			$sortQuery = "EndTime";
			break;
		case 6:
			$sortQuery = "City";
			break;
		case 7:
			$sortQuery = "State";
			break;
		case 8:
			$sortQuery = "Zipcode";
			break;
		case 9:
			$sortQueryWA = "BidAmount";
			break;
		case 10:
			$sortQuery = "WIN_NUM";
		default:
			$sortQuery = "StartDate";
	}

	if ($sortQuery != "") $sortQuery = "ORDER BY $sortQuery " . ($dir == 0 ? "ASC" : "DESC");

    $db = Core_Database::getInstance();
    $query = $db->select();
    $query->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('WorkOrderID', 'BidAmount', 'Bid_Date'));
    $query->where('TechID = ?', $techID);
    if ( $bidDateGT ) $query->where("Bid_Date >= ?", $bidDateGT.' 00:00:00');
    if ( $bidDateLT ) $query->where("Bid_Date <= ?", $bidDateLT.' 23:59:59');
    if ( $sortQueryWA ) $query->order($sortQueryWA . ($dir == 0 ? " ASC" : " DESC"));
    $bidsResult = Core_Database::fetchAll($query);

	$appliedWOList = array();
	$bids = array();
	$index = 0;
	$wosList = array();
	foreach ($bidsResult as $bid) {
		$bids[$index] = array("WIN_NUM" => $bid['WorkOrderID'], "BidAmount" => $bid['BidAmount'], "Bid_Date" => $bid['Bid_Date']);
		$wosList[] = $bid['WorkOrderID'];
		if (!array_key_exists($bid['WorkOrderID'], $appliedWOList)) $appliedWOList[$bid['WorkOrderID']] = $index;
		++$index;
	}

	$wos = array();
	if (count($wosList)) {
		$db = Zend_Registry::get('DB');

		$searchQueryWO = array();
		$searchQueryParams = array();
		if ($startDateGT) {
			$searchQueryWO[] = 'StartDate >= ?';
			$searchQueryParams[] = $startDateGT . ' 00:00:00';
		}
		if ($startDateLT) {
			$searchQueryWO[] = 'StartDate <= ?';
			$searchQueryParams[] = $startDateLT . ' 23:59:59';
		}

		if (count($searchQueryWO)) {
			$searchQueryWO = ' AND ' . implode(' AND ', $searchQueryWO);
		} else {
			$searchQueryWO = '';
		}
		$sql = "SELECT WIN_NUM, IFNULL(DATE_FORMAT(StartDate, '%m/%d/%Y'), '') AS StartDate, IFNULL(StartTime, '') AS StartTime, IFNULL(EndTime,'') AS EndTime, City, State, Zipcode, IFNULL(Headline,'') AS Headline FROM work_orders WHERE WIN_NUM IN (" . $db->quote($wosList) . ") $searchQueryWO $sortQuery";
        $wosResult = $db->fetchAll($sql, $searchQueryParams);

		foreach ($wosResult as $wo) {
			$wos[$wo["WIN_NUM"]] = $wo;
		}
	}


	$appliedWO = array();
	if (empty($sortQuery)) {
		// merge using bid result order
		foreach($bids as $bid) {
			if (!array_key_exists($bid["WIN_NUM"], $wos)) continue;
			$appliedWO[] = array_merge($bid, $wos[$bid["WIN_NUM"]]);
		}
	}
	else {
		// merge using wo result order
		foreach($wos as $k => $v) {
			if (!array_key_exists($k, $appliedWOList)) continue;
			$index = $appliedWOList[$k];
			while ($index < count($bids) && $bids[$index]["WIN_NUM"] == $k) {
				$appliedWO[] = array_merge($bids[$index], $v);
				++$index;
			}
		}
	}


//	echo "<!-- $searchQuery-->";

	$numResults = sizeof($appliedWO);
	if ($numResults == 0) {
		echo "No Records Found.";
	}
	else if ($numResults <= 100) {
?>
		<table class="resultTable">
			<tr>
            	<td colspan="11" align="justify"><b>Sub-contractors and substitutions are not permitted without prior client approval.  Without such approval the<br />
                client has the sole discretion to deny access to the work site and may not pay the technician.<br /><br />
                </b></td>
            </tr>
            <tr class="resultTop"><td colspan="11">&nbsp;</td></tr>
			<tr class="resultHeader">
				<td class="leftHeader"><a href="javascript:sortBy(10)">WIN#</a></td>
				<td class="leftHeader"><a href="javascript:sortBy(0)">Bid Date</a></td>
				<td class="middleHeader"><a href="javascript:sortBy(1)"><img src="/images/pencil.png" title="Editable Bids" alt="Editable Bids" /></a></td>
				<td class="middleHeader"><a href="javascript:sortBy(2)">Headline</a></td>
				<td class="middleHeader"><a href="javascript:sortBy(3)">Start Date</a></td>
				<td class="middleHeader"><a href="javascript:sortBy(4)">Start Time</a></td>
				<td class="middleHeader"><a href="javascript:sortBy(5)">End Time</a></td>
				<td class="middleHeader"><a href="javascript:sortBy(6)">City</a></td>
				<td class="middleHeader"><a href="javascript:sortBy(7)">State</a></td>
				<td class="middleHeader"><a href="javascript:sortBy(8)">Zip</a></td>
				<td class="rightHeader"><a href="javascript:sortBy(9)">Bid</a></td>
			</tr>

<?php
	foreach ($appliedWO as $bid) {
		$type = 0;
		$bidDate = $bid["Bid_Date"];
		$startDate = $bid["StartDate"];
		$startTime = $bid["StartTime"];
		$endTime = $bid["EndTime"];
		$city = $bid["City"];
		$state = $bid["State"];
		$zipcode = $bid["Zipcode"];
		$pay = $bid["BidAmount"];
		$id = $bid["WIN_NUM"];
		$headline = $bid["Headline"];
// Changed by GB on 8/3/2009		echo "	<tr class=\"resultRow\"><td class=\"leftRow\"><a href='" . ($type == 0 ? "wosViewApplied_headline.php" : "wosViewAppliedFLS.php") . "?v=$techID&id=$id'>$bidDate</a></td><td>" . ($type == 0 ? "<span title=\"Editable Bid\">$</span>" : "&nbsp;") . "<td>$headline</td></td><td>$startDate</td><td>$startTime</td><td>$endTime</td><td>$city</td><td>$state</td><td>$zipcode</td><td class=\"rightRow\">$pay</td></tr>";

// Changed by GB on 8/3/2009		echo "	<tr class=\"resultRow\"><td class=\"leftRow\"><a href='" . ($type == 0 ? "wosViewApplied.php" : "wosViewAppliedFLS.php") . "?v=$techID&id=$id'>$bidDate</a></td><td>" . ($type == 0 ? "<span title=\"Editable Bid\">$</span>" : "&nbsp;") . "<td>$headline</td></td><td>$startDate</td><td>$startTime</td><td>$endTime</td><td>$city</td><td>$state</td><td>$zipcode</td><td class=\"rightRow\">$pay</td></tr>";

		echo "	<tr class=\"resultRow\"><td class=\"leftRow\"><a href=\"javascript:void(0)\" onclick=\"openPopup('" . ($type == 0 ? "wosViewApplied.php" : "wosViewAppliedFLS.php") . "?v=$techID&id=$id');\">$id</a></td>
		<td>$bidDate</td><td>" . ($type == 0 ? "<span title=\"Editable Bid\">$</span>" : "&nbsp;") . "<td>$headline</td></td><td>$startDate</td><td>$startTime</td><td>$endTime</td><td>$city</td><td>$state</td><td>$zipcode</td><td class=\"rightRow\">$pay</td></tr>";

	}
?>
			<tr>
				<td class="resultBottom" colspan="11">Results 1 - <?=$numResults?></td>
			</tr>
		</table>
<?php
	}
	else {
		echo "Too many records returned.<br/>Please modify your search criteria to return fewer  records.<br/>Click <a href=\"wosViewAppliedAll.php?v=$techID\">here</a> to return to the search page.";
	}
}
?>
</div>

<!--- End Content --->
<?php require ("../footer.php"); ?>
