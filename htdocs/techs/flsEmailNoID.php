<?php
	if (!isset($_GET["TechEmail"])) die();
	$email = $_GET["TechEmail"];
	if ($email == "") die();
	require_once("../library/smtpMail.php");
	$txt = "Dear " . $_GET["Tech_Name"] . ",\n\n
You recently applied for one or more open work orders at the Technician
Bureau for our FLS client. Please be advised that in order to be eligible
to run calls and/or work any projects for FLS, you will need a Badge and
two FLS ID numbers. If you have no Badge and/or FLS ID numbers, it
indicates that you have not yet been through the required FLS online
training or you just recently went through the training, submitted the test
with a passing score and are currently waiting to be activated and/or
issued your FLS Badge and two FLS ID numbers.\n
\n 
If you have not yet been through the FLS online training to have a Badge
sent to you and two FLS ID numbers issued in your name, you will need
do so via the link below in order to be eligible for FLS. Thank you.\n
\n
Online training for FLS starts here: http://www.flsupport.com/3.html\n
\n
Regards,\n
\n
Field Solutions\n
Web: www.fieldsolutions.com\n";

	$html = "Dear " . $_GET["Tech_Name"] . ",<br/><br/>
You recently applied for one or more open work orders at the Technician
Bureau for our FLS client. Please be advised that in order to be eligible
to run calls and/or work any projects for FLS, you will need a Badge and
two FLS ID numbers. If you have no Badge and/or FLS ID numbers, it
indicates that you have not yet been through the required FLS online
training or you just recently went through the training, submitted the test
with a passing score and are currently waiting to be activated and/or
issued your FLS Badge and two FLS ID numbers.<br/>
<br/> 
If you have not yet been through the FLS online training to have a Badge
sent to you and two FLS ID numbers issued in your name, you will need
do so via the link below in order to be eligible for FLS. Thank you.<br/>
<br/>
Online training for FLS starts here: <a href='http://www.flsupport.com/3.html'>http://www.flsupport.com/3.html</a><br/>
<br/>
Regards,<br/>
<br/>
Field Solutions<br/>
Web: <a href='www.fieldsolutions.com'>www.fieldsolutions.com</a><br/>";

	smtpMailLogReceived("FLS Support", "support@fieldsolutions.com", $email, "Required FLS Online Training", $txt, $html, "flsEmailNoID.php");
?>

<script type="text/javascript">
	try {
		opener.focus()
	} catch(e) {}
	
	window.close();
</script>