<?php
// Created on: 04-06-2009 
// Author: GB
// Description: Receives WO ID's from datapage js call and updates works orders to approve status.

//	ini_set("display_errors", 1);


try {

require_once("../library/caspioAPI.php");
require_once("../library/timeStamps2.php");

$woID = $_GET['woID'];
$woID = trim($woID, ",");
//echo "<p>WO ID: " . $woID . "</p>";

		$records = caspioSelectAdv("Work_Orders", "baseTechPay, OutofScope_Amount, TripCharge, MileageReimbursement, MaterialsReimbursement, Additional_Pay_Amount, AbortFeeAmount, Amount_Per, Time_In, Time_Out, Tech_Bid_Amount, Qty_Devices, calculatedTechHrs, AbortFee, Company_ID, Company_Name, Project_Name, Approved, TechMarkedComplete, PcntDeduct", "TB_UNID = '$woID'", "", false, "`", "|^",false);

		foreach ($records as $WorkOrder)
		{
			$fields = explode("|^", $WorkOrder);
			$baseTechPay = trim($fields[0], "`"); // Remove quotes	
			$outOfScopeAmount = trim($fields[1], "`"); // Remove quotes	
			$tripCharge = trim($fields[2], "`"); // Remove quotes	
			$milageReimbursement = trim($fields[3], "`"); // Remove quotes	
			$materialsReimbursement = trim($fields[4], "`"); // Remove quotes	
			$additionalPayAmount = trim($fields[5], "`"); // Remove quotes	
			$abortFeeAmount = trim($fields[6], "`"); // Remove quotes	
			$Amount_Per = trim($fields[7], "`"); // Remove quotes	
			$startTime = strtotime(trim($fields[8], "`")); // Remove quotes	
			$endTime = strtotime(trim($fields[9], "`")); // Remove quotes
			$bid_amount = trim($fields[10], "`"); // Remove quotes
			$qty_devices = trim($fields[11], "`"); // Remove quotes
			$tech_hrs = trim($fields[12], "`"); // Remove quotes	
			$abortFee = trim($fields[13], "`"); // Remove quotes		
			$companyID = trim($fields[14], "`"); // Remove quotes		
			$companyName = trim($fields[15], "`"); // Remove quotes
			$projectName = trim($fields[16], "`"); // Remove quotes
			$approved = trim($fields[17], "`"); // Remove quotes
			$techMarkedComplete = trim($fields[18], "`"); // Remove quotes
			$pcntDeduct = trim($fields[19], "`"); // Remove quotes
		}

		if($approved != "True" && $techMarkedComplete == "True") {
		
			$base_pay = $bid_amount;
			
			// Calc hours
			if ($endTime < $startTime) {
				$endTime += 86400;
			}
				
			$calculatedTechHrs = round(($endTime - $startTime)/3600, 2);

			if ($Amount_Per == "Hour") {				
				// Calc Base Tech Pay
				if ($calculatedTechHrs == 0 ) {
					$base_pay = 0;
				} else {
					$base_pay *= $calculatedTechHrs;
				}
			} else if ($Amount_Per == "Device") {
				$base_pay *= $qty_devices;
			}
			
			$baseTechPay = $base_pay;
		
	
			// Calc Pricing Fees to get total tech pay
			$PayAmount = 0;
			
			if($abortFee == 1 ) {
				$PayAmount = $abortFeeAmount;
			} else {
				$PayAmount = $baseTechPay;
			}
			
			// let's print the international format for the en_US locale
			$PayAmount += $outOfScopeAmount + $tripCharge + $milageReimbursement + $materialsReimbursement + $additionalPayAmount;
			// echo $money will output "123.1";
			$Net_Pay_Amount = $pcntDeduct == "True" ? $PayAmount * 0.9 : $PayAmount;
			$PayAmount = sprintf("%01.2f", $PayAmount);
			
			//echo "<p>Pay Amount: " . $PayAmount . "</p>";
	
			if ($base_pay == "NULL") $base_pay = '0.00';
			if ($PayAmount == "NULL") $PayAmount = '0.00';
			$valueList = "'$PayAmount', '$calculatedTechHrs', '$base_pay', '$Net_Pay_Amount'";
			$fieldList = "PayAmount, calculatedTechHrs, baseTechPay, Net_Pay_Amount"; // These are the fields we'll be updating during bulk approval.
		
			$records = caspioUpdate("Work_Orders", "$fieldList", "$valueList", "TB_UNID = '$woID'", false);
					
			/* print("
			<script type=\"text/javascript\">
				window.location = \"Hallmark-Test Results.html?TechID=$TechID&Grade=$Score&DateTaken=$DateTaken\";
			</script>
			"); */
	
		}
		print("
			<script type=\"text/javascript\">
			try {
			parent.reloadTabFrame();
			}
			catch (e) {
				if (opener) window.close();
			}
			</script>
			");		
		
}catch (SoapFault $fault) {

smtpMail("Updated Tech Pay", "nobody@fieldsolutions.com", "gbailey@fieldsolutions.com, tngo@fieldsolutions.com", "Updated Tech Pay Script Error", "$fault", "$fault", "updateTechPay.php");
}


	
?>
 
