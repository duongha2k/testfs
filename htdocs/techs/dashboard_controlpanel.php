<?php $page = 'techs'; ?>
<?php $option = 'dashboard';?>
<?php require ("../header_notices.php"); ?>
<?php require ("../library/googleMapAPI.php"); ?>

<link rel=stylesheet type="text/css" href="/widgets/css/main.css" />
<link rel="stylesheet" type="text/css" href="/widgets/css/popup.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="/widgets/js/FSTechPopupsManager.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetTechDashboard.js"></script>
<script type="text/javascript" src="/widgets/js/FSFindWorkOrder.js"></script>
<script type="text/javascript">
    //var roll;           //  Rollover objectd
    var tpm; //Popup manage
    //var td;             //  Widgets object
    //var td_left;             //  Widgets object
    //var td_right;             //  Widgets object
    //var roll;           //  Rollover object
    //var findWorkOrder;
    $(document).ready(function(){
        tpm = new FSTechPopupsManager();
        /*roll = new FSPopupRoll();
        td = new FSWidgetTechDashboard({container:'assignedContainer'});
        //td_left = new FSWidgetTechDashboard({container:'incompleteContainer'});
        //td_right = new FSWidgetTechDashboard({container:'availableContainer'});
        findWorkOrder = new FSFindWorkOrder(td, roll);
        findWorkOrder.buildHtml();
        //td.show({tab:'tech-assigned'});
        //td_left.show({tab:'tech-incomplete'});
        //td_right.show({tab:'tech-available'});

        $("#MyPayStatusBtn").click(function(){tpm.openPopupWin('banking');});


        $("#FindWorkOrderBtn").click(function(event) {
            roll.autohide(false);
            var opt = {
                width       : 325,
                height      : 220,
                title       : 'Find a Work Order',
                handler		: findWorkOrder.prepare,
                context     : td,
                body        : $(findWorkOrder.container()).html()
            };
            roll.showNotAjax(this, event, opt);
        });*/
    });
</script>

<?php require ("../navBar_shared.php"); ?>

<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193B0000E9C6D4G0I3E9C6D4G0I3","https:");}catch(ilolli){;}</script>
<!-- Check if logged in, if not redirect -->
<script type="text/javascript">
	try {
		var test = v;
		var techZip = z;
		if (v != "<?=$_GET["v"]?>")
			window.location.replace("<?=$_SERVER['PHP_SELF']?>?v=" + v);
		if (acceptTerms == "No") {
			alert("You must accept terms in your profile before viewing available work");
			document.location.replace("./");
		}
	}
	catch (e) {
		window.location.replace("./");
	}

	var showAllTimer = null;
	function onReady() {
		initialize();
		if (myLat != "" && myLong != "") {
			//showFutureAvailable(); // showAllOpenAvailable();
		}else {
			processAddress(techZip);
			//showAllTimer = setTimeout("showAllOpenAvailable()", 5000);
		}
	}

	function saveGeocode(point) {
		myLat = point.lat();
		myLong = point.lng();
		showAllOpenAvailable();
		clearTimeout(showAllTimer);
		document.getElementById("updateCoord").src = "ajax/setMyCoordinates.php?Latitude=" + myLat + "&Longitude=" + myLong;
	}
</script>
<?php
	googleMapAPIJS("onReady");
?>
<div id="widgetContainer" >
<div class="inner10" id="dashboardContainer">
	<div class="left_col">
            <h4>My Messages</h4>
            <ul>
                <li><a id="notificationsLink" onclick="tpm.openPopupWin('notifications');" href="javascript:///">Notifications</a></li>
<!--                <li><a id="emailsLink" href="#">My emails</a></li>-->
            </ul>
            <h4>My Work</h4>
            <ul>
                <li><a id="availableWorkLink" onclick="tpm.openPopupWin('availableWO');" href="javascript:///">Available Work</a></li>
                <li><a id="assignedWorkLink" onclick="tpm.openPopupWin('assignedWO');" href="javascript:///">Assigned Work</a></li>
                <li><a id="appliedWorkLink" onclick="tpm.openPopupWin('appliedWO');" href="javascript:///">Applied Work (my bids)</a></li>
                <li><a id="myPerfomanceRatingsLink" href="#">My Perfomance Ratings</a></li>
            </ul>

            <script type="text/javascript">
                $(document).ready(function(){
                    //Fill my perfomance rating mouseover.
                });
            </script>
            <h4>My Money</h4>
            <ul>
                <li><a id="incompleteWorkOrdersLink"onclick="tpm.openPopupWin('incompleteWO');" href="javascript:///">Incomplete Work Orders</a></li>
                <li><a id="siteCompleteLink" onclick="tpm.openPopupWin('workdoneWO');" href="javascript:///">Site Complete not yet approved work</a></li>
                <li><a id="approvedNotPaidLink" onclick="tpm.openPopupWin('completeWO');" href="javascript:///">Approved not yet paid work</a></li>
                <li><a id="directDepositEnrollmentLink" onclick="tpm.openPopupWin('banking');" href="javascript:///">Direct Deposit Enrollment</a></li>
            </ul>
            <h4>My Credentials</h4>
            <ul>
                <li><a id="myBasicProfileLink" href="javascript:///" onclick="tpm.openPopupWin('myProfile');">My Basic  Profile</a>
                </li>
                <li><a id="skillsLink" href="javascript:///" onclick="tpm.openPopupWin('skills');">Skills</a></li>
                <li><a id="experienceLink" href="javascript:///" onclick="tpm.openPopupWin('experience');">Experience</a></li>
                <li><a id="bgcheckLink" href="javascript:///" onclick="tpm.openPopupWin('bg-checks');" >Background Checks</a>
                <div class="check" id="bgcheckContent"></div>
                </li>
                <li><a href="javascript:///" onclick="tpm.openPopupWin('certifications');">Certifications</a>
                    <ul>
                        <li><a href="javascript:///" onclick="tpm.openPopupWin('FLS');">FLS</a></li>
                    </ul>
                </li>
            </ul>
            <h4>To Do List</h4>
            <ul>
<li><a href="javascript:///" onclick="tpm.openPopupWin('experience');">Complete Experience</a></li>
<li><a href="javascript:///" onclick="tpm.openPopupWin('certifications');">Complete Certifications</a></li>
<li><a href="javascript:///" onclick="tpm.openPopupWin('equipment');">Equipment</a></li>
<li><a href="javascript:///" onclick="tpm.openPopupWin('submitW9');">Complete W9</a></li>
<li><a href="javascript:///" onclick="tpm.openPopupWin('todo',{url:'/techs/Training/Hallmark/POS_Conversion.php'});">Hallmark POS Conversion Test</a></li>
<li><a href="javascript:///" onclick="tpm.openPopupWin('todo',{url:'/techs/Training/NCR/NCR_Basic_Certification.php'});">NCR Basic Certification</a></li>
<li><a href="javascript:///" onclick="tpm.openPopupWin('todo',{url:'/techs/Training/FLS/Starbucks_Training.php'});">FLS Starbucks Certification</a></li>
<li><a href="javascript:///" onclick="tpm.openPopupWin('todo',{url:'/FLS_CSP.php'});">Computing Security Procedures (FLS-CSP) Policy</a></li>
<li><a href="javascript:///" onclick="tpm.openPopupWin('todo',{url:'/techs/Training/FieldSolutions/FieldSolutionsTraining.php'});">Online Tutorial &amp; Field Solutions Certification</a></li>
<li><a href="javascript:///" onclick="tpm.openPopupWin('todo',{url:'/techs/Training/Maurices/MauricesTest.php'});" >Maurices Certification</a></li>
<li><a href="javascript:///" onclick="tpm.openPopupWin('todo',{url:'/techs/Training/CORE/CORE_Test.php'});">Core-Techs Certification</a><img src="../images/new.gif" alt="" width="23" height="12"></li>
<li><a href="javascript:///" onclick="tpm.openPopupWin('todo',{url:'/techs/cablingSkills.php'});" >CCTV Skills: Update your Cabling Profile Now for New Clients!</a></li>
<li><a href="javascript:///" onclick="tpm.openPopupWin('todo',{url:'/techs/telephonySkills.php'});" >Telephony Experience</a></li>
<li><a href="javascript:///" onclick="tpm.openPopupWin('FLS');">FLS</a></li>
            </ul>
            <h4>Contact</h4>
            <ul>
                <li><a href="javascript:///" onclick="tpm.openPopupWin('help');">Help</a></li>
            </ul>
	</div>
	<div class="right_col">
<!--	<div class="tCenter tButtons">
	        <div class="topToolBtn">
		<input type="button" class="link_button middle2_button" value="Find a Work Order" id="FindWorkOrderBtn">
		</div>
        <div class="topToolBtn">
			<input type="button" class="link_button" value="Quick Bid" id="QuickBidBtn">
		</div>
        <div class="topToolBtn">
			<input type="button" class="link_button" value="My Pay Status" id="MyPayStatusBtn">
		</div>

	</div>
-->
	<br style="clear: both;" />
        <h3>My Assigned Schedule (Start date/time order)</h3>
        <div id="assignedContainer">
            <div class="bg2" >Next 7 Days <div class="fr"><a onclick="tpm.openPopupWin('assignedWO');" href="javascript:///">Show all</a></div></div>
<div class="scroll h275">
<script type="text/javascript" language="javascript">try{f_cbload("193b0000d1a1b2g7b0h0f9d2b7g3&cbResetParam=1&StartDate=<?php echo date('m/d/Y');?>&EndDate=<?php echo date('m/d/Y',strtotime('+1 week'));?>","https:");}catch(v_e){;}</script>
</div>
</div>
		<table class="gray_box per50" cellspacing=0 cellpadding=0>
			<col width=50% />
			<col width=50% />
			<thead><tr><td class="borright gray_bg"><h3><nobr>Past Date Work Orders</nobr></h3><nobr>(needs action: mark as complete to be paid)</nobr></td><td class="gray_bg"><h3><nobr>New Work Available Apply Now</nobr></h3></td></tr></thead>
			<tr>
				<td width="50%" class="borright scroll" id="incompleteContainer" valign="top"><div class="scroll h275">
                                    <script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b0000i8i4a4f1b3a1d3f1b9e&cbResetParam=1","https:");}catch(v_e){;}</script>
                                </td>
				<td width="50%" style="overflow: hidden" id="availableContainer" valign="top">
				<div class="scroll h275"><script type="text/javascript" language="javascript">try{f_cbload("193b0000d3i9c9g5e0h0f3h6j1g2&cbResetParam=1&Latitude=" + myLat + "&Longitude=" + myLong + "&Distance=50","https:");}catch(v_e){;}</script></div>
                                </td>
			</tr>
		</table>
	</div>
</div>
    <script type="text/javascript">
        //This is ugly fix for caspio datapages.
        $(document).ready(function(){
            //Assigned
            var forms = document.getElementById('assignedContainer').getElementsByTagName('form');
            for (j = 0; j< forms.length;j++) {
                form = forms[j];
                if (form.getAttribute('id') != 'caspioform')
                    continue;


                //remove download button
                $("img[title='Download Data']",$(form)).parent().parent().html('');
                list = form.getElementsByTagName("tr");
                // hide tbuni column
                try {
                heading = list[1].childNodes[tbuniColumn];
                heading.style.display = "none";
                }catch(e){}

                for (i = 2; i < list.length-2; i++) {
                        try {
                                // work orders detail link
                                tb_unid = list[i].childNodes[tbuniColumn].childNodes[0].innerHTML;
                                woIDTD = list[i].childNodes[woIDColumn];
                                woID = woIDTD.innerHTML;
                                woIDTD.innerHTML = "<a href=\"javascript:///\" onclick=\"return tpm.openPopupWin('woDetails',{url:'/techs/wosDetails.php?id=" + tb_unid + "',name: 'Work Order #"+tb_unid+"'});\">" + woID + "</a>";
                                heading = list[i].childNodes[tbuniColumn];
                                heading.style.display = "none";
                        }
                        catch (e) {}
                }
            }
            //Incomplete
            var forms = document.getElementById('incompleteContainer').getElementsByTagName('form');
            for (j = 0; j< forms.length;j++) {
                form = forms[j];
                if (form.getAttribute('id') != 'caspioform')
                    continue;

                //remove download button
                $("img[title='Download Data']",$(form)).parent().parent().html('');
                
                list = form.getElementsByTagName("tr");
                // hide tbuni column
                try {
                heading = list[1].childNodes[tbuniColumn];
                heading.style.display = "none";
                }catch(e){}

                for (i = 2; i < list.length-2; i++) {
                        try {
                                // work orders detail link
                                tb_unid = list[i].childNodes[tbuniColumn].childNodes[0].innerHTML;
                                woIDTD = list[i].childNodes[woIDColumn];
                                woID = woIDTD.innerHTML;
                                woIDTD.innerHTML = "<a href=\"javascript:///\" onclick=\"return tpm.openPopupWin('woDetails',{url:'/techs/wosDetails.php?id=" + tb_unid + "',name: 'Work Order #"+tb_unid+"'});\">" + woID + "</a>";
                                heading = list[i].childNodes[tbuniColumn];
                                heading.style.display = "none";
                        }
                        catch (e) {}
                }
            }
            //Available container
            var forms = document.getElementById('availableContainer').getElementsByTagName('form');
            for (j = 0; j< forms.length;j++) {
                var form = forms[j];

                if (form.getAttribute('id') != 'caspioform')
                    continue;

                //remove download button
                $("img[title='Download Data']",$(form)).parent().parent().html('');
                
                list = form.getElementsByTagName("tr");

                // hide tbuni column
                try {
                heading = list[0].childNodes[tbuniColumn];
                heading.style.display = "none";
                }catch(e){}

                for (i = 1; i < list.length-2; i++) {
                    // work orders detail link
                    tb_unid = list[i].childNodes[tbuniColumn].childNodes[0].innerHTML;
                    woIDTD = list[i].childNodes[woIDColumn];
                    woID = woIDTD.innerHTML;
                    woIDTD.innerHTML = "<a href=\"javascript:///\" onclick=\"return tpm.openPopupWin('woDetails',{url:'/techs/wosDetails.php?id=" + tb_unid + "',name: 'Work Order #"+tb_unid+"'});\">" + woID + "</a>";
                    heading = list[i].childNodes[tbuniColumn];
                    heading.style.display = "none";
                }
            }
        });
    </script>
<br style="clear: both;" />
<div class="bottom_bar">
<div class="bot_col">Quick Links
	<ul>
		<li><a onclick="return tpm.openPopupWin('bg-checks');" href="javascript:///">Order a Background Check</a></li>
		<li><a href="javascript:///" onclick="tpm.openPopupWin('ICA');">Independent Contractor Agreement</a></li>
		<li><a onclick="return tpm.openPopupWin('submitW9');" href="javascript:///">Submit your W-9</a></li>
	</ul>
</div>
<div class="bot_col">
	<ul>
		<li><a onclick="return tpm.openPopupWin('FSPP');" href="javascript:///">Field Solutions Payment Policy</a></li>
		<li><a  href="javascript:///" onclick="tpm.openPopupWin('CoC');">Code of Conduct</a></li>
		<li><a  href="javascript:///" onclick="tpm.openPopupWin('privacypolicy');">Privacy Policy</a></li>
                <li><a  href="javascript:///" onclick="tpm.openPopupWin('termsofuse');">Terms of Use</a></li>
	</ul></div>
<div class="bot_col">
	<ul>
		<li><a onclick="return tpm.openPopupWin('get-started');" href="javascript:///">Get started in 3 easy steps</a></li>
		<li><a href="javascript:///" onclick="tpm.openPopupWin('community');">Tech Community</a></li>
		<li><a href="javascript:///" onclick="tpm.openPopupWin('aboutus');">About Us</a></li>
	</ul>
</div>
</div>
	<div class="bottom_links">
		<a href="https://www.fieldsolutions.com/wp/our-services/?view=1" title="Our Services">Our Services</a>
		<a href="https://www.fieldsolutions.com/wp/mission-values/?view=1" title="Mission &amp; Values">Mission &amp; Values</a>
		<a href="https://www.fieldsolutions.com/wp/our-clients/?view=1" title="Our Clients">Our Clients</a>
		<a href="https://www.fieldsolutions.com/wp/our-members/?view=1" title="Our Members">Our Members</a>
		<a href="https://www.fieldsolutions.com/wp/fieldsolutions-difference/?view=1" title="FieldSolutions Difference">FieldSolutions Difference</a>
		<a href="https://www.fieldsolutions.com/wp/management-team/?view=1 title="Management Team">Management Team</a>
		<a href="https://www.fieldsolutions.com/wp/news-and-events/?view=1" title="News and Events">News and Events</a>
		<a href="https://www.fieldsolutions.com/wp/wp-content/themes/field solutions/img/privacy_policy.pdf" target="_blank" title="Privacy Policy">Privacy Policy</a>
	</div>
        <div class="copyright">Copyright &copy; 2008-2010, Field Solutions, Inc. All rights reserved.</div>
</div>
<?php require ("../footer.php"); ?>