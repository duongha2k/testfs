<?php $page = login; ?>
<?php $option = tech; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->

<div id="clientSignup">

<H1>Independent  Technicians Seeking Contract Opportunities </H1>
<h2 align="center"> 5  Easy Steps To Using Field Solutions: </h2>

<p>
<ol>
  <li><a href="https://www.fieldsolutions.com/techs/register.php">Register Here</a> on the site as a technician</li>
  <li>Login as a Tech with your User/Password and complete as much of your profile information as possible. The more information you provide, the more likely you will be contracted for work (profile information can be added using the menu tabs under the Tech Dashboard tab)</li>
  <li>Search for contract opportunities by viewing open work orders from the Tech Dashboard screen or responding to emails sent to you by our clients. You can place a bid on an open work order by clicking on Details and scrolling to the bottom of the work order.</li>
  <li>If you are chosen for work, you will be assigned the work order. You will negotiate fees directly with our client. You can view the status of work orders assigned to you from the Tech Dashboard screen.</li>
  <li>Once work is completed, you must mark your work order complete online per instructions, where the client (the company you performed the work for) will approve the work order. Once approved, Field Solutions will process payment within 14 business days of the work order approval date.*</li>
</ol>
</p>

<br>

<p>*  Clients may choose to pay you directly. If you agree to that, you  must follow their policies and Field Solutions has no control over  timing of payment to you. </p>

<br /><br />
</p>
<p align="center">If  you have questions, email us at <a href="mailto:support@fieldsolutions.com">support@fieldsolutions.com</a>
</p>

<br /><br />
	

</div>

<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
