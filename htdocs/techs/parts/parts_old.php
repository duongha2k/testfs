<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Field Solutions</title>
<meta name="keywords" content="POS, Computer, Computer Repair, Technicians, Field Service, Retail Technicians, Home Computer Repair Technicians, Troubleshooting, PC Technicians" />
<meta name="description" content="Our focus is matching technicians seeking work with clients seeking technicians" />
<meta http-equiv="distribution" content="global" />
<meta http-equiv="copyright" content=" -2008" />
<meta http-equiv="url" content="http://www.fieldsolutions.com" />
<meta name="author" content="Gerald Bailey" />
<meta name="author" content="Collin McGarry" />
<meta name="author" content="Trung Ngo" />
<script type="text/javascript" src="https://www.fieldsolutions.com/library/jquery/jquery-1.2.1.pack.js"></script>
<script type="text/javascript" src="https://www.fieldsolutions.com/library/dataValidation.js"></script>
<link rel=stylesheet type ="text/css" href="https://www.fieldsolutions.com/templates/www/main.css" />
<!-- iPhone specific information -->
<link rel="stylesheet" type="text/css" media="screen and (device-width:320px or device-width:480" href="/iphone.css")>
<!-- End iPhone specific information -->
</head>
<link rel="stylesheet" href="css/main.css" type="text/css"/>
<link type="text/css" href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" />
<script src="js/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="js/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
<script src="js/i18n/jquery-ui-i18n.js" type="text/javascript"></script>
<script src="js/main.js" type="text/javascript"></script>
<body>
	 
	<table class="type_2" cellpadding="0" style="width:800px;">
        <tr>
			<th style="width:14%">
				<b>Parts Entry 1</b>
			</th>
			<th style="width:43%">
				<b>New Part</b>
			</th>
			<th style="width:43%">
				<b>Return Part</b>
			</th>
		</tr>
		<tr>
            <td>Part#</td>
            <td>12345</td>
            <td>67890</td>
        </tr>
		<tr>
            <td>Est. Arrival</td>
            <td>1/1/2010</td>
            <td>&#160;</td>
        </tr>
		<tr>
            <td>Carrier</td>
            <td>UPS</td>
            <td>&#160;</td>
        </tr>
		<tr>
            <td>Tracking#</td>
            <td>Z456</td>
            <td>&#160;</td>
        </tr>
		<tr>
            <td>Info</td>
            <td>Here is a new part</td>
            <td>Here is a return part</td>
        </tr>
		<tr>
            <td>Ship To</td>
            <td>Site</td>
            <td>&#160;</td>
        </tr>
		<tr>
            <td>RMA</td>
            <td>&#160;</td>
            <td>3344</td>
        </tr>
    </table>
	<table class="type_2" cellpadding="0" style="width:800px;">
        <tr>
			<th style="width:14%">
				<b>Parts Entry 1</b>
			</th>
			<th style="width:43%">
				<b>New Part</b>
			</th>
			<th style="width:43%">
				<b>Return Part</b>
			</th>
		</tr>
		<tr>
            <td>Part#</td>
            <td>56-23</td>
            <td>0</td>
        </tr>
		<tr>
            <td>Est. Arrival</td>
            <td>1/1/2010</td>
            <td>&#160;</td>
        </tr>
		<tr>
            <td>Carrier</td>
            <td>UPS</td>
            <td>&#160;</td>
        </tr>
		<tr>
            <td>Tracking#</td>
            <td>X12-586</td>
            <td>&#160;</td>
        </tr>
		<tr>
            <td>Info</td>
            <td>Here is a new part</td>
            <td>Here is a return part</td>
        </tr>
		<tr>
            <td>Ship To</td>
            <td>Site</td>
            <td>&#160;</td>
        </tr>
		<tr>
            <td>RMA</td>
            <td>&#160;</td>
            <td>&#160;</td>
        </tr>
    </table>
</body>
</html>