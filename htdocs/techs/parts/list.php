<?php
/**
 * @author Alex Scherba
 */
require_once("../../headerStartSession.php");
require_once("../../library/caspioAPI.php");
require_once("../../headerHead.php");
require_once('../../includes/parts/manager.php');
require_once('../../includes/parts/Auth.php');

$woId = (int)$_GET['woId'];
if ($_SESSION["loggedIn"]!='yes' || $_SESSION["loggedInAs"]!='tech' || empty($woId)) {header("Location:/techs/logIn.php");}

$auth = new PartsManagerAuth();
$auth->setLogin($_SESSION['UserName'])
     ->setRole(PartsManagerAuth::AUTH_TECH);
$authData = $auth->getAuthData();

$manager = new PartsManager();
$manager->setWoId($woId)
        ->setUser($authData['login'])
        ->setHash($authData['password'])
        ->setRole(PartsManager::PARTS_MANAGER_TECH);

$parts = simplexml_load_string($manager->getAllParts());

?>
<script type="text/javascript" src="../../library/jquery/calendar/jquery-calendar.js"></script>
<link rel="stylesheet" href="../../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<link rel="stylesheet" href="css/main.css" type="text/css"/>
<script src="js/main.js" type="text/javascript"></script>

<body>
    <div style="padding:20px;">
        <div class='logo1'></div><div class='assignErrors' id='assignErrorsDiv'><?if(isset($errors))foreach($errors as $error)echo$error;?></div>
<!--All parts-->
        <div>
        <table cellpadding="0" cellspacing="0" border="0" id="partsListTable" width="1108px">
            <tr><td>
                <div>
                    <div class='mapperServiceButtotn'><a href='' onClick="updateParts(<?echo $woId?>); return false;">Save All Changes and Close</a></div>
                    <div class='mapperServiceButtotn'><a href='' onClick="return confirmation();">Close without Saving Changes</a></div>
                    <div class='mapperServiceButtotn'><a href='list.php?woId=<?= $woId ?>&addpart=1'>Add a Part</a></div>
                </div>
            </td></tr>
            <tr>
                <th>
                    <span><b class='rTechTop'><b class='r0'></b><b class='r1'></b><b class='r2'></b><b class='r3'></b><b class='r4'></b></b></span>
                    <span><div style='background-color:#AACAFF;padding:10px;border-style:solid;border-color:#aaa;border-width:0px 1px;'><b>New Parts:</b></div></span>
                </th>
                <th>
                    <span><b class='rWoTop'><b class='r0'></b><b class='r1'></b><b class='r2'></b><b class='r3'></b><b class='r4'></b></b></span>
                    <span><div style='background-color:#AACAFF;padding:10px;border-style:solid;border-color:#aaa;border-width:0px 1px;'><b>Return / Old Parts:</b></div></span>
                </th>
            </tr>
<?
$i = 0;
if (!empty($parts)) {
	foreach ($parts as $part) {
	    $i++;
		if(!empty($part['id'])) {
		    $pId = $part['id'];
            //echo '<tr id="'.$pId.'" valign="top"';
            //if ($part->Consumable=='False')echo' name="partEx"';
            //echo '>';
		    echo '<tr id="'.$pId.'" valign="top" name="partEx">';
            if (is_object($part->NewPart)) {
?>
<td class="<?if($i%2==0)echo'trblue';else echo'tryellow';?>">
    <div style='padding: 10px;'>
        <div style='padding:5px;'>
            Part#: <?echo (string)$part->NewPart->PartNum;?>  Model#: <?echo (string)$part->NewPart->ModelNum;?>  Serial#: <?echo (string)$part->NewPart->SerialNum;?>
        </div>
        <div style='padding:5px;'>
            Est. Arrival: <?echo (string)$part->NewPart->ETA;?>  Carrier: <?echo (string)$part->NewPart->Carrier;?> Tracking#: <?echo (string)$part->NewPart->TrackingNum;?>  
        </div>
        <div style='padding:5px;'>
            Info: <?echo (string)$part->NewPart->Description;?>
        </div>
        <div style='padding:5px;'>
            Ship to: <?echo (string)$part->NewPart->ShipTo;?>          
        </div>
	    <? if (!empty($part->NewPart->ShippingAddress)):?>
        <div style='padding:5px;'>
            Address: <?echo (string)$part->NewPart->ShippingAddress;?>          
        </div>
	    <? endif;?>
    </div>
</td>
<?
            }
            //if (is_object($part->ReturnPart)) {
?>
<td class="<?if($i%2==0)echo'trblue';else echo'tryellow';?>">
    <div id="partReturn<?echo$pId?>">
    <table cellpadding="4" style="padding:10px;">
        <tr>
            <td>Part#:</td>
            <td><input style="width:100px;" name="returnPartNum" type="text" value="<?echo (string)$part->ReturnPart->PartNum;?>" /></td>
            <td>Model#:</td>
            <td><input style="width:100px;" name="returnModelNum" type="text" value="<?echo (string)$part->ReturnPart->ModelNum;?>" /></td>
            <td>Serial#:</td>
            <td><input style="width:100px;" name="returnSerialNum" type="text" value="<?echo (string)$part->ReturnPart->SerialNum;?>" /></td>
        </tr>
        <tr>
            <td>Est.Arrival:</td>
            <td><input id="123" name="returnETA" class="datepicker" style="width:100px;" type="text" value="<?echo (string)$part->ReturnPart->ETA;?>" /></td>
            <td>Carrier:</td>
            <td><select style="width:100%" name="returnCarrier" id="returnCarrier<?echo$pId?>">
                <option value="Fedex" <?if((string)$part->ReturnPart->Carrier=='Fedex')echo'selected';?>>Fedex</option>
                <option value="UPS" <?if((string)$part->ReturnPart->Carrier=='UPS')echo'selected';?>>UPS</option>
                <option value="USPS" <?if((string)$part->ReturnPart->Carrier=='USPS')echo'selected';?>>USPS</option>
                <option value="DHL" <?if((string)$part->ReturnPart->Carrier=='DHL')echo'selected';?>>DHL</option>
                <option value="Pilot" <?if((string)$part->ReturnPart->Carrier=='Pilot')echo'selected';?>>Pilot</option>
                <option value="Ceva" <?if((string)$part->ReturnPart->Carrier=='Ceva')echo'selected';?>>Ceva</option>
                </select>
            </td>
			<td>Tracking#:</td>
            <td><input style="width:100px;" name="returnTrackingNum" type="text" value="<?echo (string)$part->ReturnPart->TrackingNum;?>" /></td>
        </tr>
        <tr>
            <td>Info:</td>
            <td colspan="5"><input name="returnDescription" type="text" style="width:450px;" value="<?echo (string)$part->ReturnPart->Description;?>" /></td>
        </tr>
        <tr>
            <td>RMA:</td>
            <td colspan="5"><input name="returnRMA" type="text" style="width:450px;" value="<?echo (string)$part->ReturnPart->RMA;?>" /></td>
        </tr>
        <tr>
            <td>Return instructions:</td>
            <td colspan="5"><input name="returnInstructions" type="text" style="width:450px;" value="<?echo (string)$part->ReturnPart->Instructions;?>" /></td>
        </tr>
    </table>
    </div>
</td>
<?
	        //}

	        echo "</tr>";
	    }
	}
}
?>

<?php if ($_REQUEST["addpart"] == "1"): ?>
<tr name="partEx" id="newpart">
<td class="<?if($i%2==0)echo'tryellow';else echo'trblue';?>">
    <div style='padding: 10px;'>
    </div>
</td>
<td class="<?if($i%2==0)echo'tryellow';else echo'trblue';?>">
    <div>
    <table cellpadding="4" style="padding:10px;">
        <tr>
            <td>Part#:</td>
            <td><input style="width:100px;" name="returnPartNum" type="text" /></td>
            <td>Model#:</td>
            <td><input style="width:100px;" name="returnModelNum" type="text" /></td>
            <td>Serial#:</td>
            <td><input style="width:100px;" name="returnSerialNum" type="text" /></td>
        </tr>
        <tr>
            <td>Est.Arrival:</td>
            <td><input id="123" name="returnETA" class="datepicker" style="width:100px;" type="text" /></td>
            <td>Carrier:</td>
            <td><select style="width:100%" name="returnCarrier" id="returnCarrier">
                <option value="Fedex">Fedex</option>
                <option value="UPS">UPS</option>
                <option value="USPS">USPS</option>
                <option value="DHL">DHL</option>
                <option value="Pilot">Pilot</option>
                <option value="Ceva">Ceva</option>
                </select>
            </td>
			<td>Tracking#:</td>
            <td><input style="width:100px;" name="returnTrackingNum" type="text" /></td>
        </tr>
        <tr>
            <td>Info:</td>
            <td colspan="5"><input name="returnDescription" type="text" style="width:450px;" /></td>
        </tr>
        <tr>
            <td>RMA:</td>
            <td colspan="5"><input name="returnRMA" type="text" style="width:450px;" /></td>
        </tr>
        <tr>
            <td>Return instructions:</td>
            <td colspan="5"><input name="returnInstructions" type="text" style="width:450px;" /></td>
        </tr>
    </table>
    </div>
</td>
</tr>
<?php endif; ?>
                <tr><td>
                <div>
                    <div class='mapperServiceButtotn'><a href='' onClick="updateParts(<?echo $woId?>); return false;">Save All Changes and Close</a></div>
                    <div class='mapperServiceButtotn'><a href='' onClick="return confirmation();">Close without Saving Changes</a></div>
                    <div class='mapperServiceButtotn'><a href='list.php?woId=<?= $woId ?>&addpart=1'>Add a Part</a></div>
                </div>
            </td></tr>
        </table>
        </div>
    </div>
</body>
</html>