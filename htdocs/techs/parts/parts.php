<?php
/**
 * @author Alex Scherba
 */
$root = $_SERVER['DOCUMENT_ROOT'];
if (isset($_GET["append"]))
	$append = 1;
if (!isset($append)) {
	require_once($root . "/headerStartSession.php");
}
require_once($root . "/library/caspioAPI.php");
if (!isset($append)) {
	require_once($root . "/headerHead.php");
}
require_once($root . '/includes/parts/manager.php');
require_once($root . '/includes/parts/Auth.php');

$woId = (int)$_GET['woId'];
$companyID = $_GET["v"];

if ($_SESSION["loggedIn"]!='yes' || ($_SESSION["loggedInAs"]!='tech' && $_SESSION["loggedInAs"]!='client') || empty($woId)) {header("Location:/techs/logIn.php");}

$path = str_replace($root, "", dirname(__FILE__));

$auth = new PartsManagerAuth();
if ($_SESSION["loggedInAs"] == "tech") {
	$auth->setLogin($_SESSION['UserName'])
		 ->setRole(PartsManagerAuth::AUTH_TECH);
	$authData = $auth->getAuthData();
	$manager = new PartsManager();
	$manager->setWoId($woId)
			->setUser($authData['login'])
			->setHash($authData['password'])
			->setRole(PartsManager::PARTS_MANAGER_TECH);
}
else {
	$auth->setLogin($_SESSION['UserName'])
		 ->setRole(PartsManagerAuth::AUTH_CLIENT);
	$authData = $auth->getAuthData();
	
	$manager = new PartsManager();
	$manager->setWoId($woId)
			->setUser($authData['login'])
			->setHash($authData['password'])
			->setPmContext(trim($companyID))
			->setRole(PartsManager::PARTS_MANAGER_CLIENT);
}

$parts = simplexml_load_string($manager->getAllParts());
?>
<link rel="stylesheet" href="<?=$path?>/css/<?=isset($_GET["print"]) ? "print.css" : "main.css"?>" type="text/css"/>
<body>
<?php
if (!isset($append)):
?>
<table class="closeBtns" style="display:none"><tr><td><div class='mapperServiceButtotn' style="clear:left"><a href='' onClick="window.close(); return false">Close</a></div></td></tr></table>
<?php
endif;?>

<div style="padding:10px;">
<?
$count = 0;
if (!empty($parts)) {
	foreach ($parts as $part) {
		if(!empty($part['id'])) {
			$count++;
		    $pId = $part['id'];

?>
<table class="type_2" cellpadding="0" cellspacing="0" border="0" id="partsListTable" style="width:500px;">
	<tr>
		<th style="width:27%">Parts&nbsp;Detail&nbsp;#&nbsp;<? echo $count; ?></th>
		<th style="width:35%">New Part</th>
		<th style="width:38%">Return Part</th>
	</tr>
	<tr>
		<td>Part#:</td>
		<td><? if (is_object($part->NewPart)) { echo (string)$part->NewPart->PartNum; } ?> </td>
		<td><? if (is_object($part->ReturnPart)) { echo (string)$part->ReturnPart->PartNum; } ?></td>
	</tr>
	<tr>
		<td>Model#:</td>
		<td><? if (is_object($part->NewPart)) { echo (string)$part->NewPart->ModelNum; } ?> </td>
		<td><? if (is_object($part->ReturnPart)) { echo (string)$part->ReturnPart->ModelNum; } ?></td>
	</tr>
	<tr>
		<td>Serial#:</td>
		<td><? if (is_object($part->NewPart)) { echo (string)$part->NewPart->SerialNum; } ?> </td>
		<td><? if (is_object($part->ReturnPart)) { echo (string)$part->ReturnPart->SerialNum; } ?></td>
	</tr>
	<tr>
		<td>Est. Arrival:</td>
		<td><? if (is_object($part->NewPart)) { echo (string)$part->NewPart->ETA; } ?></td>
		<td><? if (is_object($part->ReturnPart)) { echo (string)$part->ReturnPart->ETA; } ?></td>
	</tr>
	<tr>
		<td>Carrier:</td>
		<td><? if (is_object($part->NewPart)) { echo (string)$part->NewPart->Carrier; } ?></td>
		<td><? if (is_object($part->ReturnPart)) { echo (string)$part->ReturnPart->Carrier; } ?></td>
	</tr>
	<tr>
		<td>Tracking#:  </td>
		<td><? if (is_object($part->NewPart)) { echo (string)$part->NewPart->TrackingNum; } ?></td>
		<td><? if (is_object($part->ReturnPart)) { echo (string)$part->ReturnPart->TrackingNum; } ?> </td>
	</tr>
	<tr>
		<td>Info: </td>
		<td><? if (is_object($part->NewPart)) { echo (string)$part->NewPart->Description; } ?> </td>
		<td><? if (is_object($part->ReturnPart)) { echo (string)$part->ReturnPart->Description; } ?></td>
	</tr>
	<tr>
		<td>Ship To:</td>
		<td><? if (is_object($part->NewPart)) { echo (string)$part->NewPart->ShipTo; } ?></td>
		<td><? if (is_object($part->ReturnPart)) { ?> &#160; <? } ?></td>
	</tr>
    <? if (!empty($part->NewPart->ShippingAddress)):?>
	<tr>
		<td>Address:</td>
		<td><? if (is_object($part->NewPart)) { echo (string)$part->NewPart->ShippingAddress; } ?></td>
		<td><? if (is_object($part->ReturnPart)) { ?> &#160; <? } ?></td>
	</tr>
    <? endif;?>
	<tr>
		<td>RMA:</td>
		<td>&#160;</td>
		<td><? if (is_object($part->ReturnPart)) { echo (string)$part->ReturnPart->RMA;} ?> </td>
	</tr>
	<tr>
		<td>Return instructions:</td>
		<td>&#160;</td>
		<td><? if (is_object($part->ReturnPart)) { echo (string)$part->ReturnPart->Instructions;} ?> </td>
	</tr>
</table>
<?      }
	}
}
?>
<script type="text/javascript">
$(document).ready(function() {
	try {
		if (opener || parent == window) {
			$(".closeBtns").show();
		}
	} catch (e) {}
});
<?php
if ($count == 0):
?>
try {
	parent.collapsePartsList();
} catch (e) {}
<?php
endif;
?>
</script>
</div>
<?php
if (!isset($append)):
?>
<table class="closeBtns" style="display:none"><tr><td><div class='mapperServiceButtotn' style="clear:left"><a href='' onClick="window.close(); return false">Close</a></div></td></tr></table>
</body>
</html>
<?php
endif;