<?php

if ($_SERVER['HTTP_X_REQUESTED_WITH'] = 'XMLHttpRequest') {
    require_once("../../../headerStartSession.php");
    require_once("../../../library/caspioAPI.php");
	require_once('../../../includes/parts/manager.php');
	require_once('../../../includes/parts/Auth.php');

	if ($_SESSION["loggedIn"]!='yes' || $_SESSION["loggedInAs"]!='tech') {exit();}
    $woId = (int)$_POST['woId'];
    if (empty($woId)) {exit();}

	$auth = new PartsManagerAuth();
    $auth->setLogin($_SESSION['UserName'])
         ->setRole(PartsManagerAuth::AUTH_TECH);
    $authData = $auth->getAuthData();
	
    $consumable = (int)$_POST['Consumable'];
    $params = array_map("formatData", $_POST);
    $xml='<PartEntry><ReturnPart><PartNum>'.$params['returnPartNum'].'</PartNum><ModelNum>'.$params['returnModelNum'].'</ModelNum>'
        .'<SerialNum>'.$params['returnSerialNum'].'</SerialNum><Description>'.$params['returnDescription'].'</Description>'
        .'<Carrier>'.$params['returnCarrier'].'</Carrier><TrackingNum>'.$params['returnTrackingNum'].'</TrackingNum><ETA>'.$params['returnETA'].'</ETA>'
        .'<RMA>'.$params['returnRMA'].'</RMA><Instructions>'.$params['returnInstructions'].'</Instructions></ReturnPart></PartEntry>';
    $manager = new PartsManager();
    $manager->setWoId($woId)
            ->setUser($authData['login'])
            ->setHash($authData['password'])
            ->setRole(PartsManager::PARTS_MANAGER_TECH);
    $result = $manager->createPart($xml);
    $errors = array();
    $xml = simplexml_load_string($result);
	if (!empty($xml->error)) {
	    foreach ($xml as $er) {
	    	$errors[] = '<br/>Error : '.$er->description;
	    }
	}
        else
        {
            $authData = array('login' => $_SESSION["UserName"], 'password' => $_SESSION["UserPassword"]);
            $tech = new Core_Api_TechUser;
            $tech->checkAuthentication($authData);
            $TechID = $tech->getTechId();
            $partId = (string)$xml->PartEntry["id"];
            $Core_Api_Class = new Core_Api_Class;
            $Core_Api_Class->updatePartEntryTechIdUpd($partId, $TechID);
        }   
        
    echo json_encode(array('errors'=>$errors, 'res'=>$params['returnPartNum']));
    exit();
	
}
exit();
function formatData($data){return strip_tags(trim($data));}