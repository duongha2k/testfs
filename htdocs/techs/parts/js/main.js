$(document).ready(function(){$(".datepicker").calendar({dateFormat: 'MDY/'});});

function updatePartsCount() {
	try {
		opener.updatePartsCount();
	} catch (e) {}	
}

function updateParts(woId){
    answer = confirm("Save changes?");
	if (answer){
	   $('#assignErrorsDiv').empty();
        partsEx  = $("tr[name='partEx']");
		$("*").css("cursor", "progress");
        updatePartsRecursive(partsEx, 0, woId);
	}
	return false;
}
function updatePartsRecursive(parts, num, woId){
	if (parts[num]) {
		updatePart(parts, woId, num);
	}
    else {
		updatePartsCount();
		this.close();
	}
    
}
function updatePart(parts, woId, num){
    partId = parts[num].id;

    if (partId != "newpart") {
	    params = 'partId='+partId+'&woId='+woId;
	    allInputs = $('#'+partId+' input:enabled');
	    allInputs.each(function(n,element){if (element.type == 'text') params += '&'+element.name+'='+element.value;});
	    allSelects = $('#'+partId+' select:enabled');
	    allSelects.each(function(n,el){ params+='&'+el.name+'='+$(el).val(); });

	    $.ajax({type:"POST", url:"ajax/updatePart.php", dataType:"html", data:params,
	        success: function(data) {
	    		errors = '';
	            //jQuery.each(data.errors, function() { errors += '<b>'+this+'</b><br/>'; });
	            if (errors != ''){
	                $('#assignErrorsDiv').html(errors);
	                $('#assignErrorsDiv').css('color', '#FF0000');
	            }
	            updatePartsRecursive(parts, num+1, woId);
	        }
	    });
	}
    else {
	    params = 'woId='+woId;
	    allInputs = $('#'+partId+' input:enabled');
	    allInputs.each(function(n,element){if (element.type == 'text') params += '&'+element.name+'='+element.value;});
	    allSelects = $('#'+partId+' select:enabled');
	    allSelects.each(function(n,el){ params+='&'+el.name+'='+$(el).val(); });

	    $.ajax({type:"POST", url:"ajax/insertPart.php", dataType:"html", data:params,
	        success: function(data) {
	    		errors = '';
	            //jQuery.each(data.errors, function() { errors += '<b>'+this+'</b><br/>'; });
	            if (errors != ''){
	                $('#assignErrorsDiv').html(errors);
	                $('#assignErrorsDiv').css('color', '#FF0000');
	            }
	            updatePartsRecursive(parts, num+1, woId);
	        }
	    });
    }
}
function confirmation() {
	var answer = confirm("Are you sure?");
	if (answer) {
		updatePartsCount();
		window.close();
	}
	else return false;
}