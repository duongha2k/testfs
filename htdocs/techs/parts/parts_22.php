<?php
/**
 * @author Alex Scherba
 */
require_once("../../headerStartSession.php");
require_once("../../library/caspioAPI.php");
require_once("../../headerHead.php");
require_once('../../includes/parts/manager.php');
require_once('../../includes/parts/Auth.php');
ini_set('error_reporting', E_ALL & ~E_NOTICE);
ini_set('display_errors', 'On');

$woId = (int)$_GET['woId'];
if ($_SESSION["loggedIn"]!='yes' || $_SESSION["loggedInAs"]!='tech' || empty($woId)) {header("Location:/techs/logIn.php");}

$auth = new PartsManagerAuth();
$auth->setLogin($_SESSION['UserName'])
     ->setRole(PartsManagerAuth::AUTH_TECH);
$authData = $auth->getAuthData();

$manager = new PartsManager();
$manager->setWoId($woId)
        ->setUser($authData['login'])
        ->setHash($authData['password'])
        ->setRole(PartsManager::PARTS_MANAGER_TECH);

$parts = simplexml_load_string($manager->getAllParts());

?>
<link rel="stylesheet" href="css/main.css" type="text/css"/>
<link type="text/css" href="css/jquery-ui-1.7.2.custom.css" rel="stylesheet" />
<script src="js/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="js/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
<script src="js/i18n/jquery-ui-i18n.js" type="text/javascript"></script>
<script src="js/main.js" type="text/javascript"></script>
<body>

    <div style="padding:20px;">
        
        <div class='assignErrors' id='assignErrorsDiv'></div>
		<!--All parts-->
        <div style="padding-bottom:30px; display:inline;">
            
<?
$i = 0;
if (!empty($parts)) {
	foreach ($parts as $part) {
	    $i++;
		if(!empty($part['id'])) {
		    $pId = $part['id'];
?>
            <table class="type_2" cellpadding="0" cellspacing="0" border="0" id="partsListTable" style="width:800px;">
					<tr>
						<th style="width:18%">
							<b>Parts Entry <? echo $i ?> </b>
						</th>
						<th style="width:41%">
							<b>New Part</b>
						</th>
						<th style="width:41%">
							<b>Return Part</b>
						</th>
					</tr>

					<tr>
						<td>Part#</td>
						<td><? if (is_object($part->NewPart)) { echo (string)$part->NewPart->PartNum; } ?> </td>
						<td><? if (is_object($part->ReturnPart)) { echo (string)$part->ReturnPart->PartNum; } ?></td>
					</tr>
					<tr>
						<td>Est. Arrival</td>
						<td><? if (is_object($part->NewPart)) { echo (string)$part->NewPart->ETA; } ?></td>
						<td><? if (is_object($part->ReturnPart)) { echo (string)$part->ReturnPart->ETA; } ?></td>
					</tr>
					<tr>
						<td>Carrier</td>
						<td><? if (is_object($part->NewPart)) { echo (string)$part->NewPart->Carrier; } ?></td>
						<td><? if (is_object($part->ReturnPart)) { echo (string)$part->ReturnPart->Carrier; } ?></td>
					</tr>
					<tr>
						<td>Tracking#  </td>
						<td><? if (is_object($part->NewPart)) { echo (string)$part->NewPart->TrackingNum; } ?></td>
						<td><? if (is_object($part->ReturnPart)) { echo (string)$part->ReturnPart->TrackingNum; } ?> </td>
					</tr>
					<tr>
						<td>Info </td>
						<td><? if (is_object($part->NewPart)) { echo (string)$part->NewPart->Description; } ?> </td>
						<td><? if (is_object($part->ReturnPart)) { echo (string)$part->ReturnPart->Description; } ?></td>
					</tr>
					<tr>
						<td>Ship To</td>
						<td><? if (is_object($part->NewPart)) { echo (string)$part->NewPart->ShipTo; } ?></td>
						<td><? if (is_object($part->ReturnPart)) { ?> &#160; <? } ?></td>
					</tr>
					<tr>
						<td>RMA:</td>
						<td>&#160;</td>
						<td><? if (is_object($part->ReturnPart)) { echo (string)$part->ReturnPart->RMA;} ?> </td>
					</tr>
					 

		 
<?
	    }
	}
}
?>
            </table>
        </div>
    </div>

</body>
</html>