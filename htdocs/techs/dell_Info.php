<?php $page = 'techs'; ?>
<?php $option = 'dell_Info'; ?>
<?php
	require_once("../headerStartSession.php");
	require_once("../library/FSSession.php");
	FSSession::validateSessionParams(array("v" => "TechID"));
?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php
//	ini_set("display_errors", 1);
	require_once("../library/mySQL.php");
	require_once("../library/smtpMail.php");
	$msg = "";
	$proceed = false;
	if (isset($_POST["submit"])) {
//		print_r($_POST);
		if (array_key_exists("SSN", $_POST) && 
			stripos($_POST["SSN"], "x") === FALSE) {
			$proceed = true;
		}
		else {
			// bad account info
			$msg = "Your SSN doesn't appear to be valid";
		}
	}
	if (isset($_POST["submit"]) && $proceed) {
		// Update tech info
		require_once("../library/openSSL.php");
		

		$publicKey = getDellPublicKey();
		if (!$publicKey) {
		 	echo "Unable to save information at this time.<br/>";
		 	die();
		}
		
		$repeat = strlen($_POST["SSN"]) - 4;
		//	$masked_SSN = str_repeat("X", $repeat) . substr($_POST["SSN"], -4);
		$masked_SSN = substr($_POST["SSN"], -4);
		
		$firstname = mysqlEscape($_POST["FirstName"]);
		$middlename = mysqlEscape($_POST["MiddleName"]);
		$lastname = mysqlEscape($_POST["LastName"]);
		$ssn = mysqlEscape(encryptData($_POST["SSN"], $publicKey));
		$DOB  = date("Y-m-d", strtotime($_POST["DOB"]));
		$POBcity = mysqlEscape($_POST["POBcity"]);
		$POBstate = mysqlEscape($_POST["POBstate"]);
		$POBcountry = mysqlEscape($_POST["POBcountry"]);
		$sex = mysqlEscape($_POST["Sex"]);
		$race = mysqlEscape($_POST["Race"]);
		$telephone = mysqlEscape($_POST["Telephone"]);
		$email = mysqlEscape($_POST["Email"]);
//		$employer = mysqlEscape($_POST["Employer"]);
//		$position = mysqlEscape($_POST["Position"]);
//		$ETA  = date("H:i", strtotime($_POST["ETA"]));
//		$ETD  = date("Y-m-d", strtotime($_POST["ETD"]));
		
//		$insertQuery = "INSERT INTO Dell_TechInfo (TechID, FirstName, MiddleName, LastName, SSN, MaskedSSN, DOB, POBcity, POBstate, POBcountry, Sex, Race, Telephone, Email, Employer, Position, ETA, ETD) VALUES ('{$_POST['TechID']}', '$firstname', '$MiddleName', '$lastname', '$ssn', '$masked_SSN', '$DOB', '$POBcity', '$POBstate', '$POBcountry', '$sex', '$race', '$telephone', '$email', '$employer', '$position', '$ETA', '$ETD') ON DUPLICATE KEY UPDATE TechID = '{$_POST['TechID']}', FirstName = '$firstname', MiddleName= '$MiddleName', LastName = '$lastname', SSN = '$ssn', MaskedSSN = '$masked_SSN', DOB = '$DOB', POBcity = '$POBcity', POBstate = '$POBstate', POBcountry = '$POBcountry', Sex = '$sex', Race = '$race', Telephone = '$telephone', Email = '$email', Employer = '$employer', Position = '$position', ETA = '$ETA', ETD = '$ETD'";

		$insertQuery = "INSERT INTO Dell_TechInfo (TechID, FirstName, MiddleName, LastName, SSN, MaskedSSN, DOB, POBcity, POBstate, POBcountry, Sex, Race, Telephone, Email, Employer, Position) VALUES ('{$_POST['TechID']}', '$firstname', '$middlename', '$lastname', '$ssn', '$masked_SSN', '$DOB', '$POBcity', '$POBstate', '$POBcountry', '$sex', '$race', '$telephone', '$email', 'FieldSolutions', 'Technician') ON DUPLICATE KEY UPDATE TechID = '{$_POST['TechID']}', FirstName = '$firstname', MiddleName= '$middlename', LastName = '$lastname', SSN = '$ssn', MaskedSSN = '$masked_SSN', DOB = '$DOB', POBcity = '$POBcity', POBstate = '$POBstate', POBcountry = '$POBcountry', Sex = '$sex', Race = '$race', Telephone = '$telephone', Email = '$email'";


		if (mysqlQuery($insertQuery)) {
			$msg = "<font color='red'>Your information has been updated</font>";
			$techID = array($_POST['TechID']);

			// Build and send email to Dell Project Lead
			$vFromName = "Field Solutions Support";
			$vFromEmail = "no-replies@fieldsolutions.com";
			$eList = "va-notification@fieldsolutions.com";
			// $eList = "jturner@fieldsolutions.com";
			@$caller = "dell_Info";
			$vSubject = "Tech has updated their Dell info.";	

			// Send Email to the Tech
			$message = "$firstname $lastname has updated their Dell information.

Regards,
Field Solutions
Web: <a href='http://www.fieldsolutions.com'>www.fieldsolutions.com</a>";
		
			$htmlmessage = nl2br($message);

			// Emails Tech
			smtpMail($vFromName, $vFromEmail, $eList, $vSubject, $message, $htmlmessage, $caller);


		}



}
	
	// get tech info
	$sexId = 0;
	$techID = $_GET["v"];
	if (isset($_GET["v"]) && is_numeric($techID)) {
		$techInfo = mysqlFetchAssoc(mysqlQuery("SELECT * FROM Dell_TechInfo WHERE TechID = $techID"));
		if ($techInfo) {
			$techInfo = $techInfo[0];	
			
			//$sex = $techInfo["Sex"];
			
			$techInfo["SSN"] = "XXX-XX-" . $techInfo["MaskedSSN"];
			$techInfo["DOB"] = date("m/d/Y", strtotime($techInfo["DOB"]));
		//	$techInfo["ETA"] = date("g:i A", strtotime($techInfo["ETA"]));
		//	$techInfo["ETD"] = date("m/d/Y", strtotime($techInfo["ETD"]));

		}
		else
		{
			$techInfo = array();
					
			require_once("../library/caspioAPI.php");
			$result = caspioSelectAdv("TR_Master_List", "FirstName, LastName", "TechID = '$techID'", "", false, "`");
			$parts = explode(",", $result[0]);
			$techInfo["FirstName"] = trim($parts[0], "`");
			$techInfo["LastName"] = trim($parts[1], "`");
			
		}
		
	$sex = array("Male", "Female");
	$sexHTML = "<option value=\"\" " . ($sexId == 0 ? "selected=\"selected\"" : "") .  ">-------</option>";
	$i = 0;
	foreach ($sex as $gender) {
		$sexHTML .= "<option value=\"$sex[$i]\" " . ($techInfo["Sex"] == $sex[$i] ? "selected=\"selected\"" : "") . ">{$sex[$i]}</option>";
		$i++;
	}

	}
	
?>
<style  TYPE="text/css">
div#main td {
	text-align: left;
}

.label{ font-size:12px; color:#385C7E;}
.required {
	font-size: 12px;
	color: #FF0000;
}
.footer {
	text-align: center;
	background-color: #BACBDF; 
	border-top: #385C7E solid medium; 
	padding: 5px;
}
.caspioButton
{
/*Back Button Attributes*/
color: #ffffff;
font-size: 12px;
font-family: Verdana;
font-style: normal;
font-weight: bold;
text-align: center;
vertical-align: middle;
border-color: #5496C6;
border-style: solid;
border-width: 1px;
background-color: #032D5F;
/* Forced by default to add space between buttons */
width: auto;
height: auto;
margin: 0 3px;
}

</style>

<br /><br />

<div align="center" id="main">
	<?=$msg?>
	<form id="DellInfo" name="DellInfo" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>?v=<?=$techID?>" onSubmit="return validate()">
	<input id="TechID" name="TechID" type="hidden" value="<?=$techID?>" />
	<table id="DellInfoTable" cellpadding="0" cellspacing="0" border="0" style="border:#385C7E solid medium; width: 500px">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="5">
					<tr>
						<td class="label">First Name</td>
						<td><input id="FirstName" name="FirstName" type="text" size="25" value="<?=$techInfo["FirstName"]?>" /></td>
					</tr>
					<tr>
						<td class="label">Middle Name</td>
						<td><input id="MiddleName" name="MiddleName" type="text" size="25" value="<?=$techInfo["MiddleName"]?>" /></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>(If none, please enter NMN)</td>
					</tr>
					<tr>
						<td class="label">Last Name</td>
						<td><input id="LastName" name="LastName" type="text" size="25" value="<?=$techInfo["LastName"]?>" /></td>
					</tr>
					<tr>
						<td class="label">SSN</td>
						<td><input name="SSN" type="text" id="SSN" value="<?=$techInfo["SSN"]?>" size="12" maxlength="12" /></td>
					</tr>
					<tr>
						<td class="label">DOB</td>
						<td><input name="DOB" type="text" id="DOB" value="<?=$techInfo["DOB"]?>" size="10" maxlength="10" /></td>
					</tr>
					<tr>
						<td class="label">POB City</td>
						<td><input id="POBcity" name="POBcity" type="text" size="25" value="<?=$techInfo["POBcity"]?>" /></td>
					</tr>
					<tr>
						<td class="label">POB State</td>
						<td><input name="POBstate" type="text" id="POBstate" value="<?=$techInfo["POBstate"]?>" size="2" maxlength="2" /></td>
					</tr>
					<tr>
						<td class="label">POB Country</td>
						<td><input name="POBcountry" type="text" id="POBcountry" value="<?=$techInfo["POBcountry"]?>" size="25" maxlength="25" /></td>
					</tr>
					<tr>
						<td class="label">Sex</td>
						<td><select id="Sex" name="Sex">
							<?=$sexHTML?>
						  </select>
						</td>
				 	</tr>
					<tr>
						<td class="label">Race</td>
						<td><input id="Race" name="Race" type="text" size="25" value="<?=$techInfo["Race"]?>" /></td>
					</tr>
					<tr>
						<td class="label">Telephone</td>
						<td><input id="Telephone" name="Telephone" type="text" size="14" maxlength="14" value="<?=$techInfo["Telephone"]?>" /></td>
					</tr>
					<tr>
						<td class="label">Email</td>
						<td><input id="Email" name="Email" type="text" size="25" value="<?=$techInfo["Email"]?>" /></td>
					</tr>
                    
					<!-- tr>
						<td class="label">Employer</td>
						<td><input id="Employer" name="Employer" type="text" size="25" value="<?//=$techInfo["Employer"]?>" /></td>
					</tr>
					<tr>
						<td class="label">Position</td>
						<td><input id="Position" name="Position" type="text" size="25" value="<?//=$techInfo["Position"]?>" /></td>
					</tr>
					<tr>
						<td class="label">ETA</td>
						<td><input id="ETA" name="ETA" type="text" size="8" maxlength="8" value="<?//=$techInfo["ETA"]?>" /></td>
					</tr>
					<tr>
						<td class="label">ETD</td>
						<td><input id="ETD" name="ETD" type="text" size="10" maxlength="10" value="<?//=$techInfo["ETD"]?>" /></td>
					</tr -->
				</table>
			</td>
		</tr>
		<tr>
			<td class="footer" style="text-align: center">
				<input id="submit" name="submit" type="submit" value="Update" class="caspioButton" />
			</td>
		</tr>
	</table>
	
	</form>
	

<script type="text/javascript">

function massageMyTime() {
	this.value = massageTime(this.value);
}

function massageMyDate() {
	this.value = massageDate(this.value);
}

function massageMySSN() {
	this.value = massageSSN(this.value);
}

function massageMyPhone() {
	this.value = massagePhone(this.value);
}

function clearField() {
	if(this.value == "XXX-XX-XXXX" || this.value == "MM/DD/YYYY"){
		this.value = "";
	}
}

$(document).ready(function(){

	$("#Telephone").change(massageMyPhone);
	$("#SSN").change(massageMySSN);
	$("#DOB").change(massageMyDate);
//	$("#ETA").change(massageMyTime);
//	$("#ETD").change(massageMyDate);
	
	if ($("#MiddleName").attr("value") == undefined) {
		$("#MiddleName").focus();
	}
	
	if ($("#SSN").attr("value") == undefined) {
		$("#SSN").attr("value", "XXX-XX-XXXX");
	}
	$("#SSN").click(clearField);
	$("#SSN").focus(clearField);

	if ($("#DOB").attr("value") == undefined) {
		$("#DOB").attr("value", "MM/DD/YYYY");
	}
	$("#DOB").click(clearField);
	$("#DOB").focus(clearField);

	
});


function validate() {
	
	if ($("#FirstName").attr("value") == undefined) {
		$("#FirstName").focus();
		alert("Please enter your first name");
		return false;
	}
	
	if ($("#MiddleName").attr("value") == undefined) {
		$("#MiddleName").focus();
		alert("Please enter your middle MiddleName or use 'NMN'");
		return false;
	}
	
	if ($("#LastName").attr("value") == undefined) {
		$("#LastName").focus();
		alert("Please enter your last name");
		return false;
	}
	
	if ($("#SSN").attr("value") == undefined || !isValidSSN($("#SSN").attr("value"))) {
		$("#SSN").focus();
		alert("Please enter a valid SSN");
		return false;
	}
	
	if ($("#DOB").attr("value") == undefined || !isValidDate($("#DOB").attr("value"))) {
		$("#DOB").focus();
		alert("Please enter a valid DOB");
		return false;
	}
	
	if ($("#POBcity").attr("value") == undefined) {
		$("#POBcity").focus();
		alert("Please enter your POB City");
		return false;
	}
	
	if ($("#POBstate").attr("value") == undefined) {
		$("#POBstate").focus();
		alert("Please enter your POB State");
		return false;
	}
	
	if ($("#POBcountry").attr("value") == undefined) {
		$("#POBcountry").focus();
		alert("Please enter your POB Country");
		return false;
	}
		
	if ($("#Race").attr("value") == undefined) {
		$("#Race").focus();
		alert("Please enter your Race");
		return false;
	}
	
	if ($("#Telephone").attr("value") == undefined || !isValidPhone($("#Telephone").attr("value"))) {
		$("#Telephone").focus();
		alert("Please enter a valid telephone");
		return false;
	}

	if ($("#Email").attr("value") == undefined || !isValidEmail($("#Email").attr("value"))) {
		$("#Email").focus();
		alert("Please enter a valid email address");
		return false;
	}

/*	if ($("#Employer").attr("value") == undefined) {
		$("#Employer").focus();
		alert("Please enter your Employer");
		return false;
	}
	
	if ($("#Position").attr("value") == undefined) {
		$("#Position").focus();
		alert("Please enter your Position");
		return false;
	}

	if ($("#ETA").attr("value") == undefined || !isValidTime($("#ETA").attr("value"))) {
		$("#ETA").focus();
		alert("Please enter a valid time");
		return false;
	}
	
	if ($("#ETD").attr("value") == undefined || !isValidDate($("#ETD").attr("value"))) {
		$("#ETD").focus();
		alert("Please enter a valid ETD");
		return false;
	}
*/
	if ($("select#Sex").val() == ""){
		$("#Sex").focus();
		alert("Please select your gender");
		return false;
	}
	
	return true;
}


</script>

</div>

<!--- End Content --->
<?php require ("../footer.php"); ?>
