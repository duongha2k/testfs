<?php
	require_once("../library/caspioAPI.php");
	
//	$today = date("m/d/Y");
	$dayOfWeek = date("w"); // 0 = Sunday, 6 = Saturday

	$startWeek = date("m/d/Y", strtotime("-$dayOfWeek days")); // Sunday
	$endWeek = date("m/d/Y", strtotime("This Saturday")); // Saturday
	
	$startMonth = date("m/1/Y"); 
	$endMonth = date("m/d/Y", strtotime("$startMonth + 1 month - 1 day")); 
	
	if (isset($_GET["v"])) {
		$techID = $_GET["v"];
		if (!is_numeric($techID)) die();
		
		$woAllOpen = 0;
		$woThisMonth = 0;
		$woThisWeek = 0;
		$woToday = 0;
				
		try {
			$count = caspioSelectAdv("Work_Orders", "count(WO_ID)", "Tech_ID = '$techID' AND Deactivated = '0'", "", false, "`", "|", false);
			$woAllOpen = $count[0];

			if ($woAllOpen > 0) {
				$count = caspioSelectAdv("Work_Orders", "count(WO_ID)", "Tech_ID = '$techID' AND StartDate >= '$startMonth' AND StartDate <= '$endMonth' AND Deactivated = '0'", "", false, "`", "|", false);	
				$woThisMonth = $count[0];
			}
					
			if ($woThisMonth > 0) {
				$count = caspioSelectAdv("Work_Orders", "count(WO_ID)", "Tech_ID = '$techID' AND StartDate >= '$startWeek' AND StartDate <= '$endWeek' AND Deactivated = '0'", "", false, "`", "|", false);
				$woThisWeek = $count[0];
			}
	
/*			if ($woToday > 0) {
				$count = caspioSelectAdv("Work_Orders", "count(WO_ID)", "Company_ID = '$companyID' AND StartDate = '$today' AND Deactivated = '0'", "", false, "`", "|", false);
				$woToday = $count[0];
			}*/
		}
		catch (SoapFault $fault) {
		}
	}
?>