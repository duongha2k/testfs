<?php
	require_once("../../library/caspioAPI.php");
	
//	$today = date("m/d/Y");
	$dayOfWeek = date("w"); // 0 = Sunday, 6 = Saturday

	$startWeek = date("m/d/Y", strtotime("-$dayOfWeek days")); // Sunday
	$endWeek = date("m/d/Y", strtotime("This Saturday")); // Saturday
	
	$startMonth = date("m/1/Y"); 
	$endMonth = date("m/d/Y", strtotime("$startMonth + 1 month - 1 day")); 
	
	if (isset($_GET["v"])) {
		$techID = $_GET["v"];
		if (!is_numeric($techID)) die();
		
		$woAllOpen = 0;
		$woThisMonth = 0;
		$woThisWeek = 0;
		$woToday = 0;
				
		try {
			$dateSort = "(CASE WHEN StartDate >= '$startWeek' AND StartDate <= '$endWeek' THEN 2 WHEN StartDate >= '$startMonth' AND StartDate <= '$endMonth' THEN 1 ELSE 0 END)";
			
			$count = caspioSelectAdv("Work_Orders", "count(TB_UNID), $dateSort", "Tech_ID = '$techID' AND Deactivated = '0' GROUP BY $dateSort", "$dateSort DESC", false, "`", "|", false);
			$index = 0;
			
			$countMap = array();
			
			foreach ($count as $range) {
				if ($range == "") continue;
				$info = explode("|", $range);
				$countMap[$info[1]] = $info[0];
			}
						
			for ($i = 2; $i >= 0; $i--) {
				$woCount = array_key_exists($i, $countMap) ? $countMap[$i] : 0;
				switch ($i) {
					case 2:
						$woThisWeek = $woCount;
						break;
					case 1:
						$woThisMonth = $woCount + $woThisWeek;
						break;
					default:
						$woAllOpen = $woCount + $woThisMonth;
						break;
				}
			}
		}
		catch (SoapFault $fault) {
		}

		echo "$woAllOpen $woThisMonth $woThisWeek";
	}
?>