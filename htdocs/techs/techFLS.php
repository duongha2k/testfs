<?php $page = 'techs'; ?>
<?php $option = 'more'; ?>
<?php
	require_once("../headerStartSession.php");
	require_once("../library/FSSession.php");
	FSSession::validateSessionParams(array("TechID" => "TechID"));
?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->


<br /><br />
<script type="text/javascript">

$(document).ready(function(){
	$("body").css("width", "850px").css("min-width", "850px");
	
});

</script>

<style type="text/css">
	#content {
		width: 600px;
		margin: auto;
	}
	#mainContainer{
	min-width: 800px;
	width: 850px;
}
</style>

<div id="content">
<h3>FLS (First Line Support)</h3>
<p><b>The FLS Program/Tech Opportunity</b></p>
<p>
The FLS program/Tech opportunity is focused around running service & project calls performing work on hardware and software in the retail and grocery environments for one of our clients. Getting involved with FLS as a Technician requires some technical knowledge, as you will be replacing defective parts dealing with primarily POS equipment in retail stores (keyboards, check readers, monitors, cash registers/drawers, scanners, etc).
 </p>
 <p>
In order to be activated as a Tech with FLS (First Line Support), you will first need to go through the required online training and submit the test with a passing score. Get more information regarding the FLS program and/or begin online training here: <a href="Training/FLS/flsCertTest.php">Click here for FLS Training Process</a>
</p>
<br />

<div id="cb193b0000308e3fbd7bb5470d9ec5"><a href="http://www.caspio.com" target="_blank">Online Database</a> by Caspio</div>
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b0000308e3fbd7bb5470d9ec5","https:");}catch(v_e){;}</script>
<div id="cxkg"><a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000308e3fbd7bb5470d9ec5">Click here</a> to load this Caspio <a href="http://www.caspio.com" title="Online Database">Online Database</a>.</div>
</div>


	


<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
