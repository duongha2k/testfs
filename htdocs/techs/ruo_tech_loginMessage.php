<?php
ini_set("display_errors", 1);
//Set logged in session
setcookie("techLoginMessageViewed", "yes", time()+60*60*24*365*10, "/", ".fieldsolutions.com");
?>
<?php $page = 'none'; ?>
<?php $option = 'none'; ?>
<?php $displayHome = 'no'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>


<!-- Add Content Here -->

<br /><br />

<!-- Add Content Here -->
<style>
p.textContent{margin:0px;}
</style>
<br /><br />

<div align="center">

<table border="0" cellpadding="0" cellspacing="0">
	<tr>
<td width="700" align="left">
<h1 style="font-size:18px">Welcome to the Roll Us Out - ITC Service<br />
  Management Web Site!</h1>


<h2 style="font-size:14px;">What's New:</h2>
<p class="textContent">
First is our new look and the easy to use Navigation with fewer clicks from log-on  and throughout the work order process
</p>
<p class="textContent">
<b>The New Roll Us Out Work Order</b>
</p>
<p class="textContent">
-New navigation - Assigned, Available & Applied work on top, easy to find and go to<br />
-New work order flow with 6 sections in the sequence of work so it is easier to understand what's next<br />
-More structured content - phone numbers, dates and times now in standard formats for better accuracy<br />
</p>
<p class="textContent">
<b>New Dashboards</b>
<br />
-Real time visibility to the status of your work orders<br />
-Look up and find work orders faster<br />
-Sort work orders in any sequence for better tracking and visibility<br />
</p>

<p class="textContent">
<h2 style="font-size:14px;">What's The Same:</h2>
<br />
-Your login and password from www.FieldSolutions.com may be used here<br />
-All the same data in work orders and in your profile<br />
</p>


<p align="center" style="font-size:14px;" class="textContent">
<a href="../../../techs/dashboard.php">Continue</a>
</p>
</td>
	</tr>
</table>

</div>


<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
