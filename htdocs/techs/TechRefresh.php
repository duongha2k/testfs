<?php
/**
 * @author Sergey Petkevich (Warecorp)
 */
?>
<?php $page = 'techs'; ?>
<?php $option = 'push2tech'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php require ("../library/googleMapAPI.php"); ?>

<script type="text/javascript" src="../library/jquery/jquery.autotab.js"></script>
<script type="text/javascript">
	function onReady() {
		initialize();
		$("#EditRecordZipCode").change(mapShowAddress);
		onPointReturned = saveGeocode;
		document.getElementById("map_canvas").style.display = "none";
		loadPhoneNumbers();
		$("#PrimaryAreaCode").autotab({ target: "PrimaryExch", format: "numeric" });
		$("#PrimaryExch").autotab({ target: "PrimaryLastFour", format: "numeric", previous: "PrimaryAreaCode" });
		$("#PrimaryLastFour").autotab({ target: "PrimaryExtension", format: "numeric", previous: "PrimaryExch" });
		$("#PrimaryExtension").autotab({ format: "numeric", previous: "PrimaryLastFour" });

		$("#SecondaryAreaCode").autotab({ target: "SecondaryExch", format: "numeric" });
		$("#SecondaryExch").autotab({ target: "SecondaryLastFour", format: "numeric", previous: "SecondaryAreaCode" });
		$("#SecondaryLastFour").autotab({ target: "SecondaryExtension", format: "numeric", previous: "SecondaryExch" });
		$("#SecondaryExtension").autotab({ format: "numeric", previous: "SecondaryLastFour" });

		$("#SmsAreaCode").autotab({ target: "SmsExch", format: "numeric" });
		$("#SmsExch").autotab({ target: "SmsLastFour", format: "numeric", previous: "SmsAreaCode" });
		$("#SmsLastFour").autotab({ format: "numeric", previous: "SmsExch" });

		$("#caspioform").submit(profileSubmit);

		$("#EditRecordAllowText").change(
		    function(objEvent){
                changeAgreeDate();
	        }
        );
	}

	function saveGeocode(point) {
		$("#EditRecordLatitude").attr("value", point.lat());
		$("#EditRecordLongitude").attr("value", point.lng());
	}

	function loadPhoneNumbers() {
		var primary = $("#EditRecordPrimaryPhone").val();
		
		if (primary == undefined) return;
		
		var primaryExt = $("#EditRecordPrimaryPhoneExt").val();
		var secondary = $("#EditRecordSecondaryPhone").val();
		var secondaryExt = $("#EditRecordSecondaryPhoneExt").val();
		var smsNumb = $("#EditRecordSMS_Number").val();

		if (primary != "") {
			var phone = primary.split("-");
			$("#PrimaryAreaCode").val(phone[0]);
			$("#PrimaryExch").val(phone[1]);
			$("#PrimaryLastFour").val(phone[2]);
			$("#PrimaryExtension").val(primaryExt);
		}
		if (secondary != "") {
			var phone = secondary.split("-");
			$("#SecondaryAreaCode").val(phone[0]);
			$("#SecondaryExch").val(phone[1]);
			$("#SecondaryLastFour").val(phone[2]);
			$("#SecondaryExtension").val(secondaryExt);
		}
		if (smsNumb != "") {
			var phone = smsNumb.split("-");
			$("#SmsAreaCode").val(phone[0]);
			$("#SmsExch").val(phone[1]);
			$("#SmsLastFour").val(phone[2]);
		}
	}

	function profileSubmit() {
		var areaCode = $("#PrimaryAreaCode").val();
		var exch = $("#PrimaryExch").val();
		var lastFour = $("#PrimaryLastFour").val();
		if (areaCode != "" && exch != "" && lastFour != "") {
			if (areaCode.length != 3 || exch.length != 3 || lastFour.length != 4) {
				alert("Make sure your primary phone number is in this format: ###-###-####");
				return false;
			}		
			$("#EditRecordPrimaryPhone").val(areaCode + "-" + exch + "-" + lastFour);
		}
		else {
			alert("Please enter a Primary Phone Number");
			$("#PrimaryAreaCode").focus();
			return false;
		}
		areaCode = $("#SecondaryAreaCode").val();
		exch = $("#SecondaryExch").val();
		lastFour = $("#SecondaryLastFour").val();
		if (areaCode != "" && exch != "" && lastFour != "") {
			if (areaCode.length != 3 || exch.length != 3 || lastFour.length != 4) {
				alert("Make sure your secondary phone number is in this format: ###-###-####");
				return false;
			}
			$("#EditRecordSecondaryPhone").val(areaCode + "-" + exch + "-" + lastFour);
		}
		else {
			if (areaCode != "" || exch != "" || lastFour != "") {
				alert("Please fill in all secondary phone fields");
				return false;
			}
			else
				$("#EditRecordSecondaryPhone").val("");
		}

		areaCode = $("#SmsAreaCode").val();
		exch = $("#SmsExch").val();
		lastFour = $("#SmsLastFour").val();
		if (areaCode != "" && exch != "" && lastFour != "") {
			if (areaCode.length != 3 || exch.length != 3 || lastFour.length != 4) {
				alert("Make sure your SMS phone number is in this format: ###-###-####");
				return false;
			}
			$("#EditRecordSMS_Number").val(areaCode + "-" + exch + "-" + lastFour);
		}
		else {
			if (areaCode != "" || exch != "" || lastFour != "") {
				alert("Please fill in all phone number to receive SMS fields");
				return false;
			}
			else
				$("#EditRecordSMS_Number").val("");
		}

		$("#EditRecordPrimaryPhoneExt").val($("#PrimaryExtension").val());
		$("#EditRecordSecondaryPhoneExt").val($("#SecondaryExtension").val());

        if ($("#EditRecordPrimPhoneType").val() == "")
			$("#EditRecordPrimPhoneType").val("Office");
		if ($("#SecondaryLastFour").val() != "" && $("#EditRecordSecondPhoneType").val() == "")
			$("#EditRecordSecondPhoneType").val("Office");
		
		if ($("#EditRecordAllowText").attr("checked") && $("#EditRecordSMS_Number").val() == "") {
			alert("Please enter a SMS phone number");
			return false;
		}
		if ($("#EditRecordAllowText").attr("checked") && $("#EditRecordCellProvider").val() == "") {
			alert("Please select a cell provider");
			return false;
		}
		// end validation
	}

function termsPopUp()
{
    termsWindow=window.open('sms_terms.php','Terms','height=300,width=400');
	termsWindow.focus();
}

function changeAgreeDate()
{
    $("#EditRecordSMS_AgreeDate").val('<?php echo date('m/d/Y H:i:s');?>');
}
</script>
<?php
	googleMapAPIJS("onReady");
?>
<div id="map_canvas"></div>
<div align="center">
<table border="0" cellpadding="0" cellspacing="5" width="770">
	<tr valign="top">
<td>
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b000058d788f13cd14d5e910a","https:");}catch(v_e){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b000058d788f13cd14d5e910a">here</a> to load this Caspio <a href="http://www.caspio.com" title="Online Database">Online Database</a> app.</div>
</td>
</tr>
</table>
</div>
<?php require ("../footer.php"); ?>
