<?php $page = 'techs'; ?>
<?php $option = 'tech'; ?>
<?php require ("../header.php"); ?>
<?php
require ("../navBar.php");
require_once("../library/mySQL.php");
require_once("../headerStartSession.php");
require_once dirname(__FILE__) . '/../../includes/modules/common.init.php';

$search = new Core_Api_TechClass;
$counters = array();
$techID= $_SESSION['TechID'];
$isDisplayMySetting = Core_Api_TechClass::hasOneOfTags_DeVry_Ergo_Purple_LMS_Endeavor_andMore($techID);

$authData = array('login' => $_SESSION["UserName"], 'password' => $_SESSION["UserPassword"]);

$user = new Core_Api_TechUser();
$user->checkAuthentication($authData);

$search->getWorkOrdersCountByStatus($_SESSION["UserName"], $_SESSION["UserPassword"], $counters);

$common = new Core_Api_CommonClass;
$statesOptions = $common->getStatesArray('US');
$countryOptions = $common->getCountries();

$statesHtml = "<option value=\"\">All States</option>";
foreach ($statesOptions->data as $code => $name) {
    $statesHtml .= "<option value=\"$code\">$name</option>";
}
//041
$countriesHtml = "<option value=\"\">Show All</option>";
foreach ($countryOptions->data as $country) {
    $disabled ="";
    if($country->Name=="---South America" || $country->Name=="---EMEA" || $country->Name=="---APAC")
    {
        $disabled ="disabled";
    }
    $countriesHtml .= "<option value=\"$country->Code\" $disabled>$country->Name</option>";
}
//end 041
$bankStates = mysqlFetchAssoc(mysqlQuery("SELECT State, Abbreviation FROM states_list WHERE Country = 'US'"));
$bankStatesHTML = "<option value=\"\">All States</option>";
$i = 1;
if (is_array($bankStates)) {
    foreach ($bankStates as $state) {
        $bankStatesHTML .= "<option value=\"$i\">{$state['State']}</option>";
        $i++;
    }
}
$bankCountries = mysqlQuery("SELECT id, Name FROM Countries ORDER BY DisplayPriority, Name");
$bankCountriesHTML = "<option value=\"\" >Show All</option>";
while ($country = mysql_fetch_assoc($bankCountries)) {
    $bankCountriesHTML .= "<option value=\"{$country['id']}\">{$country['Name']}</option>";
}

$db = Core_Database::getInstance();
$select = $db->select();
$select->from("cell_carriers", array("id", "carrier"))
        ->order('carrier ASC');
$result = Core_Database::fetchAll($select);

$cellProviderHtml = "<option value=\"\">Select One</option>";
foreach ($result as $r) {
    $carrier = $r['carrier'];
    $id = $r['id'];
    $cellProviderHtml .= "<option value =\"$id\" >$carrier</option>";
}
?>
<?php
$pointsSummary = Core_FSPlusOne::getTechPointSummary($_SESSION['TechID']);
if (!$pointsSummary) {
    $pointsSummary['allPoints'] = "0";
    $pointsSummary['currentPoints'] = "0";
}
$isEligible = Core_FSPlusOne::isEligible($_SESSION['TechID']);
$core = new Core_TechLanguages();
$languegaSkill = $core->getTechLanguages($_SESSION['TechID']);

// update
$Core_Tech = new Core_Tech();
$Core_Tech->Reset_AdjustableBedExpert_Tag($_SESSION['TechID']);

//13624
$FSTagClass = new Core_Api_FSTagClass();
$FSTagList = $FSTagClass->getFSTagList_forTech($_SESSION['TechID']);
?>
<style type="text/css">@import url(../library/ui.datepicker.css);</style>
<script type="text/javascript" src="../library/ui.datepicker.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.validate.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.curvycorners.js"></script>
<link rel="stylesheet" type="text/css" href="/widgets/js/star-rating/jquery.rating.css" />
<script type="text/javascript" src="/widgets/js/jquery.tinysort.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.easing.pack.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-mod.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.jqEasyCharCounter.js"></script>
<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
<script type="text/javascript" src="/widgets/js/functions.js"></script>
<link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" media="all" />
<script type="text/javascript" src="js/copier_skills_assessment.js"></script> 
<script type="text/javascript" src="/widgets/js/GXEMCCert.js"></script> 
<script type="text/javascript" src="/library/jquery/jquery.bt.min.js"></script>
<script type="text/javascript" src="js/updateprofile.js"></script>
<script type="text/javascript" src="js/BlackBox.js"></script>
<style>
	.telephony table tr td{vertical-align: top}
</style>
<script type="text/javascript">
    var techRegValidator = "";
    var _updateprofile = new updateprofile.CreateObject({
       id:"_updateprofile",
       techid:<?= $_SESSION['TechID'] ?>
    });
    var _blackbox = new BlackBox.CreateObject({
        id:"_blackbox",
        techid:<?= $_SESSION['TechID'] ?>
    });
    //931
    function HasMultiTagLevels_onchange(name)
    {
        $("#"+name+"Input").val($.datepicker.formatDate('mm/dd/yy', new Date()));
        markChangeCred();
    };
     //end 931
    $(document).ready(function(){
     //13735
    $(".expertinfocontrols").unbind('click').bind('click', function(){
                var content = "<div id='comtantPopupID'><div style='text-align:center;height:380px;width:900px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
                $("<div></div>").fancybox(
                {
                    'showCloseButton' :true,
                    'width' : 800,
                    'height' : "auto",
                    'content': content
                }
                ).trigger('click');

                $.ajax({
                type: "POST",
                url: "/widgets/dashboard/popup/expert-list",
                data: "",
                success:function( html ) {
                    var content = html;
                    $("#comtantPopupID").html(content);
                }
            });
        });  
    //End 13735      	
    if(typeof(_ComputerPlusCertification) != 'undefined')
    {
        _ComputerPlusCertification.buildEvent();
    }    
<?php if ($user->getDeactivated() == 1): ?>
            disableLinks($("div.left_col a:not(.allowLinkTD)"));
            $(".editSection a").unbind('click').attr('onclick', '');
            $("#saveBadgePhoto a").unbind('click').attr('onclick', '');
            $(".updateButton").unbind('click').attr('onclick','');
<?php endif; ?>

        showLoader();
        $("#BusinessStatment").jqEasyCounter({
            'maxChars' : 600,
            'maxCharsWarning' : 585,
            'msgFontSize' : '12px',
            'msgTextAlign' : 'left',
            'msgWarningColor' : '#F00',
            'msgAppendMethod' : 'insertBefore'
        });
	
        if($("#acc").length > 0){
            $("#acc dt").click(function(){
                var sel = $(this);

                sel.addClass("act current");

                sel.parent().children("dd").each(function(){
                    if($(this).is(":visible") && !$(this).prev("dt").hasClass("current")){
                        if($(this).prev("dt").hasClass("act")){
                            $(this).prev("dt").removeClass("act");
                        }
                        $(this).slideUp(300);
                    }
                });

                sel.next().slideToggle(300, function(){
                    if(!$(this).is(":visible")){
                        $(this).prev("dt").removeClass("act");
                    }
                    sel.removeClass("current");
                });

                return false;techInfo.W9
            });
        }

        $("#businessProfile").validate({
            invalidHandler: function(e, validator) {
                 $.ajax({
				type: "POST",
				url: "/widgets/dashboard/do/country-require-zip",
                                dataType    : 'json',
                                cache       : false,
                                data: 'countrycode='+jQuery("#Country").children('option:selected').val(),
                                success: function (data) {
                                    if(!data.success)
                                    {
                                        jQuery("#Zipcode").removeClass("required"); 
                                        jQuery("#Zipcode").removeClass("invalid"); 
                                    }
                                    else
                                    {
                                        jQuery("#Zipcode").addClass("required"); 
                                    }    
                var errors = validator.numberOfInvalids();
	
                if (errors) {
                    var message = errors == 1
                        ? 'You missed 1 field. It has been highlighted below'
                    : 'You missed ' + errors + ' fields.  They have been highlighted below';
                    $("div.error span").html(message);
                    $("div.error").show();
                } else {
                    $("div.error").hide();
                }
                                }
                 });
            },
            onkeyup: false,
            submitHandler: function(form) {
                $("div.error").hide();
                showLoader();
                submitBusinessProfile(form);
            },
            rules: {},
            messages: {},
            errorPlacement: function(error,element){
                error.appendTo(element.parent().parent().parent().next().find("div.status"));
            },
            errorClass: "invalid",
            debug:true
        });

        jQuery.validator.addMethod("password", function( value, element ) {
                return true;
        }, "");
			

        jQuery.validator.addMethod("paymentNumCheck", function( value, element ) {
            if ($("#PaymentMethod").val() != 'Direct Deposit') return true;
            changed = $("#RoutingNum").val() != $("body").data("RoutingNumMask") || $("#AccountNum").val() != $("body").data("AccountNumMask");
            return (!isNaN(value) || !changed);
        },"This field must contain numbers only");

        jQuery.validator.addMethod("routingLenCheck", function( value, element ) {
            if ($("#PaymentMethod").val() != 'Direct Deposit') return true;
            if (value.substring(0,4) != "XXXX" && value.length != 9) {
                return false;
            }else{
                return true;
            }
        },"The Routing Number must be 9 digits");

        jQuery.validator.addMethod("emailEqualTo", function( value, element ) {
            if($('body').data('email') == $("#PrimaryEmail").val()){
                return true;
            }else{
                if($("#PrimaryEmail").val() == $("#confPrimaryEmail").val()){
                    return true;
                }else{
                    return false;
                }
            }
		
        }, "Please confirm that the address you entered is the same as above");
	
        jQuery.validator.addMethod("phoneTen", function( val, element ) {
            if ($("#Country").val() != 'US' && $("#Country").val() != 'CA') return true;
            if (val != null && val != "") {
                // massage phone number
                validFormat = /^[(]?(\d{3})[)]?(?:\s|-|.)*(\d{3})[-\s.]*(\d{4})$/;
                parts = val.match(validFormat);
                if (parts != null)
                    $(element).val(parts[1] + "-" + parts[2] + "-" + parts[3]);
                else return false;
            }
            return true;
        }, "Phone number must be ###-###-####, or change selected country");

        jQuery.validator.addMethod("phoneMX", function( val, element ) {
            if ($("#Country").val() != 'MX') return true;
            if (val != null && val != "") {
                // massage phone number
                validFormat = /^\+52 (\d{3} \d{3} \d{4}|\d{2} \d{4} \d{4})$/;
                parts = val.match(validFormat);
                if (parts == null)
                    return false;
            }
            return true;
        }, "Phone number must be +52 ## #### #### or +52 ### ### ####, or change selected country");

        jQuery.validator.addMethod("requireIfFilledTextPhone", function( value, element ) {
            if ($("#SMS_Number").val() != "" && value == "") return false;
            return true;
        }, "Please select a Service Provider");

        jQuery.validator.addMethod("pwEqualTo", function( value, element ) {
            if($('body').data('password') == $("#password").val()){
                return true;
            }else{
                if($("#password").val() == $("#passwordconfirm").val()){
                    return true;
                }else{
                    return false;
                }
            }
		
        }, "Please enter the same password as above.");

        techRegValidator = $("#techRegistration").validate({
            invalidHandler: function(e, validator) {
                 $.ajax({
				type: "POST",
				url: "/widgets/dashboard/do/country-require-zip",
                                dataType    : 'json',
                                cache       : false,
                                data: 'countrycode='+jQuery("#Country").children('option:selected').val(),
                                success: function (data) {
                                    if(!data.success)
                                    {
                                        jQuery("#Zipcode").removeClass("required"); 
                                        jQuery("#Zipcode").removeClass("invalid"); 
                                    }
                                    else
                                    {
                                        jQuery("#Zipcode").addClass("required"); 
                                    }    
                var errors = validator.numberOfInvalids();
		
                if (errors) {
                    var message = errors == 1
                        ? 'You missed 1 field. It has been highlighted below'
                    : 'You missed ' + errors + ' fields.  They have been highlighted below';
                    $("div.error span").html(message);
                    $("div.error").show();
                } else {
                    $("div.error").hide();
                }
                                    }
			});
            },
            onkeyup: false,
            submitHandler: function(form) {
                $("div.error").hide();
                showLoader();
                submitUpdateTechInfo(form);
            },
            rules: {
                PrimaryPhone: {
                    phoneTen: true,
//                    phoneMX: true,
//                    phoneBS: true,
                    remote: {
                        url: "/widgets/dashboard/do/tech-phone-not-taken",
                        type: "post",
                        data: {
                            TechID: $("#basicRegTechID").val()
                        }
                    }
                },
                SecondaryPhone: {
                    phoneTen: true,
//                    phoneMX: true,
//                    phoneBS: true,
                    remote: {
                        url: "/widgets/dashboard/do/tech-phone-not-taken",
                        type: "post",
                        data: {
                            TechID: $("#basicRegTechID").val()
                        }
                    }
                },
                SMS_Number: {
                    phoneTen: true
//                    phoneMX: true,
//                    phoneBS: true
                },
                PrimaryEmail: {
                    email: true
                },
                confPrimaryEmail: {
                    email: true,
                    emailEqualTo: true
                },
                secondaryEmail: {
                    email: true
                },
                Password: {
                    password: true
                },
                passwordconfirm: {
                    pwEqualTo: true
                },
                CellProvider: {
                    requireIfFilledTextPhone: true
                }
            },
            messages: {
                passwordconfirm: {
                    required: " ",
                    pwEqualTo: "Please enter the same password as above"	
                },
                password: {
                    required: ""
                },
                primaryEmail: {
                    email: "Please enter a valid email address",
                    remote: jQuery.validator.format("{0} is already taken, please enter a different address.")	
                },
                confPrimaryEmail:{
                    emailEqualTo: "Please confirm that the address you entered is the same as above"
                },
                PrimaryPhone: {
                    remote: "The phone number is already taken, please enter a different phone number."
                },
                SecondaryPhone: {
                    remote: "The phone number is already taken, please enter a different phone number."
                }

            },
            errorPlacement: function(error,element){
                error.appendTo(element.parent().parent().parent().next().find("div.status"));
            },
            errorClass: "invalid",
            debug:true
        });

        $("#paymentInfo").validate({
            invalidHandler: function(e, validator) {
                 $.ajax({
				type: "POST",
				url: "/widgets/dashboard/do/country-require-zip",
                                dataType    : 'json',
                                cache       : false,
                                data: 'countrycode='+jQuery("#Country").children('option:selected').val(),
                                success: function (data) {
                                    if(!data.success)
                                    {
                                        jQuery("#Zipcode").removeClass("required"); 
                                        jQuery("#Zipcode").removeClass("invalid"); 
                                    }
                                    else
                                    {
                                        jQuery("#Zipcode").addClass("required"); 
                                    }    
                var errors = validator.numberOfInvalids();
		
                if (errors) {
                    var message = errors == 1
                        ? 'You missed 1 field. It has been highlighted below'
                    : 'You missed ' + errors + ' fields.  They have been highlighted below';
                    $("div.error span").html(message);
                    $("div.error").show();
                } else {
                    $("div.error").hide();
                }
                        }
                 });
            },
            onkeyup: false,
            submitHandler: function(form) {
                $("div.error").hide();
                showLoader();
                submitPaymentInfo(form);
            },
            rules: {
                primaryEmail: {
                    email: true
                },
                RoutingNum: {
                    paymentNumCheck: true,
                    routingLenCheck: true
                },
                AccountNum: {
                    paymentNumCheck: true
                }
            },
            messages: {
                primaryEmail: {
                    email: "Please enter a valid email address",
                    remote: jQuery.validator.format("{0} is already taken, please enter a different address.")	
                },
                RoutingNum:{
                    required: ""
                },
                AccountNum: {
                    required: ""
                }
            },
            errorPlacement: function(error,element){
                error.appendTo(element.parent().parent().parent().find("div.status"));
             
                if($(element).attr("name") == "AgreeTerms"){
                    $(element).parent().addClass('invalid');
                }
            },
            success:function(label){
                if(label[0].htmlFor == "AgreeTerms" && $("#paymentAgree").hasClass("invalid")){
                    $("#paymentAgree").removeClass("invalid");
                }	
            },
            errorClass: "invalid",
            debug:true
        });

        function loadTechData(){
            var techID = '<?= $_SESSION['TechID']; ?>';
            $.ajax({
                type: "POST",
                url: '/widgets/dashboard/do/get-tech-info/',
                dataType    : 'json',
                cache       : false,
                data: {id:techID, type:"tech"},
                success: function (data) {
		
                    for(index in data.techInfo){
                        if(data.techInfo[index] == "null" || data.techInfo[index] == null){
                            data.techInfo[index] = "";
                        }
									
                        if(index == "MaskedRoutingNum"){
                            $("body").data("RoutingNumMask", data.techInfo[index]);
                            $("#RoutingNum").val(data.techInfo[index]);
                            $("#RoutingNumLabel").html(data.techInfo[index]);
                        }else if(index == "MaskedAccountNum"){
                            $("body").data("AccountNumMask", data.techInfo[index]);
                            $("#AccountNum").val(data.techInfo[index]);
                            $("#AccountNumLabel").html(data.techInfo[index]);
                        }
                        else if(index == "Country")
                        {
                            //041
                            var cntry = "";
                            var bankCntry = "";
                            for(countries in data.countries)
                            {
                                if(data.countries[countries].Code==data.techInfo[index])
                                {
                                    var cntry = data.countries[countries].Name;
                                    var bankCntry = data.countries[countries].id;
                            }
                            }
                            
                            var Requirecountry = {"US":"US","CA":"CA","MX":"MX"};

                            if(Requirecountry.hasOwnProperty(data.techInfo[index]))
                            {
                                $("#State").removeClass("required"); 
                                $("#State").addClass("required");
                                $("#State").removeAttr("disabled");
                                $("#tdstatelabel").html("<span id='reqStar'>*</span>State/Province:");
                            }
                            else
                            {
                                $("#State").removeClass("required"); 
                                $("#State").removeClass("invalid");
                                $("#State").attr("disabled","disabled");
                                $("#tdstatelabel").html("<span id='reqStar'>&nbsp;</span>State/Province:");
                            }
                            //end 041
                            $.ajax({
                                type: "POST",
                                url: "/techs/ajax/getStates.php",
                                dataType    : 'html',
                                cache       : false,
                                data: {country:bankCntry, type:'country'},
                                success: function (html) {
                                    $("#State").html(html);
                                     if(data.techInfo["State"]){
                                         $("#State").val(data.techInfo["State"]).attr('selected',true);
                                    }
                                    var SeletedTextStatCbx = jQuery("#State").children("opstion:selected").html();
                                    var SeletedValueStatCbx = jQuery("#State").children("opstion:selected").html();
                                    if(jQuery.trim(SeletedValueStatCbx))
                                    {
                                         $("#StateLabel").html(SeletedTextStatCbx);   
                                    }          
                                }
                            });
                            $("#"+index).val(data.techInfo[index]);
                            $("#"+index+"Label").html(cntry);
                        }else if (index == "State"){
                            $("#StateLabel").html(data.techInfo[index]);                 
                        }else if(index == "PrimaryPhone"){
                            detailObject.onInit.RequitePhoneForTech(
                            "#"+index+"Label",
                            data.techInfo[index],
                            data.techInfo['TechID'],
                            data.techInfo['PrimaryPhoneStatus'],
                            1,
                            'html',
                            2,
                            true,
                            "#imgReQuitePhone .imgClassReQuitePhoneTech"
                        );  
                            detailObject.onInit.RequitePhoneForTech(
                            "#"+index,
                            data.techInfo[index],
                            data.techInfo['TechID'],
                            data.techInfo['PrimaryPhoneStatus'],
                            1,
                            'text',
                            2,
                            true,
                            "#imgReQuitePhone .imgClassReQuitePhoneTech"
                        );     
                        }
                        else if(index == "ServRightBrotherStatusClicked")
                        {
                            if( data.techInfo["ServRightBrotherStatusClicked"] == 1)
                            {
                                jQuery("#ServRightBrotherStatusLabel").html("Once you have completed the cert, it will be several days before your profile is tagged. Please be patient.");
                            }
                        }
                       
                        else if(index == "GGE1stAttemptStatus"){
                            if(data.techInfo["GGE1stAttemptStatus"] == "Pass" || data.techInfo["GGE2ndAttemptStatus"] == "Pass" ){
                                var _score = 0;
                                var _imgGGE = "<img  src='/widgets/images/check_green.gif'/>";
                                if(data.techInfo["GGE1stAttemptStatus"] == "Pass") {
                                    _score = data.techInfo["GGE1stAttemptScore"];
                                    jQuery("#GGETechEvaluationDate").html(data.techInfo["GGE1stAttemptDate"]);
                                }
                                else if(data.techInfo["GGE2ndAttemptStatus"] == "Pass") {
                                    _score = data.techInfo["GGE2ndAttemptScore"];
                                    jQuery("#GGETechEvaluationDate").html(data.techInfo["GGE2ndAttemptDate"]);
                                }
                                jQuery("#GGETechEvaluationStatus").html(_imgGGE+"Pass ("+_score+"/20)");
                            } else if(data.techInfo["GGE1stAttemptStatus"] == "Pending" 
                                && (data.techInfo["GGE2ndAttemptStatus"] == null || 
                                data.techInfo["GGE2ndAttemptStatus"] == "" || 
                                data.techInfo["GGE2ndAttemptStatus"] == "N/A"))    {
                                jQuery("#GGETechEvaluationStatusLabel").html("1<sup>st</sup> Attempt - Pending");
                            }else if(data.techInfo["GGE1stAttemptStatus"] == "Fail" 
                                && (data.techInfo["GGE2ndAttemptStatus"] == null || 
                                data.techInfo["GGE2ndAttemptStatus"] == "" || 
                                data.techInfo["GGE2ndAttemptStatus"] == "N/A"))    {
                                jQuery("#GGETechEvaluationStatusLabel").html("Fail ("+data.techInfo["GGE1stAttemptScore"]+"/20) <a href=\"javascript:;\" profile=\"1\" class=\"GGETechEvaluation\">Try Again?</a>");
                            } else if(data.techInfo["GGE2ndAttemptStatus"] == "Pending")
                            {
                                jQuery("#GGETechEvaluationStatusLabel").html("2<sup>nd</sup> Attempt - Pending");
                            }else if(data.techInfo["GGE2ndAttemptStatus"] == "Fail"){
                                jQuery("#GGETechEvaluationStatusLabel").html("Fail ("+data.techInfo["GGE2ndAttemptScore"]+"/20)");
                            }
                            //var _GGETechEvaluation = GGETechEvaluation.CreateObject({
                            //        id:'_GGETechEvaluation' 
                            //        });
                        }
                        else if(index == "ServRightBrotherStatusClicked")
                        {
                            if( data.techInfo["ServRightBrotherStatusClicked"] == 1)
                            {
                                jQuery("#ServRightBrotherStatusLabel").html("Once you have completed the cert, it will be several days before your profile is tagged. Please be patient.");
                            }
                        }
                        else if(index == "SecondaryPhone")
                        {
                            detailObject.onInit.RequitePhoneForTech(
                            "#"+index+"Label",
                            data.techInfo[index],
                            data.techInfo['TechID'],
                            data.techInfo['SecondaryPhoneStatus'],
                            2,
                            'html',
                            2,
                            true,
                            "#imgSecondaryPhoneStatus .imgClassReQuitePhoneTech"
                        );  
                            detailObject.onInit.RequitePhoneForTech(
                            "#"+index,
                            data.techInfo[index],
                            data.techInfo['TechID'],
                            data.techInfo['SecondaryPhoneStatus'],
                            2,
                            'text',
                            2,
                            true,
                            "#imgSecondaryPhoneStatus .imgClassReQuitePhoneTech"
                        );     
                        }
                        else if(index == "CopierSkillsAssessment")
                        {
                            jQuery(".cmdCopiesSkillView").attr("techid",data.techInfo["TechID"]);
                            if(data.techInfo[index] == 1)
                            {    
                                $("#"+index+"Display").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                            }
                            else
                            {
                                var el = jQuery(".cmdCopiesSkillView").parent();
                                var html = jQuery(".cmdCopiesSkillView").html();
                                jQuery(el).html(html);
                            }
                        }  
                        else if(index == "CopierSkillsAssessmentDate")
                        {
                            if(data.techInfo["CopierSkillsAssessment"] == 1)
                            {    
                                $("#"+index+"Display").html(data.techInfo[index]);
                            }
                        }
                        else if(index == "SpecificExperience" || index == "SiteRef" || index == "EquipExperience"){
                            $("#"+index).html(data.techInfo[index]);
                        }
                        else if(index == "EMCCert")
                        {
                var _GXEMCCert = GXEMCCert.CreateObject({id:'_GXEMCCert',today:'<?= date('m/d/Y') ?>'});
                            _GXEMCCert.buildData(data,index); 
                                }
                        else if(index != "State"){
                            $("#"+index).val(data.techInfo[index]);
                            optionTxt = $("#" + index + " option[value='" + data.techInfo[index] + "']").text();
                            if (optionTxt != '')
                                $("#"+index+"Label").html(optionTxt);
                            else
                                $("#"+index+"Label").html(data.techInfo[index]);
                        }
							
                        if(index == "PaymentMethod"){
                            if(data.techInfo[index] == "Direct Deposit"){
                                toggleDDField(true);
                                $("#paymentAgree").show();
                                $("#AgreeTerms").addClass("required").attr('checked', true);
                            }else{
                                toggleDDField(false);
                                $("#paymentAgree").hide();
                                $("#AgreeTerms").removeClass("required").attr('checked', false);
                            }
                        }
                     //13700
                        if(index == "SilverDrugTest_Pass")
                        {   
                            var silverDrugTest_Pass = data.techInfo['SilverDrugTest_Pass'];
                            if(silverDrugTest_Pass.toLowerCase() == "pass")
                            {    
                                $("#SilverDrugStatus").append($("<img />").attr("src","/widgets/images/check_green.gif"));                           
                                if(data.techInfo['DatePassedSilverDrug']!= "" || data.techInfo['DatePassedSilverDrug'] != null)
                                {
                                    var DatePassedSilverDrugpart = data.techInfo['DatePassedSilverDrug'].split(/[- :]/);
                                    $("#SilverDrugsDate").html(DatePassedSilverDrugpart[1]+"/"+DatePassedSilverDrugpart[2]+"/"+DatePassedSilverDrugpart[0]);  
                                }
                            }
                            else
                            {
                             $("#SilverDrugsdate").parent().html("<a class='bgCheck' href='javascript:void(0);' rel='/techs/ajax/lightboxPages/backgroundChecks.php' onclick='orderBGCheck()'><img width='100' height='65' src='/widgets/images/order-dt-check.png'></a>");
                                    $(".bgCheck").click(function(){
                                        $("<div></div>").fancybox(
                                        {
                                            'autoDimensions' : false,
                                            'width'	: 685,
                                            'height' : 470,
                                            'transitionIn': 'none',
                                            'transitionOut' : 'none',
                                            'type': 'iframe',
                                            'href': $(this).attr('rel')
                                        }
                                    ).trigger('click');
                                    });
                            }
                        } 
                      //End 13700 
                        
                    }
                    if(data.techInfo.I9Status == 1)
                    {
                        jQuery("#WorkAuthorizationValue").html("");
                        jQuery("#WorkAuthorizationStatus").children('img').attr("src","/widgets/images/USPending_20x62.png"); 
                        jQuery("#WorkAuthorizationStatus").children('img').css("display",""); 
                    }
                    else if(data.techInfo.I9Status == 2)
                    {
                        jQuery("#WorkAuthorizationValue").html(data.techInfo.I9Date);
                        jQuery("#USAuthorized").html("<a target='_blank' href='/techs/ajax/work_authorization_pdf.php?techid="+data.techInfo.TechID+"'>US Verified</a>");
                        jQuery("#WorkAuthorizationStatus").children('img').css("display",""); 
                        jQuery("#WorkAuthorizationStatus").children('img').attr("src","/widgets/images/USAuthorized_20x62.png"); 
                    }
                    jQuery("#W9check").html(data.techInfo.W9); 
						
                    if(data.techInfo.State != ""){
                        var bankState = "";
                        $("#State option").each(function(){
                            if(data.techInfo.State == $(this).val()){
                                bankState = $(this).html();
                            }
                        });
			
                        $("#BankState option").each(function(){
                            if(bankState == $(this).html()){
                                bankState = $(this).val();
                            }
                        });
                    }else{
                        var bankState = "";
                    }
		
                    if(data.techInfo.SecondaryPhone == "" || data.techInfo.SecondaryPhone == null){
                        $("#SecondaryPhone").removeClass("required");
                    }

                    if(data.techInfo.ISO_Affiliation_ID !== "" &&  data.techInfo.ISO_Affiliation_ID !== null){
                        //change on click function to iso shadowbox message
                         $("#editContactInfoBtn, #editPrimaryAddressBtn, #editLoginInfoBtn, #savePaymentAddressBtn").attr("onclick","");
                         $("#pmtInfoW9").hide();
                        $("#editContactInfoBtn, #editPrimaryAddressBtn, #editLoginInfoBtn, #savePaymentAddressBtn").click(function(){
                        	$("<div></div>").fancybox(
                                    {
                                        'autoDimensions' : false,
                                        'width': 350,
                    					'height' : 125,
                                        'transitionIn': 'none',
                                        'transitionOut' : 'none',
                                        'content' : "<h1 class='error' style='line-height: 15px;'>Edits to Contact and Payment Information are not allowed for ISO Technicians. If you wish to make a change, please contact FieldSolutions Technician Support at <a href='mailto:support@fieldsolutions.com'>support@fieldsolutions.com</a>. Thank You.</h1>"
                                    }
                                ).trigger('click');
                        });
                    }
						
                    $("#headerTechInfo").html(data.techInfo.Firstname+" "+data.techInfo.Lastname+" - Tech ID "+techID);
                    $('body').data("techName",data.techInfo.Firstname+" "+data.techInfo.Lastname);
                    $('body').data("country",bankCntry);
                    $('body').data("address1",data.techInfo.Address1);
                    $('body').data("address2",data.techInfo.Address2);
                    $('body').data("city", data.techInfo.City);
                    $('body').data("state", bankState);			
                    $('body').data("zip", data.techInfo.Zipcode);
                    $('body').data("email",data.techInfo.PrimaryEmail);
                    $('body').data("password",'<?= $_SESSION['UserPassword']; ?>');
						
                    $("#username").val("<?= $_SESSION['UserName']; ?>");
                    $("#usernameLabel").html("<?= $_SESSION['UserName']; ?>");
						
                    $("#password").val("<?= $_SESSION['UserPassword']; ?>");
                    $("#passwordconfirm").val("<?= $_SESSION['UserPassword']; ?>");
                    $("#passwordLabel").html("<?= $_SESSION['UserPassword']; ?>");
                    $("#confPrimaryEmail").val($("#PrimaryEmail").val());

		
                    if(data.techInfo.State){
                        $("#State").val(data.techInfo.State).attr('selected',true);
                    }
		
                    if(data.techInfo.Country){
                        $("#Country").val(data.techInfo.Country).attr('selected',true);
                    }else{
                        $("#Country").val("US").attr('selected',true);
                    }
						
                    $("#yesResume").hide();
                    $("#noResume").show();
                    $("#deleteBadgePhoto").hide();
                    $("#badgeUpload").show();
		
                    if(data.techInfo.techFiles != null){
                        if(data.techInfo.techFiles.length >= 1){
                            $.each(data.techInfo.techFiles, function(key,value){
                                if(value.fileType == "Resume"){
                                    $('body').data("resume", value.filePath);
                                    $("#yesResume").show();
                                    $("#noResume").hide();
                                    $("#resumeFile").append($("<a>"+value.displayName+"<a/>").attr("href", "/widgets/dashboard/do/file-tech-documents-download/filename/"+value.encodedFilename+"/"+value.filePath));
										
                                }
				
                                if(value.fileType == "Profile Pic"){
                                    if(value.approved == "0"){
                                        $("#badgePhotoImage").attr("src","/widgets/images/profileAnonPending.jpg" );
                                        $('body').data("profilePic", value.filePath);
                                    }else{
                                        $("#badgePhotoImage").attr("src","https://s3.amazonaws.com/tech-docs/profile-pics/"+value.filePath );
                                        $('body').data("profilePic", value.filePath);
                                    }
                                    $("#deleteBadgePhoto").show();
                                    $("#badgeUpload").hide();
                                    $("#saveBadgePhoto").hide();
                                }
									
                                if(value.fileType == "Vehicle Image"){
                                    $('body').data("vehicleImage", value.filePath);
                                    $("#vehicleMisc").show();
                                    $("#vehicleID").val(value.id);
                                    $("#vehicleImageLabel").append($("<a>"+value.displayName+"<a/>").attr("href", "/widgets/dashboard/do/file-tech-documents-download/filename/"+value.encodedFilename+"/"+value.filePath));
                                    if(value.description != null && value.description != ""){
                                        $("#vehicleDesc").val(value.description);
                                    }
                                    if(value.filePath != null && value.filePath != ""){
                                        $("#yesVehicleImage").show();
                                    }
                                } 
                            });
                        }
                    }
                    
                    if(data.techInfo.expertiseInfo != null){
                        if(data.techInfo.expertiseInfo.length >= 1){
                            $.each(data.techInfo.expertiseInfo, function(key,value){
                                var starArr = [];
//                                if (value.selfRating == 0 || !value.selfRating)
//                                    value.selfRating = 0;
                                
                                for(i=0; i<6; i++){
                                    if(i == 0)
                                    {
                                        if(!value.selfRating)
                                        {
                                            var $tempInput = $("<span/>").html("None").css("margin-left", "6px").append($('<input type="radio"/>').attr('name',value.TechSelfRatingColumn).val("None"));
                                        }
                                        else if(value.selfRating == 0)
                                        {
                                            var $tempInput = $("<span/>").html("None").css("margin-left", "6px").append($('<input type="radio"/>').attr('name',value.TechSelfRatingColumn).attr('checked','checked').val("None"));
                                        }
                                        else
                                        {
                                            var $tempInput = $("<span/>").html("None").css("margin-left", "6px").append($('<input type="radio"/>').attr('name',value.TechSelfRatingColumn).val("None"));
                                        }
                                    }
                                    else
                                    {
                                    
                                        if(value.selfRating == i)
                                        {
                                            var $tempInput = $("<span/>").html(i).css("margin-left", "6px").append($('<input type="radio"/>').attr('name',value.TechSelfRatingColumn).attr('checked','checked').val(i));
                                        }
                                        else
                                        {
                                            var $tempInput = $("<span/>").html(i).css("margin-left", "6px").append($('<input type="radio"/>').attr('name',value.TechSelfRatingColumn).val(i));
                                        }
                                    }
                                    starArr.push($tempInput);
                                }
									
                                var newCatArr = ["33", "34", "35", "36"];
                                var categoryHtml = "";
										
                                if($.inArray(value.Category_ID, newCatArr) > -1){
                                    categoryHtml = value.Category+ "<img name='New' src='/images/new.gif' alt='New'>";
                                }else{
                                    categoryHtml = value.Category;
                                }
				//13735
                                var starstr = "";
                                if(value.FSExpertEnable=="1")
                                {
                                    if(value.FSExpert==1)
                                    {
                                        starstr = "<img height=\"16px\" bt-xtitle=\"FS-Expert: <b>"+value.Category+"</b>\n\
                                                    <div style='padding-left:15px;padding-top:10px;'>\n\
                                                        <ul>\n\
                                                            <li style='list-style:disc outside none;'>"+value.Content+"</li>\n\
                                                        </ul>\n\
                                                    </div>\" class=\"hoverTextnew\" src=\"/widgets/images/star.png\">";
                                    }
                                    else
                                    {
                                         starstr = "<img height=\"16px\" bt-xtitle=\"FS-Expert: <b>"+value.Category+"</b>\n\
                                                    <div style='padding-left:15px;padding-top:10px;'>\n\
                                                        <ul>\n\
                                                            <li style='list-style:disc outside none;'>"+value.Content+"</li>\n\
                                                        </ul>\n\
                                                    </div>\" class=\"hoverTextnew\" src=\"/widgets/images/disablestar.png\">";                                        
                                    }
                                }
                                					
                                $('#techExpertiseTable').find("tbody")
                                .append($('<tr>')
                                .append($("<td>").html(categoryHtml))
                                .append($("<td>").text(value.total).attr("align","center"))
                                .append($("<td>").append.apply($("<td>"),$.isArray(starArr) ? starArr : [starArr]))
                                .append($("<td>").append(starstr).attr("align","center"))); //13735                             
                            });
                        }
                        $.getScript('/widgets/js/star-rating/jquery.rating.js');
                    }
                     jQuery(".hoverTextnew").bt({
                        titleSelector: "attr('bt-xtitle')",
                        fill: 'white',
                        cssStyles: {color: 'black', width: '300px'},
                        animate:true
                     });
                     //end 13735
                    if(data.techInfo.equipmentInfo != null){
                        if(data.techInfo.equipmentInfo.length >= 1){
                            $.each(data.techInfo.equipmentInfo, function(key,value){
                                $("#"+value).attr("checked","checked");
                                if(value == "Ladder"){
                                    $("#ladderMisc").show();
                                }
                                if(value == "PunchTool"){
                                    $("#punchToolMisc").show();
                                }
                                if(value == "Vehicle"){
                                    $("#vehicleMisc").show();
                                }
                            });
                        }
                    }
		
                    if(data.techInfo.equipmentInfo2 != null){
                        $.each(data.techInfo.equipmentInfo2, function(key,value){
                            $("#Tools").val(value.Tools);
                        });
                    }
		
                    if(data.techInfo.socialNetworks != null){
                        if(data.techInfo.socialNetworks.length >= 1){
                            $.each(data.techInfo.socialNetworks, function(key,value){
                                $("input[name="+value.name+value.id+"]").val(value.URL);
                            });
                        }
                    }
		
                    if(data.techInfo.FLSTestDatePass != null)
                    {
                        $("#flsIdDate").html(data.techInfo.FLSTestDatePass);
                    }
                    if(data.techInfo.credentialInfo != null){
                        if (data.techInfo.credentialInfo['bgCheckFullStatus'] == 1 && data.techInfo.credentialInfo['drugTestStatus'] == 1) {
                            today = new Date();
                            yearago = new Date(today.getFullYear() - 1, today.getMonth(), today.getDate());
                            bgDate = $.datepicker.parseDate("mm/dd/yy",data.techInfo.credentialInfo['bgCheckFullDate']);
                            dtDate = $.datepicker.parseDate("mm/dd/yy",data.techInfo.credentialInfo['drugTestDate']);
                            dellMRACompliantDate = bgDate > dtDate ? bgDate : dtDate;
                            if (bgDate >= yearago || dtDate >= yearago) {
                                imgSrc = "/widgets/images/check_green.gif";
                                dellMRACompliantStatus = "Passed";
                            }
                            else {
                                imgSrc = "/widgets/images/red_cross.gif";
                                dellMRACompliantStatus = "Lapsed";
                                dellMRACompliantDate = new Date(dellMRACompliantDate.getFullYear() + 1, dellMRACompliantDate.getMonth(), dellMRACompliantDate.getDate() + 1);
                            }
                            dellMRACompliantDate = dellMRACompliantDate.getMonth() + 1 + "/" + dellMRACompliantDate.getDate() + "/" + dellMRACompliantDate.getFullYear();
                            $("#DellMRACompliantStatus").html("<img src='" + imgSrc + "'/>" + dellMRACompliantStatus + " - " + dellMRACompliantDate);
                        }
                        dellMRACompliantBreakDown =  "Basic BC: " + (data.techInfo.credentialInfo['bgCheckStatus'] == 1 ? data.techInfo.credentialInfo['bgCheckDate'] : "") + "<br/>";
                        dellMRACompliantBreakDown += "Comp BC : " + (data.techInfo.credentialInfo['bgCheckFullStatus'] == 1 ? data.techInfo.credentialInfo['bgCheckFullDate'] : "") + "<br/>";
                        dellMRACompliantBreakDown += "DT      : " + (data.techInfo.credentialInfo['drugTestStatus'] == 1 ? data.techInfo.credentialInfo['drugTestDate'] : "");
                        $("#DellMRACompliantDate").html(dellMRACompliantBreakDown);

						if (data.techInfo.credentialInfo["fsMobileDate"] && data.techInfo.credentialInfo["fsMobileDate"] != null) {
							$("#fsMobileStatus").html ("<img src='/widgets/images/fs_mobile_36x14.png' />");
						}
                        //13932  14056			
                        if(data.techInfo.credentialInfo["interimGovtStatus"] == 1)
                        {
                            imgSrc = "/widgets/images/check_green.gif";
                            $("#interimGovtStatusLabel").html("<img src='" + imgSrc + "'/>");                  
                            $("#interimGovtDate").html(data.techInfo.credentialInfo['interimGovtDate']);                      
                        }                      
                        else if(data.techInfo.credentialInfo["interimGovtStatus"] == 2)
                        {
                            $("#interimGovtStatusLabel").html('Unverified');
                            $("#interimGovtDate").html(data.techInfo.credentialInfo['interimGovtDate']); 
                        }
                        
                        if(data.techInfo.credentialInfo["topGovtStatus"] == 1)
                        {
                            imgSrc = "/widgets/images/check_green.gif";
                            $("#topGovtStatusLabel").html("<img src='" + imgSrc + "'/>");
                            $("#topGovtDate").html(data.techInfo.credentialInfo['topGovtDate']);                           
                        }                      
                        else if(data.techInfo.credentialInfo["topGovtStatus"] == 2)
                        {
                            $("#topGovtStatusLabel").html('Unverified'); 
                            $("#topGovtDate").html(data.techInfo.credentialInfo['topGovtDate']);  
                        }
                        
                        if(data.techInfo.credentialInfo["fullGovtStatus"] == 1)
                        {
                            imgSrc = "/widgets/images/check_green.gif";
                            $("#fullGovtStatusLabel").html("<img src='" + imgSrc + "'/>");
                            $("#fullGovtDate").html(data.techInfo.credentialInfo['fullGovtDate']);							                                                                
                        }                      
                        else if(data.techInfo.credentialInfo["fullGovtStatus"] == 2)
                        {
                            $("#fullGovtStatusLabel").html('Unverified');
                             $("#fullGovtDate").html(data.techInfo.credentialInfo['fullGovtDate']);	
                        }                                          
                       
                        if(data.techInfo.credentialInfo["topSCIGovtStatus"] == 1)
                        {
                            imgSrc = "/widgets/images/check_green.gif";
                            $("#topSCIGovtStatusLabel").html("<img src='" + imgSrc + "'/>");
                            $("#topSCIGovtDate").html(data.techInfo.credentialInfo['topSCIGovtDate']);							                                                                            
                        }else if(data.techInfo.credentialInfo["topSCIGovtStatus"] == 2)
                        {                          
                            $("#topSCIGovtStatusLabel").html('Unverified');
                            $("#topSCIGovtDate").html(data.techInfo.credentialInfo['topSCIGovtDate']);	
                        }
                        if(data.techInfo.credentialInfo["NACIGovtStatus"] == 1)
                        {
                            imgSrc = "/widgets/images/check_green.gif";
                            $("#NACICertStatus").html("<img src='" + imgSrc + "'/>");
                            $("#NACIDate").html(data.techInfo.credentialInfo['NACIGovtDate']);							                                                                            
                        }else if(data.techInfo.credentialInfo["NACIGovtStatus"] == 2)
                        {                          
                            $("#NACICertStatus").html('Unverified');
                            $("#NACIDate").html(data.techInfo.credentialInfo['NACIGovtDate']);	
                        }
                         // 13932  14056
                        if(data.techInfo.TrapolloMedicalCertifiedStatus==1)
                        {
                            imgSrc = "/widgets/images/check_green.gif";
                            $("#TrapolloMedicalCertifiedStatus").html("<img src='" + imgSrc + "'/>");
                            $("#TrapolloMedicalCertifiedDate").html(data.techInfo.TrapolloMedicalCertifiedDate);
                        }                       
                        //912
                        else
                        {
                            $("#TrapolloMedicalCertifiedStatus").html("");
                            $("#TrapolloMedicalCertifiedDate").html("");
                        }
                        //end 912
                        if(data.techInfo.VonagePlusStatus==1)
                        {
                            imgSrc = "/widgets/images/check_green.gif";
                            $("#VonagePlusStatusLabel").html("<img src='" + imgSrc + "'/>");
                            $("#VonagePlusStatusChk").attr('checked', true);
                            $("#VonagePlusDate").html(data.techInfo.VonagePlusDate);
                            $("#VonagePlusInput").val(data.techInfo.VonagePlusDate);
                        }
                        else
                        {
                            $("#VonagePlusStatusLabel").html("");
                        }     
                        if(data.techInfo.NCRBadgedTechnicianStatus==1)
                        {
                            imgSrc = "/widgets/images/check_green.gif";
                            $("#NCRBadgedTechnicianStatusLabel").html("<img src='" + imgSrc + "'/>");
                            $("#NCRBadgedTechnicianStatusChk").attr('checked', true);
                            $("#NCRBadgedTechnicianDate").html(data.techInfo.NCRBadgedTechnicianDate);
                            $("#NCRBadgedTechnicianInput").val(data.techInfo.NCRBadgedTechnicianDate);
                        }
                        else
                        {
                            $("#NCRBadgedTechnicianStatusLabel").html("");
                        }
                        $.each(data.techInfo.credentialInfo, function(key,value){
                            if(key.search("Status") != -1){
                                var divID = key.replace("Status","");
                                if(divID == "flsId" || divID == "flsWhse"){
                                    $("#"+divID+" td[id*='Status']").html(value);
                                }else if(value == 1){
                                    $("#"+divID+" td[id*='Status']").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                                }else if((key == "bgCheckStatus") && value != 1){
                                    if (key == "bgCheckStatus") p = "bgCheck";
                                    else p = "bgCheckFull";
                                    //$("#" + p + "Date").parent().html("<a class='bgCheck' href='javascript:void(0);' rel='/techs/ajax/lightboxPages/backgroundChecks.php'><img width='100' height='65' src='/widgets/images/order-bg-check.png'></a>");
                                    $("#" + p + "Date").parent().html("<a class='bgCheck' href='javascript:void(0);' rel='/techs/ajax/lightboxPages/backgroundChecks.php' onclick='orderBGCheck()'><img width='100' height='65' src='/widgets/images/order-bg-check.png'></a>");
                                    $(".bgCheck").click(function(){
                                        $("<div></div>").fancybox(
                                        {
                                            'autoDimensions' : false,
                                            'width'	: 685,
                                            'height' : 470,
                                            'transitionIn': 'none',
                                            'transitionOut' : 'none',
                                            'type': 'iframe',
                                            'href': $(this).attr('rel')
                                        }
                                    ).trigger('click');
                                    });
                                }
                                else if((key == "drugTestStatus") && value != 1){
                                    //$("#" + p + "Date").parent().html("<a class='bgCheck' href='javascript:void(0);' rel='/techs/ajax/lightboxPages/backgroundChecks.php'><img width='100' height='65' src='/widgets/images/order-bg-check.png'></a>");
                                    $("#drugTestDate").parent().html("<a class='bgCheck' href='javascript:void(0);' rel='/techs/ajax/lightboxPages/backgroundChecks.php' onclick='orderBGCheck()'><img width='100' height='65' src='/widgets/images/order-dt-check.png'></a>");
                                    $(".bgCheck").click(function(){
                                        $("<div></div>").fancybox(
                                        {
                                            'autoDimensions' : false,
                                            'width'	: 685,
                                            'height' : 470,
                                            'transitionIn': 'none',
                                            'transitionOut' : 'none',
                                            'type': 'iframe',
                                            'href': $(this).attr('rel')
                                        }
                                    ).trigger('click');
                                    });
                                }
                                else if(key == "w9Status" && value != 1){
                                    $("#w9Date").parent().html("<a class='uploadW9' href='javascript:void(0);' rel='/techs/ajax/lightboxPages/w9.php'><img width='85' height='45' src='/widgets/images/upload-w9.png'></a>");
                                    $(".uploadW9").click(function(){
                                        $("<div></div>").fancybox(
                                        {
                                            'autoDimensions' : false,
                                            'width'	: 900,
                                            'height' : 550,
                                            'transitionIn': 'none',
                                            'transitionOut' : 'none',
                                            'type': 'iframe',
                                            'href': $(this).attr('rel')
                                        }
                                    ).trigger('click');
                                    });
                                }
                            }
		
                            if(key.search("Date") != -1){
                                var divID = key.replace("Date","");
                                if(key == "essintialCertDate"){
                                    $("#"+divID+" span[id*=CertDateDisplay]").html(value);
                                }else{
                                    $("#"+divID+" div[id*="+key+"]").html(value);
                                }
                            }
                        });
                    }
                    if(data.techInfo.FLSstatus=='Trained' && data.techInfo.FLSID == '')
                    {
                        $("#flsIdStatus").html('Passed: You will be issued<br> an FLS Contractor ID<br> in 7-10 days.');
                    }
                    if(data.techInfo.trainingInfo != null){
                        $.each(data.techInfo.trainingInfo, function(key,value){

                            if(key.search("CertStatus") != -1 && value == 1){
                                var divID = key.replace("Status","");
                                if(data.techInfo.trainingInfo[divID+"Num"] != "" && data.techInfo.trainingInfo[divID+"Num"] != null){
                                    $("#"+divID+" span[id*=CertNumDisplay]").html(data.techInfo.trainingInfo[divID+"Num"]);
                                }else{
                                    $("#"+divID+" span[id*=CertNumDisplay]").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                                }

                                if(data.techInfo.trainingInfo[divID+"Date"] != "")
                                    $("#"+divID+" span[id*=CertDateDisplay]").html(data.techInfo.trainingInfo[divID+"Date"]);
                            }
                        });
                    }
		
                    if(data.techInfo.certificationsInfo != null){
                        $.each(data.techInfo.certificationsInfo, function(key,value){
                            if(key=="compTiaCertNum" && value != null && value != "") {
			                    $("#compTiaCertCheckDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));
			                    $("#compTiaCertNumDisplay").append(value);
                            } else  if(key.search("CertNum") != -1){
                                var divID = key.replace("Num","");
                                if(value != null && value != ""){
                                    $("#"+divID+" span[id*=CertNumDisplay]").append(value);
                                }else{
                                    //$("#"+divID+" span[id*=CertNumDisplay]").append($("<img />").attr("src","/widgets/images/check_red.gif"));
                                }
                            }
                            /*
                            if(key.search("CertNum") != -1){
                                var divID = key.replace("CertNum","");
                                $("#"+divID+" div[id*=NumLabel]").html(value);                                
                            }
                            */
                            if(key.search("Date") != ""){
                                var divID = key.replace("Date", "");
                                $("#"+divID+"Cert span[id*=CertDateDisplay]").html(value);
                                $("#"+divID+"Date").html(value);
                                $("#"+divID+"Input").val(value);
                            }
                            if((key == 'aPlusStatus' || key.search("CertStatus") != -1) && value == "1"){
                                var divID = key.replace("CertStatus","");
								if (divID == "") return;
                                if (key == 'aPlusStatus') divID = 'aPlus';
                                $("#"+divID+"CertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                                var checkBoxElement = $("#"+(divID == 'aPlus' ? 'APlus' : divID));
                                if (checkBoxElement.attr('type') == 'checkbox') {
                                    checkBoxElement.attr("checked",true);
                                }
                                $("#"+divID+"StatusLabel").html($("<img />").attr("src","/widgets/images/check_green.gif"));                            
                                $("#"+divID+"CertStatus").attr("checked",true);
                                $("#"+divID+"CertStatusEx1").attr("checked",true);
                            }
                            if(key == "DeVryCertStatus" && value == 1){
                                $("#DeVryCertNumDisplay").html("<img id='devryCert' height='15' src='/widgets/images/DeVry.png'/>");
                            }else if(key=="bicsiStatus" && value==1) {
                                $("#bicsiCertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                            }else if(key=="dellDcseStatus" && value==1) {
                                $("#dellDcseCertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif")); 
                            }else if(key=="ccnaStatus" && value==1) {
                                $("#ccnaCertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif")); 
                            }else if(key=="mcseStatus" && value==1) {
                                $("#mcseCertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));                             
                            }else if(key=="MCSACertStatus" && value==1) {
                                //$("#MCSACertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));                               
                            }else if(key=="MCPCertStatus" && value==1) {
                                //$("#MCPCertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));                             
                            }else if((key=="CompuComTechnicianCertStatus" || key=="CompuComAnalystCertStatus" || key=="CompuComSpecialistCertStatus") && value==1) {
                                var CompuComText = "&nbsp;";
                                if(key == "CompuComTechnicianCertStatus") CompuComText += "Technician";
                                else if(key == "CompuComAnalystCertStatus") CompuComText += "Analyst";
                                else if(key == "CompuComSpecialistCertStatus") CompuComText += "Specialist";
                                $("#CompuComCertifiedStatusLabel").append($("<img />").attr("src","/widgets/images/check_green.gif")).append( CompuComText);
                            }
                            if(key == "BlueRibbonTechnicianCertStatus" && value == 1)
                            {
                                $("#blueRibbonDate").html(data.techInfo.certificationsInfo['BlueRibbonTechnicianDate']);
                                $("#blueRibbonStatus").html(
                                                        $("<img />").attr("src","/widgets/images/BRT_Tag.png")
                                                                    .attr("width","20")
                                                    );
                            }
                            
                            if(key == "TBCCertStatus" && value == 1)
                            {
                                $(".bootCamp").html("Boot Camp Certified");
                                $("#bootCampDate").html(data.techInfo.certificationsInfo['TBCDate']);
                                $("#bootCampStatus").html(
                                                        $("<img />").attr("src","/widgets/images/Boot_Tag.png")
                                                                    .attr("width","18")
                                                    );
                            }
                            else if(key == "BootCampPlusCertStatus" && value == 1)
                            {
                                $(".bootCamp").html("Boot Camp Plus");
                                $("#bootCampDate").html(data.techInfo.certificationsInfo['BootCampPlusDate']);
                                $("#bootCampStatus").html(
                                                        $("<img />").attr("src","/widgets/images/Boot_Tag_PLUS.png")
                                                                    .attr("width","24")
                                                    );
                            }
                            if((key=="CompuComTechnicianDate" || key=="CompuComAnalystDate" || key=="CompuComSpecialistDate"))
                            {
                                jQuery("#CompuComCertifiedDate").html(value);
                            }

                            if (key == "TechForceICAgreementDate") {
	                        	$("#TechForceCredentialDate").text (value);
	                        }

                            if (key == "TechForceICAgreementCertStatus" && value == 1) {
		                        $("#TechForceCredentialStatusLabel").append($("<img />").attr("src","/widgets/images/check_green.gif"));
	                        }
                            //13624
                            //056
//                            if (key == "NACICertStatus" && value == 1) {
//                                $("#NACICertStatus").append($("<img />").attr("src","/widgets/images/check_green.gif"));
//                            }
                            //end 056
                            
                        });
                    }
                    $(".showOnEdit").hide();
                    hideLoader();
                    detectChange();
                    openBasicReg();
                    _updateprofile.buildEvent(data);
                    _blackbox.buildEvent();
                
                  
                }
            });
        }

        function openBasicReg(){
            var queryString = '<?= $_REQUEST['data'] ?>';
            if(queryString == "showPaymentInfo"){
                $("#paymentInfoSection").trigger('click');
            }
            else if(queryString == "showbasicRegistration"){
                $("#basicRegistration").trigger('click');
            }
            else if(queryString == "showBusinessProfileSection"){
                $("#BusinessProfileSection").trigger('click');
            }
            else if(queryString == "showExpertiseSection"){
                $("#ExpertiseSection").trigger('click');
            }
            else if(queryString == "showToolsResourcesSection"){
                $("#ToolsResourcesSection").trigger('click');
            }
            else if(queryString == "showCredentialsSection"){
                $("#CredentialsSection").trigger('click');
            }
            else{

                $("#basicRegistration").trigger('click');
            }
        }

        $(".certForms").click(function(){
            if($(this).attr('rel') == "insurance"){
                var messageHtml = "<h2>FieldSolutions Insurance Coverage</h2>";
                messageHtml += "<p>FieldSolutions insures our clients against risk of work on-site. As such, we never put you through the hassle of producing your insurance overages, we don't verify whether your payments are current; and most important we donÃ¢â‚¬â„¢t charge you a fee like other systems for coverages. Insurance coverage is just one reason FieldSolutions is the most preferred place to get technology field work. Remind your clients that you want to get your work and paid through FieldSolutions.</p>";

                $("<div></div>").fancybox(
                {
                    'autoDimensions' : false,
                    'width'	: 420,
                    'height' : 225,
                    'transitionIn': 'none',
                    'transitionOut' : 'none',
                    'content' : messageHtml
                }
            ).trigger('click');
	
            }else{
		
                $("<div></div>").fancybox(
                {
                    'autoDimensions' : false,
                    'width'	: 900,
                    'height' : 850,
                    'transitionIn': 'none',
                    'transitionOut' : 'none',
                    'type': 'iframe',
                    'href': $(this).attr('rel')
                }
            ).trigger('click');
            }
	
        });
        $(".blueRibbon").click(function(){
            var messageHtml = "<h2 style='text-decoration:underline'><center>Blue Ribbon Techs</center></h2>";
            messageHtml += "<p style='padding:0px'>FieldSolutions has partnered with Blue Ribbon Techs to redefine the way techs, clients, and platforms maintain and access Technician Background Checks, Drug Tests, and other credentials. To this end, we have moved our background check and drug test credentials to Blue Ribbon Techs so that FieldSolutions&rsquo; techs can get the benefit of this great new way to save money and get qualified for work faster.</p>";
            messageHtml += "<p style='padding:0px'>BRT&rsquo;s guiding principle is that technicians should not have to repeatedly spend time and money for the same credentials time and time again. Instead, you can focus on what matters to you &mdash; getting work, making money, and growing your business.</p>";
            messageHtml += "<p style='padding:0px'>Register at <a href='http://www.BlueRibbonTechs.com' target='_blank'>www.BlueRibbonTechs.com</a> today, and don&rsquo;t forget to add FieldSolutions as an authorized client so FieldSolutions&rsquo; clients can see your credentials! If you&rsquo;ve already registered with BRT and are not seeing the Blue Ribbon Techs &lsquo;Tag&rsquo;, log into BRT and make sure FieldSolutions is an authorized client.</p>";
            messageHtml += "<p style='padding:0px'>For more information check our FAQ or contact <a href='mailto:support@FieldSolutions.com'>support@FieldSolutions.com</a> or <a href='mailto:support@blueribbontechs.com'>support@blueribbontechs.com</a>.</p>";

            $("<div></div>").fancybox({
                'autoDimensions' : false,
                'width'	: 640,
                'height' : 300,
                'transitionIn': 'none',
                'transitionOut' : 'none',
                'content' : messageHtml
            }).trigger('click');
        });
        
        $(".bootCamp").click(function(){
            var messageHtml = "<h2 style='text-decoration:underline'><center>Technician Boot Camp Certification</center></h2>";
            messageHtml += "<p style='padding:0px'>FieldSolutions has partnered with Blue Ribbon Techs to redefine the way techs, clients, and platforms maintain and access Technician Background Checks, Drug Tests, and other credentials. To this end, we have moved our background check and drug test credentials to Blue Ribbon Techs so that FieldSolutions&rsquo; techs can get the benefit of this great new way to save money and get qualified for work faster.</p>";
            messageHtml += "<p style='padding:0px'>Only FieldSolutions has arranged exclusive early access to Blue Ribbon Techs&rsquo; Boot Camp Certification focusing on the soft skills needed to succeed as a field service technician. We believe that this certificate will soon be required by many FieldSolutions clients. As an added bonus, this industry-standard certification is available FREE to FieldSolutions techs through the end of 2012</p>";
            messageHtml += "<p style='padding:0px'>To get started, register at <a href='http://www.BlueRibbonTechs.com' target='_blank'>www.BlueRibbonTechs.com</a> and begin the Technician Boot Camp Certification Process. Don&rsquo;t forget to add FieldSolutions as an authorized platform so clients can see your credentials!</p>";
            messageHtml += "<p style='padding:0px'>For more information check our FAQ or contact <a href='mailto:support@FieldSolutions.com'>support@FieldSolutions.com</a> or <a href='mailto:support@blueribbontechs.com'>support@blueribbontechs.com</a>.</p>";

            $("<div></div>").fancybox({
                'autoDimensions' : false,
                'width'	: 640,
                'height' : 300,
                'transitionIn': 'none',
                'transitionOut' : 'none',
                'content' : messageHtml
            }).trigger('click');
        });
        $("#Vehicle").bind("click",function(){
		
            if($(this).attr("checked") == true){
                $("#vehicleMisc").show();
            }else{
                $("#vehicleMisc").hide();
            }
        });

        $("#Ladder").bind("click",function(){
		
            if($(this).attr("checked") == true){
                $("#ladderMisc").show();
            }else{
                $("#ladderMisc").hide();
            }
        });

        $("#PunchTool").bind("click",function(){
		
            if($(this).attr("checked") == true){
                $("#punchToolMisc").show();
            }else{
                $("#punchToolMisc").hide();
            }
        });

        $("#PaymentMethod").change(function(){
            if($(this).val() == "Direct Deposit"){
                toggleDDField(true);
                $("#paymentAgree").show();
                $("#AgreeTerms").addClass("required");
            }else{
                toggleDDField(false);
                $("#paymentAgree").hide();
                $("#AgreeTerms").removeClass("required");
            }
        });

        $("#Country, #BankCountry").change(function(){
            var type = "";
            if($(this).attr("id") == "Country"){
                type = "country";
            }else{
                type = "bank";
            }
            var country = $(this).val();
            //041
            if(country=="")
            {
                $("#Country option").each(function() {
                    if ($(this).text() == 'Show All')
                    {
                        $(this).attr("selected", "selected");
                    }
                });
                return false;
            }            
            var Requirecountry = {"US":"US","CA":"CA","MX":"MX"};

            if(Requirecountry.hasOwnProperty($("#Country option:selected").val()))
            {
                $("#State").removeClass("required"); 
                $("#State").addClass("required");
                $("#State").removeAttr("disabled");
                $("#tdstatelabel").html("<span id='reqStar'>*</span>State/Province:");
            }
            else
            {
                $("#State").removeClass("required"); 
                $("#State").removeClass("invalid");
                $("#State").attr("disabled","disabled");
                $("#tdstatelabel").html("<span id='reqStar'>&nbsp;</span>State/Province:");

            }
            //end 041
            
            
            $.ajax({
                type: "POST",
                url: "/techs/ajax/getStates.php",
                dataType    : 'html',
                cache       : false,
                data: {country:country, type:type},
                success: function (data) {
                    if(type == "country"){
                        $("#State").html(data);
                    }else{
                        $("#BankState").html(data);
                    }
                }
            });
        });

        loadTechData();

    });//end document.ready

    function submitBusinessProfile(form){
        if($("#deleteResume").attr("checked") == true){
            deleteTechFile("resume");
        }

        if($("#resumeUpload").val() != ""){

            $.ajaxFileUpload({
                url             : "/techs/ajax/saveBusinessProfile.php?techID="+$("#BizProfTechID").val(),
                secureuri       : false,
                fileElementId   : 'resumeUpload',
                dataType        : 'json',
                cache           : false,
                success         : function (data, status, xhr) {
                    var submitData = $(form).serialize();

                    $("#yesResume").show();
                    //$("#noResume").hide();					
                    $("#resumeFile").append($("<a>"+data.uploadResult.displayName+"<a/>").attr("href", "/widgets/dashboard/do/file-tech-documents-download/filename/"+data.uploadResult.encodedFilename+"/"+data.uploadResult.fileName));
					
                    $("#resumeUpload").val("");
                    $.ajax({
                        type: "POST",
                        url: "/techs/ajax/saveBusinessProfile.php",
                        dataType    : 'json',
                        cache       : false,
                        data: submitData,
                        success: function (data) {
                            showSaveSuccessMsg();
                        }
                    });
                },
                error:function(data, status, xhr){
                    $("#yesResume").hide();
                    //$("#noResume").show();
                    showSaveErrorMsg();
                }
            });

        }else{
            var submitData = $(form).serialize();
		  	
            $.ajax({
                type: "POST",
                url: "/techs/ajax/saveBusinessProfile.php",
                dataType    : 'json',
                cache       : false,
                data: submitData,
                success: function (data) {
                    showSaveSuccessMsg();
                }
            });
        }
    }

    function submitPaymentInfo(form){
        var tmpArr = $(form).serialize().split("&");
        var submitArr = {};
        $.each(tmpArr, function(key,value){
            e = value.split("=");
            submitArr[e[0]] = e[1];
        });
        if($('input[name="RoutingNum"]').val().search("X") != "-1"){
            //$(form)[0]['RoutingNum'] = null;
            delete submitArr['RoutingNum'];
        }
        if($('input[name="AccountNum"]').val().search("X") != "-1"){
            //$(form)[0]['AccountNum'] = null;
            delete submitArr['AccountNum'];
        }
		
        var submitString = "";
        for(var e in submitArr){
            submitString += e + "=" + escape(submitArr[e])+"&";
        }
        $.ajax({
            type: "POST",
            url: "/techs/ajax/submitPaymentInfo.php",
            dataType    : 'json',
            cache       : false,
            data: submitString,
            success: function (data) {					
                if ($("#RoutingNum").val().length > 0) {
                    repeat = $("#RoutingNum").val().length - 4;
                    mask = "";
                    for (i=0; i < repeat; ++i) {
                        mask += "X";
                    }
                    mask = mask + $("#RoutingNum").val().substr(repeat);
                    $("#RoutingNum").val(mask);
                    $("body").data("RoutingNumMask", mask)
                    repeat = $("#AccountNum").val().length - 4;
                    mask = "";
                    for (i=0; i < repeat; ++i) {
                        mask += "X";
                    }
                    mask = mask + $("#AccountNum").val().substr(repeat);
                    $("#AccountNum").val(mask);
                    $("body").data("AccountNumMask", mask)
                }
                showSaveSuccessMsg();
            },
            error: function (data){
                showSaveErrorMsg();
            }
        });
    }

    function submitUpdateTechInfo(form){
        if($("#badgePhotoUpload").val() != ""){
			
            $.ajaxFileUpload({
                url             : "/techs/ajax/saveBusinessProfile.php?techID="+$("#basicRegTechID").val(),
                secureuri       : false,
                fileElementId   : 'badgePhotoUpload',
                dataType        : 'json',
                cache           : false,
                success         : function (data, status, xhr) {
                    var submitData = $(form).serialize();
                    $("#badgeUpload").hide();
                    $("#saveBadgePhoto").hide();
                    $("#deleteBadgePhoto").show();
                    $("#badgePhotoImage").attr("src","https://s3.amazonaws.com/tech-docs/profile-pics/"+data.uploadResult.fileName);					
                    $("#badgePhotoUpload").val("");
					
                    $.ajax({
                        type: "POST",
                        url: "/techs/ajax/saveBusinessProfile.php",
                        dataType    : 'json',
                        cache       : false,
                        data: submitData,
                        success: function (data) {
                            showSaveSuccessMsg();
                        }
                    });
                },
                error:function(data, status, xhr){
                    $("#badgeUpload").show();
                    $("#deleteBadgePhoto").hide();
                    $("#saveBadgePhoto").show();
                    showSaveErrorMsg();
                }
            });

        }
        var submitData = "";
        if (editingSection['basicRegistration'])
            submitData += $(form).serialize();
        if (editingSection['cred']) {
            submitData += getSectionForm('publicCredentials');
            submitData += getSectionForm('clientCredentials');
            submitData += getSectionForm('programCertifications');
            submitData += getSectionForm('myCertifications');
        }
		
        //check if name changed and w9 == 1 
        var w9val = $("#W9check").text();
        var techfname = $("#FirstnameLabel").text();
        var techlname = $("#LastnameLabel").text();
        var showw9remind = 0;
        
        if((($("#Firstname").val() != techfname) || ($("#Lastname").val() != techlname)) && w9val == 1)
        {
            showw9remind = 1;
            submitData += "&w9=0";	
        }
        
        showLoader();
        $.ajax({
            type: "POST",
            url: "/techs/ajax/updateTechInfo.php",
            dataType    : 'json',
            cache       : false,
            data: submitData,
            success: function (data)
            {
                //showSaveSuccessMsg();
                showSaveSuccessMsg_new(showw9remind);
//                if(showw9remind==1)
//                    showW9remider();

            }
        });
    }

    function toggleDDField(flag) {
        if (flag) {
            $(".ddField").show();
            $(".ddField input, .ddField select").addClass('required');
        }
        else {
            $(".ddField").hide();
            $(".ddField input, .ddField select").removeClass('required');
        }
        if ($("#PaymentMethod").val() == "Check")
            $(".PayCheckMsg").show();
        else
            $(".PayCheckMsg").hide();
        $("#BankAddress2").removeClass('required');
    }


    function editBasicRegistration(){
        $("#techRegistration div[id*=Label], #techRegistration div[class='editSection']").each(function(){
            if($(this).attr("id").search("username") == -1)$(this).hide();
                
        });

        $("#techRegistration div[id*=Input], #techRegistration div[class='cancelSaveSection']").each(function(){
            if($(this).attr("id").search("username") == -1)$(this).show();
                
        });

        $(".showOnEdit").show();
        $("#badgeUpload").show();
    }

    function saveBasicRegistration(){
        if($('body').data('email') == $("#PrimaryEmail").val()){
            $("#PrimaryEmail").removeClass("required");
            $("#confPrimaryEmail").removeClass("required");
        }


        if($('body').data('password') == $("#password").val()){
            $("#password").removeClass("required");
            $("#passwordconfirm").removeClass("required");
        }

	

        $("#techRegistration").submit();
    }

    function cancelBasicRegistration(){
        $("#techRegistration div[id*=Label], #techRegistration div[class='editSection']").each(function(){
            $(this).show();
        });
        $("#techRegistration div[id*=Input], #techRegistration div[class='cancelSaveSection'], #techRegistration div[class='status']").each(function(){
            $(this).hide();
        });
        $(".showOnEdit").hide();
        $("#badgeUpload").hide();
        techRegValidator.hideErrors();
        $(".error").hide();
    }

    function savePaymentInfo(){
        $("#paymentInfo").submit();
    }

    function saveBusinessProfile(){
        $("#businessProfile").submit();
    }


    var loaderVisible = false;
    function showLoader(){
        if(!loaderVisible){
            $("div#loader").fadeIn("fast");
            loaderVisible = true;
        }
    }

    function hideLoader(){
        if(loaderVisible){
            var loader = $("div#loader");
            loader.stop();
            loader.fadeOut("fast");
            loaderVisible = false;
        }
    }

    function deleteTechFile(type){
        showLoader();
        var file = "";
        if(type == "resume"){
            file = $('body').data("resume");
        }else if(type == "vehicleImage"){
            file = $('body').data("vehicleImage");
        }
	
        $.ajax({
            type: "POST",
            url: '/techs/ajax/deleteTechFile.php',
            dataType    : 'json',
            cache       : false,
            data: {file:file, type: type},
            success: function (data) {
                if(type == "resume"){
                    $("#yesResume").hide();
                    $("#resumeFile").html("");
                }
            },
            error:function(data, status, xhr){
                if(type == "resume"){
                    $("#yesResume").show();
                    //$("#noResume").hide();
                }
            }
        });
    }

    function deleteBadgePhoto(){
        showLoader();
        $.ajax({
            type: "POST",
            url: '/techs/ajax/deleteTechFile.php',
            dataType    : 'json',
            cache       : false,
            data: {file:$('body').data("profilePic"), type: "profile pic"},
            success: function (data) {
                $("#badgePhotoImage").attr("src","/widgets/images/profileAnon.jpg" );
                $("#badgeUpload").show();
                $("#deleteBadgePhoto").hide();
                $("#saveBadgePhoto").show();
                window.location.href=window.location.href;
            },
            error:function(data, status, xhr){
                $("#badgeUpload").hide();
                $("#deleteBadgePhoto").show();
                $("#saveBadgePhoto").hide();
            }
        });
    }

    function setPaymentInfo(){
	
        if($("#paymentAddressSame").attr("checked") == true){
            $("#paymentInfo, #BankCountry").val($('body').data("country"));
            $("#paymentInfo, #BankAddress1").val($('body').data("address1"));
            $("#paymentInfo, #BankAddress2").val($('body').data("address2"));
            $("#paymentInfo, #BankCity").val($('body').data("city"));
            $("#paymentInfo, #BankState").val($('body').data("state"));
            $("#paymentInfo, #BankZip").val($('body').data("zip"));
        }else{
            $("#paymentInfo, #BankCountry").val("");
            $("#paymentInfo, #BankAddress1").val("");
            $("#paymentInfo, #BankAddress2").val("");
            $("#paymentInfo, #BankCity").val("");
            $("#paymentInfo, #BankState").val("");
            $("#paymentInfo, #BankZip").val("");
        }
    }

    function applyFancybox(message){
        $("<div></div>").fancybox({
            'width': 350,
            'height' : 125,
            'content' : message,
            'autoDimensions' : false,
            'centerOnScroll' : true
        }).trigger('click');
    }
    
    function applyFancyboxnew(message){
        $("<div></div>").fancybox({
            'width': 380,
            'height' : 150,
            'content' : message,
            'autoDimensions' : false,
            'centerOnScroll' : true
        }).trigger('click');
    }
    function saveBadgePhoto(){
	
        if($("#badgePhotoUpload").val() != ""){
		
            $.ajaxFileUpload({
                url             : "/techs/ajax/saveBusinessProfile.php?techID="+$("#basicRegTechID").val(),
                secureuri       : false,
                fileElementId   : 'badgePhotoUpload',
                dataType        : 'json',
                cache           : false,
                success         : function (data, status, xhr) {
				
                    $("#badgeUpload").hide();
                    $("#saveBadgePhoto").hide();
                    $("#deleteBadgePhoto").show();
                    $("#badgePhotoImage").attr("src","/widgets/images/profileAnonPending.jpg" );					
                    $("#badgePhotoUpload").val("");
                    showSaveSuccessMsg();
                    window.location.href=window.location.href;
                },
                error:function(data, status, xhr){
                    showSaveErrorMsg();
                    $("#badgeUpload").show();
                    $("#deleteBadgePhoto").hide();
                    $("#saveBadgePhoto").show();
                }
            });

        }
    }

    var numSaveExpected = 0;
    var numSaveReturned = 0;
    var saving = false;
    function showSaveSuccessMsg() {
        if (!saving) return;
        ++numSaveReturned;
        if (numSaveExpected != numSaveReturned) return;
        numSaveExpected = 0;
        numSaveReturned = 0;
        var messageHtml = "<h2>Update Complete</h2>";
        messageHtml += "<p>Your Profile has been successfully updated.</p>";
        messageHtml += "<b>Thank you!</b>";
			
        hideLoader();
        applyFancybox(messageHtml);	
        save = false;
	
        location.reload();
    }
function showSaveSuccessMsg_new(showreminder) {
        if (!saving) return;
        ++numSaveReturned;
        if (numSaveExpected != numSaveReturned) return;
        numSaveExpected = 0;
        numSaveReturned = 0;
        var messageHtml = "";
        if (showreminder == 1)
            messageHtml += "<p>Please remember to update your W-9 in Section 2: Payment Information.</p>";

        var messageHtml1 = "<h2>Update Complete</h2>";
        if (showreminder == 1)
            messageHtml1 += "<p></p>";
        messageHtml1 += "<p>Your Profile has been successfully updated.</p>";
        messageHtml1 += "<b>Thank you!</b>";		
        
        hideLoader();
        if (showreminder == 1)
        {
            applyFancybox(messageHtml);
            setTimeout(function() {
                applyFancybox(messageHtml1);
                save = false;
                location.reload();        
            },2000);
        }
        else
        {
            applyFancybox(messageHtml1);
            save = false;
            location.reload();
        }
    }


    function showSaveErrorMsg() {
        if (!saving) return;
        numSaveExpected = 0;
        numSaveReturned = 0;
        var messageHtml = "<h2>Update Error</h2>";
        messageHtml += "<p>We encounted an error when attempting to update your profile.</p>";
        messageHtml += "<p>Please check the data you entered and try again.</p>";
        messageHtml += "<b>Thank you!</b>";
        saving = false;
				
        hideLoader();
    }

    var editingSection = new Array();
    editingSection['basicRegistration'] = false;
    editingSection['paymentInfo'] = false;
    editingSection['businessProfile'] = false;
    editingSection['equipment'] = false;
    editingSection['expertise'] = false;
    editingSection['cred'] = false;

    function markChangeBasicRegistration() {
        editingSection['basicRegistration'] = true;
    }

    function markChangePaymentInfo() {
        editingSection['paymentInfo'] = true;
    }

    function markChangeBusinessProfile() {
        editingSection['businessProfile'] = true;
    }

    function markChangeEquipment() {
        editingSection['equipment'] = true;
    }

    function markChangeExpertise() {
        editingSection['expertise'] = true;
    }

    function markChangeCred() {
        editingSection['cred'] = true;
    }

    function detectChange() {
        $("#techRegistration input, #techRegistration textarea, #techRegistration select").change(markChangeBasicRegistration);
        $("#paymentInfo input, #paymentInfo textarea, #paymentInfo select").change(markChangePaymentInfo);
        $("#businessProfile input, #businessProfile textarea, #businessProfile select").change(markChangeBusinessProfile);
	
        $("input[name='basicEquipment[]']").change(markChangeEquipment);
        $("input[name='cablingEquipment[]']").change(markChangeEquipment);
        $("input[name='telephonySkills[]']").change(markChangeEquipment);
        $("#deleteVehicleImage").change(markChangeEquipment);
        $("#vehicleImage").change(markChangeEquipment);
        $('#techExpertiseTable input').change(markChangeExpertise);
	
        $("#publicCredentials input").change(markChangeCred);
        $("#clientCredentials input").change(markChangeCred);
        $("#programCertifications input").change(markChangeCred);
        $("#myCertifications input").change(markChangeCred);
    }

    function checkpasswordrule()
    {
        var _errors = _global.validPasword(
                    jQuery("#usernameLabel").html(),
                    jQuery("#password").val(),
                    jQuery("#passwordconfirm").val(),
                    jQuery("#Firstname").val(),
                    jQuery("#Lastname").val());
        jQuery.validator.addMethod("password", function( value, element ) {
            if($('body').data('password') == $("#password").val()){
                return true;
            }else{
                if(_errors["error"]){    
                    return false;
                }
                return true;
            }
        }, _errors["msg"][0]);
    }
    function saveAll(tableID) {
        var _errors = _global.validPasword(
                    jQuery("#usernameLabel").html(),
                    jQuery("#password").val(),
                    jQuery("#passwordconfirm").val(),
                    jQuery("#Firstname").val(),
                    jQuery("#Lastname").val());
        jQuery.validator.addMethod("password", function( value, element ) {
            if($('body').data('password') == $("#password").val()){
                return true;
            }else{
                if(_errors["error"]){    
                    return false;
                }
                return true;
            }
        }, _errors["msg"][0]);
        if(jQuery.trim(jQuery("#FirstnameLabel").html()) != jQuery.trim(jQuery("#Firstname").val())
                    || jQuery.trim(jQuery("#LastnameLabel").html()) != jQuery.trim(jQuery("#Lastname").val())){
            var content = "<div style='padding:5px;width:600px;'>Please contact FieldSolutions Technician Support at 952-288-2500 (ext.1) to change your name.<br><br> Thank you,<br>Your FieldSolutions Team</div>";
            $("<div></div>").fancybox(
            {
                'autoDimensions' : true,
                'showCloseButton' :true,
                'hideOnOverlayClick' : false,
                'enableEscapeButton':false,
                'content': content
            }
            ).trigger('click');    
            return false;    
        }
        saving = true;
         $.ajax({
                type: "POST",
                url: "/widgets/dashboard/do/country-require-zip",
                dataType    : 'json',
                cache       : false,
                data: 'countrycode='+jQuery("#Country").children('option:selected').val(),
                success: function (data) {
                    //931
                    if(data.success)
                    {
                        jQuery("#Zipcode").removeClass("required"); 
                        jQuery("#Zipcode").removeClass("invalid"); 
                    }
                    else
                    {
                        jQuery("#Zipcode").addClass("required"); 
                    }
                    //end 931
        if (editingSection['basicRegistration'] && !$("#techRegistration").valid())
            return;
        if (editingSection['paymentInfo'] && !$("#paymentInfo").valid()) return;
        numSaveExpected = 0;
        numSaveReturned = 0;
        if (editingSection['basicRegistration']) {
            submitUpdateTechInfo($("#techRegistration"));
            showLoader();
            ++numSaveExpected;
        }
        if (editingSection['cred']) {
            if(tableID='myCertifications')
            {
/*                if(jQuery.trim($("#mcseCertNumInput").val())=='') return;
                if($("#ccnaCertNumInput").val()=='') return;
                if($("#compTiaCertNumInput").val()=='') return;
                if($("#dellDcseCertNumInput").val()=='') return;
                if($("#bicsiCertNumInput").val()=='') return;*/

                submitUpdateTechInfo($("#techRegistration"));
                showLoader();
                ++numSaveExpected;
            }
            else
            {
                submitUpdateTechInfo($("#techRegistration"));
                showLoader();
                ++numSaveExpected;
            }
        }
        
        if (editingSection['paymentInfo']) {
            submitPaymentInfo($("#paymentInfo"));
            showLoader();
            ++numSaveExpected;
        }
        if(editingSection['LanguageSkills']){
            saveExpertise();
            showLoader();
            ++numSaveExpected;
        }
        if (editingSection['businessProfile']) {
            submitBusinessProfile($("#businessProfile"));
            showLoader();
            ++numSaveExpected;
        }
        if (editingSection['expertise']) {
            saveExpertise();
            showLoader();
            ++numSaveExpected;
        }
        if (editingSection['equipment']) {
            saveEquipment();
            showLoader();
            ++numSaveExpected;
        }
    }
        });

    }

    function saveEquipment(){
        showLoader();
        var sendData = [];
        var telephonyData = [];
        $("input[name='basicEquipment[]']").each(function(){
            if($(this).attr("checked") == true){
                sendData.push({column:this.value,value:"1"});
            }else{
                sendData.push({column:this.value,value:"0"});
            }
        });

        $("input[name='cablingEquipment[]']").each(function(){
            if($(this).attr("checked") == true){
                sendData.push({column:this.value,value: this.value,checked:"1"});
            }else{
                sendData.push({column:this.value,value: this.value,checked:"0"});
            }
        });

        $("input[name='telephonySkills[]']").each(function(){
            if($(this).attr("checked") == true){
                telephonyData.push({column:this.value,value: this.value,checked:"1"});
            }else{
                telephonyData.push({column:this.value,value: this.value,checked:"0"});
            }
        });
	
        if($("#deleteVehicleImage").attr("checked") == true){
            deleteTechFile("vehicleImage");
            /*
                $.ajax({
                        type: "POST",
                        url: '/techs/ajax/deleteTechFile.php',
                dataType    : 'json',
                cache       : false,
                        data: {file:$("#vehicleCurrentImage").val(), type: "profile pic"},
                        success: function (data) {
				
                         },
                    error:function(data, status, xhr){
				
                         }
                });
             */
        }
	
        if($("#vehicleImage").val() != ""){
		
            $.ajaxFileUpload({
                url             : "/techs/ajax/saveEquipment.php?techID="+$("#basicRegTechID").val()+"&vehicleDesc="+$("#vehicleDesc").val(),
                secureuri       : false,
                fileElementId   : 'vehicleImage',
                dataType        : 'json',
                cache           : false,
                success         : function (data, status, xhr) {
			
                    $.ajax({
                        type: "POST",
                        url: '/techs/ajax/saveEquipment.php',
                        dataType    : 'json',
                        cache       : false,
                        data: {data:JSON.stringify(sendData), 
                            techID: $("#basicRegTechID").val(), 
                            otherTools: $("#Tools").val(), 
                            telephonyData: JSON.stringify(telephonyData)
                        },
                        success: function (data) {
                            showSaveSuccessMsg();
                        },
                        error:function(data, status, xhr){
                            showSaveErrorMsg();
                        }
                    });
                },
                error:function(data, status, xhr){
                    showSaveErrorMsg();
                }
            });

        }else{

            $.ajax({
                type: "POST",
                url: '/techs/ajax/saveEquipment.php',
                dataType    : 'json',
                cache       : false,
                data: {data:JSON.stringify(sendData), 
                    techID: $("#basicRegTechID").val(), 
                    otherTools: $("#Tools").val(),
                    telephonyData: JSON.stringify(telephonyData)
                },
                success: function (data) {
                    showSaveSuccessMsg();
                },
                error:function(data, status, xhr){
                    showSaveErrorMsg();
                }
            });
        }
    }

    function editSection(tableID){

        if(tableID == "myCertifications")
        {
             //jQuery("#EMCCertDisplay .EMCCertDisplay").hide();
             jQuery("#EMCCertDateDisplay .EMCCertDateDisplay").hide();
             jQuery("#EMCCertDateDisplay .EMCCertDateInput").show();
             jQuery("#EMCCertDisplayCbx").show();
             jQuery("#EMCCertDisplay .EMCCertDisplayNew").show();
        }
        
        //912
        if(tableID != "programCertifications" && tableID != "myCertifications" &&  tableID != "clientCredentials" ){
	//end 912
            $.each($("#"+tableID+" input[id*='Input']"),function(){
                if($(this).attr("id") != "essintialCertNumInput" && $(this).attr("id") != "essintialCertDateInput" ){
			
                    var divID = $(this).attr("id").replace("Input","");
                    var dateVal = $("#"+tableID+" div[id*="+divID+"Date]").html();

                    $(this).css("display","table-cell").calendar({dateFormat:'MDY/'}).val(dateVal);
                }
			
            });
            $("#"+tableID+" div[id*='Date']").hide();
            
        }else if(tableID == "programCertifications"){
            $.each($("#"+tableID+" tr"),function(){
                var divID = $(this).attr("id");
                var certVal = $("#"+divID+"NumDisplay").html();
                if(isNaN(certVal)){
                    certVal = "";
                }
			
                $("#"+divID+"NumDisplay").hide();
                $("#"+divID+"NumInput").val(certVal).css("display","table-cell");

            });
        }
        else if(tableID != "clientCredentials"){
            $.each($("#"+tableID+" tr[id*='Cert']"),function(){
				
                if($(this).attr("id") != "aPlusCert"){
                        var divID = $(this).attr("id");
                    var certVal = $("#"+divID+"NumDisplay").html();
                    if(certVal !=null && certVal.search("<img") != "-1"){
                        certVal = $("#"+divID+"NumDisplay").contents().filter (
                        	function() {
                        		return this.nodeType === Node.TEXT_NODE;
                    }
                        ).text();
                    }
		
                    if ($("#"+divID+"NumInput").length > 0) {
                        $("#"+divID+"NumDisplay").hide();
                        $("#"+divID+"NumInput").val(certVal).css("display","inline");
                    }
	
                    var dateVal = $("#"+divID+"DateDisplay").html();
                    if ($("#"+divID+"DateInput").length > 0) {
                        $("#"+divID+"DateDisplay").hide();
                        $("#"+divID+"DateInput").css("display","table-cell").calendar({dateFormat:'MDY/'}).val(dateVal);
                    }
                    
                    $("#"+divID+"Status").css("text-align","left");
                    $("#APlus").show();
                    $("#aPlusCertNumDisplay").hide();
                    
                }else{
                    
                }

            });
        }
        //912
        if(tableID == "clientCredentials")
        {
            var certid ="";

            $.each($("#"+tableID+" tr[id*='cert']"),function()
            {
                certid = $(this).attr("id").replace("cert","");//+"StatusLabel";
                if($("#"+certid+"StatusLabel").attr("isAllowEditCert")==1)
                {
                    $("#"+certid+"StatusLabel").css("display","none");
                    $("#"+certid+"Date").css("display","none");
                    $("#"+certid+"Input").css("display","");
                    
                    var dateVal = $("#"+certid+"Date").html();

                    $("#"+certid+"Input").css("display","table-cell").calendar({dateFormat:'MDY/'}).val(dateVal);
                    
                    $("#"+certid+"chk").css("display","");
                    //931
                    $("#cbx"+certid).css("display","");
                    //end 931
                }
                
            })
        }
        //end 912
        $("#"+tableID+"Header div[class='editSection']").each(function(){
            $(this).hide();
        });

        $("#"+tableID+"Header div[class='cancelSaveSection']").each(function(){
            $(this).show();
        });
        //13932 14056
        if(tableID == "publicCredentials")
        {                                  
             jQuery("#interimGovtDate").show();
             jQuery("#fullGovtDate").show();         
             jQuery("#topGovtDate").show(); 
             jQuery("#NACIDate").show();
			 jQuery("#topSCIGovtDate").show();  	
        }//13932
        
    }

    function getSectionForm(tableID) {
        var form = $("#"+tableID+" form");

        var submitData = $(form).serialize();

        if($("#APlus").attr("checked") == false){
            submitData += "&APlus=0";	
        }
	
        return submitData;
    }

    function saveSection(tableID){

        showLoader();
	
        $.ajax({
            type: "POST",
            url: "/techs/ajax/updateTechInfo.php",
            dataType    : 'json',
            cache       : false,
            data: submitData,
            success: function (data) {
                showSaveSuccessMsg();
            }
        });
    }

    function cancelSection(tableID){
    
        if(tableID == "myCertifications")
        {
             jQuery("#EMCCertDisplay .EMCCertDisplay").show();
             jQuery("#EMCCertDateDisplay .EMCCertDateDisplay").show();
             jQuery("#EMCCertDateDisplay .EMCCertDateInput").hide();
             jQuery("#EMCCertDisplay .EMCCertDisplayNew").hide();
             jQuery("#EMCCertDisplayCbx").hide();
//             jQuery('#EMCCertDisplay .EMCCertDisplayNew').each(function() {
//                 jQuery(this).remove();
//             });
//             jQuery('#EMCCertDisplay .EMCCertDateInputNew').each(function() {
//                 jQuery(this).remove();
//             });
        }
        //912
        if(tableID == "clientCredentials")
        {
            var certid ="";
            $.each($("#"+tableID+" tr[id*='cert']"),function()
            {
                certid = $(this).attr("id").replace("cert","");
                $("#"+certid+"StatusLabel").css("display","");
                $("#"+certid+"Date").css("display","");
                $("#"+certid+"Input").css("display","none");
                $("#"+certid+"chk").css("display","none");
                //931
                $("#cbx"+certid).css("display","none");
                //end 931
            })
        }
        //end 912
        
        if(tableID != "programCertifications" && tableID != "myCertifications"){
            $.each($("#"+tableID+" input[id*='Input']"),function(){
                if($(this).attr("id") != "essintialCertNumInput" && $(this).attr("id") != "essintialCertDateInput"){
                    var divID = $(this).attr("id").replace("Input","");
				
                    var dateVal = $(this).val();
				
                    $("#"+tableID+" div[id*="+divID+"Date]").html(dateVal);
                    $(this).hide();
                }
            });
		
            $("#"+tableID+" div[id*='Date']").show();
		
        }else if(tableID == "programCertifications"){
            $.each($("#"+tableID+" tr"),function(){
                var divID = $(this).attr("id");
                var certVal = $("#"+divID+"NumInput").val();
			
                if(isNaN(certVal) || certVal == ""){
                    $("#"+divID+"NumDisplay").show();
                }else{
                    $("#"+divID+"NumDisplay").html(certVal);
                }
			
                $("#"+divID+"NumInput").css("display","none");

            });
        }
        else{
            $.each($("#"+tableID+" tr"),function(){
                if($(this).attr("id") != "aPlusCert"){
                    var divID = $(this).attr("id");
				
                    var certVal = $("#"+divID+"NumInput").val();
                    if(isNaN(certVal) || certVal == ""){
                        $("#"+divID+"NumDisplay").show();
                    }else{
                        //$("#"+divID+"NumDisplay").html(certVal);
                        $("#"+divID+"NumDisplay").show();
                    }
	
                    $("#"+divID+"NumInput").css("display","none");
	
                    var dateVal = $("#"+divID+"DateInput").val();
                    //$("#"+divID+"DateDisplay").show().html(dateVal);
                    $("#"+divID+"DateDisplay").show();
                    $("#"+divID+"DateInput").css("display","none");
                    
                    if($(this).attr("id") != "DeVryCert")
                    {
                        $("#"+divID+"Status").css("text-align","left");
                    }
                    $("#aPlusCertNumDisplay").show();
                    $("#APlus").hide();
                }else{
                    if($("#APlus").attr("checked") ==  true){
                        //$("#aPlusCertNumDisplay").html("");
                        //$("#aPlusCertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                    }else{
                        //$("#aPlusCertNumDisplay").html("");
                    }
                    
                }
            });
        }

        $("#"+tableID+"Header div[class='editSection']").each(function(){
            $(this).show();
        });

        $("#"+tableID+"Header div[class='cancelSaveSection']").each(function(){
            $(this).hide();
        });
    }

    function cancelPaymentInfo(){}

    
    
</script>

<style type="text/css">

    .required{
        padding: 0;
        color: #000;
    }
    .error{
        padding: 0;
        line-height: 12px;

    }
    *{
        margin: 0;
        padding: 0;
        border: none;
        text-decoration: none;
        outline: none;
    }
    #wr{
        margin: 20px auto;   
        width: 700px;
        text-align: left;
    }
    #acc dt, #acc dd{
        /*width: 100%;*/
    }
    .sectionHeading{
        cursor: pointer;
        display: block;
        padding: 3px 10px;
        background: #E1E1E1;
        color: #000;
        border: solid 1px #666;
        height: 20px;
        font-weight: bold;
        font-size: 14px;
        text-align:left;
    }

    #acc dt.act{
        background: #E1E1E1;
    }

    #acc dd{
        display: none;
        padding: 3px 10px;
    }

    .label{
        text-align: left;
        font-weight:bold;

    }

    td.field input.invalid, td.field select.invalid, tr.errorRow td.field input, tr.errorRow td.field select {
        background-color: #FFFFD5;
        border: 2px solid red;
        color: red;
        margin: 0;
    }
    tr td.field div.formError {
        color: #FF0000;
        display: none;
    }
    tr.errorRow td.field div.formError {
        display: block;
        font-weight: normal;
    }
    .invalid, label.invalid {
        color: red;
    }
    .invalid a {
        color: #336699;
        font-size: 12px;
        text-decoration: underline;
    }
    #section{
        border-right: 1px solid #000;
        border-left: 1px solid #000;
        border-bottom: 1px solid #000;
        
    }
    #reqStar{
        color: rgb(255, 0, 0); 
        font-size: 12px; 
        font-family: Verdana; 
        margin-left: 2px;
    }

    .round {

        border-radius: 3px; 
        -webkit-border-radius: 3px; 
        -moz-border-radius: 3px; 
        -moz-border-radius: 10px 10px 10px 10px;
        -moz-box-shadow: 2px 2px 2px #888888;
        background: url("/widgets/css/images/section_background.gif") repeat-y scroll left top transparent;
        border: 2px solid #DDDDDD;
        color: #FFF;
    } 

    #subDivider{
        width: 670px; 
        height: 25px; 
        background-color: #E1E1E1; 
        border: 1px solid #000;
        font-weight: bold;
        padding-top: 5px;
    }

    table.tablesorter thead tr th, table.tablesorter tfoot tr th {
        background-color: #4790D4;
        color: #FFF;
        border: 1px solid #000;
        font-size: 8pt;
        padding: 4px;
    }
    table.tablesorter{
        border: 1px solid #000;
        width: 600px;
    }
    table.tablesorter tbody td{
        border: 1px solid #000;

    }

    table th{
        text-align: center;
    }

    div#loader{
        display: none;
        width:100px;
        height: 100px;
        position: fixed;
        top: 50%;
        left: 50%;
        background:url(/widgets/images/loader.gif) no-repeat center #fff;
        text-align:center;
        padding:10px;
        font:normal 16px Tahoma, Geneva, sans-serif;
        border:1px solid #666;
        margin-left: -50px;
        margin-top: -50px;
        z-index:2;
        overflow: auto;
    }


    #socialNetworks{
        margin-right: auto; 
        margin-left: auto; 
        text-align: center;
        margin-top: 5px;
    }

    #socialNetworks span{
        margin: 0 13px 0 13px;
    }

    .telephony3Column{
        width: 100%
    }
    table.telephony3Column td{
        width: 33%;
    }

    table.telephony3Column td.indent{
        padding-left: 15px;
    }

    .toolsResources{
        width: 100%;
    }
    .toolsResources table{

    }
    .subSectionCol{
        width: 150px;
    }

    .cancelUpdateButton, .updateButton{
        margin-top: 0px;
        padding: 0px;
    }

    a.title{
        background: url("/widgets/css/images/bullet_toggle_plus.png") no-repeat scroll -4px -9px transparent;
        cursor: pointer;
        display: block;
        float: left;
        height: 20px;
        width: 20px;
        margin-right: 5px;
    }
    dt.act a.title{
        background: url("/widgets/css/images/bullet_toggle_minus.png") no-repeat scroll -4px -9px transparent;
        cursor: pointer;
        display: block;
        /* padding-left: 23px; */
        float: left;
        height: 20px;
        width: 20px;
        margin-right: 5px;
    }

    a.bgCheck{
        display: block;
        height: 35px;
        overflow: hidden;
        width: 100px;
        margin-left:auto;
        margin-right:auto;
    }

    a.bgCheck:hover img{
        margin-top: -33px;
    }

    a.uploadW9{
        display: block;
        height: 24px;
        overflow: hidden;
        width: 85px;
        margin-left:auto;
        margin-right:auto;
    }

    a.uploadW9:hover img{
        margin-top: -20px;
    }

</style>

<div id="loader">
    Loading...
</div>
<div id="dasboardContainer" class="wide_content">
    <?php if (empty($isEligible['errors'])) { ?>
        <div id="FSPlusOne" style="margin-left: 9px;" >
            <form method="POST" name="FSPlusOne" action="https://www.fsplusone.com/index.php?s_id=&u_id=login">
                <input type="hidden" name="FSPLUSONE_ID" value="T<?= $_SESSION['TechID']; ?><?= $_SESSION['UserObj'][0]['LastName']; ?>" />
                <input type="image" width="120" height="22" border="0" title="" alt="" src="/widgets/css/images/FSPlusOne.jpg" id="comingSoon"  style="position: absolute; top: 121px; border: none;" bt-xtitle="Click Button to Register">
                <a target="_blank" href="http://www.fsplusone.com/index.php?s_id=<?= $_SESSION['TechID'] ?>&u_id=">
                </a>
            </form>

            <div id="pointsSummary" style="position: absolute; top: 120px; margin: 0 0 0 130px; color: #6699CC; font-size: 11px;">
                <span id="lifetimePoints" style="float: left; position: absolute; top: 0px; margin: 0;">Lifetime Points: <b><?= $pointsSummary['allPoints'] ?></b></span><br />
                <span id="availablePoints" style="margin: 0px;">Available Points: <b><?= $pointsSummary['currentPoints'] ?></b></span>
            </div>
        </div>
<?php } ?>
<?php include('leftNavContent.php'); ?>

    <div id="wr">
        <dl id="acc">
            <dt id="basicRegistration" class="round sectionHeading">
            <span style="width: 20px; height: 20px;">
                <a class="title" title="Expand Section"></a>
            </span>
            <span>
                1. Account Information: 
            </span>
            <span id="headerTechInfo"></span> 
            </dt>
            <dd id="section">
                <form name="techRegistration" id="techRegistration" method="post">
                    <input type="hidden" name="techID" id="basicRegTechID" value="<?= $_SESSION['TechID']; ?>" />
                    <div class="error" style="display: none;"><span></span></div>
                    <table>
                        <!--  Contact Information -->
                        <tr><td><br /></td></tr>
                        <tr>
                            <td colspan=2>
                                <div class="round" id="subDivider">
                                    <div style="width: 200px; text-align: left; float:left; margin-left: 5px;">
                                        Contact Information: 
                                    </div> 
                                    <div class="cancelSaveSection" style="float:right; display:none;">
                                        <img class="cancelUpdateButton" onclick="cancelBasicRegistration();" title="Cancel" src="/widgets/images/button_x.png">
                                        <img class="updateButton" onclick="saveAll();" title="Save" src="/widgets/images/button_save.png">
                                    </div>
                                    <div class="editSection" style="float:right; padding-right: 5px;"> 
                                        <a href="javascript:void(0);" onclick="editBasicRegistration();" id="editContactInfoBtn" title="Edit">Edit</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table style="margin-left: 50px; margin-right: auto; line-height: 20px;">
                        <tr><td></td></tr>

                        <tr>
                            <td class="label"><span id="reqStar">*</span>First Name: </td>
                            <td class="field">
                                <div id="W9check" style="display:none;"></div>
                                <div id="FirstnameLabel"></div> 
                                <div id="FirstnameInput" style="display:none;">
                                    <input class="required" type="text" size="35" name="FirstName" id="Firstname" />

                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"><span id="reqStar">*</span>Last Name: </td>
                            <td class="field">
                                <div id="LastnameLabel"></div>
                                <div id="LastnameInput" style="display:none;">
                                    <input class="required" type="text" size="35" name="LastName" id="Lastname" /> 

                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"><span id="reqStar">*</span>Primary E-Mail: </td>
                            <td class="field">
                                <div id="PrimaryEmailLabel"></div>
                                <div id="PrimaryEmailInput" style="display:none;">
                                    <input class="required" type="text" size="35" name="PrimaryEmail" id="PrimaryEmail" /> 

                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=2><div class="status"></div></td>
                        </tr>
                        <tr class="showOnEdit">
                            <td class="label"><span id="reqStar">*</span>Confirm Primary E-Mail: </td>
                            <td class="field">
                                <div id="primaryEmailLabel"></div>
                                <div id="primaryEmailInput" style="display:none;">
                                    <input class="required" type="text" size="35" name="confPrimaryEmail" id="confPrimaryEmail" /> 

                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="font-weight: normal;padding-left:10px;">Secondary E-Mail: </td>
                            <td class="field">
                                <div id="SecondaryEmailLabel"></div>
                                <div id="SecondaryEmailInput" style="display:none;">
                                    <input type="text" size="35" name="SecondaryEmail" id="SecondaryEmail" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=2><div class="status"></div></td>
                        </tr>
                        <tr>
                            <td class="label"><span id="reqStar">*</span>Business Hours Telephone: </td>
                            <td class="field">
                                <div id="PrimaryPhoneLabel"></div>
                                <div id="PrimaryPhoneInput" style="display:none;">
                                    <input class="required primaryPhone" type="text" size="35" name="PrimaryPhone" id="PrimaryPhone" /> 

                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=2><div class="status"></div></td>
                        </tr>
                        <tr>
                            <td class="label"><span id="reqStar">*</span>Evening &amp; Weekend Telephone: </td>
                            <td class="field">
                                <div id="SecondaryPhoneLabel"></div>
                                <div id="SecondaryPhoneInput" style="display:none;">
                                    <input class="required" type="text" size="35" name="SecondaryPhone" id="SecondaryPhone" />

                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=2><div class="status"></div></td>
                        </tr>

                        <tr>
                            <td class="label" style="font-weight: normal;padding-left:10px;">Text Notifications: </td>
                            <td class="field">
                                <div id="SMS_NumberLabel"></div>
                                <div id="SMS_NumberInput" style="display:none;">	
                                    <input type="text" size="35" name="SMS_Number" id="SMS_Number" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=2><div class="status"></div></td>
                        </tr>
                        <tr>
                            <td class="label" style="font-weight: normal;padding-left:10px;">Service Provider: </td>
                            <td class="field">
                                <div id="CellProviderLabel"></div>
                                <div id="CellProviderInput" style="display:none;">	
                                    <select id="CellProvider" name="CellProvider">
<?= $cellProviderHtml ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=2 style="width:430px; text-align:center;">Get work available notices! FieldSolutions does not charge for this service. 
                                Standard message and data rates may apply.</td>
                        </tr>
                    </table>
                    <table>
                        <!--  Primary Address -->
                        <tr><td><br /></td></tr>
                        <tr>
                            <td colspan=2>
                                <div class="round" id="subDivider">
                                    <div style="width: 200px; text-align: left; float:left; margin-left: 5px;">Primary Address</div>
                                    <div class="cancelSaveSection" style="float:right; display:none;">
                                        <img class="cancelUpdateButton" onclick="cancelBasicRegistration();" title="Cancel" src="/widgets/images/button_x.png">
                                        <img class="updateButton" onclick="saveAll();" title="Save" src="/widgets/images/button_save.png">
                                    </div>
                                    <div class="editSection" style="float:right; padding-right: 5px;"> 
                                        <a href="javascript:void(0);" onclick="editBasicRegistration();" id="editPrimaryAddressBtn" title="Edit">Edit</a>								</div>
                                </div>
                            </td>
                        </tr>
                    </table>

                    <table style="margin-left: 50px; margin-right: auto; line-height: 20px;">
                        <tr><td><br /></td></tr> 
                        <tr>
                            <td class="label"><span id="reqStar">*</span>Country: </td>
                            <td class="field">
                                <div id="CountryLabel"></div>
                                <div id="CountryInput" style="display:none;">	
                                    <select class="required" id="Country" name="Country" ><?= $countriesHtml ?></select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"><span id="reqStar">*</span>Address Line 1: </td>
                            <td class="field">
                                <div id="Address1Label"></div>
                                <div id="Address1Input" style="display:none;">
                                    <input class="required" type="text" size="35" name="Address1" id="Address1" /> 
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="label" style="font-weight: normal; padding-left:10px;">Address Line 2: </td>
                            <td class="field">
                                <div id="Address2Label"></div>
                                <div id="Address2Input" style="display:none;">
                                    <input  type="text" size="35" name="Address2" id="Address2" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"><span id="reqStar">*</span>City: </td>
                            <td class="field">
                                <div id="CityLabel"></div>
                                <div id="CityInput" style="display:none;">
                                    <input class="required" type="text" size="35" name="City" id="City" /> 
                                </div>
                            </td>
                        </tr>
                        <tr>
                                                                                                                                                                                            <!--041-->
																								<td class="label" id="tdstatelabel"><span id="reqStar">*</span>State/Province: </td>
                                                                                                                                                                                            <!--end 041-->
                            <td class="field">
                                <div id="StateLabel"></div>
                                <div id="StateInput" style="display:none;">
                                    <select class="required" id="State" name="State" ><?= $statesHtml ?></select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"><span id="reqStar">*</span>Postal Code: </td>
                            <td class="field">
                                <div id="ZipcodeLabel"></div>
                                <div id="ZipcodeInput" style="display:none;">
                                    <input class="required" type="text" size="35" name="ZipCode" id="Zipcode" /> 
                                </div>
                            </td>
                        </tr>

                    </table>
                    <div id="divMySetting" style="display:<?=$isDisplayMySetting==1?"":"none"?>;">
                    <?
                    $MessageForgroup="";
                    $isDeVry = 0;
                    $ReceAvaiWorkNotiEmaiAndSMSMess_forGroupWOonly =0;
                    $ViewOnlyGroupWOonMyDash=0;
                    $hasDeVryTag=Core_Api_TechClass::hasDeVryTag($techID);
                    $hasErgoMotionTag=Core_Api_TechClass::hasErgoMotionTag($techID);
                    $hasPurpleTag=Core_Api_TechClass::hasPurpleTag($techID);
                    $hasLMSTag=Core_Api_TechClass::hasLMSTag($techID);
                    $hasEndeavorTag=Core_Api_TechClass::hasEndeavorTag($techID);
                    $TechExts = Core_Api_TechClass::getTechExts($techID);
                    if(!empty($TechExts))
                        {
                            $ReceAvaiWorkNotiEmaiAndSMSMess_forGroupWOonly = $TechExts['ReceAvaiWorkNotiEmaiAndSMSMess_forGroupWOonly'];
                            $ViewOnlyGroupWOonMyDash = $TechExts['ViewOnlyGroupWOonMyDash'];
                        }

                        if ($hasDeVryTag == 1)
                        {
                            $MessageForgroup = " DeVry Internship";
                            $isDeVry = 1;
                        } else
                        {
                            if ($hasErgoMotionTag == 1)
                            {
                                $MessageForgroup .= " ErgoMotion";
                            }
                            if ($hasPurpleTag == 1)
                            {
                                if ($MessageForgroup != "")
                                {
                                    $MessageForgroup .= " and Purple";
                                } else
                                {
                                    $MessageForgroup .= " Purple";
                                }
                            }
                            if ($hasLMSTag == 1)
                            {
                                if ($MessageForgroup != "")
                                {
                                    $MessageForgroup .= " and LMS";
                                } else
                                {
                                    $MessageForgroup .= " LMS";
                                }
                            }
                            if ($hasEndeavorTag == 1)
                            {
                                if ($MessageForgroup != "")
                                {
                                    $MessageForgroup .= " and Endeavor";
                                } else
                                {
                                    $MessageForgroup .= " Endeavor";
                                }
                            }
                        }
                        ?>
                        <table>
                        <!--  My Settings -->
                        <tr>
                            <td>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan=2>
                                <div class="round" id="subDivider">
                                    <div style="width: 200px; text-align: left; float:left; margin-left: 5px;">My Settings</div>
                                    <div class="cancelSaveSection" style="float:right; display:none;">
                                        <img class="cancelUpdateButton" onclick="cancelBasicRegistration();" title="Cancel" src="/widgets/images/button_x.png">
                                        <img class="updateButton" onclick="saveAll();" title="Save" src="/widgets/images/button_save.png">
                                    </div>
                                    <div class="editSection" style="float:right; padding-right: 5px;"> 
                                        <a href="javascript:void(0);" onclick="editBasicRegistration();" id="editMySettingsBtn" title="Edit">Edit</a>								</div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table style="margin-left: 50px; margin-right: auto; line-height: 20px;">
                        <tr>
                            <td>
                                
                            </td>
                        </tr>
                        <tr>
                            <td class="field">
                                <div id="ReceiveMessageForLMSWorkOrderLabel">
                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td style="vertical-align:top;width:15px;">
                                    <input type="checkbox"  disabled="disabled" <?=$isDeVry==1?"checked":($ReceAvaiWorkNotiEmaiAndSMSMess_forGroupWOonly==1?"checked":"")?> />
                                            </td>
                                            <td style="vertical-align:top;line-height:15px;">
                                                Receive Available Work notification emails and SMS text messages for<?=$MessageForgroup;?> Work Orders only
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="ReceiveMessageForLMSWorkOrderInput" style="display:none;">
                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td style="vertical-align:top;width:15px;">
                                    <input type="checkbox" <?=$isDeVry ==1?"disabled='disabled'":""?> name="ReceAvaiWorkNotiEmaiAndSMSMess_forGroupWOonly" id="ReceAvaiWorkNotiEmaiAndSMSMess_forGroupWOonly" <?=$isDeVry==1?"checked":($ReceAvaiWorkNotiEmaiAndSMSMess_forGroupWOonly==1?"checked":"")?> />
                                            </td>
                                            <td style="vertical-align:top;line-height:15px;">
                                                Receive Available Work notification emails and SMS text messages for<?=$MessageForgroup;?> Work Orders only
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="field">
                                <div id="ViewOnlyLMSWorkOrderLabel">
                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td style="vertical-align:top;width:15px;">
                                    <input type="checkbox"  disabled="disabled" <?=$isDeVry==1?"checked":($ViewOnlyGroupWOonMyDash==1?"checked":"")?>/>
                                            </td>
                                            <td style="vertical-align:top;line-height:15px;">
                                                View only<?=$MessageForgroup;?> Work Orders on My 'Dashboard'
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="ViewOnlyLMSWorkOrderInput" style="display:none;">
                                    <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                        <tr>
                                            <td style="vertical-align:top;width:15px;">
                                    <input type="checkbox" <?=$isDeVry ==1?"disabled='disabled'":""?> name="ViewOnlyGroupWOonMyDash" id="ViewOnlyGroupWOonMyDash" <?=$isDeVry==1?"checked":($ViewOnlyGroupWOonMyDash==1?"checked":"")?>/>
                                            </td>
                                            <td style="vertical-align:top;line-height:15px;">
                                                View only<?=$MessageForgroup;?> Work Orders on My 'Dashboard'
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    </div>
                    <table>
                        <!--  Login Information -->
                        <tr><td><br /></td></tr>
                        <tr>
                            <td colspan=2>
                                <div class="round" id="subDivider">
                                    <div style="width: 200px; text-align: left; float:left; margin-left: 5px;">Login Information</div>
                                    <div class="cancelSaveSection" style="float:right; display:none;">
                                        <img class="cancelUpdateButton" onclick="cancelBasicRegistration();" title="Cancel" src="/widgets/images/button_x.png">
                                        <img class="updateButton" onclick="saveAll();" title="Save" src="/widgets/images/button_save.png">
                                    </div>
                                    <div class="editSection" style="float:right; padding-right: 5px;"> 
                                        <a href="javascript:void(0);" onclick="editBasicRegistration();" id="editLoginInfoBtn" title="Edit">Edit</a>								</div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table style="margin-left: 50px; margin-right: auto; line-height: 20px;">
                        <tr><td></td></tr>
                        <tr>
                            <td class="label"><span id="reqStar">*</span>User Name: </td>
                            <td class="field">
                                <div id="usernameLabel"></div>
                                <div id="usernameInput" style="display:none;">
                                    <input class="required" type="text" size="35" name="UserName" id="username" /> 
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="label"><span id="reqStar">*</span>Password: </td>
                            <td class="field">
                                <div id="passwordLabel_">*******</div>
                                <div id="passwordInput" style="display:none;">
                                    <input class="required" type="password" size="35" name="Password" id="password" autocomplete="off" onchange="checkpasswordrule();"/> 
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=2><div class="status"></div></td>
                        </tr>
                        <tr class="showOnEdit">
                            <td class="label">Confirm Password: </td>
                            <td class="field">
                                <div id="passwordConfirmLabel"></div>
                                <div id="passwordConfirmInput" style="display:none;">
                                    <input class="required" type="password" size="35" name="passwordconfirm" id="passwordconfirm" autocomplete="off" />
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table>
                        <!--  Profile Badge Photo -->
                        <tr><td><br /></td></tr>
                        <tr>
                            <td colspan=2 style="padding-bottom: 5px;">
                                <div class="round" id="subDivider">
                                    <div style="width: 200px; text-align: left; float:left; margin-left: 5px;">Profile Photo</div>
                                    <div class="cancelSaveSection" style="float:right; display:none;">
                                        <img class="cancelUpdateButton" onclick="cancelBasicRegistration();" title="Cancel" src="/widgets/images/button_x.png">
                                        <img class="updateButton" onclick="saveAll();" title="Save" src="/widgets/images/button_save.png">
                                    </div>
                                    <div class="editSection" style="float:right; padding-right: 5px;"> 
                                        <a href="javascript:void(0);" onclick="editBasicRegistration();" title="Edit">Edit</a>								</div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table style="margin-left: 50px; margin-right: auto; line-height: 20px;">
                        <tr><td></td></tr>
                        <tr>
                            <td>
                                <div>
                                    <img src="/widgets/images/profileAnon.jpg" style="width: 185px; height:162px;" id="badgePhotoImage" />
                                </div>
                            </td>
                            <td>
                                <div id="deleteBadgePhoto">
                                    <input style="height: 22px;" class="link_button middle2_button" name="deleteBadge" type="button" value="Delete Photo" onclick="javascript:deleteBadgePhoto();" />
                                    <!-- a href="javascript:deleteBadgePhoto();">Delete Photo</a -->
                                </div>
                                <div id="saveBadgePhoto">
                                    <input style="height: 22px;" class="link_button middle2_button" name="saveBadge" type="button" value="Save Photo" onclick="javascript:saveBadgePhoto();" />
                                    <!--  a href="javascript:saveBadgePhoto();">Save Photo</a -->
                                </div>
                            </td>
                        </tr>
                        <tr><td><br /><br /></td></tr>
                        <tr>
                            <td colspan=2 style="text-align: center;">
                                <div id="badgeUpload">
                                    <input type="file" id="badgePhotoUpload" name="badgePhotoUpload" /> 
                                </div>
                            </td>
                        </tr>
                    </table>
                </form>
            </dd>

            <dt class="round sectionHeading" id="paymentInfoSection">
            <span style="width: 20px; height: 20px;">
                <a class="title" title="Expand Section"></a>
            </span>
            <span>
                2. Payment Information 
            </span>
            <span id="headerTechInfo"></span> 
            </dt>
            <dd id="section">
                <form name="paymentInfo" id="paymentInfo" method="post">
                    <input type="hidden" name="techID" value="<?= $_SESSION['TechID']; ?>" /> 
                    <div class="error" style="display: none;"><span></span></div>
                    <table >
                        <!--  Payment Information -->
                        <tr><td><br /></td></tr>
                        <tr>
                            <td colspan=2>
                                <div class="round" id="subDivider">
                                    <div style="width: 180px; text-align: left; float:left; margin-left: 5px;">Payment Address</div>
                                    <div> 
                                        <div class="cancelSaveSection" style="float:right;">
                                            <img class="cancelUpdateButton" onclick="cancelPaymentInfo();" title="Cancel" src="/widgets/images/button_x.png">
                                            <img class="updateButton" onclick="saveAll();" id="savePaymentAddressBtn" title="Save" src="/widgets/images/button_save.png">
                                        </div>

                                        <!--  div class="editSection" style="float:right;"> 
                                                <a href="javascript:void(0);" onclick="editBasicRegistration();" title="Edit">Edit</a>									</div-->
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table style="margin-left: 50px; margin-right: auto; line-height: 20px;">
                        <tr><td></td></tr>
                        <tr>
                            <td class="label"><span id="reqStar">*</span>Select Payment Method: </td>
                            <td class="field">
                                <div id="PaymentMethodInput">
                                    <select class="required" id="PaymentMethod" name="PaymentMethod">
                                        <option value="">Select Payment Method</option>
                                        <option value="Direct Deposit">Direct Deposit</option>
                                        <option value="Check">Check via Mail</option>
                                    </select>
                                    <!--  input type="hidden" value="239" / -->
                                </div>
                            </td>
                        </tr>
                        <!-- 
                        <tr>
                                <td class="label">Account Name: </td>
                                <td class="field">
                                        <div id="AccountNameInput">
                                                <input class="required" type="text" size="35" name="AccountName" id="AccountName" />
                                        </div>
                                </td>
                        </tr>
                        -->
                        <tr id="pmtInfoW9">
                            <td class="label"><span id="reqStar">*</span>W9: </td><td class="field"><input style="height: 22px;" name="w9" class="required certForms link_button middle2_button" type="button" class="link_button middle2_button" value="Enter W9 Online" id="EnterW9Btn" rel="/techs/ajax/lightboxPages/w9.php" /></td>
                        </tr>
                        <tr class="PayCheckMsg">
                            <td colspan="2">Checks are mailed to the primary address listed in your Technician Profile.</td>
                        </tr>
                        <tr class="ddField">
                            <td class="label"><span id="reqStar">*</span>Bank Name: </td>
                            <td class="field">
                                <div id="BankNameInput">
                                    <input class="required" type="text" size="35" name="BankName" id="BankName" />

                                </div>
                            </td>
                        </tr>
                        <tr class="ddField">
                            <td class="label"><span id="reqStar">*</span>Address Line 1: </td>
                            <td class="field">
                                <div id="BankAddress1Input">
                                    <input class="required" type="text" size="35" name="BankAddress1" id="BankAddress1" />
                                </div>
                            </td>
                        </tr>
                        <tr class="ddField">
                            <td class="label" style="font-weight: normal; padding-left:10px;">Address Line 2: </td>
                            <td class="field">
                                <input type="text" size="35" name="BankAddress2" id="BankAddress2" />
                            </td>
                        </tr>
                        <tr class="ddField">
                            <td class="label"><span id="reqStar">*</span>City: </td>
                            <td class="field">
                                <div id="BankCityInput">
                                    <input class="required" type="text" size="35" name="BankCity" id="BankCity" />
                                </div>
                            </td>
                        </tr>
                        <tr class="ddField">
                            <td class="label"><span id="reqStar">*</span>State/Province: </td>
                            <td class="field">
                                <div id="BankStateInput">
                                    <select class="required" id="BankState" name="BankState" ><?= $bankStatesHTML ?></select>
                                </div>
                            </td>
                        </tr>
                        <tr class="ddField">
                            <td class="label"><span id="reqStar">*</span>Postal Code: </td>
                            <td class="field">
                                <div id="BankZipInput">
                                    <input class="required" type="text" size="35" name="BankZip" id="BankZip" />
                                </div>
                            </td>
                        </tr>
                        <tr class="ddField">
                            <td class="label"><span id="reqStar">*</span>Name on Account: </td>
                            <td class="field">
                                <div id="AccountNameInput">
                                    <input class="required" type="text" size="35" name="AccountName" id="AccountName" />

                                </div>
                            </td>
                        </tr>
                        <tr class="ddField">
                            <td class="label"><span id="reqStar">*</span>Routing Number: </td>
                            <td class="field">
                                <div id="RoutingNumInput">
                                    <input type="text" size="35" name="RoutingNum" id="RoutingNum" />

                                </div>
                            </td>
                            <td>
                                <div class="status"></div>
                            </td>
                        </tr>
                        <tr class="ddField">
                            <td class="label"><span id="reqStar">*</span>Account Number: </td>
                            <td class="field">
                                <div id="AccountingNumInput">
                                    <input class="required" type="text" size="35" name="AccountNum" id="AccountNum" />
                                </div>
                            </td>
                            <td>
                                <div class="status"></div>
                            </td>
                        </tr>
                        <tr class="ddField">
                            <td class="label"><span id="reqStar">*</span>Account Type: </td>
                            <td class="field">
                                <div id="DepositInput">
                                    <select class="required" id="DepositType" name="DepositType">
                                        <option value="">Select Account Type</option>
                                        <option value="Checking">Checking</option>
                                        <option value="Savings">Savings</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr class="ddField"><td></td></tr>
                        <tr class="ddField"><td colspan=2><div style="width: 430px; text-align:left;">I hereby authorize Field Solutions, Inc. to initiate automatic deposits to my account at the financial institution 
                                    named below. Further, I agree not to hold Field Solutions, Inc. responsible for any delay or loss of funds due to incorrect or 
                                    incomplete information supplied by me or by my financial institution or due to an error on the part of my financial institution 
                                    in depositing funds to my account. This agreement will remain in effect until Field Solutions, Inc. receives a written notice of 
                                    cancellation from me or my financial institution, or until I submit a new direct deposit form to Field Solutions, Inc.</div></td></tr>
                        <tr class="ddField">
                            <td class="field">
                                <div id="paymentAgree" style="font-weight:bold;">
                                    <input class="required" type="checkbox" name="AgreeTerms" id="AgreeTerms" value="1" /><label for="AgreeTerms">I Agree</label><span id="reqStar">*</span>
                                </div>
                            </td>

                        </tr>
                    </table>
                </form>
            </dd>
            <dt class="round sectionHeading"  id="BusinessProfileSection">
            <span style="width: 20px; height: 20px;">
                <a class="title" title="Expand Section"></a>
            </span>
            <span>
                3. Business Profile
            </span>
            <span id="headerTechInfo"></span>
            </dt>
            <dd>
                <form name="businessProfile" id="businessProfile" method="post">
                    <input type="hidden" name="techID" id="BizProfTechID" value="<?= $_SESSION['TechID']; ?>" />
                    <table>
                        <!--  Payment Information -->
                        <tr><td><br /></td></tr>
                        <tr>
                            <td colspan=2>
                                <div class="round" id="subDivider">
                                    <div style="width: 300px; text-align: left; float:left; margin-left: 5px;">My Business Statement to Customers</div>
                                    <div class="cancelSaveSection" style="float:right;">
                                        <img class="cancelUpdateButton" onclick="javascript:void(0);" title="Cancel" src="/widgets/images/button_x.png">
                                        <img class="updateButton" onclick="saveAll();" title="Save" src="/widgets/images/button_save.png">
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>

                    <table style="margin-left: 50px; margin-right: auto; line-height: 20px;">
                        <tr><td><br /></td></tr>
                        <tr><td style="width: 350px; text-align:justify;">Tell us more about yourself. Include detailed information about your experience, skills, certifications earned, interest in earning more certifications, and preferred travel area. 
                                Anything that will assist clients in choosing you for assignments.</td></tr>
                        <tr>
                            <td>
                                <textarea rows="5" cols="70" id="SpecificExperience" name="SpecificExperience"></textarea>
                            </td>
                        </tr>
                        <tr><td>Can you provide a contact name and number for recent on-site work?</td></tr>
                        <tr>
                            <td>
                                <textarea rows="5" cols="70" id="SiteRef" name="SiteRef"></textarea>
                            </td>
                        </tr>
                        <tr><td>Equipment Experience</td></tr>
                        <tr>
                            <td>
                                <textarea rows="5" cols="70" id="EquipExperience" name="EquipExperience"></textarea>
                            </td>
                        </tr>
                        <tr><td>My Website<img name="New" src="/images/new.gif" alt="New"></td></tr>
                        <tr>
                            <td>
                                <input id="MyWebSite" name="MyWebSite" style="width:100%;" />
                            </td>
                        </tr>
                    </table>

                    <table>
                        <tr><td><br /></td></tr>
                        <tr>
                            <td colspan=2>
                                <div class="round" id="subDivider" style="">
                                    <div style="width: 200px; text-align: left; float:left; margin-left: 5px;">My Resume</div>
                                    <div class="cancelSaveSection" style="float:right;">
                                        <img class="cancelUpdateButton" onclick="javascript:void(0);" title="Cancel" src="/widgets/images/button_x.png">
                                        <img class="updateButton" onclick="saveAll();" title="Save" src="/widgets/images/button_save.png">
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>

                    <table style="margin-left: 50px; margin-right: auto; line-height: 20px;">
                        <tr>
                            <td>
                                <div style="text-align: center;" >
                                    <span id="resumeFile"></span>																											
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td colspan=2 style="text-align: center;" class="field">
                                <div>
                                    <input type="file" id="resumeUpload" name="resumeUpload" /><span id="yesResume" style="margin-left: 10px;"><input type="checkbox" name="deleteResume" id="deleteResume" />Remove</span>
                                </div>
                            </td>
                        </tr>
                    </table>

                    <table>
                        <tr><td><br /></td></tr>
                        <tr>
                            <td colspan=2>
                                <div class="round" id="subDivider">
                                    <div style="width: 200px; text-align: left; float:left; margin-left: 5px;">Social Networks</div>
                                    <div class="cancelSaveSection" style="float:right;">
                                        <img class="cancelUpdateButton" onclick="javascript:void(0);" title="Cancel" src="/widgets/images/button_x.png">
                                        <img class="updateButton" onclick="saveAll();" title="Save" src="/widgets/images/button_save.png">
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>

                    <table style="margin-left: 50px; margin-right: auto; line-height: 20px;">
                        <tr>
                            <td><img src="/widgets/css/images/socialmediaicons/twitter-24x24.png" alt="Follow Me On Twitter" />
                                <span id="Twitter">
                                    http://www.twitter.com/<input type="text" size="35" name="Twitter1" id="1" />
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td ><img src="/widgets/css/images/socialmediaicons/facebook-24x24.png"  alt="Visit Me On Facebook!" />
                                <span id="Twitter">
                                    http://www.facebook.com/<input type="text" size="35" name="Facebook2" id="2" />
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td ><img src="/widgets/css/images/socialmediaicons/linkedin-24x24.png" alt="LinkedIn" />
                                <span id="Twitter">
                                    http://www.linkedin.com/in/<input type="text" size="35" name="LinkedIn3" id="3" />
                                </span>
                            </td>
                        </tr>
                    </table>
                </form>
            </dd>
            <dt class="round sectionHeading" id="ExpertiseSection">
            <span style="width: 20px; height: 20px;">
                <a class="title" title="Expand Section"></a>
            </span>
            <span>
                4. Expertise
            </span>
            <span id="headerTechInfo"></span>
            </dt>
            <dd>
                <form id="expertiseForm">
                    <table>
                        <tr><td><br /></td></tr>
                        <tr>
                            <td colspan=2>
                                <div class="round" id="subDivider">
                                    <div style="width: 200px; text-align: left; float:left; margin-left: 5px;">Skills List</div>
                                    <div class="cancelSaveSection" style="float:right;">
                                        <img class="cancelUpdateButton" onclick="javascript:void(0);" title="Cancel" src="/widgets/images/button_x.png">
                                        <img class="updateButton" onclick="saveAll();" title="Save" src="/widgets/images/button_save.png">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr><td><br /></td></tr>
                    </table>
                    <!-- 13735 -->
                    <table style="margin-left: 10px; margin-right: auto; margin-bottom: 15px; line-height: 15px; width: 640px;text-align: justify;">       
                        <col width="40px" />
                        <col width="600px" />
                        <tr>
                            <td></td>
                            <td> Rate yourself in as many categories as you can(1=minimal, 5=expert).  Be as accurate as possible.<br /><br /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Our staff and clients will verify ratings over time. If you exaggerate your ratings, it will negatively affect your chances of being contacted for work.<br /><br /></td>
                        </tr>
                        <tr>                           
                            <td style="text-align:center;vertical-align:top;"><img name='New' src='/images/new.gif' alt='New'></td>
                            <td >FS-Experts are technicians that FieldSolutions has vetted through client training and/or work history for specific skills and work tasks.<a class="expertinfocontrols" href="javascript:void(0)">Click here</a> for more information.</td>
                        </tr>
                    </table >
                    <table id='techExpertiseTable' class='tablesorter' style="margin-left: 50px; margin-right: auto; line-height: 20px; ">            
                        <col width="205px" />
                        <col width="90px" />
                        <col width="200px" />
                        <col width="105px" />
                        <thead>
                            <tr>
                                <th align="center">Skill Category</th>
                                <th align="center">Work Orders</th>                                
                                <th align="center">Self-Rating (5=Expert)</th> 
                                 <!--13735-->
				<th align="center">FS-Expert&trade;<a class="expertinfocontrols" href="javascript:void(0)"><img style="vertical-align: middle;height:18px;width:18px" src="/widgets/images/get_info.png"></a></th>								
                            </tr>
                        </thead>                       
                        <tbody>
                        </tbody>
                    </table>
                     <!--End 13735-->    
                    <table>
                        <tr><td><br /></td></tr>
                        <tr>
                            <td colspan=2>
                                <div class="round" id="subDivider">
                                    <div style="width: 200px; text-align: left; float:left; margin-left: 5px;">Telephony Experience</div>
                                    <div class="cancelSaveSection" style="float:right;">
                                        <img class="cancelUpdateButton" onclick="javascript:void(0);" title="Cancel" src="/widgets/images/button_x.png">
                                        <img class="updateButton" onclick="saveAll();" title="Save" src="/widgets/images/button_save.png">
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table style="margin-left: 60px; line-height: 20px;">
                        <tr>
                            <td>
                                <table class="telephonyTable">
                                    <tr>
                                        <td colspan=3>Check all that apply</td>
                                    </tr>
                                    <tr>
                                        <td  style="vertical-align: top"><input type="checkbox" name="telephonySkills[]" value="4" id="InstallingTelephony" /> </td>
										<td>I have experience in installing OR maintaining (break-fix) telephony systems.</td>
                                    </tr>
                                    <tr>
                                        <td  style="vertical-align: top"><input type="checkbox" name="telephonySkills[]" value="6" id="ServerTelephony" /></td>
										<td> I have installed or performed "break-fix" activity on a Server OR VOIP based telephony system.</td>
                                    </tr>
                                    <tr>
                                        <td  style="vertical-align: top"><img name="New" src="/images/new.gif" alt="New" style="margin-left: -27px">
											<input type="checkbox" name="telephonySkills[]" value="50" id="UnderstandTelecomColorCodeExperience" /></td><td> I understand the telecom color code and have experience with termination jacks and patch panels.
                                        </td>
                                    </tr>
                                     <tr>
                                        <td style="vertical-align: top">
                                            <img name="New" src="/images/new.gif" alt="New" style="margin-left: -27px"> <input type="checkbox" name="telephonySkills[]" value="56" id="DMARCRoomsEquipmentExperience" /> </td>
											<td>I have experience with DMARC rooms and equipment such as Rj-21X, Smart jacks, lightning protectors, and cross connect fields
                                        </td>
                                    </tr>
                                     <tr>
                                        <td  style="vertical-align: top">
                                            <img name="New" src="/images/new.gif" alt="New" style="margin-left: -27px">
											<input type="checkbox" name="telephonySkills[]" value="57" id="OperationPBXExperience" /> </td><td>I have experience with basic PBX / Key system operation (ex: general programming, station card and CO line card replacement, station / extension assignment)
                                        </td>
                                    </tr>
                                </table>
                                <table class="telephony3Column">
                                    <tr>
                                        <td colspan=3><hr /></td>
                                    </tr>
                                    <tr>
                                        <td colspan=3>What experience and training do you have with the following products:</td>
                                    </tr>
                                    <tr>
                                        <td></td><td>Either Experience or Training</td><td>Within Last 12 Months?</td>
                                    </tr>
                                    <tr>
                                        <td colspan=3 style="font-weight:bold;">Avaya Small Systems</td>
                                    </tr>
                                    <tr>
                                        <td class="indent">Partner or Merlin</td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="8" id="AvayaPartnerMerlin" /></td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="9" id="AvayaPartnerMerlin6mo" /></td>
                                    </tr>
                                    <tr>
                                        <td class="indent">IP Office</td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="10" id="AvayaIPOffice" /></td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="11" id="AvayaIPOffice6mo" /></td>
                                    </tr>
                                    <tr>
                                        <td colspan=3 style="font-weight:bold;"> PBX</td>
                                    </tr>
                                    <tr>
                                        <td class="indent">Definity</td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="12" id="AvayaDefinity" /></td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="13" id="AvayaDefinity6mo" /></td>
                                    </tr>
                                    <tr>
                                        <td class="indent">8300/8500/8700</td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="14" id="AvayaPBX8300etc" /></td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="15" id="AvayaPBX8300etc6mo" /></td>
                                    </tr>
                                    <tr>
                                        <td class="indent">Gateway 250/350</td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="16" id="AyayaGateway" /></td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="17" id="AyayaGateway6mo" /></td>
                                    </tr>

                                    <tr>
                                        <td colspan=3 style="font-weight:bold;">Nortel Small Systems</td>
                                    </tr>
                                    <tr>
                                        <td class="indent">Norstar</td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="18" id="NortelNorstar" /></td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="19" id="NortelNorstar6mo" /></td>
                                    </tr>
                                    <tr>
                                        <td class="indent">BMS</td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="20" id="NortelBMS" /></td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="21" id="NortelBMS6mo" /></td>
                                    </tr>
                                    <tr>
                                        <td colspan=3 style="font-weight:bold;"> PBX</td>
                                    </tr>
                                    <tr>
                                        <td class="indent">Meridian</td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="22" id="NortelMeridian" /></td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="23" id="NortelMeridian6mo" /></td>
                                    </tr>
                                    <tr>
                                        <td class="indent">CS 1000/1500</td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="24" id="NortelCS" /></td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="25" id="NortelCS6mo" /></td>
                                    </tr>
                                    <tr>
                                        <td colspan=3 style="font-weight:bold;">Cisco</td>
                                    </tr>
                                    <tr>
                                        <td class="indent">Call Manager</td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="26" id="CiscoCM" /></td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="27" id="CiscoCM6mo" /></td>
                                    </tr>
                                    <tr>
                                        <td colspan=3 style="font-weight:bold;">Siemens (ROLM)</td>
                                    </tr>
                                    <tr>
                                        <td class="indent">9751</td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="28" id="Siemens9751" /></td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="29" id="Siemens97516mo" /></td>
                                    </tr>
                                    <tr>
                                        <td class="indent">Hicom</td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="30" id="SiemensHicom" /></td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="31" id="SiemensHicom6mo" /></td>
                                    </tr>
                                    <tr>
                                        <td class="indent">Hipath</td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="32" id="SiemensHipath" /></td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="44" id="SiemensHipath6mo" /></td>
                                    </tr>

                                    <tr>
                                        <td style="font-weight:bold;">NEC</td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="33" id="NEC" /></td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="34" id="NEC6mo" /></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight:bold;">InterTel</td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="35" id="InterTel" /></td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="36" id="InterTel6mo" /></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight:bold;">Mitel</td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="37" id="Mitel" /></td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="38" id="Mitel6mo" /></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight:bold;">Toshiba</td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="39" id="Toshiba" /></td>
                                        <td><input type="checkbox" name="telephonySkills[]" value="40" id="Toshiba6mo" /></td>
                                    </tr>								
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr><td><br /></td></tr>
                        <tr>
                            <td colspan=2>
                                <div class="round" id="subDivider">
                                    <div style="width: 200px; text-align: left; float:left; margin-left: 5px;">Cabling Experience</div>
                                    <div class="cancelSaveSection" style="float:right;">
                                        <img class="cancelUpdateButton" onclick="javascript:void(0);" title="Cancel" src="/widgets/images/button_x.png">
                                        <img class="updateButton" onclick="saveAll();" title="Save" src="/widgets/images/button_save.png">
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table style="margin-left: 60px; line-height: 20px;">
                        <tr>
                            <td>
                                <table class="cablingTable">
                                    <tr>
                                        <td colspan=3 style="vertical-align: top">Check all that apply</td>
                                    </tr>
                                    <tr>
                                        <td colspan=2 style="vertical-align: top">
                                            <input type="checkbox" name="telephonySkills[]" value="2" id="ReadWiringDiagrams" /></td>
											<td> Can you read wiring diagrams?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan=2 style="vertical-align: top">
                                            <input type="checkbox" name="telephonySkills[]" value="3" id="ExpInstallSurv" /> </td>
											<td>Do you have experience installing surveillance DVR/Cam systems?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan=2 style="vertical-align: top">
                                            <input type="checkbox" name="telephonySkills[]" value="41" id="SkilledCat5Fiber" /> </td>
											<td>Do you do consider yourself skilled with Cat 5 and or fiber cabling?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan=2 style="vertical-align: top">
                                        
                                            <input type="checkbox" name="telephonySkills[]" value="42" id="40HrsLast6Months" /> </td>
											<td>Have you performed over 40 hours of Cat 5 cabling contracts in the past 6 months?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan=2 style="vertical-align: top">
                                            <input type="checkbox" name="cablingEquipment[]" value="29" id="PossessAllLicenses" /> </td>
											<td>Do you possess all the licenses required in your city/state to perform low voltage work?
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan=2 style="vertical-align: top">
                                            <input type="checkbox" name="telephonySkills[]" value="45" id="CableTVTelcoCLECExperience" /> </td>
											<td>Do you have Cable TV Co., Telco, and/or CLEC work experience?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan=2 style="vertical-align: top">
                                            <input type="checkbox" name="telephonySkills[]" value="46" id="AdvCableLacStitchSkills" /></td>
											<td> Do you have advanced cable skills (including lacing and stitching)?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan=2 style="vertical-align: top">
                                            <input type="checkbox" name="telephonySkills[]" value="47" id="BatteryDistributionFuseBay" /></td>
											<td> Have you ever worked in a Battery Distribution Fuse Bay?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan=2 style="vertical-align: top">
                                            <input type="checkbox" name="telephonySkills[]" value="48" id="ReadingMOPsExperience" /> </td>
											<td>Do you have experience reading MOPs (Methods of Procedures)?
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan=2 style="vertical-align: top">
                                            <img name="New" src="/images/new.gif" alt="New" style="margin-left: -27px"> <input type="checkbox" name="telephonySkills[]" value="49" id="OperationScissorLiftExperience" /></td>
											<td> Do you know how to operate a scissor lift and have extensive experience using a ladder?
                                        </td>
                                    </tr>
                                     <tr>
                                        <td colspan=2 style="vertical-align: top">
                                            <img name="New" src="/images/new.gif" alt="New" style="margin-left: -27px"> <input type="checkbox" name="telephonySkills[]" value="51" id="InstallationMultipleCableExperience" /> </td>
											<td>Do you have experience with multiple cable installation methods (installing in conduit using fish tape, pull string, or shop vac)?
                                        </td>
                                    </tr>
                                     <tr>
                                        <td colspan=2 style="vertical-align: top">
                                            <img name="New" src="/images/new.gif" alt="New" style="margin-left: -27px"> <input type="checkbox" name="telephonySkills[]" value="52" id="InstallingSleevesRacewaysExperience" /> </td><td>Do you have experience installing sleeves firestops, J-Hooks, Panduit, and Wire Mold raceways?
                                        </td>
                                    </tr>
                                     <tr>
                                        <td colspan=2 style="vertical-align: top">
                                            <img name="New" src="/images/new.gif" alt="New" style="margin-left: -27px"> <input type="checkbox" name="telephonySkills[]" value="53" id="InstallationVoiceDataEquipmentExperience" /> </td><td>Do you have experience with voice and data equipment room installation and build out (ex: data cabinets and racks, plywood, 66/110 blocks, patch panels, etc)?
                                        </td>
                                    </tr>
                                     <tr>
                                        <td colspan=2 style="vertical-align: top">
                                            <img name="New" src="/images/new.gif" alt="New" style="margin-left: -27px"> <input type="checkbox" name="telephonySkills[]"  value="54" id="SingleAndMultiModeTerminationTestingExp" /> </td><td>Do you have experience in single-mode and multi-mode termination and testing?
                                        </td>
                                    </tr>
                                     <tr>
                                        <td colspan=2 style="vertical-align: top">
                                            <img name="New" src="/images/new.gif" alt="New" style="margin-left: -27px"> <input type="checkbox" name="telephonySkills[]" value="55" id="UsingLaptopForSwitchConfigurationExp" /></td><td> Do you have experience using a laptop for basic switch configuration (HyperTerminal, Terra Term, etc.)?
                                        </td>
                                    </tr>
                                    
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table>
                        <tr><td><br /></td></tr>
                        <tr>
                            <td colspan=2>
                                <div class="round" id="subDivider">
                                    <div style="width: 200px; text-align: left; float:left; margin-left: 5px;">Language Skills</div>
                                    <div class="cancelSaveSection" style="float:right;">
                                        <img class="cancelUpdateButton" onclick="javascript:void(0);" title="Cancel" src="/widgets/images/button_x.png">
                                        <img class="updateButton" onclick="saveAll();" title="Save" src="/widgets/images/button_save.png">
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                     <table style="margin-left: 60px; line-height: 20px;" width="500px">
                        <tr>
                            <td colspan="3">
                              Check all that apply
                            </td>
                        </tr>
                        <?php if(!empty($languegaSkill)) {
                            foreach($languegaSkill as $itemlanguega){
                        ?>        
                            <tr>
                                <td width="20px">
                                    <input <?= !empty($itemlanguega['TechID'])?'checked':"" ?> value="<?= $itemlanguega['id'] ?>" type="checkbox" class="LanguageSkillsCheckbox<?= $itemlanguega['id']=="9" && empty($itemlanguega['TechID'])?" LanguageSkillsAmericanCheckbox":"" ?>" name="LanguageSkillsCheckbox[]" />
                                </td>
                                <td class="LanguageSkillsLable">
                                    <?= $itemlanguega['name'] ?> 
                                </td>
                                <td>
                                    <select name="SpeakerLevel[]" style='<?= empty($itemlanguega['TechID'])?'display:none;':"" ?>width:150px;' >
                                        <option value="1" <?= ($itemlanguega['SpeakerLevel'] == "1")?'selected':"" ?> >Competent Speaker</option>
                                        <option value="2" <?= ($itemlanguega['SpeakerLevel'] == "2")?'selected':"" ?> >Fluent Speaker</option>
                                    </select>
                                </td>
                            </tr>
                        <?php }
                        } ?>
                     </table>   
                </form>
            </dd>
            <script type="text/javascript">
                function saveExpertise(){
                    //event.preventDefault();
                    showLoader();
                    var sendData = [];
                    $('#techExpertiseTable input').each(function(){
		
                        if(this.checked){
                            sendData.push({column:this.name,value:this.value});
                        }
                    });
	
                    $.ajax({
                        type: "POST",
                        url: '/techs/ajax/saveSelfRatings.php',
                        dataType    : 'json',
                        cache       : false,
                        data: {data:JSON.stringify(sendData), techID: $("#basicRegTechID").val(),languegaSkills:_updateprofile.getLanguageSkills()},
                        success: function (data) {
                            showSaveSuccessMsg();
                        },
                        error:function(data, status, xhr){
                            showSaveErrorMsg();
                        }
                    });
                }
            </script>
            <dt class="round sectionHeading" id="ToolsResourcesSection">
            <span style="width: 20px; height: 20px;">
                <a class="title" title="Expand Section"></a>
            </span>
            <span>
                5. Tools &amp; Resources
            </span>
            </dt>
            <dd>
                <div class="toolsResources">
                    <form id="equipmentForm" name="equipmentForm">
                        <table>
                            <!--  Payment Information -->
                            <tr><td><br /></td></tr>
                            <tr>
                                <td colspan=2>
                                    <div class="round" id="subDivider">
                                        <div style="width: 200px; text-align: left; float:left; margin-left: 5px;">Basic Tools</div>
                                        <div class="cancelSaveSection" style="float:right;">
                                            <img class="cancelUpdateButton" onclick="javascript:void(0);" title="Cancel" src="/widgets/images/button_x.png">
                                            <img class="updateButton" onclick="saveAll();" title="Save" src="/widgets/images/button_save.png">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table style="margin-left: 50px; line-height: 20px;">
                            <tr>
                                <td >
                                    <table>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="cablingEquipment[]" value="2" id="CellPhone" />
                                            </td>
                                            <td>
                                                Cellphone
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="cablingEquipment[]" value="1" id="Laptop" />
                                            </td>
                                            <td>
                                                Laptop (Microsoft XP or newer)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="cablingEquipment[]" value="25" id="DigitalCam" />
                                            </td>
                                            <td>
                                                Digital Camera
                                            </td>
                                        </tr>
                                        <tr>
                                            <td id="vehicleRow">
                                                <input type="checkbox" name="cablingEquipment[]" value="27" id="Vehicle" />
                                            </td>
                                            <td>
                                                Vehicle   
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan=2 id="vehicleMisc" style="display:none; padding-left: 25px;">
                                                <input type="hidden" name="vehicleID" id="vehicleID" />
                                                <input type="hidden" name="vehicleCurrentImage" id="vehicleCurrentImage" />
                                                <div style="padding-left: 2px;">
                                                    <input type="checkbox" name="cablingEquipment[]" value="30" id="PanelTruck" /> Panel Truck
                                                </div>

                                                <div style="padding-left: 2px;">
                                                    <input type="checkbox" name="cablingEquipment[]" value="28" id="TruckCarryLaddersCable" /> Vehicle can transport ladders &amp; supplies
                                                </div>
                                                <div>
                                                    <span>Vehicle Image</span><span style="margin-left: 33px;"><input type="file" name="vehicleImage" id="vehicleImage" size=15 /></span>
                                                </div>
                                                <div>
                                                    <span>Vehicle Description</span><span> <input type="text" name="vehicleDesc" id="vehicleDesc" size=25 /></span>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="basicEquipment[]" value="Ladder" id="Ladder" />
                                            </td>
                                            <td>
                                                Ladder
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan=2 id="ladderMisc" style="display:none; padding-left: 25px;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" name="cablingEquipment[]" value="5" id="Ladder_6" /> 6' Ladder
                                                        </td>
														
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" name="cablingEquipment[]" value="6" id="Ladder_12" /> 12' Ladder
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td >
                                                            <input type="checkbox" name="cablingEquipment[]" value="7" id="Ladder_20Plus" /> 20' Ladder
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  style="vertical-align: top">
                                                <input type="checkbox" name="cablingEquipment[]" value="23" id="Screwdrivers" />
                                            </td>
                                            <td>
                                                Set of Screwdrivers  
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  style="vertical-align: top">
                                                <img name="New" src="/images/new.gif" alt="New" style="margin-left: -27px"> <input type="checkbox" name="cablingEquipment[]" value="36" id="HandTools" />
                                            </td>
                                            <td>
                                                Hand Tools </td>
                                        </tr>
                                        <tr>
                                            <td  style="vertical-align: top">
                                                <img name="New" src="/images/new.gif" alt="New" style="margin-left: -27px"> <input type="checkbox" name="cablingEquipment[]" value="37" id="CordlessDrill" />
                                            </td>
                                            <td>
                                                Cordless Drill
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top">
                                                <img name="New" src="/images/new.gif" alt="New" style="margin-left: -27px"> <input type="checkbox" name="cablingEquipment[]" value="38" id="ProtectionEquipment" />
                                            </td>
                                            <td>
                                                Personal Protection Equipment (Gloves, Hard Hat, Boots)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  style="vertical-align: top">
                                                <img name="New" src="/images/new.gif" alt="New" style="margin-left: -27px"> <input type="checkbox" name="cablingEquipment[]" value="39" id="MachineLabelMaker" />
                                            </td>
                                            <td>
                                                Machine Label Maker (P-Touch, Bradley, etc.)
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="vertical-align: top;">
                                    <table>
                                        <tr>
                                            <td>
                                                <div style="vertical-align:top;">
                                                    <div>Other Tools</div>
                                                    <div><textarea cols=20 rows=5 name="Tools" id="Tools"></textarea></div>
                                                    <div style="height: 1.3em">&nbsp;</div>
                                                    <div><span id="vehicleImageLabel"></span><span id="yesVehicleImage" style="display:none;"><input type="checkbox" name="deleteVehicleImage" id="deleteVehicleImage" />Remove</span></div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                        <table>
                            <tr><td><br /></td></tr>
                            <tr>
                                <td colspan=2>
                                    <div class="round" id="subDivider">
                                        <div style="width: 200px; text-align: left; float:left; margin-left: 5px;">Cabling & Telephony Tools</div>
                                        <div class="cancelSaveSection" style="float:right;">
                                            <img class="cancelUpdateButton" onclick="javascript:void(0);" title="Cancel" src="/widgets/images/button_x.png">
                                            <img class="updateButton" onclick="saveAll();" title="Save" src="/widgets/images/button_save.png">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table style="margin-left: 50px; line-height: 20px;">
                            <tr>
                                <td colspan=1></td>
                            </tr>
                            <tr>
                                <td style="width: 50%;">
                                    <table>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="cablingEquipment[]" value="20" id="CordMasonryDrillHammer" /> Corded masonry drill hammer and bits up to 1" diameter
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="cablingEquipment[]" value="19" id="RotaryCoaxCable" /> Rotary coax cable stripper for RG-59 and RG-6
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="cablingEquipment[]" value="21" id="RechargeCCTV" /> Rechargeable CCTV installer's LCD 4
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="cablingEquipment[]" value="22" id="VoltOhmMeter" /> Volt/Ohm Meter
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="cablingEquipment[]" value="18" id="FishTape50" /> 50'+ Fish Tape
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="cablingEquipment[]" value="10" id="DigitalVOMMeter" /> Digital VOM Meter
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="cablingEquipment[]" value="11" id="ContinuityTester" /> Continuity Tester
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="cablingEquipment[]" value="9" id="ButtSet" /> Butt Set
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="cablingEquipment[]" value="8" id="ToneGeneratorAndWand" /> Tone Generator and Wand
                                            </td>
                                        </tr>	
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="cablingEquipment[]" value="24" id="PunchTool" /> Punch Tool
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan=2 id="punchToolMisc" style="display:none; padding-left: 25px;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" name="cablingEquipment[]" value="12" id="PunchTool66" /> Punch Tool with 66 Blade
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" name="cablingEquipment[]" value="13" id="PunchTool110" /> Punch Tool with 110 Blade
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" name="cablingEquipment[]" value="14" id="PunchToolBix" /> Punch Tool with Bix Blade
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" name="cablingEquipment[]" value="15" id="PunchToolKrone" /> Punch Tool With Krone Blade
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="cablingEquipment[]" value="16" id="CrimpToolRJ11" /> Crimp Tool with RJ11
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="cablingEquipment[]" value="17" id="CrimpToolRJ45" /> Crimp Tool with RJ45
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="cablingEquipment[]" value="31" id="Cable" /> Cable
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                               <img name="New" src="/images/new.gif" alt="New" style="margin-left: -27px"> <input type="checkbox" name="cablingEquipment[]" value="32" id="CableTesters" /> Cable Testers
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img name="New" src="/images/new.gif" alt="New" style="margin-left: -27px"> <input type="checkbox" name="cablingEquipment[]" value="33" id="CableCertifier" /> Cable Certifier (able to download/print results)
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td>
                                                <img name="New" src="/images/new.gif" alt="New" style="margin-left: -27px"> <input type="checkbox" name="cablingEquipment[]" value="34" id='LacingNeedle' /> Lacing Needle
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img name="New" src="/images/new.gif" alt="New" style="margin-left: -27px"> <input type="checkbox" name="cablingEquipment[]" value="35" id='WireWrapGun' /> Wire Wrap Gun
                                            </td>
                                        </tr>
                                          <tr>
                                            <td>
                                                <img name="New" src="/images/new.gif" alt="New" style="margin-left: -27px"> <input type="checkbox" name="cablingEquipment[]" value="40" id='GoferPoles' /> Gofer Poles (or Glow Rods)                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <img name="New" src="/images/new.gif" alt="New" style="margin-left: -27px"> <input type="checkbox" name="cablingEquipment[]" value="41" id='FiberOpticdBLossMeter' /> Fiber Optic dB Loss Meter & Light Source                                                
                                            </td>
                                        </tr>
                                         <tr>
                                            <td>
                                                <img name="New" src="/images/new.gif" alt="New" style="margin-left: -27px"> <input type="checkbox" name="cablingEquipment[]" value="42" id='FiberOpticTerminationKit' /> Fiber Optic Termination Kit                                                
                                            </td>
                                        </tr>
                                    </table>
                                </td>					
                            </tr>
                        </table>
                        </td>
                        </tr>

                        </table>
                    </form>
                </div>	
            </dd>

            <dt class="round sectionHeading" id="CredentialsSection">
            <span style="width: 20px; height: 20px;">
                <a class="title" title="Expand Section"></a>
            </span>
            <span>
                6. FS-TrustedTech &trade; (Credentials)
            </span>
            </dt>
            <dd>
                <br />

                <table id='publicCredentialsHeader'>
                    <tr>
                        <td colspan=2>
                            <div class="round" id="subDivider">
                                <div style="width: 200px; text-align: left; float:left; margin-left: 5px;">Public Credentials</div>
                                <!--  div style="text-align: right;">
                                        <a id="publicCredentialsEditLink" href="javascript:void(0);" onclick="editSection('publicCredentials');">Edit</a>
                                </div -->
                                <!--  <div class="editSection" style="float:right; padding-right: 5px;"> 
                                    <a href="javascript:void(0);" class="updateButton" id="publicCredentialsEditLink" onclick="editSection('publicCredentials');" title="Edit">Edit</a>
                                </div>14056-->
                                <div class="cancelSaveSection" style="float:right; display:none;">
                                    <img class="cancelUpdateButton" onclick="cancelSection('publicCredentials');" title="Cancel" src="/widgets/images/button_x.png">
                                    <img class="updateButton" onclick="saveAll();" title="Save" src="/widgets/images/button_save.png">
                                </div>
                            </div>
                        </td>
                    </tr>
                  <tr><td><br /></td></tr>
                </table>

                <table>
                    <tr>
                        <td style="width:45%;">
                            <table id='publicCredentials' class='tablesorter' style="margin-left: 50px; margin-right: auto; line-height: 20px; width:470px;" >
                                <form name="pubCred" id="pubCred">
                                    <input type="hidden" name="techID" id="basicRegTechID" value="<?= $_SESSION['TechID']; ?>" />
                                    <thead>
                                        <tr>
                                            <th width="52%">Public Credentials</th>
                                            <th>Status</th>
                                            <th width="25%">Date Completed</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr id="bgCheck">
                                            <td><a href="javascript:///" rel="/techs/ajax/lightboxPages/backgroundChecks.php" class="bgCheckLink">Background Check</a></td>
                                            <td id="credStatus"></td>
                                            <td>
                                                <div id="bgCheckDate"></div>
                                                <input id="bgCheckInput" style="display:none;" type="text" size="15" name="Bg_Test_ResultsDate_Lite" /> 
                                            </td>
                                        </tr>
                                        <tr id="bgCheckFull">
                                            <td><a href="javascript:///" rel="/techs/ajax/lightboxPages/backgroundChecks.php" class="bgCheckLink">Comprehensive Background Check</a></td>
                                            <td id="credStatus"></td>
                                            <td>
                                                <div id="bgCheckFullDate"></div>
                                                <!--  input id="bgCheckFullInput" style="display:none;" type="text" size="15" name="BG_Test_ResultsDate_Full" / --> 
                                            </td>
                                        </tr>
                                        <tr id="drugTest">
                                            <td><a href="javascript:///" rel="/techs/ajax/lightboxPages/backgroundChecks.php" class="bgCheckLink">Gold Drug Test - 9 Panel </a></td>
                                            <td id="credStatus"></td>
                                            <td>
                                                <div id="drugTestDate"></div>
                                                <!-- input id="drugTestInput" style="display:none;" type="text" size="15" name="" / --> 
                                            </td>
                                        </tr>
							
                                 <tr id="drugTestSilver">
                                    <td><a href="javascript:///" rel="/techs/ajax/lightboxPages/backgroundChecks.php" class="bgCheckLink">Silver Drug Test - 5 Panel</td>
                                    <td id="SilverDrugStatus"></td>
                                    <td>
                                        <div id="SilverDrugsDate"></div>                                    
                                    </td>
                                </tr>
                                <!--End 13700-->    
<?//13624?>                                        
                                        <tr id="blueRibbon">
                                            <td>
                                                <a class="blueRibbon"  href="javascript:void(0);">Blue Ribbon Technician</a>
                                            </td>
                                            <td id="blueRibbonStatus"></td>
                                            <td>
                                                <div id="blueRibbonDate"></div>
                                            </td>
                                        </tr>
                                        <tr id="bootCamp">
                                            <td id="bootcamlabel"><a class="bootCamp"  href="javascript:void(0);">Boot Camp Certified</a></td>
                                            <td id="bootCampStatus"></td>
                                            <td>
                                                <div id="bootCampDate"></div>
                                            </td>
                                        </tr>
                                        <tr id="fsMobile">
                                            <td>FS-Mobile <img alt="New" src="/images/new.gif" width="23" height="12"/></td>
                                            <td id="fsMobileStatus"></td>
                                            <td>
                                                <div id="fsMobileDate"></div>
                                            </td>
                                        </tr>
                                        <tr id="WorkAuthorization">
                                            <td id="USAuthorized">
                                                US Verified
                                            </td>
                                            <td id="WorkAuthorizationStatus">
                                                <img src="/widgets/images/USAuthorized_16.png" style="display:none;" />

                                            </td>
                                            <td>
                                                <div id="WorkAuthorizationValue"></div>

                                            </td>
                                        </tr>
                                        <tr id="NACICert">
                                            <td>NACI Govement Security Clearance</td>
                                            <td id="NACICertStatus" ></td>
                                            <td>
                                                <div id="NACIDate"></div>
                                                <input id="NACIInput" style="display:none;" type="text" size="15" name="NACIClearDate"  /> 
                                            </td>
                                        </tr>
                                        <!--  tr id="w9">
                                                        <td>W9</td>
                                                        <td id="credStatus"></td>
                                                        <td>
                                                                <div id="w9Date"></div>
                                                                <input id="w9Input" style="display:none;" type="text" size="15" name="" /> 
                                                        </td>
                                        </tr -->
                                 <!--13932-->	
                                <tr id="interimGovt">
                                    <td>Interim Government Security Clearance</td>
                                    <td>
                                        <div id="interimGovtStatusLabel"></div>         
                                    </td>
                                    <td>
                                        <div id="interimGovtDate"></div>
                                        <!--<input id="interimGovtInput" style="display:none;" type="text" size="15" name="InterimSecClearDate"  /> -->
                                    </td>
                                </tr>
                                <tr id="fullGovt">
                                    <td>Secret Government Security Clearance</td>
                                    <td>
                                        <div id="fullGovtStatusLabel"></div>                                        
                                    </td>
                                    <td>
                                        <div id="fullGovtDate"></div>
                                        <!--<input id="fullGovtInput" style="display:none;" type="text" size="15" name="FullSecClearDate"  /> -->
                                    </td>
                                </tr>
                                <tr id="topGovt">
                                    <td>Top Secret Government Security Clearance</td>
                                    <td>
                                        <div id="topGovtStatusLabel"></div>
                                    </td>
                                    <td>
                                        <div id="topGovtDate"></div>
                                        <!--<input id="topGovtInput" style="display:none;" type="text" size="15" name="TopSecClearDate" />-->
                                    </td>
                                </tr>
                                <!--14056-->
                                <tr id="topGovtSCI">
                                    <td>Top Secret SCI Government Security Clearance</td>
                                    <td >
                                        <div id="topSCIGovtStatusLabel"></div>                                       
                                    </td>
                                    <td>
                                        <div id="topSCIGovtDate"></div>                                       
                                    </td>
                                </tr>
                                <!--14056-->	                                
                                    </tbody>
                                </form>
                            </table>
                        </td>
                        <td style="text-align:justify; padding-left: 15px;">
                            <span style="font-weight: bold; color: #4790D4; ">FieldSolutions Insurance Coverage<br /></span>
                            <span>
                                FieldSolutions insures our clients against risk of work on-site. As such, we never put you through the hassle of producing your insurance coverages, we donâ€™t verify whether your payments are current; 
                                and most important we don&rsquo;t charge you a fee like other systems for coverages. Insurance coverage is just one reason FieldSolutions is the most preferred place to get technology field work. Remind your 
                                clients that you want to get your work and paid through FieldSolutions.</span>
                        </td>
                    </tr>
                </table>
                <br />

                <table id='clientCredentialsHeader'>
                    <tr>
                        <td colspan=2>
                            <div class="round" id="subDivider">
                                <div style="width: 200px; text-align: left; float:left; margin-left: 5px;">Client Credentials</div>

                                <!--  div style="text-align: right;"><a id="clientCredentialsEditLink" onclick="editSection('clientCredentials');" href="javascript:void(0);">Edit</a></div -->
                                <!--912-->
                                <div class="editSection" style="float:right; padding-right: 5px;"> 
                                        <a href="javascript:void(0);" class="updateButton" id="clientCredentialsEditLink" onclick="editSection('clientCredentials');" title="Edit">Edit</a>
                                </div>
                                <!--end 912-->
                                <div class="cancelSaveSection" style="float:right; display:none;">
                                    <img class="cancelUpdateButton" onclick="cancelSection('clientCredentials');" title="Cancel" src="/widgets/images/button_x.png">
                                    <img class="updateButton" onclick="saveAll();" title="Save" src="/widgets/images/button_save.png">
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr><td><br /></td></tr>
                </table>
                <!--931-->
                <table id='clientCredentials' class='tablesorter' style="margin-left: 50px; margin-right: auto; line-height: 20px;">
                    <form id="clientCred" name="clientCred">
                        <input type="hidden" name="techID" id="basicRegTechID" value="<?= $_SESSION['TechID']; ?>" />
                        <thead>
                            <tr>
                                <th>Client Credentials</th>
                                <th width="40%">Status / Certification#</th>
                                <th>Date Completed</th>
                                <!--end 931-->
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            //13624
                            if (!empty($FSTagList))
                            {
                                //839
                                $apiTechClass = new Core_Api_TechClass();
                                $currentTechID= $_SESSION['TechID'];
                                //end 839
                                $DidPurpleTechnicianDisplay = false;
                                $DidCompuComDisplay = false;
                                foreach ($FSTagList as $item)
                                {
                                    //--- prepare to display
                                    $name = $item['Name'];
                                    $title = $item["Title"];
                                    $number = $item["Number"];
                                    $date = $item["Date"];
                                    //912
                                    $FSTagId = $item["FSTagId"];
                                    
                                    if(empty($number) || $number=='null'){
                                        $number="";
                                    }
                                    
                                    $hasImage = false;
                                    $hasDate = false;
                                    $imgSrc = "";
                                    if(!empty($item["TechId"])){
                                        $hasImage=true;
                                        if(!empty($date)){
                                            $hasDate = true;
                                        }
                                        if(!empty($item["IsExistingTag"])){
                                            $imgSrc = $item["ExistingImagePath"];
                                        } else {
                                            if(!empty($item["TagArt"]))//13735
                                            {
                                            $imgSrc = "https://s3.amazonaws.com/wos-docs/tags/". str_replace ("|", "", $item["TagArt"]);
                                            }
                                        }
                                    }
                                    $innerImgTD = "";
                                    if($hasImage) {
                                        $innerImgTD = "<img src='$imgSrc' height='19px' />&nbsp;$number";
                                    }
                                    //931
                                    $innerlevelTD="";
                                    $HasMultiTagLevels = $item["HasMultiTagLevels"];
                                    $TagLevelsData = $item["TagLevelsData"];
                                    if($HasMultiTagLevels=='1')
                                    {
                                        $innerlevelTD =" L".$item["LevelOrder"]." &ndash; ".$item["LevelTitle"];
                                    }
                                    //end 931
                                    $innerDateTD = "";
                                    if($hasDate){
                                        $innerDateTD = "<div>".date_format(new DateTime($date), "m/d/Y")."</div>";
                                    }
                                    //912
                                    $techVisibility = 0;
                                    if(($item["HideFromTechs"]==1) || ($item["HideFromTechs"]==2 && $item["TechId"]==$_SESSION['TechID']))
                                    {
                                        $techVisibility =1;
                                    }
                                    else if($name=="BlackBoxCabling" || $name=="BlackBoxTelecom")
                                    {
                                        $techVisibility =$item["VisibilityId"];
                                    }
                                    
                                    $isAllowEditCert = 0;
                                    if($item["CanTechEdit"]==1)
                                    {
                                        $isAllowEditCert = 1;
                                    }
                                    if($techVisibility ==0)
                                    {
                                        continue;
                                    }
                                    $specialcase = array(
                                            "Dell_MRA_Compliant"=>"Dell_MRA_Compliant",
                                        "ncrBasic"=>"ncrBasic",
                                        "FLSID"=>"FLSID",
                                        "ComputerPlus"=>"ComputerPlus",
                                        "GGETechEvaluation"=>"GGETechEvaluation",
                                        "ServRightBrother"=>"ServRightBrother",
                                        "TechForceICAgreement"=>"TechForceICAgreement"
                                    );
                                    
                                    if(!array_key_exists($name, $specialcase))
                                    {
                                        if($name=="BlackBoxCabling")
                                        {
                                            $BlackBoxCablingStatus = $apiTechClass->GetBlackBoxCablingStatus($currentTechID);
                                            if(!empty($BlackBoxCablingStatus))
                                            {
                                                $dateBTCS = $BlackBoxCablingStatus['Date'];
                                                if(!empty($dateBTCS))
                                                {
                                                    $strDateBTCS =  date_format(new DateTime($dateBTCS), "m/d/Y");
                                                }
                                                else
                                                {
                                                    $strDateBTCS = "";
                                                }

                                                if($BlackBoxCablingStatus['Status']=="Pending")
                                                {
                                                    $islinkcomfirmBCCS = "";
                                                    $strDateBTCS = "";
                                                    $strNameBTCS = "<a class='BlackBoxCabling cmdBlackBoxCabling' href='javascript:;'>Black Box Cabling</a>";
                                                    $BTCSStatus  = $BlackBoxCablingStatus['LevelText'] . " - ".$BlackBoxCablingStatus['Status'];
                                                }
                                                else
                                                {
                                                    $islinkcomfirmBCCS="";
                                                    $strNameBTCS = "Black Box Cabling";
                                                    $BTCSStatus  = $BlackBoxCablingStatus['LevelText'];
                                                }
                                                $BTCSID = $BlackBoxCablingStatus['CertID'];
                                            }
                                            else
                                            {
                                                $BTCSID="";
                                                $strDateBTCS="";
                                                $BTCSStatus="<a class='BlackBoxCabling cmdBlackBoxCabling' href='javascript:;'>Get Tagged!</a>";
                                                $strNameBTCS = $strNameBTCS = "<a class='BlackBoxCabling cmdBlackBoxCabling' href='javascript:;'>Black Box Cabling</a>";
                                            }
                                            ?>
                                            <tr>
                                                <td>
                                                    <span id="BlackBoxCablingName"><?=$strNameBTCS?><span>
                                                </td>
                                                <td>
                                                    <span class="BlackBoxCablingStatus">
                                                        <?=$BTCSStatus?>
                                                    </span>
                                                    <input type="hidden" name="BlackBoxCablingid" value="<?=$BTCSID?>">
                                                    <select name="BlackBoxCablingcertifications" style="display:none;" id="BlackBoxCablingNumInput">
                                                        <option value="">Select Status</option>
                                                        <option value="46" <?=($BlackBoxCablingStatus['Status']!='Pending' && $BTCSID=='46')?"selected":"";?>>Helper (C1)</option>
                                                        <option value="47" <?=($BlackBoxCablingStatus['Status']!='Pending' && $BTCSID=='47')?"selected":"";?>>Advanced (C2)</option>
                                                        <option value="48" <?=($BlackBoxCablingStatus['Status']!='Pending' && $BTCSID=='48')?"selected":"";?>>Lead (C3)</option>
                                                        <option value="pending" <?=$BlackBoxCablingStatus['Status']=='Pending'?"selected":"";?>>Pending</option>
                                                    </select>
                                                </td>  
                                                <td>
                                                    <div id="BlackBoxCablingDate">
                                                        <?=$strDateBTCS.$islinkcomfirmBCCS;?>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?
                                        }
                                        else if($name=="BlackBoxTelecom")
                                        {
                                            $BlackBoxTelecomStatus = $apiTechClass->getBlackBoxTelecomStatus($currentTechID);
                                            if(!empty($BlackBoxTelecomStatus))
                                            {
                                                $dateBTTS = $BlackBoxTelecomStatus['Date'];
                                                if(!empty($dateBTCS))
                                                {
                                                    $strDateBTTS =  date_format(new DateTime($dateBTTS), "m/d/Y");
                                                }
                                                else
                                                {
                                                    $strDateBTTS = "";
                                                }
                                                if($BlackBoxTelecomStatus['Status']=="Pending")
                                                {
                                                    $islinkcomfirmBCTS = "";
                                                    $strDateBTTS="";
                                                    $strNameBTTS = "<a class='BlackBoxTelecom cmdBlackBoxTelecom' href='javascript:;'>Black Box Telecom</a>";
                                                    $BTTSStatus  = $BlackBoxTelecomStatus['LevelText'] . " - ".$BlackBoxTelecomStatus['Status'];
                                                }
                                                else
                                                {
                                                    $islinkcomfirmBCTS="";
                                                    $strNameBTTS="Black Box Telecom";
                                                    $BTTSStatus  = $BlackBoxTelecomStatus['LevelText'];
                                                }
                                                $BTTSID = $BlackBoxTelecomStatus['CertID'];
                                            }
                                            else
                                            {
                                                $BTTSID="";
                                                $strDateBTTS="";
                                                $BTTSStatus="<a class='BlackBoxTelecom cmdBlackBoxTelecom' href='javascript:;'>Get Tagged!</a>";
                                                $strNameBTTS = "<a class='BlackBoxTelecom cmdBlackBoxTelecom' href='javascript:;'>Black Box Telecom</a>";
                                            }
                                            ?>
                                            <tr>
                                                <td>
                                                    <span id="BlackBoxTelecomName"><?=$strNameBTTS?></span>
                                                </td>
                                                <td>
                                                    <span class="BlackBoxTelecomStatus">
                                                        <?=$BTTSStatus?>
                                                    </span>
                                                    <input type="hidden" name="BlackBoxTelecomid" value="<?=$BTTSID?>">
                                                    <select name="BlackBoxTelecomcertifications" style="display:none;" id="BlackBoxTelecomStatusNumInput">
                                                        <option value="">Select Status</option>
                                                        <option value="49" <?=($BlackBoxTelecomStatus['Status']!='Pending' && $BTTSID=='49')?"selected":"";?>>Helper (T1)</option>
                                                        <option value="50" <?=($BlackBoxTelecomStatus['Status']!='Pending' && $BTTSID=='50')?"selected":"";?>>Advanced (T2)</option>
                                                        <option value="pending" <?=$BlackBoxTelecomStatus['Status']=='Pending'?"selected":"";?>>Pending</option>
                                                    </select>
                                                </td>  
                                                <td>
                                                    <div id="BlackBoxTelecomDate">
                                                        <?=$strDateBTTS.$islinkcomfirmBCTS;?>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?
                                        }
                                        else
                                        {
                                        ?>
                                            <tr id="<?=$name;?>cert">
                                                <td>
                                                    <?= $title ?>
                                                </td>
                                                <td id="<?=$name;?>Status">
                                                    <div id="<?=$name;?>StatusLabel" isAllowEditCert ="<?=$isAllowEditCert?>">
                                                        <?if($hasImage)
                                                        {
                                                            //931
                                                            ?>
                                                            <img src='<?=$imgSrc;?>' height='19px' /> <?=" ".$innerlevelTD?>
                                                        <?
                                                            //end 931
                                                        }
                                                        ?>
                                                    </div>
                                                    <?
                                                    if($isAllowEditCert==1)
                                                    {
                                                        //931
                                                        if($HasMultiTagLevels=="1")
                                                        {
                                                            ?>
<!--                                                                <select id="cbx<?=$name?>" style="display:none;" name ="certificationsNewStatus_<?=$FSTagId?>">-->
                                                                <select id="cbx<?=$name?>" onchange="HasMultiTagLevels_onchange('<?=$name?>')" class="cbxHasMultiTagLevels" style="display:none;" name ="FSTag_<?= $FSTagId?>_MultiLevelCbx">
                                                                    <option value="">Select Level</option>
                                                                    <?
                                                                    foreach($TagLevelsData as $taglevel)
                                                                    {
                                                                        ?>
                                                                            <option value="<?=$taglevel['id']?>" <?=($taglevel['DisplayOrder']==$item["LevelOrder"])?"selected":""?>><?="L".$taglevel['DisplayOrder']." &ndash; ".$taglevel['Title']?></option>
                                                                            <?
                                                                    }
                                                                    ?>
                                                                </select>
                                                                <input type="hidden" value="<?=$FSTagId?>" name="certificationsNew[]" style="display: table-cell;">
                                                                <?
                                                        }
                                                        else
                                                        {
                                                        ?>
                                                            <input id="<?=$name;?>chk" style="display:none;" <?=$hasImage?"checked":""?> type="checkbox" size="15" name="certificationsNewStatus_<?=$FSTagId?>"  /> 
                                                            <input type="hidden" value="<?=$FSTagId?>" name="certificationsNew[]" style="display: table-cell;">
                                                        <?
                                                        }
                                                        //end 931
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <div id="<?=$name;?>Date" isAllowEditCert ="<?=$isAllowEditCert?>"><?= $hasImage?date_format(new DateTime($date), "m/d/Y"):""; ?></div>
                                                    <?
                                                    if($isAllowEditCert==1)
                                                    {
                                                        ?>
                                                            <input id="<?=$name;?>Input" style="display:none;" type="text" size="15" name="certificationsNewDate_<?=$FSTagId?>" value="<?= $hasImage?date_format(new DateTime($date), "m/d/Y"):""; ?>" /> 
                                                        <?
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                        <?
                                        }
                                    }
                                    //--- display
                                    if ($name == "Dell_MRA_Compliant")
                                    {
                                        ?>
                                       <tr id="DellMRACompliant">
                                            <td><?= $title ?></td>
                                            <td id="DellMRACompliantStatus"></td>
                                            <td>
                                                <div id="DellMRACompliantDate"></div>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    else if($name == "ncrBasic")
                                    {
                                        ?> 
                                        <tr id="ncrBasic">
                                            <td><a class="certForms"  href="javascript:void(0);" rel="/techs/Training/NCR/NCR_Training.php"><?= $title ?></a></td>
                                            <td id="ncrBasicStatus"></td>
                                            <td>
                                                <div id="ncrBasicDate"></div>
                                                <?
                                                if($isAllowEditCert==1)
                                                {
                                                    ?>
                                                    <input id="ncrBasicInput" style="display:none;" type="text" size="15" name="NCR_Basic_Cert_Date"  /> 
                                                    <?
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    else if($name == "FLSID")
                                    {
                                        ?>
                                        <tr id="flsId">
                                            <td><a class="certForms"  href="javascript:void(0);" rel="/techs/Training/FLS/flsCertTest.php"><?= $title ?></a></td>
                                            <td id="flsIdStatus"></td>
                                            <td>
                                                <div id="flsIdDate"></div>
                                                <?
                                                if($isAllowEditCert==1)
                                                {
                                                    ?>
                                                        <input id="flsIdnput" style="display:none;" type="text" size="15" name="flsIdDate" />
                                                    <?                    
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    else if($name == "ComputerPlus")
                                    {
                                        ?> 
                                        <tr id="ComputerPlus">
                                            <td><a href="javascript:;" class="allowLinkTD cmdComputerPlusCertification"><?= $title ?></a></td>
                                            <td id="ComputerPlusStatus">
                                                <div style="float:left;" id="ComputerPlusStatusLabel"></div>
                                            </td>
                                            <td align="left">
                                                <div id="ComputerPlusDate"></div>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    else if($name == "GGETechEvaluation")
                                    {
                                        ?> 
                                        <tr id="GGETechEvaluation">
                                            <td valign="top"><a href="javascript:;" profile="1" class="GGETechEvaluation"><?= $title ?></a></td>
                                            <td id="GGETechEvaluationStatus">
                                                <div id="GGETechEvaluationStatusLabel"></div>
                                            </td>
                                            <td align="left">
                                                <div id="GGETechEvaluationDate"></div>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    else if($name == "ServRightBrother")
                                    {
                                        ?> 
                                        <tr id="ServRightBrother">
                                            <td valign="top"><a href="javascript:;" class="ServRightBrotherCertification"><?= $title ?></a></td>
                                            <td id="ServRightBrotherStatus">
                                                <div id="ServRightBrotherStatusLabel"></div>
                                            </td>
                                            <td align="left">
                                                <div id="ServRightBrotherDate"></div>
                                            </td>
                                            <script>
                                                var _ServRightBrotherCertification = ServRightBrotherCertification.CreateObject({
                                                    id:'_ServRightBrotherCertification' 
                                                    });                                 
                                            </script>
                                        </tr>
                                    <?php
                                    }
                                    else if($name == "TechForceICAgreement")
                                    {
                                        ?>
                                        <tr id="TechForceCredential">
                                            <td valign="top"><a href="javascript:;" class="TechFORCEContractorAgreement"><?= $title ?></a></td>
                                            <td id="TechForceCredentialStatus">
                                                <div id="TechForceCredentialStatusLabel"></div>
                                            </td>
                                            <td align="left">
                                                <div id="TechForceCredentialDate"></div>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    else if ( ($name == "CompuComTechnician" || $name == "CompuComAnalyst" || $name == "CompuComSpecialist" ) && ($DidCompuComDisplay===false) )
                                    {
                                        $DidCompuComDisplay=true;
                                        ?>
                                        <tr id="CompuComCertified">
                                            <td valign="top">CompuCom Certified</td>
                                            <td id="CompuComCertifiedStatus">
                                                <div id="CompuComCertifiedStatusLabel"></div>
                                            </td>
                                            <td align="left">
                                                <div id="CompuComCertifiedDate"></div>
                                            </td>
                                        </tr>
                                    <?php
                                    }
                                    else if ( ($name == "PurpleTechnicianUnverified" || $name == "PurpleTechnicianCertified") && ($DidPurpleTechnicianDisplay===false) )
                                    {
                                        $DidPurpleTechnicianDisplay=true;
                                        ?>                            
                                        <tr id="CertifiedPurpleTechnician">
                                            <td valign="top" id="CertifiedPurpleTechnicianLable">Verified <?= $title ?></td>
                                            <td id="CertifiedPurpleTechnicianStatus">
                                                <div id="CertifiedPurpleTechnicianStatusLabel"></div>
                                            </td>
                                            <td align="left">
                                                <div id="CertifiedPurpleTechnicianDate"></div>
                                            </td>
                                        </tr>                            
                                    <?php
                                    }
                                } // end of for
                            } // end of if
                            //end 912
                    //end  13761
                            //end 839
                ?>
                        </tbody>
                    </form>
                </table>
                <br />

                <table id='programCertificationsHeader'>
                    <tr>
                        <td colspan=2>
                            <div class="round" id="subDivider">
                                <div style="width: 200px; text-align: left; float:left; margin-left: 5px;">Product/Program Training</div>
                                <!--  div style="text-align: right;"><a id="programCertificationsEditLink" href="javascript:editSection('programCertifications');">Edit</a></div -->

                                <div class="editSection" style="float:right; padding-right: 5px;"> 
                                    <a href="javascript:void(0);" class="updateButton" id="programCertificationsEditLink" onclick="editSection('programCertifications');" title="Edit" style="display:none;">Edit</a>
                                </div>
                                <div class="cancelSaveSection" style="float:right; display:none;">
                                    <img class="cancelUpdateButton" onclick="cancelSection('programCertifications');" title="Cancel" src="/widgets/images/button_x.png">
                                    <img class="updateButton" onclick="saveAll();" title="Save" src="/widgets/images/button_save.png">
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr><td><br /></td></tr>
                </table>

                <table id='programCertifications' class='tablesorter' style="margin-left: 50px; margin-right: auto; line-height: 20px; ">
                    <form id="programCert" name="programCert">
                        <input type="hidden" name="techID" id="basicRegTechID" value="<?= $_SESSION['TechID']; ?>" />
                        <thead>
                            <tr>
                                <th>Program Certifications</th>
                                <th>Status / Certification#</th>
                                <th>Date Completed</th>
                            </tr>
                        </thead>
                        <tbody>
<?//13624?>                        
                            <tr id="ncrBasic">
                                <td><a class="certForms"  href="javascript:void(0);" rel="/techs/Training/NCR/NCR_Training.php">NCR Basic Certification</a></td>
                                <td id="ncrBasicStatus"></td>
                                <td>
                                    <div id="ncrBasicDate"></div>
                                    <input id="ncrBasicInput" style="display:none;" type="text" size="15" name="NCR_Basic_Cert_Date"  /> 
                                </td>
                            </tr>
                            <tr id="flsId">
                                <td><a class="certForms"  href="javascript:void(0);" rel="/techs/Training/FLS/flsCertTest.php">FLS Contractor ID</a></td>
                                <td id="flsIdStatus"></td>
                                <td>
                                    <div id="flsIdDate"></div>
                                    <input id="flsIdnput" style="display:none;" type="text" size="15" name="flsIdDate" /> 
                                </td>
                            </tr>
                            <tr id="flsWhse">
                                <td>FLS Warehouse ID</td>
                                <td id="flsWhseStatus"></td>
                                <td>
                                    <div id="flsWhseDate"></div>
                                    <input id="flsWhseInput" style="display:none;" type="text" size="15" name="FLSWhseDate" /> 
                                </td>
                            </tr>           
                            <tr id="flsCSP">
                                <td><a class="certForms"  href="javascript:void(0);" rel="/techs/Training/FLS/flsCertTest.php">FLS Computer Security Policy</a></td>
                                <td id="flsCSPStatus"></td>
                                <td>
                                    <div id="flsCSPDate"></div>
                                    <input id="flsCSPInput" style="display:none;" type="text" size="15" name="FLSCSP_RecDate" /> 
                                </td>
                            </tr>
<?//13624 end?>                                           
                            <tr id="hallmarkCert">
                                <td><a class="certForms"  href="javascript:void(0);" rel="/techs/Training/FLS/Hallmark/Hallmark_Training.php">Hallmark POS Certification</a></td>
                                <td id="hallmarkCertStatus">
                                    <span id="hallmarkCertNumDisplay"></span>
                                    <input style="display:none;" id="hallmarkCertNumInput" type="text" size=10 name="Hallmark_Cert_Num" />
                                </td>
                                <td>
                                    <span id="hallmarkCertDateDisplay"></span>
                                    <input id="hallmarkCertDateInput" style="display:none;" type="text" size=15 name="Hallmark_Cert_Date" />
                                </td>
                            </tr>
                            <tr id="starbucksCert">
                                <td><a class="certForms"  href="javascript:void(0);" rel="/techs/Training/FLS/Starbucks/Starbucks_Training.php">Starbucks Certification</a></td>
                                <td id="starbucksCertStatus">
                                    <span id="starbucksCertNumDisplay"></span>
                                    <input style="display:none;" id="starbucksCertNumInput" type="text" size=10 name="Starbucks_Cert_Num" />
                                </td>
                                <td>
                                    <span id="starbucksCertDateDisplay"></span>
                                    <input id="starbucksCertDateInput" style="display:none;" type="text" size=15 name="Hallmark_Cert_Date" />
                                </td>
                            </tr>
                            <tr id="mauricesCert">
                                <td><a class="certForms"  href="javascript:void(0);" rel="/techs/Training/FLS/Maurices/Maurices_Training.php">Maurices Certification</a></td>
                                <td id="mauricesCertStatus">
                                    <span id="mauricesCertNumDisplay"></span>
                                    <input style="display:none;" id="mauricesCertNumInput" type="text" size=10 name="Maurices_Cert_Num" />
                                </td>
                                <td>
                                    <span id="mauricesCertDateDisplay"></span>
                                    <input id="mauricesCertDateInput" style="display:none;" type="text" size=15 name="Hallmark_Cert_Date" />
                                </td>
                            </tr>

                            <tr id="hpCert">
                                <td><a class="certForms"  href="javascript:void(0);" rel="/techs/ajax/lightboxPages/HP_Cert.php">HP Certification</a></td>
                                <td id="hpCertStatus">
                                    <span id="hpCertNumDisplay"></span>
                                    <input style="display:none;" id="hpCertNumInput" type="text" size=10 name="HP_CertNum" />
                                </td>
                                <td>
                                    <span id="hpCertDateDisplay"></span>
                                    <input id="hpCertDateInput" style="display:none;" type="text" size=15 name="Hallmark_Cert_Date" />
                                </td>
                            </tr>					
                            <tr id="electroMechCert">
                                <td><a class="certForms"  href="javascript:void(0);" rel="/techs/Training/ServRight/electroMechTest.php">Electro-Mechanical Certification</a></td>
                                <td id="electroMechCertStatus">
                                    <span id="electroMechCertNumDisplay"></span>
                                    <input style="display:none;" id="electroMechCertNumInput" type="text" size=10 name="Electro_Mechanical_Cert_Num" />
                                </td>
                                <td>
                                    <span id="electroMechCertDateDisplay"></span>
                                    <input id="electroMechCertDateInput" style="display:none;" type="text" size=15 name="Hallmark_Cert_Date" />
                                </td>
                            </tr>
                            <tr id="CopierSkillsAssessment">
                                <td style="text-align:left;"><a href="javascript:;" techid="" class="cmdCopiesSkillViewTech">Copier Skills Assessment</a></td>
                                <td id="CopierSkillsAssessmentStatus">
                                    <span id="CopierSkillsAssessmentDisplay"></span>
                                </td>
                                <td>
                                    <span id="CopierSkillsAssessmentDateDisplay"></span>
                                </td>
                            </tr>
                            <script>
                                    var _copier_skills_assessment = copier_skills_assessment.CreateObject({
                                    id:'_copier_skills_assessment' 
                                    });
                            </script>
                        </tbody>
                    </form>
                </table>
            </dd>
            <dt class="round sectionHeading"><a class="title" title="Expand Section"></a>7. Industry Certifications</dt>
            <dd>
                <br />
                <table id='myCertificationsHeader'>
                    <tr>
                        <td colspan=2>
                            <div class="round" id="subDivider">
                                <div style="width: 200px; text-align: left; float:left; margin-left: 5px;">My Certifications</div>								
                                <div class="editSection" style="float:right; padding-right: 5px;"> 
                                    <a href="javascript:void(0);" class="updateButton" id="myCertificationsEditLink" onclick="editSection('myCertifications');" title="Edit">Edit</a>
                                </div>
                                <div class="cancelSaveSection" style="float:right; display:none;">
                                    <img class="cancelUpdateButton" id="myCertificationscancelLink" onclick="cancelSection('myCertifications');" title="Cancel" src="/widgets/images/button_x.png">

                                    <img class="updateButton" onclick="_GXEMCCert.parserDataEMC();saveAll();" title="Save" src="/widgets/images/button_save.png">
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr><td><br /></td></tr>
                </table>

                <table id='myCertifications' class='tablesorter' style="margin-left: 50px; margin-right: auto; line-height: 20px; ">
                    <form id="myCerts" name="myCerts">
                        <input type="hidden" name="techID" id="basicRegTechID" value="<?= $_SESSION['TechID']; ?>" />
                        <thead>
                            <tr>
                                <th id="thfir">Industry Certifications</th>
                                <th id="thsec">Cert #</th>
                                <th>Date Completed</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $certsToRender = array(2);
                            $certSearch = new Core_TechCertifications;
                            $certMapping = $certSearch->getMapping();
                            foreach ($certsToRender as $certId)
                            {
                                if (!array_key_exists($certId, $certMapping))
                                    continue;
                                $cert = $certMapping[$certId];
                                $fieldName = $cert['name'];
                                //DeVry
    $textAlign = "left";
                                //if($fieldName=="DeVry") $textAlign="center";
                                ?>
                                <tr id="<?= $fieldName ?>Cert">
                                    <td><?= $cert['label'] ?></td>
                                    <td id="<?= $fieldName ?>CertStatus" <?php echo "style=\"text-align: $textAlign\"" ?>>
                                        <span id="<?= $fieldName ?>CertNumDisplay"></span>
                                    </td>
                                    <td>
                                        <span id="<?= $fieldName ?>CertDateDisplay"></span>
                                    </td>
                                </tr>					
    <?php
}
?>
<!--                                <tr id="blueRibbon">
                                <td>
                                    <a class="blueRibbon"  href="javascript:void(0);">Blue Ribbon Technician</a>
                                </td>
                                <td id="blueRibbonStatus"></td>
                                <td>
                                    <div id="blueRibbonDate"></div>
                                </td>
                            </tr>
                            <tr id="bootCamp">
                                <td><a class="bootCamp"  href="javascript:void(0);">Boot Camp Certified</a></td>
                                <td id="bootCampStatus"></td>
                                <td>
                                    <div id="bootCampDate"></div>
                                </td>
                            </tr>-->
                            <tr id="mcseCert">
                                <td>MCSE</td>
                                <td id="mcseCertStatus"  align="left">
                                    <span id="mcseCertNumDisplay"></span>
                                    <input style="display:none;" id="mcseCertNumInput" type="text" size=10 name="MCSE_Cert_Number" />
                                </td>
                                <td>
                                    <span id="mcseCertDateDisplay"></span>
                                    <input id="mcseCertDateInput" style="display:none;" type="text" size=15 name="MCSE_Date" />
                                </td>
                            </tr>
                            <tr id="MCSACert">
                                <td>MCSA</td>
                                <td id="MCSACertStatus"  align="left">
                                    <input type="hidden" value="1" name="MCSA_CertChkInput" id="MCSA_CertChkInput"/>
                                           
                                    <span id="MCSACertNumDisplay"></span>
                                    <input style="display:none;" id="MCSACertNumInput" type="text" size=10 name="MCSA_Cert_Number" />
                                </td>
                                <td>
                                    <span id="MCSACertDateDisplay"></span>
                                    <input id="MCSACertDateInput" style="display:none;" type="text" size=15 name="MCSA_Date" />
                                </td>
                            </tr>
                            <tr id="MCPCert">
                                <td>MCP</td>
                                <td id="MCPCertStatus"  align="left">
                                    <input type="hidden" value="1" name="MCP_CertChkInput" id="MCP_CertChkInput"/>
                                    <span id="MCPCertNumDisplay"></span>
                                    <input style="display:none;" id="MCPCertNumInput" type="text" size=10 name="MCP_Cert_Number" />
                                </td>
                                <td>
                                    <span id="MCPCertDateDisplay"></span>
                                    <input id="MCPCertDateInput" style="display:none;" type="text" size=15 name="MCP_Date" />
                                </td>
                            </tr>
                            
                            <tr id="ccnaCert">
                                <td>CCNA</td>
                                <td id="ccnaCertStatus" align="left">
                                    <span id="ccnaCertNumDisplay"></span>
                                    <input style="display:none;" id="ccnaCertNumInput" type="text" size=10 name="CCNA_Cert_Number" />
                                </td>
                                <td>
                                    <span id="ccnaCertDateDisplay"></span>
                                    <input id="ccnaCertDateInput" style="display:none;" type="text" size=15 name="CCNA_Date" />
                                </td>
                            </tr>
                            <tr id="compTiaCert">
                                <td>CompTIA Registration #</td>
                                <td id="compTiaCertStatus" align="left">
                                    <span id="compTiaCertCheckDisplay"></span><span id="compTiaCertNumDisplay"></span>
                                    <input style="display:none;" id="compTiaCertNumInput" type="text" size=10 name="CompTIA" />
                                </td>
                                <td>
                                    <span id="compTiaCertDateDisplay"></span>
                                    <input id="compTiaCertDateInput" style="display:none;" type="text" size=15 name="CompTIA_Date" />
                                </td>
                            </tr>
                            <tr id="CompTIA_A_PLUSCert">
                                <td>CompTIA A+</td>
                                <td id="CompTIA_A_PLUSCertStatus" align="left">
                                    <span id="aPlusCertCheckDisplay"></span><span id="aPlusCertNumDisplay"></span>
                                    <input id="APlus" onclick="jQuery(this).is(':checked')?jQuery('#CompTIA_A_PLUSCertStatusChk').val(1):jQuery('#CompTIA_A_PLUSCertStatusChk').val(0);" type="checkbox" name="APlus" style="display:none;" value="1" />
<!--                                    <span id="CompTIA_A_PLUSCertNumDisplay"></span>-->
                                    <input style="display:none;" id="CompTIA_A_PLUSCertNumInput"  type="text" size=10 name="certificationsNum_45" />
                                    <input type="hidden" value="45" name="certifications[]"  />
                                    <input type="hidden" name="certificationsStatus_45" size="10" id="CompTIA_A_PLUSCertStatusChk" value="1">
                                </td>
                                <td>

                                </td>
                            </tr>
                            
                            <tr id="CompTIA_PDI_PLUSCert">
                                <td>CompTIA PDI+</td>
                                <td id="CompTIA_PDI_PLUSCertStatus" align="left">
                                    <span id="CompTIA_PDI_PLUSCertNumDisplay"></span>
                                    <input style="display:none;" id="CompTIA_PDI_PLUSCertNumInput" type="text" size=10 name="certificationsNum_36" />
                                    <input type="hidden" value="36" name="certifications[]"  />
                                </td>
                                <td>
                                    <span id="CompTIA_PDI_PLUSCertDateDisplay"></span>
                                    <input id="CompTIA_PDI_PLUSCertDateInput" style="display:none;" type="text" size=15 name="certificationsDate_36" />
                                </td>
                            </tr>
                            <tr id="CompTIA_Server_PLUSCert">
                                <td>CompTIA Server+</td>
                                <td id="CompTIA_Server_PLUSCertStatus" align="left">
                                    <span id="CompTIA_Server_PLUSCertNumDisplay"></span>
                                    <input style="display:none;" id="CompTIA_Server_PLUSCertNumInput" type="text" size=10 name="certificationsNum_37" />
                                    <input type="hidden" value="37" name="certifications[]"  />
                                </td>
                                <td>
                                    <span id="CompTIA_Server_PLUSCertDateDisplay"></span>
                                    <input id="CompTIA_Server_PLUSCertDateInput" style="display:none;" type="text" size=15 name="certificationsDate_37" />
                                </td>
                            </tr>
                            <tr id="CompTIA_Network_PLUSCert">
                                <td>CompTIA Network+</td>
                                <td id="CompTIA_Network_PLUSCertStatus" align="left">
                                    <span id="CompTIA_Network_PLUSCertNumDisplay"></span>
                                    <input style="display:none;" id="CompTIA_Network_PLUSCertNumInput" type="text" size=10 name="certificationsNum_38" />
                                    <input type="hidden" value="38" name="certifications[]"  />
                                </td>
                                <td>
                                    <span id="CompTIA_Network_PLUSCertDateDisplay"></span>
                                    <input id="CompTIA_Network_PLUSCertDateInput" style="display:none;" type="text" size=15 name="certificationsDate_38" />
                                </td>
                            </tr>
                            
                           
                            
                            <tr id="dellDcseCert">
                                <td>Dell DCSE</td>
                                <td id="dellDcseCertStatus" align="left">
                                    <span id="dellDcseCertCheckDisplay"></span><span id="dellDcseCertNumDisplay"></span>
                                    <input style="display:none;" id="dellDcseCertNumInput" type="text" size=10 name="DELL_DCSE_Cert_Number" />
                                </td>
                                <td>
                                    <span id="dellDcseCertDateDisplay"></span>
                                    <input id="dellDcseCertDateInput" style="display:none;" type="text" size=15 name="DELL_DCSE_Date" />
                                </td>
                            </tr>
                            <tr id="bicsiCert">
                                <td>BICSI Certification</td>
                                <td id="bicsiCertStatus" align="left">
                                    <span id="bicsiCertCheckDisplay"></span><span id="bicsiCertNumDisplay"></span>
                                    <input style="display:none;" id="bicsiCertNumInput" type="text" size=10 name="BICSI_Cert_Number" />
                                </td>
                                <td>
                                    <span id="bicsiCertDateDisplay"></span>
                                    <input id="bicsiCertDateInput" style="display:none;" type="text" size=15 name="BICSI_Date" />
                                </td>
                            </tr>
                            <tr id="EMCCert">
                                <td style="vertical-align: top;" ><a target="_blank" href="javascript:;" id="cmdEMCCert">EMC Certification</a></td>
                                <td id="EMCCertStatus" align="left" style="padding-left:3px;">
                                    <span id="EMCCertDisplay"></span>
                                    <span style="display:none;" id="EMCCertDisplayCbx">
                                        <select id="cbxEMC"></select>
                                    </span>
                                </td>
                                <td style="" align="left" valign="top">
                                    <span id="EMCCertDateDisplay"></span>
                                    <input type="hidden" value="" name="EMC" id="EMC" />
                                </td>
                            </tr>
                        <script>
                              //jQuery(window).ready(function(){
                                  var _GXEMCCert = GXEMCCert.CreateObject({id:'_GXEMCCert',today:'<?= date('m/d/Y') ?>'}); 
                              //});
                        </script>
                        </tbody>
                    </form>
                </table>
            </dd>

        </dl>
    </div>

</div>
<?php require ("../footer.php"); ?>
