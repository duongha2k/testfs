<?php $page = techs; ?>
<?php $option = more; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>

<!-- Add Content Here -->

<br /><br />

<div id="leftcontent">

<table border="1" cellpadding="5" cellspacing="0" style="border: medium solid #00000;">
	<tr>
<td width="100%">
<h3>FLS (First Line Support)</h3>
<p><b>The FLS Program/Tech Opportunity</b></p>
<p>
The FLS program/Tech opportunity is focused around running service & project calls performing work on hardware and software in the retail and grocery environments for one of our clients. Getting involved with FLS as a Technician requires some technical knowledge, as you will be replacing defective parts dealing with primarily POS equipment in retail stores (keyboards, check readers, monitors, cash registers/drawers, scanners, etc).
 </p>
 <p>
In order to be activated as a Tech with FLS (First Line Support), you will first need to go through the required online training and submit the test with a passing score. Get more information regarding the FLS program and/or begin online training here: <a href="http://www.flsupport.com/2.html" target="_blank">Click here for FLS Training Process</a>
</p>

</td>
	</tr>
</table>
	
</div>

<div id="rightcontent">

<br /><br />

<div align="center">
	<table border="0" cellpadding="0" cellspacing="0">
		<tr>
	<td align="center">
	<h2><a href="techFLS.php">Click here</a> to update your profile<br />
	with information for FLS</h2>
	
	<br /><br />
	
	<!--
	<ul id="defaultMenu">
	<li><a href="techFLS.php">FLS</a></li>
	</ul>
	-->
	</td>
		</tr>
	</table>
	</div>
	
<br /><br /><br />


</div>

<!--- End Content --->
<?php require ("../footer.php"); ?>
		
