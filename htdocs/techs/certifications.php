<?php $page = 'techs'; ?>
<?php $option = 'certifications'; ?>
<?php $selected = "technical" ?>
<?php
	require_once("../headerStartSession.php");
	require_once("../library/FSSession.php");
	FSSession::validateSessionParams(array("TechID" => "TechID"));
?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->

<br /><br />

<script type="text/javascript">

function compTIAField() {
	return $("input#EditRecordCompTIA");
}

function APlusField() {
	return $("input#EditRecordAPlus");
}

function setCompTIAField() {
	toggle = (APlusField().attr("checked") != true);
	compTIAField().attr("disabled",toggle);
}

$("document").ready(function() {
	setCompTIAField();
	
	APlusField().change(function() {
		setCompTIAField();	
		compTIAField().focus();
	});
	
});

</script>

<div align="center">
<!-- Old AppKey: J0C1B3H4H2J0C1B3H4H2 (GB 2008-09-10) -->
<div id="cb193b00000f7a80d015094475b16a"><a href="http://www.caspio.com" target="_blank">Online Database</a> by Caspio</div>
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b00000f7a80d015094475b16a","https:");}catch(v_e){;}</script>
<div id="cxkg"><a href="https://bridge.caspio.net/dp.asp?AppKey=193b00000f7a80d015094475b16a">Click here</a> to load this Caspio <a href="http://www.caspio.com" title="Online Database">Online Database</a>.</div>

<script type="text/javascript" language="javascript">
document.forms.caspioform.EditRecordcertComplete.checked = true;
</script>

</div>

<!--- End Content --->
<?php require ("../footer.php"); ?>