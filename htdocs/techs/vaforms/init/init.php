<?php


$config = simplexml_load_file('../config/config.xml');
if (!is_dir($config->SetaPDFPath.'/FormFiller/font/')){
    echo "Can't find SetaPDF library. Please specify path to it in 'config/config.xml' file";
    exit;
}
define ('HOME_PATH',                    '../');
define ('DOCS_DIR',                     HOME_PATH.'docs/');
define ('SAVE_DIR',                     HOME_PATH.'htdocs/result/');
define ('TEMPLATES_DIR',                HOME_PATH.'templates/');
define ('LIB_DIR',                      HOME_PATH.'lib/');
define ('MESSAGES_DIR',                 HOME_PATH.'messages/');
define ('SETAPDF_FORMFILLER_FONT_PATH', $config->SetaPDFPath.'/FormFiller/font/');
define ('SMARTY_COMPILE_DIR',           HOME_PATH.'var/templates_compiled');
define ('SMARTY_TEMPLATE_DIR',          HOME_PATH.'templates');


ini_set('session.save_path', HOME_PATH .'/sessions/');
ini_set('session.gc_maxlifetime', 86400);
session_start();

set_include_path(get_include_path() . PATH_SEPARATOR . $config->SetaPDFPath);
require_once 'FormFiller/SetaPDF_FormFiller.php'; 
require_once LIB_DIR.'smarty/Smarty.class.php';
require_once LIB_DIR.'Warecorp/Form.php';
//require_once LIB_DIR.'Warecorp/ErrorMessages.php';
