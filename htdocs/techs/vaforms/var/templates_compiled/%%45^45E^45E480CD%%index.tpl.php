<?php /* Smarty version 2.6.9, created on 2009-02-04 10:22:49
         compiled from index.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'textfield', 'index.tpl', 87, false),array('function', 'date', 'index.tpl', 95, false),array('function', 'states', 'index.tpl', 103, false),array('function', 'countries', 'index.tpl', 105, false),array('function', 'monthyear', 'index.tpl', 121, false),array('function', 'radiohorisontal', 'index.tpl', 146, false),array('function', 'radiovertical', 'index.tpl', 152, false),array('function', 'code13', 'index.tpl', 454, false),array('function', 'code19', 'index.tpl', 565, false),array('function', 'code17', 'index.tpl', 1325, false),array('function', 'status', 'index.tpl', 1331, false),array('function', 'textarea', 'index.tpl', 1390, false),)), $this); ?>
<head>
  <title>Varforms</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="css/main.css" />
</head>

<?php echo '
<script language="JavaScript" src="js/calendar/calendar_us.js"></script>
<script language="JavaScript" src="js/main.js"></script>


<link rel="stylesheet" href="js/calendar/calendar.css">
'; ?>

<body><div id="wrapper">
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "errors.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php if ($this->_tpl_vars['showPDF']): ?>
    <div class="doc_pdf">
		<h4>Your PDF forms have been generated</h4> 
        <p>
            
            To download and save the forms on your computer right-click on the links below and choose "Save Target As..." (Internet Explorer) or "Save Link As..." (FireFox).        
        </p>
		<p>
			<a href="getPDF.php?file=SF85">SF85.pdf</a><br /> Standart form 85
		</p>
		<p>
			<a href="getPDF.php?file=of0306">of0306.pdf</a><br /> Declaration for Federal Employment
		</p>
		<p><?php if ($_POST['add8'] || $_POST['add9'] || $_POST['add10']): ?>
			<a href="getPDF.php?file=SF86A">SF86A.pdf</a><br /> Continuation sheet
		<?php endif; ?>
		</p>
	</div>
<?php endif; ?>
<!--<a href="index.php">Clear form</a><br>
<a href="index.php?fill=1">Fill form</a><br> -->
  <form class="form-basic" method="post" id="form2" name="form2" action="vaforms.php">
    <fieldset><legend></legend>
      <div class="note">
        <p  class="note2_1a"><b>Purpose of this Form</b></p> 
        <p class="note2_1a">The U.S. Government conducts background investigations to 
          establish that applicants or incumbents either employed by the Government or working for the 
          Government under contract, are suitable for the job. Information from this form is used primarily as the basis for 
          this investigation. Complete this form only after a conditional offer of employment has been made.</p>
        <p  class="note2_1a">Giving us the information we ask for is voluntary. However, we may not be able to complete your investigation, or complete
          it in a timely manner, if you don`t give us each item of information we request. This may affect your placement or employment prospects.</p>
        <p  class="note2_1a"><b>Authority to Request this Information</b></p> 
        <p  class="note2_1a">The U.S. Government is authorized to ask for this information under Executive Order 10577, 
          sections 3301 and 3302 of title 5, U.S. Code; and parts 5, 731, and 736 of Title 5, Code of Federal Regulations.</p>
        <p  class="note2_1a">Your Social Security Number is needed to keep records accurate, because other people may have the same name and birth date. 
          Executive Order 9397 also asks Federal agencies to use this number to help identify individuals in agency records.</p>
        <p  class="note2_1a"><b>The Investigative Process</b></p> 
        <p class="note2_1a">Background investigations are conducted using your responses on this form and on your Declaration
          for Federal Employment (OF 306) to develop information to show whether you are reliable, trustworthy, and of good conduct and character. 
          Your current employer must be contacted as part of the investigation, even if you have previously indicated on applications or other 
          forms that you do not want this.</p>
        <p  class="note2_1a"><b>Instructions for Completing this Form</b></p>
        <p  class="note2_1a">1. Follow the instructions given to you by the person who gave you the form and any other clarifying instructions furnished 
          by that person to assist you in completion of the form. Find out how many copies of the form you are to turn in. You must sign and 
          date, in black ink, the original and each copy you submit.</p>
        <p  class="note2_1a">2. Type or legibly print your answers in black ink (if your form is not legible, it will not be accepted). You may also be asked
          to submit your form in an approved electronic format.</p>
        <p  class="note2_1a">3. All questions on this form must be answered. If no response is necessary or applicable, indicate this on the form
          (for example, enter "None" or "N/A"). If you find that you cannot report an exact date, approximate or estimate the date to
          the best of your ability and indicate this by marking "APPROX." or "EST."</p>
        <p  class="note2_1a">4. Any changes that you make to this form after you sign it must be initialed and dated by you. Under certain limited
          circumstances, agencies may modify the form consistent with your intent.</p>
        <p  class="note2_1a">5. You must use the State codes (abbreviations) listed on the back of this page when you fill out this form. 
          Do not abbreviate the names of cities or foreign countries.</p>
        <p  class="note2_1a">6. The 5-digit postal ZIP codes are needed to speed the processing of your investigation. The office that provided 
          the form will assist you in completing the ZIP codes.</p>
        <p  class="note2_1a">7. All telephone numbers must include area codes.</p> 
        <p  class="note2_1a">8. All dates provided on this form must be in Month/Day/Year or 
          Month/Year format. Use numbers (1-12) to indicate months. For example, June 10, 1978, should be shown as 6/10/78.</p>
        <p  class="note2_1a">9. Whenever "City (Country)" is shown in an address block, also provide in that block the name of the country when 
          the address is outside the United States.</p>
      </div>
        </fieldset>
<!-- ----------------------------------------------------    1     ------------------------------------------------ -->
  <fieldset><legend><span>1.</span> FULL NAME</legend>
    <div class="note">
      <p>If you have only initials in your name, use them and state (IO). If you have no middle name, enter "NMN".</p>
      <p>If you are a "Jr.," "Sr.," "II," etc., enter this in the box after your middle name.</p>
    </div>
    <div class="set-default clearer">
      <?php echo smarty_function_textfield(array('label' => 'Lastname','name' => 'LName','class' => '20'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'First Name','name' => 'FName','class' => '20_2a'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'Middle Name','name' => 'MName','class' => '20_2a'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "Jr., II, etc.",'name' => 'NameSuffix','class' => '20_2a'), $this);?>

    </div>
  </fieldset>     
<!-- ----------------------------------------------------    2     ------------------------------------------------ -->
  <fieldset><legend><span>2.</span><span class="stext <?php if ($this->_tpl_vars['errorMessages']['DOB']): ?>item-error<?php endif; ?>"> DATE OF BIRTH</span></legend>
        <?php echo smarty_function_date(array('onchange' => "selService()",'name' => 'DOB'), $this);?>

      </fieldset>
<!-- ----------------------------------------------------    3     ------------------------------------------------ -->
      <fieldset><legend><span>3.</span> PLACE OF BIRTH</legend>
        <div class="set-default clearer">
          <?php echo smarty_function_textfield(array('label' => 'City','name' => 'CityOfBirth','class' => '20'), $this);?>

          <div id="pobst" <?php if ($_POST['CountryOfBirth'] != 'United States'): ?> style="display:none;"<?php endif; ?> >
            <?php echo smarty_function_textfield(array('label' => 'County','name' => 'CountyOfBirth','class' => '20_2a'), $this);?>

            <?php echo smarty_function_states(array('name' => 'StateOfBirth','label' => 'State','class' => '8_1a'), $this);?>

          </div>
          <?php echo smarty_function_countries(array('name' => 'CountryOfBirth','onchange' => "pob()",'label' => 'Country'), $this);?>

        </div>
      </fieldset>
<!-- ----------------------------------------------------    4     ------------------------------------------------ -->
      <fieldset><legend><span>4.</span><span class="stext <?php if ($this->_tpl_vars['errorMessages']['SSN']): ?> item-error <?php endif; ?>"> SOCIAL SECURITY</span></legend>
            <?php echo smarty_function_textfield(array('label' => "",'name' => 'SSN','class' => '20'), $this);?>

          </fieldset>
<!-- ----------------------------------------------------    5     ------------------------------------------------ -->
          <fieldset><legend><span>5.</span> OTHER NAMES USED</legend>
            <div class="note">
              <p>Give other names you used and the period of time you used them (for example: your maiden name, name(s) by a former marriage, former name(s), alias(es), or nickname(s)). If the other name is your maiden name, put "nee" in front of it.</p>
            </div>
            <ol>
              <li class="set-default clearer">
                <div class="item-number fl">#1</div>
                <?php echo smarty_function_textfield(array('label' => 'Name','name' => 'OtherName1','class' => '29'), $this);?>
  
                <?php echo smarty_function_monthyear(array('name' => 'DateFrom1','label' => 'From'), $this);?>

                <?php echo smarty_function_monthyear(array('name' => 'DateTo1','label' => 'To'), $this);?>

              </li>
              <li class="set-default clearer">
                <div class="item-number fl">#2</div>
                <?php echo smarty_function_textfield(array('label' => 'Name','name' => 'OtherName2','class' => '29'), $this);?>
  
                <?php echo smarty_function_monthyear(array('name' => 'DateFrom2','label' => 'From'), $this);?>

                <?php echo smarty_function_monthyear(array('name' => 'DateTo2','label' => 'To'), $this);?>

              </li>
              <li class="set-default clearer">
                <div class="item-number fl">#3</div>
                <?php echo smarty_function_textfield(array('label' => 'Name','name' => 'OtherName3','class' => '29'), $this);?>
  
                <?php echo smarty_function_monthyear(array('name' => 'DateFrom3','label' => 'From'), $this);?>

                <?php echo smarty_function_monthyear(array('name' => 'DateTo3','label' => 'To'), $this);?>

              </li>
              <li class="set-default clearer">
                <div class="item-number fl">#4</div>
                <?php echo smarty_function_textfield(array('label' => 'Name','name' => 'OtherName4','class' => '29'), $this);?>
  
                <?php echo smarty_function_monthyear(array('name' => 'DateFrom4','label' => 'From'), $this);?>

                <?php echo smarty_function_monthyear(array('name' => 'DateTo4','label' => 'To'), $this);?>

              </li>
            </ol>
          </fieldset>     
<!-- ----------------------------------------------------    6     ------------------------------------------------ -->
          <fieldset><legend><span>6.</span><span class="stext <?php if ($this->_tpl_vars['errorMessages']['sex']): ?>item-error<?php endif; ?>"> SEX</span></legend>
                <?php echo smarty_function_radiohorisontal(array('name' => 'sex','onclick' => "selService()",'labels' => "Male|Female",'values' => "male|female"), $this);?>

              </fieldset>
<!-- ----------------------------------------------------    7     ------------------------------------------------ -->
              <fieldset><legend><span>7.</span> CITIZENSHIP</legend>
                <ol class="set-nolabel list-lalpha">
                  <li><span class="stext <?php if ($this->_tpl_vars['errorMessages']['citizen']): ?>item-error<?php endif; ?>">Mark the box below that reflects your current citizenship status, and follow its instructions.</span>
                        <?php echo smarty_function_radiovertical(array('onclick' => "showCitizen()",'name' => 'citizen','labels' => " I am a U.S. citizen or national by birth in the U.S. or U.S. territory/possession.|
                        I am a U.S. citizen, but I was NOT born in the U.S. |
                        I am not a U.S. citizen. ",'values' => "USCitizen|NotBornCitizen|NotAUSCitizen"), $this);?>

						<br clear="all" />
                      </li>
                      <li id="citizenb" <?php if (! $_POST['citizen']): ?> style="display: none"; <?php endif; ?>>
                        <div class="list-lalpha-inner"><?php echo smarty_function_textfield(array('label' => 'Your Mothers Maiden Name','name' => 'MothersMaidenName','class' => '20n'), $this);?>
</div>
                        <br clear="all" />  
                      </li>
                      <li id="citizenc" <?php if ($_POST['citizen'] != 'NotBornCitizen'): ?> style="display: none"; <?php endif; ?>><div class="list-lalpha-inner">
                          <strong>United States Citizenship</strong>
                          <p class="note">Give other names you used and the period of time you used them (for example: yourmaiden name, name(s)byaformermarriage,formername(s),alias(es), or nickname(s)). If the other name is your maiden name, put "nee" in front of it.</p>
                          <p class="note"><strong>Naturalization Certificate</strong></p>
                          <p class="note"><em>Where were you naturalized?</em></p>
                          <div class="set-default clearer">
                            <?php echo smarty_function_textfield(array('label' => 'Court','name' => 'CourtOfNaturaliztion','class' => '15'), $this);?>

                            <?php echo smarty_function_textfield(array('label' => 'City','name' => 'CityOfNaturalization','class' => '15_1a'), $this);?>

                            <?php echo smarty_function_states(array('name' => 'StateOfNaturalization','label' => 'State','class' => '8_1a'), $this);?>

                            <?php echo smarty_function_textfield(array('label' => 'Certificate Number','name' => 'NaturalizationCertificate','class' => '12_2'), $this);?>
                       
                            <div class="item-date fl no-padtop">
                              <?php echo smarty_function_date(array('label' => "Month/Day/Year Issued",'name' => 'DateOfIssue1'), $this);?>

                            </div>
                          </div>
                          <p class="note2"><strong>Citizenship Certificate</strong></p>
                          <p class="note"><em>Where was the certificate issued?</em></p>
                          <div class="set-default clearer">
                            <?php echo smarty_function_textfield(array('label' => 'City','name' => 'CityOfCitizenshipCertificate','class' => '32'), $this);?>
    
                            <?php echo smarty_function_states(array('name' => 'StateOfCitizenshipCertificate','label' => 'State','class' => '8_1a'), $this);?>

                            <?php echo smarty_function_textfield(array('label' => 'Certificate Number','name' => 'CitizenshipCertificate','class' => '12_2'), $this);?>

                            <div class="item-date fl no-padtop">
                              <?php echo smarty_function_date(array('label' => "Month/Day/Year Issued",'name' => 'DateOfIssue2'), $this);?>

                            </div>
                          </div>
                          <p class="note2_3a"><strong>State Department Form 240 - Report of Birth Abroad of a Citizen of the United States</strong></p>
                          <p class="note"><em>Give the date the form was prepared and give an explanation if needed</em></p>
                          <div class="set-default clearer">
                            <div class="fl no-padtop">
                              <?php echo smarty_function_date(array('label' => "Month/Day/Year Issued",'name' => 'DateOfFormPrepared'), $this);?>

                            </div>
                            <?php echo smarty_function_textfield(array('label' => 'Explanation','name' => 'Explenation1','class' => '51a_13'), $this);?>

                          </div>
                          <p class="note2_4a"><strong>U.S. Passport.</strong></p>
                          <p class="note">This may be either a current or previous U.S. Passport.</p>
                          <div class="set-default clearer">
                            <?php echo smarty_function_textfield(array('label' => 'Passport number','name' => 'Passportn','class' => '12'), $this);?>

                            <div class="fl left_marg2a no-padtop">
                              <?php echo smarty_function_date(array('label' => "Month/Day/Year Issued",'name' => 'DateOfIssue3'), $this);?>

                            </div>
                          </div>
                        </div></li>
                      <li id="citizend" <?php if ($_POST['citizen'] != 'USCitizen' && $_POST['citizen'] != 'NotBornCitizen'): ?> style="display: none"; <?php endif; ?>> <div class="list-lalpha-inner">
                          <strong>Dual Citizenship</strong>
                          <p class="note">If you are (or were) a dual citizen of the United States and another country, provide the name of that country in the space </p>
                          <div class="set-default clearer">
                            <?php echo smarty_function_textfield(array('label' => 'Country','name' => 'CountryOfDualCitizenship','class' => '35'), $this);?>

                            <div>
                              <div></li>
                              <li id="citizene" <?php if ($_POST['citizen'] != 'NotAUSCitizen'): ?> style="display: none"; <?php endif; ?>> <div class="list-lalpha-inner">
                                  <strong>Alien</strong>
                                  <p class="note">If you are an alien, provide the following information:</p>
                                  <p class="note"><strong>Place You Entered the United States</strong></p>
                                  <div class="set-default clearer">
                                    <?php echo smarty_function_textfield(array('label' => 'City','name' => 'CityOfUSEntrance','class' => '30'), $this);?>

                                    <?php echo smarty_function_states(array('name' => 'StateOfUSEnterance','label' => 'State','class' => '8_2a'), $this);?>

                                    <div class="fl left_marg1a no-padtop">
                                      <?php echo smarty_function_date(array('label' => "Month/Day/Year Issued",'name' => 'DateOfUSEnterance'), $this);?>

                                    </div>
                                    <div><br clear="all" />
                                      <div class="set-default clearer">
                                        <?php echo smarty_function_textfield(array('label' => 'Alien registration number','name' => 'AlienRegistrationNumber','class' => '17'), $this);?>

                                        <?php echo smarty_function_textfield(array('label' => "Country(ies) of citizenship",'name' => 'CountryOfCitizenship','class' => '54_2a'), $this);?>

                                        <div>
                                          <div></li>
                                        </ol>
        </fieldset>
<br>
<br>

<!-- ----------------------------------------------------    8     ------------------------------------------------ -->
  <fieldset><legend><span>8.</span> WHERE YOU HAVE LIVED</legend>
    <p class="note">List the places where you have lived, beginning with the most recent (#1) and working back 5 years. All periods must be accounted for in your list. Be sure to indicate the actual physical location of your residence: do not use a post office box as an address, do not list a permanent address when you were actually living at a school address, etc. Be sure to specify your location as closely as possible: for example, do not list only your base or ship, list your barracks number or home port. You may omit temporary military duty locations under 90 days (list your permanent address instead), and you should use your APO/FPO address if you lived overseas.</p>
    <p class="note">For any address in the last 3 years, list a person who knew you at that address, and who preferably still lives in that area (do not list people for residences completely outside this 3-year period, and do not list your spouse, former spouses, or other relatives).</p>
    <div class="area">
      <div class="set-default clearer">
        <div class="item-number fl">#1</div>
        <?php echo smarty_function_monthyear(array('name' => 'DateFrom5','onchange' => "fillTo('DateFrom5', 'DateTo6')",'label' => 'From','class' => "10-45_1"), $this);?>

        <div class="present fl"><span class="stext2">To</span><br /><b>Present</b></div>
        <?php echo smarty_function_textfield(array('label' => 'Street Address','name' => 'StreetAddress1','class' => '20_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "Apt. #",'name' => 'Aptn1','class' => '4_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'CityCountry1','class' => '12_1'), $this);?>

        <?php echo smarty_function_states(array('name' => 'State1','label' => 'State','class' => '8_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'ZIpCode1','class' => '7_1'), $this);?>

      </div>
      <div class="set-default clearer left_marg2a">                    
        <?php echo smarty_function_textfield(array('label' => 'Name of Person Who Knows You','name' => 'NameOfPerson1','class' => '32'), $this);?>

        <?php echo smarty_function_textfield(array('label' => 'Street Address','name' => 'StreetAddressOfPerson1','class' => '20_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "Apt. #",'name' => 'AptnOfPerson1','class' => '4_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'CityCountryOfPerson1','class' => '12_1'), $this);?>

        <?php echo smarty_function_states(array('name' => 'StateOfPerson1','label' => 'State','class' => '8_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'ZIpCodeOfPerson1','class' => '7_1'), $this);?>

      </div>
    </div>
    <div class="set-default clearer">
      <div class="item-number fl">#2</div>
      <?php echo smarty_function_monthyear(array('name' => 'DateFrom6','onchange' => "fillTo('DateFrom6', 'DateTo7')",'label' => 'From','class' => "10-45_1"), $this);?>

      <div class="present fl"><span class="stext2">To</span><br /><p id="DateTo6" class="for_date_to"><?php if ($_POST['MonthDateFrom5'] && $_POST['YearDateFrom5']):  echo $_POST['MonthDateFrom5']; ?>
/<?php echo $_POST['YearDateFrom5'];  endif; ?></p></div>
      <?php echo smarty_function_textfield(array('label' => 'Street Address','name' => 'StreetAddress2','class' => '20_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "Apt. #",'name' => 'Aptn2','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'CityCountry2','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'State2','label' => 'State','class' => '8_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'ZIpCode2','class' => '7_1'), $this);?>

    </div>
    <div class="set-default clearer left_marg2a">                    
      <?php echo smarty_function_textfield(array('label' => 'Name of Person Who Knows You','name' => 'NameOfPerson2','class' => '32'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'Street Address','name' => 'StreetAddressOfPerson2','class' => '20_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "Apt. #",'name' => 'AptnOfPerson2','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'CityCountryOfPerson2','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'StateOfPerson2','label' => 'State','class' => '8_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'ZIpCodeOfPerson2','class' => '7_1'), $this);?>

    </div>
    <div class="area">
      <div class="set-default clearer">
        <div class="item-number fl">#3</div>
        <?php echo smarty_function_monthyear(array('name' => 'DateFrom7','onchange' => "fillTo('DateFrom7', 'DateTo8')",'label' => 'From','class' => "10-45_1"), $this);?>

        <div class="present fl"><span class="stext2">To</span><br /><p id="DateTo7" class="for_date_to"><?php if ($_POST['MonthDateFrom6'] && $_POST['YearDateFrom6']):  echo $_POST['MonthDateFrom6']; ?>
/<?php echo $_POST['YearDateFrom6'];  endif; ?></p></div>
        <?php echo smarty_function_textfield(array('label' => 'Street Address','name' => 'StreetAddress3','class' => '20_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "Apt. #",'name' => 'Aptn3','class' => '4_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'CityCountry3','class' => '12_1'), $this);?>

        <?php echo smarty_function_states(array('name' => 'State3','label' => 'State','class' => '8_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'ZIpCode3','class' => '7_1'), $this);?>

      </div>
      <div class="set-default clearer left_marg2a">                    
        <?php echo smarty_function_textfield(array('label' => 'Name of Person Who Knows You','name' => 'NameOfPerson3','class' => '32'), $this);?>

        <?php echo smarty_function_textfield(array('label' => 'Street Address','name' => 'StreetAddressOfPerson3','class' => '20_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "Apt. #",'name' => 'AptnOfPerson3','class' => '4_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'CityCountryOfPerson3','class' => '12_1'), $this);?>

        <?php echo smarty_function_states(array('name' => 'StateOfPerson3','label' => 'State','class' => '8_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'ZIpCodeOfPerson3','class' => '7_1'), $this);?>

      </div>
    </div>
    <div class="set-default clearer">
      <div class="item-number fl">#4</div>
      <?php echo smarty_function_monthyear(array('name' => 'DateFrom8','onchange' => "fillTo('DateFrom8', 'DateTo9')",'label' => 'From','class' => "10-45_1"), $this);?>

      <div class="present fl"><span class="stext2">To</span><br /><p id="DateTo8" class="for_date_to"><?php if ($_POST['MonthDateFrom7'] && $_POST['YearDateFrom7']):  echo $_POST['MonthDateFrom7']; ?>
/<?php echo $_POST['YearDateFrom7'];  endif; ?></p></div>
      <?php echo smarty_function_textfield(array('label' => 'Street Address','name' => 'StreetAddress4','class' => '20_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "Apt. #",'name' => 'Aptn4','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'CityCountry4','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'State4','label' => 'State','class' => '8_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'ZIpCode4','class' => '7_1'), $this);?>

    </div>
    <div class="set-default clearer left_marg2a">                    
      <?php echo smarty_function_textfield(array('label' => 'Name of Person Who Knows You','name' => 'NameOfPerson4','class' => '32'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'Street Address','name' => 'StreetAddressOfPerson4','class' => '20_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "Apt. #",'name' => 'AptnOfPerson4','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'CityCountryOfPerson4','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'StateOfPerson4','label' => 'State','class' => '8_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZIpCode','name' => 'ZIpCodeOfPerson4','class' => '7_1'), $this);?>

    </div>
    <div class="area">
      <div class="set-default clearer">
        <div class="item-number fl">#5</div>
        <?php echo smarty_function_monthyear(array('name' => 'DateFrom9','onchange' => "fillTo('DateFrom9', 'aDateTo5')",'label' => 'From','class' => "10-45_1"), $this);?>

        <div class="present fl"><span class="stext2">To</span><br /><p id="DateTo9" class="for_date_to"><?php if ($_POST['MonthDateFrom8'] && $_POST['YearDateFrom8']):  echo $_POST['MonthDateFrom8']; ?>
/<?php echo $_POST['YearDateFrom8'];  endif; ?></p></div>
        <?php echo smarty_function_textfield(array('label' => 'Street Address','name' => 'StreetAddress5','class' => '20_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "Apt. #",'name' => 'Aptn5','class' => '4_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'CityCountry5','class' => '12_1'), $this);?>

        <?php echo smarty_function_states(array('name' => 'State5','label' => 'State','class' => '8_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'ZIpCode5','class' => '7_1'), $this);?>

      </div>
      <div class="set-default clearer left_marg2a">                    
        <?php echo smarty_function_textfield(array('label' => 'Name of Person Who Knows You','name' => 'NameOfPerson5','class' => '32'), $this);?>

        <?php echo smarty_function_textfield(array('label' => 'Street Address','name' => 'StreetAddressOfPerson5','class' => '20_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "Apt. #",'name' => 'AptnOfPerson5','class' => '4_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'CityCountryOfPerson5','class' => '12_1'), $this);?>

        <?php echo smarty_function_states(array('name' => 'StateOfPerson5','label' => 'State','class' => '8_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'ZIpCodeOfPerson5','class' => '7_1'), $this);?>

      </div>
    </div>
        </fieldset>
  <a href="javascript:showadd(8);" <?php if ($_POST['add8']): ?> style="display:none;" <?php endif; ?> id="a8show">More</a>
<div <?php if (! $_POST['add8']): ?> style="display:none;" <?php endif; ?> id="a8"  >
  
Enter more fields.
<br>

  <fieldset>
    <div class="area">
      <div class="set-default clearer">
        <div class="item-number fl">#6</div>
        <?php echo smarty_function_monthyear(array('name' => 'aDateFrom5','onchange' => "fillTo('aDateFrom5', 'aDateTo6')",'label' => 'From','class' => "10-45_1"), $this);?>

        <div class="present fl"><span class="stext2">To</span><br /><p id="aDateTo5" class="for_date_to"><?php if ($_POST['MonthDateFrom5'] && $_POST['YearDateFrom5']):  echo $_POST['MonthDateFrom9']; ?>
/<?php echo $_POST['YearDateFrom9'];  endif; ?></p></div>
        <?php echo smarty_function_textfield(array('label' => 'Street Address','name' => 'aStreetAddress1','class' => '20_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "Apt. #",'name' => 'aAptn1','class' => '4_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'aCityCountry1','class' => '12_1'), $this);?>

        <?php echo smarty_function_states(array('name' => 'aState1','label' => 'State','class' => '8_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'aZIpCode1','class' => '7_1'), $this);?>

      </div>
      <div class="set-default clearer left_marg2a">                    
        <?php echo smarty_function_textfield(array('label' => 'Name of Person Who Knows You','name' => 'aNameOfPerson1','class' => '32'), $this);?>

        <?php echo smarty_function_textfield(array('label' => 'Street Address','name' => 'aStreetAddressOfPerson1','class' => '20_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "Apt. #",'name' => 'aAptnOfPerson1','class' => '4_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'aCityCountryOfPerson1','class' => '12_1'), $this);?>

        <?php echo smarty_function_states(array('name' => 'aStateOfPerson1','label' => 'State','class' => '8_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'aZIpCodeOfPerson1','class' => '7_1'), $this);?>

      </div>
    </div>
    <div class="set-default clearer">
      <div class="item-number fl">#7</div>
      <?php echo smarty_function_monthyear(array('name' => 'aDateFrom6','onchange' => "fillTo('aDateFrom6', 'aDateTo7')",'label' => 'From','class' => "10-45_1"), $this);?>

      <div class="present fl"><span class="stext2">To</span><br /><p id="aDateTo6" class="for_date_to"><?php if ($_POST['MonthDateFrom5'] && $_POST['YearDateFrom5']):  echo $_POST['aMonthDateFrom5']; ?>
/<?php echo $_POST['aYearDateFrom5'];  endif; ?></p></div>
      <?php echo smarty_function_textfield(array('label' => 'Street Address','name' => 'aStreetAddress2','class' => '20_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "Apt. #",'name' => 'aAptn2','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'aCityCountry2','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'aState2','label' => 'State','class' => '8_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'aZIpCode2','class' => '7_1'), $this);?>

    </div>
    <div class="set-default clearer left_marg2a">                    
      <?php echo smarty_function_textfield(array('label' => 'Name of Person Who Knows You','name' => 'aNameOfPerson2','class' => '32'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'Street Address','name' => 'aStreetAddressOfPerson2','class' => '20_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "Apt. #",'name' => 'aAptnOfPerson2','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'aCityCountryOfPerson2','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'aStateOfPerson2','label' => 'State','class' => '8_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'aZIpCodeOfPerson2','class' => '7_1'), $this);?>

    </div>
    <div class="area">
      <div class="set-default clearer">
        <div class="item-number fl">#8</div>
        <?php echo smarty_function_monthyear(array('name' => 'aDateFrom7','onchange' => "fillTo('aDateFrom7', 'aDateTo8')",'label' => 'From','class' => "10-45_1"), $this);?>

        <div class="present fl"><span class="stext2">To</span><br /><p id="aDateTo7" class="for_date_to"><?php if ($_POST['aMonthDateFrom6'] && $_POST['aYearDateFrom6']):  echo $_POST['aMonthDateFrom6']; ?>
/<?php echo $_POST['aYearDateFrom6'];  endif; ?></p></div>
        <?php echo smarty_function_textfield(array('label' => 'Street Address','name' => 'aStreetAddress3','class' => '20_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "Apt. #",'name' => 'aAptn3','class' => '4_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'aCityCountry3','class' => '12_1'), $this);?>

        <?php echo smarty_function_states(array('name' => 'aState3','label' => 'State','class' => '8_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'aZIpCode3','class' => '7_1'), $this);?>

      </div>
      <div class="set-default clearer left_marg2a">                    
        <?php echo smarty_function_textfield(array('label' => 'Name of Person Who Knows You','name' => 'aNameOfPerson3','class' => '32'), $this);?>

        <?php echo smarty_function_textfield(array('label' => 'Street Address','name' => 'aStreetAddressOfPerson3','class' => '20_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "Apt. #",'name' => 'aAptnOfPerson3','class' => '4_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'aCityCountryOfPerson3','class' => '12_1'), $this);?>

        <?php echo smarty_function_states(array('name' => 'aStateOfPerson3','label' => 'State','class' => '8_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'aZIpCodeOfPerson3','class' => '7_1'), $this);?>

      </div>
    </div>
    <div class="set-default clearer">
      <div class="item-number fl">#9</div>
      <?php echo smarty_function_monthyear(array('name' => 'aDateFrom8','onchange' => "fillTo('aDateFrom8', 'aDateTo9')",'label' => 'From','class' => "10-45_1"), $this);?>

      <div class="present fl"><span class="stext2">To</span><br /><p id="aDateTo8" class="for_date_to"><?php if ($_POST['aMonthDateFrom7'] && $_POST['aYearDateFrom7']):  echo $_POST['aMonthDateFrom7']; ?>
/<?php echo $_POST['aYearDateFrom7'];  endif; ?></p></div>
      <?php echo smarty_function_textfield(array('label' => 'Street Address','name' => 'aStreetAddress4','class' => '20_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "Apt. #",'name' => 'aAptn4','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'aCityCountry4','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'aState4','label' => 'State','class' => '8_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'aZIpCode4','class' => '7_1'), $this);?>

    </div>
    <div class="set-default clearer left_marg2a">                    
      <?php echo smarty_function_textfield(array('label' => 'Name of Person Who Knows You','name' => 'aNameOfPerson4','class' => '32'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'Street Address','name' => 'aStreetAddressOfPerson4','class' => '20_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "Apt. #",'name' => 'aAptnOfPerson4','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'aCityCountryOfPerson4','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'aStateOfPerson4','label' => 'State','class' => '8_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZIpCode','name' => 'aZIpCodeOfPerson4','class' => '7_1'), $this);?>

    </div>
    <div class="area">
      <div class="set-default clearer">
        <div class="item-number fl">#10</div>
        <?php echo smarty_function_monthyear(array('name' => 'aDateFrom9','label' => 'From','class' => "10-45_1"), $this);?>

        <div class="present fl"><span class="stext2">To</span><br /><p id="aDateTo9" class="for_date_to"><?php if ($_POST['aMonthDateFrom8'] && $_POST['aYearDateFrom8']):  echo $_POST['aMonthDateFrom8']; ?>
/<?php echo $_POST['aYearDateFrom8'];  endif; ?></p></div>
        <?php echo smarty_function_textfield(array('label' => 'Street Address','name' => 'aStreetAddress5','class' => '20_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "Apt. #",'name' => 'aAptn5','class' => '4_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'aCityCountry5','class' => '12_1'), $this);?>

        <?php echo smarty_function_states(array('name' => 'aState5','label' => 'State','class' => '8_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'aZIpCode5','class' => '7_1'), $this);?>

      </div>
      <div class="set-default clearer left_marg2a">                    
        <?php echo smarty_function_textfield(array('label' => 'Name of Person Who Knows You','name' => 'aNameOfPerson5','class' => '32'), $this);?>

        <?php echo smarty_function_textfield(array('label' => 'Street Address','name' => 'aStreetAddressOfPerson5','class' => '20_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "Apt. #",'name' => 'aAptnOfPerson5','class' => '4_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'aCityCountryOfPerson5','class' => '12_1'), $this);?>

        <?php echo smarty_function_states(array('name' => 'aStateOfPerson5','label' => 'State','class' => '8_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'aZIpCodeOfPerson5','class' => '7_1'), $this);?>

      </div>
    </div>
  </fieldset>

<a href="javascript:hideadd(8);" id="a8hide">Hide More</a>
<input type="hidden" name="add8" value="<?php echo $_POST['add8']; ?>
" id="add8"  /><b style="color: red;"><?php echo $this->_tpl_vars['errorMessages']['add8']; ?>
</b>
</div>
<br>
<br>
  
<!-- ----------------------------------------------------    9     ------------------------------------------------ -->

<fieldset><legend><span>9.</span> WHERE YOU WENT TO SCHOOL</legend>
  <p class="note">List the schools you have attended, beyond Junior High School, beginning with the most recent (#1) and working back 5 years. List all College or University degrees and the dates they were received. If all of your education occurred more than 5 years ago, list your most recent education beyond high school, no matter when that education occurred.</p>
  <p class="note">-For correspondence schools and extension classes, provide the address where the records are maintained.</p>
  <div class="area">
    <div class="set-default clearer">
      <div class="item-number fl">#1</div>
      <?php echo smarty_function_monthyear(array('name' => 'DateFrom10','label' => 'From','class' => "10-45_1"), $this);?>

      <?php echo smarty_function_monthyear(array('name' => 'DateTo10','label' => 'To','class' => "10_1-45_1"), $this);?>

      <?php echo smarty_function_code13(array('name' => 'Code1','label' => 'School Type'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'Name of School','name' => 'NameOfSchool1','class' => '39_1'), $this);?>

    </div>
    <div class="set-default clearer left_marg2a">                    
      <?php echo smarty_function_textfield(array('label' => "Street Address and City (Country) of School",'name' => 'StreetAddresspCityOfSchool1','class' => '32'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "Degree/Diploma/Other",'name' => 'DegreesDiplomasOther1','class' => '22_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "Month/Year Awarded",'name' => 'MonthsYearAwarded1','class' => '15_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'StateOfSchool1','label' => 'State','class' => '8_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZIP Code','name' => 'ZIpCodeOfSchool1','class' => '7_1'), $this);?>

    </div>
  </div>
  <div class="set-default clearer">
    <div class="item-number fl">#2</div>
    <?php echo smarty_function_monthyear(array('name' => 'DateFrom11','label' => 'From','class' => "10-45_1"), $this);?>

    <?php echo smarty_function_monthyear(array('name' => 'DateTo11','label' => 'To','class' => "10_1-45_1"), $this);?>

    <?php echo smarty_function_code13(array('name' => 'Code2','label' => 'School Type'), $this);?>

    <?php echo smarty_function_textfield(array('label' => 'Name of School','name' => 'NameOfSchool2','class' => '39_1'), $this);?>

  </div>
  <div class="set-default clearer left_marg2a">                    
    <?php echo smarty_function_textfield(array('label' => "Street Address and City (Country) of School",'name' => 'StreetAddresspCityOfSchool2','class' => '32'), $this);?>

    <?php echo smarty_function_textfield(array('label' => "Degree/Diploma/Other",'name' => 'DegreesDiplomasOther2','class' => '22_1'), $this);?>

    <?php echo smarty_function_textfield(array('label' => "Month/Year Awarded",'name' => 'MonthsYearAwarded2','class' => '15_1'), $this);?>

    <?php echo smarty_function_states(array('name' => 'StateOfSchool2','label' => 'State','class' => '8_1'), $this);?>

    <?php echo smarty_function_textfield(array('label' => 'ZIP Code','name' => 'ZIpCodeOfSchool2','class' => '7_1'), $this);?>

  </div>
  <div class="area">
    <div class="set-default clearer">
      <div class="item-number fl">#3</div>
      <?php echo smarty_function_monthyear(array('name' => 'DateFrom12','label' => 'From','class' => "10-45_1"), $this);?>

      <?php echo smarty_function_monthyear(array('name' => 'DateTo12','label' => 'To','class' => "10_1-45_1"), $this);?>

      <?php echo smarty_function_code13(array('name' => 'Code3','label' => 'School Type'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'Name of School','name' => 'NameOfSchool3','class' => '39_1'), $this);?>

    </div>
    <div class="set-default clearer left_marg2a">                    
      <?php echo smarty_function_textfield(array('label' => "Street Address and City (Country) of School",'name' => 'StreetAddresspCityOfSchool3','class' => '32'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "Degree/Diploma/Other",'name' => 'DegreesDiplomasOther3','class' => '22_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "Month/Year Awarded",'name' => 'MonthsYearAwarded3','class' => '15_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'StateOfSchool3','label' => 'State','class' => '8_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZIP Code','name' => 'ZIpCodeOfSchool3','class' => '7_1'), $this);?>

    </div>
  </div>
</fieldset>
<a href="javascript:showadd(9);" <?php if ($_POST['add9']): ?> style="display:none;" <?php endif; ?> id="a9show">More</a>
<div <?php if (! $_POST['add9']): ?> style="display:none;" <?php endif; ?> id="a9"  >
 Enter more fields 
  <fieldset>
    <div class="area">
      <div class="set-default clearer">
        <div class="item-number fl">#4</div>
        <?php echo smarty_function_monthyear(array('name' => 'aDateFrom10','label' => 'From','class' => "10-45_1"), $this);?>

        <?php echo smarty_function_monthyear(array('name' => 'aDateTo10','label' => 'To','class' => "10_1-45_1"), $this);?>

        <?php echo smarty_function_code13(array('name' => 'aCode1','label' => 'School Type'), $this);?>

        <?php echo smarty_function_textfield(array('label' => 'Name of School','name' => 'aNameOfSchool1','class' => '39_1'), $this);?>

      </div>
      <div class="set-default clearer left_marg2a">                    
        <?php echo smarty_function_textfield(array('label' => "Street Address and City (Country) of School",'name' => 'aStreetAddresspCityOfSchool1','class' => '32'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "Degree/Diploma/Other",'name' => 'aDegreesDiplomasOther1','class' => '22_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "Month/Year Awarded",'name' => 'aMonthsYearAwarded1','class' => '15_1'), $this);?>

        <?php echo smarty_function_states(array('name' => 'aStateOfSchool1','label' => 'State','class' => '8_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => 'ZIP Code','name' => 'aZIpCodeOfSchool1','class' => '7_1'), $this);?>

      </div>
    </div>
    <div class="set-default clearer">
      <div class="item-number fl">#5</div>
      <?php echo smarty_function_monthyear(array('name' => 'aDateFrom11','label' => 'From','class' => "10-45_1"), $this);?>

      <?php echo smarty_function_monthyear(array('name' => 'aDateTo11','label' => 'To','class' => "10_1-45_1"), $this);?>

      <?php echo smarty_function_code13(array('name' => 'aCode2','label' => 'School Type'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'Name of School','name' => 'aNameOfSchool2','class' => '39_1'), $this);?>

    </div>
    <div class="set-default clearer left_marg2a">                    
      <?php echo smarty_function_textfield(array('label' => "Street Address and City (Country) of School",'name' => 'aStreetAddresspCityOfSchool2','class' => '32'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "Degree/Diploma/Other",'name' => 'aDegreesDiplomasOther2','class' => '22_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "Month/Year Awarded",'name' => 'aMonthsYearAwarded2','class' => '15_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'aStateOfSchool2','label' => 'State','class' => '8_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZIP Code','name' => 'aZIpCodeOfSchool2','class' => '7_1'), $this);?>

    </div>
    <div class="area">
      <div class="set-default clearer">
        <div class="item-number fl">#6</div>
        <?php echo smarty_function_monthyear(array('name' => 'aDateFrom12','label' => 'From','class' => "10-45_1"), $this);?>

        <?php echo smarty_function_monthyear(array('name' => 'aDateTo12','label' => 'To','class' => "10_1-45_1"), $this);?>

        <?php echo smarty_function_code13(array('name' => 'aCode3','label' => 'School Type'), $this);?>

        <?php echo smarty_function_textfield(array('label' => 'Name of School','name' => 'aNameOfSchool3','class' => '39_1'), $this);?>

      </div>
      <div class="set-default clearer left_marg2a">                    
        <?php echo smarty_function_textfield(array('label' => "Street Address and City (Country) of School",'name' => 'aStreetAddresspCityOfSchool3','class' => '32'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "Degree/Diploma/Other",'name' => 'aDegreesDiplomasOther3','class' => '22_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => "Month/Year Awarded",'name' => 'aMonthsYearAwarded3','class' => '15_1'), $this);?>

        <?php echo smarty_function_states(array('name' => 'aStateOfSchool3','label' => 'State','class' => '8_1'), $this);?>

        <?php echo smarty_function_textfield(array('label' => 'ZIP Code','name' => 'aZIpCodeOfSchool3','class' => '7_1'), $this);?>

      </div>
    </div>
  </fieldset>
  <a href="javascript:hideadd(9);"  id="a9hide">Hide More</a>
  <input type="hidden" name="add9" value="<?php echo $_POST['add9']; ?>
" id="add9" value="0" /><b style="color: red;"><?php echo $this->_tpl_vars['errorMessages']['add9']; ?>
</b>
</div>
<br>
<br>
<!-- ----------------------------------------------------    10     ------------------------------------------------ -->
<fieldset><legend><span>10.</span> YOUR EMPLOYMENT ACTIVITIES</legend>
  <p class="note">List your employment activities, beginning with the present (#1) and working back 5 years. You should list all full-time work, part-time work, military service, temporary military duty locations over 90 days, self-employment, other paid work, and all periods of unemployment. The entire 5-year period must be accounted for without breaks, but you need not list employments before your 16th birthday.</p>
  <p class="note"><strong>Employer/Verifier Name.</strong> List the business name of your employer or the name of the person who can verify your self-employment or unemployment in this block. If military service is being listed, include your duty location or home port here as well as your branch of service. You should provide separate listings to reflect changes in your military duty locations or home ports. </p>
  <p class="note"><strong>Previous Periods of Activity.</strong> Complete these lines if you worked for an employer on more than one occasion at the same location. After entering the most recent period of employment in the initial numbered block, provide previous periods of employment at the same location on the additional lines provided. For example, if you worked at XY Plumbing in Denver, CO, during 3 separate periods of time, you would enter dates and information concerning the most recent period of employment first, and provide dates, position titles, and supervisors for the two previous periods of employment on the lines below that information.</p>
  <div class="area_w">
    <div class="set-default clearer">
      <div class="item-number fl">#1</div>
      <input type="checkbox" onClick="unemployment(1)" name="unemp1" value="1" id="unemp1" <?php if ($_POST['unemp1']): ?> checked="checked"<?php endif; ?>  /><strong>Unemployment Period</strong><br />    
    </div>
    <div class="set-default clearer">
      <?php echo smarty_function_monthyear(array('name' => 'DateFrom13','onchange' => "fillTo('DateFrom13', 'DateTo17')",'label' => 'From','class' => "10-45_1"), $this);?>

      <div class="present1 fl"><span class="stext6">To</span><br /><b>Present</b></div>
      <?php echo smarty_function_code19(array('name' => 'Code4','label' => 'Type of employment'), $this);?>

      <div class="block_position2"><?php echo smarty_function_textfield(array('label' => "Employer/Verifier <br /> Name/Military Duty Location",'name' => 'EmployersVerifierNamesMilitaryDutyLocation1','class' => '28a_1'), $this);?>
</div>
      <div class="block_position2" id="positionEmp1"><?php echo smarty_function_textfield(array('label' => "Your Position <br /> Title/Military Rank",'name' => 'PositionTitlesMilitaryRank','class' => '15a_1'), $this);?>
</div>
      
    </div>
    <div id="empblock1" <?php if ($_POST['unemp1']): ?> style="display:none;" <?php endif; ?>>
    <div class="set-default clearer">                    
      <?php echo smarty_function_textfield(array('label' => "Employer's/Verifier's Street Address",'name' => 'EmployersVerifierStreetAddress1','class' => '44a'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'EmployersVerifierCityCountry1','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'EmployersVerifierState1','label' => 'State','class' => '8_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'EmployersVerifierZIPCode1','class' => '7_1'), $this);?>

      <span class="stext6 <?php if ($this->_tpl_vars['errorMessages']['AreaCodeOfEmployersVerifier1'] || $this->_tpl_vars['errorMessages']['PhonenOfEmployersVerifier1']): ?>item-error<?php endif; ?>">Telephone Number</span>
      <?php echo smarty_function_textfield(array('label' => "",'name' => 'AreaCodeOfEmployersVerifier1','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "",'name' => 'PhonenOfEmployersVerifier1','class' => '11_a'), $this);?>

    </div>
    <div class="set-default clearer">                    
      <?php echo smarty_function_textfield(array('label' => "Street Address of Job Location <em>if different than Employer's Address</em>",'name' => 'StreetAddressOfJobLocation1','class' => '44a'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'CityCountryOfJobLocation1','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'StateOfJobLocation1','label' => 'State','class' => '8r_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'ZIPCodeOfJobLocation1','class' => '7_1'), $this);?>

      <span class="stext6 <?php if ($this->_tpl_vars['errorMessages']['AreaCodeOfJobLocation1'] || $this->_tpl_vars['errorMessages']['PhonenOfJobLocation1']): ?>item-error<?php endif; ?> ">Telephone Number</span>
      <?php echo smarty_function_textfield(array('label' => "",'name' => 'AreaCodeOfJobLocation1','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "",'name' => 'PhonenOfJobLocation1','class' => '11_a'), $this);?>

    </div>
    <div class="set-default clearer">                    
      <?php echo smarty_function_textfield(array('label' => "Supervisor's Name & Street Address <em>if different than Job Location</em>",'name' => 'SupervisorNamepStreetAddress1','class' => '44a'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'CityCountryOfSupervisor1','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'StateOfSupervisor1','label' => 'State','class' => '8r_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'ZIPCodeOfSupervisor1','class' => '7_1'), $this);?>

      <span class="stext6 <?php if ($this->_tpl_vars['errorMessages']['AreaCodeOfSupervisor1'] || $this->_tpl_vars['errorMessages']['PhonenOfSupervisor1']): ?>item-error<?php endif; ?>">Telephone Number</span>
      <?php echo smarty_function_textfield(array('label' => "",'name' => 'AreaCodeOfSupervisor1','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "",'name' => 'PhonenOfSupervisor1','class' => '11_a'), $this);?>

    </div>
    <div  class="set-default clearer">
      <div class="previous_period fl">Previous period</div>
      <div class="fl">
        <div class="set-default clearer">  
          <?php echo smarty_function_monthyear(array('name' => 'DateFrom14','label' => 'From','class' => "10-45_1"), $this);?>

          <?php echo smarty_function_monthyear(array('name' => 'DateTo14','label' => 'To','class' => "10_1-45_1"), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'PositionTitle1','class' => '16_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'Supervisor1','class' => '28a_1'), $this);?>

        </div>
        <div class="set-default clearer">
          <?php echo smarty_function_monthyear(array('name' => 'DateFrom15','label' => 'From','class' => "10-45_1"), $this);?>

          <?php echo smarty_function_monthyear(array('name' => 'DateTo15','label' => 'To','class' => "10_1-45_1"), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'PositionTitle2','class' => '16_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'Supervisor2','class' => '28a_1'), $this);?>

        </div> 
        <div class="set-default clearer">
          <?php echo smarty_function_monthyear(array('name' => 'DateFrom16','label' => 'From','class' => "10-45_1"), $this);?>

          <?php echo smarty_function_monthyear(array('name' => 'DateTo16','label' => 'To','class' => "10_1-45_1"), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'PositionTitle3','class' => '16_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'Supervisor3','class' => '28a_1'), $this);?>

        </div>
      </div>
    </div>
    </div>
  </div>  
  <div class="area">
    <div class="set-default clearer">
      <div class="item-number fl">#2</div>
      <input type="checkbox" onClick="unemployment(2)"   name="unemp2" value="2" id="unemp2" <?php if ($_POST['unemp2']): ?> checked="checked"<?php endif; ?>  /><strong>Unemployment Period</strong><br />    
    </div>
    <div class="set-default clearer">
      <?php echo smarty_function_monthyear(array('name' => 'DateFrom17','onchange' => "fillTo('DateFrom17', 'DateTo21')",'label' => 'From','class' => "10-45_1"), $this);?>

      <div class="present1 fl"><span class="stext6">To</span><br />
	  <p id="DateTo17" class="for_date_to"><?php if ($_POST['MonthDateFrom13'] && $_POST['YearDateFrom13']):  echo $_POST['MonthDateFrom13']; ?>
/<?php echo $_POST['YearDateFrom13'];  endif; ?></p></div>
      <?php echo smarty_function_code19(array('name' => 'Code5','label' => 'Type of employment'), $this);?>

      <div class="block_position2"><?php echo smarty_function_textfield(array('label' => "Employer/Verifier <br /> Name/Military Duty Location",'name' => 'EmployersVerifierNamesMilitaryDutyLocation2','class' => '28a_1'), $this);?>
</div>
      <div class="block_position2" id="positionEmp2"><?php echo smarty_function_textfield(array('label' => "Your Position <br /> Title/Military Rank",'name' => 'PositionTitlesMilitaryRank2','class' => '15a_1'), $this);?>
</div>
      
    </div>
    <div id="empblock2" <?php if ($_POST['unemp2']): ?> style="display:none;" <?php endif; ?>>
    <div class="set-default clearer">                    
      <?php echo smarty_function_textfield(array('label' => "Employer's/Verifier's Street Address",'name' => 'EmployersVerifierStreetAddress2','class' => '44a'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'EmployersVerifierCityCountry2','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'EmployersVerifierState2','label' => 'State','class' => '8_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'EmployersVerifierZIPCode2','class' => '7_1'), $this);?>

      <span class="stext6   <?php if ($this->_tpl_vars['errorMessages']['AreaCodeOfEmployersVerifier2'] || $this->_tpl_vars['errorMessages']['PhonenOfEmployersVerifier2']): ?>item-error<?php endif; ?>">Telephone Number</span>
      <?php echo smarty_function_textfield(array('label' => "",'name' => 'AreaCodeOfEmployersVerifier2','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "",'name' => 'PhonenOfEmployersVerifier2','class' => '11_a'), $this);?>

    </div>
    <div class="set-default clearer">                    
      <?php echo smarty_function_textfield(array('label' => "Street Address of Job Location <em>if different than Employer's Address</em>",'name' => 'StreetAddressOfJobLocation2','class' => '44a'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'CityCountryOfJobLocation2','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'StateOfJobLocation2','label' => 'State','class' => '8r_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'ZIPCodeOfJobLocation2','class' => '7_1'), $this);?>

      <span class="stext6 <?php if ($this->_tpl_vars['errorMessages']['AreaCodeOfJobLocation2'] || $this->_tpl_vars['errorMessages']['PhonenOfJobLocation2']): ?>item-error<?php endif; ?> ">Telephone Number</span>
      <?php echo smarty_function_textfield(array('label' => "",'name' => 'AreaCodeOfJobLocation2','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "",'name' => 'PhonenOfJobLocation2','class' => '11_a'), $this);?>

    </div>
    <div class="set-default clearer">                    
      <?php echo smarty_function_textfield(array('label' => "Supervisor's Name & Street Address <em>if different than Job Location</em>",'name' => 'SupervisorNamepStreetAddress2','class' => '44a'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'CityCountryOfSupervisor2','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'StateOfSupervisor2','label' => 'State','class' => '8r_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'ZIPCodeOfSupervisor2','class' => '7_1'), $this);?>

      <span class="stext6  <?php if ($this->_tpl_vars['errorMessages']['AreaCodeOfSupervisor2'] || $this->_tpl_vars['errorMessages']['PhonenOfSupervisor2']): ?>item-error<?php endif; ?>">Telephone Number</span>
      <?php echo smarty_function_textfield(array('label' => "",'name' => 'AreaCodeOfSupervisor2','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "",'name' => 'PhonenOfSupervisor2','class' => '11_a'), $this);?>

    </div>
    <div class="set-default clearer">
      <div class="previous_period fl">Previous period</div>
      <div class="fl">
        <div class="set-default clearer">  
          <?php echo smarty_function_monthyear(array('name' => 'DateFrom18','label' => 'From','class' => "10-45_1"), $this);?>

          <?php echo smarty_function_monthyear(array('name' => 'DateTo18','label' => 'To','class' => "10_1-45_1"), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'PositionTitle4','class' => '16_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'Supervisor4','class' => '28a_1'), $this);?>

        </div>
        <div class="set-default clearer">
          <?php echo smarty_function_monthyear(array('name' => 'DateFrom19','label' => 'From','class' => "10-45_1"), $this);?>

          <?php echo smarty_function_monthyear(array('name' => 'DateTo19','label' => 'To','class' => "10_1-45_1"), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'PositionTitle5','class' => '16_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'Supervisor5','class' => '28a_1'), $this);?>

        </div> 
        <div class="set-default clearer">
          <?php echo smarty_function_monthyear(array('name' => 'DateFrom20','label' => 'From','class' => "10-45_1"), $this);?>

          <?php echo smarty_function_monthyear(array('name' => 'DateTo20','label' => 'To','class' => "10_1-45_1"), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'PositionTitle6','class' => '16_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'Supervisor6','class' => '28a_1'), $this);?>

        </div>
      </div>
    </div>
    </div>
  </div>
  <div class="area_w">
    <div class="set-default clearer">
      <div class="item-number fl">#3</div>
      <input type="checkbox"  onclick="unemployment(3)"  name="unemp3" value="3" id="unemp3" <?php if ($_POST['unemp3']): ?> checked="checked"<?php endif; ?> /><strong>Unemployment Period</strong><br />    
    </div>
    <div class="set-default clearer">
      <?php echo smarty_function_monthyear(array('name' => 'DateFrom21','onchange' => "fillTo('DateFrom21', 'DateTo25')",'label' => 'From','class' => "10-45_1"), $this);?>

      <div class="present1 fl"><span class="stext6">To</span><br /><p id="DateTo21" class="for_date_to"><?php if ($_POST['MonthDateFrom17'] && $_POST['YearDateFrom17']):  echo $_POST['MonthDateFrom17']; ?>
/<?php echo $_POST['YearDateFrom17'];  endif; ?></p></div>
      <?php echo smarty_function_code19(array('name' => 'Code6','label' => 'Type of employment'), $this);?>

      <div class="block_position2"><?php echo smarty_function_textfield(array('label' => "Employer/Verifier <br /> Name/Military Duty Location",'name' => 'EmployersVerifierNamesMilitaryDutyLocation3','class' => '28a_1'), $this);?>
</div>
      <div class="block_position2" id="positionEmp3"><?php echo smarty_function_textfield(array('label' => "Your Position <br /> Title/Military Rank",'name' => 'PositionTitlesMilitaryRank3','class' => '15a_1'), $this);?>
</div>
      
    </div>
    <div id="empblock3" <?php if ($_POST['unemp3']): ?> style="display:none;" <?php endif; ?>>
    <div class="set-default clearer">                    
      <?php echo smarty_function_textfield(array('label' => "Employer's/Verifier's Street Address",'name' => 'EmployersVerifierStreetAddress3','class' => '44a'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'EmployersVerifierCityCountry3','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'EmployersVerifierState3','label' => 'State','class' => '8_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'EmployersVerifierZIPCode3','class' => '7_1'), $this);?>

      <span class="stext6   <?php if ($this->_tpl_vars['errorMessages']['AreaCodeOfEmployersVerifier3'] || $this->_tpl_vars['errorMessages']['PhonenOfEmployersVerifier3']): ?>item-error<?php endif; ?>">Telephone Number</span>
      <?php echo smarty_function_textfield(array('label' => "",'name' => 'AreaCodeOfEmployersVerifier3','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "",'name' => 'PhonenOfEmployersVerifier3','class' => '11_a'), $this);?>

    </div>
    <div class="set-default clearer">                    
      <?php echo smarty_function_textfield(array('label' => "Street Address of Job Location <em>if different than Employer's Address</em>",'name' => 'StreetAddressOfJobLocation3','class' => '44a'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'CityCountryOfJobLocation3','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'StateOfJobLocation3','label' => 'State','class' => '8r_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'ZIPCodeOfJobLocation3','class' => '7_1'), $this);?>

      <span class="stext6 <?php if ($this->_tpl_vars['errorMessages']['AreaCodeOfJobLocation3'] || $this->_tpl_vars['errorMessages']['PhonenOfJobLocation3']): ?>item-error<?php endif; ?> ">Telephone Number</span>
      <?php echo smarty_function_textfield(array('label' => "",'name' => 'AreaCodeOfJobLocation3','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "",'name' => 'PhonenOfJobLocation3','class' => '11_a'), $this);?>

    </div>
    <div class="set-default clearer">                    
      <?php echo smarty_function_textfield(array('label' => "Supervisor's Name & Street Address <em>if different than Job Location</em>",'name' => 'SupervisorNamepStreetAddress3','class' => '44a'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'CityCountryOfSupervisor3','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'StateOfSupervisor3','label' => 'State','class' => '8r_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'ZIPCodeOfSupervisor3','class' => '7_1'), $this);?>

      <span class="stext6 <?php if ($this->_tpl_vars['errorMessages']['AreaCodeOfSupervisor3'] || $this->_tpl_vars['errorMessages']['PhonenOfSupervisor3']): ?>item-error<?php endif; ?> ">Telephone Number</span>
      <?php echo smarty_function_textfield(array('label' => "",'name' => 'AreaCodeOfSupervisor3','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "",'name' => 'PhonenOfSupervisor3','class' => '11_a'), $this);?>

    </div>
    <div  class="set-default clearer">
      <div class="previous_period fl">Previous period</div>
      <div class="fl">
        <div class="set-default clearer">  
          <?php echo smarty_function_monthyear(array('name' => 'DateFrom22','label' => 'From','class' => "10-45_1"), $this);?>

          <?php echo smarty_function_monthyear(array('name' => 'DateTo22','label' => 'To','class' => "10_1-45_1"), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'PositionTitle7','class' => '16_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'Supervisor7','class' => '28a_1'), $this);?>

        </div>
        <div class="set-default clearer">
          <?php echo smarty_function_monthyear(array('name' => 'DateFrom23','label' => 'From','class' => "10-45_1"), $this);?>

          <?php echo smarty_function_monthyear(array('name' => 'DateTo23','label' => 'To','class' => "10_1-45_1"), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'PositionTitle8','class' => '16_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'Supervisor8','class' => '28a_1'), $this);?>

        </div> 
        <div class="set-default clearer">
          <?php echo smarty_function_monthyear(array('name' => 'DateFrom24','label' => 'From','class' => "10-45_1"), $this);?>

          <?php echo smarty_function_monthyear(array('name' => 'DateTo24','label' => 'To','class' => "10_1-45_1"), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'PositionTitle9','class' => '16_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'Supervisor9','class' => '28a_1'), $this);?>

        </div>
      </div>
    </div>
    </div>
  </div>  
  <div class="area">
    <div class="set-default clearer">
      <div class="item-number fl">#4</div>
      <input type="checkbox"  onclick="unemployment(4)"  name="unemp4" value="4" id="unemp4" <?php if ($_POST['unemp4']): ?> checked="checked"<?php endif; ?> /><strong>Unemployment Period</strong><br />    
    </div>
    <div class="set-default clearer">
      <?php echo smarty_function_monthyear(array('name' => 'DateFrom25','onchange' => "fillTo('DateFrom25', 'DateTo29')",'label' => 'From','class' => "10-45_1"), $this);?>

      <div class="present1 fl"><span class="stext6">To</span><br /><p id="DateTo25" class="for_date_to"><?php if ($_POST['MonthDateFrom21'] && $_POST['YearDateFrom21']):  echo $_POST['MonthDateFrom21']; ?>
/<?php echo $_POST['YearDateFrom21'];  endif; ?></p></div>
      <?php echo smarty_function_code19(array('name' => 'Code7','label' => 'Type of employment'), $this);?>

      <div class="block_position2"><?php echo smarty_function_textfield(array('label' => "Employer/Verifier <br /> Name/Military Duty Location",'name' => 'EmployersVerifierNamesMilitaryDutyLocation4','class' => '28a_1'), $this);?>
</div>
      <div class="block_position2" id="positionEmp4"><?php echo smarty_function_textfield(array('label' => "Your Position <br /> Title/Military Rank",'name' => 'PositionTitlesMilitaryRank4','class' => '15a_1'), $this);?>
</div>
      
    </div>
    <div id="empblock4" <?php if ($_POST['unemp4']): ?> style="display:none;" <?php endif; ?>>
    <div class="set-default clearer">                    
      <?php echo smarty_function_textfield(array('label' => "Employer's/Verifier's Street Address",'name' => 'EmployersVerifierStreetAddress4','class' => '44a'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'EmployersVerifierCityCountry4','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'EmployersVerifierState4','label' => 'State','class' => '8_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'EmployersVerifierZIPCode4','class' => '7_1'), $this);?>

      <span class="stext6  <?php if ($this->_tpl_vars['errorMessages']['AreaCodeOfEmployersVerifier4'] || $this->_tpl_vars['errorMessages']['PhonenOfEmployersVerifier4']): ?>item-error<?php endif; ?>">Telephone Number</span>
      <?php echo smarty_function_textfield(array('label' => "",'name' => 'AreaCodeOfEmployersVerifier4','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "",'name' => 'PhonenOfEmployersVerifier4','class' => '11_a'), $this);?>

    </div>
    <div class="set-default clearer">                    
      <?php echo smarty_function_textfield(array('label' => "Street Address of Job Location <em>if different than Employer's Address</em>",'name' => 'StreetAddressOfJobLocation4','class' => '44a'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'CityCountryOfJobLocation4','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'StateOfJobLocation4','label' => 'State','class' => '8r_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'ZIPCodeOfJobLocation4','class' => '7_1'), $this);?>

      <span class="stext6 <?php if ($this->_tpl_vars['errorMessages']['AreaCodeOfJobLocation4'] || $this->_tpl_vars['errorMessages']['PhonenOfJobLocation4']): ?>item-error<?php endif; ?> ">Telephone Number</span>
      <?php echo smarty_function_textfield(array('label' => "",'name' => 'AreaCodeOfJobLocation4','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "",'name' => 'PhonenOfJobLocation4','class' => '11_a'), $this);?>

    </div>
    <div class="set-default clearer">                    
      <?php echo smarty_function_textfield(array('label' => "Supervisor's Name & Street Address <em>if different than Job Location</em>",'name' => 'SupervisorNamepStreetAddress4','class' => '44a'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'CityCountryOfSupervisor4','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'StateOfSupervisor4','label' => 'State','class' => '8r_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'ZIPCodeOfSupervisor4','class' => '7_1'), $this);?>

      <span class="stext6  <?php if ($this->_tpl_vars['errorMessages']['AreaCodeOfSupervisor4'] || $this->_tpl_vars['errorMessages']['PhonenOfSupervisor4']): ?>item-error<?php endif; ?> ">Telephone Number</span>
      <?php echo smarty_function_textfield(array('label' => "",'name' => 'AreaCodeOfSupervisor4','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "",'name' => 'PhonenOfSupervisor4','class' => '11_a'), $this);?>

    </div>
    <div class="set-default clearer">
      <div class="previous_period fl">Previous period</div>
      <div class="fl">
        <div class="set-default clearer">  
          <?php echo smarty_function_monthyear(array('name' => 'DateFrom26','label' => 'From','class' => "10-45_1"), $this);?>

          <?php echo smarty_function_monthyear(array('name' => 'DateTo26','label' => 'To','class' => "10_1-45_1"), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'PositionTitle10','class' => '16_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'Supervisor10','class' => '28a_1'), $this);?>

        </div>
        <div class="set-default clearer">
          <?php echo smarty_function_monthyear(array('name' => 'DateFrom27','label' => 'From','class' => "10-45_1"), $this);?>

          <?php echo smarty_function_monthyear(array('name' => 'DateTo27','label' => 'To','class' => "10_1-45_1"), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'PositionTitle11','class' => '16_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'Supervisor11','class' => '28a_1'), $this);?>

        </div> 
        <div class="set-default clearer">
          <?php echo smarty_function_monthyear(array('name' => 'DateFrom28','label' => 'From','class' => "10-45_1"), $this);?>

          <?php echo smarty_function_monthyear(array('name' => 'DateTo28','label' => 'To','class' => "10_1-45_1"), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'PositionTitle12','class' => '16_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'Supervisor12','class' => '28a_1'), $this);?>

        </div>
      </div>
    </div>
    </div>
  </div>
  <div class="area_w">
    <div class="set-default clearer">
      <div class="item-number fl">#5</div>
      <input type="checkbox"  onclick="unemployment(5)"  name="unemp5" value="5" id="unemp5" <?php if ($_POST['unemp5']): ?> checked="checked"<?php endif; ?>  /><strong>Unemployment Period</strong><br />    
    </div>
    <div class="set-default clearer">
      <?php echo smarty_function_monthyear(array('name' => 'DateFrom29','onchange' => "fillTo('DateFrom29', 'DateTo33')",'label' => 'From','class' => "10-45_1"), $this);?>

      <div class="present1 fl"><span class="stext6">To</span><br /><p id="DateTo29" class="for_date_to"><?php if ($_POST['MonthDateFrom25'] && $_POST['YearDateFrom25']):  echo $_POST['MonthDateFrom25']; ?>
/<?php echo $_POST['YearDateFrom25'];  endif; ?></p></div>
      <?php echo smarty_function_code19(array('name' => 'Code8','label' => 'Type of employment'), $this);?>

      <div class="block_position2"><?php echo smarty_function_textfield(array('label' => "Employer/Verifier <br /> Name/Military Duty Location",'name' => 'EmployersVerifierNamesMilitaryDutyLocation5','class' => '28a_1'), $this);?>
</div>
      <div class="block_position2" id="positionEmp5"><?php echo smarty_function_textfield(array('label' => "Your Position <br /> Title/Military Rank",'name' => 'PositionTitlesMilitaryRank5','class' => '15a_1'), $this);?>
</div>
      
    </div>
    <div id="empblock5" <?php if ($_POST['unemp5']): ?> style="display:none;" <?php endif; ?>>
    <div class="set-default clearer">                    
      <?php echo smarty_function_textfield(array('label' => "Employer's/Verifier's Street Address",'name' => 'EmployersVerifierStreetAddress5','class' => '44a'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'EmployersVerifierCityCountry5','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'EmployersVerifierState5','label' => 'State','class' => '8_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'EmployersVerifierZIPCode5','class' => '7_1'), $this);?>

      <span class="stext6  <?php if ($this->_tpl_vars['errorMessages']['AreaCodeOfEmployersVerifier5'] || $this->_tpl_vars['errorMessages']['PhonenOfEmployersVerifier5']): ?>item-error<?php endif; ?>">Telephone Number</span>
      <?php echo smarty_function_textfield(array('label' => "",'name' => 'AreaCodeOfEmployersVerifier5','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "",'name' => 'PhonenOfEmployersVerifier5','class' => '11_a'), $this);?>

    </div>
    <div class="set-default clearer">                    
      <?php echo smarty_function_textfield(array('label' => "Street Address of Job Location <em>if different than Employer's Address</em>",'name' => 'StreetAddressOfJobLocation5','class' => '44a'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'CityCountryOfJobLocation5','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'StateOfJobLocation5','label' => 'State','class' => '8r_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'ZIPCodeOfJobLocation5','class' => '7_1'), $this);?>

      <span class="stext6 <?php if ($this->_tpl_vars['errorMessages']['AreaCodeOfJobLocation5'] || $this->_tpl_vars['errorMessages']['PhonenOfJobLocation5']): ?>item-error<?php endif; ?> ">Telephone Number</span>
      <?php echo smarty_function_textfield(array('label' => "",'name' => 'AreaCodeOfJobLocation5','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "",'name' => 'PhonenOfJobLocation5','class' => '11_a'), $this);?>

    </div>
    <div class="set-default clearer">                    
      <?php echo smarty_function_textfield(array('label' => "Supervisor's Name & Street Address <em>if different than Job Location</em>",'name' => 'SupervisorNamepStreetAddress5','class' => '44a'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'CityCountryOfSupervisor5','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'StateOfSupervisor5','label' => 'State','class' => '8r_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'ZIPCodeOfSupervisor5','class' => '7_1'), $this);?>

      <span class="stext6  <?php if ($this->_tpl_vars['errorMessages']['AreaCodeOfSupervisor5'] || $this->_tpl_vars['errorMessages']['PhonenOfSupervisor5']): ?>item-error<?php endif; ?> ">Telephone Number</span>
      <?php echo smarty_function_textfield(array('label' => "",'name' => 'AreaCodeOfSupervisor5','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "",'name' => 'PhonenOfSupervisor5','class' => '11_a'), $this);?>

    </div>
    <div  class="set-default clearer">
      <div class="previous_period fl">Previous period</div>
      <div class="fl">
        <div class="set-default clearer">  
          <?php echo smarty_function_monthyear(array('name' => 'DateFrom30','label' => 'From','class' => "10-45_1"), $this);?>

          <?php echo smarty_function_monthyear(array('name' => 'DateTo30','label' => 'To','class' => "10_1-45_1"), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'PositionTitle13','class' => '16_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'Supervisor13','class' => '28a_1'), $this);?>

        </div>
        <div class="set-default clearer">
          <?php echo smarty_function_monthyear(array('name' => 'DateFrom31','label' => 'From','class' => "10-45_1"), $this);?>

          <?php echo smarty_function_monthyear(array('name' => 'DateTo31','label' => 'To','class' => "10_1-45_1"), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'PositionTitle14','class' => '16_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'Supervisor14','class' => '28a_1'), $this);?>

        </div> 
        <div class="set-default clearer">
          <?php echo smarty_function_monthyear(array('name' => 'DateFrom32','label' => 'From','class' => "10-45_1"), $this);?>

          <?php echo smarty_function_monthyear(array('name' => 'DateTo32','label' => 'To','class' => "10_1-45_1"), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'PositionTitle15','class' => '16_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'Supervisor15','class' => '28a_1'), $this);?>

        </div>
      </div>
    </div>
    </div>
  </div>
  
  
  
  <div class="area">
    <div class="set-default clearer">
      <div class="item-number fl">#6</div>
      <input type="checkbox"  onclick="unemployment(6)"  name="unemp6" value="6" id="unemp6" <?php if ($_POST['unemp6']): ?> checked="checked"<?php endif; ?> /><strong>Unemployment Period</strong><br />    
    </div>
    <div class="set-default clearer">
      <?php echo smarty_function_monthyear(array('name' => 'DateFrom33','onchange' => "fillTo('DateFrom33', 'aDateTo13')",'label' => 'From','class' => "10-45_1"), $this);?>

      <div class="present1 fl"><span class="stext6">To</span><br /><p id="DateTo33" class="for_date_to"><?php if ($_POST['MonthDateFrom29'] && $_POST['YearDateFrom29']):  echo $_POST['MonthDateFrom29']; ?>
/<?php echo $_POST['YearDateFrom29'];  endif; ?></p></div>
      <?php echo smarty_function_code19(array('name' => 'Code9','label' => 'Type of employment'), $this);?>

      <div class="block_position2"><?php echo smarty_function_textfield(array('label' => "Employer/Verifier <br /> Name/Military Duty Location",'name' => 'EmployersVerifierNamesMilitaryDutyLocation6','class' => '28a_1'), $this);?>
</div>
      <div class="block_position2" id="positionEmp6"><?php echo smarty_function_textfield(array('label' => "Your Position <br /> Title/Military Rank",'name' => 'PositionTitlesMilitaryRank6','class' => '15a_1'), $this);?>
</div>
      
    </div>
    <div id="empblock6" <?php if ($_POST['unemp6']): ?> style="display:none;" <?php endif; ?>>
    <div class="set-default clearer">                    
      <?php echo smarty_function_textfield(array('label' => "Employer's/Verifier's Street Address",'name' => 'EmployersVerifierStreetAddress6','class' => '44a'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'EmployersVerifierCityCountry6','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'EmployersVerifierState6','label' => 'State','class' => '8_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'EmployersVerifierZIPCode6','class' => '7_1'), $this);?>

      <span class="stext6  <?php if ($this->_tpl_vars['errorMessages']['AreaCodeOfEmployersVerifier6'] || $this->_tpl_vars['errorMessages']['PhonenOfEmployersVerifier6']): ?>item-error<?php endif; ?> ">Telephone Number</span>
      <?php echo smarty_function_textfield(array('label' => "",'name' => 'AreaCodeOfEmployersVerifier6','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "",'name' => 'PhonenOfEmployersVerifier6','class' => '11_a'), $this);?>

    </div>
    <div class="set-default clearer">                    
      <?php echo smarty_function_textfield(array('label' => "Street Address of Job Location <em>if different than Employer's Address</em>",'name' => 'StreetAddressOfJobLocation6','class' => '44a'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'CityCountryOfJobLocation6','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'StateOfJobLocation6','label' => 'State','class' => '8r_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'ZIPCodeOfJobLocation6','class' => '7_1'), $this);?>

      <span class="stext6  <?php if ($this->_tpl_vars['errorMessages']['AreaCodeOfJobLocation6'] || $this->_tpl_vars['errorMessages']['PhonenOfJobLocation6']): ?>item-error<?php endif; ?>">Telephone Number</span>
      <?php echo smarty_function_textfield(array('label' => "",'name' => 'AreaCodeOfJobLocation6','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "",'name' => 'PhonenOfJobLocation6','class' => '11_a'), $this);?>

    </div>
    <div class="set-default clearer">                    
      <?php echo smarty_function_textfield(array('label' => "Supervisor's Name & Street Address <em>if different than Job Location</em>",'name' => 'SupervisorNamepStreetAddress6','class' => '44a'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'CityCountryOfSupervisor6','class' => '12_1'), $this);?>

      <?php echo smarty_function_states(array('name' => 'StateOfSupervisor6','label' => 'State','class' => '8r_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'ZIPCodeOfSupervisor6','class' => '7_1'), $this);?>

      <span class="stext6  <?php if ($this->_tpl_vars['errorMessages']['AreaCodeOfSupervisor6'] || $this->_tpl_vars['errorMessages']['PhonenOfSupervisor6']): ?>item-error<?php endif; ?> ">Telephone Number</span>
      <?php echo smarty_function_textfield(array('label' => "",'name' => 'AreaCodeOfSupervisor6','class' => '4_1'), $this);?>

      <?php echo smarty_function_textfield(array('label' => "",'name' => 'PhonenOfSupervisor6','class' => '11_a'), $this);?>

    </div>
    <div class="set-default clearer">
      <div class="previous_period fl">Previous period</div>
      <div class="fl">
        <div class="set-default clearer">  
          <?php echo smarty_function_monthyear(array('name' => 'DateFrom34','label' => 'From','class' => "10-45_1"), $this);?>

          <?php echo smarty_function_monthyear(array('name' => 'DateTo34','label' => 'To','class' => "10_1-45_1"), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'PositionTitle16','class' => '16_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'Supervisor16','class' => '28a_1'), $this);?>

        </div>
        <div class="set-default clearer">
          <?php echo smarty_function_monthyear(array('name' => 'DateFrom35','label' => 'From','class' => "10-45_1"), $this);?>

          <?php echo smarty_function_monthyear(array('name' => 'DateTo35','label' => 'To','class' => "10_1-45_1"), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'PositionTitle17','class' => '16_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'Supervisor17','class' => '28a_1'), $this);?>

        </div> 
        <div class="set-default clearer">
          <?php echo smarty_function_monthyear(array('name' => 'DateFrom36','label' => 'From','class' => "10-45_1"), $this);?>

          <?php echo smarty_function_monthyear(array('name' => 'DateTo36','label' => 'To','class' => "10_1-45_1"), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'PositionTitle18','class' => '16_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'Supervisor18','class' => '28a_1'), $this);?>

        </div>
      </div>
    </div>
    </div>
  </div>
  <a href="javascript:showadd(10);" <?php if ($_POST['add10']): ?> style="display:none;" <?php endif; ?> id="a10show">More</a>
  <div <?php if (! $_POST['add10']): ?> style="display:none;" <?php endif; ?> id="a10"  >
    
   Enter more fields: 
    <fieldset>
      <div class="area_w">
        <div class="set-default clearer">
          <div class="item-number fl">#7</div>
          <input type="checkbox"  onclick="unemployment(7)"  name="unemp7" value="7" id="unemp7" <?php if ($_POST['unemp7']): ?> checked="checked"<?php endif; ?>  /><strong>Unemployment Period</strong><br />    
        </div>
        <div class="set-default clearer">
          <?php echo smarty_function_monthyear(array('name' => 'aDateFrom13','onchange' => "fillTo('aDateFrom13', 'aDateTo17')",'label' => 'From','class' => "10-45_1"), $this);?>

          <div class="present1 fl"><span class="stext6">To</span><br /><p id="aDateTo13" class="for_date_to"><?php if ($_POST['MonthDateFrom33'] && $_POST['YearDateFrom33']):  echo $_POST['MonthDateFrom33']; ?>
/<?php echo $_POST['YearDateFrom33'];  endif; ?></p></div>
          <?php echo smarty_function_code19(array('name' => 'aCode4','label' => 'Type of employment'), $this);?>

          <div class="block_position2"><?php echo smarty_function_textfield(array('label' => "Employer/Verifier <br /> Name/Military Duty Location",'name' => 'aEmployersVerifierNamesMilitaryDutyLocation1','class' => '28a_1'), $this);?>
</div>
          <div class="block_position2" id="positionEmp7"><?php echo smarty_function_textfield(array('label' => "Your Position <br /> Title/Military Rank",'name' => 'aPositionTitlesMilitaryRank','class' => '15a_1'), $this);?>
</div>
          
        </div>
    <div id="empblock7" <?php if ($_POST['unemp7']): ?> style="display:none;" <?php endif; ?>>
        <div class="set-default clearer">                    
          <?php echo smarty_function_textfield(array('label' => "Employer's/Verifier's Street Address",'name' => 'aEmployersVerifierStreetAddress1','class' => '44a'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'aEmployersVerifierCityCountry1','class' => '12_1'), $this);?>

          <?php echo smarty_function_states(array('name' => 'aEmployersVerifierState1','label' => 'State','class' => '8_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'aEmployersVerifierZIPCode1','class' => '7_1'), $this);?>

          <span class="stext6  <?php if ($this->_tpl_vars['errorMessages']['aAreaCodeOfEmployersVerifier1'] || $this->_tpl_vars['errorMessages']['aPhonenOfEmployersVerifier1']): ?>item-error<?php endif; ?> ">Telephone Number</span>
          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aAreaCodeOfEmployersVerifier1','class' => '4_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aPhonenOfEmployersVerifier1','class' => '11_a'), $this);?>

        </div>
        <div class="set-default clearer">                    
          <?php echo smarty_function_textfield(array('label' => "Street Address of Job Location <em>if different than Employer's Address</em>",'name' => 'aStreetAddressOfJobLocation1','class' => '44a'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'aCityCountryOfJobLocation1','class' => '12_1'), $this);?>

          <?php echo smarty_function_states(array('name' => 'aStateOfJobLocation1','label' => 'State','class' => '8r_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'aZIPCodeOfJobLocation1','class' => '7_1'), $this);?>

          <span class="stext6  <?php if ($this->_tpl_vars['errorMessages']['aAreaCodeOfJobLocation1'] || $this->_tpl_vars['errorMessages']['aPhonenOfJobLocation1']): ?>item-error<?php endif; ?>">Telephone Number</span>
          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aAreaCodeOfJobLocation1','class' => '4_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aPhonenOfJobLocation1','class' => '11_a'), $this);?>

        </div>
        <div class="set-default clearer">                    
          <?php echo smarty_function_textfield(array('label' => "Supervisor's Name & Street Address <em>if different than Job Location</em>",'name' => 'aSupervisorNamepStreetAddress1','class' => '44a'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'aCityCountryOfSupervisor1','class' => '12_1'), $this);?>

          <?php echo smarty_function_states(array('name' => 'aStateOfSupervisor1','label' => 'State','class' => '8r_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'aZIPCodeOfSupervisor1','class' => '7_1'), $this);?>

          <span class="stext6  <?php if ($this->_tpl_vars['errorMessages']['aAreaCodeOfSupervisor1'] || $this->_tpl_vars['errorMessages']['aPhonenOfSupervisor1']): ?>item-error<?php endif; ?> ">Telephone Number</span>
          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aAreaCodeOfSupervisor1','class' => '4_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aPhonenOfSupervisor1','class' => '11_a'), $this);?>

        </div>
        <div  class="set-default clearer">
          <div class="previous_period fl">Previous period</div>
          <div class="fl">
            <div class="set-default clearer">  
              <?php echo smarty_function_monthyear(array('name' => 'aDateFrom14','label' => 'From','class' => "10-45_1"), $this);?>

              <?php echo smarty_function_monthyear(array('name' => 'aDateTo14','label' => 'To','class' => "10_1-45_1"), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'aPositionTitle1','class' => '16_1'), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'aSupervisor1','class' => '28a_1'), $this);?>

            </div>
            <div class="set-default clearer">
              <?php echo smarty_function_monthyear(array('name' => 'aDateFrom15','label' => 'From','class' => "10-45_1"), $this);?>

              <?php echo smarty_function_monthyear(array('name' => 'aDateTo15','label' => 'To','class' => "10_1-45_1"), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'aPositionTitle2','class' => '16_1'), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'aSupervisor2','class' => '28a_1'), $this);?>

            </div> 
            <div class="set-default clearer">
              <?php echo smarty_function_monthyear(array('name' => 'aDateFrom16','label' => 'From','class' => "10-45_1"), $this);?>

              <?php echo smarty_function_monthyear(array('name' => 'aDateTo16','label' => 'To','class' => "10_1-45_1"), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'aPositionTitle3','class' => '16_1'), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'aSupervisor3','class' => '28a_1'), $this);?>

            </div>
          </div>
        </div>
    </div>
      </div>  
      <div class="area">
        <div class="set-default clearer">
          <div class="item-number fl">#8</div>
          <input type="checkbox"  onclick="unemployment(8)"  name="unemp8" value="8" id="unemp8" <?php if ($_POST['unemp8']): ?> checked="checked"<?php endif; ?>  /><strong>Unemployment Period</strong><br />    
        </div>
        <div class="set-default clearer">
          <?php echo smarty_function_monthyear(array('name' => 'aDateFrom17','onchange' => "fillTo('aDateFrom17', 'aDateTo21')",'label' => 'From','class' => "10-45_1"), $this);?>

          <div class="present1 fl"><span class="stext6">To</span><br /><p id="aDateTo17" class="for_date_to"><?php if ($_POST['MonthDateFrom13'] && $_POST['aYearDateFrom13']):  echo $_POST['aMonthDateFrom13']; ?>
/<?php echo $_POST['YearDateFrom13'];  endif; ?></p></div>
          <?php echo smarty_function_code19(array('name' => 'aCode5','label' => 'Type of employment'), $this);?>

          <div class="block_position2"><?php echo smarty_function_textfield(array('label' => "Employer/Verifier <br /> Name/Military Duty Location",'name' => 'aEmployersVerifierNamesMilitaryDutyLocation2','class' => '28a_1'), $this);?>
</div>
          <div class="block_position2" id="positionEmp8"><?php echo smarty_function_textfield(array('label' => "Your Position <br /> Title/Military Rank",'name' => 'aPositionTitlesMilitaryRank2','class' => '15a_1'), $this);?>
</div>
          
        </div>
    <div id="empblock8" <?php if ($_POST['unemp8']): ?> style="display:none;" <?php endif; ?>>
        <div class="set-default clearer">                    
          <?php echo smarty_function_textfield(array('label' => "Employer's/Verifier's Street Address",'name' => 'aEmployersVerifierStreetAddress2','class' => '44a'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'aEmployersVerifierCityCountry2','class' => '12_1'), $this);?>

          <?php echo smarty_function_states(array('name' => 'aEmployersVerifierState2','label' => 'State','class' => '8_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'aEmployersVerifierZIPCode2','class' => '7_1'), $this);?>

          <span class="stext6  <?php if ($this->_tpl_vars['errorMessages']['aAreaCodeOfEmployersVerifier2'] || $this->_tpl_vars['errorMessages']['aPhonenOfEmployersVerifier2']): ?>item-error<?php endif; ?> ">Telephone Number</span>
          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aAreaCodeOfEmployersVerifier2','class' => '4_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aPhonenOfEmployersVerifier2','class' => '11_a'), $this);?>

        </div>
        <div class="set-default clearer">                    
          <?php echo smarty_function_textfield(array('label' => "Street Address of Job Location <em>if different than Employer's Address</em>",'name' => 'aStreetAddressOfJobLocation2','class' => '44a'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'aCityCountryOfJobLocation2','class' => '12_1'), $this);?>

          <?php echo smarty_function_states(array('name' => 'aStateOfJobLocation2','label' => 'State','class' => '8r_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'aZIPCodeOfJobLocation2','class' => '7_1'), $this);?>

          <span class="stext6 <?php if ($this->_tpl_vars['errorMessages']['aAreaCodeOfJobLocation2'] || $this->_tpl_vars['errorMessages']['aPhonenOfJobLocation2']): ?>item-error<?php endif; ?> ">Telephone Number</span>
          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aAreaCodeOfJobLocation2','class' => '4_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aPhonenOfJobLocation2','class' => '11_a'), $this);?>

        </div>
        <div class="set-default clearer">                    
          <?php echo smarty_function_textfield(array('label' => "Supervisor's Name & Street Address <em>if different than Job Location</em>",'name' => 'aSupervisorNamepStreetAddress2','class' => '44a'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'aCityCountryOfSupervisor2','class' => '12_1'), $this);?>

          <?php echo smarty_function_states(array('name' => 'aStateOfSupervisor2','label' => 'State','class' => '8r_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'aZIPCodeOfSupervisor2','class' => '7_1'), $this);?>

          <span class="stext6  <?php if ($this->_tpl_vars['errorMessages']['aAreaCodeOfSupervisor2'] || $this->_tpl_vars['errorMessages']['aPhonenOfSupervisor2']): ?>item-error<?php endif; ?> ">Telephone Number</span>
          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aAreaCodeOfSupervisor2','class' => '4_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aPhonenOfSupervisor2','class' => '11_a'), $this);?>

        </div>
        <div class="set-default clearer">
          <div class="previous_period fl">Previous period</div>
          <div class="fl">
            <div class="set-default clearer">  
              <?php echo smarty_function_monthyear(array('name' => 'aDateFrom18','label' => 'From','class' => "10-45_1"), $this);?>

              <?php echo smarty_function_monthyear(array('name' => 'aDateTo18','label' => 'To','class' => "10_1-45_1"), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'aPositionTitle4','class' => '16_1'), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'aSupervisor4','class' => '28a_1'), $this);?>

            </div>
            <div class="set-default clearer">
              <?php echo smarty_function_monthyear(array('name' => 'aDateFrom19','label' => 'From','class' => "10-45_1"), $this);?>

              <?php echo smarty_function_monthyear(array('name' => 'aDateTo19','label' => 'To','class' => "10_1-45_1"), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'aPositionTitle5','class' => '16_1'), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'aSupervisor5','class' => '28a_1'), $this);?>

            </div> 
            <div class="set-default clearer">
              <?php echo smarty_function_monthyear(array('name' => 'aDateFrom20','label' => 'From','class' => "10-45_1"), $this);?>

              <?php echo smarty_function_monthyear(array('name' => 'aDateTo20','label' => 'To','class' => "10_1-45_1"), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'aPositionTitle6','class' => '16_1'), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'aSupervisor6','class' => '28a_1'), $this);?>

            </div>
          </div>
        </div>
    </div>
      </div>
      <div class="area_w">
        <div class="set-default clearer">
          <div class="item-number fl">#9</div>
          <input type="checkbox"  onclick="unemployment(9)"  name="unemp9" value="9" id="unemp9" <?php if ($_POST['unemp9']): ?> checked="checked"<?php endif; ?>  /><strong>Unemployment Period</strong><br />    
        </div>
        <div class="set-default clearer">
          <?php echo smarty_function_monthyear(array('name' => 'aDateFrom21','onchange' => "fillTo('aDateFrom21', 'aDateTo25')",'label' => 'From','class' => "10-45_1"), $this);?>

          <div class="present1 fl"><span class="stext6">To</span><br /><p id="aDateTo21" class="for_date_to"><?php if ($_POST['aMonthDateFrom17'] && $_POST['aYearDateFrom17']):  echo $_POST['MonthDateFrom17']; ?>
/<?php echo $_POST['YearDateFrom17'];  endif; ?></p></div>
          <?php echo smarty_function_code19(array('name' => 'aCode6','label' => 'Type of employment'), $this);?>

          <div class="block_position2"><?php echo smarty_function_textfield(array('label' => "Employer/Verifier <br /> Name/Military Duty Location",'name' => 'aEmployersVerifierNamesMilitaryDutyLocation3','class' => '28a_1'), $this);?>
</div>
          <div class="block_position2" id="positionEmp9"><?php echo smarty_function_textfield(array('label' => "Your Position <br /> Title/Military Rank",'name' => 'aPositionTitlesMilitaryRank3','class' => '15a_1'), $this);?>
</div>
          
        </div>
    <div id="empblock9" <?php if ($_POST['unemp9']): ?> style="display:none;" <?php endif; ?>>
        <div class="set-default clearer">                    
          <?php echo smarty_function_textfield(array('label' => "Employer's/Verifier's Street Address",'name' => 'aEmployersVerifierStreetAddress3','class' => '44a'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'aEmployersVerifierCityCountry3','class' => '12_1'), $this);?>

          <?php echo smarty_function_states(array('name' => 'aEmployersVerifierState3','label' => 'State','class' => '8_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'aEmployersVerifierZIPCode3','class' => '7_1'), $this);?>

          <span class="stext6  <?php if ($this->_tpl_vars['errorMessages']['aAreaCodeOfEmployersVerifier3'] || $this->_tpl_vars['errorMessages']['aPhonenOfEmployersVerifier3']): ?>item-error<?php endif; ?>">Telephone Number</span>
          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aAreaCodeOfEmployersVerifier3','class' => '4_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aPhonenOfEmployersVerifier3','class' => '11_a'), $this);?>

        </div>
        <div class="set-default clearer">                    
          <?php echo smarty_function_textfield(array('label' => "Street Address of Job Location <em>if different than Employer's Address</em>",'name' => 'aStreetAddressOfJobLocation3','class' => '44a'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'aCityCountryOfJobLocation3','class' => '12_1'), $this);?>

          <?php echo smarty_function_states(array('name' => 'aStateOfJobLocation3','label' => 'State','class' => '8r_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'aZIPCodeOfJobLocation3','class' => '7_1'), $this);?>

          <span class="stext6 <?php if ($this->_tpl_vars['errorMessages']['aAreaCodeOfJobLocation3'] || $this->_tpl_vars['errorMessages']['aPhonenOfJobLocation3']): ?>item-error<?php endif; ?> ">Telephone Number</span>
          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aAreaCodeOfJobLocation3','class' => '4_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aPhonenOfJobLocation3','class' => '11_a'), $this);?>

        </div>
        <div class="set-default clearer">                    
          <?php echo smarty_function_textfield(array('label' => "Supervisor's Name & Street Address <em>if different than Job Location</em>",'name' => 'aSupervisorNamepStreetAddress3','class' => '44a'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'aCityCountryOfSupervisor3','class' => '12_1'), $this);?>

          <?php echo smarty_function_states(array('name' => 'aStateOfSupervisor3','label' => 'State','class' => '8r_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'aZIPCodeOfSupervisor3','class' => '7_1'), $this);?>

          <span class="stext6  <?php if ($this->_tpl_vars['errorMessages']['aAreaCodeOfSupervisor3'] || $this->_tpl_vars['errorMessages']['aPhonenOfSupervisor3']): ?>item-error<?php endif; ?> ">Telephone Number</span>
          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aAreaCodeOfSupervisor3','class' => '4_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aPhonenOfSupervisor3','class' => '11_a'), $this);?>

        </div>
        <div  class="set-default clearer">
          <div class="previous_period fl">Previous period</div>
          <div class="fl">
            <div class="set-default clearer">  
              <?php echo smarty_function_monthyear(array('name' => 'aDateFrom22','label' => 'From','class' => "10-45_1"), $this);?>

              <?php echo smarty_function_monthyear(array('name' => 'aDateTo22','label' => 'To','class' => "10_1-45_1"), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'aPositionTitle7','class' => '16_1'), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'aSupervisor7','class' => '28a_1'), $this);?>

            </div>
            <div class="set-default clearer">
              <?php echo smarty_function_monthyear(array('name' => 'aDateFrom23','label' => 'From','class' => "10-45_1"), $this);?>

              <?php echo smarty_function_monthyear(array('name' => 'aDateTo23','label' => 'To','class' => "10_1-45_1"), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'aPositionTitle8','class' => '16_1'), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'aSupervisor8','class' => '28a_1'), $this);?>

            </div> 
            <div class="set-default clearer">
              <?php echo smarty_function_monthyear(array('name' => 'aDateFrom24','label' => 'From','class' => "10-45_1"), $this);?>

              <?php echo smarty_function_monthyear(array('name' => 'aDateTo24','label' => 'To','class' => "10_1-45_1"), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'aPositionTitle9','class' => '16_1'), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'aSupervisor9','class' => '28a_1'), $this);?>

            </div>
          </div>
        </div>
    </div>
      </div>  
      <div class="area">
        <div class="set-default clearer">
          <div class="item-number fl">#10</div>
          <input type="checkbox"  onclick="unemployment(10)"  name="unemp10" value="10" id="unemp10" <?php if ($_POST['unemp10']): ?> checked="checked"<?php endif; ?>  /><strong>Unemployment Period</strong><br />    
        </div>
        <div class="set-default clearer">
          <?php echo smarty_function_monthyear(array('name' => 'aDateFrom25','label' => 'From','class' => "10-45_1"), $this);?>

          <div class="present1 fl"><span class="stext6">To</span><br /><p id="aDateTo25" class="for_date_to"><?php if ($_POST['aMonthDateFrom21'] && $_POST['aYearDateFrom21']):  echo $_POST['MonthDateFrom21']; ?>
/<?php echo $_POST['YearDateFrom21'];  endif; ?></p></div>
          <?php echo smarty_function_code19(array('name' => 'aCode7','label' => 'Type of employment'), $this);?>

          <div class="block_position2"><?php echo smarty_function_textfield(array('label' => "Employer/Verifier <br /> Name/Military Duty Location",'name' => 'aEmployersVerifierNamesMilitaryDutyLocation4','class' => '28a_1'), $this);?>
</div>
          <div class="block_position2" id="positionEmp10"><?php echo smarty_function_textfield(array('label' => "Your Position <br /> Title/Military Rank",'name' => 'aPositionTitlesMilitaryRank4','class' => '15a_1'), $this);?>
</div>
          
        </div>
    <div id="empblock10" <?php if ($_POST['unemp10']): ?> style="display:none;" <?php endif; ?>>
        <div class="set-default clearer">                    
          <?php echo smarty_function_textfield(array('label' => "Employer's/Verifier's Street Address",'name' => 'aEmployersVerifierStreetAddress4','class' => '44a'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'aEmployersVerifierCityCountry4','class' => '12_1'), $this);?>

          <?php echo smarty_function_states(array('name' => 'aEmployersVerifierState4','label' => 'State','class' => '8_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'aEmployersVerifierZIPCode4','class' => '7_1'), $this);?>

          <span class="stext6   <?php if ($this->_tpl_vars['errorMessages']['aAreaCodeOfEmployersVerifier4'] || $this->_tpl_vars['errorMessages']['aPhonenOfEmployersVerifier4']): ?>item-error<?php endif; ?>">Telephone Number</span>
          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aAreaCodeOfEmployersVerifier4','class' => '4_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aPhonenOfEmployersVerifier4','class' => '11_a'), $this);?>

        </div>
        <div class="set-default clearer">                    
          <?php echo smarty_function_textfield(array('label' => "Street Address of Job Location <em>if different than Employer's Address</em>",'name' => 'aStreetAddressOfJobLocation4','class' => '44a'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'aCityCountryOfJobLocation4','class' => '12_1'), $this);?>

          <?php echo smarty_function_states(array('name' => 'aStateOfJobLocation4','label' => 'State','class' => '8r_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'aZIPCodeOfJobLocation4','class' => '7_1'), $this);?>

          <span class="stext6 <?php if ($this->_tpl_vars['errorMessages']['aAreaCodeOfJobLocation4'] || $this->_tpl_vars['errorMessages']['aPhonenOfJobLocation4']): ?>item-error<?php endif; ?> ">Telephone Number</span>
          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aAreaCodeOfJobLocation4','class' => '4_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aPhonenOfJobLocation4','class' => '11_a'), $this);?>

        </div>
        <div class="set-default clearer">                    
          <?php echo smarty_function_textfield(array('label' => "Supervisor's Name & Street Address <em>if different than Job Location</em>",'name' => 'aSupervisorNamepStreetAddress4','class' => '44a'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "City (Country)",'name' => 'aCityCountryOfSupervisor4','class' => '12_1'), $this);?>

          <?php echo smarty_function_states(array('name' => 'aStateOfSupervisor4','label' => 'State','class' => '8r_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => 'ZipCode','name' => 'aZIPCodeOfSupervisor4','class' => '7_1'), $this);?>

          <span class="stext6  <?php if ($this->_tpl_vars['errorMessages']['aAreaCodeOfSupervisor4'] || $this->_tpl_vars['errorMessages']['aPhonenOfSupervisor4']): ?>item-error<?php endif; ?> ">Telephone Number</span>
          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aAreaCodeOfSupervisor4','class' => '4_1'), $this);?>

          <?php echo smarty_function_textfield(array('label' => "",'name' => 'aPhonenOfSupervisor4','class' => '11_a'), $this);?>

        </div>
        <div class="set-default clearer">
          <div class="previous_period fl">Previous period</div>
          <div class="fl">
            <div class="set-default clearer">  
              <?php echo smarty_function_monthyear(array('name' => 'aDateFrom26','label' => 'From','class' => "10-45_1"), $this);?>

              <?php echo smarty_function_monthyear(array('name' => 'aDateTo26','label' => 'To','class' => "10_1-45_1"), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'aPositionTitle10','class' => '16_1'), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'aSupervisor10','class' => '28a_1'), $this);?>

            </div>
            <div class="set-default clearer">
              <?php echo smarty_function_monthyear(array('name' => 'aDateFrom27','label' => 'From','class' => "10-45_1"), $this);?>

              <?php echo smarty_function_monthyear(array('name' => 'aDateTo27','label' => 'To','class' => "10_1-45_1"), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'aPositionTitle11','class' => '16_1'), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'aSupervisor11','class' => '28a_1'), $this);?>

            </div> 
            <div class="set-default clearer">
              <?php echo smarty_function_monthyear(array('name' => 'aDateFrom28','label' => 'From','class' => "10-45_1"), $this);?>

              <?php echo smarty_function_monthyear(array('name' => 'aDateTo28','label' => 'To','class' => "10_1-45_1"), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Position Title','name' => 'aPositionTitle12','class' => '16_1'), $this);?>

              <?php echo smarty_function_textfield(array('label' => 'Supervisor','name' => 'aSupervisor12','class' => '28a_1'), $this);?>

            </div>
          </div>
        </div>
    </div>
      </div>
    </fieldset>
    
    <a href="javascript:hideadd(10);" id="a10hide">Hide More</a>
<input type="hidden" name="add10" value="<?php echo $_POST['add10']; ?>
" id="add10" value="0" />
</div>
<!-- ----------------------------------------------------    11     ------------------------------------------------ -->
<fieldset><legend><span>11.</span> PEOPLE WHO KNOW YOU WELL</legend>
            <p class="note">List three people who know you well and live in the United States. 
            They should be good friends, peers, colleagues, college roommates, etc., whose combined association with you covers as well as possible the last 5 years.
            Do not list your spouse, former spouses, or other relatives, and try not to list anyone who is listed elsewhere on this form.</p>       
            <div class="area">
                <div class="set-default clearer">
                    <div class="item-number fl">#1</div>
                    <?php echo smarty_function_textfield(array('label' => 'Name','name' => 'NameOfPersonWhoKnowYouWell1','class' => '30'), $this);?>

                    <?php echo smarty_function_monthyear(array('name' => 'DateKnownFrom1','label' => 'Dates Known','class' => "06a_1a-45_1"), $this);?>

                    <?php echo smarty_function_monthyear(array('name' => 'DateKnownTo1','label' => 'To','class' => "6a_1a-45_1"), $this);?>
                   
                    <span class="stext1 <?php if ($this->_tpl_vars['errorMessages']['time1']): ?>item-error<?php endif; ?> <?php if ($this->_tpl_vars['errorMessages']['AreaCodeOfPerson1']): ?>item-error<?php endif; ?> <?php if ($this->_tpl_vars['errorMessages']['PhonenOfPerson1']): ?>item-error<?php endif; ?>">Telephone Number</span>
                    <div class="for_radio3 fl left_marg1a "><?php echo smarty_function_radiovertical(array('name' => 'time1','labels' => "Day|Night",'values' => "DaytimePhone1|NighttimePhone1",'class' => '3'), $this);?>
</div>
                    <?php echo smarty_function_textfield(array('label' => "",'name' => 'AreaCodeOfPerson1','class' => '4'), $this);?>

                    <?php echo smarty_function_textfield(array('label' => "",'name' => 'PhonenOfPerson1','class' => '16a_a'), $this);?>

                </div>
                <div class="set-default clearer left_marg2a">
                    <?php echo smarty_function_textfield(array('label' => 'Home or Work Address','name' => 'HomesWorkAddressOfPersonWhoKnowYouWell1','class' => '43a'), $this);?>

                    <?php echo smarty_function_textfield(array('label' => "City(Country)",'name' => 'CityCountryOfPersonPersonWhoKnowYouWell1','class' => '19_1'), $this);?>

                    <?php echo smarty_function_states(array('name' => 'StateOfPersonWhoKnowYouWell1','label' => 'State','class' => '8_1a'), $this);?>

                    <?php echo smarty_function_textfield(array('label' => 'Zip Code','name' => 'ZIPCodeOfPersonWhoKnowYouWell1','class' => '12_1a'), $this);?>

                </div>
            </div>
            <div class="set-default clearer">
                <div class="item-number fl">#2</div>
                <?php echo smarty_function_textfield(array('label' => 'Name','name' => 'NameOfPersonWhoKnowYouWell2','class' => '30'), $this);?>

                <?php echo smarty_function_monthyear(array('name' => 'DateKnownFrom2','label' => 'Dates Known','class' => "06a_1a-45_1"), $this);?>

                <?php echo smarty_function_monthyear(array('name' => 'DateKnownTo2','label' => 'To','class' => "6a_1a-45_1"), $this);?>
                   
                <span class="stext1 <?php if ($this->_tpl_vars['errorMessages']['time2']): ?>item-error<?php endif; ?> <?php if ($this->_tpl_vars['errorMessages']['AreaCodeOfPerson2']): ?>item-error<?php endif; ?> 
                <?php if ($this->_tpl_vars['errorMessages']['PhonenOfPerson2']): ?>item-error<?php endif; ?>">Telephone Number</span>
                <div class="for_radio3 fl left_marg1a "><?php echo smarty_function_radiovertical(array('name' => 'time2','labels' => "Day|Night",'values' => "DaytimePhone2|NighttimePhone2",'class' => '3'), $this);?>
</div>
                <?php echo smarty_function_textfield(array('label' => "",'name' => 'AreaCodeOfPerson2','class' => '4'), $this);?>

                <?php echo smarty_function_textfield(array('label' => "",'name' => 'PhonenOfPerson2','class' => '16a_a'), $this);?>

            </div>
            <div class="set-default clearer left_marg2a">
                <?php echo smarty_function_textfield(array('label' => 'Home or Work Address','name' => 'HomesWorkAddressOfPersonWhoKnowYouWell2','class' => '43a'), $this);?>

                <?php echo smarty_function_textfield(array('label' => "City(Country)",'name' => 'CityCountryOfPersonPersonWhoKnowYouWell2','class' => '19_1'), $this);?>

                <?php echo smarty_function_states(array('name' => 'StateOfPersonWhoKnowYouWell2','label' => 'State','class' => '8_1a'), $this);?>

                <?php echo smarty_function_textfield(array('label' => 'Zip Code','name' => 'ZIPCodeOfPersonWhoKnowYouWell2','class' => '12_1a'), $this);?>

            </div>
            <div class="area">
                <div class="set-default clearer">
                    <div class="item-number fl">#3</div>
                    <?php echo smarty_function_textfield(array('label' => 'Name','name' => 'NameOfPersonWhoKnowYouWell3','class' => '30'), $this);?>

                    <?php echo smarty_function_monthyear(array('name' => 'DateKnownFrom3','label' => 'Dates Known','class' => "06a_1a-45_1"), $this);?>

                    <?php echo smarty_function_monthyear(array('name' => 'DateKnownTo3','label' => 'To','class' => "6a_1a-45_1"), $this);?>
                   
                    <span class="stext1 <?php if ($this->_tpl_vars['errorMessages']['time3']): ?>item-error<?php endif; ?> <?php if ($this->_tpl_vars['errorMessages']['AreaCodeOfPerson3']): ?>item-error<?php endif; ?> <?php if ($this->_tpl_vars['errorMessages']['PhonenOfPerson1']): ?>item-error<?php endif; ?>">Telephone Number</span>
                    <div class="for_radio3 fl left_marg1a "><?php echo smarty_function_radiovertical(array('name' => 'time3','labels' => "Day|Night",'values' => "DaytimePhone3|NighttimePhone3",'class' => '3'), $this);?>
</div>
                    <?php echo smarty_function_textfield(array('label' => "",'name' => 'AreaCodeOfPerson3','class' => '4'), $this);?>

                    <?php echo smarty_function_textfield(array('label' => "",'name' => 'PhonenOfPerson3','class' => '16a_a'), $this);?>

                </div>
                <div class="set-default clearer left_marg2a">
                    <?php echo smarty_function_textfield(array('label' => 'Home or Work Address','name' => 'HomesWorkAddressOfPersonWhoKnowYouWell3','class' => '43a'), $this);?>

                    <?php echo smarty_function_textfield(array('label' => "City(Country)",'name' => 'CityCountryOfPersonPersonWhoKnowYouWell3','class' => '19_1'), $this);?>

                    <?php echo smarty_function_states(array('name' => 'StateOfPersonWhoKnowYouWell3','label' => 'State','class' => '8_1a'), $this);?>

                    <?php echo smarty_function_textfield(array('label' => 'Zip Code','name' => 'ZIPCodeOfPersonWhoKnowYouWell3','class' => '12_1a'), $this);?>

                </div>
            </div>
        </fieldset>
        <fieldset><legend><span>12.</span> YOUR SELECTIVE SERVICE RECORD <span id="selServiceLabel"></span></legend>
            <ol class="set-nolabel list-lalpha" id="selService">
                
                <li>
                    <span class="stext <?php if ($this->_tpl_vars['errorMessages']['sss']): ?>item-error<?php endif; ?>">Have you registered with the Selective Service System? If "Yes," provide your registration number.
                    If "No," show the reason for your legal exemption below.</span>
                    <?php echo smarty_function_radiohorisontal(array('onclick' => "selServiceYesNo()",'name' => 'sss','labels' => "Yes|No",'values' => "Yes|No"), $this);?>
 <br clear="all" />
                    <div id="RegistrationNumberblock" <?php if ($_POST['sss'] != 'Yes'): ?> style="display: none" <?php endif; ?>><?php echo smarty_function_textfield(array('label' => 'Registration Number','name' => 'RegistrationNumber','class' => '23'), $this);?>
</div>
                    <div id="LegalExemptionExplanationblock" <?php if ($_POST['sss'] != 'No'): ?> style="display: none" <?php endif; ?>><?php echo smarty_function_textfield(array('label' => 'Legal Exemption Explanation','name' => 'LegalExemptionExplanation','class' => '65_1a'), $this);?>
</div>
                </li>
            </ol>
        </fieldset>
        <fieldset><legend><span>13.</span> YOUR MILITARY HISTORY</legend>
            <ol class="set-nolabel list-lalpha">
                <li>
                    <span class="stext <?php if ($this->_tpl_vars['errorMessages']['military']): ?>item-error<?php endif; ?>">Have you served in the United States military?</span>
                    <?php echo smarty_function_radiohorisontal(array('name' => 'military','onclick' => "militaryPeriods()",'labels' => "Yes|No",'values' => "Yes|No"), $this);?>

                </li>
                <li>
                    <span class="stext <?php if ($this->_tpl_vars['errorMessages']['marine']): ?>item-error<?php endif; ?>">Have you served in the United States Merchant Marine?</span>
                    <?php echo smarty_function_radiohorisontal(array('name' => 'marine','onclick' => "militaryPeriods()",'labels' => "Yes|No",'values' => "Yes|No"), $this);?>
<br clear="all" />
                    <div id="militaryPeriods1" <?php if ($_POST['military'] != 'Yes' && $_POST['marine'] != 'Yes'): ?> style="display: none" <?php endif; ?>>
                    <span class="stext disp_b note2_2">List all of your military service below, including service in Reserve, National Guard, and U.S. Merchant Marine. Start with the most recent period 
                    of service (#1) and work backward. If you had a break in service, each separate period should be listed.</span>
                    <span class="stext note2_2a left_marg2a disp_b"><strong>Status.</strong> Choose the appropriate block for the status of your service during the time that you served. If your service was in 
                    the National Guard, use the two-letter code for the state to mark the block.</span>
                    <span class="stext note2_1a left_marg2a disp_b"><strong>Country.</strong> If your service was with other than the U.S. Armed Forces, identify the country for which you served.</span>
                    </div>
                </li>
            </ol>
            <div id="militaryPeriods2" <?php if ($_POST['military'] != 'Yes' && $_POST['marine'] != 'Yes'): ?> style="display: none" <?php endif; ?>>
            <div class="set-default clearer note2_2">
                <div class="item-number fl">#1</div>
                <?php echo smarty_function_monthyear(array('name' => 'DateFrom37','label' => 'From','class' => "6a-45_1"), $this);?>

                <?php echo smarty_function_monthyear(array('name' => 'DateTo37','label' => 'To','class' => "6a_1a-45_1"), $this);?>

                <?php echo smarty_function_code17(array('name' => 'Code10','label' => "Code:",'class' => '15_1a'), $this);?>

                <?php echo smarty_function_textfield(array('label' => "Service/Certificate #",'name' => 'ServicesCertificaten1','class' => '24_2a'), $this);?>

                <div class="for_radio4 fl left_marg1 "><?php echo smarty_function_radiovertical(array('name' => 'rank1','labels' => "Officer|Enlisted",'values' => "Officer1|Enlisted1",'class' => '3'), $this);?>
</div>
                <div class="block_position1"><?php echo smarty_function_states(array('name' => 'StatusNationalGuard1','label' => "National<br /> Guard ",'class' => '9'), $this);?>
</div>
            </div>
            <div class="set-default clearer left_marg2a">
                <?php echo smarty_function_status(array('name' => 'status1','label' => 'Status','class' => '10a'), $this);?>

                <?php echo smarty_function_textfield(array('label' => 'Country','name' => 'Country1','class' => '10a_1a'), $this);?>

                <!--<?php echo smarty_function_textfield(array('label' => 'Branch From Type of Discharge','name' => "",'class' => '39_2a'), $this);?>
-->
            </div>
            <div class="area">
                <div class="set-default clearer note2_2">
                    <div class="item-number fl">#2</div>
                    <?php echo smarty_function_monthyear(array('name' => 'DateFrom38','label' => 'From','class' => "6a-45_1"), $this);?>

                    <?php echo smarty_function_monthyear(array('name' => 'DateTo38','label' => 'To','class' => "6a_1a-45_1"), $this);?>

                    <?php echo smarty_function_code17(array('name' => 'Code11','label' => "Code:",'class' => '15_1a'), $this);?>

                    <?php echo smarty_function_textfield(array('label' => "Service/Certificate #",'name' => 'ServicesCertificaten2','class' => '24_2a'), $this);?>

                    <div class="for_radio4 fl left_marg1 "><?php echo smarty_function_radiovertical(array('name' => 'rank2','labels' => "Officer|Enlisted",'values' => "Officer2|Enlisted2",'class' => '3'), $this);?>
</div>
                    <div class="block_position1"><?php echo smarty_function_states(array('name' => 'StatusNationalGuard2','label' => "National<br /> Guard ",'class' => '9'), $this);?>
</div>
                </div>
                <div class="set-default clearer left_marg2a">
                    <?php echo smarty_function_status(array('name' => 'status2','label' => 'Status','class' => '10a'), $this);?>

                    <?php echo smarty_function_textfield(array('label' => 'Country','name' => 'Country2','class' => '10a_1a'), $this);?>

                    <!--<?php echo smarty_function_textfield(array('label' => 'Branch From Type of Discharge','name' => "",'class' => '39_2a'), $this);?>
-->
                </div>
            </div>
            </div>
        </fieldset>
        <fieldset><legend><span>14.</span> ILLEGAL  DRUGS</legend>
            <div class="note"><p>In the last year, have you used, possessed, supplied, or manufactured illegal drugs? When used without a prescription, illegal drugs 
            include marijuana, cocaine, hashish, narcotics (opium, morphine, codeine, heroin, etc.), stimulants (cocaine, amphetamines, etc.), 
            depressants (barbiturates, methaqualone, tranquilizers, etc.), hallucinogenics (LSD, PCP, etc.). (<strong>NOTE:</strong> Neither your truthful response 
            nor information derived from your response will be used as evidence against you in any subsequent criminal proceeding.)</p></div>
            <?php echo smarty_function_radiohorisontal(array('onclick' => "drugsPeriods()",'name' => 'drugs','labels' => "Yes|No",'values' => "Yes|No"), $this);?>
<br clear="all" />
            <div id="drugsPeriods" <?php if ($_POST['drugs'] != 'Yes'): ?> style="display: none" <?php endif; ?>>
            <div class="set-default clearer left_marg2a">
                <?php echo smarty_function_monthyear(array('name' => 'DateFrom39','label' => 'From','class' => "6a-45_1"), $this);?>

                <?php echo smarty_function_monthyear(array('name' => 'DateTo39','label' => 'To','class' => "6a_1a-45_1"), $this);?>

                                                <?php echo smarty_function_textfield(array('label' => 'Type of Substance','name' => 'TypeOfSubstance1','class' => '18a_2a'), $this);?>

                <?php echo smarty_function_textfield(array('label' => 'Explanation','name' => 'Explanation1','class' => '41_2a'), $this);?>

            </div>
            <div class="set-default clearer left_marg2a">
                <?php echo smarty_function_monthyear(array('name' => 'DateFrom40','label' => 'From','class' => "6a-45_1"), $this);?>

                <?php echo smarty_function_monthyear(array('name' => 'DateTo40','label' => 'To','class' => "6a_1a-45_1"), $this);?>

                                                <?php echo smarty_function_textfield(array('label' => 'Type of Substance','name' => 'TypeOfSubstance2','class' => '18a_2a'), $this);?>

                <?php echo smarty_function_textfield(array('label' => 'Explanation','name' => 'Explanation2','class' => '41_2a'), $this);?>

            </div>
            <div class="set-default clearer left_marg2a">
                <?php echo smarty_function_monthyear(array('name' => 'DateFrom41','label' => 'From','class' => "6a-45_1"), $this);?>

                <?php echo smarty_function_monthyear(array('name' => 'DateTo41','label' => 'To','class' => "6a_1a-45_1"), $this);?>

                                                <?php echo smarty_function_textfield(array('label' => 'Type of Substance','name' => 'TypeOfSubstance3','class' => '18a_2a'), $this);?>

                <?php echo smarty_function_textfield(array('label' => 'Explanation','name' => 'Explanation3','class' => '41_2a'), $this);?>

            </div>
            </div>
            <div class="area2"><p class="stext">Continuation Space</p></div>     
            <div class="note left_marg2a"><p>Use the space below to continue answers to all other items and any information you would like to add. 
            If more space is needed than is provided below, use a blank sheet(s) of paper. Start each sheet with your name and Social Security number. 
            Before each answer, identify the number of the item.</p></div>
            <div class="set-default clearer left_marg2a">
                <?php echo smarty_function_textarea(array('cols' => '110','rows' => '16','name' => 'ContinuationSpace'), $this);?>

            </div>
            <p class="stext5 note2_2a">Certification That My Answers Are True</p>
            <div class="note stext2 left_marg2a"><p>My statements on this form, and any attachments to it, are true, complete, and correct to the best of my knowledge and belief and are 
            made in good faith. I understand that a knowing and willful false statement on this form can be punished by fine or imprisonment or both. 
            (See section 1001 of title 18, United States Code).</p></div>
            <div class="set-default clearer left_marg2a">
                <p class="stext2">Telephone Number</p>
                <p class="stext2 <?php if ($this->_tpl_vars['errorMessages']['dayarea']): ?>item-error<?php endif; ?> <?php if ($this->_tpl_vars['errorMessages']['daytel']): ?>item-error<?php endif; ?>">#1 Day</p>
                <?php echo smarty_function_textfield(array('label' => "",'name' => 'dayarea','class' => '7'), $this);?>

                <?php echo smarty_function_textfield(array('label' => ' ','name' => 'daytel','class' => '31_1a'), $this);?>

            </div>
            <div class="set-default clearer left_marg2a">               
                <p class="stext2 <?php if ($this->_tpl_vars['errorMessages']['nightarea']): ?>item-error<?php endif; ?> <?php if ($this->_tpl_vars['errorMessages']['nighttel']): ?>item-error<?php endif; ?>">#2 Night</p>
                <?php echo smarty_function_textfield(array('label' => "",'name' => 'nightarea','class' => '7'), $this);?>

                <?php echo smarty_function_textfield(array('label' => ' ','name' => 'nighttel','class' => '31_1a'), $this);?>

            </div>
            <p class="stext5 note2_2a">Declaration for Federal Employment</p>
            <div class="note stext note2_2a left_marg2a"><p>For questions 9,10, and 11, your answers should include convictions resulting from a plea of nolo 
            contendere (no contest), but omit (1) traffic fines of $300 or less, (2) any violation of law committed before your 16th birthday, (3) 
            any violation of law committed before your 18th birthday if finally decided in juvenile court or under a Youth Offender law, (4) 
            any conviction set aside under the Federal Youth Corrections Act or similar state law, and (5) any conviction for which the record was 
            expunged under Federal or state law.</p></div>
        </fieldset>
        <fieldset><legend><span>15.</span> MILITARY SERVICE</legend>
            <div id="militaryPeriods3" <?php if ($_POST['military'] != 'Yes' && $_POST['marine'] != 'Yes'): ?> style="display: none" <?php endif; ?>>
            <div class="set-default clearer">   
                <?php echo smarty_function_textfield(array('label' => 'Branch','name' => 'Branch1','class' => '24_2a'), $this);?>

                                <?php echo smarty_function_date(array('name' => 'n8from1','label' => 'From','class' => "6a_1a-45_1 date_short",'text_class' => 'short'), $this);?>

                                <?php echo smarty_function_date(array('name' => 'n8to1','label' => 'To','class' => "6a_1a-45_1 date_short",'text_class' => 'short'), $this);?>

                <?php echo smarty_function_textfield(array('label' => 'Type of Discharge','name' => 'n8discharge1','class' => '24_2a'), $this);?>

            </div>
            <div class="set-default clearer">   
                <?php echo smarty_function_textfield(array('label' => 'Branch','name' => 'Branch2','class' => '24_2a'), $this);?>

                                                <?php echo smarty_function_date(array('name' => 'n8to2','label' => 'To','class' => "6a_1a-45_1 date_short",'text_class' => 'short'), $this);?>

                <?php echo smarty_function_date(array('name' => 'n8from2','label' => 'From','class' => "6a_1a-45_1 date_short",'text_class' => 'short'), $this);?>

                <?php echo smarty_function_textfield(array('label' => 'Type of Discharge','name' => 'n8discharge2','class' => '24_2a'), $this);?>

            </div>
            <div class="set-default clearer">   
                <?php echo smarty_function_textfield(array('label' => 'Branch','name' => 'Branch3','class' => '24_2a'), $this);?>

                                                <?php echo smarty_function_date(array('name' => 'n8from3','label' => 'From','class' => "6a_1a-45_1 date_short",'text_class' => 'short'), $this);?>

                <?php echo smarty_function_date(array('name' => 'n8to3','label' => 'To','class' => "6a_1a-45_1 date_short",'text_class' => 'short'), $this);?>

                <?php echo smarty_function_textfield(array('label' => 'Type of Discharge','name' => 'n8discharge3','class' => '24_2a'), $this);?>

            </div>
            </div>
        </fieldset>
        <fieldset><legend><span>16.</span> BACKGROUND INFORMATION</legend>
            <ol class="set-nolabel list-lalpha">
                <li>
                    <span class="stext <?php if ($this->_tpl_vars['errorMessages']['f9']): ?>item-error<?php endif; ?>">During the last 10 years, have you been convicted, been imprisoned, been on probation, or been on parole? (Includes felonies, firearms or explosives violations, misdemeanors, and all other offenses.) <i>If "YES," use item 18 to provide the date, explanation of the violation, place of occurrence, and the name and address of the police department or court involved.</i></span>
                    <?php echo smarty_function_radiohorisontal(array('name' => 'f9','labels' => "Yes|No",'values' => "Yes|No"), $this);?>

                </li>
                <li>
                    <span class="stext <?php if ($this->_tpl_vars['errorMessages']['f10']): ?>item-error<?php endif; ?>">Have you been convicted by a military court-martial in the past 10 years?
                    <i>(If no military service, answer "NO.")</i><br> <i> If "YES," use item 18 to provide the date, explanation of the violation, place of occurrence, and the name and address of the military authority or court involved.</i></span>
                    <?php echo smarty_function_radiohorisontal(array('name' => 'f10','labels' => "Yes|No",'values' => "Yes|No"), $this);?>

                </li>
                <li>
                    <span class="stext <?php if ($this->_tpl_vars['errorMessages']['f11']): ?>item-error<?php endif; ?>">Are you now under charges for any violation of law? <i> If "YES," use item 18 to provide the date, explanation of the violation, place of occurrence, and the name and address of the police department or court involved.</i></span>
                    <?php echo smarty_function_radiohorisontal(array('name' => 'f11','labels' => "Yes|No",'values' => "Yes|No"), $this);?>

                </li>
                <li>
                    <span class="stext <?php if ($this->_tpl_vars['errorMessages']['f12']): ?>item-error<?php endif; ?>">During the last 5 years, have you been fired from any job for 
                    any reason, did you quit after being told that you would be fired, did you leave any job by mutual 
                    agreement because of specific problems, or were you debarred from Federal employment by the Office of Personnel 
                    Management or any other Federal agency? <i> If "YES," use item 18 to provide the date, an explanation of the problem, reason for leaving, and the employer's name and address.</i></span>
                    <?php echo smarty_function_radiohorisontal(array('name' => 'f12','labels' => "Yes|No",'values' => "Yes|No"), $this);?>

                </li>
                <li>
                    <span class="stext <?php if ($this->_tpl_vars['errorMessages']['f13']): ?>item-error<?php endif; ?>">Are you delinquent on any Federal debt? (Includes delinquencies arising
                    from Federal taxes, loans, overpayment of benefits, and other debts to the U.S. Government, plus defaults of Federally guaranteed or 
                    insured loans such as student and home mortgage loans.) <i>If "YES," use item 18 to provide the type, length, and amount of the delinquency or default, and steps that you are taking to correct the error or repay the debt.</i></span>
                    <?php echo smarty_function_radiohorisontal(array('name' => 'f13','labels' => "Yes|No",'values' => "Yes|No"), $this);?>

                </li>
            </ol>
        </fieldset>
        <fieldset><legend><span>17.</span> ADDITIONAL QUESTIONS</legend>
            <ol class="set-nolabel list-lalpha">
                <li>
                    <span class="stext <?php if ($this->_tpl_vars['errorMessages']['f14']): ?>item-error<?php endif; ?>">Do any of your relatives work for the agency or government organization 
                    to which you are submitting this form? (Include: father, mother, husband, wife, son, daughter, brother, sister, uncle, 
                    aunt, first cousin, nephew, niece, father-in-law,mother-in-law, son-in-law, daughter-in-law, brother-in-law, sister-in-law, 
                    stepfather, stepmother, stepson, stepdaughter, stepbrother, stepsister, half brother, and half sister.) <i> If "YES," use item 18 to provide the relative's name, relationship, and the department, agency, or branch of the Armed Forces for which your relative works.</i></span>
                    <?php echo smarty_function_radiohorisontal(array('name' => 'f14','labels' => "Yes|No",'values' => "Yes|No"), $this);?>

                </li>
                <li>
                    <span class="stext <?php if ($this->_tpl_vars['errorMessages']['f15']): ?>item-error<?php endif; ?>">Do you receive, or have you ever applied for, retirement pay, 
                    pension, or other retired pay based on military, Federal civilian, or District of Columbia Government service?</span>
                    <?php echo smarty_function_radiohorisontal(array('name' => 'f15','labels' => "Yes|No",'values' => "Yes|No"), $this);?>

                </li>
            </ol>
        </fieldset>
        <fieldset><legend><span>18.</span> CONTINUATION SPACE / AGENCY OPTIONAL QUESTIONS</legend>          
            <div class="note2_2 left_marg2a"><p>Provide details requested in items 16, 17 and 19c in the space below or on attached sheets. Be sure to identify attached sheets with your name, Social Security Number, and item number, and to include ZIP Codes in all addresses. If any questions are printed below, please answer as instructed (these questions are specific to your position and your agency is authorized to ask them).
            </p></div>
            <div class="set-default clearer left_marg2a">
                <?php echo smarty_function_textarea(array('cols' => '100','rows' => '16','name' => 'n16'), $this);?>

            </div>
        </fieldset>
        <fieldset><legend><span>19.</span> APPOINTEE (ONLY RESPOND IF YOU HAVE BEEN EMPLOYED BY THE FEDERAL GOVERNMENT BEFORE)</legend>     
            <div class="note2_2 left_marg2a"><p>Your elections of life insurance during previous Federal employment may affect your eligibility for life insurance during your new appointment. 
            These questions are asked to help your personnel office make a correct determination.</p></div>
            <ol class="set-nolabel list-lalpha">
                <li>
                    <span class="stext">When did you leave your last Federal job?</span>
                    <?php echo smarty_function_date(array('name' => 'n18a','label' => 'Date'), $this);?>
<br clear="all" />
                </li>
                <li>
                    <span class="stext">When you worked for the Federal Government the last time, did you waive Basic Life Insurance or
                    any type of optional life insurance?</span>
                    <?php echo smarty_function_radiohorisontal(array('onclick' => "changeC18()",'name' => 'b18','labels' => "Yes|No|Don't know",'values' => "Yes|No|DNK"), $this);?>

                </li>
                 <li id="c18" <?php if ($_POST['b18'] != 'Yes'): ?> style="display:none" <?php endif; ?>>
                    <span class="stext">Did you later cancel the waiver(s)?<i> If your answer to item is "NO," use item 18 to identify the type(s) of insurance for which waivers were not canceled.</i></span>
                    <?php echo smarty_function_radiohorisontal(array('name' => 'c18','labels' => "Yes|No|Don't know",'values' => "Yes|No|DNK"), $this);?>

                </li>
           </ol>
        </fieldset>  
        
        <?php echo '
        <script>
        selService();
        focusError(\'';  echo $this->_tpl_vars['error']; ?>
', <?php echo $this->_tpl_vars['errorsCount'];  echo ');
        </script>
        '; ?>

<input type="submit" name="generate" value="Generate!" class="btn"> or <a href="index.php">Clear</a> <br>

</form>
</div>
</body>