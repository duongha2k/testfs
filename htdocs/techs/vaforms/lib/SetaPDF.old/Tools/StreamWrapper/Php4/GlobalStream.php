<?php
// example is taken from here: 
// http://www.php.net/manual/en/function.stream-wrapper-register.php#77057

class GlobalStream {
    var $pos;
    var $stream;
    
    function stream_open($path, $mode, $options, &$opened_path) {
        $url = parse_url($path);
        $this->stream = &$GLOBALS[$url["host"]];
        $this->pos = 0;
        if (!is_string($this->stream)) return false;
        return true;
    }
    
    function stream_read($count) {
        $ret = substr($this->stream, $this->pos, $count);
        $this->pos += strlen($ret);
        return $ret;
    }
    
    function stream_write($data){
        $l=strlen($data);
        $this->stream =
            substr($this->stream, 0, $this->pos) .
            $data .
            substr($this->stream, $this->pos += $l);
        return $l;
    }
    
    function stream_tell() {
        return $this->pos;
    }
    
    function stream_eof() {
        return $this->pos >= strlen($this->stream);
    }
    
    function stream_seek($offset, $whence) {
        $l=strlen($this->stream);
        switch ($whence) {
            case SEEK_SET: $newPos = $offset; break;
            case SEEK_CUR: $newPos = $this->pos + $offset; break;
            case SEEK_END: $newPos = $l + $offset; break;
            default: return false;
        }
        $ret = ($newPos >=0 && $newPos <=$l);
        if ($ret) $this->pos=$newPos;
        return $ret;
    }
}

stream_wrapper_register('GlobalStream', 'GlobalStream') or die('Failed to register protocol GlobalStream://');