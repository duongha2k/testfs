<?php

/**
 * Smarty form function for element - month select control
 * 
 * @package Smarty
 * @param array $params
 * @param object $smarty
 * @return string
 * @author Saharchuk Timofei
 * 
 */

function smarty_function_textfield($params, &$smarty){
    $id     = $params['id'];
    $name   = $params['name'];
    $label  = $params['label'];
    
    if ($smarty->_tpl_vars['errorMessages'][$name]){
        $error = ' item-error';
    }
    if (!$_POST[$name] && strpos($name, 'MonthsYearAwarded') !== false){
        $_POST[$name] = 'MM/YY';
    }
    $content = '
    <div class="item-default'.$error.' item-first fl text_size'.$params['class'].'">
        <label for="'.$name.'">'.$label.'</label>
        <input id="'.$name.'" name="'.$name.'" type="text" onclick="clearfield(this);" value="'.$_POST[$name].'" maxlength="50" />
        '.$smarty->_tpl_vars['errorMessages'][$name].'
    </div>';
    return $content;
}
