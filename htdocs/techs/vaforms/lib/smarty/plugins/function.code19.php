<?php

/**
 * Smarty form function for element -  select control
 * 
 * @package Smarty
 * @param array $params
 * @param object $smarty
 * @return string
 * @author Saharchuk Timofei
 * 
 */

function smarty_function_code19($params, &$smarty){
    $id     = $params['id'];
    $name   = $params['name'];
    $label  = $params['label'];
     
    if ($smarty->_tpl_vars['errorMessages'][$name]){
        $error = ' item-error';
    }
       
    //key is what will be putted to pdf form, value is in html view
    $codes = array( ''  => 'Type of employment',
                    1   => 'Active military duty stations',
                    2   => 'National Guard/Reserve',
                    3   => 'U.S.P.H.S. Commissioned Corps',
                    4   => 'Other Federal employment',
                    5   => 'State Government (Non-Federal employment)',
                    6   => 'Self-employment',
                    7   => 'Unemployment',
                    8   => 'Federal Contractor',
                    9   => 'Other');
    $content = '
    <div class="item-type'.$error.' fl">
        <label for="'.$name.'">'.$label.'</label>
        <select name="'.$name.'" id="'.$name.'">';
    foreach ($codes AS $key => $code){
        if ($key == $_POST[$name]){
            $content .= '<option selected="selected" value="'.$key.'">'.$code.'</option>';
        } else {
            $content .= '<option value="'.$key.'">'.$code.'</option>';
        }
    }
    $content .= ' 
        </select>
          '.$smarty->_tpl_vars['errorMessages'][$name].'
          </div>';
    return $content;
}
