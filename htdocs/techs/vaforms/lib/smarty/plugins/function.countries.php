<?php

/**
 * Smarty form function for element - country select control
 * 
 * @package Smarty
 * @param array $params
 * @param object $smarty
 * @return string
 * @author Saharchuk Timofei
 * 
 */

function smarty_function_countries($params, &$smarty){
    $id         = $params['id'];
    $name       = $params['name'];
    $label      = $params['label'];
    $onchange   = $params['onchange'];
     
    if ($smarty->_tpl_vars['errorMessages'][$name]){
        $error = ' item-error';
    }
       
    //key is what will be putted to pdf form, value is in html view
    $countries = array( 
''  => '  -- Choose Country --',
'United States'    => 'United States',
'Canada'=>'Canada',
'Afghanistan'=>'Afghanistan',

'Albania'=>'Albania',
'Algeria'=>'Algeria',
'American Samoa'=>'American Samoa',
'Andorra'=>'Andorra',
'Angola'=>'Angola',
'Anguilla'=>'Anguilla',

'Antigua'=>'Antigua',
'Argentina'=>'Argentina',
'Armenia'=>'Armenia',
'Aruba'=>'Aruba',
'Australia'=>'Australia',
'Austria'=>'Austria',

'Azerbaijan'=>'Azerbaijan',
'Bahamas'=>'Bahamas',
'Bahrain'=>'Bahrain',
'Bangladesh'=>'Bangladesh',
'Barbados'=>'Barbados',
'Belarus'=>'Belarus',

'Belgium'=>'Belgium',
'Belize'=>'Belize',
'Benin'=>'Benin',
'Bermuda'=>'Bermuda',
'Bhutan'=>'Bhutan',
'Bolivia'=>'Bolivia',

'Bosnia-Herzegovina'=>'Bosnia-Herzegovina',
'Botswana'=>'Botswana',
'Brazil'=>'Brazil',
'British Virgin Islands'=>'British Virgin Islands',
'Brunei'=>'Brunei',
'Bulgaria'=>'Bulgaria',

'Burkina Faso'=>'Burkina Faso',
'Burundi'=>'Burundi',
'Cambodia'=>'Cambodia',
'Cameroon'=>'Cameroon',
'Cape Verde'=>'Cape Verde',

'Cayman Islands'=>'Cayman Islands',
'Central African Republic'=>'Central African Republic',
'Chad'=>'Chad',
'Channel Islands'=>'Channel Islands',
'Chile'=>'Chile',
'China'=>'China',

'Colombia'=>'Colombia',
'Comoros'=>'Comoros',
'Congo - Brazzaville'=>'Congo - Brazzaville',
'Congo, The Republic of'=>'Congo, The Republic of',
'Cook Islands'=>'Cook Islands',
'Costa Rica'=>'Costa Rica',

'Croatia'=>'Croatia',
'Cuba'=>'Cuba',
'Cyprus'=>'Cyprus',
'Czech Republic'=>'Czech Republic',
'Denmark'=>'Denmark',
'Djibouti'=>'Djibouti',

'Dominica'=>'Dominica',
'Dominican Republic'=>'Dominican Republic',
'East Timor'=>'East Timor',
'Ecuador'=>'Ecuador',
'Egypt'=>'Egypt',
'El Salvador'=>'El Salvador',

'Equatorial Guinea'=>'Equatorial Guinea',
'Eritrea'=>'Eritrea',
'Estonia'=>'Estonia',
'Ethiopia'=>'Ethiopia',
'Faroe Islands'=>'Faroe Islands',
'Fiji'=>'Fiji',

'Finland'=>'Finland',
'France'=>'France',
'French Guiana'=>'French Guiana',
'French Polynesia'=>'French Polynesia',
'Gabon'=>'Gabon',
'Gambia'=>'Gambia',

'Georgia'=>'Georgia',
'Germany'=>'Germany',
'Ghana'=>'Ghana',
'Gibraltar'=>'Gibraltar',
'Greece'=>'Greece',
'Greenland'=>'Greenland',

'Grenada'=>'Grenada',
'Guadeloupe'=>'Guadeloupe',
'Guam'=>'Guam',
'Guatemala'=>'Guatemala',
'Guinea'=>'Guinea',
'Guinea Bissau'=>'Guinea Bissau',

'Guyana'=>'Guyana',
'Haiti'=>'Haiti',
'Honduras'=>'Honduras',
'Hong Kong'=>'Hong Kong',
'Hungary'=>'Hungary',
'Iceland'=>'Iceland',

'India'=>'India',
'Indonesia'=>'Indonesia',
'Iran'=>'Iran',
'Iraq'=>'Iraq',
'Ireland'=>'Ireland',
'Israel'=>'Israel',

'Italy'=>'Italy',
'Ivory Coast'=>'Ivory Coast',
'Jamaica'=>'Jamaica',
'Japan'=>'Japan',
'Jordan'=>'Jordan',
'Kazakhstan'=>'Kazakhstan',

'Kenya'=>'Kenya',
'Kiribati'=>'Kiribati',
'Korea, Dem Rep'=>'Korea, Dem Rep',
'Kosovo'=>'Kosovo',
'Kuwait'=>'Kuwait',
'Kyrgyzstan'=>'Kyrgyzstan',

'Laos'=>'Laos',
'Latvia'=>'Latvia',
'Lebanon'=>'Lebanon',
'Lesotho'=>'Lesotho',
'Liberia'=>'Liberia',
'Libya'=>'Libya',

'Liechtenstein'=>'Liechtenstein',
'Lithuania'=>'Lithuania',
'Luxembourg'=>'Luxembourg',
'Macau'=>'Macau',
'Macedonia'=>'Macedonia',
'Madagascar'=>'Madagascar',

'Malawi'=>'Malawi',
'Malaysia'=>'Malaysia',
'Maldives'=>'Maldives',
'Mali'=>'Mali',
'Malta'=>'Malta',
'Marshall Islands'=>'Marshall Islands',

'Martinique'=>'Martinique',
'Mauritania'=>'Mauritania',
'Mauritius'=>'Mauritius',
'Mexico'=>'Mexico',
'Micronesia'=>'Micronesia',
'Moldova'=>'Moldova',

'Monaco'=>'Monaco',
'Mongolia'=>'Mongolia',
'Montenegro'=>'Montenegro',
'Montserrat'=>'Montserrat',
'Morocco'=>'Morocco',
'Mozambique'=>'Mozambique',

'Myanmar/Burma'=>'Myanmar/Burma',
'Namibia'=>'Namibia',
'Nepal'=>'Nepal',
'Netherlands'=>'Netherlands',
'Netherlands Antilles'=>'Netherlands Antilles',
'New Caledonia'=>'New Caledonia',

'New Zealand'=>'New Zealand',
'Nicaragua'=>'Nicaragua',
'Niger'=>'Niger',
'Nigeria'=>'Nigeria',
'Norway'=>'Norway',
'Oman'=>'Oman',

'Pakistan'=>'Pakistan',
'Palau'=>'Palau',
'Palestinian Territories'=>'Palestinian Territories',
'Panama'=>'Panama',
'Papua New Guinea'=>'Papua New Guinea',
'Paraguay'=>'Paraguay',

'Peru'=>'Peru',
'Philippines'=>'Philippines',
'Poland'=>'Poland',
'Portugal'=>'Portugal',
'Puerto Rico'=>'Puerto Rico',
'Qatar'=>'Qatar',

'Reunion'=>'Reunion',
'Romania'=>'Romania',
'Russia'=>'Russia',
'Rwanda'=>'Rwanda',
'Saipan'=>'Saipan',
'Samoa'=>'Samoa',

'San Marino'=>'San Marino',
'Sao Tome and Principe'=>'Sao Tome and Principe',
'Saudi Arabia'=>'Saudi Arabia',
'Senegal'=>'Senegal',
'Serbia'=>'Serbia',
'Seychelles'=>'Seychelles',

'Sierra Leone'=>'Sierra Leone',
'Singapore'=>'Singapore',
'Slovak Republic'=>'Slovak Republic',
'Slovenia'=>'Slovenia',
'Solomon Islands'=>'Solomon Islands',
'Somalia'=>'Somalia',

'South Africa'=>'South Africa',
'South Korea'=>'South Korea',
'Spain'=>'Spain',
'Sri Lanka'=>'Sri Lanka',
'St. Barthelemy'=>'St. Barthelemy',
'St. Kitts and Nevis'=>'St. Kitts and Nevis',

'St. Lucia'=>'St. Lucia',
'St. Vincent'=>'St. Vincent',
'Sudan'=>'Sudan',
'Suriname'=>'Suriname',
'Swaziland'=>'Swaziland',
'Sweden'=>'Sweden',

'Switzerland'=>'Switzerland',
'Syria'=>'Syria',
'Taiwan'=>'Taiwan',
'Tajikistan'=>'Tajikistan',
'Tanzania'=>'Tanzania',
'Thailand'=>'Thailand',

'Togo'=>'Togo',
'Trinidad and Tobago'=>'Trinidad and Tobago',
'Tunisia'=>'Tunisia',
'Turkey'=>'Turkey',
'Turkmenistan'=>'Turkmenistan',
'Turks and Caicos Islands'=>'Turks and Caicos Islands',

'Tuvalu'=>'Tuvalu',
'U.S. Virgin Islands'=>'U.S. Virgin Islands',
'Uganda'=>'Uganda',
'Ukraine'=>'Ukraine',
'United Arab Emirates'=>'United Arab Emirates',

'United Kingdom'=>'United Kingdom',
'Uruguay'=>'Uruguay',
'Uzbekistan'=>'Uzbekistan',
'Vanuatu'=>'Vanuatu',
'Vatican City'=>'Vatican City',
'Venezuela'=>'Venezuela',

'Vietnam'=>'Vietnam',
'Wallis &amp; Futuna'=>'Wallis &amp; Futuna',
'Yemen'=>'Yemen',
'Zambia'=>'Zambia',
'Zimbabwe'=>'Zimbabwe',
);
    $content = '
    <div class="item-country'.$params['class'].' fl'.$error.'">
        <label for="'.$name.'">'.$label.'</em></label>
        <select id="'.$name.'" name="'.$name.'" onchange="'.$onchange.'">';
    foreach ($countries AS $key => $country){
        if ($key == $_POST[$name]){
            $content .= '<option selected="selected" value="'.$key.'">'.$country.'</option>';
        } else {
            $content .= '<option value="'.$key.'">'.$country.'</option>';
        }
    }
    $content .= '</select>
         '.$smarty->_tpl_vars['errorMessages'][$name].'
          </div>';
    return $content;
}
