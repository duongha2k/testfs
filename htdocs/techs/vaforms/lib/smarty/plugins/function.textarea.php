<?php

/**
 * Smarty form function for element - textarea select control
 * 
 * @package Smarty
 * @param array $params
 * @param object $smarty
 * @return string
 * @author Saharchuk Timofei
 * 
 */

function smarty_function_textarea($params, &$smarty){
    $name   = $params['name'];
    $cols   = $params['cols'];
    $rows   = $params['rows'];

     
    if ($smarty->_tpl_vars['errorMessages'][$name]){
        $error = ' item-error';
    }
       
    $content = '<div class="item-textarea fl'.$error.'">
     <textarea rows="'.$rows.'" cols="'.$cols.'" name="'.$name.'">'.$_POST[$name].'</textarea>
      '.$smarty->_tpl_vars['errorMessages'][$name].'
      </div>';
    return $content;
}
