<?php

/**
 * Smarty form function for element -  select control
 * 
 * @package Smarty
 * @param array $params
 * @param object $smarty
 * @return string
 * @author Saharchuk Timofei
 * 
 */

function smarty_function_code17($params, &$smarty){
    $id     = $params['id'];
    $name   = $params['name'];
    $label  = $params['label'];
     
    if ($smarty->_tpl_vars['errorMessages'][$name]){
        $error = ' item-error';
    }
       
    //key is what will be putted to pdf form, value is in html view
    $codes = array( ''  => 'Branch of service',
                    1   => 'Air Force',
                    2   => 'Army',
                    3   => 'Navy',
                    4   => 'Marine Corps',
                    5   => 'Coast Guard',
                    6   => 'Merchant Marine',
                    7   => 'National Guard');
    $content = '
    <div class="item-state'.$params['class'].' fl'.$error.'">
        <label for="'.$name.'">'.$label.'</label>
        <select name="'.$name.'" id="'.$name.'">';
    foreach ($codes AS $key => $code){
        if ($key == $_POST[$name]){
            $content .= '<option selected="selected" value="'.$key.'">'.$code.'</option>';
        } else {
            $content .= '<option value="'.$key.'">'.$code.'</option>';
        }
    }
    $content .= ' 
        </select>
          '.$smarty->_tpl_vars['errorMessages'][$name].'
          </div>';
    return $content;
}
