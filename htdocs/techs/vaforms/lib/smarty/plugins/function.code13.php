<?php

/**
 * Smarty form function for element -  select control
 * 
 * @package Smarty
 * @param array $params
 * @param object $smarty
 * @return string
 * @author Saharchuk Timofei
 * 
 */

function smarty_function_code13($params, &$smarty){
    $id     = $params['id'];
    $name   = $params['name'];
    $label  = $params['label'];
     
    if ($smarty->_tpl_vars['errorMessages'][$name]){
        $error = ' item-error';
    }
       
    //key is what will be putted to pdf form, value is in html view
    $codes = array( ''  => 'School Type',
                    1   => 'High School',
                    2   => 'College/University/Military College',
                    3   => 'Vocational/Technical/Trade School');
    $content = '
    <div class="item-code'.$error.' fl">
        <label for="'.$name.'">'.$label.'</label>
        <select name="'.$name.'" id="'.$name.'">';
    foreach ($codes AS $key => $code){
        if ($key == $_POST[$name]){
            $content .= '<option selected="selected" value="'.$key.'">'.$code.'</option>';
        } else {
            $content .= '<option value="'.$key.'">'.$code.'</option>';
        }
    }
    $content .= ' 
        </select>
          '.$smarty->_tpl_vars['errorMessages'][$name].'
          </div>';
    return $content;
}
