<?php

/**
 * Smarty form function for element - month select control
 * 
 * @package Smarty
 * @param array $params
 * @param object $smarty
 * @return string
 * @author Saharchuk Timofei
 * 
 */

function smarty_function_monthyear($params, &$smarty){
    $name       = $params['name'];
    $label      = $params['label'];
    $onchange   = $params['onchange'];
    //$labely = $params['labely'];
    $name       = $params['name'];
    //$values = explode($params['value']);
    
    $a = '';
    if ($name[0] == 'a'){
        $name = substr($name, 1);
        $a = 'a';
    }
    if ($smarty->_tpl_vars['errorMessages'][$a.'Month'.$name]){
        $errorm = ' item-error';
    }
       
    if ($smarty->_tpl_vars['errorMessages'][$a.'Year'.$name]){
        $errory = ' item-error';
    }
    
    if (!$_POST[$a.'Year'.$name]){
        $_POST[$a.'Year'.$name] = 'YY';
    }
   
    //key is what will be putted to pdf form, value is in html view
    $monthes = array(   ''      => '  -- Month --',
                        '01'    => 'January',
                        '02'    => 'February', 
                        '03'    => 'March', 
                        '04'    => 'April', 
                        '05'    => 'May', 
                        '06'    => 'June', 
                        '07'    => 'July', 
                        '08'    => 'August', 
                        '09'    => 'September', 
                        '10'    => 'October', 
                        '11'    => 'November', 
                        '12'    => 'December');
    $content = '
    <div class="item-month'.$params['class'].' fl'.$errorm.'">
        <label for="'.$a.'Month'.$name.'">'.$label.'</label>
        <select id="'.$a.'Month'.$name.'" onchange="'.$onchange.'" name="'.$a.'Month'.$name.'">';
    foreach ($monthes AS $key => $month){
        if ($key == $_POST[$a.'Month'.$name]){
            $content .= '<option selected="selected" value="'.$key.'">'.$month.'</option>';
        } else {
            $content .= '<option value="'.$key.'">'.$month.'</option>';
        }
    }
    $content .= '</select>
           '.$smarty->_tpl_vars['errorMessages'][$a.'Month'.$name].'
    </div>
    <div class="item-year'.$params['class'].' fl'.$errory.'">
        <label for="'.$a.'Year'.$name.'">&nbsp;</label>
        <input id="'.$a.'Year'.$name.'" name="'.$a.'Year'.$name.'" onclick="clearfield(this);" onchange="'.$onchange.'" type="text" value="'.$_POST[$a.'Year'.$name].'" maxlength="2" />
               '.$smarty->_tpl_vars['errorMessages'][$a.'Year'.$name].'
               </div></select>';
    return $content;
}
