<?php

/**
 * Smarty form function for element - states select control
 * 
 * @package Smarty
 * @param array $params
 * @param object $smarty
 * @return string
 * @author Saharchuk Timofei
 * 
 */

function smarty_function_states($params, &$smarty){
    $id     = $params['id'];
    $name   = $params['name'];
    $label  = $params['label'];
    
    if ($smarty->_tpl_vars['errorMessages'][$name]){
        $error = ' item-error';
    }
    
    //key is what will be putted to pdf form, value is in html view
    $states = array(
''  => '-- State --',
'AL'=>'Alabama',
'AK'=>'Alaska',
'AZ'=>'Arizona',
'AR'=>'Arkansas',
'CA'=>'California',
'CO'=>'Colorado',
            
'CT'=>'Connecticut',
'DC'=>'D.C.',
'DE'=>'Delaware',
'FL'=>'Florida',
'GA'=>'Georgia',
'HI'=>'Hawaii',
                          
'ID'=>'Idaho',
'IL'=>'Illinois',
'IN'=>'Indiana',
'IA'=>'Iowa',
'KS'=>'Kansas',
'KY'=>'Kentucky',
                                        
'LA'=>'Louisiana',
'ME'=>'Maine',
'MD'=>'Maryland',
'MA'=>'Massachusetts',
'MI'=>'Michigan',
'MN'=>'Minnesota',
                                                      
'MS'=>'Mississippi',
'MO'=>'Missouri',
'MT'=>'Montana',
'NE'=>'Nebraska',
'NV'=>'Nevada',
'NH'=>'New Hampshire',
                                                                    
'NJ'=>'New Jersey',
'NM'=>'New Mexico',
'NY'=>'New York',
'NC'=>'North Carolina',
'ND'=>'North Dakota',
'OH'=>'Ohio',
                                                                                  
'OK'=>'Oklahoma',
'OR'=>'Oregon',
'PA'=>'Pennsylvania',
'RI'=>'Rhode Island',
'SC'=>'South Carolina',
'SD'=>'South Dakota',
                                                                                                
'TN'=>'Tennessee',
'TX'=>'Texas',
'UT'=>'Utah',
'VT'=>'Vermont',
'VA'=>'Virginia',
'WA'=>'Washington',
                                                                                                              
'WV'=>'West Virginia',
'WI'=>'Wisconsin',
'WY'=>'Wyoming',
);
    $content = '
    <div class="item-state'.$params['class'].' fl'.$error.'">
        <label for="'.$name.'">'.$label.'</label>
        <select name="'.$name.'" id="'.$name.'">';
    foreach ($states AS $key => $state){
        if ($key == $_POST[$name]){
            $content .= '<option selected="selected" value="'.$key.'">'.$state.'</option>';
        } else {
            $content .= '<option value="'.$key.'">'.$state.'</option>';
        }
    }
    $content .= ' 
    </select>
         '.$smarty->_tpl_vars['errorMessages'][$name].'
    </div>';
    return $content;
}
