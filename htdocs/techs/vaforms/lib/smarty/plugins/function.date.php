<?php

/**
 * Smarty form function for element - date select control
 * 
 * @package Smarty
 * @param array $params
 * @param object $smarty
 * @return string
 * @author Saharchuk Timofei
 * 
 */

function smarty_function_date($params, &$smarty){
    $id         = $params['id'];
    $name       = $params['name'];
    $label      = $params['label'];
    $onchange   = $params['onchange'];
    $text_class = $params['text_class'];
     
    if ($smarty->_tpl_vars['errorMessages'][$name]){
        $error = ' item-error';
    }
       
    $content = '
    <div class="'.$error.'">
        <div class="item-date'.$params['class'].' item-first fl">
            <label for="'.$name.'">'.$label.'</label>
            <input id="'.$name.'" onchange="'.$onchange.'" class="'.$text_class.'" name="'.$name.'" type="text" value="'.$_POST[$name].'" maxlength="50" />
            <script language="JavaScript">  
                new tcal ({
                    \'controlname\': \''.$name.'\'
                });
            </script><br>'.$smarty->_tpl_vars['errorMessages'][$name].'
        </div>
        
    </div>';
    return $content;
}
