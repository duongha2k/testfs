<?php

/**
 * Smarty form function for element - status select control
 * 
 * @package Smarty
 * @param array $params
 * @param object $smarty
 * @return string
 * @author Saharchuk Timofei
 * 
 */

function smarty_function_status($params, &$smarty){
    $id     = $params['id'];
    $name   = $params['name'];
    $label  = $params['label'];
    
    if ($smarty->_tpl_vars['errorMessages'][$name]){
        $error = ' item-error';
    }
    if ($name == 'status1'){
        $num = 1;
    } else {
        $num = 2;
    }
    //key is what will be putted to pdf form, value is in html view
    $states = array(    ''                          => '-- Status --',
                        'Active'.$num               => 'Active', 
                        'ActiveReserve'.$num        => 'Active Reserve',
                        'InactiveReserve'.$num      => 'Inactive Reserve',
                        'StatusNationalGuard'.$num  => 'National Guard');
    $content = '
    <div class="item-state'.$params['class'].' fl'.$error.'">
        <label for="'.$name.'">'.$label.'</label>
        <select name="'.$name.'" id="'.$name.'">';
    foreach ($states AS $key => $state){
        if ($key == $_POST[$name]){
            $content .= '<option selected="selected" value="'.$key.'">'.$state.'</option>';
        } else {
            $content .= '<option value="'.$key.'">'.$state.'</option>';
        }
    }
    $content .= ' 
    </select>
         '.$smarty->_tpl_vars['errorMessages'][$name].'
    </div>';
    return $content;
}
