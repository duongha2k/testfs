<?php

/**
 * Smarty form function for element - vertical rasio control
 * 
 * @package Smarty
 * @param array $params
 * @param object $smarty
 * @return string
 * @author Saharchuk Timofei
 * 
 */

function smarty_function_radiovertical($params, &$smarty){
    $id       = $params['id'];
    $name     = $params['name'];
    $onclick  = $params['onclick'];
    $onchange = $params['onchange'];
    $values   = explode('|', $params['values']);
    $labels   = explode('|', $params['labels']);
     
    if ($smarty->_tpl_vars['errorMessages'][$name]){
        $error = ' item-error';
    }
       
    $content = '<div class="list-lalpha-inner item-error item_m_error">';
    foreach ($values AS $key => $value){
        $content .= '
        <div class="item-radio2 clearer">
            <input type="radio" onchange="'.$onchange.'" onclick="'.$onclick.'" name="'.$name.'" id="'.$name.$key.'" class="fl"  value="'.$value.'"';
            if ($value == $_POST[$name]){
                $content .= ' checked="checked" ';
            }    
            $content .= '/>
            <label for="'.$name.$key.'" class="fl">'.$labels[$key].'</label>
        </div>';
    }
    $content .= '<div class="item_error'.$params['class'].'">
    '.$smarty->_tpl_vars['errorMessages'][$name].'</div>
    </div>';
    
    return $content;
}
