<?php

/**
 * Smarty form function for element - horisontal radio control
 * 
 * @package Smarty
 * @param array $params
 * @param object $smarty
 * @return string
 * @author Saharchuk Timofei
 * 
 */

function smarty_function_radiohorisontal($params, &$smarty){
    $id       = $params['id'];
    $name     = $params['name'];
    $values   = explode('|', $params['values']);
    $labels   = explode('|', $params['labels']);
    $onchange = $params['onchange'];
    $onclick  = $params['onclick'];
    
    if ($smarty->_tpl_vars['errorMessages'][$name]){
        $error = ' item-error';
    }
    
    $content = '<div class="set-default'.$error.'">';
    foreach ($values AS $key => $value){
        $content .= '
        <div class="item-radio fl">
            <input type="radio" onchange="'.$onchange.'" onclick="'.$onclick.'" name="'.$name.'" id="'.$name.$key.'" value="'.$value.'"';
            if ($value == $_POST[$name]){
                $content .= ' checked="checked" ';
            }    
            $content .= ' />
            <label for="'.$name.$key.'">'.$labels[$key].'</label>
        </div>';
    }
    $content .= '<div class="item_error2">'.$smarty->_tpl_vars['errorMessages'][$name].'</div>
    </div>';
    
    return $content;
}
