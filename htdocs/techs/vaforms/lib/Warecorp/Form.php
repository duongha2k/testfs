<?php

/**
 * Warecorp Form Framework
 * @package Warecorp_Form
 * @author Dmitry Kostikov
 */
class Warecorp_Form
{
    /**
     * Array containing the form rules
     * @var  array
     */
    private $_rules = array();
    
    /**
     * Array containing custom error messages
     * @var array
     * @author Saharchuk Timofei
     */
    private $customErrorMessages = array();
    /**
     * Array with error messages
     * @var Array
     * @author Saharchuk Timofei
     */
    //private $errorMessages = array();
    /**
     * Flag shows if the form is vslid
     * @var boolean
     * @author Saharchuk Timofei
     */
    private $isValid = true;
    
    /**
     * Return error message according to rule
     * @param string $rule
     * @return string error message
     * @author Saharchuk Timofei
     */
    public function getErrorMessage($rule)
    {
        switch($rule){
            case 'phone':
                return '<span style="font-size:9px;">Please enter correct format (###-####)</span>';
                break;
            case 'yy':
                return '<span style="font-size:9px;">Please enter correct format (yy)</span>';
                break;            
            case 'mmyy':
                return '<span style="font-size:9px;">Please enter correct format (mm/yy)</span>';
                break;
            case 'mmddyy':
                return '<span style="font-size:9px;">Please enter correct format (mm/dd/yy)</span>';
                break;
            case 'mmddyyyy':
                return '<span style="font-size:9px;">Please enter correct format(mm/dd/yyyy)</span>';
                break;
            case 'zipcode':
                return '<span style="font-size:9px;">Please enter correct format (#####)</span>';
                break;
            case 'required':
                return '<span style="font-size:9px;">This field is required</span>';
                break;
            case 'area':
                return '<span style="font-size:9px;">Please enter correct format (###)</span>';
                break;
            case 'ssn':
                return '<span style="font-size:9px;">Please enter correct format (###-##-####)</span>';
                break;

        }
    }

    /**
     * Return true if form is valid
     * @author Artem Sukharev
     */
    public function isValid()
    {
        return (bool) $this->isValid;
    }
    /**
     * Set for as valid or invalid
     * @author Artem Sukharev
     */
    public function setValid($status = true)
    {
        $this->isValid = (bool) $status;
    }
    /**
     * Return form rules
     * @author Artem Sukharev
     */
    public function getRules()
    {
        return $this->_rules;
    }
    /**
     * Validate form data
     * @param array $params
     * @return bool
     * @author Saharchuk Timofei
     */
    public function validate(&$params)
    {

        $error = false;
        // check element rules
        foreach($this->_rules as $element => &$rules){
            $value = isset($params[$element]) ? $params[$element] : null;

            foreach($rules as $type => &$rule) {
                    switch ($type){
                        case 'required':
                            if (!trim($value)){ 
                                $rule['error'] = $error = true;
                            }
                            break;
                        case 'mmyy':
                            if ($value && !preg_match('|^\d{2}/\d{2}$|', $value)){
                                $rule['error'] = $error = true;
                            }
                            break;
                        case 'yy':
                            if ($params[$element] == 'yy'){
                                $params[$element] == '';
                                $value = '';
                            }
                            if ($value && !preg_match('|^\d{2}$|', $value)){
                                $rule['error'] = $error = true;
                            }
                            break;
                        case 'mmddyy':
                            if ($value && !preg_match('|^\d{2}/\d{2}/\d{2}$|', $value)){
                                $rule['error'] = $error = true;
                            }
                            break;                       
                        case 'zipcode':
                            if ($value && !preg_match('|^\d{5}$|', $value)){
                                $rule['error'] = $error = true;
                            }
                            break;
                        case 'mmddyyyy':
                            if ($value && !preg_match('|^\d{2}/\d{2}/\d{4}$|', $value)){
                                $rule['error'] = $error = true;
                            }
                            break;
                        case 'ssn':
                            $ssn = str_replace('-', '', $params[$element]);
                            if ($ssn && !preg_match('|^\d{9}$|', $ssn)){
                                $rule['error'] = $error = true;
                            } else if($ssn) {
                                $params[$element] = substr($ssn, 0, 3).'-'.substr($ssn, 3, 2).'-'.substr($ssn, 5, 4);
                            }
                            break;
                        case 'code1_3':
                            if ($value && !preg_match('|^[1-3]$|', $value)){
                                $rule['error'] = $error = true;
                            }
                            break;
                        case 'code1_7':
                            if ($value && !preg_match('|^[1-7]$|', $value)){
                                $rule['error'] = $error = true;
                            }
                            break;

                        case 'phone':
                            if ($value && !preg_match('|^\d{3}-\d{4}$|', $value)){
                                $rule['error'] = $error = true;
                            }
                            break;
                        case 'area':
                            if ($value && !preg_match('|^\d{3}$|', $value)){
                                $rule['error'] = $error = true;
                            }
                    }
            }
        }
        //proceed customError
        if($this->isValid == false) $error = true; //if set custom error
        /*echo '<pre>';
        print_r($this->_rules);
        print_r($params);
        echo '</pre>';*/

        // no rules exeption, process
        $this->setValid(!$error);
        return !$error;
    }
    public function getErrorMessages($errors_summary_id = null)
    {
        //if (!$this->errorMessages){
        $errorMessages = array();
            foreach ( $this->_rules as $_field => $_rules ) {
                foreach ( $_rules as $_rule ) {
                    if ( $_rule['error']) {
                        $errorMessages[$_field] = $_rule['message'];
                    }
                }
                //$errorMessages['custom'] = $this->customErrorMessages;
            }
        //}
        return $errorMessages;
    }
    
    public function addCustomErrorMessage($message)
    {
        $this->customErrorMessages[] = $message;
        $this->setValid(false);
    }
    
    /**
     * Adds a validation rule for the given field
     * @param    string     $element   Form element name
     * @param    string     $type      Rule type
     * @param    string     $options   (optional)Required for extra rule data
     */
    public function addRule($element, $type, $options=null)
    {
        if (!isset($this->_rules[$element])){ 
            $this->_rules[$element] = array();
        }
        $this->_rules[$element][$type] = array( 'message' => $this->getErrorMessage($type),
                                                'options' => $options,
                                                'error'   => false);
    }
}
