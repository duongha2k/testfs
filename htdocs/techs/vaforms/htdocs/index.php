<?php $page = 'techs'; ?>
<?php $option = 'home'; ?>
<?php require_once ("{$_SERVER['DOCUMENT_ROOT']}/headerStartSession.php"); ?>
<?php
// Check whether tech is logged in
$loggedIn = $_SESSION['loggedIn'];
$loggedInAs = $_SESSION['loggedInAs'];

/**
 * Modified by Sergey Petkevich (Warecorp)
 */
$redir = 'https://www.fieldsolutions.com/techs/vaforms/htdocs/vaforms.php';

if ( !empty($_SESSION['redirectURL']) ) {
    $redir = $_SESSION['redirectURL'];
}

if($loggedIn == "yes" && $loggedInAs == "tech"){
	if ( isset($_SESSION['redirectURL']) ) {
        unset($_SESSION['redirectURL']);
	}

    // Redirect to dashboard.php
    //header( 'Location: https://www.fieldsolutions.com/techs/dashboard.php' ) ;
    header( 'Location: ' . $redir );
}else{
    // Redirect to logIn.php
    header( 'Location: https://www.fieldsolutions.com/techs/logIn.php' );
}
/**
 * #####################################
 */
?>
