<?php $page = "techs"; ?>
<?php $option = "vaforms"; ?>
<?php 	require ("{$_SERVER['DOCUMENT_ROOT']}/headerStartSession.php"); ?>
<?php
//This file divided by 2 big parts
//1 validtion
//2 forms filling
//
//each of this parts has 3 parts (for each form)
//each section of this parts are the sections in current pdf document

require_once '../init/init.php';


$form = new Warecorp_Form(new Warecorp_ErrorMessages(MESSAGES_DIR.'error/index.xml'));
$showPDF = false;
if ($_GET['fill']){
    $_POST = unserialize(file_get_contents('post.txt'));
}

if (isset($_POST['generate']) && !$_GET['fill']){
    
    foreach ($_POST AS &$field){
        if ($field == 'YY' || $field == 'MM/YY'){
            $field = '';
        }
    }
    //file_put_contents('post.txt', serialize($_POST));
    //---------------------------     1     ------------------------
    $form->addRule('FName', 'required');
    //$form->addRule('FName', 'lettersonly');

    $form->addRule('LName', 'required');
    //$form->addRule('LName', 'lettersonly');    

    //---------------------------     2     ------------------------
    $form->addRule('DOB',   'required');
    $form->addRule('DOB',   'mmddyyyy');

    //---------------------------     3     ------------------------
    $form->addRule('CityOfBirth',   'required');
    if ($_POST['CountryOfBirth'] == 'United States'){
        $form->addRule('StateOfBirth',  'required');
        $form->addRule('CountyOfBirth', 'required');
    }
    $form->addRule('CountryOfBirth','required');
    //$form->addRule('StateOfBirth',  'length', array('length' => 2));

    //---------------------------     4     ------------------------
    $form->addRule('SSN',   'required');
    $form->addRule('SSN',   'ssn');

    //---------------------------     5 #1     ------------------------
    if ($_POST['OtherName1']){
        $form->addRule('MonthDateFrom1', 'required');
        $form->addRule('YearDateFrom1',  'required');
        $form->addRule('MonthDateTo1',   'required');
        $form->addRule('YearDateTo1',    'required');
        $form->addRule('YearDateFrom1',  'yy');
        $form->addRule('YearDateTo1',    'yy');
    }
    if ($_POST['MonthDateFrom1'] || $_POST['MonthDateTo1'] || $_POST['YearDateFrom1'] || $_POST['YearDateTo1']){
        $form->addRule('OtherName1','required');
    }

    //---------------------------     5 #2     ------------------------
    if ($_POST['OtherName2']){
        $form->addRule('MonthDateFrom2', 'required');
        $form->addRule('YearDateFrom2',  'required');
        $form->addRule('MonthDateTo2',   'required');
        $form->addRule('YearDateTo2',    'required');
        $form->addRule('YearDateFrom2',  'yy');
        $form->addRule('YearDateTo2',    'yy');
    }
    if ($_POST['MonthDateFrom2'] || $_POST['MonthDateTo2'] || $_POST['YearDateFrom2'] || $_POST['YearDateTo2']){
        $form->addRule('OtherName2','required');
    }


    //---------------------------     5 #3     ------------------------
    if ($_POST['OtherName3']){
        $form->addRule('MonthDateFrom3', 'required');
        $form->addRule('YearDateFrom3',  'required');
        $form->addRule('MonthDateTo3',   'required');
        $form->addRule('YearDateTo3',    'required');
        $form->addRule('YearDateFrom3',  'yy');
        $form->addRule('YearDateTo3',    'yy');

    }
    if ($_POST['MonthDateFrom3'] || $_POST['MonthDateTo3'] || $_POST['YearDateFrom3'] || $_POST['YearDateTo3']){
        $form->addRule('OtherName3','required');
    }


    //---------------------------     5 #4     ------------------------
    if ($_POST['OtherName4']){
        $form->addRule('MonthDateFrom4', 'required');
        $form->addRule('YearDateFrom4',  'required');
        $form->addRule('MonthDateTo4',   'required');
        $form->addRule('YearDateTo4',    'required');
        $form->addRule('YearDateFrom4',  'yy');
        $form->addRule('YearDateTo4',    'yy');

    }
    if ($_POST['MonthDateFrom4'] || $_POST['MonthDateTo4'] || $_POST['YearDateFrom4'] || $_POST['YearDateTo4']){
        $form->addRule('OtherName4','required');
    }


    //---------------------------     6     ------------------------
    $form->addRule('sex',               'required');

    //---------------------------     7a    ------------------------
    $form->addRule('citizen',           'required');
    
    //---------------------------     7b    ------------------------
    $form->addRule('MothersMaidenName', 'required');
    
    //---------------------------     7c    ------------------------
    if ($_POST['citizen'] == 'NotBornCitizen'){
        $form->addRule('CourtOfNaturaliztion',          'required');
        $form->addRule('CityOfNaturalization',          'required');
        $form->addRule('StateOfNaturalization',         'required');
        $form->addRule('NaturalizationCertificate',     'required');
        //$form->addRule('NaturalizationCertificate',     'numeric');
        $form->addRule('DateOfIssue1',                  'required');
        $form->addRule('DateOfIssue1',                  'mmddyyyy');
        
        $form->addRule('CityOfCitizenshipCertificate',  'required');
        $form->addRule('StateOfCitizenshipCertificate', 'required');
        $form->addRule('CitizenshipCertificate',        'required');
        //$form->addRule('CitizenshipCertificate',        'numeric');   
        $form->addRule('DateOfIssue2',                  'required');
        $form->addRule('DateOfIssue2',                  'mmddyyyy'); 
        
        $form->addRule('Passportn',                     'required');
        //$form->addRule('Passportn',                     'numeric');
        $form->addRule('DateOfIssue3',                  'required');
        $form->addRule('DateOfIssue3',                  'mmddyyyy');
    }

    //---------------------------     7d    ------------------------
    if ($_POST['citizen'] == 'USCitizen' || $_POST['citizen'] == 'NotBornCitizen'){
        //$form->addRule('CountryOfDualCitizenship',   'required');
    }

    //---------------------------     7e    ------------------------
    if ($_POST['citizen'] == 'NotAUSCitizen'){
        $form->addRule('CityOfUSEntrance',          'required');
        $form->addRule('StateOfUSEnterance',        'required');
        //$form->addRule('StateOfUSEnterance',        'length', array('length' => '2'));
        $form->addRule('DateOfUSEnterance',         'required');
        $form->addRule('DateOfUSEnterance',         'mmddyyyy');
        $form->addRule('AlienRegistrationNumber',   'required');
        $form->addRule('CountryOfCitizenship',      'required');
    }

    //---------------------------     8 #1    ------------------------
    $form->addRule('MonthDateFrom5',        'required');
    $form->addRule('YearDateFrom5',         'required');
    $form->addRule('YearDateFrom5',         'yy');
    $form->addRule('StreetAddress1',        'required');
    //$form->addRule('Aptn1',                 'required');
    $form->addRule('CityCountry1',          'required');
    $form->addRule('State1',                'required');
    $form->addRule('ZIpCode1',              'required');
    $form->addRule('ZIpCode1',              'zipcode');
    $form->addRule('NameOfPerson1',         'required');
    $form->addRule('StreetAddressOfPerson1','required');
    $form->addRule('CityCountryOfPerson1',  'required');
    $form->addRule('StateOfPerson1',        'required');
    $form->addRule('ZIpCodeOfPerson1',      'required');
    $form->addRule('ZIpCodeOfPerson1',      'zipcode');
    
    //---------------------------     8 #2    ------------------------
    if ($_POST['MonthDateFrom6']            || $_POST['YearDateFrom6'] ||$_POST['StreetAddress2'] || 
        $_POST['CityCountry2']              || $_POST['State2'] || 
        $_POST['ZIpCode2']                  || $_POST['NameOfPerson2'] || 
        $_POST['StreetAddressOfPerson2']    || $_POST['CityCountryOfPerson2'] || 
        $_POST['StateOfPerson2']            || $_POST['ZIpCodeOfPerson2'] || $_POST['MonthDateFrom7']){
        
        //$date = explode('/', $_POST['DateFrom5']);
        $form->addRule('MonthDateFrom6',        'required');
        $form->addRule('YearDateFrom6',         'required');
        $form->addRule('YearDateFrom6',         'yy');
        $form->addRule('StreetAddress2',        'required');
        $form->addRule('CityCountry2',          'required');
        $form->addRule('State2',                'required');
        $form->addRule('ZIpCode2',              'required');
        $form->addRule('ZIpCode2',              'zipcode');
        if ((date('y')-($_POST['YearDateFrom5']))<3 || ((date('y')-($_POST['YearDateFrom5'])) == 3 && date('m')<=($_POST['MonthDateFrom5']))){
            $form->addRule('NameOfPerson2',         'required');
            $form->addRule('StreetAddressOfPerson2','required');
            $form->addRule('StateOfPerson2',        'required');
            $form->addRule('CityCountryOfPerson2',  'required');
            $form->addRule('ZIpCodeOfPerson2',      'required');
            $form->addRule('ZIpCodeOfPerson2',      'zipcode');
        }
    }
    //---------------------------     8 #3    ------------------------
    if ($_POST['MonthDateFrom7']            || $_POST['YearDateFrom7'] || $_POST['StreetAddress3'] || 
        $_POST['CityCountry3']              || $_POST['State3'] || 
        $_POST['ZIpCode3']                  || $_POST['NameOfPerson3'] || 
        $_POST['StreetAddressOfPerson3']    || $_POST['CityCountryOfPerson3'] || 
        $_POST['StateOfPerson3']            || $_POST['ZIpCodeOfPerson3'] || $_POST['MonthDateFrom8']){
        
        //$date = explode('/', $_POST['DateFrom6']);
        $form->addRule('MonthDateFrom7',        'required');
        $form->addRule('YearDateFrom7',         'required');
        $form->addRule('YearDateFrom7',         'yy');
        $form->addRule('StreetAddress3',        'required');
        $form->addRule('CityCountry3',          'required');
        $form->addRule('State3',                'required');
        $form->addRule('ZIpCode3',              'required');
        $form->addRule('ZIpCode3',              'zipcode');
        if ((date('y')-($_POST['YearDateFrom6']))<3 || ((date('y')-($_POST['YearDateFrom6'])) == 3 && date('m')<=($_POST['MonthDateFrom6']))){
            $form->addRule('NameOfPerson3',         'required');
            $form->addRule('StreetAddressOfPerson3','required');
            $form->addRule('StateOfPerson3',        'required');
            $form->addRule('CityCountryOfPerson3',  'required');
            $form->addRule('ZIpCodeOfPerson3',      'required');
            $form->addRule('ZIpCodeOfPerson3',      'zipcode');
        }
    }
    //---------------------------     8 #4    ------------------------
    if ($_POST['MonthDateFrom8']            || $_POST['YearDateFrom8'] || $_POST['StreetAddress4'] || 
        $_POST['CityCountry4']              || $_POST['State4'] || 
        $_POST['ZIpCode4']                  || $_POST['NameOfPerson4'] || 
        $_POST['StreetAddressOfPerson4']    || $_POST['CityCountryOfPerson4'] || 
        $_POST['StateOfPerson4']            || $_POST['ZIpCodeOfPerson4'] || $_POST['MonthDateFrom9']){
        
        //$date = explode('/', $_POST['DateFrom7']);
        $form->addRule('MonthDateFrom8',        'required');
        $form->addRule('YearDateFrom8',         'required');
        $form->addRule('YearDateFrom8',         'yy');
        $form->addRule('StreetAddress4',        'required');
        $form->addRule('CityCountry4',          'required');
        $form->addRule('State4',                'required');
        $form->addRule('ZIpCode4',              'required');
        $form->addRule('ZIpCode4',              'zipcode');
        if ((date('y')-($_POST['YearDateFrom7']))<3 || ((date('y')-($_POST['YearDateFrom7'])) == 3 && date('m')<=($_POST['MonthDateFrom7']))){
            $form->addRule('NameOfPerson4',         'required');
            $form->addRule('StreetAddressOfPerson4','required');
            $form->addRule('StateOfPerson4',        'required');
            $form->addRule('CityCountryOfPerson4',  'required');
            $form->addRule('ZIpCodeOfPerson4',      'required');
            $form->addRule('ZIpCodeOfPerson4',      'zipcode');
        }
    }
    //---------------------------     8 #5    ------------------------
    if ($_POST['MonthDateFrom9']            || $_POST['YearDateFrom9'] || $_POST['StreetAddress5'] || 
        $_POST['CityCountry5']              || $_POST['State5'] || 
        $_POST['ZIpCode5']                  || $_POST['NameOfPerson5'] || 
        $_POST['StreetAddressOfPerson5']    || $_POST['CityCountryOfPerson5'] || 
        $_POST['StateOfPerson5']            || $_POST['ZIpCodeOfPerson5'] || $_POST['aMonthDateFrom5']){
        
        //$date = explode('/', $_POST['DateFrom8']);
        $form->addRule('MonthDateFrom9',        'required');
        $form->addRule('YearDateFrom9',         'required');
        $form->addRule('YearDateFrom9',         'yy');
        $form->addRule('StreetAddress5',        'required');
        $form->addRule('CityCountry5',          'required');
        $form->addRule('State5',                'required');
        $form->addRule('ZIpCode5',              'required');
        $form->addRule('ZIpCode5',              'zipcode');
        if ((date('y')-($_POST['YearDateFrom8']))<3 || ((date('y')-($_POST['YearDateFrom8'])) == 3 && date('m')<=($_POST['MonthDateFrom8']))){
            $form->addRule('NameOfPerson5',         'required');
            $form->addRule('StreetAddressOfPerson5','required');
            $form->addRule('StateOfPerson5',        'required');
            $form->addRule('CityCountryOfPerson5',  'required');
            $form->addRule('ZIpCodeOfPerson5',      'required');
            $form->addRule('ZIpCodeOfPerson5',      'zipcode');
        }
    }
    
    //---------------------------     9 #1    ------------------------
    $form->addRule('MonthDateFrom10',              'required');
    $form->addRule('YearDateFrom10',                'required');
    $form->addRule('MonthDateTo10',                 'required');
    $form->addRule('YearDateTo10',                  'required');
    $form->addRule('YearDateFrom10',                'yy');
    $form->addRule('YearDateTo10',                  'yy');
    $form->addRule('NameOfSchool1',                 'required');
    $form->addRule('Code1',                         'required');
    $form->addRule('Code1',                         'code1_3');
    $form->addRule('DegreesDiplomasOther1',         'required');
    $form->addRule('MonthsYearAwarded1',            'required');
    $form->addRule('MonthsYearAwarded1',            'mmyy');
    $form->addRule('StreetAddresspCityOfSchool1',   'required');
    $form->addRule('StateOfSchool1',                'required');
    $form->addRule('ZIpCodeOfSchool1',              'required');
    $form->addRule('ZIpCodeOfSchool1',              'zipcode');

    //---------------------------     9 #2    ------------------------
    if ($_POST['MonthDateFrom11']               || $_POST['YearDateFrom11'] ||
        $_POST['MonthDateTo11']                 || $_POST['YearDateTo11'] ||
        $_POST['NameOfSchool2']                 || $_POST['Code2'] || 
        $_POST['DegreesDiplomasOther2']         || $_POST['MonthsYearAwarded2'] || 
        $_POST['StreetAddresspCityOfSchool2']   || $_POST['StateOfSchool2'] || 
        $_POST['ZIpCodeOfSchool2']){
        
        $form->addRule('MonthDateFrom11',              'required');
        $form->addRule('YearDateFrom11',                'required');
        $form->addRule('MonthDateTo11',                 'required');
        $form->addRule('YearDateTo11',                  'required');
        $form->addRule('YearDateFrom11',                'yy');
        $form->addRule('YearDateTo11',                  'yy');
        $form->addRule('NameOfSchool2',                 'required');
        $form->addRule('Code2',                         'required');
        $form->addRule('Code2',                         'code1_3');
        $form->addRule('DegreesDiplomasOther2',         'required');
        $form->addRule('MonthsYearAwarded2',            'required');
        $form->addRule('MonthsYearAwarded2',            'mmyy');
        $form->addRule('StreetAddresspCityOfSchool2',   'required');
        $form->addRule('StateOfSchool2',                'required');
        $form->addRule('ZIpCodeOfSchool2',              'required');
        $form->addRule('ZIpCodeOfSchool2',              'zipcode');
    }
    
    //---------------------------     9 #3    ------------------------
    if ($_POST['MonthDateFrom12']               || $_POST['YearDateFrom12'] || 
        $_POST['MonthDateTo12']                 || $_POST['YearDateTo12'] || 
        $_POST['NameOfSchool3']                 || $_POST['Code3'] || 
        $_POST['DegreesDiplomasOther3']         || $_POST['MonthsYearAwarded3'] || 
        $_POST['StreetAddresspCityOfSchool3']   || $_POST['StateOfSchool3'] || 
        $_POST['ZIpCodeOfSchool3']){
        
        $form->addRule('MonthDateFrom12',              'required');
        $form->addRule('YearDateFrom12',                'required');
        $form->addRule('MonthDateTo12',                 'required');
        $form->addRule('YearDateTo12',                  'required');
        $form->addRule('YearDateFrom12',                'yy');
        $form->addRule('YearDateTo12',                  'yy');        
        $form->addRule('NameOfSchool3',                 'required');
        $form->addRule('Code3',                         'required');
        $form->addRule('Code3',                         'code1_3');
        $form->addRule('DegreesDiplomasOther3',         'required');
        $form->addRule('MonthsYearAwarded3',            'required');
        $form->addRule('MonthsYearAwarded3',            'mmyy');
        $form->addRule('StreetAddresspCityOfSchool3',   'required');
        $form->addRule('StateOfSchool3',                'required');
        $form->addRule('ZIpCodeOfSchool3',              'required');
        $form->addRule('ZIpCodeOfSchool3',              'zipcode');
    }
    
    //---------------------------     10 #1    ------------------------
    $form->addRule('MonthDateFrom13',                               'required');
    $form->addRule('YearDateFrom13',                                'required');
    $form->addRule('YearDateFrom13',                                'yy');
    $form->addRule('Code4',                                         'required');
    $form->addRule('EmployersVerifierNamesMilitaryDutyLocation1',   'required');
    if (!$_POST['unemp1']){
    if ($_POST['Code4'] != '7') {
        $form->addRule('PositionTitlesMilitaryRank',                'required');
    }
    $form->addRule('EmployersVerifierStreetAddress1',               'required');
    $form->addRule('EmployersVerifierCityCountry1',                 'required');
    $form->addRule('EmployersVerifierState1',                       'required');
    $form->addRule('EmployersVerifierZIPCode1',                     'required');
    $form->addRule('EmployersVerifierZIPCode1',                     'zipcode');
    $form->addRule('ZIPCodeOfJobLocation1',                         'zipcode');
    $form->addRule('ZIPCodeOfSupervisor1',                          'zipcode');
    $form->addRule('AreaCodeOfEmployersVerifier1',                  'required');
    $form->addRule('AreaCodeOfEmployersVerifier1',                  'area');
    $form->addRule('AreaCodeOfJobLocation1',                        'area');
    $form->addRule('AreaCodeOfSupervisor1',                         'area');
    $form->addRule('PhonenOfEmployersVerifier1',                    'required');
    $form->addRule('PhonenOfEmployersVerifier1',                    'phone');
    $form->addRule('PhonenOfJobLocation1',                          'phone');
    $form->addRule('PhonenOfSupervisor1',                           'phone');
    
    //Block #1
    if ($_POST['MonthDateFrom14']   || $_POST['YearDateFrom14'] || 
        $_POST['MonthDateTo14']     || $_POST['YearDateTo14'] || 
        $_POST['PositionTitle1']    || $_POST['Supervisor1']){
        
        $form->addRule('MonthDateFrom14',  'required');
        $form->addRule('YearDateFrom14',    'required');
        $form->addRule('MonthDateTo14',     'required');
        $form->addRule('YearDateTo14',      'required');
        $form->addRule('YearDateFrom14',    'yy');
        $form->addRule('YearDateTo14',      'yy');        
        $form->addRule('PositionTitle1',    'required');
        $form->addRule('Supervisor1',       'required');
    } 

    if ($_POST['MonthDateFrom15']   || $_POST['YearDateFrom15'] || 
        $_POST['MonthDateTo15']     || $_POST['YearDateTo15'] || 
        $_POST['PositionTitle2']    || $_POST['Supervisor2']){
        
        $form->addRule('MonthDateFrom15',  'required');
        $form->addRule('YearDateFrom15',    'required');
        $form->addRule('MonthDateTo15',     'required');
        $form->addRule('YearDateTo15',      'required');
        $form->addRule('YearDateFrom15',    'yy');
        $form->addRule('YearDateTo15',      'yy');        
        $form->addRule('PositionTitle2',    'required');
        $form->addRule('Supervisor2',       'required');
    }

    if ($_POST['MonthDateFrom16']   || $_POST['YearDateFrom16'] || 
        $_POST['MonthDateTo16']     || $_POST['YearDateTo16'] || 
        $_POST['PositionTitle3']    || $_POST['Supervisor3']){
        
        $form->addRule('MonthDateFrom16',  'required');
        $form->addRule('YearDateFrom16',    'required');
        $form->addRule('MonthDateTo16',     'required');
        $form->addRule('YearDateTo16',      'required');
        $form->addRule('YearDateFrom16',    'yy');
        $form->addRule('YearDateTo16',      'yy');        
        $form->addRule('PositionTitle3',    'required');
        $form->addRule('Supervisor3',       'required');
    }
    }
    
    //---------------------------     10 #2    ------------------------
    if ($_POST['MonthDateFrom17']                               || $_POST['YearDateFrom17'] || $_POST['Code5'] || 
        $_POST['EmployersVerifierNamesMilitaryDutyLocation2']   || $_POST['PositionTitlesMilitaryRank2'] || 
        $_POST['EmployersVerifierStreetAddress2']               || $_POST['EmployersVerifierCityCountry2'] || 
        $_POST['EmployersVerifierState2']                       || $_POST['EmployersVerifierZIPCode2'] || 
        $_POST['AreaCodeOfEmployersVerifier2']                  || $_POST['PhonenOfEmployersVerifier2'] || 
        $_POST['StreetAddressOfJobLocation2']                   || $_POST['CityCountryOfJobLocation2'] || 
        $_POST['StateOfJobLocation2']                           || $_POST['ZIPCodeOfJobLocation2'] || 
        $_POST['AreaCodeOfJobLocation2']                        || $_POST['PhonenOfJobLocation2'] || 
        $_POST['SupervisorNamepStreetAddress2']                 || $_POST['CityCountryOfSupervisor2'] || 
        $_POST['StateOfSupervisor2']                            || $_POST['ZIPCodeOfSupervisor2'] || 
        $_POST['AreaCodeOfSupervisor2']                         || $_POST['PhonenOfSupervisor2'] || 
        $_POST['MonthDateFrom18']                               || $_POST['MonthDateTo18'] || 
        $_POST['YearDateFrom18']                                || $_POST['YearDateTo18'] || 
        $_POST['PositionTitle4']                                || $_POST['Supervisor4'] || 
        $_POST['MonthDateFrom19']                               || $_POST['MonthDateTo19'] || 
        $_POST['YearDateFrom19']                                || $_POST['YearDateTo19'] || 
        $_POST['PositionTitle5']                                || $_POST['Supervisor5'] || 
        $_POST['MonthDateFrom20']                               || $_POST['MonthDateTo20'] || 
        $_POST['YearDateFrom20']                                || $_POST['YearDateTo20'] || 
        $_POST['PositionTitle6']                                || $_POST['Supervisor6'] || $_POST['MonthDateFrom21']){
        
        $form->addRule('MonthDateFrom17',                               'required');
        $form->addRule('YearDateFrom17',                                'required');
        $form->addRule('YearDateFrom17',                                'yy');
        $form->addRule('Code5',                                         'required');
        $form->addRule('EmployersVerifierNamesMilitaryDutyLocation2',   'required');
        if (!$_POST['unemp2']){
        if ($_POST['Code5'] != '7') {
            $form->addRule('PositionTitlesMilitaryRank2',               'required');
        }
        $form->addRule('EmployersVerifierStreetAddress2',               'required');
        $form->addRule('EmployersVerifierCityCountry2',                 'required');
        $form->addRule('EmployersVerifierState2',                       'required');
        $form->addRule('EmployersVerifierZIPCode2',                     'required');
        $form->addRule('EmployersVerifierZIPCode2',                     'zipcode');
        $form->addRule('ZIPCodeOfJobLocation2',                         'zipcode');
        $form->addRule('ZIPCodeOfSupervisor2',                          'zipcode');
        $form->addRule('AreaCodeOfEmployersVerifier2',                  'required');
        $form->addRule('AreaCodeOfEmployersVerifier2',                  'area');
        $form->addRule('AreaCodeOfJobLocation2',                        'area');
        $form->addRule('AreaCodeOfSupervisor2',                         'area');
        $form->addRule('PhonenOfEmployersVerifier2',                    'required');
        $form->addRule('PhonenOfEmployersVerifier2',                    'phone');
        $form->addRule('PhonenOfJobLocation2',                          'phone');
        $form->addRule('PhonenOfSupervisor2',                           'phone');

    
        //Block #2
        if ($_POST['MonthDateFrom18']   || $_POST['YearDateFrom18'] || 
            $_POST['MonthDateTo18']     || $_POST['YearDateTo18'] || 
            $_POST['PositionTitle4']    || $_POST['Supervisor4']){
        
            $form->addRule('MonthDateFrom18',  'required');
            $form->addRule('YearDateFrom18',    'required');
            $form->addRule('MonthDateTo18',     'required');
            $form->addRule('YearDateTo18',      'required');
            $form->addRule('YearDateFrom18',    'yy');
            $form->addRule('YearDateTo18',      'yy');        
            $form->addRule('PositionTitle4',    'required');
            $form->addRule('Supervisor4',       'required');
        } 

        if ($_POST['MonthDateFrom19']   || $_POST['YearDateFrom19'] || 
            $_POST['MonthDateTo19']     || $_POST['YearDateTo19'] || 
            $_POST['PositionTitle5']    || $_POST['Supervisor5']){
        
            $form->addRule('MonthDateFrom19',  'required');
            $form->addRule('YearDateFrom19',    'required');
            $form->addRule('MonthDateTo19',     'required');
            $form->addRule('YearDateTo19',      'required');
            $form->addRule('YearDateFrom19',    'yy');
            $form->addRule('YearDateTo19',      'yy');        
            $form->addRule('PositionTitle5',    'required');
            $form->addRule('Supervisor5',       'required');
        }

        if ($_POST['MonthDateFrom20']   || $_POST['YearDateFrom20'] || 
            $_POST['MonthDateTo20']     || $_POST['YearDateTo20'] || 
            $_POST['PositionTitle6']    || $_POST['Supervisor6']){
        
            $form->addRule('MonthDateFrom20',  'required');
            $form->addRule('YearDateFrom20',    'required');
            $form->addRule('MonthDateTo20',     'required');
            $form->addRule('YearDateTo20',      'required');
            $form->addRule('YearDateFrom20',    'yy');
            $form->addRule('YearDateTo20',      'yy');        
            $form->addRule('PositionTitle6',    'required');
            $form->addRule('Supervisor6',       'required');
        }
        }
    }
     
    //---------------------------     10 #3    ------------------------
    if ($_POST['MonthDateFrom21']                               || $_POST['YearDateFrom21'] || $_POST['Code6'] || 
        $_POST['EmployersVerifierNamesMilitaryDutyLocation3']   || $_POST['PositionTitlesMilitaryRank3'] || 
        $_POST['EmployersVerifierStreetAddress3']               || $_POST['EmployersVerifierCityCountry3'] || 
        $_POST['EmployersVerifierState3']                       || $_POST['EmployersVerifierZIPCode3'] || 
        $_POST['AreaCodeOfEmployersVerifier3']                  || $_POST['PhonenOfEmployersVerifier3'] || 
        $_POST['StreetAddressOfJobLocation3']                   || $_POST['CityCountryOfJobLocation3'] || 
        $_POST['StateOfJobLocation3']                           || $_POST['ZIPCodeOfJobLocation3'] || 
        $_POST['AreaCodeOfJobLocation3']                        || $_POST['PhonenOfJobLocation3'] || 
        $_POST['SupervisorNamepStreetAddress3']                 || $_POST['CityCountryOfSupervisor3'] || 
        $_POST['StateOfSupervisor3']                            || $_POST['ZIPCodeOfSupervisor3'] || 
        $_POST['AreaCodeOfSupervisor3']                         || $_POST['PhonenOfSupervisor3'] || 
        $_POST['MonthDateFrom22']                               || $_POST['MonthDateTo22'] || 
        $_POST['YearDateFrom22']                                || $_POST['YearDateTo22'] || 
        $_POST['PositionTitle7']                                || $_POST['Supervisor7'] || 
        $_POST['MonthDateFrom23']                               || $_POST['MonthDateTo23'] || 
        $_POST['YearDateFrom23']                                || $_POST['YearDateTo23'] || 
        $_POST['PositionTitle8']                                || $_POST['Supervisor8'] || 
        $_POST['MonthDateFrom24']                               || $_POST['MonthDateTo24'] || 
        $_POST['YearDateFrom24']                                || $_POST['YearDateTo24'] || 
        $_POST['PositionTitle9']                                || $_POST['Supervisor9'] || $_POST['MonthDateFrom25']){
        
        $form->addRule('MonthDateFrom21',                               'required');
        $form->addRule('YearDateFrom21',                                'required');
        $form->addRule('YearDateFrom21',                                'yy');
        $form->addRule('Code6',                                         'required');
        $form->addRule('EmployersVerifierNamesMilitaryDutyLocation3',   'required');
        if (!$_POST['unemp3']){
        if ($_POST['Code6'] != '7') {
            $form->addRule('PositionTitlesMilitaryRank3',               'required');
        }
        $form->addRule('EmployersVerifierStreetAddress3',               'required');
        $form->addRule('EmployersVerifierCityCountry3',                 'required');
        $form->addRule('EmployersVerifierState3',                       'required');
        $form->addRule('EmployersVerifierZIPCode3',                     'required');
        $form->addRule('EmployersVerifierZIPCode3',                     'zipcode');
        $form->addRule('ZIPCodeOfJobLocation3',                         'zipcode');
        $form->addRule('ZIPCodeOfSupervisor3',                          'zipcode');
        $form->addRule('AreaCodeOfEmployersVerifier3',                  'required');
        $form->addRule('AreaCodeOfEmployersVerifier3',                  'area');
        $form->addRule('AreaCodeOfJobLocation3',                        'area');
        $form->addRule('AreaCodeOfSupervisor3',                         'area');
        $form->addRule('PhonenOfEmployersVerifier3',                    'required');
        $form->addRule('PhonenOfEmployersVerifier3',                    'phone');
        $form->addRule('PhonenOfJobLocation3',                          'phone');
        $form->addRule('PhonenOfSupervisor3',                           'phone');

    
        //Block #3
        if ($_POST['MonthDateFrom22']   || $_POST['YearDateFrom22'] || 
            $_POST['MonthDateTo22']     || $_POST['YearDateTo22'] || 
            $_POST['PositionTitle7']    || $_POST['Supervisor7']){
        
            $form->addRule('MonthDateFrom22',  'required');
            $form->addRule('YearDateFrom22',    'required');
            $form->addRule('MonthDateTo22',     'required');
            $form->addRule('YearDateTo22',      'required');
            $form->addRule('YearDateFrom22',    'yy');
            $form->addRule('YearDateTo22',      'yy');        
            $form->addRule('PositionTitle7',    'required');
            $form->addRule('Supervisor7',       'required');
        } 

        if ($_POST['MonthDateFrom23']   || $_POST['YearDateFrom23'] || 
            $_POST['MonthDateTo23']     || $_POST['YearDateTo23'] || 
            $_POST['PositionTitle8']    || $_POST['Supervisor8']){
        
            $form->addRule('MonthDateFrom23',  'required');
            $form->addRule('YearDateFrom23',    'required');
            $form->addRule('MonthDateTo23',     'required');
            $form->addRule('YearDateTo23',      'required');
            $form->addRule('YearDateFrom23',    'yy');
            $form->addRule('YearDateTo23',      'yy');        
            $form->addRule('PositionTitle8',    'required');
            $form->addRule('Supervisor8',       'required');
        }

        if ($_POST['MonthDateFrom24']   || $_POST['YearDateFrom24'] || 
            $_POST['MonthDateTo24']     || $_POST['YearDateTo24'] || 
            $_POST['PositionTitle9']    || $_POST['Supervisor9']){
        
            $form->addRule('MonthDateFrom24',  'required');
            $form->addRule('YearDateFrom24',    'required');
            $form->addRule('MonthDateTo24',     'required');
            $form->addRule('YearDateTo24',      'required');
            $form->addRule('YearDateFrom24',    'yy');
            $form->addRule('YearDateTo24',      'yy');        
            $form->addRule('PositionTitle9',    'required');
            $form->addRule('Supervisor9',       'required');
        }
        }
    }
     
    //---------------------------     10 #4    ------------------------
    if ($_POST['MonthDateFrom25']                               || $_POST['YearDateFrom25'] || $_POST['Code7'] || 
        $_POST['EmployersVerifierNamesMilitaryDutyLocation4']   || $_POST['PositionTitlesMilitaryRank4'] || 
        $_POST['EmployersVerifierStreetAddress4']               || $_POST['EmployersVerifierCityCountry4'] || 
        $_POST['EmployersVerifierState4']                       || $_POST['EmployersVerifierZIPCode4'] || 
        $_POST['AreaCodeOfEmployersVerifier4']                  || $_POST['PhonenOfEmployersVerifier4'] || 
        $_POST['StreetAddressOfJobLocation4']                   || $_POST['CityCountryOfJobLocation4'] || 
        $_POST['StateOfJobLocation4']                           || $_POST['ZIPCodeOfJobLocation4'] || 
        $_POST['AreaCodeOfJobLocation4']                        || $_POST['PhonenOfJobLocation4'] || 
        $_POST['SupervisorNamepStreetAddress4']                 || $_POST['CityCountryOfSupervisor4'] || 
        $_POST['StateOfSupervisor4']                            || $_POST['ZIPCodeOfSupervisor4'] || 
        $_POST['AreaCodeOfSupervisor4']                         || $_POST['PhonenOfSupervisor4'] || 
        $_POST['MonthDateFrom26']                               || $_POST['MonthDateTo26'] || 
        $_POST['YearDateFrom26']                                || $_POST['YearDateTo26'] || 
        $_POST['PositionTitle10']                               || $_POST['Supervisor10'] || 
        $_POST['MonthDateFrom27']                               || $_POST['MonthDateTo27'] || 
        $_POST['YearDateFrom27']                                || $_POST['YearDateTo27'] || 
        $_POST['PositionTitle11']                               || $_POST['Supervisor11'] || 
        $_POST['MonthDateFrom28']                               || $_POST['MonthDateTo28'] || 
        $_POST['YearDateFrom28']                                || $_POST['YearDateTo28'] || 
        $_POST['PositionTitle12']                               || $_POST['Supervisor12'] || $_POST['MonthDateFrom29']){
        
        $form->addRule('MonthDateFrom25',                               'required');
        $form->addRule('YearDateFrom25',                                'required');
        $form->addRule('YearDateFrom25',                                'yy');
        $form->addRule('Code7',                                         'required');
        $form->addRule('EmployersVerifierNamesMilitaryDutyLocation4',   'required');
        if (!$_POST['unemp4']){
        if ($_POST['Code7'] != '7') {
            $form->addRule('PositionTitlesMilitaryRank4',               'required');
        }
        $form->addRule('EmployersVerifierStreetAddress4',               'required');
        $form->addRule('EmployersVerifierCityCountry4',                 'required');
        $form->addRule('EmployersVerifierState4',                       'required');
        $form->addRule('EmployersVerifierZIPCode4',                     'required');
        $form->addRule('EmployersVerifierZIPCode4',                     'zipcode');
        $form->addRule('ZIPCodeOfJobLocation4',                         'zipcode');
        $form->addRule('ZIPCodeOfSupervisor4',                          'zipcode');
        $form->addRule('AreaCodeOfEmployersVerifier4',                  'required');
        $form->addRule('AreaCodeOfEmployersVerifier4',                  'area');
        $form->addRule('AreaCodeOfJobLocation4',                        'area');
        $form->addRule('AreaCodeOfSupervisor4',                         'area');
        $form->addRule('PhonenOfEmployersVerifier4',                    'required');
        $form->addRule('PhonenOfEmployersVerifier4',                    'phone');
        $form->addRule('PhonenOfJobLocation4',                          'phone');
        $form->addRule('PhonenOfSupervisor4',                           'phone');

    
        //Block #4
        if ($_POST['MonthDateFrom26']   || $_POST['YearDateFrom26'] || 
            $_POST['MonthDateTo26']     || $_POST['YearDateTo26'] || 
            $_POST['PositionTitle10']   || $_POST['Supervisor10']){
        
            $form->addRule('MonthDateFrom26',  'required');
            $form->addRule('YearDateFrom26',    'required');
            $form->addRule('MonthDateTo26',     'required');
            $form->addRule('YearDateTo26',      'required');
            $form->addRule('YearDateFrom26',    'yy');
            $form->addRule('YearDateTo26',      'yy');        
            $form->addRule('PositionTitle10',   'required');
            $form->addRule('Supervisor10',      'required');
        } 

        if ($_POST['MonthDateFrom27']   || $_POST['YearDateFrom27'] || 
            $_POST['MonthDateTo27']     || $_POST['YearDateTo27'] || 
            $_POST['PositionTitle11']   || $_POST['Supervisor11']){
        
            $form->addRule('MonthDateFrom27',  'required');
            $form->addRule('YearDateFrom27',    'required');
            $form->addRule('MonthDateTo27',     'required');
            $form->addRule('YearDateTo27',      'required');
            $form->addRule('YearDateFrom27',    'yy');
            $form->addRule('YearDateTo27',      'yy');        
            $form->addRule('PositionTitle11',   'required');
            $form->addRule('Supervisor11',      'required');
        }

        if ($_POST['MonthDateFrom28']   || $_POST['YearDateFrom28'] || 
            $_POST['MonthDateTo28']     || $_POST['YearDateTo28'] || 
            $_POST['PositionTitle12']   || $_POST['Supervisor12']){
        
            $form->addRule('MonthDateFrom28',  'required');
            $form->addRule('YearDateFrom28',    'required');
            $form->addRule('MonthDateTo28',     'required');
            $form->addRule('YearDateTo28',      'required');
            $form->addRule('YearDateFrom28',    'yy');
            $form->addRule('YearDateTo28',      'yy');        
            $form->addRule('PositionTitle12',   'required');
            $form->addRule('Supervisor12',      'required');
        }
        }
    }
     
    //---------------------------     10 #5    ------------------------
    if ($_POST['MonthDateFrom29']                               || $_POST['YearDateFrom29'] || $_POST['Code8'] || 
        $_POST['EmployersVerifierNamesMilitaryDutyLocation5']   || $_POST['PositionTitlesMilitaryRank5'] || 
        $_POST['EmployersVerifierStreetAddress5']               || $_POST['EmployersVerifierCityCountry5'] || 
        $_POST['EmployersVerifierState5']                       || $_POST['EmployersVerifierZIPCode5'] || 
        $_POST['AreaCodeOfEmployersVerifier5']                  || $_POST['PhonenOfEmployersVerifier5'] || 
        $_POST['StreetAddressOfJobLocation5']                   || $_POST['CityCountryOfJobLocation5'] || 
        $_POST['StateOfJobLocation5']                           || $_POST['ZIPCodeOfJobLocation5'] || 
        $_POST['AreaCodeOfJobLocation5']                        || $_POST['PhonenOfJobLocation5'] || 
        $_POST['SupervisorNamepStreetAddress5']                 || $_POST['CityCountryOfSupervisor5'] || 
        $_POST['StateOfSupervisor5']                            || $_POST['ZIPCodeOfSupervisor5'] || 
        $_POST['AreaCodeOfSupervisor5']                         || $_POST['PhonenOfSupervisor5'] || 
        $_POST['MonthDateFrom30']                               || $_POST['MonthDateTo30'] || 
        $_POST['YearDateFrom30']                                || $_POST['YearDateTo30'] || 
        $_POST['PositionTitle13']                               || $_POST['Supervisor13'] || 
        $_POST['MonthDateFrom31']                               || $_POST['MonthDateTo31'] || 
        $_POST['YearDateFrom31']                                || $_POST['YearDateTo31'] || 
        $_POST['PositionTitle14']                               || $_POST['Supervisor14'] || 
        $_POST['MonthDateFrom32']                               || $_POST['MonthDateTo32'] || 
        $_POST['YearDateFrom32']                                || $_POST['YearDateTo32'] || 
        $_POST['PositionTitle15']                               || $_POST['Supervisor15'] || $_POST['MonthDateFrom33']){
        
        $form->addRule('MonthDateFrom29',                               'required');
        $form->addRule('YearDateFrom29',                                'required');
        $form->addRule('YearDateFrom29',                                'yy');
        $form->addRule('Code8',                                         'required');
        $form->addRule('EmployersVerifierNamesMilitaryDutyLocation5',   'required');
        if (!$_POST['unemp5']){
        if ($_POST['Code8'] != '7') {
            $form->addRule('PositionTitlesMilitaryRank5',               'required');
        }
        $form->addRule('EmployersVerifierStreetAddress5',               'required');
        $form->addRule('EmployersVerifierCityCountry5',                 'required');
        $form->addRule('EmployersVerifierState5',                       'required');
        $form->addRule('EmployersVerifierZIPCode5',                     'required');
        $form->addRule('EmployersVerifierZIPCode5',                     'zipcode');
        $form->addRule('ZIPCodeOfJobLocation5',                         'zipcode');
        $form->addRule('ZIPCodeOfSupervisor5',                          'zipcode');
        $form->addRule('AreaCodeOfEmployersVerifier5',                  'required');
        $form->addRule('AreaCodeOfEmployersVerifier5',                  'area');
        $form->addRule('AreaCodeOfJobLocation5',                        'area');
        $form->addRule('AreaCodeOfSupervisor5',                         'area');
        $form->addRule('PhonenOfEmployersVerifier5',                    'required');
        $form->addRule('PhonenOfEmployersVerifier5',                    'phone');
        $form->addRule('PhonenOfJobLocation5',                          'phone');
        $form->addRule('PhonenOfSupervisor5',                           'phone');

    
        //Block #5
        if ($_POST['MonthDateFrom30']   || $_POST['YearDateFrom30'] || 
            $_POST['MonthDateTo30']     || $_POST['YearDateTo30'] || 
            $_POST['PositionTitle13']   || $_POST['Supervisor13']){
        
            $form->addRule('MonthDateFrom30',  'required');
            $form->addRule('YearDateFrom30',    'required');
            $form->addRule('MonthDateTo30',     'required');
            $form->addRule('YearDateTo30',      'required');
            $form->addRule('YearDateFrom30',    'yy');
            $form->addRule('YearDateTo30',      'yy');        
            $form->addRule('PositionTitle13',   'required');
            $form->addRule('Supervisor13',      'required');
        } 

        if ($_POST['MonthDateFrom31']   || $_POST['YearDateFrom31'] || 
            $_POST['MonthDateTo31']     || $_POST['YearDateTo31'] || 
            $_POST['PositionTitle14']   || $_POST['Supervisor14']){
        
            $form->addRule('MonthDateFrom31',  'required');
            $form->addRule('YearDateFrom31',    'required');
            $form->addRule('MonthDateTo31',     'required');
            $form->addRule('YearDateTo31',      'required');
            $form->addRule('YearDateFrom31',    'yy');
            $form->addRule('YearDateTo31',      'yy');        
            $form->addRule('PositionTitle31',   'required');
            $form->addRule('Supervisor31',      'required');
        }

        if ($_POST['MonthDateFrom32']   || $_POST['YearDateFrom32'] || 
            $_POST['MonthDateTo32']     || $_POST['YearDateTo32'] || 
            $_POST['PositionTitle15']   || $_POST['Supervisor15']){
        
            $form->addRule('MonthDateFrom32',  'required');
            $form->addRule('YearDateFrom32',    'required');
            $form->addRule('MonthDateTo32',     'required');
            $form->addRule('YearDateTo32',      'required');
            $form->addRule('YearDateFrom32',    'yy');
            $form->addRule('YearDateTo32',      'yy');        
            $form->addRule('PositionTitle15',   'required');
            $form->addRule('Supervisor15',      'required');
        }
        }
    }
     
    //---------------------------     10 #6    ------------------------
    if ($_POST['MonthDateFrom33']                               || $_POST['YearDateFrom33'] || $_POST['Code9'] || 
        $_POST['EmployersVerifierNamesMilitaryDutyLocation6']   || $_POST['PositionTitlesMilitaryRank6'] || 
        $_POST['EmployersVerifierStreetAddress6']               || $_POST['EmployersVerifierCityCountry6'] || 
        $_POST['EmployersVerifierState6']                       || $_POST['EmployersVerifierZIPCode6'] || 
        $_POST['AreaCodeOfEmployersVerifier6']                  || $_POST['PhonenOfEmployersVerifier6'] || 
        $_POST['StreetAddressOfJobLocation6']                   || $_POST['CityCountryOfJobLocation6'] || 
        $_POST['StateOfJobLocation6']                           || $_POST['ZIPCodeOfJobLocation6'] || 
        $_POST['AreaCodeOfJobLocation6']                        || $_POST['PhonenOfJobLocation6'] || 
        $_POST['SupervisorNamepStreetAddress6']                 || $_POST['CityCountryOfSupervisor6'] || 
        $_POST['StateOfSupervisor6']                            || $_POST['ZIPCodeOfSupervisor6'] || 
        $_POST['AreaCodeOfSupervisor6']                         || $_POST['PhonenOfSupervisor6'] || 
        $_POST['MonthDateFrom34']                               || $_POST['MonthDateTo34'] || 
        $_POST['YearDateFrom34']                                || $_POST['YearDateTo34'] || 
        $_POST['PositionTitle16']                               || $_POST['Supervisor16'] || 
        $_POST['MonthDateFrom35']                               || $_POST['MonthDateTo35'] || 
        $_POST['YearDateFrom35']                                || $_POST['YearDateTo35'] || 
        $_POST['PositionTitle17']                               || $_POST['Supervisor17'] || 
        $_POST['MonthDateFrom36']                               || $_POST['MonthDateTo36'] || 
        $_POST['YearDateFrom36']                                || $_POST['YearDateTo36'] || 
        $_POST['PositionTitle18']                               || $_POST['Supervisor18'] || $_POST['aMonthDateFrom13']){
        
        $form->addRule('MonthDateFrom33',                               'required');
        $form->addRule('YearDateFrom33',                                'required');
        $form->addRule('YearDateFrom33',                                'yy');
        $form->addRule('Code9',                                         'required');
        $form->addRule('EmployersVerifierNamesMilitaryDutyLocation6',   'required');
        if (!$_POST['unemp6']){
        if ($_POST['Code9'] != '7') {
            $form->addRule('PositionTitlesMilitaryRank6',               'required');
        }
        $form->addRule('EmployersVerifierStreetAddress6',               'required');
        $form->addRule('EmployersVerifierCityCountry6',                 'required');
        $form->addRule('EmployersVerifierState6',                       'required');
        $form->addRule('EmployersVerifierZIPCode6',                     'required');
        $form->addRule('EmployersVerifierZIPCode6',                     'zipcode');
        $form->addRule('ZIPCodeOfJobLocation6',                         'zipcode');
        $form->addRule('ZIPCodeOfSupervisor6',                          'zipcode');
        $form->addRule('AreaCodeOfEmployersVerifier6',                  'required');
        $form->addRule('AreaCodeOfEmployersVerifier6',                  'area');
        $form->addRule('AreaCodeOfJobLocation6',                        'area');
        $form->addRule('AreaCodeOfSupervisor6',                         'area');
        $form->addRule('PhonenOfEmployersVerifier6',                    'required');
        $form->addRule('PhonenOfEmployersVerifier6',                    'phone');
        $form->addRule('PhonenOfJobLocation6',                          'phone');
        $form->addRule('PhonenOfSupervisor6',                           'phone');

    
        //Block #6
        if ($_POST['MonthDateFrom34']   || $_POST['YearDateFrom34'] || 
            $_POST['MonthDateTo34']     || $_POST['YearDateTo34'] || 
            $_POST['PositionTitle16']   || $_POST['Supervisor16']){
        
            $form->addRule('MonthDateFrom34',  'required');
            $form->addRule('YearDateFrom34',    'required');
            $form->addRule('MonthDateTo34',     'required');
            $form->addRule('YearDateTo34',      'required');
            $form->addRule('YearDateFrom34',    'yy');
            $form->addRule('YearDateTo34',      'yy');        
            $form->addRule('PositionTitle16',   'required');
            $form->addRule('Supervisor16',      'required');
        } 

        if ($_POST['MonthDateFrom35']   || $_POST['YearDateFrom35'] || 
            $_POST['MonthDateTo35']     || $_POST['YearDateTo35'] || 
            $_POST['PositionTitle17']   || $_POST['Supervisor17']){
        
            $form->addRule('MonthDateFrom35',  'required');
            $form->addRule('YearDateFrom35',    'required');
            $form->addRule('MonthDateTo35',     'required');
            $form->addRule('YearDateTo35',      'required');
            $form->addRule('YearDateFrom35',    'yy');
            $form->addRule('YearDateTo35',      'yy');        
            $form->addRule('PositionTitle17',   'required');
            $form->addRule('Supervisor17',      'required');
        }

        if ($_POST['MonthDateFrom36']   || $_POST['YearDateFrom36'] || 
            $_POST['MonthDateTo36']     || $_POST['YearDateTo36'] || 
            $_POST['PositionTitle18']   || $_POST['Supervisor18']){
        
            $form->addRule('MonthDateFrom36',  'required');
            $form->addRule('YearDateFrom36',    'required');
            $form->addRule('MonthDateTo36',     'required');
            $form->addRule('YearDateTo36',      'required');
            $form->addRule('YearDateFrom36',    'yy');
            $form->addRule('YearDateTo36',      'yy');        
            $form->addRule('PositionTitle18',   'required');
            $form->addRule('Supervisor18',      'required');
        }
        }
    }
    
    //---------------------------------------------------------     SF86A     -------------------------------------------------------
    if ($_POST['add8']){ 
        //---------------------------     8 #6    ------------------------
        $form->addRule('aMonthDateFrom5',        'required');
        $form->addRule('aYearDateFrom5',         'required');
        $form->addRule('aYearDateFrom5',         'yy');
        $form->addRule('aStreetAddress1',        'required');
        $form->addRule('aCityCountry1',          'required');
        $form->addRule('aState1',                'required');
        $form->addRule('aZIpCode1',              'required');
        $form->addRule('aZIpCode1',              'zipcode');
        if ((date('y')-($_POST['YearDateFrom9']))<3 || ((date('y')-($_POST['YearDateFrom9'])) == 3 && date('m')<=($_POST['MonthDateFrom9']))){
            $form->addRule('aNameOfPerson1',         'required');
            $form->addRule('aStreetAddressOfPerson1','required');
            $form->addRule('aCityCountryOfPerson1',  'required');
            $form->addRule('aStateOfPerson1',        'required');
            $form->addRule('aZIpCodeOfPerson1',      'required');
            $form->addRule('aZIpCodeOfPerson1',      'zipcode');
        }
        
        //---------------------------     8 #7    ------------------------
        if ($_POST['aMonthDateFrom6']            || $_POST['aYearDateFrom6'] || $_POST['aStreetAddress2'] || 
            $_POST['aCityCountry2']              || $_POST['aState2'] || 
            $_POST['aZIpCode2']                  || $_POST['aNameOfPerson2'] || 
            $_POST['aStreetAddressOfPerson2']    || $_POST['aCityCountryOfPerson2'] || 
            $_POST['aStateOfPerson2']            || $_POST['aZIpCodeOfPerson2'] || $_POST['aMonthDateFrom7']){
            
            //$date = explode('/', $_POST['aDateFrom5']);
            $form->addRule('aMonthDateFrom6',        'required');
            $form->addRule('aYearDateFrom6',         'required');
            $form->addRule('aYearDateFrom6',         'yy');
            $form->addRule('aStreetAddress2',        'required');
            $form->addRule('aCityCountry2',          'required');
            $form->addRule('aState2',                'required');
            $form->addRule('aZIpCode2',              'required');
            $form->addRule('aZIpCode2',              'zipcode');
            if ((date('y')-($_POST['aYearDateFrom5']))<3 || ((date('y')-($_POST['aYearDateFrom5'])) == 3 && date('m')<=($_POST['aMonthDateFrom5']))){
                $form->addRule('aNameOfPerson2',         'required');
                $form->addRule('aStreetAddressOfPerson2','required');
                $form->addRule('aStateOfPerson2',        'required');
                $form->addRule('aCityCountryOfPerson2',  'required');
                $form->addRule('aZIpCodeOfPerson2',      'required');
                $form->addRule('aZIpCodeOfPerson2',      'zipcode');
            }
        }
        //---------------------------     8 #8    ------------------------
        if ($_POST['aMonthDateFrom7']            || $_POST['aYearDateFrom7'] || $_POST['aStreetAddress3'] || 
            $_POST['aCityCountry3']              || $_POST['aState3'] || 
            $_POST['aZIpCode3']                  || $_POST['aNameOfPerson3'] || 
            $_POST['aStreetAddressOfPerson3']    || $_POST['aCityCountryOfPerson3'] || 
            $_POST['aStateOfPerson3']            || $_POST['aZIpCodeOfPerson3'] || $_POST['aMonthDateFrom8']){
            
            //$date = explode('/', $_POST['aDateFrom6']);
            $form->addRule('aMonthDateFrom7',        'required');
            $form->addRule('aYearDateFrom7',         'required');
            $form->addRule('aYearDateFrom7',         'yy');
            $form->addRule('aStreetAddress3',        'required');
            $form->addRule('aCityCountry3',          'required');
            $form->addRule('aState3',                'required');
            $form->addRule('aZIpCode3',              'required');
            $form->addRule('aZIpCode3',              'zipcode');
            if ((date('y')-($_POST['aYearDateFrom6']))<3 || ((date('y')-($_POST['aYearDateFrom6'])) == 3 && date('m')<=($_POST['aMonthDateFrom6']))){
                $form->addRule('aNameOfPerson3',         'required');
                $form->addRule('aStreetAddressOfPerson3','required');
                $form->addRule('aStateOfPerson3',        'required');
                $form->addRule('aCityCountryOfPerson3',  'required');
                $form->addRule('aZIpCodeOfPerson3',      'required');
                $form->addRule('aZIpCodeOfPerson3',      'zipcode');
            }
        }
        //---------------------------     8 #9    ------------------------
        if ($_POST['aMonthDateFrom8']            || $_POST['aYearDateFrom8'] || $_POST['aStreetAddress4'] || 
            $_POST['aCityCountry4']              || $_POST['aState4'] || 
            $_POST['aZIpCode4']                  || $_POST['aNameOfPerson4'] || 
            $_POST['aStreetAddressOfPerson4']    || $_POST['aCityCountryOfPerson4'] || 
            $_POST['aStateOfPerson4']            || $_POST['aZIpCodeOfPerson4'] || $_POST['aMonthDateFrom9']){
            
            //$date = explode('/', $_POST['aDateFrom7']);
            $form->addRule('aMonthDateFrom8',        'required');
            $form->addRule('aYearDateFrom8',         'required');
            $form->addRule('aYearDateFrom8',         'yy');
            $form->addRule('aStreetAddress4',        'required');
            $form->addRule('aCityCountry4',          'required');
            $form->addRule('aState4',                'required');
            $form->addRule('aZIpCode4',              'required');
            $form->addRule('aZIpCode4',              'zipcode');
            if ((date('y')-($_POST['aYearDateFrom7']))<3 || ((date('y')-($_POST['aYearDateFrom7'])) == 3 && date('m')<=($_POST['aMonthDateFrom7']))){
                $form->addRule('aNameOfPerson4',         'required');
                $form->addRule('aStreetAddressOfPerson4','required');
                $form->addRule('aStateOfPerson4',        'required');
                $form->addRule('aCityCountryOfPerson4',  'required');
                $form->addRule('aZIpCodeOfPerson4',      'required');
                $form->addRule('aZIpCodeOfPerson4',      'zipcode');
            }
        }
        //---------------------------     8 #10    ------------------------
        if ($_POST['aMonthDateFrom9']            || $_POST['aYearDateFrom9'] || $_POST['aStreetAddress5'] || 
            $_POST['aCityCountry5']              || $_POST['aState5'] || 
            $_POST['aZIpCode5']                  || $_POST['aNameOfPerson5'] || 
            $_POST['aStreetAddressOfPerson5']    || $_POST['aCityCountryOfPerson5'] || 
            $_POST['aStateOfPerson5']            || $_POST['aZIpCodeOfPerson5']){
            
            //$date = explode('/', $_POST['aDateFrom8']);
            $form->addRule('aMonthDateFrom9',        'required');
            $form->addRule('aYearDateFrom9',         'required');
            $form->addRule('aYearDateFrom9',         'yy');
            $form->addRule('aStreetAddress5',        'required');
            $form->addRule('aCityCountry5',          'required');
            $form->addRule('aState5',                'required');
            $form->addRule('aZIpCode5',              'required');
            $form->addRule('aZIpCode5',              'zipcode');
            if ((date('y')-($_POST['aYearDateFrom8']))<3 || ((date('y')-($_POST['aYearDateFrom8'])) == 3 && date('m')<=($_POST['aMonthDateFrom8']))){
                $form->addRule('aNameOfPerson5',         'required');
                $form->addRule('aStreetAddressOfPerson5','required');
                $form->addRule('aStateOfPerson5',        'required');
                $form->addRule('aCityCountryOfPerson5',  'required');
                $form->addRule('aZIpCodeOfPerson5',      'required');
                $form->addRule('aZIpCodeOfPerson5',      'zipcode');
            }
        }
    }
    if ($_POST['add9']){
        //---------------------------     9 #4    ------------------------
        $form->addRule('aMonthDateFrom10',              'required');
        $form->addRule('aYearDateFrom10',                'required');
        $form->addRule('aMonthDateTo10',                 'required');
        $form->addRule('aYearDateTo10',                  'required');
        $form->addRule('aYearDateFrom10',                'yy');
        $form->addRule('aYearDateTo10',                  'yy');
        $form->addRule('aNameOfSchool1',                 'required');
        $form->addRule('aCode1',                         'required');
        $form->addRule('aCode1',                         'code1_3');
        $form->addRule('aDegreesDiplomasOther1',         'required');
        $form->addRule('aMonthsYearAwarded1',            'required');
        $form->addRule('aMonthsYearAwarded1',            'mmyy');
        $form->addRule('aStreetAddresspCityOfSchool1',   'required');
        $form->addRule('aStateOfSchool1',                'required');
        $form->addRule('aZIpCodeOfSchool1',              'required');
        $form->addRule('aZIpCodeOfSchool1',              'zipcode');
    
        //---------------------------     9 #5    ------------------------
        if ($_POST['aMonthDateFrom11']               || $_POST['aYearDateFrom11'] || 
            $_POST['aMonthDateTo11']                 || $_POST['aYearDateTo11'] || 
            $_POST['aNameOfSchool2']                 || $_POST['aCode2'] || 
            $_POST['aDegreesDiplomasOther2']         || $_POST['aMonthsYearAwarded2'] || 
            $_POST['aStreetAddresspCityOfSchool2']   || $_POST['aStateOfSchool2'] || 
            $_POST['aZIpCodeOfSchool2']){
            
            $form->addRule('aMonthDateFrom11',              'required');
            $form->addRule('aYearDateFrom11',                'required');
            $form->addRule('aMonthDateTo11',                 'required');
            $form->addRule('aYearDateTo11',                  'required');
            $form->addRule('aYearDateFrom11',                'yy');
            $form->addRule('aYearDateTo11',                  'yy');
            $form->addRule('aNameOfSchool2',                 'required');
            $form->addRule('aCode2',                         'required');
            $form->addRule('aCode2',                         'code1_3');
            $form->addRule('aDegreesDiplomasOther2',         'required');
            $form->addRule('aMonthsYearAwarded2',            'required');
            $form->addRule('aMonthsYearAwarded2',            'mmyy');
            $form->addRule('aStreetAddresspCityOfSchool2',   'required');
            $form->addRule('aStateOfSchool2',                'required');
            $form->addRule('aZIpCodeOfSchool2',              'required');
            $form->addRule('aZIpCodeOfSchool2',              'zipcode');
        }
        
        //---------------------------     9 #6    ------------------------
        if ($_POST['aMonthDateFrom12']               || $_POST['aYearDateFrom12'] || 
            $_POST['aMonthDateTo12']                 || $_POST['aYearDateTo12'] || 
            $_POST['aNameOfSchool3']                 || $_POST['aCode3'] || 
            $_POST['aDegreesDiplomasOther3']         || $_POST['aMonthsYearAwarded3'] || 
            $_POST['aStreetAddresspCityOfSchool3']   || $_POST['aStateOfSchool3'] || 
            $_POST['aZIpCodeOfSchool3']){
            
            $form->addRule('aMonthDateFrom12',              'required');
            $form->addRule('aYearDateFrom12',                'required');
            $form->addRule('aMonthDateTo12',                 'required');
            $form->addRule('aYearDateTo12',                  'required');
            $form->addRule('aYearDateFrom12',                'yy');
            $form->addRule('aYearDateTo10',                  'yy');
            $form->addRule('aNameOfSchool3',                 'required');
            $form->addRule('aCode3',                         'required');
            $form->addRule('aCode3',                         'code1_3');
            $form->addRule('aDegreesDiplomasOther3',         'required');
            $form->addRule('aMonthsYearAwarded3',            'required');
            $form->addRule('aMonthsYearAwarded3',            'mmyy');
            $form->addRule('aStreetAddresspCityOfSchool3',   'required');
            $form->addRule('aStateOfSchool3',                'required');
            $form->addRule('aZIpCodeOfSchool3',              'required');
            $form->addRule('aZIpCodeOfSchool3',              'zipcode');
        }
    }
    if ($_POST['add10']){
        //---------------------------     10 #7    ------------------------
        $form->addRule('aMonthDateFrom13',                               'required');
        $form->addRule('aYearDateFrom13',                                'required');
        $form->addRule('aYearDateFrom13',                                'yy');
        $form->addRule('aCode4',                                         'required');
        $form->addRule('aEmployersVerifierNamesMilitaryDutyLocation1',   'required');
        if (!$_POST['unemp7']){
        if ($_POST['aCode4'] != '7') {
            $form->addRule('aPositionTitlesMilitaryRank',                'required');
        }
        $form->addRule('aEmployersVerifierStreetAddress1',               'required');
        $form->addRule('aEmployersVerifierCityCountry1',                 'required');
        $form->addRule('aEmployersVerifierState1',                       'required');
        $form->addRule('aEmployersVerifierZIPCode1',                     'required');
        $form->addRule('aEmployersVerifierZIPCode1',                     'zipcode');
        $form->addRule('aZIPCodeOfJobLocation1',                         'zipcode');
        $form->addRule('aZIPCodeOfSupervisor1',                          'zipcode');
        $form->addRule('aAreaCodeOfEmployersVerifier1',                  'required');
        $form->addRule('aAreaCodeOfEmployersVerifier1',                  'area');
        $form->addRule('aAreaCodeOfJobLocation1',                        'area');
        $form->addRule('aAreaCodeOfSupervisor1',                         'area');
        $form->addRule('aPhonenOfEmployersVerifier1',                    'required');
        $form->addRule('aPhonenOfEmployersVerifier1',                    'phone');
        $form->addRule('aPhonenOfJobLocation1',                          'phone');
        $form->addRule('aPhonenOfSupervisor1',                           'phone');
        
        //Block #7
        if ($_POST['aMonthDateFrom14']   || $_POST['aYearDateFrom14'] || 
            $_POST['aMonthDateTo14']     || $_POST['aYearDateTo14'] || 
            $_POST['aPositionTitle1']    || $_POST['aSupervisor1']){
            
            $form->addRule('aMonthDateFrom14',  'required');
            $form->addRule('aYearDateFrom14',    'required');
            $form->addRule('aMonthDateTo14',     'required');
            $form->addRule('aYearDateTo14',      'required');
            $form->addRule('aYearDateFrom14',    'yy');
            $form->addRule('aYearDateTo14',      'yy');        
            $form->addRule('aPositionTitle1',    'required');
            $form->addRule('aSupervisor1',       'required');
        } 
    
        if ($_POST['aMonthDateFrom15']   || $_POST['aYearDateFrom15'] || 
            $_POST['aMonthDateTo15']     || $_POST['aYearDateTo15'] || 
            $_POST['aPositionTitle2']    || $_POST['aSupervisor2']){
            
            $form->addRule('aMonthDateFrom15',  'required');
            $form->addRule('aYearDateFrom15',    'required');
            $form->addRule('aMonthDateTo15',     'required');
            $form->addRule('aYearDateTo15',      'required');
            $form->addRule('aYearDateFrom15',    'yy');
            $form->addRule('aYearDateTo15',      'yy');        
            $form->addRule('aPositionTitle2',    'required');
            $form->addRule('aSupervisor2',       'required');
        }
    
        if ($_POST['aMonthDateFrom16']   || $_POST['aYearDateFrom16'] || 
            $_POST['aMonthDateTo16']     || $_POST['aYearDateTo16'] || 
            $_POST['aPositionTitle3']    || $_POST['aSupervisor3']){
            
            $form->addRule('aMonthDateFrom16',  'required');
            $form->addRule('aYearDateFrom16',    'required');
            $form->addRule('aMonthDateTo16',     'required');
            $form->addRule('aYearDateTo16',      'required');
            $form->addRule('aYearDateFrom16',    'yy');
            $form->addRule('aYearDateTo16',      'yy');        
            $form->addRule('aPositionTitle3',    'required');
            $form->addRule('aSupervisor3',       'required');
        }
        }
        
        //---------------------------     10 #8    ------------------------
        if ($_POST['aMonthDateFrom17']                               || $_POST['aYearDateFrom17'] || $_POST['aCode5'] || 
            $_POST['aEmployersVerifierNamesMilitaryDutyLocation2']   || $_POST['aPositionTitlesMilitaryRank2'] || 
            $_POST['aEmployersVerifierStreetAddress2']               || $_POST['aEmployersVerifierCityCountry2'] || 
            $_POST['aEmployersVerifierState2']                       || $_POST['aEmployersVerifierZIPCode2'] || 
            $_POST['aAreaCodeOfEmployersVerifier2']                  || $_POST['aPhonenOfEmployersVerifier2'] || 
            $_POST['aStreetAddressOfJobLocation2']                   || $_POST['aCityCountryOfJobLocation2'] || 
            $_POST['aStateOfJobLocation2']                           || $_POST['aZIPCodeOfJobLocation2'] || 
            $_POST['aAreaCodeOfJobLocation2']                        || $_POST['aPhonenOfJobLocation2'] || 
            $_POST['aSupervisorNamepStreetAddress2']                 || $_POST['aCityCountryOfSupervisor2'] || 
            $_POST['aStateOfSupervisor2']                            || $_POST['aZIPCodeOfSupervisor2'] || 
            $_POST['aAreaCodeOfSupervisor2']                         || $_POST['aPhonenOfSupervisor2'] || 
            $_POST['aYearDateFrom18']                                || $_POST['aMonthDateTo18'] || 
            $_POST['aMonthDateFrom18']                               || $_POST['aYearDateTo18'] || 
            $_POST['aPositionTitle4']                                || $_POST['aSupervisor4'] || 
            $_POST['aYearDateFrom19']                                || $_POST['aMonthDateTo19'] || 
            $_POST['aMonthDateFrom19']                               || $_POST['aYearDateTo19'] || 
            $_POST['aPositionTitle5']                                || $_POST['aSupervisor5'] || 
            $_POST['aYearDateFrom20']                                || $_POST['aMonthDateTo20'] || 
            $_POST['aMonthDateFrom20']                               || $_POST['aYearDateTo20'] || 
            $_POST['aPositionTitle6']                                || $_POST['aSupervisor6'] || $_POST['aMonthDateFrom21']){
            
            $form->addRule('aMonthDateFrom17',                               'required');
            $form->addRule('aYearDateFrom17',                                'required');
            $form->addRule('aYearDateFrom17',                                'yy');
            $form->addRule('aCode5',                                         'required');
            $form->addRule('aEmployersVerifierNamesMilitaryDutyLocation2',   'required');
            if (!$_POST['unemp8']){
            if ($_POST['aCode5'] != '7') {
                $form->addRule('aPositionTitlesMilitaryRank2',               'required');
            }
            $form->addRule('aEmployersVerifierStreetAddress2',               'required');
            $form->addRule('aEmployersVerifierCityCountry2',                 'required');
            $form->addRule('aEmployersVerifierState2',                       'required');
            $form->addRule('aEmployersVerifierZIPCode2',                     'required');
            $form->addRule('aEmployersVerifierZIPCode2',                     'zipcode');
            $form->addRule('aZIPCodeOfJobLocation2',                         'zipcode');
            $form->addRule('aZIPCodeOfSupervisor2',                          'zipcode');
            $form->addRule('aAreaCodeOfEmployersVerifier2',                  'required');
            $form->addRule('aAreaCodeOfEmployersVerifier2',                  'area');
            $form->addRule('aAreaCodeOfJobLocation2',                        'area');
            $form->addRule('aAreaCodeOfSupervisor2',                         'area');
            $form->addRule('aPhonenOfEmployersVerifier2',                    'required');
            $form->addRule('aPhonenOfEmployersVerifier2',                    'phone');
            $form->addRule('aPhonenOfJobLocation2',                          'phone');
            $form->addRule('aPhonenOfSupervisor2',                           'phone');
    
        
            //Block #8
            if ($_POST['aMonthDateFrom18']   || $_POST['aYearDateFrom18'] || 
                $_POST['aMonthDateTo18']     || $_POST['aYearDateTo18'] || 
                $_POST['aPositionTitle4']    || $_POST['aSupervisor4']){
            
                $form->addRule('aMonthDateFrom18',  'required');
                $form->addRule('aYearDateFrom18',    'required');
                $form->addRule('aMonthDateTo18',     'required');
                $form->addRule('aYearDateTo18',      'required');
                $form->addRule('aYearDateFrom18',    'yy');
                $form->addRule('aYearDateTo18',      'yy');        
                $form->addRule('aPositionTitle4',    'required');
                $form->addRule('aSupervisor4',       'required');
            } 
    
            if ($_POST['aMonthDateFrom19']   || $_POST['aYearDateFrom19'] || 
                $_POST['aMonthDateTo19']     || $_POST['aYearDateTo19'] || 
                $_POST['aPositionTitle5']    || $_POST['aSupervisor5']){
            
                $form->addRule('aMonthDateFrom19',  'required');
                $form->addRule('aYearDateFrom19',    'required');
                $form->addRule('aMonthDateTo19',     'required');
                $form->addRule('aYearDateTo19',      'required');
                $form->addRule('aYearDateFrom19',    'yy');
                $form->addRule('aYearDateTo19',      'yy');        
                $form->addRule('aPositionTitle5',    'required');
                $form->addRule('aSupervisor5',       'required');
            }
    
            if ($_POST['aMonthDateFrom20']   || $_POST['aYearDateFrom20'] || 
                $_POST['aMonthDateTo20']     || $_POST['aYearDateTo20'] || 
                $_POST['aPositionTitle6']    || $_POST['aSupervisor6']){
            
                $form->addRule('aMonthDateFrom20',  'required');
                $form->addRule('aYearDateFrom20',    'required');
                $form->addRule('aMonthDateTo20',     'required');
                $form->addRule('aYearDateTo20',      'required');
                $form->addRule('aYearDateFrom20',    'yy');
                $form->addRule('aYearDateTo20',      'yy');        
                $form->addRule('aPositionTitle6',    'required');
                $form->addRule('aSupervisor6',       'required');
            }
            }
        }
         
        //---------------------------     10 #9    ------------------------
        if ($_POST['aMonthDateFrom21']                               || $_POST['aYearDateFrom21'] || $_POST['aCode6'] || 
            $_POST['aEmployersVerifierNamesMilitaryDutyLocation3']   || $_POST['aPositionTitlesMilitaryRank3'] || 
            $_POST['aEmployersVerifierStreetAddress3']               || $_POST['aEmployersVerifierCityCountry3'] || 
            $_POST['aEmployersVerifierState3']                       || $_POST['aEmployersVerifierZIPCode3'] || 
            $_POST['aAreaCodeOfEmployersVerifier3']                  || $_POST['aPhonenOfEmployersVerifier3'] || 
            $_POST['aStreetAddressOfJobLocation3']                   || $_POST['aCityCountryOfJobLocation3'] || 
            $_POST['aStateOfJobLocation3']                           || $_POST['aZIPCodeOfJobLocation3'] || 
            $_POST['aAreaCodeOfJobLocation3']                        || $_POST['aPhonenOfJobLocation3'] || 
            $_POST['aSupervisorNamepStreetAddress3']                 || $_POST['aCityCountryOfSupervisor3'] || 
            $_POST['aStateOfSupervisor3']                            || $_POST['aZIPCodeOfSupervisor3'] || 
            $_POST['aAreaCodeOfSupervisor3']                         || $_POST['aPhonenOfSupervisor3'] || 
            $_POST['aYearDateFrom22']                                || $_POST['aMonthDateTo22'] || 
            $_POST['aMonthDateFrom22']                               || $_POST['aYearDateTo22'] || 
            $_POST['aPositionTitle7']                                || $_POST['aSupervisor7'] || 
            $_POST['aYearDateFrom23']                                || $_POST['aMonthDateTo23'] || 
            $_POST['aMonthDateFrom23']                               || $_POST['aYearDateTo23'] || 
            $_POST['aPositionTitle8']                                || $_POST['aSupervisor8'] || 
            $_POST['aYearDateFrom24']                                || $_POST['aMonthDateTo24'] || 
            $_POST['aMonthDateFrom24']                               || $_POST['aYearDateTo24'] || 
            $_POST['aPositionTitle9']                                || $_POST['aSupervisor9'] || $_POST['aMonthDateFrom25']){
            
            $form->addRule('aMonthDateFrom21',                               'required');
            $form->addRule('aYearDateFrom21',                                'required');
            $form->addRule('aYearDateFrom21',                                'yy');
            $form->addRule('aDateFrom21',                                    'required');
            $form->addRule('aDateFrom21',                                    'mmyy');
            $form->addRule('aCode6',                                         'required');
            $form->addRule('aEmployersVerifierNamesMilitaryDutyLocation3',   'required');
            if (!$_POST['unemp9']){
            if ($_POST['aCode6'] != '7') {
                $form->addRule('aPositionTitlesMilitaryRank3',               'required');
            }
            $form->addRule('aEmployersVerifierStreetAddress3',               'required');
            $form->addRule('aEmployersVerifierCityCountry3',                 'required');
            $form->addRule('aEmployersVerifierState3',                       'required');
            $form->addRule('aEmployersVerifierZIPCode3',                     'required');
            $form->addRule('aEmployersVerifierZIPCode3',                     'zipcode');
            $form->addRule('aZIPCodeOfJobLocation3',                         'zipcode');
            $form->addRule('aZIPCodeOfSupervisor3',                          'zipcode');
            $form->addRule('aAreaCodeOfEmployersVerifier3',                  'required');
            $form->addRule('aAreaCodeOfEmployersVerifier3',                  'area');
            $form->addRule('aAreaCodeOfJobLocation3',                        'area');
            $form->addRule('aAreaCodeOfSupervisor3',                         'area');
            $form->addRule('aPhonenOfEmployersVerifier3',                    'required');
            $form->addRule('aPhonenOfEmployersVerifier3',                    'phone');
            $form->addRule('aPhonenOfJobLocation3',                          'phone');
            $form->addRule('aPhonenOfSupervisor3',                           'phone');
    
        
            //Block #9
            if ($_POST['aMonthDateFrom22']   || $_POST['aYearDateFrom22'] || 
                $_POST['aMonthDateTo22']     || $_POST['aYearDateTo22'] || 
                $_POST['aPositionTitle7']    || $_POST['aSupervisor7']){
            
                $form->addRule('aMonthDateFrom22',  'required');
                $form->addRule('aYearDateFrom22',    'required');
                $form->addRule('aMonthDateTo22',     'required');
                $form->addRule('aYearDateTo22',      'required');
                $form->addRule('aYearDateFrom22',    'yy');
                $form->addRule('aYearDateTo22',      'yy');        
                $form->addRule('aPositionTitle7',    'required');
                $form->addRule('aSupervisor7',       'required');
            } 
    
            if ($_POST['aMonthDateFrom23']   || $_POST['aYearDateFrom23'] || 
                $_POST['aMonthDateTo23']     || $_POST['aYearDateTo23'] || 
                $_POST['aPositionTitle8']    || $_POST['aSupervisor8']){
            
                $form->addRule('aMonthDateFrom23',  'required');
                $form->addRule('aYearDateFrom23',    'required');
                $form->addRule('aMonthDateTo23',     'required');
                $form->addRule('aYearDateTo23',      'required');
                $form->addRule('aYearDateFrom23',    'yy');
                $form->addRule('aYearDateTo23',      'yy');        
                $form->addRule('aPositionTitle8',    'required');
                $form->addRule('aSupervisor8',       'required');
            }
    
            if ($_POST['aMonthDateFrom24']   || $_POST['aYearDateFrom24'] || 
                $_POST['aMonthDateTo24']     || $_POST['aYearDateTo24'] || 
                $_POST['aPositionTitle9']    || $_POST['aSupervisor9']){
            
                $form->addRule('aMonthDateFrom24',  'required');
                $form->addRule('aYearDateFrom24',    'required');
                $form->addRule('aMonthDateTo24',     'required');
                $form->addRule('aYearDateTo24',      'required');
                $form->addRule('aYearDateFrom24',    'yy');
                $form->addRule('aYearDateTo24',      'yy');        
                $form->addRule('aPositionTitle9',    'required');
                $form->addRule('aSupervisor9',       'required');
            }
            }
        }
         
        //---------------------------     10 #10    ------------------------
        if ($_POST['aMonthDateFrom25']                               || $_POST['aYearDateFrom25'] || $_POST['aCode7'] || 
            $_POST['aEmployersVerifierNamesMilitaryDutyLocation4']   || $_POST['aPositionTitlesMilitaryRank4'] || 
            $_POST['aEmployersVerifierStreetAddress4']               || $_POST['aEmployersVerifierCityCountry4'] || 
            $_POST['aEmployersVerifierState4']                       || $_POST['aEmployersVerifierZIPCode4'] || 
            $_POST['aAreaCodeOfEmployersVerifier4']                  || $_POST['aPhonenOfEmployersVerifier4'] || 
            $_POST['aStreetAddressOfJobLocation4']                   || $_POST['aCityCountryOfJobLocation4'] || 
            $_POST['aStateOfJobLocation4']                           || $_POST['aZIPCodeOfJobLocation4'] || 
            $_POST['aAreaCodeOfJobLocation4']                        || $_POST['aPhonenOfJobLocation4'] || 
            $_POST['aSupervisorNamepStreetAddress4']                 || $_POST['aCityCountryOfSupervisor4'] || 
            $_POST['aStateOfSupervisor4']                            || $_POST['aZIPCodeOfSupervisor4'] || 
            $_POST['aAreaCodeOfSupervisor4']                         || $_POST['aPhonenOfSupervisor4'] || 
            $_POST['aYearDateFrom26']                                || $_POST['aMonthDateTo26'] || 
            $_POST['aMonthDateFrom26']                               || $_POST['aYearDateTo26'] || 
            $_POST['aPositionTitle10']                               || $_POST['aSupervisor10'] || 
            $_POST['aYearDateFrom27']                                || $_POST['aMonthDateTo27'] || 
            $_POST['aMonthDateFrom27']                               || $_POST['aYearDateTo27'] || 
            $_POST['aPositionTitle11']                               || $_POST['aSupervisor11'] || 
            $_POST['aYearDateFrom28']                                || $_POST['aMonthDateTo28'] || 
            $_POST['aMonthDateFrom28']                               || $_POST['aYearDateTo28'] || 
            $_POST['aPositionTitle12']                               || $_POST['aSupervisor12']){
            
            $form->addRule('aMonthDateFrom25',                               'required');
            $form->addRule('aYearDateFrom25',                                'required');
            $form->addRule('aYearDateFrom25',                                'yy');
            $form->addRule('aCode7',                                         'required');
            $form->addRule('aEmployersVerifierNamesMilitaryDutyLocation4',   'required');
            if (!$_POST['unemp10']){
            if ($_POST['aCode7'] != '7') {
                $form->addRule('aPositionTitlesMilitaryRank4',               'required');
            }
            $form->addRule('aEmployersVerifierStreetAddress4',               'required');
            $form->addRule('aEmployersVerifierCityCountry4',                 'required');
            $form->addRule('aEmployersVerifierState4',                       'required');
            $form->addRule('aEmployersVerifierZIPCode4',                     'required');
            $form->addRule('aEmployersVerifierZIPCode4',                     'zipcode');
            $form->addRule('aZIPCodeOfJobLocation4',                         'zipcode');
            $form->addRule('aZIPCodeOfSupervisor4',                          'zipcode');
            $form->addRule('aAreaCodeOfEmployersVerifier4',                  'required');
            $form->addRule('aAreaCodeOfEmployersVerifier4',                  'area');
            $form->addRule('aAreaCodeOfJobLocation4',                        'area');
            $form->addRule('aAreaCodeOfSupervisor4',                         'area');
            $form->addRule('aPhonenOfEmployersVerifier4',                    'required');
            $form->addRule('aPhonenOfEmployersVerifier4',                    'phone');
            $form->addRule('aPhonenOfJobLocation4',                          'phone');
            $form->addRule('aPhonenOfSupervisor4',                           'phone');
    
        
            //Block #10
            if ($_POST['aMonthDateFrom26']   || $_POST['aYearDateFrom26'] || 
                $_POST['aMonthDateTo26']     || $_POST['aYearDateTo26'] || 
                $_POST['aPositionTitle10']   || $_POST['aSupervisor10']){
            
                $form->addRule('aMonthDateFrom26',  'required');
                $form->addRule('aYearDateFrom26',    'required');
                $form->addRule('aMonthDateTo26',     'required');
                $form->addRule('aYearDateTo26',      'required');
                $form->addRule('aYearDateFrom26',    'yy');
                $form->addRule('aYearDateTo26',      'yy');        
                $form->addRule('aPositionTitle10',   'required');
                $form->addRule('aSupervisor10',      'required');
            } 
        
            if ($_POST['aMonthDateFrom27']   || $_POST['aYearDateFrom27'] || 
                $_POST['aMonthDateTo27']     || $_POST['aYearDateTo27'] || 
                $_POST['aPositionTitle11']   || $_POST['aSupervisor11']){
            
                $form->addRule('aMonthDateFrom27',  'required');
                $form->addRule('aYearDateFrom27',    'required');
                $form->addRule('aMonthDateTo27',     'required');
                $form->addRule('aYearDateTo27',      'required');
                $form->addRule('aYearDateFrom27',    'yy');
                $form->addRule('aYearDateTo27',      'yy');        
                $form->addRule('aPositionTitle11',   'required');
                $form->addRule('aSupervisor11',      'required');
            }
    
            if ($_POST['aMonthDateFrom28']   || $_POST['aYearDateFrom28'] || 
                $_POST['aMonthDateTo28']     || $_POST['aYearDateTo28'] || 
                $_POST['aPositionTitle12']   || $_POST['aSupervisor12']){
            
                $form->addRule('aMonthDateFrom28',  'required');
                $form->addRule('aYearDateFrom28',    'required');
                $form->addRule('aMonthDateTo28',     'required');
                $form->addRule('aYearDateTo28',      'required');
                $form->addRule('aYearDateFrom28',    'yy');
                $form->addRule('aYearDateTo28',      'yy');        
                $form->addRule('aPositionTitle12',   'required');
                $form->addRule('aSupervisor12',      'required');
            }
            }
        }
    } 
    //---------------------------------------------------------     /SF86A     -------------------------------------------------------
    
    //---------------------------     11 #1    ------------------------
    $form->addRule('NameOfPersonWhoKnowYouWell1',                   'required');
    $form->addRule('MonthDateKnownFrom1',                           'required');
    $form->addRule('YearDateKnownFrom1',                            'required');
    $form->addRule('MonthDateKnownTo1',                             'required');
    $form->addRule('YearDateKnownTo1',                              'required');
    $form->addRule('AreaCodeOfPerson1',                             'required');
    $form->addRule('AreaCodeOfPerson1',                             'area');
    $form->addRule('PhonenOfPerson1',                               'required');
    $form->addRule('HomesWorkAddressOfPersonWhoKnowYouWell1',       'required');
    $form->addRule('CityCountryOfPersonPersonWhoKnowYouWell1',      'required');
    $form->addRule('StateOfPersonWhoKnowYouWell1',                  'required');
    $form->addRule('ZIPCodeOfPersonWhoKnowYouWell1',                'required');
    $form->addRule('time1',                                         'required');

    //---------------------------     11 #2    ------------------------
    $form->addRule('NameOfPersonWhoKnowYouWell2',                   'required');
    $form->addRule('MonthDateKnownFrom2',                           'required');
    $form->addRule('YearDateKnownFrom2',                            'required');
    $form->addRule('MonthDateKnownTo2',                             'required');
    $form->addRule('YearDateKnownTo2',                              'required');
    $form->addRule('AreaCodeOfPerson2',                             'required');
    $form->addRule('AreaCodeOfPerson2',                             'area');
    $form->addRule('PhonenOfPerson2',                               'required');
    $form->addRule('HomesWorkAddressOfPersonWhoKnowYouWell2',       'required');
    $form->addRule('CityCountryOfPersonPersonWhoKnowYouWell2',      'required');
    $form->addRule('StateOfPersonWhoKnowYouWell2',                  'required');
    $form->addRule('ZIPCodeOfPersonWhoKnowYouWell2',                'required');
    $form->addRule('time2',                                         'required');

    //---------------------------     11 #3    ------------------------
    $form->addRule('NameOfPersonWhoKnowYouWell3',                   'required');
    $form->addRule('MonthDateKnownFrom3',                           'required');
    $form->addRule('YearDateKnownFrom3',                            'required');
    $form->addRule('MonthDateKnownTo3',                             'required');
    $form->addRule('YearDateKnownTo3',                              'required');
    $form->addRule('AreaCodeOfPerson3',                             'required');
    $form->addRule('AreaCodeOfPerson3',                             'area');
    $form->addRule('PhonenOfPerson3',                               'required');
    $form->addRule('HomesWorkAddressOfPersonWhoKnowYouWell3',       'required');
    $form->addRule('CityCountryOfPersonPersonWhoKnowYouWell3',      'required');
    $form->addRule('StateOfPersonWhoKnowYouWell3',                  'required');
    $form->addRule('ZIPCodeOfPersonWhoKnowYouWell3',                'required');
    $form->addRule('time3',                                         'required');
 
    //---------------------------     12     ------------------------    
    $dob = explode('/', $_POST['DOB']);
    if (($dob[2])>1959 && $_POST['sex'] == 'male'){
        $form->addRule('sss',                           'required');
        if ($_POST['sss'] == 'Yes'){
            $form->addRule('RegistrationNumber',        'required');
        }   
        if ($_POST['sss'] == 'No'){
            $form->addRule('LegalExemptionExplanation', 'required');
        } 
    }
    //---------------------------     13     ------------------------    
    $form->addRule('military',  'required');
    $form->addRule('marine',    'required');
    
    if ($_POST['military'] == 'Yes' || $_POST['marine'] == 'Yes'){
        $form->addRule('MonthDateFrom37',            'required');
        $form->addRule('YearDateFrom37',            'required');
        $form->addRule('YearDateFrom37',            'yy');
        $form->addRule('MonthDateTo37',              'required');
        $form->addRule('YearDateTo37',              'required');
        $form->addRule('YearDateTo37',              'yy');
        $form->addRule('Code10',                'required');
        $form->addRule('Code10',                'code1_7');
        $form->addRule('ServicesCertificaten1', 'required');
        //$form->addRule('Country1',              'required');
        $form->addRule('rank1',                 'required');
        $form->addRule('status1',               'required');
        
        if ($_POST['DateFrom38']    || $_POST['DateTo38'] || 
            $_POST['Code11']        || $_POST['ServicesCertificaten2'] ||
            $_POST['rank2']         || $_POST['Country2'] ||
            $_POST['status2']){
        
            $form->addRule('MonthDateFrom38',            'required');
            $form->addRule('YearDateFrom38',            'required');
            $form->addRule('YearDateFrom38',            'yy');
            $form->addRule('MonthDateTo38',              'required');
            $form->addRule('YearDateTo38',              'required');
            $form->addRule('YearDateTo38',              'yy');
            $form->addRule('Code11',                'required');
            $form->addRule('Code11',                'code1_7');
            $form->addRule('ServicesCertificaten2', 'required');
            $form->addRule('rank2',                 'required');
            //$form->addRule('Country2',              'required');
            $form->addRule('status2',               'required');
        }
    }            
                
    //---------------------------     14    ------------------------    
    $form->addRule('drugs',  'required');
    
    if ($_POST['drugs'] == 'Yes' ){
        $form->addRule('YearDateFrom39',        'yy');
        $form->addRule('MonthDateFrom39',        'required');
        $form->addRule('YearDateFrom39',        'required');
        $form->addRule('YearDateTo39',          'yy');
        $form->addRule('MonthDateTo39',          'required');
        $form->addRule('YearDateTo39',          'required');
        $form->addRule('TypeOfSubstance1',  'required');
        $form->addRule('Explanation1',      'required');
        
        if ($_POST['DateFrom40']        || $_POST['DateTo40'] || 
            $_POST['TypeOfSubstance2']  || $_POST['Explanation2']){

            $form->addRule('YearDateFrom40',        'required');
            $form->addRule('MonthDateFrom40',        'required');
            $form->addRule('YearDateFrom40',        'yy');
            $form->addRule('YearDateTo40',          'required');
            $form->addRule('MonthDateTo40',          'required');
            $form->addRule('YearDateTo40',          'yy');
            $form->addRule('TypeOfSubstance2',  'required');
            $form->addRule('Explanation2',      'required');
        }
        
        if ($_POST['DateFrom41']        || $_POST['DateTo41'] || 
            $_POST['TypeOfSubstance3']  || $_POST['Explanation3']){

            $form->addRule('YearDateFrom41',        'required');
            $form->addRule('MonthDateFrom41',        'required');
            $form->addRule('YearDateFrom41',        'yy');
            $form->addRule('YearDateTo41',          'required');
            $form->addRule('MonthDateTo41',          'required');
            $form->addRule('YearDateTo41',          'yy');
            $form->addRule('TypeOfSubstance3',  'required');
            $form->addRule('Explanation3',      'required');
        }
    }

    $form->addRule('HomePhone',  'phone');
    
    //---------------------------     of0306    ------------------------                              
    $form->addRule('f9',        'required');
    $form->addRule('daytel',    'phone');
    $form->addRule('dayarea',   'area');
    $form->addRule('nighttel',  'phone');
    $form->addRule('nightarea', 'area');
    $form->addRule('f10',       'required');
    $form->addRule('f11',       'required');
    $form->addRule('f12',       'required');
    $form->addRule('f13',       'required');
    $form->addRule('f14',       'required');
    $form->addRule('f15',       'required');
    if ($_POST['18b']){
        $form->addRule('b18',   'required');
    }
    if ($_POST['10']  == 'Yes' || $_POST['11']  == 'Yes' || 
        $_POST['12']  == 'Yes' || $_POST['13']  == 'Yes' || 
        $_POST['14']  == 'Yes' || $_POST['c18'] == 'Yes' ){
        
        $form->addRule('n16',   'required');
    }
    
    if ($_POST['military'] == 'Yes'){
        $form->addRule('Branch1',       'required');
        $form->addRule('n8from1',       'required');
        $form->addRule('n8from1',       'mmddyyyy');
        $form->addRule('n8to1',         'required');
        $form->addRule('n8to1',         'mmddyyyy');
        $form->addRule('n8discharge1',  'required');
        
        if ($_POST['Branch2']   || $_POST['n8from2'] || 
            $_POST['n8to2']     || $_POST['n8discharge2']){
            
            $form->addRule('Branch2',       'required');
            $form->addRule('n8from2',       'required');
            $form->addRule('n8from2',       'mmddyyyy');
            $form->addRule('n8to2',         'required');
            $form->addRule('n8to2',         'mmddyyyy');
            $form->addRule('n8discharge2',  'required');
        }        
        
        if ($_POST['Branch3']   || $_POST['n8from3'] || 
            $_POST['n8to3']     || $_POST['n8discharge3']){
            
            $form->addRule('Branch3',       'required');
            $form->addRule('n8from3',       'required');
            $form->addRule('n8from3',       'mmddyyyy');
            $form->addRule('n8to3',         'required');
            $form->addRule('n8to3',         'mmddyyyy');
            $form->addRule('n8discharge3',  'required');
        }
    }

    $form->addRule('n18a',    'mmddyyyy');

    if ($form->validate($_POST)){
        $pdf    =& SetaPDF_FormFiller::factory(DOCS_DIR."SF85.pdf", '', 'F', false, true); 
        $fields =& $pdf->getFields();


        //------------------------   1   ---------------------------
        $fields['FName']        ->setValue($_POST['FName']);  
        $fields['LName']        ->setValue($_POST['LName']);  
        $fields['NameSuffix']   ->setValue($_POST['NameSuffix']);  
        if ($_POST['MName']){
            $fields['MName']    ->setValue($_POST['MName']);  
        } else {
            $fields['MName']    ->setValue('NMN'); 
        }
        
        //------------------------   2   ---------------------------
        $fields['CityOfBirth']   ->setValue($_POST['CityOfBirth']);  
        if ($_POST['CountryOfBirth'] == 'United States'){
            $fields['StateOfBirth']  ->setValue($_POST['StateOfBirth']); 
            $fields['CountyOfBirth'] ->setValue($_POST['CountyOfBirth']); 
        } else {
            $fields['CountryOfBirth']->setValue($_POST['CountryOfBirth']);   
        }
        
        //------------------------   3   ---------------------------
        $dob = explode('/', $_POST['DOB']);
        $fields['DOB1']->setValue($dob[0]);  
        $fields['DOB2']->setValue($dob[1]);  
        $fields['DOB3']->setValue($dob[2]);  
        
        //------------------------   4   ---------------------------   
        $fields['SSN'] ->setValue($_POST['SSN']);

        //------------------------   5 #1   ---------------------------   
        $fields['OtherName1']->setValue($_POST['OtherName1']);  
        $fields['DateFrom1'] ->setValue($_POST['MonthDateFrom1'].'/'.$_POST['YearDateFrom1']);  
        $fields['DateTo1']   ->setValue($_POST['MonthDateTo1'].'/'.$_POST['YearDateTo1']);  
        
        //------------------------  5 #2        ---------------------------   
        $fields['OtherName2']->setValue($_POST['OtherName2']);  
        $fields['DateFrom2'] ->setValue($_POST['MonthDateFrom2'].'/'.$_POST['YearDateFrom2']);  
        $fields['DateTo2']   ->setValue($_POST['MonthDateTo2'].'/'.$_POST['YearDateTo2']);  
        
        //------------------------   5 #3   ---------------------------   
        $fields['OtherName3']->setValue($_POST['OtherName3']);  
        $fields['DateFrom3'] ->setValue($_POST['MonthDateFrom3'].'/'.$_POST['YearDateFrom3']);  
        $fields['DateTo3']   ->setValue($_POST['MonthDateTo3'].'/'.$_POST['YearDateTo3']);  
        
        //------------------------   5 #4   ---------------------------   
        $fields['OtherName4']->setValue($_POST['OtherName4']);  
        $fields['DateFrom4'] ->setValue($_POST['MonthDateFrom4'].'/'.$_POST['YearDateFrom4']);  
        $fields['DateTo4']   ->setValue($_POST['MonthDateTo4'].'/'.$_POST['YearDateTo4']);  
        
        //------------------------   6   ---------------------------   
        if ($_POST['sex'] == 'male' ){
                $fields['SexMale']->push();
        }
        if ($_POST['sex'] == 'female' ){
                $fields['SexFemale']->push();
        }
        
        //------------------------   7a   ---------------------------
        if ($_POST['citizen'] == 'USCitizen' ){
                $fields['USCitizen']->push();
        }
        if ($_POST['citizen'] == 'NotAUSCitizen' ){
                $fields['NotAUSCitizen']->push();
        }
        if ($_POST['citizen'] == 'NotBornCitizen' ){
                $fields['NotBornCitizen']->push();
        }
        
        //------------------------   7b   ---------------------------   
        $fields['MothersMaidenName']->setValue($_POST['MothersMaidenName']);
        
        //------------------------   7c   ---------------------------   
        if ($_POST['citizen'] == 'NotBornCitizen'){
            $fields['CourtOfNaturaliztion']         ->setValue($_POST['CourtOfNaturaliztion']);
            $fields['StateOfNaturalization']        ->setValue($_POST['StateOfNaturalization']);
            $fields['CityOfNaturalization']         ->setValue($_POST['CityOfNaturalization']);
            $fields['NaturalizationCertificate#']   ->setValue($_POST['NaturalizationCertificate']);
            $fields['DateOfIssue1']                 ->setValue($_POST['DateOfIssue1']);
        
            //------------------------          ---------------------------   
            $fields['StateOfCitizenshipCertificate']->setValue($_POST['StateOfCitizenshipCertificate']);
            $fields['CityOfCitizenshipCertificate'] ->setValue($_POST['CityOfCitizenshipCertificate']);
            $fields['CitizenshipCertificate#']      ->setValue($_POST['CitizenshipCertificate']);
            $fields['DateOfIssue2']                 ->setValue($_POST['DateOfIssue2']);
        
            //------------------------          ---------------------------   
            $fields['DateOfFormPrepared']   ->setValue($_POST['DateOfFormPrepared']);
            $fields['Explenation1']         ->setValue($_POST['Explenation1']);
        
            //------------------------          ---------------------------   
            $fields['Passport#']    ->setValue($_POST['Passportn']);
            $fields['DateOfIssue3'] ->setValue($_POST['DateOfIssue3']);
        }
        
        //------------------------   7d   ---------------------------   
        if ($_POST['citizen'] == 'USCitizen' || $_POST['citizen'] == 'NotBornCitizen'){
            $fields['CountryOfDualCitizenship']->setValue($_POST['CountryOfDualCitizenship']);
        }

        //------------------------   7e   ---------------------------   
        if ($_POST['citizen'] == 'NotAUSCitizen'){
            $entDate = explode('/', $_POST['DateOfUSEnterance']);
            $fields['CityOfUSEntrance']         ->setValue($_POST['CityOfUSEntrance']);
            $fields['StateOfUSEnterance']       ->setValue($_POST['StateOfUSEnterance']);
            $fields['MonthOfUSEnterance']       ->setValue($entDate[0]);
            $fields['DayOfUSEnterance']         ->setValue($entDate[1]);
            $fields['YearOfUSEnterance']        ->setValue($entDate[2]);
            $fields['AlienRegistrationNumber']  ->setValue($_POST['AlienRegistrationNumber']);
            $fields['CountryOfCitizenship']     ->setValue($_POST['CountryOfCitizenship']);
        }

        //------------------------   8 #1   ---------------------------
        if ($_POST['MonthDateFrom5']){
            $fields['DateFrom5']                ->setValue($_POST['MonthDateFrom5'].'/'.$_POST['YearDateFrom5']);
            $fields['StreetAddress1']           ->setValue($_POST['StreetAddress1']);
            $fields['Apt#1']                    ->setValue($_POST['Aptn1']);
            $fields['City(Country)1']           ->setValue($_POST['CityCountry1']);
            $fields['State1']                   ->setValue($_POST['State1']);
            $fields['ZIpCode1']                 ->setValue($_POST['ZIpCode1']);
            $fields['NameOfPerson1']            ->setValue($_POST['NameOfPerson1']);
            $fields['StreetAddressOfPerson1']   ->setValue($_POST['StreetAddressOfPerson1']);
            $fields['Apt#OfPerson1']            ->setValue($_POST['AptnOfPerson1']);
            $fields['City(Country)OfPerson1']   ->setValue($_POST['CityCountryOfPerson1']);
            $fields['StateOfPerson1']           ->setValue($_POST['StateOfPerson1']);
            $fields['ZIpCodeOfPerson1']         ->setValue($_POST['ZIpCodeOfPerson1']);
        }

        //------------------------   8 #2   ---------------------------
        if ($_POST['MonthDateFrom6']){
            $fields['DateFrom6']                ->setValue($_POST['MonthDateFrom6'].'/'.$_POST['YearDateFrom6']);
            $fields['DateTo6']                  ->setValue($_POST['MonthDateFrom5'].'/'.$_POST['YearDateFrom5']);
            $fields['StreetAddress2']           ->setValue($_POST['StreetAddress2']);
            $fields['Apt#2']                    ->setValue($_POST['Aptn2']);
            $fields['City(Country)2']           ->setValue($_POST['CityCountry2']);
            $fields['State2']                   ->setValue($_POST['State2']);
            $fields['ZIpCode2']                 ->setValue($_POST['ZIpCode2']);
            $fields['NameOfPerson2']            ->setValue($_POST['NameOfPerson2']);
            $fields['StreetAddressOfPerson2']   ->setValue($_POST['StreetAddressOfPerson2']);
            $fields['Apt#OfPerson2']            ->setValue($_POST['AptnOfPerson2']);
            $fields['City(Country)OfPerson2']   ->setValue($_POST['CityCountryOfPerson2']);
            $fields['StateOfPerson2']           ->setValue($_POST['StateOfPerson2']);
            $fields['ZIpCodeOfPerson2']         ->setValue($_POST['ZIpCodeOfPerson2']);
         }

        //------------------------   8 #3   ---------------------------
        if ($_POST['MonthDateFrom7']){
            $fields['DateFrom7']                ->setValue($_POST['MonthDateFrom7'].'/'.$_POST['YearDateFrom7']);
            $fields['DateTo7']                  ->setValue($_POST['MonthDateFrom6'].'/'.$_POST['YearDateFrom6']);
            $fields['StreetAddress3']           ->setValue($_POST['StreetAddress3']);
            $fields['Apt#3']                    ->setValue($_POST['Aptn3']);
            $fields['City(Country)3']           ->setValue($_POST['CityCountry3']);
            $fields['State3']                   ->setValue($_POST['State3']);
            $fields['ZIpCode3']                 ->setValue($_POST['ZIpCode3']);
            $fields['NameOfPerson3']            ->setValue($_POST['NameOfPerson3']);
            $fields['StreetAddressOfPerson3']   ->setValue($_POST['StreetAddressOfPerson3']);
            $fields['Apt#OfPerson3']            ->setValue($_POST['AptnOfPerson3']);
            $fields['City(Country)OfPerson3']   ->setValue($_POST['CityCountryOfPerson3']);
            $fields['StateOfPerson3']           ->setValue($_POST['StateOfPerson3']);
            $fields['ZIpCodeOfPerson3']         ->setValue($_POST['ZIpCodeOfPerson3']);
        }

        //------------------------   8 #4   ---------------------------
        if ($_POST['MonthDateFrom8']){
            $fields['DateFrom8']                ->setValue($_POST['MonthDateFrom8'].'/'.$_POST['YearDateFrom8']);
            $fields['DateTo8']                  ->setValue($_POST['MonthDateFrom7'].'/'.$_POST['YearDateFrom7']);
            $fields['StreetAddress4']           ->setValue($_POST['StreetAddress4']);
            $fields['Apt#4']                    ->setValue($_POST['Aptn4']);
            $fields['City(Country)4']           ->setValue($_POST['CityCountry4']);
            $fields['State4']                   ->setValue($_POST['State4']);
            $fields['ZIpCode4']                 ->setValue($_POST['ZIpCode4']);
            $fields['NameOfPerson4']            ->setValue($_POST['NameOfPerson4']);
            $fields['StreetAddressOfPerson4']   ->setValue($_POST['StreetAddressOfPerson4']);
            $fields['Apt#OfPerson4']            ->setValue($_POST['AptnOfPerson4']);
            $fields['City(Country)OfPerson4']   ->setValue($_POST['CityCountryOfPerson4']);
            $fields['StateOfPerson4']           ->setValue($_POST['StateOfPerson4']);
            $fields['ZIpCodeOfPerson4']         ->setValue($_POST['ZIpCodeOfPerson4']);
        }

        //------------------------   8 #5   ---------------------------
        if ($_POST['MonthDateFrom9']){
            $fields['DateFrom9']                ->setValue($_POST['MonthDateFrom9'].'/'.$_POST['YearDateFrom9']);
            $fields['DateTo9']                  ->setValue($_POST['MonthDateFrom8'].'/'.$_POST['YearDateFrom8']);
            $fields['StreetAddress5']           ->setValue($_POST['StreetAddress5']);
            $fields['Apt#5']                    ->setValue($_POST['Aptn5']);
            $fields['City(Country)5']           ->setValue($_POST['CityCountry5']);
            $fields['State5']                   ->setValue($_POST['State5']);
            $fields['ZIpCode5']                 ->setValue($_POST['ZIpCode5']);
            $fields['NameOfPerson5']            ->setValue($_POST['NameOfPerson5']);
            $fields['StreetAddressOfPerson5']   ->setValue($_POST['StreetAddressOfPerson5']);
            $fields['Apt#OfPerson5']            ->setValue($_POST['AptnOfPerson5']);
            $fields['City(Country)OfPerson5']   ->setValue($_POST['CityCountryOfPerson5']);
            $fields['StateOfPerson5']           ->setValue($_POST['StateOfPerson5']);
            $fields['ZIpCodeOfPerson5']         ->setValue($_POST['ZIpCodeOfPerson5']);
        }

        //------------------------   9 #1   ---------------------------
        $fields['DateFrom10']                   ->setValue($_POST['MonthDateFrom10'].'/'.$_POST['YearDateFrom10']);
        $fields['DateTo10']                     ->setValue($_POST['MonthDateTo10'].'/'.$_POST['YearDateTo10']);
        $fields['NameOfSchool1']                ->setValue($_POST['NameOfSchool1']);
        $fields['Code1']                        ->setValue($_POST['Code1']);
        $fields['Degree/Diploma/Other1']        ->setValue($_POST['DegreesDiplomasOther1']);
        $fields['Month/YearAwarded1']           ->setValue($_POST['MonthsYearAwarded1']);
        $fields['StreetAddress+CityOfSchool1']  ->setValue($_POST['StreetAddresspCityOfSchool1']);
        $fields['StateOfSchool1']               ->setValue($_POST['StateOfSchool1']);
        $fields['ZIpCodeOfSchool1']             ->setValue($_POST['ZIpCodeOfSchool1']);
        
        //------------------------   9 #2   ---------------------------
        $fields['DateFrom11']                   ->setValue($_POST['MonthDateFrom11'].'/'.$_POST['YearDateFrom11']);
        $fields['DateTo11']                     ->setValue($_POST['MonthDateTo11'].'/'.$_POST['YearDateTo11']);
        $fields['NameOfSchool2']                ->setValue($_POST['NameOfSchool2']);
        $fields['Code2']                        ->setValue($_POST['Code2']);
        $fields['Degree/Diploma/Other2']        ->setValue($_POST['DegreesDiplomasOther2']);
        $fields['Month/YearAwarded2']           ->setValue($_POST['MonthsYearAwarded2']);
        $fields['StreetAddress+CityOfSchool2']  ->setValue($_POST['StreetAddresspCityOfSchool2']);
        $fields['StateOfSchool2']               ->setValue($_POST['StateOfSchool2']);
        $fields['ZIpCodeOfSchool2']             ->setValue($_POST['ZIpCodeOfSchool2']);
        
        //------------------------   9 #3   ---------------------------
        $fields['DateFrom12']                   ->setValue($_POST['MonthDateFrom12'].'/'.$_POST['YearDateFrom12']);
        $fields['DateTo12']                     ->setValue($_POST['MonthDateTo12'].'/'.$_POST['YearDateTo12']);
        $fields['NameOfSchool3']                ->setValue($_POST['NameOfSchool3']);
        $fields['Code3']                        ->setValue($_POST['Code3']);
        $fields['Degree/Diploma/Other3']        ->setValue($_POST['DegreesDiplomasOther3']);
        $fields['Month/YearAwarded3']           ->setValue($_POST['MonthsYearAwarded3']);
        $fields['StreetAddress+CityOfSchool3']  ->setValue($_POST['StreetAddresspCityOfSchool3']);
        $fields['StateOfSchool3']               ->setValue($_POST['StateOfSchool3']);
        $fields['ZIpCodeOfSchool3']             ->setValue($_POST['ZIpCodeOfSchool3']);
        
        $fields['SSN1']->setValue($_POST['SSN']);
        
        //------------------------   10 #1   ---------------------------
        if ($_POST['MonthDateFrom13']){ 
            $fields['DateFrom13']                                   ->setValue($_POST['MonthDateFrom13'].'/'.$_POST['YearDateFrom13']);
            //$fields['DateTo13']                                   ->setValue($_POST['MonthDateTo13'].'/'.$_POST['YearDateTo13']);
            $fields['Code4']                                        ->setValue($_POST['Code4']);
            $fields['Employer/VerifierName/MilitaryDutyLocation1']  ->setValue($_POST['EmployersVerifierNamesMilitaryDutyLocation1']);
            if (!$_POST['unemp1']){
            $fields['PositionTitle/MilitaryRank']                   ->setValue($_POST['PositionTitlesMilitaryRank']);
        
            $fields['Employer/VerifierStreetAddress1']  ->setValue($_POST['EmployersVerifierStreetAddress1']);
            $fields['Employer/VerifierCity(Country)1']  ->setValue($_POST['EmployersVerifierCityCountry1']);
            $fields['Employer/VerifierState1']          ->setValue($_POST['EmployersVerifierState1']);
            $fields['Employer/VerifierZIPCode1']        ->setValue($_POST['EmployersVerifierZIPCode1']);
            $fields['AreaCodeOfEmployer/Verifier1']     ->setValue($_POST['AreaCodeOfEmployersVerifier1']);
            $fields['Phone#OfEmployer/Verifier1']       ->setValue($_POST['PhonenOfEmployersVerifier1']);
        
            $fields['StreetAddressOfJobLocation1']  ->setValue($_POST['StreetAddressOfJobLocation1']);
            $fields['City(Country)OfJobLocation1']  ->setValue($_POST['CityCountryOfJobLocation1']);
            $fields['StateOfJobLocation1']          ->setValue($_POST['StateOfJobLocation1']);
            $fields['ZIPCodeOfJobLocation1']        ->setValue($_POST['ZIPCodeOfJobLocation1']);
            $fields['AreaCodeOfJobLocation1']       ->setValue($_POST['AreaCodeOfJobLocation1']);
            $fields['Phone#OfJobLocation1']         ->setValue($_POST['PhonenOfJobLocation1']);
        
            $fields['SupervisorName+StreetAddress1']->setValue($_POST['SupervisorNamepStreetAddress1']);
            $fields['City(Country)OfSupervisor1']   ->setValue($_POST['CityCountryOfSupervisor1']);
            $fields['StateOfSupervisor1']           ->setValue($_POST['StateOfSupervisor1']);
            $fields['ZIPCodeOfSupervisor1']         ->setValue($_POST['ZIPCodeOfSupervisor1']);
            $fields['AreaCodeOfSupervisor1']        ->setValue($_POST['AreaCodeOfSupervisor1']);
            $fields['Phone#OfSupervisor1']          ->setValue($_POST['PhonenOfSupervisor1']);
        
            $fields['DateFrom14']       ->setValue($_POST['MonthDateFrom14'].'/'.$_POST['YearDateFrom14']);
            $fields['DateTo14']         ->setValue($_POST['MonthDateTo14'].'/'.$_POST['YearDateTo14']);
            $fields['PositionTitle1']   ->setValue($_POST['PositionTitle1']);
            $fields['Supervisor1']      ->setValue($_POST['Supervisor1']);
        
            $fields['DateFrom15']       ->setValue($_POST['MonthDateFrom15'].'/'.$_POST['YearDateFrom15']);
            $fields['DateTo15']         ->setValue($_POST['MonthDateTo15'].'/'.$_POST['YearDateTo15']);
            $fields['PositionTitle2']   ->setValue($_POST['PositionTitle2']);
            $fields['Supervisor2']      ->setValue($_POST['Supervisor2']);
            
            $fields['DateFrom16']       ->setValue($_POST['MonthDateFrom16'].'/'.$_POST['YearDateFrom16']);
            $fields['DateTo16']         ->setValue($_POST['MonthDateTo16'].'/'.$_POST['YearDateTo16']);
            $fields['PositionTitle3']   ->setValue($_POST['PositionTitle3']);
            $fields['Supervisor3']      ->setValue($_POST['Supervisor3']);
            }
        }
        
        //------------------------   10 #2   ---------------------------
        if ($_POST['MonthDateFrom17']){ 
            $fields['DateFrom17']                                   ->setValue($_POST['MonthDateFrom17'].'/'.$_POST['YearDateFrom17']);
            $fields['DateTo17']                                     ->setValue($_POST['MonthDateFrom13'].'/'.$_POST['YearDateFrom13']);
            $fields['Code5']                                        ->setValue($_POST['Code5']);
            $fields['Employer/VerifierName/MilitaryDutyLocation2']  ->setValue($_POST['EmployersVerifierNamesMilitaryDutyLocation2']);
            if (!$_POST['unemp2']){
            $fields['PositionTitle/MilitaryRank2']                  ->setValue($_POST['PositionTitlesMilitaryRank2']);
        
            $fields['Employer/VerifierStreetAddress2']  ->setValue($_POST['EmployersVerifierStreetAddress2']);
            $fields['Employer/VerifierCity(Country)2']  ->setValue($_POST['EmployersVerifierCityCountry2']);
            $fields['Employer/VerifierState2']          ->setValue($_POST['EmployersVerifierState2']);
            $fields['Employer/VerifierZIPCode2']        ->setValue($_POST['EmployersVerifierZIPCode2']);
            $fields['AreaCodeOfEmployer/Verifier2']     ->setValue($_POST['AreaCodeOfEmployersVerifier2']);
            $fields['Phone#OfEmployer/Verifier2']       ->setValue($_POST['PhonenOfEmployersVerifier2']);
        
            $fields['StreetAddressOfJobLocation2']  ->setValue($_POST['StreetAddressOfJobLocation2']);
            $fields['City(Country)OfJobLocation2']  ->setValue($_POST['CityCountryOfJobLocation2']);
            $fields['StateOfJobLocation2']          ->setValue($_POST['StateOfJobLocation2']);
            $fields['ZIPCodeOfJobLocation2']        ->setValue($_POST['ZIPCodeOfJobLocation2']);
            $fields['AreaCodeOfJobLocation2']       ->setValue($_POST['AreaCodeOfJobLocation2']);
            $fields['Phone#OfJobLocation2']         ->setValue($_POST['PhonenOfJobLocation2']);
        
            $fields['SupervisorName+StreetAddress2']->setValue($_POST['SupervisorNamepStreetAddress2']);
            $fields['City(Country)OfSupervisor2']   ->setValue($_POST['CityCountryOfSupervisor2']);
            $fields['StateOfSupervisor2']           ->setValue($_POST['StateOfSupervisor2']);
            $fields['ZIPCodeOfSupervisor2']         ->setValue($_POST['ZIPCodeOfSupervisor2']);
            $fields['AreaCodeOfSupervisor2']        ->setValue($_POST['AreaCodeOfSupervisor2']);
            $fields['Phone#OfSupervisor2']          ->setValue($_POST['PhonenOfSupervisor2']);
        
            $fields['DateFrom18']       ->setValue($_POST['MonthDateFrom18'].'/'.$_POST['YearDateFrom18']);
            $fields['DateTo18']         ->setValue($_POST['MonthDateTo18'].'/'.$_POST['YearDateTo18']);
            $fields['PositionTitle4']   ->setValue($_POST['PositionTitle4']);
            $fields['Supervisor4']      ->setValue($_POST['Supervisor4']);
        
            $fields['DateFrom19']       ->setValue($_POST['MonthDateFrom19'].'/'.$_POST['YearDateFrom19']);
            $fields['DateTo19']         ->setValue($_POST['MonthDateTo19'].'/'.$_POST['YearDateTo19']);
            $fields['PositionTitle5']   ->setValue($_POST['PositionTitle5']);
            $fields['Supervisor5']      ->setValue($_POST['Supervisor5']);
        
            $fields['DateFrom20']       ->setValue($_POST['MonthDateFrom20'].'/'.$_POST['YearDateFrom20']);
            $fields['DateTo20']         ->setValue($_POST['MonthDateTo20'].'/'.$_POST['YearDateTo20']);
            $fields['PositionTitle6']   ->setValue($_POST['PositionTitle6']);
            $fields['Supervisor6']      ->setValue($_POST['Supervisor6']);
            }
        }
        
        //------------------------   10 #3   ---------------------------
        if ($_POST['MonthDateFrom21']){ 
            $fields['DateFrom21']                                   ->setValue($_POST['MonthDateFrom21'].'/'.$_POST['YearDateFrom21']);
            $fields['DateTo21']                                     ->setValue($_POST['MonthDateFrom17'].'/'.$_POST['YearDateFrom17']);
            $fields['Code6']                                        ->setValue($_POST['Code6']);
            $fields['Employer/VerifierName/MilitaryDutyLocation3']  ->setValue($_POST['EmployersVerifierNamesMilitaryDutyLocation3']);
            if (!$_POST['unemp3']){
            $fields['PositionTitle/MilitaryRank3']                  ->setValue($_POST['PositionTitlesMilitaryRank3']);
        
            $fields['Employer/VerifierStreetAddress3']  ->setValue($_POST['EmployersVerifierStreetAddress3']);
            $fields['Employer/VerifierCity(Country)3']  ->setValue($_POST['EmployersVerifierCityCountry3']);
            $fields['Employer/VerifierState3']          ->setValue($_POST['EmployersVerifierState3']);
            $fields['Employer/VerifierZIPCode3']        ->setValue($_POST['EmployersVerifierZIPCode3']);
            $fields['AreaCodeOfEmployer/Verifier3']     ->setValue($_POST['AreaCodeOfEmployersVerifier3']);
            $fields['Phone#OfEmployer/Verifier3']       ->setValue($_POST['PhonenOfEmployersVerifier3']);
        
            $fields['StreetAddressOfJobLocation3']  ->setValue($_POST['StreetAddressOfJobLocation3']);
            $fields['City(Country)OfJobLocation3']  ->setValue($_POST['CityCountryOfJobLocation3']);
            $fields['StateOfJobLocation3']          ->setValue($_POST['StateOfJobLocation3']);
            $fields['ZIPCodeOfJobLocation3']        ->setValue($_POST['ZIPCodeOfJobLocation3']);
            $fields['AreaCodeOfJobLocation3']       ->setValue($_POST['AreaCodeOfJobLocation3']);
            $fields['Phone#OfJobLocation3']         ->setValue($_POST['PhonenOfJobLocation3']);
        
            $fields['SupervisorName+StreetAddress3']->setValue($_POST['SupervisorNamepStreetAddress3']);
            $fields['City(Country)OfSupervisor3']   ->setValue($_POST['CityCountryOfSupervisor3']);
            $fields['StateOfSupervisor3']           ->setValue($_POST['StateOfSupervisor3']);
            $fields['ZIPCodeOfSupervisor3']         ->setValue($_POST['ZIPCodeOfSupervisor3']);
            $fields['AreaCodeOfSupervisor3']        ->setValue($_POST['AreaCodeOfSupervisor3']);
            $fields['Phone#OfSupervisor3']          ->setValue($_POST['PhonenOfSupervisor3']);
        
            $fields['DateFrom22']       ->setValue($_POST['MonthDateFrom22'].'/'.$_POST['YearDateFrom22']);
            $fields['DateTo22']         ->setValue($_POST['MonthDateTo22'].'/'.$_POST['YearDateTo22']);
            $fields['PositionTitle7']   ->setValue($_POST['PositionTitle7']);
            $fields['Supervisor7']      ->setValue($_POST['Supervisor7']);
        
            $fields['DateFrom23']       ->setValue($_POST['MonthDateFrom23'].'/'.$_POST['YearDateFrom23']);
            $fields['DateTo23']         ->setValue($_POST['MonthDateTo23'].'/'.$_POST['YearDateTo23']);
            $fields['PositionTitle8']   ->setValue($_POST['PositionTitle8']);
            $fields['Supervisor8']      ->setValue($_POST['Supervisor8']);
        
            $fields['DateFrom24']       ->setValue($_POST['MonthDateFrom24'].'/'.$_POST['YearDateFrom24']);
            $fields['DateTo24']         ->setValue($_POST['MonthDateTo24'].'/'.$_POST['YearDateTo24']);
            $fields['PositionTitle9']   ->setValue($_POST['PositionTitle9']);
            $fields['Supervisor9']      ->setValue($_POST['Supervisor9']);
            }
        }
        
        //------------------------   10 #4   ---------------------------
        if ($_POST['MonthDateFrom25']){ 
            $fields['DateFrom25']                                   ->setValue($_POST['MonthDateFrom25'].'/'.$_POST['YearDateFrom25']);
            $fields['DateTo25']                                     ->setValue($_POST['MonthDateFrom21'].'/'.$_POST['YearDateFrom21']);
            $fields['Code7']                                        ->setValue($_POST['Code7']);
            $fields['Employer/VerifierName/MilitaryDutyLocation4']  ->setValue($_POST['EmployersVerifierNamesMilitaryDutyLocation4']);
            if (!$_POST['unemp4']){
            $fields['PositionTitle/MilitaryRank4']                  ->setValue($_POST['PositionTitlesMilitaryRank4']);
            
            $fields['Employer/VerifierStreetAddress4']  ->setValue($_POST['EmployersVerifierStreetAddress4']);
            $fields['Employer/VerifierCity(Country)4']  ->setValue($_POST['EmployersVerifierCityCountry4']);
            $fields['Employer/VerifierState4']          ->setValue($_POST['EmployersVerifierState4']);
            $fields['Employer/VerifierZIPCode4']        ->setValue($_POST['EmployersVerifierZIPCode4']);
            $fields['AreaCodeOfEmployer/Verifier4']     ->setValue($_POST['AreaCodeOfEmployersVerifier4']);
            $fields['Phone#OfEmployer/Verifier4']       ->setValue($_POST['PhonenOfEmployersVerifier4']);
        
            $fields['StreetAddressOfJobLocation4']  ->setValue($_POST['StreetAddressOfJobLocation4']);
            $fields['City(Country)OfJobLocation4']  ->setValue($_POST['CityCountryOfJobLocation4']);
            $fields['StateOfJobLocation4']          ->setValue($_POST['StateOfJobLocation4']);
            $fields['ZIPCodeOfJobLocation4']        ->setValue($_POST['ZIPCodeOfJobLocation4']);
            $fields['AreaCodeOfJobLocation4']       ->setValue($_POST['AreaCodeOfJobLocation4']);
            $fields['Phone#OfJobLocation4']         ->setValue($_POST['PhonenOfJobLocation4']);
        
            $fields['SupervisorName+StreetAddress4']->setValue($_POST['SupervisorNamepStreetAddress4']);
            $fields['City(Country)OfSupervisor4']   ->setValue($_POST['CityCountryOfSupervisor4']);
            $fields['StateOfSupervisor4']           ->setValue($_POST['StateOfSupervisor4']);
            $fields['ZIPCodeOfSupervisor4']         ->setValue($_POST['ZIPCodeOfSupervisor4']);
            $fields['AreaCodeOfSupervisor4']        ->setValue($_POST['AreaCodeOfSupervisor4']);
            $fields['Phone#OfSupervisor4']          ->setValue($_POST['PhonenOfSupervisor4']);
        
            $fields['DateFrom26']       ->setValue($_POST['MonthDateFrom26'].'/'.$_POST['YearDateFrom26']);
            $fields['DateTo26']         ->setValue($_POST['MonthDateTo26'].'/'.$_POST['YearDateTo26']);
            $fields['PositionTitle10']  ->setValue($_POST['PositionTitle10']);
            $fields['Supervisor10']     ->setValue($_POST['Supervisor10']);
        
            $fields['DateFrom27']       ->setValue($_POST['MonthDateFrom27'].'/'.$_POST['YearDateFrom27']);
            $fields['DateTo27']         ->setValue($_POST['MonthDateTo27'].'/'.$_POST['YearDateTo27']);
            $fields['PositionTitle11']  ->setValue($_POST['PositionTitle11']);
            $fields['Supervisor11']     ->setValue($_POST['Supervisor11']);
        
            $fields['DateFrom28']       ->setValue($_POST['MonthDateFrom28'].'/'.$_POST['YearDateFrom28']);
            $fields['DateTo28']         ->setValue($_POST['MonthDateTo28'].'/'.$_POST['YearDateTo28']);
            $fields['PositionTitle12']  ->setValue($_POST['PositionTitle12']);
            $fields['Supervisor12']     ->setValue($_POST['Supervisor12']);
            }
        }
        
        //------------------------   10 #5   ---------------------------
        if ($_POST['MonthDateFrom29']){ 
            $fields['DateFrom29']                                   ->setValue($_POST['MonthDateFrom29'].'/'.$_POST['YearDateFrom29']);
            $fields['DateTo29']                                     ->setValue($_POST['MonthDateFrom25'].'/'.$_POST['YearDateFrom25']);
            $fields['Code8']                                        ->setValue($_POST['Code8']);
            $fields['Employer/VerifierName/MilitaryDutyLocation5']  ->setValue($_POST['EmployersVerifierNamesMilitaryDutyLocation5']);
            if (!$_POST['unemp5']){
            $fields['PositionTitle/MilitaryRank5']                  ->setValue($_POST['PositionTitlesMilitaryRank5']);
        
            $fields['Employer/VerifierStreetAddress5']  ->setValue($_POST['EmployersVerifierStreetAddress5']);
            $fields['Employer/VerifierCity(Country)5']  ->setValue($_POST['EmployersVerifierCityCountry5']);
            $fields['Employer/VerifierState5']          ->setValue($_POST['EmployersVerifierState5']);
            $fields['Employer/VerifierZIPCode5']        ->setValue($_POST['EmployersVerifierZIPCode5']);
            $fields['AreaCodeOfEmployer/Verifier5']     ->setValue($_POST['AreaCodeOfEmployersVerifier5']);
            $fields['Phone#OfEmployer/Verifier5']       ->setValue($_POST['PhonenOfEmployersVerifier5']);
        
            $fields['StreetAddressOfJobLocation5']  ->setValue($_POST['StreetAddressOfJobLocation5']);
            $fields['City(Country)OfJobLocation5']  ->setValue($_POST['CityCountryOfJobLocation5']);
            $fields['StateOfJobLocation5']          ->setValue($_POST['StateOfJobLocation5']);
            $fields['ZIPCodeOfJobLocation5']        ->setValue($_POST['ZIPCodeOfJobLocation5']);
            $fields['AreaCodeOfJobLocation5']       ->setValue($_POST['AreaCodeOfJobLocation5']);
            $fields['Phone#OfJobLocation5']         ->setValue($_POST['PhonenOfJobLocation5']);
        
            $fields['SupervisorName+StreetAddress5']->setValue($_POST['SupervisorNamepStreetAddress5']);
            $fields['City(Country)OfSupervisor5']   ->setValue($_POST['CityCountryOfSupervisor5']);
            $fields['StateOfSupervisor5']           ->setValue($_POST['StateOfSupervisor5']);
            $fields['ZIPCodeOfSupervisor5']         ->setValue($_POST['ZIPCodeOfSupervisor5']);
            $fields['AreaCodeOfSupervisor5']        ->setValue($_POST['AreaCodeOfSupervisor5']);
            $fields['Phone#OfSupervisor5']          ->setValue($_POST['PhonenOfSupervisor5']);
        
            $fields['DateFrom30']       ->setValue($_POST['MonthDateFrom30'].'/'.$_POST['YearDateFrom30']);
            $fields['DateTo30']         ->setValue($_POST['MonthDateTo30'].'/'.$_POST['YearDateTo30']);
            $fields['PositionTitle13']  ->setValue($_POST['PositionTitle13']);
            $fields['Supervisor13']     ->setValue($_POST['Supervisor13']);
        
            $fields['DateFrom31']       ->setValue($_POST['MonthDateFrom31'].'/'.$_POST['YearDateFrom31']);
            $fields['DateTo31']         ->setValue($_POST['MonthDateTo31'].'/'.$_POST['YearDateTo31']);
            $fields['PositionTitle14']  ->setValue($_POST['PositionTitle14']);
            $fields['Supervisor14']     ->setValue($_POST['Supervisor14']);
        
            $fields['DateFrom32']       ->setValue($_POST['MonthDateFrom32'].'/'.$_POST['YearDateFrom32']);
            $fields['DateTo32']         ->setValue($_POST['MonthDateTo32'].'/'.$_POST['YearDateTo32']);
            $fields['PositionTitle15']  ->setValue($_POST['PositionTitle15']);
            $fields['Supervisor15']     ->setValue($_POST['Supervisor15']);
            }
        }
        
        //------------------------   10 #6   ---------------------------
        if ($_POST['MonthDateFrom33']){ 
            $fields['DateFrom33']                                   ->setValue($_POST['MonthDateFrom33'].'/'.$_POST['YearDateFrom33']);
            $fields['DateTo33']                                     ->setValue($_POST['MonthDateFrom29'].'/'.$_POST['YearDateFrom29']);
            $fields['Code9']                                        ->setValue($_POST['Code9']);
            $fields['Employer/VerifierName/MilitaryDutyLocation6']  ->setValue($_POST['EmployersVerifierNamesMilitaryDutyLocation6']);
            if (!$_POST['unemp6']){
            $fields['PositionTitle/MilitaryRank6']                  ->setValue($_POST['PositionTitlesMilitaryRank6']);
        
            $fields['Employer/VerifierStreetAddress6']  ->setValue($_POST['EmployersVerifierStreetAddress6']);
            $fields['Employer/VerifierCity(Country)6']  ->setValue($_POST['EmployersVerifierCityCountry6']);
            $fields['Employer/VerifierState6']          ->setValue($_POST['EmployersVerifierState6']);
            $fields['Employer/VerifierZIPCode6']        ->setValue($_POST['EmployersVerifierZIPCode6']);
            $fields['AreaCodeOfEmployer/Verifier6']     ->setValue($_POST['AreaCodeOfEmployersVerifier6']);
            $fields['Phone#OfEmployer/Verifier6']       ->setValue($_POST['PhonenOfEmployersVerifier6']);
        
            $fields['StreetAddressOfJobLocation6']  ->setValue($_POST['StreetAddressOfJobLocation6']);
            $fields['City(Country)OfJobLocation6']  ->setValue($_POST['CityCountryOfJobLocation6']);
            $fields['StateOfJobLocation6']          ->setValue($_POST['StateOfJobLocation6']);
            $fields['ZIPCodeOfJobLocation6']        ->setValue($_POST['ZIPCodeOfJobLocation6']);
            $fields['AreaCodeOfJobLocation6']       ->setValue($_POST['AreaCodeOfJobLocation6']);
            $fields['Phone#OfJobLocation6']         ->setValue($_POST['PhonenOfJobLocation6']);
        
            $fields['SupervisorName+StreetAddress6']->setValue($_POST['SupervisorNamepStreetAddress6']);
            $fields['City(Country)OfSupervisor6']   ->setValue($_POST['CityCountryOfSupervisor6']);
            $fields['StateOfSupervisor6']           ->setValue($_POST['StateOfSupervisor6']);
            $fields['ZIPCodeOfSupervisor6']         ->setValue($_POST['ZIPCodeOfSupervisor6']);
            $fields['AreaCodeOfSupervisor6']        ->setValue($_POST['AreaCodeOfSupervisor6']);
            $fields['Phone#OfSupervisor6']          ->setValue($_POST['PhonenOfSupervisor6']);
        
            $fields['DateFrom34']       ->setValue($_POST['MonthDateFrom34'].'/'.$_POST['YearDateFrom34']);
            $fields['DateTo34']         ->setValue($_POST['MonthDateTo34'].'/'.$_POST['YearDateTo34']);
            $fields['PositionTitle16']  ->setValue($_POST['PositionTitle16']);
            $fields['Supervisor16']     ->setValue($_POST['Supervisor16']);
        
            $fields['DateFrom35']       ->setValue($_POST['MonthDateFrom35'].'/'.$_POST['YearDateFrom35']);
            $fields['DateTo35']         ->setValue($_POST['MonthDateTo35'].'/'.$_POST['YearDateTo35']);
            $fields['PositionTitle17']  ->setValue($_POST['PositionTitle17']);
            $fields['Supervisor17']     ->setValue($_POST['Supervisor17']);
        
            $fields['DateFrom36']       ->setValue($_POST['MonthDateFrom36'].'/'.$_POST['YearDateFrom36']);
            $fields['DateTo36']         ->setValue($_POST['MonthDateTo36'].'/'.$_POST['YearDateTo36']);
            $fields['PositionTitle18']  ->setValue($_POST['PositionTitle18']);
            $fields['Supervisor18']     ->setValue($_POST['Supervisor18']);
            }
        }
                
        //------------------------   11 #1   ---------------------------
        $fields['NameOfPersonWhoKnowYouWell1']                  ->setValue($_POST['NameOfPersonWhoKnowYouWell1']);
        $fields['DateKnownFrom1']                               ->setValue($_POST['MonthDateKnownFrom1'].'/'.$_POST['YearDateKnownFrom1']);
        $fields['DateKnownTo1']                                 ->setValue($_POST['MonthDateKnownTo1'].'/'.$_POST['YearDateKnownTo1']);
        $fields['AreaCodeOfPerson1']                            ->setValue($_POST['AreaCodeOfPerson1']);
        $fields['Phone#OfPerson1']                              ->setValue($_POST['PhonenOfPerson1']);
        $fields['Home/WorkAddressOfPersonWhoKnowYouWell1']      ->setValue($_POST['HomesWorkAddressOfPersonWhoKnowYouWell1']);
        $fields['City(Country)OfPersonPersonWhoKnowYouWell1']   ->setValue($_POST['CityCountryOfPersonPersonWhoKnowYouWell1']);
        $fields['StateOfPersonWhoKnowYouWell1']                 ->setValue($_POST['StateOfPersonWhoKnowYouWell1']);
        $fields['ZIPCodeOfPersonWhoKnowYouWell1']               ->setValue($_POST['ZIPCodeOfPersonWhoKnowYouWell1']);
        
        if ($_POST['time1'] == 'NighttimePhone1' ){
                $fields['NighttimePhone1']->push();
        }
        if ($_POST['time1'] == 'DaytimePhone1' ){
                $fields['DaytimePhone1']->push();
        }   
        
        //------------------------   11 #2   ---------------------------
        $fields['NameOfPersonWhoKnowYouWell2']              ->setValue($_POST['NameOfPersonWhoKnowYouWell2']);
        $fields['DateKnownFrom2']                           ->setValue($_POST['MonthDateKnownFrom2'].'/'.$_POST['YearDateKnownFrom2']);
        $fields['DateKnownTo2']                             ->setValue($_POST['MonthDateKnownTo2'].'/'.$_POST['YearDateKnownTo2']);
        $fields['AreaCodeOfPerson2']                        ->setValue($_POST['AreaCodeOfPerson2']);
        $fields['Phone#OfPerson2']                          ->setValue($_POST['PhonenOfPerson2']);
        $fields['Home/WorkAddressOfPersonWhoKnowYouWell2']  ->setValue($_POST['HomesWorkAddressOfPersonWhoKnowYouWell2']);
        $fields['City(Country)OfPersonWhoKnowYouWell2']     ->setValue($_POST['CityCountryOfPersonPersonWhoKnowYouWell2']);
        $fields['State OfPersonPersonWhoKnowYouWell2']      ->setValue($_POST['StateOfPersonWhoKnowYouWell2']);
        $fields['ZIPCodeOfPersonWhoKnowYouWell2']           ->setValue($_POST['ZIPCodeOfPersonWhoKnowYouWell2']);
        
        if ($_POST['time2'] == 'NighttimePhone2' ){
                $fields['NighttimePhone2']->push();
        }
        if ($_POST['time2'] == 'DaytimePhone2' ){
                $fields['DaytimePhone2']->push();
        }   
        
        //------------------------   11 #3   ---------------------------
        $fields['NameOfPersonWhoKnowYouWell3']              ->setValue($_POST['NameOfPersonWhoKnowYouWell3']);
        $fields['DateKnownFrom3']                           ->setValue($_POST['MonthDateKnownFrom3'].'/'.$_POST['YearDateKnownFrom3']);
        $fields['DateKnownTo3']                             ->setValue($_POST['MonthDateKnownTo3'].'/'.$_POST['YearDateKnownTo3']);
        $fields['AreaCodeOfPerson3']                        ->setValue($_POST['AreaCodeOfPerson3']);
        $fields['Phone#OfPerson3']                          ->setValue($_POST['PhonenOfPerson3']);
        $fields['Home/WorkAddressOfPersonWhoKnowYouWell3']  ->setValue($_POST['HomesWorkAddressOfPersonWhoKnowYouWell3']);
        $fields['City(Country)OfPersonWhoKnowYouWell3']     ->setValue($_POST['CityCountryOfPersonPersonWhoKnowYouWell3']);
        $fields['StateOfPersonPersonWhoKnowYouWell3']       ->setValue($_POST['StateOfPersonWhoKnowYouWell3']);
        $fields['ZIPCodeOfPersonWhoKnowYouWell3']           ->setValue($_POST['ZIPCodeOfPersonWhoKnowYouWell3']);
        
        if ($_POST['time3'] == 'NighttimePhone3' ){
                $fields['NighttimePhone3']->push();
        }
        if ($_POST['time3'] == 'DaytimePhone3' ){
                $fields['DaytimePhone3']->push();
        }   
        
        //------------------------   12   ---------------------------
        if (($dob[2].$dob[0].$dob[1])>19591231 && $_POST['sex'] == 'male'){
            $fields['SelectiveServiceYes']->push();        
            if ($_POST['sss'] == 'Yes' ){
                $fields['SelectiveServiceRegisteredYes']->push();
                $fields['RegistrationNumber']           ->setValue($_POST['RegistrationNumber']);
            }
            if ($_POST['sss'] == 'No' ){
                $fields['SelectiveServiceRegisteredNo'] ->push();
                $fields['LegalExemptionExplanation']    ->setValue($_POST['LegalExemptionExplanation']);
            } 
        } else {            
            $fields['SelectiveServiceNo']->push();
        }
                 
        //------------------------   13   ---------------------------
        if ($_POST['military'] == 'Yes' ){
                $fields['USMilitaryYes']->push();
        }
        if ($_POST['military'] == 'No' ){
                $fields['USMilitaryNo']->push();
        }
        
        if ($_POST['marine'] == 'Yes' ){
                $fields['USMerchantMarineYes']->push();
        }
        if ($_POST['marine'] == 'No' ){
                $fields['USMerchantMarineNo']->push();
        }          
        //1 row
        $fields['DateFrom37']           ->setValue($_POST['MonthDateFrom37'].'/'.$_POST['YearDateFrom37']);
        $fields['DateTo37']             ->setValue($_POST['MonthDateTo37'].'/'.$_POST['YearDateTo37']);
        $fields['Code10']               ->setValue($_POST['Code10']);
        $fields['Service/Certificate#1']->setValue($_POST['ServicesCertificaten1']);
        $fields['Country1']             ->setValue($_POST['Country1']);
        
        if ($_POST['rank1'] == 'Officer1' ){
                $fields['Officer1']->push();
        }
        if ($_POST['rank1'] == 'Enlisted1' ){
                $fields['Enlisted1']->push();
        }
        
        if ($_POST['status1'] == 'Active1' ){
                $fields['Active1']->push();
        }
        if ($_POST['status1'] == 'ActiveReserve1' ){
                $fields['ActiveReserve1']->push();
        }          
        if ($_POST['status1'] == 'InactiveReserve1' ){
                $fields['InactiveReserve1']->push();
        }
        if ($_POST['status1'] == 'StatusNationalGuard1' ){
                $fields['StatusNationalGuard1']->setValue($_POST['StatusNationalGuard1']);
        }
        //2 row
        $fields['DateFrom38']           ->setValue($_POST['MonthDateFrom38'].'/'.$_POST['YearDateFrom38']);
        $fields['DateTo38']             ->setValue($_POST['MonthDateTo38'].'/'.$_POST['YearDateTo38']);
        $fields['Code11']               ->setValue($_POST['Code11']);
        $fields['Service/Certificate#2']->setValue($_POST['ServicesCertificaten2']);
        $fields['Country2']             ->setValue($_POST['Country2']);
        
        if ($_POST['rank2'] == 'Officer2' ){
                $fields['Officer2']->push();
        }
        if ($_POST['rank2'] == 'Enlisted2' ){
                $fields['Enlisted2']->push();
        }
        
        if ($_POST['status2'] == 'Active2' ){
                $fields['Active2']->push();
        }
        if ($_POST['status2'] == 'ActiveReserve2' ){
                $fields['ActiveReserve2']->push();
        }          
        if ($_POST['status2'] == 'InactiveReserve2' ){
                $fields['InactiveReserve2']->push();
        }
        if ($_POST['status2'] == 'StatusNationalGuard2' ){
                $fields['StatusNationalGuard2']->setValue($_POST['StatusNationalGuard2']);
        }

        //------------------------   14   ---------------------------
        if ($_POST['drugs'] == 'Yes' ){
                $fields['IllegalDrugsYes']->push();
        }
        if ($_POST['drugs'] == 'No' ){
                $fields['IllegalDrugsNo']->push();
        }   
        
        $fields['DateFrom39']       ->setValue($_POST['MonthDateFrom39'].'/'.$_POST['YearDateFrom39']);
        $fields['DateTo39']         ->setValue($_POST['MonthDateTo39'].'/'.$_POST['YearDateTo39']);
        $fields['TypeOfSubstance1'] ->setValue($_POST['TypeOfSubstance1']);
        $fields['Explanation1']     ->setValue($_POST['Explanation1']);
        
        $fields['DateFrom40']       ->setValue($_POST['MonthDateFrom40'].'/'.$_POST['YearDateFrom40']);
        $fields['DateTo40']         ->setValue($_POST['MonthDateTo40'].'/'.$_POST['YearDateTo40']);
        $fields['TypeOfSubstance2'] ->setValue($_POST['TypeOfSubstance2']);
        $fields['Explanation2']     ->setValue($_POST['Explanation2']);
 
        $fields['DateFrom41']       ->setValue($_POST['MonthDateFrom41'].'/'.$_POST['YearDateFrom41']);
        $fields['DateTo41']         ->setValue($_POST['MonthDateTo41'].'/'.$_POST['YearDateTo41']);
        $fields['TypeOfSubstance3'] ->setValue($_POST['TypeOfSubstance3']);
        $fields['Explanation3']     ->setValue($_POST['Explanation3']);
        
        $fields['ContinuationSpace']->setValue($_POST['ContinuationSpace']);
        
        
        
        //------------------------   Authorization   ---------------------------
        $fields['DateSigned1']      ->setValue(date('m/d/y'));
        $fields['DateSigned2']      ->setValue(date('m/d/y'));
        $fields['FullName']         ->setValue($_POST['FName'].' '.$_POST['MName'].' '.$_POST['LName']);
        $fields['OtherNames']       ->setValue( $_POST['OtherName1'].' '.
                                                $_POST['OtherName2'].' '.
                                                $_POST['OtherName3'].' '.
                                                $_POST['OtherName4'].' '.
                                                $_POST['OtherName5']);
        $fields['SSN2']             ->setValue($_POST['SSN']);
        $fields['CurrentAddress']   ->setValue($_POST['StreetAddress1'].' ('.$_POST['CityCountry1'].')');
        $fields['State']            ->setValue($_POST['State1']);
        $fields['ZIPCode']          ->setValue($_POST['ZIpCode1']);
        $fields['HomePhoneAreaCode']->setValue($_POST['dayarea']);
        $fields['HomePhone#']       ->setValue($_POST['daytel']);
        
        
        foreach ($fields AS &$field){
            if ($field->getValue() == '/'){
                $field->setValue('');
            }
        }

        $pdf->fillForms(SAVE_DIR.session_id().'SF85.pdf');
        $_SESSION['SF85'] = file_get_contents(SAVE_DIR.session_id().'SF85.pdf');
        unlink(SAVE_DIR.session_id().'SF85.pdf');

        $pdf    =& SetaPDF_FormFiller::factory(DOCS_DIR."of0306.pdf", '', 'F', false, true); 
        $fields =& $pdf->getFields();

        $fields['Lastname'] ->setValue($_POST['FName'].' '.$_POST['MName'].' '.$_POST['LName']);
        $fields['ssn']      ->setValue($_POST['SSN']);
        $fields['POB']      ->setValue($_POST['CityOfBirth'].' '.$_POST['StateOfBirth']);
        $fields['DOB']      ->setValue($_POST['DOB']);
        $fields['alais1']   ->setValue($_POST['OtherName1']);
        $fields['alias2']   ->setValue($_POST['OtherName2']);
        $fields['daytel']   ->setValue('('.$_POST['dayarea'].')'.$_POST['daytel']);
        $fields['nitetel']  ->setValue('('.$_POST['nightarea'].')'.$_POST['nighttel']);
    
        $fields['Branch1']      ->setValue($_POST['Branch1']);
        $fields['8from1']       ->setValue($_POST['n8from1']);
        $fields['8to1']         ->setValue($_POST['n8to1']);
        $fields['8discharge1']  ->setValue($_POST['n8discharge1']);
    
    
        $fields['Branch2']      ->setValue($_POST['Branch2']);
        $fields['8from2']       ->setValue($_POST['n8from2']);
        $fields['8to2']         ->setValue($_POST['n8to2']);
        $fields['8discharge2']  ->setValue($_POST['n8discharge2']);
    
  
        $fields['Branch3']      ->setValue($_POST['Branch3']);
        $fields['8from3']       ->setValue($_POST['n8from3']);
        $fields['8to3']         ->setValue($_POST['n8to3']);
        $fields['8discharge3']  ->setValue($_POST['n8discharge3']);
    

        $fields['16']   ->setValue($_POST['n16']);
        $fields['18a']  ->setValue($_POST['n18a']);
    
    
    
    
        //-------------------------    checkboxes buttons    -----------------------
        $a7  = $fields['7a'] ->getButtons();
        $b7  = $fields['7b'] ->getButtons();
        $a8  = $fields['8a'] ->getButtons();
        $f9  = $fields['9']  ->getButtons();
        $f10 = $fields['10'] ->getButtons();
        $f11 = $fields['11'] ->getButtons();
        $f12 = $fields['12'] ->getButtons();
        $f13 = $fields['13'] ->getButtons();
        $f14 = $fields['14'] ->getButtons();
        $f15 = $fields['15'] ->getButtons();
        $b18 = $fields['18b']->getButtons();
        $c18 = $fields['18c']->getButtons();
    
    
        //-------------------------    7a    -----------------------
        if (($dob[2].$dob[0].$dob[1])>19591231 && $_POST['sex'] == 'male'){
            $a7['/Yes']->push();
        } else {
            $a7['/no']->push();
        }

        //-------------------------    7b    -----------------------
        switch ($_POST['sss']){
            case 'SelectiveServiceRegisteredYes':
                $b7['/Yes']->push();
                break;
            case 'SelectiveServiceRegisteredNo':
                $b7['/No']->push();
                break;
        }

        //-------------------------    8a    -----------------------
        switch ($_POST['military']){
            case 'Yes':
                $a8['/Yes']->push();
                break;
            case 'No':
                $a8['/No']->push();
                break;
        }

        //-------------------------    f9    -----------------------
        switch ($_POST['f9']){
            case 'Yes':
                $f9['/Yes']->push();
                break;
            case 'No':
                $f9['/no']->push();
                break;
        }

        //-------------------------    f10    -----------------------
        switch ($_POST['f10']){
            case 'Yes':
                $f10['/Yes']->push();
                break;
            case 'No':
                $f10['/no']->push();
                break;
        }

        //-------------------------    f11    -----------------------
        switch ($_POST['f11']){
            case 'Yes':
                $f11['/Yes']->push();
                break;
            case 'No':
                $f11['/no']->push();
                break;
        }

        //-------------------------    f12    -----------------------
        switch ($_POST['f12']){
            case 'Yes':
                $f12['/Yes']->push();
                break;
            case 'No':
                $f12['/no']->push();
                break;
        }

        //-------------------------    f13    -----------------------
        switch ($_POST['f13']){
            case 'Yes':
                $f13['/Yes']->push();
                break;
            case 'No':
                $f13['/No']->push();
                break;
        }

        //-------------------------    f14    -----------------------
        switch ($_POST['f14']){
            case 'Yes':
                $f14['/Yes']->push();
                break;
            case 'No':
                $f14['/no']->push();
                break;
        }

        //-------------------------    f15    -----------------------
        switch ($_POST['f15']){
            case 'Yes':
                $f15['/Yes']->push();
                break;
            case 'No':
                $f15['/no']->push();
                break;
        }

        //-------------------------    b18    -----------------------
        switch ($_POST['b18']){
            case 'Yes':
                $b18['/Yes']->push();
                break;
            case 'No':
                $b18['/no']->push();
                break;
            case 'DNK':
                $b18['/maybe']->push();
                break;
        }

        //-------------------------    c18    -----------------------
        switch ($_POST['c18']){
            case 'Yes':
                $c18['/Yes']->push();
                break;
            case 'No':
                $c18['/no']->push();
                break;
            case 'DNK':
                $c18['/DNK']->push();
                break;
        }
         
        foreach ($fields AS &$field){
            if ($field->getValue() == '/'){
                $field->setValue('');
            }
        }

       
        $pdf->fillForms(SAVE_DIR.session_id().'of0306.pdf');
        $_SESSION['of0306'] = file_get_contents(SAVE_DIR.session_id().'of0306.pdf');
        unlink(SAVE_DIR.session_id().'of0306.pdf');

        if ($_POST['add8'] || $_POST['add9'] || $_POST['add10']){
            $pdf    =& SetaPDF_FormFiller::factory(DOCS_DIR."SF86A.pdf", '', 'F', false, true); 
            $fields =& $pdf->getFields();

            if ($_POST['add8']){
                //------------------------   8 #6   ---------------------------
                if ($_POST['aMonthDateFrom5']){
                    $fields['DateFrom1']                ->setValue($_POST['aMonthDateFrom5'].'/'.$_POST['aYearDateFrom5']);
                    $fields['DateTo1']                  ->setValue($_POST['MonthDateFrom9'].'/'.$_POST['YearDateFrom9']);
                    $fields['StreetAddress1']           ->setValue($_POST['aStreetAddress1']);
                    $fields['Apt#1']                    ->setValue($_POST['aAptn1']);
                    $fields['City(Country)1']           ->setValue($_POST['aCityCountry1']);
                    $fields['State1']                   ->setValue($_POST['aState1']);
                    $fields['ZIpCode1']                 ->setValue($_POST['aZIpCode1']);
                    $fields['NameOfPerson1']            ->setValue($_POST['aNameOfPerson1']);
                    $fields['StreetAddressOfPerson1']   ->setValue($_POST['aStreetAddressOfPerson1']);
                    $fields['Apt#OfPerson1']            ->setValue($_POST['aAptnOfPerson1']);
                    $fields['City(Country)OfPerson1']   ->setValue($_POST['aCityCountryOfPerson1']);
                    $fields['StateOfPerson1']           ->setValue($_POST['aStateOfPerson1']);
                    $fields['ZIpCodeOfPerson1']         ->setValue($_POST['aZIpCodeOfPerson1']);
                }

                //------------------------   8 #7   ---------------------------
                if ($_POST['aMonthDateFrom6']){
                    $fields['DateFrom2']                ->setValue($_POST['aMonthDateFrom6'].'/'.$_POST['aYearDateFrom6']);
                    $fields['DateTo2']                  ->setValue($_POST['aMonthDateFrom5'].'/'.$_POST['aYearDateFrom5']);
                    $fields['StreetAddress2']           ->setValue($_POST['aStreetAddress2']);
                    $fields['Apt#2']                    ->setValue($_POST['aAptn2']);
                    $fields['City(Country)2']           ->setValue($_POST['aCityCountry2']);
                    $fields['State2']                   ->setValue($_POST['aState2']);
                    $fields['ZIpCode2']                 ->setValue($_POST['aZIpCode2']);
                    $fields['NameOfPerson2']            ->setValue($_POST['aNameOfPerson2']);
                    $fields['StreetAddressOfPerson2']   ->setValue($_POST['aStreetAddressOfPerson2']);
                    $fields['Apt#OfPerson2']            ->setValue($_POST['aAptnOfPerson2']);
                    $fields['City(Country)OfPerson2']   ->setValue($_POST['aCityCountryOfPerson2']);
                    $fields['StateOfPerson2']           ->setValue($_POST['aStateOfPerson2']);
                    $fields['ZIpCodeOfPerson2']         ->setValue($_POST['aZIpCodeOfPerson2']);
                }
    
                //------------------------   8 #8   ---------------------------
                if ($_POST['aMonthDateFrom7']){
                    $fields['DateFrom3']                ->setValue($_POST['aMonthDateFrom7'].'/'.$_POST['aYearDateFrom7']);
                    $fields['DateTo3']                  ->setValue($_POST['aMonthDateFrom6'].'/'.$_POST['aYearDateFrom6']);
                    $fields['StreetAddress3']           ->setValue($_POST['aStreetAddress3']);
                    $fields['Apt#3']                    ->setValue($_POST['aAptn3']);
                    $fields['City(Country)3']           ->setValue($_POST['aCityCountry3']);
                    $fields['State3']                   ->setValue($_POST['aState3']);
                    $fields['ZIpCode3']                 ->setValue($_POST['aZIpCode3']);
                    $fields['NameOfPerson3']            ->setValue($_POST['aNameOfPerson3']);
                    $fields['StreetAddressOfPerson3']   ->setValue($_POST['aStreetAddressOfPerson3']);
                    $fields['Apt#OfPerson3']            ->setValue($_POST['aAptnOfPerson3']);
                    $fields['City(Country)OfPerson3']   ->setValue($_POST['aCityCountryOfPerson3']);
                    $fields['StateOfPerson3']           ->setValue($_POST['aStateOfPerson3']);
                    $fields['ZIpCodeOfPerson3']         ->setValue($_POST['aZIpCodeOfPerson3']);
                }

                //------------------------   8 #9   ---------------------------
                if ($_POST['aMonthDateFrom8']){
                    $fields['DateFrom4']                ->setValue($_POST['aMonthDateFrom8'].'/'.$_POST['aYearDateFrom8']);
                    $fields['DateTo4']                  ->setValue($_POST['aMonthDateFrom7'].'/'.$_POST['aYearDateFrom7']);
                    $fields['StreetAddress4']           ->setValue($_POST['aStreetAddress4']);
                    $fields['Apt#4']                    ->setValue($_POST['aAptn4']);
                    $fields['City(Country)4']           ->setValue($_POST['aCityCountry4']);
                    $fields['State4']                   ->setValue($_POST['aState4']);
                    $fields['ZIpCode4']                 ->setValue($_POST['aZIpCode4']);
                    $fields['NameOfPerson4']            ->setValue($_POST['aNameOfPerson4']);
                    $fields['StreetAddressOfPerson4']   ->setValue($_POST['aStreetAddressOfPerson4']);
                    $fields['Apt#OfPerson4']            ->setValue($_POST['aAptnOfPerson4']);
                    $fields['City(Country)OfPerson4']   ->setValue($_POST['aCityCountryOfPerson4']);
                    $fields['StateOfPerson4']           ->setValue($_POST['aStateOfPerson4']);
                    $fields['ZIpCodeOfPerson4']         ->setValue($_POST['aZIpCodeOfPerson4']);
                }

                //------------------------   8 #10  ---------------------------
                if ($_POST['aMonthDateFrom5']){
                    $fields['DateFrom5']                ->setValue($_POST['aMonthDateFrom9'].'/'.$_POST['aYearDateFrom9']);
                    $fields['DateTo5']                  ->setValue($_POST['aMonthDateFrom8'].'/'.$_POST['aYearDateFrom8']);
                    $fields['StreetAddress5']           ->setValue($_POST['aStreetAddress5']);
                    $fields['Apt#5']                    ->setValue($_POST['aAptn5']);
                    $fields['City(Country)5']           ->setValue($_POST['aCity(Country)5']);
                    $fields['State5']                   ->setValue($_POST['aState5']);
                    $fields['ZIpCode5']                 ->setValue($_POST['aZIpCode5']);
                    $fields['NameOfPerson5']            ->setValue($_POST['aNameOfPerson5']);
                    $fields['StreetAddressOfPerson5']   ->setValue($_POST['aStreetAddressOfPerson5']);
                    $fields['Apt#OfPerson5']            ->setValue($_POST['aAptnOfPerson5']);
                    $fields['City(Country)OfPerson5']   ->setValue($_POST['aCityCountryOfPerson5']);
                    $fields['StateOfPerson5']           ->setValue($_POST['aStateOfPerson5']);
                    $fields['ZIpCodeOfPerson5']         ->setValue($_POST['aZIpCodeOfPerson5']);
                }
            }

            if ($_POST['add9']){
                //------------------------   9 #4   ---------------------------
                $fields['DateFrom6']                    ->setValue($_POST['aMonthDateFrom10'].'/'.$_POST['aYearDateFrom10']);
                $fields['DateTo6']                      ->setValue($_POST['aMonthDateTo10'].'/'.$_POST['aYearDateTo10']);
                $fields['NameOfSchool1']                ->setValue($_POST['aNameOfSchool1']);
                $fields['Code1']                        ->setValue($_POST['aCode1']);
                $fields['Degree/Diploma/Other1']        ->setValue($_POST['aDegreesDiplomasOther1']);
                $fields['Month/YearAwarded1']           ->setValue($_POST['aMonthsYearAwarded1']);
                $fields['StreetAddress+CityOfSchool1']  ->setValue($_POST['aStreetAddresspCityOfSchool1']);
                $fields['StateOfSchool1']               ->setValue($_POST['aStateOfSchool1']);
                $fields['ZIpCodeOfSchool1']             ->setValue($_POST['aZIpCodeOfSchool1']);
                
                //------------------------   9 #5   ---------------------------
                $fields['DateFrom7']                    ->setValue($_POST['aMonthDateFrom11'].'/'.$_POST['aYearDateFrom11']);
                $fields['DateTo7']                      ->setValue($_POST['aMonthDateTo11'].'/'.$_POST['aYearDateTo11']);
                $fields['NameOfSchool2']                ->setValue($_POST['aNameOfSchool2']);
                $fields['Code2']                        ->setValue($_POST['aCode2']);
                $fields['Degree/Diploma/Other2']        ->setValue($_POST['aDegreesDiplomasOther2']);
                $fields['Month/YearAwarded2']           ->setValue($_POST['aMonthsYearAwarded2']);
                $fields['StreetAddress+CityOfSchool2']  ->setValue($_POST['aStreetAddresspCityOfSchool2']);
                $fields['StateOfSchool2']               ->setValue($_POST['aStateOfSchool2']);
                $fields['ZIpCodeOfSchool2']             ->setValue($_POST['aZIpCodeOfSchool2']);
                
                //------------------------   9 #6   ---------------------------
                $fields['DateFrom8']                    ->setValue($_POST['aMonthDateFrom12'].'/'.$_POST['aYearDateFrom12']);
                $fields['DateTo8']                      ->setValue($_POST['aMonthDateTo12'].'/'.$_POST['aYearDateTo12']);
                $fields['NameOfSchool3']                ->setValue($_POST['aNameOfSchool3']);
                $fields['Code3']                        ->setValue($_POST['aCode3']);
                $fields['Degree/Diploma/Other3']        ->setValue($_POST['aDegreesDiplomasOther3']);
                $fields['Month/YearAwarded3']           ->setValue($_POST['aMonthsYearAwarded3']);
                $fields['StreetAddress+CityOfSchool3']  ->setValue($_POST['aStreetAddresspCityOfSchool3']);
                $fields['StateOfSchool3']               ->setValue($_POST['aStateOfSchool3']);
                $fields['ZIpCodeOfSchool3']             ->setValue($_POST['aZIpCodeOfSchool3']);
            }
        
            if ($_POST['add10']){
        
                //------------------------   10 #1   ---------------------------
                $fields['DateFrom9']                                    ->setValue($_POST['aMonthDateFrom13'].'/'.$_POST['aYearDateFrom13']);
                $fields['DateTo9']                                      ->setValue($_POST['MonthDateFrom33'].'/'.$_POST['YearDateFrom33']);
                $fields['Code4']                                        ->setValue($_POST['aCode4']);
                $fields['Employer/VerifierName/MilitaryDutyLocation1']  ->setValue($_POST['aEmployersVerifierNamesMilitaryDutyLocation1']);
                if (!$_POST['unemp7']){
                $fields['PositionTitle/MilitaryRank']                   ->setValue($_POST['aPositionTitlesMilitaryRank']);
                
                $fields['Employer/VerifierStreetAddress1']  ->setValue($_POST['aEmployersVerifierStreetAddress1']);
                $fields['Employer/VerifierCity(Country)1']  ->setValue($_POST['aEmployersVerifierCityCountry1']);
                $fields['Employer/VerifierState1']          ->setValue($_POST['aEmployersVerifierState1']);
                $fields['Employer/VerifierZIPCode1']        ->setValue($_POST['aEmployersVerifierZIPCode1']);
                $fields['AreaCodeOfEmployer/Verifier1']     ->setValue($_POST['aAreaCodeOfEmployersVerifier1']);
                $fields['Phone#OfEmployer/Verifier1']       ->setValue($_POST['aPhonenOfEmployersVerifier1']);
                
                $fields['StreetAddressOfJobLocation1']  ->setValue($_POST['aStreetAddressOfJobLocation1']);
                $fields['City(Country)OfJobLocation1']  ->setValue($_POST['aCityCountryOfJobLocation1']);
                $fields['StateOfJobLocation1']          ->setValue($_POST['aStateOfJobLocation1']);
                $fields['ZIPCodeOfJobLocation1']        ->setValue($_POST['aZIPCodeOfJobLocation1']);
                $fields['AreaCodeOfJobLocation1']       ->setValue($_POST['aAreaCodeOfJobLocation1']);
                $fields['Phone#OfJobLocation1']         ->setValue($_POST['aPhonenOfJobLocation1']);
                
                $fields['SupervisorName+StreetAddress1']->setValue($_POST['aSupervisorNamepStreetAddress1']);
                $fields['City(Country)OfSupervisor1']   ->setValue($_POST['aCityCountryOfSupervisor1']);
                $fields['StateOfSupervisor1']           ->setValue($_POST['aStateOfSupervisor1']);
                $fields['ZIPCodeOfSupervisor1']         ->setValue($_POST['aZIPCodeOfSupervisor1']);
                $fields['AreaCodeOfSupervisor1']        ->setValue($_POST['aAreaCodeOfSupervisor1']);
                $fields['Phone#OfSupervisor1']          ->setValue($_POST['aPhonenOfSupervisor1']);
                
                $fields['DateFrom10']       ->setValue($_POST['aMonthDateFrom14'].'/'.$_POST['aYearDateFrom14']);
                $fields['DateTo10']         ->setValue($_POST['aMonthDateTo14'].'/'.$_POST['aYearDateTo14']);
                $fields['PositionTitle1']   ->setValue($_POST['aPositionTitle1']);
                $fields['Supervisor1']      ->setValue($_POST['aSupervisor1']);
                
                $fields['DateFrom11']       ->setValue($_POST['aMonthDateFrom15'].'/'.$_POST['aYearDateFrom15']);
                $fields['DateTo11']         ->setValue($_POST['aMonthDateTo15'].'/'.$_POST['aYearDateTo15']);
                $fields['PositionTitle2']   ->setValue($_POST['aPositionTitle2']);
                $fields['Supervisor2']      ->setValue($_POST['aSupervisor2']);
                
                $fields['DateFrom12']       ->setValue($_POST['aMonthDateFrom16'].'/'.$_POST['aYearDateFrom16']);
                $fields['DateTo12']         ->setValue($_POST['aMonthDateTo16'].'/'.$_POST['aYearDateTo16']);
                $fields['PositionTitle3']   ->setValue($_POST['aPositionTitle3']);
                $fields['Supervisor3']      ->setValue($_POST['aSupervisor3']);
                }
                
                //------------------------   10 #2   ---------------------------
                if ($_POST['aMonthDateFrom17']){
                    $fields['DateFrom13']                                   ->setValue($_POST['aMonthDateFrom17'].'/'.$_POST['aYearDateFrom17']);
                    $fields['DateTo13']                                     ->setValue($_POST['aMonthDateFrom13'].'/'.$_POST['aYearDateFrom13']);
                    $fields['Code5']                                        ->setValue($_POST['aCode5']);
                    $fields['Employer/VerifierName/MilitaryDutyLocation2']  ->setValue($_POST['aEmployersVerifierNamesMilitaryDutyLocation2']);
                    if (!$_POST['unemp8']){
                    $fields['PositionTitle/MilitaryRank2']                  ->setValue($_POST['aPositionTitlesMilitaryRank2']);
                
                    $fields['Employer/VerifierStreetAddress2']  ->setValue($_POST['aEmployersVerifierStreetAddress2']);
                    $fields['Employer/VerifierCity(Country)2']  ->setValue($_POST['aEmployersVerifierCityCountry2']);
                    $fields['Employer/VerifierState2']          ->setValue($_POST['aEmployersVerifierState2']);
                    $fields['Employer/VerifierZIPCode2']        ->setValue($_POST['aEmployersVerifierZIPCode2']);
                    $fields['AreaCodeOfEmployer/Verifier2']     ->setValue($_POST['aAreaCodeOfEmployersVerifier2']);
                    $fields['Phone#OfEmployer/Verifier2']       ->setValue($_POST['aPhonenOfEmployersVerifier2']);
                
                    $fields['StreetAddressOfJobLocation2']  ->setValue($_POST['aStreetAddressOfJobLocation2']);
                    $fields['City(Country)OfJobLocation2']  ->setValue($_POST['aCityCountryOfJobLocation2']);
                    $fields['StateOfJobLocation2']          ->setValue($_POST['aStateOfJobLocation2']);
                    $fields['ZIPCodeOfJobLocation2']        ->setValue($_POST['aZIPCodeOfJobLocation2']);
                    $fields['AreaCodeOfJobLocation2']       ->setValue($_POST['aAreaCodeOfJobLocation2']);
                    $fields['Phone#OfJobLocation2']         ->setValue($_POST['aPhonenOfJobLocation2']);
                
                    $fields['SupervisorName+StreetAddress2']->setValue($_POST['aSupervisorNamepStreetAddress2']);
                    $fields['City(Country)OfSupervisor2']   ->setValue($_POST['aCityCountryOfSupervisor2']);
                    $fields['StateOfSupervisor2']           ->setValue($_POST['aStateOfSupervisor2']);
                    $fields['ZIPCodeOfSupervisor2']         ->setValue($_POST['aZIPCodeOfSupervisor2']);
                    $fields['AreaCodeOfSupervisor2']        ->setValue($_POST['aAreaCodeOfSupervisor2']);
                    $fields['Phone#OfSupervisor2']          ->setValue($_POST['aPhonenOfSupervisor2']);
                
                    $fields['DateFrom14']       ->setValue($_POST['aMonthDateFrom18'].'/'.$_POST['aYearDateFrom18']);
                    $fields['DateTo14']         ->setValue($_POST['aMonthDateTo18'].'/'.$_POST['aYearDateTo18']);
                    $fields['PositionTitle4']   ->setValue($_POST['aPositionTitle4']);
                    $fields['Supervisor4']      ->setValue($_POST['aSupervisor4']);
                
                    $fields['DateFrom15']       ->setValue($_POST['aMonthDateFrom19'].'/'.$_POST['aYearDateFrom19']);
                    $fields['DateTo15']         ->setValue($_POST['aMonthDateTo19'].'/'.$_POST['aYearDateTo19']);
                    $fields['PositionTitle5']   ->setValue($_POST['aPositionTitle5']);
                    $fields['Supervisor5']      ->setValue($_POST['aSupervisor5']);
                
                    $fields['DateFrom16']       ->setValue($_POST['aMonthDateFrom20'].'/'.$_POST['aYearDateFrom20']);
                    $fields['DateTo16']         ->setValue($_POST['aMonthDateTo20'].'/'.$_POST['aYearDateTo20']);
                    $fields['PositionTitle6']   ->setValue($_POST['aPositionTitle6']);
                    $fields['Supervisor6']      ->setValue($_POST['aSupervisor6']);
                    }
                }

                //------------------------   10 #3   ---------------------------
                if ($_POST['aMonthDateFrom21']){
                    $fields['DateFrom17']                                   ->setValue($_POST['aMonthDateFrom21'].'/'.$_POST['aYearDateFrom21']);
                    $fields['DateTo17']                                     ->setValue($_POST['aMonthDateFrom17'].'/'.$_POST['aYearDateFrom17']);
                    $fields['Code6']                                        ->setValue($_POST['aCode6']);
                    $fields['Employer/VerifierName/MilitaryDutyLocation3']  ->setValue($_POST['aEmployersVerifierNamesMilitaryDutyLocation3']);
                    if (!$_POST['unemp9']){
                    $fields['PositionTitle/MilitaryRank3']                  ->setValue($_POST['aPositionTitlesMilitaryRank3']);
                
                    $fields['Employer/VerifierStreetAddress3']  ->setValue($_POST['aEmployersVerifierStreetAddress3']);
                    $fields['Employer/VerifierCity(Country)3']  ->setValue($_POST['aEmployersVerifierCityCountry3']);
                    $fields['Employer/VerifierState3']          ->setValue($_POST['aEmployersVerifierState3']);
                    $fields['Employer/VerifierZIPCode3']        ->setValue($_POST['aEmployersVerifierZIPCode3']);
                    $fields['AreaCodeOfEmployer/Verifier3']     ->setValue($_POST['aAreaCodeOfEmployersVerifier3']);
                    $fields['Phone#OfEmployer/Verifier3']       ->setValue($_POST['aPhonenOfEmployersVerifier3']);
                
                    $fields['StreetAddressOfJobLocation3']  ->setValue($_POST['aStreetAddressOfJobLocation3']);
                    $fields['City(Country)OfJobLocation3']  ->setValue($_POST['aCityCountryOfJobLocation3']);
                    $fields['StateOfJobLocation3']          ->setValue($_POST['aStateOfJobLocation3']);
                    $fields['ZIPCodeOfJobLocation3']        ->setValue($_POST['aZIPCodeOfJobLocation3']);
                    $fields['AreaCodeOfJobLocation3']       ->setValue($_POST['aAreaCodeOfJobLocation3']);
                    $fields['Phone#OfJobLocation3']         ->setValue($_POST['aPhonenOfJobLocation3']);
                
                    $fields['SupervisorName+StreetAddress3']->setValue($_POST['aSupervisorNamepStreetAddress3']);
                    $fields['City(Country)OfSupervisor3']   ->setValue($_POST['aCityCountryOfSupervisor3']);
                    $fields['StateOfSupervisor3']           ->setValue($_POST['aStateOfSupervisor3']);
                    $fields['ZIPCodeOfSupervisor3']         ->setValue($_POST['aZIPCodeOfSupervisor3']);
                    $fields['AreaCodeOfSupervisor3']        ->setValue($_POST['aAreaCodeOfSupervisor3']);
                    $fields['Phone#OfSupervisor3']          ->setValue($_POST['aPhonenOfSupervisor3']);
                
                    $fields['DateFrom18']       ->setValue($_POST['aMonthDateFrom22'].'/'.$_POST['aYearDateFrom22']);
                    $fields['DateTo18']         ->setValue($_POST['aMonthDateTo22'].'/'.$_POST['aYearDateTo22']);
                    $fields['PositionTitle7']   ->setValue($_POST['aPositionTitle7']);
                    $fields['Supervisor7']      ->setValue($_POST['aSupervisor7']);
                
                    $fields['DateFrom19']       ->setValue($_POST['aMonthDateFrom23'].'/'.$_POST['aYearDateFrom23']);
                    $fields['DateTo19']         ->setValue($_POST['aMonthDateTo23'].'/'.$_POST['aYearDateTo23']);
                    $fields['PositionTitle8']   ->setValue($_POST['aPositionTitle8']);
                    $fields['Supervisor8']      ->setValue($_POST['aSupervisor8']);
                    
                    $fields['DateFrom20']       ->setValue($_POST['aMonthDateFrom24'].'/'.$_POST['aYearDateFrom24']);
                    $fields['DateTo20']         ->setValue($_POST['aMonthDateTo24'].'/'.$_POST['aYearDateTo24']);
                    $fields['PositionTitle9']   ->setValue($_POST['aPositionTitle9']);
                    $fields['Supervisor9']      ->setValue($_POST['aSupervisor9']);
                    }
                }

                //------------------------   10 #4   ---------------------------
                if ($_POST['aMonthDateFrom25']){
                    $fields['DateFrom21']                                   ->setValue($_POST['aMonthDateFrom25'].'/'.$_POST['aYearDateFrom25']);
                    $fields['DateTo21']                                     ->setValue($_POST['aMonthDateFrom21'].'/'.$_POST['aYearDateFrom21']);
                    $fields['Code7']                                        ->setValue($_POST['aCode7']);
                    $fields['Employer/VerifierName/MilitaryDutyLocation4']  ->setValue($_POST['aEmployersVerifierNamesMilitaryDutyLocation4']);
                    if (!$_POST['unemp10']){
                    $fields['PositionTitle/MilitaryRank4']                  ->setValue($_POST['aPositionTitlesMilitaryRank4']);
                
                    $fields['Employer/VerifierStreetAddress4']  ->setValue($_POST['aEmployersVerifierStreetAddress4']);
                    $fields['Employer/VerifierCity(Country)4']  ->setValue($_POST['aEmployersVerifierCityCountry4']);
                    $fields['Employer/VerifierState4']          ->setValue($_POST['aEmployersVerifierState4']);
                    $fields['Employer/VerifierZIPCode4']        ->setValue($_POST['aEmployersVerifierZIPCode4']);
                    $fields['AreaCodeOfEmployer/Verifier4']     ->setValue($_POST['aAreaCodeOfEmployersVerifier4']);
                    $fields['Phone#OfEmployer/Verifier4']       ->setValue($_POST['aPhonenOfEmployersVerifier4']);
                
                    $fields['StreetAddressOfJobLocation4']  ->setValue($_POST['aStreetAddressOfJobLocation4']);
                    $fields['City(Country)OfJobLocation4']  ->setValue($_POST['aCityCountryOfJobLocation4']);
                    $fields['StateOfJobLocation4']          ->setValue($_POST['aStateOfJobLocation4']);
                    $fields['ZIPCodeOfJobLocation4']        ->setValue($_POST['aZIPCodeOfJobLocation4']);
                    $fields['AreaCodeOfJobLocation4']       ->setValue($_POST['aAreaCodeOfJobLocation4']);
                    $fields['Phone#OfJobLocation4']         ->setValue($_POST['aPhonenOfJobLocation4']);
                
                    $fields['SupervisorName+StreetAddress4']->setValue($_POST['aSupervisorNamepStreetAddress4']);
                    $fields['City(Country)OfSupervisor4']   ->setValue($_POST['aCityCountryOfSupervisor4']);
                    $fields['StateOfSupervisor4']           ->setValue($_POST['aStateOfSupervisor4']);
                    $fields['ZIPCodeOfSupervisor4']         ->setValue($_POST['aZIPCodeOfSupervisor4']);
                    $fields['AreaCodeOfSupervisor4']        ->setValue($_POST['aAreaCodeOfSupervisor4']);
                    $fields['Phone#OfSupervisor4']          ->setValue($_POST['aPhonenOfSupervisor4']);
                
                    $fields['DateFrom22']       ->setValue($_POST['aMonthDateFrom26'].'/'.$_POST['aYearDateFrom26']);
                    $fields['DateTo22']         ->setValue($_POST['aMonthDateTo26'].'/'.$_POST['aYearDateTo26']);
                    $fields['PositionTitle10']  ->setValue($_POST['aPositionTitle10']);
                    $fields['Supervisor10']     ->setValue($_POST['aSupervisor10']);
                
                    $fields['DateFrom23']       ->setValue($_POST['aMonthDateFrom27'].'/'.$_POST['aYearDateFrom27']);
                    $fields['DateTo23']         ->setValue($_POST['aMonthDateTo27'].'/'.$_POST['aYearDateTo27']);
                    $fields['PositionTitle11']  ->setValue($_POST['aPositionTitle11']);
                    $fields['Supervisor11']     ->setValue($_POST['aSupervisor11']);
                
                    $fields['DateFrom24']       ->setValue($_POST['aMonthDateFrom28'].'/'.$_POST['aYearDateFrom28']);
                    $fields['DateTo24']         ->setValue($_POST['aMonthDateTo28'].'/'.$_POST['aYearDateTo28']);
                    $fields['PositionTitle12']  ->setValue($_POST['aPositionTitle12']);
                    $fields['Supervisor12']     ->setValue($_POST['aSupervisor12']);
                    }
                }
            }
       
        
            $fields['SSN']  ->setValue($_POST['SSN']);
            $fields['SSN#1']->setValue($_POST['SSN']);
            $fields['Name1']->setValue($_POST['FName'].' '.$_POST['MName'].' '.$_POST['LName']);
            //$fields['']->setValue($_POST['']);
        
            foreach ($fields AS &$field){
                if ($field->getValue() == '/'){
                    $field->setValue('');
                }
            }


            $pdf->fillForms(SAVE_DIR.session_id().'SF86A.pdf');
            $_SESSION['SF86A'] = file_get_contents(SAVE_DIR.session_id().'SF86A.pdf');
            unlink(SAVE_DIR.session_id().'SF86A.pdf');

        }
        $showPDF = true;
    } else {
        $form->addCustomErrorMessage('You have made '.(count($form->getErrorMessages())-1).' error(s) in form');
    }

}
$keys = array_keys($form->getErrorMessages());
/*echo '<pre>';
print_r($form->getErrorMessages());
print_r($keys);
echo '</pre>';*/
$template = new Smarty();
$template->assign('form',           $form);
$template->assign('errorMessages',  $form->getErrorMessages());
$template->assign('showPDF',        $showPDF);
$template->assign('error',          $keys[0]);
$template->assign('errorsCount',    (count($keys)));
$template->display('index.tpl');

