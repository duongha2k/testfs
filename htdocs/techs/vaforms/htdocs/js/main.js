function showadd(item){
    document.getElementById('a'+item).style.display = "";
    document.getElementById('a'+item+'show').style.display = "none";
    document.getElementById('add'+item).value = "1";
}
function hideadd(item){
    document.getElementById('a'+item).style.display = "none";
    document.getElementById('a'+item+'show').style.display = "";
    document.getElementById('add'+item).value = "0";
}
function showCitizen(){
    //var form1   = document.getElementById('form2');
    //var element = form1.citizen;
    //element.value = 'USCitizen';
    /*for(i=0; i<form1.length; i++) {
        if (form1.elements[i].type == 'radio' && i<100){
            alert(form1.elements[i].value+'|'+form1.elements[i].name+'|');
        }
    }*/
    //alert(element.value);
    if (document.getElementById('citizen0').checked){
        document.getElementById('citizenc').style.display = "none";
        document.getElementById('citizene').style.display = "none";
        document.getElementById('citizend').style.display = "";
        document.getElementById('citizenb').style.display = "";
    }    
    if (document.getElementById('citizen1').checked){
        document.getElementById('citizenc').style.display = "";
        document.getElementById('citizene').style.display = "none";
        document.getElementById('citizend').style.display = "";
        document.getElementById('citizenb').style.display = "";
    }    
    if (document.getElementById('citizen2').checked){
        document.getElementById('citizenc').style.display = "none";
        document.getElementById('citizene').style.display = "";
        document.getElementById('citizend').style.display = "none";
        document.getElementById('citizenb').style.display = "";
    }
    
}
function fillTo(value1, dest){
    var a = '';

    if (value1.charAt(0) == 'a'){
        value1 = value1.substring(1);
        a = 'a';
    }
    if (document.getElementById(a+'Month'+value1).value && document.getElementById(a+'Year'+value1).value && document.getElementById(a+'Year'+value1).value != 'YY'){
        document.getElementById(dest).innerHTML =  document.getElementById(a+'Month'+value1).value+'/'+document.getElementById(a+'Year'+value1).value;
    }

}
function selService(){
    if (document.getElementById('DOB').value){
        var dob = document.getElementById('DOB').value.split('/');
        if (dob[2] && (dob[2].toString()+dob[0].toString()+dob[1].toString())>19591231 && document.getElementById('sex0').checked){
          //alert('ura');  
          document.getElementById('selService').style.display = '';
        } else {
          document.getElementById('selService').style.display = 'none';
          document.getElementById('selServiceLabel').innerHTML = '';

        }  
    } else {
        document.getElementById('selService').style.display = 'none';
    }
}

function selServiceYesNo(){
    if (document.getElementById('sss0').checked){
    document.getElementById('RegistrationNumberblock').style.display = '';
    document.getElementById('RegistrationNumberblock').style.display = '';
        document.getElementById('LegalExemptionExplanationblock').style.display = 'none';
    }
    if (document.getElementById('sss1').checked){
    document.getElementById('LegalExemptionExplanationblock').style.display = '';
    document.getElementById('LegalExemptionExplanationblock').style.display = '';
        document.getElementById('RegistrationNumberblock').style.display = 'none';
    }
    //alert('Test onclick');
}

function militaryPeriods(){
    if (document.getElementById('military0').checked || document.getElementById('marine0').checked){
        document.getElementById('militaryPeriods1').style.display = '';
        document.getElementById('militaryPeriods2').style.display = '';
        document.getElementById('militaryPeriods3').style.display = '';
    } else {
        document.getElementById('militaryPeriods1').style.display = 'none';
        document.getElementById('militaryPeriods2').style.display = 'none';
        document.getElementById('militaryPeriods3').style.display = 'none';
    }
}
function drugsPeriods(){
    if (document.getElementById('drugs0').checked ){
        document.getElementById('drugsPeriods').style.display = '';
    } else {
        document.getElementById('drugsPeriods').style.display = 'none';
    }
}

function unemployment(item){
    if (document.getElementById('unemp'+item).checked ){
        document.getElementById('empblock'+item).style.display = 'none';
        document.getElementById('positionEmp'+item).style.display = 'none';
    } else {
        document.getElementById('empblock'+item).style.display = '';
        document.getElementById('positionEmp'+item).style.display = '';
    }
}

function changeC18(){
    if (document.getElementById('b180').checked ){
        document.getElementById('c18').style.display = '';
    } else {
        document.getElementById('c18').style.display = 'none';
    }
}

function focusError(item, count) {
    if (document.getElementById(item)){
        document.getElementById(item).focus();
        if (count>1){
            alert(count+' required fields have either missing or invalid data. Please correct and re-generate.');
        } else {
            alert('1 required field has either missing or invalid data. Please correct and re-generate.');
        }
    }
}

function pob(){
    if (document.getElementById('CountryOfBirth').value == 'United States'){
        document.getElementById('pobst').style.display = '';
    } else {
        document.getElementById('pobst').style.display = 'none';
    }
}

function clearfield(obj){
    if (obj.value=='YY' || obj.value=='MM/YY'){
        obj.value='';
    }
}

