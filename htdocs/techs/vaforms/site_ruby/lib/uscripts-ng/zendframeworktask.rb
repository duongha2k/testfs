require 'uscripts-ng/migrationstask'
require 'uscripts-ng/zendframework'

namespace :db do
  task :environment do
    db_xml = { }
    db_xml = dbinfo_from_cfg_db_xml( './core/_configs/cfg.db.xml')

    $DB ||= { }
    # do not override environment variables.
    db_xml.each_pair do |k,v|
      $DB[k] ||= v
    end
  end
end

