require 'xml/libxml'

module UScriptsNG

  # Returns a hash with database information getting from config
  # cfg.db.xml
  #
  # Example:
  #
  # <?xml version="1.0" encoding="windows-1251"?>
  # <config>
  #     <database>
  #         <use>true</use>
  #         <type>PDO_MYSQL</type>
  #         <host>localhost</host>
  #         <username>root</username>
  #         <password>pass</password>
  #         <name>database</name>
  #     </database>
  # </config>    
  def dbinfo_from_cfg_db_xml( cfg_db_xml_filename)
    results = {}
    cfg_db_xml = XML::Document.file( cfg_db_xml_filename)
    results['HOST'] = cfg_db_xml.find('//config/database/host').first.content
    results['NAME'] = cfg_db_xml.find('//config/database/name').first.content
    results['USER'] = cfg_db_xml.find('//config/database/username').first.content
    results['PASS'] = cfg_db_xml.find('//config/database/password').first.content
    results
  end

end
