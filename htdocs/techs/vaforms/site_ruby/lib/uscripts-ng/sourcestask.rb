require 'uscripts-ng/sources'
require 'uscripts-ng/warecorp-services'
include UScriptsNG

require 'find'

namespace :files do
  desc "Fix permissions on all files. You can specify directories with " +
    "APACHE_WRITABLE_DIRS=\"x,y,z\", where x, y, z are Ruby-style shell globs."
  task :fix_permissions => :environment do
    $APACHE_WRITABLE_DIRS.each do |dir_patt|
      Dir.glob(dir_patt).each do |d|
        sh "chmod 777 #{d}"
      end
    end
  end

  desc "Upgrade files from svn. Without VERSION=x execute svn up. " +
    "You can also set WITHOUT_BUGZILLA=1 to disable adding version to Warecorp's Bugzilla."
  task :upgrade => :environment do
    workcopy = SVNWorkcopy.new( Dir.pwd)
    version = ENV['VERSION']
    workcopy.version = version
    puts workcopy.last_command_output
    bugzilla_add_version( $BUGZILLA_PRODUCT, version) if version && ENV['WITHOUT_BUGZILLA'].nil?
  end

  task :environment do
    if ENV['APACHE_WRITABLE_DIRS']
      $APACHE_WRITABLE_DIRS = ENV['APACHE_WRITABLE_DIRS'].split( ',')
      $APACHE_WRITABLE_DIRS.each do |dir_patt|
        dir_patt.strip!
        dir_patt.gsub!( /^"|"$/, '')
      end
    end
    
    if ENV['BUGZILLA_PRODUCT']
      $BUGZILLA_PRODUCT = ENV['BUGZILLA_PRODUCT']
    end
  end
end


