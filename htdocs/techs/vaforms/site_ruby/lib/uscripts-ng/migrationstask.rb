require 'uscripts-ng/migrations'
include UScriptsNG

require 'rake'

# This function have to be used in Rakefile. It generates typical
# tasks for database. +dbname+ is a name of database, also you can
# specify optional parameter :migrations_path which will be used for
# finding migration's files.

def generate_dbtasks_for( dbname, options = { })
  # we need downcased name for it
  dbname.downcase!
  dbenv = dbname.upcase
  dbmigrations = options[:migrations_dir] || "#{dbname}/migrate"

  common_help = "
    # Options:
    #   * #{dbenv}_VERSION=x - specific version of migration;
    #   * #{dbenv}_HOST=x    - database server hostname;
    #   * #{dbenv}_NAME=x    - database name;
    #   * #{dbenv}_USER=x    - username which will be used to connect;
    #   * #{dbenv}_PASS=x    - password for DB's connect."

  namespace dbname do
    desc "Migrate the database through scripts in #{dbmigrations}." + common_help
    task :migrate => :environment do
      eval "db = Database.new( '#{dbmigrations}', $#{dbenv}['NAME'], $#{dbenv}['USER'], $#{dbenv}['PASS'], $#{dbenv}['HOST'])"
      eval "db.migrate( ENV['#{dbenv}_VERSION'])"
    end

    desc "Drop the database." + common_help
    task :drop => :environment do
      eval "when_writing( \"Droping database \" +  ( $#{dbenv}['NAME'] || '')) do \
              if $#{dbenv}['NAME'] then \
                sh #{dbname}_mysqladmin( $#{dbenv}) + \" drop \" + $#{dbenv}['NAME'] \
              else \
                raise 'Cannot detect database name' \
              end; \
            end;"
    end

    desc "Create the database." + common_help
    task :create => :environment do
      raise "Dry-run impossible for this task" if nowrite
      eval "if $#{dbenv}['NAME'] then \
              sh #{dbname}_mysqladmin( $#{dbenv}) + \" create \" + $#{dbenv}['NAME'] \
            else \
              raise 'Cannot detect database name' \
            end;"
    end

    task :environment do
      eval "$#{dbenv} = {                     \
              'HOST' => ENV['#{dbenv}_HOST'], \
              'NAME' => ENV['#{dbenv}_NAME'], \
              'USER' => ENV['#{dbenv}_USER'], \
              'PASS' => ENV['#{dbenv}_PASS']  \
            }"
      eval "def #{dbname}_mysqladmin( dbinfo)                     \
              mysqladmin = 'mysqladmin --force ';                 \
              if $#{dbenv}['USER'] then                           \
                mysqladmin += ' --user=' + $#{dbenv}['USER']      \
              end;                                                \
              if $#{dbenv}['PASS'] then                           \
                mysqladmin += ' --password=' + $#{dbenv}['PASS']  \
              end;                                                \
              if $#{dbenv}['HOST'] then                           \
                mysqladmin += ' --host=' + $#{dbenv}['HOST']      \
              end;                                                \
              mysqladmin;                                         \
            end"

      # Return mysql console command as string.
      eval "def #{dbname}_mysql( options = { })                 \
              if options[:mysqldump] then                       \
                mysql_str = 'mysqldump ';                       \
              else                                              \
                mysql_str = 'mysql ';                           \
              end;                                              \
              if $#{dbenv}['USER'] then                         \
                mysql_str += ' --user=' + $#{dbenv}['USER']     \
              end;                                              \
              if $#{dbenv}['PASS'] then                         \
                mysql_str += ' --password=' + $#{dbenv}['PASS'] \
              end;                                              \
              if $#{dbenv}['HOST'] then                         \
                mysql_str += ' --host=' + $#{dbenv}['HOST']     \
              end;                                              \
              if options[:with_db_name] then                    \
                mysql_str += ' ' + $#{dbenv}['NAME']            \
              end;
              mysql_str;
            end"
    end
  end
end

generate_dbtasks_for( "db") unless defined? UScriptsNG::WITHOUT_DEFAULT_DB

