module UScriptsNG
  module Utils

    # Executes command with appending "&>/dev/null"
    def system_silence( command)
      system( command + " &>/dev/null")
    end

  end
end
