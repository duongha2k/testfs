require 'mysql'
require 'rake'

module UScriptsNG

  # Represents separate migration. 
  class Migration

    # filename of the current migration
    attr_reader :filename

    # version of the migration
    attr_reader :version

    # filename must be a full path to a migration file.
    def initialize( filename)
      unless filename =~ /\d\d\d_.*\.(sql(\.(gz|bz2))?|php)/
        raise ArgumentError, "Wrong filename for initialize migration."
      end
        
      @filename = filename
      @version = Integer( File.basename( filename).match(/\d\d\d/)[0].gsub(/^0+/,''))
    end

    # used for sorting migrations in db:migrate process.
    def <=>(other)
      @version <=> other.version
    end

    # for sorting facilities.
    def to_i
      version
    end
  end

  # This class represents a MySQL database for separate web
  # application. It can be used mainly for migrate database scheme to
  # the new version.
  class Database

    # migrations_path is the *path*, not glob pattern (e.g. db/migrate).
    def initialize( migrations_path, database, username = "root", password = "", hostname = "localhost")
      @migrations_path = migrations_path
      @dbh = Mysql.new( hostname, username, password, database)
      @mysql_client = "mysql --host=#{hostname} --user=#{username} --password=#{password} #{database}"
    end

    # Returns current schema version.
    def schema_version
      st = @dbh.prepare "SELECT `db_version` FROM `schema_info`;"
      st.execute
      Integer(( st.fetch)[0])
    end

    # Do migration to specific version. This method maintenance
    # version table. If +version+ parameter is ommited migrate to the
    # last available version.
    def migrate( version = nil)

      # if this is initial execution of migrate, we should create
      # table for storing version's information
      begin
        self.schema_version
      rescue
        create_schema_table
      end

      migrations = []
      Dir.glob( @migrations_path + "/*.{sql,sql.gz,sql.bz2,php}") do |file|
        migrations << Migration.new( file)
      end
      migrations.sort!

      desirable_version = version ? version : migrations.max { |a,b| a.version <=> b.version }
      migrations.find_all {|m| m.version > schema_version && m.version <= desirable_version.to_i } .each do |m|

        if m.filename =~ /.php$/
          if sh( "php -f #{m.filename}")
            self.schema_version = m.version
          else
            abort
          end
        else
          cat = 'cat'
          case m.filename
          when /.bz2$/i
            cat = 'bzcat'
          when /.gz/i
            cat = 'zcat'
          end
          
          if sh "#{cat} #{m.filename} | " + @mysql_client
            self.schema_version = m.version
          else
            abort
          end
        end
      end
    end

    private

    # Used for maintaining schema_info table.
    def schema_version=( version)
      st = @dbh.prepare "UPDATE `schema_info` SET `db_version` = ?;"
      st.execute( version)
    end

    # Creates a schema_info table in the clean database (first
    # migration for example).
    def create_schema_table
      @dbh.query <<-"EOF"
            CREATE TABLE IF NOT EXISTS `schema_info` (
              `db_version` int(11) default NULL
            ) ENGINE=MyISAM DEFAULT CHARSET=latin1;
          EOF
      # Insert zero value in version table.
      @dbh.query "INSERT INTO `schema_info` (`db_version`) VALUES(0);"
    end
  end
end


