desc "Check UScriptsNG deploy."
task :check_deployed do

  require 'test/unit'
  require 's4t-utils'
  include S4tUtils
  include S4tUtils::TestUtil

  require 'fileutils'
  include FileUtils

  require 'uscripts-ng/utils'
  include UScriptsNG::Utils

  class CheckDeploymentTest < Test::Unit::TestCase # :nodoc:
    def setup
    end

    def teardown
    end

    def test_check_rake_resultcode
      assert( system_silence( "rake -T"), "Failure in executing rake -T.")
    end
  end

end
