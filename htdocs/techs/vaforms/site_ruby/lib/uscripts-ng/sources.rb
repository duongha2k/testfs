require 'fileutils'
require 's4t-utils'
require 'rake'

require 'uscripts-ng/utils'

module UScriptsNG

  # Class represents a workcopy of a web application svn repository.
  class SVNWorkcopy
    include FileUtils
    include S4tUtils
    include UScriptsNG::Utils

    # output of the last executed svn command.
    attr_reader :last_command_output

    # +wk_dir+ is a path to a root of a workcopy.
    def initialize( wk_dir)
      unless FileTest.directory?( wk_dir)
        raise ArgumentError, "#{wk_dir} is not a directory" 
      end
      
      unless system_silence( "(cd #{wk_dir}; svn info)")
        raise ArgumentError, "#{wk_dir} is not a working copy" 
      end

      @workcopy = wk_dir
    end

    # Returns a root of workcopy.
    def root
      get_svn_param( 'Repository Root')
    end

    # Returns an url of workcopy.
    def url
      get_svn_param( 'URL')
    end

    # Returns current version of workcopy.
    def version
      url.gsub( /.*\/tags\/(.*)/, '\1')
    end

    # Up(Down)grade current workcopy to version +v+.
    def version=( v)
      chdir( @workcopy) do 
        @last_command_output = if v
                                 `svn switch #{root}/tags/#{v}`
                               else
                                 `svn up`
                               end
      end
    end

    # Returns string array of urls that contains externals
    def get_objects_with_externals

      return `svn status| grep "external item"`.split("\n").each do |s|
      s.gsub!("Performing status on external item at ","").gsub!("'","").sub!(/[^\/]*$/){$1}
      end

    end

    # Returns array of external strings from object's svn:externals property
    def get_externals( obj_path)
      return `svn propget svn:externals #{obj_path}`.split("\n")

    end
    
    # Set svn:externals property to object
    def set_externals( obj_path, ext_prop)
      property = ""
       ext_prop.each do |s|
        property+= s + "\n"
        end
      `svn propset svn:externals "#{property}" "#{obj_path}"`
    end

    # Add new property string to object's svn:externals
    def add_external( obj_path, new_prop)
      property = ""
      ext_prop=get_externals(obj_path)
      ext_prop.each do |s|
        property+= s + "\n"
      end
      property+= new_prop + "\n"
    `svn propset svn:externals "#{property}" "#{obj_path}"`
    end
      

    private

    def get_svn_param( param)
      chdir( @workcopy) do
        svn_info = `svn info`.split( /\n/)
        return svn_info.find { |s| s =~ /^#{param}:/ }.gsub( /^#{param}: (.*)/, '\1')
      end
    end
  end

  
end

