require 'test/unit'
require 'pathname'

module UScriptsNG
  module TestUtils
    # Executes block with capturing stdout and stdin.
    def capturing_output
      old_stderr = $stderr
      new_stderr = StringIO.new
      old_stdout = $stdout
      new_stdout = StringIO.new
      begin
        $stderr = new_stderr
        $stdout = new_stdout
        yield
      ensure
        $stderr = old_stderr
        $stdout = old_stdout
      end
      [ new_stdout.string, new_stderr.string ]
    end

    def test_repository( repos, filename = "")
      path = File.join( PACKAGE_ROOT, 'data', 'repositories', repos)
        File.join( path, filename) if filename
      Pathname.new( path).realpath
    end

    class Test::Unit::TestCase
      def assert_rake( targets)
        rake = "rake " + targets
        assert( system_silence( rake), "Failure in execution #{rake}.")
      end
    end
  end
end
