require 'mechanize'

module UScriptsNG
  # Adds version to the specific product in Warecorp's Bugzilla.
  def bugzilla_add_version( product, version)
    agent = WWW::Mechanize.new
    page = agent.get( 'https://warecorp.com/bugzilla')

    login_form = page.form( 'login')
    login_form.set_fields( :Bugzilla_login         => 'robot@warecorp.com',
                           :Bugzilla_password      => 'robot$WC!')
    page = login_form.submit

    page = agent.click page.links.text( 'Products')
    page = agent.click page.links.text( /#{product}/)
    page = agent.click page.links.text( /.*versions.*/)
    page = agent.click page.links.text( 'Add')

    new_version_form = page.forms.action( 'editversions.cgi')
    new_version_form.fields.name( 'version').value=( version)
    new_version_form.submit
  end
end

