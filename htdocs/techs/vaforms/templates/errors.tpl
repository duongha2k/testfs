{if  $errorsCount>0}
	
<div class="error_mes">
    {if $errorsCount>1}  
        {$errorsCount} required fields have either missing or invalid data. Please correct and re-generate.<br>
    {else}
        1 required field has either missing or invalid data. Please correct and re-generate.    
    {/if}
</div>
{/if}

