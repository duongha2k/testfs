<head>
  <title>Varforms</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="css/main.css" />
</head>

{literal}
<script language="JavaScript" src="js/calendar/calendar_us.js"></script>
<script language="JavaScript" src="js/main.js"></script>


<link rel="stylesheet" href="js/calendar/calendar.css">
{/literal}
<body><div id="wrapper">
{include file="errors.tpl"}

{if $showPDF}
    <div class="doc_pdf">
		<h4>Your PDF forms have been generated</h4> 
        <p>
            
            To download and save the forms on your computer right-click on the links below and choose "Save Target As..." (Internet Explorer) or "Save Link As..." (FireFox).        
        </p>
		<p>
			<a href="getPDF.php?file=SF85">SF85.pdf</a><br /> Standart form 85
		</p>
		<p>
			<a href="getPDF.php?file=of0306">of0306.pdf</a><br /> Declaration for Federal Employment
		</p>
		<p>{if $smarty.post.add8 || $smarty.post.add9 || $smarty.post.add10}
			<a href="getPDF.php?file=SF86A">SF86A.pdf</a><br /> Continuation sheet
		{/if}
		</p>
	</div>
{/if}
<!--<a href="index.php">Clear form</a><br>
<a href="index.php?fill=1">Fill form</a><br> -->
  <form class="form-basic" method="post" id="form2" name="form2" action="vaforms.php">
    <fieldset><legend></legend>
      <div class="note">
        <p  class="note2_1a"><b>Purpose of this Form</b></p> 
        <p class="note2_1a">The U.S. Government conducts background investigations to 
          establish that applicants or incumbents either employed by the Government or working for the 
          Government under contract, are suitable for the job. Information from this form is used primarily as the basis for 
          this investigation. Complete this form only after a conditional offer of employment has been made.</p>
        <p  class="note2_1a">Giving us the information we ask for is voluntary. However, we may not be able to complete your investigation, or complete
          it in a timely manner, if you don`t give us each item of information we request. This may affect your placement or employment prospects.</p>
        <p  class="note2_1a"><b>Authority to Request this Information</b></p> 
        <p  class="note2_1a">The U.S. Government is authorized to ask for this information under Executive Order 10577, 
          sections 3301 and 3302 of title 5, U.S. Code; and parts 5, 731, and 736 of Title 5, Code of Federal Regulations.</p>
        <p  class="note2_1a">Your Social Security Number is needed to keep records accurate, because other people may have the same name and birth date. 
          Executive Order 9397 also asks Federal agencies to use this number to help identify individuals in agency records.</p>
        <p  class="note2_1a"><b>The Investigative Process</b></p> 
        <p class="note2_1a">Background investigations are conducted using your responses on this form and on your Declaration
          for Federal Employment (OF 306) to develop information to show whether you are reliable, trustworthy, and of good conduct and character. 
          Your current employer must be contacted as part of the investigation, even if you have previously indicated on applications or other 
          forms that you do not want this.</p>
        <p  class="note2_1a"><b>Instructions for Completing this Form</b></p>
        <p  class="note2_1a">1. Follow the instructions given to you by the person who gave you the form and any other clarifying instructions furnished 
          by that person to assist you in completion of the form. Find out how many copies of the form you are to turn in. You must sign and 
          date, in black ink, the original and each copy you submit.</p>
        <p  class="note2_1a">2. Type or legibly print your answers in black ink (if your form is not legible, it will not be accepted). You may also be asked
          to submit your form in an approved electronic format.</p>
        <p  class="note2_1a">3. All questions on this form must be answered. If no response is necessary or applicable, indicate this on the form
          (for example, enter "None" or "N/A"). If you find that you cannot report an exact date, approximate or estimate the date to
          the best of your ability and indicate this by marking "APPROX." or "EST."</p>
        <p  class="note2_1a">4. Any changes that you make to this form after you sign it must be initialed and dated by you. Under certain limited
          circumstances, agencies may modify the form consistent with your intent.</p>
        <p  class="note2_1a">5. You must use the State codes (abbreviations) listed on the back of this page when you fill out this form. 
          Do not abbreviate the names of cities or foreign countries.</p>
        <p  class="note2_1a">6. The 5-digit postal ZIP codes are needed to speed the processing of your investigation. The office that provided 
          the form will assist you in completing the ZIP codes.</p>
        <p  class="note2_1a">7. All telephone numbers must include area codes.</p> 
        <p  class="note2_1a">8. All dates provided on this form must be in Month/Day/Year or 
          Month/Year format. Use numbers (1-12) to indicate months. For example, June 10, 1978, should be shown as 6/10/78.</p>
        <p  class="note2_1a">9. Whenever "City (Country)" is shown in an address block, also provide in that block the name of the country when 
          the address is outside the United States.</p>
      </div>
        </fieldset>
<!-- ----------------------------------------------------    1     ------------------------------------------------ -->
  <fieldset><legend><span>1.</span> FULL NAME</legend>
    <div class="note">
      <p>If you have only initials in your name, use them and state (IO). If you have no middle name, enter "NMN".</p>
      <p>If you are a "Jr.," "Sr.," "II," etc., enter this in the box after your middle name.</p>
    </div>
    <div class="set-default clearer">
      {textfield label="Lastname" name="LName"  class="20"}
      {textfield label="First Name" name="FName" class="20_2a"}
      {textfield label="Middle Name" name="MName" class="20_2a"}
      {textfield label="Jr., II, etc." name="NameSuffix" class="20_2a"}
    </div>
  </fieldset>     
<!-- ----------------------------------------------------    2     ------------------------------------------------ -->
  <fieldset><legend><span>2.</span><span class="stext {if $errorMessages.DOB}item-error{/if}"> DATE OF BIRTH</span></legend>
        {date onchange="selService()" name="DOB"}
      </fieldset>
<!-- ----------------------------------------------------    3     ------------------------------------------------ -->
      <fieldset><legend><span>3.</span> PLACE OF BIRTH</legend>
        <div class="set-default clearer">
          {textfield label="City" name="CityOfBirth"  class="20"}
          <div id="pobst" {if $smarty.post.CountryOfBirth != 'United States'} style="display:none;"{/if} >
            {textfield label="County" name="CountyOfBirth"  class="20_2a"}
            {states name="StateOfBirth" label="State" class="8_1a"}
          </div>
          {countries name="CountryOfBirth" onchange="pob()" label="Country"}
        </div>
      </fieldset>
<!-- ----------------------------------------------------    4     ------------------------------------------------ -->
      <fieldset><legend><span>4.</span><span class="stext {if $errorMessages.SSN} item-error {/if}"> SOCIAL SECURITY</span></legend>
            {textfield label="" name="SSN"  class="20"}
          </fieldset>
<!-- ----------------------------------------------------    5     ------------------------------------------------ -->
          <fieldset><legend><span>5.</span> OTHER NAMES USED</legend>
            <div class="note">
              <p>Give other names you used and the period of time you used them (for example: your maiden name, name(s) by a former marriage, former name(s), alias(es), or nickname(s)). If the other name is your maiden name, put "nee" in front of it.</p>
            </div>
            <ol>
              <li class="set-default clearer">
                <div class="item-number fl">#1</div>
                {textfield label="Name" name="OtherName1"  class="29"}  
                {monthyear name="DateFrom1" label="From"}
                {monthyear name="DateTo1" label="To"}
              </li>
              <li class="set-default clearer">
                <div class="item-number fl">#2</div>
                {textfield label="Name" name="OtherName2"  class="29"}  
                {monthyear name="DateFrom2" label="From"}
                {monthyear name="DateTo2" label="To"}
              </li>
              <li class="set-default clearer">
                <div class="item-number fl">#3</div>
                {textfield label="Name" name="OtherName3"  class="29"}  
                {monthyear name="DateFrom3" label="From"}
                {monthyear name="DateTo3" label="To"}
              </li>
              <li class="set-default clearer">
                <div class="item-number fl">#4</div>
                {textfield label="Name" name="OtherName4"  class="29"}  
                {monthyear name="DateFrom4" label="From"}
                {monthyear name="DateTo4" label="To"}
              </li>
            </ol>
          </fieldset>     
<!-- ----------------------------------------------------    6     ------------------------------------------------ -->
          <fieldset><legend><span>6.</span><span class="stext {if $errorMessages.sex}item-error{/if}"> SEX</span></legend>
                {radiohorisontal name="sex" onclick="selService()" labels="Male|Female" values="male|female"}
              </fieldset>
<!-- ----------------------------------------------------    7     ------------------------------------------------ -->
              <fieldset><legend><span>7.</span> CITIZENSHIP</legend>
                <ol class="set-nolabel list-lalpha">
                  <li><span class="stext {if $errorMessages.citizen}item-error{/if}">Mark the box below that reflects your current citizenship status, and follow its instructions.</span>
                        {radiovertical onclick="showCitizen()" name="citizen" labels=" I am a U.S. citizen or national by birth in the U.S. or U.S. territory/possession.|
                        I am a U.S. citizen, but I was NOT born in the U.S. |
                        I am not a U.S. citizen. " 
                        values="I am a U.S. citizen or national by birth in the U.S. or U.S. territory/possession.|I am a U.S. citizen, but I was NOT born in the U.S.
                        |I am not a U.S. citizen." values="USCitizen|NotBornCitizen|NotAUSCitizen"}
						<br clear="all" />
                      </li>
                      <li id="citizenb" {if !$smarty.post.citizen} style="display: none"; {/if}>
                        <div class="list-lalpha-inner">{textfield label="Your Mothers Maiden Name" name="MothersMaidenName"  class="20n"}</div>
                        <br clear="all" />  
                      </li>
                      <li id="citizenc" {if $smarty.post.citizen != "NotBornCitizen"} style="display: none"; {/if}><div class="list-lalpha-inner">
                          <strong>United States Citizenship</strong>
                          <p class="note">Give other names you used and the period of time you used them (for example: yourmaiden name, name(s)byaformermarriage,formername(s),alias(es), or nickname(s)). If the other name is your maiden name, put "nee" in front of it.</p>
                          <p class="note"><strong>Naturalization Certificate</strong></p>
                          <p class="note"><em>Where were you naturalized?</em></p>
                          <div class="set-default clearer">
                            {textfield label="Court" name="CourtOfNaturaliztion"  class="15"}
                            {textfield label="City" name="CityOfNaturalization"  class="15_1a"}
                            {states name="StateOfNaturalization" label="State" class="8_1a"}
                            {textfield label="Certificate Number" name="NaturalizationCertificate"  class="12_2"}                       
                            <div class="item-date fl no-padtop">
                              {date label="Month/Day/Year Issued" name="DateOfIssue1"}
                            </div>
                          </div>
                          <p class="note2"><strong>Citizenship Certificate</strong></p>
                          <p class="note"><em>Where was the certificate issued?</em></p>
                          <div class="set-default clearer">
                            {textfield label="City" name="CityOfCitizenshipCertificate"  class="32"}    
                            {states name="StateOfCitizenshipCertificate" label="State" class="8_1a"}
                            {textfield label="Certificate Number" name="CitizenshipCertificate"  class="12_2"}
                            <div class="item-date fl no-padtop">
                              {date label="Month/Day/Year Issued"  name="DateOfIssue2"}
                            </div>
                          </div>
                          <p class="note2_3a"><strong>State Department Form 240 - Report of Birth Abroad of a Citizen of the United States</strong></p>
                          <p class="note"><em>Give the date the form was prepared and give an explanation if needed</em></p>
                          <div class="set-default clearer">
                            <div class="fl no-padtop">
                              {date label="Month/Day/Year Issued" name="DateOfFormPrepared"}
                            </div>
                            {textfield label="Explanation" name="Explenation1"  class="51a_13"}
                          </div>
                          <p class="note2_4a"><strong>U.S. Passport.</strong></p>
                          <p class="note">This may be either a current or previous U.S. Passport.</p>
                          <div class="set-default clearer">
                            {textfield label="Passport number" name="Passportn"  class="12"}
                            <div class="fl left_marg2a no-padtop">
                              {date label="Month/Day/Year Issued" name="DateOfIssue3"}
                            </div>
                          </div>
                        </div></li>
                      <li id="citizend" {if $smarty.post.citizen != "USCitizen" && $smarty.post.citizen != "NotBornCitizen"} style="display: none"; {/if}> <div class="list-lalpha-inner">
                          <strong>Dual Citizenship</strong>
                          <p class="note">If you are (or were) a dual citizen of the United States and another country, provide the name of that country in the space </p>
                          <div class="set-default clearer">
                            {textfield label="Country" name="CountryOfDualCitizenship"  class="35"}
                            <div>
                              <div></li>
                              <li id="citizene" {if $smarty.post.citizen != "NotAUSCitizen"} style="display: none"; {/if}> <div class="list-lalpha-inner">
                                  <strong>Alien</strong>
                                  <p class="note">If you are an alien, provide the following information:</p>
                                  <p class="note"><strong>Place You Entered the United States</strong></p>
                                  <div class="set-default clearer">
                                    {textfield label="City" name="CityOfUSEntrance"  class="30"}
                                    {states name="StateOfUSEnterance" label="State" class="8_2a"}
                                    <div class="fl left_marg1a no-padtop">
                                      {date label="Month/Day/Year Issued" name="DateOfUSEnterance"}
                                    </div>
                                    <div><br clear="all" />
                                      <div class="set-default clearer">
                                        {textfield label="Alien registration number" name="AlienRegistrationNumber"  class="17"}
                                        {textfield label="Country(ies) of citizenship" name="CountryOfCitizenship"  class="54_2a"}
                                        <div>
                                          <div></li>
                                        </ol>
        </fieldset>
<br>
<br>

<!-- ----------------------------------------------------    8     ------------------------------------------------ -->
  <fieldset><legend><span>8.</span> WHERE YOU HAVE LIVED</legend>
    <p class="note">List the places where you have lived, beginning with the most recent (#1) and working back 5 years. All periods must be accounted for in your list. Be sure to indicate the actual physical location of your residence: do not use a post office box as an address, do not list a permanent address when you were actually living at a school address, etc. Be sure to specify your location as closely as possible: for example, do not list only your base or ship, list your barracks number or home port. You may omit temporary military duty locations under 90 days (list your permanent address instead), and you should use your APO/FPO address if you lived overseas.</p>
    <p class="note">For any address in the last 3 years, list a person who knew you at that address, and who preferably still lives in that area (do not list people for residences completely outside this 3-year period, and do not list your spouse, former spouses, or other relatives).</p>
    <div class="area">
      <div class="set-default clearer">
        <div class="item-number fl">#1</div>
        {monthyear name="DateFrom5" onchange="fillTo('DateFrom5', 'DateTo6')" label="From" class="10-45_1"}
        <div class="present fl"><span class="stext2">To</span><br /><b>Present</b></div>
        {textfield label="Street Address" name="StreetAddress1"  class="20_1"}
        {textfield label="Apt. #" name="Aptn1"  class="4_1"}
        {textfield label="City (Country)" name="CityCountry1"  class="12_1"}
        {states name="State1" label="State" class="8_1"}
        {textfield label="ZipCode" name="ZIpCode1"  class="7_1"}
      </div>
      <div class="set-default clearer left_marg2a">                    
        {textfield label="Name of Person Who Knows You" name="NameOfPerson1"  class="32"}
        {textfield label="Street Address" name="StreetAddressOfPerson1"  class="20_1"}
        {textfield label="Apt. #" name="AptnOfPerson1"  class="4_1"}
        {textfield label="City (Country)" name="CityCountryOfPerson1"  class="12_1"}
        {states name="StateOfPerson1" label="State" class="8_1"}
        {textfield label="ZipCode" name="ZIpCodeOfPerson1"  class="7_1"}
      </div>
    </div>
    <div class="set-default clearer">
      <div class="item-number fl">#2</div>
      {monthyear name="DateFrom6" onchange="fillTo('DateFrom6', 'DateTo7')" label="From" class="10-45_1"}
      <div class="present fl"><span class="stext2">To</span><br /><p id="DateTo6" class="for_date_to">{if $smarty.post.MonthDateFrom5 &&  $smarty.post.YearDateFrom5}{$smarty.post.MonthDateFrom5}/{$smarty.post.YearDateFrom5}{/if}</p></div>
      {textfield label="Street Address" name="StreetAddress2"  class="20_1"}
      {textfield label="Apt. #" name="Aptn2"  class="4_1"}
      {textfield label="City (Country)" name="CityCountry2"  class="12_1"}
      {states name="State2" label="State" class="8_1"}
      {textfield label="ZipCode" name="ZIpCode2"  class="7_1"}
    </div>
    <div class="set-default clearer left_marg2a">                    
      {textfield label="Name of Person Who Knows You" name="NameOfPerson2"  class="32"}
      {textfield label="Street Address" name="StreetAddressOfPerson2"  class="20_1"}
      {textfield label="Apt. #" name="AptnOfPerson2"  class="4_1"}
      {textfield label="City (Country)" name="CityCountryOfPerson2"  class="12_1"}
      {states name="StateOfPerson2" label="State" class="8_1"}
      {textfield label="ZipCode" name="ZIpCodeOfPerson2"  class="7_1"}
    </div>
    <div class="area">
      <div class="set-default clearer">
        <div class="item-number fl">#3</div>
        {monthyear name="DateFrom7" onchange="fillTo('DateFrom7', 'DateTo8')" label="From" class="10-45_1"}
        <div class="present fl"><span class="stext2">To</span><br /><p id="DateTo7" class="for_date_to">{if $smarty.post.MonthDateFrom6 &&  $smarty.post.YearDateFrom6}{$smarty.post.MonthDateFrom6}/{$smarty.post.YearDateFrom6}{/if}</p></div>
        {textfield label="Street Address" name="StreetAddress3"  class="20_1"}
        {textfield label="Apt. #" name="Aptn3"  class="4_1"}
        {textfield label="City (Country)" name="CityCountry3"  class="12_1"}
        {states name="State3" label="State" class="8_1"}
        {textfield label="ZipCode" name="ZIpCode3"  class="7_1"}
      </div>
      <div class="set-default clearer left_marg2a">                    
        {textfield label="Name of Person Who Knows You" name="NameOfPerson3"  class="32"}
        {textfield label="Street Address" name="StreetAddressOfPerson3"  class="20_1"}
        {textfield label="Apt. #" name="AptnOfPerson3"  class="4_1"}
        {textfield label="City (Country)" name="CityCountryOfPerson3"  class="12_1"}
        {states name="StateOfPerson3" label="State" class="8_1"}
        {textfield label="ZipCode" name="ZIpCodeOfPerson3"  class="7_1"}
      </div>
    </div>
    <div class="set-default clearer">
      <div class="item-number fl">#4</div>
      {monthyear name="DateFrom8" onchange="fillTo('DateFrom8', 'DateTo9')" label="From" class="10-45_1"}
      <div class="present fl"><span class="stext2">To</span><br /><p id="DateTo8" class="for_date_to">{if $smarty.post.MonthDateFrom7 &&  $smarty.post.YearDateFrom7}{$smarty.post.MonthDateFrom7}/{$smarty.post.YearDateFrom7}{/if}</p></div>
      {textfield label="Street Address" name="StreetAddress4"  class="20_1"}
      {textfield label="Apt. #" name="Aptn4"  class="4_1"}
      {textfield label="City (Country)" name="CityCountry4"  class="12_1"}
      {states name="State4" label="State" class="8_1"}
      {textfield label="ZipCode" name="ZIpCode4"  class="7_1"}
    </div>
    <div class="set-default clearer left_marg2a">                    
      {textfield label="Name of Person Who Knows You" name="NameOfPerson4"  class="32"}
      {textfield label="Street Address" name="StreetAddressOfPerson4"  class="20_1"}
      {textfield label="Apt. #" name="AptnOfPerson4"  class="4_1"}
      {textfield label="City (Country)" name="CityCountryOfPerson4"  class="12_1"}
      {states name="StateOfPerson4" label="State" class="8_1"}
      {textfield label="ZIpCode" name="ZIpCodeOfPerson4"  class="7_1"}
    </div>
    <div class="area">
      <div class="set-default clearer">
        <div class="item-number fl">#5</div>
        {monthyear name="DateFrom9" onchange="fillTo('DateFrom9', 'aDateTo5')" label="From" class="10-45_1"}
        <div class="present fl"><span class="stext2">To</span><br /><p id="DateTo9" class="for_date_to">{if $smarty.post.MonthDateFrom8 &&  $smarty.post.YearDateFrom8}{$smarty.post.MonthDateFrom8}/{$smarty.post.YearDateFrom8}{/if}</p></div>
        {textfield label="Street Address" name="StreetAddress5"  class="20_1"}
        {textfield label="Apt. #" name="Aptn5"  class="4_1"}
        {textfield label="City (Country)" name="CityCountry5"  class="12_1"}
        {states name="State5" label="State" class="8_1"}
        {textfield label="ZipCode" name="ZIpCode5"  class="7_1"}
      </div>
      <div class="set-default clearer left_marg2a">                    
        {textfield label="Name of Person Who Knows You" name="NameOfPerson5"  class="32"}
        {textfield label="Street Address" name="StreetAddressOfPerson5"  class="20_1"}
        {textfield label="Apt. #" name="AptnOfPerson5"  class="4_1"}
        {textfield label="City (Country)" name="CityCountryOfPerson5"  class="12_1"}
        {states name="StateOfPerson5" label="State" class="8_1"}
        {textfield label="ZipCode" name="ZIpCodeOfPerson5"  class="7_1"}
      </div>
    </div>
        </fieldset>
  <a href="javascript:showadd(8);" {if $smarty.post.add8} style="display:none;" {/if} id="a8show">More</a>
<div {if !$smarty.post.add8} style="display:none;" {/if} id="a8"  >
  
Enter more fields.
<br>

  <fieldset>
    <div class="area">
      <div class="set-default clearer">
        <div class="item-number fl">#6</div>
        {monthyear name="aDateFrom5" onchange="fillTo('aDateFrom5', 'aDateTo6')" label="From" class="10-45_1"}
        <div class="present fl"><span class="stext2">To</span><br /><p id="aDateTo5" class="for_date_to">{if $smarty.post.MonthDateFrom5 &&  $smarty.post.YearDateFrom5}{$smarty.post.MonthDateFrom9}/{$smarty.post.YearDateFrom9}{/if}</p></div>
        {textfield label="Street Address" name="aStreetAddress1"  class="20_1"}
        {textfield label="Apt. #" name="aAptn1"  class="4_1"}
        {textfield label="City (Country)" name="aCityCountry1"  class="12_1"}
        {states name="aState1" label="State" class="8_1"}
        {textfield label="ZipCode" name="aZIpCode1"  class="7_1"}
      </div>
      <div class="set-default clearer left_marg2a">                    
        {textfield label="Name of Person Who Knows You" name="aNameOfPerson1"  class="32"}
        {textfield label="Street Address" name="aStreetAddressOfPerson1"  class="20_1"}
        {textfield label="Apt. #" name="aAptnOfPerson1"  class="4_1"}
        {textfield label="City (Country)" name="aCityCountryOfPerson1"  class="12_1"}
        {states name="aStateOfPerson1" label="State" class="8_1"}
        {textfield label="ZipCode" name="aZIpCodeOfPerson1"  class="7_1"}
      </div>
    </div>
    <div class="set-default clearer">
      <div class="item-number fl">#7</div>
      {monthyear name="aDateFrom6" onchange="fillTo('aDateFrom6', 'aDateTo7')" label="From" class="10-45_1"}
      <div class="present fl"><span class="stext2">To</span><br /><p id="aDateTo6" class="for_date_to">{if $smarty.post.MonthDateFrom5 &&  $smarty.post.YearDateFrom5}{$smarty.post.aMonthDateFrom5}/{$smarty.post.aYearDateFrom5}{/if}</p></div>
      {textfield label="Street Address" name="aStreetAddress2"  class="20_1"}
      {textfield label="Apt. #" name="aAptn2"  class="4_1"}
      {textfield label="City (Country)" name="aCityCountry2"  class="12_1"}
      {states name="aState2" label="State" class="8_1"}
      {textfield label="ZipCode" name="aZIpCode2"  class="7_1"}
    </div>
    <div class="set-default clearer left_marg2a">                    
      {textfield label="Name of Person Who Knows You" name="aNameOfPerson2"  class="32"}
      {textfield label="Street Address" name="aStreetAddressOfPerson2"  class="20_1"}
      {textfield label="Apt. #" name="aAptnOfPerson2"  class="4_1"}
      {textfield label="City (Country)" name="aCityCountryOfPerson2"  class="12_1"}
      {states name="aStateOfPerson2" label="State" class="8_1"}
      {textfield label="ZipCode" name="aZIpCodeOfPerson2"  class="7_1"}
    </div>
    <div class="area">
      <div class="set-default clearer">
        <div class="item-number fl">#8</div>
        {monthyear name="aDateFrom7" onchange="fillTo('aDateFrom7', 'aDateTo8')" label="From" class="10-45_1"}
        <div class="present fl"><span class="stext2">To</span><br /><p id="aDateTo7" class="for_date_to">{if $smarty.post.aMonthDateFrom6 &&  $smarty.post.aYearDateFrom6}{$smarty.post.aMonthDateFrom6}/{$smarty.post.aYearDateFrom6}{/if}</p></div>
        {textfield label="Street Address" name="aStreetAddress3"  class="20_1"}
        {textfield label="Apt. #" name="aAptn3"  class="4_1"}
        {textfield label="City (Country)" name="aCityCountry3"  class="12_1"}
        {states name="aState3" label="State" class="8_1"}
        {textfield label="ZipCode" name="aZIpCode3"  class="7_1"}
      </div>
      <div class="set-default clearer left_marg2a">                    
        {textfield label="Name of Person Who Knows You" name="aNameOfPerson3"  class="32"}
        {textfield label="Street Address" name="aStreetAddressOfPerson3"  class="20_1"}
        {textfield label="Apt. #" name="aAptnOfPerson3"  class="4_1"}
        {textfield label="City (Country)" name="aCityCountryOfPerson3"  class="12_1"}
        {states name="aStateOfPerson3" label="State" class="8_1"}
        {textfield label="ZipCode" name="aZIpCodeOfPerson3"  class="7_1"}
      </div>
    </div>
    <div class="set-default clearer">
      <div class="item-number fl">#9</div>
      {monthyear name="aDateFrom8" onchange="fillTo('aDateFrom8', 'aDateTo9')" label="From" class="10-45_1"}
      <div class="present fl"><span class="stext2">To</span><br /><p id="aDateTo8" class="for_date_to">{if $smarty.post.aMonthDateFrom7 &&  $smarty.post.aYearDateFrom7}{$smarty.post.aMonthDateFrom7}/{$smarty.post.aYearDateFrom7}{/if}</p></div>
      {textfield label="Street Address" name="aStreetAddress4"  class="20_1"}
      {textfield label="Apt. #" name="aAptn4"  class="4_1"}
      {textfield label="City (Country)" name="aCityCountry4"  class="12_1"}
      {states name="aState4" label="State" class="8_1"}
      {textfield label="ZipCode" name="aZIpCode4"  class="7_1"}
    </div>
    <div class="set-default clearer left_marg2a">                    
      {textfield label="Name of Person Who Knows You" name="aNameOfPerson4"  class="32"}
      {textfield label="Street Address" name="aStreetAddressOfPerson4"  class="20_1"}
      {textfield label="Apt. #" name="aAptnOfPerson4"  class="4_1"}
      {textfield label="City (Country)" name="aCityCountryOfPerson4"  class="12_1"}
      {states name="aStateOfPerson4" label="State" class="8_1"}
      {textfield label="ZIpCode" name="aZIpCodeOfPerson4"  class="7_1"}
    </div>
    <div class="area">
      <div class="set-default clearer">
        <div class="item-number fl">#10</div>
        {monthyear name="aDateFrom9" label="From" class="10-45_1"}
        <div class="present fl"><span class="stext2">To</span><br /><p id="aDateTo9" class="for_date_to">{if $smarty.post.aMonthDateFrom8 &&  $smarty.post.aYearDateFrom8}{$smarty.post.aMonthDateFrom8}/{$smarty.post.aYearDateFrom8}{/if}</p></div>
        {textfield label="Street Address" name="aStreetAddress5"  class="20_1"}
        {textfield label="Apt. #" name="aAptn5"  class="4_1"}
        {textfield label="City (Country)" name="aCityCountry5"  class="12_1"}
        {states name="aState5" label="State" class="8_1"}
        {textfield label="ZipCode" name="aZIpCode5"  class="7_1"}
      </div>
      <div class="set-default clearer left_marg2a">                    
        {textfield label="Name of Person Who Knows You" name="aNameOfPerson5"  class="32"}
        {textfield label="Street Address" name="aStreetAddressOfPerson5"  class="20_1"}
        {textfield label="Apt. #" name="aAptnOfPerson5"  class="4_1"}
        {textfield label="City (Country)" name="aCityCountryOfPerson5"  class="12_1"}
        {states name="aStateOfPerson5" label="State" class="8_1"}
        {textfield label="ZipCode" name="aZIpCodeOfPerson5"  class="7_1"}
      </div>
    </div>
  </fieldset>

<a href="javascript:hideadd(8);" id="a8hide">Hide More</a>
<input type="hidden" name="add8" value="{$smarty.post.add8}" id="add8"  /><b style="color: red;">{$errorMessages.add8}</b>
</div>
<br>
<br>
  
<!-- ----------------------------------------------------    9     ------------------------------------------------ -->

<fieldset><legend><span>9.</span> WHERE YOU WENT TO SCHOOL</legend>
  <p class="note">List the schools you have attended, beyond Junior High School, beginning with the most recent (#1) and working back 5 years. List all College or University degrees and the dates they were received. If all of your education occurred more than 5 years ago, list your most recent education beyond high school, no matter when that education occurred.</p>
  <p class="note">-For correspondence schools and extension classes, provide the address where the records are maintained.</p>
  <div class="area">
    <div class="set-default clearer">
      <div class="item-number fl">#1</div>
      {monthyear name="DateFrom10" label="From" class="10-45_1"}
      {monthyear name="DateTo10" label="To" class="10_1-45_1"}
      {code13 name="Code1" label="School Type"}
      {textfield label="Name of School" name="NameOfSchool1"  class="39_1"}
    </div>
    <div class="set-default clearer left_marg2a">                    
      {textfield label="Street Address and City (Country) of School" name="StreetAddresspCityOfSchool1"  class="32"}
      {textfield label="Degree/Diploma/Other" name="DegreesDiplomasOther1"  class="22_1"}
      {textfield label="Month/Year Awarded" name="MonthsYearAwarded1"  class="15_1"}
      {states name="StateOfSchool1" label="State" class="8_1"}
      {textfield label="ZIP Code" name="ZIpCodeOfSchool1"  class="7_1"}
    </div>
  </div>
  <div class="set-default clearer">
    <div class="item-number fl">#2</div>
    {monthyear name="DateFrom11" label="From" class="10-45_1"}
    {monthyear name="DateTo11" label="To" class="10_1-45_1"}
    {code13 name="Code2" label="School Type"}
    {textfield label="Name of School" name="NameOfSchool2"  class="39_1"}
  </div>
  <div class="set-default clearer left_marg2a">                    
    {textfield label="Street Address and City (Country) of School" name="StreetAddresspCityOfSchool2"  class="32"}
    {textfield label="Degree/Diploma/Other" name="DegreesDiplomasOther2"  class="22_1"}
    {textfield label="Month/Year Awarded" name="MonthsYearAwarded2"  class="15_1"}
    {states name="StateOfSchool2" label="State" class="8_1"}
    {textfield label="ZIP Code" name="ZIpCodeOfSchool2"  class="7_1"}
  </div>
  <div class="area">
    <div class="set-default clearer">
      <div class="item-number fl">#3</div>
      {monthyear name="DateFrom12" label="From" class="10-45_1"}
      {monthyear name="DateTo12" label="To" class="10_1-45_1"}
      {code13 name="Code3" label="School Type"}
      {textfield label="Name of School" name="NameOfSchool3"  class="39_1"}
    </div>
    <div class="set-default clearer left_marg2a">                    
      {textfield label="Street Address and City (Country) of School" name="StreetAddresspCityOfSchool3"  class="32"}
      {textfield label="Degree/Diploma/Other" name="DegreesDiplomasOther3"  class="22_1"}
      {textfield label="Month/Year Awarded" name="MonthsYearAwarded3"  class="15_1"}
      {states name="StateOfSchool3" label="State" class="8_1"}
      {textfield label="ZIP Code" name="ZIpCodeOfSchool3"  class="7_1"}
    </div>
  </div>
</fieldset>
<a href="javascript:showadd(9);" {if $smarty.post.add9} style="display:none;" {/if} id="a9show">More</a>
<div {if !$smarty.post.add9} style="display:none;" {/if} id="a9"  >
 Enter more fields 
  <fieldset>
    <div class="area">
      <div class="set-default clearer">
        <div class="item-number fl">#4</div>
        {monthyear name="aDateFrom10" label="From" class="10-45_1"}
        {monthyear name="aDateTo10" label="To" class="10_1-45_1"}
        {code13 name="aCode1" label="School Type"}
        {textfield label="Name of School" name="aNameOfSchool1"  class="39_1"}
      </div>
      <div class="set-default clearer left_marg2a">                    
        {textfield label="Street Address and City (Country) of School" name="aStreetAddresspCityOfSchool1"  class="32"}
        {textfield label="Degree/Diploma/Other" name="aDegreesDiplomasOther1"  class="22_1"}
        {textfield label="Month/Year Awarded" name="aMonthsYearAwarded1"  class="15_1"}
        {states name="aStateOfSchool1" label="State" class="8_1"}
        {textfield label="ZIP Code" name="aZIpCodeOfSchool1"  class="7_1"}
      </div>
    </div>
    <div class="set-default clearer">
      <div class="item-number fl">#5</div>
      {monthyear name="aDateFrom11" label="From" class="10-45_1"}
      {monthyear name="aDateTo11" label="To" class="10_1-45_1"}
      {code13 name="aCode2" label="School Type"}
      {textfield label="Name of School" name="aNameOfSchool2"  class="39_1"}
    </div>
    <div class="set-default clearer left_marg2a">                    
      {textfield label="Street Address and City (Country) of School" name="aStreetAddresspCityOfSchool2"  class="32"}
      {textfield label="Degree/Diploma/Other" name="aDegreesDiplomasOther2"  class="22_1"}
      {textfield label="Month/Year Awarded" name="aMonthsYearAwarded2"  class="15_1"}
      {states name="aStateOfSchool2" label="State" class="8_1"}
      {textfield label="ZIP Code" name="aZIpCodeOfSchool2"  class="7_1"}
    </div>
    <div class="area">
      <div class="set-default clearer">
        <div class="item-number fl">#6</div>
        {monthyear name="aDateFrom12" label="From" class="10-45_1"}
        {monthyear name="aDateTo12" label="To" class="10_1-45_1"}
        {code13 name="aCode3" label="School Type"}
        {textfield label="Name of School" name="aNameOfSchool3"  class="39_1"}
      </div>
      <div class="set-default clearer left_marg2a">                    
        {textfield label="Street Address and City (Country) of School" name="aStreetAddresspCityOfSchool3"  class="32"}
        {textfield label="Degree/Diploma/Other" name="aDegreesDiplomasOther3"  class="22_1"}
        {textfield label="Month/Year Awarded" name="aMonthsYearAwarded3"  class="15_1"}
        {states name="aStateOfSchool3" label="State" class="8_1"}
        {textfield label="ZIP Code" name="aZIpCodeOfSchool3"  class="7_1"}
      </div>
    </div>
  </fieldset>
  <a href="javascript:hideadd(9);"  id="a9hide">Hide More</a>
  <input type="hidden" name="add9" value="{$smarty.post.add9}" id="add9" value="0" /><b style="color: red;">{$errorMessages.add9}</b>
</div>
<br>
<br>
<!-- ----------------------------------------------------    10     ------------------------------------------------ -->
<fieldset><legend><span>10.</span> YOUR EMPLOYMENT ACTIVITIES</legend>
  <p class="note">List your employment activities, beginning with the present (#1) and working back 5 years. You should list all full-time work, part-time work, military service, temporary military duty locations over 90 days, self-employment, other paid work, and all periods of unemployment. The entire 5-year period must be accounted for without breaks, but you need not list employments before your 16th birthday.</p>
  <p class="note"><strong>Employer/Verifier Name.</strong> List the business name of your employer or the name of the person who can verify your self-employment or unemployment in this block. If military service is being listed, include your duty location or home port here as well as your branch of service. You should provide separate listings to reflect changes in your military duty locations or home ports. </p>
  <p class="note"><strong>Previous Periods of Activity.</strong> Complete these lines if you worked for an employer on more than one occasion at the same location. After entering the most recent period of employment in the initial numbered block, provide previous periods of employment at the same location on the additional lines provided. For example, if you worked at XY Plumbing in Denver, CO, during 3 separate periods of time, you would enter dates and information concerning the most recent period of employment first, and provide dates, position titles, and supervisors for the two previous periods of employment on the lines below that information.</p>
  <div class="area_w">
    <div class="set-default clearer">
      <div class="item-number fl">#1</div>
      <input type="checkbox" onClick="unemployment(1)" name="unemp1" value="1" id="unemp1" {if $smarty.post.unemp1} checked="checked"{/if}  /><strong>Unemployment Period</strong><br />    
    </div>
    <div class="set-default clearer">
      {monthyear name="DateFrom13" onchange="fillTo('DateFrom13', 'DateTo17')" label="From" class="10-45_1"}
      <div class="present1 fl"><span class="stext6">To</span><br /><b>Present</b></div>
      {code19 name="Code4" label="Type of employment"}
      <div class="block_position2">{textfield label="Employer/Verifier <br /> Name/Military Duty Location" name="EmployersVerifierNamesMilitaryDutyLocation1"  class="28a_1"}</div>
      <div class="block_position2" id="positionEmp1">{textfield label="Your Position <br /> Title/Military Rank" name="PositionTitlesMilitaryRank"  class="15a_1"}</div>
      
    </div>
    <div id="empblock1" {if $smarty.post.unemp1} style="display:none;" {/if}>
    <div class="set-default clearer">                    
      {textfield label="Employer's/Verifier's Street Address" name="EmployersVerifierStreetAddress1"  class="44a"}
      {textfield label="City (Country)" name="EmployersVerifierCityCountry1"  class="12_1"}
      {states name="EmployersVerifierState1" label="State" class="8_1"}
      {textfield label="ZipCode" name="EmployersVerifierZIPCode1"  class="7_1"}
      <span class="stext6 {if $errorMessages.AreaCodeOfEmployersVerifier1 || $errorMessages.PhonenOfEmployersVerifier1}item-error{/if}">Telephone Number</span>
      {textfield label="" name="AreaCodeOfEmployersVerifier1"  class="4_1"}
      {textfield label="" name="PhonenOfEmployersVerifier1"  class="11_a"}
    </div>
    <div class="set-default clearer">                    
      {textfield label="Street Address of Job Location <em>if different than Employer's Address</em>" name="StreetAddressOfJobLocation1"  class="44a"}
      {textfield label="City (Country)" name="CityCountryOfJobLocation1"  class="12_1"}
      {states name="StateOfJobLocation1" label="State" class="8r_1" }
      {textfield label="ZipCode" name="ZIPCodeOfJobLocation1"  class="7_1"}
      <span class="stext6 {if $errorMessages.AreaCodeOfJobLocation1 || $errorMessages.PhonenOfJobLocation1}item-error{/if} ">Telephone Number</span>
      {textfield label="" name="AreaCodeOfJobLocation1"  class="4_1"}
      {textfield label="" name="PhonenOfJobLocation1"  class="11_a"}
    </div>
    <div class="set-default clearer">                    
      {textfield label="Supervisor's Name & Street Address <em>if different than Job Location</em>" name="SupervisorNamepStreetAddress1"  class="44a"}
      {textfield label="City (Country)" name="CityCountryOfSupervisor1"  class="12_1"}
      {states name="StateOfSupervisor1" label="State" class="8r_1" }
      {textfield label="ZipCode" name="ZIPCodeOfSupervisor1"  class="7_1"}
      <span class="stext6 {if $errorMessages.AreaCodeOfSupervisor1 || $errorMessages.PhonenOfSupervisor1}item-error{/if}">Telephone Number</span>
      {textfield label="" name="AreaCodeOfSupervisor1"  class="4_1"}
      {textfield label="" name="PhonenOfSupervisor1"  class="11_a"}
    </div>
    <div  class="set-default clearer">
      <div class="previous_period fl">Previous period</div>
      <div class="fl">
        <div class="set-default clearer">  
          {monthyear name="DateFrom14" label="From" class="10-45_1"}
          {monthyear name="DateTo14" label="To" class="10_1-45_1"}
          {textfield label="Position Title" name="PositionTitle1"  class="16_1"}
          {textfield label="Supervisor" name="Supervisor1"  class="28a_1"}
        </div>
        <div class="set-default clearer">
          {monthyear name="DateFrom15" label="From" class="10-45_1"}
          {monthyear name="DateTo15" label="To" class="10_1-45_1"}
          {textfield label="Position Title" name="PositionTitle2"  class="16_1"}
          {textfield label="Supervisor" name="Supervisor2"  class="28a_1"}
        </div> 
        <div class="set-default clearer">
          {monthyear name="DateFrom16" label="From" class="10-45_1"}
          {monthyear name="DateTo16" label="To" class="10_1-45_1"}
          {textfield label="Position Title" name="PositionTitle3"  class="16_1"}
          {textfield label="Supervisor" name="Supervisor3"  class="28a_1"}
        </div>
      </div>
    </div>
    </div>
  </div>  
  <div class="area">
    <div class="set-default clearer">
      <div class="item-number fl">#2</div>
      <input type="checkbox" onClick="unemployment(2)"   name="unemp2" value="2" id="unemp2" {if $smarty.post.unemp2} checked="checked"{/if}  /><strong>Unemployment Period</strong><br />    
    </div>
    <div class="set-default clearer">
      {monthyear name="DateFrom17" onchange="fillTo('DateFrom17', 'DateTo21')" label="From" class="10-45_1"}
      <div class="present1 fl"><span class="stext6">To</span><br />
	  <p id="DateTo17" class="for_date_to">{if $smarty.post.MonthDateFrom13 &&  $smarty.post.YearDateFrom13}{$smarty.post.MonthDateFrom13}/{$smarty.post.YearDateFrom13}{/if}</p></div>
      {code19 name="Code5" label="Type of employment"}
      <div class="block_position2">{textfield label="Employer/Verifier <br /> Name/Military Duty Location" name="EmployersVerifierNamesMilitaryDutyLocation2"  class="28a_1"}</div>
      <div class="block_position2" id="positionEmp2">{textfield label="Your Position <br /> Title/Military Rank" name="PositionTitlesMilitaryRank2"  class="15a_1"}</div>
      
    </div>
    <div id="empblock2" {if $smarty.post.unemp2} style="display:none;" {/if}>
    <div class="set-default clearer">                    
      {textfield label="Employer's/Verifier's Street Address" name="EmployersVerifierStreetAddress2"  class="44a"}
      {textfield label="City (Country)" name="EmployersVerifierCityCountry2"  class="12_1"}
      {states name="EmployersVerifierState2" label="State" class="8_1"}
      {textfield label="ZipCode" name="EmployersVerifierZIPCode2"  class="7_1"}
      <span class="stext6   {if $errorMessages.AreaCodeOfEmployersVerifier2 || $errorMessages.PhonenOfEmployersVerifier2}item-error{/if}">Telephone Number</span>
      {textfield label="" name="AreaCodeOfEmployersVerifier2"  class="4_1"}
      {textfield label="" name="PhonenOfEmployersVerifier2"  class="11_a"}
    </div>
    <div class="set-default clearer">                    
      {textfield label="Street Address of Job Location <em>if different than Employer's Address</em>" name="StreetAddressOfJobLocation2"  class="44a"}
      {textfield label="City (Country)" name="CityCountryOfJobLocation2"  class="12_1"}
      {states name="StateOfJobLocation2" label="State" class="8r_1" }
      {textfield label="ZipCode" name="ZIPCodeOfJobLocation2"  class="7_1"}
      <span class="stext6 {if $errorMessages.AreaCodeOfJobLocation2 || $errorMessages.PhonenOfJobLocation2}item-error{/if} ">Telephone Number</span>
      {textfield label="" name="AreaCodeOfJobLocation2"  class="4_1"}
      {textfield label="" name="PhonenOfJobLocation2"  class="11_a"}
    </div>
    <div class="set-default clearer">                    
      {textfield label="Supervisor's Name & Street Address <em>if different than Job Location</em>" name="SupervisorNamepStreetAddress2"  class="44a"}
      {textfield label="City (Country)" name="CityCountryOfSupervisor2"  class="12_1"}
      {states name="StateOfSupervisor2" label="State" class="8r_1" }
      {textfield label="ZipCode" name="ZIPCodeOfSupervisor2"  class="7_1"}
      <span class="stext6  {if $errorMessages.AreaCodeOfSupervisor2 || $errorMessages.PhonenOfSupervisor2}item-error{/if}">Telephone Number</span>
      {textfield label="" name="AreaCodeOfSupervisor2"  class="4_1"}
      {textfield label="" name="PhonenOfSupervisor2"  class="11_a"}
    </div>
    <div class="set-default clearer">
      <div class="previous_period fl">Previous period</div>
      <div class="fl">
        <div class="set-default clearer">  
          {monthyear name="DateFrom18" label="From" class="10-45_1"}
          {monthyear name="DateTo18" label="To" class="10_1-45_1"}
          {textfield label="Position Title" name="PositionTitle4"  class="16_1"}
          {textfield label="Supervisor" name="Supervisor4"  class="28a_1"}
        </div>
        <div class="set-default clearer">
          {monthyear name="DateFrom19" label="From" class="10-45_1"}
          {monthyear name="DateTo19" label="To" class="10_1-45_1"}
          {textfield label="Position Title" name="PositionTitle5"  class="16_1"}
          {textfield label="Supervisor" name="Supervisor5"  class="28a_1"}
        </div> 
        <div class="set-default clearer">
          {monthyear name="DateFrom20" label="From" class="10-45_1"}
          {monthyear name="DateTo20" label="To" class="10_1-45_1"}
          {textfield label="Position Title" name="PositionTitle6"  class="16_1"}
          {textfield label="Supervisor" name="Supervisor6"  class="28a_1"}
        </div>
      </div>
    </div>
    </div>
  </div>
  <div class="area_w">
    <div class="set-default clearer">
      <div class="item-number fl">#3</div>
      <input type="checkbox"  onclick="unemployment(3)"  name="unemp3" value="3" id="unemp3" {if $smarty.post.unemp3} checked="checked"{/if} /><strong>Unemployment Period</strong><br />    
    </div>
    <div class="set-default clearer">
      {monthyear name="DateFrom21" onchange="fillTo('DateFrom21', 'DateTo25')" label="From" class="10-45_1"}
      <div class="present1 fl"><span class="stext6">To</span><br /><p id="DateTo21" class="for_date_to">{if $smarty.post.MonthDateFrom17 &&  $smarty.post.YearDateFrom17}{$smarty.post.MonthDateFrom17}/{$smarty.post.YearDateFrom17}{/if}</p></div>
      {code19 name="Code6" label="Type of employment"}
      <div class="block_position2">{textfield label="Employer/Verifier <br /> Name/Military Duty Location" name="EmployersVerifierNamesMilitaryDutyLocation3"  class="28a_1"}</div>
      <div class="block_position2" id="positionEmp3">{textfield label="Your Position <br /> Title/Military Rank" name="PositionTitlesMilitaryRank3"  class="15a_1"}</div>
      
    </div>
    <div id="empblock3" {if $smarty.post.unemp3} style="display:none;" {/if}>
    <div class="set-default clearer">                    
      {textfield label="Employer's/Verifier's Street Address" name="EmployersVerifierStreetAddress3"  class="44a"}
      {textfield label="City (Country)" name="EmployersVerifierCityCountry3"  class="12_1"}
      {states name="EmployersVerifierState3" label="State" class="8_1"}
      {textfield label="ZipCode" name="EmployersVerifierZIPCode3"  class="7_1"}
      <span class="stext6   {if $errorMessages.AreaCodeOfEmployersVerifier3 || $errorMessages.PhonenOfEmployersVerifier3}item-error{/if}">Telephone Number</span>
      {textfield label="" name="AreaCodeOfEmployersVerifier3"  class="4_1"}
      {textfield label="" name="PhonenOfEmployersVerifier3"  class="11_a"}
    </div>
    <div class="set-default clearer">                    
      {textfield label="Street Address of Job Location <em>if different than Employer's Address</em>" name="StreetAddressOfJobLocation3"  class="44a"}
      {textfield label="City (Country)" name="CityCountryOfJobLocation3"  class="12_1"}
      {states name="StateOfJobLocation3" label="State" class="8r_1" }
      {textfield label="ZipCode" name="ZIPCodeOfJobLocation3"  class="7_1"}
      <span class="stext6 {if $errorMessages.AreaCodeOfJobLocation3 || $errorMessages.PhonenOfJobLocation3}item-error{/if} ">Telephone Number</span>
      {textfield label="" name="AreaCodeOfJobLocation3"  class="4_1"}
      {textfield label="" name="PhonenOfJobLocation3"  class="11_a"}
    </div>
    <div class="set-default clearer">                    
      {textfield label="Supervisor's Name & Street Address <em>if different than Job Location</em>" name="SupervisorNamepStreetAddress3"  class="44a"}
      {textfield label="City (Country)" name="CityCountryOfSupervisor3"  class="12_1"}
      {states name="StateOfSupervisor3" label="State" class="8r_1" }
      {textfield label="ZipCode" name="ZIPCodeOfSupervisor3"  class="7_1"}
      <span class="stext6 {if $errorMessages.AreaCodeOfSupervisor3 || $errorMessages.PhonenOfSupervisor3}item-error{/if} ">Telephone Number</span>
      {textfield label="" name="AreaCodeOfSupervisor3"  class="4_1"}
      {textfield label="" name="PhonenOfSupervisor3"  class="11_a"}
    </div>
    <div  class="set-default clearer">
      <div class="previous_period fl">Previous period</div>
      <div class="fl">
        <div class="set-default clearer">  
          {monthyear name="DateFrom22" label="From" class="10-45_1"}
          {monthyear name="DateTo22" label="To" class="10_1-45_1"}
          {textfield label="Position Title" name="PositionTitle7"  class="16_1"}
          {textfield label="Supervisor" name="Supervisor7"  class="28a_1"}
        </div>
        <div class="set-default clearer">
          {monthyear name="DateFrom23" label="From" class="10-45_1"}
          {monthyear name="DateTo23" label="To" class="10_1-45_1"}
          {textfield label="Position Title" name="PositionTitle8"  class="16_1"}
          {textfield label="Supervisor" name="Supervisor8"  class="28a_1"}
        </div> 
        <div class="set-default clearer">
          {monthyear name="DateFrom24" label="From" class="10-45_1"}
          {monthyear name="DateTo24" label="To" class="10_1-45_1"}
          {textfield label="Position Title" name="PositionTitle9"  class="16_1"}
          {textfield label="Supervisor" name="Supervisor9"  class="28a_1"}
        </div>
      </div>
    </div>
    </div>
  </div>  
  <div class="area">
    <div class="set-default clearer">
      <div class="item-number fl">#4</div>
      <input type="checkbox"  onclick="unemployment(4)"  name="unemp4" value="4" id="unemp4" {if $smarty.post.unemp4} checked="checked"{/if} /><strong>Unemployment Period</strong><br />    
    </div>
    <div class="set-default clearer">
      {monthyear name="DateFrom25" onchange="fillTo('DateFrom25', 'DateTo29')" label="From" class="10-45_1"}
      <div class="present1 fl"><span class="stext6">To</span><br /><p id="DateTo25" class="for_date_to">{if $smarty.post.MonthDateFrom21 &&  $smarty.post.YearDateFrom21}{$smarty.post.MonthDateFrom21}/{$smarty.post.YearDateFrom21}{/if}</p></div>
      {code19 name="Code7" label="Type of employment"}
      <div class="block_position2">{textfield label="Employer/Verifier <br /> Name/Military Duty Location" name="EmployersVerifierNamesMilitaryDutyLocation4"  class="28a_1"}</div>
      <div class="block_position2" id="positionEmp4">{textfield label="Your Position <br /> Title/Military Rank" name="PositionTitlesMilitaryRank4"  class="15a_1"}</div>
      
    </div>
    <div id="empblock4" {if $smarty.post.unemp4} style="display:none;" {/if}>
    <div class="set-default clearer">                    
      {textfield label="Employer's/Verifier's Street Address" name="EmployersVerifierStreetAddress4"  class="44a"}
      {textfield label="City (Country)" name="EmployersVerifierCityCountry4"  class="12_1"}
      {states name="EmployersVerifierState4" label="State" class="8_1"}
      {textfield label="ZipCode" name="EmployersVerifierZIPCode4"  class="7_1"}
      <span class="stext6  {if $errorMessages.AreaCodeOfEmployersVerifier4 || $errorMessages.PhonenOfEmployersVerifier4}item-error{/if}">Telephone Number</span>
      {textfield label="" name="AreaCodeOfEmployersVerifier4"  class="4_1"}
      {textfield label="" name="PhonenOfEmployersVerifier4"  class="11_a"}
    </div>
    <div class="set-default clearer">                    
      {textfield label="Street Address of Job Location <em>if different than Employer's Address</em>" name="StreetAddressOfJobLocation4"  class="44a"}
      {textfield label="City (Country)" name="CityCountryOfJobLocation4"  class="12_1"}
      {states name="StateOfJobLocation4" label="State" class="8r_1" }
      {textfield label="ZipCode" name="ZIPCodeOfJobLocation4"  class="7_1"}
      <span class="stext6 {if $errorMessages.AreaCodeOfJobLocation4 || $errorMessages.PhonenOfJobLocation4}item-error{/if} ">Telephone Number</span>
      {textfield label="" name="AreaCodeOfJobLocation4"  class="4_1"}
      {textfield label="" name="PhonenOfJobLocation4"  class="11_a"}
    </div>
    <div class="set-default clearer">                    
      {textfield label="Supervisor's Name & Street Address <em>if different than Job Location</em>" name="SupervisorNamepStreetAddress4"  class="44a"}
      {textfield label="City (Country)" name="CityCountryOfSupervisor4"  class="12_1"}
      {states name="StateOfSupervisor4" label="State" class="8r_1" }
      {textfield label="ZipCode" name="ZIPCodeOfSupervisor4"  class="7_1"}
      <span class="stext6  {if $errorMessages.AreaCodeOfSupervisor4 || $errorMessages.PhonenOfSupervisor4}item-error{/if} ">Telephone Number</span>
      {textfield label="" name="AreaCodeOfSupervisor4"  class="4_1"}
      {textfield label="" name="PhonenOfSupervisor4"  class="11_a"}
    </div>
    <div class="set-default clearer">
      <div class="previous_period fl">Previous period</div>
      <div class="fl">
        <div class="set-default clearer">  
          {monthyear name="DateFrom26" label="From" class="10-45_1"}
          {monthyear name="DateTo26" label="To" class="10_1-45_1"}
          {textfield label="Position Title" name="PositionTitle10"  class="16_1"}
          {textfield label="Supervisor" name="Supervisor10"  class="28a_1"}
        </div>
        <div class="set-default clearer">
          {monthyear name="DateFrom27" label="From" class="10-45_1"}
          {monthyear name="DateTo27" label="To" class="10_1-45_1"}
          {textfield label="Position Title" name="PositionTitle11"  class="16_1"}
          {textfield label="Supervisor" name="Supervisor11"  class="28a_1"}
        </div> 
        <div class="set-default clearer">
          {monthyear name="DateFrom28" label="From" class="10-45_1"}
          {monthyear name="DateTo28" label="To" class="10_1-45_1"}
          {textfield label="Position Title" name="PositionTitle12"  class="16_1"}
          {textfield label="Supervisor" name="Supervisor12"  class="28a_1"}
        </div>
      </div>
    </div>
    </div>
  </div>
  <div class="area_w">
    <div class="set-default clearer">
      <div class="item-number fl">#5</div>
      <input type="checkbox"  onclick="unemployment(5)"  name="unemp5" value="5" id="unemp5" {if $smarty.post.unemp5} checked="checked"{/if}  /><strong>Unemployment Period</strong><br />    
    </div>
    <div class="set-default clearer">
      {monthyear name="DateFrom29" onchange="fillTo('DateFrom29', 'DateTo33')" label="From" class="10-45_1"}
      <div class="present1 fl"><span class="stext6">To</span><br /><p id="DateTo29" class="for_date_to">{if $smarty.post.MonthDateFrom25 &&  $smarty.post.YearDateFrom25}{$smarty.post.MonthDateFrom25}/{$smarty.post.YearDateFrom25}{/if}</p></div>
      {code19 name="Code8" label="Type of employment"}
      <div class="block_position2">{textfield label="Employer/Verifier <br /> Name/Military Duty Location" name="EmployersVerifierNamesMilitaryDutyLocation5"  class="28a_1"}</div>
      <div class="block_position2" id="positionEmp5">{textfield label="Your Position <br /> Title/Military Rank" name="PositionTitlesMilitaryRank5"  class="15a_1"}</div>
      
    </div>
    <div id="empblock5" {if $smarty.post.unemp5} style="display:none;" {/if}>
    <div class="set-default clearer">                    
      {textfield label="Employer's/Verifier's Street Address" name="EmployersVerifierStreetAddress5"  class="44a"}
      {textfield label="City (Country)" name="EmployersVerifierCityCountry5"  class="12_1"}
      {states name="EmployersVerifierState5" label="State" class="8_1"}
      {textfield label="ZipCode" name="EmployersVerifierZIPCode5"  class="7_1"}
      <span class="stext6  {if $errorMessages.AreaCodeOfEmployersVerifier5 || $errorMessages.PhonenOfEmployersVerifier5}item-error{/if}">Telephone Number</span>
      {textfield label="" name="AreaCodeOfEmployersVerifier5"  class="4_1"}
      {textfield label="" name="PhonenOfEmployersVerifier5"  class="11_a"}
    </div>
    <div class="set-default clearer">                    
      {textfield label="Street Address of Job Location <em>if different than Employer's Address</em>" name="StreetAddressOfJobLocation5"  class="44a"}
      {textfield label="City (Country)" name="CityCountryOfJobLocation5"  class="12_1"}
      {states name="StateOfJobLocation5" label="State" class="8r_1" }
      {textfield label="ZipCode" name="ZIPCodeOfJobLocation5"  class="7_1"}
      <span class="stext6 {if $errorMessages.AreaCodeOfJobLocation5 || $errorMessages.PhonenOfJobLocation5}item-error{/if} ">Telephone Number</span>
      {textfield label="" name="AreaCodeOfJobLocation5"  class="4_1"}
      {textfield label="" name="PhonenOfJobLocation5"  class="11_a"}
    </div>
    <div class="set-default clearer">                    
      {textfield label="Supervisor's Name & Street Address <em>if different than Job Location</em>" name="SupervisorNamepStreetAddress5"  class="44a"}
      {textfield label="City (Country)" name="CityCountryOfSupervisor5"  class="12_1"}
      {states name="StateOfSupervisor5" label="State" class="8r_1" }
      {textfield label="ZipCode" name="ZIPCodeOfSupervisor5"  class="7_1"}
      <span class="stext6  {if $errorMessages.AreaCodeOfSupervisor5 || $errorMessages.PhonenOfSupervisor5}item-error{/if} ">Telephone Number</span>
      {textfield label="" name="AreaCodeOfSupervisor5"  class="4_1"}
      {textfield label="" name="PhonenOfSupervisor5"  class="11_a"}
    </div>
    <div  class="set-default clearer">
      <div class="previous_period fl">Previous period</div>
      <div class="fl">
        <div class="set-default clearer">  
          {monthyear name="DateFrom30" label="From" class="10-45_1"}
          {monthyear name="DateTo30" label="To" class="10_1-45_1"}
          {textfield label="Position Title" name="PositionTitle13"  class="16_1"}
          {textfield label="Supervisor" name="Supervisor13"  class="28a_1"}
        </div>
        <div class="set-default clearer">
          {monthyear name="DateFrom31" label="From" class="10-45_1"}
          {monthyear name="DateTo31" label="To" class="10_1-45_1"}
          {textfield label="Position Title" name="PositionTitle14"  class="16_1"}
          {textfield label="Supervisor" name="Supervisor14"  class="28a_1"}
        </div> 
        <div class="set-default clearer">
          {monthyear name="DateFrom32" label="From" class="10-45_1"}
          {monthyear name="DateTo32" label="To" class="10_1-45_1"}
          {textfield label="Position Title" name="PositionTitle15"  class="16_1"}
          {textfield label="Supervisor" name="Supervisor15"  class="28a_1"}
        </div>
      </div>
    </div>
    </div>
  </div>
  
  
  
  <div class="area">
    <div class="set-default clearer">
      <div class="item-number fl">#6</div>
      <input type="checkbox"  onclick="unemployment(6)"  name="unemp6" value="6" id="unemp6" {if $smarty.post.unemp6} checked="checked"{/if} /><strong>Unemployment Period</strong><br />    
    </div>
    <div class="set-default clearer">
      {monthyear name="DateFrom33" onchange="fillTo('DateFrom33', 'aDateTo13')" label="From" class="10-45_1"}
      <div class="present1 fl"><span class="stext6">To</span><br /><p id="DateTo33" class="for_date_to">{if $smarty.post.MonthDateFrom29 &&  $smarty.post.YearDateFrom29}{$smarty.post.MonthDateFrom29}/{$smarty.post.YearDateFrom29}{/if}</p></div>
      {code19 name="Code9" label="Type of employment"}
      <div class="block_position2">{textfield label="Employer/Verifier <br /> Name/Military Duty Location" name="EmployersVerifierNamesMilitaryDutyLocation6"  class="28a_1"}</div>
      <div class="block_position2" id="positionEmp6">{textfield label="Your Position <br /> Title/Military Rank" name="PositionTitlesMilitaryRank6"  class="15a_1"}</div>
      
    </div>
    <div id="empblock6" {if $smarty.post.unemp6} style="display:none;" {/if}>
    <div class="set-default clearer">                    
      {textfield label="Employer's/Verifier's Street Address" name="EmployersVerifierStreetAddress6"  class="44a"}
      {textfield label="City (Country)" name="EmployersVerifierCityCountry6"  class="12_1"}
      {states name="EmployersVerifierState6" label="State" class="8_1"}
      {textfield label="ZipCode" name="EmployersVerifierZIPCode6"  class="7_1"}
      <span class="stext6  {if $errorMessages.AreaCodeOfEmployersVerifier6 || $errorMessages.PhonenOfEmployersVerifier6}item-error{/if} ">Telephone Number</span>
      {textfield label="" name="AreaCodeOfEmployersVerifier6"  class="4_1"}
      {textfield label="" name="PhonenOfEmployersVerifier6"  class="11_a"}
    </div>
    <div class="set-default clearer">                    
      {textfield label="Street Address of Job Location <em>if different than Employer's Address</em>" name="StreetAddressOfJobLocation6"  class="44a"}
      {textfield label="City (Country)" name="CityCountryOfJobLocation6"  class="12_1"}
      {states name="StateOfJobLocation6" label="State" class="8r_1" }
      {textfield label="ZipCode" name="ZIPCodeOfJobLocation6"  class="7_1"}
      <span class="stext6  {if $errorMessages.AreaCodeOfJobLocation6 || $errorMessages.PhonenOfJobLocation6}item-error{/if}">Telephone Number</span>
      {textfield label="" name="AreaCodeOfJobLocation6"  class="4_1"}
      {textfield label="" name="PhonenOfJobLocation6"  class="11_a"}
    </div>
    <div class="set-default clearer">                    
      {textfield label="Supervisor's Name & Street Address <em>if different than Job Location</em>" name="SupervisorNamepStreetAddress6"  class="44a"}
      {textfield label="City (Country)" name="CityCountryOfSupervisor6"  class="12_1"}
      {states name="StateOfSupervisor6" label="State" class="8r_1" }
      {textfield label="ZipCode" name="ZIPCodeOfSupervisor6"  class="7_1"}
      <span class="stext6  {if $errorMessages.AreaCodeOfSupervisor6 || $errorMessages.PhonenOfSupervisor6}item-error{/if} ">Telephone Number</span>
      {textfield label="" name="AreaCodeOfSupervisor6"  class="4_1"}
      {textfield label="" name="PhonenOfSupervisor6"  class="11_a"}
    </div>
    <div class="set-default clearer">
      <div class="previous_period fl">Previous period</div>
      <div class="fl">
        <div class="set-default clearer">  
          {monthyear name="DateFrom34" label="From" class="10-45_1"}
          {monthyear name="DateTo34" label="To" class="10_1-45_1"}
          {textfield label="Position Title" name="PositionTitle16"  class="16_1"}
          {textfield label="Supervisor" name="Supervisor16"  class="28a_1"}
        </div>
        <div class="set-default clearer">
          {monthyear name="DateFrom35" label="From" class="10-45_1"}
          {monthyear name="DateTo35" label="To" class="10_1-45_1"}
          {textfield label="Position Title" name="PositionTitle17"  class="16_1"}
          {textfield label="Supervisor" name="Supervisor17"  class="28a_1"}
        </div> 
        <div class="set-default clearer">
          {monthyear name="DateFrom36" label="From" class="10-45_1"}
          {monthyear name="DateTo36" label="To" class="10_1-45_1"}
          {textfield label="Position Title" name="PositionTitle18"  class="16_1"}
          {textfield label="Supervisor" name="Supervisor18"  class="28a_1"}
        </div>
      </div>
    </div>
    </div>
  </div>
  <a href="javascript:showadd(10);" {if $smarty.post.add10} style="display:none;" {/if} id="a10show">More</a>
  <div {if !$smarty.post.add10} style="display:none;" {/if} id="a10"  >
    
   Enter more fields: 
    <fieldset>
      <div class="area_w">
        <div class="set-default clearer">
          <div class="item-number fl">#7</div>
          <input type="checkbox"  onclick="unemployment(7)"  name="unemp7" value="7" id="unemp7" {if $smarty.post.unemp7} checked="checked"{/if}  /><strong>Unemployment Period</strong><br />    
        </div>
        <div class="set-default clearer">
          {monthyear name="aDateFrom13" onchange="fillTo('aDateFrom13', 'aDateTo17')" label="From" class="10-45_1"}
          <div class="present1 fl"><span class="stext6">To</span><br /><p id="aDateTo13" class="for_date_to">{if $smarty.post.MonthDateFrom33 &&  $smarty.post.YearDateFrom33}{$smarty.post.MonthDateFrom33}/{$smarty.post.YearDateFrom33}{/if}</p></div>
          {code19 name="aCode4" label="Type of employment"}
          <div class="block_position2">{textfield label="Employer/Verifier <br /> Name/Military Duty Location" name="aEmployersVerifierNamesMilitaryDutyLocation1"  class="28a_1"}</div>
          <div class="block_position2" id="positionEmp7">{textfield label="Your Position <br /> Title/Military Rank" name="aPositionTitlesMilitaryRank"  class="15a_1"}</div>
          
        </div>
    <div id="empblock7" {if $smarty.post.unemp7} style="display:none;" {/if}>
        <div class="set-default clearer">                    
          {textfield label="Employer's/Verifier's Street Address" name="aEmployersVerifierStreetAddress1"  class="44a"}
          {textfield label="City (Country)" name="aEmployersVerifierCityCountry1"  class="12_1"}
          {states name="aEmployersVerifierState1" label="State" class="8_1"}
          {textfield label="ZipCode" name="aEmployersVerifierZIPCode1"  class="7_1"}
          <span class="stext6  {if $errorMessages.aAreaCodeOfEmployersVerifier1 || $errorMessages.aPhonenOfEmployersVerifier1}item-error{/if} ">Telephone Number</span>
          {textfield label="" name="aAreaCodeOfEmployersVerifier1"  class="4_1"}
          {textfield label="" name="aPhonenOfEmployersVerifier1"  class="11_a"}
        </div>
        <div class="set-default clearer">                    
          {textfield label="Street Address of Job Location <em>if different than Employer's Address</em>" name="aStreetAddressOfJobLocation1"  class="44a"}
          {textfield label="City (Country)" name="aCityCountryOfJobLocation1"  class="12_1"}
          {states name="aStateOfJobLocation1" label="State" class="8r_1" }
          {textfield label="ZipCode" name="aZIPCodeOfJobLocation1"  class="7_1"}
          <span class="stext6  {if $errorMessages.aAreaCodeOfJobLocation1 || $errorMessages.aPhonenOfJobLocation1}item-error{/if}">Telephone Number</span>
          {textfield label="" name="aAreaCodeOfJobLocation1"  class="4_1"}
          {textfield label="" name="aPhonenOfJobLocation1"  class="11_a"}
        </div>
        <div class="set-default clearer">                    
          {textfield label="Supervisor's Name & Street Address <em>if different than Job Location</em>" name="aSupervisorNamepStreetAddress1"  class="44a"}
          {textfield label="City (Country)" name="aCityCountryOfSupervisor1"  class="12_1"}
          {states name="aStateOfSupervisor1" label="State" class="8r_1" }
          {textfield label="ZipCode" name="aZIPCodeOfSupervisor1"  class="7_1"}
          <span class="stext6  {if $errorMessages.aAreaCodeOfSupervisor1 || $errorMessages.aPhonenOfSupervisor1}item-error{/if} ">Telephone Number</span>
          {textfield label="" name="aAreaCodeOfSupervisor1"  class="4_1"}
          {textfield label="" name="aPhonenOfSupervisor1"  class="11_a"}
        </div>
        <div  class="set-default clearer">
          <div class="previous_period fl">Previous period</div>
          <div class="fl">
            <div class="set-default clearer">  
              {monthyear name="aDateFrom14" label="From" class="10-45_1"}
              {monthyear name="aDateTo14" label="To" class="10_1-45_1"}
              {textfield label="Position Title" name="aPositionTitle1"  class="16_1"}
              {textfield label="Supervisor" name="aSupervisor1"  class="28a_1"}
            </div>
            <div class="set-default clearer">
              {monthyear name="aDateFrom15" label="From" class="10-45_1"}
              {monthyear name="aDateTo15" label="To" class="10_1-45_1"}
              {textfield label="Position Title" name="aPositionTitle2"  class="16_1"}
              {textfield label="Supervisor" name="aSupervisor2"  class="28a_1"}
            </div> 
            <div class="set-default clearer">
              {monthyear name="aDateFrom16" label="From" class="10-45_1"}
              {monthyear name="aDateTo16" label="To" class="10_1-45_1"}
              {textfield label="Position Title" name="aPositionTitle3"  class="16_1"}
              {textfield label="Supervisor" name="aSupervisor3"  class="28a_1"}
            </div>
          </div>
        </div>
    </div>
      </div>  
      <div class="area">
        <div class="set-default clearer">
          <div class="item-number fl">#8</div>
          <input type="checkbox"  onclick="unemployment(8)"  name="unemp8" value="8" id="unemp8" {if $smarty.post.unemp8} checked="checked"{/if}  /><strong>Unemployment Period</strong><br />    
        </div>
        <div class="set-default clearer">
          {monthyear name="aDateFrom17" onchange="fillTo('aDateFrom17', 'aDateTo21')" label="From" class="10-45_1"}
          <div class="present1 fl"><span class="stext6">To</span><br /><p id="aDateTo17" class="for_date_to">{if $smarty.post.MonthDateFrom13 &&  $smarty.post.aYearDateFrom13}{$smarty.post.aMonthDateFrom13}/{$smarty.post.YearDateFrom13}{/if}</p></div>
          {code19 name="aCode5" label="Type of employment"}
          <div class="block_position2">{textfield label="Employer/Verifier <br /> Name/Military Duty Location" name="aEmployersVerifierNamesMilitaryDutyLocation2"  class="28a_1"}</div>
          <div class="block_position2" id="positionEmp8">{textfield label="Your Position <br /> Title/Military Rank" name="aPositionTitlesMilitaryRank2"  class="15a_1"}</div>
          
        </div>
    <div id="empblock8" {if $smarty.post.unemp8} style="display:none;" {/if}>
        <div class="set-default clearer">                    
          {textfield label="Employer's/Verifier's Street Address" name="aEmployersVerifierStreetAddress2"  class="44a"}
          {textfield label="City (Country)" name="aEmployersVerifierCityCountry2"  class="12_1"}
          {states name="aEmployersVerifierState2" label="State" class="8_1"}
          {textfield label="ZipCode" name="aEmployersVerifierZIPCode2"  class="7_1"}
          <span class="stext6  {if $errorMessages.aAreaCodeOfEmployersVerifier2 || $errorMessages.aPhonenOfEmployersVerifier2}item-error{/if} ">Telephone Number</span>
          {textfield label="" name="aAreaCodeOfEmployersVerifier2"  class="4_1"}
          {textfield label="" name="aPhonenOfEmployersVerifier2"  class="11_a"}
        </div>
        <div class="set-default clearer">                    
          {textfield label="Street Address of Job Location <em>if different than Employer's Address</em>" name="aStreetAddressOfJobLocation2"  class="44a"}
          {textfield label="City (Country)" name="aCityCountryOfJobLocation2"  class="12_1"}
          {states name="aStateOfJobLocation2" label="State" class="8r_1" }
          {textfield label="ZipCode" name="aZIPCodeOfJobLocation2"  class="7_1"}
          <span class="stext6 {if $errorMessages.aAreaCodeOfJobLocation2 || $errorMessages.aPhonenOfJobLocation2}item-error{/if} ">Telephone Number</span>
          {textfield label="" name="aAreaCodeOfJobLocation2"  class="4_1"}
          {textfield label="" name="aPhonenOfJobLocation2"  class="11_a"}
        </div>
        <div class="set-default clearer">                    
          {textfield label="Supervisor's Name & Street Address <em>if different than Job Location</em>" name="aSupervisorNamepStreetAddress2"  class="44a"}
          {textfield label="City (Country)" name="aCityCountryOfSupervisor2"  class="12_1"}
          {states name="aStateOfSupervisor2" label="State" class="8r_1" }
          {textfield label="ZipCode" name="aZIPCodeOfSupervisor2"  class="7_1"}
          <span class="stext6  {if $errorMessages.aAreaCodeOfSupervisor2 || $errorMessages.aPhonenOfSupervisor2}item-error{/if} ">Telephone Number</span>
          {textfield label="" name="aAreaCodeOfSupervisor2"  class="4_1"}
          {textfield label="" name="aPhonenOfSupervisor2"  class="11_a"}
        </div>
        <div class="set-default clearer">
          <div class="previous_period fl">Previous period</div>
          <div class="fl">
            <div class="set-default clearer">  
              {monthyear name="aDateFrom18" label="From" class="10-45_1"}
              {monthyear name="aDateTo18" label="To" class="10_1-45_1"}
              {textfield label="Position Title" name="aPositionTitle4"  class="16_1"}
              {textfield label="Supervisor" name="aSupervisor4"  class="28a_1"}
            </div>
            <div class="set-default clearer">
              {monthyear name="aDateFrom19" label="From" class="10-45_1"}
              {monthyear name="aDateTo19" label="To" class="10_1-45_1"}
              {textfield label="Position Title" name="aPositionTitle5"  class="16_1"}
              {textfield label="Supervisor" name="aSupervisor5"  class="28a_1"}
            </div> 
            <div class="set-default clearer">
              {monthyear name="aDateFrom20" label="From" class="10-45_1"}
              {monthyear name="aDateTo20" label="To" class="10_1-45_1"}
              {textfield label="Position Title" name="aPositionTitle6"  class="16_1"}
              {textfield label="Supervisor" name="aSupervisor6"  class="28a_1"}
            </div>
          </div>
        </div>
    </div>
      </div>
      <div class="area_w">
        <div class="set-default clearer">
          <div class="item-number fl">#9</div>
          <input type="checkbox"  onclick="unemployment(9)"  name="unemp9" value="9" id="unemp9" {if $smarty.post.unemp9} checked="checked"{/if}  /><strong>Unemployment Period</strong><br />    
        </div>
        <div class="set-default clearer">
          {monthyear name="aDateFrom21" onchange="fillTo('aDateFrom21', 'aDateTo25')" label="From" class="10-45_1"}
          <div class="present1 fl"><span class="stext6">To</span><br /><p id="aDateTo21" class="for_date_to">{if $smarty.post.aMonthDateFrom17 &&  $smarty.post.aYearDateFrom17}{$smarty.post.MonthDateFrom17}/{$smarty.post.YearDateFrom17}{/if}</p></div>
          {code19 name="aCode6" label="Type of employment"}
          <div class="block_position2">{textfield label="Employer/Verifier <br /> Name/Military Duty Location" name="aEmployersVerifierNamesMilitaryDutyLocation3"  class="28a_1"}</div>
          <div class="block_position2" id="positionEmp9">{textfield label="Your Position <br /> Title/Military Rank" name="aPositionTitlesMilitaryRank3"  class="15a_1"}</div>
          
        </div>
    <div id="empblock9" {if $smarty.post.unemp9} style="display:none;" {/if}>
        <div class="set-default clearer">                    
          {textfield label="Employer's/Verifier's Street Address" name="aEmployersVerifierStreetAddress3"  class="44a"}
          {textfield label="City (Country)" name="aEmployersVerifierCityCountry3"  class="12_1"}
          {states name="aEmployersVerifierState3" label="State" class="8_1"}
          {textfield label="ZipCode" name="aEmployersVerifierZIPCode3"  class="7_1"}
          <span class="stext6  {if $errorMessages.aAreaCodeOfEmployersVerifier3 || $errorMessages.aPhonenOfEmployersVerifier3}item-error{/if}">Telephone Number</span>
          {textfield label="" name="aAreaCodeOfEmployersVerifier3"  class="4_1"}
          {textfield label="" name="aPhonenOfEmployersVerifier3"  class="11_a"}
        </div>
        <div class="set-default clearer">                    
          {textfield label="Street Address of Job Location <em>if different than Employer's Address</em>" name="aStreetAddressOfJobLocation3"  class="44a"}
          {textfield label="City (Country)" name="aCityCountryOfJobLocation3"  class="12_1"}
          {states name="aStateOfJobLocation3" label="State" class="8r_1" }
          {textfield label="ZipCode" name="aZIPCodeOfJobLocation3"  class="7_1"}
          <span class="stext6 {if $errorMessages.aAreaCodeOfJobLocation3 || $errorMessages.aPhonenOfJobLocation3}item-error{/if} ">Telephone Number</span>
          {textfield label="" name="aAreaCodeOfJobLocation3"  class="4_1"}
          {textfield label="" name="aPhonenOfJobLocation3"  class="11_a"}
        </div>
        <div class="set-default clearer">                    
          {textfield label="Supervisor's Name & Street Address <em>if different than Job Location</em>" name="aSupervisorNamepStreetAddress3"  class="44a"}
          {textfield label="City (Country)" name="aCityCountryOfSupervisor3"  class="12_1"}
          {states name="aStateOfSupervisor3" label="State" class="8r_1" }
          {textfield label="ZipCode" name="aZIPCodeOfSupervisor3"  class="7_1"}
          <span class="stext6  {if $errorMessages.aAreaCodeOfSupervisor3 || $errorMessages.aPhonenOfSupervisor3}item-error{/if} ">Telephone Number</span>
          {textfield label="" name="aAreaCodeOfSupervisor3"  class="4_1"}
          {textfield label="" name="aPhonenOfSupervisor3"  class="11_a"}
        </div>
        <div  class="set-default clearer">
          <div class="previous_period fl">Previous period</div>
          <div class="fl">
            <div class="set-default clearer">  
              {monthyear name="aDateFrom22" label="From" class="10-45_1"}
              {monthyear name="aDateTo22" label="To" class="10_1-45_1"}
              {textfield label="Position Title" name="aPositionTitle7"  class="16_1"}
              {textfield label="Supervisor" name="aSupervisor7"  class="28a_1"}
            </div>
            <div class="set-default clearer">
              {monthyear name="aDateFrom23" label="From" class="10-45_1"}
              {monthyear name="aDateTo23" label="To" class="10_1-45_1"}
              {textfield label="Position Title" name="aPositionTitle8"  class="16_1"}
              {textfield label="Supervisor" name="aSupervisor8"  class="28a_1"}
            </div> 
            <div class="set-default clearer">
              {monthyear name="aDateFrom24" label="From" class="10-45_1"}
              {monthyear name="aDateTo24" label="To" class="10_1-45_1"}
              {textfield label="Position Title" name="aPositionTitle9"  class="16_1"}
              {textfield label="Supervisor" name="aSupervisor9"  class="28a_1"}
            </div>
          </div>
        </div>
    </div>
      </div>  
      <div class="area">
        <div class="set-default clearer">
          <div class="item-number fl">#10</div>
          <input type="checkbox"  onclick="unemployment(10)"  name="unemp10" value="10" id="unemp10" {if $smarty.post.unemp10} checked="checked"{/if}  /><strong>Unemployment Period</strong><br />    
        </div>
        <div class="set-default clearer">
          {monthyear name="aDateFrom25" label="From" class="10-45_1"}
          <div class="present1 fl"><span class="stext6">To</span><br /><p id="aDateTo25" class="for_date_to">{if $smarty.post.aMonthDateFrom21 &&  $smarty.post.aYearDateFrom21}{$smarty.post.MonthDateFrom21}/{$smarty.post.YearDateFrom21}{/if}</p></div>
          {code19 name="aCode7" label="Type of employment"}
          <div class="block_position2">{textfield label="Employer/Verifier <br /> Name/Military Duty Location" name="aEmployersVerifierNamesMilitaryDutyLocation4"  class="28a_1"}</div>
          <div class="block_position2" id="positionEmp10">{textfield label="Your Position <br /> Title/Military Rank" name="aPositionTitlesMilitaryRank4"  class="15a_1"}</div>
          
        </div>
    <div id="empblock10" {if $smarty.post.unemp10} style="display:none;" {/if}>
        <div class="set-default clearer">                    
          {textfield label="Employer's/Verifier's Street Address" name="aEmployersVerifierStreetAddress4"  class="44a"}
          {textfield label="City (Country)" name="aEmployersVerifierCityCountry4"  class="12_1"}
          {states name="aEmployersVerifierState4" label="State" class="8_1"}
          {textfield label="ZipCode" name="aEmployersVerifierZIPCode4"  class="7_1"}
          <span class="stext6   {if $errorMessages.aAreaCodeOfEmployersVerifier4 || $errorMessages.aPhonenOfEmployersVerifier4}item-error{/if}">Telephone Number</span>
          {textfield label="" name="aAreaCodeOfEmployersVerifier4"  class="4_1"}
          {textfield label="" name="aPhonenOfEmployersVerifier4"  class="11_a"}
        </div>
        <div class="set-default clearer">                    
          {textfield label="Street Address of Job Location <em>if different than Employer's Address</em>" name="aStreetAddressOfJobLocation4"  class="44a"}
          {textfield label="City (Country)" name="aCityCountryOfJobLocation4"  class="12_1"}
          {states name="aStateOfJobLocation4" label="State" class="8r_1" }
          {textfield label="ZipCode" name="aZIPCodeOfJobLocation4"  class="7_1"}
          <span class="stext6 {if $errorMessages.aAreaCodeOfJobLocation4 || $errorMessages.aPhonenOfJobLocation4}item-error{/if} ">Telephone Number</span>
          {textfield label="" name="aAreaCodeOfJobLocation4"  class="4_1"}
          {textfield label="" name="aPhonenOfJobLocation4"  class="11_a"}
        </div>
        <div class="set-default clearer">                    
          {textfield label="Supervisor's Name & Street Address <em>if different than Job Location</em>" name="aSupervisorNamepStreetAddress4"  class="44a"}
          {textfield label="City (Country)" name="aCityCountryOfSupervisor4"  class="12_1"}
          {states name="aStateOfSupervisor4" label="State" class="8r_1" }
          {textfield label="ZipCode" name="aZIPCodeOfSupervisor4"  class="7_1"}
          <span class="stext6  {if $errorMessages.aAreaCodeOfSupervisor4 || $errorMessages.aPhonenOfSupervisor4}item-error{/if} ">Telephone Number</span>
          {textfield label="" name="aAreaCodeOfSupervisor4"  class="4_1"}
          {textfield label="" name="aPhonenOfSupervisor4"  class="11_a"}
        </div>
        <div class="set-default clearer">
          <div class="previous_period fl">Previous period</div>
          <div class="fl">
            <div class="set-default clearer">  
              {monthyear name="aDateFrom26" label="From" class="10-45_1"}
              {monthyear name="aDateTo26" label="To" class="10_1-45_1"}
              {textfield label="Position Title" name="aPositionTitle10"  class="16_1"}
              {textfield label="Supervisor" name="aSupervisor10"  class="28a_1"}
            </div>
            <div class="set-default clearer">
              {monthyear name="aDateFrom27" label="From" class="10-45_1"}
              {monthyear name="aDateTo27" label="To" class="10_1-45_1"}
              {textfield label="Position Title" name="aPositionTitle11"  class="16_1"}
              {textfield label="Supervisor" name="aSupervisor11"  class="28a_1"}
            </div> 
            <div class="set-default clearer">
              {monthyear name="aDateFrom28" label="From" class="10-45_1"}
              {monthyear name="aDateTo28" label="To" class="10_1-45_1"}
              {textfield label="Position Title" name="aPositionTitle12"  class="16_1"}
              {textfield label="Supervisor" name="aSupervisor12"  class="28a_1"}
            </div>
          </div>
        </div>
    </div>
      </div>
    </fieldset>
    
    <a href="javascript:hideadd(10);" id="a10hide">Hide More</a>
<input type="hidden" name="add10" value="{$smarty.post.add10}" id="add10" value="0" />
</div>
<!-- ----------------------------------------------------    11     ------------------------------------------------ -->
<fieldset><legend><span>11.</span> PEOPLE WHO KNOW YOU WELL</legend>
            <p class="note">List three people who know you well and live in the United States. 
            They should be good friends, peers, colleagues, college roommates, etc., whose combined association with you covers as well as possible the last 5 years.
            Do not list your spouse, former spouses, or other relatives, and try not to list anyone who is listed elsewhere on this form.</p>       
            <div class="area">
                <div class="set-default clearer">
                    <div class="item-number fl">#1</div>
                    {textfield label="Name" name="NameOfPersonWhoKnowYouWell1"  class="30"}
                    {monthyear name="DateKnownFrom1" label="Dates Known" class="06a_1a-45_1"}
                    {monthyear name="DateKnownTo1" label="To" class="6a_1a-45_1"}                   
                    <span class="stext1 {if $errorMessages.time1}item-error{/if} {if $errorMessages.AreaCodeOfPerson1}item-error{/if} {if $errorMessages.PhonenOfPerson1}item-error{/if}">Telephone Number</span>
                    <div class="for_radio3 fl left_marg1a ">{radiovertical name="time1" labels="Day|Night" values="DaytimePhone1|NighttimePhone1" class="3"}</div>
                    {textfield label="" name="AreaCodeOfPerson1"  class="4"}
                    {textfield label="" name="PhonenOfPerson1"  class="16a_a"}
                </div>
                <div class="set-default clearer left_marg2a">
                    {textfield label="Home or Work Address" name="HomesWorkAddressOfPersonWhoKnowYouWell1"  class="43a"}
                    {textfield label="City(Country)" name="CityCountryOfPersonPersonWhoKnowYouWell1"  class="19_1"}
                    {states name="StateOfPersonWhoKnowYouWell1" label="State" class="8_1a"}
                    {textfield label="Zip Code" name="ZIPCodeOfPersonWhoKnowYouWell1"  class="12_1a"}
                </div>
            </div>
            <div class="set-default clearer">
                <div class="item-number fl">#2</div>
                {textfield label="Name" name="NameOfPersonWhoKnowYouWell2"  class="30"}
                {monthyear name="DateKnownFrom2" label="Dates Known" class="06a_1a-45_1"}
                {monthyear name="DateKnownTo2" label="To" class="6a_1a-45_1"}                   
                <span class="stext1 {if $errorMessages.time2}item-error{/if} {if $errorMessages.AreaCodeOfPerson2}item-error{/if} 
                {if $errorMessages.PhonenOfPerson2}item-error{/if}">Telephone Number</span>
                <div class="for_radio3 fl left_marg1a ">{radiovertical name="time2" labels="Day|Night" values="DaytimePhone2|NighttimePhone2" class="3"}</div>
                {textfield label="" name="AreaCodeOfPerson2"  class="4"}
                {textfield label="" name="PhonenOfPerson2"  class="16a_a"}
            </div>
            <div class="set-default clearer left_marg2a">
                {textfield label="Home or Work Address" name="HomesWorkAddressOfPersonWhoKnowYouWell2"  class="43a"}
                {textfield label="City(Country)" name="CityCountryOfPersonPersonWhoKnowYouWell2"  class="19_1"}
                {states name="StateOfPersonWhoKnowYouWell2" label="State" class="8_1a"}
                {textfield label="Zip Code" name="ZIPCodeOfPersonWhoKnowYouWell2"  class="12_1a"}
            </div>
            <div class="area">
                <div class="set-default clearer">
                    <div class="item-number fl">#3</div>
                    {textfield label="Name" name="NameOfPersonWhoKnowYouWell3"  class="30"}
                    {monthyear name="DateKnownFrom3" label="Dates Known" class="06a_1a-45_1"}
                    {monthyear name="DateKnownTo3" label="To" class="6a_1a-45_1"}                   
                    <span class="stext1 {if $errorMessages.time3}item-error{/if} {if $errorMessages.AreaCodeOfPerson3}item-error{/if} {if $errorMessages.PhonenOfPerson1}item-error{/if}">Telephone Number</span>
                    <div class="for_radio3 fl left_marg1a ">{radiovertical name="time3" labels="Day|Night" values="DaytimePhone3|NighttimePhone3" class="3"}</div>
                    {textfield label="" name="AreaCodeOfPerson3"  class="4"}
                    {textfield label="" name="PhonenOfPerson3"  class="16a_a"}
                </div>
                <div class="set-default clearer left_marg2a">
                    {textfield label="Home or Work Address" name="HomesWorkAddressOfPersonWhoKnowYouWell3"  class="43a"}
                    {textfield label="City(Country)" name="CityCountryOfPersonPersonWhoKnowYouWell3"  class="19_1"}
                    {states name="StateOfPersonWhoKnowYouWell3" label="State" class="8_1a"}
                    {textfield label="Zip Code" name="ZIPCodeOfPersonWhoKnowYouWell3"  class="12_1a"}
                </div>
            </div>
        </fieldset>
        <fieldset><legend><span>12.</span> YOUR SELECTIVE SERVICE RECORD <span id="selServiceLabel"></span></legend>
            <ol class="set-nolabel list-lalpha" id="selService">
                
                <li>
                    <span class="stext {if $errorMessages.sss}item-error{/if}">Have you registered with the Selective Service System? If "Yes," provide your registration number.
                    If "No," show the reason for your legal exemption below.</span>
                    {radiohorisontal onclick="selServiceYesNo()" name="sss" labels="Yes|No" values="Yes|No"} <br clear="all" />
                    <div id="RegistrationNumberblock" {if $smarty.post.sss != 'Yes'} style="display: none" {/if}>{textfield label="Registration Number" name="RegistrationNumber"  class="23"}</div>
                    <div id="LegalExemptionExplanationblock" {if $smarty.post.sss != 'No'} style="display: none" {/if}>{textfield label="Legal Exemption Explanation" name="LegalExemptionExplanation"  class="65_1a"}</div>
                </li>
            </ol>
        </fieldset>
        <fieldset><legend><span>13.</span> YOUR MILITARY HISTORY</legend>
            <ol class="set-nolabel list-lalpha">
                <li>
                    <span class="stext {if $errorMessages.military}item-error{/if}">Have you served in the United States military?</span>
                    {radiohorisontal name="military" onclick="militaryPeriods()" labels="Yes|No" values="Yes|No"}
                </li>
                <li>
                    <span class="stext {if $errorMessages.marine}item-error{/if}">Have you served in the United States Merchant Marine?</span>
                    {radiohorisontal name="marine" onclick="militaryPeriods()" labels="Yes|No" values="Yes|No"}<br clear="all" />
                    <div id="militaryPeriods1" {if $smarty.post.military != 'Yes' && $smarty.post.marine != 'Yes' } style="display: none" {/if}>
                    <span class="stext disp_b note2_2">List all of your military service below, including service in Reserve, National Guard, and U.S. Merchant Marine. Start with the most recent period 
                    of service (#1) and work backward. If you had a break in service, each separate period should be listed.</span>
                    <span class="stext note2_2a left_marg2a disp_b"><strong>Status.</strong> Choose the appropriate block for the status of your service during the time that you served. If your service was in 
                    the National Guard, use the two-letter code for the state to mark the block.</span>
                    <span class="stext note2_1a left_marg2a disp_b"><strong>Country.</strong> If your service was with other than the U.S. Armed Forces, identify the country for which you served.</span>
                    </div>
                </li>
            </ol>
            <div id="militaryPeriods2" {if $smarty.post.military != 'Yes' && $smarty.post.marine != 'Yes' } style="display: none" {/if}>
            <div class="set-default clearer note2_2">
                <div class="item-number fl">#1</div>
                {monthyear name="DateFrom37" label="From" class="6a-45_1"}
                {monthyear name="DateTo37" label="To" class="6a_1a-45_1"}
                {code17 name="Code10" label="Code:" class="15_1a"}
                {textfield label="Service/Certificate #" name="ServicesCertificaten1"  class="24_2a"}
                <div class="for_radio4 fl left_marg1 ">{radiovertical name="rank1" labels="Officer|Enlisted" values="Officer1|Enlisted1" class="3"}</div>
                <div class="block_position1">{states name="StatusNationalGuard1" label="National<br /> Guard " class="9"}</div>
            </div>
            <div class="set-default clearer left_marg2a">
                {status name="status1" label="Status" class="10a"}
                {textfield label="Country" name="Country1"  class="10a_1a"}
                <!--{textfield label="Branch From Type of Discharge" name=""  class="39_2a"}-->
            </div>
            <div class="area">
                <div class="set-default clearer note2_2">
                    <div class="item-number fl">#2</div>
                    {monthyear name="DateFrom38" label="From" class="6a-45_1"}
                    {monthyear name="DateTo38" label="To" class="6a_1a-45_1"}
                    {code17 name="Code11" label="Code:" class="15_1a"}
                    {textfield label="Service/Certificate #" name="ServicesCertificaten2"  class="24_2a"}
                    <div class="for_radio4 fl left_marg1 ">{radiovertical name="rank2" labels="Officer|Enlisted" values="Officer2|Enlisted2" class="3"}</div>
                    <div class="block_position1">{states name="StatusNationalGuard2" label="National<br /> Guard " class="9"}</div>
                </div>
                <div class="set-default clearer left_marg2a">
                    {status name="status2" label="Status" class="10a"}
                    {textfield label="Country" name="Country2"  class="10a_1a"}
                    <!--{textfield label="Branch From Type of Discharge" name=""  class="39_2a"}-->
                </div>
            </div>
            </div>
        </fieldset>
        <fieldset><legend><span>14.</span> ILLEGAL  DRUGS</legend>
            <div class="note"><p>In the last year, have you used, possessed, supplied, or manufactured illegal drugs? When used without a prescription, illegal drugs 
            include marijuana, cocaine, hashish, narcotics (opium, morphine, codeine, heroin, etc.), stimulants (cocaine, amphetamines, etc.), 
            depressants (barbiturates, methaqualone, tranquilizers, etc.), hallucinogenics (LSD, PCP, etc.). (<strong>NOTE:</strong> Neither your truthful response 
            nor information derived from your response will be used as evidence against you in any subsequent criminal proceeding.)</p></div>
            {radiohorisontal onclick="drugsPeriods()" name="drugs" labels="Yes|No" values="Yes|No"}<br clear="all" />
            <div id="drugsPeriods" {if $smarty.post.drugs != 'Yes'} style="display: none" {/if}>
            <div class="set-default clearer left_marg2a">
                {monthyear name="DateFrom39" label="From" class="6a-45_1"}
                {monthyear name="DateTo39" label="To" class="6a_1a-45_1"}
                {* date name="DateFrom39" label="From" class="6a_1a-45_1 date_short_first" text_class="short" *}
                {* date name="DateTo39" label="To" class="6a_1a-45_1 date_short" text_class="short" *}
                {textfield label="Type of Substance" name="TypeOfSubstance1"  class="18a_2a"}
                {textfield label="Explanation" name="Explanation1"  class="41_2a"}
            </div>
            <div class="set-default clearer left_marg2a">
                {monthyear name="DateFrom40" label="From" class="6a-45_1"}
                {monthyear name="DateTo40" label="To" class="6a_1a-45_1"}
                {* date name="DateFrom40" label="From" class="6a_1a-45_1 date_short_first" text_class="short" *}
                {* date name="DateTo40" label="To" class="6a_1a-45_1 date_short" text_class="short" *}
                {textfield label="Type of Substance" name="TypeOfSubstance2"  class="18a_2a"}
                {textfield label="Explanation" name="Explanation2"  class="41_2a"}
            </div>
            <div class="set-default clearer left_marg2a">
                {monthyear name="DateFrom41" label="From" class="6a-45_1"}
                {monthyear name="DateTo41" label="To" class="6a_1a-45_1"}
                {* date name="DateFrom41" label="From" class="6a_1a-45_1 date_short_first" text_class="short" *}
                {* date name="DateTo41" label="To" class="6a_1a-45_1 date_short" text_class="short" *}
                {textfield label="Type of Substance" name="TypeOfSubstance3"  class="18a_2a"}
                {textfield label="Explanation" name="Explanation3"  class="41_2a"}
            </div>
            </div>
            <div class="area2"><p class="stext">Continuation Space</p></div>     
            <div class="note left_marg2a"><p>Use the space below to continue answers to all other items and any information you would like to add. 
            If more space is needed than is provided below, use a blank sheet(s) of paper. Start each sheet with your name and Social Security number. 
            Before each answer, identify the number of the item.</p></div>
            <div class="set-default clearer left_marg2a">
                {textarea cols="110" rows="16" name="ContinuationSpace"}
            </div>
            <p class="stext5 note2_2a">Certification That My Answers Are True</p>
            <div class="note stext2 left_marg2a"><p>My statements on this form, and any attachments to it, are true, complete, and correct to the best of my knowledge and belief and are 
            made in good faith. I understand that a knowing and willful false statement on this form can be punished by fine or imprisonment or both. 
            (See section 1001 of title 18, United States Code).</p></div>
            <div class="set-default clearer left_marg2a">
                <p class="stext2">Telephone Number</p>
                <p class="stext2 {if $errorMessages.dayarea}item-error{/if} {if $errorMessages.daytel}item-error{/if}">#1 Day</p>
                {textfield label="" name="dayarea"  class="7"}
                {textfield label=" " name="daytel"  class="31_1a"}
            </div>
            <div class="set-default clearer left_marg2a">               
                <p class="stext2 {if $errorMessages.nightarea}item-error{/if} {if $errorMessages.nighttel}item-error{/if}">#2 Night</p>
                {textfield label="" name="nightarea"  class="7"}
                {textfield label=" " name="nighttel"  class="31_1a"}
            </div>
            <p class="stext5 note2_2a">Declaration for Federal Employment</p>
            <div class="note stext note2_2a left_marg2a"><p>For questions 9,10, and 11, your answers should include convictions resulting from a plea of nolo 
            contendere (no contest), but omit (1) traffic fines of $300 or less, (2) any violation of law committed before your 16th birthday, (3) 
            any violation of law committed before your 18th birthday if finally decided in juvenile court or under a Youth Offender law, (4) 
            any conviction set aside under the Federal Youth Corrections Act or similar state law, and (5) any conviction for which the record was 
            expunged under Federal or state law.</p></div>
        </fieldset>
        <fieldset><legend><span>15.</span> MILITARY SERVICE</legend>
            <div id="militaryPeriods3" {if $smarty.post.military != 'Yes' && $smarty.post.marine != 'Yes' } style="display: none" {/if}>
            <div class="set-default clearer">   
                {textfield label="Branch" name="Branch1"  class="24_2a"}
                {* monthyear name="n8from1" label="From" class="6a_1a-45_1" *}
                {date name="n8from1" label="From" class="6a_1a-45_1 date_short" text_class="short"}
                {* monthyear name="n8to1" label="To" class="6a_1a-45_1" *}
                {date name="n8to1" label="To" class="6a_1a-45_1 date_short" text_class="short"}
                {textfield label="Type of Discharge" name="n8discharge1"  class="24_2a"}
            </div>
            <div class="set-default clearer">   
                {textfield label="Branch" name="Branch2"  class="24_2a"}
                {* monthyear name="n8from2" label="From" class="6a_1a-45_1" *}
                {* monthyear name="n8to2" label="To" class="6a_1a-45_1" *}
                {date name="n8to2" label="To" class="6a_1a-45_1 date_short" text_class="short"}
                {date name="n8from2" label="From" class="6a_1a-45_1 date_short" text_class="short"}
                {textfield label="Type of Discharge" name="n8discharge2"  class="24_2a"}
            </div>
            <div class="set-default clearer">   
                {textfield label="Branch" name="Branch3"  class="24_2a"}
                {* monthyear name="n8from3" label="From" class="6a_1a-45_1" *}
                {* monthyear name="n8to3" label="To" class="6a_1a-45_1" *}
                {date name="n8from3" label="From" class="6a_1a-45_1 date_short" text_class="short"}
                {date name="n8to3" label="To" class="6a_1a-45_1 date_short" text_class="short"}
                {textfield label="Type of Discharge" name="n8discharge3"  class="24_2a"}
            </div>
            </div>
        </fieldset>
        <fieldset><legend><span>16.</span> BACKGROUND INFORMATION</legend>
            <ol class="set-nolabel list-lalpha">
                <li>
                    <span class="stext {if $errorMessages.f9}item-error{/if}">During the last 10 years, have you been convicted, been imprisoned, been on probation, or been on parole? (Includes felonies, firearms or explosives violations, misdemeanors, and all other offenses.) <i>If "YES," use item 18 to provide the date, explanation of the violation, place of occurrence, and the name and address of the police department or court involved.</i></span>
                    {radiohorisontal name="f9" labels="Yes|No" values="Yes|No"}
                </li>
                <li>
                    <span class="stext {if $errorMessages.f10}item-error{/if}">Have you been convicted by a military court-martial in the past 10 years?
                    <i>(If no military service, answer "NO.")</i><br> <i> If "YES," use item 18 to provide the date, explanation of the violation, place of occurrence, and the name and address of the military authority or court involved.</i></span>
                    {radiohorisontal name="f10" labels="Yes|No" values="Yes|No"}
                </li>
                <li>
                    <span class="stext {if $errorMessages.f11}item-error{/if}">Are you now under charges for any violation of law? <i> If "YES," use item 18 to provide the date, explanation of the violation, place of occurrence, and the name and address of the police department or court involved.</i></span>
                    {radiohorisontal name="f11" labels="Yes|No" values="Yes|No"}
                </li>
                <li>
                    <span class="stext {if $errorMessages.f12}item-error{/if}">During the last 5 years, have you been fired from any job for 
                    any reason, did you quit after being told that you would be fired, did you leave any job by mutual 
                    agreement because of specific problems, or were you debarred from Federal employment by the Office of Personnel 
                    Management or any other Federal agency? <i> If "YES," use item 18 to provide the date, an explanation of the problem, reason for leaving, and the employer's name and address.</i></span>
                    {radiohorisontal name="f12" labels="Yes|No" values="Yes|No"}
                </li>
                <li>
                    <span class="stext {if $errorMessages.f13}item-error{/if}">Are you delinquent on any Federal debt? (Includes delinquencies arising
                    from Federal taxes, loans, overpayment of benefits, and other debts to the U.S. Government, plus defaults of Federally guaranteed or 
                    insured loans such as student and home mortgage loans.) <i>If "YES," use item 18 to provide the type, length, and amount of the delinquency or default, and steps that you are taking to correct the error or repay the debt.</i></span>
                    {radiohorisontal name="f13" labels="Yes|No" values="Yes|No"}
                </li>
            </ol>
        </fieldset>
        <fieldset><legend><span>17.</span> ADDITIONAL QUESTIONS</legend>
            <ol class="set-nolabel list-lalpha">
                <li>
                    <span class="stext {if $errorMessages.f14}item-error{/if}">Do any of your relatives work for the agency or government organization 
                    to which you are submitting this form? (Include: father, mother, husband, wife, son, daughter, brother, sister, uncle, 
                    aunt, first cousin, nephew, niece, father-in-law,mother-in-law, son-in-law, daughter-in-law, brother-in-law, sister-in-law, 
                    stepfather, stepmother, stepson, stepdaughter, stepbrother, stepsister, half brother, and half sister.) <i> If "YES," use item 18 to provide the relative's name, relationship, and the department, agency, or branch of the Armed Forces for which your relative works.</i></span>
                    {radiohorisontal name="f14" labels="Yes|No" values="Yes|No"}
                </li>
                <li>
                    <span class="stext {if $errorMessages.f15}item-error{/if}">Do you receive, or have you ever applied for, retirement pay, 
                    pension, or other retired pay based on military, Federal civilian, or District of Columbia Government service?</span>
                    {radiohorisontal name="f15" labels="Yes|No" values="Yes|No"}
                </li>
            </ol>
        </fieldset>
        <fieldset><legend><span>18.</span> CONTINUATION SPACE / AGENCY OPTIONAL QUESTIONS</legend>          
            <div class="note2_2 left_marg2a"><p>Provide details requested in items 16, 17 and 19c in the space below or on attached sheets. Be sure to identify attached sheets with your name, Social Security Number, and item number, and to include ZIP Codes in all addresses. If any questions are printed below, please answer as instructed (these questions are specific to your position and your agency is authorized to ask them).
            </p></div>
            <div class="set-default clearer left_marg2a">
                {textarea cols="100" rows="16" name="n16"}
            </div>
        </fieldset>
        <fieldset><legend><span>19.</span> APPOINTEE (ONLY RESPOND IF YOU HAVE BEEN EMPLOYED BY THE FEDERAL GOVERNMENT BEFORE)</legend>     
            <div class="note2_2 left_marg2a"><p>Your elections of life insurance during previous Federal employment may affect your eligibility for life insurance during your new appointment. 
            These questions are asked to help your personnel office make a correct determination.</p></div>
            <ol class="set-nolabel list-lalpha">
                <li>
                    <span class="stext">When did you leave your last Federal job?</span>
                    {date name="n18a" label="Date"}<br clear="all" />
                </li>
                <li>
                    <span class="stext">When you worked for the Federal Government the last time, did you waive Basic Life Insurance or
                    any type of optional life insurance?</span>
                    {radiohorisontal onclick="changeC18()" name="b18" labels="Yes|No|Don't know" values="Yes|No|DNK"}
                </li>
                 <li id="c18" {if $smarty.post.b18 != "Yes"} style="display:none" {/if}>
                    <span class="stext">Did you later cancel the waiver(s)?<i> If your answer to item is "NO," use item 18 to identify the type(s) of insurance for which waivers were not canceled.</i></span>
                    {radiohorisontal name="c18" labels="Yes|No|Don't know" values="Yes|No|DNK"}
                </li>
           </ol>
        </fieldset>  
        
        {literal}
        <script>
        selService();
        focusError('{/literal}{$error}', {$errorsCount}{literal});
        </script>
        {/literal}
<input type="submit" name="generate" value="Generate!" class="btn"> or <a href="index.php">Clear</a> {* or <a href="index.php?fill=1">Fill</a> *}
<br>

</form>
</div>
</body>
