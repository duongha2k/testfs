{literal}
<script>
function showadd(item){
    document.getElementById('a'+item).style.display = "";
    document.getElementById('a'+item+'show').style.display = "none";
    document.getElementById('add'+item).value = "1";
}
function hideadd(item){
    document.getElementById('a'+item).style.display = "none";
    document.getElementById('a'+item+'show').style.display = "";
    document.getElementById('add'+item).value = "0";
}
</script>
{/literal}

{include file="errors.tpl"}

{if $showPDF}
    <a href="result/SF85.pdf">SF85.pdf</a><br>
    <a href="result/of0306.pdf">of0306.pdf</a><br>
    {if $smarty.post.add8 || $smarty.post.add9 || $smarty.post.add10}
        <a href="result/SF86A.pdf">SF86A.pdf</a><br>
    {/if}
{/if}
<a href="index.php">Clear form</a><br>
<a href="index.php?fill=1">Fill form</a><br>
<form method="post" action="index.php">

First Name:<br>
<input type="text" name="FName" value="Tim" /><br>
Last Name:<br>
<input type="text" name="LName" value="Saharchuk" /><br>
Middle Name:<br>
<input type="text" name="MName" value="Viktorovich" /><br>
Name suffix:<br>
<input type="text" name="NameSuffix" value="Sr." /><br>


City of birth:<br>
<input type="text" name="CityOfBirth" value="Minnetonka" /><br>
State of birth:<br>
{states name="StateOfBirth" value="AL"}{*<input type="text" name="StateOfBirth" value="MN" />*}<br>
Country of birth:<br>
{countries name="CountryOfBirth" value="Unitied States"} {*<input type="text" name="CountryOfBirth" value="United States" />*}<br>
County of birth:<br>
<input type="text" name="CountyOfBirth" value="United States" /><br>
{month name="month" value="01"}
{date name="now" value="01/10/2007"}
Date of birth:<br>
<input type="text" name="DOB" value="10/18/1986" /><br>

Social security number:<br>
<input type="text" name="SSN" value="123-45-6789" /><br>
Other country:<br>
<input type="text" name="CountryOfBirth" value="Belarus" /><br>


Other name 1:<br>
<input type="text" name="OtherName1" value="Tim" /><br>
<input type="text" name="DateFrom1" value="01/07" /> to <input type="text" name="DateTo1" value="02/07"><br>
Other name 2:<br>
<input type="text" name="OtherName2" value="Timatej" /><br>
<input type="text" name="DateFrom2" value="02/07" /> to <input type="text" name="DateTo2" value="03/07"><br>
Other name 3:<br>
<input type="text" name="OtherName3" value="Timka" /><br>
<input type="text" name="DateFrom3" value="03/07" /> to <input type="text" name="DateTo3" value="04/07"><br>
Other name 4:<br>
<input type="text" name="OtherName4" value="Timorfej" /><br>
<input type="text" name="DateFrom4" value="04/07" /> to <input type="text" name="DateTo4" value="05/07"><br>


Sex:<br>
<input type="radio" name="sex" checked="checked" value="male" />Male<br>
<input type="radio" name="sex" value="female" />Female<br>


Citizenship Status:<br>
<input type="radio" name="citizen" checked="checked" value="USCitizen" />US, born<br>
<input type="radio" name="citizen" value="NotBornCitizen" />US, wasn't born<br>
<input type="radio" name="citizen" value="NotAUSCitizen" />not US<br>


Mother's Maiden Name:<br>
<input type="text" name="MothersMaidenName" value="Butseva" /><br>


<br>
Naturalization certificate:<br>
Court:<br>
<input type="text" name="CourtOfNaturaliztion" value="Unknown" /><br>
State:<br>
<input type="text" name="StateOfNaturalization" value="MN" /><br>
City:<br>
<input type="text" name="CityOfNaturalization" value="Minnetonka" /><br>
Certificate number:<br>
<input type="text" name="NaturalizationCertificate" value="431532429" /><br>
Date issued:<br>
<input type="text" name="DateOfIssue1" value="10/18/2002" /><br>


<br>
Citizenship certificate:<br>
State:<br>
<input type="text" name="StateOfCitizenshipCertificate" value="MN" /><br>
City:<br>
<input type="text" name="CityOfCitizenshipCertificate" value="Minnetonka" /><br>
Certificate number:<br>
<input type="text" name="CitizenshipCertificate" value="4315324234" /><br>
Date issued:<br>
<input type="text" name="DateOfIssue2" value="10/18/2004" /><br>


Date of form prepared:<br>
<input type="text" name="DateOfFormPrepared" value="11/18/2003" /><br>
Explanation:<br>
<input type="text" name="Explenation1" value="Explanation test" /><br>


Passport number:<br>
<input type="text" name="Passportn" value="12345" /><br>
Issued:<br>
<input type="text" name="DateOfIssue3" value="01/16/2009" /><br>


Country of dual citizenship:<br>
<input type="text" name="CountryOfDualCitizenship" value="Belarus3" /><br>


<br>Alien:<br>
City:<br>
<input type="text" name="CityOfUSEntrance" value="Minnetonka" /><br>
State:<br>
<input type="text" name="StateOfUSEnterance" value="MN" /><br>
Date Of Enterance:<br>
<input type="text" name="DateOfUSEnterance" value="11/05/1997" /><br>
Alien registration number:<br>
<input type="text" name="AlienRegistrationNumber" value="234234" /><br>
Countries of citizenship:<br>
<textarea name="CountryOfCitizenship">Belarus4</textarea><br>

<br>
--------------------------------------------------------         8        -------------------------------------------------
<br>
<br>

#1
<br>

Date From:<br>
<input type="text" name="DateFrom5" value="01/10" /><br>
Street Address:<br>
<input type="text" name="StreetAddress1" value="testtt1" /><br>
Apt#:<br>
<input type="text" name="Aptn1" value="testss1" /><br>
City(Country):<br>
<input type="text" name="CityCountry1" value="testee1" /><br>
State:<br>
<input type="text" name="State1" value="MA" /><br>
ZipCode:<br>
<input type="text" name="ZIpCode1" value="11111" /><br>
Name Of Person:<br>
<input type="text" name="NameOfPerson1" value="Tim1" /><br>
Address of Person:<br>
<input type="text" name="StreetAddressOfPerson1" value="Address1" /><br>
Apt# of Person:<br>
<input type="text" name="AptnOfPerson1" value="Apt1" /><br>
City(Country) of Person:<br>
<input type="text" name="CityCountryOfPerson1" value="CC1" /><br>
State of person:<br>
<input type="text" name="StateOfPerson1" value="SP" /><br>
ZipCode:<br>
<input type="text" name="ZIpCodeOfPerson1" value="11111" /><br>


<br>

#2
<br>

Date From:<br>
<input type="text" name="DateFrom6" value="02/10" /><br>
Street Address:<br>
<input type="text" name="StreetAddress2" value="testtt2" /><br>
Apt#:<br>
<input type="text" name="Aptn2" value="testss2" /><br>
City(Country):<br>
<input type="text" name="CityCountry2" value="testee2" /><br>
State:<br>
<input type="text" name="State2" value="MB" /><br>
ZipCode:<br>
<input type="text" name="ZIpCode2" value="22222" /><br>
Name Of Person:<br>
<input type="text" name="NameOfPerson2" value="Tim2" /><br>
Address of Person:<br>
<input type="text" name="StreetAddressOfPerson2" value="Address2" /><br>
Apt# of Person:<br>
<input type="text" name="AptnOfPerson2" value="Apt2" /><br>
City(Country) of Person:<br>
<input type="text" name="CityCountryOfPerson2" value="CC2" /><br>
State of person:<br>
<input type="text" name="StateOfPerson2" value="SB" /><br>
ZipCode:<br>
<input type="text" name="ZIpCodeOfPerson2" value="22222" /><br>

<br>

#3
<br>

Date From:<br>
<input type="text" name="DateFrom7" value="03/10" /><br>
Street Address:<br>
<input type="text" name="StreetAddress3" value="testtt3" /><br>
Apt#:<br>
<input type="text" name="Aptn3" value="testss3" /><br>
City(Country):<br>
<input type="text" name="CityCountry3" value="testee3" /><br>
State:<br>
<input type="text" name="State3" value="MB" /><br>
ZipCode:<br>
<input type="text" name="ZIpCode3" value="33333" /><br>
Name Of Person:<br>
<input type="text" name="NameOfPerson3" value="Tim3" /><br>
Address of Person:<br>
<input type="text" name="StreetAddressOfPerson3" value="Address3" /><br>
Apt# of Person:<br>
<input type="text" name="AptnOfPerson3" value="Apt3" /><br>
City(Country) of Person:<br>
<input type="text" name="CityCountryOfPerson3" value="CC3" /><br>
State of person:<br>
<input type="text" name="StateOfPerson3" value="SB" /><br>
ZipCode:<br>
<input type="text" name="ZIpCodeOfPerson3" value="33333" /><br>

<br>

#4
<br>

Date From:<br>
<input type="text" name="DateFrom8" value="04/10" /><br>
Street Address:<br>
<input type="text" name="StreetAddress4" value="testtt4" /><br>
Apt#:<br>
<input type="text" name="Aptn4" value="testss4" /><br>
City(Country):<br>
<input type="text" name="CityCountry4" value="testee4" /><br>
State:<br>
<input type="text" name="State4" value="MB" /><br>
ZipCode:<br>
<input type="text" name="ZIpCode4" value="44444" /><br>
Name Of Person:<br>
<input type="text" name="NameOfPerson4" value="Tim4" /><br>
Address of Person:<br>
<input type="text" name="StreetAddressOfPerson4" value="Address4" /><br>
Apt# of Person:<br>
<input type="text" name="AptnOfPerson4" value="Apt4" /><br>
City(Country) of Person:<br>
<input type="text" name="CityCountryOfPerson4" value="CC4" /><br>
State of person:<br>
<input type="text" name="StateOfPerson4" value="SB" /><br>
ZipCode:<br>
<input type="text" name="ZIpCodeOfPerson4" value="44444" /><br>

<br>

#5
<br>

Date From:<br>
<input type="text" name="DateFrom9" value="05/10" /><br>
Street Address:<br>
<input type="text" name="StreetAddress5" value="testtt5" /><br>
Apt#:<br>
<input type="text" name="Aptn5" value="testss5" /><br>
City(Country):<br>
<input type="text" name="CityCountry5" value="testee5" /><br>
State:<br>
<input type="text" name="State5" value="MB" /><br>
ZipCode:<br>
<input type="text" name="ZIpCode5" value="55555" /><br>
Name Of Person:<br>
<input type="text" name="NameOfPerson5" value="Tim5" /><br>
Address of Person:<br>
<input type="text" name="StreetAddressOfPerson5" value="Address5" /><br>
Apt# of Person:<br>
<input type="text" name="AptnOfPerson5" value="Apt5" /><br>
City(Country) of Person:<br>
<input type="text" name="CityCountryOfPerson5" value="CC5" /><br>
State of person:<br>
<input type="text" name="StateOfPerson5" value="SB" /><br>
ZipCode:<br>
<input type="text" name="ZIpCodeOfPerson5" value="55555" /><br>


<a href="javascript:showadd(8);" {if $smarty.post.add8} style="display:none;" {/if} id="a8show">More</a>
<div {if !$smarty.post.add8} style="display:none;" {/if} id="a8"  >

-----------------------------------------------      8 SF86A       --------------------------------------
<br>

#6
<br>

Date From:<br>
<input type="text" name="aDateFrom5" value="01/10" /><br>
Street Address:<br>
<input type="text" name="aStreetAddress1" value="testtt1" /><br>
Apt#:<br>
<input type="text" name="aAptn1" value="testss1" /><br>
City(Country):<br>
<input type="text" name="aCityCountry1" value="testee1" /><br>
State:<br>
<input type="text" name="aState1" value="MA" /><br>
ZipCode:<br>
<input type="text" name="aZIpCode1" value="11111" /><br>
Name Of Person:<br>
<input type="text" name="aNameOfPerson1" value="Tim1" /><br>
Address of Person:<br>
<input type="text" name="aStreetAddressOfPerson1" value="Address1" /><br>
Apt# of Person:<br>
<input type="text" name="aAptnOfPerson1" value="Apt1" /><br>
City(Country) of Person:<br>
<input type="text" name="aCityCountryOfPerson1" value="CC1" /><br>
State of person:<br>
<input type="text" name="aStateOfPerson1" value="SP" /><br>
ZipCode:<br>
<input type="text" name="aZIpCodeOfPerson1" value="11111" /><br>


<br>

#7
<br>

Date From:<br>
<input type="text" name="aDateFrom6" value="02/10" /><br>
Street Address:<br>
<input type="text" name="aStreetAddress2" value="testtt2" /><br>
Apt#:<br>
<input type="text" name="aAptn2" value="testss2" /><br>
City(Country):<br>
<input type="text" name="aCityCountry2" value="testee2" /><br>
State:<br>
<input type="text" name="aState2" value="MB" /><br>
ZipCode:<br>
<input type="text" name="aZIpCode2" value="22222" /><br>
Name Of Person:<br>
<input type="text" name="aNameOfPerson2" value="Tim2" /><br>
Address of Person:<br>
<input type="text" name="aStreetAddressOfPerson2" value="Address2" /><br>
Apt# of Person:<br>
<input type="text" name="aAptnOfPerson2" value="Apt2" /><br>
City(Country) of Person:<br>
<input type="text" name="aCityCountryOfPerson2" value="CC2" /><br>
State of person:<br>
<input type="text" name="aStateOfPerson2" value="SB" /><br>
ZipCode:<br>
<input type="text" name="aZIpCodeOfPerson2" value="22222" /><br>

<br>

#8
<br>

Date From:<br>
<input type="text" name="aDateFrom7" value="03/10" /><br>
Street Address:<br>
<input type="text" name="aStreetAddress3" value="testtt3" /><br>
Apt#:<br>
<input type="text" name="aAptn3" value="testss3" /><br>
City(Country):<br>
<input type="text" name="aCityCountry3" value="testee3" /><br>
State:<br>
<input type="text" name="aState3" value="MB" /><br>
ZipCode:<br>
<input type="text" name="aZIpCode3" value="33333" /><br>
Name Of Person:<br>
<input type="text" name="aNameOfPerson3" value="Tim3" /><br>
Address of Person:<br>
<input type="text" name="aStreetAddressOfPerson3" value="Address3" /><br>
Apt# of Person:<br>
<input type="text" name="aAptnOfPerson3" value="Apt3" /><br>
City(Country) of Person:<br>
<input type="text" name="aCityCountryOfPerson3" value="CC3" /><br>
State of person:<br>
<input type="text" name="aStateOfPerson3" value="SB" /><br>
ZipCode:<br>
<input type="text" name="aZIpCodeOfPerson3" value="33333" /><br>

<br>

#9
<br>

Date From:<br>
<input type="text" name="aDateFrom8" value="04/10" /><br>
Street Address:<br>
<input type="text" name="aStreetAddress4" value="testtt4" /><br>
Apt#:<br>
<input type="text" name="aAptn4" value="testss4" /><br>
City(Country):<br>
<input type="text" name="aCityCountry4" value="testee4" /><br>
State:<br>
<input type="text" name="aState4" value="MB" /><br>
ZipCode:<br>
<input type="text" name="aZIpCode4" value="44444" /><br>
Name Of Person:<br>
<input type="text" name="aNameOfPerson4" value="Tim4" /><br>
Address of Person:<br>
<input type="text" name="aStreetAddressOfPerson4" value="Address4" /><br>
Apt# of Person:<br>
<input type="text" name="aAptnOfPerson4" value="Apt4" /><br>
City(Country) of Person:<br>
<input type="text" name="aCityCountryOfPerson4" value="CC4" /><br>
State of person:<br>
<input type="text" name="aStateOfPerson4" value="SB" /><br>
ZipCode:<br>
<input type="text" name="aZIpCodeOfPerson4" value="44444" /><br>

<br>

#10
<br>

Date From:<br>
<input type="text" name="aDateFrom9" value="05/10" /><br>
Street Address:<br>
<input type="text" name="aStreetAddress5" value="testtt5" /><br>
Apt#:<br>
<input type="text" name="aAptn5" value="testss5" /><br>
City(Country):<br>
<input type="text" name="aCityCountry5" value="testee5" /><br>
State:<br>
<input type="text" name="aState5" value="MB" /><br>
ZipCode:<br>
<input type="text" name="aZIpCode5" value="55555" /><br>
Name Of Person:<br>
<input type="text" name="aNameOfPerson5" value="Tim5" /><br>
Address of Person:<br>
<input type="text" name="aStreetAddressOfPerson5" value="Address5" /><br>
Apt# of Person:<br>
<input type="text" name="aAptnOfPerson5" value="Apt5" /><br>
City(Country) of Person:<br>
<input type="text" name="aCityCountryOfPerson5" value="CC5" /><br>
State of person:<br>
<input type="text" name="aStateOfPerson5" value="SB" /><br>
ZipCode:<br>
<input type="text" name="aZIpCodeOfPerson5" value="55555" /><br>


<a href="javascript:hideadd(8);" id="a8hide">Hide More</a>
<input type="hidden" name="add8" value="{$smarty.post.add8}" id="add8"  />
</div>
<br>
<br>

-----------------------------------------------      9       --------------------------------------
<br>
School:
<br>
#1
<br>
Name of school:<br>
<input type="text" name="NameOfSchool1" value="n1" /><br>
Code:<br>
<input type="text" name="Code1" value="1" /><br>
Degree/Diploma/Other:<br>
<input type="text" name="DegreesDiplomasOther1" value="big1" /><br>
Month/Year Awarded:<br>
<input type="text" name="MonthsYearAwarded1" value="07/09" /><br>
Addres:<br>
<input type="text" name="StreetAddresspCityOfSchool1" value="test1" /><br>
StateOfSchool:<br>
<input type="text" name="StateOfSchool1" value="MA" /><br>
ZipCode:<br>
<input type="text" name="ZIpCodeOfSchool1" value="11111" /><br>
From:<br>
<input type="text" name="DateFrom10" value="01/01" /><br>
To:<br>
<input type="text" name="DateTo10" value="01/01" /><br>

<br>
#2
<br>
Name of school:<br>
<input type="text" name="NameOfSchool2" value="n2" /><br>
Code:<br>
<input type="text" name="Code2" value="2" /><br>
Degree/Diploma/Other:<br>
<input type="text" name="DegreesDiplomasOther2" value="big2" /><br>
Month/Year Awarded:<br>
<input type="text" name="MonthsYearAwarded2" value="08/09" /><br>
Addres:<br>
<input type="text" name="StreetAddresspCityOfSchool2" value="test2" /><br>
StateOfSchool:<br>
<input type="text" name="StateOfSchool2" value="MC" /><br>
ZipCode:<br>
<input type="text" name="ZIpCodeOfSchool2" value="21222" /><br>
From:<br>
<input type="text" name="DateFrom11" value="02/01" /><br>
To:<br>
<input type="text" name="DateTo11" value="02/01" /><br>

<br>
#3
<br>
Name of school:<br>
<input type="text" name="NameOfSchool3" value="n3" /><br>
Code:<br>
<input type="text" name="Code3" value="3" /><br>
Degree/Diploma/Other:<br>
<input type="text" name="DegreesDiplomasOther3" value="big3" /><br>
Month/Year Awarded:<br>
<input type="text" name="MonthsYearAwarded3" value="09/09" /><br>
Addres:<br>
<input type="text" name="StreetAddresspCityOfSchool3" value="test3" /><br>
StateOfSchool:<br>
<input type="text" name="StateOfSchool3" value="MD" /><br>
ZipCode:<br>
<input type="text" name="ZIpCodeOfSchool3" value="33333" /><br>
From:<br>
<input type="text" name="DateFrom12" value="03/01" /><br>
To:<br>
<input type="text" name="DateTo12" value="03/01" /><br>


<a href="javascript:showadd(9);" {if $smarty.post.add9} style="display:none;" {/if} id="a9show">More</a>
<div {if !$smarty.post.add9} style="display:none;" {/if} id="a9"  >
-----------------------------------------------      9 SF86A      --------------------------------------
<br>
School:
<br>
#4
<br>
Name of school:<br>
<input type="text" name="aNameOfSchool1" value="n1" /><br>
Code:<br>
<input type="text" name="aCode1" value="1" /><br>
Degree/Diploma/Other:<br>
<input type="text" name="aDegreesDiplomasOther1" value="big1" /><br>
Month/Year Awarded:<br>
<input type="text" name="aMonthsYearAwarded1" value="07/09" /><br>
Addres:<br>
<input type="text" name="aStreetAddresspCityOfSchool1" value="test1" /><br>
StateOfSchool:<br>
<input type="text" name="aStateOfSchool1" value="MA" /><br>
ZipCode:<br>
<input type="text" name="aZIpCodeOfSchool1" value="11111" /><br>
From:<br>
<input type="text" name="aDateFrom10" value="01/01" /><br>
To:<br>
<input type="text" name="aDateTo10" value="01/01" /><br>

<br>
#5
<br>
Name of school:<br>
<input type="text" name="aNameOfSchool2" value="n2" /><br>
Code:<br>
<input type="text" name="aCode2" value="2" /><br>
Degree/Diploma/Other:<br>
<input type="text" name="aDegreesDiplomasOther2" value="big2" /><br>
Month/Year Awarded:<br>
<input type="text" name="aMonthsYearAwarded2" value="08/09" /><br>
Addres:<br>
<input type="text" name="aStreetAddresspCityOfSchool2" value="test2" /><br>
StateOfSchool:<br>
<input type="text" name="aStateOfSchool2" value="MC" /><br>
ZipCode:<br>
<input type="text" name="aZIpCodeOfSchool2" value="21222" /><br>
From:<br>
<input type="text" name="aDateFrom11" value="02/01" /><br>
To:<br>
<input type="text" name="aDateTo11" value="02/01" /><br>

<br>
#6
<br>
Name of school:<br>
<input type="text" name="aNameOfSchool3" value="n3" /><br>
Code:<br>
<input type="text" name="aCode3" value="3" /><br>
Degree/Diploma/Other:<br>
<input type="text" name="aDegreesDiplomasOther3" value="big3" /><br>
Month/Year Awarded:<br>
<input type="text" name="aMonthsYearAwarded3" value="09/09" /><br>
Addres:<br>
<input type="text" name="aStreetAddresspCityOfSchool3" value="test3" /><br>
StateOfSchool:<br>
<input type="text" name="aStateOfSchool3" value="MD" /><br>
ZipCode:<br>
<input type="text" name="aZIpCodeOfSchool3" value="33333" /><br>
From:<br>
<input type="text" name="aDateFrom12" value="03/01" /><br>
To:<br>
<input type="text" name="aDateTo12" value="03/01" /><br>


<a href="javascript:hideadd(9);"  id="a9hide">Hide More</a>
<input type="hidden" name="add9" value="{$smarty.post.add9}" id="add9" value="0" />
</div>
<br>
<br>

-------------------------------------     10     ---------------------------------------
<br>
<br>
#1
<br>
From:<br>
<input type="text" name="DateFrom13" value="01/01" /><br>
Code:<br>
<input type="text" name="Code4" value="11111" /><br>
...Location:<br>
<input type="text" name="EmployersVerifierNamesMilitaryDutyLocation1" value="loc1" /><br>
Position:<br>
<input type="text" name="PositionTitlesMilitaryRank" value="rank1" /><br>

E str address:<br>
<input type="text" name="EmployersVerifierStreetAddress1" value="add1" /><br>
City:<br>
<input type="text" name="EmployersVerifierCityCountry1" value="minsk1" /><br>
State:<br>
<input type="text" name="EmployersVerifierState1" value="MN" /><br>
Zipcode:<br>
<input type="text" name="EmployersVerifierZIPCode1" value="24356" /><br>
Area code:<br>
<input type="text" name="AreaCodeOfEmployersVerifier1" value="017" /><br>
Tel:<br>
<input type="text" name="PhonenOfEmployersVerifier1" value="555-999-5001" /><br>

E str address:<br>
<input type="text" name="StreetAddressOfJobLocation1" value="jadd1" /><br>
City:<br>
<input type="text" name="CityCountryOfJobLocation1" value="jminsk1" /><br>
State:<br>
<input type="text" name="StateOfJobLocation1" value="MN" /><br>
Zipcode:<br>
<input type="text" name="ZIPCodeOfJobLocation1" value="24356" /><br>
Area code:<br>
<input type="text" name="AreaCodeOfJobLocation1" value="017" /><br>
Tel:<br>
<input type="text" name="PhonenOfJobLocation1" value="555-999-5001" /><br>

E str address:<br>
<input type="text" name="SupervisorNamepStreetAddress1" value="sadd1" /><br>
City:<br>
<input type="text" name="CityCountryOfSupervisor1" value="sminsk1" /><br>
State:<br>
<input type="text" name="StateOfSupervisor1" value="MN" /><br>
Zipcode:<br>
<input type="text" name="ZIPCodeOfSupervisor1" value="24356" /><br>
Area code:<br>
<input type="text" name="AreaCodeOfSupervisor1" value="017" /><br>
Tel:<br>
<input type="text" name="PhonenOfSupervisor1" value="555-999-5001" /><br>

From:<br>
<input type="text" name="DateFrom14" value="01/01" /><br>
To:<br>
<input type="text" name="DateTo14" value="01/02" /><br>
Position:<br>
<input type="text" name="PositionTitle1" value="pos1" /><br>
Supervisor:<br>
<input type="text" name="Supervisor1" value="sup1" /><br>

From:<br>
<input type="text" name="DateFrom15" value="01/02" /><br>
To:<br>
<input type="text" name="DateTo15" value="01/03" /><br>
Position:<br>
<input type="text" name="PositionTitle2" value="pos1" /><br>
Supervisor:<br>
<input type="text" name="Supervisor2" value="sup1" /><br>

From:<br>
<input type="text" name="DateFrom16" value="01/03" /><br>
To:<br>
<input type="text" name="DateTo16" value="01/04" /><br>
Position:<br>
<input type="text" name="PositionTitle3" value="pos1" /><br>
Supervisor:<br>
<input type="text" name="Supervisor3" value="sup1" /><br>

<br>
#2
<br>
From:<br>
<input type="text" name="DateFrom17" value="01/01" /><br>
Code:<br>
<input type="text" name="Code5" value="111" /><br>
...Location:<br>
<input type="text" name="EmployersVerifierNamesMilitaryDutyLocation2" value="loc2" /><br>
Position:<br>
<input type="text" name="PositionTitlesMilitaryRank2" value="rank2" /><br>

E str address:<br>
<input type="text" name="EmployersVerifierStreetAddress2" value="add2" /><br>
City:<br>
<input type="text" name="EmployersVerifierCityCountry2" value="minsk2" /><br>
State:<br>
<input type="text" name="EmployersVerifierState2" value="MN" /><br>
Zipcode:<br>
<input type="text" name="EmployersVerifierZIPCode2" value="24326" /><br>
Area code:<br>
<input type="text" name="AreaCodeOfEmployersVerifier2" value="017" /><br>
Tel:<br>
<input type="text" name="PhonenOfEmployersVerifier2" value="555-999-5001" /><br>

E str address:<br>
<input type="text" name="StreetAddressOfJobLocation2" value="jadd2" /><br>
City:<br>
<input type="text" name="CityCountryOfJobLocation2" value="jminsk2" /><br>
State:<br>
<input type="text" name="StateOfJobLocation2" value="MN" /><br>
Zipcode:<br>
<input type="text" name="ZIPCodeOfJobLocation2" value="23356" /><br>
Area code:<br>
<input type="text" name="AreaCodeOfJobLocation2" value="017" /><br>
Tel:<br>
<input type="text" name="PhonenOfJobLocation2" value="555-999-5001" /><br>

E str address:<br>
<input type="text" name="SupervisorNamepStreetAddress2" value="sadd2" /><br>
City:<br>
<input type="text" name="CityCountryOfSupervisor2" value="sminsk2" /><br>
State:<br>
<input type="text" name="StateOfSupervisor2" value="MN" /><br>
Zipcode:<br>
<input type="text" name="ZIPCodeOfSupervisor2" value="24356" /><br>
Area code:<br>
<input type="text" name="AreaCodeOfSupervisor2" value="017" /><br>
Tel:<br>
<input type="text" name="PhonenOfSupervisor2" value="555-999-5001" /><br>

From:<br>
<input type="text" name="DateFrom18" value="01/01" /><br>
To:<br>
<input type="text" name="DateTo18" value="01/02" /><br>
Position:<br>
<input type="text" name="PositionTitle4" value="pos4" /><br>
Supervisor:<br>
<input type="text" name="Supervisor4" value="sup4" /><br>

From:<br>
<input type="text" name="DateFrom19" value="01/02" /><br>
To:<br>
<input type="text" name="DateTo19" value="01/03" /><br>
Position:<br>
<input type="text" name="PositionTitle5" value="pos5" /><br>
Supervisor:<br>
<input type="text" name="Supervisor5" value="sup5" /><br>

From:<br>
<input type="text" name="DateFrom20" value="01/03" /><br>
To:<br>
<input type="text" name="DateTo20" value="01/04" /><br>
Position:<br>
<input type="text" name="PositionTitle6" value="pos6" /><br>
Supervisor:<br>
<input type="text" name="Supervisor6" value="sup6" /><br>

<br>
#3
<br>
From:<br>
<input type="text" name="DateFrom21" value="01/01" /><br>
Code:<br>
<input type="text" name="Code6" value="333" /><br>
...Location:<br>
<input type="text" name="EmployersVerifierNamesMilitaryDutyLocation3" value="loc3" /><br>
Position:<br>
<input type="text" name="PositionTitlesMilitaryRank3" value="rank3" /><br>

E str address:<br>
<input type="text" name="EmployersVerifierStreetAddress3" value="add3" /><br>
City:<br>
<input type="text" name="EmployersVerifierCityCountry3" value="minsk3" /><br>
State:<br>
<input type="text" name="EmployersVerifierState3" value="MN" /><br>
Zipcode:<br>
<input type="text" name="EmployersVerifierZIPCode3" value="24326" /><br>
Area code:<br>
<input type="text" name="AreaCodeOfEmployersVerifier3" value="017" /><br>
Tel:<br>
<input type="text" name="PhonenOfEmployersVerifier3" value="555-999-5001" /><br>

E str address:<br>
<input type="text" name="StreetAddressOfJobLocation3" value="jadd3" /><br>
City:<br>
<input type="text" name="CityCountryOfJobLocation3" value="jminsk3" /><br>
State:<br>
<input type="text" name="StateOfJobLocation3" value="MN" /><br>
Zipcode:<br>
<input type="text" name="ZIPCodeOfJobLocation3" value="23356" /><br>
Area code:<br>
<input type="text" name="AreaCodeOfJobLocation3" value="017" /><br>
Tel:<br>
<input type="text" name="PhonenOfJobLocation3" value="555-999-5001" /><br>

E str address:<br>
<input type="text" name="SupervisorNamepStreetAddress3" value="sadd3" /><br>
City:<br>
<input type="text" name="CityCountryOfSupervisor3" value="sminsk3" /><br>
State:<br>
<input type="text" name="StateOfSupervisor3" value="MN" /><br>
Zipcode:<br>
<input type="text" name="ZIPCodeOfSupervisor3" value="24356" /><br>
Area code:<br>
<input type="text" name="AreaCodeOfSupervisor3" value="017" /><br>
Tel:<br>
<input type="text" name="PhonenOfSupervisor3" value="555-999-5001" /><br>

From:<br>
<input type="text" name="DateFrom22" value="01/01" /><br>
To:<br>
<input type="text" name="DateTo22" value="01/02" /><br>
Position:<br>
<input type="text" name="PositionTitle7" value="pos7" /><br>
Supervisor:<br>
<input type="text" name="Supervisor7" value="sup7" /><br>

From:<br>
<input type="text" name="DateFrom23" value="01/02" /><br>
To:<br>
<input type="text" name="DateTo23" value="01/03" /><br>
Position:<br>
<input type="text" name="PositionTitle8" value="pos8" /><br>
Supervisor:<br>
<input type="text" name="Supervisor8" value="sup8" /><br>

From:<br>
<input type="text" name="DateFrom24" value="01/03" /><br>
To:<br>
<input type="text" name="DateTo24" value="01/04" /><br>
Position:<br>
<input type="text" name="PositionTitle9" value="pos9" /><br>
Supervisor:<br>
<input type="text" name="Supervisor9" value="sup9" /><br>

<br>
#4
<br>
From:<br>
<input type="text" name="DateFrom25" value="01/01" /><br>
Code:<br>
<input type="text" name="Code7" value="777" /><br>
...Location:<br>
<input type="text" name="EmployersVerifierNamesMilitaryDutyLocation4" value="loc4" /><br>
Position:<br>
<input type="text" name="PositionTitlesMilitaryRank4" value="rank4" /><br>

E str address:<br>
<input type="text" name="EmployersVerifierStreetAddress4" value="add4" /><br>
City:<br>
<input type="text" name="EmployersVerifierCityCountry4" value="minsk4" /><br>
State:<br>
<input type="text" name="EmployersVerifierState4" value="MN" /><br>
Zipcode:<br>
<input type="text" name="EmployersVerifierZIPCode4" value="24326" /><br>
Area code:<br>
<input type="text" name="AreaCodeOfEmployersVerifier4" value="017" /><br>
Tel:<br>
<input type="text" name="PhonenOfEmployersVerifier4" value="555-999-5001" /><br>

E str address:<br>
<input type="text" name="StreetAddressOfJobLocation4" value="jadd4" /><br>
City:<br>
<input type="text" name="CityCountryOfJobLocation4" value="jminsk4" /><br>
State:<br>
<input type="text" name="StateOfJobLocation4" value="MN" /><br>
Zipcode:<br>
<input type="text" name="ZIPCodeOfJobLocation4" value="23356" /><br>
Area code:<br>
<input type="text" name="AreaCodeOfJobLocation4" value="017" /><br>
Tel:<br>
<input type="text" name="PhonenOfJobLocation4" value="555-999-5001" /><br>

E str address:<br>
<input type="text" name="SupervisorNamepStreetAddress4" value="sadd4" /><br>
City:<br>
<input type="text" name="CityCountryOfSupervisor4" value="sminsk4" /><br>
State:<br>
<input type="text" name="StateOfSupervisor4" value="MN" /><br>
Zipcode:<br>
<input type="text" name="ZIPCodeOfSupervisor4" value="24356" /><br>
Area code:<br>
<input type="text" name="AreaCodeOfSupervisor4" value="017" /><br>
Tel:<br>
<input type="text" name="PhonenOfSupervisor4" value="555-999-5001" /><br>

From:<br>
<input type="text" name="DateFrom26" value="01/01" /><br>
To:<br>
<input type="text" name="DateTo26" value="01/02" /><br>
Position:<br>
<input type="text" name="PositionTitle10" value="pos10" /><br>
Supervisor:<br>
<input type="text" name="Supervisor10" value="sup10" /><br>

From:<br>
<input type="text" name="DateFrom27" value="01/02" /><br>
To:<br>
<input type="text" name="DateTo27" value="01/03" /><br>
Position:<br>
<input type="text" name="PositionTitle11" value="pos11" /><br>
Supervisor:<br>
<input type="text" name="Supervisor11" value="sup11" /><br>

From:<br>
<input type="text" name="DateFrom28" value="01/03" /><br>
To:<br>
<input type="text" name="DateTo28" value="01/04" /><br>
Position:<br>
<input type="text" name="PositionTitle12" value="pos12" /><br>
Supervisor:<br>
<input type="text" name="Supervisor12" value="sup12" /><br>

<br>
#5
<br>
From:<br>
<input type="text" name="DateFrom29" value="01/01" /><br>
Code:<br>
<input type="text" name="Code8" value="888" /><br>
...Location:<br>
<input type="text" name="EmployersVerifierNamesMilitaryDutyLocation5" value="loc5" /><br>
Position:<br>
<input type="text" name="PositionTitlesMilitaryRank5" value="rank5" /><br>

E str address:<br>
<input type="text" name="EmployersVerifierStreetAddress5" value="add5" /><br>
City:<br>
<input type="text" name="EmployersVerifierCityCountry5" value="minsk5" /><br>
State:<br>
<input type="text" name="EmployersVerifierState5" value="MN" /><br>
Zipcode:<br>
<input type="text" name="EmployersVerifierZIPCode5" value="24326" /><br>
Area code:<br>
<input type="text" name="AreaCodeOfEmployersVerifier5" value="017" /><br>
Tel:<br>
<input type="text" name="PhonenOfEmployersVerifier5" value="555-999-5001" /><br>

E str address:<br>
<input type="text" name="StreetAddressOfJobLocation5" value="jadd5" /><br>
City:<br>
<input type="text" name="CityCountryOfJobLocation5" value="jminsk5" /><br>
State:<br>
<input type="text" name="StateOfJobLocation5" value="MN" /><br>
Zipcode:<br>
<input type="text" name="ZIPCodeOfJobLocation5" value="23356" /><br>
Area code:<br>
<input type="text" name="AreaCodeOfJobLocation5" value="017" /><br>
Tel:<br>
<input type="text" name="PhonenOfJobLocation5" value="555-999-5001" /><br>

E str address:<br>
<input type="text" name="SupervisorNamepStreetAddress5" value="sadd5" /><br>
City:<br>
<input type="text" name="CityCountryOfSupervisor5" value="sminsk5" /><br>
State:<br>
<input type="text" name="StateOfSupervisor5" value="MN" /><br>
Zipcode:<br>
<input type="text" name="ZIPCodeOfSupervisor5" value="24356" /><br>
Area code:<br>
<input type="text" name="AreaCodeOfSupervisor5" value="017" /><br>
Tel:<br>
<input type="text" name="PhonenOfSupervisor5" value="555-999-5001" /><br>

From:<br>
<input type="text" name="DateFrom30" value="01/01" /><br>
To:<br>
<input type="text" name="DateTo30" value="01/02" /><br>
Position:<br>
<input type="text" name="PositionTitle13" value="pos13" /><br>
Supervisor:<br>
<input type="text" name="Supervisor13" value="sup13" /><br>

From:<br>
<input type="text" name="DateFrom31" value="01/02" /><br>
To:<br>
<input type="text" name="DateTo31" value="01/03" /><br>
Position:<br>
<input type="text" name="PositionTitle14" value="pos14" /><br>
Supervisor:<br>
<input type="text" name="Supervisor14" value="sup14" /><br>

From:<br>
<input type="text" name="DateFrom32" value="01/03" /><br>
To:<br>
<input type="text" name="DateTo32" value="01/04" /><br>
Position:<br>
<input type="text" name="PositionTitle15" value="pos15" /><br>
Supervisor:<br>
<input type="text" name="Supervisor15" value="sup15" /><br>

<br>
#6
<br>
From:<br>
<input type="text" name="DateFrom33" value="01/01" /><br>
Code:<br>
<input type="text" name="Code9" value="999" /><br>
...Location:<br>
<input type="text" name="EmployersVerifierNamesMilitaryDutyLocation6" value="loc6" /><br>
Position:<br>
<input type="text" name="PositionTitlesMilitaryRank6" value="rank6" /><br>

E str address:<br>
<input type="text" name="EmployersVerifierStreetAddress6" value="add6" /><br>
City:<br>
<input type="text" name="EmployersVerifierCityCountry6" value="minsk6" /><br>
State:<br>
<input type="text" name="EmployersVerifierState6" value="MN" /><br>
Zipcode:<br>
<input type="text" name="EmployersVerifierZIPCode6" value="24326" /><br>
Area code:<br>
<input type="text" name="AreaCodeOfEmployersVerifier6" value="017" /><br>
Tel:<br>
<input type="text" name="PhonenOfEmployersVerifier6" value="555-999-5001" /><br>

E str address:<br>
<input type="text" name="StreetAddressOfJobLocation6" value="jadd6" /><br>
City:<br>
<input type="text" name="CityCountryOfJobLocation6" value="jminsk6" /><br>
State:<br>
<input type="text" name="StateOfJobLocation6" value="MN" /><br>
Zipcode:<br>
<input type="text" name="ZIPCodeOfJobLocation6" value="23356" /><br>
Area code:<br>
<input type="text" name="AreaCodeOfJobLocation6" value="017" /><br>
Tel:<br>
<input type="text" name="PhonenOfJobLocation6" value="555-999-5001" /><br>

E str address:<br>
<input type="text" name="SupervisorNamepStreetAddress6" value="sadd6" /><br>
City:<br>
<input type="text" name="CityCountryOfSupervisor6" value="sminsk6" /><br>
State:<br>
<input type="text" name="StateOfSupervisor6" value="MN" /><br>
Zipcode:<br>
<input type="text" name="ZIPCodeOfSupervisor6" value="24356" /><br>
Area code:<br>
<input type="text" name="AreaCodeOfSupervisor6" value="017" /><br>
Tel:<br>
<input type="text" name="PhonenOfSupervisor6" value="555-999-5001" /><br>

From:<br>
<input type="text" name="DateFrom34" value="01/01" /><br>
To:<br>
<input type="text" name="DateTo34" value="01/02" /><br>
Position:<br>
<input type="text" name="PositionTitle16" value="pos16" /><br>
Supervisor:<br>
<input type="text" name="Supervisor16" value="sup16" /><br>

From:<br>
<input type="text" name="DateFrom35" value="01/02" /><br>
To:<br>
<input type="text" name="DateTo35" value="01/03" /><br>
Position:<br>
<input type="text" name="PositionTitle17" value="pos17" /><br>
Supervisor:<br>
<input type="text" name="Supervisor17" value="sup17" /><br>

From:<br>
<input type="text" name="DateFrom36" value="01/03" /><br>
To:<br>
<input type="text" name="DateTo36" value="01/04" /><br>
Position:<br>
<input type="text" name="PositionTitle18" value="pos18" /><br>
Supervisor:<br>
<input type="text" name="Supervisor18" value="sup18" /><br>

<a href="javascript:showadd(10);" {if $smarty.post.add10} style="display:none;" {/if} id="a10show">More</a>
<div {if !$smarty.post.add10} style="display:none;" {/if} id="a10"  >
-----------------------------------------------      10 SF86A      --------------------------------------
<br>
<br>
#7
<br>
From:<br>
<input type="text" name="aDateFrom13" value="01/01" /><br>
Code:<br>
<input type="text" name="aCode4" value="11111" /><br>
...Location:<br>
<input type="text" name="aEmployersVerifierNamesMilitaryDutyLocation1" value="loc1" /><br>
Position:<br>
<input type="text" name="aPositionTitlesMilitaryRank" value="rank1" /><br>

E str address:<br>
<input type="text" name="aEmployersVerifierStreetAddress1" value="add1" /><br>
City:<br>
<input type="text" name="aEmployersVerifierCityCountry1" value="minsk1" /><br>
State:<br>
<input type="text" name="aEmployersVerifierState1" value="MN" /><br>
Zipcode:<br>
<input type="text" name="aEmployersVerifierZIPCode1" value="24356" /><br>
Area code:<br>
<input type="text" name="aAreaCodeOfEmployersVerifier1" value="017" /><br>
Tel:<br>
<input type="text" name="aPhonenOfEmployersVerifier1" value="555-999-5001" /><br>

E str address:<br>
<input type="text" name="aStreetAddressOfJobLocation1" value="jadd1" /><br>
City:<br>
<input type="text" name="aCityCountryOfJobLocation1" value="jminsk1" /><br>
State:<br>
<input type="text" name="aStateOfJobLocation1" value="MN" /><br>
Zipcode:<br>
<input type="text" name="aZIPCodeOfJobLocation1" value="24356" /><br>
Area code:<br>
<input type="text" name="aAreaCodeOfJobLocation1" value="017" /><br>
Tel:<br>
<input type="text" name="aPhonenOfJobLocation1" value="555-999-5001" /><br>

E str address:<br>
<input type="text" name="aSupervisorNamepStreetAddress1" value="sadd1" /><br>
City:<br>
<input type="text" name="aCityCountryOfSupervisor1" value="sminsk1" /><br>
State:<br>
<input type="text" name="aStateOfSupervisor1" value="MN" /><br>
Zipcode:<br>
<input type="text" name="aZIPCodeOfSupervisor1" value="24356" /><br>
Area code:<br>
<input type="text" name="aAreaCodeOfSupervisor1" value="017" /><br>
Tel:<br>
<input type="text" name="aPhonenOfSupervisor1" value="555-999-5001" /><br>

From:<br>
<input type="text" name="aDateFrom14" value="01/01" /><br>
To:<br>
<input type="text" name="aDateTo14" value="01/02" /><br>
Position:<br>
<input type="text" name="aPositionTitle1" value="pos1" /><br>
Supervisor:<br>
<input type="text" name="aSupervisor1" value="sup1" /><br>

From:<br>
<input type="text" name="aDateFrom15" value="01/02" /><br>
To:<br>
<input type="text" name="aDateTo15" value="01/03" /><br>
Position:<br>
<input type="text" name="aPositionTitle2" value="pos1" /><br>
Supervisor:<br>
<input type="text" name="aSupervisor2" value="sup1" /><br>

From:<br>
<input type="text" name="aDateFrom16" value="01/03" /><br>
To:<br>
<input type="text" name="aDateTo16" value="01/04" /><br>
Position:<br>
<input type="text" name="aPositionTitle3" value="pos1" /><br>
Supervisor:<br>
<input type="text" name="aSupervisor3" value="sup1" /><br>

<br>
#8
<br>
From:<br>
<input type="text" name="aDateFrom17" value="01/01" /><br>
Code:<br>
<input type="text" name="aCode5" value="111" /><br>
...Location:<br>
<input type="text" name="aEmployersVerifierNamesMilitaryDutyLocation2" value="loc2" /><br>
Position:<br>
<input type="text" name="aPositionTitlesMilitaryRank2" value="rank2" /><br>

E str address:<br>
<input type="text" name="aEmployersVerifierStreetAddress2" value="add2" /><br>
City:<br>
<input type="text" name="aEmployersVerifierCityCountry2" value="minsk2" /><br>
State:<br>
<input type="text" name="aEmployersVerifierState2" value="MN" /><br>
Zipcode:<br>
<input type="text" name="aEmployersVerifierZIPCode2" value="24326" /><br>
Area code:<br>
<input type="text" name="aAreaCodeOfEmployersVerifier2" value="017" /><br>
Tel:<br>
<input type="text" name="aPhonenOfEmployersVerifier2" value="555-999-5001" /><br>

E str address:<br>
<input type="text" name="aStreetAddressOfJobLocation2" value="jadd2" /><br>
City:<br>
<input type="text" name="aCityCountryOfJobLocation2" value="jminsk2" /><br>
State:<br>
<input type="text" name="aStateOfJobLocation2" value="MN" /><br>
Zipcode:<br>
<input type="text" name="aZIPCodeOfJobLocation2" value="23356" /><br>
Area code:<br>
<input type="text" name="aAreaCodeOfJobLocation2" value="017" /><br>
Tel:<br>
<input type="text" name="aPhonenOfJobLocation2" value="555-999-5001" /><br>

E str address:<br>
<input type="text" name="aSupervisorNamepStreetAddress2" value="sadd2" /><br>
City:<br>
<input type="text" name="aCityCountryOfSupervisor2" value="sminsk2" /><br>
State:<br>
<input type="text" name="aStateOfSupervisor2" value="MN" /><br>
Zipcode:<br>
<input type="text" name="aZIPCodeOfSupervisor2" value="24356" /><br>
Area code:<br>
<input type="text" name="aAreaCodeOfSupervisor2" value="017" /><br>
Tel:<br>
<input type="text" name="aPhonenOfSupervisor2" value="555-999-5001" /><br>

From:<br>
<input type="text" name="aDateFrom18" value="01/01" /><br>
To:<br>
<input type="text" name="aDateTo18" value="01/02" /><br>
Position:<br>
<input type="text" name="aPositionTitle4" value="pos4" /><br>
Supervisor:<br>
<input type="text" name="aSupervisor4" value="sup4" /><br>

From:<br>
<input type="text" name="aDateFrom19" value="01/02" /><br>
To:<br>
<input type="text" name="aDateTo19" value="01/03" /><br>
Position:<br>
<input type="text" name="aPositionTitle5" value="pos5" /><br>
Supervisor:<br>
<input type="text" name="aSupervisor5" value="sup5" /><br>

From:<br>
<input type="text" name="aDateFrom20" value="01/03" /><br>
To:<br>
<input type="text" name="aDateTo20" value="01/04" /><br>
Position:<br>
<input type="text" name="aPositionTitle6" value="pos6" /><br>
Supervisor:<br>
<input type="text" name="aSupervisor6" value="sup6" /><br>

<br>
#9
<br>
From:<br>
<input type="text" name="aDateFrom21" value="01/01" /><br>
Code:<br>
<input type="text" name="aCode6" value="333" /><br>
...Location:<br>
<input type="text" name="aEmployersVerifierNamesMilitaryDutyLocation3" value="loc3" /><br>
Position:<br>
<input type="text" name="aPositionTitlesMilitaryRank3" value="rank3" /><br>

E str address:<br>
<input type="text" name="aEmployersVerifierStreetAddress3" value="add3" /><br>
City:<br>
<input type="text" name="aEmployersVerifierCityCountry3" value="minsk3" /><br>
State:<br>
<input type="text" name="aEmployersVerifierState3" value="MN" /><br>
Zipcode:<br>
<input type="text" name="aEmployersVerifierZIPCode3" value="24326" /><br>
Area code:<br>
<input type="text" name="aAreaCodeOfEmployersVerifier3" value="017" /><br>
Tel:<br>
<input type="text" name="aPhonenOfEmployersVerifier3" value="555-999-5001" /><br>

E str address:<br>
<input type="text" name="aStreetAddressOfJobLocation3" value="jadd3" /><br>
City:<br>
<input type="text" name="aCityCountryOfJobLocation3" value="jminsk3" /><br>
State:<br>
<input type="text" name="aStateOfJobLocation3" value="MN" /><br>
Zipcode:<br>
<input type="text" name="aZIPCodeOfJobLocation3" value="23356" /><br>
Area code:<br>
<input type="text" name="aAreaCodeOfJobLocation3" value="017" /><br>
Tel:<br>
<input type="text" name="aPhonenOfJobLocation3" value="555-999-5001" /><br>

E str address:<br>
<input type="text" name="aSupervisorNamepStreetAddress3" value="sadd3" /><br>
City:<br>
<input type="text" name="aCityCountryOfSupervisor3" value="sminsk3" /><br>
State:<br>
<input type="text" name="aStateOfSupervisor3" value="MN" /><br>
Zipcode:<br>
<input type="text" name="aZIPCodeOfSupervisor3" value="24356" /><br>
Area code:<br>
<input type="text" name="aAreaCodeOfSupervisor3" value="017" /><br>
Tel:<br>
<input type="text" name="aPhonenOfSupervisor3" value="555-999-5001" /><br>

From:<br>
<input type="text" name="aDateFrom22" value="01/01" /><br>
To:<br>
<input type="text" name="aDateTo22" value="01/02" /><br>
Position:<br>
<input type="text" name="aPositionTitle7" value="pos7" /><br>
Supervisor:<br>
<input type="text" name="aSupervisor7" value="sup7" /><br>

From:<br>
<input type="text" name="aDateFrom23" value="01/02" /><br>
To:<br>
<input type="text" name="aDateTo23" value="01/03" /><br>
Position:<br>
<input type="text" name="aPositionTitle8" value="pos8" /><br>
Supervisor:<br>
<input type="text" name="aSupervisor8" value="sup8" /><br>

From:<br>
<input type="text" name="aDateFrom24" value="01/03" /><br>
To:<br>
<input type="text" name="aDateTo24" value="01/04" /><br>
Position:<br>
<input type="text" name="aPositionTitle9" value="pos9" /><br>
Supervisor:<br>
<input type="text" name="aSupervisor9" value="sup9" /><br>

<br>
#10
<br>
From:<br>
<input type="text" name="aDateFrom25" value="01/01" /><br>
Code:<br>
<input type="text" name="aCode7" value="777" /><br>
...Location:<br>
<input type="text" name="aEmployersVerifierNamesMilitaryDutyLocation4" value="loc4" /><br>
Position:<br>
<input type="text" name="aPositionTitlesMilitaryRank4" value="rank4" /><br>

E str address:<br>
<input type="text" name="aEmployersVerifierStreetAddress4" value="add4" /><br>
City:<br>
<input type="text" name="aEmployersVerifierCityCountry4" value="minsk4" /><br>
State:<br>
<input type="text" name="aEmployersVerifierState4" value="MN" /><br>
Zipcode:<br>
<input type="text" name="aEmployersVerifierZIPCode4" value="24326" /><br>
Area code:<br>
<input type="text" name="aAreaCodeOfEmployersVerifier4" value="017" /><br>
Tel:<br>
<input type="text" name="aPhonenOfEmployersVerifier4" value="555-999-5001" /><br>

E str address:<br>
<input type="text" name="aStreetAddressOfJobLocation4" value="jadd4" /><br>
City:<br>
<input type="text" name="aCityCountryOfJobLocation4" value="jminsk4" /><br>
State:<br>
<input type="text" name="aStateOfJobLocation4" value="MN" /><br>
Zipcode:<br>
<input type="text" name="aZIPCodeOfJobLocation4" value="23356" /><br>
Area code:<br>
<input type="text" name="aAreaCodeOfJobLocation4" value="017" /><br>
Tel:<br>
<input type="text" name="aPhonenOfJobLocation4" value="555-999-5001" /><br>

E str address:<br>
<input type="text" name="aSupervisorNamepStreetAddress4" value="sadd4" /><br>
City:<br>
<input type="text" name="aCityCountryOfSupervisor4" value="sminsk4" /><br>
State:<br>
<input type="text" name="aStateOfSupervisor4" value="MN" /><br>
Zipcode:<br>
<input type="text" name="aZIPCodeOfSupervisor4" value="24356" /><br>
Area code:<br>
<input type="text" name="aAreaCodeOfSupervisor4" value="017" /><br>
Tel:<br>
<input type="text" name="aPhonenOfSupervisor4" value="555-999-5001" /><br>

From:<br>
<input type="text" name="aDateFrom26" value="01/01" /><br>
To:<br>
<input type="text" name="aDateTo26" value="01/02" /><br>
Position:<br>
<input type="text" name="aPositionTitle10" value="pos10" /><br>
Supervisor:<br>
<input type="text" name="aSupervisor10" value="sup10" /><br>

From:<br>
<input type="text" name="aDateFrom27" value="01/02" /><br>
To:<br>
<input type="text" name="aDateTo27" value="01/03" /><br>
Position:<br>
<input type="text" name="aPositionTitle11" value="pos11" /><br>
Supervisor:<br>
<input type="text" name="aSupervisor11" value="sup11" /><br>

From:<br>
<input type="text" name="aDateFrom28" value="01/03" /><br>
To:<br>
<input type="text" name="aDateTo28" value="01/04" /><br>
Position:<br>
<input type="text" name="aPositionTitle12" value="pos12" /><br>
Supervisor:<br>
<input type="text" name="aSupervisor12" value="sup12" /><br>


<a href="javascript:hideadd(10);" id="a10hide">Hide More</a>
<input type="hidden" name="add10" value="{$smarty.post.add10}" id="add10" value="0" />
</div>
<br>
<br>
-----------------------------      11     ---------------------------
<br>
#1
<br>
Name:<br>
<input type="text" name="NameOfPersonWhoKnowYouWell1" value="Tim" /><br>
From:<br>
<input type="text" name="DateKnownFrom1" value="10/86" /><br>
To:<br>
<input type="text" name="DateKnownTo1" value="12/08" /><br>
Area:<br>
<input type="text" name="AreaCodeOfPerson1" value="017" /><br>
Tel:<br>
<input type="text" name="PhonenOfPerson1" value="2255889" /><br>
Addr:<br>
<input type="text" name="HomesWorkAddressOfPersonWhoKnowYouWell1" value="testttttt" /><br>
City:<br>
<input type="text" name="CityCountryOfPersonPersonWhoKnowYouWell1" value="Belaruss" /><br>
State:<br>
<input type="text" name="StateOfPersonWhoKnowYouWell1" value="MN" /><br>
Zipcode:<br>
<input type="text" name="ZIPCodeOfPersonWhoKnowYouWell1" value="12578" /><br>
Time:<br>
<input type="radio" name="time1" checked="checked" value="NighttimePhone1" />Night<br>
<input type="radio" name="time1" value="DaytimePhone1" />Day<br>

<br>
#2
<br>
Name:<br>
<input type="text" name="NameOfPersonWhoKnowYouWell2" value="Tim" /><br>
From:<br>
<input type="text" name="DateKnownFrom2" value="10/86" /><br>
To:<br>
<input type="text" name="DateKnownTo2" value="12/08" /><br>
Area:<br>
<input type="text" name="AreaCodeOfPerson2" value="017" /><br>
Tel:<br>
<input type="text" name="PhonenOfPerson2" value="2255889" /><br>
Addr:<br>
<input type="text" name="HomesWorkAddressOfPersonWhoKnowYouWell2" value="testttttt" /><br>
City:<br>
<input type="text" name="CityCountryOfPersonPersonWhoKnowYouWell2" value="Belaruss" /><br>
State:<br>
<input type="text" name="StateOfPersonWhoKnowYouWell2" value="MN" /><br>
Zipcode:<br>
<input type="text" name="ZIPCodeOfPersonWhoKnowYouWell2" value="12578" /><br>
Time:<br>
<input type="radio" name="time2" checked="checked" value="NighttimePhone2" />Night<br>
<input type="radio" name="time2" value="DaytimePhone2" />Day<br>

<br>
#3
<br>
Name:<br>
<input type="text" name="NameOfPersonWhoKnowYouWell3" value="Tim" /><br>
From:<br>
<input type="text" name="DateKnownFrom3" value="10/86" /><br>
To:<br>
<input type="text" name="DateKnownTo3" value="12/08" /><br>
Area:<br>
<input type="text" name="AreaCodeOfPerson3" value="017" /><br>
Tel:<br>
<input type="text" name="PhonenOfPerson3" value="2255889" /><br>
Addr:<br>
<input type="text" name="HomesWorkAddressOfPersonWhoKnowYouWell3" value="testttttt" /><br>
City:<br>
<input type="text" name="CityCountryOfPersonPersonWhoKnowYouWell3" value="Belaruss" /><br>
State:<br>
<input type="text" name="StateOfPersonWhoKnowYouWell3" value="MN" /><br>
Zipcode:<br>
<input type="text" name="ZIPCodeOfPersonWhoKnowYouWell3" value="12278" /><br>
Time:<br>
<input type="radio" name="time3" checked="checked" value="NighttimePhone3" />Night<br>
<input type="radio" name="time3" value="DaytimePhone3" />Day<br>


<br>
<br>
-----------------------------      12     ---------------------------
<br>
... born ...:<br>
<input type="radio" name="born" checked="checked" value="SelectiveServiceYes" />Yes<br>
<input type="radio" name="born" value="SelectiveServiceNo" />No<br>
SSS:<br>
<input type="radio" name="sss" checked="checked" value="SelectiveServiceRegisteredYes" />Yes<br>
<input type="radio" name="sss" value="SelectiveServiceRegisteredNo" />No<br>
Number:<br>
<input type="text" name="RegistrationNumber" value="122578" /><br>
Explanation:<br>
<input type="text" name="LegalExemptionExplanation" value="budu kratok" /><br>

<br>
<br>
-----------------------------      13     ---------------------------
<br>
Military:<br>
<input type="radio" name="military" checked="checked" value="USMilitaryYes" />Yes<br>
<input type="radio" name="military" value="USMilitaryNo" />No<br>
Marine:<br>
<input type="radio" name="marine" checked="checked" value="USMerchantMarineYes" />Yes<br>
<input type="radio" name="marine" value="USMerchantMarineNo" />No<br>


From:<br>
<input type="text" name="DateFrom37" value="10/10/1987" /><br>
To:<br>
<input type="text" name="DateTo37" value="11/10/1987" /><br>
Code:<br>
<input type="text" name="Code10" value="1" /><br>
SC#:<br>
<input type="text" name="ServicesCertificaten1" value="1257" /><br>
Country:<br>
<input type="text" name="Country1" value="Belarus" /><br>
Rank:<br>
<input type="radio" name="rank1" checked="checked" value="Officer1" />Officer<br>
<input type="radio" name="rank1" value="Enlisted1" />Enlisted<br>
Status:<br>
<input type="radio" name="status1" checked="checked" value="Active1" />Active<br>
<input type="radio" name="status1" value="ActiveReserve1" />Active Reserve<br>
<input type="radio" name="status1" value="InactiveReserve1" />Inactive Reserve<br>
<input type="radio" name="status1" value="StatusNationalGuard1" />National Guard<br>
State:<br>
<input type="text" name="StatusNationalGuard1" value="MN" /><br>


From:<br>
<input type="text" name="DateFrom38" value="11/10/1987" /><br>
To:<br>
<input type="text" name="DateTo38" value="12/10/1987" /><br>
Code:<br>
<input type="text" name="Code11" value="3" /><br>
SC#:<br>
<input type="text" name="ServicesCertificaten2" value="1227" /><br>
Country:<br>
<input type="text" name="Country2" value="Belar2s" /><br>
Rank:<br>
<input type="radio" name="rank2" checked="checked" value="Officer2" />Officer<br>
<input type="radio" name="rank2" value="Enlisted2" />Enlisted<br>
Status:<br>
<input type="radio" name="status2" checked="checked" value="Active2" />Active<br>
<input type="radio" name="status2" value="ActiveReserve2" />Active Reserve<br>
<input type="radio" name="status2" value="InactiveReserve2" />Inactive Reserve<br>
<input type="radio" name="status2" value="StatusNationalGuard2" />National Guard<br>
Country:<br>
<input type="text" name="StatusNationalGuard2" value="BY" /><br>

<br>
<br>
-----------------------------      14     ---------------------------
<br>
Drugs:<br>
<input type="radio" name="drugs" checked="checked" value="IllegalDrugsYes" />Yes<br>
<input type="radio" name="drugs" value="IllegalDrugsNo" />No<br>



From:<br>
<input type="text" name="DateFrom39" value="11/10/1987" /><br>
To:<br>
<input type="text" name="DateTo39" value="12/10/1987" /><br>
Type<br>
<input type="text" name="TypeOfSubstance1" value="hashish" /><br>
Explanation:<br>
<input type="text" name="Explanation1" value="don`t know right answer" /><br>


From:<br>
<input type="text" name="DateFrom40" value="11/10/1987" /><br>
To:<br>
<input type="text" name="DateTo40" value="12/10/1987" /><br>
Type<br>
<input type="text" name="TypeOfSubstance2" value="hashish2" /><br>
Explanation:<br>
<input type="text" name="Explanation2" value="don`t know riwerqght answer" /><br>


From:<br>
<input type="text" name="DateFrom41" value="11/10/1987" /><br>
To:<br>
<input type="text" name="DateTo41" value="12/10/1987" /><br>
Type<br>
<input type="text" name="TypeOfSubstance3" value="hashfdsfish" /><br>
Explanation:<br>
<input type="text" name="Explanation3" value="don`t know righwqert answer" /><br>


Continuation Space:<br>
<textarea name="ContinuationSpace">I`m cool guy!</textarea><br>

<br>
<br>
-----------------------------      footer     ---------------------------
<br>
Home AreaCode:<br>
<input type="text" name="HomePhoneAreaCode" value="017" /><br>
Home Phone:<br>
<input type="text" name="HomePhone" value="555-999-5001" /><br>




<!--                  of0306.pdf                      -->



Day AreaCode:<br>
<input type="text" name="dayarea" value="017" /><br>
Day Phone:<br>
<input type="text" name="daytel" value="555-999-5001" /><br>
Night AreaCode:<br>
<input type="text" name="nightarea" value="017" /><br>
Night Phone:<br>
<input type="text" name="nighttel" value="555-999-5001" /><br>

Branch:<br>
<input type="text" name="Branch1" value="asdfdasf" /><br>
From:<br>
<input type="text" name="n8from1" value="12/24/2008" /><br>
To:<br>
<input type="text" name="n8to1" value="12/25/2008" /><br>
Disch:<br>
<input type="text" name="n8discharge1" value="ldfsadfsf" /><br>


Branch:<br>
<input type="text" name="Branch2" value="asdfdasf" /><br>
From:<br>
<input type="text" name="n8from2" value="12/27/2008" /><br>
To:<br>
<input type="text" name="n8to2" value="12/28/2008" /><br>
Disch:<br>
<input type="text" name="n8discharge2" value="ldfsadfsf" /><br>


Branch:<br>
<input type="text" name="Branch3" value="asdfdasf" /><br>
From:<br>
<input type="text" name="n8from3" value="12/25/2008" /><br>
To:<br>
<input type="text" name="n8to3" value="12/26/2008" /><br>
Disch:<br>
<input type="text" name="n8discharge3" value="ldfsadfsf" /><br>


Continuation Space:<br>
<textarea name="n16">Some Text Test</textarea><br>
Last job finish:<br>
<input type="text" name="n18a" value="07/15/2007" /><br>







f9:<br>
<input type="radio" name="f9" checked="checked" value="Yes" />Yes<br>
<input type="radio" name="f9" value="No" />No<br>

f10:<br>
<input type="radio" name="f10" checked="checked" value="Yes" />Yes<br>
<input type="radio" name="f10" value="No" />No<br>

f11:<br>
<input type="radio" name="f11" checked="checked" value="Yes" />Yes<br>
<input type="radio" name="f11" value="No" />No<br>
f12:<br>
<input type="radio" name="f12" checked="checked" value="Yes" />Yes<br>
<input type="radio" name="f12" value="No" />No<br>
f13:<br>
<input type="radio" name="f13" checked="checked" value="Yes" />Yes<br>
<input type="radio" name="f13" value="No" />No<br>
f14:<br>
<input type="radio" name="f14" checked="checked" value="Yes" />Yes<br>
<input type="radio" name="f14" value="No" />No<br>
f15:<br>
<input type="radio" name="f15" checked="checked" value="Yes" />Yes<br>
<input type="radio" name="f15" value="No" />No<br>
8a:<br>
<input type="radio" name="b18" checked="checked" value="Yes" />Yes<br>
<input type="radio" name="b18" value="No" />No<br>
<input type="radio" name="b18" value="DNK" />Don't know<br>
8a:<br>
<input type="radio" name="c18" checked="checked" value="Yes" />Yes<br>
<input type="radio" name="c18" value="No" />No<br>
<input type="radio" name="c18" value="DNK" />Don't know<br>








<input type="submit" name="generate" value="Generate!"><input type="reset" value="Reset" >
<br>

</form>
