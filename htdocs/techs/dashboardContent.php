<!--?php if ($FLSID != "" && $FLSCSP_Rec=="No"){ ?-->
<!-- ONLY LOADS IF WINDOW ALERT NEEDS TO BE SHOWN -->
<script type="text/javascript" src="../library/jquery/interface_floatingWindows.js"></script>
<link rel=stylesheet type ="text/css" href="/templates/www/floatingWindow.css" />
<!--?php } ?-->


<div id="container">


<div id="header">

    <div id="wrapper">
        <div id="content" align="center"></div>
    </div>

<!--- LEFT COLUMN --->
<div id="leftcontent">

<div id="welcomeModule">
<b>Welcome Back <span id="techName"></span></b>
</div>
	
<br> 
   
<div id="" style="padding-left:40px"> 
<table width="425" border="0" cellpadding="0">
  <tr>
    <td>
        
    <style type="text/css">
		div#alert {
			border: 1px solid #174065;
			margin-bottom: 15px;
			padding: 10px;
		}
		#alert ol,
		#alert ul {
			margin: 0 25px;
			}
		#alert ul {
			list-style: none;
		}
		#alert h3 {
			color: red;
			margin-top: -20px;
			}
		#alert h4 {
			margin-bottom: 15px;
		}
		#alert li {
			margin-bottom: 10px;
		}
	</style>
    
<!--
<div style="color: #ff0000; font-weight: bold">Due to the Thanksgiving holiday direct deposit may be late depending  
on your banks policy.
We are sorry for any delays this may cause you. Field Solutions Management</div>
-->
<br/>

<div id="alert">
<h3>Attention All Technicians:</h3>
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b0000b7e8e0e4i5c3e9f3f4g7&Section=Tech%20Dashboard","https:");}catch(v_e){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000b7e8e0e4i5c3e9f3f4g7&Section=Tech%20Dashboard">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>
</div>

<br />

</td>
  </tr>
</table>


<table width="425" border="0" cellpadding="0">
  <tr>
    <td>
        
    <style type="text/css">
		div#alert {
			border: 1px solid #174065;
			margin-bottom: 15px;
			padding: 10px;
		}
		#alert ol,
		#alert ul {
			margin: 0 25px;
			}
		#alert ul {
			list-style: none;
		}
		#alert h3 {
			color: red;
			margin-top: -20px;
			}
		#alert h4 {
			margin-bottom: 15px;
		}
		#alert li {
			margin-bottom: 10px;
		}
	</style>
    
    <div id="alert">
    <h3>Note for FLS Techs</h3>
 <script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script><!-- 193b0000e2f2g7f8c2h8i8b1a8b6 -->
<script type="text/javascript" language="javascript">try{f_cbload("193b0000e2f2g7f8c2h8i8b1a8b6&Section=Tech%20FLS%20Dashboard","https:");}catch(v_e){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000e2f2g7f8c2h8i8b1a8b6&Section=Tech%20FLS%20Dashboard">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>
   </div>
    <br />
</td>
  </tr>
</table><br />
<br />
</div>

<br><br>

</div>



<!-- RIGHT COLUMN -->
<div id="rightcontent">


<br />
<div id="workOrders" align="center" style="padding=10px">
<Script language="JavaScript">
<!-- 
function goto(form) { var index=form.select.selectedIndex
if (form.select.options[index].value != "0") {location=form.select.options[index].value;}}
-->
</script>

<form name="workOrders">
<!--<select name="select" onChange="goto(this.form)" size="1">
<option value="">-----Work Orders-----</option>
<option value="wos.php">FieldSolutions Dashboard</option>
<option value="wosFLS.php">FLS Dashboard</option>
<option value="">------------------------</option>
<option value="wosViewAppliedAll.php">My Applied Work Orders</option>
</select>-->
</form>


</div>

<br />

<!-- Retrieve WOS Open -->	
<!-- Disabled as part of migration -->
<!-- div id="viewIframe" align="center">
<script language="javascript" type="text/javascript">

// Verify flsid exists
if(document.forms[0].EditRecordFLSID.value != ""){

document.write('<a href="ajax/retrieveFLSWOsnotClosed.php?FLSTechID=' + document.forms[0].EditRecordFLSID.value + '" class="iframe w:375 h:200">Loading...</a>');

} 

</script>
<br />
</div -->

<hr style="height: 2px; color:#174065; background:#174065;" noshade="noshade" />
	

<div id="ToDoBlock" style="padding: 10px">

<p><b>To Do:</b></p>

<ul id="todo">

<script language="javascript" type="text/javascript">
var techRefresh = $("input#cbParamVirtual13").attr("value");
if (techRefresh == "No") {
	document.write('<li><b><a href="TechRefresh.php">Tech Refresh</a></b><img src="../images/new.gif" alt="" width="23" height="12"></li></li>');
}

if(document.forms[0].cbParamVirtual2.value == "No"){
document.write('<li><a href="/techs/skills.php">Complete Skills</a></li>');
}
if(document.forms[0].cbParamVirtual3.value == "No"){
document.write('<li><a href="/techs/experience.php">Complete Experience</a></li>');
}
if(document.forms[0].cbParamVirtual4.value == "No"){
document.write('<li><a href="/techs/certifications.php">Complete Certifications</a></li>');
}
//if(document.forms[0].cbParamVirtual5.value == "No"){
//document.write('<li><a href="/techs/media.php">Complete Audio Resume</a></li>');
//}
if(document.forms[0].cbParamVirtual6.value == "No"){
document.write('<li><a href="/techs/equipment.php">Equipment</a></li>');
}
if(document.forms[0].cbParamVirtual7.value == "No"){
document.write('<li><a href="/techs/w9.php">Complete W9</a></li>');
}

if ($("input#cbParamVirtual8").attr("value") == "No") {
	document.write('<li><a href="/techs/Training/Hallmark/POS_Conversion.php">Hallmark POS Conversion Test</a></li>');
}

if ($("input#cbParamVirtual11").attr("value") == "No") {
	document.write('<li><a href="/techs/Training/NCR/NCR_Basic_Certification.php">NCR Basic Certification</a></li>');
}

if ($("input#cbParamVirtual12").attr("value") == "No") {
	document.write('<li><a href="/techs/Training/FLS/Starbucks_Training.php">FLS Starbucks Certification</a></li>');
}

if ($("input#cbParamVirtual9").attr("value") == "No") {
	document.write('<li><a href="FLS_CSP.php">Computing Security Procedures (FLS-CSP) Policy</a></li>');
}

if(document.forms[0].cbParamVirtual10.value == "No"){
document.write('<li><a href="/techs/Training/FieldSolutions/FieldSolutionsTraining.php">Online Tutorial &amp; Field Solutions Certification</a></li>');
}

if(document.forms[0].cbParamVirtual15.value == "No"){
document.write('<li><a href="/techs/Training/Maurices/MauricesTest.php">Maurices Certification</a></li>');
}

if(document.forms[0].cbParamVirtual14.value == "No"){
document.write('<li><a href="/techs/Training/CORE/CORE_Test.php">Core-Techs Certification</a><img src="../images/new.gif" alt="" width="23" height="12"></li>');
}

</script>
    

<li><a href="/techs/cablingSkills.php">CCTV Skills: Update your Cabling Profile Now for New Clients!</a></li>

<li><a href="/techs/telephonySkills.php">Telephony Experience</a></li>

<li><a href="/techs/techFLS.php">FLS</a></li>

</ul>


<br />
</div>
	
<hr style="height: 2px; color:#174065; background:#174065;" noshade="noshade" />
	

<div id="quickLinks" style="padding: 10px">
<p><b>Quick Links:</b></p>

<ul id="todo">
<li><a href="../content/Privacy_Policy.pdf">Privacy Policy</a></li>
<li><a href="../content/Terms_Use.pdf" target="_blank">Terms of Use</a></li>
<li><a href="../content/Field Solutions Independent Contractor Agreement.pdf" target="_blank">Independent Contractor Agreement</a></li>
<li><a href="../content/Members_Code_of_Conduct.pdf" target="_blank">Code of Conduct</a></li>
<li><a href="https://www.fieldsolutions.com/techs/Training/FS_PaymentPolicy.php" target="_blank">Field Solutions Payment Policy</a></li>
<li><a href="/techSignup.php">Get to Work in 3 Steps</a></li>
<li><a href="w9.php">Submit your W-9 form</a></li>
</ul>
</div>





	<br />

<hr style="height: 2px; color:#174065; background:#174065;" noshade="noshade" />

<img src="images/pixel.gif" height="100px;" width="1">
</div>

</div>


<?php if ($FLSID != "" && $FLSCSP_Rec=="No"){ ?>
<!-- ONLY LOADS IF WINDOW ALERT NEEDS TO BE SHOWN -->
<!-- Floating Window -->
<div id="window">
	<div id="windowTop">
		<div id="windowTopContent">FLS - Computing Security Procedures (FLS-CSP) Policy</div>
		<img src="images/window_close.jpg" id="windowClose" />
	</div>
	<div id="windowBottom"><div id="windowBottomContent">&nbsp;</div></div>

<div id="windowContent">
<!--
<p align="center">
	<b>FLS &ndash; Computing Security Procedures (FLS-CSP) Policy</b>	</p>-->
	<p>
	As of March 1 2008, Fujitsu Transactions Services (FTXS)  is requiring all First Line Support (FLS) technicians to review, sign and submit the FLS-CSP form in order to be eligible to run calls or work projects/installs for FLS. Effective immediately, all new FLS technicians will be required to complete the FLS-CSP.<br>
	<br>
	The FLS-CSP form helps assure FTXS that FLS technicians working for them understand the following:<br>
	<br>
	As a contracted technician working for FLS, you agree to adhere to customer policies, regulations and procedures governing the security of their computers and associated data that may be provided to you by the customer.  You will be acknowledging and agreeing to not access information for any reason other than what is necessary to complete the job/task.
	<p><b>Note:</b> The FLS-CSP form that you need to download below is in PDF. Meaning, if you do not already have it, you will need the <a href="http://www.adobe.com/products/acrobat/readstep2.html" target=_blank>Adobe Reader</a> software to view and print out the form for faxing. You can download the required Adobe Reader software for free by <a href="http://www.adobe.com/products/acrobat/readstep2.html" target=_blank>
		clicking here</a>.
	<p align="center"><img height=23 alt="" src="images/pdf.jpg" width=23 border=0> <b>FLS-CSP Form</b> / <a href="forms/FLSCSP.pdf">Click here</a> to download <br>	</p>
<div align="center">
		<ul>
		  <li>Fax your completed and signed FLS-CSP form to: 888-870-1182		  </li>
		  </ul></div>
		<p>We are accepting only faxes of the Disclosure form.  Do not email it.</p>
    <p>Once we have received your signed FLS-CSP form, we will check your profile as having successfully received the form, within 3 business days.</p>
    <p>Questions? Please contact Field Solutions support at: <a href="mailto:support@fieldsolutions.com">support@fieldsolutions.com</a></p>
    <p align="center">&nbsp; </p>

	  
</div>
	<img src="images/window_resize.gif" id="windowResize" />

</div>
<?php }?>

<!-- 2ND POPUP WINDOW -->

<!--div id="window_BLUE">
	<div id="windowTop_BLUE">
		<div id="windowTopContent_BLUE">FieldSolutions Announces new FS-Rush&trade; Assignment tool and Added Suppliers from Work Market</div>
		<img src="images/window_close.jpg" id="windowClose_Two" />
	</div>
<div id="windowBottom_BLUE"><div id="windowBottomContent_BLUE">&nbsp;</div></div>

<div id="windowContent_BLUE">
<p>September 16, 2013 - FieldSolutions, North America's premier contingent technology field resource provider releases FS-Rush&trade; a new first bidder automatic assignment tool. FS-Rush clients will be publishing work and assigning it to the first matching applicant.</p>

<p>Additionally, now if you are registered as a provider on Work Market through our new alliance, you MAY also see work orders appearing on that platform via skill specific FieldSolutions 'groups'. This work will always be posted in the FieldSolutions system as well, and with the addition of providers in Work Market speed to bid will be more competitive.  Regardless of whether the application comes through FieldSolutions or from eligible providers on Work Market, the FS-Rush work orders will be auto-assigned to the first matching applicant.</p>

<p><span style="font-weight: bold"FS-Rush&trade;: New Word, Faster Response Needed</span>
<ul>
	<li>Clients engaged in the break/fix and maintenance work depend on fast response times from technicians to meet strict on-site service level agreements (SLAs)</li>
    <li>The expanded provider base, now including Work Market providers, may result in faster work order assignment</li>
    <li>The provider will be paid through the platform they bid on. Pay for work completed through Work Market will be made 30 days after client approval. Pay through FieldSolutions will remain the week after client approval.</li>
    <li>To bid faster and win more work, download FS-Mobile, FieldSolutions mobile smartphone app for iPhone and Android and bid while working.</li>
</ul>
</p>

<p>
Questions? Email: <a href="mailto:Support@FieldSolutions.com">Support@FieldSolutions.com</a>
</p>

<p>Thank you,<br/>
Your FieldSolutions Team
</p>
</div>
<img src="images/window_resize.gif" id="windowResize_Two" />
</div-->

<?php if ($FLSID != "" && true){ ?>
<div id="window_RED">
	<div id="windowTop_RED">
		<div id="windowTopContent_RED">IMPORTANT - MUST READ NOTE AND LINKED DOCUMENT</div>
		<img src="images/window_close.jpg" id="windowClose_Three" />
	</div>
<div id="windowBottom_RED"><div id="windowBottomContent_RED">&nbsp;</div></div>

<div id="windowContent_RED" align="center">
<b><p style="color:#F00;">IMPORTANT - MUST READ NOTE AND LINKED DOCUMENT</p>
<p>THIS NOTE IS DUE TO MULTIPLE ISSUES IN THE PAST MONTH THAT HAVE NEGATIVILY IMPACTED FUJITSU'S IMAGE AND HAS RESULTED IN TECHNICIANS BEING ADMINISTRATIVELY DENIED FROM THE FIRST LINE SUPPORT PROGRAM AND FIELDSOLUTIONS.</p>
<p>Technicians removed for being late for accepted task.</p>
<P>Technician was removed for inappropriate conversation and swearing.</P>
<P>Technician removed for wearing shorts and sandals to an install.</P>
<P>Technician removed for falsifying information.</P>
<P>The linked Fujitsu Policy - Customer Service and Code of Conduct Document must be read before future work is assigned.</p>
<p><a href="/techs/Training/FLS/fujitsu policy.doc">Fujitsu Policy - Customer Service and Code of Conduct</a></p></b>
<!--p>Thanks,<br />
The Field Solutions Team</p-->
</div>

<img src="images/window_resize.gif" id="windowResize_Three" />
</div>
<?php }?>



<!-- ********************** MAINTANENANCE MESSAGE **************************** -->
<!--div id="window_RED">
	<div id="windowTop_RED">
		<div id="windowTopContent_RED">ALL FIELD SOLUTIONS USERS</div>
		<img src="images/window_close.jpg" id="windowClose_Three" />
	</div>
<div id="windowBottom_RED"><div id="windowBottomContent_RED">&nbsp;</div></div>

<div id="windowContent_RED">
<p>Our site will be offline for maintenance between 8:00 PM and midnight Central time this Saturday, June 5. We appreciate your patience.</p>
<p>Thanks,<br />
The Field Solutions Team</p>
</div>

<img src="images/window_resize.gif" id="windowResize_Two" />
</div-->
<!-- ********************** MAINTANENANCE MESSAGE **************************** -->




<script type="text/javascript">
if (techRefresh == "No") {
document.write('<div id="window_Two">' +
    '<div id="windowTop_Two">' +
        '<div id="windowTopContent_Two">Urgent! - New Skills Update Today!</div>' +
        '<img src="images/window_close.jpg" id="windowClose_Two" />' +
    '</div>' +
'<div id="windowBottom_Two"><div id="windowBottomContent_Two">&nbsp;</div></div>' +
'<div id="windowContent_Two">' +
     '<p>New Clients want different skills. Update yours now. Click<a href="TechRefresh.php">here</a></p>' +
    '<img src="images/window_resize.gif" id="windowResize_Two" />' +
'</div>');
}
</script>
<!--- End Content --->


<?php if ($FLSID != "" && $FLSCSP_Rec=="No"){ ?>
<!-- ONLY LOADS IF WINDOW ALERT NEEDS TO BE SHOWN -->
<script type="text/javascript">
$(document).ready(
  function()
  {
    $('#windowOpen').bind(
      'click',
      function() {
        if($('#window').css('display') == 'none') {
          $(this).TransferTo(
            {
              to:'window',
              className:'transferer2', 
              duration: 400,
              complete: function()
              {
                $('#window').show();
              }
            }
          );
        }
        this.blur();
        return false;
      }
    );
    $('#windowClose').bind(
      'click',
      function()
      {
        $('#window').TransferTo(
          {
            to:'windowOpen',
            className:'transferer2', 
            duration: 400
          }
        ).hide();
      }
    );
    $('#windowMin').bind(
      'click',
      function()
      {
        $('#windowContent').SlideToggleUp(300);
        $('#windowBottom, #windowBottomContent').animate({height: 10}, 300);
        $('#window').animate({height:40},300).get(0).isMinimized = true;
        $(this).hide();
        $('#windowResize').hide();
        $('#windowMax').show();
      }
    );
    $('#windowMax').bind(
      'click',
      function()
      {
        var windowSize = $.iUtil.getSize(document.getElementById('windowContent'));
        $('#windowContent').SlideToggleUp(300);
        $('#windowBottom, #windowBottomContent').animate({height: windowSize.hb + 13}, 300);
        $('#window').animate({height:windowSize.hb+43}, 300).get(0).isMinimized = false;
        $(this).hide();
        $('#windowMin, #windowResize').show();
      }
    );
    $('#window').Resizable(
      {
        minWidth: 200,
        minHeight: 60,
        maxWidth: 700,
        maxHeight: 400,
        dragHandle: '#windowTop',
        handlers: {
          se: '#windowResize'
        },
        onResize : function(size, position) {
          $('#windowBottom, #windowBottomContent').css('height', size.height-33 + 'px');
          var windowContentEl = $('#windowContent').css('width', size.width - 25 + 'px');
          if (!document.getElementById('window').isMinimized) {
            windowContentEl.css('height', size.height - 48 + 'px');
          }
        }
      }
    );
  }
);
</script>
<?php } ?>



<!-- 2ND POPUP WINDOW -->
<script type="text/javascript">
if (techRefresh == "No") {
$(document).ready(
  function()
  {
    $('#windowOpen_Two').bind(
      'click',
      function() {
        if($('#window_Two').css('display') == 'none') {
          $(this).TransferTo(
            {
              to:'window_Two',
              className:'transferer_Two', 
              duration: 400,
              complete: function()
              {
                $('#window_Two').show();
              }
            }
          );
        }
        this.blur();
        return false;
      }
    );
    $('#windowClose_Two').bind(
      'click',
      function()
      {
        $('#window_Two').TransferTo(
          {
            to:'windowOpen_Two',
            className:'transferer_Two', 
            duration: 400
          }
        ).hide();
      }
    );
    $('#windowMin').bind(
      'click',
      function()
      {
        $('#windowContent_Two').SlideToggleUp(300);
        $('#windowBottom_Two, #windowBottomContent_Two').animate({height: 10}, 300);
        $('#window_Two').animate({height:40},300).get(0).isMinimized = true;
        $(this).hide();
        $('#windowResize_Two').hide();
        $('#windowMax_Two').show();
      }
    );
    $('#windowMax_Two').bind(
      'click',
      function()
      {
        var windowSize_Two = $.iUtil.getSize(document.getElementById('windowContent_Two'));
        $('#windowContent_Two').SlideToggleUp(300);
        $('#windowBottom_Two, #windowBottomContent_Two').animate({height: windowSize.hb + 13}, 300);
        $('#window_Two').animate({height:windowSize_Two.hb+43}, 300).get(0).isMinimized = false;
        $(this).hide();
        $('#windowMin_Two, #windowResize_Two').show();
      }
    );
    $('#window_Two').Resizable(
      {
        minWidth: 200,
        minHeight: 60,
        maxWidth: 700,
        maxHeight: 400,
        dragHandle: '#windowTop_Two',
        handlers: {
          se: '#windowResize_Two'
        },
        onResize : function(size, position) {
          $('#windowBottom_Two, #windowBottomContent_Two').css('height', size.height-33 + 'px');
          var windowContentEl_Two = $('#windowContent_Two').css('width', size.width - 25 + 'px');
          if (!document.getElementById('window_Two').isMinimized) {
            windowContentEl_Two.css('height', size.height - 48 + 'px');
          }
        }
      }
    );
  }
);
}
</script>


<!-- 3RD POPUP WINDOW -->
<script type="text/javascript">
/*
$(document).ready(
  function()
  {
    $('#windowOpen_BLUE').bind(
      'click',
      function() {
        if($('#window_BLUE').css('display') == 'none') {
          $(this).TransferTo(
            {
              to:'window_BLUE',
              className:'transferer_Two', 
              duration: 400,
              complete: function()
              {
                $('#window_BLUE').show();
              }
            }
          );
        }
        this.blur();
        return false;
      }
    );
    $('#windowClose_Two').bind(
      'click',
      function()
      {
        $('#window_BLUE').TransferTo(
          {
            to:'windowOpen_BLUE',
            className:'transferer_Two', 
            duration: 400
          }
        ).hide();
      }
    );
    $('#windowMin').bind(
      'click',
      function()
      {
        $('#windowContent_BLUE').SlideToggleUp(300);
        $('#windowBottom_BLUE, #windowBottomContent_BLUE').animate({height: 10}, 300);
        $('#window_BLUE').animate({height:40},300).get(0).isMinimized = true;
        $(this).hide();
        $('#windowResize_BLUE').hide();
        $('#windowMax_BLUE').show();
      }
    );
    $('#windowMax_BLUE').bind(
      'click',
      function()
      {
        var windowSize_BLUE = $.iUtil.getSize(document.getElementById('windowContent_BLUE'));
        $('#windowContent_BLUE').SlideToggleUp(300);
        $('#windowBottom_BLUE, #windowBottomContent_BLUE').animate({height: windowSize.hb + 13}, 300);
        $('#window_BLUE').animate({height:windowSize_BLUE.hb+43}, 300).get(0).isMinimized = false;
        $(this).hide();
        $('#windowMin_BLUE, #windowResize_BLUE').show();
      }
    );
    $('#window_BLUE').Resizable(
      {
        minWidth: 200,
        minHeight: 60,
        maxWidth: 700,
        maxHeight: 400,
        dragHandle: '#windowTop_BLUE',
        handlers: {
          se: '#windowResize_Two'
        },
        onResize : function(size, position) {
          $('#windowBottom_BLUE, #windowBottomContent_BLUE').css('height', size.height-33 + 'px');
          var windowContentEl_BLUE = $('#windowContent_BLUE').css('width', size.width - 25 + 'px');
          if (!document.getElementById('window_BLUE').isMinimized) {
            windowContentEl_BLUE.css('height', size.height - 48 + 'px');
          }
        }
      }
    );
  }
);
*/
</script>

<?php if ($FLSID != "") { ?>

<script type="text/javascript">

$(document).ready(
  function()
  {
    $('#windowOpen_RED').bind(
      'click',
      function() {
        if($('#window_RED').css('display') == 'none') {
          $(this).TransferTo(
            {
              to:'window_RED',
              className:'transferer_Two', 
              duration: 400,
              complete: function()
              {
                $('#window_RED').show();
              }
            }
          );
        }
        this.blur();
        return false;
      }
    );
    $('#windowClose_Three').bind(
      'click',
      function()
      {
        $('#window_RED').TransferTo(
          {
            to:'windowOpen_RED',
            className:'transferer_Two', 
            duration: 400
          }
        ).hide();
      }
    );
    $('#windowMin').bind(
      'click',
      function()
      {
        $('#windowContent_RED').SlideToggleUp(300);
        $('#windowBottom_RED, #windowBottomContent_RED').animate({height: 10}, 300);
        $('#window_RED').animate({height:40},300).get(0).isMinimized = true;
        $(this).hide();
        $('#windowResize_RED').hide();
        $('#windowMax_RED').show();
      }
    );
    $('#windowMax_RED').bind(
      'click',
      function()
      {
        var windowSize_RED = $.iUtil.getSize(document.getElementById('windowContent_RED'));
        $('#windowContent_RED').SlideToggleUp(300);
        $('#windowBottom_RED, #windowBottomContent_RED').animate({height: windowSize.hb + 13}, 300);
        $('#window_RED').animate({height:windowSize_RED.hb+43}, 300).get(0).isMinimized = false;
        $(this).hide();
        $('#windowMin_RED, #windowResize_RED').show();
      }
    );
    $('#window_RED').Resizable(
      {
        minWidth: 200,
        minHeight: 60,
        maxWidth: 700,
        maxHeight: 400,
        dragHandle: '#windowTop_RED',
        handlers: {
          se: '#windowResize_Two'
        },
        onResize : function(size, position) {
          $('#windowBottom_RED, #windowBottomContent_RED').css('height', size.height-33 + 'px');
          var windowContentEl_RED = $('#windowContent_RED').css('width', size.width - 25 + 'px');
          if (!document.getElementById('window_RED').isMinimized) {
            windowContentEl_RED.css('height', size.height - 48 + 'px');
          }
        }
      }
    );
  }
);

</script>
<?php } ?>


<script type="text/javascript">

$(document).ready(
  function()
  {
    $('#windowOpen_GREEN').bind(
      'click',
      function() {
        if($('#window_GREEN').css('display') == 'none') {
          $(this).TransferTo(
            {
              to:'window_GREEN',
              className:'transferer_Two', 
              duration: 400,
              complete: function()
              {
                $('#window_GREEN').show();
              }
            }
          );
        }
        this.blur();
        return false;
      }
    );
    $('#windowClose_Three').bind(
      'click',
      function()
      {
        $('#window_GREEN').TransferTo(
          {
            to:'windowOpen_GREEN',
            className:'transferer_Two', 
            duration: 400
          }
        ).hide();
      }
    );
    $('#windowMin').bind(
      'click',
      function()
      {
        $('#windowContent_GREEN').SlideToggleUp(300);
        $('#windowBottom_GREEN, #windowBottomContent_GREEN').animate({height: 10}, 300);
        $('#window_GREEN').animate({height:40},300).get(0).isMinimized = true;
        $(this).hide();
        $('#windowResize_GREEN').hide();
        $('#windowMax_GREEN').show();
      }
    );
    $('#windowMax_GREEN').bind(
      'click',
      function()
      {
        var windowSize_GREEN = $.iUtil.getSize(document.getElementById('windowContent_GREEN'));
        $('#windowContent_GREEN').SlideToggleUp(300);
        $('#windowBottom_GREEN, #windowBottomContent_GREEN').animate({height: windowSize.hb + 13}, 300);
        $('#window_GREEN').animate({height:windowSize_GREEN.hb+43}, 300).get(0).isMinimized = false;
        $(this).hide();
        $('#windowMin_GREEN, #windowResize_GREEN').show();
      }
    );
    $('#window_GREEN').Resizable(
      {
        minWidth: 200,
        minHeight: 60,
        maxWidth: 700,
        maxHeight: 400,
        dragHandle: '#windowTop_GREEN',
        handlers: {
          se: '#windowResize_Two'
        },
        onResize : function(size, position) {
          $('#windowBottom_GREEN, #windowBottomContent_GREEN').css('height', size.height-33 + 'px');
          var windowContentEl_GREEN = $('#windowContent_GREEN').css('width', size.width - 25 + 'px');
          if (!document.getElementById('window_GREEN').isMinimized) {
            windowContentEl_GREEN.css('height', size.height - 48 + 'px');
          }
        }
      }
    );
  }
);

</script>