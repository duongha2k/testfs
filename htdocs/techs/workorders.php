<?php $page = techs; ?>
<?php $option = wos; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>

<?php
	require_once("../library/caspioAPI.php");
	
//	$today = date("m/d/Y");
	$dayOfWeek = date("w"); // 0 = Sunday, 6 = Saturday

	$startWeek = date("m/d/Y", strtotime("-$dayOfWeek days")); // Sunday
	$endWeek = date("m/d/Y", strtotime("This Saturday")); // Saturday
	
	$startMonth = date("m/1/Y"); 
	$endMonth = date("m/d/Y", strtotime("$startMonth + 1 month - 1 day")); 
	
	if (isset($_GET["v"])) {
		$techID = $_GET["v"];
		if (!is_numeric($techID)) die();
/*//		$projectList = caspioSelectAdv("TR_Client_Projects", "DISTINCT Project_Name, Project_ID", "active = '1'", "Project_Name ASC", false, "`", "|");		
		$projectList = caspioSelectAdv("Work_Orders", "DISTINCT Project_Name, Project_ID", "Tech_ID = '$techID'", "Project_Name ASC", false, "`", "|");		
		$projectHtml = "<option value=\"None\" selected=\"selected\">Select Project</option>";
		foreach ($projectList as $project) {
			$project = explode("|", $project);
			$name = trim($project[0], "`");
			$id = trim($project[1], "`");
			$projectHtml .= "<option value=\"$id\">$name</option>";
		}*/
		
		$woAllOpen = 0;
		$woThisMonth = 0;
		$woThisWeek = 0;
		$woToday = 0;
				
		try {
/*			$count = caspioSelectAdv("Work_Orders", "count(WO_ID)", "Tech_ID = '$techID' AND Deactivated = '0'", "", false, "`", "|", false);
			$woAllOpen = $count[0];

			if ($woAllOpen > 0) {
				$count = caspioSelectAdv("Work_Orders", "count(WO_ID)", "Tech_ID = '$techID' AND StartDate >= '$startMonth' AND StartDate <= '$endMonth' AND Deactivated = '0'", "", false, "`", "|", false);	
				$woThisMonth = $count[0];
			}
					
			if ($woThisMonth > 0) {
				$count = caspioSelectAdv("Work_Orders", "count(WO_ID)", "Tech_ID = '$techID' AND StartDate >= '$startWeek' AND StartDate <= '$endWeek' AND Deactivated = '0'", "", false, "`", "|", false);
				$woThisWeek = $count[0];
			}
	
/*			if ($woToday > 0) {
				$count = caspioSelectAdv("Work_Orders", "count(WO_ID)", "Company_ID = '$companyID' AND StartDate = '$today' AND Deactivated = '0'", "", false, "`", "|", false);
				$woToday = $count[0];
			}*/
		}
		catch (SoapFault $fault) {
		}
	}
	
?>


<!-- Add Content Here -->

<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193B0000E9C6D4G0I3E9C6D4G0I3","https:");}catch(ilolli){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193B0000E9C6D4G0I3E9C6D4G0I3">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>
<!--<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b0000e8b4c0a9h0c2c2f4g8a0","https:");}catch(v_e){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000e8b4c0a9h0c2c2f4g8a0">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>-->

<!-- Check if logged in, if not redirect -->
<script type="text/javascript">
	try {
		var test = v;
		if (v != "<?=$_GET["v"]?>")
			window.location.replace("<?=$_SERVER['PHP_SELF']?>?v=" + v);
	}
	catch (e) {
		window.location.replace("./");
	}
</script>

<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript">

	var startWeek = "<?=$startWeek?>";
	var endWeek = "<?=$endWeek?>";
	var startMonth = "<?=$startMonth?>";
	var endMonth = "<?=$endMonth?>";

	var currentTab = "";

	$(document).ready(function(){
		$('#StartDate').calendar({dateFormat: 'MDY/'});
		$('#EndDate').calendar({dateFormat: 'MDY/'});
	});

	function openTab(tab) {
		try {
			document.getElementById(currentTab).className = "";
		}
		catch (e) {
		}
		try {
			document.getElementById(tab).className = "current";
			currentTab = tab;
		}
		catch(e) {
			currentTab = "All";
		}
		reloadTabFrame();
	}
	
	function reloadTabFrame() {
		// frames['tabFrame'] - accessing object
		// document.getElementById("tabFrame") - accessing html element
		url = "";
		projectId = document.forms.dashFilter.Project.value;
		if (projectId == "None") projectId = "";
		startDate = document.forms.dashFilter.StartDate.value;
		endDate = document.forms.dashFilter.EndDate.value;
		switch (currentTab) {
			case "assigned":
				url = "assignedTab.php";
				break;
			case "workdone":
				url = "workDoneTab.php";
				break;
			case "approved":
				url = "approvedTab.php";
				break;
			case "inaccounting":
				url = "inAccountingTab.php";
				break;
			case "paid":
				url = "paidTab.php";
				break;
			case "incomplete":
				url = "incompleteTab.php";
				break;
			case "all":
				url = "allTab.php";
				break;
			default:
				openTab("all");
				return;
				break;				
		}
		document.getElementById("tabFrame").src = url + "?cbResetParam=1&Project_ID=" + projectId + "&StartDate=" + startDate + "&EndDate=" + endDate;
	}
	
	function resizeTabFrame(width, height) {
		tabFrame = document.getElementById("tabFrame");
		myParent = tabFrame.parentNode;
		try {
			// FF version
			tabFrame.style.width = (width + 40) + "px";
			tabFrame.style.height = (height + 20) + "px";
		}
		catch (e) {
			// IE version
		}
	}
	
	function showToday() {
		document.getElementById("StartDate").value = today;
		document.getElementById("EndDate").value = today;
		reloadTabFrame();
	}
	
	function showWeek() {
		document.getElementById("StartDate").value = startWeek;
		document.getElementById("EndDate").value = endWeek;
		reloadTabFrame();
	}
	
	function showMonth() {
		document.getElementById("StartDate").value = startMonth;
		document.getElementById("EndDate").value = endMonth;
		reloadTabFrame();
	}

	function showAllOpen() {
		document.getElementById("StartDate").value = "";
		document.getElementById("EndDate").value = "";
		reloadTabFrame();
	}

	
</script>

<div id="container">
<div id="header">

    <div id="wrapper">
        <div id="content" align="center"></div>
    </div>


 
<!--- LEFT COLUMN --->
<div id="DashboardLeftContent">

<div id="leftMenu">
<div class="leftMenuSubHeader">WOs Assigned to Me</div>
<ul>
<li><nobr><a href="javascript:showWeek()">Today's WOs (5)</a> (<?=$woThisWeek?>)</nobr></li>
<li><nobr><a href="javascript:showWeek()">This Week's WOs</a> (<?=$woThisWeek?>)</nobr></li>
<!--<li><nobr><a href="javascript:showNextWeek()">Next Week's WOs</a> (<?=$woNextWeek?>)</nobr></li>-->
<li><nobr><a href="javascript:showMonth()">This Month's WOs</a> (<?=$woThisMonth?>)</nobr></li>
<!--<li><nobr><a href="javascript:showCompleted()">Completed WOs</a> (<?=$woCompleted?>)</nobr></li>-->
<li><nobr><a href="javascript:showAllOpen()">All WOs</a> (<?=$woAllOpen?>)</nobr></li>
<br />
<li><nobr><a href="">Find a Work Order</a></nobr></li>
</ul>
<br /><br />


<div class="leftMenuSubHeader">FLS WOs Assigned to Me</div>
<ul>
<li><nobr><a href="javascript:showWeek()">Today's WOs (5)</a> (<?=$woThisWeek?>)</nobr></li>
<li><nobr><a href="javascript:showWeek()">This Week's WOs</a> (<?=$woThisWeek?>)</nobr></li>
<li><nobr><a href="javascript:showNextWeek()">Next Week's WOs</a> (<?=$woNextWeek?>)</nobr></li>
<li><nobr><a href="javascript:showThisMonth()">This Month's WOs</a> (<?=$woThisMonth?>)</nobr></li>
<li><nobr><a href="javascript:showCompleted()">Completed WOs</a> (<?=$woCompleted?>)</nobr></li>
<li><nobr><a href="javascript:showAll()">All WOs</a> (<?=$woAllOpen?>)</nobr></li>
<br />
<li><nobr><a href="">Find a Work Order</a></nobr></li>
</ul>
<br /><br />

<div class="leftMenuSubHeader">Available WOs</div>
<ul>
<li><nobr><a href="javascript:showWeek()">Today's WOs (5)</a> (<?=$woThisWeek?>)</nobr></li>
<li><nobr><a href="javascript:showWeek()">This Week's WOs</a> (<?=$woThisWeek?>)</nobr></li>
<li><nobr><a href="javascript:showNextWeek()">Next Week's WOs</a> (<?=$woNextWeek?>)</nobr></li>
<li><nobr><a href="javascript:showThisMonth()">This Month's WOs</a> (<?=$woThisMonth?>)</nobr></li>
<li><nobr><a href="javascript:showAll()">All WOs</a> (<?=$woAllOpen?>)</nobr></li>
<br />
<li><nobr><a href="">Requested WOs</a> (150)</nobr></li>
</ul>
<br /><br />


<div class="leftMenuSubHeader">FLS Available WOs</div>
<ul>
<li><nobr><a href="javascript:showWeek()">Today's WOs (5)</a> (<?=$woThisWeek?>)</nobr></li>
<li><nobr><a href="javascript:showWeek()">This Week's WOs</a> (<?=$woThisWeek?>)</nobr></li>
<li><nobr><a href="javascript:showNextWeek()">Next Week's WOs</a> (<?=$woNextWeek?>)</nobr></li>
<li><nobr><a href="javascript:showThisMonth()">This Month's WOs</a> (<?=$woThisMonth?>)</nobr></li>
<li><nobr><a href="javascript:showAll()">All WOs</a> (<?=$woAllOpen?>)</nobr></li>
<br />
<li><nobr><a href="">Requested WOs</a> (150)</nobr></li>
</ul>
<br /><br />

</div>

<br /><br /><br /><br /><br /><br />
</div>

<!--- DASHBOARD FILTER --->
<div id="projectSearch" align="center">
<form id="dashFilter" name="dashFilter" action="<?=$_SERVER['PHP_SELF']?>?v=<?=$_GET["v"]?>" method="post">
<select id="Project" name="Project">
<?=$projectHtml?>
</select>          

<span style="font-weight: bold">Between</span>
<input type="text" name="StartDate" id="StartDate">

<span style="font-weight: bold">And</span>

<input type="text" name="EndDate" id="EndDate">
<input type="button" name="ReloadTabFrame" onClick="reloadTabFrame()" value="Refresh">
</form>
</div>
    
<!--- RIGHT COLUMN --->
<div id="DashboardRightContent">

<div id="tabbedMenu">
<ul>
<li id="assigned"><a href="javascript:openTab('assigned')">Assigned (1)</a></li>
<li id="workdone"><a href="javascript:openTab('workdone')">Work Done (1)</a></li>
<li id="approved"><a href="javascript:openTab('approved')">Approved(1)</a></li>
<li id="inaccounting"><a href="javascript:openTab('inaccounting')">In Accounting(1)</a></li>
<li id="paid"><a href="javascript:openTab('paid')">Paid (1)</a></li>
<li id="incomplete"><a href="javascript:openTab('incomplete')">Incomplete (0)</a></li>
<li id="all"><a href="javascript:openTab('all')">All (5)</a></li>
</ul>
</div>   

<br />

<div class="formHeader" align="center" style="">
	<iframe id="tabFrame" name="tabFrame" class="tabFrame" src="../blank.html" style="width: 80%; height: 100%"></iframe>
</div>    
   
<br /><br /><br /><br /><br /><br />       
</div>

<br />

</div>
	
<!--- End Content --->
<?php require ("../footer.php"); ?>

