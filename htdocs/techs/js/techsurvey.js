//-------------------------------------------------------------
techsurvey.Instances = null;
//-------------------------------------------------------------
techsurvey.CreateObject = function(config) {
    var obj = null;
    if (techsurvey.Instances !== null) {
        obj = techsurvey.Instances[config.id];
    }
    if (obj === null) {
        if (techsurvey.Instances === null) {
            techsurvey.Instances = new Object();
        }
        obj = new techsurvey(config);
        techsurvey.Instances[config.id] = obj;
    }

    obj.init();
    return obj;
};
//-------------------------------------------------------------
function techsurvey(config) {
    var Me = this;
    var win = config.win;
    this._roll = new FSPopupRoll();
    //var detailObject = new functions();
//    var _arrSB_Bline = ["S3Q1b_SB_Bline_Num", "S3Q1c_SB_Fline_Num", "S3Q1d_SB_DThru_Num", "S3Q1e_SB_Others_Num"];
//    var _arrSB_BlineL = ["S3Q1b_SB_Bline_WLeftHW_Num", "S3Q1c_SB_Fline_WLeftHW_Num", "S3Q1d_SB_DThru_WLeftHW_Num", "S3Q1e_SB_Others_WLeftHW_Num"];
//    var _arrSB_BlineR = ["S3Q1b_SB_Bline_WRightHW_Num", "S3Q1c_SB_Fline_WRightHW_Num", "S3Q1d_SB_DThru_WRightHW_Num", "S3Q1e_SB_Others_WRightHW_Num"];
    var _S4Q8 = {"S4Q8_8_a_Num": "S4Q8_8_a_Num", "S4Q8_8_b_Num": "S4Q8_8_b_Num", "S4Q8_8_c_Num": "S4Q8_8_c_Num", "S4Q8_8_d_Num": "S4Q8_8_d_Num"};
    var _S4Q9 = {"S4Q9_9_a_Num": "S4Q9_9_a_Num", "S4Q9_9_b_Num": "S4Q9_9_b_Num", "S4Q9_9_c_Num": "S4Q9_9_c_Num", "S4Q9_9_d_Num": "S4Q9_9_d_Num"};
    var _S4Q10a = {"S4Q10_10_a_Num": "S4Q10_10_a_Num", "S4Q10_10_b_Num": "S4Q10_10_b_Num", "S4Q10_10_c_Num": "S4Q10_10_c_Num", "S4Q10_10_d_Num": "S4Q10_10_d_Num"};
    var _S4Q10b = {"S4Q10_10_a_Num": "S4Q10_10_a_Num", "S4Q10_10_c_Num": "S4Q10_10_c_Num", "S4Q10_10_d_Num": "S4Q10_10_d_Num"};
    var _S4Q13 = {"1": "S4Q13a_BOutlet1_VMeter", "2": "S4Q13b_BOutlet2_VMeter", "3": "S4Q13c_BOutlet3_VMeter", "4": "S4Q13d_BOutlet4_VMeter", "5": "S4Q13e_BOutlet5_VMeter"};
    var _S4Q14 = {"1": "S4Q14a_BOutlet1_Unplug_VMeter", "2": "S4Q14b_BOutlet2_Unplug_VMeter", "3": "S4Q14c_BOutlet3_Unplug_VMeter", "4": "S4Q14d_BOutlet4_Unplug_VMeter", "5": "S4Q14e_BOutlet5_Unplug_VMeter"};
    var _S5Q = {"S5_Q_2a_Num": "S5_Q_2a_Num", "S5_Q_3a_Num": "S5_Q_3a_Num", "S5_Q_4a_Num": "S5_Q_4a_Num"};
    var _alpha = {"1": "a", "2": "b", "3": "c", "4": "d", "5": "e", "6": "f", "7": "g", "8": "h", "9": "i", "10": "k"};

    this.goNext = function()
    {
        var layoutnum = 0;
        $(".layout").each(function() {
            if ($(this).css("display") === "block")
            {
                layoutnum = parseInt($(this).attr("layoutnum")) + 1;
                $(this).hide();
                $("#layout" + layoutnum).show();
                return false;
            }
        });
    };

    this.ChangeLayout = function()
    {
        var layoutnum = 0;
        var cmd = $(this).attr("id");
        $(".layout").each(function() {
            if ($(this).css("display") === "block")
            {
                //var succes = 
                if (cmd === "cmdNext")
                {

                    var succes = Me.cmdSubmit(parseInt($(this).attr("layoutnum")));
                    layoutnum = parseInt($(this).attr("layoutnum")) + 1;
                    if (succes === true)
                    {
                        Me.showcontent(this, layoutnum);
                    }
                }
                else
                {
                    layoutnum = parseInt($(this).attr("layoutnum")) - 1;
                    Me.showcontent(this, layoutnum);
                }
                return false;
            }
        });
    };

    this.showcontent = function(el, layoutnum)
    {
        $(el).hide();
        $("#layout" + layoutnum).show();
        window.scrollTo(0, 0);
        if (layoutnum == 1)
        {
            $("#cmdBack").hide();
            $("#cmdNext").show();
            $("#cmdSubmit").hide();
        }
        else if (layoutnum == 5)
        {
            $("#cmdNext").hide();
            $("#cmdBack").show();
            $("#cmdSubmit").show();
        }
        else
        {
            $("#cmdNext").show();
            $("#cmdBack").show();
            $("#cmdSubmit").hide();
        }
    };

    this.ShowDesctiption = function()
    {
        var desobj = $(this).attr("id") + "_Other";
        if ($(this).val() === "Other")
        {
            $("#" + desobj).show();
        }
        else
        {
            $("#" + desobj).val("Description");
            $("#" + desobj).css("color", "#a6a6a6");
            $("#" + desobj).hide();
        }
    };

    this.txtDesctiption_blur = function()
    {
        if ($(this).val() === "Description" || $(this).val() === "")
        {
            $(this).css("color", "#a6a6a6");
            $(this).val("Description");
        }
        else
        {
            $(this).css("color", "#000000");
        }
    };

    this.txtDesctiption_focus = function()
    {
        if ($(this).val() === "Description")
        {
            $(this).val("");
            $(this).css("color", "#000000");
        }
    };

    this.isNumber = function(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    };

    this.onChane_SumTotal = function(el)
    {
        if (typeof(el) === "object")
        {
            if (typeof(el.name) === "undefined")
            {
                el = this;
            }
        }

        var _sum = 0;
        var strid = $(el).attr("id");
        var arrid = strid.split("_");
        var sumarr = [];
        var _S4Q10=[];
        //if($("#ifmtechsurveyL1 #Sec1Q12_StoreHasDriveThruY").is(":checked"))
        //{
            _S4Q10 = _S4Q10a;
        //}
        //else
        //{
        //    _S4Q10 = _S4Q10b;
        //}
        
        if (typeof(_S4Q8[strid]) !== "undefined")
        {
            sumarr = _S4Q8;
        }
        else if (typeof(_S4Q9[strid]) !== "undefined")
        {
            sumarr = _S4Q9;
        }
        else if (typeof(_S4Q10[strid]) !== "undefined")
        {
            sumarr = _S4Q10;
        }
        else if (typeof(_S5Q[strid]) !== "undefined")
        {
            sumarr = _S5Q;
        }

        $.each(sumarr, function(id, val)
        {
            var _val = $('#' + id).val();
            if (_val !== '') {
                if (Me.isNumber(_val)) {
                    _sum = _sum + parseInt(_val);
                }
            }

        });

        //$("#S4Q8_8_Total").text('10000');

        if (_sum > 0)
        {
            if ((arrid[0] + "_" + arrid[1]) === "S5_Q")
            {
                $("#" + arrid[0] + "_" + arrid[1] + "_Total_Zero").css("display", "");
                $("#" + arrid[0] + "_" + arrid[1] + "_Total_Zero").html(_sum);
                $("#" + arrid[0] + "_" + arrid[1] + "_Total").css("display", "none");
                $("#" + arrid[0] + "_" + arrid[1] + "_Total").val(_sum);
                $("#" + arrid[0] + "_" + arrid[1] + "_Total").text(_sum);
            }
            else
            {
                $("#" + arrid[0] + "_" + arrid[1] + "_Total_Zero").css("display", "none");
                $("#" + arrid[0] + "_" + arrid[1] + "_Total").css("display", "");
                $("#" + arrid[0] + "_" + arrid[1] + "_Total").val(_sum);
                $("#" + arrid[0] + "_" + arrid[1] + "_Total").text(_sum);
            }
        }
        else
        {
            if ((arrid[0] + "_" + arrid[1]) === "S5_Q")
            {
                $("#" + arrid[0] + "_" + arrid[1] + "_Total_Zero").css("display", "");
                $("#" + arrid[0] + "_" + arrid[1] + "_Total_Zero").html(0);
                $("#" + arrid[0] + "_" + arrid[1] + "_Total").css("display", "none");
                $("#" + arrid[0] + "_" + arrid[1] + "_Total").text('0');
            }
            else
            {
                $("#" + arrid[0] + "_" + arrid[1] + "_Total_Zero").css("display", "");
                $("#" + arrid[0] + "_" + arrid[1] + "_Total").css("display", "none");
                $("#" + arrid[0] + "_" + arrid[1] + "_Total").text('0');
            }
        }
    };

    this.BuildUploadFile = function()
    {
        var placeto = $(this).attr("placeto");
        var itemid = $(this).attr("itemid");
        var issum = $(this).attr("issum");
        var num = $(this).val();
        var i = 0;
        var str = "";
        var strlabel = [];
        if (typeof($(this).attr("strlabel")) === "undefined")
        {
            for (i = 1; i <= num; i++)
            {
                str += "<input type=\"file\" id=\"" + itemid + i + "\" name=\"" + itemid + i + "\"><br/>";
            }
        }
        else if ($(this).attr("strlabel") !== "")
        {
            strlabel = $(this).attr("strlabel").split("_");
            str = "<table cellspacing=\"5\" cellpadding=\"5\" width=\"100%\">\n\
                    <col width=\"394px\">\n\
                    <col width=\"*\">";
            for (i = 1; i <= num; i++)
            {
                str += "<tr><td>" + strlabel[0] + _alpha[i] + strlabel[1] + "</td><td><input type=\"file\" id=\"" + itemid + i + "\" name=\"" + itemid + i + "\"></td></tr>";
            }
            str += "</table>";
        }
        else
        {
            for (i = 1; i <= num; i++)
            {
                str += "<input type=\"file\" id=\"" + itemid + i + "\" name=\"" + itemid + i + "\"><br/>";
            }
        }
        $("#" + placeto).html(str).show();
        if (issum === "1")
        {
            Me.onChane_SumTotal(this);
        }
    };

    this.BuildTextInputWithLabel = function()
    {
        var placeto = $(this).attr("placeto");
        var num = $(this).val();
        var i = 0;
        var str13 = "";
        var str14 = "";
        for (i = 1; i <= num; i++)
        {
/*            str13 += "<tr>\n\
                        <td>Brewer Outlet #" + i + ":</td>\n\
                        <td>\n\
                            <input type=\"text\" maxlength=\"3\" id=\"" + _S4Q13[i] + "\" name=\"" + _S4Q13[i] + "\">\n\
                        </td>\n\
                    </tr>";*/
            str14 += "<tr>\n\
                        <td>Brewer Outlet #" + i + ":</td>\n\
                        <td>\n\
                            <input type=\"text\" maxlength=\"3\" id=\"" + _S4Q14[i] + "\" name=\"" + _S4Q14[i] + "\">\n\
                        </td>\n\
                    </tr>";
        }
/*        $("#" + placeto + " table tr").remove();
        $("#" + placeto + " table").append(str13);*/
        $("#S4Q14_BOutlet_UnPlug_VMeter table tr").remove();
        $("#S4Q14_BOutlet_UnPlug_VMeter table").append(str14);
    };

    this.showOtherText = function()
    {
        if ($(this).is(":checked"))
        {
            $("#S10Q1_Other").show();
        }
        else
        {
            $("#S10Q1_Other").val("");
            $("#S10Q1_Other").hide();
        }
    };

    this.loadsurvey = function()
    {
        $.ajax({
            url: "/widgets/dashboard/tech-survey/survey/",
            data: {win: win},
            cache: false,
            type: "POST",
            dataType: "json",
            success: function(data, status, xhr) {
                if (data.success) {
                    $("#surveyPlace").html(data.work_place);
                    $('.CalendarInput').calendar({
                        dateFormat: 'MDY/'
                    });
                    detailObject.onInit.watermark();
                    $(".Material").unbind("change").bind("change", Me.ShowDesctiption);
                    $(".txtDescription").unbind("blur").bind("blur", Me.txtDesctiption_blur);
                    $(".txtDescription").unbind("focus").bind("focus", Me.txtDesctiption_focus);
                    $.each(_S4Q8, function(id, val) {
                        $("#" + id).unbind("change").bind("change", Me.onChane_SumTotal);
                    });
                    $.each(_S4Q9, function(id, val) {
                        $("#" + id).unbind("change").bind("change", Me.onChane_SumTotal);
                    });
                    $.each(_S4Q10a, function(id, val) {
                        $("#" + id).unbind("change").bind("change", Me.onChane_SumTotal);
                    });
                    $(".buildbrowsetoll").unbind("change").bind("change", Me.BuildUploadFile);
                    $(".BuildTextInputWithLabel").unbind("change").bind("change", Me.BuildTextInputWithLabel);
                    $("#S10Q1_Other_Checked").unbind("click").bind("click", Me.showOtherText);
                }
                else
                {
                    $("#surveyPlace").html(data.error);
                }
            }
        });
    };

    this.cmdSubmit = function(layout)
    {
        var strid = "";
        var errmsg = "";
        var name = "";
        switch (layout)
        {
            case 1:
                strid = "ifmtechsurveyL1";
                if (!($("#Sec1Q12_StoreHasDriveThruY").is(":checked") || $("#Sec1Q12_StoreHasDriveThruN").is(":checked")))
                {
                    errmsg += "<div>Store has a Drive-Thru empty but required.</div>";
                }
                if ($("#Sec1Q11_Date").val() === "")
                {
                    errmsg += "<div>Actual On-Site Date empty but required.</div>";
                }

                if ($("#Sec1Q11_Time").val() === "")
                {
                    errmsg += "<div>Actual On-Site Time empty but required.</div>";
                }
                var hadimage = false;
                $("#ifmtechsurveyL1 a").each(
                        function() {
                            if ($(this).attr("imgid") === "Sec1Q13_FrontofStore")
                            {
                                hadimage = true;
                            }
                        }
                )
                if ($("#Sec1Q13_FrontofStore").val() === "" && !hadimage)
                    errmsg += "<div>Photo of the front of the store empty but required.</div>";
                if (errmsg !== "")
                {
                    Me.displayerror(errmsg);
                    return false;
                }
                break;
            case 2:
                strid = "ifmtechsurveyL2";
                break;
            case 3:
                strid = "ifmtechsurveyL3";
				if($("#S4Q12_NumPicsImg").val() == 0 && $("#S4Q11_Num").val() > 0){
					var s4Q11_Num = $("#S4Q11_Num").val();
					var _checkS4Q11_Num = false;
					if( s4Q11_Num > 0){
						for(var i=1;i<= s4Q11_Num; i++){
							var s4Q12_Pic = $("#S4Q12_Pic"+i).val().length;
							if(s4Q12_Pic > 0){
								_checkS4Q11_Num = true;
							}
						}
					}
					if(_checkS4Q11_Num==false){
						errmsg += "<div> Section 3 12 empty but required. </div>";
						Me.displayerror(errmsg);
						return false;
					}
				}
                break;
            case 4:
                strid = "ifmtechsurveyL4";
                break;
            case 5:
                strid = "ifmtechsurveyL5";
                break;
        }
        errmsg += Me.geterror(strid);
        if (errmsg !== "")
        {
            Me.displayerror(errmsg);
            return false;
        }
        $("#" + strid).submit();
        return true;
    };

    this.displayerror = function(errmsg)
    {
        var html = '\
                <div id="diverror" style="max-height: 350px; overflow: auto;"><div id="errorDiv" style="color: red;">' + errmsg + '</div></div>';
        Me.openpopup(html);
//        setTimeout(jQuery.fancybox.close, 2000);
    };

    this.openpopup = function(html)
    {
        $("<div></div>").fancybox(
                {
                    'autoDimensions': true,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'content': html,
                    'title': ""
                }
        ).trigger('click');
        $("#fancybox-wrap").css("width", "300px");
        $("#fancybox-content").css("width", "280px");
        $.fancybox.resize();
    };

    this.getdisplayname = function(name)
    {
        var questionname = "";
        name = name.split("_");
        name = name[0];
        name = name.split("Q");
        questionname = name[1];
        name = name[0];
        name = name.split("S");
        name = parseInt(name[1]) - 1;
        return"Section " + name + " " + questionname + " empty but required.";
    };

    this.geterror = function(strid)
    {
        var isdthru = true;

        $("#ifmtechsurveyL1 input:radio").each(
                function() {
                    if ($(this).is(":checked"))
                    {
                        if ($(this).val() === "1")
                        {
                            isdthru = true;
                        }
                        else if ($(this).val() === "0")
                        {
                            isdthru = false;
                        }
                    }
                }
        );

        var errmsg = "";
        $("#" + strid + " select").each(
                function() {
					//if($(this).attr('name')=='S4Q11_Num'){
						
					//}else{
						if ($(this).val() === "" && !$(this).attr("disabled"))
						{
							if (!isdthru)
							{
								if ($(this).attr("name") != 'S5Q3a_Num' && !($(this).attr("name").indexOf('_DThru_') != -1))
								{
									errmsg += "<div>" + Me.getdisplayname($(this).attr("name")) + "</div>";
								}
							}
							else
							{
								errmsg += "<div>" + Me.getdisplayname($(this).attr("name")) + "</div>";
							}
						}
						else
						{
							if ($(this).val() === "Other")
							{
								if ($("#" + $(this).attr("id") + "_Other").val() === "" || $("#" + $(this).attr("id") + "_Other").val() === "Description")
								{
									errmsg += "<div>" + Me.getdisplayname($(this).attr("name")) + "</div>";
								}
							}
						}
					//}
                }
        );

        $("#" + strid + " input:file").each(
                function() {
                    var questionid = $(this).attr("id");
                    var hadimage = false;
                    $("#" + strid + " a").each(
                            function() {
                                if ($(this).attr("imgid") === questionid)
                                {
                                    hadimage = true;
                                }
                            }
                    )
					if(
						$(this).attr('name')=='S4Q12_Pic1' || 
						$(this).attr('name')=='S4Q12_Pic2' || 
						$(this).attr('name')=='S4Q12_Pic3' || 
						$(this).attr('name')=='S4Q12_Pic4' || 
						$(this).attr('name')=='S4Q12_Pic5'
					){
						
					}else if(
						$(this).attr('name')=='S8Q2_Circuits_Pic1' || 
						$(this).attr('name')=='S8Q2_Circuits_Pic2' || 
						$(this).attr('name')=='S8Q2_Circuits_Pic3' ||
						$(this).attr('name')=='S8Q2_Circuits_Pic4' || 
						$(this).attr('name')=='S8Q2_Circuits_Pic5'){
						
					}else if ($(this).val() === "" && !hadimage && $(this).attr ("require") != "no")
                    {
                        if (!isdthru)
                        {
                            if (!($(this).attr("name").indexOf('_DThru_') != -1))
                            {
                                errmsg += "<div>" + Me.getdisplayname($(this).attr("name")) + "</div>";
                            }
                        }
                        else
                        {
                            errmsg += "<div>" + Me.getdisplayname($(this).attr("name")) + "</div>";
                        }

                        //errmsg += "<div>" + Me.getdisplayname($(this).attr("name")) + "</div>";
                    }
                }
        );
	

		
        $("#" + strid + " input:text").each(
                function() {
                    if ($(this).attr("id") !== "S10Q6_Comments" && $(this).attr("id") !== "S10Q8_Comments" && $(this).attr("id") !== "S10Q9_ActualOnSiteDepDate")
                    {
                        if ($(this).val() === "")
                        {
                            var stridarr = $(this).attr("id").split("_");

                            if (stridarr[stridarr.length - 1] !== "O" && stridarr[stridarr.length - 1] !== "Total" && stridarr[stridarr.length - 1] !== "Other")
                            {
                                errmsg += "<div>" + Me.getdisplayname($(this).attr("name")) + "</div>";
                            }
                        }
                    }
                }
        );
        return errmsg;
    };

    this.cmdSubmit_Click = function()
    {
        var errmsg = "";

        errmsg = Me.geterror("ifmtechsurveyL5");

        if (!($("#S10Q5_HasLockedEntranceY").is(":checked") || $("#S10Q5_HasLockedEntranceN").is(":checked")))
        {
            errmsg += "<div>" + Me.getdisplayname($("#S10Q5_HasLockedEntranceY").attr("name")) + "</div>";
        }
        if (!($("#S10Q7_IsThereOffSiteStorageY").is(":checked") || $("#S10Q7_IsThereOffSiteStorageN").is(":checked")))
        {
            errmsg += "<div>" + Me.getdisplayname($("#S10Q7_IsThereOffSiteStorageY").attr("name")) + "</div>";
        }
        if ($("#S10Q9_ActualOnSiteDepDate").val() === "")
        {
            errmsg += "<div>Actual On-Site Departure Date empty but required.</div>";
        }
        if ($("#S10Q1_Other_Checked").is(":checked") && $("#S10Q1_Other").val() === "")
        {
            errmsg += "<div>Section 9 1 Other empty but required.</div>";
        }
        if ($("#S10Q9_ActualOnSiteDepTime").val() === "")
        {
            errmsg += "<div>Actual On-Site Departure Time empty but required.</div>";
        }
		
		//
					var _S8Q2_Circuits_NumPicsImg = $("#S8Q2_Circuits_NumPicsImg").val();
		
		var _S8Q2_Circuits_Pic1 = $("#S8Q2_Circuits_Pic1").val();
		var _S8Q2_Circuits_Pic2 = $("#S8Q2_Circuits_Pic2").val();
		var _S8Q2_Circuits_Pic3 = $("#S8Q2_Circuits_Pic3").val();
		var _S8Q2_Circuits_Pic4 = $("#S8Q2_Circuits_Pic4").val();
		var _S8Q2_Circuits_Pic5 = $("#S8Q2_Circuits_Pic5").val();
		
		
		if(_S8Q2_Circuits_NumPicsImg ==0 && 
			_S8Q2_Circuits_Pic1.length==0 && 
			_S8Q2_Circuits_Pic2.length==0 &&
			_S8Q2_Circuits_Pic3.length==0 &&
			_S8Q2_Circuits_Pic4.length==0 &&
			_S8Q2_Circuits_Pic5.length==0
		){
			errmsg += "<div>Section 7 2 empty but required.</div>";
		}
		
		
		//

        if (errmsg !== "")
        {
            Me.displayerror(errmsg);
            return false;
        }
        $("#ifmtechsurveyL5").submit();

        $("#layout5").hide();
        $("#surveyPlace").append("<div id=\"successmsg\" style=\"text-align:center\">Success! Thank you for submitting your Site Survey. Don&rsquo;t forget to close out your Work Order.</div>");
        $("#divcmd").hide();
    };

    this.init = function() {
        $("#cmdNext").unbind("click").bind("click", Me.ChangeLayout);
        $("#cmdBack").unbind("click").bind("click", Me.ChangeLayout);
        $("#cmdSubmit").unbind("click").bind("click", Me.cmdSubmit_Click);
        Me.loadsurvey();
    };
}
