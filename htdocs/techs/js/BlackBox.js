
BlackBox.Instances = null;
//-------------------------------------------------------------

BlackBox.CreateObject = function(config) {
     
    var obj = null;
  
    if (BlackBox.Instances != null) {
        obj = BlackBox.Instances[config.id];
    }
    if (obj == null) {
        if (BlackBox.Instances == null) {
            BlackBox.Instances = new Object();
        }
        obj = new BlackBox(config);
        BlackBox.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function BlackBox(config)
{
    var Me = this;
    this.id = config.id;
    this.techid = config.techid;
    this.width = 800;
    this.height =700;
    
    //------------------------------------------------------------
    this.hideLoader = function()
    {
        $.fancybox.close();
    }
    
    //------------------------------------------------------------
    this.showLoader = function(showCloseButton)
    {
        var content = "<div style='width:"+Me.width+"px;height:"+Me.height+"px;' id='comtantPopupID'><div style='text-align:center;height:"+Me.height+"px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :showCloseButton,
            'hideOnOverlayClick' : false,
            'content': content
        }
        ).trigger('click');

    }
    
    //------------------------------------------------------------
    this.cmdBlackBoxCabling = function()
    {
        var _data='';
        if(typeof($(this).attr('techid'))!="undefined")
        {
            _data="techid="+$(this).attr('techid');
        }
        
        Me.showLoader(true);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/tech/blackboxcabling-tag",
            data: _data,
            success:function( html ) {
                jQuery("#comtantPopupID").html(html);
                jQuery("#cmdBlackBoxCabling")
                .unbind("click",Me.cmdBlackBoxContinue)
                .bind("click",Me.cmdBlackBoxContinue);
                jQuery("#cmdBlackBoxSubmit")
                .unbind("click",Me.cmdBlackBoxSubmit)
                .bind("click",Me.cmdBlackBoxSubmit); 
                jQuery("#cmdBlackBoxComeback")
                .unbind("click",Me.cmdBlackBoxComeback)
                .bind("click",Me.cmdBlackBoxComeback);
            },
            error:function(error) {
                alert('error; ' + eval(error));
            }
        });
    }
    
    //------------------------------------------------------------
    this.cmdBlackBoxTelecom = function()
    {
        var _data='';
        if(typeof($(this).attr('techid'))!="undefined")
        {
            _data="techid="+$(this).attr('techid');
        }
        
        Me.showLoader(true);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/tech/blackboxtelecom-tag",
            data: _data,
            success:function( html ) {
                jQuery("#comtantPopupID").html(html);
                jQuery("#cmdBlackBoxTelecom").unbind("click",Me.cmdBlackBoxTelecom).bind("click",Me.cmdBlackBoxTelecom);
                jQuery("#cmdBlackBoxTelecomSubmit").unbind("click",Me.cmdBlackBoxTelecomSubmit).bind("click",Me.cmdBlackBoxTelecomSubmit); 
                jQuery("#cmdBlackBoxTelecomComeback").unbind("click",Me.cmdBlackBoxTelecomComeback).bind("click",Me.cmdBlackBoxTelecomComeback);
            },
            error:function(error) {
                alert('error; ' + eval(error));
            }
        });
    }
    
    //------------------------------------------------------------
    this.cmdBlackBoxContinue = function()
    {

    }
    
    //------------------------------------------------------------
    this.cmdBlackBoxComeback = function()
    {
        $.fancybox.close();
    }
    
    //------------------------------------------------------------
    this.cmdBlackBoxTelecomComeback = function()
    {
        $.fancybox.close();
    }
    
    //------------------------------------------------------------
     this.cmdBlackBoxTelecomSubmit = function()
    {
       
        var _data = jQuery("#formBlackBox").serialize();
       
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/tech/blackboxtelecom-tag-save",
            data: _data,
            success:function( html ) {
               if(jQuery.trim(html) != "")
                {
                    var json = eval("("+html+")");
                    if(json.success == "0" && json.msg != ""){
                         $.fancybox.close();
                          window.location.reload();
                    }    
                    else if(json.success == "2"){
                        jQuery("#formBlackBox").hide();
                         $.fancybox.close();
                         window.location.reload();
                    }    
                    else if(json.success == "1"){
                        jQuery("#formBlackBox").hide();
                        setTimeout(function() {
                              $.fancybox.close();
                            window.location.reload();
                        },2000);
                    }
                }
            },
            error:function(error) {
                alert('error; ' + eval(error));
            }
        });
    }
    
    //------------------------------------------------------------
    this.cmdBlackBoxSubmit = function()
    {
        var _data = jQuery("#formBlackBox").serialize();
       
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/tech/blackboxcabling-tag-save",
            data: _data,
            success:function( html ) {
               if(jQuery.trim(html) != "")
                {
                    var json = eval("("+html+")");
                    if(json.success == "0" && json.msg != ""){
                         $.fancybox.close();
                          window.location.reload();
                    }    
                    else if(json.success == "2"){
                        jQuery("#formBlackBox").hide();
                         $.fancybox.close();
                         window.location.reload();
                    }    
                    else if(json.success == "1"){
                        jQuery("#formBlackBox").hide();
                        setTimeout(function()
                        {
                              $.fancybox.close();
                            window.location.reload();
                        },2000);
                    }
                }
            },
            error:function(error) {
                alert('error; ' + eval(error));
            }
        });
    }
    
    //------------------------------------------------------------
    this.buildEvent = function()
    {
        jQuery(".BlackBoxCabling").unbind("click",Me.cmdBlackBoxCabling).bind("click",Me.cmdBlackBoxCabling); 
        jQuery(".BlackBoxTelecom").unbind("click",Me.cmdBlackBoxTelecom).bind("click",Me.cmdBlackBoxTelecom); 
    }
    
    //------------------------------------------------------------
    this.BlackBoxConfirm_click = function()
    {
        var TechID = $(this).attr('techid');
        var CertID = $(this).attr('CertID');
        var _data = "TechID=" + TechID + "&CertID=" + CertID
        $.ajax({
            type: "POST",
            dataType : 'json',
            url: "/widgets/admindashboard/tech/blackbox-confirm",
            data: _data,
            context : this,
            success:function(data, status, xhr) {
                if(CertID==46 ||CertID==47 ||CertID==48)
                {
                    var BlackBoxCablingStatus = $(".BlackBoxCablingStatus").html();
                    $(".BlackBoxCablingStatus").html(BlackBoxCablingStatus.replace(" - Pending",""));
                    $("#BlackBoxCablingDate").html(data.strDate);
                    $("#BlackBoxCablingInput").val(data.strDate);
                    $("#BlackBoxCablingName").html("Black Box Cabling");
                    $("#BlackBoxCablingNumInput").val(CertID);
                }
                else
                {
                    var BlackBoxTelecomStatus = $(".BlackBoxTelecomStatus").html();
                    $(".BlackBoxTelecomStatus").html(BlackBoxTelecomStatus.replace(" - Pending",""));
                    $("#BlackBoxTelecomDate").html(data.strDate);
                    $("#BlackBoxTelecomInput").val(data.strDate);
                    $("#BlackBoxTelecomName").html("Black Box Telecom");
                    $("#BlackBoxTelecomStatusNumInput").val(CertID);
                }
            },
            error:function(xhr, status, error) {
                alert('error; ' + eval(error));
            }
        })
    }
    
    //------------------------------------------------------------
    this.BlackBoxDeny_click = function()
    {
        var TechID = $(this).attr('techid');
        var CertID = $(this).attr('CertID');
        var _data = "TechID=" + TechID + "&CertID=" + CertID
         $.ajax({
            type: "POST",
            dataType : 'json',
            url: "/widgets/admindashboard/tech/blackbox-deny",
            data: _data,
            context : this,
            success:function(data, status, xhr ) {
               if(CertID==46 ||CertID==47 ||CertID==48)
                {
                    $(".BlackBoxCablingStatus").html("");
                    $("#BlackBoxCablingDate").html("");
                    $("#BlackBoxCablingInput").val("");
                    $("#BlackBoxCablingNumInput").val('');
                }
                else
                {
                    $(".BlackBoxTelecomStatus").html("");
                    $("#BlackBoxTelecomDate").html("");
                    $("#BlackBoxTelecomInput").val("");
                    $("#BlackBoxTelecomStatusNumInput").val('');
                }
                
            },
            error:function(xhr, status, error) {
                alert('error; ' + eval(error));
            }
        })
    }
    
    //------------------------------------------------------------
    this.BlackBoxClientConfirm_click = function()
    {
        var TechID = $(this).attr('techid');
        var CertID = $(this).attr('CertID');
        var _data = "TechID=" + TechID + "&CertID=" + CertID
        $.ajax({
            type: "POST",
            dataType : 'json',
            url: "/widgets/admindashboard/tech/blackbox-confirm",
            data: _data,
            context : this,
            success:function(data) {
                if(CertID==46 ||CertID==47 ||CertID==48)
                {
                    $("#BlackBoxCablingStatus").find("span").remove();
                    var BlackBoxCablingStatus = $("#BlackBoxCablingStatus").html();
                    $("#BlackBoxCablingStatus").html(BlackBoxCablingStatus.replace(" - Pending",""));
                    $("#BlackBoxCablingimg").css('display','');
                    $("#BlackBoxCablingDateShow").html(data.strDate);
                    $("#BlackBoxCablingInput").val(data.strDate);
                    $("#BlackBoxCablingimgtag").css('display','');
                    $("#BlackBoxCablingNumInput").val(CertID);
                }
                else
                {
                    $("#BlackBoxTelecomStatus").find("span").remove();
                    var BlackBoxTelecomStatus = $("#BlackBoxTelecomStatus").html();
                    $("#BlackBoxTelecomStatus").html(BlackBoxTelecomStatus.replace(" - Pending",""));
                    $("#BlackBoxTelecomimg").css('display','');
                    $("#BlackBoxTelecomDateShow").html(data.strDate);
                    $("#BlackBoxTelecomInput").val(data.strDate);
                    $("#BlackBoxTelecomimgtag").css('display','');
                    $("#BlackBoxTelecomNumInput").val(CertID);
                }
            },
            error:function(error) {
                alert('error; ' + eval(error));
            }
        })
    }
    
    //------------------------------------------------------------
    this.BlackBoxClientDeny_click = function()
    {
        var TechID = $(this).attr('techid');
        var CertID = $(this).attr('CertID');
        var _data = "TechID=" + TechID + "&CertID=" + CertID
         $.ajax({
            type: "POST",
            dataType : 'json',
            url: "/widgets/admindashboard/tech/blackbox-deny",
            data: _data,
            context : this,
            success:function(data, status, xhr ) {
               if(CertID==46 ||CertID==47 ||CertID==48)
                {
                    $("#BlackBoxCablingStatus").html("");
                    $("#BlackBoxCablingDateShow").html("");
                    $("#BlackBoxCablingInput").val("");
                    $("#BlackBoxCablingimgtag").remove();
                    $("#BlackBoxCablingNumInput").val("");
                }
                else
                {
                    $("#BlackBoxTelecomStatus").html("");
                    $("#BlackBoxTelecomcertDateShow").html("");
                    $("#BlackBoxTelecomInput").val("");
                    $("#BlackBoxTelecomimgtag").remove();
                    $("#BlackBoxTelecomNumInput").val("");
                }
                
            },
            error:function(xhr, status, error) {
                alert('error; ' + eval(error));
            }
        })
    }
    
    //------------------------------------------------------------
    this.buildAdminViewTechEvent = function()
    {
        jQuery(".BlackBoxCabling").unbind("click",Me.cmdBlackBoxCabling).bind("click",Me.cmdBlackBoxCabling); 
        jQuery(".BlackBoxTelecom").unbind("click",Me.cmdBlackBoxTelecom).bind("click",Me.cmdBlackBoxTelecom); 
        jQuery(".BlackBoxConfirm").unbind("click",Me.BlackBoxConfirm_click).bind("click",Me.BlackBoxConfirm_click); 
        jQuery(".BlackBoxDeny").unbind("click",Me.BlackBoxDeny_click).bind("click",Me.BlackBoxDeny_click); 
    }
    
    //------------------------------------------------------------
    this.buildClientViewTechEvent = function()
    {
        jQuery(".BlackBoxConfirm").unbind("click",Me.BlackBoxClientConfirm_click).bind("click",Me.BlackBoxClientConfirm_click); 
        jQuery(".BlackBoxDeny").unbind("click",Me.BlackBoxClientDeny_click).bind("click",Me.BlackBoxClientDeny_click); 
    }
    
    //------------------------------------------------------------
    this.init = function() {
        Me.buildEvent();
    }
//------------------------------------------------------------
//------------------------------------------------------------
}