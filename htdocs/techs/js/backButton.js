function goBack() {
	try {
		parent.reloadTabFrame();
	}
	catch (e) {
		document.location = pathName;
	}
	document.location = httpReferer;
}

function buttonMouseOver(me) {
	cbButtonHover(me,'color: #FFFFFF; font-size: 12px; font-style: normal; font-weight: bold; text-align: center; font-family: Verdana; vertical-align: middle; border-style:solid; border-width: 1px; border-color: #032D5F; background-color: #083194;  width: auto; height: auto; margin: 0 3px;');
}

function buttonMouseOut(me) {
	cbButtonHover(me,'color: #ffffff; font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold; text-align: center; vertical-align: middle; border-color: #5496C6; border-style: solid; border-width: 1px; background-color: #032D5F;  width: auto; height: auto; margin: 0 3px;');
}

try {
	// create back button if we came from another page
	test = httpReferer;
	document.getElementById("Mod0EditRecord").parentNode.innerHTML += "<input type=\"button\" class=\"cbBackButton\" value=\"Back\" onmouseover=\"buttonMouseOver(this);\" onmouseout=\"buttonMouseOut(this);\" onClick=\"goBack();\">";
}
catch(e) {
}