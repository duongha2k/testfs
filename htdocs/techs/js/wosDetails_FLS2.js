/*
	wosDetails_FLS.js file
	Added on 2/23/2009 GB
	This is the JS for the FLS WO Details Page.
	This was added due to OOS requirements
*/


function openPopup(url) { 
  newwindow=window.open(url,'name','height=500, width=740, left=100, top=100, resizable=yes, scrollbars=yes, toolbar=no,status=no');
  if (window.focus) {newwindow.focus()}
}


function massageMyTime() {
	this.value = massageTime(this.value);
}

//document.forms.caspioform.EditRecordTime_In.onblur = massageMyTime;

var oldTechMarkedComplete = document.forms.caspioform.EditRecordTechComplete.checked;


$(document).ready(
	function () {
/*		oldWorkOrderReviewed = $("#EditRecordWorkOrderReviewed").attr("checked");
		oldWorkOrderConfirm = $("#EditRecordTechCheckedIn_24hrs").attr("checked");
		oldTime_In = $("#EditRecordTime_In").attr("value");
		oldTime_Out = $("#EditRecordTime_Out").attr("value");
*/
		//editRec = $("#EditRecordOOS").attr("checked");
		//alert(editRec);
		$("#caspioform > table > tr > td").css("border-color", "#FF0000");
		
/*		$("#EditRecordExtraTime").hide();
		$("#EditRecordExtraTime").parent().parent().prev().hide();  //Hide Field Label
		$("#EditRecordExtraTime").parent().parent().next().next().hide(); // Hide HH:MM

		$("#EditRecordUnexpected_Steps").parent().parent().hide();
		$("#EditRecordUnexpected_Steps").parent().parent().prev().hide();
//		$("#EditRecordUnexpected_Steps").next().hide();

/*		$("#EditRecordUnexpected_Desc").hide();
		$("#EditRecordUnexpected_Desc").parent().parent().prev().hide();

		$("#EditRecordAskedBy").hide();
		$("#EditRecordAskedBy").parent().parent().prev().hide();

		$("#EditRecordAskedBy_Name").hide();
		$("#EditRecordAskedBy_Name").parent().parent().prev().hide();*/


		$("#EditRecordOOS").click(function () {
			if($("#EditRecordOOS").attr("checked")){
				$("#EditRecordExtraTime").show();
				$("#EditRecordExtraTime").parent().parent().prev().show(); //show Field Label
				$("#EditRecordExtraTime").parent().parent().next().show(); // show HH:MM
				$("#EditRecordUnexpected_Steps").show();
				$("#EditRecordUnexpected_Steps").parent().parent().prev().show();
				$("#EditRecordUnexpected_Desc").show();
				$("#EditRecordUnexpected_Desc").parent().parent().prev().show();
				$("#EditRecordAskedBy").show();
				$("#EditRecordAskedBy").parent().parent().prev().show();
				$("#EditRecordAskedBy_Name").show();
				$("#EditRecordAskedBy_Name").parent().parent().prev().show();
			} else {
				$("#EditRecordExtraTime").hide();
				$("#EditRecordExtraTime").parent().parent().prev().hide(); //Hide Field Label
				$("#EditRecordExtraTime").parent().parent().next().hide(); // Hide HH:MM
				$("#EditRecordUnexpected_Steps").hide();
				$("#EditRecordUnexpected_Steps").parent().parent().prev().hide();
				$("#EditRecordUnexpected_Desc").hide();
				$("#EditRecordUnexpected_Desc").parent().parent().prev().hide();
				$("#EditRecordAskedBy").hide();
				$("#EditRecordAskedBy").parent().parent().prev().hide();
				$("#EditRecordAskedBy_Name").hide();
				$("#EditRecordAskedBy_Name").parent().parent().prev().hide();
			}
			
		});
	}
);

function UpdateCaspio(){
	try {	
		if($("#EditRecordOOS").attr("checked")){
			if ($("#EditRecordExtraTime").attr("value") == ''){
				$("#EditRecordExtraTime").focus();
				alert('Please enter amount of Extra Time HH:MM');
				return false;
			}
			if ($("#EditRecordUnexpected_Steps").attr("checked") && $("#EditRecordUnexpected_Desc") == ''){
				$("#EditRecordUnexpected_Desc").focus();
				alert('Please enter in a description of what was unexpected or undocumented.');
				return false;
			}
			if ($("#EditRecordAskedBy").attr("value") == ''){
				$("#EditRecordAskedBy").focus();
				alert('Please enter who asked you to perform these steps.');
				return false;
			}
			if ($("#EditRecordAskedBy_Name").attr("value") == ''){
				$("#EditRecordAskedBy_Name").focus();
				alert('Please enter the name of the person asking you to perform this work.');
				return false;
			}
		}		

	}
	catch (e) {
	}
}
//document.forms.caspioform.onsubmit = UpdateCaspio;
