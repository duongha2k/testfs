// Calculate 72 hours before startDate
today = new Date();
startDate = new Date(document.forms.caspioform.cbParamVirtual2.value);
//Set 1 day in milliseconds
var one_day=1000*60*60*24;
var timeLeft=Math.ceil((startDate.getTime()-today.getTime())/(one_day));
//document.write(timeLeft);
if(timeLeft > '3'){
	try {
		document.forms.caspioform.EditRecordTechCheckedIn_24hrs.readOnly=true;
		document.forms.caspioform.EditRecordTechCheckedIn_24hrs.disabled=true;
		document.forms.caspioform.EditRecordTechCheckedIn_24hrs.style.visibility = 'hidden';
	}
	catch (e) {
	}
}

function massageMyTime() {
	this.value = massageTime(this.value);
}

document.forms.caspioform.EditRecordTime_In.onblur = massageMyTime;
document.forms.caspioform.EditRecordTime_Out.onblur = massageMyTime;

var oldTechMarkedComplete = document.forms.caspioform.EditRecordTechMarkedComplete.checked;

$(document).ready(
	function () {
		if (CheckInCall || CheckOutCall || ReminderAll) {
			$(".IVRBlocks").css("display", "");
			callType = "";
			if (ReminderAll || (CheckInCall && CheckOutCall))
				// both calls
				callType = "check in / out";
			else if (CheckInCall) 
				// check in only
				callType = "check in";
			else
				// check out only
				callType = "check out";
				
			$("#IVRCheckInOutBlock").html("ACS " + callType);
			$("#IVRBlockPhone").html("Call ACS at (888) 557-4556 to " + callType);
		}
		
		var approved = ($("#cbParamVirtual25").attr("value") == "Yes");
		var moreFilter = "";
		
		var pricingFields = new Array();
		// pricingFields index value has to be id of span
		pricingFields["OutofScope_Amount"] = $("tr:has(td span[id='OutofScope_Amount'])");
		pricingFields["TripCharge"] = $("tr:has(td span[id='TripCharge'])");
		pricingFields["MileageReimbursement"] = $("tr:has(td span[id='MileageReimbursement'])");
		pricingFields["MaterialsReimbursement"] = $("tr:has(td span[id='MaterialsReimbursement'])");
		pricingFields["AbortFeeAmount"] = $("tr:has(td span[id='AbortFeeAmount'])");
		pricingFields["Additional_Pay_Amount"] = $("tr:has(td span[id='Additional_Pay_Amount'])");
		if (!approved) {
			pricingFields["baseTechPay"] = $("tr:has(td span[id='baseTechPay'])");
			pricingFields["PayAmount"] = $("tr:has(td span[id='PayAmount'])");
		}
//		alert(pricingFields);
		for (i in pricingFields) {
			// hides pricing fields when unapproved WO or blank
			var val = $.trim(pricingFields[i].find("td:has(#" + i + ")").next().text());
//			alert("'" + encodeURIComponent(val) + "'");
			if (!approved || val == "" || encodeURIComponent(val) == "%C2%A0")
				pricingFields[i].css("display", "none");
		}
				
		oldWorkOrderReviewed = $("#EditRecordWorkOrderReviewed").attr("checked");
		oldWorkOrderConfirm = $("#EditRecordTechCheckedIn_24hrs").attr("checked");
		oldTime_In = $("#EditRecordTime_In").attr("value");
		oldTime_Out = $("#EditRecordTime_Out").attr("value");
	}
);

function calculateWorkHrs(time_in, time_out) {
	var start = time_in;
	var end = time_out;
	// assume getting proper format
	var validFormat = /^(\d{1,2}):(\d{2})\s(AM|PM)$/;
	
	var parts = start.match(validFormat);
	if (!parts) {
		return new Array(0, 0);
	}
	var shour = parseInt(parts[1], 10);
	var sminute = parseInt(parts[2], 10);
	
	if (shour == 12)
		shour = 0;
	if (parts[3] == "PM")
		shour += 12;
	shour += sminute / 60.0;
				
	parts = end.match(validFormat);
	if (!parts) {
		total = 0.00;
		return new Array(0, 0);
	}
	var ehour = parseInt(parts[1], 10);
	var eminute = parseInt(parts[2], 10);
		
	if (ehour == 12)
		ehour = 0;
	if (parts[3] == "PM")
		ehour += 12;
		
	ehour += eminute / 60.0;
	if (ehour < shour) ehour += 24;
		
	var total = ehour - shour;
	
	hours = Math.floor(total);
	minutes = Math.round((total - hours) * 60);
	return new Array(Math.floor(total),  minutes);
}

function UpdateCaspio(){
	try {				
		techMarkedCompleteChanged = false;
		if (!document.forms.caspioform.EditRecordWorkOrderReviewed.checked) { // force review
			document.forms.caspioform.EditRecordWorkOrderReviewed.focus();
			alert("Please mark this work order as reviewed");
			return false;
		}
		if (!document.forms.caspioform.EditRecordTechCheckedIn_24hrs.disabled && !document.forms.caspioform.EditRecordTechCheckedIn_24hrs.checked) { // force confirm
			document.forms.caspioform.EditRecordTechCheckedIn_24hrs.focus();
			alert("Please mark this work order as confirmed");
			return false;
		}
		if (!oldTechMarkedComplete && document.forms.caspioform.EditRecordTechMarkedComplete.checked) {
			document.forms.caspioform.EditRecordDate_Completed.value = (today.getMonth()+1) + "/" + today.getDate() + "/" + today.getFullYear();
			techMarkedCompleteChanged = true;
		}
		if (document.forms.caspioform.EditRecordTechMarkedComplete.checked){
			if (document.forms.caspioform.EditRecordDate_In.value == '') {
				document.forms.caspioform.EditRecordDate_In.focus();
				alert('Please enter a Date In');
				return false;
			}
			else if (document.forms.caspioform.EditRecordDate_Out.value == '') {
				document.forms.caspioform.EditRecordDate_Out.focus();
				alert('Please enter a Date Out');
				return false;
			}
			else if (document.forms.caspioform.EditRecordTime_In.value == ''){
				document.forms.caspioform.EditRecordTime_In.focus();
				alert('Please enter a Time In');
				return false;
			}
			else if (document.forms.caspioform.EditRecordTime_Out.value == '') {
				document.forms.caspioform.EditRecordTime_Out.focus();
				alert('Please enter a Time Out');
				return false;
			}
			else if (document.forms.caspioform.EditRecordTechComments.value == '') {
				document.forms.caspioform.EditRecordTechComments.focus();
				alert('Please enter your Tech Comments');
				return false; 
			}
			duration = calculateWorkHrs(document.forms.caspioform.EditRecordTime_In.value, document.forms.caspioform.EditRecordTime_Out.value);
			if (techMarkedCompleteChanged && !confirm("You are claiming " + duration[0] + " hours and " + duration[1] + " minutes of time on site for this work order. Please Confirm this entry by clicking OK")) {
				document.forms.caspioform.EditRecordTime_In.focus();
				return false;
			}
		}

		if (!isValidTime(document.forms.caspioform.EditRecordTime_In.value)) {
			// check start time format
			document.forms.caspioform.EditRecordTime_In.focus();
			alert('Please check the Time In format');
			return false; 
		}
		else if (!isValidTime(document.forms.caspioform.EditRecordTime_Out.value)) {
			// check start time format
			document.forms.caspioform.EditRecordTime_Out.focus();
			alert('Please check the Time Out format');
			return false; 
		}		

		// time stamps
		if (oldWorkOrderReviewed != $("#EditRecordWorkOrderReviewed").attr("checked")) {
			// Reviewed
			queueCreateTimeStamp(woID, "Work Order Reviewed", companyID, companyName, projectName, "", techID, techName);
		}
		if (oldWorkOrderConfirm != $("#EditRecordTechCheckedIn_24hrs").attr("checked")) {
			// Confirmed
			queueCreateTimeStamp(woID, "Work Order Confirmed", companyID, companyName, projectName, "", techID, techName);
		}
		if (oldTime_In != $("#EditRecordTime_In").attr("value")) {
			// Check In
			queueCreateTimeStamp(woID, "Checked In Updated to: " + $("#EditRecordTime_In").attr("value"), companyID, companyName, projectName, "", techID, techName);
		}		
		if (oldTime_Out != $("#EditRecordTime_Out").attr("value")) {
			// Check Out
			queueCreateTimeStamp(woID, "Checked Out Updated to: " + $("#EditRecordTime_Out").attr("value"), companyID, companyName, projectName, "", techID, techName);
		}		
		if (oldTechMarkedComplete != document.forms.caspioform.EditRecordTechMarkedComplete.checked) {
			// Tech Mark Complete
			var oldVal = (oldTechMarkedComplete ? "Completed" : "Not Completed")
			var newVal = (document.forms.caspioform.EditRecordTechMarkedComplete.checked ? "Completed" : "Not Completed")
			queueCreateTimeStamp(woID, "Work Order Marked: " + newVal, companyID, companyName, projectName, "", techID, techName);
			if (!W9 && document.forms.caspioform.EditRecordTechMarkedComplete.checked) {
				// send W9 Reminder
				win2 = window.open("emailCompleteW9Reminder.php?techID=" + techID);
				
				win2.blur();
				window.focus();
			}				
		}
		if ($("#EditRecordPic1_FromTech").attr("value") != undefined || $("#EditRecordPic2_FromTech").attr("value") != undefined || $("#EditRecordPic3_FromTech").attr("value") != undefined) {
			// Deliverables Uploaded
			queueCreateTimeStamp(woID, "Deliverables Uploaded", companyID, companyName, projectName, "", techID, techName);
		}
		// end time stamps

	}
	catch (e) {
	}
}
document.forms.caspioform.onsubmit = UpdateCaspio;
//woid = document.forms.caspioform.cbParamVirtual1.value;
//document.getElementById("printLink").href = "myWorkOrders_Print.php?ID=" + woid;
