
var loaderVisible = false;
function showLoader(){
	if(!loaderVisible){
		loaderVisible = true;
		var content = "<div id='loader'>Loading...</div>";
		$("<div></div>").fancybox(
		{
			'autoDimensions' : true,
			'showCloseButton' :false,
			'hideOnOverlayClick' : false,
			'content': content
		}
		).trigger('click');
	}
}

function hideLoader(){
	if(loaderVisible){
		$.fancybox.close();
		loaderVisible = false;
	}
}

jQuery(window).ready(function(){
    
    function cmdDownloadPayStub_onclick()
    {
        var event = this;
        var _stringValueHidden = jQuery(this).parent().children('.hidDownload').val();
        var _stringValueHiddenArr = _stringValueHidden.split(';');
        var datePayStubText = _stringValueHiddenArr[0];
        var datePayStubValue = _stringValueHiddenArr[1];
        var form_ = '<form  name="form1">'
        +'<table border="0" cellpadding="5" cellspacing="5" width="100%">'
        +' <tr>'
        +'   <td colspan="2">' 
        +'     Pay Stub for Date Paid ' + datePayStubText 
        +'  </td>'
        +' </tr>'
        +' <tr>'
        +'  <td colspan="2">'
        +'   Select an option for download.'
        +'  </td>'
        +' </tr>'
        +' <tr>'
        +'  <td width="40px" align="right" >' 
        +'   <input type="checkbox" id="chkDownloadPDF" >'
        +'  </td>'
        +'  <td >' 
        +'    PDF'
        +'  </td>'
        +' </tr>'
        +' <tr>'
        +'   <td width="40px"  align="right">' 
        +'    <input type="checkbox" id="chkDownloadEXCEL" >'    
        +'   </td>'
        +'   <td>' 
        +'     EXCEL'
        +'   </td>'
        +' </tr>'
        +' <tr>'
        +'   <td colspan="2" align="center">'
        +'     <input type="button" value="Download" class="link_button download_button" id="cmdGetFileDownload">'
        +'   </td>'
        +' </tr>'
        +' </table>'
        +' </form>';
        //if(jQuery.trim(datePayStubValue) != "")
        { 
            roll.autohide(false);
            var opt = {
                width       : 250,
                height      : 130,
                title       : 'Download Pay Stub',
                body        : form_
            };
            roll.showNotAjax(this, event, opt);    
        }
        
        jQuery("#cmdGetFileDownload").click(function(){
           
            var chkDownloadPDF = jQuery("#chkDownloadPDF").is(":checked");
            var chkDownloadEXCEL = jQuery("#chkDownloadEXCEL").is(":checked");
            //jQuery(el).attr("disabled","disabled"); 
            // get file PDF 
            // get file Excel 
            if(chkDownloadEXCEL)
            {
                window.open('ajax/getFilePayStubExcel.php'
                    +"?datePayStubValue=" +encodeURIComponent(datePayStubValue)
                    +"&datePayStubText=" +encodeURIComponent(datePayStubText)
                    ); 
            }  
            if(chkDownloadPDF)
            {
                window.open('ajax/getFilePayStubPDF.php'
                    +"?datePayStubValue=" +encodeURIComponent(datePayStubValue)
                    +"&datePayStubText=" +encodeURIComponent(datePayStubText)
                    );  
            }  
        });
    }
    function periodLink_click()
    {
        var values = $(this).children('.hidDownload').val();
        var _dateArr =  values.split(',');
        var _displayStartDate = _dateArr[0];
        var _displayEndDate = _dateArr[1];
        var _startDate = _dateArr[2];
        var _endDate = _dateArr[3];
        $('#hidPeriodDownload').val(_displayStartDate+';'+_startDate+','+_endDate);
        showLoader();
        $.post("/widgets/dashboard/tech-dashboard/paystubsperiod/rt/iframe/", 
        {
            rt: 'iframe',
            startDate:_startDate,
            endDate:_endDate,
            displayStartDate:_displayStartDate
        }, 
        function (response) {
            hideLoader();
            $("#payPeriodResult").html(response);
            $("#divPayStubs").css('display','');            
            $("#divPayStubsMenu").css('display','none');
    }); 
    }
    function buildEventperiodLink()
    {
        $(".periodLink").unbind("click",periodLink_click)
        .bind("click",periodLink_click);
        jQuery(".cmdDownload").unbind("click",cmdDownloadPayStub_onclick)
        .bind("click",cmdDownloadPayStub_onclick);    
    }
    buildEventperiodLink();
    // config table
    function ConfigTableTn()
    {
        var hasSrollBar = false;
        
        jQuery('.divBodyPayStubsTN').each(function(){
            var tableid = jQuery(this).attr("tableid");
            if(jQuery(this).height() < jQuery(this).children('table').height() 
                && jQuery('.divTrPayStubsTN'+tableid).css("display") != "none")
                {
                hasSrollBar = true;
                //jQuery(this).css("padding-right","19px");
                return;
            }
            if(hasSrollBar) return;
        });
        if(hasSrollBar)
        {
            jQuery(".divHeaderPayStubsTN").each(function(){
                jQuery(this).css("padding-right","19px");
            });
            jQuery('.divBodyPayStubsTN').each(function(){
                var tableid = jQuery(this).attr("tableid");
                if(jQuery(this).height() > jQuery(this).children('table').height() 
                    && jQuery('.divTrPayStubsTN'+tableid).css("display") != "none")
                    {
                    jQuery(this).css("padding-right","19px");
                }
            });
        }
        else
        {
            jQuery(".divHeaderPayStubsTN").each(function(){
                jQuery(this).css("padding-right","0px");
            });
            jQuery('.divBodyPayStubsTN').each(function(){
                var tableid = jQuery(this).attr("tableid");
                if(jQuery(this).height() > jQuery(this).children('table').height() 
                    && jQuery('.divTrPayStubsTN'+tableid).css("display") != "none")
                    {
                    jQuery(this).css("padding-right","0px");
                }
            });
        }
    }
    ConfigTableTn();
    jQuery(".cmdCollspan").click(function(){
        ajaxGetDatapayStubs(this);
    });
    function ConfigTableTnAjax(event)
    {
        var tableid = jQuery(event).attr("tableid");
        if(jQuery('.divTrPayStubsTN'+tableid).css("display") == "none")
        {
            jQuery('.divTrPayStubsTM').css("display","none");
            jQuery('.classCollspanTN').attr('src','images/img_plus.jpg');
            jQuery('.divTrPayStubsTN'+tableid).css("display","");   
            jQuery('.classCollspan'+tableid).attr('src','images/img_minus.jpg');
            
        }
        else
        {
            jQuery('.divTrPayStubsTN'+tableid).css("display","none");
            jQuery('.classCollspan'+tableid).attr('src','images/img_plus.jpg');
        }
        ConfigTableTn();
    }
    function ajaxGetDatapayStubs(event)
    {
        var tableid = jQuery(event).attr("tableid");
        var el = jQuery(".divTrPayStubsTN"+tableid);
        if(typeof(jQuery(el).get(0)) == 'undefined' && false)
        {
            var GrossPayForYear = jQuery(event).attr("GrossPayForYear");
            var netPayYear = jQuery(event).attr("netPayYear");
            var i = jQuery(event).attr("i");
            showLoader();
            $.ajax({
                type: "POST",
                url: "/widgets/dashboard/tech-dashboard/paystubsyear",
                data: "tableid="+tableid+"&GrossPayForYear="+GrossPayForYear+"&netPayYear="+netPayYear+"&i="+i,
                success:function( html ) {
                    if(typeof(jQuery(el).get(0)) == 'undefined') {
                        jQuery(".divHeaderPayStubsTN"+tableid).after(html);
                        buildEventperiodLink();
                    }
                    ConfigTableTnAjax(event);
                    hideLoader();
                }
    });
        }
        else
        {
            ConfigTableTnAjax(event);
        }    
    }
    var countYear = jQuery("#countYear").val();
    var i = 1;
    function getValueDayOfPeriod(i,j,leg)
    {
        if(leg <= 0) return false;
        if(j > leg) return false; 
        var el = jQuery("#trAjaxSpePayStubsTN_"+i+"_"+j);
        var startday = jQuery(el).attr("startday");
        var endday = jQuery(el).attr("endday");
        $.ajax({
            type: "POST",
			dataType:'json',
            url: "/widgets/dashboard/tech-dashboard/paystubsday",
            data: "startday="+startday+"&endday="+endday,
            success:function( response ) {

				   jQuery(el).children(".GrossPayForPeriod").html(response.GrossPayForPeriod);
				   jQuery(el).children(".NetPayForPeriod").html(response.NetPayForPeriod);
				   if(response.dateDisplay!=""){
					   jQuery(el).find(".periodLink span").html(response.dateDisplay);
					   var hidDownload = jQuery(el).find(".NetPayForDownload .hidDownload").val();
					   var arrhidDownload = hidDownload.split(";");
					   hidDownload = response.dateDisplay+";"+arrhidDownload[1];
					   jQuery(el).find(".NetPayForDownload .hidDownload").val(hidDownload);
					   //link
					   var hidDownload = jQuery(el).find(".periodLink .hidDownload").val();
					   var arrhidDownload = hidDownload.split(",");
					   hidDownload = response.dateDisplay+","+response.dateValue+","+arrhidDownload[2]+","+arrhidDownload[3];
					   jQuery(el).find(".periodLink .hidDownload").val(hidDownload);
				   }  

				getValueDayOfPeriod(i , j + 1,leg); 			   
            }
        });    
    }
    function getValueYearOfPeriod(i,countYear)
    {
        if(countYear <= 0) return false;
        if(i > countYear) return false;
        getValueYearOfPeriod(i + 1,countYear);
        
        var leg = jQuery(".trAjaxPayStubsTN"+i).length;
        var j = 1;
        getValueDayOfPeriod(i,j,leg);
    }
    getValueYearOfPeriod(i,countYear);
}); 