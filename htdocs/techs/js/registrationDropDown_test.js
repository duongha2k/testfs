
// https://www.fieldsolutions.com/techs/register.php
// TR Non-WUA Data Pages/Tech Registration

function value(element) {
	return $(element).attr("value");
}

function sourceDropDown() {
	return $("select#cbParamVirtual1");
}

function hiddenSourceField() {
	return $("input#InsertRecordTechSource");
}

function specifyField() {
	return $("input#cbParamVirtual2");
}

function registerForm() {
	return $("form#caspioform");
}

function enable(element) {
	element.attr("disabled",false);
	element.attr("style","color: black");
}

function disable(element) {
	element.attr("disabled",true);
	element.attr("style","color: #ccc");
}

function hideSpecify() {
	specifyField().css("display", "none");
}

function showSpecify() {
	specifyField().css("display", "");
}
	

var specify_text = "Please specify";
var specify = false;

$(document).ready(function() {

//specifyField().attr("disabled",true);
//specifyField().attr("style","color: #ccc;");
// disable(specifyField());
specifyField().attr("value",specify_text);
hideSpecify();

// select#cbParamVirtual1
sourceDropDown().change(function() {
	var source = sourceDropDown().attr("value");
	
	switch(source)
	{
	case "Blog (please specify)":
	case "School (please specify)":
	case "Referral (please specify)":
	case "Other (please specify)":
	  specify = true;
	  showSpecify();
//		enable(specifyField());
//	  specifyField().attr("disabled", false);
		specifyField().focus();
	  break;
	default:
	  specify = false;
//		specifyField().attr("value", specify_text);
//		disable(specifyField());
		hideSpecify();
		specifyField().attr("value", sourceDropDown().attr("value"));
	}
});


registerForm().submit(function() {
		if (specify) {
			var specify_type = "Other: ";
			switch(sourceDropDown().attr("value"))
			{
				case "Blog (please specify)":
					specify_type = "Blog: ";
					break;
				case "School (please specify)":
					specify_type = "School: ";
					break;
				case "Referral (please specify)":
					specify_type = "Referral: ";
					break;
				default:
					specify_type = "Other: ";
			}
			var source = specifyField().attr("value");
			if (blank(source))	{
				alert("Please specify where you heard about us.");
				return false;
			}
			hiddenSourceField().attr("value", specify_type + source);
		}
		else
		{
			hiddenSourceField().attr("value", sourceDropDown().attr("value"));
		}
		//alert(hiddenSourceField().attr("value"));
		return true;
		//return false;

});

function blank(source) {
	if (typeof(source) == "undefined" || source == "undefined" || source == "" || source == specify_text)
	{
		specifyField().focus();
		return true;
	}
	else
	{
		return false;
	}
}


/*
specifyField().blur(function() {
	if (blank(specifyField().attr("value"))) {
		alert("Please specify where you heard about us.");
		return false;
	}
});
*/
specifyField().focus(function() {
	specifyField().attr("value","");
});

specifyField().change(function() {
	hiddenSourceField().attr("value",specifyField().attr("value"));
});

});
