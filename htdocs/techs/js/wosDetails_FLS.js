/*
	wosDetails_FLS.js file
	Added on 2/23/2009 GB
	This is the JS for the FLS WO Details Page.
	This was added due to OOS requirements
*/


function openPopup(url) { 
  newwindow=window.open(url,'name','height=500, width=740, left=100, top=100, resizable=yes, scrollbars=yes, toolbar=no,status=no');
  if (window.focus) {newwindow.focus()}
}

function massageMyTime24() {
	this.value = massageTime24(this.value);
}

$(document).ready(
	function () {
		$("#EditRecordStartTime").change(massageMyTime24);
		$("#EditRecordEndTime").change(massageMyTime24);		

		if(OOS_Trigger == "False"){
				$("#EditRecordOOS").parent().parent().parent().parent().prev().hide();
				$("#EditRecordOOS").parent().hide(); // Hide Yes
				
				$("#EditRecordExtraTime").hide();
				$("#EditRecordExtraTime").parent().parent().prev().hide(); //Hide Field Label
				$("#extraTime").hide(); // Hide HH:MM
				
				$("#EditRecordUnexpected_Steps").parent().parent().parent().parent().prev().hide();
				$("#EditRecordUnexpected_Steps").parent().hide(); // Hide Yes
				
				$("#EditRecordUnexpected_Desc").hide();
				$("#EditRecordUnexpected_Desc").parent().parent().prev().hide();
				
				$("#EditRecordAskedBy").hide();
				$("#EditRecordAskedBy").parent().parent().prev().hide();
				
				$("#EditRecordAskedBy_Name").hide();
				$("#EditRecordAskedBy_Name").parent().parent().prev().hide();
	
		}

		if(!$("#EditRecordOOS").attr("checked")){

				$("#EditRecordExtraTime").hide();
				$("#EditRecordExtraTime").parent().parent().prev().hide(); //Hide Field Label
				$("#extraTime").hide(); // Hide HH:MM
				
				$("#EditRecordUnexpected_Steps").parent().parent().parent().parent().prev().hide();
				$("#EditRecordUnexpected_Steps").parent().hide(); // Hide Yes
				
				$("#EditRecordUnexpected_Desc").hide();
				$("#EditRecordUnexpected_Desc").parent().parent().prev().hide();
				
				$("#EditRecordAskedBy").hide();
				$("#EditRecordAskedBy").parent().parent().prev().hide();
				
				$("#EditRecordAskedBy_Name").hide();
				$("#EditRecordAskedBy_Name").parent().parent().prev().hide();
		}

		$("#EditRecordOOS").click(function () {
			if($("#EditRecordOOS").attr("checked")){
				$("#EditRecordExtraTime").show();
				$("#EditRecordExtraTime").parent().parent().prev().show(); //show Field Label
				$("#extraTime").show(); // show HH:MM
				
				$("#EditRecordUnexpected_Steps").parent().parent().parent().parent().prev().show();
				$("#EditRecordUnexpected_Steps").parent().show(); // show Yes
				
				$("#EditRecordUnexpected_Desc").show();
				$("#EditRecordUnexpected_Desc").parent().parent().prev().show();
				
				$("#EditRecordAskedBy").show();
				$("#EditRecordAskedBy").parent().parent().prev().show();
				
				$("#EditRecordAskedBy_Name").show();
				$("#EditRecordAskedBy_Name").parent().parent().prev().show();
			} else {
				$("#EditRecordExtraTime").hide();
				$("#EditRecordExtraTime").parent().parent().prev().hide(); //Hide Field Label
				$("#extraTime").hide(); // Hide HH:MM
				
				$("#EditRecordUnexpected_Steps").parent().parent().parent().parent().prev().hide();
				$("#EditRecordUnexpected_Steps").parent().hide(); // Hide Yes
				
				$("#EditRecordUnexpected_Desc").hide();
				$("#EditRecordUnexpected_Desc").parent().parent().prev().hide();
				
				$("#EditRecordAskedBy").hide();
				$("#EditRecordAskedBy").parent().parent().prev().hide();
				
				$("#EditRecordAskedBy_Name").hide();
				$("#EditRecordAskedBy_Name").parent().parent().prev().hide();
			}
			
		});
	}
);

function roundTime(hours) {
	numHours = Math.floor(hours);
	numMins = hours - numHours;
	if (numMins == 0)
		numMins = 0;
	else if (numMins <= 0.25)
		numMins = 0.25;
	else if (numMins <= 0.50)
		numMins = 0.50;
	else if (numMins <= 0.75)
		numMins = 0.75;
	else if (numMins <= 1.00)
		numMins = 1.00;
	return numHours + numMins;
}

function calculateDuration() {
	var start = $("#EditRecordStartTime").attr("value");
	var end = $("#EditRecordEndTime").attr("value");


// assume getting proper format
	var validFormat = /^(\d{1,2}):(\d{2})/;


	var parts = start.match(validFormat);
	if (!parts) {
		total = 0.00;
		$("#EditRecordCalculatedDuration").attr("value", total.toFixed(2));

		return true;
	}
	var shour = parseInt(parts[1], 10);
	var sminute = parseInt(parts[2], 10);
	
/*	if (shour == 12)
		shour = 0;
	if (parts[3] == "PM")
		shour += 12;
*/	shour += sminute / 60.0;
				
	parts = end.match(validFormat);
	if (!parts) {
		total = 0.00;
		$("#EditRecordCalculatedDuration").attr("value", total.toFixed(2));

		return true;
	}
	var ehour = parseInt(parts[1], 10);
	var eminute = parseInt(parts[2], 10);
		
/*	if (ehour == 12)
		ehour = 0;
	if (parts[3] == "PM")
		ehour += 12;
*/		
	ehour += eminute / 60.0;
	if (ehour < shour) ehour += 24;
		
	var total = ehour - shour;
	total = roundTime(total);
	$("#EditRecordCalculatedDuration").attr("value", total.toFixed(2));
	
//	alert($("#EditRecordCalculatedDuration").attr("value"));
}



function UpdateCaspio(){	
		if($("#EditRecordOOS").attr("checked") && OOS_Trigger){	
			if ($("#EditRecordExtraTime").val() == '' || !isValidElapsedTime($("#EditRecordExtraTime").val())){
	//		if ($("#EditRecordExtraTime").val() == ''){
				$("#EditRecordExtraTime").focus();
				alert('Please enter amount of Extra Time in the format: HH:MM');
				return false;
			}else if ($("#EditRecordUnexpected_Steps").attr("checked") && $("#EditRecordUnexpected_Desc").val() == ''){
				$("#EditRecordUnexpected_Desc").focus();
				alert('Please enter in a description of what was unexpected or undocumented.');
				return false;
			}else if ($("#EditRecordAskedBy").val() == ''){
				$("#EditRecordAskedBy").focus();
				alert('Please enter who asked you to perform these steps.');
				return false;
			}else if ($("#EditRecordAskedBy_Name").val() == ''){
				$("#EditRecordAskedBy_Name").focus();
				alert('Please enter the name of the person asking you to perform this work.');
				return false;
			}
		}
		if (!isValidTime24($("#EditRecordStartTime").val())) {
			alert("Please enter a start time in the format: HH:MM");
			$("#EditRecordStartTime").focus();
			return false;
		}
		if (!isValidTime24($("#EditRecordEndTime").val())) {
			alert("Please enter a end time in the format: HH:MM");
			$("#EditRecordEndTime").focus();
			return false;
		}
		
		calculateDuration();

}

document.forms.caspioform.onsubmit = UpdateCaspio;
