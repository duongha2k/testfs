


//-------------------------------------------------------------
//-------------------------------------------------------------
//-------------------------------------------------------------
copier_skills_assessment.Instances = null;
//-------------------------------------------------------------
copier_skills_assessment.CreateObject = function(config) {
    var obj = null;
    if (copier_skills_assessment.Instances != null) {
        obj = copier_skills_assessment.Instances[config.id];
    }
    if (obj == null) {
        if (copier_skills_assessment.Instances == null) {
            copier_skills_assessment.Instances = new Object();
        }
        obj = new copier_skills_assessment(config);
        copier_skills_assessment.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function copier_skills_assessment(config) {
    var Me = this;
    this.id = config.id;
    this.loaderVisible = false;
    this.width = (jQuery(window).width() * 70) / 100;
    this.height = 400;
    //------------------------------------------------------------
    this.hideLoader = function()
    {
        $.fancybox.close();
    }
    //------------------------------------------------------------
    this.showLoader = function(width,height,showCloseButton)
    {
        return false;
        var content = "<div style='text-align:center;width:"+width+"px;height:"+height+"px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div>";
        $("<div></div>").fancybox(
        {
            'autoDimensions' : false,
            'showCloseButton' :showCloseButton,
            'hideOnOverlayClick' : false,
            'width' : width,
            'height' : height,
            'content': content
        }
        ).trigger('click');

    }
    //------------------------------------------------------------
    this.CopierSkillsOnlyView = function()
    {
        var techid = jQuery(this).attr("techid");
        
        Me.showLoader(400,300,true);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/popup/copier-skills-assessment-view",
            data: "techid="+techid,
            success:function( html ) {
                var content = html;
                $("<div></div>").fancybox(
                {
                    'autoDimensions' : true,
                    'showCloseButton' :true,
                    'hideOnOverlayClick' : false,
                    'width' : 400,
                    'height' : 300,
                    'content': "<div style='color:#1F497D;'>"+content+"</div>",
                    'onComplete' : function(){
                        $.fancybox.resize();
                    }
                }
                ).trigger('click');
            }
        });
    }
    //------------------------------------------------------------
    this.cmdCopierSkills = function(event,data)
    {
        if(typeof(data) == 'undefined'){
            data = "";
            Me.showLoader(Me.width,Me.height,true);
        }   
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/tech/copier-skills-assessment?",
            data: data,
            success:function( html ) {
                var content = "<div id='devContentCopiesSkillListAjax46' style='color:#1F497D;width:"+ (Me.width - 20) +"px;'>"+html+"</div>";
                $("<div></div>").fancybox(
                {
                    'autoDimensions' : true,
                    'showCloseButton' :true,
                    'hideOnOverlayClick' : false,
                    'width' : Me.width,
                    'content': content
                }
                ).trigger('click');
                jQuery("#cmdSubmitCopierSkills")
                .unbind("click",Me.cmdSubmitCopierSkills)
                .bind("click",Me.cmdSubmitCopierSkills);  
                jQuery(".cmdCopiesSkillBack")
                .unbind("click",Me.cmdCopiesSkillBack)
                .bind("click",Me.cmdCopiesSkillBack);     
            },
            error:function(error) {
                alert('error; ' + eval(error));
            }
        });
    }
    this.cmdCopierSkills_submit = function(event,data)
    {
        if(typeof(data) == 'undefined'){
            data = "";
            Me.showLoader(Me.width,Me.height,true);
        }   
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/tech/copier-skills-assessment?",
            data: data,
            success:function( html ) {
                jQuery("#devContentCopiesSkillListAjax46").html(html);
                jQuery("#cmdSubmitCopierSkills")
                .unbind("click",Me.cmdSubmitCopierSkills)
                .bind("click",Me.cmdSubmitCopierSkills);  
                jQuery(".cmdCopiesSkillBack")
                .unbind("click",Me.cmdCopiesSkillBack)
                .bind("click",Me.cmdCopiesSkillBack);     
            },
            error:function(error) {
                alert('error; ' + eval(error));
            }
        });
    }
    this.cmdCopiesSkillBack = function()
    {
        Me.cmdCopierSkills_submit(this,"");
    }
    this.cmdSubmitCopierSkills = function()
    {
        //get data
        var data = "";
        jQuery(".QuestionIdCopiseSkill").each(function() {
            var questionId = jQuery(this).attr("questionid");
            var value = "";
            jQuery(this).find(".QuestionIdCopiseSkillItem").each(function(){
                if(jQuery(this).is(':checked'))
                {    
                    value += ","+jQuery(this).attr("questionitemid");
                }
            });
            if(value != "")
            {
                value = value.substring(1, value.length);    
            }
            data += ",{\"key\":\""+questionId+"\",\"value\":\""+value+"\"}";
        });
        if(data != "")
        {
            data = "data="+ Base64.encode(data.substring(1, data.length));
        }
        else
        {
            data = "data="+Base64.encode("{}");
        }    
        Me.cmdCopierSkills_submit(this,data);
    }
    //------------------------------------------------------------
    //------------------------------------------------------------
    this.init = function() {
        jQuery(".cmdCopierSkills").unbind("click",Me.cmdCopierSkills).bind("click",Me.cmdCopierSkills);
        jQuery(".cmdCopiesSkillView").unbind("click",Me.CopierSkillsOnlyView).bind("click",Me.CopierSkillsOnlyView);
        jQuery(".cmdCopiesSkillViewTech").unbind("click",Me.cmdCopierSkills).bind("click",Me.cmdCopierSkills);
        
    }
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
}