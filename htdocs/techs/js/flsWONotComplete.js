function massageMyTime24() {
	this.value = massageTime24(this.value);
}

$(document).ready(
	function () {
		$("#EditRecordStartTime").change(massageMyTime24);
		$("#EditRecordEndTime").change(massageMyTime24);		
	}
);


function UpdateCaspio(){	
		if (!isValidTime24($("#EditRecordStartTime").val())) {
			alert("Please enter a start time in the format: HH:MM");
			$("#EditRecordStartTime").focus();
			return false;
		}
		if (!isValidTime24($("#EditRecordEndTime").val())) {
			alert("Please enter a end time in the format: HH:MM");
			$("#EditRecordEndTime").focus();
			return false;
		}
}

document.forms.caspioform.onsubmit = UpdateCaspio;
