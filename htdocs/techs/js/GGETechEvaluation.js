


//-------------------------------------------------------------
//-------------------------------------------------------------
//-------------------------------------------------------------
GGETechEvaluation.Instances = null;
//-------------------------------------------------------------
GGETechEvaluation.CreateObject = function(config) {
    var obj = null;
    if (GGETechEvaluation.Instances != null) {
        obj = GGETechEvaluation.Instances[config.id];
    }
    if (obj == null) {
        if (GGETechEvaluation.Instances == null) {
            GGETechEvaluation.Instances = new Object();
        }
        obj = new GGETechEvaluation(config);
        GGETechEvaluation.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function GGETechEvaluation(config) {
    var Me = this;
    this.id = config.id;
    this.width = ((jQuery(window).width() * 90) / 100) - 10;
    this.height =(jQuery(window).height() * 80) / 100;
    this.secs = 15*60;
    this.timeleft = null;
    this.sec = 0;
    this.roll = new FSPopupRoll();
    this.submit = false;
    this._lefttime = 0;
    this.reload = false;
    //------------------------------------------------------------
    this.hideLoader = function()
    {
        $.fancybox.close();
    }
    
    //------------------------------------------------------------
    this.showLoader = function(showCloseButton)
    {
        var content = "<div style='width:"+Me.width+"px;' id='comtantPopupID'><div style='text-align:center;height:"+Me.height+"px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :showCloseButton,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'content': content
        }
        ).trigger('click');
        

    }
    //------------------------------------------------------------
    this.showLoaderNew = function(width,height,showCloseButton)
    {
        var content = "<div style='width:"+width+"px;' id='comtantPopupID'><div style='text-align:center;height:"+height+"px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :showCloseButton,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'content': content
        }
        ).trigger('click');
        

    }
    //------------------------------------------------------------
    this.cmdGGETechEvaluation = function()
    {
        var profile = jQuery(this).attr("profile");
        if(profile == 1) Me.reload = true;
        else Me.reload = false;
        var data = "";
        Me.showLoader(true);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/tech/gge-tech-evaluation",
            data: data,
            success:function( html ) {
                jQuery("#comtantPopupID").html(html);
                jQuery("#GGETechEvaluationContinue")
                .unbind("click",Me.GGETechEvaluationContinue)
                .bind("click",Me.GGETechEvaluationContinue); 
                jQuery("#GGETechEvaluationContinuetimeleft")
                .unbind("click",Me.GGETechEvaluationContinue)
                .bind("click",Me.GGETechEvaluationContinue); 
                jQuery("#GGETechEvaluationContinueOneFail")
                .unbind("click",Me.GGETechEvaluationContinue)
                .bind("click",Me.GGETechEvaluationContinue); 
            },
            error:function(error) {
                alert('error; ' + eval(error));
            }
        });
    }
    //------------------------------------------------------------
    this.setTimeoutLeft = function()
    {
        if(jQuery("#GGETechEvaluationTimeLeftValue").length <=0 || Me.submit)
        {
            if(Me.timeLeft != null)
            {
                clearTimeout(Me.timeLeft);
            }
            return false;
        }    
        if(Me.sec < Me.secs)
        {
            Me.sec++;
            var _secs = Me.secs - Me.sec;
            if(_secs == 120)
            {
                jQuery("#GGETechEvaluationTimeLeftValue").css("color","red");
            }
            var minsvalue = parseInt(_secs/60);
            minsvalue = minsvalue <10?"0"+minsvalue:minsvalue;
            var secsvalue = parseInt(_secs%60);
            secsvalue = secsvalue <10?"0"+secsvalue:secsvalue;
            jQuery("#GGETechEvaluationTimeLeftValue").html(minsvalue+":"+secsvalue);
            Me.timeLeft = setTimeout(Me.setTimeoutLeft,1000);
            return false;
        }
        if(Me.timeLeft != null)
        {
            clearTimeout(Me.timeLeft);
        }    
        //save data
        Me._lefttime = 1;
        Me.saveSurveyToServer();
        Me.submit = true;
        
        
    }
    //------------------------------------------------------------
    this.GGETechEvaluationContinue = function()
    {
        jQuery("#fancybox-close").unbind("click");
        jQuery("#fancybox-close").unbind("click",Me.closePopup).bind("click",Me.closePopup);
        jQuery("#GGETechEvaluationCancel").unbind("click",Me.closePopup).bind("click",Me.closePopup);
        jQuery("#GGETechEvaluationSubmid").unbind("click",Me.saveSurveyToServer).bind("click",Me.saveSurveyToServer);
        jQuery("#GGETechEvaluationForm").css("height",Me.height+"px"); 
        jQuery("#GGETechEvaluationInformation").hide();
        jQuery("#GGETechEvaluationTimeLeftMissFail").hide();
        jQuery("#GGETechEvaluationSubmitOneFail").hide();
       
        jQuery("#GGETechEvaluationFormAnswer").show();
        jQuery("#GGETechEvaluationTimeLeft").show();
        Me.sec = 0;
        Me.submit = false;
        jQuery("#GGETechEvaluationTimeLeftValue").css("color","#1F497D");
        Me.timeLeft = setTimeout(Me.setTimeoutLeft,1000);
    }
    //------------------------------------------------------------
    this.saveSurveyToServer = function()
    {
        if(Me.timeLeft != null)
        {
            clearTimeout(Me.timeLeft);
        }
        Me.submit = true;
        jQuery("#fancyboxbox-loading").show();
         var _data = jQuery("#frmGGETechEvaluation").serialize();
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/tech/gge-tech-evaluation",
            data: _data+"&lefttime="+Me._lefttime,
            success:function( data ) {
                jQuery("#fancyboxbox-loading").hide();
                $(".correctanswer").html(data.correctanswer);
                if(Me.sec < Me.secs)
                {
                    if(data.allowed == 1) 
                    {
                        if(data.status == "Pass") 
                        {
                            jQuery("#GGETechEvaluationPass").show();
                        }
                    else 
                        {
                            jQuery("#GGETechEvaluationSubmitOneFail").show();
                }
                    }
                    else 
                    {
                        if(data.status == "Pass") 
                        {
                            jQuery("#GGETechEvaluationPass").show();
                        }
                        else
                        {
                            jQuery("#GGETechEvaluationComnpleteFail").show();
                        }
                    }
                }
                else
                {
                    if(data.status == "Pass") 
                    {
                        jQuery("#GGETechEvaluationPass").show();
                    }
                    else
                    {
                        jQuery("#GGETechEvaluationTimeLeftMissFail").show();
                    }
                    
                }
                jQuery("#GGETechEvaluationFormAnswer").hide();
                jQuery("#GGETechEvaluationTimeLeft").hide();
                jQuery("#GGETechEvaluationForm").css("height","auto"); 
                Me.unBindClickClose();
            },
            error:function(){
                Me.unBindClickClose();
            }
        });  
    }
    //------------------------------------------------------------
    this.closePopup = function()
    {
        if(jQuery("#fancyboxbox-loading").css("display") != 'none') return false;
        
        Me.roll.autohide(false);
         var html = "<table width='100%'>"
            html += "<tr>";
            html +=     "<td colspan='2'>";
            html +=     "Your answers will not be submitted for grading unless you click the ?Submit? button, but leaving the evaluation before completion still counts as one of your two attempts.";
            html +=     "</td>";
            html += "</tr>";
            html += "<tr>";
            html +=     "<td align='center'>";
            html +=     "<input type='button' class='link_button middle2_button' id='cmdLeaveEvaluation' value='Leave Evaluation' />";
            html +=     "</td>";
            html +=     "<td align='center'>";
            html +=     "<input type='button' class='link_button middle2_button' id='cmdReturntoEvaluation' value='Return to Evaluation' />";
            html +=     "</td>";
            html += "</tr>";
            html +="</table>";
         var opt = {
            width       : 300,
            height      : '',
            zindex : 2000,
            position    : 'middle',
            title       : '<span style="color:red;">Are you sure you want to leave this evaluation?</span>',
            body        : html
        }; 
        Me.roll.showNotAjax(null, null, opt);
        jQuery("#cmdLeaveEvaluation").unbind("click",Me.cmdLeaveEvaluationPopup).bind("click",Me.cmdLeaveEvaluationPopup);
        jQuery("#cmdReturntoEvaluation").unbind("click",Me.cmdReturntoEvaluation).bind("click",Me.cmdReturntoEvaluation);
    }
    this.cmdReturntoEvaluation = function(){
        Me.roll.hide();
    }
    //------------------------------------------------------------
    this.unBindClickClose = function()
    {
        jQuery("#fancybox-close").unbind("click");
        jQuery("#fancybox-close").unbind("click",Me.cmdLeaveEvaluation).bind("click",Me.cmdLeaveEvaluation);
    }
    //------------------------------------------------------------
    this.cmdLeaveEvaluationPopup = function()
    {
        Me.saveSurveyToServer();
        Me.unBindClickClose();
        $.fancybox.close();
        Me.roll.hide();
    }
    //------------------------------------------------------------
    this.cmdLeaveEvaluation = function()
    {
        Me.unBindClickClose();
        $.fancybox.close();
        Me.roll.hide();
    }
    //------------------------------------------------------------
    this.TechFORCEContractorAgreement_click = function(techID){
        Me.showLoaderNew(500,200,true);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/tech/tech-force-contractor-agreement",
            data: "TechID="+techID,
            success:function( html ) {
                jQuery("#comtantPopupID").html(html);
                jQuery("#cmdCancelPopup")
                .unbind("click",Me.hideLoader)
                .bind("click",Me.hideLoader); 
                jQuery("#cmdSubmitTFEAPopup")
                .unbind("click",Me.cmdSubmitTFEAPopup_click)
                .bind("click",Me.cmdSubmitTFEAPopup_click);
            },
            error:function(error) {
                alert('error; ' + eval(error));
            }
        });
    }
    //------------------------------------------------------------
    this.cmdSubmitTFEAPopup_click = function(){
        var checked = jQuery("#frmTECFICAgreement #TechFORCEContractorAgreementAgree").is(":checked")?1:0;
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/tech/tech-force-contractor-agreement",
            data: "p=yes&checked="+checked,
            success:function( html ) {
                Me.hideLoader();
            },
            error:function(error) {
                alert('error; ' + eval(error));
            }
        });
    }
    //------------------------------------------------------------
    this.buildEvent = function()
    {
        jQuery(".GGETechEvaluation").unbind("click",Me.cmdGGETechEvaluation).bind("click",Me.cmdGGETechEvaluation); 
        jQuery(".TechFORCEContractorAgreement").unbind("click",Me.TechFORCEContractorAgreement_click).bind("click",Me.TechFORCEContractorAgreement_click);
    }
    //------------------------------------------------------------
    this.init = function() {
        Me.buildEvent();
    }
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
}