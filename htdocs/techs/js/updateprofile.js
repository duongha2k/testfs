
updateprofile.Instances = null;
//-------------------------------------------------------------
updateprofile.CreateObject = function(config) {
    var obj = null;
    if (updateprofile.Instances != null) {
        obj = updateprofile.Instances[config.id];
    }
    if (obj == null) {
        if (updateprofile.Instances == null) {
            updateprofile.Instances = new Object();
        }
        obj = new updateprofile(config);
        updateprofile.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function updateprofile(config) {
    var Me = this;
    this.id = config.id;
    this.techid = config.techid;
    this.htmlSignLanguageHeader = "<div style='text-align:center;'>Sign Language Fluent Techs Wanted!</div>\
                                   <br><div style='color:red;' id='languegaError'></div>\
                                   <div>FieldSolutions is looking for Technicians with intermediate to advanced skills in American Sign Language.</div>\
                                   <br><div>Get qualified as an American Sign Language Speaker and start getting work today! Add comments regarding your Sign Language fluency and other qualifications below.</div>\
                                   ";
    this.htmlSignLanguageSelect = "<br><div>Skill Level</span><span style='color:red;vertical-align: top;'>*</span><select style='width: 150px; display: inline;margin-left:9px;'>\
                                        <option value='1'>Competent Speaker</option>\
                                        <option value='2'>Fluent Speaker</option>\
                                    </select></div>";
    this.htmlSignLanguageFooter =  "<br><div><span style='vertical-align: top;'>Comments</span><span style='color:red;vertical-align: top;'>*</span>  <textarea style='width:70%;' id='languegaSkillComment'></textarea></div>\
                                    <br><div style='text-align:center;'><input type='button' class='link_button_sm' id='cmdCancel' value='Cancel' />&nbsp;&nbsp;<input type='button' class='link_button_sm' id='cmdlanguegaSkillComment' value='Submit' /></div>";
    //------------------------------------------------------------
    this.hideLoader = function()
    {
        $.fancybox.close();
    }
    
    //------------------------------------------------------------
    this.showLoader = function(width,height,showCloseButton)
    {
        var content = "<div style='width:"+width+"px;' id='comtantPopupID'><div style='text-align:center;height:"+height+"px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :showCloseButton,
            'hideOnOverlayClick' : false,
            'content': content
        }
        ).trigger('click');

    }
    //------------------------------------------------------------
    this.configlanguegaSkillClientCredentials = function(status,date){
        if(status == "pending"){
            jQuery("#CertifiedPurpleTechnicianStatusLabel").html("Pending");
            jQuery("#CertifiedPurpleTechnicianLable").html("Verified Purple Technician");
            jQuery("#CertifiedPurpleTechnicianDate").html("");
        }else  if(status == "empty"){
            jQuery("#CertifiedPurpleTechnicianLable").html("<a href='javascript:;'>Verified Purple Technician</a>")
        }else if(status == "ok"){
            jQuery("#CertifiedPurpleTechnicianLable").html("Verified Purple Technician");
            jQuery("#CertifiedPurpleTechnicianDate").html(date);
            jQuery("#CertifiedPurpleTechnicianStatusLabel").html('<img src="/widgets/images/check_green.gif"> Certified');
        }
    }
    //------------------------------------------------------------
    this.cmdlanguegaSkillComment_click = function(){
        var comment = jQuery("#languegaSkillComment").val();
        if(jQuery.trim(comment) == ""){
            jQuery("#languegaError").html("Please enter Comment.");
            return false;
        }
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/do/save-comment-languge-skill",
            data: "techid="+Me.techid
                +"&comment="+comment
                +"&changeTextCer="+Me.changeTextCer
                +"&languegaSkills="+Me.getLanguageSkills()
                +"&states="+jQuery(".LanguageSkillsAmericanCheckbox").parent().parent().find("select[name='SpeakerLevel[]']").val(),
            success:function( html ) {
                Me.hideLoader();
                if(Me.changeTextCer){
                    Me.configlanguegaSkillClientCredentials("pending");
                }
            },
            error:function(error) {
                alert('error; ' + eval(error));
            }
        });
    }
    //------------------------------------------------------------
    this.popuplanguageSkillSelect_Change = function(){
        var event = jQuery(".LanguageSkillsAmericanCheckbox").parent().parent().find("select[name='SpeakerLevel[]']");
        jQuery(event).val(jQuery("#comtantPopupID select").val());
    }
    //------------------------------------------------------------
    this.ShowCertifiedPurpleTechnicianLable = function(){
        Me.showLoader(400,240,true);
        var html = Me.htmlSignLanguageHeader+Me.htmlSignLanguageSelect+Me.htmlSignLanguageFooter;
        jQuery("#comtantPopupID").html(html);
        Me.changeTextCer = 1;
        jQuery("#cmdlanguegaSkillComment").unbind("click",Me.cmdlanguegaSkillComment_click).bind("click",Me.cmdlanguegaSkillComment_click); 
        jQuery("#comtantPopupID select").unbind("change",Me.popuplanguageSkillSelect_Change).bind("change",Me.popuplanguageSkillSelect_Change); 
        jQuery("#cmdCancel").click(function(){
            Me.hideLoader();
        });
        var event = jQuery(".LanguageSkillsAmericanCheckbox").parent().parent().find("select[name='SpeakerLevel[]']");
      
        jQuery("#comtantPopupID select").val(jQuery(event).val());
        
    }
    //------------------------------------------------------------
    this.CertifiedPurpleTechnicianLable_onclick = function(){
        Me.ShowCertifiedPurpleTechnicianLable();
    }
    //------------------------------------------------------------
    this.LanguageSkillsAmericanCheckbox_onclick = function(){
        if(jQuery(this).is(":checked")){
            Me.ShowCertifiedPurpleTechnicianLable();
        }
    }
    //------------------------------------------------------------
    this.configCertifiedPurpleTechnician = function(data){
        var certificationsInfo = data.techInfo.certificationsInfo;
        if(certificationsInfo["PurpleTechnicianUnverifiedCertStatus"] == 1
            && certificationsInfo["PurpleTechnicianCertifiedCertStatus"] != 1){
            Me.configlanguegaSkillClientCredentials("pending");
        }else if(certificationsInfo["PurpleTechnicianCertifiedCertStatus"] == 1){
            Me.configlanguegaSkillClientCredentials("ok",certificationsInfo["PurpleTechnicianCertifiedDate"]);
        } else{
            Me.configlanguegaSkillClientCredentials("empty");
        }
    }
    //------------------------------------------------------------
    this.LanguageSkillsCheckbox_click = function(){
        var event = jQuery(this).parent().parent();
        if(jQuery(this).is(":checked")){
            jQuery(event).find("select[name='SpeakerLevel[]']").show();
        }else{
            jQuery(event).find("select[name='SpeakerLevel[]']").hide();
        }
        editingSection['LanguageSkills'] = true;
    }
    //------------------------------------------------------------
    this.selectLangageSkill_Change = function(){
        editingSection['LanguageSkills'] = true;
    }
    //------------------------------------------------------------
    this.buildEvent = function(data)
    {
        jQuery(".LanguageSkillsAmericanCheckbox").unbind("click",Me.LanguageSkillsAmericanCheckbox_onclick).bind("click",Me.LanguageSkillsAmericanCheckbox_onclick); 
        Me.configCertifiedPurpleTechnician(data);
        jQuery("#CertifiedPurpleTechnicianLable a").unbind("click",Me.CertifiedPurpleTechnicianLable_onclick).bind("click",Me.CertifiedPurpleTechnicianLable_onclick); 
        jQuery(".LanguageSkillsCheckbox").unbind("click",Me.LanguageSkillsCheckbox_click).bind("click",Me.LanguageSkillsCheckbox_click); 
        jQuery("select[name='SpeakerLevel']").unbind("change",Me.selectLangageSkill_Change).bind("change",Me.selectLangageSkill_Change); 
    }
    //------------------------------------------------------------
    this.getLanguageSkills = function(){
        var data = "";
        jQuery(".LanguageSkillsCheckbox").each(function(){
            var event = jQuery(this).parent().parent();
            data += ',{"checked":"'+(jQuery(this).is(":checked")?1:0)+'","value":"'+jQuery(this).val()+'","selected":"'+jQuery(event).find("select[name='SpeakerLevel[]']").val()+'"}';
        });
        if(data != ""){
            data = "["+data.substring(1, data.length)+"]";
        }
        return data;
    }
    //------------------------------------------------------------
    this.init = function() {
    }
}