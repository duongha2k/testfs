


//-------------------------------------------------------------
//-------------------------------------------------------------
//-------------------------------------------------------------
ComputerPlusCertification.Instances = null;
//-------------------------------------------------------------
ComputerPlusCertification.CreateObject = function(config) {
    var obj = null;
    if (ComputerPlusCertification.Instances != null) {
        obj = ComputerPlusCertification.Instances[config.id];
    }
    if (obj == null) {
        if (ComputerPlusCertification.Instances == null) {
            ComputerPlusCertification.Instances = new Object();
        }
        obj = new ComputerPlusCertification(config);
        ComputerPlusCertification.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function ComputerPlusCertification(config) {
    var Me = this;
    this.id = config.id;
    this.width = (jQuery(window).width() * 90) / 100;
    this.height =(jQuery(window).height() * 80) / 100;
    //------------------------------------------------------------
    this.hideLoader = function()
    {
        $.fancybox.close();
    }
    
    //------------------------------------------------------------
    this.showLoader = function(showCloseButton)
    {
        var content = "<div style='width:"+Me.width+"px;height:"+Me.height+"px;' id='comtantPopupID'><div style='text-align:center;height:"+Me.height+"px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :showCloseButton,
            'hideOnOverlayClick' : false,
            'content': content
        }
        ).trigger('click');

    }
    this.cmdComputerPlusCertification = function()
    {
        Me.showLoader(true);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/tech/computer-plus-tag",
            data: "",
            success:function( html ) {
                jQuery("#comtantPopupID").html(html);
                jQuery("#cmdComputerPTContinue")
                .unbind("click",Me.cmdComputerPTContinue)
                .bind("click",Me.cmdComputerPTContinue);
                jQuery("#cmdcomputerFTSubmit")
                .unbind("click",Me.cmdcomputerFTSubmit)
                .bind("click",Me.cmdcomputerFTSubmit); 
                jQuery("#cmdComputerComeback")
                .unbind("click",Me.cmdComputerComeback)
                .bind("click",Me.cmdComputerComeback);
            },
            error:function(error) {
                alert('error; ' + eval(error));
            }
        });
    }
    //------------------------------------------------------------
    this.cmdComputerPTContinue = function()
    {
        jQuery("#computerPTInformation").hide();
        jQuery("#computerPTFormAnswer").show();
        jQuery("#computerPTForm").scrollTop(0);
    }
    this.cmdComputerComeback = function()
    {
        jQuery("#computerPTForm").show();
        jQuery("#computerPTFail").hide();
        jQuery("#computerPTForm").scrollTop(0);
    }
    this.cmdcomputerFTSubmit = function()
    {
        var _data = jQuery("#frmComputerFT").serializeArray();
        $.ajax({
            type: "POST",
            url: "/techs/Training/ComputersPlus/ComputerPlusTest.php",
            data: _data,
            success:function( html ) {
                if(jQuery.trim(html) != "")
                {
                    var json = eval("("+html+")");
                    if(json.success == "0" && json.msg != "")
                    {
                        window.location = json.msg;
                    }    
                    else if(json.success == "2")
                    {
                        jQuery("#computerPTForm").hide();
                        jQuery("#computerPTFail").show();
                        jQuery("#computerFTRate").html(json.msg);
                    }    
                    else if(json.success == "1")
                    {
                        jQuery("#computerPTForm").hide();
                        jQuery("#computerPTPass").show();
                        setTimeout(function() {
                            window.location.reload();
                        },2000);
                        
                    }
                }
            },
            error:function(error) {
                alert('error; ' + eval(error));
            }
        });
    }
    this.buildEvent = function()
    {
        jQuery(".cmdComputerPlusCertification").unbind("click",Me.cmdComputerPlusCertification).bind("click",Me.cmdComputerPlusCertification); 
    }
    //------------------------------------------------------------
    this.init = function() {
        Me.buildEvent();
    }
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
}