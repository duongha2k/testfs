//-------------------------------------------------------------
//-------------------------------------------------------------
wosTechDetails.Instances = null;
//-------------------------------------------------------------
wosTechDetails.CreateObject = function(config) {
    var obj = null;
    if (wosTechDetails.Instances != null) {
        obj = wosTechDetails.Instances[config.id];
    }
    if (obj == null) {
        if (wosTechDetails.Instances == null) {
            wosTechDetails.Instances = new Object();
        }
        obj = new wosTechDetails(config);
        wosTechDetails.Instances[config.id] = obj;
    }
    obj.init();

    return obj;
}
//-------------------------------------------------------------
function wosTechDetails(config) {
    var Me = this;
    this.id = config.id;   
    this.detailsWidget=config.detailsWidget;
    this.roll = new FSPopupRoll();
    this.win = config.win;
    this.techid = config.techid;
    this.leavePage = true;
    this.eventLeavePage = null;
    this.go = 0;
    //------------------------------------------------------------
    this.TechapplyWOBtn_Onclick = function(){
        var opt = {
            url         : "/widgets/dashboard/popup/bid-search-wo/",
            width       : 450,
            height      : "",
            data        : 'win='+Me.win+"&tech_id="+Me.techid,
            title       : "",
            body        : "",
            successHander: Me.eventApplyWo
        };
        Me.roll.showAjax(this, this, opt);
    }
    
    this.QABox = function (rel) {
        
        var rel = '/widgets/dashboard/popup/questionandanswer?WIN_NUM=' + $(this).attr('WIN_NUM');
        var content = "<div id='comtantPopupID'><div style='text-align:center;height:380px;width:900px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :true,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'content': content
        }
        ).trigger('click');

        $.ajax({
            type: "POST",
            url: rel,
            data: "",
            context     : this,
            success:function( html ) {
                jQuery("#comtantPopupID").html(html);
                jQuery("#cmdPostQuestion")
                .unbind("click",Me.PostQuestion)
                .bind("click",Me.PostQuestion); 
                
                //674
                jQuery("#qaapplybid")
                .unbind("click",Me.ApplyonQA)
                .bind("click",Me.ApplyonQA); 
                
                jQuery("#qawidthdrawbid")
                .unbind("click",Me.opentechwidthdrawQA)
                .bind("click",Me.opentechwidthdrawQA); 
                //end 674
            },
            error:function(error) {
                alert('error; ' + eval(error));
            }
        });
    }
    
    //674
    this.ApplyonQA  = function()
    {
        win = $(this).attr('win');
        techid = $(this).attr('techid');
        
        var opt = {
            url         : "/widgets/dashboard/popup/bid-search-wo/",
            width       : 450,
            height      : "",
            data        : 'win='+win+"&tech_id="+techid,
            title       : "",
            body        : "",
            zindex      : 9999,
            successHander: Me.eventApplyWoqa
        };
        roll.showAjax($("#winnum"), $("#winnum"), opt);
    }
     this.eventApplyWoqa = function(){
        quickBid._roll = roll;
        quickBid.eventbutton = jQuery("#qaapplybid");
        quickBid.afterBid = function(){
            window.location.reload();
        };
        quickBid.initBidForm();
    }    
    this.opentechwidthdrawQA = function()
    {
        win = $(this).attr('win');
        techid = $(this).attr('techid');
        var strtitle ="<div style='font-size: 13px;width:200px;'>Review Application <a class=ajaxLink href=wosDetails.php?id="+win+"><strong>WIN# "+win+"</strong></a></div>";
        var opt = {
            url         : "/widgets/dashboard/popup/bid-withdraw-qa/",
            width       : 450,
            height      : "",
            data        : 'win='+win+"&tech_id="+techid,
            title       : strtitle,
            body        : "",
            zindex      : 9999
//            successHander: eventApplyWo
        };
        roll.showAjax($("#winnum"), $("#winnum"), opt);
    };
    //end 674
    
    this.PostQuestion = function () {
        var data = jQuery("#frmPostQuestion").serialize();
        if($("#frmPostQuestion").find("#question").val()!="")
        {
        $.ajax({
            type: "POST",
            url: '/widgets/dashboard/popup/postquestion',
            data: data,
            context : this,
            success:function( html ) {
                jQuery("#comtantPopupID").html("<div style='width:200px;'>"+html+"</div>");
                $('#fancybox-content').width(400);
                $('#fancybox-wrap').width(420);
                $.fancybox.resize();
            },
            error:function(error) {
                alert('error; ' + eval(error));
            }
        });
    }
    }
    //------------------------------------------------------------
    this.confirmSubmitBtn_Onclick = function(){
        Me.detailsWidget.updateWO({
            validator : null, 
            options : {
                'WorkOrderReviewed' : 'True'
            }
        });
    }
    this.confirm24SubmitBtn_Onclick = function(){
        Me.detailsWidget.updateWO({
            validator : null, 
            options : {
                'TechCheckedIn_24hrs' : 'True'
            }
        });
    }
    //------------------------------------------------------------
    Me.eventApplyWo = function(){
        quickBid._roll = Me.roll;
        quickBid.eventbutton = jQuery("#TechapplyWOBtn");
        quickBid.afterBid = function(){
            window.location.reload();
        };
        quickBid.initBidForm();
    }
    //------------------------------------------------------------
    this.jCustomSelect = function(cont){  
        var selectors = jQuery('.customSelect');
        if(cont){
            selectors = cont.find('.customSelect');
        }
        if (selectors.length > 0) {
            selectors.each(function(index,selector) {
                runCustomSelect(selector);
            });
        }
	
        function runCustomSelect(sel,afterOnClick) {		
            var selectorObj = jQuery(sel);
            selectorObj[0].csValue = selectorObj.find('.csValue')[0];		
            selectorObj[0].csText = selectorObj.find('.csText')[0];		
            if(!selectorObj.find('select')[0]) return;
            if(selectorObj.find('.csIcon').attr("disabled") == "true") return;
            var strLi = '';
            selectorObj.find('select option').each(function(index, opt){
                if(index==0){
                    strLi = '<li class="first" id="' + jQuery(opt).val() + '">'+ jQuery(opt).html() +'</li>';				
                }else{
                    strLi += '<li id="' + jQuery(opt).val() + '">'+ jQuery(opt).html() +'</li>';
                }
                if(jQuery(opt).attr('selected')){
                    if(typeof( selectorObj[0].csValue) != 'undefined') selectorObj[0].csValue.value = jQuery(opt).val();
                    selectorObj[0].csText.value = jQuery(opt).html();	
                }
            });
            if(typeof(selectorObj[0].csText.csLayer)!='undefined'){
                selectorObj[0].csText.csLayer.remove();
            }
            selectorObj[0].csText.csLayer = jQuery('<div class="customOption"><div class="csLayer"><ul>' + strLi + '</ul></div></div>');		
            selectorObj[0].csText.csLayer.appendTo(document.body);
            selectorObj[0].csText.csLayer.data('opened',false);
            
            selectorObj.find('.csIcon').unbind().click(function(e){
                e.preventDefault();
                var liHeight = jQuery(selectorObj[0].csText.csLayer.find('li')[0]).outerHeight();
                if(selectorObj[0].csText.csLayer.find('li').length > 4 ){
                    var _height = Math.min(liHeight * 4, ((selectorObj[0].csText.csLayer.find('li').length) * liHeight));
                    _height = 182;
                }else {
                    var _height = (selectorObj[0].csText.csLayer.find('li').length * liHeight);
                }
                var _width = selectorObj.outerWidth();
                var coords = selectorObj.offset();
                var coords2 = selectorObj.find('.csIcon').offset();
                
                if(!selectorObj[0].csText.csLayer.data('opened')){
                    selectorObj[0].csText.csLayer.addClass('smScrollContent').css({
                        'z-index': 9999,
                        'overflow': 'hidden',
                        'height': _height,
                        'width': _width - 19,
                        'position': 'absolute',
                        'top': coords.top + selectorObj.outerHeight(),
                        'left': coords2.left
                    });                
                    selectorObj[0].csText.csLayer.data('opened',true);
                }else{
                    close();
                }
            
                selectorObj[0].csText.csLayer.find(':first').css('height', _height);
                if(jQuery(selectorObj[0].csText.csLayer.find('li').length > 4)){
                    selectorObj[0].csText.csLayer.find(':first').css({
                        'overflow':'hidden',
                        'overflow-y': 'auto'
                    });
                }
                // selectorObj[0].csText.csLayer.find(".csLayer").scrollTo(selectorObj[0].csText.csLayer.find(".csLayer li[id='9:00 AM']"));
                jQuery(".csLayer").scrollTop(650);
            });
				
            selectorObj[0].csText.csLayer.find('li').each(function (ind, li) {
                jQuery(li).css('cursor', 'pointer');
                jQuery(li).unbind().bind({
                    'click': function(e){
                        e.preventDefault();
                        jQuery(selectorObj[0].csText).focus();
                        var valueCbx = "";
                        if(typeof( selectorObj[0].csValue) != 'undefined'){
                            valueCbx = jQuery(this).attr('id');
                            selectorObj[0].csValue.value = valueCbx;
                        }else{
                            valueCbx = jQuery(this).text();
                        }
                        selectorObj[0].csText.value = jQuery(this).text();		
                        woDetails.gettotalduration(document.getElementById('woForm'));
                        //callback
                        callback(valueCbx);
                        //end callback	
                        close();
                    },
                    'mouseenter': function(){
                        selectorObj[0].csText.csLayer.find('li').removeClass('current');
                        jQuery(this).addClass('current');					
                    }
                });
            });
            function callback(value){
                if(selectorObj.find(".ServiceScheduleTimeItem").length > 0){
                     Me.detailsWidget._modified = true;
                }
				Me.TechCheckCheckChange_change(null,selectorObj);
            }
            function close(layer){
                if(typeof(layer)!='undefined'){
                    layer.css('top', -15000);
                    layer.data('opened',false);
                }else{
                    selectorObj[0].csText.csLayer.css('top', -15000);
                    selectorObj[0].csText.csLayer.data('opened',false);
                }
            }
        }
        jQuery(document).unbind('mousedown').mousedown(function(e){				
            if(!jQuery(e.target).hasClass('customOption') && jQuery(e.target).parents('.customOption').length == 0 /*&& !jQuery(e.target).hasClass('smScroller') && jQuery(e.target).parents('.smScroller').length == 0*/){
                jQuery('.customOption').css('top', -15000);
                jQuery('.customOption').each(function(index, sel){				
                    jQuery(sel).data('opened',false);
                });
            }
        });

    }
    //------------------------------------------------------------
    this.TimeInput_blur = function(event,el){
        if(typeof(el) != 'undefined') timeObject = el;
        else timeObject = this;
        var r1 = /^(1[0-2]|0?\d)\s*(am|pm)$/i;
        var r2 = /^(1[0-2]|0?\d)\:([0-5]\d)\s*(am|pm)$/i;
        var result;
        var time = $.trim($(timeObject).val()).toUpperCase();
        
       
	       if ( time && (result = time.match(r1)) != null ) {
	            $(timeObject).val(parseInt(result[1],10).toString() + ":00 " + result[2]);
	        } else if ( time && (result = time.match(r2)) != null ) {
	            $(timeObject).val(parseInt(result[1],10).toString() + ":" + result[2] + " " + result[3]);
	        } else {
	            $(timeObject).val(time);
	        }
    }
    //------------------------------------------------------------
    this.eventVisit = function(){
    	
        $('.CalendarInput').calendar({
            dateFormat:'MDY/'
        }); 
        
        jQuery(".TimeInput")
        .unbind("blur",Me.TimeInput_blur)
        .bind("blur",Me.TimeInput_blur);
        
        detailObject.onInit.watermark();
        
    }
    //------------------------------------------------------------
    this.buildGUIVisit = function(i){
        var html = jQuery("#trVisitEdit").html();
        html = "<tr class='trVisitEdit trVisitEdit"+i+"'>"+html+"</tr>";
        html = html.replace(/CheckInDate1/g, "CheckInDate"+i);
        html = html.replace(/CheckInTime1/g, "CheckInTime"+i);
        html = html.replace(/CheckOutDate1/g, "CheckOutDate"+i);
        html = html.replace(/CheckOutTime1/g, "CheckOutTime"+i);
        html = html.replace(/WoVisitID1/g, "WoVisitID"+i);
        return html;
    }
    //------------------------------------------------------------
    this.cmdAddVisit_Onclick = function(){
        var lengthVisit = jQuery(".trVisitEdit").length;
        if(typeof(lengthVisit) == 'undefined') lengthVisit = 0;
        lengthVisit++;
        var content = Me.buildGUIVisit(lengthVisit);
        jQuery("#tableVisitEdit").append(content);
        //clear data
        jQuery(".trVisitEdit"+lengthVisit+" input:text").val("");
        jQuery(".trVisitEdit"+lengthVisit+" input:hidden").val("");
        jQuery(".trVisitEdit"+lengthVisit+" .numberVisit").html(lengthVisit);
        Me.eventVisit();
        Me.jCustomSelect(jQuery(".trVisitEdit"+lengthVisit));
        
    }
    this.LeavePageSubmit_onclick = function(){
        Me.eventLeavePage = jQuery(this);
        return false;
    }
    //------------------------------------------------------------
    this.LeavePage_onclick = function(){
        Me.eventLeavePage = jQuery(this);
    }
    //------------------------------------------------------------
    this.leavePageYes = function() {
        $.fancybox.close();
        Me.detailsWidget._modified = false;
        if(Me.eventLeavePage == null) {
            window.location.href = window.location.href;
            return false;
        }
        var href = jQuery(Me.eventLeavePage).attr("href");
        if(typeof(href) == 'undefined'){
            jQuery(Me.eventLeavePage).click();
        } else {
            if(href.indexOf("/") >= 0
                && href.indexOf("javascript") < 0){
                location.href = href;
            }else{
                jQuery(Me.eventLeavePage).click();
            }
        }
        Me.eventLeavePage = null;
    //return false;
    }
    this.fancyboxclose = function(){
        Me.eventLeavePage = null;
    }
    //------------------------------------------------------------
    this.buildEventLeavepage = function(){
        
        $(window).unbind("beforeunload");
        $(window).bind("beforeunload", function(){
            if(Me.detailsWidget._modified){
                $("<div></div>").fancybox(
                {
                    'autoDimensions' : true,
                    'showCloseButton' : true,
                    'hideOnOverlayClick' : false,
                    'enableEscapeButton':false,
                    'content': '<div style="padding-bottom:7px;width:330px;">Don\'t forget to save your changes!</div><div style="text-align:center;"><input id="cmdSaveNoGo" type="button" class="link_button" value="Save" />&nbsp;&nbsp;<input class="link_button" id="cmdDontSave" type="button" value="Don\'t Save" /></div>'
                }
                ).trigger('click');
            }
            $("#cmdDontSave").css('background',' url("/widgets/images/button_middle.png") no-repeat scroll left top transparent');
            $("#cmdSaveNoGo").css('background',' url("/widgets/images/button_middle.png") no-repeat scroll left top transparent');
            $("#fancybox-close").css('background','url(\'/widgets/js/fancybox/fancybox.png\') -40px 0px');
            
            jQuery("#cmdSaveNoGo").unbind('click',Me.LeavePage_onclick);
            jQuery("#cmdDontSave").unbind('click',Me.LeavePage_onclick);
            jQuery("#fancybox-close").unbind('click',Me.fancyboxclose).bind('click',Me.fancyboxclose);
            jQuery("#cmdSaveNoGo").click(function() { 
                $.fancybox.close();
                jQuery("#hidsavetype").val(1);
                Me.detailsWidget.updateWOTechNew();
            });
            jQuery("#cmdDontSave").click(function() { 
                $.fancybox.close();
                Me.leavePageYes();
            });
        });
        // ask the user to confirm the window closing
        window.onbeforeunload = function()
        {
            if(Me.detailsWidget._modified){
                window.setTimeout(function() {
                    if(jQuery.browser.msie){
                        document.execCommand('Stop');
                    }else{
                        window.stop();
                    }
                }, 1);
            }
        }
        jQuery("a").unbind('click',Me.LeavePage_onclick).bind('click',Me.LeavePage_onclick);
        jQuery("input:button").unbind('click',Me.LeavePage_onclick).bind('click',Me.LeavePage_onclick);
        jQuery("input:submit").unbind('click',Me.LeavePage_onclick).bind('click',Me.LeavePage_onclick);
        jQuery(".woSections_TopInfo input:button").unbind('click',Me.LeavePage_onclick);
        jQuery(".woSections_TopInfo input:submit").unbind('click',Me.LeavePage_onclick);
        jQuery(".woSections_TopInfo a").unbind('click',Me.LeavePage_onclick);
        jQuery("#FindWorkOrderBtn").unbind('click',Me.FindWorkOrderBtn_click).bind('click',Me.FindWorkOrderBtn_click);
    }
    //------------------------------------------------------------
    this.FindWorkOrderBtn_click = function(){
        jQuery("#fWOrdForm input:submit").unbind('click',Me.LeavePageSubmit_onclick).bind('click',Me.LeavePageSubmit_onclick);
    }
    //------------------------------------------------------------
    this.reloadPage = function(){
        Me.detailsWidget._modified = false;
        if($("#backBtn").attr("class") == "link_button back_button") 
        {
            jQuery("#backBtn").click();
        }
        else {
        	location.reload();
        }
    }
    //893
    //------------------------------------------------------------
    this.backBtn_onclick = function(){
        //var i = 0;
        if($.browser.msie)
        {
            if(history.length > 0) {
                history.go(-1);   
            }
        }
        else
        {
			var i = -(history.length + 1 - Me.go);
			if(history.length>1) history.go(i);
        }
    }
    //end 893
    
    //------------------------------------------------------------
    this.init = function() {
        if(history.length>1) Me.go = history.length;
        jQuery("#TechapplyWOBtn")
        .unbind("click", Me.TechapplyWOBtn_Onclick)
        .bind("click", Me.TechapplyWOBtn_Onclick);
        jQuery("#confirmSubmitBtn")
        .unbind("click", Me.confirmSubmitBtn_Onclick)
        .bind("click", Me.confirmSubmitBtn_Onclick);
        jQuery("#confirm24hSubmitBtn")
        .unbind("click", Me.confirm24SubmitBtn_Onclick)
        .bind("click", Me.confirm24SubmitBtn_Onclick);
        jQuery("#cmdAddVisit")
        .unbind("click", Me.cmdAddVisit_Onclick)
        .bind("click", Me.cmdAddVisit_Onclick);
        jQuery(".TechQABtn")
        .unbind("click", Me.QABox)
        .bind("click", Me.QABox);
        Me.eventVisit();
        
        Me.jCustomSelect();
        jQuery(".TimeInput").unbind("change",Me.TechCheckCheckChange_change).bind("change",Me.TechCheckCheckChange_change);
        
        if(typeof($(".hoverText").bt) != 'undefined'){
            $(".hoverText").bt({
                titleSelector: "attr('bt-xtitle')",
                fill: 'white',
                cssStyles: {
                    color: 'black', 
                    fontWeight: 'bold', 
                    width: '130px'
                },
                animate:true
            });   
        }
        Me.buildEventLeavepage();
        jQuery("#backBtn")
        .unbind("click", Me.backBtn_onclick)
        .bind("click", Me.backBtn_onclick);
    }

    this.TechCheckCheckChange_change = function(ev, event){
        var checkin, checkout, row;
        if(typeof(event) == 'undefined') event = this;
        row = $(event).parent().parent();
        event = $(event).parent().parent().parent().parent().parent();
        
        if($(event).attr("class") === "trVisitEdit"){
        	row = $(event);
        	event = $(this).parent().parent().parent().parent();
        }
        
        checkin = jQuery(event).find(".CheckInTime");
        checkout = jQuery(event).find(".CheckOutTime");

        $.each (checkin, function (key, value) {
        	var time = $.trim($(this).val()).toUpperCase();
        	if($(this).val() != "" && $(this).val() != null){
        		if(time.match(/(0?\d|1[0-2]):(0\d|[0-5]\d) (AM|PM)/i) == null){
        			//alert("Check In/Out time formatting is not H:MM AM/PM");
        			//$(this).val("");
        		}else{
        			Me.TimeInput_blur(null, value);
        		}
        	}
        });

        $.each (checkout, function (key, value) {
        	var time = $.trim($(this).val()).toUpperCase();
        	if($(this).val() != "" && $(this).val() != null){
        		if(time.match(/(0?\d|1[0-2]):(0\d|[0-5]\d) (AM|PM)/i) == null){
        			//alert("Check In/Out time formatting is not H:MM AM/PM");
        			//$(this).val("");
        		}else{
        			Me.TimeInput_blur(null, value);
        		}
        	}
        });

        var TechCheckCheckInDate = jQuery.trim(jQuery(row).find(".CheckInDate").val()) == "MM/DD/YYYY"?"":jQuery(row).find(".CheckInDate").val();
        var TechCheckCheckInTime = jQuery.trim(jQuery(row).find(".CheckInTime").val()) == "H:MM AM/PM"?"":jQuery(row).find(".CheckInTime").val();
        var TechCheckCheckOutDate = jQuery.trim(jQuery(row).find(".CheckOutDate").val()) == "MM/DD/YYYY"?"":jQuery(row).find(".CheckOutDate").val();
        var TechCheckCheckOutTime = jQuery.trim(jQuery(row).find(".CheckOutTime").val()) == "H:MM AM/PM"?"":jQuery(row).find(".CheckOutTime").val();
        var dataempty = false;
        if(TechCheckCheckInDate == "") dataempty = true;
        if(TechCheckCheckInTime == "") dataempty = true;
        if(TechCheckCheckOutDate == "") dataempty = true;
        if(TechCheckCheckOutTime == "") dataempty = true;
        if(dataempty){
            jQuery(row).find(".DisplayTechTotalHours").html("");
            jQuery(row).find(".DisplayTechTotalHours").attr("total",0);
            Me.TotalDuration();
            return false;
        }
        var data = 'CORCheckInDate='+ TechCheckCheckInDate 
        +'&CORCheckInTime='+ TechCheckCheckInTime 
        +'&CORCheckOutDate='+ TechCheckCheckOutDate 
        +'&CORCheckOutTime='+ TechCheckCheckOutTime; 
        $.ajax({
            url : "/widgets/dashboard/details/get-total-hours",
            data : data,
            cache : false,
            type : 'POST',
            success : function(data) {
                jQuery(row).find(".DisplayTechTotalHours").html(data.result);
                jQuery(row).find(".DisplayTechTotalHours").attr("total",data.total);
                Me.TotalDuration();
            }
        });
    }
    //------------------------------------------------------------
    this.TotalDuration = function(){
        var str_total;
        var total = 0;
        var displayTotalDuration = "";
        var edit_calculatedTechHrs = "";
        jQuery(".DisplayTechTotalHours").each(function(){
            str_total = jQuery(this).attr("total");
            if (typeof (str_total) != "undefined")
                total += parseInt(jQuery(this).attr("total"));
        });
        if(total > 0){
            var ost = total%3600;
            displayTotalDuration = (!ost) ? (total/3600)
            +((total/3600) == 1?' hour':' hours') : ((total-ost)/3600)
            +(((total-ost)/3600) == 1?' hour ':' hours ')+Math.round(ost/60)+' min';
            edit_calculatedTechHrs = total/3600;
        }
        
        jQuery("#displayTotalDuration").html(displayTotalDuration);
    /*        if(!Me.fb){
            jQuery("#edit_calculatedTechHrs").val(edit_calculatedTechHrs);
            if(jQuery("#edit_Amount_Per option:selected").val() == "Hour"){
                $("#edit_baseTechPay").val("");
                Me.detailsWidget.updateFinalPay();
	        }
	    }
        Me.fb = false;*/
    }
}
