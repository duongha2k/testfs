


//-------------------------------------------------------------
//-------------------------------------------------------------
//-------------------------------------------------------------
ServRightBrotherCertification.Instances = null;
//-------------------------------------------------------------
ServRightBrotherCertification.CreateObject = function(config) {
    var obj = null;
    if (ServRightBrotherCertification.Instances != null) {
        obj = ServRightBrotherCertification.Instances[config.id];
    }
    if (obj == null) {
        if (ServRightBrotherCertification.Instances == null) {
            ServRightBrotherCertification.Instances = new Object();
        }
        obj = new ServRightBrotherCertification(config);
        ServRightBrotherCertification.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function ServRightBrotherCertification(config) {
    var Me = this;
    this.id = config.id;
    this.width = ((jQuery(window).width() * 90) / 100) - 5;
    this.height =(jQuery(window).height() * 90) / 100;
    //------------------------------------------------------------
    this.hideLoader = function()
    {
        $.fancybox.close();
    }
    
    //------------------------------------------------------------
    this.showLoader = function(showCloseButton)
    {
        var content = "<div style='width:"+Me.width+"px;' id='comtantPopupID'><div style='text-align:center;height:"+Me.height+"px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :showCloseButton,
            'hideOnOverlayClick' : false,
            'content': content
        }
        ).trigger('click');

    }
    this.cmdServRightBrotherCertification = function()
    {
//        var profile = jQuery(this).attr("profile");
        var data = "";
//        if(typeof(profile) != 'undefined')
//           data = "profile=1"; 
        Me.showLoader(true);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/tech/servright-brother-certification",
            data: data,
            success:function( html ) {
                jQuery("#comtantPopupID").html(html);
                jQuery("#ServRightBrotherCertificationContinue")
                .unbind("click",Me.ServRightBrotherCertificationContinue)
                .bind("click",Me.ServRightBrotherCertificationContinue);
                jQuery("#ServRightBrotherCertificationContinue2")
                .unbind("click",Me.ServRightBrotherCertificationContinue2)
                .bind("click",Me.ServRightBrotherCertificationContinue2);
                
                jQuery("#ServRightReturntoInstructions")
                .unbind("click",Me.ServRightReturntoInstructions)
                .bind("click",Me.ServRightReturntoInstructions);
                
                jQuery("#ServRightContinuetoTesting")
                .unbind("click",Me.ServRightContinuetoTesting)
                .bind("click",Me.ServRightContinuetoTesting);
                
            },
            error:function(error) {
                alert('error; ' + eval(error));
            }
        });
    }
    //------------------------------------------------------------
    this.ServRightBrotherCertificationContinue = function()
    {
        jQuery("#ServRightBrotherCertificationForm").css("height",(Me.height - 100)+"px"); 
        jQuery("#ServRightBrotherCertificationInformation").hide();
        jQuery("#ServRightBrotherCertificationFormAnswer").show();
    }
    this.ServRightBrotherCertificationContinue2 = function()
    {
        jQuery("#ServRightBrotherCertificationFormAnswer").hide();
        jQuery("#ServRightBrotherCertificationFormResult").show();
        jQuery("#ServRightBrotherCertificationForm").css("height","auto"); 
        jQuery("#comtantPopupID").css("height","auto"); 
    }
    this.ServRightReturntoInstructions = function()
    {
        jQuery("#ServRightBrotherCertificationForm").css("height",Me.height+"px");  
        jQuery("#ServRightBrotherCertificationFormResult").hide();
        jQuery("#ServRightBrotherCertificationFormAnswer").show();
    }
    this.ServRightContinuetoTesting = function()
    {
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/tech/servright-brother-certification",
            data: "saveclicked=1",
            success:function( html ) {
               
            },
            error:function(error) {
                alert('error; ' + eval(error));
            }
        });
        window.open("http://support.brother-usa.com/default.aspx");
//        var url = window.location;
//        url = url.toLowerCase();
//        if(url.indexOf("updateprofile.php"))
//            window.location.refresh();
    }
    //------------------------------------------------------------
    this.buildEvent = function()
    {
        jQuery(".ServRightBrotherCertification").unbind("click",Me.cmdServRightBrotherCertification).bind("click",Me.cmdServRightBrotherCertification); 
    }
    //------------------------------------------------------------
    this.init = function() {
        Me.buildEvent();
    }
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
}