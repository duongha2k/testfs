﻿<?php $page = 'login'; ?>
<?php $option = 'tech'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); 
require_once dirname(__FILE__).'/../../includes/modules/common.init.php';
$common = new Core_Api_CommonClass;
$statesOptions = $common->getStatesArray('US');

$statesHtml = "<option value=\"\">Select State</option>";
foreach ($statesOptions->data as $code => $name) {
        $statesHtml .= "<option value=\"$code\">$name</option>";
}


// Create a random string, leaving out 'o' to avoid confusion with '0'
$char = strtoupper(substr(str_shuffle('abcdefghjkmnpqrstuvwxyz'), 0, 4));

// Concatenate the random string onto the random numbers
// The font 'Anorexia' doesn't have a character for '8', so the numbers will only go up to 7
// '0' is left out to avoid confusion with 'O'
$str = rand(1, 7) . rand(1, 7) . $char;

// Set the session contents
$_SESSION['captcha_id'] = $str;

$common = new Core_Api_CommonClass;
$usStates = $common->getStatesArray('US');
$canadaStates = $common->getStatesArray('CA');
$mexicoStates = $common->getStatesArray('MX');
$CaymanIslands = $common->getStatesArray('KY');
$Bahamas = $common->getStatesArray('BS');
$Brazil = $common->getStatesArray('BR');
$England = $common->getStatesArray('UK');
//14041
$Argentina = $common->getStatesArray('AR');
$Caribbean = $common->getStatesArray('CB');
$CostaRica = $common->getStatesArray('CR');
$Venezuela = $common->getStatesArray('VE');
$Austria = $common->getStatesArray('AT');
$Belgium = $common->getStatesArray('BE');
$Denmark = $common->getStatesArray('DK');
$France = $common->getStatesArray('FR');
$Germany = $common->getStatesArray('DE');
$Ireland = $common->getStatesArray('IE');
$Italy = $common->getStatesArray('IT');
$Netherlands = $common->getStatesArray('NL');
$Spain = $common->getStatesArray('SP');
$Switzerland = $common->getStatesArray('CH');
$Sweden = $common->getStatesArray('SE');
$Turkey = $common->getStatesArray('TR');
$Australia = $common->getStatesArray('AU');
$China = $common->getStatesArray('CN');
$HongKong = $common->getStatesArray('HK');
$India = $common->getStatesArray('IN');
$Japan = $common->getStatesArray('JP');
$Malaysia = $common->getStatesArray('MY');
$Phillipines = $common->getStatesArray('PH');
$Singapore = $common->getStatesArray('SG');
$SouthKorea = $common->getStatesArray('KR');
$Taiwan = $common->getStatesArray('TW');
$Thailand = $common->getStatesArray('TH');
//end 14041
//14145

$techsource = new Core_Api_TechClass;
//$sourceOptions = $common->getTechSourceArray();
$sourceOptions = $techsource->getTechSourceList_forRegisterPage();
$techSourceHtml = "<select class=\"required\" id=\"TechSource\" name=\"TechSource\" width=\"243\" style=\"width: 243px\"><option value=\"\">Select Source</option>";
foreach ($sourceOptions as $code) {
       
        $techSourceHtml .= "<option value=\"$code\">$code</option>";
   
}
$techSourceHtml .="</select>";   
//14145

$countryOptions = $common->getCountries();
$usStatesHtml = "<select class=\"required\" id=\"state\" name=\"state\" ><option value=\"\">All States</option>";
foreach ($usStates->data as $code => $name) {
	$usStatesHtml .= "<option value=\"$code\">$name</option>";
}
$usStatesHtml .="</select>";

$canadaStatesHtml = "<select class=\"required\" id=\"state\" name=\"state\" ><option value=\"\">All States</option>";
foreach ($canadaStates->data as $code => $name) {
	$canadaStatesHtml .= "<option value=\"$code\">$name</option>";
}
$canadaStatesHtml .= "</select>";

$mexicoStatesHtml = "<select class=\"required\" id=\"state\" name=\"state\" ><option value=\"\">All States</option>";
foreach ($mexicoStates->data as $code => $name) {
	$mexicoStatesHtml .= "<option value=\"$code\">$name</option>";
}
$mexicoStatesHtml .= "</select>";

$CaymanIslandsHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>"; //14041
foreach ($CaymanIslands->data as $code => $name) {
    $CaymanIslandsHtml .= "<option value=\"$code\">$name</option>";
}
$CaymanIslandsHtml .= "</select>";

$BahamasHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";//14041
foreach ($Bahamas->data as $code => $name) {
    $BahamasHtml .= "<option value=\"$code\">$name</option>";
}
$BahamasHtml .= "</select>";

$BrazilHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   //14041
foreach ($Brazil->data as $code => $name) {
    $BrazilHtml .= "<option value=\"$code\">$name</option>";
}
$BrazilHtml .= "</select>";

$EnglandHtml = "<select class=\"required\" id=\"state\" name=\"state\" ><option value=\"\">All States</option>";
foreach ($England->data as $code => $name) {
    $EnglandHtml .= "<option value=\"$code\">$name</option>";
}
$EnglandHtml .= "</select>";
 //14041
 $ArgentinaHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($Argentina->data as $code => $name) {
    $ArgentinaHtml .= "<option value=\"$code\">$name</option>";
}
$ArgentinaHtml .= "</select>";

$CaribbeanHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($Caribbean->data as $code => $name) {
    $CaribbeanHtml .= "<option value=\"$code\">$name</option>";
}
$CaribbeanHtml .= "</select>";
$CostaRicaHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($CostaRica->data as $code => $name) {
    $CostaRicaHtml .= "<option value=\"$code\">$name</option>";
}
$CaribbeanHtml .= "</select>";
$VenezuelaHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($Venezuela->data as $code => $name) {
    $VenezuelaHtml .= "<option value=\"$code\">$name</option>";
}
$VenezuelaHtml .= "</select>";
$AustriaHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($Austria->data as $code => $name) {
    $AustriaHtml .= "<option value=\"$code\">$name</option>";
}
$AustriaHtml .= "</select>";
$BelgiumHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($Belgium->data as $code => $name) {
    $BelgiumHtml .= "<option value=\"$code\">$name</option>";
}
$BelgiumHtml .= "</select>";
$DenmarkHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($Denmark->data as $code => $name) {
    $DenmarkHtml .= "<option value=\"$code\">$name</option>";
}
$DenmarkHtml .= "</select>";
$FranceHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($France->data as $code => $name) {
    $FranceHtml .= "<option value=\"$code\">$name</option>";
}
$FranceHtml .= "</select>";
$GermanyHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($Germany->data as $code => $name) {
    $GermanyHtml .= "<option value=\"$code\">$name</option>";
}
$GermanyHtml .= "</select>";
$IrelandHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($Ireland->data as $code => $name) {
    $IrelandHtml .= "<option value=\"$code\">$name</option>";
}
$IrelandHtml .= "</select>";
$ItalyHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($Italy->data as $code => $name) {
    $ItalyHtml .= "<option value=\"$code\">$name</option>";
}
$ItalyHtml .= "</select>";
$NetherlandsHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($Netherlands->data as $code => $name) {
    $NetherlandsHtml .= "<option value=\"$code\">$name</option>";
}
$NetherlandsHtml .= "</select>";
$SpainHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($Spain->data as $code => $name) {
    $SpainHtml .= "<option value=\"$code\">$name</option>";
}
$SpainHtml .= "</select>";
$SwitzerlandHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($Switzerland->data as $code => $name) {
    $SwitzerlandHtml .= "<option value=\"$code\">$name</option>";
}
$SwitzerlandHtml .= "</select>";
$SwedenHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($Sweden->data as $code => $name) {
    $SwedenHtml .= "<option value=\"$code\">$name</option>";
}
$SwedenHtml .= "</select>";
$TurkeyHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($Turkey->data as $code => $name) {
    $TurkeyHtml .= "<option value=\"$code\">$name</option>";
}
$TurkeyHtml .= "</select>";
$AustraliaHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($Australia->data as $code => $name) {
    $AustraliaHtml .= "<option value=\"$code\">$name</option>";
}
$AustraliaHtml .= "</select>";
$ChinaHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($China->data as $code => $name) {
    $ChinaHtml .= "<option value=\"$code\">$name</option>";
}
$ChinaHtml .= "</select>";
$HongKongHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($HongKong->data as $code => $name) {
    $HongKongHtml .= "<option value=\"$code\">$name</option>";
}
$HongKongHtml .= "</select>";
$IndiaHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($India->data as $code => $name) {
    $IndiaHtml .= "<option value=\"$code\">$name</option>";
}
$IndiaHtml .= "</select>";
$JapanHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($Japan->data as $code => $name) {
    $JapanHtml .= "<option value=\"$code\">$name</option>";
}
$JapanHtml .= "</select>";
$MalaysiaHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($Malaysia->data as $code => $name) {
    $MalaysiaHtml .= "<option value=\"$code\">$name</option>";
}
$MalaysiaHtml .= "</select>";
$PhillipinesHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($Phillipines->data as $code => $name) {
    $PhillipinesHtml .= "<option value=\"$code\">$name</option>";
}
$PhillipinesHtml .= "</select>";
$SingaporeHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($Singapore->data as $code => $name) {
    $SingaporeHtml .= "<option value=\"$code\">$name</option>";
}
$SingaporeHtml .= "</select>";
$SouthKoreaHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($SouthKorea->data as $code => $name) {
    $SouthKoreaHtml .= "<option value=\"$code\">$name</option>";
}
$SouthKoreaHtml .= "</select>";
$TaiwanHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($Taiwan->data as $code => $name) {
    $TaiwanHtml .= "<option value=\"$code\">$name</option>";
}
$TaiwanHtml .= "</select>";
$ThailandHtml = "<select  id=\"state\" name=\"state\" ><option value=\"\">All States</option>";   
foreach ($Thailand->data as $code => $name) {
    $ThailandHtml .= "<option value=\"$code\">$name</option>";
}
$ThailandHtml .= "</select>";
//end 14041


$countriesHtml = "";
$dotline="----------------";
$icount=0;
foreach ($countryOptions->data as $country) {
   //14041 
    if($country->Code=="")
    {
       $countriesHtml .= "<option value=\"$country->Code\" disabled='disabled'>$country->Name</option>";  
    }
    
    else{
    $countriesHtml .= "<option value=\"$country->Code\">$country->Name</option>";
    }   //end  14041
}

$db = Core_Database::getInstance();
$select = $db->select();
$select->from("cell_carriers")
	->order('carrier ASC');
$result = Core_Database::fetchAll($select);

$cellProviderHtml = "<select id=\"CellProvider\" name=\"CellProvider\"><option value=\"\">Select One</option>";
foreach($result as $r){
	$carrier = $r['carrier'];
	$cellProviderHtml .= "<option value =\"$carrier\" >$carrier</option>";
}
$cellProviderHtml .= "</select>";

?>
<script type="text/javascript" src="/widgets/js/jquery.cookie.js"></script>

<script type="text/javascript" src="/widgets/js/jquery.validate.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.curvycorners.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.deserialize.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.easing.pack.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript" src="/widgets/js/functions.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.easing.pack.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.autotab.js"></script>
<link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" media="all" />
<?php script_nocache ("/widgets/js/FSTechRegistration.js"); ?>

<script type="text/javascript">

var email_handler = function(){
	name = $(this).attr("name");
	data = $(this).val();
	input = $(this);
	
	var displayMessage = "";
	submitData = "";
	if(name == "primaryEmail"){
		submitData = "field=PrimaryEmail&data="+data;
		displayMessage = "This email address is already in use, please try logging in above";
	}else{
		submitData = "field=UserName&data="+data;
		displayMessage = "This username is already in use, please try logging in above";
	}
	
	if(data != ""){
		currentRow = $(this).closest('tr');
		$(currentRow).find("#lookupIdle").attr("id", "lookupLoader");
		$.ajax({
			type: "POST",
			url: "/ajax/techRegisterLookup.php",
			dataType    : 'json',
			cache       : false,
			data: submitData,
			success: function (data) {
			$(currentRow).find("#lookupLoader").attr("id", "lookupIdle");
				if(data.message != "success"){
					$(input).addClass("invalid");
					if($(currentRow).next("tr").find(".status .invalid2").length == 0){
						$(currentRow).next("tr").find(".status").append("<label class='invalid2' for='primaryEmail' style='display:inline;'>"+displayMessage+"</label>");
					}
				}else{
					if(name == "primaryEmail")
						$("body").data("iso_id", data.iso);  
                        //14145  
                        if($("body").data("iso_id"))
                        {
                        
                                   $("#TechSource").find("option:contains('ISO')").each(function(){
                                 if( $(this).text() == 'ISO' ) {
                                    $(this).attr("selected","selected");
                                    }
                                  });
                           
                        }
                        //14145  
					if($(currentRow).next("tr").find(".status .invalid2").length != 0){
						$(currentRow).next("tr").find(".status .invalid2").remove();
						if($(input).hasClass("invalid") == true){
							$(input).removeClass("invalid");
                             
                            
						}
					}
				}
			}
		});
      
	}
    
};
//14145
var phone_handler = function(){
    name = $(this).attr("name");
    data = $(this).val();
    input = $(this);
    
    var displayMessage = "";
    submitData = "";
    if(name == "PrimaryPhone"){
       // submitData = "field=PrimaryPhone&data="+data;
        submitData = "data="+data;
        displayMessage = "This email address is already in use, please try logging in above";
    }else{
        submitData = "field=UserName&data="+data;
        displayMessage = "This username is already in use, please try logging in above";
    }
    
    if(data != ""){
        currentRow = $(this).closest('tr');
        $(currentRow).find("#lookupIdle").attr("id", "lookupLoader");
        $.ajax({
            type: "POST",
            url: "/ajax/techRegisterPhone.php",
            dataType    : 'json',
            cache       : false,
            data: submitData,
			error: function (x, y, z) {
				//console.log (x, y, z);
			},
            success: function (data) {
            $(currentRow).find("#lookupLoader").attr("id", "lookupIdle");
                if(data.message != "success"){
                    $(input).addClass("invalid");
                    if($(currentRow).next("tr").find(".status .invalid2").length == 0){
                        $(currentRow).next("tr").find(".status").append("<label class='invalid2' for='PrimaryPhone' style='display:inline;'>"+displayMessage+"</label>");
                    }
                }else{
                    if(name == "PrimaryPhone")
                    {
                        $("body").data("iso_id", data.iso);
                        $("body").data("exists", data.exists); 
                        //alert($("body").data("exists"));
                         
                        if($("body").data("iso_id") && $("body").data("exists")=='1' )
                        {
                        
                                   $("#TechSource").find("option:contains('ISO')").each(function(){
                                 if( $(this).text() == 'ISO' ) {
                                    $(this).attr("selected","selected");
                                    }
                                  });
                           
                        }
                        
                    } 
                    if($(currentRow).next("tr").find(".status .invalid2").length != 0){
                        $(currentRow).next("tr").find(".status .invalid2").remove();
                        if($(input).hasClass("invalid") == true){
                            $(input).removeClass("invalid");
                             
                            
                        }
                    }
                }
            }
        });
      
    } 
    
}; 
//14145
$(document).ready(function(){
	var tech_source = ($.cookie ("Tech_Source"));

	
	if (tech_source) $("#tech_source").val ($.cookie ("Tech_Source"));

		$("#refreshimg").live('click',function(){
			$.post('/ajax/newsession.php');
			$("#captchaimage").load('/ajax/image_req.php');
			return false;
		});
		$("#SSN1").autotab({target: 'SSN2'});
		$("#SSN2").autotab({target: 'SSN3'});
		$("#SSN3").autotab({target: 'DigSig'});

		$("#EIN1").autotab({target: 'EIN2'});
		$("#EIN2").autotab({target: 'DigSig'});
		$("#primaryPhone1,#primaryPhone2, #primaryPhone3, #secondaryPhone1, #secondaryPhone2, #secondaryPhone3").autotab_magic();
		
		
	$("#primaryEmail, #username").blur(email_handler);
    $("#PrimaryPhone, #username").blur(phone_handler);  //14145

	
	$("input[name=FTC]").click(function(){
		/*
		if($("input[name=FTC]:checked").attr("id") == "FTCOther"){
			$("#FTCOtherDisplay").show();
			$("#FTCOtherInput").addClass("required");
		}else{
			$("#FTCOtherDisplay").hide();
			$("#FTCOtherInput").removeClass("required");
		}
		*/

		if($("input[name=FTC]:checked").val() == "C Corp"){
			//$("#EIN1, #EIN2").addClass("required");
			//$("#SSN1, #SSN2, #SSN3").removeClass("required");
			$("#EINRadio").attr("checked","checked");
			$("#SSN1, #SSN2, #SSN3").val("");
		}else{
			//$("#SSN1, #SSN2, #SSN3").addClass("required");
			//$("#EIN1, #EIN2").removeClass("required");
			$("#SSNRadio").attr("checked","checked");
			$("#EIN1, #EIN2").val("");
		}
		/*
		if($("input[name=FTC]:checked").val() == "LLC"){
			$("#LLCTax").addClass("required");
		}else{
			$("#LLCTax").removeClass("required");
		}
		*/
	});
	
	$("#SSN1, #EIN1").keyup(function(e){
		$this = $(e.target);
		if($this.attr("id") == "SSN1" && $("input[name=FTC]:checked").val() != "C Corp"){
			$("#SSNRadio").attr("checked","checked");
			//$("#SSN1, #SSN2, #SSN3").addClass("required");
			//$("#EIN1, #EIN2").removeClass("required");
			//$("#EINRadio").removeClass("required");
			$("#EIN1, #EIN2").val("");
		}else{
			$("#EINRadio").attr("checked","checked");
			//$("#EIN1, #EIN2").addClass("required");
			//$("#SSN1, #SSN2, #SSN3").removeClass("required");
			//$("#SSNRadio").removeClass("required");
			$("#SSN1, #SSN2, #SSN3").val(""); 
		}
	});
	

	$("#country").change(function(e){
		$this = $(e.target);
		if($this.val()=="US"){
			$("#stateSelect").html('<?=$usStatesHtml;?>');
		}else if($this.val()=="CA"){
			$("#stateSelect").html('<?=$canadaStatesHtml;?>');
		}else if($this.val() =="MX"){
			$("#stateSelect").html('<?=$mexicoStatesHtml;?>');
        }else if($this.val() =="KY"){
            $("#stateSelect").html('<?=$CaymanIslandsHtml;?>');
        }else if($this.val() =="BS"){
            $("#stateSelect").html('<?=$BahamasHtml;?>');
        }else if($this.val() =="BR"){
            $("#stateSelect").html('<?=$BrazilHtml;?>');
        }else if($this.val() =="UK"){
            $("#stateSelect").html('<?=$EnglandHtml;?>');
		}  //14041
        else if($this.val() =="AR"){
            $("#stateSelect").html('<?=$ArgentinaHtml;?>');
        }
else if($this.val() =="CB"){
            $("#stateSelect").html('<?=$CaribbeanHtml;?>');
        }
else if($this.val() =="CR"){
            $("#stateSelect").html('<?=$CostaRicaHtml;?>');
        }
else if($this.val() =="VE"){
            $("#stateSelect").html('<?=$VenezuelaHtml;?>');
        }
else if($this.val() =="AT"){
            $("#stateSelect").html('<?=$AustriaHtml;?>');
        }
else if($this.val() =="BE"){
            $("#stateSelect").html('<?=$BelgiumHtml;?>');
        }
else if($this.val() =="DK"){
            $("#stateSelect").html('<?=$DenmarkHtml;?>');
        }
else if($this.val() =="FR"){
            $("#stateSelect").html('<?=$FranceHtml;?>');
        }
else if($this.val() =="DE"){
            $("#stateSelect").html('<?=$GermanyHtml;?>');
        }
else if($this.val() =="IE"){
            $("#stateSelect").html('<?=$IrelandHtml;?>');
        }
else if($this.val() =="IT"){
            $("#stateSelect").html('<?=$ItalyHtml;?>');
        }
else if($this.val() =="NL"){
            $("#stateSelect").html('<?=$NetherlandsHtml;?>');
        }
else if($this.val() =="SP"){
            $("#stateSelect").html('<?=$SpainHtml;?>');
        }
else if($this.val() =="CH"){
            $("#stateSelect").html('<?=$SwitzerlandHtml;?>');
        }
else if($this.val() =="SE"){
            $("#stateSelect").html('<?=$SwedenHtml;?>');
        }
else if($this.val() =="TR"){
            $("#stateSelect").html('<?=$TurkeyHtml;?>');
        }
else if($this.val() =="AU"){
            $("#stateSelect").html('<?=$AustraliaHtml;?>');
        }
else if($this.val() =="CN"){
            $("#stateSelect").html('<?=$ChinaHtml;?>');
        }
else if($this.val() =="HK"){
            $("#stateSelect").html('<?=$HongKongHtml;?>');
        }
else if($this.val() =="IN"){
            $("#stateSelect").html('<?=$IndiaHtml;?>');
        }
else if($this.val() =="JP"){
            $("#stateSelect").html('<?=$JapanHtml;?>');
        }
else if($this.val() =="MY"){
            $("#stateSelect").html('<?=$MalaysiaHtml;?>');
        }
else if($this.val() =="PH"){
            $("#stateSelect").html('<?=$PhillipinesHtml;?>');
        }
else if($this.val() =="SG"){
            $("#stateSelect").html('<?=$SingaporeHtml;?>');
        }
else if($this.val() =="KR"){
            $("#stateSelect").html('<?=$SouthKoreaHtml;?>');
        }
else if($this.val() =="TW"){
            $("#stateSelect").html('<?=$TaiwanHtml;?>');
        }
else if($this.val() =="TH"){
            $("#stateSelect").html('<?=$ThailandHtml;?>');
        }//end 14041
 var Requirecountry = {"US":"US","CA":"CA","MX":"MX"};
            
        if(Requirecountry.hasOwnProperty($("#country option:selected").val()))
        {
            $("#state").removeAttr("disabled");
             $("span[id*=reqStar1]").show(); 
        }
        else
        {
            $("#state option").each(function()
            {
                if ($(this).val() == '')
                {
                    $(this).attr("selected", "selected");
                }
            });
            $("#state").attr("disabled","disabled");
           $("span[id*=reqStar1]").hide(); 
            return false;
        }
         //end 14041
	});

	


	$("#toggleW9").click(function(){
		if($("#techW9").is(":hidden")){
			$("#techW9").show("slow");
			$("#toggleW9").val("Hide W-9");
		}else{
			$("#toggleW9").val("Complete W-9 Now");
			$("#techW9").slideUp();
		}
	});
});

function refreshimg(){
	$.post('/ajax/newsession.php');
	$("#captchaimage").load('/ajax/image_req.php');
	return false;
}

function disclaimerFunction() {
    $(".AcknowledgeOuterDiv").scroll(function() {
        var outerDiv = $(this);
        var innerDiv = $(">.AcknowledgeInnerDiv", $(this));
        var ScrollMod = 1;
        if (outerDiv.offset().top < innerDiv.outerHeight()) {
            ScrollMod = -1;
        }
        if (Math.round((ScrollMod * innerDiv.offset().top) + outerDiv.height() + outerDiv.offset().top) >= innerDiv.outerHeight() && Math.abs(innerDiv.offset().top) != 0) {
            $(".AcknowledgeCheckBox input").attr("disabled", false);
            $(this).unbind("scroll");
        } else {
            $(".AcknowledgeCheckBox input").attr("disabled", true);
        }
    });
}

function agreeICA() {
	$("#agreeICAYes").attr({"disabled" : false, "checked" : true});
}
function notAgreeICA() {
	$("#agreeICANo").attr({"checked" : true});
}

function agreeTerms() {
	$("#InsertRecordAcceptTerms").attr({"value" : "Yes"});
}
function notAgreeTerms() {
	$("#InsertRecordAcceptTerms").attr({"value" : "No"});
}

</script>

<?php
	$reg_template = $_SERVER['DOCUMENT_ROOT'] . "/templates/" . $siteTemplate . "/includes/registration.php";

	if (!file_exists ($reg_template)) {
		$reg_template = $_SERVER['DOCUMENT_ROOT'] . "/templates/www/includes/registration.php";
	}

	require ($reg_template)
?>

<br/>


<?php require ("../footer.php"); ?>
