<?php $page = 'techs'; ?>
<?php $option = 'backgroundChecks'; ?>
<?php $display = 'yes'; ?>
<?php $selected = 'backgroundFAQ'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->
<br />

      <h3>Background Checks - FAQ	
      </h3>
      <p style="font-weight: bold">Is a background check required? 
	<p>Field Solutions does not require a background check. The requirement for a background check is 100% driven by Clients. If a Client requires a background check and you have not passed it, you will not be eligible to work any jobs/installs that require a background check.<br>
        <br>
	    <span style="font-weight: bold">If I've had a background check performed for another company, can I use those results for Field Solutions?</span></p>
	<p>Background checks performed on behalf of another company cannot be transferred to or used by Field Solutions.</p>
	<p style="font-weight: bold">What company actually performs the background check?</p>
	<p>Field Solutions uses ADP for background checks.</p>
	<p style="font-weight: bold">If/when I have a background check performed for Field Solutions, how long is it good for? </p>
	<p>Background checks are valid for as long as you keep your technician account/profile with Field Solutions, or until client requirements change.  Clients normally want a background check to be performed annually.</p>
	<p style="font-weight: bold">If/when I have a background check performed for Field Solutions, can I use it for other companies I work for?</p>
	<p>That will be up to those individual companies.</p>
	<p style="font-weight: bold">Is the background check good for all clients of Field Solutions?</p>
	<p>Yes, as long as the client is okay with the level of background check performed. For example, if you had a "Basic" background check done and a particular client of Field Solutions required a check, you would need to have a "Comprehensive" background check performed in order to be eligible to work for that client or a specific project for that client. </p>
	<p style="font-weight: bold">Are there any additional background checks or drug tests that I will be required to submit in the future?</p>
	<p>Technicians are encouraged but not required to have the "Basic Background Check" ($19.00 non-refundable fee) performed at this time. Some clients have indicated that they may require more comprehensive background checks in the future. If/when a more comprehensive background check becomes a requirement for any client, techs will be notified and supplied with instructions plus additional fee information (if applicable).</p>
	<p><br />
	  Drug Tests - If a client requires a drug test, technicians will be notified and supplied with instructions plus fee information (if applicable).</p>
	<p style="font-weight: bold">How do I get the Basic Background Check started?</p>
	<p>Click <a onclick="$('<div></div>').fancybox({'autoDimensions' : false,'width' : 600,'height' : 330,'transitionIn': 'none','transitionOut' : 'none','type': 'iframe','href': '/techs/ajax/lightboxPages/backgroundChecks.php'}).trigger('click');" href="javascript:;">here</a> and follow the instructions step-by-step. Be sure not to skip any steps or do things out of order.<br>
        <br>
	    <span style="font-weight: bold">How much will it cost to have a Basic Background Check performed and where/how do I pay for it? </span></p>
	<p>The cost for a basic background check is $19.00. Prior to the background check being performed, you will need to submit the proper form(s) and payment to Field Solutions. Please click <a onclick="$('<div></div>').fancybox({'autoDimensions' : false,'width' : 600,'height' : 330,'transitionIn': 'none','transitionOut' : 'none','type': 'iframe','href': '/techs/ajax/lightboxPages/backgroundChecks.php'}).trigger('click');" href="javascript:;">here</a> for further details regarding payment and the entire process.</p>
<!--	<p><span style="font-weight: bold">Special Note:</span>  New York residents are assessed $55 more for background checks due to New York  State legislative requirements. Field Solutions will cover the extra costs for NY residents but that is subject to change at any time and without prior notice.</p>-->
	<p style="font-weight: bold">How long does it take to get the results back on a Basic Background Check and will I receive a copy of the results?</p>
	<p>On average, you should allow 3-5 business days for a pass or fail to be indicated in your tech profile/account with Field Solutions. </p>
	<p style="font-weight: bold">How many years are searched in a background check?</p>
	<p>That varies based upon the information available from each jurisdiction and the level of background check.</p>
	<p style="font-weight: bold">What is being searched for in a Basic Background Check?</p>
	<p>The Basic Background Check searches for Criminal Misdemeanor and Felony convictions.</p>
	<p style="font-weight: bold">What geographical region(s) are Basic Background Checks conducted in?</p>
	<p>In your county and state of residence.</p>
	<p style="font-weight: bold">What happens if I fail a background check?</p>
	<p>You will not be eligible to run work orders for clients requiring a background check for those specified work orders.</p>
	<p style="font-weight: bold">What constitutes failing a Basic Background Check and how will I know if I have &ldquo;failed&rdquo;?</p>
	<p>Felonies, Criminal Misdemeanor convictions (last 7 years) and/or more than one DUI/DWI will cause you to fail a Basic Background Check. If you fail a background check, it will be noted in your profile.  Clients will only see an indicator that you have not submitted for a background check. </p>
	<p style="font-weight: bold">What if the results of my background check are not accurate? How are disputes handled regarding accuracy?</p>
	<p>You have the right to dispute the results of your background check. Disputes are handled directly with the background check processor. This will initiate a reinvestigation. The reinvestigation process is governed by the FCRA. The established timeframe to complete a reinvestigation is generally within 30 days of receiving the inquiry from the applicant.  </p>
	<p style="font-weight: bold">How does the reinvestigation process work?</p>
	<p>Reinvestigation is the sole responsibility of and there is no charge to the client or
applicant for reinvestigation. This process is followed per the directives of the Fair Credit Reporting Act (FCRA). </p>
	<p>If the applicant disputes a reported finding, the applicant will be directed to the processor to initiate the reinvestigation process. The applicant must leave a message including their name, daytime phone number, and specific report finding under dispute. It is required that the applicant leave a message, as it is included in the legal documentation of the applicant's file. </p>
    <p>The processor will contact the applicant within 24 hours to discuss the dispute.  The company will also notify the client that the applicant has requested a reinvestigation. Upon completing the reinvestigation, the processor will send a Letter of Explanation to the applicant that outlines the findings of the reinvestigation; and if the report was amended, a copy will accompany the Letter of Explanation.</p>
	<p style="font-weight: bold">Will I be reimbursed for the background check if/when I pass?</p>
	<p>No. Field Solutions does not reimburse technicians for background checks. You should consider the fee for a background check an investment in your business of being a self-employed, independent contractor. You may want to look as a possible tax deduction. </p>
	<p style="font-weight: bold">Will I be reimbursed for the background check if I fail?</p>
	<p>No. Field Solutions does not reimburse technicians for background checks. You should consider the fee for a background check an investment in your business of being a self-employed, independent contractor.</p></td>
  </tr>
</table>
	
	


	


<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
