<?php $page = login; ?>
<?php $option = tech; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->

<br>

<table cellspacing=0 cellpadding=0 width=525 border=0>
	<tr>
<td>

<BR><CENTER><FONT face=Arial size=5><B>Field Solutions Model</B></FONT></CENTER>
<P>
<FONT face=Arial size=2>
The company and people behind Field Solutions have spent years modifying and perfecting a system designed to help field service Technicians line up continuous work/projects by forming relationships with companies (clients) seeking Techs on a regular basis.  While the concept (illustrated below) is quite simple, you should not under estimate effectiveness. As Field Solutions consistently works to form relationships with companies (clients) across the U.S. and portions of Canada, there will be more and more work (opportunities) available via Field Solutions. 
</FONT>
</P>

<P>
<CENTER><IMG SRC="images/ttb-model.jpg" WIDTH=412 HEIGHT=338 BORDER=0  ALT=""></CENTER>
</P>

<P>
<FONT face=Arial size=3><B>Two different ways to find work via Field Solutions</B></FONT>
</P>

<p>
<FONT face=Arial size=2>
<b>1)</b> Techs are encouraged to proactively view, apply for and/or bid on work orders. To do so, login to your Field Solutions account/profile and follow the instructions on your Tech Dashboard for searching, applying for and/or bidding on work orders.</FONT></p> 
<p>
<FONT face=Arial size=2>
Please note: It is common for work orders to be entered many days/weeks out, which means it may take a while before clients/staff get to your work order application(s) and/or bid(s), so please be patient. Keep in mind that there may be many Techs applying for and/or bidding on the same work orders. If no one contacts you regarding your applications/bids, you should assume that the work orders were awarded to another Tech. To help increase your chances of being contacted regarding your applications/bids, you should apply for and/or bid on as many work orders that you have the skill sets for and do so as far in advance as possible. Do not be concerned about applying for and/or bidding on work orders that fall over the same time period. If/when the client/staff contacts you, they will work out any schedule conflicts at that time. 
</FONT>
</p>
<p>
<FONT face=Arial size=2>
<b>2)</b> If/when your services are needed, Field Solutions clients will contact you directly, usually via email after accessing your secure profile located in our database as a result of you registering with Field Solutions.
</FONT>
</p>


<P>
<FONT face=Arial size=2>
There is no downside to being registered with Field Solutions. As long as you possess technical installation experience and/or the appropriate skills, you are encouraged to register no matter what your situation/availability is. You can always turn down work if/when a client contacts you.
</FONT>
</P>

</td>
	</tr>
</table>

	


<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
