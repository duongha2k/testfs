<?php $page = "techs"; ?>
<?php $option = "training"; ?>
<?php $selected = "fs" ?>
<?php $display = "fs" ?>
<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>
<script type="text/javascript" src="https://www.fieldsolutions.com/library/js/TJK_ToggleDL/TJK_ToggleDL.js"></script>
<link rel=stylesheet type ="text/css" href="https://www.fieldsolutions.com/library/js/TJK_ToggleDL/TJK_ToggleDL.css" />

<script type="text/javascript">
$(document).ready(function() {
TJK_ToggleDL(); // Controls the FAQ expand/close
});

</script>
<!-- Add Content Here -->

<br /><br />



<div id="wrap" style="margin:10px;">

<div id="leftSide" style="float:left; width:60%;">
<h1>Frequently Asked Questions</h1>


<?PHP include '_contents.php'; ?>


<p>
<br />
<h2>Background  Checks </h2>
<br />
</p>

<dl id="TJK_DL">
<dt>Q:  What is the background check for and where can I get full details?</dt>
<dd>A: Some clients will have their  own requirements for technicians that must be completed in addition  to FieldSolutions requirements. The requirements are often a drug  test and /or a background check. This is required so that the client  will know you are trustworthy. There is a fee for the background  check, this fee is $19.
<p>Please refer to the following link  for full details including FAQs: </strong> </p>
<p><a href="/techs/background_FAQ.php">https://www.FieldSolutions.com/techs/background_FAQ.php</a>
</p>
</dd>



</dl>

<br /><br /><br />
</div>
<div id="rightSide" style="float:right; width:40%;">

<h1><b>Still need help?</b> Please email us and we will answer your questions ASAP.</h1>
<?PHP include '_contactUs.php'; ?>
</div>


<!--- End Content --->
<?php require ("../../footer.php"); ?>
		
