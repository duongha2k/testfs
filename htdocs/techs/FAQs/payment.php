<?php $page = "techs"; ?>
<?php $option = "training"; ?>
<?php $selected = "fs" ?>
<?php $display = "fs" ?>
<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>
<script type="text/javascript" src="https://www.fieldsolutions.com/library/js/TJK_ToggleDL/TJK_ToggleDL.js"></script>
<link rel=stylesheet type ="text/css" href="https://www.fieldsolutions.com/library/js/TJK_ToggleDL/TJK_ToggleDL.css" />

<script type="text/javascript">
$(document).ready(function() {
TJK_ToggleDL(); // Controls the FAQ expand/close
});

</script>
<!-- Add Content Here -->

<br /><br />



<div id="wrap" style="margin:10px;">

<div id="leftSide" style="float:left; width:60%;">
<h1>Frequently Asked Questions</h1>


<?PHP include '_contents.php'; ?>

<p>
<br />
<h2>Payment  Policy and Schedule </h2>
<br />
</p>

<dl id="TJK_DL">
<dt>Q:  When will I be paid for work orders complete?</dt>
<dd>A:  You will be paid on work orders completed when the client has  approved your work order and verified that the work was completed  satisfactorily. The method of payment you select will determine the  amount of time it takes to pay you.
<p>Please  refer to the payment schedule &amp; policy via the following link in  the "Technician Support" section: <a href="https://www.FieldSolutions.com/techs/Training/FS_PaymentPolicy.php">https://www.FieldSolutions.com/techs/Training/FS_PaymentPolicy.php</a>
</p>
</dd>

<dt>Q:  What payment methods are available?</dt>
<dd>A:  FieldSolutions pays you for properly completed and approved work  orders via our online system. You have the option of having a  check sent via standard US Mail or having your payment Direct  Deposited. Please visit the link below for further details: <a href="https://www.FieldSolutions.com/techs/Training/FS_PaymentPolicy.php">https://www.FieldSolutions.com/techs/Training/FS_PaymentPolicy.php</a></dd>

<dt>Q:  I am awaiting a payment for work orders completed; I think the  payment is late, what can I do?</dt>
<dd>A: It is  important that you properly complete your work order(s) online and  have them approved by the client. FieldSolutions cannot pay on a work  order that has not been approved by the client. If you have questions  about this, please refer to the "Help" section regarding  completing your work orders online and also the payment policy &amp;  schedule here: <a href="https://www.FieldSolutions.com/techs/Training/FS_PaymentPolicy.php">https://www.FieldSolutions.com/techs/Training/FS_PaymentPolicy.php</a>
<p>If  you are not able to find the answer to your question, please email <a href="mailto:support@FieldSolutions.com">support@FieldSolutions.com</a> and we are happy to assist you!</p></dd>

<dt>Q:  What if there is a dispute with the amount I was paid for job? </dt>
<dd>A:  While FieldSolutions pays you for your client approved work orders  online, we can only pay the amount the client approved the work order  for. If there is a discrepancy in pay, you will need to speak with  the client you serviced to work out the discrepancy. Many questions  surrounding pay can be answered by going to the "Help" section to  view our payment policy. <a href="https://www.FieldSolutions.com/techs/Training/FS_PaymentPolicy.php">https://www.FieldSolutions.com/techs/Training/FS_PaymentPolicy.php</a>
</dd>

<dt>Q:  Does FieldSolutions offer "Direct Deposit"? If so, how do I sign  up for it? </dt>
<dd>A:  Yes, FieldSolutions offers Direct Deposit, which means you can  request to have payment for work performed, deposited directly into  your checking or savings account instead of having a paper check sent  via standard US Mail. Not only does Direct Deposit mean that you have  your money instantly, rather than waiting for a paper check to  arrive, but we pay Direct Deposits 3 days prior to mailing paper  checks which means you get paid FASTER! For more information on  Direct Deposit, please refer to <a href="https://www.FieldSolutions.com/techs/Training/FS_DirectDeposit.php">https://www.FieldSolutions.com/techs/Training/FS_DirectDeposit.php</a></dd>



</dl>

<br /><br /><br />
</div>
<div id="rightSide" style="float:right; width:40%;">

<h1><b>Still need help?</b> Please email us and we will answer your questions ASAP.</h1>
<?PHP include '_contactUs.php'; ?>
</div>


<!--- End Content --->
<?php require ("../../footer.php"); ?>

