<?php $page = "techs"; ?>
<?php $option = "training"; ?>
<?php $selected = "fs" ?>
<?php $display = "fs" ?>
<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>
<script type="text/javascript" src="https://www.fieldsolutions.com/library/js/TJK_ToggleDL/TJK_ToggleDL.js"></script>
<link rel=stylesheet type ="text/css" href="https://www.fieldsolutions.com/library/js/TJK_ToggleDL/TJK_ToggleDL.css" />

<script type="text/javascript">
$(document).ready(function() {
TJK_ToggleDL(); // Controls the FAQ expand/close
});

</script>
<!-- Add Content Here -->

<br /><br />



<div id="wrap" style="margin:10px;">

<div id="leftSide" style="float:left; width:60%;">
<h1>Frequently Asked Questions</h1>


<?PHP include '_contents.php'; ?>

<p>
<br />
<h2>Applying  for and Running open work orders</h2>
<br />
</p>


<dl id="TJK_DL">
<dt>Q:  Where do I go to bid on work orders?</dt>
<dd>A:  You can view open work orders via the "Work Orders" link in the  top blue menu after logging into your FieldSolutions technician  account. You may run a search based on your area to find work orders  with in distance to your zip code. When you find a work order you are  qualified for and interested in, please place a bid, you will be  notified if you are awarded the bid and notified if you do not get  awarded the work. Either way, we won’t leave you guessing.
<p><strong>Tip:  our online tutorial has a great video that details the work order  bidding process.</strong></p>
</dd>

<dt>Q:  Why am I not being selected for work orders I am bidding?</dt>
<dd>A:  We are very glad that you are interested in FieldSolutions and want  you to know we do want to put you to work. Depending on the area  there may be multiple technicians bidding on the same work order. We  do encourage you to check back often and continually bid. Getting  your name and skills in front of the clients is a step in the right  direction to being awarded a work order. </p>
<p><strong>Tip:  If you are looking to get your foot in the door at a client, it may  be a good idea to under bid a work order so that the client can get  to know you and try you out for the first time. </strong> </p>
</dd>

<dt>Q:  Work orders that I have applied are no longer in my applied work  orders section, why is that?</dt>
<dd>A:  As work orders that you have applied for are assigned to technicians,  they will be taken out of your applied work order section. </dd>

<dt>Q:  What things can I do as a technician to give myself a better chance  of getting work via FieldSolutions? </dt>
<dd>A: The most effective tool you have is your technician profile. Be sure  your profile has the most up to date information on your skills and  certifications. Be sure to take the FieldSolutions certification as  clients strongly prefer to work with technicians who have completed  this certification. While our team works very hard to keep our  technicians in the loop on work orders that are open, it is important  for you to stay proactive, keep an eye on open work orders. Take a  few minutes to view our work orders on a daily basis if possible. It  can also help if you are willing to work for less and/or travel more  at first (if needed) in order to get your "foot in the door" with  FieldSolutions clients. </dd>

<dt>Q:  Why am I unable to find a specific work order?</dt>
<dd>A:  Staff may email a work opportunity to technicians and then later turn  off the ability for techs to view it (call it up via a search). This  happens for a variety of reasons but most frequently when another  technician has been identified for the work.</dd>

<dt>Q:  I am unable to call up any open work orders in my area, why is that?</dt>
<dd>A:  If no open work orders show up in your area it is possible there are  no open work orders in your area at this time. Be sure to use the  "Advanced Search" feature in the "Distance" field drop-down  and search by different criteria such as city, zip code radius, etc.  Also, be sure that you are searching both general open work orders  and FLS work orders ("FLS WOS").</dd>

<dt>Q:  What if I have questions about a work order before I apply, how do I  get those answered?</dt>
<dd>A:  Please feel free to bid for the work order, at the time a work order  is offered to you a resource coordinator will be able to speak with  you further about the project. </dd>

<dt>Q:  Do clients and/or FieldSolutions cover travel plus other expenses?</dt>
<dd>A:  FieldSolutions and our clients do not cover travel/other expenses.  However, there are times that you may be able to negotiate some  expenses being paid up front. If you have a lot of expenses you know  you will incur, such as having to drive 100 miles each way to the  client, please adjust your bid to account for this and put a note in  the comments section explaining what your bid is comprised of.
</dd>

<dt>Q:  Are there other ways of getting work via FieldSolutions besides  applying for and/or bidding on work orders?</dt>
<dd>A:  FieldSolutions staff and/or clients may contact you via your profile,  often times via email regarding upcoming work opportunities.</dd>

<dt>Q:  How will I receive notice that I have been accepted for a work order  I applied for?</dt>
<dd>A:  You will receive an email asking you to confirm your acceptance of a  work order assigned to you. This email will also have instructions  for you detailing the specifics of the work order. Print this  information out and save it. You will be able to see all work orders  assigned in your technician profile by selecting "work orders"  and navigating to "work orders assigned".</dd>

<dt>Q:  Who should I contact if/when I cannot make it to a job that I have  confirmed?  </dt>
<dd>A:  It is very important that you make every effort to be on time to the  work order you have been assigned to. We certainly understand that  there are unforeseeable circumstances that will prevent you from  being at a site to complete your work. If you are unable to make it,  you should contact the person that set you up with the work order. If  you have no contacts, please send an email to <a href="mailto:support@FieldSolutions.com">support@FieldSolutions.com</a>.  It is very important that when you miss a work order it does not  become habitual as techs are given a score on work orders completed and no shows.
</dd>

<dt>Q:  I completed my work order(s) online per instructions, why are they  not approved? </dt>
<dd>A: First, please make sure that you have checked the work order as complete and have followed all instructions correctly. Check to see if the client has included any notes in your work order informing you  of something they need prior to being able to approve your work  order. You may have done something incorrectly or left something out. If you cannot see any notes and do not understand why it is not being  approved, please contact the client (the company you performed the  work for). If you are still not able to get resolution please contact  support@FieldSolutions.com </dd>

<dt>Q: How long will it typically take for a FieldSolutions client to approve my work order online once I have marked it as complete per instructions?  </dt>
<dd>A:  Typically, once you have marked your work order as complete the  client should approve your work order within 1-2 business days. If your work order is not approved within 1-2 business days of you marking it complete online, check the work order to see if the client  has included any notes informing you of something they need prior to  the work order being approved. If a work order is not approved and  you completed it as requested, contact the client directly to obtain  approval of the work order. As a reminder, FieldSolutions cannot pay  for work orders that are not completed properly and approved by the  client.</dd>

<dt>Q:  I completed my work order(s) online per instructions, why are they  not getting paid?</dt>
<dd>A:  Check to see if the client has approved the work order.  FieldSolutions cannot pay on a work order until it the client has  approved it. If the work order is approved and you have not received  payment please refer to the payment policy &amp; schedule for more  detail: <a href="https://www.FieldSolutions.com/techs/Training/FS_PaymentPolicy.php">https://www.FieldSolutions.com/techs/Training/FS_PaymentPolicy.php</a></dd>

<dt>Q:  Who do I contact to get my work order approved and paid if I am  having trouble contacting the client?</dt>
<dd>A: Whenever in doubt, please contact </strong><a href="mailto:support@FieldSolutions.com">support@FieldSolutions.com</a> and a staff member will get back to you ASAP.</dd>


</dl>

<br /><br /><br />
</div>

<h1><b>Still need help?</b> Please email us and we will answer your questions ASAP.</h1>
<?PHP include '_contactUs.php'; ?>
</div>


<!--- End Content --->
<?php require ("../../footer.php"); ?>

