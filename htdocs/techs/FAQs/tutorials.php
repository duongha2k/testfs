<?php $page = "techs"; ?>
<?php $option = "training"; ?>
<?php $selected = "fs" ?>
<?php $display = "fs" ?>
<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>
<script type="text/javascript" src="https://www.fieldsolutions.com/library/js/TJK_ToggleDL/TJK_ToggleDL.js"></script>
<link rel=stylesheet type ="text/css" href="https://www.fieldsolutions.com/library/js/TJK_ToggleDL/TJK_ToggleDL.css" />

<script type="text/javascript">
$(document).ready(function() {
TJK_ToggleDL(); // Controls the FAQ expand/close
});

</script>
<!-- Add Content Here -->

<br /><br />



<div id="wrap" style="margin:10px;">

<div id="leftSide" style="float:left; width:60%;">
<h1>Frequently Asked Questions</h1>


<?PHP include '_contents.php'; ?>


<p>
<br />
<h2>FieldSolutions  Tutorials</h2>
<br />
</p>

<dl id="TJK_DL">

<dt>Q:  Is there a FieldSolutions tutorial online anywhere? If so, where? </dt>
<dd>
A:  We are pleased to offer an easy to use, comprehensive on line  tutorial. These tutorials will walk you through the process of  registration, bidding, getting paid and our policies and procedures  for technicians. Technicians are strongly encouraged to view these  tutorials in their entirety, it only takes about 27 minutes, and  become "FieldSolutions Certified". The video tutorials are  accessible via the "FieldSolutions Certified" link on the home  page of your tech account (profile).

<p>Please  visit this link for more information:</p>
<p><a href="www.fieldsolutions.com/techs/Training/FieldSolutions/FieldSolutionsTraining">www.fieldsolutions.com/techs/Training/FieldSolutions/FieldSolutionsTraining</a>
</p>

</dd>


</dl>

<br /><br /><br />
</div>
<div id="rightSide" style="float:right; width:40%;">

<h1><b>Still need help?</b> Please email us and we will answer your questions ASAP.</h1>
<?PHP include '_contactUs.php'; ?>
</div>


<!--- End Content --->
<?php require ("../../footer.php"); ?>
		
