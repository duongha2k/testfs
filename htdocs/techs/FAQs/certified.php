<?php $page = "techs"; ?>
<?php $option = "training"; ?>
<?php $selected = "fs" ?>
<?php $display = "fs" ?>
<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>
<script type="text/javascript" src="https://www.fieldsolutions.com/library/js/TJK_ToggleDL/TJK_ToggleDL.js"></script>
<link rel=stylesheet type ="text/css" href="https://www.fieldsolutions.com/library/js/TJK_ToggleDL/TJK_ToggleDL.css" />

<script type="text/javascript">
$(document).ready(function() {
TJK_ToggleDL(); // Controls the FAQ expand/close
});

</script>
<!-- Add Content Here -->

<br /><br />



<div id="wrap" style="margin:10px;">

<div id="leftSide" style="float:left; width:60%;">
<h1>Frequently Asked Questions</h1>


<?PHP include '_contents.php'; ?>

<p>
<br />
<h2>FieldSolutions  Certified</h2>
<br />
</p>

<dl id="TJK_DL">
<dt>Q:  What is required of techs to get "FieldSolutions Certified" and  what is it exactly?</dt>
<dd>A:  In order to get "FieldSolutions Certified", you will need to  watch the online video tutorials and also review the "Help"  section (if needed). The video tutorials are accessible via the  "FieldSolutions Certified" link on the home page of your tech  account (profile). Getting "FieldSolutions Certified" is a way to  let clients of FieldSolutions  know that you understand how FieldSolutions works and are prepared to  run work orders. Once you have watched the video tutorials, you will  need to submit the FieldSolutions Certification review at the bottom  of the video tutorial page.</dd>

</dl>

<br /><br /><br />
</div>
<div id="rightSide" style="float:right; width:40%;">

<h1><b>Still need help?</b> Please email us and we will answer your questions ASAP.</h1>
<?PHP include '_contactUs.php'; ?>
</div>


<!--- End Content --->
<?php require ("../../footer.php"); ?>

