<?php $page = "techs"; ?>
<?php $option = "training"; ?>
<?php $selected = "fs" ?>
<?php $display = "fs" ?>
<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>
<script type="text/javascript" src="https://www.fieldsolutions.com/library/js/TJK_ToggleDL/TJK_ToggleDL.js"></script>
<link rel=stylesheet type ="text/css" href="https://www.fieldsolutions.com/library/js/TJK_ToggleDL/TJK_ToggleDL.css" />

<script type="text/javascript">
$(document).ready(function() {
TJK_ToggleDL(); // Controls the FAQ expand/close
});

</script>
<!-- Add Content Here -->

<br /><br />



<div id="wrap" style="margin:10px;">

<div id="leftSide" style="float:left; width:60%;">
<h1>Frequently Asked Questions</h1>


<?PHP include '_contents.php'; ?>

<p>
<br />
<h2>Technician  Forums</h2>
<br />
</p>

<dl id="TJK_DL">
<dt>Q:  What are the Forums and how do I access them?</dt>
<dd>A:  The Forums are a place for technicians to post questions to staff and  other techs, express their concerns, etc. Access the Forums here: <a href="http://groups.google.com/group/mytechnicianspace">http://groups.google.com/group/mytechnicianspace</a></dd>



</dl>

<br /><br /><br />
</div>
<div id="rightSide" style="float:right; width:40%;">

<h1><b>Still need help?</b> Please email us and we will answer your questions ASAP.</h1>
<?PHP include '_contactUs.php'; ?>
</div>


<!--- End Content --->
<?php require ("../../footer.php"); ?>

