<?php $page = "techs"; ?>
<?php $option = "training"; ?>
<?php $selected = "fs" ?>
<?php $display = "fs" ?>
<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>
<script type="text/javascript" src="https://www.fieldsolutions.com/library/js/TJK_ToggleDL/TJK_ToggleDL.js"></script>
<link rel=stylesheet type ="text/css" href="https://www.fieldsolutions.com/library/js/TJK_ToggleDL/TJK_ToggleDL.css" />

<script type="text/javascript">
$(document).ready(function() {
TJK_ToggleDL(); // Controls the FAQ expand/close
});

</script>
<!-- Add Content Here -->

<br /><br />



<div id="wrap" style="margin:10px;">

<div id="leftSide" style="float:left; width:60%;">
<h1>Frequently Asked Questions</h1>


<?PHP include '_contents.php'; ?>

<p>
<br />
<h2>Technician  Registration / Account Login &amp; Maintenance</h2>
<br />
</p>

<dl id="TJK_DL">


<dt>Q:  Is there a fee to register as a technician with FieldSolutions?</dt>
<dd>A:  No, technicians do not pay a fee to register with FieldSolutions. We  do not take a commission from you either, the bill rate you bid and  are accepted at is the bill rate you will be paid.</dd>

<dt>Q:  Can I register for multiple technician accounts?</dt>
<dd>A:  No, please do not register for multiple accounts. It is only  necessary to have one profile as you are able to fill in information  about your complete skills set and experience. We encourage you to  complete your technician profile with as much information as possible  to provide a complete and accurate picture of your experience and  skills.</strong></dd>

<dt>Q: Where should I submit my resume?</dt>
<dd>A:  You are able to upload your resume in your FieldSolutions technician  profile and are encouraged to do so. Please do not email or fax your  resume to FieldSolutions.</dd>

<dt>Q:  Can/should I register as a technician with FieldSolutions if I live  outside the United States or Canada?</dt>
<dd>A:  If you do not plan on living in the US or Canada, please do not  register as most of our work is currently in the US, Puerto Rico &amp;  Canada. </dd>

<dt>Q:  How do I go about registering as a technician/company with  FieldSolutions with my own technicians working under my name/company  that administer payment to me?</dt>
<dd>A: You  should instruct  each of your technicians&nbsp;to register as a technician with  FieldSolutions as they will need their own FieldSolutions tech ID for  updating their work orders online, etc.&nbsp;If you want to pay the  technicians yourself&nbsp;for work&nbsp;performed via FieldSolutions,  they must list&nbsp;your company information or name on their W-9  forms they submit to FieldSolutions in order for&nbsp;you&nbsp;to  receive payment&nbsp;on ALL work&nbsp;performed by your technicians  via FieldSolutions.</dd>

<dt>Q:  Where do I login as a registered technician with FieldSolutions?</dt>
<dd>A:  You log in via the FieldSolutions Home page: <a href="http://www.FieldSolutions.com/">http://www.FieldSolutions.com</a></dd>

<dt>Q:  I am having trouble logging in, where can I get help or recover my  password/ ID?</dt>
<dd>
A:  If you are having trouble logging in to your technician account with  FieldSolutions, please visit the following web page to retrieve your  login: <a href="http://www.FieldSolutions.com/misc/lostPassword.php">http://www.FieldSolutions.com/misc/lostPassword.php</a>
<p>
If  you are unable to successfully retrieve your login via the web page  above, please email support at: <a href="mailto:support@FieldSolutions.com">support@FieldSolutions.com</a>
</p></dd>

<dt>Q:  How do I update/modify my tech profile including my email address and  other contact info?  </dt>
<dd>A:  To update your technician profile, simply login to your  FieldSolutions account, click on the "My Profile" link in the  top-left, modify your information and be sure to click on the  "Update" button on the bottom of the page. Note: You are not able  to modify your name. If you entered your name incorrectly upon  registration, please send an email to: <a href="mailto:support@FieldSolutions.com">support@FieldSolutions.com</a> and staff will be happy to modify that for you. Please note; you may be asked to verify your identity prior to any name changes being  made.</dd>

<dt>Q:  Where can I modify my technician profile with additional experience,  certifications, etc? </dt>
<dd>A:  To modify your profile, simply log in to your profile at
<a href="http://www.FieldSolutions.com/">www.FieldSolutions.com</a>.  The blue menu across the top of the screen offers links to the  various profile sections. Be sure to click "Update" before  leaving a page or the information will not be saved.</dd>

<dt>Q: What does FieldSolutions  require of technicians prior to them working for your different  clients?  </dt>
<dd>A:

<ol>
<li>Register with  	FieldSolutions- it is fast and easy!</li>
<li>Agree to the  	terms of our technician agreement.</li>
<li>Submit your W-9, you do not have to do this prior to working but you must  	complete this step before you can be paid for work done. Refer to  	the following link for more info: <a href="https://www.FieldSolutions.com/techs/w9.php">https://www.FieldSolutions.com/techs/w9.php</a></li>
</ol>

<p><strong>Note: In addition to  any Field Solution requirements, keep in mind that individual clients  may also have specific requirements</strong></p>
</dd>

<dt>Q: What requirements do clients of  FieldSolutions have?</dt>
<dd>A:  Each client of FieldSolutions has their own set of requirements. Some  clients require a background and/ or drug test to be conducted; you  will be notified if this is a requirement for a work order. Please  note, our clients strongly prefer FieldSolutions certified  technicians, this certification tells our clients you understand the  specific requirements and process to check in, check out, payment,  tools and more. This certification is very beneficial to you so you  understand the process of working with FieldSolutions as well as  letting our clients know you understand how to run a job with us.</dd>


</dl>

<br /><br /><br />
</div>
<div id="rightSide" style="float:right; width:40%;">

<h1><b>Still need help?</b> Please email us and we will answer your questions ASAP.</h1>
<?PHP include '_contactUs.php'; ?>
</div>


<!--- End Content --->
<?php require ("../../footer.php"); ?>

