<?php $page = "techs"; ?>
<?php $option = "training"; ?>
<?php $selected = "support"; ?>
<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>
<script type="text/javascript" src="https://www.fieldsolutions.com/library/js/TJK_ToggleDL/TJK_ToggleDL.js"></script>
<link rel=stylesheet type ="text/css" href="https://www.fieldsolutions.com/library/js/TJK_ToggleDL/TJK_ToggleDL.css" />

<script type="text/javascript">
$(document).ready(function() {
TJK_ToggleDL(); // Controls the FAQ expand/close
});

</script>
<!-- Add Content Here -->

<br /><br />



<div id="wrap" style="margin:10px;">

<div id="leftSide" style="float:left; width:60%;">
<h1>Frequently Asked Questions</h1>


<?PHP include '_contents.php'; ?>
<p>
<br />
<h2>General Information</h2>
<br />
</p>

<dl id="TJK_DL">
<dt>Q:  What is FieldSolutions and how does it work?</dt>
<dd><p>A:  FieldSolutions is a provider of field technicians to our client  companies. FieldSolutions operates in all 50 U.S. states, Canada and  Puerto Rico.</p>

<p>FieldSolutions  resource for our clients to locate and procure field technicians for  a variety of projects. Our clients register with <a href="http://www.FieldSolutions.com/">www.FieldSolutions.com</a> to publish work orders and select the technicians they would like for  the project.  </p>
</dd>

<dt>Q:  What is your privacy practice regarding the information I submit to  FieldSolutions?</dt>
<dd>A:  FieldSolutions is committed to maintaining your privacy, we make  every effort to ensure that your confidential information is secure  and private. <a href="http://www.FieldSolutions.com/">www.FieldSolutions.com</a> is a secure website. Please refer to the following link for more  information on our privacy policy: <a href="https://www.FieldSolutions.com/content/Privacy_Policy.pdf">https://www.FieldSolutions.com/content/Privacy_Policy.pdf</a></dd>

<dt>Q:  How can I get my questions answered as they relate to use of the  FieldSolutions website, how to apply for work orders, payment, etc?</dt>
<dd>At  FieldSolutions, we are committed to ensuring that you have a first  rate experience with us. Our commitment to quality means that we have  created several channels for you to get your questions answered in a  timely manner.
<p>
<ol>
  <li>
    <p>Thoroughly cover the "Help" section- this is available at <a href="http://www.FieldSolutions.com/">www.FieldSolutions.com</a> , you do not have to be logged in to access the "Help" section.</p>
  </li>
  <li>
    <p>Read our FAQ document</p>
  </li>
  <li>
    <p>Review the FieldSolutions  	video tutorial or join us on a Q &amp; A call/web-cast</p>
  </li>
  <li>
    <p>Email your questions to<a href="mailto:support@FieldSolutions.com">support@FieldSolutions.com</a></p>
  </li>
</ol>
</p>

</dd>

<dt>Q:  Where can I get a bit more information regarding current technician  opportunities with FieldSolutions?  </dt>
<dd>A: Once you have registered your  technician profile you will be able to view current work orders and  bid on projects. Comprehensive sign up instructions can be found on  our website: </strong><a href="https://www.FieldSolutions.com/techSignup.php"><strong>https://www.FieldSolutions.com/techSignup.php</strong></a></dd>



</dl>

<br /><br /><br />
</div>
<div id="rightSide" style="float:right; width:40%;">


<h1><b>Still need help?</b> Please email us and we will answer your questions ASAP.</h1>
<?PHP include '_contactUs.php'; ?>

</div>


<!--- End Content --->
<?php require ("../../footer.php"); ?>
		
