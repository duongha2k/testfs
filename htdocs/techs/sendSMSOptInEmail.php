<?php
	require_once("../headerStartSession.php");
	$techID = $_SESSION['TechID'];
	$send = $_GET["send"];
	$old = $_GET["old"];
	$fromProfile = isset($_GET["TechID"]);
	$redirect = ($fromProfile ? "updateTechInfo.php?TechID={$_GET["TechID"]}" : "dashboard.php");
	if (!$fromProfile)
		$old = "No";
		
	if (($send != "Yes" && $send != "Y") || $old == "Yes") {
		header("Location: $redirect");
//		echo "$redirect";
		die();
	}
//	ini_set("display_errors",1);
	require_once("../library/caspioAPI.php");
	require_once("../library/smtpMail.php");
	require_once("../includes/SMS_Send.php");
	
	$emailList = getSMSEmailFromTechID(array($techID));
	$message = "Welcome to FS Text Messaging: You will be receiving new work announcements via texting soon!";
	$htmlmessage = nl2br($message);
	
//	print_r($emailList);
	
	if (sizeof($emailList) > 0) {
		$SendTo = $emailList[0];
		if ($SendTo != "")
//			echo "$SendTo";
			smtpMail("Field Solutions", "nobody@fieldsolutions.com", $SendTo, "Field Solutions SMS", $message, $htmlmessage, "SMS_Test");
	}
//	echo "$redirect";
	header("Location: $redirect");
?>