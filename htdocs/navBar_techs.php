<link rel="stylesheet" href="/css/simpleDropdown/style.css" type="text/css" media="screen"/>
<!--[if lte IE 7]>
	<link rel="stylesheet" type="text/css" href="css/simpleDropdown/ie.css" media="screen" />
<![endif]-->
<script type="text/javascript" src="/library/jquery/jquery.dropdownPlain.js"></script>
<?php script_nocache ("/widgets/js/global.js"); ?>
<script type="text/javascript">
	function openMapping(url) {
        openPopupWin(url,null,{width:1200,height:650});
	}
	function openCreateWO(url) {
        openPopupWin(url);
	}	
</script>

<?php
$techIsDeactivated = false;
if ($_SESSION["loggedInAs"] == 'tech') {
	$authData = array('login'=>$_SESSION["UserName"], 'password'=>$_SESSION["UserPassword"]);

	$user = new Core_Api_TechUser();
	$user->checkAuthentication($authData);
	if ($user->getDeactivated() == 1 && !empty($option) && $option != 'pay_stubs' && $option != 'tech' && $option != 'dashboard' && ($option != 'wos' || $selected == 'wosViewApplied') && $option != 'details') {
?>
<script type="text/javascript">
		document.location = "/techs/dashboard.php";
</script>
<?php
	}
}
/*
<script type="text/javascript" src="/widgets/js/FSWidgetAppliedWork.js"></script>
<script type="text/javascript">
    var widgetAppliedWork = new FSWidgetAppliedWork();
</script>
*/ ?>

<!-- TOP NAVBAR -->
<div class="maintab">
<ul class="dropdown">


<?php if ( $page == "techs" || $page == "contactus") :?>
<!-- TECH NAVBAR TOP -->
<li><a href="/techs/">Home</a></li>
<li <?php echo ($option == 'dashboard') ? 'class="selected"' : ''; ?>><a href="/techs/dashboard.php" >Control Panel</a></li>
<li>
    <a href="/techs/wos.php?tab=techassigned">My Work Orders</a>
    <ul class="submenu" style="visibility: hidden;">

        <li><a href="/techs/wos.php?tab=techavailable&date_start=<?php echo date('n/j/Y',time()); ?>">Available Work</a></li>
		<li><a href="/techs/wosViewAppliedAll.php">Applied Work</a></li>
		<li><a href="/techs/wos.php?tab=techassigned">My Assigned Work</a></li>
		<li><a href="/techs/wos.php?tab=techincomplete">Incomplete</a></li>
		<li><a href="/techs/wos.php?tab=techworkdone">Work Done</a></li>
		<li><a href="/techs/wos.php?tab=techapproved">Approved</a></li>
        <li><a href="/techs/wos.php?tab=techpaid">Paid Work Orders</a></li>
        <li><a href="/techs/wos.php?tab=techall">All/Archive</a></li>

            <?php /*

        <li><a onclick="tpm.openPopupWin('availableWO');" href="javascript:void(0)">Available Work</a></li>
		<li><a  onclick="tpm.openPopupWin('appliedWO');" href="javascript:void(0)">Applied Work</a></li>
        / * <li><a  onclick="widgetAppliedWork.show();" href="javascript:void(0)">Applied Work</a></li> * /
		<li><a onclick="tpm.openPopupWin('assignedWO');" href="javascript:void(0)">My Assigned Work</a></li>
		<li><a onclick="tpm.openPopupWin('incompleteWO');" href="javascript:void(0)">Incomplete</a></li>
		<li><a onclick="tpm.openPopupWin('workdoneWO');" href="javascript:void(0)">Work Done</a></li>
		<li><a onclick="tpm.openPopupWin('approvedWO');" href="javascript:void(0)">Approved</a></li>
        <li><a onclick="tpm.openPopupWin('paidWO');" href="javascript:void(0)">Paid Work Orders</a></li>
        <li><a onclick="tpm.openPopupWin('allWO');" href="javascript:void(0)">All/Archive</a></li>
         */
        ?>

<!-- 
		<li><a  onclick="tpm.openPopupWin('availableWO');" href="javascript:void(0)">Available Work</a></li>
		<li><a  onclick="tpm.openPopupWin('appliedWO');" href="javascript:void(0)">Applied Work</a></li>
        <?php /* <li><a  onclick="widgetAppliedWork.show();" href="javascript:void(0)">Applied Work</a></li> */ ?>
		<li><a onclick="tpm.openPopupWin('assignedWO');" href="javascript:void(0)">My Assigned Work</a></li>
		<li><a onclick="tpm.openPopupWin('incompleteWO');" href="javascript:void(0)">Incomplete</a></li>
		<li><a onclick="tpm.openPopupWin('workdoneWO');" href="javascript:void(0)">Work Done</a></li>
		<li><a onclick="tpm.openPopupWin('approvedWO');" href="javascript:void(0)">Approved</a></li>
        <li><a onclick="tpm.openPopupWin('paidWO');" href="javascript:void(0)">Paid Work Orders</a></li>
        <li><a onclick="tpm.openPopupWin('allWO');" href="javascript:void(0)">All/Archive</a></li>
-->

    </ul>
</li>
</ul>
<ul class="dropdown2">
<li class="fr"><a href="/logOut.php">Log Out</a></li>
<!---<li class="fr"><a href="javascript:void(0)">My Credentials</a>
    <ul class="submenu">
        <li><a  onclick="tpm.openPopupWin('myProfile');" href="javascript:void(0)">My Basic Profile</a></li>
        <li><a  onclick="tpm.openPopupWin('equipment');" href="javascript:void(0)">Equipment</a>
            <ul>
                <li><a onclick="tpm.openPopupWin('equipment');" href="javascript:void(0)">Basic Equipment</a></li>
                <li><a onclick="tpm.openPopupWin('cablingSkills');" href="javascript:void(0)">Cabling</a></li>
                <li><a onclick="tpm.openPopupWin('telephony');" href="javascript:void(0)">Telephony</a></li>
            </ul>
        </li>
        <li class=""><a onclick="tpm.openPopupWin('certifications');" href="javascript:void(0)">Certifications</a>
            <ul>
                <li><a onclick="tpm.openPopupWin('certifications');" href="javascript:void(0)">Technical Certifications</a></li>
            </ul>
        </li>
        <li class=""><a onclick="tpm.openPopupWin('skills');" href="javascript:void(0)">Skills</a></li>
        <li class=""><a onclick="tpm.openPopupWin('experience',{'queryString' :'?TechID=<?=$_SESSION['TechID']?>'});" href="javascript:void(0)">Experience</a></li>
        <li class=""><a onclick="tpm.openPopupWin('bg-checks');" href="javascript:void(0)">Background Checks</a></li>
        <li class=""><a onclick="tpm.openPopupWin('banking');" href="javascript:void(0)">Direct Deposit</a></li>
        <li class=""><a onclick="tpm.openPopupWin('FLS');" href="javascript:void(0)">FLS</a></li>
    </ul>
</li>--->
<?php endif;
    if ( !empty($__jsinit__) ) {
        echo $__jsinit__;
    }
    $toolButtons = ( !empty($toolButtons) && is_array($toolButtons) ) ? $toolButtons : array();
    // $toolButtons = array_unique(array_merge(array('findworkorder', 'paystatus', 'quickbid'), $toolButtons));
    $toolButtons = array_unique(array_merge(array('quickbid_tech', 'findavailablework_tech', 'findworkorder_tech'), $toolButtons));
    //$toolButtons = array_unique(array_merge(array('findavailablework_tech','findworkorder_tech'), $toolButtons));
?>
</ul>
</div>
<div id="tabcontent" class="clearfix">
	<?php include 'buttons.php'; ?>
</div>



<script type="text/javascript">
    /**
     *  calendarInit
     *
     *  Display filter post handler
     */
    function calendarInit() {
        $.map(['filterStartDate', 'filterEndDate','filterDateCreated'], function( id ) {
            $('#'+id).focus( function() {

                $(this).calendar({dateFormat:'MDY/'});

                $('#calendar_div').css('z-index', 1000);
            });
        })
    }
    /**
     * calendarMove
     *
     * @param event  event
     * @access public
     * @return void
     */

    function calendarMove( input ) {
        $('#calendar_div').css('top', $(input).offset().top + $(input).height() + 6 + 'px');
        $('#calendar_div').css('left', $(input).offset().left + 'px');
        popUpCal.showFor(input);
    }

    var quickBid;
    
    window._currentDate = '<?=date('n/j/Y')?>';
    $(document).ready(function(){


        
        $("#MyPayStatusBtn").click(function(event) {
            roll.autohide(false);
            var opt = {
                width       : 320,
                height      : '',
                title       : 'My Pay Status',
                handler		: statusWidget.prepare,
                body        : $(statusWidget.container()).html()
            };
            roll.showNotAjax(this, event, opt);
        });


        $("#FindWorkOrderBtn").click(function(event) {
            roll.autohide(false);
            var opt = {
                width       : 300,
                height      : '',
                title       : 'Find a Work Order',
                handler		: findWorkOrder.prepare,
                body        : $(findWorkOrder.container()).html()
            };
            roll.showNotAjax(this, event, opt);
        });

        if (td == null) {
        	td = new FSTechDashboard();
        }
        quickBid = new FSQuickBidTool(td, roll);

        $("#QuickBidBtn").click(function(event) {
        	
            if('<?=$_SESSION["acceptedICA"]?>' == 'No'){
                 var opt = {
     	                width       : 283,
     	                height      : '',
     	                title       : 'Quick Bid Error',
     	                body        : '<p>Please agree to the <a href="../content/techTerms_form.php" target="_parent">Independent Contractor Agreement</a> before applying or bidding for work.</p>'
     	            };
            }else{
	            
	            var opt = {
	                width       : 283,
	                height      : '',
	                title       : 'Quick Bid',
	                body        : quickBid.body()
	            };
	           
            }
            quickBid.button(this);
            roll.autohide(false);
            roll.showNotAjax(this,event,opt);
        });
        
        $("#FindAvailableWorkBtn").click(function(event){
            roll.autohide(false);
            var opt = {
                width       : 361,
                height      : 220,
                title       : 'Find Work',
                context     : aw,
                handler     : aw.prepareFilters,
                postHandler : calendarInit,
                body        : $(aw.filters().container()).html()
            };
            roll.showNotAjax(this, event, opt);
        });
    });

    function quickApplyCheck(el, woNum){
    	quickBid.button(el); 
		if('<?=$_SESSION["acceptedICA"]?>' == 'No'){
			var opt = {
 	                width       : 283,
 	                height      : '',
 	                title       : 'Quick Bid Error',
 	                body        : '<p>Please agree to the <a href="../content/techTerms_form.php" target="_parent">Independent Contractor Agreement</a> before applying or bidding for work.</p>'
 	            };
			roll.showNotAjax(el,null,opt);
		}else{
    		quickBid.searchWO(woNum,el);
		}
    }
function quickWithdraw(el, woNum){
    quickBid.button(el); 
    quickBid.WithdrawWO(woNum,el);
}
</script>
<?php
    $username = "";
    $lname = "";
    $fname = "";
    $password = "";
    $PasswordUpdateClass = new Core_Api_PasswordUpdateClass();
    $hasPasswordUpdate = 0;
    if($loggedIn == "yes"){
        if($_SESSION['loggedInAs'] == "tech"){
            $hasPasswordUpdate = $PasswordUpdateClass->MustChangePasswordorNot_Tech($_SESSION['TechID']);
        }
        if(empty($hasPasswordUpdate)){
            $hasPasswordUpdate = 0;
        }
        $WosClass = new Core_Api_WosClass();
        $TechInfo = $WosClass->getTechInfo($_SESSION['TechID']);
        $password = $TechInfo['Password'];
        $username = $TechInfo['UserName'];
        $lname = $TechInfo['LastName'];
        $fname = $TechInfo['FirstName'];
    }
?>
<script>
    var _global = global.CreateObject({
        id:"_global",hasPasswordUpdate:<?= $hasPasswordUpdate ?>,
        loggedInAs:'<?= $_SESSION['loggedInAs'] ?>',
        PHP_SELF:'<?= $_SERVER['HTTP_REFERER'] ?>',
        userid:'<?= $_SESSION['TechID'] ?>',
        password:'<?= $password ?>',
        username:'<?= $username ?>',
        lname:'<?= $lname ?>',
        fname:'<?= $fname ?>',
        company:'<?= empty($_REQUEST['v'])?$_SESSION['PM_Company_ID']:$_REQUEST['v'] ?>'
    });  
    jQuery(window).ready(function(){
        _global.callUpdatePassWordSecurity();
    });

</script> 
