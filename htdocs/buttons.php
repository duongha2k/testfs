<?php
$mytechs_script = script_nocache ("/widgets/js/FSMyTechs.js", true);
$findwo_script = script_nocache ("/widgets/js/FSFindWorkOrder.js", true);

if ($_SESSION['loggedInAs'] == 'tech'):
        $authData = array('login'=>$_SESSION["UserName"], 'password'=>$_SESSION["UserPassword"]);

        $user = new Core_Api_TechUser();
        $user->checkAuthentication($authData);
endif;

if ( !empty($toolButtons) ) :
	$isDash = !empty($_GET['isDash']);
	$hideBtn = $isDash ? "$('#FSIdeas').hide();" : "";
    $__buttons__ = array(
		'mytechs' => <<<EOTXT
            <div class="topToolBtn">
                <input type="button" id="MyTechsBtn" value="My Techs" class="link_button middle2_button MyTechsBtn-7y4422422" />
            </div>
			$mytechs_script
           <script>
            var myTechs; 		// 	My Techs Gadgets
            function keyUpmyTechs(event)
            {
                if (event.keyCode == 13)
                {
                    myTechs.doFind(); roll.hide();
                    return false;
                }
            }
			$(document).ready(function() {
				$hideBtn
                            myTechs = new FSMyTechs(wd, roll);
                          $("#MyTechsBtn").click(function(event) {
                                var el = this;
                                $.post("/widgets/dashboard/client/accessctl/", {
                                        rt:'iframe',
                                       attributeId:'45,48'
                                    }, 
                                   function (response) {
                                       if(response=="1") { 
                                            roll.autohide(false);
                                                var opt = {
                                                    width       : 325,
                                                    height      : '',
                                                    title       : 'My Techs',
                                                    handler     : myTechs.prepare,
                                                    context     : wd,
                                                    body        : $(myTechs.container()).html()
                                                };
                                                roll.showNotAjax(el, event, opt); 
                                                
                                        } 
                                        else {
                                            Attention(response);
											}
                    
                                       });                        
                                    });
                                    myTechs.buildHtml(); 
                                }); 

            </script>
EOTXT
,
        'findavailablework' => <<<EOTXT
            <div class="topToolBtn">
                <input type="button" id="FindAvailableWorkBtn" value="Find Work" class="link_button middle2_button" />
            </div>
EOTXT
,
        'findavailablework_tech' => <<<EOTXT
            <div class="topToolBtn">
                <input type="button" id="FindAvailableWorkBtn" value="Find Work" class="link_button middle2_button" />
            </div>
EOTXT
,
        'findworkorder' => <<<EOTXT
            <div class="topToolBtn">
                <input type="button" id="FindWorkOrderBtn" value="Find a Work Order" class="link_button middle2_button" />
            </div>
            $findwo_script
            <script>
            var findWorkOrder;  //  Find work order object
            function keyUpFindWorkOrder(event)
            {
                if (event.keyCode == 13)
                {
                    findWorkOrder.doFind();
                    return false;
                }
            }
            $(document).ready(function() {			
            	findWorkOrder = new FSFindWorkOrder(wd, roll);
        		$("#FindWorkOrderBtn").click(function(event) {
                        var el = this;
					$.post("/widgets/dashboard/client/accessctl/", 
                        {rt:'iframe',attributeId:'45,9'}, 
        function (response) {
            if(response=="1") { 
                 roll.autohide(false);
                    var opt = {
                        width       : 280,
                        height      : '',
                        title       : 'Find a Work Order',
                        handler		: findWorkOrder.prepare,
                        context     : wd,
                        body        : $(findWorkOrder.container()).html()
                    };
                    roll.showNotAjax(el, event, opt);  
            } 
            else {
                Attention(response);
            }                        
                    
        });
                    
                });
                findWorkOrder.buildHtml();
                
        	});
        	</script>
EOTXT
,
        'findworkorder_tech' => <<<EOTXT
            <div class="topToolBtn">
                <input type="button" id="FindWorkOrderBtn" value="Find a Work Order" class="link_button middle2_button" />
            </div>
EOTXT
,

        'createworkorder' => <<<EOTXT
            <div class="topToolBtn">
			    <input type="button" id="CreateWorkOrderBtn" value="Create a Work Order" class="link_button middle_button" />
            </div>
            <script>
            $(document).ready(function() {
            	$("#CreateWorkOrderBtn").click(function(event) {
                        $.post("/widgets/dashboard/client/accessctl/", {
                            rt:'iframe',
                            attributeId:'45,2'
                        }, 
                        function (response) {
                            if(response=="1") { 
                                document.location = '/clients/createWO.php?v={$_GET["v"]}'; 
                            } 
                            else {
                                Attention(response);
                            }                        
                        });

                });
        	});	
            </script>
EOTXT
,

        'quickassign' => <<<EOTXT
            <div class="topToolBtn">
                <input type="button" id="QuickAssignBtn" value="Quick Assign" class="link_button  middle2_button" />
            </div>
            <script>
            function keyUpQuickAssign(event)
            {
                if (event.keyCode == 13)
                {
                    assignTool.assign();
                    return false;
                }
            }
            $(document).ready(function() {
                assignTool  = new FSQuickAssignTool( wd );
        		$("#QuickAssignBtn").click(function(event) {
                                var el = this;
				$.post("/widgets/dashboard/client/accessctl/", {
						rt:'iframe',
						attributeId:'45,3'
					}, 
					function (response) {
						if(response=="1") { 
							 roll.autohide(false);
							var opt = {
								popup       : 'quickassigntool',
								title       : 'Quick Assign',
								width       : 400,
								preHandler  : assignTool.getWidgetHtml,
								postHandler : assignTool.initEvents,
								context     : assignTool,
								delay       : 0
							}  
                                                         roll.show(el,event,opt);
						} 
						else {
							Attention(response);
						}                        
					});
                    
                   
                });
            });
            </script>
EOTXT
,

        'quickapprove' => <<<EOTXT
            <div class="topToolBtn">
                <input type="button" id="QuickApproveBtn" value="Quick Approve" class="link_button" />
            </div>
            <script>
            $(document).ready(function() {
        		$("#QuickApproveBtn").click(function(event) {
                                        var el = this;
					$.post("/widgets/dashboard/client/accessctl/", {
						rt:'iframe',
						attributeId:'45,8'
					}, 
					function (response) {
						if(response=="1") { 
							 roll.autohide(false);
                                                var opt = {
                                                    popup       : 'quickapprove',
                                                    title       : 'Quick Approve',
                                                    width       : 540,
                                                    context     : wd,
                                                    preHandler  : wd.getWidgetHtml,
                                                    delay       : 0,
                                                    postHandler : function() {
                                                            
                                                            }
                                                            }
                                                roll.show(el,event,opt);
						} 
						else {
							Attention(response);
						}                        
                     jQuery(".Approve_approveForm-t321328").keyup(function(event){
                        if (event.which == 13)
                        {
                            jQuery(".updateApproveButton-t77333").click();
                            return false;
                        }
						});
					});
                    
                });
			});
            </script>
EOTXT
,
        
        'quickbid'  =>
            '<div class="topToolBtn">
                <input type="button" id="QuickBidBtn" value="Quick Bid" class="link_button" />
            </div>
            ',

        'quickbid_tech'  =>
            '<div class="topToolBtn">
                <input type="button" id="QuickBidBtn" value="Quick Bid" class="link_button middle2_button" />
            </div>
            ',

        'paystatus' =>
            '<div class="topToolBtn">
                   <input type="button" class="link_button" value="My Pay Status" id="MyPayStatusBtn">
                 </div>'
    );

    if ( is_string($toolButtons) ) {
        $toolButtons = array($toolButtons);
    }

    if ( is_array($toolButtons) ) : 
        $toolButtons = array_unique($toolButtons);
        ?>
    <div id="toolbtnmenu">
		<?php if ($page == "techs" ){echo "<table class=\"tCenter\" style=\"margin-left: auto; margin-right: auto;\">";}else{echo "<table class=\"tCenter\" style=\"margin-left: 15%; float:left; width: auto;\">";}?>
		
<tr>
        <?php

        foreach ( $toolButtons as $__btn ) {
            if ( !empty($__buttons__[strtolower(trim($__btn))]) ) {
                echo "<td>" . $__buttons__[strtolower(trim($__btn))] ."</td>";
            }
        }
        ?>
</tr>
</table>
<?php if (empty($selected) && $page != "techs" && $option == "wos"):?>
			
				<div class="full_lite" style="visibility: hidden">
					<span> View:
					<select id="dashboardVersion" onchange="wd.changeDasboaardVersion(this)">
						<option value="Full" <?=$_COOKIE["dashboardVersionDefault"] == "Full" ? "selected='selected'" : ""?>>Full</option>
						<option value="Lite" <?=$_COOKIE["dashboardVersionDefault"] == "Lite" ? "selected='selected'" : ""?>>Lite</option>
					</select>
					</span>
				</div>
			
	<?php endif;?>
         	<?php if ($page == "techs" )
                    {
                        echo '<div id="FSIdeas-techs" style="width:110px;"><span><a href="javascript:;" onclick="detailObject.onInit.showMyscheduleDownload(this);">
					<img width="25" height="25" border="0" title="" src="/images/CalendarIcon.gif" alt="" title="">
					</a>&nbsp;&nbsp;</span>';
                    }
                    else
                    {
                        echo '<div id="FSIdeas">';
                    }?> 
 
					<span><a href="mailto:ideas@fieldsolutions.com?subject=I have a suggestion or idea for FieldSolutions">
					<img width="60" height="22" border="0" title="Click Button to Register" src="/templates/<?php echo $_SESSION['template']; ?>/images/Ideas.png" alt="">
					</a></span>
				</div>
       		
       
      
     
	

    <?php endif; ?>
	</div>
 <?php endif;

if (!empty($user) && $user->getDeactivated() == 1):
?>
<?php script_nocache ("/widgets/js/functions.js"); ?>
<script type="text/javascript">
$(document).ready(function() {
	bindTechDeactivatedMsg("#QuickBidBtn");
	bindTechDeactivatedMsg("#FindAvailableWorkBtn");
	bindTechDeactivatedMsg("#comingSoon");
	bindTechDeactivatedMsg("#FSIdeas-techs");
	$("#FSIdeas-techs").hide();
});
</script>
<?php
endif;
?>

