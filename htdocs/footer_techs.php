<br style="clear: both;" />
<div class="bottom_bar gray_bg">
    <div class="bot_col">Quick Links
        <ul>
            <li><a class="bgCheckLink" rel="/techs/ajax/lightboxPages/backgroundChecks.php" href="javascript:///">Order a Background Check and/or Drug Test</a></li>
            <li><a href="javascript:///" onclick="tpm.openPopupWin('ICA');">Independent Contractor Agreement</a></li>
            <li><a onclick="return tpm.openPopupWin('help');" href="javascript:///">Help</a></li>
        </ul>
    </div>
    <div class="bot_col">
        <ul>
            <li><a onclick="return tpm.openPopupWin('FSPP');" href="javascript:///">Field Solutions Payment Policy</a></li>
            <li><a  href="javascript:///" onclick="tpm.openPopupWin('CoC');">Code of Conduct</a></li>
            <li><a  href="javascript:///" onclick="tpm.openPopupWin('privacypolicy');">Privacy Policy</a></li>
                    <li><a  href="javascript:///" onclick="tpm.openPopupWin('termsofuse');">Terms of Use</a></li>
        </ul></div>
    <div class="bot_col">
        <ul>
            <li><a onclick="return tpm.openPopupWin('get-started');" href="javascript:///">Get started in 3 easy steps</a></li>
            <li><a href="javascript:///" onclick="tpm.openPopupWin('community');">Tech Community</a></li>
            <li><a href="javascript:///" onclick="tpm.openPopupWin('aboutus');">About Us</a></li>
        </ul>
    </div>
</div>
	<!--<div class="bottom_links">
		<a href="https://www.fieldsolutions.com/wp/our-services/?view=1" title="Our Services">Our Services</a>
		<a href="https://www.fieldsolutions.com/wp/mission-values/?view=1" title="Mission &amp; Values">Mission &amp; Values</a>
		<a href="https://www.fieldsolutions.com/wp/our-clients/?view=1" title="Our Clients">Our Clients</a>
		<a href="https://www.fieldsolutions.com/wp/our-members/?view=1" title="Our Members">Our Members</a>
		<a href="https://www.fieldsolutions.com/wp/fieldsolutions-difference/?view=1" title="FieldSolutions Difference">FieldSolutions Difference</a>
		<a href="https://www.fieldsolutions.com/wp/management-team/?view=1 title="Management Team">Management Team</a>
		<a href="https://www.fieldsolutions.com/wp/news-and-events/?view=1" title="News and Events">News and Events</a>
		<a href="https://www.fieldsolutions.com/wp/wp-content/themes/field solutions/img/privacy_policy.pdf" target="_blank" title="Privacy Policy">Privacy Policy</a>
	</div>-->

        <br style="clear: both;" />

<div class="copyright">Copyright &copy; 2008-<?=date('Y');?>, Field Solutions, Inc. All rights reserved.</div>
</div>
<br /><br /><br /><br /><br />

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-4102511-1");
pageTracker._setDomainName(".fieldsolutions.com");
pageTracker._trackPageview();
} catch(err) {}</script>


</body>
</html>
