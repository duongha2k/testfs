<?
require_once('../../../includes/modules/web.init.php');
require_once('../../../includes/modules/common.init.php');
require_once('../../../includes/modules/tables.php');
$rptid = isset($_REQUEST['reportid']) ? $_REQUEST['reportid'] : "";

$auth = $_SESSION['Auth_User'];

$client_login_old = $auth->login;

if (!empty($_SESSION['PM_Company_ID']))
{
    $user = new Core_Api_User();
    $clients = $user->getRepClientforCompany($_SESSION['PM_Company_ID']);
    $client_login = $clients[0]['UserName'];
    $client_password = $clients[0]['Password'];
} else
{
    $client_login = $auth->login;
    $client_password = $auth->password;
}

$client = new Core_Api_Class();
$isPM = $client->isGPMByUserName($client_login_old);
$company = isset($_REQUEST['company']) ? $_REQUEST['company'] : "";
$CusReportClass = new Core_Api_CusReportClass();
$FieldList = $CusReportClass->getReportFieldList2($rptid);
$FilterList = $CusReportClass->getFilterList2($rptid);
//print_r('FilterList<pre>');
//print_r($FilterList);
//print_r('</pre>');
function getTime($RRDTimeOfDay)
{
    $TIME = "";
    $timestamp = "";
    $timepart3 = 0;
    $timepart2 = "";
    $timepart1 = "";
    $i = 0;
    for ($i = 0; $i < 24; $i++)
    {
        for ($j = 1; $j < 3; $j++)
        {
            if ($j == 1)
                $timepart2 = "00";
            else
                $timepart2 = ($j - 1) * 30;

            if ($i < 12 || $i == 24)
                $timepart3 = "AM";
            else
                $timepart3 = "PM";
            if ($i == 0)
                $timepart1 = "12";
            else if ($i < 13 && $i > 0)
                $timepart1 = $i;
            else
                $timepart1 = $i - 12;

            $timestamp = $timepart1 . ":" . $timepart2 . " " . $timepart3;
            $selected = "";
            if ($RRDTimeOfDay == $timestamp)
            {
                $selected = "selected";
            }

            $TIME .= ("<option value='" . $timestamp . "' " . $selected . ">" . $timestamp . "</option>");
        }
    }
    return $TIME;
}

function loaddatetime($key, $data_ref, $from, $to, $section)
{
    if (!empty($from))
    {
        $datefrom = new DateTime($from);
        $from = $datefrom->format('m/d/Y');
    }
    if (!empty($to))
    {
        $dateto = new DateTime($to);
        $to = $dateto->format('m/d/Y');
    }
    $str = "<select class='clsdatetime' section='" . $section . "' sectionid='" . $key . "' style='width:210px' id='Date_" . $key . "'>
                                    <option value='FixedDateRange' " . ($data_ref == "FixedDateRange" ? "selected" : "") . ">Fixed Date Range</option>
                                    <option value='Today' " . ($data_ref == "Today" ? "selected" : "") . ">Today</option>
                                    <option value='TodayAndTomorrow' " . ($data_ref == "TodayAndTomorrow" ? "selected" : "") . ">Today and Tomorrow</option>
                                    <option value='TodayAndNextBusinessDay' " . ($data_ref == "TodayAndNextBusinessDay" ? "selected" : "") . ">Today and Next Business Day</option>
                                    <option value='Tomorrow' " . ($data_ref == "Tomorrow" ? "selected" : "") . ">Tomorrow</option>
                                    <option value='NextBusinessDay' " . ($data_ref == "NextBusinessDay" ? "selected" : "") . ">Next Business Day</option>
                                    <option value='Next2Days' " . ($data_ref == "Next2Days" ? "selected" : "") . ">Next 2 Days</option>
                                    <option value='Next2BusinessDays' " . ($data_ref == "Next2BusinessDays" ? "selected" : "") . ">Next 2 Business Days</option>
                                    <option value='Next3Days' " . ($data_ref == "Next3Days" ? "selected" : "") . ">Next 3 Days</option>
                                    <option value='Next3BusinessDays' " . ($data_ref == "Next3BusinessDays" ? "selected" : "") . ">Next 3 Business Days</option>
                                    <option value='Next4Days' " . ($data_ref == "Next4Days" ? "selected" : "") . ">Next 4 Days</option>
                                    <option value='Next4BusinessDays' " . ($data_ref == "Next4BusinessDays" ? "selected" : "") . ">Next 4 Business Days</option>
                                    <option value='Next5Days' " . ($data_ref == "Next5Days" ? "selected" : "") . ">Next 5 Days</option>
                                    <option value='Next5BusinessDays' " . ($data_ref == "Next5BusinessDays" ? "selected" : "") . ">Next 5 Business Days</option>
                                    <option value='ThisWeek' " . ($data_ref == "ThisWeek" ? "selected" : "") . ">This Week</option>
                                    <option value='ThisMonth' " . ($data_ref == "ThisMonth" ? "selected" : "") . ">This Month</option>
                                    <option value='Yesterday' " . ($data_ref == "Yesterday" ? "selected" : "") . ">Yesterday</option>
                                    <option value='LastBusinessDay' " . ($data_ref == "LastBusinessDay" ? "selected" : "") . ">Last Business Days</option>
                                    <option value='Last2Days' " . ($data_ref == "Last2Days" ? "selected" : "") . ">Last 2 Days</option>
                                    <option value='Last2BusinessDays' " . ($data_ref == "Last2BusinessDays" ? "selected" : "") . ">Last 2 Business Days</option>
                                    <option value='Last3Days' " . ($data_ref == "Last3Days" ? "selected" : "") . ">Last 3 Days</option>
                                    <option value='Last3BusinessDays' " . ($data_ref == "Last3BusinessDays" ? "selected" : "") . ">Last 3 Business Days</option>
                                    <option value='Last4Days' " . ($data_ref == "Last4Days" ? "selected" : "") . ">Last 4 Days</option>
                                    <option value='Last4BusinessDays' " . ($data_ref == "Last4BusinessDays" ? "selected" : "") . ">Last 4 Business Days</option>
                                    <option value='Last5Days' " . ($data_ref == "Last5Days" ? "selected" : "") . ">Last 5 Days</option>
                                    <option value='Last5BusinessDays' " . ($data_ref == "Last5BusinessDays" ? "selected" : "") . ">Last 5 Business Days</option>
                                    <option value='LastWeek' " . ($data_ref == "LastWeek" ? "selected" : "") . ">Last Week</option>
                                    <option value='LastMonth' " . ($data_ref == "LastMonth" ? "selected" : "") . ">Last Month</option>
                                    <option value='Q1' " . ($data_ref == "Q1" ? "selected" : "") . ">Q1 " . date("Y") . "</option>
                                    <option value='Q2' " . ($data_ref == "Q2" ? "selected" : "") . ">Q2 " . date("Y") . "</option>
                                    <option value='Q3' " . ($data_ref == "Q3" ? "selected" : "") . ">Q3 " . date("Y") . "</option>
                                    <option value='Q4' " . ($data_ref == "Q4" ? "selected" : "") . ">Q4 " . date("Y") . "</option>
                                    <option value='H1' " . ($data_ref == "H1" ? "selected" : "") . ">H1 " . date("Y") . "</option>
                                    <option value='H2' " . ($data_ref == "H2" ? "selected" : "") . ">H2 " . date("Y") . "</option>
                                    <option value='YTD' " . ($data_ref == "YTD" ? "selected" : "") . ">YTD</option>
                                </select>&nbsp;between&nbsp;<input type='text' value='" . $from . "' style='width:80px' class='showCal' id='StartDate" . $key . "'>
                            &nbsp;and&nbsp;<input type='text' value='" . $to . "' style='width:80px' class='showCal' id='EndDate" . $key . "'>";
    return $str;
}

function loadDropdown($ComID, $isPM, $fieldCode, $SelectMultiple)
{
    $CusReportClass = new Core_Api_CusReportClass();
    $SelectMultiple = $SelectMultiple;
    $DropdownOptions = $CusReportClass->getFilterDropdownOptions($fieldCode, $ComID, $isPM);
    $str .="<select sectionid='$fieldCode' class='DropdownBoxchange' style='min-width:155px' id='$fieldCode'>";
    if (!empty($DropdownOptions) && count($DropdownOptions))
    {
        foreach ($DropdownOptions as $option)
        {
            $value = $option['Value'];
            $caption = $option['Caption'];
            $display = 1;
            if (!empty($SelectMultiple) && count($SelectMultiple))
            {
                foreach ($SelectMultiple as $selValue)
                {
                    if ($selValue == $value)
                    {
                        $display = 0;
                    }
                }
            }
            $str .= "<option value='$value' style='" . ($display == 1 ? "" : "display:none;") . "' ".(!empty($option["Disable"])?$option["Disable"]:"").">$caption</option>";
        }
    }
    $str.="</select><br/>";
    if (!empty($SelectMultiple) && count($SelectMultiple))
    {
        foreach ($SelectMultiple as $selValue)
        {
            $selKey = $selValue;
            $option = $DropdownOptions["$selKey"];
            $caption = $option['Caption'];
            $str.="
                <span>
                    <img height='15px' id='$fieldCode" . "__$fieldCode" . "__$selKey' valstr='$caption' style='cursor:pointer;' src='/widgets/css/images/removedoc.png' class='cmdremovedropdownitem'>&nbsp;&nbsp;$caption
                    <br>
                </span>";
        }
    }
    return $str;
}

function loadDYN($fieldCode, $SelectMultiple)
{
    $SelectMultiple = $SelectMultiple;
    $DropdownOptions = array("t" => "Yes", "f" => "No", "a" => "Any");
    $str .="<select sectionid='$fieldCode' class='' style='min-width:155px' id='$fieldCode'>";
    foreach ($DropdownOptions as $key => $val)
    {
        $str .= "<option value='$key' " . ($SelectMultiple[0] == $key ? "selected" : "") . ">$val</option>";
    }
    $str.="</select><br/>";
    return $str;
}

function loadtext($fieldCode, $stringValue)
{
    $str = "<input type='text' style='width:155px' id='$fieldCode' value='$stringValue'>";
    return $str;
}

function loadN2($fieldCode, $numFrom, $numTo)
{
    $str = "from&nbsp;<input class='txtN2' type='text' id='" . $fieldCode . "__from' " . $fieldCode . "_type='N2' style='width: 50px;" . (empty($numFrom) ? "color:#A6A6A6;" : "") . "'  value='" . (empty($numFrom) ? "#.##" : $numFrom) . "' />&nbsp;to&nbsp;
                <input class='txtN2' type='text' id='" . $fieldCode . "__to' " . $fieldCode . "_type='N2' style='width: 50px;" . (empty($numTo) ? "color:#A6A6A6;" : "") . "' value='" . (empty($numTo) ? "#.##" : $numTo) . "' />";
    return $str;
}

function loadT2($fieldCode, $timeFrom, $timeTo, $time_tz)
{
    $str = "from <select  style='width:110px' id='" . $fieldCode . "__from' " . $fieldCode . "_type='T2'> " . getT2options($timeFrom) . "</select>
                &nbsp;&nbsp;&nbsp;to <select  style='width:110px' id='" . $fieldCode . "__to' " . $fieldCode . "_type='T2'> " . getT2options($timeTo) . "</select>
                &nbsp;&nbsp;&nbsp;<select id='" . $fieldCode . "est' style='width:150px;'>
                                <option value ='-10' " . ($time_tz == "-10" ? "selected" : "") . ">Hawaii (GMT-10:00)</option>
                                <option value ='-9' " . ($time_tz == "-9" ? "selected" : "" ) . ">Alaska (GMT-8:00)</option>
                                <option value ='-8' " . ($time_tz == "-8" ? "selected" : "") . ">Pacific (GMT-7:00)</option>
                                <option value ='-7' " . ($time_tz == "-7" ? "selected" : "") . ">Mountain (GMT-6:00)</option>
                                <option value ='-6' " . ($time_tz == "-6" ? "selected" : "") . ">Central (GMT-5:00)</option>
                                <option value ='-5' " . ($time_tz == "-5" ? "selected" : "") . ">Eastern (GMT-4:00)</option>
                            </select>";
    return $str;
}

function getT2options($selValue)
{
    $timepart3 = 0;
    $timepart2 = "";
    $timepart1 = "";
    $Time = "";
    $Time .= "<option value=''>Select Time</option>";
    for ($i = 0; $i < 24; $i++)
    {
        for ($j = 1; $j < 5; $j++)
        {
            if ($j == 1)
                $timepart2 = "00";
            else
                $timepart2 = ($j - 1) * 15;

            if ($i < 12 || $i == 24)
                $timepart3 = "AM";
            else
                $timepart3 = "PM";

            if ($i == 0)
                $timepart1 = "12";
            else if ($i < 13 && $i > 0)
                $timepart1 = $i;
            else
                $timepart1 = $i - 12;

            $timestamp = $timepart1 . ":" . $timepart2 . " " . $timepart3;

            $selected = '';
            if ($selValue == $timestamp)
            {
                $selected = "selected='selected'";
            }
            $Time .= "<option value='" . $timestamp . "' $selected >" . $timestamp . "</option>";
        }
    }
    return $Time;
}

function buildcontrol($filterType, $ComID, $isPM, $fieldCode, $fieldvalue)
{
    switch ($filterType)
    {
        case "D":
            return loadDropdown($ComID, $isPM, $fieldCode, $fieldvalue['SelectMultiple']);
            break;
        case "T":
            return loadtext($fieldCode, $fieldvalue['string_value']);
            break;
        case "DT":
            return loaddatetime($fieldCode, $fieldvalue["datetime_relative_data_ref"], $fieldvalue["datetime_range_from"], $fieldvalue["datetime_range_to"]);
            break;
        case "N2":
            return loadN2($fieldCode, $fieldvalue['numeric_range_from'], $fieldvalue['numeric_range_to']);
            break;
        case "DYN":
            return loadDYN($fieldCode, $fieldvalue['SelectMultiple']);
            break;
        case "T2":
            return loadT2($fieldCode, $fieldvalue['time_from'], $fieldvalue['time_to'], $fieldvalue['time_tz']);
            break;
    }
}

$FieldOrder = array(
    "WIN_NUM", "WO_ID", "Project_ID", "PO", "WorkOrderOwner", "Type_ID", "WO_Category_ID", "_AccountManager",
    "_ClientServicesDirector", "_ServiceType", "_LegalEntity", "Headline", "Description", "Requirements", "SpecialInstructions",
    "Ship_Contact_Info", "Qty_Devices", "Lead", "Assist", "Update_Reason", "Update_Requested", "ShortNotice", "_VisitStartTypeID",
    "_VisitStartDate", "_VisitStartTime", "_VisitEndDate", "_VisitEndTime", "_VisitSiteTimeZone", "_VisitDayPart", "_VisitLeadTime",
    "_VisitEstimatedDuration", "Qty_Visits", "_VisitActualCheckInDate", "_VisitActualCheckInTime", "_VisitActualCheckInResult", "_VisitActualCheckOutDate",
    "_VisitActualCheckOutTime", "_VisitActualCheckOutResult", "_VisitActualCalculatedDuration", "_VisitActualDurationResult", "SiteNumber",
    "SiteName", "Address", "City", "State", "Zipcode", "Country", "_Geography", "Region", "Route", "Latitude", "Longitude", "Site_Contact_Name",
    "SiteEmail", "SitePhone", "SiteFax", "_TechID", "_BiddingTechFLSID", "_Tech_Fname", "_Tech_Lname", "_BiddingTechEmail", "_BiddingTechPhone",
    "_BiddingTechPreferred", "_BidPayMax", "_BidAmount_Per", "_BidAmount", "_Comments", "_Bid_Date", "_Hide", "Qty_Applicants", "Tech_ID",
    "FLS_ID", "Tech_FName", "Tech_LName", "_PrimaryEmail", "_SecondaryEmail", "_PrimaryPhone", "_SecondaryPhone", "_AssignedTechPreferred",
    "_AssignedTechDenied", "_TechBidAmount", "_TechAmount_Per", "bid_comments", "_TechBid_Date", "_TechBackedOut", "BackOut_Tech",
    "_BackOutFLSTechID", "_TechNoShowed", "NoShow_Tech", "_NoShowFLSTechID", "BU1FLSID", "Backup_FLS_ID", "BU1Info", "_EmailBlastRadius",
    "ProjectManagerName", "ProjectManagerEmail", "ProjectManagerPhone", "ResourceCoordinatorName", "ResourceCoordinatorEmail",
    "ResourceCoordinatorPhone", "_ACSNotifiPOC", "_ACSNotifiPOC_Email", "_ACSNotifiPOC_Phone", "EmergencyName", "EmergencyEmail",
    "EmergencyPhone", "CheckInOutName", "CheckInOutEmail", "CheckInOutNumber", "TechnicalSupportName", "TechnicalSupportEmail",
    "TechnicalSupportPhone", "WorkOrderReviewed", "TechCheckedIn_24hrs", "StoreNotified", "Notifiedby", "NotifyNotes", "PreCall_1",
    "PreCall_1_Status", "PreCall_2", "PreCall_2_Status", "CheckedIn", "Checkedby", "CheckInNotes", "Incomplete_Paperwork", "Paperwork_Received",
    "MissingComments", "ClientComments", "SATRecommended", "SATPerformance", "TechMarkedComplete", "CallClosed", "ContactHD", "HDPOC",
    "Unexpected_Steps", "Unexpected_Desc", "AskedBy", "AskedBy_Name", "FLS_OOS", "Penalty_Reason", "TechMiles", "TechComments", "_WINTagAdded",
    "_TotalNumOfWINTags", "_WINTagAddedDate", "_WINTagAddedBy", /*"_WINTagComplete", "_WINTagIncomplete", "_WINTagRescheduled",
    "_WINTagSiteCancelled",*/ "_WINTag", "_WINTagOwner", "_WINTagDetails", "_WINTagNotes", "_WINTagBillable", "_WINTagCheckInResult",
    "_WINTagCheckOutResult", "_WINTagDurationResult", "_WINTagTechPreferenceRating", "_WINTagTechPerformanceRating", "_NumofNewParts",
    "_NumofReturnParts", "_PartManagerUpdatedByTech", "_NewPartPartNum", "_NewPartModelNum", "_NewPartSerialNum", "_NewPartEstArrival",
    "_NewPartCarrier", "_NewPartTrackingNum", "_NewPartInfo", "_NewPartShipsTo", "_NewPartShippingNotes", "_NewPartisConsumable",
    "_ReturnPartPartNum", "_ReturnPartModelNum", "_ReturnPartSerialNum", "_ReturnPartEstArrial", "_ReturnPartCarrier", "_ReturnPartTrackingNum",
    "_ReturnPartInfo", "_ReturnPartRMA", "_ReturnPartInstructions", "PayMax", "Tech_Bid_Amount", "Amount_Per", "calculatedTechHrs",
    "baseTechPay", "Additional_Pay_Amount", "OutofScope_Amount", "TripCharge", "MaterialsReimbursement", "MileageReimbursement", "AbortFee",
    "AbortFeeAmount", "Total_Reimbursable_Expense", "Add_Pay_AuthBy", "Add_Pay_Reason", "Approved", "approvedBy", "PayAmount", "PricingRuleID",
    "PricingCalculationText", "PricingRuleApplied", "PricingRan", "Invoiced", "calculatedInvoice", "PcntDeduct", "PcntDeductPercent",
    "Net_Pay_Amount", "TechPaid", "FS_InternalComments", "DateEntered", "Username", "_PublishedDate", "_PublishedBy", "SourceByDate",
    "Date_Assigned", "_AssignedBy", "DateNotified", "Notifiedby", "Date_Completed", "DateIncomplete", "_MarkedIncompleteBy", "DateApproved",
    "_approvedBy", "DateInvoiced", "DatePaid", "Deactivated", "DeactivatedDate", "DeactivatedBy", "DeactivationCode", "Deactivated_Reason",
    "lastModDate", "lastModBy");
?>
<table id="filterTable" cellpadding="0" cellspacing="0">
    <colgroup>
        <col width="*">
        <col width="*">
    </colgroup>
    <tbody>
        <?
        foreach ($FieldOrder as $key => $val)
        {
            if (!empty($FieldList[$val]["FilterType"]))
            {
                ?>
                <tr fieldcode="<?= $val ?>" control_type="<?= $FieldList[$val]["FilterType"] ?>" datatype="<?= $FilterList[$val]["datatype"] ?>">
                    <td style="vertical-align:top;">
                        <?= $FieldList[$val]["field_title"] ?>
                    </td>
                    <td>
                        <?=
                        buildcontrol($FieldList[$val]["FilterType"], $company, $isPM, $val, $FilterList[$val], "");
                        ?>
                    </td>
                </tr>
                <?
            }
        }
        ?>
    <tbody>
</table>