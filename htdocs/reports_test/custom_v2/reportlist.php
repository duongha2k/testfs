<?php
$page = "clients";
$option = "reports";
$selected = 'MetricsReport';
$UserType = (!empty($_SESSION['UserType']) ) ? $_SESSION['UserType'] : NULL;
$isFLSManager = (strtolower($Company_ID) == 'fls' && (strtolower($UserType) === 'install desk' || strtolower($UserType) === 'manager'));

if ($isFLSManager)
{
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
} else
{
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
}

require ("../../header.php");
require ("../../navBar.php");
require ("../../clients/includes/adminCheck.php");
require_once("../../clients/includes/PMCheck.php");

$displayPMTools = true;
$companyID = $_GET["v"];
?>
<script>
    var _roll = new FSPopupRoll();

    this.deleteReport = function(el, id, name) {
        var opt = {};
        var html = '<div style="padding-bottom:10px;">\n\
                    Are you sure you wish to delete this report: <b>' + name + '</b>?\n\
                </div>\n\
                <div style="text-align:center;">\n\
                    <input type="button" id="cmdCancel" class="link_button link_button_popup" value="Cancel">\n\
                    <input type="button" id="cmddeletereport" class="link_button link_button_popup" value="Delete">\n\
                </div>';
        opt.width = 440;
        opt.height = '';
        opt.title = '<div style="text-align:center;">Delete Report</div>';
        opt.body = html;
        opt.zindex = 999;
        roll.showNotAjax(el, null, opt);
        $("#cmdCancel").unbind('click').bind('click', function() {
            roll.hide();
        });
        $("#cmddeletereport").unbind('click').bind('click', function() {
            roll.hide();
            dodeleteReport(id);
        });
    };

    this.dodeleteReport = function(id) {
        var _onSuccess = function(data) {
            window.location.reload();
        };
        _global.data = "";
        _global.url = "/widgets/dashboard/custom-report/do-delete-report?report_id=" + id;
        _global.onSuccess = _onSuccess;
        _global.show();
    };

    this.loadownerreport = function()
    {
        var comid = '<?= $companyID ?>';
        var page = $(this).attr('page');
        var size = 50;
        if (typeof(page) === 'undefined')
            page = 1;
        if (typeof(size) === 'undefined')
            size = 50;
        if (typeof(rt) === 'undefined')
            rt = 1;
        var submitData = '&v=' + comid + '&page=' + page + '&size=' + size + '&rt=' + rt + '&owner=1';

        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/custom-report/report-list/",
            dataType: 'html',
            cache: false,
            context: this,
            data: submitData,
            success: function(data, status, xhr) {
                $("#reportlistcontent").html(data);
                $(".paginatorLinkActiv").unbind("click").bind("click", loadownerreport);
                $(".copyreport").unbind("click").bind("click", copyreport);
                $(".sortable").unbind("click").bind("click", sortreport);
            }
        });
    };

    this.loadreportlist = function()
    {
        var comid = '<?= $companyID ?>';
        var page = $(this).attr('page');
        var size = 50;
        if (typeof(page) === 'undefined')
            page = 1;
        if (typeof(size) === 'undefined')
            size = 50;
        if (typeof(rt) === 'undefined')
            rt = 1;
        var submitData = '&v=' + comid + '&page=' + page + '&size=' + size + '&rt=' + rt;

        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/custom-report/report-list/",
            dataType: 'html',
            cache: false,
            context: this,
            data: submitData,
            success: function(data, status, xhr) {
                $("#reportlistcontent").html(data);
                $(".paginatorLinkActiv").unbind("click").bind("click", loadreportlist);
                $(".copyreport").unbind("click").bind("click", copyreport);
                $(".sortable").unbind("click").bind("click", sortreport);
            }
        });
    };

    this.copyreport = function()
    {
        if ($(this).attr("isGPM") === "1")
        {
            var params = {"company": "<?= $companyID ?>", "reportid": $(this).attr("reportid")};

            $.ajax({
                url: '/widgets/dashboard/custom-report/load-com-list/',
                dataType: 'json',
                data: params,
                cache: false,
                type: 'post',
                context: this,
                success: function(data, status, xhr) {
                    if (data) {
                        var html = '\
                        <div class="tCenter">\n\
                            <table style="text-align: left; margin-right:auto; margin-left: auto;">\n\
                                <tbody>\n\
                                    <tr>\n\
                                        <td colspan="2" align="left">Select Company</td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td colspan="2">\n\
                                            <select id="copyToCompany">';
                        for (index in data.companies) {
                            html += '<option value="' + data.companies[index] + '"';
                            if (data.companies[index] === params.company) {
                                html += "selected='selected'";
                            }
                            html += ' >' + index + '</option>';
                        }
                        html += '\
                                            </select>\n\
                                        </td>\n\
                                    </tr>\n\
                                    <tr>\n\
                                        <td colspan="2" align="center">\n\
                                            <input type="button" class="link_button_popup" id="cancelrptSubmit" value="Cancel" onclick="_roll.hide();" />\n\
                                            <input type="button" class="link_button_popup" id="copyrptSubmit" value="Create" onclick="copyrpt(this);" reportid="' + params.reportid + '"/>\n\
                                        </td>\n\
                                    </tr>';
                        html += '</tbody><table></div>';

                        _roll.autohide(false);
                        var opt = {
                            width: 350,
                            height: '',
                            position: 'middle',
                            title: 'Copy Report',
                            body: html
                        };
                        _roll.showNotAjax(null, null, opt);

                    }
                }
            });
        }
        else
        {
            var rptid = $(this).attr("reportid");
            window.location = "custom_report.php?v=<?= $companyID ?>&rptcopy=" + rptid + "&oldv=<?= $companyID ?>";
        }
    };

    this.copyrpt = function(el)
    {
        window.location = "custom_report.php?v=" + $("#copyToCompany option:selected").val() + "&rptcopy=" + $(el).attr("reportid") + "&oldv=<?= $companyID ?>";
    };

    this.ownerreport_click = function()
    {
        if ($(this).attr("id") === "optMineOnly")
        {
            loadownerreport();
        }
        else
        {
            loadreportlist();
        }
    };

    this.sortreport = function()
    {
        var el = this;
        var field = $(this).attr("field");
        var fieldorder = $(this).attr("fieldorder");
        var isownerreport = "";
        if (!$("#optTotalComp").is(":checked"))
        {
            isownerreport = "&owner=1";
        }
        var comid = '<?= $companyID ?>';
        var page = $(this).attr('page');
        var size = 50;
        if (typeof(page) === 'undefined')
            page = 1;
        if (typeof(size) === 'undefined')
            size = 50;

        var submitData = '&v=' + comid + '&page=' + page + '&size=' + size + isownerreport + "&sort=" + field + " " + fieldorder;
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/custom-report/report-list/",
            dataType: 'html',
            cache: false,
            context: this,
            data: submitData,
            success: function(data, status, xhr) {
                $("#reportlistcontent").html(data);
                $(".paginatorLinkActiv").unbind("click").bind("click", loadreportlist);
                $(".copyreport").unbind("click").bind("click", copyreport);
                $(".sortable").unbind("click").bind("click", sortreport);
                $(".sortable").each(function(){
                    if($(this).attr("field") ===field)
                    {
                        if(fieldorder ==="asc")
                        {
                            $(this).attr("fieldorder","desc");
                        }
                        else
                        {
                            $(this).attr("fieldorder","asc");
                        }
                    }
                });
            }
        });

    }

    jQuery(window).ready(function() {

        loadreportlist();
        $("#optTotalComp").unbind("click").bind("click", ownerreport_click);
        $("#optMineOnly").unbind("click").bind("click", ownerreport_click);
    });
</script>
<div align="center">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th nowrap="nowrap" align="center" scope="col">Create, Generate &amp; Store Custom Reports</th>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                <div align="center">
                    <input type="button" onclick="window.location = 'custom_report.php?v=<?= $_GET['v'] ?>&t=1'" class="link_button middle2_button" value="Create New Report">
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div style="margin-top:-50px;text-align:left;">
                    <?
                    $auth = $_SESSION['Auth_User'];
                    $owner = array("created_by" => $auth["login"]);
                    $Core_Report = new Core_Api_CusReportClass();
                    $ownerreportcount = sizeof($Core_Report->getList($companyID, 0, 0, $owner));
                    if ($ownerreportcount > 0)
                    {
                        echo("Show Stored Reports<br/>");
                    }
                    ?>
                    <div id="divownerreport" align = "left" style="margin-left:5px;<?= ($ownerreportcount > 0) ? "" : "display:none;" ?>">
                        <input type="radio" id="optTotalComp" name="displayoption" checked><span style="padding-left:5px;">Total Company</span><br/>
                        <input type="radio" id="optMineOnly" name="displayoption" ><span style="padding-left:5px;">Mine Only</span>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td id="reportlistcontent">
            </td>
        </tr>
        <tr>
            <td>
                <div align="center"></div>
            </td>
        </tr>
    </table>
</div>

<?php require ("../../footer.php"); ?><!-- ../ only if in sub-dir -->
