<?php
$page = "clients";
$option = "reports";
$selected = 'MetricsReport';
if ($isFLSManager)
{
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
} else
{
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
}
require ("../../header.php");
require ("../../navBar.php");
require ("../../clients/includes/adminCheck.php");
$displayPMTools = true;
require_once("../../clients/includes/PMCheck.php");

$auth = $_SESSION['Auth_User'];

$client_login_old = $auth->login;

if (!empty($_SESSION['PM_Company_ID']))
{
    $user = new Core_Api_User();
    $clients = $user->getRepClientforCompany($_SESSION['PM_Company_ID']);
    $client_login = $clients[0]['UserName'];
    $client_password = $clients[0]['Password'];
} else
{
    $client_login = $auth->login;
    $client_password = $auth->password;
}
$client = new Core_Api_Class();
$isPM = $client->isGPMByUserName($client_login_old);

$ComID = $_GET["v"];
$reportid = $_GET["report_id"];
$rptcopy = $_GET["rptcopy"];
$oldv = $_GET["oldv"];
if (!empty($rptcopy))
{
    $reportid = $rptcopy;
}
$CusReportClass = new Core_Api_CusReportClass();

function getDay($Day)
{
    switch ($Day)
    {
        case 1:
            return(2);
            break;
        case 2:
            return(3);
            break;
        case 3:
            return(4);
            break;
        case 4:
            return(5);
            break;
        case 5:
            return(6);
            break;
        case 6:
            return(7);
            break;
        case 7:
            return(0);
            break;
    }
}

$Today = date("m/d/Y");
$Tomorrow = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")));
$Yesterday = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - 1, date("Y")));

$j = 1;
$k = 1;
for ($i = 1; $i <= 7; $i++)
{
    if ($j <= 5)
    {
        $lastdatenum = date("w", mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));

        if ($lastdatenum > 0 && $lastdatenum < 6)
        {
            switch ($j)
            {
                case 1:
                    $LastBusinessDay = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));
                    break;
                case 2:
                    $Last2Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));
                    break;
                case 3:
                    $Last3Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));
                    break;
                case 4:
                    $Last4Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));
                    break;
                case 5:
                    $Last5Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));
                    break;
            }
            $j++;
        }
    }

    if ($k <= 5)
    {
        $nextdatenum = date("w", mktime(0, 0, 0, date("m"), date("d") + $i, date("Y")));
        if ($nextdatenum > 0 && $nextdatenum < 6)
        {
            switch ($k)
            {
                case 1:
                    $NextBusinessDay = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + $i, date("Y")));
                    break;
                case 2:
                    $Next2Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + $i, date("Y")));
                    break;
                case 3:
                    $Next3Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + $i, date("Y")));
                    break;
                case 4:
                    $Next4Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + $i, date("Y")));
                    break;
                case 5:
                    $Next5Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + $i, date("Y")));
                    break;
            }
            $k++;
        }
    }
}

for ($i = 1; $i <= 7; $i++)
{
    $lastdatenum = date("w", mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));
    if ($lastdatenum > 0 && $lastdatenum < 6)
    {
        $LastDayend = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));
        break;
    }
}

for ($i = 1; $i <= 7; $i++)
{
    $nextdatenum = date("w", mktime(0, 0, 0, date("m"), date("d") + $i, date("Y")));
    if ($nextdatenum > 0 && $nextdatenum < 6)
    {
        $NextDaystart = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + $i, date("Y")));
        break;
    }
}

$Next2days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + 2, date("Y")));
$Next3days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + 3, date("Y")));
$Next4days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + 4, date("Y")));
$Next5days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + 5, date("Y")));

$Last2days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - 2, date("Y")));
$Last3days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - 3, date("Y")));
$Last4days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - 4, date("Y")));
$Last5days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - 5, date("Y")));

// This Week (Mon = 1 - Sun = 7)
$BOW = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - getDay(date("N")) + 1, date("Y")));
$EOW = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - getDay(date("N")) + 7, date("Y")));

// last Week (Mon = 1 - Sun = 7)
$BOLW = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - getDay(date("N")) - 6, date("Y")));
$EOLW = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - getDay(date("N")), date("Y")));

// This Month
$BOM = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - date("d") + 1, date("Y")));
$EOM = date("m/d/Y", mktime(0, 0, 0, date("m") + 1, date("d") - date("d"), date("Y")));

// Last Month
$PrevBOM = date("m/d/Y", mktime(0, 0, 0, date("m") - 1, date("d") - date("d") + 1, date("Y")));
$PrevEOM = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - date("d"), date("Y")));

// Q1
$Q1Start = date("m/d/Y", mktime(0, 0, 0, 1, 1, date("Y")));
$Q1End = date("m/d/Y", mktime(0, 0, 0, 4, date("d") - date("d"), date("Y")));

// Q2
$Q2Start = date("m/d/Y", mktime(0, 0, 0, 4, 1, date("Y")));
$Q2End = date("m/d/Y", mktime(0, 0, 0, 7, date("d") - date("d"), date("Y")));

// Q3
$Q3Start = date("m/d/Y", mktime(0, 0, 0, 7, 1, date("Y")));
$Q3End = date("m/d/Y", mktime(0, 0, 0, 10, date("d") - date("d"), date("Y")));

// Q4
$Q4Start = date("m/d/Y", mktime(0, 0, 0, 10, 1, date("Y")));
$Q4End = date("m/d/Y", mktime(0, 0, 0, 13, date("d") - date("d"), date("Y")));

// H1
$H1Start = date("m/d/Y", mktime(0, 0, 0, 1, 1, date("Y")));
$H1End = date("m/d/Y", mktime(0, 0, 0, 7, date("d") - date("d"), date("Y")));

// H2
$H2Start = date("m/d/Y", mktime(0, 0, 0, 7, 1, date("Y")));
$H2End = date("m/d/Y", mktime(0, 0, 0, 13, date("d") - date("d"), date("Y")));

// YTD
$YTDStart = date("m/d/Y", mktime(0, 0, 0, 1, 1, date("Y")));
$YTDEnd = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d"), date("Y")));

function getTime($RRDTimeOfDay)
{
    $TIME = "";
    $timestamp = "";
    $timepart3 = 0;
    $timepart2 = "";
    $timepart1 = "";
    $i = 0;
    for ($i = 0; $i < 24; $i++)
    {
        for ($j = 1; $j < 3; $j++)
        {
            if ($j == 1)
                $timepart2 = "00";
            else
                $timepart2 = ($j - 1) * 30;

            if ($i < 12 || $i == 24)
                $timepart3 = "AM";
            else
                $timepart3 = "PM";
            if ($i == 0)
                $timepart1 = "12";
            else if ($i < 13 && $i > 0)
                $timepart1 = $i;
            else
                $timepart1 = $i - 12;

            $timestamp = $timepart1 . ":" . $timepart2 . " " . $timepart3;
            $selected = "";
            if ($RRDTimeOfDay == $timestamp)
            {
                $selected = "selected";
            }

            $TIME .= ("<option value='" . $timestamp . "' " . $selected . ">" . $timestamp . "</option>");
        }
    }
    return $TIME;
}

$ReportInfo = array();

function loaddatetime($key, $data_ref, $from, $to, $section)
{
    if (!empty($from))
    {
        $datefrom = new DateTime($from);
        $from = $datefrom->format('m/d/Y');
    }
    if (!empty($to))
    {
        $dateto = new DateTime($to);
        $to = $dateto->format('m/d/Y');
    }

    $str = "<select class='clsdatetime' section='" . $section . "' sectionid='" . $key . "' style='width:210px' id='Date_" . $key . "'>
                                    <option value='FixedDateRange' " . ($data_ref == "FixedDateRange" ? "selected" : "") . ">Fixed Date Range</option>
                                    <option value='Today' " . ($data_ref == "Today" ? "selected" : "") . ">Today</option>
                                    <option value='TodayAndTomorrow' " . ($data_ref == "TodayAndTomorrow" ? "selected" : "") . ">Today and Tomorrow</option>
                                    <option value='TodayAndNextBusinessDay' " . ($data_ref == "TodayAndNextBusinessDay" ? "selected" : "") . ">Today and Next Business Day</option>
                                    <option value='Tomorrow' " . ($data_ref == "Tomorrow" ? "selected" : "") . ">Tomorrow</option>
                                    <option value='NextBusinessDay' " . ($data_ref == "NextBusinessDay" ? "selected" : "") . ">Next Business Day</option>
                                    <option value='Next2Days' " . ($data_ref == "Next2Days" ? "selected" : "") . ">Next 2 Days</option>
                                    <option value='Next2BusinessDays' " . ($data_ref == "Next2BusinessDays" ? "selected" : "") . ">Next 2 Business Days</option>
                                    <option value='Next3Days' " . ($data_ref == "Next3Days" ? "selected" : "") . ">Next 3 Days</option>
                                    <option value='Next3BusinessDays' " . ($data_ref == "Next3BusinessDays" ? "selected" : "") . ">Next 3 Business Days</option>
                                    <option value='Next4Days' " . ($data_ref == "Next4Days" ? "selected" : "") . ">Next 4 Days</option>
                                    <option value='Next4BusinessDays' " . ($data_ref == "Next4BusinessDays" ? "selected" : "") . ">Next 4 Business Days</option>
                                    <option value='Next5Days' " . ($data_ref == "Next5Days" ? "selected" : "") . ">Next 5 Days</option>
                                    <option value='Next5BusinessDays' " . ($data_ref == "Next5BusinessDays" ? "selected" : "") . ">Next 5 Business Days</option>
                                    <option value='ThisWeek' " . ($data_ref == "ThisWeek" ? "selected" : "") . ">This Week</option>
                                    <option value='ThisMonth' " . ($data_ref == "ThisMonth" ? "selected" : "") . ">This Month</option>
                                    <option value='Yesterday' " . ($data_ref == "Yesterday" ? "selected" : "") . ">Yesterday</option>
                                    <option value='LastBusinessDay' " . ($data_ref == "LastBusinessDay" ? "selected" : "") . ">Last Business Days</option>
                                    <option value='Last2Days' " . ($data_ref == "Last2Days" ? "selected" : "") . ">Last 2 Days</option>
                                    <option value='Last2BusinessDays' " . ($data_ref == "Last2BusinessDays" ? "selected" : "") . ">Last 2 Business Days</option>
                                    <option value='Last3Days' " . ($data_ref == "Last3Days" ? "selected" : "") . ">Last 3 Days</option>
                                    <option value='Last3BusinessDays' " . ($data_ref == "Last3BusinessDays" ? "selected" : "") . ">Last 3 Business Days</option>
                                    <option value='Last4Days' " . ($data_ref == "Last4Days" ? "selected" : "") . ">Last 4 Days</option>
                                    <option value='Last4BusinessDays' " . ($data_ref == "Last4BusinessDays" ? "selected" : "") . ">Last 4 Business Days</option>
                                    <option value='Last5Days' " . ($data_ref == "Last5Days" ? "selected" : "") . ">Last 5 Days</option>
                                    <option value='Last5BusinessDays' " . ($data_ref == "Last5BusinessDays" ? "selected" : "") . ">Last 5 Business Days</option>
                                    <option value='LastWeek' " . ($data_ref == "LastWeek" ? "selected" : "") . ">Last Week</option>
                                    <option value='LastMonth' " . ($data_ref == "LastMonth" ? "selected" : "") . ">Last Month</option>
                                    <option value='Q1' " . ($data_ref == "Q1" ? "selected" : "") . ">Q1 " . date("Y") . "</option>
                                    <option value='Q2' " . ($data_ref == "Q2" ? "selected" : "") . ">Q2 " . date("Y") . "</option>
                                    <option value='Q3' " . ($data_ref == "Q3" ? "selected" : "") . ">Q3 " . date("Y") . "</option>
                                    <option value='Q4' " . ($data_ref == "Q4" ? "selected" : "") . ">Q4 " . date("Y") . "</option>
                                    <option value='H1' " . ($data_ref == "H1" ? "selected" : "") . ">H1 " . date("Y") . "</option>
                                    <option value='H2' " . ($data_ref == "H2" ? "selected" : "") . ">H2 " . date("Y") . "</option>
                                    <option value='YTD' " . ($data_ref == "YTD" ? "selected" : "") . ">YTD</option>
                                </select>&nbsp;between&nbsp;<input type='text' value='" . $from . "' style='width:80px' class='showCal' id='StartDate" . $key . "'>
                            &nbsp;and&nbsp;<input type='text' value='" . $to . "' style='width:80px' class='showCal' id='EndDate" . $key . "'>";
    return $str;
}

function loadDropdown($ComID, $isPM, $fieldCode, $SelectMultiple)
{
    $CusReportClass = new Core_Api_CusReportClass();
    $SelectMultiple = $SelectMultiple;
    $DropdownOptions = $CusReportClass->getFilterDropdownOptions($fieldCode, $ComID, $isPM);
    $str .="<select sectionid='$fieldCode' class='DropdownBoxchange' style='min-width:155px' id='$fieldCode'>";
    if (!empty($DropdownOptions) && count($DropdownOptions))
    {
        foreach ($DropdownOptions as $option)
        {
            $value = $option['Value'];
            $caption = $option['Caption'];
            $display = 1;
            if (!empty($SelectMultiple) && count($SelectMultiple))
            {
                foreach ($SelectMultiple as $selValue)
                {
                    if ($selValue == $value)
                    {
                        $display = 0;
                    }
                }
            }
            $str .= "<option value='$value' style='" . ($display == 1 ? "" : "display:none;") . "' " . (!empty($option["Disable"]) ? $option["Disable"] : "") . ">$caption</option>";
        }
    }
    $str.="</select><br/>";
    if (!empty($SelectMultiple) && count($SelectMultiple))
    {
        foreach ($SelectMultiple as $selValue)
        {
            $selKey = $selValue;
            $option = $DropdownOptions["$selKey"];
            $caption = $option['Caption'];
            $str.="
                <span>
                    <img height='15px' id='$fieldCode" . "__$fieldCode" . "__$selKey' valstr='$caption' style='cursor:pointer;' src='/widgets/css/images/removedoc.png' class='cmdremovedropdownitem'>&nbsp;&nbsp;$caption
                    <br>
                </span>";
        }
    }
    return $str;
}

function loadDYN($fieldCode, $SelectMultiple)
{
    $SelectMultiple = $SelectMultiple;
    $DropdownOptions = array("t" => "Yes", "f" => "No", "a" => "Any");
    $str .="<select sectionid='$fieldCode' class='' style='min-width:155px' id='$fieldCode'>";
    foreach ($DropdownOptions as $key => $val)
    {
        $str .= "<option value='$key' " . ($SelectMultiple[0] == $key ? "selected" : "") . ">$val</option>";
    }
    $str.="</select><br/>";
    return $str;
}

function loadtext($fieldCode, $stringValue)
{
    $str = "<input type='text' style='width:155px' id='$fieldCode' value='$stringValue'>";
    return $str;
}

function loadN2($fieldCode, $numFrom, $numTo)
{
    $str = "from&nbsp;<input class='txtN2' type='text' id='" . $fieldCode . "__from' " . $fieldCode . "_type='N2' style='width: 50px;" . (empty($numFrom) ? "color:#A6A6A6;" : "") . "'  value='" . (empty($numFrom) ? "#.##" : $numFrom) . "' />&nbsp;to&nbsp;
                <input class='txtN2' type='text' id='" . $fieldCode . "__to' " . $fieldCode . "_type='N2' style='width: 50px;" . (empty($numTo) ? "color:#A6A6A6;" : "") . "' value='" . (empty($numTo) ? "#.##" : $numTo) . "' />";
    return $str;
}

function loadT2($fieldCode, $timeFrom, $timeTo, $time_tz)
{
    $str = "from <select  style='width:100px' id='" . $fieldCode . "__from' " . $fieldCode . "_type='T2'> " . getT2options($timeFrom) . "</select>
            &nbsp;&nbsp;&nbsp;to <select  style='width:100px' id='" . $fieldCode . "__to' " . $fieldCode . "_type='T2'> " . getT2options($timeTo) . "</select>
            &nbsp;&nbsp;&nbsp;<select id='" . $fieldCode . "est' style='width:150px;'>
                                <option value ='-10' " . ($time_tz == "-10" ? "selected" : "") . ">Hawaii (GMT-10:00)</option>
                                <option value ='-9' " . ($time_tz == "-9" ? "selected" : "" ) . ">Alaska (GMT-8:00)</option>
                                <option value ='-8' " . ($time_tz == "-8" ? "selected" : "") . ">Pacific (GMT-7:00)</option>
                                <option value ='-7' " . ($time_tz == "-7" ? "selected" : "") . ">Mountain (GMT-6:00)</option>
                                <option value ='-6' " . ($time_tz == "-6" ? "selected" : "") . ">Central (GMT-5:00)</option>
                                <option value ='-5' " . ($time_tz == "-5" ? "selected" : "") . ">Eastern (GMT-4:00)</option>
                            </select>";
    return $str;
}

function getT2options($selValue)
{
    $timepart3 = 0;
    $timepart2 = "";
    $timepart1 = "";
    $Time = "";
    $Time .= "<option value=''>Select Time</option>";
    for ($i = 0; $i < 24; $i++)
    {
        for ($j = 1; $j < 5; $j++)
        {
            if ($j == 1)
                $timepart2 = "00";
            else
                $timepart2 = ($j - 1) * 15;

            if ($i < 12 || $i == 24)
                $timepart3 = "AM";
            else
                $timepart3 = "PM";

            if ($i == 0)
                $timepart1 = "12";
            else if ($i < 13 && $i > 0)
                $timepart1 = $i;
            else
                $timepart1 = $i - 12;

            $timestamp = $timepart1 . ":" . $timepart2 . " " . $timepart3;

            $selected = '';
            if ($selValue == $timestamp)
            {
                $selected = "selected='selected'";
            }
            $Time .= "<option value='" . $timestamp . "' $selected >" . $timestamp . "</option>";
        }
    }
    return $Time;
}

function buildcontrol($filterType, $ComID, $isPM, $fieldCode, $fieldvalue)
{
    switch ($filterType)
    {
        case "D":
            return loadDropdown($ComID, $isPM, $fieldCode, $fieldvalue['SelectMultiple']);
            break;
        case "T":
            return loadtext($fieldCode, $fieldvalue['string_value']);
            break;
        case "DT":
            return loaddatetime($fieldCode, $fieldvalue["datetime_relative_data_ref"], $fieldvalue["datetime_range_from"], $fieldvalue["datetime_range_to"]);
            break;
        case "N2":
            return loadN2($fieldCode, $fieldvalue['numeric_range_from'], $fieldvalue['numeric_range_to']);
            break;
        case "DYN":
            return loadDYN($fieldCode, $fieldvalue['SelectMultiple']);
            break;
        case "T2":
            return loadT2($fieldCode, $fieldvalue['time_from'], $fieldvalue['time_to'], empty($fieldvalue['time_tz']) ? "Central" : $fieldvalue['time_tz']);
            break;
    }
}

$FieldOrder = array(
    "wostatus" => array(
        "created" => "0",
        "published" => "1",
        "assigned" => "2",
        "work_done" => "3",
        "incomplete" => "4",
        "approved" => "5",
        "in_accounting" => "6",
        "completed" => "7",
        "deactivated" => "8"),
    "fieldwoInfo" => array(
        "WIN_NUM" => "0",
        "WO_ID" => "1",
        "Status" => "2",
        "Project_Name" => "3",
        "Project_ID" => "4",
        "PO" => "5",
        "WorkOrderOwner" => "6",
        "Type_ID" => "7",
        "WO_Category_ID" => "8",
        "_AccountManager" => "9",
        "_ClientServicesDirector" => "10",
        "_ServiceType" => "11",
        "_LegalEntity" => "12",
        "Headline" => "13",
        "Description" => "14",
        "Requirements" => "15",
        "SpecialInstructions" => "16",
        "Ship_Contact_Info" => "17",
        "Qty_Devices" => "18",
        "Lead" => "19",
        "Assist" => "20",
        "Update_Reason" => "21",
        "Update_Requested" => "22",
        "ShortNotice" => "23"),
    "fieldwoDates" => array(
        "_VisitStartTypeID" => "0",
        "_VisitStartDate" => "1",
        "_VisitStartTime" => "2",
        "_VisitEndDate" => "3",
        "_VisitEndTime" => "4",
        "_VisitSiteTimeZone" => "5",
        "_VisitDayPart" => "6",
        "_VisitLeadTime" => "7",
        "_VisitEstimatedDuration" => "8",
        "Qty_Visits" => "9",
        "_VisitActualCheckInDate" => "10",
        "_VisitActualCheckInTime" => "11",
        "_VisitActualCheckInResult" => "12",
        "_VisitActualCheckOutDate" => "13",
        "_VisitActualCheckOutTime" => "14",
        "_VisitActualCheckOutResult" => "15",
        "_VisitActualCalculatedDuration" => "16",
        "_VisitActualDurationResult" => "17"),
    "fieldsite" => array(
        "SiteNumber" => "0",
        "SiteName" => "1",
        "Address" => "2",
        "City" => "3",
        "State" => "4",
        "Zipcode" => "5",
        "Country" => "6",
        "_Geography" => "7",
        "Region" => "8",
        "Route" => "9",
        "Latitude" => "10",
        "Longitude" => "11",
        "Site_Contact_Name" => "12",
        "SiteEmail" => "13",
        "SitePhone" => "14",
        "SiteFax" => "15"),
    "fieldbid" => array(
        "_TechID" => "0",
        "_BiddingTechFLSID" => "1",
        "_Tech_Fname" => "2",
        "_Tech_Lname" => "3",
        "_BiddingTechEmail" => "4",
        "_BiddingTechPhone" => "5",
        "_BiddingTechPreferred" => "6",
        "_BidPayMax" => "7",
        "_BidAmount_Per" => "8",
        "_BidAmount" => "9",
        "_Comments" => "10",
        "_Bid_Date" => "11",
        "_Hide" => "12",
        "Qty_Applicants" => "13"),
    "fieldtechnicians" => array(
        "Tech_ID" => "0",
        "FLS_ID" => "1",
        "Tech_FName" => "2",
        "Tech_LName" => "3",
        "_PrimaryEmail" => "4",
        "_SecondaryEmail" => "5",
        "_PrimaryPhone" => "6",
        "_SecondaryPhone" => "7",
        "_AssignedTechPreferred" => "8",
        "_AssignedTechDenied" => "9",
        "_TechBidAmount" => "10",
        "_TechAmount_Per" => "11",
        "bid_comments" => "12",
        "_TechBid_Date" => "13",
        "_TechBackedOut" => "14",
        "BackOut_Tech" => "15",
        "_BackOutFLSTechID" => "16",
        "_TechNoShowed" => "17",
        "NoShow_Tech" => "18",
        "_NoShowFLSTechID" => "19",
        "BU1FLSID" => "20",
        "Backup_FLS_ID" => "21",
        "BU1Info" => "22"),
    "fieldCommunications" => array(
        "_EmailBlastRadius" => "0",
        "ProjectManagerName" => "1",
        "ProjectManagerEmail" => "2",
        "ProjectManagerPhone" => "3",
        "ResourceCoordinatorName" => "4",
        "ResourceCoordinatorEmail" => "5",
        "ResourceCoordinatorPhone" => "6",
        "_ACSNotifiPOC" => "7",
        "_ACSNotifiPOC_Email" => "8",
        "_ACSNotifiPOC_Phone" => "9",
        "EmergencyName" => "10",
        "EmergencyEmail" => "11",
        "EmergencyPhone" => "12",
        "CheckInOutName" => "13",
        "CheckInOutEmail" => "14",
        "CheckInOutNumber" => "15",
        "TechnicalSupportName" => "16",
        "TechnicalSupportEmail" => "17",
        "TechnicalSupportPhone" => "18"),
    "fieldconfirmation" => array(
        "WorkOrderReviewed" => "0",
        "TechCheckedIn_24hrs" => "1",
        "StoreNotified" => "2",
        "Notifiedby" => "3",
        "NotifyNotes" => "4",
        "PreCall_1" => "5",
        "PreCall_1_Status" => "6",
        "PreCall_2" => "7",
        "PreCall_2_Status" => "8",
        "CheckedIn" => "9",
        "Checkedby" => "10",
        "CheckInNotes" => "11",
        "Incomplete_Paperwork" => "12",
        "Paperwork_Received" => "13",
        "MissingComments" => "14",
        "ClientComments" => "15",
        "SATRecommended" => "16",
        "SATPerformance" => "17",
        "TechMarkedComplete" => "18",
        "CallClosed" => "19",
        "ContactHD" => "20",
        "HDPOC" => "21",
        "Unexpected_Steps" => "22",
        "Unexpected_Desc" => "23",
        "AskedBy" => "24",
        "AskedBy_Name" => "25",
        "FLS_OOS" => "26",
        "Penalty_Reason" => "27",
        "TechMiles" => "28",
        "TechComments" => "29"),
    "wintag" => array(
        "_WINTagAdded" => "0",
        "_TotalNumOfWINTags" => "1",
        "_WINTagAddedDate" => "2",
        "_WINTagAddedBy" => "3",
        /*
          "_WINTagComplete" => "4",
          "_WINTagIncomplete" => "5",
          "_WINTagRescheduled" => "6",
          "_WINTagSiteCancelled" => "7",
         */
        "_WINTag" => "4",
        "_WINTagOwner" => "5",
        "_WINTagDetails" => "6",
        "_WINTagNotes" => "7",
        "_WINTagBillable" => "8",
        "_WINTagCheckInResult" => "9",
        "_WINTagCheckOutResult" => "10",
        "_WINTagDurationResult" => "11",
        "_WINTagTechPreferenceRating" => "12",
        "_WINTagTechPerformanceRating" => "13"),
    "partmanager" => array(
        "_NumofNewParts" => "0",
        "_NumofReturnParts" => "1",
        "_PartManagerUpdatedByTech" => "2",
        "_NewPartPartNum" => "3",
        "_NewPartModelNum" => "4",
        "_NewPartSerialNum" => "5",
        "_NewPartEstArrival" => "6",
        "_NewPartCarrier" => "7",
        "_NewPartTrackingNum" => "8",
        "_NewPartInfo" => "9",
        "_NewPartShipsTo" => "10",
        "_NewPartShippingNotes" => "11",
        "_NewPartisConsumable" => "12",
        "_ReturnPartPartNum" => "13",
        "_ReturnPartModelNum" => "14",
        "_ReturnPartSerialNum" => "15",
        "_ReturnPartEstArrial" => "16",
        "_ReturnPartCarrier" => "17",
        "_ReturnPartTrackingNum" => "18",
        "_ReturnPartInfo" => "19",
        "_ReturnPartRMA" => "20",
        "_ReturnPartInstructions" => "21"),
    "fieldPricing" => array(
        "PayMax" => "0",
        "Tech_Bid_Amount" => "1",
        "Amount_Per" => "2",
        "calculatedTechHrs" => "3",
        "baseTechPay" => "4",
        "Additional_Pay_Amount" => "5",
        "OutofScope_Amount" => "6",
        "TripCharge" => "7",
        "MaterialsReimbursement" => "8",
        "MileageReimbursement" => "9",
        "AbortFee" => "10",
        "AbortFeeAmount" => "11",
        "Total_Reimbursable_Expense" => "12",
        "Add_Pay_AuthBy" => "13",
        "Add_Pay_Reason" => "14",
        "Approved" => "15",
        "approvedBy" => "16",
        "PayAmount" => "17",
        "PricingRuleID" => "18",
        "PricingCalculationText" => "19",
        "PricingRuleApplied" => "20",
        "PricingRan" => "21",
        "Invoiced" => "22",
        "calculatedInvoice" => "23",
        "PcntDeduct" => "24",
        "PcntDeductPercent" => "25",
        "Net_Pay_Amount" => "26",
        "TechPaid" => "27",
        "FS_InternalComments" => "28"),
    "fieldadditional" => array(
        "DateEntered" => "0",
        "Username" => "1",
        "_PublishedDate" => "2",
        "_PublishedBy" => "3",
        "SourceByDate" => "4",
        "Date_Assigned" => "5",
        "_AssignedBy" => "6",
        "DateNotified" => "7",
        "Notifiedby" => "8",
        "Date_Completed" => "9",
        "DateIncomplete" => "10",
        "_MarkedIncompleteBy" => "11",
        "DateApproved" => "12",
        "_approvedBy" => "13",
        "DateInvoiced" => "14",
        "DatePaid" => "15",
        "Deactivated" => "16",
        "DeactivatedDate" => "17",
        "DeactivatedBy" => "18",
        "DeactivationCode" => "19",
        "Deactivated_Reason" => "20",
        "lastModDate" => "21",
        "lastModBy" => "22")
);
$weekdays = array("1" => "Monday", "2" => "Tuesday", "3" => "Wednesday", "4" => "Thursday", "5" => "Friday", "6" => "Saturday", "7" => "Sunday");
$Monthdayorder = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "Last Day");
$MonthlyDayOrder = array("1" => "1<sup>st</sup>", "2" => "2<sup>nd</sup>", "3" => "3<sup>rd</sup>", "4" => "4<sup>th</sup>");
$disabled = "";
$RRDEnable = "";
$RRDEmailTo = "Use &ldquo;,&rdquo; to separate multiple email addresses";
$RRDEmailToColor = "color:#a6a6a6;";
$RRDEmailFromName = "FS-ReportWriter";
$RRDEmailFrom = "FS-ReportWriter@fieldsolutions.com";
$RRDEmailSubject = "";
$RRDEmailMessage = "";
$RRDEmailSchedule = "Daily";
$RRDTimeOfDay = "9:00 AM";
$RRDTimeZone = "-5";
$RRDDailyOptions = 1;
$RRDDailyEveryDaySpaceOutNum = 1;
$RRDWeeklyDays = array("1");
$RRDMonthlyScheduleOptions = 1;
$RRDMonthlyScheduleDayNum = 1;
$RRDMonthlyScheduleDayOrder = 1;
$RRDMonthlyScheduleDayOrderDay = 1;
$RRDWeeklySpaceOutNum = 1;
$RRDMonthlyDayNumSpaceOutNum = 1;
$RRDMonthlyDayOrderSpaceOutNum = 1;

if (!empty($reportid))
{

    $PrjectList = $CusReportClass->getPrjectList_ByCompanyID($ComID);
    $WOStatusList = $CusReportClass->getWOStatusList();
    $FilterList = $CusReportClass->getFilterList2($reportid);

    $ReportColumns = $CusReportClass->getReportFieldList2($reportid);
    if (!empty($rptcopy))
    {
        $reportid = "";
    } else
    {
        $ReportInfo = $CusReportClass->getReportInfo2($reportid);
    }
    if (empty($rptcopy))
    {
        if (!empty($ReportInfo['RRDEmailSubject']))
        {
            if (trim($ReportInfo['RRDEmailSubject']) == "FieldSolutions Report:")
            {
                $RRDEmailSubject = "FieldSolutions Report: " . $ReportInfo['rpt_name'];
            } else
            {
                $RRDEmailSubject = $ReportInfo['RRDEmailSubject'];
            }
        }

        $RRDEnable = $ReportInfo['RRDEnable'] == "1" ? "checked" : "";
        $disabled = $ReportInfo['RRDEnable'] == "1" ? "" : "disabled";
        $RRDEmailTo = !empty($ReportInfo['RRDEmailTo']) ? $ReportInfo['RRDEmailTo'] : "Use &ldquo;,&rdquo; to separate multiple email addresses";
        $RRDEmailToColor = !empty($ReportInfo['RRDEmailTo']) ? "" : "color:#a6a6a6;";
        $RRDEmailFromName = (!empty($ReportInfo['RRDEmailFromName'])) ? $ReportInfo['RRDEmailFromName'] : "FieldSolutions";
        $RRDEmailFrom = (!empty($ReportInfo['RRDEmailFrom'])) ? $ReportInfo['RRDEmailFrom'] : "no-replies@fieldsolutions.com";

        $RRDEmailMessage = (!empty($ReportInfo['RRDEmailMessage'])) ? $ReportInfo['RRDEmailMessage'] : "";
        $RRDEmailSchedule = $ReportInfo['RRDEmailSchedule'];
        $RRDTimeOfDay = $ReportInfo['RRDTimeOfDay'];
        $RRDTimeZone = $ReportInfo['RRDTimeZone'];
        $RRDWeeklyDays = explode(",", $ReportInfo['RRDWeeklyDays']);
        $RRDMonthlyScheduleOptions = $ReportInfo['RRDMonthlyScheduleOptions'];
        $RRDMonthlyScheduleDayNum = $ReportInfo['RRDMonthlyScheduleDayNum'];
        $RRDMonthlyScheduleDayOrder = $ReportInfo['RRDMonthlyScheduleDayOrder'];
        $RRDMonthlyScheduleDayOrderDay = $ReportInfo['RRDMonthlyScheduleDayOrderDay'];

        $RecurringReportStatusLabel = "";
//14100
        $RRDMonthlyDayOrderSpaceOutNum = $ReportInfo['RRDMonthlyDayOrderSpaceOutNum'];
        $RRDMonthlyDayNumSpaceOutNum = $ReportInfo['RRDMonthlyDayNumSpaceOutNum'];
        $RRDWeeklySpaceOutNum = $ReportInfo['RRDWeeklySpaceOutNum'];
        $RRDDailyOptions = $ReportInfo['RRDDailyOptions'];
//        $RRDDailyEveryWeekday = $ReportInfo['RRDDailyEveryWeekday'];
        $RRDDailyEveryDaySpaceOutNum = $ReportInfo['RRDDailyEveryDaySpaceOutNum'];

        if ($RRDEmailSchedule === "Daily")
        {
            if ($RRDDailyOptions == 1)
            {
                if ($RRDDailyEveryDaySpaceOutNum > 1)
                {
                    $RecurringReportStatusLabel = "Every " . $RRDDailyEveryDaySpaceOutNum . " Days";
                } else
                {
                    $RecurringReportStatusLabel = $RRDEmailSchedule;
                }
            } else
            {
                $RecurringReportStatusLabel = "Weekdays";
            }
        } else if ($RRDEmailSchedule === "Weekly")
        {
            $RecurringReportStatusLabel = $RRDEmailSchedule;
            if ($RRDWeeklySpaceOutNum > 1)
            {
                $RecurringReportStatusLabel = "Every " . $RRDWeeklySpaceOutNum . " Weeks";
            } else if ($RRDWeeklyDays !== "")
            {
                $RecurringReportStatusLabel = $RRDEmailSchedule;
            }
        } else
        {
            if ($RRDMonthlyScheduleOptions == 1)
            {
                if ($RRDMonthlyDayNumSpaceOutNum > 1)
                {
                    $RecurringReportStatusLabel = "Every " . $RRDMonthlyDayNumSpaceOutNum . " Months";
                } else
                {
                    if ($RRDMonthlyScheduleDayNum == "Last Day")
                    {
                        $RecurringReportStatusLabel = $RRDEmailSchedule . " - " . $RRDMonthlyScheduleDayNum;
                    } else
                    {
                        $RecurringReportStatusLabel = $RRDEmailSchedule . " - Day " . $RRDMonthlyScheduleDayNum;
                    }
                }
            } else
            {
                if ($RRDMonthlyDayOrderSpaceOutNum > 1)
                {
                    $RecurringReportStatusLabel = "Every " . $RRDMonthlyDayOrderSpaceOutNum . " Months";
                } else
                {
                    $RRDMonthlyScheduleDayOrderlabel = "";
                    switch ($RRDMonthlyScheduleDayOrder)
                    {
                        case 1:
                            $RRDMonthlyScheduleDayOrderlabel = "1<sup>st</sup>";
                            break;
                        case 2:
                            $RRDMonthlyScheduleDayOrderlabel = "2<sup>nd</sup>";
                            break;
                        case 3:
                            $RRDMonthlyScheduleDayOrderlabel = "3<sup>rd</sup>";
                            break;
                        case 4:
                            $RRDMonthlyScheduleDayOrderlabel = "4<sup>th</sup>";
                            break;
                    }
                    $RecurringReportStatusLabel = $RRDEmailSchedule . " - " . $RRDMonthlyScheduleDayOrderlabel . " " . $weekdays[$RRDMonthlyScheduleDayOrderDay];
                }
            }
        }
    }

    $WorkOrderStatusColumns = array();
    $BasicInformationColumns = array();
    $DatesTimesColumns = array();
    $SiteInformationColumns = array();
    $BidInformationColumns = array();
    $TechniciansColumns = array();
    $CommunicationsColumns = array();
    $ConfirmationsCloseOutColumns = array();
    $PartsManagerColumns = array();
    $WinTagsColumns = array();
    $PricingPayDetailsColumns = array();
    $AdditionalInformationColumns = array();

    foreach ($ReportColumns as $key => $val)
    {
        $location = $val['Location'];
        if ($location == Core_Api_CusReportClass::FieldFilter_Location_WO_Work_Order_Status)
        {
            $WorkOrderStatusColumns[$key] = $val;
        } else if ($location == Core_Api_CusReportClass::FieldFilter_Location_WO_Basic_Information)
        {
            $BasicInformationColumns[$key] = $val;
        } else if ($location == Core_Api_CusReportClass::FieldFilter_Location_WO_Dates_Times)
        {
            $DatesTimesColumns[$key] = $val;
        } else if ($location == Core_Api_CusReportClass::FieldFilter_Location_WO_Site_Information)
        {
            $SiteInformationColumns[$key] = $val;
        } else if ($location == Core_Api_CusReportClass::FieldFilter_Location_WO_Bid_Information)
        {
            $BidInformationColumns[$key] = $val;
        } else if ($location == Core_Api_CusReportClass::FieldFilter_Location_WO_Technicians)
        {
            $TechniciansColumns[$key] = $val;
        } else if ($location == Core_Api_CusReportClass::FieldFilter_Location_WO_Communications)
        {
            $CommunicationsColumns[$key] = $val;
        } else if ($location == Core_Api_CusReportClass::FieldFilter_Location_WO_Confirmations_Close_Out)
        {
            $ConfirmationsCloseOutColumns[$key] = $val;
        } else if ($location == Core_Api_CusReportClass::FieldFilter_Location_WO_PartsManager)
        {
            $PartsManagerColumns[$key] = $val;
        } else if ($location == Core_Api_CusReportClass::FieldFilter_Location_WO_WinTags)
        {
            $WinTagsColumns[$key] = $val;
        } else if ($location == Core_Api_CusReportClass::FieldFilter_Location_WO_Pricing_Pay_Details)
        {
            $PricingPayDetailsColumns[$key] = $val;
        } else if ($location == Core_Api_CusReportClass::FieldFilter_Location_WO_Additional_Information)
        {
            $AdditionalInformationColumns[$key] = $val;
        }
    }
} else
{
    $BasicInformationColumnstemp = $CusReportClass->getBasicWOInformationDefaultFields();
    $temparr = array();
    foreach ($BasicInformationColumnstemp as $key => $val)
    {
        $temparr[$key] = array("field_title" => $val);
    }
    $BasicInformationColumns = $temparr;
}
?>
<script type="text/javascript" src="js/custom_report.js"></script>
<link rel="stylesheet" href="../../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript" src="../../library/jquery/jquery.bt.min.js"></script>
<style>
    .section
    {
        border-bottom: 1px solid #F69630;
        padding-top: 17px;
        margin-left: 15px;
        cursor: pointer;
    }
    .section a.title {
        color: black;
        font-size: 12px;
        font-weight: bold;
    }
    .divopen
    {
        padding-top: 17px;
        background: url('../../widgets/images/triangle_down.gif') no-repeat scroll 0px 17px transparent;
        padding-left: 16px;
        margin-left: -15px;
    }

    .divclose
    {
        padding-top: 17px;
        background: url('../../widgets/images/triangle_up.gif') no-repeat scroll 0px 17px transparent;
        padding-left: 16px;
        margin-left: -15px;
    }

    ul.listitem
    {
        width: 100%;
        overflow: hidden;
    }
    li.listitem
    {
        float: left;
        display: inline;
        line-height: 1.5em;
        width: 50%;
    }
</style>

<script>
    var currYr = "<?= date("Y") ?>";
    var Tomorrow = <?= "\"" . $Tomorrow . "\""; ?>;
    var Yesterday = <?= "\"" . $Yesterday . "\""; ?>;
    var LastDayend = <?= "\"" . $LastDayend . "\""; ?>;
    var NextDaystart = <?= "\"" . $NextDaystart . "\""; ?>;
    var Nextbusinessday = <?= "\"" . $NextBusinessDay . "\""; ?>;
    var Next2days = <?= "\"" . $Next2days . "\""; ?>;
    var Next3days = <?= "\"" . $Next3days . "\""; ?>;
    var Next4days = <?= "\"" . $Next4days . "\""; ?>;
    var Next5days = <?= "\"" . $Next5days . "\""; ?>;
    var Next2businessdays = <?= "\"" . $Next2Businessdays . "\""; ?>;
    var Next3businessdays = <?= "\"" . $Next3Businessdays . "\""; ?>;
    var Next4businessdays = <?= "\"" . $Next4Businessdays . "\""; ?>;
    var Next5businessdays = <?= "\"" . $Next5Businessdays . "\""; ?>;

    var Lastbusinessday = <?= "\"" . $LastBusinessDay . "\""; ?>;
    var Last2days = <?= "\"" . $Last2days . "\""; ?>;
    var Last3days = <?= "\"" . $Last3days . "\""; ?>;
    var Last4days = <?= "\"" . $Last4days . "\""; ?>;
    var Last5days = <?= "\"" . $Last5days . "\""; ?>;

    var Last2businessdays = <?= "\"" . $Last2Businessdays . "\""; ?>;
    var Last3businessdays = <?= "\"" . $Last3Businessdays . "\""; ?>;
    var Last4businessdays = <?= "\"" . $Last4Businessdays . "\""; ?>;
    var Last5businessdays = <?= "\"" . $Last5Businessdays . "\""; ?>;

    var Today = <?= "\"" . $Today . "\""; ?>;
    var BOW = <?= "\"" . $BOW . "\""; ?>;
    var EOW = <?= "\"" . $EOW . "\""; ?>;
    var BOLW = <?= "\"" . $BOLW . "\""; ?>;
    var EOLW = <?= "\"" . $EOLW . "\""; ?>;
    var BOM = <?= "\"" . $BOM . "\""; ?>;
    var EOM = <?= "\"" . $EOM . "\""; ?>;
    var PrevBOM = <?= "\"" . $PrevBOM . "\""; ?>;
    var PrevEOM = <?= "\"" . $PrevEOM . "\""; ?>;
    var Q1Start = <?= "\"" . $Q1Start . "\""; ?>;
    var Q1End = <?= "\"" . $Q1End . "\""; ?>;
    var Q2Start = <?= "\"" . $Q2Start . "\""; ?>;
    var Q2End = <?= "\"" . $Q2End . "\""; ?>;
    var Q3Start = <?= "\"" . $Q3Start . "\""; ?>;
    var Q3End = <?= "\"" . $Q3End . "\""; ?>;
    var Q4Start = <?= "\"" . $Q4Start . "\""; ?>;
    var Q4End = <?= "\"" . $Q4End . "\""; ?>;
    var H1Start = <?= "\"" . $H1Start . "\""; ?>;
    var H1End = <?= "\"" . $H1End . "\""; ?>;
    var H2Start = <?= "\"" . $H2Start . "\""; ?>;
    var H2End = <?= "\"" . $H2End . "\""; ?>;
    var YTDStart = <?= "\"" . $YTDStart . "\""; ?>;
    var YTDEnd = <?= "\"" . $YTDEnd . "\""; ?>;
    var _custom_report = null;

    jQuery(window).ready(function() {
        var _custom_report = custom_report.CreateObject({
            id: '_custom_report',
            company: '<?= $_GET["v"] ?>',
            report_id: '<?= $reportid ?>',
            FieldOrder: <?= json_encode($FieldOrder) ?>
        });
        _custom_report.init();
    });

    function showhidegroup(el)
    {
        var id = $(el).attr("groupid");
        if ($("#logicaldatagroup" + id).css("display") === "none")
        {
            $("#logicaldatagroup" + id).css("display", "");
            $("#group" + id).attr("src", "/widgets/images/arrows-up5.png");
        }
        else
        {
            $("#logicaldatagroup" + id).css("display", "none");
            $("#group" + id).attr("src", "/widgets/images/arrows-down5.png");
        }
    }
    ;
</script>

<div style='padding-left:10px;padding-right:10px; width:97%'>
    <input type="hidden" id="reportid" value="<?= $reportid ?>">
    <input type="hidden" id="companyid" value="<?= $_GET["v"] ?>">
    <input type="hidden" id="templatename" value="<?= $ReportInfo["rpt_name"] ?>">
    <div style="padding-left:5px;">
        <a href="reportlist.php?v=<?= $_GET["v"] ?>">Back to List</a>
    </div>
    <center>
        <br/>
        <h4>
            <?= empty($reportid) ? "Create New" : "Edit" ?> Report
        </h4>
        <input type="hidden" id="hidComID" value="<?= $_GET["v"] ?>">
    </center>
    <div style="float: left; width: 100%; padding-bottom: 20px;">
        <table cellpadding='0' cellspacing="0" width="100%">
            <tr>
                <td width="160px;;" align="left" style="vertical-align:top;">
                    Report Name:
                </td>
                <td  colspan="2" align="left" style="padding-bottom:5px;">
                    <input value="<?= $ReportInfo["rpt_name"] ?>" type="text" name="reportname" id="reportname" style="width:138px;" />
                </td>
            </tr>
            <tr>
                <td align="left" style="vertical-align:top;">
                    Recurring Report Delivery:
                </td>
                <td  colspan="2" align="left" style="padding-bottom:5px;">
                    <?
                    if ($ReportInfo['RRDEnable'] == 1)
                    {
                        ?>
                        <span id="RecurringReportStatus"><?= $RecurringReportStatusLabel ?></span>&nbsp;(<a id="cmdRecurringReportStatus" href="javascript:;" isenable="0">edit</a>)
                        <?
                    } else
                    {
                        ?>
                        <span id="RecurringReportStatus">Disabled</span>&nbsp;(<a id="cmdRecurringReportStatus" href="javascript:;" isenable="0">Enable</a>)
                        <?
                    }
                    ?>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <div id="divColoumns" style="border: 2px solid #F69630;width:100%;padding-left:5px;padding-right:5px;display:none;">
                        <div class="section">
                            <a class="title <?= !empty($reportid) ? "divopen" : "divclose" ?>" title="Collapse Section" isopen="<?= !empty($reportid) ? "0" : "1" ?>" section="ReportType">
                                <b>Select Report Type</b>
                            </a>
                        </div>
                        <div id="divReportType" style="<?= !empty($reportid) ? "display:none;" : "" ?> padding-left:15px;">
                            <?
                            if (empty($ReportInfo["report_type"]))
                            {
                                $report_type = 1;
                            } else
                            {
                                $report_type = $ReportInfo["report_type"];
                            }
                            ?>
                            <table id="tdReportType" border="0" cellspacing="0" cellpadding="0" width="100%">
                                <col width="20px">
                                <col width="*">
                                <tr>
                                    <td>
                                        <input class="rptType" id="rptWo" value="1" type="radio" name="reportType" <?= $report_type == "1" ? "checked" : "" ?>>
                                    </td>
                                    <td>
                                        Work Order Report<span style="color:#A6A6A6;"> - one row per Work Order</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input class="rptType" id="rptBid" value="2" type="radio" name="reportType" section="fieldbid" <?= $report_type == "2" ? "checked" : "" ?>>
                                    </td>
                                    <td>
                                        Bids Report<span style="color:#A6A6A6;"> - one row per Bid</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input class="rptType" id="rptWintag" value="3" type="radio" name="reportType" section="wintag" <?= $report_type == "3" ? "checked" : "" ?>>
                                    </td>
                                    <td>
                                        WIN-Tags&trade; Report<span style="color:#A6A6A6;"> - one row per WIN-Tag&trade;</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input class="rptType" id="rptPart" value="4" type="radio" name="reportType" section="partmanager" <?= $report_type == "4" ? "checked" : "" ?>>
                                    </td>
                                    <td>
                                        Parts Report<span style="color:#A6A6A6;"> - one row per Part</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input class="rptType" id="rptVisit" value="5" type="radio" name="reportType" section="fieldwoDates" <?= $report_type == "5" ? "checked" : "" ?>>
                                    </td>
                                    <td>
                                        Visits Report<span style="color:#A6A6A6;"> - one row per Visit</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="section">
                            <a class="title <?= !empty($FilterList["Project_Name"]["SelectMultiple"]) ? "divclose" : "divopen" ?>" title="Collapse Section" isopen="<?= !empty($FilterList["Project_Name"]["SelectMultiple"]) ? "1" : "0" ?>" section="Project">
                                <b>Select Projects</b>
                            </a>
                            <a class="addremovefilteritem" id="Project" href="javascript:;"><?= !empty($FilterList["Project_Name"]["SelectMultiple"]) ? "(select/remove)" : "(select)" ?></a>
                        </div>
                        <div id="divProject" style="<?= !empty($FilterList["Project_Name"]["SelectMultiple"]) ? "" : "display:none;" ?> padding-left:15px;">
                            <ul id="ulProject" class="listitem">
                                <?
                                if (!empty($rptcopy) && $oldv !== $ComID)
                                {
                                    $projectFilterList = "";
                                } else
                                {
                                    foreach ($FilterList["Project_Name"]["SelectMultiple"] as $projectName)
                                    {
                                        foreach ($PrjectList as $projectitem)
                                        {
                                            if ($projectitem['Project_Name'] == $projectName)
                                            {
                                                $projectFilterList .="
                                                    <li Projectid=\"" . $projectitem['Project_ID'] . "\" id=\"Project" . $projectitem['Project_ID'] . "\" class=\"listitem\">
                                                        <img height=\"15px\" section=\"Project\" Projectid=\"" . $projectitem['Project_ID'] . "\" style=\"cursor:pointer;\" src=\"/widgets/css/images/removedoc.png\" class=\"cmdremove\" filterid=\"" . $FilterList["Project_Name"]["id"] . "\" fieldname=\"" . $projectitem['Project_Name'] . "\"> " . $projectitem['Project_Name'] . "
                                                    </li>";
                                            }
                                        }
                                    }
                                }
                                echo($projectFilterList);
                                ?>
                            </ul>
                        </div>
                        <div class="section">
                            <a class="title <?= !empty($FilterList["Status"]["SelectMultiple"]) ? "divclose" : "divopen" ?>" title="Collapse Section" isopen="<?= !empty($FilterList["Status"]["SelectMultiple"]) ? "1" : "0" ?>" section="wostatus">
                                <b>Select Work Order Status</b><span style='color:red;'>*</span> 
                            </a>
                            <a class="addremovefilteritem" id="wostatus" href="javascript:;"><?= !empty($FilterList["Status"]["SelectMultiple"]) ? "(select/remove)" : "(select)" ?></a>
                        </div>
                        <div id="divwostatus" style="<?= !empty($FilterList["Status"]["SelectMultiple"]) ? "" : "display:none;" ?> padding-left:15px;">
                            <table id="tdwostatus" cellspacing="" cellpadding="5">
                                <col width='15px'>
                                <col width='300px'>
                                <col width='*'>
                                <?
                                foreach ($FieldOrder["wostatus"] as $id => $val)
                                {
                                    foreach ($FilterList["Status"]["SelectMultiple"] as $item)
                                    {
                                        if ($id == $item)
                                        {
                                            ?>
                                            <tr ordernum = "<?= $val ?>" wostatusid = "<?= $item ?>" id = "wostatus<?= $item ?>" <?= $item ?>_type="" style="padding:5px;">
                                                <td style="vertical-align:top;">
                                                    <img height="15px" section="wostatus" wostatusid="<?= $item ?>" style="cursor:pointer;" src="/widgets/css/images/removedoc.png" class="cmdremove" filterid="">
                                                </td>
                                                <td style="vertical-align:top;"><?= $WOStatusList[$item] ?></td>
                                                <td style="vertical-align:top;"></td>
                                            </tr>
                                            <?
                                        }
                                    }
                                }
                                ?>
                            </table>
                        </div>
                        <div class="section">
                            <a class="title divclose" title="Collapse Section" isopen="1" section="fieldwoInfo">
                                <b>Basic Work Order Information</b>
                            </a>
                            <a class="addremovefilteritem" id="fieldwoInfo" href="javascript:;"><?= count($BasicInformationColumns) > 0 ? "(select/remove)" : "(select)" ?></a>
                        </div>
                        <div id="divfieldwoInfo" style="padding-left:15px;">
                            <table id="tdfieldwoInfo" cellspacing="" cellpadding="5">
                                <col width='15px'>
                                <col width='300px'>
                                <col width='*'>
                                <?
                                if (!empty($BasicInformationColumns))
                                {
                                    foreach ($FieldOrder["fieldwoInfo"] as $key => $val)
                                    {
                                        if (array_key_exists($key, $BasicInformationColumns))
                                        {
                                            ?>
                                            <tr ordernum="<?= $FieldOrder["fieldwoInfo"][$key] ?>" fieldwoInfoid="<?= $key ?>" id="fieldwoInfo<?= $key ?>" <?= $key ?>_type='<?= $BasicInformationColumns[$key]["FilterType"] ?>' style='padding:5px;'>
                                                <td style='vertical-align:top;'>
                                                    <img height="15px" section="fieldwoInfo" fieldwoInfoid="<?= $key ?>" style='cursor:pointer;' src='/widgets/css/images/removedoc.png' class='cmdremove' filterid="<?= $FilterList[$key]["id"] ?>">
                                                </td>
                                                <td style='vertical-align:top;' <?= empty($BasicInformationColumns[$key]["FilterType"]) ? "colspan='2'" : "" ?> ><?= $BasicInformationColumns[$key]["field_title"] ?></td>
                                                <?
                                                if (!empty($BasicInformationColumns[$key]["FilterType"]))
                                                {
                                                    ?>
                                                    <td style='vertical-align:top;'>
                                                        <?=
                                                        buildcontrol($BasicInformationColumns[$key]["FilterType"], $ComID, $isPM, $key, $FilterList[$key], "fieldwoInfo");
                                                        ?>
                                                    </td>
                                                    <?
                                                }
                                                ?>
                                            </tr>
                                            <?
                                        }
                                    }
                                }
                                ?>
                            </table>
                        </div>
                        <div class="section">
                            <a class="title <?= empty($DatesTimesColumns) ? "divopen" : "divclose" ?>" title="Collapse Section" isopen="<?= empty($DatesTimesColumns) ? "1" : "0" ?>" section="fieldwoDates">
                                <b>On-Site Schedule</b>
                            </a>
                            <a class="addremovefilteritem" id="fieldwoDates" href="javascript:;"><?= !empty($DatesTimesColumns) ? "(select/remove)" : "(select)" ?></a>
                        </div>
                        <div id="divfieldwoDates" style="<?= !empty($DatesTimesColumns) ? "" : "display:none;" ?> padding-left:15px;">

                            <table id="tdfieldwoDates" cellspacing="" cellpadding="5">
                                <col width='15px'>
                                <col width='300px'>
                                <col width='*'>
                                <?
                                if (!empty($DatesTimesColumns))
                                {
                                    foreach ($FieldOrder["fieldwoDates"] as $key => $val)
                                    {
                                        if (array_key_exists($key, $DatesTimesColumns))
                                        {
                                            ?>
                                            <tr ordernum="<?= $FieldOrder["fieldwoDates"][$key] ?>" fieldwoDatesid="<?= $key ?>" id="fieldwoDates<?= $key ?>" <?= $key ?>_type='<?= $DatesTimesColumns[$key]["FilterType"] ?>' style='padding:5px;'>
                                                <td style='vertical-align:top;'>
                                                    <img height="15px" section="fieldwoDates" fieldwoDatesid="<?= $key ?>" style='cursor:pointer;' src='/widgets/css/images/removedoc.png' class='cmdremove' filterid="<?= $FilterList[$key]["id"] ?>">
                                                </td>
                                                <td style='vertical-align:top;' <?= empty($DatesTimesColumns[$key]["FilterType"]) ? "colspan='2'" : "" ?>><?= $DatesTimesColumns[$key]["field_title"] ?></td>
                                                <?
                                                if (!empty($DatesTimesColumns[$key]["FilterType"]))
                                                {
                                                    ?>
                                                    <td style='vertical-align:top;'>
                                                        <?=
                                                        buildcontrol($DatesTimesColumns[$key]["FilterType"], $ComID, $isPM, $key, $FilterList[$key], "fieldwoDates");
                                                        ?>
                                                    </td>
                                                    <?
                                                }
                                                ?>
                                            </tr>
                                            <?
                                        }
                                    }
                                }
                                ?>
                            </table>

                        </div>
                        <div class="section">
                            <a class="title <?= empty($SiteInformationColumns) ? "divopen" : "divclose" ?>" title="Collapse Section" isopen="<?= empty($SiteInformationColumns) ? "1" : "0" ?>" section="fieldsite">
                                <b>Site Information </b>
                            </a>
                            <a class="addremovefilteritem" id="fieldsite" href="javascript:;"><?= !empty($SiteInformationColumns) ? "(select/remove)" : "(select)" ?></a>
                        </div>
                        <div id="divfieldsite" style="<?= !empty($SiteInformationColumns) ? "" : "display:none;" ?> padding-left:15px;">
                            <table id="tdfieldsite" cellspacing="" cellpadding="5">
                                <col width='15px'>
                                <col width='300px'>
                                <col width='*'>
                                <?
                                if (!empty($SiteInformationColumns))
                                {
                                    foreach ($SiteInformationColumns as $key => $val)
                                    {
                                        ?>
                                        <tr ordernum="<?= $FieldOrder["fieldsite"][$key] ?>" fieldsiteid="<?= $key ?>" id="fieldsite<?= $key ?>" <?= $key ?>_type='<?= $val["FilterType"] ?>' style='padding:5px;'>
                                            <td style='vertical-align:top;'>
                                                <img height="15px" section="fieldsite" fieldsiteid="<?= $key ?>" style='cursor:pointer;' src='/widgets/css/images/removedoc.png' class='cmdremove' filterid="<?= $FilterList[$key]["id"] ?>">
                                            </td>
                                            <td style='vertical-align:top;' <?= empty($val["FilterType"]) ? "colspan='2'" : "" ?>><?= $val["field_title"] ?></td>
                                            <?
                                            if (!empty($val["FilterType"]))
                                            {
                                                ?>
                                                <td style='vertical-align:top;'>
                                                    <?=
                                                    buildcontrol($val["FilterType"], $ComID, $isPM, $key, $FilterList[$key], "fieldsite");
                                                    ?>
                                                </td>
                                                <?
                                            }
                                            ?>
                                        </tr>
                                        <?
                                    }
                                }
                                ?>
                            </table>
                        </div>
                        <div class="section">
                            <a class="title <?= empty($BidInformationColumns) ? "divopen" : "divclose" ?>" title="Collapse Section" isopen="<?= empty($BidInfomationColumns) ? "1" : "0" ?>" section="fieldbid">
                                <b>Bid Information </b>
                            </a>
                            <a class="addremovefilteritem" id="fieldbid" href="javascript:;"><?= !empty($BidInformationColumns) ? "(select/remove)" : "(select)" ?></a>
                        </div>
                        <div id="divfieldbid" style="<?= !empty($BidInformationColumns) ? "" : "display:none;" ?> padding-left:15px;">
                            <table id="tdfieldbid" cellspacing="" cellpadding="5">
                                <col width='15px'>
                                <col width='300px'>
                                <col width='*'>
                                <?
                                if (!empty($BidInformationColumns))
                                {
                                    foreach ($BidInformationColumns as $key => $val)
                                    {
                                        ?>
                                        <tr ordernum="<?= $FieldOrder["fieldbid"][$key] ?>" fieldbidid="<?= $key ?>" id="fieldbid<?= $key ?>" <?= $key ?>_type='<?= $val["FilterType"] ?>' style='padding:5px;'>
                                            <td style='vertical-align:top;'>
                                                <img height="15px" section="fieldbid" fieldbidid="<?= $key ?>" style='cursor:pointer;' src='/widgets/css/images/removedoc.png' class='cmdremove' filterid="<?= $FilterList[$key]["id"] ?>">
                                            </td>
                                            <td style='vertical-align:top;' <?= empty($val["FilterType"]) ? "colspan='2'" : "" ?>><?= $val["field_title"] ?></td>
                                            <?
                                            if (!empty($val["FilterType"]))
                                            {
                                                ?>
                                                <td style='vertical-align:top;'>
                                                    <?=
                                                    buildcontrol($val["FilterType"], $ComID, $isPM, $key, $FilterList[$key], "fieldbid");
                                                    ?>
                                                </td>
                                                <?
                                            }
                                            ?>
                                        </tr>
                                        <?
                                    }
                                }
                                ?>
                            </table>
                        </div>
                        <div class="section">
                            <a class="title <?= empty($TechniciansColumns) ? "divopen" : "divclose" ?>" title="Collapse Section" isopen="<?= empty($TechnicianColumns) ? "1" : "0" ?>" section="fieldtechnicians">
                                <b>Assigned Technicians </b>
                            </a>
                            <a class="addremovefilteritem" id="fieldtechnicians" href="javascript:;"><?= !empty($TechniciansColumns) ? "(select/remove)" : "(select)" ?></a>
                        </div>
                        <div id="divfieldtechnicians" style="<?= !empty($TechniciansColumns) > 0 ? "" : "display:none;" ?> padding-left:15px;">
                            <table id="tdfieldtechnicians" cellspacing="" cellpadding="5">
                                <col width='15px'>
                                <col width='300px'>
                                <col width='*'>
                                <?
                                if (!empty($TechniciansColumns))
                                {
                                    foreach ($TechniciansColumns as $key => $val)
                                    {
                                        ?>
                                        <tr ordernum="<?= $FieldOrder["fieldtechnicians"][$key] ?>" fieldtechniciansid="<?= $key ?>" id="fieldtechnicians<?= $key ?>" <?= $key ?>_type='<?= $val["FilterType"] ?>' style='padding:5px;'>
                                            <td style='vertical-align:top;'>
                                                <img height="15px" section="fieldtechnicians" fieldtechniciansid="<?= $key ?>" style='cursor:pointer;' src='/widgets/css/images/removedoc.png' class='cmdremove' filterid="<?= $FilterList[$key]["id"] ?>">
                                            </td>
                                            <td style='vertical-align:top;' <?= empty($val["FilterType"]) ? "colspan='2'" : "" ?>><?= $val["field_title"] ?></td>
                                            <?
                                            if (!empty($val["FilterType"]))
                                            {
                                                ?>
                                                <td style='vertical-align:top;'>
                                                    <?=
                                                    buildcontrol($val["FilterType"], $ComID, $isPM, $key, $FilterList[$key], "fieldtechnicians");
                                                    ?>
                                                </td>
                                                <?
                                            }
                                            ?>
                                        </tr>
                                        <?
                                    }
                                }
                                ?>
                            </table>
                        </div>
                        <div class="section">
                            <a class="title <?= empty($CommunicationsColumns) ? "divopen" : "divclose" ?>" title="Collapse Section" isopen="<?= empty($CommunicationsColumns) ? "1" : "0" ?>" section="fieldCommunications">
                                <b>Communications</b>
                            </a>
                            <a class="addremovefilteritem" id="fieldCommunications" href="javascript:;"><?= !empty($CommunicationsColumns) ? "(select/remove)" : "(select)" ?></a>
                        </div>
                        <div id="divfieldCommunications" style="<?= !empty($CommunicationsColumns) ? "" : "display:none;" ?> padding-left:15px;">
                            <table id="tdfieldCommunications" cellspacing="" cellpadding="5">
                                <col width='15px'>
                                <col width='300px'>
                                <col width='*'>
                                <?
                                if (!empty($CommunicationsColumns))
                                {
                                    foreach ($CommunicationsColumns as $key => $val)
                                    {
                                        ?>
                                        <tr ordernum="<?= $FieldOrder["fieldCommunications"][$key] ?>" fieldCommunicationsid="<?= $key ?>" id="fieldCommunications<?= $key ?>" <?= $key ?>_type='<?= $val["FilterType"] ?>' style='padding:5px;'>
                                            <td style='vertical-align:top;'>
                                                <img height="15px" section="fieldCommunications" fieldCommunicationsid="<?= $key ?>" style='cursor:pointer;' src='/widgets/css/images/removedoc.png' class='cmdremove' filterid="<?= $FilterList[$key]["id"] ?>">
                                            </td>
                                            <td style='vertical-align:top;' <?= empty($val["FilterType"]) ? "colspan='2'" : "" ?>><?= $val["field_title"] ?></td>
                                            <?
                                            if (!empty($val["FilterType"]))
                                            {
                                                ?>
                                                <td style='vertical-align:top;'>
                                                    <?=
                                                    buildcontrol($val["FilterType"], $ComID, $isPM, $key, $FilterList[$key], "fieldCommunications");
                                                    ?>
                                                </td>
                                                <?
                                            }
                                            ?>
                                        </tr>
                                        <?
                                    }
                                }
                                ?>
                            </table>
                        </div>
                        <div class="section">
                            <a class="title <?= empty($ConfirmationsCloseOutColumns) ? "divopen" : "divclose" ?>" title="Collapse Section" isopen="<?= empty($ConfirmationsCloseOutColumns) ? "1" : "0" ?>" section="fieldconfirmation">
                                <b>Confirmations &amp; Close Out</b>
                            </a>
                            <a class="addremovefilteritem" id="fieldconfirmation" href="javascript:;"><?= !empty($ConfirmationsCloseOutColumns) ? "(select/remove)" : "(select)" ?></a>
                        </div>
                        <div id="divfieldconfirmation" style="<?= !empty($ConfirmationsCloseOutColumns) ? "" : "display:none;" ?> padding-left:15px;">
                            <table id="tdfieldconfirmation" cellspacing="" cellpadding="5">
                                <col width='15px'>
                                <col width='300px'>
                                <col width='*'>
                                <?
                                if (!empty($ConfirmationsCloseOutColumns))
                                {
                                    foreach ($FieldOrder["fieldconfirmation"] as $key => $val)
                                    {
                                        if (array_key_exists($key, $ConfirmationsCloseOutColumns))
                                        {
                                            ?>
                                            <tr ordernum="<?= $FieldOrder["fieldconfirmation"][$key] ?>" fieldconfirmationid="<?= $key ?>" id="fieldconfirmation<?= $key ?>" <?= $key ?>_type='<?= $ConfirmationsCloseOutColumns[$key]["FilterType"] ?>' style='padding:5px;'>
                                                <td style='vertical-align:top;'>
                                                    <img height="15px" section="fieldconfirmation" fieldconfirmationid="<?= $key ?>" style='cursor:pointer;' src='/widgets/css/images/removedoc.png' class='cmdremove' filterid="<?= $FilterList[$key]["id"] ?>">
                                                </td>
                                                <td style='vertical-align:top;' <?= empty($ConfirmationsCloseOutColumns[$key]["FilterType"]) ? "colspan='2'" : "" ?>><?= $ConfirmationsCloseOutColumns[$key]["field_title"] ?></td>
                                                <?
                                                if (!empty($ConfirmationsCloseOutColumns[$key]["FilterType"]))
                                                {
                                                    ?>
                                                    <td style='vertical-align:top;'>
                                                        <?=
                                                        buildcontrol($ConfirmationsCloseOutColumns[$key]["FilterType"], $ComID, $isPM, $key, $FilterList[$key], "fieldconfirmation");
                                                        ?>
                                                    </td>
                                                    <?
                                                }
                                                ?>
                                            </tr>
                                            <?
                                        }
                                    }
                                }
                                ?>
                            </table>
                        </div>
                        <div class="section">
                            <a class="title <?= empty($PartsManagerColumns) ? "divopen" : "divclose" ?>" title="Collapse Section" isopen="<?= empty($PartsManagerColumns) ? "1" : "0" ?>" section="partmanager">
                                <b>Parts Manager</b>
                            </a>
                            <a class="addremovefilteritem" id="partmanager" href="javascript:;"><?= !empty($PartsManagerColumns) ? "(select/remove)" : "(select)" ?></a>
                        </div>
                        <div id="divpartmanager" style="<?= !empty($PartsManagerColumns) ? "" : "display:none;" ?> padding-left:15px;">
                            <table id="tdpartmanager" cellspacing="" cellpadding="5">
                                <col width='15px'>
                                <col width='300px'>
                                <col width='*'>
                                <?
                                if (!empty($PartsManagerColumns))
                                {
                                    foreach ($PartsManagerColumns as $key => $val)
                                    {
                                        ?>
                                        <tr ordernum="<?= $FieldOrder["partmanager"][$key] ?>" partmanagerid="<?= $key ?>" id="partmanager<?= $key ?>" <?= $key ?>_type='<?= $val["FilterType"] ?>' style='padding:5px;'>
                                            <td style='vertical-align:top;'>
                                                <img height="15px" section="partmanager" partmanagerid="<?= $key ?>" style='cursor:pointer;' src='/widgets/css/images/removedoc.png' class='cmdremove' filterid="<?= $FilterList[$key]["id"] ?>">
                                            </td>
                                            <td style='vertical-align:top;' <?= empty($val["FilterType"]) ? "colspan='2'" : "" ?>><?= $val["field_title"] ?></td>
                                            <?
                                            if (!empty($val["FilterType"]))
                                            {
                                                ?>
                                                <td style='vertical-align:top;'>
                                                    <?=
                                                    buildcontrol($val["FilterType"], $ComID, $isPM, $key, $FilterList[$key], "partmanager");
                                                    ?>
                                                </td>
                                                <?
                                            }
                                            ?>
                                        </tr>
                                        <?
                                    }
                                }
                                ?>
                            </table>
                        </div>
                        <div class="section">
                            <a class="title <?= empty($WinTagsColumns) ? "divopen" : "divclose" ?>" title="Collapse Section" isopen="<?= empty($WinTagsColumns) ? "1" : "0" ?>" section="wintag">
                                <b>WIN-Tags<sup>TM</sup> Performance Reporting</b>
                            </a>
                            <a class="addremovefilteritem" id="wintag" href="javascript:;"><?= !empty($WinTagsColumns) ? "(select/remove)" : "(select)" ?></a>
                        </div>
                        <div id="divwintag" style="<?= !empty($WinTagsColumns) ? "" : "display:none;" ?> padding-left:15px;">
                            <table id="tdwintag" cellspacing="" cellpadding="5">
                                <col width='15px'>
                                <col width='300px'>
                                <col width='*'>
                                <?
                                if (!empty($WinTagsColumns))
                                {
                                    foreach ($FieldOrder["wintag"] as $key => $val)
                                    {
                                        if (array_key_exists($key, $WinTagsColumns))
                                        {
                                            ?>
                                            <tr ordernum="<?= $FieldOrder["wintag"][$key] ?>" wintagid="<?= $key ?>" id="wintag<?= $key ?>" <?= $key ?>_type='<?= $WinTagsColumns[$key]["FilterType"] ?>' style='padding:5px;'>
                                                <td style='vertical-align:top;'>
                                                    <img height="15px" section="wintag" wintagid="<?= $key ?>" style='cursor:pointer;' src='/widgets/css/images/removedoc.png' class='cmdremove' filterid="<?= $FilterList[$key]["id"] ?>">
                                                </td>
                                                <td style='vertical-align:top;' <?= empty($WinTagsColumns[$key]["FilterType"]) ? "colspan='2'" : "" ?>><?= $WinTagsColumns[$key]["field_title"] ?></td>
                                                <?
                                                if (!empty($WinTagsColumns[$key]["FilterType"]))
                                                {
                                                    ?>
                                                    <td style='vertical-align:top;'>
                                                        <?=
                                                        buildcontrol($WinTagsColumns[$key]["FilterType"], $ComID, $isPM, $key, $FilterList[$key], "wintag");
                                                        ?>
                                                    </td>
                                                    <?
                                                }
                                                ?>
                                            </tr>
                                            <?
                                        }
                                    }
                                }
                                ?>
                            </table>
                        </div>
                        <div class="section">
                            <a class="title <?= empty($PricingPayDetailsColumns) ? "divopen" : "divclose" ?>" title="Collapse Section" isopen="<?= empty($PricingPayDetailsColumns) ? "1" : "0" ?>" section="fieldPricing">
                                <b>Pricing &amp; Pay Details</b>
                            </a>
                            <a class="addremovefilteritem" id="fieldPricing" href="javascript:;"><?= !empty($PricingPayDetailsColumns) ? "(select/remove)" : "(select)" ?></a>
                        </div>
                        <div id="divfieldPricing" style="<?= !empty($PricingPayDetailsColumns) ? "" : "display:none;" ?> padding-left:15px;">
                            <table id="tdfieldPricing" cellspacing="" cellpadding="5">
                                <col width='15px'>
                                <col width='300px'>
                                <col width='*'>
                                <?
                                if (!empty($PricingPayDetailsColumns))
                                {
                                    foreach ($FieldOrder["fieldPricing"] as $key => $val)
                                    {
                                        if (array_key_exists($key, $PricingPayDetailsColumns))
                                        {
                                            ?>
                                            <tr ordernum="<?= $FieldOrder["fieldPricing"][$key] ?>" fieldPricingid="<?= $key ?>" id="fieldPricing<?= $key ?>" <?= $key ?>_type='<?= $PricingPayDetailsColumns[$key]["FilterType"] ?>' style='padding:5px;'>
                                                <td style='vertical-align:top;'>
                                                    <img height="15px" section="fieldPricing" fieldPricingid="<?= $key ?>" style='cursor:pointer;' src='/widgets/css/images/removedoc.png' class='cmdremove' filterid="<?= $FilterList[$key]["id"] ?>">
                                                </td>
                                                <td style='vertical-align:top;' <?= empty($PricingPayDetailsColumns[$key]["FilterType"]) ? "colspan='2'" : "" ?>><?= $PricingPayDetailsColumns[$key]["field_title"] ?></td>
                                                <?
                                                if (!empty($PricingPayDetailsColumns[$key]["FilterType"]))
                                                {
                                                    ?>
                                                    <td style='vertical-align:top;'>
                                                        <?=
                                                        buildcontrol($PricingPayDetailsColumns[$key]["FilterType"], $ComID, $isPM, $key, $FilterList[$key], "fieldPricing");
                                                        ?>
                                                    </td>
                                                    <?
                                                }
                                                ?>
                                            </tr>
                                            <?
                                        }
                                    }
                                }
                                ?>
                            </table>
                        </div>
                        <div class="section">
                            <a class="title <?= empty($AdditionalInformationColumns) ? "divopen" : "divclose" ?>" title="Collapse Section" isopen="<?= empty($AdditionalInformationColumns) ? "1" : "0" ?>" section="fieldadditional">
                                <b>Work Order Activity Log</b>
                            </a>
                            <a class="addremovefilteritem" id="fieldadditional" href="javascript:;"><?= !empty($AdditionalInformationColumns) ? "(select/remove)" : "(select)" ?></a>
                        </div>
                        <div id="divfieldadditional" style="<?= !empty($AdditionalInformationColumns) ? "" : "display:none" ?>; padding-left:15px;">
                            <table id="tdfieldadditional" cellspacing="" cellpadding="5">
                                <col width='15px'>
                                <col width='300px'>
                                <col width='*'>
                                <?
                                if (!empty($AdditionalInformationColumns))
                                {
                                    foreach ($AdditionalInformationColumns as $key => $val)
                                    {
                                        ?>
                                        <tr ordernum="<?= $FieldOrder["fieldadditional"][$key] ?>" fieldadditionalid="<?= $key ?>" id="fieldadditional<?= $key ?>" <?= $key ?>_type='<?= $val["FilterType"] ?>' style='padding:5px;'>
                                            <td style='vertical-align:top;'>
                                                <img height="15px" section="fieldadditional" fieldadditionalid="<?= $key ?>" style='cursor:pointer;' src='/widgets/css/images/removedoc.png' class='cmdremove' filterid="<?= $FilterList[$key]["id"] ?>">
                                            </td>
                                            <td style='vertical-align:top;' <?= empty($AdditionalInformationColumns[$key]["FilterType"]) ? "colspan='2'" : "" ?>><?= $val["field_title"] ?></td>
                                            <?
                                            if (!empty($AdditionalInformationColumns[$key]["FilterType"]))
                                            {
                                                ?>
                                                <td style='vertical-align:top;'>
                                                    <?=
                                                    buildcontrol($val["FilterType"], $ComID, $isPM, $key, $FilterList[$key], "fieldadditional");
                                                    ?>
                                                </td>
                                                <?
                                            }
                                            ?>
                                        </tr>
                                        <?
                                    }
                                }
                                ?>
                            </table>
                        </div>
                        <div style="vetical-align:middle;text-align:left;padding-top:5px;">
                            <input id="cmdChooseFilters" type="button" value="Back to Filters" class="link_button middle2_button" />
                            <div style="vetical-align:middle;text-align:center ;margin-left:0px;">
                                <input type="button" value="Cancel" id="Cancel" class="link_button middle2_button" onclick="window.location = 'reportlist.php?v=<?= $_GET["v"] ?>'" />
                                <input type="button" value="Save &amp; Generate Report" id="cmdsaveandgenerate" class="link_button middle2_button"/>
                                <input type="button" value="Save" id="cmdAdd" class="link_button link_button_popup"/>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
<div id="reportrecurrent" style="display:none;">
    <table cellspacing="" cellpadding="5" width="100%">
        <tr>
            <td style="width:250px;padding-bottom:5px;">Enable Recurring Report Delivery</td>
            <td style="vertical-align:top;">
                <input id="chkEnableRecurringReport" type="checkbox" <?= $RRDEnable ?> >
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <hr style="height:1px;background: none repeat scroll 0 0 #dddddd;margin:0px 0px 5px;">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table cellspacing="" cellpadding="5" width="100%">
                    <tr>
                        <td style="width:90px;" id="lblsendto">Send to:</td>
                        <td>
                            <input class="RecurringReportEle" id="txtsendto" type="text" style="width:94%;<?= $RRDEmailToColor ?>" value="<?= $RRDEmailTo ?>" <?= $disabled ?>/>&nbsp;
                            <a title="" bt-xtitle="" id="getinfocontrols" href="javascript:void(0)" class="bt-active bt-active">
                                <img src="/widgets/images/get_info.png" style="vertical-align: middle;height:15px;">
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>From Name:</td>
                        <td>
                            <input id="txtFromName" class="RecurringReportEle" type="text" style="width:94%;" value="<?= $RRDEmailFromName ?>" <?= $disabled ?>/>
                        </td>
                    </tr>
                    <tr>
                        <td>From Email:</td>
                        <td>
                            <input id="txtFromMail" class="RecurringReportEle" type="text" style="width:94%;" value="<?= $RRDEmailFrom ?>" <?= $disabled ?>/>
                        </td>
                    </tr>
                    <tr>
                        <td>Subject:</td>
                        <td>
                            <input id="txtSubject" class="RecurringReportEle" type="text" style="width:94%;" value="<?= $RRDEmailSubject ?>" <?= $disabled ?>/>
                        </td>
                    </tr>
                    <tr>
                        <td>Message:</td>
                        <td>
                            <textarea id="txtmessage" class="RecurringReportEle" style="width: 93%; height: 58px;resize: none;" <?= $disabled ?>><?= $RRDEmailMessage ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top;">Frequency:</td>
                        <td>
                            <select id="cbxSchedule" class="RecurringReportEle" style="width:100px" <?= $disabled ?>>
                                <option value="Daily" <?= $RRDEmailSchedule == "Daily" ? "selected" : "" ?>>Daily</option>
                                <option value="Weekly" <?= $RRDEmailSchedule == "Weekly" ? "selected" : "" ?>>Weekly</option>
                                <option value="Monthly" <?= $RRDEmailSchedule == "Monthly" ? "selected" : "" ?>>Monthly</option>
                            </select>
                            <!--14100-->
                            <div id="ScheduleDayly" style="<?= $RRDEmailSchedule == "Daily" ? "" : "display:none;" ?>">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <col width="20px">
                                    <col width="100px">
                                    <col width="110px">
                                    <col width="*">
                                    <tr>
                                        <td>
                                            <input type="radio" id="DaylyDayNum" class="Dayly" name="RRDDailyOptions" <?= $RRDDailyOptions == 1 ? "checked" : "" ?>  <?= $disabled ?>>
                                        </td>
                                        <td>Every </td>
                                        <td>
                                            <input id="txteverydaynum" value="<?= $RRDDailyEveryDaySpaceOutNum ?>" style="width:97px;" <?= $disabled ?>>
                                        </td>
                                        <td>day(s)</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="radio" id="DaylyWeekday" class="Dayly" name="RRDDailyOptions" <?= $RRDDailyOptions == 2 ? "checked" : "" ?>  <?= $disabled ?>>
                                        </td>
                                        <td>Every weekday</td>
                                        <td>
                                        </td>
                                        <td>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="ScheduleWeekly" style="<?= $RRDEmailSchedule == "Weekly" ? "" : "display:none;" ?>">
                                <div style="float:left;margin-top:-20px;padding-left:110px">
                                    Generate & Deliver every <input id="WeeklyWeekNum" type="text" value="<?= $RRDWeeklySpaceOutNum ?>" style="width:50px;"  <?= $disabled ?>> week(s) on:
                                </div>
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <col width="20px">
                                    <col width="85px">
                                    <col width="20px">
                                    <col width="*">
                                    <tr>
                                        <td>
                                            <input type="checkbox" id="WeeklyMonday" class="Weekly" value="1" <?= in_array("1", $RRDWeeklyDays) ? "checked" : "" ?> <?= $disabled ?>>
                                        </td>
                                        <td>Monday</td>
                                        <td>
                                            <input type="checkbox" id="WeeklyFriday" class="Weekly" value="5"  <?= in_array("5", $RRDWeeklyDays) ? "checked" : "" ?> <?= $disabled ?>>
                                        </td>
                                        <td>Friday</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="checkbox" id="WeeklyTuesday" class="Weekly" value="2" <?= in_array("2", $RRDWeeklyDays) ? "checked" : "" ?> <?= $disabled ?>>
                                        </td>
                                        <td>Tuesday</td>
                                        <td>
                                            <input type="checkbox" id="WeeklySaturday" class="Weekly" value="6" <?= in_array("6", $RRDWeeklyDays) ? "checked" : "" ?> <?= $disabled ?>>
                                        </td>
                                        <td>Saturday</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="checkbox" id="WeeklyWednesday" class="Weekly" value="3" <?= in_array("3", $RRDWeeklyDays) ? "checked" : "" ?> <?= $disabled ?>>
                                        </td>
                                        <td>Wednesday</td>
                                        <td>
                                            <input type="checkbox" id="WeeklySunday" class="Weekly" value="7" <?= in_array("7", $RRDWeeklyDays) ? "checked" : "" ?> <?= $disabled ?>>
                                        </td>
                                        <td>Sunday</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="checkbox" id="WeeklyThursday" class="Weekly" value="4" <?= in_array("4", $RRDWeeklyDays) ? "checked" : "" ?> <?= $disabled ?>>
                                        </td>
                                        <td>Thursday</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                            <div id="ScheduleMonthly" style="<?= $RRDEmailSchedule == "Monthly" ? "" : "display:none;" ?>">
                                <table cellspacing="0" cellpadding="0" width="100%">
                                    <col width="15px">
                                    <col width="90px">
                                    <col width="110px">
                                    <col width="*">
                                    <tr>
                                        <td>
                                            <input type="radio" id="MonthlyDayNum" class="Monthly" name="RRDMonthlyScheduleOptions" <?= $RRDMonthlyScheduleOptions == 1 ? "checked" : "" ?> <?= $disabled ?>>
                                        </td>
                                        <td>On day</td>
                                        <td>
                                            <select id="cbxMonthlyDayNum" style="width:100px" <?= $disabled ?>>
                                                <?
                                                foreach ($Monthdayorder as $key => $val)
                                                {
                                                    ?>
                                                    <option value="<?= $val ?>" <?= $RRDMonthlyScheduleDayNum == $val ? "selected" : "" ?>><?= $val ?></option>
                                                    <?
                                                }
                                                ?>
                                            </select>
                                        </td>
                                        <td>
                                            of every <input id="MonthlyMonthOndayNum" type="text" value="<?= $RRDMonthlyDayNumSpaceOutNum ?>" style="width:20px;"  <?= $disabled ?>> month(s)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="radio" id="MonthlyDayOrder" class="Monthly" name="RRDMonthlyScheduleOptions" <?= $RRDMonthlyScheduleOptions == 2 ? "checked" : "" ?> <?= $disabled ?>>
                                        </td>
                                        <td>On the</td>
                                        <td>
                                            <select id="cbxMonthlyDayOrder" style="width:100px"  <?= $disabled ?>>
                                                <?
                                                foreach ($MonthlyDayOrder as $key => $val)
                                                {
                                                    ?>
                                                    <option value="<?= $key ?>" <?= $RRDMonthlyScheduleDayOrder == $key ? "selected" : "" ?>><?= $val ?></option>
                                                    <?
                                                }
                                                ?>
                                            </select>
                                        </td>
                                        <td>
                                            <select id="cbxMonthlyDayOrderName" style="width:100px"  <?= $disabled ?>>
                                                <?
                                                foreach ($weekdays as $key => $val)
                                                {
                                                    ?>
                                                    <option value="<?= $key ?>" <?= $RRDMonthlyScheduleDayOrderDay == $key ? "selected" : "" ?>><?= $val ?></option>
                                                    <?
                                                }
                                                ?>
                                            </select>
                                            of every <input id="MonthlyMonthOnthedayNum" type="text" value="<?= $RRDMonthlyDayOrderSpaceOutNum ?>" style="width:20px;"  <?= $disabled ?>> month(s)
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Time of Day:</td>
                        <td>
                            <select id="cbxtimeSchedule" class="RecurringReportEle" style="width:100px" <?= $disabled ?>>
                                <?= getTime($RRDTimeOfDay) ?>
                            </select>
                            <select id="cbxestSchedule" class="RecurringReportEle" style="width:150px" <?= $disabled ?>>
                                <option value ="-10" <?= $RRDTimeZone == "-10" ? "selected" : "" ?>>Hawaii (GMT-10:00)</option>
                                <option value ="-9"  <?= $RRDTimeZone == "-9" ? "selected" : "" ?>>Alaska (GMT-8:00)</option>
                                <option value ="-8" <?= $RRDTimeZone == "-8" ? "selected" : "" ?>>Pacific (GMT-7:00)</option>
                                <option value ="-7" <?= $RRDTimeZone == "-7" ? "selected" : "" ?>>Mountain (GMT-6:00)</option>
                                <option value ="-6" <?= $RRDTimeZone == "-6" ? "selected" : "" ?>>Central (GMT-5:00)</option>
                                <option value ="-5" <?= $RRDTimeZone == "-5" ? "selected" : "" ?>>Eastern (GMT-4:00)</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

<?php require ("../../footer.php"); ?>
