//-------------------------------------------------------------
custom_report.Instances = null;
//-------------------------------------------------------------
custom_report.CreateObject = function(config) {
    var obj = null;
    if (custom_report.Instances !== null) {
        obj = custom_report.Instances[config.id];
    }
    if (obj === null) {
        if (custom_report.Instances === null) {
            custom_report.Instances = new Object();
        }
        obj = new custom_report(config);
        custom_report.Instances[config.id] = obj;
    }
//    obj.init();
    return obj;
};
//-------------------------------------------------------------
function custom_report(config) {
    var Me = this;
    this.id = config.id;
    this.company = config.company;
    this.report_id = config.report_id;
    this._roll = new FSPopupRoll();
    this.FieldOrder = config.FieldOrder;
    this._recurringreport = {
        'RRDEnable': "",
        'RRDEmailTo': "",
        'RRDEmailFromName': "",
        'RRDEmailFrom': "",
        'RRDEmailSubject': "",
        'RRDEmailMessage': "",
        'RRDEmailSchedule': "",
        'RRDTimeOfDay': "",
        'RRDTimeZone': "",
        'RRDWeeklyDays': "",
        'RRDMonthlyScheduleOptions': "",
        'RRDMonthlyScheduleDayNum': "",
        'RRDMonthlyScheduleDayOrder': "",
        'RRDMonthlyScheduleDayOrderDay': "",
        'RRDWeeklySpaceOutNum': "",
        'RRDMonthlyDayNumSpaceOutNum': "",
        'RRDMonthlyDayOrderSpaceOutNum': "",
        'RRDDailyOptions': "",
        'RRDDailyEveryWeekday': "",
        'RRDDailyEveryDaySpaceOutNum': ""
    };
    this._weekday = {
        "1": "Monday",
        "2": "Tuesday",
        "3": "Wednesday",
        "4": "Thursday",
        "5": "Friday",
        "6": "Saturday",
        "7": "Sunday"};
    this._Filter_Field = {
        "filterdaterange": {},
        "filtersite": {},
        "filterbid": {},
        "filterpricingpay": {},
        "filteradditional": {},
        "ReportType": {},
        "fieldwoInfo": {},
        "fieldwoDescription": {},
        "fieldwoDates": {},
        "fieldwoDatesActual": {},
        "fieldsite": {},
        "fieldbid": {},
        "fieldtechnicians": {},
        "fieldCommunications": {},
        "fieldconfirmation": {},
        "fieldconfirmationClientNote": {},
        "fieldconfirmationTechCloseOut": {},
        "fieldPricing": {},
        "fieldPricingAdditional": {},
        "fieldPricingFinal": {},
        "fieldadditional": {},
        "partmanager": {},
        "wostatus": {},
        "wintag": {},
        "wintagQuantity": {}
    };
    this.secid = {"ulProject": "ulProject",
        "tdwostatus": "tdwostatus",
        "tdfieldwoInfo": "tdfieldwoInfo",
        "tdfieldwoDates": "tdfieldwoDates",
        "tdfieldsite": "tdfieldsite",
        "tdfieldbid": "tdfieldbid",
        "tdfieldtechnicians": "tdfieldtechnicians",
        "tdfieldCommunications": "tdfieldCommunications",
        "tdfieldconfirmation": "tdfieldconfirmation",
        "tdpartmanager": "tdpartmanager",
        "tdwintag": "tdwintag",
        "tdfieldPricing": "tdfieldPricing",
        "tdfieldadditional": "tdfieldadditional"
    };
    //------------------------------------------------------------
    function filterDate() {
        try {
            test = currYr;
        } catch (e) {
            currYr = "";
        }
        var sectionid = $(this).attr("sectionid");
        var section = "td" + $(this).attr("section");
        switch ($(this).find("option:selected").val()) {
            case "Show All":
                showAll(sectionid);
                break;
            case "Today":
                showToday(sectionid);
                break;
            case "ThisWeek":
                showWeek(sectionid);
                break;
            case "LastWeek":
                showLastWeek(sectionid);
                break;
            case "ThisMonth":
                showMonth(sectionid);
                break;
            case "LastMonth":
                lastMonth(sectionid);
                break;
            case "Q1":
                Q1(sectionid);
                break;
            case "Q2":
                Q2(sectionid);
                break;
            case "Q3":
                Q3(sectionid);
                break;
            case "Q4":
                Q4(sectionid);
                break;
            case "H1":
                H1(sectionid);
                break;
            case "H2":
                H2(sectionid);
                break;
            case "YTD":
                YTD(sectionid);
                break;
            case "Tomorrow":
                showTomorrow(sectionid);
                break;
            case "NextBusinessDay":
                showNextBusinessDay(sectionid);
                break;
            case "Next2Days":
                showNext2Day(sectionid);
                break;
            case "Next3Days":
                showNext3Day(sectionid);
                break;
            case "Next4Days":
                showNext4Day(sectionid);
                break;
            case "Next5Days":
                showNext5Day(sectionid);
                break;
            case "Next2BusinessDays":
                showNext2BusinessDay(sectionid);
                break;
            case "Next3BusinessDays":
                showNext3BusinessDay(sectionid);
                break;
            case "Next4BusinessDays":
                showNext4BusinessDay(sectionid);
                break;
            case "Next5BusinessDays":
                showNext5BusinessDay(sectionid);
                break;
            case "Yesterday":
                showYesterday(sectionid);
                break;
            case "LastBusinessDay":
                showLastBusinessDay(sectionid);
                break;
            case "Last2Days":
                showLast2Day(sectionid);
                break;
            case "Last3Days":
                showLast3Day(sectionid);
                break;
            case "Last4Days":
                showLast4Day(sectionid);
                break;
            case "Last5Days":
                showLast5Day(sectionid);
                break;
            case "Last2BusinessDays":
                showLast2BusinessDay(sectionid);
                break;
            case "Last3BusinessDays":
                showLast3BusinessDay(sectionid);
                break;
            case "Last4BusinessDays":
                showLast4BusinessDay(sectionid);
                break;
            case "Last5BusinessDays":
                showLast5BusinessDay(sectionid);
                break;
            case "TodayAndTomorrow":
                showTodayAndTomorrow(sectionid);
                break;
            case "TodayAndNextBusinessDay":
                showTodayAndNextBusinessDay(sectionid);
                break;
        }
    }

    //------------------------------------------------------------
    function showAll(sectionid) {
        $("#StartDate" + sectionid).attr("value", "");
        $("#EndDate" + sectionid).attr("value", "");
    }

    //------------------------------------------------------------
    function showToday(sectionid) {
        $("#StartDate" + sectionid).attr("value", Today);
        $("#EndDate" + sectionid).attr("value", Today);
    }

    //------------------------------------------------------------
    function showWeek(sectionid) {
        $("#StartDate" + sectionid).attr("value", BOW);
        $("#EndDate" + sectionid).attr("value", EOW);
    }

    //------------------------------------------------------------
    function showLastWeek(sectionid) {
        $("#StartDate" + sectionid).attr("value", BOLW);
        $("#EndDate" + sectionid).attr("value", EOLW);
    }

    //------------------------------------------------------------
    function showMonth(sectionid) {
        $("#StartDate" + sectionid).attr("value", BOM);
        $("#EndDate" + sectionid).attr("value", EOM);
    }

    //------------------------------------------------------------
    function lastMonth(sectionid) {
        $("#StartDate" + sectionid).attr("value", PrevBOM);
        $("#EndDate" + sectionid).attr("value", PrevEOM);
    }

    //------------------------------------------------------------
    function Q1(sectionid) {
        $("#StartDate" + sectionid).attr("value", Q1Start);
        $("#EndDate" + sectionid).attr("value", Q1End);
    }

    //------------------------------------------------------------
    function Q2(sectionid) {
        $("#StartDate" + sectionid).attr("value", Q2Start);
        $("#EndDate" + sectionid).attr("value", Q2End);
    }

    //------------------------------------------------------------
    function Q3(sectionid) {
        $("#StartDate" + sectionid).attr("value", Q3Start);
        $("#EndDate" + sectionid).attr("value", Q3End);
    }

    //------------------------------------------------------------
    function Q4(sectionid) {
        $("#StartDate" + sectionid).attr("value", Q4Start);
        $("#EndDate" + sectionid).attr("value", Q4End);
    }

    //------------------------------------------------------------
    function H1(sectionid) {
        $("#StartDate" + sectionid).attr("value", H1Start);
        $("#EndDate" + sectionid).attr("value", H1End);
    }

    //------------------------------------------------------------
    function H2(sectionid) {
        $("#StartDate" + sectionid).attr("value", H2Start);
        $("#EndDate" + sectionid).attr("value", H2End);
    }

    //------------------------------------------------------------
    function YTD(sectionid) {
        $("#StartDate" + sectionid).attr("value", YTDStart);
        $("#EndDate" + sectionid).attr("value", YTDEnd);
    }

    //------------------------------------------------------------
    function showTomorrow(sectionid) {
        $("#StartDate" + sectionid).attr("value", Tomorrow);
        $("#EndDate" + sectionid).attr("value", Tomorrow);
    }

    //------------------------------------------------------------
    function showYesterday(sectionid) {
        $("#StartDate" + sectionid).attr("value", Yesterday);
        $("#EndDate" + sectionid).attr("value", Yesterday);
    }

    //------------------------------------------------------------
    function showNextBusinessDay(sectionid) {
        $("#StartDate" + sectionid).attr("value", Nextbusinessday);
        $("#EndDate" + sectionid).attr("value", Nextbusinessday);
    }

    //------------------------------------------------------------
    function showNext2Day(sectionid) {
        $("#StartDate" + sectionid).attr("value", Tomorrow);
        $("#EndDate" + sectionid).attr("value", Next2days);
    }

    //------------------------------------------------------------
    function showNext3Day(sectionid) {
        $("#StartDate" + sectionid).attr("value", Tomorrow);
        $("#EndDate" + sectionid).attr("value", Next3days);
    }

    //------------------------------------------------------------
    function showNext4Day(sectionid) {
        $("#StartDate" + sectionid).attr("value", Tomorrow);
        $("#EndDate" + sectionid).attr("value", Next4days);
    }

    //------------------------------------------------------------
    function showNext5Day(sectionid) {
        $("#StartDate" + sectionid).attr("value", Tomorrow);
        $("#EndDate" + sectionid).attr("value", Next5days);
    }

    //------------------------------------------------------------
    function showNext2BusinessDay(sectionid) {
        $("#StartDate" + sectionid).attr("value", NextDaystart);
        $("#EndDate" + sectionid).attr("value", Next2businessdays);
    }

    //------------------------------------------------------------
    function showNext3BusinessDay(sectionid) {
        $("#StartDate" + sectionid).attr("value", NextDaystart);
        $("#EndDate" + sectionid).attr("value", Next3businessdays);
    }

    //------------------------------------------------------------
    function showNext4BusinessDay(sectionid) {
        $("#StartDate" + sectionid).attr("value", NextDaystart);
        $("#EndDate" + sectionid).attr("value", Next4businessdays);
    }

    //------------------------------------------------------------
    function showNext5BusinessDay(sectionid) {
        $("#StartDate" + sectionid).attr("value", NextDaystart);
        $("#EndDate" + sectionid).attr("value", Next5businessdays);
    }

    //------------------------------------------------------------
    function showLastBusinessDay(sectionid) {
        $("#StartDate" + sectionid).attr("value", Lastbusinessday);
        $("#EndDate" + sectionid).attr("value", Lastbusinessday);
    }

    //------------------------------------------------------------
    function showLast2Day(sectionid) {
        $("#StartDate" + sectionid).attr("value", Last2days);
        $("#EndDate" + sectionid).attr("value", Yesterday);
    }

    //------------------------------------------------------------
    function showLast3Day(sectionid) {
        $("#StartDate" + sectionid).attr("value", Last3days);
        $("#EndDate" + sectionid).attr("value", Yesterday);
    }

    //------------------------------------------------------------
    function showLast4Day(sectionid) {
        $("#StartDate" + sectionid).attr("value", Last4days);
        $("#EndDate" + sectionid).attr("value", Yesterday);
    }

    //------------------------------------------------------------
    function showLast5Day(sectionid) {
        $("#StartDate" + sectionid).attr("value", Last5days);
        $("#EndDate" + sectionid).attr("value", Yesterday);
    }

    //------------------------------------------------------------
    function showLast2BusinessDay(sectionid) {
        $("#StartDate" + sectionid).attr("value", Last2businessdays);
        $("#EndDate" + sectionid).attr("value", LastDayend);
    }

    //------------------------------------------------------------
    function showLast3BusinessDay(sectionid) {
        $("#StartDate" + sectionid).attr("value", Last3businessdays);
        $("#EndDate" + sectionid).attr("value", LastDayend);
    }

    //------------------------------------------------------------
    function showLast4BusinessDay(sectionid) {
        $("#StartDate" + sectionid).attr("value", Last4businessdays);
        $("#EndDate" + sectionid).attr("value", LastDayend);
    }

    //------------------------------------------------------------
    function showLast5BusinessDay(sectionid) {
        $("#StartDate" + sectionid).attr("value", Last5businessdays);
        $("#EndDate" + sectionid).attr("value", LastDayend);
    }

    //------------------------------------------------------------
    function showTodayAndTomorrow(sectionid) {
        $("#StartDate" + sectionid).attr("value", Today);
        $("#EndDate" + sectionid).attr("value", Tomorrow);
    }

    //------------------------------------------------------------
    function showTodayAndNextBusinessDay(sectionid) {
        $("#StartDate" + sectionid).attr("value", Today);
        $("#EndDate" + sectionid).attr("value", Nextbusinessday);
    }

    //------------------------------------------------------------
    this.fanxyboxresize = function()
    {
        $("#fancybox-wrap").css("width", "600px");
        $("#fancybox-content").css("width", "580px");
        $.fancybox.resize();
    };
    //------------------------------------------------------------
    this.openpopup = function(html)
    {
        $("<div></div>").fancybox(
                {
                    'autoDimensions': true,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'content': html,
                    'title': ""
                }
        ).trigger('click');
    };
    //------------------------------------------------------------
    this.cmdShowFilterOrField_click = function()
    {
        if ($(this).attr("id") === "cmdChooseFilters")
        {
            $("#divfilters").css("display", "");
            $("#divColoumns").css("display", "none");
        }
        else
        {
            $("#divfilters").css("display", "none");
            $("#divColoumns").css("display", "");
        }
    };
    //------------------------------------------------------------
    this.addremovefilteritem_click = function()
    {
        var headerinfo = "";
        var title = "Filters";
        var id = $(this).attr("id");
        if (id.search("field") !== -1)
        {
            title = "Fields";
        }

        if (id === "fieldbid" || id === "partmanager" || id === "fieldwoDates")
        {
            var headerstr = "";
            if (id === "fieldbid")
            {
                headerstr = "To enable reporting on individual bids, select 'Bid Report' in the 'Report Type' section above.";
            }
            else if (id === "partmanager")
            {
                headerstr = "To enable reporting on individual Parts, select 'Parts Report' in the 'Report Type' section above.";
            }
            else if (id === "fieldwoDates")
            {
                headerstr = "To enable reporting on individual Visits, select 'Visits Report' in the 'Report Type' section above.";
            }
            else if (id === "wintag")
            {
                headerstr = "To enable reporting on individual Visits, select 'WIN-Tags&trade; Report' in the 'Report Type' section above.";
            }

            headerinfo = "<div>\n\
                <table cellspacing='0' cellpadding='0' width='100%'>\n\
                    <col width='5%'>\n\
                    <col width='95%'>\n\
                    <tr>\n\
                        <td colspan='2'>\n\
                        " + headerstr + "</td>\n\
                    </tr>\n\
                    <tr>\n\
                        <td colspan='2'>\n\
                            <hr style='height:1px;background: none repeat scroll 0 0 #dddddd;margin:0px 0px 5px;'>\n\
                        </td>\n\
                    </tr>\n\
                </table>\n\
            </div>";
        }

        var html = '\
            <div>\n\
                <div style="text-align:center;">\n\
                    <div><u><b>Select ' + title + '</b></u></div>\n\
                    <div style="float:right;margin-top:-16px;">\n\
                        <a section="' + id + '" href="javascript:;" id="cmduncheckall" checkall="1" style="display:none;">Uncheck All</a>&nbsp;&nbsp;&nbsp;\n\
                        <a section="' + id + '" href="javascript:;" id="cmdcheckall" checkall="0">Check All</a>\n\
                    </div><br/>\n\
                </div>' + headerinfo + '\n\
                <div id="ContainerID_' + id + '" style="max-height: 300px; overflow: auto;">\n\
                </div>\n\
            </div>';
        Me.openpopup(html);
        $("#cmdcheckall").unbind("click").bind("click", Me.checkall_click);
        $("#cmduncheckall").unbind("click").bind("click", Me.checkall_click);
//        $("#" + id + "_ID").unbind("click").bind("click", Me.filterbid_ID_click);
        var ComID = $("#hidComID").val();
        Me.loadFilterFieldList(ComID, "ContainerID_" + id, id);
    };
    //------------------------------------------------------------
    this.loadFilterFieldList = function(ComID, ContainerID, TypeID)
    {
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/custom-report/load-filter-field/",
            dataType: 'html',
            cache: false,
            context: this,
            data: {ComID: ComID, TypeID: TypeID},
            success: function(data, status, xhr) {
                Me.onLoadSuccess(ContainerID, data);
            }
        });
    };
    //------------------------------------------------------------
    this.onLoadSuccess = function(ContainerID, val)
    {
        $("#" + ContainerID).html(val);
        if (ContainerID === "ContainerID_fieldconfirmation" || ContainerID === "ContainerID_fieldPricing")
        {
            $("#fancybox-wrap").css("width", "700px");
            $("#fancybox-content").css("width", "680px");
            $.fancybox.resize();
        }
        else
        {
            Me.fanxyboxresize();
        }
        $("#" + ContainerID + " input").unbind("click").bind("click", Me.getItem);
        Me.fillDataOnPage(ContainerID);
        var valueSpilit = ContainerID.split("_");
        var elem = null;
        if (valueSpilit[1] === "filterpricingpay")
        {
            $("#" + ContainerID + " #Amount_Per").attr("checked", "checked");
            elem = $("#" + ContainerID + " #Amount_Per");
        }
        else if (valueSpilit[1] === "fieldbid")
        {
            Me.callfilterbid_ID_click("#rptBid");
        }
        else if (valueSpilit[1] === "fieldwoDates")
        {
            Me.callfilterbid_ID_click("#rptVisit");
        }
        else if (valueSpilit[1] === "partmanager")
        {
            Me.callfilterbid_ID_click("#rptPart");
        }
        else if (valueSpilit[1] === "wintag")
        {
            Me.callfilterbid_ID_click("#rptWintag");
        }
    };

    //------------------------------------------------------------
    this.getItem = function()
    {
        var section = $(this).attr("section");
        var i = 0;
        $("#ContainerID_" + section + " input").each(
                function() {
                    if ($(this).is(":checked"))
                    {
                        i += 1;
                    }
                });
        if (i === 0)
        {
            $("#cmduncheckall").hide();
        }
        else if (i === $("#ContainerID_" + section + " input").length)
        {
            $("#cmduncheckall").show();
            $("#cmdcheckall").hide();
        }
        else
        {
            $("#cmduncheckall").show();
            $("#cmdcheckall").show();
        }
        var sectionid = $(this).attr(section + "id");
        var sectionname = $(this).attr(section + "name");
        if ($(this).is(":checked"))
        {

            Me.generateItem(section, sectionid, sectionname);
            Me.isCreateBidsReport(section);
        }
        else
        {
            Me.removeItem(section, sectionid);
            Me.isCreateBidsReport(section);
        }
        Me.changetitle(section);
    };
    //------------------------------------------------------------
    this.fillDataOnPage = function(ContainerID)
    {
        var section = ContainerID.split('_');
        var parentstr = "";
        if (section[1] === "wostatus")
        {
            parentstr = "td" + section[1] + " img";
        }
        else if (Me._Filter_Field[section[1]])
        {
            parentstr = "td" + section[1] + " tr";
        }
        else
        {
            parentstr = "ul" + section[1] + " li";
        }
        var arrmainsec = ["fieldwoInfo", "fieldwoDates", "wintag", "fieldconfirmation", "fieldPricing"];
        if ($.inArray(section[1], arrmainsec) !== -1)
        {
            var i = 0;
            var sectionarr = "";
            if (section[1] === "fieldwoInfo")
            {
                sectionarr = ["fieldwoInfo", "fieldwoDescription"];
            }
            else if (section[1] === "fieldwoDates")
            {
                sectionarr = ["fieldwoDates", "fieldwoDatesActual"];
            }
            else if (section[1] === "wintag")
            {
                sectionarr = ["wintag", "wintagQuantity"];
            }
            else if (section[1] === "fieldconfirmation")
            {
                sectionarr = ["fieldconfirmation", "fieldconfirmationClientNote", "fieldconfirmationTechCloseOut"];
            }
            else if (section[1] === "fieldPricing")
            {
                sectionarr = ["fieldPricing", "fieldPricingAdditional", "fieldPricingFinal"];
            }
            $.each(sectionarr, function(id, val) {
                $("#td" + val + " tr").each(
                        function()
                        {
                            $("#ContainerID_" + section[1] + " #" + $(this).attr(section[1] + "id")).attr("checked", true);
                            $("#ContainerID_" + section[1] + " #" + $(this).attr(section[1] + "id")).removeAttr("disabled");
                            i += 1;
                        }
                );
            });
        }
        else
        {
            var i = 0;
            $("#" + parentstr).each(
                    function()
                    {
                        $("#ContainerID_" + section[1] + " #" + $(this).attr(section[1] + "id")).attr("checked", true);
                        $("#ContainerID_" + section[1] + " #" + $(this).attr(section[1] + "id")).removeAttr("disabled");
                        i += 1;
                    }
            );
        }
        if (i === 0)
        {
            $("#cmduncheckall").hide();
        }
        else if (i === $("#ContainerID_" + section[1] + " input").length)
        {
            $("#cmduncheckall").show();
            $("#cmdcheckall").hide();
        }
        else
        {
            $("#cmduncheckall").show();
            $("#cmdcheckall").show();
        }

        if (section[1] === "filterbid" || section[1] === "fieldbid")
        {
            if ($("#CreateBidsReport").val() === "1")
            {
                $("#ContainerID_" + section[1] + " tr input").removeAttr("disabled");
                $("#" + section[1] + "_ID").attr("checked", true);
            }
        }
    };
    //------------------------------------------------------------
    this.isCreateBidsReport = function(section)
    {
        if (section === "filterbid" || section === "fieldbid")
        {
            $("#ContainerID_" + section + " tr input").each(
                    function() {
                        if ($(this).attr("id") !== "Qty_Applicants" && $(this).is(":checked"))
                        {
                            $("#CreateBidsReport").val("1");
                            return;
                        }
                    });
        }
        var filterbidnum = 0;
        $("#tdfilterbid img").each(
                function() {
                    if ($(this).attr("filterbidid") !== "Qty_Applicants")
                    {
                        filterbidnum += 1;
                        return;
                    }
                });
        var fieldbidnum = 0;
        $("#tdfieldbid img").each(
                function() {
                    if ($(this).attr("fieldbidid") !== "Qty_Applicants")
                    {
                        fieldbidnum += 1;
                        return;
                    }
                });
        if (filterbidnum > 0 || fieldbidnum > 0)
        {
            $("#CreateBidsReport").val("1");
        }
        else
        {
            $("#CreateBidsReport").val("0");
        }
    };
    //------------------------------------------------------------
    this.resetsort = function() {
        var param = {
            reportid: $("#rptid").val()
        };
        $.ajax({
            url: 'custom_report_resetsort.php',
            dataType: 'html',
            data: param,
            cache: false, type: 'POST',
            context: this,
            success: function(data, status, xhr) {
                $("#showhide2").html(data);
                $("#sort1").unbind('change').bind("change", Me.sort_onchange);
                $("#sort2").unbind('change').bind("change", Me.sort_onchange);
                $("#sort3").unbind('change').bind("change", Me.sort_onchange);
            },
            error: function(xhr, status, error) {
            }
        });
    };
    //------------------------------------------------------------
    this.resetfilter = function() {
        var param = {
            reportid: $("#rptid").val(),
            company: Me.company
        };
        $.ajax({
            url: 'custom_report_resetfilter.php',
            dataType: 'html',
            data: param,
            cache: false, type: 'POST',
            context: this,
            success: function(data, status, xhr) {
                $("#showhide3").html(data);
                $(".DropdownBoxchange").unbind("change").bind("change", Me.DropdownBox_onChange);
                $(".txtN2").unbind("blur").bind("blur", Me.txtN2blur);
                $(".txtN2").unbind("focus").bind("focus", Me.txtN2focus);
                $('.showCal').calendar({dateFormat: 'MDY/'});
                $(".cmdremovedropdownitem").unbind("click").bind("click", Me.cmdremovedropdownitem_click);
                $(".clsdatetime").unbind("change").bind("change", filterDate);
            },
            error: function(xhr, status, error) {
            }
        });
    };
    //------------------------------------------------------------
    this.checkall_click = function()
    {
        if ($(this).attr("checkall") === "0")
        {
            $("#ContainerID_" + $(this).attr("section") + " input:checkbox:enabled").attr("checked", true);
            $("#cmduncheckall").show();
            $("#cmdcheckall").hide();
        }
        else
        {
            $("#ContainerID_" + $(this).attr("section") + " input:checkbox:enabled").removeAttr("checked");
            $("#cmduncheckall").hide();
            $("#cmdcheckall").show();
        }
        Me.putItemToParentSection($(this).attr("section"));
        if ($(this).attr("section") === "filterbid")
        {
            Me.isCreateBidsReport($(this).attr("section"));
        }

    };

    //------------------------------------------------------------
    this.callfilterbid_ID_click = function(elem)
    {
        var section = $(elem).attr("section");
        var sectionname = {"#rptBid": "fieldbid", "#rptPart": "partmanager", "#rptVisit": ["fieldwoDates", "fieldwoDatesActual"], "#rptWintag": ["wintag", "wintagQuantity"]};
        if ($(elem).is(":checked"))
        {
            $("#ContainerID_" + section + " input").each(
                    function() {
                        $(this).removeAttr("disabled");
                    });
            if (elem === "#rptBid" || elem === "#rptPart")
            {
                if ($("#td" + sectionname[elem] + " tr").length === 0)
                {
                    $("#cmdcheckall").click();
                }
            }
            else if (elem === "#rptVisit" || elem === "#rptWintag")
            {
                var itemno = 0;
                $.each(sectionname[elem], function(id, val) {
                    itemno += $("#td" + sectionname[elem][id] + " tr").length;
                });
                if (itemno === 0)
                {
                    var checkedarr = "";
                    if (elem === "#rptVisit")
                    {
                        checkedarr = ["_VisitStartTypeID", "_VisitStartDate", "_VisitStartTime", "_VisitEndDate", "_VisitEndTime", "_VisitActualCheckInDate", "_VisitActualCheckInTime", "_VisitActualCheckInResult", "_VisitActualCheckOutDate", "_VisitActualCheckOutTime", "_VisitActualCheckOutResult", "_VisitActualDurationResult"];
                    }
                    else if (elem === "#rptWintag")
                    {
                        checkedarr = ["_WINTagAddedDate", "_WINTag", "_WINTagOwner", "_WINTagDetails", "_WINTagNotes", "_WINTagBillable"];
                    }

                    $.each(checkedarr, function(id, val) {
                        $("#" + val).attr("checked", true);
                        var sectionid = $("#" + val).attr(section + "id");
                        var sectionname = $("#" + val).attr(section + "name");
                        if ($("#" + val).is(":checked"))
                        {
                            Me.generateItem(section, sectionid, sectionname);
                            Me.isCreateBidsReport(section);
                        }
                    });
                }
                Me.changetitle(section);
            }
        }
        var i = 0;
        $("#ContainerID_" + section + " input").each(
                function() {
                    if ($(this).is(":checked"))
                    {
                        i += 1;
                    }
                });
        if ($("#Qty_Applicants").attr("disabled") && i > 0)
        {
            i = i - 1;
        }

        if (i === 0)
        {
            $("#cmduncheckall").hide();
        }
        else
        {
            $("#cmduncheckall").show();
        }
    };
    //------------------------------------------------------------
    this.putItemToParentSection = function(section)
    {
        $("#ContainerID_" + section + " input:checkbox").each(
                function() {
                    var sectionid = $(this).attr(section + "id");
                    var sectionname = $(this).attr(section + "name");
                    if ($(this).is(":checked"))
                    {
                        Me.generateItem(section, sectionid, sectionname);
                    }
                    else
                    {
                        Me.removeItem(section, sectionid);
                    }
                });
        Me.changetitle(section);
    };

    //------------------------------------------------------------
    this.builncoltable = function(section, rownoarr, checkedItem)
    {
        if (typeof(Me.FieldOrder[section]) !== "undefined")
        {
            var colno = 1;
            if (checkedItem.length > 5)
            {
                colno = 2;
            }

            var rowno = 0;
            $.each(rownoarr, function(id, val) {
                if (rowno < val)
                {
                    rowno = val;
                }
            });
            var str = "";
            var i = 0;
            str = "<table id='td" + section + "' width='100%' cellspacing='0' cellpadding='0' border='0'>\n\
                        <col width='18px'>\n\
                                <col width='48%'>\n\
                                <col width='18px'>\n\
                                <col width='49%'>";
            for (i = 0; i < rowno; i++)
            {
                if (typeof(checkedItem[i]) !== "undefined")
                {
                    str += "<tr style='padding:5px;'>";
                    str += "<td style='vertical-align:top;'>\n\
                                <img id='" + section + checkedItem[i]["sectionid"] + "' " + section + "_type='" + checkedItem[i]["type"] + "' " + section + "name='" + checkedItem[i]["sectionname"] + "' class='cmdremove' height='15px' src='/widgets/css/images/removedoc.png' style='cursor:pointer;' " + section + "id='" + checkedItem[i]["sectionid"] + "' section='" + section + "' filterid=''>\n\
                            </td>";
                    str += Me.buildcontrolbyType(section, checkedItem[i]["sectionid"], checkedItem[i]["type"], checkedItem[i]["sectionname"]);
                    if (colno > 1)
                    {
                        if (typeof(checkedItem[(i + rowno)]) !== "undefined")
                        {
                            str += "<td style='vertical-align:top;'>\n\
                                        <img id='" + section + checkedItem[(i + rowno)]["sectionid"] + "' " + section + "_type='" + checkedItem[(i + rowno)]["type"] + "' " + section + "name='" + checkedItem[(i + rowno)]["sectionname"] + "' class='cmdremove' height='15px' src='/widgets/css/images/removedoc.png' style='cursor:pointer;' " + section + "id='" + checkedItem[(i + rowno)]["sectionid"] + "' section='" + section + "' filterid=''>\n\
                                    </td>";
                            str += Me.buildcontrolbyType(section, checkedItem[(i + rowno)]["sectionid"], checkedItem[(i + rowno)]["type"], checkedItem[(i + rowno)]["sectionname"]);
                        }
                    }
                    else
                    {
                        str += "<td style='vertical-align:top;'></td><td style='vertical-align:top;'></td>";
                    }
                    str += "</tr>";
                }
            }
            str += "</table>";
            $("#div" + section).html(str);
        }
    };

    //------------------------------------------------------------
    this.changetitle = function(section)
    {
        var parentstr = "";
        if (Me._Filter_Field[section])
        {
            parentstr = "td" + section + " tr";
        }
        else
        {
            parentstr = "ul" + section + " li";
        }

        var numItem = 0;
        numItem = $("#" + parentstr).length;
        if (section === "fieldwoInfo")
        {
            numItem += $("#tdfieldwoDescription tr").length;
        }
        else if (section === "fieldwoDates")
        {
            numItem += $("#tdfieldwoDatesActual tr").length;
        }
        else if (section === "wintag")
        {
            numItem += $("#tdwintagQuantity tr").length;
        }
        else if (section === "fieldconfirmation")
        {
            numItem += $("#tdfieldconfirmationClientNote tr").length;
            numItem += $("#tdfieldconfirmationTechCloseOut tr").length;
        }
        else if (section === "fieldPricing")
        {
            numItem += $("#tdfieldPricingAdditional tr").length;
            numItem += $("#tdfieldPricingFinal tr").length;
        }

        if (numItem > 0)
        {
            $("#" + section).html("(select/remove)");
            $("#" + section).parent().find("a").each(
                    function() {
                        if ($(this).attr("section") === section)
                            Me.expanddiv(this);
                    });
        }

        if (numItem <= 0)
        {
            $("#" + section).html("(select)");
            $("#" + section).parent().find("a").each(
                    function() {
                        if ($(this).attr("section") === section)
                            Me.collapseddiv(this);
                    });
        }
    };
    //------------------------------------------------------------
    this.generateItem = function(section, sectionid, sectionname)
    {
        if ($("#" + section + sectionid).html() === null)
        {
            var type = $("#ContainerID_" + section + " #" + sectionid).attr(section + "_type");
            var ordernum = "";
            var str = "";

            if (section === "Project")
            {
                if (type === "" || typeof(type) === "undefined")
                {
                    str = "<li id=\"" + section + sectionid + "\" " + section + "id=\"" + sectionid + "\" " + sectionid + "_type=\"" + type + "\" class=\"listitem\">\n\
                            <img class=\"cmdremove\" height=\"15px\" src=\"/widgets/css/images/removedoc.png\" style=\"cursor:pointer;\" " + section + "id=\"" + sectionid + "\" section=\"" + section + "\" filterid='' fieldname=\"" + sectionname + "\"> " + sectionname + "\
                    </li>";
                    $("#ul" + section).append(str);
                }
                else
                {
                    ordernum = Me.FieldOrder[section][sectionid];
                    str = Me.buildcontrol(section, sectionid, type, sectionname, ordernum);
                    $("#td" + section).append(str);
                    Me.sortlist(section);
                }
            }
            else
            {
                ordernum = Me.FieldOrder[section][sectionid];
                str = Me.buildcontrol(section, sectionid, type, sectionname, ordernum);
                $("#td" + section).append(str);
                Me.sortlist(section);
            }

            $(".txtN2").unbind("blur").bind("blur", Me.txtN2blur);
            $(".txtN2").unbind("focus").bind("focus", Me.txtN2focus);
            $('.showCal').calendar({dateFormat: 'MDY/'});

            $(".clsdatetime").unbind("change").bind("change", filterDate);
            $(".DropdownBoxchange").unbind("change").bind("change", Me.DropdownBox_onChange);

            if (type === "DT")
            {
                $("#Date_" + sectionid).change();
            }
            else if (type === "D")
            {

            }
            $(".cmdremove").unbind("click").bind("click", Me.removeItem_click);
        }
    };

    //------------------------------------------------------------
    this.buildcontrolbyType = function(section, sectionid, type, sectionname)
    {
        var str = "";
        var spacing = "";
        if (section === "fieldwoDates")
        {
            spacing = "width:80%;";
        }

        if (type === "D")
        {
            str += "<td style='vertical-align:top;'>" + sectionname + "</td>\n\
                    <td style='vertical-align:top;" + spacing + "'>\n\
                        <select id='" + sectionid + "' style='width:140px' class='DropdownBoxchange' sectionid='" + sectionid + "'>" + Me.loadDropdownBoxList($("#hidComID").val(), sectionid, section) + "</select><br/>";
        }
        else if (type === "T")
        {
            str += "<td style='vertical-align:top;'>" + sectionname + "</td>\n\
                    <td style='vertical-align:top;" + spacing + "'><input type='text' id='" + sectionid + "' style='width:138px'/>";
        }
        else if (type === "DT")
        {
            var year = (new Date).getFullYear();
            str += "<td style=vertical-align:top;'>" + sectionname + "</td>\n\
                        <td style='vertical-align:top;" + spacing + "'>\n\
                                            <select class='clsdatetime' id='Date_" + sectionid + "' style='width:210px' sectionid='" + sectionid + "' section='" + section + "'>\n\
                                                <option value='FixedDateRange' selected>Fixed Date Range</option>\n\
                                                <option value='Today'>Today</option>\n\
                                                <option value='TodayAndTomorrow'>Today and Tomorrow</option>\n\
                                                <option value='TodayAndNextBusinessDay'>Today and Next Business Day</option>\n\
                                                <option value='Tomorrow'>Tomorrow</option>\n\
                                                <option value='NextBusinessDay'>Next Business Day</option>\n\
                                                <option value='Next2Days'>Next 2 Days</option>\n\
                                                <option value='Next2BusinessDays'>Next 2 Business Days</option>\n\
                                                <option value='Next3Days'>Next 3 Days</option>\n\
                                                <option value='Next3BusinessDays'>Next 3 Business Days</option>\n\
                                                <option value='Next4Days'>Next 4 Days</option>\n\
                                                <option value='Next4BusinessDays'>Next 4 Business Days</option>\n\
                                                <option value='Next5Days'>Next 5 Days</option>\n\
                                                <option value='Next5BusinessDays'>Next 5 Business Days</option>\n\
                                                <option value='ThisWeek'>This Week</option>\n\
                                                <option value='ThisMonth'>This Month</option>\n\
                                                <option value='Yesterday'>Yesterday</option>\n\
                                                <option value='LastBusinessDay'>Last Business Day</option>\n\
                                                <option value='Last2Days'>Last 2 Days</option>\n\
                                                <option value='Last2BusinessDays'>Last 2 Business Days</option>\n\
                                                <option value='Last3Days'>Last 3 Days</option>\n\
                                                <option value='Last3BusinessDays'>Last 3 Business Days</option>\n\
                                                <option value='Last4Days'>Last 4 Days</option>\n\
                                                <option value='Last4BusinessDays'>Last 4 Business Days</option>\n\
                                                <option value='Last5Days'>Last 5 Days</option>\n\
                                                <option value='Last5BusinessDays'>Last 5 Business Days</option>\n\
                                                <option value='LastWeek'>Last Week</option>\n\
                                                <option value='LastMonth'>Last Month</option>\n\
                                                <option value='Q1'>Q1 " + year + "</option>\n\
                                                <option value='Q2'>Q2 " + year + "</option>\n\
                                                <option value='Q3'>Q3 " + year + "</option>\n\
                                                <option value='Q4'>Q4 " + year + "</option>\n\
                                                <option value='H1'>H1 " + year + "</option>\n\
                                                <option value='H2'>H2 " + year + "</option>\n\
                                                <option value='YTD'>YTD</option>\n\
                                            </select>\n\
                                        &nbsp;between\n\
                                        &nbsp;<input type='text' id='StartDate" + sectionid + "' class='showCal' style='width:65px' value=''>\n\
                                        &nbsp;and\n\
                                        &nbsp;<input type='text' id='EndDate" + sectionid + "' class='showCal' style='width:65px' value=''>\n\
                                    ";
        }
        else if (type === "N2")
        {
            str += "<td style='vertical-align:top;'>" + sectionname + "</td>\n\
                    <td style='vertical-align:top;" + spacing + "'>\n\
                        from <input class='txtN2' type='text' id='" + sectionid + "__from' " + sectionid + "_type='N2' style='width:50px;color:#a6a6a6;' value='#.##'/>\n\
                        &nbsp;&nbsp;&nbsp;to <input class='txtN2' type='text' id='" + sectionid + "__to' " + sectionid + "_type='N2' style='width:50px;color:#a6a6a6;' value='#.##'/>";
        }
        else if (type === "T2")
        {
            str += "<td style='vertical-align:top;'>" + sectionname + "</td>\n\
                    <td style='vertical-align:top;" + spacing + "'>\n\
                        from <select style='width:100px' id='" + sectionid + "__from' " + sectionid + "_type='T2'> " + Me.getT2options() + "</select>\n\
                        &nbsp;&nbsp;&nbsp;to <select  style='width:100px' id='" + sectionid + "__to' " + sectionid + "_type='T2'> " + Me.getT2options() + "</select>\n\
                        &nbsp;&nbsp;&nbsp;<select id='" + sectionid + "est' style='width:150px;'>\n\
                                <option value ='-10'>Hawaii (GMT-10:00)</option>\n\
                                <option value ='-9'>Alaska (GMT-8:00)</option>\n\
                                <option value ='-8'>Pacific (GMT-7:00)</option>\n\
                                <option value ='-7'>Mountain (GMT-6:00)</option>\n\
                                <option value ='-6' selected>Central (GMT-5:00)</option>\n\
                                <option value ='-5' >Eastern (GMT-4:00)</option>\n\
                            </select>";
        }
        else if (type === "DYN")
        {
            str += "<td style='vertical-align:top;'>" + sectionname + "</td>\n\
                    <td style='vertical-align:top;" + spacing + "'>\n\
                <select id='Date_" + sectionid + "' style='width:140px' sectionid='" + sectionid + "' section='" + section + "'>\n\
                                                <option value='t'>Yes</option>\n\
                                                <option value='f'>No</option>\n\
                                                <option value='a' selected>Any</option>\n\
                    </select>";
        }
        else if (type === "S")
        {
            str += "<td style='vertical-align:top;'>" + sectionname + "</td>\n\
                    <td style='vertical-align:top;'>";
        }
        else if (type === "")
        {
            if (section === "wostatus")
            {
                str += "<td style='vertical-align:top;'>" + sectionname;
            }
            else
            {
                str += "<td style='vertical-align:top;' colspan='2'>" + sectionname;
            }
        }

        str += "</td>";
        return str;
    };
    //------------------------------------------------------------
    this.buildcontrol = function(section, sectionid, type, sectionname, order)
    {
        var str = "";
        str = "<tr ordernum='" + order + "' style='padding:5px;' id='" + section + sectionid + "' " + section + "id='" + sectionid + "' " + sectionid + "_type='" + type + "'>\n\
                    <td style='vertical-align:top;'>\n\
                        <img class='cmdremove' height='15px' src='/widgets/css/images/removedoc.png' style='cursor:pointer;' " + section + "id='" + sectionid + "' section='" + section + "' filterid=''>\n\
                    </td>";
        str += Me.buildcontrolbyType(section, sectionid, type, sectionname);
        str += "</tr>";
        return str;
    };
    //------------------------------------------------------------
    // accending sort
    function asc_sort(a, b)
    {
        return (parseInt($(b).attr("ordernum"))) < (parseInt($(a).attr("ordernum"))) ? 1 : -1;
    }

    //------------------------------------------------------------
    // decending sort
    function dec_sort(a, b)
    {
        return ($(b).text()) > ($(a).text()) ? 1 : -1;
    }

    //------------------------------------------------------------
    this.sortlist = function(id)
    {
        $("#td" + id + " tr").sort(asc_sort).appendTo('#td' + id);
    };
    //------------------------------------------------------------
    this.txtN2blur = function()
    {
        if ($(this).val() === "#.##" || $(this).val() === "")
        {
            $(this).css("color", "#a6a6a6");
            $(this).val("#.##");
        }
        else
        {
            $(this).css("color", "#000000");
        }
    };
    //------------------------------------------------------------
    this.txtN2focus = function()
    {
        if ($(this).val() === "#.##")
        {
            $(this).val("");
            $(this).css("color", "#000000");
        }
    };
    //------------------------------------------------------------
    this.getT2options = function()
    {
        var timepart3 = 0;
        var timepart2 = "";
        var timepart1 = "";
        var Time = "";
        var timestamp = "";
        Time += "<option value=''>Select Time</option>";
        for (i = 0; i < 24; i++)
        {
            for (j = 1; j < 5; j++) {
                if (j === 1)
                    timepart2 = "00";
                else
                    timepart2 = (j - 1) * 15;
                if (i < 12 || i === 24)
                    timepart3 = "AM";
                else
                    timepart3 = "PM";
                if (i === 0)
                    timepart1 = "12";
                else if (i < 13 && i > 0)
                    timepart1 = i;
                else
                    timepart1 = i - 12;
                timestamp = timepart1 + ":" + timepart2 + " " + timepart3;
                Time += "<option value='" + timestamp + "'>" + timestamp + "</option>";
            }
        }
        return Time;
    };
    //------------------------------------------------------------
    this.removeItem = function(section, sectionid)
    {
        if (Me.FieldOrder[section])
        {
            $("#td" + section + " #" + section + sectionid).remove();
        }
        else
        {
            $("#ul" + section + " #" + section + sectionid).remove();
        }
        Me.changetitle(section);
    };
    //------------------------------------------------------------
    this.DropdownBox_onChange = function()
    {
        var section = "";
        var key = "";
        var val = "";
        var sectionname = $(this).parent().parent().find("img").attr("section");
        if (sectionname === "fieldwoDates" || sectionname === "wintag")
        {
            section = $(this).parent().parent().find("img").attr("section") + $(this).attr("id");
            key = $("#" + section + " option:selected").val();
            val = $("#" + section + " option:selected").text();
        }
        else if (typeof(sectionname) === "undefined")
        {
            key = $(this).find("option:selected").val();
            val = $(this).find("option:selected").text();
        }
        else
        {
            section = "td" + $(this).parent().parent().find("img").attr("section");
            key = $("#" + section + " #" + $(this).attr("id") + " option:selected").val();
            val = $("#" + section + " #" + $(this).attr("id") + " option:selected").text();
        }

        if (key !== "")
        {
            var subitem = "<span>\n\
                            <img class='cmdremovedropdownitem' height='15px' src='/widgets/css/images/removedoc.png' style='cursor:pointer;' valstr='" + val + "' \n\
                            id='" + $(this).attr("sectionid") + "__" + $(this).attr("id") + "__" + key + "'>&nbsp;&nbsp;" + val + "<br/></span>";
            $(this).parent().append(subitem);
            $(".cmdremovedropdownitem").unbind("click").bind("click", Me.cmdremovedropdownitem_click);
            if (typeof(sectionname) === "undefined")
            {
                $(this).find("option:selected").css("display", "none");
            }
            else
            {
                $("#" + section + " #" + $(this).attr("id") + " option:selected").css("display", "none");
            }
            if ($(this).attr("id") === "WorkOrderOwner")
            {
                if (typeof(sectionname) === "undefined")
                {
                    $(this).find("option").each(function() {
                        if ($(this).text() === 'Select Work Order Owner')
                        {
                            $(this).attr("selected", "selected");
                        }
                    });
                }
                else
                {
                    $("#" + section + " #" + $(this).attr("id")).find("option").each(function() {
                        if ($(this).text() === 'Select Work Order Owner')
                        {
                            $(this).attr("selected", "selected");
                        }
                    });
                }
            }
            else if ($(this).attr("id") === "State")
            {
                if (typeof(sectionname) === "undefined")
                {
                    $(this).find("option").each(function() {
                        if ($(this).text() === 'Select State')
                        {
                            $(this).attr("selected", "selected");
                        }
                    });
                }
                else
                {
                    $("#" + section + " #" + $(this).attr("id")).find("option").each(function() {
                        if ($(this).text() === 'Select State')
                        {
                            $(this).attr("selected", "selected");
                        }
                    });
                }
            }
            else
            {
                if (typeof(sectionname) === "undefined")
                {
                    $(this).find("option").each(function() {
                        if ($(this).val() === '')
                        {
                            $(this).attr("selected", "selected");
                        }
                    });
                }
                else
                {
                    $("#" + section + " #" + $(this).attr("id")).find("option").each(function() {
                        if ($(this).val() === '')
                        {
                            $(this).attr("selected", "selected");
                        }
                    });
                }
            }

        }
    };
    //------------------------------------------------------------
    this.cmdremovedropdownitem_click = function()
    {
        var temp = $(this).attr("id").split("__");
        var key = temp[2];
        $(this).parent().parent().find("select option[value='" + key + "']").css("display", "");
        $(this).parent().remove();
    };
    //------------------------------------------------------------
    this.removeItem_click = function()
    {
        var section = $(this).attr("section");
        if (Me._Filter_Field[section])
        {
            $(this).parent().parent().remove();
        }
        else
        {
            $(this).parent().remove();
        }
        Me.changetitle(section);
    };
    //------------------------------------------------------------
    this.loadDropdownBoxList = function(ComID, Key, section)
    {
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/custom-report/load-dropdown-list/",
            dataType: 'html',
            cache: false,
            context: this,
            data: {ComID: ComID, Key: Key},
            success: function(data, status, xhr) {
                if (section === "fieldwoDates" || section === "wintag")
                {
                    $("#div" + section + " #" + Key).html(data);
                }
                else
                {
                    $("#td" + section + " #" + Key).html(data);
                }

            }
        });
    };
    //------------------------------------------------------------
    this.displayerror = function(errmsg)
    {
        var html = '\
                <div id="diverror" style="max-height: 350px; overflow: auto;"><div id="errorDiv" style="color: red;">' + errmsg + '</div></div>';
        Me.openpopup(html);
        setTimeout(jQuery.fancybox.close, 2000);
    };
    //------------------------------------------------------------
    this.onSuccess = function(data, isgenerate) {
        if (data.success)
        {
            if (isgenerate)
            {
                window.location = 'reportview.php?v=' + Me.company + "&rptid=" + data.repID;
            }
            else
            {
                window.location = 'reportlist.php?v=' + Me.company;
            }
        }
        else
        {
            Me.displayerror(data.error);
            return false;
        }
    };
    //------------------------------------------------------------
    this.cmdAddReport_click = function(isgenerate)
    {
        if (typeof(isgenerate) === "undefined" || typeof(isgenerate) === "object")
        {
            isgenerate = false;
        }
        var reportname = jQuery.trim(jQuery("#reportname").val());
        if (reportname === "") {
            Me.displayerror("Please enter a Report Name.");
            return false;
        }
        var companyID = $("#hidComID").val();
        var repName = $("#reportname").val();
        var repTable = "work_orders";
        var fieldArray = [];
        var filterArray = [];
        //***Fields***
        fieldArray = Me.getfield();
        //***Filter***
        filterArray = Me.getfilter();
        Me.SaveReport(companyID, repName, repTable, fieldArray, filterArray, isgenerate);
    };
    //------------------------------------------------------------
    this.SaveReport = function(companyID, repName, repTable, fieldArray, filterArray, isgenerate)
    {
        //***Recurring report***
        var recurringreportArray = [];
        recurringreportArray = Me.getrecurringreport();
        var reportType = 1;
        $(".rptType").each(function() {
            if ($(this).is(":checked"))
            {
                reportType = $(this).val();
            }
        });
        var params = {"companyID": companyID, "repName": repName, "repTable": repTable, "fieldArray": fieldArray, "filterArray": filterArray, "recurringreportArray": recurringreportArray, "reportType": reportType};
        var postData = params;
        var postUrl = "/widgets/dashboard/custom-report/do-create-report-v2";
        $.ajax({
            type: "POST",
            url: postUrl,
            dataType: 'json',
            cache: true,
            async: true,
            data: postData,
            success: function(data) {
                Me.onSuccess(data, isgenerate);
            }
        });
    };
    //------------------------------------------------------------
    this.cmdUpdateReport_click = function(isgenerate)
    {
        if (typeof(isgenerate) === "undefined" || typeof(isgenerate) === "object")
        {
            isgenerate = false;
        }

        var reportname = $.trim($("#reportname").val());
//        var CreateBidsReport = $.trim($("#CreateBidsReport").val());
        if (reportname === "") {
            if (reportname === "") {
                Me.displayerror("Please enter a Report Name.");
                return false;
            }
        }

        var companyID = $("#hidComID").val();
        var repID = $("#reportid").val();
        var repName = $("#reportname").val();
        var fieldArray = [];
        var filterArray = [];
        //***Fields***
        fieldArray = Me.getfield();
        //***Filter***
        filterArray = Me.getfilter();
        var reportType = 1;
        $(".rptType").each(function() {
            if ($(this).is(":checked"))
            {
                reportType = $(this).val();
            }
        });
        var params = {"companyID": companyID, "repID": repID, "repName": repName, "fieldArray": fieldArray, "filterArray": filterArray, "reportType": reportType};
        var postData = params;
        var postUrl = "/widgets/dashboard/custom-report/do-update-report-v2";
        $.ajax({
            type: "POST",
            url: postUrl,
            dataType: 'json',
            cache: true,
            async: true,
            data: postData,
            success: function(data) {
                Me.onSuccess(data, isgenerate);
            }
        });
    };
    //------------------------------------------------------------
    this.cmdsaveandgenerate_click = function()
    {
        if ($("#reportid").val() !== "")
        {
            Me.cmdUpdateReport_click(true);
        }
        else
        {
            Me.cmdAddReport_click(true);
        }
    };
    //------------------------------------------------------------
    this.getfield = function()
    {
        var fieldArray = [];
        var strFieldID = "";
        $.each(Me.secid, function(id, val) {
            if (id !== "tdwostatus" && id !== "ulProject")
            {
                $("#" + id + " img").each(function()
                {
                    strFieldID += $(this).attr(val.substr(2) + "id") + ",";
                });
            }
        });
        fieldArray = strFieldID.split(",");
        fieldArray.splice(fieldArray.length - 1, 1);
        return fieldArray;
    };
    //------------------------------------------------------------
    this.getfilter = function()
    {
        var filterArray = [];
        $.each(Me.secid, function(id, val) {
            if (id === "ulProject")
            {
                var projectarr = Me.getprojectfilter();
                if ($(projectarr).size() > 0)
                {
                    filterArray.push(projectarr);
                }
            }
            else if (id === "tdwostatus")
            {
                var wostatusarr = Me.getwostatusfilter();
                if ($(wostatusarr).size() > 0)
                {
                    filterArray.push(wostatusarr);
                }
            }
            else
            {
                $("#" + id + ">tbody>tr").each(function()
                {
                    var temarr = [];
                    var fieldcode = $(this).attr(val.substr(2) + "id");
                    var fieldtype = $(this).attr(fieldcode + "_type");
                    var el = "";
                    if (fieldtype === "D")
                    {
                        el = $(this).find("select");
                        temarr = Me.getselectfilter(el);
                    }
                    else if (fieldtype === "T")
                    {
                        el = $(this).find("input");
                        temarr = Me.gettextfilter(el);
                    }
                    else if (fieldtype === "DT")
                    {
                        el = $(this).find("img");
                        //var fieldcodeID = $(this).attr("filterbidid");
                        temarr = Me.getdaterangefilter(el, val);
                    }
                    else if (fieldtype === "N2")
                    {
                        el = $(this).find("img");
                        temarr = Me.getN2filter(el);
                    }
                    else if (fieldtype === "T2")
                    {
                        el = $(this).find("img");
                        temarr = Me.getT2filter(el);
                    }
                    else if (fieldtype === "DYN")
                    {
                        el = $(this).find("select");
                        temarr = Me.getDYNfilter(el);
                    }

                    if ($(temarr).size() > 0)
                    {
                        filterArray.push(temarr);
                    }
                });
            }
        });

        return filterArray;
    };
    //------------------------------------------------------------
    this.getprojectfilter = function()
    {
        var projectarr = [];
        var filterid = "";
        var strProjectID = [];
        $("#ulProject img").each(function()
        {
            strProjectID.push($(this).attr("fieldname"));
            if ($(this).attr("filterid") !== "")
            {
                filterid = $(this).attr("filterid");
            }
        });
        if (strProjectID.length > 0)
        {
            if ($("#reportid").val() !== "")
            {
                projectarr = {'id': filterid, 'fieldcode': 'Project_Name', 'filter_at': 3, 'SelectMultiple': strProjectID};
            }
            else
            {
                projectarr = {'fieldcode': 'Project_Name', 'filter_at': 3, 'SelectMultiple': strProjectID};
            }
        }

        return projectarr;
    };
    //------------------------------------------------------------
    this.getwostatusfilter = function()
    {
        var wostatusarr = [];
        var filterid = "";
        var strwostatusID = [];
        $("#tdwostatus img").each(function()
        {
            strwostatusID.push($(this).attr("wostatusid"));
            if ($(this).attr("filterid") !== "")
            {
                filterid = $(this).attr("filterid");
            }
        });
        if (strwostatusID.length > 0)
        {
            if ($("#reportid").val() !== "")
            {
                wostatusarr = {'id': filterid, 'fieldcode': 'Status', 'filter_at': 3, 'string_matching': 'SelectMultiple', 'SelectMultiple': strwostatusID};
            }
            else
            {
                wostatusarr = {'fieldcode': 'Status', 'filter_at': 3, 'string_matching': 'SelectMultiple', 'SelectMultiple': strwostatusID};
            }

        }
        return wostatusarr;
    };
    //------------------------------------------------------------
    this.getdaterangefilter = function(el, id)
    {
        var fieldcode = $(el).attr(id.substr(2) + "id");
        var filterid = $(el).attr("filterid");
        var datetime_relative_data_ref = $("#Date_" + fieldcode + " option:selected").val();
        var datetime_range_from = $("#StartDate" + fieldcode).val();
        var datetime_range_to = $("#EndDate" + fieldcode).val();
        var temarr = [];
        if ($("#reportid").val() !== "")
        {
            temarr = {
                'id': filterid,
                'fieldcode': fieldcode,
                'filter_at': 3,
                'datetime_relative_data_ref': datetime_relative_data_ref,
                'datetime_range_from': datetime_range_from,
                'datetime_range_to': datetime_range_to
            };
        }
        else
        {
            temarr = {
                'fieldcode': fieldcode,
                'filter_at': 3,
                'datetime_relative_data_ref': datetime_relative_data_ref,
                'datetime_range_from': datetime_range_from,
                'datetime_range_to': datetime_range_to
            };
        }
        return temarr;
    };
    //------------------------------------------------------------
    this.gettextfilter = function(el)
    {
        var temarr = [];
        var fieldcode = $(el).attr("id");
        var filterid = $(el).parent().parent().find('img').attr("filterid");
        var string_value = $(el).val();
        if ($("#reportid").val() !== "")
        {
            temarr = {
                'id': filterid,
                'fieldcode': fieldcode,
                'filter_at': 3,
                'string_matching': "ExactMatch",
                'string_value': string_value
            };
        }
        else
        {
            temarr = {
                'fieldcode': fieldcode,
                'filter_at': 3,
                'string_matching': "ExactMatch",
                'string_value': string_value
            };
        }

        return temarr;
    };
    //------------------------------------------------------------
    this.getselectfilter = function(el)
    {
        var filteradditionalSelecttarr = [];
        var temarr = [];
        var strfilteradditionalID = "";
        var fieldcode = "";
        var filterid = "";
        filterid = $(el).parent().parent().find(".cmdremove").attr('filterid');
        $(el).parent().find(".cmdremovedropdownitem").each(function()
        {
            fieldcode = $(this).attr("id").split("__");
            strfilteradditionalID += fieldcode[2] + ",";
        });
        if (strfilteradditionalID !== "")
        {
            filteradditionalSelecttarr = strfilteradditionalID.split(",");
            filteradditionalSelecttarr.splice(filteradditionalSelecttarr.length - 1, 1);
            if ($("#reportid").val() !== "")
            {
                temarr = {
                    'id': filterid,
                    'fieldcode': fieldcode[1],
                    'filter_at': 3,
                    'string_matching': "SelectMultiple",
                    'SelectMultiple': filteradditionalSelecttarr
                };
            }
            else
            {
                temarr = {
                    'fieldcode': fieldcode[1],
                    'filter_at': 3,
                    'string_matching': "SelectMultiple",
                    'SelectMultiple': filteradditionalSelecttarr
                };
            }

            return temarr;
        }
    };
    //------------------------------------------------------------
    this.getDYNfilter = function(el)
    {
        var filteradditionalSelecttarr = [];
        var temarr = [];
        var fieldcode = $(el).attr("sectionid");
        var filterid = $(el).parent().parent().find(".cmdremove").attr('filterid');
        if ($(el).val() !== "")
        {
            filteradditionalSelecttarr = $(el).val().split(",");
            if ($("#reportid").val() !== "")
            {
                temarr = {
                    'id': filterid,
                    'fieldcode': fieldcode,
                    'filter_at': 3,
                    'string_matching': "SelectMultiple",
                    'SelectMultiple': filteradditionalSelecttarr
                };
            }
            else
            {
                temarr = {
                    'fieldcode': fieldcode,
                    'filter_at': 3,
                    'string_matching': "SelectMultiple",
                    'SelectMultiple': filteradditionalSelecttarr
                };
            }

            return temarr;
        }
    };
    //------------------------------------------------------------
    this.getN2filter = function(el)
    {
        var section = $(el).attr("section");
        var fieldcode = $(el).attr(section + "id");
        var filterid = $(el).attr("filterid");
        var numeric_range_from = "";
        var numeric_range_to = "";
        if ($("#" + fieldcode + "__from").val() !== "#.##")
        {
            numeric_range_from = $("#" + fieldcode + "__from").val();
        }
        if ($("#" + fieldcode + "__to").val() !== "#.##")
        {
            numeric_range_to = $("#" + fieldcode + "__to").val();
        }

        var temarr = [];
        if ($("#reportid").val() !== "")
        {
            temarr = {
                'id': filterid,
                'fieldcode': fieldcode,
                'filter_at': 3,
                'numeric_range_from': numeric_range_from,
                'numeric_range_to': numeric_range_to
            };
        }
        else
        {
            temarr = {
                'fieldcode': fieldcode,
                'filter_at': 3,
                'numeric_range_from': numeric_range_from,
                'numeric_range_to': numeric_range_to
            };
        }
        return temarr;
    };
    //------------------------------------------------------------
    this.getT2filter = function(el)
    {
        var section = $(el).attr("section");
        var fieldcode = $(el).attr(section + "id");
        var filterid = $(el).attr("filterid");
        var time_from = $("#" + fieldcode + "__from").val();
        var time_to = $("#" + fieldcode + "__to").val();
        var time_tz = $("#" + fieldcode + "est").val();
        var temarr = [];
        if ($("#reportid").val() !== "")
        {
            temarr = {
                'id': filterid,
                'fieldcode': fieldcode,
                'filter_at': 3,
                'time_from': time_from,
                'time_to': time_to,
                'time_tz': time_tz
            };
        }
        else
        {
            temarr = {
                'fieldcode': fieldcode,
                'filter_at': 3,
                'time_from': time_from,
                'time_to': time_to
            };
        }
        return temarr;
    };
    //------------------------------------------------------------
    this.getrecurringreport = function()
    {
        return Me._recurringreport;
    };
    //------------------------------------------------------------
    this.opendiv = function()
    {
        var section = $(this).attr("section");
        var strcontenter = "";
        if (Me._Filter_Field[section])
        {
            strcontenter = "td" + section + " tr";
        }
        else
        {
            strcontenter = "ul" + section + " li";
        }

        if ($("#" + strcontenter).length > 0)
        {
            if ($(this).attr("isopen") === "0")
            {
                Me.expanddiv(this);
            }
            else
            {
                Me.collapseddiv(this);
            }
        }
        else
        {
            $("#" + section).click();
        }
    };
    //------------------------------------------------------------
    this.expanddiv = function(el)
    {
        $(el).removeClass("divopen");
        $(el).addClass("divclose");
        $(el).attr("isopen", "1");
        $("#div" + $(el).attr("section")).css("display", "");
    };
    //------------------------------------------------------------
    this.collapseddiv = function(el)
    {
        $(el).removeClass("divclose");
        $(el).addClass("divopen");
        $(el).attr("isopen", "0");
        $("#div" + $(el).attr("section")).css("display", "none");
    };
    //------------------------------------------------------------
    this.cmdRecurringReportStatus_click = function()
    {
        var reportname = $.trim($("#reportname").val());
        if (reportname === "") {
            Me.displayerror("Please enter a Report Name.");
            return false;
        }

        var id = "ContainerID_" + $(this).attr("id");
        var html = '\
            <div>\n\
                <div style="text-align:center;">\n\
                    <div>\n\
                        <u><b>Recurring Report Delivery</b></u>\n\
                    </div><br/>\n\
                </div>\n\
                <div id="' + id + '" style="max-height: 350px; overflow: auto;"></div>\n\
                <div style="text-align:center">\n\
                    <input type="button" class="link_button_popup" value="Cancel" id="cmdCancelRecurringReport">\n\
                    <input type="button" class="link_button_popup" value="Save" id="cmdSaveRecurringReport">\n\
                </div>\n\
            </div>';
        Me.openpopup(html);
        $("#" + id).html($("#reportrecurrent").html());
        Me.fanxyboxresize();
        var reportname = $("#reportname").val();
        if ($("#" + id + " #txtSubject").val() === "")
        {
            $("#" + id + " #txtSubject").val("FieldSolutions Report: " + reportname);
        }
        if ($("#" + id + " #txtmessage").val() === "")
        {
            $("#" + id + " #txtmessage").val("Please find " + reportname + " attached.\nThanks, Your FieldSolutions Team ");
        }
        Me.bindrecurringreportData();
        $("#" + id + " #txtsendto").unbind("blur").bind("blur", Me.txtsendto_blur);
        $("#ContainerID_cmdRecurringReportStatus #txtsendto").blur();
        $("#" + id + " #txtsendto").unbind("focus").bind("focus", Me.txtsendto_focus);
        $("#" + id + " #chkEnableRecurringReport").unbind("click").bind("click", Me.chkEnableRecurringReport_click);
        $("#" + id + " #cbxSchedule").unbind("change").bind("change", Me.cbxSchedule_change);
        $("#" + id + " #MonthlyDayNum").unbind("click").bind("click", Me.MonthlyDay_click);
        $("#" + id + " #MonthlyDayOrder").unbind("click").bind("click", Me.MonthlyDay_click);
        $("#" + id + " #DaylyDayNum").unbind("click").bind("click", Me.DaylyDay_click);
        $("#" + id + " #DaylyWeekday").unbind("click").bind("click", Me.DaylyDay_click);
        $("#cmdCancelRecurringReport").unbind("click").bind("click", Me.Cancel_click);
        $("#cmdSaveRecurringReport").unbind("click").bind("click", Me.SaveRecurringReport_Click);
        if ($("#reportid").val() === "")
        {
            $("#" + id + " #chkEnableRecurringReport").attr("checked", true);
        }
        var btstr = "If you do not receive emailed reports as expected, ensure that the address entered in the &lsquo;Send From&rsquo; field is marked as a &lsquo;Safe Sender&rsquo; in your email client.";
        $("#" + id + " #getinfocontrols").bt(btstr,
                {
                    trigger: 'click',
                    positions: 'left',
                    fill: 'white',
                    cssStyles: {color: 'black', width: '300px', height: '60px'}
                });
    };
    //------------------------------------------------------------
    this.txtsendto_blur = function()
    {
        if ($(this).val() === $('<div />').html("Use &ldquo;,&rdquo; to separate multiple email addresses").text() || $(this).val() === "")
        {
            $(this).css("color", "#a6a6a6");
            $(this).val($('<div />').html("Use &ldquo;,&rdquo; to separate multiple email addresses").text());
        }
        else
        {
            $(this).css("color", "#000000");
        }
    };
    //------------------------------------------------------------
    this.txtsendto_focus = function()
    {
        if ($(this).val() === $('<div />').html("Use &ldquo;,&rdquo; to separate multiple email addresses").text())
        {
            $(this).val("");
            $(this).css("color", "#000000");
        }
    };
    //------------------------------------------------------------
    this.chkEnableRecurringReport_click = function()
    {
        var selectedval = $("#ContainerID_cmdRecurringReportStatus #cbxSchedule option:selected").val();
        if ($(this).is(":checked"))
        {
            $("#ContainerID_cmdRecurringReportStatus .RecurringReportEle").removeAttr("disabled");
            $("#ContainerID_cmdRecurringReportStatus #lblsendto").html("Send to<span style='color:red;'>*</span>:");
            if (selectedval === "Daily")
            {
                $("#ContainerID_cmdRecurringReportStatus #DaylyDayNum").removeAttr("disabled");
                $("#ContainerID_cmdRecurringReportStatus #txteverydaynum").removeAttr("disabled");
                $("#ContainerID_cmdRecurringReportStatus #DaylyWeekday").removeAttr("disabled");
            }
            else if (selectedval === "Weekly")
            {
                $("#ContainerID_cmdRecurringReportStatus #WeeklyWeekNum").removeAttr("disabled");
                $("#ContainerID_cmdRecurringReportStatus #ScheduleWeekly .Weekly").removeAttr("disabled");
            }
            else if (selectedval === "Monthly")
            {
                $("#ContainerID_cmdRecurringReportStatus #MonthlyDayNum").removeAttr("disabled");
                $("#ContainerID_cmdRecurringReportStatus #MonthlyDayOrder").removeAttr("disabled");
                if ($("#ContainerID_cmdRecurringReportStatus #MonthlyDayNum").is(":checked"))
                {
                    $("#ContainerID_cmdRecurringReportStatus #cbxMonthlyDayNum").removeAttr("disabled");
                    $("#ContainerID_cmdRecurringReportStatus #MonthlyMonthOndayNum").removeAttr("disabled");
                }

                if ($("#ContainerID_cmdRecurringReportStatus #MonthlyDayOrder").is(":checked"))
                {
                    $("#ContainerID_cmdRecurringReportStatus #cbxMonthlyDayOrder").removeAttr("disabled")
                    $("#ContainerID_cmdRecurringReportStatus #cbxMonthlyDayOrderName").removeAttr("disabled")
                    $("#ContainerID_cmdRecurringReportStatus #MonthlyMonthOnthedayNum").removeAttr("disabled")
                }
            }
        }
        else
        {
            $("#ContainerID_cmdRecurringReportStatus .RecurringReportEle").attr("disabled", true);
            $("#ContainerID_cmdRecurringReportStatus #lblsendto").html("Send to:");
            if (selectedval === "Daily")
            {
                $("#ContainerID_cmdRecurringReportStatus #DaylyDayNum").attr("disabled", true);
                $("#ContainerID_cmdRecurringReportStatus #DaylyWeekday").attr("disabled", true);
                $("#ContainerID_cmdRecurringReportStatus #txteverydaynum").attr("disabled", true);
            }
            else if (selectedval === "Weekly")
            {
                $("#ContainerID_cmdRecurringReportStatus #WeeklyWeekNum").attr("disabled", true);
                $("#ContainerID_cmdRecurringReportStatus #ScheduleWeekly .Weekly").attr("disabled", true);
            }
            else if (selectedval === "Monthly")
            {
                $("#ContainerID_cmdRecurringReportStatus #MonthlyDayNum").attr("disabled", true);
                $("#ContainerID_cmdRecurringReportStatus #MonthlyDayOrder").attr("disabled", true);

                $("#ContainerID_cmdRecurringReportStatus #cbxMonthlyDayNum").attr("disabled", true);
                $("#ContainerID_cmdRecurringReportStatus #MonthlyMonthOndayNum").attr("disabled", true);

                $("#ContainerID_cmdRecurringReportStatus #cbxMonthlyDayOrder").attr("disabled", true);
                $("#ContainerID_cmdRecurringReportStatus #cbxMonthlyDayOrderName").attr("disabled", true);
                $("#ContainerID_cmdRecurringReportStatus #MonthlyMonthOnthedayNum").attr("disabled", true);
            }
        }
    };
    //------------------------------------------------------------
    this.cbxSchedule_change = function()
    {
        if ($(this).val() === "Weekly")
        {
            $("#ContainerID_cmdRecurringReportStatus #ScheduleDayly").css("display", "none");
            $("#ContainerID_cmdRecurringReportStatus #DaylyDayNum").attr("disabled", true);
            $("#ContainerID_cmdRecurringReportStatus #DaylyWeekday").attr("disabled", true);
            $("#ContainerID_cmdRecurringReportStatus #txteverydaynum").attr("disabled", true);

            $("#ContainerID_cmdRecurringReportStatus #ScheduleWeekly").css("display", "");
            $("#ContainerID_cmdRecurringReportStatus #WeeklyWeekNum").removeAttr("disabled");
            $("#ContainerID_cmdRecurringReportStatus #ScheduleWeekly .Weekly").removeAttr("disabled");

            $("#ContainerID_cmdRecurringReportStatus #ScheduleMonthly").css("display", "none");

            $("#ContainerID_cmdRecurringReportStatus #MonthlyDayNum").attr("disabled", true);
            $("#ContainerID_cmdRecurringReportStatus #MonthlyDayOrder").attr("disabled", true);
            $("#ContainerID_cmdRecurringReportStatus #cbxMonthlyDayNum").attr("disabled", true);
            $("#ContainerID_cmdRecurringReportStatus #MonthlyMonthOndayNum").attr("disabled", true);

            if (Me._recurringreport["RRDWeeklyDays"] !== "")
            {
                var i = 0;
                $("#ScheduleWeekly .Weekly").each(function() {
                    if ($(this).is(":checked"))
                    {
                        i++;
                    }
                });

                if (i === 0)
                {
                    $("#ContainerID_cmdRecurringReportStatus  #ScheduleWeekly #WeeklyMonday").attr("checked", "true");
                }
            }
            else
            {
                $("#ContainerID_cmdRecurringReportStatus  #ScheduleWeekly .Weekly").removeAttr("checked")
                $("#ContainerID_cmdRecurringReportStatus  #ScheduleWeekly #WeeklyMonday").attr("checked", "true");
            }

        }
        else if ($(this).val() === "Monthly")
        {
            $("#ContainerID_cmdRecurringReportStatus #ScheduleDayly").css("display", "none");
            $("#ContainerID_cmdRecurringReportStatus #DaylyDayNum").attr("disabled", true);
            $("#ContainerID_cmdRecurringReportStatus #DaylyWeekday").attr("disabled", true);
            $("#ContainerID_cmdRecurringReportStatus #txteverydaynum").attr("disabled", true);

            $("#ContainerID_cmdRecurringReportStatus #ScheduleWeekly").css("display", "none");
            $("#ContainerID_cmdRecurringReportStatus #WeeklyWeekNum").attr("disabled", true);
            $("#ContainerID_cmdRecurringReportStatus #ScheduleWeekly .Weekly").attr("disabled", true);

            $("#ContainerID_cmdRecurringReportStatus #ScheduleMonthly").css("display", "");
            $("#ContainerID_cmdRecurringReportStatus #MonthlyDayNum").removeAttr("disabled");
            $("#ContainerID_cmdRecurringReportStatus #MonthlyDayOrder").removeAttr("disabled");
            $("#ContainerID_cmdRecurringReportStatus #cbxMonthlyDayNum").removeAttr("disabled");
            $("#ContainerID_cmdRecurringReportStatus #MonthlyMonthOndayNum").removeAttr("disabled");
        }
        else if ($(this).val() === "Daily")
        {
            $("#ContainerID_cmdRecurringReportStatus #ScheduleDayly").css("display", "");
            $("#ContainerID_cmdRecurringReportStatus #DaylyDayNum").removeAttr("disabled");
            $("#ContainerID_cmdRecurringReportStatus #DaylyWeekday").removeAttr("disabled");
            $("#ContainerID_cmdRecurringReportStatus #txteverydaynum").removeAttr("disabled");

            $("#ContainerID_cmdRecurringReportStatus #ScheduleWeekly").css("display", "none");
            $("#ContainerID_cmdRecurringReportStatus #WeeklyWeekNum").attr("disabled", true);
            $("#ContainerID_cmdRecurringReportStatus #ScheduleWeekly .Weekly").attr("disabled", true);

            $("#ContainerID_cmdRecurringReportStatus #ScheduleMonthly").css("display", "none");
            $("#ContainerID_cmdRecurringReportStatus #MonthlyDayNum").attr("disabled", true);
            $("#ContainerID_cmdRecurringReportStatus #MonthlyDayOrder").attr("disabled", true);
            $("#ContainerID_cmdRecurringReportStatus #cbxMonthlyDayNum").attr("disabled", true);
            $("#ContainerID_cmdRecurringReportStatus #MonthlyMonthOndayNum").attr("disabled", true);
        }

    };
    //------------------------------------------------------------
    this.MonthlyDay_click = function()
    {
        if ($(this).attr("id") === "MonthlyDayNum")
        {
            $("#ContainerID_cmdRecurringReportStatus #cbxMonthlyDayNum").removeAttr("disabled");
            $("#ContainerID_cmdRecurringReportStatus #MonthlyMonthOndayNum").removeAttr("disabled");
            $("#ContainerID_cmdRecurringReportStatus #cbxMonthlyDayOrder").attr("disabled", "true");
            $("#ContainerID_cmdRecurringReportStatus #cbxMonthlyDayOrderName").attr("disabled", "true");
            $("#ContainerID_cmdRecurringReportStatus #MonthlyMonthOnthedayNum").attr("disabled", "true");
        }
        else
        {
            $("#ContainerID_cmdRecurringReportStatus #cbxMonthlyDayNum").attr("disabled", "true");
            $("#ContainerID_cmdRecurringReportStatus #MonthlyMonthOndayNum").attr("disabled", "true");
            $("#ContainerID_cmdRecurringReportStatus #cbxMonthlyDayOrder").removeAttr("disabled");
            $("#ContainerID_cmdRecurringReportStatus #cbxMonthlyDayOrderName").removeAttr("disabled");
            $("#ContainerID_cmdRecurringReportStatus #MonthlyMonthOnthedayNum").removeAttr("disabled");
        }
    };
    //------------------------------------------------------------
    this.DaylyDay_click = function()
    {
        if ($(this).attr("id") === "DaylyDayNum")
        {
            $("#ContainerID_cmdRecurringReportStatus #txteverydaynum").removeAttr("disabled");
            $("#ContainerID_cmdRecurringReportStatus #cbxDaylyDayOrderName").attr("disabled", "true");
        }
        else
        {
            $("#ContainerID_cmdRecurringReportStatus #txteverydaynum").attr("disabled", "true");
            $("#ContainerID_cmdRecurringReportStatus #cbxDaylyDayOrderName").removeAttr("disabled");
        }
    };
    //------------------------------------------------------------
    this.Cancel_click = function()
    {
        $.fancybox.close();
    };
    //------------------------------------------------------------
    this.loadRecurringReportLabel = function()
    {
        var RecurringReportStatusLabel = "";
        if (Me._recurringreport['RRDEmailSchedule'] === "Daily")
        {
            if (parseInt(Me._recurringreport['RRDDailyOptions']) === 1)
            {
                if (Me._recurringreport['RRDDailyEveryDaySpaceOutNum'] > 1)
                {
                    RecurringReportStatusLabel = "Every " + Me._recurringreport['RRDDailyEveryDaySpaceOutNum'] + " Days";
                }
                else
                {
                    RecurringReportStatusLabel = Me._recurringreport['RRDEmailSchedule'];
                }
            } else
            {
                RecurringReportStatusLabel = "Weekdays";
            }
        }
        else if (Me._recurringreport['RRDEmailSchedule'] === "Monthly")
        {
            if (Me._recurringreport['RRDMonthlyScheduleOptions'] === 1)
            {
                if (parseInt(Me._recurringreport['RRDMonthlyDayNumSpaceOutNum']) > 1)
                {
                    RecurringReportStatusLabel = "Every " + Me._recurringreport['RRDMonthlyDayNumSpaceOutNum'] + " Months";
                }
                else
                {
                    RecurringReportStatusLabel = Me._recurringreport['RRDEmailSchedule'] + " - Day " + Me._recurringreport['RRDMonthlyScheduleDayNum'];
                }
            } else
            {
                if (parseInt(Me._recurringreport['RRDMonthlyDayOrderSpaceOutNum']) > 1)
                {
                    RecurringReportStatusLabel = "Every " + Me._recurringreport['RRDMonthlyDayOrderSpaceOutNum'] + " Months";
                }
                else
                {
                    var RRDMonthlyScheduleDayOrderlabel = "";
                    switch (Me._recurringreport['RRDMonthlyScheduleDayOrder'])
                    {
                        case "1":
                            RRDMonthlyScheduleDayOrderlabel = "1<sup>st</sup>";
                            break;
                        case "2":
                            RRDMonthlyScheduleDayOrderlabel = "2<sup>nd</sup>";
                            break;
                        case "3":
                            RRDMonthlyScheduleDayOrderlabel = "3<sup>rd</sup>";
                            break;
                        case "4":
                            RRDMonthlyScheduleDayOrderlabel = "4<sup>th</sup>";
                            break;
                    }
                    RecurringReportStatusLabel = Me._recurringreport['RRDEmailSchedule'] + " - " + RRDMonthlyScheduleDayOrderlabel + " " + Me._weekday[Me._recurringreport['RRDMonthlyScheduleDayOrderDay']];
                }
            }
        }
        else
        {
            if (parseInt(Me._recurringreport['RRDWeeklySpaceOutNum']) > 1)
            {
                RecurringReportStatusLabel = "Every " + Me._recurringreport['RRDWeeklySpaceOutNum'] + " Weeks";
            }
            else if (Me._recurringreport['RRDWeeklyDays'] !== "")
            {
                RecurringReportStatusLabel = Me._recurringreport['RRDEmailSchedule'];
            }
        }
        return RecurringReportStatusLabel;
    };
    //------------------------------------------------------------
    this.SaveRecurringReport_Click = function()
    {
        var id = "ContainerID_cmdRecurringReportStatus";
        if ($("#" + id + " #chkEnableRecurringReport").is(":checked"))
        {
            if ($("#" + id + " #txtsendto").val() === $('<div />').html("Use &ldquo;,&rdquo; to separate multiple email addresses").text())
            {
                alert("Send to is empty but required.");
                return false;
            }
        }
        var tememail = $("#" + id + " #txtsendto").val().split(",");
        var errmsg = "";
        $.each(tememail, function(id, val) {
            if (!Me.IsEmail(val))
            {
                errmsg = "Invalid email format.";
            }
        });
        if (errmsg !== "")
        {
            alert(errmsg);
            return false;
        }
        Me.setrecurringreport("ContainerID_cmdRecurringReportStatus");
        if ($("#reportid").val() !== "")
        {
            var params = {"repID": $("#reportid").val(), "RRDParams": Me.getrecurringreport()};
            var postData = params;
            var postUrl = "/widgets/dashboard/custom-report/do-save-recurring-report-v2";
            $.ajax({
                type: "POST",
                url: postUrl,
                dataType: 'json',
                cache: true,
                async: true,
                data: postData,
                success: function(data) {
                    //window.location.reload();
                }
            });
        }
        $("#RecurringReportStatus").html(Me.loadRecurringReportLabel());
        $("#cmdRecurringReportStatus").html("edit");
        $.fancybox.close();
    };
    //------------------------------------------------------------
    this.setrecurringreport = function(id)
    {
        //var id = "ContainerID_cmdRecurringReportStatus";
        var RRDEnable = $("#" + id + " #chkEnableRecurringReport").is(":checked") ? 1 : 0;
        var RRDEmailTo = "";
        if ($("#" + id + " #txtsendto").val() === $('<div />').html("Use &ldquo;,&rdquo; to separate multiple email addresses").text())
        {
            RRDEmailTo = "";
        }
        else
        {
            RRDEmailTo = $("#" + id + " #txtsendto").val();
        }
        //14100
        var RRDEmailFromName = $("#" + id + " #txtFromName").val();
        var RRDEmailFrom = $("#" + id + " #txtFromMail").val();
        var RRDEmailSubject = $("#" + id + " #txtSubject").val();
        var RRDEmailMessage = $("#" + id + " #txtmessage").val();
        var RRDEmailSchedule = $("#" + id + " #cbxSchedule option:selected").val();
        var RRDTimeOfDay = $("#" + id + " #cbxtimeSchedule option:selected").val();
        var RRDTimeZone = $("#" + id + " #cbxestSchedule option:selected").val();
        var RRDWeeklyDays = "";
        var RRDWeeklySpaceOutNum = 1;
        var RRDMonthlyScheduleOptions = 1;
        var RRDMonthlyScheduleDayNum = "";
        var RRDMonthlyScheduleDayOrder = "";
        var RRDMonthlyScheduleDayOrderDay = "";
        var RRDMonthlyDayNumSpaceOutNum = 1;
        var RRDMonthlyDayOrderSpaceOutNum = 1;
        var RRDDailyEveryDaySpaceOutNum = 1;
        var RRDDailyEveryWeekday = "";
        var RRDDailyOptions = "";
        if (RRDEmailSchedule === "Weekly")
        {
            $("#" + id + " #ScheduleWeekly tr input").each(
                    function() {
                        if ($(this).is(":checked"))
                        {
                            RRDWeeklyDays += $(this).val() + ",";
                        }
                    }
            );
            if (RRDWeeklyDays !== "")
            {
                RRDWeeklyDays = RRDWeeklyDays.split(",");
                RRDWeeklyDays.splice(RRDWeeklyDays.length - 1, 1);
            }
            RRDWeeklySpaceOutNum = $("#" + id + " #WeeklyWeekNum").val();
            RRDDailyEveryWeekday = "";
            RRDDailyOptions = "";
            RRDMonthlyScheduleOptions = "";
            RRDMonthlyScheduleDayNum = "";
            RRDMonthlyScheduleDayOrder = "";
            RRDMonthlyScheduleDayOrderDay = "";
        }

        if (RRDEmailSchedule === "Monthly")
        {
            RRDMonthlyScheduleOptions = $("#" + id + " #MonthlyDayNum").is(":checked") ? 1 : 2;
            if (RRDMonthlyScheduleOptions === 1)
            {
                RRDMonthlyScheduleDayNum = $("#" + id + " #cbxMonthlyDayNum option:selected").val();
                RRDMonthlyDayNumSpaceOutNum = $("#" + id + " #MonthlyMonthOndayNum").val();
            }
            else
            {
                RRDMonthlyScheduleDayOrder = $("#" + id + " #cbxMonthlyDayOrder option:selected").val();
                RRDMonthlyScheduleDayOrderDay = $("#" + id + " #cbxMonthlyDayOrderName option:selected").val();
                RRDMonthlyDayOrderSpaceOutNum = $("#" + id + " #MonthlyMonthOnthedayNum").val();
            }
            RRDDailyEveryWeekday = "";
            RRDDailyOptions = "";
            RRDWeeklyDays = "";
        }

        if (RRDEmailSchedule === "Daily")
        {
            RRDDailyOptions = $("#" + id + " #DaylyDayNum").is(":checked") ? 1 : 2;
            if (RRDDailyOptions === 1)
            {
                RRDDailyEveryDaySpaceOutNum = $("#" + id + " #txteverydaynum").val();
            }
            else
            {
                RRDDailyEveryWeekday = $("#" + id + " #cbxDaylyDayOrderName option:selected").val();
            }
            RRDWeeklyDays = "";
            RRDMonthlyScheduleOptions = "";
            RRDMonthlyScheduleDayNum = "";
            RRDMonthlyScheduleDayOrder = "";
            RRDMonthlyScheduleDayOrderDay = "";
        }

        Me._recurringreport = {
            'RRDEnable': RRDEnable,
            'RRDEmailTo': RRDEmailTo,
            'RRDEmailFromName': RRDEmailFromName,
            'RRDEmailFrom': RRDEmailFrom,
            'RRDEmailSubject': RRDEmailSubject,
            'RRDEmailMessage': RRDEmailMessage,
            'RRDEmailSchedule': RRDEmailSchedule,
            'RRDTimeOfDay': RRDTimeOfDay,
            'RRDTimeZone': RRDTimeZone,
            'RRDWeeklyDays': RRDWeeklyDays,
            'RRDMonthlyScheduleOptions': RRDMonthlyScheduleOptions,
            'RRDMonthlyScheduleDayNum': RRDMonthlyScheduleDayNum,
            'RRDMonthlyScheduleDayOrder': RRDMonthlyScheduleDayOrder,
            'RRDMonthlyScheduleDayOrderDay': RRDMonthlyScheduleDayOrderDay,
            'RRDWeeklySpaceOutNum': RRDWeeklySpaceOutNum,
            'RRDMonthlyDayNumSpaceOutNum': RRDMonthlyDayNumSpaceOutNum,
            'RRDMonthlyDayOrderSpaceOutNum': RRDMonthlyDayOrderSpaceOutNum,
            'RRDDailyOptions': RRDDailyOptions,
            'RRDDailyEveryWeekday': RRDDailyEveryWeekday,
            'RRDDailyEveryDaySpaceOutNum': RRDDailyEveryDaySpaceOutNum
        };
    };
    //------------------------------------------------------------
    this.bindrecurringreportData = function()
    {
        var id = "ContainerID_cmdRecurringReportStatus";
        if (Me._recurringreport !== null)
        {
            $("#" + id + " .RecurringReportEle").removeAttr("disabled");
            $("#" + id + " #lblsendto").html("Send to<span style='color:red;'>*</span>:");
            $("#" + id + " #chkEnableRecurringReport").attr("checked", true);

            if (Me._recurringreport["RRDEmailTo"] !== "")
            {
                $("#" + id + " #txtsendto").val(Me._recurringreport["RRDEmailTo"]);
            }

            if (Me._recurringreport["RRDEmailFromName"] !== "")
            {
                $("#" + id + " #txtFromName").val(Me._recurringreport["RRDEmailFromName"]);
            }

            if (Me._recurringreport["RRDEmailFrom"] !== "")
            {
                $("#" + id + " #txtFromMail").val(Me._recurringreport["RRDEmailFrom"]);
            }

            if (Me._recurringreport["RRDEmailSubject"] !== "")
            {
                $("#" + id + " #txtSubject").val(Me._recurringreport["RRDEmailSubject"]);
            }

            if (Me._recurringreport["RRDEmailMessage"] !== "")
            {
                $("#" + id + " #txtmessage").val(Me._recurringreport["RRDEmailMessage"]);
            }

            $("#" + id + " #cbxSchedule option").each(function() {
                if ($(this).val() === Me._recurringreport["RRDEmailSchedule"])
                {
                    $(this).attr("selected", "selected");
                }
            });
            if (Me._recurringreport["RRDEmailSchedule"] === "Daily")
            {
                $("#" + id + " #ScheduleDayly").css("display", "");
                if (Me._recurringreport["RRDDailyOptions"] === 1)
                {
                    $("#" + id + " #DaylyDayNum").attr("checked", true);
                    $("#" + id + " #DaylyDayNum").removeAttr("disabled");
                    $("#" + id + " #txteverydaynum").removeAttr("disabled");
                    $("#" + id + " #DaylyWeekday").removeAttr("disabled");
                    $("#" + id + " #txteverydaynum").val(Me._recurringreport["RRDDailyEveryDaySpaceOutNum"]);
                }
                else
                {
                    //$("#" + id + " #DaylyDayNum").attr("disabled", true);
                    $("#" + id + " #DaylyWeekday").attr("checked", true);
                }

                $("#" + id + " #ScheduleMonthly").css("display", "none");
                $("#" + id + " #ScheduleWeekly").css("display", "none");
            }
            else if (Me._recurringreport["RRDEmailSchedule"] === "Weekly")
            {
                $("#" + id + " #ScheduleWeekly").css("display", "");
                $.each(Me._recurringreport["RRDWeeklyDays"], function(key, val) {
                    $("#" + id + " #ScheduleWeekly tr input").each(
                            function() {
                                if ($(this).val() === val)
                                {
                                    $(this).attr("checked", true);
                                }
                            }
                    );
                });

                $("#" + id + " #WeeklyWeekNum").val(Me._recurringreport["RRDWeeklySpaceOutNum"]);
                $("#" + id + " #WeeklyWeekNum").removeAttr("disabled");
                $("#" + id + " #ScheduleWeekly .Weekly").removeAttr("disabled");
                $("#" + id + " #ScheduleMonthly").css("display", "none");
                $("#" + id + " #ScheduleDayly").css("display", "none");
            }
            else if (Me._recurringreport["RRDEmailSchedule"] === "Monthly")
            {
                $("#" + id + " #ScheduleMonthly").css("display", "");
                $("#" + id + " #ScheduleWeekly").css("display", "none");
                $("#" + id + " #ScheduleDayly").css("display", "none");
            }

            $("#" + id + " #cbxtimeSchedule option").each(function() {
                if ($(this).val() === Me._recurringreport["RRDTimeOfDay"])
                {
                    $(this).attr("selected", "selected");
                }
            });
            $("#" + id + " #cbxestSchedule option").each(function() {
                if ($(this).val() === Me._recurringreport["RRDTimeZone"])
                {
                    $(this).attr("selected", "selected");
                }
            });
            if (Me._recurringreport["RRDMonthlyScheduleOptions"] === 1 || Me._recurringreport["RRDMonthlyScheduleOptions"] === "")
            {
                $("#" + id + " #cbxMonthlyDayOrder").attr("disabled", true);
                $("#" + id + " #cbxMonthlyDayOrderName").attr("disabled", true);
                $("#" + id + " #MonthlyMonthOnthedayNum").attr("disabled", true);
                $("#" + id + " #cbxMonthlyDayNum").removeAttr("disabled");
                $("#" + id + " #MonthlyMonthOndayNum").removeAttr("disabled");
                $("#" + id + " #MonthlyMonthOndayNum").val(Me._recurringreport["RRDMonthlyDayNumSpaceOutNum"]);
                $("#" + id + " #MonthlyDayNum").attr("checked", true);
                $("#" + id + " #cbxMonthlyDayNum option").each(function() {
                    if ($(this).val() === Me._recurringreport["RRDMonthlyScheduleDayNum"])
                    {
                        $(this).attr("selected", "selected");
                    }
                });
            }
            else if (Me._recurringreport["RRDMonthlyScheduleOptions"] === 2)
            {
                $("#" + id + " #cbxMonthlyDayOrder").removeAttr("disabled");
                $("#" + id + " #cbxMonthlyDayOrderName").removeAttr("disabled");
                $("#" + id + " #MonthlyMonthOnthedayNum").removeAttr("disabled");
                $("#" + id + " #MonthlyMonthOnthedayNum").val(Me._recurringreport["RRDMonthlyDayOrderSpaceOutNum"]);
                $("#" + id + " #cbxMonthlyDayNum").attr("disabled", true);
                $("#" + id + " #MonthlyDayOrder").attr("checked", true);
                $("#" + id + " #cbxMonthlyDayOrder option").each(function() {
                    if ($(this).val() === Me._recurringreport["RRDMonthlyScheduleDayOrder"])
                    {
                        $(this).attr("selected", "selected");
                    }
                });
                $("#" + id + " #cbxMonthlyDayOrderName option").each(function() {
                    if ($(this).val() === Me._recurringreport["RRDMonthlyScheduleDayOrderDay"])
                    {
                        $(this).attr("selected", "selected");
                    }
                });
            }
        }
    };
    //------------------------------------------------------------
    this.getTime = function()
    {
        var TIME = "";
        var timestamp = "";
        var timepart3 = 0;
        var timepart2 = "00";
        var timepart1 = "";
        var i = 0;
        for (i = 0; i < 24; i++)
        {

            if (i < 12 || i === 24)
                timepart3 = "AM";
            else
                timepart3 = "PM";
            if (i === 0)
                timepart1 = "12";
            else if (i < 13 && i > 0)
                timepart1 = i;
            else
                timepart1 = i - 12;
            timestamp = timepart1 + ":" + timepart2 + " " + timepart3;
            TIME += ("<option value='" + timestamp + "'>" + timestamp + "</option>");
        }
        return TIME;
    };
    //------------------------------------------------------------
    this.changeRptType = function()
    {
        var removearr = {
            "rptBid": ["Qty_Applicants", "_BidPayMax", "_BidAmount_Per"],
            "rptPart": ["_NumofNewParts", "_NumofReturnParts", "_PartManagerUpdatedByTech"],
            "rptVisit": ["_VisitStartTypeID", "_VisitStartDate", "_VisitStartTime", "_VisitEndDate", "_VisitEndTime", "_VisitActualCheckInDate", "_VisitActualCheckInTime", "_VisitActualCheckInResult", "_VisitActualCheckOutDate", "_VisitActualCheckOutTime", "_VisitActualCheckOutResult", "_VisitActualDurationResult"],
            "rptWintag": ["_WINTagAddedDate", "_WINTag", "_WINTagOwner", "_WINTagDetails", "_WINTagNotes", "_WINTagBillable"]
        };
        var removesection = {"rptBid": "fieldbid", "rptPart": "partmanager", "rptVisit": ["fieldwoDates"], "rptWintag": ["wintag"]};
        var repotTypeid = $(this).attr("id");
        $.each(removearr, function(id, tempval) {
            if (id !== repotTypeid)
            {
                var section = removesection[id];
                if (id === "rptWintag")
                {
                    $.each(section, function(id, val) {
                        var subsection = val;
                        $.each(Me.FieldOrder[subsection], function(id, val) {
                            Me.removeItem(section[0], id);
                        });
                    });
                }
                else if (id === "rptVisit")
                {
                    $.each(section, function(id, val) {
                        var subsection = val;
                        $.each(Me.FieldOrder[subsection], function(id, val) {
                            if ($.inArray(id, tempval) === -1)
                            {
                                Me.removeItem(section[0], id);
                            }
                        });
                    });
                }
                else
                {
                    $.each(Me.FieldOrder[section], function(id, val) {
                        if ($.inArray(id, tempval) === -1)
                        {
                            Me.removeItem(section, id);
                        }
                    });
                }
            }
        });
    };
    //------------------------------------------------------------
    //Report View
    //------------------------------------------------------------
    this.showhide = function()
    {
        var id = $(this).attr("groupid");
        if ($("#showhide" + id).css("display") === "none")
        {
            $("#showhide" + id).css("display", "");
            $("#group" + id).attr("src", "/widgets/images/arrows-up5.png");
        }
        else
        {
            $("#showhide" + id).css("display", "none");
            $("#group" + id).attr("src", "/widgets/images/arrows-down5.png");
        }
    };
    //------------------------------------------------------------
    this.sort_onchange = function()
    {
        var selectedval = $(this).val();
        var cursort = $("#hidsort" + $(this).attr("curid")).val();
        var anosort1 = "sort" + $(this).attr("anosort1");
        var anosort2 = "sort" + $(this).attr("anosort2");
        if (selectedval !== "")
        {
            $("#" + anosort1 + " option").each(function() {
                if ($(this).val() === cursort) {
                    $(this).removeAttr("disabled");
                }
                if ($(this).val() === selectedval) {
                    $(this).attr("disabled", "true");
                }
            });
            $("#" + anosort2 + " option").each(function() {
                if ($(this).val() === cursort) {
                    $(this).removeAttr("disabled");
                }
                if ($(this).val() === selectedval) {
                    $(this).attr("disabled", "true");
                }
            });
        }
        else
        {
            $("#" + anosort1 + " option").each(function() {
                if ($(this).val() === cursort) {
                    $(this).removeAttr("disabled");
                }
            });
            $("#" + anosort2 + " option").each(function() {
                if ($(this).val() === cursort) {
                    $(this).removeAttr("disabled");
                }
            });
        }
        $("#hidsort" + $(this).attr("curid")).val(selectedval);
//        Me.issavetemplate();
    };
    //------------------------------------------------------------
    this.clearsort = function() {
        $("#showhide2 select").each(function() {
            $("#" + $(this).attr("id") + " option").each(function() {
                $(this).removeAttr("disabled");
            });
            $(this).val("");
        });
        Me.issavetemplate();
    };
    //------------------------------------------------------------
    this.downloadreport = function()
    {
        var html = '\
        <div>\n\
            <div id="ContainerID_frmFilter">\n\
                <table>\n\
                    <tr>\n\
                        <td>\n\
                            <input type="radio" class="checkdownload" id="cmdDownloadexcel" value="csv" checked>\n\
                        </td>\n\
                        <td>Excel CSV</td>\n\
                    </tr>\n\
                    <tr>\n\
                        <td>\n\
                            <input type="radio" class="checkdownload" id="cmdDownloadpdf" value="pdf">\n\
                        </td>\n\
                        <td>PDF</td>\n\
                    </tr>\n\
                    <tr>\n\
                        <td>\n\
                            <input type="radio" class="checkdownload" id="cmdDownloadxml" value="xml">\n\
                        </td>\n\
                        <td>XML</td>\n\
                    </tr>\n\
                </table>\n\
            </div>\n\
            <div style="text-align:center;">\n\
                <input type="button" id="cmdDownloadCancel" class="link_button link_button_popup" value="Cancel">\n\
                <input type="button" id="cmddownloadfile" class="link_button link_button_popup" value="Download">\n\
            </div>\n\
        </div>';
        var opt = {};
        opt.width = 300;
        opt.height = '';
        opt.position = 'middle';
        opt.paddingTop = -200;
        opt.title = '<div style="text-align:center;">Download Options</div>';
        opt.body = html;
        opt.zindex = 999;
        var _roll = new FSPopupRoll();
        _roll.showNotAjax(null, null, opt);
        $("#cmdDownloadCancel").unbind('click').bind('click', function()
        {
            _roll.hide();
        });
        $("#cmddownloadfile").unbind('click').bind('click', function()
        {
            var Ext = "";
            if ($("#cmdDownloadexcel").is(":checked"))
            {
                Ext = "csv";
            }
            else if ($("#cmdDownloadpdf").is(":checked"))
            {
                Ext = "pdf";
            }
            else if ($("#cmdDownloadxml").is(":checked"))
            {
                Ext = "xml";
            }
            var reportname = $("#txtTemplateName").val();
            var sortData = ""; //14100
            sortData += '&sort_1=' + $("#sort1 option:selected").val() + '&sort_dir_1=' + $("#sortdir1 option:selected").val();
            sortData += '&sort_2=' + $("#sort2 option:selected").val() + '&sort_dir_2=' + $("#sortdir2 option:selected").val();
            sortData += '&sort_3=' + $("#sort3 option:selected").val() + '&sort_dir_3=' + $("#sortdir3 option:selected").val();
            var filterArray = [];
            filterArray = Me.reportfilteronview();
            var filterparam = "";
            $.each(filterArray, function(filterid, val) {
                $.each(val, function(itemid, val) {
                    if (itemid === "SelectMultiple")
                    {
                        $.each(val, function(id, val) {
                            filterparam += "&filterArray[" + filterArray[filterid]["fieldcode"] + "][" + itemid + "][" + id + "]=" + val;
                        });
                    }
                    else
                    {
                        filterparam += "&filterArray[" + filterArray[filterid]["fieldcode"] + "][" + itemid + "]=" + val;
                    }
                });
            });

            document.location = 'ExportReportViewToFile.php?Ext=' + Ext + "&rptid=" + $("#rptid").val() + '&txtTemplateName=' + reportname + sortData + filterparam;

        });
        $(".checkdownload").unbind('click').bind('click', function()
        {
            if ($(this).is(":checked"))
            {
                $(".checkdownload").removeAttr("checked");
                $(this).attr('checked', 'true');
            }
        });
    };

    //------------------------------------------------------------
    this.columnMoveUp = function()
    {
        var row = $(this).closest("tr");
        var currclass = row.attr("class");
        if (row.prev().length > 0) {
            row.attr("class", row.prev().attr("class"));
            row.prev().attr("class", currclass);
            row.insertBefore(row.prev());
        }
        $("#tblarrange tbody .up").html('Up');
        $("#tblarrange tbody > tr:first .up").html('&nbsp;&nbsp;');
        $("#tblarrange tbody .down").html('Down');
        $("#tblarrange tbody > tr:last .down").html('');
        return false;
    };

    //------------------------------------------------------------
    this.columnMoveDown = function()
    {
        var row = $(this).closest("tr");
        var currclass = row.attr("class");
        if (row.next().length > 0) {
            row.attr("class", row.next().attr("class"));
            row.next().attr("class", currclass);
            row.insertAfter(row.next());
        }
        $("#tblarrange tbody .up").html('Up');
        $("#tblarrange tbody > tr:first .up").html('&nbsp;&nbsp;');
        $("#tblarrange tbody .down").html('Down');
        $("#tblarrange tbody > tr:last .down").html('');
        return false;
    };

    //------------------------------------------------------------
    this.AverageIfOtherGrouped = function()
    {
        var grcol_No = 0;
        $("#groupTable tbody tr input").each(function() {
            if ($(this).is(":checked"))
            {
                if ($(this).attr("fieldtype") === "chkgro")
                {
                    grcol_No += 1;
                }
            }
        });
        if (grcol_No > 0)
        {
            $("#groupTable tbody tr input").each(function() {
                if ($(this).is(":checked"))
                {
                    if ($(this).attr("CountIfOtherGrouped") === "1" || $(this).attr("SumIfOtherGrouped") === "1" || $(this).attr("AverageIfOtherGrouped") === "1")
                    {
                        $(this).attr("checked", true);
                    }
                }
            });
        }
    };

    //------------------------------------------------------------
    this.bindreportevent = function()
    {
        Me.loadreport();
        $('<iframe id="printf" name="printf" style="display:none;" />').appendTo($('body'));
        $("#cmdReports").unbind("click").bind("click", Me.loadreport);
//        $("#cmdsendreport").unbind("click").bind("click", Me.reportsendnow_click);
        $("#sort1").unbind("change").bind("change", Me.sort_onchange);
        $("#sort2").unbind("change").bind("change", Me.sort_onchange);
        $("#sort3").unbind("change").bind("change", Me.sort_onchange);
        $("#resetsort").unbind('click').bind("click", Me.resetsort);
        $("#resetfilter").unbind('click').bind("click", Me.resetfilter);
        $("#group2").unbind("click").bind("click", Me.showhide);
        $("#group3").unbind("click").bind("click", Me.showhide);
        $("#group4").unbind("click").bind("click", Me.showhide);
        $("#cmddownload").unbind("click").bind("click", Me.downloadreport);

        //arrange column
        $("#tblarrange tbody > tr:first .up").html('&nbsp;&nbsp;');
        $("#tblarrange tbody > tr:last .down").html('');
        $(".up").unbind("click").bind("click", Me.columnMoveUp);
        $(".down").unbind("click").bind("click", Me.columnMoveDown);
        $(".DropdownBoxchange").unbind("change").bind("change", Me.DropdownBox_onChange);
        $(".txtN2").unbind("blur").bind("blur", Me.txtN2blur);
        $(".txtN2").unbind("focus").bind("focus", Me.txtN2focus);
        $('.showCal').calendar({dateFormat: 'MDY/'});
        $(".cmdremovedropdownitem").unbind("click").bind("click", Me.cmdremovedropdownitem_click);
        $(".clsdatetime").unbind("change").bind("change", filterDate);
    };

    //------------------------------------------------------------
    this.reportsendnow_click = function()
    {
        var id = "ContainerID_reportsendnow";
        var html = '\
            <div>\n\
                <div style="text-align:center;">\n\
                    <div>\n\
                        <u><b>Immediate Report Delivery</b></u>\n\
                    </div><br/>\n\
                </div>\n\
                <div id="' + id + '" style="max-height: 350px; overflow: auto;"></div>\n\
                <div style="text-align:center">\n\
                    <input type="button" class="link_button_popup" value="Cancel" id="cmdCancelreportsendnow">\n\
                    <input type="button" class="link_button_popup" value="Send" id="cmdSendNowreportsendnow">\n\
                </div>\n\
            </div>';
        Me.openpopup(html);
        $("#" + id).html($("#reportsendnow").html());
        Me.fanxyboxresize();
        $("#" + id + " #txtsendto").unbind("blur").bind("blur", Me.txtsendto_blur);
        $("#" + id + " #txtsendto").unbind("focus").bind("focus", Me.txtsendto_focus);
        $("#cmdCancelreportsendnow").unbind("click").bind("click", Me.Cancel_click);
        $("#cmdSendNowreportsendnow").unbind("click").bind("click", Me.SendNow_Click);
    };

    //------------------------------------------------------------
    this.SendNow_Click = function()
    {
        var to = $("#ContainerID_reportsendnow #txtsendto").val();
        if (to === $('<div />').html("Use &ldquo;,&rdquo; to separate multiple email addresses").text() || to === "")
        {
            alert("Send to is empty but required.");
            return false;
        }
        var tememail = to.split(",");
        var errmsg = "";
        $.each(tememail, function(id, val) {
            if (!Me.IsEmail(val))
            {
                errmsg = "Invalid email format.";
            }
        });
        if (errmsg !== "")
        {
            alert(errmsg);
            return false;
        }
        var fromName = $("#ContainerID_reportsendnow #txtFromName").val();
        var fromMail = $("#ContainerID_reportsendnow #txtFromMail").val();
        var subject = $("#ContainerID_reportsendnow #txtSubject").val();
        var message = $("#ContainerID_reportsendnow #txtmessage").val();
        var reportname = $("#txtTemplateName").val();

        var sortData = ""; //14100
        sortData += '&sort_1=' + $("#sort1 option:selected").val() + '&sort_dir_1=' + $("#sortdir1 option:selected").val();
        sortData += '&sort_2=' + $("#sort2 option:selected").val() + '&sort_dir_2=' + $("#sortdir2 option:selected").val();
        sortData += '&sort_3=' + $("#sort3 option:selected").val() + '&sort_dir_3=' + $("#sortdir3 option:selected").val();
        var filterArray = [];
        filterArray = Me.reportfilteronview();
        $.ajax({
            type: "POST",
            url: "ExportReportViewToFile.php",
            dataType: 'html',
            cache: false,
            context: this,
            data: {rptid: Me.report_id, txtTemplateName: reportname, Ext: "csv", filterArray: filterArray, attachmail: 1},
            success: function(data, status, xhr) {

                $.ajax({
                    type: "POST",
                    url: "RecurringDelivery.php",
                    dataType: 'html',
                    cache: false,
                    context: this,
                    data: {to: to, fromName: fromName, fromMail: fromMail, subject: subject, message: message, reportname: reportname, data: data},
                    success: function(data, status, xhr) {
                        var html = '\
                            <div style="text-align:center;">\n\
                                <div>\n\
                                    <u><b>Success!</b></u>\n\
                                </div><br/>\n\
                            </div>\n\
                            <div id="diverror" style="max-height: 350px; overflow: auto;">Your report has been sent.</div>';
                        Me.openpopup(html);
                        setTimeout($.fancybox.close, 2000);
                    }
                });
            }
        });
    };

    //------------------------------------------------------------
    this.IsEmail = function(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    };

    //------------------------------------------------------------
    this.loadreport = function()
    {
        var _url = "/widgets/dashboard/custom-report/report-view/";
        //--- page,size
        var page = $(this).attr('page');
        var size = $("#pagesize").val();
        if (typeof(page) === 'undefined')
            page = 1;
        if (typeof(size) === 'undefined')
            size = 50;
        if (typeof(rt) === 'undefined')
            rt = 1;
        var submitData = [];
        var arrangecol = "";
        $("#divsortable ul li").each(function() {
            if ($(this).attr("fieldname") !== "" && typeof($(this).attr("fieldname")) !== "undefined")
            {
                arrangecol += $(this).attr("fieldname") + ",";
            }
        });
        arrangecol = arrangecol.split(",");
        arrangecol.splice(arrangecol.length - 1, 1);

        var filterArray = [];
        filterArray = Me.reportfilteronview();
        submitData =
                {
                    "rptid": Me.report_id,
                    "v": Me.company,
                    "page": page,
                    "size": size,
                    "filterArray": filterArray,
                    "arrcol": arrangecol,
                    "sort_1": $("#sort1 option:selected").val(),
                    "sort_dir_1": $("#sortdir1 option:selected").val(),
                    "sort_2": $("#sort2 option:selected").val(),
                    "sort_dir_2": $("#sortdir2 option:selected").val(),
                    "sort_3": $("#sort3 option:selected").val(),
                    "sort_dir_3": $("#sortdir3 option:selected").val()
                };
        Me.showLoader();
        //---
        $.ajax({
            url: _url,
            dataType: 'html',
            data: submitData,
            cache: false,
            type: 'POST',
            context: this,
            success: function(data, status, xhr)
            {
                Me.hideLoader();
                $("#reportcontent").html("");
                $("#reportcontent").html(data);
                $("#cmddownload").unbind("click").bind("click", Me.downloadreport);
                $("#cmdPrint").unbind("click").bind("click", Me.downloadreportfile);
                $(".paginatorLinkActiv").unbind("click").bind("click", Me.loadreport_changepage);
                $("#pagesize").unbind("change").bind("change", Me.loadreport_changepage);
                var rowno = $("#reportcontent table").attr("rowno");
                if (rowno > 0)
                {
                    $("#cmdsendreport").unbind("click").bind("click",Me.reportsendnow_click);
                    $("#cmdsendreport").css("background","url('/widgets/images/button.png')");
                }
                else
                {
                    $("#cmdsendreport").unbind("click");
                    $("#cmdsendreport").css("background","url('/widgets/images/button_disabled.png')");
                }
            },
            error: function(xhr, status, error)
            {
            }
        });
    };
    //------------------------------------------------------------
    this.loadreport_changepage = function()
    {
        var _url = "/widgets/dashboard/custom-report/report-view/";
        //--- page,size
        var page = $(this).attr('page');
        var size = $("#pagesize").val();
        if (typeof(page) === 'undefined')
            page = 1;
        if (typeof(size) === 'undefined')
            size = 50;
        if (typeof(rt) === 'undefined')
            rt = 1;

        var filterArray = Me.reportfilteronview();
        var submitData =
                {
                    "rptid": Me.report_id,
                    "v": Me.company,
                    "page": page,
                    "size": size,
                    "filterArray": filterArray
                };
        Me.showLoader();
        //---
        $.ajax({
            url: _url,
            dataType: 'html',
            data: submitData,
            cache: false,
            type: 'POST',
            context: this,
            success: function(data, status, xhr)
            {
                Me.hideLoader();
                $("#reportcontent").html("");
                $("#reportcontent").html(data);
                $(".paginatorLinkActiv").unbind("click").bind("click", Me.loadreport_changepage);
                $("#pagesize").unbind("change").bind("change", Me.loadreport_changepage);
            },
            error: function(xhr, status, error)
            {
            }
        });
    };

    this.reportfilteronview = function()
    {
        var filterArray = [];
        $("#filterTable tbody tr").each(function()
        {
            var temarr = [];
            var fieldcode = $(this).attr("fieldcode");
            var fieldtype = $(this).attr("control_type");
            var datatype = $(this).attr("datatype");

            var el = "";
            if (fieldtype === "D")
            {
                el = $(this).find("select");
                var strfilteradditionalID = "";
                var filteradditionalSelecttarr = "";
                var fieldcodepart = "";
                $(el).parent().find(".cmdremovedropdownitem").each(function()
                {
                    fieldcodepart = $(this).attr("id").split("__");
                    strfilteradditionalID += fieldcodepart[2] + ",";
                });
                if (strfilteradditionalID !== "")
                {
                    filteradditionalSelecttarr = strfilteradditionalID.split(",");
                    filteradditionalSelecttarr.splice(filteradditionalSelecttarr.length - 1, 1);

                    temarr = {
                        'fieldcode': fieldcode,
                        'filter_at': 3,
                        'string_matching': "SelectMultiple",
                        'SelectMultiple': filteradditionalSelecttarr,
                        'FilterType': fieldtype,
                        'datatype': datatype
                    };
                }

            }
            else if (fieldtype === "T")
            {
                el = $(this).find("input");
                temarr = {
                    'fieldcode': fieldcode,
                    'filter_at': 3,
                    'string_matching': "ExactMatch",
                    'string_value': $(el).val(),
                    'FilterType': fieldtype,
                    'datatype': datatype
                };
            }
            else if (fieldtype === "DT")
            {
                var datetime_relative_data_ref = $("#Date_" + fieldcode + " option:selected").val();
                var datetime_range_from = $("#StartDate" + fieldcode).val();
                var datetime_range_to = $("#EndDate" + fieldcode).val();
                var temarr = [];
                temarr = {
                    'fieldcode': fieldcode,
                    'filter_at': 3,
                    'datetime_relative_data_ref': datetime_relative_data_ref,
                    'datetime_range_from': datetime_range_from,
                    'datetime_range_to': datetime_range_to
                };
            }
            else if (fieldtype === "N2")
            {
                var numeric_range_from = "";
                var numeric_range_to = "";
                if ($("#" + fieldcode + "__from").val() !== "#.##")
                {
                    numeric_range_from = $("#" + fieldcode + "__from").val();
                }
                if ($("#" + fieldcode + "__to").val() !== "#.##")
                {
                    numeric_range_to = $("#" + fieldcode + "__to").val();
                }
                temarr = {
                    'fieldcode': fieldcode,
                    'filter_at': 3,
                    'numeric_range_from': numeric_range_from,
                    'numeric_range_to': numeric_range_to,
                    'FilterType': fieldtype,
                    'datatype': datatype
                };
            }
            else if (fieldtype === "T2")
            {
                el = $(this).find("img");
                var time_from = $("#" + fieldcode + "__from").val();
                var time_to = $("#" + fieldcode + "__to").val();
                var time_tz = $("#" + fieldcode + "est").val();
                temarr = {
                    'fieldcode': fieldcode,
                    'filter_at': 3,
                    'time_from': time_from,
                    'time_to': time_to,
                    'time_tz': time_tz
                };
            }
            else if (fieldtype === "DYN")
            {
                el = $(this).find("select");
                if ($(el).val() !== "")
                {
                    temarr = {
                        'fieldcode': fieldcode,
                        'filter_at': 3,
                        'string_matching': "SelectMultiple",
                        'SelectMultiple': $(el).val(),
                        'FilterType': fieldtype,
                        'datatype': datatype
                    };
                }
            }

            if ($(temarr).size() > 0)
            {
                filterArray.push(temarr);
            }
        });
        return filterArray;
    };

    //------------------------------------------------------------
    var loaderVisible = false;
    this.showLoader = function() {
        if (!loaderVisible) {
            loaderVisible = true;
            var content = "<div id='loader'>Loading...</div>";
            $("<div></div>").fancybox(
                    {
                        'autoDimensions': true,
                        'showCloseButton': false,
                        'hideOnOverlayClick': false,
                        'content': content
                    }
            ).trigger('click');
        }
    };
    //------------------------------------------------------------
    this.hideLoader = function() {
        setTimeout(jQuery.fancybox.close, 500);
        loaderVisible = false;
    };
    //------------------------------------------------------------
    this.init = function() {
        $("#cmdChooseFilters").unbind("click").bind("click", Me.cmdShowFilterOrField_click);
        $("#cmdChooseFields").unbind("click").bind("click", Me.cmdShowFilterOrField_click);
        $(".addremovefilteritem").unbind("click").bind("click", Me.addremovefilteritem_click);
        $(".cmdremove").unbind("click").bind("click", Me.removeItem_click);
        if ($("#reportid").val() !== "")
        {
            $("#cmdAdd").unbind("click").bind("click", Me.cmdUpdateReport_click);
        }
        else
        {
            $("#cmdAdd").unbind("click").bind("click", Me.cmdAddReport_click);
        }

        $("#cmdsaveandgenerate").unbind("click").bind("click", Me.cmdsaveandgenerate_click);
        $(".clsdatetime").unbind("change").bind("change", filterDate);
        $(".DropdownBoxchange").unbind("change").bind("change", Me.DropdownBox_onChange);
        $(".cmdremovedropdownitem").unbind("click").bind("click", Me.cmdremovedropdownitem_click);
        $(".title").unbind("click").bind("click", Me.opendiv);
        $("#cmdRecurringReportStatus").unbind("click").bind("click", Me.cmdRecurringReportStatus_click);
        $(".rptType").unbind("click").bind("click", Me.changeRptType);
        $('.showCal').calendar({dateFormat: 'MDY/'});
        Me.setrecurringreport("reportrecurrent");
        $("#divfilters").css("display", "none"); //
        $("#divColoumns").css("display", "");
        $("#cmdChooseFields").attr("disabled", true).css("display", "none");
        $("#cmdChooseFilters").attr("disabled", true).css("display", "none");
        $("#cmdAdd").attr("disabled", true).css("display", "none");
        $.each(Me.FieldOrder, function(id, val) {
            Me.sortlist(id);
        });
        $(".txtN2").unbind("blur").bind("blur", Me.txtN2blur);
        $(".txtN2").unbind("focus").bind("focus", Me.txtN2focus);
    };
}