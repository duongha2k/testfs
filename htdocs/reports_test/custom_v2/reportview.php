<?
$page = "clients";
$option = "reports";
$selected = 'MetricsReport';
$UserType = (!empty($_SESSION['UserType']) ) ? $_SESSION['UserType'] : NULL;
$isFLSManager = (strtolower($Company_ID) == 'fls' && (strtolower($UserType) === 'install desk' || strtolower($UserType) === 'manager'));

if ($isFLSManager)
{
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
} else
{
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
}
require ("../../header.php");
require ("../../navBar.php");
require ("../../clients/includes/adminCheck.php");
$displayPMTools = true;
require_once("../../clients/includes/PMCheck.php");

$rptid = isset($_REQUEST['rptid']) ? $_REQUEST['rptid'] : "";
$v = isset($_REQUEST['v']) ? $_REQUEST['v'] : "";
$auth = $_SESSION['Auth_User'];

$client_login_old = $auth->login;

if (!empty($_SESSION['PM_Company_ID']))
{
    $user = new Core_Api_User();
    $clients = $user->getRepClientforCompany($_SESSION['PM_Company_ID']);
    $client_login = $clients[0]['UserName'];
    $client_password = $clients[0]['Password'];
} else
{
    $client_login = $auth->login;
    $client_password = $auth->password;
}
$client = new Core_Api_Class();
$isPM = $client->isGPMByUserName($client_login_old);

$CusReportClass = new Core_Api_CusReportClass();
$ReportInfo = $CusReportClass->getReportInfo2($rptid);
$FieldList = $CusReportClass->getReportFieldList2($rptid);
$FilterList = $CusReportClass->getFilterList2($rptid);

function getDay($Day)
{
    switch ($Day)
    {
        case 1:
            return(2);
            break;
        case 2:
            return(3);
            break;
        case 3:
            return(4);
            break;
        case 4:
            return(5);
            break;
        case 5:
            return(6);
            break;
        case 6:
            return(7);
            break;
        case 7:
            return(0);
            break;
    }
}

$Today = date("m/d/Y");
$Tomorrow = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + 1, date("Y")));
$Yesterday = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - 1, date("Y")));

$j = 1;
$k = 1;
for ($i = 1; $i <= 7; $i++)
{
    if ($j <= 5)
    {
        $lastdatenum = date("w", mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));

        if ($lastdatenum > 0 && $lastdatenum < 6)
        {
            switch ($j)
            {
                case 1:
                    $LastBusinessDay = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));
                    break;
                case 2:
                    $Last2Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));
                    break;
                case 3:
                    $Last3Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));
                    break;
                case 4:
                    $Last4Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));
                    break;
                case 5:
                    $Last5Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));
                    break;
}
            $j++;
        }
    }

    if ($k <= 5)
    {
        $nextdatenum = date("w", mktime(0, 0, 0, date("m"), date("d") + $i, date("Y")));
        if ($nextdatenum > 0 && $nextdatenum < 6)
        {
            switch ($k)
            {
                case 1:
                    $NextBusinessDay = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + $i, date("Y")));
                    break;
                case 2:
                    $Next2Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + $i, date("Y")));
                    break;
                case 3:
                    $Next3Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + $i, date("Y")));
                    break;
                case 4:
                    $Next4Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + $i, date("Y")));
                    break;
                case 5:
                    $Next5Businessdays = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + $i, date("Y")));
                    break;
            }
            $k++;
        }
    }
}

for ($i = 1; $i <= 7; $i++)
{
    $lastdatenum = date("w", mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));
    if ($lastdatenum > 0 && $lastdatenum < 6)
    {
        $LastDayend = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")));
        break;
    }
}

for ($i = 1; $i <= 7; $i++)
{
    $nextdatenum = date("w", mktime(0, 0, 0, date("m"), date("d") + $i, date("Y")));
    if ($nextdatenum > 0 && $nextdatenum < 6)
    {
        $NextDaystart = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + $i, date("Y")));
        break;
    }
}

$Next2days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + 2, date("Y")));
$Next3days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + 3, date("Y")));
$Next4days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + 4, date("Y")));
$Next5days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") + 5, date("Y")));

$Last2days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - 2, date("Y")));
$Last3days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - 3, date("Y")));
$Last4days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - 4, date("Y")));
$Last5days = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - 5, date("Y")));

// This Week (Mon = 1 - Sun = 7)
$BOW = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - getDay(date("N")) + 1, date("Y")));
$EOW = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - getDay(date("N")) + 7, date("Y")));

// last Week (Mon = 1 - Sun = 7)
$BOLW = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - getDay(date("N")) - 6, date("Y")));
$EOLW = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - getDay(date("N")), date("Y")));

// This Month
$BOM = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - date("d") + 1, date("Y")));
$EOM = date("m/d/Y", mktime(0, 0, 0, date("m") + 1, date("d") - date("d"), date("Y")));

// Last Month
$PrevBOM = date("m/d/Y", mktime(0, 0, 0, date("m") - 1, date("d") - date("d") + 1, date("Y")));
$PrevEOM = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d") - date("d"), date("Y")));

// Q1
$Q1Start = date("m/d/Y", mktime(0, 0, 0, 1, 1, date("Y")));
$Q1End = date("m/d/Y", mktime(0, 0, 0, 4, date("d") - date("d"), date("Y")));

// Q2
$Q2Start = date("m/d/Y", mktime(0, 0, 0, 4, 1, date("Y")));
$Q2End = date("m/d/Y", mktime(0, 0, 0, 7, date("d") - date("d"), date("Y")));

// Q3
$Q3Start = date("m/d/Y", mktime(0, 0, 0, 7, 1, date("Y")));
$Q3End = date("m/d/Y", mktime(0, 0, 0, 10, date("d") - date("d"), date("Y")));

// Q4
$Q4Start = date("m/d/Y", mktime(0, 0, 0, 10, 1, date("Y")));
$Q4End = date("m/d/Y", mktime(0, 0, 0, 13, date("d") - date("d"), date("Y")));

// H1
$H1Start = date("m/d/Y", mktime(0, 0, 0, 1, 1, date("Y")));
$H1End = date("m/d/Y", mktime(0, 0, 0, 7, date("d") - date("d"), date("Y")));

// H2
$H2Start = date("m/d/Y", mktime(0, 0, 0, 7, 1, date("Y")));
$H2End = date("m/d/Y", mktime(0, 0, 0, 13, date("d") - date("d"), date("Y")));

// YTD
$YTDStart = date("m/d/Y", mktime(0, 0, 0, 1, 1, date("Y")));
$YTDEnd = date("m/d/Y", mktime(0, 0, 0, date("m"), date("d"), date("Y")));

function getTime($RRDTimeOfDay)
{
    $TIME = "";
    $timestamp = "";
    $timepart3 = 0;
    $timepart2 = "";
    $timepart1 = "";
    $i = 0;
    for ($i = 0; $i < 24; $i++)
    {
        for ($j = 1; $j < 3; $j++)
        {
            if ($j == 1)
                $timepart2 = "00";
            else
                $timepart2 = ($j - 1) * 30;

            if ($i < 12 || $i == 24)
                $timepart3 = "AM";
            else
                $timepart3 = "PM";
            if ($i == 0)
                $timepart1 = "12";
            else if ($i < 13 && $i > 0)
                $timepart1 = $i;
            else
                $timepart1 = $i - 12;

            $timestamp = $timepart1 . ":" . $timepart2 . " " . $timepart3;
            $selected = "";
            if ($RRDTimeOfDay == $timestamp)
            {
                $selected = "selected";
            }

            $TIME .= ("<option value='" . $timestamp . "' " . $selected . ">" . $timestamp . "</option>");
        }
    }
    return $TIME;
}

function loaddatetime($key, $data_ref, $from, $to, $section)
{
    if (!empty($from))
    {
    $datefrom = new DateTime($from);
    $from = $datefrom->format('m/d/Y');
    }
    if (!empty($to))
    {
    $dateto = new DateTime($to);
    $to = $dateto->format('m/d/Y');
    }
    $str = "<select class='clsdatetime' section='" . $section . "' sectionid='" . $key . "' style='width:210px' id='Date_" . $key . "'>
                                    <option value='FixedDateRange' " . ($data_ref == "FixedDateRange" ? "selected" : "") . ">Fixed Date Range</option>
                                    <option value='Today' " . ($data_ref == "Today" ? "selected" : "") . ">Today</option>
                                    <option value='TodayAndTomorrow' " . ($data_ref == "TodayAndTomorrow" ? "selected" : "") . ">Today and Tomorrow</option>
                                    <option value='TodayAndNextBusinessDay' " . ($data_ref == "TodayAndNextBusinessDay" ? "selected" : "") . ">Today and Next Business Day</option>
                                    <option value='Tomorrow' " . ($data_ref == "Tomorrow" ? "selected" : "") . ">Tomorrow</option>
                                    <option value='NextBusinessDay' " . ($data_ref == "NextBusinessDay" ? "selected" : "") . ">Next Business Day</option>
                                    <option value='Next2Days' " . ($data_ref == "Next2Days" ? "selected" : "") . ">Next 2 Days</option>
                                    <option value='Next2BusinessDays' " . ($data_ref == "Next2BusinessDays" ? "selected" : "") . ">Next 2 Business Days</option>
                                    <option value='Next3Days' " . ($data_ref == "Next3Days" ? "selected" : "") . ">Next 3 Days</option>
                                    <option value='Next3BusinessDays' " . ($data_ref == "Next3BusinessDays" ? "selected" : "") . ">Next 3 Business Days</option>
                                    <option value='Next4Days' " . ($data_ref == "Next4Days" ? "selected" : "") . ">Next 4 Days</option>
                                    <option value='Next4BusinessDays' " . ($data_ref == "Next4BusinessDays" ? "selected" : "") . ">Next 4 Business Days</option>
                                    <option value='Next5Days' " . ($data_ref == "Next5Days" ? "selected" : "") . ">Next 5 Days</option>
                                    <option value='Next5BusinessDays' " . ($data_ref == "Next5BusinessDays" ? "selected" : "") . ">Next 5 Business Days</option>
                                    <option value='ThisWeek' " . ($data_ref == "ThisWeek" ? "selected" : "") . ">This Week</option>
                                    <option value='ThisMonth' " . ($data_ref == "ThisMonth" ? "selected" : "") . ">This Month</option>
                                    <option value='Yesterday' " . ($data_ref == "Yesterday" ? "selected" : "") . ">Yesterday</option>
                                    <option value='LastBusinessDay' " . ($data_ref == "LastBusinessDay" ? "selected" : "") . ">Last Business Days</option>
                                    <option value='Last2Days' " . ($data_ref == "Last2Days" ? "selected" : "") . ">Last 2 Days</option>
                                    <option value='Last2BusinessDays' " . ($data_ref == "Last2BusinessDays" ? "selected" : "") . ">Last 2 Business Days</option>
                                    <option value='Last3Days' " . ($data_ref == "Last3Days" ? "selected" : "") . ">Last 3 Days</option>
                                    <option value='Last3BusinessDays' " . ($data_ref == "Last3BusinessDays" ? "selected" : "") . ">Last 3 Business Days</option>
                                    <option value='Last4Days' " . ($data_ref == "Last4Days" ? "selected" : "") . ">Last 4 Days</option>
                                    <option value='Last4BusinessDays' " . ($data_ref == "Last4BusinessDays" ? "selected" : "") . ">Last 4 Business Days</option>
                                    <option value='Last5Days' " . ($data_ref == "Last5Days" ? "selected" : "") . ">Last 5 Days</option>
                                    <option value='Last5BusinessDays' " . ($data_ref == "Last5BusinessDays" ? "selected" : "") . ">Last 5 Business Days</option>
                                    <option value='LastWeek' " . ($data_ref == "LastWeek" ? "selected" : "") . ">Last Week</option>
                                    <option value='LastMonth' " . ($data_ref == "LastMonth" ? "selected" : "") . ">Last Month</option>
                                    <option value='Q1' " . ($data_ref == "Q1" ? "selected" : "") . ">Q1 " . date("Y") . "</option>
                                    <option value='Q2' " . ($data_ref == "Q2" ? "selected" : "") . ">Q2 " . date("Y") . "</option>
                                    <option value='Q3' " . ($data_ref == "Q3" ? "selected" : "") . ">Q3 " . date("Y") . "</option>
                                    <option value='Q4' " . ($data_ref == "Q4" ? "selected" : "") . ">Q4 " . date("Y") . "</option>
                                    <option value='H1' " . ($data_ref == "H1" ? "selected" : "") . ">H1 " . date("Y") . "</option>
                                    <option value='H2' " . ($data_ref == "H2" ? "selected" : "") . ">H2 " . date("Y") . "</option>
                                    <option value='YTD' " . ($data_ref == "YTD" ? "selected" : "") . ">YTD</option>
                                </select>&nbsp;between&nbsp;<input type='text' value='" . $from . "' style='width:80px' class='showCal' id='StartDate" . $key . "'>
                            &nbsp;and&nbsp;<input type='text' value='" . $to . "' style='width:80px' class='showCal' id='EndDate" . $key . "'>";
    return $str;
}

function loadDropdown($ComID, $isPM, $fieldCode, $SelectMultiple)
{
    $CusReportClass = new Core_Api_CusReportClass();
    $SelectMultiple = $SelectMultiple;
    $DropdownOptions = $CusReportClass->getFilterDropdownOptions($fieldCode, $ComID, $isPM);
    $str .="<select sectionid='$fieldCode' class='DropdownBoxchange' style='min-width:155px' id='$fieldCode'>";
    if (!empty($DropdownOptions) && count($DropdownOptions))
    {
        foreach ($DropdownOptions as $option)
        {
            $value = $option['Value'];
            $caption = $option['Caption'];
            $display = 1;
            if (!empty($SelectMultiple) && count($SelectMultiple))
            {
                foreach ($SelectMultiple as $selValue)
                {
                    if ($selValue == $value)
                    {
                        $display = 0;
                    }
                }
            }
            $str .= "<option value='$value' style='" . ($display == 1 ? "" : "display:none;") . "' ".(!empty($option["Disable"])?$option["Disable"]:"").">$caption</option>";
        }
    }
    $str.="</select><br/>";
    if (!empty($SelectMultiple) && count($SelectMultiple))
    {
        foreach ($SelectMultiple as $selValue)
        {
            $selKey =  $selValue;
            $option = $DropdownOptions["$selKey"];
            $caption = $option['Caption'];
            $str.="
                <span>
                    <img height='15px' id='$fieldCode" . "__$fieldCode" . "__$selKey' valstr='$caption' style='cursor:pointer;' src='/widgets/css/images/removedoc.png' class='cmdremovedropdownitem'>&nbsp;&nbsp;$caption
                    <br>
                </span>";
        }
    }
    return $str;
}

function loadDYN($fieldCode, $SelectMultiple)
{
    $SelectMultiple = $SelectMultiple;
    $DropdownOptions = array("t" => "Yes", "f" => "No", "a" => "Any");
    $str .="<select sectionid='$fieldCode' class='' style='min-width:155px' id='$fieldCode'>";
    foreach ($DropdownOptions as $key => $val)
    {
        $str .= "<option value='$key' " . ($SelectMultiple[0] == $key ? "selected" : "") . ">$val</option>";
    }
    $str.="</select><br/>";
    return $str;
}

function loadtext($fieldCode, $stringValue)
{
    $str = "<input type='text' style='width:155px' id='$fieldCode' value='$stringValue'>";
    return $str;
}

function loadN2($fieldCode, $numFrom, $numTo)
{
    $str = "from&nbsp;<input class='txtN2' type='text' id='" . $fieldCode . "__from' " . $fieldCode . "_type='N2' style='width: 50px;" . (empty($numFrom) ? "color:#A6A6A6;" : "") . "'  value='" . (empty($numFrom) ? "#.##" : $numFrom) . "' />&nbsp;to&nbsp;
                <input class='txtN2' type='text' id='" . $fieldCode . "__to' " . $fieldCode . "_type='N2' style='width: 50px;" . (empty($numTo) ? "color:#A6A6A6;" : "") . "' value='" . (empty($numTo) ? "#.##" : $numTo) . "' />";
    return $str;
}

function loadT2($fieldCode, $timeFrom, $timeTo, $time_tz)
{
    $str = "from <select  style='width:110px' id='" . $fieldCode . "__from' " . $fieldCode . "_type='T2'> " . getT2options($timeFrom) . "</select>
            &nbsp;&nbsp;&nbsp;to <select  style='width:110px' id='" . $fieldCode . "__to' " . $fieldCode . "_type='T2'> " . getT2options($timeTo) . "</select>
            &nbsp;&nbsp;&nbsp;<select id='" . $fieldCode . "est' style='width:150px;'>
                        <option value ='-10' " . ($time_tz == "-10" ? "selected" : "") . ">Hawaii (GMT-10:00)</option>
                        <option value ='-9' " . ($time_tz == "-9" ? "selected" : "" ) . ">Alaska (GMT-8:00)</option>
                        <option value ='-8' " . ($time_tz == "-8" ? "selected" : "") . ">Pacific (GMT-7:00)</option>
                        <option value ='-7' " . ($time_tz == "-7" ? "selected" : "") . ">Mountain (GMT-6:00)</option>
                        <option value ='-6' " . ($time_tz == "-6" ? "selected" : "") . ">Central (GMT-5:00)</option>
                        <option value ='-5' " . ($time_tz == "-5" ? "selected" : "") . ">Eastern (GMT-4:00)</option>
                        </select>";
    return $str;
}

function getT2options($selValue)
{
    $timepart3 = 0;
    $timepart2 = "";
    $timepart1 = "";
    $Time = "";
    $Time .= "<option value=''>Select Time</option>";
    for ($i = 0; $i < 24; $i++)
    {
        for ($j = 1; $j < 5; $j++)
        {
            if ($j == 1)
                $timepart2 = "00";
            else
                $timepart2 = ($j - 1) * 15;

            if ($i < 12 || $i == 24)
                $timepart3 = "AM";
            else
                $timepart3 = "PM";

            if ($i == 0)
                $timepart1 = "12";
            else if ($i < 13 && $i > 0)
                $timepart1 = $i;
            else
                $timepart1 = $i - 12;

            $timestamp = $timepart1 . ":" . $timepart2 . " " . $timepart3;

            $selected = '';
            if ($selValue == $timestamp)
            {
                $selected = "selected='selected'";
            }
            $Time .= "<option value='" . $timestamp . "' $selected >" . $timestamp . "</option>";
        }
    }
    return $Time;
}

function buildcontrol($filterType, $ComID, $isPM, $fieldCode, $fieldvalue)
{
    switch ($filterType)
    {
        case "D":
            return loadDropdown($ComID, $isPM, $fieldCode, $fieldvalue['SelectMultiple']);
            break;
        case "T":
            return loadtext($fieldCode, $fieldvalue['string_value']);
            break;
        case "DT":
            return loaddatetime($fieldCode, $fieldvalue["datetime_relative_data_ref"], $fieldvalue["datetime_range_from"], $fieldvalue["datetime_range_to"]);
            break;
        case "N2":
            return loadN2($fieldCode, $fieldvalue['numeric_range_from'], $fieldvalue['numeric_range_to']);
            break;
        case "DYN":
            return loadDYN($fieldCode, $fieldvalue['SelectMultiple']);
            break;
        case "T2":
            return loadT2($fieldCode, $fieldvalue['time_from'], $fieldvalue['time_to'], empty($fieldvalue['time_tz'])?"Central":$fieldvalue['time_tz']);
            break;
    }
}

$countRows = $CusReportClass->runReport2($rptid, 0, 0, true);
$txtTemplateName = $ReportInfo['rpt_name'];

function loadsort($FieldList, $selectValue = '')
{
    $sort = "";
    if (!(empty($FieldList)))
    {
        foreach ($FieldList as $Field)
        {
            $fieldTitle = $Field['field_title'];
            $selected = "";
            if ($Field['fieldname'] == $selectValue)
            {
                $selected = "selected='selected'";
            }
            $sort.="<option  value='" . $Field['fieldname'] . "' $selected >" . $fieldTitle . "</option>";
        }
    }
    return $sort;
}

$FieldOrder = array(
    "WIN_NUM", "WO_ID", "Project_ID", "PO", "WorkOrderOwner", "Type_ID", "WO_Category_ID", "_AccountManager",
    "_ClientServicesDirector", "_ServiceType", "_LegalEntity", "Headline", "Description", "Requirements", "SpecialInstructions",
    "Ship_Contact_Info", "Qty_Devices", "Lead", "Assist", "Update_Reason", "Update_Requested", "ShortNotice", "_VisitStartTypeID",
    "_VisitStartDate", "_VisitStartTime", "_VisitEndDate", "_VisitEndTime", "_VisitSiteTimeZone", "_VisitDayPart", "_VisitLeadTime",
    "_VisitEstimatedDuration", "Qty_Visits", "_VisitActualCheckInDate", "_VisitActualCheckInTime", "_VisitActualCheckInResult", "_VisitActualCheckOutDate",
    "_VisitActualCheckOutTime", "_VisitActualCheckOutResult", "_VisitActualCalculatedDuration", "_VisitActualDurationResult", "SiteNumber",
    "SiteName", "Address", "City", "State", "Zipcode", "Country", "_Geography", "Region", "Route", "Latitude", "Longitude", "Site_Contact_Name",
    "SiteEmail", "SitePhone", "SiteFax", "_TechID", "_BiddingTechFLSID", "_Tech_Fname", "_Tech_Lname", "_BiddingTechEmail", "_BiddingTechPhone",
    "_BiddingTechPreferred", "_BidPayMax", "_BidAmount_Per", "_BidAmount", "_Comments", "_Bid_Date", "_Hide", "Qty_Applicants", "Tech_ID",
    "FLS_ID", "Tech_FName", "Tech_LName", "_PrimaryEmail", "_SecondaryEmail", "_PrimaryPhone", "_SecondaryPhone", "_AssignedTechPreferred",
    "_AssignedTechDenied", "_TechBidAmount", "_TechAmount_Per", "bid_comments", "_TechBid_Date", "_TechBackedOut", "BackOut_Tech",
    "_BackOutFLSTechID", "_TechNoShowed", "NoShow_Tech", "_NoShowFLSTechID", "BU1FLSID", "Backup_FLS_ID", "BU1Info", "_EmailBlastRadius",
    "ProjectManagerName", "ProjectManagerEmail", "ProjectManagerPhone", "ResourceCoordinatorName", "ResourceCoordinatorEmail",
    "ResourceCoordinatorPhone", "_ACSNotifiPOC", "_ACSNotifiPOC_Email", "_ACSNotifiPOC_Phone", "EmergencyName", "EmergencyEmail",
    "EmergencyPhone", "CheckInOutName", "CheckInOutEmail", "CheckInOutNumber", "TechnicalSupportName", "TechnicalSupportEmail",
    "TechnicalSupportPhone", "WorkOrderReviewed", "TechCheckedIn_24hrs", "StoreNotified", "NotifyNotes", "PreCall_1",
    "PreCall_1_Status", "PreCall_2", "PreCall_2_Status", "CheckedIn", "Checkedby", "CheckInNotes", "Incomplete_Paperwork", "Paperwork_Received",
    "MissingComments", "ClientComments", "SATRecommended", "SATPerformance", "TechMarkedComplete", "CallClosed", "ContactHD", "HDPOC",
    "Unexpected_Steps", "Unexpected_Desc", "AskedBy", "AskedBy_Name", "FLS_OOS", "Penalty_Reason", "TechMiles", "TechComments", "_WINTagAdded",
    "_TotalNumOfWINTags", "_WINTagAddedDate", "_WINTagAddedBy",/* "_WINTagComplete", "_WINTagIncomplete", "_WINTagRescheduled",
    "_WINTagSiteCancelled", */ "_WINTag", "_WINTagOwner", "_WINTagDetails", "_WINTagNotes", "_WINTagBillable", "_WINTagCheckInResult",
    "_WINTagCheckOutResult", "_WINTagDurationResult", "_WINTagTechPreferenceRating", "_WINTagTechPerformanceRating", "_NumofNewParts",
    "_NumofReturnParts", "_PartManagerUpdatedByTech", "_NewPartPartNum", "_NewPartModelNum", "_NewPartSerialNum", "_NewPartEstArrival",
    "_NewPartCarrier", "_NewPartTrackingNum", "_NewPartInfo", "_NewPartShipsTo", "_NewPartShippingNotes", "_NewPartisConsumable",
    "_ReturnPartPartNum", "_ReturnPartModelNum", "_ReturnPartSerialNum", "_ReturnPartEstArrial", "_ReturnPartCarrier", "_ReturnPartTrackingNum",
    "_ReturnPartInfo", "_ReturnPartRMA", "_ReturnPartInstructions", "PayMax", "Tech_Bid_Amount", "Amount_Per", "calculatedTechHrs",
    "baseTechPay", "Additional_Pay_Amount", "OutofScope_Amount", "TripCharge", "MaterialsReimbursement", "MileageReimbursement", "AbortFee",
    "AbortFeeAmount", "Total_Reimbursable_Expense", "Add_Pay_AuthBy", "Add_Pay_Reason", "Approved", "approvedBy", "PayAmount", "PricingRuleID",
    "PricingCalculationText", "PricingRuleApplied", "PricingRan", "Invoiced", "calculatedInvoice", "PcntDeduct", "PcntDeductPercent",
    "Net_Pay_Amount", "TechPaid", "FS_InternalComments", "DateEntered", "Username", "_PublishedDate", "_PublishedBy", "SourceByDate",
    "Date_Assigned", "_AssignedBy", "DateNotified", /* "Notifiedby", */ "Date_Completed", "DateIncomplete", "_MarkedIncompleteBy", "DateApproved",
    "_approvedBy", "DateInvoiced", "DatePaid", /* "Deactivated", */ "DeactivatedDate", "DeactivatedBy", "DeactivationCode", "Deactivated_Reason",
    "lastModDate", "lastModBy");
?>
<script type="text/javascript" src="js/custom_report.js"></script>
<script type="text/javascript" src="/widgets/js/jquery-ui-1.8.min.js"></script>
<link rel="stylesheet" href="../../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../../library/jquery/calendar/jquery-calendar.js"></script>
<style>
    div#loader {
        background: url("/widgets/images/loader.gif") no-repeat scroll center center #FFFFFF;
        font: 16px Tahoma,Geneva,sans-serif;
        height: 100px;
        padding: 10px;
        text-align: center;
        width: 100px;
    }
    .sortable-item
    {
        cursor: move;
    }
</style>

<div style="padding-left:5px;">
    <a href="reportlist.php?v=<?= $v ?>">Back to List</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="custom_report.php?report_id=<?= $rptid ?>&v=<?= $v ?>">Edit Report</a>
</div>
<div style="padding:5px;text-align: center;">
    <b><?= $txtTemplateName ?></b>
</div>
<br/>
<form id='frmfilter'>
    <input id="txtTemplateName" type='hidden' name='txtTemplateName' value='<?= $txtTemplateName ?>'>
    <input id="rptid" type='hidden' name='rptid' value='<?= $rptid ?>'>
    <table style="background-color: #f2f1ef;width:100%;">
        <col width="33%">
        <col width="33%">
        <col width="33%">
        <tr>
            <td style="vertical-align: top;">
                <div style="padding:5px;">
                    <label><b>Filter</b></label> 
                    <span>(<a id="resetfilter" href="javascript:;">reset</a>)</span>
                    <img id="group3" height="13px" src="/widgets/images/arrows-down5.png" groupid="3" title="" alt="">
                </div>
                <div id="showhide3" style="display:none;margin-left:20px;">
                    <table id="filterTable" cellpadding="5" cellspacing="5">
                        <colgroup>
                            <col width="*">
                            <col width="*">
                        </colgroup>
                        <tbody>
                            <?php
                            foreach ($FieldOrder as $key => $val)
                            {
                                if (!empty($FieldList[$val]["FilterType"]))
                                {
                                    ?>
                                    <tr fieldcode="<?= $val ?>" control_type="<?= $FieldList[$val]["FilterType"] ?>" datatype="<?= $FilterList[$val]["datatype"] ?>">
                                        <td style="vertical-align:top;">
                                            <?= $FieldList[$val]["field_title"]; ?>
                                        </td>
                                        <td>
                                            <?=
                                            buildcontrol($FieldList[$val]["FilterType"], $v, $isPM, $val, $FilterList[$val], "");
                                            ?>
                                        </td>
                                    </tr>
                                    <?
                                }
                            }
                            ?>
                        <tbody>     
                    </table>
                </div>
            </td>
            <td style="vertical-align: top;">
                <div style="padding:5px;">
                    <label><b>Sort</b></label> 
                    <span>(<a id="resetsort" href="javascript:;">reset</a>)</span>
                    <img id="group2" height="13px" src="/widgets/images/arrows-up5.png" groupid="2" title="" alt="">
                </div>
                <div id="showhide2" style='margin-left:20px;'>
                    <table id='tblSort'>
                        <tr>
                            <td>
                                <select id="sort1" curid="1" anosort1="2"  anosort2="3" style="width:150px;">
                                    <option value="">None</option>
                                    <?= loadsort($FieldList, $ReportInfo['sort_1']); ?>
                                </select>
                                <input type="hidden" id="hidsort1" value="">
                            </td>
                            <td>
                                <select  class="onchange" id="sortdir1" style="width:100px;">
                                    <option value="asc" <?= $ReportInfo['sort_dir_1'] == 'asc' ? "selected='selected'" : "" ?> >Ascending</option>
                                    <option value="desc" <?= $ReportInfo['sort_dir_1'] == 'desc' ? "selected='selected'" : "" ?> >Descending</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <select id="sort2" curid="2" anosort1="1"  anosort2="3" style="width:150px;">
                                    <option value="">None</option>
                                    <?= loadsort($FieldList, $ReportInfo['sort_2']); ?>
                                </select>
                                <input type="hidden" id="hidsort2" value="">
                            </td>
                            <td>
                                <select class="onchange" id="sortdir2" style="width:100px;">
                                    <option value="asc" <?= $ReportInfo['sort_dir_2'] == 'asc' ? "selected='selected'" : "" ?>>Ascending</option>
                                    <option value="desc" <?= $ReportInfo['sort_dir_2'] == 'desc' ? "selected='selected'" : "" ?>>Descending</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <select id="sort3" curid="3" anosort1="1"  anosort2="2" style="width:150px;">
                                    <option value="">None</option>
                                    <?= loadsort($FieldList, $ReportInfo['sort_3']); ?>
                                </select>
                                <input type="hidden" id="hidsort3" value="">
                            </td>
                            <td>
                                <select class="onchange" id="sortdir3" style="width:100px;">
                                    <option value="asc" <?= $ReportInfo['sort_dir_3'] == 'asc' ? "selected='selected'" : "" ?>>Ascending</option>
                                    <option value="desc" <?= $ReportInfo['sort_dir_3'] == 'desc' ? "selected='selected'" : "" ?>>Descending</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td style="vertical-align: top;">
                <div style="padding:5px;">
                    <label><b>Arrange Columns</b></label> 
                    <img id="group4" height="13px" src="/widgets/images/arrows-down5.png" groupid="4" title="" alt="">
                </div>
                <div id="showhide4" style="display:none;margin-left:20px;">
                    <div id="divsortable">
                        <ul class="sortable-list ui-sortable">
                            <?
                            foreach ($FieldList as $field)
                            {
                                ?>
                                <li class="sortable-item odd_tr"  fieldname="<?= $field['fieldname'] ?>"><?= $field['field_title'] ?></li>
                                <?
                            }
                            ?>
                        </ul>
                    </div>
                </div>
            </td>
                        </tr>
                    </table>
</form>
<div style="padding-top: 5px;">
    <input type="button" value="Re-Generate Report" class="link_button middle2_button" id="cmdReports" name="cmdReports" page="1">
    <div style="float:right;padding-right:270px;">
        <?php
    if ($countRows > 0)
    {
        ?>
        <input type="button" value="Send Now" class="link_button" id="cmdsendreport" name="cmdsendreport" style="width: 96px; background: url('/widgets/images/button.png') no-repeat scroll 0px 0px transparent;">
        <?
    } else
    {
        ?>
        <input type="button" value="Send Now" class="link_button"  id="cmdsendreport" style="cursor:default;width: 96px; background: url('/widgets/images/button_disabled.png') no-repeat scroll 0px 0px transparent;">
        <?
    }
    ?>
        <input type="button" value="Download" class="link_button download_button" id="cmddownload">
        <!--<input type="button" value="Print" class="link_button link_button_popup" id="cmdPrint" name="cmdPrint" filetype="Printter">-->
    </div>
</div>
<div id="reportcontent">
</div>
<div id="reportsendnow" style="display:none;">
    <table cellspacing="5" cellpadding="5" width="100%">
        <tr>
            <td colspan="2">
                <table cellspacing="5" cellpadding="5" width="100%">
                    <tr>
                        <td style="width:90px;" id="lblsendto">Send to:</td>
                        <td>
                            <input class="RecurringReportEle" id="txtsendto" type="text" style="width:94%;color:#a6a6a6" value="Use &ldquo;,&rdquo; to separate multiple email addresses" />&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>From Name:</td>
                        <td>
                            <input id="txtFromName" class="RecurringReportEle" type="text" style="width:94%;" value="FS-ReportWriter"/>
                        </td>
                    </tr>
                    <tr>
                        <td>From Email:</td>
                        <td>
                            <input id="txtFromMail" class="RecurringReportEle" type="text" style="width:94%;" value="FS-ReportWriter@fieldsolutions.com"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Subject:</td>
                        <td>
                            <input id="txtSubject" class="RecurringReportEle" type="text" style="width:94%;" value="FieldSolutions Report: <?= $txtTemplateName ?>"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Message:</td>
                        <td>
                            <textarea id="txtmessage" class="RecurringReportEle" style="width: 93%; height: 58px;resize: none;">Please find <?= $txtTemplateName ?> attached.
Thanks, Your FieldSolutions Team
                            </textarea>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

<script>
    var currYr = "<?= date("Y") ?>";
    var Tomorrow = <?= "\"" . $Tomorrow . "\""; ?>;
    var Yesterday = <?= "\"" . $Yesterday . "\""; ?>;
    var LastDayend = <?= "\"" . $LastDayend . "\""; ?>;
    var NextDaystart = <?= "\"" . $NextDaystart . "\""; ?>;
    var Nextbusinessday = <?= "\"" . $NextBusinessDay . "\""; ?>;
    var Next2days = <?= "\"" . $Next2days . "\""; ?>;
    var Next3days = <?= "\"" . $Next3days . "\""; ?>;
    var Next4days = <?= "\"" . $Next4days . "\""; ?>;
    var Next5days = <?= "\"" . $Next5days . "\""; ?>;
    var Next2businessdays = <?= "\"" . $Next2Businessdays . "\""; ?>;
    var Next3businessdays = <?= "\"" . $Next3Businessdays . "\""; ?>;
    var Next4businessdays = <?= "\"" . $Next4Businessdays . "\""; ?>;
    var Next5businessdays = <?= "\"" . $Next5Businessdays . "\""; ?>;

    var Lastbusinessday = <?= "\"" . $LastBusinessDay . "\""; ?>;
    var Last2days = <?= "\"" . $Last2days . "\""; ?>;
    var Last3days = <?= "\"" . $Last3days . "\""; ?>;
    var Last4days = <?= "\"" . $Last4days . "\""; ?>;
    var Last5days = <?= "\"" . $Last5days . "\""; ?>;

    var Last2businessdays = <?= "\"" . $Last2Businessdays . "\""; ?>;
    var Last3businessdays = <?= "\"" . $Last3Businessdays . "\""; ?>;
    var Last4businessdays = <?= "\"" . $Last4Businessdays . "\""; ?>;
    var Last5businessdays = <?= "\"" . $Last5Businessdays . "\""; ?>;

    var Today = <?= "\"" . $Today . "\""; ?>;
    var BOW = <?= "\"" . $BOW . "\""; ?>;
    var EOW = <?= "\"" . $EOW . "\""; ?>;
    var BOLW = <?= "\"" . $BOLW . "\""; ?>;
    var EOLW = <?= "\"" . $EOLW . "\""; ?>;
    var BOM = <?= "\"" . $BOM . "\""; ?>;
    var EOM = <?= "\"" . $EOM . "\""; ?>;
    var PrevBOM = <?= "\"" . $PrevBOM . "\""; ?>;
    var PrevEOM = <?= "\"" . $PrevEOM . "\""; ?>;
    var Q1Start = <?= "\"" . $Q1Start . "\""; ?>;
    var Q1End = <?= "\"" . $Q1End . "\""; ?>;
    var Q2Start = <?= "\"" . $Q2Start . "\""; ?>;
    var Q2End = <?= "\"" . $Q2End . "\""; ?>;
    var Q3Start = <?= "\"" . $Q3Start . "\""; ?>;
    var Q3End = <?= "\"" . $Q3End . "\""; ?>;
    var Q4Start = <?= "\"" . $Q4Start . "\""; ?>;
    var Q4End = <?= "\"" . $Q4End . "\""; ?>;
    var H1Start = <?= "\"" . $H1Start . "\""; ?>;
    var H1End = <?= "\"" . $H1End . "\""; ?>;
    var H2Start = <?= "\"" . $H2Start . "\""; ?>;
    var H2End = <?= "\"" . $H2End . "\""; ?>;
    var YTDStart = <?= "\"" . $YTDStart . "\""; ?>;
    var YTDEnd = <?= "\"" . $YTDEnd . "\""; ?>;
    var _custom_report = null;
    $(document).ready(function()
    {
        var _custom_report = custom_report.CreateObject({
            id: '_custom_report',
            company: '<?= $_GET["v"] ?>',
            report_id: '<?= $rptid ?>'
        });
        $('#divsortable .sortable-list').sortable();
        _custom_report.bindreportevent();
    });
</script>