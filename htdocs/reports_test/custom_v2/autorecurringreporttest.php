<?php
$page = "clients";
$option = "reports";
$selected = 'MetricsReport';

require ("../../header.php");
//$core = new Core_Api_CusReportClass2();
//$TimezoneListstandard = array("-10" => 10, "-9" => 8, "-8" => 7, "-7" => 6, "-6" => 5, "-5" => 4);
//$result = $core->getTimeForTimezoneStandardOffset("3:00 AM","-6");
//print_r("corn result<pre>");
//print_r($result);
//print_r("</pre>");die;

function getT2options($selValue)
{
    $timepart3 = 0;
    $timepart2 = "";
    $timepart1 = "";
    $Time = "";
    $Time .= "";
    for ($i = 0; $i < 24; $i++)
    {
        for ($j = 1; $j < 5; $j++)
        {
            if ($j == 1)
                $timepart2 = "00";
            else
                $timepart2 = ($j - 1) * 15;

            if ($i < 12 || $i == 24)
                $timepart3 = "AM";
            else
                $timepart3 = "PM";

            if ($i == 0)
                $timepart1 = "12";
            else if ($i < 13 && $i > 0)
                $timepart1 = $i;
            else
                $timepart1 = $i - 12;

            $timestamp = $timepart1 . ":" . $timepart2 . " " . $timepart3;

            $selected = '';
            if ($selValue == $timestamp)
            {
                $selected = "selected='selected'";
            }
            $Time .= "<option value='" . $timestamp . "' $selected >" . $timestamp . "</option>";
        }
    }
    return $Time;
}
?>

<link rel="stylesheet" href="../../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript" src="../../library/jquery/jquery.bt.min.js"></script>
<script>
    jQuery(window).ready(function() {
        $('.showCal').calendar({dateFormat: 'MDY/'});
//        $("#cmdrunauto").bind("click").unbind("click", runautomail());
    });

    function runautomail()
    {
        var date = $("#txtdate").val();
        var time = $("#cbxtime option:selected").val();
        var _url = "/widgets/dashboard/custom-report/run-automail/";
        $.ajax({
            type: "POST",
            url: _url,
            dataType: 'html',
            cache: false,
            context: this,
            data: {'date': date, 'time': time},
            success: function(data, status, xhr) {
                $("#divresult").html(data);
            }
        });
    }
</script>

<table cellspacing="5" cellpadding="5" width="100%">
    <tr>
        <td style="padding-left:50px;width:100px">
            Date
        </td>
        <td>
            <input id="txtdate" type='text' value='' style='width:80px' class='showCal'>
        </td>
    </tr>
    <tr>
        <td style="padding-left:50px;width:100px">
            Time
        </td>
        <td>
            <select id="cbxtime">
                <?= getT2options(); ?>
            </select>
        </td>
    </tr>
    <tr>
        <td style="padding-left:50px;width:100px">
        </td>
        <td>
            <input id="cmdrunauto" type="button" class="link_button link_button_popup" value="run" onclick="runautomail();">
        </td>
    </tr>
</table>
<div id="divresult">

</div>
<?php require ("../../footer.php"); ?>