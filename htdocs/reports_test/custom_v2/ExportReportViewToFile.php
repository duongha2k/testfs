<?php
ob_start();
require_once(dirname(__FILE__) . '/../../../includes/modules/common.init.php');
$rptid = $_REQUEST["rptid"];
//$Company_ID = $_REQUEST["v"];
$templateName = $_REQUEST["txtTemplateName"];
$Ext = strtolower($_REQUEST["Ext"]);

$filename = preg_replace("/[^a-zA-Z0-9]/", "", $templateName);
$datestr = date("Ymd");
$Core_Report = new Core_Api_CusReportClass();

$filename = $filename . '-' . $datestr . '.' . $Ext;
//$filenameattemail = $filename . '.' . $Ext;
//--- filters
//14100
$sorts = array();
$attachmail = empty($_REQUEST["attachmail"]) ? 0 : $_REQUEST["attachmail"];
if (!empty($sorts))
{
    for ($i = 1; $i <= 3; $i++)
    {
        if (!empty($_REQUEST['sort_' . $i]))
        {
            $sorts['sort_' . $i] = $_REQUEST['sort_' . $i];
            if (!empty($_REQUEST['sort_dir_' . $i]))
            {
                $sorts['sort_dir_' . $i] = $_REQUEST['sort_dir_' . $i];
            } else
            {
                $sorts['sort_dir_' . $i] = 'asc';
            }
        }
    }
} else
{
    $sorts = $Core_Report->getSortInfo2($rptid);
    unset($sorts["id"]);
}

$filterArray = $_REQUEST["filterArray"];
$FilterList = $Core_Report->getFilterList2($rptid);
if (empty($filterArray))
{
    foreach ($FilterList as $key => $val)
    {
        $filterArray["$key"] = $val;
    }
} else
{
    if (!empty($FilterList["Project_Name"]))
    {
        $filterArray["Project_Name"] = $FilterList["Project_Name"];
    }
    if (!empty($FilterList["Status"]))
    {
        $filterArray["Status"] = $FilterList["Status"];
    }
}

$BodyList = $Core_Report->runReport2($rptid, 0, 0, false, null, $filterArray);
$FieldList = $Core_Report->getReportFieldList2($rptid);

if ($attachmail === 0)
{
    switch ($Ext)
    {
        case "csv":
            // CREATE HEADER AND OPEN CSV FILE
            header("Content-type: text/csv");
            header("Content-disposition: attachment; filename=$filename");
            $tmpcsv = realpath(APPLICATION_PATH . '/../../htdocs/clients/downloadtmp') . "/tmpcsv";

            if (!is_dir($tmpcsv))
            {
                mkdir($tmpcsv);
            }

            $fp = fopen($tmpcsv . "/$filename", 'w');

            //csv columns headers
            $header = array();
            foreach ($FieldList as $field)
            {
                array_push($header, $field['field_title']);
            }
            fputcsv($fp, $header);

            // PARSE QUERY RESULTS INTO FIELDS
            foreach ($BodyList as $item)
            {
                $dataRow = array();
                foreach ($FieldList as $field)
                {
                    $itemString = empty($item[$field['fieldname']]) ? "" : trim($item[$field['fieldname']]);
                    array_push($dataRow, $itemString);
                }
                fputcsv($fp, $dataRow);
            }

            fclose($fp);
            echo file_get_contents($tmpcsv . "/$filename");
            break;
        case "pdf":
            // define Font-Path
            $specialcol = array("Deactivated_Reason", "WO_ID", "Headline", "Project_Name", "PO", "SpecialInstructions", "Ship_Contact_Info", "Requirements", "Update_Reason", "Description", "_Comments", "ClientComments", "HDPOC", "MissingComments", "NotifyNotes", "TechComments", "Unexpected_Desc", "Add_Pay_Reason", "FS_InternalComments", "OutOfScope_Reason", "Penalty_Reason", "PricingCalculationText", "bid_comments", "BU1Info");
            define('SETAPDF_FORMFILLER_FONT_PATH', 'FormFiller/font/');

            // require API
            require_once('../../library/pdf/class.ezpdf.php');

            $pdf = & new Cezpdf();
            $pdf->selectFont('../../library/pdf/fonts/Helvetica.afm');
            //header
            $header = array();
            foreach ($FieldList as $field)
            {
//            array_push($header, $field['field_title'] );
                if ($field['fieldname'] == "CallClosed")
                {
                    $header[$field['fieldname']] = "Marked Complete";
                } else
                {
                    $header[$field['fieldname']] = $field['field_title'];
                }
            }

            //body
            $body = array();
            $i = 0;
            foreach ($BodyList as $item)
            {
                $dataRow = "";
                foreach ($FieldList as $field)
                {
                    if (in_array($field['fieldname'], $specialcol))
                    {
                        $body[$i][$field['fieldname']] = htmlspecialchars(preg_replace("/[^a-zA-Z0-9()\s_]/", "", (empty($item[$field['fieldname']]) ? "" : $item[$field['fieldname']])), ENT_QUOTES);
                    } else
                    {
                        $body[$i][$field['fieldname']] = htmlspecialchars((empty($item[$field['fieldname']]) ? "" : $item[$field['fieldname']]), ENT_QUOTES);
                    }
                }
                $i++;
            }

            $pdf->ezTable($body
                    , $header
                    , $filename
                    , array('showHeadings' => 1
                , 'shaded' => 1
                , 'showLines' => 1
                , 'width' => 550
                    )
            );
            header("Cache-Control: cache, must-revalidate");
            header("Pragma: public");
            header("Content-Type: application/force-download");
            header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
            echo $pdf->output();
            break;
        case "xml":
            header("Content-type: text/xml");
            header("Content-disposition: attachment; filename=$filename");
            $body = "<records>";

            foreach ($BodyList as $item)
            {
                $dataRow = "<record>";
                foreach ($FieldList as $field)
                {
                    $dataRow.="<" . $field['fieldname'] . ">";
                    $dataRow.=htmlspecialchars($item[$field['fieldname']], ENT_QUOTES);
                    $dataRow.="</" . $field['fieldname'] . ">";
                }
                $dataRow .= "</record>";
                $body.=$dataRow;
            }
            $body .= "</records>";
            echo($body);
            break;
        case "printter":
            ?>
            <table cellpadding = "0" cellspacing = "0" border = "1" width = "100%" >
                <!--Header-->
                <tr>
                    <?php
                    foreach ($FieldList as $field)
                    {
                        ?>
                        <td><?= $field['field_title'] ?></td>
                        <?php
                    }
                    ?>
                </tr>
                <!--Body-->

                <?php
                foreach ($BodyList as $item)
                {
                    ?>
                    <tr>
                        <?php
                        foreach ($FieldList as $field)
                        {
                            ?>
                            <td><?= !empty($item[$field['fieldname']]) ? $item[$field['fieldname']] : "&nbsp;" ?></td>
                            <?php
                        }
                        ?>
                    </tr>
                    <?php
                }
                ?>

            </table>
            <?php
            break;
    }
} else
{
    echo("There were no results for the selected filters.");
}
?>
