<?php

require_once(dirname(__FILE__) . '/../../../includes/modules/common.init.php');
$to = $_REQUEST['to'];
$fromName = $_REQUEST['fromName'];
$fromMail = $_REQUEST['fromMail'];
$subject = $_REQUEST['subject'];
$reportname = preg_replace("/[^a-zA-Z0-9]/", "_", $_REQUEST['reportname']);
$message = nl2br($_REQUEST['message']);
$data = $_REQUEST['data'];

$mail = new Core_Mail();
$mail->setBodyText($message);
$mail->setFromEmail($fromMail);
$mail->setFromName($fromName);
$mail->setToEmail($to);
$mail->setToName($to);
$mail->setSubject($subject);
$mail->setreportmail(true);
if (!empty($data))
{
    if ($data === "There were no results for the selected filters." || empty($data))
    {
        $mail->setemptyattach(true);
    } else
    {
        $att = new Zend_Mime_Part($data);
        $att->type = 'text/csv';
        $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
        $att->encoding = Zend_Mime::ENCODING_BASE64;
        $att->filename = $reportname . '.csv';
        $mail->addAttachments($att);
    }
}

$mail->send(true); // forces HTML
?>