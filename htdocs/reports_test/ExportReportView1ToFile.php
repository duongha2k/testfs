<?php
ob_start();
require ("../headerStartSession.php");
$rptid = $_REQUEST["rptid"];
//$Company_ID = $_REQUEST["v"];
$templateName = $_REQUEST["txtTemplateName"];
$Ext = strtolower($_REQUEST["Ext"]);

$filename = preg_replace("/[\s_]/", "", $templateName);
$datestr = date("Ymd");
$Core_Report = new Core_Api_CusReportClass();

$filename = $filename . '-' . $datestr . '.' . $Ext;
//--- filters
$filters = array();
$objparams = $_REQUEST;

foreach ($objparams as $k => $v)
{
    if (strpos($k, 'Filter') !== false)
    {
        $part = explode('__', $k);
        $filterID = $part[0];
        $field = $part[1];
        if (!isset($filters[$filterID]))
        {
            $filters[$filterID] = array();
        }
        $filters[$filterID][$field] = $v;
    }
}

//--- sorts
$sorts = array();
if (!empty($objparams['Sort1__fieldcode']))
{
    $sorts['sort_1'] = $objparams['Sort1__fieldcode'];
    if (!empty($objparams['Sort1__sortdir']))
    {
        $sorts['sort_dir_1'] = $objparams['Sort1__sortdir'];
    } else
    {
        $sorts['sort_dir_1'] = 'asc';
    }
}

if (!empty($objparams['Sort2__fieldcode']))
{
    $sorts['sort_2'] = $objparams['Sort2__fieldcode'];
    if (!empty($objparams['Sort2__sortdir']))
    {
        $sorts['sort_dir_2'] = $objparams['Sort2__sortdir'];
    } else
    {
        $sorts['sort_dir_2'] = 'asc';
    }
}

if (!empty($objparams['Sort3__fieldcode']))
{
    $sorts['sort_3'] = $objparams['Sort3__fieldcode'];
    if (!empty($objparams['Sort3__sortdir']))
    {
        $sorts['sort_dir_3'] = $objparams['Sort3__sortdir'];
    } else
    {
        $sorts['sort_dir_3'] = 'asc';
    }
}

//--- groups
$groups = array();
foreach ($objparams as $k => $v)
{
    if (strpos($k, 'chkgro') !== false)
    {
        $pos1 = strpos($k, '_');
        $fieldName = substr($k, $pos1 + 1);
        $groups[] = $fieldName;
    }
}

//--- counts
$counts = array();
foreach ($objparams as $k => $v)
{
    if (strpos($k, 'chkcou') !== false)
    {
        $pos1 = strpos($k, '_');
        $fieldName = substr($k, $pos1 + 1);
        $counts[] = $fieldName;
    }
}

//--- sums
$sums = array();
foreach ($objparams as $k => $v)
{
    if (strpos($k, 'chksum') !== false)
    {
        $pos1 = strpos($k, '_');
        $fieldName = substr($k, $pos1 + 1);
        $sums[] = $fieldName;
    }
}

$FieldList = $Core_Report->getReportFieldList_forDisplay($rptid);
$BodyList = $Core_Report->runReport($rptid, $filters, 0, 0, false, null, $groups, $counts, $sums);

switch ($Ext)
{
    case "csv":
        // CREATE HEADER AND OPEN CSV FILE
        header("Content-type: text/csv");
        header("Content-disposition: attachment; filename=$filename");
        $fp = fopen("/tmp/$filename", 'w');

        //csv columns headers
        $header = "";
        for ($i = 0; $i < count($FieldList); $i++)
        {
            $header.= $FieldList[$i]['fieldname'] . ",";
        }
        if (strlen($header) > 0)
        {
            $header = substr($header, 0, strlen($header) - 1);
        }

        $csv_columns = explode(",", $header);
        fputcsv($fp, $csv_columns);

        // PARSE QUERY RESULTS INTO FIELDS
        $body = "";
        for ($i = 0; $i < count($BodyList); $i++)
        {
            $row = $BodyList[$i];
            $dataRow = "";
            for ($j = 0; $j < count($FieldList); $j++)
            {
                $dataRow.= $row[$FieldList[$j]['fieldname']] . ",";
            }
            if (strlen($dataRow) > 0)
            {
                $dataRow = substr($dataRow, 0, strlen($dataRow) - 1);
            }
            $formattedFields = explode(",", $dataRow);
            fputcsv($fp, $formattedFields);
        }
//        
        fclose($fp);

        echo file_get_contents("/tmp/$filename");
        break;
    case "pdf":
        // define Font-Path
        define('SETAPDF_FORMFILLER_FONT_PATH', 'FormFiller/font/');

        // require API
        require_once('../library/pdf/class.ezpdf.php');

        $pdf = & new Cezpdf();
        $pdf->selectFont('../library/pdf/fonts/Helvetica.afm');

        //header
        $header = array();
        for ($i = 0; $i < count($FieldList); $i++)
        {
            array_push($header, "\"" . $FieldList[$i]['fieldname'] . "\"=>array('width' => 15px)");
        }

        //body
        $body = array();
        for ($i = 0; $i < count($BodyList); $i++)
        {
            $row = $BodyList[$i];
            //$dataRow = "";
            for ($j = 0; $j < count($FieldList); $j++)
            {
                //$dataRow.="\"" . $row[$FieldList[$j]['fieldname']] . "\",";
                $body[$i][$FieldList[$j]['fieldname']] = $row[$FieldList[$j]['fieldname']];
            }
        }

        $pdf->ezTable($body
                , null
                , $templateName
                , array('showHeadings' => 1
            , 'shaded' => 1
            , 'showLines' => 1
            , 'width' => 550
            //,'xPos'=>'left'
            , 'cols' => $header
                )
        );
        header("Cache-Control: cache, must-revalidate");
        header("Pragma: public");
        header("Content-Type: application/force-download");
        header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
        echo $pdf->output();

        break;
    case "xml":
        header("Content-type: text/xml");
        header("Content-disposition: attachment; filename=$filename");
        $body = "<records>";
        for ($i = 0; $i < count($BodyList); $i++)
        {
            $row = $BodyList[$i];
            $dataRow = "<record>";
            for ($j = 0; $j < count($FieldList); $j++)
            {
                $dataRow.="<" . $FieldList[$j]['fieldname'] . ">";
                $dataRow.=$row[$FieldList[$j]['fieldname']];
                $dataRow.="</" . $FieldList[$j]['fieldname'] . ">";
            }
            $dataRow .= "</record>";
            $body.=$dataRow;
        }
        $body .= "</records>";
        echo($body);
        break;
    case "printter":
        ?>
        <table cellpadding = "0" cellspacing = "0" border = "1" width = "100%" >
            <!--Header-->
            <tr>
                <?php
                for ($i = 0; $i < count($FieldList); $i++)
                {
                    ?>
                    <td><?= $FieldList[$i]['fieldname'] ?></td>
                    <?php
                }
                ?>
            </tr>
            <!--Body-->

            <?php
            for ($i = 0; $i < count($BodyList); $i++)
            {
                ?>
                <tr>
                    <?php
                    $row = $BodyList[$i];
                    for ($j = 0; $j < count($FieldList); $j++)
                    {
                        ?>
                        <td><?= !empty($row[$FieldList[$j]['fieldname']]) ? $row[$FieldList[$j]['fieldname']] : "&nbsp;" ?></td>
                        <?php
                    }
                    ?>
                </tr>
                <?php
            }
            ?>

        </table>
        <?php
        break;
}
?>
