<?
$page = "clients";
$option = "reports";
$selected = 'MetricsReport';
$UserType = (!empty($_SESSION['UserType']) ) ? $_SESSION['UserType'] : NULL;
$isFLSManager = (strtolower($Company_ID) == 'fls' && (strtolower($UserType) === 'install desk' || strtolower($UserType) === 'manager'));

if ($isFLSManager)
{
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
} else
{
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
}
require ("../header.php");
require ("../navBar.php");
require ("../headerStartSession.php");

$rptid = isset($_REQUEST['rptid']) ? $_REQUEST['rptid'] : "";
$v = isset($_REQUEST['v']) ? $_REQUEST['v'] : "";

$CusReportClass = new Core_Api_CusReportClass();
$ReportInfo = $CusReportClass->getReportInfo($rptid);
$txtTemplateName = $ReportInfo['rpt_name'];
$FilterList = $CusReportClass->getFilterList($rptid);
$FieldList = $CusReportClass->getReportFieldList_forDisplay($rptid);
$resultColListForGroup = $CusReportClass->getReportFieldList_forGroup_1($rptid);
$rptColList = $CusReportClass->getReportFieldList($rptid);

function dateFields($filter)
{
    //$filter['fieldcode']
    $size = 10;
    $filterID = $filter['id'];

    $nameGreater = "Filter$filterID" . "__datetime_range_from";
    $nameLess = "Filter$filterID" . "__datetime_range_to";
    $readOnly = $filter['filter_at'] == 1 ? "readonly" : "";

    $isdate = $filter['filter_at'] == 1 ? "" : "showCal";
    $range_from = "";
    if (!empty($filter['datetime_range_from']))
    {
        $range_from = date("Y-m-d", strtotime($filter['datetime_range_from']));
    }
    $range_to = "";
    if (!empty($filter['datetime_range_to']))
    {
        $range_to = date("Y-m-d", strtotime($filter['datetime_range_to']));
    }

    return "From <input name='$nameGreater' id='$nameGreater' style='width:80px;' class='$isdate onchange' $readOnly value='$range_from'/> 
        &nbsp;&nbsp;To&nbsp;&nbsp;<input name='$nameLess' id='$nameLess'  style='width:80px;' class='$isdate onchange' $readOnly value='$range_to'/>
        <input type='hidden' name='Filter$filterID" . "__datatype' value='" . $filter['datatype'] . "'>
        <input type='hidden' name='Filter$filterID" . "__fieldcode' value='" . $filter['fieldcode'] . "'>
        ";
}

function numFields($filter)
{
    $filterID = $filter['id'];
    $range_from = $filter['numeric_range_from'];
    $range_to = $filter['numeric_range_to'];
    if ($filter['datatype'] == 'int')
    {
        if (isset($range_from))
        {
            $range_from = round($filter['numeric_range_from'], 0);
        }
        if (isset($range_to))
        {
            $range_to = round($filter['numeric_range_to'], 0);
        }
    }
    if ($filter['filter_at'] == 2) //filter at run time
    {
        $range_from = "";
        $range_to = "";
    }

    $nameGreater = "Filter$filterID" . "__numeric_range_from";
    $nameLess = "Filter$filterID" . "__numeric_range_to";
    $readOnly = $filter['filter_at'] == 1 ? "readonly" : "";
    return "From <input name='$nameGreater' id='$nameGreater'  style='width:80px;' class='numtype onchange' $readOnly value='$range_from'/> 
        &nbsp;&nbsp;To&nbsp;&nbsp;<input name='$nameLess' id='$nameLess'  style='width:80px;' class='numtype onchange' $readOnly value='$range_to'/>
        <input type='hidden' name='Filter$filterID" . "__datatype' value='" . $filter['datatype'] . "'>
        <input type='hidden' name='Filter$filterID" . "__fieldcode' value='" . $filter['fieldcode'] . "'>
        ";
}

function boolFields($filter)
{
    $filterID = $filter['id'];
    $bool_value = $filter['bool_value']; //bool_value t:true, f:false, a:any
    $ischecktrue = "";
    $ischeckfalse = "";
    $ischeckany = "";
    if ($filter['filter_at'] == 2) //filter at run time
    {
        $bool_value = "a";
    }
    $name = "Filter$filterID" . "__bool_value";
    $readOnly = $filter['filter_at'] == 1 ? "disabled" : "";

    if ($bool_value == "t")
    {
        $ischecktrue = "checked";
    }
    if ($bool_value == "f")
    {
        $ischeckfalse = "checked";
    }
    if ($bool_value == "a")
    {
        $ischeckany = "checked";
    }

    $strReturn = "<input class='onchange' type='radio' name='$name' id='true$name' value='t' $readOnly $ischecktrue/><lable for='true$name'>True</lable>
            <input class='onchange' type='radio' name='$name' id='false$name' value='f' $readOnly $ischeckfalse/><lable for='false$name'>False</lable>
            <input class='onchange' type='radio' name='$name' id='any$name' value='a' $readOnly $ischeckany/><lable for='any$name'>Any</lable>
    <input type='hidden' name='Filter$filterID" . "__datatype' value='" . $filter['datatype'] . "'>
    <input type='hidden' name='Filter$filterID" . "__fieldcode' value='" . $filter['fieldcode'] . "'>
            ";
    if ($filter['filter_at'] == 1)
    {
        $strReturn .="<input type='hidden' name='$name' value='$bool_value'>";
    }
    return $strReturn;
}

function stringFields($filter, $table)
{
    $filterID = $filter['id'];
    $string_value = $filter['string_value'];
    $string_matching = $filter['string_matching'];

    $ExactMatchSel = $string_matching == "ExactMatch" ? "selected='selected'" : "";
    $BeginsWithSel = $string_matching == "BeginsWith" ? "selected='selected'" : "";
    $EndsWithSel = $string_matching == "EndsWith" ? "selected='selected'" : "";
    $ContainsSel = $string_matching == "Contains" ? "selected='selected'" : "";
    $IsNot = $string_matching == "IsNot" ? "selected='selected'" : "";
    $SelectMultiple = $string_matching == "SelectMultiple" ? "selected='selected'" : "";
    $editmulti = $string_matching == "SelectMultiple" ? "display:;" : "display:none;";
    $noteditmulti = $string_matching == "SelectMultiple" ? "display:none;" : "display:;";

    $readOnly = $filter['filter_at'] == 1 ? "readonly" : "";
    $disabled = $filter['filter_at'] == 1 ? "disabled" : "";
    $strReturn = "
            <select class='cbxmatching' id='Filter$filterID" . "__string_matching' name='Filter$filterID" . "__string_matching' $disabled style='width:103px'>
                <option value='ExactMatch' $ExactMatchSel>Exact Match</option>
                <option value='BeginsWith' $BeginsWithSel>Begins With</option>
                <option value='EndsWith' $EndsWithSel>Ends With</option>
                <option value='Contains' $ContainsSel>Contains</option>
                <option value='IsNot' $IsNot>Is Not</option>
                <option value='SelectMultiple' $SelectMultiple>Select Multiple</option>
            </select>&nbsp;&nbsp;
            <a style='$editmulti' class='editSelectMultiple' href='javascript:;'  rpttable='" . $table . "' fieldcode='" . $filter['fieldcode'] . "' fieldid='" . $filter['id'] . "' v='" . $_GET['v'] . "' >edit</a>
            <input class='stringvalue' id='Filter$filterID" . "__string_value' name='Filter$filterID" . "__string_value'  style='width:114px;$noteditmulti' value='$string_value' $readOnly/>
            <input type='hidden' name='Filter$filterID" . "__datatype' value='" . $filter['datatype'] . "'>
            <input type='hidden' name='Filter$filterID" . "__fieldcode' value='" . $filter['fieldcode'] . "'>
            ";
    if(!empty($filter['SelectMultiple']))
    {
        foreach($filter['SelectMultiple'] as $selecteditem)
        {
            
            $strReturn .= "<input type='hidden' name='Filter$filterID" . "__SelectMultiple[]' value='" . $selecteditem . "'>";
        }
        
    }
    if ($filter['filter_at'] == 1)
    {
        $strReturn .= "<input type='hidden' name='Filter$filterID" . "__string_matching' value='$string_matching'>";
    }
    return $strReturn;
}

function loadsort($FieldList, $selectValue = '')
{
    $sort = "";
    if (!(empty($FieldList)))
    {
        foreach ($FieldList as $Field)
        {
            $fieldTitle = $Field['field_title'];
            $selected = "";
            if ($Field['field_code'] == $selectValue)
            {
                $selected = "selected='selected'";
            }
            $sort.="<option  value='" . $Field['field_code'] . "' $selected >" . $fieldTitle . "</option>";
        }
    }
    return $sort;
}
?>
<link rel="stylesheet" href="../../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript" src="custom/js/custom_report.js"></script>
<script type="text/javascript" src="custom/js/custom_report_addfilter.js"></script>
<style>
    div#loader {
        background: url("/widgets/images/loader.gif") no-repeat scroll center center #FFFFFF;
        font: 16px Tahoma,Geneva,sans-serif;
        height: 100px;
        padding: 10px;
        text-align: center;
        width: 100px;
    }
</style>

<div style="padding-left:5px;">
    <a href="custom/reportlist.php?v=<?= $v ?>">Back to List</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="custom/custom_report.php?report_id=<?= $rptid ?>&v=<?= $v ?>">Edit Report</a>
</div>
<div style="padding:5px;text-align: center;">
    <input type="text" id="txtTemplateName" readonly name="txtTemplateName" value="<?= $txtTemplateName ?>" style="display:none;"/>
    <b><?= $txtTemplateName ?></b>
</div>
<form id='frmfilter'>
    <input id="txtTemplateName" type='hidden' name='txtTemplateName' value='<?= $txtTemplateName ?>'>
    <input id="rptid" type='hidden' name='rptid' value='<?= $rptid ?>'>
    <table width="100%">
        <col width="500px">
        <col width="280px">
        <col width="*">
        <tr>
            <td style="vertical-align: top;">
                <div style="padding:5px;">
                    <label><b>Filters</b></label> 
                    <?
                    if (!empty($FilterList))
                    {
                        ?>
                        <span>(<a id="resetfilter" href="javascript:;">reset</a>)</span>
                        <span>(<a id="clearfilter" href="javascript:;">clear</a>)</span>
                        <?
                    }
                    ?><img id="group1" height="13px" src="/widgets/images/arrows-up5.png" groupid="1" title="" alt="">
                </div>
                <div style='margin-left:20px;' id="showhide1">
                    <table cellspacing='0' cellpadding='0' id='tblFilter'>
                        <col width='250px'>
                        <col width='270px'>
                        <?
                        $tablecontrol = "";
                        $controlstr = "";
                        //echo("<br/>FilterList:<pre>");print_r($FilterList);echo("</pre>");
                        if (!empty($FilterList))
                        {
                            $tablecontrol = "";
                            foreach ($FilterList as $filter)
                            {
                                $controlstr .= "<tr filterid='" . $filter['id'] . "' datatype='" . $filter['datatype'] . "' fieldcode='" . $filter['fieldcode'] . "'>
                                    <td style='padding-right:20px;' >" . $filter['field_title'] . "</td>";
                                switch ($filter['datatype'])
                                {
                                    case "datetime":
                                        $controlstr .= "<td>" . dateFields($filter) . "</td>";
                                        break;
                                    case "decimal(10,2)":
                                    case "decimal(10,4)":
                                    case "float":
                                    case "int":
                                    case "money":
                                        $controlstr .= "<td>" . numFields($filter) . "</td>";
                                        break;
                                    case "string":
                                        $controlstr .= "<td>" . stringFields($filter, $ReportInfo["table"]) . "</td>";
                                        break;
                                    case "tinyint":
                                        $controlstr .= "<td>" . boolFields($filter) . "</td>";
                                        break;
                                }
                                $controlstr .= "</tr>";
                            }
                            echo($tablecontrol . $controlstr . "");
                        } else
                        {
                            ?>
                            <div style="padding:25px;">No Filter for this report.</div>
                            <?
                        }
                        ?>
                    </table>
                </div>
            </td>
            <td style="vertical-align: top;">
                <div style="padding:5px;">
                    <label><b>Sort</b></label> 
                    <?
                    if (!empty($FilterList))
                    {
                        ?>
                        <span>(<a id="resetsort" href="javascript:;">reset</a>)</span>
                        <span>(<a id="clearsort" href="javascript:;">clear</a>)</span>
                        <?
                    }
                    ?><img id="group2" height="13px" src="/widgets/images/arrows-up5.png" groupid="2" title="" alt="">
                </div>
                <div id="showhide2" style='margin-left:20px;'>
                    <table id='tblSort'>
                        <tr>
                            <td>
                                <select id="sort1" curid="1" anosort1="2"  anosort2="3" style="width:150px;">
                                    <option value="">None</option>
                                    <?= loadsort($rptColList, $ReportInfo['sort_1']); ?>
                                </select>
                                <input type="hidden" id="hidsort1" value="">
                            </td>
                            <td>
                                <select  class="onchange" id="sortdir1" style="width:100px;">
                                    <option value="asc" <?= $ReportInfo['sort_dir_1'] == 'asc' ? "selected='selected'" : "" ?> >Ascending</option>
                                    <option value="desc" <?= $ReportInfo['sort_dir_1'] == 'desc' ? "selected='selected'" : "" ?> >Descending</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <select id="sort2" curid="2" anosort1="1"  anosort2="3" style="width:150px;">
                                    <option value="">None</option>
                                    <?= loadsort($rptColList, $ReportInfo['sort_2']); ?>
                                </select>
                                <input type="hidden" id="hidsort2" value="">
                            </td>
                            <td>
                                <select class="onchange" id="sortdir2" style="width:100px;">
                                    <option value="asc">Ascending</option>
                                    <option value="desc">Descending</option>
                                </select>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <select id="sort3" curid="3" anosort1="1"  anosort2="2" style="width:150px;">
                                    <option value="">None</option>
                                    <?= loadsort($rptColList, $ReportInfo['sort_3']); ?>
                                </select>
                                <input type="hidden" id="hidsort3" value="">
                            </td>
                            <td>
                                <select class="onchange" id="sortdir3" style="width:100px;">
                                    <option value="asc">Ascending</option>
                                    <option value="desc">Descending</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
            <td style="vertical-align: top;">
                <div style="padding:5px;">
                    <input id="rptid" type='hidden' name='rptid' value='<?= $rptid ?>'>
                    <label><b>Format Report Data</b></label> 
                    <?
                    if (!empty($FilterList))
                    {
                        ?>
                        <span>(<a id="resetformat" href="javascript:;">reset</a>)</span>
                        <span>(<a id="clearformat" href="javascript:;">clear</a>)</span>
                        <?
                    }
                    ?><img id="group3" height="13px" src="/widgets/images/arrows-down5.png" groupid="3" title="" alt="">
                </div>
                <div id="showhide3" style="display:none;margin-left:20px;" >
                    <table id="groupTable" cellpadding="0" cellspacing="0">
                        <colgroup>
                            <col width="*">
                            <col width="55px">
                            <col width="55px">
                            <col width="80px">
                        </colgroup>
                        <tr>
                            <th></th>
                            <th style="text-align:center;">Sum</th>
                            <th style="text-align:center;">Count</th>
                            <th style="text-align:center;">Group By</th>
                        </tr>
                        <tbody>
                            <?php
                            $isSumDisable = "";
                            foreach ($resultColListForGroup as $item)
                            {
                                if ($item['datatype'] == "boolean" || $item['datatype'] == "datetime" || $item['datatype'] == "string")
                                {
                                    $isSumDisable = "disabled";
                                }
                                else
                                    $isSumDisable = "";
                                $fieldCode = $item['field_code'];
                                ?>
                                <tr style="text-align:center;">
                                    <td style="padding-right:20px;text-align:left;" fieldname="<?= $fieldCode ?>"><?= $item['field_title'] ?></td>
                                    <td><input class="onchange" id="chksum_<?= $fieldCode ?>" name="chksum_<?= $fieldCode ?>" type="checkbox"<?= $item["issum"] == 1 ? "checked" : ""; ?> <?= $isSumDisable ?> ></td>
                                    <td><input class="onchange" id="chkcou_<?= $fieldCode ?>" name="chkcou_<?= $fieldCode ?>" type="checkbox"<?= $item["iscount"] == 1 ? "checked" : ""; ?>></td>
                                    <td><input class="onchange" id="chkgro_<?= $fieldCode ?>" name="chkgro_<?= $fieldCode ?>" type="checkbox"<?= $item["isgroupby"] == 1 ? "checked" : ""; ?>></td>
                                </tr>
                                <?
                            }
                            ?>
                        <tbody>     
                    </table>
                </div>
            </td>
        </tr>
    </table>
</form>
<br/>
<div style="padding: 5px;">
    <input type="button" value="Generate Report" class="link_button middle2_button" id="cmdReports" name="cmdReports" page="1">
    <input type="button" value="Save to Template" class="link_button link_button_disabled" id="cmdsavetemplatefilter" style="<?= empty($FilterList) ? "display:none;" : "" ?>;color:#696969;" disable>
    <div style="float:right;padding-right:200px;">
        <input type="button" value="Download" class="link_button download_button" id="cmddownload">
        <input type="button" value="Print" class="link_button link_button_popup" id="cmdPrint" name="cmdPrint" filetype="Printter">
    </div>
</div>
<div id="reportcontent">
</div>

<script>
                            var _custom_report = null;
                            $(document).ready(function()
                            {
                                var _custom_report = custom_report.CreateObject({
                                    id: '_custom_report',
                                    company: '<?= $_GET["v"] ?>',
                                    report_id: '<?= $rptid ?>'
                                });

                                _custom_report.bindreportevent();

                                $('.showCal').calendar({dateFormat: 'YMD-'});
                            });
                            

</script>