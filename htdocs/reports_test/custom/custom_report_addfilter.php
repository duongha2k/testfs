<?php
require_once('../../../includes/modules/web.init.php');
require_once('../../../includes/modules/common.init.php');
require_once('../../../includes/modules/tables.php');
$reportid = $_REQUEST["rptid"];
$fieldCode = isset($_REQUEST["fieldcode"]) ? $_REQUEST["fieldcode"] : "";
$fieldid = isset($_REQUEST["fieldid"]) ? $_REQUEST["fieldid"] : "";

//$fieldName = isset($_REQUEST["fieldname"]) ? $_REQUEST["fieldname"] : "";
//$fieldType = isset($_REQUEST["fieldtype"]) ? $_REQUEST["fieldtype"] : "";
$filtercode = isset($_REQUEST["filtercode"]) ? $_REQUEST["filtercode"] : "";
$reporttable = isset($_REQUEST["reporttable"]) ? $_REQUEST["reporttable"] : "";
$reportname = isset($_REQUEST["reportname"]) ? $_REQUEST["reportname"] : "";

$FilterAt = isset($_REQUEST["FilterAt"]) ? $_REQUEST["FilterAt"] : "";
$Filterby = isset($_REQUEST["Filterby"]) ? $_REQUEST["Filterby"] : "";
$fromval = isset($_REQUEST["fromval"]) ? $_REQUEST["fromval"] : "";
$toval = isset($_REQUEST["toval"]) ? $_REQUEST["toval"] : "";
$fieldtype = isset($_REQUEST["fieldtype"]) ? $_REQUEST["fieldtype"] : "";
$multiselect = isset($_REQUEST["multiselect"]) ? $_REQUEST["multiselect"] : "";

foreach($multiselect["data"] as $key =>$value)
{
    $multiselectitems[] = trim($value['val']);
}
 

$existFilter = array();
$CusReportClass = new Core_Api_CusReportClass();

if ($reportid != "")
{
    if ($fieldid != "")
    {
        $Filterinfo = $CusReportClass->getFilter($reportid, $fieldid);
    } else
    {
        $Filterinfo = Array(
            "filter_at" => 3,
        );
    }

    $ReportFieldList = $CusReportClass->getListofFields_InTable($reporttable);
    $ReportInfo = $CusReportClass->getReportInfo($reportid);
    $ReportFilterList = $CusReportClass->getFilterList($reportid);

    if (!empty($ReportFilterList))
    {
        foreach ($ReportFilterList as $Filter)
        {
            if ($fieldCode != $Filter['fieldcode'])
                $existFilter[$Filter['fieldcode']] = $Filter['fieldcode'];
        }
    }
    //fieldcode
    $Page->_rptid = $reportid;
    $Page->_Company_ID = $ReportInfo["companyid"]; //BW, FS
    $templatename = $ReportInfo["rpt_name"];
} else
{ // add filter during creating report template
    $i = 0;
    $ReportFieldListtemp = $CusReportClass->getListofFields_InTable($reporttable);
    foreach ($ReportFieldListtemp as $field)
    {
        $ReportFieldList[$i] = array("field_code" => $field['field_code'], "field_title" => $field['field_title'], "datatype" => $field['datatype']);
        $i++;
    }

    if (!empty($filtercode))
    {
        foreach ($filtercode as $filteritem)
        {
            if ($fieldCode != $filteritem)
                $existFilter[$filteritem] = $filteritem;
        }
    }
    $templatename = $reportname;
    if (empty($FilterAt))
    {
        $FilterAt = 3;
    }
    $Filterinfo = Array(
        "filter_at" => $FilterAt,
        "datetime_range_from" => $fromval,
        "datetime_range_to" => $toval,
        "string_matching" => $Filterby,
        "string_value" => $fromval,
        "bool_value" => $fromval,
        "numeric_range_from" => $fromval,
        "numeric_range_to" => $toval,
        "datetime_range_int_from" => $fromval,
        "datetime_range_int_to" => $toval,
        "datetime_relative_data_ref" => $Filterby,
        "datatype" => $fieldtype,
        "SelectMultiple"=>$multiselectitems
    );
}
?>
<form>
    <?
//    print_r("Filterinfo<pre>");
//    print_r($Filterinfo);
//    print_r("</pre>");
//    print_r("existFilter<pre>");
//    print_r($filtercode);
//    print_r("</pre>");
    ?>
    <input type="hidden" id='fieldid' value="<?= $fieldid ?>"/>
    <div style="max-height: 400px; overflow: auto; padding-bottom: 8px;">
        <div id="divError" style="display:none;">
        </div>
        <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
                <td style="padding-top:5px; width:25%;">
                    Template Name
                </td>
                <td style="padding-top:5px;">      
                    <b><?= $templatename ?></b>
                </td>
            </tr>
            <tr>
                <td style="padding-top:5px;">
                    Field Name
                </td>
                <td style="padding-top:5px;">      
                    <select id="cbxFieldName" name="cbxFieldName" style="width:200px;" <?= $fieldid != "" ? "disabled" : "" ?>>
                        <?php
                        foreach ($ReportFieldList as $field)
                        {
                            if (!array_key_exists($field['field_code'], $existFilter))
                            {
                                ?>
                                <option value="<?= $field['field_code'] ?>"  vxdatatype="<?= $field['datatype'] ?>" <?= $fieldCode == $field['field_code'] ? "selected" : "" ?>><?= $field['field_title'] ?></option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td style="padding-top:5px; vertical-align: top;">
                    Filtered At
                </td>
                <td style="padding-top:5px;">
                    <div>
                        <input type="radio" name="radFilteredAt" id="radTemplate" value="1" <?= $Filterinfo['filter_at'] == '1' ? "checked" : "" ?> >
                        <label for="radTemplate">Template</label>
                    </div>
                    <div>
                        <input type="radio" name="radFilteredAt" id="radRunTimewithDefaultValue" value="3" <?= empty($Filterinfo['filter_at']) ? "checked" : $Filterinfo['filter_at'] == '3' ? "checked" : ""  ?>>
                        <label for="radRunTimewithDefaultValue">Run Time</label>
                    </div>

                </td>
            </tr>
            <tr class="trDateType">
                <td style="padding-top:5px; vertical-align: top;">
                    Relative Data Reference
                </td>
                <td style="padding-top:5px;">         
                    <select id="cbxRelativeDataReference" name="cbxRelativeDataReference" style="width:200px;" >
                        <option value="AsEntered" <?= ($Filterinfo['datetime_relative_data_ref'] == "AsEntered") ? "selected" : ""; ?>>As Entered</option>
                        <option value="RunTimeDate" <?= ($Filterinfo['datetime_relative_data_ref'] == "RunTimeDate") ? "selected" : ""; ?>>Run Time Date</option>
                        <option value="CurrentYear" <?= ($Filterinfo['datetime_relative_data_ref'] == "CurrentYear") ? "selected" : ""; ?>>Current Year</option>
                        <option value="LastYear" <?= ($Filterinfo['datetime_relative_data_ref'] == "LastYear") ? "selected" : ""; ?>>Last Year</option>
                        <option value="CurrentQuarter" <?= ($Filterinfo['datetime_relative_data_ref'] == "CurrentQuarter") ? "selected" : ""; ?>>Current Quarter</option>
                        <option value="LastQuarter" <?= ($Filterinfo['datetime_relative_data_ref'] == "LastQuarter") ? "selected" : ""; ?>>Last Quarter</option>
                    </select>
                </td>
            </tr>
            <?
            $isdisplaydaterange = "display:none;";
            $isdisplaydateAsEntered = "display:none;";
            $isdisplaydateRunTime = "display:none;";

            if (($Filterinfo['datetime_relative_data_ref'] != "AsEntered") && ($Filterinfo['datetime_relative_data_ref'] != "RunTimeDate"))
            {

                $isdisplaydaterange = "display:none;";
                $isdisplaydateAsEntered = "display:none;";
                $isdisplaydateRunTime = "display:none;";
            } else if ($Filterinfo['datetime_relative_data_ref'] == "AsEntered")
            {
                $isdisplaydaterange = "";
                $isdisplaydateAsEntered = "";
                $isdisplaydateRunTime = "display:none;";
            } else if ($Filterinfo['datetime_relative_data_ref'] == "RunTimeDate")
            {
                $isdisplaydaterange = "";
                $isdisplaydateAsEntered = "display:none;";
                $isdisplaydateRunTime = "";
            }
            ?>
            <tr id="trDateType" class="trDateType">
                <td class="DateTyperange" style="padding-top:5px; vertical-align: top; <? echo($isdisplaydaterange) ?>" >
                    Range
                </td>
                <td class="DateTyperange" style="padding-top:5px;<?= $isdisplaydaterange ?>">
                    <div id="divDateRangeRuntime" style="<?= $isdisplaydateRunTime; ?>">
                        <input type="text" id="txtFromRange" name="txtRuntimeFrom" style="width:80px;" value="<?= $Filterinfo['datetime_range_int_from'] ?>">&nbsp;&nbsp;&nbsp;
                        To&nbsp;&nbsp;&nbsp;<input type="text" id="txtToRange" name="txtRuntimeTo" style="width:80px;" value="<?= $Filterinfo['datetime_range_int_to'] ?>">
                        <span style="font-weight: bold; font-size: 11px;">example: -2 to 2</span>
                    </div>
                    <div id="divDateAsEnter" style="<?= $isdisplaydateAsEntered; ?>">
                        <input type="text" class="showCal" id="txtDateAsEnterForm" name="txtFromRange" style="width:80px;" value="<?= !empty($Filterinfo['datetime_range_from']) ? date("Y-m-d", strtotime($Filterinfo['datetime_range_from'])) : ""; ?>">&nbsp;&nbsp;&nbsp;
                        To&nbsp;&nbsp;&nbsp;<input type="text" class="showCal" id="txtDateAsEnterTo" name="txtToRange" style="width:80px;" value="<?= !empty($Filterinfo['datetime_range_to']) ? date("Y-m-d", strtotime($Filterinfo['datetime_range_to'])) : ""; ?>">
                    </div>
                </td>
            </tr>
            <tr class="trStringType">
                <td style="padding-top:5px; vertical-align: top;">
                    Matching
                </td>
                <td style="padding-top:5px;">         
                    <select id="cbxMatching" name="cbxMatching" style="width:200px;" reporttable="<?=$reporttable?>" company='<?=$_REQUEST["v"] ?>'>
                        <option value="ExactMatch" <?= ($Filterinfo['string_matching'] == "ExactMatch") ? "selected" : ""; ?>>Exact Match</option>
                        <option value="BeginsWith" <?= ($Filterinfo['string_matching'] == "BeginsWith") ? "selected" : ""; ?>>Begins With</option>
                        <option value="EndsWith" <?= ($Filterinfo['string_matching'] == "EndsWith") ? "selected" : ""; ?>>Ends With</option>
                        <option value="Contains" <?= ($Filterinfo['string_matching'] == "Contains") ? "selected" : ""; ?>>Contains</option>
                        <option value="IsNot" <?= ($Filterinfo['string_matching'] == "IsNot") ? "selected" : ""; ?>>Is Not</option>
                        <option value="SelectMultiple" <?= ($Filterinfo['string_matching'] == "SelectMultiple") ? "selected" : ""; ?>>Select Multiple</option>
                    </select>
                </td>
            </tr>
            <tr class="trStringType">
                <td style="padding-top:5px; vertical-align: top;">
                    Value
                </td>
                <td style="padding-top:5px;">         
                    <div id="notmulti" style="<?= ($Filterinfo['string_matching'] == "SelectMultiple") ? "display:none;" : "" ?>">
                        <input type="text" id="txtStringTypeValue" style="width:200px;" name="txtStringTypeValue" value="<?= $Filterinfo['string_value']; ?>">
                    </div>
                    <div id="multi" style="<?= ($Filterinfo['string_matching'] == "SelectMultiple") ? "" : "display:none;" ?>max-height:70px;overflow: auto;">
                        <?
                        if ($Filterinfo['string_matching'] == "SelectMultiple")
                        {
                            $multilist = $CusReportClass->getValueListOfField($reporttable, $fieldCode, $_REQUEST["v"] );
                            if (!empty($multilist))
                            {
                                ?>
                                <table>
                                    <?
                                    foreach ($multilist as $key => $value)
                                    {
                                        $ischecked = "";
                                        foreach($Filterinfo['SelectMultiple']  as $key1 => $value1)
                                        {
                                            if(trim($value1)==trim($value))
                                            {
                                               $ischecked="checked" ;
                                            }
                                        }
                                        ?>
                                        <tr>
                                            <td style="vertical-align: top;">
                                                <input id="chk_<?= $key ?>" val="<?= $value ?>" class="SelectMultiple" type="checkbox" name="SelectMultiple_<?= $key ?>" <?=$ischecked;?> axc=''>
                                            </td>
                                            <td style="vertical-align: top;">
                                                <?= $value ?>
                                            </td>
                                        </tr>
                                        <?
                                    }
                                    ?>
                                </table>
                                <?
                            }
                        }
                        ?>
                    </div>
                </td>
            </tr>
            <tr class="trBooleanType">
                <td style="padding-top:5px; vertical-align: top;">
                    Value
                </td>
                <td style="padding-top:5px;">         
                    <div>
                        <input type="radio" name="radBoolean" id="radBooleanTrue" value="true" <?= $Filterinfo['bool_value'] == 't' ? "checked" : "" ?> >
                        <label for="radBooleanTrue">True</label>
                    </div>
                    <div>
                        <input type="radio" name="radBoolean" id="radBooleanFalse" value="false"  <?= $Filterinfo['bool_value'] == 'f' ? "checked" : "" ?> >
                        <label for="radBooleanFalse">False</label>
                    </div>
                    <div>
                        <input type="radio" name="radBoolean" id="radBooleanAny" value="any"   <?= !empty($Filterinfo) ? ($Filterinfo['bool_value'] == 'a' ? "checked" : "") : "checked" ?> >
                        <label for="radBooleanAny">Any</label>
                    </div>
                </td>
            </tr>
            <tr class="trNumericType">
                <td style="padding-top:5px; vertical-align: top;">
                    Range
                </td>
                <td style="padding-top:5px;">         
                    <input type="text" id="txtNumericRangeFrom" name="txtNumericRangeFrom" style="width:80px;" value="<?= $Filterinfo['numeric_range_from'] ?>">&nbsp;&nbsp;&nbsp;
                    To&nbsp;&nbsp;&nbsp;<input type="text" id="txtNumericRangeTo" name="txtNumericRangeTo" style="width:80px;" value="<?= $Filterinfo['numeric_range_to'] ?>">
                </td>
            </tr>
        </table>
    </div>
    <script>
        var _custom_report_addfilter = null;
        jQuery(window).ready(function() {
            _custom_report_addfilter = custom_report_addfilter.CreateObject({
                id: '_custom_report_addfilter',
                company: '<?= $_GET["v"] ?>',
                report_id: '<?= $_REQUEST["rptid"] ?>',
                cbxFieldName: '#cbxFieldName'
            });

            $.map(['txtDateAsEnterForm', 'txtDateAsEnterTo'], function(id) {
                $('#' + id).calendar({dateFormat: 'YMD-'});
            });

            $('#cbxRelativeDataReference')
                    .unbind("change")
                    .bind("change", _custom_report_addfilter.cbxRelativeDataReference_onChange);
            if ($("#cbxFieldName").attr("disabled"))
            {
                var ValueSelect = $("#cbxFieldName option:selected").attr("vxdatatype");
                _custom_report_addfilter.VisibleGUI(ValueSelect);
            }

        })
    </script>
</form>