<script type="text/javascript" src="js/custom_report.js"></script>
<?php
require_once('../../../includes/modules/web.init.php');
require_once('../../../includes/modules/common.init.php');
require_once('../../../includes/modules/tables.php');
$reportid = $_REQUEST["rptid"];

$tableid = isset($_REQUEST["tableid"]) ? $_REQUEST["tableid"] : "";
$fieldcode = isset($_REQUEST["fieldcode"]) ? $_REQUEST["fieldcode"] : "";
$reportname = isset($_REQUEST["reportname"]) ? $_REQUEST["reportname"] : "";

$CusReportClass = new Core_Api_CusReportClass();
$existcol = array();

if ($reportid != "")
{
    $ReportInfo = $CusReportClass->getReportInfo($reportid);
    $rptColList = $CusReportClass->getReportFieldList($reportid);

    foreach ($rptColList as $col)
    {
        $existcol[$col['field_code']] = $col['field_code'];
    }
    $tableColList = $CusReportClass->getListofFields_InTable($ReportInfo["table"]);
    $templatename = $ReportInfo["rpt_name"];
} else
{
    $tableColList = $CusReportClass->getListofFields_InTable($tableid);
    foreach ($fieldcode as $field)
    {
        $existcol[$field] = $field;
    }

    $templatename = $reportname;
}
?>
<div style="max-height: 400px; overflow: auto; padding-bottom: 8px;">
    <label>Template Name</label>&nbsp;&nbsp;<label><b><?= $templatename ?></b></label>
    <table cellpadding="0" cellspacing="0" width="400px" class="">
        <col width="100x" />
        <col width="200px" />                   
        <tr>
            <td style="padding-top:0px;">
                Field Name
            </td>
            <td style="padding-top:3px;">      
                <select id="cbxFieldName" name="cbxFieldName" style="width:200px;">
                    <?php
                    foreach ($tableColList as $field)
                    {
                        if (!array_key_exists($field['field_code'], $existcol))
                        {
                            ?>
                            <option value="<?= $field['field_code'] ?>"  vxdatatype="<?= $field['datatype'] ?>"><?= $field['field_title'] ?></option>
                            <?php
                        }
                    }
                    ?>
                </select>
            </td>
        </tr>

    </table>      
</div>
<input type="hidden" id="rptid" name="rptid" value="<?= $_REQUEST["rptid"] ?>">
<input type="hidden" id="v" name="v" value="<?= $_REQUEST["v"] ?>">       