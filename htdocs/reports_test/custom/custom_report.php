<?php $page = "clients"; ?>
<?php $option = "reports"; ?>
<?php $selected = 'MetricsReport'; ?>
<?php require ("../../header.php"); ?>
<?php require ("../../navBar.php"); ?>
<?php require ("../../clients/includes/adminCheck.php"); ?>
<?php
$displayPMTools = true;
require_once("../../clients/includes/PMCheck.php");
?>
<?php
$id = $_GET["v"];
?>
<script type="text/javascript" src="/widgets/js/jquery.tinysort.js"></script>
<script type="text/javascript" src="js/custom_report.js"></script>
<script type="text/javascript" src="js/custom_report_addfilter.js"></script>
<!--
<style type="text/css">@import url(../../library/ui.datepicker.css);</style>
<script type="text/javascript" src="../../library/ui.datepicker.js"></script>
-->
<link rel="stylesheet" href="../../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../../library/jquery/calendar/jquery-calendar.js"></script>
<style>
    .titleNew {
        color: black;
        font-size: 12px;
        font-weight: bold;
        border-bottom: 1px solid #F69630;
        height: 18px;
        margin-left: 0px;
        padding-top:5px;
    }
    span {
        display: inline-block;
        width: 190px;
    }
</style>
<?php
$CusReportClass = new Core_Api_CusReportClass();
$ReportInfo = array();
$tableName = $CusReportClass->getListOfTables();
$reportid = $_GET["report_id"];
if (!empty($reportid))
{
    $rptColList = $CusReportClass->getReportFieldList($reportid);
    $ReportInfo = $CusReportClass->getReportInfo($reportid);
    $tableColList = $CusReportClass->getListofFields_InTable($ReportInfo["table"]);
}
?>

<div style='padding-left:10px;padding-right:10px; width:900px'>
    <input type="hidden" id="reportid" value="<?= $reportid ?>">
    <input type="hidden" id="companyid" value="<?= $_GET["v"] ?>">
    <input type="hidden" id="templatename" value="<?= $ReportInfo["rpt_name"] ?>">
    <div style="padding-left:5px;">
        <a href="reportlist.php?v=<?= $_GET["v"] ?>">Back to List</a>
    </div>
    <center>
        <br/>
        <h4><?= empty($reportid) ? "Create" : "Edit" ?> Report Template</h4>
    </center>
    <table cellpadding='5' cellspacing="5" width="100%">
        <tr>
            <td align="left" colspan="3" style="display:none;">
                &nbsp;
                <div id="errorDiv" style="color: red;"></div>
            </td>
        </tr>
        <tr>
            <td width="110px;" align="left">
                Template Name:
            </td>
            <td  colspan="2" align="left">
                <input value="<?= $ReportInfo["rpt_name"] ?>" type="text" name="reportname" id="reportname" style="width:138px;" />
            </td>
        </tr>
        <tr style="<?= empty($reportid) ? "" : "display:none;" ?>">
            <td align="left">
                Report Type:
            </td>
            <td colspan="2" align="left">
                <select id="reporttable" name="reporttable" width="200px">
                    <option value="">Select One</option>
                    <?php
                    if (!empty($tableName))
                    {
                        foreach ($tableName as $table)
                        {
                            ?>
                            <option <?= $ReportInfo["table"] == $table["table_code"] ? "selected" : "" ?> value="<?= $table["table_code"] ?>"><?= $table["table_title"] ?></option>
                            <?php
                        }
                    }
                    ?>

                </select>
            </td>
        </tr>
        <tr style="height:5px;">
            <td colspan="3"></td>
        </tr>
        <tr>
            <td>
                <div class="titleNew" style="border-bottom: 1px solid white;">
                    Filters
                </div>
            </td>
            <td colspan="2" style="text-align:left;">
                <input type="button" id="addfilter" class="link_button middle2_button" value="Add Filter"> 
            </td>
        </tr>
        <tr>
            <td colspan="3" id="filterslist">

            </td>
        </tr>
        <tr style="height:5px;">
            <td colspan="3"></td>
        </tr>
        <tr>
            <td>
                <div class="titleNew" style="border-bottom: 1px solid white;">
                    Columns
                </div>
            </td>
            <td colspan="2" style="text-align:left;">
                <input type="button" id="addcolumn" class="link_button middle2_button" value="Add Column"> 
            </td>
        </tr>
        <tr>
            <td colspan="3" id="columnslist">
            </td>
        </tr>
        <tr style="height:5px;">
            <td colspan="3"></td>
        </tr>
        <tr>
            <td align="center" colspan="3">                
                <div style="float:right; margin-left:0px;">
                    <input type="button" value="Cancel" id="Cancel" class="link_button link_button_popup" onclick="window.location = 'reportlist.php?v=<?= $_GET["v"] ?>'" />
                    <input type="button" value="Save &amp; Generate Report" id="cmdsaveandgenerate" class="link_button middle2_button"/>
                    <input type="button" value="Save" id="cmdAdd" class="link_button link_button_popup"/>
                </div>
            </td>
        </tr>
    </table>
</div>

<!--- End Content --->
<?php require ("../../footer.php"); ?><!-- ../ only if in sub-dir -->

<script>
                        var _custom_report = null;
                        jQuery(window).ready(function() {
                            var _custom_report = custom_report.CreateObject({
                                id: '_custom_report',
                                company: '<?= $_GET["v"] ?>',
                                report_id: '<?= $reportid ?>'
                            });
                        });

                        function showhidegroup(el)
                        {
                            var id = $(el).attr("groupid");
                            if ($("#logicaldatagroup" + id).css("display") == "none")
                            {
                                $("#logicaldatagroup" + id).css("display", "");
                                $("#group" + id).attr("src", "/widgets/images/arrows-up5.png")
                            }
                            else
                            {
                                $("#logicaldatagroup" + id).css("display", "none");
                                $("#group" + id).attr("src", "/widgets/images/arrows-down5.png")
                            }
                        }
                        ;
</script>