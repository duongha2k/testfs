<?php
require_once('../../../includes/modules/web.init.php');
require_once('../../../includes/modules/common.init.php');
require_once('../../../includes/modules/tables.php');

$CusReportClass = new Core_Api_CusReportClass();
$resultFilterList = $CusReportClass->getFilterList($_REQUEST["report_id"]);
//print_r("<pre>");
//print_r($resultFilterList);
//print_r("</pre>");
?>
<table cellpadding="0" cellspacing="0" width="100%" class="gradBox_table_inner" id='tblFilter'>
    <colgroup>
        <col width="*">
        <col width="*">
        <col width="*">
        <col width="60px">
        <col width="30px">
    </colgroup>
    <tr class="table_head">
        <th>
            Field Name 
        </th>
        <th>
            Filtered At
        </th>
        <th>
            Default Value
        </th>
        <th>
            Edit
        </th>
        <th>
            Delete
        </th>
    </tr>
    <?php
    $i = 1;
    foreach ($resultFilterList as $item)
    {
        $filteredAt = '';
        if ($item['filter_at'] == 1)  {
            $filteredAt = "template";
        } else if ($item['filter_at'] == 3)  {
            $filteredAt = "run time";
        } else {
            $filteredAt = "";
        }
        ?>
        <tr class="border_td <?= ($i % 2) ? "odd_tr" : "even_tr" ?>">
            <td>
                <?= $item['field_title'] ?>
            </td>
            <td>
                <?= $filteredAt ?>
            </td>
            <td>
                <?= $item['DefaultValue'] ?>
            </td>
            <td style="text-align:center;">
                <a class="editfilter" href="javascript:;" fieldid="<?= $item['id']; ?>" fieldcode="<?= $item['fieldcode']; ?>"><img title="Edit" style="vertical-align: middle;height:13px;" src="/widgets/images/Pencilicon.png"></a>							                       
            </td>            
            <td style="text-align:center;">
                <a class="deletefilter" href="javascript:;" fieldid="<?= $item['id']; ?>"><img title="Delete" style="vertical-align: middle;height:13px;" src="/widgets/images/delete.gif"></a>							                       
            </td>
        </tr>
        <?
        $i++;
    }
    ?>	
</table>
