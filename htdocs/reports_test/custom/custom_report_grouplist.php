<?php
require_once('../../../includes/modules/web.init.php');
require_once('../../../includes/modules/common.init.php');
require_once('../../../includes/modules/tables.php');

$CusReportClass = new Core_Api_CusReportClass();
//$resultFilterList = $CusReportClass->getFilterList($_REQUEST["report_id"]);
$resultFilterList = $CusReportClass->getReportFieldList_forGroup_1($_REQUEST["report_id"]);
?>
<table id="groupTable" cellpadding="0" cellspacing="0">
    <colgroup>
        <col width="*">
        <col width="55px">
        <col width="55px">
        <col width="80px">
    </colgroup>
    <tr>
        <th></th>
        <th style="text-align:center;">Sum</th>
        <th style="text-align:center;">Count</th>
        <th style="text-align:center;">Group By</th>
    </tr>
    <tbody>
    <?php
    $isSumDisable="";    
    foreach ($resultFilterList as $item)
    {
        if( $item['datatype']=="boolean" ||$item['datatype']=="datetime" ||$item['datatype']=="string" )
        {
            $isSumDisable="disabled";
        }
        else $isSumDisable="";
        $fieldCode = $item['field_code'];  
    ?>
        <tr style="text-align:center;">
            <td style="padding-right:20px;text-align:left;" fieldname="<?=$fieldCode ?>"><?= $item['field_title'] ?></td>
            <td><input id="chksum_<?=$fieldCode ?>" name="chksum_<?=$fieldCode ?>" type="checkbox"<?=$item["issum"]==1?"checked":"";?> <?=$isSumDisable?> ></td>
            <td><input id="chkcou_<?=$fieldCode ?>" name="chkcou_<?=$fieldCode ?>" type="checkbox"<?=$item["iscount"]==1?"checked":"";?>></td>
            <td><input id="chkgro_<?=$fieldCode ?>" name="chkgro_<?=$fieldCode ?>" type="checkbox"<?=$item["isgroupby"]==1?"checked":"";?>></td>
        </tr>
        <?
    }
    ?>
   <tbody>     
</table>
