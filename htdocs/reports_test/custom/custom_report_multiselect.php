<?php
require_once('../../../includes/modules/web.init.php');
require_once('../../../includes/modules/common.init.php');
require_once('../../../includes/modules/tables.php');

$reporttable = $_REQUEST["reporttable"];
$fieldCode = $_REQUEST["fieldCode"];
$v = $_REQUEST["v"];
$CusReportClass = new Core_Api_CusReportClass();
$multilist = $CusReportClass->getValueListOfField($reporttable, $fieldCode,$v);
if (!empty($multilist))
{
    ?>
    <table>
        <?
        foreach ($multilist as $key => $value)
        {
            ?>
            <tr>
                <td style="vertical-align: top;">
                    <input id="chk_<?= $key ?>" val="<?= $value ?>" class="SelectMultiple" type="checkbox" name="SelectMultiple_<?= $key ?>">
                </td>
                <td style="vertical-align: top;">
                    <?= $value ?>
                </td>
            </tr>
            <?
        }
        ?>
    </table>
    <?
}
?>

