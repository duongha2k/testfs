<?
require_once('../../../includes/modules/web.init.php');
require_once('../../../includes/modules/common.init.php');
require_once('../../../includes/modules/tables.php');
$rptid = isset($_REQUEST['reportid']) ? $_REQUEST['reportid'] : "";
$CusReportClass = new Core_Api_CusReportClass();
$ReportInfo = $CusReportClass->getReportInfo($rptid);
$FilterList = $CusReportClass->getFilterList($rptid);

function dateFields($filter)
{
    //$filter['fieldcode']
    $size = 10;
    $filterID = $filter['id'];

    $nameGreater = "Filter$filterID" . "__datetime_range_from";
    $nameLess = "Filter$filterID" . "__datetime_range_to";
    $readOnly = $filter['filter_at'] == 1 ? "readonly" : "";

    $isdate = $filter['filter_at'] == 1 ? "" : "showCal";
    $range_from = "";
    if (!empty($filter['datetime_range_from']))
    {
        $range_from = date("Y-m-d", strtotime($filter['datetime_range_from']));
    }
    $range_to = "";
    if (!empty($filter['datetime_range_to']))
    {
        $range_to = date("Y-m-d", strtotime($filter['datetime_range_to']));
    }

    return "From <input name='$nameGreater' id='$nameGreater' style='width:80px;' class='$isdate' $readOnly value='$range_from'/> 
        &nbsp;&nbsp;To&nbsp;&nbsp;<input name='$nameLess' id='$nameLess'  style='width:80px;' class='$isdate' $readOnly value='$range_to'/>
        <input type='hidden' name='Filter$filterID" . "__datatype' value='" . $filter['datatype'] . "'>
        <input type='hidden' name='Filter$filterID" . "__fieldcode' value='" . $filter['fieldcode'] . "'>
        ";
}

function numFields($filter)
{
    $filterID = $filter['id'];
    $range_from = $filter['numeric_range_from'];
    $range_to = $filter['numeric_range_to'];
    if ($filter['datatype'] == 'int')
    {
        if (isset($range_from))
        {
            $range_from = round($filter['numeric_range_from'], 0);
        }
        if (isset($range_to))
        {
            $range_to = round($filter['numeric_range_to'], 0);
        }
    }
    if ($filter['filter_at'] == 2) //filter at run time
    {
        $range_from = "";
        $range_to = "";
    }

    $nameGreater = "Filter$filterID" . "__numeric_range_from";
    $nameLess = "Filter$filterID" . "__numeric_range_to";
    $readOnly = $filter['filter_at'] == 1 ? "readonly" : "";
    return "From <input name='$nameGreater' id='$nameGreater'  style='width:80px;' class='numtype' $readOnly value='$range_from'/> 
        &nbsp;&nbsp;To&nbsp;&nbsp;<input name='$nameLess' id='$nameLess'  style='width:80px;' class='numtype' $readOnly value='$range_to'/>
        <input type='hidden' name='Filter$filterID" . "__datatype' value='" . $filter['datatype'] . "'>
        <input type='hidden' name='Filter$filterID" . "__fieldcode' value='" . $filter['fieldcode'] . "'>
        ";
}

function boolFields($filter)
{
    $filterID = $filter['id'];
    $bool_value = $filter['bool_value']; //bool_value t:true, f:false, a:any
    $ischecktrue = "";
    $ischeckfalse = "";
    $ischeckany = "";
    if ($filter['filter_at'] == 2) //filter at run time
    {
        $bool_value = "a";
    }
    $name = "Filter$filterID" . "__bool_value";
    $readOnly = $filter['filter_at'] == 1 ? "disabled" : "";

    if ($bool_value == "t")
    {
        $ischecktrue = "checked";
    }
    if ($bool_value == "f")
    {
        $ischeckfalse = "checked";
    }
    if ($bool_value == "a")
    {
        $ischeckany = "checked";
    }

    $strReturn = "<input type='radio' name='$name' id='true$name' value='t' $readOnly $ischecktrue/><lable for='true$name'>True</lable>
            <input type='radio' name='$name' id='false$name' value='f' $readOnly $ischeckfalse/><lable for='false$name'>False</lable>
            <input type='radio' name='$name' id='any$name' value='a' $readOnly $ischeckany/><lable for='any$name'>Any</lable>
    <input type='hidden' name='Filter$filterID" . "__datatype' value='" . $filter['datatype'] . "'>
    <input type='hidden' name='Filter$filterID" . "__fieldcode' value='" . $filter['fieldcode'] . "'>
            ";
    if ($filter['filter_at'] == 1)
    {
        $strReturn .="<input type='hidden' name='$name' value='$bool_value'>";
    }
    return $strReturn;
}

function stringFields($filter, $table, $companyid)
{
    $filterID = $filter['id'];
    $string_value = $filter['string_value'];
    $string_matching = $filter['string_matching'];

    if ($filter['filter_at'] == 2) //filter at run time
    {
        $string_value = "";
        $string_matching = "ExactMatch";
    }
    
    $ExactMatchSel = $string_matching == "ExactMatch" ? "selected='selected'" : "";
    $BeginsWithSel = $string_matching == "BeginsWith" ? "selected='selected'" : "";
    $EndsWithSel = $string_matching == "EndsWith" ? "selected='selected'" : "";
    $ContainsSel = $string_matching == "Contains" ? "selected='selected'" : "";
    $IsNot = $string_matching == "IsNot" ? "selected='selected'" : "";
    $SelectMultiple = $string_matching == "SelectMultiple" ? "selected='selected'" : "";
    $editmulti = ($string_matching == "SelectMultiple") ? "" : "display:none;";
    $noteditmulti = ($string_matching == "SelectMultiple") ? "display:none;" : "";
    $readOnly = $filter['filter_at'] == 1 ? "readonly" : "";
    $disabled = $filter['filter_at'] == 1 ? "disabled" : "";

    $strReturn = "
            <select class='cbxmatching' id='Filter$filterID" . "__string_matching' name='Filter$filterID" . "__string_matching' $disabled style='width:103px'>
                <option value='ExactMatch' $ExactMatchSel>Exact Match</option>
                <option value='BeginsWith' $BeginsWithSel>Begins With</option>
                <option value='EndsWith' $EndsWithSel>Ends With</option>
                <option value='Contains' $ContainsSel>Contains</option>
                <option value='IsNot' $IsNot>Is Not</option>
                <option value='SelectMultiple' $SelectMultiple>Select Multiple</option>
            </select>&nbsp;&nbsp;
            <a style='".$editmulti."' 
                editmulti='".$editmulti."' 
                    class='editSelectMultiple' 
                    href='javascript:;' 
                    rpttable='" . $table . "' 
                        fieldcode='" . $filter['fieldcode'] . "' 
                            fieldid='" . $filter['id'] . "' 
                                v='" . $companyid . "' >edit</a>
            <input class='stringvalue' id='Filter$filterID" . "__string_value' name='Filter$filterID" . "__string_value'  style='width:114px;$noteditmulti' value='$string_value' $readOnly/>
            <input type='hidden' name='Filter$filterID" . "__datatype' value='" . $filter['datatype'] . "'>
            <input type='hidden' name='Filter$filterID" . "__fieldcode' value='" . $filter['fieldcode'] . "'>
            ";
    if (!empty($filter['SelectMultiple']))
    {
        foreach ($filter['SelectMultiple'] as $selecteditem)
        {

            $strReturn .= "<input type='hidden' name='Filter$filterID" . "__SelectMultiple[]' value='" . $selecteditem . "'>";
        }
    }
    if ($filter['filter_at'] == 1)
    {
        $strReturn .= "<input type='hidden' name='Filter$filterID" . "__string_matching' value='$string_matching'>";
    }
    return $strReturn;
}
?>
<table cellspacing='0' cellpadding='0'>
    <col width='250px'>
    <col width='270px'>
    <?
    $tablecontrol = "";
    $controlstr = "";
    if (!empty($FilterList))
    {
        $tablecontrol = "";
        foreach ($FilterList as $filter)
        {
            $controlstr .= "<tr filterid='" . $filter['id'] . "' datatype='" . $filter['datatype'] . "' fieldcode='" . $filter['fieldcode'] . "'>
                                    <td style='padding-right:20px;' >" . $filter['field_title'] . "</td>";
            switch ($filter['datatype'])
            {
                case "datetime":
                    $controlstr .= "<td>" . dateFields($filter) . "</td>";
                    break;
                case "decimal(10,2)":
                case "decimal(10,4)":
                case "float":
                case "int":
                case "money":
                    $controlstr .= "<td>" . numFields($filter) . "</td>";
                    break;
                case "string":
                    $controlstr .= "<td>" . stringFields($filter, $ReportInfo["table"], $ReportInfo["companyid"]) . "</td>";
                    break;
                case "tinyint":
                    $controlstr .= "<td>" . boolFields($filter) . "</td>";
                    break;
            }
            $controlstr .= "</tr>";
        }
        echo($tablecontrol . $controlstr . "");
    } else
    {
        ?>
        <div style="padding:25px;">No Filter for this report.</div>
        <?
    }
    ?>
</table>