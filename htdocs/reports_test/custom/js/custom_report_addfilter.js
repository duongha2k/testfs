
//-------------------------------------------------------------
//-------------------------------------------------------------
//-------------------------------------------------------------
custom_report_addfilter.Instances = null;
//-------------------------------------------------------------
custom_report_addfilter.CreateObject = function(config) {
    var obj = null;
    if (custom_report_addfilter.Instances != null) {
        obj = custom_report_addfilter.Instances[config.id];
    }
    if (obj == null) {
        if (custom_report_addfilter.Instances == null) {
            custom_report_addfilter.Instances = new Object();
        }
        obj = new custom_report_addfilter(config);
        custom_report_addfilter.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function custom_report_addfilter(config) {
    var Me = this;
    this.id = config.id;
    this.company = config.company;
    this.report_id = config.report_id;
    this.cbxFieldName = config.cbxFieldName;


    //------------------------------------------------------------   
    //------------------------------------------------------------   
    //------------------------------------------------------------   
    //------------------------------------------------------------  
    //------------------------------------------------------------   
    this.VisibleGUI = function(val)
    {
        if (typeof(val) == "undefined")
            return;
        var type = val.toLowerCase();
        var customtype = "";
        if (type == 'tinyint') {
            customtype = "boolean";
        } else if ((type.indexOf("decimal") >= 0) || type == 'float' || type == 'int' || type == 'money') {
            customtype = "numeric";
        } else if (type == "datetime" || type == "string") {
            customtype = type;
        }

        switch (customtype)
        {
            case 'datetime':
                $('.trDateType').show();
                $('.trStringType').hide();
                $('.trBooleanType').hide();
                $('.trNumericType').hide();

                var DataReference = $("#cbxRelativeDataReference option:selected").val();

                if (DataReference == "AsEntered")
                {
                    $(".DateTyperange").css("display", "");
                    $("#divDateRangeRuntime").css("display", "none");
                    $("#divDateAsEnter").css("display", "");
                }
                else if (DataReference == "RunTimeDate")
                {
                    $(".DateTyperange").css("display", "");
                    $("#divDateRangeRuntime").css("display", "");
                    $("#divDateAsEnter").css("display", "none");
                }
                else
                {
                    $("#divDateRangeRuntime").css("display", "none");
                    $("#divDateAsEnter").css("display", "none");
                }
                break;
            case 'string':
                $('.trDateType').hide();
                $('.trStringType').show();
                $('.trBooleanType').hide();
                $('.trNumericType').hide();
                break;
            case 'boolean':
                $('.trDateType').hide();
                $('.trStringType').hide();
                $('.trBooleanType').show();
                $('.trNumericType').hide();
                break;
            case 'numeric':
                $('.trDateType').hide();
                $('.trStringType').hide();
                $('.trBooleanType').hide();
                $('.trNumericType').show();
                break;

        }
    }

    //------------------------------------------------------------  
    this.cbxFieldName_onChange = function() {
        var datatype = $(Me.cbxFieldName + " option:selected").attr("vxdatatype");
        //clear data
        $("#radFilteredAt").removeAttr("checked");
        //$("#radRunTimewithDefaultValue").removeAttr("checked");
        $("#radRunTimewithDefaultValue").attr("checked", "checked");
        $("#cbxRelativeDataReference option:first").attr("selected", "selected");
        Me.cbxRelativeDataReference_onChange();
        $("#txtFromRange").val("");
        $("#txtToRange").val("");
        $("#cbxMatching option:first").attr("selected", "selected");
        $("#txtStringTypeValue").val("");
        $("#radBooleanAny").attr("checked", "checked");
        $("#txtNumericBaseValue").val("");
        $("#txtNumericRangeFrom").val("");
        $("#txtNumericRangeTo").val("");
        Me.VisibleGUI(datatype);
    }
    this.cbxMatching_onChange = function()
    {
        if ($("#cbxMatching option:selected").val() == "SelectMultiple")
        {
            $("#notmulti").css("display", "none");
            $("#multi").css("display", "");
            var reporttable = $("#cbxMatching").attr("reporttable");
            var fieldCode = $("#cbxFieldName option:selected").val()
            var company = $("#cbxMatching").attr("company");
            
            var data = "reporttable=" + reporttable + "&fieldCode=" + fieldCode +"&v="+company;
            $.ajax({
                type: "POST",
                url: "/reports_test/custom/custom_report_multiselect.php",
                data: data,
                success: function(html) {
                    if (html != "")
                    {
                        jQuery("#multi").html(html);
                    }
                }
            });
        }
        else
        {
            $("#notmulti").css("display", "");
            $("#multi").css("display", "none");
        }
    }

    //------------------------------------------------------------  
    this.cbxRelativeDataReference_onChange = function()
    {
        var DataReference = $("#cbxRelativeDataReference option:selected").val();
        if (DataReference == "RunTimeDate")
        {
            $("#trDateType").css("display", "");
            $(".DateTyperange").css("display", "");
            $("#divDateRangeRuntime").css("display", "");
            $("#divDateAsEnter").css("display", "none");
        }
        else if (DataReference == "AsEntered")
        {
            $("#trDateType").css("display", "");
            $(".DateTyperange").css("display", "");
            $("#divDateRangeRuntime").css("display", "none");
            $("#divDateAsEnter").css("display", "");
        }
        else
        {
            $("#trDateType").css("display", "none");
            $(".DateTyperange").css("display", "none");
            $("#divDateRangeRuntime").css("display", "none");
            $("#divDateAsEnter").css("display", "none");
        }
    }

    //------------------------------------------------------------
    this.ResetData = function() {

        $(Me.cbxFieldName + " option:first").attr("selected", "selected");
        $("#radFilteredAt").removeAttr("checked");
        $("#radRunTimewithDefaultValue").removeAttr("checked");
        $("#radRunTime").attr("checked", "checked");
        $("#cbxRelativeDataReference option:first").attr("selected", "selected");
        $("#txtFromRange").val("");
        $("#txtToRange").val("");
        $("#cbxMatching option:first").attr("selected", "selected");
        $("#txtStringTypeValue").val("");
        $("#radBooleanAny").attr("checked", "checked");
        $("#txtNumericBaseValue").val("");
        $("#txtNumericRangeFrom").val("");
        $("#txtNumericRangeTo").val("");

        var ValueSelect = $(Me.cbxFieldName + " option:selected").attr("vxdatatype");
        Me.VisibleGUI(ValueSelect);
    }

    //------------------------------------------------------------   
    this.InitPage = function() {
        var ValueSelect = $(Me.cbxFieldName + " option:selected").attr("vxdatatype");
        if (typeof(ValueSelect) != "undefined")
        {
            Me.VisibleGUI(ValueSelect);
        }
        else
        {
            $('.trDateType').hide();
            $('.trStringType').hide();
            $('.trBooleanType').hide();
            $('.trNumericType').hide();
        }
    }
    //------------------------------------------------------------
    this.inputData_onlyInteger = function(e)
    {

        var valueInput = $(this).val();
        var keyPressed;
        if (!e)
            var e = window.event;
        if (e.keyCode)
            keyPressed = e.keyCode;
        else if (e.which)
            keyPressed = e.which;
        //var hasDecimalPoint = (($(this).val().split('.').length-1)>0);

        var hasSub = ((valueInput.split('-').length - 1) > 0);
        var IsStartSub = valueInput.substring(0, "-".length);
        var isMutilSub = (!hasSub) ? ((!IsStartSub) ? false : true) : true;
        //if isMutilSub==false then check value input is lenght ==0 purpose check sub is first input 
        isMutilSub = !isMutilSub ? ((valueInput.length == 0) ? false : true) : isMutilSub;
        if (
                ((keyPressed == 45
                        || keyPressed == 109
                        )
                        && (!isMutilSub))
                || keyPressed == 8
                || keyPressed == 27
                || keyPressed == 13
                // Allow: Ctrl+A
                || (keyPressed == 65 && e.ctrlKey === true)
                // Allow: home, end, left, right
                || (keyPressed >= 35 && keyPressed <= 39)
                )
        {
            // let it happen, don't do anything
            return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (e.shiftKey || (keyPressed < 48 || keyPressed > 57) && (keyPressed < 96 || keyPressed > 105)) {
                e.preventDefault();
            }
        }
    }
    //------------------------------------------------------------
    this.init = function() {

        Me.InitPage();

        if ($("#cbxFieldName").attr("disabled"))
        {
            var ValueSelect = $("#cbxFieldName option:selected").attr("vxdatatype");
            Me.VisibleGUI(ValueSelect);
        }

        $(Me.cbxFieldName)
                .unbind("change", Me.cbxFieldName_onChange)
                .bind("change", Me.cbxFieldName_onChange);
        $('#cbxRelativeDataReference')
                .unbind("change")
                .bind("change", Me.cbxRelativeDataReference_onChange);
        $('#txtNumericRangeFrom')
                .unbind("keydown", Me.inputData_onlyInteger)
                .bind("keydown", Me.inputData_onlyInteger);

        $('#txtNumericRangeTo')
                .unbind("keydown", Me.inputData_onlyInteger)
                .bind("keydown", Me.inputData_onlyInteger);

        $('#txtNumericBaseValue')
                .unbind("keydown", Me.inputData_onlyInteger)
                .bind("keydown", Me.inputData_onlyInteger);
        $("#cbxMatching")
                .unbind("change")
                .bind("change", Me.cbxMatching_onChange);

    }
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
}

