//-------------------------------------------------------------
custom_report.Instances = null;

//-------------------------------------------------------------
custom_report.CreateObject = function(config) {
    var obj = null;
    if (custom_report.Instances != null) {
        obj = custom_report.Instances[config.id];
    }
    if (obj == null) {
        if (custom_report.Instances == null) {
            custom_report.Instances = new Object();
        }
        obj = new custom_report(config);
        custom_report.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}

//-------------------------------------------------------------
function custom_report(config) {
    var Me = this;
    this.id = config.id;
    this.selectOutput = jQuery("#selectOutput");
    this.selectInput = jQuery("#selectInput");
    this.company = config.company;
    this.report_id = config.report_id;
    this.MultiselectList = [];
    this._roll = new FSPopupRoll();
    this.ItemFilter = '<div style="padding-top:5px;width:600px;">'
            + '<img style="cursor:pointer;padding-right:2px;" src="/widgets/images/ico_closex9.png" class="imageRemove classClickRemoveRateFS" projectvalue="_VXPROJECTVALUE_" projectname="_VXPROJECTNAME_">'
            + '<span>_VXPROJECTNAME_</span>'
            + '<select id="cbxOperator_VXPROJECTVALUE_" name="cbxOperator_VXPROJECTVALUE_" >'
            + '<option value="EQUALS">Equals</option>'
            + '<option value="GREATERS">Greater</option>'
            + '<option value="LESS">Less</option>'
            + '<option value="LIKE">Like</option>'
            + '</select>'
            + '&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="txtInputOperator_VXPROJECTVALUE_" name="txtInputOperator_VXPROJECTVALUE_">'
            + '</div>';
    //------------------------------------------------------------
    this.SpaceSelect = "&nbsp;&nbsp;&nbsp;";

    //------------------------------------------------------------
    this.checkHasGroup = function() {
        var has = false;
        jQuery(Me.selectInput).find("option").each(function() {
            if (jQuery(this).val() == 0) {
                has = true;
                return false;
            }
        });
        return has;
    }

    //------------------------------------------------------------
    this.getDataFrom = function() {
        var data = [];
        // valid
        var reportname = jQuery.trim(jQuery("#reportname").val());
        var i = 0;
        data["msg"] = [];
        if (reportname == "") {
            data["msg"][i] = "Please enter Report Name.";
            i++;
        }
        var reporttable = jQuery.trim(jQuery("#reporttable option:selected").val());
        if (reporttable == "") {
            data["msg"][i] = "Please select Table.";
            i++;
        }

        if ($("#collist tr").length == 1) {
            data["msg"][i] = "Please add at least one column.";
        }

        return data;
    }

    //------------------------------------------------------------
    this.validForm = function(msg) {
        jQuery("#errorDiv").html("");
        var html = "";
        for (var i = 0; i < msg.length; i++) {
            html += "<div>" + msg[i] + "</div>";
        }
        jQuery("#errorDiv").html(html);
        $("#errorDiv").parent().css('display', '');
    }

    //------------------------------------------------------------
    this.cmdAdd_click = function() {
        jQuery("#errorDiv").html("");
        var data = Me.getDataFrom();
        if (data["msg"].length > 0) {
            Me.validForm(data["msg"]);
            return false;
        }

        var _onSuccess = function(data) {
            window.location = 'reportlist.php?v=' + Me.company;
        }

        var reportname = jQuery.trim(jQuery("#reportname").val());
        var reporttable = jQuery.trim(jQuery("#reporttable option:selected").val());

        var colList = "";
        colList = Me.getColDataValueForNewReport();

        var filterList = "";
        filterList = Me.getFilterDataValueForCreateReport();

        var arrangeList = Me.getArrangeColForNewReport();

        var param = {
            reportname: reportname,
            reporttable: reporttable,
            company: Me.company,
            collist: colList,
            filterlist: filterList,
            arrangelist: arrangeList
        };


        var postData = param;
        var postUrl = "/widgets/dashboard/custom-report/do-create-report";
        $.ajax({
            type: "POST",
            url: postUrl,
            dataType: 'json',
            cache: true,
            async: true,
            data: postData,
            success: function() {
                _onSuccess();
            }
        });

    }

    //------------------------------------------------------------
    this.SortOptions = function(id) {
        var prePrepend = "#";
        if (id.match("^#") == "#")
            prePrepend = "";
        $(prePrepend + id).html($(prePrepend + id + " option").sort(
                function(a, b) {
                    return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
                }));
    }

    //------------------------------------------------------------
    this.imageRemove_onClick = function() {
        var value = jQuery(this).attr("projectvalue");
        var name = jQuery(this).attr("projectname");
        var option = jQuery("<option>")
                .val(value)
                .html(name);
        jQuery("#cbxAllFilter").append(option);
        jQuery(this).parent().remove();

        //Me.SortOptions("#cbxAllFilter");
    }

    //------------------------------------------------------------
    this.AppendDataForAllFilter = function(value, text) {
        jQuery("#cbxAllFilter").append(jQuery("<option>").val(value).html(text));
    }

    //------------------------------------------------------------
    this.RemoveDataForAllFilter = function(value, text) {
        jQuery("#cbxAllFilter option[value=\"" + value + "\"]").remove();
        jQuery("#divListFilter img[projectvalue=\"" + value + "\"]").parent().remove();
    }

    //------------------------------------------------------------
    this.BindDataFilter = function(elem) {
        var value = jQuery(elem).val();
        var Name = jQuery("#cbxAllFilter option:selected").html();

        var item = Me.ItemFilter.replace(/_VXPROJECTVALUE_/g, value).replace(/_VXPROJECTNAME_/g, Name);
        jQuery("#divListFilter").append(item);
        jQuery("#cbxAllFilter option:selected").remove();
        jQuery(".imageRemove")
                .unbind("click", Me.imageRemove_onClick)
                .bind("click", Me.imageRemove_onClick);
    }

    //------------------------------------------------------------
    this.cbxAllFilter_onChange = function()
    {
        var value = jQuery(this).val();
        if (value == "")
            return;
        Me.BindDataFilter(this);
    }

    //------------------------------------------------------------
    this.BindDataForcbxAllFilter = function()
    {
        var html = "";
        html += '<option value="">Select One</option>';
        jQuery(Me.selectOutput).find("option").each(function() {
            html += '<option value="' + jQuery(this).val() + '">' + jQuery(this).html() + '</option>';
        });
        jQuery("#cbxAllFilter").html(html);
        jQuery("#divListFilter").html("");
    }

    //------------------------------------------------------------
    this.InitPage = function() {
        jQuery(document).ready(function() {
            jQuery(Me.selectOutput).find("option").each(function() {
                var value = jQuery(this).val();
                jQuery(Me.selectInput).find("option[value=\"" + value + "\"]").remove();
            });
        });
    }

    //606
    //load filter list
    //------------------------------------------------------------
    this.loadfilterlist = function()
    {
        var data = "report_id=" + Me.report_id;
        $.ajax({
            type: "POST",
            url: "/reports_test/custom/custom_report_filterslist.php",
            data: data,
            success: function(html) {
                if (html != "")
                {
                    jQuery("#filterslist").html(html);
                    jQuery(".deletefilter").unbind("click", Me.deletefilter_click).bind("click", Me.deletefilter_click);
                    jQuery(".editfilter").unbind("click", Me.cmdaddfilter_click).bind("click", Me.cmdaddfilter_click);
                }
            }
        });
    }

    //popup add filter
    //------------------------------------------------------------
    this.cmdaddfilter_click = function() {
        var el = $(this).attr('class');
        var html = "";
        if (el == "editfilter")
        {
            html = '\
            <div>\n\
                <div id="ContainerID_frmFilter">\n\
                </div>\n\
                <div>\n\
                    <table width="100%">\n\
                        <tr>\n\
                        <td style="width:103px;"></td>\n\
                        <td style="width:120px;">\n\
                            <input type="button" id="cmdFilterCancel" class="link_button button80" name="cmdFilterCancel" value="Cancel">\n\
                        </td>\n\
                        <td>\n\
                            <input type="button" id="cmdFilterSave" class="link_button button80" name="cmdFilterSave" value="Save">\n\
                        </td>\n\
                        <td></td>\n\
                        </tr>\n\
                    </table>\n\
                </div>';
        }
        else
        {
            html = '\
            <div>\n\
                <div id="ContainerID_frmFilter">\n\
                </div>\n\
                <div style="text-align:center;" >\n\
                    <input type="button" id="cmdFilterCancel" class="link_button link_button_popup" name="cmdFilterCancel" value="Cancel">\n\
                    <input type="button" id="cmdFilterSaveAddMore" class="link_button" name="cmdFilterSaveAddMore" value="Save &amp; Add Another">\n\
                    <input type="button" id="cmdFilterSave" class="link_button link_button_popup" name="cmdFilterSave" value="Save">\n\
            </div>\n\
            </div>';
        }

        var opt = {};
        opt.width = 440;
        opt.height = '';
        opt.position = 'top';
        opt.wTop = 250;
        //opt.paddingTop = -200;
        if (el == "editfilter")
        {
            opt.title = '<div style="text-align:center;">Edit Filter</div>';
        }
        else
        {
            opt.title = '<div style="text-align:center;">Add Filter</div>';
        }
        opt.body = html;
        opt.zindex = 999;
        Me._roll.showNotAjax(null, null, opt);

        var rptid = $("#reportid").val();
        var companyid = $("#companyid").val();
        var template = $("#templatename").val();
        var reportname = jQuery.trim(jQuery("#reportname").val());
        var reporttable = $("#reporttable option:selected").val();

        var data = "rptid=" + rptid + "&v=" + companyid + "&template=" + template + "&reporttable=" + reporttable + "&reportname=" + reportname;

        if (Me.report_id == "")
        {
            data += Me.loadTempFilterFields();
        }
        if (el == "editfilter")
        {
            data += Me.loadTempFilterValue(this);
        }
        var param = {"rptid": rptid,
            "v": companyid,
            "template": template,
            "reporttable": reporttable,
            "reportname": template,
            "fieldcode": $(this).attr("fieldcode"),
            "fieldid": $(this).attr("fieldid"),
            "fieldtype": $(this).attr("fieldtype"),
            "Filterby": $(this).attr("filterby"),
            "fromval": $(this).attr("fromval"),
            "toval": $(this).attr("toval"),
            "FilterAt": $(this).attr("selectedfilterat"),
            "multiselect": Me.MultiselectList[$(this).attr("fieldcode")]

        };

        if (el != "editfilter")
        {
            param = data;
        }

        $.ajax({
            type: "POST",
            url: "/reports_test/custom/custom_report_addfilter.php",
            data: param,
            success: function(html) {
                if (html != "")
                {
                    jQuery("#ContainerID_frmFilter").html(html);
                    $("#cmdFilterclose").unbind('click').bind('click', function() {
                        Me._roll.hide();
                    })
                    $("#cmdFilterCancel").unbind('click').bind('click', function() {
                        Me._roll.hide();
                    })

                    if ($("#cbxFieldName option").length == 0)
                    {
                        Me.isDisabledbutton(true, "cmdFilterSaveAddMore", "cmdFilterSave")
                        $('#cmdFilterSaveAddMore').unbind("click", Me.saveAddedFilterAddMore);
                        $('#cmdFilterSave').unbind("click", Me.saveAddedFilter);
                    }
                    else
                    {
                        Me.isDisabledbutton(false, "cmdFilterSaveAddMore", "cmdFilterSave")
                        $('#cmdFilterSaveAddMore').unbind("click", Me.saveAddedFilterAddMore).bind("click", Me.saveAddedFilterAddMore);
                        $('#cmdFilterSave').unbind("click", Me.saveAddedFilter).bind("click", Me.saveAddedFilter);
                    }

                    if (el == "editfilter")
                    {
                        $('#cmdFilterSaveAddMore').unbind("click");
                        $('#cmdFilterSaveAddMore').attr("disabled", true);
                        $('#cmdFilterSaveAddMore').removeClass("link_button");
                        $('#cmdFilterSaveAddMore').addClass("link_button_disabled");
                        $('#cmdFilterSaveAddMore').css('color', '#696969');
                    }
                }
            }
        });
    }

    this.saveAddedFilter = function(ismore)
    {
        if (!Me.CheckValid())
            return;
        if (typeof(ismore) != 'boolean')
        {
            ismore = false;
        }

        $('#cmdFilterSaveAddMore').attr("disabled", true);
        $('#cmdFilterSave').attr("disabled", true);
        if (Me.report_id != "")
        {
            Me.doSavefilter(ismore)
        }
        else
        {
            Me.doAddFilterToTable(ismore);
        }
    }

    this.saveAddedFilterAddMore = function()
    {
        Me.saveAddedFilter(true);
    }

    this.doSavefilter = function(ismore)
    {
        var colcode = $("#cbxFieldName option:selected").val();
        var fieldid = "";
        if ($("#fieldid").val() != "")
            fieldid = $("#fieldid").val();

        var data = Me.getDataValue();
        var key_value_array = [];
        key_value_array.push(data);
        var param = {
            report_id: Me.report_id,
            fieldid: fieldid,
            key_value_array: key_value_array
        };
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/custom-report/do-add-filter-report",
            data: param,
            success: function(data) {
                if (data.success)
                {
                    if (!ismore)
                    {
                        Me.loadfilterlist();
                        Me.loadcolumnlist();
                        Me._roll.hide();
                    }
                    else
                    {
                        Me.removeItem("filter", "cbxFieldName", colcode);
                        Me.loadcolumnlist();
                        Me.loadfilterlist();
                    }
                }
                else
                {
                    //error
                }
            }
        });
    }

    this.doAddFilterToTable = function(ismore)
    {
        var fieldname = $("#cbxFieldName option:[selected]").html();
        var fieldcode = $("#cbxFieldName option:[selected]").val();
        var fieldtype = $("#cbxFieldName option:[selected]").attr("vxdatatype");
        var fieldidupdate = $("#fieldid").val();
        var FilteredAt = "";
        var Filterby = "";
        var DefaultValue = "";
        var fromval = "";
        var toval = "";
        var selectedFilterAt = 0;
        Me.MultiselectList[fieldcode] = [];
        var MultiselectList = [];

        if (fieldtype == 'tinyint') {
            fieldtype = "boolean";
        } else if ((fieldtype.indexOf("decimal") >= 0) || fieldtype == 'float' || fieldtype == 'int' || fieldtype == 'money') {
            fieldtype = "numeric";
        } else if (fieldtype == "datetime" || fieldtype == "string") {
            fieldtype = fieldtype;
        }

        if (fieldtype == "datetime")
        {
            Filterby = $("#cbxRelativeDataReference option:[selected]").val();
            if (Filterby == "AsEntered")
            {
                fromval = $("#txtDateAsEnterForm").val();
                toval = $("#txtDateAsEnterTo").val();
            }
            else if (Filterby == "RunTimeDate")
            {
                fromval = $("#txtFromRange").val();
                toval = $("#txtToRange").val();
            }


        }
        else if (fieldtype == "string")
        {
            fromval = $("#txtStringTypeValue").val();
            Filterby = $("#cbxMatching option:[selected]").val();
            if (Filterby == "SelectMultiple")
            {

                $("#multi input").each(function() {
                    if ($(this).is(":checked"))
                    {
                        var data = null;

                        data =
                                {
                                    val: $(this).attr('val')
                                };
                        MultiselectList.push(data);
                    }
                });
                Me.MultiselectList[fieldcode] = {fieldcode: fieldcode, data: MultiselectList};
            }
        }
        else if (fieldtype == "boolean")
        {
            if ($("#radBooleanTrue").is(":checked"))
            {
                Filterby = "t";
            }
            else if ($("#radBooleanFalse").is(":checked"))
            {
                Filterby = "f";
            }
            else if ($("#radBooleanAny").is(":checked"))
            {
                Filterby = "a";
            }
        }
        else if (fieldtype == "numeric")
        {
            fromval = $("#txtNumericRangeFrom").val();
            toval = $("#txtNumericRangeTo").val();
        }

        DefaultValue = Me.getDefaultval(fieldtype, Filterby, fromval, toval);

        if ($("#radTemplate").is(":checked"))
        {
            FilteredAt = "template";
            selectedFilterAt = 1;
        }
        else if ($("#radRunTime").is(":checked"))
        {
            FilteredAt = "";
            selectedFilterAt = 2;
        }
        else if ($("#radRunTimewithDefaultValue").is(":checked"))
        {
            FilteredAt = "run time";
            selectedFilterAt = 3;
        }

        var strclass = "";
        if (($("#filterslist tr").length + 1) % 2 > 0)
        {
            strclass = "even_tr";
        }
        else
        {
            strclass = "odd_tr";
        }

        var str = "<tr class=\"border_td  " + strclass + "\" fieldcode=\"" + fieldcode + "\" fieldtype=\"" + fieldtype + "\" \n\
        runtime=\"" + selectedFilterAt + "\" filterby=\"" + Filterby + "\"  fromval=\"" + fromval + "\"  toval=\"" + toval + "\">\n\
                        <td>" + fieldname + "</td>\n\
                        <td>" + FilteredAt + "</td>\n\
                        <td>" + DefaultValue + "</td>\n\
                        <td style=\"text-align:center;\">\n\
                            <a fieldtype=\"" + fieldtype + "\" Filterby=\"" + Filterby + "\" fromval=\"" + fromval + "\" toval=\"" + toval + "\" selectedFilterAt=\"" + selectedFilterAt + "\"  fieldname=\"" + fieldname + "\" fieldid=\"" + fieldcode + "\" fieldcode=\"" + fieldcode + "\" href=\"javascript:;\" class=\"editfilter\">\n\
                            <img src=\"/widgets/images/Pencilicon.png\" style=\"vertical-align: middle;height:13px;\" title=\"Edit\"></a>\n\
                        </td>\n\
                        <td style=\"text-align:center;\">\n\
                            <a fieldname=\"" + fieldname + "\" fieldid=\"" + fieldcode + "\" href=\"javascript:;\" class=\"deletefilter\">\n\
                            <img src=\"/widgets/images/delete.gif\" style=\"vertical-align: middle;height:13px;\" title=\"Delete\"></a>\n\
                        </td>\n\
                    </tr>";
        var strupdate = "<td>" + fieldname + "</td>\n\
                        <td>" + FilteredAt + "</td>\n\
                        <td>" + DefaultValue + "</td>\n\
                        <td style=\"text-align:center;\">\n\
                            <a fieldtype=\"" + fieldtype + "\" Filterby=\"" + Filterby + "\" fromval=\"" + fromval + "\" toval=\"" + toval + "\" selectedFilterAt=\"" + selectedFilterAt + "\"  fieldname=\"" + fieldname + "\" fieldid=\"" + fieldcode + "\" fieldcode=\"" + fieldcode + "\" href=\"javascript:;\" class=\"editfilter\">\n\
                            <img src=\"/widgets/images/Pencilicon.png\" style=\"vertical-align: middle;height:13px;\" title=\"Edit\"></a>\n\
                        </td>\n\
                        <td style=\"text-align:center;\">\n\
                            <a fieldname=\"" + fieldname + "\" fieldid=\"" + fieldcode + "\" href=\"javascript:;\" class=\"deletefilter\">\n\
                            <img src=\"/widgets/images/delete.gif\" style=\"vertical-align: middle;height:13px;\" title=\"Delete\"></a>\n\
                        </td>";
        if (fieldidupdate == "")
        {
            $("#filterslist tbody").append(str);
        }
        else
        {
            $("#filterslist tbody tr").each(
                    function() {
                        if ($(this).attr("fieldcode") == fieldidupdate)
                        {
                            $(this).html(strupdate);
                            $(this).attr("toval", toval);
                            $(this).attr("fromval", fromval);
                            $(this).attr("filterby", Filterby);
                            $(this).attr("runtime", selectedFilterAt);
                            $(this).attr("fieldtype", fieldtype);
                            $(this).attr("fieldcode", fieldcode);
                        }
                    });

        }

        jQuery(".deletefilter").unbind("click", Me.deletefilter_click).bind("click", Me.deletefilter_click);
        jQuery(".editfilter").unbind("click", Me.cmdaddfilter_click).bind("click", Me.cmdaddfilter_click);
        if (!$("#cbxFieldName").attr('disabled'))
        {
            Me.doAddColumnToTable(fieldname, fieldcode, fieldtype, ismore);
        }
        if (ismore)
        {
            Me.removeItem("filter", "cbxFieldName", fieldcode);
        }
        else
        {
            Me._roll.hide();
        }
    }

    //popup delete filter
    this.deletefilter_click = function() {
        var fieldid = $(this).attr('fieldid');
        var html = '\
<div style="margin-top:-20px">\n\
    <div style="text-align:left;height:20px;">\n\
    Would you like to delete this item?</div>\n\
    <div style="text-align:center;" >\n\
        <input type="button" id="btnDelFilterCancel" class="link_button_popup" name="btnDelFilterCancel" value="Cancel">\n\
        <input type="button" id="btnFilterDelete" class="link_button_popup" name="btnFilterDelete" value="Delete" fieldid="' + fieldid + '">\n\
    </div>\n\
</div>';
        var opt = {};
        opt.width = 440;
        opt.height = '';
        opt.position = 'middle';
        opt.title = '<div style="text-align:center;"></div>';
        opt.body = html;
        opt.zindex = 99999;
        Me._roll.showNotAjax(null, null, opt);
        jQuery("#btnDelFilterCancel").unbind("click").bind('click', function() {
            Me._roll.hide();
        });
        jQuery("#btnFilterDelete").unbind("click").bind('click', Me.dodeletefilter);
    }

    this.dodeletefilter = function()
    {
        var rptid = Me.report_id;
        var filterID = $(this).attr('fieldid');
        if (rptid == "")
        {
            $("#filterslist tbody tr[fieldcode='" + filterID + "']").remove();
            Me._roll.hide();
        }
        else
        {
            var param = {
                rptid: rptid,
                filterID: filterID
            };
            $.ajax({
                type: "POST",
                url: "/widgets/dashboard/custom-report/do-delete-filter-report",
                data: param,
                success: function(data) {
                    if (data.success)
                    {
                        Me.loadfilterlist();
                        Me._roll.hide();
                    }
                    else
                    {

                    }
                }
            });
        }
    }

    //load columns list
    //------------------------------------------------------------
    this.loadcolumnlist = function()
    {
        var data = "report_id=" + Me.report_id;
        $.ajax({
            type: "POST",
            url: "/reports_test/custom/custom_report_columnslist.php",
            data: data,
            success: function(html) {
                if (html != "")
                {
                    jQuery("#columnslist").html(html);
                    $(".deletecolumn").unbind("click").bind("click", Me.deletecolumn_click);
                }
            }
        });
    }

    //popup add column
    this.addcolumn_click = function()
    {
        var html = '\
<div>\n\
    <div id="ContainerID_frmaddcolumn">\n\
    </div>\n\
    <div style="text-align:center;" >\n\
        <input type="button" id="btnAddCancel" class="link_button link_button_popup" name="btnAddCancel" value="Cancel">\n\
        <input type="button" id="btnAddSaveAddMore" class="link_button" name="btnAddSaveAddMore" value="Save &amp; Add Another">\n\
        <input type="button" id="btnAddSave" class="link_button link_button_popup" name="btnAddSave" value="Save">\n\
    </div>\n\
</div>';
        var opt = {};
        opt.width = 440;
        opt.height = "";
        opt.position = 'top';
        opt.wTop = 250;
        opt.title = '<div style="text-align:center;">Add Report Columns</div>';
        opt.body = html;
        opt.zindex = 99999;
        Me._roll.showNotAjax(null, null, opt);

        var rptid = $("#reportid").val();
        var companyid = $("#companyid").val();
        var template = $("#templatename").val();
        var tableid = "";
        var reportname = jQuery.trim(jQuery("#reportname").val());

        var data = "rptid=" + rptid + "&v=" + companyid + "&template=" + template + "&reportname=" + reportname;

        if (Me.report_id == "")
        {
            tableid = $("#reporttable option:[selected]").val();
            data += "&tableid=" + tableid;
            data += Me.loadTempFields();
        }

        $.ajax({
            type: "POST",
            url: "/reports_test/custom/custom_report_addcolumn.php",
            data: data,
            success: function(html) {
                if (html != "")
                {
                    jQuery("#ContainerID_frmaddcolumn").html(html);
                    jQuery("#btnAddCancel").unbind("click").bind('click', function() {
                        Me._roll.hide();
                    });
                    if ($("#cbxFieldName option").length == 0)
                    {
                        Me.isDisabledbutton(true, "btnAddSaveAddMore", "btnAddSave");
                        $('#btnAddSaveAddMore').unbind("click", Me.saveAddedColAddMore);
                        $('#btnAddSave').unbind("click", Me.saveAddedCol);
                    }
                    else
                    {
                        Me.isDisabledbutton(false, "btnAddSaveAddMore", "btnAddSave");
                        jQuery("#btnAddSave").unbind("click").bind('click', Me.saveAddedCol);
                        jQuery("#btnAddSaveAddMore").unbind("click").bind('click', Me.saveAddedColAddMore);
                    }
                }
            }
        });

    }

    this.saveAddedCol = function(ismore)
    {

        if (typeof(ismore) != 'boolean')
        {
            ismore = false;
        }

        if (Me.report_id != "")
        {
            Me.doSaveColumn(ismore);
        }
        else
        {
            var fieldname = $("#cbxFieldName option:[selected]").html();
            var fieldcode = $("#cbxFieldName option:[selected]").val();
            var fieldtype = $("#cbxFieldName option:[selected]").attr("vxdatatype");
            Me.doAddColumnToTable(fieldname, fieldcode, fieldtype, ismore);
        }
    }

    this.saveAddedColAddMore = function()
    {
        Me.saveAddedCol(true);
    }

    this.doSaveColumn = function(ismore)
    {

        var rptid = $("#rptid").val();
        var colcode = $("#cbxFieldName option:selected").val();
        var param = {
            rptid: rptid,
            colcode: colcode
        };
        $('#btnAddSaveAddMore').attr("disabled", true);
        $('#btnAddSave').attr("disabled", true);

        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/custom-report/do-add-col-report",
            data: param,
            success: function(data) {
                if (data.success)
                {
                    if (!ismore)
                    {
                        Me.loadcolumnlist();
                        Me._roll.hide();
                    }
                    else
                    {
                        Me.removeItem("column", "cbxFieldName", colcode);
                        Me.loadcolumnlist();
                    }
                }
                else
                {
                    //error
                }
            }
        });
    }

    this.doAddColumnToTable = function(fieldname, fieldcode, fieldtype, ismore)
    {
        var strclass = "";
        if (($("#columnslist tr").length + 1) % 2 > 0)
        {
            strclass = "even_tr";
        }
        else
        {
            strclass = "odd_tr";
        }

        var str = "<tr class=\"border_td  " + strclass + "\" fieldcode=\"" + fieldcode + "\" fieldname=\"" + fieldname + "\" fieldtype=\"" + fieldtype + "\">\n\
                        <td fieldname=\"" + fieldcode + "\" >" + fieldname + "</td>\n\
                        <td style=\"padding:0px 3px 0px 3px;\">" + "" + "\
<div style=\"width: 40%;float: left;\"><a class=\"up\" id=\"up_" + fieldcode + "\" href=\"javascript:;\" >Up</a></div>\n\
<div style=\"width: 50%;float: left;\"><a class=\"down\" id=\"down_" + fieldcode + "\" href=\"javascript:;\" >Down</a></div>\n\
</td>\n\
                        <td style=\"text-align:center;\">\n\
                            <a fieldname=\"" + fieldname + "\" fieldid=\"" + fieldcode + "\" href=\"javascript:;\" class=\"deletecolumn\">\n\
                            <img src=\"/widgets/images/delete.gif\" style=\"vertical-align: middle;height:13px;\" title=\"Delete\"></a>\n\
                        </td>\n\
                    </tr>";

        $("#collist").append(str);
        $(".deletecolumn").unbind("click").bind("click", Me.deletecolumn_click);
        $("#collist tbody .up").html('Up');
        $("#collist tbody > tr:first .up").html('&nbsp;&nbsp;');
        $("#collist tbody .down").html('Down');
        $("#collist tbody > tr:last .down").html('');

        $(".up").unbind("click").bind("click", Me.cmdUp_click);
        $(".down").unbind("click").bind("click", Me.cmdDown_click);

//        Me.BindGroupList(fieldcode, fieldname, fieldtype);
        if (ismore)
        {
            Me.removeItem("column", "cbxFieldName", fieldcode);
        }
        else
        {
            Me._roll.hide();
        }
    }

    //popup delete column
    this.deletecolumn_click = function() {
        var fieldname = $(this).attr('fieldname');
        var fieldid = $(this).attr('fieldid');
        var html = '<div style="margin-top:-20px"><div style="text-align:left;height:20px;">\n\
    Are you sure you would like to delete field  <b>' + fieldname + '</b>?</div>\n\
    <div style="text-align:center;" >\n\
        <input type="button" id="btnDelColumnCancel" class="link_button_popup" name="btnDelColumnCancel" value="No">\n\
        <input type="button" id="btnColumnDelete" class="link_button_popup" name="btnColumnDelete" value="Yes" fieldid="' + fieldid + '">\n\
    </div>\n\
</div>';
        var opt = {};
        opt.width = 440;
        opt.height = '';
        opt.position = 'middle';
        opt.title = '<div style="text-align:center;"></div>';
        opt.body = html;
        opt.zindex = 99999;
        Me._roll.showNotAjax(null, null, opt);
        jQuery("#btnDelColumnCancel").unbind("click").bind('click', function() {
            Me._roll.hide();
        });
        jQuery("#btnColumnDelete").unbind("click").bind('click', Me.dodeleteCol);

    }

    this.dodeleteCol = function()
    {
        var rptid = Me.report_id;
        var fieldcode = $(this).attr('fieldid');
        if (rptid == "")
        {
            $("#filterslist tbody tr[fieldcode='" + fieldcode + "']").remove();
            $("#columnslist tbody tr[fieldcode='" + fieldcode + "']").remove();
            $("#collist tbody .up").html('Up');
            $("#collist tbody > tr:first .up").html('&nbsp;&nbsp;');
            $("#collist tbody .down").html('Down');
            $("#collist tbody > tr:last .down").html('');
            Me._roll.hide();
        }
        else
        {
            var param = {
                rptid: rptid,
                fieldcode: fieldcode
            };
            $.ajax({
                type: "POST",
                url: "/widgets/dashboard/custom-report/do-delete-col-report",
                data: param,
                success: function(data) {
                    if (data.success)
                    {
                        Me.loadfilterlist();
                        Me.loadcolumnlist();
                        Me._roll.hide();
                    }
                    else
                    {

                    }
                }
            });
        }

    }

    this.btnArrSave_click = function()
    {
        var rptid = $("#rptid").val();
        var colcode = [];
        $('#arrColTable tr').each(function() {
            if ($(this).find("td:first").length > 0) {
                var columnField = $(this.cells[0]).attr("fieldname");
                colcode.push(columnField);
            }
        });
        var param = {
            rptid: rptid,
            colcode: colcode
        };

        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/custom-report/do-save-arrange-report",
            data: param,
            success: function(data) {
                if (data.success)
                {
                    Me.loadcolumnlist();
                    Me._roll.hide();
                }

            }
        });
    }

    this.getArrangeColForNewReport = function()
    {
        var colcode = [];
        $('#collist tr').each(function() {
            if ($(this).find("td:first").length > 0) {
                var columnField = $(this.cells[0]).attr("fieldname");
                colcode.push(columnField);
            }
        });
        return colcode;
    }

    //------------------------------------------------------------
    this.reporttable_change = function()
    {
        if ($("#reporttable option:selected").val() != "")
        {
            Me.disabledaddcolandfilterbutton(false);
            jQuery("#addfilter").unbind("click").bind("click", Me.cmdaddfilter_click);
            jQuery("#addcolumn").unbind("click").bind("click", Me.addcolumn_click);
            jQuery("#arrcolumns").unbind("click").bind("click", Me.arrcolumns_click);
            $("#tblFilter tr").each(function() {
                if ($(this).attr("class") != "table_head")
                {
                    $(this).remove();
                }
            });
            $("#collist tr").each(function() {
                if ($(this).attr("class") != "table_head")
                {
                    $(this).remove();
                }
            });
        }
        else
        {
            Me.disabledaddcolandfilterbutton(true);
            jQuery("#addfilter").unbind("click");
            jQuery("#addcolumn").unbind("click");
            jQuery("#arrcolumns").unbind("click");
        }
    }

    //------------------------------------------------------------
    //general function
    //------------------------------------------------------------
    this.CheckValid = function()
    {
        var isCheckRunTime = $("#radRunTime").is(":checked");
        var returnvalue = true;
        if (isCheckRunTime)
        {
            returnvalue = Me.CheckValidForRunTime();
        }
        else
        {
            returnvalue = Me.CheckValidForTemplateOrRunTimeDefault();
        }

        return returnvalue;
    }

    //------------------------------------------------------------
    this.CheckValidForRunTime = function()
    {
        var datatype = $("#cbxFieldName option:selected").attr("vxdatatype");
        var returnvalue = true;
        switch (datatype.toLowerCase())
        {
            case 'datetime':
                var DataReference = $("#cbxRelativeDataReference option:selected").val();
                var NumericRangeFrom = "";
                var NumericRangeTo = "";
                if (DataReference == "AsEntered")
                {
                    NumericRangeFrom = $('#txtDateAsEnterForm').val();
                    NumericRangeTo = $('#txtDateAsEnterTo').val();
                }
                else
                {
                    NumericRangeFrom = $('#txtFromRange').val();
                    NumericRangeTo = $('#txtToRange').val();
                }


                if ((NumericRangeFrom == "") && (NumericRangeTo != ""))
                {
                    $("#divError")
                            .html("Please input value \"<em>Range</em>\"")
                            .css("color", "red")
                            .css("font-weight", "bold")
                            .css("display", "");

                    $('#txtFromRange').focus();
                    returnvalue = false;
                    break;
                }

                if ((NumericRangeFrom != "") && (NumericRangeTo == ""))
                {
                    $("#divError")
                            .html("Please input value \"<em>Range</em>\"")
                            .css("color", "red")
                            .css("font-weight", "bold")
                            .css("display", "");

                    $('#txtToRange').focus();
                    returnvalue = false;
                    break;
                }

                if (NumericRangeFrom != "" || NumericRangeTo != "")
                {
                    var valuerankfrom = parseInt(NumericRangeFrom);
                    if (NumericRangeFrom == "")
                        valuerankfrom = 0;
                    var valuerankto = parseInt(NumericRangeTo);
                    if (NumericRangeTo == "")
                        valuerankto = 0;

                    if (valuerankfrom > valuerankto)
                    {
                        $("#divError")
                                .html("Please input value \"<em>Range From</em>\" must less \"<em>Range To</em>\"")
                                .css("color", "red")
                                .css("font-weight", "bold")
                                .css("display", "");

                        $('#txtFromRange').focus();
                        returnvalue = false;
                        break;
                    }
                }
                break;
            case 'string':
                break;
            case 'boolean':
                break;
            case 'numeric':
                var NumericRangeFrom = $('#txtNumericRangeFrom').val();
                var NumericRangeTo = $('#txtNumericRangeTo').val();
                if (NumericRangeFrom != "" || NumericRangeTo != "")
                {
                    var valuerankfrom = parseInt(NumericRangeFrom);
                    if (NumericRangeFrom == "")
                        valuerankfrom = 0;
                    var valuerankto = parseInt(NumericRangeTo);
                    if (NumericRangeTo == "")
                        valuerankto = 0;

                    if (valuerankfrom >= valuerankto)
                    {
                        $("#divError")
                                .html("Please input value \"<em>Range From</em>\" must less \"<em>Range To</em>\"")
                                .css("color", "red")
                                .css("font-weight", "bold")
                                .css("display", "");

                        $('#txtNumericRangeFrom').focus();
                        returnvalue = false;
                        break;
                    }
                }
                break;
        }
        return  returnvalue;

    }

    //------------------------------------------------------------
    this.CheckValidForTemplateOrRunTimeDefault = function() {
        var returnvalue = true;
        var datatype = $("#cbxFieldName option:selected").attr("vxdatatype");

        switch (datatype.toLowerCase())
        {
            case 'datetime':
                var DataReference = $("#cbxRelativeDataReference option:selected").val();
                var NumericRangeFrom = "";
                var NumericRangeTo = "";
                if (DataReference == "AsEntered")
                {
                    NumericRangeFrom = $('#txtDateAsEnterForm').val();
                    NumericRangeTo = $('#txtDateAsEnterTo').val();
                }
                else
                {
                    NumericRangeFrom = $('#txtFromRange').val();
                    NumericRangeTo = $('#txtToRange').val();
                }

                if (DataReference == "AsEntered" || DataReference == "RunTimeDate")
                {
                    if ((NumericRangeFrom == "") && (NumericRangeTo == ""))
                    {
                        $("#divError")
                                .html("Please input value \"<em>Range</em>\"")
                                .css("color", "red")
                                .css("font-weight", "bold")
                                .css("display", "");

                        $('#txtFromRange').focus();
                        returnvalue = false;
                        break;
                    }
                    if ((NumericRangeFrom == "") && (NumericRangeTo != ""))
                    {

                        $("#divError")
                                .html("Please input value \"<em>Range</em>\"")
                                .css("color", "red")
                                .css("font-weight", "bold")
                                .css("display", "");

                        $('#txtFromRange').focus();
                        returnvalue = false;
                        break;
                    }
                    if ((NumericRangeFrom != "") && (NumericRangeTo == ""))
                    {
                        $("#divError")
                                .html("Please input value \"<em>Range</em>\"")
                                .css("color", "red")
                                .css("font-weight", "bold")
                                .css("display", "");

                        $('#txtToRange').focus();
                        returnvalue = false;
                        break;
                    }
                    if (NumericRangeFrom != "" || NumericRangeTo != "")
                    {
                        if (DataReference == "AsEntered")
                        {
                            var strdate1 = $("#txtDateAsEnterForm").val();
                            var strdate2 = $("#txtDateAsEnterTo").val();
                            if (strdate1 > strdate2)
                            {
                                $("#divError")
                                        .html("Please input value \"<em>Range From</em>\" must less \"<em>Range To</em>\"")
                                        .css("color", "red")
                                        .css("font-weight", "bold")
                                        .css("display", "");

                                $('#txtFromRange').focus();
                                returnvalue = false;
                                break;
                            }
                        }
                        else
                        {
                            var valuerankfrom = parseInt(NumericRangeFrom);
                            if (NumericRangeFrom == "")
                                valuerankfrom = 0;
                            var valuerankto = parseInt(NumericRangeTo);
                            if (NumericRangeTo == "")
                                valuerankto = 0;

                            if (valuerankfrom >= valuerankto)
                            {
                                $("#divError")
                                        .html("Please input value \"<em>Range From</em>\" must less \"<em>Range To</em>\"")
                                        .css("color", "red")
                                        .css("font-weight", "bold")
                                        .css("display", "");

                                $('#txtFromRange').focus();
                                returnvalue = false;
                                break;
                            }
                        }

                    }
                }
                break;
            case 'boolean':
                break;
            case 'string':
                var StringTypeValue = $("#txtStringTypeValue").val();
                var StringType = $("#cbxMatching option:selected").val();
                if (StringTypeValue == "" && StringType != "IsNot" && StringType != "SelectMultiple")
                {
                    $("#divError")
                            .html("Please input value \"<em>Value</em>\"")
                            .css("color", "red")
                            .css("font-weight", "bold")
                            .css("display", "");
                    $('#txtStringTypeValue').focus();
                    returnvalue = false;
                    break;
                }
                break;

            case 'numeric':
                var NumericBaseValue = $('#txtNumericBaseValue').val();
                var NumericRangeFrom = $('#txtNumericRangeFrom').val();
                var NumericRangeTo = $('#txtNumericRangeTo').val();

                if (NumericBaseValue == "")
                {

                    $("#divError")
                            .html("\"<em>Base Value</em>\" is required.")
                            .css("color", "red")
                            .css("font-weight", "bold")
                            .css("display", "");
                    $('#txtNumericBaseValue').focus();
                    returnvalue = false;
                    break;
                }
                if (NumericRangeFrom != "" || NumericRangeTo != "")
                {

                    var valuerankfrom = parseInt(NumericRangeFrom);
                    if (NumericRangeFrom == "")
                        valuerankfrom = 0;
                    var valuerankto = parseInt(NumericRangeTo);
                    if (NumericRangeTo == "")
                        valuerankto = 0;

                    if (valuerankfrom >= valuerankto)
                    {
                        $("#divError")
                                .html("Please input value \"<em>Range From</em>\" must less \"<em>Range To</em>\"")
                                .css("color", "red")
                                .css("font-weight", "bold")
                                .css("display", "");

                        $('#txtNumericRangeFrom').focus();
                        returnvalue = false;
                        break;
                    }
                }

                break;
        }
        return  returnvalue;
    }

    //------------------------------------------------------------
    this.getDataValue = function() {
        var datatype = $("#cbxFieldName option:selected").attr("vxdatatype");
        var fieldcodeval = $("#cbxFieldName option:selected").val();
        var data = null;
        var selectedFilterAt = 0;
        var RunTime = $("#radRunTime").is(":checked");
        var Template = $("#radTemplate").is(":checked");
        var RunTimewithDefaultValue = $("#radRunTimewithDefaultValue").is(":checked");
        if (Template)
        {
            selectedFilterAt = 1;
        }
        else if (RunTime)
        {
            selectedFilterAt = 2;
        }
        else if (RunTimewithDefaultValue)
        {
            selectedFilterAt = 3;
        }
        var type = datatype.toLowerCase();
        var customtype = "";
        if (type == 'tinyint') {
            customtype = "boolean";
        } else if ((type.indexOf("decimal") >= 0) || type == 'float' || type == 'int' || type == 'money') {
            customtype = "numeric";
        } else if (type == "datetime" || type == "string") {
            customtype = type;
        }

        switch (customtype)
        {
            case 'datetime':
                if ($("#cbxRelativeDataReference option:selected").val() == "AsEntered")
                {
                    data = {
                        fieldcode: fieldcodeval,
                        filter_at: selectedFilterAt,
                        datetime_relative_data_ref: $("#cbxRelativeDataReference option:selected").val(),
                        datetime_range_from: $("#txtDateAsEnterForm").val(),
                        datetime_range_to: $("#txtDateAsEnterTo").val()
                    };
                }
                else
                {
                    data = {
                        fieldcode: fieldcodeval,
                        filter_at: selectedFilterAt,
                        datetime_relative_data_ref: $("#cbxRelativeDataReference option:selected").val(),
                        datetime_range_int_from: $("#txtFromRange").val(),
                        datetime_range_int_to: $("#txtToRange").val()
                    };
                }

                break;
            case 'string':
                if ($("#cbxMatching option:selected").val() == 'SelectMultiple')
                {
                    var SelectMultiple = [];
                    var i = 0;
                    $(".SelectMultiple").each(
                            function() {
                                if ($(this).is(":checked"))
                                {
                                    SelectMultiple[i] = $(this).attr("val");
                                    i++;
                                }
                            });

                    if (i == 0)
                    {
                        SelectMultiple = "";
                    }
                    data = {
                        fieldcode: fieldcodeval,
                        filter_at: selectedFilterAt,
                        string_matching: $("#cbxMatching option:selected").val(),
                        string_value: $("#txtStringTypeValue").val(),
                        SelectMultiple: SelectMultiple
                    };
                }
                else
                {
                    data = {
                        fieldcode: fieldcodeval,
                        filter_at: selectedFilterAt,
                        string_matching: $("#cbxMatching option:selected").val(),
                        string_value: $("#txtStringTypeValue").val()
                    };
                }


                break;
            case 'boolean':
                var BooleanTrue = $("#radBooleanTrue").is(":checked");
                var BooleanFalse = $("#radBooleanFalse").is(":checked");
                var BooleanAny = $("#radBooleanAny").is(":checked");
                var BooleanValue = BooleanTrue ? "t" : BooleanFalse ? 'f' : 'a';

                data = {
                    fieldcode: fieldcodeval,
                    filter_at: selectedFilterAt,
                    bool_value: BooleanValue
                };
                break;
            case 'numeric':
                data = {
                    fieldcode: fieldcodeval,
                    filter_at: selectedFilterAt,
                    numeric_base_value: $("#txtNumericBaseValue").val(),
                    numeric_range_from: $("#txtNumericRangeFrom").val(),
                    numeric_range_to: $("#txtNumericRangeTo").val()
                };
                break;

        }
        return data;
    }

    this.getColDataValueForNewReport = function()
    {
        var datas = [];
        $("#columnslist tbody tr").each(
                function() {
                    var data = null;

                    if (typeof($(this).attr("fieldcode")) != "undefined")
                    {
                        var fieldcode = $(this).attr("fieldcode");
                        var fieldname = $(this).attr("fieldname");
                        data = {
                            fieldcode: fieldcode,
                            fieldname: fieldname
                        };
                        datas.push(data);
                    }
                });
        return datas;
    }

    //------------------------------------------------------------
    this.removeItem = function(name, obj, itemid)
    {
        $("#" + obj + " option[value='" + itemid + "']").remove();
        if ($("#" + obj + " option").length == 0)
        {
            if (name == "column")
            {
                Me.isDisabledbutton(true, "btnAddSaveAddMore", "btnAddSave");
                $('#btnAddSave').unbind("click");
                $('#btnAddSaveAddMore').unbind("click");
            }
            else if (name == "filter")
            {
                Me.isDisabledbutton(true, "cmdFilterSaveAddMore", "cmdFilterSave");
                $('#cmdFilterSaveAddMore').unbind("click");
                $('#cmdFilterSave').unbind("click");
            }
        }
        else
        {
            $("#" + obj).change();
            if (name == "column")
            {
                Me.isDisabledbutton(false, "btnAddSaveAddMore", "btnAddSave");
            }
            else if (name == "filter")
            {
                Me.isDisabledbutton(false, "cmdFilterSaveAddMore", "cmdFilterSave");
            }
        }
    }

    //------------------------------------------------------------
    this.isDisabledbutton = function(isdisabled, button1, button2)
    {
        if (isdisabled)
        {
            $('#' + button1).attr("disabled", true);
            $('#' + button1).removeClass("link_button");
            $('#' + button1).addClass("link_button_disabled");
            $('#' + button1).css('color', '#696969');
            $('#' + button2).attr("disabled", true);
            $('#' + button2).removeClass("link_button");
            $('#' + button2).addClass("disabled");
        }
        else
        {
            $('#' + button2).removeAttr("disabled");
            $('#' + button1).removeAttr("disabled");
        }
    }

    //------------------------------------------------------------
    this.loadTempFields = function()
    {
        var fieldcode = "";
        var i = 0;
        $("#columnslist tbody tr").each(
                function() {
                    if (typeof($(this).attr("fieldcode")) != "undefined")
                    {
                        fieldcode += "&fieldcode[]=" + $(this).attr("fieldcode")
                                + "&fieldname[]=" + $(this).attr("fieldname")
                                + "&fieldtype[]=" + $(this).attr("fieldtype");
                    }
                    i++;
                });
        return  fieldcode;
    }

    //------------------------------------------------------------
    this.loadTempFilterFields = function()
    {
        var fieldcode = "";
        var i = 0;
        $("#columnslist tbody tr").each(
                function() {
                    if (typeof($(this).attr("fieldcode")) != "undefined")
                    {
                        fieldcode += "&fieldcode[]=" + $(this).attr("fieldcode")
                                + "&fieldname[]=" + $(this).attr("fieldname")
                                + "&fieldtype[]=" + $(this).attr("fieldtype");
                    }
                    i++;
                });
        $("#filterslist tbody tr").each(
                function() {
                    if (typeof($(this).attr("fieldcode")) != "undefined")
                    {
                        fieldcode += "&filtercode[]=" + $(this).attr("fieldcode");
                    }
                });
        return  fieldcode;
    }

    //------------------------------------------------------------
    this.loadTempFilterValue = function(el)
    {
        var fieldcode = "";
        fieldcode = "&fieldcode=" + $(el).attr("fieldcode")
                + "&fieldid=" + $(el).attr("fieldid")
                + "&fieldtype=" + $(el).attr("fieldtype")
                + "&Filterby=" + $(el).attr("filterby")
                + "&fromval=" + $(el).attr("fromval")
                + "&toval=" + $(el).attr("toval")
                + "&FilterAt=" + $(el).attr("selectedfilterat")
                + "&multiselect=" + Me.MultiselectList[$(el).attr("fieldcode")];
        return  fieldcode;
    }

    //------------------------------------------------------------
    this.getDefaultval = function(datatype, filterby, fromval, toval)
    {
        var result = "";
        if (datatype == "datetime")
        {
            if (filterby == "AsEntered")
            {
                if (fromval != "")
                {
                    result = ">= '" + fromval + "'";
                }

                if (toval != "")
                {
                    if (result != "")
                    {
                        result += " AND ";
                    }
                    result += "<= '" + toval + "'";
                }
            }
            else if (filterby == "CurrentQuarter")
            {
                result = "Current Quater";
            }
            else if (filterby == "CurrentYear")
            {
                result = "Current Year";
            }
            else if (filterby == "LastQuarter")
            {
                result = "Last Quater";
            }
            else if (filterby == "LastYear")
            {
                result = "Last Year";
            }
            else if (filterby == "RunTimeDate")
            {
                result = "Run Time Date";
            }
        }
        else if (datatype == "string")
        {
            if (filterby == "ExactMatch")
            {
                result = "= '" + fromval + "'";
            } else if (filterby == "BeginsWith") {
                result = "BEGINS_WITH '" + fromval + "'";
            } else if (filterby == "EndsWith") {
                result = "ENDS_WITH '" + fromval + "'";
            } else if (filterby == "Contains")
            {
                result = "CONTAINS '" + fromval + "'";
            } else if (filterby == "IsNot")
            {
                result = "IS_NOT '" + fromval + "'";
            }
            else if (filterby == "SelectMultiple")
            {
                result = "SELECT_MULTIPLE";
            }
        }
        else if (datatype == "boolean")
        {
            if (filterby == "t") {
                result = "True";
            } else if (filterby == "f") {
                result = "False";
            } else if (filterby == "a") {
                result = "Any";
            }
        }
        else if (datatype == "numeric")
        {
            var fromvalue = Math.round(fromval).toFixed(0);
            var tovalue = Math.round(toval).toFixed(0);

            if (fromval != "")
            {
                result = ">= " + fromvalue + ""
            }

            if (toval != "")
            {
                if (result != "")
                {
                    result += " AND ";
                }

                result += "<= " + tovalue + "";

            }
        }
        return result;
    }

    //------------------------------------------------------------
    this.disabledaddcolandfilterbutton = function(isdisable)
    {
        if (isdisable)
        {
            $('#addcolumn').attr("disabled", true);
            $('#addcolumn').removeClass("link_button");
            $('#addcolumn').addClass("link_button_disabled");
            $('#addcolumn').css('color', '#696969');

            $('#addfilter').attr("disabled", true);
            $('#addfilter').removeClass("link_button");
            $('#addfilter').addClass("link_button_disabled");
            $('#addfilter').css('color', '#696969');
        }
        else
        {
            $('#addcolumn').removeAttr("disabled");
            $('#addcolumn').removeClass("link_button_disabled");
            $('#addcolumn').addClass("link_button");
            $('#addcolumn').css('color', '');

            $('#addfilter').removeAttr("disabled");
            $('#addfilter').removeClass("link_button_disabled");
            $('#addfilter').addClass("link_button");
            $('#addfilter').css('color', '');
        }
    }

    //------------------------------------------------------------
    this.btnGenerate_click = function()
    {
        var rptid = $("#reportid").val();
        var grpid = 1;

        var arrangeList = Me.getArrangeColForNewReport();
        var reportname = $("#reportname").val();
        var param = {rptid: rptid, grpid: grpid, arrangelist: arrangeList, reportname: reportname};

        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/custom-report/do-generate-report",
            data: param,
            success: function(data) {
                if (data.success)
                {
                    window.location = 'reportlist.php?v=' + Me.company;
                }
                else
                {
                    alert(data.error);
                }
            }
        });
    }

    this.cmdSaveAndGenerate_click = function()
    {
        var rptid = $("#reportid").val();
        var grpid = 1;
        var arrangeList = Me.getArrangeColForNewReport();
        var reportname = jQuery.trim(jQuery("#reportname").val());

        if (Me.report_id == "")
        {
            jQuery("#errorDiv").html("");
            var dataerr = Me.getDataFrom();
            if (dataerr["msg"].length > 0) {
                Me.validForm(dataerr["msg"]);
                return false;
            }

            var reporttable = jQuery.trim(jQuery("#reporttable option:selected").val());

            var colList = "";
            colList = Me.getColDataValueForNewReport();

            var filterList = "";
            filterList = Me.getFilterDataValueForCreateReport();
            var param = {
                reportname: reportname,
                reporttable: reporttable,
                company: Me.company,
                collist: colList,
                filterlist: filterList,
                arrangelist: arrangeList
            };

            $.ajax({
                type: "POST",
                url: "/widgets/dashboard/custom-report/do-create-report",
                dataType: 'json',
                cache: true,
                async: true,
                data: param,
                success: function(data)
                {
                    if (data.success)
                    {
                        window.location = '../reportview_new.php?rptid=' + data.repID + '&v=' + Me.company;
                    }
                    else
                    {
                        alert(data.error);
                    }
                }
            });
        }
        else
        {
            var param = {
                rptid: rptid,
                grpid: grpid,
                arrangelist: arrangeList,
                reportname: reportname
            };

            $.ajax({
                type: "POST",
                url: "/widgets/dashboard/custom-report/do-generate-report",
                data: param,
                success: function(data) {
                    if (data.success)
                    {
                        window.location = '../reportview_new.php?rptid=' + rptid + '&v=' + Me.company;
                    }
                    else
                    {
                        alert(data.error);
                    }
                },
                error: function(data) {

                }
            });
        }
    }

    this.cmdUp_click = function()
    {
        //var row = $(this).closest("tr");
        var row = $(this).closest("tr");
        if (row.prev().length > 0) {
            row.insertBefore(row.prev());
        }
        $("#collist tbody .up").html('Up');
        $("#collist tbody > tr:first .up").html('&nbsp;&nbsp;');
        $("#collist tbody .down").html('Down');
        $("#collist tbody > tr:last .down").html('');
        return false;
    }

    this.cmdDown_click = function()
    {
        var row = $(this).closest("tr");
        if (row.next().length > 0) {
            row.insertAfter(row.next());
        }
        $("#collist tbody .up").html('Up');
        $("#collist tbody > tr:first .up").html('&nbsp;&nbsp;');
        $("#collist tbody .down").html('Down');
        $("#collist tbody > tr:last .down").html('');
        return false;
    }

    //------------------------------------------------------------
    this.bindreportevent = function()
    {
        Me.loadreport(null, 0);
        $("#frmfilter select").each(
                function() {
                    $(this).change(function() {
                        Me.issavetemplate();
                    });
                }
        );

        $("#frmfilter input").each(
                function() {
                    $(this).change(function() {
                        Me.issavetemplate();
                    });
                }
        );
        $('<iframe id="printf" name="printf" style="display:none;" />').appendTo($('body'));
        $(".editSelectMultiple").unbind('click').bind("click", Me.editMultiselect);
        $("#group1").unbind('click').bind("click", Me.showhide);
        $("#group2").unbind('click').bind("click", Me.showhide);
        $("#group3").unbind('click').bind("click", Me.showhide);
        $("#sort1").unbind('change').bind("change", Me.sort_onchange);
        $("#sort2").unbind('change').bind("change", Me.sort_onchange);
        $("#sort3").unbind('change').bind("change", Me.sort_onchange);
        $(".cbxmatching").unbind('change').bind("change", Me.cbxMatching_onChange);

        $("#resetfilter").unbind('click').bind("click", Me.resetfilter);
        $("#clearfilter").unbind('click').bind("click", Me.clearfilter);
        $("#resetsort").unbind('click').bind("click", Me.resetsort);
        $("#clearsort").unbind('click').bind("click", Me.clearsort);
        $("#resetformat").unbind('click').bind("click", Me.resetformat);
        $("#clearformat").unbind('click').bind("click", Me.clearformat);
        $("#cmdReports").unbind('click').bind("click", Me.loadreport);
    }

    this.issavetemplate = function()
    {
        $("#cmdsavetemplatefilter").unbind("click").bind("click", Me.savetemplatefilter);
        $('#cmdsavetemplatefilter').removeAttr("disabled");
        $('#cmdsavetemplatefilter').removeClass("link_button_disabled");
        $('#cmdsavetemplatefilter').addClass("link_button");
        $('#cmdsavetemplatefilter').css('color', '');
    }

    this.editMultiselect = function()
    {
        var reporttable = $(this).attr('rpttable');
        var v = $(this).attr('v');
        html = '\
            <div>\n\
                <div id="ContainerID_EditMultiSelect" style=\"max-height:200px;overflow:auto;\">\n\
                </div>\n\
                <div style="padding-top:10px">\n\
                    <div style="text-align:center;" >\n\
                    <input type="button" id="cmdEditMultiSelectCancel" class="link_button link_button_popup" name="cmdFilterCancel" value="Cancel">\n\
                    <input type="button" id="cmdEditMultiSelectSaveAndApply" class="link_button" name="cmdFilterSaveAddMore" value="Save &amp; Apply Filter">\n\
                    <input type="button" id="cmdEditMultiSelectSave" class="link_button link_button_popup" name="cmdFilterSave" value="Save">\n\
            </div>\n\
                </div>';

        var opt = {};
        opt.width = 440;
        opt.height = '';
        opt.position = 'middle';
        opt.paddingTop = -200;
        opt.title = '<div style="text-align:center;">Edit Filter</div>';
        opt.body = html;
        opt.zindex = 999;
        Me._roll.showNotAjax(null, null, opt);
        var rptid = $("#rptid").val();
        var template = $("#txtTemplateName").val();
        var data = "rptid=" + rptid + "&v=" + v + "&template=" + template + "&reporttable=" + reporttable + "&reportname=" + template;

        data += Me.loadTempFilterValue(this);

        $.ajax({
            type: "POST",
            url: "/reports_test/custom/custom_report_addfilter.php",
            data: data,
            success: function(html) {
                if (html != "")
                {
                    jQuery("#ContainerID_EditMultiSelect").html(html);

                    $("#cmdEditMultiSelectCancel").unbind('click').bind('click', function() {
                        Me._roll.hide();
                    });
                    $("#cmdEditMultiSelectSaveAndApply").unbind('click').bind('click', Me.EditMultiSelectSaveAndApply);
                    $('#cmdEditMultiSelectSave').unbind("click").bind("click", Me.EditMultiSelectSave);

                }
            }
        });
    }

    var loaderVisible = false;
    this.showLoader = function() {
        if (!loaderVisible) {
            loaderVisible = true;
            var content = "<div id='loader'>Loading...</div>";
            $("<div></div>").fancybox(
                    {
                        'autoDimensions': true,
                        'showCloseButton': false,
                        'hideOnOverlayClick': false,
                        'content': content
                    }
            ).trigger('click');
        }
    }

    this.hideLoader = function() {
        setTimeout(jQuery.fancybox.close, 500);
        loaderVisible = false;
    }

    this.loadreport = function(event, rt)
    {
        var _url = "/widgets/dashboard/custom-report/load-report/";
        //--- page,size
        var page = $(this).attr('page');
        var size = $("#pagesize").val();
        if (typeof(page) == 'undefined')
            page = 1;
        if (typeof(size) == 'undefined')
            size = 50;
        if (typeof(rt) == 'undefined')
            rt = 1;
        var submitData = $('#frmfilter').serialize();
        submitData += '&page=' + page + '&size=' + size + '&rt=' + rt;

        for (i = 1; i < 4; i++)
        {
            var sort = $("#sort" + i + " option:selected").val();
            var sortdir = $("#sortdir" + i + " option:selected").val();
            if (sort != "")
            {
                submitData += '&Sort' + i + '__fieldcode=' + sort;
                submitData += '&Sort' + i + '__sortdir=' + sortdir;
            }
        }

        Me.showLoader();
        //---
        $.ajax({
            url: _url,
            dataType: 'html',
            data: submitData,
            cache: false,
            type: 'POST',
            context: this,
            success: function(data, status, xhr)
            {
                Me.hideLoader();
                $("#reportcontent").html("");
                $("#reportcontent").html(data);
                $("#cmddownload").unbind("click").bind("click", Me.downloadreport);
                $("#cmdPrint").unbind("click").bind("click", Me.downloadreportfile);
                $(".paginatorLinkActiv").unbind("click").bind("click", Me.loadreport);
            },
            error: function(xhr, status, error)
            {
            }
        });
    }

    this.downloadreportfile = function()
    {
        var Ext = 'Printter';
        var para = $('#frmfilter').serialize();

        $.ajax({
            url: 'ExportReportView1ToFile.php?Ext=' + Ext + '&' + para,
            dataType: 'html',
            data: '',
            cache: false,
            type: 'POST',
            context: this,
            success: function(data, status, xhr) {
                jQuery("iframe").contents().find('body').html(data);
                window.frames["printf"].print()
            },
            error: function(xhr, status, error) {
            }
        });
    }

    this.downloadreport = function()
    {
        var html = '\
        <div>\n\
            <div id="ContainerID_frmFilter">\n\
                <table>\n\
                    <tr>\n\
                        <td>\n\
                            <input type="radio" class="checkdownload" id="cmdDownloadexcel" value="csv" checked>\n\
                        </td>\n\
                        <td>Excel CSV</td>\n\
                    </tr>\n\
                    <tr>\n\
                        <td>\n\
                            <input type="radio" class="checkdownload" id="cmdDownloadpdf" value="pdf">\n\
                        </td>\n\
                        <td>PDF</td>\n\
                    </tr>\n\
                    <tr>\n\
                        <td>\n\
                            <input type="radio" class="checkdownload" id="cmdDownloadxml" value="xml">\n\
                        </td>\n\
                        <td>XML</td>\n\
                    </tr>\n\
                </table>\n\
            </div>\n\
            <div style="text-align:center;">\n\
                <input type="button" id="cmdDownloadCancel" class="link_button link_button_popup" value="Cancel">\n\
                <input type="button" id="cmddownloadfile" class="link_button link_button_popup" value="Download">\n\
            </div>\n\
        </div>';
        var opt = {};
        opt.width = 300;
        opt.height = '';
        opt.position = 'middle';
        opt.paddingTop = -200;
        opt.title = '<div style="text-align:center;">Download Options</div>';
        opt.body = html;
        opt.zindex = 999;
        var _roll = new FSPopupRoll();
        _roll.showNotAjax(null, null, opt);
        $("#cmdDownloadCancel").unbind('click').bind('click', function()
        {
            _roll.hide();
        });
        $("#cmddownloadfile").unbind('click').bind('click', function()
        {
            var Ext = "";
            if ($("#cmdDownloadexcel").is(":checked"))
            {
                Ext = "csv";
            }
            else if ($("#cmdDownloadpdf").is(":checked"))
            {
                Ext = "pdf";
            }
            else if ($("#cmdDownloadxml").is(":checked"))
            {
                Ext = "xml";
            }
            var para = $('#frmfilter').serialize();
            document.location = 'ExportReportView1ToFile.php?Ext=' + Ext + '&' + para + "&rptid=" + $("#rptid").val();
        })
        $(".checkdownload").unbind('click').bind('click', function()
        {
            if ($(this).is(":checked"))
            {
                $(".checkdownload").removeAttr("checked");
                $(this).attr('checked', 'true');
            }

        });
    }

    this.clearfilter = function()
    {
        $("#showhide1 select").each(function() {
            if (!$(this).attr("disabled")) {
                if ($(this).find("option:selected").val() != "SelectMultiple")
                {
                    $(this).val("");
                }
            }
        });
        $("#showhide1 input").each(function() {
            if (!$(this).attr("disabled"))
            {
                if ($(this).attr("type") == 'text')
                {
                    $(this).val("");
                }
                if ($(this).attr("type") == 'radio')
                {
                    if ($(this).val() == 'a')
                    {
                        $(this).attr("checked", "checked");
                    }
                }
            }
        }
        );
        Me.issavetemplate();
    }

    this.resetfilter = function()
    {
        var param = {
            reportid: $("#rptid").val()
        };
        $.ajax({
            url: 'custom/custom_report_resetfilter.php',
            dataType: 'html',
            data: param,
            cache: false, type: 'POST',
            context: this,
            success: function(data, status, xhr) {
                $("#showhide1").html(data);
                $(".cbxmatching").unbind('change').bind("change", Me.cbxMatching_onChange);
                $(".editSelectMultiple").unbind('click').bind("click", Me.editMultiselect);
            },
            error: function(xhr, status, error) {
            }
        });
    }

    this.clearsort = function() {
        $("#showhide2 select").each(function() {
            $("#" + $(this).attr("id") + " option").each(function() {
                $(this).removeAttr("disabled");
            });
            $(this).val("");
        });
        Me.issavetemplate();
    }

    this.resetsort = function() {
        var param = {
            reportid: $("#rptid").val()
        };
        $.ajax({
            url: 'custom/custom_report_resetsort.php',
            dataType: 'html',
            data: param,
            cache: false, type: 'POST',
            context: this,
            success: function(data, status, xhr) {
                $("#showhide2").html(data);
                $("#sort1").unbind('change').bind("change", Me.sort_onchange);
                $("#sort2").unbind('change').bind("change", Me.sort_onchange);
                $("#sort3").unbind('change').bind("change", Me.sort_onchange);
            },
            error: function(xhr, status, error) {
            }
        });
    }

    this.clearformat = function() {
        $("#showhide3 input").each(function() {
            if (!$(this).attr("disabled"))
            {
                $(this).removeAttr("checked");
            }
        });
        Me.issavetemplate();
    }

    this.resetformat = function() {
        var param = {
            report_id: $("#rptid").val()
        };
        $.ajax({
            url: 'custom/custom_report_grouplist.php',
            dataType: 'html',
            data: param,
            cache: false, type: 'POST',
            context: this,
            success: function(data, status, xhr) {
                $("#showhide3").html(data);
                $("#frmfilter input").each(
                        function() {
                            $(this).change(function() {
                                Me.issavetemplate();
                            });
                        }
                );
            },
            error: function(xhr, status, error) {
            }
        });
    }

    this.savetemplatefilter = function() {
        var FilterDataValue = Me.getFilterDataValueForNewReport();
        var SortDataValue = Me.getSortDataValueForNewReport();
        var FormatDataValue = Me.getFormatDataValueForNewReport();

        var param = {
            reportid: $("#rptid").val(),
            company: '<?= $v ?>',
            filterlist: FilterDataValue,
            sortlist: SortDataValue,
            formatlist: FormatDataValue
        };
        $.ajax({
            url: '/widgets/dashboard/custom-report/do-save-report-filters/',
            dataType: 'html',
            data: param,
            cache: false, type: 'POST',
            context: this,
            success: function(data, status, xhr) {
                var content = "<div>Template Saved</div>";
                $("<div></div>").fancybox(
                        {
                            'autoDimensions': true,
                            'showCloseButton': false,
                            'hideOnOverlayClick': false,
                            'content': content,
                            'onComplete': function() {
                                setTimeout(function() {
                                    $.fancybox.close();
                                }, 1000);
                            },
                        }
                ).trigger('click');

            },
            error: function(xhr, status, error) {
            }
        });
    }

    //------------------------------------------------------------
    this.getFilterDataValueForCreateReport = function()
    {
        var datas = [];

        $("#filterslist tbody tr").each(
                function() {
                    var data = null;
                    if (typeof($(this).attr("fieldcode")) != "undefined")
                    {
                        var datatype = $(this).attr("fieldtype");
                        var fieldcodeval = $(this).attr("fieldcode");
                        var runtime = $(this).attr("runtime");
                        var filterby = $(this).attr("filterby");
                        var fromval = $(this).attr("fromval");
                        var toval = $(this).attr("toval");

                        switch (datatype.toLowerCase())
                        {
                            case 'datetime':
                                data = {
                                    fieldcode: fieldcodeval,
                                    filter_at: runtime,
                                    datetime_relative_data_ref: filterby,
                                    datetime_range_from: fromval,
                                    datetime_range_to: toval
                                };
                                break;
                            case 'string':
                                var SelectMultiple = [];
                                if (filterby == "SelectMultiple")
                                {
                                    var i = 0;
                                    $.each(Me.MultiselectList[fieldcodeval]["data"],
                                            function(index, value) {

                                                SelectMultiple[i] = value['val'];
                                                i++;
                                            })
                                }

                                data = {
                                    fieldcode: fieldcodeval,
                                    filter_at: runtime,
                                    string_matching: filterby,
                                    string_value: fromval,
                                    SelectMultiple: SelectMultiple
                                };
                                break;
                            case 'boolean':
                                data = {
                                    fieldcode: fieldcodeval,
                                    filter_at: runtime,
                                    bool_value: filterby
                                };
                                break;
                            case 'numeric':
                                data = {
                                    fieldcode: fieldcodeval,
                                    filter_at: runtime,
                                    numeric_base_value: 0,
                                    numeric_range_from: fromval,
                                    numeric_range_to: toval
                                };
                                break;
                        }
                        datas.push(data);
                    }
                });

        return datas;
    }

    this.getFilterDataValueForNewReport = function()
    {
        var datas = [];
        $("#tblFilter tbody tr").each(function() {
            var data = null;
            var filteridval = $(this).attr("filterid");
            var datatypeval = $(this).attr("datatype");
            var fieldcodeval = $(this).attr("fieldcode");
            var fieldtype = "";
            var boolval = "";
            if (datatypeval == 'tinyint')
            {
                fieldtype = "boolean";
            }
            else if ((datatypeval.indexOf("decimal") >= 0) || datatypeval == 'float' || datatypeval == 'int' || datatypeval == 'money')
            {
                fieldtype = "numeric";
            }
            else if (datatypeval == "datetime" || datatypeval == "string")
            {
                fieldtype = datatypeval;
            }
            switch (fieldtype.toLowerCase()) {
                case 'datetime':
                    data = {
                        filterid: filteridval,
                        fieldcode: fieldcodeval,
                        datetime_range_from: $("#Filter" + filteridval + "__datetime_range_from").val(),
                        datetime_range_to: $("#Filter" + filteridval + "__datetime_range_to").val()};
                    break;
                case 'string':
                    data = {
                        filterid: filteridval,
                        fieldcode: fieldcodeval,
                        string_matching: $("#Filter" + filteridval + "__string_matching option:selected").val(),
                        string_value: $("#Filter" + filteridval + "__string_value").val()};
                    break;
                case 'boolean':

                    if ($("#trueFilter" + filteridval + "__bool_value").is(":checked"))
                    {
                        boolval = "t";
                    }
                    else if ($("#falseFilter" + filteridval + "__bool_value").is(":checked"))
                    {
                        boolval = "f";
                    }
                    else
                    {
                        boolval = "a";
                    }
                    data =
                            {
                                filterid: filteridval,
                                fieldcode: fieldcodeval,
                                bool_value: boolval
                            };
                    break;
                case 'numeric':
                    data =
                            {
                                filterid: filteridval,
                                fieldcode: fieldcodeval,
                                numeric_base_value: 0,
                                numeric_range_from: $("#Filter" + filteridval + "__numeric_range_from").val(), numeric_range_to: $("#Filter" + filteridval + "__numeric_range_to").val()
                            };
                    break;
            }
            datas.push(data);
        });
        return datas;
    }

    this.getSortDataValueForNewReport = function()
    {
        var datas = [];
        datas =
                {
                    Sort1__fieldcode: $("#sort1 option:selected").val(),
                    Sort1__sortdir: $("#sortdir1 option:selected").val(),
                    Sort2__fieldcode: $("#sort2 option:selected").val(),
                    Sort2__sortdir: $("#sortdir2 option:selected").val(),
                    Sort3__fieldcode: $("#sort3 option:selected").val(),
                    Sort3__sortdir: $("#sortdir3 option:selected").val()
                };
        return datas;
    }

    this.getFormatDataValueForNewReport = function()
    {
        var datas = [];

        $("#groupTable tbody input").each(function() {

            var data = null;
            if ($(this).is(":checked")) {
                data =
                        {
                            fieldcode: $(this).attr("name"),
                            fieldval: 1,
                        };
            } else {
                data =
                        {
                            fieldcode: $(this).attr("name"),
                            fieldval: 0,
                        };

            }
            if (data != null)
            {
                datas.push(data);
            }
        });
        return datas;
    }

    this.sort_onchange = function()
    {
        var selectedval = $(this).val();
        var cursort = $("#hidsort" + $(this).attr("curid")).val();
        var anosort1 = "sort" + $(this).attr("anosort1");
        var anosort2 = "sort" + $(this).attr("anosort2");

        if (selectedval != "")
        {
            $("#" + anosort1 + " option").each(function() {
                if ($(this).val() == cursort) {
                    $(this).removeAttr("disabled");
                }
                if ($(this).val() == selectedval) {
                    $(this).attr("disabled", "true");
                }
            });
            $("#" + anosort2 + " option").each(function() {
                if ($(this).val() == cursort) {
                    $(this).removeAttr("disabled");
                }
                if ($(this).val() == selectedval) {
                    $(this).attr("disabled", "true");
                }
            });
        }
        else
        {
            $("#" + anosort1 + " option").each(function() {
                if ($(this).val() == cursort) {
                    $(this).removeAttr("disabled");
                }
            });
            $("#" + anosort2 + " option").each(function() {
                if ($(this).val() == cursort) {
                    $(this).removeAttr("disabled");
                }
            });
        }
        $("#hidsort" + $(this).attr("curid")).val(selectedval);
        Me.issavetemplate();
    }

    this.showhide = function()
    {
        var id = $(this).attr("groupid");
        if ($("#showhide" + id).css("display") == "none")
        {
            $("#showhide" + id).css("display", "");
            $("#group" + id).attr("src", "/widgets/images/arrows-up5.png")
        }
        else
        {
            $("#showhide" + id).css("display", "none");
            $("#group" + id).attr("src", "/widgets/images/arrows-down5.png")
        }
    }

    this.cbxMatching_onChange = function()
    {
        var objparent = $(this).parent();
        if ($(this).val() == "SelectMultiple")
        {
            $(objparent).find("input").css("display", "none");
            $(objparent).find("a").css("display", "");
        }
        else
        {
            $(objparent).find("input").css("display", "");
            $(objparent).find("a").css("display", "none");
        }
        Me.issavetemplate();
        var fieldid = $(objparent).find("a").attr('fieldid');
        $(objparent).find("input").each(
                function() {
                    if ($(this).attr("name") == "Filter" + fieldid + "__SelectMultiple[]")
                        $(this).remove();
                });
        $(".editSelectMultiple").unbind('click').bind("click", Me.editMultiselect);
    }

    this.EditMultiSelectSaveAndApply = function()
    {
//        var colcode = $("#cbxFieldName option:selected").val();
        var fieldid = "";
        if ($("#fieldid").val() != "")
            fieldid = $("#fieldid").val();

        var data = Me.getDataValue();
        var key_value_array = [];
        key_value_array.push(data);
        var param = {
            report_id: Me.report_id,
            fieldid: fieldid,
            key_value_array: key_value_array
        };
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/custom-report/do-add-filter-report",
            data: param,
            success: function(data) {
                if (data.success)
                {
                    Me._roll.hide();
                    Me.renewMultiselectList(fieldid, key_value_array[0]['SelectMultiple']);
                    Me.loadreport(null, 0);
                }
                else
                {
                    //error
                }
            }
        });
    }

    this.renewMultiselectList = function(fieldid, Multiselectlist)
    {
        $("#tblFilter input").each(
                function() {
                    if ($(this).attr("name") == "Filter" + fieldid + "__SelectMultiple[]")
                        $(this).remove();
                });
        var el = $("#Filter" + fieldid + "__string_matching").parent();
        var strReturn = "";
        $.each(Multiselectlist, function(key, value) {
            strReturn += "<input type='hidden' name='Filter" + fieldid + "__SelectMultiple[]' value='" + value + "'>";
        });
        el.append(strReturn);
    }

    this.EditMultiSelectSave = function()
    {
        //var colcode = $("#cbxFieldName option:selected").val();
        var fieldid = "";
        if ($("#fieldid").val() != "")
            fieldid = $("#fieldid").val();

        var data = Me.getDataValue();
        var key_value_array = [];
        key_value_array.push(data);
        var param = {
            report_id: Me.report_id,
            fieldid: fieldid,
            key_value_array: key_value_array
        };
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/custom-report/do-add-filter-report",
            data: param,
            success: function(data) {
                if (data.success)
                {
                    Me._roll.hide();
                    Me.renewMultiselectList(fieldid, key_value_array[0]['SelectMultiple']);
                }
                else
                {
                    //error
                }
            }
        });
    }
    //end 606
    //------------------------------------------------------------
    this.init = function() {
        jQuery("#up").unbind("click", Me.cmdup_click).bind("click", Me.cmdup_click);
        jQuery("#uptop").unbind("click", Me.cmdaddall_click).bind("click", Me.cmuptop_click);
        jQuery("#down").unbind("click", Me.cmdremove_click).bind("click", Me.cmddown_click);
        jQuery("#downbottom").unbind("click", Me.cmdremoveall_click).bind("click", Me.cmddownbottom_click);
        if (jQuery.trim(Me.report_id) == "") {
            jQuery("#cmdAdd").unbind("click", Me.cmdAdd_click).bind("click", Me.cmdAdd_click);
            jQuery("#cmdsaveandgenerate").unbind("click", Me.cmdSaveAndGenerate_click).bind("click", Me.cmdSaveAndGenerate_click);
        } else {
            jQuery("#cmdAdd").unbind("click", Me.btnGenerate_click).bind("click", Me.btnGenerate_click);
            jQuery("#cmdsaveandgenerate").unbind("click", Me.cmdSaveAndGenerate_click).bind("click", Me.cmdSaveAndGenerate_click);
        }
        $("#reporttable").unbind("change").bind("change", Me.reporttable_change);
        //606
        Me.loadfilterlist();
        Me.loadcolumnlist();
        if (Me.report_id == "")
        {
            Me.disabledaddcolandfilterbutton(true);
        }
        else
        {
            jQuery("#addfilter").unbind("click", Me.cmdaddfilter_click).bind("click", Me.cmdaddfilter_click);
            jQuery("#addcolumn").unbind("click", Me.addcolumn_click).bind("click", Me.addcolumn_click);
            jQuery("#arrcolumns").unbind("click", Me.arrcolumns_click).bind("click", Me.arrcolumns_click);
        }

        jQuery("#btnGenerate").unbind("click", Me.btnGenerate_click).bind("click", Me.btnGenerate_click);

        //end 606

        Me.InitPage();
    }
//------------------------------------------------------------
}