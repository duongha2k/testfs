<?
require_once('../../../includes/modules/web.init.php');
require_once('../../../includes/modules/common.init.php');
require_once('../../../includes/modules/tables.php');
$rptid = isset($_REQUEST['reportid']) ? $_REQUEST['reportid'] : "";
$CusReportClass = new Core_Api_CusReportClass();
$rptColList = $CusReportClass->getReportFieldList($rptid);
function loadsort($FieldList)
{
    $sort = "";
    if (!(empty($FieldList)))
    {
        foreach ($FieldList as $Field)
        {
            $fieldTitle = $Field['field_title'];
            $sort.="<option  value='" . $Field['field_code'] . "'>" . $fieldTitle . "</option>";
        }
    }
    return $sort;
}
?>
<table>
    <tr>
        <td>
            <select id="sort1" curid="1" anosort1="2"  anosort2="3" style="width:150px;">
                <option value="">None</option>
                <?= loadsort($rptColList); ?>
            </select>
            <input type="hidden" id="hidsort1" value="">
        </td>
        <td>
            <select id="sortdir1" style="width:100px;">
                <option value="asc">Ascending</option>
                <option value="desc">Descending</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>
            <select id="sort2" curid="2" anosort1="1"  anosort2="3" style="width:150px;">
                <option value="">None</option>
                <?= loadsort($rptColList); ?>
            </select>
            <input type="hidden" id="hidsort2" value="">
        </td>
        <td>
            <select id="sortdir2" style="width:100px;">
                <option value="asc">Ascending</option>
                <option value="desc">Descending</option>
            </select>

        </td>
    </tr>
    <tr>
        <td>
            <select id="sort3" curid="3" anosort1="1"  anosort2="2" style="width:150px;">
                <option value="">None</option>
                <?= loadsort($rptColList); ?>
            </select>
            <input type="hidden" id="hidsort3" value="">
        </td>
        <td>
            <select id="sortdir3" style="width:100px;">
                <option value="asc">Ascending</option>
                <option value="desc">Descending</option>
            </select>
        </td>
    </tr>
</table>