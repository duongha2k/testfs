<script type="text/javascript" src="js/custom_report.js"></script>
<?php

$page = "clients";
$option = "reports";
$selected = 'MetricsReport';
$UserType = (!empty($_SESSION['UserType']) ) ? $_SESSION['UserType'] : NULL;
$isFLSManager = (strtolower($Company_ID) == 'fls' && (strtolower($UserType) === 'install desk' || strtolower($UserType) === 'manager'));

if ($isFLSManager) {
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
} else {
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
}


require ("../../header.php");
require ("../../navBar.php");
require ("../../clients/includes/adminCheck.php");
require_once("../../clients/includes/PMCheck.php");

$displayPMTools = true;
$companyID = $_GET["v"];
$CusReportClass = new Core_Api_CusReportClass();

$Current_Page = $_SERVER['PHP_SELF'];
$Max_Results = 10;
$Page_Block_Size = 3;
$page = 1;
$offset = 0;
if (isset($_GET['page']))
{
    $page = $_GET['page'];
    if ($page < 1)
    {
        $page = 1;
    }
}

function round_up($num)
{
    if ($num != (int) $num)
    {
        $num = (int) $num + 1;
    }
    return (int) $num;
}

$result_count = sizeof($CusReportClass->getList($companyID));

$total_pages = round_up(($result_count / $Max_Results));

if ($page > $total_pages)
{
    $page = $total_pages;
}

$offset = (($page - 1) * $Max_Results);

$total_blocks = round_up($total_pages / $Page_Block_Size);

$page_block = round_up(($page / $Page_Block_Size) + 1);

$first_page = (int) (($page_block - 1) * $Page_Block_Size) + 1;
$last_page = $first_page + $Page_Block_Size - 1;

if ($page < $first_page)
{
    $page_block = $page_block - 1;
    $first_page = $first_page - $Page_Block_Size;
    $last_page = $last_page - $Page_Block_Size;
}

if ($last_page > $total_pages)
{
    $last_page = $total_pages;
}

$CusReportClasslist = $CusReportClass->getList($companyID, $Max_Results, $offset);

if ($result_count < $Max_Results)
{
    $net_results = $result_count;
} else
{
    $net_results = sizeof($CusReportClasslist);
}
$Max_Page = $result_count / $Max_Results;

$CusReportClasslist = $CusReportClass->getList($companyID, $Max_Results, $offset);

?>

<div align="center">
    <table width="100%" border="1" cellspacing="5" cellpadding="0">
        <tr>
            <th nowrap="nowrap" align="center" scope="col" colspan="3">Create & Generate Custom Reports</th>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div align="center">
                    <input type="button" value="Create New Report" class="link_button middle2_button" onclick="window.location = 'custom_report.php?v=<?= $_GET['v'] ?>'" />
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table class="gradBox_table_inner"  width="100%" border="1" cellspacing="5" cellpadding="0">
                    <col width="*">
                    <col width="60px">
                    <col width="60px">
                    <col width="100px">
                    <col width="100px">
                    <col width="200px">
                    <col width="100px">
                    <col width="60px">
                    <?
                    if (!empty($CusReportClasslist))
                    {
                        ?>
                        <tr class="table_head">
                            <th>Report Name</th>
                            <th>Run</th>
                            <th>Edit</th>
                            <th>Date Created</th>
                            <th>Last Modified</th>
                            <th>Last Modified By</th>
                            <th>Last Run</th>
                            <th>Delete</th>
                        </tr>
                        <?
                        $i = 1;
                        foreach ($CusReportClasslist as $item)
                        {
                            $rowAlt = ($i % 2) ? 'border_td odd_tr' : 'border_td even_tr';
                            $creation_date = date_format(new DateTime($item['creation_date']), "m/d/Y");
                            $lastmodified = date_format(new DateTime($item['lastupdated_date']), "m/d/Y");
                            $lastrun = !empty($item['last_run_date']) ? date_format(new DateTime($item['last_run_date']), "m/d/Y") : "";
                            ?>
                            <tr class="<?php echo $rowAlt ?>">
                                <td align="left" nowrap="nowrap">
                                    <?= $item['rpt_name'] ?>
                                </td>
                                <td align="center" nowrap="nowrap">
                                    <a style="cursor: pointer;" href="../reportview_new.php?rptid=<?= $item['id'] ?>&v=<?= $_GET['v'] ?>" >
                                        <img style="border: 1px;" src="../../widgets/images/ico_download.png" alt=""/>
                                    </a>
                                </td>
                                <td align="center" nowrap="nowrap">
                                    <a href="custom_report.php?report_id=<?= $item['id'] ?>&v=<?= $_GET['v'] ?>" >
                                        <img style="border: 1px;" src="../..//widgets/images/Pencilicon.png" alt=""/>
                                    </a>
                                </td>
                                <td align="left" nowrap="nowrap"><?= $creation_date ?></td>
                                <td align="left" nowrap="nowrap"><?= $lastmodified ?></td>
                                <td align="left" nowrap="nowrap"><?= empty($item['last_modified_by'])?$item['created_by']:$item['last_modified_by'] ?> </td>
                                <td align="center" nowrap="nowrap"><?= $lastrun ?></td>
                                <td align="center">
                                    <a href="javascript:;" onclick="deleteReport(this, '<?= $item['id'] ?>', '<?= $item['rpt_name'] ?>');"><img src="../../widgets/images/delete.gif" alt=""/> </a>
                                </td>
                            </tr>
                            <?
                            $i++;
                        }
                        ?>
                        <tr class="table_head">
                            <th colspan="8" style="text-align:center;">
                                <?php
                                $next_page = $page + 1;
                                $prev_page = $page - 1;

                                if ($page_block > 1)
                                {
                                    $prev_block = $first_page - $Page_Block_Size;
                                    echo "<a href='$Current_Page?v=$companyID&page=$prev_block&t=" . rand(444, 1000000) . "'>&lt;&lt;</a>&nbsp;";
                                }

                                if ($page > 1)
                                {
                                    echo "<a href='$Current_Page?v=$companyID&page=$prev_page&t=" . rand(444, 1000000) . "'>&lt;</a>&nbsp;";
                                }

                                for ($i = $first_page; $i <= $last_page; $i++)
                                {
                                    if ($page != $i)
                                    {
                                        echo "<a href='$Current_Page?v=$companyID&page=$i&t=" . rand(444, 1000000) . "'>$i</a>&nbsp;";
                                    } else
                                    {
                                        echo "$i ";
                                    }
                                }

                                if ($page < $Max_Page)
                                {
                                    echo "<a href='$Current_Page?v=$companyID&page=$next_page&t=" . rand(444, 1000000) . "'>&gt;</a>&nbsp;";
                                }

                                if ($page_block < $total_blocks)
                                {
                                    $next_block = $last_page + 1;
                                    echo "<a href='$Current_Page?v=$companyID&page=$next_block&t=" . rand(444, 1000000) . "'>&gt;&gt;</a>&nbsp;";
                                }
                                ?>
                                <br/>Results <?= $offset + 1 ?> - <?= $offset + $net_results; ?> of <?= $result_count ?>
                            </th>
                        </tr>
                        <?
                    } else
                    {
                        ?>
                        <tr>
                            <td colspan="8">
                                no record
                            </td>
                        </tr>
                        <?
                    }
                    ?>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3"><div align="center"></div></td>
        </tr>
    </table>
</div>
<script>
                        this.deleteReport = function(el, id, name) {
                            var opt = {};
                            var html = '<div style="padding-bottom:10px;">\n\
                    Are you sure you wish to delete this report: <b>' + name + '</b>?\n\
                </div>\n\
                <div style="text-align:center;">\n\
                    <input type="button" id="cmdCancel" class="link_button link_button_popup" value="Cancel">\n\
                    <input type="button" id="cmddeletereport" class="link_button link_button_popup" value="Delete">\n\
                </div>';
                            opt.width = 440;
                            opt.height = '';
                            opt.title = '<div style="text-align:center;">Delete Report</div>';
                            opt.body = html;
                            opt.zindex = 999;
                            roll.showNotAjax(el, null, opt);
                            $("#cmdCancel").unbind('click').bind('click', function() {
                                roll.hide();
                            })
                            $("#cmddeletereport").unbind('click').bind('click', function() {
                                roll.hide();
                                dodeleteReport(id);
                            })
                        }

                        this.dodeleteReport = function(id) {
                            var _onSuccess = function(data) {
                                window.location.reload();
                            }
                            _global.data = "";
                            _global.url = "/widgets/dashboard/custom-report/do-delete-report?report_id=" + id;
                            _global.onSuccess = _onSuccess;
                            _global.show();
                        }
</script>
<?php require ("../../footer.php"); ?><!-- ../ only if in sub-dir -->