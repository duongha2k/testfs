<?php
require_once('../../../includes/modules/web.init.php');
require_once('../../../includes/modules/common.init.php');
require_once('../../../includes/modules/tables.php');

$CusReportClass = new Core_Api_CusReportClass();
$rptColList = $CusReportClass->getReportFieldList($_REQUEST["report_id"]);

?>
<script>    
        $(document).ready(function() { 
            $("#collist tbody > tr:first .up").html('&nbsp;&nbsp;');
            $("#collist tbody > tr:last .down").html('');
            $("a.up").click(function() {
            var row = $(this).closest("tr");
            if (row.prev().length > 0) {               
                row.insertBefore(row.prev());
            }
            $("#collist tbody .up").html('Up');
            $("#collist tbody > tr:first .up").html('&nbsp;&nbsp;');  
            $("#collist tbody .down").html('Down');
            $("#collist tbody > tr:last .down").html('');
            return false;
            });
            $("a.down").click(function() {
            var row = $(this).closest("tr");
            if (row.next().length > 0) {      
                row.insertAfter(row.next());
            }
            $("#collist tbody .up").html('Up');
            $("#collist tbody > tr:first .up").html('&nbsp;&nbsp;');  
            $("#collist tbody .down").html('Down');
            $("#collist tbody > tr:last .down").html('');
           
            return false;
            });
        });
</script>
<table id="collist" cellpadding="0" cellspacing="0" width="100%" class="gradBox_table_inner">
    <colgroup>
        <col width="*">
        <col width="60px">
        <col width="30px" />       
    </colgroup>
    <thead>
    <tr class="table_head">
        <th>
            Field Name 
        </th>
        <th>
            Move
        </th>
        <th>
            Delete
        </th>
    </tr>
    </thead>

    <?
    $i = 1;
    foreach ($rptColList as $col)
    {
        ?>
        <tr class="border_td  <?= ($i % 2) ? "odd_tr" : "even_tr" ?>">
            <td fieldname="<?=$col['field_code']?>">
                <?= $col['field_title']; ?>
            </td>
            <td style="padding:0px 3px 0px 3px;">                
                <div style="width: 40%;float: left;"><a class="up" id="up_<?= $i; ?>" href="javascript:;" >Up</a></div>
                <div style="width: 50%;float: left;"><a class="down" id="down_<?= $i; ?>" href="javascript:;" >Down</a></div>                              
            </td>
            <td style="text-align:center;">
                <a class="deletecolumn" href="javascript:;" fieldid="<?= $col['field_code']; ?>"  fieldname="<?= $col['fieldname']; ?>"><img title="Delete" style="vertical-align: middle;height:13px;" src="/widgets/images/delete.gif"></a>							                       
            </td>
        </tr>
        <?
        $i++;
    }
    ?>
</table> 
