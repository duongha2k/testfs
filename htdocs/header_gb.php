<?php
// Set site URL
$siteUrl = "https://www.fieldsolutions.com";

$cookie_path = "/";
$cookie_timeout = "0";
$cookie_domain = ".fieldsolutions.com";
session_set_cookie_params($cookie_timeout, $cookie_path, $cookie_domain);
session_name(fsTemplate);
session_start();

// Get Subdomain
$urlParts = explode('.', $_SERVER['HTTP_HOST']);
$template = $urlParts[0];
$siteTemplate = $_SESSION['template'];

if($siteTemplate == ""){
	$_SESSION['template']=$template;
	$siteTemplate = $_SESSION['template'];
}







// Catches when there is no subdomain
//if ($getTemplate=="technicianbureau"){$getTemplate="www";}// delete
if ($siteTemplate=="technicianbureau"){$siteTemplate="www";}
?>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<title>Field Solutions</title>
<meta name="keywords" content="POS, Computer, Computer Repair, Technicians, Field Service, Retail Technicians, Home Computer Repair Technicians, Troubleshooting, PC Technicians" />
<meta name="description" content="Our focus is matching technicians seeking work with clients seeking technicians" />
<meta http-equiv="distribution" content="global" />
<meta http-equiv="copyright" content=" -2008" />
<meta http-equiv="url" content="http://www.fieldsolutions.com" />
<meta name="author" content="Gerald Bailey" />
<meta name="author" content="Collin McGarry" />
<meta name="author" content="Trung Ngo" />

<script type="text/javascript" src="<?php echo $siteUrl;?>/library/jquery/jquery-1.2.1.pack.js"></script>

<link rel=stylesheet type ="text/css" href="<?php echo $siteUrl;?>/templates/<?php echo $siteTemplate;?>/main.css" />


<!-- iPhone specific information -->

<!-- End iPhone specific information -->

</head>

<body>

<div id="mainContainer">

	<div id="headerSlider">
		<div id="headerRight" align="right">
			<div id="topNavBar">
			<ul>
			<li><a href="http://<?php echo $siteTemplate;?>.fieldsolutions.com/index.php">Home</a></li>
			<li><a href="http://<?php echo $siteTemplate;?>.fieldsolutions.com/content/aboutUs.php">About Us</a></li>
			<li><a href="http://<?php echo $siteTemplate;?>.fieldsolutions.com/content/contactUs.php">Contact Us</a></li>
			<li><a href="http://<?php echo $siteTemplate;?>.fieldsolutions.com/OtherOpportunities.php">Sponsored Links</a></li>
			</ul>
			</div>
		</div>
		<div id="headerLogo"></div>
	</div>
	
	