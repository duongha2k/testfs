<?php
	require_once dirname(__FILE__) . '/bootstrap.php';
	$db = Zend_Registry::get('DB');

	try {
		require_once(realpath(dirname(__FILE__) . "/../") . "/library/caspioAPI.php");
		require_once(realpath(dirname(__FILE__) . "/../") . "/library/smtpMail.php");

        // clears WO bids first before update to ensure proper count.
		$db->update('work_orders', array('Qty_Applicants' => 0));

        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('WorkOrderID', 'count(WorkOrderID) as cnt'));
        $query->where('Hide = ?', 0);
        $query->where('DeactivatedTech = ?', 0);
        $query->group('WorkOrderID');
        $applicantCounts = Core_Database::fetchAll($query);

        $IDsByCount = array();
        if ( $applicantCounts && sizeof($applicantCounts) != 0 ) {
            foreach ( $applicantCounts as $countInfo ) {
                $IDsByCount[$countInfo['cnt']][] = $countInfo['WorkOrderID'];
            }
            foreach ($IDsByCount as $count => $ids) {
                // update workorders with applicant count
                $db->update('work_orders', array('Qty_Applicants' => $count), 'WIN_NUM IN (' . implode(',', $ids) . ')');
            }

        }
	} catch (SoapFault $fault) {
		smtpMail("Applicant Count (Main Site) Script", "no-replies@fieldsolutions.com", "codem01@gmail.com,gerald.bailey@fieldsolutions.com", "Applicant Count (Main Site) Script Error", "$fault", "$fault", "applicantCount_MainSite.php");
	}
?>