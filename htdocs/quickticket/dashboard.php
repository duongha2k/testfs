<?php $page = "customers"; ?>
<?php  $option = "dashboard";?>
<?php 
require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');
require_once ("../headerStartSession.php"); ?>
<?php 
$siteTemplate = $_SESSION['template'];
if ($siteTemplate == "ruo") {
	require ("../templates/ruo/includes/header.php");
} else {
	require ("../header.php");
 }
?>
<?php 
$loggedIn = $_SESSION['loggedIn'];
$Company_ID = $_SESSION['Company_ID'];

if($loggedIn!="yes"){ 
// Redirect to logIn.php
header( 'Location: https://'.$siteTemplate.'.fieldsolutions.com/quickticket/logIn.php' ) ;
}
?>
<?php require ("../navBar.php"); ?>
<?php 
//$loggedIn = $_SESSION['loggedIn'];
//echo "$loggedIn";
?>


<!-- Add Content Here -->
<?php

if ($siteTemplate == "ruo") {
	require ("../templates/ruo/includes/customer_dashboardContent.php");
} else {
//	require ("dashboardContent.php");
}
?>


<!--- End Content --->
<?php require ("../footer.php"); ?>

