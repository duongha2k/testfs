<?php
//This page is will log all clients that login by inserting into Caspio using SOAP.
//require_once("../library/smtpMail.php");
if (!isset($_SERVER['HTTP_REFERER']) || strpos(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST), "fieldsolutions.com") === false) {
	//	echo "No External Access Allowed";
	//	die();	
//	smtpMail("Client Login Session", "nobody@fieldsolutions.com", "codem01@gmail.com", "Client Login Session Referer", "{$_SERVER['REMOTE_ADDR']} {$_SERVER['HTTP_USER_AGENT']} {$_SERVER['HTTP_REFERER']}", "{$_SERVER['REMOTE_ADDR']} {$_SERVER['HTTP_USER_AGENT']} {$_SERVER['HTTP_REFERER']}", "applicantCount.php");
}
require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');
try {
// Retrieve values from login
$UserName = $_REQUEST['UserName'];
$Password = $_REQUEST['Password'];


$authData = array('login'=>$UserName, 'password'=>$Password);

$user = new Core_Api_CustomerUser();
$user->checkAuthentication($authData);

if (!$user->isAuthenticate()){
    //330
	header("Location: /quickticket/logIn.php?login=false");
	return API_Response::fail();
}

$Company_ID = $user->getCompanyId();
$CustomerID = $user->getCustomerId();

// Set timestamp
$DateTime = date("m/d/Y G:i");

//echo "$UserName";
//echo "$DateTime";


// Insert Login History into TR_Client_Login_History
//$insertWorkorders = caspioInsert("TR_Client_Login_History", "UserName, DateTime", "'$UserName', '$DateTime'");

require_once("../headerStartSession.php");

//Set logged in session
/*$cookie_path = "/";
$cookie_timeout = "0";
$cookie_domain = ".fieldsolutions.com";
session_set_cookie_params($cookie_timeout, $cookie_path, $cookie_domain);
session_name(fsTemplate);
session_start();*/

$_SESSION['loggedIn']="yes";
$_SESSION['loggedInAs']="customers";
$_SESSION['CustomerID'] = $CustomerID;
$_SESSION['Company_ID']=$Company_ID;
$_SESSION['UserName']=$UserName;
$_SESSION['Password']=$Password;
$_SESSION['ContactName']= $user->getContactName();

// Library calls
require_once("../library/caspioAPI.php");
// Insert Login History into Login_History
//$insertWorkorders = caspioInsert("Login_History", "UserName, DateTime, UserType", "'$UserName', GETDATE(), 'Customer'",false);
$user->loginHistory($UserName);
// Get Subdomain
$urlParts = explode('.', $_SERVER['HTTP_HOST']);
$template = $urlParts[0];
$siteTemplate = $_SESSION['template'];

$checkIndexDisplay = $template;

if($siteTemplate == ""){
	$_SESSION['template']=$template;
	$siteTemplate = $_SESSION['template'];
}else if($siteTemplate != $template){
	$_SESSION['template']=$template;
	$siteTemplate = $_SESSION['template'];
}

}catch (SoapFault $fault){
//FTP Failure code

//smtpMail("getClientLoginHistory", "admin@fieldsolutions.com", "cmcgarry@fieldsolutions.com,gbailey@fieldsolutions.com", "getClientLoginHistory Error", "$fault", "$fault", "getClientLoginHistory.php");
}

$forceSSL = $siteTemplate != "dev";
//header('Location: http'.($_SERVER['HTTPS'] == 'on' || $forceSSL ? 's' : '').'://'.$_SERVER['HTTP_HOST'].'/customers/dashboard.php?v='.$Company_ID.'&t='.$CustomerID.'');
//330
header('Location: http'.($_SERVER['HTTPS'] == 'on' || $forceSSL ? 's' : '').'://'.$_SERVER['HTTP_HOST'].'/quickticket/wos.php?v='.$Company_ID.'&t='.$CustomerID.'');

//header('Location: http'.($_SERVER['HTTPS'] == 'on' || $forceSSL ? 's' : '').'://'.$_SERVER['HTTP_HOST'].'/quickticket/dashboard.php?v='.$Company_ID.'&t='.$CustomerID.'');

/*
$clientLoginMessageViewed = "yes"; //$_COOKIE["clientLoginMessageViewed"]; (Left this in case we want to add a msg in the future - GB)
if($clientLoginMessageViewed=="yes"){
// Redirect to dashboard.php
//header('Location: https://www.fieldsolutions.com/clients/dashboard.php' );
header('Location: https://'.$siteTemplate.'.fieldsolutions.com/customers/dashboard.php' );
} else if($siteTemplate == "ruo"){
header('Location: https://ruo.fieldsolutions.com/customers/ruo_loginMessage.php' );
}else{
header('Location: https://www.fieldsolutions.com/customers/loginMessage.php' );
}
*/

?>

