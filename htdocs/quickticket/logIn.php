<?php $page = 'customers'; ?>
<?php $option = 'home'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>

<!-- Add Content Here -->

<script type="text/javascript">
$(document).ready(function () {
	
	$('#customersLoginForm').submit(function(event) {
		event.preventDefault();
		
	  	var formData = $(this).serializeArray();
	   
	   var idx;
	   var query = "";
	   
	   for ( idx in formData ) {
           if ( formData[idx].value ) {
               query += '&' + formData[idx].name + '=' + encodeURIComponent(formData[idx].value).replace(/\~/g, '%7E').replace(/\!/g, '%21').replace(/\'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29');
           }
       }

	    l  = location.protocol + '//' + location.host;
	    l += '/quickticket/setCustomerLoginSession.php?v=true';
        l += query;
	   
	    location = l; 
	});

	var reqLogin = '<?=$_REQUEST['login']?>';
	
	if(reqLogin == "false"){
		$("#authFailed").show();
	}else{
		$("#authFailed").hide();
	}
	
});
</script>

<br /><br />
<!--330-->
<div id="leftcontent" style="border-right: thin solid #ffffff;">
<h1>QuickTicket Sign In</h1>
<!--end 330-->
<br/>
<div align="center">

<style type="text/css">
table#customerLogin{
	border: 2px solid rgb(56, 92, 126); 
	border-collapse: collapse;
}
table#customerLogin tr{
	background-color: rgb(255, 255, 255); 
	padding: 7px;
}
table#customerLogin td{
	text-align: left; 
	vertical-align: top; 
	width: auto; 
	white-space: nowrap; 
	padding: 2px 5px; 
	color: rgb(56, 92, 126); 
	font-size: 12px; 
	font-family: Verdana; 
	font-style: normal; 
	font-weight: normal;
}

.customerFormInput{
	color: rgb(0, 0, 0); 
	font-size: 12px; 
	font-family: Verdana; 
	font-style: normal; 
	font-weight: normal; 
	border: 1px solid rgb(0, 0, 0); 
	padding: 1px;
}

#authFailed{
	color: rgb(255, 0, 0); 
	font-size: 12px; 
	font-family: Verdana; 
	font-style: normal; 
	font-weight: bold; 
	text-align: left; 
	vertical-align: middle; 
	margin-left: 5px;
	display: none;
	text-align: center;
}
</style>

<p id="authFailed">
Authentication failed. The information you have provided cannot be authenticated. Check your login information and try again.
</p>

<form id="customersLoginForm" name="customersLoginForm" method="post" action="" style="margin: 0px;">
	<table id="customerLogin" cellspacing="0" style="">
	<tbody>
		<tr>
			<td>
				<label for="xip_UserName">Customer User Name</label>
			</td>
			<td>
				<input class="customerFormInput" type="text" value="" maxlength="255" size="15" id="UserName" name="UserName">
			</td>
		</tr>
		<tr>
			<td>
				<label for="xip_Password">Password</label>
			</td>
			<td>
				<input class="customerFormInput" type="password" value="" maxlength="255" size="15" id="Password" name="Password" >
			</td>
		</tr>
		<tr style="background-color: rgb(255, 255, 255); padding: 7px;">
			<td style="padding: 5px 0px; text-align: center; vertical-align: middle; border: 2px solid rgb(42, 73, 125); background-color: rgb(186, 203, 223);" colspan="2">
				<input type="submit" id="xip_datasrc_TR_Master_List"  value="Login" name="xip_datasrc_TR_Master_List">
			</td>
		</tr>
	</tbody>
	</table>
</form>
<!--330-->
<!--<p><a href="../misc/lostPassword.php">Forgot User Name and/or Password?</a></p>-->
<!--end 330-->

</div>

<!-- CHANGE WHEN USING OPEN LETTER ON RIGHT COLUMN -->
<img src="/images/pixel.gif" border="0" width="1" height="100">
</div>

<div id="rightcontent" width="85%">
	<?php if ($siteTemplate == "rtsflex"): ?>
    <div>
        <div style="border: solid 1px #1F497D;padding: 5px;color:#1F497D;width:400px;">
            <table cellspacing='0' cellpadding='0' border='0' width='100%'>
                <tr>
                    <td colspan="2" style="font-size: 16px; text-align:center;">Welcome to QuickTicket!</td>
                </tr>
                <tr>
                    <td colspan="2">QuickTicket gives you a detailed view of the 'work order' payroll events for Flextronics-RTS 'temp-to-perm' contractors.</td>
                </tr>
                <tr>
                    <td style="vertical-align: top; padding-left:15px; padding-right:2px;">1. </td>
                    <td>Use the 'Filter' tool to select 'work orders' for an individual contractor or pay period.</td>
                </tr>
                <tr>
                    <td style="vertical-align: top; padding-left:15px; padding-right:2px;">2. </td>
                    <td>Once filtered, Flextronics HR and Management users can view work order payroll event details, including total hours, pay rate, and total pay for the entire pay period.</td>
                </tr>
                <tr>
                    <td style="vertical-align: top; padding-left:15px; padding-right:2px;">3. </td>
                    <td>You can also download the filtered and sorted list of work orders to assist in confirming FieldSolutions payments are for the expected amount.</td>
                </tr>
            </table>
        </div>
    </div>
	<?php else: ?>
    <div style="margin-left:50%;width:100%;">
        <div style="border: solid 1px #1F497D;padding: 5px;color:#1F497D;width:400px;">
            <table cellspacing='0' cellpadding='0' border='0' width='100%'>
                <tr>
                    <td colspan="2" style="font-size: 16px; text-align:center;">Welcome to QuickTicket!</td>
                </tr>
                <tr>
                    <td colspan="2">QuickTicket gives you a detailed view of the work your vendor is running for you. Using the links in the upper left corner of your screen, you can:</td>
                </tr>
                <tr>
                    <td style="vertical-align: top; padding-left:15px; padding-right:2px;">1. </td>
                    <td>View and edit your contact information and password in "My Profile."</td>
                </tr>
                <tr>
                    <td style="vertical-align: top; padding-left:15px; padding-right:2px;">2. </td>
                    <td>"Create a QuickTicket" (Work Order) for your vendor.</td>
                </tr>
                <tr>
                    <td style="vertical-align: top; padding-left:15px; padding-right:2px;">3. </td>
                    <td>See the Work Orders your vendor is running for you in "QuickView." </td>
                </tr>
            </table>
        </div>
    </div>
	<?php endif; ?>

<br /><br />

	<div align="center">
	<table border="0" cellpadding="0" cellspacing="0">
		<tr>
	<td>
	<script language="JavaScript" type="text/javascript">
	<!--
		function goto(form) { var index=form.select.selectedIndex
		if (form.select.options[index].value != "0") {location=form.select.options[index].value;}}
	-->
	</script>
	
	<p>
	</p>
		
	</td>
		</tr>
	</table>
<br />

	</div>

</div>
<!--- End Content --->

<!--  -

<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b0000d6i9d3a1h3h6b6j6b1c","https:");}catch(ilolli){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000d6i9d3a1h3h6b6j6b1c">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>
-->

<br /><br />

</div>
<!--- End Content --->
<?php if ($siteTemplate != "rtsflex") require ("../footer.php"); ?>

