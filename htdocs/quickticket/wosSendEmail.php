<?php
// Created on: 07-28-08
// Author: GB
// Description: Receives Workorder info from datapage, retrieves project email info and notifies of assigned workorder.
try {

require_once("../library/timeStamps2.php");
require_once("../library/caspioAPI.php");
require_once("../library/smtpMail.php");
require_once("../library/woContactLog.php");

$tbUNID = $_REQUEST['tbUNID'];
$techID = $_REQUEST['techID'];
$projectID = $_REQUEST['projectID'];
$type = $_REQUEST['type'];
$company = $_REQUEST['company'];
$companyID = $_REQUEST['CompanyID'];



/* ----------------------------------------------------- */
/* NOTIFICATION OF PROJECT MANAGERS */
/* ------------------------------------------------------*/

/* Queries database for Work Order details */

	if ($type == "create") {
		$woInfo = caspioSelectAdv("Work_Orders", "TOP 1 WO_ID, Username, CONVERT(VARCHAR, StartDate, 101), StartTime, Project_Name, Address, City, State, Zipcode, Description, SpecialInstructions, Company_Name", "TB_UNID = '$tbUNID'", "", false, "'", "|");
				
		if (sizeof($woInfo) == 1 && $woInfo[0] != "") {
			$info = explode("|", $woInfo[0]);
			$workOrderID = trim($info[0], "'");
			$companyname = trim($info[11], "'");
			$username = trim($info[1], "'");
			$startDate = trim($info[2], "'");
			$startTime = trim($info[3], "'");
			$projectName = trim($info[4], "'");
			$address = trim($info[5], "'");
			$city = trim($info[6], "'");
			$state = trim($info[7], "'");
			$zipcode = trim($info[8], "'");
			$description = trim($info[9], "'");
			$specialInstr = trim($info[10], "'");

			Create_TimeStamp($tbUNID, $username, "Work Order Created (Quick Ticket)", $companyID, $companyname, $projectName, $UserName, "", "");
	
			$message = "		
			Work Order ID: $workOrderID
			Company Name: $companyname
			Username: $username
			Start Date: $startDate
			Start Time: $startTime
			Project Name: $projectName
			Address: $address
			City: $city
			State: $state
			Zip: $zipcode
			
			Description:
			
			$description
			
			Special Instructions:
			
			$specialInstr";
			
			$htmlmessage = nl2br($message);
			
			// Get Project info
			$project = caspioSelect("TR_Client_Projects", "To_Email", "Project_ID = '$projectID'", false);
			$To_Email = trim($project[0], "'");
			
			smtpMailLogReceived("Work Order Created", "nobody@fieldsolutions.com", "NewWOsNote@fieldsolutions.com, $To_Email", "New $company WO - $projectName", $message, $htmlmessage, "New Orders");

//			smtpMailLogReceived("Work Order Created", "nobody@fieldsolutions.com", "NewWOsNote@fieldsolutions.com, '$To_Email'", "New $company WO - $projectName", $message, $htmlmessage, "New Orders");
	

		}
	}


}catch (SoapFault $fault) {

smtpMail("WOS Email Tech When Assigned", "nobody@fieldsolutions.com", "gbailey@fieldsolutions.com", "WOS Email Tech When Assigned Script Error", "$fault", "$fault", "wosEmailTechWhenAssigned.php");
	
}// END CATCH

?>


<?php if ($type == "create"){ ?>
<?php $page = "customers"; ?>
<?php $option = "dashboard"; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->

<div align="center">
<br><br>

<br />
<p>
Your Quick Ticket has been posted. Click <a href="../quickticket/create_QuickTicket.php"><b>here</b></a> return to the Quick Ticket entry screen.
</p>
</div>

<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->

<?php }else{ ?>

<SCRIPT LANGUAGE="JavaScript"> 

	setTimeout('close()',100); 

</SCRIPT>

<? } ?>  



		

