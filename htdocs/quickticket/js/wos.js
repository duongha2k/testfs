


//-------------------------------------------------------------
//-------------------------------------------------------------
//-------------------------------------------------------------
wos.Instances = null;
//-------------------------------------------------------------
wos.CreateObject = function(config) {
    var obj = null;
    if (wos.Instances != null) {
        obj = wos.Instances[config.id];
    }
    if (obj == null) {
        if (wos.Instances == null) {
            wos.Instances = new Object();
        }
        obj = new wos(config);
        wos.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function wos(config) {
    var Me = this;
    this.id = config.id;
    this.dataFilter = null;
    this.roll = new FSPopupRoll();
    this.htmlFilter = "";
    //------------------------------------------------------------
    this.cmdFilter = function(){
        Me.showPopup(300, 550,'Filter By',this);
        //cache
        if(Me.htmlFilter != ""){
            Me.buildhtmlFilter(Me.htmlFilter);
            return false;
        }
        $.ajax({
            url : "/widgets/dashboard/popup/filter-quickticket",
            data : "",
            cache : false,
            type : 'POST',
            dataType : 'html',
            context : this,
            error : function (xhr, status, error) {
                $('body').css('cursor', '');
                if ( xhr.responseText === "authentication required" ) {
                    document.location = "/clients/logIn.php";
                } else {
                    jQuery("#comtantPopupID").html("Sorry, but an error occured. No results found.");
                }
            },
            success : function(data, status, xhr) {
                Me.htmlFilter = data;
                Me.buildhtmlFilter(data);
            }
        });
    }
    //------------------------------------------------------------
    this.buildhtmlFilter = function(data){
        jQuery("#comtantPopupID").html(data);
        jQuery("#filterForm .datefilter").calendar({
            dateFormat:'MDY/'
        });
        jQuery("#filterForm .datefilter").unbind("click",Me.calendarMove).bind("click",Me.calendarMove); 
        jQuery("#filterForm #cmdReset").unbind("click",Me.cmdReset).bind("click",Me.cmdReset); 
        jQuery("#filterForm #cmdCancel").unbind("click",Me.cmdCancel).bind("click",Me.cmdCancel); 
        jQuery("#filterForm #cmdOk").unbind("click",Me.cmdFilterOk).bind("click",Me.cmdFilterOk); 
        jQuery("#filterForm #filterCountry").unbind("change",Me.onChangeCountry).bind("change",Me.onChangeCountry);  
        Me.filterData();
    }
    //------------------------------------------------------------
    this.showPopup = function(width,height,title,el){
        Me.roll = new FSPopupRoll();
        var content = "<div id='comtantPopupID'><div style='text-align:center;height:"+height+"px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
        var opt = {
            width       : width,
            height      : '',
            title       : title,
            body        : content
        };
        Me.roll.showNotAjax(el, null, opt);
    }
    //------------------------------------------------------------
    this.calendarMove = function( input ) {
        $('#calendar_div').css('top', $(this).offset().top + $(this).height() + 6 + 'px');
        $('#calendar_div').css('left', $(this).offset().left + 'px');
        popUpCal.showFor(this);
    }
    //------------------------------------------------------------
    this.cmdCancel = function(){
        Me.roll.hide();
    }
    //------------------------------------------------------------
    this.cmdReset = function(){
        jQuery("#filterForm select").val("");
        jQuery("#filterForm input:text").val("");
        jQuery("#filterForm input:checkbox").removeAttr("checked");
    }
    //------------------------------------------------------------
    this.cmdFilterOk = function(){
        Me.dataFillter = jQuery("#filterForm").serializeArray();
        var params = {};
        params.tech = jQuery("#filterForm #filterTechID").val();
		params.contractor = jQuery("#filterForm #filterContractorID").val();
        params.win = jQuery("#filterForm #filterwin").val();
        params.client_wo = jQuery("#filterForm #filterClientID").val();
        params.po = jQuery("#filterForm #filterClientPO").val();
        params.date_start = jQuery("#filterForm #filterStartDate").val();
        params.date_end = jQuery("#filterForm #filterEndDate").val();
        params.city = jQuery("#filterForm #filterCity").val();
        params.state = jQuery("#filterForm #filterState").val();
        params.country = jQuery("#filterForm #filterCountry").val();
        params.zip = jQuery("#filterForm #filterZip").val();
        params.site_name = jQuery("#filterForm #filterSiteName").val();
        params.site_number = jQuery("#filterForm #filterSiteNumber").val();
        params.tech_lname = jQuery("#filterForm #filterTechLastName").val();
        params.tech_fname = jQuery("#filterForm #filterTechFirstName").val();
        params.tech_confirmed = jQuery("#filterForm #filterTechConfirmed").val();
        params.checked_in = jQuery("#filterForm #filterTechCheckedIn").val();
        params.checked_out = jQuery("#filterForm #filterTechCheckedOut").val();
        //13892
        params.StandardOffset = jQuery("#filterForm #StandardOffset").val();
        var projects = [];
        var i = 0;
        for(index in Me.dataFillter){
            if(Me.dataFillter[index].name == "filterProjects[]"){
               projects[i] = Me.dataFillter[index].value;
               i++;
            }
        }
        params.projects = projects;
        wd.setParams(params);
        wd.show({tab:wd.currentTab,params:wd.getParams(false)});
        Me.roll.hide();
    }
    //------------------------------------------------------------
    this.filterData = function(){
        if(Me.dataFillter != null){
            var state = "";
            for(index in Me.dataFillter){
                if(Me.dataFillter[index].name == "filterProjects[]"){
                    jQuery('#allProjectsTRContainer input:checkbox[value="'+Me.dataFillter[index].value+'"]').attr("checked",true);
                }else{
                    if(jQuery("#"+Me.dataFillter[index].name).attr("type") == "checkbox"){
                        jQuery("#"+Me.dataFillter[index].name).attr("checked",true);
                    }else{
                        if(Me.dataFillter[index].name == "filterState"){
                            state = Me.dataFillter[index].value;
                        }
                        jQuery("#"+Me.dataFillter[index].name).val(Me.dataFillter[index].value);
                        if(Me.dataFillter[index].name == "filterCountry"){
                            Me.onChangeCountry(state);
                        }
                    }
                }
            }
        }
    }
    //------------------------------------------------------------
    this.onChangeCountry = function (state) {
        $.ajax({
            url         : '/widgets/dashboard/js/state-info/',
            dataType    : 'json',
            data        : {
                country: $("#filterCountry").children("option:selected").val()
                },
            cache       : false,
            type        : 'post',
            context     : this,
            success     : function (data, status, xhr) {
                if (data) {
                    var option = '';
                    option += '<option selected="selected" value="">Select State</option>';
                    for(index in data) {
                        option += '<option value="' + index + '">' + data[index] + '</option>';
                    }
                    $("#filterState").html(option);
                    if (state) $('#filterState').val(state);
                }
            }
        });	
    }
    //------------------------------------------------------------
    //------------------------------------------------------------
    //------------------------------------------------------------
    //------------------------------------------------------------
    //------------------------------------------------------------
    //------------------------------------------------------------
    this.buildEvent = function()
    {
        jQuery("#cmdFilter").unbind("click",Me.cmdFilter).bind("click",Me.cmdFilter); 
    }
    //------------------------------------------------------------
    this.init = function() {
        Me.buildEvent();
    }
}