
function massageMyTime() {
	this.value = massageTime(this.value);
}

document.forms.caspioform.InsertRecordStartTime.onblur = massageMyTime;
document.forms.caspioform.InsertRecordEndTime.onblur = massageMyTime;

function massageMyPhone() {
	this.value = massagePhone(this.value);
}

document.forms.caspioform.InsertRecordSitePhone.onblur = massageMyPhone;
document.forms.caspioform.InsertRecordSiteFax.onblur = massageMyPhone;


function UpdateCaspio(){

	if (!isValidEmail(document.forms.caspioform.InsertRecordSiteEmail.value)) {
		document.forms.caspioform.InsertRecordSiteEmail.focus();
		alert("The site email is not valid");
		return false;
	}
	
	if (!isValidTime(document.forms.caspioform.InsertRecordStartTime.value)) {
	// check start time format
	document.forms.caspioform.InsertRecordStartTime.focus();
	alert("Start Time is not formated correctly");
	return false;
	}
	
	if (!isValidTime(document.forms.caspioform.InsertRecordEndTime.value)) {
	// check end time format
	document.forms.caspioform.InsertRecordEndTime.focus();
	alert("End Time is not formated correctly");
	return false;
	}
	
	if (!isValidPhone(document.forms.caspioform.InsertRecordSitePhone.value)) {
	// check site phone format
	document.forms.caspioform.InsertRecordSitePhone.focus();
	alert("Site Phone is not formated correctly");
	return false;
	}

	if (!isValidPhone(document.forms.caspioform.InsertRecordSiteFax.value)) {
	// check site fax
	document.forms.caspioform.InsertRecordSiteFax.focus();
	alert("Site Fax is not formated correctly");
	return false;
	}
	
}

document.forms.caspioform.onsubmit = UpdateCaspio;

