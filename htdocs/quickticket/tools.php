<?php
$page = "customers";
$option = "quickTools";
$project_id = 5543; //5902;
$certification_id = 22;

require_once("../headerStartSession.php");
require ("../header.php");

$Company_ID = (!empty($_GET['v']) ) ? $_GET['v'] : $_SESSION['Company_ID'];
$UserType = (!empty($_SESSION['UserType']) ) ? $_SESSION['UserType'] : NULL;
$isFLSManager = (strtolower($Company_ID) == 'fls' && (strtolower($UserType) === 'install desk' || strtolower($UserType) === 'manager'));
$Core_Api_User = new Core_Api_User;
$dbVersion = 'Full';

//$dbVersion = (isset($_POST['version']) && strtolower($_POST['version']) === 'lite') ? "lite" : strtolower($Core_Api_User->getdbVersion()) == "lite" ? 'lite' : 'full';
$_COOKIE["dashboardVersionDefault"] = ($dbVersion == 'lite') ? 'Lite' : 'Full';
//$dbVersion    = (isset($_POST['version']) && strtolower($_POST['version'])==='lite')?'lite':'full';

if ($isFLSManager) {
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
}
else {
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
}

require '../navBar.php';
?>

<?php script_nocache ("/widgets/js/functions.js"); ?>
<?php script_nocache ("/widgets/js/FSWidgetManager.js"); ?>
<script  src="../library/jquery/calendar/jquery-calendar.js"></script>

<link rel=stylesheet type="text/css" href="/widgets/css/dashContent.css" />
<link rel=stylesheet type="text/css" href="/widgets/css/main.css" />
<link rel="stylesheet" type="text/css" href="/widgets/css/popup.css" />
<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />

<!-- Check if logged in, if not redirect -->
<script type="text/javascript">
//<![CDATA[
    try {
        window._company = '<?= $Company_ID ?>';
        window._isFLSManager = <?= ($isFLSManager) ? 'true' : 'false'; ?>;
        window._search       = <?= (isset($_GET['search']) && $_GET['search'] + 0) ? 'true' : 'false'; ?>;
        if ( !isPM && _company !== window._company ) {
            //window.location.replace("<?= $_SERVER['PHP_SELF'] ?>?v=" + companyID);
        }
    } catch (e) {
        //window.location.replace("./");
    }
    //  use for load images and save it in browser cache only!!!
    var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
    var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";
//]]>
</script>

<script type="text/javascript" src="/widgets/js/jquery.form.js"></script>
<?php script_nocache ("/widgets/js/FSWidget.js"); ?>
<?php script_nocache ("/widgets/js/FSWidgetDashContentLeft.js"); ?>
<?php script_nocache ("/widgets/js/FSWidgetDashContentRight.js"); ?>
<?php //script_nocache ("/widgets/js/FSWidgetSortTool.js"); ?>
<?php if (strtolower($Company_ID) == 'fls'): ?>
	<?php //script_nocache ("/widgets/js/FSWidgetSortToolFLS.js"); ?>
<?php endif; ?>
<?php script_nocache ("/widgets/js/FSWidgetDeactivationTool.js"); ?>
<?php script_nocache ("/widgets/js/FSWidgetActivityLog.js"); ?>
<?php script_nocache ("/widgets/js/FSQuickAssignTool.js"); ?>
<?php script_nocache ("/widgets/js/FSPopupAbstract.js"); ?>
<script type="text/javascript" src="/widgets/js/jquery-ui.js"></script>
<script type="text/javascript" src="/widgets/js/livevalidation.js"></script>
<script type="text/javascript" src="/library/jquery/contentmenu/jquery.contextmenu.js"></script>

<?php script_nocache ("/widgets/js/resizable.js"); ?>
<?php script_nocache ("/widgets/js/FSPopupRoll.js"); ?>
<?php script_nocache ("/widgets/js/ProgressBar.js"); ?>
<?php script_nocache ("/widgets/js/ProgressBarAdapterAbstract.js"); ?>
<?php script_nocache ("/widgets/js/ProgressBarAdapterPopupSpin.js"); ?>
<?php script_nocache ("/widgets/js/FSTechPayUtils.js"); ?>
<?php script_nocache ("/widgets/js/FSWidgetDashboard.js"); ?>
<?php script_nocache ("/widgets/js/FSWidgetSortTool.js"); ?>
<?php script_nocache ("/widgets/js/FSCustomerQuickTools.js"); ?>

<link rel="stylesheet" href="/widgets/css/resizable.css" type="text/css" />
<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>

<link rel="stylesheet" href="/library/jquery/contentmenu/jquery.contextmenu.css" type="text/css" />

<script type="text/javascript">
//<![CDATA[
var _project = <?= $project_id ?>;
var quicktools = false;
var roll;
var wd;

function calendarInit() {
	$.map(['filterStartDate', 'filterEndDate'], function( id ) {
		$('#' + roll.container().id + ' #' + id).focus( function() {
			$('#calendar_div').css('z-index', ++roll.zindex);
			$(this).calendar({dateFormat:'MDY/'});
		});
	});
}

function calendarMove( input ) {
	$('#calendar_div').css('top', $(input).offset().top + $(input).height() + 6 + 'px');
	$('#calendar_div').css('left', $(input).offset().left + 'px');
	popUpCal.showFor(input);
}

jQuery(document).ready (function () {
	var sort;

	roll = new FSPopupRoll();

	jQuery("#dashleftcontent").show ().width ("120px");
	jQuery("#widgetContainer").css ("min-width", "auto");
	jQuery("#filterForm").show ();

	quicktools = new FSCustomerQuickTools ({
		container	: "widgetContainer",
		tab			: "quicktools",
		popup		: "quicktools",
		company		: _company,
		roll		: roll,
		project		: _project,
		cert		: <?= $certification_id ?>
	});

	quicktools.loadSites ("statesSelect", "sitesContainer");

	sort = new FSWidgetSortTool (null, null, quicktools, roll, 'quick');
	quicktools.sortTool (sort);
	quicktools.resetFilters (roll);

	$("#sortButton").click (function (event) {
		quicktools.sortTool().switchSortTools("full");
		roll.autohide (true);

		var opt = {
			width   	: 325,
			height  	: 225,
			title   	: "Sort List",
			context 	: quicktools.sortTool(),
			handler 	: quicktools.sortTool().initSortPopup,
			body    	: quicktools.sortTool().makeFullSortPopup()
		};

		roll.showNotAjax (this, event, opt);
	});

	$("#filterButton").click (function (event) {
		roll.autohide (true);

		var opt = {
			width		: 325,
			height		: 300,
			title		: 'Filter List',
			context		: quicktools,
			handler		: quicktools.prepareFilters,
			postHandler	: calendarInit,
			body		: jQuery("#filterContent").html()
		};

		roll.showNotAjax (this, event, opt);
	});

	wd = quicktools;
});
//]]>
</script>

<div id="container">
	<div id="header" style="width: 100%; height: 100%">
		<table width="100%" cellspacing="0" cellpadding="0">
			<tr>
				<td valign="top" align="left">
					<div style="width: 140px; padding-left: 8px; padding-right: 8px;">
						<div style="margin-bottom: 10px;">
							<img src="/widgets/images/arrow_blue_icon.png" width="21" height="23" border="0" style="position:relative; top: -3px; vertical-align: middle;" />
							<span style="font-weight: bold;">QuickTools</span><br/>
						</div>
						<input id="filterButton" type="button" class="link_button" style="margin-bottom: 8px;" value="Filter" /><br/>
						<input id="sortButton" type="button" class="link_button" style="margin-bottom: 8px;" value="Sort" /><br/>
						<input type="button" class="link_button download_button" style="margin-bottom: 8px;" value="Download" /><br/>
					</div>
				</td>
				<td valign="top" align="left">
					<div id="widgetContainer"></div>
				</td>
			</tr>
		</table>
	</div>
</div>

<?php
	//TODO: The following code needs to be moved into controller actions and view scripts
	$db = Core_Database::getInstance();

	$select = $db->select ()->distinct ();
	$select->from ("sites", array ("SiteInfo1"));
	$select->where ("Project_ID = ?", $project_id);
	$info_result1 = $db->fetchAll ($select);

	$select = $db->select ()->distinct ();
	$select->from ("sites", array ("SiteInfo2"));
	$select->where ("Project_ID = ?", $project_id);
	$info_result2 = $db->fetchAll ($select);

	$select = $db->select ();
	$select->from ("States");
	$states = $db->fetchAll ($select);
?>

<div id="filterContent" style="display: none;">
<form name="filterForm" id="filterForm" method="get" action="/">
	<input type="hidden" value="<?= $Company_ID ?>" name="filterCompanyId" id="filterCompanyId" />
	<input type="hidden" value="<?= $certification_id ?>" name="cert" />
	<div style="text-align:center;">
		<input type="button" class="link_button_popup" onclick="wd.resetFilterForm(roll);" value="Reset" />
		<input type="button" class="link_button_popup" onclick="roll.hide(); FSWidget.filterActivate(false);" value="Cancel" />
		<input type="button" class="link_button_popup" onclick="wd.applyFilters(roll);" value="Ok" />
	</div>

	<div  class="filter_popup">
		<table width="100%">
			<col width="33%" />
			<col width="*" />
			<tr id='sectionSiteInfo1' class='filterSection'>
				<td class="tRight">DSC Zone</td>
				<td>
					<select id="filterSiteInfo1" name="filterSiteInfo1">
						<option value="">Select Zone</option>
						<?php foreach ($info_result1 as $result): ?>
							<option value="<?= $result["SiteInfo1"] ?>"><?= $result["SiteInfo1"] ?></option>
						<?php endforeach; ?>
					</select>
				</td>
			</tr>
			<tr id='sectionSiteInfo2' class='filterSection'>
				<td class="tRight">DSC Market</td>
				<td>
					<select id="filterSiteInfo2" name="filterSiteInfo2">
						<option value="">Select Market</option>
						<?php foreach ($info_result2 as $result): ?>
							<option value="<?= $result["SiteInfo2"] ?>"><?= $result["SiteInfo2"] ?></option>
						<?php endforeach; ?>
					</select>
				</td>
			</tr>
			<tr id='sectionCity' class='filterSection'>
				<td class="tRight">DSC City</td>
				<td>
					<input type="text" name="filterCity" id="filterCity" />
				</td>
			</tr>
			<tr id='sectionState' class='filterSection'>
				<td class="tRight">DSC State</td>
				<td>
					<select id="filterState" name="filterState">
						<option value="">Select State</option>
						<?php foreach ($states as $result): ?>
							<option value="<?= $result["Abbreviation"] ?>"><?= $result["Name"] ?></option>
						<?php endforeach; ?>
					</select>
				</td>
			</tr>
			<tr id='sectionZip' class='filterSection'>
				<td class="tRight">DSC Zip</td>
				<td>
					<input type="text" name="filterZip" id="filterZip" />
				</td>
			</tr>
			<tr id='sectionDisplayContractors' class='filterSection'>
				<td class="tRight">Display Contractors</td>
				<td>
					<select id="filterDisplayContractors" name="filterDisplayContractors">
						<option value="" selected>All</option>
						<option value="approved">Approved Only</option>
						<option value="unapproved">Unapproved Only</option>
					</select>
				</td>
			</tr>
			<tr id='sectioncontractor_id' class='filterSection'>
				<td class="tRight">Contactor ID</td>
				<td>
					<input type="text" name="filtercontractor_id" id="filtercontractor_id" />
				</td>
			</tr>
			<tr id='sectionLastName' class='filterSection'>
				<td class="tRight">Contractor Last Name</td>
				<td>
					<input type="text" name="filterLastName" id="filterLastName" />
				</td>
			</tr>
		</table>
	</div>
</form>
</div>



<div id="editContent" style="display: none;">
<form name="editForm" id="editForm" method="get" action="/">
	<input type="hidden" name="tech_id" id="editTechID" />

	<div class="filter_popup">
		<table width="100%">
			<col width="33%" />
			<col width="*" />
			<tr class='filterSection'>
				<td class="tRight">Contractor Name</td>
				<td id="editContractorName">
				</td>
			</tr>
			<tr class='filterSection'>
				<td class="tRight">Contractor ID#</td>
				<td>
					<input type="text" name="contractor_id" id="editContractorID" />
				</td>
			</tr>
			<tr class='filterSection'>
				<td class="tRight">Assign Service Center</td>
				<td>
					<select id="statesSelect">
						<option value="">Select a State</option>
					</select>
					<div id="sitesContainer" style="font-size: 11px; line-height: 18px;"></div>
				</td>
			</tr>
		</table>
	</div>

	<div style="text-align:center;">
		<input type="button" class="link_button_popup" onclick="roll.hide();" value="Cancel" />
		<input type="button" class="link_button_popup" onclick="quicktools.updateContractor(jQuery(roll.container()).find('#editForm').serialize()); roll.hide();" value="Save" />
	</div>
</form>
</div>

<?php require ("../footer2.php"); ?>

