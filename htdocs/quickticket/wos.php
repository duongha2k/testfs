<?php
$page = "customers";
$option = "projectStatus";

require_once("../headerStartSession.php");
require ("../header.php");

$full_search = true;

if (isset($_GET['full_search']))
{
    $full_search = $_GET['full_search'];
    $sort = $_GET;
    $fillter = $_GET;
    if (isset($fillter['start_date']))
    {
        $fillter['start_date'] = str_replace('_', '/', $fillter['start_date']);
    }
    if (isset($fillter['end_date']))
    {
        $fillter['end_date'] = str_replace('_', '/', $fillter['end_date']);
    }
    if (isset($fillter['projects']))
    {
        $_projects = "";
        for ($i = 0; $i < count($fillter['projects']); $i++)
        {
            $_projects .= ",'" . $fillter['projects'][$i] . "'";
        }
        if ($_projects != "")
        {
            $_projects = '[' . substr($_projects, 1) . ']';
        }
        $fillter['projects'] = $_projects;
    }
}
?>
<script>
    var isPostBack = false;
    var dataSort = {};
    var dataFillter = {};

</script>
<?php
$Company_ID = (!empty($_GET['v']) ) ? $_GET['v'] : $_SESSION['Company_ID'];
$UserType = (!empty($_SESSION['UserType']) ) ? $_SESSION['UserType'] : NULL;
$isFLSManager = (strtolower($Company_ID) == 'fls' && (strtolower($UserType) === 'install desk' || strtolower($UserType) === 'manager'));
$Core_Api_User = new Core_Api_User;
$dbVersion = 'Full';
//$dbVersion = (isset($_POST['version']) && strtolower($_POST['version']) === 'lite') ? "lite" : strtolower($Core_Api_User->getdbVersion()) == "lite" ? 'lite' : 'full';
$_COOKIE["dashboardVersionDefault"] = ($dbVersion == 'lite') ? 'Lite' : 'Full';
//$dbVersion    = (isset($_POST['version']) && strtolower($_POST['version'])==='lite')?'lite':'full';
if ($isFLSManager)
{
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
} else
{
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
}

require '../navBar.php';
?>

<?php script_nocache ("/widgets/js/functions.js"); ?>
<?php script_nocache ("/quickticket/js/wos.js"); ?>
<?php script_nocache ("/widgets/js/FSWidgetManager.js"); ?>
<script  src="../library/jquery/calendar/jquery-calendar.js"></script>

<script type="text/javascript">
    var wManager = new FSWidgetManager();
</script>

<link rel=stylesheet type="text/css" href="/widgets/css/dashContent.css" />
<link rel=stylesheet type="text/css" href="/widgets/css/main.css" />
<link rel="stylesheet" type="text/css" href="/widgets/css/popup.css" />
<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<!-- Check if logged in, if not redirect -->
<script type="text/javascript">
    try {
        companyID = window._company      = '<?= $Company_ID ?>';
        window._isFLSManager = <?= ($isFLSManager) ? 'true' : 'false'; ?>;
        window._search       = <?= (isset($_GET['search']) && $_GET['search'] + 0) ? 'true' : 'false'; ?>;
        var test = companyID;
        if ( !isPM && companyID !== window._company ) {
            //window.location.replace("<?= $_SERVER['PHP_SELF'] ?>?v=" + companyID);
        }
    } catch (e) {
        //window.location.replace("./");
    }
    //  use for load images and save it in browser cache only!!!
    var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
    var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";
</script>

<script type="text/javascript" src="/widgets/js/jquery.form.js"></script>
<?php script_nocache ("/widgets/js/FSWidget.js"); ?>
<?php script_nocache ("/widgets/js/FSWidgetDashContentLeft.js"); ?>
<?php script_nocache ("/widgets/js/FSWidgetDashContentRight.js"); ?>
<?php script_nocache ("/widgets/js/FSWidgetCustomerDashboardDetails.js"); ?>
<?php script_nocache ("/widgets/js/FSWidgetSortTool.js"); ?>
<?php if (strtolower($Company_ID) == 'fls'): ?>
	<?php script_nocache ("/widgets/js/FSWidgetSortToolFLS.js"); ?>
<?php endif; ?>
<?php script_nocache ("/widgets/js/FSWidgetDeactivationTool.js"); ?>
<?php script_nocache ("/widgets/js/FSWidgetActivityLog.js"); ?>
<?php script_nocache ("/widgets/js/FSQuickAssignTool.js"); ?>
<?php script_nocache ("/widgets/js/FSPopupAbstract.js"); ?>
<script type="text/javascript" src="/widgets/js/jquery-ui.js"></script>
<script type="text/javascript" src="/widgets/js/livevalidation.js"></script>
<script type="text/javascript" src="/library/jquery/contentmenu/jquery.contextmenu.js"></script>
<link rel="stylesheet" href="/library/jquery/contentmenu/jquery.contextmenu.css" type="text/css" />

<script type="text/javascript">
    //Default validation settings
    var LVDefaultFunction = function(){this.addFieldClass();}
    var LVDefaults = {
        onValid : LVDefaultFunction,
        onInvalid : LVDefaultFunction
    }

    var pmContext = "<?= $_GET["v"] ?>";
        
    
</script>

<?php script_nocache ("/widgets/js/resizable.js"); ?>
<?php script_nocache ("/widgets/js/FSPopupRoll.js"); ?>
<?php script_nocache ("/widgets/js/ProgressBar.js"); ?>
<?php script_nocache ("/widgets/js/ProgressBarAdapterAbstract.js"); ?>
<?php script_nocache ("/widgets/js/ProgressBarAdapterPopupSpin.js"); ?>
<?php script_nocache ("/widgets/js/FSTechPayUtils.js"); ?>
<link rel="stylesheet" href="/widgets/css/resizable.css" type="text/css" />
<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript">
    /**
     * toggleTab
     *
     * Switch Tabs
     *
     * @param string name Name of currect Tab
     * @param object link Tab's link object
     * @param object wd wd object
     * @return false always
     */
    var hasData = false;
    function toggleTab( name, link, wd, withPage ) {
 
        hasData = true;

        if (wd.currentTab != name) {
            //wd.resetFilters();
            wd.params.isBid = 0;
            wd.currentTab = name;
            //_wos.dataFillter = null;
            //wd.sortTool().initSortTool(); 
        }

        if (withPage == undefined) withPage = false;
        wd.show({tab:wd.currentTab,params:wd.getParams(withPage)});
        link.blur();
        $("#toppaginatorContainer").html("");

        //set default state, then add custom classes.
        jQuery('.toolGroup').css('display',"");
        return false;
    }

    var wd;             //  Widgets object
    var roll;           //  Rollover object
    var popup;          //  Rollover object
    var progressBar;    //  Progress bar object
    var activityLog;    //  Activity Log object

    function saveDBState() {
        p = wd.getParams();
        p.page = wd.sortTool()._page;
        try {
            $.cookie('FSWidgetDashboardState', JSON.stringify({tab: wd.currentTab, params: p}));
        } catch (e) {}
    }

    try {
        window.onbeforeunload = function() {
            try {
                saveDBState();
            } catch (e) {}
            return;
        };
    } catch (e) {
        $(window).unload(function() {
            try {
                saveDBState();
            } catch (e) {}
        });
    }

    function startup() {
        var _ajaxSuccess = function(fun,_el,data) { 
            hasData = true;
            if(typeof(fun) =='function')
            {
                fun(_el,data);
            }
            return false;
        };
        var tab = '<?= (!empty($_GET['tab'])) ? strtolower($_GET['tab']) : '__invalid__'; ?>';
        var projectid = '<?= (!empty($_GET['projectid'])) ? strtolower($_GET['projectid']) : ''; ?>';
                    
        dataFillter.projects = projectid;

        var isBid = 0;
        if(projectid != '')
        {
            isBid = '<?= (!empty($_GET['techid'])) ? strtolower($_GET['techid']) : ''; ?>';  
        }
                    
        wd = new FSWidgetDashboard({
            container:'widgetContainer',
            tab:'quickviewopen',
            dbVersion:'<?= $dbVersion ?>',
            isBid:isBid,
            saveStateCallback: saveDBState,
            ajaxSuccess:_ajaxSuccess});  
        wd.setParamsSession(dataFillter);
        if ( wd._isValidTab(tab) ) {
            wd.currentTab = tab;
        }
        /* Find tab in URL params */

        roll        = new FSPopupRoll();
        progressBar = new ProgressBar();
        activityLog = new FSWidgetActivityLog( roll );

        wd.htmlFilters();
<?php if (strtolower($Company_ID) == 'fls') : ?>
            wd.sortTool(new FSWidgetSortToolFLS('sortSelect', 'directionSelect', wd, roll, 'quick'));
<?php else : ?>
            wd.sortTool(new FSWidgetSortTool('sortSelect', 'directionSelect', wd, roll, 'quick'));
<?php endif; ?>
                    
        wd.deactivationTool(new FSWidgetDeactivationTool(wd,roll));



        /*  Load tab by parameters in URL */
        savedState = $.cookie('FSWidgetDashboardState');
        parse = wd.parseUrl();
        size = 0;
        for (key in parse) {
            ++size;
        }
        if (size > 1) savedState = null;
        if (!savedState) {
            wd.setParams(parse);
            toggleTab(wd.currentTab, $('#'+wd.currentTab), wd);
        }
        /*  Load tab by parameters in URL */

        else {
            $.cookie('FSWidgetDashboardState', null);
            state = JSON.parse(savedState);
            wd.setParams(state.params);
            wd.currentTab = state.params.tab;
            //            wd.sortTool().prepareItems(version);
            wd.sortTool().initSortTool();
            alert(state.params.sort1);
            $('#sortSelect').val(state.params.sort1);
            $('#directionSelect').val(state.params.dir1);
            wd.sortTool().update();
            wd.sortTool().setPage(state.params.page);
            try {
                // IE 7 workaround
                a = typeof state.params.projects[0];
                if (a == 'function') {
                    state.params.projects.reverse();
                    state.params.projects.pop();
                    state.params.projects.reverse();
                }
                wd.filters().setProject(state.params.projects);
            } catch (e){}
            //		wd.show(state);
            toggleTab(wd.currentTab, $('#'+wd.currentTab), wd, true);

            //	        toggleTab(state.tab, $('#'+state.tab), wd);
        }
        progressBar.key('approve');
        progressBar.adapter(new ProgressBarAdapterPopupSpin(progressBar, null));
        jQuery("#widgetContainer").html("");
        
        if(dataSort.sort1 !== "" && typeof(dataSort.sort1) != 'undefined')
        {
            jQuery("#sortSelect").val(dataSort.sort1);  
            jQuery("#directionSelect").val(dataSort.dir1);  
        }

        loadContent ();

        return false;
    }

    function reloadTabFrame() {
        wd.show({tab: wd.currentTab, params: wd.getParams(true)});
    }

</script>


<div id="container" class="use-sidebar-right use-sidebar-left">
    <div id="header" style="width: 100%">
        <div id="wrapper">
            <div id="content" align="center"></div>
        </div>

        <!-- DASHBOARD FILTER -->
        <div id="projectSearch" onclick="return true;">
            <form id="dashFilter" name="dashFilter" action="<?= $_SERVER['PHP_SELF'] ?>?v=<?= $_GET["v"] ?>" method="post">
                <div id="contents">
                    <?php require("includes/getWOCount.php"); ?>

                    <div class="toolGroup">
                        <table width="100%">
                            <tr>
                                <td width="140px">
                                    <input id="sortButton" type="button" value="Sort" class="link_button" />
                                </td>
                                <td width="140px">
                                    <input id="cmdFilter" type="button" value="Filter" class="link_button" />
                                </td>
                                <td width="140px">
                                    <input id="downloadButton" type="button" value="Download" class="link_button download_button" />
                                </td>

                                <td style="text-align: right;padding-right:10px;top:0px;"></td>
                                <td width="110px"></td>
                                <td width="70px">
                                </td>
                                <td width="70px">
                                    Quick Sort
                                </td>
                                <td width="10px">
                                    <div class="toolBtn">
                                        <select id="sortSelect" onchange="return wd.sortTool().applySort('quick');"></select>
                                    </div>
                                </td>
                                <td width="10px">
                                    <div class="toolBtn" style="margin:0;">
                                        <select id="directionSelect" onchange="return wd.sortTool().applySort('quick');"></select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div id="tabbedMenu" style="height:16px;min-width: 300px;">
                                        <ul>
                                            <li id="quickviewopen"><a attributeId="14" classs href="javascript:void(0)" onclick="toggleTab('quickviewopen', this, wd);">Open</a></li>
                                            <li id="quickviewclose"><a attributeId="15" href="javascript:void(0)" onclick="toggleTab('quickviewclose', this, wd);">Closed</a></li>
                                        </ul>
                                    </div>

                                </td>
                                <td colspan="4" id="toppaginatorContainer" class="paginator_top" style="text-align: right;padding-right:10px;top:0px;"></td>
                                <td colspan="2" width="100px">
                                    <script>
                                        function onChangePageSize()
                                        {
                                            wd.sortTool().setPage(1);
                                            reloadTabFrame();
                                        }
                                    </script>
                                    <div  style="padding-left:40px;" class="toolBtn">Results per page:
                    </div>
                                    <div id="toppaginatorSizeContainer"></div>
                                </td>
                            </tr>
                        </table>
                </div>
                </div>
                <script type="text/javascript">
                    var rightNavWidget;
                    function loadContent () {

                        objMain = $("#container");
                        wd.setParamsSession(dataFillter);

                        $("#downloadButton").click(function(event){
                            var opt = wd.getParams();
                            opt['download']='csv';
                            opt['companyid'] = '<?= $_GET['v'] ?>';
                            wd.open({
                                tab     : wd.currentTab,
                                params  : opt
                            });
                        });

                        $("#sortButton").click(function(event){
                            wd.sortTool().switchSortTools('full');
                            roll.autohide(false);
                            var opt = {
                                width   : 325,
                                height  : 225,
                                title   : 'Sort Work Orders...',
                                context : wd.sortTool(),
                                handler : wd.sortTool().initSortPopup,
                                body    : wd.sortTool().makeFullSortPopup()
                            };
                            roll.showNotAjax(this,event,opt);
                        });
                    }
                </script>

                <br class="clear" />

        </div>
        

        <div id="widgetContainer"></div>
        </form>
    </div>
    <div id="dashrightcontent"></div>
</div>
</div>
<!-- End Content -->
<script>
    //$(document).ready(function() {

    if (typeof(startup) == 'function') startup();

    //});
    var _wos = null;
    $(window).ready(function() {
        _wos = wos.CreateObject({
           id:'_wos'
        });
    });
</script>    
<?php require ("../footer2.php"); ?>

