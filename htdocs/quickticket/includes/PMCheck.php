<?php
	require_once($_SERVER['DOCUMENT_ROOT'] . "/library/caspioAPI.php");
	if (!isset($displayPMTools)) {
		$displayPMTools = true;
	}

	$db = Zend_Registry::get('DB');
	$select = $db->select();
	$select->from(Core_Database::TABLE_CLIENTS, "ProjectManager")
	        ->where("UserName = " . $db->quoteInto("?", $_SESSION['UserName']));
	$result = $db->fetchCol($select);
	if ($result === false)
		$isPM = FALSE; // username not found
	else
		$isPM = $result[0] == 1;

//	$isPM = false;
	$redirect = false;
	if ((!$isPM && ($_SESSION["Company_ID"] != $_GET['v'] || $_SESSION["Company_ID"] == "")) || $_GET["v"] == "") {
		if ($_SESSION["Company_ID"] == "") $_SESSION["Company_ID"] = "BLANKID"; // shouldn't happen but should be fail safe
		if ($_GET["v"] != "BLANKID")
			$redirect = true;
	}


//	echo "CID: " . $_SESSION["Company_ID"] . "<br/>v: " . $_GET["v"] . "<br/>";
//	echo "isPM: " . ($isPM ? "true" : "false");

	if ($isPM && isset($_GET['v'])) {
		$_SESSION['PM_Company_ID'] = $_GET['v'];
	} else {
		unset($_SESSION['PM_Company_ID']);
	}

    /**
     * if company has been changed - warm user
     * @author Artem Sukharev
     */
    $oldCampany  = isset($_GET['v1']) && !empty($_GET['v1']) ? $_GET['v1'] : '';
    $needWarm = isset($_GET['v1']) && !empty($_GET['v1']) ? true : false;
	if ($isPM && $displayPMTools) {
		$selectedCompany = $_GET["v"];
		$select = $db->select();
		$select->from(Core_Database::TABLE_CLIENTS, array("CompanyName", "Company_ID"))
			->distinct()
			->order(array("CompanyName"));
		$companyList = $db->fetchPairs($select);

//		$companyHtml = "<option value=\"\" " . ($_GET["v"] == "" ? "selected=\"selected\"" : "") . ">All Company</option>";
		$companyHtml = "";
		foreach ($companyList as $name => $id) {
			$companyHtml .= "<option value=\"$id\" " . ($_GET["v"] == $id ? "selected=\"selected\"" : "") . ">$name - $id</option>";
		}
?>
<div id="companySelect">
Select Company:
<select id="PMCompany" onchange="reloadPage(document.getElementById('PMCompany').value)">
    <?=$companyHtml?>
</select>
<?php if ( $needWarm ) : ?><span class="red">Company has been changed from "<?= $oldCampany ?>" to "<?= $_GET["v"] ?>".</span><?php endif; ?>
</div>
<?php }else{
	print "<br/>";
}?>
<script type="text/javascript">
	function reloadPage(company) {
		if (company == "BLANKID") {
            url = "./logIn.php";
        } else {
            url = "<?php echo $_SERVER['PHP_SELF']?>?v=" + company;
            if (typeof(wd) == "object") {
                url +='&tab='+wd.currentTab;
            }
        }
		document.location.replace(url);
	}
	<?php if ($redirect) : ?>
			reloadPage("<?php echo $_SESSION["Company_ID"]?>");
	<?php endif;?>
	var isPM = <?php echo ($isPM ? "true" : "false")?>;
</script>
