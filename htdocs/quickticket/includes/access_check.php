<?php
$CheckPages = array(
                array("name"=>"ProjectManagementReports.php","attrId"=>28)
              );

$currentPageVirtualPath = $_SERVER["SCRIPT_NAME"]; 
//---  $attrId
$attrId = 0;
foreach($CheckPages as $page)
{
    $pos = strpos($currentPageVirtualPath,$page['name']);    
    if($pos !== false) {$attrId= $page['attrId'];break;}
}
if($attrId>0)
{
    //--- Check authentication
    $login    = $_SESSION['Auth_User']['login'];
    $password = $_SESSION['Auth_User']['password'];
    if ( !$login || !$password ) {
        throw new Core_Auth_Exception('Authentication required');
    }
    //--- Client Info
    $user = new Core_Api_User();
    $clientInfo = $user->loadWithExt(array('login' => $login,'password' => $password));
    $FSClientServiceDirectorName = $clientInfo->FSClientServiceDirectorName;
    $FSClientServiceDirectorPhone = $clientInfo->FSClientServiceDirectorPhone;
    $ClientID = $clientInfo->ClientID;
    
    //--- Check Access Attribute Value
    $_SESSION['Auth_User']['restrictattrid']=null; 
    $attrValue = $user->GetAccessValueByClientAndAttribute($ClientID,$attrId);
    $_SESSION['Auth_User']['restrictattrid']=$attrId; 
    if($attrValue==$user->_restrictedValue)
    {
        //echo("<br/>vao khong?: ".$attrValue);
        //header('Location: /clients/dashboard.php') ;
        echo("<script>window.location='/clients/dashboard.php?restrictattrid=$attrId'</script>");
    }
    ///clients/dashboard.php?v=DELL
    //echo("<br/>attrValue: ".$attrValue);
    //echo("<br/>_restrictedValue: ".$user->_restrictedValue);
}
?>
