<?php
$page   = 'customers';
$option = 'createQuickTicket';
require ("header.php");
require ("../navBar.php");
?>

<script type="text/javascript">
    window._company = '<?=$_GET['v']?>';
    //  use for load images and save it in browser cache only!!!
    var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
    var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";
</script>

<script type="text/javascript" src="/widgets/js/jquery.min.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetCreateQuickTicket.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
<script type="text/javascript" src="/widgets/js/functions.js"></script>
<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<link rel="stylesheet" href="/widgets/css/popup.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.formatCurrency-1.4.0.js"></script>

<script type="text/javascript">
    function gotoTab(name) {
        location = location.toString().replace('<?=$_SERVER['SCRIPT_NAME']?>', '/clients/wos.php') + '&tab=' + name;
        return false;
    }
    /**
     *  calendarInit
     *
     *  Display filter post handler
     */
    function calendarInit() {
        $.map(['edit_StartDate', 'edit_EndDate','edit_DateNotified'], function( id ) {
            $('#'+id).calendar({dateFormat:'MDY/'});
        });
    }
    /**
     * calendarMove
     *
     * @param event  event
     * @access public
     * @return void
     */
    function calendarMove( input ) {
        $('#calendar_div').css('top', $(input).offset().top + $(input).height() + 6 + 'px');
        $('#calendar_div').css('left', $(input).offset().left + 'px');
        popUpCal.showFor(input);
    }
    var roll;
    var detailsWidget;
    $(document).ready(function() {
        roll = new FSPopupRoll();
        detailsWidget = new FSWidgetCreateQuickTicket({container:'detailsContainer',tab:'create'},roll);
        detailsWidget.show({tab:'create',params:{company:window._company,container:'detailsContainer',tab:"new"}});

        <?php if ($navBarState == 'closed'): ?>
            var btn = $('#navBarToggle');
            var bar = $('#leftVerticalContainer');
            var content = $('#detailsContainer');
            btn.text('>>');
            btn.parent().css('width', '100%');
            bar.css('display', 'none');
            bar.parent().css('width', '2%');
            bar.parent().css('background', '');
            content.css('width', '97%');
        <?php endif; ?>
    });
</script>
<!-- Main content -->
<div class="inner10 clr indentTop">
    <div id="detailsContainer"></div>
</div>

<?php require ("../footer.php"); ?>
