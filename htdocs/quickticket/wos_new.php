<?php
$page = "customers";
$option = "projectStatus";

require_once("../headerStartSession.php");
require ("../header.php");


$full_search = true;

if (isset($_GET['full_search']))
{
    $full_search = $_GET['full_search'];
    $sort = $_GET;
    $fillter = $_GET;
    if (isset($fillter['start_date']))
    {
        $fillter['start_date'] = str_replace('_', '/', $fillter['start_date']);
    }
    if (isset($fillter['end_date']))
    {
        $fillter['end_date'] = str_replace('_', '/', $fillter['end_date']);
    }
    if (isset($fillter['projects']))
    {
        $_projects = "";
        for ($i = 0; $i < count($fillter['projects']); $i++)
        {
            $_projects .= ",'" . $fillter['projects'][$i] . "'";
        }
        if ($_projects != "")
        {
            $_projects = '[' . substr($_projects, 1) . ']';
        }
        $fillter['projects'] = $_projects;
    }
}
?>
<script>
    var isPostBack = false;
    var dataSort = {};
    var dataFillter = {};

</script>
<?php
$Company_ID = (!empty($_GET['v']) ) ? $_GET['v'] : $_SESSION['Company_ID'];
$UserType = (!empty($_SESSION['UserType']) ) ? $_SESSION['UserType'] : NULL;
$isFLSManager = (strtolower($Company_ID) == 'fls' && (strtolower($UserType) === 'install desk' || strtolower($UserType) === 'manager'));
$Core_Api_User = new Core_Api_User;
$dbVersion = (isset($_POST['version']) && strtolower($_POST['version']) === 'lite') ? "lite" : strtolower($Core_Api_User->getdbVersion()) == "lite" ? 'lite' : 'full';
$_COOKIE["dashboardVersionDefault"] = ($dbVersion == 'lite') ? 'Lite' : 'Full';
//$dbVersion    = (isset($_POST['version']) && strtolower($_POST['version'])==='lite')?'lite':'full';
if ($isFLSManager)
{
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder');
} else
{
    $toolButtons = array('MyTechs', 'FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
}
?>
<script>
    //$(document).ready(function() {

    //    if (typeof(startup) == 'function') startup();

    //});
</script>    
<?php
require '../navBar.php';


//require 'includes/adminCheck.php';
//require 'includes/PMCheck.php';
?>

<script type="text/javascript" src="/widgets/js/functions.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetManager.js"></script>
<script type="text/javascript">
    var wManager = new FSWidgetManager();
</script>

<link rel=stylesheet type="text/css" href="/widgets/css/dashContent.css" />
<link rel=stylesheet type="text/css" href="/widgets/css/main.css" />
<link rel="stylesheet" type="text/css" href="/widgets/css/popup.css" />




<!-- Check if logged in, if not redirect -->
<script type="text/javascript">
    try {
        companyID = window._company      = '<?= $Company_ID ?>';
        window._isFLSManager = <?= ($isFLSManager) ? 'true' : 'false'; ?>;
        window._search       = <?= (isset($_GET['search']) && $_GET['search'] + 0) ? 'true' : 'false'; ?>;
        var test = companyID;
        if ( !isPM && companyID !== window._company ) {
            //window.location.replace("<?= $_SERVER['PHP_SELF'] ?>?v=" + companyID);
        }
    } catch (e) {
        //window.location.replace("./");
    }
    //  use for load images and save it in browser cache only!!!
    var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
    var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";
</script>

<script type="text/javascript" src="/widgets/js/jquery.form.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetDashContentLeft.js?11222011"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetDashContentRight.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetDashboardNew.js?11232011"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetSortTool.js?11112011"></script>
<?php if (strtolower($Company_ID) == 'fls'): ?>
    <script type="text/javascript" src="/widgets/js/FSWidgetSortToolFLS.js"></script>
<?php endif; ?>
<script type="text/javascript" src="/widgets/js/FSWidgetDeactivationTool.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetActivityLog.js"></script>
<script type="text/javascript" src="/widgets/js/FSQuickAssignTool.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/jquery-ui.js"></script>
<script type="text/javascript" src="/widgets/js/livevalidation.js"></script>
<script type="text/javascript" src="/library/jquery/contentmenu/jquery.contextmenu.js"></script>
<link rel="stylesheet" href="/library/jquery/contentmenu/jquery.contextmenu.css" type="text/css" />
<script type="text/javascript">
    //Default validation settings
    var LVDefaultFunction = function(){this.addFieldClass();}
    var LVDefaults = {
        onValid : LVDefaultFunction,
        onInvalid : LVDefaultFunction
    }

    var pmContext = "<?= $_GET["v"] ?>";

    function calcBasePay(){
        if ($("#Approve_Amount_Per").val() != "Hour") return;
        var bidAmount = $("#Approve_Tech_Bid_Amount").text();
        bidAmount = String(bidAmount);
        var parts = bidAmount.split(" / ");
        bidAmount = parseFloat(parts[0]) * ($("#Approve_calculatedTechHrs").val()*1);
        $("#Approve_baseTechPay").val(bidAmount).change();
    } 
    var _menu  = [ 
        {'Open new Tab/Window':function(menuItem,menu) { 
                var href = jQuery(this).attr("href");
                if(typeof(href) != 'undefined' && jQuery.trim(href) != "") 
                {
                    if(href.indexOf('?') >= 0)
                    {    
                        href += "&closing=1";      
                    }
                    else
                    {
                        href += "?closing=1";  
                    }   
                    var uiTab = window.open(href); 
                }
            } }
    ];
</script>

<script type="text/javascript" src="/widgets/js/resizable.js"></script>

<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
<script type="text/javascript" src="/widgets/js/ProgressBar.js"></script>
<script type="text/javascript" src="/widgets/js/ProgressBarAdapterAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/ProgressBarAdapterPopupSpin.js"></script>


<script type="text/javascript" src="/widgets/js/FSTechPayUtils.js"></script>

<link rel="stylesheet" href="/widgets/css/resizable.css" type="text/css" />

<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript">
    /**
     *  calendarInit
     *
     *  Display filter post handler
     */
    function calendarInit() {
        $.map(['filterStartDate', 'filterEndDate'], function( id ) {
            $('#' + roll.container().id + ' #' + id).focus( function() {
                $('#calendar_div').css('z-index', ++roll.zindex);
                $(this).calendar({dateFormat:'MDY/'});
            });
        });
    }
    /**
     * calendarMove
     *
     * @param event  event
     * @access public
     * @return void
     */
    function calendarMove( input ) {
        $('#calendar_div').css('top', $(input).offset().top + $(input).height() + 6 + 'px');
        $('#calendar_div').css('left', $(input).offset().left + 'px');
        popUpCal.showFor(input);
    }
    function toggleChecks(cbox)
    {
        switch ( wd.currentTab ) {
            case 'assigned' :
                $.each($('#widgetContainer input[name="rePublishCheckbox"]'), function (idx, item){ 
                    item.checked =  cbox.checked;
                    var win = /^rePublishCheckbox_(\d+)$/.exec($(item).attr('id'))[1];

                    if ( item.checked ) {
                        $("#rePublishOptions_" + win).removeClass('displayNone');
                    } else {
                        $("#rePublishOptions_" + win).addClass('displayNone');
                    }
                });
                break;
            default :
                $.each($('#widgetContainer input[name="approveCheckbox"]'), function (idx, item){ item.checked = cbox.checked; });
                break;
        }
    }
    /**
     * doApprove
     *
     * @access public
     * @return void
     */
    function doApprove() {
        var wos = [];
        var _url;
        $.each($('#widgetContainer input[name="approveCheckbox"]'), function (idx, item){ if (item.checked) wos.push($(item).val()); });

        var payAmounts = {};

        switch ( wd.currentTab ) {
            case 'workdone' :
            case 'incomplete' :
                _url = '/widgets/dashboard/do/approve/';
                progressBar.key('approve');

                var wosCount = wos.length;
                for (var i = 0; i < wosCount; i++) {
                    var wId = wos[i];
                    payAmounts[wId] = $("#approvePayAmount_" + wId).val();
                }
                break;
            case 'created'  :
                _url = '/widgets/dashboard/do/publish/';
                progressBar.key('publish');
                break;
            default         : return false; //  Return from function on any other tabs
        }

        if ( wos.length ) {
            progressBar.run();

            $.ajax({
                url         : _url,
                dataType    : 'json',
                data        : { wo_ids : wos, payAmounts: payAmounts, company: pmContext},
                cache       : false,
                type        : 'post',
                context     : wd,
                success     : function (data, status, xhr) {
                    this.show({ tab:this.currentTab, params:this.getParams() });
                },
                error       : function ( xhr, status, error ) {
                    progressBar.stop();
                    this.show({ tab:this.currentTab, params:this.getParams() });
                }
            });
        }
    }
    /**
     * toggleTab
     *
     * Switch Tabs
     *
     * @param string name Name of currect Tab
     * @param object link Tab's link object
     * @param object wd wd object
     * @return false always
     */
    var hasData = false;
    function toggleTab( name, link, wd, withPage ) {
        /* Disable full search */
        var attributeId = jQuery(link).attr("attributeId");
        //wd.ajaxSuccess = function() { }; 
        $.post("/widgets/dashboard/client/accessctl/", {
            rt:'iframe',
            attributeId:attributeId
        }, 
        function (response) {
            if(response=="1") { 
                hasData = true;
                if ( name !== 'all' && (window._search || wd.filters().fullSearch() == 'true' || wd.filters().fullSearch() === true)) {
                    wd.resetFilters();
                    window._search = false;
                }

                if (wd.currentTab != name) {
                    //wd.resetFilters();
                    wd.params.isBid = 0;
                    wd.currentTab = name;
                    wd.sortTool().prepareItems(version);
                    wd.sortTool().initSortTool(); 
                    wd.params.ajaxSuccess = null;
                }

                if (withPage == undefined) withPage = false;
                var ajaxSuccess = null;
                wd.show({tab:wd.currentTab,params:wd.getParams(withPage)});
                link.blur();
                $("#toppaginatorContainer").html("");

                //set default state, then add custom classes.

                $('#dashboardVersion').removeAttr('disabled');
                $('MyTechsBtn').addClass("displayNone");
                jQuery('.toolGroup').css('display',"");
                
            } 
            else {
                if(!hasData)
                {
                    jQuery('.toolGroup').css('display',"none");
                }
                
                Attention(response);
            }                        
        });
        
       
        return false;
    }

    var wd;             //  Widgets object
    var roll;           //  Rollover object
    var popup;          //  Rollover object
    var progressBar;    //  Progress bar object
    var activityLog;    //  Activity Log object

    function saveDBState() {
        p = wd.getParams();
        p.page = wd.sortTool()._page;
        try {
            $.cookie('FSWidgetDashboardState', JSON.stringify({tab: wd.currentTab, params: p}));
        } catch (e) {}
    }

    try {
        window.onbeforeunload = function() {
            try {
                saveDBState();
            } catch (e) {}
            return;
        };
    } catch (e) {
        $(window).unload(function() {
            try {
                saveDBState();
            } catch (e) {}
        });
    }

    function startup() {
        var _ajaxSuccess = function(fun,_el,data) { 

            $.post("/widgets/dashboard/client/accessctl/", {
                rt:'iframe',
                attributeId:17
            }, 
            function (response) {

                if(response=="1") { 
                    //wd = new FSWidgetDashboard({container:'widgetContainer',tab:'workdone',dbVersion:'<?= $dbVersion ?>',saveStateCallback: saveDBState});    
                    hasData = true;
                    if(typeof(fun) =='function')
                    {
                        fun(_el,data);
                    }
                    return false;
                } 
                else {
                    jQuery("#widgetContainer").html("");
                    jQuery('.toolGroup').css('display',"none");
                    Attention(response); 
                    return true;
                }   
                /* Find tag in URL params */
            }); 
        };
        var tab = '<?= (!empty($_GET['tab'])) ? strtolower($_GET['tab']) : '__invalid__'; ?>';
        var projectid = '<?= (!empty($_GET['projectid'])) ? strtolower($_GET['projectid']) : ''; ?>';
                    
        dataFillter.projects = projectid;

        var isBid = 0;
        if(projectid != '')
        {
            isBid = '<?= (!empty($_GET['techid'])) ? strtolower($_GET['techid']) : ''; ?>';  
        }
                    
        wd = new FSWidgetDashboard({
            container:'widgetContainer',
            tab:'quickview',
            dbVersion:'<?= $dbVersion ?>',
            isBid:isBid,
            saveStateCallback: saveDBState,
            ajaxSuccess:_ajaxSuccess});  
        wd.setParamsSession(dataFillter);
        if ( wd._isValidTab(tab) ) {
            wd.currentTab = tab;
        }
        /* Find tab in URL params */

        roll        = new FSPopupRoll();
        progressBar = new ProgressBar();
        activityLog = new FSWidgetActivityLog( roll );

        wd.htmlFilters();
<?php if (strtolower($Company_ID) == 'fls') : ?>
            wd.sortTool(new FSWidgetSortToolFLS('sortSelect', 'directionSelect', wd, roll, 'quick'));
<?php else : ?>
            wd.sortTool(new FSWidgetSortTool('sortSelect', 'directionSelect', wd, roll, 'quick'));
<?php endif; ?>
                    
        wd.deactivationTool(new FSWidgetDeactivationTool(wd,roll));



        /*  Load tab by parameters in URL */
        savedState = $.cookie('FSWidgetDashboardState');
        parse = wd.parseUrl();
        size = 0;
        for (key in parse) {
            ++size;
        }
        if (size > 1) savedState = null;
        if (!savedState) {
            wd.setParams(parse);
            toggleTab(wd.currentTab, $('#'+wd.currentTab), wd);
        }
        /*  Load tab by parameters in URL */

        else {
            $.cookie('FSWidgetDashboardState', null);
            state = JSON.parse(savedState);
            wd.setParams(state.params);
            wd.currentTab = state.params.tab;
            //            wd.sortTool().prepareItems(version);
            wd.sortTool().initSortTool();
            $('#sortSelect').val(state.params.sort1);
            $('#directionSelect').val(state.params.dir1);
            wd.sortTool().update();
            wd.sortTool().setPage(state.params.page);
            try {
                // IE 7 workaround
                a = typeof state.params.projects[0];
                if (a == 'function') {
                    state.params.projects.reverse();
                    state.params.projects.pop();
                    state.params.projects.reverse();
                }
                wd.filters().setProject(state.params.projects);
            } catch (e){}
            //		wd.show(state);
            toggleTab(wd.currentTab, $('#'+wd.currentTab), wd, true);

            //	        toggleTab(state.tab, $('#'+state.tab), wd);
        }
        progressBar.key('approve');
        progressBar.adapter(new ProgressBarAdapterPopupSpin(progressBar, null));
        jQuery("#widgetContainer").html("");
        
        if(dataSort.sort1 !== "" && typeof(dataSort.sort1) != 'undefined')
        {
            jQuery("#sortSelect").val(dataSort.sort1);  
            jQuery("#directionSelect").val(dataSort.dir1);  
        }

        loadContent ();

        return false;
    }

    function reloadTabFrame() {
        wd.show({tab: wd.currentTab, params: wd.getParams(true)});
    }

</script>


<div id="container" class="use-sidebar-right use-sidebar-left">

    <div id="header" style="width: 100%">
        <div id="wrapper">
            <div id="content" align="center"></div>
        </div>

        <!-- DASHBOARD FILTER -->
        <div id="projectSearch" onclick="return true;">
            <form id="dashFilter" name="dashFilter" action="<?= $_SERVER['PHP_SELF'] ?>?v=<?= $_GET["v"] ?>" method="post">
                <div id="contents">
                    <?php require("includes/getWOCount.php"); ?>

                    <div class="toolGroup">
                        <div class="toolGroupLeft" style="min-width: 650px">
                            <div class="toolBtn">
                                <input id="sortButton" type="button" value="Sort" class="link_button" />
                            </div>
                            <div class="toolBtn">
                                <input id="downloadButton" type="button" value="Download" class="link_button download_button" />
                            </div>
                        </div>
                        <div class="toolGroupRight">
                            <div class="toolBtn topSm">
                                Quick Sort
                            </div>
                            <div class="toolBtn">
                                <select id="sortSelect" onchange="return wd.sortTool().applySort('quick');"></select>
                            </div>
                            <div class="toolBtn" style="margin:0;">
                                <select id="directionSelect" onchange="return wd.sortTool().applySort('quick');"></select>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        
                        var rightNavWidget;

                        var checkMinWidth = function(){
                            if(objMain.hasClass('use-sidebar-left') && !objMain.hasClass('use-sidebar-right')){

                                $("#mainContainer").css("width", "100%");
                                $("#header").css("width", "77%");
								
                            }
                            if(objMain.hasClass('use-sidebar-left') && objMain.hasClass('use-sidebar-right')){

                                $("#mainContainer").css("min-width", "950px");
                                $("#header").css("width", "54%");
                            }

                            if(objMain.hasClass("use-sidebar-right") && !objMain.hasClass("use-sidebar-left")){

                                $("#mainContainer").css("min-width", "950px");
                                $("#header").css("width", "77%");
                            }

                            if(!objMain.hasClass("use-sidebar-right") && !objMain.hasClass("use-sidebar-left")){

                                $("#mainContainer").css("min-width", "750px");
                                $("#header").css("width", "100%");
                            }
                        };

                        //$(document).ready(function() {
                        function loadContent () {
		
                            objMain = $("#container");

                            dr = new FSWidgetDashContentRight({container:'dashrightcontent', objMain: objMain, checkMinWidth: checkMinWidth, company:window._company});
<?php if (!$isFLSManager): ?>
            dr.show();
            dr.separatorClick();
<?php endif; ?>
        dl = new FSWidgetDashContentLeft({container:'dashleftcontent', objMain: objMain, dr: dr, checkMinWidth: checkMinWidth, company:window._company});
<?php if (!$isFLSManager): ?>
            dl.show();

            dl.separatorClick();
<?php endif; ?>

        //		rightNavWidget = new FSWidgetDashboard({container:'rightsidebar',tab:'rightside',customfunctions:{showPreloader:function(){}}},null);
        //        rightNavWidget.show({tab:'rightside',params:{companyId:window._company,container:'rightsidebar'}});


		
        $("#activityLogButton").click(function(event) {
            roll.autohide(false);
            var opt = {
                width       : 604,
                height      : '',
                title       : 'View Activity Log',
                body        : activityLog.buildHtml(),
                context     : activityLog,
                handler     : activityLog.initActivityLogTool
            };
            roll.showNotAjax(this, event, opt);
        });
<?php /* if ($isFLSManager) : */ ?>
        /*$("#QuickAssignBtn").click(function(event) {
                                roll.autohide(false);
                                var opt = {
                                    popup       : 'quickassigntool',
                                    title       : 'Quick Assign',
                                    width       : 400,
                                    preHandler  : assignTool.getWidgetHtml,
                                    postHandler : assignTool.initEvents,
                                    context     : assignTool,
                                    delay       : 0
                                }
                                roll.show(this,event,opt);
                            });*/

        /*$("#QuickApproveBtn").click(function(event) {
                                roll.autohide(false);
                                var opt = {
                                    popup       : 'quickapprove',
                                    title       : 'Quick Approve',
                                    width       : 400,
                                    context     : wd,
                                    preHandler  : wd.getWidgetHtml,
                                    delay       : 0,
                                    postHandler : function() {
                                            $("#Approve_approveForm").ajaxForm({dataType: 'json', success: function(data) {
                                            if (data.error == 0) {
                                                roll.hide();
                                                reloadTabFrame();
                                            } else {
                                                $('#woErrorMsg').html(data.msg);
                                                $('#woErrorMsg').show('fast',function(){
                                                    setTimeout(function () { $('#woErrorMsg').hide(500); }, 3000);
                                                });
                                            }
                                                    }, beforeSubmit: function() {
                                                    progressBar.key('updatepayfinan');
                                                    progressBar.run();
                                                    return true;
                                                    }});
                                            }
                                }
                                roll.show(this,event,opt);
                            });*/
        $("#paperworkBtn").click(function(event) {
            params = [];
            $("input[name=paperwork_received_data]:checked").each(function(index,value){params[index]=$(value).val()});
            if (params.length>0) {
                roll.autohide(false);
                var opt = {
                    width       : 300,
                    height      : '',
                    position    : 'middle',
                    title       : 'Processing',
                    body        : "<div class=\"tCenter\"><img alt='Wait...' src='/widgets/images/progress.gif' /></div>"
                };
                roll.showNotAjax(null, null, opt);
                $.ajax({
                    url:'/widgets/dashboard/do/receive-paperwork/',
                    cache:false,
                    type:'POST',
                    data: {'ids':params,'company':window._company},
                    success: function(data) {
                        roll.hide();
                        reloadTabFrame();
                    }
                });
            }
        });

<?php /* endif; */ ?>
        $("#MassApproveBtn").click(function (event) {
            
            $.post("/widgets/dashboard/client/accessctl/", 
            {rt:'iframe',attributeId:8}, 
            function (response) {
                if(response=="1") { doApprove();} 
                else { Attention(response); }                        
            });
        });
        
        $("#MassAssignBtn").click(function (event) {
            $.post("/widgets/dashboard/client/accessctl/", 
            {rt:'iframe',attributeId:3}, 
            function (response) {
                if(response=="1") { assignTool.massAssign();} 
                else { Attention(response); }                        
            });

        });

        $("#MassPublishBtn").click(function(event) {
            $.post("/widgets/dashboard/client/accessctl/", 
            {rt:'iframe',attributeId:2}, 
            function (response) {
                if(response=="1") { 
                    assignTool.massPublish();
                } 
                else { Attention(response); }                        
            });
            
        });
        //                    wd.setParamsSession(dataFillter);

        $("#downloadButton").click(function(event){
            var opt = wd.getParams();
            opt['download']='csv';
            opt['companyid'] = '<?= $_GET['v'] ?>';
            wd.open({
                tab     : wd.currentTab,
                params  : opt
            });
        });

        $("#sortButton").click(function(event){
            wd.sortTool().switchSortTools('full');
            roll.autohide(false);
            var opt = {
                width   : 325,
                height  : 225,
                title   : 'Sort Work Orders...',
                context : wd.sortTool(),
                handler : wd.sortTool().initSortPopup,
                body    : wd.sortTool().makeFullSortPopup()
            };
            roll.showNotAjax(this,event,opt);
        });

        

        //});
    }
                    </script>

                    <br class="clear" />
                    <div id="tabbedMenu" style="height:16px;">
                        <script>
                            function onChangePageSize()
                            {
                                wd.sortTool().setPage(1);
                                reloadTabFrame();
                            }
                        </script>
                        <div id="toppaginatorSizeContainer"></div>
                        <table cellspacing='0' cellpadding='0' class="paginator_top" style="float:right; ">
                            <tr>
                                <td class="tCenter" id="toppaginatorContainer"></td>
                            </tr>
                        </table>

                    </div>
                </div>
                <br class="clear" />

                <div id="widgetContainer"></div>

        </div>
        </form>
    </div>
    <div id="dashrightcontent"></div>
</div></div>
<!-- End Content -->
<script>
    //$(document).ready(function() {

    if (typeof(startup) == 'function') startup();

    //});
</script>    
<?php require ("../footer2.php"); ?>
