<?php require_once("../library/timeStamps2.php");?>
<div align="center"><br/>
<?php
	try {
		require_once("../library/caspioAPI.php");
		require_once("../library/smtpMail.php");
		require_once("../library/pricing.php");
		$db = Zend_Registry::get('DB');
//		ini_set("display_errors", 1);
		set_time_limit(800);


		$InvoiceDate = $_COOKIE['defaultDate2'];
		$CompanyID = $_REQUEST['CompanyID'];
		$projectID = $_REQUEST['projectID'];
		$dateFrom = $_REQUEST['dateFrom'];
		$dateTo = $_REQUEST['dateTo'];
		
		$InvoiceDate = date("Y-m-d", strtotime($InvoiceDate));
		$dateFrom = empty($dateFrom) ? "" : date("Y-m-d", strtotime($dateFrom));
		$dateTo = empty($dateTo) ? "" : date("Y-m-d", strtotime($dateTo));

		$CompanyQuery = ($CompanyID != "" ? "AND Company_ID = '$CompanyID'" : "");
		$customQuery = "Deactivated = '0' AND Approved = '1' AND Invoiced = '0'";

		if ($projectID != "" && $projectID != 0){
			$customQuery .= "AND Project_ID = '$projectID'";
		}
		if ($dateFrom != ""){
			$customQuery .= "AND StartDate >= '$dateFrom'";
		}
		if ($dateTo != ""){
			$customQuery .= "AND StartDate <= '$dateTo'";
		}

//		$records = caspioSelectAdv("Work_Orders", "TB_UNID, Company_ID, Company_Name, Project_Name, Tech_ID", "$customQuery $CompanyQuery", "", false, "`", "|", false);
		
		$sql = "SELECT WIN_NUM, Company_ID, Company_Name, Project_Name, Tech_ID FROM work_orders WHERE $customQuery $CompanyQuery";
		$records = $db->fetchAll($sql, array(), Zend_Db::FETCH_NUM);

		$fieldList = array("Invoiced", "DateInvoiced", "Status");
		$valuesList = array(1, "$InvoiceDate", "in accounting");

		echo "Updating " . sizeof($records) . " records to 'Invoiced' with an Invoice Date of: " . $InvoiceDate . "<br/><br/>";

		$techList = array();
		$invoicedList = array();

		foreach ($records as $row) {
			if ($row == "") continue;
			$row[1] = trim($row[1], "`");
			$row[2] = trim($row[2], "`");
			$row[3] = trim($row[3], "`");
			$techList[] = $row[4];
			$invoicedList[] = $row[0];
			echo $row[0] . " : " . $InvoiceDate . "<br />";

			Create_TimeStamp($row[0], "Accounting", "Work Order In Accounting", $row[1], $row[2], $row[3], "", "", "");
			flush();
			ob_flush();
		}

//		$records = caspioUpdate("Work_Orders", $fieldList, $valuesList, "Approved = '1' AND Invoiced = '0' AND Deactivated = '0' AND Project_ID = '$projectID' AND StartDate >= '$dateFrom' AND StartDate <= '$dateTo' $CompanyQuery", false);
//		$records = caspioUpdate("Work_Orders", $fieldList, $valuesList, "$customQuery $CompanyQuery", false);

		$updateFields = array_combine($fieldList, $valuesList);

		$records = $db->update('work_orders', $updateFields, "$customQuery $CompanyQuery");

		echo "<br/><br/>" . $records . " updated!";

		set_time_limit(800);
		echo "<br/><br/>Running pricing on recently approved WOs<br/><br/>";
		flush();
		ob_flush();
		
		$sql = "SELECT " . WHICH_PRICING_RULE . ", WIN_NUM FROM work_orders WHERE WIN_NUM IN (" . implode(",", $invoicedList) . ") AND Invoiced = '1' AND PricingRan = '0'";
		$woList = $db->fetchAll($sql, array(), Zend_Db::FETCH_NUM);

		//FIXME input parameter of batchPricingCalculationWithList was changed
		$result = batchPricingCalculationWithList($woList);


		$message = "Total WOs: {$result[0]}
		Calc Ran: {$result[1]}
		Errors:

		{$result[2]}";
		$messageHTML = nl2br($message);
		smtpMail("Pricing Invoice Calculation", "nobody@fieldsolutions.com", "tngo@fieldsolutions.com, jsinning@fieldsolutions.com", "Invoice Calculations Ran (from invoice script)", $message, $messageHTML, "Invoice Calculation");


		$records = caspioSelectAdv("TR_Master_List", "PrimaryEmail", "TechID IN (" . implode(",", $techList) . ") AND ISNULL(ISO_Affiliation_ID, 0) = 0 AND W9 = '0'", "", false, "`", "|", false);

		$emailList = array();
		foreach ($records as $value) {
			if ($value == "") continue;
			$emailList[] = trim($value, "`");
		}

//		print_r($emailList);

		if (sizeof($emailList) > 0) {
			// Email Techs W9 Reminders
			$message = "Field Solutions must have a W-9 on record prior to paying you for work.  Unless your W-9 is received today, payment will be delayed until the next processing period.

	Instructions for submitting your W-9 can be found by clicking within the HELP tab after logging in to the site, or by clicking https://www.fieldsolutions.com/techs/w9.php.

	If you are submitting your W-9 in a company name, please include your name and/or your technician ID somewhere on the W-9.

	Fax your completed W-9 to 888-258-1656, or email it to accounting@fieldsolutions.com.  W-9's are recorded as received in your profile within 1 business day.  An email will be sent to you acknowledging receipt.

	Thank you for your prompt attention to this matter.

	Sincerely,

	Accounting
	Field Solutions
	";

//			$message .= print_r($emailList, true);
			$htmlmessage = nl2br($message);

//			smtpMailLogReceived("Field Solutions Accounting", "accounting@fieldsolutions.com", "codem01@gmail.com", "Payment for work will be delayed unless W-9 is received today", $message, $htmlmessage, $caller);

			// Emails Tech
			smtpMailLogReceived("Field Solutions Accounting", "accounting@fieldsolutions.com", implode(",", $emailList), "Payment for work will be delayed unless W-9 is received today", $message, $htmlmessage, $caller);
		}

		echo "<br/>done<br/>";

//		print_r( caspioSelect("Work_Orders_TN", "COUNT(TB_UNID)", "Approved = '1' AND Invoiced = '0'", ""));

/*		// NOTE: GROUP BY is contained in criteria field and after at least one criteria. This is a hack of the Caspio API and may not continue to work in the future
		$records = caspioSelect("Work_Orders", "TB_UNID", "Approved = '1' AND Invoiced = '0'", "TB_UNID ASC", false);

		echo "Updating " . sizeof($records) . " records to 'Invoiced' with an Invoice Date of: " . $InvoiceDate;

		$fieldList = "Invoiced, DateInvoiced";
		$valuesList = "1, '$InvoiceDate'";

		foreach ($records as $row) {

			caspioUpdate("Work_Orders", $fieldList, $valuesList, "Approved = '1' AND Invoiced = '0'", false);

		}
		echo "<br /><br />" . sizeof($records) . " updated!";*/

	}
	catch (SoapFault $fault) {
//		smtpMail("Update FLS Invoiced Script", "nobody@fieldsolutions.com", "gerald.bailey@fieldsolutions.com", "Update FLS Invoiced Script Error", "$fault", "$fault", "Staff-Update MainSite Invoiced WOs.php");
	}

?>
</div>
<!--- End Content --->
