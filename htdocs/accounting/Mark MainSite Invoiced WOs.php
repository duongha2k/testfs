<?php $page = 'accounting'; ?>
<?php $option = 'accounting'; ?>
<?php $selected = ''; ?>
<?php require_once("../library/timeStamps2.php");?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->
<div align="center"><br/>
<a href="./">Return to Accounting Page</a><br/>
<?php
	try {
		require_once("../library/caspioAPI.php");
		require_once("../library/smtpMail.php");

		$InvoiceDate = $_REQUEST['InvoiceDate'];
		$CompanyID = $_REQUEST['CompanyID'];

//		ini_set("display_errors", 1);

		set_time_limit(800);
		$CompanyQuery = ($CompanyID != "" ? "AND Company_ID = '$CompanyID'" : "");

//		$records = caspioSelect("Work_Orders", "TB_UNID", "Approved = '1' AND Invoiced = '0' $CompanyQuery", "");
		$records = caspioSelectAdv("Work_Orders", "TB_UNID, Company_ID, Company_Name, Project_Name, Tech_ID", "Deactivated != '0' AND Approved = '1' AND Invoiced = '0' $CompanyQuery", "", false, "`", "|", false);

		$fieldList = "Invoiced, DateInvoiced";
		$valuesList = "1, '$InvoiceDate'";

		echo "Updating " . sizeof($records) . " records to 'Invoiced' with an Invoice Date of: " . $InvoiceDate . "<br/><br/>";

		$techList = array();
		$invoicedList = array();

		foreach ($records as $row) {
			if ($row == "") continue;
			$row = explode("|", $row);
			$row[1] = trim($row[1], "`");
			$row[2] = trim($row[2], "`");
			$row[3] = trim($row[3], "`");
			$techList[] = $row[4];
			$invoicedList[] = $row[0];
			echo $row[0] . " : " . $InvoiceDate . "<br />";
			Create_TimeStamp($row[0], "Accounting", "Work Order In Accounting", $row[1], $row[2], $row[3], "", "", "");
			flush();
			ob_flush();
		}

		$records = caspioUpdate("Work_Orders", $fieldList, $valuesList, "Approved = '1' AND Invoiced = '0' $CompanyQuery", false);

		echo "<br/><br/>" . $records . " updated!";

		set_time_limit(800);
		echo "<br/><br/>Running pricing on recently approved WOs<br/><br/>";
		flush();
		ob_flush();
		require_once("../library/pricing.php");
		$woList = caspioSelectAdv("Work_Orders", WHICH_PRICING_RULE . ", TB_UNID", "PricingRan = '0' AND TB_UNID IN ('" . implode("','", $invoicedList) . "')", "", false, "`", "|", false);
		//FIXME input parameter of batchPricingCalculationWithList was changed
		batchPricingCalculationWithList($woList);

		$records = caspioSelectAdv("TR_Master_List", "PrimaryEmail", "TechID IN (" . implode(",", $techList) . ") AND ISNULL(ISO_Affiliation_ID, 0) = 0 AND W9 = '0'", "", false, "`", "|", false);

		$emailList = array();
		foreach ($records as $value) {
			if ($value == "") continue;
			$emailList[] = trim($value, "`");
		}

//		print_r($emailList);

		if (sizeof($emailList) > 0) {
			// Email Techs W9 Reminders
			$message = "Field Solutions must have a W-9 on record prior to paying you for work.  Unless your W-9 is received today, payment will be delayed until the next processing period.

	Instructions for submitting your W-9 can be found by clicking within the HELP tab after logging in to the site, or by clicking https://www.fieldsolutions.com/techs/w9.php.

	If you are submitting your W-9 in a company name, please include your name and/or your technician ID somewhere on the W-9.

	Fax your completed W-9 to 888-258-1656, or email it to accounting@fieldsolutions.com.  W-9's are recorded as received in your profile within 1 business day.  An email will be sent to you acknowledging receipt.

	Thank you for your prompt attention to this matter.

	Sincerely,

	Accounting
	Field Solutions
	";

//			$message .= print_r($emailList, true);
			$htmlmessage = nl2br($message);

//			smtpMailLogReceived("Field Solutions Accounting", "accounting@fieldsolutions.com", "codem01@gmail.com", "Payment for work will be delayed unless W-9 is received today", $message, $htmlmessage, $caller);

			// Emails Tech
			smtpMailLogReceived("Field Solutions Accounting", "accounting@fieldsolutions.com", implode(",", $emailList), "Payment for work will be delayed unless W-9 is received today", $message, $htmlmessage, $caller);
		}

		echo "<br/>done<br/>";

//		print_r( caspioSelect("Work_Orders_TN", "COUNT(TB_UNID)", "Approved = '1' AND Invoiced = '0'", ""));

/*		// NOTE: GROUP BY is contained in criteria field and after at least one criteria. This is a hack of the Caspio API and may not continue to work in the future
		$records = caspioSelect("Work_Orders", "TB_UNID", "Approved = '1' AND Invoiced = '0'", "TB_UNID ASC", false);

		echo "Updating " . sizeof($records) . " records to 'Invoiced' with an Invoice Date of: " . $InvoiceDate;

		$fieldList = "Invoiced, DateInvoiced";
		$valuesList = "1, '$InvoiceDate'";

		foreach ($records as $row) {

			caspioUpdate("Work_Orders", $fieldList, $valuesList, "Approved = '1' AND Invoiced = '0'", false);

		}
		echo "<br /><br />" . sizeof($records) . " updated!";*/

	}
	catch (SoapFault $fault) {
//		smtpMail("Update FLS Invoiced Script", "nobody@fieldsolutions.com", "gerald.bailey@fieldsolutions.com", "Update FLS Invoiced Script Error", "$fault", "$fault", "Staff-Update MainSite Invoiced WOs.php");
	}

?>
</div>
<!--- End Content --->
<?php require ("../footer.php"); ?>