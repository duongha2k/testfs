<?php
//	ini_set("display_errors", 1);

	function escapeCSVField($value) {
		$delimiter = ",";
		$enclosure = "\"";
		if (strpos($value, ' ') !== false ||
			strpos($value, $delimiter) !== false ||
			strpos($value, $enclosure) !== false ||
			strpos($value, "\n") !== false ||
			strpos($value, "\r") !== false ||
			strpos($value, "\t") !== false)
		{
			$str2 = $enclosure;
			$escaped = 0;
			$len = strlen($value);
			for ($i=0;$i<$len;$i++)
			{
				if (!$escaped && $value[$i] == $enclosure)
					$str2 .= $enclosure;
				$str2 .= $value[$i];
			}
			$str2 .= $enclosure;
			$value = $str2;
		}
		return $value;
	}

	if (isset($_GET["download"])) {
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE')) {
			session_cache_limiter("public");
		}
		session_name("fsTemplate");
		session_start();
		// download csv
		require_once("../library/openSSL.php");
		require_once("../library/mySQL.php");
		require_once("../library/caspioAPI.php");
		$privateKey = getBankingPrivateKey($_SESSION["BankingInfoPW"]);
		if (!$privateKey) {
			header("Location: bankingLogin.php");
			die();
		}

		header("Content-disposition: attachment; filename=TechBankingInfo.csv");

		$result = mysqlQuery("SELECT TechID, FirstName, LastName, PaymentMethod, DATE_FORMAT(DateChange, '%m/%d/%Y %h:%i:%s') AS DateChange, DepositType, BankName, BankAddress1, BankAddress2, BankCity, states_list.State AS BankState, BankZip, Countries.Name AS BankCountry, AccountName, RoutingNum, AccountNum FROM TechBankInfo LEFT JOIN states_list ON states_list.Abbreviation = BankState LEFT JOIN Countries ON Countries.id = BankCountry " . $_SESSION["searchCriteria"]);
		
		$techList = array();
		$idList = array();
		
		while ($row = mysql_fetch_assoc($result)) {
			$techID = $row["TechID"];
			$techList[$techID] = $row;
			$techList[$techID]["RoutingNum"] = decryptData($techList[$techID]["RoutingNum"], $privateKey);
			$techList[$techID]["AccountNum"] = decryptData($techList[$techID]["AccountNum"], $privateKey);
			$idList[] = $techID;
		}
		
		$idList = implode(",", $idList);
		$contactInfo = caspioSelect("TR_Master_List", "TechID, PrimaryPhone, PrimaryEmail", "TechID IN ($idList)", "");
		foreach ($contactInfo as $info) {
			$info = explode(",", $info);
			$techList[$info[0]]["PrimaryPhone"] = trim($info[1], "'");
			$techList[$info[0]]["PrimaryEmail"] = trim($info[2], "'");
		}
		
		echo "FSTechID,FirstName,LastName,PrimaryPhone,PrimaryEmail,PaymentMethod,DateModified,AccountType,BankName,BankAddress1,BankAddress2,City,State,Zip,Country,NameonAccount,Routing#,Account#\n";
		foreach ($techList as $tech) {
			$tech['FirstName'] = escapeCSVField($tech['FirstName']);
			$tech['LastName'] = escapeCSVField($tech['LastName']);
			$tech['PrimaryPhone'] = escapeCSVField($tech['PrimaryPhone']);
			$tech['PaymentMethod'] = escapeCSVField($tech['PaymentMethod']);
			$tech['BankName'] = escapeCSVField($tech['BankName']);
			$tech['BankAddress1'] = escapeCSVField($tech['BankAddress1']);
			$tech['BankAddress2'] = escapeCSVField($tech['BankAddress2']);
			$tech['BankCity'] = escapeCSVField($tech['BankCity']);
			$tech['BankState'] = escapeCSVField($tech['BankState']);
			$tech['BankZip'] = escapeCSVField($tech['BankZip']);
			$tech['BankCountry'] = escapeCSVField($tech['BankCountry']);
			$tech['AccountName'] = escapeCSVField($tech['AccountName']);
			$tech['AccountNum'] = escapeCSVField($tech['AccountNum']);

			echo "{$tech['TechID']},{$tech['FirstName']},{$tech['LastName']},{$tech['PrimaryPhone']},{$tech['PrimaryEmail']},{$tech['PaymentMethod']},\"{$tech['DateChange']}\",{$tech['DepositType']},{$tech['BankName']},{$tech['BankAddress1']},{$tech['BankAddress2']},{$tech['BankCity']},{$tech['BankState']},{$tech['BankZip']},{$tech['BankCountry']},{$tech['AccountName']},\"=\"\"{$tech['RoutingNum']}\"\"\",\"=\"\"{$tech['AccountNum']}\"\"\"\n";
//			echo "{$tech['TechID']},\"{$tech['FirstName']}\",\"{$tech['LastName']}\",\"{$tech['PrimaryPhone']}\",\"{$tech['PrimaryEmail']}\",\"{$tech['PaymentMethod']}\",\"{$tech['DateChange']}\",{$tech['DepositType']},\"{$tech['BankName']}\",\"{$tech['BankAddress1']}\",\"{$tech['BankAddress2']}\",\"{$tech['BankCity']}\",\"{$tech['BankState']}\",\"{$tech['BankZip']}\",\"{$tech['BankCountry']}\",\"{$tech['AccountName']}\",{$tech['RoutingNum']},\"{$tech['AccountNum']}\"\n";
		}
		die();
	}
?>
<?php $page = 'accounting'; ?>
<?php $option = 'accounting'; ?>
<?php $selected = 'Banking'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php
	if (!isset($_SESSION["BankingInfoPW"])) header("Location: bankingLogin.php");
	require_once("../library/caspioAPI.php");
	require_once("../library/mySQL.php");
	
	function parseDate($date) {
		$parts = explode("/", $date);
		if (!is_numeric($parts[0]) || !is_numeric($parts[1]) || !is_numeric($parts[2]))
			return FALSE;
		return "{$parts[2]}/{$parts[0]}/{$parts[1]}";
	}
?>
<!-- Add Content Here -->

<script type="text/javascript">
	function sortBy(num) {
		frm = document.getElementById("bankInfo");
		frm.sortDir.value = (frm.sortCol.value == num ? (frm.sortDir.value == 0 ? 1 : 0) : 0);
		frm.sortCol.value = num;
		frm.submit();
	}
	
	function gotoPage(num) {		
		frm = document.getElementById("bankInfo");
		frm.currPage.value = parseInt(num);
		frm.submit();
	}
	
	function nextPage() {
		frm = document.getElementById("bankInfo");
		gotoPage(parseInt(frm.currPage.value) + 1);
	}
	
	function prevPage() {
		frm = document.getElementById("bankInfo");
		gotoPage(parseInt(frm.currPage.value) - 1);
	}
	
</script>

<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#ChangeDateGT').calendar({dateFormat: 'MDY/'});
		$('#ChangeDateLT').calendar({dateFormat: 'MDY/'});
	});  
</script>

<style  TYPE="text/css">
.label{ font-size:12px; color:#385C7E;}

.resultTable {
	border-spacing: 0px;
	text-align: center;
	color: #000000;
	padding: 0px;
	border-collapse: collapse;
}

.resultTop {
	background-color: #949CCE;
	border-top: 1px solid #000000;
	border-left: 1px solid #000000;
	border-right: 1px solid #000000;
}

.leftHeader {
	border-left: 2px solid #2A497D;
}

.rightHeader {
	border-right: 2px solid #2A497D;
}

.middleHeader {
	border-left: 1px solid #FFFFFF;
	border-right: 1px solid #FFFFFF;
}

.resultHeader a, .resultRow a:hover {
	color: #2A497D;
}

.resultHeader a:hover, .resultRow a {
	color: #6373B5;
}

.resultHeader td {
	color: #000099;
	text-align: center; 
	background-color: #A0A8AA; 
	padding: 7px 10px;
	font-weight: bold;
	cursor: pointer;
	border-top: 2px solid #2A497D;
}

.resultRow {
	background-color: #FFFFFF;
}

.resultRow:hover {
	background-color: #EAEFF5;
}

.leftRow {
	border-left: 2px solid #2A497D;
}

.rightRow {
	border-right: 2px solid #2A497D;
}

.resultRow td {
	padding: 0px 10px;
}

.resultBottom {
	background-color: #A0A8AA;
	border: 2px solid #2A497D;
}
</style>

<br /><br />

<div align="center">
<?php
$currPage = (isset($_POST["currPage"]) ? $_POST["currPage"] : 1);
$resultsPerPage = 25;
if (!is_numeric($currPage))
$currPage = 1;
if (!isset($_POST["search"])) {
?>
<form id="bankInfo" name="bankInfo" action="<?=$_SERVER['PHP_SELF']?>" method="post">
	<table cellpadding="0" cellspacing="0" border="0" style="border:#385C7E solid medium;">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="5">
					<tr>
						<td class="label">Payment Method</td>
						<td>
							<select id="PaymentMethod" name="PaymentMethod">
								<option value="">Select Payment Method</option>
								<option value="Direct Deposit">Direct Deposit</option>
								<option value="Check">Check</option>
							</select>
						</td>
					</tr>
					<tr>
						<td class="label">FS Tech ID</td>
						<td><input id="TechID" name="TechID" type="text" size="13" /></td>
					</tr>
					<tr>
						<td class="label">First Name (contains)</td>
						<td><input id="FirstName" name="FirstName" type="text" size="13" /></td>
					</tr>
					<tr>
						<td class="label">Last Name (contains)</td>
						<td><input id="LastName" name="LastName" type="text" size="13" /></td>
					</tr>
					<tr>
						<td class="label">Date Changed (>=)</td>
						<td><input id="ChangeDateGT" name="ChangeDateGT" type="text" size="13" /></td>
					</tr>
					<tr>
						<td class="label">Date Changed (<=)</td>
						<td><input id="ChangeDateLT" name="ChangeDateLT" type="text" size="13" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="text-align:center; background-color:#BACBDF; border-top:#385C7E solid medium; padding:5px;">
			<input id="search" name="search" type="submit" value="Search" style="background-color:#385C7E; color:#FFFFFF; font-size:12px; font-weight:bold;" />
			<input name="sortCol" type="hidden" value="0" />
			<input name="sortDir" type="hidden" value="0" />
			<input id="currPage" name="currPage" type="hidden" value="1" />
			</td>
		</tr>
	</table>
</form>
<?php
}
else {
	$PaymentMethod =  $_POST["PaymentMethod"];
	$TechID = $_POST["TechID"];
	$FirstName = $_POST["FirstName"];
	$LastName = $_POST["LastName"];
	$ChangeDateGT = parseDate($_POST["ChangeDateGT"]);
	$ChangeDateLT = parseDate($_POST["ChangeDateLT"]);

	$sort = $_POST["sortCol"];
	$dir = $_POST["sortDir"];
	switch ($sort) {
		case 0: 
			$sortQuery = "TechID";
			break;
		case 1: 
			$sortQuery = "FirstName";
			break;
		case 2: 
			$sortQuery = "LastName";
			break;
		case 3: 
			$sortQuery = "PaymentMethod";
			break;
		case 4: 
			$sortQuery = "DateChange";
			break;
		default:
			$sortQuery = "TechID";
	}
	
	if ($sortQuery != "") {
		$sortQuery = "ORDER BY $sortQuery " . ($dir == 0 ? "ASC" : "DESC");
	}
	
	$criteria = "TechID = TechID ";
	if ($_POST['PaymentMethod'] != "") {
		$esc = mysqlEscape($_POST['PaymentMethod']);
		$criteria .= " AND PaymentMethod = '$esc'";
	}
	if ($_POST['TechID'] != "") {
		$esc = mysqlEscape($TechID);
		$criteria .= " AND TechID = '$esc'";
	}
	if ($_POST['FirstName'] != "") {
		$esc = mysqlEscape($FirstName);
		$criteria .= " AND FirstName LIKE '%$esc%'";
	}
	if ($_POST['LastName'] != "") {
		$esc = mysqlEscape($LastName);
		$criteria .= " AND LastName LIKE '%$esc%'";
	}
	if ($_POST['ChangeDateGT'] != "") {
		$date = $ChangeDateGT;
		$criteria .= " AND DateChange >= '$date 00:00:00'";
	}
	if ($_POST['ChangeDateLT'] != "") {
		$date = $ChangeDateLT;
		$criteria .= " AND DateChange <= '$date 23:59:59'";
	}

	$resultsOffset = ($currPage - 1) * $resultsPerPage;

	$result = mysqlQuery("SELECT SQL_CALC_FOUND_ROWS TechID, FirstName, LastName, PaymentMethod, DATE_FORMAT(DateChange, '%m/%d/%Y %h:%i:%s') AS DateChange FROM TechBankInfo " . ($criteria != "" ? "WHERE " . $criteria : "") . " $sortQuery LIMIT $resultsOffset, $resultsPerPage");
	
	$_SESSION["searchCriteria"] = ($criteria != "" ? "WHERE " . $criteria : "") . " $sortQuery";
		
//	echo htmlentities("SELECT SQL_CALC_FOUND_ROWS * FROM TechBankInfo " . ($criteria != "" ? "WHERE " . $criteria : "") . " $sortQuery LIMIT $resultsOffset, $resultsPerPage");
			
	$searchCount = mysqlQuery("SELECT FOUND_ROWS()");
	$countArray = mysql_fetch_row($searchCount);
	$count = $countArray[0];

	$pageSelect = "<select id='pageSelect' name='pageSelect' style='width: 4em; text-align: center' onChange='gotoPage(this.selectedIndex + 1);'>";
	$numPages = ceil($count / ($resultsPerPage * 1.0));
	for ($i = 1; $i <= $numPages; $i++) {
		$pageSelect .= "<option value='$i'" . ($currPage == $i ? " selected='selected' " :  "") . ">$i</option>";
	}
	$pageSelect .= "</select>";

?>

<form id="bankInfo" name="BankInfo" action="<?=$_SERVER['PHP_SELF']?>" method="post">
	<input id="PaymentMethod" name="PaymentMethod" type="hidden" value="<?=$PaymentMethod?>" />
	<input id="TechID" name="TechID" type="hidden" value="<?=$TechID?>" />
	<input id="FirstName" name="FirstName" type="hidden" value="<?=$FirstName?>" />
	<input id="LastName" name="LastName" type="hidden" value="<?=$LastName?>" />
	<input id="ChangeDateGT" name="ChangeDateGT" type="hidden" value="<?=$ChangedDateGT?>" />
	<input id="ChangeDateLT" name="ChangeDateLT" type="hidden" value="<?=$ChangedDateLT?>" />
	<input name="search" type="hidden" value="Search" />
	<input name="sortCol" type="hidden" value="<?php echo $_POST["sortCol"] ?>" />
	<input name="sortDir" type="hidden" value="<?php echo $_POST["sortDir"] ?>" />
	<input id="currPage" name="currPage" type="hidden" value="<?php echo $_POST["currPage"] ?>" />
</form>

<?php
	if ($count == 0) {
		echo "No Records Found.";
	}
	else {
?>	
		<table class="resultTable">
			<tr class="resultTop"><td colspan="5"><a href="<?php echo $_SERVER['PHP_SELF'];?>?download=1" style="display:none">Download</a></td></tr>
			<tr class="resultHeader">
				<td class="leftHeader"><a href="javascript:sortBy(0)">TechID</a></td>
				<td class="middleHeader"><a href="javascript:sortBy(1)">First Name</a></td>
				<td class="middleHeader"><a href="javascript:sortBy(2)">Last Name</a></td>
				<td class="middleHeader"><a href="javascript:sortBy(3)">Payment Method</a></td>
				<td class="rightHeader"><a href="javascript:sortBy(4)">Date Changed</a></td>
			</tr>
<?php
	while ($row = mysql_fetch_assoc($result)) {
		echo "<tr class=\"resultRow\"><td class=\"leftRow\"><a href='bankingDetails.php?id={$row['TechID']}'>{$row['TechID']}</a></td><td>{$row['FirstName']}</td><td>{$row['LastName']}</td><td>{$row['PaymentMethod']}</td><td class=\"rightRow\">{$row['DateChange']}</td></tr>";
	}
	
	echo "<tr><td class=\"resultBottom\" colspan='" . mysql_num_fields($result) . "'>Showing " . ($resultsOffset + 1) . "-" . min($resultsPerPage * $currPage, $count) . " of $count records found</td></tr><tr><td class=\"resultBottom\" colspan='" . mysql_num_fields($result) . "'>" . ($currPage > 1 ? "<a href='javascript:prevPage();'>&lt;</a>" : "") . " Page: " . $pageSelect . " " . ($currPage < $numPages ? "<a href='javascript:nextPage();'>&gt;</a>" : "") . "</td></tr>";

?>
	</table>
<?php
	}
}
?>
</div>

<!--- End Content --->
<?php require ("../footer.php"); ?>
