<?php
// Created on: 07-25-08
// Author: GB
// Description: Receives Project ID from datapage


try {

require_once("../../library/caspioAPI.php");

//$projectID = "18510";
$clientID = $_REQUEST['clientID'];

$records = caspioSelectAdv("TR_Client_Projects", "Project_Name, Project_ID", "Project_Company_ID = '$clientID'", "Project_Name ASC", false, "`", "|^",false);

// Counts # of records returned
$count = sizeof($records);

// Populate array with workorders
foreach ($records as $project) {

	$fields = explode("|^", $project);

	// Get data from array and set values
//	$projectID = trim($fields[1], "`");

	if ($fields[0] == ""){
		$ProjectName = "Not Found";
	}else{
		if($fields[0] != 'NULL'){
			$ProjectName = trim($fields[0], "`");
			$ProjectID = trim($fields[1], "`");
			
			echo "$ProjectName^$ProjectID|";
		}

	}

	}
}catch (SoapFault $fault) {

smtpMail("Project Selection Error in Accounting", "nobody@fieldsolutions.com", "gbailey@fieldsolutions.com", "Accounting Select Project Script Error", "$fault", "$fault", "projectLookup.php");

}

?>
