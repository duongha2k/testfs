<?php
require_once("database/db_connect.php");

function saveSyncVar($name, $value, $ticket_id = NULL) {
	$name_esc = "'" . mysql_real_escape_string($name) . "'";
	$value_esc = "'" . mysql_real_escape_string($value) . "'";
	if ($ticket_id !== NULL)
		$ticket_id_esc = mysql_real_escape_string($ticket_id);
	else
		$ticket_id_esc = "NULL";
	if (!readSyncVar($name, $ticket_id))
		mysql_query("INSERT INTO sync_vars (name, value, ticket_id) VALUES ($name_esc, $value_esc, $ticket_id_esc)");
	else
		mysql_query("UPDATE sync_vars SET name = $name_esc, value = $value_esc, ticket_id = $ticket_id_esc WHERE name = $name_esc");
}

function readSyncVar($name, $ticket_id = NULL) {
	$name = "'" . mysql_real_escape_string($name) . "'";
	$result = NULL;
	if ($ticket_id == NULL)
		$result = mysql_query("SELECT value FROM sync_vars WHERE name = $name LIMIT 1");
	else
		$result = mysql_query("SELECT value FROM sync_vars WHERE name = $name AND ticket_id = '" . mysql_real_escape_string($ticket_id) . "' LIMIT 1");
	$val = mysql_fetch_assoc($result);
	return $result && mysql_num_rows($result) > 0 ?  $val["value"] : false;
}

function unsetSyncVar($name, $ticket_id = NULL) {
	$name = "'" . mysql_real_escape_string($name) . "'";
	$result = NULL;
	if ($ticket_id == NULL)
		$result = mysql_query("DELETE FROM sync_vars WHERE name = $name LIMIT 1");
	else
		$result = mysql_query("DELETE FROM sync_vars WHERE name = $name AND ticket_id = '" . mysql_real_escape_string($ticket_id) . "' LIMIT 1");
}
