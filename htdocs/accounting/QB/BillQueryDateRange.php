<?php
	require_once($_SERVER["DOCUMENT_ROOT"] . '/accounting/QB/syncVars.php');
	define("BILL_QUERY_DATE_RANGE_FILE", $_SERVER['DOCUMENT_ROOT'] . "/accounting/QB/BillQueryDateRange.txt");
	function getBillQueryDateRange() {
		$range = readSyncVar("SyncRange");
		return $range ? @unserialize($range) : false;
	}
	
	function setBillQueryDateRange($start, $end) {
		saveSyncVar("SyncRange", serialize(array("StartRange" => $start, "EndRange" => $end)));
//		file_put_contents(BILL_QUERY_DATE_RANGE_FILE, serialize(array("StartRange" => $start, "EndRange" => $end)));
	}

	function setBillQueryDateRangeMoving($range) {
		saveSyncVar("SyncRange", serialize(array("MovingRange" => $range)));
//		file_put_contents(BILL_QUERY_DATE_RANGE_FILE, serialize(array("MovingRange" => $range)));
	}
?>
