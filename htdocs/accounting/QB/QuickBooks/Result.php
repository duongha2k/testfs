<?php

/**
 * QuickBooks result base class
 * 
 * Result subclasses are returned by the SOAP server handler methods to pass 
 * data back to the QuickBooks web connector. 
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 * 
 * @package QuickBooks
 */

/**
 * QuickBooks result base class
 */
abstract class QuickBooks_Result
{
	/**
	 * Placeholder constructor method
	 */
	abstract public function __construct($var);
}

?>