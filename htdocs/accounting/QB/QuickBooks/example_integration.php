<?php

/**
 * Example integration with an application
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 */
 
// Require the queueuing class
require_once 'QuickBooks/Queue.php';

if ($_POST['customer'])
{
	// Oooh, here's a new customer, let's do some stuff with them
	
	// Insert into our local MySQL database
	mysql_query("INSERT INTO my_customer_table ( name, phone, email ) VALUES ( '" . $_POST['customer']['name'] . "', '" . $_POST['customer']['phone'] . "', '" . $_POST['customer']['email'] . "' ) ");
	$id_value = mysql_insert_id();
	
	// QuickBooks queueing class
	$queue = new QuickBooks_Queue('mysql://root:password@localhost/my_database');
	
	// Queue it up!
	$queue->enqueue('CustomerAdd', $id_value);
}


?>