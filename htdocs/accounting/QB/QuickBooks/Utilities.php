<?php

/**
 * Various QuickBooks related utility methods
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 * 
 * @package QuickBooks
 */

$doc_root = $_SERVER["DOCUMENT_ROOT"];

/**
 * QuickBooks base classes/constants
 */
require_once($doc_root . '/accounting/QB/QuickBooks.php');

/**
 * Various QuickBooks related utilities
 * 
 * All methods are static
 */
class QuickBooks_Utilities
{
	/**
	 * Parse a DSN style connection string 
	 * 
	 * @param string $dsn		The DSN connection string
	 * @param string $part		If you want just a specific part of the string, choose which part here: scheme, host, port, user, pass, query, fragment
	 * @return mixed 			An array or a string, depending on if you wanted the whole thing parsed or just a piece of it 
	 */
	static public function parseDSN($dsn, $defaults = array(), $part = null)
	{
		// Some DSN strings look like this:		filesystem:///path/to/file
		//	parse_url() will not parse this *unless* we provide some sort of hostname (in this case, null)
		$dsn = str_replace(':///', '://null/', $dsn);
		
		$defaults = array_merge(array(
			'scheme' => '', 
			'host' => '', 
			'port' => 0, 
			'user' => '', 
			'pass' => '',
			'path' => '', 
			'query' => '',
			'fragment' => '',   
			), $defaults);
		
		$parse = array_merge($defaults, parse_url($dsn));
		
		if (is_null($part))
		{
			return $parse;
		}
		else if (isset($parse[$part]))
		{
			return $parse[$part];
		}
		
		return null;
	}
	
	/**
	 * Write to the API log file
	 * 
	 * This was only used for development purposes, don't use it anymore.
	 * 
	 * @deprecated
	 * 
	 * @param string $msg
	 * @param integer $tabs
	 * @return void
	 */
	static public function log($msg, $tabs = 0)
	{
		if (QUICKBOOKS_DEBUG)
		{
			$fp = fopen(QUICKBOOKS_LOG, 'a+');
			fwrite($fp, date('Y-m-d H:i:s') . ': ' . str_repeat("\t", $tabs) . $msg . "\n");
			fclose($fp);
		}
		
		return null;
	}
	
	/**
	 * Create an instance of a driver class from a DSN connection string *or* a connection resource
	 * 
	 * You can actually pass in *either* a DSN-style connection string OR an already connected database resource
	 * 	- mysql://user:pass@localhost:port/database
	 * 	- $var (Resource ID #XYZ, valid MySQL connection resource)
	 * 
	 * @param mixed $dsn_or_conn	A DSN-style connection string or a PHP resource
	 * @param array $config			An array of configuration options for the driver
	 * @return object				A class instance, a child class of QuickBooks_Driver
	 */
	static public function driverFactory($dsn_or_conn, $config = array())
	{
		if (is_resource($dsn_or_conn))
		{
			$scheme = current(explode(' ', get_resource_type($dsn_or_conn)));
		}
		else
		{
			$scheme = QuickBooks_Utilities::parseDSN($dsn_or_conn, array(), 'scheme');
		}
		
		$class = 'QuickBooks_Driver_' . ucfirst(strtolower($scheme));
		
		global $doc_root;
		require_once($doc_root . '/accounting/QB/QuickBooks/Driver/' . ucfirst(strtolower($scheme)) . '.php');
		
		if (class_exists($class))
		{
			return new $class($dsn_or_conn, $config);
		}
		
		return null;
	}
	
	/**
	 * Generate a valid QuickBooks Web Connector *.QWC file 
	 * 
	 * @param string $name			The name of the QuickBooks Web Connector job (something descriptive, this gets displayed to the end-user)
	 * @param string $descrip		A short description of the QuickBooks Web Connector job (something descriptive, this gets displayed to the end-user)
	 * @param string $appurl		The absolute URL to the SOAP server (this *MUST* be a HTTPS:// link *UNLESS* it's running on localhost)
	 * @param string $appsupport	A URL where an end-user can go to get support for the application
	 * @param string $username		The username that QuickBooks Web Connector should use to connect
	 * @param string $fileid		A file-ID value... apparently you can just make this up, make it resemble this string: {57F3B9B6-86F1-4fcc-B1FF-966DE1813D20}
	 * @param string $ownerid		As above, apparently you can just make this up, make it resemble this string: {57F3B9B6-86F1-4fcc-B1FF-966DE1813D20}
	 * @param string $qbtype		Either QUICKBOOKS_TYPE_QBFS or QUICKBOOKS_TYPE_QBPOS
	 * @param boolean $readonly		Whether or not to open the connection as read-only
	 * @param integer $run_every_n_seconds		If you want to schedule the job to run every once in a while automatically, you can pass in a number of seconds between runs here
	 * @return string
	 */
	static public function generateQWC($name, $descrip, $appurl, $appsupport, $username, $fileid, $ownerid, $qbtype = QUICKBOOKS_TYPE_QBFS, $readonly = true, $run_every_n_seconds = null)
	{
		$xml = '
		<?xml version="1.0"?>
		<QBWCXML>
			<AppName>' . htmlentities($name) . '</AppName>
			<AppID></AppID>
			<AppURL>' . htmlentities($appurl) . '</AppURL>
			<AppDescription>' . htmlentities($descrip) . '</AppDescription>
			<AppSupport>' . htmlentities($appsupport) . '</AppSupport>
			<UserName>' . htmlentities($username) . '</UserName>
			<OwnerID>' . $ownerid . '</OwnerID>
			<FileID>' . $fileid . '</FileID>
			<QBType>' . $qbtype . '</QBType>
		';
		
		if ((int) $run_every_n_seconds > 0 and (int) $run_every_n_seconds < 60)
		{
			$xml .= '
				<Scheduler>
					<RunEveryNSeconds>' . (int) $run_every_n_seconds . '</RunEveryNSeconds>
				</Scheduler>
			';
		}
		else if ((int) $run_every_n_seconds >= 60)
		{
			$xml .= '
				<Scheduler>
					<RunEveryNMinutes>' . floor($run_every_n_seconds / 60) . '</RunEveryNMinutes>
				</Scheduler>
			';
		}
		
		if ($readonly)
		{
			$xml .= '<IsReadOnly>true</IsReadOnly>';
		}
		else
		{
			$xml .= '<IsReadOnly>false</IsReadOnly>';
		}
		
		$xml .= '
		</QBWCXML>';
		
		return trim($xml);		
	}
	
	/**
	 * Create a user for the QuickBooks Web Connector SOAP server
	 * 
	 * @param string $dsn		A DSN-style connection string for the back-end driver
	 * @param string $username	The username for the new user
	 * @param string $password	The password for the new user
	 * @return boolean 			
	 */
	static public function createUser($dsn, $username, $password)
	{
		$driver = QuickBooks_Utilities::driverFactory($dsn);
		
		return $driver->authCreate($username, $password);
	}
	
	/**
	 * Initialize the backend driver
	 * 
	 * Initialization should only be done once, and is used to take care of 
	 * things like creating the database schema, etc.
	 * 
	 * @param string $dsn	A DSN-style connection string
	 * @return boolean
	 */
	static public function initialize($dsn)
	{
		$driver = QuickBooks_Utilities::driverFactory($dsn);
		
		return $driver->initialize();
	}
	
	/**
	 * Make sure all characters are ASCII (and thus will work with UTF-8)
	 * 
	 * We have a lot of content that gets pasted in from Microsoft Word, which 
	 * often generates some very wierd characters, which seem to not always 
	 * convert correctly to UTF-8 and then get rejected by the QuickBooks Web 
	 * Connector. This method tries to make sure that everything that should 
	 * get converted gets converted, and gets converted cleanly. 
	 * 
	 * There is probably a better way to do this...  
	 *  
	 * @param string $str	The string to convert to ASCII
	 * @return string
	 */
	static protected function _castCharset($str)
	{
		// These extended ASCII characters get mapped to things in the normal ASCII character set
		$replace = array(
			chr(129) => 'u', 
			chr(130) => 'e', 
			chr(131) => 'a', 
			chr(132) => 'a', 
			chr(133) => 'a',
			chr(134) => 'a',
			chr(136) => 'e', 
			chr(137) => 'e', 
			chr(138) => 'e', 
			chr(139) => 'i', 
			chr(140) => 'i', 
			chr(141) => 'i', 
			chr(142) => 'A', 
			chr(143) => 'A', 
			chr(144) => 'E', 
			chr(145) => 'ae', 
			chr(146) => 'AE', 
			chr(147) => 'o', 
			chr(148) => 'o', 
			chr(149) => 'o', 
			chr(150) => 'u', 
			chr(151) => 'u', 
			chr(152) => '_', 
			chr(153) => 'O', 
			chr(154) => 'U', 
			chr(158) => '_', 
			chr(160) => 'a', 
			chr(161) => 'i', 
			chr(162) => 'o', 
			chr(163) => 'u', 
			chr(164) => 'n', 
			chr(165) => 'N', 
			chr(173) => 'i', 
			chr(174) => '<', 
			chr(175) => '>', 
			chr(179) => '|', 
			chr(196) => '-', 
			chr(242) => '>=', 
			chr(243) => '<=', 
			chr(246) => '/', 
			chr(247) => '~', 
			chr(249) => '.', 
			chr(250) => '.', 
			chr(252) => '_', 
			);
		
		$count = strlen($str);
		for ($i = 0; $i < $count; $i++)
		{
			$ord = ord($str{$i});
			
			if ($ord != ord("\t") and 
				$ord != ord("\n") and 
				$ord != ord("\r") and 
				($ord < 32 or $ord > 126)) 
			{
				if (isset($replace[$ord]))
				{
					$str{$i} = $replace[$ord];
				}
				else
				{
					$str{$i} = ' ';
				}
			}
		}
		
		return $str;
	}
	
	/**
	 * Convert certain strings to their abbreviations
	 * 
	 * QuickBooks often uses unusually short field lengths. This function will 
	 * convert common long words to shorter abbreviations in an attempt to make 
	 * a string fit cleanly into the very short fields.
	 * 
	 * @param string $value		The value to apply the abbreviations to
	 * @return string
	 */
	static protected function _castAbbreviations($value)
	{
		$abbrevs = array(
			'Administration' => 'Admin.', 
			'Academic' => 'Acad.', 
			'Academy' => 'Acad.', 
			'Association' => 'Assn.',
			'Boulevard' => 'Blvd.', 
			'Building' => 'Bldg.', 
			'College' => 'Coll.', 
			'Company' => 'Co.', 
			'Consolidated' => 'Consol.', 
			'Corporation' => 'Corp.', 
			'Incorporated' => 'Inc.', 
			'Department' => 'Dept.', 
			'Division' => 'Div.', 
			'District' => 'Dist.', 
			'Eastern' => 'E.', 
			'Government' => 'Govt.', 
			'International' => 'Intl.', 
			'Institute' => 'Inst.', 
			'Institution' => 'Inst.', 
			'Laboratory' => 'Lab.', 
			'Limited' => 'Ltd.', 
			'Manufacturing' => 'Mfg.', 
			'Manufacturer' => 'Mfr.', 
			'Miscellaneous' => 'Misc.', 
			'Museum' => 'Mus.', 
			'Northern' => 'N.', 
			'School' => 'Sch.',
			'Services' => 'Svcs.', // This is *before* Service so that we don't get "Svc.s"			
			'Service' => 'Svc.', 
			'Southern' => 'S.', 
			'University' => 'Univ.', 
			'Western' => 'W.', 
			);
			
		return str_ireplace(array_keys($abbrevs), array_values($abbrevs), $value);
	}
	
	/**
	 * Shorten a string to a specific length by truncating or abbreviating the string
	 * 
	 * QuickBooks often uses unusually short field lengths. This function can 
	 * be used to try to make long strings fit cleanly into the QuickBooks 
	 * fields. It tries to do a few things:
	 * 	- Convert long words to shorter abbreviations
	 * 	- Remove non-ASCII characters
	 * 	- Truncate the string if it's still too long
	 * 
	 * @param string $value				The string to shorten
	 * @param integer $length			The max. length the string should be
	 * @param boolean $with_abbrevs		Whether or not to abbreviate some long words to shorten the string
	 * @return string					The shortened string
	 */
	static protected function _castTruncate($value, $length, $with_abbrevs = true)
	{
		$value = QuickBooks_Utilities::_castCharset($value);
		
		if (strlen($value) > $length)
		{
			if ($with_abbrevs)
			{
				$value = QuickBooks_Utilities::_castAbbreviations($value);
			}
			
			if (strlen($value) > $length)
			{
				$value = substr($value, 0, $length);
			}
		}
		
		return utf8_encode($value);
	}
	
	/**
	 * Cast a value to ensure that it will fit in a particular field within QuickBooks
	 * 
	 * QuickBooks has some strange length limits on some fields (the max. 
	 * length of the CompanyName field for Customers is only 41 characters, 
	 * etc.) so this method provides an easy way to cast the data type and data 
	 * length of a value to the correct type and length for a specific field.
	 * 
	 * @param string $object_type	The QuickBooks object type (Customer, Invoice, etc.)
	 * @param string $field_name	The QuickBooks field name (these correspond to the qbXML field names: Addr1, Name, CompanyName, etc.)
	 * @param mixed $value			The value you want to cast
	 * @param boolean $usee_abbrevs	There are a lot of strings which can be abbreviated to shorten lengths, this is whether or not you want to use those abbrevaitions ("University" to "Univ.", "Incorporated" to "Inc.", etc.)
	 * @return string
	 */
	static public function castToField($object_type, $field_name, $value, $use_abbrevs = true)
	{
		switch (strtolower($object_type))
		{
			case 'customer':
				
				switch (strtolower($field_name))
				{
					case 'name':
					case 'companyname':
					case 'addr1':
					case 'addr2':
					case 'addr3':
					case 'addr4':
					case 'addr5':
					case 'note':
					case 'contact':
					case 'altcontact':
						
						return QuickBooks_Utilities::_castTruncate($value, 41);
					case 'firstname':
					case 'lastname':
						
						return QuickBooks_Utilities::_castTruncate($value, 25);
					case 'middlename':
						
						return QuickBooks_Utilities::_castTruncate($value, 5);
					case 'salutation':
						
						return QuickBooks_Utilities::_castTruncate($value, 15);
					case 'phone':
					case 'altphone':
					case 'fax':
					case 'state':
						
						return QuickBooks_Utilities::_castTruncate($value, 21);
					case 'city':
					case 'country':
						
						return QuickBooks_Utilities::_castTruncate($value, 31);
					case 'postalcode':
						
						return QuickBooks_Utilities::_castTruncate($value, 13);
					case 'email':
						
						return QuickBooks_Utilities::_castTruncate($value, 1023);
				}
				
				break;
			case 'invoice':
				
				switch (strtolower($field_name))
				{
					case 'addr1':
					case 'addr2':
					case 'addr3':
					case 'addr4':
					case 'addr5':
					case 'note':
						
						return QuickBooks_Utilities::_castTruncate($value, 41);
					case 'desc':
						
						return QuickBooks_Utilities::_castTruncate($value, 4000);
					case 'city':
						
						return QuickBooks_Utilities::_castTruncate($value, 31);
				}
				
				break;
		}
		
		return $value;
	}
}


?>