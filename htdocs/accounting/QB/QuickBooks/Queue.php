<?php

/**
 * QuickBooks action queue - A queue of actions to be performed via the QBWC 
 * 
 * The QuickBooks action queue can be used to queue up actions that need to be 
 * pushed out to QuickBooks, or queue up items which need to be fetched from 
 * QuickBooks. 
 * 
 * For instance, everytime someone creates an account on your website, you 
 * would "push" their name, e-mail, etc. into a customer account within 
 * QuickBooks. You could use the QuickBooks_Queue class to enqueue a request 
 * for each new client like this:
 * <code>
 * require_once 'QuickBooks/Queue.php';
 * $queue = new QuickBooks_Queue('mysql://user:pass@localhost/database');
 * $queue->enqueue(QUICKBOOKS_ADD_CUSTOMER, $the_customer_ID_number_here);
 * </code>
 * 
 * The next time the QuickBooks Web Connector calls your QuickBooks SOAP server, 
 * the SOAP server calls your SOAP server handler function associated with 
 * adding customer records, your handler function will generate the qbXML 
 * request, and the request will be sent off to QuickBooks and (if everything 
 * goes well) the customer will appear in QuickBooks.
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 * 
 * @package QuickBooks
 */

$doc_root = $_SERVER["DOCUMENT_ROOT"];

/**
 * QuickBooks base class
 */
require_once($doc_root . '/accounting/QB/QuickBooks.php');

/**
 * Various QuickBooks-related utilities
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Utilities.php');

/**
 * QuickBooks queueing class - Queue up actions to be performed in QuickBooks
 */
class QuickBooks_Queue
{
	/**
	 * Create a new QuickBooks queue instance
	 * 
	 * @param mixed $dsn_or_conn	A DSN-style connection string (i.e.: mysq://root:pass@locahost/database) or a database connection (if you wish to re-use an existing database connection)
	 * @param array $config			Configuration array for the driver
	 */
	public function __construct($dsn_or_conn, $config = array())
	{
		$this->_driver = QuickBooks_Utilities::driverFactory($dsn_or_conn, $config);
	}
	
	/**
	 * Request to enter "Interactive Mode" with the Web Connector
	 * 
	 * @param integer $priority
	 * @return boolean
	 */
	public function interactive($priority = 0)
	{
		if ($this->_driver)
		{
			$tmp = array_merge(range('a', 'z'), range(0, 9));
			shuffle($tmp);
			$random = substr(implode('', $tmp), 0, 8);
			
			return $this->_driver->queueEnqueue(QUICKBOOKS_INTERACTIVE_MODE, $random, true, $priority);
		}
		
		return false;
	}
	
	/**
	 * Add a new item/action to the QuickBooks queue
	 * 
	 * @param string $action		An action to be performed within QuickBooks (see the qbXML and QuickBooks SDK documentation, i.e.: "CustomerAdd", "InvoiceAdd", "CustomerMod", etc.)
	 * @param mixed $ident			A unique identifier (if required) for a record being operated on (i.e. if you're doing a "CustomerAdd", you'd probaly put a unique customer ID number here, so you're SOAP handler function knows which customer it is supposed to add)
	 * @param integer $priority		The priority of the update (higher priority actions will be pushed to QuickBooks before lower priority actions)
	 * @param array $extra			If you need to make additional bits of data available to your request/response functions, you can pass an array of extra data here
	 * @param boolean $replace		Whether or not to replace any other currently queued entries with the same action/ident
	 * @return boolean
	 */	
	public function enqueue($action, $ident = null, $priority = 0, $extra = null, $replace = true)
	{
		if (is_null($ident))
		{
			$tmp = array_merge(range('a', 'z'), range(0, 9));
			shuffle($tmp);
			$ident = substr(implode('', $tmp), 0, 8);
		}
		
		if ($this->_driver)
		{
			return $this->_driver->queueEnqueue($action, $ident, $replace, $priority, $extra);
		}
		
		return false;
	}
	
	/**
	 * Tell whether or not an action/ident already exists in the queue
	 * 
	 * @param string $action	An action to be performed within QuickBooks
	 * @param mixed $ident		A unique identifier (if required) for the record being operated on
	 * @return boolean			Whether or not that action/ident tuple is already in the queue
	 */
	public function exists($action, $ident)
	{
		if ($this->_driver)
		{
			return $this->_driver->queueExists($action, $ident);
		}
		
		return null;
	}
	
	/**
	 * Get debugging information from the queue
	 * 
	 * @return array
	 */
	public function debug()
	{
		return array(
			'driver' => var_export($this->_driver), 
			);
	}
}

?>