<?php

/**
 * QuickBooks response object for responses to the ->getServerVersion() SOAP method call
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 * 
 * @package QuickBooks
 * @subpackage 
 */

/**
 * QuickBooks result base class
 */
require_once 'QuickBooks/Result.php';

/**
 * QuickBooks response object for responses to the ->getServerVersion() SOAP method call
 */
class QuickBooks_Result_GetServerVersion
{
	/**
	 * A string describing the server version
	 * 
	 * @var string
	 */
	public $serverVersionResult;
	
	/**
	 * Create a new result object
	 * 
	 * @param string $version
	 */
	public function __construct($version)
	{
		$this->serverVersionResult = $version;
	}
}

?>