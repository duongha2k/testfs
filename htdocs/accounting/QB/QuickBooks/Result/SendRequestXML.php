<?php

/**
 * Response result for the SOAP ->sendRequestXML() method call
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 * 
 * @package QuickBooks
 * @subpackage Result
 */

$doc_root = $_SERVER["DOCUMENT_ROOT"];

/**
 * QuickBooks result base class
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Result.php');

/**
 * Response result for the SOAP ->sendRequestXML() method call
 */
class QuickBooks_Result_SendRequestXML extends QuickBooks_Result
{
	/**
	 * A QBXML XML request string
	 * 
	 * @var string
	 */
	public $sendRequestXMLResult;
	
	/**
	 * Create a new result response
	 * 
	 * @param string $xml	The XML request to send to QuickBooks
	 */
	public function __construct($xml)
	{
		$this->sendRequestXMLResult = $xml;
	}
}

?>