<?php

/**
 * Result container object for the SOAP ->authenticate() method call
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 * 
 * @package QuickBooks
 * @subpackage Result
 */

$doc_root = $_SERVER["DOCUMENT_ROOT"];

/**
 * QuickBooks result base class
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Result.php');

/**
 * Result container object for the SOAP ->authenticate() method call
 */
class QuickBooks_Result_Authenticate extends QuickBooks_Result
{
	/**
	 * A two element array indicating the result of the call to ->authenticate()
	 * 
	 * @var array
	 */
	public $authenticateResult;
	
	/**
	 * Create a new result object
	 * 
	 * @param string $ticket	The ticket of the new login session
	 * @param string $status	The status of the new login session (blank, a company file path, or "nvu" for an invalid login)
	 */
	public function __construct($ticket, $status)
	{
		$this->authenticateResult = array( $ticket, $status );
	}
}

?>