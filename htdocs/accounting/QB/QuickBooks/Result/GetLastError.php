<?php

/**
 * Result container object for the SOAP ->getLastError() method call
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 * 
 * @package QuickBooks
 * @subpackage Result
 */

$doc_root = $_SERVER["DOCUMENT_ROOT"];

/**
 * Result base class
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Result.php');

/**
 * Result container object for the SOAP ->getLastError() method call
 */
class QuickBooks_Result_GetLastError extends QuickBooks_Result
{
	/**
	 * An error message
	 * 
	 * @param string $resp
	 */
	public $getLastErrorResult;
	
	/**
	 * Create a new result object
	 * 
	 * @param string $resp 		A message describing the last error that occured
	 */
	public function __construct($result)
	{
		$this->getLastErrorResult = $result;
	}
}

?>