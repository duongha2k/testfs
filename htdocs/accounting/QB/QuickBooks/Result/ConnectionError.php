<?php

/**
 * Result container object for the SOAP ->connectionError() method call
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 * 
 * @package QuickBooks
 * @subpackage Result
 */

$doc_root = $_SERVER["DOCUMENT_ROOT"];

/**
 * Result interface
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Result.php');

/**
 * Result container object for the SOAP ->connectionError() method call
 */
class QuickBooks_Result_ConnectionError extends QuickBooks_Result
{
	/**
	 * An error message
	 * 
	 * @var string
	 */
	public $connectionErrorResult;
	
	/**
	 * Create a new result object
	 * 
	 * @param string $err		An error message describing the problem
	 */
	public function __construct($err)
	{
		$this->connectionErrorResult = $err;
	}
}

?>