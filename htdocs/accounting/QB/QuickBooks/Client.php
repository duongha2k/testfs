<?php

/**
 * QuickBooks SOAP client for testing purposes
 * 
 * Unused for now, might be here for testing in later versions
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 * 
 * @package QuickBooks
 */

$client = new SoapClient("some.wsdl");

class QuickBooks_Client extends SoapClient
{
	
}

?>