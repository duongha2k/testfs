<?php

/**
 * Authenticate QuickBooks Web Connector users against a MySQL database
 * 
 * @package QuickBooks
 * @subpackage Authenticate
 */

$doc_root = $_SERVER["DOCUMENT_ROOT"];

/**
 * Authenticate interface
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Authenticate.php');

require_once($doc_root . '/accounting/QB/database/db_connect.php');


/**
 * Utilities for parsing DSN strings
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Utilities.php');

/**
 * Authenticate a QuickBooks Web Connector user against a MySQL database
 */
class QuickBooks_Authenticate_Mysql implements QuickBooks_Authenticate
{
	/**
	 * MySQL table name
	 * @var string
	 */
	protected $_table_name;
	
	/**
	 * MySQL field name which contains usernames
	 * @var string
	 */
	protected $_field_username;
	
	/**
	 * MySQL field name which contains passwords
	 * @var string
	 */
	protected $_field_password;
	
	/**
	 * Callback function used for encryption/hashing
	 * @var string
	 */
	protected $_crypt_function;
	
	/**
	 * MySQL database connection
	 * @var resource
	 */
	protected $_conn;
	
	/**
	 * Create a new instance of the MySQL Web Connector authenticator
	 * 
	 * @param string $dsn	A DSN-style MySQL connection string (i.e.: mysql://your-username:your-password@your-localhost:port/your-database)
	 */
	public function __construct($dsn)
	{
		$conn_defaults = array(
			'scheme' => 'mysql', 
			'user' => 'root', 
			'pass' => '',
			'host' => 'localhost',  
			'port' => 3306,
			'path' => '/quickbooks',  
			'query' => '', 
			);
		
		$param_defaults = array(
			'table_name' => 'quickbooks_user', 
			'field_username' => 'qb_username', 
			'field_password' => 'qb_password', 
			'crypt_function' => 'sha1', 
			);
		
		// mysql://user:pass@localhost:port/database?table_name=quickbooks_user&field_username=username&field_password=password&crypt_function=md5
		$parse = QuickBooks_Utilities::parseDSN($dsn, $conn_defaults);
		
		$vars = array();
		parse_str($parse['query'], $vars);
		
		$param_defaults = array_merge($param_defaults, $vars);
		
		$attempt = 0;
		do {
			mail2("tngo@fieldsolutions.com", "Mysql Auth", "$attempt attempt " . print_r($this->_conn));
			$this->_conn = mysql_connect($parse['host'] . ':' . $parse['port'], $parse['user'], $parse['pass'], true);
			if (!$this->_conn) sleep(1);
			++$attempt;
		} while ($attempt < 20 && !mysql_ping($this->_conn));
		mysql_select_db(substr($parse['path'], 1), $this->_conn);
		
		$this->_table_name = mysql_real_escape_string($param_defaults['table_name'], $this->_conn);
		$this->_field_username = mysql_real_escape_string($param_defaults['field_username'], $this->_conn);
		$this->_field_password = mysql_real_escape_string($param_defaults['field_password'], $this->_conn);
		$this->_crypt_function = $param_defaults['crypt_function'];
	}
	
	/**
	 * Perform authentication against a MySQL database
	 * 
	 * @param string $username
	 * @param string $password
	 * @return boolean
	 */
	public function authenticate($username, $password)
	{
		$func = $this->_crypt_function;

//		require_once(realpath(dirname(__FILE__) . "/../../" . '/mail2.php');
/*		mail2("tngo@fieldsolutions.com", "QB", "
                        SELECT
                                *
                        FROM
                                " . $this->_table_name . "
                        WHERE
                                " . $this->_field_username . " = '" . mysql_real_escape_string($username, $this->_conn) . "' AND
                                " . $this->_field_password . " = '" . mysql_real_escape_string($func($password), $this->_conn) . "' ");*/
		
		$res = db_connect_query("
			SELECT 
				* 
			FROM 
				" . $this->_table_name . " 
			WHERE 
				" . $this->_field_username . " = '" . mysql_real_escape_string($username, $this->_conn) . "' AND 
				" . $this->_field_password . " = '" . mysql_real_escape_string($func($password), $this->_conn) . "' ");
		
		$fp = fopen('/tmp/QBLOG/password1.txt', 'w');
		fwrite($fp, $password);
		fclose($fp);
		
		return mysql_num_rows($res) == 1;
	}
}

?>
