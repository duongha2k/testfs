<?php

/**
 * 
 * 
 * @package QuickBooks
 * @subpackage Authenticate
 */

$doc_root = $_SERVER["DOCUMENT_ROOT"];

/**
 * 
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Authenticate.php');

/**
 * 
 */
class QuickBooks_Authenticate_Ldapv3 implements QuickBooks_Authenticate
{
	function __construct($dsn)
	{
		
	}
	
	function authenticate($username, $password)
	{
		$username = addslashes($username);
		$password = addslashes($password);
		
		if ($PHP_AUTH_USER != "" && $PHP_AUTH_PW != "")
		{
			$ds=@ldap_connect($ldapconfig['host'],$ldapconfig['port']);
			$r = @ldap_search( $ds, $ldapconfig['basedn'], 'uid=' . $PHP_AUTH_USER);
			if ($r) 
			{
				$result = @ldap_get_entries( $ds, $r);
				if ($result[0]) 
				{
					if (@ldap_bind( $ds, $result[0]['dn'], $PHP_AUTH_PW) ) 
					{
						return true;
					}
				}
			}
		}
		
		return false;
	}
}