<?php

/**
 * QuickBooks SOAP server (extends the PHP5 SOAP server)
 * 
 * Use the QuickBooks SOAP server to create a basic SOAP server which 
 * communicates with the QuickBooks Web Connector application. You can register 
 * handler functions which generate QuickBooks qbXML requests and register 
 * handler functions which react to qbXML responses. 
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 * 
 * @package QuickBooks
 */
 
$doc_root = $_SERVER["DOCUMENT_ROOT"];

require_once($doc_root . '/accounting/QB/mail2.php');

/**
 * QuickBooks base classes and constants
 */
require_once($doc_root . '/accounting/QB/QuickBooks.php');

/**
 * Base handlers for each of the methods required by the QuickBooks Web Connector
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Server/Handlers.php');

/**
 * QuickBooks SOAP Server
 */
class QuickBooks_Server extends SoapServer
{
	/**
	 * Create a new QuickBooks SOAP server
	 * 
	 * @param mixed $dsn_or_conn		Either a DSN-style connection string *or* a database resource (if reusing an existing connection)
	 * @param array $map				An associative array mapping queued commands to function calls
	 * @param array $onerror			An array mapping error codes to function calls   
	 * @param string $wsdl				The path to the WSDL file to use for the SOAP server methods
	 * @param array $soap_options		Options to pass to the SOAP server (these mirror the default PHP SOAP server options)
	 * @param array $handler_options	Options to pass to the handler class
	 * @param array $driver_options		Options to pass to the driver class (i.e.: MySQL, etc.)
	 */
	public function __construct($dsn_or_conn, $map, $onerror = array(), $wsdl = QUICKBOOKS_WSDL, $soap_options = array(), $handler_options = array(), $driver_options = array())
	{
		//mail2("tngo@fieldsolutions.com", "QBServer", "Construct");
		// This makes debugging very difficult...
		ini_set('soap.wsdl_cache_enabled', '0');
		
		parent::__construct($wsdl, $soap_options);
		
//		mail2("tngo@fieldsolutions.com", "QBServer", "Input " . file_get_contents('php://input'));
		// Base handlers
		$this->setClass('QuickBooks_Server_Handlers', $dsn_or_conn, $map, $onerror, file_get_contents('php://input'), $handler_options, $driver_options);
//		mail2("tngo@fieldsolutions.com", "QBServer", "Construct Done");
	}
	
	/**
	 * Handle the SOAP request
	 * 
	 * @param boolean $return
	 * @return void
	 */
	public function handle($return = false)
	{
		header('Content-type: text/xml');
		
		//mail2("tngo@fieldsolutions.com", "QBServer", "Handle Start" . file_get_contents('php://input'));
		if ($return)
		{
			ob_start();
		}
		
		parent::handle();
		
		if ($return)
		{
			$contents = ob_get_contents();
			ob_end_flush();
			
			//mail2("tngo@fieldsolutions.com", "QBServer 1", "Handle Start" . $contents);
			return $contents;
		}
		
		
		return;
	}
	
	/**
	 * Get debugging information from the SOAP server
	 * 
	 * @return array 
	 */
	public function debug()
	{
		return var_export($this, true);
	}
}

?>
