<?php

/**
 * QuickBooks registry class
 * 
 * Unused, was used during development and then dropped in favor of a better 
 * approach.
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 * 
 * @package QuickBooks
 */

define('QUICKBOOKS_SERVER_REGISTRY_REQUEST', 'q');
define('QUICKBOOKS_SERVER_REGISTRY_RESPONSE', 's');
define('QUICKBOOKS_SERVER_REGISTRY_CONFIG', 'c');

/**
 * 
 * @deprecated
 */
class QuickBooks_Server_Registry
{
	protected $_registry;
	
	public function getInstance()
	{
		static $instance;
		
		if (is_nulL($instance))
		{
			$instance = new QuickBooks_Server_Registry();
		}
		
		return $instance;
	}
	
	protected function __construct()
	{
		$this->_registry = array();
	}
	
	public function register($type, $key, $value)
	{
		if (!isset($this->_registry[$type . '-' . $key]))
		{
			$this->_registry[$type . '-' . $key] = $value;
			
			return true;
		}
		
		return false;
	}
	
	public function resolve($type, $key)
	{
		if (isset($this->_registry[$type . '-' . $key]))
		{
			return $this->_registry[$type . '-' . $key];
		}
		
		return null;
	}
	
	public function debug()
	{
		return $this->_registry;
	}
}

?>