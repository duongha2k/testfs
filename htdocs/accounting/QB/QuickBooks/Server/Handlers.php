<?php

/**
 * Handlers for each of the QBWC SOAP server required methods
 * 
 * The QuickBooks Web Connector requires that your SOAP server be able to 
 * handle six basic methods. Each of the six methods are implemented in this 
 * class and called by the QuickBooks_Server class instance. 
 * 
 * These methods in turn will call the action handlers you register with the 
 * SOAP server, and also log quite a bit of debugging information to the 
 * database so that you can see what's happening during the QBWC exchange with 
 * your SOAP server. 
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 * 
 * @package QuickBooks
 * @subpackage Server
 */

$doc_root = $_SERVER["DOCUMENT_ROOT"];
$last_update = date('Y-m-d G:i:s');

require_once($doc_root . '/accounting/QB/mail2.php');

/**
 * Various QuickBooks related utilities methods
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Utilities.php');

/**
 * Response container for calls to ->authenticate()
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Result/Authenticate.php');

/**
 * Response container for calls to ->closeConnection()
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Result/CloseConnection.php');

/**
 * Response container for calls to ->connectionError()
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Result/ConnectionError.php');

/**
 * Response container for calls to ->getLastError()
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Result/GetLastError.php');

/**
 * Response container for calls to ->receiveResponseXML()
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Result/ReceiveResponseXML.php');

/**
 * Response container for calls to ->sendRequestXML()
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Result/SendRequestXML.php');

/**
 * Response container for calls to ->getServerVersion()
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Result/ServerVersion.php');

/**
 * Response container for calls to ->clientVersion()
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Result/ClientVersion.php');

/**
 * Handlers for each of the QBWC SOAP server required methods
 * 
 * @todo Fix error handling so that ->GetLastError() does in fact return the last error
 */
class QuickBooks_Server_Handlers
{
	/**
	 * Driver object instance for backend of SOAP server
	 * @var QuickBooks_Driver
	 */
	protected $_driver;
	
	/**
	 * Raw XML input
	 * @var string
	 */
	protected $_input;
	
	/**
	 * Map of queued actions to function handlers
	 * @var array 
	 */
	protected $_map;
	
	/**
	 * Map of error codes to function handler
	 * @var array 
	 */
	protected $_onerror;
	
	/**
	 * Create the server handler instance
	 * 
	 * Optional configuration items should be passed as an associative array with any of these keys:
	 * 	- qb_company_file				The full filesystem path to a specific QuickBooks company file (by default, it will use the currently open company file)
	 * 	- qbwc_min_version				Minimum version of the Web Connector that must be used to connect (by default, any version may connect)
	 * 	- qbwc_wait_before_next_update 	Tell the Web Connector to wait this number of seconds before doign another update
	 * 	- qbwc_min_run_every_n_seconds	Tell the Web Connector to run every n seconds (overrides whatever was in the .QWC web connector configuration file)
	 * 	- qbwc_interactive_url			The URL to use for Interactive QuickBooks Web Connector sessions
	 * 	- server_version				Server version string
	 * 	- authenticate_handler			If you want to use some custom authentication method, put the function name of your custom authentication function here
	 * 	- autoadd_missing_requestid		This defaults to TRUE, if TRUE and you forget to embed a requestID="..." attribute, it will try to automatically add that attribute for you
	 * 
	 * @param mixed $dsn_or_conn		DSN connection string for QuickBooks queue
	 * @param array $map				A map of QuickBooks API calls to callback functions
	 * @param array $onerror			A map of QuickBooks error codes to callback functions
	 * @param string $input				Raw XML input from QuickBooks API call
	 * @param array $handler_config		An array of configuration options
	 * @param array $driver_config		An array of driver configuration options
	 */
	public function __construct($dsn_or_conn, $map, $onerror, $input, $handler_config = array(), $driver_config = array())
	{
//		mail2("tngo@fieldsolutions.com", "handler", "handler construct");

		$this->_driver = QuickBooks_Utilities::driverFactory($dsn_or_conn, $driver_config);
		$this->_input = $input;
		$this->_map = $map;
		$this->_onerror = $onerror;
		//$this->_err = '';
		
		$this->_driver->log('Handler is starting up...', '', QUICKBOOKS_LOG_VERBOSE);
		
		$this->_config = $this->_defaults($handler_config);
//		mail2("tngo@fieldsolutions.com", "handler", "handler construct finish");
	}
	
	/**
	 * Massage any optional configuration flags
	 * 
	 * @param array $config
	 * @return array 
	 */
	protected function _defaults($config)
	{
		$defaults = array(
			'qb_company_file' => null,					// To force a specific company file to be used		
			'qbwc_min_version' => null, 				// Minimum version of the QBWC that must be used to connect
			'qbwc_wait_before_next_update' => null, 	// Tell the QBWC to wait this number of seconds before doing another update
			'qbwc_min_run_every_n_seconds' => null,		// Tell the QBWC to run every n seconds (overrides whatever was in the .QWC web connector configuration file)
			'qbwc_version_warning_message' => null,		// Not implemented... 
			'qbwc_version_error_message' => null,  		// Not implemented...
			'qbwc_interactive_url' => null, 			// Provide the URL for an interactive session to the QuickBooks Web Connector
			'autoadd_missing_requestid' => true,  
			'server_version' => 'PHP QuickBooks SOAP Server v1.3 at ' . $_SERVER['REQUEST_URI'],	// Server version string
			'authenticate_dsn' => null, 			// If you want to use some custom authentication scheme (and not the quickbooks_user MySQL table) you can specify your own function here
			);
			
		return array_merge($defaults, $config);
	}
	
	/**
	 * Authenticate method for the QuickBooks Web Connector SOAP service
	 * 
	 * The authenticate method is called when the Web Connector establishes a 
	 * connection with the SOAP server in order to ensure that there is work to 
	 * do and that the Web Connector is allowed to connect/that it actually is 
	 * the Web Connector that is connecting and sending us messages.
	 * 
	 * The stdClass object that is received as a parameter will have two 
	 * members:
	 * 	- strUserName	The username provided in the QWC file to the Web Connector
	 * 	- strPassword 	The password the end-user enters into the QuickBooks Web Connector application
	 * 
	 * The return object should be an array with two elements. The first 
	 * element is a generated login ticket (or an empty string if the login 
	 * failed) and the second string is either "none" (for successful log-ins 
	 * with nothing to do in the queue) or "nvu" if the login failed. 
	 * 
	 * @param stdClass $obj						The SOAP object that gets sent by the Web Connector
	 * @return QuickBooks_Result_Authenticate	A container object to send back to the Web Connector
	 */
	public function authenticate($obj)
	{
//		mail2("tngo@fieldsolutions.com", "handler", "auth");
		//  Check IP address, if wrong IP, auth should fail.
		//allowed IP range end 
//		$allowedip = '67.91.185.222';		
		$allowedip = '209.46.27.60';
		$ip = $_SERVER['REMOTE_ADDR'];
		
/*		if($ip != $allowedip) {
			echo "$ip -- $allowedip";

			$error_message = "QB - Authentication Error\r\r";
			$error_message .= "Wrong IP Address: $ip";
			
			mail_error("QB - Authentication Error", $error_message);
			echo "here2";
			log_error("AuthenticationError_" . time() . ".txt", nl2br(htmlentities($error_message)));
			die;
		} */
		
//		mail2("tngo@fieldsolutions.com", "handler", "ip check");
		/**
		 * Queue Stuff
		 * This is put here because authenticate is only called once during
		 * the synchronization process, whereas the server.php file is 
		 * called many times
		 */		
		
		global $doc_root;
		require_once($doc_root . '/accounting/QB/QuickBooks/Queue.php');
		require_once($doc_root . '/accounting/QB/database/db_connect.php');
		mail2("tngo@fieldsolutions.com", "handler", "queuing");
		$qbMysqlHost = "db.fieldsolutions.com";
	        $qbMysqlUser = "fsolutions";
	        $qbMysqlPW = "fire76longer";
	        $qbMysqlDB = "gbailey_technicianbureau";

		$queue = new QuickBooks_Queue("mysql://$qbMysqlUser:$qbMysqlPW@$qbMysqlHost/$qbMysqlDB");

		$member_query = "SELECT member_id FROM quickbooks_iso WHERE address1 = ''";
		$isoFix = array();
		if($member_result = db_connect_query($member_query)) {
			while($member_record = mysql_fetch_array($member_result)) {
				$id = $member_record['member_id'];
				if (empty($id)) continue;
				$isoFix[] = $id;
			}
		}
		if (sizeof($isoFix) > 0) {
			mail2("tngo@fieldsolutions.com", "handler iso", print_r($isoFix, true));
//			require_once($doc_root . '/library/quickbooks.php');
//			copyISOProfile($isoFix);
//			copyISOBankInfo($isoFix);
//			mail2("tngo@fieldsolutions.com", "handler done", print_r($isoFix, true));
		}

		
		// Check what needs to be queued and queue it
		// Queue Vendors
		$member_query = "SELECT id, list_id, 0 AS type FROM quickbooks_members WHERE last_update_by = 'Caspio' AND iso_affiliation_id IS NULL UNION SELECT id, list_id, 1 AS type FROM quickbooks_iso WHERE last_update_by = 'Caspio'";
		if($member_result = db_connect_query($member_query)) {
			while($member_record = mysql_fetch_array($member_result)) {
				$type = $member_record['type'];
				$m_id = $member_record['id'];
				$m_list_id = $member_record['list_id'];
				
				if($m_list_id != "") {
					$queue->enqueue("VendorUpdateEditSequence", $m_id . "-" . $type, 100);
					$queue->enqueue("VendorMod", $m_id . "-" . $type, 50);
					$queue->enqueue('DataExtMod_Vendor', $m_id . "-" . $type, 25);
				}
				else {
					$queue->enqueue('VendorAdd', $m_id . "-" . $type, 100);
					$queue->enqueue('DataExtMod_Vendor', $m_id . "-" . $type, 25);
				}

			}

		}
		else {
			// bad query
			$error = date("Y-m-d G:i:s") . "\r\r";
			$error .= "MySQL error queuing quickbooks_members\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=VendorQueueError_" . time() . ".txt\r\r";
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			$error .= "MySQL Query: " . $member_query . "\r\r";
						
			log_error("VendorQueueError_" . time() . ".txt", nl2br(htmlentities($error)));
			mail_error("QB - Vendor Queue Error", $error);
		}

		
		
		// Queue Clients
		/*
		$client_query = "SELECT id, list_id FROM clients WHERE last_update_by = 'Caspio'";
		if($client_result = db_connect_query($client_query)) {
			while($client_record = mysql_fetch_array($client_result)) {
				$c_id = $client_record['id'];
				$c_list_id = $client_record['list_id'];
				
				if($m_list_id != "") {
					$queue->enqueue("CustomerUpdateEditSequence", $c_id, 100);
					$queue->enqueue("CustomerMod", $c_id, 50);
				}
				else {
					$queue->enqueue('CustomerAdd', $c_id, 100);
				}
			}
		}
		else {
			// bad query
			$error = date("Y-m-d G:i:s") . "\r\r";
			$error .= "MySQL error queuing customer\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=CustomerQueueError_" . time() . ".txt\r\r";
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			$error .= "MySQL Query: " . $client_query . "\r\r";
						
			log_error("CustomerQueueError_" . time() . ".txt", nl2br(htmlentities($error)));
			mail_error("QB - Customer Queue Error", $error);
		}
		*/
		
		// Queue Bills
/*		$bill_query = "SELECT id FROM quickbooks_bills WHERE last_update_by = 'Caspio' AND caspio_sync = 0";
		if($bill_result = db_connect_query($bill_query)) {
			while($bill_record = mysql_fetch_array($bill_result)) {
				$b_id = $bill_record['id'];				
				$queue->enqueue("BillAdd", $b_id, 50);
			}
		}
		else {
			// bad query
			$error = date("Y-m-d G:i:s") . "\r\r";
			$error .= "MySQL error queuing bill\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=BillQueueError_" . time() . ".txt\r\r";
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			$error .= "MySQL Query: " . $client_query . "\r\r";
						
			log_error("BillQueueError_" . time() . ".txt", nl2br(htmlentities($error)));
			mail_error("QB - Bill Queue Error", $error);
		}*/

		// Queue a bill Query to get paid bills
		$queue->enqueue('BillQuery');

		
		// End Queue Stuff
		
		$this->_driver->log('authenticate()', '', QUICKBOOKS_LOG_VERBOSE);
		
		$ticket = '';
		$status = '';
		
		$override_dsn = $this->_config['authenticate_dsn'];
		$auth = null;

//		echo "HERE 20202";
		
		if (strlen($override_dsn))
		{
			$parse = QuickBooks_Utilities::parseDSN($override_dsn);
			$class = 'QuickBooks_Authenticate_' . $parse['scheme'];
			
			require_once($doc_root . '/accounting/QB/QuickBooks/Authenticate/' . ucfirst(strtolower($parse['scheme'])) . '.php');
			
			$auth = new $class($override_dsn);
		}
		
		if (strlen($override_dsn) and 	// Custom authentication
			is_object($auth))
		{
			if ($auth->authenticate($obj->strUserName, $obj->strPassword) and 
				$ticket = $this->_driver->authLogin($obj->strUserName, $obj->strPassword, true))
			{
				$this->_driver->log('Login (' . $parse['scheme'] . '): ' . $obj->strUserName, $ticket, QUICKBOOKS_LOG_DEBUG);
				
				if (!$this->_driver->queueDequeue())
				{
					$status = 'none';
				}
				
				// Login success (with a custom login handler)!
			}
			else
			{
				$this->_driver->log('Login failed (' . $parse['scheme'] . '): ' . $obj->strUserName, '', QUICKBOOKS_LOG_DEBUG);
				
				$ticket = '';
				$status = 'nvu'; // Invalid username/password
			}
			
			return new QuickBooks_Result_Authenticate($ticket, $status);
		} 
		else	// Standard authentication
		{
			//mail_error("QB - Authentication Error", "HERE - 1 " . $obj->strUserName);
			if ($ticket = $this->_driver->authLogin($obj->strUserName, $obj->strPassword))
			{
				$this->_driver->log('Login: ' . $obj->strUserName, $ticket, QUICKBOOKS_LOG_DEBUG);
				
				if (!$this->_driver->queueDequeue())
				{
					$status = 'none'; // Good login, but there isn't anything in the queue
				}
				
				// Login success!
			}
			else
			{
				echo "FAIL nvu " . $obj->strUserName . " " . $obj->strPassword . print_r($obj,true); die();
				$this->_driver->log('Login failed: ' . $obj->strUserName, '', QUICKBOOKS_LOG_DEBUG);
				
				$ticket = '';
				$status = 'nvu'; // Invalid username/password
			}
			
			return new QuickBooks_Result_Authenticate($ticket, $status);
		}
	}
	
	/**
	 * SendRequestXML method for the QuickBooks Web Connector SOAP server - Generate and send a request to QuickBooks
	 * 
	 * The QuickBooks Web Connector calls this method to ask for things to do. 
	 * So, calling this method is the Web Connectors way of saying: "Please 
	 * send me a command so that I can pass that command on to QuickBooks." 
	 * After it passes the command to QuickBooks, it will pass the response 
	 * back via a call to receiveResponseXML(). 
	 * 
	 * The stdClass object passed as a parameter should contain these members: 
	 * 	- ticket				The login session ticket
	 *  - strHCPResponse		
	 * 	- strCompanyFileName	
	 * 	- qbXMLCountry			The country code for whatever version of QuickBooks is sitting behind the Web Connector
	 * 	- qbXMLMajorVers		The major version code of the QuickBooks web connector
	 * 	- qbXMLMinorVers 		The minor version code of the QuickBooks web connector
	 * 
	 * You should return either an empty string "" to signal an error state, or 
	 * a valid qbXML or qbposXML request. 
	 * 
	 * @param stdClass $obj
	 * @return QuickBooks_Result_SendRequestXML
	 */
	public function sendRequestXML($obj)
	{
		$this->_driver->log('sendRequestXML()', $obj->ticket, QUICKBOOKS_LOG_VERBOSE);
		
		if ($this->_driver->authCheck($obj->ticket)) 
		{
			if ($next = $this->_driver->queueDequeue(true)) // Fetch the next action/command from the queue
			{
				$this->_driver->log('Dequeued: ( ' . $next['qb_action'] . ', ' . $next['ident'] . ' ) ', $obj->ticket, QUICKBOOKS_LOG_DEBUG);
				
				// Here's a strange case, interactive mode handler
				if ($next['qb_action'] == QUICKBOOKS_INTERACTIVE_MODE)
				{
					// Set the error to "Interactive mode"
					$this->_driver->errorLog($obj->ticket, QUICKBOOKS_ERROR_OK, QUICKBOOKS_INTERACTIVE_MODE);
					
					// This will cause ->getLastError() to be called, and ->getLastError() will then return the string "Interactive mode" which will cause QuickBooks to call ->getInteractiveURL() and start an interactive session... I think...?
					return new QuickBooks_Result_SendRequestXML('');
				}
				
				$extra = '';
				if ($next['extra'])
				{
					$extra = unserialize($next['extra']);
				}
				
				$err = '';
				$xml = '';
				
				// Call the mapped function that should generate an appropriate qbXML request
				$xml = $this->_callMappedFunction(0, $next['qb_action'], $next['ident'], $extra, $err);
				
				if (!$this->_extractRequestID($xml) and 
					$this->_config['autoadd_missing_requestid'] and 
					false !== strpos($xml, '<' . $next['qb_action'] . 'Rq')) 
				{
					// Stupid person didn't add a requestID field, now we need to fix it for them
					
					$xml = str_replace(
						'<' . $next['qb_action'], 
						'<' . $next['qb_action'] . ' requestID="' . $this->_constructRequestID($next['qb_action'], $next['ident']) . '" ', 
						$xml);
				}
				
				if ($err) // The function encountered an error when generating the qbXML request
				{
					//$this->_driver->errorLog($obj->ticket, QUICKBOOKS_ERROR_HANDLER, $err);
					//$this->_driver->log('ERROR: ' . $err, $obj->ticket, QUICKBOOKS_LOG_NORMAL);
					//$this->_driver->queueStatus($obj->ticket, $next['qb_action'], $next['ident'], QUICKBOOKS_STATUS_ERROR, 'Registered handler returned error: ' . $err);
					
					$errerr = '';
					$this->_handleError($obj->ticket, QUICKBOOKS_ERROR_HANDLER, $err, $this->_constructRequestID($next['qb_action'], $next['ident']), $next['qb_action'], $next['ident'], $extra, $errerr, $xml);
					
					return new QuickBooks_Result_SendRequestXML('');
				}
				else
				{
					$this->_driver->log('Outgoing XML request: ' . $xml, $obj->ticket, QUICKBOOKS_LOG_DEBUG);
					
					if (strlen($xml) and  // Returned XML AND 
						!$this->_extractRequestID($xml)) // Does not have a requestID in the request
					{
						// Mark it as successful right now
						$this->_driver->queueStatus($obj->ticket, $next['qb_action'], $next['ident'], QUICKBOOKS_STATUS_SUCCESS, 'Unverified... no requestID attribute in XML stream.');
					}
					else
					{
						// Mark it as in progress
						$this->_driver->queueStatus($obj->ticket, $next['qb_action'], $next['ident'], QUICKBOOKS_STATUS_PROCESSING);
					}
					
					return new QuickBooks_Result_SendRequestXML($xml);
				}
			}
		}
		
		// Reporting an error, this will cause the QBWC to call ->getLastError()
		return new QuickBooks_Result_SendRequestXML('');
	}
	
	/**
	 * Extract the requestID attribute from an XML stream
	 * 
	 * @param string $xml	The XML stream to look for a requestID attribute in
	 * @return mixed		The request ID
	 */
	protected function _extractRequestID($xml)
	{
		if (false !== ($start = strpos($xml, ' requestID="')) and 
			false !== ($end = strpos($xml, '"', $start + 12)))
		{
			return substr($xml, $start + 12, $end - $start - 12);
		}
		
		return false;
	}
	
	/**
	 * Create a requestID string from action and ident parts
	 * 
	 * @param string $action
	 * @param mixed $ident
	 * @return string
	 */
	protected function _constructRequestID($action, $ident)
	{
		return base64_encode($action . '|' . $ident . '|' . date("m-d-Y H:i:s"));
	}
	
	/**
	 * Parse a requestID string into it's action and ident parts
	 * 
	 * @param string $requestID
	 * @param string $action
	 * @param mixed $ident
	 * @return void
	 */
	protected function _parseRequestID($requestID, &$action, &$ident)
	{
		$tmp = explode('|', base64_decode($requestID));
		$action = $tmp[0];
		$ident = $tmp[1];
	}
	
	/**
	 * Extract a unique record identifier from an XML response
	 * 
	 * Some (most?) records within QuickBooks have unique identifiers which are 
	 * returned with the qbXML responses. This method will try to extract all  
	 * identifiers it can find from a qbXML response and return them in an 
	 * associative array. 
	 * 
	 * For example, Customers have unique ListIDs, Invoices have unique TxnIDs, 
	 * etc. For an AddCustomer request, you'll get an array that looks like 
	 * this:
	 * <code>
	 * array(
	 * 	'ListID' => '2C0000-1039887390'
	 * )
	 * </code>
	 * 
	 * Other transactions might have more than one identifier. For instance, a 
	 * call to AddInvoice returns both a ListID and a TxnID:
	 * <code>
	 * array(
	 * 	'ListID' => '200000-1036881887', // This is actually part of the 'CustomerRef' entity in the Invoice XML response 
	 * 	'TxnID' => '11C26-1196256987', // This is the actual transaction ID for the Invoice XML response
	 * )
	 * </code>
	 * 
	 * *** IMPORTANT *** If there are duplicate fields (i.e.: 3 different 
	 * ListIDs returned) then only the first value encountered will appear in 
	 * the associative array.  
	 * 
	 * The following elements/attributes are supported:
	 * 	- ListID
	 * 	- TxnID
	 * 	- iteratorID
	 * 	- OwnerID
	 * 	- TxnLineID
	 * 
	 * @param string $xml	The XML stream to look for an identifier in
	 * @return array		An associative array mapping identifier fields to identifier values
	 */
	protected function _extractIdentifiers($xml)
	{
		$list = array();
		
		if (false !== ($start = strpos($xml, '<ListID>')) and 
			false !== ($end = strpos($xml, '</ListID>')))
		{
			$list['ListID'] = substr($xml, $start + 8, $end - $start - 8);
		}
		
		if (false !== ($start = strpos($xml, '<TxnID>')) and 
			false !== ($end = strpos($xml, '</TxnID>')))
		{
			$list['TxnID'] = substr($xml, $start + 7, $end - $start - 7);
		}
		
		if (false !== ($start = strpos($xml, ' iteratorID="')) and 
			false !== ($end = strpos($xml, '"', $start + 13)))
		{
			$list['iteratorID'] = substr($xml, $start + 13, $end - $start - 13);
		}
		
		if (false !== ($start = strpos($xml, '<OwnerID>')) and 
			false !== ($end = strpos($xml, '</OwnerID>')))
		{
			$list['OwnerID'] = substr($xml, $start + 9, $end - $start - 9);	
		}
		
		if (false !== ($start = strpos($xml, '<TxnLineID>')) and 
			false !== ($end = strpos($xml, '</TxnLineID>')))
		{
			$list['TxnLineID'] = substr($xml, $start + 11, $end - $start - 11);
		}
		
		return $list;
	}
	
	/**
	 * Extract the status code from an XML response
	 * 
	 * Each qbXML response should return a status code and a status message 
	 * indicating whether or not an error occured. 
	 * 
	 * @param string $xml	The XML stream to look for a response status code in
	 * @return integer		The response status code (0 if OK, another positive integer if an error occured)
	 */
	protected function _extractStatusCode($xml)
	{
		if (false !== ($start = strpos($xml, ' statusCode="')) and 
			false !== ($end = strpos($xml, '" ', $start + 13)))
		{
			return substr($xml, $start + 13, $end - $start - 13);
		}
		
		return 1;
	}
	
	/**
	 * Extract the status message from an XML response
	 * 
	 * Each qbXML response should return a status code and a status message 
	 * indicating whether or not an error occured. 
	 * 
	 * @param string $xml	The XML stream to look for a response status message in
	 * @return string		The response status message
	 */
	protected function _extractStatusMessage($xml)
	{
		if (false !== ($start = strpos($xml, ' statusMessage="')) and 
			false !== ($end = strpos($xml, '" ', $start + 16)))
		{
			return substr($xml, $start + 16, $end - $start - 16);
		}
		
		return '';
	}
	
	/**
	 * Call the mapped function for a given action 
	 * 
	 * @param integer $which			Whether or call the request action handler (pass a 0) or the response action handler (pass a 1)
	 * @param string $action			
	 * @param mixed $ident				
	 * @param mixed $extra
	 * @param string $err				If the function returns an error message, the error message will be stored here
	 * @param string $xml				A qbXML response (if you're calling the response handler)
	 * @param array $qb_identifier		
	 * @return string
	 */
	protected function _callMappedFunction($which, $action, $ident, $extra, &$err, $xml = '', $qb_identifier = array())
	{
		// Call the appropriate callback function 
		if (isset($this->_map[$action]))
		{
			if (isset($this->_map[$action][$which]))
			{
				$func = $this->_map[$action][$which];
				if (function_exists($func))
				{
					$requestID = $this->_constructRequestID($action, $ident);
					$err = '';
					$xml = $func($requestID, $action, $ident, $extra, $err, $xml, $qb_identifier);
					
					return $xml;
				}
				else
				{
					// A function was registered, but the function doesn't exist
					$err = 'Registered function does not exist: ' . $this->_map[$action][$which];
				}
			}
			else
			{
				// There was no function registered for that action and request/response
				$err = 'No function handlers for action: ' . $action;
			}
		}
		else
		{
			// There are *no* functions registered for that action
			$err = 'No registered functions for action: ' . $action;
		}
		
		return '';
	}
	
	/**
	 * Call an error-handler function and update the status of a request to ERROR
	 * 
	 * @param string $ticket
	 * @param integer $errnum		The error number from QuickBooks (see the QuickBooks SDK/IDN for a list of error codes)
	 * @param string $errmsg		The error message from QuickBooks
	 * @param string $requestID
	 * @param string $action
	 * @param mixed $ident
	 * @param array $extra
	 * @param string $err
	 * @param string $xml
	 * @param array $qb_identifiers
	 */
	protected function _handleError($ticket, $errnum, $errmsg, $requestID, $action, $ident, $extra, &$err, $xml, $qb_identifiers = array())
	{
		// Call the error handler (if one is set)
		
		$func = '';
		if (isset($this->_onerror[$errnum]) and function_exists($this->_onerror[$errnum]))
		{
			$func = $this->_onerror[$errnum];
		}
		else if (isset($this->_onerror[$action]))
		{
			$func = $this->_onerror[$action];
		}
		else if (isset($this->_onerror['!']) and function_exists($this->_onerror['!']))
		{
			$func = $this->_onerror['!'];
		}
		
		if ($func)	// Call the error handler function
		{
			$errerr = '';	// This is an error message *returned by* the error handler function
			$func($requestID, $action, $ident, $extra, $errerr, $xml, $errnum, $errmsg);
			// $requestID, $action, $IDX, $extra, $err, $xml, $errnum, $errmsg
		}
		
		// Log the last error (for the ticket)
		$this->_driver->errorLog($ticket, $errnum, $errmsg);
		
		// Log the last error (for the log)
		$this->_driver->log('Error: ' . $errnum . ': ' . $errmsg, $ticket, QUICKBOOKS_LOG_NORMAL);
		
		// Update the queue status
		if ($action and $ident)
		{
			return $this->_driver->queueStatus($ticket, $action, $ident, QUICKBOOKS_STATUS_ERROR, $errnum . ': ' . $errmsg);
		}
		
		return true;
	}
	
	/**
	 * ReceiveResponseXML() method for the QuickBooks Web Connector - Receive and handle a resonse form QuickBooks 
	 * 
	 * The stdClass object passed as a parameter will have the following members: 
	 * 	- ->ticket 		The QuickBooks Web Connector ticket
	 * 	- ->response	An XML response message
	 * 	- ->hresult		Error code
	 * 	- ->message		Error message
	 * 
	 * The sole data member of the returned object should be an integer. 
	 * 	- The data member should be -1 if an error occured and QBWC should call ->getLastError()
	 * 	- Should be an integer 0 <= x < 100 to indicate success *and* that the application should continue to call ->sendRequestXML() at least one more time (more queued items still in the queue, the integer represents the percentage complete the total batch job is)
	 * 	- Should be 100 to indicate success *and* that the queue has been exhausted
	 * 
	 * @param stdClass $obj
	 * @return QuickBooks_Result_ReceiveResponseXML
	 */
	public function receiveResponseXML($obj)
	{
		$this->_driver->log('receiveResponseXML()', $obj->ticket, QUICKBOOKS_LOG_VERBOSE);
		
		if ($this->_driver->authCheck($obj->ticket)) // Check the ticket
		{
			$this->_driver->log('Incoming XML response: ' . $obj->response, $obj->ticket, QUICKBOOKS_LOG_DEBUG);
			
			// Check if we got a error message...
			if (strlen($obj->message) or 
				$this->_extractStatusCode($obj->response)) // or an error code
			{
				if ($requestID = $this->_extractRequestID($obj->response)) // This will be non-0 if an error occurs
				{
					$errnum = $this->_extractStatusCode($obj->response);
					
					//$action = current(explode('|', $requestID));
					//$ident = next(explode('|', $requestID));
					$action = '';
					$ident = '';
					$this->_parseRequestID($requestID, $action, $ident);
					
					$extra = '';
					if ($current = $this->_driver->queueFetch($action, $ident, QUICKBOOKS_STATUS_PROCESSING))
					{
						if ($current['extra'])
						{
							$extra = unserialize($current['extra']);
						}
					}
					
					if ($obj->message)
					{
						$errmsg = $obj->message;
					}
					else if ($status = $this->_extractStatusMessage($obj->response))
					{
						$errmsg = $status;
					}
					
					$errerr = '';
					$this->_handleError($obj->ticket, $errnum, $errmsg, $requestID, $action, $ident, $extra, $errerr, $obj->response, array());
					//					$errnum, $errmsg, $requestID, $action, $ident, $extra, &$err, $xml, $qb_identifiers = array()
				}
				else	// generic error (poorly encoded XML, XML syntax error, etc.)
				{
					$errerr = '';
					$this->_handleError($obj->ticket, $obj->hresult, $obj->message, null, null, null, null, $errerr, $obj->response, array()); 
				}
				
				return new QuickBooks_Result_ReceiveResponseXML(-1);
			}
			
			$action = null;
			$ident = null;
			
			$requestID = null;
			if ($requestID = $this->_extractRequestID($obj->response))
			{
				//$action = current(explode('|', $requestID));
				//$ident = end(explode('|', $requestID));
				$action = '';
				$ident = '';
				$this->_parseRequestID($requestID, $action, $ident);
				
				$extra = '';
				if ($current = $this->_driver->queueFetch($action, $ident, QUICKBOOKS_STATUS_PROCESSING))
				{
					if ($current['extra'])
					{
						$extra = unserialize($current['extra']);
					}
				}
				
				$this->_driver->queueStatus($obj->ticket, $action, $ident, QUICKBOOKS_STATUS_SUCCESS);
			}
			
			$identifiers = $this->_extractIdentifiers($obj->response);
			
			$err = null;
			
			$this->_callMappedFunction(1, $action, $ident, $extra, $err, $obj->response, $identifiers);
			
			if ($err)
			{
				$errerr = '';
				$this->_handleError($obj->ticket, QUICKBOOKS_ERROR_HANDLER, $err, $requestID, $action, $ident, $extra, $errerr, $obj->response, $identifiers);
				
				return new QuickBooks_Result_ReceiveResponseXML(-1);
			}
			
			$current = $this->_driver->queueSize();
			$processed = $this->_driver->queueProcessed($obj->ticket);
			
			if ($current)
			{
				$progress = min(99, floor(100 * ($processed / ($processed + $current))));
			}
			else
			{
				$progress = 100;
			}
			
			$this->_driver->log($progress . '% complete... ', $obj->ticket, QUICKBOOKS_LOG_VERBOSE);
			
			return new QuickBooks_Result_ReceiveResponseXML($progress);
		}
		
		return new QuickBooks_Result_ReceiveResponseXML(-1);
	}

	/**
	 * QuickBooks Web Connector ->connectionError() SOAP method
	 * 
	 * The stdClass object passed in as a parameter has these members:
	 * 	- ->ticket 		The ticket string
	 * 	- ->hresult		An error code
	 * 	- ->message 	An error message from QuickBooks/QBWC
	 * 
	 * @param stdClass $obj
	 * @return QuickBooks_Result_ConnectionError
	 */
	public function connectionError($obj)
	{
		$this->_driver->log('connectionErrorXML()', $obj->ticket, QUICKBOOKS_LOG_VERBOSE);
		
		if ($this->_driver->authCheck($obj->ticket))
		{
			return new QuickBooks_Result_ConnectionError('done');
		}
		
		return new QuickBooks_Result_ConnectionError('done');
	}

	/**
	 * QuickBooks Web Connector ->getLastError() SOAP method
	 * 
	 * The stdClass object passed in as a parameter has these members:
	 * 	- ->ticket		The ticket string
	 * 
	 * The returned object should have just one member, an error message describing the last error that occured
	 * 
	 * @return QuickBooks_Result_GetLastError
	 */
	public function getLastError($obj)
	{
		$this->_driver->log('getLastError()', $obj->ticket, QUICKBOOKS_LOG_VERBOSE);
		
		if ($this->_driver->authCheck($obj->ticket))
		{
			$lasterr = $this->_driver->errorLast($obj->ticket);
			
			return new QuickBooks_Result_GetLastError($lasterr);
		}
	
		return new QuickBooks_Result_GetLastError('Bad ticket.');
	}
	
	/**
	 * QuickBooks Web Connector ->closeConnection() SOAP method
	 * 
	 * The stdClass object passed in as a parameter has these members:
	 * 	- ->ticket 		The ticket string
	 * 
	 * The sole member of the returned object should be a string describing the reason for closing the connection
	 * 
	 * @return QuickBooks_Result_CloseConnection
	 */
	public function closeConnection($obj)
	{
		// count amount transferred to QB and the number of bills, log it and send an email
		$ticket = $obj->ticket;
		global $last_update;
		
		$query = "SELECT bills_total, number_of_bills, num_paid, amount_paid, num_new_techs, num_mod_techs, touch_datetime, quickbooks_ticket_id FROM quickbooks_ticket WHERE ticket = '$ticket'";
		if($result = db_connect_query($query)) {
			if($record = mysql_fetch_array($result)) {
			
				$amt_bills = $record['bills_total'];
				$num_bills = $record['number_of_bills'];
				$amt_paid = $record['amount_paid'];
				$num_paid = $record['num_paid'];
//				$new_techs = $record['num_new_techs'];
//				$mod_techs = $record['num_mod_techs'];
				$datetime = $record['touch_datetime'];
				
				$quickbooks_ticket_id = $record['quickbooks_ticket_id'];
				
				// Get list of techs adds / mods
				
				$addResult = db_connect_query("SELECT member_id FROM quickbooks_queue AS qbq LEFT JOIN quickbooks_members AS mem ON qbq.ident = mem.id WHERE qbq.quickbooks_ticket_id = '$quickbooks_ticket_id' AND qbq.qb_action = 'VendorAdd'");
				
				$new_techs = mysql_num_rows($addResult);
				
				$modResult = db_connect_query("SELECT member_id FROM quickbooks_queue AS qbq LEFT JOIN quickbooks_members AS mem ON qbq.ident = mem.id WHERE qbq.quickbooks_ticket_id = '$quickbooks_ticket_id' AND qbq.qb_action = 'VendorMod'");

				$mod_techs = mysql_num_rows($modResult);
				


				require_once($_SERVER["DOCUMENT_ROOT"] . '/accounting/QB/BillQueryDateRange.php');
				$dateRange = getBillQueryDateRange();
				
				if (!$dateRange)
					$rangeTxt = "- 7 days";
				else if (isset($dateRange["MovingRange"]))
					$rangeTxt = $dateRange["MovingRange"];
				else
					$rangeTxt = $dateRange["StartRange"] . " thru " . $dateRange["EndRange"];
						
				$to = 'quickbooks@fieldsolutions.com';
				$message = "Sync Log for ticket: $ticket\r";
				$message .= "Completed: $datetime\r\r";
				$message .= "Bills Sent to QuickBooks -\r";
				$message .= "\tTotal Amount: $amt_bills\r";
				$message .= "\tNumber of Bills: $num_bills\r\r";
				$message .= "Bills Paid - (Range:  $rangeTxt)\r";
				$message .= "\tAmount Paid: $amt_paid\r";
				$message .= "\tNumber of Bills Paid: $num_paid\r\r";
				$message .= "Techs Added/Modifed -\r";
				$message .= "\tNumber of New Techs: $new_techs\r";
				$message .= "\tNumber of Mod Techs: $mod_techs\r\r";

				if ($new_techs > 0) {
					$message .= "New Techs List:\r\r";
					$message .= "TechID\r";
					
					while ($row = mysql_fetch_row($addResult)) {
						$message .= "{$row[0]}\r";
					}
					$message .= "\r";
				}

				if ($mod_techs > 0) {
					$message .= "Mod Techs List:\r\r";
					$message .= "TechID\r";
					
					while ($row = mysql_fetch_row($modResult)) {
						$message .= "{$row[0]}\r";
					}
					$message .= "\r";
				}

				// Log bills sent to QB
/*				$woInvoicedResult = db_connect_query("SELECT wo_id FROM quickbooks_queue LEFT JOIN bills ON ident = id WHERE quickbooks_ticket_id = '$quickbooks_ticket_id' AND qb_action = 'BillAdd'");

				if (mysql_num_rows($woInvoicedResult) > 0) {
					$message .= "Bills Sent to QB:\r\r";
					$message .= "WO_ID\r";
					
					while ($row = mysql_fetch_row($woInvoicedResult)) {
						$message .= "{$row[0]}\r";
					}
				}*/

				mail2($to, "Sync Log", $message);				
				log_error("SyncLog_" . $ticket . ".txt", $message);
			}
			else {
				$error = $last_update . "\r\r";
				$error .= "MySQL error reading quickbooks_ticket table\r\r";
				$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=BillsTotalError_" . $ticket . ".txt\r\r";
				$error .= "MySQL Error: " . mysql_error() . "\r\r";
				$error .= "Ticket:\r" . $ticket;
				
				log_error("BillsTotalError_" . $requestID . ".txt", $error);
				mail_error("QB - MySQL Bills Total Error", $error);
			}
		}
		else {
			$error = $last_update . "\r\r";
			$error .= "MySQL error reading quickbooks_ticket table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=BillsTotalError_" . $ticket . ".txt\r\r";
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			$error .= "Ticket:\r" . $ticket;
			
			log_error("BillsTotalError_" . $ticket . ".txt", $error);
			mail_error("QB - MySQL Bills Total Error", $error);
		}
		// end count
		
		// Delete Password File
//		unlink("/tmp/lexiconjob.txt");
		unsetSyncVar("lexiconjob");
		// End Delete Password File
		
		$this->_driver->log('closeConnection()', $obj->ticket, QUICKBOOKS_LOG_VERBOSE);
		
		if ($this->_driver->authCheck($obj->ticket))
		{
			
			
			// 
			return new QuickBooks_Result_CloseConnection('Complete!');
		}
		
		// bad ticket
		return new QuickBooks_Result_CloseConnection('Bad ticket.');
	}
	
	/**
	 * QuickBooks Web Connector ->getServerVersion() SOAP method
	 * 
	 * @param stdClass $obj
	 * @return QuickBooks_Result_GetServerVersion
	 */
	public function serverVersion($obj)
	{
		$this->_driver->log('serverVersion()', '', QUICKBOOKS_LOG_VERBOSE);
		
		return new QuickBooks_Result_ServerVersion($this->_config['server_version']);
	}
	
	/**
	 * QuickBooks Web Connector ->clientVersion() SOAP method - Receive the QuickBooks Web Connector client version (and, if neccessary, act on it)
	 * 
	 * This is an *optional* method, and not all versions of the QuickBooks Web 
	 * Connector will support this method. It doesn't really even *need* to be 
	 * implemented, but PHP will dump notices in our error log if we don't 
	 * implement it and then the web connector tries to call it. 
	 * 
	 * The stdClass object passed as a parameter will have the following members:
	 * 	- strVersion	A string version code indicating the version of the QuickBooks Web Connector that's being used
	 * 
	 * The one member of the returned object should be:
	 * 	- The empty string to tell the web connector to continue with the update
	 * 	- A string that begins with "W:" to display a warning message to the end-user, specify the warning message after the "W:"
	 * 	- A string that begins with "E:" to display an error message to the end-user, specify the error message after the "E:"
	 * 	- A string that begins with "O:" (as in OKAY) to tell the end-user that the server expects a newer version of the web connector, specify the minimum required version after the "O:"
	 * 
	 * @param stdClass $obj
	 * @return QuickBooks_Result_ClientVersion
	 */
	public function clientVersion($obj)
	{
		$this->_driver->log('clientVersion()', '', QUICKBOOKS_LOG_VERBOSE);
		
		if (!is_null($this->_config['qbwc_min_version']))
		{
			if (version_compare($obj->strVersion, $this->_config['qbwc_min_version'], '<'))
			{
				$this->_driver->log('Version Requirement, current: ' . $obj->strVersion . ', required: ' . $this->_config['qbwc_min_version'], '', QUICKBOOKS_LOG_NORMAL);
				
				return new QuickBooks_Result_ClientVersion('O:' . $this->_config['qbwc_min_version']);
			}
		}

		return new QuickBooks_Result_ClientVersion('');
	}
	
	/**
	 * QuickBooks Web Connector ->getInteractiveURL() SOAP method - Get the URL to use for an interactive session
	 * 
	 * The stdClass object passed as a parameter will have the following members:
	 * 	- ticket		The ticket string
	 * 	- sessionID		??? (undocumented in QBWC documentation...?)
	 * 
	 * @param stdClass $obj
	 * @return QuickBooks_Result_GetInteractiveURL
	 */
	public function getInteractiveURL($obj)
	{
		$this->_driver->log('getInteractiveURL()', '', QUICKBOOKS_LOG_VERBOSE);
		
		return new QuickBooks_Result_GetInteractiveURL($this->_config['qbwc_interactive_url']);
	}
	
	public function interactiveRejected($obj)
	{
		
	}
	
	/**
	 * 
	 * The stdClass object passed as a parameter will have the following members:
	 * 	- ticket
	 * 
	 * @param stdClass $obj
	 * @return QuickBooks_Result_InteractiveDone
	 */
	public function interactiveDone($obj)
	{
		return new QuickBooks_Result_InteractiveDone('Done');
	}
	
	/*******************************************************************/
	/****************  NON QUEUE PROCESSING FUNCTIONS  *****************/
	/*******************************************************************/
	
	// used for emailing when errors happen
	function mail_error($subject, $message) {
		$to      = 'quickbooks@fieldsolutions.com';
			echo "mailing";
		mail2($to, $subject, $message);
	}
	
	// used to log error when they happen
	function log_error($log_file, $error) {
		$date = date("Ymd");
		
		if(!is_dir("/tmp/QBLOG/$date")) {
			mkdir("/tmp/QBLOG/$date");
		}
		
/*		if($fp = fopen("/tmp/QBLOG/$date/$log_file", "a+")) {
			fwrite($fp, $error);
			fclose($fp);
		}
		else {
			$mail_error = $last_upadate . "\r\r";
			$mail_error .= "Unable to write log to /tmp/QBLOG/$date/$log_file\r\r";
			$mail_error .= "Error: $error";
			mail_error("QB - Log Failed", $error);
		}*/
	}
}

?>
