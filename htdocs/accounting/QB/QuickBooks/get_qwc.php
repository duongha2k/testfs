<? 

$doc_root = $_SERVER["DOCUMENT_ROOT"];

include($doc_root . '/QuickBooks/Utilities.php');

/**
 * Generate a valid QuickBooks Web Connector *.QWC file 
 * 
 * @param string $name			The name of the QuickBooks Web Connector job (something descriptive, this gets displayed to the end-user)
 * @param string $descrip		A short description of the QuickBooks Web Connector job (something descriptive, this gets displayed to the end-user)
 * @param string $appurl		The absolute URL to the SOAP server (this *MUST* be a HTTPS:// link *UNLESS* it's running on localhost)
 * @param string $appsupport	A URL where an end-user can go to get support for the application
 * @param string $username		The username that QuickBooks Web Connector should use to connect
 * @param string $fileid		A file-ID value... apparently you can just make this up, make it resemble this string: {57F3B9B6-86F1-4fcc-B1FF-966DE1813D20}
 * @param string $ownerid		As above, apparently you can just make this up, make it resemble this string: {57F3B9B6-86F1-4fcc-B1FF-966DE1813D20}
 * @param string $qbtype		Either QUICKBOOKS_TYPE_QBFS or QUICKBOOKS_TYPE_QBPOS
 * @param boolean $readonly		Whether or not to open the connection as read-only
 * @param integer $run_every_n_seconds		If you want to schedule the job to run every once in a while automatically, you can pass in a number of seconds between runs here
 * @return string
 */

echo 'Here is your QWC:<br /><br />';

$name = "QuickBooks Test";
$descrip = "Test Import DB info into QB";
$appurl = "https://www.fieldsolutions.com/Accounting/QuickBooks/Server/";
$appsupport = "https://www.fieldsolutions.com";
$username = "quickbooks";
$fileid = "57F3B9B6-86F1-4fcc-B1FF-966DE1813D20";
$ownerid = "57F3B9B6-86F1-4fcc-B1FF-966DE1813D20";

$xml = generateQWC($name, $descrip, $appurl, $appsupport, $username, $fileid, $ownerid, $qbtype = QUICKBOOKS_TYPE_QBFS, $readonly = true, $run_every_n_seconds = null);

print("
	<pre>
		$xml
	</pre>
");





function generateQWC($name, $descrip, $appurl, $appsupport, $username, $fileid, $ownerid, $qbtype = QUICKBOOKS_TYPE_QBFS, $readonly = true, $run_every_n_seconds = null)
	{
	$xml = '
	<?xml version="1.0"?>
	<QBWCXML>
		<AppName>' . htmlentities($name) . '</AppName>
		<AppID></AppID>
		<AppURL>' . htmlentities($appurl) . '</AppURL>
		<AppDescription>' . htmlentities($descrip) . '</AppDescription>
		<AppSupport>' . htmlentities($appsupport) . '</AppSupport>
		<UserName>' . htmlentities($username) . '</UserName>
		<OwnerID>' . $ownerid . '</OwnerID>
		<FileID>' . $fileid . '</FileID>
		<QBType>' . $qbtype . '</QBType>
	';
	
	if ((int) $run_every_n_seconds > 0 and (int) $run_every_n_seconds < 60)
	{
		$xml .= '
			<Scheduler>
				<RunEveryNSeconds>' . (int) $run_every_n_seconds . '</RunEveryNSeconds>
			</Scheduler>
		';
	}
	else if ((int) $run_every_n_seconds >= 60)
	{
		$xml .= '
			<Scheduler>
				<RunEveryNMinutes>' . floor($run_every_n_seconds / 60) . '</RunEveryNMinutes>
			</Scheduler>
		';
	}
	
	if ($readonly)
	{
		$xml .= '<IsReadOnly>true</IsReadOnly>';
	}
	else
	{
		$xml .= '<IsReadOnly>false</IsReadOnly>';
	}
	
	$xml .= '
	</QBWCXML>';
	
	return trim($xml);
}
?>