<?php

/**
 * Base class for QuickBooks objects
 * 
 * This is slated for a future release of the class... it isn't currently used.
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 * 
 * @package QuickBooks
 */

/**
 * Base class for QuickBooks objects
 */
abstract class QuickBooks_Object
{
	/**
	 * Keys/values stored within the object
	 * 
	 * @var array
	 */
	protected $_object = array();
	
	/**
	 * Set a value within the object
	 * 
	 * @param string $key
	 * @param string $value
	 * @return void
	 */
	public function set($key, $value)
	{
		$this->_object[$key] = $value;
	}
	
	/**
	 * Get a value from the object
	 * 
	 * @param string $key		The key to fetch the value for
	 * @param mixed $default	If there is no value set for the given key, this will be returned
	 * @return mixed			The value fetched
	 */
	public function get($key, $default = null)
	{
		if (isset($this->_object[$key]))
		{
			return $this->_object[$key];
		}
		
		return $default;
	}
	
	/**
	 * 
	 */
	protected function _arrayToXML($arr, $required_elems = array())
	{
		
	}
	
	/**
	 * Convert this object to a valid qbXML request/response
	 * 
	 * @return string
	 */
	public function toXML()
	{
		
	}
}

?>