<?php

/**
 * QuickBooks Customer object container
 * 
 * Not used, might be used in future versions
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 * 
 * @package QuickBooks
 * @subpackage Object
 */

$doc_root = $_SERVER["DOCUMENT_ROOT"];

/**
 * 
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Object.php');

/**
 * 
 */
class QuickBooks_Object_Customer implements QuickBooks_Object
{
	public function __construct($name)
	{
		
	}
	
	public function toXML()
	{
		
	}
}

/*
<Name >STRTYPE</Name> <!-- required -->
<IsActive >BOOLTYPE</IsActive> <!-- optional -->
<ParentRef> <!-- optional --> 
<ListID >IDTYPE</ListID> <!-- optional -->
<FullName >STRTYPE</FullName> <!-- optional -->
</ParentRef>
<CompanyName >STRTYPE</CompanyName> <!-- optional -->
<Salutation >STRTYPE</Salutation> <!-- optional -->
<FirstName >STRTYPE</FirstName> <!-- optional -->
<MiddleName >STRTYPE</MiddleName> <!-- optional -->
<LastName >STRTYPE</LastName> <!-- optional -->
<BillAddress> <!-- optional --> 
<Addr1 >STRTYPE</Addr1> <!-- optional -->
<Addr2 >STRTYPE</Addr2> <!-- optional -->
<Addr3 >STRTYPE</Addr3> <!-- optional -->
<Addr4 >STRTYPE</Addr4> <!-- optional -->
<Addr5 >STRTYPE</Addr5> <!-- optional -->
<City >STRTYPE</City> <!-- optional -->
<State >STRTYPE</State> <!-- optional -->
<PostalCode >STRTYPE</PostalCode> <!-- optional -->
<Country >STRTYPE</Country> <!-- optional -->
<Note >STRTYPE</Note> <!-- optional -->
</BillAddress>
<ShipAddress> <!-- optional --> 
<Addr1 >STRTYPE</Addr1> <!-- optional -->
<Addr2 >STRTYPE</Addr2> <!-- optional -->
<Addr3 >STRTYPE</Addr3> <!-- optional -->
<Addr4 >STRTYPE</Addr4> <!-- optional -->
<Addr5 >STRTYPE</Addr5> <!-- optional -->
<City >STRTYPE</City> <!-- optional -->
<State >STRTYPE</State> <!-- optional -->
<PostalCode >STRTYPE</PostalCode> <!-- optional -->
<Country >STRTYPE</Country> <!-- optional -->
<Note >STRTYPE</Note> <!-- optional -->
</ShipAddress>
<Phone >STRTYPE</Phone> <!-- optional -->
<AltPhone >STRTYPE</AltPhone> <!-- optional -->
<Fax >STRTYPE</Fax> <!-- optional -->
<Email >STRTYPE</Email> <!-- optional -->
<Contact >STRTYPE</Contact> <!-- optional -->
<AltContact >STRTYPE</AltContact> <!-- optional -->
<CustomerTypeRef> <!-- optional --> 
<ListID >IDTYPE</ListID> <!-- optional -->
<FullName >STRTYPE</FullName> <!-- optional -->
</CustomerTypeRef>
<TermsRef> <!-- optional --> 
<ListID >IDTYPE</ListID> <!-- optional -->
<FullName >STRTYPE</FullName> <!-- optional -->
</TermsRef>
<SalesRepRef> <!-- optional --> 
<ListID >IDTYPE</ListID> <!-- optional -->
<FullName >STRTYPE</FullName> <!-- optional -->
</SalesRepRef>
<OpenBalance >AMTTYPE</OpenBalance> <!-- optional -->
<OpenBalanceDate >DATETYPE</OpenBalanceDate> <!-- optional -->
<SalesTaxCodeRef> <!-- optional --> 
<ListID >IDTYPE</ListID> <!-- optional -->
<FullName >STRTYPE</FullName> <!-- optional -->
</SalesTaxCodeRef>
<ItemSalesTaxRef> <!-- optional --> 
<ListID >IDTYPE</ListID> <!-- optional -->
<FullName >STRTYPE</FullName> <!-- optional -->
</ItemSalesTaxRef>
<ResaleNumber >STRTYPE</ResaleNumber> <!-- optional -->
<AccountNumber >STRTYPE</AccountNumber> <!-- optional -->
<CreditLimit >AMTTYPE</CreditLimit> <!-- optional -->
<PreferredPaymentMethodRef> <!-- optional --> 
<ListID >IDTYPE</ListID> <!-- optional -->
<FullName >STRTYPE</FullName> <!-- optional -->
</PreferredPaymentMethodRef>
<CreditCardInfo> <!-- optional --> 
<CreditCardNumber >STRTYPE</CreditCardNumber> <!-- optional -->
<ExpirationMonth >INTTYPE</ExpirationMonth> <!-- optional -->
<ExpirationYear >INTTYPE</ExpirationYear> <!-- optional -->
<NameOnCard >STRTYPE</NameOnCard> <!-- optional -->
<CreditCardAddress >STRTYPE</CreditCardAddress> <!-- optional -->
<CreditCardPostalCode >STRTYPE</CreditCardPostalCode> <!-- optional -->
</CreditCardInfo>
<!-- JobStatus may have one of the following values: Awarded, Closed, InProgress, None [DEFAULT], NotAwarded, Pending -->
<JobStatus >ENUMTYPE</JobStatus> <!-- optional -->
<JobStartDate >DATETYPE</JobStartDate> <!-- optional -->
<JobProjectedEndDate >DATETYPE</JobProjectedEndDate> <!-- optional -->
<JobEndDate >DATETYPE</JobEndDate> <!-- optional -->
<JobDesc >STRTYPE</JobDesc> <!-- optional -->
<JobTypeRef> <!-- optional --> 
<ListID >IDTYPE</ListID> <!-- optional -->
<FullName >STRTYPE</FullName> <!-- optional -->
</JobTypeRef>
<Notes >STRTYPE</Notes> <!-- optional -->
<PriceLevelRef> <!-- optional --> 
<ListID >IDTYPE</ListID> <!-- optional -->
<FullName >STRTYPE</FullName> <!-- optional -->
</PriceLevelRef>
</CustomerAdd>
<IncludeRetElement >STRTYPE</IncludeRetElement> <!-- optional, may repeat -->
</CustomerAddRq>



<CustomerAddRs statusCode="INTTYPE" statusSeverity="STRTYPE" statusMessage="STRTYPE"> 
<CustomerRet> <!-- optional --> 
<ListID >IDTYPE</ListID> <!-- required -->
<TimeCreated >DATETIMETYPE</TimeCreated> <!-- required -->
<TimeModified >DATETIMETYPE</TimeModified> <!-- required -->
<EditSequence >STRTYPE</EditSequence> <!-- required -->
<Name >STRTYPE</Name> <!-- required -->
<FullName >STRTYPE</FullName> <!-- required -->
<IsActive >BOOLTYPE</IsActive> <!-- optional -->
<ParentRef> <!-- optional --> 
<ListID >IDTYPE</ListID> <!-- optional -->
<FullName >STRTYPE</FullName> <!-- optional -->
</ParentRef>
<Sublevel >INTTYPE</Sublevel> <!-- required -->
<CompanyName >STRTYPE</CompanyName> <!-- optional -->
<Salutation >STRTYPE</Salutation> <!-- optional -->
<FirstName >STRTYPE</FirstName> <!-- optional -->
<MiddleName >STRTYPE</MiddleName> <!-- optional -->
<LastName >STRTYPE</LastName> <!-- optional -->
<BillAddress> <!-- optional --> 
<Addr1 >STRTYPE</Addr1> <!-- optional -->
<Addr2 >STRTYPE</Addr2> <!-- optional -->
<Addr3 >STRTYPE</Addr3> <!-- optional -->
<Addr4 >STRTYPE</Addr4> <!-- optional -->
<Addr5 >STRTYPE</Addr5> <!-- optional -->
<City >STRTYPE</City> <!-- optional -->
<State >STRTYPE</State> <!-- optional -->
<PostalCode >STRTYPE</PostalCode> <!-- optional -->
<Country >STRTYPE</Country> <!-- optional -->
<Note >STRTYPE</Note> <!-- optional -->
</BillAddress>
<BillAddressBlock> <!-- optional --> 
<Addr1 >STRTYPE</Addr1> <!-- optional -->
<Addr2 >STRTYPE</Addr2> <!-- optional -->
<Addr3 >STRTYPE</Addr3> <!-- optional -->
<Addr4 >STRTYPE</Addr4> <!-- optional -->
<Addr5 >STRTYPE</Addr5> <!-- optional -->
</BillAddressBlock>
<ShipAddress> <!-- optional --> 
<Addr1 >STRTYPE</Addr1> <!-- optional -->
<Addr2 >STRTYPE</Addr2> <!-- optional -->
<Addr3 >STRTYPE</Addr3> <!-- optional -->
<Addr4 >STRTYPE</Addr4> <!-- optional -->
<Addr5 >STRTYPE</Addr5> <!-- optional -->
<City >STRTYPE</City> <!-- optional -->
<State >STRTYPE</State> <!-- optional -->
<PostalCode >STRTYPE</PostalCode> <!-- optional -->
<Country >STRTYPE</Country> <!-- optional -->
<Note >STRTYPE</Note> <!-- optional -->
</ShipAddress>
<ShipAddressBlock> <!-- optional --> 
<Addr1 >STRTYPE</Addr1> <!-- optional -->
<Addr2 >STRTYPE</Addr2> <!-- optional -->
<Addr3 >STRTYPE</Addr3> <!-- optional -->
<Addr4 >STRTYPE</Addr4> <!-- optional -->
<Addr5 >STRTYPE</Addr5> <!-- optional -->
</ShipAddressBlock>
<Phone >STRTYPE</Phone> <!-- optional -->
<AltPhone >STRTYPE</AltPhone> <!-- optional -->
<Fax >STRTYPE</Fax> <!-- optional -->
<Email >STRTYPE</Email> <!-- optional -->
<Contact >STRTYPE</Contact> <!-- optional -->
<AltContact >STRTYPE</AltContact> <!-- optional -->
<CustomerTypeRef> <!-- optional --> 
<ListID >IDTYPE</ListID> <!-- optional -->
<FullName >STRTYPE</FullName> <!-- optional -->
</CustomerTypeRef>
<TermsRef> <!-- optional --> 
<ListID >IDTYPE</ListID> <!-- optional -->
<FullName >STRTYPE</FullName> <!-- optional -->
</TermsRef>
<SalesRepRef> <!-- optional --> 
<ListID >IDTYPE</ListID> <!-- optional -->
<FullName >STRTYPE</FullName> <!-- optional -->
</SalesRepRef>
<Balance >AMTTYPE</Balance> <!-- optional -->
<TotalBalance >AMTTYPE</TotalBalance> <!-- optional -->
<SalesTaxCodeRef> <!-- optional --> 
<ListID >IDTYPE</ListID> <!-- optional -->
<FullName >STRTYPE</FullName> <!-- optional -->
</SalesTaxCodeRef>
<ItemSalesTaxRef> <!-- optional --> 
<ListID >IDTYPE</ListID> <!-- optional -->
<FullName >STRTYPE</FullName> <!-- optional -->
</ItemSalesTaxRef>
<ResaleNumber >STRTYPE</ResaleNumber> <!-- optional -->
<AccountNumber >STRTYPE</AccountNumber> <!-- optional -->
<CreditLimit >AMTTYPE</CreditLimit> <!-- optional -->
<PreferredPaymentMethodRef> <!-- optional --> 
<ListID >IDTYPE</ListID> <!-- optional -->
<FullName >STRTYPE</FullName> <!-- optional -->
</PreferredPaymentMethodRef>
<CreditCardInfo> <!-- optional --> 
<CreditCardNumber >STRTYPE</CreditCardNumber> <!-- optional -->
<ExpirationMonth >INTTYPE</ExpirationMonth> <!-- optional -->
<ExpirationYear >INTTYPE</ExpirationYear> <!-- optional -->
<NameOnCard >STRTYPE</NameOnCard> <!-- optional -->
<CreditCardAddress >STRTYPE</CreditCardAddress> <!-- optional -->
<CreditCardPostalCode >STRTYPE</CreditCardPostalCode> <!-- optional -->
</CreditCardInfo>
<!-- JobStatus may have one of the following values: Awarded, Closed, InProgress, None [DEFAULT], NotAwarded, Pending -->
<JobStatus >ENUMTYPE</JobStatus> <!-- optional -->
<JobStartDate >DATETYPE</JobStartDate> <!-- optional -->
<JobProjectedEndDate >DATETYPE</JobProjectedEndDate> <!-- optional -->
<JobEndDate >DATETYPE</JobEndDate> <!-- optional -->
<JobDesc >STRTYPE</JobDesc> <!-- optional -->
<JobTypeRef> <!-- optional --> 
<ListID >IDTYPE</ListID> <!-- optional -->
<FullName >STRTYPE</FullName> <!-- optional -->
</JobTypeRef>
<Notes >STRTYPE</Notes> <!-- optional -->
<PriceLevelRef> <!-- optional --> 
<ListID >IDTYPE</ListID> <!-- optional -->
<FullName >STRTYPE</FullName> <!-- optional -->
</PriceLevelRef>
<DataExtRet> <!-- optional, may repeat --> 
<OwnerID >GUIDTYPE</OwnerID> <!-- optional -->
<DataExtName >STRTYPE</DataExtName> <!-- required -->
<!-- DataExtType may have one of the following values: AMTTYPE, DATETIMETYPE, INTTYPE, PERCENTTYPE, PRICETYPE, QUANTYPE, STR1024TYPE, STR255TYPE -->
<DataExtType >ENUMTYPE</DataExtType> <!-- required -->
<DataExtValue >STRTYPE</DataExtValue> <!-- required -->
</DataExtRet>
</CustomerRet>
*/

?>