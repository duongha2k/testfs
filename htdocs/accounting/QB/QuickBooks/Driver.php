<?php

/**
 * QuickBooks driver base class
 * 
 * The Driver classes act as back-end to the Queue class and SOAP server. 
 * Driver classes should extend this base class and implement all abstract 
 * methods.  
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 * 
 * @package QuickBooks
 */

/**
 * QuickBooks driver base class
 */
abstract class QuickBooks_Driver
{
	/**
	 * Constructor
	 * 
	 * @param string $dsn		A DSN-style connection string
	 * @param array $config		An array of configuration information
	 */
	abstract public function __construct($dsn, $config);
	
	/**
	 * Place an action into the queue, along with a unique identifier (if neccessary)
	 * 
	 * Example: 
	 * <code>
	 * 	$driver->queueEnqueue('CustomerAdd', 1234); // Push customer #1234 over to QuickBooks
	 * </code>
	 * 
	 * @param string $action	The QuickBooks action to do
	 * @param mixed $ident		A unique identifier 
	 * @return boolean
	 */
	abstract public function queueEnqueue($action, $ident, $replace = true, $priority = 0, $extra = null);
	
	/**
	 * Remove an item from the queue
	 * 
	 * @param boolean $by_priority	If TRUE, remove the item with the highest priority next
	 * @return boolean
	 */
	abstract public function queueDequeue($by_priority = false);
	
	/**
	 * Update the status of a particular item in the queue
	 * 
	 * @param string $ticket		The ticket of the process which is updating the status
	 * @param string $action		The action
	 * @param mixed $ident			The ident string
	 * @param char $new_status		The new status code (QUICKBOOKS_STATUS_SUCCESS, QUICKBOOKS_STATUS_ERROR, etc.)
	 * @param string $msg			An error message (if an error message occured)
	 * @return boolean
	 */
	abstract public function queueStatus($ticket, $action, $ident, $new_status, $msg = '');
	
	/**
	 * Tell the number of items left in the queue
	 * 
	 * @return integer
	 */
	abstract public function queueSize();
	
	/**
	 * Fetch a specific item from the queue
	 * 
	 * @param string $action
	 * @param mixed $ident
	 * @param char $status
	 * @return array 
	 */
	abstract public function queueFetch($action, $ident, $status = QUICKBOOKS_STATUS_QUEUED);
	
	/**
	 * Tell how many commands have been processed during this login session
	 * 
	 * @param string $ticket		The ticket for the login session
	 * @return integer				The number of commands processed so far
	 */
	abstract public function queueProcessed($ticket);
	
	/**
	 * Tell whether or not an item exists in the queue
	 * 
	 * @param string $action
	 * @param mixed $ident
	 * @return boolean
	 */
	abstract public function queueExists($action, $ident);
	
	/**
	 * Log an error that occured for a specific ticket
	 * 
	 * @param string $ticket
	 * @param string $errno
	 * @param string $errstr
	 * @return boolean
	 */
	abstract public function errorLog($ticket, $errno, $errstr);
	
	/**
	 * Get the last error that occured
	 * 
	 * @param string $ticket
	 * @return string
	 */
	abstract public function errorLast($ticket);
	
	/**
	 * Establish a session for a user (log that user in)
	 * 
	 * The QuickBooks Web Connector will pass a username and password to the 
	 * SOAP server. There is a SOAP ->authenticate() method which logs the user 
	 * in. 
	 * 
	 * @param string $username		The username for the QuickBooks Web Connector user
	 * @param string $password		The password for the QuickBooks Web Connector user
	 * @param boolean $override		If set to TRUE, a correct password will not be required
	 * @return string				The ticket for the login session
	 */
	abstract public function authLogin($username, $password, $override = false);
	
	/**
	 * Check to see whether or not a ticket is for a valid, unexpired login session
	 * 
	 * @param string $ticket	The login session ticket to check
	 * @return boolean 			Whether or not the ticket is valid
	 */
	abstract public function authCheck($ticket);
	
	/**
	 * End a log-in session
	 * 
	 * @param string $ticket	The ticket for the session
	 * @return boolean
	 */
	abstract public function authLogout($ticket);
	
	/**
	 * Create a user account with the given username and password
	 * 
	 * @param string $username	The desired username
	 * @param string $password	The desired password
	 * @return boolean 			Whether or not the user was created
	 */
	abstract public function authCreate($username, $password);
	
	/**
	 * Log a message to the QuickBooks log
	 * 
	 * @param string $msg		The message to place in the log
	 * @param string $ticket	The ticket for the login session
	 * @param integer $lvl		
	 * @return boolean 			Whether or not the message was logged successfully
	 */
	abstract public function log($msg, $ticket, $lvl = QUICKBOOKS_LOG_NORMAL);
	
	/**
	 * One-way hash a password for storage in the database
	 * 
	 * @param string $password
	 * @return string
	 */	
	protected function _hash($password)
	{
		return sha1($password . QUICKBOOKS_SALT);
	}
}

?>