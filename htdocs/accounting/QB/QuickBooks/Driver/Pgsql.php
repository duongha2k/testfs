<?php

/**
 * PostgreSQL backend for the QuickBooks SOAP server
 * 
 * You need to use some sort of backend to facilitate communication between the 
 * SOAP server and your application. The SOAP server stores queue requests 
 * using the backend. 
 * 
 * This backend driver is for a PostgreSQL database. You can use the 
 * {@see QuickBooks_Utilities} class to initalize the four tables in the 
 * PostgreSQL database. 
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 * 
 * @package QuickBooks
 * @subpackage Driver
 */

$doc_root = $_SERVER["DOCUMENT_ROOT"];

/**
 * Salt used when hashing ticket values
 * 
 * @deprecated
 */
define('QUICKBOOKS_DRIVER_PGSQL_SALT', '@ndP3pp@');

if (!defined('QUICKBOOKS_DRIVER_PGSQL_QUEUETABLE'))
{
	/**
	 * MySQL table name to store queued requests in
	 * 
	 * @var string
	 */
	define('QUICKBOOKS_DRIVER_PGSQL_QUEUETABLE', 'quickbooks_queue');
}

if (!defined('QUICKBOOKS_DRIVER_PGSQL_USERTABLE'))
{
	/**
	 * MySQL table name to store usernames/passwords for the QuickBooks SOAP server
	 * 
	 * @var string
	 */
	define('QUICKBOOKS_DRIVER_PGSQL_USERTABLE', 'quickbooks_user');
}

if (!defined('QUICKBOOKS_DRIVER_PGSQL_TICKETTABLE'))
{
	/**
	 * The table name to store session tickets in
	 * 
	 * @var string
	 */
	define('QUICKBOOKS_DRIVER_PGSQL_TICKETTABLE', 'quickbooks_ticket');
}

if (!defined('QUICKBOOKS_DRIVER_PGSQL_LOGTABLE'))
{
	/**
	 * The table name to store log data in
	 * 
	 * @var string
	 */
	define('QUICKBOOKS_DRIVER_PGSQL_LOGTABLE', 'quickbooks_log');
}

/**
 * QuickBooks driver base class
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Driver.php');

/**
 * QuickBooks utilities class
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Utilities.php');

/**
 * QuickBooks PGSQL back-end driver
 */
class QuickBooks_Driver_Pgsql extends QuickBooks_Driver
{
	/**
	 * PGSQL connection resource
	 * 
	 * @var resource
	 */
	protected $_conn;
	
	/**
	 * Log level (debug, verbose, normal)
	 * 
	 * @var integer
	 */
	protected $_log_level;
	
	/**
	 * Create a new PostgreSQL back-end driver
	 * 
	 * @param string $dsn		A DSN-style connection string (i.e.: "pgsql://your-postgres-username:your-postgres-password@your-postgres-host:port/your-postgres-database")
	 * @param array $config		Configuration options for the driver (not currently supported)
	 */
	public function __construct($dsn_or_conn, $config)
	{
		if (is_resource($dsn_or_conn))
		{
			$this->_conn = $dsn_or_conn;
		}
		else
		{
			$defaults = array(
				'scheme' => 'pgsql', 
				'host' => 'localhost', 
				'port' => 5432, 
				'user' => 'pgsql', 
				'pass' => '', 
				'path' => '/quickbooks',
				);
			
			$parse = QuickBooks_Utilities::parseDSN($dsn_or_conn, $defaults);
			
			$config = $this->_defaults($config);
			$this->_log_level = (int) $config['log_level'];
			
			$this->_connect($parse['host'], $parse['port'], $parse['user'], $parse['pass'], substr($parse['path'], 1), $config['connect_type']);
		}
	}
	
	/**
	 * Merge an array of configuration options with the defaults
	 * 
	 * @param array $config
	 * @return array 
	 */
	protected function _defaults($config)
	{
		$defaults = array(
			'log_level' => QUICKBOOKS_LOG_NORMAL,
			'connect_type' => PGSQL_CONNECT_FORCE_NEW, 
			);
		
		return array_merge($defaults, $config);
	}
	
	/**
	 * Store the last error which occured 
	 * 
	 * @param string $ticket
	 * @param string $errnum
	 * @param string $errmsg
	 * @return boolean
	 */
	public function errorLog($ticket, $errnum, $errmsg)
	{
		$pg_errnum = 0;
		$pg_errmsg = '';
		
		if ($ticket_id = $this->_ticketResolve($ticket))
		{
			return $this->_query("
				UPDATE 
					" . QUICKBOOKS_DRIVER_PGSQL_TICKETTABLE . "
				 SET 
					lasterror_num = '" . $this->_escape($errnum) . "', 
					lasterror_msg = '" . $this->_escape($errmsg) . "' 
				WHERE 
					quickbooks_ticket_id = " . $ticket_id, $pg_errnum, $pg_errmsg);
		}
		
		return false;
	}
	
	/**
	 * Retreive the last error message which occured for a given ticket (session)
	 * 
	 * @param string $ticket
	 * @return string
	 */
	public function errorLast($ticket)
	{
		$errnum = 0;
		$errmsg = '';
		
		if ($ticket_id = $this->_ticketResolve($ticket))
		{
			if ($arr = $this->_fetch($this->_query("SELECT lasterror_msg FROM " . QUICKBOOKS_DRIVER_PGSQL_TICKETTABLE . " WHERE quickbooks_ticket_id = '" . $ticket_id . "' LIMIT 1", $errnum, $errmsg)))
			{
				return $arr['lasterror_msg'];
			}
		}
		
		return 'Error fetching last error.';
	}
	
	/**
	 * Add an item to the queue
	 * 
	 * @param string $action
	 * @param mixed $ident
	 * @return boolean 
	 */
	public function queueEnqueue($action, $ident, $replace = true, $priority = 0, $extra = null)
	{
		$errnum = 0;
		$errmsg = '';
		
		if ($replace)
		{
			$this->_query("
				DELETE FROM 
					" . QUICKBOOKS_DRIVER_PGSQL_QUEUETABLE . " 
				WHERE 
					qb_action = '" . $this->_escape($action) . "' AND 
					ident = '" . $this->_escape($ident) . "' ", $errnum, $errmsg);
		}
		
		if ($extra)
		{
			$extra = serialize($extra);
		}
		
		return $this->_query("
			INSERT INTO 
				" . QUICKBOOKS_DRIVER_PGSQL_QUEUETABLE . "
			(
				qb_action, 
				ident, 
				extra,				
				priority, 
				qb_status, 
				enqueue_datetime
			) VALUES (
				'" . $this->_escape($action) . "', 
				'" . $this->_escape($ident) . "',
				'" . $this->_escape($extra) . "',
				" . (int) $priority . ",  
				'" . QUICKBOOKS_STATUS_QUEUED . "', 
				NOW() 
			) ", $errnum, $errmsg);
	}
	
	/**
	 * Fetch a particular item from the queue
	 * 
	 * @param integer $queue_id
	 * @return array 
	 */
	public function queueFetch($action, $ident, $status = QUICKBOOKS_STATUS_QUEUED)
	{
		$errnum = 0;
		$errmsg = '';
		
		$sql = "
			SELECT 
				* 
			FROM
				" . QUICKBOOKS_DRIVER_PGSQL_QUEUETABLE . "
			WHERE
				qb_action = '" . $this->_escape($action) . "' AND 
				ident = '" . $this->_escape($ident) . "' AND 
				qb_status = '" . $this->_escape($status) . "'  
			LIMIT 1 ";
		
		return $this->_fetch($this->_query($sql, $errnum, $errmsg));
	}
	
	/**
	 * Remove an item from the queue
	 * 
	 * @param boolean $by_priority
	 * @return array 
	 */
	public function queueDequeue($by_priority = false)
	{
		$errnum = 0;
		$errmsg = '';
		
		$sql = "
			SELECT 
				* 
			FROM
				" . QUICKBOOKS_DRIVER_PGSQL_QUEUETABLE . " 
			WHERE	
				qb_status = '" . QUICKBOOKS_STATUS_QUEUED . "' ";
		
		if ($by_priority)
		{
			$sql .= ' ORDER BY priority DESC ';
		}
		
		return $this->_fetch($this->_query($sql . ' LIMIT 1', $errnum, $errmsg));
	}
	
	/**
	 * Tell how many items are in the queue
	 * 
	 * @return integer
	 */
	public function queueSize()
	{
		$errnum = 0;
		$errmsg = '';
		
		$arr = $this->_fetch($this->_query("
			SELECT 
				COUNT(*) AS queuesize 
			FROM 
				" . QUICKBOOKS_DRIVER_PGSQL_QUEUETABLE . " 
			WHERE 
				qb_status = '" . QUICKBOOKS_STATUS_QUEUED . "' ", $errnum, $errmsg));
		
		return $arr['queuesize'];
	}
	
	/**
	 * Resolve a ticket string back to a ticket ID number
	 * 
	 * @param string $ticket
	 * @return integer
	 */
	protected function _ticketResolve($ticket)
	{
		$errnum = 0;
		$errmsg = '';
		
		if ($arr = $this->_fetch($this->_query("SELECT * FROM " . QUICKBOOKS_DRIVER_PGSQL_TICKETTABLE . " WHERE ticket = '" . $this->_escape($ticket) . "' ", $errnum, $errmsg)))
		{
			return $arr['quickbooks_ticket_id'];
		}
		
		return 0;
	}
	
	/**
	 * Update the status of an item in the queue
	 * 
	 * @param string $ticket
	 * @param string $action
	 * @param mixed $ident
	 * @param char $status
	 * @param string $msg
	 * @return boolean 
	 */
	public function queueStatus($ticket, $action, $ident, $new_status, $msg = null)
	{
		$errnum = 0;
		$errmsg = '';
		
		if ($ticket_id = $this->_ticketResolve($ticket))
		{
			$sql = "
				UPDATE
					" . QUICKBOOKS_DRIVER_PGSQL_QUEUETABLE . " 
				SET 
					qb_status = '" . $this->_escape($new_status) . "', 
					msg = '" . $this->_escape($msg) . "' 
				WHERE
					qb_action = '" . $this->_escape($action) . "' AND 
					ident = '" . $this->_escape($ident) . "' ";
			
			$this->_query($sql, $errnum, $errmsg);
			
			if ($new_status == QUICKBOOKS_STATUS_SUCCESS)
			{
				$this->_query("UPDATE " . QUICKBOOKS_DRIVER_PGSQL_TICKETTABLE . " SET processed = processed + 1 WHERE quickbooks_ticket_id = " . (int) $ticket_id . " ", $errnum, $errmsg);
			}
			else if ($new_status == QUICKBOOKS_STATUS_PROCESSING)
			{
				$this->_query("UPDATE " . QUICKBOOKS_DRIVER_PGSQL_QUEUETABLE . " SET quickbooks_ticket_id = " . (int) $ticket_id . ", dequeue_datetime = NOW() WHERE qb_action = '" . $this->_escape($action) . "' AND ident = '" . $this->_escape($ident) . "' ", $errnum, $errmsg);
			}
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * Tell how many items have been processed during this session
	 * 
	 * @param string $ticket
	 * @return integer
	 */
	public function queueProcessed($ticket)
	{
		$errnum = 0;
		$errmsg = '';
		
		if ($arr = $this->_fetch($this->_query("SELECT processed FROM " . QUICKBOOKS_DRIVER_PGSQL_TICKETTABLE . " WHERE ticket = '" . $this->_escape($ticket) . "' ", $errnum, $errmsg)))
		{
			return $arr['processed'];
		}
		
		return 0;
	}
	
	/**
	 * Tell whether or not an item exists in the queue
	 * 
	 * @param string $action
	 * @param mixed $ident
	 * @return boolean
	 */
	public function queueExists($action, $ident)
	{
		$errnum = 0;
		$errmsg = '';
		
		return $this->_count($this->_query("
			SELECT 
				quickbooks_queue_id
			FROM
				" . QUICKBOOKS_DRIVER_PGSQL_QUEUETABLE . " 
			WHERE
				qb_action = '" . $this->escape($action) . "' AND 
				ident = '" . $this->escape($ident) . "' AND 
				qb_status = '" . QUICKBOOKS_STATUS_QUEUED . "' ", $errnum, $errmsg)) > 0;
	}
	
	/**
	 * Create a new user for the SOAP server
	 * 
	 * @param string $username
	 * @param string $password
	 * @return boolean 
	 */
	public function authCreate($username, $password)
	{
		$errnum = 0;
		$errmsg = '';
		
		if (!$this->_count($this->_query("SELECT qb_username FROM " . QUICKBOOKS_DRIVER_PGSQL_USERTABLE . " WHERE qb_username = '" . $this->_escape($username) . "' LIMIT 1", $errnum, $errmsg)))
		{
			return $this->_query("
				INSERT INTO 
					" . QUICKBOOKS_DRIVER_PGSQL_USERTABLE . " 
				( 
					qb_username, 
					qb_password, 
					write_datetime, 
					touch_datetime
				) VALUES (
					'" . $this->_escape($username) . "', 
					'" . $this->_escape($this->_hash($password)) . "', 
					NOW(), 
					NOW() 
				) ", $errnum, $errmsg);
		}
		
		return false;
	}
	
	/**
	 * Log a user in
	 * 
	 * @param string $username		
	 * @param string $password		
	 * @param boolean $override		If this is set to TRUE, a correct password *is not* required
	 * @return string				A session ticket, or an empty string if the login failed
	 */
	public function authLogin($username, $password, $override = false)
	{
		if ($override) // We still need to make sure that the user exists, even if using external authentication
		{
			$this->authCreate($username, $password);
		}
		
		$errnum = 0;
		$errmsg = '';
		
		if ($override or 
			$arr = $this->_fetch($this->_query("
				SELECT 
					* 
				FROM 
					" . QUICKBOOKS_DRIVER_PGSQL_USERTABLE . " 
				WHERE 
					qb_username = '" . $this->_escape($username) . "' AND 
					qb_password = '" . $this->_escape($this->_hash($password)) . "' 
				LIMIT 
					1", $errnum, $errmsg)))
		{
			$ticket = md5((string) microtime() . $username . QUICKBOOKS_DRIVER_PGSQL_SALT);
			
			$this->_query("
				INSERT INTO 
					" . QUICKBOOKS_DRIVER_PGSQL_TICKETTABLE . "
				( 
					qb_username, 
					ticket, 
					ipaddr, 
					write_datetime, 
					touch_datetime
				) VALUES ( 
					'" . $this->_escape($username) . "', 
					'" . $this->_escape($ticket) . "',
					'" . $_SERVER['REMOTE_ADDR'] . "',  
					NOW(), 
					NOW()
				) ", $errnum, $errmsg);
			
			$this->_query("
				UPDATE 
					" . QUICKBOOKS_DRIVER_PGSQL_USERTABLE . " 
				SET 
					touch_datetime = NOW()
				WHERE 
					qb_username = '" . $this->_escape($username) . "' ", $errnum, $errmsg);
			
			return $ticket;
		}
		
		return null;
	}
	
	/**
	 * Check to see if a log in session is valid
	 * 
	 * @param string $ticket
	 * @return boolean
	 */
	public function authCheck($ticket)
	{
		$errnum = 0;
		$errmsg = '';
		
		if ($arr = $this->_fetch($this->_query("SELECT quickbooks_ticket_id FROM " . QUICKBOOKS_DRIVER_PGSQL_TICKETTABLE . " WHERE ticket = '" . $this->_escape($ticket) . "' AND touch_datetime > '" . date('Y-m-d H:i:s', time() - QUICKBOOKS_TIMEOUT) . "' ", $errnum, $errmsg)))
		{
			$this->_query("UPDATE " . QUICKBOOKS_DRIVER_PGSQL_TICKETTABLE . " SET touch_datetime = NOW() WHERE quickbooks_ticket_id = " . $arr['quickbooks_ticket_id'], $errnum, $errmsg);
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * Log a user out
	 * 
	 * @param string $ticket
	 * @return boolean
	 */
	public function authLogout($ticket)
	{
		return true;
	}
	
	/**
	 * Write a message to the log file
	 * 
	 * @param string $msg
	 * @param string $ticket
	 * @param integer $log_level
	 * @return boolean
	 */
	public function log($msg, $ticket, $log_level = null)
	{
		$errnum = 0;
		$errmsg = '';
		
		if (is_null($log_level) or $log_level >= $this->_log_level)
		{
			if ($ticket_id = $this->_ticketResolve($ticket))
			{
				return $this->_query("INSERT INTO " . QUICKBOOKS_DRIVER_PGSQL_LOGTABLE . " ( quickbooks_ticket_id, msg, log_datetime ) VALUES ( " . $ticket_id . ", '" . $this->_escape($msg) . "', NOW() ) ", $errnum, $errmsg);
			}
			else
			{
				return $this->_query("INSERT INTO " . QUICKBOOKS_DRIVER_PGSQL_LOGTABLE . " ( msg, log_datetime ) VALUES ( '" . $this->_escape($msg) . "', NOW() ) ", $errnum, $errmsg);
			}
		}
		
		return true;
	}
	
	/**
	 * Initialize the PGSQL driver back-end (create the PGSQL table schema)
	 * 
	 * @return boolean
	 */
	public function initialize()
	{
		return false;
	}
	
	/**
	 * Connect to the database
	 * 
	 * @param string $host				The hostname the database is located at
	 * @param integer $port				The port the database is at
	 * @param string $user				Username for connecting
	 * @param string $pass				Password for connecting
	 * @param string $db				The database name
	 * @param boolean $new_link			TRUE for establishing a new link to the database, FALSE to re-use an existing one
	 * @param integer $client_flags		Database connection flags (see the PHP/PGSQL documentation)
	 * @return boolean
	 */
	protected function _connect($host, $port, $user, $pass, $db, $connect_type)
	{
		$str = '';
		
		if ($host)
		{
			$str .= " host=" . $host;
		}
		
		if ((int) $port)
		{
			$str .= " port=" . (int) $port;
		}
		
		if ($user)
		{
			$str .= " user='" . $user . "' ";
		}
		
		if ($pass)
		{
			$str .= " pass='" . $pass . "' ";
		}
		
		$str .= " dbname='" . $db . "' ";
		
		$this->_conn = pg_connect($str, $connect_type);
		
		return true;
	}
	
	/**
	 * Fetch an array from a database result set
	 * 
	 * @param resource $res
	 * @return array
	 */
	protected function _fetch($res)
	{
		return pg_fetch_assoc($res);
	}
	
	/**
	 * Query the database
	 * 
	 * @param string $sql
	 * @return resource
	 */
	protected function _query($sql, &$errnum, &$errmsg)
	{
		$res = pg_query($this->_conn, $sql);
		
		if (!$res)
		{
			$errnum = -1;
			$errmsg = pg_last_error($this->_conn);
			
			print('Error Num.: ' . $errnum . "\n" . 'Error Msg.:' . $errmsg . "\n");
		}
		
		return $res;
	}
	
	/**
	 * Escape a string for the database
	 * 
	 * @param string $str
	 * @return string
	 */
	protected function _escape($str)
	{
		return pg_escape_string($this->_conn, $str);
	}
	
	/**
	 * Count the number of rows returned from the database
	 * 
	 * @param resource $res
	 * @return integer
	 */
	protected function _count($res)
	{
		return pg_num_rows($res);
	}
}

?>