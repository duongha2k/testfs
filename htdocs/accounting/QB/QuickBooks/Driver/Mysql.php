<?php

/**
 * MySQL backend for the QuickBooks SOAP server
 * 
 * You need to use some sort of backend to facilitate communication between the 
 * SOAP server and your application. The SOAP server stores queue requests 
 * using the backend. 
 * 
 * This backend driver is for a MySQL database. You can use the 
 * {@see QuickBooks_Utilities} class to initalize the four tables in the MySQL 
 * database. 
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 * 
 * @package QuickBooks
 * @subpackage Driver
 */

$doc_root = $_SERVER["DOCUMENT_ROOT"];

require_once($doc_root . '/accounting/QB/mail2.php');

/**
 * Salt used when hashing ticket values
 * 
 * @deprecated
 */
define('QUICKBOOKS_DRIVER_MYSQL_SALT', '@ndP3pp@');

if (!defined('QUICKBOOKS_DRIVER_MYSQL_QUEUETABLE'))
{
	/**
	 * MySQL table name to store queued requests in
	 * 
	 * @var string
	 */
	define('QUICKBOOKS_DRIVER_MYSQL_QUEUETABLE', 'quickbooks_queue');
}

if (!defined('QUICKBOOKS_DRIVER_MYSQL_USERTABLE'))
{
	/**
	 * MySQL table name to store usernames/passwords for the QuickBooks SOAP server
	 * 
	 * @var string
	 */
	define('QUICKBOOKS_DRIVER_MYSQL_USERTABLE', 'quickbooks_user');
}

if (!defined('QUICKBOOKS_DRIVER_MYSQL_TICKETTABLE'))
{
	/**
	 * The table name to store session tickets in
	 * 
	 * @var string
	 */
	define('QUICKBOOKS_DRIVER_MYSQL_TICKETTABLE', 'quickbooks_ticket');
}

if (!defined('QUICKBOOKS_DRIVER_MYSQL_LOGTABLE'))
{
	/**
	 * The table name to store log data in
	 * 
	 * @var string
	 */
	define('QUICKBOOKS_DRIVER_MYSQL_LOGTABLE', 'quickbooks_log');
}

/**
 * QuickBooks driver base class
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Driver.php');

/**
 * QuickBooks utilities class
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Utilities.php');

/**
 * QuickBooks MySQL back-end driver
 */
class QuickBooks_Driver_Mysql extends QuickBooks_Driver
{
	/**
	 * MySQL connection resource
	 * 
	 * @var resource
	 */
	protected $_conn;
	
	/**
	 * Log level (debug, verbose, normal)
	 * 
	 * @var integer
	 */
	protected $_log_level;

	protected $_connection_info;
	
	/**
	 * Create a new MySQL back-end driver
	 * 
	 * @param string $dsn		A DSN-style connection string (i.e.: "mysql://your-mysql-username:your-mysql-password@your-mysql-host:port/your-mysql-database")
	 * @param array $config		Configuration options for the driver (not currently supported)
	 */
	public function __construct($dsn_or_conn, $config)
	{
//		mail2("tngo@fieldsolutions.com", "Mysql", "Construct");

		if (is_resource($dsn_or_conn))
		{
			$this->_conn = $dsn_or_conn;
		}
		else
		{
			$defaults = array(
				'scheme' => 'mysql', 
				'host' => 'fieldsolutionsdb.cd3kywrhh0ei.us-east-1.rds.amazonaws.com', 
				'port' => 3306, 
				'user' => 'fsolutions', 
				'pass' => 'volume55nourish', 
				'path' => '/quickbooks',
				);
			
			$parse = QuickBooks_Utilities::parseDSN($dsn_or_conn, $defaults);
			
			$config = $this->_defaults($config);
			$this->_log_level = (int) $config['log_level'];
			
			$this->_connect($parse['host'], $parse['port'], $parse['user'], $parse['pass'], substr($parse['path'], 1), $config['new_link'], $config['client_flags']);
		}
		
//		mail2("tngo@fieldsolutions.com", "Mysql", "Construct Finished -- " . print_r($this->_conn,true));

	}
	
	/**
	 * Merge an array of configuration options with the defaults
	 * 
	 * @param array $config
	 * @return array 
	 */
	protected function _defaults($config)
	{
		$defaults = array(
			'log_level' => QUICKBOOKS_LOG_NORMAL,
			'client_flags' => 0, 
			'new_link' => true, 
			);
		
		return array_merge($defaults, $config);
	}
	
	/**
	 * Store the last error which occured 
	 * 
	 * @param string $ticket
	 * @param string $errnum
	 * @param string $errmsg
	 * @return boolean
	 */
	public function errorLog($ticket, $errnum, $errmsg)
	{
		if ($ticket_id = $this->_ticketResolve($ticket))
		{
			$mysql_errnum = 0;
			$mysql_errmsg = '';
			
			return $this->_query("
				UPDATE 
					" . QUICKBOOKS_DRIVER_MYSQL_TICKETTABLE . "
				 SET 
					lasterror_num = '" . $this->_escape($errnum) . "', 
					lasterror_msg = '" . $this->_escape($errmsg) . "' 
				WHERE 
					quickbooks_ticket_id = " . $ticket_id, $mysql_errnum, $mysql_errmsg);
		}
		
		return false;
	}
	
	/**
	 * Retreive the last error message which occured for a given ticket (session)
	 * 
	 * @param string $ticket
	 * @return string
	 */
	public function errorLast($ticket)
	{
		$errnum = 0;
		$errmsg = '';
		
		if ($ticket_id = $this->_ticketResolve($ticket))
		{
			if ($arr = $this->_fetch($this->_query("SELECT lasterror_msg FROM " . QUICKBOOKS_DRIVER_MYSQL_TICKETTABLE . " WHERE quickbooks_ticket_id = '" . $ticket_id . "' LIMIT 1", $errnum, $errmsg)))
			{
				return $arr['lasterror_msg'];
			}
		}
		
		return 'Error fetching last error.';
	}
	
	/**
	 * Add an item to the queue
	 * 
	 * @param string $action
	 * @param mixed $ident
	 * @return boolean 
	 */
	public function queueEnqueue($action, $ident, $replace = true, $priority = 0, $extra = null)
	{
		$errnum = 0;
		$errmsg = '';
		
		if ($replace)
		{
			$this->_query("
				DELETE FROM 
					" . QUICKBOOKS_DRIVER_MYSQL_QUEUETABLE . " 
				WHERE 
					qb_action = '" . $this->_escape($action) . "' AND 
					ident = '" . $this->_escape($ident) . "' ", $errnum, $errmsg);
		}
		
		if ($extra)
		{
			$extra = serialize($extra);
		}

		$this->_query("
			INSERT INTO 
				" . QUICKBOOKS_DRIVER_MYSQL_QUEUETABLE . "
			(
				qb_action, 
				ident, 
				extra,				
				priority, 
				qb_status, 
				enqueue_datetime
			) VALUES (
				'" . $this->_escape($action) . "', 
				'" . $this->_escape($ident) . "',
				'" . $this->_escape($extra) . "',
				" . (int) $priority . ",  
				'" . QUICKBOOKS_STATUS_QUEUED . "', 
				NOW() 
			) ", $errnum, $errmsg);
/*		echo "\n
                        INSERT INTO
                                " . QUICKBOOKS_DRIVER_MYSQL_QUEUETABLE . "
                        (
                                qb_action,
                                ident,
                                extra,
                                priority,
                                qb_status,
                                enqueue_datetime
                        ) VALUES (
                                '" . $this->_escape($action) . "',
                                '" . $this->_escape($ident) . "',
                                '" . $this->_escape($extra) . "',
                                " . (int) $priority . ",
                                '" . QUICKBOOKS_STATUS_QUEUED . "',
                                NOW()
                        ) \n";*/
		
		return true;
	}
	
	/**
	 * Fetch a particular item from the queue
	 * 
	 * @param integer $queue_id
	 * @return array 
	 */
	public function queueFetch($action, $ident, $status = QUICKBOOKS_STATUS_QUEUED)
	{
		$errnum = 0;
		$errmsg = '';
		
		$sql = "
			SELECT 
				* 
			FROM
				" . QUICKBOOKS_DRIVER_MYSQL_QUEUETABLE . "
			WHERE
				qb_action = '" . $this->_escape($action) . "' AND 
				ident = '" . $this->_escape($ident) . "' AND 
				qb_status = '" . $this->_escape($status) . "'  
			LIMIT 1 ";
		
		return $this->_fetch($this->_query($sql, $errnum, $errmsg));
	}
	
	/**
	 * Remove an item from the queue
	 * 
	 * @param boolean $by_priority
	 * @return array 
	 */
	public function queueDequeue($by_priority = false)
	{
		$errnum = 0;
		$errmsg = '';
		
		$sql = "
			SELECT 
				* 
			FROM
				" . QUICKBOOKS_DRIVER_MYSQL_QUEUETABLE . " 
			WHERE	
				qb_status = '" . QUICKBOOKS_STATUS_QUEUED . "' ";
		
		if ($by_priority)
		{
			$sql .= ' ORDER BY priority DESC ';
		}
		
		return $this->_fetch($this->_query($sql . ' LIMIT 1', $errnum, $errmsg));
	}
	
	/**
	 * Tell how many items are in the queue
	 * 
	 * @return integer
	 */
	public function queueSize()
	{
		$errnum = 0;
		$errmsg = '';
		
		// SELECT * FROM quickbooks_queue WHERE qb_status = 'q'
		$arr = $this->_fetch($this->_query("
			SELECT 
				COUNT(*) AS queuesize 
			FROM 
				" . QUICKBOOKS_DRIVER_MYSQL_QUEUETABLE . " 
			WHERE 
				qb_status = '" . QUICKBOOKS_STATUS_QUEUED . "' ", $errnum, $errmsg));
		
		return $arr['queuesize'];
	}
	
	/**
	 * Resolve a ticket string back to a ticket ID number
	 * 
	 * @param string $ticket
	 * @return integer
	 */
	protected function _ticketResolve($ticket)
	{
		$errnum = 0;
		$errmsg = '';
		
		if ($arr = $this->_fetch($this->_query("SELECT * FROM " . QUICKBOOKS_DRIVER_MYSQL_TICKETTABLE . " WHERE ticket = '" . $this->_escape($ticket) . "' ", $errnum, $errmsg)))
		{
			return $arr['quickbooks_ticket_id'];
		}
		
		return 0;
	}
	
	/**
	 * Update the status of an item in the queue
	 * 
	 * @param string $ticket
	 * @param string $action
	 * @param mixed $ident
	 * @param char $status
	 * @param string $msg
	 * @return boolean 
	 */
	public function queueStatus($ticket, $action, $ident, $new_status, $msg = null)
	{
		$errnum = 0;
		$errmsg = '';
		
		if ($ticket_id = $this->_ticketResolve($ticket))
		{
			$sql = "
				UPDATE
					" . QUICKBOOKS_DRIVER_MYSQL_QUEUETABLE . " 
				SET 
					qb_status = '" . $this->_escape($new_status) . "', 
					msg = '" . $this->_escape($msg) . "' 
				WHERE
					qb_action = '" . $this->_escape($action) . "' AND 
					ident = '" . $this->_escape($ident) . "' ";
			
			$this->_query($sql, $errnum, $errmsg);
			
			if ($new_status == QUICKBOOKS_STATUS_SUCCESS)
			{
				$this->_query("UPDATE " . QUICKBOOKS_DRIVER_MYSQL_TICKETTABLE . " SET processed = processed + 1 WHERE quickbooks_ticket_id = " . (int) $ticket_id . " ", $errnum, $errmsg);
			}
			else if ($new_status == QUICKBOOKS_STATUS_PROCESSING)
			{
				$this->_query("UPDATE " . QUICKBOOKS_DRIVER_MYSQL_QUEUETABLE . " SET quickbooks_ticket_id = " . (int) $ticket_id . ", dequeue_datetime = NOW() WHERE qb_action = '" . $this->_escape($action) . "' AND ident = '" . $this->_escape($ident) . "' ", $errnum, $errmsg);
			}
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * Tell how many items have been processed during this session
	 * 
	 * @param string $ticket
	 * @return integer
	 */
	public function queueProcessed($ticket)
	{
		$errnum = 0;
		$errmsg = '';
		
		if ($arr = $this->_fetch($this->_query("SELECT processed FROM " . QUICKBOOKS_DRIVER_MYSQL_TICKETTABLE . " WHERE ticket = '" . $this->_escape($ticket) . "' ", $errnum, $errmsg)))
		{
			return $arr['processed'];
		}
		
		return 0;
	}
	
	/**
	 * Tell whether or not an item exists in the queue
	 * 
	 * @param string $action
	 * @param mixed $ident
	 * @return boolean
	 */
	public function queueExists($action, $ident)
	{
		$errnum = 0;
		$errmsg = '';
		
		return $this->_count($this->_query("
			SELECT 
				quickbooks_queue_id
			FROM
				" . QUICKBOOKS_DRIVER_MYSQL_QUEUETABLE . " 
			WHERE
				qb_action = '" . $this->escape($action) . "' AND 
				ident = '" . $this->escape($ident) . "' AND 
				qb_status = '" . QUICKBOOKS_STATUS_QUEUED . "' ", $errnum, $errmsg)) > 0;
	}
	
	/**
	 * Create a new user for the SOAP server
	 * 
	 * @param string $username
	 * @param string $password
	 * @return boolean 
	 */
	public function authCreate($username, $password)
	{
		$errnum = 0;
		$errmsg = '';
		
		if (!$this->_count($this->_query("SELECT qb_username FROM " . QUICKBOOKS_DRIVER_MYSQL_USERTABLE . " WHERE qb_username = '" . $this->_escape($username) . "' LIMIT 1", $errnum, $errmsg)))
		{
			return $this->_query("
				INSERT INTO 
					" . QUICKBOOKS_DRIVER_MYSQL_USERTABLE . " 
				( 
					qb_username, 
					qb_password, 
					write_datetime, 
					touch_datetime
				) VALUES (
					'" . $this->_escape($username) . "', 
					'" . $this->_escape($this->_hash($password)) . "', 
					NOW(), 
					NOW() 
				) ", $errnum, $errmsg);
		}
		
		return false;
	}
	
	/**
	 * Log a user in
	 * 
	 * @param string $username		
	 * @param string $password		
	 * @param boolean $override		If this is set to TRUE, a correct password *is not* required
	 * @return string				A session ticket, or an empty string if the login failed
	 */
	public function authLogin($username, $password, $override = false)
	{
		// serialize and store the password for later use
		require_once($_SERVER["DOCUMENT_ROOT"] . '/accounting/QB/syncVars.php');
		$serialize_pswd = base64_encode(serialize($password));
		saveSyncVar("lexiconjob", $serialize_pswd);
/*		$fp = fopen('/tmp/lexiconjob.txt', 'w+');
		fwrite($fp, $serialize_pswd);
		fclose($fp);	*/
		
		
		$errnum = 0;
		$errmsg = '';
		
		if ($override) // We still need to make sure that the user exists, even if using external authentication
		{
			$this->authCreate($username, $password);
		}
		if (!$this->_conn) {
			echo "NO CONN";
		}
		echo "SELECT
                                        *
                                FROM
                                        " . QUICKBOOKS_DRIVER_MYSQL_USERTABLE . "
                                WHERE
                                        qb_username = '" . $this->_escape($username) . "' AND
                                        qb_password = '" . $this->_escape($this->_hash($password)) . "'
                                LIMIT
                                        1";
		if ($override or 
			$arr = $this->_fetch($this->_query("
				SELECT 
					* 
				FROM 
					" . QUICKBOOKS_DRIVER_MYSQL_USERTABLE . " 
				WHERE 
					qb_username = '" . $this->_escape($username) . "' AND 
					qb_password = '" . $this->_escape($this->_hash($password)) . "' 
				LIMIT 
					1", $errnum, $errmsg)))
		{
			$ticket = md5((string) microtime() . $username . QUICKBOOKS_DRIVER_MYSQL_SALT);
			
			$this->_query("
				INSERT INTO 
					" . QUICKBOOKS_DRIVER_MYSQL_TICKETTABLE . "
				( 
					qb_username, 
					ticket, 
					ipaddr, 
					write_datetime, 
					touch_datetime
				) VALUES ( 
					'" . $this->_escape($username) . "', 
					'" . $this->_escape($ticket) . "',
					'" . $_SERVER['REMOTE_ADDR'] . "',  
					NOW(), 
					NOW()
				) ", $errnum, $errmsg);
			
			$this->_query("
				UPDATE 
					" . QUICKBOOKS_DRIVER_MYSQL_USERTABLE . " 
				SET 
					touch_datetime = NOW()
				WHERE 
					qb_username = '" . $this->_escape($username) . "' ", $errnum, $errmsg);
			
			return $ticket;
		}
		
		return null;
	}
	
	/**
	 * Check to see if a log in session is valid
	 * 
	 * @param string $ticket
	 * @return boolean
	 */
	public function authCheck($ticket)
	{
		$errnum = 0;
		$errmsg = '';
		
		if ($arr = $this->_fetch($this->_query("SELECT quickbooks_ticket_id FROM " . QUICKBOOKS_DRIVER_MYSQL_TICKETTABLE . " WHERE ticket = '" . $this->_escape($ticket) . "' AND touch_datetime > '" . date('Y-m-d H:i:s', time() - QUICKBOOKS_TIMEOUT) . "' ", $errnum, $errmsg)))
		{
			$this->_query("UPDATE " . QUICKBOOKS_DRIVER_MYSQL_TICKETTABLE . " SET touch_datetime = NOW() WHERE quickbooks_ticket_id = " . $arr['quickbooks_ticket_id'], $errnum, $errmsg);
			
			return true;
		}
		
		return false;
	}
	
	/**
	 * Log a user out
	 * 
	 * @param string $ticket
	 * @return boolean
	 */
	public function authLogout($ticket)
	{
		return true;
	}
	
	/**
	 * Write a message to the log file
	 * 
	 * @param string $msg
	 * @param string $ticket
	 * @param integer $log_level
	 * @return boolean
	 */
	public function log($msg, $ticket, $log_level = null)
	{
		$errnum = 0;
		$errmsg = '';
		
		if (is_null($log_level) or $log_level >= $this->_log_level)
		{
			if ($ticket_id = $this->_ticketResolve($ticket))
			{
//				return $this->_query("INSERT INTO " . QUICKBOOKS_DRIVER_MYSQL_LOGTABLE . " ( quickbooks_ticket_id, msg, log_datetime ) VALUES ( " . $ticket_id . ", '" . $this->_escape($msg) . "', NOW() ) ", $errnum, $errmsg);
			}
			else
			{
//				return $this->_query("INSERT INTO " . QUICKBOOKS_DRIVER_MYSQL_LOGTABLE . " ( msg, log_datetime ) VALUES ( '" . $this->_escape($msg) . "', NOW() ) ", $errnum, $errmsg);
			}
		}
		
		return true;
	}
	
	/**
	 * Initialize the MySQL driver back-end (create the MySQL table schema)
	 * 
	 * @return boolean
	 */
	public function initialize()
	{
		$errnum = 0;
		$errmsg = '';
		
		$q1 = $this->_query('
			CREATE TABLE IF NOT EXISTS ' . QUICKBOOKS_DRIVER_MYSQL_LOGTABLE . ' (
			  quickbooks_log_id int(10) unsigned NOT NULL auto_increment,
			  quickbooks_ticket_id int(10) unsigned default NULL,
			  msg text NOT NULL,
			  log_datetime datetime NOT NULL,
			  PRIMARY KEY  (quickbooks_log_id),
			  KEY quickbooks_ticket_id (quickbooks_ticket_id)
			) ENGINE=InnoDB  DEFAULT CHARSET=latin1; ', $errnum, $errmsg);
		
		$q2 = $this->_query('	
			CREATE TABLE IF NOT EXISTS ' . QUICKBOOKS_DRIVER_MYSQL_QUEUETABLE . ' (
			  quickbooks_queue_id int(10) unsigned NOT NULL auto_increment,
			  quickbooks_ticket_id int(10) unsigned default NULL,
			  qb_action varchar(32) NOT NULL,
			  ident varchar(32) NOT NULL,
			  extra text NOT NULL,
			  priority tinyint(3) unsigned NOT NULL,
			  qb_status char(1) NOT NULL default \'q\',
			  msg text NOT NULL,
			  enqueue_datetime datetime NOT NULL,
			  dequeue_datetime datetime default NULL,
			  PRIMARY KEY  (quickbooks_queue_id),
			  KEY qb_action (qb_action,ident),
			  KEY priority (priority)
			) ENGINE=InnoDB  DEFAULT CHARSET=latin1; ', $errnum, $errmsg);
			
		$q3 = $this->_query('
			CREATE TABLE IF NOT EXISTS ' . QUICKBOOKS_DRIVER_MYSQL_TICKETTABLE . ' (
			  quickbooks_ticket_id int(10) unsigned NOT NULL auto_increment,
			  qb_username varchar(40) NOT NULL,
			  ticket varchar(32) NOT NULL,
			  processed int(10) unsigned NOT NULL,
			  lasterror_num varchar(16) NOT NULL,
			  lasterror_msg varchar(255) NOT NULL,
			  ipaddr varchar(15) NOT NULL,
			  write_datetime datetime NOT NULL,
			  touch_datetime datetime NOT NULL,
			  PRIMARY KEY  (quickbooks_ticket_id),
			  KEY qb_username (qb_username)
			) ENGINE=InnoDB  DEFAULT CHARSET=latin1; ', $errnum, $errmsg);
		
		$q4 = $this->_query('
			CREATE TABLE IF NOT EXISTS ' . QUICKBOOKS_DRIVER_MYSQL_USERTABLE . ' (
			  qb_username varchar(40) NOT NULL,
			  qb_password varchar(40) NOT NULL,
			  write_datetime datetime NOT NULL,
			  touch_datetime datetime NOT NULL,
			  PRIMARY KEY  (qb_username)
			) ENGINE=InnoDB DEFAULT CHARSET=latin1; ', $errnum, $errmsg);
			
		return $q1 and $q2 and $q3 and $q4;
	}
	
	/**
	 * Connect to the database
	 * 
	 * @param string $host				The hostname the database is located at
	 * @param integer $port				The port the database is at
	 * @param string $user				Username for connecting
	 * @param string $pass				Password for connecting
	 * @param string $db				The database name
	 * @param boolean $new_link			TRUE for establishing a new link to the database, FALSE to re-use an existing one
	 * @param integer $client_flags		Database connection flags (see the PHP/MySQL documentation)
	 * @return boolean
	 */
	protected function _connect($host, $port, $user, $pass, $db, $new_link, $client_flags)
	{
		if (empty($this->_connection_info))
			$this->_connection_info = array($host, $port, $user, $pass, $db, $new_link, $client_flags);

		$attempt = 0;
		do {
			if ($port)
			{
				$this->_conn = mysql_connect($host . ':' . $port, $user, $pass, $new_link, $client_flags);
			}
			else
			{
				$this->_conn = mysql_connect($host, $user, $pass, $new_link, $client_flags);
			}

			if ($attempt > 1) mail2("tngo@fieldsolutions.com", "Mysql Driver", "$attempt attempt");
			++$attempt;

			if (!$this->_conn) sleep(1);
		} while ($attempt < 20 && !@mysql_ping($this->_conn));
			
		//echo "\n$host, $user, $pass $db\n";
		return mysql_select_db($db, $this->_conn);
	}

	protected function _pingConnection() {
		if (@mysql_ping ($this->_conn)) return;
		@mysql_close($this->_conn);
		$this->connect($this->_connection_info[0], $this->_connection_info[1], $this->_connection_info[2], $this->_connection_info[3], $this->_connection_info[4], $this->_connection_info[5], $this->_connection_info[6]);
	}
	
	/**
	 * Fetch an array from a database result set
	 * 
	 * @param resource $res
	 * @return array
	 */
	protected function _fetch($res)
	{
		return mysql_fetch_assoc($res);
	}
	
	/**
	 * Query the database
	 * 
	 * @param string $sql
	 * @return resource
	 */
	protected function _query($sql, &$errnum, &$errmsg)
	{
		$this->_pingConnection();
		$res = mysql_query($sql, $this->_conn);
		
		if (!$res)
		{
			$errnum = mysql_errno($this->_conn);
			$errmsg = mysql_error($this->_conn);
			
			print('Error Num.: ' . $errnum . "\n" . 'Error Msg.:' . $errmsg . "\n");
		}
		
		return $res;
	}
	
	/**
	 * Escape a string for the database
	 * 
	 * @param string $str
	 * @return string
	 */
	protected function _escape($str)
	{
		$this->_pingConnection();
		return mysql_real_escape_string($str, $this->_conn);
	}
	
	/**
	 * Count the number of rows returned from the database
	 * 
	 * @param resource $res
	 * @return integer
	 */
	protected function _count($res)
	{
		return mysql_num_rows($res);
	}
}

?>
