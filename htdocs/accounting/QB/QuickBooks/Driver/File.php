<?php

/**
 * QuickBooks driver base class
 * 
 * The Driver classes act as back-end to the Queue class and SOAP server. 
 * Driver classes should extend this base class and implement all abstract 
 * methods.  
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 * 
 * @package QuickBooks
 * @subpackage Driver
 */

$doc_root = $_SERVER["DOCUMENT_ROOT"];

/**
 * 
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Driver.php');

define('QUICKBOOKS_DRIVER_FILE_USERFILE', 'qbuser.dat');		// serialized
define('QUICKBOOKS_DRIVER_FILE_TICKETFILE', 'qbticket.dat');	// serialized
define('QUICKBOOKS_DRIVER_FILE_QUEUEFILE', 'qbqueue.dat');		// serialized
define('QUICKBOOKS_DRIVER_FILE_LOGFILE', 'qblog.log');			// plain-text logfile

/**
 * QuickBooks driver base class
 */
class QuickBooks_Driver_File extends QuickBooks_Driver
{
	/**
	 * 
	 */
	protected $_directory;
	
	/**
	 * Constructor
	 * 
	 * @param string $dsn		A DSN-style connection string
	 * @param array $config		An array of configuration information
	 */
	public function __construct($dsn, $config)
	{
		$defaults = array(
			'scheme' => 'file', 
			'host' => 'null', 
			'port' => 0, 
			'user' => '', 
			'pass' => '', 
			'path' => '/home/quickbooks',
			);
		
		$parse = QuickBooks_Utilities::parseDSN($dsn, $defaults);
		
		$config = $this->_defaults($config);
		$this->_log_level = (int) $config['log_level'];
		
		// try to open/create a file for each "table" 
	}
	
	protected function _lock($which, &$errnum, &$errmsg)
	{
		while (file_exists($this->_directory . '/' . $which . '.lock'))
		{
			sleep(1);
		}
		
		return touch($this->_directory . '/' . $which . '.lock');
	}
	
	protected function _unlock($which, &$errnum, &$errmsg)
	{
		if (file_exists($this->_directory . '/' . $which . '.lock'))
		{
			return unlink($this->_directory . '/' . $which . '.lock');
		}
		
		return false;
	}
	
	protected function _write($which, $data, &$errnum, &$errmsg)
	{
		$this->_lock($which);
		
		$fp = fopen($this->_directory . '/' . $which, 'wb+');
		$bytes = fwrite($fp, serialize($data));
		fclose($fp);
		
		$this->_unlock($which);
		
		return $bytes;
	}
	
	protected function _read($which, &$errnum, &$errmsg)
	{
		$this->_lock($which);
		
		$fp = fopen($this->_directory . '/' . $which, 'rb');
		$contents = fread($fp, filesize($this->_directory . '/' . $which));
		fclose($fp);
		
		$this->_unlock($which);
		
		return unserialize($contents);
	}
	
	/**
	 * Merge an array of configuration options with the defaults
	 * 
	 * @param array $config
	 * @return array 
	 */
	protected function _defaults($config)
	{
		$defaults = array(
			'log_level' => QUICKBOOKS_LOG_NORMAL,
			);
		
		return array_merge($defaults, $config);
	}
	
	protected function _sortByPriority(&$queue)
	{
		return true;
	}
	
	/**
	 * Place an action into the queue, along with a unique identifier (if neccessary)
	 * 
	 * Example: 
	 * <code>
	 * 	$driver->queueEnqueue('CustomerAdd', 1234); // Push customer #1234 over to QuickBooks
	 * </code>
	 * 
	 * @param string $action	The QuickBooks action to do
	 * @param mixed $ident		A unique identifier 
	 * @return boolean
	 */
	public function queueEnqueue($action, $ident, $replace = true, $priority = 0, $extra = null)
	{
		$errnum = 0;
		$errmsg = '';
		
		$queue = $this->_read(QUICKBOOKS_DRIVER_FILE_QUEUEFILE, $errnum, $errmsg);
		
		if (!$errnum)
		{
			$this->_sortByPriority($queue);
		
			return $this->_write(QUICKBOOKS_DRIVER_FILE_QUEUEFILE, $queue, $errnum, $errmsg) > 0;
		}
		
		return false;
	}
	
	/**
	 * Remove an item from the queue
	 * 
	 * @param boolean $by_priority	If TRUE, remove the item with the highest priority next
	 * @return boolean
	 */
	public function queueDequeue($by_priority = false)
	{
		$errnum = 0;
		$errmsg = '';
		
		$queue = $this->_read(QUICKBOOKS_DRIVER_FILE_QUEUEFILE, $errnum, $errmsg);
		
		if (!$errnum)
		{
			$item = array_shift($queue);
			
			return $item;
		}
		
		return false;
	}
	
	/**
	 * Update the status of a particular item in the queue
	 * 
	 * @param string $ticket		The ticket of the process which is updating the status
	 * @param string $action		The action
	 * @param mixed $ident			The ident string
	 * @param char $new_status		The new status code (QUICKBOOKS_STATUS_SUCCESS, QUICKBOOKS_STATUS_ERROR, etc.)
	 * @param string $msg			An error message (if an error message occured)
	 * @return boolean
	 */
	public function queueStatus($ticket, $action, $ident, $new_status, $msg = '')
	{
		$errnum = 0;
		$errmsg = '';
		
		$queue = $this->_read(QUICKBOOKS_DRIVER_FILE_QUEUEFILE, $errnum, $errmsg);
		
		if (!$errnum)
		{
			$count = count($queue);
			for ($i = 0; $i < $count; $i++)
			{
				$item =& $queue[$i];
				
				if ($item['qb_action'] == $action and $item['ident'] == $ident)
				{
					$item['qb_status'] = $new_status;
					$item['msg'] = $msg;
					
					break;
				}
			}
			
			return $this->_write(QUICKBOOKS_DRIVER_FILE_QUEUEFILE, $queue, $errnum, $errmsg);
		}
		
		return false;
	}
	
	/**
	 * Tell the number of items left in the queue
	 * 
	 * @return integer
	 */
	public function queueSize()
	{
		$errnum = 0;
		$errmsg = '';
		
		$queue = $this->_read(QUICKBOOKS_DRIVER_FILE_QUEUEFILE, $errnum, $errmsg);
		
		if (!$errnum)
		{
			$queued = 0;
			
			$count = count($queue);
			for ($i = 0; $i < $count; $i++)
			{
				$item =& $queue[$i];
				
				if ($item['qb_status'] == QUICKBOOKS_STATUS_QUEUED)
				{
					$queued++;
				}
			}
			
			return $queued;
		}
		
		return 0;
	}
	
	/**
	 * Fetch a specific item from the queue
	 * 
	 * @param string $action
	 * @param mixed $ident
	 * @param char $status
	 * @return array 
	 */
	public function queueFetch($action, $ident, $status = QUICKBOOKS_STATUS_QUEUED)
	{
		
	}
	
	/**
	 * Tell how many commands have been processed during this login session
	 * 
	 * @param string $ticket		The ticket for the login session
	 * @return integer				The number of commands processed so far
	 */
	public function queueProcessed($ticket)
	{
		
	}
	
	/**
	 * Tell whether or not an item exists in the queue
	 * 
	 * @param string $action
	 * @param mixed $ident
	 * @return boolean
	 */
	public function queueExists($action, $ident)
	{
		
	}
	
	/**
	 * Log an error that occured for a specific ticket
	 * 
	 * @param string $ticket
	 * @param string $errno
	 * @param string $errstr
	 * @return boolean
	 */
	public function errorLog($ticket, $errno, $errstr)
	{
		
	}
	
	/**
	 * Get the last error that occured
	 * 
	 * @param string $ticket
	 * @return string
	 */
	public function errorLast($ticket)
	{
		
	}
	
	/**
	 * Establish a session for a user (log that user in)
	 * 
	 * The QuickBooks Web Connector will pass a username and password to the 
	 * SOAP server. There is a SOAP ->authenticate() method which logs the user 
	 * in. 
	 * 
	 * @param string $username		The username for the QuickBooks Web Connector user
	 * @param string $password		The password for the QuickBooks Web Connector user
	 * @param boolean $override		If set to TRUE, a correct password will not be required
	 * @return string				The ticket for the login session
	 */
	public function authLogin($username, $password, $override = false)
	{
		
	}
	
	/**
	 * Check to see whether or not a ticket is for a valid, unexpired login session
	 * 
	 * @param string $ticket	The login session ticket to check
	 * @return boolean 			Whether or not the ticket is valid
	 */
	public function authCheck($ticket)
	{
		
	}
	
	/**
	 * End a log-in session
	 * 
	 * @param string $ticket	The ticket for the session
	 * @return boolean
	 */
	public function authLogout($ticket)
	{
		
	}
	
	/**
	 * Create a user account with the given username and password
	 * 
	 * @param string $username	The desired username
	 * @param string $password	The desired password
	 * @return boolean 			Whether or not the user was created
	 */
	public function authCreate($username, $password)
	{
		
	}
	
	/**
	 * Log a message to the QuickBooks log
	 * 
	 * @param string $msg		The message to place in the log
	 * @param string $ticket	The ticket for the login session
	 * @param integer $lvl		
	 * @return boolean 			Whether or not the message was logged successfully
	 */
	public function log($msg, $ticket, $lvl = QUICKBOOKS_LOG_NORMAL)
	{
		
	}
	
	/**
	 * One-way hash a password for storage in the database
	 * 
	 * @param string $password
	 * @return string
	 */	
	protected function _hash($password)
	{
		return sha1($password . QUICKBOOKS_SALT);
	}
}

?>