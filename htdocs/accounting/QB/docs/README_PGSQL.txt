--
-- PostgreSQL database dump
--

SET client_encoding = 'LATIN1';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: pgsql
--

COMMENT ON SCHEMA public IS 'Standard public schema';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: quickbooks_log; Type: TABLE; Schema: public; Owner: pgsql; Tablespace: 
--

CREATE TABLE quickbooks_log (
    quickbooks_log_id integer NOT NULL,
    quickbooks_ticket_id integer,
    msg text NOT NULL,
    log_datetime timestamp without time zone NOT NULL
);


ALTER TABLE public.quickbooks_log OWNER TO pgsql;

--
-- Name: quickbooks_log_quickbooks_log_id_seq; Type: SEQUENCE; Schema: public; Owner: pgsql
--

CREATE SEQUENCE quickbooks_log_quickbooks_log_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.quickbooks_log_quickbooks_log_id_seq OWNER TO pgsql;

--
-- Name: quickbooks_log_quickbooks_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgsql
--

ALTER SEQUENCE quickbooks_log_quickbooks_log_id_seq OWNED BY quickbooks_log.quickbooks_log_id;


--
-- Name: quickbooks_log_quickbooks_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgsql
--

SELECT pg_catalog.setval('quickbooks_log_quickbooks_log_id_seq', 551, true);


--
-- Name: quickbooks_queue; Type: TABLE; Schema: public; Owner: pgsql; Tablespace: 
--

CREATE TABLE quickbooks_queue (
    quickbooks_queue_id integer NOT NULL,
    quickbooks_ticket_id integer,
    qb_action character varying(32) NOT NULL,
    ident character varying(32) NOT NULL,
    priority integer DEFAULT 0 NOT NULL,
    qb_status character(1) DEFAULT 'q'::bpchar NOT NULL,
    msg text,
    enqueue_datetime time without time zone DEFAULT now() NOT NULL,
    dequeue_datetime time without time zone,
    extra text NOT NULL
);


ALTER TABLE public.quickbooks_queue OWNER TO pgsql;

--
-- Name: quickbooks_queue_quickbooks_queue_id_seq; Type: SEQUENCE; Schema: public; Owner: pgsql
--

CREATE SEQUENCE quickbooks_queue_quickbooks_queue_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.quickbooks_queue_quickbooks_queue_id_seq OWNER TO pgsql;

--
-- Name: quickbooks_queue_quickbooks_queue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgsql
--

ALTER SEQUENCE quickbooks_queue_quickbooks_queue_id_seq OWNED BY quickbooks_queue.quickbooks_queue_id;


--
-- Name: quickbooks_queue_quickbooks_queue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgsql
--

SELECT pg_catalog.setval('quickbooks_queue_quickbooks_queue_id_seq', 2, true);


--
-- Name: quickbooks_ticket; Type: TABLE; Schema: public; Owner: pgsql; Tablespace: 
--

CREATE TABLE quickbooks_ticket (
    quickbooks_ticket_id integer NOT NULL,
    qb_username character varying(40) NOT NULL,
    ticket character varying(32) NOT NULL,
    processed integer DEFAULT 0 NOT NULL,
    ipaddr character(15) NOT NULL,
    write_datetime timestamp without time zone DEFAULT now() NOT NULL,
    touch_datetime timestamp without time zone DEFAULT now() NOT NULL,
    lasterror_num character varying(16),
    lasterror_msg character varying(255)
);


ALTER TABLE public.quickbooks_ticket OWNER TO pgsql;

--
-- Name: quickbooks_ticket_quickbooks_ticket_id_seq; Type: SEQUENCE; Schema: public; Owner: pgsql
--

CREATE SEQUENCE quickbooks_ticket_quickbooks_ticket_id_seq
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.quickbooks_ticket_quickbooks_ticket_id_seq OWNER TO pgsql;

--
-- Name: quickbooks_ticket_quickbooks_ticket_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pgsql
--

ALTER SEQUENCE quickbooks_ticket_quickbooks_ticket_id_seq OWNED BY quickbooks_ticket.quickbooks_ticket_id;


--
-- Name: quickbooks_ticket_quickbooks_ticket_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pgsql
--

SELECT pg_catalog.setval('quickbooks_ticket_quickbooks_ticket_id_seq', 30, true);


--
-- Name: quickbooks_user; Type: TABLE; Schema: public; Owner: pgsql; Tablespace: 
--

CREATE TABLE quickbooks_user (
    qb_username character varying(40) NOT NULL,
    qb_password character varying(40) NOT NULL,
    write_datetime timestamp without time zone DEFAULT now() NOT NULL,
    touch_datetime timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.quickbooks_user OWNER TO pgsql;

--
-- Name: quickbooks_log_id; Type: DEFAULT; Schema: public; Owner: pgsql
--

ALTER TABLE quickbooks_log ALTER COLUMN quickbooks_log_id SET DEFAULT nextval('quickbooks_log_quickbooks_log_id_seq'::regclass);


--
-- Name: quickbooks_queue_id; Type: DEFAULT; Schema: public; Owner: pgsql
--

ALTER TABLE quickbooks_queue ALTER COLUMN quickbooks_queue_id SET DEFAULT nextval('quickbooks_queue_quickbooks_queue_id_seq'::regclass);


--
-- Name: quickbooks_ticket_id; Type: DEFAULT; Schema: public; Owner: pgsql
--

ALTER TABLE quickbooks_ticket ALTER COLUMN quickbooks_ticket_id SET DEFAULT nextval('quickbooks_ticket_quickbooks_ticket_id_seq'::regclass);


--
-- Name: quickbooks_log_pkey; Type: CONSTRAINT; Schema: public; Owner: pgsql; Tablespace: 
--

ALTER TABLE ONLY quickbooks_log
    ADD CONSTRAINT quickbooks_log_pkey PRIMARY KEY (quickbooks_log_id);


--
-- Name: quickbooks_queue_pkey; Type: CONSTRAINT; Schema: public; Owner: pgsql; Tablespace: 
--

ALTER TABLE ONLY quickbooks_queue
    ADD CONSTRAINT quickbooks_queue_pkey PRIMARY KEY (quickbooks_queue_id);


--
-- Name: quickbooks_ticket_pkey; Type: CONSTRAINT; Schema: public; Owner: pgsql; Tablespace: 
--

ALTER TABLE ONLY quickbooks_ticket
    ADD CONSTRAINT quickbooks_ticket_pkey PRIMARY KEY (quickbooks_ticket_id);


--
-- Name: quickbooks_user_pkey; Type: CONSTRAINT; Schema: public; Owner: pgsql; Tablespace: 
--

ALTER TABLE ONLY quickbooks_user
    ADD CONSTRAINT quickbooks_user_pkey PRIMARY KEY (qb_username);


--
-- Name: quickbooks_log_quickbooks_ticket_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pgsql
--

ALTER TABLE ONLY quickbooks_log
    ADD CONSTRAINT quickbooks_log_quickbooks_ticket_id_fkey FOREIGN KEY (quickbooks_ticket_id) REFERENCES quickbooks_ticket(quickbooks_ticket_id);


--
-- Name: quickbooks_queue_quickbooks_ticket_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pgsql
--

ALTER TABLE ONLY quickbooks_queue
    ADD CONSTRAINT quickbooks_queue_quickbooks_ticket_id_fkey FOREIGN KEY (quickbooks_ticket_id) REFERENCES quickbooks_ticket(quickbooks_ticket_id);


--
-- Name: quickbooks_ticket_qb_username_fkey; Type: FK CONSTRAINT; Schema: public; Owner: pgsql
--

ALTER TABLE ONLY quickbooks_ticket
    ADD CONSTRAINT quickbooks_ticket_qb_username_fkey FOREIGN KEY (qb_username) REFERENCES quickbooks_user(qb_username);


--
-- Name: public; Type: ACL; Schema: -; Owner: pgsql
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM pgsql;
GRANT ALL ON SCHEMA public TO pgsql;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

