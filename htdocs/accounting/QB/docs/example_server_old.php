<?php

/**
 * Example QuickBooks SOAP Server
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 * @edtited Tom Anderson
 */

$doc_root = $_SERVER["DOCUMENT_ROOT"];

// Require the framework
require_once($doc_root . '/QuickBooks/Server.php');

// Map QuickBooks actions to handler functions
$map = array(
	'CustomerAdd' => array( '_quickbooks_customer_add_request', '_quickbooks_customer_add_response' ),
	// ... more action handlers here ...
	);

$soap_options = array();		// See http://www.php.net/soap
$handler_options = array();		// See the comments in the QuickBooks/Server/Handlers.php file
$driver_options = array(		// See the comments in the QuickBooks/Driver/<YOUR DRIVER HERE>.php file ( i.e. 'Mysql.php', etc. )
	'log_level' => QUICKBOOKS_LOG_DEBUG, 
	);

// Create a new server and tell it to handle the requests
// This assumes that:
//	- You are connecting to MySQL with the username 'root'
//	- You are connecting to MySQL with the password ''
//	- Your MySQL server is located on the same machine as the script ( i.e.: 'localhost', if it were on another machine, you might use 'other-machines-hostname.com', or '192.168.1.5', or ... etc. )
//	- Your MySQL database name containing the QuickBooks tables is named 'qbtest' 
$server = new QuickBooks_Server('mysql://root:@localhost/qbtest', $map, QUICKBOOKS_WSDL, $soap_options, $handler_options, $driver_options);
$server->handle();

/**
 * Generate a qbXML response to add a particular customer to QuickBooks
 * 
 * @param string $requestID		You should include this in your qbXML request (it helps with debugging later)
 * @param string $action		The QuickBooks action being performed (CustomerAdd in this case)
 * @param mixed $ID				The unique identifier for the record (maybe a customer ID number in your database or something)
 * @param string $err			An error message, assign a value to $err if you want to report an error
 * @return string				A valid qbXML request
 */
function _quickbooks_customer_add_request($requestID, $action, $ID, &$err) {
	// Fetch your customer record from your database
	$record = mysql_fetch_array(mysql_query("SELECT name, phone, email FROM my_customer_table WHERE id = " . $ID));
	
	// Create and return a qbXML request
	$qbxml = '<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="2.0"?>
		<QBXML>
			<QBXMLMsgsRq onError="stopOnError">
				<CustomerAddRq requestID="' . $requestID . '">
					<CustomerAdd>
						<Name>' . $record['name'] . '</Name>
						<Phone>' . $record['phone'] . '</Phone>
						<Email>' . $record['email'] . '</Email>
					</CustomerAdd>
				</CustomerAddRq>
			</QBXMLMsgsRq>
		</QBXML>';
		
	return $qbxml;
}

/**
 * Receive a response from QuickBooks 
 * 
 * @param string $requestID		The requestID you passed to QuickBooks previously
 * @param string $action		The action that was performed (CustomerAdd in this case)
 * @param mixed $ID				The unique identifier of the record
 * @param string $err			An error message, assign a valid to $err if you want to report an error
 * @param string $xml			The complete qbXML response
 * @param array $idents			An array of identifiers that are contained in the qbXML response
 * @return void
 */
function _quickbooks_customer_add_response($requestID, $action, $ID, &$err, $xml, $idents) {
	// Instead of just printing this out, you would probably want to store the ListID value somewhere
	// You'll need the ListID value later if you decide to push Invoices over to QuickBooks or if 
	// you decide you want to push over Customer modifications later
	
	if(mysql_query("UPDATE my_customer_table SET list_id = " . $idents['ListID'] . " WHERE id = " . $ID)) {
		// do nothing
	}
	else {
		$err = "Customer Table Not Updated";
	}
	
	//echo 'Customer #' . $ID . ' has been added to QuickBooks with a QuickBooks ID value of: ' . $idents['ListID'];
}

?>