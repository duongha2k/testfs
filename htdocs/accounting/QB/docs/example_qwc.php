<?php

/**
 * Example of generating QuickBooks *.QWC files 
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 * 
 * @package QuickBooks
 * @subpackage Documentation
 */

/**
 * Require the utilities class
 */

$doc_root = $_SERVER["DOCUMENT_ROOT"];
 
require_once($doc_root . '/accounting/QB/QuickBooks/Utilities.php');

$name = 'Field Solutions Quickbooks Sync';				// A name for your server (make it whatever you want)
$descrip = 'Field Solutions Quickbooks Sync';		// A description of your server 

$appurl = 'https://www.fieldsolutions.com/accounting/QB/qb_sync/server.php';		// This *must* be httpS:// (path to your QuickBooks SOAP server)
$appsupport = 'https://www.fieldsolutions.com/'; 		// This *must* be httpS:// and the domain name must match the domain name above

$username = 'quickbooks';		// This is the username you stored in the 'quickbooks_user' table by using QuickBooks_Utilities::createUser()

$fileid = '{d54f50ee-4abe-4ba6-890e-9696dcf6899e}';		// Just make this up, but make sure it keeps that format
$ownerid = '{a91788d3-22a4-4506-b8ea-21f2f786a7c2}';		// Just make this up, but make sure it keeps that format

$qbtype = QUICKBOOKS_TYPE_QBFS;	// You can leave this as-is unless you're using QuickBooks POS

$readonly = false; // No, we want to write data to QuickBooks

$run_every_n_seconds = ''; // Run every 600 seconds (10 minutes)

// Generate the XML file
$xml = QuickBooks_Utilities::generateQWC($name, $descrip, $appurl, $appsupport, $username, $fileid, $ownerid, $qbtype, $readonly, $run_every_n_seconds);

// Send as a file download
header('Content-type: text/xml');
header('Content-Disposition: attachment; filename="qb_test.qwc"');
print($xml);
exit;

?>