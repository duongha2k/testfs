<?php

/**
 * Example QuickBooks SOAP Server
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 * 
 * @package QuickBooks
 * @subpackage Documentation
 */

// Require the framework
require_once 'QuickBooks/Server.php';

// Map QuickBooks actions to handler functions
$map = array(
	'CustomerAdd' => array( '_quickbooks_customer_add_request', '_quickbooks_customer_add_response' ),
	// ... more action handlers here ...
	);

// This is entirely optional, use it to trigger actions when an error is returned by QuickBooks
$errmap = array(
	3070 => '_quickbooks_error_stringtoolong',				// Whenever a string is too long to fit in a field, call this function: _quickbooks_error_stringtolong()
	// 'CustomerAdd' => '_quickbooks_error_customeradd', 	// Whenever an error occurs while trying to perform an 'AddCustomer' action, call this function: _quickbooks_error_customeradd()
	// '!' => '_quickbooks_error_catchall', 				// Using a key value of '!' will catch any errors which were not caught by another error handler
	// ... more error handlers here ...
	);

$soap_options = array();		// See http://www.php.net/soap
$handler_options = array(
	//'authenticate_handler' => ' *** YOU DO NOT NEED TO PROVIDE THIS CONFIGURATION VARIABLE TO USE THE DEFAULT AUTHENTICATION METHOD FOR THE DRIVER YOU'RE USING (I.E.: MYSQL) *** '
	//'authenticate_handler' => 'ldapv3://ldap.example.com:389/ou=People,dc=example,dc=com',
	//'authenticate_handler' => 'mysql://user:pass@localhost/database?quickbooks_user',  
	//'authenticate_handler' => 'postgresql://user:pass@localhost/database?quickbooks_user', 
	);		// See the comments in the QuickBooks/Server/Handlers.php file
$driver_options = array(		// See the comments in the QuickBooks/Driver/<YOUR DRIVER HERE>.php file ( i.e. 'Mysql.php', etc. )
	'log_level' => QUICKBOOKS_LOG_DEBUG, 
	);

// Create a new server and tell it to handle the requests
// This assumes that:
//	- You are connecting to MySQL with the username 'root'
//	- You are connecting to MySQL with the password 'password'
//	- Your MySQL server is located on the same machine as the script ( i.e.: 'localhost', if it were on another machine, you might use 'other-machines-hostname.com', or '192.168.1.5', or ... etc. )
//	- Your MySQL database name containing the QuickBooks tables is named 'your_database' 
$server = new QuickBooks_Server('mysql://root:password@localhost/your_database', $map, $errmap, QUICKBOOKS_WSDL, $soap_options, $handler_options, $driver_options);
$server->handle();

/**
 * Generate a qbXML response to add a particular customer to QuickBooks
 * 
 * @param string $requestID		You should include this in your qbXML request (it helps with debugging later)
 * @param string $action		The QuickBooks action being performed (CustomerAdd in this case)
 * @param mixed $ID				The unique identifier for the record (maybe a customer ID number in your database or something)
 * @param array $extra			
 * @param string $err			An error message, assign a value to $err if you want to report an error
 * @return string				A valid qbXML request
 */
function _quickbooks_customer_add_request($requestID, $action, $ID, $extra, &$err)
{
	// Fetch your customer record from your database
	$record = mysql_fetch_array(mysql_query("SELECT * FROM your_customer_table WHERE your_customer_ID_field = " . $ID));
	
	// Create and return a qbXML request
	$qbxml = '<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="2.0"?>
		<QBXML>
			<QBXMLMsgsRq onError="stopOnError">
				<CustomerAddRq requestID="' . $requestID . '">
					<CustomerAdd>
						<Name>' . $record['your_customer_name_field'] . '</Name>
						<CompanyName>' . $record['your_customer_company_field'] . '</CompanyName>
						
						... lots of other customer related fields ...
						
					</CustomerAdd>
				</CustomerAddRq>
			</QBXMLMsgsRq>
		</QBXML>';
		
	return $qbxml;
}

/**
 * Receive a response from QuickBooks 
 * 
 * @param string $requestID		The requestID you passed to QuickBooks previously
 * @param string $action		The action that was performed (CustomerAdd in this case)
 * @param mixed $ID				The unique identifier of the record
 * @param array $extra			
 * @param string $err			An error message, assign a valid to $err if you want to report an error
 * @param string $xml			The complete qbXML response
 * @param array $idents			An array of identifiers that are contained in the qbXML response
 * @return void
 */
function _quickbooks_customer_add_response($requestID, $action, $ID, $extra, &$err, $xml, $idents)
{
	// Instead of just printing this out, you would probably want to store the ListID value somewhere
	// You'll need the ListID value later if you decide to push Invoices over to QuickBooks or if 
	// you decide you want to push over Customer modifications later
	print('Customer #' . $ID . ' has been added to QuickBooks with a QuickBooks ID value of: ' . $idents['ListID']);
}

/**
 * Catch and handle a "that string is too long for that field" error (err no. 3070) from QuickBooks
 * 
 * @param string $requestID			
 * @param string $action
 * @param mixed $ID
 * @param mixed $extra
 * @param string $err
 * @param string $xml
 * @param mixed $errnum
 * @param string $errmsg
 * @return void
 */
function _quickbooks_error_stringtoolong($requestID, $action, $ID, $extra, &$err, $xml, $errnum, $errmsg)
{
	mail('your-email@your-domain.com', 
		'QuickBooks error occured!', 
		'QuickBooks thinks that ' . $action . ': ' . $ID . ' has a value which will not fit in a QuickBooks field...');
}

?>