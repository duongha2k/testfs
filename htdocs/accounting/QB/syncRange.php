<?php
	require_once("../../headerSimple.php");
	require_once($_SERVER["DOCUMENT_ROOT"] . '/accounting/QB/BillQueryDateRange.php');
//	setBillQueryDateRangeMoving("-3 days");
?>
<div align="center">
<?php
	if (isset($_POST)) {
		if ($_POST["daysAgo"] != "") {
			if (is_numeric($_POST["daysAgo"]))
				setBillQueryDateRangeMoving("-" . $_POST["daysAgo"] . " days");
			echo "Updated.<br/><br/>";
		}
		else if ($_POST["startRange"] != "" && $_POST["endRange"] != "") {
			echo "Updated.<br/><br/>";
			setBillQueryDateRange($_POST["startRange"], $_POST["endRange"]);
		}
	}

	$dateRange = getBillQueryDateRange();
	
	if (!$dateRange) {
		setBillQueryDateRangeMoving("-5 days");
		$dateRange = getBillQueryDateRange();
	}
	
	if (isset($dateRange["MovingRange"]))
		$rangeTxt = $dateRange["MovingRange"];
	else
		$rangeTxt = $dateRange["StartRange"] . " thru " . $dateRange["EndRange"];
?>
	Current date range: <span style="font-weight: bold"><?=$rangeTxt?></span><br/><br/>
	<form action="<?=$_SERVER['PHP_SELF']?>" method="post">
    	Previous <input id="daysAgo" name="daysAgo" value="" size="3"/> days (Format: #)<br/><br/>
        - OR -<br/><br/>
    	Specific Date Range <input id="startRange" name="startRange" value="" size="10"/> thru <input id="endRange" name="endRange" value="" size="10"/> (Format: YYYY-MM-DD)<br/><br/>
        <input id="submit" name="submit" type="submit" value="Change Date" />
    </form>
</div>
<?php require_once("../../footer.php");?>
