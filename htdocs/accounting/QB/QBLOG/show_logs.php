<?
	print("<b>Logs</b><br/><br />");
	
	
	if($handle = opendir('/tmp/QBLOG/')) {
		$a = 0;
		
		while(false !== ($file = readdir($handle))) {
			if($file != "." && $file != "..") {
				$filetime = filemtime('/tmp/QBLOG/' . $file);
				$file_array[0][$a] = $file;
				$file_array[1][$a] = $filetime;			
			}
			$a++;
		}
		closedir($handle);
	}
	
	//sort($file_array);
	array_multisort($file_array[1], SORT_DESC, $file_array[0], SORT_DESC);
	
	print('
		<table cellpadding="0" cellspacing="0" border="1">
			<tr>
				<td><b>Date</b></td>
				<td><b>Mod Time</b></td>
			</tr>
	');
	
	for($b = 0; $b < count($file_array[1]); $b++) {
		$name = $file_array[0][$b];
		$time = date("m-d-Y H:i:s", $file_array[1][$b]);
		//$time = $file_array[1][$b];
		
		print("
			<tr>
				<td><a href=\"/accounting/QB/QBLOG/read_dir.php?dirname=$name\">$name</a></td>
				<td>$time</td>
			</tr>
		");
	}
?>