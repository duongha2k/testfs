<?php

/**
 * QuickBooks integration package via the QuickBooks Web Connector SOAP service 
 * 
 * This class provides a framework for generating and handling messages to send 
 * to and from QuickBooks via the Web Connector. For every operation you wish 
 * to perform on QuickBooks, you should be expecting to implement two functions:
 * 	- A function responsible for generating the qbXML request (telling QuickBooks what you want to do)
 * 	- A function responsible for parsing and handling the qbXML response (handling the response form QuickBooks)
 * 
 * So, for example, if you want to add a customer to QuickBooks, you might 
 * implement these two functions:
 * 	- string customer_add_request($requestID, $action, $ident, $extra, &$err) { ... }
 * 	- void customer_add_response($requestID, $action, $ident, $extra, &$err, $xml, $qbID) { ... }
 * 
 * The function customer_add_request() is passed the following parameters:
 * 	- string $requestID 	You should embed this in the XML string you generate so that you can track requests/responses, example: <QBXML> ... <CustomerAddRq requestID="<?php print($requestID); ?>"> ... </QBXML>
 * 	- string $action 		This is the action you used when you queued the job, example: CustomerAdd
 * 	- mixed $ident			The identifier string you used when you queued the job: example: #1234
 * 	- array $extra			Any extra data you chose to pass when you called ->enqueue()
 * 	- string $err			If an error occurs in your handler function, assign an error message to $err
 * 	
 * The function customer_add_request() should return a string containing a valid qbXML request (see the QuickBooks SDK/Onscreen Reference for valid qbXML requests)
 * 
 * The function customer_add_response() is passed the following parameters:
 * 	- string $requestID		The requestID you (hopefully) embedded in your qbXML request
 * 	- string $action		The action you used when you queued the job (this will *only be included if you embedded the requestID in your qbXML request* )
 * 	- mixed $ident			The identifier string you used when you queued the job (this will *only be included if you embedded the requestID in your qbXML request* )
 * 	- array $extra			Any extra data you chose to pass in when you called ->enqueue()
 * 	- string $err			If an error occurs in your handler function, assign an error message to $err
 * 	- string $xml			The qbXML response that QuickBooks generated (you can parse this and use the data if you want, see the Onscreen Reference for examples of responses)
 * 	- mixed $qbIDs			Most QuickBooks requests generate unique internal QuickBooks identifiers, this will be an array of unique identifiers extracted from the XML response (i.e.: for the AddInvoice action, one of the array keys will be 'TxnID' and the value will be the internal QuickBooks transaction ID for that invoice, for a AddCustomer action, one of the array keys will be 'ListID', the internal identifier value for that customer in QuickBooks)
 * 
 * The function customer_add_response() does not need to return a value.
 * 
 * Register your function handlers with the QuickBooks SOAP server when you instantiate it:
 * <code>
 * <?php
 * // File: my-quickbooks-soap-server.php 
 * 
 * require_once 'QuickBooks/Server.php';
 * 
 * $your_mysql_username = 'user';
 * $your_mysql_password = 'pass';
 * $your_mysql_server = 'localhost';
 * $your_mysql_database = 'your_database_name';
 * 
 * // Builds a string that looks like this: "mysql://user:pass@localhost/your_database_name"
 * $your_mysql_dsn_string = 'mysql://' . $your_mysql_username . ':' . $your_mysql_password . '@' . $your_mysql_server . '/' . $your_mysql_database;
 * 
 * // This array maps QuickBooks actions to function names: one function for generating the QuickBooks qbXML request, and one function for handling the QuickBooks qbXML response
 * $map = array(
 * 	QUICKBOOKS_ADD_CUSTOMER => array( 'customer_add_request', 'customer_add_response' ), 
 * 	QUICKBOOKS_ADD_INVOICE => array( 'invoice_add_request', 'invoice_add_response' ), );
 * 
 * // Create the QuickBooks server
 * $server = new QuickBooks_Server($your_mysql_dsn_string, $map);
 * $server->handle();
 * 
 * ?>
 * </code>
 * 
 * Add items to the QuickBooks queue using the {@see QuickBooks_Queue} class. 
 * The next time the QuickBooks Web Connector connects to the SOAP server, it 
 * will be instructed to perform commands based on what has been placed in the 
 * queue. So if you queued up "CustomerAdd" "1234", #1234, 
 * customer_add_request() will generate a qbXML request telling QuickBooks to 
 * add that customer, the SOAP server will send that request out, and 
 * QuickBooks will send back a qbXML response indicating whether or not that 
 * customer was added successfully. 
 * 
 * The QuickBooks Web Connector (QBWC) works like this:
 * 	- You create a SOAP server that response to a set of SOAP methods
 * 	- You install and run the QBWC alongside your existing QuickBooks installation
 * 	- You register your SOAP server with the QBWC
 * 	- The QBWC calls the ->authenticate() method via a SOAP request
 * 	- You create and assign a 'ticket' (essentially a session ID value) to the QBWC session, this ticket gets sent to your SOAP server for authentication purposes with every request thereafter
 * 	- The QBWC calls the ->sendRequestXML() method via a SOAP request, if there is work to do, you send back qbXML commands encapsulated in an object
 * 	- The QBWC passes these qbXML commands to QuickBooks, QuickBooks processes them and passes them back
 * 	- The QBWC passes back the response from QuickBooks to your SOAP server via a SOAP call to ->receiveResponseXML()
 * 	- If you return an integer between 0 and 99 (inclusive) from ->receiveResponseXML(), the QBWC will call ->sendRequestXML() again, to get the next qbXML command
 * 	- Once you return a 100 from ->receiveResponseXML(), the QBWC calls ->closeConnection() and closes the socket connection shortly thereafter
 * 
 * Some notes:
 * 	- Go download the QuickBooks SDK (it has lots of helpful stuff in it) 
 * 	- Onscreen Reference (shows all of the XML commands)
 * 	- Tools > qbXML Validator (the QuickBooks Web Connector error log shows almost no debugging information, run your XML through the Validator and it will tell you *exactly* what the error in your XML stream is)
 * 	- Your version of QuickBooks might not support the latest version of the qbXML SDK, so you might have to set the qbXML message version with: <?qbxml version="x.y"?> (try 2.0 or another low number if you get error messages about versions)
 * 	- Check our the QuickBooks_Utilities class, it contains a few helpful static methods
 * 	- This class *requires* the PHP5 SOAP extension
 * 
 * Troubleshooting:
 * 	- Errors which QuickBooks reports will be logged (check the quickbooks_queue and quickbooks_log tables in MySQL)
 * 	- If actions get stuck in the queue and never seem to be pulled out, it is most likely that you're generating badly-formed XML for that request. Check the XML document you're creating for well-formedness, *and especially* character set issues
 * 	- If you're using the MySQL backend, the quickbooks_log table shows everything that is happening 
 * 
 * @author Keith Palmer <keith@AcademicKeys.com>
 * 
 * @package QuickBooks
 */

$doc_root = $_SERVER["DOCUMENT_ROOT"];

if (substr(strtolower(PHP_OS), 0, 3) == 'win')
{
	define('QUICKBOOKS_DIRECTORY_SEPARATOR', ';');
}
else
{
	define('QUICKBOOKS_DIRECTORY_SEPARATOR', ':');
}

// Include path modifications (relative paths within library)
ini_set('include_path', ini_get('include_path') . QUICKBOOKS_DIRECTORY_SEPARATOR . dirname(__FILE__));

if (!defined('QUICKBOOKS_SALT'))
{
	/**
	 * Salt value for hashing passwords
	 * @var string
	 */
	define('QUICKBOOKS_SALT', 'andB@++3ry');
}

if (!defined('QUICKBOOKS_TIMEOUT'))
{
	/**
	 * The number of seconds without any activity a session can stay open before automatically closed
	 * @var integer
	 */
	define('QUICKBOOKS_TIMEOUT', 120);
}

if (!defined('QUICKBOOKS_WSDL'))
{
	/**
	 * Path to the QuickBooks WSDL file (the default WSDL is included with this package, you shouldn't need to override this generally) 
	 * @var string
	 */
	define('QUICKBOOKS_WSDL', $doc_root . '/accounting/QB/QuickBooks/QBWebConnectorSvc.wsdl');
}

if (!defined('QUICKBOOKS_DEBUG'))
{
	/**
	 * Whether or not to turn on debugging (unsupported for now...?)
	 * @var boolean
	 */
	define('QUICKBOOKS_DEBUG', true);
}

if (!defined('QUICKBOOKS_LOG'))
{
	/**
	 * Debug log (unsupported?)
	 * 
	 * @deprecated
	 * @var string
	 */
	define('QUICKBOOKS_LOG', $doc_root . '/accounting/QB/QBLOG/qb-debug.log');
}

/**
 * Debug logging (too much data is logged... generally only useful for debugging)
 * @var integer
 */
define('QUICKBOOKS_LOG_DEBUG', 3);

/**
 * Verbose logging (lots of data is logged)
 * @var integer
 */
define('QUICKBOOKS_LOG_VERBOSE', 2);

/**
 * Normal logging (minimal data is logged)
 * @var integer
 */
define('QUICKBOOKS_LOG_NORMAL', 1);

/**
 * No logging at all (you probably should not use this...)
 * @var integer
 */
define('QUICKBOOKS_LOG_NONE', 0);

/**
 * 
 */
define('QUICKBOOKS_TYPE_QBFS', 'QBFS');

/**
 * 
 */
define('QUICKBOOKS_TYPE_QBPOS', 'QBPOS');

/**
 * QuickBooks flag to request to enter "Interactive Mode"
 * 
 * *** DO NOT CHANGE THIS *** This is a required QuickBooks-defined constant that is neccessary for interactive mode requests
 * 
 * @var string
 */
define('QUICKBOOKS_INTERACTIVE_MODE', 'Interactive mode');

define('QUICKBOOKS_ADD_ACCOUNT', 'AccountAdd');
define('QUICKBOOKS_MOD_ACCOUNT', 'AccountMod');
define('QUICKBOOKS_QUERY_ACCOUNT', 'AccountQuery');

define('QUICKBOOKS_ADD_BILL', 'BillAdd');
define('QUICKBOOKS_MOD_BILL', 'BillMod');
define('QUICKBOOKS_QUERY_BILL', 'BillQuery');

define('QUICKBOOKS_ADD_CHECK', 'CheckAdd');
define('QUICKBOOKS_MOD_CHECK', 'CheckMod');
define('QUICKBOOKS_QUERY_CHECK', 'CheckQuery');

define('QUICKBOOKS_ADD_DEPOSIT', 'DepositAdd');
define('QUICKBOOKS_MOD_DEPOSIT', 'DepositMod');
define('QUICKBOOKS_QUERY_DEPOSIT', 'DepositQuery');

define('QUICKBOOKS_ADD_EMPLOYEE', 'EmployeeAdd');
define('QUICKBOOKS_MOD_EMPLOYEE', 'EmployeeMod');
define('QUICKBOOKS_QUERY_EMPLOYEE', 'EmployeeQuery');

define('QUICKBOOKS_ADD_ESTIMATE', 'EstimateAdd');
define('QUICKBOOKS_MOD_ESTIMATE', 'EstimateMod');
define('QUICKBOOKS_QUERY_ESTIMATE', 'EstimateQuery');



/**
 * QuickBooks request to add a customer record
 * @var string
 */
define('QUICKBOOKS_ADD_CUSTOMER', 'CustomerAdd');

/**
 * QuickBooks request to modify a customer record
 * @var string
 */
define('QUICKBOOKS_MOD_CUSTOMER', 'CustomerMod');

/**
 * QuickBooks request to create an invoice
 * @var string
 */
define('QUICKBOOKS_ADD_INVOICE', 'InvoiceAdd');

/**
 * QuickBooks request to modify an invoice
 * @var string
 */
define('QUICKBOOKS_MOD_INVOICE', 'InvoiceMod');

/**
 * QuickBooks request to run a query for invoices
 * @var string
 */
define('QUICKBOOKS_QUERY_INVOICE', 'InvoiceQuery');

/**
 * QuickBooks request to search for/query for customer records
 * @var string
 */
define('QUICKBOOKS_QUERY_CUSTOMER', 'CustomerQuery');

/**
 * QuickBooks request to register a payment as received
 * @var string
 */
define('QUICKBOOKS_ADD_RECEIVE_PAYMENT', 'ReceivePaymentAdd');

define('QUICKBOOKS_MOD_RECEIVE_PAYMENT', 'ReceivePaymentMod');

define('QUICKBOOKS_QUERY_RECEIVE_PAYMENT', 'ReceivePaymentQuery');

define('QUICKBOOKS_ADD_PURCHASE_ORDER', 'PurchaseOrderAdd');
define('QUICKBOOKS_MOD_PURCHASE_ORDER', 'PurchaseOrderMod');
define('QUICKBOOKS_QUERY_PURCHASE_ORDER', 'PurchaseOrderQuery');

/**
 * 
 */
define('QUICKBOOKS_ADD_TIMETRACKING','TimeTrackingAdd');
define('QUICKBOOKS_MOD_TIMETRACKING','TimeTrackingMod');
define('QUICKBOOKS_QUERY_TIMETRACKING','TimeTrackingQuery');

/**
 * QuickBooks request to delete a transaction
 * @var string
 */
define('QUICKBOOKS_DEL_TRANSACTION', 'TxnDel');

define('QUICKBOOKS_QUERY_TRANSACTION', 'TransactionQuery');
define('QUICKBOOKS_VOID_TRANSACTION', 'TxnVoid');

define('QUICKBOOKS_ADD_VENDOR', 'VendorAdd');
define('QUICKBOOKS_MOD_VENDOR', 'VendorMod');
define('QUICKBOOKS_QUERY_VENDOR', 'VendorQuery');

/**
 * Queuing status for queued QuickBooks transactions - QUEUED
 * @var char
 */
define('QUICKBOOKS_STATUS_QUEUED', 'q');

/**
 * QuickBooks status for queued QuickBooks transactions - was queued, then SUCCESSFULLY PROCESSED
 * @var char
 */
define('QUICKBOOKS_STATUS_SUCCESS', 's');

/**
 * QuickBooks status for queued QuickBooks transactions - was queued, an ERROR OCCURED when processing it
 * @var char
 */
define('QUICKBOOKS_STATUS_ERROR', 'e');

/**
 * QuickBooks status for items that have been dequeued and are being processed by QuickBooks (we assume) but we havn't received a response back about them yet
 * @var char
 */
define('QUICKBOOKS_STATUS_PROCESSING', 'i');

/**
 * Error code for errors that are not really errors... this is a side-effect of QuickBooks strange "interactive mode" stuff
 * @var integer
 */
define('QUICKBOOKS_ERROR_OK', 0);

/**
 * Error code for SOAP server errors that occur internally (misc. errors)
 * @var integer
 */
define('QUICKBOOKS_ERROR_INTERNAL', -1);

/**
 * Error code for errors that occur within function handlers
 * @var integer
 */
define('QUICKBOOKS_ERROR_HANDLER', -2);

/**
 * Error code for errors that occur within driver classes
 * @var integer
 */
define('QUICKBOOKS_ERROR_DRIVER', -3);

/**
 * SOAP server for QuickBooks web services
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Server.php');

/**
 * Queue class for QuickBooks queueing 
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Queue.php');

/**
 * Various QuickBooks utility classes
 */
require_once($doc_root . '/accounting/QB/QuickBooks/Utilities.php');

?>