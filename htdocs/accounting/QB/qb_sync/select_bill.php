<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Which Vendor's Bills Would You Like To View?</title>
	</head>

	<body>
		<div align="center">
			<b>Which Vendor's Bills Would You Like To View?</b><br />
			
			<?
			$doc_root = $_SERVER["DOCUMENT_ROOT"];
			require_once($doc_root . '/accounting/QB/database/db_connect.php');
			
			// get a list of customers
			$member_query = "SELECT DISTINCT members.id, members.firstname, members.lastname FROM members, bills WHERE bills.member_id = members.member_id ORDER BY members.lastname ASC";
			$member_query_result = mysql_query($member_query) or die('Member Query failed: ' . mysql_error());
			?>
			
			<form name="view_bills" id="view_bills" action="view_bills.php" method="POST">
				<select name="member_id">
					<option value="">Select One</option>
				
					<?
					while($row = mysql_fetch_array($member_query_result)) {
						$member_id = $row[0];
						$firstname = $row[1];
						$lastname = $row[2];
						echo "<option value=\"$member_id\">$firstname $lastname</option>";
					}
					?>
				
				</select>
				<br /><br />
				<input type="submit" value="View">
			</form>
			<br />
			<a href="/accounting/QB/qb_sync/index.php">Go back to index page</a><br />
		</div>
	</body>
</html>
