<?php
	session_start();
	if (isset($_GET["logout"])) {
		unset($_SESSION["quicklogin"]);
		header("Location: ./");
	}
	if ($_POST["pass"] == "qb!cP") {
		$_SESSION["quicklogin"] = 1;
	}
	if ($_SESSION["quicklogin"] != 1) {
	?>
		<form action="<?php echo $_SERVER['PHP_SELF']?>" method="post">
			Password: <input name="pass" type="password"/>
		</form>
<?php	
		die();
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>QuickBooks Integration Test</title>
	</head>

	<body>
		<div align="center">
			<b>Quickbooks Tools</b><br />
			<br />
			<a href="/accounting/QB/qb_sync/create_customer.php">Create a Customer</a><br />
			<a href="/accounting/QB/qb_sync/create_vendor.php">Create a Vendor</a><br />
			<br />
			<a href="/accounting/QB/qb_sync/select_customer.php">Edit a Customer</a><br />
			<a href="/accounting/QB/qb_sync/select_vendor.php">Edit a Vendor</a><br />
			<br />
			<a href="/accounting/QB/qb_sync/create_bill.php">Create a Vendor Bill</a><br />
			<a href="/accounting/QB/qb_sync/select_bill.php">View Bills</a><br />
			<br />
			<a href="/accounting/QB/qb_sync/queue_primer.php">Queue Primer</a><br />
			<a href="/accounting/QB/qb_sync/queue_load.php">Queue Load Test</a><br />
			<br />
			<a href="/accounting/QB/QBLOG/show_logs.php" target="_new">Show Logs</a><br />
			<a href="/accounting/QB/qb_sync/update_quickbooks.php" target="_new">Update Quickbooks Tool</a><br />
			<a href="/accounting/QB/qb_sync/?logout=1">Logout</a><br />
		</div>
	</body>
</html>
