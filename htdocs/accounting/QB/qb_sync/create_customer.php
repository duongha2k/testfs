<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Create Customer</title>
	</head>

<?
	$id = time();

?>

	<body>
		<div align="center">
			<b>Create Customer</b><br />
			<form name="create_customer" id="create_customer" action="/accounting/QB/qb_sync/handler.php" method="post">
				<input name="action" type="hidden" value="create_customer" />
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td align="right">Client ID: &nbsp;</td>
						<td align="left"><input name="client_id" type="text" value="<? echo $id; ?>" readonly /></td>
					</tr>
					<tr>
						<td align="right">Company Name: &nbsp;</td>
						<td align="left"><input name="company_name" type="text" size="25" maxlength="41" /></td>
					</tr>
					<tr>
						<td align="right">Contact First Name: &nbsp;</td>
						<td align="left"><input name="contact_firstname" type="text" size="25" maxlength="25" /></td>
					</tr>
					<tr>
						<td align="right">Contact Last Name: &nbsp;</td>
						<td align="left"><input name="contact_lastname" type="text" size="25" maxlength="25" /></td>
					</tr>
					<tr>
						<td align="right">Contact Email Address: &nbsp;</td>
						<td align="left"><input name="contact_email" type="text" size="25" maxlength="1023" /></td>
					</tr>
					<tr>
						<td align="right">Address 1: &nbsp;</td>
						<td align="left"><input name="address1" type="text" size="25" maxlength="41" /></td>
					</tr>
					<tr>
						<td align="right">Address 2: &nbsp;</td>
						<td align="left"><input name="address2" type="text" size="25" maxlength="41" /></td>
					</tr>
					<tr>
						<td align="right">City: &nbsp;</td>
						<td align="left"><input name="city" type="text" size="25" maxlength="31" /></td>
					</tr>
					<tr>
						<td align="right">Country: &nbsp;</td>
						<td align="left"><input name="country" type="text" size="25" maxlength="31" /></td>
					</tr>
					<tr>
						<td align="right">State: &nbsp;</td>
						<td align="left"><input name="state" type="text" size="25" maxlength="21" /></td>
					</tr>
					<tr>
						<td align="right">Zip: &nbsp;</td>
						<td align="left"><input name="zip" type="text" size="13" maxlength="13" /></td>
					</tr>
					<tr>
						<td align="right">Phone: &nbsp;</td>
						<td align="left"><input name="phone" type="text" size="25" maxlength="21" /></td>
					</tr>
					<tr>
						<td align="right">Fax: &nbsp;</td>
						<td align="left"><input name="fax" type="text" size="25" maxlength="21" /></td>
					</tr>
					<tr>
						<td></td>
						<td align="left"><input type="submit" value="Create" /></td>
					</tr>
				</table>
			</form>
		</div>				
	</body>
</html>
