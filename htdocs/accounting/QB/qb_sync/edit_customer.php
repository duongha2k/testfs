<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Edit Customer</title>
	</head>

	<?
		// set doc_root
		$doc_root = $_SERVER["DOCUMENT_ROOT"];
		require_once($doc_root . '/accounting/QB/database/db_connect.php');
		
		foreach ($_POST as $var => $value) {
			$$var = $value;
			//print("$$var = $value<br />");
		}
		
		// get customer info
		$customer_query = "SELECT * FROM clients WHERE id = $customer_id";
		$record = mysql_fetch_array(mysql_query($customer_query));
		
		$id = $record['id'];
		$client_id = $record['client_id'];
		$company_name = $record['company_name'];
		$contact_firstname = $record['contact_firstname'];
		$contact_lastname = $record['contact_lastname'];
		$contact_email = $record['contact_email'];
		$address1 = $record['address1'];
		$address2 = $record['address2'];
		$city = $record['city'];
		$country = $record['country'];
		$state = $record['state'];
		$zip = $record['zip'];
		$phone = $record['phone'];
		$fax = $record['fax'];
	?>

	<body>
		<div align="center">
			Edit Customer<br />
			<br />
			<form name="edit_customer" id="edit_customer" action="/accounting/QB/qb_sync/handler.php" method="post">
				<input name="action" type="hidden" value="update_customer" />
				<input name="id" type="hidden" value="<? echo $id; ?>" />
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td align="right">Client ID: &nbsp;</td>
						<td align="left"><input name="client_id" type="text" value="<? echo $client_id; ?>" readonly /></td>
					</tr>
					<tr>
						<td align="right">Company Name: &nbsp;</td>
						<td align="left"><input name="company_name" type="text" size="25" maxlength="41" value="<? echo $company_name; ?>"/></td>
					</tr>
					<tr>
						<td align="right">Contact First Name: &nbsp;</td>
						<td align="left"><input name="contact_firstname" type="text" size="25" maxlength="25" value="<? echo $contact_firstname; ?>" /></td>
					</tr>
					<tr>
						<td align="right">Contact Last Name: &nbsp;</td>
						<td align="left"><input name="contact_lastname" type="text" size="25" maxlength="25" value="<? echo $contact_lastname; ?>" /></td>
					</tr>
					<tr>
						<td align="right">Contact Email Address: &nbsp;</td>
						<td align="left"><input name="contact_email" type="text" size="25" maxlength="1023" value="<? echo $contact_email; ?>" /></td>
					</tr>
					<tr>
						<td align="right">Address 1: &nbsp;</td>
						<td align="left"><input name="address1" type="text" size="25" maxlength="41" value="<? echo $address1; ?>" /></td>
					</tr>
					<tr>
						<td align="right">Address 2: &nbsp;</td>
						<td align="left"><input name="address2" type="text" size="25" maxlength="41" value="<? echo $address2; ?>" /></td>
					</tr>
					<tr>
						<td align="right">City: &nbsp;</td>
						<td align="left"><input name="city" type="text" size="25" maxlength="31" value="<? echo $city; ?>" /></td>
					</tr>
					<tr>
						<td align="right">Country: &nbsp;</td>
						<td align="left"><input name="country" type="text" size="25" maxlength="31" value="<? echo $country; ?>" /></td>
					</tr>
					<tr>
						<td align="right">State: &nbsp;</td>
						<td align="left"><input name="state" type="text" size="25" maxlength="21" value="<? echo $state; ?>" /></td>
					</tr>
					<tr>
						<td align="right">Zip: &nbsp;</td>
						<td align="left"><input name="zip" type="text" size="13" maxlength="13" value="<? echo $zip; ?>" /></td>
					</tr>
					<tr>
						<td align="right">Phone: &nbsp;</td>
						<td align="left"><input name="phone" type="text" size="25" maxlength="21" value="<? echo $phone; ?>" /></td>
					</tr>
					<tr>
						<td align="right">Fax: &nbsp;</td>
						<td align="left"><input name="fax" type="text" size="25" maxlength="21" value="<? echo $fax; ?>" /></td>
					</tr>
					<tr>
						<td></td>
						<td align="left"><input type="submit" value="Update" /></td>
					</tr>
				</table>
			</form>
		</div>
	</body>
</html>
