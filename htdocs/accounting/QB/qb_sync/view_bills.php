<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>View Bill</title>
	</head>
	<body>
		<div align="center">
			<?
			foreach ($_POST as $var => $value) {
				$$var = $value;
				//print("$$var = $value<br />");
			}
			
			// set doc_root
			$doc_root = $_SERVER["DOCUMENT_ROOT"];
			require_once($doc_root . '/accounting/QB/database/db_connect.php');
			
			// Get Vendor Name
			$vendor_query = "SELECT firstname, lastname FROM members WHERE id = $member_id";
			$vendor_result = mysql_fetch_array(mysql_query($vendor_query));
			$firstname = $vendor_result['firstname'];
			$lastname = $vendor_result['lastname'];
			
			// Get Bill Info
			$bill_query = "SELECT id, transaction_line_id, edit_sequence, wo_id, bill_date, pay_amount, pay_terms, is_paid, transaction_id, 
			paid_amount, paid_date, check_number, last_update, last_update_by FROM bills WHERE member_id = $member_id";
			
			$bill_result = mysql_query($bill_query);
			
			print("
				<b>View Bills for $firstname $lastname</b><br />
				<br />				
				<table cellpadding=\"5\" cellspacing=\"2\" border=\"0\" style=\"background: #CCCCCC;\">
					<tr style=\"background: #FFFFFF;\">
						<td><b>Bill ID</b></td>
						<td><b>Transaction ID</b></td>
						<td><b>Transaction Line ID</b></td>
						<td><b>Edit Sequence</b></td>
						<td><b>WO ID</b></td>
						<td><b>Bill Date</b></td>
						<td><b>Pay Amount</b></td>
						<td><b>Pay Terms</b></td>
						<td><b>Is Paid</b></td>						
						<td><b>Paid Amount</b></td>
						<td><b>Paid Date</b></td>
						<td><b>Check Number</b></td>
						<td><b>Last Update</b></td>
						<td><b>Last Update By</b></td>
					</tr>
			");
			
			while($row = mysql_fetch_array($bill_result)) {
				$id = $row['id'];
				$transaction_id = $row['transaction_id'];
				$transaction_line_id = $row['transaction_line_id'];
				$edit_sequence = $row['edit_sequence'];
				$wo_id = $row['wo_id'];
				$bill_date = $row['bill_date'];
				$pay_amount = $row['pay_amount'];
				$pay_terms = $row['pay_terms'];
				$is_paid = $row['is_paid'];				
				$paid_amount = $row['paid_amount'];
				$paid_date = $row['paid_date'];
				$check_number = $row['check_number'];
				$last_update = $row['last_update'];
				$last_update_by = $row['last_update_by'];
				
				if($is_paid == 1) {
					$is_paid = "Yes";
				}
				else {
					$is_paid = "No";
				}
				
				print("
					<tr style=\"background: #FFFFFF;\">
						<td>$id</td>
						<td>$transaction_id</td>
						<td>$transaction_line_id</td>
						<td>$edit_sequence</td>
						<td>$wo_id</td>
						<td>$bill_date</td>
						<td>$pay_amount</td>
						<td>$pay_terms</td>
						<td>$is_paid</td>						
						<td>$paid_amount</td>
						<td>$paid_date</td>
						<td>$check_number</td>
						<td>$last_update</td>
						<td>$last_update_by</td>
					</tr>
				");
			}
			
			print("</table>");
			?>
			<br />
			<a href="/accounting/QB/qb_sync/index.php">Go back to index page</a><br />
		</div>
	</body>
</html>