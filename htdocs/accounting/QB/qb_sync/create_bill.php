<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Create a Bill</title>
	</head>
	
	<?
	$doc_root = $_SERVER["DOCUMENT_ROOT"];
	require_once($doc_root . '/accounting/QB/database/db_connect.php');
	?>
	
	<body>
		<div align="center">
			<form name="create_bill" id="create_bill" action="/accounting/QB/qb_sync/handler.php" method="post">
				<input name="action" type="hidden" value="create_bill" />
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td align="right">Select a Vendor: &nbsp;</td>
						<td>
							<select name="member_id" id="member_id">
								<option value="">Select One</option>
			
								<?
									// get vendors
									$vendor_query = "SELECT id, member_id, firstname, lastname, payment_method FROM members";
									$vendor_result = mysql_query($vendor_query);
									
									while($row = mysql_fetch_row($vendor_result)) {
										$id = $row[0];
										$firstname = $row[2];
										$lastname = $row[3];
										
										echo "<option value=\"$id\">$firstname $lastname</option>";
									}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td align="right">Work Order ID: &nbsp;</td>
						<td align="left"><input name="wo_id" type="text" size="25" maxlength="20" /></td>
					</tr>
					<tr>
						<td align="right">Pay Amount: &nbsp;</td>
						<td align="left"><input name="pay_amount" type="text" size="10" maxlength="10" /></td>
					</tr>
					<tr>
						<td></td>
						<td align="left"><input type="submit" value="submit" /></td>
					</tr>
				</table>
			</form>
		</div>
	</body>
</html>
