<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Create Vendor</title>
	</head>
	
	<script type="text/javascript">
		function show_hide_ach() {
			if(document.forms['create_vendor'].payment_method.value == "Direct Deposit") {
				document.getElementById('ach').style.display = 'block';
			}
			else {
				document.getElementById('ach').style.display = 'none';
			}
		}	
	</script>
	
	<? 	$id = time(); ?>

	<body>
		<div align="center">
			<b>Create Vendor</b><br />
			<form name="create_vendor" id="create_vendor" action="/accounting/QB/qb_sync/handler.php" method="post">
				<input name="action" type="hidden" value="create_vendor" />
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td align="right">Vendor ID: &nbsp;</td>
						<td align="left"><input name="member_id" type="text" value="<? echo $id; ?>" readonly /></td>
					</tr>
					<tr>
						<td align="right">Company Name: &nbsp;</td>
						<td align="left"><input name="company_name" type="text" size="25" maxlength="41" /></td>
					</tr>
					<tr>
						<td align="right">First Name: &nbsp;</td>
						<td align="left"><input name="firstname" type="text" size="25" maxlength="25" /></td>
					</tr>
					<tr>
						<td align="right">Middle Initial: &nbsp;</td>
						<td align="left"><input name="middlename" type="text" size="3" maxlength="1" /></td>
					</tr>
					<tr>
						<td align="right">Last Name: &nbsp;</td>
						<td align="left"><input name="lastname" type="text" size="25" maxlength="25" /></td>
					</tr>
					<tr>
						<td align="right">Email Address: &nbsp;</td>
						<td align="left"><input name="email" type="text" size="25" maxlength="1023" /></td>
					</tr>
					<tr>
						<td align="right">Address 1: &nbsp;</td>
						<td align="left"><input name="address1" type="text" size="25" maxlength="41" /></td>
					</tr>
					<tr>
						<td align="right">Address 2: &nbsp;</td>
						<td align="left"><input name="address2" type="text" size="25" maxlength="41" /></td>
					</tr>
					<tr>
						<td align="right">City: &nbsp;</td>
						<td align="left"><input name="city" type="text" size="25" maxlength="31" /></td>
					</tr>
					<tr>
						<td align="right">Country: &nbsp;</td>
						<td align="left"><input name="country" type="text" size="25" maxlength="31" /></td>
					</tr>
					<tr>
						<td align="right">State: &nbsp;</td>
						<td align="left"><input name="state" type="text" size="25" maxlength="21" /></td>
					</tr>
					<tr>
						<td align="right">Zip: &nbsp;</td>
						<td align="left"><input name="zip" type="text" size="13" maxlength="13" /></td>
					</tr>
					<tr>
						<td align="right">Phone: &nbsp;</td>
						<td align="left"><input name="phone" type="text" size="25" maxlength="21" /></td>
					</tr>
					<tr>
						<td colspan="2">
							<br />
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td align="right">Payment Method: &nbsp;</td>
									<td align="left">
										<select name="payment_method" onchange="show_hide_ach();">
											<option value="">Select One</option>
											<option value="Direct Deposit">Direct Deposit</option>
											<option value="Check">Check</option>
										</select>										
									</td>
								</tr>
							</table>
							<br />
							<div name="ach" id="ach" style="display: none;">
								<table cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td align="right">Name on Bank Account: &nbsp;</td>
										<td align="left"><input name="ach_account_name" type="text" size="25" maxlength="31" /></td>
									</tr>
									<tr>
										<td align="right">Bank Name: &nbsp;</td>
										<td align="left"><input name="ach_bank_name" type="text" size="25" maxlength="31" /></td>
									</tr>
									<tr>
										<td align="right">Bank City: &nbsp;</td>
										<td align="left"><input name="ach_bank_city" type="text" size="25" maxlength="31" /></td>
									</tr>
									<tr>
										<td align="right">Bank Country: &nbsp;</td>
										<td align="left"><input name="ach_bank_country" type="text" size="25" maxlength="31" /></td>
									</tr>
									<tr>
										<td align="right">Bank State/Province: &nbsp;</td>
										<td align="left"><input name="ach_bank_state" type="text" size="25" maxlength="31" /></td>
									</tr>
									<tr>
										<td align="right">Bank Postal Code: &nbsp;</td>
										<td align="left"><input name="ach_bank_zip" type="text" size="25" maxlength="31" /></td>
									</tr>
									<tr>
										<td align="right">Bank Account Type: &nbsp;</td>
										<td align="left">
											<select name="ach_account_type">
												<option value="">Select One</option>
												<option value="Checking">Checking</option>
												<option value="Savings">Savings</option>
											</select>
										</td>
									</tr>
									<tr>
										<td align="right">Bank Routing Number: &nbsp;</td>
										<td align="left"><input name="ach_routing_number" type="text" size="25" maxlength="31" /></td>
									</tr>
									<tr>
										<td align="right">Bank Account Number: &nbsp;</td>
										<td align="left"><input name="ach_account_number" type="text" size="25" maxlength="31" /></td>
									</tr>
								</table>
								<br />
							</div>
						</tr>
					<tr>
						<td></td>
						<td align="left"><input type="submit" value="Create" /></td>
					</tr>
				</table>
			</form>
		</div>				
	</body>
</html>
