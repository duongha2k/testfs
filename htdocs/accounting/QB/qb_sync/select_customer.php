<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Select Customer to Edit</title>
	</head>

	<body>
		<div align="center">
			<b>Select a Customer to Edit</b><br />
			
			<?
			$doc_root = $_SERVER["DOCUMENT_ROOT"];
			require_once($doc_root . '/accounting/QB/database/db_connect.php');
			
			// get a list of customers
			$customer_query = "SELECT id, company_name FROM clients";
			$customer_query_result = mysql_query($customer_query) or die('Client Query failed: ' . mysql_error());
			?>
			
			<form name="update_customer" id="update_customer" action="edit_customer.php" method="POST">
				<select name="customer_id">
					<option value="">Select One</option>
				
					<?
					while($row = mysql_fetch_array($customer_query_result)) {
						$customer_id = $row[0];
						$customer_name = $row[1];
						echo "<option value=\"$customer_id\">$customer_name</option>";
					}
					?>
				
				</select>
				<br /><br />
				<input type="submit" value="Update">
			</form>
			<br />
			<a href="/accounting/QB/qb_sync/index.php">Go back to index page</a><br />
		</div>
	</body>
</html>
