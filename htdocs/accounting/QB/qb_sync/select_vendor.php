<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Select Vendor  to Edit</title>
	</head>

	<body>
		<div align="center">
			<b>Select a Vendor to Edit</b><br />
			
			<?
			$doc_root = $_SERVER["DOCUMENT_ROOT"];
			require_once($doc_root . '/accounting/QB/database/db_connect.php');
			
			// get a list of customers
			$member_query = "SELECT id, firstname, lastname FROM members ORDER BY lastname";
			$member_query_result = mysql_query($member_query) or die('Member Query failed: ' . mysql_error());
			?>
			
			<form name="edit_vendor" id="edit_vendor" action="edit_vendor.php" method="POST">
				<select name="member_id">
					<option value="">Select One</option>
				
					<?
					while($row = mysql_fetch_array($member_query_result)) {
						$member_id = $row[0];
						$firstname = $row[1];
						$lastname = $row[2];
						echo "<option value=\"$member_id\">$firstname $lastname</option>";
					}
					?>
				
				</select>
				<br /><br />
				<input type="submit" value="Update">
			</form>
			<br />
			<a href="/accounting/QB/qb_sync/index.php">Go back to index page</a><br />
		</div>
	</body>
</html>
