<?php
	
	// set some vars
	$doc_root = $_SERVER["DOCUMENT_ROOT"];
	$time = time();
	$last_update = date('Y-m-d G:i:s', $time);
	$do_logs = true;
	
	// Require the framework
	require_once($doc_root . '/accounting/QB/QuickBooks/Server.php');
	require_once($doc_root . '/accounting/QB/database/db_connect.php');
	require_once($doc_root . '/accounting/QB/QuickBooks/Queue.php');
	require_once($doc_root . '/library/openSSL.php');
	
	// Map QuickBooks actions to handler functions
	$map = array(
		'CustomerAdd' => array('customer_add_request', 'customer_add_response'), 
		'CustomerMod' => array('customer_mod_request', 'customer_mod_response'), 
		'CustomerUpdateEditSequence' => array('customer_update_edit_sequence_request', 'customer_update_edit_sequence_response'), 
		'VendorAdd' => array('vendor_add_request', 'vendor_add_response'), 	
		'VendorMod' => array('vendor_mod_request', 'vendor_mod_response'), 
		'VendorUpdateEditSequence' => array('vendor_update_edit_sequence_request', 'vendor_update_edit_sequence_response'), 
		'DataExtMod_Vendor' => array('vendor_dataext_mod_request', 'vendor_dataext_mod_response'), 
		'BillAdd' => array('bill_add_request', 'bill_add_response'), 
		'BillQuery' => array('bill_query_request', 'bill_query_response'), 
		'VendorQuery' =>array('vendor_query_request', 'vendor_query_response'), 
		);
	
	// This is entirely optional, use it to trigger actions when an error is returned by QuickBooks
	$errmap = array(
		'!' => 'report_quickbooks_errors',
		);
	
	$soap_options = array();	// See http://www.php.net/soap
	
	$handler_options = array();	// See the comments in the QuickBooks/Server/Handlers.php file
	$driver_options = array('log_level' => QUICKBOOKS_LOG_DEBUG, );
	
	$server = new QuickBooks_Server('mysql://quickbooks:DevTeam2007$@localhost/quickbooks', $map, $errmap, QUICKBOOKS_WSDL, $soap_options, $handler_options, $driver_options);
	$server->handle();
	
	// Customer Add Request
	function customer_add_request($requestID, $action, $ID, $extra, &$err) {
		global $last_update;
		global $do_logs;
		
		// Get the customer information from the MySQL databse
		$query = "SELECT * FROM clients WHERE id = $ID";
		if($result = mysql_query($query)) {
			if($record = mysql_fetch_array($result)) {
				$client_id = htmlentities($record['client_id']);
				$company_name = htmlentities($record['company_name']);
				$contact_firstname = htmlentities($record['contact_firstname']);
				$contact_lastname = htmlentities($record['contact_lastname']);
				$contact_email = htmlentities($record['contact_email']);
				$address1 = htmlentities($record['address1']);
				$address2 = htmlentities($record['address2']);
				$city = htmlentities($record['city']);
				$country = htmlentities($record['country']);
				$state = htmlentities($record['state']);
				$zip = htmlentities($record['zip']);
				$phone = htmlentities($record['phone']);
				$fax = htmlentities($record['fax']);
				
				// Create the qbXML request
				$qbxml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
					<?qbxml version=\"7.0\"?>
					<QBXML>
						<QBXMLMsgsRq onError=\"stopOnError\">
							<CustomerAddRq requestID=\"$requestID\">
								<CustomerAdd>
									<Name>$company_name</Name>
									<CompanyName>$company_name</CompanyName>
									<FirstName>$contact_firstname</FirstName>
									<LastName>$contact_lastname</LastName>
									<BillAddress>
										<Addr1>$address1</Addr1>
										<Addr2>$address2</Addr2>
										<City>$city</City>
										<State>$state</State>
										<PostalCode>$zip</PostalCode>
										<Country>$country</Country>
									</BillAddress>
									<Phone>$phone</Phone>
									<Fax>$fax</Fax>
									<Email>$contact_email</Email>
									<Contact>$contact_firstname $contact_lastname</Contact>
									<AccountNumber>$client_id</AccountNumber>
								</CustomerAdd>
							</CustomerAddRq>
						</QBXMLMsgsRq>
					</QBXML>";
				
				// Log the created QBXML			
				if($do_logs == true) {
					do_log("CustomerAddRequest_" . $requestID . ".xml", $qbxml);
				}
				
				// Return the QBXML that will be sent to QuickBooks.
				return $qbxml;
			}
			else {
				// mysql_fetch_array returned false, there are no records that match the query
				$error = $last_update . "\r\r";
				$error .= "Queue existed for a client that is not in the database.\r\r";
				$error .= "Request ID = $RequestID\r";
				$error .= "Client id = $ID\r\r";
				
				log_error("CustomerAddRequestError_" . $requestID . ".txt", $error);
				mail_error("QB - MySQL Customer Add Request Error", $error);
				
				// return an error to QBWC
				$err = "CustomerAddRequest: There is not a client with the id of $ID in the MySQL database.  Request ID: $requestID";
			}
		}
		else {
			//Query Failed
			$error = $last_update . "\r\r";
			$error .= "MySQL Error Querying clients table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=CustomerAddRequestError_" . $requestID . ".txt\r\r";
			$error .= "Request ID = $requestID\r";
			$error .= "Client ID = $ID\r\r";			
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			
			log_error("CustomerAddRequestError_" . $requestID . ".txt", $error);
			mail_error('QB - MySQL Customer Add Request Error', $error);
			
			// return an error to QBWC
			$err = "CustomerAddRequest: The MySQL query failed for request number $requestID, Client ID $ID";
		}
	}
	
	// Customer Add Response
	function customer_add_response($requestID, $action, $ID, $extra, &$err, $xml, $idents) {
		global $last_update;
		global $do_logs;
		
		// Log the response XML
		if($do_logs == true) {
			do_log("CustomerAddResponse_" . $requestID . ".xml", $xml);
		}
		
		// Get info from the response XML
		$parse_xml = new SimpleXMLElement($xml);
		$list_id = $parse_xml->QBXMLMsgsRs->CustomerAddRs->CustomerRet->ListID;
		$edit_sequence = $parse_xml->QBXMLMsgsRs->CustomerAddRs->CustomerRet->EditSequence;
				
		// Create the query and put the info into the MySQL database
		$query = "UPDATE clients SET list_id = '$list_id', edit_sequence = '$edit_sequence', last_update = '$last_update', 
			last_update_by = 'QuickBooks' WHERE id = $ID";
		
		$result = mysql_query($query);
		
		// unalble to update mysql DB
		if(!$result) {
			$error = $last_update . "\r\r";
			$error .= "MySQL error updating clients table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=CustomerAddResponseError_" . $requestID . ".txt\r\r";
			$error .= "Request ID = $RequestID\r";
			$error .= "Client id = $ID\r\r";
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			$error .= "Response XML:\r" . $xml;
			
			log_error("CustomerAddResponseError_" . $requestID . ".txt", $error);
			mail_error("QB - MySQL Customer Add Response Error", $error);
			
			// return an error to QBWX
			$err = "CustomerAddResponse: A MySQL error occured.  Unable to update clients table where clients.id = $ID";
		}
	}
	
	// Customer Mod Request
	function customer_mod_request($requestID, $action, $ID, $extra, &$err) {
		global $last_update;
		global $do_logs;
		
		// Get the customer information from the MySQL database
		$query = "SELECT * FROM clients WHERE id = $ID";		
		if($result = mysql_query($query)) {		
			if($record = mysql_fetch_array($result)) {
				$list_id = htmlentities($record['list_id']);
				$edit_sequence = htmlentities($record['edit_sequence']);
				$client_id = htmlentities($record['client_id']);
				$company_name =htmlentities( $record['company_name']);
				$contact_firstname = htmlentities($record['contact_firstname']);
				$contact_lastname = htmlentities($record['contact_lastname']);
				$contact_email = htmlentities($record['contact_email']);
				$address1 = htmlentities($record['address1']);
				$address2 = htmlentities($record['address2']);
				$city = htmlentities($record['city']);
				$country = htmlentities($record['country']);
				$state = htmlentities($record['state']);
				$zip = htmlentities($record['zip']);
				$phone = htmlentities($record['phone']);
				$fax = htmlentities($record['fax']);
				
				// Create the qbXML request
				$qbxml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
					<?qbxml version=\"7.0\"?>
					<QBXML>
						<QBXMLMsgsRq onError=\"stopOnError\">
							<CustomerModRq requestID=\"$requestID\">
								<CustomerMod>
									<ListID>$list_id</ListID>
									<EditSequence>$edit_sequence</EditSequence>
									<Name>$company_name</Name>
									<CompanyName>$company_name</CompanyName>
									<FirstName>$contact_firstname</FirstName>
									<LastName>$contact_lastname</LastName>
									<BillAddress>
										<Addr1>$address1</Addr1>
										<Addr2>$address2</Addr2>
										<City>$city</City>
										<State>$state</State>
										<PostalCode>$zip</PostalCode>
										<Country>$country</Country>
									</BillAddress>
									<Phone>$phone</Phone>
									<Fax>$fax</Fax>
									<Email>$contact_email</Email>
									<Contact>$contact_firstname $contact_lastname</Contact>
									<AccountNumber>$client_id</AccountNumber>
								</CustomerMod>
							</CustomerModRq>
						</QBXMLMsgsRq>
					</QBXML>";
					
				// Log the created QBXML
				if($do_logs == true) {
					do_log("CustomerModRequest_" . $requestID . ".xml", $qbxml);
				}
				
				// Return the QBXML that will be sent to QuickBooks.
				return $qbxml;
			}
			else {
				// mysql_fetch_array returned false, there are no records that match the query
				$error = $last_update . "\r\r";
				$error .= "Queue existed for a client that is not in the database.\r\r";
				$error .= "Request ID = $requestID\r\r";
				$error .= "Client id = $ID\r\r";
				
				log_error("CustomerModRequestError_" . $requestID . ".txt", $error);
				mail_error("QB - MySQL Customer Mod Request Error", $error);
				
				// return an error to QBWX
				$err = "CustomerModRequest: There is not a client with the id of $ID in the MySQL database";
			}
		}
		else {
			// Query failed
			$error = $last_update . "\r\r";
			$error .= "MySQL Error Querying clients table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=CustomerModRequestError_" . $requestID . ".txt\r\r";
			$error .= "Request ID = $requestID\r";
			$error .= "Client ID = $ID\r\r";			
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			
			log_error("CustomerModRequestError_" . $requestID . ".txt", $error);
			mail_error('QB - MySQL Customer Add Request Error', $error);
			
			// return an error to QBWC
			$err = "CustomerModRequest: The MySQL query failed for request number $requestID, Client ID $ID";
		}
	}
	
	// Customer Mod Response
	function customer_mod_response($requestID, $action, $ID, $extra, &$err, $xml, $idents) {
		global $last_update;
		global $do_logs;
		
		// Log the response XML
		if($do_logs == true) {
			do_log("CustomerModResponse_" . $requestID . ".xml", $xml);
		}
		
		// Get info from the response XML
		$parse_xml = new SimpleXMLElement($xml);
		$list_id = $parse_xml->QBXMLMsgsRs->CustomerModRs->CustomerRet->ListID;
		$edit_sequence = $parse_xml->QBXMLMsgsRs->CustomerModRs->CustomerRet->EditSequence;
		
		// Create the query and put the info into the MySQL database
		$query = "UPDATE clients SET edit_sequence = '$edit_sequence', last_update = '$last_update', last_update_by = 'QuickBooks' WHERE id = $ID";
		$result = mysql_query($query);
		
		// Report errors in a log file
		if(!$result) {
			$error = $last_update . "\r\r";
			$error .= "MySQL error updating clients table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=CustomerModResponseError_" . $requestID . ".txt\r\r";
			$error .= "Request ID = $RequestID\r";
			$error .= "Client id = $ID\r\r";
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			$error .= "Response XML:\r" . $xml;
			
			log_error("CustomerModResponseError_" . $requestID . ".txt", $error);
			mail_error("QB - MySQL Customer Mod Response Error", $error);
			
			// return an error to QBWX
			$err = "CustomerModResponse: A MySQL error occured.  Unable to update clients table where clients.id = $ID";
		}
	}
	
	// Customer Update Edit Sequence Request
	function customer_update_edit_sequence_request($requestID, $action, $ID, $extra, &$err) {
		global $last_update;
		global $do_logs;
		
		// get the vendors list_id from MySQL
		$query = "SELECT list_id FROM clients WHERE id = $ID";
		if($result = mysql_query($query)) {		
			if($record = mysql_fetch_array($result)) {
				$list_id = htmlentities($record['list_id']);
				
				// Create the qbXML request
				$qbxml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
					<?qbxml version=\"7.0\"?>
					<QBXML>
						<QBXMLMsgsRq onError=\"stopOnError\"> 
							<CustomerQueryRq requestID=\"$requestID\"> 
								<ListID>$list_id</ListID>
							</CustomerQueryRq>
						</QBXMLMsgsRq>
					</QBXML>";
				
				// Log the created QBXML
				if($do_logs == true) {
					do_log("CustomerUpdateEditSequenceRequest_" . $requestID . ".xml", $qbxml);
				}
								
				// Return the QBXML that will be sent to QuickBooks.
				return $qbxml;
			}
			else {
				// mysql_fetch_array returned false, there are no records that match the query
				$error = $last_update . "\r\r";
				$error .= "Queue existed for a client that is not in the database.\r\r";
				$error .= "Request ID = $requestID\r\r";
				$error .= "Client id = $ID\r\r";
				
				log_error("CustomerUpdateEditSequenceRequestError_" . $requestID . ".txt", $error);
				mail_error("QB - MySQL Customer Update Edit Sequence Request Error", $error);
				
				// return an error to QBWX
				$err = "CustomerUpdateEditSequenceRequest: There is not a client with the id of $ID in the MySQL database";
			}
		}
		else {
			// Query failed
			$error = $last_update . "\r\r";
			$error .= "MySQL Error Querying clients table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=CustomerUpdateEditSequenceRequestError_" . $requestID . ".txt\r\r";
			$error .= "Request ID = $requestID\r";
			$error .= "Client ID = $ID\r\r";			
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			
			log_error("CustomerUpdateEditSequenceRequestError_" . $requestID . ".txt", $error);
			mail_error('QB - MySQL Customer Update Edit Sequence Request Error', $error);
			
			// return an error to QBWC
			$err = "CustomerUpdateEditSequenceRequest: The MySQL query failed for request number $requestID, Client ID $ID";
		}
	}
	
	// Customer Update Edit Sequence Response
	function customer_update_edit_sequence_response($requestID, $action, $ID, $extra, &$err, $xml, $idents) {
		global $last_update;
		global $do_logs;
		
		// Log the response XML
		if($do_logs == true) {
			do_log("CustomerUpdateEditSequenceResponse_" . $requestID . ".xml", $xml);
		}
		
		// Get info from the response XML
		$parse_xml = new SimpleXMLElement($xml);
		$list_id = $parse_xml->QBXMLMsgsRs->CustomerQueryRs->CustomerRet->ListID;
		$edit_sequence = $parse_xml->QBXMLMsgsRs->CustomerQueryRs->CustomerRet->EditSequence;
		
		// Create the query and put the info into the MySQL database
		$query = "UPDATE clients SET edit_sequence = '$edit_sequence', last_update = '$last_update', last_update_by = 'QuickBooks' WHERE id = $ID";
		$result = mysql_query($query);
		
		// Report errors in a log file
		if(!$result) {
			$error = $last_update . "\r\r";
			$error .= "MySQL error updating clients table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=CustomerUpdateEditSequenceResponseError_" . $requestID . ".txt\r\r";
			$error .= "Request ID = $RequestID\r";
			$error .= "Client id = $ID\r\r";
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			$error .= "Response XML:\r" . $xml;
			
			log_error("CustomerUpdateEditSequenceResponseError_" . $requestID . ".txt", $error);
			mail_error("QB - MySQL Customer Update Edit Sequence Response Error", $error);
			

			// return an error to QBWX
			$err = "CustomerUpdateEditSequenceReponse: A MySQL error occured.  Unable to update clients table where clients.id = $ID";
		}
	}
	
	// Vendor Add Request
	function vendor_add_request($requestID, $action, $ID, $extra, &$err) {
		global $last_update;
		global $do_logs;
		
		// Get the vendor information from the MySQL database
		$query = "SELECT * FROM members WHERE id = $ID";
		if($result = mysql_query($query)) {
			if($record = mysql_fetch_array($result)) {
				$member_id = htmlentities($record['member_id']);
				$company_name = trim($record['company_name']);
				$company_name = ($company_name == "NULL" ? "" : $company_name);
				$w9_company_name = trim($record['w9_company_name']);
				$which_as_taxid = $record['which_as_taxid'];
				if ($which_as_taxid == 2)
				// EIN selected
					$company_name = $w9_company_name;
				if ($which_as_taxid == 0)
					// no taxid has been set
					$taxid = "";
				else
					$taxid = ($which_as_taxid == 1 ? $record['ssn'] : $record['ein']);
				$company_name = htmlentities($company_name);
				$firstname = htmlentities($record['firstname']);
				$middlename = htmlentities($record['middlename']);
				$lastname = htmlentities($record['lastname']);
				$phone = htmlentities($record['phone']);
				$email = htmlentities($record['email']);
				$address1 = htmlentities($record['address1']);
				$address2 = htmlentities($record['address2']);
				$city = htmlentities($record['city']);
				$country = htmlentities($record['country']);
				$state = htmlentities($record['state']);
				$zip = htmlentities($record['zip']);
				$payment_method = htmlentities($record['payment_method']);	


				/*******************************************/
				// Decrypt W9 info
				
				// get password
				$filename = "/tmp/lexiconjob.txt";
				$handle = fopen($filename, "r");
				$contents = fread($handle, filesize($filename));
				fclose($handle);

				$password = unserialize($contents);

				$privateKey = getBankingPrivateKey($password);
				
				if (!$privateKey) {
					$error = $last_update . "\r\r";
					$error .= "Invalid password for TaxID.\r\r";
					$error .= "Request ID = $requestID\r\r";
					$error .= "Member ID = $ID\r\r";
					
					log_error("VendorAddRequestError_" . $requestID . ".txt", $error);
					mail_error("QB - MySQL Vendor Add Request Error", $error);
					
					// return an error to QBWX
					$err = "VendorAddRequest: Invalid password for decrypting TaxID";
				}
				else {
					if($taxid && $taxid != "") {
						$taxid = htmlentities(substr(decryptData($taxid, $privateKey), 0, 15)); // decodes encrypted info
					}
				}
				
				if ($taxid == "")
					// default taxid
					$taxid = ($which_as_taxid == 2 ? "00-0000000" : "000-00-0000");
				
				// Create the qbXML request
				$qbxml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
					<?qbxml version=\"7.0\"?>
					<QBXML>
						<QBXMLMsgsRq onError=\"stopOnError\">
							<VendorAddRq requestID=\"$requestID\"> 
								<VendorAdd>
									<Name>$member_id</Name>
									<CompanyName>$company_name</CompanyName>
									<FirstName>$firstname</FirstName>
									<MiddleName>$middlename</MiddleName>
									<LastName>$lastname</LastName>
									<VendorAddress>
										<Addr1>$firstname $lastname</Addr1>
										<Addr2>$address1</Addr2>
										<Addr3>$address2</Addr3>
										<City>$city</City>
										<State>$state</State>
										<PostalCode>$zip</PostalCode>
										<Country>$country</Country>
									</VendorAddress>
									<Phone>$phone</Phone>
									<Email>$email</Email>
									<Contact>$firstname ";
										if($middlename != "") {
											$qbxml .= "$middlename ";
										}
										
										$qbxml .= "$lastname</Contact>
									<NameOnCheck>";
									
										if(($which_as_taxid == 0 && $company_name != "") || ($which_as_taxid == 2)) {
											// if (neither SSN or EIN has been selected as taxid AND the tech profile company name is not blank) OR (EIN was selected as taxid)
											$qbxml .= "$company_name";
										}
										else {
											$qbxml .= "$firstname ";
											
											if($middlename != "") {
												$qbxml .= "$middlename ";
											}
											
											$qbxml .= "$lastname";
										}
										
									$qbxml .= "</NameOnCheck>
									<VendorTypeRef>
										<FullName>TB Tech</FullName>
									</VendorTypeRef>
									";
										
										if($payment_method == "Direct Deposit") {
											$qbxml .= "
												<TermsRef>
													<FullName>ACH</FullName>
												</TermsRef>
											";
										}
										
									$qbxml .= "
									<VendorTaxIdent>$taxid</VendorTaxIdent>
									<IsVendorEligibleFor1099>true</IsVendorEligibleFor1099>
								</VendorAdd>
							</VendorAddRq>
						</QBXMLMsgsRq>
					</QBXML>";
				
				// Log the created QBXML
				if($do_logs == true) {
					do_log("VendorAddRequest_" . $requestID . ".xml", $qbxml);
				}
				
				// Return the QBXML that will be sent to QuickBooks.
				return $qbxml;
			}
			else {
				// mysql_fetch_array returned false, there are no records that match the query
				$error = $last_update . "\r\r";
				$error .= "Queue existed for a vendor that is not in the database.\r\r";
				$error .= "Request ID = $requestID\r\r";
				$error .= "Member ID = $ID\r\r";
				
				log_error("VendorAddRequestError_" . $requestID . ".txt", $error);
				mail_error("QB - MySQL Vendor Add Request Error", $error);
				
				// return an error to QBWX
				$err = "VendorAddRequest: There is not a member with the id of $ID in the MySQL database";
			}
		}
		else {
			// Query failed
			$error = $last_update . "\r\r";
			$error .= "MySQL Error Querying members table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=VendorAddRequestError_" . $requestID . ".txt\r\r";
			$error .= "Request ID = $requestID\r";
			$error .= "Member ID = $ID\r\r";			
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			
			log_error("VendorAddRequestError_" . $requestID . ".txt", $error);
			mail_error('QB - MySQL Vendor Add Request Error', $error);
			
			// return an error to QBWC
			$err = "VendorAddRequest: The MySQL query failed for request number $requestID, Member ID $ID";
		}
	}
	
	// Vendor Add Response
	function vendor_add_response($requestID, $action, $ID, $extra, &$err, $xml, $idents) {
		global $last_update;
		global $do_logs;
		
		// Log the response XML
		if($do_logs == true) {
			do_log("VendorAddResponse_" . $requestID . ".xml", $xml);
		}
		
		// Get info from the response XML
		$parse_xml = new SimpleXMLElement($xml);
		$list_id = $parse_xml->QBXMLMsgsRs->VendorAddRs->VendorRet->ListID;
		$edit_sequence = $parse_xml->QBXMLMsgsRs->VendorAddRs->VendorRet->EditSequence;
		
		// Create the query and put the info into the MySQL database
		$query = "UPDATE members SET list_id = '$list_id', edit_sequence = '$edit_sequence', last_update = '$last_update', last_update_by = 'QuickBooks' WHERE id = $ID";
		$result = mysql_query($query);
		
		// Report errors in a log file
		if(!$result) {
			$error = $last_update . "\r\r";
			$error .= "MySQL error updating members table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=VendorAddResponseError_" . $requestID . ".txt\r\r";
			$error .= "Request ID = $RequestID\r";
			$error .= "Member ID = $ID\r\r";
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			$error .= "Response XML:\r" . $xml;
			
			log_error("VendorAddResponseError_" . $requestID . ".txt", $error);
			mail_error("QB - MySQL Vendor Add Response Error", $error);
			
			// return an error to QBWX
			$err = "VendorAddReponse: A MySQL error occured.  Unable to update members table where members.id = $ID";
		}
		
		// update num_new_techs in the quickbooks_ticket table - 
		// There is one transaction ID per sync so this is an ideal place to keep this kind of data
		// get the ticket id
		$ticket_query = "SELECT quickbooks_ticket_id FROM quickbooks_queue WHERE qb_action = 'VendorAdd' AND ident = '$ID'";
		if($ticket_result = mysql_query($ticket_query)) {
			if($ticket_record = mysql_fetch_array($ticket_result)) {
				$ticket_id = $ticket_record['quickbooks_ticket_id'];
				
				$total_query = "UPDATE quickbooks_ticket SET num_new_techs = num_new_techs + 1 WHERE quickbooks_ticket_id = $ticket_id";
				$total_result = mysql_query($total_query);
				
				// Report errors in a log file	
				if(!$total_result) {
					$error = $last_update . "\r\r";
					$error .= "MySQL error updating quickbooks_ticket table - to update num_new_techs\r\r";
					$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=TicketUpdateNumNewTechsError_" . $requestID . ".txt\r\r";
					$error .= "Request ID = $RequestID\r";
					$error .= "Tech ID = $ID\r\r";
					$error .= "MySQL Error: " . mysql_error() . "\r\r";
					$error .= "MySQL Query: " . $total_query . "\r\r";
					$error .= "Response XML:\r" . $xml;
					
					log_error("TicketUpdateNumNewTechsError_" . $requestID . ".txt", nl2br(htmlentities($error)));
					mail_error("QB - Ticket Update NumNewTechs Error", $error);
				}
			}
			else {
				// mysql_fetch_array returned false, there are no records that match the query
				$error = $last_update . "\r\r";
				$error .= "MySQL could not find a record that matches the query while updating the ticket for num_new_techs.\r\r";
				$error .= "Request ID = $requestID\r\r";
				$error .= "Tech ID = $ID\r\r";
				$error .= "Query: $ticket_query\r\r";
				
				log_error("VendorAddResponseError_" . $requestID . ".txt", $error);
				mail_error("QB - MySQL Vendor Add Request Error", $error);
				
				// return an error to QBWX
				$err = "VendorAddResponse: MySQL could not find a record that matches the query while updating the ticket for num_new_techs";
			}
		}
		else {
			// Query failed
			$error = $last_update . "\r\r";
			$error .= "MySQL Error Querying quickbooks_queue table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=VendorAddRequestError_" . $requestID . ".txt\r\r";
			$error .= "Request ID = $requestID\r";
			$error .= "Member ID = $ID\r\r";			
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			$error .= "MySQL Query: " . $ticket_query . "\r\r";
			
			log_error("VendorAddResponseError_" . $requestID . ".txt", $error);
			mail_error('QB - MySQL Vendor Add Response Error', $error);
			
			// return an error to QBWC
			$err = "VendorAddResponse: The MySQL query failed for request number $requestID, Member ID $ID";
		}
	}
	
	// Vendor Mod Request
	function vendor_mod_request($requestID, $action, $ID, $extra, &$err) {
		global $last_update;
		global $do_logs;
		
		// Get the vendor information from the MySQL database
		$query = "SELECT * FROM members WHERE id = $ID";
		if($result = mysql_query($query)) {
			if($record = mysql_fetch_array($result)) {
				// Set variables
				$list_id = htmlentities($record['list_id']);
				$edit_sequence = htmlentities($record['edit_sequence']);
				$member_id = htmlentities($record['member_id']);
				$company_name = trim($record['company_name']);
				$company_name = ($company_name == "NULL" ? "" : $company_name);
				$w9_company_name = trim($record['w9_company_name']);
				$which_as_taxid = $record['which_as_taxid'];
				if ($which_as_taxid == 2)
				// EIN selected
					$company_name = $w9_company_name;
				if ($which_as_taxid == 0)
					// no taxid has been set
					$taxid = "";
				else
					$taxid = ($which_as_taxid == 1 ? $record['ssn'] : $record['ein']);
				$company_name = htmlentities($company_name);
				$firstname = htmlentities($record['firstname']);
				$middlename = htmlentities($record['middlename']);
				$lastname = htmlentities($record['lastname']);
				$phone = htmlentities($record['phone']);
				$email = htmlentities($record['email']);
				$address1 = htmlentities($record['address1']);
				$address2 = htmlentities($record['address2']);
				$city = htmlentities($record['city']);
				$country = htmlentities($record['country']);
				$state = htmlentities($record['state']);
				$zip = htmlentities($record['zip']);
				$payment_method = htmlentities($record['payment_method']);
				

				/*******************************************/
				// Decrypt W9 info
				
				// get password
				$filename = "/tmp/lexiconjob.txt";
				$handle = fopen($filename, "r");
				$contents = fread($handle, filesize($filename));
				fclose($handle);

				$password = unserialize($contents);

				$privateKey = getBankingPrivateKey($password);
				
				if (!$privateKey) {
					$error = $last_update . "\r\r";
					$error .= "Invalid password for TaxID.\r\r";
					$error .= "Request ID = $requestID\r\r";
					$error .= "Member ID = $ID\r\r";
					
					log_error("VendorModRequestError_" . $requestID . ".txt", $error);
					mail_error("QB - MySQL Vendor Mod Request Error", $error);
					
					// return an error to QBWX
					$err = "VendorModRequest: Invalid password for decrypting TaxID";
				}
				else {
					if($taxid && $taxid != "") {
						$taxid = htmlentities(substr(decryptData($taxid, $privateKey), 0, 15)); // decodes encrypted info
					}
				}
				
				if ($taxid == "")
					// default taxid
					$taxid = ($which_as_taxid == 2 ? "00-0000000" : "000-00-0000");

				// Create the qbXML request
				$qbxml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
					<?qbxml version=\"7.0\"?>
					<QBXML>
						<QBXMLMsgsRq onError=\"stopOnError\">
							<VendorModRq requestID=\"$requestID\"> 
								<VendorMod>
									<ListID>$list_id</ListID>
									<EditSequence>$edit_sequence</EditSequence>
									<Name>$member_id</Name>
									<CompanyName>$company_name</CompanyName>
									<FirstName>$firstname</FirstName>
									<MiddleName>$middlename</MiddleName>
									<LastName>$lastname</LastName>
									<VendorAddress>
										<Addr1>$firstname $lastname</Addr1>
										<Addr2>$address1</Addr2>
										<Addr3>$address2</Addr3>
										<City>$city</City>
										<State>$state</State>
										<PostalCode>$zip</PostalCode>
										<Country>$country</Country>
									</VendorAddress>
									<Phone>$phone</Phone>
									<Email>$email</Email>
									<Contact>$firstname ";
										if($middlename != "") {
											$qbxml .= "$middlename ";
										}
										
										$qbxml .= "$lastname</Contact>
									<NameOnCheck>";
										
											if(($which_as_taxid == 0 && $company_name != "") || ($which_as_taxid == 2)) {
												// if (neither SSN or EIN has been selected as taxid AND the tech profile company name is not blank) OR (EIN was selected as taxid)
												$qbxml .= "$company_name";
											}
											else {
												$qbxml .= "$firstname ";
												
												if($middlename != "") {
													$qbxml .= "$middlename ";
												}
												
												$qbxml .= "$lastname";
											}
											
										$qbxml .= "</NameOnCheck>";
											
										if($payment_method == "Direct Deposit") {
											$qbxml .= "
												<TermsRef>
													<FullName>ACH</FullName>
												</TermsRef>
											";
										}
										else {
											$qbxml .= "
												<TermsRef>
													<FullName></FullName>
												</TermsRef>
											";
										}
											
										$qbxml .= "
								</VendorMod>
							</VendorModRq>
						</QBXMLMsgsRq>
					</QBXML>";
				
				// Log the created QBXML
				if($do_logs == true) {
					do_log("VendorModRequest_" . $requestID . ".xml", $qbxml);
				}
				
				// Return the QBXML that will be sent to QuickBooks.
				return $qbxml;
			}
			else {
				// mysql_fetch_array returned false, there are no records that match the query
				$error = $last_update . "\r\r";
				$error .= "Queue existed for a member that is not in the mysql database.\r\r";
				$error .= "Request ID = $requestID\r\r";
				$error .= "Member ID = $ID\r\r";
				
				log_error("VendorModRequestError_" . $requestID . ".txt", $error);
				mail_error("QB - MySQL Vendor Mod Request Error", $error);
				
				// return an error to QBWX
				$err = "VendorModRequest: There is not a member with the id of $ID in the MySQL database";
			}
		}
		else {
			// Query failed
			$error = $last_update . "\r\r";
			$error .= "MySQL Error Querying members table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=VendorModRequestError_" . $requestID . ".txt\r\r";
			$error .= "Request ID = $requestID\r";
			$error .= "Member ID = $ID\r\r";			
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			
			log_error("VendorModRequestError_" . $requestID . ".txt", $error);
			mail_error('QB - MySQL Vendor Mod Request Error', $error);
			
			// return an error to QBWC
			$err = "VendorModRequest: The MySQL query failed for request number $requestID, Member ID $ID";
		}
	}
	
	// Vendor Mod Response
	function vendor_mod_response($requestID, $action, $ID, $extra, &$err, $xml, $idents) {	
		global $last_update;
		global $do_logs;
		
		// Log the response XML
		if($do_logs == true) {
			do_log("VendorModResponse_" . $requestID . ".xml", $xml);
		}
		
		// Get info from the response XML
		$parse_xml = new SimpleXMLElement($xml);
		$list_id = $parse_xml->QBXMLMsgsRs->VendorModRs->VendorRet->ListID;
		$edit_sequence = $parse_xml->QBXMLMsgsRs->VendorModRs->VendorRet->EditSequence;
		
		// Create the query and put the info into the MySQL database
		$query = "UPDATE members SET edit_sequence = '$edit_sequence', last_update = '$last_update', last_update_by = 'QuickBooks' WHERE id = $ID";
		$result = mysql_query($query);
		
		// Report errors in a log file
		if(!$result) {
			$error = $last_update . "\r\r";
			$error .= "MySQL error updating members table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=VendorModResponseError_" . $requestID . ".txt\r\r";
			$error .= "Request ID = $RequestID\r";
			$error .= "Member ID = $ID\r\r";
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			$error .= "Response XML:\r" . $xml;
			
			log_error("VendorModResponseError_" . $requestID . ".txt", $error);
			mail_error("QB - MySQL Vendor Mod Response Error", $error);
			
			// return an error to QBWX
			$err = "VendorNModReponse: A MySQL error occured.  Unable to update members table where members.id = $ID";
		}
		
		// update num_mod_techs in the quickbooks_ticket table - 
		// There is one transaction ID per sync so this is an ideal place to keep this kind of data
		// get the ticket id
		$ticket_query = "SELECT quickbooks_ticket_id FROM quickbooks_queue WHERE qb_action = 'VendorMod' AND ident = '$ID'";
		if($ticket_result = mysql_query($ticket_query)) {
			if($ticket_record = mysql_fetch_array($ticket_result)) {
				$ticket_id = $ticket_record['quickbooks_ticket_id'];
				
				$total_query = "UPDATE quickbooks_ticket SET num_mod_techs = num_mod_techs + 1 WHERE quickbooks_ticket_id = $ticket_id";
				$total_result = mysql_query($total_query);
				
				// Report errors in a log file	
				if(!$total_result) {
					$error = $last_update . "\r\r";



					$error .= "MySQL error updating quickbooks_ticket table - to update num_mod_techs\r\r";
					$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=TicketUpdateNumModTechsError_" . $requestID . ".txt\r\r";
					$error .= "Request ID = $RequestID\r";
					$error .= "Tech ID = $ID\r\r";
					$error .= "MySQL Error: " . mysql_error() . "\r\r";
					$error .= "MySQL Query: " . $total_query . "\r\r";
					$error .= "Response XML:\r" . $xml;
					
					log_error("TicketUpdateNumModTechsError_" . $requestID . ".txt", nl2br(htmlentities($error)));
					mail_error("QB - Ticket Update NumModTechs Error", $error);
				}
			}
			else {
				// mysql_fetch_array returned false, there are no records that match the query
				$error = $last_update . "\r\r";
				$error .= "MySQL could not find a record that matches the query while updating the ticket for num_mod_techs.\r\r";
				$error .= "Request ID = $requestID\r\r";
				$error .= "Tech ID = $ID\r\r";
				$error .= "Query: $ticket_query\r\r";
				
				log_error("VendorModResponseError_" . $requestID . ".txt", $error);
				mail_error("QB - MySQL Vendor Mod Request Error", $error);
				
				// return an error to QBWX
				$err = "VendorModResponse: MySQL could not find a record that matches the query while updating the ticket for num_mod_techs";
			}
		}
		else {
			// Query failed
			$error = $last_update . "\r\r";
			$error .= "MySQL Error Querying quickbooks_queue table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=VendorModRequestError_" . $requestID . ".txt\r\r";
			$error .= "Request ID = $requestID\r";
			$error .= "Member ID = $ID\r\r";			
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			$error .= "MySQL Query: " . $ticket_query . "\r\r";
			
			log_error("VendorModResponseError_" . $requestID . ".txt", $error);
			mail_error('QB - MySQL Vendor Mod Response Error', $error);
			
			// return an error to QBWC
			$err = "VendorModResponse: The MySQL query failed for request number $requestID, Member ID $ID";
		}
	}
	
	// DataExtMod Vendor Mod Request - ACH Information - account number is masked in log
	function vendor_dataext_mod_request($requestID, $action, $ID, $extra, &$err) {
		global $last_update;
		global $do_logs;
		
		// Get the vendor custom fields information from the MySQL database
		$query = "SELECT list_id, member_id, edit_sequence, ach_account_name, ach_bank_name, ach_bank_city, ach_bank_state, 
			ach_bank_zip, ach_bank_country, ach_account_type, ach_routing_number, ach_account_number FROM members WHERE id = $ID";
		
		if($result = mysql_query($query)) {
			if($record = mysql_fetch_array($result)) {
				$list_id = $record['list_id'];
				$member_id = $record['member_id'];
				$edit_sequence = $record['edit_sequence'];
				$account_name = htmlentities(substr($record['ach_account_name'], 0, 29));
				$bank_name = htmlentities(substr($record['ach_bank_name'], 0, 29));
				$bank_city = htmlentities(substr($record['ach_bank_city'], 0, 29));
				$bank_state = htmlentities(substr($record['ach_bank_state'], 0, 29));
				$bank_zip = htmlentities(substr($record['ach_bank_zip'], 0, 29));
				$bank_citystatezip = htmlentities(substr("$bank_city, $bank_state, $bank_zip", 0, 29));
				$bank_country = htmlentities(substr($record['ach_bank_country'], 0, 29));
				$account_type = htmlentities(substr($record['ach_account_type'], 0, 29));
				
				$routingNum = $record['ach_routing_number'];
				$accountNum = $record['ach_account_number'];			
				
				/*******************************************/
				// Decrypt account info
				
				// get password
				$filename = "/tmp/lexiconjob.txt";
				$handle = fopen($filename, "r");
				$contents = fread($handle, filesize($filename));
				fclose($handle);

				$password = unserialize($contents);
								
				$privateKey = getBankingPrivateKey($password);
				
				if (!$privateKey) {
					$error = $last_update . "\r\r";
					$error .= "Invalid password for decrypting account nunbers.\r\r";
					$error .= "Request ID = $requestID\r\r";
					$error .= "Member ID = $ID\r\r";
					
					log_error("DataExtMod_VenderModRequestError_" . $requestID . ".txt", $error);
					mail_error("QB - MySQL DataExtMod_Vendor Mod Request Error", $error);
					
					// return an error to QBWX
					$err = "DataExtMod_VenderModRequest: Invalid password for decrypting account numbers";
				}
				else {
					if($routingNum && $routingNum != "") {
						$routing_number = htmlentities(substr(decryptData($routingNum, $privateKey), 0, 29)); // decodes encrypted info
					}
					if($accountNum && $accountNum != "") {
						$account_number = htmlentities(substr(decryptData($accountNum, $privateKey), 0, 29));
					}
				}
				
				// End Decrypt Account Info
				/*******************************************/
				
				// Create the qbXML request
				$qbxml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
				<?qbxml version=\"7.0\"?>
				<QBXML>
					<QBXMLMsgsRq onError=\"stopOnError\">
						<DataExtModRq requestID=\"$requestID 1\"> 
							<DataExtMod>
								<OwnerID>0</OwnerID>
								<DataExtName>Name on Bank Account</DataExtName>
								<ListDataExtType>Vendor</ListDataExtType>
								<ListObjRef>
									<ListID>$list_id</ListID>
									<FullName>$member_id</FullName>
								</ListObjRef>
								<DataExtValue>";
								
								if($account_name) {
									$qbxml .= "$account_name";
								}
								else {
									$qbxml .= "N/A";
								}
								
								$qbxml .= "</DataExtValue>
							</DataExtMod>
						</DataExtModRq>
						<DataExtModRq requestID=\"$requestID 2\">
							<DataExtMod>
								<OwnerID>0</OwnerID>
								<DataExtName>Bank Name</DataExtName>
								<ListDataExtType>Vendor</ListDataExtType>
								<ListObjRef>
									<ListID>$list_id</ListID>
									<FullName>$member_id</FullName>
								</ListObjRef>
								<DataExtValue>";
								
								if($bank_name) {
									$qbxml .= "$bank_name";
								}
								else {
									$qbxml .= "N/A";
								}
								
								$qbxml .= "</DataExtValue>
							</DataExtMod>
						</DataExtModRq>
						<DataExtModRq requestID=\"$requestID 3\">
							<DataExtMod>
								<OwnerID>0</OwnerID>
								<DataExtName>Bank City, State Zip</DataExtName>
								<ListDataExtType>Vendor</ListDataExtType>
								<ListObjRef>
									<ListID>$list_id</ListID>
									<FullName>$member_id</FullName>
								</ListObjRef>
								<DataExtValue>";
								
								if($bank_citystatezip != ", , ") {
									$qbxml .= "$bank_citystatezip";
								}
								else {
									$qbxml .= "N/A";
								}
								
								$qbxml .= "</DataExtValue>
							</DataExtMod>
						</DataExtModRq>
						<DataExtModRq requestID=\"$requestID 4\">
							<DataExtMod>
								<OwnerID>0</OwnerID>
								<DataExtName>Bank Country</DataExtName>
								<ListDataExtType>Vendor</ListDataExtType>
								<ListObjRef>
									<ListID>$list_id</ListID>
									<FullName>$member_id</FullName>
								</ListObjRef>
								<DataExtValue>";
								
								if($bank_country) {
									$qbxml .= "$bank_country";
								}
								else {
									$qbxml .= "N/A";
								}
								
								$qbxml .= "</DataExtValue>
							</DataExtMod>
						</DataExtModRq>
						<DataExtModRq requestID=\"$requestID 5\">
							<DataExtMod>
								<OwnerID>0</OwnerID>
								<DataExtName>Account Type</DataExtName>
								<ListDataExtType>Vendor</ListDataExtType>
								<ListObjRef>
									<ListID>$list_id</ListID>
									<FullName>$member_id</FullName>
								</ListObjRef>
								<DataExtValue>";
								
								if($account_type) {
									$qbxml .= "$account_type";
								}
								else {
									$qbxml .= "N/A";
								}
								
								$qbxml .= "</DataExtValue>
							</DataExtMod>
						</DataExtModRq>
						<DataExtModRq requestID=\"$requestID 6\">
							<DataExtMod>
								<OwnerID>0</OwnerID>
								<DataExtName>Routing Number</DataExtName>
								<ListDataExtType>Vendor</ListDataExtType>
								<ListObjRef>
									<ListID>$list_id</ListID>
									<FullName>$member_id</FullName>
								</ListObjRef>
								<DataExtValue>";
								
								if($routing_number) {
									$qbxml .= "$routing_number";
								}
								else {
									$qbxml .= "N/A";
								}
								
								$qbxml .= "</DataExtValue>
							</DataExtMod>
						</DataExtModRq>
						<DataExtModRq requestID=\"$requestID 7\">
							<DataExtMod>
								<OwnerID>0</OwnerID>
								<DataExtName>Account Number</DataExtName>
								<ListDataExtType>Vendor</ListDataExtType>
								<ListObjRef>
									<ListID>$list_id</ListID>
									<FullName>$member_id</FullName>
								</ListObjRef>
								<DataExtValue>";
								
								if($account_number) {
									$qbxml .= "$account_number";
								}
								else {
									$qbxml .= "N/A";
								}
								
								$qbxml .= "</DataExtValue>
							</DataExtMod>
						</DataExtModRq>
					</QBXMLMsgsRq>
				</QBXML>";
				
				// Log the created QBXML - needs to be modified to remove account number
				if($do_logs == true) {
					// mask account number for logging
					$parse_xml = new SimpleXMLElement($qbxml);		
					foreach($parse_xml->QBXMLMsgsRq->DataExtModRq as $mod) {
						$dataExtName = $mod->DataExtMod->DataExtName;
						
						if($dataExtName == "Account Number" || $dataExtName == "Routing Number") {
							$DataExtValue = $mod->DataExtMod->DataExtValue;		
							$new_number = substr_replace($DataExtValue, "xxxxx", 0, 5);		
							$mod->DataExtMod->DataExtValue = $new_number;					
							$log_qbxml = $parse_xml->asXML();
						}
					}
					do_log("DataExtMod_VenderModRequest_" . $requestID . ".xml", $log_qbxml);
				}
				
				// Return the QBXML that will be sent to QuickBooks.
				return $qbxml;
			}
			else {
				// mysql_fetch_array returned false, there are no records that match the query
				$error = $last_update . "\r\r";
				$error .= "Queue existed for a member that is not in the mysql database.\r\r";
				$error .= "Request ID = $requestID\r\r";
				$error .= "Member ID = $ID\r\r";
				
				log_error("DataExtMod_VenderModRequestError_" . $requestID . ".txt", $error);
				mail_error("QB - MySQL DataExtMod_Vendor Mod Request Error", $error);
				
				// return an error to QBWX
				$err = "DataExtMod_VenderModRequest: There is not a member with the id of $ID in the MySQL database";
			}
		}
		else {
			// Query failed
			$error = $last_update . "\r\r";
			$error .= "MySQL Error Querying members table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=DataExtMod_VenderModRequestError_" . $requestID . ".txt\r\r";
			$error .= "Request ID = $requestID\r";
			$error .= "Member ID = $ID\r\r";			
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			
			log_error("DataExtMod_VenderModRequestError_" . $requestID . ".txt", $error);
			mail_error('QB - MySQL DataExtMod_Vendor Mod Request Error', $error);
			
			// return an error to QBWC
			$err = "DataExtMod_VenderModRequest: The MySQL query failed for request number $requestID, Member ID $ID";
		}
	}
	
	// DataExtMod Vendor Mod Response - ACH Information - account number is masked in log
	function vendor_dataext_mod_response($requestID, $action, $ID, $extra, &$err, $xml, $idents) {
		global $last_update;
		global $do_logs;
		
		// Log the response XML - needs to be modified to remove account number
		if($do_logs == true) {
			// mask account number for logging
			$parse_xml = new SimpleXMLElement($xml);		
			foreach($parse_xml->QBXMLMsgsRs->DataExtModRs as $mod) {
				$dataExtName = $mod->DataExtRet->DataExtName;
				
				if($dataExtName == "Account Number" || $dataExtName == "Routing Number") {
					$DataExtValue = $mod->DataExtRet->DataExtValue;		
					$new_number = substr_replace($DataExtValue, "xxxxx", 0, 5);		
					$mod->DataExtRet->DataExtValue = $new_number;
					
					$log_xml = $parse_xml->asXML();
				}
			}
			
			do_log("DataExtMod_VenderModResponse_" . $requestID . ".xml", $log_xml);
		}
		
		// Create a query to update last_update in the MySQL database
		// EditSequence is not returned in the QBXML Reponse and will be picked up the next time this
		// Vendor is synced.
		$query = "UPDATE members SET last_update = '$last_update', last_update_by = 'QuickBooks' WHERE id = $ID";
		$result = mysql_query($query);
		
		// Report errors in a log file
		if(!$result) {
			$error = $last_update . "\r\r";
			$error .= "MySQL error updating members table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=DataExtMod_VenderModResponseError_" . $requestID . ".txt\r\r";
			$error .= "Request ID = $RequestID\r";
			$error .= "Member ID = $ID\r\r";
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			$error .= "Response XML:\r" . $xml; // needs to be updated to remove account number
			
			log_error("DataExtMod_VenderModResponseError_" . $requestID . ".txt", $error);
			mail_error("QB - MySQL DataExtMod_Vendor Mod Response Error", $error);
			
			// return an error to QBWX
			$err = "DataExtMod_VenderModResponse: A MySQL error occured.  Unable to update members table where members.id = $ID";
		}
	}
	
	// Vendor Update Edit Sequence Request
	function vendor_update_edit_sequence_request($requestID, $action, $ID, $extra, &$err) {
		global $last_update;
		global $do_logs;
		
		// get the vendors list_id from MySQL
		$query = "SELECT list_id FROM members WHERE id = $ID";
		if($result = mysql_query($query)) {
			if($record = mysql_fetch_array($result)) {
				$list_id = htmlentities($record['list_id']);
				
				// Create the qbXML request
				$qbxml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
					<?qbxml version=\"7.0\"?>
					<QBXML>
						<QBXMLMsgsRq onError=\"stopOnError\"> 
							<VendorQueryRq requestID=\"$requestID\"> 
								<ListID>$list_id</ListID>
							</VendorQueryRq>
						</QBXMLMsgsRq>
					</QBXML>";
				
				// Log the created QBXML - needs to be modified to remove account number
				if($do_logs == true) {
					do_log("VendorUpdateEditSequenceRequest_" . $requestID . ".xml", $qbxml);
				}
				
				// Return the QBXML that will be sent to QuickBooks.
				return $qbxml;
			}
			else {
				// mysql_fetch_array returned false, there are no records that match the query
				$error = $last_update . "\r\r";
				$error .= "Queue existed for a member that is not in the mysql database.\r\r";
				$error .= "Request ID = $requestID\r\r";
				$error .= "Member ID = $ID\r\r";
				
				log_error("VendorUpdateEditSequenceRequestError_" . $requestID . ".txt", $error);
				mail_error("QB - MySQL Vendor Update Edit Sequence Request Error", $error);
				
				// return an error to QBWX
				$err = "VendorUpdateEditSequenceRequest: There is not a member with the id of $ID in the MySQL database";
			}
		}
		else {
			// Query failed
			$error = $last_update . "\r\r";
			$error .= "MySQL Error Querying members table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=VendorUpdateEditSequenceRequestError_" . $requestID . ".txt\r\r";
			$error .= "Request ID = $requestID\r";
			$error .= "Member ID = $ID\r\r";			
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			
			log_error("VendorUpdateEditSequenceRequestError_" . $requestID . ".txt", $error);
			mail_error('QB - MySQL Vendor Update Edit Sequence Request Error', $error);
			
			// return an error to QBWC
			$err = "VendorUpdateEditSequenceRequest: The MySQL query failed for request number $requestID, Member ID $ID";
		}
	}
	
	// Vendor Update Edit Sequence Reponse
	function vendor_update_edit_sequence_response($requestID, $action, $ID, $extra, &$err, $xml, $idents) {
		global $last_update;
		global $do_logs;
		
		// Log the response XML - needs to be modified to remove account number
		if($do_logs == true) {
			do_log("VendorUpdateEditSequenceResponse_" . $requestID . ".xml", $xml);
		}
		
		// Get info from the response XML
		$parse_xml = new SimpleXMLElement($xml);
		$list_id = $parse_xml->QBXMLMsgsRs->VendorQueryRs->VendorRet->ListID;
		$edit_sequence = $parse_xml->QBXMLMsgsRs->VendorQueryRs->VendorRet->EditSequence;
		
		// Create the query and put the info into the MySQL database
		$query = "UPDATE members SET edit_sequence = '$edit_sequence', last_update = '$last_update', last_update_by = 'QuickBooks' WHERE id = $ID";
		$result = mysql_query($query);
		
		// Report errors in a log file
		if(!$result) {
			$error = $last_update . "\r\r";
			$error .= "MySQL error updating members table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=VendorUpdateEditSequenceResponseError_" . $requestID . ".txt\r\r";
			$error .= "Request ID = $RequestID\r";
			$error .= "Member ID = $ID\r\r";
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			$error .= "Response XML:\r" . $xml; // needs to be updated to remove account number
			
			log_error("VendorUpdateEditSequenceResponseError_" . $requestID . ".txt", $error);
			mail_error("QB - MySQL Vendor Update Edit Sequence Response Error", $error);
			
			// return an error to QBWX
			$err = "VendorUpdateEditSequenceResponse: A MySQL error occured.  Unable to update members table where members.id = $ID";
		}
	}
	
	// Bill Add Request
	function bill_add_request($requestID, $action, $ID, $extra, &$err) {
		global $last_update;
		global $do_logs;
		
		// Fetch Bill information
		$bill_query = "SELECT member_id, wo_id, bill_date, pay_amount, (SELECT iso_affiliation_id FROM members WHERE bills.member_id = members.member_id) AS iso_aff, caspio_type, caspio_unique_id FROM bills WHERE id = $ID";
		if($bill_result = mysql_query($bill_query)) {
			if($bill_record = mysql_fetch_array($bill_result)) {
				$member_id = htmlentities($bill_record['member_id']);
				$wo_id = htmlentities($bill_record['wo_id']);
				$bill_date = htmlentities($bill_record['bill_date']);
				$pay_amount = htmlentities($bill_record['pay_amount']);
				$iso_aff = htmlentities($bill_record['iso_aff']);
				$due_date_timestamp = strtotime($bill_date . "+7 days");
				$due_date = date('Y-m-d', $due_date_timestamp);
				$caspio_type = htmlentities($bill_record['caspio_type']);
				$caspio_unique_id = htmlentities($bill_record['caspio_unique_id']);
				$wo_unique_id = ($caspio_type == 0 ? "Main_" : "FLS_") . $caspio_unique_id;
				
				// Fetch Vendor Information - pull in ISO info if company is paid
				if ($iso_aff == "NULL" || $iso_aff == "")
					$vendor_query = "SELECT list_id, edit_sequence, firstname, lastname, payment_method FROM members WHERE member_id = '$member_id'";
				else
					$vendor_query = "SELECT list_id, edit_sequence, (SELECT firstname FROM members WHERE member_id = '$member_id') AS firstname, (SELECT lastname FROM members WHERE member_id = '$member_id') AS lastname, payment_method FROM members WHERE member_id = 'ISO-$iso_aff'";

				if($vendor_result = mysql_query($vendor_query)) {
					if($vendor_record = mysql_fetch_array($vendor_result)) {
						$list_id = htmlentities($vendor_record['list_id']);
						$edit_sequence = htmlentities($vendor_record['edit_sequence']);
						$firstname = htmlentities($vendor_record['firstname']);
						$lastname = htmlentities($vendor_record['lastname']);
						$pay_terms = htmlentities($vendor_record['payment_method']);
						$memo = "$wo_unique_id|$wo_id|$member_id|$firstname $lastname";
						
						// Create the qbXML request
						$qbxml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
						<?qbxml version=\"7.0\"?>
						<QBXML>
							<QBXMLMsgsRq onError=\"stopOnError\">
								<BillAddRq requestID=\"$requestID\">
									<BillAdd>
										<VendorRef>
											<ListID>$list_id</ListID>
										</VendorRef>
										<APAccountRef>
											<FullName>Accounts Payable - Labor</FullName>
										</APAccountRef>
										<TxnDate>$bill_date</TxnDate>
										<DueDate>$due_date</DueDate>
										<RefNumber>$lastname</RefNumber>
										";
										
										if($pay_terms == "Direct Deposit") {
											$qbxml .= "
												<TermsRef>
													<FullName>ACH</FullName>
												</TermsRef>
											";
										}
										
										$qbxml .= "
										<Memo>$memo</Memo>
										<ExpenseLineAdd>
											<AccountRef>
												<FullName>Cost of Revenue - Tech Labor</FullName>
											</AccountRef>
											<Amount>$pay_amount</Amount>
											<Memo>$memo</Memo>
										</ExpenseLineAdd>
									</BillAdd>
								</BillAddRq>
							</QBXMLMsgsRq>
						</QBXML>";
						
						// Log the created QBXML - needs to be modified to remove account number
						if($do_logs == true) {
							do_log("BillAddRequest_" . $requestID . ".xml", $qbxml);
						}
						
						// Return the QBXML that will be sent to QuickBooks.
						return $qbxml;
					}
					else {
						// mysql_fetch_array returned false, there are no records that match the query
						$error = $last_update . "\r\r";
						$error .= "Queue existed for a vendor that is not in the mysql database.\r\r";
						$error .= "Request ID = $requestID\r\r";
						$error .= "Bill ID = $ID\r\r";
						$error .= "Member ID = $member_id\r\r";
						$error .= "Query: " . $vendor_query . "\r\r";
						$error .= "MySQL Error: " . mysql_error() . "\r\r";
						
						log_error("BillAddRequestError_" . $requestID . ".txt", $error);
						mail_error("QB - MySQL Bill Add Request Error", $error);
						
						// return an error to QBWX
						$err = "BillAddRequest: There is not a member with the id of $ID in the MySQL database";
					}
				}
				else {
					// Query failed
					$error = $last_update . "\r\r";
					$error .= "MySQL Error Querying members table\r\r";
					$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=BillAddRequestError_" . $requestID . ".txt\r\r";
					$error .= "Request ID = $requestID\r";
					$error .= "Member ID = $ID\r\r";			
					$error .= "MySQL Error: " . mysql_error() . "\r\r";
					
					log_error("BillAddRequestError_" . $requestID . ".txt", $error);
					mail_error('QB - MySQL Bill Add Request Error', $error);
					
					// return an error to QBWC
					$err = "BillAddRequest: The MySQL query failed for request number $requestID, Member ID $ID";
				}
			}
			else {
				// mysql_fetch_array returned false, there are no records that match the query
				$error = $last_update . "\r\r";
				$error .= "Queue existed for a bill that is not in the mysql database.\r\r";
				$error .= "Request ID = $requestID\r\r";
				$error .= "Bill ID = $ID\r\r";
				
				log_error("BillAddRequestError_" . $requestID . ".txt", $error);
				mail_error("QB - MySQL Bill Add Request Error", $error);
				
				// return an error to QBWX
				$err = "BillAddRequest: There is not a bill with the id of $ID in the MySQL database";
			}
		}
		else {
			// Query failed
			$error = $last_update . "\r\r";
			$error .= "MySQL Error Querying bills table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=BillAddRequestError_" . $requestID . ".txt\r\r";
			$error .= "Request ID = $requestID\r";
			$error .= "Bill ID = $ID\r\r";			
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			
			log_error("BillAddRequestError_" . $requestID . ".txt", $error);
			mail_error('QB - MySQL Bill Add Request Error', $error);
			
			// return an error to QBWC
			$err = "BillAddRequest: The MySQL query failed for request number $requestID, Bill ID $ID";
		}
	}
	
	// Bill Add Response
	function bill_add_response($requestID, $action, $ID, $extra, &$err, $xml, $idents) {
		global $last_update;
		global $do_logs;
		
		// Log the response XML
		if($do_logs == true) {
			do_log("BillAddResponse_" . $requestID . ".xml", $xml);
		}
		
		// Get info from the response XML
		$parse_xml = new SimpleXMLElement($xml);
		$transaction_id = $parse_xml->QBXMLMsgsRs->BillAddRs->BillRet->TxnID ;
		$edit_sequence = $parse_xml->QBXMLMsgsRs->BillAddRs->BillRet->EditSequence;
		$transaction_line_id = $parse_xml->QBXMLMsgsRs->BillAddRs->BillRet->ExpenseLineRet->TxnLineID;
		$transaction_number = $parse_xml->QBXMLMsgsRs->BillAddRs->BillRet->TxnNumber;
		$bill_amount = $parse_xml->QBXMLMsgsRs->BillAddRs->BillRet->AmountDue;
		
		// enter info into bills table of MySQL database
		$query = "UPDATE bills SET transaction_id = '$transaction_id', edit_sequence = '$edit_sequence', 
			transaction_line_id = '$transaction_line_id', last_update = '$last_update', last_update_by = 'QuickBooks', 
			transaction_number = $transaction_number WHERE id = $ID";
		$result = mysql_query($query);
		
		// Report errors in a log file	
		if(!$result) {
			$error = $last_update . "\r\r";
			$error .= "MySQL error updating bills table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=BillAddResponseError_" . $requestID . ".txt\r\r";
			$error .= "Request ID = $RequestID\r";
			$error .= "Bill ID = $ID\r\r";
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			$error .= "Response XML:\r" . $xml;
			
			log_error("BillAddResponseError_" . $requestID . ".txt", $error);
			mail_error("QB - MySQL Bill Add Response Error", $error);
			
			// return an error to QBWX
			$err = "BillAddResponse: A MySQL error occured.  Unable to update bills table where bills.id = $ID";
		}
		
		// update bills_total in the quickbooks_ticket table - 
		// There is one transaction ID per sync so this is an ideal place to keep this kind of data
		
		// get the ticket id
		$ticket_query = "SELECT quickbooks_ticket_id FROM quickbooks_queue WHERE qb_action = 'BillAdd' AND ident = '$ID'";
		if($ticket_result = mysql_query($ticket_query)) {
			if($ticket_record = mysql_fetch_array($ticket_result)) {
				$ticket_id = $ticket_record['quickbooks_ticket_id'];
				
				$total_query = "UPDATE quickbooks_ticket SET bills_total = bills_total + $bill_amount, number_of_bills = number_of_bills + 1 
					WHERE quickbooks_ticket_id = $ticket_id";
				$total_result = mysql_query($total_query);
				
				// Report errors in a log file	
				if(!$total_result) {
					$error = $last_update . "\r\r";
					$error .= "MySQL error updating quickbooks_ticket table - to update bills_total\r\r";
					$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=TicketUpdateBillsTotalError_" . $requestID . ".txt\r\r";
					$error .= "Request ID = $RequestID\r";
					$error .= "Bill ID = $ID\r\r";
					$error .= "MySQL Error: " . mysql_error() . "\r\r";
					$error .= "MySQL Query: " . $total_query . "\r\r";
					$error .= "Response XML:\r" . $xml;
					
					log_error("TicketUpdateBillsTotalError_" . $requestID . ".txt", nl2br(htmlentities($error)));
					mail_error("QB - Ticket Update Bills Total Error", $error);
					
					// return an error to QBWX
					//$err = "BillAddResponse: A MySQL error occured.  Unable to update bills table where bills.id = $ID";
				}
			}
			else {
				// mysql_fetch_array returned false, there are no records that match the query
				$error = $last_update . "\r\r";
				$error .= "MySQL could not find a record that matches the query while updating the ticket for bills_total.\r\r";
				$error .= "Request ID = $requestID\r\r";
				$error .= "Bill ID = $ID\r\r";
				$error .= "Query: $ticket_query\r\r";
				
				log_error("BillAddRequestError_" . $requestID . ".txt", $error);
				mail_error("QB - MySQL Bill Add Request Error", $error);
				
				// return an error to QBWX
				$err = "BillAddRequest: MySQL could not find a record that matches the query while updating the ticket for bills_total";
			}
		}
		else {
			// Query failed
			$error = $last_update . "\r\r";
			$error .= "MySQL Error Querying quickbooks_queue table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=BillAddRequestError_" . $requestID . ".txt\r\r";
			$error .= "Request ID = $requestID\r";
			$error .= "Member ID = $ID\r\r";			
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			$error .= "MySQL Query: " . $ticket_query . "\r\r";
			
			log_error("BillAddRequestError_" . $requestID . ".txt", $error);
			mail_error('QB - MySQL Bill Add Request Error', $error);
			
			// return an error to QBWC
			$err = "BillAddRequest: The MySQL query failed for request number $requestID, Member ID $ID";
		}
	}
	
	// Bill Query Request
	function bill_query_request($requestID, $action, $ID, $extra, &$err) {
		global $last_update;
		global $do_logs;
		
		// Get start date
		$query = "SELECT DISTINCT bill_date FROM bills WHERE is_paid = 0 ORDER BY bill_date ASC LIMIT 1";
		if($result = mysql_query($query)) {
			$today = date('Y-m-d');
			
			// if a sync has not been performed, set last sync date to 01/01/2007
			if(mysql_num_rows($result) > 0) {
				$row = mysql_fetch_row($result);
				$from_date = $row[0];
				$from_date = strtotime($from_date);
				$from_date = date('Y-m-d' ,$from_date);
			}
			else {
				$from_date = date('Y-m-d');
			}
			
			$from_date = date("Y-m-d", strtotime("-10 days"));
			
			// Create QBXML
			$qbxml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
			<?qbxml version=\"7.0\"?>
			<QBXML>
				<QBXMLMsgsRq onError=\"continueOnError\">
					<BillQueryRq requestID=\"$requestID\">
						<TxnDateRangeFilter>
							<FromTxnDate>$from_date</FromTxnDate>
							<ToTxnDate>$today</ToTxnDate>
						</TxnDateRangeFilter>
						<AccountFilter>
							<FullName>Accounts Payable - Labor</FullName>
						</AccountFilter>
						<PaidStatus>PaidOnly</PaidStatus>
						<IncludeLinkedTxns>true</IncludeLinkedTxns>
					</BillQueryRq>
				</QBXMLMsgsRq>
			</QBXML>";
			
			// Log the created QBXML - needs to be modified to remove account number
			if($do_logs == true) {
				do_log("BillQueryRequest_" . $requestID . ".xml", $qbxml);
			}
			
			return $qbxml;
		}
		else {
			// Query failed
			$error = $last_update . "\r\r";
			$error .= "MySQL Error Querying quickbooks_ticket table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=BillQueryRequestError_" . $requestID . ".txt\r\r";
			$error .= "Request ID = $requestID\r";
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			
			log_error("BillQueryRequestError_" . $requestID . ".txt", $error);
			mail_error('QB - MySQL Bill Query Request Error', $error);
			
			// return an error to QBWC
			$err = "BillQueryRequest: The MySQL query failed for request number $requestID";
		}
	}
	
	// Bill Query Response
	function bill_query_response($requestID, $action, $ID, $extra, &$err, $xml, $idents) {
		global $last_update;
		global $do_logs;
		
		// Log the response XML
		if($do_logs == true) {
			do_log("BillQueryResponse_" . $requestID . ".xml", $xml);
		}
		
		$parse_xml = new SimpleXMLElement($xml);
		foreach($parse_xml->QBXMLMsgsRs->BillQueryRs->BillRet as $bill) {
			$paid_date = $bill->LinkedTxn->TxnDate;
			$check_number = $bill->LinkedTxn->RefNumber;
			$paid_amount = $bill->LinkedTxn->Amount;
			$paid_amount = ltrim($paid_amount, "-");
			$transaction_id = $bill->TxnID;
			
			// update num_paid and amount_paid in the quickbooks_ticket table - 
			// There is one transaction ID per sync so this is an ideal place to keep this kind of data

			// get the ticket id

			$ticket_query = "SELECT quickbooks_ticket_id, is_paid FROM quickbooks_queue LEFT JOIN bills ON transaction_id = '$transaction_id' WHERE qb_action = 'BillQuery' AND ident = '$ID'";
			$ticket_result = mysql_query($ticket_query);
			
			// put info into MySQL
			$bill_update_query = "UPDATE bills SET is_paid = 1, paid_date = '$paid_date', check_number = '$check_number', paid_amount = '$paid_amount', 
				last_update = '$last_update', last_update_by = 'QuickBooks' WHERE transaction_id = '$transaction_id'";
			
			$result = mysql_query($bill_update_query);
			if(!$result) {
				$error = $last_update . "\r\r";
				$error .= "MySQL error updating bills table\r\r";
				$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=BillQueryResponseError_" . $requestID . ".txt\r\r";
				$error .= "Request ID = $RequestID\r";
				$error .= "Transaction ID = $transaction_id\r\r";
				$error .= "MySQL Error: " . mysql_error() . "\r\r";
				$error .= "Response XML:\r" . $xml;
				
				log_error("BillQueryResponseError_" . $requestID . ".txt", $error);
				mail_error("QB - MySQL Bill Query Response Error", $error);
				
				// return an error to QBWX
				$err = "BillQueryResponse: A MySQL error occured.  Unable to update bills table where bills.transaction_id = $transaction_id";
			}
						
			if($ticket_result) {
				if($ticket_record = mysql_fetch_array($ticket_result)) {
					$ticket_id = $ticket_record['quickbooks_ticket_id'];
					$is_paid = $ticket_record['is_paid'];
										
					if ($is_paid == 0 && $is_paid != NULL) { 
						// only count work orders that haven't been synced back to caspio

						$total_query = "UPDATE quickbooks_ticket SET amount_paid = amount_paid + $paid_amount, num_paid = num_paid + 1 
							WHERE quickbooks_ticket_id = $ticket_id";
						$total_result = mysql_query($total_query);
						
						// Report errors in a log file	
						if(!$total_result) {
							$error = $last_update . "\r\r";
							$error .= "MySQL error updating quickbooks_ticket table - to update amount_paid or num_paid\r\r";
							$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=TicketUpdateBillsPaidError_" . $requestID . ".txt\r\r";
							$error .= "Request ID = $RequestID\r";
							$error .= "Bill ID = $ID\r\r";
							$error .= "MySQL Error: " . mysql_error() . "\r\r";
							$error .= "MySQL Query: " . $total_query . "\r\r";
							$error .= "Response XML:\r" . $xml;
							
							log_error("TicketUpdateBillsPaidError_" . $requestID . ".txt", nl2br(htmlentities($error)));
							mail_error("QB - Ticket Update Bills Paid Error", $error);
							
							// return an error to QBWX
							//$err = "BillAddResponse: A MySQL error occured.  Unable to update bills table where bills.id = $ID";
						}
					}
				}
				else {
					// mysql_fetch_array returned false, there are no records that match the query
					$error = $last_update . "\r\r";
					$error .= "MySQL could not find a record that matches the query while updating the ticket for bills_paid.\r\r";
					$error .= "Request ID = $requestID\r\r";
					$error .= "Bill ID = $ID\r\r";
					$error .= "Query: $ticket_query\r\r";
					
					log_error("BillQueryResponseError_" . $requestID . ".txt", $error);
					mail_error("QB - MySQL Bill Query Response Error", $error);
					
					// return an error to QBWX
					$err = "BillQueryResponse: MySQL could not find a record that matches the query while updating the ticket for bills_total";
				}
			}
			else {
				// Query failed
				$error = $last_update . "\r\r";
				$error .= "MySQL Error Querying quickbooks_queue table\r\r";
				$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=BillQueryResponseError_" . $requestID . ".txt\r\r";
				$error .= "Request ID = $requestID\r";
				$error .= "Bill ID = $ID\r\r";			
				$error .= "MySQL Error: " . mysql_error() . "\r\r";
				$error .= "MySQL Query: " . $ticket_query . "\r\r";
				
				log_error("BillQueryResponseError_" . $requestID . ".txt", $error);
				mail_error('QB - MySQL Bill Query Response Error', $error);
				
				// return an error to QBWC
				$err = "BillAddRequest: The MySQL query failed for request number $requestID, Bill ID $ID";
			}
		}
	}
	
	// Vendor Query Request
	function vendor_query_request($requestID, $action, $ID, $extra, &$err) {
		global $last_update;
		global $do_logs;
		
		// get the member id
		$query = "SELECT member_id FROM members WHERE id = $ID";
		if($result = mysql_query($query)) {
			if($record = mysql_fetch_array($result)) {
				$member_id = htmlentities($record['member_id']);
				
				// Create QBXML
				$qbxml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>
					<?qbxml version=\"7.0\"?>
					<QBXML>
						<QBXMLMsgsRq onError=\"continueOnError\">
							<VendorQueryRq requestID=\"$requestID\">
								<FullName>$member_id</FullName>
							</VendorQueryRq>
						</QBXMLMsgsRq>
					</QBXML>";
				
				// Log the created QBXML
				if($do_logs == true) {
					do_log("VendorQueryRequest_" . $requestID . ".xml", $qbxml);
				}
				
				// Return the QBXML that will be sent to QuickBooks.
				return $qbxml;
			}
			else {
				// mysql_fetch_array returned false, there are no records that match the query
				$error = $last_update . "\r\r";
				$error .= "Queue existed for a vendor that is not in the database.\r\r";
				$error .= "Request ID = $requestID\r\r";
				$error .= "Member ID = $ID\r\r";
				
				log_error("VendorQueryRequestError_" . $requestID . ".txt", $error);
				mail_error("QB - MySQL Vendor Query Request Error", $error);
				
				// return an error to QBWX
				$err = "VendorQueryRequest: There is not a member with the id of $ID in the MySQL database";
			}
		}
		else {
			// Query failed
			$error = $last_update . "\r\r";
			$error .= "MySQL Error Querying members table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=VendorQueryRequestError_" . $requestID . ".txt\r\r";
			$error .= "Request ID = $requestID\r";
			$error .= "Member ID = $ID\r\r";			
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			
			log_error("VendorQueryRequestError_" . $requestID . ".txt", $error);
			mail_error('QB - MySQL Vendor Query Request Error', $error);
			
			// return an error to QBWC
			$err = "VendorQueryRequest: The MySQL query failed for request number $requestID, Member ID $ID";
		}
	}
	
	// Vendor Query Response
	function vendor_query_response($requestID, $action, $ID, $extra, &$err, $xml, $idents) {
		global $last_update;
		global $do_logs;
		
		// Log the response XML
		if($do_logs == true) {
			do_log("VendorQueryResponse_" . $requestID . ".xml", $xml);
		}
		
		// Get info from the response XML
		$parse_xml = new SimpleXMLElement($xml);
		$list_id = $parse_xml->QBXMLMsgsRs->VendorQueryRs->VendorRet->ListID;
		$edit_sequence = $parse_xml->QBXMLMsgsRs->VendorQueryRs->VendorRet->EditSequence;
		
		// enter info into members table of MySQL database
		$query = "UPDATE members SET list_id = '$list_id', edit_sequence = '$edit_sequence', last_update = '$last_update', 
			last_update_by = 'QuickBooks' WHERE id = $ID";
		$result = mysql_query($query);
		
		if(!$result) {
			$error = $last_update . "\r\r";
			$error .= "MySQL error updating members table\r\r";
			$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=VendorQueryResultError_" . $requestID . ".txt\r\r";
			$error .= "Request ID = $RequestID\r";
			$error .= "Member ID = $ID\r\r";
			$error .= "MySQL Error: " . mysql_error() . "\r\r";
			$error .= "MySQL Query: " . $query . "\r\r";
			$error .= "Response XML:\r" . $xml;
			
			log_error("VendorQueryResultError_" . $requestID . ".txt", nl2br(htmlentities($error)));
			mail_error("QB - Vendor Query Result Error", $error);
			
			// return an error to QBWX
			$err = "VendorQueryResponse: A MySQL error occured.  Unable to update members table where id = $ID";
		}
	}
	
	/*******************************************************************/
	/****************  NON QUEUE PROCESSING FUNCTIONS  *****************/
	/*******************************************************************/
		
	// QB String too long error function
	function _quickbooks_error_stringtoolong($requestID, $action, $ID, $extra, &$err, $xml, $errnum, $errmsg) {
		mail('quickbooks@fieldsolutions.com', 
			'QuickBooks error occured!', 
			'QuickBooks thinks that ' . $action . ': ' . $ID . ' has a value which will not fit in a QuickBooks field...');
	}
	
	// catch-all quickbooks errors
	function report_quickbooks_errors($requestID, $action, $ID, $extra, &$err, $xml, $errnum, $errmsg) {
		global $last_update;
		
		$error = $last_update . "\r\r";
		$error .= "Quickbooks Error: $errnum\r\r";
		$error .= "Log is at http://www.fieldsolutions.com/accounting/QB/QBLOG/read_log.php?filename=QuickbooksError-$errnum_" . $requestID . ".txt\r\r";
		$error .= "ID: $ID\r";
		$error .= "Request ID = $RequestID\r";
		$error .= "Action = $action\r\r";
		$error .= "XML:\r";
		$error .= $xml;
		
		log_error("QuickbooksError: " . $errnum . "_" . $requestID . ".txt", nl2br(htmlentities($error)));
		mail_error("QB - QuickbooksError: $errnum", $error);
	}
	
	// used for emailing when errors happen
	function mail_error($subject, $message) {
		$to      = 'quickbooks@fieldsolutions.com';
		mail($to, $subject, $message);
	}
	
	// used to log error when they happen
	function log_error($log_file, $error) {
		$date = date("Ymd");
		
		if(!is_dir("/tmp/QBLOG/$date")) {
			mkdir("/tmp/QBLOG/$date");
		}
		
		if($fp = fopen("/tmp/QBLOG/$date/$log_file", "a+")) {
			fwrite($fp, $error);
			fclose($fp);
		}
		else {
			$mail_error = $last_upadate . "\r\r";
			$mail_error .= "Unable to write log to /tmp/QBLOG/$date/$log_file\r\r";
			$mail_error .= "Error: $error";
			mail_error("QB - Log Failed", $error);
		}
	}
	
	// used to log requests and responses
	function do_log($log_file, $log) {
		$date = date("Ymd");
		
		if(!is_dir("/tmp/QBLOG/$date")) {
			mkdir("/tmp/QBLOG/$date");
		}
		if($fp = fopen("/tmp/QBLOG/$date/$log_file", "w")) {
			fwrite($fp, $log);
			fclose($fp);
		}
		else {
			$error = $last_upadate . "\r\r";
			$error = "Unable to write log to /tmp/QBLOG/$date/$log_file\r\r";
			$error = "Log:\r" . $log;
			
			mail_error("QB - Log Failed", $error);
		}
	}
?>