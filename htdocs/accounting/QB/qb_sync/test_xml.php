<?
	$xml = '<?xml version="1.0" ?>
<QBXML>
	<QBXMLMsgsRs>
		<DataExtModRs requestID="RGF0YUV4dE1vZF9WZW5kb3J8NTE3fDAzLTIwLTIwMDggMTE6MDQ6NDE= 1" statusCode="0" statusSeverity="Info" statusMessage="Status OK">
			<DataExtRet>
				<OwnerID>0</OwnerID>
				<DataExtName>Name on Bank Account</DataExtName>
				<DataExtType>STR255TYPE</DataExtType>
				<DataExtValue>Erik Polcer</DataExtValue>
			</DataExtRet>
		</DataExtModRs>
		<DataExtModRs requestID="RGF0YUV4dE1vZF9WZW5kb3J8NTE3fDAzLTIwLTIwMDggMTE6MDQ6NDE= 2" statusCode="0" statusSeverity="Info" statusMessage="Status OK">
			<DataExtRet>
				<OwnerID>0</OwnerID>
				<DataExtName>Bank Name</DataExtName>
				<DataExtType>STR255TYPE</DataExtType>
				<DataExtValue>The American National Bank of</DataExtValue>
			</DataExtRet>
		</DataExtModRs>
		<DataExtModRs requestID="RGF0YUV4dE1vZF9WZW5kb3J8NTE3fDAzLTIwLTIwMDggMTE6MDQ6NDE= 3" statusCode="0" statusSeverity="Info" statusMessage="Status OK">
			<DataExtRet>
				<OwnerID>0</OwnerID>
				<DataExtName>Bank City, State Zip</DataExtName>
				<DataExtType>STR255TYPE</DataExtType>
				<DataExtValue>Terrell, TX, 75160</DataExtValue>
			</DataExtRet>
		</DataExtModRs>
		<DataExtModRs requestID="RGF0YUV4dE1vZF9WZW5kb3J8NTE3fDAzLTIwLTIwMDggMTE6MDQ6NDE= 4" statusCode="0" statusSeverity="Info" statusMessage="Status OK">
			<DataExtRet>
				<OwnerID>0</OwnerID>
				<DataExtName>Bank Country</DataExtName>
				<DataExtType>STR255TYPE</DataExtType>
				<DataExtValue>United States</DataExtValue>
			</DataExtRet>
		</DataExtModRs>
		<DataExtModRs requestID="RGF0YUV4dE1vZF9WZW5kb3J8NTE3fDAzLTIwLTIwMDggMTE6MDQ6NDE= 5" statusCode="0" statusSeverity="Info" statusMessage="Status OK">
			<DataExtRet>
				<OwnerID>0</OwnerID>
				<DataExtName>Account Type</DataExtName>
				<DataExtType>STR255TYPE</DataExtType>
				<DataExtValue>Checking</DataExtValue>
			</DataExtRet>
		</DataExtModRs>
		<DataExtModRs requestID="RGF0YUV4dE1vZF9WZW5kb3J8NTE3fDAzLTIwLTIwMDggMTE6MDQ6NDE= 6" statusCode="0" statusSeverity="Info" statusMessage="Status OK">
			<DataExtRet>
				<OwnerID>0</OwnerID>
				<DataExtName>Routing Number</DataExtName>
				<DataExtType>STR255TYPE</DataExtType>
				<DataExtValue>111901519</DataExtValue>
			</DataExtRet>
		</DataExtModRs>
		<DataExtModRs requestID="RGF0YUV4dE1vZF9WZW5kb3J8NTE3fDAzLTIwLTIwMDggMTE6MDQ6NDE= 7" statusCode="0" statusSeverity="Info" statusMessage="Status OK">
			<DataExtRet>
				<OwnerID>0</OwnerID>
				<DataExtName>Account Number</DataExtName>
				<DataExtType>STR255TYPE</DataExtType>
				<DataExtValue>704064013</DataExtValue>
			</DataExtRet>
		</DataExtModRs>
	</QBXMLMsgsRs>
</QBXML>';
	
	$parse_xml = new SimpleXMLElement($xml);		
	foreach($parse_xml->QBXMLMsgsRs->DataExtModRs as $mod) {
		$dataExtName = $mod->DataExtRet->DataExtName;
		
		if($dataExtName == "Account Number" || $dataExtName == "Routing Number") {
			$DataExtValue = $mod->DataExtRet->DataExtValue;		
			$new_number = substr_replace($DataExtValue, "xxxxx", 0, 5);		
			$mod->DataExtRet->DataExtValue = $new_number;
			
			$log_xml = $parse_xml->asXML();
		}
	}
	
	echo $log_xml;
?> 