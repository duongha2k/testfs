<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Edit Customer</title>
	</head>

	<?
		// set doc_root
		$doc_root = $_SERVER["DOCUMENT_ROOT"];
		require_once($doc_root . '/accounting/QB/database/db_connect.php');
		
		foreach ($_POST as $var => $value) {
			$$var = $value;
			//print("$$var = $value<br />");
		}
		
		// get customer info
		$member_query = "SELECT * FROM members WHERE id = $member_id";
		$record = mysql_fetch_array(mysql_query($member_query));
		
		$id = $record['id'];
		$member_id = $record['member_id'];
		$company_name = $record['company_name'];
		$firstname = $record['firstname'];
		$middlename = $record['middlename'];
		$lastname = $record['lastname'];
		$phone = $record['phone'];
		$email = $record['email'];
		$address1 = $record['address1'];
		$address2 = $record['address2'];
		$city = $record['city'];
		$country = $record['country'];
		$state = $record['state'];
		$zip = $record['zip'];
		$payment_method = $record['payment_method'];
		$ach_account_name = $record['ach_account_name'];
		$ach_bank_name = $record['ach_bank_name'];
		$ach_bank_city = $record['ach_bank_city'];
		$ach_bank_state = $record['ach_bank_state'];
		$ach_bank_zip = $record['ach_bank_zip'];
		$ach_bank_country = $record['ach_bank_country'];
		$ach_account_type = $record['ach_account_type'];
		$ach_routing_number = $record['ach_routing_number'];
		$ach_account_number = $record['ach_account_number'];
	?>
	
	<script type="text/javascript">
		function show_hide_ach() {
			if(document.forms['edit_vendor'].payment_method.value == "Direct Deposit") {
				document.getElementById('ach').style.display = 'block';
			}
			else {
				document.getElementById('ach').style.display = 'none';
				document.getElementById('ach_account_name').value = "";
				document.getElementById('ach_bank_name').value = "";
				document.getElementById('ach_bank_city').value = "";
				document.getElementById('ach_bank_state').value = "";
				document.getElementById('ach_bank_zip').value = "";
				document.getElementById('ach_bank_country').value = "";
				document.getElementById('ach_account_type').value = "";
				document.getElementById('ach_routing_number').value = "";
				document.getElementById('ach_account_number').value = "";
			}
		}	
	</script>
	
	<?
		if($payment_method == "Direct Deposit") {
			echo "<body onLoad=\"show_hide_ach();\">";
		}
		else {
			echo "<body>";
		}
	?>
	
		<div align="center">
			Edit Member<br />
			<br />
			<form name="edit_vendor" id="edit_vendor" action="/accounting/QB/qb_sync/handler.php" method="post">
				<input name="action" type="hidden" value="update_vendor" />
				<input name="id" type="hidden" value="<? echo $id; ?>" />
				<table cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td align="right">Member ID: &nbsp;</td>
						<td align="left"><input name="member_id" type="text" value="<? echo $member_id; ?>" readonly /></td>
					</tr>
					<tr>
						<td align="right">Company Name: &nbsp;</td>
						<td align="left"><input name="company_name" type="text" size="25" maxlength="41" value="<? echo $company_name; ?>"/></td>
					</tr>
					<tr>
						<td align="right">First Name: &nbsp;</td>
						<td align="left"><input name="firstname" type="text" size="25" maxlength="25" value="<? echo $firstname; ?>" /></td>
					</tr>
					<tr>
						<td align="right">Middle Initial: &nbsp;</td>
						<td align="left"><input name="middlename" type="text" size="3" maxlength="1" value="<? echo $middlename; ?>" /></td>
					</tr>
					<tr>
						<td align="right">Last Name: &nbsp;</td>
						<td align="left"><input name="lastname" type="text" size="25" maxlength="25" value="<? echo $lastname; ?>" /></td>
					</tr>
					<tr>
						<td align="right">Email Address: &nbsp;</td>
						<td align="left"><input name="email" type="text" size="25" maxlength="1023" value="<? echo $email; ?>" /></td>
					</tr>
					<tr>
						<td align="right">Address 1: &nbsp;</td>
						<td align="left"><input name="address1" type="text" size="25" maxlength="41" value="<? echo $address1; ?>" /></td>
					</tr>
					<tr>
						<td align="right">Address 2: &nbsp;</td>
						<td align="left"><input name="address2" type="text" size="25" maxlength="41" value="<? echo $address2; ?>" /></td>
					</tr>
					<tr>
						<td align="right">City: &nbsp;</td>
						<td align="left"><input name="city" type="text" size="25" maxlength="31" value="<? echo $city; ?>" /></td>
					</tr>
					<tr>
						<td align="right">Country: &nbsp;</td>
						<td align="left"><input name="country" type="text" size="25" maxlength="31" value="<? echo $country; ?>" /></td>
					</tr>
					<tr>
						<td align="right">State: &nbsp;</td>
						<td align="left"><input name="state" type="text" size="25" maxlength="21" value="<? echo $state; ?>" /></td>
					</tr>
					<tr>
						<td align="right">Zip: &nbsp;</td>
						<td align="left"><input name="zip" type="text" size="13" maxlength="13" value="<? echo $zip; ?>" /></td>
					</tr>
					<tr>
						<td align="right">Phone: &nbsp;</td>
						<td align="left"><input name="phone" type="text" size="25" maxlength="21" value="<? echo $phone; ?>" /></td>
					</tr>
					<tr>
						<td colspan="2">
							<br />
							<table cellpadding="0" cellspacing="0" border="0">
								<tr>
									<td align="right">Payment Method: &nbsp;</td>
									<td align="left">
										<select name="payment_method" onchange="show_hide_ach();">
											<option value="">Select One</option>
											<option value="Direct Deposit" <? if($payment_method == "Direct Deposit") { echo "SELECTED";} ?>>Direct Deposit</option>
											<option value="Check" <? if($payment_method == "Check") { echo "SELECTED";} ?>>Check</option>
										</select>										
									</td>
								</tr>
							</table>
							<div name="ach" id="ach" style="display: none;">
								<table cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td align="right">Name on Bank Account: &nbsp;</td>
										<td align="left"><input id="ach_account_name" name="ach_account_name" type="text" size="25" maxlength="31" value="<? echo $ach_account_name; ?>" /></td>
									</tr>
									<tr>
										<td align="right">Bank Name: &nbsp;</td>
										<td align="left"><input id="ach_bank_name" name="ach_bank_name" type="text" size="25" maxlength="31" value="<? echo $ach_bank_name; ?>" /></td>
									</tr>
									<tr>
										<td align="right">Bank City: &nbsp;</td>
										<td align="left"><input id="ach_bank_city" name="ach_bank_city" type="text" size="25" maxlength="31" value="<? echo $ach_bank_city; ?>" /></td>
									</tr>
									<tr>
										<td align="right">Bank Country: &nbsp;</td>
										<td align="left"><input id="ach_bank_country" name="ach_bank_country" type="text" size="25" maxlength="31" value="<? echo $ach_bank_country; ?>" /></td>
									</tr>
									<tr>
										<td align="right">Bank State/Province: &nbsp;</td>
										<td align="left"><input id="ach_bank_state" name="ach_bank_state" type="text" size="25" maxlength="31" value="<? echo $ach_bank_state; ?>" /></td>
									</tr>
									<tr>
										<td align="right">Bank Postal Code: &nbsp;</td>
										<td align="left"><input id="ach_bank_zip" name="ach_bank_zip" type="text" size="25" maxlength="31" value="<? echo $ach_bank_zip; ?>" /></td>
									</tr>
									<tr>
										<td align="right">Bank Account Type: &nbsp;</td>
										<td align="left">
											<select id="ach_account_type" name="ach_account_type">
												<option value="">Select One</option>
												<option value="Checking" <? if($ach_account_type == "Checking") { echo "SELECTED"; } ?>>Checking</option>
												<option value="Savings" <? if($ach_account_type == "Savings") { echo "SELECTED"; } ?>>Savings</option>
											</select>
										</td>
									</tr>
									<tr>
										<td align="right">Bank Routing Number: &nbsp;</td>
										<td align="left"><input id="ach_routing_number" name="ach_routing_number" type="text" size="25" maxlength="31" value="<? echo $ach_routing_number; ?>" /></td>
									</tr>
									<tr>
										<td align="right">Bank Account Number: &nbsp;</td>
										<td align="left"><input id="ach_account_number" name="ach_account_number" type="text" size="25" maxlength="31" value="<? echo $ach_account_number; ?>" /></td>
									</tr>
								</table>
							</div>
						</tr>
					<tr>
						<td></td>
						<td align="left">
							<div style="margin-top: 10px;">
								<input type="submit" value="Update" />
							</div>
						</td>
					</tr>
				</table>
			</form>
		</div>
	</body>
</html>
