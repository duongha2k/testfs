<?
	// Post Vars
	// username
	// old_password
	// new_password
	// new_password_conf
	
	ini_set("display_errors", 1);
	
	$doc_root = $_SERVER["DOCUMENT_ROOT"];
	require_once($doc_root . '/accounting/QB/database/db_connect.php');
	require_once($doc_root . '/accounting/QB/QuickBooks.php');
	require_once($doc_root . '/accounting/QB/QuickBooks/Utilities.php');
	require_once($doc_root . '/library/openSSL.php');
	
	foreach ($_POST as $var => $value) {
		$$var = $value;
		//print("$$var = $value<br />");
	}
	
	echo "<center>";
	
	if($new_password == $new_password_conf) {
		// check to see if the old password matches what is in the database
		$query = "SELECT qb_password FROM quickbooks_user WHERE qb_username = '$username'";
		if($result = mysql_query($query)) {
			if($record = mysql_fetch_array($result)) {
				$current_password = $record['qb_password'];
				
				if(sha1(mysql_real_escape_string($old_password) . QUICKBOOKS_SALT) == $current_password) {
					// remove current QB account
					$remove_query = "DELETE FROM quickbooks_user WHERE qb_username = 'quickbooks'";
					$remove_result = mysql_query($remove_query);
					
					if(!$remove_result) {
						echo "There was a problem removing the old account.<br /><br />";
						echo "MySQL Query: $remove_query<br />";
						echo "MySQL Error: " . mysql_error() . "<br />";
					}
					else {	
						$a = new QuickBooks_Utilities();
						if($a->createUser("mysql://quickbooks:$qbMysqlPW@localhost/quickbooks", 'quickbooks', $new_password)) {
							echo "QuickBooks QBWC password has been updated<br />";
							
							// change decrypt password here
							/********************************/
							
							$privkey = getBankingPrivateKey($old_password); // decode current key using current password
							
							if (!$privkey) {
								echo "Invalid current password<br />";
								die();
							}
								
							if (!openssl_pkey_export($privkey, $privatekey, $new_password)) { // encodes key using new password
								echo "Unable to create key using this password.<br />";
								die();
							}
							
							// backup old key file
							if(copy(realpath(dirname(__FILE__) . "/../../../../") . "/keys/key.pri", dirname(__FILE__) . "/../../../../") . "/keys/key_backup.pri")) {
								// create the new key file
								if(file_put_contents(dirname(__FILE__) . "/../../../../") . "/keys/key.pri", $privatekey)) { // replace key with reencoded key 
									echo "Encryption key password changed<br />";
								}
							}
							
							/********************************/
							// end change decrypt password
						}
						else {
							echo "There was a problem creating the new account.  Contact Tom Anderson for assistance.<br /><br />";
						}
					}				
				}
				else {
					echo "The old password provided does not match the password currently in the database<br />";
					echo "<a href=\"pswd_change.php\">Go Back</a>";
				}
			}
			else {
				// that username probably doesn't exist
				echo "There was a problem with the MySQL query.  That username probably doesn't exist<br /><br />";
				echo "MySQL Query: $query<br />";
				echo "MySQL Error: " . mysql_error() . "<br />";
			}
		}
		else {
			// problem with the query
			echo "There was a problem with the MySQL query.<br /><br />";
			echo "MySQL Query: $query<br />";
			echo "MySQL Error: " . mysql_error() . "<br />";
		}
	}
	else {
		echo "New Password did not equal Confirm New Password<br /><br />";
		echo "<a href=\"pswd_change.php\">Go Back</a>";
	}
	
	echo "</center>";
	
?>