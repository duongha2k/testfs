<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Handler</title>
	</head>
	
	<body>
		<div align="center">
			<?
			ini_set("display_errors", 1);
			
			/* 
			 * The handler put data into the MySQL quickbooks database and queues the data
			 * to be synched with quickbooks
			 */
			
			foreach ($_POST as $var => $value) {
				$value = addslashes($value);
				$$var = $value;
				
				//print("$$var = $value<br />");  /* Debug Code */
			}
			
			// set the doc_root
			$doc_root = $_SERVER["DOCUMENT_ROOT"];
			
			// include important files
			require_once($doc_root . '/accounting/QB/database/db_connect.php');
			
			$update_datetime = date('Y-m-d G:i:s');
			$update_by = "Caspio";
			
			switch($action){
				case "create_customer":
					// put data into the MySQL quickbooks database
					$query = "INSERT INTO quickbooks_clients (client_id, company_name, contact_firstname, contact_lastname, contact_email, 
						address1, address2, city, country, state, zip, phone, fax, last_update, last_update_by) VALUES ($client_id, 
						'$company_name', '$contact_firstname', '$contact_lastname', '$contact_email', '$address1', '$address2', '$city', 
						'$country', '$state', '$zip', '$phone', '$fax', '$update_datetime', '$update_by')";
					
					if(mysql_query($query)) {
						print("A record for <b>$company_name</b> has been created in the database<br />");
					}
					else {
						print("An error occured.  The new client record was not stored in the database. Error: ". mysql_error());
					}
					
					print("
						<br />
						<a href=\"/accounting/QB/qb_sync/index.php\">Click Here</a> to go back to the index page.
					");
					
					break;
				
				case "update_customer":
					// put data into the MySQL quickbooks database
					$query = "UPDATE quickbooks_clients SET 
						company_name = '$company_name', 
						contact_firstname = '$contact_firstname', 
						contact_lastname = '$contact_lastname', 
						contact_email = '$contact_email', 
						address1 = '$address1', 
						address2 = '$address2', 
						city = '$city', 
						country = '$country', 
						state = '$state', 
						zip = '$zip', 
						phone = '$phone', 
						fax = '$fax', 
						last_update = '$update_datetime', 
						last_update_by = '$update_by' 
						WHERE id = $id";
					
					if(mysql_query($query)) {
						print("The record for <b>$company_name</b> has been updated in the database<br />");
					}
					else {
						// Error if we are unable to put the posted information into MySQL
						print("An error occured.  The client information could not be stored in the database. Error: ". mysql_error());
					}
					
					// message shown after queue-ing is done
					print("
						<br />
						<a href=\"/accounting/QB/qb_sync/index.php\">Click Here</a> to go back to the index page.
					");
					
					break;
				
				case "create_vendor":
					// put data into the MySQL quickbooks database
					$query = "INSERT INTO quickbooks_members (member_id, company_name, firstname, middlename, lastname, phone, email, 
						address1, address2, city, country, state, zip, payment_method, ach_account_name, ach_bank_name, ach_bank_city, 
						ach_bank_state, ach_bank_zip, ach_bank_country, ach_account_type, ach_routing_number, ach_account_number, last_update, 
						last_update_by) VALUES ($member_id, '$company_name', '$firstname', '$middlename', '$lastname', '$phone', 
						'$email', '$address1', '$address2', '$city', '$country', '$state', '$zip', '$payment_method', '$ach_account_name', 
						'$ach_bank_name', '$ach_bank_city', '$ach_bank_state', '$ach_bank_zip', '$ach_bank_country', '$ach_account_type', 
						'$ach_routing_number', '$ach_account_number', '$update_datetime', '$update_by')";
					
					if(mysql_query($query)) {
						$id_value = mysql_insert_id();
						print("A record for <b>$firstname $lastname</b> has been created in the database<br />");
					}
					else {
						print("An error occured.  The new client record was not stored in the database.<br /><br />Error: ". mysql_error() . "<br />");
					}
					
					break;
				
				case "update_vendor":
					// put data into the MySQL quickbooks database
					$query = "UPDATE quickbooks_members SET 
						company_name = '$company_name', 
						firstname = '$firstname', 
						middlename = '$middlename', 
						lastname = '$lastname', 
						phone = '$phone', 
						email = '$email', 
						address1 = '$address1', 
						address2 = '$address2', 
						city = '$city', 
						country = '$country', 
						state = '$state', 
						zip = '$zip', 
						payment_method = '$payment_method', 
						last_update = '$update_datetime', 
						last_update_by = '$update_by', 
						ach_account_name = '$ach_account_name', 
						ach_bank_name = '$ach_bank_name', 
						ach_bank_city = '$ach_bank_city', 
						ach_bank_state = '$ach_bank_state', 
						ach_bank_zip = '$ach_bank_zip', 
						ach_bank_country = '$ach_bank_country', 
						ach_account_type = '$ach_account_type', 
						ach_routing_number = '$ach_routing_number', 
						ach_account_number = '$ach_account_number' 
						WHERE id = $id";
					
					if(mysql_query($query)) {
						print("The record for <b>$firstname $lastname</b> has been updated in the database<br />");
					}
					else {
						// Error if we are unable to put the posted information into MySQL
						print("An error occured.  The new venoor record was not stored in the database. Error: ". mysql_error() . "<br />");
					}
					
					// message shown after queue-ing is done
					print("
						<br />
						<a href=\"/accounting/QB/qb_sync/index.php\">Click Here</a> to go back to the index page.<br />
					");
					
					break;
					
				case "create_bill":
					// Get member information
					$member_query = "SELECT payment_method FROM quickbooks_members WHERE id = $member_id";
					$member_result = mysql_fetch_array(mysql_query($member_query));
					$payment_method = $member_result['payment_method'];
					
					if($payment_method == "Direct Deposit") {
						$pay_terms = "ACH";
					}
					else {
						$pay_terms = "";
					}
					
					// Enter bill information into db
					$today = date('Y-m-d');
					
					$bill_query = "INSERT INTO quickbooks_bills (wo_id, member_id, bill_date, pay_amount, pay_terms, is_paid, last_update, 
						last_update_by) VALUES ('$wo_id', '$member_id', '$today', $pay_amount, '$pay_terms', 0, '$update_datetime', 
						'$update_by')";
					
					if(mysql_query($bill_query)) {
						print("A bill record for work order: $wo_id has been created in the database<br />");
					}
					else {
						print("An error occured. The new bill record was not stored in the database.<br /><br />Error: " . mysql_error() . "<br />");
					}
					
					print("
						<br />
						<a href=\"/accounting/QB/qb_sync/index.php\">Go back to index page</a><br />
					");
					
					break;
			}
			?>
		</div>
	</body>
</html>
