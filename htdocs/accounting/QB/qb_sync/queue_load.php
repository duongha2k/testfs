<?
	// set the doc_root
	$doc_root = $_SERVER["DOCUMENT_ROOT"];
	
	// include important files
	require_once($doc_root . '/accounting/QB/database/db_connect.php');
	require_once($doc_root . '/accounting/QB/QuickBooks/Queue.php');
	
	// Set some vars
	$members_queued = 0;
	$queue = new QuickBooks_Queue('mysql://quickbooks:CRUfru7a@localhost/quickbooks_sm');
	
	// What's in the members table
	$query = "SELECT id, payment_method FROM members";
	if($result = mysql_query($query)) {
		while($record = mysql_fetch_array($result)) {
			$id = $record['id'];
			$payment_method = $record['payment_method'];
			
			if($queue->enqueue('VendorAdd', $id, 100)) {
				if($payment_method == "Direct Deposit") {
					$queue->enqueue('DataExtMod_Vendor', $id, 50);
				}
				
				$members_queued++;
			}
			else {
				echo "Could not queue member: $id<br />";
			}
		}
	}
	else {
		echo "Error: " . mysql_error() . "<br />";
	}
	
	// What's in the bills table
	$bills_query = "SELECT id FROM bills";
	if($bills_result = mysql_query($bills_query)) {
		while($bills_record = mysql_fetch_array($bills_result)) {
			$bills_id = $bills_record['id'];
			
			if($queue->enqueue('BillAdd', $bills_id, 25)) {
				$bills_queued++;
			}
			else {
				echo "Could not queue bill: $bills_id<br />";
			}
		}
	}
	else {
		echo "Error: " . mysql_error() . "<br />";
	}
	
	echo "Members queued: $members_queued<br />";
	echo "Bills queued: $bills_queued<br />";
?>