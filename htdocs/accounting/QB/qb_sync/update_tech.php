<?	
	// set the doc_root
	$doc_root = $_SERVER["DOCUMENT_ROOT"];
	
	ini_set("display_errors", 1);
	
	// include important files
	require_once($doc_root . '/accounting/QB/database/db_connect.php');
	require_once($doc_root . '/accounting/QB/QuickBooks/Queue.php');
	
	$queue = new QuickBooks_Queue("mysql://$qbMysqlUser:$qbMysqlPW@$qbMysqlHost/$qbMysqlDB");
	//$queue = new QuickBooks_Queue("mysql://quickbooks:$qbMysqlPW@localhost/quickbooks");
	
	echo "<div align=\"center\">";
	
	// Set some vars
	foreach ($_POST as $var => $value) {
		$$var = $value;
		print("$$var = $value<br />");
	}
	
	// get the vendors id from the members table
	$id_query = "SELECT id, firstname, lastname FROM quickbooks_members WHERE member_id = '$techid'";
	if($id_result = mysql_query($id_query)) {
		if($id_row = mysql_fetch_array($id_result)) {
			$member_id = $id_row['id'];
			$firstname = $id_row['firstname'];
			$lastname = $id_row['lastname'];
			
			// remove VendorAdd record
			$remove_query = "DELETE FROM quickbooks_queue WHERE ident = '{$member_id}-0' AND qb_action = 'VendorAdd'";
			if($remove_result = mysql_query($remove_query)) {
				
				if($queue->enqueue('VendorQuery', $member_id . "-0", 125)) {
					echo "A VendorQuery for $firstname $lastname has been queued, and will update the next time the sync is run.<br />";
				}
				else {
					echo "Could not queue VendorQueryfor $firstname $lastname for update.<br />";
				}
				
				// queue VendorQuery Record				
				if($queue->enqueue('VendorMod', $member_id . "-0", 100)) {
					echo "A VendorMod for $firstname $lastname has been queued, and will update the next time the sync is run.<br />";
				}
				else {
					echo "Could not queue VendorMod for $firstname $lastname for update.<br />";
				}
				
				// set last updated by to QuickBooks
				$update_query = "UPDATE quickbooks_members SET last_update_by = 'QuickBooks' WHERE member_id = '$techid'";
				if(!$update_query = mysql_query($update_query)) {
					echo "Could not set last update by to QuickBooks.<br />";
					echo "Query: $update_query";
				}
			}
			else {
				// remove not successful
				echo "The query to remove the VendorAdd queue record failed.<br />";
				echo "Query: $remove_query<br />";
			}
		}
		else {
			// couldn't fetch row
			echo "The query to get the Vendors MySQL id failed.<br />";
			echo "Query: $id_query<br />";
		}
	}
	else {
		// id not found
		echo "Could not find a member with that id.";
	}
	
	echo "<br /><a href=\"index.php\">Go Back</a></div>";
?>