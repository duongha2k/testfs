<?php $page = 'accounting'; ?>
<?php $option = 'fsplusone'; ?>
<?php $selected = 'editPricingField'; 
require_once("../library/caspioAPI.php");
?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php 

	if(isset($_POST['fsplusoneAction'])){
		//get tech info
		$tech = new API_Tech();
		$tech->lookupID($_POST['techID']);
		
	    Core_FSPlusOne::applyPoints($_POST['fsplusoneAction'],$tech->TechID, $tech->Lastname, "tech");
	}
	
	$points = Core_FSPlusOne::getActionsPoints();
?>
<br/><br/>
<style type="text/css">
.fsForm{
	margin-left: auto; 
	margin-right: auto;
	}
</style>

<form name="fsplusone" method="post" action="<?= $_SERVER['PHP_SELF']?>">
<div id="container" style="">
	<table class="fsForm" cellspacing="10">
		<tr>
			<td>
			<select name="fsplusoneAction">
				<?php foreach ($points as $p){ ?>
					<option value="<?= $p['id']?>"><?= $p['action']?> (<?=$p['points']?>)</option>
				<?php } ?>
				
			</select>
			</td>
			<td>
			Tech ID# <input type="text" size="30" name="techID"></input>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center"><input type="submit" value="Update Points" /></td>
		</tr>
	</table>
</div>
</form>