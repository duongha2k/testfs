<?php
	set_time_limit(3600);
	$page = 'accounting';
	$option = 'accounting';
	require_once ("../header.php");
	require_once ("../navBar.php");
	ini_set("display_errors",1);

//	try {
//		if (parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) != $_SERVER['HTTP_HOST']) echo "External Access";
		require_once("../library/caspioAPI.php");
		
		require_once("../library/quickbooks.php");
	
//			$invoiceDate = $_POST['date'];

		$db = Zend_Registry::get('DB');

		// find zero pay amounts
		//$zeroMain = caspioSelect("Work_Orders", "WO_ID", "Deactivated = '0' AND MovedToMysql = '0' AND Invoiced = '1' AND TechPaid = '0' AND CAST(Net_Pay_Amount AS MONEY) = 0.00", "", false);

		$sql = "SELECT WO_ID FROM work_orders WHERE Deactivated = '0' AND MovedToMysql = '0' AND Invoiced = '1' AND TechPaid = '0' AND IFNULL(Net_Pay_Amount,0) = 0";
		$zeroMain = $db->fetchAll($sql, array(), Zend_Db::FETCH_NUM);
		if (sizeof($zeroMain) > 0) {
			echo "The following Main Site work orders have an invalid pay amount:<br/>";
			foreach ($zeroMain as $row) {
//				$row = trim($row[0], "'");
				echo "{$row[0]}<br/>";
			}
			echo "<br/><br/>";
		}
		
/*		$zeroFLS = caspioSelect("FLS_Work_Orders", "WorkOrderNo", "MovedToMysql = '0' AND Invoiced = '1' AND Paid = 'No' AND Deactivated != 'Yes' AND Kickback = '0' AND ISNUMERIC(PayAmt) = 1 AND CAST(PayAmt AS MONEY) = 0.00", "", false);
		
		if (sizeof($zeroFLS) != 1 || $zeroFLS[0] != '') {
			echo "The following FLS work orders have an invalid pay amount:<br/>";
			foreach ($zeroFLS as $row) {
				echo "{$row}<br/>";
			}
			echo "<br/><br/>";
		}*/

		try {			
//			$billable = caspioSelectAdv("Work_Orders", "WO_ID, Tech_ID, CAST(PayAmount AS MONEY), TB_UNID, CONVERT(varchar,DateInvoiced, 120)", "Deactivated = '0' AND MovedToMysql = '0' AND Invoiced = '1' AND TechPaid = '0' AND CAST(PayAmount AS MONEY) != 0.00 AND Company_ID NOT IN ('suss', 'CBD')", "", false, "`", "|", false);

//			$billable = caspioSelectAdv("Work_Orders", "WO_ID, Tech_ID, CAST(Net_Pay_Amount AS MONEY), TB_UNID, CONVERT(varchar,DateInvoiced, 120)", "Deactivated = '0' AND MovedToMysql = '0' AND Invoiced = '1' AND TechPaid = '0' AND CAST(Net_Pay_Amount AS MONEY) != 0.00 AND Company_ID NOT IN ('suss', 'CBD')", "", false, "`", "|", false);

			$sql = "SELECT WO_ID, Tech_ID, Net_Pay_Amount, WIN_NUM, DATE_FORMAT(DateInvoiced, '%Y-%m-%d') FROM work_orders WHERE Deactivated = '0' AND MovedToMysql = '0' AND Invoiced = '1' AND TechPaid = '0' AND IFNULL(Net_Pay_Amount, 0) != 0 AND Company_ID NOT IN ('suss', 'CBD', 'BW')";
			$billable = $db->fetchAll($sql, array(), Zend_Db::FETCH_NUM);



//			$billable = caspioSelectAdv("Work_Orders", "WO_ID, Tech_ID, (CASE WHEN (ISNULL(Net_Pay_Amount,0) != 0) THEN Net_Pay_Amount ELSE CAST(PayAmount AS MONEY) END), TB_UNID, CONVERT(varchar,DateInvoiced, 120)", "Deactivated = '0' AND MovedToMysql = '0' AND Invoiced = '1' AND TechPaid = '0' AND CAST(PayAmount AS MONEY) != 0.00 AND Company_ID NOT IN ('suss', 'CBD')", "", false, "`", "|", false);

/*			$blankIDs = caspioSelect("FLS_Work_Orders", "WorkOrderID, (SELECT TechID FROM TR_Master_List WHERE FLSID = FLS_Work_Orders.FLSTechID)", "(ISNULL(TB_ID, '') = '' OR ISNUMERIC(TB_ID) = 0) AND MovedToMysql = '0' AND Invoiced = '1' AND Paid = 'No' AND Deactivated != 'Yes' AND Kickback = '0'", "", false);
			
			foreach ($blankIDs as $row) {
				// TechID lookup
				$row = explode(",", $row);
		//		echo "UPDATE FLS_Work_Orders SET TB_ID = '{$row[1]}' WHERE WorkOrderID = '{$row[0]}'<br/>";
				caspioUpdate("FLS_Work_Orders", "TB_ID", "'{$row[1]}'", "WorkOrderID = '{$row[0]}'", false);
			}
			
			$billableFLS = caspioSelectAdv("FLS_Work_Orders", "WorkOrderNo, TB_ID, CAST(PayAmt AS MONEY), WorkOrderID, CONVERT(varchar,DateInvoiced, 120)", "MovedToMysql = '0' AND Invoiced = '1' AND Paid = 'No' AND Deactivated != 'Yes' AND Kickback = '0' AND CAST(PayAmt AS MONEY) != 0.00", "", false, "`", "|", false);*/
		}
		catch (SoapFault $fault) {
			echo "An error occurred when trying to find billable work orders: $fault";
			die();
		}
			
		if (sizeof($billable) > 0) {
//			array_walk($billable, 'resultsToArray2', array("`", "|"));
	
			echo "Pulling billable main site work orders ...<br/>";
			copyBillableToMysql($billable, 0);
			echo "Marking work order as pulled ...<br/>";
//			$markFS = caspioUpdate("Work_Orders", "MovedToMysql", "'1'", "Deactivated = '0' AND MovedToMysql = '0' AND Invoiced = '1' AND TechPaid = '0' AND CAST(Net_Pay_Amount AS MONEY) != 0.00", false);
			$markFS = $db->update('work_orders', array('MovedToMysql' => 1), "Deactivated = '0' AND MovedToMysql = '0' AND Invoiced = '1' AND TechPaid = '0' AND IFNULL(Net_Pay_Amount,0) != 0");

			echo "Total - $markFS<br/>";

			require_once("QB/QuickBooks/syncBillQueue.php");
		}
		else {
			echo "No billable main site work orders<br/>";
		}
		
		echo "<br/><br/>";

/*		if (sizeof($billableFLS) != 1 || $billableFLS[0] != "") {
			array_walk($billableFLS, 'resultsToArray2', array("`", "|"));
	
			echo "Pulling billable FLS work orders ...<br/>";
			copyBillableToMysql($billableFLS, 1);
			echo "Marking work order as pulled ...<br/>";
			$markFLS = caspioUpdate("FLS_Work_Orders", "MovedToMysql", "'1'", "MovedToMysql = '0' AND Invoiced = '1' AND Paid = 'No' AND Deactivated != 'Yes' AND Kickback = '0' AND CAST(PayAmt AS MONEY) != 0.00", false);
			echo "Total - $markFLS<br/>";
		}
		else {
			echo "No billable FLS work orders<br/>";
		}*/

		echo "<br/><br/>";
		echo "Pulling required tech records ...<br/>";
		getRequiredTechInfo();
		echo "Process complete.<br/>";
				
/*				$fieldList = "Invoiced, DateInvoiced";
				$valuesList = "1, '$invoiceDate'";
		
				echo "Updating " . sizeof($billable) . " main site records to 'Invoiced' with an Invoice Date of: " . $invoiceDate . "<br/><br/>";
		
				foreach ($billable as $row) {
					echo "{$row[3]}<br />";
				}

//				$actual = caspioUpdate("Work_Order", $fieldList, $valuesList, "Approved = '1' AND Invoiced = '0'", false);
	
				echo "<br/><br/>" . $actual . " updated!<br/><br/>";
			}
			else
				echo "No main site records found<br/>";

			if (sizeof($billableFLS) != 1 || $billable[0] != "") {
				array_walk($billableFLS, 'resultsToArray');

				copyBillableToMysql($billableFLS, 1);
	
				echo "Updating " . sizeof($billableFLS) . " FLS records to 'Invoiced' with an Invoice Date of: " . $invoiceDate . "<br/><br/>";
		
				foreach ($billableFLS as $row) {
					echo "{$row[3]}<br />";
				}
		
//				$actual = caspioUpdate("FLS_Work_Order", $fieldList, $valuesList, "PayApprove = '1' AND Invoiced = '0' AND Deactivated != 'Yes' AND Kickback = '0'", "WorkOrderID ASC", false);
				
				echo "<br/><br/>" . $actual . " updated!<br/><br/>";
				
				getRequiredTechInfo();
			}
			else
				echo "No FLS records found<br/>";
		}*/
//	}
//	catch (SoapFault $fault) {
//		echo "An error occurred: $fault";
//		die();
//	}
?>
<!--<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>

<script type="text/javascript">
function saveDate()
{
	var expireDate = new Date;
	expireDate.setMonth(expireDate.getMonth()+6);

	var defaultDate = document.getBillable.date.value;

	document.cookie = "defaultDate2=" + defaultDate + ";expires=" + expireDate.toGMTString() + ";path=/";
}

function get_cookie(theCookie)
{
	var search = theCookie + "="
	var returnvalue = "";
	if (document.cookie.length > 0)
	{
		offset = document.cookie.indexOf(search);
		// if cookie exists
		if (offset != -1)
		{
			offset += search.length;
			// set index of beginning of value
			end = document.cookie.indexOf(";", offset);
			// set index of end of cookie value
			if (end == -1) end = document.cookie.length;
			returnvalue=unescape(document.cookie.substring(offset, end));
		}
	}
	return returnvalue;
}

function isValidDate(date) {
	var parts = date.split("/");
	var m = parseInt(parts[0], 10);
	var d = parseInt(parts[1], 10);
	var y = parseInt(parts[2], 10);
	var dateObj = new Date(date);
	return (m == dateObj.getMonth() + 1 && d == dateObj.getDate() && y == dateObj.getFullYear());
}

function checkDefaultDate() {
	var date = document.getBillable.date.value;
	if (isValidDate(date))
		setDefaultDate();
	else
		alert("Invalid Date " + date);
}

function setDefaultDate() {
	var date = document.getBillable.date.value;
	document.getBillable.date.value = date;
	saveDate();
}

$(document).ready(function() {
	document.forms.getBillable.date.value = get_cookie("defaultDate2");
	$('#date').calendar({dateFormat: 'MDY/'});
});

function checkDate(date) {
	if (isValidDate(date))
		return true;
	alert("Invalid Date " + date);
	return false;
}

</script>


<div align="center">
<a href="./">Return to Accounting Page</a><br/><br/>
<form id="getBillable" name="getBillable" method="post" action="<?php $_SERVER['PHP_SELF']?>">
	Click on 'Move to Quickbooks' send all billable work orders to Quickbooks<br/>and mark them as invoiced with the following date:<br/><br/>
	<label for="date">Date (mm/dd/yyyy): </label><input id="date" name="date" type="text" size="10" />
	<input name="save" type="button" value="Save" onClick="checkDefaultDate()" /><br/><br/>
	<input name="submit" type="submit" value="Move to Quickbooks" onClick="return checkDate(document.forms.getBillable.date.value)" />
</form><br/>
</div>-->
<!--- End Content --->
