<?php $page = 'accounting'; ?>
<?php $option = 'accounting'; ?>
<?php $selected = 'Banking'; ?>
<?php require ("../header.php"); ?>
<?php
        require_once("../library/openSSL.php");
        require_once("../library/mySQL.php");
        require_once("../library/caspioAPI.php");
        $privateKey = getBankingPrivateKey($_SESSION["BankingInfoPW"]);
        if (!$privateKey) {
        		$redir = '<script type="text/javascript">';
        		$redir .= 'window.location = "bankingLogin.php"';
        		$redir .= '</script>';
        		echo $redir;
               // header("Location: bankingLogin.php");
                die();
        }
?>

<?php require ("../navBar.php"); ?>
<?php
	
	if (!isset($_GET["id"]) || !is_numeric($_GET["id"])) die();
	$techID = $_GET["id"];

	$result = mysqlQuery("SELECT TechID, FirstName, LastName, PaymentMethod, DATE_FORMAT(DateChange, '%m/%d/%Y %h:%i:%s') AS DateChange, DepositType, BankName, BankAddress1, BankAddress2, BankCity, states_list.State AS BankState, BankZip, Countries.Name AS BankCountry, AccountName, RoutingNum, AccountNum FROM TechBankInfo LEFT JOIN states_list ON states_list.Abbreviation = BankState LEFT JOIN Countries ON Countries.id = BankCountry WHERE TechID = '$techID'");
	
	$techList = array();
		
	$row = mysql_fetch_assoc($result);
	
	if ($row) {
		$techID = $row["TechID"];
		$tech = $row;
		$tech["RoutingNum"] = decryptData($tech["RoutingNum"], $privateKey);
		$tech["AccountNum"] = decryptData($tech["AccountNum"], $privateKey);
	}

	$contactInfo = caspioSelect("TR_Master_List", "PrimaryPhone, PrimaryEmail", "TechID = '$techID'", "");
		
	$info = explode(",", $contactInfo[0]);
	$tech["PrimaryPhone"] = trim($info[0], "'");
	$tech["PrimaryEmail"] = trim($info[1], "'");
		
/*	foreach ($techList as $tech) {
		echo "{$tech['TechID']}, {$tech['FirstName']}, {$tech['LastName']}, {$tech['PrimaryPhone']}, {$tech['PrimaryEmail']}, {$tech['PaymentMethod']}, {$tech['DateChange']}, {$tech['DepositType']}, {$tech['BankName']}, {$tech['BankAddress1']}, {$tech['BankAddress2']}, {$tech['BankCity']}, {$tech['BankState']}, {$tech['BankZip']}, {$tech['BankCountry']}, {$tech['AccountName']}, {$tech['RoutingNum']}, {$tech['AccountNum']}\n";
	}*/
?>
<style  TYPE="text/css">
.label{ font-size:12px; color:#385C7E;}

.resultTable {
	border-spacing: 0px;
	text-align: center;
	color: #000000;
	padding: 0px;
	border-collapse: collapse;
}

.resultTop {
	background-color: #949CCE;
	border-top: 1px solid #000000;
	border-left: 1px solid #000000;
	border-right: 1px solid #000000;
}

.leftHeader {
	border-left: 2px solid #2A497D;
}

.rightHeader {
	border-right: 2px solid #2A497D;
}

.middleHeader {
	border-left: 1px solid #FFFFFF;
	border-right: 1px solid #FFFFFF;
}

.resultHeader a, .resultRow a:hover {
	color: #2A497D;
}

.resultHeader a:hover, .resultRow a {
	color: #6373B5;
}

.resultHeader td {
	color: #000099;
	text-align: center; 
	background-color: #A0A8AA; 
	padding: 7px 10px;
	font-weight: bold;
	cursor: pointer;
	border-top: 2px solid #2A497D;
}

.resultRow {
	background-color: #FFFFFF;
}

.resultRow:hover {
	background-color: #EAEFF5;
}

.leftRow {
	border-left: 2px solid #2A497D;
}

.rightRow {
	border-right: 2px solid #2A497D;
}

.resultRow td {
	padding: 0px 10px;
}

.resultBottom {
	background-color: #A0A8AA;
	border: 2px solid #2A497D;
}
</style>

<br/><br/>


<div style="">
	<table id="BankingInfoTable" cellpadding="0" cellspacing="0" border="0" style="border:#385C7E solid medium; width: 500px; margin: 0px auto;">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="5" style="width: 100%">
					<tr>
						<td class="label">FS Tech ID</td>
						<td>
							<?php echo $tech['TechID'];?>
						</td>
					</tr>
					<tr>
						<td class="label">First Name</td>
						<td>
							<?php echo $tech['FirstName'];?>
						</td>
					</tr>
					<tr>
						<td class="label">Last Name</td>
						<td>
							<?php echo $tech['LastName'];?>
						</td>
					</tr>
					<tr>
						<td class="label">Primary Phone</td>
						<td>
							<?php echo $tech['PrimaryPhone'];?>
						</td>
					</tr>
					<tr>
						<td class="label">Primary Email</td>
						<td>
							<?php echo $tech['PrimaryEmail'];?>
						</td>
					</tr>
					<tr>
						<td class="label">Payment Method</td>
						<td>
							<?php echo $tech['PaymentMethod'];?>
						</td>
					</tr>
					<tr>
						<td class="label">Date Modified</td>
						<td>
							<?php echo $tech['DateChange'];?>
						</td>
					</tr>
					<tr>
						<td class="label">Account Type</td>
						<td>
							<?php echo $tech['DepositType'];?>
						</td>
					</tr>
					<tr>
						<td class="label">Bank Name</td>
						<td>
							<?php echo $tech['BankName'];?>
						</td>
					</tr>
					<tr>
						<td class="label">Bank Address1</td>
						<td>
							<?php echo $tech['BankAddress1'];?>
						</td>
					</tr>
					<tr>
						<td class="label">Bank Address2</td>
						<td>
							<?php echo $tech['BankAddress2'];?>
						</td>
					</tr>
					<tr>
						<td class="label">City</td>
						<td>
							<?php echo $tech['BankCity'];?>
						</td>
					</tr>
					<tr>
						<td class="label">State</td>
						<td>
							<?php echo $tech['BankState'];?>
						</td>
					</tr>
					<tr>
						<td class="label">Zip</td>
						<td>
							<?php echo $tech['BankZip'];?>
						</td>
					</tr>
					<tr>
						<td class="label">Country</td>
						<td>
							<?php echo $tech['BankCountry'];?>
						</td>
					</tr>
					<tr>
						<td class="label">Name on Account</td>
						<td>
							<?php echo $tech['AccountName'];?>
						</td>
					</tr>
					<tr>
						<td class="label">Rounting#</td>
						<td>
							<?php echo $tech['RoutingNum'];?>
						</td>
					</tr>
					<tr>
						<td class="label">Account#</td>
						<td>
							<?php echo $tech['AccountNum'];?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</div>

<!--- End Content --->
<?php require ("../footer.php"); ?>
