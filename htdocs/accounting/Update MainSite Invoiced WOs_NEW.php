<?php $page = 'accounting'; ?>
<?php $option = 'accounting'; ?>
<?php $selected = ''; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->
<h1>Update MainSite Invoiced WOs</h1>
<style type="text/css">@import url(../library/ui.datepicker.css);</style>
<script type="text/javascript" src="../library/ui.datepicker.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('.showCal').datepicker({dateFormat: 'mm/dd/yy'});
});

function saveDate()
{
	var expireDate = new Date;
	expireDate.setMonth(expireDate.getMonth()+6);

	var defaultDate = document.forms[0].date.value;

	document.cookie = "defaultDate2=" + defaultDate + ";expires=" + expireDate.toGMTString() + ";path=/";
}

function get_cookie(theCookie)
{
	var search = theCookie + "="
	var returnvalue = "";
	if (document.cookie.length > 0)
	{
		offset = document.cookie.indexOf(search);
		// if cookie exists
		if (offset != -1)
		{
			offset += search.length;
			// set index of beginning of value
			end = document.cookie.indexOf(";", offset);
			// set index of end of cookie value
			if (end == -1) end = document.cookie.length;
			returnvalue=unescape(document.cookie.substring(offset, end));
		}
	}
	return returnvalue;
}

function isValidDate(date) {
	var parts = date.split("/");
	var m = parseInt(parts[0], 10);
	var d = parseInt(parts[1], 10);
	var y = parseInt(parts[2], 10);
	var dateObj = new Date(date);
	return (m == dateObj.getMonth() + 1 && d == dateObj.getDate() && y == dateObj.getFullYear());
}

function checkDefaultDate() {
	var date = document.forms[0].date.value;
	if (isValidDate(date))
		setDefaultDate();
	else {
		alert("Invalid Date " + date);
		document.forms[0].date.value = get_cookie("defaultDate2");
	}
}

function setDefaultDate() {
	var date = document.forms[0].date.value;
	document.forms[0].date.value = date;
	saveDate();
}
</script>
<div align="center">
<form id="date" name="date">
	<label for="date">Date (mm/dd/yyyy): </label><input name="date" type="text" size="10" class="showCal" onchange="checkDefaultDate()"/>
</form><br/>
<iframe style="width: 100%; height: 1700px" src="https://<?=$_SERVER['SERVER_NAME']?>/reports/reportviewer/report.html?name=Update_Invoice_WOs.prpt&ignoreDefaultDates=true&autoSubmit=false"></iframe>
<script type="text/javascript">
	document.forms[0].date.value = get_cookie("defaultDate2");
</script>
</div>
<!--- End Content --->
<?php require ("../footer.php"); ?>
