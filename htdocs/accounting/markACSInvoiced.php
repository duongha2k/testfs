<?php $page = 'accounting'; ?>
<?php $option = 'accounting'; ?>
<?php $selected = ''; ?>
<?php require_once("../library/timeStamps2.php");?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->
<div align="center"><br/>
<a href="./findACSInvoicableWO.php">Return to ACS Invoicing</a><br/>
<?php
	try {
		require_once("../library/caspioAPI.php");
		require_once("../library/smtpMail.php");
		require_once("../library/acsInvoicing.php");

		$InvoiceDate = $_REQUEST['InvoiceDate'];
		$CompanyID = $_REQUEST['CompanyID'];
		
//		ini_set("display_errors", 1);
				
		set_time_limit(800);
		$CompanyQuery = ($CompanyID != "" ? "AND Company_ID = '$CompanyID'" : "");
		
//		$records = caspioSelect("Work_Orders", "TB_UNID", "Approved = '1' AND Invoiced = '0' $CompanyQuery", "");
		$criteria = "(Approved = '1' OR Deactivated = '1') AND ACSInvoiced = '0' AND ((ReminderAll = '1' OR ReminderAcceptance = '1' OR Reminder24Hr = '1' OR Reminder1Hr = '1' OR ReminderNotMarkComplete = '1' OR ReminderIncomplete = '1' OR CheckInCall = '1' OR CheckOutCall = '1') OR " . WO_ACS_LOG_EXISTS . ") $CompanyQuery";
		$records = caspioSelectAdv("Work_Orders", "TB_UNID, Company_ID, Company_Name, Project_Name, Tech_ID", $criteria, "", false, "`", "|", false);
		
		$fieldList = "ACSInvoiced, ACSDateInvoiced";
		$valuesList = "1, '$InvoiceDate'";

		echo "Updating " . sizeof($records) . " records to 'ACSInvoiced' with an Invoice Date of: " . $InvoiceDate . "<br/><br/>";
		
		$techList = array();
		$invoicedList = array();

		foreach ($records as $row) {
			if ($row == "") continue;
			$row = explode("|", $row);
			$row[1] = trim($row[1], "`");
			$row[2] = trim($row[2], "`");
			$row[3] = trim($row[3], "`");
			$techList[] = $row[4];
			$invoicedList[] = $row[0];
			echo $row[0] . " : " . $InvoiceDate . "<br />";
			Create_TimeStamp($row[0], "Accounting", "Work Order ACS Invoiced", $row[1], $row[2], $row[3], "", "", "");
			flush();
			ob_flush();
		}

		$records = caspioUpdate("Work_Orders", $fieldList, $valuesList, $criteria, false);
		
		echo "<br/><br/>" . $records . " updated!";
				
		echo "<br/>done<br/>";
				
	}
	catch (SoapFault $fault) {
//		smtpMail("Update FLS Invoiced Script", "nobody@fieldsolutions.com", "gerald.bailey@fieldsolutions.com", "Update FLS Invoiced Script Error", "$fault", "$fault", "Staff-Update MainSite Invoiced WOs.php");
	}

?>
</div>
<!--- End Content --->
<?php require ("../footer.php"); ?>