<?php
	// Call to Caspio WO tables with any recently paid WOs
	ini_set("display_errors", 1);
	require_once("../library/smtpMail.php");
	reportError("Quickbooks Sync Paid", "Test", "Sync Starting", "QBSyncPaid.php");
	try {
		require_once("../library/caspioAPI.php");
		require_once("../library/quickbooks.php");
	
		$result = mysqlQuery("SELECT caspio_unique_id, caspio_type, paid_amount, DATE_FORMAT(paid_date, '%m/%d/%Y'), check_number FROM bills WHERE is_paid = '1' AND caspio_sync = '0' AND last_update_by = 'QuickBooks'");
		
		$paidWO = array();
		while ($row = mysql_fetch_row($result)) {
			$paidWO[] = $row;
		}
		
		if (sizeof($paidWO) > 0) {
			copyPaidToCaspio($paidWO);
			
			mysqlQuery("UPDATE bills SET last_update_by = 'Caspio_final', caspio_sync = '1' WHERE is_paid = '1' AND caspio_sync = '0' AND last_update_by = 'QuickBooks'");
		}
		reportError("Quickbooks Sync Paid", "Test", "Sync Finished", "QBSyncPaid.php");
	}
	catch (SoapFault $fault) {
		echo "Error while copying info to caspio: $fault";
		reportError("Quickbooks Sync Paid", "Error Occurred While Syncing", "An error occured while trying to sync the paid work orders with Caspio: $fault", "QBSyncPaid.php");
	}
?>
