<?php $page = "accounting"; ?>
<?php $option = "accounting"; ?>
<?php $selected = "Banking"; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php
	require_once("../library/openSSL.php");
	require_once("../library/quickbooks.php");
	if (isset($_POST["PW"])) {
		$_SESSION["BankingInfoPW"] = $_POST["PW"];
	}
	if (!isset($_SESSION["BankingInfoPW"]) || !($privateKey = getBankingPrivateKey($_SESSION["BankingInfoPW"]))) {
		// not logged in or bad password
?>
<br/><br/>
<div style="text-align: center">
	<form name="login" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>?v=<?=$_GET["v"]?>">
		Enter password to view tech Banking information:<br/><br/>
		<input name="PW" type="password" type="text" />
		<input name="submit" type="submit" value="Submit"/>
	</form>	
</div>
<?php
	}
	else {
		// logged in
?>

<!-- Add Content Here -->

<?php
	//ini_set("display_errors", 1);
	if (isset($_POST["submit"]) && isset($_POST["member_id"]) && $_POST["member_id"] != "") {
		$member_id = mysqlEscape($_POST["member_id"]);
		$publicKey = getBankingPublicKey();
		if (!$publicKey) {
			echo "Unable to save information at this time.<br/>";
			die();
		}
		$ssn = mysqlEscape(encryptData($_POST["ssn"], $publicKey));
		$ein = mysqlEscape(encryptData($_POST["ein"], $publicKey));
		$which_as_taxid = (isset($_POST["which_as_taxid"]) ? mysqlEscape($_POST["which_as_taxid"]) : 0);
		$_POST["w9_company_name"] = trim($_POST["w9_company_name"]);
		$_POST["w9_name"] = trim($_POST["w9_name"]);		
		$w9_company_name = ($_POST["w9_company_name"] == "" ? "NULL" : "'" . mysqlEscape($_POST["w9_company_name"]) . "'");
		$w9_name = ($_POST["w9_name"] == "" ? "NULL" : "'" . mysqlEscape($_POST["w9_name"]) . "'");
				
		$updateQuery = "UPDATE members SET ssn = '$ssn', ein = '$ein', which_as_taxid = '$which_as_taxid', w9_company_name = $w9_company_name, w9_name = $w9_name, last_update = NOW(), last_update_by = 'Caspio' WHERE member_id = '$member_id'";
		
//		echo htmlentities($updateQuery) . "<br/>";

		if (mysqlQuery($updateQuery)) {
			$msg = "Your information has been updated";
		}
	}
			
	if (isset($_GET["v"])) {
		$member_id = mysqlEscape($_GET["v"]);
		$memberInfo = mysqlFetchAssoc(mysqlQuery("SELECT firstname, lastname, ssn, ein, which_as_taxid, w9_company_name, w9_name FROM members WHERE member_id = '$member_id'"));
		if ($memberInfo)
			$memberInfo = $memberInfo[0];
		$memberInfo["ssn"] = decryptData($memberInfo["ssn"], $privateKey);
		$memberInfo["ein"] = decryptData($memberInfo["ein"], $privateKey);
//		print_r($memberInfo);
	}
	
	$memberList = mysqlFetchAssoc(mysqlQuery("SELECT member_id FROM members WHERE iso_affiliation_id IS NULL ORDER BY member_id ASC"));
	$memberSelect = "<option value=\"\">Select Tech</option>";
	foreach ($memberList as $val) {
		$val = $val["member_id"];
		$isMe = ($val == $member_id ? "selected=\"selected\"" : "");
		$memberSelect .= "<option value=\"$val\" $isMe>$val</option>";
	}
	
	
			//	$TechID = $_GET["TechID"];
?>
<style  TYPE="text/css">
.resultsTable {
	margin: 10px auto 0px auto;
}
	
.resultsTable thead {
	text-align: center;
	color: #2A497D;
	border: 1px solid #2A497D;
	background-color: #A0A8AA;
	cursor: default;
}

.resultsTable td {
	padding: 3px 5px;
}

#searchBox {
	margin-top: 40px;
}

.label{ font-size:12px; color:#385C7E;}
.required {
	font-size: 12px;
	color: #FF0000;
}
.footer {
	text-align: center;
	background-color: #BACBDF; 
	border-top: #385C7E solid medium; 
	padding: 5px;
}
.caspioButton
{
/*Back Button Attributes*/
color: #ffffff;
font-size: 12px;
font-family: Verdana;
font-style: normal;
font-weight: bold;
text-align: center;
vertical-align: middle;
border-color: #5496C6;
border-style: solid;
border-width: 1px;
background-color: #032D5F;
/* Forced by default to add space between buttons */
width: auto;
height: auto;
margin: 0 3px;
}

</style>

<br /><br />

<div align="center" id="main">
	<?=$msg?>
	<form id="W9Info" name="W9Info" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>?v=<?=$member_id?>" onSubmit="return validate()">
	<table id="W9InfoTable" cellpadding="0" cellspacing="0" border="0" style="border:#385C7E solid medium; width: 350px">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="5" style="width: 100%">
					<tr>
						<td class="label">Member ID</td>
						<td><select id="member_id" name="member_id" onchange="changeMember()"><?=$memberSelect?></select></td>
					</tr>
					<tr>
						<td class="label">Member Name</td>
						<td><?php echo htmlentities($memberInfo["firstname"] . " " . $memberInfo["lastname"]);?></td>
					</tr>
					<tr>
						<td class="label">Company Name on W9</td>
						<td><input id="w9_company_name" name="w9_company_name" value="<?php echo htmlentities($memberInfo["w9_company_name"])?>" size="30" /></td>
					</tr>
					<tr>
						<td class="label"></td>
						<td><input id="clearW9CompanyName" name="clearW9CompanyName" type="checkbox" <?php echo ($memberInfo["w9_company_name"] == "" ? "checked=\"checked\"" : "")?>/> Use Profile Company Name</td>
					</tr>
                    <tr>
						<td class="label">Name on W9</td>
						<td><input id="w9_name" name="w9_name" value="<?php echo htmlentities($memberInfo["w9_name"])?>" size="30" /></td>
					</tr>
					<tr>
						<td class="label"></td>
						<td><input id="clearW9Name" name="clearW9Name" type="checkbox" <?php echo ($memberInfo["w9_name"] == "" ? "checked=\"checked\"" : "")?>/> Use Profile Name</td>
					</tr>
					<tr>
						<td class="label"><input name="which_as_taxid" type="radio" value="1" <?php echo ($memberInfo["which_as_taxid"] == 1 ? "checked=\"checked\"" : "");?>/>&nbsp;SSN</td>
						<td><input id="ssn" name="ssn" type="text" size="17" maxlength="17" value="<?=$memberInfo["ssn"]?>" /></td>
					</tr>
					<tr>
						<td class="label"><input name="which_as_taxid" type="radio" value="2" <?php echo ($memberInfo["which_as_taxid"] == 2 ? "checked=\"checked\"" : "");?>/>&nbsp;EIN</td>
						<td><input id="ein" name="ein" type="text" size="17" maxlength="17" value="<?=$memberInfo["ein"]?>" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="footer" style="text-align: center">
				<input id="submit" name="submit" type="submit" value="Update" class="caspioButton" />
			</td>
		</tr>
	</table>
	
	</form>
    
    <div id="searchBox">
	    Search for Tech by ID, first or last name: <input id="techQuery" name="techQuery" type="text" style="width: 200px" />&nbsp;&nbsp;
        <input id="runSearch" name="runSearch" type="button" value="Find Techs" />
    </div>
    
    <div id="resultsBox">
    	
    </div>
	

<script type="text/javascript">
var scheduledSearch = null;

function liveSearchTech() {
	if (scheduledSearch != null)
		clearTimeout(scheduledSearch);
	scheduledSearch = setTimeout("findTechs();", 400);
}

function findTechs() {
	var query = $("#techQuery").attr("value");
	if (!query) return;
	$.get("ajax/QBTechSearch.php", {"q" : query}, 
		function(data) {
			if (data == "") {
				$("#resultsBox").html("<br/>No Matching Techs");
				return;
			}
			var rows = data.split("%^");
			var html = "<table class=\"resultsTable\"><thead><tr><td>TechID</td><td>First Name</td><td>Last Name</td><td>Address</td><td>City</td><td>State</td><td>Zip</td></tr></thead><tbody>";
			for (i = 0; i < rows.length; i++) {	
				columns = rows[i].split("|");
				html += "<tr>";
				for (j = 0; j < columns.length; j++) {
					if (j == 0)
						html += "<td><a href=\"<?=$_SERVER['PHP_SELF']?>?v=" + columns[j] + "\">" + columns[j] + "</a></td>";
					else
						html += "<td>" + columns[j] + "</td>";
				}
				html += "</tr>";
			}
			html += "</tbody></table>";
			$("#resultsBox").html(html);
		}
	);
}

$(document).ready(function () {
	$("#techQuery").keyup(liveSearchTech);
	$("#runSearch").click(findTechs);

	$("#w9_company_name").focus(function() {
		$("#clearW9CompanyName").attr("checked", false);
	});
	$("#w9_name").focus(function() {
		$("#clearW9Name").attr("checked", false);
	});
	$("#w9_company_name").blur(function() {
		var val = document.getElementById("w9_company_name").value;
		if (val == "undefined" || val == "")
			$("#clearW9CompanyName").attr("checked", true);
	});
	$("#w9_name").blur(function() {
		var val = document.getElementById("w9_name").value;
		if (val == "undefined" || val == "")
			$("#clearW9Name").attr("checked", true);
	});
	$("#clearW9CompanyName").click(function() {
		if ($("#clearW9CompanyName").attr("checked"))
			$("#w9_company_name").attr("value", "");
	});
	$("#clearW9Name").click(function() {
		if ($("#clearW9Name").attr("checked"))
			$("#w9_name").attr("value", "");
	});
});

function changeMember()
{
	document.location.replace("<?php echo $_SERVER["PHP_SELF"]?>?v=" + document.getElementById("member_id").value);
}

function validate() {
	frm = document.forms.W9Info;
	return true;
}

</script>

</div>
<?php
	} // end logged in
?>
<!--- End Content --->
<?php require ("../footer.php"); ?>

