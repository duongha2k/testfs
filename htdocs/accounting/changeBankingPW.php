<?php $page = 'accounting'; ?>
<?php $option = 'accounting'; ?>
<?php $selected = 'Banking'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php
//if (!array_key_exists("HTTPS", $_SERVER) 
//	header("Location: https://www.fieldsolutions.com{$_SERVER['PHP_SELF']}");
ini_set("display_error", 1);
require_once("../library/openSSL.php");

if (isset($_POST["submit"])) {
	$passwordOld = $_POST["passwordOld"];
	$password = $_POST["passwordNew"];
	
	$privkey = getPrivateKey("../key.pri", $passwordOld);
	if (!$privkey) {
		echo "Invalid current password.\n";
		die();
	}
	
	if (!openssl_pkey_export($privkey, $privatekey, $password)) {
		echo "Unable to create key using this password.\n";
		die();
	}
	
	file_put_contents("/tmp/keyNew.pri", $privatekey);
	//header("Content-disposition: attachment; filename=key.pri");
	//echo $privatekey;
	echo "Password Set";
}

?>
<style type="text/css">
	div.main{
		margin:30px auto; 
		width:50%;
	}
	div.box label{
		display:block;
		margin-bottom: 10px;
	}

	div.box span{
		padding: 3px 10px 0px 0px;
		display: block;
		width: 20%;
		text-align: right;
		float: left;
	}
	
	div.box span.colspan {
		display: block;
		width: 100%;
		text-align: center;
	}

</style>
<div class="main">
	<div class="box">
		<form name="changePW" method="post" onsubmit="return checkNewPassword()" action="<?php echo $_SERVER['PHP_SELF']?>">
			<label for="passwordOld">
				<span>Old Password</span>
				<input name="passwordOld" type="password" />
			</label>
			<label for="passwordNew">
				<span>New Password</span>
				<input name="passwordNew" type="password" />
			</label>
			<label for="passwordRetype">
				<span>Retype</span>
				<input name="passwordRetype" type="password" />
			</label>
			<span class="colspan"><input name="submit" type="submit" value="Submit" /></span>
		</form>
	</div>
</div>
<script type="text/javascript">
	function checkNewPassword() {
		if (document.forms.changePW.passwordRetype.value != document.forms.changePW.passwordNew.value) {
			alert("New password doesn't match retyped password");
			return false;
		}
		if (document.forms.changePW.passwordNew.value == "") {
			alert("Enter a new password");
			return false;
		}
	}
</script>