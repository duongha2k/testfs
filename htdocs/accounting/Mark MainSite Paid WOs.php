<?php $page = 'accounting'; ?>
<?php $option = 'accounting'; ?>
<?php $selected = ''; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->
<div align="center"><br/>
<a href="./">Return to Accounting Page</a><br/>
<?php
	try {
		require_once("../library/caspioAPI.php");
		require_once("../library/smtpMail.php");

		$PaidDate = $_REQUEST['PaidDate'];
		$DateApproved = ($_REQUEST['DateApproved'] != "" ? "AND DateApproved < '{$_REQUEST['DateApproved']}'" : "");
		$DateInvoiced = ($_REQUEST['DateInvoiced'] != "" ? "AND DateInvoiced < '{$_REQUEST['DateInvoiced']}'" : "");
		
		$fieldList = "TechPaid, DatePaid";
		$valuesList = "'1', '$PaidDate'";

		$records = caspioSelect("Work_Orders", "TB_UNID", "Approved = '1' AND Invoiced = '1' AND TechPaid = '0' AND Deactivated != '1' $DateApproved $DateInvoiced", "", false);
		
		echo "Updating " . sizeof($records) . " records to 'Paid' with an Paid Date of: " . $PaidDate . "<br/><br/>";

		foreach ($records as $row) {
			if ($row == "") continue;
			echo $row . " : " . $PaidDate . "<br />";
		}		

		$records = caspioUpdate("Work_Orders", $fieldList, $valuesList, "Approved = '1' AND Invoiced = '1' AND TechPaid = '0' AND Deactivated != '1' $DateApproved $DateInvoiced", false);
				
		echo "<br /><br />" . $records . " updated!";
								
//		print_r( caspioSelect("Work_Orders_TN", "COUNT(TB_UNID)", "Approved = '1' AND Invoiced = '1' AND TechPaid = '0' AND Deactivated != '1' $DateApproved $DateInvoiced", "", false));

//		die();

//		$result = caspioUpdate("Work_Orders_TN", $fieldList, $valuesList, "Approved = '1' AND Invoiced = '1' AND TechPaid = '0' AND Deactivated != '1' $DateApproved $DateInvoiced", false);

//		echo "<br /><br />" . $result . " updated!";

		// NOTE: GROUP BY is contained in criteria field and after at least one criteria. This is a hack of the Caspio API and may not continue to work in the future
/*		$records = caspioSelect("FLS_Work_Orders", "WorkOrderID", "PayApprove = '1' AND Invoiced = '0' AND Deactivated != 'Yes' AND Kickback = '0'", "WorkOrderID ASC", false);
		
		echo "Updating " . sizeof($records) . " records to 'Invoiced' with an Invoice Date of: " . $InvoiceDate;

		$fieldList = "Invoiced, DateInvoiced";
		$valuesList = "1, '$InvoiceDate'";
		
		foreach ($records as $row) {
			
			caspioUpdate("FLS_Work_Orders", $fieldList, $valuesList, "PayApprove = '1' AND Invoiced = '0' AND Deactivated != 'Yes' AND Kickback = '0'", "WorkOrderID ASC", false);
			echo $WorkOrderID . " : " . $InvoiceDate . "<br />";
			
		}
		echo "<br /><br />" . sizeof($records) . " updated!";*/
		
	}
	catch (SoapFault $fault) {
//		smtpMail("Update FLS Invoiced Script", "nobody@fieldsolutions.com", "gerald.bailey@fieldsolutions.com", "Update FLS Invoiced Script Error", "$fault", "$fault", "Staff-Update Paid WOs.php");
	}

?>
</div>
<!--- End Content --->
<?php require ("../footer.php"); ?>