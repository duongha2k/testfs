// ******************************************************
//
// RUNTIME CODE:
//
// ******************************************************

function cancelAction() {
	return false;
}


$(document).ready(function() {
	try {
		$("#Value4_1").change(getProjects);
//		$("#Value5_1").change(setProject);


}
	catch (e) { }
});

// FUNCTION DEFINITIONS
function getProjects() {
	var clientID = $("#Value4_1").attr("value");
	$.get("ajax/projectLookup.php", {"clientID" : clientID},
		function(data) {
		//	var getProjectInfo = data.split("|^");
		var element_dropdown = "Value5_1";
		
		$("#" + element_dropdown).empty();
		index = 1;
		selIndex = 0;
		$("#" + element_dropdown).append("<option value=\"\">Select Project</option>");
		
		var projects = data.split("|"); 
		for (i = 0; i < projects.length; i++) {  
			var parts = projects[i].split("^");  
			var project_name = parts[0];  
			var project_id = parts[1];  
//			if (v != '' && company != v) continue;
			if(parts != "") {
				htmlEle = "<option value=\"" + project_id + "\">" + project_name + "</option>";
				$("#" + element_dropdown).append(htmlEle);
				index++;
			}
		}
		document.getElementById(element_dropdown).selectedIndex = selIndex;
	});
}

function setProject() {
	var theProject = document.getElementById("Value5_1");
	if(theProject.selectedIndex != 0){
		var option = theProject.options[theProject.selectedIndex].value;
		document.forms[0].projectID.value = option;
		document.forms[1].Value5_1.value = option;
	}
}