<?php $page = 'accounting'; ?>
<?php $option = 'accounting'; ?>
<?php $selected = ''; ?>

<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->
<div align="center"><br/>
<a href="./">Return to Accounting Page</a><br/>
<?php
	try {
		require_once("../library/caspioAPI.php");
		require_once("../library/smtpMail.php");

		$InvoiceDate = $_REQUEST['InvoiceDate'];
		
		$records = caspioSelect("FLS_Work_Orders", "WorkOrderID", "PayApprove = '1' AND Invoiced = '0' AND Deactivated != 'Yes' AND Kickback = '0'", "");

		$fieldList = "Invoiced, DateInvoiced";
		$valuesList = "1, '$InvoiceDate'";

		echo "Updating " . sizeof($records) . " records to 'Invoiced' with an Invoice Date of: " . $InvoiceDate . "<br/><br/>";

		foreach ($records as $row) {
			if ($row == "") continue;
			echo $row . " : " . $InvoiceDate . "<br />";
		}

		$records = caspioUpdate("FLS_Work_Orders", $fieldList, $valuesList, "PayApprove = '1' AND Invoiced = '0' AND Deactivated != 'Yes' AND Kickback = '0'", "WorkOrderID ASC", false);
		
		echo "<br/><br/>" . $records . " updated!";
		
//		print_r( caspioSelect("FLS_Work_Orders_TN", "Count(WorkOrderID)", "PayApprove = '1' AND Invoiced = '0' AND Deactivated != 'Yes' AND Kickback = '0'", ""));
		
		// NOTE: GROUP BY is contained in criteria field and after at least one criteria. This is a hack of the Caspio API and may not continue to work in the future
/*		$records = caspioSelect("FLS_Work_Orders", "WorkOrderID", "PayApprove = '1' AND Invoiced = '0' AND Deactivated != 'Yes' AND Kickback = '0'", "WorkOrderID ASC", false);
		
		echo "Updating " . sizeof($records) . " records to 'Invoiced' with an Invoice Date of: " . $InvoiceDate;

		$fieldList = "Invoiced, DateInvoiced";
		$valuesList = "1, '$InvoiceDate'";
		
		foreach ($records as $row) {
			
			caspioUpdate("FLS_Work_Orders", $fieldList, $valuesList, "PayApprove = '1' AND Invoiced = '0' AND Deactivated != 'Yes' AND Kickback = '0'", "WorkOrderID ASC", false);
			echo $WorkOrderID . " : " . $InvoiceDate . "<br />";
			
		}
		echo "<br /><br />" . sizeof($records) . " updated!";*/
		
	}
	catch (SoapFault $fault) {
//		smtpMail("Update FLS Invoiced Script", "nobody@fieldsolutions.com", "gerald.bailey@fieldsolutions.com", "Update FLS Invoiced Script Error", "$fault", "$fault", "Staff-Update Invoiced WOs.php");
	}

?>
</div>
<!--- End Content --->
<?php require ("../footer.php"); ?>