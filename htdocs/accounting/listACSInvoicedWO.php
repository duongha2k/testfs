<?php $page = 'accounting'; ?>
<?php $option = 'accounting'; ?>
<?php $selected = ''; ?>
<?php
// =============================================================
// CONSTANTS
require_once("../library/caspioAPI.php");
require_once("../library/acsInvoicing.php");
$ShowQuery = false;

$Current_Page = $_SERVER['PHP_SELF'];
$Max_Results = 25;
$Page_Block_Size = 10;
$Default_Sort_Order = "TB_UNID";
$Default_Sort_Dir = "ASC";

$sortByParam = $Default_Sort_Order;
$sortDir = $Default_Sort_Dir;

$max_records = 5000;
$first_record = 1;


if (isset($_GET['inc'])) {
	$max_records = $_GET['inc'];
}

if (!isset($_GET['first'])) {
?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<div align="center">
<!-- Add Content Here -->

<style type="text/css">
	div#search {
		width: 1000px;
		margin: auto;
	}

	div#results {
	}

	div#no_results {
		width: 100px;
		margin: auto;
		margin-top: 35px;
	}

	#search {

	}

	#search dl {
		bborder: 1px solid black;
		padding-bottom: 15px;
	}

	#search dl dt {
		float: left;
		width: 130px;
		padding-bottom: 4px;
		min-height: 1px; /* required to give layout to empty elements */
	}
	#search dl dd {
		margin-left: 170px;
		padding-bottom: 4px;
	}

	#bottom dl dd {
		margin-left: 100px;
	}

	#column_1 dl.ratings {
		margin-top: 10px;
	}

	dl.ratings select {
		margin-right: 5px;
	}

	#column_1 {
		float: left;
		padding-right: 20px;
	}
	#column_2 {
		float: right;
	}

	#bottom {
		clear: both;
	}
	#ratings_1 {
		float: left;
	}
	#ratings_2 {
		margin-left: 432px;
	}

	dl.ratings {
		width: 425px;
	}



	p.submit {
		clear: both;
		margin: auto;
		padding-top: 0;
		width: 200px;
	}
	p.submit input {
		background-color: #385c7e;
		color: white;
		font-weight: bold;
	}


	input#vSubmit {
		margin: 10px;
		padding: 2px;
	}

	dd.email select {
		width: 100px;
	}

	dl.ratings select.skill {
		width: 150px;
	}
	dl.ratings select.rating {
		width: 70px;
	}

	dd.date select {
		margin-right: 10px;
		width: 70px;
	}
	dd.accept_terms select {
		width: 140px;
	}
	dd.state select {
		width: 200px;
	}
	dd.client_denied select {
		width: 120px;
	}
	dd.fls_status select {
		width: 120px;
	}
	select.yes_no {
		width: 90px;
	}
	select.blank {
		width: 100px;
	}
	dd.certifications select {
		width: 300px;
	}
	.zipcode input {
		width: 45px;
	}
	.zipcode select {
		width: 145px;
	}


</style>


<style  TYPE="text/css">
.label{ font-size:12px; color:#385C7E;}

.resultTable {
	border: 1px solid black;
	border-spacing: 0;
	text-align: center;
	color: #000000;
	padding: 0;
	border-collapse: collapse;
}

.resultTop {
	background-color: #949CCE;
	border-top: 1px solid #000000;
	border-left: 2px solid #000000;
	border-right: 2px solid #000000;
}

.leftHeader {
	border-left: 2px solid #2A497D;
}

.rightHeader {
	border-right: 2px solid #2A497D;
}

.middleHeader {
	border-left: 1px solid #FFFFFF;
	border-right: 1px solid #FFFFFF;
}

.resultHeader a, .resultRow a:hover {
	color: #2A497D;
}

.resultHeader a:hover, .resultRow a {
	color: #6373B5;
}

.resultHeader td {
	ccolor: #000099;
	color: #444;
	text-align: center;
	background-color: #A0A8AA;
	padding: 7px 10px;
	font-weight: bold;
	ccursor: pointer;
	border-top: 2px solid #2A497D;
}

.resultRow {
	background-color: #FFFFFF;
	border: 1px solid #EAEFF5;
}

.resultRow:hover {
	background-color: #EAEFF5;
}

.leftRow {
	border-left: 2px solid #2A497D;
}

.rightRow {
	border-right: 2px solid #2A497D;
}

.resultRow td {
	ppadding: 0px 10px;
	padding: 1px 3px;
}

.pagination td {
	padding: 2px 10px;
}
.pagination td.left {
	border-left: 2px solid #2A497D;
}
.pagination td.right {
	border-right: 2px solid #2A497D;
}

.pagination td img {
	padding: 0 5px;
}

.resultBottom td {
	background-color: #A0A8AA;
	border: 2px solid #2A497D;
}
.resultBottom td.left {
	border-right: none;
}

.resultBottom td.right {
	border-left: none;
}
</style>
<?php
}



// =============================================================

// =============================================================
// FUNCTIONS
/**
 * rm() -- Vigorously erase files and directories.
 *
 * @param $fileglob mixed If string, must be a file name (foo.txt), glob pattern (*.txt), or directory name.
 *                        If array, must be an array of file names, glob patterns, or directories.
 */
function rm($fileglob)
{
    if (is_string($fileglob)) {
        if (is_file($fileglob)) {
            return unlink($fileglob);
        } else if (is_dir($fileglob)) {
            $ok = rm("$fileglob/*");
            if (! $ok) {
                return false;
            }
            return rmdir($fileglob);
        } else {
            $matching = glob($fileglob);
            if ($matching === false) {
                trigger_error(sprintf('No files match supplied glob %s', $fileglob), E_USER_WARNING);
                return false;
            }
            $rcs = array_map('rm', $matching);
            if (in_array(false, $rcs)) {
                return false;
            }
        }
    } else if (is_array($fileglob)) {
        $rcs = array_map('rm', $fileglob);
        if (in_array(false, $rcs)) {
            return false;
        }
    } else {
        trigger_error('Param #1 must be filename or glob pattern, or array of filenames or glob patterns', E_USER_ERROR);
        return false;
    }

    return true;
}

function get_date($array, $index) {
	if (array_key_exists($index, $array)) {
		return format_date($array[$index]);
	} else {
		return "";
	}
}

// FUNCTION myImplode
//
// surrounds the strings in an array with other strings
//
// useful in adding tags or quotes to the strings in an array
//
function myImplode($before, $after, $glue, $array){
		$output = '';
    $nbItem = count($array);
    $i = 1;
    foreach($array as $item){
        if($i < $nbItem){
            $output .= "$before$item$after$glue";
        }else $output .= "$before$item$after";
        $i++;
    }
    return $output;
}

// FUNCTION str_truncate
//
// truncates a string by the number of characters specified
//
function str_truncate($text, $chars=25) {
	if (strlen($text) > 25) {
		if ($text != "") {
			// Change to the number of characters you want to display
			$text = $text." ";
			$text = substr($text,0,$chars);

			$text = substr($text,0,strrpos($text,' '));

			$text .= "...";
		}
	}
	return $text;
}



// function format_date
//
// outputs date string in "MM/DD/YY" format.
//
function format_date($date_str) {
	$output = $date_str;
	if ($output != "") {
		$output = strftime("%m/%d/%y", strtotime($output));
	}
	return $output;
}

// FUNCTION process
//
// for use when processing an array returned by caspioSelectAdv.
// trims the quotes off and filters "NULL" into ""
//
function process($str) {
	$output = trim($str, "`");
	switch ($str) {
		case "NULL":
			$output = "";
			break;
		case "True":
		case "Yes":
			$output = "Y";
			break;
		case "False":
		case "No":
			$output = "N";
			break;
	}
	return $output;
}

// FUNCTION dateFields
//
// writes the greater-than and less-than input fields for date
// comparisons
//
function dateFields($name, $size=8) {
	$nameGreater = $name . "Greater";
	$nameLess = $name . "Less";



	return "(>=) <input name='$nameGreater' id='$nameGreater' size='$size' /> (<=) <input name='$nameLess' id='$nameLess' size='$size' />";
}

// FUNCTION yesNoSelect
//
// writes a select with three options, Yes, No, and "Any".
//
function yesNoSelect($name, $id="", $value="1", $selected="") {
	if ($id == "") {
		$id = $name;
	}
	$selected_0 = "";
	$selected_1 = "";
	switch ($selected) {
		case "Yes":
		case "1":
			$selected_1 = "selected='selected'";
			break;
		case "No":
		case "0":
			$selected_0 = "selected='selected'";
			break;
	}

	if ($value == "1") {
		$yes = '1';
		$no = '0';
	} else {
		$yes = 'Yes';
		$no= 'No';
	}

	return "<select name='$name' id='$id' class='yes_no'>
		<option value=''>Any&nbsp;&nbsp;</option>
		<option value='$yes' $selected_1>Yes</option>
		<option value='$no' $selected_0>No</option>
	 </select>";
}


// FUNCTION yesNoBlankSelect
//
// writes a select with three options, Yes, No, and "Any".
//
function yesNoBlankSelect($name, $id="", $value="1", $selected="") {
	if ($id == "") {
		$id = $name;
	}
	$selected_0 = "";
	$selected_1 = "";
	switch ($selected) {
		case "Yes":
		case "1":
			$selected_1 = "selected='selected'";
			break;
		case "No":
		case "0":
			$selected_0 = "selected='selected'";
			break;
		case "Blank":
			$selected_blank = "selected='selected'";
	}

	if ($value == "1") {
		$yes = '1';
		$no = '0';
	} else {
		$yes = 'Yes';
		$no= 'No';
	}

	return "<select name='$name' id='$id' class='yes_no'>
		<option value=''>Any&nbsp;&nbsp;</option>
		<option value='$yes' $selected_1>Yes</option>
		<option value='$no' $selected_0>No</option>
		<option value='X', $selected_blank>Blank</option>
	 </select>";
}

function blankSelect($field_name) {
$field = $field_name . "Blank";
$output = "<select class='blank' name='$field' id='$field'>
	<option value=''>Any</option>
	<option value='Y'>Is Blank</option>
	<option value='X'>Is Not Blank</option>
</select>";
return $output;
}

// FUNCTION getFormField
//
// returns the given field name from the posted fields
//
function getFormField($field_name) {
	$result = "";
	if (isset($_POST[$field_name])) {
		$result = $_POST[$field_name];
	}
	return $result;
}

function addQuery($query_string, $var_name, $variable, $type="=", $is_blank=false) {
	$add_to_string = "";
	if ($is_blank || ($variable != "" && $variable != "NULL")) {

		if ($type == "LIKE") {
			$variable = "'%$variable%'";
		}
		else {
			$variable = "'$variable'";
		}
		switch ($is_blank) {
			case "Y":
				$type = "IS";
				$variable = "NULL";
				$add_query = " or $var_name LIKE ''";
				break;
			case "X":
				$type = "IS NOT";
				$variable = "NULL";
				$add_query = " and datalength($var_name) <> 0";
				break;
		}
		$add_to_string = appendQuery($add_to_string, "($var_name $type $variable  $add_query)");

		/*
		switch ($is_blank) {
			case "Y":
				$add_to_string = $add_to_string . " or $var_name LIKE ''";
				break;
			case "X":
				$add_to_string = $add_to_string . " and $var_name NOT LIKE ''";
				break;
		}
		*/
	} elseif ($variable == "NULL") {
		$add_to_string = appendQuery($add_to_string, "$var_name $type NULL");
	}
	$query_string = appendQuery($query_string, $add_to_string);
	return $query_string;
}

function appendQuery($query_string, $new_query) {
	if ($new_query != "") {
		if ($query_string != "") {
			$query_string .= " and $new_query";
		} else {
			$query_string = $new_query;
		}
	}
	return $query_string;
}

function addQueryEqual($query_string, $var_name, $variable, $is_blank=false) {
	return addQuery($query_string, $var_name, $variable, "=", $is_blank);
}

function addQueryLike($query_string, $var_name, $variable, $is_blank=false) {
	return addQuery($query_string, $var_name, $variable, "LIKE", $is_blank);
}

function addQueryGreater($query_string, $var_name, $variable, $is_blank=false) {
	return addQuery($query_string, $var_name, $variable, ">", $is_blank);
}

function addQueryLess($query_string, $var_name, $variable, $is_blank=false) {
	return addQuery($query_string, $var_name, $variable, "<", $is_blank);
}

function addQueryGreaterEqual($query_string, $var_name, $variable, $is_blank=false) {
	return addQuery($query_string, $var_name, $variable, ">=", $is_blank);
}

function addQueryLessEqual($query_string, $var_name, $variable, $is_blank=false) {
	return addQuery($query_string, $var_name, $variable, "<=", $is_blank);
}

// formats an array into a string for use in a SQL
// IN (...) clause
//
// format_list(['1','2','3']) => "('1','2','3')"
//
function format_list($results_array) {
	for ($i=0; $i < sizeof($results_array); $i++) {
		$results_array[$i] = str_replace("NULL", "", $results_array[$i]);
		$results_array[$i] = str_replace("`", "", $results_array[$i]);
		$results_array[$i] = caspioEscape($results_array[$i]);
		$results_array[$i] = "'" . $results_array[$i] . "'";
	}

	$list = implode(",", $results_array);
	return $list;
}


// FUNCTION queryWOs(fields, conditions, order)
//

function queryWOs($fields, $conditions, $order="") {
	if ($showQuery) {
		echo "<p>QUERY: caspioSelectAdv(\"Work_Orders\", \"$fields\", \"$conditions\", \"$order\", ...)</p>";
	}
//	ini_set("display_errors",1);
	$result = caspioSelectAdv("Work_Orders", $fields, $conditions, $order, FALSE, "`", "|", false);
	return $result;
}

function queryWOsFromList($fields, $list, $order="TechID ASC") {
	$output = array();
	if (sizeof($list) != 0) {
		$list_string = format_list($list);
		$output = queryWOs($fields, "TB_UNID in ($list_string)", $order);
	}
	return $output;
}

function compareDate($field_name, $greater, $less, $query) {
	$end_result = array();

	if ($less != "" || $greater != "") {
		$condition = "";

		if ($less != "" && $greater == "") {
			$condition = "$field_name <= '$less' and $query";
		}
		elseif ($greater != "" && $less == "") {
			$condition = "$field_name >= '$greater' and $query";
		}
		elseif ($less != "" && $greater != "") {
			$condition = "$field_name <= '$less' and $field_name >= '$greater' and $query";
		}

		$end_result = queryWOs("TB_UNID", $condition);
	}

	return $end_result;
}


function getLatest($results_array) {

	$results_hash = array();
	$results_array = $results_array[0];

	foreach ($results_array as $entry) {

		$parts = explode("|",$entry);

		$field_1 = process($parts[0]);
		$field_2 = process($parts[1]);

		// store only the first (most recent) login entry for each username

		//echo "<p>field_1: $field_1, field_2: $field_2</p>";
		if (!array_key_exists($field_1, $results_hash)) {
			$results_hash[$field_1] = $field_2;
		}
	}

	return $results_hash;
}

function getLatestResults($table_name, $techIDfield, $date_field, $id_list) {
	if (is_array($id_list)) {
		$a = array();

		$username_array = array();
		for ($i = 0; $i < sizeof($id_list); $i += 1000) {
			$a[] = array_slice($id_list, $i, 1000);
		}

		$output_array = array();

		foreach ($a as $array) {
			if (sizeof($array) > 0) {

				//echo "X ";

				$array = array_map("trimQuote", $array);
				$array = array_map("caspioEscape", $array);
				$array = array_map("addQuote", $array);

				$id_list = implode(", ", $array);

				$temp_array[] = caspioSelectAdv($table_name, "$techIDfield, $date_field", "$techIDfield in ($id_list)", "$date_field DESC", FALSE, "`", "|");

				//echo "<p>$output_array: " . sizeof($output_array) . "</p>";

				foreach ($temp_array as $array) {
					$output_array = array_merge($output_array, $temp_array);
				}
				unset($temp_array);
			}
		}

	} else
	{
		$output_array = caspioSelectAdv($table_name, "$techIDfield, $date_field", "$techIDfield in ($id_list)", "$date_field DESC", FALSE, "`", "|");
	}

	//echo "<p>OUTPUT ARRAY: " . sizeof($output_array) . "</p>";
	$output = getLatest($output_array);

	//echo "<p>OUTPUT: " . implode(", ", $output) . "</p>";

	return $output;
}

function trimQuote($str) {
	return trim($str, "`");
}

function addQuote($str) {
	return "'$str'";
}

function columnHeading($field_name, $link_name="") {
	$output_dir = "";
	$output = "";
	if ($link_name == "") {
		$link_name = str_replace(" ", "", $field_name);
	}
	$dir = "ASC";
	if (isset($_SESSION["sort"]) && $_SESSION["sort"] == $link_name) {
		if (isset($_SESSION["dir"]) && $_SESSION["dir"] == "ASC") {
			$dir = "DESC";
		}
		if ($dir == "ASC") {
			$output_dir = " <img src='https://bridge.caspio.net/images/set5_descending.gif' />";
		} else {
			$output_dir = " <img src='https://bridge.caspio.net/images/set5_ascending.gif' />";
		}
	}
	$link_name = urlencode($link_name);
	//$output = "<nobr>";
	$output .= "<a href='?sort=$link_name&dir=$dir'>$field_name</a>";
	$output .= " $output_dir";
	//$output .= "</nobr>";

	return $output;
}

function round_up($num) {
	if ($num != (int) $num) {
		$num = (int) $num + 1;
	}
	return (int) $num;
}


function rangeQuery($clause, $less, $greater) {
	$output = "";
	if ($less != "" || $greater != "") {

		if ($less != "" && $greater == "") {
			$output = "$clause <= '$less'";
		}
		elseif ($greater != "" && $less == "") {
			$output = "$clause >= '$greater'";
		}
		elseif ($less != "" && $greater != "") {
			$output = "$clause <= '$less' and $clause >= '$greater'";
		}
	}
  return $output;
}



// END FUNCTIONS
//===================================================================

if ($_POST) {
// BASIC QUERY

$query = "ACSInvoiced = '1'";

	if ($_POST["Company"] != "None") {
		$_SESSION["searchACSCompany"] = caspioEscape($_POST["Company"]);
		$query = addQueryEqual($query, "Company_ID", caspioEscape($_POST["Company"]));
	}
	else {
		$_SESSION["searchACSCompany"] = "";
	}
			
	if ($ShowQuery) {
		echo "<p>QUERY: $query ";
	}
	
} // end of "if $_POST"



// GET PAGE VARIABLE

$page = 1;
$offset = 0;

if (isset($_GET['page'])) {
	$page = $_GET['page'];
	if ($page < 1) {
		$page = 1;
	}
}


// IF RESULTS HAVE BEEN POSTED, SHOW THE RESULTS

// DETERMINE THE SORT ORDER

if (isset($_GET['page']) || isset($_GET['sort'])|| isset($_GET['download'])  || $_POST) {

// MAIN QUERY

	if (isset($_GET['sort'])) {
		$sortByParam = $_GET['sort'];
	}
	if (isset($_GET['dir'])) {
		$sortDir = $_GET['dir'];
	}

	$_SESSION['sort'] = $sortByParam;
	$_SESSION['dir'] = $sortDir;

	$sortBy = $sortByParam;

	// if search criteria have been posted, run the search:
	if ($_POST)
	{
		$gross_results_base = queryWOs("TB_UNID", $query, "$sortBy $sortDir");

		foreach ($gross_results_base as $entry) {
			$results_array[] = process($entry);
		}
		$gross_results = array_unique($results_array);
		$_SESSION['gross_results'] = $gross_results;
	}

	// if results haven't been posted, load the results array
	// from the session
	if (!$_POST)
	{
		$gross_results = $_SESSION['gross_results'];
	}

	$result_count = sizeof($gross_results);
	$net_result_count = sizeof($gross_results);

	$results_list = format_list($gross_results);
	
	// NO RESULTS

	if ($results_list == "" || $result_count < 1 || $gross_results[0] == "")
	{
		echo "<div id='no_results'>";
		echo "No results.";
		echo "</div>";
		die();
	}

if (!isset($_GET['download'])) {
	// --------------------------------------------------
	// PAGES ARE DIVIDED INTO BLOCKS FOR NAVIGATION
	// there are $Page_Block_Size pages per each block

	// Page_Block_Size - max number of pages per block
	// Max_Results - max number of entries per page
	// total_pages - total number of pages of results
	// page - current page
	// offset - the offset before the first record on the page
	// total_blocks - total number of page blocks
	// first_page - first page of the current page block
	// last_page - last page of the current page block


	// 100 / 25 = 4 pages
	// 104 / 25 = 5 pages
	$total_pages = round_up(($result_count / $Max_Results));

	if ($page > $total_pages) {
		$page = $total_pages;
	}

	// ((1-1) * 25) = (0 * 25) = 0
	// ((2-1) * 25) = (1 * 25) = 25
	// ((3-1) * 25) = (2 * 25) = 50
	$offset = (($page - 1) * $Max_Results);

	// 1000 results / 25 per page = 40 pages = 4 blocks

	// 40 pages / 10 = 4 page blocks
	// 44 pages / 10 = 5 page blocks
	$total_blocks = round_up($total_pages / $Page_Block_Size);

	$page_block = round_up(($page / $Page_Block_Size) + 1);

	$first_page = (int) (($page_block-1) * $Page_Block_Size) + 1;
	$last_page = $first_page + $Page_Block_Size - 1;

	if ($page < $first_page) {
		$page_block = $page_block - 1;
		$first_page = $first_page - $Page_Block_Size;
		$last_page = $last_page - $Page_Block_Size;
	}

	if ($last_page > $total_pages) {
		$last_page = $total_pages;
	}


	// --------------------------------------------------
	// GETS THE FIRST $Max_Results ENTRIES FROM THE $results ARRAY

	if ($result_count < $Max_Results) {
		$net_results = $gross_results;
	}
	else {
		$net_results = array_slice($gross_results, $offset, $Max_Results);
	}
	$Max_Page = $result_count / $Max_Results;

	// --------------------------------------------------
	// FORMATS THE LIST OF IDs FOR USE IN LAST QUERY

	$results_list = format_list($net_results);
} elseif (!isset($_GET['first'])) {

		echo "<h1>Download search results</h1>";

		echo "<p>There are " . sizeof($gross_results) . " search results total.</p>";

		for ($i=1; $i <= sizeof($gross_results); $i = $i + $max_records) {
			$f = $i;
			$m = $f + $max_records - 1;
			if ($m > sizeof($gross_results)) {
				$m = sizeof($gross_results);
			}
			echo "<p>Download results <a href='$Current_Page?download&first=$f&max=$m'>$f to $m</a></p>";
		}

require ("../footer.php");

}

if (isset($_GET["first"])) {
	$first_record = (int) $_GET["first"];

	if (isset($_GET["max"])) {
		$max_records = (int) $_GET["max"];
	}

	$last_record = $max_records;

	if ($last_record > sizeof($gross_results)) {
		$last_record = sizeof($gross_results);
	}

	$csv_filename = "InvoicedACS_" . $first_record . "_" . $last_record . "_" . session_id() . ".csv";

}

if (!isset($_GET['download']) || isset($_GET['first'])) {

	// --------------------------------------------------
	// RETURN THE FULL DATA FOR THE CURRENT PAGE OF RESULTS
	// as more fields are added to the search, the system gets overwhelmed and the text fields must be limited more
	$text_field_length = 257;
	//$text_field_length = 1;
//ini_set("display_errors",1);
	$csv_columns = array("TB_UNID", "WO_ID", "Project_Name", "Company_Name", "Tech_ID", "Tech_FName", "Tech_LName", "Date_Assigned", "Approved", "Deactivated", "ACSDateInvoiced", "DateInvoiced", "IVRPriceBundle", "IVRPriceEach", "ReminderAll", "SMSBlast", "ReminderAcceptance", "Reminder24Hr", "Reminder1Hr", "CheckInCall", "CheckOutCall", "ReminderNotMarkComplete", "ReminderIncomplete", "Total");

	$columns = array("TB_UNID", "WO_ID", "Project_Name", "Company_Name", "Tech_ID", "Tech_FName", "Tech_LName", "CONVERT(varchar, Date_Assigned, 101)", "Approved", "Deactivated", "CONVERT(varchar, ACSDateInvoiced, 101)", "CONVERT(varchar, DateInvoiced, 101)", "CAST(" . ACS_COST_BUNDLE . " AS DECIMAL(3,2))", "CAST(" . ACS_COST_EACH . " AS DECIMAL(3,2))", HW_REMINDER_ALL, HW_REMINDER_SMSBLAST, HW_REMINDER_ACCEPTANCE, HW_REMINDER_24HR, HW_REMINDER_1HR, HW_CHECK_IN_CALL, HW_CHECK_OUT_CALL, HW_REMINDER_NOT_MARK_COMPLETE, HW_REMINDER_INCOMPLETE, "CAST(" . CALCULATED_ACS_COST . " AS DECIMAL(3,2))");
	$columns_list = implode(", ", $columns);

	if (isset($_GET['first'])) {
		$results_array = array_slice($gross_results, $first_record-1, $max_records - $first_record + 1);
	} else {
		$results_array = $net_results;
	}
	//echo "<p>SIZEOF RESULTS: " . sizeof($results_array) . "</p>";

	$results = queryWOsFromList($columns_list, $results_array, "$sortBy $sortDir");

	$net_result_count = sizeof($results);
	//echo $net_result_count;
	
	if (!isset($_GET['first'])) {


// ==================================================================
// DISPLAY SEARCH RESULTS


	echo "<div id='results'>";

	$column_count = sizeof($columns);
?>
		<table cellpadding='10' class="resultTable">
			<tr class="resultTop"><td colspan="<?= $column_count ?>" valign="left"><a href="?download&amp;s=<?= rand(1,1000) ?>"><img src="https://bridge.caspio.net/images/box_blue.gif" align="left" border="0" /></a></td></tr>
			<tr class="resultHeader">
				<td class="leftHeader"><?= columnHeading('WO_UNID') ?></td>
				<td><?= columnHeading('WO_ID') ?></td>
				<td><?= columnHeading('Project_Name') ?></td>
				<td><?= columnHeading('Company_Name') ?></td>
				<td><?= columnHeading('Tech_ID') ?></td>
				<td><?= columnHeading('Tech_FName') ?></td>
				<td><?= columnHeading('Tech_LName') ?></td>
				<td><?= columnHeading('Date_Assigned', $columns[7]) ?></td>
				<td><?= columnHeading('Approved', $columns[8]) ?></td>
				<td><?= columnHeading('Deactivated', $columns[9]) ?></td>
				<td><?= columnHeading('ACS Date Invoiced', $columns[10]) ?></td>
				<td><?= columnHeading('Regular Date Invoiced', $columns[11]) ?></td>
				<td><?= columnHeading('Price Bundle', $columns[12]) ?></td>
				<td><?= columnHeading('Price Each', $columns[13]) ?></td>
				<td><?= columnHeading('ReminderAll', $columns[14] ) ?></td>
				<td><?= columnHeading('SMS', $columns[15] ) ?></td>
				<td><?= columnHeading('ReminderAcceptance', $columns[16] ) ?></td>
				<td><?= columnHeading('Reminder24Hr', $columns[17] ) ?></td>
				<td><?= columnHeading('Reminder1Hr', $columns[18] ) ?></td>
				<td><?= columnHeading('CheckInCall', $columns[19] ) ?></td>
				<td><?= columnHeading('CheckOutCall', $columns[20] ) ?></td>
				<td><?= columnHeading('ReminderNotMarkComplete', $columns[21] ) ?></td>
				<td><?= columnHeading('ReminderIncomplete', $columns[22] ) ?></td>
				<td><?= columnHeading('Total', $columns[23] ) ?></td>
			</tr>
<?php
} // of (isset GET first)
if (isset($_GET['first'])) {
	// CREATE HEADER AND OPEN CSV FILE
	header("Content-disposition: attachment; filename=$csv_filename");
	$fp = fopen("/tmp/$csv_filename", 'w');

	fputcsv($fp, $csv_columns);
	//echo implode(", ", $csv_columns) . "<br />";
}

	$str_length = 80;

	if (isset($_GET['first'])) {
		$str_length = 256;
	} else {
		$str_length = 80;
	}

	// PARSE QUERY RESULTS INTO FIELDS
	foreach ($results as $result) {

		$parts = explode("|",$result);
		$WOUNID = process($parts[0]);
		$WO_ID = process($parts[1]);
		$projectName = process($parts[2]);
		$companyName = process($parts[3]);
		$techID = process($parts[4]);
		$techFName = process($parts[5]);
		$techLName = process($parts[6]);
		$dateAssigned = process($parts[7]);
		$approved = process($parts[8]);
		$deactivated = process($parts[9]);
		$acsDateInvoiced = process($parts[10]);
		$dateInvoiced = process($parts[11]);
		$ivrPriceBundle = process($parts[12]);
		$ivrPriceEach = process($parts[13]);
		$reminderAll = process($parts[14]);
		$SMSBlast = process($parts[15]);
		$reminderAcceptance = process($parts[16]);
		$reminder24Hr = process($parts[17]);
		$reminder1Hr = process($parts[18]);
		$checkInCall = process($parts[19]);
		$checkOutCall = process($parts[20]);
		$reminderNotMarkComplete = process($parts[21]);
		$reminderIncomplete = process($parts[22]);
		$total = process($parts[23]);

	if (!isset($_GET['first'])) {
		// IF IT'S NOT A DOWNLOAD, DISPLAY THE RESULTS

		echo "<tr class='resultRow'>";
		echo "<td class='leftRow'>$WOUNID</td>";
		echo "<td>$WO_ID</td>";
		echo "<td>$projectName</td>";
		echo "<td>$companyName</td>";
		echo "<td>$techID</td>";
		echo "<td>$techFName</td>";
		echo "<td>$techLName</td>";
		echo "<td>$dateAssigned</td>";
		echo "<td>$approved</td>";
		echo "<td>$deactivated</td>";
		echo "<td>$acsDateInvoiced</td>";
		echo "<td>$dateInvoiced</td>";
		echo "<td>$ivrPriceBundle</td>";
		echo "<td>$ivrPriceEach</td>";
		echo "<td>$reminderAll</td>";
		echo "<td>$SMSBlast</td>";
		echo "<td>$reminderAcceptance</td>";
		echo "<td>$reminder24Hr</td>";
		echo "<td>$reminder1Hr</td>";
		echo "<td>$checkInCall</td>";
		echo "<td>$checkOutCall</td>";
		echo "<td>$reminderNotMarkComplete</td>";
		echo "<td>$reminderIncomplete</td>";
		echo "<td class='rightRow'>$total</td>";
		echo "</tr>";
	} else {
		// WRITE THE RESULTS TO CSV FILE

		//echo implode(", ", $formattedFields) . "<br /><br />";
		fputcsv($fp, array($WOUNID, $WO_ID, $projectName, $companyName, $techID, $techFName, $techLName, $dateAssigned, $approved, $deactivated, $ACSDateInvoiced, $dateInvoiced, $ivrPriceBundle, $ivrPriceEach, $reminderAll, $SMSBlast,$reminderAcceptance, $reminder24Hr, $reminder1Hr, $checkInCall, $checkOutCall, $reminderNotMarkComplete, $reminderIncomplete, $total));
	}
}


	if (!isset($_GET['first'])) {
		// IF NOT A DOWNLOAD, DISPLAY PAGINATION

?>
<tr class="pagination">
	<td colspan="17" class="left">
  <?php

		$next_page = $page + 1;
		$prev_page = $page - 1;

		$sort_params = "&sort=$sortByParam&dir=$sortDir";

		if ($page_block > 1) {
			$prev_block = $first_page - $Page_Block_Size;
			echo "<a href='$Current_Page?page=$prev_block$sort_params'><img src='https://bridge.caspio.net/images/set5_prev10.gif' /></a>";
		}

		if ($page > 1) {
			echo "<a href='$Current_Page?page=$prev_page$sort_params'><img src='https://bridge.caspio.net/images/set5_prev.gif' /></a>";
		}
		
		if ($last_page != 1) {
			for ($i=$first_page; $i <= $last_page; $i++) {
				if ($page != $i) {
					echo "<a href='$Current_Page?page=$i$sort_params'>$i</a> ";
				} else {
					echo "$i ";
				}
			}
		}
		
		if ($page < $Max_Page) {
			echo "<a href='$Current_Page?page=$next_page$sort_params'><img src='https://bridge.caspio.net/images/set5_next.gif' /></a>";
		}

		if ($page_block < $total_blocks) {
			$next_block = $last_page + 1;
			echo "<a href='$Current_Page?page=$next_block$sort_params'><img src='https://bridge.caspio.net/images/set5_next10.gif' /></a>";
		}
	?>
  </td>
  <td colspan="<?= $column_count - 10 ?>" class="right"></td>
</tr>
<tr class="resultBottom">
				<td class="left" colspan="17">
        Results <?= $offset + 1 ?> - <?=  $offset + $net_result_count; ?> of <?= $result_count ?>
        </td>
        <td class="right" colspan="<?= $column_count - 10 ?>"></td>
			</tr>
<?php

	echo "</table>";
	echo "</div>";
}

		} // end if (!isset GET first)
		
		if (isset($_GET['first'])) {
			// CLOSE THE CSV FILE AND SEND THE CONTENTS

			fclose($fp);

			echo file_get_contents("/tmp/$csv_filename");

			rm("/tmp/$csv_filename");  // delete the file

		}
}
else
{
	$companyList = caspioSelectAdv("TR_Client_List", "DISTINCT CompanyName, Company_ID", "", "CompanyName ASC", false, "`", "|", false);		
	$companyHtml = "<option value=\"None\" selected=\"selected\">All Companies</option>";
	foreach ($companyList as $company) {
		$company = explode("|", $company);
		$name = trim($company[0], "`");
		$id = trim($company[1], "`");
		$companyHtml .= "<option value=\"$id\">$name</option>";
	}
?>


<!-- SEARCH CRITERIA -->

<div id="search">


<form id="search_form" name="search_form" action="<?= $Current_Page ?>" method="post">

<!--
  <p class="submit"><input type="submit" /></p>
-->
<div id="column_1">

<dl>
  <dt>Company</dt>
	<dd class="companyID">
    <select id="Company" name="Company">
		<?=$companyHtml?>
	</select>
    </dd>
</dl>
  <p class="submit"><input type="submit" /></p>
</form>
</div>




<?php
}
if (!isset($_GET['first'])) {
?>
</div>
<!--- End Content --->
<?php require ("../footer.php"); ?>
<?php
}
?>