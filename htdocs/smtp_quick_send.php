<?php
session_start();
require_once ("library/smtpMail.php");

/*
	This is used for sending emails using GET Parameters.
	NOTE:  Emails sent uisng this DO NOT include footers.
*/

// check referer
if (!isset($_SESSION["referer"])) {
//	if (!isset($_SERVER['HTTP_REFERER']) || strpos(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST), "fieldsolutions.com") === false) die();
}

if (!isset($_GET["vFromName"]) ||
	!isset($_GET["vFromEmail"]) ||
	!isset($_GET["vSubject"]) ||
	!isset($_GET["eList"]) ||
	!isset($_GET["vMessage"]) ||
	!isset($_GET["Caller"])) 
	die();

	# $result = "Success";	 
	$calledFrom = (empty($_SERVER['HTTP_REFERER']) ? "Unknown" : $_SERVER['HTTP_REFERER']);

	$vFromName = urldecode($_GET["vFromName"]);
	$vFromEmail = urldecode($_GET["vFromEmail"]);
	$vSubject = stripslashes(urldecode($_GET["vSubject"]));
	$eList = urldecode($_GET["eList"]);
	$vMessage = stripslashes(urldecode($_GET["vMessage"]));
	$caller = urldecode($_GET["Caller"]);
/*
	echo($vFromName . "<br>");
	echo($vFromEmail . "<br>");
	echo($vSubject . "<br>");
	echo($eList . "<br>");
	echo($vMessage . "<br>");
	echo($Caller . "<br><br>");
*/
	smtpMailLogReceived($vFromName, $vFromEmail, $eList, $vSubject, $vMessage, nl2br($vMessage), $caller);
//	echo "<br><br><b>Your email has been sent!</b>";
//	echo "<p><input type=\"button\" value=\"Close Window\" onclick=\"window.close();\" /></p>";
	echo "<script type=\"text/javascript\">window.close();</script>";



?>
