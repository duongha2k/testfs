<?php

require ("headerSimple.php");
require_once("library/smtpMail.php");
$win = $_REQUEST["win"];
if (isset($win))
{
    //$wo = new Core_WorkOrder(401933);
    $apiclass = new Core_Api_Class();
    $wo = $apiclass->getWorkOrdersWithWinNum($win, '', 'WIN_NUM', '');
    $wo = $wo[0];
    $woaddifields = $apiclass->getWorkOrderAdditionalFields($win);

    if (!empty($wo))
    {
        $to='';
        if(!empty($woaddifields)){
            $to = $woaddifields['ACSNotifiPOC_Email'];        
        }

        if (!empty($to))
        {
            if (!empty($wo['StartDate']) && $wo['StartDate'] != "0000-00-00")
            {
                $StartDate = date_format(new DateTime($wo['StartDate']), "m/d/Y");
            }
            $message = "<br/>";
            $message .= "<p>Please be aware that FieldSolutions was unable to contact the Site Contact. FieldSolutions  recommends confirming that there will be someone available to provide the technician access to the site.</p>";
            $message .="<p>WIN#: " . $wo['WIN_NUM'] . "<br/>";
            $message .="Client Work Order ID: " . $wo['WO_ID'] . "<br/>";
            $message .="Project: " . $wo['Project_Name'] . "<br/>";
            $message .="Start Date: " . $StartDate . " " . $wo['StartTime'] . "<br/>";
            $message .="Site Name: " . $wo['SiteName'] . "<br/>";
            $message .="Site #: " . $wo['SiteNumber'] . "<br/>";
            $message .="Site Address: " . $wo['Address'] . ", " . $wo['City'] . ", " . $wo['State'] . "" . $wo['Zipcode'] . "<br/>";
            $message .="Site Contact Name: " . $wo['Site_Contact_Name'] . "<br/>";
            $message .="Site Phone #: " . $wo['SitePhone'] . "<br/>";
            $message .="FS-Tech ID#: " . $wo['Tech_ID'] . "<br/>";
            $message .="Tech Name: " . $wo['Tech_FName'] . " " . $wo['Tech_LName'] . "<br/>";
            $message .="Tech Phone #: " . $wo['TechPhone'] . "<br/>";
            $message .="Tech Email: " . $wo['TechEmail'] . "</p>";
            $message .="<p>Thank you,<br/>";
            $message .="Your FieldSolutions Team</p>";
            $message .= "<br/>";
            $subject = "WIN# " . $wo['WIN_NUM'] . ": Unable to Reach Site Contact";
            $fromName = "FieldSolutions";
            $fromEmail = "support@fieldsolutions.com";
            @$caller = "EmergencyContactBackOut";
            //echo("<br/>to: $to<br/>");print_r($message);//test
            smtpMailLogReceived($fromName, $fromEmail, $to, $subject, $message, $message, $caller);
        }
    }
}