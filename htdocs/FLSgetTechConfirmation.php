<?php

// This script retrieves all tech jobs that are to be completed in 48 hours
// An email is sent to the tech in which he clicks a link to verify that the job will be
// completed.


try {

require_once("library/caspioAPI.php");
require_once("library/smtpMail.php");

// Set Variables
//$tomorrow = date('m/d/Y', strtotime('now'));//today for test purposes	
$tomorrow = date('m-d-Y', strtotime('+2 days'));//look ahead 48 hours
$result = "Success"; 
$calledFrom = (empty($_SERVER['HTTP_REFERER']) ? "Unknown" : $_SERVER['HTTP_REFERER']);// library needs this

// Get Project info
$project = caspioSelect("FLS_Projects", "ProjectNo, From_Email, ProjectName, Coordinator, Coordinator_Phone", "", false);

$projectCount = sizeof($project);

// Get workorders and tech info 
$records = caspioSelectAdv("FLS_Work_Orders", "PjctStartDate, WorkOrderID, PayAmt, PjctStartTime, TB_ID, SiteName, Project_ID, TechCheckedIn_48hrs, WorkOrderNo, Address1, City, State, Zip, FLSComments", "PjctStartDate = '$tomorrow' AND TechCheckedIn_48hrs='0' AND IsNull(Deactivated, '') = '' AND Kickback = '0' AND Project_ID!='0' AND TB_ID !=' '", "PjctStartDate", false, "'", "|");

// Counts # of records returned
$count = sizeof($records);

/* Takes a single row returned by a select and returns an array of Fields */
function getPipedFields($row) {
		$row = str_replace("'", "", $row); // gets rid of quotes
		return explode("|", $row);
	}
	
// Populate array with workorders
foreach ($records as $order) {
	
	$fields = getPipedFields($order);
		
	// Get data from array and set values for email
	$startDate = $fields[0];
	$startDate = date("m/d/Y", strtotime($startDate));
	$WO_ID = $fields[1];
	$PayAmount = str_replace("$", "", $fields[2]);
	$StartTime = $fields[3];
	$TB_ID = $fields[4];
	$siteName = $fields[5];	
	$ProjectID = $fields[6];
	$checkedin = $fields[7];
	$workOrderNo = $fields[8];
	$Address1 = $fields[9];
	$City = $fields[10];
	$State = $fields[11];
	$Zip = $fields[12];
	$FLSComments = $fields[13];
	
	// Get Tech info
	if (is_numeric($TB_ID) == 'true'){ // Make sure tech id is a number
	$tech = caspioSelect("TR_Master_List", "PrimaryEmail", "TechID='$TB_ID'", false);
	
	foreach ($tech as $techorder) {
		
		$techFields = getFields($techorder);
		$sendToEmail = $techFields[0];	
	}
	
	// Populate array with project info
	foreach ($project as $order) {
	
		$projectFields = getFields($order);
		
		if ($projectFields[0] == $ProjectID){
	
			$vFromEmail = $projectFields[1];	// From Email address
			$projectName = $projectFields[2];
			
			$filterOut = array(",", "."); //remove odd char from coordinator name
			$vFromName = str_replace($filterOut, "", $projectFields[3]);// Coordinator
			$projectCCphone = $projectFields[4];
			
			// use if project send to email is blank
			if ($vFromEmail == ""){
				$vFromEmail = "callcoordinators@ftxs.fujitsu.com";
		}
		
		$vSubject = "REMINDER NOTICE Project: $projectName - $siteName";	
		
		$eList = $sendToEmail;
		//$eList = "collin.mcgarry@fieldsolutions.com";
		@$caller = "FLSgetTechConfirmation";
		
		$vMessage = "Dear Technician,/r/r This is a reminder - You are scheduled for work. Please see the details below then click the link to confirm that you will make this install. If you are not going to make this install, it is very important that you contact the person that recruited you for the work./r/r Follow this link to confirm (REQUIRED!): https://www.fieldsolutions.com/techs/FLSTechCheckIn.php?woID=$WO_ID /r/r Work order number: $workOrderNo/r Date of work: $startDate/r Start Time: $StartTime/r Site Name: $siteName/r Address: $Address1/r City: $City/r State: $State/r Zip: $Zip/r/r Comments: $FLSComments/r/r Amount of Pay: $ $PayAmount/r/r/r Coordinator: $vFromName/r Email: $vFromEmail/r Phone: $projectCCphone/r/r/r Thank you./r/r Regards,/r/r Field Solutions/r Web: www.fieldsolutions.com";
		
		$html_message = "<p>Dear Technician,</p><p>This is a reminder - You are scheduled for work. Please see the details below then click the link to confirm that you will make this install. If you are not going to make this install, it is very important that you contact the person that recruited you for the work.</p><p><a href='https://www.fieldsolutions.com/techs/FLSTechCheckIn.php?woID=$WO_ID'>Click here to confirm</a> <b>(REQUIRED!)</b></p><p>Work order number: $workOrderNo<br>Date of work: $startDate<br>Start Time: $StartTime</p><p>Site Name: $siteName<br>Address: $Address1<br>City: $City<br>State: $State<br>Zip: $Zip</p><p>Amount of Pay: $ $PayAmount</p><p>Coordinator: $vFromName<br>Email: $vFromEmail<br>Phone: $projectCCphone</p><p>Thank you.</p><p>Regards,</p><p>Field Solutions<br>Web: <a href='http://www.fieldsolutions.com'>www.fieldsolutions.com</a></p>";
	
		// Emails Tech
		smtpMail($vFromName, $vFromEmail, $eList, $vSubject, $vMessage, $html_message, $caller);
	

   	}
   }
  }

 }
}
catch (SoapFault $fault) {
		
smtpMail("FLS Tech Confirmation Script", "nobody@fieldsolutions.com", "collin.mcgarry@fieldsolutions.com,gerald.bailey@fieldsolutions.com", "FLS Tech Confirmation Script Error", "$fault", "$fault", "FLSgetTechConfirmation.php");
}
	
?>