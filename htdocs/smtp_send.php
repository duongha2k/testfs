<?php
require_once ("library/caspioAPI.php");
require_once ("library/smtpMail.php");


$validToken = '31d9717ed0fac2e3a9bafbd55fd34286';
if (isset($_REQUEST['token']) && $_REQUEST['token'] == $validToken) {
	foreach ($_REQUEST as $k=>$v) {
		$_POST[$k] = $v;
	}
	$_POST['vMessage'] = nl2br($_POST['vMessage']);
}
/* 
!!!!!!!!!!!!!!!!! IMPORTANT !!!!!!!!!!!!!!!!!!!!!!!
Inputs using either GET method or curl GET/POST should be encoded using:
	for PHP: urlencode(string)
	for Javascript: encodeURIComponent(string)
	
Inputs using POST should be decoded using stripslashes
*/

// check for well formed request and ignores if not
if (!isset($_POST["vFromName"]) ||
	!isset($_POST["vFromEmail"]) ||
	!isset($_POST["vSubject"]) ||
	!isset($_POST["eList"]) ||
	!isset($_POST["vMessage"])) 
	die();

$result = "Success";

if (!isset($_SERVER['HTTP_REFERER']) || strpos(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST), "fieldsolutions.com") === false) {
	if (!isset($_POST['token']) || $_POST['token'] != $validToken)
		$result = "Failure: Called from unknown host - " . $_SERVER['HTTP_REFERER'];
}

$calledFrom = (empty($_SERVER['HTTP_REFERER']) ? "Unknown" : $_SERVER['HTTP_REFERER']);

$vFromName = urldecode($_POST["vFromName"]);
$vFromEmail = urldecode($_POST["vFromEmail"]);
$vSubject = stripslashes(urldecode($_POST["vSubject"]));
$eList = urldecode($_POST["eList"]);

$eList = Core_Tech::filterOutEmailContactOptOut($eList);
// send copy to self?
$eList .= (isset($_POST["copySelf"]) ? ",$vFromEmail" : "");
$vMessage = stripslashes(urldecode($_POST["vMessage"]));
@$caller = urldecode($_POST["caller"]);
@$footer = $_POST["footer"];

//$footer = "FLS";

// check for email injection attacks
if (eregi("(\r|\n)", $from) || contains_bad_str($from))
	$result = "Failure: from = $from - Suspected injection attempt - mail not being sent.";

// end check for email injection attacks

if ($result == "Success") {
// send mail, if no problems so far
/*	$headers = "From: $from";
	// Generate a boundary string
	$semi_rand = md5(time());
	$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";

	// Add the headers for plain / html email
	$headers .= "\nMIME-Version: 1.0\n" .
				"Content-Type: multipart/alternative;\n" .
				" boundary=\"{$mime_boundary}\"";*/
	
	$html_message = $vMessage;
	$html_message = str_replace(chr(13) . chr(10), '<br/>', $html_message);
		
	// add message footer
	switch ($footer) {
		case "FLS":
	        $html_message.="<br/><br/><br/><br/>Note: If you have not yet been through online training to be activated as a Tech with the FLS program<br/>and/or do not have a Badge and IDs yet, please do not respond to this email, you are not eligible.<br/>To get yourself eligible, make sure that you have gone through training beginning, at the link below and<br/>that you have your Badge and IDs.<br/><br/><a href='http://www.flsupport.com/3.html'>FLS online training</a>";

			$html_message.="<br/><br/>Have you visited the new Tech Community website? Click <a href='http://groups.google.com/group/mytechnicianspace/'>here</a> for fun and information on the tech community";
		
			$html_message .= "<br/><br/>You are receiving this email as a registered technician on www.fieldsolutions.com.<br>Click <a href='http://www.fieldsolutions.com/unsubscribe/'>here</a> to stop receiving future e-mails from us.";
			break;
		case "Client":
			$html_message.="<br/><br/>Have you visited the new Tech Community website? Click <a href='http://groups.google.com/group/mytechnicianspace/'>here</a> for fun and information on the tech community";
		
			$html_message .= "<br/><br/>You are receiving this email as a registered technician on www.fieldsolutions.com.<br>Click <a href='http://www.fieldsolutions.com/unsubscribe/'>here</a> to stop receiving future e-mails from us.";
		default:
			break;
	}
	
	// Plain text version of message
/*	$vMessage = str_replace('<br/>', chr(13) . chr(10), $html_message); 
	
	// Add a multipart boundary above the plain message
	$vMessage = "This is a multi-part message in MIME format.\n\n" .
				"--{$mime_boundary}\n" .
				"Content-Type: text/plain; charset=\"iso-8859-1\"\n" .
				"Content-Transfer-Encoding: 7bit\n\n" .
				$vMessage . "\n\n";
			
	// Add html message
	$vMessage .= "--{$mime_boundary}\n" .
				"Content-Type: text/html; charset=\"iso-8859-1\"\n" .
				"Content-Transfer-Encoding: 7bit\n\n" .
				$html_message . "\n\n" .
				"--{$mime_boundary}--\n";*/
				
//	$eList = "codem01@gmail.com";
	
	smtpMailLogReceived($vFromName, $vFromEmail, $eList, $vSubject, $vMessage, $html_message, $caller);
	echo "<br><br><b>Your email has been sent!</b>";
	echo "<p><input type=\"button\" value=\"Close Window\" onclick=\"window.close();\" /></p>";
/*	$recp =	explode(",", $eList);
		
	foreach ($recp as $to) {
//		if (!@mail($to, $vSubject, $vMessage, $headers))
//			$result .= ", $to failed";
		$message = new Mail_Mime();
		$message->setTXTBody($vMessage);
		$message->setHTMLBody($html_message);
		$body = $message->get();
		$extraheaders = array("From"=>"$from", "Subject"=>"$vSubject");
		$headers = $message->headers($extraheaders);

		// Send via SMTP
		$host = "mail.fieldsolutions.com";
		$username = "admin@technicianbureau.net";
		$password = "Blaster~0705";

/*		$host = "mail.fieldsolutions.com";
		$username = "trung.ngo+fieldsolutions.com";
		$password = "clutch34";

		$smtp = Mail::factory('smtp',
			array ('host' => $host,
			'auth' => true,
			'username' => $username,
			'password' => $password));
			
		$mail = $smtp->send($to, $headers, $body);		
		if (PEAR::isError($mail))
			$result .= ", $to failed";		
	}*/
}
?>	

