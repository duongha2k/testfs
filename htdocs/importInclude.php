<?php
	class MyEnum {
	}
	
	$FIELD_EXISTS = new MyEnum();
	
	class caspioFieldProperties {
		var $type, $unique;
		
		public function caspioFieldProperties($t, $u) {
			$this->type = $t;
			$this->unique = $u;
		}
		public function checkValue($value) {
			$result = false;
			switch ($this->type) {
				case "ShortText":
				case "LongText":
					$result = $this->checkText($value);
					break;
				case "AutoNumber":
				case "Number":
					$result = $this->checkNumber($value);
					break;
				case "Date/Time":
					$result = $this->checkDateTime($value);
					break;
				case "Yes/No":
					$result = $this->checkYesNo($value);
					break;					
				case "File":
					$result = $this->checkFile($value);
					break;
			}
			return $result;
		}
		
		public function getType() {
			return $this->type;
		}
		
		protected function checkText($value) {
			// long and short text
			return true;
		}
		protected function checkNumber($value) {
			// number and autonumber
			return (is_numeric($value) || $value == "");
		}
		protected function checkDateTime($value) {
			// date/time
			if ($value == "") return true;
			$parse = date_parse($value); 
			return ($parse && $parse["error_count"] == 0 && checkdate($parse["month"], $parse["day"], $parse["year"]));
		}
		protected function checkYesNo($value) {
			// Yes / No
//			$value = trim($value);
			return ($value == "0" || $value == "1" || $value == "");
		}
		protected function checkFile($value) {
			// File
			return true;
		}
	}	

	class TwoFieldValidator {
		var $nameMap, $value;
		var $col1, $col2;
		
		public function TwoFieldValidator($validFieldsTable, $vfc1, $vfc2, $c1, $c2) {
			$this->col1 = $c1;
			$this->col2 = $c2;
			$results = caspioSelectAdv($validFieldsTable, "$vfc1, $vfc2", "", "",false, "`", "|");
			$this->nameMap = array();
			foreach ($results as $row) {
				$fields = str_replace("`", "", $row);
				$fields = explode("|", $fields);
				$this->nameMap[$fields[0]] = $fields[1];
			}
			$this->value = array();
		}

		public function generateErrorMsg($row, $errorCount) {
			$name = $this->value[$this->col1];
			$id = $this->value[$this->col2];
			generateError("Row <a href=\"#$errorCount\">$row</a>: Values for " . $this->col1 . " and " . $this->col2 . " do not match ('$name' / '$id')");
		}

		public function resetValue() {
			$this->value = array();
		}

		public function readyToValidate() {
			return (array_key_exists($this->col1, $this->value) && array_key_exists($this->col2, $this->value));
		}

		public function setValue($name, $value) {
			$this->value[$name] = $value;
			return $this->readyToValidate();
		}

		public function validate() {
//			echo $this->nameMap[$this->value[$this->col1]] . " " . $this->value[$this->col2];
			return (array_key_exists($this->value[$this->col1], $this->nameMap) && $this->nameMap[$this->value[$this->col1]] == $this->value[$this->col2]);
		}
	}
	

	function checkMustEqual($name, $value, $column) {
		global $FIELD_EXISTS;
		// returns false if bad data
		$checkRequired = array_key_exists($name, $column);
		if ($checkRequired && is_array($column[$name])) {
				$compareStr = array();
				foreach ($column[$name] as $validVal)
					$compareStr[] = "\"$value\" == \"$validVal\"";
				$compareStr = implode(" || ", $compareStr);
				eval("\$result = ($compareStr);");
				return $result;
		}
		return (!$checkRequired || $value == $column[$name] || $column[$name] === $FIELD_EXISTS);
	}
	
	function checkMissingMustEqualColumns($column, $import) {
		$columnMap = array_flip($import);
		foreach ($column as $name => $value) {
			if (!array_key_exists($name, $columnMap))
				generateError("Column list error: Missing column $name");
		}
	}

?>