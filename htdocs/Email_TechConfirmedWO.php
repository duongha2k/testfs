<?php

require ("headerSimple.php");
require_once("library/smtpMail.php");
$win = $_REQUEST["win"];
if (isset($win))
{
    $apiclass = new Core_Api_Class();
    $apiProjectClass = new Core_Api_ProjectClass();
    
    $wo = $apiclass->getWorkOrdersWithWinNum($win, '', 'WIN_NUM', '');
    $wo = $wo[0];

    $owner = $wo['WorkOrderOwner'];
    $projectID =  $wo['Project_ID'];
    
    //--- get info from 'my-settings'
    $future = new Core_FutureWorkOrderInfo;
    $row = $future->find($owner)->toArray();
    $futureInfo = $row[0];
    //--- get info from project
    $projectInfo = $apiProjectClass->getProjectById($projectID);
    //--- 
    /*WorkConfirmedByTechEmailTo
ReceiveWorkConfirmedByTechEmail*/
    $mySetting_Email = '';
    if(!empty($futureInfo)){
        $mySetting_Email = $futureInfo['WorkConfirmedByTechEmailTo'];    
    }
    $receiveEmail = '';
    $prj_Email = '';
    if(!empty($projectInfo)){
        $receiveEmail = $projectInfo['ReceiveWorkConfirmedByTechEmail'];
        $prj_Email = $projectInfo['WorkConfirmedByTechEmailTo'];
    }

    if (!empty($wo) && !empty($receiveEmail))
    {
        $to='';
        if(!empty($mySetting_Email)){
            $to = $mySetting_Email;        
        } else {
            $to = $prj_Email;        
        }
        
        if (!empty($to))
        {
            if(!empty($wo['StartDate']) && $wo['StartDate'] !="0000-00-00")
            {
                $StartDate = date_format(new DateTime($wo['StartDate']), "m/d/Y");
            }

            $fromName = "Tech Confirmed - FieldSolutions";
            $fromEmail = "no-replies@fieldsolutions.com";
            $subject = "WIN# ".$wo['WIN_NUM'].": Work Order ID [".$wo['WO_ID']."] CONFIRMED by Tech";   
                     
            $message = "<br/>";
            $message .= "<p>The technician assigned to WIN# ".$wo['WIN_NUM']." Client WO ID# ".$wo['WO_ID']." confirmed that they will be on-site as scheduled.</p>";
            $message .="<p>Project: " . $wo['Project_Name'] . "<br/>";
            $message .="Headline: " . $wo['Headline'] . "<br/>";
            $message .="Region: " . $wo['Region'] . "<br/>";
            $message .="Route: " . $wo['Route'] . "<br/>";
            $message .="Site Name: " . $wo['SiteName'] . "<br/>";
            $message .="Site #: " . $wo['SiteNumber'] . "<br/>";
            $message .="Site Address: " . $wo['Address'] . "<br/> " . $wo['City'] . ", " . $wo['State'] . " " . $wo['Zipcode'] . "<br/>";
            $message .="Start Date/Time: " . $StartDate . " " . $wo['StartTime'] . "<br/>";
            $message .="FS-Tech ID#: " . $wo['Tech_ID'] . "<br/>";
            $message .="Tech Name: " . $wo['Tech_FName'] . " " . $wo['Tech_LName'] . "<br/>";
            $message .="Tech Phone: " . $wo['TechPhone'] . "<br/>";
            $message .="Tech Email: " . $wo['TechEmail'] . "</p>";
            
            $message .="<p>Thank you,<br/>";
            $message .="Your FieldSolutions Team</p>";
            $message .= "<br/>";
            
            @$caller = "EmailTechAcceptedWO";
            
            echo("<br/>to: $to");echo("<br/>fromName: $fromName");echo("<br/>fromEmail: $fromEmail");echo("<br/>subject: $subject");print_r($message);//test
            //smtpMailLogReceived($fromName, $fromEmail, $to, $subject, $message, $message, $caller);
        }
    }
}