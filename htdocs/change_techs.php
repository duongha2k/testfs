<?php

// ================================================================================
// This code is used to copy Preferrred Techs from one client over to another. GB |
// ================================================================================

//ini_set("display_errors","stdout");

require_once("library/caspioAPI.php");
require ("headerStartSession.php");

$companyList = "";
	try{
		$selectCompanies = $db->select()->distinct();
		$selectCompanies->from(Core_Database::TABLE_CLIENT_PREFERRED_TECHS, array("CompanyID"))->order(array('CompanyID ASC'));
		$companyList = $db->fetchAll($selectCompanies);
		//$result = $result && sizeof($result) > 0;
	}catch(Exception $e){
		$companyList = null;
	}
//$companyList = caspioSelectAdv("Client_Preferred_Techs", "DISTINCT CompanyID", "", "CompanyID ASC", false, "`", "|");		

$fromCompanyHtml = "<option value=\"None\" selected=\"selected\">From Company</option>";
$toCompanyHtml = "<option value=\"None\" selected=\"selected\">To Company</option>";
foreach ($companyList as $company) {
	//$company = explode("|", $company);
	//$id = trim($company[0], "`");
	$id = trim($company['CompanyID']);
	
	$fromCompanyHtml .= "<option value=\"$id\">$id</option>";
	$toCompanyHtml .= "<option value=\"$id\">$id</option>";
}

$toList = "";
	try{
		$toCompanies = $db->select()->distinct();
		$toCompanies->from(Core_Database::TABLE_CLIENTS, array("Company_ID"))->order(array('Company_ID ASC'));
		$toList = $db->fetchAll($toCompanies);
		//$result = $result && sizeof($result) > 0;
	}catch(Exception $e){
		$toList = null;
	}

//$toList = caspioSelectAdv("TR_Client_List", "DISTINCT Company_ID", "", "Company_ID ASC", false, "`", "|");		
$toCompanyHtml = "<option value=\"None\" selected=\"selected\">To Company</option>";
foreach ($toList as $toCompany) {
	//$toCompany = explode("|", $toCompany);
	//$toID = trim($toCompany[0], "`");
	$toID = trim($toCompany['Company_ID']);
	
	$fromCompanyHtml .= "<option value=\"$toID\">$toID</option>";
	$toCompanyHtml .= "<option value=\"$toID\">$toID</option>";
}

if (isset($_POST["submit"])) {
	$fromCompany = $_POST["FromCompany"];
	$toCompany = $_POST["ToCompany"];

	"<p>Copying, please wait...</p>";
	
	try{
		$fromCompanies = $db->select()->distinct();
		$fromCompanies->from(Core_Database::TABLE_CLIENT_PREFERRED_TECHS, array("CompanyID", "Tech_ID", "PreferLevel"));
		$results = $db->fetchAll($fromCompanies);
		//$result = $result && sizeof($result) > 0;
	}catch(Exception $e){
		$results = null;
	}
	
	//$results = caspioSelectAdv("Client_Preferred_Techs", "Tech_ID, PreferLevel", "CompanyID = '$fromCompany'", "Tech_ID ASC", FALSE, "'", "|");


	foreach ($results as $result) {
		//$parts = explode("|", $result);
		//$techID = trim($parts[0], "`");	
		
		$techID = trim($result["Tech_ID"]);	
		//$preferLevel = trim($parts[1], "`");		
		$preferLevel = trim($result["PreferLevel"]);
		
		Core_Tech::addPreferredTech($toCompany, $techID, $preferLevel);
		//caspioInsert("Client_Preferred_Techs", "Tech_ID, PreferLevel, CompanyID", "'$techID', '$preferLevel', 'RFT'");
	}

	//$results = caspioSelectAdv("Client_Preferred_Techs_GB", "Tech_ID, PreferLevel", "CompanyID = '$toCompany'", "Tech_ID ASC", FALSE, "'", "|");
	$results = Core_Tech::getClientPreferredTechsArray($toCompany);
	echo "<p>" . sizeof($results) . " preferred techs copied.</p>";

}

?>

<div id="projectSearch">
<form id="copyPrefTechs" name="copyPrefTechs" action="<?=$_SERVER['PHP_SELF']?>" method="post">
<select id="FromCompany" name="FromCompany">
<?=$fromCompanyHtml?>
</select>

<select id="ToCompany" name="ToCompany">
<?=$toCompanyHtml?>
</select>

<input id="submit" name="submit" type="submit" value="Copy" class="caspioButton" />
</form>
</div>

<?php

// Commented out the following to keep it from running by accident.  Uncomment to use this code.

/*

$results = caspioSelectAdv("Client_Preferred_Techs", "Tech_ID, PreferLevel", "CompanyID = 'CTS'", "Tech_ID ASC", FALSE, "'", "|");

echo "<p>" . sizeof($results) . "</p>";

	foreach ($results as $result) {
		$parts = explode("|", $result);
		
		$techID = trim($parts[0], "`");		
		$preferLevel = trim($parts[1], "`");		

		caspioInsert("Client_Preferred_Techs", "Tech_ID, PreferLevel, CompanyID", "'$techID', '$preferLevel', 'RFT'");
		}
		

$results = caspioSelectAdv("Client_Preferred_Techs", "Tech_ID, PreferLevel", "CompanyID = 'RFT'", "Tech_ID ASC", FALSE, "'", "|");

echo "<p>" . sizeof($results) . "</p>";

*/

?>
