<?php
	try {
		require_once("library/caspioAPI.php");
		require_once("library/smtpMail.php");
		
		$cmd = "";
		$warning = "";
		if (!isset($_GET["cmd"])) {
			if (!isset($argv[1])) {
				echo "Command missing";
				die();				
			}
			else
				$cmd = $argv[1];
		}
		else
			$cmd = $_GET["cmd"];
		switch ($cmd) {
			case "FLSIncomplete":
				$yesterday = date('m-d-Y', strtotime('-1 days'));
				$sixWeeksAgo = date('m-d-Y', strtotime('-6 weeks'));
//				$notCompleteOrder = caspioSelect("FLS_Work_Orders", "FLSTechID, WorkOrderNo, PjctStartDate, TechName, MisssingInfo", "IsNull(FLSTechID, '') != '' AND PjctStartDate <= '$yesterday' AND PjctStartDate >= '$sixWeeksAgo' AND TechComplete = '0' AND PayApprove = '0' AND Kickback = '0' AND Invoiced = '0' AND IsNull(Deactivated, '') = ''", "", false);
				$notCompleteOrder = caspioSelect("FLS_Work_Orders", "FLSTechID, WorkOrderNo, (SELECT PrimaryEmail FROM TR_Master_List WHERE FLSID = FLSTechID)", "IsNull(FLSTechID, '') != '' AND PjctStartDate <= '$yesterday' AND PjctStartDate >= '$sixWeeksAgo' AND TechComplete = '0' AND PayApprove = '0' AND Kickback = '0' AND Invoiced = '0' AND IsNull(Deactivated, '') = ''", "", false);
				foreach ($notCompleteOrder as $order) {
					$fields = getFields($order);
					$FLSID = $fields[0];
					$workOrderNo = $fields[1];
					$email = $fields[2];
					if ($email == "NULL" || $email == "") {
						$warning .= "Work order# " . $workOrderNo . " contains an unknown FLSID " . $FLSID . " or the email address is blank<br />";
						continue;
					}
					$subject = "Payment Delayed - Work Order# $workOrderNo not updated on FS website.";
					$message = "Work Order# $workOrderNo needs to be updated on Field Solutions website.  Please insure that you have closed it out with FTXS Dispatch center as well.  Payment cannot be approved until these steps are taken.";
					smtpMailLogReceived("Field Solutions", "callcoordinators@ftxs.fujitsu.com", $email, $subject, $message, $message, "AutoMailer - FLS Incomplete Orders");
//					smtpMailLogReceived("Call Coordinators", "callcoordinators@ftxs.fujitsu.com", "gbailey@fieldsolutions.com", $subject, $message, $message, "AutoMailer - FLS Incomplete Orders");
//					echo "Mailing to $email <br />";
				}
				break;
			default:
//				echo "Unknown command.";			
				reportError("AutoMailer Script", "AutoMailer Error - cmd is " . $cmd, "Unknown Command", "automailer.php?cmd=" . $cmd);
				die();
				break;
		}
	}
	catch (SoapFault $fault) {
		reportError("AutoMailer Script", "AutoMailer Error - cmd is " . $cmd, "$fault", "automailer.php?cmd=" . $cmd);
	}
	if ($warning != "")
		reportError("AutoMailer Script", "AutoMailer Warning - FLSID not found or email blank", $warning, "AutoMailer - FLS Incomplete Orders");
?>