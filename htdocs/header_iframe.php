<?php
require_once("headerStartSession.php");
?>

<script type="text/javascript" src="/widgets/js/json-serialization.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.min.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.cookie.js"></script>
<script src="/widgets/js/jquery.appear.min.js"></script>
<script src="/library/dataValidation.js?v=3"></script>
<script src="../library/jquery/interface_floatingWindows.js"></script>
<script  src="/widgets/js/jquery.cookie.js"></script>
<script  src="../library/jquery/calendar/jquery-calendar.js"></script>
<script  src="/widgets/js/jquery.formatCurrency-1.4.0.js"></script>
<script type="text/javascript" src="/widgets/js/functions.js"></script>
<script type="text/javascript" src="/widgets/js/base64.js"></script>
<script type="text/javascript" src="/library/jquery/jquery.bt.min.js"></script>
<!--[if IE]><script src="/library/jquery/excanvas.js" type="text/javascript" charset="utf-8"></script><![endif]-->
<!--  script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.easing.pack.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-mod.js"></script -->
<link rel="stylesheet" type ="text/css" href="/templates/www/floatingWindow.css" />
<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="/widgets/css/main.css?04032011" />
<!-- link rel=stylesheet type ="text/css" href="https://www.fieldsolutions.com/wp/wp-content/themes/fieldsolutions/style.css" / -->
<link rel="stylesheet" type="text/css" href="/widgets/css/popup.css" />
<link rel="stylesheet" type="text/css" href="/widgets/css/details.css?22022011" />
<link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" media="all" />
<link rel="stylesheet" href="/css/simpleDropdown/style.css" type="text/css" media="screen"/>
<link rel="stylesheet" href="<?=empty($_SERVER['HTTPS']) ? 'http' : 'https'?>://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/ui-lightness/jquery-ui.css" type="text/css" />



