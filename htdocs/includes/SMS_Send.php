<?php
//	ini_set("display_errors", 1);
	require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');
	require_once(realpath(dirname(__FILE__) . "/../") . "/library/caspioAPI.php");

	function getSMSEmailFromTechID($techIDList) {
		//$sms_domain = "ISNULL((SELECT TOP 1 SMS_Domain FROM Cell_Carrier WHERE CellProvider = Carrier), '')";
		if (!is_array($techIDList) || sizeof($techIDList) == 0) return array();
		//$emailList = caspioSelectAdv("TR_Master_List", "SMS_Number, $sms_domain", "TechID IN ('" . implode("','", $techIDList) . "') AND AllowText = '1' AND AcceptTerms = 'Yes' AND Deactivated != '1' AND $sms_domain != '' AND Deactivated !='1'", "", false, "`", "|", false);
//			$techIdArray = implode(",", $techIDList);
			$db = Core_Database::getInstance();
			$smsSelect = $db->select()->from(array("tbi" => Core_Database::TABLE_TECH_BANK_INFO), array("SMS_Number"))
							  	  ->joinLeft(array("carrier" =>"cell_carriers"), "tbi.CellProvider = carrier.id", array("sms_domain"))
								  ->where('TechID IN(?)', $techIDList)
								  ->where("AllowText = '1'")
								  ->where("AcceptTerms = 'Yes'")
                                                                  ->where("HideBids != '1'")
								  ->where("Deactivated !='1'");
			$emailList = Core_Database::fetchAll($smsSelect);
		
		if ($emailList[0] == "") return array();
			$retList = array();
			foreach ($emailList as $e) {
				//if ($parts == "") continue;
				//$info = explode("|", $parts);
				//$phone = trim($info[0], "`");
				//$domain = trim($info[1], "`");
				$phone  = $e['SMS_Number'];
				$domain = $e['sms_domain'];
				
				$phone = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
				$phone = str_replace(array("-", ".", " ", "(", ")"), array("", "", "", "", ""), $phone);
				
				if ($phone!= "" && $domain != "")
					$retList[] = $phone . $domain;
		}
		return $retList;
	}



function sms_send_SMTP() {

	// require_once("../library/smtpMail.php");
	
	$message = "
	
	Nickname: fieldsolutions  
	Destination: 5129216540
	Message: Hello, this is a test coming at you from Field Solutions! 
	Subject: test message 
	
	
	";
	
	$htmlmessage = nl2br($message);
	
	$SendTo = "gateway@celltrust.com";
	
	// COMMENTED OUT - DON'T USE SMTP TO SEND.
	// smtpMailLogReceived("Cell Trust Test", "gbailey@fieldsolutions.com", $SendTo, "Field Solutions SMS", $message, $htmlmessage, "SMS_Test");

}
// COMMENTED OUT - DON'T USE SMTP TO SEND.
//sms_send_SMTP();

function sms_send($smsTo, $message) {
//echo "<p>Message: ".$message."</p>";

	$URN = "urn:notify.soap.primemessage.com"; 
	$WSDL="http://pmgateway.net/pmws/services/TxTMessageService?wsdl"; 
	$encoding = SOAP_DEFAULT_ENCODING; 
	
	//SOAP elements (dont edit, and case sensitive!) 
	$SOAP_ACTION = "sendMessage"; 
	$CTUSERNAME = "Username"; 
	$CTPASSWORD = "Password"; 
	$CTNICKNAME = "nickname"; 
	$DESTINATION = "destination"; 
	$MESSAGE = "message"; 
	$RECIPIENT = "recipients"; 
	$ADMINEMAIL = "adminEmail";
	$SUBJECT = "subject"; 
	
	$USER_ID = "fieldsolutions"; //your username at CellTrust 
	$NICKNAME = "fieldsolutions"; //your nickname at Celltrust 
	$PASSWORD = "gbailey"; //your password at Celltrust 
	
	//create user and password SOAP header elements 
	$UserHdr = new SoapHeader( $URN, $CTUSERNAME, $USER_ID, false); 
	$PassHdr = new SoapHeader( $URN, $CTPASSWORD, $PASSWORD, false); 
	
	//create SOAP body elements, add your destination phone number, and message here 
	/*
	$woID = $_POST["woID"];
	$startDate = $_POST["startDate"];
	$endDate = $_POST["endDate"];
	$startTime = $_POST["startTime"];
	$endTime = $_POST["endTime"];
	$location = $_POST["location"];
	$offer = $_POST["offer"];
	*/
	
	$smsTo .= ",5129216540,9522707290"; // cc'ing Gerald Bailey & Jeff Sussna here for testing purposes.
	
	$sendTo = explode(",",$smsTo);
	
	$dialstring = array();
	
	$count = 0;
	
	foreach ($sendTo as $x)
	{
		//	echo "<p>".$smsTo."</p>";
		//$sendTo[$count] = "15129216540"; // SEND ONLY TO GERALD'S CELL FOR NOW.
		
		$dialstring[$count] = array("deliveryType" => 0, "destination" => "1" . $sendTo[$count], "firstName" => "", "lastName" => "");
		$count++;
	}
	
	
	/*
		$dialstring = array(array("deliveryType" => 0, "destination" => "15129216540", "firstName" => "Gerald", "lastName" => "Bailey"),
		array("deliveryType" => 0, "destination" => "14805551213", "firstName" => "Judy", "lastName" => "Doe"),
		array("deliveryType" => 0, "destination" => "14805551214", "firstName" => "Test", "lastName" => "Doe")); 
	*/
	//$dialstring = array(array("deliveryType" => 0, "destination" => "15129216540", "firstName" => "", "lastName" => "")); 
	
	$subject = "Push2Tech Workorder!"; // Note this really isn't used.
	
	//$msg = "WO# " . "15527" . chr(10) . "Start/End Date: 01/14/2009 - 01/14/2009" . chr(10) . "Start/End Time: 08:00 AM - 11:00 AM" . chr(10) . "Location: Round Rock, TX 78681" . chr(10) . "Offer: $1.00/Site";
	//$message = "test";
	
	$bodymulti = array($CTNICKNAME => $NICKNAME, $SUBJECT => $subject, $MESSAGE => $message, $RECIPIENT => $dialstring, $ADMINEMAIL = ""); 
	
	//create SOAP client 
	$client = new SoapClient($WSDL, array('trace' => 1, 'exceptions' =>0)); 
	
	//set SOAP headers 
	$client->__setSoapHeaders(array($UserHdr, $PassHdr)); 
	
	//call web service single sms 
	$result = $client->__call("sendMessage", $bodymulti, NULL); 
	
	//very usefull for debug purposes
	//show what was requested 
//	var_dump($client->__getLastRequest());
	//show gateways response
//	var_dump($client->__getLastResponse());

}

?>
