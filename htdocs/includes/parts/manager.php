<?php
require_once(realpath(dirname(__FILE__) . "/../../") . "/library/woapi.php");
class PartsManager
{
    const PARTS_MANAGER_CLIENT = 'client';
    const PARTS_MANAGER_TECH = 'tech';
    
    private $requestUrl = '';
    private $woId;
    private $user;
    private $hash;
    private $role;
    private $pmContext;
    private $postMethod = '';
    private $postParams = '';

    function __construct(){}
    
    public function setWoId($val) {
        $this->woId = (int)$val;
        return $this;
    }
    public function setUser($val) {
        $this->user = urlencode($val);
        return $this;
    }
    public function setHash($val) {
        $this->hash = urlencode($val);
        return $this;
    }
    public function setRole($val) {
        $this->role = $val;
        return $this;
    }
    public function setPmContext($val) {
        $this->pmContext = $val;
        return $this;
    }
    /**
     * Create Part
     *
     * @param string $xmlParams
     * @return mixed
     */
    public function createPart($xmlParams) {
        if (!empty($xmlParams)) {
			$pmParam = $this->role == PartsManager::PARTS_MANAGER_CLIENT ? "|pmContext=$this->pmContext" : "";
            $url = WOAPI_URL . "/$this->role/rest/parts/$this->woId?user=$this->user$pmParam&pass=$this->hash";
            $this->postMethod = 'post';
            $this->postParams = 'parts='.$xmlParams;
            $this->requestUrl = $url;
            return $this->executeRequest();
        }
        return false;
    }
    /**
     * Update part
     *
     * @param string $partId
     * @param string $xmlParams
     * @return mixed
     */
    public function updatePart($partId, $xmlParams) {
        if (!empty($partId) && !empty($xmlParams)) {
			$pmParam = $this->role == PartsManager::PARTS_MANAGER_CLIENT ? "|pmContext=$this->pmContext" : "";
            $url = WOAPI_URL . "/$this->role/rest/parts/$this->woId/$partId?user=$this->user$pmParam&pass=$this->hash";
        	$this->postMethod = 'post';
        	$this->postParams = 'parts='.$xmlParams;
        	$this->requestUrl = $url;
        	return $this->executeRequest();
        }
        return false;
    }
    /**
     * Remove part
     *
     * @param string $partId
     * @return mixed
     */
    public function removePart($partId) {
        if (!empty($partId)) {
			$pmParam = $this->role == PartsManager::PARTS_MANAGER_CLIENT ? "|pmContext=$this->pmContext" : "";
            $url = WOAPI_URL . "/$this->role/rest/parts/$this->woId/$partId?user=$this->user$pmParam&pass=$this->hash";
        	$this->postMethod = 'delete';
        	$this->requestUrl = $url;
        	return $this->executeRequest();
        }
        return false;
    }
    /**
     * Get All Parts
     *
     * @return mixed
     */
    public function getAllParts() {
        $this->postMethod = 'get';
		$pmParam = $this->role == PartsManager::PARTS_MANAGER_CLIENT ? "|pmContext=$this->pmContext" : "";
        $url = WOAPI_URL . "/$this->role/rest/parts/$this->woId?user=$this->user$pmParam&pass=$this->hash";
        $this->requestUrl = $url;
        return $this->executeRequest();
    }
    
    /**
     * Return result of CURL request
     *
     * @param string $type
     * @return mixed
     */
    private function executeRequest() {
        if (!empty($this->requestUrl)) {
        	$ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$this->requestUrl);
            curl_setopt($ch, CURLOPT_FAILONERROR, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
            curl_setopt($ch, CURLOPT_TIMEOUT, 10); // times out after 4s
            if ($this->postMethod == 'post') {
            	curl_setopt($ch, CURLOPT_POST, 1); // set POST method
                curl_setopt($ch, CURLOPT_POSTFIELDS, $this->postParams);
            } elseif ($this->postMethod == 'delete') {
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE"); // set DELETE method
            }
            $result = curl_exec($ch); // run the whole process
            curl_close($ch);
            return $result;
        }
        return false;
    }
}
