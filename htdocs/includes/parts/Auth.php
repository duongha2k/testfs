<?php

class PartsManagerAuth
{
    const AUTH_CLIENT = 'TR_Client_List';
    const AUTH_TECH = 'TR_Master_List';
    
    private $role = '';
    private $login = '';
    private $isAuht = false;
    
    function __construct() {
    }
    
    public function setLogin($val) {
        $this->login = $val;
        return $this;
    }
    public function setRole($val) {
        $this->role = $val;
        return $this;
    }
    
    public function getAuthData() {
		if (empty($this->login) || empty($this->role)) return null;
//        $result = caspioSelectAdv("$this->role", "Password", "UserName= '".$this->login."'", "", false, "`", "|");
		if (!empty($_SESSION['UserPassword']))
			return array('login'=>$this->login, 'password'=>$_SESSION['UserPassword']);
		return null;    
	}
}