<?php
/**
 * The class manipulates with the WO Categories
 * @author Sergey Petkevich (Warecorp)
 */

class WoCategories
{    private $WoCategories = null;


    public function getListAsArray()
    {        $woCategories = array();

        if ($this->WoCategories === null) {            $woCategories = caspioSelectAdv("WO_Categories", "Category_ID, Category", "", "", false, "`", "|");
        }

        foreach ($woCategories as $key=>&$val) {
            list($categoryId, $categoryName) = explode("|", $val);

            $this->WoCategories[trim($categoryId, '`')] = trim($categoryName, '`');
        }

        return $this->WoCategories;
    }

}
