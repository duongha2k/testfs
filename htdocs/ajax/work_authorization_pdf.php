
<?php

require '../../headerStartSession.php';
$Core_Api_TechClass = new Core_Api_TechClass;
$TechID = $_GET['techid'];
$Tech = $Core_Api_TechClass->getExts($TechID);
$TechName = $Tech['FirstName'] . ' ' . $Tech['LastName'];
$DateVerified = date_format(new DateTime($Tech['I9Date']), 'm/d/Y');

$Content = 'FieldSolutions is contracting directly with <u>' . $TechName . '</u> as an';
$Content .= ' independent contractor, and attests that <u>' . $TechName . '</u> has submitted';
$Content .= ' acceptable documents establishing authorization to work in the United';
$Content .= ' States. Upon review, FieldSolutions has authorized using this independent';

$Content .= ' contractor to perform services in the United States.';

// define Font-Path
define('SETAPDF_FORMFILLER_FONT_PATH', 'FormFiller/font/');
// require API
require_once('../../library/pdf/class.ezpdf.php');

$pdf = & new Cezpdf();
$pdf->selectFont('../../library/pdf/fonts/Helvetica.afm');

$image = "../../../widgets/images/us_authorized_verifile.jpg";
$pdf->ezSetMargins(0, 0, 0, 0);
$pdf->ezImage($image, 0, 700, "none", "center");
$pdf->ezSetY(640);
$pdf->ezText("  ");
//$dataHerdater = array(
//    array("", $Content, "")
//);
//
//$pdf->ezTable($dataHerdater, array(0 => '', 1 => '', 2 => '')
//        , '', array('showHeadings' => 0
//    , 'shaded' => 0
//    , 'minRowSpace' => 10
//    , 'showLines' => 0
//    , 'cols' => array(0 => array('width' => 90)
//        , 1 => array('width' => 320, 'justification' => 'center','spacing'=>2)
//        , 2 => array('width' => 90))
//    , 'width' => '500px'));
$pdf->ezSetMargins(0, 0, 120, 120);
$pdf->ezText($Content, 11, array('justification' => 'center','spacing'=> 1.25));
$pdf->ezSetMargins(0, 0, 0, 0);
$pdf->addText(160, 671, 11, 'Name: '.$TechName);
$pdf->addText(160, 646, 11, 'FieldSolutions Technician ID #: '.$TechID);
$pdf->addText(80, 745, 8, $DateVerified);

header("Cache-Control: cache, must-revalidate");
header("Pragma: public");
header("Content-Type: application/force-download");
header("Content-Disposition: attachment; filename=\"FS_Work_Authorization_" . $Tech['LastName'] . '_' . $TechID . ".pdf\"");
//header("Content-Disposition: attachment; filename=\"FSPayStub_1sds_" . $endDateFileName . ".pdf\""); // for test

echo $pdf->output();
exit;
?>
