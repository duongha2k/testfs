<?php

if ($_SERVER["HTTP_X_REQUESTED_WITH"] = "XMLHttpRequest") {
	require_once ("{$_SERVER['DOCUMENT_ROOT']}/../includes/modules/common.init.php");

	$type		= isset ($_REQUEST["login_type"]) ? $_REQUEST["login_type"] : "client";
	$login_key	= isset ($_REQUEST["login_key"]) ? $_REQUEST["login_key"] : NULL;

	$table		= $type == "client" ? "client_login" : "tech_login";
	$db			= Core_Database::getInstance();
	$ip			= $_SERVER["REMOTE_ADDR"];
	$random		= rand (1000, 9999);
	$datetime	= new DateTime ();
	$new_key	= md5($ip . "." . $random) . md5($datetime->format(DATE_ATOM));
	$result		= array ("errors" => array ());
	$success	= true;

	$messages = array (
		"INVALID_KEY"	=>	"Invalid login key.",
		"DB_ERROR"		=>	"Failed to create login key.  This feature may ".
							"be temporarily disabled for security purposes."
	);

	if ($login_key != NULL) {
		$select = $db->select ();
		$select->from ($table)
			->where ("login_key = ?", $login_key);
	
		$record = $db->fetchAll ($select);
	
		if (!empty ($record)) {
			$record = $record[0];

			if ($db->update ($table, array (
				"login_key" => $new_key
			), $db->quoteInto("id = ?", $record["id"]))) {
		
				$result["login_data"] = $record["login_data"];
			}
			else {
				$result["errors"][] = $messages["DB_ERROR"];
				$success = false;
			}
		}
		else {
			if (!$db->insert ($table, array ("login_key" => $new_key))) {
				$result["errors"][] = $messages["DB_ERROR"];
				$success = false;
			}
		}
	}
	else {
		if (!$db->insert ($table, array ("login_key" => $new_key))) {
			$result["errors"][] = $messages["DB_ERROR"];
			$success = false;
		}
	}

	$result["login_key"] = $new_key;

	header ("Content-Type", "text/javascript");
	header ("Content-Length", strlen($result));

	echo (json_encode (array ("success" => $success, "results" => $result)));
}

exit (0);
