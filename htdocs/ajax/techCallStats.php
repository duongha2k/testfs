<?php
    require_once("../headerStartSession.php");

    $client = new Core_Api_Class();
    
    if (empty($_GET["id"]) || !is_numeric($_GET["id"])) die();
    
    $result = $client->get12MonthsCallStats(
        $_SESSION['UserName'],
        $_SESSION['UserPassword'],
        $_GET["id"]
    );
    
    $resultAjax = array (
        $result['imacsFLS'] + $result['imacsMain'],
        $result['serviceFLS'] + $result['serviceMain'],
        $result['backOutFLS'] + $result['backOutMain'],
        $result['noShowFLS'] + $result['noShowMain']
    );
    
    echo implode('|', $resultAjax);