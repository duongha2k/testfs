<?php
//This page is designed to import records from D1 into Caspio using SOAP.

// Global Variables
global $getZips;
global $file;
global $GetRecord;
global $eList;

// We only want to get zipcodes once
$getZipsOnce = "yes";

try {
// Library calls
require_once("library/caspioAPI.php");
require_once("library/smtpMail.php");

// Get Zips and place in array
$getZips = caspioSelect("USA_ZipCodes", "ZIPcode, Latitude, Longitude", "", "ZIPcode", false);
	
// IMPORT D1 FILES via FTP
//FLS FTP Hostname
$ftp_server = "216.226.164.210";
$ftp_username = "mgk";
$ftp_userpassword = "dCM3fz6D";
$ftp_dir_in = "/d1/mgk/in/";
//$ftp_dir_in = "/d1/mgk/test/"; // for testing
$ftp_dir_out = "/d1/mgk/out/";

// Get files to import
$ftp_path_in = "ftp://" . $ftp_username . ":" . $ftp_userpassword . "@" . $ftp_server . $ftp_dir_in;
$ftp_path_out = "ftp://" . $ftp_username . ":" . $ftp_userpassword . "@" . $ftp_server . $ftp_dir_out;

// Get "in" directory listing from FTXS FTP Server
if ($Directory = scandir($ftp_path_in)){

$conn = ftp_connect($ftp_server) or die("Could not connect");
		ftp_login($conn,$ftp_username,$ftp_userpassword);
		
foreach($Directory as $file){

	$filename=$file;
	read_file($ftp_path_in . $file, $filename);		
	ftp_delete($conn,$ftp_dir_in . $file);  //delete the file when done importing it!
}	
	ftp_close($conn);
}


}catch (SoapFault $fault){
//FTP Failure code

smtpMail("D1 Import", "admin@fieldsolutions.com", "jsussna@fieldsolutions.com,gbailey@fieldsolutions.com,tngo@fieldsolutions.com,outages@fieldsolutions.com", "D1 FTP Error", "$fault", "$fault", "D1_Import.php");
}


//________________________________________________________
// Read text file from ftp server
function read_file($file,$filename){

	$import_file = file($file);
	$backupfile = dirname(__FILE__) . "/d1/mgk/importBackup/" . $filename . ".txt";

	$f = fopen($backupfile, 'a+');
	
	foreach($import_file as $GetRecord){
		
		// add wo to backup file
		fwrite($f, $GetRecord);
			
		$GetRecord = str_replace('"','',$GetRecord);
		$GetRecord = str_replace('\'','\'\'',$GetRecord);
		
		// Counts # of records returned
		$getCount = explode("|", $GetRecord);
		$count = count($getCount);
		
// Check for proper # of records, used for when D1 is adding a new field
if ($count == 25){
		list($WO, $SiteName, $CallType, $Problem, $SpecInstr, $PartNum, $transitNumber, $ItemNum, $Waybill, $QtyParts, $StoreInfo, $Address1, $Address2, $City, $State, $Zip, $partName, $Attn,  $partAddress0, $partAddress1, $partAddress2, $partCity, $partState, $partZip, $CallManagementArea) = explode("|", $GetRecord);

$CallManagementArea = trim($CallManagementArea); // Remove the newline char from last field.

// if new field hasn't been added yet
}else{
		list($WO, $SiteName, $CallType, $Problem, $SpecInstr, $PartNum, $transitNumber, $ItemNum, $Waybill, $QtyParts, $StoreInfo, $Address1, $Address2, $City, $State, $Zip, $partName, $Attn,  $partAddress0, $partAddress1, $partAddress2, $partCity, $partState, $partZip) = explode("|", $GetRecord);

$CallManagementArea = " "; // New Field, give it filler data for the insert
$partZip = trim($partZip); // Remove the newline char from last field.
}
		
		
$siteAddress = $Address1 . " " . $Address2;		
$siteAddress = caspioEscape($siteAddress);
	
$FLS_Comments = "Problem: " . $Problem . chr(10) . chr(13) . "Special Instructions: " . $SpecInstr . chr(10) . chr(13). "InTransit Item Number: " . $transitNumber . chr(10) . chr(13) . "Name of Store/FedEX where part will be and address info: " . $StoreInfo . chr(10) . chr(13) . "Attention: " . $Attn . chr(10) . chr(13) . "Part Info: " . chr(10) . chr(13) . "Name: " . $partName . chr(10) . chr(13) . "Address: " . $partAddress0 . $partAddress1 . $partAddress2 . chr(10) . chr(13) . "City: " . $partCity . chr(10) . chr(13) . "State: " . $partState . chr(10) . chr(13) . "Zipcode: " . $partZip;
		
$FLS_Comments = htmlentities($FLS_Comments);
$description = "Description: " . $Problem . chr(10) . chr(13) . "";
$description = htmlentities($description);
		
//$SiteName = $SiteName;
$DateImported = date("m/d/Y");
$startDate = date("m/d/Y");
$Paid = "No";
//$PayApprove = "0";
	
// Verify WO and File didn't already exist
$checkForWO = caspioSelect("FLS_Work_Orders", "WorkOrderNo, Import_File_Name", "WorkOrderNo = '$WO' AND Import_File_Name='$filename'", "WorkOrderNo", false);
	
foreach ($checkForWO as $checkForWOorder) {
	$checkForWOfields = getFields($checkForWOorder);
	
	// Get data from array
	$checkForWONumber = $checkForWOfields[0];
	$checkForFileName = $checkForWOfields[1];
				
		if($checkForWONumber == $WO && $checkForFileName == $filename){
		$WOexists="yes";
		}else{
		$WOexists="no";
		}
    }	

	//Only execute if WO and file do not exist	
	if($WOexists=="no"){
	
	//$CallType = strtoupper($CallType);
	//if($CallType == "DOWN" || $CallType == "P1" || $CallType == "P2" || $CallType == "P3" || $CallType == "P4" || $CallType == "P5" || $CallType == "SRC" ){
	
$CallType="Service Call";

// Insert Workorder into FLS Work Orders table	
$insertWorkorders = caspioInsert("FLS_Work_Orders", "WorkOrderNo, SiteName, CallType, PartNos, Waybill, QtyParts, Address1, City, State, Zip, DateImported, DateEntered, Paid, FLSComments,  Import_File_Name, PjctStartDate, CallManagementArea, Addedby", "'$WO', '$SiteName', '$CallType', '$PartNum', '$Waybill', '$QtyParts', '$siteAddress', '$City', '$State', '$Zip', '$DateImported', '$DateImported', '$Paid', '$FLS_Comments', '$filename', '$startDate', '$CallManagementArea', 'D1 Import'");
		
	
// Get Techs
Get_Techs($WO, $SiteName, $description, $City, $State, $Zip, $CallType);
	  //}	
	}
  }			
fclose($f);
}

// Get techs in 50mi radius of zipcode
function Get_Techs($WO, $SiteName, $description, $City, $State, $Zip, $CallType){

// Set Variables for query
$eList = "";
$subjectCity = $City;
$subjectState = $State;
$subjectZip = $Zip;
$tableName = "TR_Master_List";
$zipTable = "USA_ZipCodes";
$zipfieldList = "ZIPCode, Latitude, Longitude";	
$fieldList = "PrimaryEmail, ZipCode";	
$distance = "50";
$comparison = "<=";
$distance = $comparison . $distance;	

// Set Variables for email blast
$vFromName = "Fujitsu Call Coordinator";
$vFromEmail = "callcoordinators@ftxs.fujitsu.com";
@$caller = "D1Import";

// Loop through array of zips
global $getZips;

foreach ($getZips as $ziporder) {

	$zipfields = getFields($ziporder);
		
	// Get data from array
	$zipcode = $zipfields[0];
	$latitude = $zipfields[1];
	$longitude= $zipfields[2];
	
	if ($zipcode == $Zip){
		
//Get all techs in zipcode
$getTechs = caspioProximitySearchByCoordinatesRaw($tableName, false, $latitude, $longitude, "Latitude", "Longitude", $distance,"3",$fieldList, "AND (Private = '0' OR (SELECT Client_ID FROM ISO WHERE UNID = ISO_Affiliation_ID) = 'FTXS') AND FLSID != 'NULL' AND FLSID != '' AND AcceptTerms ='YES' AND Deactivated !='1' AND FLSstatus ='Trained' AND PrimaryEmail != ''", "FLSID", true, "","");
	
	
	// Output Techs
	// Loop through array of zips
	$techCount = sizeof($getTechs);
	
	foreach ($getTechs as $order) {
		$fields = getFields($order);
			
		// Get data from array
		$PrimaryEmail= $fields[0];
		$ZipCode= $fields[1];
		$eList = $PrimaryEmail . "," . $eList;
	  }
	}
  }
// Send emails to techs
Blast_Email($WO, $SiteName, $description, $subjectCity, $subjectState, $subjectZip, $CallType, $eList);
}


function Blast_Email($WO, $SiteName, $description, $subjectCity, $subjectState, $subjectZip, $CallType, $eList){

$vSubject = "$CallType in $subjectCity, $subjectState $subjectZip";
$vFromName = "Fujitsu";
$vFromEmail = "callcoordinators@ftxs.fujitsu.com";

// Send Email to the Tech
$message = "		
Please respond to this email, specifying that you want to run this call.

PLEASE DO NOT CALL AS THE PREFERRED METHOD OF DISPATCHING IS VIA EMAIL.

Be sure to include first name, last name AND 6-digit Contractor ID number when responding to this email. 

If you do not receive a reply back, it is likely that another Technician has already been chosen to run the call.

Work Order Number: $WO
$description

Thanks,
Fujitsu Call Coordinator";
		
$htmlmessage = nl2br($message);


// execute mail function
smtpMailLogReceived($vFromName, $vFromEmail, $eList, $vSubject, $message, $htmlmessage, $caller);
//smtpMailLogReceived($vFromName, $vFromEmail, "cmcgarry@fieldsolutions.com", $vSubject, $message, $htmlmessage, $caller);

}
?>