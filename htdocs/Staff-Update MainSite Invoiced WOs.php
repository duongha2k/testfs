<?php

	try {
		require_once("library/caspioAPI.php");
		require_once("library/smtpMail.php");

		$InvoiceDate = $_REQUEST['InvoiceDate'];
		
		// NOTE: GROUP BY is contained in criteria field and after at least one criteria. This is a hack of the Caspio API and may not continue to work in the future
		$records = caspioSelect("Work_Orders", "TB_UNID", "Approved = '1' AND Invoiced = '0'", "TB_UNID ASC", false);
		
		echo "Updating " . sizeof($records) . " records to 'Invoiced' with an Invoice Date of: " . $InvoiceDate;

		$fieldList = "Invoiced, DateInvoiced";
		$valuesList = "1, '$InvoiceDate'";
		
		foreach ($records as $row) {
			
			caspioUpdate("Work_Orders", $fieldList, $valuesList, "Approved = '1' AND Invoiced = '0'", false);
			
		}
		echo "<br /><br />" . sizeof($records) . " updated!";
		
	}
	catch (SoapFault $fault) {
		smtpMail("Update FLS Invoiced Script", "nobody@fieldsolutions.com", "gerald.bailey@fieldsolutions.com", "Update FLS Invoiced Script Error", "$fault", "$fault", "Staff-Update MainSite Invoiced WOs.php");
	}

?>