<?php
if (!is_object($registered)) {
    exit;
}
require_once('includes/WorkOrder.php');
require_once(TEMLATE_PATH.'/accepting.php');

if (empty($action)) {
    showAcceptingForm();
} elseif ($action == 'showacceptform') {

	$woId = 0;
	if (!empty($_POST['wo_id'])) {        $woId = $_POST['wo_id'];
        showAcceptingFormDetails($woId);
    } else {        acceptingTemplate::woUnavailable();
    }
} elseif($action == 'submit') {
    $errors = array();
    if (empty($_POST['woReviewed'])) {
        $errors[] = 'The checkbox must be checked';
    }
    if (!empty($errors[0])) {
        $error = 'Errors:<br />'.implode('<br />', $errors);
        showAcceptingFormDetails($_POST['wo_id'], $error);
    } else { // make a bid
        $woId = (int)$_POST['wo_id'];

        if (!empty($woId)) {

            if (empty($_SESSION['wap']['accepts'][$_POST['wo_id']])) {
                
                $woTech = new Core_Api_TechClass();
                $updateResult = $woTech->acceptWorkOrder(
                    $_SESSION['API']['user']['name'], 
                    $_SESSION['API']['user']['pass'],
                    $woId
                );

                $_SESSION['wap']['accepts'][$_POST['wo_id']] = true;
                acceptingTemplate::woAccepted();
            } else {
                acceptingTemplate::woAlreadyAccepted();
            }

        } else {
            acceptingTemplate::woUnavailable();
        }
    }
}



function showAcceptingForm($message = null)
{
    $session = Session::getInstance();

    acceptingTemplate::$message = $message;
    acceptingTemplate::$session = $session;

    acceptingTemplate::showAcceptingForm();
}

function showAcceptingFormDetails($woId, $message = null)
{
    $session = Session::getInstance();
    $workOrderDetails = acceptingGetWorkOrderDetails($woId);
    if (!empty($workOrderDetails) && $workOrderDetails['workOrderReviewed'] == false /*$workOrderDetails['workOrderReviewed'] == 'False'*/) {    	acceptingTemplate::$workOrderDetails = $workOrderDetails;
    	acceptingTemplate::$session = $session;
        acceptingTemplate::$message = $message;
        acceptingTemplate::$woId = $woId;

        acceptingTemplate::showAcceptingFormDetails();
    } elseif (!empty($workOrderDetails) && $workOrderDetails['workOrderReviewed'] == 'True'){
        showAcceptingForm('Sorry, this work order has already been accepted');
    } else {
    	showAcceptingForm('Sorry, there is no such work order');
    }
}



function acceptingGetWorkOrderDetails($woId)
{
    $workOrder = new WorkOrder($woId);

    $filters = new API_WorkOrderFilter;
    $filters->TB_UNID = (int)$woId;
    $filters->Tech_ID =(int)$_SESSION['wap']['user']['TechID'];
    $filters->Deactivated = false;

    return $workOrder->getDetailsByAPI($filters);
}