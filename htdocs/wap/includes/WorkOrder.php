<?php
class WorkOrder
{
    private $id = null;

    public function __construct($id)
    {
        $this->id = (int)$id;
    }

    /**
     * Old function for backward compatibility. DONT USE IT.
     * replaced by getDetailsByAPI
     * @param unknown_type $sqlFilter
     * @param unknown_type $reset
     * @return Ambigous <multitype:, string>|NULL
     */
    public function getDetails($sqlFilter = null, $reset = false)
    {
        if ($reset) {
            // SESSIONS IS NOT FOR DATA, REMOVE IT!!!
            unset($_SESSION['wap']['work_order_detail'][$this->id]);
        }

        if (empty($sqlFilter)) {
            $sqlFilter = 'TB_UNID = '.$this->id;
        }

        if (empty($_SESSION['wap']['work_order_detail'][$this->id])) {
            $results = array();

			$workOrder = caspioSelectAdv(TABLE_WORK_ORDERS, 'TB_UNID, WO_ID, SiteName, Company_ID, Username, PayMax, Amount_Per, Description, Requirements, Tools, SpecialInstructions, StartDate, StartTime, WorkOrderReviewed, isProjectAutoAssign, Headline, EndDate, EndTime, City, Zipcode, WO_Category_ID, Project_ID, (SELECT FixedBid FROM ' . TABLE_PROJECTS . ' WHERE ' . TABLE_WORK_ORDERS . '.Project_ID = ' . TABLE_PROJECTS . '.Project_ID)', $sqlFilter, "", false, "`", "|");
//            $workOrder = caspioSelectAdv(TABLE_WORK_ORDERS, 'TB_UNID, WO_ID, SiteName, Company_ID, Username, PayMax, Amount_Per, Description, Requirements, Tools, SpecialInstructions, StartDate, StartTime, WorkOrderReviewed, isProjectAutoAssign, Headline, EndDate, EndTime, City, Zipcode, WO_Category_ID, Project_ID, ', $sqlFilter, "", false, "`", "|");
            //$workOrder = caspioSelectAdv('Work_Orders', 'TB_UNID, WO_ID, SiteName, Company_ID, Username, PayMax, Amount_Per, Description, Requirements, Tools, SpecialInstructions, StartDate, StartTime', $sqlFilter, "", false, "`", "|");

            $workRecord = explode("|", $workOrder[0]);
            if (!empty($workOrder) && !empty($workOrder[0])) {
                $results['tbUid'] = trim($workRecord[0], "`");
                $results['woId']  = trim($workRecord[1], "`");
                $results['siteName']  = trim($workRecord[2], "`");
                $results['companyId']  = trim($workRecord[3], "`");
                $results['createdBy']  = trim($workRecord[4], "`");
                $results['payMax']  = trim($workRecord[5], "`");
                $results['amountPer']  = trim($workRecord[6], "`");
                $results['description'] = trim($workRecord[7], "`");
                $results['requirements'] = trim($workRecord[8], "`");
                $results['tools'] = trim($workRecord[9], "`");
                $results['specialInstructions'] = trim($workRecord[10], "`");
                $results['startDate'] = trim($workRecord[11], "`");
                $results['sartTime'] = trim($workRecord[12], "`");
                $results['workOrderReviewed'] = trim($workRecord[13], "`");
                $results['isProjectAutoAssign'] = trim($workRecord[14], "`");

                $results['Headline'] = trim($workRecord[15], "`");
                $results['EndDate'] = trim($workRecord[16], "`");
                $results['EndTime'] = trim($workRecord[17], "`");
                $results['City'] = trim($workRecord[18], "`");
                $results['Zipcode'] = trim($workRecord[19], "`");
                $results['WO_Category_ID'] = trim($workRecord[20], "`");
                $results['Project_ID'] = trim($workRecord[21], "`");

                $results['FixedBid'] = trim($workRecord[22], "`");

                $_SESSION['wap']['work_order_detail'][$woId] = $results;
                return $results;
            } else {
                return null;
            }
        } else {
            return $_SESSION['wap']['work_order_detail'][$woId];
        }
    }


    public function getDetailsByAPI($filters = null, $reset = false)
    {
        // TODO removed session cache part.
        // if it needed store it in new adapter

        $woTech = new Core_Api_TechClass();

        //if ($sqlFilter )

        if (is_null($filters) ) {
            $filters = new API_WorkOrderFilter;
        }

        if ($filters instanceof API_WorkOrderFilter ) {
            $filters->TB_UNID = (int)$this->id;
        }/* else {
            // error report in Core_Api_TechClass
        }*/

        $workOrder = $woTech->getWorkOrders(
            $_SESSION['API']['user']['name'],
            $_SESSION['API']['user']['pass'],
            null,
            $filters
        );
        $results = array();

        if ( $workOrder->success && !empty($workOrder->data) ) {
            //$result = $workOrder->data[0]->makeWorkOrderArray();
            $result = array();

            $fields = array(
                WIN_NUM => 'tbUid',
                WO_ID => 'woId',
                SiteName => 'siteName',
                Company_ID => 'companyId',
                Username => 'createdBy',
                PayMax => 'payMax',
                Amount_Per => 'amountPer',
                Description => 'description',
                Requirements => 'requirements',
                Tools => 'tools',
                SpecialInstructions => 'specialInstructions',
                StartDate => 'startDate',
                StartTime => 'sartTime',
                WorkOrderReviewed => 'workOrderReviewed',
                isProjectAutoAssign => 'isProjectAutoAssign',
                Headline => 'Headline',
                EndDate => 'EndDate',
                EndTime => 'EndTime',
                City => 'City',
                Zipcode => 'Zipcode',
                WO_Category_ID => 'WO_Category_ID',
                Project_ID => 'Project_ID',
                PcntDeduct => 'PcntDeduct'
            );

            foreach ($fields as $key => $value) {
                $result[$value] = $workOrder->data[0]->{$key};
            }

            // make other order for bid , (SELECT FixedBid FROM ' . TABLE_PROJECTS . ' WHERE ' . TABLE_WORK_ORDERS . '.Project_ID = ' . TABLE_PROJECTS . '.Project_ID)
            $prRes = $woTech->getProjects(
                $_SESSION['API']['user']['name'],
                $_SESSION['API']['user']['pass'],
                $workOrder->data[0]->Project_ID,
                null
            );

            if ( $prRes->success && !empty($prRes->data) ) {
                $result += array('FixedBid' => $prRes->data[0]->FixedBid);
            }

            return $result;
        } else {
            return null;
        }
    }


    /**
     * Get Bid lists by API
     * @param int $workOrderId
     * @param int $techId
     * @return multitype:|NULL
     */
    public static function getBid($workOrderId, $techId)
    {
        $workOrderId = (int)$workOrderId;
        $techId = (int)$techId;

        $db = Core_Database::getInstance();
        $query = $db->select();
        $query->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('*'));
        $query->where('WorkOrderID = ?', $workOrderId);
        $query->where('TechID = ?', $techId);
        $result = Core_Database::fetchAll($query);

        if ( !$result ) return null;
        foreach ( $result as $ind => &$value ) {
            $value['bidAmount'] = sprintf('%01.2f', $value['BidAmount']);
            $value['bidDate'] = new Zend_Date($value['Bid_Date'], 'yyyy-MM-dd hh:mm:ss');
            $value['bidDate'] = $value['bidDate']->toString('MM/dd/yyyy h:mm:ss a');
        }
        return $result;
    }
}