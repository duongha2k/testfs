<?php
class acceptingTemplate
{
    static public $session;
    static public $message;
    static public $workOrderDetails;
    static public $woId;

    static public function showAcceptingForm()
    {
        if (!empty(self::$message)) {
            echo '<p>'.self::$message.'</p>';
        }

        echo '<form action="index.php" method="post">
              <input type="hidden" name="sid" value="'.self::$session->getSid().'"/>
              <input type="hidden" name="controller" value="accepting"/>
              <input type="hidden" name="action" value="showacceptform"/>
              <input type="hidden" name="pagetype" value="html"/>
              <p>WIN #<br/><input name="wo_id" maxlength="255" size="15" /></p>
              <input type="submit" value="Submit" />
              </form>
              <br/><br/>
             ';
        showBackLink();
    }

    static public function showAcceptingFormDetails()
    {
        echo '<p><b>WIN #</b> '.self::$woId.'</p><br />';
        if (!empty(self::$workOrderDetails['startDate']) && self::$workOrderDetails['startDate'] != 'NULL') {
            $date = explode(' ', self::$workOrderDetails['startDate']);
            echo '<b>Start Date</b><br/>
            '.$date[0].'
            <br/><br/>';
        }
        if (!empty($workOrderDetails['startTime']) && self::$workOrderDetails['startTime'] != 'NULL') {
            echo '<b>Start Time</b><br/>
            '.self::$workOrderDetails['startTime'].'
            <br/><br/>';
        }
        if (!empty(self::$workOrderDetails['siteName'])) {
            echo '<b>Site Name</b><br />
            '.self::$workOrderDetails['siteName'].'
            <br/><br/>';
        }
        if (!empty(self::$workOrderDetails['description'])) {
            echo '<b>Work Description</b><br/>
            '.self::$workOrderDetails['description'].'
        	<br/><br/>';
        }
        if (!empty(self::$message)) {
            echo '<hr />'.self::$message.'<hr />';
        }

        // Accept
        echo '<form action="index.php" method="post">
        <input type="hidden" name="sid" value="'.self::$session->getSid().'"/>
        <input type="hidden" name="wo_id" value="'.self::$workOrderDetails['tbUid'].'"/>
        <input type="hidden" name="controller" value="accepting"/>
        <input type="hidden" name="action" value="submit"/>
        <input type="hidden" name="pagetype" value="html"/>
         <p><input type="checkbox" name="woReviewed" value="1" /> Tech has reviewed all
         work order Details, documentation and has agreed to do the work?</p>
         <input type="submit" value="Submit" />
             </form>
';

        echo '<br/><br/>';
        showBackLink();
    }

    static public function woAccepted()
    {
        echo 'Work order has been accepted<br /><br />';
        showBackLink();
    }

    static public function woAlreadyAccepted()
    {
        echo 'You have already accepted this work order<br /><br />';
        showBackLink();
    }

    static public function woUnavailable()
    {
        echo 'The work order is unavailable<br /><br />';
        showBackLink();
    }
}