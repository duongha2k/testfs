<?php
class biddingTemplate
{
    static public $workOrderDetails;
    static public $session;
    static public $message;
    static public $params;
    static public $prevBid;
    static public $categoryName;


    static public function submitBidding()
    {
        echo '<card id="biddingsuccess" title="Thank you!" newcontext="true">';
        echo 'Your bid has been recorded<br /><br />';
        showBackLink();
        echo '</card>';
    }

    static public function woNotFound()
    {
        echo '<card id="biddingsuccess" title="Not Found" newcontext="true">';
        echo 'Sorry, work order is not available<br /><br />';
        showBackLink();
        echo '</card>';
    }

    static public function woUnavailableBidding()
    {
        echo '<card id="biddingsuccess" title="Thank you!" newcontext="true">';
        echo 'The work order is unavailable';
        showBackLink();
        echo '</card>';
    }

    static public function showBiddingForm()
    {
        echo '<card id="bidding" title="Enter WIN# for bidding" newcontext="true">';

        if (!empty(self::$message)) {
            echo '<p>'.self::$message.'</p>';
        }

        echo '<fieldset title="">
             <p>WIN#<br/><input name="wo_id" maxlength="255" size="15" /></p>
             </fieldset>
             <p><anchor>Submit
            <go method="post" href="index.php">
                <postfield name="sid" value="'.self::$session->getSid().'"/>
                <postfield name="controller" value="bidding"/>
                <postfield name="action" value="showbidform"/>
                <postfield name="wo_id" value="$(wo_id)"/>
    		</go>
    	</anchor></p><br/><br/>
             ';
        showBackLink();
        echo '</card>';
    }

    static public function showBiddingFormDetails()
    {
    	echo '<card id="bidding" title="Work order" newcontext="true">';

        echo '<b>WIN#</b> '.Wml::escape(self::$workOrderDetails['tbUid']).'<br />
        <b>'.Wml::escape(self::$workOrderDetails['Headline']).'</b><br /><br />';

        $startDate = explode(' ', self::$workOrderDetails['startDate']);
        $endDate = explode(' ', self::$workOrderDetails['EndDate']);

        if (!empty($startDate[0])) {
            echo '<b>Start:</b> '.$startDate[0].' - '.self::$workOrderDetails['sartTime'].'<br/><br/>';
        }

        if (!empty($endDate[0])) {
            echo '<b>End:</b> '.$endDate[0].' - '.self::$workOrderDetails['EndTime'].'<br/><br/>';
        }

        if (!empty(self::$categoryName)) {
            echo '<b>Category:</b> '.Wml::escape(self::$categoryName).'<br/><br/>';
        }

        echo '<b>Location:</b> '.Wml::escape(self::$workOrderDetails['City']).', '.Wml::escape(self::$workOrderDetails['Zipcode']).'<br/><br/>';

        echo '<b>Work Description</b><br />
        '.Wml::escape(self::$workOrderDetails['description']).'
        <br/><br/>
        <b>Tech Requirements &amp; Tools Required</b><br/>
        '.Wml::escape(self::$workOrderDetails['requirements']). (self::$workOrderDetails['tools']? '<br />'. Wml::escape(self::$workOrderDetails['tools']) : ''). '
        <br/><br/>
        <b>Special Instructions</b><br/>
        '.Wml::escape(self::$workOrderDetails['specialInstructions']).'
        <br/><br/>';

        if (!empty(self::$message)) {
            echo '<hr />'.Wml::escape(self::$message).'<hr />';
        }

        echo '<p><b>Client Offer:</b> $$'.Wml::escape(self::$workOrderDetails['payMax']);
        if (!empty(self::$workOrderDetails['amountPer'])) {
            echo ' Per '.Wml::escape(self::$workOrderDetails['amountPer']);
        }
        echo '</p>';



        echo '<b>Your previous bids are:</b><br/><br/>';
        foreach (self::$prevBid as $bid) {
            echo '<b>Bid Amount:</b> $'.$bid['bidAmount'].' , Bid Date: '.$bid['bidDate'].'<br/>';
        }
        echo '<br/><br/>';

            echo '<fieldset title="">
            <p><input type="checkbox" name="agree" value="1" /> I agree that the named technician applying for this work order will be the individual performing the work at the work site.</p>
        <p><input type="checkbox" name="understand" value="1" /> I understand that if a different technician goes to the work site then the client has the sole discretionary right to deny access
        to the work site, and may choose not to pay the agreed upon rate for the work order.</p><br /><br />';

			echo "<p>When you place a bid, <span style=\"text-decoration: underline\">you are committing to do the work.</span>  If you are assigned the work and fail to do to the job, it may result in you not being qualified for future work with Field Solutions.<br/><br/>
You may &quot;bid&quot; a lower amount when you apply for this Work Order if you are willing to do the work for less than the Client's maximum pay amount.</p>";

		if (self::$workOrderDetails['isProjectAutoAssign'] == true) {
			echo '<b>Push2Tech&trade; bids are firm bids. Pay will only be the amount you are bidding.</b><br/>';
		}

		if (self::$workOrderDetails['FixedBid'] == true) {
			echo "FIXED TOTAL PAY WORK ORDER: this work order is<br/>
a total pre-set fixed pay work order.  No extra $<br/>
or hourly rates or bids will be accepted.  No added<br/>
comments will be considered or accepted.<br/>
<br/>
While you may add comments, no comments<br/>
regarding $ will be considered or accepted.";
		}

		if (self::$workOrderDetails['PcntDeduct']) {
			echo "<p>A 10% service fee will be deducted from your final total payment amount from this work order.</p>";
		}

		echo '<p>What is your bid amount for this work? <br/>$$<input name="bid" maxlength="20" size="5" style="height:20px; font-size:16px;" ';
			if (self::$workOrderDetails['FixedBid'] == true) {
            	echo ' value="'.strip_tags(self::$workOrderDetails['payMax']).'"' . ' readonly="readonly"';
			}
            else if(!empty(self::$params['bid'])){
            	echo ' value="'.strip_tags(self::$params['bid']).'"';
            }

            echo' />';
            if (!empty(self::$workOrderDetails['amountPer'])) {
                echo ' Per '.self::$workOrderDetails['amountPer'];
            }
            if (self::$workOrderDetails['isProjectAutoAssign'] != true) {
             echo '</p>
             <p>Comments: <input name="comments" maxlength="150" size="40"';
             if(!empty(self::$params['comments'])){
             	echo ' value="'.strip_tags(Wml::escape(self::$params['comments'])).'"';
             }
             echo ' />';
            }
            echo '</p></fieldset>
            <p><anchor>Submit
            <go method="post" href="index.php">
                <postfield name="sid" value="'.self::$session->getSid().'"/>
                <postfield name="controller" value="bidding"/>
                <postfield name="action" value="submit"/>
                <postfield name="wo_id" value="'.self::$workOrderDetails['tbUid'].'"/>
                <postfield name="bid" value="$(bid)"/>
                <postfield name="comments" value="$(comments)"/>
                <postfield name="agree" value="$(agree)"/>
                <postfield name="understand" value="$(understand)"/>
    		</go>
    	</anchor></p>
                 ';


            echo '<br/><br/>';
            showBackLink();
            echo '</card>';
    }
}