<?php
class acceptingTemplate
{
    static public $session;
    static public $message;
    static public $workOrderDetails;
    static public $woId;

    static public function showAcceptingForm()
    {
        echo '<card id="accepting" title="Enter WIN# to accept" newcontext="true">';

        if (!empty(self::$message)) {
            echo '<p>'.self::$message.'</p>';
        }

        echo '<fieldset title="">
             <p>WIN #<br/><input name="wo_id" maxlength="255" size="15" /></p>
             </fieldset>
             <p><anchor>Submit
            <go method="post" href="index.php">
                <postfield name="sid" value="'.self::$session->getSid().'"/>
                <postfield name="controller" value="accepting"/>
                <postfield name="action" value="showacceptform"/>
                <postfield name="wo_id" value="$(wo_id)"/>
    		</go>
    	</anchor></p><br/><br/>
             ';
        showBackLink();
        echo '</card>';
    }

    static public function showAcceptingFormDetails()
    {
        echo '<card id="accepting" title="Work order" newcontext="true">';

        echo '<p><b>WIN #</b> '.self::$woId.'</p><br />';
        if (!empty(self::$workOrderDetails['startDate']) && self::$workOrderDetails['startDate'] != 'NULL') {
            $date = explode(' ', self::$workOrderDetails['startDate']);
            echo '<b>Start Date</b><br/>
            '.$date[0].'
            <br/><br/>';
        }
        if (!empty($workOrderDetails['startTime']) && self::$workOrderDetails['startTime'] != 'NULL') {
            echo '<b>Start Time</b><br/>
            '.self::$workOrderDetails['startTime'].'
            <br/><br/>';
        }
        if (!empty(self::$workOrderDetails['siteName'])) {
            echo '<b>Site Nam</b>e<br />
            '.Wml::escape(self::$workOrderDetails['siteName']).'
            <br/><br/>';
        }
        if (!empty(self::$workOrderDetails['description'])) {
            echo '<b>Work Description</b><br/>
            '.Wml::escape(self::$workOrderDetails['description']).'
        	<br/><br/>';
        }
        if (!empty(self::$message)) {
            echo '<hr />'.Wml::escape(self::$message).'<hr />';
        }

        // Accept
        echo '<fieldset title="">
         <p><input type="checkbox" name="woReviewed" value="1" /> Tech has reviewed all
         work order Details, documentation and has agreed to do the work?</p>
             </fieldset>
             <p><anchor>Submit
             <go method="post" href="index.php">
                 <postfield name="sid" value="'.self::$session->getSid().'"/>
                 <postfield name="controller" value="accepting"/>
                 <postfield name="action" value="submit"/>
                 <postfield name="wo_id" value="'.self::$workOrderDetails['tbUid'].'"/>
                 <postfield name="woReviewed" value="$(woReviewed)"/>
     		</go>
     	</anchor></p>';

        echo '<br/><br/>';
        showBackLink();
            echo '</card>';
    }

    static public function woAccepted()
    {
        echo '<card id="acceptingsuccess" title="Thank you!" newcontext="true">';
        echo 'Work order has been accepted<br /><br />';
        showBackLink();
        echo '</card>';
    }

    static public function woAlreadyAccepted()
    {
        echo '<card id="acceptingsuccess" title="You have already accepted this work order" newcontext="true">';
        echo 'You have already accepted this work order<br /><br />';
        showBackLink();
        echo '</card>';
    }

    static public function woUnavailable()
    {
        echo '<card id="acceptingsuccess" title="The work order is unavailable" newcontext="true">';
        echo 'The work order is unavailable<br /><br />';
        showBackLink();
        echo '</card>';
    }
}