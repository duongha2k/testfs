<?php

require_once(realpath(dirname(__FILE__) . '/../../../') . "/includes/classes/Core/Api/TechUser.php");

class Registered
{
    private $userId;
    private $username;
    private $isLoggedIn = false;

    public function techLogin($username, $password) {
    	$usernameEscaped = mysql_escape_string($username);
    	$passwordEscaped = mysql_escape_string($password);

    	$session = Session::getInstance();

        $result = caspioSelectAdv("TR_Master_List", "TechID, FirstName, LastName, UserName, SMS_Number, Deactivated, AcceptTerms, PrimaryEmail, PrimaryPhone, PrimaryPhoneExt", "UserName = '$usernameEscaped' AND Password = '$passwordEscaped'", "", false, "`", "|", false);

        // Logged in successfully
        if (!empty($result[0])) {
        	$record = explode("|", $result[0]);
        	$this->userId = trim($record[0], "`");
        	$this->username = trim($record[3], "`");;

        	$_SESSION['wap']['loggedin']  = true;
        	$_SESSION['wap']['user']['TechID']    = $this->userId;
            $_SESSION['wap']['user']['UserName']  = $this->username;
            $_SESSION['wap']['user']['FirstName'] = trim($record[1], "`");
            $_SESSION['wap']['user']['LastName']  = trim($record[2], "`");
            $_SESSION['wap']['user']['SMSNumber']  = trim($record[4], "`");
            $_SESSION['wap']['user']['Deactivated']  = trim($record[5], "`");
            $_SESSION['wap']['user']['AcceptTerms']  = trim($record[6], "`");
            $_SESSION['wap']['user']['PrimaryEmail']  = trim($record[7], "`");
            $_SESSION['wap']['user']['PrimaryPhone']  = trim($record[8], "`");
            $_SESSION['wap']['user']['PrimaryPhoneExt']  = trim($record[9], "`");
            
            // set up account for API
            $_SESSION['API']['user']['name']  = $this->username;
            $_SESSION['API']['user']['pass']  = $password;
            
            $session->regenerateId();
            return true;
        } else {
            return false;
        }
    }

    public function isLoggedIn()
    {
        if ($_SESSION['wap']['loggedin']) {
            $this->isLoggedIn = true;
        }

        return $this->isLoggedIn;
    }
}