<?php
class Wml
{
    static public function escape($val)
    {
        $val = str_replace('&', '&amp;', $val);
        $val = str_replace('$', '$$', $val);
        return $val;
    }
}