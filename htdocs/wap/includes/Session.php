<?php
// Singleton pattern
class Session
{    private $sid = null;
    public static $session = null;

    private function __construct()
    {        ini_set('session.use_cookies', 0);
        ini_set('use_only_cookies', 0);
    }

    public static function getInstance()
    {        if (empty(Session::$session)) {            Session::$session = new self;
        }

        return Session::$session;
    }

    public function setSid($val)
    {        $this->sid = trim($val);
    }

    public function getSid()
    {        return $this->sid;
    }

    public function start()
    {        if (!empty($this->sid)) {            session_id($this->sid);
        }

        session_start();

        if (!$this->sid) {            $this->sid = session_id();
        }
    }

    public function regenerateId()
    {        session_regenerate_id();
        $this->sid = session_id();
    }
}