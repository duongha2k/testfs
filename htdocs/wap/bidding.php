<?php

if (!is_object($registered)) {
    exit;
}
require_once('includes/WorkOrder.php');
require_once(TEMLATE_PATH.'/bidding.php');

$Deactivated = $_SESSION['wap']['user']['Deactivated'];

if (empty($action)) {
    showBiddingForm();
    
} elseif ($action == 'showbidform') {
	$woId = 0;
	if (!empty($_POST['wo_id'])) {
		if ($_SESSION['wap']['user']['Deactivated'] != "True") {
			$woId = $_POST['wo_id'];
        	showBiddingFormDetails($woId);
		} else {
	        biddingTemplate::woNotFound();
		}
    } else {
        biddingTemplate::woNotFound();
    }
} elseif($action == 'submit') {
    $bid = round(str_replace(',', '.', $_POST['bid']), 2);
    $bid = sprintf("%01.2f", $bid);

    $errors = array();
    if (empty($_POST['agree']) || empty($_POST['understand'])) {
        $errors[] = 'Both checkboxes must be checked';
    }

    if ($bid <= 0) {
        $errors[] = 'The bid amount is incorrect';
    }

    if (!empty($errors[0])) {
        $error = 'Errors:<br />'.implode('<br />', $errors);
        showBiddingFormDetails($_POST['wo_id'], $error);
    } else { // make a bid


        $workOrder = new WorkOrder($_POST['wo_id']);
        $workOrderDetails = $workOrder->getDetailsByAPI();

		if (strtoupper($workOrderDetails["FixedBid"]) ==  true) {
			$bid = $workOrderDetails["payMax"];
		    $bid = round(str_replace(',', '.', $bid), 2);
		    $bid = sprintf("%01.2f", $bid);
		}

        if (is_array($workOrderDetails)) {
            $objDateNow = new Zend_Date();
            $data = array(
                'WorkOrderID' => $_POST['wo_id'],
                'WorkOrderNum' => $workOrderDetails['woId'],
                'TechID' => $_SESSION['wap']['user']['TechID'],
                'Tech_FName' => $_SESSION['wap']['user']['FirstName'],
                'Tech_LName' => $_SESSION['wap']['user']['LastName'],
                'Company_ID' => $workOrderDetails['companyId'],
                'BidAmount' => $bid,
                'Bid_Date' => $objDateNow->toString('yyyy-MM-dd hh:mm:ss'),
                'BidModifiedDate' => $objDateNow->toString('yyyy-MM-dd hh:mm:ss'),
                'Comments' => substr($_POST['comments'], 0, 150),
                'ClientEmail' => '', //E-mail address of the creator of the WO
                'DeactivatedTech' => 0,
                'CreatedByUser' => $workOrderDetails['createdBy'],
                'Hide' => 0
            );
            Core_Database::insert(Core_Database::TABLE_WORK_ORDER_BIDS, $data);
            
            $_SESSION['wap']['bids'][$_POST['wo_id']] = true;

            // check WO auto assign
            require_once("../library/timeStamps2.php");
            AutoAssign::assignFirstBidTech($_POST['wo_id'],$_SESSION['wap']['user']['TechID'],$bid, trim($_POST['comments']));
            require_once("../library/clientBidNotification.php");
            sendBidNotificationToClient($workOrderDetails['companyId'], $workOrderDetails['Project_ID'], $_POST['wo_id'], $workOrderDetails['woId'], $_SESSION['wap']['user']['FirstName'], $_SESSION['wap']['user']['LastName'], $bid, substr($_POST['comments'], 0, 150), $_SESSION['wap']['user']['TechID']);
            biddingTemplate::submitBidding();
        } else {
            biddingTemplate::woUnavailableBidding();
        }
    }
}



function showBiddingForm($message = null)
{
    $session = Session::getInstance();

    biddingTemplate::$message = $message;
    biddingTemplate::$session = $session;

    biddingTemplate::showBiddingForm($message);


}

/**
 * Show bidding form detail
 * @param unknown_type $woId
 * @param unknown_type $message
 */
function showBiddingFormDetails($woId, $message = null)
{
    $session = Session::getInstance();
    // MARK OK
    $workOrderDetails = biddingGetWorkOrderDetails($woId);
    if (!empty($workOrderDetails)) {
    	$prevBid = WorkOrder::getBid($woId, $_SESSION['wap']['user']['TechID']);
        biddingTemplate::$workOrderDetails = $workOrderDetails;
        biddingTemplate::$message = $message;
        biddingTemplate::$prevBid = $prevBid;
        biddingTemplate::$params = $_POST;
        biddingTemplate::$session = $session;

        if ($workOrderDetails['WO_Category_ID'] > 0) {

            $woClass = new Core_Api_Class();

            $categoryData = $workOrder = $woClass->getWOCategories(
                $_SESSION['API']['user']['name'],
                $_SESSION['API']['user']['pass'],
                $workOrderDetails['WO_Category_ID']
            );

            if ($categoryData->success && !empty($categoryData->data[0])) {
                biddingTemplate::$categoryName = trim($categoryData->data[0], "`");
            }
        }

        biddingTemplate::showBiddingFormDetails();

    } else {
        showBiddingForm('Sorry, work order is not available');
    }
}

/**
 * Get WO detail
 * @param int $woId
 * @return Ambigous <NULL, number, multitype:>
 */
function biddingGetWorkOrderDetails($woId)
{
    $filters = new API_WorkOrderFilter;
    $filters->TB_UNID = (int)$woId;
    $filters->ShowTechs = 1;
    $filters->Deactivated = false;
    $filters->Tech_ID_IS_NULL = true;

    $workOrder = new WorkOrder($woId);

    return $workOrder->getDetailsByAPI($filters);
}