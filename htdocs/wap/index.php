<?php
/**
 * @author Sergey Petkevich
 */
//ini_set('error_reporting', E_ALL & ~E_NOTICE);
ini_set('display_errors', 'On');

require_once('common.php');
require_once('includes/Session.php');
require_once('includes/Registered.php');

require_once realpath(dirname(__FILE__) . '/../') . '/includes/_init.service.php';


require_once('includes/Wml.php');
require_once('session.php');

$_SESSION['loggedIn'] != 'yes'; // to make caspio available
$argc = true;
require_once("../library/caspioAPI.php");
//require_once("../library/core/Core/Api/TechUser.php");
require_once("../clients/autoassign/includes/AutoAssign.php");


// Begin WAP Browswer code
$controller = '';
$action     = '';
if (!empty($_GET['controller'])) {
	$controller = $_GET['controller'];
} elseif(!empty($_POST['controller'])) {
	$controller = $_POST['controller'];
}

$pagetype = 'wml';
//$pagetype = 'html';

if (!empty($_GET['pagetype'])) {
	$pagetype = $_GET['pagetype'];
} elseif(!empty($_POST['pagetype'])) {
	$pagetype = $_POST['pagetype'];
}

if (!empty($_GET['action'])) {
	$action = $_GET['action'];
} elseif(!empty($_POST['action'])) {
	$action = $_POST['action'];
}

switch ($pagetype) {
   case 'html':
	 define('TEMLATE_PATH', 'includes/templates/html');
	 define('PAGE_TYPE', 'html');
	 break;
   default: // wml
	 header("Content-type: text/vnd.wap.wml");
	 define('TEMLATE_PATH', 'includes/templates/wml');
	 define('PAGE_TYPE', 'wml');
	 break;
 }

$registered = new Registered;
//Check if logged in
if (!$registered->isLoggedIn()) {
	$controller = 'login';
}

require_once(TEMLATE_PATH.'/header.php');

switch ($controller) {
  case 'bidding':
  	if ($_SESSION['wap']['user']["AcceptTerms"] == "No") {
		echo "<p>You must accept terms in your profile to view available work</p>";
		showBackLink();
	}
	else
		require_once('bidding.php');
	break;

  case 'accepting':
	require_once('accepting.php');
	break;

  case 'login':
	require_once('login.php');
	break;

  default:
	wapIndex();
	break;
}

require_once(TEMLATE_PATH.'/footer.php');

// Main menu
function wapIndex()
{
	$session = Session::getInstance();
	$sid = $session->getSid();
	require_once(TEMLATE_PATH.'/index.php');
}

function showBackLink()
{
	$session = Session::getInstance();
	echo '<a href="?sid=' . $session->getSid() . '&amp;pagetype='.PAGE_TYPE.'">Index</a>';
}

?>


