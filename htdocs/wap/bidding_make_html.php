<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=320; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"/>

<title>Work order bidding</title>
<script type="text/javascript" src="https://www.fieldsolutions.com/library/jquery/jquery-1.2.1.pack.js"></script>
<script type="text/javascript" src="https://www.fieldsolutions.com/library/dataValidation.js?v=1"></script>

<script type="text/javascript">

function validate() {
    //bid = document.forms.caspioform.InsertRecordBidAmount.value;
    var bid = $('#InsertRecordBidAmount').val();
    if (bid == "") {
        $('#InsertRecordBidAmount').focus();
		alert("Please enter a bid amount");
		return false;
	} else if (!isValidMoney(bid)) {
		$('#InsertRecordBidAmount').focus();
		alert("Bid must be formated like 0.00");
		return false;
	}
	return true;
}

function massageMyMoney() {
	this.value = massageMoney(this.value);
}

/*try {
	document.forms.caspioform.InsertRecordBidAmount.onblur = massageMyMoney;

	document.forms.caspioform.onsubmit = validate;
}
catch (e) {
}
*/
$(document).ready(function(){
    $('#caspioform').bind("submit", function (e) {
	                           return validate();
	                       });
    $('#InsertRecordBidAmount').bind('blur', function (e) {
    	                                       var amountVal = $('#InsertRecordBidAmount').val();
                                               amountVal = massageMoney(amountVal);
                                               $('#InsertRecordBidAmount').val(amountVal);
    	                                       });
  });


</script>

<style type="text/css">
<!--
html {
margin : 0;
padding : 0;
}
BODY {
max-width : 380px;
} -->
</style></head>

<body>
<table width="290" border="1" cellspacing="0" cellpadding="0" >
  <tr>
    <td>
    
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b0000f4i7j6j2b0h1g2c4a7b9","https:");}catch(v_e){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000f4i7j6j2b0h1g2c4a7b9">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>

    </td>
  </tr>
</table>
</body>
</html>

