<?php
if (empty($action) || $action != 'do') {
    showLogInForm();
} elseif ($action == 'do') {

    if ($registered->techLogin($_POST['username'], $_POST['password'])) {
        // If logged in show the main menu
        $session->regenerateId(); // regenerate session id
        wapIndex();
    } else {
        showLogInForm('Sorry, the username/password combination is incorrect');
    }
}

function showLogInForm($message = null)
{
    $session = Session::getInstance();
    require_once(TEMLATE_PATH.'/login.php');

}