<?php $page = login; ?>
<?php $option = clients; ?>
<?php require ("header.php"); ?>
<?php require ("navBar.php"); ?>
<!-- Add Content Here -->


<h1>Client Registration</h1>

<div id="leftcontent">

	<div id="clientSales">
	<p>
	<h2>What do you get by registering?</h2>
    </p>
	
	<br />
	
	
	<ul id="clientSalesText">
	<li>3 day access to technicians seeking work.</li>
	<li>Advanced search features to narrow tech search by zip code, skill sets, performance ratings, and several other criteria.</li>
	<li>All technician contact and ratings information so you can negotiate fees directly with the technicians</li>
	</ul>
		
	<p align="center"><a href="/clients/registerForm.php" style="font-size:16px">Register Now</a></p>
	
	</div>

</div>




<div id="rightcontent">

	<table border="0" cellpadding="10" cellspacing="0">
		<tr>
	<td>
	<div id="clientSales">
	<p>
	<h2>Premium Client Services</h2>
	</p>
	
	<br />
	
	<ul id="clientSalesText">
	<li>Post work orders for technicians seeking work</li>
	<li> Email blast technicians seeking work to notify them of your posted work order(s)</li>
	<li> Allow technicians to bid on your work orders</li>
	<li>Negotiate fees directly with technicians</li>
	<li>Assign selected technician(s) to work orders</li>
	<li>Approve work orders for pay when work completed to your satisfaction</li>
	<li>Have Field Solutions pay technicians directly within 14 days and bill you with net 30 terms</li>
	<li>Contract Field Solutions for project management, resource coordination, and/or administration of large projects</li>
	</ul>
	
	<p>	Contact our sales department to <a href="mailto:sales@fieldsolutions.com">learn more</a></p>
	
	</div>
	</td>
		</tr>
	</table>

</div>

<!---
<div id="adsenseFooter">

<script type="text/javascript">
google_ad_client = "pub-3938591336002460";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_type = "text_image";
//2007-10-11: Field Solutions Site
google_ad_channel = "7447584706";
google_color_border = "FFFFFF";
google_color_bg = "FFFFFF";
google_color_link = "174065";
google_color_text = "000000";
google_color_url = "000000";

</script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>

</div>
--->

<!--- End Content --->
<?php require ("footer.php"); ?><!-- ../ only if in sub-dir -->
		
