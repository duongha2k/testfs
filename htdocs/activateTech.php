<?php require ("header.php"); ?>
<?php require ("navBar.php"); ?>
<?php
require_once("{$_SERVER['DOCUMENT_ROOT']}/library/woapi.php");
require ("{$_SERVER['DOCUMENT_ROOT']}/headerStartSession.php");

if (isset($_GET['email']) && preg_match('/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/',$_GET['email'])) {
	$email = $_GET['email'];
}

if( isset($_GET['activate']) && strlen($_GET['activate']) == 32 )
	$code = $_GET['activate'];

$result = false;
if(isset($email) && isset($code)){
	$result = Core_Tech::confirmTechRegistration($email, $code);
}

if($result != false){
	
	$tech = new Core_Api_TechUser();
	$sendArr['login'] = $result['UserName'];
	$sendArr['password'] = $result['Password'];
	$tech = $tech->load($sendArr);
}

?>

<?php if($result == true){ ?>
	<script type="text/javascript">
	 function redirectTech(){  
	   var query = "";
	   
        query += '&' + 'UserName' + '=' + encodeURIComponent("<?=$tech->getLogin();?>").replace(/\~/g, '%7E').replace(/\!/g, '%21').replace(/\'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29');
        query += '&' + 'Password' + '=' + encodeURIComponent("<?=$tech->getPassword();?>").replace(/\~/g, '%7E').replace(/\!/g, '%21').replace(/\'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29');
		
	    l  = location.protocol + '//' + location.host;
	    l += '/techs/setTechLoginSession.php?activateRegistration=true';
     	l += query;
	   
	    location = l;
	 }
	</script>
	<p>Thank you for verifying your email address. Your account is now active. An email has been sent to you with information about completing your profile. <a href="/techs/logIn.php">Click Here</a> to login now.</p>
<?php } else {?>

	<h1>Oops !Your account could not be activated. Please recheck the link or contact the system administrator.</h1>
<?php }  ?>

<?php require ("footer.php"); ?>