<?php
    require_once 'library/caspioAPI.php';
$siteTemplate = $_SESSION['template'];
if ($siteTemplate == "ruo") {
	include ("{$_SERVER['DOCUMENT_ROOT']}/templates/ruo/includes/header.php");
} else if ($siteTemplate == "wap") {
	// Don't load WAP Header
	header("Location: ". "http://".$_SERVER['HTTP_HOST']."/wap/index.php?pagetype=html");
	exit;

} else {
	include ("headerSimple.php");
?>

<body style="min-width:1135px;">
<script type="text/javascript" src="/widgets/js/jquery.min.js"></script>
<?php
    require_once 'library/StaticMerger.php';
    $merger = new HTTP_StaticMerger('a-secret-and-constant-string');
    echo $merger->getHtml("/js_css_merge.php/", array(
        "/widgets/js/functions.js",
        "/widgets/js/jquery.form.js",
        "/widgets/js/FSPopupAbstract.js",
        "/widgets/js/FSPopupRoll.js",
        "/widgets/js/FSWidget.js",
        "/widgets/js/FSWidgetDashboard.js",
        "/widgets/js/FSTechDashboard.js",
        "/widgets/js/FSTechFindWorkOrder.js",
        "/widgets/js/FSTechPayStatus.js",
        "/widgets/js/jquery-ui.js",
        "/widgets/js/FSTechPopupsManager.js",
        "/widgets/js/FSWidgetManager.js",
        "/widgets/js/FSTechFindAvailableWork.js",
        "/library/jquery/interface_floatingWindows.js",
        "/library/jquery/calendar/jquery-calendar.js",
        "/widgets/js/FSQuickBidTool.js",
        "/widgets/js/ProgressBar.js",
        "/widgets/js/ProgressBarAdapterAbstract.js",
        "/widgets/js/ProgressBarAdapterPopupSpin.js",
        "/widgets/js/FSTechPayUtils.js",
        "/widgets/js/FSWidgetSortTool.js",
        "/widgets/js/livevalidation.js",
        "/widgets/js/?032220111" /* hack to unique url */
    
    ), false);

    $mergerCSS = new HTTP_StaticMerger('a-secret-and-constant-string');
    echo $mergerCSS->getHtml("/js_css_merge.php/", array(
        "/widgets/css/main.css",
        "/widgets/css/popup.css",
        "/library/jquery/calendar/jquery-calendar.css",
        "/widgets/css/popup.css",
        "/templates/www/floatingWindow.css",
        "/widgets/css/resizable.css"
    ), false);

?>
<script type="text/javascript" src="/library/jquery/jquery.bt.min.js"></script>
<!--[if IE]><script src="/library/jquery/excanvas.js" type="text/javascript" charset="utf-8"></script><![endif]-->
<script type="text/javascript" src="/widgets/dashboard/js/get-tech-info/id/<?=$_SESSION['TechID'];?>/" ></script>

<script type="text/javascript">
    var tpm = new FSTechPopupsManager();
    var wManager = new FSWidgetManager(); //  Widget's manager
    var td;             //  Widgets object
</script>

<div id="mainContainer">
	<div id="headerSlider">
            <div class="clr">
	<div id="headerLogo" class="fl" onClick="location=location.protocol+'//'+location.host+'/techs'" style="cursor:pointer; margin-top:10px;"></div>
        <div class="fr indentTop10 header_right">
            <table width="100%" cellpadding=0 cellspacing=0>
                <tr>
                    <td>
                    <table width="100%" cellpadding=0 cellspacing=0>
                        <tr>
                            <td colspan=2><nobr><b><u>Logged in as <span id="techN"><?=$_SESSION["UserName"]?></span> Tech ID# <?=$_SESSION['TechID']?></u></b>&nbsp;&nbsp;&nbsp;&nbsp;</nobr></td>
                        </tr>
                                        <tr class="displayNone" id="FLSID">
                        </tr>
                        <tr>
                            <td class="tSmall innerTop5 vTop"><nobr><b>For Tech Support: </b></nobr></td><td class="tSmall innerTop5 vTop"><nobr> Email: <a href="mailto:support@fieldsolutions.com">support@fieldsolutions.com</a></nobr><br /><nobr>Telephone: 952-288-2500 extension 1</nobr></td>
                        </tr>
                    </table>
                    </td>
                    <td class="tSmall innerTop5" valign="middle">
                        <div id="header_Checks"></div>
                    </td>
                </tr>
            </table>
            <script type="text/javascript">
                var roll;           //  Rollover object
                var findWorkOrder;
                var statusWidget;
                var aw;
                $(document).ready(function(){
                      //$("#techN").html(techInfo.Firstname+' '+techInfo.Lastname);
                      if (techInfo.FLSID && techInfo.FLSID != 'undefined') {
                          $("#FLSID").removeClass('displayNone').html('<td colspan=2 align="right" ><nobr><b><u>FLS ID# '+techInfo.FLSID+'</u></b>&nbsp;&nbsp;&nbsp;&nbsp;</nobr></td>');
                      }

                      var content = '<table>';


                      content+='<tr><td>';

                      if (techInfo.W9 == "1") {
                          content += '<img src="/widgets/images/check_green.gif" align="absmiddle" /></td><td> W9</td><td>&nbsp';
                      }else{
                          content += '<img src="/widgets/images/red_cross.gif" align="absmiddle" title="This item has not been completed. Click on the link to complete." /></td><td> <a name="w9" class="certForms" value="Enter W9 Online"  rel="/techs/ajax/lightboxPages/w9.php" href="javascript:///">Submit Your W9</a></td><td>&nbsp;';
                      }
                      content+='</td></tr><tr><td>';
			
                      if(techInfo.BG_Test_ResultsDate_Lite){
                    	  d = new Date();
                          d.setFullYear(d.getFullYear()-1);
                          d2 = new Date(techInfo.BG_Test_ResultsDate_Lite.replace(/-/g,"/"));
                      }

                      if (!techInfo.Bg_Test_Pass_Lite || !techInfo.BG_Test_ResultsDate_Lite) {
                         content+='<img src="/widgets/images/red_cross.gif" align="absmiddle" title="This item has not been completed. Click on the link to complete." /></td><td><a class="bgCheckLink" rel="/techs/ajax/lightboxPages/backgroundChecks.php" href="javascript:///">Complete a Background Check</a></td><td>&nbsp;';
                      }else if (d2>d){
                         content+='<img src="/widgets/images/check_green.gif" align="absmiddle" /></td><td>Background Check<br />Date: '+(d2.getMonth()+1)+'/'+d2.getDate()+'/'+d2.getFullYear()+'</td><td>&nbsp;';
                      }else {
                         content+='<img src="/widgets/images/red_cross.gif" align="absmiddle" title="This item has not been completed. Click on the link to complete." /></td><td> <a class="bgCheckLink" rel="/techs/ajax/lightboxPages/backgroundChecks.php" href="javascript:///">Complete a Background Check</a><br />Date: '+(d2.getMonth()+1)+'/'+d2.getDate()+'/'+d2.getFullYear()+'</td><td><img src="/widgets/images/red_!.png" />';
                      }
                      content+='</td></tr><tr><td>';
                      if (techInfo.DirectDeposit) {
                          content += '<img src="/widgets/images/check_green.gif" align="absmiddle" /></td><td> Direct Deposit</td><td>&nbsp';
                      }else{
                          content += '<img src="/widgets/images/red_cross.gif" align="absmiddle" title="This item has not been completed. Click on the link to complete." /></td><td> <a id="directDeposit" href="javascript:///">Sign Up for Direct Deposit</a></td><td>&nbsp;';
                      }

                      content+='</td></tr></table>';
                      $("#header_Checks").html(content);

                      $("#directDeposit").click(function(){
			document.location = "/techs/updateProfile.php?data=showPaymentInfo";
                          /* $("#paymentInfoSection").click(); */
                      });

                      roll = new FSPopupRoll();
                      findWorkOrder = new FSTechFindWorkOrder(roll);
                      findWorkOrder.buildHtml();
                      statusWidget = new FSTechPayStatus(null,roll);
                      statusWidget.buildHtml();

                      aw = new FSTechFindAvailableWork({tab:'techavailable'});
                      aw.htmlFilters();
                });
                </script>
        </div>
        </div>
</div>
<?php 
}
?>	
<script type="text/javascript" src="/widgets/js/fancybox/jquery.easing.pack.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-mod.js"></script>
<link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" media="all" />
<script type="text/javascript">
    function closebgCheckLinkbox(data)
    {
        $.fancybox.close();
    }
	$(document).ready(function() {
		$(".bgCheckLink").click(function(){
		$("<div></div>").fancybox(
                                       {
                                            'autoDimensions' : false,
                                        'width'     : 685,
                                        'height' : 470,
                                            'transitionIn': 'none',
                                            'transitionOut' : 'none',
                                            'type': 'iframe',
                                            'href': $(this).attr('rel')
                                        }
                                    ).trigger('click');
                                    });
	});
</script>
