<?php

// This script retrieves all work orders that have not been veified by techs in the 48 hour email notice
// An email is sent to the project coordinator 24 hours before project start date

try {

require_once("library/caspioAPI.php");
require_once("library/smtpMail.php");

// Set Variables
//$tomorrow = date('m/d/Y', strtotime('now'));//today for test purposes		
$tomorrow = date('m-d-Y', strtotime('+1 days'));//look ahead 24 hours
$result = "Success"; //Value required by our library, use of try should negate this
$calledFrom = (empty($_SERVER['HTTP_REFERER']) ? "Unknown" : $_SERVER['HTTP_REFERER']);// library needs this


// Get Project info
$project = caspioSelect("FLS_Projects", "ProjectNo, From_Email, ProjectName, Coordinator", "", false);


$projectCount = sizeof($project);
//echo "Project Count: $projectCount<br>";
	

// Get workorders and tech info 
$records = caspioSelect("FLS_Work_Orders", "PjctStartDate, WorkOrderID, PayAmt, PjctStartTime, TB_ID, TechEmail, SiteName, Project_ID, TechCheckedIn_48hrs, TechPhone, TechFName, TechLName, WorkOrderNo", "PjctStartDate = '$tomorrow' AND IsNull(Deactivated, '') = '' AND TechCheckedIn_48hrs='0' AND Project_ID!='0' AND CallType != 'Service Call' AND Kickback='0'", false);

// NOTE: To test remove: AND Project_ID!='0' after all projects have Project ID this won't be necessary

// Counts # of records returned
$count = sizeof($records);
//echo "Record Count: $count<br><br>";

// Populate array with workorders
foreach ($records as $order) {
	
	$fields = getFields($order);
	
	// Get data from array and set values for email
	$startDate = $fields[0];
	$startDate = date("m/d/Y", strtotime($startDate));
	$WO_ID = $fields[1];
	$PayAmount= $fields[2];
	$PayAmount = str_replace("$", "", $PayAmount);
	$StartTime = $fields[3];
	$TB_UNID = $fields[4];
	$techEmail = $fields[5];
	$siteName = $fields[6];	
	$ProjectID = $fields[7];
	$checkedin = $fields[8];
	$techPhone = $fields[9];
	$techFname = $fields[10];
	$techLname = $fields[11];
	$WorkOrderNo = $fields[12];
	
	//echo "________________________________________<br>";
	//echo "<b>Work Order</b><br>";
	//echo "TB UNID: $TB_UNID<br>";
	//echo "Tech Checked In: $checkedin<br>";
	//echo "Tech Email: $techEmail<br>";
	//echo "Project ID: $ProjectID<br>";
	//echo "Start Date: $startDate<br>";
	//echo "WOID: $WO_ID<br><br>";
	
// Default mail values
$vFromName = "Field Solutions";
$vFromEmail = "Projects@fieldsolutions.com";
	
	
	// Populate array with project info
foreach ($project as $order) {
	
	$projectFields = getFields($order);
	
	if ($projectFields[0] == $ProjectID){
	
	$projectID = $projectFields[0]; 	// Project ID
	$sendToEmail = $projectFields[1];	// To Email address
	$projectName = $projectFields[2];   // Project Name
	$cc = $projectFields[3]; // Call Coordinator
	
	// use if project send to email is blank
	if ($sendToEmail == ""){
		$sendToEmail = "callcoordinators@ftxs.fujitsu.com";
	}
	
	//echo "<b>Project Info</b><br>";
	//echo "Project Name: $projectName<br>";
	//echo "Project ID: $projectID<br>";
	//echo "Send to email: $sendToEmail<br>";
	//echo "CC: $cc<br>";
	
	$vSubject = "Tech has not confirmed: $projectName - $WorkOrderNo"; 
	
	$eList = $sendToEmail;
	//$eList = "collin.mcgarry@fieldsolutions.com";
	//$eList = "collin.mcgarry@fieldsolutions.com,gerald.bailey@fieldsolutions.com";
	@$caller = "FLSTechNotConfirmed";
		
	
	$vMessage = "Dear Resource Coordinator,/r/r This email is to inform you that the Technician assigned to your project has not confirmed./r/r <b>Project Information:</b>/r/r Project Name: $projectName/r Project ID: $projectID/r Work order number: $WO_ID/r Date of work: $startDate/r Start Time: $StartTime/r Site Name: $siteName/r/r <b>Tech Contact Information:</b>/r/r Name: $techFname $techLname/r Email: $techEmail/r Phone: $techPhone/r /r/r/r Thank you,/r Field Solutions Management";	
	
$html_message = "<p>Dear Resource Coordinator,</p><p>This email is to inform you that the Technician assigned to your project has not confirmed.</p><p>Project Information:</p><p>Project Name: $projectName<br>Project ID: $projectID<br>Work order number: $WO_ID<br>Date of work: $startDate<br>Start Time: $StartTime<br>Site Name: $siteName</p><p>Tech Contact Information:</p><p>Name: $techFname $techLname<br>Email: $techEmail<br>Phone: $techPhone</p><p>Regards,</p><p>Field Solutions<br> Web: <a href='http://www.fieldsolutions.com'>www.fieldsolutions.com</a></p>";
	
// Emails Client
smtpMailLogReceived($vFromName, $vFromEmail, $eList, $vSubject, $vMessage, $html_message, $caller);
//smtpMailLogReceived($vFromName, $vFromEmail, "gbailey@fieldsolutions.com", $vSubject, $vMessage, $html_message, $caller);
   }
  }
 }
}
	catch (SoapFault $fault) {
		
smtpMail("FLS Tech Not Confirmed Script", "nobody@fieldsolutions.com", "collin.mcgarry@fieldsolutions.com", "FLS Tech Not Confirmed Script Error", "$fault", "$fault", "FLSTechNotConfirmed.php");


}
	
?>