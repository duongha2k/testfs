<?php
	require_once("library/caspioAPI.php");
	ini_set("display_errors",1);
	set_time_limit(600);

//	$woList = array(44733, 46007, 49692, 51584, 47099, 50028, 46008, 48090);
/*	print_r(caspioUpdate("Work_Orders", "SATRecommended, SATPerformance", "3, 3", "TB_UNID IN (44733, 46007, 49692)", false));
	print_r(caspioUpdate("Work_Orders", "SATRecommended, SATPerformance", "4, 3", "TB_UNID IN (51584, 47099, 50028)", false));
	print_r(caspioUpdate("Work_Orders", "SATRecommended", "4", "TB_UNID IN (46008, 48090)", false));*/
	
	function accumulateTechSAT($column) {
		$techList = caspioSelectAdv("Work_Orders","Tech_ID, AVG($column), COUNT(Tech_ID)","ISNULL(Tech_ID,'') != '' AND $column IS NOT NULL GROUP BY Tech_ID", "",false, "`", "|", false);
		$accumlation = array();

		foreach ($techList as $info) {
			if ($info == "") continue;
			$parts = explode("|", $info);
			$techID = trim($parts[0], "`");
			$avg = trim($parts[1], "`");
			$count = trim($parts[2], "`");
			$accumlation[$techID] = array("Avg" => $avg, "Total" => $count);
		}
		return $accumlation;
	}
	
	$SATUpdate = array();
	
	$scoreList = array("SATRecommended", "SATPerformance");
	
	$score = array();
	$techList = array();
	
	$columnList = array();
		
	foreach ($scoreList as $col) {
		$score[$col] = accumulateTechSAT($col);
		$techList = array_merge(array_keys($techList), array_keys($score[$col]));
		$columnList[$col . "Avg"] = "NULL";
		$columnList[$col . "Total"] = "NULL";
	}
		
	echo "RESET: UPDATE " . implode(",", array_keys($columnList)) . " (" . implode(",", array_values($columnList)) . ")<br/><br/>";
	print_r(caspioUpdate("TR_Master_List", implode(",", array_keys($columnList)), implode(",", array_values($columnList)), "", false));

			
	foreach ($techList as $techID) {
		if ($techID == 0) continue;
		foreach ($score as $scoreColumn=>$values) {
			if (array_key_exists($techID, $values)) {
				if (!array_key_exists($techID, $SATUpdate))
					$SATUpdate[$techID] = array("Fields" => array(), "Values" => array());
				$SATUpdate[$techID]["Fields"][] = $scoreColumn . "Avg";
				$SATUpdate[$techID]["Fields"][] = $scoreColumn . "Total";
				$SATUpdate[$techID]["Values"][] = $values[$techID]["Avg"];
				$SATUpdate[$techID]["Values"][] = $values[$techID]["Total"];
			}
		}
	}
				
	foreach ($SATUpdate as $tech=>$update) {
		echo "UPDATE " . implode(",", $update["Fields"]) . " (" . implode(",", $update["Values"]) . ") WHERE $tech<br/>";
		print_r(caspioUpdate("TR_Master_List", implode(",", $update["Fields"]), implode(",", $update["Values"]), "TechID = '$tech'", false));
		ob_flush();
		flush();
	}
