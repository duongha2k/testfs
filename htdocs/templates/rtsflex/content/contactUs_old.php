<?php $page = login; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>

<!-- Add Content Here -->
<h1>Support</h1>


<p>
In order for us to assist you in a timely manner, it is important that you choose the appropriate contact below when emailing support.
</p>

<div id="hLine">&nbsp;</div>

<p><img src="images/globe.jpg" width=30 height=28 border=0  alt=""> 
<b>FLS/Service Call program Support:</b>

<p> If you are a Technician, any questions/problems that you have regarding the FLS Service Call program, should be directed <a href="http://www.flsupport.com/21.html"><font face=Arial color=#0062c4 size=2>here</font></a>.
</p>

<hr>

<p><img src="images/globe.jpg" width=30 height=28 border=0  alt=""> 
<b>Questions regarding how The Technician Bureau works?</b>
</p>

<p> If you are a Technician, get some or all of your questions related to how The Technician Bureau works, can be answered by reviewing <a href="/ttb-model.html"><font face=Arial color=#0062c4 size=2>The Technician Bureau Model</font></a>.
</p>

<hr>

<p>
<img src="images/globe.jpg" width=30 height=28 border=0  alt=""> 
<b>Login problems?</b>
</p>

<p> If you are a Technician and have forgotten your TB login, please <a href="/51.html"><font face=Arial color=#0062c4 size=2>go here </font></a> to recover it. If you are sure that you have the correct login but it is not working, send an email to the address listed below.
</p>

<p> 
<img src="images/email.jpg" width=29 height=21 border=0  alt=""> 
<a href="&#109;&#097;&#105;&#108;&#116;&#111;:support&#064;fieldsolutions.com?Subject=TB Support (s): TB Login Problems"><img src="images/supp_at_tb.jpg" width=189 height=18 border=0  alt=""></a>
</p>

<hr>

<p>
<img src="images/globe.jpg" width=30 height=28 border=0  alt=""> <b>Client Support: </b>
</p>

<p> If you are a client or prospective client of The Technician Bureau and need assistance, please send an email to the address listed below. By "client or prospective client", we are referring to companies seeking Technicians for field service work involving deployment (installs) and/or service.
</p>

<p> 
<img src="images/email.jpg" width=29 height=21 border=0  alt=""> 
<a href="&#109;&#097;&#105;&#108;&#116;&#111;:support&#064;fieldsolutions.com?Subject=TB Support (s): Client Inquiry"><img src="images/supp_at_tb.jpg" width=189 height=18 border=0  alt=""></a>
</p>

<hr>

<p>
<img src="images/globe.jpg" width=30 height=28 border=0  alt=""> 
<b>General Questions/Assistance:</b>
</p>

<p> If you are a Technician and have a general question/problem that is not listed in a category above, please send an email to the address listed below.
</p>

<p> 
<img src="images/email.jpg" width=29 height=21 border=0  alt=""> 
<a href="mailto:support@fieldsolutions.com?Subject=TB Support (s): General Tech Question"><img src="images/supp_at_tb.jpg" width=189 height=18 border=0  alt=""></a>
</p>

<hr>

<p>
<img src="images/globe.jpg" width=30 height=28 border=0  alt=""> 
<b>W-9 questions/problems?</b>
</p>

<p> If you are a Technician and have a question regarding how/where to download the W-9 form and/or how/where to fax it, please first refer to <a href="/46.html"><font face=Arial color=#0062c4 size=2>Submitting Your W-9 Form</font></a> prior to contacting support. If you still have a question/problem as it relates to your W-9 form, send it to the email address listed below.
</p>

<p> 
<img src="images/email.jpg" width=29 height=21 border=0  alt=""> <a href="&#109;&#097;&#105;&#108;&#116;&#111;:sarah.kimberlin&#064;fieldsolutions.com?Subject=TB Suport (s): W-9 Question/Issue"><img src="images/sk.jpg" width=233 height=17 border=0  alt=""></a>
</p>

<hr>

<p>
<img src="images/globe.jpg" width=30 height=28 border=0  alt=""> 
<b>Payment questions/issues?</b>
</p>

<p> If you are a Technician and have a question/problem regarding payment, please send it to the email address listed below.
</p>

<p> <img src="images/email.jpg" width=29 height=21 border=0  alt=""> 
<a href="mailto:accounting@fieldsolutions.com?Subject=TB Suport (s): Payment Question/Issue"><img src="images/acc.jpg" width=209 height=19 border=0  alt=""></a>
</p>

<hr>

<br>
<br>
<br>


<!--- End Content --->
<?php require ("../footer.php"); ?>
		
