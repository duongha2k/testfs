<?php $page = login; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>

<div id="container">


<div id="header">

    <div id="wrapper">
        <div id="content" align="center"><h1>About Us</h1></div>
    
<!-- Add Content Here -->


<p>
The concept behind our service is not new. Before a financial institution grants a loan to a customer they do a credit check with the credit bureau. Before you contract with an independent technician you should check their audio resume and ratings with us. Our focus is matching technicians seeking work with clients seeking technicians. We are gathering new information on our registered technicians daily. You need fresh information on technicians and that's what you get from our service.
</p>

<h2>I'm an independent technician. Why should I register with you?</h2>

<p>
By registering with The Technician Bureau, you present yourself to companies and consumers seeking technicians for field service work. Additionally, if our clients contact you via an alternative "resume" service or website, it's likely they will search for you on our service to ensure they are contracting the right technician for their needs. If you're not listed with The Technician Bureau, you risk losing the opportunity to your competitors. You should want your good performance captured to give you a competitive edge in a growing industry for field technicians. 
</p>

<p>
Registration is quick and simple. And, we take no cut of your negotiated fees with our clients. You keep 100% of the agreed fees for work performed.   
</p>
<h2>
We're a service organization that uses independent technicians. What differentiates you from all the others?
</h2>
<p>
We don't rely on what the technicians tell us. We don't rely on clients to update performance information because they don't do it. And if they do, it's usually when there is poor performance. Good performance goes undocumented and everyone loses. We have a dedicated staff that updates technician information the old fashioned way ... by talking to the technicians, the clients contracting them, and the end customers working with them on-site.
</p>
<p>
We   encourage all technicians to submit an audio/video resume and picture to their profiles. In the growing world of contracting technicians virtually, technology is beginning to allow us to emulate the interview process over the worldwide web.
</p>
<p>
You contact the technicians directly via our service. You negotiate the terms of your specific requirements, deliverables, and pay amount. When the work is completed, you submit a pay request through our service and we cut a check to the technician within 14 business days.
</p>

 <br /><br />

<p>
If you have questions, email us at <a href="mailto:support@fieldsolutions.com">support@fieldsolutions.com</a>.
</p>

</div></div>

<!--- End Content --->
<?php require ("../footer.php"); ?>
		
