<ul id="maintab">
<?php if ( $page == "login" ) {?>
<li class="selected"><a href="/index2.php">Login</a></li>
<?php } ?>

<?php if ( $page == "techs" ) {?>
<li class="selected"><a href="<?php echo $siteUrl;?>/techs/dashboard.php">Tech Dashboard</a></li>
<?php } ?>

<?php if ( $page == "clients" ) {?>
<li class="selected"><a href="<?php echo $siteUrl;?>/clients/dashboard.php">Client Dashboard</a></li>
<?php } ?>

<?php if ( $page == "staff" ) {?>
<li class="selected"><a href="<?php echo $siteUrl;?>/staff/dashboard.php">Staff Dashboard</a></li>
<?php } ?>

<?php if ( $page == "admin" ) {?>
<li class="selected"><a href="<?php echo $siteUrl;?>/admin/dashboard.php">TB Admin Dashboard</a></li>
<?php } ?>

<?php if ( $page == "sales" ) {?>
<li class="selected"><a href="<?php echo $siteUrl;?>/sales/dashboard.php">Sales Dashboard</a></li>
<?php } ?>


<li><a href="http://MyTechnicianSpace.com" target="_blank">Tech Community</a></li>
</ul>

<?php if ( $page != "login" ) {?>
<div style="text-align:right; margin-top: -20px;" class="clearfix">
<ul id="logoutTab">
	<li><a href="https://bridge.caspio.net/folderlogout/">Logout</a></li>
</ul>
</div>
<?php } ?>

<div id="tabcontent" class="clearfix">

<!--Login -->
<?php if ( $page == "login" ){echo '<ul class="selected">';}else{echo '<ul>';}?>
<li><a href="<?php echo $siteUrl;?>/techs/index.php">Tech</a></li>
<li><a href="<?php echo $siteUrl;?>/clients/index.php">Client</a></li>
<li><a href="<?php echo $siteUrl;?>/staff/index.php">Staff</a></li>
<li><a href="<?php echo $siteUrl;?>/admin/index.php">Admin</a></li>
</ul>

<!--Techs -->
<?php if ( $page == "techs" ){echo '<ul class="selected">';}else{echo '<ul>';}?>

<?php if ( $option == "profile" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/techs/profile.php">My Profile</a></li>

<?php if ( $option == "skills" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/techs/skills.php">Skills</a></li>

<?php if ( $option == "experience" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/techs/experience.php">Experience</a></li>

<?php if ( $option == "certifications" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/techs/certifications.php">Certifications</a></li>

<?php if ( $option == "media" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/techs/media.php">Video / Audio</a></li>

<?php if ( $option == "equipment" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/techs/equipment.php">Equipment</a></li>

<?php if ( $option == "more" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/techs/more.php">More</a></li>
</ul>

<!--Clients -->
<?php if ( $page == "clients" ){echo '<ul class="selected">';}else{echo '<ul>';}?>
<?php if ( $option == "profile" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/clients/profile.php">My Profile</a></li>
<?php if ( $option == "techs" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/clients/techsredirect.php">Find Techs</a></li>

<?php if ( $option == "wos" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/clients/wos.php">WO's</a></li>


<?php if ( $option == "reports" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/clients/reports.php">Reports</a></li>

<?php if ( $option == "users" ){echo '<li class="selected">';}else{echo '<li>';}?>
<span id="users"></span></li>

<?php if ( $option == "projects" ){echo '<li class="selected">';}else{echo '<li>';}?>
<span id="projects"></span></li>

<?php if ( $option == "customers" ){echo '<li class="selected">';}else{echo '<li>';}?>
<span id="customers"></span></li>


</ul>

<!--Staff -->
<?php if ( $page == "staff" ){echo '<ul class="selected">';}else{echo '<ul>';}?>

<?php if ( $option == "profile" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/staff/profile.php">My Profile</a></li>
<?php if ( $option == "staff" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/staff/staff.php">Staff</a></li>
<?php if ( $option == "techs" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/staff/techs.php">Techs</a></li>
<?php if ( $option == "wosFLS" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/staff/wosFLS.php">FLS WO's</a></li>
<?php if ( $option == "dbTesting" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/staff/dbTesting.php">D&B Testing</a></li>
</ul>

<!--Admin -->
<?php if ( $page == "admin" ){echo '<ul class="selected">';}else{echo '<ul>';}?>

<?php if ( $option == "techs" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/admin/adminTechs.php">Techs</a></li>
<?php if ( $option == "clients" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/admin/adminClients.php">Client</a></li>
<?php if ( $option == "wos" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/admin/adminWos.php">WO's</a></li>
<?php if ( $option == "staff" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/admin/adminStaff.php">Staff</a></li>
<?php if ( $option == "requests" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/admin/adminRequests.php">Requests</a></li>
<?php if ( $option == "reports" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/admin/adminReports.php">Reports</a></li>
</ul>


<!--Sales -->
<?php if ( $page == "sales" ){echo '<ul class="selected">';}else{echo '<ul>';}?>

<?php if ( $option == "techs" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/sales/salesTechs.php">Techs</a></li>
<?php if ( $option == "clients" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="<?php echo $siteUrl;?>/sales/salesClients.php">Client</a></li>
<?php if ( $option == "wos" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/sales/salesWos.php">WO's</a></li>
</ul>

</div>

