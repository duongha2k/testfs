<?
$cfgSite = Zend_Registry::get('CFG_SITE');

$googleKey = $cfgSite->get("site")->get("google_map_key");
$googleClientId = $cfgSite->get("site")->get("google_map_client_id");
$v= "PITBC";
?>
<script src="<?= empty($_SERVER['HTTPS']) ? 'http' : 'https' ?>://maps.googleapis.com/maps/api/js?client=<?= $googleClientId ?>&sensor=false&v=3.7" type="text/javascript"></script>

<style>
    #topNavBar li a{
        color: #4A8CBD;
        text-decoration: underline;
    }
    #topNavBar li a:hover{
        color: #4A8CBD;
        text-decoration: none;
        background: #FFFFFF;
    }
    ul{
        list-style-type:disc;
    }
    ul li{
        padding-bottom: 7px;

    }

    .backgorundBottom{
        padding:0 38px;
        height:126px;
        width:100%;
        background-repeat: no-repeat;
        background-image: url("/widgets/images/devry_right.jpg");
        background-size: 100% 126px;
        -webkit-background-size:100% 126px;
        -moz-background-size:100% 126px;
        -o-background-size:100% 126px;
        -ms-background-size:100% 126px;
        -ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/widgets/images/devry_right.jpg',sizingMethod='scale')";
    }
    .login_Top_center{
        background-image: url("/widgets/images/devry_right_B.png");
        background-position: center top;
        background-repeat: repeat-x;
        height: 126px;
        margin-top: 12px;
        margin-left: 6px;
    }
    .login_Top_right {
        background-image: url("/widgets/images/devry_right_R.png");
        background-position: right top;
        background-repeat: no-repeat;
        height: 126px;
    }

    .login_Top_left {
        background-image: url("/widgets/images/devry_right_L.png");
        background-position: left top;
        background-repeat: no-repeat;
        height: 126px;
    }
    .login_Top_left_Trans {
        background-image: url("/widgets/images/devry_right_L_T.png");
        background-position: left top;
        background-repeat: no-repeat;
        height: 126px;
    }
    .listWos
    {
        height:96px;
        overflow:auto;
        overflow-y: hidden;
        padding-left:25px;
        padding-top:25px
    }
    .listWos ul{list-style: none; margin: 0;padding: 0}
    .listWos ul li
    {
        border-bottom: 1px solid #ccc;
        text-align: left;
        padding-left: 25px;
        background-image: url("/widgets/images/feedicon.png");
        background-repeat: no-repeat;
    }
</style>

<script type="text/javascript" src="parts/js/login.js"></script>

<table width="1000px" cellspacing="0" cellpadding="0">
    <tr>
        <td style="padding: 10px;" valign="top">
            <div style='height:450px;width:315px;background-repeat: no-repeat;background-image: url("/widgets/images/left_login_devry.jpg");background-size: 315px 450px;'>
                <div style="padding: 20px;">
                    <h2 style="font-size: 16px;color: #F69630;">Why Register with FieldSolutions?</h2>
                    <div style="padding-top: 7px;">Over 100,000 technician service calls are  available through FieldSolutions every year:</div>
                    <ul  style="list-style: inherit; margin-left: 14px;padding-top: 7px;">
                        <li>
                            PC
                        </li>
                        <li> Printer & Copier </li>
                        <li> Point of Sale (POS) </li>
                        <li> Digital Signage & Flat Panel  </li>
                        <li> Cabling  </li>
                        <li> Telephony  </li>
                        <li> Data Center  </li>
                        <li> ATM  </li>
                    </ul>
                    <div style="padding-top: 7px;"><span  style="font-weight: bold;">Make More Money </span>: quick payment and  steady flow of work</div>
                    <div style="padding-top: 7px;"><span  style="font-weight: bold;">Get Full Time Technician Jobs</span>: free training, discounted industry certifications, and exposure working for the world's largest technology companies. FieldSolutions lets our clients hire service professionals directly from our registered techs with no fees to the client</div>
                </div>
            </div> 
            <div style="vertical-align: bottom; height: 65px; display:table-cell; list-style-type:none;">
                <a target="_blank" href="http://www.fieldsolutions.com">
                    <img  style="border:0px;width: 314px;" src="/widgets/images/learnmore_fs_devry.jpg" style="cursor: pointer;" />
                </a>
            </div>
        </td>
        <td valign="top" width="925px">
            <div>
                <h2 style="font-size: 13px;color: #F69630;padding-top:20px;">About FieldSolutions</h2>
                <div style="float: right;margin-top:-35px">
                    <a href="/techSignup.php?welcome=1&v=<?=$v?>"><img style="border: 0px;cursor: pointer;" src="/widgets/images/devry_register_now.jpg" />    </a>
                </div>
                <div>
                    FieldSolutions (<a target="_blank"  href="http://www.fieldsolutions.com" >www.FieldSolutions.com</a>) is a leader in technology field service and is committed to supporting  independent contractor technicians. Our on-line service management system provides work to over 6,000 independent technicians annually. FieldSolutions offers the FS-PlusOne performance rewards program, and continuous training and skill development opportunities.
                </div>
                <br>
                <h2 style="font-size: 13px;color: #F69630;">Technicians Needed for Nationwide Field Service Jobs</h2>
                <div style="padding-right:10px;">
                    FieldSolutions  is growing rapidly and our Global Leading Clients need high quality, technology service professionals throughout North America with installation and repair skills in all technology areas. Find technician jobs, including: individual service calls, projects, and long term contract positions. 
                    </br><a id="cmdReadWork" style="text-decoration: underline;" href="javascript:;">Work Available through FieldSolutions</a>
                </div>
                <br>
                <h2 style="font-size: 13px;color: #F69630;">Steady Work Flow, Fast Payment - At No Cost To You</h2>
                <div>
                    Along with access to a large volume of work, you'll be able to make more money in one place. </br> Register for free and explore FieldSolutions opportunities: 
                </div>
                <ul style="list-style: inherit; margin-left: 60px;padding-top: 7px;">
                    <li>Receive automatic notification for work available in your area</li>
                    <li>Get the free FieldSolutions smartphone apps that make you more efficient</li>
                    <li>Sign up for the FS-PlusOne Rewards program to earn cash bonuses</li>
                    <li>Complete your skills resume so employers know who you are and come to you with work</li>
					<li>Note: The 'Getting Started Tutorial' video linked below mentions Background Checks that are <u>not</u> applicable to techs living in Canada.</li>
                </ul>
                <div class="login_Top_center" style="margin-top:0px;">
                    <div class="login_Top_right">
                        <div class="login_Top_left">
                            <br>
                            <div style="float: left;width:50%;padding-left: 43px;">
                                <h2 style="font-size: 13px;color: #F69630;">Get Started!</h2>
                                <div style="padding-top: 7px;">1) Register and create your Technician Profile</div>
                                <div>2) View Available Tech Jobs in your area</div>
                                <div>3) Apply for work you are qualified and interested in</div>
                            </div>
                            <div style="float: left;width:40%;padding-top: 10px;">
                                <div><a target="_blank" style="text-decoration: underline;" href="http://www.screencast.com/t/EabxqPsvm">Watch the Getting Started Tutorial</a></div>
                                <div style="padding-top: 7px;"><a id="cmdReadQuick" style="text-decoration: underline;" href="javascript:;">Read Quick Steps for Getting Work  </a></div>
                                <div style="padding-top: 7px;"><a id="cmdReadTips" style="text-decoration: underline;" href="javascript:;">Read Tips for Techs</a></div>
                                <div style="padding-top: 7px;"><a style="text-decoration: underline;" href="/techSignup.php?welcome=1&v=<?=$v?>">REGISTER NOW</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
</table>
<?php include ("readquicktemplate.php"); ?>
<?php include ("readtipstemplate.php"); ?>
<?php include ("readworktemplate.php"); ?>
<?php require ("../footer.php"); ?>
<script>
    var _login = login.CreateObject({id:"_login"});
</script>