<div id="ReadWorkTemplate" style="display: none;">
    <div style="font-size: 20px;">ACCESS MORE WORK, MAKE MORE MONEY</div>
    <hr style="background: #1F497D;">
   
	<p>
		FieldSolutions' on-line work order management system and fast payment program provides work to over 6,000 technicians annually. Technicians are needed throughout North America with skills in Computer Install and De-Install, Cabling, Networking, Swap Outs, Diagnose and Repair, and Troubleshooting.
	</p>
	
	<div style="font-size: 20px;">WORK AVAILABLE THROUGHT FIELDSOLUTIONS</div>
	 <hr style="background: #1F497D;">
	<div class="work-form">
		<h2>Displays</h2>
		<ul>
			<li>Digital Signare</li>
			<li>Touch Screen</li>
			<li>Flat Panel and Theater</li>
			<li>Monitors</li>
		</ul>
	</div>
	<div class="work-form">
		<h2>Central Processing Units</h2>
		<ul>
			<li>PCs: Desktop,Laptop,Rugged</li>
			<li>Servers, Data Centers</li>
			<li>Point of Sale(POS)</li>
			<li>Kiosks, ATMs</li>
		</ul>
	</div>
	<div class="work-form">
		<h2>Peripherals</h2>
		<ul>
			<li>Printers, Copiers</li>
			<li>Payment Transaction Devices</li>
			<li>Scaners</li>
			<li>Electromechanical</li>
		</ul>
	</div>
	<div class="work-form">
		<h2>Connectivity</h2>
		<ul>
			<li>Cabling: Internet, Phone, Coax, CCTV</li>
			<li>Internet Hubs, Routers, Ports</li>
			<li>Wifi</li>
			<li>Telephony</li>
		</ul>
	</div>
</div>
<style>
	.work-form{margin:0; padding: 0 ; float: left; width: 350px;}
	.work-form h2{text-align: center; font-weight: bold; font-size: 12px;padding: 3px 0}
	.work-form ul{list-style-type: circle;list-style-position:inside;}
	.work-form ul li{}
</style>