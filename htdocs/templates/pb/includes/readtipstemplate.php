<div id="ReadTipsTemplate" style="display: none;">
    <div style="font-size: 20px;">TIPS FOR TECHS</div>
    <hr style="background: #1F497D;">
    <br>
    <div style="float: left;width: 50%;">
        <div style="font-weight: bold;">Complete your Profile</div>
        <br>
        <ul style="list-style: inherit; margin-left: 40px;">
            <li>Clients almost always review a technician's </br>profile to assess if the tech is qualified for a </br> given work order</li>
            <li>Most important profile categories to </br>complete are your skill ratings, tools and </br>equipment, and resume </li>
        </ul>
        <br>
        <div style="font-weight: bold;">Complete a Background Check </div>
        <br>
        <ul style="list-style: inherit; margin-left: 40px;">
            <li>Available through FieldSolutions for $19</li>
            <li>Background checks are required for 50% </br>of FieldSolutions work and many clients </br>prefer to assign a technician who has a </br>background check on file  </li>
        </ul>
        <br>
    </div>
    <div style="float: left;width: 45%;padding-left: 20px;">
        <div style="font-weight: bold;">Complete Client Certifications</div>
        <br>
        <ul style="list-style: inherit; margin-left: 40px;">
            <li>FieldSolutions offers a range of free </br>certifications that cover specific technical </br>skills as well as industry softskills </li>
            <li>Having completed a client certification</br> makes you eligible for that client's work </br>and tags your profile as client preferred</li>
        </ul>
        <br>
        <div style="font-weight: bold;">When bidding, add a short note in the bid comment box, such as:</div>
        <br>
        <ul style="list-style: inherit; margin-left: 40px;">
            <li>Your experience with the given scope of</br> work, ie: years of experience or  relevant </br>work history</li>
            <li>An explanation of your bid rate, ie: if</br> compensation is required for travel</li>
            <li>Miscellaneous notes, such as if you are</br> interested in negotiating  rate based on</br> being assigned multiple work orders in the</br> area</li>
        </ul>
        <div style="margin: 5px 0;text-align:center;">
            <a href="/techSignup.php?welcome=1&v=<?=$v?>"><img src="/widgets/images/devry_register_now.jpg" style="border: 0px;">    </a>
        </div>
    </div>
</div>