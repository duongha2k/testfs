<style>

	div.sectionHead {
	border-bottom: 2px solid;
	font-weight: bold;
	border-bottom-color: #950F66;
	}

	div.sectionHead2 {
	padding-top:10px;
	border-bottom: 2px solid;
	font-weight: bold;
	border-bottom-color: #950F66;
	}

	table.myClass {
	padding-top: 15px;
	}

</style>



<div class="sectionHead">ACCESS MORE WORK, MAKE MORE MONEY</div>
<div style="text-align:justify; padding-bottom:10px;">
LMS is increasingly running their nationwide field service work through FieldSolutions. To gain access to this work and 80,000 more work orders each year, you must be enrolled with FieldSolutions.
</div>

<div class="sectionHead2">WORK AVAILABLE THROUGH FIELDSOLUTIONS</div>
<table width="500" border="0" cellspacing="0" cellpadding="2" class="myClass">
  <tr>
    <td width="250" align="center" nowrap="nowrap"><strong>Displays</strong></td>
    <td width="250" align="center" nowrap="nowrap"><strong>Central Processing Units</strong></td>
  </tr>
  <tr>
    <td width="250" nowrap="nowrap"><ul>
      <li>Digital Signage</li>
      <li>Touch Screen</li>
      <li>Flat Panel and Theater</li>
      <li>Monitors </li>
    </ul></td>
    <td width="250" nowrap="nowrap"><ul>
      <li>PCs: Desktop, Laptop, Rugged</li>
      <li>Servers, Data Centers</li>
      <li>Point of Sale (POS)</li>
      <li>Kiosks, ATMs</li>
    </ul></td>
  </tr>
</table>

<table width="500" border="0" cellspacing="0" cellpadding="2" class="myClass">
  <tr>
    <td width="250" align="center" nowrap="nowrap"><strong>Peripherals</strong></td>
    <td width="250" align="center" nowrap="nowrap"><strong>Connectivity</strong></td>
  </tr>
  <tr>
    <td width="250" nowrap="nowrap"><ul>
      <li>Printers, Copiers</li>
      <li>Payment Transaction Devices</li>
      <li>Scanners</li>
      <li>Electromechanical</li>
    </ul></td>
    <td width="250" nowrap="nowrap"><ul>
      <li>Cabling: Internet, Phone, Coax, CCTV</li>
      <li>Internet Hubs, Routers, Ports</li>
      <li>WiFi</li>
      <li>Telephony</li>
    </ul></td>
  </tr>
</table>
