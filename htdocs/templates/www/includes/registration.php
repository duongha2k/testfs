<style type="text/css">
.required{
	padding: 0;
}

*{
	margin: 0;
	padding: 0;
	border: none;
	text-decoration: none;
	outline: none;
}
#wr{
	margin: 20px auto;
	width: 820px;
	text-align: left;
}
#acc dt, #acc dd{
	/*width: 100%;*/
}
.sectionHeading{
	cursor: pointer;
	display: block;
	padding: 3px 10px;
	background: #E1E1E1;
	color: #000;
	border: solid 1px #666;
	height: 20px;
	font-weight: bold;
	font-size: 14px;
	text-align:left;
}

#acc dt.act{
	background: #E1E1E!;
}

#acc dd{
	/*display: none;*/
	padding: 3px 10px;
}

.label{
	text-align: right;
	font-weight:bold;

}
/*14041*/   
#reqStar1{
    color: rgb(255, 0, 0); 
    font-size: 12px; 
    font-family: Verdana; 
    margin-left: 2px;
}
/*end 14041 */

td.field input.invalid, td.field select.invalid, tr.errorRow td.field input, tr.errorRow td.field select,
span.field input.invalid, span.field select.invalid{
    background-color: #FFFFD5;
    border: 2px solid red;
    color: red;
    margin: 0;
}
tr td.field div.formError, span.field div.formError {
    color: #FF0000;
    display: none;
}
tr.errorRow td.field div.formError {
    display: block;
    font-weight: normal;
}
div.invalid, label.invalid, label.invalid2 {
    color: red;
}
div.invalid a {
    color: #336699;
    font-size: 12px;
    text-decoration: underline;
}
#section{
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 1px solid #000;
}
#reqStar{
	color: rgb(255, 0, 0); 
	font-size: 12px; 
	font-family: Verdana; 
	margin-left: 2px;
}

.round {
    border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px; 
} 

.required{
	color: #000;
}
div.status{
	text-align: right;
	margin-right: 13px;
}

div#loader{
	display: none;
    width:100px;
    height: 100px;
    position: fixed;
    top: 50%;
    left: 50%;
    background:url(/widgets/images/loader.gif) no-repeat center #fff;
    text-align:center;
    padding:10px;
    font:normal 16px Tahoma, Geneva, sans-serif;
    border:1px solid #666;
    margin-left: -50px;
    margin-top: -50px;
    z-index:2;
    overflow: auto;
}

#W9Agreement p{
margin: 0 80px 0 80px;
}

a, form a{
font-size: 12px;
}

.sectionHeader{
border-bottom: 1px solid #000; 
margin-bottom: 5px;
}

#lookupLoader{
	background:url(/widgets/images/ajax-loader-orange.gif) no-repeat center #fff;
	width: 20px;
	height: 16px;
	float:right;
}
#lookupIdle{
	width: 20px;
	height: 16px;
	float:right;
}
</style>

<div id="loader">
	Loading...
</div>

<div id="wr">
	<dl id="acc">
		<dt id="basicRegistration" class="round sectionHeading">1. Technician Registration<span id="reqStar" style="float:right;">* Required Fields</span></dt>
		<dd id="section">
			<form name="techRegistration" id="techRegistration" method="post">
				<input type="hidden" name="tech_source" id="tech_source" value="" />
				<div class="error" style="display: none;"><span></span></div>
				<table style="margin-left: 15px; margin-right: auto; line-height: 20px;">
					<!--  Contact Information -->
					<tr style="border-bottom: 1px solid #000; margin-bottom: 5px; font-weight:bold; font-size: 15px;">
						<td colspan=2 >Contact Information</td>
					</tr>
				</table>
				<table style="margin-left: auto; margin-right: auto; line-height: 20px;">
					<tr><td></td></tr>
					<tr>
						<td class="label"><span id="reqStar">*</span>First Name: </td><td class="field"><input class="required" type="text" size="35" name="FName" id="FName" /> </td>
					</tr>
					<tr>
						<td class="label"><span id="reqStar">*</span>Last Name: </td><td class="field"><input class="required" type="text" size="35" name="LName" id="LName" /> </td>
					</tr>
					<tr>
						<td class="label"><span id="reqStar">*</span>Primary E-Mail: </td><td class="field"><input class="required" type="text" size="35" id="primaryEmail" name="primaryEmail" /><span id="lookupIdle"></span> </td>
					</tr>
					<tr>
  						<td colspan=2><div class="status"></div></td>
  					</tr>
					<tr>
						<td class="label"><span id="reqStar">*</span>Confirm Primary E-Mail: </td><td class="field"><input class="required" type="text" size="35" id="confPrimaryEmail" name="confPrimaryEmail" /> </td>
					</tr>
					<tr>
  						<td colspan=2><div class="status"></div></td>
  					</tr>
					<tr>
						<td class="label">Secondary E-Mail: </td><td class="field"><input type="text" size="35" name="secondaryEmail" /></td>
					</tr>
					<tr>
						<td class="label"><span id="reqStar">*</span>Business Hours Telephone: </td>
						<td class="field">
                    		<input class="required primaryPhone" type="text" size="35" name="PrimaryPhone" id="PrimaryPhone" /><span id="lookupIdle"></span> <!--14145-->
						</td>
					</tr>
  					<tr>
  						<td colspan=2><div class="status"></div></td>
  					</tr>
					<tr>
						<td class="label"><span id="reqStar">*</span>Evening &amp; Weekend Telephone: </td>
						<td class="field">
							<input class="required" type="text" size="35" name="SecondaryPhone" id="SecondaryPhone" />
						</td>
					</tr>
  					<tr>
  						<td colspan=2><div class="status"></div></td>
  					</tr>
					<tr>
					 <!--14057-->
						<td class="label" style="font-weight:normal;">Text Notifications: </td><td class="field">
							<input type="text" size="35" name="SMS_Number" id="SMS_Number" />
						</td>
					</tr>
  					<tr>
  						<td colspan=2><div class="status"></div></td>
  					</tr>
					<tr> 
           
						<td class="label" style="font-weight:normal;"  id="srvProvider">Service Provider: </td>
						<td class="field">
						 <?=$cellProviderHtml?>	
						</td>     
						<!--End 14057-->  
					</tr>
					
					<tr>
						<td colspan=2 style="width:430px; text-align:center;">Get work available notices! FieldSolutions does not charge for this service. 
						Standard message and data rates may apply.</td>
					</tr>
					</table>
					
					<table style="margin-left: 15px; margin-right: auto; line-height: 20px; width: 100%;">
					<!--  Primary Address -->
					<tr><td><br /></td></tr>
					<tr style="border-bottom: 1px solid #000; margin-bottom: 5px;font-weight:bold; font-size: 15px;">
						<td colspan=2 >Primary Address </td>
					</tr>
					<tr><td></td></tr>
					</table>
					
					<table style="margin-left: 275px; margin-right: auto; line-height: 20px;">
					<tr>
						<td class="label"><span id="reqStar">*</span>Country: </td><td class="field"><select class="required" id="country" name="country" ><?=$countriesHtml?></select> </td>
					</tr>
					<tr>
						<td class="label"> <span id="reqStar">*</span>Address Line 1: </td><td class="field"><input class="required" type="text" size="35" name="primaryAddress" /></td>
					</tr>
					<tr>
						<td class="label">Address Line 2: </td><td class="field"><input  type="text" size="35" name="primaryAddress2" /></td>
					</tr>
					<tr>
						<td class="label"> <span id="reqStar">*</span>City: </td><td class="field"><input class="required" type="text" size="35" name="city" /></td>
					</tr>
					<tr>
						<td class="label"><span id="reqStar1">*</span>State/Province: </td><td class="field"><span id="stateSelect"><?=$usStatesHtml;?></span> </td> <!-- 14070 -->  
					</tr>
					<tr>
						<td class="label"><span id="reqStar">*</span>Postal Code: </td><td class="field"><input class="required" id="zip" type="text" size="35" name="zip" /> </td>
						
					</tr>
					
					<!--  Primary Address -->
					<tr><td><br /></td></tr>
					</table>
					
					
					
					
					<table style="margin-left: 15px; margin-right: auto; line-height: 20px; width: 100%;">
					<!--  Login Information -->
					<tr><td><br /></td></tr>
					<tr style="border-bottom: 1px solid #000; margin-bottom: 5px;font-weight:bold; font-size: 15px;">
						<td colspan=2 >Login Information</td>
					</tr>
					</table>
					<table style="margin-left: 253px; margin-right: auto; line-height: 20px;">
					<tr><td></td></tr>
					<tr>
						<td class="label"><span id="reqStar">*</span>User Name: </td><td class="field"><input class="required" type="text" size="35" name="username" id="username" /></td><td><span id="lookupIdle"></span></td>   <!--14041-->
					</tr>
					<tr>
  						<td colspan=2><div class="status"></div></td>
  					</tr>
                                        <tr id="trerr" style="display:none;">
                                            <td  colspan="2">
                                                <div id="errmsg" style="color:red; width:100%; line-height: 12px;"></div>
                                            </td>
                                        </tr>
					<tr>
                                            <td class="label"><span id="reqStar">*</span>Password: </td>
                                            <td class="field">
                                                <input class="required" type="password" size="35" id="password" name="password" autocomplete="off"/>
                                            </td>
					</tr>
					<tr>
  						<td colspan=2><div class="status"></div></td>
  					</tr>
					<tr>
                                            <td class="label"><span id="reqStar">*</span>Confirm Password: </td>
                                            <td class="field">
                                                <input class="required" type="password" size="35" id="passwordconfirm" name="passwordconfirm" autocomplete="off"/>
                                            </td>
					</tr>
					<tr>
  						<td colspan=2><div class="status"></div></td>
  					</tr>
					<tr>
  						<td colspan=2><div class="status"></div></td>
  					</tr>
  					</table>
  					
  					<table style="margin-left: 130px; margin-right: auto; line-height: 20px;">
					<!--  Registration Agreements -->
					<tr><td><br/></td></tr>
					
					<tr><td></td></tr>
					
					<tr>
  						<td style="font-weight:bold;"><span id="reqStar">*</span>Enter Verification Code Shown Below:</td>
						<td class="field"> <input type="text" size="35" name="captcha" id="captcha" /><br /></td>
  					</tr>
  					<tr>
  						<td colspan=2><div class="status"></div></td>
  					</tr>
  					
					<tr>
    					<td colspan="2" align="right">
    						<div id="captchaimage">
    							<a href="" id="refreshimg" onclick="refreshimg(); return false;" title="Click to refresh image">
    								<img src="/ajax/captchaimage.php?<?php echo time(); ?>" alt="Captcha image" width="232" height="76" />
    							</a>
    						</div>
    					</td>
  					</tr>
  					</table>
  					
  					<!-- paste table here -->
  					<table cellspacing="0" cellpadding="0" width="800" border="0" id="techW9" style="display:none;">
					<tr><td>		
						<input type="hidden" name="techID" value="<?=$_SESSION['TechID']?>">

						<table>
						<tr><td class="divider"><br /></td></tr>
					
						<tr style="border-bottom: 1px solid #000; margin-bottom: 5px;">
							<td colspan=2 class="sectionHeader"><span style="color: #FFF; background-color: #6699CC;">W-9 Tax Information</span> (<a href="http://www.irs.gov/pub/irs-pdf/fw9.pdf?portlet=103" target="_blank">click here for instructions</a>)<span style="font-size: 10px; margin-top: 5px; float: right; color: rgb(255, 0, 0);">This information is encrypted during transmission and is stored on a secure site.</span></td>
						</tr>
						
						<tr><td class="divider"><br /></td></tr>
						<tr>
							<td><span class="label"><span id="reqStar">*</span>Name (as shown on your income tax return):</span></td>
						</tr>
						<tr>
							<td colspan=2>
								
								<span>First Name</span>
								<span class="field"><input  type="text" size="20" name="FirstName" id="FirstName" class="W9" /></span>
								
								<span>MI </span>
								<span class="field"><input  type="text" size="1" name="MI" class="W9" /></span>
								
								<span>Last Name</span>
								<span class="field"><input   type="text" size="20" name="LastName" id="LastName" class="W9" /></span>
							</td>
						</tr>
						<tr><td colspan=2><br /><hr /></td></tr>
						<tr>
							<td><span class="label"><span id="reqStar">*</span>Business name/disregarded entity name (if different from above):</span></td>
						</tr>
						<tr>
							<td colspan=2>
								
								<span class="label"> </span>
								<span class="field">
									<input type="text" size="75" name="BusinessName" class="W9" />
								</span>
							</td>
						</tr>
						<tr><td colspan=2 ><br /><hr /></td></tr>
						<tr>
							<td><span class="label"><span id="reqStar">*</span>Check appropriate box for federal tax classification:</span></td>
						</tr>

					<tr>
						<td class="label" style="vertical-align: top;">  </td>
					</tr>
					<tr>
						<td class="field" colspan=2>
							<table id="FedTaxClass" style="line-height: 20px;">
								<tr>
									<td><input type="radio" name="FTC" value="Individual/Sole Proprietor" /> Individual/Sole Proprietor</td>
								
									<td><input type="radio" name="FTC" value="C Corp" /> C Corporation</td>
								
									<td><input type="radio" name="FTC" value="S Corp" /> S Corporation</td>
								
									<td><input type="radio" name="FTC" value="Partnership" /> Partnership</td>
								
									<td><input type="radio" name="FTC" value="Trust/Estate" /> Trust/Estate</td>
								</tr>
								<tr>
									<td><input type="radio" name="FTC" value="LLC" /> Limited Liability Company. </td>
									
									<td colspan=4>Enter the tax classification (C= C corporation, S=S corporation, P=partnership) &rsaquo; <input type="text" size="5" name="LLCTax" id="LLCTax" /></td>
								
									<td style="padding-left: 20px;"><input type="checkbox" name="ExemptPayee" checked/> Exempt Payee</td>
								</tr>
								<tr>
									<td colspan=5><input type="radio" name="FTC" value="Other" id="FTCOther" /> Other (see instructions)
										<span id="FTCOtherDisplay" style="display:none;">
											<input type="text" size="20" name="FTCOtherInput" id="FTCOtherInput" />
										</span>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr><td colspan=2 ><br /><hr /></td></tr>
						<tr>
							<td style="width: 65%">
								<table>
									<tr>
										<td><span class="label"><span id="reqStar">*</span>Address (number, street, and apt. or suite no.) </span></td>
									</tr>
									<tr>
										<td colspan=2>
											<span class="field">
												<input type="text" size="75" name="Address1" class="W9" />
											</span>
										</td>
									</tr>
									
									<tr>
										<td><span class="label"><span id="reqStar">*</span>City, state, and ZIP code</span></td>
									</tr>
									<tr>
										<td colspan=2 class="field">
											 
											<span class="field">
												<input type="text" size="20" name="City" id="City" class="W9" />
											</span>
											<span class="field">
												<select id="State" name="State" class="W9" ><?=$statesHtml?></select>
											</span>
											<span class="field">
												<input type="text" size="20" name="ZipCode" id="ZipCode"  class="W9"  />
											</span>
                                            
										</td>
									</tr>
									
									<tr>
										<td>List account number(s) here (optional)</td>
									</tr>
									<tr>
										<td colspan=2>
											<span class="label"> </span>
											<span class="field">
												<input
												  type="text" size="75" name="AccountNumbers" />
											</span>
										</td>
									</tr>
							</table>
						</td>
						<td style="width: 35%; padding-left: 10px;">
							<table>
								<tr>
									<td>Requester's name and address (optional)</td>
								</tr>
								<tr>
									<td>
										<span class="label"> </span>
										<span class="field">
											<table>
												<tr><td>FieldSolutions</td></tr>
												<tr><td>10400 Yellow Circle Dr. Suite 300</td></tr>
												<tr><td>Minnetonka, MN 55343</td></tr> 
											</table>
										</span>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
					
					<table cellspacing="0" cellpadding="0" width="800" border="0">
						<tr><td colspan=5><br /><hr /></td></tr>
						<tr>
							<td style="font-weight: bold;">
								<span style="padding: 0 10px 0 10px; color: #FFF; background-color: #000;">Part I</span>
								<span>Taxpayer Identification Number (TIN)</span>
							</td>
						</tr>
						<tr><td colspan=5><hr /></td></tr>
						<tr>
							<td style="width: 65%;"> 
								Enter your TIN in the appropriate box. The TIN provided must match the name given on the “Name�? line
								to avoid backup withholding. For individuals, this is your social security number (SSN). However, for a
								resident alien, sole proprietor, or disregarded entity, see the Part I instructions on page 3. For other
								entities, it is your employer identification number (EIN). If you do not have a number, see How to get a
								TIN on page 3.
							</td>
							<td style="vertical-align: middle; padding-left: 20px;">
								<table>
									<tr>
										<td class="label" style="text-align: left;"><span id="reqStar">*</span> Social Security Number: </td>
									</tr>
									<tr>
										<td class="field">
											<input type="radio" name="TIN" value="SSN" id="SSNRadio" /> 
											<span id="SSNInput"> 
												<input  type="text" size="3" maxlength="3" name="SSN1" id="SSN1" /> - 
												<input  type="text" size="2" maxlength="2" name="SSN2" id="SSN2" /> - 
												<input  type="text" size="4" maxlength="4" name="SSN3" id="SSN3" /> 
											</span>  
										</td>
									</tr>
								</table>
							</td>
						</tr>
						
						<tr>
							<td style="width: 65%;"> 
								Note. If the account is in more than one name, see the chart on page 4 for guidelines on whose
								number to enter.
							</td>
							<td style="vertical-align: middle; padding-left: 20px;">
								<table>
									<tr>
										<td class="label"><span id="reqStar">*</span> Employer Identification Number: </td>
									</tr>
									<tr>
										<td class="field">
											<input type="radio" name="TIN" value="EIN" id="EINRadio" />
											<span id="EINInput">
												<input type="text" size="2" maxlength="2" name="EIN1" id="EIN1" /> - 
												<input type="text" size="7" maxlength="7" name="EIN2" id="EIN2" /> 
											</span> 
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					
					<table cellspacing="0" cellpadding="0" width="800" border="0">
						<tr><td colspan=5><br /><hr /></td></tr>
						<tr>
							<td style="font-weight: bold;">
								<span style="padding: 0 10px 0 10px; color: #FFF; background-color: #000;">Part II</span>
								<span>Certification</span>
							</td>
						</tr>
						<tr><td colspan=5><hr /></td></tr>
						<tr>
							<td colspan=5 id="W9Agreement"> 
								Under penalties of perjury, I certify that:<br /><br />
								1. The number shown on this form is my correct taxpayer identification number (or I am waiting for a number to be issued to me), and<br /><br />
								
								2. I am not subject to backup withholding because: (a) I am exempt from backup withholding, or (b) I have not been notified by the Internal Revenue
								Service (IRS) that I am subject to backup withholding as a result of a failure to report all interest or dividends, or (c) the IRS has notified me that I am
								no longer subject to backup withholding, and<br /><br />
								
								3. I am a U.S. citizen or other U.S. person (defined below).<br /><br />
								
								<b>Certification instructions</b> You must cross out item 2 above if you have been notified by the IRS that you are currently subject to backup withholding
								because you have failed to report all interest and dividends on your tax return. For real estate transactions, item 2 does not apply. For mortgage
								interest paid, acquisition or abandonment of secured property, cancellation of debt, contributions to an individual retirement arrangement (IRA), and
								generally, payments other than interest and dividends, you are not required to sign the certification, but you must provide your correct TIN. See the
								instructions on page 4.<br />
							</td>
						</tr>
						<tr>
							<td style="border-top: 1px solid #000; border-bottom:1px solid #000;">
								<span class="label" style="border-right:1px solid #000; border-spacing: 0px; font-weight:bold;" >Enter your initials here to Certify the information contained herein, to be used for your IRS Form W-9: </span>
								<span class="field" style="padding-left: 10px;">
									<input type="text" name="DigitalSignature" id="DigitalSignature" size="2" maxlength="2" class="W9" />
								</span>
								<span id="reqStar">*</span>
							</td>
						</tr>
						<tr>
							<td colspan=2 align="right"><div class="status"></div></td>
						</tr>
	                    <tr><td>Print a copy of your W-9 now for your records. For your protection future views of your online W-9 will not show your SSN.</td></tr>                    
					</table>
					</td>
					</tr>
					</table>
  					
  					<table style="margin-left: auto; margin-right: auto; line-height: 20px; background: #E8E8E8; width: 600px; text-align:justify; margin-bottom:10px;">
  					<tr style="border-bottom: 1px solid #000; margin-bottom: 5px; font-weight:bold;">
						<td colspan=2 >Federal Government W-9 Tax Form</td>
					</tr>
					<tr><td></td></tr>
					<tr>
						<td>You may register and complete the W-9 tax form now or at a later date. To accept work NOW, complete your W-9 Tax form NOW. 
						You cannot accept work without a completed W-9 Tax Form.
						<input type="button" id="toggleW9" onclick="javascript:void(0);" value="Complete W-9 Now" name="enterW9" class="link_button middle2_button" style="height: 22px;"></td>
					</tr>
					</table>
                    <!--14145-->
                    
                    <table style="margin-left: 15px; margin-right: auto; line-height: 20px;">
                        <tr style="border-bottom: 1px solid #000; margin-bottom: 5px;font-weight:bold; font-size: 15px;">
                             <td colspan=2 >Source/Referral</td>
                        </tr>
                        <tr>
                        
                        <td><span id="reqStar">*</span>How did you head about FieldSolutions?</td>
                        <td class="field" style="padding-left:126px;">
                        
                        <?=$techSourceHtml;?>
                        
                        </td>
                        </tr>
                    
                    </table>
  					<!--14145-->
  					<table style="margin-left: 15px; margin-right: auto; line-height: 20px;">
                    
<?php   //14145
/*$httpHost = $_SERVER['HTTP_HOST'];
$requestUri = $_SERVER["REQUEST_URI"];
//894
$v=$_REQUEST['v'];
$TechClass = new Core_Api_TechClass;

if(!empty($v))
{
    $techSource = $TechClass->getTechSourcenew($httpHost,$requestUri,$v);
}
else
{
$techSource = $TechClass->getTechSource($httpHost,$requestUri);
}

print_r("Techsoure:");print_r($techSource);  die();
//end 894 
*/
?>

  					<tr style="border-bottom: 1px solid #000; margin-bottom: 5px;font-weight:bold; font-size: 15px;">
						<td colspan=2 >Registration Agreements</td>
					</tr>
					<tr>
						<td ><a href="javascript:openPopup('/content/Privacy_Policy.pdf');"><span id="reqStar">*</span>Privacy Statment: </a></td>
						<td class="field">
							<input class="required" type="radio" size="35" name="privacyStatement" value="1" /> I Agree
							<input class="required" type="radio" size="35" name="privacyStatement" value="0" /> I Do Not Agree</td>
					</tr>
					<tr>
  						<td colspan=2><div class="status"></div></td>
  					</tr>
					<tr>
						<td ><a href="javascript:openPopup('/content/Terms_Use.pdf');"><span id="reqStar">*</span>Terms of Use: </a></td>
						<td class="field">
							<input class="required" type="radio" size="35" name="termsOfUse" value="Yes" id="agreeTermsYes" /> I Agree
							<input class="required" type="radio" size="35" name="termsOfUse" value="No" checked="checked" id="agreeTermsNo" /> I Do Not Agree 
							
						</td>
					</tr>
					<tr>
  						<td colspan=2><div class="status"></div></td>
  					</tr>
					<tr>
						<td ><a href="javascript:openPopup('/content/techTerms_combined.php');"><span id="reqStar">*</span>Independent Contractor Agreement:</a><span style="font-weight:bold;">(Must <a href="javascript:openPopup('/content/techTerms_combined.php');">view</a> and scroll to the bottom of the page) </span></td>
						<td class="field">
							<input class="required" type="radio" size="35" name="IAC" value="1" id="agreeICAYes" disabled="disabled" /> I Agree
							<input class="required" type="radio" size="35" name="IAC" value="0" id="agreeICANo" checked="checked" /> I Do Not Agree 
						</td>
					</tr>
					<tr>
  						<td colspan=2><div class="status"></div></td>
  					</tr>
					<tr><td><br /></td></tr>
					<tr>
                                        <input type="hidden" value="<?= $_REQUEST["devry"] ?>" name="devry" id="devry" />
                                        <!--894-->
                                        <input type="hidden" value="<?= $_REQUEST["v"] ?>" name="v" id="v" />
                                        <!--end 894-->
						<td colspan=2 align="center"><input style="height: 22px;" class="link_button middle2_button" type="submit" value="Submit" id="submit1" /></td>
					</tr>
					
				</table>
				
				
			</form>
		</dd>
	</dl>
</div>
