<style>

	div.sectionHead {
	border-bottom: 2px solid;
	font-weight: bold;
	border-bottom-color: #A00;
	}

</style>


<p>
<div class="sectionHead">ABOUT FIELDSOLUTIONS</div>
<div style="text-align:justify">
FieldSolutions is a leader in providing  third party field technicians serving the most advanced technology companies in the world. Their on-line work order management system and fast payment programs provide work to over 2000 technicians each week. Only FieldSolutions offers a performance rewards program and continuous training and skill development  opportunities. FieldSolutions' technologies include: PC's, telephony, internet, cabling, POS, ATMs, cable TV, and CCTV/security. 
</div>
</p>

<p>
<div style="font-weight: bold; text-align:justify">
FieldSolutions is always seeking more, high quality technicians throughout North American with skills in Computer Install and De-Install, Cabling, Networking, Swap Outs, Diagnose and Repair, and Troubleshooting.
</div>
</p>