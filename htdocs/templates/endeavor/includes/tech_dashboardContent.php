<style>

div.sectionHead {
	border-bottom: 2px solid;
	font-weight: bold;
	text-indent:5px;
	padding-left:5px;
	border-bottom-color: #A00;
}

div.sectionBody {
	padding:5px;
	width: 450px;
	padding-left:450px;
	padding-bottom:10px;
}

.unordered_list { padding-left: 20px; }
.unordered_list li { list-style: disc outside !important; display: list-item !important; }

.ordered_list { padding-left: 20px; }

/*ul {
	list-style-type: disc !important;
}*/

</style>


<div class="sectionHead">A New Way to Work With Endeavor</div>
<div class="sectionBody">
	In order to better support our customers and service partner network we have licensed the FieldSolutions portal technology for all of our service provider communications and payments.<br/><br/>
	The new Endeavor branded portal delivered through the FieldSolutions platform allows us to automate communication with our technicians, and allows our technicians to take advantage of new mobile (iPhone and Android) applications to accept and manage work orders. In addition, it allows us to more efficiently collect call closing information, approve payments faster and reduce your telephone hold times.
</div>

<div class="sectionHead">FieldSolutions, Our New Partner</div>
<div class="sectionBody">Endeavor chose FieldSolutions based on its integrity, service support team, responsiveness to the needs of Endeavor, and its focus on quality and efficiency.</div>
<div class="sectionBody">
  <ul class="unordered_list">
    <li>Across the technology service industry, FieldSolutions is preferred by providers based on its integrity, reliability, focus on quality, and support team. Our expectations are that you will find, as we have, that FieldSolutions will be responsive to your needs for a technology platform.</li>
  </ul><br/>
  <a href="javascript:showAbout()">Read More</a>
</div>  



<div class="sectionHead">What's in it for You?</div>
<div class="sectionBody">Aside from faster payments, there are a range of conveniences and efficiency tools that come with FieldSolutions. And you'll also be able to make more money in one place. When you explore FieldSolutions website, take advantage of all it offers.</div>
<div class="sectionBody">
  <ul class="unordered_list">
    <li>Sign up for free services like its mobile apps that make you more efficient.</li>
    <li>Sign up for the FS-PlusOne Rewards program that gives you ways to earn cash bonuses for doing a great job, and getting more skills and certifications</li>
    <li>And complete your Technician Profile so you are more likely to get more work from other companies as well.</li>
  </ul><br/>
  <a href="javascript:accessWork()">Read More</a>
</div>  


<div class="sectionHead">Get Started!</div>
<div class="sectionBody">
  <ol class="ordered_list">
    <li>Register and create your Technician Profile</li>
    <li>View Available Work in your area</li>
    <li>Apply for work you are qualified and interested in</li>
  </ol>
</div>  
<div class="sectionBody">
	<div><a href="http://www.screencast.com/t/EabxqPsvm">Watch the Getting Started Tutorial</a></div>
	<div><a href="javascript:getWork()">Read Quick Steps for Getting Work</a></div>
	<div><a href="javascript:getTips()">Read Tips for Techs</a></div>
	<!-- <div><a href="../techSignup.php">REGISTER NOW</a></div> -->
	<div><a href="/techSignup.php">REGISTER NOW</a></div>	
</div>

<script type="text/javascript">

var showAbout = function(){
$("<div></div>").fancybox(
{
'type' : 'iframe',
'href' : '../templates/<?php echo $siteTemplate;?>/includes/AboutFieldSolutions.php',
'autoDimensions' : false,
'width' : 600,
'height' : 400,
'transitionIn': 'none',
'transitionOut' : 'none'
}
).trigger('click');
};

var accessWork = function(){
$("<div></div>").fancybox(
{
'type' : 'iframe',
'href' : '../templates/<?php echo $siteTemplate;?>/includes/AccessWork.php',
'autoDimensions' : false,
'width' : 600,
'height' : 425,
'transitionIn': 'none',
'transitionOut' : 'none'
}
).trigger('click');
};

var getTips = function(){
$("<div></div>").fancybox(
{
'type' : 'iframe',
'href' : '../templates/<?php echo $siteTemplate;?>/includes/GetTips.php',
'autoDimensions' : false,
'width' : 700,
'height' : 600,
'transitionIn': 'none',
'transitionOut' : 'none'
}
).trigger('click');
};


var getWork = function(){
$("<div></div>").fancybox(
{
'type' : 'iframe',
'href' : '../templates/<?php echo $siteTemplate;?>/includes/GetWork.php',
'autoDimensions' : false,
'width' : 700,
'height' : 425,
'transitionIn': 'none',
'transitionOut' : 'none'
}
).trigger('click');
};

var registerNow = function(){

//	var exdate=new Date();
//	exdate.setDate(exdate.getDate() + 1);
//	var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
//	document.cookie="Endeavor" + "=" + "true";

//	$("<div></div>").fancybox().trigger('click');
};

</script>