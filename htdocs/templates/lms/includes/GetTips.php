<style>

	div.sectionHead {
	border-bottom: 2px solid;
	font-weight: bold;
	border-bottom-color: #950F66;
	}

	div.sectionHead2 {
	padding-top:10px;
	border-bottom: 2px solid;
	font-weight: bold;
	border-bottom-color: #950F66;
	}

	table.myClass {
	padding-top: 15px;
	text-align:justify;
	}

</style>



<div class="sectionHead">TIPS FOR TECHS</div>
<table width="650" border="0" cellspacing="0" cellpadding="2" class="myClass">
  <tr>
    <td width="300" align="left" valign="bottom" nowrap="nowrap"><strong>Complete your Profile</strong></td>
    <td width="10" align="left" valign="bottom" nowrap="nowrap">&nbsp;</td>
    <td width="300" align="left" valign="bottom" nowrap="nowrap"><strong>Complete Client Certifications</strong></td>
  </tr>
  <tr>
    <td width="300"><ul>
      <li>Clients almost always review a technician&rsquo;s profile to assess if the tech is qualified for a given work order</li>
      <li>Most important profile categories to complete are your skill ratings, tools and equipment, and resume</li>
    </ul></td>
    <td width="10">&nbsp;</td>
    <td width="300"><ul>
      <li>FieldSolutions offers a range of free certifications that cover specific technical skills as well as industry softskills</li>
      <li>Having completed a client certification makes you eligible for that client&rsquo;s work and tags your profile as client preferred</li>
    </ul></td>
  </tr>
  <tr>
    <td width="300">&nbsp;</td>
    <td width="10">&nbsp;</td>
    <td width="300">&nbsp;</td>
  </tr>
  <tr>
    <td width="300" align="left" valign="bottom"><strong>Set up Payment</strong></td>
    <td width="10" align="left" valign="bottom">&nbsp;</td>
    <td width="300" align="left" valign="bottom"><strong>When bidding, add a short note in the bid comment box, such as:</strong></td>
  </tr>
  <tr>
    <td width="300"><ul>
      <li>You must have a W9 on file with FieldSolutions in order to be paid. W9 is located in the top right of your tech homepage</li>
      <li>Sign up for Direct Deposit to receive pay deposited to your banking account, up a week earlier than the default option of being mailed a check</li>
    </ul></td>
    <td width="10">&nbsp;</td>
    <td width="300"><ul>
      <li>Your experience with the given scope of work, ie: years of experience or  relevant work history</li>
      <li>An explanation of your bid rate, ie: if compensation is required for travel</li>
      <li>Miscellaneous notes, such as if you are interested in negotiating  rate based on being assigned multiple work orders in the area</li>
    </ul></td>
  </tr>
  <tr>
    <td width="300" align="left">&nbsp;</td>
    <td width="10" align="left">&nbsp;</td>
    <td width="300" align="left">&nbsp;</td>
  </tr>
  <tr>
    <td width="300" align="left" valign="bottom"><strong>Complete a Background Check</strong></td>
    <td width="10" align="left">&nbsp;</td>
    <td width="300" align="left">&nbsp;</td>
  </tr>
  <tr>
    <td width="300"><ul>
      <li>Available through FieldSolutions for $19</li>
      <li>Background checks are required for 50% of FieldSolutions work and many clients prefer to assign a technician who has a background check on file</li>
    </ul></td>
    <td width="10" align="center">&nbsp;</td>
    <td width="300" align="center"><a href="../../../techSignup.php" target="_parent">Get Started Now</a></td>
  </tr>
</table>
