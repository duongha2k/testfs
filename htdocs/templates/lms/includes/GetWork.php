<style>

	div.sectionHead {
	border-bottom: 2px solid;
	font-weight: bold;
	border-bottom-color: #950F66;
	}

	div.sectionHead2 {
	padding-top:10px;
	border-bottom: 2px solid;
	font-weight: bold;
	border-bottom-color: #950F66;
	}

	table.myClass {
	padding-top: 15px;
	}

</style>



<div class="sectionHead">QUICK STEPS FOR GETTING WORK</div>
<div style="text-align:justify; padding-bottom:10px;">
LMS is increasingly running their nationwide field service work through FieldSolutions. To gain access to this work and 80,000 more work orders each year, you must be enrolled with FieldSolutions.
</div>

<table width="675" border="0" cellspacing="0" cellpadding="2" class="myClass">
  <tr>
    <td width="675" nowrap="nowrap"><strong>1) Create your Technician Profile</strong></td>
  </tr>
  <tr>
    <td width="675" nowrap="nowrap"><ul>
      <li>Register with FieldSolutions at www.FieldSolutions.com. Basic contact information is required to create your profile</li>
      <li>Highlight your skills and expertise in your technician profile.  The more you add, the more likely you will be selected for work. </li>
    </ul></td>
  </tr>
  <tr>
    <td width="675" nowrap="nowrap"><strong>2) View our Current Postings of Available Work</strong></td>
  </tr>
  <tr>
    <td width="675" nowrap="nowrap"><ul>
      <li>Sign into your technician account and view available work orders on your dashboard.</li>
      <li>Download FieldSolutions iPhone and Android apps to view work while your in the field.</li>
      <li>Receive work alert emails and text messages when new work is published nearby.</li>
    </ul></td>
  </tr>
  <tr>
    <td width="675" nowrap="nowrap"><strong>3) Apply for Available Work Orders</strong></td>
  </tr>
  <tr>
    <td width="675" nowrap="nowrap"><ul>
      <li>Simply enter the rate you want for performing that work and submit to be considered by the buyer.</li>
      <li>Add comments or questions when you apply in order to communicate any unique requirements or costs to the coordinator.</li>
    </ul></td>
  </tr>
</table>
