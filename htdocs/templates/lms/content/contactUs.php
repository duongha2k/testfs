<?php $page = login; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>



<!-- Add Content Here -->

<br />

<div id="leftcontent">

<h1>Self-Help</h1>


<p>
<b>FLS/Service Call program Support</b>
</p>

<div id="workOrders" align="center" style="padding="10px">
	<script type="text/javascript" language="JavaScript">
	<!-- 
	function loadPage(list) {location.href=list.options[list.selectedIndex].value}
	-->
	</script>

	<form name="workOrders">
	<select name="file" size="1"
  		onchange="loadPage(this.form.elements[0])"
  		target="_parent._blank"
  		onmouseclick="this.focus()">
	<option value="">-------Select One-------</option>
	<option value="http://www.flsupport.com/3.html">FLS Online Training</option>
	<option value="http://www.flsupport.com/21.html">FLS Support</option>
	<option value="http://www.flsupport.com/24.html">FLS FAQ</option>
	<option value="http://flsupport.com/20.html">FLS Work Order Help</option>
	<option value="http://flsupport.com/15.html">FLS Badge/ID help</option>
	<option value="https://fieldsolutions.com/techs/ttb-payment.php">FLS/TB Payment Policy</option>
	</select>
	</form>

</div>

<br /><br />


<p>
<b>Questions regarding how The Technician Bureau works?</b>
</p>

<p><a href="/techs/ttb-model.php">How 
Technician Bureau works for the Technician</a></p>
<p><a href="/techs/ttb-payment.php">Technician Bureau Payment Policy</a></p>

<hr>

<p><b>Login problems?</b></p>

<p><a href="/misc/lostPassword.php">Password Recovery</a></p>


<hr noshade size="1">

<p><b>W-9 questions/problems?</b></p>

<p><a href="/techs/w9.php">Submit Your W-9 Form</a></p>




<br />
<br />
<br />
<br />
<br />
<br />
</div>


<div id="rightcontent">

<div id="contactUs" style="padding="10px"> 

<h1>Contact Us</h1>

<form id="contactUs" name="contactUs" method="post" onsubmit="return validate_form(this)" action="contactUsSend.php">


<div id="contactRow">Name:</span><image src="/images/pixel.gif" height="1" width="30" border="0">
<input id="vFromName" name="vFromName" size="25" maxlength="255">
</div>

<div id="contactRow">Email:<image src="/images/pixel.gif" height="1" width="37" border="0"><input id="vFromEmail" name="vFromEmail" size="25" maxlength="255">
</div>

<div id="contactRow">Topic:<image src="/images/pixel.gif" height="1" width="35" border="0">
<select name="getTopicSelect" id="getTopicSelect" onchange='change(1)'>
<option>Select Topic</option>
</select>
<br />
</div>

<div id="subjectRow">Subject:<image src="/images/pixel.gif" height="1" width="23" border="0">
<select name="vSubjectSelect" id="vSubjectSelect"  onChange='change(null);' ><option>---</option></select>
<br /><br /></div>

<div id="WorkOrderRow">WO #:<image src="/images/pixel.gif" height="1" width="33" border="0">
<input id="Workorder" name="Workorder" size="25" maxlength="255" value="" onChange="ajaxFunction();">

<br />

</div>

<div id="clientInfoRow" align="center">none entered</div>

<div id="messageRow">Your Message:<br /><br />
<textarea id="vMessage" name="vMessage" rows="5" cols="40"></textarea>
</div>
<br />
<input id="eList" name="eList" value="support@fieldsolutions.com" type="hidden">
<input id="getTopic" name="getTopic" type="hidden">
<input id="vSubject" name="vSubject" type="hidden">
<input id="caller" name="caller" type="hidden" value="contactus">
<div id="contactRow" align="center"><input type="submit" value="Submit"></div>
</form>
<br>
</div>

<style type="text/css">
#subjectRow{display: none;}
#WorkOrderRow{display: none;}
#clientInfoRow{display: none;}
</style>

<script type='text/javascript'>
/**
* Class Options
*/
function Options () {
this.name = Array();
this.suboptions = Array();

this.add = function (name, suboptions) {
this.name[this.name.length] = name;
this.suboptions[this.suboptions.length] = suboptions;
}
this.get = function (index) {
return Array(this.name[index], this.suboptions[index]);
}
this.getRecurs = function (indexes, offset) {
if (indexes.length == 0)
return this;

var retVal = null;
if (offset == null)
offset = 0;
var index = indexes[offset];
if (index!= null) {
var o = this.suboptions[index];
if (offset == indexes.length -1)
retVal = o;
else
retVal = o.getRecurs(indexes, offset+1);
}
return retVal;
}
this.size = function () {
return this.name.length;
}
}

/**
* Class SelectLister
*/
function SelectList (options) {
this.selects = Array();
this.options = options;

this.register = function (elm, num) {
if (num == null)
num = this.selects.length;
this.selects[num] = elm;
}
this.build = function (index) {
var select = this.selects[index];
select.innerHTML = ""; // clean it up...

// Find chosen indices
var chosen = Array();
for (var i=0; i<index; i++)
chosen[chosen.length] = this.selects[i].selectedIndex;

// Show options...
var options = this.options.getRecurs(chosen, 0);
if (options!= null) {
var l = options.size();
for (var i=0; i<l; i++) {
var o = document.createElement("option");
o.innerHTML = options.name[i];
select.insertBefore(o, null);
}
}
}
}


var options, temp;
options = new Options();
options.add("Select One", null);

temp = new Options();
temp.add("--",null);
options.add("Sales", temp);

temp = new Options();
temp.add("Select One",null);
temp.add("Website Issues",null);
temp.add("Cannot Login",null);
temp.add("Other",null);
options.add("Technical Support", temp);

temp = new Options();
temp.add("--",null);
options.add("Client Support", temp);

temp = new Options();
temp.add("--",null);
options.add("W9 Issues", temp);

temp = new Options();
temp.add("Select One",null);
temp.add("Work Order not approved",null);
temp.add("Work Order Approved but not paid",null);
temp.add("Other",null);
options.add("Payment Issues", temp);

temp = new Options();
temp.add("Select One",null);
options.add("Other Questions or Comments", temp);


var selectList = new SelectList(options);
</script>


<script type='text/javascript'>
selectList.register(document.getElementById('getTopicSelect'));
selectList.register(document.getElementById('vSubjectSelect'));
selectList.build(0); // build first select

function change (depth) {
if (depth)
selectList.build(depth);

var eList = document.getElementById('eList');
var getTopic = document.getElementById('getTopic');
var vSubject = document.getElementById('vSubject');
var Workorder = document.getElementById('Workorder');


switch (selectList.selects[0].selectedIndex) {

// nothing selected
case 0:
document.getElementById('subjectRow').style.display = 'none';
document.getElementById('WorkOrderRow').style.display = 'none';
document.getElementById('clientInfoRow').style.display = 'none';

eList.value = 'support@fieldsolutions.com';
getTopic.value = 'TB Support';
vSubject.value = '';
Workorder.value = '';
break;

// Sales
case 1:
document.getElementById('subjectRow').style.display = 'none';
document.getElementById('WorkOrderRow').style.display = 'none';
document.getElementById('clientInfoRow').style.display = 'none';

eList.value = 'sales@fieldsolutions.com';
getTopic.value = 'TB Support: Sales';
vSubject.value = '';
Workorder.value = '';
break;

// Technical Support
case 2:
document.getElementById('subjectRow').style.display = 'inline';
document.getElementById('WorkOrderRow').style.display = 'none';
document.getElementById('clientInfoRow').style.display = 'none';

eList.value = 'support@fieldsolutions.com';
getTopic.value = 'TB Support: Technical';
Workorder.value = '';

if (selectList.selects[1].selectedIndex == 1) {
	vSubject.value = 'Website Issues';
} else if (selectList.selects[1].selectedIndex == 2){
	vSubject.value = 'Cannot Login';
} else {
	vSubject.value = 'other';
}

break;

// Client Support
case 3:
document.getElementById('subjectRow').style.display = 'none';
document.getElementById('WorkOrderRow').style.display = 'none';
document.getElementById('clientInfoRow').style.display = 'none';

eList.value = 'support@fieldsolutions.com';
getTopic.value = 'TB Support: Client Support';
vSubject.value = '';
Workorder.value = '';
break;

// W9 Issues
case 4:
document.getElementById('subjectRow').style.display = 'none';
document.getElementById('WorkOrderRow').style.display = 'none';
document.getElementById('clientInfoRow').style.display = 'none';

eList.value = 'sarah.kimberlin@fieldsolutions.com';
getTopic.value = 'TB Support: W-9 Question/Issue';
vSubject.value = '';
Workorder.value = '';
break;

// Payment Issues
case 5:
document.getElementById('subjectRow').style.display = 'inline';
document.getElementById('WorkOrderRow').style.display = 'inline';
document.getElementById('clientInfoRow').style.display = 'inline';
var ajaxDisplay = document.getElementById('clientInfoRow');
ajaxDisplay.innerHTML = 'None Entered';

getTopic.value = 'TB Support: Payment Issue';

if (selectList.selects[1].selectedIndex == 1) {
	eList.value = 'accounting@fieldsolutions.com';
	vSubject.value = 'Work Order not Approved';
} else if (selectList.selects[1].selectedIndex == 2){
	eList.value = 'accounting@fieldsolutions.com';
	vSubject.value = 'Work Order Approved but not paid';
} else {
	eList.value = 'support@fieldsolutions.com';
	vSubject.value = 'other';
}
break;

// Other Questions or Comments
case 6:
document.getElementById('subjectRow').style.display = 'none';
document.getElementById('WorkOrderRow').style.display = 'none';
document.getElementById('clientInfoRow').style.display = 'none';
var ajaxDisplay = document.getElementById('clientInfoRow');
ajaxDisplay.innerHTML = 'None Entered';

eList.value = 'support@fieldsolutions.com';
getTopic.value = 'TB Support: Other Questions or Comments';
vSubject.value = '';
Workorder.value = '';
break;


default:
alert ("?");
}
}


</script> 

<script language="javascript" type="text/javascript">
<!-- 

document.forms[1].Workorder.value = document.forms[1].Workorder.value;

			
//Browser Support Code
function ajaxFunction(){
	var ajaxRequest;  // The variable that makes Ajax possible!
	
	try{
		// Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				// Something went wrong
				alert("Your browser broke!");
				return false;
			}
		}
	}
	
// Create a function that will receive data sent from the server

ajaxRequest.onreadystatechange = function(){
 if(ajaxRequest.readyState == 4){

var ajaxDisplay = document.getElementById('clientInfoRow');
ajaxDisplay.innerHTML = ajaxRequest.responseText;
			
//document.forms[0].EditRecordTechFName.value = getTechInfo[1];
//document.forms[0].EditRecordTechLName.value = getTechInfo[2];
//document.forms[0].EditRecordTechEmail.value = getTechInfo[3];
//document.forms[0].EditRecordFLSTechID.value = getTechInfo[0];
//document.forms[0].EditRecordTB_ID.value = getTechInfo[4];


		}
	}
	
	
	var Workorder = document.getElementById('Workorder').value;
	var queryString = "?Workorder=" + Workorder;
	ajaxRequest.open("GET", "/ajax/retrieveWO.php" + queryString, true);
	ajaxRequest.send(null); 
}

//-->
</script>


<script type="text/javascript">
// Used to validate form

function validate_required(field,alerttxt)
{
with (field)
{
if (value==null||value=="")
  {alert(alerttxt);return false}
else {return true}
}
}

function validate_email(field,alerttxt)
{
with (field)
{
apos=value.indexOf("@")
dotpos=value.lastIndexOf(".")
if (apos<1||dotpos-apos<2) 
  {alert(alerttxt);return false}
else {return true}
}
}

function validate_form(thisform)
{
with (thisform)
{

	if (validate_required(vFromName,"Please enter your name must be filled out!")==false)
	  {vFromName.focus();return false}
		
	if (validate_required(vFromEmail,"You forgot to enter an email address!")==false)
	  {vFromEmail.focus();return false}
	  
	if (validate_email(vFromEmail,"Not a valid e-mail address!")==false)
	  {vFromEmail.focus();return false}
	  
	if (validate_required(vMessage,"You forgot to enter a message!")==false)
	  {vMessage.focus();return false}
	
	
	
	
}
}
</script>


<!--- End Content --->
<?php require ("../footer.php"); ?>
		
