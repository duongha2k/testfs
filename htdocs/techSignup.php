<?php $page = login; ?>
<?php $option = tech; ?>
<?php require ("header.php"); ?>
<?php require ("navBar.php"); ?>
<!-- Add Content Here -->
<style type="text/css">
<!--
@import url(https://www.fieldsolutions.com/css/techRegister.css);
//-->
</style>

<?php if ($_REQUEST["welcome"] != "1"): ?>
<script type="text/javascript">
//<![CDATA[
	jQuery.cookie ("Tech_Source", null, { path: "/", domain: ".fieldsolutions.com" });
//]]>
</script>
<?php endif; ?>

<div id="clientSignup">

<div id="intro">
<p>Field Solutions&#8482; is a leader in sourcing and management of independent third party field technicians serving the electronics industry. Our on-line work order management system and fast payment programs give work to over 1000 technicians each week. We never charge our technicians a fee.  
</p>
</div> <!-- end DIV "intro" -->

<div id="sidebar">

<p>We need field service technicians throughout North America. See listings below for details on those job types:</p>

<ul>
	<li><a href="/descPOSTech.php">POS (Point of Sale) Technician</a></li>
  <li><a href="/descCablingTech.php">Cabling Technician</a></li>
  <li><a href="/descNetworkTech.php">Network Technician</a></li>
  <li><a href="/descElectronicsTech.php">Home Electronics Technician</a></li>
</ul>

</div> <!-- end DIV "sidebar" -->

<div id="body">


<p>We need field service technicians throughout North America with skills in Computer Install and De-install, Cabling, Networking, Swap Outs, Diagnose and repair, and Troubleshooting. FieldSolutions technologies include: PC's, telephony, internet, cabling, Point Of Sale (POS), cable TV, and CCTV/security. 
</p>
<h1>3-Steps to Getting Work through Field Solutions&#8482;:
</h1>

<div class="steps">
<ol id="steps">
	<li>Register</li
  ><li>View</li>
  <li>Bid</li>
</ol>
</div> <!-- end DIV "steps" -->
<!--894-->
<div class="register">
    <form method="GET" action="https://<?= $siteTemplate ?>.fieldsolutions.com/techs/register.php?devry=<?= $_REQUEST["devry"] ?>&v=<?= $_REQUEST["v"] ?>">
	<input class="register" type="submit" value="Register" style="cursor: hand; cursor: pointer;" />
        <input type="hidden" value="<?= $_REQUEST["devry"] ?>" name="devry" />
        <input type="hidden" value="<?= $_REQUEST["v"] ?>" name="v" />
</form>
</div> <!-- end DIV "register" -->
<!--end 894-->
<br style="clear: both;" />

<h3>1. Create your Technician Profile
</h3>
<p>To create your profile highlighting your skills and geographic area, register now. The more you add to your profile the more likely you will be selected for work. Please take the time to complete your profile including skills, certifications, upload your resume and other relevant information.  
</p>
<h3>2. View our Current Postings
</h3>
<p>You'll receive a confirmation email. After you have acknowledged that notification, you can go to www.FieldSolutions.com as a registered technician and view all of our open work orders and postings. You'll also start receiving emails inviting you to apply for postings.
</p>
<p>Search for independent contractor work by viewing open work orders from the "Tech Dashboard" screen or responding to emails sent to you by ourselves or our clients.
</p>
<h3>3. Bid on Open Work Orders
</h3>
<p>Registered technicians are then able to bid on open work orders. You place a bid on an open work order by clicking on "Details" button and scrolling to the bottom of the work order. Enter the price for which you are willing to do the work and you are on your way... 
</p>
</p>
<p align="center">If  you have questions, email us at <a href="mailto:support@fieldsolutions.com">support@fieldsolutions.com</a>
</p>
<div class="register">
    <form method="GET" action="/techs/register.php?devry=<?= $_REQUEST["devry"] ?>">
	<input class="register" type="submit" value="Register" style="cursor: hand; cursor: pointer;" />
        <input type="hidden" value="<?= $_REQUEST["devry"] ?>" name="devry" />
</form>
</div> <!-- end DIV "register" -->

</div> <!-- end DIV "body" -->

</div>

<!---
<div id="adWrap">
<br /><br />
<div align="center" id="adsenseFooter">

<script type="text/javascript">
google_ad_client = "pub-3938591336002460";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_type = "text_image";
//2007-10-11: Field Solutions Site
google_ad_channel = "7447584706";
google_color_border = "FFFFFF";
google_color_bg = "FFFFFF";
google_color_link = "174065";
google_color_text = "000000";
google_color_url = "000000";
</script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br />

</div>
<br /><br />
</div>
--->

<!--- End Content --->
<?php require ("footer.php"); ?><!-- ../ only if in sub-dir -->
		
