<?php
	require_once("library/caspioAPI.php");
	require_once("library/smtpMail.php");
		
	$techList = caspioSelectAdv("TR_Master_List", "UserName, PrimaryEmail", "From_FM = '1'", "", false, "`", "|", false);
	
	$subject = "subject";

	foreach ($techList as $tech) {
		$info = explode("|", $tech);
		$username = trim($info[0], "`");
		$email = trim($info[1], "`");
		
		$message = "Dear Technician,
 
As a registered Technician with FieldMart, we are alerting you of our recent 
merger with Field Solutions. This immediately affects you. Your Tech 
profile will be transferred to Field Solutions database, including your 
original login for accessing your profile at FieldMart web site.  
 
Note:  We had to change the usernames of some Tech profiles as their
originals are already being used in Field Solutions database.    

Your username is:  $username
 
Transferring  of your profile  into Field Solutions database will give 
you visibility and access to open work orders that you can apply for/bid on 
right now .
 
It is strongly recommend that you spend a few minutes logging into your
account in the new location at  www.fieldsolutions.com  as soon as
possible via the link below. Again, you will use the same password that
you did when logging in at FieldMart. Most of you will also use the same
username, with some being issued  a new one. Whatever is listed above
for a username, is what you should use to login to your account at TB. If
your you do not remember your password, please visit the password
recovery link below before contacting support. 

TB Tech login: https://www.fieldsolutions.com/techs/index.php
 
Password recovery: https://www.fieldsolutions.com/misc/lostPassword.php
 
Once successfully logged into your account at the new location, we
encourage you to look around and get involved. Your should fill in any
blank fields that you can in your profile to make it complete in the new
database. Sorry for the inconvenience with possibly having to enter
information that you have already entered in the past at FieldMart's web
site. Due to different file structures, etc, we may be unable to transfer
some of the data contained in your FieldMart profile.
 
Thanks for your patience during the transfer process. If you need
assistance at any time, please contact support via the link below. 
 
http://fieldsolutions.com/content/contactUs.php 
 
Thank you.
 
Regards,
 
John Sinning
Director
Field Solutions/FieldMart
 
Dan Kraemer
Technician Training & Support
Field Solutions / Field Solutions";

		$html_message = "Dear Technician,
 
As a registered Technician with FieldMart, we are alerting you of our recent 
merger with Field Solutions. This immediately affects you. Your Tech 
profile will be transferred to Field Solutions database, including your 
original login for accessing your profile at FieldMart web site.  
 
Note:  We had to change the usernames of some Tech profiles as their
originals are already being used in Field Solutions database.    

Your username is:  $username
 
Transferring  of your profile  into Field Solutions database will give 
you visibility and access to open work orders that you can apply for/bid on 
right now .
 
It is strongly recommend that you spend a few minutes logging into your
account in the new location at  <a href='http://www.fieldsolutions.com'>www.fieldsolutions.com</a>  as soon as
possible via the link below. Again, you will use the same password that
you did when logging in at FieldMart. Most of you will also use the same
username, with some being issued  a new one. Whatever is listed above
for a username, is what you should use to login to your account at TB. If
your you do not remember your password, please visit the password
recovery link below before contacting support. 

TB Tech login: <a href='https://www.fieldsolutions.com/techs/index.php'>https://www.fieldsolutions.com/techs/index.php</a>
 
Password recovery: <a href='https://www.fieldsolutions.com/misc/lostPassword.php'>https://www.fieldsolutions.com/misc/lostPassword.php</a>
 
Once successfully logged into your account at the new location, we
encourage you to look around and get involved. Your should fill in any
blank fields that you can in your profile to make it complete in the new
database. Sorry for the inconvenience with possibly having to enter
information that you have already entered in the past at FieldMart's web
site. Due to different file structures, etc, we may be unable to transfer
some of the data contained in your FieldMart profile.
 
Thanks for your patience during the transfer process. If you need
assistance at any time, please contact support via the link below. 
 
<a href='http://fieldsolutions.com/content/contactUs.php'>http://fieldsolutions.com/content/contactUs.php</a>
 
Thank you.
 
Regards,
 
John Sinning
Director
Field Solutions/FieldMart
 
Dan Kraemer
Technician Training & Support
Field Solutions / Field Solutions";

		$html_message = nl2br($html_message);
		
//		echo "**** START <br/> email: $email <br/> $html_message <br/> END ****";
		
//		smtpMailLogReceived("FieldMart and Field Solutions", "MemberServices@FieldMart.com", $email, "FieldMart & Field Solutions merge technician databases", $message, $html_message, "Blast to FieldMart");
	}

//	print_r($techList);
	die();
?>