<style type="text/css">
	td { border:#385C7E solid medium; padding: 0px 5px; text-align: center; font-size:xx-small; }
	.errorCell { background-color: #FF8800; cursor: default; }
</style>

<div id="errorDiv" style="text-align: center; background-color: #FF0000; color: #FFFFFF; display: none; margin: 10px 0px; font-weight: bold;">Error</div>

<form id="uploadImport" name="uploadImport" method="post" action="<?php echo $_SERVER['PHP_SELF']?>" enctype="multipart/form-data">
	Import type:
	<select name="importType">
		<option value="FLS" selected="selected">FLS</option>
		<option value="Main">Main</option>
	</select><br/>
	File to parse:
	<input name="importFile" type="file" /> (limit 150k)<br/> 
	<input name="submit" type="submit" value="Submit" />
</form>

<?php
	require_once("library/caspioAPI.php");
	require_once("importInclude.php");

	if (isset($_POST["submit"])) {	
		if ($_POST["importType"] == "FLS") {
			$flsColumnMustEqual = array();
			$flsFieldPairValidator = array();
			$flsColumnMustEqual["Paid"] = "No";
			$flsColumnMustEqual["PayApprove"] = "0";
			$flsColumnMustEqual["DateImported"] = array(date("m/d/Y"), date("n/j/Y"), date("m/j/Y"), date("n/d/Y"));
			$flsColumnMustEqual["CallType"] = array("Service Call", "Install");
			
			$flsFieldPairValidator["ProjectName"] = new TwoFieldValidator("FLS_Projects", "ProjectName", "ProjectNo", "ProjectName", "Project_ID");
			$flsFieldPairValidator["Project_ID"] = $flsFieldPairValidator["ProjectName"];			
		}
		else {
			$mainColumnMustEqual = array();
			$mainFieldPairValidator = array();
			$mainColumnMustEqual["Approved"] = "0";
			$mainColumnMustEqual["Invoiced"] = "0";
			$mainColumnMustEqual["TechPaid"] = "0";
			$mainColumnMustEqual["Company_Name"] = $FIELD_EXISTS;
			$mainColumnMustEqual["Company_ID"] = $FIELD_EXISTS;
			$mainColumnMustEqual["Type_ID"] = array("1", "2");
			
			$mainFieldPairValidator["Company_Name"] = new TwoFieldValidator("TR_Client_List", "CompanyName", "Company_ID", "Company_Name", "Company_ID");
			$mainFieldPairValidator["Company_ID"] = $mainFieldPairValidator["Company_Name"];
			$mainFieldPairValidator["Project_ID"] = new TwoFieldValidator("TR_Client_Projects", "Project_ID", "Project_Name", "Project_ID", "Project_Name");
			$mainFieldPairValidator["Project_Name"] = $mainFieldPairValidator["Project_ID"];
		}
	}

	// caches text for multiple valid must equal values (ie "Service Call OR Install")
	$cachedMustEqual = array();

	$errorHtml = "";
	$errorCount = 0;
	
	function generateError($msg) {
		global $errorHtml, $errorCount;
		$errorHtml .= "$msg<br/>";
		$errorCount++;
	}
	
	if (isset($_POST["submit"])) {
		
		if ($_POST["importType"] == "FLS") {
			$tableName = "FLS_Work_Orders";
			$columnMustEqual = $flsColumnMustEqual;
			$columnFieldPairValidator = $flsFieldPairValidator;
		}
		else {
			$tableName = "Work_Orders";
			$columnMustEqual = $mainColumnMustEqual;
			$columnFieldPairValidator = $mainFieldPairValidator;
		}
		
		if (!isset($_FILES["importFile"]["tmp_name"]) || !file_exists($_FILES["importFile"]["tmp_name"])) {
			echo "No file Uploaded";
			die();
		}

		$columns = caspioGetTableDesign($tableName, FALSE);
		
		$tableColumns = array();
		foreach ($columns as $info) {
			$fieldInfo = explode(",",$info);
			$fieldName = $fieldInfo[0];
			$fieldType = $fieldInfo[1];
			$fieldUnique = $fieldInfo[2];
			$tableColumns[$fieldName] = new caspioFieldProperties($fieldType, $fieldUnique);
		}
		
		if ($_FILES["importFile"]["size"] > 150000) {
			// file too large
		}
			
		$handle = fopen($_FILES["importFile"]["tmp_name"], "r");
		$importColumns = fgetcsv($handle);
		
		// match import file columns to table columns and clean-up import column names
		$importColumnNum = sizeof($importColumns);
		
		$htmlTable = "<table cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;</td>";
		$columnList = array();
		for ($i = 0; $i < $importColumnNum; $i++) {
			$importColumns[$i] = trim($importColumns[$i]);
			$name = $importColumns[$i];
						
			if (!array_key_exists($name, $tableColumns)) {
				$htmlTable .= "<td class=\"errorCell\" title=\"No match found for $name\"><a name=\"$errorCount\">$name</a></td>";
				generateError("Column list error: No match found for <a href=\"#$errorCount\">$name</a>");
			}
			else if (array_key_exists($name, $columnList)) {
				// duplicate column
				$htmlTable .= "<td class=\"errorCell\" title=\"Duplicate column: $name\"><a name=\"$errorCount\">$name</a></td>";
				generateError("Column list error: Duplicate column <a href=\"#$errorCount\">$name</a>");
			}
			else {
				$htmlTable .= "<td>$name</td>";
				$columnList[$name] = 1;
			}
		}

		$htmlTable .= "</tr>";
		
		checkMissingMustEqualColumns($columnMustEqual, $importColumns);

		$currRow = 1;
		
		$woidList = array();
			
		while (!feof($handle)) {
			$importValues = fgetcsv($handle);
			if (sizeof($importValues) == 1) {
				// blank row not at end of file
				if (!feof($handle)) {
					$htmlTable .= "<tr><td>$currRow</td><td class=\"errorCell\"colspan=\"$importColumnNum\" title=\"Row is blank\"><a name=\"$errorCount\">&nbsp;</a></td>";
					generateError("Row <a href=\"#$errorCount\">$currRow</a>: Row is blank");
				}
				$currRow++;
				continue;
			}
			
			$htmlTable .= "<tr><td>$currRow</td>";
			$importValuesNum = sizeof($importValues);
			
			foreach ($columnFieldPairValidator as $val) {
				$val->resetValue();
			}
			
			for ($i = 0; $i < $importColumnNum; $i++) {
				if ($i >= $importValuesNum) {
					// number columns more then number of values
					$htmlTable .= "<td>&nbsp;</td>";
					continue;
				}
				if (!array_key_exists($importColumns[$i], $tableColumns)) {
					// non-matching column - don't check values
					$htmlTable .= "<td>{$importValues[$i]}</td>";
					continue;
				}				
				$column = $tableColumns[$importColumns[$i]];
				$output = ($importValues[$i] == "" ? "&nbsp;" : $importValues[$i]);
				if (array_key_exists($importColumns[$i], $columnFieldPairValidator) && $columnFieldPairValidator[$importColumns[$i]]->setValue($importColumns[$i], $importValues[$i])) {
					$validator = $columnFieldPairValidator[$importColumns[$i]];
					if (!$validator->validate()) {
						$htmlTable .= "<td class=\"errorCell\"><a name=\"$errorCount\">$output</a></td>";
						$validator->generateErrorMsg($currRow, $errorCount);
					}
					else
						$htmlTable .= "<td>$output</td>";
				}
				else if (!checkMustEqual($importColumns[$i], $importValues[$i], $columnMustEqual)) {
					// verify any must equal values
					if (is_array($columnMustEqual[$importColumns[$i]])) {
						$htmlTable .= "<td class=\"errorCell\" title=\"{$importColumns[$i]} must equal {$cachedMustEqual[$importColumns[$i]]}\"><a name=\"$errorCount\">$output</a></td>";
						generateError("Row <a href=\"#$errorCount\">$currRow</a>: {$importColumns[$i]} must equal " . (array_key_exists($importColumns[$i], $cachedMustEqual) ? $cachedMustEqual[$importColumns[$i]] : $cachedMustEqual[$importColumns[$i]] = implode(" OR ", $columnMustEqual[$importColumns[$i]])));
					}
					else {
						$htmlTable .= "<td class=\"errorCell\" title=\"{$importColumns[$i]} must equal {$columnMustEqual[$importColumns[$i]]}\"><a name=\"$errorCount\">$output</a></td>";
						generateError("Row <a href=\"#$errorCount\">$currRow</a>: {$importColumns[$i]} must equal {$columnMustEqual[$importColumns[$i]]}");
					}
				}
				else if (!$column->checkValue($importValues[$i])) {
					// verify data type correctness
					generateError("Row <a href=\"#$errorCount\">$currRow</a>: {$importColumns[$i]} datatype mismatch (" . $column->getType() . ")");
					$htmlTable .= "<td class=\"errorCell\" title=\"{$importColumns[$i]} datatype mismatch (" . $column->getType() . ")\"><a name=\"$errorCount\">$output</a></td>";
				}
				else {
					// Main site duplicate WO_ID
					if ($importColumns[$i] == "WO_ID") {
						if (array_key_exists($importValues[$i], $woidList)) {
							generateError("WO_ID found more than once <a href=\"#$errorCount\">$currRow</a>: $importValues[$i]");
							$htmlTable .= "<td class=\"errorCell\" title=\"WO_ID found more than once\"><a name=\"$errorCount\">$output</a></td>";
						}
						else {
							$woidList[$importValues[$i]] = $currRow;
							$htmlTable .= "<td>$output</td>";
						}
					}
					else {
						$htmlTable .= "<td>$output</td>";
					}
				}
			}
			
			
			$htmlTable .= "</tr>";
			
			$currRow++;

		}
	}
	
	if (sizeof($woidList) > 0) {
		$woidList = "'" . implode("', '", array_flip($woidList)) . "'";
		$duplicates = caspioSelect("Work_Orders", "WO_ID", "WO_ID IN ($woidList)", "");
		if (sizeof($duplicates) > 0 && $duplicates[0] != "") {
			generateError("WO_IDs already in database: " . implode(", ", $duplicates));
		}
	}
	
	echo $errorHtml;
	echo $htmlTable;
?>

<script type="text/javascript">
	if (<?=$errorCount?> > 0)
		document.getElementById("errorDiv").style.display = "";
</script>