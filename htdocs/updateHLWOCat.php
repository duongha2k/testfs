<?php
	require_once("library/caspioAPI.php");
	ini_set("display_errors",1);
	
	$woTable = "Work_Orders";
	$criteria = "TR_Client_Projects.Project_ID = $woTable.Project_ID";
	$projectHeadline = "(SELECT Headline FROM TR_Client_Projects WHERE $criteria)";
	$projectWOCATID = "(SELECT WO_Category_ID FROM TR_Client_Projects WHERE $criteria)";
	$projectWOCATName = "(SELECT Category FROM WO_Categories WHERE Category_ID = $projectWOCATID)";
	
	// WOs with project ID and Default Headline and Default WO_Category AND the project headline and project WO Category ID is not blank
	$updateCriteria = "ISNULL(Project_ID, '') != '' AND (ISNULL(Headline, '') = 'New Work Available in your area' OR ISNULL(Headline, '') = '') AND ISNULL(WO_Category_ID,'') = '' AND (ISNULL($projectHeadline,'') != '' AND ISNULL($projectWOCATID,'') != '')";
		
//	caspioUpdate($woTable, "Headline, WO_Category_ID, WO_Category", "$projectHeadline, $projectWOCATID, $projectCATName", $updateCriteria, false);
	$woList = caspioSelectAdv($woTable, "TB_UNID, CAST($projectHeadline as varchar) + '^' + CAST($projectWOCATID as varchar) + '^' + CAST ($projectWOCATName as varchar)", $updateCriteria, "", false, "`", "|", false);
	
	$update = array();
	foreach ($woList as $wo) {
		if ($wo[0] == "") continue;
		$info = explode("|", $wo);
		$info[1] = trim($info[1], "`");
		if (!array_key_exists($info[1], $update))
			$update[$info[1]] = array();
		$update[$info[1]][] = $info[0];
	}
	
//	print_r($update);
//	die();

	foreach ($update as $vals => $wos) {
		$info = explode("^", $vals);
		$headline = $info[0];
		$catID = $info[1];
		$catName = $info[2];
		$woList = "'" . implode("','", $wos) . "'";
		echo "UPDATE $headline, $catID, $catName WHERE TB_UNID IN ($woList)";
//		print_r(caspioUpdate($woTable, "Headline, WO_Category_ID, WO_Category", "'$headline', '$catID', '$catName'", "TB_UNID IN ($woList)", false));
		echo "<br/>";
	}
	
	die();
?>
