<?php
if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE')) {
	session_cache_limiter("public");
	header("Cache-Control: must-revalidate, max-age=10");
}

//ini_set("display_errors", 1);
set_time_limit(3200);

function escapeCSVField($value) {
	$delimiter = ",";
	$enclosure = "\"";
	if (strpos($value, ' ') !== false ||
		strpos($value, $delimiter) !== false ||
		strpos($value, $enclosure) !== false ||
		strpos($value, "\n") !== false ||
		strpos($value, "\r") !== false ||
		strpos($value, "\t") !== false)
	{
		$str2 = $enclosure;
		$escaped = 0;
		$len = strlen($value);
		for ($i=0;$i<$len;$i++)
		{
			if (!$escaped && $value[$i] == $enclosure)
				$str2 .= $enclosure;
			$str2 .= $value[$i];
		}
		$str2 .= $enclosure;
		$value = $str2;
	}
	return $value;
}

// The following sets up variables for the date pop-ups.
function getDay($Day){
	switch($Day) {
		case 1:
			return(2);
			break;
		case 2:
			return(3);
			break;
		case 3:
			return(4);
			break;						
		case 4:
			return(5);
			break;
		case 5:
			return(6);
			break;				
		case 6:
			return(7);
			break;				
		case 7:
			return(0);
			break;
	}
}


$Today = date("m-d-Y");
$Tomorrow  = date("m-d-Y", mktime(0, 0, 0, date("m"), date("d")+1, date("Y")));

// This Week (Mon = 1 - Sun = 7)
$BOW = date("m-d-Y", mktime(0, 0, 0, date("m"), date("d")-getDay(date("N"))+1, date("Y")));
$EOW = date("m-d-Y", mktime(0, 0, 0, date("m"), date("d")-getDay(date("N"))+7, date("Y")));

// last Week (Mon = 1 - Sun = 7)
$BOLW = date("m-d-Y", mktime(0, 0, 0, date("m"), date("d")-getDay(date("N"))-6, date("Y")));
$EOLW = date("m-d-Y", mktime(0, 0, 0, date("m"), date("d")-getDay(date("N")), date("Y")));

// This Month
$BOM = date("m-d-Y", mktime(0, 0, 0, date("m"), date("d")-date("d")+1, date("Y")));
$EOM = date("m-d-Y", mktime(0, 0, 0, date("m")+1, date("d")-date("d"), date("Y")));

// Last Month
$PrevBOM = date("m-d-Y", mktime(0, 0, 0, date("m")-1, date("d")-date("d")+1, date("Y")));
$PrevEOM = date("m-d-Y", mktime(0, 0, 0, date("m"), date("d")-date("d"), date("Y")));

// Q1
$Q1Start = date("m-d-Y", mktime(0, 0, 0, 1, 1, date("Y")));
$Q1End = date("m-d-Y", mktime(0, 0, 0, 4, date("d")-date("d"), date("Y")));

// Q2
$Q2Start = date("m-d-Y", mktime(0, 0, 0, 4, 1, date("Y")));
$Q2End = date("m-d-Y", mktime(0, 0, 0, 7, date("d")-date("d"), date("Y")));

// Q3
$Q3Start = date("m-d-Y", mktime(0, 0, 0, 7, 1, date("Y")));
$Q3End = date("m-d-Y", mktime(0, 0, 0, 10, date("d")-date("d"), date("Y")));

// Q4
$Q4Start = date("m-d-Y", mktime(0, 0, 0, 10, 1, date("Y")));
$Q4End = date("m-d-Y", mktime(0, 0, 0, 13, date("d")-date("d"), date("Y")));

// H1
$H1Start = date("m-d-Y", mktime(0, 0, 0, 1, 1, date("Y")));
$H1End = date("m-d-Y", mktime(0, 0, 0, 7, date("d")-date("d"), date("Y")));

// H2
$H2Start = date("m-d-Y", mktime(0, 0, 0, 7, 1, date("Y")));
$H2End = date("m-d-Y", mktime(0, 0, 0, 13, date("d")-date("d"), date("Y")));

// YTD
$YTDStart = date("m-d-Y", mktime(0, 0, 0, 1, 1, date("Y")));
$YTDEnd = date("m-d-Y", mktime(0, 0, 0, date("m"), date("d"), date("Y")));


if (isset($_POST["formattedFields"])) {
//	$companyID = $_GET["v"];

	// download csv
	header("Content-disposition: attachment; filename=NoShow_and_Backouts_Report.csv");
	
	$formattedFields = $_POST["formattedFields"];

	echo $formattedFields;
	
	die();
} 
	

	$page =  "clients"; 
	$option = "reports";
	require ("header.php");
	require ("navBar.php");
//	require ("includes/adminCheck.php");
//	require ("includes/PMCheck.php");

	require_once("library/caspioAPI.php");
?>

<style type="text/css">
	#emailBlast {
		text-align: center;
	}
	.verdana2 {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		clear: both;
	}

	.verdana2bold {
		font-family:Verdana, Arial, Helvetica, sans-serif;
		font-size: small;
		font-weight: bold;
		clear: both;
	}

	.fieldGroup {
		padding-bottom: 5px;
		border-bottom: 2px solid #2A497D;
	}

	.formBox, .formBoxFooter {
		margin: 15px auto 0px auto;
		padding: 15px;
		width: 920px;
		color: #2A497D;
		font-size: 12px;
		border: 2px solid #2A497D;
	}

	.formBox input {
		font-size: 12px;
		height: 14px;
	}

	.formBoxFooter {
		background-color: #BACBDF;
		text-align: center;
		margin: 0px auto 10px auto;
		padding: 5px 15px;
		border-top: none;
	}
	.formRow3, .formRow4, .formRow5, .formRow6 {
		width: 100%;
		clear: both;
	}
	.formRow3 div {
		width: 33%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow4 div {
		width: 25%;
		float: left;
		padding: 0px;
		margin: 0px 0px 5px 0px;
	}

	.formRow5 div {
		width: 20%;
		float: left;
		margin-bottom: 5px;
	}

	.formRow6 div {
		width: 16%;
		float: left;
		margin-bottom: 5px;
	}

	.resultsTable {
		width: 80%;
		margin: 10px auto 0px auto;
	}

	.resultsTable thead {
		text-align: center;
		color: #2A497D;
		border: 1px solid #2A497D;
		background-color: #A0A8AA;
		cursor: default;
	}

	.sortAble, #nextPageBtn, #prevPageBtn {
		cursor: pointer;
	}

	.sortAbleSelected {
		cursor: pointer;
		background-color: #FF9900;
		color: #FFFFFF;
		font-weight: bold;
	}

	.resultsTable tfoot {
		text-align: center;
		color: #000000;
		border: 1px solid #2A497D;
		background-color: #A0A8AA;
	}

	.resultsTable td {
		padding: 3px 5px;
	}

	.resultsTable .evenRow {
		background-color: #E9EEF8;
	}

	.resultsTable .oddRow {
		background-color: #FBFCFD;
	}

	.resultsTable .preferred {
		background-color: #FFFF8C;
	}

	.searchControls {
		margin-top: 10px;
		text-align: center;
	}

	.noResultsDiv {
		margin-top: 10px;
		text-align: center;
	}

	#grayOutLoading {
		background-color: #999999;
		width: 100%;
		height: 100%;
		position: absolute;
		top: 0px;
		left: 0px;
		filter: alpha(opacity=50);
		opacity: 0.5;
		<?=(isset($_POST["search"]) ? "" : "display: none;")?>
	}
	#loadingMsg {
		margin: 25% auto;
		font-size: 20px;
		color: #FFFFFF;
		text-align: center;
	}

</style>

    <div id="grayOutLoading">
    	<div id="loadingMsg">
        	<img src="/images/loading.gif" /> Loading ...
        </div>
    </div>
    
<script type="text/javascript">
	$(document).ready(function() {
		$("#grayOutLoading").hide();
	});
</script>


<?php
if (!isset($_POST["search"])) {
	
	$DateRangeHtml = "<option value=\"showAll\" selected=\"selected\">Show All</option>";
	$DateRangeHtml .= "<option value=\"showToday\">Today</option>";
	$DateRangeHtml .= "<option value=\"showWeek\">This Week</option>";
	$DateRangeHtml .= "<option value=\"showLastWeek\">Last Week</option>";
	$DateRangeHtml .= "<option value=\"showMonth\">This Month</option>";
	$DateRangeHtml .= "<option value=\"lastMonth\">Last Month</option>";
	$DateRangeHtml .= "<option value=\"5\">Q1 " . date("Y") . "</option>";
	$DateRangeHtml .= "<option value=\"6\">Q2 " . date("Y") . "</option>";
	$DateRangeHtml .= "<option value=\"7\">Q3 " . date("Y") . "</option>";
	$DateRangeHtml .= "<option value=\"8\">Q4 " . date("Y") . "</option>";
	$DateRangeHtml .= "<option value=\"9\">H1 " . date("Y") . "</option>";
	$DateRangeHtml .= "<option value=\"10\">H2 " . date("Y") . "</option>";
	$DateRangeHtml .= "<option value=\"11\">YTD " . date("Y") . "</option>";

?>
<br /><br />

<div id="errorDiv" style="text-align: center; background-color: #FF0000; color: #FFFFFF; display: none; margin: 10px 0px; font-weight: bold;">Error</div>

<div align="center">
<form id="ActivityReport" name="ActivityReport" onsubmit="showLoadingMsg()" action="<?= $_SERVER['PHP_SELF']?>" method="post">
<table width="400" border="0" cellspacing="0" cellpadding="0">
   <tr>
    <td colspan="5" align="center"><h2>No-Show & Back-out Report</h2></td>
  </tr>
   <tr>
     <td colspan="5" align="left">&nbsp;</td>
   </tr>
   <tr>
    <td colspan="5" align="left"><label><b>Select Date Range:</b> </label><select id="DateRange" name="DateRange"><?=$DateRangeHtml?></select></td>
  </tr>
   <tr>
    <td colspan="5" align="center">&nbsp;</td>
  </tr>
 <tr>
    <td nowrap="nowrap">
   	<label><b>Start Date >=</b> </label><input id="StartDate" name="StartDate" type="text" size="13" />    </td>
    <td nowrap="nowrap">&nbsp;</td>
    <td nowrap="nowrap">
    <label><b>End Date <=</b> </label><input id="EndDate" name="EndDate" type="text" size="13" />    </td>
    <td nowrap="nowrap"><input id="v" name="v" type="hidden" value="<?=$_GET["v"]?>" /> &nbsp;</td>
    <td nowrap="nowrap">
    <input id="search" name="search" type="submit" value="Run Report" style="background-color:#385C7E; color:#FFFFFF; font-size:12px; font-weight:bold;" />	</td>
 </tr>
 <tr><td colspan="5"><br /><i>Note: Excludes work orders that have no start and completion dates.</i></td></tr>
</table>
</form>
</div>


<link rel="stylesheet" href="library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript" src="library/js/dateDropDown.js"></script>
<script type="text/javascript">
	var currYr = " <?=date("Y")?>";
	var Today = <?php echo "\"".$Today."\""; ?>;
	var BOW = <?php echo "\"".$BOW."\""; ?>;
	var EOW = <?php echo "\"".$EOW."\""; ?>;
	var BOLW = <?php echo "\"".$BOLW."\""; ?>;
	var EOLW = <?php echo "\"".$EOLW."\""; ?>;
	var BOM = <?php echo "\"".$BOM."\""; ?>;
	var EOM = <?php echo "\"".$EOM."\""; ?>;
	var PrevBOM = <?php echo "\"".$PrevBOM."\""; ?>;
	var PrevEOM = <?php echo "\"".$PrevEOM."\""; ?>;
	var Q1Start = <?php echo "\"".$Q1Start."\""; ?>;
	var Q1End = <?php echo "\"".$Q1End."\""; ?>;
	var Q2Start = <?php echo "\"".$Q2Start."\""; ?>;
	var Q2End = <?php echo "\"".$Q2End."\""; ?>;
	var Q3Start = <?php echo "\"".$Q3Start."\""; ?>;
	var Q3End = <?php echo "\"".$Q3End."\""; ?>;
	var Q4Start = <?php echo "\"".$Q4Start."\""; ?>;
	var Q4End = <?php echo "\"".$Q4End."\""; ?>;
	var H1Start = <?php echo "\"".$H1Start."\""; ?>;
	var H1End = <?php echo "\"".$H1End."\""; ?>;
	var H2Start = <?php echo "\"".$H2Start."\""; ?>;
	var H2End = <?php echo "\"".$H2End."\""; ?>;
	var YTDStart = <?php echo "\"".$YTDStart."\""; ?>;
	var YTDEnd = <?php echo "\"".$YTDEnd."\""; ?>;

	function showLoadingMsg() {
		$("#grayOutLoading").show();
	}
	
	$(document).ready(function(){
		$('#StartDate').calendar({dateFormat: 'MDY/'});
		$('#EndDate').calendar({dateFormat: 'MDY/'});
		$("#DateRange").change(filterDate);
	});

</script>



<?php
} else {
?>

<?php
//$StartDate = date("m-d-Y", mktime(0, 0, 0, date("m"), date("d")-60, date("Y")));
$Start =  $_POST['StartDate'] != '' ? $_POST['StartDate'] : "01/01/1907";
$End =  $_POST['EndDate'] != '' ? $_POST['EndDate'] : "12/31/2099";

$wos = caspioSelectAdv("Work_Orders", "TB_UNID, convert(varchar, StartDate, 101), convert(varchar, EndDate, 101), PayMax, NoShow_Tech, BackOut_Tech, Amount_Per, Company_ID, Company_Name, Deactivated", "StartDate >= '$Start' AND StartDate <= '$End' AND (ISNULL(NoShow_Tech, '') != '' OR ISNULL(BackOut_Tech, '') != '') ", "", false, "`", "|^",false);


echo "<br><div id=\"results\"><form id=\"formattedFields\" name=\"formattedFields\" action=\"" . $_SERVER['PHP_SELF'] . "\" method=\"post\">
	<table class=\"resultsTable\"><thead>";
echo "<tr class=\"resultTop\"><td colspan=\"14\" valign=\"left\"><input type=\"image\" src=\"https://bridge.caspio.net/images/box_blue.gif\" name=\"image\" align=\"left\" border=\"0\"/><a href=\"https://www.fieldsolutions.com/BackoutNoShowReport.php\">New Report</a></td></tr><tr class=\"resultHeader\">";

echo "<td scope=\"col\" valign=\"bottom\">WIN #</th>
<td scope=\"col\" valign=\"bottom\">Start Date</th>
<td scope=\"col\" valign=\"bottom\">End Date</th>
<td scope=\"col\" valign=\"bottom\">Max Pay</th>
<td scope=\"col\" valign=\"bottom\">No-Show Tech ID</th>
<td scope=\"col\" valign=\"bottom\">No-Show Name</th>
<td scope=\"col\" valign=\"bottom\">No-Show Email</th>
<td scope=\"col\" valign=\"bottom\">Back-Out Tech ID</th>
<td scope=\"col\" valign=\"bottom\">Back-Out Name</th>
<td scope=\"col\" valign=\"bottom\">Back-Out Email</th>
<td scope=\"col\" valign=\"bottom\">Amount Per</th>
<td scope=\"col\" valign=\"bottom\">Company ID</th>
<td scope=\"col\" valign=\"bottom\">Company Name</th>
<td scope=\"col\" valign=\"bottom\">Deactivated</th>";

echo "</tr></thead><tbody>";

$formattedFields = "WIN#, Start Date, End Date, Max Pay, No-Show Tech ID, No-Show Name, No-Show Email, Back-Out Tech ID, Back-Out Name, Back-Out  Email, Amount Per, Company ID, Company Name, Deactivated \n";

$rowNumber = 1;
$rowColor = ($rowNumber % 2 == 0 ? "evenRow" : "oddRow");

foreach ($wos as $key => $order) {
	$fields = explode("|^", $order);
	$fields = preg_replace('/NULL/','',$fields);

	$win = trim($fields[0], "`"); // Remove quotes	
	$start = trim($fields[1], "`"); // Remove quotes	
	$end = trim($fields[2], "`"); // Remove quotes	
	$maxPay = trim($fields[3], "`"); // Remove quotes	
	$noShow_ID = trim($fields[4], "`"); // Remove quotes	
	$backOut_ID = trim($fields[5], "`"); // Remove quotes	
	$amountPer = trim($fields[6], "`"); // Remove quotes	
	$companyID = trim($fields[7], "`"); // Remove quotes	
	$companyName = trim($fields[8], "`"); // Remove quotes	
	$deactivated = trim($fields[9], "`"); // Remove quotes	

//	$amountPer = ($amountPer == "") ? "&nbsp;" : $amountPer;

	$noShowName = "";
	$noShowEmail = "";
	
	if($noShow_ID != '') {	
		$techs = caspioSelectAdv("TR_Master_List", "FirstName, LastName, PrimaryEmail", "TechID = '$noShow_ID'", "", false, "`", "|^",false);
		if (sizeof($techs) == 1 && $techs[0] != '') {
			$techFields = explode("|^", $techs[0]);
			$noShowName = trim($techFields[1], "`") . ", " . trim($techFields[0], "`");
			$noShowEmail = trim($techFields[2], "`");
		}
	}

	$backOutName = "";
	$backOutEmail = "";

	if($backOut_ID != '') {	
		$techs = caspioSelectAdv("TR_Master_List", "FirstName, LastName, PrimaryEmail", "TechID = '$backOut_ID'", "", false, "`", "|^",false);
		if (sizeof($techs) == 1 && $techs[0] != '') {
			$techFields = explode("|^", $techs[0]);
			$backOutName = trim($techFields[1], "`") . ", " . trim($techFields[0], "`");
			$backOutEmail = trim($techFields[2], "`");
		}
	}
	
//	$backoutName = "";
//	$backoutEmail = "";
	
	
	echo "<tr class=\"" . $rowColor."\">
	<td>$win</td>
	<td>$start</td>
	<td>$end</td>
	<td>$maxPay</td>
	<td>$noShow_ID</td>
	<td>$noShowName</td>
	<td>$noShowEmail</td>
	<td>$backOut_ID</td>
	<td>$backOutName</td>
	<td>$backOutEmail</td>
	<td>$amountPer</td>
	<td>$companyID</td>
	<td>$companyName</td>
	<td>$deactivated</td>
	</tr>";
	
	$formattedFields .= escapeCSVField($win) . ",". escapeCSVField($start) . ",". escapeCSVField($end) . ",". escapeCSVField($maxPay) . ",". escapeCSVField($noShow_ID) . ",". escapeCSVField($noShowName) . "," . escapeCSVField($noShowEmail) . "," . escapeCSVField($backOut_ID) . "," . escapeCSVField($backOutName) . "," . escapeCSVField($backOutEmail) . ",". escapeCSVField($amountPer) . ",". escapeCSVField($companyID) . ",". escapeCSVField($companyName) . ",". escapeCSVField($deactivated)."\n";
	
	$rowNumber ++;
	$rowColor = ($rowNumber % 2 == 0 ? "evenRow" : "oddRow");
	ob_flush();
	flush();

}
echo "<tr><td><b>WO's:</b></td><td><b>".sizeof($wos)."</b></td></tr>";
echo "</table>";
	echo "</tbody></table>  <textarea name=\"formattedFields\" id=\"formattedFields\" style=\"display:none\">" . $formattedFields . "</textarea></div>";

}


?>