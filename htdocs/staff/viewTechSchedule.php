<!-- 
Created on: 12-03-07 
Author: CM
Description: Retrieves a techs schedule from the main site as well as FLS
-->

<link rel="stylesheet" href="../css/tablesorter.css" type="text/css" />

<script type="text/javascript" src="../library/jquery/jquery-packed-1.1.4.js"></script>
<script type="text/JavaScript" src="../library/jquery/tablesorter/jquery.tablesorter.pack.js"></script>

<table cellpadding="0" cellspacing="0" border="0" style="border:solid thin #2a497d">
	<tr>
<td>
<table id="wos" class="tablesorter" cellspacing="0" cellpadding="0">             
<thead>
    <tr>
<th colspan="7" align="center">
<div style="color:#000000; font-weight:bold; font-size:16px;" id='divtechName'>Schedule</div>
</th>
	</tr>
    <tr> 
<th><nobr>Start Date&nbsp;&nbsp;&nbsp;&nbsp;</nobr></th> 
<th><nobr>End Date&nbsp;&nbsp;&nbsp;&nbsp;</nobr></th> 
<th><nobr>Start Time&nbsp;&nbsp;&nbsp;&nbsp;</nobr></th>
<th><nobr>End Time&nbsp;&nbsp;&nbsp;&nbsp;</nobr></th>
<th>City&nbsp;&nbsp;&nbsp;&nbsp;</th>
<th>State&nbsp;&nbsp;&nbsp;&nbsp;</th>
<th>Zip&nbsp;&nbsp;&nbsp;&nbsp;</th>
    </tr> 
</thead> 
<tbody> 

<?php

//

try {

require_once("../library/caspioAPI.php");


$techID = $_REQUEST['techID'];
//$techID = "22723";
$today = date('m-d-Y', strtotime('now'));
// If Tech id and not reviewed
if ($techID != ""){

// Get Tech Contact info
$techInfo = caspioSelect("TR_Master_List", "techID, FirstName, LastName", "TechID = '$techID'", false);

// Populate array with WO info
foreach ($techInfo as $order) {
	$techInfoFields = getFields($order);
	$techID = $techInfoFields[0];
	$FirstName = $techInfoFields[1];
	$LastName = $techInfoFields[2];
}


// Get main Work Orders
$workOrders = caspioSelect("Work_Orders", "Tech_ID, StartDate, StartTime, EndDate, EndTime, City, State, Zipcode", "Tech_ID = '$techID' AND StartDate >= '$today' AND IsNull(Deactivated, '') = ''", 'StartDate',false);

$workOrderCount = count($workOrders);
$Main=1;
// Populate array with WO info
foreach ($workOrders as $order) {
	$workOrderFields = getFields($order);
	
	$Tech_ID = $workOrderFields[0];
	$StartDate = $workOrderFields[1];
	$StartTime = $workOrderFields[2];
	$EndDate = $workOrderFields[3];
	$EndTime = $workOrderFields[4];
	$City = $workOrderFields[5];
	$State = $workOrderFields[6];
	$Zipcode = $workOrderFields[7];
	
	if ($EndDate == "NULL"){$EndDate = "";}else{
	$EndDate = date("m/d/Y", strtotime($EndDate));}

$StartDate = date("m/d/Y", strtotime($StartDate));

if (date("Y", strtotime($StartDate)) != "1969"){
echo "<tr class='wosRow'>";
echo "<td>$StartDate</td><td>$EndDate</td><td>$StartTime</td>";
echo "<td>$EndTime</td><td><nobr>$City</nobr></td><td>$State</td><td>$Zipcode</td>";
echo "</tr>";
 }else{$Main=0;}
}

// Get FLS Work Orders
$flsworkOrders = caspioSelect("FLS_Work_Orders", "TB_ID, PjctStartDate, PjctStartTime, City, State, Zip", "TB_ID = '$techID' AND PjctStartDate >= '$today' AND IsNull(Deactivated, '') = ''", 'PjctStartDate', false);

$FLSworkOrderCount = count($flsworkOrders);
$FLS=1;

// Populate array with WO info
foreach ($flsworkOrders as $order) {
	$flsworkOrderFields = getFields($order);
	
	$FLSTech_ID = $flsworkOrderFields[0];
	$FLSStartDate = $flsworkOrderFields[1];
	$FLSStartTime = $flsworkOrderFields[2];
	$FLSEndDate = "";
	$FLSEndTime = "";
	$FLSCity = $flsworkOrderFields[3];
	$FLSState = $flsworkOrderFields[4];
	$FLSZipcode = $flsworkOrderFields[5];

$FLSStartDate = date("m/d/Y", strtotime($FLSStartDate));

if (date("Y", strtotime($FLSStartDate)) != "1969"){
echo "<tr class='wosRow'>";
echo "<td>$FLSStartDate</td><td>$FLSEndDate</td><td>$FLSStartTime</td>";
echo "<td>$FLSEndTime</td><td><nobr>$FLSCity</nobr></td><td>$FLSState</td><td>$FLSZipcode</td>";
echo "</tr>";
  }else{$FLS=0;}
 }
}

if ($FLS==0 && $Main==0 && $workOrderCount==1 && $FLSworkOrderCount==1){
echo "<tr class='wosRow'>";
echo "<td colspan='7' align='center'>None found</td></tr>";
}

}catch (SoapFault $fault) {
		
smtpMail("view Tech Schedule Script", "nobody@fieldsolutions.com", "collin.mcgarry@fieldsolutions.com", "view Tech Schedule Script Error", "$fault", "$fault", "viewTechSchedule.php");
}

?>

<tfoot>
<th colspan="7" align="right">&nbsp;</th>
</tfoot>
</tbody>
</table>
</td></tr></table>
<script language="JavaScript">

var displaytechName = document.getElementById('divtechName');
displaytechName.innerHTML = "<?php echo "$FirstName $LastName's "; ?>" +displaytechName.innerHTML;
		
$(document).ready(function() 
    { 
        $('#wos').tablesorter({widgets: ['zebra'], sortList: [[0,0]]});
		
	} 
); 
</script> 

<script type="text/javascript">
$(document).ready(function(){
$(".wosRow tr").mouseover(function() {$(this).addClass("over");}).mouseout(function() {$(this).removeClass("over");});
//$(".wosRow tr:even").addClass("alt");
});
</script>