<?php $page = staff; ?>
<?php $option = home; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php
// Check whether client is logged in
$loggedInAs = $_SESSION['loggedInAs'];

if($loggedIn == "yes" && $loggedInAs == "staff"){
// Redirect to dashboard.php
header( 'Location: dashboard.php' );
}else{
// Redirect to logIn.php
header( 'Location: logIn.php' );
} 
?>