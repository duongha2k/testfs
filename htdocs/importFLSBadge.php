<?php $page = admin; ?>
<?php $option = techs; ?>
<?php $selected = flsidupload; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->

<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script> <script type="text/javascript" language="javascript">try{f_cbload("193B0000D9H7B8C5C7D9H7B8C5C7","https:");}catch(li){;}</script> <div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193B0000D9H7B8C5C7D9H7B8C5C7">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>

<script type="text/javascript">
	try {
		var test = adminID;
	}
	catch (e) {
		window.location.replace("./");
	}
</script>

<style type="text/css">
</style>

<br/><br/>

<div style="text-align: center">
<form name="uploadImport" method="post" action="<?php echo $_SERVER['PHP_SELF']?>" enctype="multipart/form-data">
	<label for="importFile">FLS Badge Import:</label>
	<input name="importFile" type="file" size="40"/>
	<input name="ignoreWarning" type="checkbox" value="1" /> Ignore warnings<br/><br/>
	<input name="submit" type="submit" value="Submit" />
</form>

<br/>
<?php
	require_once("../library/caspioAPI.php");
//	require_once("importInclude.php");
	
	if (isset($_POST["submit"])) {
		// CSR# - FLSID
		// Warehouse - FLSWhse
		// Fedex Tracking - FLSBadgeFedX
		// FLSBadge
		
		$requiredColumns = array("TechID", "CSR#", "Warehouse", "Fedex Tracking");
		
		if (!isset($_FILES["importFile"]["tmp_name"]) || !file_exists($_FILES["importFile"]["tmp_name"])) {
			echo "No file Uploaded";
			die();
		}
		
		if ($_FILES["importFile"]["size"] > 150000) {
			// file too large
		}

		echo "Checking uploaded file ... <br/><br/>";
			
		$handle = fopen($_FILES["importFile"]["tmp_name"], "r");
		$importColumns = fgetcsv($handle);
		
		$sizeImport = sizeof($importColumns);
		$sizeRequired = sizeof($requiredColumns);
		if ($sizeImport != $sizeRequired) {
			echo "Unexpected number of columns found - expecting $sizeRequired but found $sizeImport";
			die();
		}
		
		$index = 0;
		foreach ($importColumns as $col) {
			if ($col != $requiredColumns[$index]) {
				echo "Unexpected column found - expecting '{$requiredColumns[$index]}' but found '$col'";
				die();
			}
			$index++;
		}
		
		$updateQuery = "";
		
		$TechIDList = array();
		$TechIDFoundMap = array();
		$updateQuery = array();
		
		while (!feof($handle)) {
			$importRow = fgetcsv($handle);
			if (sizeof($importRow) == 1 && $importRow[0] == '') continue;
			$TechID = $importRow[0];
			$TechIDList[] = "'$TechID'";
			$FLSID = $importRow[1];
			$warehouse = $importRow[2];
			$fedexTracking = $importRow[3];
			
			if (!is_numeric($TechID)) {
				echo "TechID is not a number: $TechID";
				die();
			}
			if (!array_key_exists($TechID, $TechIDFoundMap)) {
				$TechIDFoundMap[$TechID] = 0;
			}
			else {
				echo "Duplicate TechID: $TechID";
				die();
			}
			$updateQuery["'$TechID'"] = "'$FLSID', '$warehouse', '$fedexTracking', '1'";			
		}
		
		if (sizeof($TechIDList) == 0) {
			echo "No data found.";
			die();
		}
		
		$numImportedRows = sizeof($TechIDList);
		$TechIDList = implode(",", $TechIDList);
		
		$TechIDFound = caspioSelect("TR_Master_List", "TechID, FLSID", "TechID IN ($TechIDList)", "");
			
		$error = false;
		$warning = false;
		
		if ($numImportedRows != sizeof($TechIDFound)) {
			foreach ($TechIDFound as $row) {
				$id = explode(",", $row, 2);
				$id = $id[0];
				if (array_key_exists($id, $TechIDFoundMap)) {
					$TechIDFoundMap[$id] = 1;
				}
			}
			echo "Unable to find the following TechID: <br/>";
			foreach ($TechIDFoundMap as $key=>$val) {
				if ($val == 1) continue;
				echo "$key<br/>";
				$error = true;
			}
			echo "<br/>";
		}
				
		foreach ($TechIDFound as $row) {
			$row = explode(",", $row);
			$id = $row[0];
			$flsID = $row[1];
			
			if ($flsID == "NULL" || $flsID == "''") continue;
			$warning = true;
			echo "Warning: TechID $id already has a FLSID - $flsID<br/>";
		}
		
		if ($error || ($warning && !isset($_POST["ignoreWarning"])) ) {
			echo "<br/>No data imported.";
			die();
		}
					
		echo "<br/>Checks Successful.<br/>";
		
		echo "<br/>Importing ... <br/>";
		foreach ($updateQuery as $TechID => $values) {
//			echo "UPDATE (FLSID, FLSWhse, FLSBadgeFedX, FLSBadge) $values WHERE TechID = $TechID<br/>";
			caspioUpdate("TR_Master_List", "FLSID, FLSWhse, FLSBadgeFedX, FLSBadge", "$values", "TechID = $TechID");
		}
		
		echo "<br/>Import Complete.<br/>";
	}
?>
</div>
<?php require ("../footer.php"); ?>
