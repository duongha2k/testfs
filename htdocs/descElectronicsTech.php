<?php $page = login; ?>
<?php $option = tech; ?>
<?php require ("header.php"); ?>
<?php require ("navBar.php"); ?>
<!-- Add Content Here -->
<style type="text/css">
<!--
@import url(https://www.fieldsolutions.com/css/techRegister.css);
//-->
</style>

<div id="clientSignup">

<div id="intro">
<p>Field Solutions&#8482; is a leader in sourcing and management of independent third party field technicians serving the electronics industry. Our on-line work order management system and fast payment programs give work to over 1000 technicians each week. We never charge our technicians a fee.  
</p>
</div> <!-- end DIV "intro" -->

<div id="sidebar">

<p>We need field service technicians throughout North America. See listings below for details on those job types:</p>

<ul>
	<li><a href="/descPOSTech.php">POS (Point of Sale) Technician</a></li>
  <li><a href="/descCablingTech.php">Cabling Technician</a></li>
  <li><a href="/descNetworkTech.php">Network Technician</a></li>
  <li>Home Electronics Technician</li>
  <li><a href="/techSignup.php">3 Steps to Getting Work through Fieldsolutions</a></li>
</ul>

</div> <!-- end DIV "sidebar" -->

<div id="body">

<h1>Home Electronics Technician
</h1>
<h2>General Requirements:
</h2>
<ul>
	<li>Excellent customer service and communications skills.</li>
	<li>Must be flexible to work some nights and weekends.</li>
	<li>Ability to self supervise work in virtual capacity.</li>
	<li>Detailed-orientated to complete required paperwork and submit information to management on a daily basis.</li>
	<li>Requires ability to follow standard operating procedures.</li>
</ul>

<h2>Specific Requirements:
</h2>
<ul>
	<li>Audio/video experience
	<li>Drill, sockets, level and other tools to complete job.</li>
	<li>Must be able to lift up to 75 pounds on your own.</li>
	<li>Satellite installation/experience helpful.</li>
</ul>

<p><a href="/techs/register.php">Register</a> or <a href="/techs/logIn.php">Log In</a> to search for available jobs in your area.</p>


</div> <!-- end DIV "body" -->

</div>

<!---
<div id="adWrap">
<br /><br />
<div align="center" id="adsenseFooter">

<script type="text/javascript">
google_ad_client = "pub-3938591336002460";
google_ad_width = 728;
google_ad_height = 90;
google_ad_format = "728x90_as";
google_ad_type = "text_image";
//2007-10-11: Field Solutions Site
google_ad_channel = "7447584706";
google_color_border = "FFFFFF";
google_color_bg = "FFFFFF";
google_color_link = "174065";
google_color_text = "000000";
google_color_url = "000000";
</script>
<script type="text/javascript"
  src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br />

</div>
<br /><br />
</div>
--->

<!--- End Content --->
<?php require ("footer.php"); ?><!-- ../ only if in sub-dir -->
		
