   <?php
    // fix selected undefined warning test abcd efffefefefeffefefefefefefefefefef
    if (!isset($selected) ) {
        $selected = null;
    }
    $_GET['v'] = ( isset($_GET['v']) ) ? $_GET['v'] : NULL;
	$Company_ID =  $_GET['v'];
	if(!$includepA || !isset($includepA))
	{
?>
<script type="text/javascript" src="/widgets/js/json-serialization.js"></script>
<?php
if (empty($jqueryVersion)):
?>
<script type="text/javascript" src="/widgets/js/jquery.min.js"></script>
<?php
elseif ($jqueryVersion == '1.9.1'):
?>
<script type="text/javascript" src="/widgets/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="/widgets/js/jquery-migrate-1.2.1.min.js"></script>
<?php
endif;
?>
<?php script_nocache ("/widgets/js/jquery.idle-timer.js"); ?>
<script type="text/javascript" src="/widgets/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.tinysort.js"></script>

<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.easing.pack.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-mod.js"></script>
<link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" media="all" />
<?php } ?>


<!--script src="/widgets/js/functions.js?07242012"></script-->
<?php script_nocache ("/widgets/js/functions.js"); ?>
<?php script_nocache ("/widgets/js/global.js"); ?>
<script src="/widgets/js/base64.js"></script>
<link rel="stylesheet" href="/css/simpleDropdown/style.css" type="text/css" media="screen"/>
<!--[if lte IE 7]>
	<link rel="stylesheet" type="text/css" href="css/simpleDropdown/ie.css" media="screen" />
<![endif]-->
<script type="text/javascript" src="/library/jquery/jquery.dropdownPlain.js"></script>
<script type="text/javascript">
    window._company = '<?=$_GET['v']?>';
	function openMapping(url) {
        openPopupWin(url,null,{width:1200,height:650});
	}
	function openCreateWO(url) {
        openPopupWin(url);
	}
      
</script>
<script type="text/javascript">   
$(document).ready(function() { 
    $(".checkaccess a").click(function(){
        var _attribid=$(this).attr("attribid");
        var href=$(this).attr("href");
        $.post("/widgets/dashboard/client/accessctl/", {rt:'iframe',attributeId:_attribid}, 
            function (response) {
                if(response=="1") { window.location = href;} 
                else { Attention(response); }                        
        });                  
        return false;
    });
});    
</script>

<?php $siteTemplate = $_SESSION['template'];?>
<?php
    if ( isset($_SESSION['ISO_ID']) ) {
        $ISO_ID = $_SESSION['ISO_ID'];
    } else {
        $ISO_ID = NULL;
    }

    if ( isset($_SESSION['UserType']) ) {
        $UserType = $_SESSION["UserType"];
    } else {
        $UserType = NULL;
    }
?>

<!-- TOP NAVBAR -->
<ul id="maintab" class="dropdown" style="height: 24px;">
<?php if ( $page == "login" ) {?>
<!--<li class="selected"><a href="/index2.php">Login</a></li>-->
<?php } ?>

<?php if ( $page == "adminlogin" ) {?>
<!--Admin/Staff Login-->
<?php if ( $option == "adminLogin" ){echo '<li class="">';}else{echo '<li>';}?>

<?php if ( $option == "admin" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/admin/index.php">Admin</a></li>
<?php } ?>


<?php if($loggedIn=="yes" && $loggedInAs=="customers"){ ?>
<!-- CUSTOMER NAVBAR TOP -->
<?php if ( $option == "profile" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/quickticket/profile.php?v=<?=$_GET["v"]?>&t=<?=$_GET["t"]?>">My Profile</a></li>
<?
$CSapi = new Core_Api_CustomerClass();
    $QTV = $CSapi->getQTV_ByCustomerID($_SESSION['CustomerID']);
    if($QTV['EnableCreateWO']==1)
                {
?>
<?php if ( $option == "createQuickTicket" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/quickticket/create_QuickTicket.php?v=<?=$_GET["v"]?>&t=<?=$_GET["t"]?>">Create QuickTicket</a></li>
<?}
?>
<?php if ( $option == "projectStatus" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/quickticket/wos.php?v=<?=$_GET["v"]?>&t=<?=$_GET["t"]?>">QuickView</a></li>
<?php if ($Company_ID == "FATT"): ?>
	<li <?= $option == "quickTools" ? 'class="selected"' : "" ?>>
		<a href="/quickticket/tools.php?v=<?=$Company_ID?>&t=<?=$_GET["t"]?>">QuickTools</a>
	</li>
<?php endif; ?>
<?php } ?>

<?php if($loggedIn=="yes" && $loggedInAs=="tech"){ ?>
<?php if ( $page == "techs" || $page == "contactus") {?>
<!-- TECH NAVBAR TOP -->
<?php if ( $option == "profile" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/profile.php">My Profile</a></li>


<?php
if ( $option == "banking" ){
	echo '<li class="selected">';
}else{
	echo '<li>';
}?>

<?php
if($ISO_ID ==""){
echo "<a href='https://".$siteTemplate.".fieldsolutions.com/techs/banking.php'>Banking</a></li>";
} else {
echo "</li>";
}
?>

<!--a href="https://<?php //echo $siteTemplate;?>.fieldsolutions.com/techs/banking.php">Banking</a></li-->

<?php if ( $option == "skills" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/skills.php">Skills</a></li>
<?php if ( $option == "experience" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/experience.php">Experience</a></li>
<?php if ( $option == "certifications" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/certifications.php">Certifications</a></li>
<?php if ( $option == "media" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/media.php">Video / Audio</a></li>
<?php if ( $option == "equipment" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/cablingSkills.php">Equipment</a></li>
<?php if ( $option == "wos" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/techs/wosAssigned.php">Work Orders</a></li>
<?php if ( $option == "backgroundChecks" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/techs/backgroundChecks.php">Background Checks</a></li>

<?php
if ($siteTemplate != "ruo") {

if ( $option == "training" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/techs/FAQs/index.php">Help</a></li>
<?php if ( $option == "more" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/techFLS.php">FLS</a></li>
<?php } ?>
<?php } ?>
<?php } ?>


<?php if($loggedIn=="yes" && $loggedInAs=="client"){ ?>
<?php if ( $page == "clients" || $page == "contactus") {?>
<!-- CLIENTS -->
<?php if ($UserType == "" || $UserType == "Admin") { ?>
<?php if ( $option == "techs" ){echo '<li style="margin-left: 2px" class="selected checkaccess">';}else{echo '<li class="checkaccess" style="margin-left: 2px">';}?>
<a attribid="6" check="true" class="AttentionLink" href="/clients/techs.php?v=<?=$_GET["v"]?>&newSearch=1">Find Techs</a>

	<ul class="submenu">
		<li><a check="true" attribid="6" class="AttentionLink" href="/clients/techs.php?v=<?=$_GET["v"]?>&newSearch=1">Find All Techs</a></li>
		<!--  li><a href="/clients/techs_cabling.php?v=<?=$_GET["v"]?>&newSearch=1">Find Cabling &amp; CCTV Techs</a></li -->
		<!--  li><a href="/clients/techs_telephony.php?v=<?=$_GET["v"]?>&newSearch=1">Find Telephony Techs</a></li -->
		<li><a check="true" attribid="6" class="AttentionLink" href="/clients/projectBlast.php?v=<?=$_GET["v"]?>">Send Project Email</a></li>
		<li><a check="true" attribid="6" class="AttentionLink" href="/clients/recruitmentEmail.php?v=<?=$_GET["v"]?>">Send Recruitment Email</a></li>
		<?php if ( $_GET["v"] == "FLS" ):?>
		<li><a check="true" attribid="6" class="AttentionLink" href="/clients/FLS/importFLSBadge.php?v=<?=$_GET["v"]?>">FLS ID Upload</a></li>
		<?php endif; ?>
	</ul>

</li>
<?php } ?>

<?php if ($UserType == "" || $UserType == "Admin") { ?>

<?php if ( $option == "wos" ){echo '<li class="selected checkaccess">';}else{echo '<li class="checkaccess">';}?>
<a attribid="7"  href="/clients/wos.php?v=<?php echo $_GET["v"]?>&reset=1">My Work Orders</a>
	<ul class="submenu">
		<li><a attribid="7"  class="AttentionLink" href="/clients/wos.php?v=<?php echo $_GET["v"]?>&reset=1">My Dashboard</a></li>
		<li><a attribid="2" class="AttentionLink" href="/clients/createWO.php?v=<?=$_GET["v"]?>">Create a Work Order</a></li>
		<li><a attribid="9" class="AttentionLink" href="/clients/wosView.php?v=<?php echo $_GET["v"]?>">Find a Work Order</a></li>
		<li><a attribid="10" class="AttentionLink" href="/clients/recruitmentEmail.php?v=<?=$_GET["v"]?>">Send Recruitment Email</a></li>
		<li><a attribid="11" class="AttentionLink" href="/clients/projectBackupTechBlast.php?v=<?=$_GET["v"]?>">Send Back-Up Tech Email</a></li>
		<li><a attribid="12" class="AttentionLink" href="/clients/projectBlast.php?v=<?=$_GET["v"]?>">Send Project Email</a></li>
		<li><a attribid="13" class="AttentionLink" href="/clients/importWO.php?v=<?php echo $_GET["v"]?>">Upload Work Orders</a></li>

<?php } else {?>
<?php if ( $option == "wos" ){echo '<li class="selected">';}else{echo '<li>';}?>
 <a href="/clients/wosView.php?v=<?php echo $_GET["v"]?>">My Work Orders</a>

	<ul class="submenu checkaccess">
        <li><a href="wos.php?v=<?php echo $_GET["v"]?>">My Dashboard</a></li>
		<li><a attribid="2" class="AttentionLink" href="/clients/createWO.php?v=<?php echo $_GET["v"]?>">Create a Work Order</a></li>
		<li><a href="javascript:openPopup('/clients/wosView.php?v=<?php echo $_GET["v"]?>', null, 750)">Find a Work Order</a></li>
<?php } ?>
<?php
	require_once("library/caspioAPI.php");
	$db = Zend_Registry::get('DB');
	$select = $db->select();
	$select->from(Core_Database::TABLE_CLIENTS, "ProjectManager")
		->where("UserName = " . $db->quoteInto("?", $_SESSION['UserName']));
	$result = $db->fetchCol($select);
	if ($result === false)
		$isPM = FALSE; // username not found
	else
		$isPM = $result[0] == 1;
	if ($isPM)
            //14100
		echo "<li><a href='/clients/GPM_FindWO.php?v=".$_GET['v']."'>Find WIN#</a></li>";//14100
        //end 14100
?>

	</ul>

</li>

<?php
	if (isset($_SESSION["tbAdminLevel"])) {
		$_COOKIE["tbAdminLevel"] = $_SESSION["tbAdminLevel"];
		$getAdminLevel = $_SESSION["tbAdminLevel"];
//		unset($_SESSION["tbAdminLevel"]);
	}else{
		$getAdminLevel = ( isset($_COOKIE['tbAdminLevel']) ) ? $_COOKIE["tbAdminLevel"] : NULL;
	}
	
	
	if ( $getAdminLevel == "YES"){
		
		if ( $option == "projects") {
			echo '<li class="selected checkaccess">';
		}else{
			echo '<li class="checkaccess">';
		}
?>	
	
	<a attribid="23" href="/clients/projectsView.php?v=<?=$_GET["v"]?>&Start=&nbsp;&End=Z&Active=1">My Projects</a>
	<ul class="submenu">
		<li><a attribid="23" href="/clients/projectsView.php?v=<?php echo $_GET["v"]?>&Start=&nbsp;&End=Z&Active=1">View / Edit Projects</a></li>
		<li><a attribid="24" href="/clients/projectsCreate_new.php?v=<?php echo $_GET["v"]?>">Add Project</a></li>
		<li><a attribid="25" href="/clients/importSites.php?v=<?php echo $_GET["v"]?>">Import Site List</a></li>
	</ul>
</li>

<?php  } /*end $getAdminLevel if */	?>

<?php if ($UserType == "" || $UserType == "Admin") { ?>
<?php if ( $option == "fsmapper" ){echo '<li class="selected checkaccess">';}else{echo '<li class="checkaccess" >';}?>
<a attribid="26" href="/clients/techs.php?v=<?=$_GET["v"]?>&newSearch=1&navBar=1">FS-Mapper</a>
</li>
<?php } ?>
<?php

/*	if ( $getAdminLevel == "YES"):
	if ( $option == "customers" && $getAdminLevel == "YES"){echo '<li id="mainLinkCustomers" class="selected checkaccess">';}else{echo '<li class="checkaccess" id="mainLinkCustomers">';}?>
	<a attribid="41" class="AttentionLink" href="/clients/customersView.php?v=//<?=$_GET["v"]?>">QuickTicket</a>
	<ul class="submenu">
		<li><a attribid="42" class="AttentionLink" href="/clients/customersView.php?v=//<?php echo $_GET["v"]?>">Edit QuickTicket Users</a></li>
		<li><a attribid="43" class="AttentionLink" href="/clients/customersCreate.php?v=//<?php echo $_GET["v"]?>">Add QuickTicket User</a></li>
	</ul>*/
        if ( $getAdminLevel == "YES"):
	if ( $option == "quickview" && $getAdminLevel == "YES"){echo '<li id="mainLinkCustomers" class="selected checkaccess">';}else{echo '<li class="checkaccess" id="mainLinkCustomers">';}?>
	<a attribid="41" class="AttentionLink" href="profile.php?v=<?php echo $_GET["v"]?>&tabid=clientQuickTickets">QuickTicket</a>
</li>
<?php endif; ?>

<?php
/* REVERSE ORDER TO WHAT IS DISPLAYED DUE TO FLOAT RIGHT */

	if ( $option == "mysettings" ){echo '<li id="mainLinkProfile" class="selected">';}else{echo '<li id="mainLinkProfile">';}?>
<a href="/clients/profile.php?v=<?php echo $_GET["v"]?>&tabid=clientMySettings">Settings</a></li>

<?php if ($UserType == "Manager") {?>
	<?php if ( $option == "reports" ){echo '<li id="mainLinkReports" class="selected checkaccess">';}else{echo '<li id="mainLinkReports" class="checkaccess">';}?>
    <a attribid="27" href="#">Reports &nbsp;&nbsp;&nbsp;&nbsp;</a>
<?php } else {?>
	<?php if ( $option == "reports" ){echo '<li id="mainLinkReports" class="selected checkaccess">';}else{echo '<li id="mainLinkReports" class="checkaccess">';}?>
    <a attribid="27" href="#">Reports</a>
<?php } ?>

        <ul class="submenu checkaccess">
            <!--14100-->
            <?
                require_once("library/caspioAPI.php");
                $db = Zend_Registry::get('DB');
                $select = $db->select();
                $select->from(Core_Database::TABLE_CLIENTS, "ProjectManager")
                        ->where("UserName = " . $db->quoteInto("?", $_SESSION['UserName']));
                $result = $db->fetchCol($select);
                if ($result === false)
                    $isPM = FALSE; // username not found
                else
                    $isPM = $result[0] == 1;
                if ($isPM)
                {
                ?>
                <li><a href="/reports_test/custom_v2/reportlist.php?v=<?php echo $_GET["v"]?>">FS-ReportWriter</a></li>
                <?
                }
            ?>
            <!--end 14100-->
		<?php if ($UserType == "" || $UserType == "Admin") { ?>
            <li><a attribid="28" href="/clients/ProjectManagementReports.php?v=<?php echo $_GET["v"]?>">Project Management Reports</a></li>
            <li><a attribid="29" href="/clients/Metrics_Reports.php?v=<?php echo $_GET["v"]?>">FS-Metrics Reports</a></li>
            <li><a attribid="28" href="/clients/FinancialReports.php?v=<?php echo $_GET["v"]?>">Financial Reports</a></li>
            <li><a attribid="30" href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=UnassignedWorkOrders.prpt&ignoreDefaultDates=true&autoSubmit=false')">Unassigned Work Orders</a></li>
            <!--<li><a attribid="31" href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=Technician_SLA_Compliance.prpt&ignoreDefaultDates=true&ignoreDefaultDates=true&autoSubmit=false')">Technician SLA Compliance</a></li>-->
            <!--<li><a attribid="46" href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=TechSiteSatisfaction.prpt&ignoreDefaultDates=true&autoSubmit=false')">Tech Site Satisfaction Survey List</a></li>-->
            <!--<li><a attribid="32" href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=Tech_Review_and_Confirm.prpt&ignoreDefaultDates=true&autoSubmit=false')">Tech Review and Confirm</a></li>-->
            <li><a attribid="28" href="/clients/TechnicianReports.php?v=<?php echo $_GET["v"]?>">Tech Analysis & Reports</a></li>
			<?php //if ($isPM):?>
	            <!--<li><a attribid="33" href="/clients/techsDeniedEdit.php?v=<?php echo $_GET["v"]?>">Client Denied Techs</a></li>-->
			<?php //endif;?>
            <!--<li><a attribid="34" href="/clients/techsClientPreferredEdit.php?v=<?php echo $_GET["v"]?>">Client Preferred Techs</a></li>-->
            <!--<li><a attribid="35" href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=Tech_Called_Report.prpt&ignoreDefaultDates=true&autoSubmit=false')">Tech Called Report</a></li>-->
            <!--<li><a attribid="36" href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=OutOfScope.prpt&ignoreDefaultDates=true&autoSubmit=false')">Out of Scope Report</a></li>-->
            <!--<li><a attribid="37" href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=Tech_WO_Address_Report.prpt&amp;ignoreDefaultDates=true&amp;autoSubmit=false')">Tech WO Address Report</a></li>-->
		<?php if ($_GET["v"] == "MPDP"):?>
            <li><a href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=MPD_SLA.prpt')">SLA Report</a></li>
		<?php endif;?>
		<?php } else if ($UserType == "Manager") {?>
          		<li><a href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=FAI_Financial%20Metrics%20Report.prpt&amp;ignoreDefaultDates=true&amp;autoSubmit=false')">Financials</a></li>
		        <li><a href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=Project_Status_and_Deliverables_v4.prpt&ignoreDefaultDates=true&autoSubmit=false')">Project Status with Deliverables</a></li>
		<?php } ?>
    <?php if ( $_GET["v"] == "FLS" && $UserType != "Manager" && $UserType != "Install Desk"):?>
            <li><a href="/clients/FTXS_Reports.php?v=<?php echo $_GET["v"]?>">FAI Reports</a></li>
    <?php endif;?>
    	</ul>
 </li>


<?php } ?>
<?php } ?>
 <style>
     .backgroundNoticeNumBer{
         background-color: red;
         -moz-border-radius: 4px; /* Firefox */
         -webkit-border-radius: 4px; /* Safari and Chrome */
         border-radius: 4px; /* Opera 10.5+, future browsers, and now also Internet Explorer 6+ using IE-CSS3 */
          padding-bottom: 1px;
          padding-left: 1px;
         padding-right: 1px;
          text-align: center;
        vertical-align: middle;
         /*behavior: url(/widgets/css/ie-css3.htc);*/
         float: left;
     }
     .backgroundNoticeNumBerOne{
         width: 11px;
     }
     .backgroundNoticeNumBerTwo{
         width: 17px;
     }
     .backgroundNoticeNumBer99{
         width: 24px;
     }
 </style>
<?php if($loggedIn=="yes" && $loggedInAs=="client"): ?>
<?php 
    $core = new Core_Api_NoticeClass();
    $countNotices = $core->getCountNotices_NotViewYet($_SESSION['ClientID']);
    $htmlNumberBotice = "";
    if(empty($countNotices)){
        $htmlNumberBotice = "";
    }else if($countNotices < 10){
        $htmlNumberBotice = "<div class='backgroundNoticeNumBer backgroundNoticeNumBerOne'>$countNotices</div>";
   }else if($countNotices < 100){
        $htmlNumberBotice = "<div class='backgroundNoticeNumBer backgroundNoticeNumBerTwo'>$countNotices</div>";
   }else{
       $htmlNumberBotice = "<div class='backgroundNoticeNumBer backgroundNoticeNumBer99'>99+</div>";
   }
 ?>
<li style="float: right;position: static;color: #000000;height: 24px;" >
    <a  href="javascript:;" id="mainLinkNotices" class=""><div style="float: left;">Inbox&nbsp;</div><?= $htmlNumberBotice ?> </a>
    <div style="clear: both;"></div>
    <div id="MyNoticesMenu" style=""></div>
</li>
<?php endif; ?>

<?php if($loggedIn=="yes" && $loggedInAs=="staff"){ ?>
<?php if ( $page == "staff" ) {?>
<!-- STAFF NAVBAR TOP -->
<?php if ( $option == "profile" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/staff/profile.php">My Profile</a></li>
<?php if ( $option == "staff" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/staff/staff.php">Staff</a></li>
<?php if ( $option == "projects" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/staff/projects.php">Manage Projects</a></li>
<?php if ( $option == "cibs" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/staff/cibs.php">CIBs</a></li>
<?php if ( $option == "techs" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/staff/techView.php">Techs</a></li>
<!--?php if ( $option == "help" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="https://<!-?php echo $siteTemplate;?>.fieldsolutions.com/content/contactUs.php">Help</a></li>
-->
<!--<?php if ( $option == "wosFLS" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/staff/wosFLS.php">FLS Work Orders</a></li>-->
<?php } ?>
<?php } ?>


<?php if($loggedIn=="yes" && $loggedInAs=="admin"){ ?>
<?php if ( $page == "admin" || $page == "accounting" ) {?>
<!-- ADMIN NAVBAR TOP -->
<!--841-->
<?php if ( $option == "techs" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/advancedTechSearch.php?f=1">Find Techs</a></li>
<!--end 841-->
<?php if ( $option == "client" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/adminClientList.php">Client</a></li>
<?php if ( $option == "ISO" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/ISO_List.php">ISO</a></li>
<?php if ( $option == "adminWorkorder" || $option == "wos" ){echo '<li class="selected">';}else{echo '<li>';}?>
        <a href="/admin/wos.php?loaddasboard=1">Work Orders</a></li><!--622-->
<?php if ( $option == "reports" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/adminReports.php">Reports</a></li>
<?php if ( $option == "pricing" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/adminPricing.php">Pricing</a></li>

<!--<?php if ( $option == "fsplusone" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/fsplusone.php">FSPlusOne</a></li>-->
<?php } ?>
<?php } ?>

<?php if ( $page == "aboutus" ) {?>
<!-- ABOUT US NAVBAR TOP -->
<?php if ( $option == "aboutus" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/wp/our-services/">Our Business</a></li>
<?php if ( $option == "missionvision" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/wp/mission-values/">Mission & Values</a></li>
<?php if ( $option == "ourClients" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/wp/our-clients/">Our Clients</a></li>
<?php if ( $option == "ourMembers" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/wp/our-members/">Our Members</a></li>
<?php if ( $option == "thedifference" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/wp/fieldsolutions-difference/">FieldSolutions Difference</a></li>
<?php if ( $option == "management" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/wp/management-team/">Management Team</a></li>
<?php if ( $option == "newsAndEvents" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/wp/news-and-events/">News and Events</a></li>
<?php if ( $option == "aboutContactUs" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/wp/contact-us/">Contact Us</a></li>
<?php } ?>


<?php if ( $page == "sales" ) {?>
<!-- SALES NAVBAR TOP -->
<li class="selected"><a href="<?php echo $siteUrl;?>/sales/dashboard.php">Sales Dashboard</a></li>
<?php } ?>

<?php if ( ($page == "admin" || $page == "accounting") && $_SESSION['isAccounting'] == "yes" ) {?>
<!-- SALES NAVBAR TOP -->
<?php if ( $option == "home" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/accounting/index.php">Accounting</a></li>
<?php /* if ( $option == "fsplusone" && $_SESSION['isFsplusone'] == "yes"){echo '<li class="selected">';}else{echo '<li>';} */?><!-- a href="/accounting/fsplusone.php">FSPlusOne</a></li --> 
<?php } ?>

<?php if ( ($page == "admin" || $page == "accounting") && ( $_SESSION['isFsplusone'] == "yes")) {?>
<!-- SALES NAVBAR TOP -->
<?php /* if ( $option == "home" ){echo '<li class="selected">';}else{echo '<li>';} */ ?><!-- a href="/accounting/index.php">Accounting</a></li -->
<?php if ( $option == "fsplusone" && $_SESSION['isFsplusone'] == "yes"){echo '<li class="selected">';}else{echo '<li>';}?><a href="/accounting/fsplusone.php">FSPlusOne</a></li>
<?php } ?>
</ul>

<?php $loggedIn = ( !empty($_SESSION['loggedIn']) ) ? $_SESSION['loggedIn'] : NULL; ?>

<div id="tabcontent" class="clearfix">



<?php if ($page == "techs" && $option == "certifications") {?>
<ul class="selected">
<?php if ($selected == "technical" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/certifications.php">Technical Certifications</a></li>
<?php if ($selected == "FSCertification" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/Training/FieldSolutions/FS_Certification.php">Field Solutions Certification</a></li>
</ul>
<?php } ?>

<?php if ($page == "login"){?>
<!--Login -->
<!--
<?php if ( $page == "login" ){echo '<ul class="selected">';}else{echo '<ul>';}?>
<li><a href="<?php echo $siteUrl;?>/techs/index.php">Tech</a></li>
<li><a href="<?php echo $siteUrl;?>/clients/index.php">Client</a></li>
<li><a href="<?php echo $siteUrl;?>/staff/index.php">Staff</a></li>
<li><a href="<?php echo $siteUrl;?>/admin/index.php">Admin</a></li>
</ul>
-->
<?php } ?>



<?php if ($page == "techs"){?>


<?php if ($option=="wos"){?>
<!--TECH 2nd NAVBAR -->


<ul class="selected">
<?php if ($selected == "wosAssigned" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="wosAssigned.php?v=<?php echo $_GET["v"]; ?>">Assigned Work</a></li>
<?php if ($selected == "wosAvailable" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="wosAvailable.php?v=<?php echo $_GET["v"]; ?>">Available Work</a></li>
<?php if ($selected == "wosViewApplied" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="wosViewAppliedAll.php?v=<?php echo $_GET["v"]; ?>">My Applied WOs</a></li>
<!--<li><a href="wosFLSAssigned.php">FLS Work Orders</a></li>-->
<!--<li><nobr><a id="dbTypeLink" href="wosFLSAssigned.php" style="display: none">FLS WOs</a></nobr></li>-->
</ul>



<!--<li><nobr><a href="javascript:showNextWeek()">Next Week's WOs</a> (<?=$woNextWeek?>)</nobr></li>-->
<!--<li><nobr><a href="javascript:showNextWeek()">Next Week's WOs</a></nobr></li>-->
<?php } ?>

<?php if ($option=="equipment"){?>
<!--TECH 2nd NAVBAR equipment -->
<ul class="selected">
<?php if ($selected == "equipMisc" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="equipment.php">Basic</a></li>
<?php if ($selected == "equipCabling" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="cablingSkills.php?>">Cabling &amp; CCTV</a></li>
<?php if ($selected == "equipTelephony" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="telephonySkills.php?>">Telephony</a></li>
</ul>

<!--<li><nobr><a href="javascript:showNextWeek()">Next Week's WOs</a> (<?=$woNextWeek?>)</nobr></li>-->
<!--<li><nobr><a href="javascript:showNextWeek()">Next Week's WOs</a></nobr></li>-->
<?php } ?>

<?php if ($option=="wosFLS"){?>
<!--TECH 2nd NAVBAR FLS WORKORDERS-->
<ul class="selected">
<?php if ($selected == "wosFLSAssigned" ){echo '<li class="selected">';}else{echo '<li>';}?><!--<a href="wosFLSAssigned.php?v=<?php echo $_GET["v"]; ?>">Assigned Work</a></li>-->
<?php if ($selected == "wosFLSAvailable" ){echo '<li class="selected">';}else{echo '<li>';}?><!--<a href="wosFLSAvailable.php?v=<?php echo $_GET["v"]; ?>">Available Work</a></li>-->
<?php if ($selected == "wosViewApplied" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="wosViewAppliedAll.php">My Applied WOs</a></li>
<li><a href="wosAssigned.php">FieldSolutions WOs</a></li>
<!--<li><nobr><a id="dbTypeLink" href="wosAssigned.php" style="display: none">Field Solutions</a></nobr></li>-->
</ul>

<!--<li><nobr><a href="javascript:showNextWeek()">Next Week's WOs</a> (<?=$woNextWeek?>)</nobr></li>-->
<!--<li><nobr><a href="javascript:showNextWeek()">Next Week's WOs</a></nobr></li>-->
<?php } ?>

<!-- TECHS - TRAINING -->

<?php if ($option == "training"){?>
<ul class="selected">
 <!--  <?php if ( $selected == "support" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/content/contactUs.php">Technician Support</a></li>-->

 <?php if ( $selected == "support" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/FAQs/index.php">FAQ / Support</a></li>

    <?php if ( $selected == "fs" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/Training/FS_Training.php">Field Solutions How-To's</a></li>
    <?php if ( $selected == "FSTraining" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/Training/FieldSolutions/FieldSolutionsTraining.php">Field Solutions Online Tutorial</a></li>
    <?php if ( $selected == "fls" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="http://www.flsupport.com/2.html" target="_blank">FLS How-To's <span class="offsite">(Off-Site)</a></a></li>
</ul>

<?php } ?>

<!-- TECHS - MORE -->
<?php if ($option == "backgroundChecks"){?>
<ul class="selected">
	<?php if ($display == "yes"){?>
        <?php if ( $selected == "backgroundChecks" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="backgroundChecks.php">Instructions</a></li>
        <?php if ( $selected == "backgroundPolicy" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="backgroundPolicy.php">Policy</a></li>
        <?php if ( $selected == "backgroundFAQ" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="background_FAQ.php">FAQ</a></li>
    <?php } ?>
</ul>

<?php } ?>
<?php } ?>



<?php if ($page == "clients"){?>
<!--CLIENTS 2nd NAVBAR -->

<?php if ($option == 'dashboard') {
        showHeaderForNavBar();
        include 'buttons.php';
 } ?>

<?php if ($option == "techs"){
    showHeaderForNavBar();
    ?>
<div id="subSectionLabel" style="" class="clearfix">
<!-- TECHS -->
<ul class="selected">
<?php if (isset($selected) && $selected == "find" ):?>
<li><a href="/clients/techs.php">Find All Techs</a></li>
<?php elseif (isset($selected) && $selected == "findCabling" ):?>
<li><a href="/clients/techs_cabling.php?v=<?=$_GET["v"]?>&newSearch=1">Find Cabling &amp; CCTV Techs</a></li>
<?php elseif (isset($selected) && $selected == "findTelephony" ):?>
<li><a href="/clients/techs_telephony.php?v=<?=$_GET["v"]?>&newSearch=1">Find Telephony Techs</a></li>
<?php elseif (isset($selected) && $selected == "recruitmentemail" ):?>
<li><a href="/clients/recruitmentEmail.php?v=<?=$_GET["v"]?>">Send Recruitment Email</a></li>
<?php elseif (isset($selected) && $selected == "flsidupload"):?>
<li><a href="/clients/FLS/importFLSBadge.php?v=<?=$_GET["v"]?>">FLS ID Upload</a></li>
<?php endif;?>
<li>
	<?php  include "buttons.php"; ?>
</li>


</ul>
</div>
<?php } ?>

<?php if ($option == "wos"){?>
<!-- WORKORDERS -->
<div id="subSectionLabel" style="margin-left:1px;" class="clearfix">
<ul class="selected">
<?php if (isset($selected) && $selected == "createWO" ):?>
<li><a attribid="2" class="AttentionLink" href="createWO.php?v=<?php echo $_GET["v"]?>">Create a Work Order</a></li>
<li>
	<?php  
	    showHeaderForNavBar();
	    include "buttons.php"; 
	?>
</li>
<?php elseif (isset($selected) && $selected == "wosView" ):?>
<li><a href="wosView.php?v=<?php echo $_GET["v"]?>">Find a Work Order</a></li>
<li>
	<?php  
	    showHeaderForNavBar();
	    include "buttons.php"; 
	?>
</li>
<?php elseif (isset($selected) && $selected == "recruitmentemail" ):?>
<li><a href="/clients/recruitmentEmail.php?v=<?=$_GET["v"]?>">Send Recruitment Email</a></li>
<li>
	<?php  
	    showHeaderForNavBar();
	    include "buttons.php"; 
	?>
</li>
<?php elseif (isset($selected) && $selected == "projectBlast" ):?>
<li><a href="/clients/projectBlast.php?v=<?=$_GET["v"]?>">Send Project Email</a></li>
<li>
	<?php  
	    showHeaderForNavBar();
	    include "buttons.php"; 
	?>
</li>
<?php elseif (isset($selected) && $selected == "importWO" ):?>
<li><a href="importWO.php?v=<?php echo $_GET["v"]?>">Upload Work Orders</a></li>
<li>
	<?php  
	    showHeaderForNavBar();
	    include "buttons.php"; 
	?>
</li>
<?php elseif (isset($selected) && $selected == "FindWIN" ):?>
<li><a href="GPM_FindWO.php?v=<?= $_GET['v'] ?>">Find WIN#</a></li>
<li>
	<?php  
	    showHeaderForNavBar();
	    include "buttons.php"; 
	?>
</li>

<?php elseif (isset($selected) && $selected == "paperworkReceived" ):?>
<li><a href="paperworkReceived.php">Paperwork Received</a></li>
<?php else:?>
<li><a href="wos.php?v=<?php echo $_GET["v"]?>">My Dashboard</a></li>
<li>
	<?php  include "buttons.php"; ?>
</li>
<?php endif;?></ul>
				</div>
<?php } ?>

<?php 
// THIS Block for include widgets to pages other WO
if ( !in_array($option, array("wos", "techs", "dashboard")) && !empty($toolButtons) ){
    
    showHeaderForNavBar();
    ?>

    
    <div id="subSectionLabel" style="margin-left:1px;" class="clearfix">
        <ul class="selected">
        <li>
        	<?php  include "buttons.php"; ?>
        </li>
        </ul>
	</div>
<?php 
}
?>

<?php if ($option == "reports"){?>
<div style="margin-top:1px;" class="clearfix">

<!-- REPORTS -->
<ul class="selected">
<?php if (isset($selected) && $selected == "confirmReport" ):?>
<li><a href="confirmReport.php?v=<?php echo $_GET["v"]?>">Tech Review and Confirm</a></li>
<?php elseif (isset($selected) && $selected == "techsDeniedEdit" ):?>
<li><a href="techsDeniedEdit.php?v=<?php echo $_GET["v"]?>">Client Denied Techs</a></li>
<?php elseif (isset($selected) && $selected == "techsPreferredEdit" ):?>
<li><a href="techsClientPreferredEdit.php?v=<?php echo $_GET["v"]?>">Client Preferred Techs</a></li>
<?php elseif (isset($selected) && $selected == "techCalled" ):?>
<li><a href="techCalled.php?v=<?php echo $_GET["v"]?>">Tech Called Report</a></li>
<?php elseif (isset($selected) && $selected == "wosView_OOS" ):?>
<li><a href="wosView_OOS.php?v=<?php echo $_GET["v"]?>">Out of Scope Report</a></li>
<?php elseif (isset($selected) && $selected == "wosUnassigned" ):?>
<li><a href="wosUnassigned.php?v=<?php echo $_GET["v"]?>">Unassigned Work Orders</a></li>
<?php elseif (isset($selected) && $selected == "techAddressReport" ):?>
<li><a href="tech_Address_Report.php?v=<?php echo $_GET["v"]?>">Tech WO Address Report</a></li>
<?php elseif (isset($selected) && $selected == "projectTimesReport" ):?>
<li><a href="projectTimes.php?v=<?php echo $_GET["v"]?>">Project Status with Deliverables</a></li>
<?php elseif (isset($selected) && $selected == "FTXSReport" ):?>
<li><a href="projectTimes.php?v=<?php echo $_GET["v"]?>">FTXS Reports</a></li>
<?php endif;?>
</ul>
</div>
<?php } ?>

<?php if ($option == "projects"){?>
<!-- PROJECTS -->
<div id="subSectionLabel">
<ul class="selected">
<?php if ($selected == "projectsView" ):?>
<li><a href="projectsView.php?v=<?php echo $_GET["v"]?>">View / Edit Projects</a></li>
<?php elseif ( $selected == "projectsCreate" ):?>
<li><a href="projectsCreate_new.php?v=<?php echo $_GET["v"]?>">Add Project</a></li>
<?php endif;?>
</ul>
<?php  include_once 'buttons.php'; ?>
</div>
<?php } ?>

<?php if ($option == "users"){?>
<!-- USERS -->
<div id="subSectionLabel">
<ul class="selected">
<?php if ($selected == "usersView" ):?>
<li><a href="usersView.php?v=<?php echo $_GET["v"]?>">View / Edit Users</a></li>
<?php elseif ( $selected == "usersCreate" ):?>
<li><a href="usersCreate.php?v=<?php echo $_GET["v"]?>">Add User</a></li>
<?php endif;?>
</ul>
</div>
<?php } ?>

<?php if ($option == "customers"){?>
<!-- CUSTOMERS -->
<ul class="selected">
<?php if ($selected == "customersView" ):?>
<li><a href="customersView.php?v=<?php echo $_GET["v"]?>">View / Edit Customers</a></li>
<?php elseif ( $selected == "customersCreate" ):?>
<li><a href="customersCreate.php?v=<?php echo $_GET["v"]?>">Add Customer</a></li>
<?php endif;?>
</ul>
<?php } ?>

<!-- END OF CLIENTS -->
<?php } ?>



<?php if ($page == "staff"){?>
<!--Staff 2nd bar-->
<?php if ( $page == "staff" ){echo '<ul class="selected">';}else{echo '<ul>';}?>
<!--
<?php if ( $option == "profile" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/staff/profile.php">My Profile</a></li>
-->
</ul>
<?php } ?>

<?
if ($loggedIn == "yes" && $loggedInAs == "admin")
{
    //622
    if ($option == "dashboard")
    {
        ?>
            <ul class="selected">
                <Script language="JavaScript">
<!--
function openPopupWindowdasboad(w,h)
{
    w = (typeof(w) == 'unansigned') ? 740 : w;
    h = (typeof(h) == 'unansigned') ? 500 : h;
    newwindow=window.open("wosSearchPay.php",'name','height='+h+', width='+w+', left=100, top=100, resizable=yes, scrollbars=yes, toolbar=no,status=no');
    w=0;h=0;
    if (newwindow && newwindow.focus)
    {
        newwindow.focus()
    }
}
-->
</script>


                <li <?= $selected == "advanced" ? "class='selected'" : "" ?>>
                    <a href="/admin/advancedTechSearch.php">Find Techs</a>
                </li>
                <li <?= $selected == "addClient" ? "class='selected'" : "" ?>>
                    <a href="adminAddClient.php">Add Client</a>
                </li>
                <li <?= $selected == "clientprofileedit" ? "class='selected'" : "" ?>>
                    <a href="client_choose_profile.php?type=Client">Client Profile</a>
                </li>
                <li <?=$selected == "SearchPayWorkOrders"?"class='selected'":""?>>
                    <a href="javascript:;" onclick="openPopupWindowdasboad(900, 700);">Search/Pay Work Orders</a>
                </li>
                <li <?= $selected == "clientMasterList" ? "class='selected'" : "" ?>>
                    <a href="clientMasterList.php">Client Master List</a>
                </li>
            </ul>
        <?
    }
    //end 622
?>
<?php if ($page == "admin" || $page == "accounting"){?>
<!--ADMIN 2nd NAVBAR-->

<?php if ($option == "techs"){?>
<ul class="selected">
<!--841-->
<?php if ( $selected == "advanced" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/advancedTechSearch.php?f=1">Find Techs</a></li>
<!--end 841-->
<?php if ( $selected == "Admin Denied" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/techsAdminDenied.php">Admin / Client Denied</a></li>
<?php if ( $selected == "loginhistory" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="javascript:void(0)" onclick="window.open('/reports/reportviewer/report.html?name=TechLoginHistory.prpt&ignoreDefaultDates=true&autoSubmit=false', 'Tech Login History', 'width=650, height=600');">Tech Login History</a></li>
<?php if ( $selected == "copyPrefTechs" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/copyPrefTechs.php?type=Tech">Copy Pref. Techs</a></li>
<?php if ( $selected == "phoneAuditLog" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/phone_audit_log.php?type=Tech">Phone Audit Log</a></li>
<?php if ( $selected == "electroMechTest" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="javascript:void(0)" onclick="window.open('/reports/reportviewer/report.html?name=ServRightElectroMech.prpt&autoSubmit=false', 'Electro-Mechanical Skills', 'width=650, height=600');">Electro-Mechanical Skills</a></li>
<?php if ( $selected == "photoAudit" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/photo_audit.php" >Picture Audit</a></li>
</ul>
<?php } ?>

<?php if ($option == "ISO"){?>
<ul class="selected">
<?php if ( $selected == "ISOList" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="ISO_List.php">ISO List</a></li>
<?php if ( $selected == "addISO" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="ISO_Add.php">Add ISO</a></li>
<?php if ( $selected == "addISO_Tech" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="ISO_Add_Tech.php">Add ISO Tech</a></li>
</ul>
<?php } ?>

<?php if ($option == "client"){?>
<ul class="selected">
<?php if ( $selected == "clientlist" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="adminClientList.php">Client List</a></li>
<?php if ( $selected == "addClient" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="adminAddClient.php">Add Client</a></li>
<?php if ( $selected == "loginhistory" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="javascript:void(0);" onclick="window.open('/reports/reportviewer/report.html?name=ClientLoginHistory.prpt&ignoreDefaultDates=true&autoSubmit=false', 'Client Login History', 'width=650, height=600');">Client Login History</a></li>
<?php if ( $selected == "projectList" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="adminClientProjectList.php?type=Client">Client Project List</a></li>
<?php if ( $selected == "customerList" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="adminClientCustomerList.php?type=Client">Client Customer List</a></li>
<?php if ( $selected == "clientprofile" || 
        $selected == 'clientprofileedit' ){echo '<li class="selected">';}else{echo '<li>';}?><a href="client_choose_profile.php?type=Client">Client Profile</a></li>
<li <?= $selected == "clientMasterList" ? "class='selected'" : "" ?>>
                    <a href="clientMasterList.php">Client Master List</a>
</li>
<li <?= $selected == "clientActivityLog" ? "class='selected'" : "" ?>>
                    <a href="clientActivityLog.php">Activity Log</a>
</li>

<?php if ( $selected == "fstags" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="fstags.php?type=Client">Create FS-Tag</a></li>
<?php if ( $selected == "fstagsList" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="fstagsList.php?type=Client">FS-Tags List</a></li>
</ul>
<?php } ?>

<?
//622
if ($option == "adminWorkorder" || $option == "wos" )
{
?>
<ul class="selected">
<!-- REPORTS -->
<Script language="JavaScript">
<!--
function openPopupWindow(w,h)
{
    w = (typeof(w) == 'unansigned') ? 740 : w;
    h = (typeof(h) == 'unansigned') ? 500 : h;
    newwindow=window.open("wosSearchPay.php",'name','height='+h+', width='+w+', left=100, top=100, resizable=yes, scrollbars=yes, toolbar=no,status=no');
    w=0;h=0;
    if (newwindow && newwindow.focus)
    {
        newwindow.focus()
}
}
-->
</script>
    <li <?=$selected == "SearchPayWorkOrders"?"class='selected'":""?>>
        <a href="javascript:;" onclick="openPopupWindow(900, 700);">Search/Pay Work Orders</a>
    </li>
    <li <?=$option == "wos" ?"class='selected'":""?>>
        <a href="/admin/wos.php">Load Dashboard</a>
    </li>
</ul>
<?php } ?>

<?php if ($option == "admin"){?>
<ul class="selected">
<?php if ( $selected == "adminUpdate" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="adminUpdate.php">View / Update Addmins</a></li>
<?php if ( $selected == "addAdmin" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="adminAdminAdd.php">Add Admin</a></li>
</ul>
<?php } ?>

<?php if ($option == "staff"){?>
<ul class="selected">
<?php if ( $selected == "staffUpdate" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="adminStaffUpdate.php">View / Update Staff</a></li>
<?php if ( $selected == "addstaff" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="adminStaffAdd.php">Add Staff</a></li>
</ul>
<?php } ?>

<?php if ($option == "reports"){?>
<ul class="selected">
<!--?php if ( $selected == "financialsFLS" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="financials_FLS.php">FLS Financials</a></li-->
<!--?php if ( $selected == "financialsMain" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="financials_MainSite.php">Main Site Financials</a></li-->
<?php if ( $selected == "SMTPEmailLog" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="SMTPEmailLog.php">Email Log</a></li>
<?php if ( $selected == "SMTPBlockedEmail" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="SMTPBlockedEmail.php">Blocked Email Addresses</a></li>
<!--?php if ( $selected == "addStaff" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="AddStaff.php">Add Staff Profile</a></li-->
<?php if ( $selected == "wosSummary" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="wosSummary.php">Work Order Summary</a></li>
<!--?php if ( $selected == "unassigned" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="unassigned.php">Unassigned Work Orders</a></li-->
<!--?php if ( $selected == "deactivated" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="deactivatedWorkOrders.php">Deactivated Work Orders</a></li-->
<?php if ( $selected == "preferredReport" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="preferredTechsReport.php">Preferred Techs Report</a></li>
<?php if ( $selected == "devryReport" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=DevryBiddersReport.prpt&ignoreDefaultDates=true&autoSubmit=false')">DeVry Bidders Report</a></li>
<!--?php if ( $selected == "filledReport" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="fillRateReport.php">Fill Rate Report</a></li-->
<?php if ( $selected == "acsWOReport" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=WOASCChangeLog.prpt&ignoreDefaultDates=true&autoSubmit=false')">WO ACS Change Log</a></li>
<?php if ( $selected == "acsProjectReport" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=ProjectASCChangeLog.prpt&ignoreDefaultDates=true&autoSubmit=false')">P2T / ACS Projects Report</a></li>
<!--?php if ( $selected == "noCellPhoneReport" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="noCellPhoneReport.php">Techs No Cell Phone Report</a></li-->
<!--?php if ( $selected == "techTimeReport" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="techTimeReport.php">Tech Times</a></li-->
<!--?php if ( $selected == "acsServicesDeliveredReport" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="acsServicesDeliveredReport.php">ACS Services Delivered</a></li-->
<?php if ( $selected == "acsProjectReport" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=FS-PlusOnePointsReport.prpt&ignoreDefaultDates=true&autoSubmit=false')">Points Report</a></li>
<?php if ( $selected == "FTEReport" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="javascript:openPopup('http://<?=$_SERVER["SERVER_NAME"]?>/reports/reportviewer/report.html?name=FTE%20Report.prpt&autoSubmit=false')">FTE Report</a></li>
<?php if ( $selected == "PasswordUpadateReportReport" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="UpdatePasswordList.php">Password Update Report</a></li>
<!--?php if ( $selected == "IVR_Log" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="IVR_Log.php">IVR Log</a></li-->
</ul>
<?php } ?>

<?php if ($option == "pricing"){?>
<ul class="selected">
<?php if ( $selected == "addPricingField" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="addPricingField.php">Add Field</a></li>
<?php if ( $selected == "editPricingField" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="editPricingField.php">View / Edit Fields</a></li>
<?php if ( $selected == "editPricingRule" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="viewEditPricingRules.php">View / Edit Rules</a></li>
<?php if ( $selected == "assignRuleClient" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="adminClientList.php">Assign Default Rule to Client</a></li>
<?php if ( $selected == "assignRuleProject" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="adminClientProjectList.php?type=Client">Assign Rule to Project</a></li>
<?php if ( $selected == "assignRuleProject" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="wos.php">Assign Rule to WO</a></li>
<?php if ( $selected == "pricingRulesAssignedReport" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="pricingRulesAssigned.php">Pricing Rules Assigned Report</a></li></ul>
<?php } ?>

<!--<?php if ($option == "fsplusone" && $_SESSION['isFsplusone'] == "yes"){?>
<ul class="selected">
<?php if ( $selected == "addPoints" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="fsplusone.php">Add FSPlusOne Points</a></li>
<?php if ( $selected == "invoicePoints" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="FSPlusOneMarkApplied.php">Invoice FSPlusOne Points</a></li>
<?php } ?>-->

<?php } ?>
<?php } ?>


<?php if ($page == "staff"){?>
<?php if ($option == "techs"){?>
<ul class="selected">
<?php if ( $selected == "techview" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="techView.php">View Technicians</a></li>
<?php if ( $selected == "techviewratings" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="techViewRatings.php">View Technician Ratings</a></li>
<?php if ( $selected == "techaddratings" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="techAddRatings.php">Add Technician Ratings</a></li>
</ul>
<?php } ?>
<?php } ?>


<?php if ($page == "accounting"){ ?>
<?php /*if($_SESSION['isAccounting'] == "yes"){ */?>
<?php if ($option == "accounting"){?>
<ul class="selected">
<?php if ( $selected == "Billing" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="index.php">Billing</a></li>
<?php if ( $selected == "Banking" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="banking.php">Banking</a></li>
</ul>
<?php } ?>

<?php if ($option == "fsplusone" && $_SESSION['isFsplusone'] == "yes"){?>
<ul class="selected">
<?php if ( $selected == "addPoints" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="fsplusone.php">Add FSPlusOne Points</a></li>
<?php if ( $selected == "invoicePoints" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="FSPlusOneMarkApplied.php">Invoice FSPlusOne Points</a></li>
<?php } ?>

<?php } ?>

<?php if ($page == "sales"){?>
<!--Sales -->
<ul class="selected">

<?php if ( $option == "techs" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/sales/salesTechs.php">Techs</a></li>
<?php if ( $option == "clients" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="<?php echo $siteUrl;?>/sales/salesClients.php">Client</a></li>
<?php if ( $option == "wos" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/sales/salesWos.php">Work Orders</a></li>
</ul>
<?php } ?>

</div>


<?php 
function showHeaderForNavBar()
{
    static $isShowed = false;
    
    if ($isShowed)    return;
    $isShowed = true;
    
?>
<script src="/widgets/js/jquery.form.js"></script>
<!--script src="/widgets/js/FSWidget.js"></script-->
<?php script_nocache ("/widgets/js/FSWidget.js"); ?>
<!--script src="/widgets/js/FSWidgetDashboard.js?13012011"></script-->
<?php script_nocache ("/widgets/js/FSWidgetDashboard.js"); ?>
<!--script type="text/javascript" src="/widgets/js/FSWidgetSortTool.js"></script-->
<?php script_nocache ("/widgets/js/FSWidgetSortTool.js"); ?>
<?php if(strtolower($Company_ID)=='fls'):?>
<!--script src="/widgets/js/FSWidgetSortToolFLS.js"></script-->
<?php script_nocache ("/widgets/js/FSWidgetSortToolFLS.js"); ?>
<?php endif; ?>
<!--script src="/widgets/js/FSWidgetDeactivationTool.js"></script-->
<?php script_nocache ("/widgets/js/FSWidgetDeactivationTool.js"); ?>
<!--script src="/widgets/js/FSWidgetActivityLog.js"></script-->
<?php script_nocache ("/widgets/js/FSWidgetActivityLog.js"); ?>
<!--script src="/widgets/js/FSQuickAssignTool.js?20110526"></script-->
<?php script_nocache ("/widgets/js/FSQuickAssignTool.js"); ?>
<!--script src="/widgets/js/FSPopupAbstract.js"></script-->
<?php script_nocache ("/widgets/js/FSPopupAbstract.js"); ?>
<script src="<?=empty($_SERVER['HTTPS']) ? 'http' : 'https'?>://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<link rel="stylesheet" href="<?=empty($_SERVER['HTTPS']) ? 'http' : 'https'?>://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/ui-lightness/jquery-ui.css" type="text/css" />
<!--script src="/widgets/js/livevalidation.js"></script-->
<?php script_nocache ("/widgets/js/livevalidation.js"); ?>
<script src="/widgets/js/datatables/js/jquery.dataTables.min.js"></script>
<!--script src="/widgets/js/FSPopupRoll.js"></script-->
<?php script_nocache ("/widgets/js/FSPopupRoll.js"); ?>
<!--script src="/widgets/js/ProgressBar.js"></script-->
<?php script_nocache ("/widgets/js/ProgressBar.js"); ?>
<!--script src="/widgets/js/ProgressBarAdapterAbstract.js"></script-->
<?php script_nocache ("/widgets/js/ProgressBarAdapterAbstract.js"); ?>
<!--script src="/widgets/js/ProgressBarAdapterPopupSpin.js"></script-->
<?php script_nocache ("/widgets/js/ProgressBarAdapterPopupSpin.js"); ?>

<!--script src="/widgets/js/functions.js"></script-->
<?php script_nocache ("/widgets/js/functions.js"); ?>
<!--script src="/widgets/js/FSWidgetManager.js"></script-->
<?php script_nocache ("/widgets/js/FSWidgetManager.js"); ?>
<!--script src="/widgets/js/FSWidgetWODetails.js?08182011"></script-->
<?php script_nocache ("/widgets/js/FSWidgetWODetails.js"); ?>
<!--script src="/widgets/js/FSFindWorkOrder.js?03172011"></script-->
<?php script_nocache ("/widgets/js/FSFindWorkOrder.js"); ?>


<script>
var wd;             //  Widgets object
var roll;           //  Rollover object
var popup;          //  Rollover object
var progressBar;    //  Progress bar object

$(document).ready(function() {
    wd = new FSWidgetDashboard();

    roll        = new FSPopupRoll();
    progressBar = new ProgressBar();

    progressBar.key('approve');
    progressBar.adapter(new ProgressBarAdapterPopupSpin(progressBar, null));

});
</script> 
<?php
}
$password = "";
$PasswordUpdateClass = new Core_Api_PasswordUpdateClass();
$hasPasswordUpdate = 0;
$userid = 0;
$username = "";
$lname = "";
$fname = "";
if($loggedIn == "yes"){
    if($_SESSION['loggedInAs'] == "client"){
        $userid = $_SESSION['ClientID'];
        $hasPasswordUpdate = $PasswordUpdateClass->MustChangePasswordorNot_Client($_SESSION['ClientID']);
        $Class = new Core_Api_Class();
        $clientinfo = $Class->getClientByID($_SESSION['ClientID']);
        $ContactName = $clientinfo['ContactName'];
        $arrayCN = explode(" ", $ContactName);
        $lname = $arrayCN[0];
        if(count($arrayCN) > 1){
            $fname = $arrayCN[count($arrayCN) - 1];
        }
        $username = $clientinfo['UserName'];
        $password = $clientinfo['Password'];
    }else if($_SESSION['loggedInAs'] == "admin"){
        $userid = $_SESSION['UserName'];
        $hasPasswordUpdate = $PasswordUpdateClass->MustChangePasswordorNot_AdminUserName($_SESSION['UserName']);
        $AdminUser = new Core_Api_AdminUser();
        $AdminInfo = $AdminUser->getAdminInfo($_SESSION['UserName']);
        $username = $_SESSION['UserName'];
        $lname = $AdminInfo['Fname'];
        $fname = $AdminInfo['Lname'];
        $password = $AdminInfo['Password'];
    }
    if(empty($hasPasswordUpdate)){
        $hasPasswordUpdate = 0;
    }
}
?>
<script>
    var _global = global.CreateObject({id:"_global",
        hasPasswordUpdate:<?= $hasPasswordUpdate ?>,
        loggedInAs:'<?= $_SESSION['loggedInAs'] ?>',
        PHP_SELF:'<?= $_SERVER['HTTP_REFERER'] ?>',
        userid:'<?= $userid ?>',
        password:'<?= $password ?>',
        username:'<?= $username ?>',
        lname:'<?= $lname ?>',
        fname:'<?= $fname ?>',
        company:'<?= empty($_REQUEST['v'])?$_SESSION['PM_Company_ID']:$_REQUEST['v'] ?>'
    });  
    jQuery(window).ready(function(){
        _global.callUpdatePassWordSecurity();
    });

</script> 


