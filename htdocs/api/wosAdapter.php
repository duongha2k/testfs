<?php
/**
 * Adapter to let API using library/caspioAPI.php
 */

session_start();
$_SESSION['loggedIn'] != 'yes';
$argc = true;
$callType = $_REQUEST['callType'];

switch ($callType)
{
    case 'create':
        include('../clients/wosEmailTechWhenAssigned_P2T.php');
        break;
        
    case 'update':
        include('../clients/wosEmailTechWhenAssigned_P2T.php');
        break;
        
    case 'appoved':
        include('../clients/wosEmailTechWhenApproved.php');
        break;
        
    case 'tech_complete':
        include('../clients/wosEmailTechWhenCompleteUnchecked.php');
        break;
}
