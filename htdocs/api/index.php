<?php
require_once('../../includes/modules/web.init.php');
require_once('../../includes/modules/tables.php');

//ini_set('error_reporting', E_ALL);
error_reporting(0);
ini_set('display_errors', 0);

require_once(DOCUMENT_INCLUDES . '/classes/Smarty/Smarty.class.php');
$smarty = new Smarty();
$smarty->debugging = false;
$smarty->force_compile = true;
$smarty->caching = false;
$smarty->compile_check = true;
$smarty->cache_lifetime = -1;
$smarty->template_dir = DOCUMENT_INCLUDES .'/templates';
$smarty->compile_dir = DOCUMENT_INCLUDES .'/templates_c';
$smarty->plugins_dir = array(
                             SMARTY_DIR . 'plugins',
                             DOCUMENT_INCLUDES . 'plugins');

Zend_Registry::set('smarty', $smarty);


$frontController = Zend_Controller_Front::getInstance();
$frontController->throwExceptions(false);

$routes = array(
    'rest' => new Zend_Controller_Router_Route(
        'client/rest/:action/:winNum/*',
        array(
            'controller'    =>  'rest',
            'action'        =>  'index'
        )),
    'rest_tech_up_wos'  => new Zend_Controller_Router_Route(
        'client/rest/techupwo/*',
        array(
            'controller'    => 'rest',
            'action'        => 'techupwo'
        )),
		'rest_clientaccess' => new Zend_Controller_Router_Route(
        'client/rest/client-access/*',
            array('controller'=>'rest',
                'action' => 'client-access')),
		'rest_tech_wo'  => new Zend_Controller_Router_Route(
        'client/rest/techwos/*',
        array(
            'controller'    => 'rest',
            'action'        => 'techwos'
        )),
	'rest_wovisit' => new Zend_Controller_Router_Route(
        'client/rest/wosvisit/*',
            array('controller'=>'rest',
                'action' => 'wovisit')),
	'rest_wo' => new Zend_Controller_Router_Route(
        'client/rest/wos/:winNum/',
            array('controller'=>'rest',
                'action' => 'wo')),
	'rest_wo_winnum' => new Zend_Controller_Router_Route(
        'client/rest/wos/:woID/winnum',
            array('controller'=>'rest',
                'action' => 'winnum')),
  	'rest_wo_publish' => new Zend_Controller_Router_Route(
        'client/rest/wos/:winNum/publish',
            array('controller'=>'rest',
                'action' => 'publish')),
   'rest_wo_assign' => new Zend_Controller_Router_Route(
        'client/rest/wos/:winNum/assign',
            array('controller'=>'rest',
                'action' => 'assign')),
	'rest_wo_approve' => new Zend_Controller_Router_Route(
        'client/rest/wos/:winNum/approve',
            array('controller'=>'rest',
                'action' => 'approve')),
	'rest_wo_deactivate' => new Zend_Controller_Router_Route(
        'client/rest/wos/:winNum/deactivate',
            array('controller'=>'rest',
                'action' => 'deactivate')),
	'rest_wo_incomplete' => new Zend_Controller_Router_Route(
        'client/rest/wos/:winNum/incomplete',
            array('controller'=>'rest',
                'action' => 'incomplete')),
    'rest_wos' => new Zend_Controller_Router_Route(
        'client/rest/wos/',
            array('controller'=>'rest',
                'action' => 'wos')),
    'rest_projects' => new Zend_Controller_Router_Route(
        'client/rest/projects/',
            array('controller'=>'rest',
                'action' => 'projects')),
    'rest_parts' => new Zend_Controller_Router_Route(
        'client/rest/parts/:winNum',
            array('controller'=>'rest',
                'action' => 'parts')),
    'rest_parts_partid' => new Zend_Controller_Router_Route(
        'client/rest/parts/:winNum/:partID',
            array('controller'=>'rest',
                'action' => 'parts')),
    'rest_tech_parts' => new Zend_Controller_Router_Route(
        'tech/rest/parts/:winNum/',
            array('controller'=>'rest',
                'action' => 'techparts')),
    'rest_tech_part_update' => new Zend_Controller_Router_Route(
        'tech/rest/parts/:winNum/:partID',
            array('controller'=>'rest',
                'action' => 'techparts')),
    'salesforce_sync' => new Zend_Controller_Router_Route(
        'salesforce/sync/:companyID/:token',
            array('controller'=>'salesforce',
                'action' => 'sync')),
    'salesforce_wsdl' => new Zend_Controller_Router_Route(
        'salesforce/wsdl/:companyID',
            array('controller'=>'salesforce',
                'action' => 'wsdl')),
    'client_soap' => new Zend_Controller_Router_Route(
        'client/soap/:version',
            array('controller'=>'client',
                'action' => 'soap')),
    'client_wsdl' => new Zend_Controller_Router_Route(
        'client/wsdl/:version',
            array('controller'=>'client',
                'action' => 'wsdl'))
    );

$router = $frontController->getRouter();
$router->addRoutes($routes);

$frontController->setParam('noViewRenderer', true);
$frontController->setControllerDirectory(DOCUMENT_INCLUDES . '/controllers');
$frontController->setBaseUrl('/api/');

try
{
    $frontController->dispatch();
}
catch  (Zend_Controller_Dispatcher_Exception $e) {
}


