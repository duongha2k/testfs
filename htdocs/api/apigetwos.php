<?php

require_once('../../includes/modules/web.init.php');
require_once('../../includes/modules/common.init.php');
require_once('../../includes/modules/tables.php');

$fromdate = $_REQUEST['fromdate'];
$todate = $_REQUEST['todate'];
$distance = $_REQUEST['distance'];
$zipcode = $_REQUEST['zipcode'];

$errs = array();
$return = "";

if ($fromdate == "")
{
    $errs[] = array("err" => "From date cannot be empty.");
} else
{
    try
    {
        $fromdatetime = strtotime($fromdate);
        if (!$fromdatetime)
        {
            $errs[] = array("err" => "From date format must be YYYY-mm-dd.");
        }
    } catch (Exception $e)
    {
        $errs[] = array("err" => "From date format must be YYYY-mm-dd.");
    }
}

if ($todate == "")
{
    $errs[] = array("err" => "To date cannot be empty.");
} else
{
    try
    {
        $todatetime = strtotime($todate);
        if (!$todatetime)
        {
            $errs[] = array("err" => "To date format must be YYYY-mm-dd.");
        }
    } catch (Exception $e)
    {
        $errs[] = array("err" => "To date format must be YYYY-mm-dd.");
    }
}

if ($distance == "")
{
    $errs[] = array("err" => "Distance cannot be empty.");
} else
{
    if (!is_numeric($distance))
    {
        $errs[] = array("err" => "Distance must be numeric.");
    } else
    {
        if ($distance <= 0)
        {
            $errs[] = array("err" => "Distance must be greater than zero.");
        }
    }
}

if ($zipcode == "")
{
    $errs[] = array("err" => "Zip code cannot be empty.");
} else
{
    if (strlen($zipcode) != 5)
    {
        $errs[] = array("err" => "Zip code incorrect format.");
    }
}

if (empty($errs))
{
    $fromdatetime = strtotime($fromdate);
    $fromdate = date("Y-m-d", $fromdatetime);

    $$todatetime = strtotime($todate);
    $todate = date("Y-m-d", $$todatetime);
    $apiWos = new Core_Api_WosClass();
    $woslist = $apiWos->getAPIWOS($fromdate, $todate, $distance, $zipcode);
    if (!empty($woslist))
    {
        foreach ($woslist as $wo)
        {
            $return.="<wo>
                            <ObservationEligible>" . ($wo["AllowDeVryInternstoObserveWork"] == 1 ? "Yes" : "No") . "</ObservationEligible>
                            <WIN>" . $wo["WIN_NUM"] . "</WIN>
                            <WorkOrderStatus>" . $wo["Status"] . "</WorkOrderStatus>
                            <ProjectName>" . htmlspecialchars($wo["Project_Name"], ENT_QUOTES) . "</ProjectName>
                            <WorkOrderHeadline>" . htmlspecialchars($wo["Headline"], ENT_QUOTES) . "</WorkOrderHeadline>
                            <SkillCategory>" . htmlspecialchars($wo["WO_Category"], ENT_QUOTES) . "</SkillCategory>
                            <StartType>" . ($wo["StartTypeID"] == 1 ? "Firm Start Time" : "Start Time Window") . "</StartType>
                            <StartDate>" . $wo["StartDate"] . "</StartDate>
                            <StartTime>" . $wo["StartTime"] . "</StartTime>
                            <EndDate>" . $wo["EndDate"] . "</EndDate>
                            <EndTime>" . $wo["EndTime"] . "</EndTime>
                            <EstimatedDurationOnsite>" . $wo["EstimatedDurationOnsite"] . "</EstimatedDurationOnsite>
                            <SiteAddress>" . htmlspecialchars($wo["Address"], ENT_QUOTES) . "</SiteAddress>
                            <City>" . htmlspecialchars($wo["City"], ENT_QUOTES) . "</City>
                            <State>" . htmlspecialchars($wo["State"], ENT_QUOTES) . "</State>
                            <ZipCode>" . $wo["Zipcode"] . "</ZipCode>
                            <AssignedTechnicianID>" . $wo["Tech_ID"] . "</AssignedTechnicianID>
                            <AssignedTechnicianFirstName>" . htmlspecialchars($wo["Tech_FName"], ENT_QUOTES) . "</AssignedTechnicianFirstName>
                            <AssignedTechnicianLastName>" . htmlspecialchars($wo["Tech_LName"], ENT_QUOTES) . "</AssignedTechnicianLastName>
                            <AssignedTechnicianEmailAddress>" . htmlspecialchars($wo["TechEmail"], ENT_QUOTES) . "</AssignedTechnicianEmailAddress>
                            <AssignedTechnicianPhone>" . $wo["TechPhone"] . "</AssignedTechnicianPhone>
                            <WorkDescription>" . htmlspecialchars($wo["Description"], ENT_QUOTES) . "</WorkDescription>
                            <TechRequirementsandRequiredTools>" . htmlspecialchars($wo["Requirements"], ENT_QUOTES) . "</TechRequirementsandRequiredTools>
                            <SpecialInstructions>" . htmlspecialchars($wo["SpecialInstructions"], ENT_QUOTES) . "</SpecialInstructions>
                        </wo>";
        }
        $return = "
                <result>
                    <wos>" . $return . "</wos>
                </result>";
    } else
    {
        $return = "<result>no result</result>";
    }
} else
{
    $return = "
            <result>
                <errors>";
    $i = 0;
    foreach ($errs as $err)
    {
        $return .="<error>" . $err['err'] . "</error>";
    }

    $return .="</errors></result>";
}
header('Content-type: application/xml');
header("Content-Length: " . strlen($return));
print_r($return);
?>
