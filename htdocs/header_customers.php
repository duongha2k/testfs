<?php 
$siteTemplate = $_SESSION['template'];

if ($siteTemplate == "ruo") {
	include ("{$_SERVER['DOCUMENT_ROOT']}/templates/ruo/includes/header.php");
} else if ($siteTemplate == "wap") {
	header("Location: ". "http://".$_SERVER['HTTP_HOST']."/wap/index.php?pagetype=html");
	exit;
} else {
	include ("headerSimple.php");
?>
<body style="min-width:1135px;">
	<!--script type="text/javascript" src="js/jquery.dataTables.min.js"></script-->
	<?php script_nocache ("/widgets/js/functions.js"); ?>
	<?php script_nocache ("/widgets/js/FSWidgetManager.js"); ?>
    <script type="text/javascript" src="/library/jquery/jquery.bt.min.js"></script>
<!--[if IE]><script src="/library/jquery/excanvas.js" type="text/javascript" charset="utf-8"></script><![endif]-->
<script type="text/javascript">
    var wManager = new FSWidgetManager(); //  Widget's manager
</script>
<div id="mainContainer">
	<div id="headerSlider">
		<div id="headerRight" align="right">
			<div id="topNavBar">
    <ul>
    <?php if ($loggedInAs=="customers" && !empty($_SESSION["UserName"]) ) : ?>
<li><nobr><b>Logged in as <span id="custN"><?=$_SESSION["UserName"]?></span></b>&nbsp;&nbsp;&nbsp;&nbsp;</nobr></li>
<?php endif;?>   
<?php if ($loggedInAs=="client") {?>
<li><a href="/clients/Training/FS_HowTo.php">Help</a></li>
<?php }?>

<?php if ( isset($page) && ($page == "clients" || $page == "techs" || $page == "admin" || $page == "staff" || $page == "accounting"  || $page == "customers") ) { ?>
<!-- LOGOUT BUTTON -->
<?php
    $Params="";
    $uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
    if($uri!=""||$uri!=null)
    {
        $arrURI=  explode("/", $uri);   
        if(($page == "customers")&&(strtolower($arrURI[1])==strtolower("quickticket")))
        {
            $Params="?FromLogin=$arrURI[1]";
        }        
    }
?>
<li><a href="/logOut.php<?=$Params?>">Logout</a></li>
<?php } ?>

<?php if( isset($displayHome) && $displayHome!="no"){   ?>
    <?php if($loggedIn=="yes" && $siteTemplate!="admin"){ //If Logged In  ?>

        <?php
        if($loggedInAs=="client"){ //Logged in as client
            echo '<li><a href="/clients/dashboard.php">Home</a></li>';
        }else if($loggedInAs=="tech"){ //Logged in as tech
            echo '<li><a href="/techs/dashboard.php">Home</a></li>';
        }else if($loggedInAs=="customer"){ //Logged in as customer
            echo '<li><a href="/quickticket/dashboard.php">Home</a></li>';
        }else{ // Default link
            echo '<li><a href="/index.php">Home</a></li>';
        }
        ?>

<?php }else{ // Not logged in or could be admin ?>
<li><a href="index.php">Home</a></li>
<?php } ?>
<li><a href="content/aboutUs.php">About Us</a></li>

<li><a href="mailto:info@fieldsolutions.com">Contact Us</a></li>
</ul>
<?php } else {?>
&nbsp;
<?php } ?>
			</div>
		</div>
	<div id="headerLogo" onclick="location=location.protocol+'//'+location.host+'/quickticket/'" style="cursor:pointer;"></div>
</div>
<?php
}
?>
