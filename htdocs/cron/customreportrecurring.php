<?php

require_once dirname(__FILE__) . '/bootstrap.php';
set_time_limit(600);

$core = new Core_Api_CusReportClass2();
$result = $core->getReportListForAutoEmail();
//print_r("corn result<pre>");
//print_r($result);
//print_r("</pre>");die;
if (!empty($result))
{
    foreach ($result as $item)
    {
        $uri = "http://www.fieldsolutions.com/reports_test/custom_v2/ExportReportViewToFile.php?Ext=csv&rptid=" . $item['id'] . "&txtTemplateName=" . urlencode($item['rpt_name']. "&attachmail=1");
        $config = array(
            'adapter' => 'Zend_Http_Client_Adapter_Curl',
            'curloptions' => array(CURLOPT_FOLLOWLOCATION => true),
        );
        $client = new Zend_Http_Client($uri, $config);
        $response = $client->request();
        $to = $item['RRDEmailTo'];
        $fromName = $item['RRDEmailFromName'];
        $fromMail = $item['RRDEmailFrom'];
        $subject = $item['RRDEmailSubject'];
        $message = $item['RRDEmailMessage'];
        $reportname = $item['rpt_name'];

        $mail = new Core_Mail();
        $mail->setBodyText($message);
        $mail->setFromEmail($fromMail);
        $mail->setFromName($fromName);
        $mail->setToEmail($to);
        $mail->setToName($to);
        $mail->setSubject($subject);
        $mail->setreportmail(true);
        if ($response->getBody() == "There were no results for the selected filters." || empty($response->getBody()))
        {
            $mail->setemptyattach(true);
        } else
        {
            $att = new Zend_Mime_Part($response->getBody());
            $att->type = 'text/csv';
            $att->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;
            $att->encoding = Zend_Mime::ENCODING_BASE64;
            $att->filename = $reportname . '.csv';
            $mail->addAttachments($att);
        }
        $mail->send(true);
    }
}
?>