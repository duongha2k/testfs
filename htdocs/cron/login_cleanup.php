<?php
	require_once dirname(__FILE__) . '/bootstrap.php';

	$db = Zend_Registry::get('DB');

	//Delete any login keys over a day old that have never been used.
	$db->delete ("client_login", "ISNULL(login_data) AND time_stamp < DATE_SUB(NOW(), INTERVAL 1 DAY)");

	//Delete any stale login keys that have not been used in the past month.
	$db->delete ("client_login", "time_stamp < DATE_SUB(NOW(), INTERVAL 30 DAY)");

	//Delete any login keys over a day old that have never been used.
	$db->delete ("tech_login", "ISNULL(login_data) AND time_stamp < DATE_SUB(NOW(), INTERVAL 1 DAY)");

	//Delete any stale login keys that have not been used in the past month.
	$db->delete ("tech_login", "time_stamp < DATE_SUB(NOW(), INTERVAL 30 DAY)");

