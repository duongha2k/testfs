<?php
	$dir = dirname(__FILE__);
	require_once $dir . '/bootstrap.php';
	$db = Zend_Registry::get('DB');

	$select = $db->select();
	$db->select();
	$select->from('work_order_import_kettle', array('id', 'file', 'username', 'company_id', 'mail', 'hash', 'work_order_owner'))->where('processed = ?', 0);
	$result = $db->fetchAll($select);
	if (count($result) == 0) die();
	$idList = array();
	foreach ($result as $import) {
		$idList[] = $import['id'];
	}
	$db->update('work_order_import_kettle', array('processed' => '1'), $db->quoteInto('id IN (?)', $idList));
	foreach ($result as $import) {
		if (strtolower($import['company_id']) == 'fls') {
	    	exec('php ' . $dir . '/../kettle/kettleScriptFls.php '.escapeshellarg($import['file']).' '.escapeshellarg($import['username']).' '.escapeshellarg($import['hash']).' '.escapeshellarg($import['mail']).' FLS'.' '.escapeshellarg($import['work_order_owner']));
		} else {
			exec('php ' . $dir . '/../kettle/kettleScript.php '.escapeshellarg($import['file']).' '.escapeshellarg($import['username']).' '.escapeshellarg($import['hash']).' '.escapeshellarg($import['mail']).' '.escapeshellarg($import['company_id']).' '.escapeshellarg($import['work_order_owner']));
		}		
	}

