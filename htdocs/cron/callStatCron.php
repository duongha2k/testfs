<?php
	require_once dirname(__FILE__) . '/bootstrap.php';
	$db = Zend_Registry::get('DB');

	try {
		require_once(realpath(dirname(__FILE__) . "/../") . "/library/caspioAPI.php");
		require_once(realpath(dirname(__FILE__) . "/../") . "/library/smtpMail.php");

		$sql = "SELECT Tech_ID, Count(WIN_NUM) AS cnt FROM work_orders WHERE IFNULL(Tech_ID,'') != '' AND Type_ID = 1 GROUP BY Tech_ID ORDER BY cnt ASC";
		$mainInstallCount = $db->fetchAll($sql);

		$countByID = array();
		foreach ($mainInstallCount as $nCnt) {
			$id = $nCnt['Tech_ID'];
			$count = $nCnt['cnt'];
			$countByID[$id] = $count;
		}

		$IDsByCount = array();
		foreach ($countByID as $id => $count) {
			// group Tech_ID by number of jobs
			if (!isset($IDsByCount[$count]))
				$IDsByCount["$count"] = $id;
			else
				$IDsByCount["$count"] .= "," . $id;
		}
		
		// clears Qty_IMAC_Calls
		caspioUpdate("TR_Master_List", "Qty_IMAC_Calls", "'0'", "", false);
		$db->update(Core_Database::TABLE_TECH_BANK_INFO, array('Qty_IMAC_Calls' => 0));

		foreach ($IDsByCount as $count => $ids) {
			// update tech's record with no show count
			//echo "UPDATE: Qty_IMAC_Calls '$count' TechID IN ($ids)\n\n";
			caspioUpdate("TR_Master_List", "Qty_IMAC_Calls", "'$count'", "TechID IN ($ids)", false);
			$db->update(Core_Database::TABLE_TECH_BANK_INFO, array('Qty_IMAC_Calls' => $count), "TechID IN ($ids)");
		}

//		die();
		
		// Count Service Calls
		
		$sql = "SELECT Tech_ID, Count(WIN_NUM) AS cnt FROM work_orders WHERE IFNULL(Tech_ID,'') != '' AND Type_ID = 2 GROUP BY Tech_ID ORDER BY cnt ASC";
		$mainServiceCount = $db->fetchAll($sql);

		$countByID = array();
		foreach ($mainServiceCount as $nCnt) {
			$id = $nCnt['Tech_ID'];
			$count = $nCnt['cnt'];
			$countByID[$id] = $count;
		}

		$IDsByCount = array();
		foreach ($countByID as $id => $count) {
			// group Tech_ID by number of jobs
			if (!isset($IDsByCount[$count]))
				$IDsByCount["$count"] = $id;
			else
				$IDsByCount["$count"] .= "," . $id;
		}
		
		// clears Qty_IMAC_Calls
		caspioUpdate("TR_Master_List", "Qty_FLS_Service_Calls", "'0'", "", false);
		$db->update(Core_Database::TABLE_TECH_BANK_INFO, array('Qty_FLS_Service_Calls' => 0));

		foreach ($IDsByCount as $count => $ids) {
			// update tech's record with no show count
			//echo "UPDATE: Qty_FLS_Service_Calls '$count' TechID IN ($ids)\n\n";
			caspioUpdate("TR_Master_List", "Qty_FLS_Service_Calls", "'$count'", "TechID IN ($ids)", false);
			$db->update(Core_Database::TABLE_TECH_BANK_INFO, array('Qty_FLS_Service_Calls' => $count), "TechID IN ($ids)");
		}
		
		
	}
	catch (SoapFault $fault) {
//		smtpMail("Call Stat Count Script", "nobody@fieldsolutions.com", "codem01@gmail.com,gerald.bailey@fieldsolutions.com", "Call Stat Script Error", "$fault", "$fault", "callStatCount.php");
	}
?>