<?php

    require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');

	require_once(realpath(dirname(__FILE__) . "/../") . "/library/caspioAPI.php");
	require_once(realpath(dirname(__FILE__) . "/../") . "/library/smtpMail.php");
	ini_set("display_errors", 1);
	set_time_limit(400);

	$cfgSite = Zend_Registry::get('CFG_SITE');

    $googleMapApiKey = $cfgSite->get("site")->get("google_map_key");
	$googleMapClientId = $cfgSite->get("site")->get("google_map_client_id");
	
	function geocodeAddress($address, $region = 'us') {
		global $googleMapApiKey, $googleMapClientId;
		// create a new cURL resource
		$ch = curl_init();
		$address = urlencode($address);
		$region = strtolower($region);
		// set URL and other appropriate options
		$options = array(CURLOPT_URL => "http://maps.google.com/maps/geo?q=$address&output=csv&client=$googleMapClientId&sensor=false&gl=$region",
						 CURLOPT_HEADER => false,
						 CURLOPT_RETURNTRANSFER => true
						);

		curl_setopt_array($ch, $options);
		
		// grab URL and pass it to the browser
		$response = curl_exec($ch);
		
		// close cURL resource, and free up system resources
		curl_close($ch);
		
		if ($response) {
			$info = explode(",", $response);
			if ($info[0] == "200") {
				$lat = $info[2];
				$long = $info[3];
				return array($lat, $long);
			}
		}		
		return false;
	}

	$table = "TR_Master_List";

	$ignoreList = array(13527, 14900, 16613, 16846, 16913, 17480, 17659, 18110, 18360, 18631, 19066, 19189, 20212, 20212, 20279,20350,20427,20621,20955,21392,21860,22264,22377,22461,22774,22781,22787,22995,23133,23213,23392,23411,23908,24453,25269, 25352, 25379, 25457, 25475, 25502, 25956, 26251, 26298, 27025, 27087, 27535, 27772, 27824, 28062, 28155, 28353, 28419,30895, 31300, 31303, 31304, 31355, 31405, 31533, 31771, 31843, 31849, 31859, 32217, 32341, 32343,32349, 32393, 32421, 32475, 32485, 32518, 32532, 32797, 33340, 33438, 33910, 33919, 33996, 34253, 34334, 34668, 34679, 35121, 36380);
	
	$db = Core_Database::getInstance();
	$select = $db->select();
	$select->from(array('t' => Core_Database::TABLE_TECH_BANK_INFO), array('TechID', 'State', 'Zipcode', 'Address1', 'Country', 'City'))
		->joinLeft(array('z' => 'zipcodes'), '`t`.Zipcode = `z`.ZIPCode', array('Latitude', 'Longitude'))
		->where("`t`.Zipcode IS NOT NULL AND `t`.Zipcode != ''")
		->where("`t`.Latitude IS NULL OR `t`.Longitude IS NULL OR ((Country = 'US' OR Country = '' OR Country IS NULL) AND (ABS(`z`.Latitude - `t`.Latitude) >= 1 OR ABS(`z`.Longitude - `t`.Longitude) >= 1)) OR (Country != 'US' AND Country != '' AND Country IS NOT NULL AND `z`.ZIPCode IS NOT NULL AND (ABS(`z`.Latitude - `t`.Latitude) <= 1 OR ABS(`z`.Longitude - `t`.Longitude) <= 1))")
		->where('`t`.TechID NOT IN (?)', $ignoreList);
	$techList = $db->fetchAll($select);
		
	$unable = array();
	
	if (!$techList || sizeof($techList) == 0) exit;
	
	foreach ($techList as $wo) {
		if (empty($wo)) continue;
		$info = $wo;
		$newLat = $info['Latitude'];
		$newLong = $info['Longitude'];
		$techID = $info['TechID'];
		$state = $info['State'];
		$city = $info['City'];
		$country = $info['Country'];
		if (empty($country)) $country = 'US';
//		if (array_key_exists($techID,$ignoreList)) continue;
		
		$zipcode = $info['Zipcode'];
		$address = $info['Address1'];

		if (strtolower($address) == '123 main st') $address = '';

		$location = "$address $city, $state, $zipcode, $country";
		$origloc = $location;
//		echo "TRY: TechID = '$techID' $country $address $zipcode\n";
		sleep(1);
		$coord = geocodeAddress($location, $country);
		if (empty($coord[0])) {
			$location = "$zipcode, $country";
			sleep(1);
			$coord = geocodeAddress($location, $country);
		}
		if (!empty($coord[0]) && !empty($coord[1])) {
			$newLat = $coord[0];
			$newLong = $coord[1];
		}
		if ($newLat == "" || $newLong == "") {
			//echo "UNABLE: WHERE TechID = '$techID' $country $address $zipcode\n";
			$unable[] = $techID;
//			smtpMail("Geocode Tech CRON", "no-replies@fieldsolutions.us", "tngo@fieldsolutions.com", "Unable to geocode TechID: $techID", "TechID: $techID", "TechID: $techID", "Geocode Tech CRON");
			continue;
		}
//		echo "Update $newLat, $newLong WHERE TechID = '$techID' $origloc ... \n";
		$db->update(Core_Database::TABLE_TECH_BANK_INFO, array('Latitude' => $newLat, 'Longitude' => $newLong), $db->quoteInto('TechID = ?', $techID));
		//echo "<br/>";
	}
	
	//echo implode(", ", $unable);
?>
