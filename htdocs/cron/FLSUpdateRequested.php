<?php
require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');
$db = Core_Database::getInstance();
$select = $db->select();
$select->from(array('w' => Core_Database::TABLE_WORK_ORDERS), array('WO_ID', 'Project_Name', 'Update_Reason', 'lastModDate'))
	->joinLeft(array('t' => Core_Database::TABLE_TIMESTAMPS), 
	"w.WIN_NUM = t.WIN_NUM AND ((Update_Reason = 'Tech Backed Out' AND t.Description LIKE 'Tech Backed Out (IVR)') OR (t.Description LIKE 'Work Order Updated:%' AND 
t.Description LIKE '%Update_Requested%'))",
	array('DateTime_Stamp'))
	->where('w.Status IN (?)', array('assigned', 'work done', 'incomplete'))
	->where('w.Update_Requested = ?', 1)
	->where('w.Company_ID = ?', 'FLS')
	->order('w.DateEntered DESC')
	->order('t.DateTIme_Stamp DESC')
	->group('w.WO_ID');

$wos = $db->fetchAll($select);

if (empty($wos)) die();

$email = new Core_EmailForUpdateRequested;
$email->setWOData($wos);
$email->sendEmail();