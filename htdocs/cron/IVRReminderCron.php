<?php
	try {
	$rootPath = realpath(dirname(__FILE__) . "/../");
//	require_once($rootPath . "/ivr/ivrheader.php");
//	require_once($rootPath . "/ivr/library/voxeoAPI.php");
	require_once($rootPath . "/library/smtpMail.php");
//	smtpMail("IVR Application", "nobody@fieldsolutions.com", "tngo@fieldsolutions.com", "IVR START", "IVR CRON START", "IVR CRON START", "IVR Application");
	ini_set("display_errors",1);
	$_SESSION["loggedIn"] = "yes";
	require_once($rootPath . "/library/caspioAPI.php");
//	smtpMail("IVR Application", "nobody@fieldsolutions.com", "tngo@fieldsolutions.com", "IVR INIT", "IVR A", "IVR A", "IVR Application");
	require_once($rootPath . "/ivr/library/ivrLib.php");
//	smtpMail("IVR Application", "nobody@fieldsolutions.com", "tngo@fieldsolutions.com", "IVR INIT", "IVR B", "IVR B", "IVR Application");
	require_once($rootPath . "/ivr/library/reminderCallBGLib.php");
//	smtpMail("IVR Application", "nobody@fieldsolutions.com", "tngo@fieldsolutions.com", "IVR INIT", "IVR C", "IVR C", "IVR Application");
	require_once($rootPath . "/library/woContactLog_ivr.php");
//	smtpMail("IVR Application", "nobody@fieldsolutions.com", "tngo@fieldsolutions.com", "IVR INIT DONE", "IVR D", "IVR D", "IVR Application");

	$msg = "";

	set_time_limit(600);

	function runCall($callType, $woList) {
		switch ($callType) {
			case LOG_CALLTYPE_SITE_CONTACT_CALL:
			case LOG_CALLTYPE_1_HR_CALL:
			case LOG_CALLTYPE_CUSTOM_HR_CALL_1:
			case LOG_CALLTYPE_CUSTOM_HR_CALL_2:
			case LOG_CALLTYPE_CUSTOM_HR_CALL_3:
				$callListNew = array();
				foreach ($woList as $info) {
					$uniqueID = $info['WIN_NUM'];
					$startDate = $info['StartDate'];
					$startTime = $info['StartTime'];
					$endDate = $info['EndDate'];
					$endTime = $info['EndTime'];

					$endDate = $endDate == "" || $endDate < "1970-01-01" ? $startDate : $endDate;
					$endTime = $endTime == "" ? $startTime : $endTime;

					if ($endDate != $startDate) {
						//echo "Ignoring UniqueID due to date range: $uniqueID of between $startDate and $endDate<br/>";
						$msg = "Date Range on WO with 1 HR Reminder: $uniqueID";
//						smtpMail("IVR Application", "nobody@fieldsolutions.com", "tngo@fieldsolutions.com", "Unable to do 1 HR Reminder", $msg, nl2br($msg), "IVR Application");
					}
					else
						$callListNew[] = $info;
				}
				$woList = $callListNew;
				break;
			default:
				break;
		}

		$groupedCallsUniqueID = array();
		$groupedCallsCompanyID = array();
		$currentTech = -1;
		$numWO = sizeof($woList);
		$processed = 0;
		$currentStartDate = -1;
		$currentStartTime = -1;
		$currentEndDate = -1;
		$currentEndTime = -1;
		foreach ($woList as $info) {
			//echo "Pending $callType: ";
			$uniqueID = $info['WIN_NUM'];
			$techID = $info['Tech_ID'];
			$primaryPhone = $info['PrimaryPhone'];
			$secondaryPhone = $info['SecondaryPhone'];
			$primPhoneType = $info['PrimPhoneType'];
			$secondPhoneType = $info['SecondPhoneType'];
			$emergencyPhone = $info['ACSNotifiPOC_Phone'];
			$companyID = $info['Company_ID'];
			$startDate = $info['StartDate'];
			$startTime = $info['StartTime'];
			$endDate = $info['EndDate'];
			$endTime = $info['EndTime'];

			if ($info == "") {
				//echo "<div style=\"margin: 0px 0px 0px 15px\">None</div><br/>";
				continue;
			}
//			if ($companyID != "CBD" && $companyID != "BW" && $companyID != "FS")
//				continue;


			$endDate = $endDate == "" || $endDate < "1970-01-01" ? $startDate : $endDate;
			$endTime = $endTime == "" ? $startTime : $endTime;

			/*echo "<div style=\"margin: 0px 0px 0px 15px\">WOUNID: $uniqueID</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Company: $companyID</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">TechID: $techID</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Primary Phone: $primaryPhone</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Secondary Phone: $secondaryPhone</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Primary Type Phone: $primPhoneType</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Secondary Type Phone: $secondPhoneType</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Emergency Phone: $emergencyPhone</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Start Date: $startDate</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Start Time: $startTime</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">End Date: $endDate</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">End Time: $endTime</div>";*/

			if ($callType != LOG_CALLTYPE_EMERGENCY_DISPATCH_BACK_OUT_CALL && $callType != LOG_CALLTYPE_EMERGENCY_DISPATCH_NOT_ON_SITE_CALL && $callType != LOG_CALLTYPE_EMERGENCY_DISPATCH_SITE_CONTACT_BACK_OUT_CALL) {
				if ($callType == LOG_CALLTYPE_SITE_CONTACT_CALL) {
					$phone = $info["SitePhone"];
				}
				else
					$phone = ($callType == LOG_CALLTYPE_1_HR_CALL && $primPhoneType != "Cell" && $secondaryPhone != "" && $secondPhoneType == "Cell" ? $secondaryPhone : $primaryPhone);
			}
			else {
				$phone = $emergencyPhone;
			}

			$phone = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);

			$phone = str_replace(array("-", ".", " ", "(", ")"), array("", "", "", "", ""), $phone);

			//echo "<br/><div style=\"margin: 0px 0px 0px 15px\">Using number: $phone</div><br/>";
			@ob_flush();
			@flush();

			// Testing purposes
//			if ($phone != "5122728328" || $phone != "8326878350")
//				$phone = "5122728328";
//				$phone = "8326878350";
			
			$phone = "+1" . $phone;
			//echo "<div style=\"margin: 0px 0px 0px 15px\">Dialing $phone</div>";

			$processed++;

			if ($callType != LOG_CALLTYPE_EMERGENCY_DISPATCH_BACK_OUT_CALL && $callType != LOG_CALLTYPE_EMERGENCY_DISPATCH_NOT_ON_SITE_CALL && $callType != LOG_CALLTYPE_EMERGENCY_DISPATCH_SITE_CONTACT_BACK_OUT_CALL) {
				if ($currentTech == -1 || ($callType != LOG_CALLTYPE_1_HR_CALL && $currentTech == $techID) || ($callType == LOG_CALLTYPE_1_HR_CALL && ($currentStartDate == $startDate && $currentStartTime == $startTime && $currentEndDate == $endDate && $currentEndTime == $endTime && $currentTech == $techID)) ) {
					$groupedCallsUniqueID[] = $uniqueID;
					$groupedCallsCompanyID[] = $companyID;
					$currentTech = $techID;
					if ($callType == LOG_CALLTYPE_1_HR_CALL) {
						$currentStartDate = $startDate;
						$currentStartTime = $startTime;
						$currentEndDate = $endDate;
						$currentEndTime = $endTime;
					}
					$currentPhone = $phone;
				}

				if ($processed == $numWO || ($callType != LOG_CALLTYPE_1_HR_CALL && $currentTech != $techID) || ($callType == LOG_CALLTYPE_1_HR_CALL && ($currentStartDate != $startDate || $currentStartTime != $startTime || $currentEndDate != $endDate || $currentEndTime != $endTime || $currentTech != $techID)) ) {
					$groupedCalls = implode(",", $groupedCallsUniqueID);
					$groupedCompanyID = implode(",", $groupedCallsCompanyID);
					//echo "<br/>**** CALL: TechID:$currentTech Phone:$currentPhone WOs:$groupedCalls**** <br/>";
					reminderCallBG(implode(",", $groupedCallsUniqueID), implode(",", $groupedCallsCompanyID), $currentPhone, $callType, $currentTech);
					if ($processed == $numWO && (($callType != LOG_CALLTYPE_1_HR_CALL && $currentTech != $techID) || ($callType == LOG_CALLTYPE_1_HR_CALL && ($currentStartDate != $startDate || $currentStartTime != $startTime || $currentEndDate != $endDate || $currentEndTime != $endTime || $currentTech != $techID))) ) {
						//echo "<br/>**** CALL: TechID:$techID Phone:$phone WOs:$uniqueID **** <br/>";
						reminderCallBG($uniqueID, $companyID, $phone, $callType, $techID);
					}
					else {
						$groupedCallsUniqueID = array($uniqueID);
						$groupedCallsCompanyID = array($companyID);
						$currentTech = $techID;
						if ($callType == LOG_CALLTYPE_1_HR_CALL) {
							$currentStartDate = $startDate;
							$currentStartTime = $startTime;
							$endStartDate = $endDate;
							$endStartTime = $endTime;
						}
						$currentPhone = $phone;
					}
				}
			}
			else {
				reminderCallBG($uniqueID, $companyID, $phone, $callType, $techID);
			}
		}
		//echo "<hr/>";
	}

	// find work orders needing reminder calls

	$fieldList = array(
		'WIN_NUM',
		'Tech_ID',
		'PrimaryPhone',
		'SecondaryPhone',
		'PrimPhoneType',
		'SecondPhoneType',
		'ACSNotifiPOC_Phone',
		'Company_ID',
		'StartDate',
		'StartTime',
		'EndDate',
		'EndTime',
		'is.RedialCount',
		'SitePhone'
	);

	// keep mapping table updatedp
	$sql = "INSERT IGNORE INTO tech_id_mapping (tbi_id, wo_tech_id, tech_id) SELECT id, CAST(TechID AS UNSIGNED), TechID FROM TechBankInfo AS t LEFT JOIN tech_id_mapping AS m ON t.id = m.tbi_id WHERE m.tbi_id IS NULL AND t.TechID NOT LIKE '%ISO%'";
	$db = Core_Database::getInstance();
	$r = $db->query($sql);

	$emergencyDisptachNotOnSiteCalls = getReminderEmergencyDispatchNotOnSite($fieldList);
	//echo "\n\n" . count($emergencyDisptachNotOnSiteCalls) . "\n\n";
	$emergencyDisptachBackOutCalls = getReminderEmergencyDispatchBackOut($fieldList);
	//echo "\n\n" . count($emergencyDisptachBackOutCalls) . "\n\n";
	$oneHrCalls = getReminder1Hr($fieldList);
	$siteContact = getReminderSiteContactHr($fieldList);
	//echo "\n\n" . count($oneHrCalls) . "\n\n";
	$customHrCalls1 = getReminderCustomHr1($fieldList);
	//echo "Custom Call #1\n";
	$customHrCalls2 = getReminderCustomHr2($fieldList);
	//echo "Custom Call #2\n";
	$customHrCalls3 = getReminderCustomHr3($fieldList);
	//echo "Custom Call #3\n";
	$twentyFourHrCalls = getReminder24Hr($fieldList);
	//echo "\n\n" . count($twentyFourHrCalls) . "\n\n";
	$acceptanceCalls = getReminderAcceptance($fieldList);
	//echo "\n\n" . count($acceptanceCalls) . "\n\n";
	$notCompleteCalls = getReminderNotMarkedComplete($fieldList);
	//echo "\n\n" . count($notCompleteCalls) . "\n\n";
	$incompleteCalls = getReminderIncomplete($fieldList);
	//echo "\n\n" . count($incompleteCalls) . "\n\n";

	runCall(LOG_CALLTYPE_EMERGENCY_DISPATCH_BACK_OUT_CALL, $emergencyDisptachBackOutCalls);
	runCall(LOG_CALLTYPE_EMERGENCY_DISPATCH_NOT_ON_SITE_CALL, $emergencyDisptachNotOnSiteCalls);
	runCall(LOG_CALLTYPE_1_HR_CALL, $oneHrCalls);
	runCall(LOG_CALLTYPE_SITE_CONTACT_CALL, $siteContact);	
	runCall(LOG_CALLTYPE_CUSTOM_HR_CALL_1, $customHrCalls1);
	runCall(LOG_CALLTYPE_CUSTOM_HR_CALL_2, $customHrCalls2);
	runCall(LOG_CALLTYPE_CUSTOM_HR_CALL_3, $customHrCalls3);
	runCall(LOG_CALLTYPE_24_HR_CALL, $twentyFourHrCalls);
	runCall(LOG_CALLTYPE_ACCEPTANCE_CALL, $acceptanceCalls);
	runCall(LOG_CALLTYPE_NOT_MARKED_COMPLETE_CALL, $notCompleteCalls);
	runCall(LOG_CALLTYPE_INCOMPLETE_CALL, $incompleteCalls);
	}
	catch (Exception $e) {
		smtpMail("IVR Application", "nobody@fieldsolutions.com", "tngo@fieldsolutions.com", "IVR ERROR", "IVR CRON ERROR $e", "IVR CRON ERROR $e", "IVR Application");
		
	}
//	smtpMail("IVR Application", "nobody@fieldsolutions.com", "tngo@fieldsolutions.com", "IVR RAN", "IVR CRON FINSIHED", "IVR CRON FINISHED", "IVR Application");
?>
