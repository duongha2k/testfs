<?php
	require_once dirname(__FILE__) . '/bootstrap.php';
	$db = Zend_Registry::get('DB');

	require_once(realpath(dirname(__FILE__) . "/../") . "/library/caspioAPI.php");
    require_once(realpath(dirname(__FILE__) . "/../") . "/library/smtpMail.php");

	$techsWithPaymentPending = array();
	
	// is it time to send w9 reminder email
	$sql = "SELECT TechID FROM W9Reminders WHERE  EmailsSent < 5 OR DATE_SUB(NOW(), INTERVAL 7 DAY)  > DateLastSent";
    $timeToTechIn = $db->fetchCol($sql);
    if (empty($timeToTechIn)) {
        $timeToTechIn = '0';
    } else {
        $timeToTechIn = implode(',', $timeToTechIn);
    }
    
    // is tech new to the w9 reminder list
    $sql = "SELECT TechID FROM W9Reminders";
    $allTechs = $db->fetchCol($sql);
    $allTechIn = '';
    if (empty($allTechs)) {
        $allTechIn = '0';
    } else {
        $allTechIn = implode(',', $allTechs);
    }

    $techsWithPaymentPending = caspioSelectAdv("TR_Master_List", "TechID, PrimaryEmail", "W9 = '0' AND IFNULL(ISO_Affiliation_ID, 0) = 0 AND (TechID IN ({$timeToTechIn}) OR TechID NOT IN({$allTechIn}) ) AND AcceptTerms = 'Yes' AND Country = 'US'", "", false, "`", "|", false);
    
	$techIDS = array();

	foreach ($techsWithPaymentPending as $value) {
		if ($value == "") continue;
		$value = explode("|", $value);
		$techID = $value[0];
		$techIDS[] = $techID;
	}

	$sql = "SELECT DISTINCT Tech_ID FROM work_orders WHERE Tech_ID IN (" . $db->quote($techIDS) . ") AND Invoiced=1 AND Deactivated=0 AND TechPaid = 0 ORDER BY Tech_ID";
	$techs = $db->fetchCol($sql);
	if (!empty($techs)) {
	    $techs = array_combine($techs, $techs);
	} else {
	    $techs = array();
	}

	$techList = array();
	$emailList = array();

	foreach ($techsWithPaymentPending as $key=>$value) {
		if ($value == "") continue;
		$value = explode("|", $value);
		$techID = $value[0];
		if (!isset($techs[$techID])) continue;
		$email = trim($value[1], "`");
		$insertRequired = !in_array($techID, $allTechs);
		$techList[] = $techID;
		$emailList[] = $email;
		if ($insertRequired) {
            $data = array(
                'TechID' => $techID,
                'EmailsSent' => '0'
            );
            $db->insert("W9Reminders", $data);
		}
	}

	if (sizeof($techList) > 0) {
	    $data = array(
	        'EmailsSent' => new Zend_Db_Expr('EmailsSent + 1'),
	        'DateLastSent' => new Zend_Db_Expr('NOW()')
	    );
	    $where = 'TechID IN ( ' . implode(',' , $techList) . ')';
	    $db->update('W9Reminders', $data, $where);
	}
	//print_r($techsWithPaymentPending);
	//print_r($emailList);
	//die();
	
	
	if (sizeof($emailList) > 0) {
		$message = "You are owed money for work orders that have been completed and approved. However, we do not have a W-9 on record for you and cannot pay you until we do.

W-9�s must be submitted online through the Field Solutions system. Please go to the W-9 section within your profile and complete and submit your W-9.

Your W-9 status will automatically be changed, and we will send you your money on the next scheduled pay date, based on your banking election of Check or Direct Deposit.

Thank you for your prompt attention to this matter";



		smtpMailLogReceived("Field Solutions Accounting", "accounting@fieldsolutions.com", implode(",", $emailList), "We Owe You Money, submit your W-9 to Field Solutions.", $message, nl2br($message), "W9 Reminder Cron");
	}

?>
