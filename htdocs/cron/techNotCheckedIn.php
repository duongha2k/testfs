<?php
require_once dirname(__FILE__) . '/bootstrap.php';
$db = Zend_Registry::get('DB');

// Follow up to wosEmailTechWhenAssigned.php 24 hours later. This script will email the project coordinator if the Tech did not click the link and check the "I have reviewed all Work Order Details and documentation." on the work order.

require_once(realpath(dirname(__FILE__) . "/../") . "/library/smtpMail.php");

// Set Variables
//$yesterday = date('m/d/Y', strtotime('now'));//look back 24 hours
$yesterday = date('Y-m-d', strtotime('-1 days'));//look back 24 hours

//echo "Yesterday: $yesterday<br>";
$result = "Success"; //Value required by our library, use of try should negate this
$calledFrom = (empty($_SERVER['HTTP_REFERER']) ? "Unknown" : $_SERVER['HTTP_REFERER']);// library needs this


// Get Projects Info
$sql = "SELECT Project_ID, To_Email, Project_Name FROM projects";
$project = $db->fetchAll($sql);
//$project = caspioSelect("TR_Client_Projects", "Project_ID, To_Email, Project_Name", "", false);

$projectCount = sizeof($project);
//echo "Project Count: $projectCount<br>";

// Get workorders and tech info from a view called "Tech_Called_MainSite
$sql = "SELECT StartDate, WIN_NUM, PayAmount, StartTime, WIN_NUM, PrimaryEmail, SiteName, SiteNumber, General10, Project_ID, TechCheckedIn_24hrs, PrimaryPhone, SecondaryPhone, Tech_FName, Tech_LName, Approved, Invoiced, Date_Assigned"
. " FROM Tech_Called_MainSite"
. " WHERE WorkOrderReviewed = 0 AND Approved = 0 AND Date_Assigned = ? AND Deactivated = 0 AND Project_ID != 0"
. " ORDER BY StartDate";

$records = $db->fetchAll($sql, array($yesterday), Zend_Db::FETCH_NUM);
//$records = caspioSelectView("Tech_Called_MainSite", "StartDate, WO_ID, PayAmount, StartTime, TB_UNID, PrimaryEmail, SiteName, SiteNumber, General10, Project_ID, TechCheckedIn_24hrs, PrimaryPhone, SecondaryPhone, Tech_FName, Tech_LName, Approved, Invoiced, Date_Assigned", "WorkOrderReviewed = '0' AND Approved='0' AND Date_Assigned = '$yesterday' AND IsNull(Deactivated, '') = '' AND Project_ID!='0'", "StartDate", false);

// Counts # of records returned
$count = sizeof($records);
//echo "Record Count: $count<br><br>";

// Populate array with workorders
foreach ($records as $fields) {
	// Get data from array and set values for email
	$startDate = $fields[0];
	$startDate = date("m/d/Y", strtotime($startDate));
	$WO_ID = $fields[1];
	$PayAmount= $fields[2];
	$PayAmount = str_replace("$", "", $PayAmount);
	$StartTime = $fields[3];
	$TB_UNID = $fields[4];
	$techEmail = $fields[5];
	$siteName = $fields[6];
	$siteNumber = $fields[7];
	$General10 = $fields[8];
	$ProjectID = $fields[9];
	$checkedin = $fields[10];
	$primaryPhone = $fields[11];
	$secondaryPhone = $fields[12];
	$techFname = $fields[13];
	$techLname = $fields[14];
	$approved = $fields[15];
	$invoiced = $fields[16];
	$dateAssigned = $fields[17];

	if (Core_Tech::isWMTech($TB_UNID)) continue;
	//echo "________________________________________<br>";
	//echo "<b>Work Order</b><br>";
	//echo "TB UNID: $TB_UNID<br>";
	//echo "Tech Checked In: $checkedin<br>";
	//echo "Approved: $approved<br>";
	//echo "Invoiced: $invoiced<br>";
	//echo "Tech Email: $techEmail<br>";
	//echo "Project ID: $ProjectID<br>";
	//echo "Start Date: $startDate<br>";
	//echo "Date Assigned: $dateAssigned<br>";
	//echo "WOID: $WO_ID<br><br>";

	// Default mail values
	$vFromName = "Field Solutions";
	$vFromEmail = "Projects@fieldsolutions.com";

	$wo = new Core_WorkOrder();
	$wo->setData($WO_ID);
	$sysGenEmailTo = $wo->getSystemGeneratedEmailTo();
    //13736
    $ApiWosClass = new Core_Api_WosClass();
    $FutureWOInfo = $ApiWosClass->getFutureWOInfo_ByWinNum($WO_ID);
    $SystemGeneratedEmailTo = $FutureWOInfo['SystemGeneratedEmailTo'];
    //end 13736
	
	// Populate array with project info
foreach ($project as $order) {
	if ($order['Project_ID'] == $ProjectID){

	$projectID = $order['Project_ID']; 	// Project ID
    $projectName = $order['Project_Name'];   // Project Name
    
    //13736
    if(!empty($sysGenEmailTo))    {
        $sendToEmail=$sysGenEmailTo;
    }  else if(!empty($SystemGeneratedEmailTo)) {
        $sendToEmail=$SystemGeneratedEmailTo;
    }  else {
        $sendToEmail=$order['To_Email'];
    }
    //end 13736        
    
	$vSubject = "Tech has not checked in: $projectName, Site# $siteNumber";

	$eList = $sendToEmail;

	@$caller = "techNotCheckedIn";


	$vMessage = "Dear Resource Coordinator,/r/r This email is to inform you that the Technician assigned to your project has not acknowledged being assigned to this project./r/r <b>Project Information:</b>/r/r Project Name: $projectName/r Work order number: $WO_ID/r Date of work: $startDate/r Start Time: $StartTime/r Site Name: $siteName/r Site Number: $siteNumber/r/r <b>Tech Contact Information:</b>/r/r Name: $techFname $techLname/r Email: $techEmail/r Primary Phone: $primaryPhone/r Secondary Phone: $secondaryPhone/r/r/r Thank you,/r Field Solutions Management";


	$html_message = "Dear Resource Coordinator,<br /><p>This email is to inform you that the Technician assigned to your project has not acknowledged being assigned to this project.</p><p><b>Project Information:</b></p><p>Project Name: $projectName<br>Work order number: $WO_ID<br />Date of work: $startDate<br />Start Time: $StartTime<br />Site Name: $siteName<br />Site Number: $siteNumber<br /></p><p><b>Tech Contact Information:</b></p><p>Name: $techFname $techLname<br>Email: $techEmail<br>Primary Phone: $primaryPhone<br>Secondary Phone: $secondaryPhone</p><br><p>Thank you,<br />Field Solutions Management</p>";


// Emails Client
smtpMailLogReceived($vFromName, $vFromEmail, $eList, $vSubject, $vMessage, $html_message, $caller);
//smtpMailLogReceived($vFromName, $vFromEmail, "gbailey@fieldsolutions.com", $vSubject, $vMessage, $html_message, $caller);


$CCeList = "collin.mcgarry@fieldsolutions.com";
//smtpMail($vFromName, $vFromEmail, $CCeList, $vSubject, $vMessage, $html_message, $caller);
   }
  }
 }
