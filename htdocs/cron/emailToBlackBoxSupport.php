<?php

require_once dirname(__FILE__) . '/bootstrap.php';
$db = Zend_Registry::get('DB');

require_once(realpath(dirname(__FILE__) . "/../") . "/library/smtpMail.php");

//$fromDateTime = date('Y-m-d', strtotime('-1 days'));
//$toDateTime = date('Y-m-d', strtotime('+0 days'));
//$currentTime = "00:00:00 AM";//

//$fromDateTime = $fromDateTime . " " . $currentTime;
//$toDateTime = $toDateTime . " " . $currentTime;
$apiTech = new Core_Api_TechClass();

$C1List = $apiTech->getTechList_RequestBBCablingTag(1);
$C2List = $apiTech->getTechList_RequestBBCablingTag(2);
$C3List = $apiTech->getTechList_RequestBBCablingTag(3);

$T1List = $apiTech->getTechList_RequestBBTelephonyTag(1);
$T2List = $apiTech->getTechList_RequestBBTelephonyTag(2);

$message = "<p>Technicians listed below have indicated that they have the required tools and experience to become a Black Box Technician:</p>";
if (!empty($C1List))
{
    $message .="<div style='text-decoration:underline;'>Black Box Cabling &ndash; Helper (C1)</div>";
    $message .="<ul>";
    foreach ($C1List as $C1)
    {
        $message .="<li>" . $C1['tech_id'] . " &ndash; " . $C1['LastName'] . ", " . $C1['FirstName'] . " &ndash; " . $C1['PrimaryPhone'] . " &ndash; " . $C1['PrimaryEmail'] . "</li>";
    }
    $message .="</ul>";
}

if (!empty($C2List))
{
    $message .="<div style='text-decoration:underline;'>Black Box Cabling &ndash; Advanced (C2)</div>";
    $message .="<ul>";
    foreach ($C2List as $C2)
    {
        $message .="<li>" . $C2['tech_id'] . " &ndash; " . $C2['LastName'] . ", " . $C2['FirstName'] . " &ndash; " . $C2['PrimaryPhone'] . " &ndash; " . $C2['PrimaryEmail'] . "</li>";
    }
    $message .="</ul>";
}

if (!empty($C3List))
{
    $message .="<div style='text-decoration:underline;'>Black Box Cabling &ndash; Lead (C3)</div>";
    $message .="<ul>";
    foreach ($C3List as $C3)
    {
        $message .="<li>" . $C3['tech_id'] . " &ndash; " . $C3['LastName'] . ", " . $C3['FirstName'] . " &ndash; " . $C3['PrimaryPhone'] . " &ndash; " . $C3['PrimaryEmail'] . "</li>";
    }
    $message .="</ul>";
}

if (!empty($T1List))
{
    $message .="<div style='text-decoration:underline;'>Black Box Telecom &ndash; Helper (T1)</div>";
    $message .="<ul>";
    foreach ($T1List as $T1)
    {
        $message .="<li>" . $T1['tech_id'] . " &ndash; " . $T1['LastName'] . ", " . $T1['FirstName'] . " &ndash; " . $T1['PrimaryPhone'] . " &ndash; " . $T1['PrimaryEmail'] . "</li>";
    }
    $message .="</ul>";
}

if (!empty($T2List))
{
    $message .="<div style='text-decoration:underline;'>Black Box Telecom &ndash; Advance (T2)</div>";
    $message .="<ul>";
    foreach ($T2List as $T2)
    {
        $message .="<li>" . $T2['tech_id'] . " &ndash; " . $T2['LastName'] . ", " . $T2['FirstName'] . " &ndash; " . $T2['PrimaryPhone'] . " &ndash; " . $T2['PrimaryEmail'] . "</li>";
    }
    $message .="<ul>";
}

$subject = "Black Box Cabling and Telephony Techs";
$to = "blackbox@fieldsolutions.com";
$fromName = "support@fieldsolutions.com";
$fromEmail = "support@fieldsolutions.com";
@$caller = "BlackBoxSupport";
// Emails Client

if (empty($C1List) && empty($C2List) && empty($C3List) && empty($T1List) && empty($T2List))
{
    
} else
{
    smtpMailLogReceived($fromName, $fromEmail, $to, $subject, $message, $message, $caller);
}
