<?php
require_once dirname(__FILE__) . '/bootstrap.php';
$db = Zend_Registry::get('DB');

// This script uses Caspio View to retrieve all tech jobs that are to be completed in 48 hours
// An email is sent to the tech in which he clicks a link to verify that the job will be
// completed.


require_once(realpath(dirname(__FILE__) . "/../") . "/library/smtpMail.php");

// Set Variables
//$tomorrow = date('m/d/Y', strtotime('now'));//today for test purposes
$tomorrow = date('Y-m-d', strtotime('+2 days'));//look ahead 48 hours
$result = "Success"; //Value required by our library, use of try should negate this
$calledFrom = (empty($_SERVER['HTTP_REFERER']) ? "Unknown" : $_SERVER['HTTP_REFERER']);// library needs this


// Get Projects Info
$sql = "SELECT Project_ID, From_Email, Resource_Coordinator, Project_Name FROM projects";
$project = $db->fetchAll($sql);
//$project = caspioSelect("TR_Client_Projects", "Project_ID, From_Email, Resource_Coordinator, Project_Name", "", false);

$projectCount = sizeof($project);
//echo "Project Count: $projectCount<br>";

$sql = "SELECT StartDate, WO_ID, PayAmount, StartTime, WIN_NUM, PrimaryEmail, SiteName, SiteNumber,General10,Project_ID, TechCheckedIn_24hrs, City, State, Zipcode,SitePhone,Address"
. " FROM Tech_Called_MainSite"
. " WHERE StartDate = ? AND TechCheckedIn_24hrs = 0 AND Deactivated = 0 AND Project_ID != 0 AND TechMarkedComplete = 0 AND Approved = 0"
. " ORDER BY StartDate";

$records = $db->fetchAll($sql, array($tomorrow), Zend_Db::FETCH_NUM);
// Get workorders and tech info from a view called "Tech_Called_MainSite
//$records = caspioSelectView("Tech_Called_MainSite", "StartDate, WO_ID, PayAmount, StartTime, TB_UNID, PrimaryEmail, SiteName, SiteNumber,General10,Project_ID, TechCheckedIn_24hrs, City, State, Zipcode,SitePhone,Address", "StartDate = '$tomorrow' AND TechCheckedIn_24hrs='0' AND IsNull(Deactivated, '') = '' AND Project_ID!='0' AND TechMarkedComplete != '1' AND Approved != '1'", "StartDate", false);

// Counts # of records returned
$count = sizeof($records);
//echo "Record Count: $count<br><br>";

// Populate array with workorders
foreach ($records as $fields) {
	// Get data from array and set values for email
	$startDate = $fields[0];
	$startDate = date("m/d/Y", strtotime($startDate));
	$WO_ID = $fields[1];
	$PayAmount= $fields[2];
	$PayAmount = str_replace("$", "", $PayAmount);
	$StartTime = $fields[3];
	$TB_UNID = $fields[4];
	$sendToEmail = $fields[5];
	$siteName = $fields[6];
	$siteNumber = $fields[7];
	$General10 = $fields[8];
	$ProjectID = $fields[9];
	$checkedin = $fields[10];
	$City = $fields[11];
	$State = $fields[12];
	$Zipcode = $fields[13];
	$SitePhone = $fields[14];
	$Address = $fields[15];
	
	if (Core_Tech::isWMTech($TB_UNID)) continue;

	// if project from email is blank
	if ($SitePhone === null){
		$SitePhone = "none";
	}

	// Populate array with project info
foreach ($project as $order) {
	if ($order['Project_ID'] == $ProjectID){

//	$vFromEmail = $order['From_Email'];	// From Email address
	$vFromName = $order['Resource_Coordinator'];		// From Name
	$projectName = $order['Project_Name'];   // Project Name

    
	if ($siteName == ""){
		$vSubject = "REMINDER NOTICE Site: $siteNumber";
	}else{
		$vSubject = "REMINDER NOTICE Site: $siteName";
	}
	//13736
	$wosClass = new Core_Api_WosClass;    
	
	$result = $wosClass->getSystemGeneratedEmailAddress_ByWinNum($TB_UNID);
	$vFromEmail = $result["SystemGeneratedEmailFrom"];
	
	if (empty($vFromEmail)) {
		// Default mail values
		$vFromName = "Field Solutions";
		$vFromEmail = "Projects@fieldsolutions.com";
	}
    //end 13736        
    
	$eList = $sendToEmail;
	//$eList = "collin.mcgarry@fieldsolutions.com";

	@$caller = "getTechConfirmation";

	$vMessage = "Dear Field Solutions Technician,/r/r This is an auto generated note to remind you that are scheduled for work. Please see the details below and click the following link to confirm you will make this install. If you are not going to make this install please contact the person that recruited you for the work. /r/r THIS IS MANDATORY - You may be required to sign-in Click here: https://www.fieldsolutions.com/techs/TechCheckIn.php?TBID=$TB_UNID /r/r Work order number: $WO_ID/r Date of work: $startDate/r Start Time: $StartTime/r Site Name: $siteName/r Site Number: $siteNumber/r Site Phone: $SitePhone/r/r Address: $Address/r City: $City/r State: $State/r Zipcode: $Zipcode/r/r Amount of Pay: $ $PayAmount/r $General10/r/r Thank you for your support,/r Field Solutions Management";

	$html_message = "Dear Field Solutions Technician,<br /><p>This is an auto generated note to remind you that are scheduled for work. Please see the details below and click the following link to confirm you will make this install. If you are not going to make this install please contact the person that recruited you for the work.</p><p><a href='https://www.fieldsolutions.com/techs/TechCheckIn.php?TBID=$TB_UNID'>Click here to confirm (This Is Mandatory! - You may be required to sign-in)</a></p><p>Work order number: $WO_ID<br />Date of work: $startDate<br />Start Time: $StartTime</p><p>Site Name: $siteName<br />Site Number: $siteNumber<br />Site Phone: $SitePhone<br><br>Address: $Address<br />City: $City<br />State: $State<br />Zipcode: $Zipcode</p><p>Amount of Pay: $ $PayAmount<br /><br />$General10</p><p>Thank you for your support,<br />Field Solutions Management</p>";

	// Emails Tech
	smtpMail($vFromName, $vFromEmail, $eList, $vSubject, $vMessage, $html_message, $caller);

   }
  }
 }
