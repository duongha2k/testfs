<?php
	require_once dirname(__FILE__) . '/bootstrap.php';
	require_once(realpath(dirname(__FILE__) . "/../") . "/library/smtpMail.php");

	set_time_limit (1800);

	$db					= Zend_Registry::get('DB');
	$client_defaults	= array ();
	$prj_defaults		= array ();
	$prj_link			= "http://www.fieldsolutions.com/clients/projectsView_new.php";
	$prj_live			= array ();
	$prj_mon			= array ();
	$prj_notify			= array ();
	$wo_link			= "http://www.fieldsolutions.com/clients/wosDetails.php";
	$wo_live			= array ();
	$wo_mon				= array ();
	$wo_notify			= array ();
	$email_to			= "jflaherty@fieldsolutions.com,gbailey@fieldsolutions.com,tngo@fieldsolutions.com";
	$email_error_to		= "development@fieldsolutions.com";
	$email_from			= "no-replies@fieldsolutions.com";
	$message			= "";

	try {
		//---------------------------------------------------------------------
		//Query client defaults, current project settings and previous settings
		$query = $db->select();
		$query->from (Core_Database::TABLE_CLIENTS_TECH_DEDUCT);
		$rs_client_defaults = Core_Database::fetchAll ($query);

		$query = $db->select();
		$query->from (Core_Database::TABLE_PROJECTS, array(
			"Project_ID as id", "Project_Company_ID as Company_ID", 
			"Project_Name", "PcntDeduct", "PcntDeductPercent"
		));
        $query->where ("Active = 1");
		$rs_prj_live = Core_Database::fetchAll ($query);

		$query = $db->select();
		$query->from ("deducts_monitor_project", array(
			"project_id as id", "PcntDeduct", "PcntDeductPercent"
		));
		$rs_prj_mon = Core_Database::fetchAll ($query);

		//---------------------------------------------------------------------
		//Query current work order settings and previous settings
		$query = $db->select();
		$query->from (Core_Database::TABLE_WORK_ORDERS, array(
			"WIN_NUM AS id", "Project_ID AS project_id", 
			"Company_ID AS company_id", "Headline", 
			"PcntDeduct", "PcntDeductPercent"
		));
        $query->where ("Deactivated = 0");
        $query->where ("Status <> 'completed'");
        $rs_wo_live = Core_Database::fetchAll ($query);

		$query = $db->select();
		$query->from ("deducts_monitor_wo", array(
			"WIN_NUM as id", "PcntDeduct", "PcntDeductPercent"
		));
		$rs_wo_mon = Core_Database::fetchAll ($query);

		//---------------------------------------------------------------------
		//Store client defaults by company ID so that we can check settings of
		//newly created projects.
		foreach ($rs_client_defaults as $record) {
			$client_defaults[$record["Company_ID"]] = $record[PcntDeductPercent];
		}

		//Store previously "monitored" settings by project ID
		foreach ($rs_prj_mon as $record) {
			$prj_mon[$record["id"]] = array (
				"id" => $record["id"],
				"PcntDeduct" => $record["PcntDeduct"],
				"PcntDeductPercent" => $record["PcntDeductPercent"]
			);
		}

		//Store current settings by project ID
        foreach ($rs_prj_live as $record) {
			$temp = array (
				"id" => $record["id"],
				"company_id" => $record["Company_ID"],
				"title" => $record["Project_Name"],
				"PcntDeduct" => $record["PcntDeduct"],
				"PcntDeductPercent" => $record["PcntDeductPercent"]
			);
			$prj_live[$record["id"]] = $temp;
			$prj_defaults[$record["id"]] = $temp;
        }

		//---------------------------------------------------------------------
        //Store previously "monitored" settings by WIN#
		foreach ($rs_wo_mon as $record) {
			$wo_mon[$record["id"]] = array (
				"id" => $record["id"],
				"PcntDeduct" => $record["PcntDeduct"],
				"PcntDeductPercent" => $record["PcntDeductPercent"]
			);
		}

		//Store current settings by WIN#
        foreach ($rs_wo_live as $record) {
			$wo_live[$record["id"]] = array (
				"id" => $record["id"],
				"project_id" => $record["project_id"],
				"title" => $record["Headline"],
				"PcntDeduct" => $record["PcntDeduct"],
				"PcntDeductPercent" => $record["PcntDeductPercent"]
			);
        }

        //---------------------------------------------------------------------
        //Check each old project settings against the current settings, 
        //removing each once it's been checked.  Add to notify list if changed.
        //Delete record if the project no longer exists.
		foreach ($prj_mon as $id => $record) {
			if (isset ($prj_live[$id])) {
				if ($prj_live[$id]["PcntDeduct"] != $record["PcntDeduct"] || 
					$prj_live[$id]["PcntDeductPercent"] != $record["PcntDeductPercent"]) {
					//Setting has been changed, add to notify list.
					$prj_notify[$id] = $prj_live[$id];

					//Update monitored setting in datebase.
					$db->update ("deducts_monitor_project", 
						array(
							"PcntDeduct" => $prj_live[$id]["PcntDeduct"],
							"PcntDeductPercent" => $prj_live[$id]["PcntDeductPercent"]
						),
						$db->quoteInto ("project_id = ?", $id)
					);
				}
			}
			else {
				//Project no longer exists or is not active, so delete.
				$db->delete ("deducts_monitor_project", 
					$db->quoteInto ("project_id = ?", $id));
			}

			//Remove from current and previous lists once checked.
			unset ($prj_live[$id]);
			unset ($prj_mon[$id]);
		}

		//---------------------------------------------------------------------
		//Anything left in the projects list at this point is not being
		//monitored.  Add the current settings to the database and, if the
		//value is not the same as the current default in client settings,
		//add to notify list.
		foreach ($prj_live as $id => $record) {
			$db->insert ("deducts_monitor_project", array (
				"project_id" => $id,
				"PcntDeduct" => $record["PcntDeduct"],
				"PcntDeductPercent" => $record["PcntDeductPercent"]
			));

			$default = $client_defaults[$record["company_id"]];

			if ($record["PcntDeductPercent"] != $default) {
				$prj_notify[$id] = $record;
			}
		}

        //---------------------------------------------------------------------
        //Check each old work order settings against the current settings, 
        //removing each once it's been checked.  Add to notify list if changed.
        //Delete record if the project no longer exists.
		foreach ($wo_mon as $id => $record) {
			if (isset ($wo_live[$id])) {
				if ($wo_live[$id]["PcntDeduct"] != $record["PcntDeduct"] || 
					$wo_live[$id]["PcntDeductPercent"] != $record["PcntDeductPercent"]) {
					//Setting has been changed, add to notify list.
					$wo_notify[$id] = $wo_live[$id];

					//Update monitored setting in datebase.
					$db->update ("deducts_monitor_wo", 
						array(
							"PcntDeduct" => $wo_live[$id]["PcntDeduct"],
							"PcntDeductPercent" => $wo_live[$id]["PcntDeductPercent"]
						),
						$db->quoteInto ("WIN_NUM = ?", $id)
					);
				}
			}
			else {
				//Project no longer exists or is not active, so delete.
				$db->delete ("deducts_monitor_wo", 
					$db->quoteInto ("WIN_NUM = ?", $id));
			}

			//Remove from current and previous lists once checked.
			unset ($wo_live[$id]);
			unset ($wo_mon[$id]);
		}

		//---------------------------------------------------------------------
		//Anything left in the work orders list at this point is not being
		//monitored.  Add the current settings to the database and, if the
		//value is not the same as the current default in client settings,
		//add to notify list.
		foreach ($wo_live as $id => $record) {
			$db->insert ("deducts_monitor_wo", array (
				"WIN_NUM" => $id,
				"PcntDeduct" => $record["PcntDeduct"],
				"PcntDeductPercent" => $record["PcntDeductPercent"]
			));

			$default = $prj_defaults[$record["project_id"]];

			if ($record["PcntDeduct"] != $default["PcntDeduct"] ||
				$record["PcntDeductPercent"] != $default["PcntDeductPercent"]) {
				$wo_notify[$id] = $record;
			}
		}

		//---------------------------------------------------------------------
		//Build and send e-mail message if any changes were detected.  Should
		//not e-mail on first run (no huge e-mails).
		if ((count ($prj_notify) > 0 || count ($wo_notify) > 0) && count ($wo_notify) < 5000) {
			$message  = "This is a notification that the following percent deduct ";
			$message .= "settings have been changed.<br/><br/>";

			if (count ($prj_notify) > 0) {
				$message .= "Projects:<br/><table border='1' cellspacing='0' cellpadding='3'>";
				$message .= "<tr valign='top' style='font-weight: bold;'><td>ID</td><td>Project Name</td><td>Deduct Value</td><td>Deduct %</td>";

				foreach ($prj_notify as $record) {
					$message .= "<tr valign='top'><td>{$record["id"]}</td>";
					$message .= "<td><a href='$prj_link?v={$record["company_id"]}&project_id={$record["id"]}'>{$record["title"]}</a></td>";
					$message .= "<td>{$record["PcntDeduct"]}</td>";
					$message .= "<td>" . ($record["PcntDeductPercent"] * 100.0) . "%</td></tr>";
				}

				$message .= "</table><br/><br/>";
			}

			if (count ($wo_notify) > 0) {
				$message .= "Work Orders:<br/><table border='1' cellspacing='0' cellpadding='3'>";
				$message .= "<tr valign='top' style='font-weight: bold;'><td>ID</td><td>Headline</td><td>Deduct Value</td><td>Deduct %</td>";

				foreach ($wo_notify as $record) {
					$message .= "<tr valign='top'><td>{$record["id"]}</td>";
					$message .= "<td><a href='$wo_link?v={$record["company_id"]}&id={$record["id"]}'>{$record["title"]}</a></td>";
					$message .= "<td>{$record["PcntDeduct"]}</td>";
					$message .= "<td>" . ($record["PcntDeductPercent"] * 100.0) . "%</td></tr>";
				}

				$message .= "</table><br/>";
			}

			smtpMail (
				"Percent Deduct Change Notification Script", 
				$email_from, $email_to, 
				"Percent Deduct Change Notifications", 
				$message,
				$message
			);
		}
	}
	catch (SoapFault $fault) {
		smtpMail (
			"Percent Deduct Change Notification Script", 
			$email_from, $email_error_to, 
			"Percent Deduct Change Notification Script Error", 
			$fault, 
			$fault, 
			"prctdeduct_notify.php"
		);
	}
?>