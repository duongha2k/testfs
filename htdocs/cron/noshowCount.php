<?php
	require_once dirname(__FILE__) . '/bootstrap.php';
	$db = Zend_Registry::get('DB');

	try {
		require_once(realpath(dirname(__FILE__) . "/../") . "/library/caspioAPI.php");
		require_once(realpath(dirname(__FILE__) . "/../") . "/library/smtpMail.php");

		$sql = "SELECT NoShow_Tech, Count(NoShow_Tech) AS cnt FROM work_orders WHERE NoShow_Tech <> '' GROUP BY NoShow_Tech ORDER BY NoShow_Tech ASC";
		$mainNoShowsCount = $db->fetchAll($sql);

		//$mainNoShowsCount = caspioSelect("Work_Orders", "NoShow_Tech, Count(NoShow_Tech)", "NoShow_Tech <> '' GROUP BY NoShow_Tech", "NoShow_Tech ASC", false);
//		$flsNoShowsCount = caspioSelect("FLS_Work_Orders", "NoShow_Tech, Count(NoShow_Tech)", "NoShow_Tech <> '' GROUP BY NoShow_Tech", "NoShow_Tech ASC", false);

		$countByID = array();
		foreach ($mainNoShowsCount as $nCnt) {
			// combining counts from fls and main site work orders
			$id = $nCnt['NoShow_Tech'];
			$count = $nCnt['cnt'];
			$countByID[$id] = $count;
		}
/*		foreach ($flsNoShowsCount as $nCnt) {
			// combining counts from fls and main site work orders
			$pair = explode(",", $nCnt);
			$id = $pair[0];
			$count = $pair[1];
			if (!isset($countByID[$id]))
				$countByID[$id] = $count;
			else
				$countByID[$id] += $count;
		}*/

		$IDsByCount = array();
		foreach ($countByID as $id => $count) {
			// group NoShow_Tech by number of no shows
			if (!isset($IDsByCount[$count]))
				$IDsByCount["$count"] = $id;
			else
				$IDsByCount["$count"] .= "," . $id;
		}

		// clears no shows
		caspioUpdate("TR_Master_List", "No_Shows", "'0'", "", false);
		$db->update(Core_Database::TABLE_TECH_BANK_INFO, array('No_Shows' => 0));

		foreach ($IDsByCount as $count => $ids) {
			// update tech's record with no show count
			caspioUpdate("TR_Master_List", "No_Shows", "'$count'", "TechID IN ($ids)", false);
			$db->update(Core_Database::TABLE_TECH_BANK_INFO, array('No_Shows' => $count), "TechID IN ($ids)");
		}
	}
	catch (SoapFault $fault) {
		smtpMail("NoShow Count Script", "nobody@fieldsolutions.com", "codem01@gmail.com,gerald.bailey@fieldsolutions.com", "NoShow Count Script Error", "$fault", "$fault", "noshowCount.php");
	}
?>
