<?php
require_once dirname(__FILE__) . '/bootstrap.php';
$db = Zend_Registry::get('DB');

// Main Site
// Description: This script retrieves all WOs that have not been closed 24 hours after project date and emails the tech.
// Created on: 12/06/07
// Revision History:
//

require_once(realpath(dirname(__FILE__) . "/../") . "/library/smtpMail.php");

// Set Variables
$yesterday = date('Y-m-d', strtotime('-1 days'));//look back 24 hours
$result = "Success"; //Value required by our library, use of try should negate this
$calledFrom = (empty($_SERVER['HTTP_REFERER']) ? "Unknown" : $_SERVER['HTTP_REFERER']);// library needs this


// Get Projects Info
$sql = "SELECT Project_ID, From_Email, Resource_Coordinator, Project_Name FROM projects";
$project = $db->fetchAll($sql);
//$project = caspioSelect("TR_Client_Projects", "Project_ID, From_Email, Resource_Coordinator, Project_Name", "", false);

$projectCount = sizeof($project);
//echo "Project Count: $projectCount<br>";

// Get workorders and tech info from a view called "Tech_Called_MainSite

$startDateField = "CAST(CONCAT(`v`.StartDate, ' ', STR_TO_DATE(`v`.StartTime, '%l:%i %p')) AS DATETIME)";
$endDateField = "CAST(CONCAT(`v`.EndDate, ' ', STR_TO_DATE(`v`.EndTime, '%l:%i %p')) AS DATETIME)";
$dateField = "(CASE WHEN `v`.EndDate IS NULL THEN $startDateField ELSE $endDateField END)";
$sql = "SELECT DATE_FORMAT(MAX($dateField), '%m/%d/%Y') AS Date, `w`.WO_ID, PayAmount, DATE_FORMAT(MAX($dateField), '%h:%i %p') AS Time, `w`.WIN_NUM, `t`.PrimaryEmail, SiteName, SiteNumber, TechMarkedComplete, Project_ID, `t`.FirstName, `t`.LastName, Company_Name"
. " FROM work_orders AS `w` JOIN tech_id_mapping AS `m` ON `w`.Tech_ID = `m`.wo_tech_id JOIN TechBankInfo AS `t` ON `m`.tbi_id = `t`.id JOIN work_order_visits AS `v` ON `w`.WIN_NUM = `v`.WIN_NUM"
. " WHERE `v`.VisitDisable = 0 AND `w`.Deactivated = 0 AND TechMarkedComplete = 0 AND ShowTechs = 1 AND Status NOT IN ('approved', 'in accounting', 'completed') GROUP BY `w`.WIN_NUM HAVING MAX($dateField) <= ?"
. " ORDER BY Date";

$records = $db->fetchAll($sql, array($yesterday), Zend_Db::FETCH_NUM);
//$records = caspioSelectView("Tech_Called_MainSite", "StartDate, WO_ID, PayAmount, StartTime, TB_UNID, PrimaryEmail, SiteName, SiteNumber, TechMarkedComplete, Project_ID, Tech_FName, Tech_LName, Company_ID", "StartDate <= '$yesterday' AND IsNull(Deactivated, '') = '' AND TechMarkedComplete != '1' AND ShowTechs != '0'", "StartDate", false);

// Populate array with workorders
foreach ($records as $fields) {
	// Get data from array and set values for email
	$startDate = $fields[0];
	$startDate = date("m/d/Y", strtotime($startDate));
	$WO_ID = $fields[1];
	$PayAmount= $fields[2];
	$PayAmount = str_replace("$", "", $PayAmount);
	$StartTime = $fields[3];
	$TB_UNID = $fields[4];
	$sendToEmail = $fields[5];
	$siteName = $fields[6];
	$siteNumber = $fields[7];
	$TechMarkedComplete = $fields[8];
	$ProjectID = $fields[9];
	$FirstName = $fields[10];
	$LastName = $fields[11];
	$Company_Name = $fields[12];

	if (Core_Tech::isWMTech($TB_UNID)) continue;
	
// Default mail values
$vFromName = "Field Solutions";
$vFromEmail = "Projects@fieldsolutions.com";

	// Populate array with project info
foreach ($project as $order) {
	if ($order['Project_ID'] == $ProjectID){

	$vFromEmail = $order['From_Email'];	// From Email address
	$vFromName = $order['Resource_Coordinator'];		// From Name
	$projectName = $order['Project_Name'];   // Project Name

	$wosClass = new Core_Api_WosClass;
	$result = $wosClass->getSystemGeneratedEmailAddress_ByWinNum($TB_UNID);
	$vFromEmail = $result["SystemGeneratedEmailFrom"];

	// use if project send to email is blank
	if ($vFromEmail == ""){
		$vFromEmail = "Projects@fieldsolutions.com";
	}


	if ($siteName == ""){
		$vSubject = "REMINDER NOTICE Site: $siteNumber";
	}else{
		$vSubject = "REMINDER NOTICE Site: $siteName";
	}

	$eList = $sendToEmail;
	//$eList = "collin.mcgarry@fieldsolutions.com";

	@$caller = "tech_NotComplete";

	$message = "
Dear $FirstName $LastName,

This is an auto generated note to remind you that 24 hours
has passed since finishing your project. You will need to
close this work order on the main site so as to get paid. Please follow the
link below to complete your work order.

<a href='https://www.fieldsolutions.com/techs/wosDetails.php?id=$TB_UNID'>Click here to confirm (You may be required to sign-in)</a>

Work order number: $WO_ID
Client: $Company_Name
Pay Amount: $$PayAmount
Date of work: $startDate

Thank you for your support,
Field Solutions Management";

$htmlmessage = nl2br($message);

// Emails Tech
smtpMailLogReceived($vFromName, $vFromEmail, $eList, $vSubject, $message, $htmlmessage, $caller);
//smtpMailLogReceived($vFromName, $vFromEmail, "gbailey@fieldsolutions.com", $vSubject, $message, $htmlmessage, $caller);
   }
  }
 }
