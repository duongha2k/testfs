<?php
	require_once dirname(__FILE__) . '/bootstrap.php';
	$db = Zend_Registry::get('DB');

	set_time_limit(600);

	try {
		require_once(realpath(dirname(__FILE__) . "/../") . "/library/caspioAPI.php");
		require_once(realpath(dirname(__FILE__) . "/../") . "/library/smtpMail.php");


		$cmd = "";
		$warning = "";
		if (!isset($_GET["cmd"])) {
			if (!isset($argv[1])) {
				//echo "Command missing";
				die();
			}
			else
				$cmd = $argv[1];
		}
		else
			$cmd = $_GET["cmd"];
			$yesterday = date('m-d-Y', strtotime('-1 days'));
			$sixWeeksAgo = date('m-d-Y', strtotime('-6 weeks'));

		switch ($cmd) {
			case "FLSIncomplete":
                        
                            
			@$caller = "wos_Incomplete_FLSIncomplete";

				// Get work orders
				$workOrder = caspioSelectAdv("FLS_Work_Orders", "WorkOrderNo, SiteName, Waybill, Address1, City, State, Zip, TB_ID, MisssingInfo, Project_ID, (SELECT PrimaryEmail FROM TR_Master_List WHERE TechID = TB_ID)", "PjctStartDate <= '$yesterday' AND PjctStartDate >= '$sixWeeksAgo' AND PayApprove = '0' AND Kickback = '0' AND Invoiced = '0' AND IsNull(Deactivated, '') = '' AND TechComplete != '1' AND CAST(MisssingInfo AS varchar) != ''", "", false, "'", "|^", false);
//print_r($workOrder);
			foreach ($workOrder as $order) {
				//$fields = getFields($order);
				$fields = explode("|^", $order);
				$companyID = trim($fields[0], "'"); // Remove quotes

				$WorkOrderNo = trim($fields[0], "'");
				$missingInfo = trim($fields[8], "'");
				$TB_UNID = trim($fields[7], "'");
				$eList = trim($fields[10], "'");
//				echo $eList . "\n";
				$ProjectNo = trim($fields[9], "'");
                                
				// Get Projects Info
				$project = caspioSelect("FLS_Projects", "ProjectNo,ProjectName,Coordinator,From_Email,Coordinator_Phone", "ProjectNo = '$ProjectNo'", false);
				foreach ($project as $proj) {
					$projectFields = getFields($proj);

					$projectID = trim($projectFields[0], "'");		// Project No
					$projectName = trim($projectFields[1], "'");
					$vFromName = trim(str_replace(".", "", $projectFields[2]), "'");
					
                                        //$vFromEmail = trim($projectFields[3], "'");
					
					$projectCCphone = trim($projectFields[4], "'");

					// if project from name is blank
					if ($vFromName == ""){
						$vFromName = "Field Solutions";
					}

					// if project from email is blank
					if ($vFromEmail == ""){
						//$vFromEmail = "no-replies@fieldsolutions.com";
                                                
					}
				}

				$vSubject = "Payment Delayed - Work Order# $WorkOrderNo not updated on FS website.";

				$vMessage = "Dear FS Technician,/r/r Work Order No: $WorkOrderNo needs to be updated on Field Solutions website.  Payment cannot be approved until these steps are taken./r/r Comments: $missingInfo/r/r Follow this link to update the work order: https://www.fieldsolutions.com/techs/wosDetailsNonTab.php?id=$TB_UNID/r/r Coordinator: $vFromName/r Email: $vFromEmail/r Phone: $projectCCphone/r/r Thank you./r/r Regards,/r/r Field Solutions/r Web: www.fieldsolutions.com";

				$html_message = "<p>Dear FS Technician,</p><p>Work Order No: $WorkOrderNo needs to be updated on Field Solutions website.  Payment cannot be approved until these steps are taken.</p><p>Missing Info / Comments: $missingInfo</p><p><a href='https://www.fieldsolutions.com/techs/wosDetailsNonTab.php?id=$TB_UNID'>Click here to update the workorder</a></p><p>Coordinator: $vFromName<br>Email: $vFromEmail<br>Phone: $projectCCphone</p><p>Thank you.</p><p>Regards,</p><p>Field Solutions<br>Web: <a href='http://www.fieldsolutions.com'>www.fieldsolutions.com</a></p>";

//$eList = "gbailey@fieldsolutions.com";

				// Emails Tech
				if ($eList != '') {
					smtpMail($vFromName, $vFromEmail, $eList, $vSubject, $vMessage, $html_message, $caller);
				}
			}
			break;

			case "MainSiteIncomplete":
				//736
				$wosClass = new Core_Api_WosClass;    
				//End 736

				@$caller = "wos_Incomplete_MainSiteIncomplete";

				$sql = "SELECT WO_ID, SiteName, Address, City, State, Zipcode, WIN_NUM, MissingComments, Project_ID, Tech_ID, Approved FROM work_orders WHERE StartDate <= ? AND Deactivated = 0 AND TechMarkedComplete = 0 AND MissingComments IS NOT NULL AND MissingComments <> '' AND Approved = 0";
				$workOrder = $db->fetchAll($sql, array(date('Y-m-d', strtotime('-1 days'))));

				$techIds = array();
				foreach ($workOrder as $order) {
					if ($order['Tech_ID']) $techIds[] = $order['Tech_ID'];
				}
				$techEmails = array();
				if (count($techIds) > 0) {
					$result = caspioSelectAdv('TR_Master_List', 'TechId,PrimaryEmail', 'TechID IN (' . implode(',', $techIds) . ')', "", false, "'", "|^", false);
					foreach ($result as $row) {
						$fields = explode("|^", $row);
						$techEmails[trim($fields[0], "'")] = trim($fields[1], "'");
					}
				}

				// Get work orders
				//$workOrder = caspioSelectAdv("Work_Orders", "WO_ID, SiteName, Address, City, State, Zipcode, TB_UNID, MissingComments, Project_ID, (SELECT PrimaryEmail FROM TR_Master_List WHERE TechID = Tech_ID)", "StartDate <= '$yesterday' AND IsNull(Deactivated, '') = '' AND TechMarkedComplete != '1' AND CAST(MissingComments AS varchar) != ''", "", false, "'", "|^", false);

			foreach ($workOrder as $order) {
				if (!$order['Tech_ID'] || !isset($techEmails[$order['Tech_ID']])) continue;
				//$fields = getFields($order);
				$fields = explode("|^", $order);

				$WorkOrderNo = $order['WO_ID'];
				//$WorkOrderNo = trim($fields[0], "'");
				$missingInfo = $order['MissingComments'];
				//$missingInfo = trim($fields[7], "'");
				$TB_UNID = $order['WIN_NUM'];
				//$TB_UNID = trim($fields[6], "'");
				$eList = $techEmails[$order['Tech_ID']];
				//$eList = trim($fields[9], "'");
//				echo $eList . "\n";
				$ProjectNo = $order['Project_ID'];
				//$ProjectNo = trim($fields[8], "'");

				//736
				$result = $wosClass->getSystemGeneratedEmailAddress_ByWinNum($TB_UNID);
				$vFromEmail = $result["SystemGeneratedEmailFrom"];
				//End 736
				
				// Get Projects Info
				$sql = "SELECT Project_ID,Project_Name,Resource_Coordinator,From_Email,Resource_Coord_Phone FROM projects WHERE Project_ID=?";
				$project = $db->fetchRow($sql, array($ProjectNo));
				//$project = caspioSelect("TR_Client_Projects", "Project_ID,Project_Name,Resource_Coordinator,From_Email,Resource_Coord_Phone", "Project_ID = '$ProjectNo'", false);

				$projectID = $project['Project_ID'];		// Project No
				$projectName = $project['Project_Name'];
				$vFromName = str_replace(".", "", $project['Resource_Coordinator']);
				$projectCCphone = $projectFields['Resource_Coord_Phone'];

				// if project from name is blank
				if ($vFromName == ""){
					$vFromName = "Field Solutions";
				}

				// if project from email is blank
				if ($vFromEmail == ""){
					$vFromEmail = "no-replies@fieldsolutions.com";					
				}

				$vSubject = "WIN#: $TB_UNID Payment Delayed - Work Order# $WorkOrderNo not updated on FS website.";

				$vMessage = "Dear FS Technician,/r/r Work Order No: $WorkOrderNo needs to be updated on Field Solutions website.  Payment cannot be approved until these steps are taken./r/r Comments: $missingInfo/r/r Follow this link to update the work order: https://www.fieldsolutions.com/techs/wosDetailsNonTab.php?id=$TB_UNID/r/r Coordinator: $vFromName/r Email: $vFromEmail/r Phone: $projectCCphone/r/r Thank you./r/r Regards,/r/r Field Solutions/r Web: www.fieldsolutions.com";

				$html_message = "<p>Dear FS Technician,</p><p>Work Order No: $WorkOrderNo needs to be updated on Field Solutions website.  Payment cannot be approved until these steps are taken.</p><p>Missing Info / Comments: $missingInfo</p><p><a href='https://www.fieldsolutions.com/techs/wosDetailsNonTab.php?id=$TB_UNID'>Click here to update the workorder</a></p><p>Coordinator: $vFromName<br>Email: $vFromEmail<br>Phone: $projectCCphone</p><p>Thank you.</p><p>Regards,</p><p>Field Solutions<br>Web: <a href='http://www.fieldsolutions.com'>www.fieldsolutions.com</a></p>";

//$eList = "gbailey@fieldsolutions.com";

				// Emails Tech
				if ($eList != '') {
					smtpMail($vFromName, $vFromEmail, $eList, $vSubject, $vMessage, $html_message, $caller);
				}
			}
			break;

			default:
//				echo "Unknown command.";
				reportError("wos_Incomplete Script", "wos_Incomplete Error - cmd is " . $cmd, "Unknown Command", "wos_Incomplete.php?cmd=" . $cmd);
				die();
				break;
		}
	}
	catch (SoapFault $fault) {
		reportError("wos_Incomplete Script", "wos_Incomplete Error - cmd is " . $cmd, "$fault", "wos_Incomplete.php?cmd=" . $cmd);
	}
	if ($warning != "")
		reportError("wos_Incomplete Script", "wos_Incomplete Warning - FLSID or Tech ID not found or email blank", $warning, "wos_Incomplete - FLS and Main Site Incomplete Orders");
?>