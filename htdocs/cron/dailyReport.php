<?php
require_once(dirname(__FILE__) . "/../library/bootstrapAPI.php");
$date = new Zend_Date();
$date->add(1, Zend_Date::DAY);
$tomorrow = $date->toString("yyyy-MM-dd");
$tomorrowFormat = $date->toString("MM/dd/YYYY");
$date->add(4, Zend_Date::DAY);
$endDate = $date->toString("yyyy-MM-dd");
$endDateFormat = $date->toString("MM/dd/YYYY");
$uri = "http://www.fieldsolutions.com/reports/?renderMode=REPORT&Project=0&StartDateMoreOrEqual=$tomorrow&StartDateLessOrEqual=$endDate&output-target=table%2Fexcel%3Bpage-mode%3Dflow&accepted-page=0&autoSubmit=false&showParameters=true&ignoreDefaultDates=true&name=DailyReport.prpt&autoreport=r3p0rtaUt0";
$config = array(
	'adapter'   => 'Zend_Http_Client_Adapter_Curl',
	'curloptions' => array(CURLOPT_FOLLOWLOCATION => true),
);
$client = new Zend_Http_Client($uri, $config);
$response = $client->request();

$mail = new Zend_Mail();
$at = $mail->createAttachment($response->getBody());
$at->filename = 'dailyReport_' . $tomorrow . '.xls';
$mail->setBodyText("Work orders starting $tomorrowFormat thru $endDateFormat are attached. This is an automated email.");
$mail->setFrom('no-replies@fieldsolutions.com', '');
//$mail->addTo('tngo@fieldsolutions.com', 'Trung Ngo');
$mail->addTo('dailyreport@fieldsolutions.com', '');
$mail->addBcc('tngo@fieldsolutions.com', 'Trung Ngo');
//$mail->addBcc('gbailey@fieldsolutions.com', 'Gerald Bailey');
$mail->setSubject("FieldSolutions Scheduled Work for $tomorrowFormat");
$mail->send();
