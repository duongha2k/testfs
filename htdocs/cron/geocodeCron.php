<?php
	require_once dirname(__FILE__) . '/bootstrap.php';
	$db = Zend_Registry::get('DB');

	//	ini_set("display_errors", 1);
	require_once(realpath(dirname(__FILE__) . "/../") . "/library/caspioAPI.php");
	require_once(realpath(dirname(__FILE__) . "/../") . "/library/smtpMail.php");
	require_once(realpath(dirname(__FILE__) . "/../") . "/library/mySQL.php");

	$cfgSite = Zend_Registry::get('CFG_SITE');

    $googleMapApiKey = $cfgSite->get("site")->get("google_map_key");
    $googleMapClientId = $cfgSite->get("site")->get("google_map_client_id");

	function geocodeAddress($address) {
		global $googleMapApiKey, $googleMapClientId;
		// create a new cURL resource
		$ch = curl_init();
		$address = urlencode($address);
		// set URL and other appropriate options
		$options = array(CURLOPT_URL => "http://maps.google.com/maps/geo?q=$address&output=csv&client=$googleMapClientId&sensor=false",
						 CURLOPT_HEADER => false,
						 CURLOPT_RETURNTRANSFER => true
						);

		curl_setopt_array($ch, $options);

		// grab URL and pass it to the browser
		$response = curl_exec($ch);

		// close cURL resource, and free up system resources
		curl_close($ch);

		if ($response) {
			$info = explode(",", $response);
			if ($info[0] == "200") {
				$lat = $info[2];
				$long = $info[3];
				return array($lat, $long);
			}
		}
		return false;
	}

	$sql = "SELECT z.Latitude AS zLatitude, z.Longitude AS zLongitude, wo.WIN_NUM, wo.Latitude, wo.Longitude, wo.State, wo.Zipcode, wo.Address, wo.Company_ID, wo.Country"
		. " FROM work_orders AS wo"
		. " LEFT JOIN zipcodes AS z ON wo.Zipcode = z.ZIPCode"
		. " WHERE wo.ShowTechs = 1 AND wo.Deactivated = 0 AND wo.Zipcode IS NOT NULL AND wo.Zipcode <> '' AND IFNULL(wo.Tech_ID,'') = ''"
                . " AND (wo.Latitude IS NULL OR wo.Longitude IS NULL OR ABS(z.Latitude - wo.Latitude) >= 1 OR ABS(z.Longitude - wo.Longitude) >= 1)";

	//$diffLat = "ABS((SELECT TOP 1 USA_ZIPCodes.Latitude FROM USA_ZIPCodes WHERE USA_ZIPCodes.ZIPCode = Work_Orders.Zipcode) - Latitude)";
	//$diffLong = "ABS((SELECT TOP 1 USA_ZIPCodes.Longitude FROM USA_ZIPCodes WHERE USA_ZIPCodes.ZIPCode = Work_Orders.Zipcode) - Longitude)";

	//$woList = caspioSelectAdv("Work_Orders", "(SELECT TOP 1 USA_ZIPCodes.Latitude FROM USA_ZIPCodes WHERE USA_ZIPCodes.ZIPCode = Work_Orders.Zipcode), (SELECT TOP 1 USA_ZIPCodes.Longitude FROM USA_ZIPCodes WHERE USA_ZIPCodes.ZIPCode = Work_Orders.Zipcode), TB_UNID, Latitude, Longitude, State, Zipcode, Address, Company_ID", "ShowTechs = '1' AND ISNULL(Zipcode,'') != '' AND Deactivated = '0' AND ISNULL(Tech_ID,'') = '' AND ((ISNULL(Latitude,'') = '' OR ISNULL(Longitude, '') = '') OR ($diffLat >= 1 OR $diffLong >= 1))","", false, "`", "|", false);
	$woList = $db->fetchAll($sql);

//	print_r($woList);

	foreach ($woList as $wo) {
		$newLat = $wo['zLatitude'];
		$newLong = $wo['zLongitude'];
		$unid = $wo['WIN_NUM'];
		$zipcode = $wo['Zipcode'];
		$address = $wo['Address'];
		$companyID = $wo['Company_ID'];
		$country = $wo['Country'];
		if (empty($country)) $country = 'US';
		if ($newLat === null || $newLong === null) {
			if (!is_numeric($zipcode) & strlen($zipcode) == 6) {
				$zipcode = strtoupper($zipcode);
				$zipcode = substr($zipcode, 0, 3) . " " . substr($zipcode, 3, 3);
			}
			$location = "$address $zipcode $country";
			$coord = geocodeAddress($location);
			$newLat = $coord[0];
			$newLong = $coord[1];
			if ($country != 'US' || $newLat == "" || $newLong == "") {
				$location = "$zipcode $country";
				$coord = geocodeAddress($location);
				$newLat = $coord[0];
				$newLong = $coord[1];
			}
		}
		if ($newLat == "" || $newLong == "") {
			$lrDate = $db->fetchOne("SELECT lastNotified + INTERVAL 24 HOUR < NOW() FROM alreadyNotifiedGeocodeFail WHERE winNum = '$unid' LIMIT 0,1");
			if ($lrDate[0] != 1) continue;
			//echo "notifying ...\n";

			$insertQuery = "INSERT INTO alreadyNotifiedGeocodeFail (winNum, lastNotified) VALUES ('$unid', NOW()) ON DUPLICATE KEY UPDATE lastNotified = NOW()";
			$db->query($insertQuery);
			smtpMail("Geocode CRON", "no-replies@fieldsolutions.us", "projectmanagement@fieldsolutions.com, trcteam@fieldsolutions.com, tngo@fieldsolutions.com", "Unable to geocode win# $unid for company $companyID", "Unable to find a latitude / longitude for win# $unid for company $companyID", "Unable to find a latitude / longitude for win# $unid for company $companyID", "Geocode CRON");
			continue;
		}
		//echo "Update $newLat, $newLong WHERE TB_UNID = '$unid' {$wo['State']} $zipcode<br/>";
		$db->update('work_orders', array('Latitude' => $newLat, 'Longitude' => $newLong, 'Zipcode' => $zipcode), $db->quoteInto('WIN_NUM=?', $unid));
		//print_r(caspioUpdate("Work_Orders", "Latitude, Longitude, Zipcode", "'$newLat', '$newLong', '$zipcode'", "TB_UNID = '$unid'", false));
	}

?>