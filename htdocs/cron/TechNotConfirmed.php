<?php
require_once dirname(__FILE__) . '/bootstrap.php';
$db = Zend_Registry::get('DB');

// This script uses Caspio View to retrieve all work orders that have not been veified by techs in the 48 hour email notice
// An email is sent to the project coordinator 24 hours before project start date

require_once(realpath(dirname(__FILE__) . "/../") . "/library/smtpMail.php");

// Set Variables
$tomorrow = date('Y-m-d', strtotime('+1 days'));//look ahead 24 hours
$result = "Success"; //Value required by our library, use of try should negate this
$calledFrom = (empty($_SERVER['HTTP_REFERER']) ? "Unknown" : $_SERVER['HTTP_REFERER']);// library needs this


// Get Projects Info
$sql = "SELECT Project_ID, To_Email, Project_Name FROM projects";
$project = $db->fetchAll($sql);
//$project = caspioSelect("TR_Client_Projects", "Project_ID, To_Email, Project_Name", "", false);

$projectCount = sizeof($project);
//echo "Project Count: $projectCount<br>";

$sql = "SELECT StartDate, WIN_NUM, PayAmount, StartTime, WIN_NUM, PrimaryEmail, SiteName, SiteNumber,General10,Project_ID,TechCheckedIn_24hrs, PrimaryPhone, SecondaryPhone, Tech_FName, Tech_LName"
. " FROM Tech_Called_MainSite"
. " WHERE StartDate = ? AND TechCheckedIn_24hrs = 0 AND Deactivated = 0 AND Project_ID != 0 AND Approved = 0"
. " ORDER BY StartDate";

$records = $db->fetchAll($sql, array($tomorrow), Zend_Db::FETCH_NUM);
// Get workorders and tech info from a view called "Tech_Called_MainSite
//$records = caspioSelectView("Tech_Called_MainSite", "StartDate, WO_ID, PayAmount, StartTime, TB_UNID, PrimaryEmail, SiteName, SiteNumber,General10,Project_ID,TechCheckedIn_24hrs, PrimaryPhone, SecondaryPhone, Tech_FName, Tech_LName", "StartDate = '$tomorrow' AND TechCheckedIn_24hrs='0' AND IsNull(Deactivated, '') = '' AND Project_ID!='0'", "StartDate", false);

// Counts # of records returned
$count = sizeof($records);
//echo "Record Count: $count<br><br>";

// Populate array with workorders
foreach ($records as $fields) {
	// Get data from array and set values for email
	$startDate = $fields[0];
	$startDate = date("m/d/Y", strtotime($startDate));
	$WO_ID = $fields[1];
	$PayAmount= $fields[2];
	$PayAmount = str_replace("$", "", $PayAmount);
	$StartTime = $fields[3];
	$TB_UNID = $fields[4];
	$techEmail = $fields[5];
	$siteName = $fields[6];
	$siteNumber = $fields[7];
	$General10 = $fields[8];
	$ProjectID = $fields[9];
	$checkedin = $fields[10];
	$primaryPhone = $fields[11];
	$secondaryPhone = $fields[12];
	$techFname = $fields[13];
	$techLname = $fields[14];

	if (Core_Tech::isWMTech($TB_UNID)) continue;
	
	//echo "________________________________________<br>";
	//echo "<b>Work Order</b><br>";
	//echo "TB UNID: $TB_UNID<br>";
	//echo "Tech Checked In: $checkedin<br>";
	//echo "Tech Email: $techEmail<br>";
	//echo "Project ID: $ProjectID<br>";
	//echo "Start Date: $startDate<br>";
	//echo "WOID: $WO_ID<br><br>";

// Default mail values
$vFromName = "Field Solutions";
$vFromEmail = "Projects@fieldsolutions.com";

	$wo = new Core_WorkOrder();
	$wo->setData($WO_ID);
	$sysGenEmailTo = $wo->getSystemGeneratedEmailTo();
        $ApiWosClass = new Core_Api_WosClass();
        $FutureWOInfo = $ApiWosClass->getFutureWOInfo_ByWinNum($WO_ID);
        //$ApiWosClass->getFutureWOInfo($WorkOrderOwner);
        $SystemGeneratedEmailTo = $FutureWOInfo['SystemGeneratedEmailTo'];
	// Populate array with project info
foreach ($project as $order) {
	if ($order['Project_ID'] == $ProjectID){
            //13731
            if(!empty($sysGenEmailTo))
            {
                $sendToEmail=$sysGenEmailTo;
            }
            else if(!empty($SystemGeneratedEmailTo))
            {
                $sendToEmail=$SystemGeneratedEmailTo;
            }
            else
            {
                $sendToEmail=$order['To_Email'];
            }

	$projectID = $order['Project_ID']; 	// Project ID
	//$sendToEmail = $sysGenEmailTo != "" ? $sysGenEmailTo : $order['To_Email'];	// To Email address
	$projectName = $order['Project_Name'];   // Project Name


	//echo "<b>Project Info</b><br>";
	//echo "Project Name: $projectName<br>";
	//echo "Project ID: $projectID<br>";
	//echo "Project CC: $sendToEmail<br>";

	$vSubject = "Tech has not confirmed: $projectName, Site# $siteNumber";

	$eList = $sendToEmail;
	//$eList = "collin.mcgarry@fieldsolutions.com";
	//$eList = "collin.mcgarry@fieldsolutions.com,gerald.bailey@fieldsolutions.com";
	@$caller = "TechNotConfirmed";


	$vMessage = "Dear Resource Coordinator,/r/r This email is to inform you that the Technician assigned to your project has not confirmed./r/r <b>Project Information:</b>/r/r Project Name: $projectName/r Project ID: $projectID/r Work order number: $WO_ID/r Date of work: $startDate/r Start Time: $StartTime/r Site Name: $siteName/r Site Number: $siteNumber/r/r <b>Tech Contact Information:</b>/r/r Name: $techFname $techLname/r Email: $techEmail/r Primary Phone: $primaryPhone/r Secondary Phone: $secondaryPhone/r/r/r Thank you,/r Field Solutions Management";


	$html_message = "Dear Resource Coordinator,<br /><p>This email is to inform you that the Technician assigned to your project has not confirmed.</p><p><b>Project Information:</b></p><p>Project Name: $projectName<br>Project ID: $projectID<br>Work order number: $WO_ID<br />Date of work: $startDate<br />Start Time: $StartTime<br />Site Name: $siteName<br />Site Number: $siteNumber<br /></p><p><b>Tech Contact Information:</b></p><p>Name: $techFname $techLname<br>Email: $techEmail<br>Primary Phone: $primaryPhone<br>Secondary Phone: $secondaryPhone</p><br><p>Thank you,<br />Field Solutions Management</p>";

	// Emails Client
smtpMailLogReceived($vFromName, $vFromEmail, $eList, $vSubject, $vMessage, $html_message, $caller);
//smtpMailLogReceived($vFromName, $vFromEmail, "gbailey@fieldsolutions.com", $vSubject, $vMessage, $html_message, $caller);




$CCeList = "collin.mcgarry@fieldsolutions.com";
//smtpMail($vFromName, $vFromEmail, $CCeList, $vSubject, $vMessage, $html_message, $caller);
   }
  }
 }
