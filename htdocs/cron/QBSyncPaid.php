<?php
	require_once dirname(__FILE__) . '/bootstrap.php';
	$db = Zend_Registry::get('DB');

	// Call to Caspio WO tables with any recently paid WOs
	ini_set("display_errors", 1);
	$myPath = realpath(dirname(__FILE__) . "/../");
	require_once($myPath . "/library/caspioAPI.php");
	require_once($myPath . "/library/quickbooks.php");
	require_once($myPath . "/library/smtpMail.php");
	try {
//		require_once("library/caspioAPI.php");

		$paidWO = $db->fetchAll("SELECT caspio_unique_id, caspio_type, paid_amount, DATE_FORMAT(paid_date, '%Y-%m-%d'), check_number FROM quickbooks_bills WHERE is_paid = '1' AND caspio_sync = '0' AND last_update_by = 'QuickBooks'", array(), Zend_Db::FETCH_BOTH);

		$total_amount = 0;
		foreach ($paidWO as $row) {
			$total_amount += $row[2];
		}

		if (sizeof($paidWO) > 0) {
			copyPaidToCaspio($paidWO);

/*			if (!@mysql_ping ($link)) {
				@mysql_close($link);
				$link = mysql_connect($dbhost, $dbuser, $dbpass)
					or die('Could not connect: ' . mysql_error());
				mysql_select_db("quickbooks");
			}*/
			mysqlQuery("UPDATE quickbooks_bills SET last_update_by = 'Caspio_final', caspio_sync = '1' WHERE is_paid = '1' AND caspio_sync = '0' AND last_update_by = 'QuickBooks'");
		}
		$message = "Paid WOs Synced back to Caspio -\n";
		$message .= "\tTotal Amount: $total_amount\n";
		$message .= "\tTotal Number WOs: " . sizeof($paidWO) . "\n\n";

		// find zero pay amounts main site
		$sql = "SELECT WIN_NUM FROM work_orders WHERE Deactivated = 0 AND MovedToMysql = 0 AND Invoiced = 1 AND TechPaid = 0 AND (Net_Pay_Amount = 0.00 OR Net_Pay_Amount IS NULL)";
		$zeroMain = $db->fetchCol($sql);
		//$zeroMain = caspioSelect("Work_Orders", "WO_ID", "Deactivated = '0' AND MovedToMysql = '0' AND Invoiced = '1' AND TechPaid = '0' AND CAST(Net_Pay_Amount AS MONEY) = 0.00", "", false);

		if (count($zeroMain)) {
			$zeroList = implode(",", $zeroMain);
			$message .= "These main site work orders with zero pay have been marked as paid - $zeroList\n\n";
			$db->update('work_orders', array('TechPaid' => 1, 'DatePaid' => new Zend_Db_Expr('NOW()'), 'Status' => 'completed'), $db->quoteInto('WIN_NUM IN (?)', $zeroMain));
			//caspioUpdate("Work_Orders", "TechPaid, DatePaid", "'1', GETDATE()", "WO_ID IN ($zeroList)", false);
		}

/*		$zeroFLS = caspioSelect("FLS_Work_Orders", "WorkOrderNo", "MovedToMysql = '0' AND Invoiced = '1' AND Paid = 'No' AND Deactivated != 'Yes' AND Kickback = '0' AND ISNUMERIC(PayAmt) = 1 AND CAST(PayAmt AS MONEY) = 0.00", "", false);

		if (sizeof($zeroFLS) != 1 || $zeroFLS[0] != '') {
			$zeroList = implode(",", $zeroFLS);
			$message .= "These FLS work orders with zero pay have been marked as paid - $zeroList\n\n";
			caspioUpdate("FLS_Work_Orders", "Paid, DatePaid", "'Yes', GETDATE()", "WorkOrderNo IN ($zeroList)", false);
		}*/

		$html = nl2br($message);
		smtpMail("Quickbooks Sync Paid", "nobody@fieldsolutions.com", "quickbooks@fieldsolutions.com", "Paid WO Sync Completed", $message, $html, "QBSyncPaid.php");
	}
	catch (SoapFault $fault) {
		//echo "Error while copying info to caspio: $fault";
		$message = "Error while copying paid WO to Caspio.\n\n";
		$message .= "Error received: $fault";
		$html = nl2br($message);
		smtpMail("Quickbooks Sync Paid", "nobody@fieldsolutions.com", "quickbooks@fieldsolutions.com", "Paid WO Sync Error", "", "Error while copying info to caspio: $fault", "QBSyncPaid.php");
	}
?>
