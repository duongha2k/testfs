
/* 
	IE7's JavaScript doesn't support Array.indexOf
	
	Here's a workaround:
	Source: http://developer.mozilla.org/en/docs/Core_JavaScript_1.5_Reference:Objects:Array:indexOf
*/
if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}




// project_selector_field_name = "select#InsertRecordProject_ID";
// default_amount_field_name = "input#InsertRecordPayAmt";

function getDefaultAmount(id) {
	index = projectIDs.indexOf(parseInt(id));
	amount = defaultAmounts[parseInt(index)];
	return amount;
}

//alert(getDefaultAmount(2));

function projectSelector() {
	return $(project_selector_field_name);
}

function defaultAmountField() {
	return $(default_amount_field_name);
}


$(document).ready(function() {
	//alert("test");
	projectSelector().change(function() {
		val = projectSelector().attr("value");
		
		//alert(val);
		//alert(getDefaultAmount(val));
		
		amt = getDefaultAmount(val);
		amt = parseFloat(amt);
		amt = amt.toFixed(2);
		
		def = defaultAmountField().attr("value");
		if (def > 0)
		{;}
		else 
		{
			defaultAmountField().attr("value", amt);
		}
		
	});

});

