<?php $page = admin; ?>
<?php $option = techs; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php require ("../library/caspioAPI.php"); ?>
<!-- Add Content Here -->

<?php
checkAdminLogin2();
/*if (parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) != "fieldsolutions.com") {
	echo "No External Access Allowed - click <a href='" . $_SERVER['PHP_SELF']. "'>here</a> to reload";
	die();
}*/
$projectID = $_POST["projectID"];
$importDate = $_POST["importDate"];

@$parts = explode("/", $importDate);
	
@$month = $parts[0];
@$date = $parts[1];
@$year = $parts[2];

$dateQuery = ($month != "" && $date != "" && $year != "" ? "AND DATEPART(mm, DateEntered) = '$month' AND DATEPART(dd, DateEntered) = '$date' AND DATEPART(yyyy, DateEntered) = '$year'" : "");

$techIDMap = array();

//$result = caspioSelect("Work_Orders_test", "Tech_ID, Tech_FName, Tech_LName, TechEmail, TechPhone", "ISNULL(Tech_ID, '') <> '' AND Project_ID = '$projectID' $dateQuery", "");

//$result = caspioSelectAdv("TR_Master_List", "FirstName, LastName, PrimaryEmail, PrimaryPhone", "TechID = '22110'", "", false, "`", "|");

//$result = caspioUpdate("Work_Orders_test", "Tech_FName, Tech_LName, TechEmail, TechPhone", "NULL, NULL, NULL, NULL", "ISNULL(Tech_ID, '') <> '' AND Project_ID = '$projectID' $dateQuery");

$result = caspioUpdate("Work_Orders", "Tech_FName, Tech_LName, TechEmail, TechPhone", "(SELECT TOP 1 FirstName FROM TR_Master_List WHERE TechID = Tech_ID), (SELECT TOP 1 LastName FROM TR_Master_List WHERE TechID = Tech_ID), (SELECT TOP 1 PrimaryEmail FROM TR_Master_List WHERE TechID = Tech_ID),(SELECT TOP 1 PrimaryPhone FROM TR_Master_List WHERE TechID = Tech_ID)", "Deactivated = '0' AND ISNULL(Tech_ID, '') <> '' AND Project_ID = '$projectID' $dateQuery");

?>

<br/><br/>
<div style="text-align: center">
<?php 
	echo "Work Order Tech Information Updated (Total: $result records).<br/>";
?>
<a href="projectWOSearch.php?t=UpdateInfo">New search</a>
</div>

<!--- End Content --->
<?php require ("../footer.php"); ?>
