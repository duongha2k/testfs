<?php
$siteTemplate   = ( isset($_SESSION['template']) )  ? $_SESSION['template'] : NULL;
$ISO_ID         = ( isset($_SESSION['ISO_ID']) )    ? $_SESSION['ISO_ID']   : NULL;
$UserType       = ( isset($_SESSION['UserType']) )  ? $_SESSION["UserType"] : NULL;

if ($loggedIn == "yes" && $loggedInAs == "tech") {
    require "navBar_techs.php";
}else{
    require "navBar_clients.php";
}
