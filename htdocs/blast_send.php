<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Email Blast</title>
</head>

<body>
<?php


require_once( "library/contactSettings.php" );
	$vars = (isset($_GET["AutoMailer"]) && $_GET["AutoMailer"] == "TRUE" ? $_GET : $_POST);
	$FLS_Request = (isset($vars["FLS_Request"]) && $vars["FLS_Request"] == "TRUE");
	$eFailed = false;
	
	// check for from name
	if(isset($vars['vFromName'])) {
		$vFromName = $_POST['vFromName'];
	}
	else  {
		$eFailed = true;
		echo "The from name was not supplied<br />";
	}
	
	// check for from email
	if(isset($vars['vFromEmail'])) {
		$vFromEmail = $_POST['vFromEmail'];
	}
	else {
		$eFailed = true;
		echo "The from email address was not supplied<br />";
	}
	
	// check for subject
	if(isset($vars['vSubject'])) {
		$vSubject = $_POST['vSubject'];
	}
	else {
		$eFailed = true;
		echo "The Subject was not supplied<br />";
	}
	
	// check for priority
	/*
	if(isset($_POST['vPriority'])) {
		$vPrioroty = $_POST['Priority'];
		
		if($vPriority == 1) {
			$vMS_Priority = "High";
		}
		else if (vPriority  == 3) {
			$vMS_Priority = "Normal";
		}
		else {
			$vMS_Priority = "Low";
		}
	}
	else {
		$eFailed = true;
		echo "Please select a priority";
	}
	*/

	// check for message
	if(isset($vars['vMessage'])) {
		$vMessage = $_POST['vMessage'];
	}
	else {
		$eFailed = true;
		echo "The Message was not supplied<br />";
	}
	
	// check form recipients
	if(isset($vars['eList'])) {
		$eList = $_POST['eList'];
		
		// vince debugging
        $contact = new ContactSettings();
        $doNotEmail = $contact->getEmailList();
        $eList = $contact->filterEmail($eList);
	}
	else {
		$eFailed = true;
		echo "There are no email recipients<br />";
	}
	
	if($eFailed == false) {
		$vMessage .= "<br><br><br><br>";
		if ($FLS_Request) {
			$vMessage .= "Note: If you have not yet been through online training to be activated as a Tech with the FLS program<br>";
			$vMessage .= "and/or do not have a Badge and IDs yet, please do not respond to this email, you are not eligible.<br>";
			$vMessage .= "To get yourself eligible, make sure that you have gone through training beginning, at the link below and<br>";
			$vMessage .= "that you have your Badge and IDs.<br>";
			$vMessage .= "<br>";
			$vMessage .= "<a href=\"http://www.flsupport.com/3.html\">FLS online training</a><br>";
			$vMessage .= "<br>";
			$vMessage .= "You are receiving this email as a registered technician on www.fieldsolutions.com.<br>";
			$vMessage .= "Click <a href=\"http://www.fieldsolutions.com/unsubscribe/\">here</a> to stop receiving future e-mails from us.<br>";
		}
		$html_message = str_replace(chr(13) . chr(10), '<br>', $vMessage);
		$text_message = strip_tags(str_replace("<br>", "\n", $vMessage));
		
		$boundary = md5(uniqid(rand(), true));
		$eol="\r\n";
		
		#common headers
		$headers = "From: $vFromName <$vFromEmail>" . $eol;
		$headers .= "Reply-To: $vFromName  <$vFromEmail>" . $eol;
		//$headers .= "X-Priority: $vPriority" . $eol . $eol;
		//$headers .= "X-MSmail-Priority: $vMS_Priority" . $eol . $eol;
		$headers .= "Return-Path: $vFromName  <$vFromEmail>" . $eol;
		$headers .= "Message-ID: <".$now." TheSystem@".$_SERVER['SERVER_NAME'].">".$eol;
		$headers .= "X-Mailer: PHP v".phpversion()."" . $eol;
		$headers .= "Mime-Version: 1.0" . $eol;
		$headers .= "Content-Type: multipart/related; boundary=\"" . $boundary . "\"" . $eol;		
		
		$this_message .= "Content-Type: multipart/alternative" . $eol;
//		$this_message = "";
		$this_message .= "This is a multipart MIME formatted message." . $eol;
		
		$this_message .= '--' . $boundary . $eol;
		$this_message .= "Content-Transfer-Encoding: 7bit" . $eol;
		$this_message .= "Content-Type: text/plain" . $eol . $eol;
		$this_message .= $text_message;
	
		$this_message .= '--' . $boundary . $eol;
		$this_message .= "Content-Transfer-Encoding: 7bit" . $eol;
		$this_message .= "Content-Type: text/html" . $eol . $eol;
		$this_message .= $html_message;
		
		
//		$email_array = explode(",", $eList);
		
/*		echo $headers;
		echo $eList;
		echo $vSubject;
		echo $this_message;*/
	
		if(mail($eList, $vSubject, $this_message, $headers))
			print("Email sent<br />");
		else
			print("Email Failed<br />");

/*		for($e = 0; $e < count($email_array); $e++) {
			$to_address = $email_array[$e];
			if(mail($to_address, $vSubject, $this_message, $headers)) {
				print("Email sent<br />");
			}
			else {
				print("Email Failed<br />");
			}
		}*/
	}
	else {
		print("
			<br />
			Some or all of the required information for an email blast is missing,  
			please use your browsers back button to try again.
		");
	}
?>	