<?php
	include ("headerSimple.php");
?>
<style type="text/css">
.cbBackButton
{
/*Back Button Attributes*/
color: #ffffff;
font-size: 12px;
font-family: Verdana;
font-style: normal;
font-weight: bold;
text-align: center;
vertical-align: middle;
border-color: #5496C6;
border-style: solid;
border-width: 1px;
background-color: #032D5F;
/* Forced by default to add space between buttons */
width: auto;
height: auto;
margin: 0 3px;
}
</style>

<script type="text/javascript">
function redirectToNonTab() {
	if (parent.resizeTabFrame) return;
	try {
		var parts = pageName.split("_");
		parts[1] = (parts[1] == undefined ? "" : parts[1]);
		if (parts[0] == "wosDetails") {
			// redirect to non tab page if cannot find parent frame
			var redirectLink = parts[0] + "NonTab" + (parts[1] != "" ? "_" + parts[1] : "") + ".php?" + "<?=$_SERVER['QUERY_STRING']?>";
			try {
				$("#caspioform").css("display", "none");
			}
			catch (e) {
			}
			document.location.replace(redirectLink);
		}
	}
	catch (e) {
	}
}


	var httpReferer = "<?php echo (isset($_GET['referer']) ? urldecode($_GET['referer']) : $_SERVER["HTTP_REFERER"]) ?>";
	redirectToNonTab();
	
	$(document).ready(function(){
		width = document.documentElement.scrollWidth; 
		height = document.documentElement.scrollHeight;
		try {
			parent.resizeTabFrame(width, height, window);
		}
		catch (e) {
			redirectToNonTab();
		}
	});
</script>

