<?php

//Set logged in session
/*$cookie_path = "/";
$cookie_timeout = "0";
$cookie_domain = ".fieldsolutions.com";
session_set_cookie_params($cookie_timeout, $cookie_path, $cookie_domain);
session_name(fsTemplate);
session_start();*/

require_once("headerStartSession.php");

$_SESSION['loggedIn']="no";
$_SESSION['loggedInAs']="";
$_SESSION['TermsAccepted']="";
unset($_SESSION['BankingInfoPW']);

$FromLogin = "/";
if(isset($_GET["FromLogin"]))
{
    $FromLogin = "/" . $_GET["FromLogin"];
}
elseif (isset ($_SESSION["LoginPage"]) && $_SESSION["LoginPage"] != NULL) {
	$FromLogin = $_SESSION["LoginPage"];
}

//unsetting entire session
$_SESSION = array();
session_destroy();

header( "Location: $FromLogin" ) ;
?>