
</div>
<div class="breaklineFooter">
<br /><br /><br /><br /><br />
</div>
<?php
$urlParts = explode('.', $_SERVER['HTTP_HOST']);
$template = $urlParts[0];
$checkIndexDisplay = $template;

if ($checkIndexDisplay == "hms") { ?>

		<!-- FOOTER -->
		<div class="footer_menu clr" style="clear: both;">
			<table cellspacing="10" cellpadding="10" align="center">
				<tbody><tr>
					<td valign="top" class="footer_items">
					<a style="text-decoration: none;" href="http://www.hmstech.com/index.asp">
					<span style="color: rgb(140, 140, 140);">
					HMS Home
					</span>
					</a>
					</td>
					
					<td valign="top" class="footer_items">
					<a style="text-decoration: none;" href="http://www.hmstech.com/solutions/index.asp">
					<span style="color: rgb(140, 140, 140);">
					Solutions
					</span>
					</a>
					</td>
					
					<td valign="top" class="footer_items">
					<a style="text-decoration: none;" href="http://www.hmstech.com/about-hms/index.asp">
					<span style="color: rgb(140, 140, 140);">
					About HMS
					</span>
					</a>
					</td>
					
					<td valign="top" class="footer_items">
					<a style="text-decoration: none;" href="http://www.hmstech.com/business-info/index.asp">
					<span style="color: rgb(140, 140, 140);">
					Business Info
					</span>
					</a>
					</td>
					
					<td valign="top" class="footer_items">
					<a style="text-decoration: none;" href="http://www.hmstech.com/news/index.asp">
					<span style="color: rgb(140, 140, 140);">
					News
					</span>
					</a>
					</td>
					
					<td valign="top" class="footer_items">
					<a style="text-decoration: none;" href="http://www.hmstech.com/support/contact-us.asp">
					<span style="color: rgb(140, 140, 140);">
					Contact HMS
					</span>
					</a>
					</td>
				</tr>
			</tbody></table>
			<br><br>
			<div class="copyright">
			<span style="color: rgb(140, 140, 140);">Copyright &copy; 2007-<?PHP echo date("Y") ?>, FieldSolutions, Inc. All rights reserved.&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;	
			</span>
			<a style="text-decoration: none;" title="Privacy Policy" target="_blank" href="https://hms.fieldsolutions.com/wp/wp-content/themes/fieldsolutions/img/privacy_policy.pdf">
			<span style="color: rgb(140, 140, 140);">
			Privacy Policy
			</span>
			</a>
			</div>

		</div>
        

<?php } else { ?>

<div class="footer_menu clr" style="clear: both;">
			<table cellspacing="10" cellpadding="10" align="center">
				<tbody><tr>
					<td valign="top" class="footer_items">
					<a style="text-decoration: none;" href="/wp/our-services/">
					<span style="color: rgb(140, 140, 140);">
					Our Services
					</span>
					</a>
					</td>
					
					<td valign="top" class="footer_items">
					<a style="text-decoration: none;" href="/wp/our-clients/">
					<span style="color: rgb(140, 140, 140);">
					Our Clients
					</span>
					</a>
					</td>
					
					<td valign="top" class="footer_items">
					<a style="text-decoration: none;" href="/wp/our-members/">
					<span style="color: rgb(140, 140, 140);">
					Our Members
					</span>
					</a>
					</td>
					
					<td valign="top" class="footer_items">
					<a style="text-decoration: none;" href="/wp/fieldsolutions-difference/">
					<span style="color: rgb(140, 140, 140);">
					FieldSolutions Difference
					</span>
					</a>
					</td>
					
					<td valign="top" class="footer_items">
					<a style="text-decoration: none;" href="/wp/news-and-events/">
					<span style="color: rgb(140, 140, 140);">
					News and Events
					</span>
					</a>
					</td>
					
					<td valign="top" class="footer_items">
						<a style="text-decoration: none;" href="/wp/about-us/">
						<span style="color: rgb(140, 140, 140);">
						About Us
						</span>
						</a>
						<ul id="footer_aboutus">
							<li>
							<a href="/wp/mission-and-values/">
							<span style="color: rgb(140, 140, 140);">
							Mission and Values
							</span>
							</a>
							</li>
							
							<li>
			 				<a href="/wp/careers/">
							<span style="color: rgb(140, 140, 140);">
			 				Careers
			 				</span>
							</a>
			 				</li>
							
			 				<li>
			 				<a href="/wp/management-team/">
							<span style="color: rgb(140, 140, 140);">
			 				Management Team
			 				</span>
							</a>
			 				</li>
			 				
			 				<li>
			 				<a href="../content/Terms_Use.pdf">
							<span style="color: rgb(140, 140, 140);">
			 				Terms Of Use
			 				</span>
							</a>
			 				</li>
			 			</ul>
					</td>
					
					<td valign="top" class="footer_items">
					<a style="text-decoration: none;" href="/wp/contact-us/">
					<span style="color: rgb(140, 140, 140);">
					Contact Us
					</span>
					</a>
					</td>
				</tr>
			</tbody></table>
			<br><br>
			<div class="copyright">
			<span style="color: rgb(140, 140, 140);">Copyright &copy; 2007-<?PHP echo date("Y") ?>, FieldSolutions, Inc. All rights reserved.&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
			</span>
			<a style="text-decoration: none;" title="Privacy Policy" target="_blank" href="https://www.fieldsolutions.com/wp/wp-content/themes/fieldsolutions/img/privacy_policy.pdf">
			<span style="color: rgb(140, 140, 140);">
			Privacy Policy
			</span>
			</a>
			</div>

		</div>


<?php }?>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-4102511-1");
pageTracker._setDomainName(".fieldsolutions.com");
pageTracker._trackPageview();
} catch(err) {}
</script>


</body>
</html>
