<?php $page = login; ?>
<?php require ("header.php"); ?>
<?php require ("fls/FLSnavBar.php"); ?>
<!-- Add Content Here -->
<?php


if(isset($_POST['vSubmit']))
	{

		$emailList = $_POST["emailList"];

}
else 
	$emailList = "codem01@gmail.com";


?>
<form id="blastForm" name="blastForm" method="post" onSubmit="return validate();" action="Email_Blast_FLS.php">
	<table width="500" border="0" cellpadding="0" cellspacing="2">
		<tr>
			<td width="10" nowrap="nowrap">&nbsp;</td>
			<td nowrap="nowrap">
				<div align="right" class="style4">
					<div align="left">From Name : </div>
				</div>
			</td>
			<td>
				<label>
				<input name="vFromName" type="text" id="vFromName" size="60" />
				</label>
			</td>
		</tr>
		<tr>
			<td nowrap="nowrap">&nbsp;</td>
			<td nowrap="nowrap">
				<div align="right" class="style4">
					<div align="left">From Email :</div>
				</div>
			</td>
			<td nowrap="nowrap">
				<label>
				<input name="vFromEmail" type="text" id="vFromEmail" size="60" />
				&nbsp;<input name="copySelf" type="checkbox" />&nbsp;Copy&nbsp;to&nbsp;Self
				</label>
			</td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap">&nbsp;</td>
			<td valign="top" nowrap="nowrap">
				<div align="right" class="style4">
					<div align="left">Subject : </div>
				</div>
			</td>
			<td>
				<label>
				<input name="vSubject" type="text" id="vSubject" size="60" />
				</label>
			</td>
		</tr>
		<tr>
			<td valign="top">&nbsp;</td>
			<td valign="top">
				<div align="right" class="style4">
					<div align="left">Message : </div>
				</div>
			</td>
			<td>
				<label>
				<textarea name="vMessage" cols="70" rows="30" id="vMessage"></textarea>
				</label>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<div align="right"></div>
			</td>
			<td>
				<input name="vReset" type="reset" id="vReset" value="Reset" />
				<input name="vBlast" type="submit" id="vBlast" value="Send" />
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>
				<div align="right"></div>
			</td>
			<td>
				<input name="eList" type="hidden" id="eList" value="<?php echo "$emailList"; ?>" size="100">
			</td>
		</tr>
	</table>
</form>
<script type="text/javascript">

function validate() {
	var address = document.forms.blastForm.vFromEmail.value;
	var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	var valid = false;
	if (filter.test(address)) {
		valid = true;
		saveUserInfo();
	}
	else{
		alert("Please input a valid from email address!");
		valid = false;
	}
	return valid;
}

function saveUserInfo()
{

	var expireDate = new Date;
	expireDate.setMonth(expireDate.getMonth()+6);

	var eFrom = document.forms[0].vFromName.value;
	var eFromEmail = document.forms[0].vFromEmail.value;
	var eCopySelf = (document.forms[0].copySelf.checked ? "1" : "0");

	document.cookie = "blast_From=" + eFrom + ";expires=" + expireDate.toGMTString() + ";path=/";
	document.cookie = "blast_Email=" + eFromEmail + ";expires=" + expireDate.toGMTString() + ";path=/";
	document.cookie = "blast_CopySelf=" + eCopySelf + ";expires=" + expireDate.toGMTString() + ";path=/";
}

function get_cookie(theCookie)
{
	var search = theCookie + "="
	var returnvalue = "";
	if (document.cookie.length > 0)
	{
		offset = document.cookie.indexOf(search);
		// if cookie exists
		if (offset != -1)
		{
			offset += search.length;
			// set index of beginning of value
			end = document.cookie.indexOf(";", offset);
			// set index of end of cookie value
			if (end == -1) end = document.cookie.length;
			returnvalue=unescape(document.cookie.substring(offset, end));
		}
	}
	return returnvalue;
}

/*
function delineate(str)
{
//alert(str.indexOf("="));
start = str.indexOf("=")+1;
return(str.slice(start));
}

document.forms.blastForm.eList.visible=false;

var locate = window.location;
document.forms.blastForm.eList.value = locate;
document.forms.blastForm.eList.value = delineate(document.forms.blastForm.eList.value);
*/

document.forms.blastForm.vFromName.value = get_cookie("blast_From");
document.forms.blastForm.vFromEmail.value = get_cookie("blast_Email");
document.forms.blastForm.copySelf.checked = (get_cookie("blast_CopySelf") == "1");

//document.forms[0].onsubmit=saveUserInfo; // Save User From Name & Email Address in cookie

</script>
<!--- End Content --->
<?php require ("footer.php"); ?><!-- ../ only if in sub-dir -->
