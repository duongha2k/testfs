<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "DTD/xhtml1-transitional.dtd">
<html>
<head>
	<title>JavaScript Fly-out Test</title>

	<link rel="stylesheet" type="text/css" href="/widgets/css/main.css?04032011">

	<style type="text/css">
		.rotate90 {
			display: block;
			writing-mode: tb-rl;
			-webkit-transform: rotate(90deg);
			-webkit-transform-origin: bottom left;
			-moz-transform: rotate(90deg);
			-moz-transform-origin: bottom left;
			-o-transform: rotate(90deg);
			-o-transform-origin: bottom left;
			-ms-transform: rotate(90deg);
			-ms-transform-origin: bottom left;
			transform: rotate(90deg);
			transform-origin: bottom left;
			position: absolute;
		}

		.suggested_pay { width: 338px; height: 252px; padding: 0 0 0 0; }
		.suggested_pay .rotate90 { left: 8px; top: 10px; }
		.suggested_pay .tab { float: left; width: 30px; height: 100%; cursor: pointer; }
		.suggested_pay .tab .label { font: normal 14px Calibri, Times New Roman; color: #ffffff; }

		.suggested_pay .panel { float: right; width: 308px; height: 100%; background-color: #ffffff; padding: 0 0 0 0; }
		.suggested_pay .panel .heading { font: normal 14px Calibri, Times New Roman; color: #000000; }
		.suggested_pay .panel .description { font: normal 11px Calibri, Times New Roman; color: #000000; }
		.suggested_pay .panel .label { font: normal 11px Calibri, Times New Roman; color: #000000; }
		.suggested_pay .panel .value { font: bold 11px Calibri, Times New Roman; color: #000000; }
		.suggested_pay .panel .rule { height: 14px; }
		.suggested_pay .panel .apply_button { height: 23px; }
	</style>

	<!--[if IE]>
	<style type="text/css">
		.suggested_pay .rotate90 { left: -175px; top: 48px; white-space: nowrap; }
		.suggested_pay .panel .rule { height: 12px; }
		.suggested_pay .panel .label { font: normal 11px/12px Calibri, Times New Roman; color: #000000; }
		.suggested_pay .panel .value { font: bold 11px/12px Calibri, Times New Roman; color: #000000; }
		.suggested_pay .panel .apply_button { height: 29px; }
	</style>
	<![endif]-->

	<script type="text/javascript" src="/widgets/js/jquery.min.js"></script>
	<script type="text/javascript" src="/widgets/js/jquery-ui-1.8.min.js"></script>
	<script type="text/javascript">
	//-------------------------------------------------------------------------
	var BaseClass = (function () {
		var BaseClass = function () {
			this.listen = function (event, handler, context) {
				var _self = this;
				var method = "on" + event;
				var caller = _self[method];

				if (typeof context == "undefined") context = this;

				handler = handler.bind (context);

				if (typeof caller == "undefined") {
					caller = new MultiCast (handler);
					caller.bind (this);
					_self[method] = caller.call
				}
				else if (typeof caller.multicast != "undefined") {
					caller.multicast.listen (handler);
				}
			};

			this.trigger = function (event) {
				var _self = this;
				var method = "on" + event;
				var caller = _self[method];
				var args = Array.prototype.slice.call (arguments, 1);

				if (typeof caller == "function") {
					caller.apply (this, args);
				}
			};

			this.halt = function (event, handler) {
			};
		};

		return BaseClass;
	}) ();

	//-------------------------------------------------------------------------
	var Property = (function () {
		return function () {
			var Property, isSet, set, _func;
			var _value = arguments[0];
			var _reference = arguments[1];
			var _parent = new BaseClass ();

			Property = function () {
				return _func.apply (Property, arguments);
			};

			isSet = function () {
				return (typeof _value != "undefined");
			};

			set = function (value) {
				_value = value;
			};

			_func = function (value) {
				if (typeof value != "undefined") {
					if (typeof this.onassign == "function")
						value = this.onassign (value, _value, _reference);

					if (value !== _value) {
						if (typeof this.onchange == "function") {
							value = this.onchange (value, _value, _reference);
						}
					}

					_value = value;
				}

				return _value;
			};

			//Polymorphism doesn't work on function objects for some browsers
			//and requires the following hack
			Object.keys (_parent).forEach (function (method) {
				Property[method] = _parent[method];
			});

			Property.isSet = isSet;
			Property.quietSet = set;

			return Property;
		};
	}) ();

	//-------------------------------------------------------------------------
	var MultiCast = (function () {
		var _resultmethod_onchange;

		var MultiCast = function () {
			var _self = this;
			var init_args = Array.prototype.slice.call(arguments, 0);

			this._listeners = new Array ();
			this._binding = false;
			this.resultMethod = new Property ("chain");
			this.resultMethod.onchange = _resultmethod_onchange;

			this.call = function () {
				var args, result, temp;

				for (var i = 0; i < _self._listeners.length; i++) {
					args = Array.prototype.slice.call(arguments, 0);
					if (typeof (result) != "undefined") args.push (result);

					if (_self._binding === false) {
						temp = _self._listeners[i].apply (this, args);
					}
					else {
						temp = _self._listeners[i].apply (_self._binding, args);
					}

					if (typeof temp != "undefined") result = temp;
				}

				return result;
			};

			this.call.multicast = this;

			init_args.forEach (function (listener) {
				if (typeof listener != "undefined")
					this.listen (listener);
			}, this);
		};

		MultiCast.prototype.listen = function (listener) {
			if (typeof listener == "function") {
				//if (listener.multicast && listener.multicast != this) {
				//	listener.multicast.combine (pvt);
				//}
				//else {
					if (typeof listener.ignore != "function") {
						listener.ignore = function () {
							this.ignore (listener);
						}.bind (this);
					}

					this._listeners.push (listener);
				//}
			}
			else {
				throw new TypeError ("Multicast.listen(): Parameter 1 " +
					"must be a function");
			}

			return this;
		};

		MultiCast.prototype.ignore = function (listener) {
			var index = false;

			if (typeof listener == "function") {
				for (var i = 0; i < this._listeners.length; i++) {
					if (this._listeners[i] == listener) index = i;
				}

				if (index !== false) this._listeners.splice (index, 1);
			}
			else {
				throw new TypeError ("Multicast.ignore(): Parameter 1 " +
					"must be a function");
			}

			return this;
		};

		MultiCast.prototype.combine = function (mcast_private) {
			for (var i = 0; i < mcast_private._listeners.length; i++) {
				this._listeners.push (mcast_private._listeners[i]);
			}

			mcast_private._listeners = this._listeners;
		};

		MultiCast.prototype.count = function () {
			return this._listeners.length;
		};

		MultiCast.prototype.bind = function (object) {
			this._binding = object;

			return this;
		};

		_resultmethod_onchange = function (value) {
			return (value | "chain").toString ().toLowerCase ();
		};

		return MultiCast;
	}) ();

	//-------------------------------------------------------------------------
	var CollectionModel = (function () {
		var _data = {};

		var CollectionModel = function () {
			var fields = Array.prototype.slice.call(arguments, 0);
			var _toString = Object.prototype.toString;
			var _update_lock = false;

			this.set = function (key, data) {
				if (_toString.call (key) == "[object String]") {
					if (typeof _data[key] != "function") {
						_data[key] = new Property (data, this);
					}
					else {
						(_data[key]) (data);
					}

					if (_update_lock == false && typeof this.onupdate == "function")
						this.onupdate.bind (this) (key, (_data[key])());
				}
				else if (_toString.call (key) == "[object Object]") {
					_update_lock = true;
					Object.keys (key).forEach (function (index) {
						this.set (index, key[index]);
					}, this);
					_update_lock = false;

					if (typeof this.onupdate == "function")
						this.onupdate.bind (this) (this.toObject());
				}
			};

			this.getProperty = function (key) {
				return _data[key];
			};

			this.toObject = function () {
				var result = {};
				var _data_reup = _data;

				Object.keys (_data).forEach (function (key) {
					result[key] = _data_reup[key] ();
				});

				return result;
			};

			this._ = this.getProperty;

			for (var i = 0; i < fields.length; i++) {
				this.set (fields[i]);
			}
		};

		CollectionModel.prototype = new BaseClass();

		return CollectionModel;
	}) ();




	//-------------------------------------------------------------------------
		var test = new Property (69.0, "this is a reference");
		var obj = { "foo": "bar" };
		var func1 = function (value) { console.log ("This is function 1", value, this.foo); };
		var func2 = function (value) { console.log ("This is function 2", value, this.foo); };
		var func3 = function (value) { console.log ("This is function 3", value, this.foo); };
		var mc = new MultiCast ();

		mc.bind (obj);
		mc.listen (func1);
		mc.listen (func2);
		mc.listen (func3);

		//mc.call (69);

		/*test.onchange = function (value, current, ref) {
			console.log (value, current, ref, this.foo);
			return value;
		};//*/
		/*test.listen ("change", function (value, current, ref) {
			console.log (value, current, ref, this.foo);
			return value;
		}, obj);

		test (100);
		//*/


		var collection = new CollectionModel ("id", "name", "address", "timestamp");

		collection._("address").listen ("change", function (value, current, ref) {
			console.log ("CHANGE", value, current, ref);
			return value + " special";
		}); 
		collection.listen ("update", function (key, data) {
			console.log ("UPDATE", key, data);
		}); 
		collection.set ({ id: 12, name: "Jason K. Flaherty", address: "nowhere" });
		collection.set ("address", "somewhere");
		//collection._("address").quietSet (23872);
		console.log (collection.toObject ());

		var window_w;
		var window_h;
		var panel;
		var panel_tab
		var panel_tab_arrows;
		var panel_pos1;
		var panel_pos2;

		jQuery.noConflict ();
		jQuery (document).ready (function () {
			window_w                    = jQuery (window).width ();
			window_h                    = jQuery (window).height ();
			panel						= jQuery ("#suggested_pay");
			panel_tab					= jQuery ("#suggested_pay_tab");
			panel_tab_arrows			= jQuery (".suggested_pay_arrow");
			panel_pos1					= (window_w - 30);
			panel_pos2					= panel_pos1 - jQuery("#suggested_pay_panel").width ();
 
			panel.css ({
				"position": "fixed",
				"left": panel_pos1 + "px",
				"top": "16px"
			});

			panel_tab.click (flyout);
		});

		function flyout (event) {
			panel_tab.unbind ("click");
			panel.stop ().animate ({ 'left': panel_pos2 + 'px' }, 300, function () {
				panel_tab_arrows.attr ("src", "/widgets/images/popup/arrow_blue_right_9x16.png");
				panel_tab.click (flyin);
			});
		}

		function flyin (event) {
			panel_tab.unbind ("click");
			panel.stop ().animate ({ 'left': panel_pos1 + 'px' }, 300, function () {
				panel_tab_arrows.attr ("src", "/widgets/images/popup/arrow_blue_left_9x16.png");
				panel_tab.click (flyout);
			});
		}
	</script>
</head>

<body>
	<div id="suggested_pay" class="suggested_pay">
		<div id="suggested_pay_tab" class="tab">
			<div style="width: 100%; height: 100%;">
				<table cellspacing="0" cellpadding="0" border="0">
					<tr height="8">
						<td><img src="/widgets/images/popup/border_orange_top_30x8.png" width="30" height="8" border="0" /><br/></td>
					</tr>
					<tr style="height: 16px; background-color: #f69630; text-align: center;">
						<td><img src="/widgets/images/popup/arrow_blue_left_9x16.png" class="suggested_pay_arrow" width="9" height="16" border="0" /><br/></td>
					</tr>
					<tr>
						<td style="height: 204px; background-color: #f69630; text-align: center; vertical-align: middle;">
							<div class="rotate90" style="width: 200px; text-align: center;">
								<span id="suggested_pay_amount_label" class="label">
									SuggestTechPayTool <!-- add colon when populating amount -->
								</span>
								<span id="suggested_pay_amount" class="label">
									$34.04
								</span><br/>
							</div>
						</td>
					</tr>
					<tr style="height: 16px; background-color: #f69630; text-align: center;">
						<td><img src="/widgets/images/popup/arrow_blue_left_9x16.png" class="suggested_pay_arrow" width="9" height="16" border="0" /><br/></td>
					</tr>
					<tr height="8">
						<td><img src="/widgets/images/popup/border_orange_bottom_30x8.png" width="30" height="8" border="0" /><br/></td>
					</tr>
				</table>
			</div>
		</div>
		<div id="suggested_pay_panel" class="panel">
			<div style="width: 302px; height: 246px; border: 3px solid #f69630;">
				<div style="width: 290px; height: 238px; margin: 4px 6px 4px 6px;">
					<!-- content -->
					<table cellspacing="0" cellpadding="1" border="0" width="100%">
						<tr valign="middle" align="center">
							<td colspan="2">
								<span class="heading">
									Suggested Tech Pay&nbsp;&nbsp;
								</span>
								<img width="16px" height="16px" style="vertical-align: middle;cursor: pointer;" src="/widgets/images/FAQi.png" id="ESSReportingHelp" bt-xtitle="" title=""><br/>
							</td>
						</tr>
						<tr valign="middle">
							<td colspan="2">
								<span class="description">Data analytics calculation based on this Work Orders' data - add more information to increase accuracy.</span><br/>
							</td>
						</tr>
						<tr valign="middle">
							<td colspan="2" class="rule">
								<hr size="1" width="270" align="center" color="#f69630" style="margin-top: 0; margin-bottom: 0;" />
							</td>
						</tr>
						<tr valign="middle">
							<td width="50%">
								<span class="label">Geography:</span><br/>
							</td>
							<td width="50%">
								<span class="value">Urban / Metro</span><br/>
							</td>
						</tr>
						<tr valign="middle">
							<td width="50%">
								<span class="label">Skill Category:</span><br/>
							</td>
							<td width="50%">
								<span class="value">Point of Sale</span><br/>
							</td>
						</tr>
						<tr valign="middle">
							<td width="50%">
								<span class="label">Lead Time:</span><br/>
							</td>
							<td width="50%">
								<span class="value">Next Business Day</span><br/>
							</td>
						</tr>
						<tr valign="middle">
							<td width="50%">
								<span class="label">Day Part:</span><br/>
							</td>
							<td width="50%">
								<span class="value">Evening</span><br/>
							</td>
						</tr>
						<tr valign="middle">
							<td width="50%">
								<span class="label">On-Site Duration:</span><br/>
							</td>
							<td width="50%">
								<span class="value">3 hours 30 min</span><br/>
							</td>
						</tr>
						<tr valign="middle">
							<td width="50%">
								<span class="label">Includes Admin Fee:</span><br/>
							</td>
							<td width="50%">
								<span class="value">10%</span><br/>
							</td>
						</tr>
						<tr valign="middle">
							<td colspan="2" class="rule">
								<hr size="1" width="270" align="center" color="#f69630" style="margin-top: 0; margin-bottom: 0;" />
							</td>
						</tr>
						<tr valign="middle">
							<td width="50%">
								<span class="label">Suggested Hourly Tech Pay:</span><br/>
							</td>
							<td width="50%">
								<span class="value">$37.44 / hour</span><br/>
							</td>
						</tr>
						<tr valign="middle">
							<td width="50%">
								<span class="label">Est. Hourly Pay Range:</span><br/>
							</td>
							<td width="50%">
								<span class="value">$25.22 &nbsp; to &nbsp; $51.68</span><br/>
							</td>
						</tr>
						<tr valign="middle">
							<td width="50%">
								<span class="label">Suggested Client $ Offer:</span><br/>
							</td>
							<td width="50%">
								<span class="value">$131.05 / site</span><br/>
							</td>
						</tr>
						<tr valign="bottom">
							<td colspan="2" align="center" class="apply_button">
								<input type="button" class="link_button middle_button" value="Apply Suggested Offer" /><br/>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>

	<br/><br/>

	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	Scrolling...<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	<div id="pc_reference"></div>
</body>
</html>
