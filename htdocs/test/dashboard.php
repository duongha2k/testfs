<?php $page = techs; ?>
<?php $option = dashboard; ?>
<?php require ("../header.php"); ?>
<?php 
$loggedIn = $_SESSION['loggedIn'];
$TechID = $_SESSION['TechID'];
$FLSID = $_SESSION['FLSID'];
$FLSCSP_Rec= $_SESSION['FLSCSP_Rec'];

if($loggedIn!="yes"){ 
// Redirect to logIn.php
header( 'Location: https://www.fieldsolutions.com/techs/logIn.php' ) ;
}
?>

<?php require ("../navBar.php"); ?>

<style type="text/css" media="all">

body
{
	background: #fff;
	height: 100%;
}
#window
{
	position: absolute;
	left: 200px;
	top: 100px;
	width: 400px;
	height: 300px;
	overflow: hidden;
	display: none;
}
#windowTop
{
	height: 30px;
	overflow: 30px;
	background-image: url(images/window_top_end.png);
	background-position: right top;
	background-repeat: no-repeat;
	position: relative;
	overflow: hidden;
	cursor: move;
}
#windowTopContent
{
	margin-right: 13px;
	background-image:url(images/window_top_start.png);
	background-position:left top;
	background-repeat: no-repeat;
	overflow: hidden;
	height: 30px;
	line-height: 30px;
	text-indent: 10px;
	font-family:Arial, Helvetica, sans-serif;
	font-weight: bold;
	font-size: 14px;
	color: #6caf00;
}
#windowMin
{
	position: absolute;
	right: 25px;
	top: 10px;
	cursor: pointer;
}
#windowMax
{
	position: absolute;
	right: 25px;
	top: 10px;
	cursor: pointer;
	display: none;
}
#windowClose
{
	position: absolute;
	right: 10px;
	top: 10px;
	cursor: pointer;
}
#windowBottom
{
	position: relative;
	height: 270px;
	background-image: url(images/window_bottom_end.png);
	background-position: right bottom;
	background-repeat: no-repeat;
}
#windowBottomContent
{
	position: relative;
	height: 270px;
	background-image: url(images/window_bottom_start.png);
	background-position: left bottom;
	background-repeat: no-repeat;
	margin-right: 13px;
}
#windowResize
{
	position: absolute;
	right: 3px;
	bottom: 5px;
	cursor: se-resize;
}
#windowContent
{
	position:absolute;
	top: 30px;
	left: 10px;
	width: auto;
	height: auto;
	overflow: auto;
	margin-right: 10px;
	border: 1px solid #6caf00;
	height: 255px;
	width: 375px;
	font-family:Arial, Helvetica, sans-serif;
	font-size: 11px;
	background-color: #fff;
}
#windowContent *
{
	margin: 10px;
}
.transferer2
{
	border: 1px solid #6BAF04;
	background-color: #B4F155;
	filter:alpha(opacity=30); 
	-moz-opacity: 0.3; 
	opacity: 0.3;
}
</style>

<script type="text/javascript" src="../library/jquery/jquery_iframe.js"></script>
<script type="text/javascript" src="../library/jquery/interface.js"></script>


<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script> <script type="text/javascript" language="javascript">try{f_cbload("I4H0E7G3A9I4H0E7G3A9","https:");}catch(li){;}</script> <div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=I4H0E7G3A9I4H0E7G3A9">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>

<!-- Check if logged in, if not redirect -->
<script language="JavaScript">
try {
	var getUsernameField = document.forms[0].xip_UserName.value;
	setTimeout('window.location.replace("/techs/index.php")');
} catch(e) {} 
</script>



<!-- Add Content Here -->
<div id="container">


<div id="header">

    <div id="wrapper">
        <div id="content" align="center"></div>
    </div>

<!--- LEFT COLUMN --->
<div id="leftcontent">

<div id="welcomeModule">
<b>Welcome Back <span id="techName"></span></b>
</div>
	
<br> 
   
<div id="" style="padding-left:40px"> 
<table width="377" border="0" cellpadding="0">
  <tr>
    <td><b>IMPORTANT NOTICE!</b> The warehouse that parts are shipped to for the FLS program will be changing soon. This means that new Waybills will eventually be needed. This change was originally going to take place on Feb. 22, then Feb. 29 and now a date to be determined later. Please stay tuned for the new date this change is going to occur. For the time being your old Waybills will still work.<br>
      <br>
Click <a href="tech_ShipTo.php">here</a> order a new supply of Waybills for the FLS program/client</td>
  </tr>
</table><br /><br />
<ul id="todo">
<li><b>Processing FLS Work Orders</b>
<br /><br />
<ul id="todo">
<li><a href="ttb-processingWOS.php#bidFLS">How to search and apply for open work orders</a></li>
<li><a href="ttb-processingWOS.php#assignedFLS">How to search work orders assigned to you</a></li>
<li><a href="ttb-processingWOS.php#completedFLS">How to process work orders online once completed</a></li>
</ul>
<br />
</li>
<li><b>Processing Non-FLS Work Orders</b>
<br /><br />
<ul id="todo">
<li><a href="ttb-processingWOS.php#bidMain">How to search and bid on open work orders</a></li>
<li><a href="ttb-processingWOS.php#assignedMain">How to search work orders assigned to you</a></li>
<li><a href="ttb-processingWOS.php#completedMain">How to process work orders online once completed</a></li>
<li><a href="ttb-processingWOS.php#printWOS">Printing work orders</a></li>
</ul>
<br />
</li>
<li><a href="ttb-payment.php">Field Solutions Payment Policy</a></li>
</ul>
</div>

<br><br>

</div>



<!-- RIGHT COLUMN -->
<div id="rightcontent">


<br />
<div id="workOrders" align="center" style="padding="10px">
<Script language="JavaScript">
<!-- 
function goto(form) { var index=form.select.selectedIndex
if (form.select.options[index].value != "0") {location=form.select.options[index].value;}}
-->
</script>

<form name="workOrders">
<!--<select name="select" onChange="goto(this.form)" size="1">
<option value="">-----Work Orders-----</option>
<option value="wos.php">FieldSolutions Dashboard</option>
<option value="wosFLS.php">FLS Dashboard</option>
<option value="">------------------------</option>
<option value="wosViewAppliedAll.php">My Applied Work Orders</option>
</select>-->
</form>


</div>

<br />

<!-- Retrieve WOS Open -->	
<div id="viewIframe" align="center">
<script language="javascript" type="text/javascript">

// Verify flsid exists
if(document.forms.caspioform.EditRecordFLSID.value != ""){

document.write('<a href="ajax/retrieveFLSWOsnotClosed.php?FLSTechID=' + document.forms.caspioform.EditRecordFLSID.value + '" class="iframe w:375 h:200">Loading...</a>');

} 

</script>
<br />
</div>

<hr style="height: 2px; color:#174065; background:#174065;" noshade="noshade" />
	

<div id="ToDoBlock" style="padding="10px">

<p><b>To Do:</b></p>

<ul id="todo">

<script language="javascript" type="text/javascript">
<!-- 
if(document.forms.caspioform.cbParamVirtual2.value == "No"){
document.write('<li><a href="/techs/skills.php">Complete Skills</a></li>');
}
if(document.forms.caspioform.cbParamVirtual3.value == "No"){
document.write('<li><a href="/techs/experience.php">Complete Experience</a></li>');
}
if(document.forms.caspioform.cbParamVirtual4.value == "No"){
document.write('<li><a href="/techs/certifications.php">Complete Certifications</a></li>');
}
//if(document.forms.caspioform.cbParamVirtual5.value == "No"){
//document.write('<li><a href="/techs/media.php">Complete Audio Resume</a></li>');
//}
if(document.forms.caspioform.cbParamVirtual6.value == "No"){
document.write('<li><a href="/techs/equipment.php">Equipment</a></li>');
}
if(document.forms.caspioform.cbParamVirtual7.value == "No"){
document.write('<li><a href="/techs/w9.php">Complete W9</a></li>');
}
        
-->
</script>
    

<li><a href="backgroundChecks.php">Background Check Instructions</a><img src="../images/new.gif" width="23" height="12"></li>
<li><a href="FLS_CSP.php">Computing Security Procedures (FLS-CSP) Policy</a><img src="../images/new.gif" alt="" width="23" height="12"></li>
<li><a href="/techs/more.php">More</a></li>
</ul>


<br />
</div>
	
<hr style="height: 2px; color:#174065; background:#174065;" noshade="noshade" />
	

<div id="quickLinks" style="padding="10px">
<p><b>Quick Links:</b></p>

<ul id="todo">
<li><a href="signup.php">Sign Up Review</a></li>
<li><a href="/content/contactUs.php">Self Help/Contact Us</a></li>
</ul>
</div>

	<br />

<hr style="height: 2px; color:#174065; background:#174065;" noshade="noshade" />

<img src="images/pixel.gif" height="100px;" width="1">
</div>

</div>

<a href="#" id="windowOpen">Open window</a>
<div id="window">
	<div id="windowTop">
		<div id="windowTopContent">Window example</div>
		<img src="../techs/images/window_min.jpg" id="windowMin" />
		<img src="../techs/images/window_max.jpg" id="windowMax" />
		<img src="../techs/images/window_close.jpg" id="windowClose" />
	</div>
	<div id="windowBottom"><div id="windowBottomContent">&nbsp;</div></div>

	<div id="windowContent"><p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Ut eget  lectus in diam iaculis tempor. Suspendisse consectetuer, justo nec  euismod sodales, augue nisi convallis diam, in posuere pede turpis at  massa. Donec mi nunc, iaculis eu, vehicula sed, malesuada eget, ligula.  In eu urna. Suspendisse pede quam, tempor id, tempor at, placerat eget,  massa. Maecenas porta elementum tellus. Ut nulla diam, posuere non,  consequat nec, fermentum vitae, turpis. Pellentesque tempor metus at  lorem. Donec nulla ipsum, euismod in, pretium vitae, ornare nonummy,  lectus. Aliquam ullamcorper. Vestibulum eget leo. Proin pretium, enim  vitae bibendum accumsan, dolor urna feugiat felis, et ultrices ipsum  odio et dui. Praesent non eros sit amet nisi laoreet aliquet. Nulla  diam. Maecenas et sem eget lorem porttitor tincidunt. Donec sed dui. </p>
	  
	</div>
	<img src="../techs/images/window_resize.gif" id="windowResize" />
</div>
<!--- End Content --->
<?php require ("../footer.php"); ?>

<script type="text/javascript" language="javascript">

<!-- Hide submit button on caspio datapage -->
document.getElementById('Mod0EditRecord').style.display = 'none';

<!-- display First and Last name retrieved from Caspio datapage -->
var ajaxDisplay = document.getElementById('techName');
ajaxDisplay.innerHTML = document.forms.caspioform.EditRecordFirstName.value + " " + document.forms.caspioform.EditRecordLastName.value;


// Jquery Ready Function..when everything loaded execute JS code
$(document).ready(function(){ 

// Verify flsid exists
if(document.forms.caspioform.EditRecordFLSID.value != ""){
$('a.iframe').iframe();
}

}); 
//-->
</script>

<script type="text/javascript" language="javascript">
function openPopup(url) {
	newwindow=window.open(url,'name','height=500, width=740, left=100, top=100, resizable=yes, scrollbars=yes, toolbar=no,status=no');
	if (window.focus) {newwindow.focus()}
}
</script>

<?php if ($FLSID != "" && $FLSCSP_Rec=="No"){ ?>
<script type="text/javascript">
//openPopup('https://www.fieldsolutions.com/techs/FLS_CSP_simple.php');

//$(window).unload( function () { alert("Bye now!"); } );

</script>
<?php } ?>
<script language="JavaScript" type="text/javascript">var client_id = 1;</script>
<script type="text/javascript">
$(document).ready(
	function()
	{
		$('#windowOpen').bind(
			'click',
			function() {
				if($('#window').css('display') == 'none') {
					$(this).TransferTo(
						{
							to:'window',
							className:'transferer2', 
							duration: 400,
							complete: function()
							{
								$('#window').show();
							}
						}
					);
				}
				this.blur();
				return false;
			}
		);
		$('#windowClose').bind(
			'click',
			function()
			{
				$('#window').TransferTo(
					{
						to:'windowOpen',
						className:'transferer2', 
						duration: 400
					}
				).hide();
			}
		);
		$('#windowMin').bind(
			'click',
			function()
			{
				$('#windowContent').SlideToggleUp(300);
				$('#windowBottom, #windowBottomContent').animate({height: 10}, 300);
				$('#window').animate({height:40},300).get(0).isMinimized = true;
				$(this).hide();
				$('#windowResize').hide();
				$('#windowMax').show();
			}
		);
		$('#windowMax').bind(
			'click',
			function()
			{
				var windowSize = $.iUtil.getSize(document.getElementById('windowContent'));
				$('#windowContent').SlideToggleUp(300);
				$('#windowBottom, #windowBottomContent').animate({height: windowSize.hb + 13}, 300);
				$('#window').animate({height:windowSize.hb+43}, 300).get(0).isMinimized = false;
				$(this).hide();
				$('#windowMin, #windowResize').show();
			}
		);
		$('#window').Resizable(
			{
				minWidth: 200,
				minHeight: 60,
				maxWidth: 700,
				maxHeight: 400,
				dragHandle: '#windowTop',
				handlers: {
					se: '#windowResize'
				},
				onResize : function(size, position) {
					$('#windowBottom, #windowBottomContent').css('height', size.height-33 + 'px');
					var windowContentEl = $('#windowContent').css('width', size.width - 25 + 'px');
					if (!document.getElementById('window').isMinimized) {
						windowContentEl.css('height', size.height - 48 + 'px');
					}
				}
			}
		);
	}
);
</script>
