<?php
//	ini_set("display_errors", 1);
	require_once("ivrheader.php");
	require_once("../library/caspioAPI.php");
	require_once("library/ivrLib.php");
	require_once("../library/timeStamps2.php");
	require_once("../library/woContactLog_ivr.php");
	writeIVRHeader();

	$uniqueID = $_SESSION["uniqueID"];
	$type = $_POST["type"];
	
//	$uniqueID = 7103;
//	$type = 0;
	
	//$woList = caspioSelectAdv("Work_Orders_With_Timezone", "ISNULL(CONVERT(VARCHAR, StartDate, 101), ''), ISNULL(StartTime, ''), ISNULL(City, ''), ISNULL(State, ''), ISNULL(Zipcode, ''), ISNULL(Headline, ''), Company_ID, Company_Name, Project_Name, ProjectManagerEmail, EmergencyPhone, Tech_ID, UserName, " . TIME_CURRENT_TIME_LOCAL . ", Firstname, Lastname", "TB_UNID = '" . caspioEscape($uniqueID) . "' AND (" . WO_CHECK_IN_CALL . " OR " . WO_CHECK_OUT_CALL . " OR " . WO_REMINDER_ALL . ") AND " . CRITERIA_ALL_CALL, "", true, "`", "|", false);
	
	$db = Zend_Registry::get('DB');
	$sql = "SELECT IFNULL(DATE_FORMAT(StartDate, '%m/%d/%Y'), ''), IFNULL(StartTime, ''), IFNULL(City, ''), IFNULL(State, ''), IFNULL(Zipcode, ''), IFNULL(Headline, ''), Company_ID, Company_Name, Project_Name, ProjectManagerEmail,  EmergencyPhone, Tech_ID, UserName, " . TIME_CURRENT_TIME_LOCAL . ", Firstname, Lastname, WO_ID FROM work_orders_with_timezone_view_leftjoinzip WHERE WIN_NUM = ? AND Tech_ID = ? AND (" . WO_CHECK_IN_CALL . " OR " . WO_CHECK_OUT_CALL . " OR " . WO_REMINDER_ALL . ") AND " . CRITERIA_ALL_CALL;
	$woList = $db->fetchAll($sql, array($uniqueID, $_SESSION["TechID"]), Zend_Db::FETCH_NUM);
	
	$foundWO = false;
	$emergencyDispatch = "";
	$typeTxt = "";
	if (count($woList) > 0) {
		$foundWO = true;
		$info = $woList[0];
		$startDate = trim($info[0], "`");
		$startTime = trim($info[1], "`");
		$location = htmlentities(trim($info[2], "`") . ", " . trim($info[3], "`") . " " . trim($info[4], "`"));
		$headline = trim($info[5], "`");

		$companyID = trim($info[6], "`");
		$clientName = trim($info[7], "`");
		$projectName = trim($info[8], "`");
		$to_email = trim($info[9], "`");
		$emergencyDispatch = trim($info[10], "`");
		
		// set client notification session vars
		$_SESSION["ProjectName"] = $projectName;
		$_SESSION["ClientEmail"] = $to_email;
				
		$_SESSION["TechID"] = trim($info[11], "`");
//		$_SESSION["UserName"] = trim($info[12], "`");
		$currentTime = trim($info[13], "`");
		
		$currentDate = date("m/d/Y", strtotime($currentTime));
		$currentDateMysql = date("Y-m-d", strtotime($currentTime));
		$currentTime = date("h:i A", strtotime($currentTime));
		
		$wo_id = $info[16];
//		$_SESSION["TechName"] = trim($info[14], "`") . " " . trim($info[15], "`");
		
		$type = $_POST["type"];
		
		$typeTxt = ($type == 0 ? "in" : "out");


		$client = new Core_Api_TechClassPublic;
		if ($type == 1) {
//			caspioUpdate("Work_Orders", "Date_Out, Time_Out","'" . caspioEscape($currentDate) . "','" . caspioEscape($currentTime) . "'", "TB_UNID = '$uniqueID'", false);
//			$db->update("work_orders", array("Date_Out" => $currentDateMysql, "Time_Out" => $currentTime), $db->quoteInto('WIN_NUM=?', $uniqueID));
			$wo = new API_TechWorkOrder();
			$wo->Date_Out = $currentDateMysql;
			$wo->Time_Out = $currentTime;
			$client->updateWorkOrder($_SESSION["UserName"],$_SESSION["Password"],$uniqueID, $wo);

			Create_TimeStamp($uniqueID, $_SESSION["UserName"], "Checked Out Updated to: $currentDate $currentTime (IVR)", $companyID, $clientName, $projectName, "", $_SESSION["TechID"], $_SESSION["TechName"]);
			logIVRContact("IVR Check Out", $companyID, $uniqueID, $_SESSION["TechID"], $_SESSION["outboundType"], "Result: Checked Out $currentDate $currentTime");
			$_SESSION["IVRCallResult"] = LOG_CALLRESULT_CIO_SUCCESS_CHECK_OUT;
			if ($obsync = Core_WorkOrder_OutboundSync_Factory::build($companyID)) {
				$note = new Core_WorkOrder_OutboundSync_Note(NULL, array('WO_ID' => $wo_id), Core_WorkOrder_OutboundSync_Note::AUDIENCE_ALL);
				$obsync->sync($uniqueID, $note, Core_WorkOrder_OutboundSync_Abstract::STATUS_ASSIGNED_CHECKED_OUT, Core_WorkOrder_OutboundSync_Abstract::CONTEXT_TECH, "");
			}			
		}
		else if ($type == 0) {
//			caspioUpdate("Work_Orders", "Date_In, Time_In","'" . caspioEscape($currentDate) . "','" . caspioEscape($currentTime) . "'", "TB_UNID = '$uniqueID'", false);			
//			$db->update("work_orders", array("Date_In" => $currentDateMysql, "Time_In" => $currentTime, "CheckedIn" => 1), $db->quoteInto('WIN_NUM=?', $uniqueID));
			$wo = new API_TechWorkOrder();
			$wo->Date_In = $currentDateMysql;
			$wo->Time_In = $currentTime;
			$wo->CheckedIn = 1;
			$client->updateWorkOrder($_SESSION["UserName"],$_SESSION["Password"],$uniqueID, $wo);
			Create_TimeStamp($uniqueID, $_SESSION["UserName"], "Checked In Updated to: $currentDate $currentTime (IVR)", $companyID, $clientName, $projectName, "", $_SESSION["TechID"], $_SESSION["TechName"]);
			logIVRContact("IVR Check In", $companyID, $uniqueID, $_SESSION["TechID"], $_SESSION["outboundType"], "Result: Checked In $currentDate $currentTime");
			$_SESSION["IVRCallResult"] = LOG_CALLRESULT_CIO_SUCCESS_CHECK_IN;
			if ($obsync = Core_WorkOrder_OutboundSync_Factory::build($companyID)) {
				$note = new Core_WorkOrder_OutboundSync_Note(NULL, array('WO_ID' => $wo_id), Core_WorkOrder_OutboundSync_Note::AUDIENCE_ALL);
				$obsync->sync($uniqueID, $note, Core_WorkOrder_OutboundSync_Abstract::STATUS_ASSIGNED_CHECKED_IN, Core_WorkOrder_OutboundSync_Abstract::CONTEXT_TECH, "");
			}
		}
		else {
			Create_TimeStamp($uniqueID, $_SESSION["UserName"], "Checked In: Tech not on site", $companyID, $clientName, $projectName, "", $_SESSION["TechID"], $_SESSION["TechName"]);
			logIVRContact("IVR Check In", $companyID, $uniqueID, $_SESSION["TechID"], $_SESSION["outboundType"], "Result: Tech not on site");
			$_SESSION["IVRCallResult"] = LOG_CALLRESULT_CIO_SUCCESS_NOT_ON_SITE;
			$_SESSION["CallEmergencyDispatch"] = array($uniqueID, $_SESSION["TechID"], $emergencyDispatch, LOG_CALLTYPE_EMERGENCY_DISPATCH_NOT_ON_SITE_CALL);
		}
	}
	else {
		$foundWO = false;
		$_SESSION["IVRCallResult"] = LOG_CALLRESULT_CIO_INVALID_WOUNID;
		notifyMaintainer("Unable to find WO UNID '$uniqueID' while trying perform check in / out.");
	}
?>
<vxml version="2.1">
	<meta name="maintainer" content="tngo@fieldsolutions.com"/>
    
    <catch event="connection.disconnect.hangup">
		<submit next="handleDisconnect.php?s=<?=session_id()?>" namelist=""/>
		<exit/>
	</catch>
    
    <form>
        <field name="dummy">
            <grammar type="text/gsl">
       			<![CDATA[[
                  [(do not match forty two)]
				]
				]]>
                
            </grammar>        	
            <property name="timeout" value="50ms"/>
        	<prompt>
<?php
	if (!$foundWO):
?>
				Unable to find this work order.
<?php
	elseif ($type == 2):
?>
				Please call the emergency client dispatch number: <break strength="weak"/> <say-as interpret-as="telephone"><?=$emergencyDispatch?></say-as> immediately.
<?php
	else:
?>
            	Thank you for checking <?=$typeTxt?>.
<?php
	endif;
?>
				Thank you and goodbye.
            </prompt>
            <filled>
                <prompt></prompt>
            </filled>
            <noinput>
                <disconnect/>
            </noinput>
            <nomatch>
                <disconnect/>
            </nomatch>            
        </field>
    </form>    
</vxml>
