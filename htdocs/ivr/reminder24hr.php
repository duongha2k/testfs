<?php
//	ini_set("display_errors", 1);
	require_once("ivrheader.php");
	require_once("../library/caspioAPI.php");
	require_once("library/ivrLib.php");
	require_once("../library/woContactLog_ivr.php");
	writeIVRHeader();
	$uniqueID = $_SESSION["uniqueID"];
	$cpa_result = $_SESSION["CPAResult"];
	
//	$_SESSION["TechID"] = 19806;
//	$uniqueID = 7103;
	
	if ($_SESSION["TechID"] == "") die();
	
	// Group Call Code
	if ($_SESSION["GroupedCall"]) {
		if (!isset($_SESSION["GroupedCallWOList"])) { 
			// $wo = caspioSelectAdv("Work_Orders_With_Timezone", "ISNULL(CONVERT(VARCHAR, StartDate, 101), ''), ISNULL(StartTime, ''), ISNULL(City, ''), ISNULL(State, ''), ISNULL(Zipcode, ''), " . TIME_CURRENT_TIME_LOCAL . ", ISNULL(Headline, ''), ISNULL(Tech_Bid_Amount, ''), ISNULL(Amount_Per, 'Site'), Company_ID, Company_Name, Project_Name, " . TIME_STARTTIME_HOURS_BEFORE_START . ", ProjectManagerEmail, TB_UNID", "TB_UNID IN ($uniqueID) AND Tech_ID = '" . caspioEscape($_SESSION["TechID"]) . "'", "", true, "`", "|", false);
			
			$db = Zend_Registry::get('DB');
			$sql = "SELECT IFNULL(DATE_FORMAT(StartDate, '%m/%d/%Y'), ''), IFNULL(StartTime, ''), IFNULL(City, ''), IFNULL(State, ''), IFNULL(Zipcode, ''), " . TIME_CURRENT_TIME_LOCAL . ", IFNULL(Headline, ''), IFNULL(Tech_Bid_Amount, ''), IFNULL(Amount_Per, 'Site'), Company_ID, Company_Name, Project_Name, " . TIME_STARTTIME_HOURS_BEFORE_START . ", ProjectManagerEmail, WIN_NUM FROM work_orders_with_timezone_view WHERE WIN_NUM = ? AND Tech_ID = ?";
			$wo = $db->fetchAll($sql, array($uniqueID, $_SESSION["TechID"]), Zend_Db::FETCH_NUM);

			$_SESSION["GroupedCallWOList"] = $wo;
			$_SESSION["GroupedCallCurrent"] = 0; // used for looping of acceptance, 24 hr, 1 hr.
			$_SESSION["GroupedCallTotal"] = $wo[0] != "" ? sizeof($wo) : 0;
		}
		$wo = $_SESSION["GroupedCallWOList"];
		$currentWO = $_SESSION["GroupedCallCurrent"];
		$totalWO = $_SESSION["GroupedCallTotal"];
		$wo[0] = $wo[$currentWO];
		$_SESSION["GroupedCallCurrent"] += 1;
	}
	else {
//		$wo = caspioSelectAdv("Work_Orders_With_Timezone", "ISNULL(CONVERT(VARCHAR, StartDate, 101), ''), ISNULL(StartTime, ''), ISNULL(City, ''), ISNULL(State, ''), ISNULL(Zipcode, ''), " . TIME_CURRENT_TIME_LOCAL . ", ISNULL(Headline, ''), ISNULL(Tech_Bid_Amount, ''), ISNULL(Amount_Per, 'Site'), Company_ID, Company_Name, Project_Name, " . TIME_STARTTIME_HOURS_BEFORE_START . ", ProjectManagerEmail", "TB_UNID = '$uniqueID' AND Tech_ID = '" . caspioEscape($_SESSION["TechID"]) . "'", "", true, "`", "|", false);
		$db = Zend_Registry::get('DB');
		$sql = "SELECT IFNULL(DATE_FORMAT(StartDate, '%m/%d/%Y'), ''), IFNULL(StartTime, ''), IFNULL(City, ''), IFNULL(State, ''), IFNULL(Zipcode, ''), " . TIME_CURRENT_TIME_LOCAL . ", IFNULL(Headline, ''), IFNULL(Tech_Bid_Amount, ''), IFNULL(Amount_Per, 'Site'), Company_ID, Company_Name, Project_Name, " . TIME_STARTTIME_HOURS_BEFORE_START . ", ProjectManagerEmail, WIN_NUM FROM work_orders_with_timezone_view WHERE WIN_NUM = ? AND Tech_ID = ?";
		$wo = $db->fetchAll($sql, array($uniqueID, $_SESSION["TechID"]), Zend_Db::FETCH_NUM);
	}	
		
	$foundWO = false;
	if (count($wo) > 0)
		$foundWO = true;
	else
		$foundWO = false;
	
	if ($foundWO) {
		$info = $wo[0];
		$startDate = trim($info[0], "`");
		$startTime = trim($info[1], "`");
		$location = htmlentities(trim($info[2], "`") . ", " . trim($info[3], "`") . " " . trim($info[4], "`"));
		$localTime = trim($info[5], "`");
		$headline = htmlentities(trim($info[6], "`"));
		$bidAmount = "$" . trim($info[7], "`") . " per " . trim($info[8], "`");

		$companyID = trim($info[9], "`");
		$clientName = trim($info[10], "`");
		$projectName = trim($info[11], "`");
		$timeDiff = trim($info[12], "`");
		$to_email = trim($info[13], "`");

		// Group Call Code
		if ($_SESSION["GroupedCall"]) {
			$uniqueID = trim($info[14], "`");
			$_SESSION["GroupedCallCurrentUniqueID"] = $uniqueID;
		}
		
		$sdUnixTS = strtotime($startDate);
		$sd = date("m-d-Y", $sdUnixTS);
		
		$startsWhen = "tomorrow";
		
		if ($sd != $tomorrow)
			$startsWhen = date("l", $sdUnixTS);
	}
	else {
		notifyMaintainer("Unable to find WO UNID '$uniqueID'.");
	}
	
	if ($cpa_result != "human") {
		// Log voice mail
//		$_SESSION["IVRCallResult"] = LOG_CALLRESULT_ANSWERED_MACHINE;
		$logID = $_SESSION["GroupedCall"] ? $_SESSION["GroupedCallUniqueIDToLogID"][$uniqueID] : $_SESSION["IVRCallID"];
		setIVRCallResult($logID, LOG_CALLRESULT_ANSWERED_MACHINE);
		logIVRContact("IVR Reminder", $companyID, $uniqueID, $_SESSION["calledNumber"], $_SESSION["outboundType"], "Result: " . LOG_CALLRESULT_ANSWERED_MACHINE);
	}
	else {
//		$_SESSION["IVRCallResult"] = LOG_CALLRESULT_ANSWERED_NO_RESPONSE;
		$logID = $_SESSION["GroupedCall"] ? $_SESSION["GroupedCallUniqueIDToLogID"][$uniqueID] : $_SESSION["IVRCallID"];
		setIVRCallResult($logID, LOG_CALLRESULT_ANSWERED_NO_RESPONSE);
		logIVRContact("IVR Reminder", $companyID, $uniqueID, $_SESSION["calledNumber"], $_SESSION["outboundType"], "Result: " . LOG_CALLRESULT_ANSWERED_NO_RESPONSE);
	}
?>
<vxml version="2.1">
	<meta name="maintainer" content="tngo@fieldsolutions.com"/>
        
    <var name="uniqueID" expr="'<?=$uniqueID?>'"/>
    <var name="sessionId" expr="'<?=session_id()?>'"/>

    <catch event="connection.disconnect.hangup">
		<exit namelist="sessionId"/>
	</catch>
    
<?php
if (!$foundWO):
?>
	<form>
    	<block>
     		Unable to retrieve win number: <?=$uniqueID?>. Field Solutions has been notified of this issue.
            <disconnect />
        </block>
    </form>
<?php
endif;
?>    

<form id="introBlock">
   	<field name="dummy">
        <grammar type="text/gsl">
			<![CDATA[[
			  [(do not match forty two)]
			]
			]]>
		</grammar>        	
       	<property name="timeout" value="100ms"/>

		<prompt>
<?php
if ($cpa_result == "human"):
?>        
<?php
else:
	if ($_SESSION["GroupedCall"]):
?>
		Hello, Field Solutions is calling to remind you that you have $totalWO work orders scheduled to begin within 24 hours or during the weekend.
        
        Login to the Field Solutions website to confirm you will be there.  If we don't hear from you in 30 minutes, we will call back again.
        
        Thank you and goodbye.
<?
	else:
?>
			Hello, Field Solutions is calling to remind you that you are scheduled <?=$startsWhen?> to perform win number <break strength="x-weak"/>
<say-as interpret-as="number" format="digits"><?=$uniqueID?></say-as> <break strength="x-weak"/> to <break strength="x-weak"/> <?=$headline?> scheduled for <break strength="x-weak"/> <?=$startDate?> <break strength="x-weak"/> <?=$startTime?> in <?=$location?>. 

			Login to the Field Solutions website to confirm you will be there.  If we don't hear from you in 30 minutes, we will call back again.
                    
			Thank you and goodbye.
<?php
	endif;
endif;
?>
		</prompt>
		<nomatch count="30">
			<disconnect/>
		</nomatch>
        <filled>
        	<prompt></prompt>
        </filled>
        <nomatch>
        	<reprompt/>
        </nomatch>
		<noinput>
<?php
if ($cpa_result == "human"):
?>        
			<goto next="#choiceForm"/>
<?php
else:
?>
			<disconnect/>
<?php
endif;
?>
		</noinput>
	</field>
</form>

<?php
if ($cpa_result == "human"):
?>
	<form id="choiceForm">
    	<field name="choice">
	        <grammar type="text/gsl">
				<![CDATA[[
				  [(dtmf-1)] {<choice "yes">}
				  [(dtmf-2)] {<choice "no">}
				  [(dtmf-0)] {<choice "repeat">}
				]
				]]>
			</grammar>    
            <property name="inputmodes" value="dtmf"/>
	    	<prompt bargintype="hotword">
<?php
	if ($_SESSION["GroupedCall"]) {
		if ($currentWO == 0)
			$woIntro = "Hello, Field Solutions is calling to remind you have <break strength=\"x-weak\"/>$totalWO work order starting within 24 hours or during this weekend. ";
		$woIntro .= "Work order number " . ($currentWO + 1) . " of $totalWO:";
	}
	else
		$woIntro = "Hello, Field Solutions is calling to remind you that";
?>
				<?=$woIntro?> you are scheduled <?=$startsWhen?> to perform win number <break strength="x-weak"/>
<say-as interpret-as="number" format="digits"><?=$uniqueID?></say-as> <break strength="x-weak"/> to <break strength="x-weak"/> <?=$headline?> scheduled for <break strength="x-weak"/> <?=$startDate?> <break strength="x-weak"/> <?=$startTime?> in <?=$location?>. 
                            
				Press 1 to confirm your commitment to performing this work
                Press 2 if you will not be there as scheduled
                Press 0 to repeat
			</prompt>
            <nomatch count="10">
                <disconnect/>
            </nomatch>
            <noinput count="10">
                <disconnect/>
            </noinput>
            <filled namelist="choice">
            	<if cond="choice == 'yes'">
	            	<goto next="#yesForm"/>
				<elseif cond="choice == 'no'" />
                	<goto next="#noConfirmForm"/>
				<else/>
                	<clear namelist="choice"/>
                    <goto next="#choiceForm"/>
                </if>
            </filled>
        </field>
    </form>
    
    <form id="noConfirmForm">
        <field name="choice">
	        <grammar type="text/gsl">
				<![CDATA[[
				  [(dtmf-1)] {<choice "back out">}
				  [(dtmf-2)] {<choice "reschedule">}
				  [(dtmf-3)] {<choice "accept">}
				]
				]]>
			</grammar>        	
            <property name="inputmodes" value="dtmf"/>
        	<prompt bargeintype="hotword">You have choosen to back out of this work assignment. Press 1 to back out. Press 3 if you wish to confirm your commitment to performing this work.</prompt>
            <nomatch count="10">
                <disconnect/>
            </nomatch>
            <noinput count="10">
                <disconnect/>
            </noinput>
            <filled namelist="choice">
				<if cond="choice == 'back out'">
					<submit next="backOutWO.php?s=<?=session_id()?>" namelist="" method="post"/>
				<elseif cond="choice == 'rescheduleNOTUSED'"/>
					<submit next="rescheduleWO.php?s=<?=session_id()?>" namelist="" method="post"/>
				<elseif cond="choice == 'accept'"/>
					<goto next="#yesForm"/>
				</if>
			</filled>
        </field>        
    </form>
    
    <form id="yesForm">
    	<block>
			<submit next="confirm24hrWO.php?s=<?=session_id()?>" namelist="" method="post"/>
        </block>
    </form>
<?php
endif;
?>
</vxml>
