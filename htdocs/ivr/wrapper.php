<?php
	require_once("../headerStartSession.php");
	$mainVXMLApp = "verifyTech.php";
	$parameterList = "detectedMachine TechID Pin outboundType calledNumber IVRLogID CPAResult uniqueID groupedCall";
	$groupedCall = "false";
	$numWOs = 1;
	if (!is_numeric($_GET["uniqueID"])) {
		$idList = explode(",",$_GET["uniqueID"]);
		$numWOs = sizeof($idList);
		foreach ($idList as $id) {
			if (!is_numeric($id)) {
				die(); // bad input
			}
		}
	}
?>
<?="<?xml version=\"1.0\" encoding=\"UTF-8\"?>"?>
<ccxml version="1.0" xmlns="http://www.w3.org/2002/09/ccxml" xmlns:voxeo="http://community.voxeo.com/xmlns/ccxml">

<meta name="maintainer" content="tngo@fieldsolutions.com"/>

<var name="state_0" expr="'init'"/>
<var name="message_Dlg"/>
<var name="call_0"/>

<var name="dlgStatus" expr="'stopped'"/>
<var name="dlgPrepared" expr="false"/>
<var name="detectedMachine" expr="false"/>
<var name="restartMsg" expr="false"/>
<var name="forcedDlgExit" expr="false"/>
<var name="filename"/>
<var name="TechID" expr="'<?=$_GET["TechID"]?>'"/>
<var name="Pin" expr="'<?=$_GET["Pin"]?>'"/>
<var name="outboundType" expr="'<?=$_GET["outboundType"]?>'"/>
<var name="calledNumber" expr="'<?=$_GET["calledNumber"]?>'"/>
<var name="IVRLogID" expr="'<?=$_GET["IVRLogID"]?>'"/>
<var name="uniqueID" expr="'<?=$_GET["uniqueID"]?>'"/>
<var name="groupedCall" expr="'<?=$groupedCall?>'"/>
<var name="CPAResult" expr="'human'"/>
<var name="savedSession"/>

<var name="disconnected" expr="false"/>

<eventprocessor statevariable="state_0">

<!-- *********************** -->
<!-- App Initialization      -->
<!-- *********************** -->

<transition state="init" event="ccxml.loaded">
  <log expr="'*** CCXML LOADED -- PREPARE INIT DIALOG BEFORE CREATING CALL ***'"/>
  <dialogprepare src="'<?=$mainVXMLApp?>'"
        type="'application/voicexml+xml'"
        dialogid="message_Dlg"
        namelist="<?=$parameterList?>"/>
</transition>

<transition state="init" event="dialog.prepared">
  <log expr="'*** INIT DIALOG PREPARED -- CREATE CALL ***'"/>
  <assign name="state_0" expr="'calling'"/>
  <assign name="dlgPrepared" expr="true"/>
  <createcall dest="'tel:<?=$_GET["numbertodial"]?>'"
            callerid="'tel:+12316227469'"
            timeout="'60s'"
            voxeo-cpa-runtime="'<?=$_GET["voxeo-cpa-runtime"]?>'"
            voxeo-cpa-maxtime="'<?=$_GET["voxeo-cpa-maxtime"]?>'"
            voxeo-cpa-maxsilence="'<?=$_GET["voxeo-cpa-maxsilence"]?>'"
            voxeo-cpa-maskevent="'human,machine,beep,modem,faxtone,sit'"
            voxeo-cpa-maskstop="'modem,faxtone,sit'"
            connectionid="call_0"/>
</transition>

<!-- *********************** -->
<!-- Connection Events -->
<!-- *********************** -->

<transition event="connection.connected">
<!--  <log expr="'*** START CALL RECORDING ***'"/>
  <assign name="filename" expr="event$.connection._RecordCall(100,'TestRecording')"/>-->
  <log expr="'*** CALL CONNECTED ***'"/>
  <send name="'playMsg'" target="session.id"/>
</transition>

<transition event="connection.failed">
  <log expr="'*** CALL FAILED -- REASON [' + event$.reason + '] ***'"/>
  <exit/>
</transition>

<transition event="connection.disconnected">
  <log expr="'*** CALL DISCONNECTED -- EXIT ***'"/>
  <assign name="disconnected" expr="true"/>
  <send name="'user.kill.unconditional'" target="session.id" delay="'60s'"/>
</transition>

<transition event="user.kill.unconditional">
  <log expr="'**** UNCONDITIONAL KILL - EXITING SESSION'"/>
  <exit/>
</transition>

<!-- *********************** -->
<!-- CPA Result Events -->
<!-- *********************** -->

<transition event="voxeo.cpa.result" cond="event$.type == 'beepdetected'">
  <log expr="'*** CPA SAYS [' + event$.type + '] ***'"/>
  <if cond="detectedMachine == true &amp;&amp; dlgStatus == 'started'">
    <log expr="'*** MACHINE PREVIOUSLY DETECTED / DIALOG CURRENTLY STARTED -- STOP MESSAGE ***'"/>
    <assign name="restartMsg" expr="true"/>
    <send name="'stopMsg'" target="session.id"/>
  <elseif cond="detectedMachine == true"/>
    <log expr="'*** NO DIALOG CURRENTLY STARTED -- EVENT IGNORED ***'"/>
  <else/>
    <log expr="'*** MACHINE WAS NOT PREVIOUSLY DETECTED -- EVENT IGNORED ***'"/>
  </if>
</transition>

<transition event="voxeo.cpa.result" cond="event$.type == 'machinedetected'">
  <log expr="'*** CPA SAYS [' + event$.type + '] ***'"/>
  <assign name="detectedMachine" expr="true"/>
  <assign name="CPAResult" expr="'machine'"/>
  <if cond="dlgStatus == 'started'">
    <log expr="'*** DIALOG CURRENTLY STARTED -- STOP MESSAGE ***'"/>
    <assign name="restartMsg" expr="false"/>
    <send name="'stopMsg'" target="session.id"/>
  <else/>
    <log expr="'*** NO DIALOG CURRENTLY STARTED -- WAIT FOR NEXT CPA EVENT ***'"/>
  </if>
</transition>

<transition event="voxeo.cpa.result" cond="event$.type == 'machine' || event$.type == 'beep'">
  <log expr="'*** CPA SAYS [' + event$.type + '] ***'"/>
  <script>
    // machine has been detected
    // lower min duration monotonous signal needed for beep detection
    // this allows less stringent and faster delivery of beepdetected event
    session.connections[call_0]._CPA.Beep.ontime = 1;
  </script>
  <assign name="detectedMachine" expr="true"/>
  <assign name="CPAResult" expr="'machine'"/>
  <assign name="restartMsg" expr="true"/>
  <if cond="dlgStatus == 'started'">
    <log expr="'*** DIALOG CURRENTLY STARTED -- STOP MESSAGE ***'"/>

    <send name="'stopMsg'" target="session.id"/>
  <elseif cond="dlgStatus == 'pending_start'"/>
    <log expr="'*** DIALOG PENDING START -- WAIT FOR MESSAGE ***'"/>
  <else/>
    <log expr="'*** NO DIALOG CURRENTLY STARTED -- PLAY MESSAGE ***'"/>
    <assign name="dlgStatus" expr="'pending_start'"/>
    <send name="'playMsg'" target="session.id"/>
  </if>
</transition>

<transition event="voxeo.cpa.result" cond="event$.type == 'modem' || event$.type == 'faxtone' || event$.type == 'sit' || event$.type == 'unknown'">
  <log expr="'*** CPA EVENT [' + event$.type + '] DETECTED -- EXIT ***'"/>
</transition>

<transition event="voxeo.cpa.result" cond="event$.type == 'human'">
  <log expr="'*** CPA EVENT [' + event$.type + '] IGNORED ***'"/>
</transition>

<!-- *********************** -->
<!-- User Defined Events -->
<!-- *********************** -->

<transition event="playMsg" cond="dlgStatus == 'started'">
  <log expr="'*** DIALOG ALREADY STARTED -- playMsg EVENT IGNORED ***'"/>
</transition>

<transition event="playMsg">
  <assign name="forcedDlgExit" expr="false"/>
  <assign name="dlgStatus" expr="'started'"/>
  <if cond="dlgPrepared == true">
    <dialogstart prepareddialogid="message_Dlg" connectionid="call_0"/>
  <else/>
    <dialogstart src="'<?=$mainVXMLApp?>'"
          type="'application/voicexml+xml'"
          connectionid="call_0"
          dialogid="message_Dlg"
          namelist="<?=$parameterList?>"/>
  </if>
</transition>

<transition event="stopMsg" cond="dlgStatus == 'started'">
  <assign name="forcedDlgExit" expr="true"/>
  <assign name="dlgStatus" expr="'pending_stop'"/>
  <dialogterminate dialogid="message_Dlg" immediate="true"/>
</transition>

<transition event="stopMsg">
  <log expr="'*** NO DIALOG CURRENTLY STARTED -- stopMsg EVENT IGNORED ***'"/>
</transition>

<transition event="confirmed_human">
  <log expr="'*** HUMAN CONFIRMED IN VXML -- TURN CPA OFF ***'"/>
  <script>
    // Turning CPA off
    session.connections[call_0]._CPA.Enabled = false;
  </script>
</transition>

<!-- *********************** -->
<!-- Dialog Events -->
<!-- *********************** -->

<transition event="dialog.exit" cond="forcedDlgExit == true">
  <log expr="'*** DIALOG EXITED ***'"/>
  <assign name="dlgStatus" expr="'stopped'"/>
  <assign name="dlgPrepared" expr="false"/>
  <dialogprepare src="'<?=$mainVXMLApp?>'"
        type="'application/voicexml+xml'"
        dialogid="message_Dlg"
        namelist="<?=$parameterList?>"/>
  <if cond="restartMsg == true">
    <log expr="'*** DELAYED MESSAGE RESTART ***'"/>
    <assign name="dlgStatus" expr="'pending_start'"/>
    <send name="'playMsg'" target="session.id" delay="'1000ms'"/>
  <elseif cond="restartMsg == false"/>
    <log expr="'*** WAIT FOR NEXT CPA EVENT ***'"/>
  </if>
</transition>

<transition event="dialog.exit" cond="forcedDlgExit == false">
  <log expr="'*** MESSAGE COMPLETE -- EXIT ***'"/>
  <if cond="disconnected == false">
	  <disconnect connectionid="call_0"/>
  </if>
  <log expr="'event$.values.sessionId = ' + event$.values.sessionId"/>
  <assign name="savedSession" expr="event$.values.sessionId"/>
  <send name="'user.call.cleanup'" target="'handleDisconnect.php'" targettype="'basichttp'" namelist="savedSession"/>
  <send name="'user.kill.unconditional'" target="session.id" delay="'<?=15 * $numWOs?>s'"/>
</transition>

<transition event="user.kill.unconditional">
  <log expr="'**** UNCONDITIONAL KILL - EXITING SESSION'"/>
  <exit/>
</transition>

<transition event="dialog.prepared">
  <log expr="'*** DIALOG PREPARED -- WAIT FOR playMsg ***'"/>
  <assign name="dlgPrepared" expr="true"/>
</transition>

<!-- *********************** -->
<!-- Exception Events -->
<!-- *********************** -->

<transition event="error.*">
  <log expr="'*** AN ERROR HAS OCCURRED [' + event$.name + '] ***'"/>
  <log expr="'*** ERROR REASON [' + event$.reason + '] ***'"/>
  <exit/>
</transition>

</eventprocessor>
</ccxml>