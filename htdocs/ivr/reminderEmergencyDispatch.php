<?php
//	ini_set("display_errors", 1);
	require_once("ivrheader.php");
	require_once("../library/caspioAPI.php");
	require_once("library/ivrLib.php");
	require_once("../library/woContactLog_ivr.php");
	writeIVRHeader();
	$uniqueID = $_SESSION["uniqueID"];
	$cpa_result = $_SESSION["CPAResult"];
	
//	$_SESSION["TechID"] = 19806;
//	$uniqueID = 7103;
	
	if ($_SESSION["TechID"] == "") die();
	
//	$wo = caspioSelectAdv("Work_Orders_With_Timezone", "ISNULL(CONVERT(VARCHAR, StartDate, 101), ''), ISNULL(StartTime, ''), ISNULL(City, ''), ISNULL(State, ''), ISNULL(Zipcode, ''), " . TIME_CURRENT_TIME_LOCAL . ", ISNULL(Headline, ''), ISNULL(Tech_Bid_Amount, ''), ISNULL(Amount_Per, 'Site'), Company_ID, Company_Name, Project_Name, " . TIME_STARTTIME_HOURS_BEFORE_START . ", ProjectManagerEmail, NoShow_Tech, Tech_ID", "TB_UNID = '$uniqueID' AND Tech_ID = '" . caspioEscape($_SESSION["TechID"]) . "'", "", true, "`", "|", false);

	$db = Zend_Registry::get('DB');
	$sql = "SELECT IFNULL(DATE_FORMAT(StartDate, '%m/%d/%Y'), ''), IFNULL(StartTime, ''), IFNULL(City, ''), IFNULL(State, ''), IFNULL(Zipcode, ''), " . TIME_CURRENT_TIME_LOCAL . ", IFNULL(Headline, ''), IFNULL(Tech_Bid_Amount, ''), IFNULL(Amount_Per, 'Site'), Company_ID, Company_Name, Project_Name, " . TIME_STARTTIME_HOURS_BEFORE_START . ", ProjectManagerEmail, NoShow_Tech, Tech_ID FROM work_orders_with_timezone_view WHERE WIN_NUM = ? AND Tech_ID = ?";
	$wo = $db->fetchAll($sql, array($uniqueID, $_SESSION["TechID"]), Zend_Db::FETCH_NUM);

	$foundWO = false;
	$backedOut = false;
	if (count($wo) > 0)
		$foundWO = true;
	else
		$foundWO = false;
	
	if ($foundWO) {
		$info = $wo[0];
		$startDate = trim($info[0], "`");
		$startTime = trim($info[1], "`");
		$location = htmlentities(trim($info[2], "`") . ", " . trim($info[3], "`") . " " . trim($info[4], "`"));
		$localTime = trim($info[5], "`");
		$headline = trim($info[6], "`");
		$bidAmount = "$" . trim($info[7], "`") . " per " . trim($info[8], "`");

		$companyID = trim($info[9], "`");
		$clientName = trim($info[10], "`");
		$projectName = trim($info[11], "`");
		$timeDiff = trim($info[12], "`");
		$to_email = trim($info[13], "`");

		$noshow_tech = trim($info[14], "`");
		$assignedTech = trim($info[15], "`");
		
		$backedOut = false;
		if ($noshow_tech == $assignedTech)
			$backedOut = true;
	}
	else {
		notifyMaintainer("Unable to find WO UNID '$uniqueID'.");
	}
	if ($cpa_result != "human") {
		$_SESSION["IVRCallResult"] = LOG_CALLRESULT_ANSWERED_MACHINE;
		logIVRContact("IVR Reminder", $companyID, $uniqueID, $_SESSION["calledNumber"], $_SESSION["outboundType"], "Result: " . LOG_CALLRESULT_ANSWERED_MACHINE);
	}
	else {
		$_SESSION["IVRCallResult"] = LOG_CALLRESULT_ANSWERED_SUCCESS;
		logIVRContact("IVR Reminder", $companyID, $uniqueID, $_SESSION["calledNumber"], $_SESSION["outboundType"], "Result: " . LOG_CALLRESULT_ANSWERED_SUCCESS);
	}
	
?>
<vxml version="2.1">
	<meta name="maintainer" content="tngo@fieldsolutions.com"/>
        
    <var name="uniqueID" expr="'<?=$uniqueID?>'"/>

	<var name="sessionId" expr="'<?=session_id()?>'"/>
    
    <catch event="connection.disconnect.hangup">
		<exit namelist="sessionId"/>
	</catch>
    
<?php
if (!$foundWO):
?>
	<form>
    	<block>
     		Unable to retrieve win number: <?=$uniqueID?>. Field Solutions has been notified of this issue.
            <disconnect />
        </block>
    </form>
<?php
endif;
?>    

    <form id="introBlock">
        <field name="choice">
            <grammar type="text/gsl">
                <![CDATA[[
				  [(dtmf-1)] {<choice "repeat">}
                ]
                ]]>
            </grammar>        	
            <property name="inputmodes" value="dtmf"/>
    
            <prompt>
                Hello, this is Field Solutions calling regarding win number <break strength="x-weak"/><prosody rate="slow">
    <say-as interpret-as="number" format="digits"><?=$uniqueID?></say-as></prosody> scheduled to begin today at <?=$startTime?>. 
    
    <?php
    if ($_SESSION["outboundType"] == LOG_CALLTYPE_EMERGENCY_DISPATCH_BACK_OUT_CALL):
    ?>
    <?php
	    if ($backedOut):
    ?>
                    The tech has indicated they will back out of the work order.
    <?php
	    else:
    ?>
                    We have not been able to contact this tech to confirm they will be on site.
    <?php
	    endif;
    ?>
    <?php
    else:
    ?>
    				The tech has indicated they will not be on site at check in time.
    <?php
    endif;
    ?>
    
					Again this is regarding win number <break strength="x-weak"/><prosody rate="slow">
    <say-as interpret-as="number" format="digits"><?=$uniqueID?></say-as></prosody>
    
				    Press 1 to repeat this message.
    
            </prompt>
            <filled namelist="choice">
				<if cond="choice == 'repeat'">
                	<clear namelist="choice"/>
                    <goto next="#introBlock"/>
				</if>
			</filled>
            <nomatch>
				<prompt>Press 1 to repeat this message.</prompt>
            </nomatch>
            <noinput count="1">
				<prompt>Press 1 to repeat this message.</prompt>
            </noinput>
            <noinput count="2">
                <prompt>Thank you and goodbye.</prompt>
                <disconnect/>
            </noinput>
            </field>
    </form>
</vxml>
