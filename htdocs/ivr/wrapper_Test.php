<?php
	require_once("../headerStartSession.php");
	$_SESSION["loggedIn"] = "yes";
	require_once("../library/caspioAPI.php");
	require_once("library/ivrLib.php");
	ini_set("display_errors", 1);
	$uniqueID = $_GET["uniqueID"];
	$phone = "";
	if (isset($_POST["uniqueID"]) && is_numeric($_POST["uniqueID"])) {
		$uniqueID = $_POST["uniqueID"];
		$outboundType = $_POST["outboundType"];
		$phone = $_POST["phone"];
//		$woList = caspioSelectAdv("Work_Orders", "Tech_ID, Company_ID, (SELECT PrimaryPhone FROM TR_Master_List WHERE TechID = Tech_ID)", "TB_UNID = '$uniqueID'", "", false, "`", "|", false);		
		$db = Zend_Registry::get('DB');

		$sqlJoin = "`work_orders` LEFT JOIN `TechBankInfo` ON `TechBankInfo`.`id` = (SELECT id FROM `TechBankInfo` WHERE  `TechBankInfo`.`TechID` = CAST(`work_orders`.`Tech_ID` AS char) LIMIT 1)";
		$sql = "SELECT `work_orders`.`Tech_ID`, `work_orders`.`Company_ID`, `TechBankInfo`.`PrimaryPhone` FROM $sqlJoin WHERE WIN_NUM = '$uniqueID'";
                $woList = $db->fetchAll($sql, array(), Zend_Db::FETCH_NUM);

		if (count($woList[0]) > 0) {
			$info = $woList[0];
			$techID = trim($info[0], "`");
			$companyID = trim($info[1], "`");
			
			if ($phone == "")
				$phone = trim($info[2], "`");
											
			$phone = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
				
			$phone = str_replace(array("-", ".", " ", "(", ")"), array("", "", "", "", ""), $phone);
			
			if (strpos($phone, "+1") === FALSE)
				$phone = "+1" . $phone;
			
			$logID = logIVRCall($uniqueID, $techID, $outboundType, "Pending", 0);
			
			echo  "Calling $phone...<br/>";
			ob_flush();
			flush();
			
			//TOKEN_OUTBOUND_REMINDER
			// b9fd2228e13b0d488d537236919bc04731f32861aa1698b9b8ccdf599f657f7f258416f545536a366fbc795b
			$callResult = outboundCall(TOKEN_OUTBOUND_REMINDER, $phone, array("TechID" => $techID, "Pin" => "outbound", "outboundType" => $outboundType, "uniqueID" => $uniqueID, "IVRLogID" => $logID, "calledNumber" => $phone, "voxeo-cpa-maxtime" => "3400", "voxeo-cpa-maxsilence" => "1200", "voxeo-cpa-runtime" => "20000") );
	
			echo  "Call Result: $callResult";
			ob_flush();
			flush();

			$msg = "Phone: $phone
			TechID: $techID
			CallResult: $callResult
			Unique ID: $uniqueID
			";
			
			smtpMail("IVR Application", "nobody@fieldsolutions.com", "tngo@fieldsolutions.com", "Reminder Call Type - $outboundType (ID: $logID)", $msg, nl2br($msg), "IVR Application");
	
			processCallResult($uniqueID, $companyID, $phone, $callResult, $logID, $outboundType);
		}
	}
?>
<form action="<?=$_SERVER["PHP_SELF"]?>" method="post"/>
	Unique WO ID: <input type="text" name="uniqueID" size="8" value="<?=$uniqueID?>"/><br/>
	Phone Number (leave blank for Tech's): <input type="text" name="phone" size="12" value="<?=$phone?>"/><br/>
    Call Type:
    <select name="outboundType">
    	<option value="<?=LOG_CALLTYPE_ACCEPTANCE_CALL?>">Acceptance</option>
    	<option value="<?=LOG_CALLTYPE_24_HR_CALL?>">24 Hrs</option>
    	<option value="<?=LOG_CALLTYPE_1_HR_CALL?>">1 Hr</option>
    	<option value="<?=LOG_CALLTYPE_NOT_MARKED_COMPLETE_CALL?>">Not Marked Complete</option>
    	<option value="<?=LOG_CALLTYPE_INCOMPLETE_CALL?>">Incomplete</option>
    </select>
    <br/>
    <input type="submit" value="Call"/>
</form>
