<?php
//	ini_set("display_errors", 1);
	set_time_limit(600);
	if (!empty($_POST["savedSession"]) || !empty($_GET['s'])) {
		$sid = !empty($_POST["savedSession"]) ? $_POST["savedSession"] : $_GET['s'];
		session_id($sid);
		require_once("../headerStartSession.php");
	}
	$_SESSION["loggedIn"] = "yes";
	require_once("ivrheader.php");
	require_once("../library/caspioAPI.php");
	require_once("library/ivrLib.php");
	require_once("../library/woContactLog_ivr.php");
	require_once("../library/timeStamps2.php");
	$_SESSION["loggedIn"] = "no";
	writeIVRHeader();
	
	$logID = $_SESSION["IVRLogID"];
	
	if ($logID != "") {
		if ($_SESSION["GroupedCall"]) {
			$logIDs = explode(",", $logID);
			$uniqueIDs = explode(",", $_SESSION["uniqueID"]);
			$index = 0;
			foreach ($_SESSION["IVRCallResult"] as $result) {
				// updates call result and time
				logIVRCallUpdate($logIDs[$index], $result, time() - $_SESSION["callBegin"], $_SESSION["TechID"], $uniqueIDs[$index]);
				
				// process any Queued Contact Logs
				proccessQueuedIVRContactLog();

				// notification email to clients / SDM
				doNotifications($uniqueIDs[$index], $_SESSION["outboundType"], $result, $logIDs[$index]);
				$index++;
			}
		}
		else {		
			// updates call result and time
			logIVRCallUpdate($logID, $_SESSION["IVRCallResult"], time() - $_SESSION["callBegin"], $_SESSION["TechID"], $_SESSION["uniqueID"]);
			
			// process any Queued Contact Logs
			proccessQueuedIVRContactLog();
			// notification email to clients / SDM
			doNotifications($_SESSION["uniqueID"], $_SESSION["outboundType"], $_SESSION["IVRCallResult"], $logID);
		}
	}
	

?>
<vxml version="2.1">
    <form>
        <block>
            <exit/>
        </block>
    </form>
</vxml>