<?php
//	ini_set("display_errors", 1);
	require_once("ivrheader.php");
	require_once("../library/caspioAPI.php");
	require_once("../library/woContactLog_ivr.php");
	require_once("../library/timeStamps2.php");
	require_once("library/ivrLib.php");
	writeIVRHeader();
	$uniqueID = $_SESSION["GroupedCall"] ? $_SESSION["GroupedCallCurrentUniqueID"] : $_SESSION["uniqueID"];
	
//	$_SESSION["TechID"] = 19806;
//	$uniqueID = 7103;
	if ($_SESSION["TechID"] == "") die();
	
//	$wo = caspioSelectAdv("Work_Orders_With_Timezone", "Company_ID, Company_Name, Project_Name, " . TIME_STARTTIME_HOURS_BEFORE_START . ", ProjectManagerEmail", "TB_UNID = '$uniqueID' AND Tech_ID = '" . caspioEscape($_SESSION["TechID"]) . "'", "", true, "`", "|", false);
	$db = Zend_Registry::get('DB');
	$sql = "SELECT Company_ID, Company_Name, Project_Name, " . TIME_STARTTIME_HOURS_BEFORE_START . ", ProjectManagerEmail FROM work_orders_with_timezone_view  WHERE WIN_NUM = ? AND Tech_ID = ?";
	$wo = $db->fetchAll($sql, array($uniqueID, $_SESSION["TechID"]), Zend_Db::FETCH_NUM);
		
	$foundWO = false;
	if (count($wo) > 0)
		$foundWO = true;
	else
		$foundWO = false;
	
	if ($foundWO) {
		$info = $wo[0];
		$companyID = trim($info[0], "`");
		$clientName = trim($info[1], "`");
		$projectName = trim($info[2], "`");
		$timeDiff = trim($info[3], "`");
		$to_email = trim($info[4], "`");
		markAcceptWO($uniqueID, $companyID, $clientName, $projectName);
		logIVRContact("IVR Reminder", $companyID, $uniqueID, $_SESSION["calledNumber"], LOG_CALLTYPE_ACCEPTANCE_CALL, "Result: Tech Accepted");
//		$_SESSION["IVRCallResult"] = LOG_CALLRESULT_ANSWERED_SUCCESS;
		$logID = $_SESSION["GroupedCall"] ? $_SESSION["GroupedCallUniqueIDToLogID"][$uniqueID] : $_SESSION["IVRCallID"];
		setIVRCallResult($logID, LOG_CALLRESULT_ANSWERED_SUCCESS);
	}
	else {
		notifyMaintainer("Unable to find WO UNID '$uniqueID' while trying to accept.");
	}
?>
<vxml version="2.1">
	<meta name="maintainer" content="tngo@fieldsolutions.com"/>
    <var name="sessionId" expr="'<?=session_id()?>'"/>

    <catch event="connection.disconnect.hangup">
		<exit namelist="sessionId"/>
	</catch>

<?php
if (!$foundWO):
?>
	<form>
	   	<field name="dummy">
            <grammar type="text/gsl">
                <![CDATA[[
                  [(do not match forty two)]
                ]
                ]]>
            </grammar>        	
        	<property name="timeout" value="1s"/>
        	<prompt>
	     		Unable to retrieve win number: <?=$uniqueID?>. Field Solutions has been notified of this issue.
            </prompt>
            <filled>
                <prompt></prompt>
            </filled>
            <noinput>
            	<disconnect/>
            </noinput>
        </field>
    </form>
<?php
endif;
?>    

<form id="introBlock">
   	<field name="dummy">
        <grammar type="text/gsl">
			<![CDATA[[
			  [(do not match forty two)]
			]
			]]>
		</grammar>        	
       	<property name="timeout" value="50ms"/>
    	<prompt>
<?php
	if ($_SESSION["GroupedCall"] && $_SESSION["GroupedCallCurrent"] != $_SESSION["GroupedCallTotal"]):
?>
		Your acceptance has been confirmed. Proceeding to the next work order.
<?php
	else:
?>
		Your acceptance has been confirmed.  Thank you and goodbye
<?php
	endif;
?>
        </prompt>
		<filled>
        	<prompt></prompt>
        </filled>
        <noinput>
<?php
	if ($_SESSION["GroupedCall"] && $_SESSION["GroupedCallCurrent"] != $_SESSION["GroupedCallTotal"]):
?>
			<submit next="<?=$_SESSION["outboundScript"]?>?s=<?=session_id()?>" namelist="" method="post" />
<?php
	else:
?>
        	<disconnect/>
<?php
	endif;
?>
        </noinput>
	</field>
</form>
</vxml>
