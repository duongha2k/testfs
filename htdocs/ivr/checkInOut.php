<?php
//	ini_set("display_errors", 1);
	require_once("ivrheader.php");
	$_SESSION["loggedIn"] = "yes";
	require_once("../library/caspioAPI.php");
	require_once("library/ivrLib.php");
	$_SESSION["loggedIn"] = "no";
	writeIVRHeader();
	$uniqueID = $_POST["uniqueID"];
	
	$techID = $_POST["TechID"];
	
//	$uniqueID = "7103";
//	$techID = "19806";

	if ($uniqueID == "" || $techID == "") die();
	
	
// $woList = caspioSelectAdv("Work_Orders_With_Timezone", "ISNULL(CONVERT(VARCHAR, StartDate, 101), ''), ISNULL(StartTime, ''), ISNULL(City, ''), ISNULL(State, ''), ISNULL(Zipcode, ''), ISNULL(Headline, ''), Company_ID, Company_Name, Project_Name, ProjectManagerEmail, EmergencyPhone, Tech_ID, CheckInCall, CheckOutCall, ISNULL(Time_In, ''), ISNULL(Time_Out, ''), ReminderAll", "TB_UNID = '" . caspioEscape($uniqueID) . "' AND Tech_ID = '" . caspioEscape($techID) . "' AND " . CRITERIA_ALL_CALL, "", true, "`", "|", false);
	$db = Zend_Registry::get('DB');
	$sql = "SELECT IFNULL(DATE_FORMAT(StartDate, '%m/%d/%Y'), ''), IFNULL(StartTime, ''), IFNULL(City, ''), IFNULL(State, ''), IFNULL(Zipcode, ''), IFNULL(Headline, ''), Company_ID, Company_Name, Project_Name, ProjectManagerEmail, EmergencyPhone, Tech_ID, CheckInCall, CheckOutCall, IFNULL(Time_In, ''), IFNULL(Time_Out, ''), ReminderAll FROM work_orders_with_timezone_view_leftjoinzip WHERE WIN_NUM = ? AND Tech_ID = ? AND " . CRITERIA_ALL_CALL;
	$woList = $db->fetchAll($sql, array($uniqueID, $techID), Zend_Db::FETCH_NUM);
		
	$foundWO = false;
	if (count($woList) > 0) {
		$foundWO = true;
		$info = $woList[0];
		$startDate = trim($info[0], "`");
		$startTime = trim($info[1], "`");
		$location = htmlentities(trim($info[2], "`") . ", " . trim($info[3], "`") . " " . trim($info[4], "`"));
		$headline = htmlentities(trim($info[5], "`"));

		$companyID = trim($info[6], "`");
		$clientName = trim($info[7], "`");
		$projectName = trim($info[8], "`");
		$to_email = trim($info[9], "`");
		$emergencyDispatch = trim($info[10], "`");
		
		$_SESSION["TechID"] = trim($info[11], "`");
		$checkInCall = (trim($info[12], "`")  == 1);
		$checkOutCall = (trim($info[13], "`")  == 1);
		$timeIn = trim($info[14], "`");
		$timeOut = trim($info[15], "`");
		$reminderAll = (trim($info[16], "`")  == 1);
		
		$checkInCall = ($reminderAll ? $reminderAll : $checkInCall);
		$checkOutCall = ($reminderAll ? $reminderAll : $checkOutCall);
		
		$acsCheckInOutRequired = $checkInCall || $checkOutCall;
				
		$noOptions = ($timeIn != "" && $timeOut != "") || ($timeIn != "" && !$checkOutCall) || ($timeOut != "" && !$checkInCall);
		
		$info = caspioSelectAdv("TR_Master_List", TECH_SESSION_INFO, "TechID = '" . caspioEscape($_SESSION["TechID"]) . "'", "", false, "`", "|", false);

		$verified = ($info[0] != "");
		if ($verified) {
			// set session info
			$info = explode("|", $info[0]);
			setTechSession($info);
			$_SESSION["uniqueID"] = $uniqueID;
		}		
	}
	else {
		$_SESSION["IVRCallResult"] = LOG_CALLRESULT_CIO_INVALID_WOUNID;
		$foundWO = false;
	}

?>
<vxml version="2.1">
	<meta name="maintainer" content="tngo@fieldsolutions.com"/>
    <var name="failedID" expr="<?=$uniqueID?>" />

    <catch event="connection.disconnect.hangup">
		<submit next="handleDisconnect.php?s=<?=session_id()?>" namelist=""/>
		<exit/>
	</catch>

    <form>
    	<block>
<?php
	if ($foundWO):
		if ($acsCheckInOutRequired && !$noOptions):
			if ($verified):
				$_SESSION["SelectWOTries"] = 0;
?>
	            <goto next="#confirmWO"/>
<?php
			else:
?>
					I was unable to access your account via I V R at this time. Please visit www.fieldsolutions.com to update your status regarding upcomming work.
		            <disconnect/>
<?php
			endif;
		elseif ($acsCheckInOutRequired):
			$_SESSION["SelectWOTries"] = 0;
?>
				<prompt>ACS check in is not available for this work order because it is already entered.  If this is a return visit to the site, enter your check in time manually on the website when you close out the work order. Goodbye.</prompt>
	            <disconnect/>
<?php
		else:
			$_SESSION["SelectWOTries"] = 0;
?>
				<prompt>ACS check in is not required on this work order.  Read your work order document for the correct check in procedure. Goodbye</prompt>
	            <disconnect/>
<?php
		endif;
	elseif ($_SESSION["SelectWOTries"] < 2):
			++$_SESSION["SelectWOTries"];
?>
			<prompt>We're sorry, the WIN number or Technician ID do not match.  Please try entering the WIN number and Tech ID again.</prompt>
			<submit next="selectWOCIO.php?s=<?=session_id()?>"/>
<?php
	else:
?>
			<prompt>Your entry still does not match so you cannot check in with this system.  Please enter your check in time manually on the website when you close out the work order</prompt>
			<disconnect/>
<?php
	endif;
?>
        </block>
    </form>
   
    <form id="confirmWO">
    	<field name="choice">
	        <grammar type="text/gsl">
       			<![CDATA[[
                      [(dtmf-1) yes] {<choice "yes">}
                      [(dtmf-2) no] {<choice "no">}
                      [(dtmf-0) repeat (start over)] {<choice "repeat">}
				]
				]]>
				
			</grammar>  
			<property name="inputmodes" value="dtmf"/>
   	    	<prompt bargeintype="hotword">
				This is a valid Work Order. Please confirm that it is the correct work order. 
                
				This is for <?=$headline?> being performed at <?=$location?>.
                
                Press 1 to confirm.
                Press 2 to not confirm and start over.
                
	        </prompt>
            <filled namelist="choice">
            	<if cond="choice == 'yes'">
	            	<goto next="#performCIO"/>
				<elseif cond="choice == 'no'" />
                	<submit next="selectWOCIO.php?s=<?=session_id()?>" />
				<else/>
                	<clear namelist="choice"/>
                    <goto next="#confirmWO"/>
                </if>
            </filled>            
		</field>
    </form>
    <form id="performCIO">
    	<field name="type">
	        <grammar type="text/gsl">
       			<![CDATA[[
<?php
		if ($checkInCall):
?>
				  [(dtmf-1) (check in)] {<type "0">}
<?php
		endif;
		if ($checkOutCall):
?>
				  [(dtmf-2) (check out)] {<type "1">}
<?php
		endif;
		if ($checkInCall):
?>
				  [(dtmf-3) (not on site)] {<type "2">}
<?php
		endif;
?>
                ]
                ]]>				
			</grammar>
			<property name="inputmodes" value="dtmf"/>
            <prompt>
<?php
		if ($checkInCall):
?>
				Press 1 to check in.
<?php
		endif;
		if ($checkOutCall):
?>
				Press 2 to check out.
<?php
		endif;
		if ($checkInCall):
?>
                Press 3 if you are not on site
<?php
		endif;
?>
            </prompt>
            <filled namelist="type">
               	<submit next="performCheckInOut.php?s=<?=session_id()?>" namelist="type" method="post" />
            </filled>
        </field>
    </form>
</vxml>
