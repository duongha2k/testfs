<?php
//	require_once("ivrheader.php");
//	ini_set("display_errors",1);
	$_SESSION["loggedIn"] = "yes";
	require_once("../library/caspioAPI.php");
	require_once("library/ivrLib.php");

	// CRITERIA FOR CALLS - ACCEPTANCE
	// Acceptance calls at 10 AM Local Time - Repeat 3 times at 30 min interval
	//define("CRITERIA_ACCEPTANCE_CALL_T", CRITERIA_ALL_CALL . " AND " . WO_ASSIGNED_MORE_THAN_24_HRS . " AND " . WO_NOT_REVIEWED . " AND " . WO_NOT_MARK_COMPLETED . " AND (" . WO_REMINDER_ACCEPTANCE . " OR " . WO_REMINDER_ALL . ") AND " . ACCEPTANCE_CALL_WO_STARTS_OVER_24HR . " AND " . ACCEPTANCE_CALL_IGNORE_TODAY_WO_IF_REMINDER_ONE_HR . " AND " . REDIAL_CRTERIA_ACCEPTANCE . " AND " . REDIAL_ACCEPTANCE_NO_SUCCESS_YET . " AND " . REDIAL_ACCEPTANCE_BELOW_RETRY_LIMIT); // PROBLEM WITH WO ASSIGNED FRIDAY?
	define("CRITERIA_ACCEPTANCE_CALL_T", CRITERIA_ALL_CALL . " AND " . WO_ASSIGNED_MORE_THAN_24_HRS . " AND " . WO_NOT_REVIEWED . " AND " . WO_NOT_MARK_COMPLETED . " AND (" . WO_REMINDER_ACCEPTANCE . " OR " . WO_REMINDER_ALL . ") AND " . ACCEPTANCE_CALL_WO_STARTS_OVER_24HR . " AND " . ACCEPTANCE_CALL_IGNORE_TODAY_WO_IF_REMINDER_ONE_HR . " AND " . REDIAL_CRITERIA_ACCEPTANCE); // PROBLEM WITH WO ASSIGNED FRIDAY?
	// END CRITERIA FOR CALLS - ACCEPTANCE

	// CRITERIA FOR CALLS - 24 HR
	// 24 Hr calls at 10 AM Local Time w/ adjustments for weekend and Early Morning - Repeat 3 times at 30 min interval
	//define("CRITERIA_24_HR_CALL_T", CRITERIA_ALL_CALL . " AND " . WO_NOT_MARK_COMPLETED . " AND (" . WO_REMINDER_24_HR . " OR " . WO_REMINDER_ALL . ") AND " . WO_STARTS_24_HR_ADJUST_WEEKEND_EARLY_MORNING . " AND " . REDIAL_24_HR_NO_SUCCESS_YET . " AND " . REDIAL_24_HR_BELOW_RETRY_LIMIT);
	define("CRITERIA_24_HR_CALL_T", CRITERIA_ALL_CALL . " AND " . WO_NOT_MARK_COMPLETED . " AND (" . WO_REMINDER_24_HR . " OR " . WO_REMINDER_ALL . ") AND " . WO_STARTS_24_HR_ADJUST_WEEKEND_EARLY_MORNING . " AND is.Success=0  AND is.RedialCount < " . REDIAL_24_HR_RETRY_LIMIT);
	// END CRITERIA FOR CALLS - 24 HR

	// CRITERIA FOR CALLS - 1 HR
	// 1 Hr calls at Local Time - Repeat 3 times at 10 min interval
	//define("CRITERIA_1_HR_CALL_T", CRITERIA_ALL_CALL . " AND " . WO_NOT_MARK_COMPLETED . " AND (" . WO_REMINDER_1_HR . " OR " . WO_REMINDER_ALL . ") AND " . WO_STARTS_1_HR_LOCAL_TIME . " AND NOT " . WO_STARTED . " AND " . REDIAL_1_HR_NO_SUCCESS_YET . " AND " . REDIAL_1_HR_BELOW_RETRY_LIMIT);
	define("CRITERIA_1_HR_CALL_T", CRITERIA_ALL_CALL . " AND " . WO_NOT_MARK_COMPLETED . " AND (" . WO_REMINDER_1_HR . " OR " . WO_REMINDER_ALL . ") AND " . WO_STARTS_1_HR_LOCAL_TIME . " AND NOT " . WO_STARTED . " AND is.Success=0  AND is.RedialCount < " . REDIAL_1_HR_RETRY_LIMIT);
	// CRITERIA FOR CALLS - 1 HR

	// CRITERIA FOR CALLS - EMERGENCY DISPATCH
	// Emergency Dispatch - Repeat 3 times at 10 min interval
//	define("EMERGENCY_DISPATCH_CALL_", "NOT(" . WO_REMINDER_1_HR . " AND " . WO_STARTS_TODAY . ")");
	define("CRITERIA_EMERGENCY_DISPATCH_BACK_OUT_CALL_T", CRITERIA_ALL_CALL . " AND " . WO_NOT_MARK_COMPLETED . " AND " . WO_STARTS_1_HR_LOCAL_TIME . " AND " . REDIAL_CRITERIA_EMERGENCY_DISPATCH_BACK_OUT);
	// CRITERIA FOR CALLS - EMERGENCY DISPATCH

	// CRITERIA FOR CALLS - EMERGENCY DISPATCH
	// Emergency Dispatch - Repeat 3 times at 10 min interval
//	define("EMERGENCY_DISPATCH_CALL_", "NOT(" . WO_REMINDER_1_HR . " AND " . WO_STARTS_TODAY . ")");
	define("CRITERIA_EMERGENCY_DISPATCH_NOT_ON_SITE_CALL_T", CRITERIA_ALL_CALL . " AND " . WO_NOT_MARK_COMPLETED . " AND " . WO_STARTS_1_HR_LOCAL_TIME . " AND " . REDIAL_CRITERIA_EMERGENCY_DISPATCH_NOT_ON_SITE);
	// CRITERIA FOR CALLS - EMERGENCY DISPATCH

/*	// Check IN / OUT at 10 AM Local Time
	define("CRITERIA_CHECK_IN_OUT_CALL", CRITERIA_ALL_CALL . " AND " . WO_NOT_MARK_COMPLETED . " AND " . WO_REMINDER_CHECK_IN_OUT . " AND " . WO_ENDED_YESTERDAY);*/

	// CRITERIA FOR CALLS - NOT MARKED COMPLETE
	// Not Marked Complete calls at 10 AM Local Time - Repeat 3 times at daily interval
	//define("CRITERIA_NOT_MARKED_COMPLETE_CALL_T", CRITERIA_ALL_CALL . " AND " . WO_NOT_MARK_COMPLETED . " AND (" . WO_REMINDER_NOT_MARKED_COMPLETE . " OR " . WO_REMINDER_ALL . ") AND " . WO_ENDED_BEFORE_TODAY . " AND " . REDIAL_NOT_MARKED_COMPLETE_NO_SUCCESS_YET . " AND " . REDIAL_NOT_MARKED_COMPLETE_BELOW_RETRY_LIMIT);
	define("CRITERIA_NOT_MARKED_COMPLETE_CALL_T", CRITERIA_ALL_CALL . " AND " . WO_NOT_MARK_COMPLETED . " AND (" . WO_REMINDER_NOT_MARKED_COMPLETE . " OR " . WO_REMINDER_ALL . ") AND " . WO_ENDED_BEFORE_TODAY . " AND is.Success=0  AND is.RedialCount < " . REDIAL_NOT_MARKED_COMPLETE_RETRY_LIMIT);
	// END CRITERIA FOR CALLS - NOT MARKED COMPLETE

	// CRITERIA FOR CALLS - INCOMPLETE
	// Incomplete calls at 10 AM Local Time - Repeat 3 times at daily interval
	//define("CRITERIA_INCOMPLETE_CALL_T", CRITERIA_ALL_CALL . " AND " . WO_NOT_MARK_COMPLETED . " AND (" . WO_REMINDER_INCOMPLETE . " OR " . WO_REMINDER_ALL . ") AND " . WO_INCOMPLETE_BEFORE_TODAY . " AND " . REDIAL_INCOMPLETE_NO_SUCCESS_YET . " AND " . REDIAL_INCOMPLETE_BELOW_RETRY_LIMIT);
	define("CRITERIA_INCOMPLETE_CALL_T", CRITERIA_ALL_CALL . " AND " . WO_NOT_MARK_COMPLETED . " AND (" . WO_REMINDER_INCOMPLETE . " OR " . WO_REMINDER_ALL . ") AND " . WO_INCOMPLETE_BEFORE_TODAY . " AND is.Success=0  AND is.RedialCount < " . REDIAL_INCOMPLETE_RETRY_LIMIT);
	// END CRITERIA FOR CALLS - INCOMPLETE


	function getReminderAcceptance_t($fields) {
		return fetchReminderResults($fields, LOG_CALLTYPE_ACCEPTANCE_CALL, CRITERIA_ACCEPTANCE_CALL_T);
	}

	function getReminder24Hr_t($fields) {
		return fetchReminderResults($fields, LOG_CALLTYPE_24_HR_CALL, CRITERIA_24_HR_CALL_T);
	}

	function getReminder1Hr_t($fields) {
		return fetchReminderResults($fields, LOG_CALLTYPE_1_HR_CALL, CRITERIA_1_HR_CALL_T);
	}

	function getReminderEmergencyDispatchBackOut_t($fields) {
		return fetchReminderResults($fields, LOG_CALLTYPE_EMERGENCY_DISPATCH_BACK_OUT_CALL, CRITERIA_EMERGENCY_DISPATCH_BACK_OUT_CALL_T);
	}

	function getReminderEmergencyDispatchNotOnSite_t($fields) {
		return fetchReminderResults($fields, LOG_CALLTYPE_EMERGENCY_DISPATCH_NOT_ON_SITE_CALL, CRITERIA_EMERGENCY_DISPATCH_NOT_ON_SITE_CALL_T);
	}


	function getReminderNotMarkedComplete_t($fields) {
		return fetchReminderResults($fields, LOG_CALLTYPE_NOT_MARKED_COMPLETE_CALL, CRITERIA_NOT_MARKED_COMPLETE_CALL_T);
	}

	function getReminderIncomplete_t($fields) {
		return fetchReminderResults($fields, LOG_CALLTYPE_INCOMPLETE_CALL, CRITERIA_INCOMPLETE_CALL_T);
	}

/*	function getReminderAcceptance_t($fields, $id = '') {
		$uniqueIDCriteria = ($id != "" ? "AND TB_UNID = '" . caspioEscape($id) . "'" : "");

//		$reminderCallCriteria = "((" . CRITERIA_ACCEPTANCE_CALL . ") OR (" . CRITERIA_24_HR_CALL . ") OR (" . CRITERIA_1_HR_CALL . ") OR (" . CRITERIA_NOT_MARKED_COMPLETE_CALL . ") OR (" . CRITERIA_INCOMPLETE_CALL . "))";
		$reminderCallCriteria = CRITERIA_ACCEPTANCE_CALL_T;


		return caspioSelectAdv("Work_Orders_With_Timezone", $fields, "$uniqueIDCriteria $reminderCallCriteria", "StartDate", true, "`", "|", false);
	}

	function getReminder24Hr_t($fields, $id = '') {
		$uniqueIDCriteria = ($id != "" ? "AND TB_UNID = '" . caspioEscape($id) . "'" : "");

		$reminderCallCriteria = CRITERIA_24_HR_CALL_T;

		return caspioSelectAdv("Work_Orders_With_Timezone", $fields, "$uniqueIDCriteria $reminderCallCriteria", "StartDate", true, "`", "|", false);
	}

	function getReminder1Hr_t($fields, $id = '') {
		$uniqueIDCriteria = ($id != "" ? "AND TB_UNID = '" . caspioEscape($id) . "'" : "");

		$reminderCallCriteria = CRITERIA_1_HR_CALL_T;

		return caspioSelectAdv("Work_Orders_With_Timezone", $fields, "$uniqueIDCriteria $reminderCallCriteria", "StartDate", true, "`", "|", false);
	}

	function getReminderEmergencyDispatchBackOut_t($fields, $id = '') {
		$uniqueIDCriteria = ($id != "" ? "AND TB_UNID = '" . caspioEscape($id) . "'" : "");

		$reminderCallCriteria = CRITERIA_EMERGENCY_DISPATCH_BACK_OUT_CALL_T;

		return caspioSelectAdv("Work_Orders_With_Timezone", $fields, "$uniqueIDCriteria $reminderCallCriteria", "StartDate", true, "`", "|", false);
	}

	function getReminderEmergencyDispatchNotOnSite_t($fields, $id = '') {
		$uniqueIDCriteria = ($id != "" ? "AND TB_UNID = '" . caspioEscape($id) . "'" : "");

		$reminderCallCriteria = CRITERIA_EMERGENCY_DISPATCH_NOT_ON_SITE_CALL_T;

		return caspioSelectAdv("Work_Orders_With_Timezone", $fields, "$uniqueIDCriteria $reminderCallCriteria", "StartDate", true, "`", "|", false);
	}


	function getReminderNotMarkedComplete_t($fields, $id = '') {
		$uniqueIDCriteria = ($id != "" ? "AND TB_UNID = '" . caspioEscape($id) . "'" : "");

		$reminderCallCriteria = CRITERIA_NOT_MARKED_COMPLETE_CALL_T;

		return caspioSelectAdv("Work_Orders_With_Timezone", $fields, "$uniqueIDCriteria $reminderCallCriteria", "StartDate", true, "`", "|", false);
	}

	function getReminderIncomplete_t($fields, $id = '') {
		$uniqueIDCriteria = ($id != "" ? "AND TB_UNID = '" . caspioEscape($id) . "'" : "");

		$reminderCallCriteria = CRITERIA_INCOMPLETE_CALL_T;

		return caspioSelectAdv("Work_Orders_With_Timezone", $fields, "$uniqueIDCriteria $reminderCallCriteria", "StartDate", true, "`", "|", false);
	}*/


	$msg = "";

	set_time_limit(600);

//	caspioDelete("IVR_Log", "id = id", false);
//	die();

	echo "<div style=\"text-align: center\">Pending Reminder Calls</div>";
	function runCall($callType, $woList) {
		foreach ($woList as $info) {
			echo "Pending $callType: ";
			if ($info == "") {
				echo "<div style=\"margin: 0px 0px 0px 15px\">None</div><br/>";
				continue;
			}
			$uniqueID = $info['WIN_NUM'];
			$techID = $info['Tech_ID'];
			$primaryPhone = $info['PrimaryPhone'];
			$secondaryPhone = $info['SecondaryPhone'];
			$primPhoneType = $info['PrimPhoneType'];
			$secondPhoneType = $info['SecondPhoneType'];
			$emergencyPhone = $info['EmergencyPhone'];
			$companyID = $info['Company_ID'];

			echo "<div style=\"margin: 0px 0px 0px 15px\">WOUNID: $uniqueID</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Company: $companyID</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">TechID: $techID</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Primary Phone: $primaryPhone</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Secondary Phone: $secondaryPhone</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Primary Type Phone: $primPhoneType</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Secondary Type Phone: $secondPhoneType</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Emergency Phone: $emergencyPhone</div>";
//			print_r($parts);

			if ($callType != LOG_CALLTYPE_EMERGENCY_DISPATCH_BACK_OUT_CALL && $callType != LOG_CALLTYPE_EMERGENCY_DISPATCH_NOT_ON_SITE_CALL) {
				$phone = ($callType == LOG_CALLTYPE_1_HR_CALL && $primPhoneType != "Cell" && $secondaryPhone != "" && $secondPhoneType == "Cell" ? $secondaryPhone : $primaryPhone);
			}
			else {
				$phone = $emergencyPhone;
			}

			$phone = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);

			$phone = str_replace(array("-", ".", " ", "(", ")"), array("", "", "", "", ""), $phone);

//			echo "<br/><div style=\"margin: 0px 0px 0px 15px\">Using number: $phone</div><br/>";
			@ob_flush();
			@flush();

			// Testing purposes
//			if ($phone != "5122728328" || $phone != "8326878350")
//				$phone = "5122728328";
			if ($companyID != "CBD" && $companyID != "suss" && $companyID != "BW" && $companyID != "FS")
				continue;

			$phone = "+1" . $phone;
			echo "<div style=\"margin: 0px 0px 0px 15px\">Will Dial: $phone</div><br/>";
		}
		echo "<hr/>";
	}

	// find work orders needing reminder calls

	$fieldList = array(
		'WIN_NUM',
		'Tech_ID',
		'PrimaryPhone',
		'SecondaryPhone',
		'PrimPhoneType',
		'SecondPhoneType',
		'EmergencyPhone',
		'Company_ID'
	);

	$emergencyDisptachNotOnSiteCalls = getReminderEmergencyDispatchNotOnSite_t($fieldList);
	$emergencyDisptachBackOutCalls = getReminderEmergencyDispatchBackOut_t($fieldList);
	$oneHrCalls = getReminder1Hr_t($fieldList);
	$twentyFourHrCalls = getReminder24Hr_t($fieldList);
	$acceptanceCalls = getReminderAcceptance_t($fieldList);
	$notCompleteCalls = getReminderNotMarkedComplete_t($fieldList);
	$incompleteCalls = getReminderIncomplete_t($fieldList);

	runCall(LOG_CALLTYPE_EMERGENCY_DISPATCH_BACK_OUT_CALL, $emergencyDisptachBackOutCalls);
	runCall(LOG_CALLTYPE_EMERGENCY_DISPATCH_NOT_ON_SITE_CALL, $emergencyDisptachNotOnSiteCalls);
	runCall(LOG_CALLTYPE_1_HR_CALL, $oneHrCalls);
	runCall(LOG_CALLTYPE_24_HR_CALL, $twentyFourHrCalls);
	runCall(LOG_CALLTYPE_ACCEPTANCE_CALL, $acceptanceCalls);
	runCall(LOG_CALLTYPE_NOT_MARKED_COMPLETE_CALL, $notCompleteCalls);
	runCall(LOG_CALLTYPE_INCOMPLETE_CALL, $incompleteCalls);
?>