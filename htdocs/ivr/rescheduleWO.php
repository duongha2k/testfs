<?php
//	ini_set("display_errors", 1);
	require_once("ivrheader.php");
	require_once("../library/caspioAPI.php");
	require_once("../library/woContactLog_ivr.php");
	require_once("../library/timeStamps2.php");
	require_once("../library/smtpMail.php");
	require_once("library/ivrLib.php");
	writeIVRHeader();
	$uniqueID = $_SESSION["uniqueID"];
	
//	$_SESSION["TechID"] = 19806;
//	$uniqueID = 7103;
	if ($_SESSION["TechID"] == "") die();
	
	//$wo = caspioSelectAdv("Work_Orders_With_Timezone", "Company_ID, Company_Name, Project_Name, " . TIME_STARTTIME_HOURS_BEFORE_START . ", ProjectManagerEmail, ProjectManagerPhone", "TB_UNID = '$uniqueID' AND Tech_ID = '" . caspioEscape($_SESSION["TechID"]) . "'", "", true, "`", "|", false);
	$db = Zend_Registry::get('DB');
	$sql = "SELECT Company_ID, Company_Name, Project_Name, " . TIME_STARTTIME_HOURS_BEFORE_START . ", ProjectManagerEmail, ProjectManagerPhone FROM work_orders_with_timezone_view WHERE WIN_NUM = ? AND Tech_ID = ?";
	$wo = $db->fetchAll($sql, array($uniqueID, $_SESSION["TechID"]), Zend_Db::FETCH_NUM);

	$foundWO = false;
	if (count($wo) > 0)
		$foundWO = true;
	else
		$foundWO = false;
	
	$clientDispatch = "";
	if ($foundWO) {
		$info = $wo[0];
		$companyID = trim($info[0], "`");
		$clientName = trim($info[1], "`");
		$projectName = trim($info[2], "`");
		$timeDiff = trim($info[3], "`");
		$to_email = trim($info[4], "`");
		$clientDispatch = trim($info[5], "`");
		
		// set client notification session vars
		$_SESSION["ProjectName"] = $projectName;
		$_SESSION["ClientEmail"] = $to_email;
				
		rescheduleWO($uniqueID, $companyID, $clientName, $projectName);
		logIVRContact("IVR Reminder", $companyID, $uniqueID, $_SESSION["calledNumber"], $_SESSION["outboundType"], "Result: Tech Requested Reschedule");
		$_SESSION["IVRCallResult"] = LOG_CALLRESULT_ANSWERED_SUCCESS_RESCHEDULE;
	}
	else {
		notifyMaintainer("Unable to find WO UNID '$uniqueID' while trying to accept.");
	}
?>
<vxml version="2.1">
	<meta name="maintainer" content="tngo@fieldsolutions.com"/>

	<var name="sessionId" expr="'<?=session_id()?>'"/>
    
    <catch event="connection.disconnect.hangup">
		<exit namelist="sessionId"/>
	</catch>

<?php
if (!$foundWO):
?>
	<form>
	   	<field name="dummy">
            <grammar type="text/gsl">
                <![CDATA[[
                  [(do not match forty two)]
                ]
                ]]>
            </grammar>        	
        	<property name="timeout" value="1s"/>
        	<prompt>
	     		Unable to retrieve Work order unique i d: <?=$uniqueID?>. Field Solutions has been notified of this issue.
            </prompt>
            <filled>
                <prompt></prompt>
            </filled>
            <noinput>
            	<disconnect/>
            </noinput>
        </field>
    </form>
<?php
endif;
?>    

<form id="introBlock">
   	<field name="dummy">
        <grammar type="text/gsl">
			<![CDATA[[
			  [(do not match forty two)]
			]
			]]>
		</grammar>        	
       	<property name="timeout" value="1s"/>
    	<prompt>
			You have requested a reschedule of this work order. Please call the client dispatch number: <break strength="weak"/> <say-as interpret-as="telephone"><?=$clientDispatch?></say-as>. Thank you and goodbye
        </prompt>
		<filled>
        	<prompt></prompt>
        </filled>
        <noinput>
        	<disconnect/>
        </noinput>
	</field>
</form>
</vxml>
