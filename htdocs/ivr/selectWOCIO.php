<?php
//	ini_set("display_errors", 1);
	$page = "login";
	require_once("ivrheader.php");
	$_SESSION["loggedIn"] = "yes";
	require_once("../library/caspioAPI.php");
	require_once("library/ivrLib.php");
	$_SESSION["loggedIn"] = "no";
	writeIVRHeader();
	
?>
<vxml version="2.1">
	<meta name="maintainer" content="tngo@fieldsolutions.com"/>

    <catch event="connection.disconnect.hangup">
		<submit next="handleDisconnect.php?s=<?=session_id()?>" namelist=""/>
		<exit/>
	</catch>

    <form>
    	<block>
<?php
	if ($_SESSION["IVRLogID"] == ""):
		$_SESSION["IVRCallResult"] = LOG_CALLRESULT_CIO_INVALID_WOUNID;	
		$_SESSION["callBegin"] = time();		
		$_SESSION["IVRLogID"] = logIVRCall("", "", LOG_CALLTYPE_CIO_CALL, "Pending", 0);
		$_SESSION["loggedIn"] = "yes";
		$_SESSION["outboundType"] = LOG_CALLTYPE_CIO_CALL;
		$_SESSION["SelectWOTries"] = 0;
?>
        	This is Field Solutions work order check in and out system.
<?php
	endif;
?>
            <goto next="#getUniqueID"/>
        </block>
    </form>
   
    <form id="getUniqueID">
    	<field name="uniqueID" type="digits?minlength=3">
			<property name="inputmodes" value="dtmf"/>
	    	<prompt bargeintype="hotword">
            	Enter the win number followed by the pound sign.
	        </prompt>
            <filled namelist="uniqueID">
				<goto nextitem="TechID"/>
            </filled>
		</field>
    	<field name="TechID" type="digits?minlength=3">
			<property name="inputmodes" value="dtmf"/>
	    	<prompt bargeintype="hotword">
            	Enter your Technician ID followed by the pound sign.
	        </prompt>
            <filled namelist="uniqueID TechID">
				<submit next="checkInOut.php?s=<?=session_id()?>" namelist="uniqueID TechID" method="post" />
            </filled>
		</field>        
    </form>
</vxml>
