<?php
//	ini_set("display_errors", 1);
	require_once("ivrheader.php");
	require_once("../library/caspioAPI.php");
	require_once("library/ivrLib.php");
	require_once("../library/woContactLog_ivr.php");

	writeIVRHeader();
	$uniqueID = $_SESSION["uniqueID"];
	$cpa_result = $_SESSION["CPAResult"];
	
//	$_SESSION["TechID"] = 19806;
//	$uniqueID = 7103;
	
	if ($_SESSION["TechID"] == "") die();
	
	// Group Call Code
	if ($_SESSION["GroupedCall"]) {
/*		if (!isset($_SESSION["GroupedCallWOList"])) { 
			// $wo = caspioSelectAdv("Work_Orders_With_Timezone", "ISNULL(CONVERT(VARCHAR, StartDate, 101), ''), ISNULL(StartTime, ''), ISNULL(City, ''), ISNULL(State, ''), ISNULL(Zipcode, ''), " . TIME_CURRENT_TIME_LOCAL . ", ISNULL(Headline, ''), ISNULL(Tech_Bid_Amount, ''), ISNULL(Amount_Per, 'Site'), Company_ID, Company_Name, Project_Name, " . TIME_STARTTIME_HOURS_BEFORE_START . ", ProjectManagerEmail, EmergencyPhone, ISNULL(EndTime, ''), TB_UNID", "TB_UNID IN ($uniqueID) AND Tech_ID = '" . caspioEscape($_SESSION["TechID"]) . "'", "", true, "`", "|", false);
			$db = Zend_Registry::get('DB');
			$sql = "SELECT IFNULL(DATE_FORMAT(work_orders_with_timezone_view.StartDate, '%m/%d/%Y'), ''), IFNULL(work_orders_with_timezone_view.StartTime, ''), IFNULL(City, ''), IFNULL(State, ''), IFNULL(Zipcode, ''), " . TIME_CURRENT_TIME_LOCAL . ", IFNULL(work_orders_with_timezone_view.Headline, ''), IFNULL(Tech_Bid_Amount, ''), IFNULL(work_orders_with_timezone_view.Amount_Per, 'Site'), Company_ID, Company_Name, work_orders_with_timezone_view.Project_Name, " . TIME_STARTTIME_HOURS_BEFORE_START . ", ProjectManagerEmail, work_orders_with_timezone_view.EmergencyPhone, IFNULL(work_orders_with_timezone_view.EndTime, ''), WIN_NUM, work_orders_with_timezone_view.Project_ID, ReminderCustomHr, ReminderCustomHr_2, ReminderCustomHr_3 FROM work_orders_with_timezone_view JOIN projects AS p ON work_orders_with_timezone_view.Project_ID = p.Project_ID WHERE work_orders_with_timezone_view.WIN_NUM = ? AND Tech_ID = ?";
			$wo = $db->fetchAll($sql, array($uniqueID, $_SESSION["TechID"]), Zend_Db::FETCH_NUM);

			$_SESSION["GroupedCallWOList"] = $wo;
			$_SESSION["GroupedCallCurrent"] = 0; // used for looping of acceptance, 24 hr, 1 hr.
			$_SESSION["GroupedCallTotal"] = count($wo);
		}
		$wo = $_SESSION["GroupedCallWOList"];
		$currentWO = $_SESSION["GroupedCallCurrent"];
		$totalWO = $_SESSION["GroupedCallTotal"];
		$wo[0] = $wo[$currentWO];
		$_SESSION["GroupedCallCurrent"] += 1;*/
	}
	else {
//		$wo = caspioSelectAdv("Work_Orders_With_Timezone", "ISNULL(CONVERT(VARCHAR, StartDate, 101), ''), ISNULL(StartTime, ''), ISNULL(City, ''), ISNULL(State, ''), ISNULL(Zipcode, ''), " . TIME_CURRENT_TIME_LOCAL . ", ISNULL(Headline, ''), ISNULL(Tech_Bid_Amount, ''), ISNULL(Amount_Per, 'Site'), Company_ID, Company_Name, Project_Name, " . TIME_STARTTIME_HOURS_BEFORE_START . ", ProjectManagerEmail, EmergencyPhone, ISNULL(EndTime, '')", "TB_UNID = '$uniqueID' AND Tech_ID = '" . caspioEscape($_SESSION["TechID"]) . "'", "", true, "`", "|", false);
		$db = Zend_Registry::get('DB');
		$sql = "SELECT IFNULL(DATE_FORMAT(work_orders_with_timezone_view.StartDate, '%m/%d/%Y'), ''), IFNULL(work_orders_with_timezone_view.StartTime, ''), IFNULL(City, ''), IFNULL(State, ''), IFNULL(Zipcode, ''), " . TIME_CURRENT_TIME_LOCAL . ", IFNULL(work_orders_with_timezone_view.Headline, ''), IFNULL(Tech_Bid_Amount, ''), IFNULL(work_orders_with_timezone_view.Amount_Per, 'Site'), Company_ID, Company_Name, work_orders_with_timezone_view.Project_Name, " . TIME_STARTTIME_HOURS_BEFORE_START . ", ProjectManagerEmail, work_orders_with_timezone_view.EmergencyPhone, IFNULL(work_orders_with_timezone_view.EndTime, ''), WIN_NUM, work_orders_with_timezone_view.Project_ID, ReminderCustomHr, ReminderCustomHr_2, ReminderCustomHr_3, work_orders_with_timezone_view.Type_ID FROM work_orders_with_timezone_view JOIN projects AS p ON work_orders_with_timezone_view.Project_ID = p.Project_ID WHERE work_orders_with_timezone_view.WIN_NUM = ? AND Tech_ID = ?";
		$wo = $db->fetchAll($sql, array($uniqueID, $_SESSION["TechID"]), Zend_Db::FETCH_NUM);
	}
		
	$foundWO = false;
	if (count($wo) > 0)
		$foundWO = true;
	else
		$foundWO = false;
	
	if ($foundWO) {
		$info = $wo[0];
		$startDate = trim($info[0], "`");
		$startTime = trim($info[1], "`");
		$location = htmlentities(trim($info[2], "`") . ", " . trim($info[3], "`") . " " . trim($info[4], "`"));
		$localTime = trim($info[5], "`");
		$headline = htmlentities(trim($info[6], "`"));
		$bidAmount = "$" . trim($info[7], "`") . " per " . trim($info[8], "`");

		$companyID = trim($info[9], "`");
		$clientName = trim($info[10], "`");
		$projectName = trim($info[11], "`");
		$timeDiff = trim($info[12], "`");
		$to_email = trim($info[13], "`");
		$emergencyDispatch = trim($info[14], "`");
		$endTime = trim($info[15], "`");
		
		$endTime = $endTime == "" ? $startTime : $endTime;
		$callType = $info[21] == 1 ? "an install" : "a service call";

		// Group Call Code
		if ($_SESSION["GroupedCall"]) {
			$uniqueID = trim($info[16], "`");
			$_SESSION["GroupedCallCurrentUniqueID"] = $uniqueID;
		}

		$dayofweek = date("l", strtotime($startDate));
		$scheduledTime = "<break strength=\"x-weak\"/> $dayofweek, $startDate";
	}
	else {
		notifyMaintainer("Unable to find WO UNID '$uniqueID'.");
	}
	
	if ($cpa_result != "human") {
		// Log voice mail
		$logID = $_SESSION["GroupedCall"] ? $_SESSION["GroupedCallUniqueIDToLogID"][$uniqueID] : $_SESSION["IVRCallID"];
		setIVRCallResult($logID, LOG_CALLRESULT_ANSWERED_MACHINE);
		logIVRContact("IVR Reminder", $companyID, $uniqueID, $_SESSION["calledNumber"], $_SESSION["outboundType"], "Result: " . LOG_CALLRESULT_ANSWERED_MACHINE);
		$_SESSION["CallEmergencyDispatch"] = array($uniqueID, $_SESSION["TechID"], $emergencyPhone, LOG_CALLTYPE_EMERGENCY_DISPATCH_BACK_OUT_CALL);
	}
	else {
		$_SESSION["IVRCallResult"] = LOG_CALLRESULT_ANSWERED_NO_RESPONSE;
		$logID = $_SESSION["GroupedCall"] ? $_SESSION["GroupedCallUniqueIDToLogID"][$uniqueID] : $_SESSION["IVRCallID"];
		setIVRCallResult($logID, LOG_CALLRESULT_ANSWERED_NO_RESPONSE);
		logIVRContact("IVR Reminder", $companyID, $uniqueID, $_SESSION["calledNumber"], $_SESSION["outboundType"], "Result: " . LOG_CALLRESULT_ANSWERED_NO_RESPONSE);
	}
?>
<vxml version="2.1">
	<meta name="maintainer" content="tngo@fieldsolutions.com"/>
        
    <var name="uniqueID" expr="'<?=$uniqueID?>'"/>
    <var name="sessionId" expr="'<?=session_id()?>'"/>

    <catch event="connection.disconnect.hangup">
		<exit namelist="sessionId"/>
	</catch>
    
<?php
if (!$foundWO):
?>
	<form>
    	<block>
     		Unable to retrieve win number: <?=$uniqueID?>. Field Solutions has been notified of this issue.
            <disconnect />
        </block>
    </form>
<?php
endif;
?>    

<form id="introBlock">
   	<field name="dummy">
        <grammar type="text/gsl">
			<![CDATA[[
			  [(do not match forty two)]
			]
			]]>
		</grammar>        	
       	<property name="timeout" value="100ms"/>

		<prompt>
<?php
if ($cpa_result == "human"):
?>        
<?php
else:
	if ($_SESSION["GroupedCall"]):
?>
<?
	else:
?>
			Hello, This is a reminder that you have a technician scheduled to come on-site at <?=$startTime?> on <?=$scheduledTime?> to perform <?=$callType?>. We will call back in 10 minutes to confirm that the technician will have access to the site. You can also call the Emergency Contact at <prosody rate="slow"><say-as interpret-as="telephone"><?=$emergencyDispatch?></say-as></prosody> to confirm technician site access. Thank you. Goodbye.

<?php
	endif;
endif;
?>
		</prompt>
        <filled>
        	<prompt></prompt>
        </filled>
        <nomatch count="30">
            <disconnect/>
        </nomatch>
        <nomatch>
        	<reprompt/>
        </nomatch>
		<noinput>
<?php
if ($cpa_result == "human"):
?>        
			<goto next="#choiceForm"/>
<?php
else:
?>
			<disconnect/>
<?php
endif;
?>
		</noinput>
	</field>
</form>

<?php
if ($cpa_result == "human"):
?>
	<form id="choiceForm">
    	<field name="choice">
	        <grammar type="text/gsl">
				<![CDATA[[
				  [(dtmf-1)] {<choice "oneHr">}
				  [(dtmf-2)] {<choice "backOut">}
				  [(dtmf-0)] {<choice "repeat">}
				]
				]]>
			</grammar>    
            <property name="inputmodes" value="dtmf"/>
	    	<prompt bargintype="hotword">
<?php
	if ($_SESSION["GroupedCall"]) {
/*		if ($currentWO == 0) {
			$woIntro = "Hello, Field Solutions is calling to remind you that you have <break strength=\"x-weak\"/>$totalWO work orders scheduled to begin soon. ";
			$woIntro .= "Work order number " . ($currentWO + 1) . " of $totalWO:";
		}*/
	}
	else
		$woIntro = "Hello, Field Solutions is calling to remind you that";
?>
				Hello, This is a reminder that you have a technician scheduled to come on-site at <?=$startTime?> on <?=$scheduledTime?> to perform <?=$callType?>. 
                            
				Press 1 to confirm that the technician will have access to the site at this time.
                Press 2 if no one will be available to provide access to the technician at this time.
                Press 0 to hear this message again.
			</prompt>
            <nomatch count="10">
                <disconnect/>
            </nomatch>
            <noinput count="10">
                <disconnect/>
            </noinput>
            <filled namelist="choice">
            	<if cond="choice == 'oneHr'">
					<submit next="confirmSiteContactWO.php?s=<?=session_id()?>" namelist="choice" method="post"/>
				<elseif cond="choice == 'backOut'" />
                	<goto next="#noConfirmForm"/>
				<else/>
                	<clear namelist="choice"/>
                    <goto next="#choiceForm"/>
                </if>
            </filled>
        </field>
    </form>
    
    <form id="noConfirmForm">
        <field name="choice">
	        <grammar type="text/gsl">
				<![CDATA[[
				  [(dtmf-1)] {<choice "oneHr">}
				  [(dtmf-2)] {<choice "back out">}
				  [(dtmf-0)] {<choice "repeat">}
				]
				]]>
			</grammar>        	
            <property name="inputmodes" value="dtmf"/>
        	<prompt bargeintype="hotword">Press 1 to confirm that the technician will have access to the site at this time. Press 2 if no one will be available to provide access to the technician at this time. Press pound to hear this message again.</prompt>
            <nomatch count="10">
                <disconnect/>
            </nomatch>
            <noinput count="10">
                <disconnect/>
            </noinput>
            <filled namelist="choice">
				<if cond="choice == 'back out'">
					<submit next="backOutSiteContactWO.php?s=<?=session_id()?>" namelist="" method="post"/>
				<elseif cond="choice == 'oneHr'"/>
					<submit next="confirmSiteContactWO.php?s=<?=session_id()?>" namelist="choice" method="post"/>
				<else/>
                	<clear namelist="choice"/>
                    <goto next="#noConfirmForm"/>
				</if>
			</filled>
        </field>        
    </form>    
<?php
endif;
?>
</vxml>
