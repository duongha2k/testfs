<?php
	require_once(dirname(__FILE__) . '/../../../includes/modules/common.init.php');

	$myPath = realpath(dirname(__FILE__) . "/../../");
	require_once($myPath . "/library/smtpMail.php");
	require_once($myPath . "/library/timeStamps2.php");
	require_once($myPath . "/ivr/library/voxeoAPI.php"); // CHANGE DIRECTORY
	require_once($myPath . "/library/woContactLog_ivr.php");
	define("TECH_SESSION_INFO", "TechID, Date_TermsAccepted, FS_Cert_Test_Pass, Latitude, Longitude, Username, Firstname, Lastname, Password");

	$now = strtotime("now");

	date_default_timezone_set("America/New_York");
	$easternDSActive = date("I", $now);

	date_default_timezone_set("America/Chicago");
	$centralDSActive = date("I", $now);

	date_default_timezone_set("America/Denver");
	$mountainDSActive = date("I", $now);

	date_default_timezone_set("America/Los_Angeles");
	$pacificDSActive = date("I", $now);

	date_default_timezone_set("America/Chicago");

	$tomorrow = date("Y-m-d", strtotime("+1 day", $now));
	$dayAfterTomorrow = date("Y-m-d", strtotime("+2 day", $now));
	$nextWeekday = (date("w", $now) == 5 ? date("Y-m-d", strtotime("+3 day", $now)) : $tomorrow);
	$dayAfterNextWeekday = (date("w", $now) == 5 ? date("Y-m-d", strtotime("+4 day", $now)) : $dayAfterTomorrow);
	$today = date("Y-m-d", $now);
	$today10AM = "$today 10:00";
	$today1005AM = "$today 10:59";

	// local time zone offset adjusted if DS is active
	define("TIME_LOCALZONE_OFFSET", "(StandardOffset + (CASE WHEN DST = '1' THEN (CASE WHEN StandardOffset = -5 THEN $easternDSActive WHEN StandardOffset = -6 THEN $centralDSActive WHEN StandardOffset = -7 THEN $mountainDSActive ELSE $pacificDSActive END) ELSE 0 END))");

	// current local time
	define("TIME_CURRENT_TIME_LOCAL", "UTC_TIMESTAMP() + INTERVAL " . TIME_LOCALZONE_OFFSET . " HOUR");
	// is current date a weekday
	define("TIME_CURRENT_DATE_IS_WEEKDAY", "(DAYOFWEEK(" . TIME_CURRENT_TIME_LOCAL . ") != 1 AND DAYOFWEEK(" . TIME_CURRENT_TIME_LOCAL . ") != 7)");
	// local time greater equal to 10 AM
	define("TIME_CURRENT_TIME_LATER_THAN_10AM_LOCAL", "(" . TIME_CURRENT_TIME_LOCAL . " >= '$today10AM' AND " . TIME_CURRENT_TIME_LOCAL . " <= '$today1005AM')");
	// casts text starttime into a date / time
	define("TIME_STARTTIME_CAST", "CAST(CONCAT(work_orders_with_timezone_view.StartDate, ' ', STR_TO_DATE(work_orders_with_timezone_view.StartTime, '%l:%i %p')) AS DATETIME)");
	// finds start times between midnight and 8am
	define("TIME_STARTTIME_BETWEEN_MIDNIGHT_8AM", "(work_orders_with_timezone_view.StartTime LIKE '[1234567]:__ AM' OR work_orders_with_timezone_view.StartTime LIKE '12:__ AM' OR work_orders_with_timezone_view.StartTime = '8:00 AM')");
	define("TIME_STARTTIME_HOURS_BEFORE_START", "((UNIX_TIMESTAMP( " . TIME_STARTTIME_CAST . ") - UNIX_TIMESTAMP(" . TIME_CURRENT_TIME_LOCAL . ")) / 3600)");
	define("TIME_STARTTIME_MINUTES_BEFORE_START", "((UNIX_TIMESTAMP( " . TIME_STARTTIME_CAST . ") - UNIX_TIMESTAMP(" . TIME_CURRENT_TIME_LOCAL . ")) / 60)");
	define("TIME_BACKOUT_THERSHOLD_MINUTES", "1440");

	// WO STATE
	define("WO_ASSIGNED", "Tech_ID IS NOT NULL");
	define("WO_NOT_INVOICE", "Invoiced = '0'");
	define("WO_NOT_DEACTIVATED", "Deactivated = '0'");
	define("WO_NOT_REVIEWED", "WorkOrderReviewed = '0'");
	define("WO_REVIEWED", "WorkOrderReviewed = '1'");
	define("WO_NOT_MARK_COMPLETED", "TechMarkedComplete = '0'");
	define("WO_MARK_COMPLETED", "TechMarkedComplete = '1'");

/*	define("WO_REMINDER_ACCEPTANCE", "ReminderAcceptance = '1'");
	define("WO_REMINDER_24_HR", "Reminder24Hr = '1'");
	define("WO_REMINDER_1_HR", "Reminder1Hr = '1'");
//	define("WO_REMINDER_CHECK_IN_OUT", "ReminderCheckInOut = '1'");
	define("WO_REMINDER_NOT_MARKED_COMPLETE", "ReminderNotMarkComplete = '1'");
	define("WO_REMINDER_INCOMPLETE", "ReminderIncomplete = '1'");*/

	// WO REMINDER SETTINGS ---- ***************** SET TO ZERO FOR TESTING
	define("WO_REMINDER_ALL", "ReminderAll = '1'");
	define("WO_REMINDER_ACCEPTANCE", "ReminderAcceptance = '1'");
	define("WO_REMINDER_24_HR", "Reminder24Hr = '1'");
	define("WO_REMINDER_1_HR", "Reminder1Hr = '1'");
	define("WO_REMINDER_SITE_CONTACT", "SiteContactReminderCall = '1'");
	define("WO_REMINDER_CUSTOM_HR_1", "ReminderCustomHrChecked = '1'");
	define("WO_REMINDER_CUSTOM_HR_2", "ReminderCustomHrChecked_2 = '1'");
	define("WO_REMINDER_CUSTOM_HR_3", "ReminderCustomHrChecked_3 = '1'");
	define("WO_REMINDER_NOT_MARKED_COMPLETE", "ReminderNotMarkComplete = '1'");
	define("WO_REMINDER_INCOMPLETE", "ReminderIncomplete = '1'");

	define("WO_CHECK_IN_CALL", "CheckInCall = '1'");
	define("WO_CHECK_OUT_CALL", "CheckOutCall = '1'");


	// LOG
	define("LOG_CALLTYPE_ACCEPTANCE_CALL", "Acceptance Reminder");
	define("LOG_CALLTYPE_24_HR_CALL", "24-HR Reminder");
	define("LOG_CALLTYPE_1_HR_CALL", "1-HR Reminder");
	define("LOG_CALLTYPE_CUSTOM_HR_CALL_1", "Custom HR Reminder 1");
	define("LOG_CALLTYPE_CUSTOM_HR_CALL_2", "Custom HR Reminder 2");
	define("LOG_CALLTYPE_CUSTOM_HR_CALL_3", "Custom HR Reminder 3");
	define("LOG_CALLTYPE_CIO_CALL", "Check In / Out");
	define("LOG_CALLTYPE_EMERGENCY_DISPATCH_BACK_OUT_CALL", "Emergency Dispatch Reminder - Back out");
	define("LOG_CALLTYPE_EMERGENCY_DISPATCH_NOT_ON_SITE_CALL", "Emergency Dispatch Reminder - Check In but not on site");
	define("LOG_CALLTYPE_NOT_MARKED_COMPLETE_CALL", "Not Marked Complete Reminder");
	define("LOG_CALLTYPE_INCOMPLETE_CALL", "Incomplete Reminder");
	define("LOG_CALLTYPE_SITE_CONTACT_CALL", "Site Contact Reminder");
	define("LOG_CALLTYPE_EMERGENCY_DISPATCH_SITE_CONTACT_BACK_OUT_CALL", "Emergency Dispatch Reminder Site Contact - Back out");
	

	define("LOG_CALLRESULT_NO_ANSWER", "No Answer");
	define("LOG_CALLRESULT_ANSWERED_MACHINE", "Answered by Machine");
	define("LOG_CALLRESULT_ANSWERED_NO_RESPONSE", "Answered by Person - Disconnected Without Response");
	define("LOG_CALLRESULT_ANSWERED_SUCCESS", "Success");
	define("LOG_CALLRESULT_ANSWERED_SUCCESS_BACK_OUT", "Success - Back out");
	define("LOG_CALLRESULT_ANSWERED_SUCCESS_RESCHEDULE", "Success - Reschedule");
	define("LOG_CALLRESULT_CIO_SUCCESS", "Success");
	define("LOG_CALLRESULT_CIO_SUCCESS_CHECK_IN", "Success - Check in");
	define("LOG_CALLRESULT_CIO_SUCCESS_CHECK_OUT", "Success - Check out");
	define("LOG_CALLRESULT_CIO_SUCCESS_NOT_ON_SITE", "Success - Not on Site");
	define("LOG_CALLRESULT_CIO_INVALID_WOUNID", "Invalid WOUNID");
	define("LOG_CALLRESULT_EMERGENCY_DISPATCH_CANCEL", "Success - Cancelled");

	// WO DETAILS
	define("WO_STARTS_TODAY", "StartDate = '$today'");
	define("WO_STARTS_24_HR_ADJUST_WEEKEND_EARLY_MORNING","(StartDate > '$today' AND ((StartDate <= '$nextWeekday' AND NOT " . TIME_STARTTIME_BETWEEN_MIDNIGHT_8AM . ") OR (StartDate <= '$dayAfterNextWeekday' AND " . TIME_STARTTIME_BETWEEN_MIDNIGHT_8AM . ")))");
	define("WO_ASSIGNED_MORE_THAN_24_HRS", "((UNIX_TIMESTAMP(" . TIME_CURRENT_TIME_LOCAL . ") - UNIX_TIMESTAMP(Date_Assigned)) >= 1440*60)");
	define("WO_STARTS_1_HR_LOCAL_TIME", "((UNIX_TIMESTAMP(" . TIME_STARTTIME_CAST . ") - UNIX_TIMESTAMP(" . TIME_CURRENT_TIME_LOCAL . ")) <= 60*60)");
		
	define("WO_STARTS_DATE_CUSTOM_ADJUST_1", "DATE_SUB(work_orders_with_timezone_view.StartDate, INTERVAL ((p.ReminderCustomHr DIV 24) - 1) DAY)");
	define("WO_STARTS_DATE_CUSTOM_ADJUST_2", "DATE_SUB(work_orders_with_timezone_view.StartDate, INTERVAL ((p.ReminderCustomHr_2 DIV 24) - 1) DAY)");
	define("WO_STARTS_DATE_CUSTOM_ADJUST_3", "DATE_SUB(work_orders_with_timezone_view.StartDate, INTERVAL ((p.ReminderCustomHr_3 DIV 24) - 1) DAY)");

	define("WO_STARTS_CUSTOM_1_24_HR_ADJUST_WEEKEND_EARLY_MORNING","(" . WO_STARTS_DATE_CUSTOM_ADJUST_1 . " > '$today' AND ((" . WO_STARTS_DATE_CUSTOM_ADJUST_1 . " <= '$nextWeekday' AND NOT " . TIME_STARTTIME_BETWEEN_MIDNIGHT_8AM . ") OR (" . WO_STARTS_DATE_CUSTOM_ADJUST_1 . " <= '$dayAfterNextWeekday' AND " . TIME_STARTTIME_BETWEEN_MIDNIGHT_8AM . ")))");
	define("WO_STARTS_CUSTOM_2_24_HR_ADJUST_WEEKEND_EARLY_MORNING","(" . WO_STARTS_DATE_CUSTOM_ADJUST_2 . " > '$today' AND ((" . WO_STARTS_DATE_CUSTOM_ADJUST_2 . " <= '$nextWeekday' AND NOT " . TIME_STARTTIME_BETWEEN_MIDNIGHT_8AM . ") OR (" . WO_STARTS_DATE_CUSTOM_ADJUST_2 . " <= '$dayAfterNextWeekday' AND " . TIME_STARTTIME_BETWEEN_MIDNIGHT_8AM . ")))");
	define("WO_STARTS_CUSTOM_3_24_HR_ADJUST_WEEKEND_EARLY_MORNING","(" . WO_STARTS_DATE_CUSTOM_ADJUST_3 . " > '$today' AND ((" . WO_STARTS_DATE_CUSTOM_ADJUST_3 . " <= '$nextWeekday' AND NOT " . TIME_STARTTIME_BETWEEN_MIDNIGHT_8AM . ") OR (" . WO_STARTS_DATE_CUSTOM_ADJUST_3 . " <= '$dayAfterNextWeekday' AND " . TIME_STARTTIME_BETWEEN_MIDNIGHT_8AM . ")))");
	
	define("WO_STARTS_CUSTOM_HR_LOCAL_TIME_1", "((UNIX_TIMESTAMP(" . TIME_STARTTIME_CAST . ") - UNIX_TIMESTAMP(" . TIME_CURRENT_TIME_LOCAL . ")) <= p.ReminderCustomHr*3600)");
	define("WO_STARTS_CUSTOM_HR_LOCAL_TIME_2", "((UNIX_TIMESTAMP(" . TIME_STARTTIME_CAST . ") - UNIX_TIMESTAMP(" . TIME_CURRENT_TIME_LOCAL . ")) <= p.ReminderCustomHr_2*3600)");
	define("WO_STARTS_CUSTOM_HR_LOCAL_TIME_3", "((UNIX_TIMESTAMP(" . TIME_STARTTIME_CAST . ") - UNIX_TIMESTAMP(" . TIME_CURRENT_TIME_LOCAL . ")) <= p.ReminderCustomHr_3*3600)");

	define("WO_STARTED", "((UNIX_TIMESTAMP(" . TIME_STARTTIME_CAST . ") - UNIX_TIMESTAMP(" . TIME_CURRENT_TIME_LOCAL . ")) <= 0)");
	
	$startDateField = "CAST(CONCAT(`v`.StartDate, ' ', STR_TO_DATE(`v`.StartTime, '%l:%i %p')) AS DATETIME)";
	$endDateField = "CAST(CONCAT(`v`.EndDate, ' ', STR_TO_DATE(`v`.EndTime, '%l:%i %p')) AS DATETIME)";
	$dateField = "(CASE WHEN `v`.EndDate IS NULL THEN $startDateField ELSE $endDateField END)";
	define("WO_VISITS_LAST", "(SELECT MAX($dateField) FROM work_order_visits AS `v` WHERE `v`.WIN_NUM = work_orders_with_timezone_view.WIN_NUM)");
								
	define("WO_ENDED_BEFORE_TODAY", "((CASE WHEN " . WO_VISITS_LAST . " IS NULL THEN 0 WHEN " . WO_VISITS_LAST . " < '$today' THEN 1 ELSE 0 END) = 1)");
	define("WO_INCOMPLETE_BEFORE_TODAY", "((CASE WHEN (DateIncomplete IS NULL) THEN 0 WHEN DateIncomplete < '$today' THEN 1 ELSE 0 END) = 1)");


	// REDIAL RULES - ACCEPTANCE REMINDER - 3 times at 30 min interval
	define("REDIAL_ACCEPTANCE_RETRY_LIMIT", 3);
	define("REDIAL_ACCEPTANCE_RETRY_INTERVAL", 30);

	define("REDIAL_CRITERIA_ACCEPTANCE", '((is.LastCalledTime IS NULL) OR (is.Success=0 AND is.RedialCount < '. REDIAL_ACCEPTANCE_RETRY_LIMIT . ' AND is.LastCalledTime + INTERVAL ' . REDIAL_ACCEPTANCE_RETRY_INTERVAL . ' MINUTE < NOW()))');
	// END REDIAL RULES - ACCEPTANCE REMINDER

	// REDIAL RULES - 24 HR REMINDER - 3 times at 30 min interval
	define("REDIAL_24_HR_RETRY_LIMIT", 3);
	define("REDIAL_24_HR_RETRY_INTERVAL", 30);

	define("REDIAL_CRITERIA_24_HR", '((is.LastCalledTime IS NULL) OR (is.Success=0 AND is.RedialCount < '. REDIAL_24_HR_RETRY_LIMIT . ' AND is.LastCalledTime + INTERVAL ' . REDIAL_24_HR_RETRY_INTERVAL . ' MINUTE < NOW()))');
	// END REDIAL RULES - 24 HR REMINDER

	// REDIAL RULES - SITE CONTACT REMINDER - 3 times at 10 min interval
	define("REDIAL_SITE_CONTACT_RETRY_LIMIT", 2);
	define("REDIAL_SITE_CONTACT_RETRY_INTERVAL", 10);

	define("WO_STARTS_SITE_CONTACT_LOCAL_TIME", "((UNIX_TIMESTAMP(" . TIME_STARTTIME_CAST . ") - UNIX_TIMESTAMP(" . TIME_CURRENT_TIME_LOCAL . ")) <= (p.SiteContactReminderCallHours * 3600) + (" . REDIAL_SITE_CONTACT_RETRY_INTERVAL . " * 60))");
	define("REDIAL_CRITERIA_SITE_CONTACT", '((is.LastCalledTime IS NULL) OR (is.Success=0 AND is.RedialCount < '. REDIAL_SITE_CONTACT_RETRY_LIMIT . ' AND is.LastCalledTime + INTERVAL ' . REDIAL_SITE_CONTACT_RETRY_INTERVAL . ' MINUTE < NOW()))');
	// END REDIAL RULES - SITE CONTACT REMINDER

	// REDIAL RULES - 1 HR REMINDER - 3 times at 10 min interval
	//define("REDIAL_1_HR_CALL", "CallType = '" . LOG_CALLTYPE_1_HR_CALL. "'");
	define("REDIAL_1_HR_RETRY_LIMIT", 3);
	define("REDIAL_1_HR_RETRY_INTERVAL", 10);

	define("REDIAL_CRITERIA_1_HR", '((is.LastCalledTime IS NULL) OR (is.Success=0 AND is.RedialCount < '. REDIAL_1_HR_RETRY_LIMIT . ' AND is.LastCalledTime + INTERVAL ' . REDIAL_1_HR_RETRY_INTERVAL . ' MINUTE < NOW()))');
	// END REDIAL RULES - 1 HR REMINDER

	// REDIAL RULES - EMERGENCY DISPATCH SITE CONTACT BACK OUT REMINDER - 3 times at 10 min interval
	define("REDIAL_EMERGENCY_DISPATCH_SITE_CONTACT_BACK_OUT_RETRY_LIMIT", 3);
	define("REDIAL_EMERGENCY_DISPATCH_SITE_CONTACT_BACK_OUT_RETRY_INTERVAL", 10);

	define("REDIAL_CRITERIA_EMERGENCY_DISPATCH_SITE_CONTACT_BACK_OUT", "is.LastCalledTime IS NOT NULL AND is.Success = 0 AND is.RedialCount < " . REDIAL_EMERGENCY_DISPATCH_SITE_CONTACT_BACK_OUT_RETRY_LIMIT . " AND is.LastCalledTime + INTERVAL " . REDIAL_EMERGENCY_DISPATCH_SITE_CONTACT_BACK_OUT_RETRY_INTERVAL . " MINUTE < NOW()"); //Try do deal only once. No entry in ivr_status (previously no calls where made).
	// END REDIAL RULES - EMERGENCY DISPATCH SITE CONTACT BACK OUT REMINDER

	// REDIAL RULES - EMERGENCY DISPATCH BACK OUT REMINDER - 3 times at 10 min interval
	define("REDIAL_EMERGENCY_DISPATCH_BACK_OUT_RETRY_LIMIT", 3);
	define("REDIAL_EMERGENCY_DISPATCH_BACK_OUT_RETRY_INTERVAL", 10);

	define("REDIAL_CRITERIA_EMERGENCY_DISPATCH_BACK_OUT", "is.LastCalledTime IS NOT NULL AND is.Success = 0 AND is.RedialCount < " . REDIAL_EMERGENCY_DISPATCH_BACK_OUT_RETRY_LIMIT . " AND is.LastCalledTime + INTERVAL " . REDIAL_EMERGENCY_DISPATCH_BACK_OUT_RETRY_INTERVAL . " MINUTE < NOW()"); //Try do deal only once. No entry in ivr_status (previously no calls where made).
	// END REDIAL RULES - EMERGENCY DISPATCH BACK OUT REMINDER

	// REDIAL RULES - EMERGENCY DISPATCH NOT ON SITE REMINDER - 3 times at 10 min interval
	define("REDIAL_EMERGENCY_DISPATCH_NOT_ON_SITE_RETRY_LIMIT", 3);
	define("REDIAL_EMERGENCY_DISPATCH_NOT_ON_SITE_RETRY_INTERVAL", 10);

	define("REDIAL_CRITERIA_EMERGENCY_DISPATCH_NOT_ON_SITE", "is.LastCalledTime IS NOT NULL AND is.Success = 0 AND is.RedialCount < " . REDIAL_EMERGENCY_DISPATCH_NOT_ON_SITE_RETRY_LIMIT . " AND is.LastCalledTime + INTERVAL " . REDIAL_EMERGENCY_DISPATCH_NOT_ON_SITE_RETRY_INTERVAL . " MINUTE < NOW()");  //Try do deal only once. No entry in ivr_status (previously no calls where made).
	// END REDIAL RULES - EMERGENCY DISPATCH NOT ON SITE  REMINDER

	// REDIAL RULES - NOT MARK COMPLETE REMINDER - 3 times at daily interval
	define("REDIAL_NOT_MARKED_COMPLETE_RETRY_LIMIT", 3);

	define("REDIAL_CRITERIA_NOT_MARKED_COMPLETE", '((is.LastCalledTime IS NULL) OR (is.Success=0 AND is.RedialCount < '. REDIAL_NOT_MARKED_COMPLETE_RETRY_LIMIT . ' AND DATEDIFF(NOW(), is.LastCalledTime) > 1))');
	// END REDIAL RULES - NOT MARK COMPLETE REMINDER

	// REDIAL RULES - INCOMPLETE REMINDER - 3 times at daily interval
	define("REDIAL_INCOMPLETE_RETRY_LIMIT", 3);

	define("REDIAL_CRITERIA_INCOMPLETE", '((is.LastCalledTime IS NULL) OR (is.Success=0 AND is.RedialCount < '. REDIAL_INCOMPLETE_RETRY_LIMIT . ' AND DATEDIFF(NOW(), is.LastCalledTime) > 1))');
	// END REDIAL RULES - INCOMPLETE REMINDER

	// CRITERIA FOR CALLS - GENERIC
	define("CRITERIA_ALL_CALL", WO_ASSIGNED . " AND " . WO_NOT_INVOICE . " AND " . WO_NOT_DEACTIVATED);
	// END CRITERIA FOR CALLS - GENERIC

	define("CRITERIA_HAVE_START_DATE_TIME", "work_orders_with_timezone_view.StartDate IS NOT NULL AND IFNULL(work_orders_with_timezone_view.StartTime, '') != ''");

	// CRITERIA FOR CALLS - ACCEPTANCE
	// Acceptance calls at 10 AM Local Time - Repeat 3 times at 30 min interval
	define("ACCEPTANCE_CALL_IGNORE_TODAY_WO_IF_REMINDER_ONE_HR", "NOT((" . WO_REMINDER_1_HR . " OR " . WO_REMINDER_ALL . ") AND " . WO_STARTS_TODAY . ")");
	define("ACCEPTANCE_CALL_WO_STARTS_OVER_24HR", TIME_STARTTIME_MINUTES_BEFORE_START . " > 1440");
	define("CRITERIA_ACCEPTANCE_CALL", CRITERIA_ALL_CALL . " AND (" . TIME_CURRENT_TIME_LATER_THAN_10AM_LOCAL . " OR is.LastCalledTime IS NOT NULL) AND " . TIME_CURRENT_DATE_IS_WEEKDAY . " AND " . WO_ASSIGNED_MORE_THAN_24_HRS . " AND " . WO_NOT_REVIEWED . " AND " . WO_NOT_MARK_COMPLETED . " AND (" . WO_REMINDER_ACCEPTANCE . " OR " . WO_REMINDER_ALL . ") AND " . ACCEPTANCE_CALL_WO_STARTS_OVER_24HR . " AND " . ACCEPTANCE_CALL_IGNORE_TODAY_WO_IF_REMINDER_ONE_HR . " AND " . REDIAL_CRITERIA_ACCEPTANCE);
	// END CRITERIA FOR CALLS - ACCEPTANCE

	// CRITERIA FOR CALLS - 24 HR
	// 24 Hr calls at 10 AM Local Time w/ adjustments for weekend and Early Morning - Repeat 3 times at 30 min interval
	define("CRITERIA_24_HR_CALL", CRITERIA_ALL_CALL . " AND (" . TIME_CURRENT_TIME_LATER_THAN_10AM_LOCAL . " OR is.LastCalledTime IS NOT NULL) AND " . TIME_CURRENT_DATE_IS_WEEKDAY . " AND " . WO_NOT_MARK_COMPLETED . " AND (" . WO_REMINDER_24_HR . " OR " . WO_REMINDER_ALL . ") AND " . WO_STARTS_24_HR_ADJUST_WEEKEND_EARLY_MORNING . " AND " . REDIAL_CRITERIA_24_HR . " AND " . CRITERIA_HAVE_START_DATE_TIME);

	define("CRITERIA_CUSTOM_1_24_HR_CALL", CRITERIA_ALL_CALL . " AND (" . TIME_CURRENT_TIME_LATER_THAN_10AM_LOCAL . " OR is.LastCalledTime IS NOT NULL) AND " . TIME_CURRENT_DATE_IS_WEEKDAY . " AND " . WO_NOT_MARK_COMPLETED . " AND (" . WO_REMINDER_CUSTOM_HR_1 . ") AND " . WO_STARTS_CUSTOM_1_24_HR_ADJUST_WEEKEND_EARLY_MORNING . " AND " . REDIAL_CRITERIA_24_HR . " AND " . CRITERIA_HAVE_START_DATE_TIME);
	define("CRITERIA_CUSTOM_2_24_HR_CALL", CRITERIA_ALL_CALL . " AND (" . TIME_CURRENT_TIME_LATER_THAN_10AM_LOCAL . " OR is.LastCalledTime IS NOT NULL) AND " . TIME_CURRENT_DATE_IS_WEEKDAY . " AND " . WO_NOT_MARK_COMPLETED . " AND (" . WO_REMINDER_CUSTOM_HR_2 . ") AND " . WO_STARTS_CUSTOM_2_24_HR_ADJUST_WEEKEND_EARLY_MORNING . " AND " . REDIAL_CRITERIA_24_HR . " AND " . CRITERIA_HAVE_START_DATE_TIME);
	define("CRITERIA_CUSTOM_3_24_HR_CALL", CRITERIA_ALL_CALL . " AND (" . TIME_CURRENT_TIME_LATER_THAN_10AM_LOCAL . " OR is.LastCalledTime IS NOT NULL) AND " . TIME_CURRENT_DATE_IS_WEEKDAY . " AND " . WO_NOT_MARK_COMPLETED . " AND (" . WO_REMINDER_CUSTOM_HR_3 . ") AND " . WO_STARTS_CUSTOM_3_24_HR_ADJUST_WEEKEND_EARLY_MORNING . " AND " . REDIAL_CRITERIA_24_HR . " AND " . CRITERIA_HAVE_START_DATE_TIME);
	// END CRITERIA FOR CALLS - 24 HR

	// CRITERIA FOR CALLS - 1 HR
	// 1 Hr calls at Local Time - Repeat 3 times at 10 min interval
	define("CRITERIA_1_HR_CALL", CRITERIA_ALL_CALL . " AND " . WO_NOT_MARK_COMPLETED . " AND (" . WO_REMINDER_1_HR . " OR " . WO_REMINDER_ALL . ") AND " . WO_STARTS_1_HR_LOCAL_TIME . " AND NOT " . WO_STARTED . " AND " . REDIAL_CRITERIA_1_HR . " AND " . CRITERIA_HAVE_START_DATE_TIME);
	// CRITERIA FOR CALLS - 1 HR

	// CRITERIA FOR CALLS - SITE CONTACT
	// Site Contact calls at Local Time - Repeat 2 times at 10 min interval
	define("CRITERIA_SITE_CONTACT_CALL", CRITERIA_ALL_CALL . " AND " . WO_NOT_MARK_COMPLETED . " AND (" . WO_REMINDER_SITE_CONTACT . ") AND " . WO_STARTS_SITE_CONTACT_LOCAL_TIME . " AND NOT " . WO_STARTED . " AND " . REDIAL_CRITERIA_SITE_CONTACT . " AND " . CRITERIA_HAVE_START_DATE_TIME);
	// CRITERIA FOR CALLS - SITE CONTACT

	define("CRITERIA_CUSTOM_HR_CALL_1", CRITERIA_ALL_CALL . " AND " . WO_NOT_MARK_COMPLETED . " AND (" . WO_REMINDER_CUSTOM_HR_1 . ") AND " . WO_STARTS_CUSTOM_HR_LOCAL_TIME_1 . " AND NOT " . WO_STARTED . " AND " . REDIAL_CRITERIA_1_HR . " AND " . CRITERIA_HAVE_START_DATE_TIME);
	define("CRITERIA_CUSTOM_HR_CALL_2", CRITERIA_ALL_CALL . " AND " . WO_NOT_MARK_COMPLETED . " AND (" . WO_REMINDER_CUSTOM_HR_2 . ") AND " . WO_STARTS_CUSTOM_HR_LOCAL_TIME_2 . " AND NOT " . WO_STARTED . " AND " . REDIAL_CRITERIA_1_HR . " AND " . CRITERIA_HAVE_START_DATE_TIME);
	define("CRITERIA_CUSTOM_HR_CALL_3", CRITERIA_ALL_CALL . " AND " . WO_NOT_MARK_COMPLETED . " AND (" . WO_REMINDER_CUSTOM_HR_3 . ") AND " . WO_STARTS_CUSTOM_HR_LOCAL_TIME_3 . " AND NOT " . WO_STARTED . " AND " . REDIAL_CRITERIA_1_HR . " AND " . CRITERIA_HAVE_START_DATE_TIME);


	define("CRITERIA_CUSTOM_HR_CALL_1_CASE", CRITERIA_ALL_CALL . " AND " . WO_NOT_MARK_COMPLETED . " AND " . CRITERIA_HAVE_START_DATE_TIME ./* " AND work_orders_with_timezone_view.StartDate >= CURDATE()" */ " AND (CASE WHEN p.ReminderCustomHr > 24 THEN (". CRITERIA_CUSTOM_1_24_HR_CALL .") ELSE (". CRITERIA_CUSTOM_HR_CALL_1 .") END)");
	define("CRITERIA_CUSTOM_HR_CALL_2_CASE", CRITERIA_ALL_CALL . " AND " . WO_NOT_MARK_COMPLETED . " AND " . CRITERIA_HAVE_START_DATE_TIME ./* " AND work_orders_with_timezone_view.StartDate >= CURDATE()"*/  " AND (CASE WHEN p.ReminderCustomHr_2 > 24 THEN (". CRITERIA_CUSTOM_2_24_HR_CALL .") ELSE (". CRITERIA_CUSTOM_HR_CALL_2 .") END)");
	define("CRITERIA_CUSTOM_HR_CALL_3_CASE", CRITERIA_ALL_CALL . " AND " . WO_NOT_MARK_COMPLETED . " AND " . CRITERIA_HAVE_START_DATE_TIME ./* " AND work_orders_with_timezone_view.StartDate >= CURDATE()"*/  " AND (CASE WHEN p.ReminderCustomHr_3 > 24 THEN (". CRITERIA_CUSTOM_3_24_HR_CALL .") ELSE (". CRITERIA_CUSTOM_HR_CALL_3 .") END)");

	// CRITERIA FOR CALLS - EMERGENCY DISPATCH
	// Emergency Dispatch - Repeat 3 times at 10 min interval
	define("CRITERIA_EMERGENCY_DISPATCH_BACK_OUT_CALL", CRITERIA_ALL_CALL . " AND " . WO_NOT_MARK_COMPLETED . " AND " . WO_STARTS_1_HR_LOCAL_TIME . " AND " . REDIAL_CRITERIA_EMERGENCY_DISPATCH_BACK_OUT);
	// CRITERIA FOR CALLS - EMERGENCY DISPATCH

	// CRITERIA FOR CALLS - EMERGENCY DISPATCH
	// Emergency Dispatch - Repeat 3 times at 10 min interval
	define("CRITERIA_EMERGENCY_DISPATCH_NOT_ON_SITE_CALL", CRITERIA_ALL_CALL . " AND " . WO_NOT_MARK_COMPLETED . " AND " . WO_STARTS_1_HR_LOCAL_TIME . " AND " . REDIAL_CRITERIA_EMERGENCY_DISPATCH_NOT_ON_SITE);
	// CRITERIA FOR CALLS - EMERGENCY DISPATCH

/*	// Check IN / OUT at 10 AM Local Time
	define("CRITERIA_CHECK_IN_OUT_CALL", CRITERIA_ALL_CALL . " AND " . WO_NOT_MARK_COMPLETED . " AND " . WO_REMINDER_CHECK_IN_OUT . " AND " . WO_ENDED_YESTERDAY);*/

	// CRITERIA FOR CALLS - NOT MARKED COMPLETE
	// Not Marked Complete calls at 10 AM Local Time - Repeat 3 times at daily interval
	define("CRITERIA_NOT_MARKED_COMPLETE_CALL", CRITERIA_ALL_CALL . " AND " . TIME_CURRENT_TIME_LATER_THAN_10AM_LOCAL . " AND " . TIME_CURRENT_DATE_IS_WEEKDAY . " AND " . WO_NOT_MARK_COMPLETED . " AND (" . WO_REMINDER_NOT_MARKED_COMPLETE . " OR " . WO_REMINDER_ALL . ") AND " . WO_ENDED_BEFORE_TODAY . " AND " . REDIAL_CRITERIA_NOT_MARKED_COMPLETE . " AND " . CRITERIA_HAVE_START_DATE_TIME);
	// END CRITERIA FOR CALLS - NOT MARKED COMPLETE

	// CRITERIA FOR CALLS - INCOMPLETE
	// Incomplete calls at 10 AM Local Time - Repeat 3 times at daily interval
	define("CRITERIA_INCOMPLETE_CALL", CRITERIA_ALL_CALL . " AND " . TIME_CURRENT_TIME_LATER_THAN_10AM_LOCAL . " AND " . TIME_CURRENT_DATE_IS_WEEKDAY . " AND " . WO_NOT_MARK_COMPLETED . " AND (" . WO_REMINDER_INCOMPLETE . " OR " . WO_REMINDER_ALL . ") AND " . WO_INCOMPLETE_BEFORE_TODAY . " AND " . REDIAL_CRITERIA_INCOMPLETE . " AND " . CRITERIA_HAVE_START_DATE_TIME);
	// END CRITERIA FOR CALLS - INCOMPLETE


	/**
	 * Make query and fetch result
	 * @param (array|string) $fields array of fields or comma separated string with fields
	 * @param string $callType call type
	 * @param string $criteria SQL where statement
	 */
	function fetchReminderResults(array $fields, $callType, $criteria) {
		$db = Zend_Registry::get('DB');

		$select = $db->select()->from('work_orders_with_timezone_view', $fields)
		->joinLeft(array('is' => 'ivr_status'), "work_orders_with_timezone_view.WIN_NUM = is.WIN_NUM AND is.CallType = " . $db->quote($callType), '')
		->where('AcceptTerms != ?', 'WM')
		->where($criteria)->order('work_orders_with_timezone_view.StartDate');
		
		if ($callType == LOG_CALLTYPE_CUSTOM_HR_CALL_1 ||
			$callType == LOG_CALLTYPE_CUSTOM_HR_CALL_2 ||
			$callType == LOG_CALLTYPE_CUSTOM_HR_CALL_3 || 
			$callType == LOG_CALLTYPE_SITE_CONTACT_CALL)
			$select->join(array('p' => 'projects'), "work_orders_with_timezone_view.Project_ID = p.Project_ID", 
						 array("ReminderCustomHrChecked", "ReminderCustomHr", "ReminderCustomHrChecked_2", "ReminderCustomHr_2", "ReminderCustomHrChecked_3", "ReminderCustomHr_3", "SiteContactReminderCall"));

//		echo (string)$select;

		return $db->fetchAll($select);
	}

	function getReminderAcceptance(array $fields) {
		return fetchReminderResults($fields, LOG_CALLTYPE_ACCEPTANCE_CALL, CRITERIA_ACCEPTANCE_CALL);
	}

	function getReminder24Hr(array $fields) {
		return fetchReminderResults($fields, LOG_CALLTYPE_24_HR_CALL, CRITERIA_24_HR_CALL);
	}

	function getReminder1Hr(array $fields) {
		return fetchReminderResults($fields, LOG_CALLTYPE_1_HR_CALL, CRITERIA_1_HR_CALL);
	}

	function getReminderSiteContactHr(array $fields) {
		return fetchReminderResults($fields, LOG_CALLTYPE_SITE_CONTACT_CALL, CRITERIA_SITE_CONTACT_CALL);
	}

	function getReminderCustomHr1(array $fields) {
		return fetchReminderResults($fields, LOG_CALLTYPE_CUSTOM_HR_CALL_1, CRITERIA_CUSTOM_HR_CALL_1_CASE);
	}

	function getReminderCustomHr2(array $fields) {
		return fetchReminderResults($fields, LOG_CALLTYPE_CUSTOM_HR_CALL_2, CRITERIA_CUSTOM_HR_CALL_2_CASE);
	}

	function getReminderCustomHr3(array $fields) {
		return fetchReminderResults($fields, LOG_CALLTYPE_CUSTOM_HR_CALL_3, CRITERIA_CUSTOM_HR_CALL_3_CASE);
	}

	function getReminderEmergencyDispatchBackOut(array $fields) {
		return fetchReminderResults($fields, LOG_CALLTYPE_EMERGENCY_DISPATCH_BACK_OUT_CALL, CRITERIA_EMERGENCY_DISPATCH_BACK_OUT_CALL);
	}

	function getReminderEmergencyDispatchNotOnSite(array $fields) {
		return fetchReminderResults($fields, LOG_CALLTYPE_EMERGENCY_DISPATCH_NOT_ON_SITE_CALL, CRITERIA_EMERGENCY_DISPATCH_NOT_ON_SITE_CALL);
	}

	function getReminderNotMarkedComplete(array $fields) {
		return fetchReminderResults($fields, LOG_CALLTYPE_NOT_MARKED_COMPLETE_CALL, CRITERIA_NOT_MARKED_COMPLETE_CALL);
	}

	function getReminderIncomplete(array $fields) {
		return fetchReminderResults($fields, LOG_CALLTYPE_INCOMPLETE_CALL, CRITERIA_INCOMPLETE_CALL);
	}

	function initiateEmergencyOutboundBG($uniqueID, $techID, $phone, $type) {
		$phone = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);

		$phone = str_replace(array("-", ".", " ", "(", ")"), array("", "", "", "", ""), $phone);

//		if ($phone != "5122728328" || $phone != "8326878350")
//			$phone = "5122728328";

		if (strpos($phone, "+1") === FALSE)
			$phone = "+1" . $phone;

		echo "<!-- (/usr/bin/php -f " . dirname(__FILE__) . "/emergencyDispatchBG.php " . escapeshellarg($uniqueID) . " " . escapeshellarg($techID) . " " . escapeshellarg($phone) . " " . escapeshellarg($type) . ") -->";
		exec("(/usr/bin/php -f " . dirname(__FILE__) . "/emergencyDispatchBG.php " . escapeshellarg($uniqueID) . " " . escapeshellarg($techID) . " " . escapeshellarg($phone) . " " . escapeshellarg($type) . ") > /dev/null 2>&1 &");

	}

	function initiateEmergencyOutbound($uniqueID, $techID, $phone, $type) {
		$logID = logIVRCall($uniqueID, $techID, $type, "Pending", 0);

		$callResult = outboundCall(TOKEN_OUTBOUND_REMINDER, $phone, array("TechID" => $techID, "Pin" => "outbound", "outboundType" => $type, "uniqueID" => $uniqueID, "IVRLogID" => $logID, "calledNumber" => $phone, "voxeo-cpa-maxtime" => "3400", "voxeo-cpa-maxsilence" => "1200", "voxeo-cpa-runtime" => "20000") );

		$msg = "Phone: $phone
		TechID: $techID
		CallResult: $callResult
		";

//		smtpMail("IVR Application", "nobody@fieldsolutions.com", "tngo@fieldsolutions.com", "Reminder Call Type - $type (ID: $logID)", $msg, nl2br($msg), "IVR Application");

		processCallResult($uniqueID, $companyID, $phone, $callResult, $logID, $type);
	}

	function cancelEmergencyOutbound($uniqueID, $techID, $phone, $type) {
		logIVRCall($uniqueID, $techID, $type, LOG_CALLRESULT_EMERGENCY_DISPATCH_CANCEL, 0);
	}


	function processCallResult($uniqueID, $companyID, $phone, $callResult, $logID, $callType) {
		$parts = explode(": ", $callResult);
		$callResultOrig = $callResult;
		$callResult = trim($parts[0]);
		$cpaResult = trim($parts[1]);

		if ($callResult == "Call number is bad" || $callResult == "Invalid Token.")
			$cpaResult = $callResult;


		if ($callResult != "success" && $callResult != "Success") {
			switch ($cpaResult) {
				case "No Answer":
				case "Busy":
					break;
				case "Call number is bad":
					notifyMaintainer("Bad phone number.<br/>LogID: - $logID<br/>Phone - $phone");
					break;
				case "Invalid Token.":
					notifyMaintainer("Invalid outgoing token.<br/>LogID: - $logID<br/>");
					break;
				default:
					$cpaResult = LOG_CALLRESULT_NO_ANSWER;
					$callResult = LOG_CALLRESULT_NO_ANSWER;
			}

			logIVRCallUpdate($logID, $callResult, 0);
			logIVRContact("IVR Reminder", $companyID, $uniqueID, $phone, $callType, "Result: $cpaResult");
			requestUpdateForNoAnswer($uniqueID, $callType, $clientEmail);
			doNotifications($uniqueID, $callType, LOG_CALLRESULT_NO_ANSWER, $logID);
		}
	}

	function doNotifications($uniqueID, $callType, $callResult, $logID = "") {
			// Client / SDM Email Notification


			$subect = "";
			$msg = "";

			$fieldsList = array(
				'Project_Name',
				'ProjectManagerEmail',
				'EmergencyPhone',
				'Tech_ID',
				'is.RedialCount',
				'work_orders_with_timezone_view.StartDate',
				'work_orders_with_timezone_view.StartTime',
				'Headline'
			);
			$criteria = 'work_orders_with_timezone_view.WIN_NUM = ' . intval($uniqueID);

			$wo = fetchReminderResults($fieldsList, $callType, $criteria);
			$wo = $wo[0];
			$coreClass = new Core_Api_Class;
			$additionalFields = $coreClass->getWorkOrderAdditionalFields($uniqueID);
			
			$projectName = $wo['Project_Name'];
			$clientEmail = $additionalFields['ACSNotifiPOC_Email'];
			$emergencyPhone = $additionalFields['ACSNotifiPOC_Phone'];
			$techID = $wo['Tech_ID'];
			$callCount = $wo['RedialCount'];
			$startDate = $wo['StartDate'];
			$startTime = $wo['StartTime'];
			$headline = $wo['Headline'];
			
			$customCallMoreThan24Hr = false;
			$customCallMoreThan1Hr = false;
			if ($callType == LOG_CALLTYPE_CUSTOM_HR_CALL_1 || $callType == LOG_CALLTYPE_CUSTOM_HR_CALL_2 || $callType == LOG_CALLTYPE_CUSTOM_HR_CALL_3) {
				$customHr_1 = $wo['ReminderCustomHr'];
				$customHr_2 = $wo['ReminderCustomHr_2'];
				$customHr_3 = $wo['ReminderCustomHr_3'];
				if ($callType == LOG_CALLTYPE_CUSTOM_HR_CALL_1) {
					if ($customHr_1 > 24) $customCallMoreThan24Hr = true;
					if ($customHr_1 > 1) $customCallMoreThan1Hr = true;
				}
				else if ($callType == LOG_CALLTYPE_CUSTOM_HR_CALL_2) {
					if ($customHr_2 > 24) $customCallMoreThan24Hr = true;
					if ($customHr_2 > 1) $customCallMoreThan1Hr = true;
				}
				else if ($callType == LOG_CALLTYPE_CUSTOM_HR_CALL_3) {
					if ($customHr_3 > 24) $customCallMoreThan24Hr = true;
					if ($customHr_3 > 1) $customCallMoreThan1Hr = true;
				}
			}

			$noAnswerNotification = false;
			$doEmergencyCall = false;
			$emergencyType = "";
									
			if ($callResult == LOG_CALLRESULT_NO_ANSWER || $callResult == LOG_CALLRESULT_ANSWERED_MACHINE || $callResult == LOG_CALLRESULT_ANSWERED_NO_RESPONSE) {
				switch ($callType) {
					case LOG_CALLTYPE_ACCEPTANCE_CALL:
						if ($callCount == REDIAL_ACCEPTANCE_RETRY_LIMIT)
							$noAnswerNotification = true;
						break;
					case LOG_CALLTYPE_24_HR_CALL:
						if ($callCount == REDIAL_24_HR_RETRY_LIMIT)
							$noAnswerNotification = true;
						break;
					case LOG_CALLTYPE_NOT_MARKED_COMPLETE_CALL:
						if ($callCount == REDIAL_NOT_MARKED_COMPLETE_RETRY_LIMIT)
							$noAnswerNotification = true;
						break;
					case LOG_CALLTYPE_INCOMPLETE_CALL:
						if ($callCount == REDIAL_INCOMPLETE_RETRY_LIMIT)
							$noAnswerNotification = true;
						break;
					case LOG_CALLTYPE_SITE_CONTACT_CALL:
							if ($callCount >= REDIAL_SITE_CONTACT_RETRY_LIMIT)
								$noAnswerNotification = true;
							if ($callCount == REDIAL_SITE_CONTACT_RETRY_LIMIT) {
								$doEmergencyCall = true;
								$emergencyType = LOG_CALLTYPE_EMERGENCY_DISPATCH_SITE_CONTACT_BACK_OUT_CALL;
							}
						break;
					case LOG_CALLTYPE_1_HR_CALL:
					case LOG_CALLTYPE_CUSTOM_HR_CALL_1:
					case LOG_CALLTYPE_CUSTOM_HR_CALL_2:
					case LOG_CALLTYPE_CUSTOM_HR_CALL_3:
						if (!$customCallMoreThan24Hr) {
							$callCountThreshold = $customCallMoreThan1Hr ? (REDIAL_1_HR_RETRY_LIMIT) : (REDIAL_1_HR_RETRY_LIMIT - 1); // how many reminders before emergency call is triggered -- for <=1hr do after 2nd reminder
							if ($callCount >= REDIAL_1_HR_RETRY_LIMIT - 1)
								$noAnswerNotification = true;
							if ($callCount == $callCountThreshold) {
								$doEmergencyCall = true;
								$emergencyType = LOG_CALLTYPE_EMERGENCY_DISPATCH_BACK_OUT_CALL;
							}
						}
						else {
							// do 24 hr logic
							if ($callCount == REDIAL_24_HR_RETRY_LIMIT)
								$noAnswerNotification = true;
						}
						break;
				}
			}
			else {
				// Notification for successful calls
				switch ($callResult) {
					case LOG_CALLRESULT_ANSWERED_SUCCESS_BACK_OUT:
						$subject = "Action Needed: Tech Back out of work order ($projectName / $callType)";
						if (!$customCallMoreThan24Hr && ($callType == LOG_CALLTYPE_1_HR_CALL || $callType == LOG_CALLTYPE_CUSTOM_HR_CALL_1 || $callType == LOG_CALLTYPE_CUSTOM_HR_CALL_2 || $callType == LOG_CALLTYPE_CUSTOM_HR_CALL_3 || $callType == LOG_CALLTYPE_SITE_CONTACT_CALL)) {
							$subject = "URGENT Action Needed: Tech NO-SHOW for work order ($projectName / $callType)";
							$doEmergencyCall = true;
							$emergencyType = $callType == LOG_CALLTYPE_SITE_CONTACT_CALL ? LOG_CALLTYPE_EMERGENCY_DISPATCH_SITE_CONTACT_BACK_OUT_CALL : LOG_CALLTYPE_EMERGENCY_DISPATCH_BACK_OUT_CALL;
						}
						break;
					case LOG_CALLRESULT_ANSWERED_SUCCESS_RESCHEDULE:
						$subject = "Tech has requested a reschedule of project $projectName during a $callType";
						break;
					case LOG_CALLRESULT_CIO_SUCCESS_NOT_ON_SITE:
						$subject = "Action Needed: Tech is not on site for check in ($projectName / $callType)";
						$doEmergencyCall = true;
						$emergencyType = LOG_CALLTYPE_EMERGENCY_DISPATCH_NOT_ON_SITE_CALL;
						break;
					default:
						$subject = "";
				}
			}

			if ($noAnswerNotification) {
				$attemptCount = $callType == LOG_CALLTYPE_1_HR_CALL ? "after $callCount tries " : "";
                                //831
                                if(isset($_SERVER['HTTP_REFERER']) 
                                        && strpos($_SERVER['HTTP_REFERER'], "verifyTech.php")!== false
                                        && $callType == LOG_CALLTYPE_NOT_MARKED_COMPLETE_CALL )
                                {
                                    $subject = "";
                                }
                                else
                                {
				    $subject = "Action Needed: Unable to contact tech " . $attemptCount . "($projectName / $callType)";
			        }
                                //end 831
			}
			$fromName = "IVR Application";
			$fromEmail = "nobody@fieldsolutions.com";

			if ($subject != "") {
				$msg = "WIN#: $uniqueID
					Headline: $headline";
				if ($callType == LOG_CALLTYPE_SITE_CONTACT_CALL) {
					// emails for site contact call
					$woaddifields = $additionalFields;
					$fromName = "FieldSolutions";
					$fromEmail = "support@fieldsolutions.com";
				    $apiclass = new Core_Api_Class();
				    $wo = $apiclass->getWorkOrdersWithWinNum($uniqueID, '', 'WIN_NUM', '');
				    $wo = $wo[0];
				    $woaddifields = $apiclass->getWorkOrderAdditionalFields($uniqueID);
		            if(!empty($wo['StartDate']) && $wo['StartDate'] !="0000-00-00")
		            {
		                $StartDate = date_format(new DateTime($wo['StartDate']), "m/d/Y");
		            }

					if ($noAnswerNotification) {
						// no answer
						$subject = "WIN# $uniqueID: Unable to Reach Site Contact";
						$reason = "Please be aware that FieldSolutions was unable to contact the Site Contact.";
					}
					else if ($callResult == LOG_CALLRESULT_ANSWERED_SUCCESS_BACK_OUT) {
						// back out
						$subject = "WIN# $uniqueID: Site Contact Backed Out";
						$reason = "Please be aware that the Site Contact for the Work Order listed below has &lsquo;backed out.&rsquo;";
					}
		            $msg = "<br/>";
		            $msg .= "<p>$reason FieldSolutions recommends confirming that there will be someone available to provide the technician access to the site.</p>";
		            $msg .="<p>WIN#: " . $wo['WIN_NUM'] . "<br/>";
		            $msg .="Client Work Order ID: " . $wo['WO_ID'] . "<br/>";
		            $msg .="Project: " . $wo['Project_Name'] . "<br/>";
		            $msg .="Start Date: " . $StartDate . " " . $wo['StartTime'] . "<br/>";
		            $msg .="Site Name: " . $wo['SiteName'] . "<br/>";
		            $msg .="Site #: " . $wo['SiteNumber'] . "<br/>";
		            $msg .="Site Address: " . $wo['Address'] . ", " . $wo['City'] . ", " . $wo['State'] . "" . $wo['Zipcode'] . "<br/>";
		            $msg .="Site Contact Name: " . $wo['Site_Contact_Name'] . "<br/>";
		            $msg .="Site Phone #: " . $wo['SitePhone'] . "<br/>";
		            $msg .="FS-Tech ID#: " . $wo['Tech_ID'] . "<br/>";
		            $msg .="Tech Name: " . $wo['Tech_FName'] . " " . $wo['Tech_LName'] . "<br/>";
		            $msg .="Tech Phone #: " . $wo['TechPhone'] . "<br/>";
		            $msg .="Tech Email: " . $wo['TechEmail'] . "</p>";
		            $msg .="<p>Thank you,<br/>";
		            $msg .="Your FieldSolutions Team</p>";
		            $msg .= "<br/>";

				}
				else if (!$customCallMoreThan24Hr && ($callType == LOG_CALLTYPE_1_HR_CALL || $callType == LOG_CALLTYPE_CUSTOM_HR_CALL_1 || $callType == LOG_CALLTYPE_CUSTOM_HR_CALL_2 || $callType == LOG_CALLTYPE_CUSTOM_HR_CALL_3 || $callType == LOG_CALLTYPE_CUSTOM_HR_CALL_3)) {
					$msg .= "
					Start Time: $startDate $startTime

					The technician assigned to the above work order has NO-Showed for work starting in less than one hour. You should consider this technician for this work order a NO SHOW and take immediate urgent action either contacting the technician, or end-client/site, or finding a new technician for coverage.

					Thank you.
					Field Solutions";
				}
				else if ($callResult == LOG_CALLRESULT_ANSWERED_SUCCESS_BACK_OUT) {
					$msg .= "

					The technician assigned to the above work order has backed out of his/her offer. You should consider this work order UNASSIGNED and needing a new assignment. Please call the original assigned tech or select another applicant.

					Thank you.
					Field Solutions";
				}
				else if ($noAnswerNotification) {
					$msg .= "

					This technician has not responded to contact attempts. Action is needed.

					Thank you.
					Field Solutions";
				}
				else {
					$msg .= "

					Thank you.
					Field Solutions";
				}
				$html = nl2br($msg);
				smtpMail($fromName, $fromEmail, "$clientEmail", $subject, $msg, $html, "IVR Application");
			}

			$msg = "LogID: $logID
			UniqueID: $uniqueID
			CallType: $callType
			callResult: $callResult
			Email Sent: $subject
			Emergency Type: $emergencyType

			" . $msg;
			$html = nl2br($msg);
			
			if ($callType == LOG_CALLTYPE_SITE_CONTACT_CALL) {
				// do updates for site contact
				$db = Zend_Registry::get('DB');
				$siteConfirmed = 0;
				$siteBackout = 1;
				$notifyTime = date("Y-m-d H:i:s");
				$notes = "";
				if ($callResult == LOG_CALLRESULT_NO_ANSWER || $callResult == LOG_CALLRESULT_ANSWERED_MACHINE || $callResult == LOG_CALLRESULT_ANSWERED_NO_RESPONSE) {
					$notes = "Site Contact answered " . ($callCount == 1 ? "first" : "second") . " attempt and hung up\n";
					if ($callCount == 2)
						$notes .= "No Answer on second attempt.\n";
				}
				else if ($callResult == LOG_CALLRESULT_ANSWERED_SUCCESS) {
					$notes = "Site Contact Confirmed\n";
					$siteConfirmed = 1;
					$siteBackout = 0;
				}
				$db->update('work_orders', array('StoreNotified' => $siteConfirmed, 'DateNotified' => $notifyTime, 'Notifiedby' => 'ACS Call', 'NotifyNotes' => new Zend_Db_Expr("CONCAT(IFNULL(NotifyNotes,'')," . $db->quoteInto("?", $notes) . ")")), $db->quoteInto('WIN_NUM=?', $uniqueID));		
				$db->update('work_orders_additional_fields', array('SiteContactBackedOutChecked' => $siteBackout, 'NotifiedTime' => $notifyTime), $db->quoteInto('WINNUM=?', $uniqueID));
			}
//			smtpMail("IVR Application", "nobody@fieldsolutions.com", "tngo@fieldsolutions.com", "doNotification Grouped (LogID: $logID)", $msg, $html, "IVR Application");


			if ($doEmergencyCall && !empty($emergencyPhone)) {
				initiateEmergencyOutboundBG($uniqueID, $techID, $emergencyPhone, $emergencyType);
			}
	}

	function setTechSession($info) {
		// set user session variables after login verified
		if (!is_array($info)) return false;
		$_SESSION["loggedIn"] = "yes";
		$_SESSION["TechID"] = $info[0];
		$_SESSION["FSCert"] = ($info[2] == "True");
		$_SESSION["Latitude"] = $info[3];
		$_SESSION["Longitude"] = $info[4];
		$_SESSION['UserName'] = trim($info[5], "`");
		$_SESSION['Firstname'] = trim($info[6], "`");
		$_SESSION['Lastname'] = trim($info[7], "`");
		$_SESSION["TechName"] = $_SESSION['Firstname'] . " " . $_SESSION['Lastname'];
		$_SESSION["Password"] = trim($info[8], "`");
	}

	function notifyMaintainer($msg) {
		smtpMailLogReceived("IVR Application", "nobody@fieldsolutions.com", "tngo@fieldsolutions.com", "IVR Application Message", $msg, $msg, "IVR Application");
	}

	function logIVRCall($woUNID, $techID, $callType, $result, $length, $parentCall = 0) {
		return Core_IVR_Log::create($woUNID, $techID, $callType, $parentCall);
		}

	function logIVRCallUpdate($logID, $result, $length, $techID = "", $WOUNID = "", $parentCall = "") {
		Core_IVR_Log::update($logID, $result, $length, $techID, $WOUNID, $parentCall);
		}
		
	function markAcceptWO($id, $companyID, $clientName, $projectName) {
		$db = Zend_Registry::get('DB');
		// mark a WO as reviewed
		$db->update('work_orders', array('WorkOrderReviewed' => 1), $db->quoteInto('WIN_NUM=?', $id));
		//caspioUpdate("Work_Orders", "WorkOrderReviewed", "'1'", "TB_UNID = '" . caspioEscape($id) . "'", false);
		Core_TimeStamp::createTimeStamp($id, $_SESSION["UserName"], "Work Order Reviewed (IVR)", $companyID, $clientName, $projectName, "", $_SESSION["TechID"], $_SESSION["TechName"]);
	}

	function mark24HrConfirmWO($id, $companyID, $clientName, $projectName) {
		$db = Zend_Registry::get('DB');
		// mark a WO as confirmed
		$db->update('work_orders', array('TechCheckedIn_24hrs' => 1), $db->quoteInto('WIN_NUM=?', $id));
		//caspioUpdate("Work_Orders", "TechCheckedIn_24hrs", "'1'", "TB_UNID = '" . caspioEscape($id) . "'", false);
		Core_TimeStamp::createTimeStamp($id, $_SESSION["UserName"], "Work Order Confirmed (IVR)", $companyID, $clientName, $projectName, "", $_SESSION["TechID"], $_SESSION["TechName"]);
	}

	function mark1HrConfirmWO($id, $companyID, $clientName, $projectName) {
		// mark a WO as confirmed
//		caspioUpdate("Work_Orders", "TechCheckedIn_24hrs", "'1'", "TB_UNID = '" . caspioEscape($id) . "'", false);
		Core_TimeStamp::createTimeStamp($id, $_SESSION["UserName"], "Work 1 Hr Confirmed (IVR)", $companyID, $clientName, $projectName, "", $_SESSION["TechID"], $_SESSION["TechName"]);
	}

	function backOutWO($id, $companyID, $clientName, $projectName, $timeDiff, $to_email, $phone = "") {
		// mark a WO as backed out
		$backOutField = "BackOut_Tech";
		$backOutValue = $backOutField;
		$callType = $_SESSION["outboundType"];
		if (callType == LOG_CALLTYPE_24_HR_CALL || ($_SESSION["CustomCallMoreThan24Hr"] === true))
			$backOutValue = ($timeDiff >= TIME_BACKOUT_THERSHOLD_MINUTES ? "BackOut_Tech" : "{$_SESSION["TechID"]}");
		else if ($_SESSION["outboundType"] == LOG_CALLTYPE_1_HR_CALL || $callType == LOG_CALLTYPE_CUSTOM_HR_CALL_1 || $callType == LOG_CALLTYPE_CUSTOM_HR_CALL_2 || $callType == LOG_CALLTYPE_CUSTOM_HR_CALL_3) {
			$backOutField = "NoShow_Tech";
			$backOutValue = "{$_SESSION["TechID"]}";
		}
		$db = Zend_Registry::get('DB');
		$db->update('work_orders', array($backOutField => $backOutValue, 'Update_Requested' => 1, 'Update_Reason' => 'Tech Backed Out'), $db->quoteInto('WIN_NUM=?', $id));
		//caspioUpdate("Work_Orders", "$backOutField, Update_Requested, Update_Reason", "$backOutValue, '1', 'Tech Backed Out'", "TB_UNID = '" . caspioEscape($id) . "'", false);
		Core_TimeStamp::createTimeStamp($id, $_SESSION["UserName"], "Tech Backed Out (IVR)", $companyID, $clientName, $projectName, "", $_SESSION["TechID"], $_SESSION["TechName"]);
		$msg = "UNID: $id
		Project: $projectName
		Tech ID: {$_SESSION["TechID"]}";
		$html = nl2br($msg);
//		smtpMail("IVR Application", "nobody@fieldsolutions.com", $to_email, "A tech has backed out of project $projectName", $msg, $html, "IVR Application");
	}

	function rescheduleWO($id, $companyID, $clientName, $projectName, $timeDiff, $to_email) {
		// request reschedule
//		caspioUpdate("Work_Orders", "WorkOrderReviewed, TechCheckedIn_24hrs, Tech_ID, Tech_LName, Tech_FName, TechEmail, TechPhone, Update_Requested, Update_Reason", "'0', '0', DEFAULT, '', '', '', '', '1', 'Tech Reschedule Request'", "TB_UNID = '" . caspioEscape($id) . "'", false);
		Core_TimeStamp::createTimeStamp($id, $_SESSION["UserName"], "Tech Reschedule Request (IVR)", $companyID, $clientName, $projectName, "", $_SESSION["TechID"], $_SESSION["TechName"]);
		$msg = "UNID: $id
		Project: $projectName
		Tech ID: {$_SESSION["TechID"]}";
		$html = nl2br($msg);
//		smtpMail("IVR Application", "nobody@fieldsolutions.com", $to_email, "A tech has requested reschedule for project $projectName", $msg, $html, "IVR Application");
	}

	function requestUpdateForNoAnswer($id, $reminderCallType, $clientEmail) {
		$db = Zend_Registry::get('DB');
		$db->update('work_orders', array('Update_Requested' => 1, 'Update_Reason' => 'No answer'), $db->quoteInto('WIN_NUM=?', $id));
		//caspioUpdate("Work_Orders", "Update_Requested, Update_Reason", "'1', 'No answer'", "TB_UNID = '" . caspioEscape($id) . "'", false);
//		Create_TimeStamp($id, $_SESSION["UserName"], "Tech Reschedule Request (IVR)", $companyID, $clientName, $projectName, "", $_SESSION["TechID"], $_SESSION["TechName"]);
		$msg = "UNID: $id
		Project: $projectName
		Tech ID: {$_SESSION["TechID"]}";
		$html = nl2br($msg);
//		smtpMail("IVR Application", "nobody@fieldsolutions.com", $clientEmail, "Unable to reach tech during $reminderCallType call for project $projectName", $msg, $html, "IVR Application");
	}

	function setIVRCallResult($logID, $result) {
		if ($_SESSION["GroupedCall"]) {
			$logID = explode(",", $logID);
			if (!isset($_SESSION["IVRCallResult"]) || !is_array($_SESSION["IVRCallResult"]))
				$_SESSION["IVRCallResult"] = array();
			foreach ($logID as $id) {
				$_SESSION["IVRCallResult"][$id] = $result;
			}
		}
		else
			$_SESSION["IVRCallResult"] = $result;
	}
?>
