<?php
	define("TOKEN_OUTBOUND_REMINDER", "293f435b5ee27a4cbdc217cde53d83a36dd1b480df43d5002021e0411155a6fcf16129e24857414d9e4953cf");
//	define("TOKEN_OUTBOUND_REMINDER", "177c879fe88cc24b9c1114422a088d3e73b0d860fbf216e5d10667ddcb6d5fff9add28d001fbf7cee24e73bf"); // testing token

	function outboundCall($appTokenID, $phoneNumber, $miscVars = NULL) {
		// call a phone number and begin running a VoiceXML application with the matching appTokenID
		
		$miscVarsTxt = "";
		if (is_array($miscVars)) {
			foreach ($miscVars as $var=>$val) {
				$var = urlencode($var);
				$val = urlencode($val);
				$miscVarsTxt .= "&$var=$val";
			}
		}
		$ch = curl_init();
		$appTokenID = urlencode($appTokenID);
		$phoneNumber =  urlencode($phoneNumber);
		// set URL and other appropriate options
//		echo "http://api.voxeo.net/SessionControl/VoiceXML.start?tokenid=$appTokenID&numbertodial=$phoneNumber&callerid=$phoneNumber" . $miscVarsTxt;
		$options = array(CURLOPT_URL => "http://api.voxeo.net/SessionControl/CCXML10.start?tokenid=$appTokenID&numbertodial=$phoneNumber&calltimeout=120" . $miscVarsTxt,
						 CURLOPT_HEADER => false,
						 CURLOPT_RETURNTRANSFER => true
						);
		
//		$options["calledNumber"] = $phoneNumber;
		
		curl_setopt_array($ch, $options);
		
		// grab URL and pass it to the browser
		$response = curl_exec($ch);
		
		// close cURL resource, and free up system resources
		curl_close($ch);
		return $response;
	}
?>
