<?php
	require_once("ivrLib.php");
	require_once("voxeoAPI.php");
	require_once(realpath(dirname(__FILE__) . "/../../") . "/library/smtpMail.php");

	function reminderCallBG($uniqueID, $companyID, $phone, $callType, $techID) {
		$pathInfo = dirname(__FILE__);
		//echo "\n\n(/usr/bin/php -f " . dirname(__FILE__) . "/reminderCallBG.php " . escapeshellarg($uniqueID) . " " . escapeshellarg($companyID) . " " . escapeshellarg($phone) . " " . escapeshellarg($callType) . " " . escapeshellarg($techID) . ") > /dev/null 2>&1 &\n\n";
		exec("(/usr/bin/php -f " . dirname(__FILE__) . "/reminderCallBG.php " . escapeshellarg($uniqueID) . " " . escapeshellarg($companyID) . " " . escapeshellarg($phone) . " " . escapeshellarg($callType) . " " . escapeshellarg($techID) . ") > /dev/null 2>&1 &");
	}

	function reminderCall($uniqueID, $companyID, $phone, $callType, $techID) {
		error_log("*** Start Call");
		$isGrouped = !is_numeric($uniqueID);
		$uniqueIDList = NULL;
		$companyIDList = NULL;
		$parentCall = 0;
		if ($isGrouped) {
			$uniqueIDList = explode(",", $uniqueID);
			$companyIDList = explode(",", $companyID);
			$logIDList = array();
			$gotParent = FALSE;
			foreach ($uniqueIDList as $uid) {
				$newID = logIVRCall($uid, $techID, $callType, "Pending", 0, $parentCall);
				if (!$gotParent) {
					$parentCall = $newID;
					$gotParent = TRUE;
				}
				$logIDList[] = $newID;
			}
			$logID = implode(",", $logIDList);
		}
		else
			$logID = logIVRCall($uniqueID, $techID, $callType, "Pending", 0);
//		if ($phone != "5122728328" || $phone != "8326878350")
//			$phone = "5122728328";
//		if ($companyID != "CBD" && $companyID != "BW" && $companyID != "FS")
//			return;

//		$phone = "5122728328";
		
		if (strpos($phone, "+1") === FALSE)
			$phone = "+1" . $phone;

		$callResult = outboundCall(TOKEN_OUTBOUND_REMINDER, $phone, array("TechID" => $techID, "Pin" => "outbound", "outboundType" => $callType, "uniqueID" => $uniqueID, "IVRLogID" => $logID, "calledNumber" => $phone, "voxeo-cpa-maxtime" => "3200", "voxeo-cpa-maxsilence" => "1200", "voxeo-cpa-runtime" => "20000", "calltimeout" => "120") );

		$msg = "Phone: $phone
		TechID: $techID
		CallResult: $callResult
		";
			
//		smtpMail("IVR Application", "nobody@fieldsolutions.com", "tngo@fieldsolutions.com", "Reminder Call Type Grouped - $callType (ID: $logID)", $msg, nl2br($msg), "IVR Application");
		
		$index = 0;
		if ($isGrouped) {
			foreach ($uniqueIDList as $unid) {
				processCallResult($unid, $companyIDList[$index], $phone, $callResult, $logIDList[$index], $callType);
				$index++;
			}
		}
		else
			processCallResult($uniqueID, $companyID, $phone, $callResult, $logID, $callType);
	}		
?>
