<?php
//	ini_set("display_errors", 1);
	require_once("ivrheader.php");
	require_once("../library/caspioAPI.php");
	require_once("library/ivrLib.php");
	require_once("../library/woContactLog_ivr.php");	
	writeIVRHeader();
	$uniqueID = $_SESSION["uniqueID"];
	$cpa_result = $_SESSION["CPAResult"];
	
//	$_SESSION["TechID"] = 19806;
//	$uniqueID = 7103;
	
	if ($_SESSION["TechID"] == "") die();
	
	// Group Call Code
	if ($_SESSION["GroupedCall"]) {
		if (!isset($_SESSION["GroupedCallWOList"])) {	
//			$wo = caspioSelectAdv("Work_Orders_With_Timezone", "ISNULL(CONVERT(VARCHAR, StartDate, 101), ''), ISNULL(StartTime, ''), ISNULL(City, ''), ISNULL(State, ''), ISNULL(Zipcode, ''), " . TIME_CURRENT_TIME_LOCAL . ", ISNULL(Headline, ''), ISNULL(Tech_Bid_Amount, ''), ISNULL(Amount_Per, 'Site'), Company_ID, Company_Name, Project_Name, " . TIME_STARTTIME_HOURS_BEFORE_START . ", ProjectManagerEmail, TB_UNID", "TB_UNID IN ($uniqueID) AND Tech_ID = '" . caspioEscape($_SESSION["TechID"]) . "'", "", true, "`", "|", false);
			$db = Zend_Registry::get('DB');
			$sql = "SELECT IFNULL(DATE_FORMAT(StartDate, '%m/%d/%Y'), ''), IFNULL(StartTime, ''), IFNULL(City, ''), IFNULL(State, ''), IFNULL(Zipcode, ''), " . TIME_CURRENT_TIME_LOCAL . ", IFNULL(Headline, ''), IFNULL(Tech_Bid_Amount, ''), IFNULL(Amount_Per, 'Site'), Company_ID, Company_Name, Project_Name, " . TIME_STARTTIME_HOURS_BEFORE_START . ", ProjectManagerEmail, WIN_NUM FROM work_orders_with_timezone_view WHERE WIN_NUM = ? AND Tech_ID = ?";
			$wo = $db->fetchAll($sql, array($uniqueID, $_SESSION["TechID"]), Zend_Db::FETCH_NUM);

			$_SESSION["GroupedCallWOList"] = $wo;
			$_SESSION["GroupedCallCurrent"] = 0; // used for looping of acceptance, 24 hr, 1 hr.
			$_SESSION["GroupedCallTotal"] = count($wo);
		}
		$wo = $_SESSION["GroupedCallWOList"];
		$currentWO = $_SESSION["GroupedCallCurrent"];
		$totalWO = $_SESSION["GroupedCallTotal"];
		$wo[0] = $wo[$currentWO];
		$_SESSION["GroupedCallCurrent"] += 1;
	}
	else {
//		$wo = caspioSelectAdv("Work_Orders_With_Timezone", "ISNULL(CONVERT(VARCHAR, StartDate, 101), ''), ISNULL(StartTime, ''), ISNULL(City, ''), ISNULL(State, ''), ISNULL(Zipcode, ''), " . TIME_CURRENT_TIME_LOCAL . ", ISNULL(Headline, ''), ISNULL(Tech_Bid_Amount, ''), ISNULL(Amount_Per, 'Site'), Company_ID, Company_Name, Project_Name, " . TIME_STARTTIME_HOURS_BEFORE_START . ", ProjectManagerEmail", "TB_UNID = '$uniqueID' AND Tech_ID = '" . caspioEscape($_SESSION["TechID"]) . "'", "", true, "`", "|", false);
		$db = Zend_Registry::get('DB');
		$sql = "SELECT IFNULL(DATE_FORMAT(StartDate, '%m/%d/%Y'), ''), IFNULL(StartTime, ''), IFNULL(City, ''), IFNULL(State, ''), IFNULL(Zipcode, ''), " . TIME_CURRENT_TIME_LOCAL . ", IFNULL(Headline, ''), IFNULL(Tech_Bid_Amount, ''), IFNULL(Amount_Per, 'Site'), Company_ID, Company_Name, Project_Name, " . TIME_STARTTIME_HOURS_BEFORE_START . ", ProjectManagerEmail, WIN_NUM FROM work_orders_with_timezone_view WHERE WIN_NUM = ? AND Tech_ID = ?";
		$wo = $db->fetchAll($sql, array($uniqueID, $_SESSION["TechID"]), Zend_Db::FETCH_NUM);		
	}
		
	$foundWO = false;
	if (count($wo) > 0)
		$foundWO = true;
	else
		$foundWO = false;
	
	if ($foundWO) {
		$info = $wo[0];
		$startDate = trim($info[0], "`");
		$startTime = trim($info[1], "`");
		$location = htmlentities(trim($info[2], "`") . ", " . trim($info[3], "`") . " " . trim($info[4], "`"));
		$localTime = trim($info[5], "`");
		$headline = htmlentities(trim($info[6], "`"));
		$bidAmount = "$" . trim($info[7], "`") . " per " . trim($info[8], "`");

		$companyID = trim($info[9], "`");
		$clientName = trim($info[10], "`");
		$projectName = trim($info[11], "`");
		$timeDiff = trim($info[12], "`");
		$to_email = trim($info[13], "`");

		// Group Call Code
		if ($_SESSION["GroupedCall"]) {
			$uniqueID = trim($info[14], "`");
			$_SESSION["GroupedCallCurrentUniqueID"] = $uniqueID;
		}
	}
	else {
		notifyMaintainer("Unable to find WO UNID '$uniqueID'.");
	}
	if ($cpa_result != "human") {
		// Log voice mail
//		$_SESSION["IVRCallResult"] = LOG_CALLRESULT_ANSWERED_MACHINE;
		$logID = $_SESSION["GroupedCall"] ? $_SESSION["GroupedCallUniqueIDToLogID"][$uniqueID] : $_SESSION["IVRCallID"];
		setIVRCallResult($logID, LOG_CALLRESULT_ANSWERED_MACHINE);
		logIVRContact("IVR Reminder", $companyID, $uniqueID, $_SESSION["calledNumber"], $_SESSION["outboundType"], "Result: " . LOG_CALLRESULT_ANSWERED_MACHINE);
 	}
	else {
//		$_SESSION["IVRCallResult"] = LOG_CALLRESULT_ANSWERED_SUCCESS;
		$logID = $_SESSION["GroupedCall"] ? $_SESSION["GroupedCallUniqueIDToLogID"][$uniqueID] : $_SESSION["IVRCallID"];
		setIVRCallResult($logID, LOG_CALLRESULT_ANSWERED_SUCCESS);
		logIVRContact("IVR Reminder", $companyID, $uniqueID, $_SESSION["calledNumber"], $_SESSION["outboundType"], "Result: " . LOG_CALLRESULT_ANSWERED_SUCCESS);
	}
	

?>
<vxml version="2.1">
	<meta name="maintainer" content="tngo@fieldsolutions.com"/>
        
    <var name="uniqueID" expr="'<?=$uniqueID?>'"/>
    
    <var name="sessionId" expr="'<?=session_id()?>'"/>

    <catch event="connection.disconnect.hangup">
		<exit namelist="sessionId"/>
	</catch>
    
<?php
if (!$foundWO):
?>
	<form>
    	<block>
     		Unable to retrieve win number: <?=$uniqueID?>. Field Solutions has been notified of this issue.
            <disconnect />
        </block>
    </form>
<?php
endif;
?>    

<form id="introBlock">
   	<field name="dummy">
        <grammar type="text/gsl">
			<![CDATA[[
			  [(do not match forty two)]
			]
			]]>
		</grammar>        	
       	<property name="timeout" value="100ms"/>

		<prompt>
<?php
if ($cpa_result == "human"):
?>        
<?php
else:
	if ($_SESSION["GroupedCall"]):
?>
		Hello, this is Field Solutions calling to remind you that you have $totalWO work orders that are incomplete.
        
        You must log in to the website, review the client comments and add the required information or it will not be approved for payment. Please log onto Field Solutions today.
        
        Thank you and goodbye.
<?
	else:
?>
			Hello, this is Field Solutions calling to remind you that the client would not approve your work order, win number: <break strength="x-weak"/>
<say-as interpret-as="number" format="digits"><?=$uniqueID?></say-as> <break strength="x-weak"/> <?=$headline?> <break strength="x-weak"/> <?=$location?> scheduled on <break strength="x-weak"/> <?=$startDate?> for payment. There is incomplete information on the work order that must be added and you will not be paid until it is completed and approved.   You must log in to the website, review the client comments and add the required information or it will not be approved for payment. Please log onto Field Solutions today.

	        Thank you and goodbye.
<?php
	endif;
endif;
?>
		</prompt>
        <nomatch count="30">
           <disconnect/>
        </nomatch>
        <filled>
        	<prompt></prompt>
        </filled>
        <nomatch>
        	<reprompt/>
        </nomatch>
		<noinput>
<?php
if ($cpa_result == "human"):
?>        
			<goto next="#choiceForm"/>
<?php
else:
?>
			<disconnect/>
<?php
endif;
?>
		</noinput>
	</field>
</form>

<?php
if ($cpa_result == "human"):
?>
	<form id="choiceForm">
    	<field name="dummy">
            <grammar type="text/gsl">
                <![CDATA[[
                  [(do not match forty two)]
                ]
                ]]>
            </grammar>        	
	       	<property name="timeout" value="50ms"/>
	    	<prompt>
<?php
	if ($_SESSION["GroupedCall"]) {
		if ($currentWO == 0) {
			$woIntro = "Hello, Field Solutions is calling to remind you that you have <break strength=\"x-weak\"/>$totalWO work orders that are incomplete. ";
			$woIntro .= "Work order number " . ($currentWO + 1) . " of $totalWO:";
		}
	}
	else
		$woIntro = "Hello, Field Solutions is calling to remind you that";
?>
				<?=$woIntro?> The client would not approve your work order, win number: <break strength="x-weak"/>
<say-as interpret-as="number" format="digits"><?=$uniqueID?></say-as> <break strength="x-weak"/> <?=$headline?> <break strength="x-weak"/> <?=$location?> scheduled on <break strength="x-weak"/> <?=$startDate?> for payment. There is incomplete information on the work order that must be added and you will not be paid until it is completed and approved.   You must log in to the website, review the client comments and add the required information or it will not be approved for payment. Please log onto Field Solutions today.

<?php
	if ($_SESSION["GroupedCall"] && $_SESSION["GroupedCallCurrent"] != $_SESSION["GroupedCallTotal"]):
?>
			Thank you. Proceeding to the next work order.
<?php
	else:
?>
			Thank you and Goodbye.
<?php
	endif;
?>
			</prompt>
            <nomatch count="10">
                <disconnect/>
            </nomatch>
            <noinput count="10">
                <disconnect/>
            </noinput>
            <filled>
            	<prompt></prompt>
            </filled>
            <noinput>
<?php
	if ($_SESSION["GroupedCall"] && $_SESSION["GroupedCallCurrent"] != $_SESSION["GroupedCallTotal"]):
?>
			<submit next="<?=$_SESSION["outboundScript"]?>?s=<?=session_id()?>" namelist="" method="post" />
<?php
	else:
?>
        	<disconnect/>
<?php
	endif;
?>
            </noinput>
        </field>
    </form>
<?php
endif;
?>
</vxml>
