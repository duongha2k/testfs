<?php
/* 
	Verify tech ID and PIN. Go to IVR main menu on match or otherwise repeats login. Bypass pin for outbound calls.
	Input: 
		techID - techID of logged in user
		outgoingType - type of outbound call
*/
	$page = "login";
//	ini_set("display_errors",1);
	require_once("ivrheader.php");
	// bypass caspio failsafe
	$_SESSION["loggedIn"] = "yes";
	require_once("../library/caspioAPI.php");
	require_once("library/ivrLib.php");
	$_SESSION["loggedIn"] = "no";

	writeIVRHeader();
	if (isset($_GET["TechID"]) && isset($_GET["Pin"]) && $_GET["Pin"] == "outbound") {
		$techID = $_GET["TechID"];
		$pin = $_GET["Pin"];
	}
	else {
		$techID = $_POST["TechID"];
		$pin = $_POST["Pin"];
	}
	$outboundType = $_GET["outboundType"];
	$outboundScript = "";
	$pinCriteria = ($pin == "outbound" ? "" : "AND PIN = '$pin'");
	$calledNumber = $_GET["calledNumber"];
	$logID = $_GET["IVRLogID"];
		
	// Set Grouped Call vars
	$groupedCall = !is_numeric($logID);
	$_SESSION["GroupedCall"] = $groupedCall;
	
	if ($techID != "" && $pin != "") {
		$info = caspioSelectAdv("TR_Master_List", TECH_SESSION_INFO, "TechID = '" . caspioEscape($techID) . "' AND AcceptTerms = 'Yes' $pinCriteria", "", false, "`", "|", false);

		$verified = ($info[0] != "" ? "true" : "false");
		$loginFailedReason = ($verified == "false" ? "BadLogin" : "");
		if ($verified == "true") {
			// set session info
			$info = explode("|", $info[0]);
			setTechSession($info);
			$_SESSION["calledNumber"] = $calledNumber;
			$_SESSION["callBegin"] = time();
			setIVRCallResult($logID, LOG_CALLRESULT_ANSWERED_NO_RESPONSE);
//			$_SESSION["IVRCallResult"] = "Answered: Disconnected with no response";
			if ($pin != "outbound") {
				// skip ICA checck for outgoing since if tech somehow got assigned work we want to remind them
				$Date_TermsAccepted = strtotime(trim($info[1], "`"));
				if ($Date_TermsAccepted == "" || $Date_TermsAccepted < strtotime('03/03/2008 10:00:00 PM')) {
					$verified = "false";
					$loginFailedReason = "MissingICA";
				}
			}
			else {
				$_SESSION["IVRLogID"] = $logID;
				$_SESSION["CPAResult"] = $_GET["CPAResult"];
				$_SESSION["uniqueID"] = $_GET["uniqueID"];
				
				if ($groupedCall) {
					$index = 0;
					$_SESSION["GroupedCallUniqueIDToLogID"] = array();
					$uniqueIDList = explode(",", $_SESSION["uniqueID"]);
					$logIDList = explode(",", $logID);
					foreach ($logIDList as $id) {
						$_SESSION["GroupedCallUniqueIDToLogID"][$uniqueIDList[$index]] = $id;
						$index++;
					}
				}
					
				if ($_SESSION["CPAResult"] != "human")
					setIVRCallResult($logID, LOG_CALLRESULT_ANSWERED_MACHINE);
					
				$customCallMoreThan24Hr = false;
				if ($outboundType == LOG_CALLTYPE_CUSTOM_HR_CALL_1 || $outboundType == LOG_CALLTYPE_CUSTOM_HR_CALL_2 || $outboundType == LOG_CALLTYPE_CUSTOM_HR_CALL_3) {
					// check if call is for 24+ hr
					$db = Zend_Registry::get('DB');
					$sql = "SELECT ReminderCustomHr, ReminderCustomHr_2, ReminderCustomHr_3 FROM work_orders_with_timezone_view JOIN projects AS p ON work_orders_with_timezone_view.Project_ID = p.Project_ID WHERE work_orders_with_timezone_view.WIN_NUM = ?";
					$wo = $db->fetchAll($sql, array($_SESSION["uniqueID"]), Zend_Db::FETCH_NUM);
					$wo = $wo[0];
					if ($outboundType == LOG_CALLTYPE_CUSTOM_HR_CALL_1 && $wo[0] > 24) $customCallMoreThan24Hr = true;
					if ($outboundType == LOG_CALLTYPE_CUSTOM_HR_CALL_2 && $wo[1] > 24) $customCallMoreThan24Hr = true;
					if ($outboundType == LOG_CALLTYPE_CUSTOM_HR_CALL_3 && $wo[2] > 24) $customCallMoreThan24Hr = true;
				}
				$_SESSION["CustomCallMoreThan24Hr"] = $customCallMoreThan24Hr;
				
				$_SESSION["outboundType"] = $outboundType;
				switch ($outboundType) {
					case LOG_CALLTYPE_ACCEPTANCE_CALL:
						$outboundScript = "reminderAcceptance.php";				
						break;
					case LOG_CALLTYPE_24_HR_CALL:
						$outboundScript = "reminder24hr.php";
						break;
					case LOG_CALLTYPE_SITE_CONTACT_CALL:
							$outboundScript = "reminderSiteContact.php";
						break;
					case LOG_CALLTYPE_1_HR_CALL:
					case LOG_CALLTYPE_CUSTOM_HR_CALL_1:
					case LOG_CALLTYPE_CUSTOM_HR_CALL_2:
					case LOG_CALLTYPE_CUSTOM_HR_CALL_3:
						if (!$customCallMoreThan24Hr)
							$outboundScript = "reminder1hr.php";
						else
							$outboundScript = "reminder24hr.php";
						break;
					case LOG_CALLTYPE_EMERGENCY_DISPATCH_BACK_OUT_CALL:
					case LOG_CALLTYPE_EMERGENCY_DISPATCH_NOT_ON_SITE_CALL:
						$outboundScript = "reminderEmergencyDispatch.php";
						break;
					case LOG_CALLTYPE_EMERGENCY_DISPATCH_SITE_CONTACT_BACK_OUT_CALL:
						$outboundScript = "reminderEmergencyDispatchSiteContact.php";
						break;
					case LOG_CALLTYPE_NOT_MARKED_COMPLETE_CALL:
						$outboundScript = "reminderNotMarkedComplete.php";
						break;
					case LOG_CALLTYPE_INCOMPLETE_CALL:
						$outboundScript = "reminderIncomplete.php";
						break;
				}
				$_SESSION["outboundScript"] = $outboundScript;
			}
		}
	}
	else {
		$verified = false;		
		if ($pin == "outbound")
			notifyMaintainer("Outbound call to tech failed to login.<br/><br/>TechID - $techID<br/>OutboundType - $outboundType");
	}
?>
<vxml version="2.1">
	<meta name="maintainer" content="tngo@fieldsolutions.com"/>
    <var name="verified" expr="<?=$verified?>"/>
    <var name="techID" expr="<?=$techID?>"/>
    <var name="loginFailedReason" expr="'<?=$loginFailedReason?>'"/>
    <var name="pin" expr="'<?=$pin?>'"/>
    <var name="outboundType" expr="'<?=$outboundType?>'"/>
	<property name="fetchtimeout" value="15s"/>
    
    <catch event="connection.disconnect.hangup">
		<submit next="handleDisconnect.php?s=<?=session_id()?>" namelist=""/>
		<exit/>
	</catch>
	<form>
    	<field name="dummy">
            <grammar type="text/gsl">
                <![CDATA[[
                  [(do not match forty two)]
                ]
                ]]>
            </grammar>        	
	       	<property name="timeout" value="50ms"/>
            
            <filled>
            	<prompt></prompt>
            </filled>
            <nomatch>
            	<reprompt/>
            </nomatch>
            <noinput>
                <if cond="pin == 'outbound'">
                    <if cond="verified == true">
                        <submit next="<?=$outboundScript?>?s=<?=session_id()?>" namelist="" method="post" />
                    <else/>
                        <prompt>
                            This is a reminder call from Field Solutions about pending work assigned to you. However I was unable to access your account via I V R at this time. Please visit www.fieldsolutions.com to update your status regarding upcomming work.
                        </prompt>
                        <disconnect/>
                    </if>
<!--                <else/>
                    <if cond="verified == true">
                        <prompt>You have logged in sucessfully.</prompt>
                        <goto next="IVRMainMenu.php"/>
                    <else/>
                        <if cond="loginFailedReason == 'MissingICA'">
                            <prompt>Please go to www.fieldsolutions.com to agree to the Independent Contractor&quot;s Contract before proceeding.</prompt>
                            <disconnect/>
                        <else/>
                            <prompt>I was unable to verify your information. Please try again.</prompt>
                        </if>
                        <goto next="techLogin.php"/>
                    </if>-->
				</if>
            </noinput>
        </field>
    </form>
</vxml>
