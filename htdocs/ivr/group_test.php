<?php
	$_SESSION["loggedIn"] = "yes";
	require_once("../library/caspioAPI.php");
	require_once("library/ivrLib.php");
	require_once("library/voxeoAPI.php");
//	ini_set("display_errors", 1);
	$uniqueID = $_GET["uniqueID"];
	if (isset($_POST["uniqueID"])) {
		$uniqueID = $_POST["uniqueID"];
		$outboundType = $_POST["outboundType"];
//		$phone = $_POST["phone"];
		$fieldList = "TB_UNID, Tech_ID, PrimaryPhone, SecondaryPhone, PrimPhoneType, SecondPhoneType, EmergencyPhone, Company_ID, StartDate, StartTime, ISNULL(EndDate, ''), ISNULL(EndTime,'')";
		runCall($outboundType, $callList);
	}
		
	function runCall($callType, $woList) {
		switch ($callType) {
			case LOG_CALLTYPE_1_HR_CALL:
				$woList = caspioSelectAdv("Work_Orders_With_Timezone", $fieldList, "TB_UNID IN ($uniqueID)", "Tech_ID, StartDate + StartTime + EndTime", true, "`", "|", false);
				$callListNew = array();
				foreach ($woList as $info) {
					if ($info == "")
						continue;
					$parts = explode("|", $info);
					$uniqueID = trim($parts[0], "`");
					$startDate = trim($parts[8], "`");
					$startTime = trim($parts[9], "`");
					$endDate = trim($parts[10], "`");
					$endTime = trim($parts[11], "`");
															
					$endDate = $endDate == "" || $endDate == "1/1/1900 12:00:00 AM" ? $startDate : $endDate;
					$endTime = $endTime == "" ? $startTime : $endTime;
					
					if ($endDate != $startDate) {
						echo "Ignoring UniqueID due to date range: $uniqueID of between $startDate and $endDate<br/>";
						$msg = "Date Range on WO with 1 HR Reminder: $uniqueID";
						smtpMail("IVR Application", "nobody@fieldsolutions.com", "tngo@fieldsolutions.com", "Unable to do 1 HR Reminder", $msg, nl2br($msg), "IVR Application");
					}
					else
						$callListNew[] = $info;
				}
				$woList = $callListNew;
				break;
			default:
				break;
		}

		$groupedCallsUniqueID = array();
		$groupedCallsCompanyID = array();
		$currentTech = -1;
		$numWO = sizeof($woList);
		$processed = 0;
		$currentStartDate = -1;
		$currentStartTime = -1;
		$currentEndDate = -1;
		$currentEndTime = -1;
		foreach ($woList as $info) {
			echo "Pending $callType: ";
			if ($info == "") {
				echo "<div style=\"margin: 0px 0px 0px 15px\">None</div><br/>";
				continue;
			}
			$parts = explode("|", $info);
			$uniqueID = trim($parts[0], "`");
			$techID = trim($parts[1], "`");
			$primaryPhone = trim($parts[2], "`");
			$secondaryPhone = trim($parts[3], "`");
			$primPhoneType = trim($parts[4], "`");
			$secondPhoneType = trim($parts[5], "`");
			$emergencyPhone = trim($parts[6], "`");
			$companyID = trim($parts[7], "`");
			$startDate = trim($parts[8], "`");
			$startTime = trim($parts[9], "`");
			$endDate = trim($parts[10], "`");
			$endTime = trim($parts[11], "`");
			
			$endDate = $endDate == "" || $endDate == "1/1/1900 12:00:00 AM" ? $startDate : $endDate;
			$endTime = $endTime == "" ? $startTime : $endTime;
			
			echo "<div style=\"margin: 0px 0px 0px 15px\">WOUNID: $uniqueID</div>";	
			echo "<div style=\"margin: 0px 0px 0px 15px\">Company: $companyID</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">TechID: $techID</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Primary Phone: $primaryPhone</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Secondary Phone: $secondaryPhone</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Primary Type Phone: $primPhoneType</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Secondary Type Phone: $secondPhoneType</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Emergency Phone: $emergencyPhone</div>";									
			echo "<div style=\"margin: 0px 0px 0px 15px\">Start Date: $startDate</div>";									
			echo "<div style=\"margin: 0px 0px 0px 15px\">Start Time: $startTime</div>";									
			echo "<div style=\"margin: 0px 0px 0px 15px\">End Date: $endDate</div>";									
			echo "<div style=\"margin: 0px 0px 0px 15px\">End Time: $endTime</div>";									
				
			if ($callType != LOG_CALLTYPE_EMERGENCY_DISPATCH_BACK_OUT_CALL && $callType != LOG_CALLTYPE_EMERGENCY_DISPATCH_NOT_ON_SITE_CALL) {
				$phone = ($callType == LOG_CALLTYPE_1_HR_CALL && $primPhoneType != "Cell" && $secondaryPhone != "" && $secondPhoneType == "Cell" ? $secondaryPhone : $primaryPhone);
			}
			else {
				$phone = $emergencyPhone;
			}
						
			$phone = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
			
			$phone = str_replace(array("-", ".", " ", "(", ")"), array("", "", "", "", ""), $phone);
			
			echo "<br/><div style=\"margin: 0px 0px 0px 15px\">Using number: $phone</div><br/>";
			@ob_flush();
			@flush();
			
			// Testing purposes
//			if ($phone != "5122728328" || $phone != "8326878350")
//				$phone = "5122728328";
//				$phone = "8326878350";
//			if ($companyID != "CBD" && $companyID != "suss" && $companyID != "BW" && $companyID != "FS")
//				continue;
			
			$phone = "+1" . $phone;
			echo "<div style=\"margin: 0px 0px 0px 15px\">Dialing $phone</div>";
			
			$processed++;
			
			if ($callType != LOG_CALLTYPE_EMERGENCY_DISPATCH_BACK_OUT_CALL && $callType != LOG_CALLTYPE_EMERGENCY_DISPATCH_NOT_ON_SITE_CALL) {
				if ($currentTech == -1 || ($callType != LOG_CALLTYPE_1_HR_CALL && $currentTech == $techID) || ($callType == LOG_CALLTYPE_1_HR_CALL && ($currentStartDate == $startDate && $currentStartTime == $startTime && $currentEndDate == $endDate && $currentEndTime == $endTime && $currentTech == $techID)) ) {
					$groupedCallsUniqueID[] = $uniqueID;
					$groupedCallsCompanyID[] = $companyID;
					$currentTech = $techID;
					if ($callType == LOG_CALLTYPE_1_HR_CALL) {
						$currentStartDate = $startDate;
						$currentStartTime = $startTime;
						$currentEndDate = $endDate;
						$currentEndTime = $endTime;
					}
					$currentPhone = $phone;
				}
				
				if ($processed == $numWO || ($callType != LOG_CALLTYPE_1_HR_CALL && $currentTech != $techID) || ($callType == LOG_CALLTYPE_1_HR_CALL && ($currentStartDate != $startDate || $currentStartTime != $startTime || $currentEndDate != $endDate || $currentEndTime != $endTime || $currentTech != $techID)) ) {
					$groupedCalls = implode(",", $groupedCallsUniqueID);
					$groupedCompanyID = implode(",", $groupedCallsCompanyID);
					echo "<br/>**** CALL: TechID:$currentTech Phone:$currentPhone WOs:$groupedCalls**** <br/>";
					reminderCallBG(implode(",", $groupedCallsUniqueID), implode(",", $groupedCallsCompanyID), $currentPhone, $callType, $currentTech);
					if ($processed == $numWO && (($callType != LOG_CALLTYPE_1_HR_CALL && $currentTech != $techID) || ($callType == LOG_CALLTYPE_1_HR_CALL && ($currentStartDate != $startDate || $currentStartTime != $startTime || $currentEndDate != $endDate || $currentEndTime != $endTime || $currentTech != $techID))) ) {
						echo "<br/>**** CALL: TechID:$techID Phone:$phone WOs:$uniqueID **** <br/>";
						reminderCallBG($uniqueID, $companyID, $phone, $callType, $techID);
					}
					else {
						$groupedCallsUniqueID = array($uniqueID);
						$groupedCallsCompanyID = array($companyID);
						$currentTech = $techID;
						if ($callType == LOG_CALLTYPE_1_HR_CALL) {
							$currentStartDate = $startDate;
							$currentStartTime = $startTime;
							$endStartDate = $endDate;
							$endStartTime = $endTime;
						}
						$currentPhone = $phone;
					}
				}
			}
			else {
				reminderCallBG($uniqueID, $companyID, $phone, $callType, $techID);
			}						
		}
		echo "<hr/>";
	}
	
	function reminderCallBG($uniqueID, $companyID, $phone, $callType, $techID) {
		$isGrouped = !is_numeric($uniqueID);
		$uniqueIDList = NULL;
		$companyIDList = NULL;
		$parentCall = 0;
		if ($isGrouped) {
			$uniqueIDList = explode(",", $uniqueID);
			$companyIDList = explode(",", $companyID);
			$logIDList = array();
			$gotParent = FALSE;
			foreach ($uniqueIDList as $uid) {
				$newID = logIVRCall($uid, $techID, $callType, "Pending", 0, $parentCall);
				if (!$gotParent) {
					$parentCall = $newID;
					$gotParent = TRUE;
				}
				$logIDList[] = $newID;
			}
			$logID = implode(",", $logIDList);
		}
		else
			$logID = logIVRCall($uniqueID, $techID, $callType, "Pending", 0);
//		if ($phone != "5122728328" || $phone != "8326878350")
//			$phone = "5122728328";
//		if ($companyID != "CBD" && $companyID != "suss" && $companyID != "BW" && $companyID != "FS")
//			return;

//		$phone = "5122728328";
		
		if (strpos($phone, "+1") === FALSE)
			$phone = "+1" . $phone;

		$callResult = outboundCall(TOKEN_OUTBOUND_REMINDER, $phone, array("TechID" => $techID, "Pin" => "outbound", "outboundType" => $callType, "uniqueID" => $uniqueID, "IVRLogID" => $logID, "calledNumber" => $phone, "voxeo-cpa-maxtime" => "3200", "voxeo-cpa-maxsilence" => "1200", "voxeo-cpa-runtime" => "20000", "calltimeout" => "120") );

		$msg = "Phone: $phone
		TechID: $techID
		CallResult: $callResult
		";
			
//		smtpMail("IVR Application", "nobody@fieldsolutions.com", "tngo@fieldsolutions.com", "Reminder Call Type Grouped - $callType (ID: $logID)", $msg, nl2br($msg), "IVR Application");
		
		$index = 0;
		if ($isGrouped) {
			foreach ($uniqueIDList as $unid) {
				processCallResult($unid, $companyIDList[$index], $phone, $callResult, $logIDList[$index], $callType);
				$index++;
			}
		}
		else
			processCallResult($uniqueID, $companyID, $phone, $callResult, $logID, $callType);
	}		
?>
<form action="<?=$_SERVER["PHP_SELF"]?>" method="post"/>
	WIN#: <input type="text" name="uniqueID" size="50" value="<?=$uniqueID?>"/><br/>(ex. 7103,49983,49993,49985,50018,50028)<br/>
<!--	Phone Number (leave blank for Tech's): <input type="text" name="phone" size="12" value="<?=$phone?>"/><br/>-->
    Call Type:
    <select name="outboundType">
    	<option value="<?=LOG_CALLTYPE_ACCEPTANCE_CALL?>">Acceptance</option>
    	<option value="<?=LOG_CALLTYPE_24_HR_CALL?>">24 Hrs</option>
    	<option value="<?=LOG_CALLTYPE_1_HR_CALL?>">1 Hr</option>
    	<option value="<?=LOG_CALLTYPE_NOT_MARKED_COMPLETE_CALL?>">Not Marked Complete</option>
    	<option value="<?=LOG_CALLTYPE_INCOMPLETE_CALL?>">Incomplete</option>
    </select>
    <br/>
    <input type="submit" value="Call"/>
</form>