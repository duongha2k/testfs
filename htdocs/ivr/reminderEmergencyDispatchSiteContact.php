<?php
//	ini_set("display_errors", 1);
	require_once("ivrheader.php");
	require_once("../library/caspioAPI.php");
	require_once("library/ivrLib.php");
	require_once("../library/woContactLog_ivr.php");
	writeIVRHeader();
	$uniqueID = $_SESSION["uniqueID"];
	$cpa_result = $_SESSION["CPAResult"];
	
//	$_SESSION["TechID"] = 19806;
//	$uniqueID = 7103;
	
	if ($_SESSION["TechID"] == "") die();
	
//	$wo = caspioSelectAdv("Work_Orders_With_Timezone", "ISNULL(CONVERT(VARCHAR, StartDate, 101), ''), ISNULL(StartTime, ''), ISNULL(City, ''), ISNULL(State, ''), ISNULL(Zipcode, ''), " . TIME_CURRENT_TIME_LOCAL . ", ISNULL(Headline, ''), ISNULL(Tech_Bid_Amount, ''), ISNULL(Amount_Per, 'Site'), Company_ID, Company_Name, Project_Name, " . TIME_STARTTIME_HOURS_BEFORE_START . ", ProjectManagerEmail, NoShow_Tech, Tech_ID", "TB_UNID = '$uniqueID' AND Tech_ID = '" . caspioEscape($_SESSION["TechID"]) . "'", "", true, "`", "|", false);

	$db = Zend_Registry::get('DB');
	$sql = "SELECT IFNULL(DATE_FORMAT(StartDate, '%m/%d/%Y'), ''), IFNULL(StartTime, ''), IFNULL(City, ''), IFNULL(State, ''), IFNULL(Zipcode, ''), " . TIME_CURRENT_TIME_LOCAL . ", IFNULL(Headline, ''), IFNULL(Tech_Bid_Amount, ''), IFNULL(Amount_Per, 'Site'), Company_ID, Company_Name, Project_Name, " . TIME_STARTTIME_HOURS_BEFORE_START . ", ProjectManagerEmail, NoShow_Tech, Tech_ID, ivr_status.Success FROM work_orders_with_timezone_view JOIN ivr_status ON (work_orders_with_timezone_view.WIN_NUM = ivr_status.WIN_NUM AND CallType = '" . LOG_CALLTYPE_SITE_CONTACT_CALL . "') WHERE work_orders_with_timezone_view.WIN_NUM = ? AND work_orders_with_timezone_view.Tech_ID = ?";
	$wo = $db->fetchAll($sql, array($uniqueID, $_SESSION["TechID"]), Zend_Db::FETCH_NUM);

	$foundWO = false;
	$backedOut = false;
	if (count($wo) > 0)
		$foundWO = true;
	else
		$foundWO = false;
	
	if ($foundWO) {
		$info = $wo[0];
		$startDate = trim($info[0], "`");
		$startTime = trim($info[1], "`");
		$zipcode = $info[4];
		$dayofweek = date("l", strtotime($startDate));
		$location = htmlentities(trim($info[2], "`") . ", " . trim($info[3], "`") . " " . trim($info[4], "`"));
		$localTime = trim($info[5], "`");
		$headline = trim($info[6], "`");
		$bidAmount = "$" . trim($info[7], "`") . " per " . trim($info[8], "`");

		$companyID = trim($info[9], "`");
		$clientName = trim($info[10], "`");
		$projectName = trim($info[11], "`");
		$timeDiff = trim($info[12], "`");
		$to_email = trim($info[13], "`");

		$noshow_tech = trim($info[14], "`");
		$assignedTech = trim($info[15], "`");
		$success = $info[16];
		
		$backedOut = $success == 1;
		$reason = $backedOut ? "The Site Contact has backed out of the work order." : "We have been unable to confirm Site Contact availability at the site.";
	}
	else {
		notifyMaintainer("Unable to find WO UNID '$uniqueID'.");
	}
	if ($cpa_result != "human") {
		$_SESSION["IVRCallResult"] = LOG_CALLRESULT_ANSWERED_MACHINE;
		logIVRContact("IVR Reminder", $companyID, $uniqueID, $_SESSION["calledNumber"], $_SESSION["outboundType"], "Result: " . LOG_CALLRESULT_ANSWERED_MACHINE);
	}
	else {
		$_SESSION["IVRCallResult"] = LOG_CALLRESULT_ANSWERED_SUCCESS;
		logIVRContact("IVR Reminder", $companyID, $uniqueID, $_SESSION["calledNumber"], $_SESSION["outboundType"], "Result: " . LOG_CALLRESULT_ANSWERED_SUCCESS);
	}
	
?>
<vxml version="2.1">
	<meta name="maintainer" content="tngo@fieldsolutions.com"/>
        
    <var name="uniqueID" expr="'<?=$uniqueID?>'"/>

	<var name="sessionId" expr="'<?=session_id()?>'"/>
    
    <catch event="connection.disconnect.hangup">
		<exit namelist="sessionId"/>
	</catch>
    
<?php
if (!$foundWO):
?>
	<form>
    	<block>
     		Unable to retrieve win number: <?=$uniqueID?>. Field Solutions has been notified of this issue.
            <disconnect />
        </block>
    </form>
<?php
endif;
?>    

    <form id="introBlock">
        <field name="dummy">
            <grammar type="text/gsl">
                <![CDATA[[
                  [(do not match forty two)]
                ]
                ]]>
            </grammar>        	
            <property name="timeout" value="50ms"/>
    
            <prompt>            
				Hello. This is FieldSolutions regarding WIN number <prosody rate="slow"><say-as interpret-as="number" format="digits"><?=$uniqueID?></say-as></prosody> in postal code <prosody rate="slow"><?=$zipcode?></prosody> scheduled to begin <?=$dayofweek?>, <?=$startDate?> at <?=$startTime?>.
                <?=$reason?>
				FieldSolutions requires confirmation that there will be someone available to provide the technician access to the site. Again, this is FieldSolutions calling regarding WIN# <prosody rate="slow"><say-as interpret-as="number" format="digits"><?=$uniqueID?></say-as></prosody> in postal code <prosody rate="slow"><?=$zipcode?></prosody>. <?=$reason?> Thank you. Goodbye.
            </prompt>
            <filled>
                <prompt></prompt>
            </filled>
            <nomatch>
                <reprompt/>
            </nomatch>
            <noinput>
                <disconnect/>
            </noinput>
            </field>
    </form>
</vxml>
