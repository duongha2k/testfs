<?php
/* 
	Prompts for tech login
*/
//	ini_set("display_errors", 1);
	$page = "login";
	require_once("ivrheader.php");
	writeIVRHeader();
?>
<vxml version="2.1">
	<meta name="maintainer" content="tngo@fieldsolutions.com"/>
	<var name="callerID" expr="session.callerid"/>
    <form>
<!--    		<block>
            	<var name="TechID" expr="19806"/>
            	<var name="Pin" expr="1144"/>
	            <submit next="verifyTech.php?s=<?=session_id()?>" namelist="TechID Pin" method="post"/>
			</block>-->
    	<block>
            <prompt>Welcome to the Field Solutions IVR System.</prompt>
			<goto next="#authentication"/>
        </block>
    </form>
    <form id="authentication">
    	<field name="TechID" type="digits?minlength=4">
        	<prompt bargeintype="hotword">
		    	... ... To login, please say or enter your Technician ID.
            </prompt>
            <noinput>
            	<prompt>
                	I did not hear a response. Please try again.
                </prompt>
				<clear namelist="TechID"/>
            	<reprompt/>
            </noinput>
            <nomatch>
            	<prompt>
                	I did not understand. Please try again.
                </prompt>
				<clear namelist="TechID"/>
            	<reprompt/>
            </nomatch>
            <filled namelist="TechID">
            	<goto nextitem="RepeatTechID"/>
            </filled>
        </field>
        
	    <field name="RepeatTechID" type="boolean">
	    	<prompt bargeintype="hotword">
            	Your technician I D is <say-as interpret-as="number" format="digits"><value expr="TechID$.utterance"/></say-as>. If this is correct say yes or press 1. If this is not correct say no or press 2.
	        </prompt>
            <noinput>
            	<prompt>
                	I did not hear a response. Please try again.
                </prompt>
            	<reprompt/>
            </noinput>
            <nomatch>
            	<prompt>
                	I did not understand. Please try again.
                </prompt>
            	<reprompt/>
            </nomatch>
            <filled namelist="RepeatTechID">
            	<if cond="RepeatTechID == false">
					<clear namelist="TechID RepeatTechID"/>
                	<goto nextitem="TechID"/>
				<elseif cond="RepeatTechID == true"/>
                	<goto nextitem="Pin"/>
				</if>
            </filled>
	    </field>        
        
    	<field name="Pin" type="digits?minlength=4">
        	<prompt bargeintype="hotword">
		    	Please say or enter your pin number.
            </prompt>
            <noinput>
            	<prompt>
                	I did not hear a response. Please try again.
                </prompt>
				<clear namelist="Pin"/>                
            	<reprompt/>
            </noinput>
            <nomatch>
            	<prompt>
                	I did not understand. Please try again.
                </prompt>
				<clear namelist="Pin"/>                
            	<reprompt/>
            </nomatch>
        </field>

	    <field name="RepeatPin" type="boolean">
	    	<prompt bargeintype="hotword">
            	Your PIN is <say-as interpret-as="number" format="digits"><value expr="Pin$.utterance"/></say-as>. If this is correct say yes or press 1. If this is not correct say no or press 2.
	        </prompt>
            <noinput>
            	<prompt>
                	I did not hear a response. Please try again.
                </prompt>
            	<reprompt/>
            </noinput>
            <nomatch>
            	<prompt>
                	I did not understand. Please try again.
                </prompt>
            	<reprompt/>
            </nomatch>
            <filled namelist="RepeatPin TechID Pin">
            	<if cond="RepeatPin == false">
					<clear namelist="RepeatPin Pin"/>
                	<goto nextitem="Pin"/>
				<elseif cond="RepeatPin == true"/>
	                <submit next="verifyTech.php?s=<?=session_id()?>" namelist="TechID Pin" method="post"/>
				</if>
            </filled>
	    </field>
    </form>
</vxml>
