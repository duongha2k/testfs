<?php
	require_once("library/caspioAPI.php");
	require_once("library/smtpMail.php");

	ini_set("display_errors", 1);

	// get assigned work orders from previous import
	$id = caspioSelectAdv("FLS_Work_Orders", "FLSID = FLSTechID, TechID = TB_ID, FName = TechFName, LName = TechLName, TEmail = TechEmail, TPhone = TechPhone, SName = SiteName, WB = Waybill, SiteAdd = Address1, SiteCity = City, SiteState = State, SiteZip = Zip, FComments = '', WNO = WorkOrderNo, PNO = PartNos, ProjNum = '', ProjName = '', ProjCoord = '', ProjFromEmail = '', ProjCoordPhone = '', TechSecPhone = '', TechAdd1 = '', TechAdd2 = '', TechCity = '', TechState = '', TechZip = '', TechPrimPhoneType = '', TechSecPhoneType = ''", "WorkOrderID = 0 UNION SELECT FWO.FLSTechID, TL.TechID, TL.FirstName, TL.LastName, ISNULL(TL.PrimaryEmail, ''), ISNULL(TL.PrimaryPhone, ''), ISNULL(FWO.SiteName, ''), ISNULL(FWO.Waybill, ''), FWO.Address1, FWO.City, FWO.State, FWO.Zip, CAST(FWO.FLSComments AS varchar(4000)), FWO.WorkOrderNo, ISNULL(FWO.PartNos, ''), Proj.ProjectNo, ISNULL(Proj.ProjectName, ''), ISNULL(Proj.Coordinator, ''), ISNULL(Proj.From_Email, ''), ISNULL(Proj.Coordinator_Phone, ''), ISNULL(TL.SecondaryPhone, ''), TL.Address1, TL.Address2, TL.City, TL.State, TL.ZipCode, ISNULL(TL.PrimPhoneType, ''), ISNULL(TL.SecondPhoneType, '') FROM FLS_Work_Orders AS FWO LEFT JOIN TR_Master_List AS TL ON FWO.FLSTechID = TL.FLSID LEFT JOIN FLS_Projects AS Proj ON Proj.ProjectNo = FWO.Project_ID WHERE ISNULL(FWO.DateEntered, '') = '' AND ISNULL(FWO.DateImported, '') != '' AND ISNULL(FWO.FLSTechID, '') <> '' AND ISNULL(FWO.Deactivated, '') <> 'Yes' AND FWO.Kickback = '0'", "", false, "`", "|", false);
	
//	print_r($id);
		
	$techList = array();
	
	if (sizeof($id) > 0 && $id[0] != "") {
		
		echo "Number of WOs assigned:" . sizeof($id) . "<br/>";
		foreach ($id as $row) {
			// pull in tech info and prepare email
			$row = explode("|", $row);
			$flsID = trim($row[0], "`");
			$techID = $row[1];
			$techFName = trim($row[2], "`");
			$techLName = trim($row[3], "`");
			$techEmail = trim($row[4], "`");
			$techPhone = trim($row[5], "`");
			
			$siteName = trim($row[6], "`");
			$Waybill = trim($row[7], "`");
			$Address1 = trim($row[8], "`");
			$City = trim($row[9], "`");
			$State = trim($row[10], "`");
			$Zip = trim($row[11], "`");
			$FLSComments = trim($row[12], "`");
			$WorkOrderNo = trim($row[13], "`");
			$partNo = trim($row[14], "`");
	
			$projectNo = trim($row[15], "`");	
			$projectName = trim($row[16], "`");
			$vFromName = trim($row[17], "`");
			$vFromEmail = trim($row[18], "`");
			$projectCCphone = trim($row[19], "`");
			
			$secondaryPhone = trim($row[20], "`");
			$ContactAddress1 = trim($row[21], "`");
			$ContactAddress2 = trim($row[22], "`");
			$ContactCity = trim($row[23], "`");
			$ContactState = trim($row[24], "`");
			$ContactZipCode = trim($row[25], "`");
			$PrimPhoneType = trim($row[26], "`");
			$SecondPhoneType = trim($row[27], "`");
			// if project from email is blank
			if ($vFromName == "") {
				$vFromName = "Call Coordinators";
			}
			if ($vFromEmail == "") {
				$vFromEmail = "callcoordinators@ftxs.fujitsu.com";
			}
			
			// tech info
			if (!array_key_exists($techID, $techList)) 
				$techList[$techID] = array("flsID" => $flsID, "techFName" => $techFName, "techLName" => $techLName, "techEmail" => $techEmail, "techPhone" => $techPhone, "secondaryPhone" => $secondaryPhone, "ContactAddress1" => $ContactAddress1, "ContactAddress2" => $ContactAddress2, "ContactCity" => $ContactCity, "ContactState" => $ContactState, "ContactZipCode" => $ContactZipCode, "PrimPhoneType" => $PrimPhoneType, "SecondPhoneType" => $SecondPhoneType);
				
			// assigned work order info
			if (!array_key_exists("woList", $techList[$techID]))
				$techList[$techID]["woList"] = array();
			
			$techList[$techID]["woList"][] = array("siteName" => $siteName, "wayBill" => $Waybill, "Address1" => $Address1, "City" => $City, "State" => $State, "Zip" => $Zip, "FLSComments" => $FLSComments, "WorkOrderNo" => $WorkOrderNo, "partNo" => $partNo, "projectNo" => $projectNo, "projectName" => $projectName, "vFromName" => $vFromName, "vFromEmail" => $vFromEmail, "projectCCphone" => $projectCCphone);
	
		}
		
	//	print_r($techList);
		
		echo "Number of Distinct Techs: " . sizeof($techList) . "<br/>";
		
		foreach ($techList as $techID => $info) {
			// pull in additional tech info
			echo "Pulling in Tech Info to Work Orders... <br/>";
			print_r(caspioUpdate("FLS_Work_Orders", "TB_ID, TechFName, TechLName, TechEmail, TechPhone", "'$techID','{$info['techFName']}','{$info['techLName']}','{$info['techEmail']}','{$info['techPhone']}'", "FLSTechID = '{$info['flsID']}' AND ISNULL(DateEntered, '') = ''", false));
			echo "UPDATE FLS_Work_Orders SET TB_ID = '$techID', TechFName = '{$info['techFName']}', TechLName = '{$info['techLName']}', TechEmail = '{$info['techEmail']}', TechPhone = '{$info['techPhone']}' WHERE FLSTechID = '{$info['flsID']}' AND ISNULL(DateEntered, '') = ''<br/>";
	
			// send emails to techs assigned
			$assignedWO = "";
			foreach ($info["woList"] as $wo) {
				// combine work orders assigned to single tech
				$assignedWO .= "--- {$wo['WorkOrderNo']} ---
				<a href='https://www.fieldsolutions.com/techs/wosFLSWorkOrderReviewed.php?woID={$wo['WorkOrderNo']}&techID=$techID'>I have reviewed all Work Order Details and documentation.</a>
				
			Work Order: {$wo['WorkOrderNo']}
			Site Name: {$wo['siteName']}
			Address: {$wo['Address1']}
	
			City: {$wo['City']}
			State: {$wo['State']}
			Zip: {$wo['Zip']}
				
			Part No: {$wo['partNo']}
			Waybill: {$wo['wayBill']}
				
			Comments: 
			---
			{$wo['FLSComments']}
			---
				
			Fujitsu Coordinator: {$wo['vFromName']}
			Email: {$wo['vFromEmail']}
			Phone: {$wo['projectCCphone']}
			
			";
	
			}
			
			$message = "
			Dear FS Technician,
				
			Congratulations! You have been awarded work from Fujitsu.
				
			You must click on the link below to review the work order information plus any related documentation. You need to confirm to Fujitsu that you are ready to perform the work listed in the work order by checking the box.
				
			You will receive another email 48 hours before your install. You will be required to revisit the web site to confirm that  you are ready to go for the install.
				
			Failing to respond to this email and the 48-hour email per instructions, will result in Fujitsu activating another Tech for the job.
				
			Your Contact Information:
				
			{$info['ContactAddress1']} {$info['ContactAddress2']}
			{$info['ContactCity']}, {$info['ContactState']}  {$info['ContactZipCode']}
				
			Primary Phone: {$info['techPhone']}  {$info['PrimPhoneType']}
			Secondary Phone: {$info['secondaryPhone']}  {$info['SecondPhoneType']}
			Email: {$info['techEmail']}
				
			<a href='https://www.fieldsolutions.com/techs/profile.php'>Update Contact Information</a>
			
			Your assigned work order(s):
			
			$assignedWO
						
			Thank you.
			Regards,
				
			Field Solutions, LLC
			Web: <a href='http://www.fieldsolutions.com'>www.fieldsolutions.com</a>";
	
			$vSubject = "Project(s) Assigned";
			
			$htmlmessage = nl2br($message);
			
			// Emails Tech
			smtpMailLogReceived($wo['vFromName'], $wo['vFromEmail'], $info['techEmail'], $vSubject, $message, $htmlmessage, "flsAfterImport.php");
			echo "*******************************************<br/>";
			echo "Emailing... FromName: {$wo['vFromName']} FromEmail: {$wo['vFromEmail']} TechEmail: {$info['techEmail']} Subject: $vSubject <br/><br/>" . $htmlmessage . "<br/><br/>";
		}
		
	}
	
	// calculate short notice and update
	echo "Calculating short notice ... <br/>";
	$startDate = "DateImported";
	$endDate = "PjctStartDate";
		
	$bDaysStartWeek = "CASE DATEPART(dw, $startDate) WHEN 7 THEN 0 ELSE 7 - DATEPART(dw, $startDate) - 1 END";
		
	$bDaysWholeWeeks = "((DATEDIFF(week, CAST($startDate AS DATETIME), $endDate) - 1) * 5)";
	
	$bDaysEndWeek = "CASE DATEPART(dw, $endDate) WHEN 7 THEN 5 ELSE DATEPART(dw, $endDate) - 1 END";
		
	$daysBefore = "$bDaysStartWeek + $bDaysWholeWeeks + $bDaysEndWeek";
		
	$isShort = caspioSelectAdv("FLS_Work_Orders", "WorkOrderID", "$daysBefore <= 5 AND ISNULL(DateEntered, '') = ''", "$endDate DESC", false, "`", "|", false);
	
	$total = 0;
		
	if (sizeof($isShort) > 1 || $isShort[0] != '') {
		$list = implode(",", $isShort);
		print_r(caspioUpdate("FLS_Work_Orders", "ShortNotice", "'1'", "WorkOrderID IN ($list) ", false));
		echo "UPDATE FLS_Work_Orders SET ShortNotice = '1' WHERE WorkOrderID IN ($list)<br/>";
		$total += sizeof($isShort);
	}
		
	echo "<br/>";

	$isNotShort = caspioSelectAdv("FLS_Work_Orders", "WorkOrderID", "$daysBefore > 5 AND ISNULL(DateEntered, '') = ''", "$endDate DESC", false, "`", "|", false);

	if (sizeof($isNotShort) > 1 || $isNotShort[0] != '') {
		$list = implode(",", $isNotShort);
		print_r(caspioUpdate("FLS_Work_Orders", "ShortNotice", "'0'", "WorkOrderID IN ($list)", false));
		echo "UPDATE FLS_Work_Orders SET ShortNotice = '0' WHERE WorkOrderID IN ($list)<br/>";
		$total += sizeof($isNotShort);
	}
	
	echo "<br/>";

	// copy dateImport to DateEntered
	print_r(caspioUpdate("FLS_Work_Orders", "DateEntered", "CAST(DateImported AS DATETIME)", "ISNULL(DateEntered, '') = '' AND ISNULL(DateImported, '') <> ''", false));
	echo "Setting DateEntered ... <br/>";
	echo "UPDATE FLS_Work_Orders SET DateEntered = CAST(DateImported AS DATETIME) WHERE ISNULL(DateEntered, '') = '' AND ISNULL(DateImported, '') <> ''<br/>";
	
	if ($argc)
		smtpMailLogReceived("flsAfterImport Script", "nobody@fieldsolutions.com", "tngo@fieldsolutions.com", "FLS After Import Ran", "Total Imported: $total", "Total Imported: $total", "flsAfterImport.php");
	
	die();
	
?>