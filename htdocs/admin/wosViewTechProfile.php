<?php $page = 'admin'; ?>
<?php $option = 'techs'; ?>
<?php $selected = 'find'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php

require_once("../library/caspioAPI.php");


checkAdminLogin2();


$httpReferer = (isset($_GET['referer']) ? urldecode($_GET['referer']) : $_SERVER["HTTP_REFERER"]);
$saveReferer = $_SESSION["saveReferer"];
if ($_SESSION["saveReferer"] != $httpReferer)
    $_SESSION["saveReferer"] = $httpReferer;

require_once("../headerStartSession.php");
require_once dirname(__FILE__) . '/../../includes/modules/common.init.php';

?>
<script type="text/javascript" src="/widgets/js/jquery.validate.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.curvycorners.js"></script>
<link rel="stylesheet" type="text/css" href="/widgets/js/star-rating/jquery.rating.css" />

<script src="/library/jquery/calendar/jquery-calendar.js"></script>
<style type="text/css">@import url(../library/ui.datepicker.css);</style>
<script type="text/javascript" src="../library/ui.datepicker.js"></script>
<script type="text/javascript" src="/library/jquery/jquery.bt.min.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.easing.pack.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-mod.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.jqEasyCharCounter.js"></script>
<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetDashboard.js"></script>
<script type="text/javascript" src="/widgets/js/FSTechProfile.js"></script>
<script src="/widgets/js/base64.js"></script>
<script src="/widgets/js/FSPopupAbstract.js"></script>
<script src="/widgets/js/FSPopupRoll.js"></script>
<link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" media="all" />
<script type="text/javascript" src="../techs/js/copier_skills_assessment.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.tinysort.js"></script>
<script type="text/javascript" src="/widgets/js/GXEMCCert.js"></script> 
<script type="text/javascript" src="../techs/js/GGETechEvaluation.js"></script> 
<script type="text/javascript" src="../techs/js/BlackBox.js"></script> 

<script type="text/javascript">
    var techRegValidator = "";
    var urlOld = "";
    var isSave = false;
    var _GGETechEvaluation = null;
    $(document).ready(function(){
        $.datepicker.setDefaults({
            dateFormat: 'yy-mm-dd'
        });

        //	showLoader();
        profile = new FSTechProfile({container: 'wr', tab: 'adminprofile', techId: '<?= $_GET['TechID']; ?>'});
        profile.show({tab: 'adminprofile', params: {container: 'wr'}});
		
        var techID = '<?= $_SESSION['TechID']; ?>';
        _GGETechEvaluation = GGETechEvaluation.CreateObject({
                   id:'_GGETechEvaluation' 
                });        
        
        _blackbox = BlackBox.CreateObject({id:'_blackbox',Techid:techID});

    });//end document.ready

    function TechFORCEContractorAgreement_click(techID){
        _GGETechEvaluation.TechFORCEContractorAgreement_click(techID);
    }

    function submitBusinessProfile(form){
        if($("#deleteResume").attr("checked") == true){
            deleteResume();
        }
		
        if($("#resumeUpload").val() != ""){

            $.ajaxFileUpload({
                url             : "/widgets/dashboard/do/save-business-profile/?techID="+$("#BizProfTechID").val(),
                secureuri       : false,
                fileElementId   : 'resumeUpload',
                dataType        : 'json',
                cache           : false,
                success         : function (data, status, xhr) {
                    var submitData = $(form).serialize();

                    $("#yesResume").show();
                    //$("#noResume").hide();
                    $("#resumeFile").html("<a href='/widgets/dashboard/do/file-tech-documents-download/filename/"+data.uploadResult.encodedFilename+"/"+data.uploadResult.fileName+">"+data.uploadResult.displayName+"</a>");
                    $("#resumeUpload").val("");
                    $.ajax({
                        type: "POST",
                        url: "/widgets/dashboard/do/save-business-profile/",
                        dataType    : 'json',
                        cache       : false,
                        data: submitData,
                        success: function (data) {
						
                            var messageHtml = "<h2>Update Complete</h2>";
                            messageHtml += "<p>Your Business Profile has been successfully updated.</p>";
                            messageHtml += "<b>Thank you!</b>";
								
                            hideLoader();
								
                            $("<div></div>").fancybox({
                                'width': 350,
                                'height' : 125,
                                'content' : messageHtml,
                                'autoDimensions' : false,
                                'centerOnScroll' : true
                            }).trigger('click');
                        }
                    });
                },
                error:function(data, status, xhr){
                    $("#yesResume").hide();
                    //$("#noResume").show();
                    var messageHtml = "<h2>Update Error</h2>";
                    messageHtml += "<p>We encounted an error when attempting to update your business profile.</p>";
                    messageHtml += "<p>Please check the data you entered and try again.</p>";
                    messageHtml += "<b>Thank you!</b>";
					
                    hideLoader();
					
                    $("<div></div>").fancybox({
                        'width': 350,
                        'height' : 125,
                        'content' : messageHtml,
                        'autoDimensions' : false,
                        'centerOnScroll' : true
                    }).trigger('click');
                }
            });

        }else{
            var submitData = $(form).serialize();
		  	
            $.ajax({
                type: "POST",
                url: "/widgets/dashboard/do/save-business-profile/",
                dataType    : 'json',
                cache       : false,
                data: submitData,
                success: function (data) {
						
                    var messageHtml = "<h2>Update Complete</h2>";
                    messageHtml += "<p>Your Tech Profile has been successfully updated.</p>";
                    messageHtml += "<b>Thank you!</b>";
						
                    hideLoader();
						
                    $("<div></div>").fancybox({
                        'width': 350,
                        'height' : 125,
                        'content' : messageHtml,
                        'autoDimensions' : false,
                        'centerOnScroll' : true
                    }).trigger('click');
                }
            });
        }
    }

    function submitPaymentInfo(form){

        if($('input[name="RoutingNum"]').val().search("X") != "-1"){
            $(form)[0]['RoutingNum'] = null;
        }

        if($('input[name="AccountNum"]').val().search("X") != "-1"){
            $(form)[0]['AccountNum'] = null;
        }
		
        var submitData = $(form).serialize();

        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/do/submit-payment-info/",
            dataType    : 'json',
            cache       : false,
            data: submitData,
            success: function (data) {
			
                var messageHtml = "<h2>Update Complete</h2>";
                messageHtml += "<p>Your Payment Info has been successfully updated.</p>";
                messageHtml += "<b>Thank you!</b>";
					
                hideLoader();
					
                if ($("#RoutingNum").val().length > 0) {
                    repeat = $("#RoutingNum").val().length - 4;
                    mask = "";
                    for (i=0; i < repeat; ++i) {
                        mask += "X";
                    }
                    $("#RoutingNum").val(mask + $("#RoutingNum").val().substr(repeat));
                    repeat = $("#AccountNum").val().length - 4;
                    mask = "";
                    for (i=0; i < repeat; ++i) {
                        mask += "X";
                    }
                    $("#AccountNum").val(mask + $("#AccountNum").val().substr(repeat));
                }
                $("<div></div>").fancybox({
                    'width': 350,
                    'height' : 125,
                    'content' : messageHtml,
                    'autoDimensions' : false,
                    'centerOnScroll' : true
                }).trigger('click');
            },
            error: function (data){
				
            }
        });
    }

    function submitUpdateTechInfo(form){
        if($("#badgePhotoUpload").val() != ""){
			
            $.ajaxFileUpload({
                url             : "/widgets/dashboard/do/save-business-profile/?techID="+$("#basicRegTechID").val(),
                secureuri       : false,
                fileElementId   : 'badgePhotoUpload',
                dataType        : 'json',
                cache           : false,
                success         : function (data, status, xhr) {
                    var submitData = $(form).serialize();
					if(data.success == '1'){
                        jQuery("#saveBadgePhoto").hide();
                        jQuery("#deleteBadgePhoto").show();
                        jQuery("#badgePhotoImage").attr("src",
                            "https://s3.amazonaws.com/tech-docs/profile-pics/"
                                +data.file.uploadResult.fileName);					
                        jQuery("#badgePhotoUpload").val("");
                    }
                    $.ajax({
                        type: "POST",
                        url: "/widgets/dashboard/do/save-business-profile/",
                        dataType    : 'json',
                        cache       : false,
                        data: submitData,
                        success: function (data) {
						       
                            var messageHtml = "<h2>Update Complete</h2>";
                            messageHtml += "<p>Your Business Profile has been successfully updated.</p>";
                            messageHtml += "<b>Thank you!</b>";
                                
                            hideLoader();
								
                            $("<div></div>").fancybox({
                                'width': 350,
                                'height' : 125,
                                'content' : messageHtml,
                                'autoDimensions' : false,
                                'centerOnScroll' : true
                            }).trigger('click');
                        }
                    });
                },
                error:function(data, status, xhr){
                    var messageHtml = "<h2>Update Error</h2>";
                    messageHtml += "<p>We encounted an error when attempting to update your business profile.</p>";
                    messageHtml += "<p>Please check the data you entered and try again.</p>";
                    messageHtml += "<b>Thank you!</b>";
					
                    hideLoader();
                    $("#badgeUpload").show();
                    $("#deleteBadgePhoto").hide();
                    $("#saveBadgePhoto").show();
                    $("<div></div>").fancybox({
                        'width': 350,
                        'height' : 125,
                        'content' : messageHtml,
                        'autoDimensions' : false,
                        'centerOnScroll' : true
                    }).trigger('click');
                }
            });

        }else{
            var submitData = $(form).serialize();
            $.ajax({
                type: "POST",
                url: "/widgets/dashboard/do/update-tech-info/",
                dataType    : 'json',
                cache       : false,
                data: submitData,
                success: function (data) {
							
                    var messageHtml = "<h2>Update Complete</h2>";
                    messageHtml += "<p>Your Tech Profile has been successfully updated.</p>";
                    messageHtml += "<b>Thank you!</b>";
                    profile.loadTechInfo();
                    hideLoader();
							
                    $("<div></div>").fancybox({
                        'width': 350,
                        'height' : 125,
                        'content' : messageHtml,
                        'autoDimensions' : false,
                        'centerOnScroll' : true
                    }).trigger('click');
                }
            });
        }
    }


    function savePaymentInfo(){
        $("#paymentInfo").submit();
    }

    function saveBusinessProfile(){
        $("#businessProfile").submit();
    }


    var loaderVisible = false;
    function showLoader(){
        profile.showLoader();
    }

    function hideLoader(){
        profile.hideLoader();
    }

    function deleteResume(){
        showLoader();
        $.ajax({
            type: "POST",
            url: '/widgets/dashboard/do/delete-tech-file/',
            dataType    : 'json',
            cache       : false,
            data: {file:$('body').data("resume"), type: "resume"},
            success: function (data) {
                hideLoader();
                $("#yesResume").hide();
                $("#resumeFile").html("");
            },
            error:function(data, status, xhr){
                hideLoader();
                $("#yesResume").show();
                //$("#noResume").hide();
            }
        });
    }

    function deleteBadgePhoto(){
        showLoader();
        $.ajax({
            type: "POST",
            url: '/widgets/dashboard/do/delete-tech-file/',
            dataType    : 'json',
            cache       : false,
            data: {file:$('body').data("profilePic"), type: "profile pic"},
            success: function (data) {
                hideLoader();
                $("#badgePhotoImage").attr("src","/widgets/images/profileAnon.jpg" );
                $("#badgeUpload").show();
                $("#deleteBadgePhoto").hide();
                $("#saveBadgePhoto").show();
            },
            error:function(data, status, xhr){
                hideLoader();
                $("#badgeUpload").hide();
                $("#deleteBadgePhoto").show();
                $("#saveBadgePhoto").hide();
            }
        });
    }

    function setPaymentInfo(){
	
        if($("#paymentAddressSame").attr("checked") == true){
            $("#paymentInfo, #BankCountry").val($('body').data("country"));
            $("#paymentInfo, #BankAddress1").val($('body').data("address1"));
            $("#paymentInfo, #BankAddress2").val($('body').data("address2"));
            $("#paymentInfo, #BankCity").val($('body').data("city"));
            $("#paymentInfo, #BankState").val($('body').data("state"));
            $("#paymentInfo, #BankZip").val($('body').data("zip"));
        }else{
            $("#paymentInfo, #BankCountry").val("");
            $("#paymentInfo, #BankAddress1").val("");
            $("#paymentInfo, #BankAddress2").val("");
            $("#paymentInfo, #BankCity").val("");
            $("#paymentInfo, #BankState").val("");
            $("#paymentInfo, #BankZip").val("");
        }
    }

    function applyFancybox(message){
        $("<div></div>").fancybox({
            'width': 350,
            'height' : 125,
            'content' : message,
            'autoDimensions' : false,
            'centerOnScroll' : true
        }).trigger('click');
    }
    function saveBadgePhoto(){
	
        if($("#badgePhotoUpload").val() != ""){
		
            $.ajaxFileUpload({
                url             : "/widgets/dashboard/do/save-business-profile/?techID="+$("#basicRegTechID").val(),
                secureuri       : false,
                fileElementId   : 'badgePhotoUpload',
                dataType        : 'json',
                cache           : false,
                success         : function (data, status, xhr) {
					if(data.success == '1')
                    {
                    $("#badgeUpload").hide();
                    $("#saveBadgePhoto").hide();
                        jQuery("#divApprovePhoto").show();
                        $("#badgePhotoImagePending").show();
                    $("#deleteBadgePhoto").show();
                        $("#badgePhotoImage").attr("src","https://s3.amazonaws.com/tech-docs/profile-pics/"+data.file.uploadResult.fileName );					
                    $("#badgePhotoUpload").val("");
                    var messageHtml = "<h2>Update Complete</h2>";
                    messageHtml += "<p>Your Business Profile has been successfully updated.</p>";
                    messageHtml += "<b>Thank you!</b>";
				
                    hideLoader();
                    applyFancybox(messageHtml);
                    }
                    else
                    {
						var messageHtml = "<h2>Update Error</h2>";
                        messageHtml += "<p>We encounted an error when attempting to update your business profile.</p>";
                        messageHtml += "<p>Please check the data you entered and try again.</p>";
                        messageHtml += "<b>Thank you!</b>";
				
                        hideLoader();
                        $("#badgeUpload").show();
                        $("#deleteBadgePhoto").hide();
                        $("#saveBadgePhoto").show();
                        applyFancybox(messageHtml);
					}		
				
                },
                error:function(data, status, xhr){
                    var messageHtml = "<h2>Update Error</h2>";
                    messageHtml += "<p>We encounted an error when attempting to update your business profile.</p>";
                    messageHtml += "<p>Please check the data you entered and try again.</p>";
                    messageHtml += "<b>Thank you!</b>";
				
                    hideLoader();
                    $("#badgeUpload").show();
                    $("#deleteBadgePhoto").hide();
                    $("#saveBadgePhoto").show();
                    applyFancybox(messageHtml);
                }
            });

        }
    }
	function ApprovePhoto(el)
    {
        showLoader();
        var techid = jQuery(el).attr("techid");
        var photo = jQuery(el).attr("photo");
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/do/approvephoto/",
            dataType    : 'json',
            cache       : false,
            data: 'techid='+techid,
            success: function (data) {

                var messageHtml = "Your Profile has been updated";
                if(data.success == 'error')
                {
                    messageHtml = "Your Profile has been error.";                 
                }
                else
                {
                    var photo = jQuery(el).parent().attr('photo');
                    jQuery("#badgePhotoImage").attr('src',photo);
                }
                profile.loadTechInfo();
                hideLoader();
							
                $("<div></div>").fancybox({
                    'width': 350,
                    'height' : 125,
                    'content' : messageHtml,
                    'autoDimensions' : false,
                    'centerOnScroll' : true
                }).trigger('click');
            }
        });
    }
    function saveEquipment(){
        showLoader();
        var sendData = [];
        var telephonyData = [];
        $("input[name='basicEquipment[]']").each(function(){
            if($(this).attr("checked") == true){
                sendData.push({column:this.value,value:"1"});
            }else{
                sendData.push({column:this.value,value:"0"});
            }
        });

        $("input[name='cablingEquipment[]']").each(function(){
            if($(this).attr("checked") == true){
                sendData.push({column:this.value,value: this.value,checked:"1"});
            }else{
                sendData.push({column:this.value,value: this.value,checked:"0"});
            }
        });

        $("input[name='telephonySkills[]']").each(function(){
            if($(this).attr("checked") == true){
                telephonyData.push({column:this.value,value: this.value,checked:"1"});
            }else{
                telephonyData.push({column:this.value,value: this.value,checked:"0"});
            }
        });

        if($("#vehicleImage").val() != "" && $("#vehicleID").val() != ""){
            $.ajax({
                type: "POST",
                url: '/widgets/dashboard/do/delete-tech-file/',
                dataType    : 'json',
                cache       : false,
                data: {file:$("#vehicleCurrentImage").val(), type: "profile pic"},
                success: function (data) {
				
                },
                error:function(data, status, xhr){
				
                }
            });
        }
	var languegaSkills = "";
        jQuery(".LanguageSkillsCheckbox").each(function(){
            var event = jQuery(this).parent().parent();
            languegaSkills += ',{"checked":"'+(jQuery(this).is(":checked")?1:0)+'","value":"'+jQuery(this).val()+'","selected":"'+jQuery(event).find("select[name='SpeakerLevel[]']").val()+'"}';
        });
        if(languegaSkills != ""){
            languegaSkills = "["+languegaSkills.substring(1, languegaSkills.length)+"]";
        }
        if($("#vehicleImage").val() != ""){
		
            $.ajaxFileUpload({
                url             : "/widgets/dashboard/do/save-equipment/?techID="+$("#basicRegTechID").val()+"&vehicleDesc="+$("#vehicleDesc").val(),
                secureuri       : false,
                fileElementId   : 'vehicleImage',
                dataType        : 'json',
                cache           : false,
                success         : function (data, status, xhr) {
			
                    $.ajax({
                        type: "POST",
                        url: '/widgets/dashboard/do/save-equipment/',
                        dataType    : 'json',
                        cache       : false,
                        data: {data:JSON.stringify(sendData), 
                            techID: $("#basicRegTechID").val(), 
                            otherTools: $("#Tools").val(), 
                            telephonyData: JSON.stringify(telephonyData)
                        },
                        success: function (data) {
                            hideLoader();
                            var messageHtml = "<h2>Update Complete</h2>";
                            messageHtml += "<p>Your Business Profile has been successfully updated.</p>";
                            messageHtml += "<b>Thank you!</b>";
                            applyFancybox(messageHtml);
						
                        },
                        error:function(data, status, xhr){
                            hideLoader();
                            var messageHtml = "<h2>Update Error</h2>";
                            messageHtml += "<p>We encounted an error when attempting to update your business profile.</p>";
                            messageHtml += "<p>Please check the data you entered and try again.</p>";
                            messageHtml += "<b>Thank you!</b>";
                            applyFancybox(messageHtml);
                        }
                    });
                },
                error:function(data, status, xhr){
                    var messageHtml = "<h2>Update Error</h2>";
                    messageHtml += "<p>We encounted an error when attempting to update your business profile.</p>";
                    messageHtml += "<p>Please check the data you entered and try again.</p>";
                    messageHtml += "<b>Thank you!</b>";
				
                    hideLoader();
                    applyFancybox(messageHtml);
                }
            });

        }else{

            $.ajax({
                type: "POST",
                url: '/widgets/dashboard/do/save-equipment/',
                dataType    : 'json',
                cache       : false,
                data: {data:JSON.stringify(sendData), 
                    techID: $("#basicRegTechID").val(), 
                    otherTools: $("#Tools").val(),
                    telephonyData: JSON.stringify(telephonyData),
                    languegaSkills : languegaSkills
                },
                success: function (data) {
                    hideLoader();
                    var messageHtml = "<h2>Update Complete</h2>";
                    messageHtml += "<p>Your Business Profile has been successfully updated.</p>";
                    messageHtml += "<b>Thank you!</b>";
                    applyFancybox(messageHtml);
				
                },
                error:function(data, status, xhr){
                    hideLoader();
                    var messageHtml = "<h2>Update Error</h2>";
                    messageHtml += "<p>We encounted an error when attempting to update your business profile.</p>";
                    messageHtml += "<p>Please check the data you entered and try again.</p>";
                    messageHtml += "<b>Thank you!</b>";
                    applyFancybox(messageHtml);
                }
            });
        }
    }
    
    function editSection(tableID){

        if(tableID == "clientCredentials")
        {
            //13624
            //931
	    $(".cbxHasMultiTagLevels").show();
            //931
            $(".ClientCredentialsImage").hide();
            if(jQuery("#divGGETechEvaluationAllow").attr("allow") == "1"){
                jQuery("#divGGETechEvaluationAllow").show();
            }
            jQuery("#BlackBoxCablingNumInput").show();
            jQuery(".BlackBoxCablingStatus").hide();
            jQuery("#BlackBoxTelecomStatusNumInput").show();
            jQuery(".BlackBoxTelecomStatus").hide();
            
        }
        if(tableID != "programCertifications" && tableID != "myCertifications"){
            $.each($("#"+tableID+" input[id*='Input']"),function(){
                
                var key = $(this).attr("id");
                //761
                if(key=="BlackBoxCablingInput")
                {
                    if($("#BlackBoxCablingNumInput").val()!="pending")
                    {
                        $("#BlackBoxCablingInput").val($.trim($("#BlackBoxCablingDate").html()));
                    }
                    $(this).css("display","table-cell").calendar({dateFormat:'MDY/'}).val();
                    $("#BlackBoxCablingNumInput").unbind('change').bind('change',function(){
                        if($(this).val()!='pending' && $(this).val()!='')
                        {
                            $("#BlackBoxCablingInput").val($.datepicker.formatDate('mm/dd/yy', new Date()));
                }
                        else
                        {
                            $("#BlackBoxCablingInput").val('');
                        }
                    })
                }
                else if  (key=="BlackBoxTelecomInput")
                {
                     if($("#BlackBoxTelecomNumInput").val()!="pending")
                    {
                        $("#BlackBoxTelecomInput").val($.trim($("#BlackBoxTelecomDate").html()));
                    }
                    $(this).css("display","table-cell").calendar({dateFormat:'MDY/'}).val();
                    $("#BlackBoxTelecomStatusNumInput").unbind('change').bind('change',function(){
                        if($(this).val()!='pending' && $(this).val()!='')
                        {
                            $("#BlackBoxTelecomInput").val($.datepicker.formatDate('mm/dd/yy', new Date()));
                }
                else
                {
                            $("#BlackBoxTelecomInput").val('');
                        }
                    })
                }
                else
                {
                if(key.search("NumInput") != -1 && key != "essintialCertNumInput" && key != "essintialCertDateInput"){
                    var divID = $(this).attr("id").replace("NumInput","");
                    var Val = $("#"+divID+" div[id*=NumLabel]").html();
                    $(this).val(Val);
                }
                else if($(this).attr("id") != "essintialCertNumInput" && $(this).attr("id") != "essintialCertDateInput" ){
			
                    var divID = $(this).attr("id").replace("Input","");
                    var dateVal = $("#"+tableID+" div[id*="+divID+"Date]").html();
			
                    $(this).css("display","table-cell").calendar({dateFormat:'MDY/'}).val(dateVal);
                    
                    jQuery("#GGETechEvaluationDateInputFirst").val(jQuery("#GGETechEvaluationDateInputFirst").attr("val"));
                    jQuery("#GGETechEvaluationDateInputSecond").val(jQuery("#GGETechEvaluationDateInputSecond").attr("val"));
                }
                }
			
            });
            $.each($("#"+tableID+" input"),function(){
                $(this).css("display","table-cell");
            });
            $.each($("#"+tableID+" .StatusLabel"),function(){
                $(this).css("display","table-cell");
            });
            $.each($("#"+tableID+" div[id*=Label]"),function(){
                $(this).hide();
            });
            
            $("#"+tableID+" div[id*='Date']").hide();
            
            $("#CompuComCertifiedStatusInput").show();
            var CompuComdateVal = $("#CompuComCertifiedDate").html();
            $("#CompuComCertifiedDateInput").css("display","table-cell").calendar({dateFormat:'MDY/'}).val(CompuComdateVal);
            $("#flsIdStatus").hide();
            $("#flsIdNumInput").val($("#flsIdStatus").html());
            $("#flsIdStatusInput").show();
            var flsIdVal = $("#flsIdDate").html();
            $("#flsIdDateInput").css("display","table-cell").calendar({dateFormat:'MDY/'}).val(flsIdVal);
            $("#ErgoMotionCertifiedTechnicianStatusChk").hide();
            $("#ErgoMotionCertifiedTechnicianInput").hide();
            $("#ErgoMotionCertifiedTechnicianStatusLabel").show();
            $("#ErgoMotionCertifiedTechnicianDate").show();
            
            $("#"+tableID+" #WorkAuthorizationStatus").children("img").css("display","none");
            
            $("#"+tableID+" #I9Status").css("display","");
            $("#"+tableID+" #WorkAuthorizationValue").css("display","none");
            isSave = false;
            if($("#"+tableID+" #I9Status").val() == 2)
            {
                jQuery("#I9Date").css("display","");  
                urlOld = jQuery("#USAuthorized").html();
                //$("#I9Date").calendar({dateFormat:'MDY/'}).val(jQuery("#I9Date").val());
            }
            else
            {
                jQuery("#I9Date").css("display","none");      
            }
            jQuery("#USAuthorized").html("US Verified");
		
            jQuery("#TMCStatusDisplay").css("display","none");      
            jQuery("#TMCDate").css("display","none");      
            
            //$.datepicker.setDefaults({ dateFormat: 'mm/dd/yy'});
            jQuery("#TMCDateTxt" ).datepicker({ dateFormat: 'mm/dd/yy'});
            //clientCredentials
            
            if(tableID == "publicCredentials"){
	        	if ($("#fsMobileStatus img").length) {
					$("#fsMobileStatus").html ("&nbsp;<input type='checkbox' name='FS_Mobile_Enabled' id='FS_Mobile_Enabled' value='1' checked />");
	        	}
	        	else {
					$("#fsMobileStatus").html ("&nbsp;<input type='checkbox' name='FS_Mobile_Enabled' id='FS_Mobile_Enabled' value='1' />");
	        	}
            //13932
            var nDate = $("#NACIDate").html();
            $("#NACIInput").css("display","table-cell").calendar({dateFormat:'MDY/'}).val(nDate);
                        
            //056
            $("#NACICertStatus").show();
            $("#interimGovtCertStatus").show();
            $("#fullGovtCertStatus").show();
            $("#topGovtCertStatus").show();
            $("#topSCIGovtStatus").show();
            //end 056
            }
		
        }else if(tableID == "programCertifications"){
            $.each($("#"+tableID+" tr"),function(){
                var divID = $(this).attr("id");
                var certVal = $("#"+divID+"NumDisplay").html();
                if(isNaN(certVal)){
                    certVal = "";
                }
                $("#"+divID+"NumDisplay").hide();
                $("#"+divID+"NumInput").val(certVal).css("display","table-cell");

            });
        }else{
            $.each($("#"+tableID+" tr[id*='Cert']"),function(){
                var divID = $(this).attr("id");
                divID = divID.replace("Cert","");
                var checkBoxElement = $("#"+(divID == 'aPlus' ? 'APlus' : divID));
					
                if(checkBoxElement.length == 0){
					var certVal = $("#"+divID+"CertNumDisplay").text();
		
                    $("#"+divID+"CertNumDisplay").hide();
                    $("#"+divID+"CertCheckCertNumDisplay").hide();
                    $("#APlus").show();
                    $("#aPlusCertNumDisplay").hide();
                    $("#"+divID+"CertNumInput").val(certVal).css("display","table-cell");
	
                }else{
                    $("#"+divID+"CertNumDisplay").hide();
                    checkBoxElement.show();
                }
				
                $("#"+divID+"CertStatus").css("text-align","center");
                
                var dateVal = $("#"+divID+"CertDateDisplay").html();
                if(divID != 'EMC')
                {
	                $("#"+divID+"CertDateDisplay").hide();
                	$("#"+divID+"CertDateInput").css("display","table-cell").calendar({dateFormat:'MDY/'}).val(dateVal);
                }
            });
        }

        $("#"+tableID+"Header div[class='editSection']").each(function(){
            $(this).hide();
        });

        $("#"+tableID+"Header div[class='cancelSaveSection']").each(function(){
            $(this).show();
        });
	
    }

    function saveSection(formID){
        var form = $("#"+formID);

        var submitData = $(form).serialize();
        var formCheckbox = $("#"+formID + " input[type='checkbox']");
		
        formCheckbox.each(function(index) {
            checked = $(this).attr('checked') ? "1" : "0";
            submitData += "&" + $(this).attr('id') + "=" + checked;
        });

        certData = [];

        $("input[name='certifications[]']").each(function(){
            divID = $(this).attr('id');
            if($(this).attr("checked") == true)
            {
                certData.push({column:this.value,value: this.value,checked:"1",date:$("#"+divID+"CertDateInput").val(),number: null});
            }
            else
            {
                certData.push({column:this.value,value: this.value,checked:"0",date:$("#"+divID+"CertDateInput").val(),number: null});
            }
        });
	
        submitData += "&certificationsInfo=" + JSON.stringify(certData);

        showLoader();
	
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/do/update-tech-info/",
            dataType    : 'json',
            cache       : false,
            data: submitData,
            success: function (data) {
                urlOld = "";
                isSave = true;
                if(jQuery("#I9Status").val() == "2")
                {
                    urlOld = ("<a target='_blank' href='/techs/ajax/work_authorization_pdf.php?techid="+jQuery("#basicRegTechID").val()+"'>US Verified</a>");
                }
                else
                {
                    urlOld = ("US Verified");             
                }
                                
                if(data.success=='success')
                {
                    if(data.section != null && data.section=='ClientCredentials')
                    {
                        var TMCStatus = data.TMCStatus;
                        var TMCDate = data.TMCDate;
                        if(TMCStatus==1)
                        {
                            imgSrc = "/widgets/images/check_green.gif";
                            $("#TMCStatusDisplay").html("<img src='" + imgSrc + "'/>");
                            if(TMCDate != null)
                                $("#TMCDate").html(TMCDate);
                        }
                        else
                        {
                            $("#TMCStatusDisplay").html('');
                            $("#TMCDate").html('');
                            $("#TMCDateTxt").val('');
                        }
                    }
                var messageHtml = "<h2>Update Complete</h2>";
                messageHtml += "<p>Your Tech Profile has been successfully updated.</p>";
                messageHtml += "<b>Thank you!</b>";
                hideLoader();
                applyFancybox(messageHtml);
                
                location.reload(); 
            }
                else
                {
                    var messageHtml = "<h2>Error </h2>";
                    messageHtml += "<p>"+data.success+"</p>";
                    hideLoader();
                    applyFancybox(messageHtml);                    
                }
            },
			error: function (xhr, msg, e) {
                var messageHtml = "<h2>Update Failed</h2>";
                messageHtml += "<p>Your Tech Profile was unable to be updated at this time.</p>";
                messageHtml += "<b>We are sorry for the inconvenience.</b>";
				hideLoader();
                applyFancybox(messageHtml);
			}
        });
    }

    function cancelSection(tableID){
        if(tableID == "clientCredentials")
        {
            //13624
            //931
	    $(".cbxHasMultiTagLevels").hide();
            //931
            $(".ClientCredentialsImage").show();
            jQuery("#divGGETechEvaluationAllow").hide();
            jQuery("#BlackBoxCablingNumInput").hide();
            jQuery(".BlackBoxCablingStatus").show();
            jQuery("#BlackBoxTelecomStatusNumInput").hide();
            jQuery(".BlackBoxTelecomStatus").show();
        }
        if(tableID != "programCertifications" && tableID != "myCertifications"){
            $.each($("#"+tableID+" input[id*='Input']"),function(){
                if($(this).attr("id") != "essintialCertNumInput" && $(this).attr("id") != "essintialCertDateInput"){
                    var divID = $(this).attr("id").replace("Input","");
				
                    var dateVal = $(this).val();
				
                    $("#"+tableID+" div[id*="+divID+"Date]").html(dateVal);
                    $(this).hide();
                }
            });
		
            $.each($("#"+tableID+" input"),function(){
                $(this).hide();
            });
            $.each($("#"+tableID+" .StatusLabel"),function(){
                $(this).hide();
            });
            $.each($("#"+tableID+" div[id*=Label]"),function(){
                $(this).css("display","table-cell");
            });
		
            $("#"+tableID+" div[id*='Date']").show();
            $("#"+tableID+" #WorkAuthorizationValue").css("display","");
            if(urlOld != "")
            {
                jQuery("#USAuthorized").html(urlOld);
            }
            if(isSave)
            {
                if($("#"+tableID+" #I9Status").children("option:selected").val() == 2)
                {
                    $("#"+tableID+" #WorkAuthorizationStatus").children("img").css("display","");   
                    $("#"+tableID+" #WorkAuthorizationValue").html(jQuery("#I9Date").val());
                }
                else if($("#"+tableID+" #I9Status").children("option:selected").val() == 1)
                {
                    $("#"+tableID+" #WorkAuthorizationStatus").children("img").css("display","none");   
                    $("#"+tableID+" #WorkAuthorizationValue").html("Pending");  
                }
                else
                {
                    $("#"+tableID+" #WorkAuthorizationValue").html("");  
                }
            }
            else
            {
                if(jQuery.trim($("#"+tableID+" #WorkAuthorizationValue").html()) != "Pending"
                    && jQuery.trim($("#"+tableID+" #WorkAuthorizationValue").html()) != "")
                {
                    $("#"+tableID+" #WorkAuthorizationStatus").children("img").css("display",""); 
                }
                else
                {
                     $("#"+tableID+" #WorkAuthorizationStatus").children("img").css("display","none");         
                }
            }
            jQuery("#I9Date").css("display","none");
            $("#"+tableID+" #I9Status").css("display","none");
            jQuery("#TMCStatusDisplay").css("display","table-cell");
            jQuery("#TMCDate").css("display","table-cell");            		
        }else if(tableID == "programCertifications"){
            $.each($("#"+tableID+" tr"),function(){
                var divID = $(this).attr("id");
                var certVal = $("#"+divID+"NumInput").val();
			
                if(isNaN(certVal) || certVal == ""){
                    $("#"+divID+"NumDisplay").show();
                }else{
                    $("#"+divID+"NumDisplay").html(certVal);
                }
			
                $("#"+divID+"NumInput").css("display","none");

            });
        }else{
            $.each($("#"+tableID+" tr"),function(){
                var divID = $(this).attr("id");
                if (divID == "") return;
                divID = divID.replace("Cert","");
                var checkBoxElement = $("#"+(divID == 'aPlus' ? 'APlus' : divID));
                if(checkBoxElement.length == 0){				
                    var certVal = $("#"+divID+"CertNumInput").val();
                    if(isNaN(certVal) || certVal == ""){
                        $("#"+divID+"CertNumDisplay").show();
                    }else{
                        //$("#"+divID+"CertNumDisplay").html(certVal);
                        $("#"+divID+"CertNumDisplay").show();
                    }
	
                    $("#"+divID+"CertNumInput").css("display","none");
	
                }else{
                    if(checkBoxElement.attr("checked") ==  true){
                        $("#"+divID+"CertNumDisplay").html("");
                        $("#"+divID+"CertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                    }else{
                        $("#"+divID+"CertNumDisplay").html("");
                    }
                    $("#"+divID+"CertNumDisplay").show();
                    $("#APlus").hide();
                    $("#aPlusCertNumDisplay").show();
                    checkBoxElement.hide();
                }
                var dateVal = $("#"+divID+"CertDateInput").val();
                $("#"+divID+"CertDateDisplay").show().html(dateVal);
                $("#"+divID+"CertDateInput").css("display","none");
                
                $("#"+divID+"CertStatus").css("text-align","left");
                
            });
        }
        //056
        if($("#publicCredentials #NACICertStatus option:selected").val()!= $("#publicCredentials #NACIStaVal").val())
        {
            $("#publicCredentials #NACICertStatus option").each(function() {
                    if ($(this).val() === $("#publicCredentials #NACIStaVal").val())
                    {
                        $(this).attr("selected", "selected");
                    }
                });
            $("#publicCredentials #NACIDate").html($("#publicCredentials #NACIVal").val());
            $("#publicCredentials #NACIInput").val($("#publicCredentials #NACIVal").val());
        }
        $("#NACICertStatus").hide();
        
        if($("#publicCredentials #interimGovtCertStatus option:selected").val()!= $("#publicCredentials #interimGovtStaVal").val())
        {
            $("#publicCredentials #interimGovtCertStatus option").each(function() {
                    if ($(this).val() === $("#publicCredentials #interimGovtStaVal").val())
                    {
                        $(this).attr("selected", "selected");
                    }
                });
            $("#publicCredentials #interimGovtDate").html($("#publicCredentials #interimGovtVal").val());
            $("#publicCredentials #interimGovtInput").val($("#publicCredentials #interimGovtVal").val());
        }
        $("#interimGovtCertStatus").hide();
        
        if($("#publicCredentials #topGovtCertStatus option:selected").val()!= $("#publicCredentials #topGovtStaVal").val())
        {
            $("#publicCredentials #topGovtCertStatus option").each(function() {
                    if ($(this).val() === $("#publicCredentials #topGovtStaVal").val())
                    {
                        $(this).attr("selected", "selected");
                    }
                });
            $("#publicCredentials #topGovtDate").html($("#publicCredentials #topGovtVal").val());
            $("#publicCredentials #topGovtInput").val($("#publicCredentials #topGovtVal").val());
        }
        $("#fullGovtCertStatus").hide();
        
        if($("#publicCredentials #fullGovtCertStatus option:selected").val()!= $("#publicCredentials #fullGovtStaVal").val())
        {
            $("#publicCredentials #fullGovtCertStatus option").each(function() {
                    if ($(this).val() === $("#publicCredentials #fullGovtStaVal").val())
                    {
                        $(this).attr("selected", "selected");
                    }
                });
            $("#publicCredentials #fullGovtDate").html($("#publicCredentials #fullGovtVal").val());
            $("#publicCredentials #fullGovtInput").val($("#publicCredentials #fullGovtVal").val());
        }
        $("#topGovtCertStatus").hide();
        
        if($("#publicCredentials #topSCIGovtStatus option:selected").val()!= $("#publicCredentials #fullGovtStaVal").val())
        {
            $("#publicCredentials #topSCIGovtStatus option").each(function() {
                    if ($(this).val() === $("#publicCredentials #fullGovtStaVal").val())
                    {
                        $(this).attr("selected", "selected");
                    }
                });
            $("#publicCredentials #topSCIGovtDate").html($("#publicCredentials #topSCIGovtVal").val());
            $("#publicCredentials #topSCIGovtInput").val($("#publicCredentials #topSCIGovtVal").val());
        }
        $("#topSCIGovtStatus").hide();
        //end 056
        $("#CompuComCertifiedStatusInput").hide();
        $("#flsIdStatus").show();
        $("#flsIdStatusInput").hide();
        $("#"+tableID+"Header div[class='editSection']").each(function(){
            $(this).show();
        });

        $("#"+tableID+"Header div[class='cancelSaveSection']").each(function(){
            $(this).hide();
        });
    }

    function cancelPaymentInfo(){}



</script>

<style type="text/css">

    .required{
        padding: 0;
        color: #000;
    }
    .error{
        padding: 0;
        line-height: 12px;

    }
    *{
        margin: 0;
        padding: 0;
        border: none;
        text-decoration: none;
        outline: none;
    }
    #wr{
        margin: 20px auto;
        width: 740px;
        text-align: left;
    }
    #acc dt, #acc dd{
        /*width: 100%;*/
    }
    .sectionHeading{
        cursor: pointer;
        display: block;
        padding: 3px 10px;
        background: #E1E1E1;
        color: #000;
        border: solid 1px #666;
        height: 20px;
        font-weight: bold;
        font-size: 14px;
        text-align:left;
    }

    #acc dt.act{
        background: #E1E1E!;
    }

    #acc dd{
        display: none;
        padding: 3px 10px;
    }

    .label{
        text-align: left;
        font-weight:bold;

    }

    td.field input.invalid, td.field select.invalid, tr.errorRow td.field input, tr.errorRow td.field select {
        background-color: #FFFFD5;
        border: 2px solid red;
        color: red;
        margin: 0;
    }
    tr td.field div.formError {
        color: #FF0000;
        display: none;
    }
    tr.errorRow td.field div.formError {
        display: block;
        font-weight: normal;
    }
    .invalid, label.invalid {
        color: red;
    }
    .invalid a {
        color: #336699;
        font-size: 12px;
        text-decoration: underline;
    }
    #section{
        border-right: 1px solid #000;
        border-left: 1px solid #000;
        border-bottom: 1px solid #000;
    }
    #reqStar{
        color: rgb(255, 0, 0); 
        font-size: 12px; 
        font-family: Verdana; 
        margin-left: 2px;
    }

    .round {

        border-radius: 3px; 
        -webkit-border-radius: 3px; 
        -moz-border-radius: 3px; 
        -moz-border-radius: 10px 10px 10px 10px;
        -moz-box-shadow: 2px 2px 2px #888888;
        background: url("/widgets/css/images/section_background.gif") repeat-y scroll left top transparent;
        border: 2px solid #DDDDDD;
        color: #FFF;
    } 

    #subDivider{
        width: 710px; 
        height: 25px; 
        background-color: #E1E1E1; 
        border: 1px solid #000;
        font-weight: bold;
        padding-top: 5px;
    }

    table.tablesorter thead tr th, table.tablesorter tfoot tr th {
        background-color: #4790D4;
        color: #FFF;
        border: 1px solid #000;
        font-size: 8pt;
        padding: 4px;
    }
    table.tablesorter{
        border: 1px solid #000;
        width: 600px;
    }
    table.tablesorter tbody td{
        border: 1px solid #000;

    }

    table.tablesorter2 thead tr th, table.tablesorter2 tfoot tr th {
        background-color: #4790D4;
        color: #FFF;
        border: 1px solid #000;
        font-size: 8pt;
        padding: 4px;
    }
    table.tablesorter2{
        border: 1px solid #000;
        width: 400px;
    }
    table.tablesorter2 tbody td{
        border: 1px solid #000;

    }

    table th{
        text-align: center;
    }

    div#loader{
        display: none;
        width:100px;
        height: 100px;
        position: fixed;
        top: 50%;
        left: 50%;
        background:url(/widgets/images/loader.gif) no-repeat center #fff;
        text-align:center;
        padding:10px;
        font:normal 16px Tahoma, Geneva, sans-serif;
        border:1px solid #666;
        margin-left: -50px;
        margin-top: -50px;
        z-index:2;
        overflow: auto;
    }

    #socialNetworks{
        margin-right: auto; 
        margin-left: auto; 
        text-align: center;
        margin-top: 5px;
    }

    #socialNetworks span{
        margin: 0 13px 0 13px;
    }

    .telephony3Column{
        width: 100%
    }
    table.telephony3Column td{
        width: 33%;
    }

    table.telephony3Column td.indent{
        padding-left: 15px;
    }

    .toolsResources{
        width: 100%;
    }
    .toolsResources table{

    }
    .subSectionCol{
        width: 150px;
    }

    .cancelUpdateButton, .updateButton{
        margin-top: 0px;
        padding: 0px;
    }

    a.title{
        background: url("/widgets/css/images/bullet_toggle_plus.png") no-repeat scroll -4px -9px transparent;
        cursor: pointer;
        display: block;
        float: left;
        height: 20px;
        width: 20px;
        margin-right: 5px;
    }
    dt.act a.title{
        background: url("/widgets/css/images/bullet_toggle_minus.png") no-repeat scroll -4px -9px transparent;
        cursor: pointer;
        display: block;
        /* padding-left: 23px; */
        float: left;
        height: 20px;
        width: 20px;
        margin-right: 5px;
    }

    a.bgCheck{
        display: block;
        height: 35px;
        overflow: hidden;
        width: 100px;
        margin-left:auto;
        margin-right:auto;
    }

    a.bgCheck:hover img{
        margin-top: -33px;
    }

    a.uploadW9{
        display: block;
        height: 24px;
        overflow: hidden;
        width: 85px;
        margin-left:auto;
        margin-right:auto;
    }

    a.uploadW9:hover img{
        margin-top: -20px;
    }

</style>

<div id="loader">
    Loading...
</div>
<div id="dasboardContainer" class="wide_content">

    <div id="wr">
    </div>
<?php require ("../footer.php"); ?>