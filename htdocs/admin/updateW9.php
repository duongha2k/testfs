<?php $page = admin; ?>
<?php $option = techs; ?>
<?php $selected = "updateW9"; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->

<?php
	require_once("../library/caspioAPI.php");

	checkAdminLogin2();
?>

<style type="text/css">
</style>

<br/><br/>

<div style="text-align: center">
<form name="uploadImport" method="post" action="<?php echo $_SERVER['PHP_SELF']?>" enctype="multipart/form-data">
	<label for="importFile">List of Techs in CSV format:</label>
	<input name="importFile" type="file" size="40"/>
	<input name="ignoreWarning" type="checkbox" value="1" /> Ignore warnings<br/><br/>
	<input name="submit" type="submit" value="Submit" />
</form>


<br/>
<?php

require_once("../library/caspioAPI.php");
//	require_once("importInclude.php");


	// has file been uploaded?
	if (isset($_POST["submit"])) {		
		$requiredColumns = array("TechID");
		
		if (!isset($_FILES["importFile"]["tmp_name"]) || !file_exists($_FILES["importFile"]["tmp_name"])) {
			echo "No file Uploaded";
			die();
		}

		// is file too large?
		if ($_FILES["importFile"]["size"] > 150000) {
			// file too large
		}
		
		
		echo "<p>Checking uploaded file ... </p>";
		
					
		$handle = fopen($_FILES["importFile"]["tmp_name"], "r");
		$importColumns = fgetcsv($handle);
		
		$sizeImport = sizeof($importColumns);
		$sizeRequired = sizeof($requiredColumns);
		if ($sizeImport != $sizeRequired) {
			echo "Unexpected number of columns found - expecting $sizeRequired but found $sizeImport";
			die();
		}
		
		$index = 0;
		foreach ($importColumns as $col) {
			if ($col != $requiredColumns[$index]) {
				echo "Unexpected column found - expecting '{$requiredColumns[$index]}' but found '$col'";
				die();
			}
			$index++;
		}
		$updateQuery = "";
		
		$TechIDList = array();
		$TechIDFoundMap = array();
		$updateQuery = array();
		
		while (!feof($handle)) {
			$importRow = fgetcsv($handle);
			if (sizeof($importRow) == 1 && $importRow[0] == '') continue;
			$TechID = $importRow[0];
			$TechIDList[] = "'$TechID'";			

			if (!is_numeric($TechID)) {
				echo "TechID is not a number: $TechID";
				die();
			}
			/*
			if (!array_key_exists($TechID, $TechIDFoundMap)) {
				$TechIDFoundMap[$TechID] = 0;
			}
			else {
			
				echo "Duplicate TechID: $TechID";
				die();
			}
			*/
//			$updateQuery[] = "'$TechID'";
		}

		if (sizeof($TechIDList) == 0) {
			echo "No data found.";
			die();
		}
		
		
		
		foreach ($TechIDList as $row) {
			$row = explode(",", $row);
			$techID = trim($row[0], "'");
			
			echo "$techID<br />";
		}
				
		$numImportedRows = sizeof($TechIDList);
		$TechIDList = implode(",", $TechIDList);
		
		echo "Size of array: $numImportedRows";

		caspioUpdate("TR_Master_List", "W9", "0", "TechID > 0");
		caspioUpdate("TR_Master_List", "W9, W9_Date_Rec", "1, GETDATE()", "TechID IN ($TechIDList)");

		// $TechIDFound = caspioSelect("TR_Master_List_JC", "TechID, W9", "TechID IN ($TechIDList)", "");


			//caspioUpdate("TR_Master_List_JC", "W9, W9_Date_Rec", "1, GETDATE()", "TechID = $TechID");
/*			
		$error = false;
		$warning = false;

		if ($numImportedRows != sizeof($TechIDFound)) {
			foreach ($TechIDFound as $row) {
				$id = explode(",", $row, 2);
				$id = $id[0];
				$w9 = $id[1];
				if (array_key_exists($id, $TechIDFoundMap)) {
					$TechIDFoundMap[$id] = 1;
				}
			}
			echo "Unable to find the following TechID: <br/>";
			foreach ($TechIDFoundMap as $key=>$val) {
				if ($val == 1) continue;
				echo "$key<br/>";
				$error = true;
			}
			echo "<br/>";
		}
		
*/	

		/*

		*/
		
		
/*
		if ($error || ($warning && !isset($_POST["ignoreWarning"])) ) {
			echo "<br/>No data imported.";
			die();
		}

		echo "<br/>Checks Successful.<br/>";
		echo "<br/>Importing ... <br/>";



		foreach ($updateQuery as $TechID) {
			echo "UPDATE (W9, W9_Date_Rec) 1, '04/01/2008' WHERE TechID = $TechID<br/>";
			
		}
*/
		echo "<br/>Import Complete.<br/>";		
	}
?>
</div>
<?php require ("../footer.php"); ?>
