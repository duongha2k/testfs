<?php header("Content-Type: text/html; charset=utf-8");?>
<?php $page = "admin"; ?>
<?php $option = "techs"; ?>
<?php $selected = "bidsurvey"; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->

<?php
	ini_set("display_errors", 1);
	require_once("../library/caspioAPI.php");
	require_once("../library/smtpMail.php");
	if (!isset($_POST["send"]) && !isset($_GET["send"])) die();
	
	if (isset($_POST["send"])) {
		// after email form submitted
		// get bidding tech's email address
		$project_id = caspioEscape($_POST["Project"]);
		$emailAddressQuery = "SELECT PrimaryEmail FROM TR_Master_List WHERE Work_Order_Applicants.TechID = TR_Master_List.TechID AND AcceptTerms = 'Yes'";
		
		// find unassigned non-deactivated and non-invoiced and with an email address
		$unassignedWOQuery = "SELECT DISTINCT ($emailAddressQuery) FROM Work_Order_Applicants JOIN Work_Orders ON Work_Orders.TB_UNID = Work_Order_Applicants.WorkOrderID WHERE Project_ID = '$project_id' AND Deactivated = '0' AND Invoiced = '0' AND ISNULL(Work_Orders.Tech_ID, '') = '' AND Hide = '0'";
		$techList = caspioSelectAdv("Call_Type", "PrimaryEmail = ''", "id = 0 UNION ($unassignedWOQuery)", "", false, "`", "|", false);
		
		$fromEmail = caspioSelectAdv("TR_Client_Projects", "TOP 1 From_Email", "Project_ID = '$project_id'", "", false, "`", "|");
		$fromEmail = trim($fromEmail[0], "`");
				
		if (sizeof($techList) > 0 && (!sizeof($techList) == 1 || !$techList[0] == "")) {
//			$techList[] = "";
			echo "<br/><br/>Sending to " . sizeof($techList) . " techs";
			$techList = implode(",", $techList);
			$techList = str_replace("`", "", $techList);
			$message = caspioEscape($_POST["Comments"]);
			$htmlmessage = nl2br($message);
//			smtpMailLogReceived("", $fromEmail, "codem01@gmail.com", "Survey Message", $message, $htmlmessage, "Survey Email");
			echo "<br/><br/>------------------------------<br/><br/>";
			echo htmlentities("From: $fromEmail") . "<br/>";
			echo "To: " . wordwrap($techList, 200, "<br/>", TRUE) . "<br/><br/>";
			echo "Message:<br/>" . $htmlmessage;
		}
		else {
			echo "<br/><br/>No matching techs.";
		}
	}
	
	
	$projectList = caspioSelectAdv("TR_Client_Projects", "Project_Name, Project_ID, Project_Company_ID", "active = '1'", "Project_Name ASC", false, "`", "|");		
	$projectHtml = "<option value=\"None\" selected=\"selected\">Select Project</option>";
	foreach ($projectList as $project) {
		$project = explode("|", $project);
		$name = trim($project[0], "`") . " - " . trim($project[2], "`");
		$id = trim($project[1], "`");
		$projectHtml .= "<option value=\"$id\">$name</option>";
	}

?>

<?php if (isset($_GET["send"])) :?>

<script type="text/javascript">
function saveUserInfo()
{
	if (document.forms.survey.Project.selectedIndex == 0) {
		alert("Please select a project");
		return false;
	}

	var expireDate = new Date;
	expireDate.setMonth(expireDate.getMonth()+6);
		
//	var eCopySelf = (document.forms.survey.copySelf.checked ? "1" : "0");

//	document.cookie = "blast_CopySelf=" + eCopySelf + ";expires=" + expireDate.toGMTString() + ";path=/";
}

function get_cookie(theCookie)
{
	var search = theCookie + "="
	var returnvalue = "";
	if (document.cookie.length > 0)
	{
		offset = document.cookie.indexOf(search);
		// if cookie exists
		if (offset != -1)
		{
			offset += search.length;
			// set index of beginning of value
			end = document.cookie.indexOf(";", offset);
			// set index of end of cookie value
			if (end == -1) end = document.cookie.length;
			returnvalue=unescape(document.cookie.substring(offset, end));
		}
	}
	return returnvalue;
}
</script>

<style  TYPE="text/css">
.label{ font-size:12px; color:#385C7E;}
</style>

<div align="center">
<h1>Bidding Survey Email</h1>
<form id="survey" name="survey" action="<?=$_SERVER['PHP_SELF']?>" method="post">
	<table cellpadding="0" cellspacing="0" border="0" style="border:#385C7E solid medium;">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="5">
					<tr>
						<td class="label">Project</td>
						<td>
							<select id="Project" name="Project">
								<?=$projectHtml?>
							</select>
							<!--&nbsp;<input name="copySelf" type="checkbox" />&nbsp;Copy&nbsp;to&nbsp;Self-->
						</td>
					</tr>
					<tr>
						<td class="label">Comments</td>
						<td>
							<textarea id="Comments" name="Comments" style="width: 30em; height: 10em"><?php echo "http://www.fieldsolutions.com/surveyDownload.php?f=" . urlencode($_GET['send']);?></textarea>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="text-align:center; background-color:#BACBDF; border-top:#385C7E solid medium; padding:5px;">
			<input id="send" name="send" type="submit" value="Send" style="background-color:#385C7E; color:#FFFFFF; font-size:12px; font-weight:bold;" />
			</td>
		</tr>
	</table>
</form>

<script type="text/javascript">
//	document.forms.survey.copySelf.checked = (get_cookie("blast_CopySelf") == "1");
	document.forms.survey.onsubmit = saveUserInfo;
</script>

<?php endif;?>

<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->