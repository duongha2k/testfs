<?php header("Content-Type: text/html; charset=utf-8");?>
<?php $page = admin; ?>
<?php $option = reports; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php 
	checkAdminLogin2();
?>
<!-- Add Content Here -->
<?php
	require_once("../library/caspioAPI.php");

	if (!isset($_POST["search"])) {
		$projectList = caspioSelectAdv("FLS_Projects", "ProjectName, ProjectNo", "active = '1'", "ProjectName ASC", false, "`", "|");		
		$projectHtml = "";
		foreach ($projectList as $project) {
			$project = explode("|", $project);
			$name = trim($project[0], "`");
			$id = trim($project[1], "`");
			$projectHtml .= "<option value=\"$id\" " . ($_POST["Project"] == $id ? "selected=\"selected\"" : "") . ">$name</option>";
		}
	}
	else {
		$dateQuery = (" AND PjctStartDate >= '$today'";
		$eList = caspioSelect("FLS_Work_Orders", "DISTINCT (SELECT ISNULL(FirstName, ''), ISNULL(FirstName, '') FROM TR_Master_List WHERE TR_Master_List.FLSID = FLS_Work_Orders.FLSTechID)", "IsNull(Deactivated, '') = '' AND Kickback = '0' AND ISNULL(FLS_Work_Orders.FLSTechID, '') <> '' AND Project_ID = '$projectID' $dateQuery", "");
	}
?>

<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#startDateGT').calendar({dateFormat: 'MDY-'});
    $('#startDateLT').calendar({dateFormat: 'MDY-'});
  });
</script>

<style  TYPE="text/css">
.label{ font-size:12px; color:#385C7E;}
</style>

<br /><br /><br />

<div align="center">
<h1>Project Email Blast</h1>
<form name="flsWOSummary" method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
<table border="0" cellpadding="10" cellspacing="10">
	<tr>
		<td>

			<table cellpadding="0" cellspacing="0" border="0" style="border:#385C7E solid medium;">
				<tr>
					<td>
						<table cellpadding="0" cellspacing="5">
							<tr>
								<td class="label">
									Start Date (>=)
								</td>
								<td>
									<input id="startDateGT" name="startDateGT" type="text" size="15" />
								</td>
							</tr>
							<tr>
								<td class="label">
									Start Date (<=)
								</td>
								<td>
									<input id="startDateLT" name="startDateLT" type="text" size="15" />
								</td>
							</tr>
							<tr>
								<td class="label">
									Project
								</td>
								<td>
									<select id="Project" name="Project">
										<?=$projectHtml?>
									</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="text-align:center; background-color:#BACBDF; border-top:#385C7E solid medium; padding:5px;">
						<input id="send" name="search" type="submit" value="Search" style="background-color:#385C7E; color:#FFFFFF; font-size:12px; font-weight:bold;" />
					</td>
				</tr>
			</table>

		</td>
	</tr>
</table>
</form>
</div>
<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
