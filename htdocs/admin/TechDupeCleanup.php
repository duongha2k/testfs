<?php
//ini_set("display_errors",1);
require_once("../headerStartSession.php");
require_once("../library/caspioAPI.php");
checkAdminLogin2();

	function escapeCSVField($value) {
		$delimiter = ",";
		$enclosure = "\"";
		if (strpos($value, ' ') !== false ||
			strpos($value, $delimiter) !== false ||
			strpos($value, $enclosure) !== false ||
			strpos($value, "\n") !== false ||
			strpos($value, "\r") !== false ||
			strpos($value, "\t") !== false)
		{
			$str2 = $enclosure;
			$escaped = 0;
			$len = strlen($value);
			for ($i=0;$i<$len;$i++)
			{
				if (!$escaped && $value[$i] == $enclosure)
					$str2 .= $enclosure;
				$str2 .= $value[$i];
			}
			$str2 .= $enclosure;
			$value = $str2;
		}
		return $value;
	}


if (isset($_GET["download"])) {
	// download attachment
	header("Content-disposition: attachment; filename=TechDupeCleanup.csv");
	
	//$records = caspioSelectAdv("TR_Master_List", "TechID, FirstName, LastName, Address1, Address2, City, State, ZipCode, PrimaryPhone, SecondaryPhone, PrimaryEmail, SecondaryEmail, UserName, Password, AcceptTerms, Date_TermsAccepted, W9, FLSID, FLSBadge, NumberRankings, Qty_IMAC_Calls, Qty_FLS_Service_Calls, RegDate", "LastName = 'Bailey'", "LastName", false, "", "|");

	$records = caspioSelectAdv("TR_Master_List", "TechID, FirstName, LastName, Address1, Address2, City, State, ZipCode, PrimaryPhone, SecondaryPhone, PrimaryEmail, SecondaryEmail, UserName, Password, AcceptTerms, Date_TermsAccepted, W9, FLSID, FLSBadge, NumberRankings, Qty_IMAC_Calls, Qty_FLS_Service_Calls, RegDate, BG_Test_Pass_Lite, FLSCSP_Rec, TechSource, PrimPhoneType, SecondPhoneType", "AcceptTerms = 'Yes' AND ISNULL(Deactivated, '') = ''", "LastName", false, "", "|");


	echo "FSTechID,FirstName,LastName,Address1,Address2,City,State,Zip,PrimaryPhone,PrimPhoneType,SecondaryPhone,SecondPhoneType,PrimaryEmail,SecondaryEmail,Username,Password,AcceptTerms,AcceptDate,W9,FLSID,FLSBadge,NumRankings,IMACs,ServCalls,RegDate,BG_Pass_Lite,FLSCS,Source\n";

	 foreach ($records as $tech) {
		//$tech['FirstName'] = escapeCSVField($tech['FirstName']);

 		$tech = str_replace("'","",$tech);
 		$tech = str_replace("NULL","",$tech);
 		$fields = explode("|", $tech);

 
		$techID = escapeCSVField($fields[0]);
		$FirstName = escapeCSVField($fields[1]);
		$LastName = escapeCSVField($fields[2]);
		$Address1 = escapeCSVField($fields[3]);
		$Address2 = escapeCSVField($fields[4]);
		$City =  escapeCSVField($fields[5]);
		$State = escapeCSVField($fields[6]);
		$Zip = escapeCSVField($fields[7]);
		$Phone1 = escapeCSVField($fields[8]);
		$PhoneType1 = escapeCSVField($fields[26]);
		$Phone2 = escapeCSVField($fields[9]);
		$PhoneType2 = escapeCSVField($fields[27]);
		$Email1 = escapeCSVField($fields[10]);
		$Email2 = escapeCSVField($fields[11]);
		$Username = escapeCSVField($fields[12]);
		$Password = escapeCSVField($fields[13]);
		$AgreeTerms = escapeCSVField($fields[14]);
		$AgreeDate = escapeCSVField($fields[15]);
		$W9 = escapeCSVField($fields[16]);
		$FLSID = escapeCSVField($fields[17]);
		$FLSBadge =  escapeCSVField($fields[18]);
		$NumberRankings = escapeCSVField($fields[19]);
		$IMACs = escapeCSVField($fields[20]);
		$ServCalls = escapeCSVField($fields[21]);
		$RegDate = escapeCSVField($fields[22]);
		$BG_Lite = escapeCSVField($fields[23]);
		$FLSCSP_Rec = escapeCSVField($fields[24]);
		$Source = escapeCSVField($fields[25]);
		
		echo "$techID,$FirstName,$LastName,$Address1,$Address2,$City,$State,$Zip,$Phone1,$PhoneType1,$Phone2,$PhoneType2,$Email1,$Email2,$Username,$Password,$AgreeTerms,$AgreeDate,$W9,$FLSID,$FLSBadge,$NumberRankings,$IMACs,$ServCalls,$RegDate,$BG_Lite,$FLSCSP_Rec,$Source\n";
	
	}
	
	die();
	}

?>
 

<?php
// Created on: 3-17-08 
// Author: GB
// Description: Display Tech info to help clean up duplicates.


$page = admin; 
$option = results; 

?>
<?php $page = 'admin'; ?>
<?php $option = 'techs'; ?>
<?php $selected = 'DupeCleanup'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>

<link rel="stylesheet" href="../css/tablesorter.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/jquery-1.2.1.pack.js"></script>
<script type="text/JavaScript" src="../library/jquery/tablesorter/jquery.tablesorter.pack.js"></script>
<!-- Add Content Here -->
 
<br /><br />
<a href="TechDupeCleanup.php?download=1"><img src="../images/downloadIcon.gif" /></a>



<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b0000fdd8fca9479e41548093","https:");}catch(v_e){;}</script>
<div id="cxkg"><a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000fdd8fca9479e41548093">Click here</a> to load this Caspio <a href="http://www.caspio.com" title="Online Database">Online Database</a>.</div>

<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->

