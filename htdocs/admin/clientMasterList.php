<?php
$page = 'admin';
$option = 'client';
$selected = 'clientMasterList';
require ("../header.php");
require ("../navBar.php");
checkAdminLogin2();

?>

<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetClientDetails.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetManager.js"></script>
<script type="text/javascript">
var wManager = new FSWidgetManager();
</script>
<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
<script type="text/javascript" src="/widgets/js/functions.js"></script>
<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
<link rel="stylesheet" href="/widgets/css/popup.css" type="text/css" />
<script type="text/javascript" src="/widgets/js/jquery.formatCurrency-1.4.0.js"></script>
<script type="text/javascript" src="js/clientMasterList.js"></script>


<div align="center">
<br/>
<div id="detailsContainer"></div>
</div>
<?php require ("../footer.php"); ?>

<script type="text/javascript">
    var _clientMasterList = clientMasterList.CreateObject({
        id:"_clientMasterList"
    });
</script>