<?php $page = 'admin'; ?>
<?php $option = 'pricing'; ?>
<?php $selected = 'editPricingField';
require ("../header.php");
require ("../navBar.php");
	require_once("../library/pricing.php");
	if ($_POST) {
		$type = 'Edit';
		Core_Pricing::saveField($_POST[$type . 'FieldID'], $_POST[$type . 'DisplayName']);
	}
?>
<style type="text/css" title="currentStyle">
			@import "/widgets/js/datatables/css/demo_page.css";
			@import "/widgets/js/datatables/css/demo_table.css";
			@import "/widgets/js/datatables/css/datatable_theme.css";
		</style>

<script  src="/widgets/js/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript">
	var edit = '<?=empty($_GET['edit']) ? '' : $_GET['edit']?>';
	
	function validateFields(type) {
		description = $("#" + type + "DisplayName");
		
		if (description.val() == "") {
			description.focus();
			alert("Enter a display name");
			return false;
		}
		return true;
	}

	function validateEditFields() {
		return validateFields("Edit");
	}
		
	function editField(id) {
		if ($("#fieldTable .editRow").length > 0) { document.location.replace('<?=$_SERVER['PHP_SELF']?>edit=' + id); return;}
		columnName = ['EditFieldID', 'EditDisplayName', 'EditActualName'];
		row = $("#fieldID" + id);
		row.children().each(function(index, element) {
			val = table.fnGetData(row.get(0), index);
			switch (index) {
				case 0:
					$(element).html(id + "<input type='hidden' id='" + columnName[index] + "' name='" + columnName[index] + "' value='" + val + "' />");
					break;
				case 2:
					break;
				case 3:
					$(element).html("<a href='javascript:saveField(" + id + ")'>Save</a><br/><a href='javascript:cancelEditField(" + id + ")'>Cancel</a>");
					break;
				default:
					$(element).html("<input type='text' id='" + columnName[index] + "' name='" + columnName[index] + "' value='" + val + "' style='width: 90%' />");
		}
	});
		row.addClass('editRow');
	}

	function saveField(id) {
		if (validateEditFields())
			$("#fieldsForm").submit();
	}

	function cancelEditField(id) {
		document.location.replace('<?=$_SERVER['PHP_SELF']?>');
	}
</script>
<form id="fieldsForm" method="post" action="<?=$_SERVER['PHP_SELF']?>">
<table cellspacing='1' id='fieldsTable' class='tablesorter'><thead><tr><th>ID</th><th>Display Name</th><th>Actual Name</th><th></th></tr></thead><tbody>
<?php
	$fields = Core_Pricing::getFields();
	foreach ($fields as $k=>$field) {
?>
		<tr id="fieldID<?=$k?>">
        	<td><?=$k?></td><td><?=htmlentities($field[0])?></td><td><?=$field[1]?></td><td><a href='javascript:editField(<?=$k?>)'>Edit</a></td>
        </tr>
<?php
	}
?>
</tbody></table>
</form>
<script type="text/javascript">
	var table;
	$(document).ready(function() {
		table = $('#fieldsTable').dataTable({
				"aoColumnDefs": [
					{ "bSortable": false, "aTargets": [ 3 ]},
					{ "bSearchable": false, "aTargets": [ 3 ]},	
				],
				"aaSorting": [[0, 'asc']],
				"bPaginate": false,
				"bFilter": false,
				"fnRowCallback": function(row, data, index, indexFull) { 
						return row; 
				}
		});
		if (edit != '') {
			try {
				editField(edit);
			} catch (e) {}
		}
	});
</script>