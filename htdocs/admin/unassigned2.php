<?php header("Content-Type: text/html; charset=utf-8");?>
<?php $page = 'admin'; ?>
<?php $option = 'reports'; ?>
<?php $selected = 'unassigned'; ?>

<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php 
	require_once("../library/caspioAPI.php");
	checkAdminLogin2();
?>
<!-- Add Content Here -->

<?php

$company_name = isset($_GET["Company_Name"]) ? $_GET["Company_Name"] : "";

if (!isset($_GET["cbResetParam"])) {
	header("Location: https://admin.fieldsolutions.com/admin/unassigned2.php?cbResetParam=1&Company_Name=$company_name");
}

?>
<?php

$results = caspioSelect("Work_Orders", "DISTINCT Company_Name", "", "Company_Name ASC", FALSE);

?>

<form id="companyForm" name="companyForm" action="<?=$_SERVER['PHP_SELF']?>">

<select id="Company_Name" name="Company_Name">
<option value="">All companies</option>
<?php

foreach ($results as $bid) {
		$option = trim($bid, "'");
echo "<option>$option</option>";
}

?>
</select>
<!--
<input type="submit" value="Select Company" />
-->
</form>

<script type="text/javascript">
<!--


$("form#companyForm").submit(function() {
	if ($("select#Company_Name").attr("value") == "All companies") {
		$("select#Company_Name").attr("value","Hello");
		alert($("select#Company_Name").attr("value"));
	}
});

$("select#Company_Name").change(function() {
	$("form#companyForm").submit();
});

//-->
</script>



<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script type="text/javascript" language="javascript">try{f_cbload("193b0000g3d1e5c3d1g8h4i5f3e8","https:");}catch(ilolli){;}</script>
<div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193b0000g3d1e5c3d1g8h4i5f3e8">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>


<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
