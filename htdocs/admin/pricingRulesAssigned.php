<?php $page = "admin"; ?>
<?php $option = "pricing"; ?>
<?php $selected = "pricingRulesAssignedReport"; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<div>
    <h1>Client Level</h1>
    <iframe name="Pricing Rules Assigned - Client Level" style="width: 1000px; height: 500px" src="pricingRuleClient.php">Sorry, but your browser does not support frames.</iframe>
</div>

<div>
    <h1>Project Level</h1>
    <iframe name="Pricing Rules Assigned - Project Level" style="width: 1000px; height: 500px" src="pricingRuleProject.php">Sorry, but your browser does not support frames.</iframe>
</div>

<div>
    <h1>Work Order Level</h1>
    <iframe name="Pricing Rules Assigned - WO Level" style="width: 1000px; height: 500px" src="https://bridge.caspio.net/dp.asp?AppKey=193b0000ab959b0270564aef83fb">Sorry, but your browser does not support frames.</iframe>
</div>
<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->