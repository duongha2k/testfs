<?php
//	if (!isset(argc)) die();
	require_once("../library/caspioAPI.php");
	require_once("../library/smtpMail.php");

	$message = "I have received your W-9.  Your payment(s) will be processed as scheduled.

Thank you,

Accounting
Field Solutions";
		
	smtpMailLogReceived("Field Solutions Accounting", "accounting@fieldsolutions.com", $_GET["email"], "W-9 has been received by Field Solutions", $message, nl2br($message), "W9 Received");
?>
<script type="text/javascript">
	window.close();
</script>