<?php $page = 'admin'; ?>
<?php $option = 'pricing'; ?>
<?php 
	require_once("../headerStartSession.php");
//	ini_set("display_errors", 1);
	require_once("../library/pricing.php");
	if (!isset($_GET["id"]) || !is_numeric($_GET["id"])) die();
	
	$ruleToClone = $_GET["id"];
	
	if (clonePricingRule($ruleToClone))
		echo "Pricing rule has been cloned.";
	else
		echo "An error occurred while cloning rule."
?>
<script type="text/javascript">
	try {
		opener.location.reload();
	}
	catch (e) {}
</script>
<br/><br/>
<input type="button" onclick="window.close()" value="Close Window" />
