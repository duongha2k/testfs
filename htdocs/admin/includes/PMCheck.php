<?php
	require_once($_SERVER['DOCUMENT_ROOT'] . "/library/caspioAPI.php");
	if (!isset($displayPMTools)) {
		$displayPMTools = true;
	}
	
//	$isPM = (bool)Zend_Registry::get('DB')->fetchOne('SELECT ProjectManager FROM clients WHERE UserName=?', array($_SESSION['UserName']));
	$isPM = false;

	if ($displayPMTools) {
		$selectedCompany = $_GET["v"];
		//$companyList = caspioSelectAdv("TR_Client_List", "DISTINCT CompanyName, Company_ID", "ISNULL(Company_ID, '') <> '' AND ShowOnReports = '1'", "CompanyName ASC", false, "`", "|");
		$db = Zend_Registry::get('DB');
		$select = $db->select();
		$select->from(Core_Database::TABLE_CLIENTS, array("CompanyName", "Company_ID"))
			->distinct()
			->where("IFNULL(Company_ID,'') <> ''")
			->where("ShowOnReports = 1")
			->order(array("CompanyName"));
		$companyList = $db->fetchPairs($select);

//		$companyHtml = "<option value=\"\" " . ($_GET["v"] == "" ? "selected=\"selected\"" : "") . ">All Company</option>";
		$companyHtml = "";
		foreach ($companyList as $name => $id) {
			$companyHtml .= "<option value=\"$id\" " . ($_GET["v"] == $id ? "selected=\"selected\"" : "") . ">$name - $id</option>";
		}
?>
<br/>
Select Company:
<select id="PMCompany" onchange="reloadPage(this.value)">
<option value=""> -- all -- </option>
    <?=$companyHtml?>
</select>
<br/>
<?php } ?>
<script type="text/javascript">
	function reloadPage(company) {
		if (company == "BLANKID") {
            url = "./logIn.php";
        } else {
            url = "<?php echo $_SERVER['PHP_SELF']?>?v=" + company;
            if (typeof(wd) == "object" ) {
                url +='&tab='+wd.currentTab;
            }
        }
		document.location.replace(url);
	}
	<?php if (!empty($redirect)) : ?>
			reloadPage("<?php echo $_SESSION["Company_ID"]?>");
	<?php endif;?>
	var isPM = <?php echo ($isPM ? "true" : "false")?>;
</script>
