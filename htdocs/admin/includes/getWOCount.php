<?php
	require_once("../library/caspioAPI.php");

//	$today = date("m/d/Y");
	$dayOfWeek = date("w"); // 0 = Sunday, 6 = Saturday

	$startWeek = date("Y-m-d", strtotime("-$dayOfWeek days")); // Sunday
	$endWeek = date("Y-m-d", strtotime("This Saturday")); // Saturday

	$startMonth = date("Y-m-1");
	$endMonth = date("Y-m-d", strtotime("$startMonth + 1 month - 1 day"));

	$woAllOpen = 0;
	$woThisMonth = 0;
	$woThisWeek = 0;
	$woToday = 0;

	$db = Zend_Registry::get('DB');
	$woAllOpen = $db->fetchOne("SELECT COUNT(*) FROM work_orders WHERE Deactivated=0");
	//$count = caspioSelectAdv("Work_Orders", "count(WO_ID)", "Deactivated = '0'", "", false, "`", "|", false);

	if ($woAllOpen > 0) {
		$woThisMonth = $db->fetchOne("SELECT COUNT(*) FROM work_orders WHERE StartDate >= ? AND StartDate <= ? AND Deactivated=0", array($startMonth, $endMonth));
		//$count = caspioSelectAdv("Work_Orders", "count(WO_ID)", "StartDate >= '$startMonth' AND StartDate <= '$endMonth' AND Deactivated = '0'", "", false, "`", "|", false);
	}

	if ($woThisMonth > 0) {
		$woThisWeek = $db->fetchOne("SELECT COUNT(*) FROM work_orders WHERE StartDate >= ? AND StartDate <= ? AND Deactivated=0", array($startWeek, $endWeek));
		//$count = caspioSelectAdv("Work_Orders", "count(WO_ID)", "StartDate >= '$startWeek' AND StartDate <= '$endWeek' AND Deactivated = '0'", "", false, "`", "|", false);
	}
?>