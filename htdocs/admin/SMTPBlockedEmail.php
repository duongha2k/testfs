<?php 
	header("Content-Type: text/html; charset=utf-8");
	if (!isset($_SERVER['HTTP_REFERER']) || strpos(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST), "fieldsolutions.com") === false) {
		echo "No External Access Allowed";
		die();
	}
	require_once("../headerStartSession.php");
	require_once("../library/mySQL.php");
	if (isset($_GET["download"])) {
		$result = mysqlQuery("SELECT DISTINCT email FROM blockedAddresses WHERE block = '1' AND email NOT LIKE '%postmaster%'");
		header('Content-Disposition: attachment; filename="blockedEmailAddresses.csv"');
		while ($row = mysql_fetch_row($result)) {
			echo $row[0] . "\n";
		}
		die();
	}
	if (isset($_POST["updateBlock"])) {
		$id = $_POST["updateBlock"];
		$v = $_POST["val"];
		mysqlQuery("UPDATE blockedAddresses SET block = '$v', modifiedDate = NOW() WHERE recordID = '$id'");
		die();
	}
?>
<?php $page = 'admin'; ?>
<?php $option = 'reports'; ?>
<?php $selected = 'SMTPBlockedEmail'; ?>

<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php 
	require_once("../library/caspioAPI.php");
	checkAdminLogin2();
?>
<!--- Add Content Here --->

<br />

<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#blockedDateGT').calendar({dateFormat: 'YMD-'});
	$('#blockedDateLT').calendar({dateFormat: 'YMD-'});
    $('#modifiedDateGT').calendar({dateFormat: 'YMD-'});
	$('#modifiedDateLT').calendar({dateFormat: 'YMD-'});
  });

function gotoPage(num) {
	document.forms[1].currPage.value = parseInt(num);
	document.forms[1].submit();
}

function nextPage() {
	gotoPage(parseInt(document.forms[1].currPage.value) + 1);
}

function prevPage() {
	gotoPage(parseInt(document.forms[1].currPage.value) - 1);
}

function sortBy(column) {
	if (document.forms[1].sortBy.value == column)
		document.forms[1].order.value = (document.forms[1].order.value == 1 ? 2 : 1);
	else {
		document.forms[1].sortBy.value = column;
		document.forms[1].order.value = 1;
	}
	document.forms[1].submit();
}

function updateBlock(id) {
	v = (document.getElementById(id).checked ? 1 : 0);
	$("#updating").css({ visibility: "visible" });
	$.post("<?php echo $_SERVER['PHP_SELF']?>", { updateBlock: id, val: v}, 
		function (data, textStatus){
			$("#updating").css({ visibility: "hidden" });
		}
	);
}
</script>

<div id="content" style="position: relative;" align="center">

<h1>Blocked Email List</h1>

<?php 
	$currPage = $_POST["currPage"];
	$resultsPerPage = 25;
	if (!is_numeric($currPage))
		$currPage = 1;
	if (!isset($_POST["search"])) {
?>

<style  TYPE="text/css">
.label{ font-size:12px; color:#385C7E;}
</style>
<a href="<?=$_SERVER['PHP_SELF']?>?download=1">Download List</a><br/><br/>
<form id="emailLog" name="emailLog" action="<?=$_SERVER['PHP_SELF']?>" method="post">
<table cellpadding="0" cellspacing="0" border="0" style="border:#385C7E solid medium;">
	<tr>
<td>
	<table cellpadding="0" cellspacing="5">
		<tr>
			<td class="label">Email Address</td>
			<td><input id="email" name="email" type="text" size="25" /></td>
		</tr>
		<tr>
			<td class="label">Blocked Date (>=)</td>
			<td><input id="blockedDateGT" name="blockedDateGT" type="text" size="25" /></td>
		</tr>
		<tr>
			<td class="label">Blocked Date (<=)</td>
			<td><input id="blockedDateLT" name="blockedDateLT" type="text" size="25" /></td>
		</tr>
		<tr>
			<td class="label">Modified Date (>=)</td>
			<td><input id="modifiedDateGT" name="modifiedDateGT" type="text" size="25" /></td>
		</tr>
		<tr>
			<td class="label">Modified Date (<=)</td>
			<td><input id="modifiedDateLT" name="modifiedDateLT" type="text" size="25" /></td>
		</tr>
	</table>
</td>
	</tr>
	<tr>
<td style="text-align:center; background-color:#BACBDF; border-top:#385C7E solid medium; padding:5px;"><input id="search" name="search" type="submit" value="Search" style="background-color:#385C7E; color:#FFFFFF; font-size:12px; font-weight:bold;" /></td>
	</tr>
     
	<input id="currPage" name="currPage" type="hidden" value="1" />
	<input id="sortBy" name="sortBy" type="hidden" value="" />
	<input id="order" name="order" type="hidden" value="1" />
</form>
</table> 

<?php
	}
	else {
		// hidden search info and display search results
?>	
<div id="updating" style="padding: 0px 1em; background-color: #FF0000; color: #FFFFFF; visibility: hidden;">Updating...</div>
<form id="emailLog" name="emailLog" action="<?=$_SERVER['PHP_SELF']?>" method="post">
	<input id="email" name="email" type="hidden" value="<?php echo $_POST['email']?>" />
	<input id="blockedDateGT" name="blockedDateGT" type="hidden" value="<?php echo $_POST['blockedDateGT']?>" />
	<input id="blockedDateLT" name="blockedDateLT" type="hidden" value="<?php echo $_POST['blockedDateLT']?>" />
	<input id="received" name="received" type="hidden" value="<?php echo $_POST['received']?>" />
	<input id="modifiedDateGT" name="modifiedDateGT" type="hidden" value="<?php echo $_POST['modifiedDateGT']?>" />
	<input id="modifiedDateLT" name="modifiedDateLT" type="hidden" value="<?php echo $_POST['modifiedDateLT']?>" />
	<input id="currPage" name="currPage" type="hidden" value="<?php echo $_POST['currPage']?>" />
	<input id="sortBy" name="sortBy" type="hidden" value="<?php echo mysqlEscape($_POST['sortBy'])?>" />
	<input id="order" name="order" type="hidden" value="<?php echo $_POST['order']?>" />
	<input id="search" name="search" type="hidden" value="search" />
</form>


<?php
		// perform search
		$block = array("return \"<input id='\$rowID' type='checkbox' value='checked' onclick='updateBlock(\$rowID)' />\";", "return \"<input id='\$rowID' type='checkbox' checked='checked' value='checked' onclick='updateBlock(\$rowID)' />\";");
		$criteria = "recordID = recordID ";

		if ($_POST['email'] != "") {
			$esc = mysqlEscape($_POST['email']);
			$criteria .= " AND email LIKE '%$esc%'";
		}

		if ($_POST['blockedDateGT'] != "") {
			$date = date("Y-m-d", strtotime($_POST['blockedDateGT']));
			$criteria .= " AND blockedDate >= '$date 00:00:00'";
		}
		
		if ($_POST['blockedDateLT'] != "") {
			$date = date("Y-m-d", strtotime($_POST['blockedDateLT']));
			$criteria .= " AND blockedDate <= '$date 23:59:59'";
		}
	
		if ($_POST['modifiedDateGT'] != "") {
			$date = date("Y-m-d", strtotime($_POST['modifiedDateGT']));
			$criteria .= " AND modifiedDate >= '$date 00:00:00'";
		}
		
		if ($_POST['modifiedDateLT'] != "") {
			$date = date("Y-m-d", strtotime($_POST['modifiedDateLT']));
			$criteria .= " AND modifiedDate <= '$date 23:59:59'";
		}
		
		$resultsOffset = ($currPage - 1) * $resultsPerPage;
		$sortBy = ($_POST["sortBy"] != "" ? " ORDER BY " . mysqlEscape($_POST["sortBy"]) . ($_POST["order"] == 1 ? " ASC" : " DESC")  : "");
		$search = mysqlQuery("SELECT SQL_CALC_FOUND_ROWS * FROM blockedAddresses " . ($criteria != "" ? "WHERE " . $criteria : "") . "$sortBy LIMIT $resultsOffset, $resultsPerPage");
		$searchCount = mysqlQuery("SELECT FOUND_ROWS()");
		$countArray = mysql_fetch_row($searchCount);
		$count = $countArray[0];
//		echo "SELECT * FROM SMTPEmailLog " . ($criteria != "" ? "WHERE " . $criteria : "") . "$sortBy LIMIT $resultsOffset, $resultsPerPage";
		$pageSelect = "<select id='pageSelect' name='pageSelect' style='width: 4em; text-align: center' onChange='gotoPage(this.selectedIndex + 1);'>";
		$numPages = ceil($count / ($resultsPerPage * 1.0));
		for ($i = 1; $i <= $numPages; $i++) {
			$pageSelect .= "<option value='$i'" . ($currPage == $i ? " selected='selected' " :  "") . ">$i</option>";
		}
		$pageSelect .= "</select>";
		$html = "<style type='text/css'>table { border-spacing: 0px } td { border: solid 1px #444444; padding: 0px 5px; }</style>";
		$row = $search ? mysql_fetch_assoc($search) : false;

		if (!$row) { 
			$html .= "No Records Found";
		}
		else {
			$html .= "<table style='text-align: center'>";
			// display column labels
			$html .= "<tr>";
			foreach ($row as $key => $value) {
				$html .= "<td><a href=\"javascript:sortBy('$key')\">$key</a></td>";
			}		
			$html .= "</tr>";
	
			do {
				$rowID = $row["recordID"];
				$html .= "<tr>";
				foreach ($row as $key => $value) {
					$valueMap = $$key;
					$v = (isset($$key) && isset($valueMap[$value]) ? eval($valueMap[$value]) : $value);				
					$v = (strlen($v) > 75 && strpos($v, "return") != 0 ? wordwrap($v, 75, "<br />", TRUE) : $v);
					$html .= "<td>$v</td>";
				}		
				$html .= "</tr>";
			} while ($row = mysql_fetch_assoc($search));
			$html .= "<tr><td colspan='" . mysql_num_fields($search) . "'>Showing " . ($resultsOffset + 1) . "-" . min($resultsPerPage * $currPage, $count) . " of $count records found</td></tr><tr><td colspan='" . mysql_num_fields($search) . "'>" . ($currPage > 1 ? "<a href='javascript:prevPage();'>&lt;</a>" : "") . " Page: " . $pageSelect . " " . ($currPage < $numPages ? "<a href='javascript:nextPage();'>&gt;</a>" : "") . "</td></tr></table>";
		}
		echo $html;
	}
?>
</div>

<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
