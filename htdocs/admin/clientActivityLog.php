<?php
$page = 'admin';
$option = 'client';
$selected = 'clientActivityLog';
require ("../header.php");
require ("../navBar.php");
checkAdminLogin2();

?>

<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetClientDetails.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetManager.js"></script>
<script type="text/javascript">
var wManager = new FSWidgetManager();
</script>
<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
<script type="text/javascript" src="/widgets/js/functions.js"></script>
<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
<link rel="stylesheet" href="/widgets/css/popup.css" type="text/css" />
<script type="text/javascript" src="/widgets/js/jquery.formatCurrency-1.4.0.js"></script>
<script type="text/javascript" src="/library/jquery/calendar/jquery-calendar.js"></script>
<link rel="stylesheet" href="/library/jquery/calendar/jquery-calendar.css" type="text/css" media="screen"/>
<script type="text/javascript" src="js/clientActivityLog.js"></script>


<div align="center">
<br/>
<div align="center">
    <h1>Client Activity Log</h1>
</div>
<br/>
<div id="detailsContainer"></div>
</div>
<?php require ("../footer.php"); ?>

<script type="text/javascript">
    var _clientActivityLog = clientActivityLog.CreateObject({
        id:"_clientActivityLog"
    });
</script>