<?
$page = 'admin';
$option = 'client';
$selected = 'fstags';

require ("../header.php");
require ("../navBar.php");

checkAdminLogin2();
?>
<script src="/widgets/js/FSWidget.js"></script>
<script src="/widgets/js/FSPopupAbstract.js"></script>
<script src="/widgets/js/FSPopupRoll.js"></script>
<script src="/widgets/js/ajaxfileupload.js"></script>
<script src="/widgets/js/base64.js"></script>
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<?php script_nocache("/admin/js/fstags.js"); ?>

<script>
    function cbLinkHover(obj, css) {
        try {
            obj.style.cssText = css;
        } catch (e) {
        }
    }

    function cbButtonHover(obj, css) {
        try {
            obj.style.cssText = css;
        } catch (e) {
        }
    }

</script>

<?
$db = Zend_Registry::get('DB');
$select = $db->select();
$select->from(Core_Database::TABLE_CLIENTS, array("Company_ID", "CompanyName"))
        ->distinct()
        ->order(array("CompanyName"));
$companyList = $db->fetchPairs($select);
$companyHtml = "";
foreach ($companyList as $id => $name)
{
    if (!empty($_GET["v"]))
    {
        $companyHtml .= "<option value=\"$id\" " . ($_GET["v"] == $id ? "selected=\"selected\"" : "") . " textname=\"$name - $id\">$name - $id</option>";
    } else
    {
        $companyHtml .= "<option value=\"$id\" textname=\"$name - $id\">$name - $id</option>";
    }
}
?>
<style>
    .col1
    {
        text-align: left;
        vertical-align: middle;
        width: auto;
        white-space: nowrap;
        padding: 2px 5px 2px 5px;
        color: #032D5F;
        font-size: 12px;
        font-family: Verdana;
        font-style: normal;
        font-weight: normal;
    }

    .col2
    {
        text-align: left;
        vertical-align: top;
        width: auto;
        padding: 5px 10px 5px 10px;
        white-space: normal;
    }

    .trstyle
    {
        background-color: #FFFFFF;
        padding: 7px;
    }

    .cbxstyle
    {
        color: #000000;
        font-size: 12px;
        font-family: Verdana;
        font-style: normal;
        font-weight: normal;
    }

</style>
<div id="CreateTag" align="center">
    <h1>Create Custom FS-Tags</h1>
    <iframe style="height: 0px;" id="iframecreatefstag" name="iframecreatefstag"></iframe>
    <form  enctype="multipart/form-data"  target="iframecreatefstag" method="post" action="/widgets/admindashboard/client/uploadtag"
           name="frmcreatefstag" id="frmcreatefstag">
        <table cellspacing="0" cellpadding="0" border="0">
            <tbody>
                <tr>
                    <td>
                        <table cellspacing="0" style="border-color: #032D5F; border-width: 2px; border-style: solid; border-collapse: collapse;width: 100%;">
                            <tbody>
                                <tr class="trstyle">
                                    <td class="col1">
                                        <span style="color:red">*</span> Select System Visibility
                                    </td>
                                    <td class="col2">
                                        <select name="cbxSystemVisibility" id="cbxSystemVisibility" onchange="cbxSystemVisibilitychange();">
                                            <option value="1" textname="Selected Client(s) Only">Selected Client(s) Only</option>
                                            <option value="2" textname="All Users">All Users</option>
                                            <option value="3" textname="GPM Only">GPM Only</option>
                                        </select> 
                                    </td>
                                </tr>
                                <tr class="trstyle">
                                    <td class="col1" style="vertical-align:top;">
                                        <span style="color:red">*</span> Select Client(s)
                                    </td>
                                    <td class="col2" style="vertical-align:top:">
                                        <table cellpadding="10" cellspacing="0" width="100%">
                                            <tr>
                                                <td id="tdclient">
                                                    <div id ="Clientdiv0">
                                                        <select class="cbxstyle" name="Client[]">
                                                            <option value="" textname="">Select Client</option>
                                                            <?= $companyHtml ?>
                                                        </select> 
                                                    </div>
                                                </td>
                                                <td style="vertical-align:top;">
                                                    <a href="javascript:;" id="addclient" style="cursor: pointer;">+ Add a Client</a>
                                                    <input type="hidden" id="clientnum" value="1"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <!--13912-->
                                <tr class="trstyle">
                                    <td class="col1">
                                        Client (non-GPM) Edits
                                    </td>
                                    <td class="col2">                                       
                                        <input type="checkbox" id="canNonGPMEdit" name="canNonGPMEdit" value="">
                                    </td>
                                </tr>
                                <tr class="trstyle">
                                    <td class="col1">
                                        GPM Edits
                                    </td>
                                    <td class="col2">                                       
                                        <input type="checkbox" id="canGPMEdit" name="canGPMEdit" checked value="1">
                                    </td>
                                </tr>

                                <tr class="trstyle">
                                    <td class="col1">
                                        <span style="color:red">*</span> Tech Visibility
                                    </td>
                                    <td class="col2">                                       
                                        <div id ="techHidediv0">
                                            <select class="cbxstyle" name="techHide" id="cbxtechhide" onchange="cbxtechhidechange();">
                                                <option value="1" >Yes</option>
                                                <option value="0" >No</option>
                                                <option value="2" >Tagged Techs Only</option>
                                            </select> 
                                        </div>
                                    </td>
                                </tr>
                                <tr class="trstyle">
                                    <td class="col1">
                                        Tech Edits
                                    </td>
                                    <td class="col2">                                       
                                        <input type="checkbox" id="canTechEdit" name="canTechEdit" value="">
                                    </td>
                                </tr>
                                <!--End 13912-->
                                <tr class="trstyle">
                                    <td class="col1">
                                        <span style="color:red">*</span> Enter Tag Title
                                    </td>
                                    <td class="col2">
                                        <input type="text" id="tagtitle" name="tagtitle">&nbsp;<img class="viewTaglist" alt="" title="View tag list" src="/widgets/images/magnify.gif" style="cursor:pointer;">
                                        <input type="hidden" id="TagName" name="TagName" value="">
                                        <input type="hidden" id="TagSource" name="TagSource" value="">
                                    </td>
                                </tr>
                                <!-- 13931 -->
                                <tr class="trstyle">
                                    <td class="col1">
                                        &nbsp;&nbsp; Multiple Tag Levels
                                    </td>
                                    <td class="col2">
                                        <input type="checkbox" name="multipletaglevels" id="multipletaglevels" value="1">
                                        <input type="hidden" name="hidNumberItem" id="hidNumberItem" value="3">
                                    </td>
                                </tr>
                                <tr class="trstyle trmultipletaglevelschecked">
                                    <td colspan="2">
                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="tbmultipletaglevels">
                                            <tbody></tbody>
                                        </table>
                                    </td>
                                </tr>
                                <!-- End 13931 -->
                                <tr class="trstyle trmultipletaglevelsunchecked" >
                                    <td class="col1">
                                        <span style="color:red">*</span> Upload Tag Art (PNG files only)<br/>
                                        <div style="text-align:right;">(Max height: 26px)</div><!--829-->
                                    </td>
                                    <td class="col2">
                                        <input type="file" id="TagArt" name="TagArt" accept="image/png">
                                    </td>
                                </tr>
                                <tr style="background-color: #FFFFFF;">
                                    <td style="padding: 5px 0px 5px 0px; text-align: center; vertical-align: middle; border-color: #032D5F; border-width: 2px; border-style: solid; background-color: #5091CB;" colspan="2">
                                        <input type="submit" id="cmdSubmit" value="Submit" onmouseout="cbButtonHover(this, 'color: #ffffff; font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold; text-align: center; vertical-align: middle; border-color: #5091CB; border-style: solid; border-width: 1px; background-color: #032D5F;  width: auto; height: auto; margin: 0 3px;');" onmouseover="cbButtonHover(this, 'color: #FFFFFF; font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold; text-align: center; vertical-align: middle; border-style:solid; border-width: 1px; border-color: #032D5F; background-color: #083194;  width: auto; height: auto; margin: 0 3px;');" style="color: rgb(255, 255, 255); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold; text-align: center; vertical-align: middle; border-color: rgb(80, 145, 203); border-style: solid; border-width: 1px; background-color: rgb(3, 45, 95); width: auto; height: auto; margin: 0pt 3px;">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
<div id="tagsexist" style="display:none;width:500px;height:200px;">
    <div style="height:300px;">
        <div style="text-align: center;"><b>Existing Tag List</b></div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td style="color:white;text-align:center;background-color:#5091cb;">Select Tag Name</td>
                <td style="color:white;text-align:center;background-color:#5091cb;">Tag Title</td>
                <td style="color:white;text-align:center;background-color:#5091cb;">Tag Source</td>
            </tr>

            <?
            $FSTagClass = new Core_Api_FSTagClass();
            $ExistingTagList = $FSTagClass->getExistingTagList();
            if (!empty($ExistingTagList))
            {
                foreach ($ExistingTagList as $ExistingTag)
                {
                    ?>
                    <tr>
                        <td><a class="selecttag" href="javascript:;" TagName="<?= $ExistingTag['TagName'] ?>" TagTitle="<?= $ExistingTag['TagTitle'] ?>"  TagSource="<?= $ExistingTag['TagSource'] ?>"><?= $ExistingTag['TagName'] ?></a></td>
                        <td><?= $ExistingTag['TagTitle'] ?></td>
                        <td><?= $ExistingTag['TagSource'] ?></td>
                    </tr>
                    <?
                }
            }
            ?>
        </table>
    </div>
</div>
<div id="divEditTag" style="display: none;"></div>
<script>
    jQuery(window).ready(function() {
        //13931
        _fstags = fstags.CreateObject({
            id: '_fstags',
            chkmultipletaglevels: '#multipletaglevels',
            hidNumberItem: '#hidNumberItem'

        });
//        _fstags.initPage();
        _fstags.BindEventSubLevel();
        //End 13931

        $(".viewTaglist").unbind('click', showTaglistInfo).bind('click', showTaglistInfo);
        $("#addclient").unbind('click', addclient_click).bind('click', addclient_click);
        $("#tagtitle").unbind('change', isDuplicateTag).bind('change', isDuplicateTag);

        $('#TagArt').change(function() {
            var file = this.files[0];
            if (file.type != "image/png")
            {
                alert("Tag Art must be *.png only");
                $('#TagArt').val("");
            }
        });


        jQuery('#cmdSubmit').click(function(event) {
            //624
            var Client_id = "";
            var Client_no = $("#clientnum").val();
            var Visibility = $("#cbxSystemVisibility").val();
            //end 624

            var errmsg = "";
            var el = this;
            //624
            //912
            if (Visibility == 2 || Visibility == 3)
            {
                Client_id = "yes"
            }
            else if (Client_no >= 1)
            {
                for (i = 0; i < Client_no; i++)
                {
                    if ($("#Clientdiv" + (i) + " .cbxstyle").val() != "")
                    {
                        Client_id = "yes"
                    }
                }
            }
            //end 912
            //end 624

            if (Client_id == "")
            {
                errmsg += 'Please choose company.\n';
            }
            //931
            var tagtitle = "";
            var TagArt = "";
            tagtitle = $("#tagtitle").val();
            
            if (tagtitle == "")
            {
                errmsg += 'Please enter Tag title.\n';
            }
           
            if ($("#multipletaglevels").is(":checked"))
            {
                var numberitem = parseInt($("#hidNumberItem").val());
                for(var i=1; i<=numberitem; i++)
                {
                    tagtitle = $("#tagTitleLevel_"+i).val();
                    TagArt = $("#TagArt_"+i).val();   
                    if (tagtitle == "")
                    {
                        errmsg += 'Please enter Tag level '+i+' title .\n';
                    }
                    if (TagArt == "")
                    {
                        errmsg += 'Please choose Tag  level '+i+' Art.\n';
                    }
                }
                
            }
            else
            {
                TagArt = $("#TagArt").val();
                
                if (TagArt == "")
                {
                    errmsg += 'Please choose Tag Art.\n';
                }
            }
            //end 931

            if (errmsg != "")
            {
                alert(errmsg);
                return false;
            }
        });
    });

    function addclient_click()
    {
        var clientnum = parseInt($("#clientnum").val());
        $("#clientnum").val(clientnum + 1);

        var newclient = $("#Clientdiv0").html();
        //624
        newclient = '<div id ="Clientdiv' + (clientnum) + '">' + newclient + '</div>';
        $("#tdclient").append(newclient);
        $("#Clientdiv" + (clientnum) + " .cbxstyle").val(0);
        //end 624
    }

    function cbxSystemVisibilitychange()
    {
        if ($("#frmcreatefstag").find("#cbxSystemVisibility").val() == 2)
        {
            disableclient();
            $("#frmcreatefstag").find("#canNonGPMEdit").attr("disabled", ""); //13912   
        }
        //13912
        else if ($("#frmcreatefstag").find("#cbxSystemVisibility").val() == 3)
        {
            disableclient();
            $("#frmcreatefstag").find("#canNonGPMEdit").removeAttr("checked");
            $("#frmcreatefstag").find("#canNonGPMEdit").attr("disabled", "disabled");
        }
        else
        {
            $("#frmcreatefstag").find("#canNonGPMEdit").attr("disabled", "");
            $("#frmcreatefstag").find("#addclient").bind('click', addclient_click);
            $("#frmcreatefstag").find("#Clientdiv0 .cbxstyle").attr("disabled", "");
        }
        //End 13912
    }
    //912
    function disableclient()
    {
        var clientnum = $("#clientnum").val();
        $("#frmcreatefstag").find("#Clientdiv0 .cbxstyle").attr("disabled", "disabled");

        var i = 0;
        for (i = 1; i <= clientnum; i++)
        {
            $("#frmcreatefstag").find("#Clientdiv" + i).remove();
        }
        $("#clientnum").val(1);
        $("#frmcreatefstag").find("#addclient").unbind('click');
    }
    function cbxtechhidechange()
    {

        if ($("#cbxtechhide option:[selected]").val() == 0)
        {
            $("#canTechEdit").removeAttr("checked");
            $("#canTechEdit").attr("disabled", "disabled");
        }
        else
        {
            $("#canTechEdit").removeAttr("disabled");
        }
    }
    //end 912
    //931
    function callPopupSubmit(imgarr)
    {
        var Visibility = $("#frmcreatefstag").find("#cbxSystemVisibility option:selected").attr('textname');
        var VisibilityType = $("#frmcreatefstag").find("#cbxSystemVisibility").val();
        var Client = $("#frmcreatefstag").find(".cbxstyle option:selected").attr('textname');
        var title = $("#tagtitle").val();
        var content = "\
<div style='height:10ppx;width:300px;'>\n\
    <div style='text-align:center;padding-top:5px;'><b>Confirm FS-Tag Creation</b></div>"
        if (VisibilityType != 2)
        {
            content += "<div style='text-align:left;padding-top:5px;padding-left:5px;'>Client: " + Client + "</div>";
        }
        content += "\
    <div style='text-align:left;padding-top:5px;padding-left:5px;'>Tag Title: " + title + "</div>\n\
    <div style='text-align:left;padding-top:5px;padding-left:5px;'>Visibility: " + Visibility + "</div>\n\
    <div style='text-align:left;padding-top:5px;padding-left:5px;'>Tag Art:</div>\n\
    <table class='resultsTable'>\n\
        <tr>\n\
            <td style='color:white;text-align:center;background-color:#5091cb;'>Total<br>Work Orders</td>\n\
            <td style='color:white;text-align:center;background-color:#5091cb;'>No&nbsp;Shows<hr style='color: white;background: none repeat scroll 0 0 white;margin-bottom: 0px;height:1px;'>Back Outs</td>\n\
            <td style='color:white;text-align:center;background-color:#5091cb;'>FS-Tags&trade;</td>\n\
            <td style='color:white;text-align:center;background-color:#5091cb;'>Background<hr style='color: white;background: none repeat scroll 0 0 white;margin-bottom: 0px;height:1px;'>Drug Test</td>\n\
        </tr>\n\
        <tr class='preferred'>\n\
            <td style='text-align:center;'>435</td>\n\
            <td style='text-align:center;'><span>0</span><hr style='background-color:#DDD;height:1px;'><span>0</span></td>\n\
            <td style='min-width: 60px'>\n\
                <table cellspacing='2' cellpadding='2'>\n\
                    <tbody>\n\
                        <tr style='padding-bottom:2px;'>\n\
                            <td><img src='/widgets/images/dellMRAPassed.png'></td>\n\
                            <td><img src='/widgets/images/VonagePlusTag.png'></td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td colspan='2'><hr style='background-color:#DDD;height:1px;'></td>\n\
                        </tr>";
        //931
        var imgarrlist = imgarr.split('|');
        content += "<td><img style='height:24px' src='/widgets/images/fs_mobile_26x23.png'></td>";  
        content += "<td><img style='height:24px' src='https://s3.amazonaws.com/wos-docs/tags/" + imgarrlist[0] + "'></td>";  
        content += "</tr>";
        //end 931
        content += "</tbody>\n\
                </table>\n\
            </td>\n\
            <td><span style='width:50px;'><img src='/widgets/images/check_green.gif'><img style='margin-left: 2px;' src='/widgets/images/check_green.gif'></span><hr style='color: white;background: none repeat scroll 0 0 white;margin-bottom: 0px;'><span><img src='/widgets/images/check_green.gif'></span></td>\n\
        </tr>\n\
        <tr>\n\
            <td colspan='4' style='color:white;text-align:center;background-color:#5091cb;'>&nbsp;</td>\n\
        </tr>\n\
    </table>\n\
    <div style='text-align:center;'><input style=\"background: url('/widgets/images/button_popup.png') no-repeat scroll left top transparent; border: medium none; color: #385C7E; cursor: pointer; font: 10px/10px Verdana,Arial,Helvetica,sans-serif; padding: 4px 0 8px; width: 64px;\" type='button' class='submitdata' value='Submit' fname='" + imgarr + "'><input style=\"background: url('/widgets/images/button_popup.png') no-repeat scroll left top transparent; border: medium none; color: #385C7E; cursor: pointer; font: 10px/10px Verdana,Arial,Helvetica,sans-serif; padding: 4px 0 8px; width: 64px;\" type='button' class='canceldata' value='Cancel'></div>\n\
</div>";
        $("<div></div>").fancybox({'autoDimensions': true, 'showCloseButton': true, 'hideOnOverlayClick': false, 'enableEscapeButton': false, 'content': content}).trigger('click');

        $(".submitdata").unbind('click', sumitdata).bind('click', sumitdata);
        $(".canceldata").unbind('click', canceldata).bind('click', canceldata);
    }
     //end 931
    function canceldata()
    {
        $.fancybox.close();
    }

    function sumitdata()
    {
        var dataForm = jQuery("#frmcreatefstag").serialize();
        dataForm += "&fname=" + $(".submitdata").attr("fname");
        $.ajax({
            url: '/widgets/admindashboard/client/createfstag/',
            dataType: 'json',
            data: dataForm,
            async: false,
            type: 'post',
            success: function(data, status, xhr) {
                if (data.success)
                    window.location = 'fstagsList.php?type=Client&search=1';
                else
                {
                    alert(data.err);
                }
            }
        });
    }

    function showTaglistInfo()
    {
        var content = $("#tagsexist").html();
        $("<div></div>").fancybox(
                {
                    'autoDimensions': true,
                    'showCloseButton': true,
                    'width': 1010,
                    'height': 800,
                    'hideOnOverlayClick': false,
                    'enableEscapeButton': false,
                    'content': content
                }).trigger('click');

        $(".selecttag").unbind('click', fillinTagInfo).bind('click', fillinTagInfo);

    }

    function fillinTagInfo()
    {
        var TagName = $(this).attr("TagName");
        var TagTitle = $(this).attr("TagTitle");
        var TagSource = $(this).attr("TagSource");

        var dataForm = "TagName=" + TagName;
        dataForm += "&TagTitle=" + TagTitle;
        dataForm += "&TagSource=" + TagSource;
        $.ajax({
            url: '/widgets/admindashboard/client/isduplicatetagname/',
            dataType: 'json',
            data: dataForm,
            async: false,
            type: 'post',
            success: function(data, status, xhr) {
                if (data.duplicate == 1)
                {
                    var dataForm = "tagtitle=" + TagTitle;
                    dataForm += "&tagfrom=1";
                    $.ajax({
                        url: "/widgets/admindashboard/client/edittag/",
                        dataType: 'html',
                        data: dataForm,
                        async: false,
                        type: 'post',
                        success: function(data, status, xhr) {
                            $.fancybox.close();
                            $("#divEditTag").html(data);
                            $("#divEditTag").css("display", "");
                            $("#CreateTag").css("display", "none");

                        }
                    });
                }
            }
        });
    }

    function edittag()
    {
        var dataForm = "tagtitle=" + $(".edittag").attr("tagtitle");
        dataForm += "&tagfrom=1";
        $.ajax({
            url: "/widgets/admindashboard/client/edittag/",
            dataType: 'html',
            data: dataForm,
            async: false,
            type: 'post',
            success: function(data, status, xhr) {
                $.fancybox.close();
                $("#divEditTag").html(data);
                $("#divEditTag").css("display", "");
                $("#CreateTag").css("display", "none");

            }
        });
    }

    function canceledittag()
    {
        $("#tagtitle").val("");
        $("#tagtitle").focus();
        $.fancybox.close();
    }
    function isDuplicateTag()
    {
        var tagtitle = $(this).val();
        var dataForm = "tagtitle=" + tagtitle;
        $.ajax({
            url: '/widgets/admindashboard/client/isduplicatetag/',
            dataType: 'json',
            data: dataForm,
            async: false,
            type: 'post',
            success: function(data, status, xhr) {
                if (data.success)
                {
                    if (data.duplicate == 1)
                    {
                        var content = "\
<div style='height:10ppx;width:300px;'>\n\
    <div style='text-align:left;padding-top:5px;'>This FS-Tag already exists. Would you like to edit this FS-Tag?</div>\n\
    <div style='text-align:center;'><input style=\"background: url('/widgets/images/button_popup.png') no-repeat scroll left top transparent; border: medium none; color: #385C7E; cursor: pointer; font: 10px/10px Verdana,Arial,Helvetica,sans-serif; padding: 4px 0 8px; width: 64px;\" type='button' class='edittag' value='Edit' tagtitle='" + tagtitle + "'><input style=\"background: url('/widgets/images/button_popup.png') no-repeat scroll left top transparent; border: medium none; color: #385C7E; cursor: pointer; font: 10px/10px Verdana,Arial,Helvetica,sans-serif; padding: 4px 0 8px; width: 64px;\" type='button' class='canceledittag' value='Cancel'></div>\n\
</div>";
                        $("<div></div>").fancybox({'autoDimensions': true, 'showCloseButton': true, 'hideOnOverlayClick': false, 'enableEscapeButton': false, 'content': content}).trigger('click');

                        $(".edittag").unbind('click', edittag).bind('click', edittag);
                        $(".canceledittag").unbind('click', canceledittag).bind('click', canceledittag);
                    }
                }
            }
        });
    }

</script>
<?php require ("../footer.php"); ?>

