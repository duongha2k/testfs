<?php $page = admin; ?>
<?php $option = clients; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->

<br /><br />

<div align="center">

<ul id="defaultMenu" style="text-align:center">
<li><a href="/admin/IVR_Log.php">IVR Log</a></li>
<li><a href="/importParser.php">Work Order Import Parser</a></li>
<li><a href="/availableWOGeocode.php">Geocode Available WOs</a></li>
<li><a href="projectWOSearch.php?t=UpdateInfo">Update Work Order's Tech Information</a></li>
<li><a href="/flsAfterImport.php">FLS After Import Script</a></li>
<li><hr /></li>
<li><a href="importFLSBadge.php">Import FLS Badge</a></li>
<li><hr /></li>
<li><a href="/admin/CMS_AddContent.php">Add CMS Content</a></li>
<li><a href="/admin/CMS_UpdateContent.php">Update CMS Content</a></li>
</ul>

</div>

<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->