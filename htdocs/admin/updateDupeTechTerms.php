<?php $page = admin; ?>
<?php $option = techs; ?>
<?php $selected = "updateW9"; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->

<?php
	require_once("../library/caspioAPI.php");

	checkAdminLogin2();
?>

<style type="text/css">
</style>

<br/><br/>

<div style="text-align: center">
<form name="uploadImport" method="post" action="<?php echo $_SERVER['PHP_SELF']?>" enctype="multipart/form-data">
	<label for="importFile">List of Techs in CSV format:</label>
	<input name="importFile" type="file" size="40"/>
	<input name="ignoreWarning" type="checkbox" value="1" /> Ignore warnings<br/><br/>
	<input name="submit" type="submit" value="Submit" />
</form>


<br/>
<?php

require_once("../library/caspioAPI.php");

	ini_set("display_errors", 1);


	// has file been uploaded?
	if (isset($_POST["submit"])) {		
		$requiredColumns = array("TechID");
		
		if (!isset($_FILES["importFile"]["tmp_name"]) || !file_exists($_FILES["importFile"]["tmp_name"])) {
			echo "No file Uploaded";
			die();
		}

		// is file too large?
		if ($_FILES["importFile"]["size"] > 150000) {
			// file too large
		}
		
		
		echo "<p>Checking uploaded file ... </p>";
		
					
		$handle = fopen($_FILES["importFile"]["tmp_name"], "r");
		$importColumns = fgetcsv($handle);
		
		$sizeImport = sizeof($importColumns);
		$sizeRequired = sizeof($requiredColumns);
		if ($sizeImport != $sizeRequired) {
			echo "Unexpected number of columns found - expecting $sizeRequired but found $sizeImport";
			die();
		}
		
		$index = 0;
		foreach ($importColumns as $col) {
			if ($col != $requiredColumns[$index]) {
				echo "Unexpected column found - expecting '{$requiredColumns[$index]}' but found '$col'";
				die();
			}
			$index++;
		}
		$updateQuery = "";
		
		$TechIDList = array();
		$TechIDFoundMap = array();
		$updateQuery = array();
		
		while (!feof($handle)) {
			$importRow = fgetcsv($handle);
			if (sizeof($importRow) == 1 && $importRow[0] == '') continue;
			$TechID = $importRow[0];
			$TechIDList[] = "'$TechID'";			

			if (!is_numeric($TechID)) {
				echo "TechID is not a number: $TechID";
				die();
			}
		}

		if (sizeof($TechIDList) == 0) {
			echo "No data found.";
			die();
		}
		
		// https://admin.fieldsolutions.com/admin/updateDupeTechTerms.php
		
		// Look for possible orphaned work orders
		/*foreach ($TechIDList as $row) {
			$row = explode(",", $row);
			$techID = trim($row[0], "'");
			
			echo "$techID - Related Work Orders (Main Site): ";
			
			$WorkOrders = caspioSelect("Work_Orders", "WO_ID", "Tech_ID = $techID", "", false);
			$FoundWorkOrders = sizeof($WorkOrders);
			echo "$FoundWorkOrders <br />";

			/*foreach ($WorkOrders as $row){
				echo "$row <br />";
			}
		}*/
			
					
		$numDeletedRows = sizeof($TechIDList);
		$TechIDList = implode(",", $TechIDList);
		
		echo "Size of array: $numDeletedRows <br />";
		
		print_r(caspioUpdate("TR_Master_List", "AcceptTerms", "'No'", "TechID IN ($TechIDList)", false));
		
		echo "<br/>Turning off Accept Terms on duplicate techs Complete.<br/>";		
	}
?>
</div>
<?php require ("../footer.php"); ?>
