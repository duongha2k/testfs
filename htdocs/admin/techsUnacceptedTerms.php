<?php header("Content-Type: text/html; charset=utf-8");?>
<?php $page = techs; ?>
<?php $option = wos; ?>
<?php $selected = 'wosViewApplied'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php
	require_once("../library/caspioAPI.php");
	checkAdminLogin2();
?>



<br/>
<div align="center">

<script type="text/javascript">
<!--
	
	function sortBy(num) {
		frm = document.getElementById("sortForm");
		frm.sortDir.value = (frm.sortCol.value == num ? (frm.sortDir.value == 0 ? 1 : 0) : 0);
		frm.sortCol.value = num;
		frm.submit();
	}
//-->
</script>
</div>


<div align="center">
<h1>Technicians To Accept Terms</h1>
<?php
{
?>
<form id="sortForm" name="sortForm" action="<?=$_SERVER['PHP_SELF']?>" method="post">
	<input name="sortCol" type="hidden" value="<?php echo $_POST["sortCol"] ?>" />
	<input name="sortDir" type="hidden" value="<?php echo $_POST["sortDir"] ?>" />
</form>
<?php
	$sort = isset($_POST["sortCol"]) ? $_POST["sortCol"] : 0;
	$dir = isset($_POST["sortDir"]) ? $_POST["sortDir"] : 0;
	$dir = $_POST["sortDir"];
	switch ($sort) {
		case 0: 
			$sortQuery = "ID";
			break;
		case 1: 
			$sortQuery = "FN";
			break;
		case 2:
			$sortQuery = "LN";
			break;
		case 3: 
			$sortQuery = "CID";
			break;
		case 4: 
			$sortQuery = "CM";
			break;
		default:
			$sortQuery = "LN";
	}
	
	if ($sortQuery != "") {
		$sortQuery = "ORDER BY $sortQuery " . ($dir == 0 ? "ASC" : "DESC");
	}

// SELECT TechID, TechFName, TechLName FROM TR_Master_List WHERE FLSDeactivated = '1' UNION SELECT TechID, TL.TechFName, TL.TechLName FROM Deactivated_Techs LEFT JOIN TR_Master_List AS TL ON DeactivatedTechs.TechID = TL.TechID

/*
TR_Master_List.AcceptTerms = "Yes" AND
TR_Master_List.Date_TermsAccepted <= '03/03/2008' AND
Work_Orders.StartDate >= '03/18/2008' OR
FLS_Work_Orders.PjctStartDate >= '03/18/2008'
*/

$results = caspioSelectAdv("TR_Master_List", "ID=TechID, FN=FirstName, LN=LastName", "AcceptTerms = 'Yes' AND Date_TermsAccepted <= '03/03/2008'", "", FALSE, "`", "|");

//$results = caspioSelectAdv("TR_Master_List", "ID=TechID, FN=FirstName, LN=LastName, CM=FLScomments, CID='FLS'", "FLSstatus = 'Admin Denied' UNION ALL SELECT TL.TechID, TL.FirstName, TL.LastName, Deactivated_Techs_By_Clients.Comments, Deactivated_Techs_By_Clients.Company_ID FROM Deactivated_Techs_By_Clients LEFT JOIN TR_Master_List AS TL ON Deactivated_Techs_By_Clients.TechID = TL.TechID $sortQuery", "", FALSE, "`", "|");

	
	$numResults = sizeof($results);
	if ($numResults == 1 && $results[0] == "") {
		echo "No Records Found.";
	}
	else if ($numResults <= 250) {
?>	
		<table cellpadding='10' class="resultTable">
			<tr class="resultTop"><td colspan="9">&nbsp;</td></tr>
			<tr class="resultHeader">
				<td class="leftHeader"><a href="javascript:sortBy(0)">TechID</a></td>
                <td><a href="javascript:sortBy(1)">FirstName</a></td	>
				<td class="rightHeader"><a href="javascript:sortBy(2)">LastName</a></td>
			</tr>

<?php
	foreach ($results as $bid) {
		$parts = explode("|",$bid);
		$techID = trim($parts[0], "`");
		$firstName = trim($parts[1], "`");
		$lastName = trim($parts[2], "`");
		
		echo "<tr class='resultRow'><td class=\"leftRow\">$techID</td><td>$firstName</td><td class=\"rightRow\">$lastName</td></tr>";
	}	
?>
			<tr>
				<td class="resultBottom" colspan="9">Results 1 - <?=$numResults?></td>
			</tr>
		</table>
<?php
	}
	else {
		echo "Too many records returned.<br/>Please modify your search criteria to return fewer  records.<br/>Click <a href=\"wosViewAppliedAll.php?v=$techID\">here</a> to return to the search page.";
	}
}
?>
</div>


<style  TYPE="text/css">
.label{ font-size:12px; color:#385C7E;}

.resultTable {
	border: 1px solid black;
	border-spacing: 0;
	text-align: center;
	color: #000000;
	padding: 0;
	border-collapse: collapse;
}

.resultTop {
	background-color: #949CCE;
	border-top: 1px solid #000000;
	border-left: 1px solid #000000;
	border-right: 1px solid #000000;
}

.leftHeader {
	border-left: 2px solid #2A497D;
}

.rightHeader {
	border-right: 2px solid #2A497D;
}

.middleHeader {
	border-left: 1px solid #FFFFFF;
	border-right: 1px solid #FFFFFF;
}

.resultHeader a, .resultRow a:hover {
	color: #2A497D;
}

.resultHeader a:hover, .resultRow a {
	color: #6373B5;
}

.resultHeader td {
	color: #000099;
	text-align: center; 
	background-color: #A0A8AA; 
	padding: 7px 10px;
	font-weight: bold;
	cursor: pointer;
	border-top: 2px solid #2A497D;
}

.resultRow {
	background-color: #FFFFFF;
	border: 1px solid #EAEFF5;
}

.resultRow:hover {
	background-color: #EAEFF5;
}

.leftRow {
	border-left: 2px solid #2A497D;
}

.rightRow {
	border-right: 2px solid #2A497D;
}

.resultRow td {
	padding: 2px 10px;
}

.resultBottom {
	background-color: #A0A8AA;
	border: 2px solid #2A497D;
}
</style>

<!--- End Content --->
<?php require ("../footer.php"); ?>

