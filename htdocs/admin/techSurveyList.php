<?php $page = 'admin'; ?>
<?php $option = 'reports'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php 
//	ini_set("display_errors", 1);
	$overwritten = false;
	$uploads_dir = "{$_SERVER['DOCUMENT_ROOT']}/surveys";
	if (isset($_POST["submit"])) {
		$tmp_name = $_FILES["upload"]["tmp_name"];
		if ($_FILES["upload"]["error"] == UPLOAD_ERR_OK && is_uploaded_file($tmp_name)) {
			$name = $_FILES["upload"]["name"];
//			echo "$uploads_dir --- $tmp_name -- $name";
			if (file_exists($name))
				$overwritten = true;
			$write = fopen("$uploads_dir/$name", "w");
			$contents = file_get_contents($tmp_name);
			fwrite($write, $contents);
			fclose($write);			
		}
	}
	
	if (isset($_GET["delete"])) {
		// delete file
		$file = base64_decode($_GET["delete"]);
		$file = "$uploads_dir/$file";
		if (is_file($file))
			unlink($file);
	}
	
	$fileList = array();
	if ($handle = opendir($uploads_dir)) {
		while (false !== ($file = readdir($handle))) {
			if ($file == "." || $file == "..") continue;
			$fileList[] = $file;
		}	
		closedir($handle);
	}
?>
<style type="text/css">
	#surveyList td {
		border: none;
		padding: 0px 10px;		
	}
	#surveyList thead {
		background-color: #000099;
		color: #FFFFFF;
		font-weight: bold;		
	}
</style>
<br/><br/>
<div align="center">
    <form name="surveyUpload" method="post" action="<?=$_SERVER['PHP_SELF']?>" enctype="multipart/form-data">
    	Upload a new survey:&nbsp;&nbsp;
        <input name="upload" type="file" />
        <input name="submit" type="submit" value="Upload" />
    </form>
    <br/><br/>
    <h2>Survey Files</h2><br/><br/>
    <table id="surveyList" cellpadding="0" cellspacing="0">
    	<thead>
            <tr>
                <td>Survey File</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
		</thead>
		<tbody>
<?php
		if (sizeof($fileList) == 0) {
?>
			<tr>
            	<td colspan="3">No surveys found</td>
            </tr>
<?php
		}
        foreach ($fileList as $fileName) {
			$base64Name = urlencode(base64_encode($fileName));
?>
			<tr>
            	<td><?=htmlentities($fileName)?></td>
                <td><a href="surveyEmailBlast.php?send=<?=$base64Name?>" target="_blank">Send</a></td>
                <td><a href="<?=$_SERVER['PHP_SELF']?>?delete=<?=$base64Name?>">Delete</a></td>
            </tr>
<?php
		}
?>
		</tbody>
	</table>
</div>
<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
