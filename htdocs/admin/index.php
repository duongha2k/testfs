<?php $page = 'adminlogin'; ?>
<?php $option = 'adminlogin'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->
<script type="text/javascript">
$(document).ready(function () {
	
	$('#authLoginForm').submit(function(event) {
		event.preventDefault();
		
	  	var formData = $(this).serializeArray();
	   
	   var idx;
	   var query = "";
	   
	   for ( idx in formData ) {
           if ( formData[idx].value ) {
               query += '&' + formData[idx].name + '=' + encodeURIComponent(formData[idx].value).replace(/\~/g, '%7E').replace(/\!/g, '%21').replace(/\'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29');
           }
       }

	    l  = location.protocol + '//' + location.host;
	    l += '/admin/setAdminLoginSession.php?v=true';
        l += query;
	   
	    location = l; 
	});

	var reqLogin = '<?=$_REQUEST['login']?>';
	
	if(reqLogin == "false"){
		$("#authFailed").show();
	}else{
		$("#authFailed").hide();
	}
	
});
</script>

<style type="text/css">
table#authLogin{
	border: 2px solid rgb(56, 92, 126); 
	border-collapse: collapse;
}
table#authLogin tr{
	background-color: rgb(255, 255, 255); 
	padding: 7px;
}
table#authLogin td{
	text-align: left; 
	vertical-align: top; 
	width: auto; 
	white-space: nowrap; 
	padding: 2px 5px; 
	color: rgb(56, 92, 126); 
	font-size: 12px; 
	font-family: Verdana; 
	font-style: normal; 
	font-weight: normal;
}

.authFormInput{
	color: rgb(0, 0, 0); 
	font-size: 12px; 
	font-family: Verdana; 
	font-style: normal; 
	font-weight: normal; 
	border: 1px solid rgb(0, 0, 0); 
	padding: 1px;
}

#authFailed{
	color: rgb(255, 0, 0); 
	font-size: 12px; 
	font-family: Verdana; 
	font-style: normal; 
	font-weight: bold; 
	text-align: left; 
	vertical-align: middle; 
	margin-left: 5px;
	display: none;
	text-align: center;
}
</style>

<p id="authFailed">
Authentication failed. The information you have provided cannot be authenticated. Check your login information and try again.
</p>

<br />

<div align="center">


<form id="authLoginForm" name="authLoginForm" method="post" action="" style="margin: 0px;">
	<input type="hidden" value="193b0000i8g9d0e1d0b9c5i2c3b1" name="appKey">
	<input type="hidden" value="http://test.fieldsolutions.com/admin/" name="pathname">
	<input type="hidden" value="0" name="PrevPageID">
	<table id="authLogin" cellspacing="0" style="">
	<tbody>
		<tr>
			<td>
				<label for="xip_UserName">Admin User Name</label>
			</td>
			<td>
				<input class="authFormInput" type="text" value="" maxlength="255" size="15" id="UserName" name="UserName">
			</td>
		</tr>
		<tr>
			<td>
				<label for="xip_Password">Password</label>
			</td>
			<td>
				<input class="authFormInput" type="password" value="" maxlength="255" size="15" id="Password" name="Password" >
			</td>
		</tr>
		<tr style="background-color: rgb(255, 255, 255); padding: 7px;">
			<td style="padding: 5px 0px; text-align: center; vertical-align: middle; border: 2px solid rgb(42, 73, 125); background-color: rgb(186, 203, 223);" colspan="2">
				<input type="submit" id="xip_datasrc_TR_Master_List"  value="Login" name="xip_datasrc_TR_Master_List">
			</td>
		</tr>
	</tbody>
	</table>
</form>

Username and Password are case sensitive 

<br />
<p><a href="../misc/lostPassword.php">Forgot User Name and/or Password?</a></p>


</div>

	


<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
