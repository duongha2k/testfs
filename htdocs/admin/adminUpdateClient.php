<?php
$page = 'admin';
$option = 'client';
$selected = 'updateClient';
require ("../header.php");
require ("../navBar.php");
checkAdminLogin2();
?>
<script type="text/javascript" src="/widgets/js/jquery.min.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetClientDetails.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
<script type="text/javascript" src="/widgets/js/functions.js"></script>
<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
<link rel="stylesheet" href="/widgets/css/popup.css" type="text/css" />
<script type="text/javascript" src="/widgets/js/jquery.formatCurrency-1.4.0.js"></script>
<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>

<script type="text/javascript">
function calendarInit() {
    $('#ClientExpiration').click( function() {
        $('#calendar_div').css('z-index', ++roll.zindex);
        $(this).calendar({dateFormat:'MDY/'});
        $('#ClientExpiration').blur();
    });
}
function calendarMove( input ) {
    $('#calendar_div').css('top', $(input).offset().top + $(input).height() + 6 + 'px');
    $('#calendar_div').css('left', $(input).offset().left + 'px');
    popUpCal.showFor(input);
}

    function checkpasswordrule()
    {
        var username = $('#UserName').val();
        var password = $('#Password').val();
        var comfrimps = $('#passwordconfirmtext').val();
        var namearr = $('#ContactName').val().split(' ');
        
        var fname = namearr[0];
        var lname = namearr[namearr.length-1];
        
        var oldpass = $('#oldpass').val();
        if(oldpass!=password)
        {
            var err = _global.validPasword(username,password,comfrimps,fname,lname);
                                    
            if(err['error']==1)
            {
                $('#trerr').css("display", "");
                $('#errmsg').html(err['msg'][0]);
                if(err['id'][0]==2)
                {
                    $('#passwordconfirmtext').css('border','2px solid red');
                    $('#Password').css('border','1px solid black');
                }
                else
                {
                    $('#Password').css('border','2px solid red');
                    $('#passwordconfirmtext').css('border','1px solid black');
                }
                return false;
            }
            else
            {
                $('#trerr').css("display", "none");
                $('#Password').css('border','1px solid black');
                $('#passwordconfirmtext').css('border','1px solid black');
            }
        }
        else
        {
            $('#trerr').css("display", "none");
                $('#Password').css('border','1px solid black');
                $('#passwordconfirmtext').css('border','1px solid black');
        }
    }

var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";

var roll;
var detailsWidget;

$('document').ready(function(){
    roll = new FSPopupRoll();
    detailsWidget = new FSWidgetClientDetails({container:'detailsContainer',tab:'adminupdate'},roll);
        detailsWidget.show({params:{id:'<?php echo $_GET['id']; ?>'}});
});
</script>

<div align="center">
<br/>
<div id="detailsContainer"></div>
</div>

<!--- End Content --->
<?php require ("../footer.php"); ?>