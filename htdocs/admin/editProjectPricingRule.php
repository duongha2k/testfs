<?php $page = 'admin'; ?>
<?php $option = 'client'; ?>
<?php $selected = 'projectList'; ?>

<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>


<script type="text/javascript">
    window._company = '<?=$_GET['v']?>';
    //  use for load images and save it in browser cache only!!!
    var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
    var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";
    var formType = 'update';
</script>

<script type="text/javascript" src="/widgets/js/jquery.min.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetAdminProjectDetails.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
<script type="text/javascript" src="/widgets/js/functions.js"></script>
<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
<link rel="stylesheet" href="/widgets/css/popup.css" type="text/css" />
<script type="text/javascript" src="/widgets/js/jquery.formatCurrency-1.4.0.js"></script>

<script type="text/javascript">
function clientShowField() {
	return $("input#cbParamVirtual2");
}
function projectShowField() {
	return $("input#InsertRecordShowOnReports");
}

var roll;
var detailsWidget;
var updateP2TWindow = null;
var updateOptionsWindow = null;
var autoSubmitAfterP2TAuthorize = false;
var acceptedP2T = false;
var backClicked = false;
var currentUser = <?php echo$_SESSION['login'];?>

$('document').ready(function(){
	var value = "N";
	if (clientShowField().attr("value") == "Yes")
	{
		value = "Y";
	}
	projectShowField().attr("value",value);

    roll = new FSPopupRoll();
    detailsWidget = new FSWidgetAdminProjectDetails({container:'detailsContainer',tab:'pricingRule'},roll);
    detailsWidget.show({params:{company:window._company,project_id:'<?=$_REQUEST['id']?>'}});
});
</script>

<div align="center">
<br/>
<div id="detailsContainer"></div>
</div>

<?php require ("../footer.php"); ?>
