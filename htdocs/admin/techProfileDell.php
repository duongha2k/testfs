<?php
//	ini_set("display_errors", 1);
	set_time_limit(600);
	require_once("../headerStartSession.php");
	require_once("../library/caspioAPI.php");
	
	checkAdminLogin2();

	function caspioReturnTableFieldNames($tableName){
		$columns = caspioGetTableDesign($tableName, FALSE);	
		$array = array_slice($columns, 1);
		$fieldList = '';
		$tableColumns = array();
		global $fieldType;
		$x = 0;
		foreach ($array as $info) {
			$fieldInfo = explode(",",$info);
			$fieldName = $fieldInfo[0];
			$fieldType[$x] = $fieldInfo[1];
			$fieldList .= $fieldName . ',';
			$x ++;
		}
		$fieldList = substr($fieldList, 0, strlen($fieldList)-1);
		return $fieldList;
	}
	
	$fieldList = caspioReturnTableFieldNames("TR_Master_List");

	$deniedList = Core_Tech::getClientDeniedTechsArray('DELL');
	$deniedFilter = "";
	if (!empty($deniedList))
	    $deniedFilter .= ' AND TechID NOT IN (' . implode(',',$deniedList) . ')';
	
	$techList = caspioSelectAdv("TR_Master_List", "$fieldList", "AcceptTerms = 'Yes' AND Deactivated = '0' $deniedFilter AND (MRA_Compliant = '1' OR CDM = '1' OR L1 = '1' OR L2 = '1' OR L3 = '1' OR L4 = '1' OR DELL_DCSE_Reg = '1' OR DCSE_Soft_Skills = '1' OR DCSE_Lead = '1' OR VA = '1' OR SSA = '1' OR Secret = '1' OR Top_Secret = '1' OR NACI = '1' OR General1 = '1' OR General2 = '1' OR General3 = '1' OR General5 = '1' OR General5 = '1' OR General6 = '1')", "", false, "`", "|", false);	
	
//	echo "$fieldList";

	$csv_filename = "tech_profile_dell" . session_id() . ".csv";
	$fp = fopen("/tmp/$csv_filename", 'w');

	fputcsv($fp, explode(",", $fieldList));
		
	foreach ($techList as $row) {
		$parts = explode("|", $row);
		foreach ($parts as $key=>$part) {
			$parts[$key] = trim($part, "`");
		}
		fputcsv($fp, $parts);
	}
	
	fclose($fp);
	header("content-type: text/csv");
	header("Content-disposition: attachment; filename=" . str_replace(session_id(), "", $csv_filename));
	echo file_get_contents("/tmp/$csv_filename");
?>