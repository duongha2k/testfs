<?php
header("Content-Type: text/html; charset=utf-8");
require_once("../headerStartSession.php");
require_once("../library/mySQL.php");

?>
<?php $page = 'admin'; ?>
<?php $option = 'reports'; ?>
<?php $selected = 'PasswordUpadateReportReport'; ?>

<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php
require_once("../library/caspioAPI.php");
checkAdminLogin2();
?>
<!--- Add Content Here --->

<br />

<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript" src="js/UpdatePasswordList.js"></script>

<div id="content" style="position: relative;" align="center">

    <h1>Password Update Report</h1>
    <div id="clientnotupdatepassword">
        <center><img src='/widgets/images/wait.gif' border='0' /></center>
    </div>
    <div id="technotupdatepassword"></div>
</div>
<script>
    var _UpdatePasswordList = null;
    jQuery(window).ready(function(){
        _UpdatePasswordList = UpdatePasswordList.CreateObject({id:"_UpdatePasswordList"});
    });
</script>
<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->

