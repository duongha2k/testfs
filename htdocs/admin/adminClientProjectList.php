<?php $page = 'admin'; ?>
<?php $option = 'client'; ?>
<?php $selected = 'projectList'; ?>

<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); 

    require 'includes/PMCheck.php';
?>



<!-- Add Content Here -->
<script type="text/javascript" src="../library/jquery/jquery.simplemodal-1.3.min.js"></script>
<style type="text/css">
	.optionBtns {
		width: 90%;
		text-align: center;
		margin: 0px auto;
	}
	#optionMessage {
		margin: 15px 0px 20px 0px;
		font-size: 15px;
		font-weight: bold;		
	}
	.optionBtns div {
		margin: 5px 0px;
	}
	
	.cbButton
	{
		/*Back Button Attributes*/
		color: #ffffff;
		font-size: 12px;
		font-family: Verdana;
		font-style: normal;
		font-weight: bold;
		text-align: center;
		vertical-align: middle;
		border-color: #5496C6;
		border-style: solid;
		border-width: 1px;
		background-color: #032D5F;
		/* Forced by default to add space between buttons */
		width: auto;
		height: auto;
		margin: 0 3px;
	}
	
	.cbButton:hover {
		background-color: #234D7F;
	}
</style>


<div align="center">

<br /><br />

<!-- update_projects_admin.js -->
<script type="text/javascript">
amount_per_selector_field_name = "select#EditRecordAmount_Per";
device_quantity_field_name = "input#EditRecordQty_Devices";
</script>

<script type="text/javascript" src="/update_projects_admin.js">
</script>
<!-- end update_projects_admin.js -->

<script type="text/javascript" src="/widgets/js/FSWidgetManager.js"></script>
<script type="text/javascript">
var wManager = new FSWidgetManager();
</script>

<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
<script type="text/javascript" src="/widgets/js/ProgressBar.js"></script>
<script type="text/javascript" src="/widgets/js/ProgressBarAdapterAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/ProgressBarAdapterPopupSpin.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetAdminProject.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetSortTool.js"></script>

<script type="text/javascript">
try {
	if (isPM) {
		var customerList = "<?=$customerList?>";
		/* build dropdown using project list */
		var element_dropdown = "EditRecordClient_ID";
		
		/* build dropdown using project list */
		$("#" + element_dropdown).empty();
		index = 1;
		selIndex = 0;
		$("#" + element_dropdown).append("<option value=\"\">Select Company</option>");
		
		var customers = customerList.split("|"); 
		for (i = 0; i < customers.length; i++) {  
			var parts = customers[i].split("^");  
			var customer_id = parts[0];  
			var customer_name = parts[1];  
			htmlEle = "<option value=\"" + customer_id + "\">" + customer_name + "</option>";
			$("#" + element_dropdown).append(htmlEle);
			if (selectedCID == customer_id)
				selIndex = index;
			index++;
		}
		document.getElementById(element_dropdown).selectedIndex = selIndex;
	}
}
catch (e) {}

</script>


<script>

var proj;           //  Widgets object
var roll;           //  Rollover object
var popup;          //  Rollover object
var progressBar;    //  Progress bar object


$(document).ready( function() {
	proj = new FSWidgetAdminProject({container:'projectContainer',dbVersion:'<?=$dbVersion?>'});
    /* Find tag in URL params */

    //proj 		= new FSWidgetAdminProject();
    roll        = new FSPopupRoll();
    progressBar = new ProgressBar();

    /*  Load tab by parameters in URL */
    proj.setParams(/*proj.parseUrl()*/ {hideInactive: 'true', company: '<?=$_REQUEST['v'];?>'} );
    
    /*  Load tab by parameters in URL */
    progressBar.key('approve');
    progressBar.adapter(new ProgressBarAdapterPopupSpin(progressBar, null));

    var opt = proj.getParams();
    opt.company = '<?=isset($_GET['v'])?$_GET['v']:''?>';
    proj.show({params: opt});

    // add to manager
    wManager.add(proj, 'mainContainer');
    wManager._widgets.mainContainer = proj;

    $("#downloadButton").click(function(event){
        var opt = proj.getParams();
        opt['download']='csv';
        proj.open({
            tab     : 'list',
            params  : opt
        });
    });
});


function setStatus () {
    activeStatus = $("#SetActive").attr("checked");
    if (activeStatus != true)
	    activeStatus = "false";

	var opt=proj.getParams();
	opt.hideInactive = activeStatus;
	opt.page = 1;
	proj.setParams(opt);
	proj.show({tab:'list',params:opt});
}

</script>
<input type="button" class="link_button download_button" value="Download" id="downloadButton" style="float: left;">
<br class="clear" />
<div id="tabbedMenu" style="height:16px;">
<table cellspacing='0' cellpadding='0' class="paginator_top"><tr><td class="tCenter" id="toppaginatorContainer"></td></tr></table>
</div>
<br class="clear" />

<div id="projectContainer" style="margin-left:auto; margin-right:auto; width:100%; height:100%;"></div>



<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
