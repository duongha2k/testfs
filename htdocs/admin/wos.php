<?php
    $page = 'admin';
    $option = 'wos';
    $_GET['v'] = ( isset($_GET['v']) ) ? $_GET['v'] : NULL;
    $toolButtons = array('FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
    require_once '../headerStartSession.php';
    require '../header.php';
    require '../navBar.php';
    
    require 'includes/PMCheck.php';
    //require 'includes/adminCheck.php';
    //require 'includes/PMCheck.php';
    
?>
<script type="text/javascript" src="/widgets/js/FSWidgetManager.js"></script>
<script type="text/javascript">
var wManager = new FSWidgetManager();
 <?php
 //13794
  if($_GET['version'] && $_GET['version']=='sp'){
?>
  var _companyId = '<?=isset($_GET['company_id'])?$_GET['company_id']:'' ?>';
   $('#PMCompany').val(_companyId);  
  //$("#PMCompany").change();
<?php
    }   //end 13794
?>    
       
</script>
<script type="text/javascript" src="/widgets/js/jquery.min.js"></script>
<link rel=stylesheet type="text/css" href="/widgets/css/main.css" />
<link rel="stylesheet" type="text/css" href="/widgets/css/popup.css" />
<!-- Add Content Here -->
<?php /*<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script> <script type="text/javascript" language="javascript">try{f_cbload("193B0000D3D2C0H0E2D3D2C0H0E2","https:");}catch(li){;}</script> <div id="cxkg">Click <a href="https://bridge.caspio.net/dp.asp?AppKey=193B0000D3D2C0H0E2D3D2C0H0E2">here</a> to load this <a href="http://caspio.com">Caspio Bridge DataPage</a>.</div>*/?>

<!-- Check if logged in, if not redirect -->
<script type="text/javascript">
function openPopup(url,w,h) {
    w = (typeof(w) == 'unansigned') ? 740 : w;
    h = (typeof(h) == 'unansigned') ? 500 : h;
    newwindow=window.open(url,'name','height='+h+', width='+w+', left=100, top=100, resizable=yes, scrollbars=yes, toolbar=no,status=no');
    w=0;h=0;
    if (window.focus) {newwindow.focus()}
}
try {
window._company = '<?=isset($_GET['v'])?$_GET['v']:''?>';
window._search  = <?= (isset($_GET['search']) && $_GET['search'] + 0) ? 'true' : 'false'; ?>;
} catch (e) {
        window.location.replace("./");
}

//  use for load images and save it in browser cache only!!!
var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";
</script>

<script type="text/javascript" src="/widgets/js/jquery.form.js"></script>
<!--  script type="text/javascript" src="/clients/js/popupWindow.js"></script -->
<?php /*<script type="text/javascript" src="/clients/js/createLinks.js"></script>*/?>
<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetAdminDashboard.js?25022011"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetAdminSortTool.js"></script>

<script type="text/javascript" src="/widgets/js/FSWidgetDeactivationTool.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetActivityLog.js"></script>
<script type="text/javascript" src="/widgets/js/FSQuickAssignTool.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/jquery-ui.js"></script>
<script type="text/javascript" src="/widgets/js/livevalidation.js"></script>
<script type="text/javascript" src="/widgets/js/functions.js"></script>
<script type="text/javascript">
//Default validation settings
var LVDefaultFunction = function(){this.addFieldClass();}
var LVDefaults = {
    onValid : LVDefaultFunction,
    onInvalid : LVDefaultFunction
}

var pmContext = "<?=$_GET["v"]?>";
function reloadTabFrame() {
	// old dashboard reload tab adapter
//

	wd.params.WOFILTERS._company = window._company = $('#PMCompany').val();
	try {
	        wd.show({tab:wd.currentTab,params:wd.getParams()});
	} catch (e) {}
}

function calcBasePay(){
   if ($("#Approve_Amount_Per").val() != "Hour") return;
   var bidAmount = $("#Approve_Tech_Bid_Amount").text();
   bidAmount = String(bidAmount);
   var parts = bidAmount.split(" / ");
   bidAmount = parseFloat(parts[0]) * ($("#Approve_calculatedTechHrs").val()*1);
   $("#Approve_baseTechPay").val(bidAmount).change();
} 
</script>

<script type="text/javascript" src="/widgets/js/resizable.js"></script>

<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
<script type="text/javascript" src="/widgets/js/ProgressBar.js"></script>
<script type="text/javascript" src="/widgets/js/ProgressBarAdapterAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/ProgressBarAdapterPopupSpin.js"></script>
<script type="text/javascript" src="/widgets/js/FSFindWorkOrder.js"></script>
<script type="text/javascript" src="/widgets/js/FSTechPayUtils.js"></script>

<link rel="stylesheet" href="/widgets/css/resizable.css" type="text/css" />

<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript">
    /**
     *  calendarInit
     *
     *  Display filter post handler
     */
    function calendarInit() {
        $.map(['filterStartDate', 'filterEndDate'], function( id ) {
            $('#' + roll.container().id + ' #' + id).focus( function() {
                $('#calendar_div').css('z-index', ++roll.zindex);
                $(this).calendar({dateFormat:'MDY/'});
            });
        });
    }
    /**
     * calendarMove
     *
     * @param event  event
     * @access public
     * @return void
     */
    function calendarMove( input ) {
        $('#calendar_div').css('top', $(input).offset().top + $(input).height() + 6 + 'px');
        $('#calendar_div').css('left', $(input).offset().left + 'px');
        popUpCal.showFor(input);
    }
    function toggleChecks(cbox)
    {
        switch ( wd.currentTab ) {
            case 'assigned' :
                $.each($('#widgetContainer input[name="rePublishCheckbox"]'), function (idx, item){ 
                    item.checked =  cbox.checked;
                    var win = /^rePublishCheckbox_(\d+)$/.exec($(item).attr('id'))[1];

                    if ( item.checked ) {
                        $("#rePublishOptions_" + win).removeClass('displayNone');
                    } else {
                        $("#rePublishOptions_" + win).addClass('displayNone');
                    }
                });
                break;
            default :
                $.each($('#widgetContainer input[name="approveCheckbox"]'), function (idx, item){ item.checked = cbox.checked; });
                break;
        }
    }
    /**
     * doApprove
     *
     * @access public
     * @return void
     */
    function doApprove() {
        var wos = [];
        var _url;
        $.each($('#widgetContainer input[name="approveCheckbox"]'), function (idx, item){ if (item.checked) wos.push($(item).val()); });

        var payAmounts = {};

        switch ( wd.currentTab ) {
            case 'workdone' :
            case 'incomplete' :
                _url = '/widgets/admindashboard/do/approve/';
                progressBar.key('approve');

                var wosCount = wos.length;
        		for (var i = 0; i < wosCount; i++) {
        			var wId = wos[i];
        			payAmounts[wId] = $("#approvePayAmount_" + wId).val();
        		}
                break;
            case 'created'  :
                _url = '/widgets/admindashboard/do/publish/';
                progressBar.key('publish');
                break;
            default         : return false; //  Return from function on any other tabs
        }

        if ( wos.length ) {
            progressBar.run();

            $.ajax({
                url         : _url,
                dataType    : 'json',
                data        : { wo_ids : wos, payAmounts: payAmounts, company: pmContext},
                cache       : false,
                type        : 'post',
                context     : wd,
                success     : function (data, status, xhr) {
                    this.show({ tab:this.currentTab, params:this.getParams() });
                },
                error       : function ( xhr, status, error ) {
                    progressBar.stop();
                    this.show({ tab:this.currentTab, params:this.getParams() });
                }
            });
        }
    }
    /**
     * toggleTab
     *
     * Switch Tabs
     *
     * @param string name Name of currect Tab
     * @param object link Tab's link object
     * @param object wd wd object
     * @return false always
     */
    function toggleTab( name, link, wd, isloaddasboard ) {
        /* Disable full search */
        if ( name !== 'all' && window._search ) {
            wd.resetFilters();
            window._search = false;
        }
        if(typeof(isloaddasboard)=='undefined')
        {
            wd.filters().loaddasboard("");
        }
        if (wd.currentTab != name) {
            //wd.resetFilters();
            wd.currentTab = name;
            wd.sortTool().initSortTool();
        }
        window._company = $('#PMCompany').val();
        if('<?= $_REQUEST['version'] ?>' == "")
        $('#dashboardVersion').val('Full');

        wd.show({tab:wd.currentTab,params:wd.getParams()});
        link.blur();
        $("#toppaginatorContainer").html('');

        //set default state, then add custom classes.
        $('#MassApproveBtn').addClass("displayNone");
        $('#deactivationButton').addClass("displayNone");
        $('#MassAssignBtn').addClass("displayNone");
        $('#MassPublishBtn').addClass("displayNone");
        $('#dashboardVersion').removeAttr('disabled');
        switch ( name ) {
            case 'created':
                $('#MassApproveBtn').removeClass("displayNone").val('Mass Publish');
                $('#QuickAssignBtn').removeClass("displayNone");
                break;
            case 'workdone' :
            case 'incomplete' :
                $('#MassApproveBtn').removeClass("displayNone").val('Mass Approve');
                break;
            case 'published':
                $('#MassAssignBtn').removeClass("displayNone");
                break;
            case 'deactivated':
                $('#dashboardVersion').attr('disabled', 'disabled');
            	$('#deactivationButton').removeClass("displayNone");
                break;
            case 'assigned':
            	$('#MassPublishBtn').removeClass("displayNone");
            	break;
        }
        return false;
    }

    var wd;             //  Widgets object
    var roll;           //  Rollover object
    var popup;          //  Rollover object
    var progressBar;    //  Progress bar object
    var activityLog;    //  Activity Log object
    var findWorkOrder;  //  Find work order object

    $(document).ready( function() {
            
    	window._company = $('#PMCompany').val();

        wd          = new FSWidgetDashboard({container:'widgetContainer',tab:'workdone'});
        /* Find tag in URL params */
        var tab = '<?= (!empty($_GET['tab'])) ? strtolower($_GET['tab']) : '__invalid__'; ?>';
        var loaddasboard = '<?= (!empty($_GET['loaddasboard'])) ? ($_GET['loaddasboard']) : '0'; ?>';

        if ( wd._isValidTab(tab) ) {
            wd.currentTab = tab;
        }
        /* Find tab in URL params */
        
        roll        = new FSPopupRoll();
        assignTool  = new FSQuickAssignTool( wd );
        progressBar = new ProgressBar();
        activityLog = new FSWidgetActivityLog( roll );
        findWorkOrder = new FSFindWorkOrder(wd, roll);

        wd.htmlFilters();
        wd.sortTool(new FSWidgetAdminSortTool('sortSelect', 'directionSelect', wd, roll, 'quick'));
        
        wd.deactivationTool(new FSWidgetDeactivationTool(wd,roll));
        findWorkOrder.buildHtml();

        /*  Load tab by parameters in URL */
        wd.setParams(wd.parseUrl());
        wd.htmlFilters();
        toggleTab(wd.currentTab, $('#'+wd.currentTab), wd, 1);
        /*  Load tab by parameters in URL */

        progressBar.key('approve');
        progressBar.adapter(new ProgressBarAdapterPopupSpin(progressBar, null));
    });

    function reloadTabFrame() {
        wd.show({tab: wd.currentTab, params: wd.getParams()});
    }

</script>

<div id="container">
    <div id="header">
        <div id="wrapper">
            <div id="content" align="center"></div>
        </div>


<!-- DASHBOARD FILTER -->
<div id="projectSearch">
    <form id="dashFilter" name="dashFilter" action="<?=$_SERVER['PHP_SELF']?>?v=<?=$_GET["v"]?>" method="post">
    <div>
    <?php require("includes/getWOCount.php"); ?>

<div class="toolGroup">
    <div class="toolGroupLeft">
		<div class="toolBtn">
			<input id="viewFiltersButton" type="button" value="Filter" class="link_button" />
		</div>
        <div class="toolBtn">
			<input id="sortButton" type="button" value="Sort" class="link_button" />
		</div>
		<div class="toolBtn">
			<input id="downloadButton" type="button" value="Download" class="link_button download_button" />
        </div>

        <!--div class="toolBtn">
            <input id="activityLogButton" type="button" value="View Activity Log" class="link_button" />
        </div-->

	</div>
	<div class="toolGroupRight">
		<div class="toolBtn topSm">
			Quick Sort
		</div>
		<div class="toolBtn">
			<select id="sortSelect" onchange="return wd.sortTool().applySort('quick');"></select>
		</div>
		<div class="toolBtn">
			<select id="directionSelect" onchange="return wd.sortTool().applySort('quick');"></select>
		</div>
	</div>
</div>
<script type="text/javascript">
	function viewDeactivationReason(win) {
		wd.deactivationTool().setWin(win);
		$("#deactivationButton").click();
	}

	$(document).ready(function() {

		
		
		$("#activityLogButton").click(function(event) {
            roll.autohide(false);
            var opt = {
                width       : 604,
                height      : '',
                title       : 'View Activity Log',
                body        : activityLog.buildHtml(),
                context     : activityLog,
                handler     : activityLog.initActivityLogTool
            };
            roll.showNotAjax(this, event, opt);
        });

        $("#QuickAssignBtn").click(function(event) {
            roll.autohide(false);
            var opt = {
                popup       : 'quickassigntool',
                title       : 'Quick Assign',
                width       : 400,
                preHandler  : assignTool.getWidgetHtml,
                postHandler : assignTool.initEvents,
                context     : assignTool,
                delay       : 0
            }
            roll.show(this,event,opt);
        });

        $("#viewFiltersButton").click(function(event){
            roll.autohide(false);
            var opt = {
                width       : 325,
                height      : 405,
                title       : 'Filter By',
                context     : wd,
                handler     : wd.prepareFilters,
                postHandler : calendarInit,
                body        : $(wd.filters().container()).html()
            };
            roll.showNotAjax(this, event, opt);
        });

        $("#downloadButton").click(function(event){
            var opt = wd.getParams();
            opt['download']='csv';

            if ($('#dashboardVersion').val() == 'sp') {
            	opt['version'] = 'searchpay';
            }
            
            wd.open({
                tab     : wd.currentTab,
                params  : opt
            });
        });

        $("#sortButton").click(function(event){
            wd.sortTool().switchSortTools('full');
            roll.autohide(false);
            var opt = {
                width   : 325,
                height  : 225,
                title   : 'Sort Work Orders...',
                context : wd.sortTool(),
                handler : wd.sortTool().initSortPopup,
                body    : wd.sortTool().makeFullSortPopup()
            };
            roll.showNotAjax(this,event,opt);
        });

	});


</script>

<br class="clear" />
<div id="tabbedMenu" style="height:16px;"><ul>
    <li id="created"><a href="javascript:void(0)" onclick="toggleTab('created', this, wd);">Created</a></li>
    <li id="published"><a href="javascript:void(0)" onclick="toggleTab('published', this, wd);">Published</a></li>
    <li id="assigned"><a href="javascript:void(0)" onclick="toggleTab('assigned', this, wd);">Assigned</a></li>
    <li id="workdone"><a href="javascript:void(0)" onclick="toggleTab('workdone', this, wd);"><nobr>Work Done</nobr></a></li>
    <li id="approved"><a href="javascript:void(0)" onclick="toggleTab('approved', this, wd);">Approved</a></li>
    <li id="inaccounting"><a href="javascript:void(0)" onclick="toggleTab('inaccounting', this, wd);">In Accounting</a></li>
    <li id="incomplete"><a href="javascript:void(0)" onclick="toggleTab('incomplete', this, wd);">Incomplete</a></li>
    <li id="completed"><a href="javascript:void(0)" onclick="toggleTab('completed', this, wd);">Completed</a></li>
    <li id="all"><a href="javascript:void(0)" onclick="toggleTab('all', this, wd);">All</a></li>
    <li id="deactivated" style="margin-left:10px;"><a href="javascript:void(0)" onclick="toggleTab('deactivated', this, wd);">Deactivated</a></li>
</ul>
<script>
function onChangePageSize()
{
	reloadTabFrame();
}
</script>
<div id="toppaginatorSizeContainer"></div>
<table cellspacing='0' cellpadding='0' class="paginator_top"><tr><td class="tCenter" id="toppaginatorContainer"></td></tr></table>
</div>
<br class="clear" />
<input type="hidden" name="version" id="dashboardVersion" value="<?= $_REQUEST["version"] ?>">
<div id="widgetContainer" style="margin-left:auto; margin-right:auto; width:100%; height:100%;"></div>

<!-- End Content -->
<?php require ("../footer2.php"); ?>
 
