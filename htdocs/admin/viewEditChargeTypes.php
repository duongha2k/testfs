<?php
//	ini_set("display_errors",1);
	$page = "admin";
	include ("../headerSimple.php");
	require_once("../library/pricing.php");
?>
<?php
	initPricingJS();
	// pull in charge type matrix
	$viewingPricingRuleID = $_GET["id"];
	if ($_POST) {
		if (isset($_POST['DeleteChargeTypeID'])) {
			Core_Pricing::deleteChargeType($_POST['DeleteChargeTypeID']);
		}
		else {
			$type = 'Edit';
			if (!isset($_POST['EditChargeTypeID'])) {
				$type = 'Add';
			}
			Core_Pricing::saveChargeType($_POST[$type . 'ChargeTypeID'], $_POST[$type . 'Description'], $_POST[$type . 'Percentage'], $_POST[$type . 'FlatFee'], $_POST[$type . 'PerX'], $_POST[$type . 'Min'], $_POST[$type . 'Max'], $_POST[$type . 'InputField'], $_POST[$type . 'OutputField'], $_POST[$type . 'CalculationOrder'], $_POST[$type . 'ApplyWhen'], $viewingPricingRuleID);
		}
	}
	$chargeTypes = getChargeTypes($viewingPricingRuleID);
	$calcMatrix = createPricingCalculationMatrix($chargeTypes);
	$_SESSION["calcMatrixParam"] = $calcMatrix;
	
//	print_r($calcMatrix);
?>
<style type="text/css" title="currentStyle">
			@import "/widgets/js/datatables/css/demo_page.css";
			@import "/widgets/js/datatables/css/demo_table.css";
			@import "/widgets/js/datatables/css/datatable_theme.css";
		</style>

<script  src="/widgets/js/datatables/js/jquery.dataTables.js"></script>
<script type="text/javascript">
	var viewingPricingRuleID = "<?=$_GET["id"]?>";
	var nextAvailableCalcOrder = <?php echo getNextCalculationOrder($_GET["id"]);?>;
	var add = '<?=empty($_GET['add']) ? '' : $_GET['add']?>';
	var edit = '<?=empty($_GET['edit']) ? '' : $_GET['edit']?>';
	var numColumns = 0;
	var inputFieldColumn = 0;
	var outputFieldColumn = 0;
	var applyWhenColumn = 0;
	var ruleIDColumn = 0;
	var addRow = null;
	var chargeTypeTable = null;
	var addInputField = null;
	var addOutputField = null;
	var addApplyWhen = null;
	var addPricingRuleID = null;
	
	function validateFields(type) {
		description = $("#" + type + "Description");
		
		if (description.val() == "") {
			description.focus();
			alert("Enter a charge type description");
			return false;
		}
		if ($("#" + type + "Percentage").val() == "" && $("#" + type + "FlatFee").val() == "" && $("#" + type + "PerX").val() == "") {
			alert("Enter a percentage, flat fee or per X fee to charge");
			return false;
		}
		return true;
	}

	function validateAddFields() {
		return validateFields("Add");
	}

	function validateEditFields() {
		return validateFields("Edit");
	}
	
	$(document).ready(function(){
		width = document.documentElement.scrollWidth; 
		height = document.documentElement.scrollHeight;
		parent.resizeFrame(width, height, window);
/*		$("#Mod0InlineAdd").click(validateAddFields);
		$("#Mod0InlineEdit").click(validateEditFields);
		addRow = $("#InlineAddInputField").parent().parent().parent();
		row = addRow.children();
		numColumns = row.length;
		for (i = 0; i < numColumns; i++) {
			me = row.eq(i).children().eq(0).children().eq(0);
			if (me.attr("name") == "InlineAddInputField") {
				inputFieldColumn = i;
				addInputField = me;
			}
			else if (me.attr("name") == "InlineAddOutputField") {
				outputFieldColumn = i;
				addOutputField = me;
			}
			else if (me.attr("name") == "InlineAddApplyWhen") {
				applyWhenColumn = i;
				addApplyWhen = me;
			}
			else if (me.attr("name") == "InlineAddPricingRuleID")
				addPricingRuleID = me;
		}
		
		addCalculationOrder = $("#InlineAddCalculationOrder");*/
		
		// fix add ChargeType row
/*		selectHTML = createSelectPricingFields(addInputField.attr("name"));
		addInputField.replaceWith(selectHTML);
		selectHTML = createSelectPricingFields(addOutputField.attr("name"));
		addOutputField.replaceWith(selectHTML);
		selectHTML = createSelectCalculationOrder(addCalculationOrder.attr("name"), nextAvailableCalcOrder);
		addCalculationOrder.replaceWith(selectHTML);
		addPricingRuleID.attr("value", viewingPricingRuleID).attr("readonly", true);
		
		chargeTypeRows = addRow.nextAll();
		numRows = chargeTypeRows.length;
		
		for (i = 0; i < numRows; i++) {
			row = chargeTypeRows.eq(i).children();
			inputFieldCell = row.eq(inputFieldColumn);
			outputFieldCell = row.eq(outputFieldColumn);
			applyWhenCell = row.eq(applyWhenColumn);
			ruleIDCell = row.eq(ruleIDColumn);
			if (inputFieldCell.children().length > 0) {
				// inline edit
				inputFieldCell = $("#InlineEditInputField");;
				outputFieldCell = $("#InlineEditOutputField");
				applyWhenCell = $("#InlineEditApplyWhen");
				calcOrderCell = $("#InlineEditCalculationOrder");
				ruleIDCell = $("#InlineEditPricingRuleID");
				selectHTML = createSelectPricingFields(inputFieldCell.attr("name"), inputFieldCell.attr("value"));
				inputFieldCell.replaceWith(selectHTML);
				selectHTML = createSelectPricingFields(outputFieldCell.attr("name"), outputFieldCell.attr("value"));
				outputFieldCell.replaceWith(selectHTML);
				selectHTML = createSelectCalculationOrder(calcOrderCell.attr("name"), nextAvailableCalcOrder, calcOrderCell.attr("value"));
				calcOrderCell.replaceWith(selectHTML);
				ruleIDCell.attr("readonly", true);
			}
			else {
				inputFieldCell.html(getPricingFieldDisplayName(inputFieldCell.html()));
				outputFieldCell.html(getPricingFieldDisplayName(outputFieldCell.html()));
			}
		}*/
		
	});
	
	function editChargeType(id) {
		if ($("#chargeTypeTable .editRow").length > 0) { document.location.replace('<?=$_SERVER['PHP_SELF']?>?id=<?=$_GET["id"]?>&edit=' + id); return;}
		columnName = ['EditChargeTypeID', 'EditDescription', 'EditPercentage', 'EditFlatFee', 'EditPerX', 'EditMin', 'EditMax', 'EditInputField', 'EditOutputField', 'EditCalculationOrder', 'EditApplyWhen'];
		row = $("#chargeTypeID" + id);
		row.children().each(function(index, element) {
			val = table.fnGetData(row.get(0), index);
			switch (index) {
				case 0:
					$(element).html(id + "<input type='hidden' id='" + columnName[index] + "' name='" + columnName[index] + "' value='" + val + "' />");
					break;
				case 7:
				case 8:
					selectHTML = createSelectPricingFields(columnName[index], val, index);
					$(element).html(selectHTML);
					break;
				case 9:
					selectHTML = createSelectCalculationOrder(columnName[index], nextAvailableCalcOrder, val);
					$(element).html(selectHTML);				
					break;
				case 11:
					$(element).html("<a href='javascript:saveChargeType(" + id + ")'>Save</a><br/><a href='javascript:cancelEditChargeType(" + id + ")'>Cancel</a>");
					break;
				default:
					$(element).html("<input type='text' id='" + columnName[index] + "' name='" + columnName[index] + "' value='" + val + "' />");
		}
	});
		row.addClass('editRow');
	}

	function addChargeType() {
		if (validateAddFields())
			$("#chargeTypeForm").submit();
	}

	function saveChargeType(id) {
		if (validateEditFields())
			$("#chargeTypeForm").submit();
	}

	function deleteChargeType(id) {
		$("#chargeTypeForm").append("<input type='hidden' id='DeleteChargeTypeID' name='DeleteChargeTypeID' value='" + id + "'/>");
		$("#chargeTypeForm").submit();
	}

	function cancelEditChargeType(id) {
		document.location.replace('<?=$_SERVER['PHP_SELF']?>?id=<?=$_GET["id"]?>');
	}
</script>
<form id="chargeTypeForm" method="post" action="<?=$_SERVER['PHP_SELF']?>?id=<?=$_GET["id"]?>">
<table cellspacing='1' id='chargeTypeTable' class='tablesorter'><thead><tr><th>ChargeType ID</th><th>Description</th><th>Percentage</th><th>Flat Fee</th><th>Per X</th><th>Min</th><th>Max</th><th>Input Field</th><th>Output Field</th><th>Calculation Order</th><th>Apply When</th><th></th></tr></thead><tbody>
<?php
	$charges = Core_Pricing::getChargeTypes($viewingPricingRuleID);
	foreach ($charges as $charge) {
?>
		<tr id="chargeTypeID<?=$charge['ChargeTypeID']?>">
        	<td><?=$charge['ChargeTypeID']?></td><td><?=htmlentities($charge['Description'])?></td><td><?=$charge['Percentage']?></td><td><?=$charge['FlatFee']?></td><td><?=$charge['PerX']?></td><td><?=$charge['Min']?></td><td><?=$charge['Max']?></td><td><?=$charge['InputField']?></td><td><?=$charge['OutputField']?></td><td><?=$charge['CalculationOrder']?></td><td><?=htmlentities($charge['ApplyWhen'])?></td><td><a href='javascript:editChargeType(<?=$charge['ChargeTypeID']?>)'>Edit</a><br/><a href='javascript:deleteChargeType(<?=$charge['ChargeTypeID']?>)'>Delete</a></td>
        </tr>
<?php
	}
?>
		<tr id="chargeTypeID0">
        	<td>0</td><td><input type="text" id="AddDescription" name="AddDescription"></td><td><input type="text" id="AddPercentage" name="AddPercentage"></td><td><input type="text" id="AddFlatFee" name="AddFlatFee"></td><td><input type="text" id="AddPerX" name="AddPerX"></td><td><input type="text" id="AddMin" name="AddMin"></td><td><input type="text" id="AddMax" name="AddMax"></td><td><?=createSelectPricingFields('AddInputField')?></td><td><?=createSelectPricingFields('AddOutputField')?></td>
            <td>
            <select id="AddCalculationOrder" name="AddCalculationOrder" \>
			<?php 
				$max = getNextCalculationOrder($viewingPricingRuleID);
				for ($i = 0; $i <= $max; ++$i) {
?>
				<option value="<?=$i?>"><?=$i?></option>
<?php
				}
			?>
            </select>
			</td><td><input type="text" id="AddApplyWhen" name="AddApplyWhen"></td><td><a href="javascript:addChargeType()">Add</a></td>
        </tr>
</tbody></table>
</form>
<script type="text/javascript">
	var table;
	$(document).ready(function() {
		table = $('#chargeTypeTable').dataTable({
				"aoColumnDefs": [
					{ "bSortable": false, "aTargets": [ 11 ]},
					{ "bSearchable": false, "aTargets": [ 11 ]},	
				],
				"aaSorting": [[0, 'asc'], [9, 'asc']],
				"bPaginate": false,
				"bFilter": false,
				"fnRowCallback": function(row, data, index, indexFull) { 
						if (data[0] == 0) return row;
						$(row).children().eq(7).html(getPricingFieldDisplayName(data[7])); 
						$(row).children().eq(8).html(getPricingFieldDisplayName(data[8])); 
						return row; 
				}
		});
		if (add != '') {
			try {
				addChargeType();
			} catch (e) {}
		}
		if (edit != '') {
			try {
				editChargeType(edit);
			} catch (e) {}
		}
	});
</script>