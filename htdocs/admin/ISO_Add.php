<?php 
$page = 'admin';
$option = 'ISO';
$selected = 'addISO';
require ("../header.php");
require ("../navBar.php");
	checkAdminLogin2();
?>
<script type="text/javascript" src="/widgets/js/jquery.min.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetISODetails.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
<script type="text/javascript" src="/widgets/js/functions.js"></script>
<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
<link rel="stylesheet" href="/widgets/css/popup.css" type="text/css" />
<script type="text/javascript" src="/widgets/js/jquery.formatCurrency-1.4.0.js"></script>

<script type="text/javascript">
var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";

var roll;
var detailsWidget;

$('document').ready(function(){
    roll = new FSPopupRoll();
    detailsWidget = new FSWidgetISODetails({container:'detailsContainer',tab:'create'},roll);
    detailsWidget.show({params:{company:window._company}});
	detailsWidget.onChangeCountry();
});
</script>

<div align="center">
<br/>
<div id="detailsContainer"></div>
</div>

<!--- End Content --->
<?php require ("../footer.php"); ?>