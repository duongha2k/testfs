<?php $page = admin; ?>
<?php $option = techs; ?>
<?php $selected = 'phoneAuditLog'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php
checkAdminLogin2();

// ================================================================================
// This code is used to copy Preferrred Techs from one client over to another. GB |
// ================================================================================


require_once("../library/caspioAPI.php");
require ("../headerStartSession.php");
$search = new Core_Api_AdminClass;
//get companies
$fieldsList = array('Company_ID', 'CompanyName');
$companies = $search->getCompanies($_SESSION["UserName"], $_SESSION["UserPassword"]
        , $fieldsList, 'CompanyName', 'CompanyName');
$companies = $companies->data;
?>
<script src="/widgets/js/FSWidget.js"></script>
<script src="/widgets/js/FSPopupAbstract.js"></script>
<script src="/widgets/js/FSPopupRoll.js"></script>
<script src="/widgets/js/base64.js"></script>
<style type="text/css">@import url(../library/ui.datepicker.css);</style>
<script type="text/javascript" src="../library/ui.datepicker.js"></script>

<script  src="js/phone_audit_log.js"></script>
<style>
    .table_report_phone td{
        padding-right: 15px;
    }
</style>
<form id="frmData" name="frmData" >
    <table class="table_report_phone" width="100%">
        <tr>
            <td colspan="2">
                <h3>Phone Audit Log</h3>
            </td>
        </tr>
        <tr>
            <td align="right">
                Date Reported: >=
            </td>
            <td>
                <input type="text" id="txtDateportFrom" name="ReportedStartDate" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <=
            </td>
            <td>
                <input type="text" id="txtDateportTo" name="ReportedEndDate" />
            </td>
        </tr>
        <tr>
            <td align="right">
                Include Validated Phone #s
            </td>
            <td>
                <input type="checkbox" id="chkvalidPhone" name="IncludeValidatedPhone" />
            </td>
        </tr>
        <tr>
            <td align="right">
                Phone #
            </td>
            <td>
                <input type="text" id="txtPhone" name="PhoneNumber" />
            </td>
        </tr>
        <tr>
            <td align="right">
                Day/Evening
            </td>
            <td>
                <select id="cbxPS" name="PhoneType">
                    <option value="">Select All</option>
                    <option value="1">Day</option>
                    <option value="2">Evening</option>
                </select>
            </td>
        </tr>
        <tr>
            <td align="right">
                Tech ID#
            </td>
            <td>
                <input type="text" id="txtTechID" name="TechID" />
            </td>
        </tr>
        <tr>
            <td align="right">
                Tech Last Name
            </td>
            <td>
                <input type="text" id="txtTechLastName" name="TechLastName" />
            </td>
        </tr>
        <tr>
            <td align="right">
                Tech First Name
            </td>
            <td>
                <input type="text" id="txtTechFirstName" name="TechFisrtName" />
            </td>
        </tr>
        <tr>
            <td align="right">
                Reporting Client
            </td>
            <td>
                <select id="cbxCompany" name="ReportingClientId">
                    <option value="">Select All</option>
                    <?php foreach ($companies as $item) {
                        ?>
                        <option value="<?= $item->Company_ID ?>"> <?= $item->CompanyName ?> </option>
                        <?php
                    }
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td align="right">
                Reporting User
            </td>
            <td>
                <input type="text" id="txtReportingUser" name="ReportingUser" />
            </td>
        </tr>
        <tr>
            <td align="right">
                <input class="link_button download_button" value="Download"  type="button" id="cmdDownload" />
            </td>
            <td>
                <input class="link_button middle2_button" value="Search" type="button" id="cmdSearch" name="cmdSearch" />
            </td>
        </tr>
    </table>
    <div style="width:100%;padding-top: 10px;" id="divReport">

    </div>
    <script>
        var _phone_audit_log = phone_audit_log.CreateObject({
            id:'_phone_audit_log' 
        });
    </script>
</form>   