<?
$page = 'admin';
$option = 'client';
$selected = 'fstagsList';

require ("../header.php");
require ("../navBar.php");

checkAdminLogin2();

$visibility = $_POST['SystemVisibility'];
$companyID = $_POST['client'];
$tagTitleLike = $_POST['adminusername'];
$addByUsername = $_POST['tagtitle'];
$activity = $_POST['adminuser'];

$FSTagClass = new Core_Api_FSTagClass();
$TagSearchResultlist = $FSTagClass->getTagSearchResult($visibility, $companyID, $tagTitleLike, $addByUsername, $activity);
?>
<script src="/widgets/js/FSWidget.js"></script>
<script src="/widgets/js/FSPopupAbstract.js"></script>
<script src="/widgets/js/FSPopupRoll.js"></script>
<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
<script src="/widgets/js/base64.js"></script>
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>

<script>
    function cbLinkHover(obj, css) {
        try {
            obj.style.cssText = css;
        } catch (e) {
        }
    }
    function cbButtonHover(obj, css) {
        try {
            obj.style.cssText = css;
        } catch (e) {
        }
    }

</script>

<style>
    .col1
    {
        text-align: left;
        vertical-align: middle;
        width: auto;
        white-space: nowrap;
        padding: 2px 5px 2px 5px;
        color: #032D5F;
        font-size: 12px;
        font-family: Verdana;
        font-style: normal;
        font-weight: normal;
    }
    .col2
    {
        text-align: left;
        vertical-align: top;
        width: auto;
        padding: 5px 10px 5px 10px;
        white-space: normal;
    }
    .trstyle
    {
        background-color: #FFFFFF;
        padding: 7px;
    }
    .cbxstyle
    {
        color: #000000;
        font-size: 12px;
        font-family: Verdana;
        font-style: normal;
        font-weight: normal;
    }

    .tableheader
    {
        color:white;
        text-align:left;
        background-color:#5091cb;
    }
    .oddRow
    {
        background-color: #F2F5F9;
        border-bottom: 1px solid #000000;
    }
    .evenRow {
        background-color: #FFFFEF;
        border-bottom: 1px solid #000000;
    }
</style>
<div align="center">

    <h1>FS-Tags List</h1>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td class="tableheader">Client</td>
            <td class="tableheader">Tag Title</td>
            <td class="tableheader">Visibility</td>
            <td class="tableheader">Tag Added By User</td>
            <td class="tableheader">Tag Added Date</td>
            <td class="tableheader">Active?</td>
            <td class="tableheader">Edit</td>
        </tr>
        <?
        if (!empty($TagSearchResultlist))
        {
            $rowcount = 1;
            foreach ($TagSearchResultlist as $Tag)
            {
                $rowcolor = ($rowcount % 2 == 0 ? "evenRow" : "oddRow");
                $AddedDate = date("m/d/Y H:i:s A", strtotime($Tag['AddedDate']));
                ?>
                <tr class="<?=$rowcolor?>">
                    <td><?= $Tag['CompanyId'] ?></td>
                    <td><?= $Tag['Title'] ?></td>
                    <td><?= $Tag['VisibilityId'] == 0 ? "All Client Users" : "Selected Client(s) Only" ?></td>
                    <td><?= $Tag['AddedByUser'] ?></td>
                    <td><?= $AddedDate ?></td>
                    <td><?= $Tag['Active'] == 0 ? "No" : "Yes" ?></td>
                    <td><a class="edittag" href="javascript:;" style="text-decoration: underline;" >Edit</a></td>
                </tr>
                <?
                $rowcount ++;
            }
        }
        ?>
    </table>
</div>
<script>
    jQuery(window).ready(function(){
        jQuery('#cmdSubmit').click(function(event){
            var dataForm = jQuery("#frmsearchfstag").serialize();
            dataForm += "&adminuser="+$("#cbxadminuser").find("option:selected").attr("textname"); 
            alert(dataForm);
            $.ajax({
                type: "POST",
                url: "/widgets/admindashboard/client/getclientcompany1111?company_id="+company_id,
                data: "",
                success:function( html ) {
                    window.location = "/admin/admin_fstaglist.php?type=Client&client_id="+data.client_id;
                }
            });
        })
    });
</script>
<?php require ("../footer.php"); ?>