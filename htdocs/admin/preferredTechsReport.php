<?php $page = 'admin'; ?>
<?php $option = 'techs'; ?>
<?php $selected = 'preferredReport';
	require ("../headerStartSession.php");
	require_once("../library/caspioAPI.php");
	checkAdminLogin2();
	$company_id = $_GET["Company_ID"];

	$sort = isset($_GET["sortCol"]) ? $_GET["sortCol"] : 0;
	$dir = isset($_GET["sortDir"]) ? $_GET["sortDir"] : 0;
	$dir = $_GET["sortDir"];
	switch ($sort) {
		case 0:
			$sortQuery = "ID";
			break;
		case 1:
			$sortQuery = "FN";
			break;
		case 2:
			$sortQuery = "LN";
			break;
		case 3:
			$sortQuery = "CID";
			break;
		case 4:
			$sortQuery = "CM";
			break;
		case 5:
			$sortQuery = "PP";
			break;
		case 6:
			$sortQuery = "SP";
			break;
		case 7:
			$sortQuery = "PE";
			break;
		case 8:
			$sortQuery = "SE";
			break;
		default:
			$sortQuery = "LN";
	}

	if ($sortQuery != "") {
		$sortQuery = "$sortQuery " . ($dir == 0 ? "ASC" : "DESC");
	}

// SELECT TechID, TechFName, TechLName FROM TR_Master_List WHERE FLSDeactivated = '1' UNION SELECT TechID, TL.TechFName, TL.TechLName FROM Deactivated_Techs LEFT JOIN TR_Master_List AS TL ON DeactivatedTechs.TechID = TL.TechID


        $db = Zend_Registry::get('DB');
	$ttable = array('ID' => 'tl.TechID', 'FN' => 'tl.FirstName', 'LN' => 'tl.LastName', 'PP' => 'PrimaryPhone', 'SP' => 'SecondaryPhone', 'PE' => 'PrimaryEmail', 'SE' => 'SecondaryEmail' );
	$dtable = array('CID' => 'd.CompanyID');
	$select = $db->select();
	$select->from(array('d' => Core_Database::TABLE_CLIENT_PREFERRED_TECHS), $dtable)
	->join(array('tl' => Core_Database::TABLE_TECH_BANK_INFO), 'tl.TechID = d.Tech_ID', $ttable)
	->group('tl.TechID')
	->order($sortQuery);
	switch($company_id) {
		case "FLS":
			$clientDeniedCriteria = "FLSstatus = 'Admin Denied'";
			$select->where($clientDeniedCriteria)
			->where('d.CompanyID = ?', 'FLS');
			$results = $db->fetchAll($select);

			break;
		case "":
/*			$deniedList = Core_Tech::getClientDeniedTechsArray();
			if (!empty($deniedList))
			    $clientDeniedCriteria = "tl.TechID IN (" . implode(',',$deniedList) . ")";
			else
			    $clientDeniedCriteria = 'tl.TechID IN (0)';*/
//			->where($clientDeniedCriteria)
			$select->group('d.CompanyID');
			$results = $db->fetchAll($select);
			$companyListByTechID = array();
			foreach ($results as $row) {
				$companyListByTechID[$row['ID']][] = $row['CID'];
			}
			break;
		default:
/*			$deniedList = Core_Tech::getClientDeniedTechsArray($company_id);
			if (!empty($deniedList))
			    $clientDeniedCriteria = "tl.TechID IN (" . implode(',',$deniedList) . ") AND d.CompanyID = '$company_id'";
			else
			    $clientDeniedCriteria = 'tl.TechID IN (0)';*/
//			->where($clientDeniedCriteria)
			$select->where('d.CompanyID = ?', $company_id);
			$results = $db->fetchAll($select);
	}

	$numResults = sizeof($results);

	if (!empty($_GET['download'])) {
		$f = fopen('php://temp', 'w+');
		fputcsv($f, array('TechID', 'FirstName', 'LastName', 'PrimaryPhone', 'SecondaryPhone', 'PrimaryEmail', 'SecondaryEmail', 'CompanyID'));
	}
	$html = "";

	foreach ($results as $bid) {
		$techID = $bid['ID'];
		$firstName = $bid['FN'];
		$lastName = $bid['LN'];
		if (!$companyListByTechID || !array_key_exists($techID, $companyListByTechID))
			$companyID = $bid['CID'];
		else {
			if ($companyListByTechID[$techID] === true) {
				--$numResults;
				continue;
			}
			$companyID = implode(',',$companyListByTechID[$techID]);
			$companyListByTechID[$techID] = true;
		}
		$primaryPhone = $bid['PP'];
		$primaryEmail = $bid['PE'];
		$secondaryPhone = $bid['SP'];
		$secondaryEmail = $bid['SE'];
		if (!empty($_GET['download']))
			fputcsv($f, array($techID, $firstName, $lastName, $primaryPhone, $secondaryPhone, $primaryEmail, $secondaryEmail, $companyID));
		else
			$html .= "<tr class='resultRow'><td class=\"leftRow\">$techID</td><td>$firstName</td><td>$lastName</td><td>$primaryPhone</td><td>$secondaryPhone</td><td>$primaryEmail</td><td>$secondaryEmail</td><td>$companyID</td></tr>";
	}

	if (!empty($_GET['download'])) {
		header("Content-disposition: attachment; filename=techsClientPreferred.csv");
		echo stream_get_contents($f, -1, 0);
		fclose($f);
		die();
	}

	require ("../header.php");
	require ("../navBar.php");
?>
<br/>
<div align="center">

<script type="text/javascript">
<!--

	function sortBy(num) {
		frm = document.getElementById("sortForm");
		frm.sortDir.value = (frm.sortCol.value == num ? (frm.sortDir.value == 0 ? 1 : 0) : 0);
		frm.sortCol.value = num;
		frm.submit();
	}
//-->
</script>
</div>


<div align="center">
<h1>Client Preferred Technicians</h1>


<?php

//$companies = caspioSelectAdv("TR_Master_List", "CID='FLS'", "FLSstatus = 'Admin Denied' UNION SELECT Deactivated_Techs_By_Clients.Company_ID FROM Deactivated_Techs_By_Clients LEFT JOIN TR_Master_List AS TL ON Deactivated_Techs_By_Clients.TechID = TL.TechID ORDER BY CID", "", FALSE, "`", "|");
?>

<style type="text/css">
<!--
form#companyForm {
	margin-bottom: 20px;
}
//-->
</style>
<form id="companyForm" name="companyForm" action="<?=$_SERVER['PHP_SELF']?>">
Select a company:
<select id="Company_ID" name="Company_ID">
<option value="">All companies&nbsp;&nbsp;</option>
<?php

        $select = $db->select();
        $select->from(Core_Database::TABLE_CLIENT_PREFERRED_TECHS, array("CompanyID"))
                ->distinct()
                ->where("IFNULL(CompanyID,'') <> ''")
                ->order(array("CompanyID"));
        $companyList = $db->fetchCol($select);

        foreach ($companyList as $id) {
		$selected = "";
		if ($company_id == $id)
			$selected = " selected='selected'";
                echo "<option $selected value='$id'>$id</option>";
        }

?>
</select>
<!--
<input type="submit" value="Select Company" />
-->
</form>

<script type="text/javascript">
<!--


function selectCompany() {
	return $("select#Company_ID");
}

$("form#companyForm").submit(function() {
	if (selectCompany().attr("value") == "All companies") {
		selectCompany().attr("value","Hello");
		alert(selectCompany().attr("value"));
	}
});

selectCompany().change(function() {
	$("form#companyForm").submit();
});

//-->
</script>


<?php
{
?>
<form id="sortForm" name="sortForm" action="<?=$_SERVER['PHP_SELF']?>" method="get">
	<input name="Company_ID", type="hidden", value="<?php echo $company_id ?>">
	<input name="sortCol" type="hidden" value="<?php echo $_POST["sortCol"] ?>" />
	<input name="sortDir" type="hidden" value="<?php echo $_POST["sortDir"] ?>" />
</form>
<?php
	if (empty($numResults)) {
		echo "No Records Found.";
	}
	else {
?>
		<table cellpadding='10' class="resultTable">
			<tr class="resultTop">
				<td colspan="9">
					<input type="button" value="download" onclick="document.location = '<?=$_SERVER['PHP_SELF']?>?download=1&<?=$_SERVER['QUERY_STRING']?>'" class="link_button download_button" style="float:left" id="downloadButton">&nbsp;
				</td>
			</tr>
			<tr class="resultHeader">
				<td class="leftHeader"><a href="javascript:sortBy(0)">TechID</a></td>
                <td><a href="javascript:sortBy(1)">FirstName</a></td	>
				<td><a href="javascript:sortBy(2)">LastName</a></td>
				<td><a href="javascript:sortBy(5)">PrimaryPhone</a></td>
				<td><a href="javascript:sortBy(6)">SecondaryPhone</a></td>
				<td><a href="javascript:sortBy(7)">PrimaryEmail</a></td>
				<td><a href="javascript:sortBy(8)">SecondaryEmail</a></td>
				<td><a href="javascript:sortBy(3)">CompanyID</a></td>
			</tr>

<?php
	echo $html;
?>
			<tr>
				<td class="resultBottom" colspan="9">Results 1 - <?=$numResults?></td>
			</tr>
		</table>
<?php
	}
}
?>
</div>


<style  TYPE="text/css">
.label{ font-size:12px; color:#385C7E;}

.resultTable {
	border: 1px solid black;
	border-spacing: 0;
	text-align: center;
	color: #000000;
	padding: 0;
	border-collapse: collapse;
}

.resultTop {
	background-color: #949CCE;
	border-top: 1px solid #000000;
	border-left: 1px solid #000000;
	border-right: 1px solid #000000;
}

.leftHeader {
	border-left: 2px solid #2A497D;
}

.rightHeader {
	border-right: 2px solid #2A497D;
}

.middleHeader {
	border-left: 1px solid #FFFFFF;
	border-right: 1px solid #FFFFFF;
}

.resultHeader a, .resultRow a:hover {
	color: #2A497D;
}

.resultHeader a:hover, .resultRow a {
	color: #6373B5;
}

.resultHeader td {
	color: #000099;
	text-align: center;
	background-color: #A0A8AA;
	padding: 7px 10px;
	font-weight: bold;
	cursor: pointer;
	border-top: 2px solid #2A497D;
}

.resultRow {
	background-color: #FFFFFF;
	border: 1px solid #EAEFF5;
}

.resultRow:hover {
	background-color: #EAEFF5;
}

.leftRow {
	border-left: 2px solid #2A497D;
}

.rightRow {
	border-right: 2px solid #2A497D;
}


.resultRow td {
	padding: 2px 10px;
}

.resultBottom {
	background-color: #A0A8AA;
	border: 2px solid #2A497D;
}
</style>

<!--- End Content --->
<?php require ("../footer.php"); ?>

