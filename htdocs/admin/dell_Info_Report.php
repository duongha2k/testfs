<?php
//	ini_set("display_errors", 1);
	$page = 'admin';
	$option = 'dell_Info';
	$selected = 'Dell';
	
	require_once("../library/headerStartSession.php");

	function escapeCSVField($value) {
		$delimiter = ",";
		$enclosure = "\"";
		if (strpos($value, ' ') !== false ||
			strpos($value, $delimiter) !== false ||
			strpos($value, $enclosure) !== false ||
			strpos($value, "\n") !== false ||
			strpos($value, "\r") !== false ||
			strpos($value, "\t") !== false)
		{
			$str2 = $enclosure;
			$escaped = 0;
			$len = strlen($value);
			for ($i=0;$i<$len;$i++)
			{
				if (!$escaped && $value[$i] == $enclosure)
					$str2 .= $enclosure;
				$str2 .= $value[$i];
			}
			$str2 .= $enclosure;
			$value = $str2;
		}
		return $value;
	}

	if (isset($_GET["download"])) {
		if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE')) {
			session_cache_limiter("public");
		}
		// download csv
		require_once("../library/openSSL.php");
		require_once("../library/mySQL.php");
		require_once("../library/caspioAPI.php");
		
		$privateKey = getDellPrivateKey($_SESSION["DellInfoPW"]);
		if (!$privateKey) {
			header("Location: dell_InfoLogin.php");
			die();
		}

		header("Content-disposition: attachment; filename=DellTechInfo.csv");

		$result = mysqlQuery("SELECT TechID, FirstName, MiddleName, LastName, SSN, DOB, POBcity, POBstate, POBcountry, Sex, Race, Telephone, Email, Employer, Position, ETA, ETD FROM Dell_TechInfo " . $_SESSION["searchCriteria"]);
		$techList = array();
		$idList = array();
		while ($row = mysql_fetch_assoc($result)) {
			$techID = $row["TechID"];
			$techList[$techID] = $row;
			$techList[$techID]["SSN"] = decryptData($techList[$techID]["SSN"], $privateKey);
			$idList[] = $techID;
		}
		
		$idList = implode(",", $idList);

		echo "FSTechID,FirstName,MiddleName,LastName,SSN, DOB, POBcity, POBstate, POBcountry, Sex, Race, Telephone, Email, Employer, Position, ETA, ETD \n";
		foreach ($techList as $tech) {
			$tech['FirstName'] = escapeCSVField($tech['FirstName']);
			$tech['MiddleName'] = escapeCSVField($tech['MiddleName']);
			$tech['LastName'] = escapeCSVField($tech['LastName']);
			//$tech['MaskedSSN'] = escapeCSVField($tech['MaskedSSN']);
			$tech['DOB'] = escapeCSVField($tech['DOB']);
			$tech['POBcity'] = escapeCSVField($tech['POBcity']);
			$tech['POBstate'] = escapeCSVField($tech['POBstate']);
			$tech['POBcountry'] = escapeCSVField($tech['POBcountry']);
			$tech['Sex'] = escapeCSVField($tech['Sex']);
			$tech['Race'] = escapeCSVField($tech['Race']);
			$tech['Telephone'] = escapeCSVField($tech['Telephone']);
			$tech['Email'] = escapeCSVField($tech['Email']);
			$tech['Employer'] = escapeCSVField($tech['Employer']);
			$tech['Position'] = escapeCSVField($tech['Position']);
			$tech['ETA'] = escapeCSVField($tech['ETA']);
			$tech['ETD'] = escapeCSVField($tech['ETD']);

			echo "{$tech['TechID']},{$tech['FirstName']},{$tech['MiddleName']},{$tech['LastName']},\"{$tech['SSN']}\",\"{$tech['DOB']}\",{$tech['POBcity']},{$tech['POBstate']},{$tech['POBcountry']},{$tech['Sex']},{$tech['Race']},{$tech['Telephone']},{$tech['Email']},{$tech['Employer']},{$tech['Position']},\"{$tech['ETA']}\",\"{$tech['ETD']}\"\n";

//			echo "{$tech['TechID']},\"{$tech['FirstName']}\",\"{$tech['Initial']}\",\"{$tech['LastName']}\",\"{$tech['SSN']}\",\"{$tech['DOB']}\",\"{$tech['POBcity']}\",\"{$tech['POBstate']}\",{$tech['POBcountry']},\"{$tech['Sex']}\",\"{$tech['Race']}\",\"{$tech['Telephone']}\",\"{$tech['Email']}\",\"{$tech['Employer']}\",\"{$tech['Position']}\",\"{$tech['ETA']}\",\"{$tech['ETD']}\"\n";
		}
		die();
	}
?>

<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php
	if (!isset($_SESSION["DellInfoPW"])) header("Location: dell_InfoLogin.php");
	require_once("../library/caspioAPI.php");
	require_once("../library/mySQL.php");
	
	function parseDate($date) {
		$parts = explode("/", $date);
		if (!is_numeric($parts[0]) || !is_numeric($parts[1]) || !is_numeric($parts[2]))
			return FALSE;
		return "{$parts[2]}/{$parts[0]}/{$parts[1]}";
	}
?>
<!-- Add Content Here -->

<script type="text/javascript">
	function sortBy(num) {
		frm = document.getElementById("Dell_TechInfo");
		frm.sortDir.value = (frm.sortCol.value == num ? (frm.sortDir.value == 0 ? 1 : 0) : 0);
		frm.sortCol.value = num;
		frm.submit();
	}
	
	function gotoPage(num) {		
		frm = document.getElementById("Dell_TechInfo");
		frm.currPage.value = parseInt(num);
		frm.submit();
	}
	
	function nextPage() {
		frm = document.getElementById("Dell_TechInfo");
		gotoPage(parseInt(frm.currPage.value) + 1);
	}
	
	function prevPage() {
		frm = document.getElementById("Dell_TechInfo");
		gotoPage(parseInt(frm.currPage.value) - 1);
	}
	
</script>

<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#ChangeDateGT').calendar({dateFormat: 'MDY/'});
		$('#ChangeDateLT').calendar({dateFormat: 'MDY/'});
	});  
</script>

<style  TYPE="text/css">
.label{ font-size:12px; color:#385C7E;}

.resultTable {
	border-spacing: 0px;
	text-align: center;
	color: #000000;
	padding: 0px;
	border-collapse: collapse;
}

.resultTop {
	background-color: #949CCE;
	border-top: 1px solid #000000;
	border-left: 1px solid #000000;
	border-right: 1px solid #000000;
}

.leftHeader {
	border-left: 2px solid #2A497D;
}

.rightHeader {
	border-right: 2px solid #2A497D;
}

.middleHeader {
	border-left: 1px solid #FFFFFF;
	border-right: 1px solid #FFFFFF;
}

.resultHeader a, .resultRow a:hover {
	color: #2A497D;
}

.resultHeader a:hover, .resultRow a {
	color: #6373B5;
}

.resultHeader td {
	color: #000099;
	text-align: center; 
	background-color: #A0A8AA; 
	padding: 7px 10px;
	font-weight: bold;
	cursor: pointer;
	border-top: 2px solid #2A497D;
}

.resultRow {
	background-color: #FFFFFF;
}

.resultRow:hover {
	background-color: #EAEFF5;
}

.leftRow {
	border-left: 2px solid #2A497D;
}

.rightRow {
	border-right: 2px solid #2A497D;
}

.resultRow td {
	padding: 0px 10px;
}

.resultBottom {
	background-color: #A0A8AA;
	border: 2px solid #2A497D;
}
</style>

<br /><br />

<div align="center">
<?php
$currPage = (isset($_POST["currPage"]) ? $_POST["currPage"] : 1);
$resultsPerPage = 25;
if (!is_numeric($currPage))
$currPage = 1;
if (!isset($_POST["search"])) {
?>
<form id="Dell_TechInfo" name="Dell_TechInfo" action="<?=$_SERVER['PHP_SELF']?>" method="post">
	<table cellpadding="0" cellspacing="0" border="0" style="border:#385C7E solid medium;">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="5">
				<tr>
					<td class="label">FS Tech ID</td>
					<td><input id="TechID" name="TechID" type="text" size="13" /></td>
				</tr>
					<tr>
						<td class="label">First Name (contains)</td>
						<td><input id="FirstName" name="FirstName" type="text" size="25" /></td>
					</tr>
					<tr>
						<td class="label">Last Name (contains)</td>
						<td><input id="LastName" name="LastName" type="text" size="25" /></td>
					</tr>
					<tr>
						<td class="label">Downloaded?</td>
						<td>
                        	<input name="Downloaded" type="radio" id="Downloaded" value="Yes" tabindex="1"/>Yes
                            <input name="Downloaded" type="radio" id="Downloaded" value="No" tabindex="2"/>No
                            <input name="Downloaded" type="radio" id="Downloaded" tabindex="3" value="" checked="checked"/>
                          Any
                       </td>
					</tr>
					<tr>
						<td class="label">Approved?</td>
						<td>
                        	<input name="Approved" type="radio" id="Approved" value="Yes" tabindex="1"/>Yes
                            <input name="Approved" type="radio" id="Approved" value="No" tabindex="2"/>No
                            <input name="Approved" type="radio" id="Approved" tabindex="3" value="" checked="checked"/>
                          Any
                        </td>
				  </tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="text-align:center; background-color:#BACBDF; border-top:#385C7E solid medium; padding:5px;">
			<input id="search" name="search" type="submit" value="Search" style="background-color:#385C7E; color:#FFFFFF; font-size:12px; font-weight:bold;" />
			<input name="sortCol" type="hidden" value="0" />
			<input name="sortDir" type="hidden" value="0" />
			<input id="currPage" name="currPage" type="hidden" value="1" />
			</td>
		</tr>
	</table>
</form>
<?php
}
else {
	$TechID = $_POST["TechID"];
	$FirstName = $_POST["FirstName"];
	$LastName = $_POST["LastName"];
	$Downloaded = $_POST["Downloaded"];
	$Approved = $_POST["Approved"];

	$sort = $_POST["sortCol"];
	$dir = $_POST["sortDir"];
	switch ($sort) {
		case 0: 
			$sortQuery = "TechID";
			break;
		case 1: 
			$sortQuery = "FirstName";
			break;
		case 2: 
			$sortQuery = "LastName";
			break;
		case 3: 
			$sortQuery = "Downloaded";
			break;
		case 4: 
			$sortQuery = "Approved";
			break;
		default:
			$sortQuery = "TechID";
	}
	
	if ($sortQuery != "") {
		$sortQuery = "ORDER BY $sortQuery " . ($dir == 0 ? "ASC" : "DESC");
	}
	
	$criteria = "TechID = TechID ";
//	$criteria = "TechID = TechID ";
	if ($_POST['TechID'] != "") {
		$esc = mysqlEscape($TechID);
		$criteria .= " AND TechID = '$esc'";
	}
	if ($_POST['FirstName'] != "") {
		$esc = mysqlEscape($FirstName);
		$criteria .= " AND FirstName LIKE '%$esc%'";
	}
	if ($_POST['LastName'] != "") {
		$esc = mysqlEscape($LastName);
		$criteria .= " AND LastName LIKE '%$esc%'";
	}
	if ($_POST['Downloaded'] != "") {
		switch ($Downloaded) {
			case "Yes":
				$criteria .= " AND Downloaded = '1'";
				break;
			case "No":
				$criteria .= " AND Downloaded = '0'";
				break;
			default:
				// Default is not not filter by Downloaded.
		}
	}
	if ($_POST['Approved'] != "") {
		switch ($Approved) {
			case "Yes":
				$criteria .= " AND Approved = '1'";
				break;
			case "No":
				$criteria .= " AND Approved = '0'";
				break;
			default:
				// Default is not not filter by Approved.
		}
	
		//$criteria .= " AND Approved = '1'";
	}

	$resultsOffset = ($currPage - 1) * $resultsPerPage;

	$result = mysqlQuery("SELECT SQL_CALC_FOUND_ROWS TechID, FirstName, MiddleName, LastName, Downloaded, Approved FROM Dell_TechInfo " . ($criteria != "" ? "WHERE " . $criteria : "") . " $sortQuery LIMIT $resultsOffset, $resultsPerPage");
	
	$_SESSION["searchCriteria"] = ($criteria != "" ? "WHERE " . $criteria : "") . " $sortQuery";
		
//	echo htmlentities("SELECT SQL_CALC_FOUND_ROWS * FROM TechBankInfo " . ($criteria != "" ? "WHERE " . $criteria : "") . " $sortQuery LIMIT $resultsOffset, $resultsPerPage");
			
	$searchCount = mysqlQuery("SELECT FOUND_ROWS()");
	$countArray = mysql_fetch_row($searchCount);
	$count = $countArray[0];

	$pageSelect = "<select id='pageSelect' name='pageSelect' style='width: 4em; text-align: center' onChange='gotoPage(this.selectedIndex + 1);'>";
	$numPages = ceil($count / ($resultsPerPage * 1.0));
	for ($i = 1; $i <= $numPages; $i++) {
		$pageSelect .= "<option value='$i'" . ($currPage == $i ? " selected='selected' " :  "") . ">$i</option>";
	}
	$pageSelect .= "</select>";

?>

<form id="Dell_TechInfo" name="Dell_TechInfo" action="<?=$_SERVER['PHP_SELF']?>" method="post">
	<input id="TechID" name="TechID" type="hidden" value="<?=$TechID?>" />
	<input id="FirstName" name="FirstName" type="hidden" value="<?=$FirstName?>" />
	<input id="LastName" name="LastName" type="hidden" value="<?=$LastName?>" />
	<input id="Downloaded" name="Downloaded" type="hidden" value="<?=$Downloaded?>" />
	<input id="Approved" name="Approved" type="hidden" value="<?=$Approved?>" />
	<input name="search" type="hidden" value="Search" />
	<input name="sortCol" type="hidden" value="<?php echo $_POST["sortCol"] ?>" />
	<input name="sortDir" type="hidden" value="<?php echo $_POST["sortDir"] ?>" />
	<input id="currPage" name="currPage" type="hidden" value="<?php echo $_POST["currPage"] ?>" />
</form>

<?php
	if ($count == 0) {
		echo "No Records Found.";
	}
	else {
?>	
		<table class="resultTable">
			<tr class="resultTop"><td colspan="5"><a href="<?php echo $_SERVER['PHP_SELF'];?>?download=1">Download</a></td></tr>
			<tr class="resultHeader">
				<td class="leftHeader"><a href="javascript:sortBy(0)">TechID</a></td>
				<td class="middleHeader"><a href="javascript:sortBy(1)">First Name</a></td>
				<td class="middleHeader"><a href="javascript:sortBy(2)">Middle Name</a></td>
				<td class="middleHeader"><a href="javascript:sortBy(2)">Last Name</a></td>
				<td class="middleHeader"><a href="javascript:sortBy(3)">Downloaded</a></td>
				<td class="rightHeader"><a href="javascript:sortBy(4)">Approved</a></td>
			</tr>
<?php
	while ($row = mysql_fetch_assoc($result)) {
		echo "<tr class=\"resultRow\"><td class=\"leftRow\"><a href='dell_TechDetails.php?id={$row['TechID']}'>{$row['TechID']}</a></td><td align=\"left\">{$row['FirstName']}</td><td align=\"left\">{$row['MiddleName']}</td><td align=\"left\">{$row['LastName']}</td><td>{$row['Downloaded']}</td><td class=\"rightRow\">{$row['Approved']}</td></tr>";

	}
	
	echo "<tr><td class=\"resultBottom\" colspan='" . mysql_num_fields($result) . "'>Showing " . ($resultsOffset + 1) . "-" . min($resultsPerPage * $currPage, $count) . " of $count records found</td></tr><tr><td class=\"resultBottom\" colspan='" . mysql_num_fields($result) . "'>" . ($currPage > 1 ? "<a href='javascript:prevPage();'>&lt;</a>" : "") . " Page: " . $pageSelect . " " . ($currPage < $numPages ? "<a href='javascript:nextPage();'>&gt;</a>" : "") . "</td></tr>";

?>
	</table>
<?php
	}
}
?>
</div>

<!--- End Content --->
<?php require ("../footer.php"); ?>