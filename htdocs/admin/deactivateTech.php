<?php
//	if (!isset(argc)) die();
	require_once("../library/caspioAPI.php");
	require_once("../library/smtpMail.php");
	require ("../headerStartSession.php");

	$TechID = $_GET["TechID"];
	$Deactivate = $_GET["Deact"];
	$Username = $_SESSION['UserName'];

	date_default_timezone_set("America/Chicago");

	$fieldList = "DateTimeStamp, Tech_ID, Username, Status";
	$dateTimeStamp = date("m-d-Y H:i:s");
	
	if ($Deactivate == 'True') {
        $data = array('DeactivatedTech' => 1);
        $criteria = "TechID = '".$TechID."'";
        Core_Database::update(Core_Database::TABLE_WORK_ORDER_BIDS, $data, $criteria);

//		$records = caspioUpdate("FLS_Work_Order_Applicants", "DeactivatedTech", "'1'", "TechID = '$TechID'", false);
		$message = "The following tech has been deactivated:" . $TechID;
//		smtpMailLogReceived("Field Solutions Admin", "support@fieldsolutions.com", "gbailey@fieldsolutions.com" , "Tech Deactivated - [". $TechID ."]", $message, nl2br($message), "Tech Deactivated");
		$status = "Deactivated";
	} else {
        $data = array('DeactivatedTech' => 0);
        $criteria = "TechID = '".$TechID."'";
        Core_Database::update(Core_Database::TABLE_WORK_ORDER_BIDS, $data, $criteria);
        
//		$records = caspioUpdate("FLS_Work_Order_Applicants", "DeactivatedTech", "'0'", "TechID = '$TechID'", false);
		$message = "The following tech has been re-activated:" . $TechID;
//		smtpMailLogReceived("Field Solutions Admin", "support@fieldsolutions.com", "gbailey@fieldsolutions.com" , "Tech Re-activated - [". $TechID ."]", $message, nl2br($message), "Tech Re-activated");	
		$status = "Reactivated";
	}
	
	$valueList = "'$dateTimeStamp','$TechID','$Username','$status'";
	$result = caspioInsert("Tech_Deactivation_History", $fieldList, $valueList, false);
	/*
	echo "<p>".$fieldList."</p>";
	echo "<p>".$valueList."</p>";
	die;
	*/
?>
<script type="text/javascript">
	window.close();
</script>