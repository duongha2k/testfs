<?php
//ini_set("display_errors",1);
require_once("../headerStartSession.php");
require_once("../library/caspioAPI.php");
checkAdminLogin2();

	function escapeCSVField($value) {
		$delimiter = ",";
		$enclosure = "\"";
		if (strpos($value, ' ') !== false ||
			strpos($value, $delimiter) !== false ||
			strpos($value, $enclosure) !== false ||
			strpos($value, "\n") !== false ||
			strpos($value, "\r") !== false ||
			strpos($value, "\t") !== false)
		{
			$str2 = $enclosure;
			$escaped = 0;
			$len = strlen($value);
			for ($i=0;$i<$len;$i++)
			{
				if (!$escaped && $value[$i] == $enclosure)
					$str2 .= $enclosure;
				$str2 .= $value[$i];
			}
			$str2 .= $enclosure;
			$value = $str2;
		}
		return $value;
	}


if (isset($_GET["download"])) {
	// download attachment
	header("Content-disposition: attachment; filename=TechDupeCleanup.csv");
	
	$records = caspioSelectAdv("TR_Master_List", "TechID, FirstName, LastName, Address1, Address2, City, State, ZipCode, PrimaryPhone, SecondaryPhone, PrimaryEmail, SecondaryEmail, UserName, Password, AcceptTerms, Date_TermsAccepted, W9, FLSID, FLSBadge, NumberRankings, Qty_IMAC_Calls, Qty_FLS_Service_Calls, RegDate", "LastName = 'Bailey'", "LastName", false, "", "|");


	echo "FSTechID,FirstName,LastName,Address1,Address2,City,State,Zip,PrimaryPhone,SecondaryPhone,PrimaryEmail,SecondaryEmail,Username,Password,AcceptTerms,AcceptDate,W9,FLSID,FLSBadge,NumRankings,IMACs,ServCalls,RegDate\n";

	 foreach ($records as $tech) {
		//$tech['FirstName'] = escapeCSVField($tech['FirstName']);

 		$tech = str_replace("'","",$tech);
 		$tech = str_replace("NULL","",$tech);
 		$fields = explode("|", $tech);

 
		$techID = $fields[0];
		$FirstName = $fields[1];
		$LastName = $fields[2];
		$Address1 = $fields[3];
		$Address2 = $fields[4];
		$City =  $fields[5];
		$State = $fields[6];
		$Zip = $fields[7];
		$Phone1 = $fields[8];
		$Phone2 = $fields[9];
		$Email1 = $fields[10];
		$Email2 = $fields[11];
		$Username = $fields[12];
		$Password = $fields[13];
		$AgreeTerms = $fields[14];
		$AgreeDate = $fields[15];
		$W9 = $fields[16];
		$FLSID = $fields[17];
		$FLSBadge =  $fields[18];
		$NumberRankings = $fields[19];
		$IMACs = $fields[20];
		$ServCalls = $fields[21];
		$RegDate = $fields[22];
	
		echo "$techID,$FirstName,$LastName,$Address1,$Address2,$City,$State,$Zip,$Phone1,$Phone2,$Email1,$Email2,$Username,$Password,$AgreeTerms,$AgreeDate,W9,$FLSID,$FLSBadge,$NumberRankings,$IMACs,$ServCalls,$RegDate\n";
	
	}
	
	die();
	}

?>
 
