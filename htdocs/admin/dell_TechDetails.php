<?php
	$page = 'admin';
	$option = 'dell_Info';
	$selected = 'Dell';

	require ("../header.php");
	require ("../navBar.php");
	
	require_once("../library/openSSL.php");
	require_once("../library/mySQL.php");
	require_once("../library/caspioAPI.php");
	
	require_once("../library/headerStartSession.php");
	if (!isset($_SESSION["DellInfoPW"])) header("Location: dell_InfoLogin.php");

	$privateKey = getDellPrivateKey($_SESSION["DellInfoPW"]);
	if (!$privateKey) {
		header("Location: dell_InfoLogin.php");
		die();
	}
	
	if (!isset($_GET["id"]) || !is_numeric($_GET["id"])) die();
	$techID = $_GET["id"];

	ini_set("display_errors", 1);
	$msg = "";
	$proceed = false;
	if (isset($_POST["submit"])) {
		// Update tech info
				
		$downloaded = mysqlEscape($_POST["Downloaded"]);
		$approved = mysqlEscape($_POST["Approved"]);
		
		$insertQuery = "UPDATE Dell_TechInfo SET Downloaded = '$downloaded', Approved = '$approved' WHERE TechID = '$techID'";

		if (mysqlQuery($insertQuery)) {
			$msg = "Your information has been updated";
			header("Location: dell_Info_Report.php");

		}
	}
	
	// get tech info
	$sexId = 0;
	
	$techInfo = mysqlFetchAssoc(mysqlQuery("SELECT * FROM Dell_TechInfo WHERE TechID = $techID"));
	if ($techInfo) {
		$techInfo = $techInfo[0];	
			
		//$sex = $techInfo["Sex"];
			
		$techInfo["SSN"] = "XXX-XX-" . $techInfo["MaskedSSN"];
		$techInfo["DOB"] = date("m/d/Y", strtotime($techInfo["DOB"]));
		$techInfo["ETA"] = date("g:i A", strtotime($techInfo["ETA"]));
		$techInfo["ETD"] = date("m/d/Y", strtotime($techInfo["ETD"]));
	}
	else
	{
		$techInfo = array();
				
		require_once("../library/caspioAPI.php");
		$result = caspioSelectAdv("TR_Master_List", "FirstName, LastName", "TechID = '$techID'", "", false, "`");
		$parts = explode(",", $result[0]);
		$techInfo["FirstName"] = trim($parts[0], "`");
		$techInfo["LastName"] = trim($parts[1], "`");
			
	}
		
	$sex = array("Male", "Female");
	$sexHTML = "<option value=\"\" " . ($sexId == 0 ? "selected=\"selected\"" : "") .  ">-------</option>";
	$i = 0;
	foreach ($sex as $gender) {
		$sexHTML .= "<option value=\"$sex[$i]\" " . ($techInfo["Sex"] == $sex[$i] ? "selected=\"selected\"" : "") . ">{$sex[$i]}</option>";
		$i++;
	}

?>
<style  TYPE="text/css">
div#main td {
	text-align: left;
}

.label{ font-size:12px; color:#385C7E;}
.required {
	font-size: 12px;
	color: #FF0000;
}
.footer {
	text-align: center;
	background-color: #BACBDF; 
	border-top: #385C7E solid medium; 
	padding: 5px;
}
.caspioButton
{
/*Back Button Attributes*/
color: #ffffff;
font-size: 12px;
font-family: Verdana;
font-style: normal;
font-weight: bold;
text-align: center;
vertical-align: middle;
border-color: #5496C6;
border-style: solid;
border-width: 1px;
background-color: #032D5F;
/* Forced by default to add space between buttons */
width: auto;
height: auto;
margin: 0 3px;
}

</style>

<br /><br />

<div align="center" id="main">
	<?=$msg?>
	<form id="DellInfo" name="DellInfo" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>?id=<?=$techID?>" onSubmit="return validate()">
	<input id="TechID" name="TechID" type="hidden" value="<?=$techID?>" />
	<table id="DellInfoTable" cellpadding="0" cellspacing="0" border="0" style="border:#385C7E solid medium; width: 500px">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="5">
					<tr>
						<td class="label">First Name</td>
						<td><?php echo $techInfo['FirstName'];?></td>
					</tr>
					<tr>
						<td class="label">Middle Name</td>
						<td><?php echo $techInfo['MiddleName'];?></td>
					</tr>
					<tr>
						<td class="label">Last Name</td>
						<td><?php echo $techInfo['LastName'];?></td>
					</tr>
					<tr>
						<td class="label">SSN</td>
						<td><?php echo $techInfo['SSN'];?></td>
					</tr>
					<tr>
						<td class="label">DOB</td>
						<td><?php echo $techInfo['DOB'];?></td>
					</tr>
					<tr>
						<td class="label">POB City</td>
						<td><?php echo $techInfo['POBcity'];?></td>
					</tr>
					<tr>
						<td class="label">POB State</td>
						<td><?php echo $techInfo['POBstate'];?></td>
					</tr>
					<tr>
						<td class="label">POB Country</td>
						<td><?php echo $techInfo['POBcountry'];?></td>
					</tr>
					<tr>
						<td class="label">Sex</td>
						<td><?php echo $techInfo['Sex'];?></td>
				 	</tr>
					<tr>
						<td class="label">Race</td>
						<td><?php echo $techInfo['Race'];?></td>
					</tr>
					<tr>
						<td class="label">Telephone</td>
						<td><?php echo $techInfo['Telephone'];?></td>
					</tr>
					<tr>
						<td class="label">Email</td>
						<td><?php echo $techInfo['Email'];?></td>
					</tr>
                    
					<tr>
						<td class="label">Employer</td>
						<td><?php echo $techInfo['Employer'];?></td>
					</tr>
					<tr>
						<td class="label">Position</td>
						<td><?php echo $techInfo['Position'];?></td>
					</tr>
					<tr>
						<td class="label">ETA</td>
						<td><?php echo $techInfo['ETA'];?></td>
					</tr>
					<tr>
						<td class="label">ETD</td>
						<td><? echo $techInfo["ETD"];?></td>
					</tr>
					<tr>
						<td class="label">Downloaded?</td>
						<td><input id="Downloaded" type="radio" value="1" name="Downloaded" <?= $techInfo["Downloaded"] == 1 ? "checked='checked'" : "" ?> />Yes
                        <input id="Downloaded" type="radio" value="0" name="Downloaded" <?= $techInfo["Downloaded"] == 0 ? "checked='checked'" : "" ?> />No</td>
                    </tr>
					<tr>
						<td class="label">Approved?</td>
						<td><input id="Approved" name="Approved" type="radio" value="1" <?= $techInfo["Approved"] == 1 ? "checked='checked'" : "" ?> />Yes
                        <input id="Approved" name="Approved" type="radio" value="0" <?= $techInfo["Approved"] == 0 ? "checked='checked'" : "" ?> />No</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="footer" style="text-align: center">
				<input id="submit" name="submit" type="submit" value="Update" class="caspioButton" />
			</td>
		</tr>
	</table>
	
	</form>

</div>

<!--- End Content --->
<?php require ("../footer.php"); ?>
