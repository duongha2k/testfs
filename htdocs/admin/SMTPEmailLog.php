<?php header("Content-Type: text/html; charset=utf-8");?>
<?php $page = 'admin'; ?>
<?php $option = 'reports'; ?>
<?php $selected = 'SMTPEmailLog'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php 
	checkAdminLogin2();
?>
<!--- Add Content Here --->

<br />

<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#timestampGT').calendar({dateFormat: 'YMD-'});
	$('#timestampLT').calendar({dateFormat: 'YMD-'});
    $('#receivedTimestampGT').calendar({dateFormat: 'YMD-'});
	$('#receivedTimestampLT').calendar({dateFormat: 'YMD-'});
  });

function gotoPage(num) {
	document.forms[0].currPage.value = parseInt(num);
	document.forms[0].submit();
}

function nextPage() {
	gotoPage(parseInt(document.forms[0].currPage.value) + 1);
}

function prevPage() {
	gotoPage(parseInt(document.forms[0].currPage.value) - 1);
}

function sortBy(column) {
	if (document.forms[0].sortBy.value == column)
		document.forms[0].order.value = (document.forms[0].order.value == 1 ? 2 : 1);
	else {
		document.forms[0].sortBy.value = column;
		document.forms[0].order.value = 1;
	}
	document.forms[0].submit();
}

</script>

<div align="center">

<h1>Email Log (from SMTP server only)</h1>

<?php 
	require_once("../library/mySQL.php");
	$currPage = $_POST["currPage"];
	$resultsPerPage = 25;
	if (!is_numeric($currPage))
		$currPage = 1;
	if (!isset($_POST["search"])) {
?>

<style  TYPE="text/css">
#label{ font-size:12px; color:#385C7E;}
</style>
<form id="emailLog" name="emailLog" action="<?=$_SERVER['PHP_SELF']?>" method="post">
<table cellpadding="0" cellspacing="0" border="0" style="border:#385C7E solid medium;">
	<tr>
<td>
	<table cellpadding="0" cellspacing="5">
		<tr>
			<td id="label">LogID</td>
			<td><input id="logId" name="logId" type="text" size="25" /></td>
		</tr>
		<tr>
			<td id="label">From Name</td>
			<td><input id="vFromName" name="vFromName" type="text" size="25" /></td>
		</tr>
		<tr>
			<td id="label">From Email</td>
			<td><input id="vFromEmail" name="vFromEmail" type="text" size="25" /></td>
		</tr>
		<tr>
			<td id="label">Subject (contains)</td>
			<td><input id="vSubject" name="vSubject" type="text" size="25" /></td>
		</tr>
		<tr>
			<td id="label">eList (contains)</td>
			<td><input id="eList" name="eList" type="text" size="25" /></td>
		</tr>
		<tr>
			<td id="label">Message (contains)</td>
			<td><input id="vMessage" name="vMessage" type="text" size="25" /></td>
		</tr>
		<tr>
			<td id="label">Timestamp (>=)</td>
			<td><input id="timestampGT" name="timestampGT" type="text" size="25" /></td>
		</tr>
		<tr>
			<td id="label">Timestamp (<=)</td>
			<td><input id="timestampLT" name="timestampLT" type="text" size="25" /></td>
		</tr>
		<tr>
			<td id="label">Result (contains)</td>
			<td><input id="result" name="result" type="text" size="25" /></td>
		</tr>
		<tr>
			<td id="label">Called From (contains)</td>
			<td><input id="calledFrom" name="calledFrom" type="text" size="25" /></td>
		</tr>
		<tr>
			<td id="label">Caller</td>
			<td><input id="caller" name="caller" type="text" size="25" /></td>
		</tr>
		<tr>
			<td id="label">Received</td>
			<td><input id="receivedY" name="received" type="radio" value="1" />Yes&nbsp;<input id="receivedN" name="received" type="radio" value="0" />No&nbsp;<input id="receivedA" name="received" type="radio" value="" checked="checked" />Any&nbsp;</td>
		</tr>
		<tr>
			<td id="label">Received Timestamp (>=)</td>
			<td><input id="receivedTimestampGT" name="receivedTimestampGT" type="text" size="25" /></td>
		</tr>
		<tr>
			<td id="label">Received Timestamp (<=)</td>
			<td><input id="receivedTimestampLT" name="receivedTimestampLT" type="text" size="25" /></td>
		</tr>
	</table>
</td>
	</tr>
	<tr>
<td style="text-align:center; background-color:#BACBDF; border-top:#385C7E solid medium; padding:5px;"><input id="search" name="search" type="submit" value="Search" style="background-color:#385C7E; color:#FFFFFF; font-size:12px; font-weight:bold;" /></td>
	</tr>
     
	<input id="currPage" name="currPage" type="hidden" value="1" />
	<input id="sortBy" name="sortBy" type="hidden" value="" />
	<input id="order" name="order" type="hidden" value="1" />
</form>
</table> 

<?php
	}
	else {
		// hidden search info and display search results
?>	

<form id="emailLog" name="emailLog" action="<?=$_SERVER['PHP_SELF']?>" method="post">
	<input id="logId" name="logId" type="hidden" value="<?php echo $_POST['logId']?>" />
	<input id="vFromName" name="vFromName" type="hidden" value="<?php echo $_POST['vFromName']?>" />
	<input id="vFromEmail" name="vFromEmail" type="hidden" value="<?php echo $_POST['vFromEmail']?>" />
	<input id="vSubject" name="vSubject" type="hidden" value="<?php echo $_POST['vSubject']?>" />
	<input id="eList" name="eList" type="hidden" value="<?php echo $_POST['eList']?>" />
	<input id="vMessage" name="vMessage" type="hidden" value="<?php echo $_POST['vMessage']?>" />
	<input id="timestampGT" name="timestampGT" type="hidden" value="<?php echo $_POST['timestampGT']?>" />
	<input id="timestampLT" name="timestampLT" type="hidden" value="<?php echo $_POST['timestampLT']?>" />
	<input id="result" name="result" type="hidden" value="<?php echo $_POST['result']?>" />
	<input id="calledFrom" name="calledFrom" type="hidden" value="<?php echo $_POST['calledFrom']?>" />
	<input id="caller" name="caller" type="hidden" value="<?php echo $_POST['caller']?>" />
	<input id="received" name="received" type="hidden" value="<?php echo $_POST['received']?>" />
	<input id="receivedTimestampGT" name="receivedTimestampGT" type="hidden" value="<?php echo $_POST['receivedTimestampGT']?>" />
	<input id="receivedTimestampLT" name="receivedTimestampLT" type="hidden" value="<?php echo $_POST['receivedTimestampLT']?>" />
	<input id="currPage" name="currPage" type="hidden" value="<?php echo $_POST['currPage']?>" />
	<input id="sortBy" name="sortBy" type="hidden" value="<?php echo mysqlEscape($_POST['sortBy'])?>" />
	<input id="order" name="order" type="hidden" value="<?php echo $_POST['order']?>" />
	<input id="search" name="search" type="hidden" value="search" />
</form>


<?php
//		print_r($_POST);
		// perform search
		$received = array("No", "Yes");
		$criteria = "id = id ";
		if ($_POST['logId'] != "") {
			$esc = mysqlEscape($_POST['logId']);
			$criteria .= " AND id = '$esc'";
		}
		
		if ($_POST['vFromName'] != "") {
			$esc = mysqlEscape($_POST['vFromName']);
			$criteria .= " AND vFromName = '$esc'";
		}

		if ($_POST['vFromEmail'] != "") {
			$esc = mysqlEscape($_POST['vFromEmail']);
			$criteria .= " AND vFromEmail = '$esc'";
		}

		if ($_POST['vSubject'] != "") {
			$esc = mysqlEscape($_POST['vSubject']);
			$criteria .= " AND vSubject LIKE '%$esc%'";
		}
		
		if ($_POST['eList'] != "") {
			$esc = mysqlEscape($_POST['eList']);
			$criteria .= " AND eList LIKE '%$esc%'";
		}

		if ($_POST['vMessage'] != "") {
			$esc = mysqlEscape($_POST['vMessage']);
			$criteria .= " AND vMessage LIKE '%$esc%'";
		}

		if ($_POST['timestampGT'] != "") {
			$date = date("Y-m-d", strtotime($_POST['timestampGT']));
			$criteria .= " AND timestamp >= '$date 00:00:00'";
		}
		
		if ($_POST['timestampLT'] != "") {
			$date = date("Y-m-d", strtotime($_POST['timestampLT']));
			$criteria .= " AND timestamp <= '$date 23:59:59'";
		}

		if ($_POST['result'] != "") {
			$esc = mysqlEscape($_POST['result']);
			$criteria .= " AND result LIKE '%$esc%'";
		}

		if ($_POST['calledFrom'] != "") {
			$esc = mysqlEscape($_POST['calledFrom']);
			$criteria .= " AND calledFrom LIKE '%$esc%'";
		}

		if ($_POST['caller'] != "") {
			$esc = mysqlEscape($_POST['caller']);
			$criteria .= " AND caller = '$esc'";
		}

		if ($_POST['received'] != "") {
			$esc = mysqlEscape($_POST['received']);
			$criteria .= " AND received = '$esc'";
		}
		
		if ($_POST['receivedTimestampGT'] != "") {
			$date = date("Y-m-d", strtotime($_POST['receivedTimestampGT']));
			$criteria .= " AND receivedTimestamp >= '$date 00:00:00'";
		}
		
		if ($_POST['receivedTimestampLT'] != "") {
			$date = date("Y-m-d", strtotime($_POST['receivedTimestampLT']));
			$criteria .= " AND receivedTimestamp <= '$date 23:59:59'";
		}
		
		$resultsOffset = ($currPage - 1) * $resultsPerPage;
		$sortBy = ($_POST["sortBy"] != "" ? " ORDER BY " . mysqlEscape($_POST["sortBy"]) . ($_POST["order"] == 1 ? " ASC" : " DESC")  : "");
		$search = mysqlQuery("SELECT SQL_CALC_FOUND_ROWS * FROM SMTPEmailLog " . ($criteria != "" ? "WHERE " . $criteria : "") . "$sortBy LIMIT $resultsOffset, $resultsPerPage");
		$searchCount = mysqlQuery("SELECT FOUND_ROWS()");
		$countArray = mysql_fetch_row($searchCount);
		$count = $countArray[0];
//		echo "SELECT * FROM SMTPEmailLog " . ($criteria != "" ? "WHERE " . $criteria : "") . "$sortBy LIMIT $resultsOffset, $resultsPerPage";
		$pageSelect = "<select id='pageSelect' name='pageSelect' style='width: 4em; text-align: center' onChange='gotoPage(this.selectedIndex + 1);'>";
		$numPages = ceil($count / ($resultsPerPage * 1.0));
		for ($i = 1; $i <= $numPages; $i++) {
			$pageSelect .= "<option value='$i'" . ($currPage == $i ? " selected='selected' " :  "") . ">$i</option>";
		}
		$pageSelect .= "</select>";
		$html = "<style type='text/css'>table { border-spacing: 0px } td { border: solid 1px #444444; padding: 0px 5px; }</style>";
		$row = mysql_fetch_assoc($search);

		if (!$row) { 
			$html .= "No Records Found";
		}
		else {
			$html .= "<table style='text-align: center'>";
			// display column labels
			$html .= "<tr>";
			foreach ($row as $key => $value) {
				$html .= "<td><a href=\"javascript:sortBy('$key')\">$key</a></td>";
			}		
			$html .= "</tr>";
	
			do {
				$html .= "<tr>";
				foreach ($row as $key => $value) {
					$valueMap = $$key;
					$v = (isset($$key) && isset($valueMap[$value]) ? $valueMap[$value] : $value);				
					$v = (strlen($v) > 75 ? wordwrap($v, 75, "<br />", TRUE) : $v);
					$html .= "<td>$v</td>";
				}		
				$html .= "</tr>";
			} while ($row = mysql_fetch_assoc($search));
			$html .= "<tr><td colspan='" . mysql_num_fields($search) . "'>Showing " . ($resultsOffset + 1) . "-" . min($resultsPerPage * $currPage, $count) . " of $count records found</td></tr><tr><td colspan='" . mysql_num_fields($search) . "'>" . ($currPage > 1 ? "<a href='javascript:prevPage();'>&lt;</a>" : "") . " Page: " . $pageSelect . " " . ($currPage < $numPages ? "<a href='javascript:nextPage();'>&gt;</a>" : "") . "</td></tr></table>";
		}
		echo $html;
	}
?>
</div>

<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
