<?php $page = 'admin'; ?>
<?php $option = 'ISO'; ?>
<?php $selected = 'ISOList';
require ("../header.php");
require ("../navBar.php");
checkAdminLogin2();
?>
<script type="text/javascript" src="/widgets/js/jquery.min.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetISODetails.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
<script type="text/javascript" src="/widgets/js/functions.js"></script>
<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
<link rel="stylesheet" href="/widgets/css/popup.css" type="text/css" />
<script type="text/javascript" src="/widgets/js/jquery.formatCurrency-1.4.0.js"></script>
<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>

<script type="text/javascript">
function calendarInit() {
    $('#ClientExpiration').click( function() {
        $('#calendar_div').css('z-index', ++roll.zindex);
        $(this).calendar({dateFormat:'MDY/'});
        $('#ClientExpiration').blur();
    });
}
function calendarMove( input ) {
    $('#calendar_div').css('top', $(input).offset().top + $(input).height() + 6 + 'px');
    $('#calendar_div').css('left', $(input).offset().left + 'px');
    popUpCal.showFor(input);
}
var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";

var roll;
var detailsWidget;

$('document').ready(function(){
    roll = new FSPopupRoll();
    detailsWidget = new FSWidgetISODetails({container:'detailsContainer',tab:'update'},roll);
    detailsWidget.show({params:{id:'<?php echo $_GET['id'];?>'}});
});
</script>

<div align="center">
<br/>
<div id="detailsContainer"></div>
</div>

<!--- End Content --->
<?php require ("../footer.php"); ?>