<?php $page = 'admin'; ?>
<?php $option = 'client'; ?>
<?php $selected = 'clientprofileedit'; ?>

<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<script src="../library/ui.datepicker.js"></script>
<script src="/widgets/js/base64.js"></script>
<script src="/widgets/js/FSWidget.js"></script>
<script src="/widgets/js/FSPopupAbstract.js"></script>
<script src="/widgets/js/FSPopupRoll.js"></script>

<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
<script type="text/javascript" src="js/admin_client_profile.js"></script>
<link href="../library/jquery/calendar/ui.datepicker.css" rel="stylesheet" />
<script src="/widgets/js/FSWidgetClientDetails.js?042620111"></script>
<?php
checkAdminLogin2();
?>
<script type="text/javascript">
var wManager = new FSWidgetManager();
var roll = new FSPopupRoll();
</script>
<!--- Add Content Here --->
<style>
    .contentBodyTab
    {
        margin-left:auto; margin-right:auto; width:100%; 
        height:100%; clear: left;
    }
    #clientProfile .form_table
    {
        float: left;
    }
    .borderStyleDiv
    {
        width:70%;
    }
    .form_tableV2 td {
        padding: 2px 5px;
        vertical-align: top;
    }
    #clientProfile .form_tableV2 {
        float: left;
    }
    .popup_form_holder .form_tableV2 {
        margin: 0 auto;
        text-align: left;
    }
    .form_tableV2 {
        margin: 0 auto;
        text-align: left;
        font-size: 12px;
        border-color: #666666;
        border-style: solid;
        border-width: 0 1px 1px;
    }
    .form_tableV2 .tbPt {
        border-color: #666666;
        border-style: solid;
        border-width: 0px 0px 1px 0px;
    }
    ul.tabs a:hover {
      text-decoration: underline;
      background-color: #FFC24B;
    }
    ul.tabs a.current {
        background-color: #F69630;
        border-bottom: 1px solid #F69630;
        color: #ffffff;
        cursor: default;
        padding-top: 5px;
        height: 22px;
        top: 0;
    }
    ul.tabs a {
        color:#385C7E;
        padding: 5px 10px 6px;
        border-width: 0;
    }
    div.tabsbottom {
        height: 4px;
    }
</style>
<div id="clientProfile">
    <div style="height:38px;" id="tabbedMenuNew">
        <center>
            <ul class="tabs" style="width:480px;border: 0px;">
                <li class="currenttab" tabid="detailsContainer" style="width:120px;"><a class="current" href="javascript:void(0)" style="width:99px; font-size: 12px;">Client Profile</a></li>
                <li tabid="clientQuickTickets" style="width:120px;"><a style="width:99px; font-size: 12px;" href="javascript:void(0)">QuickTicket</a></li>
                <li tabid="clientUsers" style="width:120px;"><a style="width:99px; font-size: 12px;" href="javascript:void(0)">Users</a></li>
                <li tabid="clientAccessSettings" style="width:120px;"><a href="javascript:void(0)" style="width:99px; font-size: 12px;">Access Settings</a></li>    
                <li tabid="clientProjectSettings" style="width:110px;display: none;"><a href="javascript:void(0)">Project Settings</a></li>
                <li tabid="clientReportSettings" style="width:110px;display: none;"><a href="javascript:void(0)">Report Settings</a></li>
                <li tabid="clientFinancialStatements" style="width:120px;display: none;"><a href="javascript:void(0)">FinancialStatements</a></li>
            </ul>
            <div class="tabsbottom borderStyleDiv" style="border: 0px;"></div>
        </center> 
    </div>
    <div id="detailsContainer" class="contentBodyTab borderStyleDiv">
    </div>
    <div id="clientQuickTickets"  class="contentBodyTab borderStyleDiv" style="display:none;">

    </div>  
    <div id="clientAccessSettings" class="contentBodyTab borderStyleDiv" style="display:none;">

    </div> 
     <div id="clientUsers"  class="contentBodyTab borderStyleDiv" style="display:none;">
    </div> 
    <div id="clientProjectSettings" class="contentBodyTab" style="display:none;">
        <table class="form_table" style="margin-top: 0px;height:200px;" width="65%">
            <tr>
                <td align="center">
                    Project Settings
                </td>
            </tr>
        </table>
    </div>  
    <div id="clientReportSettings" class="contentBodyTab" style="display:none;">
        <table class="form_table" style="margin-top: 0px;height:200px;" width="65%">
            <tr>
                <td align="center">
                    Report Settings
                </td>
            </tr>
        </table>
    </div>  
    <div id="clientFinancialStatements" class="contentBodyTab" style="display:none;">
        <table class="form_table" style="margin-top: 0px;height:200px;" width="65%">
            <tr>
                <td align="center">
                    Financial Statements
                </td>
            </tr>
        </table>
    </div>  
    <div style="clear: both;">

    </div>
</div>
<?php
    $user = new Core_Api_User();
    $clientInfo = $user->loadById(
                    $_REQUEST['client_id']
    );
    $companyname = $clientInfo->Company_ID;
?>
<script>

    var _admin_client_profile = admin_client_profile.CreateObject({
        id:'_admin_client_profile',
        client_id:'<?= isset($_REQUEST['client_id']) ? $_REQUEST['client_id'] : 0 ?>',
        companyId : '<?= $companyname ?>',
        tabid:'<?= isset($_REQUEST['tabid'])?$_REQUEST['tabid']:"detailsContainer" ?>'
    }); 
</script>


<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->

