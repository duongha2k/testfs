<?php

require ("../../headerStartSession.php");

$csv_filename = "PhoneAuditLog_" . date('Ymd_hms') . ".csv";
header("Content-disposition: attachment; filename=$csv_filename");

echo "\"Date Reported\",\"Current Status\",\"Invalid Phone Number\",\"Day/Evening\",\"Tech ID\",\"Tech Name\",\"Reporting Client\",\"Reporting User\" \n";
$params = $_GET;
$datainput = Array("ReportedStartDate" => empty($params['ReportedStartDate'])?"":date_format(new DateTime($params['ReportedStartDate']),'Y-m-d')
            , "ReportedEndDate" => empty($params['ReportedEndDate'])?"":date_format(new DateTime($params['ReportedEndDate']),'Y-m-d')
            , "IncludeValidatedPhone" => $params['IncludeValidatedPhone'] == "on" ? 1 : 0
            , "PhoneNumber" => $params['PhoneNumber']
            , "PhoneType" => $params['PhoneType']
            , "TechID" => $params['TechID']
            , "TechLastName" => $params['TechLastName']
            , "TechFisrtName" => $params['TechFisrtName']
            , "ReportingClientId" => $params['ReportingClientId']
            , "ReportingUser" => $params['ReportingUser']
        );

$columnsort =  urldecode($params['columnsort']);
$typesort = urldecode($params['typesort']);
$sort = $columnsort . ' ' . $typesort;
    
$Core_Api_TechClass = new Core_Api_TechClass;
$result = $Core_Api_TechClass->getPhoneAuditLog($datainput, $sort);

if (isset($result) && count($result) > 0) {
    $length = count($result);
    for ($i = 0; $i < $length; $i++) {
        $auditdate= $result[$i]['auditdate'];
        $auditdate = new DateTime($auditdate);
        $auditdate = $auditdate->format("m/d/Y");
        
        $CurrentStatus = $result[$i]['CurrentStatus']== 1 ? "Valid" : "Invalid";
        
        $techphonevalue = $result[$i]['techphonevalue'];
        
        $phonetype = $result[$i]['phonetype']== 1 ? "Day" : "Evening";
        
        $techid = $result[$i]['techid'];
        
        $FirstName = $result[$i]['FirstName'].' '.$result[$i]['LastName'];
        
        $reportingclient = $result[$i]['reportingclient'];
        
        $reportinguser = $result[$i]['reportinguser'];
        
        $csv = "\"" . $auditdate . "\",\""
                . $CurrentStatus . "\",\""
                . $techphonevalue . "\",\""
                . $phonetype . "\",\""
                . $techid . "\",\""
                . $FirstName . "\",\""
                . $reportingclient . "\",\""
                . $reportinguser . "\" \n";
        echo($csv);
    }
}
 
 
?>
