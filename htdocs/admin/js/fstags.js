//-------------------------------------------------------------
//-------------------------------------------------------------
fstags.Instances = null;
//-------------------------------------------------------------
fstags.CreateObject = function(config) {
    var obj = null;
    if (fstags.Instances != null) {
        obj = fstags.Instances[config.id];
    }
    if (obj == null) {
        if (fstags.Instances == null) {
            fstags.Instances = new Object();
        }
        obj = new fstags(config);
        fstags.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function fstags(config) {
    var Me = this;
    this.id = config.id;
    this.chkmultipletaglevels = config.chkmultipletaglevels;
    this.hidNumberItem = config.hidNumberItem;

    this.addStringSubLevel = function(isFirst, isLast)
    {
        var str = '';
        str = '<tr class="trstyle trSubLevel_NumberLevel">'
                + '<td class="col1" colspan="2">'
                + '<b>Level <span class="titleNumber">NumberLevel</span></b> '
                + '<span class="spanDelete">(<a href="javascript:;" class="deleteSubLevel" vxitemLevel="NumberLevel" style="cursor:pointer;">delete level</a>)</span>';
        if (isFirst == 0 && isLast == 1)
        {
            str += '<span class="spanMoveUp">(<a href="javascript:;" class="moveupSubLevel" vxitemLevel="NumberLevel" style="cursor:pointer;">move up</a>)</span>';
        }
        if (isFirst == 1 && isLast == 0)
        {
            str += '<span class="spanMoveDown">(<a href="javascript:;" class="movedownSubLevel" vxitemLevel="NumberLevel" style="cursor:pointer;">move down</a>)</span>';
        }
        if (isFirst == 0 && isLast == 0)
        {
            str += '<span class="spanMoveUp">(<a href="javascript:;" class="moveupSubLevel" vxitemLevel="NumberLevel" style="cursor:pointer;">move up</a>)</span>';
            str += '<span class="spanMoveDown">(<a href="javascript:;" class="movedownSubLevel" vxitemLevel="NumberLevel" style="cursor:pointer;">move down</a>)</span>';
        }
        str += '</td>'
                + '</tr>'
                + '<tr class="trstyle trSubLevel_NumberLevel">'
                + '<td class="col1" width="20%">'
                + '<span style="color:red">*</span> Enter Level <span class="titleNumber">NumberLevel</span> Tag Label'
                + '</td>'
                + '<td class="col2">'
                + '<input type="text" id="tagTitleLevel_NumberLevel" name="tagTitleLevel_NumberLevel" >'
                + '</td>'
                + '</tr>'
                + '<tr class="trstyle trSubLevel_NumberLevel">'
                + '<td class="col1">'
                + '<span style="color:red">*</span> Upload Tag Art (PNG files only)<br/>'
                + '<div style="text-align:right;">(Max height: 26px)</div><!--829-->'
                + '</td>'
                + '<td class="col2">'
                + '<input type="file" id="TagArt_NumberLevel" name="TagArt_NumberLevel" accept="image/png"><a style="cursor: pointer;" class="addSubLevel" href="javascript:;" vxitemLevel="NumberLevel">+ Add a Level</a>'
                + '</td>'
                + '</tr>';
        return str;
    }

    this.reducedItem = function(level) {
        var reduced = level - 1;
        Me.changeItem(level, reduced);
    }

    //------------------------------------------------------------  
    this.increasePossition = function(level) {
        var inscrease = level + 1;
        Me.changeItem(level, inscrease);
    }

    //------------------------------------------------------------  
    this.swapItem = function(from, to) {
        var swap = parseInt($("#hidNumberItem").val()) + 1;
        Me.changeItem(from, swap);
        Me.changeItem(to, from);
        Me.changeItem(swap, to);
    }

    //------------------------------------------------------------  
    this.changeItem = function(from, to)
    {
        $(".trSubLevel_" + from).each(function() {
            $(this).removeClass("trSubLevel_" + from)
                    .addClass("trSubLevel_" + to);
            $(this).find(".titleNumber").each(function() {
                $(this).html(to);
            })
            $(this).find(".deleteSubLevel").attr("vxitemLevel", to);
            $(this).find(".moveupSubLevel").attr("vxitemLevel", to);
            $(this).find(".movedownSubLevel").attr("vxitemLevel", to);
            $(this).find(".addSubLevel").attr("vxitemLevel", to);
            $(this).find("input").each(function() {
                var id = $(this).attr("id");
                var arrID = id.split("_");
                $(this)
                        .attr("id", arrID[0] + "_" + to)
                        .attr("name", arrID[0] + "_" + to)
            })
        })
    }

    //------------------------------------------------------------
    this.addItem = function(level, isFirst, isLast) {
        var szItem = Me.addStringSubLevel(isFirst, isLast).replace(/NumberLevel/g, level);
        $(".tbmultipletaglevels").append(szItem);
    }
    //------------------------------------------------------------
    this.clearData = function() {
        $(".tbmultipletaglevels").empty();
        $(Me.hidNumberItem).val(3);

    }
    
    //------------------------------------------------------------
    this.addSubLevel_onClick = function() {
        var itemLevel = 0;
        try {
            itemLevel = parseInt($(this).attr("vxitemlevel"));
        }
        catch (ex) {
            itemLevel = 0;
        }
        //931
        var trParent = $(this).parent().parent().parent();
        //end 931
        var numberitem = 0;
        try {
            numberitem = parseInt($(Me.hidNumberItem).val());
        }
        catch (ex) {
            numberitem = 0;
        }

        for (var i = numberitem; i > itemLevel; i--)
        {
            Me.increasePossition(i);
        }

        //add new level
        var inscrease = itemLevel + 1
        var szItem = "";
        if (inscrease == (numberitem + 1))
        {
            $(".trSubLevel_" + (itemLevel)).first().find("td").first().append('<span class="spanMoveDown">(<a href="javascript:;" class="movedownSubLevel" vxitemLevel="NumberLevel" style="cursor:pointer;">move down</a>)</span>');
            szItem = Me.addStringSubLevel(0, 1).replace(/NumberLevel/g, inscrease);
        }
        else
        {
            szItem = Me.addStringSubLevel(0, 0).replace(/NumberLevel/g, inscrease);
        }
        $(szItem).insertAfter(trParent);

        numberitem = numberitem + 1;
        $(Me.hidNumberItem).val(numberitem);

        //Rebind Event
        Me.BindEventSubLevel();
    }

    //------------------------------------------------------------
    this.deleteSubLevel_onClick = function() {
        var itemLevel = 0;
        try {
            itemLevel = parseInt($(this).attr("vxitemlevel"));
        }
        catch (ex) {
            itemLevel = 0;
        }

        //delete item
        $(".trSubLevel_" + itemLevel).remove();

        var numberitem = 0;
        try {
            numberitem = parseInt($(Me.hidNumberItem).val());
        }
        catch (ex) {
            numberitem = 0;
        }
        for (var i = itemLevel + 1; i <= numberitem; i++)
        {
            Me.reducedItem(i);
        }
        //remove movedown/moveup
        if (itemLevel == numberitem)
        {
            $(".trSubLevel_" + (itemLevel - 1)).first().find("td").first().find("span").last().remove();
        }
        else if (itemLevel == 1)
        {
            $(".trSubLevel_" + (itemLevel)).first().find("td").first().find("span.spanMoveUp").remove();
        }

        numberitem = numberitem - 1;
        $(Me.hidNumberItem).val(numberitem);
        if (numberitem == 0)
        {
            $(Me.chkmultipletaglevels).removeAttr("checked");
        }
        if ($(".tbmultipletaglevels tr").length <= 0)
        {
            $(".trmultipletaglevelsunchecked").css("display", "");
            $(".trmultipletaglevelschecked").css("display", "none");
            Me.clearData();
        }
    }

    this.moveupSubLevel_onClick = function()
    {
        var itemLevel = 0;
        try {
            itemLevel = parseInt($(this).attr("vxitemlevel"));
        }
        catch (ex) {
            itemLevel = 0;
        }
        Me.moveobj(itemLevel, 1);
    }

    //------------------------------------------------------------
    this.movedownSubLevel_onClick = function()
    {
        var itemLevel = 0;
        try {
            itemLevel = parseInt($(this).attr("vxitemlevel"));
        }
        catch (ex) {
            itemLevel = 0;
        }
        Me.moveobj(itemLevel, 2);
    }
    
    //------------------------------------------------------------
    //direction = 1 Up
    //direction = 2 Down
    this.moveobj = function(level, direction)
    {
        var numberitem = 0;
        try {
            numberitem = parseInt($(Me.hidNumberItem).val());
        }
        catch (ex) {
            numberitem = 0;
        }
        
        if (direction == 2)
        {
            level = level + 1;
        }
        if ((level - 1) > 0)
        {
            $(".trSubLevel_" + level).each(
                    function()
                    {
                        $($(this)).insertBefore($(".trSubLevel_" + (level - 1)).first());
                    }
            );
            Me.swapItem(level, level - 1);
            
            if(level==numberitem)
            {
                $(".trSubLevel_" + (level)).first().find("td").first().find("span.spanMoveUp").remove();
                $(".trSubLevel_" + (level)).first().find("td").first().find("span.spanMoveDown").remove();
                $(".trSubLevel_" + (level)).first().find("td").first().append('<span class="spanMoveUp">(<a href="javascript:;" class="moveupSubLevel" vxitemLevel="'+level+'" style="cursor:pointer;">move up</a>)</span>');
                if((level - 1)==1)
                {
                    $(".trSubLevel_" + (level - 1)).first().find("td").first().find("span.spanMoveUp").remove();
                    $(".trSubLevel_" + (level - 1)).first().find("td").first().append('<span class="spanMoveDown">(<a href="javascript:;" class="movedownSubLevel" vxitemLevel="'+(level-1)+'" style="cursor:pointer;">move down</a>)</span>');
                }
                else
                {
                    $(".trSubLevel_" + (level - 1)).first().find("td").first().append('<span class="spanMoveDown">(<a href="javascript:;" class="movedownSubLevel" vxitemLevel="'+(level-1)+'" style="cursor:pointer;">move down</a>)</span>');
                }
            }
            else if((level - 1)==1)
            {
                $(".trSubLevel_" + (level)).first().find("td").first().find("span.spanMoveUp").remove();
                $(".trSubLevel_" + (level)).first().find("td").first().find("span.spanMoveDown").remove();
                $(".trSubLevel_" + (level)).first().find("td").first().append('<span class="spanMoveUp">(<a href="javascript:;" class="moveupSubLevel" vxitemLevel="'+level+'" style="cursor:pointer;">move up</a>)</span>');
                $(".trSubLevel_" + (level)).first().find("td").first().append('<span class="spanMoveDown">(<a href="javascript:;" class="movedownSubLevel" vxitemLevel="'+level+'" style="cursor:pointer;">move down</a>)</span>');
                $(".trSubLevel_" + (level - 1)).first().find("td").first().find("span.spanMoveUp").remove();
                $(".trSubLevel_" + (level - 1)).first().find("td").first().find("span.spanMoveDown").remove();
                $(".trSubLevel_" + (level - 1)).first().find("td").first().append('<span class="spanMoveDown">(<a href="javascript:;" class="movedownSubLevel" vxitemLevel="'+(level - 1)+'" style="cursor:pointer;">move down</a>)</span>');
            }
            
            
        }
        Me.BindEventSubLevel();
    }
    //------------------------------------------------------------

    //------------------------------------------------------------
    this.BindEventSubLevel = function() {
        $(".addSubLevel")
                .unbind("click", Me.addSubLevel_onClick)
                .bind("click", Me.addSubLevel_onClick);
        $(".deleteSubLevel")
                .unbind("click", Me.deleteSubLevel_onClick).bind("click", Me.deleteSubLevel_onClick);
        $(".moveupSubLevel")
                .unbind("click").bind("click", Me.moveupSubLevel_onClick);
        $(".movedownSubLevel")
                .unbind("click").bind("click", Me.movedownSubLevel_onClick);
        //931
        $(Me.chkmultipletaglevels)
                .unbind("click", Me.chkmultipletaglevels_onClick)
                .bind("click", Me.chkmultipletaglevels_onClick);
        //end 931
    }
    //------------------------------------------------------------ 
    this.buildSubLevelTag = function() {
        var numberitem = 0;
        try {
            numberitem = parseInt($(Me.hidNumberItem).val());

        }
        catch (ex) {
            numberitem = 0;
        }

        for (var i = 0; i < numberitem; i++)
        {
            if (i == 0)
            {
                Me.addItem(i + 1, 1, 0);
            }
            else if (i == (numberitem - 1))
            {
                Me.addItem(i + 1, 0, 1);
            }
            else
            {
                Me.addItem(i + 1, 0, 0);
            }

        }
        Me.BindEventSubLevel();

    }
    
    //------------------------------------------------------------
    this.chkmultipletaglevels_onClick = function() {
        var isChecked = $(this).is(":checked");
        if (isChecked)
        {
            $(".trmultipletaglevelsunchecked").css("display", "none");
            $(".trmultipletaglevelschecked").css("display", "");
            Me.buildSubLevelTag();
        }
        else
        {
            $(".trmultipletaglevelsunchecked").css("display", "");
            $(".trmultipletaglevelschecked").css("display", "none");
            Me.clearData();
        }
    }
    //931
    //------------------------------------------------------------
    this.selectnewtag = function()
    {
        var el = $(this).parent().parent();
        $(el).find(".divcanvel").css("display","")
        $(el).find(".uploadimg").css("display","");
        $(el).find(".selectnewtag").css("display","none");
        $(el).find(".tagname").css("display","none");

        if(!$("#multipletaglevelsedit").is(":checked"))
        {
            $(".resultsTable").css("display","none");
        }
    }
    //------------------------------------------------------------
    this.canceltag = function()
    {
        var el = $(this).parent().parent().parent();
        $(el).find(".divcanvel").css("display","none")
        $(el).find(".uploadimg").css("display","none");
        $(el).find(".selectnewtag").css("display","");
        $(el).find(".tagname").css("display","");

        if(!$("#multipletaglevelsedit").is(":checked"))
        {
            $(".resultsTable").css("display","");
        }
    }
    //end 931
    //------------------------------------------------------------ 
    this.initPage = function() {
        var isChecked = $(Me.chkmultipletaglevels).is(":checked");

        if (isChecked)
        {
            $(".trmultipletaglevelsunchecked").css("display", "none");
            $(".trmultipletaglevelschecked").css("display", "");
        }
        else
        {
            $(".trmultipletaglevelsunchecked").css("display", "");
            $(".trmultipletaglevelschecked").css("display", "none");
            Me.clearData();
        }
        //931
        Me.BindEventSubLevel();
        //end 931
    }
    //------------------------------------------------------------  
    this.init = function() {
            
    }
}