function showTechsField() {
	return $("input#EditRecordShowTechs");
}

runPricing = false;
function calculateBtnClick() {
	if (!$("#EditRecordApproved").attr("checked")) {
		alert("The work order must be approved before calculating invoice amount");
		return false;
	}
	runPricing = true;
	if (UpdateCaspio()) {
		$("#caspioform").submit();
	}
}

$(document).ready(
	function () {
		try {
			oldWorkOrderReviewed = $("#EditRecordWorkOrderReviewed").attr("checked");
			oldWorkOrderConfirmed = $("#EditRecordTechCheckedIn_24hrs").attr("checked");
			oldTechMarkedComplete = $("#EditRecordTechMarkedComplete").attr("checked");
			oldApproved = $("#EditRecordApproved").attr("checked");
			oldDeactivated = $("#EditRecordDeactivated").attr("checked");
			oldTech_ID = $("#EditRecordTech_ID").attr("value");
			oldShowTechs = $("#EditRecordShowTechs").attr("checked");
			oldInvoiced = $("#EditRecordInvoiced").attr("checked");
			$("#calculateBtn").click(calculateBtnClick);
			// set / reset pricing ran according to WO state
			$("#EditRecordPricingRan").attr("checked", ($("#EditRecordPricingRan").attr("checked") && $("#EditRecordApproved").attr("checked") ? true : false)); 
//			$("#EditRecordPricingRan").attr("value", pricingRan && $("#EditRecordApproved").attr("checked") ? "Y" : "N");
		}
		catch (e) {}
	}
);

function UpdateCaspio() {
	var theProject = document.getElementById("EditRecordProject_ID");
	if(theProject.selectedIndex != 0){
		var option = theProject.options[theProject.selectedIndex].text;
		document.forms.caspioform.EditRecordProject_Name.value = option;
	} else{
		document.forms.caspioform.EditRecordProject_Name.value = "";
	}

	// time stamp variables
	var woID = $("#GetTB_UNID").attr("value");
	var clientName = $("#EditRecordCompany_Name").attr("value");
	var projectName = $("#EditRecordProject_Name").attr("value");
	var techComplete = $("#EditRecordTechMarkedComplete").attr("checked");
	
	woID = (woID == undefined ? "" : woID);
	clientName = (clientName == undefined ? "" : clientName);
	projectName = (projectName == undefined ? "" : projectName);
	
	// set / reset pricing ran according to WO state
	$("#EditRecordPricingRan").attr("checked", ($("#EditRecordPricingRan").attr("checked") && $("#EditRecordApproved").attr("checked") ? true : false)); 
//	$("#EditRecordPricingRan").attr("value", pricingRan && $("#EditRecordApproved").attr("checked") ? "Y" : "N");

	if (runPricing || (!oldInvoiced && $("#EditRecordInvoiced").attr("checked")) )
		// run pricing calc for newly invoiced or when calc button clicked
		queuePricingCalculation(woID);
	
	// time stamps
	if (oldShowTechs != showTechsField().attr("checked") && showTechsField().attr("checked") == true) {
		// Work Order Published
		queueCreateTimeStamp(woID, "Work Order Published", Company_ID, clientName, projectName, "", "", "");
	}
	
	if (oldTech_ID != $("#EditRecordTech_ID").attr("value") && $("#EditRecordTech_ID").attr("value") != undefined) {
		// Assigned
		queueCreateTimeStamp(woID, "Work Order Assigned", Company_ID, clientName, projectName, "", "", "");
	}
		
	if (oldWorkOrderReviewed != $("#EditRecordWorkOrderReviewed").attr("checked") && $("#EditRecordWorkOrderReviewed").attr("checked")) {
		// Reviewed
		queueCreateTimeStamp(woID, "Work Order Reviewed", Company_ID, clientName, projectName, "", "", "");
	}
	
	if (oldWorkOrderConfirmed != $("#EditRecordTechCheckedIn_24hrs").attr("checked") && $("#EditRecordTechCheckedIn_24hrs").attr("checked")) {
		// Confirmed
		queueCreateTimeStamp(woID, "Work Order Confirmed", Company_ID, clientName, projectName, "", "", "");
	}
	
	if (oldTechMarkedComplete != techComplete) {
		// Tech Mark Complete
		var oldVal = (oldTechMarkedComplete ? "Completed" : "Not Completed")
		var newVal = (techComplete ? "Completed" : "Not Completed")
		queueCreateTimeStamp(woID, "Work Order Marked: " + newVal, Company_ID, clientName, projectName, "",  "", "");
		if (!techComplete)
			// Incomplete
			queueCreateTimeStamp(woID, "Work Order Marked Incomplete", Company_ID, clientName, projectName, "",  "", "");
	}
	
/*	if ($("#EditRecordAttachedFile").attr("value") != undefined) {
		// Attached File
		queueCreateTimeStamp(woID, "File Attached", Company_ID, clientName, projectName, "",  "", "");
	}*/

	if (oldApproved != $("#EditRecordApproved").attr("checked") && $("#EditRecordApproved").attr("checked")) {
		// Approved
		queueCreateTimeStamp(woID, "Work Order Approved", Company_ID, clientName, projectName, "",  "", "");
	}
		
	if (oldDeactivated != $("#EditRecordDeactivated").attr("checked") && $("#EditRecordDeactivated").attr("checked")) {
		// Deactivated
		queueCreateTimeStamp(woID, "Work Order Deactivated", Company_ID, clientName, projectName, "",  "", "");
	}
	
	if ($("#EditRecordPic1_FromTech").attr("value") != "" || $("#EditRecordPic2_FromTech").attr("value") != "" || $("#EditRecordPic3_FromTech").attr("value") != "") {
		// Deliverables Uploaded
		queueCreateTimeStamp(woID, "Deliverables Uploaded", Company_ID, clientName, projectName, "", "", "");
	}
	return true;
}

document.forms.caspioform.onsubmit = UpdateCaspio;
