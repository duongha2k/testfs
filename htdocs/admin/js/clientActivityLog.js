


//-------------------------------------------------------------
//-------------------------------------------------------------
//-------------------------------------------------------------
clientActivityLog.Instances = null;
//-------------------------------------------------------------
clientActivityLog.CreateObject = function(config) {
    var obj = null;
    if (clientActivityLog.Instances != null) {
        obj = clientActivityLog.Instances[config.id];
    }
    if (obj == null) {
        if (clientActivityLog.Instances == null) {
            clientActivityLog.Instances = new Object();
        }
        obj = new clientActivityLog(config);
        clientActivityLog.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function clientActivityLog(config) {
    var Me = this;
    this.id = config.id;
    //------------------------------------------------------------
    this.getDataList = function(){
        _global.LoadingAjax("#detailsContainer");
        _global.url = "/widgets/admindashboard/client/client-activity-log";
        _global.data = "";
        _global.onSuccess = function(data){
            jQuery("#detailsContainer").html(data);
            jQuery("#frmClientActivityLog #cmdSearch").unbind("click",Me.cmdSearch_click).bind("click",Me.cmdSearch_click);
            $('.calendarInput').calendar({dateFormat:'MDY/'});
        }
        _global.show();
    }
    //------------------------------------------------------------
    this.getDataPage = function(page,event){
        _global.url = "/widgets/admindashboard/client/client-activity-log";
        var data = jQuery("#frmClientActivityLog").serialize();
        _global.data = data+"&page="+page;
        _global.onSuccess = function(data){
            jQuery("#detailsContainer").html(data);
            jQuery("#frmClientActivityLog .paginatorLinkActiv").unbind("click").bind("click",Me.search_changepage);
            jQuery("#frmClientActivityLog .sortable").unbind("click").bind("click",Me.search_sortable);
            _global.hideLoader();
        }
        _global.show(event);
    }
    this.search_sortable = function(){
        jQuery("#sortfield").val(jQuery(this).attr("sort"));
        jQuery("#sortdir").val(jQuery(this).attr("sortdir"));
        _global.showLoaderLoading();
        Me.getDataPage(1,null);
    }
    //------------------------------------------------------------
    this.cmdSearch_click = function(){
        
        Me.getDataPage(1,jQuery(this));
    }
    //------------------------------------------------------------
    this.search_changepage = function(){
        var page = jQuery(this).attr("page");
        _global.showLoaderLoading();
        Me.getDataPage(page,null);
    }
    //------------------------------------------------------------
    this.init = function() {
        Me.getDataList();
    }
//------------------------------------------------------------

}