// GB - 2008-08-20

function ajaxFunction() {
	var runonce = "done";
	// lookup client information
	if(formType == "insert"){
		var ISO = document.forms[0].InsertRecordISO_Affiliation.value;

		$.get("ajax/ISO_Lookup.php", {"ISO" : ISO}, 
		function(data) {
			var getISOInfo = data.split(",");
			document.forms[0].InsertRecordISO_Affiliation_ID.value = getISOInfo[0];
			document.forms[0].InsertRecordCompanyName.value = document.forms[0].InsertRecordISO_Affiliation.value;
			//document.forms[0].cbParamVirtual1.value = getISOInfo[0];
			
			//var aReturn=document.forms[0].getElementsByTagName("SPAN");
			//aReturn[51].childNodes[0].nodeValue = getISOInfo[0];

			//document.forms[0].InsertRecordClient_Name.value = getISOInfo[1];
			//document.forms[0].InsertRecordClient_Email.value = getISOInfo[2];
		}
	);
	}else{
		var ISO = document.forms[0].EditRecordISO_Affiliation.value;

		$.get("ajax/ISO_Lookup.php", {"ISO" : ISO}, 
		function(data) {
			var getISOInfo = data.split(",");
			document.forms[0].EditRecordISO_Affiliation_ID.value = getISOInfo[0];
			document.forms[0].cbParamVirtual1.value = getISOInfo[0];
			
			// Update the Display (cbParamVirtual1) for visual effect only.
			// If another field gets inserted before this one, this number (51) will need to change.
			var aReturn=document.forms[0].getElementsByTagName("SPAN");
			aReturn[51].childNodes[0].nodeValue = getISOInfo[0];
			
			//document.forms[0].EditRecordClient_Name.value = getISOInfo[1];
			//document.forms[0].EditRecordClient_Email.value = getISOInfo[2];
		}
	);		
	}
}


$("#InsertRecordISO_Affiliation").change(ajaxFunction)
$("#EditRecordISO_Affiliation").change(ajaxFunction)