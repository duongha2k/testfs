


//-------------------------------------------------------------
//-------------------------------------------------------------
//-------------------------------------------------------------
photo_audit.Instances = null;
//-------------------------------------------------------------
photo_audit.CreateObject = function(config) {
    var obj = null;
    if (photo_audit.Instances != null) {
        obj = photo_audit.Instances[config.id];
    }
    if (obj == null) {
        if (photo_audit.Instances == null) {
            photo_audit.Instances = new Object();
        }
        obj = new photo_audit(config);
        photo_audit.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function photo_audit(config) {
    var Me = this;
    this.id = config.id;
    
    this.cmdSearch_click = function()
    {
        var _data = jQuery("#frmPhotoAudit").serialize();
        var page = jQuery(this).attr("page");
        if(typeof(page) == 'undefined') page = 1;
        detailObject.onInit.showStartPopup();
        $.ajax({
            type: "POST",
            url: "/widgets/admindashboard/tech/getphotoaudit?page="+page,
            data: _data,
            success: function (data) {
                jQuery("#tableSearch").css("display","none");
                jQuery("#divResult").html(data);
                jQuery("#divResult").css("display","");
                detailObject.onInit._roll.hide();
                if(jQuery('#notrd').val() == "1")
                {
                   jQuery("#cmdApprove").css("display","none");
                }
                jQuery(".paginatorLinkActiv").unbind('click', Me.cmdSearch_click)
                .bind('click', Me.cmdSearch_click);
                jQuery("#cmdApprove").unbind('click', Me.cmdApprove_click)
                .bind('click', Me.cmdApprove_click);        
                        
            }
        });
    }
    
    this.cmdApprove_click = function()
    {
        var _roll = new FSPopupRoll();
        var html = "Do you want to approve all of the photos in this date range? ";
        html += '<div style="width:50%;float:left;"><input value="Approve Batch" class="link_button middle2_button" type="button" id="cmdApproveSave" /></div>';
        html += '<div style="width:50%;float:right;"><input value="Cancel" class="link_button middle2_button" type="button" id="cmdCancel" /></div>';
        detailObject.onInit._roll.autohide(false);
        var opt = {
            width       : 350,
            height      : '',
            //position    : 'middle',
            body        : html
        };
        _roll.showNotAjax(this, this, opt);
        
        jQuery("#cmdApproveSave").unbind('click', Me.cmdApproveSave_click)
        .bind('click', Me.cmdApproveSave_click);
        jQuery("#cmdCancel").click(function(){
            _roll.hide();
        });               
        
    }
    //------------------------------------------------------------
    this.cmdApproveSave_click = function()
    {
        detailObject.onInit.showStartPopup();
        var _data = jQuery("#frmPhotoAudit").serialize();
        
        $.ajax({
            type: "POST",
            url: "/widgets/admindashboard/tech/doapproveall",
            data: _data,
            success: function (data) {
                detailObject.onInit._roll.hide();
                Me.cmdSearch_click();
                jQuery("#cmdCancel").click();
            }
        });
    }
    //------------------------------------------------------------
    this.init = function() {
        // config 
        jQuery("#txtDateUpdatedForm").datepicker({
            dateFormat: 'mm/dd/yy'
        });
        jQuery("#txtDateUpdatedTo").datepicker({
            dateFormat: 'mm/dd/yy'
        });
        jQuery('#cmdSearch').unbind('click', Me.cmdSearch_click)
        .bind('click', Me.cmdSearch_click);
    }
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
}
