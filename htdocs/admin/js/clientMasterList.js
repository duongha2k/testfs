


//-------------------------------------------------------------
//-------------------------------------------------------------
//-------------------------------------------------------------
clientMasterList.Instances = null;
//-------------------------------------------------------------
clientMasterList.CreateObject = function(config) {
    var obj = null;
    if (clientMasterList.Instances != null) {
        obj = clientMasterList.Instances[config.id];
    }
    if (obj == null) {
        if (clientMasterList.Instances == null) {
            clientMasterList.Instances = new Object();
        }
        obj = new clientMasterList(config);
        clientMasterList.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function clientMasterList(config) {
    var Me = this;
    this.id = config.id;
    this.flag = false;
    //------------------------------------------------------------
    this.getDataList = function(){
        _global.LoadingAjax("#detailsContainer");
        _global.url = "/widgets/admindashboard/client/client-master-list";
        _global.data = "";
        _global.onSuccess = function(data){
            jQuery("#detailsContainer").html(data);
            jQuery("#divfrmClientList #cmdEdit").unbind("click",Me.cmdEdit_click).bind("click",Me.cmdEdit_click);
            jQuery("#divfrmClientList .cmdCancelUpdateButton").unbind("click",Me.cmdCancelUpdateButton_click).bind("click",Me.cmdCancelUpdateButton_click);
            jQuery("#divfrmClientList #cmdSave").unbind("click",Me.cmdSave_click).bind("click",Me.cmdSave_click);
            jQuery("#frmClientList select").unbind("change",Me.frmClientListselect_change).bind("change",Me.frmClientListselect_change);
             _global.hideLoader();
        }
        _global.show();
    }
    this.eventClick = null;
    this.frmClientListselect_change = function(){
        Me.flag = true;
        jQuery("a").click(function(){
            if(Me.flag){
                Me.eventClick = this;
                Me.beforeSave = function(){
                    var href = jQuery(Me.eventClick).attr("href");
                    window.location = href;
                }
                Me.configSaveTag();
                return false;
            }
        })
    }
    //------------------------------------------------------------
    this.cmdEdit_click = function(){
        jQuery("#honderButtonSave").show();
        jQuery(this).hide();
        // edit
        jQuery("#frmClientList .inputSelectSave").parent().children('span').hide();
        jQuery("#frmClientList .inputSelectSave").show();
        //end edit
    }
    this.beforeSave = function(){
        
    } 
    this.configSaveTag = function(){
       var html = '<div style=""><div>Please confirm that you want to leave this page. Any changes you have made will not be saved.</div><div style="text-align:center; margin-top: 5px"><input type="button" value="Leave Page" class="link_button middle2_button cmdLeaveTag">&nbsp;<input type="button" value="Save Changes" class="link_button middle2_button cmdSaveTag"></div></div>';
       _global.showLoader(320,70,true);
       jQuery("#comtantPopupID").html(html);
       jQuery(".cmdLeaveTag").click(function(){
           Me.beforeSave();
           _global.hideLoader();
       });
       jQuery(".cmdSaveTag").click(function(){
           jQuery("#cmdSave").click();
           _global.hideLoader();
       });
    }
    //------------------------------------------------------------
    this.cmdCancelUpdateButton_click = function(){
        Me.beforeSave = function(){
            Me.flag = false;
            _global.hideLoader();
        jQuery("#honderButtonSave").hide();
        jQuery("#divfrmClientList #cmdEdit").show();

        jQuery("#frmClientList .inputSelectSave").parent().children('span').show();
        jQuery("#frmClientList .inputSelectSave").hide();
        }
        if(Me.flag){
            Me.configSaveTag();
        }else{
            Me.beforeSave();
        }
        
    }
    //------------------------------------------------------------
    this.cmdSave_click = function(){
        _global.url = "/widgets/admindashboard/client/save-client-master";
        _global.data = jQuery("#frmClientList").serialize();
        _global.onSuccess = function(data){
            _global.showMessages(data);
            Me.getDataList();
            Me.beforeSave();
            _global.hideLoader();
            Me.flag = false;
        }
        _global.showLoaderSaving();
        _global.show();
    }
    //------------------------------------------------------------
    //------------------------------------------------------------
    this.init = function() {
        Me.getDataList();
    }
//------------------------------------------------------------

}