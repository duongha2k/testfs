


//-------------------------------------------------------------
//-------------------------------------------------------------
//-------------------------------------------------------------
admin_client_profile.Instances = null;
//-------------------------------------------------------------
admin_client_profile.CreateObject = function(config) {
    var obj = null;
    if (admin_client_profile.Instances != null) {
        obj = admin_client_profile.Instances[config.id];
    }
    if (obj == null) {
        if (admin_client_profile.Instances == null) {
            admin_client_profile.Instances = new Object();
        }
        obj = new admin_client_profile(config);
        admin_client_profile.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function admin_client_profile(config) {
    var Me = this;
    this.id = config.id;
    this.client_id = config.client_id;
    this._redirect = false;
    this._redirectTo = "";
    this._rollMsg = new FSPopupRoll();
    this._roll = new FSPopupRoll();
    this._rollInstruction = new FSPopupRoll();
    this._rollWaitingSave = new FSPopupRoll();
    this._rollActivityLog = new FSPopupRoll();
    this.companyName = "";
    this.companyId = config.companyId;
    this.emailclient = "";
    this.tab = config.tabid;
    //------------------------------------------------------------
    this.LoadingAjax = function(ContentId){
        jQuery(ContentId).html("<center><img src='/widgets/images/wait.gif' border='0' /></center>");
    }
    //------------------------For Client profile-----------------------    
    /**
     * For Client profile
     */
    //------------------------------------------------------------------------
    //------------------------------------------------------------
    this.FSAccountManager_onchange = function()
    {
        var phone = jQuery(this).children('option:selected').attr("phone");
        var email = jQuery(this).children('option:selected').attr("email");
        jQuery("#detailsContainer #txtFSAccountManagerEmail").val((typeof(email) == "undefined")?'':email);
        jQuery("#detailsContainer #txtFSAccountManagerPhone").val((typeof(phone) == "undefined")?'':phone);
    }
    //------------------------------------------------------------
    this.FSClientServiceDirector_onchange = function()
    {
        var phone = jQuery(this).children('option:selected').attr("phone");
        var email = jQuery(this).children('option:selected').attr("email");
        jQuery("#detailsContainer #txtFSClientServiceDirectorName").val((typeof(email) == "undefined")?'':email);
        jQuery("#detailsContainer #txtFSClientServiceDirectorPhone").val((typeof(phone) == "undefined")?'':phone);
    }
    this.EditRecordCountry_onchange = function()
    {
        var contrycode = jQuery(this).children('option:selected').attr('contrycode');
        $.ajax({
            type: "POST",
            url: "/widgets/admindashboard/client/getstatis?contry_id="+contrycode,
            data: "",
            success:function( html ) {
                jQuery("#EditRecordBusinessState").html(html);
            }
        });
    }
    //------------------------------------------------------------
    
    //------------------------------------------------------------
    this.checkValidProfile = function()
    {
        //check valid
        
        var EIN = jQuery.trim(jQuery("#detailsContainer #EditRecordEIN").val());
        var data = {
            success:0,
            errors:['EIN or Tax ID must be XX-XXXXXXX.']
        };
        Me._redirect = false;    
        if(jQuery.trim(EIN) != '' && jQuery("#EditRecordCountry").val() == "US")
        {
            if(EIN.length != 10)
            {
                Me.showMessages(data);
                return false;
            }
            if(EIN[2] != '-')
            {
                Me.showMessages(data);
                return false;     
            }
        }
        //        if(EIN.substring(5, EIN.length) != '22333')
        //        {
        //            Me.showMessages(data);
        //            return false;     
        //        }
        
        
        return true;
    }
    //------------------------------------------------------------
    this.getdataProfile = function(_mode)
    {
        var ContentId = "#detailsContainer";
        Me.LoadingAjax(ContentId);
        $.ajax({
            type: "POST",
            url: "/widgets/admindashboard/client/updateclient?client_id="+Me.client_id+'&mode='+_mode,
            data: "",
            success:function( html ) {
                jQuery(ContentId).html(html);
                _mode = 'edit';
                if(_mode == 'edit')
                {   
                    //970
                    //build event Save 
                    jQuery("#detailsContainer .cmdSave").unbind("click",Me.saveUpdateProfile)
                    .bind("click",Me.saveUpdateProfile);
                    jQuery("#detailsContainer #FSAccountManager").unbind("change",Me.FSAccountManager_onchange)
                    .bind("change",Me.FSAccountManager_onchange);
                    jQuery("#detailsContainer #FSClientServiceDirector").unbind("change",Me.FSClientServiceDirector_onchange)
                    .bind("change",Me.FSClientServiceDirector_onchange);
                
                    jQuery("#detailsContainer #EditRecordCountry").unbind("change",Me.EditRecordCountry_onchange)
                    .bind("change",Me.EditRecordCountry_onchange);
                    jQuery("#detailsContainer .cbxrule").unbind("change")
                        .bind("change",Me.cbxrule_Onchange);
                    jQuery("#detailsContainer #ClientdeninedNo").unbind("click")
                            .bind("click",Me.isDisplayClientdenined_Onclick);
                    jQuery("#detailsContainer #Clientdeninedyes").unbind("click")
                            .bind("click",Me.isDisplayClientdenined_Onclick);
                    
                    // bind data
                    var phone = jQuery("#detailsContainer #FSAccountManager").children('option:selected').attr("phone");
                    var email = jQuery("#detailsContainer #FSAccountManager").children('option:selected').attr("email");
                    jQuery("#detailsContainer #txtFSAccountManagerEmail").val((typeof(email) == "undefined")?'':email);
                    jQuery("#detailsContainer #txtFSAccountManagerPhone").val((typeof(phone) == "undefined")?'':phone);
                
                    jQuery("#detailsContainer .cmdCancelUpdateButton").click(function() {
                        Me.getdataProfile('view');
                    });
                    jQuery("#txtDateAgreement").datepicker({
                        dateFormat:'mm/dd/yy'
                    });
                    jQuery("#txtDateInformation").datepicker({
                        dateFormat:'mm/dd/yy'
                    });
                    jQuery("#txtDateService").datepicker({
                        dateFormat:'mm/dd/yy'
                    });
                    
                    jQuery("#detailsContainer #cmdUpdateEntity").unbind("click",Me.cmdUpdateEntity)
                    .bind("click",Me.cmdUpdateEntity);
                    jQuery("#detailsContainer #cmdUpdateServiceType").unbind("click",Me.cmdUpdateServiceType)
                    .bind("click",Me.cmdUpdateServiceType);
                    jQuery("#detailsContainer #cmdUpdateCSD").unbind("click",Me.cmdUpdateCSD)
                    .bind("click",Me.cmdUpdateCSD);
                }
                else
                {
                    jQuery("#detailsContainer #cmdEdit").click(function() {
                        Me.getdataProfile('edit'); 
                    }); 
                }
            }
        });
    }
    //970
    this.cbxrule_Onchange = function()
    {
        var parentclass = $(this).parent().parent().attr("class");
        if($("#cbx"+ parentclass +" option:selected").val()=="")
        {
            $("."+parentclass +" .timeframe").css("display","none");
        }
        else
        {
            $("."+parentclass +" .timeframe").css("display","");
        }
        
    }
    
    this.isDisplayClientdenined_Onclick = function()
    {
        if($("#ClientdeninedNo").is(":checked"))
        {
            $("#clientdeninedyes").html("Yes");
            $(".tdClientdeninedyes").css("display","none");
            $("#cbxnoshows option").filter(function() {
                return $(this).val() == ""; 
            }).attr('selected', true)
            $("#cbxbackouts option").filter(function() {
                return $(this).val() == ""; 
            }).attr('selected', true)
            $("#cbxpreferencerating option").filter(function() {
                return $(this).val() == ""; 
            }).attr('selected', true)
            $("#cbxperformancerating option").filter(function() {
                return $(this).val() == ""; 
            }).attr('selected', true)
            $(".timeframe").css("display","none");
        }
        else
        {
            $(".tdClientdeninedyes").css("display","");
            $("#clientdeninedyes").html("Yes &ndash; Automatically client-deny the techs with any of the following, across all FieldSolutions clients:");
             
        }
    }

    this.updateSignOffDisplay = function () {
	var signoff_enabled = jQuery("#edit_SignOff_Disabled").attr ("checked");
	var custom_enabled = (jQuery("input[name=CustomSignOff_Enabled]:checked").val() == "1"
		&& $("#PicCustomSignOff").val () != "");

	if (signoff_enabled && custom_enabled) {
		jQuery("input[name=PicCustomSignOff]").show ();
	}
	else {
		jQuery("input[name=PicCustomSignOff]").hide ();
	}

	if (signoff_enabled) {
		jQuery(".signoffoptions_display").show ();
		jQuery("input[name=CustomSignOff_Enabled]").removeAttr ("disabled");
	}
	else {
		jQuery("input[name=CustomSignOff_Enabled]").attr ("disabled","disabled");
		jQuery(".signoffoptions_display").hide ();
	}
};
    //end 970
    //------------------------------------------------------------
    this.cmdUpdateEntity = function(){
        _global.data = "EntityTypeId="+jQuery("#EntityTypeId").val()
            +"&client_id="+Me.client_id;
        _global.url = "/widgets/admindashboard/client/client-profile-update-entity";
        var _onSuccess = function(data){
            jQuery("#comtantPopupID").html(data);
            // build event
            jQuery("#cmdCancelPopup").click(function(){
                _global.hideLoader();
            });
            jQuery("#frmEntityType #cmdUpdateEntityType")
                .unbind("click",Me.cmdUpdateEntityType_click)
                .bind("click",Me.cmdUpdateEntityType_click);
        }
        _global.onSuccess = _onSuccess;
        _global.showLoader(700,350,true);
        _global.show();
    }
    this.cmdUpdateEntityType_click = function(){
        var dataForm = jQuery("#frmEntityType").serialize();
        _global.data = dataForm+"&pro=update";
        _global.url = "/widgets/admindashboard/client/client-profile-update-entity";
        var _onSuccess = function(data){
            var ServiceTypeId = jQuery("#popupEntityTypeId option:selected").val();
            var ServiceTypeText = jQuery("#popupEntityTypeId option:selected").html();
            jQuery("#EntityTypeId").val(ServiceTypeId);
            if(ServiceTypeId!="")
            {
                jQuery("#contentEntity").html(ServiceTypeText);
            }
            else
            {
                jQuery("#contentEntity").html("");
            }

             _global.hideLoader();
        }
        _global.onSuccess = _onSuccess;
        _global.show(this);
    }
    //------------------------------------------------------------
    this.cmdUpdateServiceType = function(){
        _global.data = "ServiceTypeId="+jQuery("#ServiceTypeId").val()
            +"&client_id="+Me.client_id;
        _global.url = "/widgets/admindashboard/client/client-profile-update-serviceyype";
        var _onSuccess = function(data){
            jQuery("#comtantPopupID").html(data);
            // build event
            jQuery("#cmdCancelPopup").click(function(){
                _global.hideLoader();
            });
            jQuery("#frmServiceType #cmdUpdateServiceType")
                .unbind("click",Me.cmdUpdateServiceType_click)
                .bind("click",Me.cmdUpdateServiceType_click);
        }
        _global.onSuccess = _onSuccess;
        _global.showLoader(700,350,true);
        _global.show();
    }
    this.cmdUpdateServiceType_click = function(){
        var dataForm = jQuery("#frmServiceType").serialize();
        _global.data = dataForm+"&pro=update";
        _global.url = "/widgets/admindashboard/client/client-profile-update-serviceyype";
        var _onSuccess = function(data){
             var ServiceTypeId = jQuery("#popupServiceTypeId option:selected").val();
             var ServiceTypeText = jQuery("#popupServiceTypeId option:selected").html();
             jQuery("#ServiceTypeId").val(ServiceTypeId);
             if(ServiceTypeId!="")
            {
             jQuery("#contentServiceType").html(ServiceTypeText);
            }
            else
            {
                jQuery("#contentServiceType").html("");
            }

             _global.hideLoader();
        }
        _global.onSuccess = _onSuccess;
        _global.show(this);
    }
    //------------------------------------------------------------
    this.cmdUpdateCSD = function(){
        _global.data = "CSDId="+jQuery("#CSDId").val()
        +"&client_id="+Me.client_id;
        _global.url = "/widgets/admindashboard/client/client-profile-update-csd";
        var _onSuccess = function(data){
            jQuery("#comtantPopupID").html(data);
            // build event
            jQuery("#cmdCancelPopup").click(function(){
                _global.hideLoader();
            });
            jQuery("#frmCSD #cmdUpdateCSD")
            .unbind("click",Me.cmdUpdateCSD_click)
            .bind("click",Me.cmdUpdateCSD_click);
        }
        _global.onSuccess = _onSuccess;
        _global.showLoader(700,350,true);
        _global.show();
    }
    this.cmdUpdateCSD_click = function(){
        var dataForm = jQuery("#frmCSD").serialize();
        _global.data = dataForm+"&pro=update";
        _global.url = "/widgets/admindashboard/client/client-profile-update-csd";
        var _onSuccess = function(data){
            var CSDId = jQuery("#popupCSDId option:selected").val();
            var CSDText = jQuery("#popupCSDId option:selected").html();
            var CSDmail = jQuery("#popupCSDId option:selected").attr('email');
            var CSDphone = jQuery("#popupCSDId option:selected").attr('phone');
            
            jQuery("#CSDId").val(CSDId);
            if(CSDId!="")
            {
                jQuery("#contentCSD").html(CSDText);
                jQuery("#txtFSClientServiceDirectorName").val(CSDmail);
                jQuery("#txtFSClientServiceDirectorPhone").val(CSDphone);
            }
            else
            {
                jQuery("#contentCSD").html("");
            }

            _global.hideLoader();
        }
        _global.onSuccess = _onSuccess;
        _global.show(this);
    }

    //------------------------------------------------------------
    this.saveUpdateProfile = function()
    {
        if(Me.checkValidProfile())
        {
            Me.WaitingSave();
            Me.uploadFile();    
        }
    }
    //-----------------------------------------------------------
    this.WaitingSave = function()
    {
        var html ='<img border="0" src="/widgets/images/wait.gif">';
        Me._roll.autohide(false);

        var opt = {};
        opt.width    = 200;
        opt.height   = '';
        opt.position = 'middle';
        opt.title    = '';
        opt.body     = html;
        
        return Me._rollWaitingSave.showNotAjax(null, null, opt);
    }
    //-----------------------------------------------------------
    
    this.showMessages = function(data,fnSuccess, successMsg, autohide) {
    
        var html = '';
        Me._rollWaitingSave.hide();
        if (Me._redirect && data.success) { //always refresh page on success
            if (window.opener) {
                try{
                    window.opener.reloadTabFrame();
                } catch (e) {}
            }
            //window.location = Me._redirectTo;
            if(typeof(fnSuccess) == "function")
            {
                fnSuccess();
            }
        //Me.getdataProfile();
        }
        if (data.success) {
            if (!successMsg)
                html += '<div class="success"><ul><li>Success!</li></ul></div>';
            else
                html = msg;
        }else{
            html+='<div class="errors"><ul>';
            for (i=0;i<data.errors.length;i++) {
                if(typeof data.errors[i] === 'object')
                    html+='<li>'+data.errors[i].message+'</li>';
                else
                    html+='<li>'+data.errors[i]+'</li>';
            }
            html+='</ul></div>';
        }
        if (!successMsg)
            html+='<div class="tCenter"><input type="button" class="link_button_popup" onclick="_admin_client_profile._rollMsg.hide();" value="Ok" /></div>';
        if (autohide == undefined) autohide = false;
        Me._rollMsg.autohide(autohide);

        var opt = {};
        opt.width    = 400;
        opt.height   = '';
        opt.position = 'middle';
        opt.title    = '';
        opt.body     = html;
     
        if (data.success) {
            opt.width = 300;
        }
        return Me._rollMsg.showNotAjax(null, null, opt);
    }
    //------------------------------------------------------------
    this.saveToProfile = function()
    {
        var data = "";
        jQuery("#detailsContainer .KeyNamePost").each(function() {
            var type = jQuery(this).attr("type");
            switch (type)
            {
                case 'checkbox':
                    var name = jQuery(this).attr("name");
                    var checked = jQuery(this).is(':checked');     
                    if(name.toLowerCase() == "company_logo_rm" && checked)
                    {
                        data += "&"+jQuery(this).attr("name")+"="+1;    
                    }
                    else
                    {
                        data += "&"+jQuery(this).attr("name")+"="+checked;        
                    }     
                    break;
                default:
                    data += "&"+jQuery(this).attr("name")+"="+jQuery(this).val(); 
                    break;
                    
            }
            
        });
        if(data != "")
        {
            data = data.substring(1, data.length);     
            
            $.ajax({
                type: "POST",
                url: "/widgets/admindashboard/client/do-updateclient?client_id="+Me.client_id,
                data: data,
                success:function( html ) {
                    if(html != "")
                    {
                        var data = eval("("+html+")");
                        if(data.success)
                        {
                            Me._redirect = true;
                            Me.showMessages(data,Me.getdataProfile); 
                        }
                        else
                        {
                            Me.showMessages(data);    
                        }  
                    }  
                }
            });
        }
        
    }
    //------------------------------------------------------------
    this.StepUploadFile = function(next,total)
    {
        if(next > total)
        {
            Me.saveToProfile();
            return true;     
        }
        switch(next)
        {
            case 1:
                if(jQuery("#detailsContainer #chkAgreement").is(':checked'))
                {
                    next++;
                    Me.StepUploadFile(next,total)
                }
                else
                {
                    Me.uploadFileTemp('fileUploadAgreement','hidFileUploadAgreement',next,total); 
                }
                
                break;
            case 2:
                if(jQuery("#detailsContainer #chkInformation").is(':checked'))
                {
                    next++;
                    Me.StepUploadFile(next,total)
                }
                else
                {
                    Me.uploadFileTemp('fileUploadInformation','hidfileUploadInformation',next,total); 
                }
                break;
            case 3:
                if(jQuery("#detailsContainer #chkService").is(':checked'))
                {
                    next++;
                    Me.StepUploadFile(next,total)
                }
                else
                {
                    Me.uploadFileTemp('fileUploadService','hidfileUploadService',next,total);  
                }
                break;
        }
    }
    //------------------------------------------------------------
    this.uploadFileTemp = function(file_id,hidden_id,next,total)
    {
        var id_content = file_id;
        var uploadUrl = '/widgets/admindashboard/client/ajax-upload-file-temp';
        var descr = "";
        if(jQuery("#"+hidden_id).attr("updated") != "true")
        {
            $.ajaxFileUpload({
                url           : uploadUrl + "/file/"+id_content+"/company//clt_act/update/descr/"+descr+'?client_id='+Me.client_id,
                secureuri     : false,
                fileElementId : id_content,
                dataType      : 'json',
                context       : this,
                cache         : false,
                success       : function (data, status, xhr) {
                    //Me.saveToProfile();
                    if(data.success == 1 || data.success == -1)
                    {
                        jQuery("#"+hidden_id).val(data.file); 
                        jQuery("#"+hidden_id).attr("updated",'true');   
                        next++;
                        Me.StepUploadFile(next,total);
                    }
                    else if(data.success == 0)
                    {
                        Me.showMessages(data);     
                    }
                
                },
                error:function(data, status, xhr){
                    next++;
                    Me.StepUploadFile(next,total);
                }
            });
        }
        else
        {
            next++;
            Me.StepUploadFile(next,total);  
        }
    }
    //------------------------------------------------------------
    this.uploadFile = function()
    {
        var descr = '';
        var id_content = "Company_Logo";
        var uploadUrl = '/widgets/admindashboard/client/ajax-upload-file';
	
        $.ajaxFileUpload({
            url           : uploadUrl + "/file/"+id_content+"/company//clt_act/update/descr/"+descr+'?client_id='+Me.client_id,
            secureuri     : false,
            fileElementId : id_content,
            dataType      : 'json',
            context       : this,
            cache         : false,
            success       : function (data, status, xhr) {
                //Me.saveToProfile();
                if(data.success == 1 || data.success == -1)
                {
                    Me.StepUploadFile(1,3);
                }
                else if(data.success == 0)
                {
                    Me.showMessages(data);     
                }
                
            },
            error:function(data, status, xhr){
                Me.StepUploadFile(1,3);
            }
        });
    }
    //------------------------For Access Setting-----------------------    
    /**
         * For Access Setting
         */
    //------------------------------------------------------------------------
    this.UpdateAccessSettings = function()
    {
        Me.WaitingSave();
        var json = '';
        var enableAuto21DayRestricTool;
        if($("#EnableAuto21DayRestricTool:checked").length > 0)
        {
            enableAuto21DayRestricTool = 1;
        }
        else
        {
            enableAuto21DayRestricTool = 0;
        }
        jQuery(Me.dicContentAccessID+" .trChildren").each(function(){
            var val = 1;
            var item_id = jQuery(this).attr('item_id');
            if(item_id > 0)
            {
                if(jQuery(this).find('.ckChildrenOpen').attr("checked") == "true")
                {
                    val = jQuery(this).find('.ckChildrenOpen').attr("value_id");     
                }
                else if(jQuery(this).find('.ckChildrenOpen').attr("checked") == "square")
                {
                    val = jQuery(this).find('.ckChildrenOpen').attr("value_id");     
                }
                else if(jQuery(this).find('.ckChildrenRestricted').attr("checked") == "true")
                {
                    val = jQuery(this).find('.ckChildrenRestricted').attr("value_id");     
                }
                json += ',{"id":"'+item_id+'","val":"'+val+'"}';
            }
        });
        if(json != "")
        {
            json = Base64.encode("["+json.substring(1, json.length)+"]");     
        }
        $.ajax({
            type: "POST",
            url: "/widgets/admindashboard/client/update-accesssettingprofile",
            data: "json="+json+"&client_id="+Me.client_id+"&EnableAuto21DayRestricTool="+enableAuto21DayRestricTool,
            success:function( html ) {
                var data = eval("("+html+")");
                if(data.success)
                {
                    Me._redirect = true;
                    Me.showMessages(data,Me.getdataAccess('view')); 
                }
                else
                {
                    Me.showMessages(data);    
                }  
            }
        });
    }
    //---------------------------------------------------------------------
    this.cmdEditAccessSettings = function()
    {
        Me.getdataAccess('edit');
    }
    //--------------------------------------------------------------------
    this.cmdCancelUpdateButtonAccessSettings = function()
    {
        Me.getdataAccess('view');
    }
    //---------------------------------------------------------------------
    this.onInitCheck = function()
    {
        jQuery(Me.dicContentAccessID+" .trChildren").each(function(){
            var item_id = jQuery(this).attr("item_id");
            var el = this; 
            if(item_id == 0 || jQuery(this).find(".ckChildrenOpen").attr("checked") == "true")
            {
                var i =0;
                var countI = 0;
                jQuery(Me.dicContentAccessID+" .trChildrenId"+item_id).each(function(){
                    countI++;
                    if(jQuery(this).find(".ckChildrenOpen").attr("checked") == 'true')
                    {
                        i++;
                    }
                });
                
                if(i > 0 && i < countI)
                {
                    Me.checkSpuareOpen(el,'square'); 
                    Me.checkSpuareRe(el,'square'); 
                }
                else if(i == countI)
                {
                    Me.checkSpuareOpen(el,'checked');       
                }
                else if(i == 0)
                {
                    Me.checkSpuareRe(el,'checked');     
                }
              
            }
        
        });
    }
    //---------------------------------------------------------------------
    this.pageAccessLog = 1;
    this.ajaxLinkAccessLog_click = function()
    {
        Me._rollActivityLog.hide();
        Me.pageAccessLog = jQuery(this).attr('page');
        Me.cmdActivityLogAccessSettings(null);
    }
    //---------------------------------------------------------------------
    this.cmdActivityLogAccessSettings = function(event)
    {
        Me.WaitingSave();
        var el = this;
        if(event != null){
            Me.pageAccessLog = 1;
        }
        $.ajax({
            type: "POST",
            url: "/widgets/admindashboard/client/activitylogprofile?client_id="+Me.client_id+"&page="+Me.pageAccessLog,
            data: "",
            success:function( html ) {
                var autohide = false;
                Me._rollWaitingSave.hide();
                Me._rollActivityLog.autohide(autohide);
                var opt = {};
                opt.width    = 600;
                opt.height   = 400;
                opt.position = 'top';
                opt.wTop = 10;
                opt.title    = '<div style="text-align: center;">Profile Activity Log<div>';
                opt.body     = html;
                Me._rollActivityLog.showNotAjax(el, el, opt);
                // binh event
                jQuery("#AcivitiLogUserTN .ajaxLink").unbind("click",Me.ajaxLinkAccessLog_click)
                .bind("click",Me.ajaxLinkAccessLog_click);
            }
        });
    }
    //---------------------------------------------------------------------
    this.urlCheckFalse = '/widgets/images/checklist_flase.jpg';
    this.urlCheckTrue = '/widgets/images/checklist_true.jpg';
    this.urlCheckBlock = '/widgets/images/checklist_block.jpg';
    
    this.urlCheckTrueSquare = '/widgets/images/checklist_true_square.jpg';
    this.urlCheckBlockSquare = '/widgets/images/checklist_block_suare.jpg';
    
    this.urlImg_minus= '/widgets/images/img_minus.jpg';
    this.urlImg_plus= '/widgets/images/img_plus.jpg';
    this.dicContentAccessID = "#clientAccessSettings";
    //--------------------------------------------------------------------
    this.checkOpen = function(el,checked,item_id)
    {
        if(checked  == "true")
        {
            jQuery(el).attr("src",Me.urlCheckFalse);
            jQuery(el).attr("checked","false");
            jQuery(Me.dicContentAccessID
                +" .ckChildrenRestrictedId"+item_id).attr("checked","true");
            jQuery(Me.dicContentAccessID
                +" .ckChildrenRestrictedId"+item_id).attr("src",Me.urlCheckBlock);
        }
        else
        {
            jQuery(el).attr("src",Me.urlCheckTrue);  
            jQuery(el).attr("checked","true");
            jQuery(Me.dicContentAccessID
                +" .ckChildrenRestrictedId"+item_id).attr("checked","false");
            jQuery(Me.dicContentAccessID
                +" .ckChildrenRestrictedId"+item_id).attr("src",Me.urlCheckFalse);
        }
        
    }
    //--------------------------------------------------------------------
    this.checkRestricted = function(el,checked,item_id)
    {
        if(checked == "true")
        {
            jQuery(el).attr("src",Me.urlCheckFalse);
            jQuery(el).attr("checked","false");
            jQuery(Me.dicContentAccessID
                +" .ckChildrenOpenId"+item_id).attr("checked","true");
            jQuery(Me.dicContentAccessID
                +" .ckChildrenOpenId"+item_id).attr("src",Me.urlCheckTrue);
        }
        else
        {
            jQuery(el).attr("src",Me.urlCheckBlock);  
            jQuery(el).attr("checked","true");
            jQuery(Me.dicContentAccessID
                +" .ckChildrenOpenId"+item_id).attr("checked","false");
            jQuery(Me.dicContentAccessID
                +" .ckChildrenOpenId"+item_id).attr("src",Me.urlCheckFalse);
        }
        
    }
    //---------------------------------------------------------------------
    this.checkSpuareOpen = function(el,action)
    {
        var elOpen = jQuery(el).find('.ckChildrenOpen');
        var elRe = jQuery(el).find('.ckChildrenRestricted');
        if(action == 'none')
        {
            jQuery(elOpen).attr("src",Me.urlCheckFalse);
            jQuery(elOpen).attr("checked","false");
        }
        else if(action == 'square')
        {
            jQuery(elOpen).attr("src",Me.urlCheckTrueSquare);  
            jQuery(elOpen).attr("checked","square");
        }
        else if(action == 'checked')
        {
            jQuery(elOpen).attr("src",Me.urlCheckTrue);  
            jQuery(elOpen).attr("checked","true");
            jQuery(elRe).attr("src",Me.urlCheckFalse);  
            jQuery(elRe).attr("checked","false");
        }
    }
    //---------------------------------------------------------------------
    this.checkSpuareRe = function(el,action)
    {
        //var elOpen = jQuery(el).find('.ckChildrenOpen');
        var elRe = jQuery(el).find('.ckChildrenRestricted');
        if(action == 'none')
        {
            jQuery(elRe).attr("src",Me.urlCheckFalse);
            jQuery(elRe).attr("checked","false");
        }
        else if(action == 'square')
        {
            jQuery(elRe).attr("src",Me.urlCheckBlockSquare);  
            jQuery(elRe).attr("checked","square");
        }
        else if(action == 'checked')
        {
            jQuery(elRe).attr("src",Me.urlCheckBlock);  
            jQuery(elRe).attr("checked","true");
        }
    }
    //---------------------------------------------------------------------
    this.configCheckbox = function(_id)
    {
        var checked = jQuery(Me.dicContentAccessID+' .trCurrentId'+_id+" .ckChildrenOpen").attr("checked");
        var item_id = jQuery(Me.dicContentAccessID+' .trCurrentId'+_id).attr('item_id');
        var parent_id = jQuery(Me.dicContentAccessID+' .trCurrentId'+_id).attr('parent_id');
        var Reparent_id = parent_id;
        jQuery(Me.dicContentAccessID+' .trChildrenId'+item_id+" .ckChildrenOpen").each(function() {
            var _itme_id = jQuery(this).attr("item_id"); 
            Me.checkOpen(this,checked == "true"?"false":"true",_itme_id);
        });
        // config Open
        while (parent_id >= 0 && typeof(parent_id) != "undefined")
        {
            var i = 0;
            var countI = 0;
            jQuery(Me.dicContentAccessID+' .trChildrenId'+parent_id).each(function() { 
                countI++;
                if(jQuery(this).find(".ckChildrenOpen").attr("checked") == 'square')
                {
                    i = -1;
                    return false;
                }
                if(jQuery(this).find(".ckChildrenOpen").attr("checked") == 'true')
                {
                    i++;
                }
            });
            var el = jQuery(Me.dicContentAccessID+' .trCurrentId'+parent_id); 
            if(i == 0)
            { 
                Me.checkSpuareOpen(el,'none');
            }
            else if(countI == i)
            {
                Me.checkSpuareOpen(el,'checked');    
            }
            else
            {
                Me.checkSpuareOpen(el,'square');   
            }
            
            parent_id = jQuery(Me.dicContentAccessID+' .trCurrentId'+parent_id).attr("parent_id");
        }
        //config Re
        parent_id = Reparent_id;
        while (parent_id >= 0 && typeof(parent_id) != "undefined")
        {
            var i = 0;
            var countI = 0;
            jQuery(Me.dicContentAccessID+' .trChildrenId'+parent_id).each(function() { 
                countI++;
                if(jQuery(this).find(".ckChildrenRestricted").attr("checked") == 'square')
                {
                    i = -1;
                    return false;
                }
                if(jQuery(this).find(".ckChildrenRestricted").attr("checked") == 'true')
                {
                    i++;
                }
            });
            var el = jQuery(Me.dicContentAccessID+' .trCurrentId'+parent_id); 
            if(i == 0)
            { 
                Me.checkSpuareRe(el,'none');
            }
            else if(countI == i)
            {
                Me.checkSpuareRe(el,'checked');    
            }
            else
            {
                Me.checkSpuareRe(el,'square');   
            }
            
            parent_id = jQuery(Me.dicContentAccessID+' .trCurrentId'+parent_id).attr("parent_id");
        }
    }
    //---------------------------------------------------------------------
    this.ckChildrenOpen_onclick = function()
    {
        var checked = jQuery(this).attr("checked");
        var item_id = jQuery(this).attr("item_id");
        Me.checkOpen(this,checked,item_id);
        Me.configCheckbox(item_id);
    }
    //---------------------------------------------------------------------
    this.ckChildrenRestricted_onclick = function()
    {
        var checked = jQuery(this).attr("checked");
        var item_id = jQuery(this).attr("item_id");
        Me.checkRestricted(this,checked,item_id);
        Me.configCheckbox(item_id);
    }
    //---------------------------------------------------------------------
    this.cmdCollspan_onclick = function()
    {
        var item_id = jQuery(this).attr("item_id");
        var display = jQuery(Me.dicContentAccessID + " .trChildrenId"+item_id).css('display');
        var img = '';
        var childrenDisplay = '';
        if(display == 'none')
        {
            img = Me.urlImg_minus;
            childrenDisplay = '';
        }
        else
        {
            img = Me.urlImg_plus;
            childrenDisplay = 'none';    
        }
        jQuery(this).children('img').attr('src',img);
        jQuery(Me.dicContentAccessID)
        .find(".trChildrenId"+item_id).css('display',childrenDisplay);
    }
    //---------------------------------------------------------------------
    //---------------------------------------------------------------------
    this.getdataAccess = function(mode)
    {
        Me.LoadingAjax(Me.dicContentAccessID);
        $.ajax({
            type: "POST",
            url: "/widgets/admindashboard/client/accesssettingprofile?client_id="+Me.client_id+'&mode='+mode,
            data: "",
            success:function( html ) {
                jQuery(Me.dicContentAccessID).html(html);
                //build event Save 
                if(mode == 'view')
                {
                    jQuery("#clientAccessSettings #cmdEdit").unbind("click",Me.cmdEditAccessSettings)
                    .bind("click",Me.cmdEditAccessSettings);      
                }
                else
                {
                    jQuery(Me.dicContentAccessID +" #cmdSave").unbind("click",Me.UpdateAccessSettings)
                    .bind("click",Me.UpdateAccessSettings);  
                    jQuery(Me.dicContentAccessID +" .cmdCancelUpdateButton").unbind("click",Me.cmdCancelUpdateButtonAccessSettings)
                    .bind("click",Me.cmdCancelUpdateButtonAccessSettings);
                    
                    jQuery(Me.dicContentAccessID +" .ckChildrenOpen").unbind("click",Me.ckChildrenOpen_onclick)
                    .bind("click",Me.ckChildrenOpen_onclick);
                    jQuery(Me.dicContentAccessID +" .ckChildrenRestricted").unbind("click",Me.ckChildrenRestricted_onclick)
                    .bind("click",Me.ckChildrenRestricted_onclick);
                }
                jQuery(Me.dicContentAccessID +" #cmdActivityLog").unbind("click",Me.cmdActivityLogAccessSettings)
                .bind("click",Me.cmdActivityLogAccessSettings);
                jQuery(Me.dicContentAccessID +" .cmdCollspan").unbind("click",Me.cmdCollspan_onclick)
                .bind("click",Me.cmdCollspan_onclick);
                Me.onInitCheck();
            }
        });
    }
    
    //--------------------- End Access Settings----------------------------
    this.getdataActivityLog = function()
    {
            
    }
   
    /**
     * Quick Ticket
     */
    //-----------------------------------------------------------
    this.clientQuickTickets = "#clientQuickTickets";
    this.getdataQuickTickets = function()
    {
        Me.LoadingAjax(Me.clientQuickTickets);
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/quick-ticket/admin-list",
            data: "client_id="+Me.client_id,
            success:function( html ) {
                jQuery(Me.clientQuickTickets).html(html);
                //build event Save 
                jQuery(Me.clientQuickTickets +" #cmdAddEditQuickTicket").unbind("click",Me.cmdAddEditQuickTickets)
                .bind("click",Me.cmdAddEditQuickTickets);
                jQuery(Me.clientQuickTickets +" .cmdAddEditQuickTicket").unbind("click",Me.cmdAddEditQuickTickets)
                .bind("click",Me.cmdAddEditQuickTickets);
                jQuery(".cmdEditProjectQT").unbind("click",Me.cmdEditProjectQT)
                .bind("click",Me.cmdEditProjectQT);
                jQuery(".cmdAddUserQT").unbind("click",Me.cmdAddUserQT)
                .bind("click",Me.cmdAddUserQT);
                jQuery(".cmdEditUserQT").unbind("click",Me.cmdEditUserQT)
                .bind("click",Me.cmdEditUserQT);
                jQuery(".cmdRemoveUserQT").unbind("click",Me.cmdRemoveUserQT)
                .bind("click",Me.cmdRemoveUserQT);
                jQuery("#cmdWhatIsQuickTicket").unbind("click",Me.cmdWhatIsQuickTicket)
                .bind("click",Me.cmdWhatIsQuickTicket);
                jQuery(".cmdSampleQT").unbind("click",Me.cmdSeeSample)
                .bind("click",Me.cmdSeeSample);
                if(jQuery("#isnewqt").val() == "1")
                {
                    jQuery("#cmdWhatIsQuickTicket").click();
                }    
            }
        });
    }
    //----------------------------------------------------
    this.cmdAddEditQuickTickets = function()
    {
        var  paddingleft = 0;
        if(jQuery(this).attr('id') == "cmdAddEditQuickTicket") paddingleft = -225;
        Me._roll.autohide(false);
        var opt = {};
        opt.width    = 400;
        opt.height   = 250;
        opt.paddingleft   = paddingleft;
        opt.paddingTop   = 0;
        //opt.position = 'middle';
        opt.title    = '<center>Add / Edit QuickTicket Views</center>';
        opt.body     = "<div id='contantAddEditQuickTicket'><center><img src='/widgets/images/wait.gif' border='0' /></center></div>";
        Me._roll.showNotAjax(this, this, opt);
        
        var el = this;
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/quick-ticket/client-edit",
            data: "client_id="+Me.client_id,
            success:function( html ) {
                jQuery("#contantAddEditQuickTicket").html(html);
                //build event Save 
                jQuery("#cmdAddNewQuickTicket").unbind("click",Me.cmdAddNewQuickTicket)
                .bind("click",Me.cmdAddNewQuickTicket);
                jQuery("#cmdSaveQuickTecket").unbind("click",Me.cmdSaveQuickTecket)
                .bind("click",Me.cmdSaveQuickTecket);
                jQuery(".cmdCancelQuickTecket").unbind("click",Me.hideRoll)
                .bind("click",Me.hideRoll);
            }
        });
    }
    //----------------------------------------------------
    this.cmdAddNewQuickTicket = function()
    {
        var html = "<tr qtid='0'>\
                        <td style='padding:3px;'>"+Me.companyName+" - <input style='width:130px;' type='text' class='txtQuickTicketName' /></td>\
                        <td style='padding:3px;' align='center' width='20%'><input type='checkbox' checked='true' class='QuickTicketChk' /></td>\
                    </tr>";
        jQuery("#contentQT").find('tbody').append(html);
    }
    //------------------------------------------------------------
    this.validEmailListNew = function(list) {
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var address = list;
        if(reg.test(address) == false) {
            return false;
        }
        return true;
    }
    ////----------------------------------------------------
    this.cmdSaveQuickTecket = function()
    {
        var data = "";
        var error = false;
        var ik = 0;
        var e = null;
        var el = [];
        jQuery("#tbodyQuickTiket").find('tr').each(function(i) {
            var value = jQuery(this).find('.txtQuickTicketName').val();
            //var email = jQuery(this).find('.txtQuickTicketEmail').val();
            var active = jQuery(this).find('.QuickTicketChk').is(':checked')?1:0;
            var qtid = jQuery(this).attr("qtid");
            el[i] = jQuery(this).find('.txtQuickTicketName');
            jQuery(this).find('.txtQuickTicketName').removeClass('avaidQT');
            if(jQuery.trim(value) != "")
            {
                data += ',{"id":"'+qtid+'","value":"'+Base64.encode(value)+'","active":"'+active+'"}';
                jQuery(this).find('.txtQuickTicketName').removeClass('errorQT');
            }    
            else
            {
                if(jQuery.trim(value) == "")
                {
                    if(i==0) e = jQuery(this).find('.txtQuickTicketName');
                    jQuery(this).find('.txtQuickTicketName').addClass('errorQT');
                    error = true;
                    ik++;
                }
            }    
        });
        // check blank
        if(error) 
        {
            jQuery("#QuickTicketError").html('You missed '+ik+' fields. They have been highlighted below');
            jQuery(e).focus();
            return false;
        }    
        // check existing
        var error = false;
        var e1 = null;
        var k = 0;
        if(el.length > 0)
        {
            for(var i = 0; i < el.length; i++)
            {
                var val1 = jQuery(el[i]).val();
                e1 = jQuery(el[i]);
                for(var j = i+ 1 ; j < el.length; j++)
                {
                    var val2 = jQuery(el[j]).val();
                    if(jQuery.trim(val1) == jQuery.trim(val2))
                    {
                        jQuery(el[j]).addClass('errorQT');
                        if(!error) e = jQuery(el[j]);
                        k++;
                        error = true;
                    }    
                }  
                if(error) break;
            }    
        }   
        if(error) 
        {
            jQuery("#QuickTicketError").html('You have '+k+' fields existing. They have been highlighted below');
            jQuery(e1).addClass('avaidQT');
            jQuery(e).focus();
            return false;
        }
        // run
        if(data != "")
        {
            data = "["+data.substring(1, data.length) + "]";
        }  
        
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/quick-ticket/client-save",
            data: "json="+data + "&client_id="+Me.client_id,
            success:function( html ) {
                if(html.success && Me._roll != null)
                {
                    Me._roll.hide();
                    Me.getdataQuickTickets();
                }    
            }
        });
    }
    this.isEmail = function(value){
        var re = new RegExp("^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]{2,4}$");
        return (value.search(re) != -1);
    }
    ////--------------------------------------------------
    this.cmdEditProjectQT = function()
    {
        Me._roll.autohide(false);
        var opt = {};
        opt.width    = 400;
        opt.height   = 250;
        opt.paddingleft   = -250;
        opt.paddingTop   = 0;
        //opt.position = 'middle';
        opt.title    = '<center>Select Projects for '+jQuery(this).attr('CompanyName')+' '+jQuery(this).attr('CustomerName')+'</center>';
        opt.body     = "<div id='contantEditProjectQT'><center><img src='/widgets/images/wait.gif' border='0' /></center></div>";
        Me._roll.showNotAjax(this, this, opt);
        
        var el = this;
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/quick-ticket/client-project-edit",
            data: "id="+jQuery(el).attr('qtid')+"&client_id="+Me.client_id,
            success:function( html ) {
                jQuery("#contantEditProjectQT").html(html);
                jQuery(".cmdCancelQuickTecket").unbind("click",Me.hideRoll)
                .bind("click",Me.hideRoll);
                jQuery("#cmdSaveProjectQuickTecket").unbind("click",Me.cmdSaveProjectQuickTecket)
                .bind("click",Me.cmdSaveProjectQuickTecket);
            }
        });
    }
    ////----------------------------------------------------
    this.hideRoll = function()
    {
        if(Me._roll != null)
        {
            Me._roll.hide();
        }    
    }
    ////----------------------------------------------------
    this.cmdSaveProjectQuickTecket = function()
    {
        var data = "";
        jQuery("#contentProjectQT").find("#tbodyProjectQuickTiket .QuickTicketChk").each(function(){
            if(jQuery(this).is(":checked"))
            {
                data += ","+jQuery(this).attr("qtid");
            }    
        });
        if(data !="")
        {
            data = data.substring(1, data.length);
        }    
        var idqt = jQuery("#hidQTid").val();
        var el = this;
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/quick-ticket/client-save-project-edit",
            data: "proid="+data+"&id="+idqt+"&client_id="+Me.client_id,
            success:function( html ) {
                if(html.success && Me._roll != null)
                {
                    Me._roll.hide();
                    Me.getdataQuickTickets();
                }  
            }
        });
    }
    //------------------------------------------------------------
    this.cmdAddUserQT = function()
    {
        Me._roll.autohide(false);
        var opt = {};
        opt.width    = 300;
        opt.height   = 250;
        opt.paddingleft   = -160;
        opt.paddingTop   = 0;
        //opt.position = 'middle';
        opt.title    = '<center>Add/Edit Customer User</center>';
        opt.body     = "<div id='contantAddUserQT'><center><img src='/widgets/images/wait.gif' border='0' /></center></div>";
        Me._roll.showNotAjax(this, this, opt);
        
        var el = this;
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/quick-ticket/client-user-add",
            data: "id="+jQuery(el).attr('qtid')
            +"&CompanyName="+jQuery(el).attr("CompanyName")
            +"&CustomerName="+jQuery(el).attr('CustomerName')
            +"&client_id="+Me.client_id,
            success:function( html ) {
                jQuery("#contantAddUserQT").html(html);
                jQuery(".cmdCancelQuickTecket").unbind("click",Me.hideRoll)
                .bind("click",Me.hideRoll);
                jQuery("#cmdSaveUserQuickTecket").unbind("click",Me.cmdSaveUserQuickTecket)
                .bind("click",Me.cmdSaveUserQuickTecket);
            }
        });
    }
    //------------------------------------------------------------
    this.cmdSaveUserQuickTecket = function()
    {  
        if(Me.checkValidUser())
        {
            // save data
            var data = jQuery("#formAddUserQT").serialize();
            if(jQuery.trim(data) != "") data +="&client_id="+Me.client_id;
            else data += "client_id="+Me.client_id;
            $.ajax({
                type: "POST",
                url: "/widgets/dashboard/quick-ticket/client-save-user-add",
                data: data,
                success:function( html ) {
                    if(html.success == 0)
                    {
                        Me.showMessages(html);
                    }   
                    else
                    {
                        Me._roll.hide();
                        Me.getdataQuickTickets();
                    }    
                }
            });
        } 
    }
    //------------------------------------------------------------
    this.checkValidUser = function()
    {
        Me._redirect = false;
        var updatestatus = {
            success:1, 
            errors:[]
        };

        if($('#ContactName').val() == '') updatestatus.errors.push('Contact Name is empty but reqired');
        if($('#ContactPhone1').val() == '') updatestatus.errors.push('Primary Phone is empty but reqired');
        if($('#Email1').val() == '') updatestatus.errors.push('Email Address is empty but reqired');
        if($('#Username').val() == '') updatestatus.errors.push('Desired Username is empty but reqired');
        if($('#Password').val() == '') updatestatus.errors.push('Desired Password is empty but reqired');
        if($('#PasswordConf').val() == '' && typeof($('#PasswordConf').get(0)) != 'undefined') updatestatus.errors.push('Confirm Password is empty but reqired');
        if(!Me.isValidContactPhone($('#ContactPhone1').val(),true) && $('#ContactPhone1').val()!='')
            updatestatus.errors.push('Primary Phone must be a valid 10 digit number with optional extension similar to ###-###-#### x ####');
        if(!Me.validEmailListNew($('#Email1').val()) && $('#Email1').val()!='')
            updatestatus.errors.push('Primary Email Address is not a valid email address');
        if($('#Password').val() != $('#PasswordConf').val() && typeof($('#PasswordConf').get(0)) != 'undefined') updatestatus.errors.push('Values in Desired Password and Confirm Password are not the same');

        if($('#Active').val() == '') updatestatus.errors.push('Active is empty but reqired');
        if(updatestatus.errors.length <= 0)
        {
            return true;
        }
        else
        {
            updatestatus.success = 0;
            Me.showMessages(updatestatus);
            return false;
        }    
    }
    //------------------------------------------------------------
    this.cmdEditUserQT = function()
    {
        Me._roll.autohide(false);
        var opt = {};
        opt.width    = 300;
        opt.height   = 230;
        //opt.position = 'middle';
        opt.title    = '<center>Edit Customer User</center>';
        opt.body     = "<div id='contantEditUserQT'><center><img src='/widgets/images/wait.gif' border='0' /></center></div>";
        Me._roll.showNotAjax(this, this, opt);
        
        var el = this;
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/quick-ticket/client-user-edit",
            data: "id="+jQuery(el).attr('qtid')
            +"&userid="+jQuery(el).attr('userid')
            +"&CompanyName="+jQuery(el).attr("CompanyName")
            +"&CustomerName="+jQuery(el).attr('CustomerName')
            +"&client_id="+Me.client_id,
            success:function( html ) {
                jQuery("#contantEditUserQT").html(html);
                jQuery(".cmdCancelQuickTecket").unbind("click",Me.hideRoll)
                .bind("click",Me.hideRoll);
                jQuery("#cmdSaveEditUserQuickTecket").unbind("click",Me.cmdSaveEditUserQuickTecket)
                .bind("click",Me.cmdSaveEditUserQuickTecket);
            }
        });
    }
    //------------------------------------------------------------
    this.cmdRemoveUserQT = function()
    {
        var userid = jQuery(this).attr("userid");
        var username = jQuery(this).attr("username");
        Me._roll.autohide(false);
        var opt = {};
        opt.width    = 300;
        opt.height   = 80;
        opt.paddingleft   = -250;
        opt.paddingTop   = 0;
        //opt.position = 'middle';
        opt.title    = '<center>Remove User</center>';
        opt.body     = "<div id='contantEditUserQT'>Are you sure to delete the '"+username+"' user?</div>"
        +"<div style='padding-top:5px;'>"
        +'<div style="float: right;"><input type="button" id="cmdDeleteUserQuickTecket" value="Delete" class="link_button_popup" /></div>'
        +'<div style="float: right;"><input type="button" value="Cancel" class="link_button_popup cmdCancelQuickTecket" /></div>'
        +"</div>";
        Me._roll.showNotAjax(this, this, opt);
        
        jQuery("#cmdDeleteUserQuickTecket").click(function() {
            Me.cmdDeleteUserQuickTecket(userid);
        });
        jQuery(".cmdCancelQuickTecket").unbind("click",Me.hideRoll)
        .bind("click",Me.hideRoll);
        
    }
    //------------------------------------------------------------
    this.cmdDeleteUserQuickTecket = function(userid)
    {
        var el = this;
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/quick-ticket/client-user-delete",
            data: "id="+userid+"&client_id="+Me.client_id,
            success:function( html ) {
                if(html.success == 0)
                {
                    Me.showMessages(html);
                }   
                else
                {
                    Me._roll.hide();
                    Me.getdataQuickTickets();
                }    
            }
        });
    }
    //------------------------------------------------------------
    this.cmdSaveEditUserQuickTecket = function()
    {  
        if(Me.checkValidUser())
        {
            // save data
            var data = jQuery("#formEditUserQT").serialize();
            if(jQuery.trim(data) != "") data += "&client_id="+Me.client_id;
            else data += "client_id="+Me.client_id;
            $.ajax({
                type: "POST",
                url: "/widgets/dashboard/quick-ticket/client-save-user-edit",
                data: data,
                success:function( html ) {
                    if(html.success == 0)
                    {
                        Me.showMessages(html);
                    }   
                    else
                    {
                        Me._roll.hide();
                        Me.getdataQuickTickets();
                    }    
                }
            });
        } 
    }
    //------------------------------------------------------------
    this.cmdWhatIsQuickTicket = function()
    {
        Me._rollInstruction.autohide(false);
        var opt = {};
        opt.width    = 250;
        opt.height   = 450;
        //opt.position = 'middle';
        opt.title    = '<center>What is QuickTicket?</center>';
        opt.body     = "<div id='contantEditUserQT'>\n\
                            QuickTicket gives your customers visibility to the work you are running for them. Designated customer users can view, sort and download Work Orders from the projects you select, allowing them to track project status, but see no technician pay or project cost details.\n\
                        </div>\n\
                        <hr style='background-color:#a6a6a6;margin-left:20px;margin-right:20px;'>\n\
                        <center><b>Instructions</b></center>\n\
                        <table cellspacing='0' cellpadding='0' border='0' width='100%'>\n\
                            <tr>\n\
                                <td style='vertical-align:top; width:15px;'>1.</td>\n\
                                <td colspan='2'>Click the Add/Edit QuickTicket Views button in the upper right corner to create a QuickTicket View.</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td></td>\n\
                                <td style='vertical-align:top;width:15px;'>- </td>\n\
                                <td>Suggestion: Name the QuickTicket View after the Customer you are serving.</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td></td>\n\
                                <td style='vertical-align:top;'>- </td>\n\
                                <td>The Email Contact will be notified when your Customer Users add a QuickTicket Work Order. These Work Orders will be placed in the Created Tab of your Work Order Dashboard and will not be published without your approval</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td style='vertical-align:top;'>2. </td>\n\
                                <td colspan='2'>Click the Add/Remove Projects button to select the projects your Customer will see.</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td style='vertical-align:top;'>3. </td>\n\
                                <td colspan='2'>Click the Add Customer User button to create a user account for your Customer. You can add as many users as you wish.</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td style='vertical-align:top;'>4. </td>\n\
                                <td colspan='2'>Direct your Customer to <a href='http://www.fieldsolutions.com/quickticket' target='_blank'>www.fieldsolutions.com/quickticket</a> to view Work Orders from the Projects you chose.</td>\n\
                            </tr>\n\
                        </table>";
        Me._rollInstruction.showNotAjax(this, this, opt);
    }
    //------------------------------------------------------------
    this.validEmailList = function(list) {
        if (list == null || list == "") return false;
        list = list.replace(/\s/g);
        var listParts = list.split(",");
        var valid = true;
        for (var index in listParts) {
            var email = listParts[index];
            if (!isValidEmail(email)) {
                valid = false;
            }
        }
        return valid;
    }
    //------------------------------------------------------------
    this.isValidContactPhone = function(val, allowExt) {
        if (val == null || val == "") return true;
        var valStrip = val.replace(/[^0-9]/g, "");
        if (valStrip.length == 10) {
            val = massagePhone(val);
            return isValidPhone(val);
        }
        else {
            if (!allowExt) return false;
            var validFormat = /^[(]?(\d{3})[)]?(?:\s|-|.)*(\d{3})[-\s.]*(\d{4})[^0-9]+([0-9]*)[^0-9]*([0-9]*)$/;
            var parts = val.match(validFormat);
            return !(parts == null);
        }
    }
    //------------------------------------------------------------
    this.cmdSeeSample = function()
    {
        $("<div></div>").fancybox({
            'autoDimensions' : true,
            'hideOnOverlayClick' : false,
            'content' : "<div style='width:900px;'><img style='width:900px;' src='/widgets/images/sampleQT.png' /><div>"
    }).trigger('click');
    }
    
    this.clientUsers = "#clientUsers";
    this.getdataUsers = function()
        {
        Me.LoadingAjax(Me.clientUsers);
        detailsWidget = new FSWidgetClientDetails({
            container:'clientUsers',
            tab:'listadmin'
        },Me._roll);
        detailsWidget.prepareForm = function() { 
            jQuery("#cmdActivityLogUser").unbind("click",Me.cmdActivityLogAccessSettings)
            .bind("click",Me.cmdActivityLogAccessSettings);
        }
        detailsWidget.setParams({
            Company_ID:Me.companyId,
            AccountEnabled:"1",
            size:"20",
            Admin:'1',
            ClientID:Me.client_id
        });
        detailsWidget.show();
        // add to manager
        if(!wManager._widgets.mainContainer){
            wManager.add(detailsWidget, 'mainContainer');
            wManager._widgets.mainContainer = detailsWidget;
        }
    }
    //------------------------------------------------------------
    this.cmdActivityLogUser_click = function(){
        Me.WaitingSave();
        var el = this;
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/popup/activity-log-user",
            data: "",
            success:function( html ) {
                var autohide = false;
                Me._rollWaitingSave.hide();
                Me._rollActivityLog.autohide(autohide);
                var opt = {};
                opt.width    = 600;
                opt.height   = 400;
                opt.position = 'top';
                opt.wTop = 10;
                opt.title    = '<div style="text-align: center;">Activity Log<div>';
                opt.body     = html;
                Me._rollActivityLog.showNotAjax(el, el, opt);
            }
        });
    }
    //------------------------------------------------------------
    this.toggleTab = function(tab) {
        if (tab == undefined) {
            if (Me._clickedTab == undefined) return;
            tab = Me._clickedTab;
        }
            jQuery("#tabbedMenuNew").find('li').attr('class','');
            jQuery("#tabbedMenuNew").find('li').children('a').attr('class','');
        jQuery(tab).attr("class","currenttab");
        jQuery(tab).children('a').attr("class","current");
        var tabid = jQuery(tab).attr("tabid");
            jQuery("#clientProfile").find(".contentBodyTab").css("display","none");
            jQuery("#"+tabid).css("display","");
            //show tab profile
        Me.configtab(tabid,false);		
    }
    //------------------------------------------------------------
    this.configtab = function(tabid,buildtab)
    {
        if(typeof(tabid) == 'undefined' || jQuery.trim(tabid) == "")
        {
            tabid = 'detailsContainer';
        }
            switch (tabid)
            {
                case 'detailsContainer':
                    Me.getdataProfile('view');
                    break;
                case 'clientAccessSettings':
                    Me.getdataAccess('view')
                    break;
                case 'clientProjectSettings':
                    //Attention('asdasdasdas');
                    break;    
               case 'clientQuickTickets':
                    Me.getdataQuickTickets();
                    break;      
            case 'clientUsers':
                Me.getdataUsers();
                break;      
            }
        if(buildtab)
        {
            jQuery('#tabbedMenuNew li').removeClass('currenttab'); 
            jQuery('#tabbedMenuNew li a').removeClass('current'); 
            jQuery('#tabbedMenuNew li[tabid="'+tabid+'"]').addClass('currenttab');
            jQuery('#tabbedMenuNew li[tabid="'+tabid+'"] a').addClass('current');
        }
        jQuery('#'+tabid).show();
    }
    //------------------------------------------------------------
    this.init = function() {
//        if(jQuery("#detailsContainer").css('display') != 'none')
//        {
//            Me.getdataProfile();
//        }
        jQuery("#tabbedMenuNew li").click(function() {
            Me.toggleTab(this);
        }); 
        Me.configtab(Me.tab,true);
    }
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
}