


//-------------------------------------------------------------
//-------------------------------------------------------------
//-------------------------------------------------------------
phone_audit_log.Instances = null;
//-------------------------------------------------------------
phone_audit_log.CreateObject = function(config) {
    var obj = null;
    if (phone_audit_log.Instances != null) {
        obj = phone_audit_log.Instances[config.id];
    }
    if (obj == null) {
        if (phone_audit_log.Instances == null) {
            phone_audit_log.Instances = new Object();
        }
        obj = new phone_audit_log(config);
        phone_audit_log.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function phone_audit_log(config) {
    var Me = this;
    this.id = config.id;
    //------------------------------------------------------------
    //------------------------------------------------------------
    //------------------------------------------------------------
    //------------------------------------------------------------
    //------------------------------------------------------------
    this.dataInput = jQuery("#frmData").serialize();
    this.column = "auditdate";
    this.sort = "DESC";
    this.getData = function(columnsort,typesort,_dataInput,page)
    {
        window.detailObject.onInit.showStartPopup();
        var data = _dataInput;
        data += "&columnsort="+columnsort+"&typesort="+typesort;
        $.ajax({
            type: "POST",
            url: "/widgets/admindashboard/tech/getauditphone?page="+page,
            data: data,
            success:function( html ) {
                jQuery("#divReport").html(html);
                window.detailObject.onInit._roll.hide();
                jQuery(".cmdSort").unbind("click",Me.cmdSort_Onclick)
                .bind("click",Me.cmdSort_Onclick);
                jQuery(".paginatorLinkActiv").unbind("click",Me.paginatorLinkActiv_Onclick)
                .bind("click",Me.paginatorLinkActiv_Onclick);
            }
        });
    }
    //------------------------------------------------------------
    this.cmdSort_Onclick = function()
    {
        var sorttype = jQuery(this).attr("sort");
        var column = jQuery(this).attr("collumn");
        var page = jQuery(this).attr("page");
        if(sorttype == "DESC")
        {
            sorttype = "ASC";    
        }
        else if(sorttype == "ASC")
        {
            sorttype = "DESC"; 
        }
        Me.column = column;
        Me.sort = sorttype;
        Me.getData(column,sorttype,Me.dataInput,page);
    }
    //------------------------------------------------------------
    this.paginatorLinkActiv_Onclick = function()
    {
        var page = jQuery(this).attr("page");
        Me.getData(Me.column,Me.sort,Me.dataInput,page);
    }
    //------------------------------------------------------------
    Me.cmdSearch_Onclick = function()
    {
        var _dataInput = jQuery("#frmData").serialize();
        Me.dataInput = _dataInput;
        Me.column = "auditdate";
        Me.sort = "DESC";
        Me.getData("auditdate","DESC",_dataInput,1);
    }
    //------------------------------------------------------------
    this.cmdDownload_Onclick = function()
    {
        var data = Me.dataInput;
        data += "&columnsort="+Me.column+"&typesort="+Me.sort;
        
        window.open('/admin/ajax/report_phone_audit_log.php?'+data);
    }
    //------------------------------------------------------------
    this.init = function() {
        jQuery("#cmdSearch").unbind("click",Me.cmdSearch_Onclick)
        .bind("click",Me.cmdSearch_Onclick);
        $( "#txtDateportFrom" ).datepicker();
        $( "#txtDateportTo" ).datepicker();
        Me.cmdSearch_Onclick();
        jQuery("#cmdDownload").unbind("click",Me.cmdDownload_Onclick)
        .bind("click",Me.cmdDownload_Onclick);
        jQuery(window).keyup(function(event) {
            if(event.which == 13)
            {
                jQuery("#cmdSearch").click();
            }
            
        });
    }
//------------------------------------------------------------

}