


//-------------------------------------------------------------
//-------------------------------------------------------------
//-------------------------------------------------------------
UpdatePasswordList.Instances = null;
//-------------------------------------------------------------
UpdatePasswordList.CreateObject = function(config) {
    var obj = null;
    if (UpdatePasswordList.Instances != null) {
        obj = UpdatePasswordList.Instances[config.id];
    }
    if (obj == null) {
        if (UpdatePasswordList.Instances == null) {
            UpdatePasswordList.Instances = new Object();
        }
        obj = new UpdatePasswordList(config);
        UpdatePasswordList.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function UpdatePasswordList(config) {
    var Me = this;
    this.id = config.id;
    
    //------------------------------------------------------------
    this.loadClient = function(page){
        _global.data = "page="+page;
        _global.url = "/widgets/admindashboard/client/clientnotupdatepassword";
        var _onSuccess = function(data){
            jQuery("#clientnotupdatepassword").html(data);
            jQuery("#clientnotupdatepassword .paginatorLinkActiv")
                .unbind("click",Me.paginatorLinkActiv_client)
                .bind("click",Me.paginatorLinkActiv_client);
        }
        _global.onSuccess = _onSuccess;
        _global.show();
    }
    this.paginatorLinkActiv_client = function(){
        var page = jQuery(this).attr("page");
        Me.loadClient(page);
    }
    //------------------------------------------------------------
    this.loadTech = function(page){
        _global.data = "page="+page;
        _global.url = "/widgets/admindashboard/client/technotupdatepassword";
        var _onSuccess = function(data){
            jQuery("#technotupdatepassword").html(data);
            jQuery("#technotupdatepassword .paginatorLinkActiv")
                .unbind("click",Me.paginatorLinkActiv_tech)
                .bind("click",Me.paginatorLinkActiv_tech);
        }
        _global.onSuccess = _onSuccess;
        _global.show();
    }
    this.paginatorLinkActiv_tech = function(){
        var page = jQuery(this).attr("page");
        Me.loadTech(page);
    }
    //------------------------------------------------------------
    this.init = function() {
        Me.loadClient(1);
        Me.loadTech(1);
    }
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
}
