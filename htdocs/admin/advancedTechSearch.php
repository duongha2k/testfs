<?php
/*

4/9/2012 VCR:
    ADDED: added settings check

9/29/08 JC:
	FIXED: The advanced search function reveals "no results" when a search for all techs with a resume is run.  I changed function addQuery to include the $type ("IS", "IS NOT") in a "NULL" search.  Originally it just added "IS NULL" anytime the variable was "NULL", which ignored whether we want to check for null or not null.

9/25/08 JC:
	TODO: Add private, w2, payee, and dispatch fields to admin advanced tech search.

8/12/08 JC:
	FIXED: AcceptTerms clause wasn't properly parenthesized

7/25/08 JC:
	FIXED: "Is Blank" was only returning NULL values.  Changed to "IS NULL or = ''"

7/22/08 JC:
	FIXED: The CSV download was trying to send the "maximum record count" number of records instead of the proper increment, so it was timing out and causing problems.  Fixed now.
	FIXED: Now deletes the CSV file after sending it.

7/21/08 JC:
	FIXED: 5000-count didn't work.  Changed to 4000.

7/18/08 JC:
	FIXED: 8000-count downloads causing errors, reduced to 5000
	FIXED: On default search, all downloads work except 5001-10000

7/15/08 JC:
	DONE: Fix CSS alignment in Safari

	http://www.assembla.com/spaces/field_sw/tickets/471#comment:6

	DONE: include Referred By column in display and download. 25 characters for display is fine, but full field for download. This is actually pretty critical as I need to be able to extract from information found in this field.

7/15/08 JC http://www.assembla.com/spaces/field_sw/tickets/471:
	TODO: Preferred Tech – the drop down list will be: Any, All, Listing of Clients where you can choose.
	TODO: Date of Preferred Tech status – same as other search criteria that has a date range (data doesn't exist currently)
	TODO: Display – Who the tech is Preferred for. should display on one row, with a comma separating each client. Date of Preferred status.
	TODO: What happens if a tech loses preferred status? Add a date for Preferred Status removed (data doesn't exist currently)

	DONE: Accept Terms set to Yes, and a 1 week date range, returned 16000 records. It returned all those with Yes and a blank date range and those with a date weren’t in the range that was selected
	DONE: BC Passed - need to add "blank" in the drop down. Yes should return those that passed, No returns those that didn't, blank returns those without any action. I selected No and it returned 2600 records. Because we don't show a results field (only the date), it's hard to tell if failed, maybe we put a 'F'? in front of the date-just an idea
	DONE: BC Requested – Y, BC Passed – N, showed 21, not right. Should only show those with a real No, this is probably related to #2
	DONE: Accept Terms, change heading in report and on display to Accept ICA. When they register they implicitly accept terms of use. It gets confusing.
	DONE: Add a date range to the FLS CSP field. This will allow dan to know how many csp’s he’s processed in a given time period.


7/1/08 JC http://www.assembla.com/spaces/field_sw/tickets/264
	DONE: Add FS Certification
	DONE: Remove "_JC" from $Current_Page constant

6/23/08 JC:
	FIXED: Add proximity search to zipcode
	FIXED: Email blast list only appears on first page of results

6/20/08 JC:
	FIXED: "yes w/ blank rating" gives error on Skill Rating
	FIXED: Change color of second button
	FIXED: CSV doesn't return first record
	FIXED: Find techs w/ parsing issues and fix

6/19/08 JC:
	FIXED: Removed overlap of first/last records in multiple CSV downloads
	FIXED: "Certifications" column is "Dell Certifications"
	FIXED: Client Denied column shows no data
	FIXED: Can sort by Client Denied column
	FIXED: Can sort by Last Logged On Date
	FIXED: Pagination retains the sort by field and order
	FIXED: BG Check Req Date
	FIXED: BG Check Pass Date

	FIXED: Pagination doesnt retain search criteria?
	FIXED: Second "Submit" button on search criteria
	FIXED: Blank? option on CompTIA search
	FIXED: Is Not Blank? option on all "Is blank" options

	FIXED: eliminate duplication in CSV download code
	FIXED: remove extra query for email blast

*/
//ini_set("display_errors","stdout");

//session_cache_limiter('public');

// =============================================================
// CONSTANTS


$ShowQuery = false;

$Current_Page = $_SERVER['PHP_SELF'];
$Max_Results = 25;
$Page_Block_Size = 10;
$Default_Sort_Order = "TechID";
$Default_Sort_Dir = "ASC";

$sortByParam = $Default_Sort_Order;
$sortDir = $Default_Sort_Dir;

$max_records = 5000;
$first_record = 1;


if (isset($_GET['inc'])) {
	$max_records = $_GET['inc'];
}

// CLAUSES
// subqueries and conditionals for sorting


$resumeClause = "(case when Resume IS NOT NULL then 'Y' else 'N' end)";
$DrugPassedClause = "DrugTest_Pass";
$SilverDrugPassedClause = "SilverDrugTest_Pass";

/*$lastWORunMainClause = "(select top 1 Work_Orders.StartDate from Work_Orders where TR_Master_List.TechID = Work_Orders.Tech_ID order by StartDate DESC)";

$lastWORunFLSClause = "(select top 1 FLS_Work_Orders.PjctStartDate from FLS_Work_Orders where FLS_Work_Orders.FLSTechID <> '' and CAST(TR_Master_List.TechID as nvarchar) = FLS_Work_Orders.TB_ID order by PjctStartDate DESC)";

$lastWORunAnyClause = "(case when exists $lastWORunMainClause and not exists $lastWORunFLSClause then $lastWORunMainClause when exists $lastWORunFLSClause and not exists $lastWORunMainClause then $lastWORunFLSClause when $lastWORunMainClause >= $lastWORunFLSClause then $lastWORunMainClause when $lastWORunFLSClause > $lastWORunMainClause then $lastWORunFLSClause else '' end)";*/

//$lastWOBidMainClause = "(select top 1 Work_Order_Applicants.Bid_Date from Work_Order_Applicants where TR_Master_List.TechID = Work_Order_Applicants.TechID order by Bid_Date DESC)";
$lastWOBidMainClause = "(select MAX(Bid_Date) from work_orders__bids AS w where w.TechID = t.TechID AND IFNULL(w.Company_ID,'') != 'FLS' order by Bid_Date DESC)";
//$lastWOBidFLSClause = "(select top 1 FLS_Work_Order_Applicants.Bid_Date from FLS_Work_Order_Applicants where TR_Master_List.TechID = FLS_Work_Order_Applicants.TechID order by Bid_Date DESC)";
$lastWOBidFLSClause = "(select MAX(Bid_Date) from work_orders__bids AS w where w.TechID = t.TechID AND IFNULL(w.Company_ID,'') = 'FLS' order by Bid_Date DESC)";

$lastWOBidAnyClause = "(select MAX(Bid_Date) from work_orders__bids AS w where w.TechID = t.TechID order by Bid_Date DESC)";

$lastWORunMainClause = "(select MAX(StartDate) from work_orders AS w where w.Tech_ID = t.TechID AND IFNULL(w.Company_ID,'') != 'FLS' order by StartDate DESC)";

$lastWORunFLSClause = "(select MAX(StartDate) from work_orders AS w where w.Tech_ID = t.TechID AND w.Company_ID = 'FLS' order by StartDate DESC)";

$lastWORunAnyClause = "(select MAX(StartDate) from work_orders AS w where w.Tech_ID = t.TechID order by StartDate DESC)";


require_once("../headerStartSession.php");
require_once("../library/caspioAPI.php");
require_once("../library/googleMapAPI.php");

//--- $LoggedOnDateGreater, $LoggedOnDateLess
if($_POST)
{
    $LoggedOnDateGreater = getFormField("LoggedOnDateGreater");
    $LoggedOnDateLess = getFormField("LoggedOnDateLess");
    $_SESSION['LoggedOnDateGreater'] = $LoggedOnDateGreater;
    $_SESSION['LoggedOnDateLess'] = $LoggedOnDateLess;    
}
else
{
    $LoggedOnDateGreater = empty($_SESSION['LoggedOnDateGreater']) ? "" : $_SESSION['LoggedOnDateGreater'];
    $LoggedOnDateLess = empty($_SESSION['LoggedOnDateLess']) ? "" : $_SESSION['LoggedOnDateLess'];    
}
$_SESSION['LoggedOnDateGreater']=$LoggedOnDateGreater;
$_SESSION['LoggedOnDateLess']=$LoggedOnDateLess;
//--- dateClaues
$dateClause = "(select login_history.DateTime from login_history where " .Core_Database::TABLE_TECH_BANK_INFO . ".UserName = login_history.UserName";
$dateClause .= " AND login_history.UserType = 'Tech' ";
if (!empty($LoggedOnDateGreater)) $dateClause .= " AND login_history.DateTime >= '$LoggedOnDateGreater'";
if (!empty($LoggedOnDateLess)) $dateClause .= " AND login_history.DateTime <= '$LoggedOnDateLess'";
$dateClause .= " order by login_history.DateTime DESC LIMIT 1)";
$I9StatusClause = "(select tech_ext.I9Status from tech_ext where " .Core_Database::TABLE_TECH_BANK_INFO . ".TechID = tech_ext.TechID LIMIT 1)";
$I9DateClause = "(select tech_ext.I9Date from tech_ext where " .Core_Database::TABLE_TECH_BANK_INFO . ".TechID = tech_ext.TechID LIMIT 1)";

if ($_POST) {
	$deniedList = Core_Tech::getClientDeniedTechsArray();
	$clientDeniedSelect = "(case when FLSstatus = 'Admin Denied' then 'FLS' when TechID IN (" . implode(',', $deniedList) . ") then 'ANY' else '' end)";
}

$qs = explode("&", $_SERVER['QUERY_STRING']);

foreach ($qs as $key=>$val) {
	$k = explode("=", $val);
	if ($k[0] == "DisplayOption") {
		$_SESSION["DisplayOption"] = $k[1];
		unset($qs[$key]);
	}
}

$currentURL = $_SERVER['PHP_SELF'] . "?" . implode("&", $qs);

checkAdminLogin2();
//841
parse_str(parse_url($_SESSION['currentlink'], PHP_URL_QUERY), $queries1);
$_SESSION['currentlink']=$currentURL;

//--- Last Post 13761
if($_POST) {
    $_SESSION['LastPost'] = $_POST;
} else {
    if(isset($_GET["f"]))
    {
        $_SESSION['LastPost']="";
        $sel_txt_elements=array();
        header("Location:advancedTechSearch.php");
    }
    else if (empty($queries1))
    {
        $LastPost ="";
        $sel_txt_elements=array();
    }
    else 
    {
        $LastPost = $_SESSION['LastPost'];
        foreach($LastPost as $key=>$val)
        {
            $strsel_txt_elements.=$key.",";
        }
        $sel_txt_elements = explode(",", $strsel_txt_elements);
    }
}
//end 841
$last_sel_txt_elements = array();
foreach($sel_txt_elements as $ele)
{
    if(!empty($LastPost[$ele]) || $LastPost[$ele] == '0' )
    {
        $last_sel_txt_elements[$ele] = $LastPost[$ele];
    }
}
//end 13761



if (!isset($_GET['first'])) {
?>
<?php header("Content-Type: text/html; charset=utf-8");?>
<?php $page = 'admin'; ?>
<?php $option = 'techs'; ?>
<?php $selected = 'advanced'; ?>

<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>

<style type="text/css">@import url(../library/ui.datepicker.css);</style>
<script type="text/javascript" src="../library/ui.datepicker.js"></script>

<!-- Add Content Here -->
<script>
$(document).ready(function(){
    <?php 
    //841
        if(count($last_sel_txt_elements)>0)
        { 
            foreach($last_sel_txt_elements as $k=>$v)
            {
                if($k=="KeyCertifiedContractor")
                {
                    ?>
                    $('.itemtags_28').css("display","");
                    <?
                }
                if($k=="PurpleTechnician")
                {
                    ?>
                    $('.itemtags_25').css("display","");
                    <?
                }
                if($k=="CompuComCertified")
                {
                    ?>
                    $('.itemtags_22').css("display","");
                    <?
                }
                if($k=="GGETechEvaluation")
                {
                    ?>
                    $('.itemtags_19').css("display","");
                    <?
                }
                ?>
                $('#<?=$k?>').val('<?=$v?>');
                <?php 
            }
        }
        //end 841
    ?>
});
</script>

<style type="text/css">
	div#search {
		width: 1150px;
		margin: auto;
	}

	div#results {
	}

	div#no_results {
		width: 100px;
		margin: auto;
		margin-top: 35px;
	}

	#search {

	}

	#search dl {
            /*border: 1px solid black;*/
		padding-bottom: 15px;
	}

	#search dl dt, #cabling dl dt {
		float: left;
		width: 190px;
		padding-bottom: 4px;
		min-height: 1px; /* required to give layout to empty elements */
	}
	#search dl dd {
		margin-left: 190px;
		padding-bottom: 4px;
	}
	
	.leftSkill {
	}
	
	.rightSkill {
		margin-left: 15px;
	}
	
	span.rightSkill {
		margin-left: 45px;
	}
	span.MidSkill {
		margin-left: 80px;
	}
	.skillHeading {
		font-weight: bold;
	}

	#telephony {
		width: 675px;
	}
	
	#cabling {
		width: 455px;
	}
	
	#cabling dl dt {
		width: 390px;
	}
	#cabling dl dd {
		margin-left: 360px !important;
		padding-bottom: 4px !important;
	}

	#telephony dl dt {
		width: 520px;
	}
	#telephony dl dd {
		margin-left: 360px !important;
		padding-bottom: 4px !important;
	}

	#bottom dl dd, #cabling dl dd {
		margin-left: 100px;
	}

	.col1 {
		width: 520px;
	}
	
	.col dl.ratings {
		margin-top: 10px;
	}

	dl.ratings select {
		margin-right: 5px;
	}
	
	.col1, #internal_1 {
		float: left;
		padding-right: 20px;
	}
	.col2, #internal_2 {
		float: right;
		width: 610px;
	}
	#bottom {
		clear: both;
	}
	
	.atsHeading {
		clear: both;
		border-top: 3px solid #000;
		padding-top: 5px;
	}
	
	.atsHeading, #atsTop {
		margin-bottom: 10px;
		font-weight: bold;
		font-size: 16px;
	}

	#ratings_1 {
		float: left;
		width: 540px;
	}
	#ratings_2 {
	}

	dl.ratings {
	}



	p.submit {
		clear: both;
		margin: auto;
		padding-top: 0;
		width: 200px;
	}
	p.submit input {
		background-color: #385c7e;
		color: white;
		font-weight: bold;
	}


	input#vSubmit {
		margin: 10px;
		padding: 2px;
	}

	dd.email select {
		width: 100px;
	}
    dd.certifications_5 select {
        width: 60px;
    }    
    dd.certifications_18 select {
        width: 60px;
    }   
    dd.certifications_19 select {
        width: 60px;
    }   
    dd.certifications_20 select {
        width: 60px;
    }   
    dd.certifications_21 select {
        width: 60px;
    }  
    dd.certifications_25 select {
        width: 60px;
    } 
     dd.certifications_26 select {
        width: 60px;
    }
    dd.certifications_24 select {
        width: 60px;
    }
    dd.certifications_31 select {
        width: 60px;
    }  
    dd.certifications_32 select {
        width: 60px;
    }  
    /*408*/
    dd.EMCcertifications select {
        width: 60px;
    }    
	dl.ratings select.skill {
		width: 150px;
	}
	dl.ratings select.rating {
		width: 70px;
	}

	dd.date select {
		margin-right: 10px;
		width: 70px;
	}
	dd.accept_terms select {
		width: 140px;
	}
	dd.state select {
		width: 200px;
	}
	dd.client_denied select {
		width: 120px;
	}
	dd.fls_status select {
		width: 120px;
	}
	select.yes_no {
		width: 90px;
	}
        .certificationscbx select{
            width: 90px;
        }
	select.blank {
		width: 100px;
	}
	dd.certifications select {
		width: 300px;
	}
	.zipcode input {
		width: 45px;
	}
	.zipcode select {
		width: 145px;
	}


</style>


<style  TYPE="text/css">
.label{ font-size:12px; color:#385C7E;}

.resultTable {
	border: 1px solid black;
	border-spacing: 0;
	text-align: center;
	color: #000000;
	padding: 0;
	border-collapse: collapse;
}

.resultTop {
	background-color: #949CCE;
	border-top: 1px solid #000000;
	border-left: 2px solid #000000;
	border-right: 2px solid #000000;
}

.leftHeader {
	border-left: 2px solid #2A497D;
}

.rightHeader {
	border-right: 2px solid #2A497D;
}

.middleHeader {
	border-left: 1px solid #FFFFFF;
	border-right: 1px solid #FFFFFF;
}

.resultHeader a, .resultRow a:hover {
	color: #2A497D;
}

.resultHeader a:hover, .resultRow a {
	color: #6373B5;
}

.resultHeader td {
	ccolor: #000099;
	color: #444;
	text-align: center;
	background-color: #A0A8AA;
	padding: 7px 10px;
	font-weight: bold;
	font-size:small;
	ccursor: pointer;
	border-top: 2px solid #2A497D;
}

.resultRow {
	background-color: #FFFFFF;
	border: 1px solid #EAEFF5;
}

.resultRow:hover {
	background-color: #EAEFF5;
}

.leftRow {
	border-left: 2px solid #2A497D;
}

.rightRow {
	border-right: 2px solid #2A497D;
}

.resultRow td {
	ppadding: 0px 10px;
	padding: 1px 3px;
}

.pagination td {
	padding: 2px 10px;
}
.pagination td.left {
	border-left: 2px solid #2A497D;
}
.pagination td.right {
	border-right: 2px solid #2A497D;
}

.pagination td img {
	padding: 0 5px;
}

.resultBottom td {
	background-color: #A0A8AA;
	border: 2px solid #2A497D;
}
.resultBottom td.left {
	border-right: none;
}

.resultBottom td.right {
	border-left: none;
}
</style>
<?php
}

// =============================================================
// FUNCTIONS
/**
 * rm() -- Vigorously erase files and directories.
 *
 * @param $fileglob mixed If string, must be a file name (foo.txt), glob pattern (*.txt), or directory name.
 *                        If array, must be an array of file names, glob patterns, or directories.
 */
function rm($fileglob)
{
    if (is_string($fileglob)) {
        if (is_file($fileglob)) {
            return unlink($fileglob);
        } else if (is_dir($fileglob)) {
            $ok = rm("$fileglob/*");
            if (! $ok) {
                return false;
            }
            return rmdir($fileglob);
        } else {
            $matching = glob($fileglob);
            if ($matching === false) {
                trigger_error(sprintf('No files match supplied glob %s', $fileglob), E_USER_WARNING);
                return false;
            }
            $rcs = array_map('rm', $matching);
            if (in_array(false, $rcs)) {
                return false;
            }
        }
    } else if (is_array($fileglob)) {
        $rcs = array_map('rm', $fileglob);
        if (in_array(false, $rcs)) {
            return false;
        }
    } else {
        trigger_error('Param #1 must be filename or glob pattern, or array of filenames or glob patterns', E_USER_ERROR);
        return false;
    }

    return true;
}


function get_date($array, $index) {
	if (array_key_exists($index, $array)) {
		return format_date($array[$index]);
	} else {
		return "";
	}
}

// FUNCTION myImplode
//
// surrounds the strings in an array with other strings
//
// useful in adding tags or quotes to the strings in an array
//
function myImplode($before, $after, $glue, $array){
		$output = '';
    $nbItem = count($array);
    $i = 1;
    foreach($array as $item){
        if($i < $nbItem){
            $output .= "$before$item$after$glue";
        }else $output .= "$before$item$after";
        $i++;
    }
    return $output;
}

// FUNCTION str_truncate
//
// truncates a string by the number of characters specified
//
function str_truncate($text, $chars=25) {
	if (strlen($text) > 25) {
		if ($text != "") {
			// Change to the number of characters you want to display
			$text = $text." ";
			$text = substr($text,0,$chars);

			$text = substr($text,0,strrpos($text,' '));

			$text .= "...";
		}
	}
	return $text;
}

// function format_date
//
// outputs date string in "MM/DD/YY" format.
//
function format_date($date_str) {
	$output = $date_str;
	if ($output != "") {
		$output = strftime("%m/%d/%y", strtotime($output));
	}
	return $output;
}

// FUNCTION process
//
// for use when processing an array returned by caspioSelectAdv.
// trims the quotes off and filters "NULL" into ""
//
function process($str) {
	$output = trim($str, "`");
	switch ($str) {
		case "NULL":
			$output = "";
			break;
		case "True":
		case "Yes":
			$output = "Y";
			break;
		case "False":
		case "No":
			$output = "N";
			break;
	}
	return $output;
}

// FUNCTION dateFields
//
// writes the greater-than and less-than input fields for date
// comparisons
//
function dateFields($name, $size = 8, $DateGreater="",$DateLess ="")
{
	$nameGreater = $name . "Greater";
	$nameLess = $name . "Less";



    return "(>=) <input name='$nameGreater' id='$nameGreater' size='$size' class='showCal' value='".$DateGreater."'/> (<=) <input name='$nameLess' id='$nameLess' size='$size' class='showCal' value='".$DateLess."'/>";
}
//13988
function filterFields($name, $size = 8)
{
    $nameGreater = $name . "Greater";
    $nameLess = $name . "Less";



    return "(>=) <input name='$nameGreater' id='$nameGreater' size='$size' /> (<=) <input name='$nameLess' id='$nameLess' size='$size'/>";
}

//end 13988

// FUNCTION yesNoSelect
//
// writes a select with three options, Yes, No, and "Any".
//
function yesNoSelect($name, $id="", $value="1", $selected="", $class_name = "yes_no") {
	if ($id == "") {
		$id = $name;
	}
	$selected_0 = "";
	$selected_1 = "";  
	switch ($selected) {
		case "Yes":
		case "1":
            $selected_1 = "selected";
			break;  
		case "No":
		case "0":
            $selected_0 = "selected";
			break;
	}

	if ($value == "1") {
		$yes = '1';
		$no = '0';
	} else {
		$yes = 'Yes';  
		$no= 'No';
	}

	return "<select name='$name' id='$id' class='$class_name'>
		<option value=''>Any&nbsp;&nbsp;</option>
		<option value='$yes' ". $selected_1.">Yes</option>
		<option value='$no' ". $selected_0.">No</option>
	 </select>";
}
//13931
//14056
function yesNoSelectnew($name, $id="", $value="2", $selected="", $class_name = "yes_no") {
    if ($id == "") {
        $id = $name;
    }
    $selected_0 = "";
    $selected_1 = "";
    $selected_2 = "";   
    switch ($selected) {
         case "Lapsed":
        case "2":    
            $selected_2 = "selected";  
            break;
        case "Yes":
        case "1":
            $selected_1 = "selected";
            break;
       
        case "No":
        case "0":
            $selected_0 = "selected";
            break;
    }

    if ($value == "2") {
        
        $lapsed = '2';
        $no = '0';
        $yes = '1';
    }
    else if($value == "1")
    {
        $lapsed = '2';
        $no = '0';
        $yes = '1';
    }
    else if($value == "0")
    {
        $lapsed = '2';
        $no = '0';
        $yes = '1';
    
    }
     else
      {
       // $yes = 'Yes';
       // $lapsed='Lapsed';  
       // $no= 'No';
    }

    return "<select name='$name' id='$id' class='$class_name'>
        <option value=''>Any&nbsp;&nbsp;</option>
        <option value='$yes' ". $selected_1.">Yes</option>
        <option value='$lapsed' ". $selected_2.">Unverified</option>
        <option value='$no' ". $selected_0.">No</option>
     </select>";
}
//end 14056
function yesNoSelectLevel($name, $id="", $value="1", $selected="", $class_name = "yes_no",$hasMultiTagLevels,$TagLevelsData) {
	if ($id == "") {
		$id = $name;
	}
	$selected_0 = "";
	$selected_1 = "";
	switch ($selected) {
		case "Yes":
		case "1":
            $selected_1 = "selected";
			break;
		case "No":
		case "0":
            $selected_0 = "selected";
			break;
	}

	if ($value == "1") {
		$yes = '1';
		$no = '0';
	} else {
		$yes = 'Yes';
		$no= 'No';
	}
       
    $i = 1;
    $level=1;  
    $stringBuild="";
    $stringBuild.="<select name='$name' id='$id' class='$class_name'>";
    $stringBuild.="<option value=''>Any&nbsp;&nbsp;</option>";
    $stringBuild.="<option value='$yes' ". $selected_1.">Yes</option>";
    $stringBuild.="<option value='$no' ". $selected_0.">No</option>";
   
    if($hasMultiTagLevels){  
        if(!empty($TagLevelsData)){
            foreach($TagLevelsData as $itm){                                               
                $level=$itm["id"];              
                $stringBuild.= "<option value=\"LevelID_$level\" >L$i</option>";
                $i++;
            }
        }
    }
    $stringBuild.="</select>";
    return $stringBuild;
}

//
// FUNCTION yesNoSelectWithClass
//
// writes a select with three options, Yes, No, and "Any".
//
function yesNoSelectWithClass($name, $class_name = "") {
	return yesNoSelect($name, "", "1", "", $class_name);
}

// FUNCTION yesNoBlankSelect
//
// writes a select with three options, Yes, No, and "Any".
//
function yesNoBlankSelect($name, $id="", $value="1", $selected="") {
	if ($id == "") {
		$id = $name;
	}
	$selected_0 = "";
	$selected_1 = "";
	switch ($selected) {
		case "Yes":
		case "1":
			$selected_1 = "selected='selected'";
			break;
		case "No":
		case "0":
			$selected_0 = "selected='selected'";
			break;
		case "Blank":
			$selected_blank = "selected='selected'";
	}

	if ($value == "1") {

		$yes = '1';
		$no = '0';
	} else {
		$yes = 'Yes';
		$no= 'No';
	}

	return "<select name='$name' id='$id' class='yes_no'>
		<option value=''>Any&nbsp;&nbsp;</option>
		<option value='$yes' $selected_1>Yes</option>
		<option value='$no' $selected_0>No</option>
		<option value='X', $selected_blank>Blank</option>
	 </select>";
}

function blankSelect($field_name) {
$field = $field_name . "Blank";
$output = "<select class='blank' name='$field' id='$field'>
	<option value=''>Any</option>
	<option value='Y'>Is Blank</option>
	<option value='X'>Is Not Blank</option>
</select>";
return $output;
}

function isDownloadSizeSelected($size) {
	if ($_GET["inc"] != $size) return;
	echo "selected=\"selected\"";
}

// FUNCTION getFormField
//
// returns the given field name from the posted fields
//
function getFormField($field_name) {
	$result = "";
	if (isset($_POST[$field_name])) {
		$result = $_POST[$field_name];
	}
	return $result;
}

function addQuery($query_string, $var_name, $variable, $type="=", $is_blank=false) {
	$add_to_string = "";
	if ($is_blank || ($variable != "" && $variable != "NULL")) {
		$db = Core_Database::getInstance();
		if ($type == "LIKE") {
			$variable = "%$variable%";
		}
		else {
			$variable = "$variable";
		}
		$variable = $db->quote($variable);
		switch ($is_blank) {
			case "Y":
				$type = "IS";
				$variable = "NULL";
				$add_query = " or $var_name LIKE ''";
				break;
			case "X":
				$type = "IS NOT";
				$variable = "NULL";
				$add_query = " and CHAR_LENGTH($var_name) <> 0";
				break;
		}
		$add_to_string = appendQuery($add_to_string, "($var_name $type $variable  $add_query)");

		/*
		switch ($is_blank) {
			case "Y":
				$add_to_string = $add_to_string . " or $var_name LIKE ''";
				break;
			case "X":
				$add_to_string = $add_to_string . " and $var_name NOT LIKE ''";
				break;
		}
		*/
	} elseif ($variable == "NULL") {
		$add_to_string = appendQuery($add_to_string, "$var_name $type NULL");
	}
	$query_string = appendQuery($query_string, $add_to_string);
	return $query_string;
}

function appendQuery($query_string, $new_query) {
	if ($new_query != "") {
		if ($query_string != "") {
			$query_string .= " and $new_query";
		} else {
			$query_string = $new_query;
		}
	}
	return $query_string;
}

function addQueryEqual($query_string, $var_name, $variable, $is_blank=false) {
	return addQuery($query_string, $var_name, $variable, "=", $is_blank);
}

function addQueryLike($query_string, $var_name, $variable, $is_blank=false) {
	return addQuery($query_string, $var_name, $variable, "LIKE", $is_blank);
}

function addQueryGreater($query_string, $var_name, $variable, $is_blank=false) {
	return addQuery($query_string, $var_name, $variable, ">", $is_blank);
}

function addQueryLess($query_string, $var_name, $variable, $is_blank=false) {
	return addQuery($query_string, $var_name, $variable, "<", $is_blank);
}

function addQueryGreaterEqual($query_string, $var_name, $variable, $is_blank=false) {
	return addQuery($query_string, $var_name, $variable, ">=", $is_blank);
}

function addQueryLessEqual($query_string, $var_name, $variable, $is_blank=false) {
	return addQuery($query_string, $var_name, $variable, "<=", $is_blank);
}

function addQuerySkills($query_string, $var_name, $rating, $yesno) {
	$add_query = "";
	if ($yesno == 'Z') {
		$add_query = "($var_name IS NULL or $var_name = 0) ";
	}
	else if ($yesno == '0') {
		$add_query = "($var_name IS NULL)";
	}
	else if ($yesno == 'X') {
		$add_query = "($var_name = 0)";
	}
	else if ($yesno == '1')
		$add_query = "($var_name >= 0)";
	if (!empty($rating))
		$add_query = appendQuery($add_query, "($var_name >= $rating)");
	return appendQuery($query_string, $add_query);
}
// formats an array into a string for use in a SQL
// IN (...) clause
//
// format_list(['1','2','3']) => "('1','2','3')"
//
function format_list($results_array) {
	for ($i=0; $i < sizeof($results_array); $i++) {
		$results_array[$i] = str_replace("NULL", "", $results_array[$i]);
		$results_array[$i] = str_replace("`", "", $results_array[$i]);
		$results_array[$i] = $results_array[$i];
		$results_array[$i] = "'" . $results_array[$i] . "'";
	}

	$list = implode(",", $results_array);
	return $list;
}


// FUNCTION queryTechs(fields, conditions, order)
//
// queries TR_Master_List for data

function queryTechs($fields, $conditions, $order="TechID ASC", $zipcode="", $distance="1") {
	if (false) {
		echo "<p>QUERY: caspioSelectAdv(\"TR_Master_List\", \"$fields\", \"$conditions\", \"$order\", ...)</p>";
		ob_flush();
		flush();
	}
	$db = Core_Database::getInstance();
	foreach ($fields as $k=>$v) {
		$v = trim($v, "'");
		if (!empty($v)) continue;
		$fields[$k] = new Zend_Db_Expr('NULL');
	}
	$select = $db->select();
	$select->from(Core_Database::TABLE_TECH_BANK_INFO, $fields)
		->order($order);

	if (!empty($conditions))
		$select->where($conditions);

		switch ($distance) {
		case "1":
			$result = $db->fetchAll($select, array(), Zend_Db::FETCH_NUM);
                    
			break;
		default:
			$result = proximityTechSearch($zipcode, $distance, $fields, $conditions, $order);
                    
	}
	return $result;
}

function queryTechsFromList($fields, $list, $order="TechID ASC") {
	$output = array();
	if (sizeof($list) != 0) {
		$list_string = format_list($list);
		$output = queryTechs($fields, "TechID in ($list_string)", $order);
	}
	return $output;
}


// function proximityTechSearch
//
// returns list of results that meet search criteria including
// proximity search
//
function proximityTechSearch($zipcode, $distance, $fields, $conditions, $order="TechID ASC") {
	$centerLatLong = geocodeAddress($zipcode);
	$db = Core_Database::getInstance();
	$select = $db->select();
	$select->from(Core_Database::TABLE_TECH_BANK_INFO, $fields);
	if (!empty($conditions)) $select->where($conditions);
	$select = Core_Database::whereProximity($select, $centerLatLong[0], $centerLatLong[1], "Latitude", "Longitude", $distance, "Distance");
	$result = $db->fetchAll($select, array(), Zend_Db::FETCH_NUM);
	return $result;
}

function getLatest($results_array) {

	$results_hash = array();
	$results_array = $results_array[0];

	foreach ($results_array as $entry) {

		$parts = $entry;

		$field_1 = process($parts[0]);
		$field_2 = process($parts[1]);

		// store only the first (most recent) login entry for each username

		//echo "<p>field_1: $field_1, field_2: $field_2</p>";
		if (!array_key_exists($field_1, $results_hash)) {
			$results_hash[$field_1] = $field_2;
		}
	}

	return $results_hash;
}

function trimQuote($str) {
	return trim($str, "`");
}

function addQuote($str) {
	return "'$str'";
}

function getClientDeniedResult($id, $main, $fls) {
		$output = "";
		if (array_key_exists($id, $main) && $main[$id] != "") {
			$output = $main[$id];
		}
		if (array_key_exists($id, $fls) && $fls[$id] == "Admin Denied") {
			$output = "FLS";
		}
		return $output;
}

function skillRating($label, $name="") {
  if ($name == "") {
  	$name = $label;
  }
	$nameSelfRating = $name . "SelfRating";
	$class_name = strtolower($name);
  return "<dt>$label</dt>
  <dd class='$class_name'>
    <select name='$name' id='$name' class='skill'>
      <option value='' selected='selected'>Any</option>
      <option value='1'>Yes</option>
      <option value='0'>No</option>
      <option value='X'>Yes (w/ blank rating)</option>
      <option value='Z'>Any blank rating</option>
    </select>
		Rating
		<select name='$nameSelfRating' id='$nameSelfRating' class='rating'>
			<option value=''>Any&nbsp;&nbsp;</option>
			<option value='1'>1</option>
			<option value='1'>2</option>
			<option value='1'>3</option>
			<option value='1'>4</option>
			<option value='1'>5</option>
		</select>
  </dd>";
}

function Loademccert($label, $name="")
{
    if ($name == "")
    {
        $name = $label;
    }
    
    $api = new Core_Api_TechClass();
    $emcitems = $api->getEMCList();
    $emcselect = "<option value=''>Any</option>";
    foreach ($emcitems as $item)
    {
        $emcselect .= "<option value='". $item['name']."' >". $item['label'] ."</option>";
    }
        
    return "<select name='$name' id='$name' style='width:155px;'>
                $emcselect
            </select>";
}

function excludeBasedOnDisplayOption($val, $maxIncludeLevel = 0) {
	global $DisplayOption;
	if ($DisplayOption > $maxIncludeLevel)
		$val = "''";
	return $val;
}

function checkBoxForm($label, $name="", $class_name = "") {
	if ($name == "") {
		$name = $label;
	}
	
	$select = yesNoSelectWithClass($name, $class_name);

	$result =  "<dt>$label</dt>
  <dd class='$class_name'>
    " . $select . "
  </dd>";

  return $result;
}

function checkBoxSkillsForm($label, $name="", $name2="", $class_name = "") {
	$select = yesNoSelectWithClass($name, "leftSkill");
	$select2 = yesNoSelectWithClass($name2, "rightSkill");
	$result =  "<dt>$label</dt>
  <dd class='$class_name'>
    $select
    $select2
  </dd>";

  return $result;
}

/**
 * @author Sergey Petkevich
 */
function skillRatingForm($label, $name="", $nameSelfRating="") {
  if ($name == "") {
  	$name = $label;
  }
	//$nameSelfRating = $name . "SelfRating";
	$class_name = strtolower($name);

	$result =  "<dt>$label</dt>
  <dd class='$class_name'>
    <select name='$name' id='$name' class='skill'>
      <option value='' selected='selected'>Any</option>
<option value='1'>Yes</option>
<option value='0'>No Rating Selected</option>
<option value='X'>Yes (with no experience)</option>
<option value='Z'>No Rating or None</option>
    </select>	
		<select name='$nameSelfRating' id='$nameSelfRating' class='rating'>
			<option value=''>Any&nbsp;&nbsp;</option>
			<option value='1'>1</option>
			<option value='2'>2</option>
			<option value='3'>3</option>
			<option value='4'>4</option>
			<option value='5'>5</option>
		</select>        		
  </dd>";

  return $result;
}
//13735
function skillRatingFSExpertForm($label, $name="", $nameSelfRating="",$nameFSExperts="") {
    if ($name == "") {
  	   $name = $label;
    }
	//$nameSelfRating = $name . "SelfRating";
	$class_name = strtolower($name);
    $FSExpertClass = new Core_Api_FSExpertClass();
    $isFSExpert = $FSExpertClass->isFSExpertEnable_TechSelfRatingColumn($nameSelfRating);
	$result =  "<dt>$label</dt>
  <dd class='$class_name'>
    <select name='$name' id='$name' class='skill'>
      <option value='' selected='selected'>Any</option>
<option value='1'>Yes</option>
<option value='0'>No Rating Selected</option>
<option value='X'>Yes (with no experience)</option>
<option value='Z'>No Rating or None</option>
    </select>	
		<select name='$nameSelfRating' id='$nameSelfRating' class='rating'>
			<option value=''>Any&nbsp;&nbsp;</option>
			<option value='1'>1</option>
			<option value='2'>2</option>
			<option value='3'>3</option>
			<option value='4'>4</option>
			<option value='5'>5</option>
		</select>";

    if($isFSExpert) {
        $result .= "<select name='$nameFSExperts' id='$nameFSExperts' class='rating'>
            <option value=''>Any&nbsp;</option>
            <option value='1'>Yes</option>
            <option value='0'>No</option>            
        </select>";    
    }
        
    $result .= "</dd>";
    return $result;
}

function addQuerySkillsFSExpert($query_string, $var_name, $expertFS) {
	$add_query = "";
	$techIDExpert = "''";
        $coreExpert = new Core_Api_FSExpertClass();
	if (!empty($expertFS))
	{
        $techIDExpert = $coreExpert->getTechIDArray_HasOrNotFSExpert($var_name,1);
		if(is_array($techIDExpert)){
			$techIDExpert = implode(",", $techIDExpert);
		}		
		if ($expertFS == '1') {						 
			$add_query = "TechID IN ($techIDExpert)";                    
		}
		else if ($expertFS == '0') {				
			$add_query = "TechID NOT IN ($techIDExpert)";                     
		}
	}	
	return appendQuery($query_string, $add_query);
}

//End 13735
function processSkill($skill, $yes="Y") {
	if ($skill == "X") {
		$skill = $yes;
	}
	if ($skill == "Z") {
		$skill = "";
	}
	return $skill;
}


function skillBlank($skill) {
	return ($skill == "X" || $skill == "Z");
}

function columnHeading($field_name, $link_name="") {
	$output_dir = "";
	$output = "";
	if ($link_name == "") {
		$link_name = str_replace(" ", "", $field_name);
	}
	$dir = "ASC";
	if (isset($_SESSION["sort"]) && $_SESSION["sort"] == $link_name) {
		if (isset($_SESSION["dir"]) && $_SESSION["dir"] == "ASC") {
			$dir = "DESC";
		}
		if ($dir == "ASC") {
			$output_dir = " <img src='/widgets/js/datatables/images/sort_desc.png' />";
		} else {
			$output_dir = " <img src='/widgets/js/datatables/images/sort_asc.png' />";
		}
	}
	//$output = "<nobr>";
	$output .= "<a href='?sort=$link_name&dir=$dir&t=".rand(444, 1000000)."' >$field_name</a>";
	$output .= " $output_dir";
	//$output .= "</nobr>";

	return $output;
}

function skillColumnHeading($skill, $yn = null, $selfRating = null) {
	$lower_skill = strtolower($skill);

	if (empty($yn)) {
        $yn = $lower_skill . "YN";
	}

	if (empty($selfRating)) {
        $selfRating = $lower_skill . "SelfRating";
	}

	$output = "<td>" . columnHeading($skill, $yn) . "</td>";
	$output = $output . "<td>" . columnHeading("$skill Rating", $selfRating) . "</td>";
	return $output;
}


function round_up($num) {
	if ($num != (int) $num) {
		$num = (int) $num + 1;
	}
	return (int) $num;
}


function rangeQueryMySql($clause, $less, $greater) {
	if (!empty($less))
		$less = date("Y-m-d", strtotime($less));
	if (!empty($greater))
		$greater = date("Y-m-d", strtotime($greater));
	return rangeQuery($clause, $less, $greater);
}

function rangeQuery($clause, $less, $greater) {
	$output = "";
	if ($less != "" || $greater != "") {

		if ($less != "" && $greater == "") {
			$output = "$clause <= '$less'";
		}
		elseif ($greater != "" && $less == "") {
			$output = "$clause >= '$greater'";
		}
		elseif ($less != "" && $greater != "") {
			$output = "$clause <= '$less' and $clause >= '$greater'";
		}
	}
  return $output;
}

function ZipCodeDistances() {
	$distanceOptions = Core_ZipRadius::getByCode();

	$distanceHtml = "<option value=\"1\">Select Distance</option>";
	foreach ($distanceOptions as $code => $name) {
		$selected = $code == "50" ? "selected=\"selected\"" : "";
		$distanceHtml .= "<option value=\"$code\" $selected>$name</option>";
	}
	
	return $distanceHtml;
}
//14035
function getReasonNameById($id) {
	
    $reasonName ='';
    $db = Core_Database::getInstance();
    $select = $db->select();
    $select->from(Core_Database::TABLE_TECH_DEACTIVATION_REASONS, array('id', 'Reason'));
    $select->where('id = (?)', $id);
    $records = Core_Database::fetchAll($select);
    if(!empty($records)) 
    {
        $reasonName=$records[0]['Reason'];
}
    return $reasonName;
}

function ZipCodeSelect($id = 'ProximityDistance', $name = '') {
	$name = ($name == "" ? $id : 'ProximityDistance');
	return "<select id='ProximityDistance' name='ProximityDistance' >" . ZipCodeDistances() . "</select>";
}

function displayOptionDropdown() {
?>
    	<select name="DisplayOption" id="DisplayOption">
    		<option value="0" <?=($_SESSION["DisplayOption"] == 0 ? "selected=\"selected\"" : "")?>>All</option>
    		<option value="1" <?=($_SESSION["DisplayOption"] == 1 ? "selected=\"selected\"" : "")?>>All except Last Activity Info</option>
    		<option value="2" <?=($_SESSION["DisplayOption"] == 2 ? "selected=\"selected\"" : "")?>>Basic Tech Info</option>
    		<option value="3" <?=($_SESSION["DisplayOption"] == 3 ? "selected=\"selected\"" : "")?>>Name / Email</option>
		</select>
<?php
}


// END FUNCTIONS
//===================================================================
$DisplayOption = 1;
if (!$_POST) {
	$_SESSION["DisplayOption"] = 1;
	$common = new Core_Api_CommonClass;
	$countryOptions = $common->getCountries();
		
	$countriesHtml = "<option value=\"\">Show All</option>";
        //14041 
	foreach ($countryOptions->data as $country) {
            if($country->Code=="")
            {
            $countriesHtml .= "<option value=\"$country->Code\" disabled='disabled'>$country->Name</option>";  
            }

            else{
		$countriesHtml .= "<option value=\"$country->Code\">$country->Name</option>";
            }   //end 14041
	}
	$isoOptions = $common->getISOsArray();
	$isos = $isoOptions->data;
}
//start search
if ($_POST) {
// SEARCH FIELD VARIABLES

$DisplayOption = getFormField("DisplayOption");
$_SESSION["DisplayOption"] = $DisplayOption;

$TechID = getFormField("TechID");
$LastName = getFormField("LastName");
$FirstName = getFormField("FirstName");
$Username = getFormField("Username");
$Password = getFormField("Password");
$PrimaryPhone = getFormField("PrimaryPhone");
$SecondaryPhone = getFormField("SecondaryPhone");
$SMS_Number = getFormField("SMS_Number");
$isWMTech = getFormField("isWMTech");
$WMID = getFormField("WMID");
$DrugPassed = getFormField("DrugPassed");
if ($DrugPassed == "0") {
	$DrugPassed = "NULL";
}
/*13700*/
$SilverDrugPassed=getFormField("SilverDrugPassed");
if ($SilverDrugPassed == "0") {
	$SilverDrugPassed = "NULL";
}

$Email = getFormField("Email");
$EmailType = getFormField("EmailType");
$City = getFormField("City");
$State = getFormField("State");
$Country = getFormField("Country");
$ZipCode = getFormField("ZipCode");
$ZipCodeDistance = getFormField("ProximityDistance");

$W9 = getFormField("W9");
$W9_Date_RecGreater = getFormField("W9_Date_RecGreater");
$W9_Date_RecLess = getFormField("W9_Date_RecLess");
$Push2Tech = getFormField("Push2Tech");

$Qty_IMAC_CallsGreater = getFormField("Qty_IMAC_CallsGreater");
$Qty_IMAC_CallsLess = getFormField("Qty_IMAC_CallsLess");
$Qty_FLS_Service_CallsGreater = getFormField("Qty_FLS_Service_CallsGreater");
$Qty_FLS_Service_CallsLess = getFormField("Qty_FLS_Service_CallsLess");
$No_ShowsGreater = getFormField("No_ShowsGreater");
$No_ShowsLess = getFormField("No_ShowsLess");
$Back_OutsGreater = getFormField("Back_OutsGreater");
$Back_OutsLess = getFormField("Back_OutsLess");
$Pay_Company = getFormField("Pay_Company");
$ISO_Affiliation_ID_M = getFormField("ISO_Affiliation_ID_M");
$ISO_Affiliation_ID = getFormField("ISO_Affiliation_ID");
$ISO_Tech_Only = getFormField("ISO_Tech_Only");
$ISO_Affiliation = getFormField("ISO_Affiliation");
$SSA = getFormField("SSA");
$Secret = getFormField("Secret");
$Top_Secret = getFormField("Top_Secret");
$NACI = getFormField("NACI");//13932
$NACIGreater = getFormField("NACIGreater");
$NACILess = getFormField("NACILess");//13932
$General1 = getFormField("General1");
$General2 = getFormField("General2");
$General3 = getFormField("General3");
$General4 = getFormField("General4");
$General5 = getFormField("General5");
$General6 = getFormField("General6");
$MRA_Compliant = getFormField("MRA_Compliant");
$CDM = getFormField("CDM");
$L1 = getFormField("L1");
$L2 = getFormField("L2");
$L3 = getFormField("L3");
$L4 = getFormField("L4");
$DELL_DCSE_Reg = getFormField("DELL_DCSE_Reg");

$DCSE_Soft_Skills = getFormField("DCSE_Soft_Skills");
$DCSE_Lead = getFormField("DCSE_Lead");
$VA = getFormField("VA");

$SMS_AgreeDateGreater = getFormField("SMS_AgreeDateGreater");
    $SMS_AgreeDateLess = getFormField("SMS_AgreeDateLess");
$AcceptTerms = getFormField("AcceptTerms");
$RegDateGreater = getFormField("RegDateGreater");
$RegDateLess = getFormField("RegDateLess");
$TechSource = getFormField("TechSource");
$ClientDenied = getFormField("ClientDenied");
$HideBids = getFormField("HideBids");//14018

// add SMS/Email opt out fields
$UnsubscribeWorkEmail = getFormField("UnsubscribeAvailWorkEmail");
$unsubscribeWorkSMS = getFormField("UnsubscribeAvailWorkSMS");

$Deactivated = getFormField("Deactivated");
$DeactivatedReason = getFormField("DeactivatedReason");
$FLSID = getFormField("FLSID");
$FLSstatus = getFormField("FLSstatus");
$FLSCSP_Rec = getFormField("FLSCSP_Rec");
$Bg_Test_Req_Lite = getFormField("Bg_Test_Req_Lite");
$Bg_Test_Pass_Lite = getFormField("Bg_Test_Pass_Lite");
if ($Bg_Test_Pass_Lite == "0") {
	$Bg_Test_Pass_Lite = "NULL";
}
$Bg_Test_Pass_Full = getFormField("Bg_Test_Pass_Full");
if ($Bg_Test_Pass_Full == "0") {
	$Bg_Test_Pass_Full = "NULL";
}
    //$TechPreferred = getFormField("TechPreferred");
$Private = getFormField("Private");
$W2 = getFormField("W2");
$Dispatch = getFormField("Dispatch");
$ISO_Affiliation = getFormField("ISO_Affiliation");

$FSCert = getFormField("FSCert");
$FSCertLess = getFormField("FSCertLess");
$FSCertGreater = getFormField("FSCertGreater");
$BgPassGreater = getFormField("BgPassGreater");
$BgPassLess = getFormField("BgPassLess");
$BgPassFullGreater = getFormField("BgPassFullGreater");
$BgPassFullLess = getFormField("BgPassFullLess");
$BgReqGreater = getFormField("BgReqGreater");
$BgReqLess = getFormField("BgReqLess");
//$LoggedOnDateGreater = getFormField("LoggedOnDateGreater");
//$LoggedOnDateLess = getFormField("LoggedOnDateLess");
$FSMobile = getFormField("FSMobile");
$FSMobileGreater = getFormField("FSMobileGreater");
$FSMobileLess = getFormField("FSMobileLess");
$LastWOBidType = getFormField("LastWOBidType");
$LastWOBidGreater = getFormField("LastWOBidGreater");
$LastWOBidLess = getFormField("LastWOBidLess");
$LastWORunType = getFormField("LastWORunType");
$LastWORunGreater = getFormField("LastWORunGreater");
$LastWORunLess = getFormField("LastWORunLess");
$AcceptTermsGreater = getFormField("AcceptTermsGreater");
$AcceptTermsLess = getFormField("AcceptTermsLess");
$FLSCSPGreater = getFormField("FLSCSP_RecDateGreater");
$FLSCSPLess = getFormField("FLSCSP_RecDateLess");
//13932
$InGovClearance = getFormField("InGovClearance");
$InGovClearanceGreater = getFormField("InGovClearanceGreater");
$InGovClearanceLess = getFormField("InGovClearanceLess");
//13932
//issue_13085
$FSPlusOneReg = getFormField("FSPlusOneReg");
$FSPlusOneDateRegGreater = getFormField("FSPlusOneDateRegGreater");
$FSPlusOneDateRegLess = getFormField("FSPlusOneDateRegLess");
//end issue_13085
$EMCcertifications = getFormField("EMCcertifications");
$EMCcertificationsGreater = getFormField("EMCcertificationsGreater");
$EMCcertificationsLess = getFormField("EMCcertificationsLess");

$BgDrugPassedGreater = getFormField("BgDrugPassedGreater");
$BgDrugPassedLess = getFormField("BgDrugPassedLess");

/*13700*/
$BgSilverDrugPassedGreater = getFormField("BgSilverDrugPassedGreater");
$BgSilverDrugPassedLess = getFormField("BgSilverDrugPassedLess");
/*end 13700*/

$Skills = getFormField("Skills");
$SkillsBlank = getFormField("SkillsBlank");
$DellCert = getFormField("DellCert");

$EMC = getFormField("EMCCert");
$EMCGreater = getFormField("EMCCert"."Greater");
$EMCLess = getFormField("EMCCert"."Less");
        
$MCSE = getFormField("MCSE");
$CCNA = getFormField("CCNA");
$APlus = getFormField("APlus");
$CompTIA = getFormField("CompTIA");
$CompTIABlank = getFormField("CompTIABlank");
$Certifications = getFormField("Certifications");
$CertificationsBlank = getFormField("CertificationsBlank");
$SpecificExperience = getFormField("SpecificExperience");
$ExperienceBlank = getFormField("ExperienceBlank");
$Resume = getFormField("Resume");

$AlarmSystems = getFormField("AlarmSystems");//13978
$Electronics = getFormField("Electronics");
$AdjustableBedRepair = getFormField("AdjustableBedRepair");
$Apple = getFormField("Apple");
$ATM = getFormField("atm");
$Cabling = getFormField("Cabling");
$CCTV = getFormField("CCTV");
$CentralOfficeCablingSelfRating = getFormField("CentralOfficeCablingSelfRating");
$Construction = getFormField("Construction");
$Copiers = getFormField("Copiers");
$Desktop = getFormField("Desktop");
$DigitalSignage = getFormField("DigitalSignage");
$Dsl = getFormField("Dsl");
$Electrical = getFormField("Electrical");
$ElectricianCertified = getFormField("ElectricianCertified");//14121
$electroMech = getFormField("electroMech");
$FiberCabling = getFormField("FiberCabling");
$FlatPanelTV = getFormField("FlatPanelTV");
$GeneralWiring = getFormField("GeneralWiring");
$Networking = getFormField("Networking");
$Kiosk = getFormField("Kiosk");
$LowVoltage = getFormField("LowVoltage");
$MobileDevices = getFormField("MobileDevices");
$Scanners = getFormField("Scanners");//13735
$POS = getFormField("pos");
$Printers = getFormField("Printers");
$RFID = getFormField("RFID");
$Routers = getFormField("Routers");
$savesSecCabinets = getFormField("savesSecCabinets");
$Satellite = getFormField("Satellite");
$Servers = getFormField("Servers");
$ServerSoftware = getFormField("ServerSoftware");
$SiteSurvey = getFormField("SiteSurvey");
$Telephony = getFormField("Telephony");
$TelephonyVoIP = getFormField("TelephonyVoIP");
$Wireless = getFormField("Wireless");
$Residential = getFormField("Residential");
$Commercial = getFormField("Commercial");
$Government = getFormField("Government");
$EnterpriseStorage = getFormField("EnterpriseStorage");
$ElectronicsSelfRating = getFormField("electronicsSelfRating");
$AlarmSystemsSelfRating = getFormField("AlarmSystemsSelfRating");//13978 
$AdjustableBedRepairSelfRating = getFormField("AdjustableBedRepairSelfRating");
$appleSelfRating = getFormField("appleSelfRating");
$ATMSelfRating = getFormField("atmSelfRating");
$CablingSelfRating = getFormField("cablingSelfRating");
$CCTVSelfRating = getFormField("CCTVSelfRating");
$CentralOfficeCabling = getFormField("CentralOfficeCabling");
$ConstructionSelfRating = getFormField("constructionSelfRating");
$CopiersSelfRating = getFormField("CopiersSelfRating");
$DesktopSelfRating = getFormField("desktopSelfRating");
$DigitalSignageSelfRating = getFormField("DigitalSignageSelfRating");
$DslSelfRating = getFormField("DslSelfRating");
$ElectricalSelfRating = getFormField("electricalSelfRating");
$ElectricianCertifiedSelfRating = getFormField("ElectricianCertifiedSelfRating");//14121
$electroMechSelfRating = getFormField("electroMechSelfRating");
$FiberCablingSelfRating = getFormField("FiberCablingSelfRating");
$FlatPanelTVSelfRating = getFormField("FlatPanelTVSelfRating");
$GeneralWiringSelfRating = getFormField("GeneralWiringSelfRating");
$NetworkingSelfRating = getFormField("networkingSelfRating");
$KioskSelfRating = getFormField("KioskSelfRating");
$LowVoltageSelfRating = getFormField("LowVoltageSelfRating");
$MobileDevicesSelfRating = getFormField("MobileDevicesSelfRating");
$ScannersSelfRating = getFormField("ScannersSelfRating");//13735
$POSSelfRating = getFormField("posSelfRating");
$PrintersSelfRating = getFormField("PrintersSelfRating");
$RFIDSelfRating = getFormField("RFIDSelfRating");
$RoutersSelfRating = getFormField("routersSelfRating");
$savesSecCabinetsSelfRating = getFormField("savesSecCabinetsSelfRating");
$SatelliteSelfRating = getFormField("SatelliteSelfRating");
$ServersSelfRating = getFormField("ServersSelfRating");
$ServerSoftwareSelfRating = getFormField("ServerSoftwareSelfRating");
$SiteSurveySelfRating = getFormField("SiteSurveySelfRating");
$TelephonySelfRating = getFormField("TelephonySelfRating");
$TelephonyVoIPSelfRating = getFormField("telephonyVoIPSelfRating");
$WirelessSelfRating = getFormField("WirelessSelfRating");
$ResidentialSelfRating = getFormField("ResidentialSelfRating");
$CommercialSelfRating = getFormField("CommercialSelfRating");
$GovernmentSelfRating = getFormField("GovernmentSelfRating");
$EnterpriseStorageSelfRating = getFormField("EnterpriseStorageSelfRating");
$preference = getFormField("PreferencePercent");
$preference = rtrim($preference, "%");
$prefRatings = getFormField("SATRecommendedTotal");
$Performance = getFormField("PerformancePercent");
$Performance = rtrim($Performance, "%");
$perfRatings = getFormField("SATPerformanceTotal");
$hp_Cert = getFormField("HP_CertNum");
$samsungCert = getFormField("SamsungCert");
$techSourceFilter = getFormField("TechSourceFilter"); //13702
//13735
$electronicsFSExpert = getFormField("electronicsFSExpert");
$AlarmSystemsFSExpert = getFormField("AlarmSystemsFSExpert"); //13978
$AdjustableBedRepairFSExpert = getFormField("AdjustableBedRepairFSExpert");
$appleFSExpert = getFormField("appleFSExpert");
$atmFSExpert = getFormField("atmFSExpert");
$cablingFSExpert = getFormField("cablingFSExpert");


$CCTVFSExpert = getFormField("CCTVFSExpert");
$CentralOfficeCablingFSExpert = getFormField("CentralOfficeCablingFSExpert");
$constructionFSExpert = getFormField("constructionFSExpert");
$CopiersFSExpert = getFormField("CopiersFSExpert");
$desktopFSExpert = getFormField("desktopFSExpert");

$DigitalSignageFSExpert = getFormField("DigitalSignageFSExpert");
$DslFSExpert = getFormField("DslFSExpert");
$electricalFSExpert = getFormField("electricalFSExpert");
$ElectricianCertifiedFSExpert = getFormField("ElectricianCertifiedFSExpert"); //14121
$electroMechFSExpert = getFormField("electroMechFSExpert");
$EnterpriseStorageFSExpert = getFormField("EnterpriseStorageFSExpert");

$FiberCablingFSExpert = getFormField("FiberCablingFSExpert");
$FlatPanelTVFSExpert = getFormField("FlatPanelTVFSExpert");
$GeneralWiringFSExpert = getFormField("GeneralWiringFSExpert");
$networkingFSExpert = getFormField("networkingFSExpert");
$KioskFSExpert = getFormField("KioskFSExpert");
//
$LowVoltageFSExpert = getFormField("LowVoltageFSExpert");
$MobileDevicesFSExpert = getFormField("MobileDevicesFSExpert");
$ScannersFSExpert = getFormField("ScannersFSExpert");//13735
$posFSExpert = getFormField("posFSExpert");
$PrintersFSExpert = getFormField("PrintersFSExpert");
$RFIDFSExpert = getFormField("RFIDFSExpert");

$routersFSExpert = getFormField("routersFSExpert");
$savesSecCabinetsFSExpert = getFormField("savesSecCabinetsFSExpert");
$SatelliteFSExpert = getFormField("SatelliteFSExpert");
$ServersFSExpert = getFormField("ServersFSExpert");
$ServerSoftwareFSExpert = getFormField("ServerSoftwareFSExpert");

$SiteSurveyFSExpert = getFormField("SiteSurveyFSExpert");
$telephonyFSExpert = getFormField("telephonyFSExpert");
$TelephonyVoIPFSExpert = getFormField("TelephonyVoIPFSExpert");
$WirelessFSExpert = getFormField("WirelessFSExpert");
//13932
$InGovClearance = getFormField("InGovClearance");
$InGovClearanceGreater = getFormField("InGovClearanceGreater");
$InGovClearanceLess = getFormField("InGovClearanceLess");
$FullGovClearance = getFormField("FullGovClearance");
$FullGovClearancGreater = getFormField("FullGovClearanceGreater");
$FullGovClearanceLess = getFormField("FullGovClearanceLess");
$TopGovClearance = getFormField("TopGovClearance"); 

$TopGovClearanceGreater = getFormField("TopGovClearanceGreater");
$TopGovClearanceLess = getFormField("TopGovClearanceLess");
$TopSecretSCIGovClearance = getFormField("TopSecretSCIGovClearance");//14056   
$TopSecretSCIGovClearanceGreater = getFormField("TopSecretSCIGovClearanceGreater");//14056
$TopSecretSCIGovClearanceLess = getFormField("TopSecretSCIGovClearanceLess"); //14056
//13932

//030
$ProfilePicture = getFormField("ProfilePicture");

if(($ProfilePicture!=""))
{
    $classtech= new Core_Tech();
    $searchTechByPhotos = $classtech->searchTechByPhotos($ProfilePicture);

    if($searchTechByPhotos !="")
    {
        $searchTechByPhotosquery = " TechID in (".$searchTechByPhotos.") ";
    }
    else
    {
        $searchTechByPhotosquery = " 1=0 ";
    }
}
//end 030
$techEquipmentField = array(27, 20, 19, 21, 22, 5, 6, 7, 8, 9, 10, 11, 24, 16, 17, 18, 26, 29, 1, 23, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42);
$techEquipment = array();
$techEquipmentNo = array();
foreach ($techEquipmentField as $id) {
	$f = getFormField("equipment_$id");
	if (!empty($f))
		$techEquipment[] = $id;
	else if ($f == '0')
		$techEquipmentNo[] = $id;
}

//--- US Authorized
$USAuthorizedValue = getFormField("USAuthorized");
$USAuthorizedDateGreater = getFormField("USAuthorizedDateGreater"); 
$USAuthorizedDateLess = getFormField("USAuthorizedDateLess"); 
//echo("<br/>USAuthorizedDateGreater: $USAuthorizedDateGreater");//test
//echo("<br/>USAuthorizedDateLess: $USAuthorizedDateLess");//test
//--- CopierSkills
$CopierSkillsValue = getFormField("CopierSkills");
$CopierSkillsGreater = getFormField("CopierSkillsGreater"); 
$CopierSkillsLess = getFormField("CopierSkillsLess"); 
//--- CopierSkills: end

$search = new Core_TechEquipment;
$withEquipment = $search->getTechsWithEquipment($techEquipment);
$withEquipmentNo = $search->getTechsWithEquipment($techEquipmentNo);

$techSkillsField = array(2, 3, 4, 6, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57);
$techSkills = array();
$techSkillsNo = array();
foreach ($techSkillsField as $id) {
	$f = getFormField("skills_$id");
	if (!empty($f))
		$techSkills[] = $id;
	else if ($f == '0')
		$techSkillsNo[] = $id;
}

$search = new Core_TechSkills;
$withSkills = $search->getTechsWithSkills($techSkills);
$withSkillsNo = $search->getTechsWithSkills($techSkillsNo);
$techCertField = array(2, 3, 4, 5, 18, 6, 19, 20, 21, 22, 23, 24,25,26,31,32,36,37,38,44);
$techCert = array();
$techCertNo = array();
$techCertDateGreater = array();
$techCertDateLess = array();
$withCPTestFail = array();

foreach ($techCertField as $id) {
	$f = getFormField("certifications_$id");
	$d1 = getFormField("certifications_$id"."Greater");
	$d2 = getFormField("certifications_$id"."Less");

	if (!empty($f) && ($id != 24 || $f != '-1')) {
		$techCert[] = $id;
		if (!empty($d1))
        {
			$techCertDateGreater["$id"] = $d1;
        }
		if (!empty($d2))
        {
			$techCertDateLess["$id"] = $d2;
		}
	}
	else if ($f == '0')
    {
		$techCertNo[] = $id;
	}
    else if($id == 24 && $f == '-1')
    {
        $withCPTestFail = Core_TechCertifications::getTechIdsArray_ByComputersPlusTestFail();
    }
}

$search = new Core_TechCertifications;
$withCert = $search->getTechsWithCertification($techCert, $techCertDateGreater, $techCertDateLess);
$withCertNo = $search->getTechsWithCertification($techCertNo);

//--- 367
if(empty($withCert) && !empty($techCert))
{
    $withCert[]= 0; //force no results to return;
}

if(empty($withCert) && !empty($techCert))
{
    $withCert[]= 0; //force no results to return;
}

// BASIC QUERY

$query = "";

$db = Core_Database::getInstance();
if (sizeof($withEquipment) > 0 && sizeof($withSkills) > 0)
	$combined = array_intersect($withSkills, $withEquipment);
else
	$combined = array_merge($withSkills, $withEquipment);
	
if (sizeof($combined) > 0 && sizeof($withCert) > 0)
	$combined = array_intersect($combined, $withCert);
else
	$combined = array_merge($combined, $withCert); 

if (sizeof($combined) > 0 && sizeof($withCPTestFail) > 0)
    $combined = array_intersect($combined, $withCPTestFail);
else if (sizeof($withCPTestFail) > 0)
    $combined = $withCPTestFail;


if (!empty($combined)) {
	$query = appendQuery($query, $db->quoteInto("TechID IN (?)", $combined));
}
else if (!empty($techEquipment)
        || !empty($techEquipmentNo)
        || !empty($techSkills)
        || !empty($techSkillsNo)
        || !empty($techCertNo)
        || !empty($techCert)){
	$query = appendQuery($query, "TechID IN (0) AND TechID NOT IN (0)");
}
	
unset($withSkills);
unset($withEquipment);
unset($withCert);
unset($withCPTestFail);
unset($combined);

$combined = array_merge($withSkillsNo, $withEquipmentNo, $withCertNo);
if (!empty($combined))
	$query = appendQuery($query, $db->quoteInto("TechID NOT IN (?)", $combined));
unset($withSkillsNo);
unset($withEquipmentNo);
unset($withCertNo);
unset($combined);

$GGETechEvaluation = getFormField("GGETechEvaluation");
$GGETechEvaluationGreater = getFormField("GGETechEvaluationGreater");
$GGETechEvaluationLess = getFormField("GGETechEvaluationLess");
$Core_Api_TechClass = new Core_Api_TechClass;
if(!empty($GGETechEvaluation))
{    
    $GGETechEvaluationGreater = !empty($GGETechEvaluationGreater)?date_format(new DateTime($GGETechEvaluationGreater), "Y-m-d"):"";
    $GGETechEvaluationLess = !empty($GGETechEvaluationLess)?date_format(new DateTime($GGETechEvaluationLess), "Y-m-d"):"";
    $techIDGGETechEvaluation = $Core_Api_TechClass->getTechIdsArray_ByGGEStatus(
            $GGETechEvaluation,
            $GGETechEvaluationGreater,
            $GGETechEvaluationLess);
    $query = appendQuery($query, "TechID IN ($techIDGGETechEvaluation)");
}
// 13761
$BlackBoxCabling = getFormField("BlackBoxCabling");
$BlackBoxCablingGreater = getFormField("BlackBoxCablingGreater");
$BlackBoxCablingLess = getFormField("BlackBoxCablingLess");
$Core_Api_TechClass = new Core_Api_TechClass;
if(!empty($BlackBoxCabling))
{    
    $BlackBoxCablingGreater = !empty($BlackBoxCablingGreater)?date_format(new DateTime($BlackBoxCablingGreater), "Y-m-d"):"";
    $BlackBoxCablingLess = !empty($BlackBoxCablingLess)?date_format(new DateTime($BlackBoxCablingLess), "Y-m-d"):"";
    $techIDBlackBoxCabling = $Core_Api_TechClass->getTechIdsArray_ByBBCablingStatus(
            $BlackBoxCabling,
            $BlackBoxCablingGreater,
            $BlackBoxCablingLess);
    if(strtoupper($BlackBoxCabling) == "NO")
    {
        $query = appendQuery($query, "TechID NOT IN ($techIDBlackBoxCabling)");        
    }else
    {
        $query = appendQuery($query, "TechID IN ($techIDBlackBoxCabling)");
    }
}

$BlackBoxTelecom = getFormField("BlackBoxTelecom");
$BlackBoxTelecomGreater = getFormField("BlackBoxTelecomGreater");
$BlackBoxTelecomLess = getFormField("BlackBoxTelecomLess");
$Core_Api_TechClass = new Core_Api_TechClass;
if(!empty($BlackBoxTelecom))
{    
    $BlackBoxTelecomGreater = !empty($BlackBoxTelecomGreater)?date_format(new DateTime($BlackBoxTelecomGreater), "Y-m-d"):"";
    $BlackBoxTelecomLess = !empty($BlackBoxTelecomLess)?date_format(new DateTime($BlackBoxTelecomLess), "Y-m-d"):"";
    $techIDBlackBoxTelecom = $Core_Api_TechClass->getTechIdsArray_ByBBTelecomStatus(
            $BlackBoxTelecom,
            $BlackBoxTelecomGreater,
            $BlackBoxTelecomLess);
    if(strtoupper($BlackBoxTelecom) == "NO")
    {
        $query = appendQuery($query, "TechID NOT IN ($techIDBlackBoxTelecom)");        
    }else
    {
        $query = appendQuery($query, "TechID IN ($techIDBlackBoxTelecom)");
    }
}
$LanguageSkills = getFormField("LanguageSkills");
$LanguageSkillsRating = getFormField("LanguageSkillsRating");
if(!empty($LanguageSkills)){
    $Core_TechLanguages = new Core_TechLanguages();
    $techids = $Core_TechLanguages->findTechIDs_ByLangAndRating($LanguageSkills,$LanguageSkillsRating);
    if(is_array($techids)){
       $techids = implode(",", $techids);
    }
    else{
        $techids = "''";
    }
    $query = appendQuery($query, "TechID IN ($techids)");
}

$PurpleTechnicianGreater = getFormField("PurpleTechnicianGreater");
$PurpleTechnicianLess = getFormField("PurpleTechnicianLess");
$PurpleTechnician = getFormField("PurpleTechnician");
$_search = new Core_TechCertifications;
$_combined = "";
if($PurpleTechnician == "Yes"){
    $withCert_39 = $_search->getTechsWithCertification(array(39), $PurpleTechnicianGreater, $PurpleTechnicianLess);
    $withCert_40 = $_search->getTechsWithCertification(array(40), $PurpleTechnicianGreater, $PurpleTechnicianLess);
    $_combined = array_merge($withCert_39, $withCert_40);   
}else if($PurpleTechnician == "No"){
    $withCert_39 = $_search->getTechsWithCertification(array(39));
    $withCert_40 = $_search->getTechsWithCertification(array(40));
    $_combined = array_merge($withCert_39, $withCert_40); 
}else if($PurpleTechnician == ""){
    // nothing
}else if($PurpleTechnician == "Certified"){
    $_combined = $_search->getTechsWithCertification(array(40), $PurpleTechnicianGreater, $PurpleTechnicianLess);
}else if($PurpleTechnician == "Unverified"){
    $_combined = $_search->getTechsWithCertification(array(39), $PurpleTechnicianGreater, $PurpleTechnicianLess);
}
if (!empty($_combined) && $PurpleTechnician != "No") {
    $query = appendQuery($query, $db->quoteInto("TechID IN (?)", $_combined));
}else   if (!empty($_combined) && $PurpleTechnician == "No") {
    $query = appendQuery($query, $db->quoteInto("TechID NOT IN (?)", $_combined));
}

$KeyCertifiedContractorGreater = getFormField("KeyCertifiedContractorGreater");
$KeyCertifiedContractorLess = getFormField("KeyCertifiedContractorLess");
$KeyCertifiedContractor = getFormField("KeyCertifiedContractor");
$_search = new Core_TechCertifications;
$_combined = "";
if($KeyCertifiedContractor == "Yes"){
    $withCert_39 = $_search->getTechsWithCertification(array(41), $KeyCertifiedContractorGreater, $KeyCertifiedContractorLess);
    $withCert_40 = $_search->getTechsWithCertification(array(42), $KeyCertifiedContractorGreater, $KeyCertifiedContractorLess);
    $_combined = array_merge($withCert_39, $withCert_40);   
}else if($KeyCertifiedContractor == "No"){
    $withCert_39 = $_search->getTechsWithCertification(array(41));
    $withCert_40 = $_search->getTechsWithCertification(array(42));
    $_combined = array_merge($withCert_39, $withCert_40); 
}else if($KeyCertifiedContractor == ""){
    // nothing
}else if($KeyCertifiedContractor == "KeyContractor"){
    $_combined = $_search->getTechsWithCertification(array(41), $KeyCertifiedContractorGreater, $KeyCertifiedContractorLess);
}else if($KeyCertifiedContractor == "CertifiedTech"){
    $_combined = $_search->getTechsWithCertification(array(42), $KeyCertifiedContractorGreater, $KeyCertifiedContractorLess);
}elseif($KeyCertifiedContractor == "BothTags"){
    $_combined = $_search->getTechsWithCertification(array(41,42), $KeyCertifiedContractorGreater, $KeyCertifiedContractorLess);
}

if (!empty($_combined) && $KeyCertifiedContractor != "No") {
    $query = appendQuery($query, $db->quoteInto("TechID IN (?)", $_combined));
}else   if (!empty($_combined) && $KeyCertifiedContractor == "No") {
    $query = appendQuery($query, $db->quoteInto("TechID NOT IN (?)", $_combined));
}else if($KeyCertifiedContractor != ""){
    $query = appendQuery($query, "1=0");
}
$_apitech = new Core_Api_TechClass;
$bootcamp = getFormField("BootCamp");
$bootcampDateRegGreater = getFormField("BootCampcertificationsGreater");
$bootcampDateRegLess = getFormField("BootCampcertificationsLess");
$_bootcampcombined = "";
if($bootcamp =='1')
{
    $withCert_3 = $_apitech->getTechIdsArray_ByCertiId(3, $bootcampDateRegGreater, $bootcampDateRegLess);
    $withCert_43 = $_apitech->getTechIdsArray_ByCertiId(43, $bootcampDateRegGreater, $bootcampDateRegLess);
    $_bootcampcombined = array_merge($withCert_3, $withCert_43);
}
else if($bootcamp =='0')
{
    $withCert_3 = $_apitech->getTechIdsArray_ByCertiId(3);
    $withCert_43 = $_apitech->getTechIdsArray_ByCertiId(43);
    $_bootcampcombined = array_merge($withCert_3, $withCert_43);
}
else if($bootcamp =='3')
{
    $withCert_3 = $_apitech->getTechIdsArray_ByCertiId(3, $bootcampDateRegGreater, $bootcampDateRegLess);
    $_bootcampcombined = array_merge($withCert_3);
}
else if($bootcamp =='43')
{
    $withCert_43 = $_apitech->getTechIdsArray_ByCertiId(43, $bootcampDateRegGreater, $bootcampDateRegLess);
    $_bootcampcombined = array_merge($withCert_43);
}

if (empty($_bootcampcombined) && $bootcamp != "")
{
    $query = appendQuery($query, $db->quoteInto("TechID 1=0"));
}
else if (!empty($_bootcampcombined) && $bootcamp != "0")
{
    $query = appendQuery($query, $db->quoteInto("TechID IN (?)", $_bootcampcombined));
}
else if (!empty($_bootcampcombined) && $bootcamp == "0")
{
    $query = appendQuery($query, $db->quoteInto("TechID NOT IN (?)", $_bootcampcombined));
}
$Core_TechCertifications = new Core_TechCertifications();
$CompuComCertified = getFormField("CompuComCertified");
$CompuComCertifiedGreater = getFormField("CompuComCertifiedGreater");
$CompuComCertifiedLess = getFormField("CompuComCertifiedLess");
//13840
if($CompuComCertified != ""){
    if($CompuComCertified == "N"){
        $techCompuComCertified = $Core_TechCertifications->getTechIDArray_ByCompuComCertified(
            "Y",
            $CompuComCertifiedGreater,
            $CompuComCertifiedLess);
       if(!empty($techCompuComCertified) && count($techCompuComCertified) > 0) {
           $techCompuComCertified = implode(",", $techCompuComCertified);
           $query = appendQuery($query, "TechID NOT IN ($techCompuComCertified)");
       }
    } else {
        $techCompuComCertified = $Core_TechCertifications->getTechIDArray_ByCompuComCertified(
                $CompuComCertified,
                $CompuComCertifiedGreater,
                $CompuComCertifiedLess);
        if(!empty($techCompuComCertified) && count($techCompuComCertified) > 0){
            $techCompuComCertified = implode(",", $techCompuComCertified);
            $query = appendQuery($query, "TechID IN ($techCompuComCertified)");
        }else if(isset($techCompuComCertified) && count($techCompuComCertified) == 0){
            $query = appendQuery($query, "1 = 0");
        }        
    }
}
//end 13840
// search Tags
$tags = $_REQUEST["tags"];
$arrayTags = array();
    if (!empty($tags))
    {
        foreach ($tags as $tag)
        {
            $any_yes_no = getFormField("tagsitem_" . $tag);
            $greater_date = getFormField("tagsDate_" . $tag . "Greater");
            $less_date = getFormField("tagsDate_" . $tag . "Less");
            if (trim($any_yes_no) != "")
            {
                $arrayTags[] = array('fstag_id' => $tag, 'yes_no' => $any_yes_no, 'greater_date' => $greater_date, 'less_date' => $less_date);
        }
    }
}
//624
//build array tag params
$arrayTagsexists = array();
    foreach ($arrayTags as $tag)
{
        if (!empty($tag['yes_no']) || !empty($tag['greater_date']) || !empty($tag['less_date']))
    {
            $arrayTagsexists[] = array('fstag_id' => $tag['fstag_id'], 'yes_no' => $tag['yes_no'], 'greater_date' => $tag['greater_date'], 'less_date' => $tag['less_date']);
    }
}
$_SESSION['SearchClientCredentials']= $arrayTagsexists;

function buildparamhiden($arrayTagsexists) 
{
        $paramhiden = "";
        foreach ($arrayTagsexists as $Tagsexists)
    {
            $paramhiden.="<input type='hidden' value='" . $Tagsexists['fstag_id'] . "' name='fstag_id[]'>
                      <input type='hidden' value='" . $Tagsexists['yes_no'] . "' name='yes_no[]'>
                      <input type='hidden' value='" . $Tagsexists['greater_date'] . "' name='greater_date[]'>
                      <input type='hidden' value='" . $Tagsexists['less_date'] . "' name='less_date[]'>";
    }
        $paramhiden.="<input type='hidden' value='1' name='isback'>";
    return $paramhiden;
}

//624
//end build link back
$FSTagClass = new Core_Api_FSTagClass();
    if (!empty($arrayTags))
    {
    $techTags = $FSTagClass->getTechIdsArray_forFSTagSearch($arrayTags);
        if (!empty($techTags))
        {
        $query = appendQuery($query, $db->quoteInto("TechID IN (?)", $techTags));
        } else
        {
        $query = appendQuery($query, "1=0");
    }
}

//$query = "(Certifications LIKE '')";
// ADD CONDITIONS TO THE QUERY

$query = addQueryLike($query, "TechID", $TechID);
$query = addQueryLike($query, "LastName", $LastName);
$query = addQueryLike($query, "FirstName", $FirstName);
$query = addQueryLike($query, "Username", $Username);
$query = addQueryEqual($query, "Password", $Password);
$query = addQueryLike($query, "PrimaryPhone", $PrimaryPhone);
$query = addQueryLike($query, "SecondaryPhone", $SecondaryPhone);
$query = addQueryLike($query, "SMS_Number", $SMS_Number);

if ($isWMTech == "1") {
	$query = appendQuery ($query, "NOT ISNULL(WMID)");
}
else if ($isWMTech == "0") {
	$query = appendQuery ($query, "ISNULL(WMID)");
}
else if (!empty ($WMID)) {
	$query = addQueryEqual($query, "WMID", $WMID);
}

$query = addQueryEqual($query, "DrugTest_Pass", $DrugPassed, ($DrugPassed == "X" ? "Y" : ""));
/*13700*/
$query = addQueryEqual($query, "SilverDrugTest_Pass", $SilverDrugPassed, ($SilverDrugPassed == "X" ? "Y" : ""));
/*end 13700*/

$query = addQueryGreaterEqual($query, "Qty_IMAC_Calls", $Qty_IMAC_CallsGreater);
$query = addQueryLessEqual($query, "Qty_IMAC_Calls", $Qty_IMAC_CallsLess);
$query = addQueryGreaterEqual($query, "Qty_FLS_Service_Calls", $Qty_FLS_Service_CallsGreater);
$query = addQueryLessEqual($query, "Qty_FLS_Service_Calls", $Qty_FLS_Service_CallsLess);
$query = addQueryGreaterEqual($query, "No_Shows", $No_ShowsGreater);
$query = addQueryLessEqual($query, "No_Shows", $No_ShowsLess);
$query = addQueryGreaterEqual($query, "Back_Outs", $Back_OutsGreater);
$query = addQueryLessEqual($query, "Back_Outs", $Back_OutsLess);
$query = addQueryEqual($query, "Pay_Company", $Pay_Company);
$query = addQueryEqual($query, "ISO_Affiliation_ID", $ISO_Affiliation_ID_M);
$query = addQueryEqual($query, "ISO_Affiliation_ID", $ISO_Affiliation_ID);
if (!empty($ISO_Tech_Only))
	$query = appendQuery($query, "(ISO_Affiliation_ID IS NOT NULL AND ISO_Affiliation_ID <> 0)");
else if ($ISO_Tech_Only === '0')
	$query = appendQuery($query, "(ISO_Affiliation_ID IS NULL OR ISO_Affiliation_ID = 0)");

$query = addQueryLike($query, "ISO_Affiliation", $ISO_Affiliation);
$query = addQueryEqual($query, "SSA", $SSA);
$query = addQueryEqual($query, "Secret", $Secret);
$query = addQueryEqual($query, "Top_Secret", $Top_Secret);
//$query = addQueryEqual($query, "NACI", $NACI);
$query = addQueryEqual($query, "General1", $General1);
$query = addQueryEqual($query, "General2", $General2);
$query = addQueryEqual($query, "General3", $General3);
$query = addQueryEqual($query, "General4", $General4);
$query = addQueryEqual($query, "General5", $General5);
$query = addQueryEqual($query, "General6", $General6);
$query = addQueryEqual($query, "MRA_Compliant", $MRA_Compliant);
$query = addQueryEqual($query, "CDM", $CDM);
$query = addQueryEqual($query, "L1", $L1);
$query = addQueryEqual($query, "L2", $L2);
$query = addQueryEqual($query, "L3", $L3);
$query = addQueryEqual($query, "L4", $L4);
$query = addQueryEqual($query, "DELL_DCSE_Reg", $DELL_DCSE_Reg);
$query = addQueryEqual($query, "DCSE_Soft_Skills", $DCSE_Soft_Skills);
$query = addQueryEqual($query, "DCSE_Lead", $DCSE_Lead);
$query = addQueryEqual($query, "VA", $VA);

$query = addQueryLike($query, "City", $City);
$query = addQueryEqual($query, "State", $State);
$query = addQueryEqual($query, "Country", $Country);
if ($ZipCodeDistance == "1") {
	$query = addQueryEqual($query, "ZipCode", $ZipCode);
}

$query = addQueryEqual($query, "W9", $W9);
$query = addQueryGreaterEqual($query, "W9_Date_Rec", $W9_Date_RecGreater);
$query = addQueryLessEqual($query, "W9_Date_Rec", $W9_Date_RecLess);
$query = addQueryEqual($query, "Push2Tech", $Push2Tech);
$query = addQueryGreaterEqual($query, "SMS_AgreeDate", $SMS_AgreeDateGreater);
$query = addQueryLessEqual($query, "SMS_AgreeDate", $SMS_AgreeDateLess);
$query = addQueryEqual($query, "Private", $Private);
$query = addQueryEqual($query, "W2_Tech", $W2);
$query = addQueryEqual($query, "Dispatch", $Dispatch);
$query = addQueryLike($query, "ISO_Affiliation", $ISO_Affiliation);

// added unsubscribe setting query changes.
//$query = addSettingsQuery($query, "Email", $UnsubscribeWorkEmailQuery );
//$query = addSettingsQuery($query, "SMS", $UnsubscribeWorkSMSQuery );

$query = addQueryEqual($query, "EmailContactOptOut", $UnsubscribeWorkEmail );
$query = addQueryEqual($query, "AllowText", $unsubscribeWorkSMS );

$query = addQueryEqual($query, "Deactivated", $Deactivated);
$query = addQueryEqual($query, "Deactivate_Reason", $DeactivatedReason);
/*
if ($AcceptTerms == "X") {
	$query = addQueryEqual($query, "AcceptTerms", "Yes");
} else {
	$query = addQueryEqual($query, "AcceptTerms", $AcceptTerms);
}
*/

$query = addQueryEqual($query, "FS_Cert_Test_Pass", $FSCert);

if ($hp_Cert == 'Any HP Cert') {
//	$query = $addQueryEqual($query, "ISNULL(HP_CertNum, '') <> ''");
	$query = appendQuery($query, "IFNULL(HP_CertNum, '') <> ''");
} else {
	$query = addQueryEqual($query, "HP_CertNum", $hp_Cert);
}

$query = addQueryEqual($query, "SamsungCert", $samsungCert);

$query = addQueryGreaterEqual($query, "RegDate", $RegDateGreater);
$query = addQueryLessEqual($query, "RegDate", $RegDateLess);
$query = addQueryLike($query, "TechSource", $TechSource);
$query = addQueryLike($query, "FLSID", $FLSID);
$query = addQueryEqual($query, "FLSstatus", $FLSstatus);
$query = addQueryEqual($query, "FLSCSP_Rec", $FLSCSP_Rec);
//issue_13085
$query = addQueryEqual($query, "FSPlusOneReg", $FSPlusOneReg);
$query = appendQuery($query, rangeQuery("FSPlusOneDateReg", $FSPlusOneDateRegLess, $FSPlusOneDateRegGreater));
//end issue_13085
$query = addQueryEqual($query, "Bg_Test_Req_Lite", $Bg_Test_Req_Lite);
$query = addQueryEqual($query, "Bg_Test_Pass_Lite", $Bg_Test_Pass_Lite, ($Bg_Test_Pass_Lite == "X" ? "Y" : ""));
$query = addQueryEqual($query, "Bg_Test_Pass_Full", $Bg_Test_Pass_Full, ($Bg_Test_Pass_Full == "X" ? "Y" : ""));

$query = addQueryLike($query, "Skills", $Skills, $SkillsBlank);

$query = addQueryEqual($query, "DellCert", $DellCert);
$query = addQueryEqual($query, "MCSE", $MCSE);
$query = addQueryEqual($query, "CCNA", $CCNA);
$query = addQueryEqual($query, "APlus", $APlus);
$query = addQueryLike($query, "CompTIA", $CompTIA, $CompTIABlank);
$query = addQueryLike($query, "Certifications", $Certifications, $CertificationsBlank);
$query = addQueryLike($query, "SpecificExperience", $SpecificExperience, $ExperienceBlank);
$query = addQueryGreaterEqual($query, "PreferencePercent", $preference, false);
$query = addQueryGreaterEqual($query, "SATRecommendedTotal", $prefRatings, false);
$query = addQueryGreaterEqual($query, "PerformancePercent", $Performance, false);
$query = addQueryGreaterEqual($query, "SATPerformanceTotal", $perfRatings, false);
$query = addQuerySkills($query, "electronicsSelfRating", $ElectronicsSelfRating, $Electronics);
$query = addQuerySkills($query, "AlarmSystemsSelfRating", $AlarmSystemsSelfRating, $AlarmSystems);//13978 

$query = addQuerySkills($query, "AdjustableBedRepairSelfRating", $AdjustableBedRepairSelfRating, $AdjustableBedRepair);
$query = addQuerySkills($query, "appleSelfRating", $appleSelfRating, $Apple);
$query = addQuerySkills($query, "atmSelfRating", $ATMSelfRating, $ATM);
$query = addQuerySkills($query, "cablingSelfRating", $CablingSelfRating,$Cabling);
$query = addQuerySkills($query, "CCTVSelfRating", $CCTVSelfRating,$CCTV);
$query = addQuerySkills($query, "CentralOfficeCablingSelfRating", $CentralOfficeCablingSelfRating,$CentralOfficeCabling);
$query = addQuerySkills($query, "constructionSelfRating", $ConstructionSelfRating, $Construction);
$query = addQuerySkills($query, "CopiersSelfRating", $CopiersSelfRating, $Copiers);
$query = addQuerySkills($query, "desktopSelfRating", $DesktopSelfRating, $Desktop);
$query = addQuerySkills($query, "DigitalSignageSelfRating", $DigitalSignageSelfRating, $DigitalSignage);
$query = addQuerySkills($query, "DslSelfRating", $DslSelfRating, $Dsl);
$query = addQuerySkills($query, "electricalSelfRating", $ElectricalSelfRating, $Electrical);
$query = addQuerySkills($query, "ElectricianCertifiedSelfRating", $ElectricianCertifiedSelfRating, $ElectricianCertified);//14121
$query = addQuerySkills($query, "electroMechSelfRating", $electroMechSelfRating, $electroMech);
$query = addQuerySkills($query, "FiberCablingSelfRating", $FiberCablingSelfRating, $FiberCabling);
$query = addQuerySkills($query, "FlatPanelTVSelfRating", $FlatPanelTVSelfRating, $FlatPanelTV);
$query = addQuerySkills($query, "GeneralWiringSelfRating", $GeneralWiringSelfRating, $GeneralWiring);
$query = addQuerySkills($query, "networkingSelfRating", $NetworkingSelfRating, $Networking);
$query = addQuerySkills($query, "KioskSelfRating", $KioskSelfRating, $Kiosk);
$query = addQuerySkills($query, "LowVoltageSelfRating", $LowVoltageSelfRating, $LowVoltage);
$query = addQuerySkills($query, "posSelfRating", $POSSelfRating, $POS);
$query = addQuerySkills($query, "PrintersSelfRating", $PrintersSelfRating, $Printers);
$query = addQuerySkills($query, "RFIDSelfRating", $RFIDSelfRating, $RFID);
$query = addQuerySkills($query, "routersSelfRating", $RoutersSelfRating, $Routers);
$query = addQuerySkills($query, "savesSecCabinetsSelfRating", $savesSecCabinetsSelfRating, $savesSecCabinets);
$query = addQuerySkills($query, "SatelliteSelfRating", $SatelliteSelfRating, $Satellite);
$query = addQuerySkills($query, "ServersSelfRating", $ServersSelfRating, $Servers);
$query = addQuerySkills($query, "ServerSoftwareSelfRating", $ServerSoftwareSelfRating, $ServerSoftware);
$query = addQuerySkills($query, "SiteSurveySelfRating", $SiteSurveySelfRating, $SiteSurvey);
$query = addQuerySkills($query, "telephonySelfRating", $TelephonySelfRating, $Telephony);
$query = addQuerySkills($query, "TelephonyVoIPSelfRating", $TelephonyVoIPSelfRating, $TelephonyVoIP);
$query = addQuerySkills($query, "WirelessSelfRating", $WirelessSelfRating, $Wireless);
$query = addQuerySkills($query, "ResidentialSelfRating", $ResidentialSelfRating, $Residential);
$query = addQuerySkills($query, "CommercialSelfRating", $CommercialSelfRating, $Commercial);
$query = addQuerySkills($query, "GovernmentSelfRating", $GovernmentSelfRating, $Government);
$query = addQuerySkills($query, "EnterpriseStorageSelfRating", $EnterpriseStorageSelfRating, $EnterpriseStorage);
$query = addQuerySkills($query, "MobileDevicesSelfRating", $MobileDevicesSelfRating, $MobileDevices);
$query = addQuerySkills($query, "ScannersSelfRating", $ScannersSelfRating, $Scanners);// 13735
$query = addQuerySkillsFSExpert($query, "LowVoltage", $LowVoltageFSExpert);
$query = addQuerySkillsFSExpert($query, "MobileDevices", $MobileDevicesFSExpert);
$query = addQuerySkillsFSExpert($query, "Scanners", $ScannersFSExpert);//13735
$query = addQuerySkillsFSExpert($query, "pos", $posFSExpert);
$query = addQuerySkillsFSExpert($query, "Printers", $PrintersFSExpert);
$query = addQuerySkillsFSExpert($query, "RFID", $RFIDFSExpert);

$query = addQuerySkillsFSExpert($query, "routers", $routersFSExpert);
$query = addQuerySkillsFSExpert($query, "savesSecCabinets", $savesSecCabinetsFSExpert);
$query = addQuerySkillsFSExpert($query, "Satellite", $SatelliteFSExpert);
$query = addQuerySkillsFSExpert($query, "Servers", $ServersFSExpert);
$query = addQuerySkillsFSExpert($query, "ServerSoftware", $ServerSoftwareFSExpert);

$query = addQuerySkillsFSExpert($query, "SiteSurvey", $SiteSurveyFSExpert);
$query = addQuerySkillsFSExpert($query, "telephony", $telephonyFSExpert);
$query = addQuerySkillsFSExpert($query, "TelephonyVoIP", $TelephonyVoIPFSExpert);
$query = addQuerySkillsFSExpert($query, "Wireless", $WirelessFSExpert);
//
$query = addQuerySkillsFSExpert($query, "electronics", $electronicsFSExpert);
$query = addQuerySkillsFSExpert($query, "AlarmSystems", $AlarmSystemsFSExpert); //13978
$query = addQuerySkillsFSExpert($query, "AdjustableBedRepair", $AdjustableBedRepairFSExpert);
$query = addQuerySkillsFSExpert($query, "apple", $appleFSExpert);
$query = addQuerySkillsFSExpert($query, "atm", $atmFSExpert);
$query = addQuerySkillsFSExpert($query, "cabling", $cablingFSExpert);

$query = addQuerySkillsFSExpert($query, "CCTV", $CCTVFSExpert);
$query = addQuerySkillsFSExpert($query, "CentralOfficeCabling", $CentralOfficeCablingFSExpert);
$query = addQuerySkillsFSExpert($query, "construction", $constructionFSExpert);
$query = addQuerySkillsFSExpert($query, "Copiers", $CopiersFSExpert);
$query = addQuerySkillsFSExpert($query, "desktop", $desktopFSExpert);

$query = addQuerySkillsFSExpert($query, "DigitalSignage", $DigitalSignageFSExpert);
$query = addQuerySkillsFSExpert($query, "Dsl", $DslFSExpert);
$query = addQuerySkillsFSExpert($query, "electrical", $electricalFSExpert);
$query = addQuerySkillsFSExpert($query, "ElectricianCertified", ElectricianCertifiedFSExpert); //14121
$query = addQuerySkillsFSExpert($query, "electroMech", $electroMechFSExpert);
$query = addQuerySkillsFSExpert($query, "EnterpriseStorage", $EnterpriseStorageFSExpert);

$query = addQuerySkillsFSExpert($query, "FiberCabling", $FiberCablingFSExpert);
$query = addQuerySkillsFSExpert($query, "FlatPanelTV", $FlatPanelTVFSExpert);
$query = addQuerySkillsFSExpert($query, "GeneralWiringF", $GeneralWiringFSExpert);
$query = addQuerySkillsFSExpert($query, "networking", $networkingFSExpert);
$query = addQuerySkillsFSExpert($query, "Kiosk", $KioskFSExpert);
//
$Electronics = processSkill($Electronics, "1");
$AlarmSystems = processSkill($AlarmSystems, "1");  //13978
$AdjustableBedRepair = processSkill($AdjustableBedRepair, "1");
$Apple = processSkill($Apple, "1");
$ATM = processSkill($ATM, "1");
$Cabling = processSkill($Cabling, "1");
$CCTV = processSkill($CCTV, "1");
$Construction = processSkill($Construction, "1");
$Desktop = processSkill($Desktop, "1");
$DigitalSignage = processSkill($DigitalSignage, "1");
$Dsl = processSkill($Dsl, "1");
$Electrical = processSkill($Electrical, "1");
$ElectricianCertified = processSkill($ElectricianCertified, "1");  //14121
$FiberCabling = processSkill($FiberCabling, "1");
$GeneralWiring = processSkill($GeneralWiring, "1");
$Networking = processSkill($Networking, "1");
$Kiosk = processSkill($Kiosk, "1");
$LowVoltage = processSkill($LowVoltage, "1");
$POS = processSkill($POS, "1");
$Printers = processSkill($Printers, "1");
$RFID = processSkill($RFID, "1");
$Routers = processSkill($Routers, "1");
$Satellite = processSkill($Satellite, "1");
$Servers = processSkill($Servers, "1");
$ServerSoftware = processSkill($ServerSoftware, "1");
$SiteSurvey = processSkill($SiteSurvey, "1");
$Telephony = processSkill($Telephony, "1");
$TelephonyVoIP = processSkill($TelephonyVoIP, "1");
$Wireless = processSkill($Wireless, "1");
$Residential = processSkill($Residential, "1");
$Commercial = processSkill($Commercial, "1");
$Government = processSkill($Government, "1");
$EnterpriseStorage = processSkill($EnterpriseStorage, "1");
$MobileDevices = processSkill($MobileDevices, "1");

    /* $query = addQueryEqual($query, "electronicsYN", $Electronics);
$query = addQueryEqual($query, "Apple", $Apple);
$query = addQueryEqual($query, "atmYN", $ATM);
$query = addQueryEqual($query, "cablingYN", $Cabling);
$query = addQueryEqual($query, "CCTVYN", $CCTV);
$query = addQueryEqual($query, "constructionYN", $Construction);
$query = addQueryEqual($query, "desktopYN", $Desktop);
$query = addQueryEqual($query, "DigitalSignageYN", $DigitalSignage);
$query = addQueryEqual($query, "DslYN", $Dsl);
$query = addQueryEqual($query, "electricalYN", $Electrical);
$query = addQueryEqual($query, "FiberCablingYN", $FiberCabling);
$query = addQueryEqual($query, "GeneralWiringYN", $GeneralWiring);
$query = addQueryEqual($query, "networkingYN", $Networking);
$query = addQueryEqual($query, "KioskYN", $KioskYN);
$query = addQueryEqual($query, "LowVoltageYN", $LowVoltageYN);
$query = addQueryEqual($query, "posYN", $POS);
$query = addQueryEqual($query, "PrintersYN", $Printers);
$query = addQueryEqual($query, "RFIDYN", $RFID);
$query = addQueryEqual($query, "routersYN", $Routers);
$query = addQueryEqual($query, "SatelliteYN", $Satellite);
$query = addQueryEqual($query, "ServersYN", $Servers);
$query = addQueryEqual($query, "ServerSoftwareYN", $ServerSoftware);
$query = addQueryEqual($query, "SiteSurveyYN", $SiteSurvey);
$query = addQueryEqual($query, "telephonyYN", $Telephony);
$query = addQueryEqual($query, "TelephonyVoIPYN", $TelephonyVoIP);
$query = addQueryEqual($query, "WirelessYN", $Wireless);
$query = addQueryEqual($query, "Residential", $Residential);
$query = addQueryEqual($query, "Commercial", $Commercial);
$query = addQueryEqual($query, "Government", $Government);*/


$apiTechClass = new Core_Api_TechClass();
if($Resume == "0" || $Resume == "1")
{
    $techIDsHaveResume = $apiTechClass->getTechIDs_HaveResume();    
    if($Resume==1)
    {
        $query = appendQuery($query, $db->quoteInto("TechID IN (?)", $techIDsHaveResume));
}
    else if($Resume==0)
    {
        $query = appendQuery($query, $db->quoteInto("TechID NOT IN (?)", $techIDsHaveResume));
}
}


if ($EmailType == "Primary" && $Email != "") {
	$query = addQueryLike($query, "PrimaryEmail", $Email);
}
elseif ($EmailType == "Secondary" && $Email != "") {
	$query = addQueryLike($query, "SecondaryEmail", $Email);
}
elseif ($Email != "") {
	$query = ($query != "" ? "$query and " : "") . "(PrimaryEmail LIKE '%$Email%' or SecondaryEmail LIKE '%$Email%')";
}

//-------------------------------------------------------------------
// CLIENT DENIED:

if ($ClientDenied != "") {
	switch ($ClientDenied) {
		case "Any":
			if (!empty($deniedList))
			    $deniedQuery = "(TechID IN (" . implode(',',$deniedList) . ") OR FLSstatus = 'Admin Denied')";
			else
			    $deniedQuery = "FLSstatus = 'Admin Denied'";
			$query = appendQuery($query, $deniedQuery);
			break;
		case "None":
			if (!empty($deniedList))
			    $deniedQuery = "(TechID NOT IN (" . implode(',',$deniedList) . ") AND FLSstatus <> 'Admin Denied')";
			else
			    $deniedQuery = "FLSstatus <> 'Admin Denied'";
			$query = appendQuery($query, $deniedQuery);
			break;
		case "FLS":
			$query = appendQuery($query, "FLSstatus = 'Admin Denied'");
			break;
		default:
			$deniedListCompany = Core_Tech::getClientDeniedTechsArray($ClientDenied);
			if (!empty($deniedListCompany)) {
			    $deniedQuery = "(TechID IN (" . implode(',',$deniedListCompany) . "))";
			}
			else
			    $deniedQuery = "TechID IN (0)";
			$query = appendQuery($query, $deniedQuery);
	}
}
//-------------------------------------------------------------------
// HIDE BIDS: 14018

if ($HideBids != "") {
    if($HideBids == '1'){
        $query = appendQuery($query, 'HideBids = 1');
    } else if($HideBids=='0') {
        $query = appendQuery($query, 'HideBids = 0');
    }
}


//--------------------------------------------------------------
// LAST LOGGED ON DATE
//

if (!empty($LoggedOnDateGreater) || !empty($LoggedOnDateLess)) {
	$db = Zend_Registry::get('DB');
	//echo "SELECT DISTINCT Tech_ID FROM work_orders AS t WHERE Tech_ID IS NOT NULL AND $WORunQuery";
	$range = array();
	if (!empty($LoggedOnDateGreater)) $range[] = "login_history.DateTime >= '$LoggedOnDateGreater'";
	if (!empty($LoggedOnDateLess)) $range[] = "login_history.DateTime <= '$LoggedOnDateLess'";
        $whereClause = implode(" AND ", $range);
        $whereClause .= empty($whereClause) ? " login_history.UserType = 'Tech'" : " AND login_history.UserType = 'Tech'";
        if(!empty($whereClause)) $whereClause = " WHERE ".$whereClause;
	$mysqlResult = $db->fetchCol("SELECT DISTINCT UserName FROM login_history " . $whereClause);

	if ($mysqlResult) {
		$query = appendQuery($query, "UserName IN ('" . implode("','", $mysqlResult) . "')");
	}
	else
		$query = appendQuery($query, "UserName IN (0)");
}

//---  US Authorized
$I9SearchCriteria="";
$api= new Core_Api_TechClass;  

if($USAuthorizedValue==1)//Pending
{
    $ids= $api->getTechIdsByI9Status(1);
    if(!empty($ids))
    {
       $I9SearchCriteria.='TechID IN ('.$ids.')'; 
    }
    else
    {
       $I9SearchCriteria.='1=0';  
    }            
    $query = appendQuery($query, $I9SearchCriteria);   
} 
if($USAuthorizedValue==2)//Authorized
{
    $ids= $api->getTechIdsByUSAuthorized($USAuthorizedDateGreater,$USAuthorizedDateLess);
    if(!empty($ids))
    {
       $I9SearchCriteria.='TechID IN ('.$ids.')'; 
    }
    else
    {
       $I9SearchCriteria.='1=0';  
    }            
    $query = appendQuery($query, $I9SearchCriteria);   
} 
if($USAuthorizedValue==3)//Pending OR Authorized
{
    $ids= $api->getTechIdsWithUSAuthorizedOrPending();
    if(!empty($ids))
    {
       $I9SearchCriteria.='TechID IN ('.$ids.')'; 
    }
    else
    {
       $I9SearchCriteria.='1=0';  
    }            
    $query = appendQuery($query, $I9SearchCriteria);   
} 
if($USAuthorizedValue==4)//Filter Value = 4: that means searching for 'Non Confirmation'
{
    $ids= $api->getTechIdsByI9Status(3);
    if(!empty($ids))
    {
       $I9SearchCriteria.='TechID IN ('.$ids.')'; 
    }
    else
    {
       $I9SearchCriteria.='1=0';  
    }            
    $query = appendQuery($query, $I9SearchCriteria);   
} 
//--- CopierSkills: 
//Pass, Fail, No
$CopierSkillsCriteria = "";
if($CopierSkillsValue=='Pass')
{
    $ids=$api->getTechIds_PassCopierSkills($CopierSkillsGreater,$CopierSkillsLess);
    if(!empty($ids))
    {
       $CopierSkillsCriteria.='TechID IN ('.$ids.')'; 
    }
    else
    {
       $CopierSkillsCriteria.='1=0';  
    }            
    $query = appendQuery($query, $CopierSkillsCriteria);   
} else if($CopierSkillsValue=='Fail')
{
    $ids=$api->getTechIds_FailCopierSkills();
    if(!empty($ids))
    {
       $CopierSkillsCriteria.='TechID IN ('.$ids.')'; 
    }
    else
    {
       $CopierSkillsCriteria.='1=0';  
    }            
    $query = appendQuery($query, $CopierSkillsCriteria);       
} else if($CopierSkillsValue=='No')
{
    $ids=$api->getTechIds_CopierSkills($CopierSkillsGreater,$CopierSkillsLess);
    //echo("<br/>getTechIds_CopierSkills: ".$ids);
    if(!empty($ids))
    {
       $CopierSkillsCriteria.='TechID NOT IN ('.$ids.')'; 
       $query = appendQuery($query, $CopierSkillsCriteria);
    }    
}



//--- US Authorized: end

//---  EMC Certification
$EMCSearchCriteria="";
$apiEMCsearch = new Core_Api_TechClass();
$EMCsearchresult = $apiEMCsearch -> getTechIdsForCertification($EMC,$EMCGreater,$EMCLess);
if($EMCsearchresult != '')//Pending
{

    $EMCSearchCriteria .= 'TechID IN ('.$EMCsearchresult.')'; 
    $query = appendQuery($query, $EMCSearchCriteria);  
}
//---  EMC Certification end

$EMCcertificationCriteria="";
if(trim($EMCcertifications) !="") {
	$EMCcertificationsresult = $api->getTechIdsArray_ByEMCStatus($EMCcertifications,$EMCcertificationsGreater,$EMCcertificationsLess);

	if($EMCcertificationsresult !="") {
	    $EMCcertificationCriteria = "TechID IN ('".implode("','",$EMCcertificationsresult)."')"; 

	    $query = appendQuery($query, $EMCcertificationCriteria);  
	}
	//else {
	//    $EMCcertificationCriteria = "1=0"; 
	//}
}

//--------------------------------------------------------------
// ACCEPT TERMS DATE
//

$acceptTermsClause = "Date_TermsAccepted";

switch ($AcceptTerms) {
	case "Yes":
		$query = appendQuery($query, "AcceptTerms = 'Yes'");
		break;
	case "No":
		$query = appendQuery($query, "(AcceptTerms <> 'Yes' OR AcceptTerms IS NULL)");
		break;
	case "X":
		$query = appendQuery($query, "(AcceptTerms = 'Yes' and $acceptTermsClause IS NULL)");
		break;
}
		$query = appendQuery($query, rangeQuery($acceptTermsClause, $AcceptTermsLess, $AcceptTermsGreater));

// LAST WORK ORDER RUN DATE
$WORunQuery = "";
switch ($LastWORunType) {
	case "Main":
//		$query = appendQuery($query, "EXISTS ($lastWORunMainClause)");
		$WORunQuery = appendQuery($WORunQuery, "Company_ID != 'FLS' AND " . rangeQueryMySql("StartDate", $LastWORunLess, $LastWORunGreater));
		break;
	case "FLS":
//		$query = appendQuery($query, "EXISTS ($lastWORunFLSClause)");
		$WORunQuery = appendQuery($WORunQuery, "Company_ID = 'FLS' AND " . rangeQueryMySql("StartDate", $LastWORunLess, $LastWORunGreater));
		break;
	case "Any":
		$WORunQuery = appendQuery($WORunQuery, rangeQueryMySql("StartDate", $LastWORunLess, $LastWORunGreater));
		break;
	default:
}

if (!empty($WORunQuery)) {
	$db = Zend_Registry::get('DB');
	//echo "SELECT DISTINCT Tech_ID FROM work_orders AS t WHERE Tech_ID IS NOT NULL AND $WORunQuery";
	$mysqlResult = $db->fetchCol("SELECT DISTINCT Tech_ID FROM work_orders AS t WHERE Tech_ID IS NOT NULL AND $WORunQuery");
	//echo "SELECT DISTINCT Tech_ID FROM work_orders AS t WHERE Tech_ID IS NOT NULL AND $WORunQuery,br/>";
	if ($mysqlResult) {
		$query = appendQuery($query, "TechID IN (" . implode(",", $mysqlResult) . ")");
	}
	else
		$query = appendQuery($query, "TechID IN (0)");
}


// LAST WORK ORDER BID DATE

switch ($LastWOBidType) {
	case "Main":
		$WOBidQuery = appendQuery($WOBidQuery, rangeQuery("Bid_Date", $LastWOBidLess, $LastWOBidGreater)) . " AND Company_ID != 'FLS'";
		break;
	case "FLS":
		$WOBidQuery = appendQuery($WOBidQuery, rangeQuery("Bid_Date", $LastWOBidLess, $LastWOBidGreater)) . " AND Company_ID = 'FLS'";
		break;
	case "Any":
		$WOBidQuery = appendQuery($WOBidQuery, rangeQuery("Bid_Date", $LastWOBidLess, $LastWOBidGreater));
		break;
	default:
}

if (!empty($WOBidQuery)) {
        $db = Zend_Registry::get('DB');
        //echo "SELECT DISTINCT Tech_ID FROM work_orders AS t WHERE Tech_ID IS NOT NULL AND $WORunQuery";
        $mysqlResult = $db->fetchCol("SELECT DISTINCT TechID FROM work_orders__bids AS t WHERE TechID IS NOT NULL AND $WOBidQuery");
	//echo "SELECT DISTINCT TechID FROM work_orders__bids AS t WHERE TechID IS NOT NULL AND $WOBidQuery<br/>";
        if ($mysqlResult) {
                $query = appendQuery($query, "TechID IN (" . implode(",", $mysqlResult) . ")");
        }
}

// FS-Mobile DATE
if ($FSMobile != "" && $FSMobileLess == "" && $FSMobileGreater == "") {
	if ($FSMobile == "1") {
		$FSMobileGreater = "2000-01-01";
	}
	else {
		if ($query != "") {
			$query .= " and ISNULL (FS_Mobile_Date)";
		} else {
			$query = "ISNULL (FS_Mobile_Date)";
		}
	}
}

$query = appendQuery ($query, rangeQuery("FS_Mobile_Date", $FSMobileLess, $FSMobileGreater));

// BG REQUEST DATE

$query = appendQuery($query, rangeQuery("Bg_Test_ReqDate_Lite", $BgReqLess, $BgReqGreater));

// FS Cert DATE

$query = appendQuery($query, rangeQuery("FS_Cert_Date_Taken", $FSCertLess, $FSCertGreater));
// BG PASS DATE

$query = appendQuery($query, rangeQuery("BG_Test_ResultsDate_Lite", $BgPassLess, $BgPassGreater));
$query = appendQuery($query, rangeQuery("BG_Test_ResultsDate_Full", $BgPassFullLess, $BgPassFullGreater));
$query = appendQuery($query, rangeQuery("DatePassDrug", $BgDrugPassedLess, $BgDrugPassedGreater));
/*13700 */
$query = appendQuery($query, rangeQuery("DatePassedSilverDrug", $BgSilverDrugPassedLess, $BgSilverDrugPassedGreater));
/*end 13700 */
//14056
if(trim($NACI)!= "")
{
    if($NACI == '1'){
        $query = appendQuery($query, '( NACI = 1 )');  
    } else if($NACI == '2') {
        $query = appendQuery($query, '( NACI = 1 OR NACI = 2 )');  
    } else if($NACI == '0'){
        $query = appendQuery($query, '( NACI = 0 OR NACI IS NULL )');  
    }
    $query = appendQuery($query, rangeQuery("NACIDate",$NACILess, $NACIGreater)); 
}
if(trim($InGovClearance)!= "")
{
    if($InGovClearance == '1'){
        $query = appendQuery($query, '( InterimSecClear = 1 )');  
    } else if($InGovClearance == '2') {
        $query = appendQuery($query, '( InterimSecClear = 1 OR InterimSecClear = 2 )');  
    } else if($InGovClearance == '0'){
        $query = appendQuery($query, '( InterimSecClear = 0 OR InterimSecClear IS NULL )');  
    }
    $query = appendQuery($query, rangeQuery("InterimSecClearDate",$InGovClearanceLess, $InGovClearanceGreater)); 
}
if(trim($FullGovClearance)!= "")
{
    if($FullGovClearance == '1'){
        $query = appendQuery($query, '( FullSecClear = 1 )');  
    } else if($FullGovClearance == '2') {
        $query = appendQuery($query, '( FullSecClear = 1 OR FullSecClear = 2 )');  
    } else if($FullGovClearance == '0'){
        $query = appendQuery($query, '( FullSecClear = 0 OR FullSecClear IS NULL )');  
    }
    $query = appendQuery($query, rangeQuery("FullSecClearDate",$FullGovClearanceLess, $FullGovClearanceGreater)); 
    //print_r($query); die();
}
if(trim($TopGovClearance)!= "")
{
    if($TopGovClearance == '1'){
        $query = appendQuery($query, '( TopSecClear = 1 )');  
    } else if($TopGovClearance == '2') {
        $query = appendQuery($query, '( TopSecClear = 1 OR TopSecClear = 2 )');  
    } else if($TopGovClearance == '0'){
        $query = appendQuery($query, '( TopSecClear = 0 OR TopSecClear IS NULL )');  
    }
    $query = appendQuery($query, rangeQuery("TopSecClearDate",$TopGovClearanceLess, $TopGovClearanceGreater)); 
    //print_r($query); die();
}
if(trim($TopSecretSCIGovClearance)!= "")
{
    if($TopSecretSCIGovClearance == '1'){
        $query = appendQuery($query, '( TopSecSCIClear = 1 )');  
    } else if($TopSecretSCIGovClearance == '2') {
        $query = appendQuery($query, '( TopSecSCIClear = 1 OR TopSecSCIClear = 2 )');  
    } else if($TopSecretSCIGovClearance == '0'){
        $query = appendQuery($query, '( TopSecSCIClear = 0 OR TopSecSCIClear IS NULL )');  
    }
    $query = appendQuery($query, rangeQuery("TopSecSCIClearDate",$TopSecretSCIGovClearanceLess, $TopSecretSCIGovClearanceGreater)); 
    //print_r($query); die();
}
//end 14056
// FLS CSP DATE

$query = appendQuery($query, rangeQuery("FLSCSP_RecDate", $FLSCSPLess, $FLSCSPGreater));

// TECH PREFERRED

$query = appendQuery($query, "TechID NOT LIKE '%ISO%'");


//030
$query = appendQuery($query, $searchTechByPhotosquery);

//end 030

// TECH SOURCE, 13702
if(!empty($techSourceFilter)){
    $query = appendQuery($query, "TechSource = '$techSourceFilter'");    
}


	if ($ShowQuery) {
		echo "<p>QUERY: $query ";
	}
} // end of "if $_POST"


// GET PAGE VARIABLE

$page = 1;
$offset = 0;

if (isset($_GET['page'])) {
	$page = $_GET['page'];
	if ($page < 1) {
		$page = 1;
	}
}
// get source list, 13702
$Core_Api_TechClass = new Core_Api_TechClass();
$techSourceList = $Core_Api_TechClass->getTechSourceList_forAdmin();//14145
//end 13702

$isback = getFormField("isback");

// IF RESULTS HAVE BEEN POSTED, SHOW THE RESULTS
// DETERMINE THE SORT ORDER

if ((isset($_GET['page']) || isset($_GET['sort']) || isset($_GET['download']) || $_POST) && ($isback!=1))
{
	// LOAD THE SORT ORDER FROM THE URL PARAMS
	if (isset($_GET['sort'])) {
		$sortByParam = $_GET['sort'];
	}
	if (isset($_GET['dir'])) {
		$sortDir = $_GET['dir'];
	}

	$_SESSION['sort'] = $sortByParam;
	$_SESSION['dir'] = $sortDir;

	$sortBy = $sortByParam;

	// SORT BY

	switch ($sortByParam) {
		case "Deactivated":
			$sortBy = $Deactivated;
			break;
		case "ClientDenied":
			$sortBy = $clientDeniedSelect;
			break;
		case "LoggedOnDate":
			$sortBy = $dateClause;
			break;
        case "USAuthorized":
            $sortBy = $I9StatusClause;
            break;
		case "Resume":
			$sortBy = $resumeClause;
			break;
		case "LastWORun":
//			$sortBy = $lastWORunMainClause;
			break;
		case "LastWORunFLS":
//			$sortBy = $lastWORunFLSClause;
			break;
		case "LastWOBid":
//			$sortBy = $lastWOBidMainClause;
			break;
		case "LastWOBidFLS":
//			$sortBy = $lastWOBidFLSClause;
			break;
		case "Experience":
			$sortBy = "cast(SpecificExperience as char(20))";
			break;
		case "OtherCerts":
			$sortBy = "cast(Certifications as char(20))";
			break;
		case "Skills":
			$sortBy = "cast(Skills as char(20))";
			break;
	}

	//echo "SORT BY: $sortBy $sortDir</p><p>";
// MAIN QUERY

	$results_array = array ();
     
	// if search criteria have been posted, run the search:
	if ($_POST)
	{
		$gross_results_base = queryTechs(array('TechID', 'PrimaryEmail'), $query, "$sortBy $sortDir", $ZipCode, $ZipCodeDistance);

		foreach ($gross_results_base as $entry) {
			$parts = $entry;
			$results_array[] = process($parts[0]);
			$email_results[] = process($parts[1]);
		}
		$gross_results = array_unique($results_array);

		$_SESSION['gross_results'] = $gross_results;
	}

	// if results haven't been posted, load the results array
	// from the session
	if (!$_POST)
	{
		$DisplayOption = $_SESSION["DisplayOption"];
		$gross_results = $_SESSION['gross_results'];
		$results_list = format_list($gross_results);

		$gross_results_base = queryTechs(array('TechID', 'PrimaryEmail'), "TechID in ($results_list)", "$sortBy $sortDir");

		foreach ($gross_results_base as $entry) {
			$parts = $entry;
			$results_array[] = process($parts[0]);
			$email_results[] = process($parts[1]);
		}
		$gross_results = array_unique($results_array);
	}

	$result_count = sizeof($gross_results);

	$results_list = format_list($gross_results);

	// NO RESULTS

	if ($results_list == "" || $result_count < 1) // || $gross_results[0] == "")
	{
		echo "<div id='no_results'>";
		echo "No results.";
		echo "</div>";
		die();
	}


if (!isset($_GET['download'])) {
	// --------------------------------------------------
	// PAGES ARE DIVIDED INTO BLOCKS FOR NAVIGATION
	// there are $Page_Block_Size pages per each block
	// Page_Block_Size - max number of pages per block
	// Max_Results - max number of entries per page
	// total_pages - total number of pages of results
	// page - current page
	// offset - the offset before the first record on the page
	// total_blocks - total number of page blocks
	// first_page - first page of the current page block
	// last_page - last page of the current page block
	// 100 / 25 = 4 pages
	// 104 / 25 = 5 pages
	$total_pages = round_up(($result_count / $Max_Results));

	if ($page > $total_pages) {
		$page = $total_pages;
	}

	// ((1-1) * 25) = (0 * 25) = 0
	// ((2-1) * 25) = (1 * 25) = 25
	// ((3-1) * 25) = (2 * 25) = 50
	$offset = (($page - 1) * $Max_Results);

	// 1000 results / 25 per page = 40 pages = 4 blocks
	// 40 pages / 10 = 4 page blocks
	// 44 pages / 10 = 5 page blocks
	$total_blocks = round_up($total_pages / $Page_Block_Size);

	$page_block = round_up(($page / $Page_Block_Size) + 1);

	$first_page = (int) (($page_block-1) * $Page_Block_Size) + 1;
	$last_page = $first_page + $Page_Block_Size - 1;

	if ($page < $first_page) {
		$page_block = $page_block - 1;
		$first_page = $first_page - $Page_Block_Size;
		$last_page = $last_page - $Page_Block_Size;
	}

	if ($last_page > $total_pages) {
		$last_page = $total_pages;
	}


	// --------------------------------------------------
	// GETS THE FIRST $Max_Results ENTRIES FROM THE $results ARRAY

	if ($result_count < $Max_Results) {
		$net_results = $gross_results;
	}
	else {
		$net_results = array_slice($gross_results, $offset, $Max_Results);
	}
	$Max_Page = $result_count / $Max_Results;

	// --------------------------------------------------
	// FORMATS THE LIST OF IDs FOR USE IN LAST QUERY

	$results_list = format_list($net_results);

} elseif (!isset($_GET['first'])) {
	
		echo "<h1>Download search results</h1>";
		?>
		<div>records per file <select id="downloadSize"><option value='5000' <?=isDownloadSizeSelected(5000)?>>5000</option><option value='2500' <?=isDownloadSizeSelected(2500)?>>2500</option><option value='1000' <?=isDownloadSizeSelected(1000)?>>1000</option><option value='500' <?=isDownloadSizeSelected(500)?>>500</option><option value='250' <?=isDownloadSizeSelected(250)?>>250</option><option value='100' <?=isDownloadSizeSelected(100)?>>100</option></select></div>
        
        <script type="text/javascript">
			function downloadSizeChange() {
				qs = "<?=$_SERVER['QUERY_STRING']?>";
				qs = qs.replace("&inc=<?=$_GET["inc"]?>", "");
				document.location.replace("<?=$_SERVER['PHP_SELF']?>?" + qs + "&inc=" + $("#downloadSize").val());
			}
			
			$(document).ready(function() {
				$("#downloadSize").change(downloadSizeChange);
			});
		</script>
        
        <?php

		echo "<p>There are " . sizeof($gross_results) . " search results total.</p>";

		for ($i=1; $i <= sizeof($gross_results); $i = $i + $max_records) {
			$f = $i;
			$m = $f + $max_records - 1;
			if ($m > sizeof($gross_results)) {
				$m = sizeof($gross_results);
			}
			echo "<p>Download results <a href='$Current_Page?download&first=$f&max=$m&DisplayOption=$DisplayOption'>$f to $m</a></p>";
		}

require ("../footer.php");

}


if (isset($_GET["first"])) {
	$first_record = (int) $_GET["first"];

	if (isset($_GET["max"])) {
		$max_records = (int) $_GET["max"];
	}


	$last_record = $max_records;

	if ($last_record > sizeof($gross_results)) {
		$last_record = sizeof($gross_results);
	}

	$csv_filename = "tech_recs_" . $first_record . "_" . $last_record . "_" . session_id() . ".csv";

}

if (!isset($_GET['download']) || isset($_GET['first'])) {

	// --------------------------------------------------
	// RETURN THE FULL DATA FOR THE CURRENT PAGE OF RESULTS
	// as more fields are added to the search, the system gets overwhelmed and the text fields must be limited more
	$text_field_length = 259;
	//$text_field_length = 1;
//13735
	$csv_columns = array("TechID", "FirstName", "LastName", "UserName", "PrimaryEmail", "SecondaryEmail", "Primary Phone", "City", "State", "ZipCode", "Country", "Password", "RegDate", "W9 Date", "Tech Source", "FS PlusOne", "Terms Accepted", "Terms Accepted Date", "FLSID", "FLS Status", "FLSCSP Rec", "FLSCSP Rec Date", "FS Certification", "FS Certification Date", "BG Check Req", "BG Check Pass", "Comprehensive BC Passed",        //14145
    "US Verified", "DeVry", "Tech Boot Camp","Gold Drug Status", "Gold Drug Date","Silver Drug Status", "Silver Drug Date", "IMACs", "Service Calls", "No Shows","Back Outs",
	"Skills", "DellCert", "MCSE", "CCNA", "APlus","APlus Cert#", "CompTIA","PDIPlus","ServerPlus","NetworkPlus", "Other Certs", "Experience", "Resume", "Client Denied", "Last Logged On", "Last WO Run", "Last WO Run (FLS)", "Last WO Bid", "Last WO Bid (FLS)",
	//"Cabling", "Cabling Rating", "Electrical", "Electrical Rating", "Construction", "Construction Rating", "Desktop", "Desktop Rating", "Telephony", "Telephony Rating", "Networking", "Networking Rating", "Routers", "Routers Rating", "Electronics", "Electronics Rating", "POS", "POS Rating", "ATM", "ATM Rating",
    "Preference", "# Pref. Rates", "Performance", "# Perf. Rates","Alarm Systems", "Alarm Systems Rating","Adjustable Bed Repair", "Adjustable Bed Repair Rating",  "Audio / Video (A/V)", "Audio / Video (A/V) Rating", "Apple brand PCs/Laptops Rating", "ATM", "ATM Rating", "CAT5 Cabling", "CAT5 Cabling Rating", "CCTV / Surveillance", "CCTV / Surveillance Rating", "Construction", "Construction Rating", "Copiers Rating", "PC - Desktop / Laptop", "PC - Desktop / Laptop Rating", "Digital Signage/Flat Panel", "Digital Signage/Flat Panel Rating", "DSL/DMARC/T1", "DSL/DMARC/T1 Rating", "Electrical Wiring", "Electrical Wiring Rating", "Electrician - Licensed/Certified", "Electrician - Licensed/Certified Rating", "Electro-Mechanical Binding Eq.", "Enterprise Storage", "Enterprise Storage Rating", "Fiber Cabling", "Fiber Cabling Rating", "Flat Panel TV", "Flat Panel TV Rating", "General Wiring", "General Wiring Rating", "Home Networking", "Home Networking Rating", "Kiosk", "Kiosk Rating", "Low Voltage", "Low Voltage Rating", "Mobile Devices", "Mobile Devices Rating", "Point of Sale", "Point of Sale Rating", "Printers / Copiers", "Printers Rating", //13675, 13875,14121
    "RFID", "RFID Rating", "Routers / Switches", "Routers / Switches Rating", "Safes/Security Cabinets", "Satellite Install", "Satellite Install Rating","Scanners", "Scanners Rating", "Server Hardware", "Server Hardware Rating", "Server Software", "Server Software Rating", "Site Survey", "Site Survey Rating", "Telephony - Non-VoIP", "Telephony - Non-VoIP Rating", "Telephony - VoIP", "Telephony - VoIP Rating", "Wireless Networking", "Wireless Networking Rating", "Residential", "Residential Rating", "Commercial", "Commercial Rating", "Government", "Government Rating",
	"Private", "W2_Tech", "Dispatch", "Pay Company", "ISO_Affiliation", "Deactivated", "Deactivate_Reason");
//13735
	$columns = array("TechID", "FirstName", "LastName", excludeBasedOnDisplayOption("UserName",2), excludeBasedOnDisplayOption("PrimaryPhone",2), "PrimaryEmail", excludeBasedOnDisplayOption("SecondaryEmail",2), excludeBasedOnDisplayOption("City",2), excludeBasedOnDisplayOption("State",2), excludeBasedOnDisplayOption("ZipCode",2), excludeBasedOnDisplayOption("Country",2), excludeBasedOnDisplayOption("Password",2), excludeBasedOnDisplayOption("RegDate",2), excludeBasedOnDisplayOption("W9_Date_Rec",2), excludeBasedOnDisplayOption("Date_TermsAccepted", 2), excludeBasedOnDisplayOption("FLSID",2), excludeBasedOnDisplayOption("FLSstatus",2), excludeBasedOnDisplayOption("FLSCSP_Rec",2), excludeBasedOnDisplayOption("Bg_Test_ReqDate_Lite",2), excludeBasedOnDisplayOption("BG_Test_ResultsDate_Lite",2), excludeBasedOnDisplayOption("Qty_IMAC_Calls",2), excludeBasedOnDisplayOption("Qty_FLS_Service_Calls",2), excludeBasedOnDisplayOption("No_Shows",2), excludeBasedOnDisplayOption(new Zend_Db_Expr("cast(Skills as char($text_field_length))"),1), "DellCert", "MCSE", "CCNA", "APlus", new Zend_Db_Expr("cast(Certifications as char($text_field_length))"),  excludeBasedOnDisplayOption(new Zend_Db_Expr("cast(SpecificExperience as char($text_field_length))"),2), excludeBasedOnDisplayOption($resumeClause,2),
	//"cablingYN", "cablingSelfRating", "electricalYN", "electricalSelfRating", "constructionYN", "constructionSelfRating", "desktopYN", "desktopSelfRating", "telephonyYN", "telephonySelfRating", "networkingYN", "networkingSelfRating", "routersYN", "routersSelfRating", "electronicsYN", "electronicsSelfRating", "posYN", "posSelfRating", "atmYN", "atmSelfRating",

	excludeBasedOnDisplayOption("electronicsYN", 1), excludeBasedOnDisplayOption("electronicsSelfRating", 1), 
            excludeBasedOnDisplayOption("atmYN", 1), excludeBasedOnDisplayOption("atmSelfRating", 1),
            excludeBasedOnDisplayOption("cablingYN", 1), excludeBasedOnDisplayOption("cablingSelfRating", 1), excludeBasedOnDisplayOption("CCTVYN", 1), excludeBasedOnDisplayOption("CCTVSelfRating", 1), excludeBasedOnDisplayOption("constructionYN", 1), excludeBasedOnDisplayOption("constructionSelfRating", 1), excludeBasedOnDisplayOption("desktopYN", 1), excludeBasedOnDisplayOption("desktopSelfRating", 1), excludeBasedOnDisplayOption("DigitalSignageYN", 1), excludeBasedOnDisplayOption("DigitalSignageSelfRating", 1), excludeBasedOnDisplayOption("DslYN", 1), excludeBasedOnDisplayOption("DslSelfRating", 1), excludeBasedOnDisplayOption("electricalYN", 1), excludeBasedOnDisplayOption("electricalSelfRating", 1), excludeBasedOnDisplayOption("FiberCablingYN", 1), excludeBasedOnDisplayOption("FiberCablingSelfRating", 1),excludeBasedOnDisplayOption("GeneralWiringYN", 1), excludeBasedOnDisplayOption("GeneralWiringSelfRating", 1), excludeBasedOnDisplayOption("networkingYN", 1), excludeBasedOnDisplayOption("networkingSelfRating", 1),
    excludeBasedOnDisplayOption("KioskYN", 1), excludeBasedOnDisplayOption("KioskSelfRating", 1), excludeBasedOnDisplayOption("LowVoltageYN", 1), excludeBasedOnDisplayOption("LowVoltageSelfRating", 1), excludeBasedOnDisplayOption("posYN", 1), excludeBasedOnDisplayOption("posSelfRating", 1), excludeBasedOnDisplayOption("PrintersYN", 1), excludeBasedOnDisplayOption("PrintersSelfRating", 1), excludeBasedOnDisplayOption("RFIDYN", 1), excludeBasedOnDisplayOption("RFIDSelfRating", 1), excludeBasedOnDisplayOption("routersYN", 1), excludeBasedOnDisplayOption("routersSelfRating", 1), excludeBasedOnDisplayOption("SatelliteYN", 1), excludeBasedOnDisplayOption("SatelliteSelfRating", 1), excludeBasedOnDisplayOption("ScannersRepairYN", 1), excludeBasedOnDisplayOption("ScannersSelfRating", 1), excludeBasedOnDisplayOption("ServersYN", 1), excludeBasedOnDisplayOption("ServersSelfRating", 1), excludeBasedOnDisplayOption("ServerSoftwareYN", 1), excludeBasedOnDisplayOption("ServerSoftwareSelfRating", 1), excludeBasedOnDisplayOption("SiteSurveyYN", 1), excludeBasedOnDisplayOption("SiteSurveySelfRating", 1), excludeBasedOnDisplayOption("telephonyYN", 1), excludeBasedOnDisplayOption("telephonySelfRating", 1), excludeBasedOnDisplayOption("TelephonyVoIPYN", 1), excludeBasedOnDisplayOption("TelephonyVoIPSelfRating", 1), excludeBasedOnDisplayOption("WirelessYN", 1), excludeBasedOnDisplayOption("WirelessSelfRating", 1), excludeBasedOnDisplayOption("Residential", 1), excludeBasedOnDisplayOption("ResidentialSelfRating", 1), excludeBasedOnDisplayOption("Commercial", 1), excludeBasedOnDisplayOption("CommercialSelfRating", 1), excludeBasedOnDisplayOption("Government", 1), excludeBasedOnDisplayOption("GovernmentSelfRating", 1),
	excludeBasedOnDisplayOption("compTIA", 1), excludeBasedOnDisplayOption($clientDeniedSelect, 1), excludeBasedOnDisplayOption($dateClause, 1), "AcceptTerms", 
excludeBasedOnDisplayOption("FS_Cert_Test_Pass", 1), excludeBasedOnDisplayOption("FLSCSP_RecDate", 1), excludeBasedOnDisplayOption("TechSource", 1), excludeBasedOnDisplayOption("FS_Cert_Date_Taken", 1), "Private", "W2_Tech", "Dispatch", "Pay_Company", "ISO_Affiliation", "Deactivated", "Deactivate_Reason", "PreferencePercent", "SATRecommendedTotal", "PerformancePercent", "SATPerformanceTotal", "Back_Outs", excludeBasedOnDisplayOption("appleSelfRating", 1), excludeBasedOnDisplayOption("CopiersSelfRating", 1), excludeBasedOnDisplayOption("electroMechSelfRating", 1), excludeBasedOnDisplayOption("savesSecCabinetsSelfRating", 1), excludeBasedOnDisplayOption("BG_Test_ResultsDate_Full",2), excludeBasedOnDisplayOption($I9StatusClause, 1),excludeBasedOnDisplayOption($I9DateClause, 1),excludeBasedOnDisplayOption("EnterpriseStorage", 1), excludeBasedOnDisplayOption("EnterpriseStorageSelfRating", 1), excludeBasedOnDisplayOption("MobileDevices", 1), excludeBasedOnDisplayOption("MobileDevicesSelfRating", 1),excludeBasedOnDisplayOption("DatePassDrug",2), excludeBasedOnDisplayOption($DrugPassedClause,2), excludeBasedOnDisplayOption('FSPlusOneReg',2), excludeBasedOnDisplayOption('FSPlusOneDateReg',2),  excludeBasedOnDisplayOption("FlatPanelTVYN", 1), excludeBasedOnDisplayOption("FlatPanelTVSelfRating", 1),
excludeBasedOnDisplayOption("AdjustableBedRepairYN", 1), 
excludeBasedOnDisplayOption("AdjustableBedRepairSelfRating", 1),
excludeBasedOnDisplayOption("DatePassedSilverDrug",2), 
excludeBasedOnDisplayOption($SilverDrugPassedClause,2),
excludeBasedOnDisplayOption("AlarmSystemsYN", 1),    //13978
excludeBasedOnDisplayOption("AlarmSystemsSelfRating", 1),   //13978
	"WMID",
excludeBasedOnDisplayOption("ElectricianCertifiedYN", 1),    //14121
excludeBasedOnDisplayOption("ElectricianCertifiedSelfRating", 1)   //14121

);        //13978

        ////excludeBasedOnDisplayOption("SATRecommendedAvg", 1), excludeBasedOnDisplayOption("SATRecommendedTotal", 1),	excludeBasedOnDisplayOption("SATPerformanceAvg", 1), excludeBasedOnDisplayOption("SATPerformanceTotal", 1), 
	
//	$fields = array_flip($columns);
//	var_dump($fields);
//foreach($columns as $k=>$v){  echo("<br/>$k=>$v"); }//test
	if (isset($_GET['first'])) {
		$results_array = array_slice($gross_results, $first_record-1, $max_records - $first_record + 1);
	} else {
		$results_array = $net_results;
	}
	//echo "<p>SIZEOF RESULTS: " . sizeof($results_array) . "</p>";

	$certSearch = new Core_TechCertifications;
	$certByTech = $certSearch->getTechCertification($results_array);
	$results = queryTechsFromList($columns, $results_array, "$sortBy $sortDir");
	$lastWORunByTech = array();
	if ($DisplayOption == 0) {
		// mysql query for last wo run stat
//		echo "SELECT TechID, $lastWORunMainClause AS lastWORunMain, $lastWORunFLSClause AS lastWORunFLS, $lastWORunAnyClause AS lastWORunAny FROM TechBankInfo AS t WHERE TechID IN (" . implode(",", $results_array)  . ")";
		$db = Zend_Registry::get('DB');
//		echo "SELECT TechID, $lastWORunMainClause AS lastWORunMain, $lastWORunFLSClause AS lastWORunFLS, $lastWOBidMainClause AS lastWOBidMain, $lastWOBidFLSClause AS lastWOBidFLSClause FROM TechBankInfo AS t WHERE TechID IN (" . implode(",", $results_array)  . ")";
		$mysqlResult = $db->fetchAll("SELECT TechID, $lastWORunMainClause AS lastWORunMain, $lastWORunFLSClause AS lastWORunFLS, $lastWOBidMainClause AS lastWOBidMain, $lastWOBidFLSClause AS lastWOBidFLSClause FROM TechBankInfo AS t WHERE TechID IN (" . implode(",", $results_array)  . ")");
//		$mysqlResult = array();

		foreach ($mysqlResult as $val) {
			$lastWORunByTech[$val["TechID"]] = array("main" => $val["lastWORunMain"], "FLS" => $val["lastWORunFLS"]);
			$lastWOBidByTech[$val["TechID"]] = array("main" => $val["lastWOBidMain"], "FLS" => $val["lastWOBidMain"]);
		}
	}

	$net_result_count = sizeof($results);
	//echo $net_result_count;



if (!isset($_GET['first'])) {


// ==================================================================
// DISPLAY SEARCH RESULTS


	echo "<div id='results'>";

?>
<script type="text/javascript">
	$(document).ready(function () {
		$("#DisplayOption").change(function() {
			document.location.replace("<?=$currentURL?>&DisplayOption=" + $(this).val());
		});
	});
</script>

<h1>Advanced Tech Search</h1>
<?php
	$column_count = 142;    //13978,14121
?>
<dl>
	<dd class="techid">
    	<?php displayOptionDropDown();?>
    </dd>
</dl>
		<table cellpadding='10' class="resultTable">
			<tr class="resultTop"><td colspan="<?= $column_count ?>" valign="left"><input id='downloadButton' style="float:left" class='link_button download_button' type='button' onclick="document.location = '<?=$_SERVER['PHP_SELF']?>?download&amp;s=<?= rand(1,1000)?>'" value='download'/></td></tr>
			<tr class="resultHeader">

				<td class="leftHeader"><?= columnHeading('TechID') ?></td>
        <td><?= columnHeading('First Name') ?></td>
				<td><?= columnHeading('Last Name') ?></td>
				<td><?= columnHeading('UserName') ?></td>
				<td><?= columnHeading('Pri Phone', 'PrimaryPhone') ?></td>
				<td><?= columnHeading('Pri Email', 'PrimaryEmail') ?></td>
				<td><?= columnHeading('Sec Email', 'SecondaryEmail') ?></td>
				<td><?= columnHeading('City') ?></td>
				<td><?= columnHeading('State') ?></td>
				<td><?= columnHeading('ZipCode') ?></td>
                <td><?= columnHeading('Country') ?></td>                
				<td><?= columnHeading('Password') ?></td>
				<td><?= columnHeading('Registration Date', "RegDate") ?></td>
				<td><?= columnHeading('W9', 'W9_Date_Rec') ?></td>
				<td><?= columnHeading('Tech Source', 'TechSource') ?></td> <!--14145--> 
                                <td><?= columnHeading('FS PlusOne', 'FSPlusOneDateReg') ?></td>
        <td><?= columnHeading('Accept ICA', 'AcceptTerms') ?></td>
				<td><?= columnHeading('Accept ICA Date', 'Date_TermsAccepted') ?></td>
				<td><?= columnHeading('Work Market ID#', 'WMID') ?></td>
				<td><?= columnHeading('FLS ID', 'FLSID') ?></td>
				<td><?= columnHeading('FLS Status', 'FLSstatus') ?></td>
				<td><?= columnHeading('FLS CSP', 'FLSCSP_Rec') ?></td>
				<td><?= columnHeading('FLS CSP Date', 'FLSCSP_RecDate') ?></td>
				<td><?= columnHeading('FS Cert', 'FS_Cert_Test_Pass') ?></td>
				<td><?= columnHeading('FS Cert Date', 'FS_Cert_Date_Taken') ?></td>
				<td><?= columnHeading('BC Req', 'Bg_Test_ReqDate_Lite') ?></td>
				<td><?= columnHeading('BC Pass', 'BG_Test_ResultsDate_Lite') ?></td>
				<td><?= columnHeading('Comp BC Pass', 'BG_Test_ResultsDate_Full') ?></td>
                                <td><?= columnHeading('Gold Drug Status', 'DrugPassed') ?></td>
                                <td><?= columnHeading('Gold Drug Date', 'DatePassDrug') ?></td>
                                <!--13700 -->
                                <td><?= columnHeading('Silver Drug Status', 'SilverDrugPassed') ?></td>
                                <td><?= columnHeading('Silver Drug Date', 'DatePassedSilverDrug') ?></td>
                                <!--end 13700 -->
		        <td><?= columnHeading('US Verified') ?></td>
				<td><?= columnHeading('DeVry') ?></td>
				<td><?= columnHeading('Tech Boot Camp') ?></td>
        <td><?= columnHeading('Logged On Date') ?></td>
        <td><?= columnHeading('Last WO Bid') ?></td>
        <td><?= columnHeading('Last WO Run') ?></td>
        <td><?= columnHeading('Last WO Bid (FLS)', 'LastWOBidFLS') ?></td>
        <td><?= columnHeading('Last WO Run (FLS)', 'LastWORunFLS') ?></td>
        <td><?= columnHeading('Deactivated') ?></td>
        <td><?= columnHeading('Deactivated Reason') ?></td>
        <td><?= columnHeading('Client Denied') ?></td>
				<td><?= columnHeading('IMACs', 'Qty_IMAC_Calls') ?></td>
				<td><?= columnHeading('Service Calls', 'Qty_FLS_Service_Calls') ?></td>
				<td><?= columnHeading('No Shows') ?></td>
				<td><?= columnHeading('Back Outs') ?></td>
				<td width='80'><?= columnHeading('Skills') ?></td>
				<td width='80'><?= columnHeading('Certifications', 'DellCert') ?></td>
				<td><?= columnHeading('MCSE') ?></td>
				<td><?= columnHeading('CCNA') ?></td>
				<td><?= columnHeading('A+') ?></td>
                                
                                <td><?= columnHeading('A+ Cert#') ?></td>
                                
                                
				<td><?= columnHeading('CompTIA') ?></td>
                                
                                <td><?= columnHeading('PDI+') ?></td>
                                <td><?= columnHeading('Server+') ?></td>
                                <td><?= columnHeading('Network+') ?></td>
                                
				<td width='80'><?= columnHeading('Other Certs') ?></td>
				<td width='80'><?= columnHeading('Experience') ?></td>

				<td width='80'><?= columnHeading('Preference') ?></td>
				<td width='80'><?= columnHeading('# Pref. Ratings') ?></td>
				<td width='80'><?= columnHeading('Performance') ?></td>
				<td width='80'><?= columnHeading('# Perf. Ratings') ?></td>
<?= skillColumnHeading("Alarm Systems", "AlarmSystemsYN", "AlarmSystemsSelfRating"); ?> <!--13978 -->
        <?= skillColumnHeading("Adjustable Bed Repair", "AdjustableBedRepairYN", "AdjustableBedRepairSelfRating"); ?>
        <?= skillColumnHeading("Audio / Video (A/V)", "electronicsYN", "electronicsSelfRating"); ?>
		<td><?= columnHeading('Apple brand PCs/Laptops', 'appleSelfRatings') ?></td>
        <?= skillColumnHeading("ATM"); ?>
        <?= skillColumnHeading("CAT5 Cabling", "cablingYN", "cablingSelfRating"); ?>
        <?= skillColumnHeading("CCTV / Surveillance", "CCTVYN", "CCTVSelfRating"); ?>
        <?= skillColumnHeading("Construction"); ?>
        <td><?= columnHeading('Copier Rating', 'CopiersSelfRating') ?></td>
        <?= skillColumnHeading("PC - Desktop / Laptop", "desktopYN", "desktopSelfRating"); ?>  <!--13675-->
        <?= skillColumnHeading("Digital Signage/Flat Panel", "DigitalSignageYN", "DigitalSignageSelfRating"); ?>
        <?= skillColumnHeading("DSL/DMARC/T1", "DslYN", "DslSelfRating"); ?> <!--13875-->
        <?= skillColumnHeading("Electrical Wiring", "electricalYN", "electricalSelfRating"); ?>
        <?= skillColumnHeading("Electrician - Licensed/Certified", "ElectricianCertifiedYN", "ElectricianCertifiedSelfRating"); ?><!--14121-->
		<td><?= columnHeading('Electro-Mechanical Binding Eq.', 'electroMechSelfRatings') ?></td>
		<?= skillColumnHeading("Enterprise Storage", "EnterpriseStorage", "EnterpriseStorageSelfRating") ?>
        <?= skillColumnHeading("Fiber Cabling", "FiberCablingYN", "FiberCablingSelfRating"); ?>
        <?= skillColumnHeading("Flat Panel TV", "FlatPanelTVYN", "Flat Panel TVSelfRating"); ?>
        <?= skillColumnHeading("General Wiring", "GeneralWiringYN", "GeneralWiringSelfRating") ?>
        <?= skillColumnHeading("Home Networking", "networkingYN", "networkingSelfRating") ?>
        <?= skillColumnHeading("Kiosk", "KioskYN", "KioskSelfRating") ?>
        <?= skillColumnHeading("Low Voltage", "LowVoltageYN", "LowVoltageSelfRating") ?>
        <?= skillColumnHeading("Mobile Devices", "MobileDevices", "MobileDevicesSelfRating") ?>
        <?= skillColumnHeading("Point of Sale", "posYN", "posSelfRating") ?>
        <?= skillColumnHeading("Printers", "PrintersYN", "PrintersSelfRating") ?>
        <?= skillColumnHeading("RFID", "RFIDYN", "RFIDSelfRating") ?>
        <?= skillColumnHeading("Routers / Switches", "routersYN", "routersSelfRating") ?>
        <td><?= columnHeading('Safes/Security Cabinets', 'savesSecCabinetsSelfRating') ?></td>
        <?= skillColumnHeading("Satellite Install", "SatelliteYN", "SatelliteSelfRating") ?>
<!-- 13735 -->
<?= skillColumnHeading("Scanners", "ScannersRepairYN", "ScannersSelfRating") ?>
<!-- end 13735 -->
        <?= skillColumnHeading("Server Hardware", "ServersYN", "ServersSelfRating") ?>
        <?= skillColumnHeading("Server Software", "ServerSoftwareYN", "ServerSoftwareSelfRating") ?>
        <?= skillColumnHeading("Site Survey", "SiteSurveyYN", "SiteSurveySelfRating") ?>
        <?= skillColumnHeading("Telephony - Non-VoIP", "telephonyYN", "telephonySelfRating") ?>
        <?= skillColumnHeading("Telephony - VoIP", "TelephonyVoIPYN", "TelephonyVoIPSelfRating") ?>
        <?= skillColumnHeading("Wireless Networking", "WirelessYN", "WirelessSelfRating") ?>
        <?= skillColumnHeading("Residential", "ResidentialYN", "ResidentialSelfRating") ?>
        <?= skillColumnHeading("Commercial", "CommercialYN", "CommercialSelfRating") ?>
        <?= skillColumnHeading("Government", "GovernmentYN", "GovernmentSelfRating") ?>

		<td><?= columnHeading('Private') ?></td>
        <td><?= columnHeading('W2 Tech') ?></td>
        <td><?= columnHeading('Dispatch') ?></td>
        <td><?= columnHeading('Payee','ISO_Affiliation') ?></td>
				<td class="rightHeader"><?= columnHeading('Resume'); ?></td>
			</tr>

<?php
} // of (isset GET first)

if (isset($_GET['first'])) {
	// CREATE HEADER AND OPEN CSV FILE
	header("Content-type: text/csv");
	header("Content-disposition: attachment; filename=$csv_filename");
	$fp = fopen("/tmp/$csv_filename", 'w');

	fputcsv($fp, $csv_columns);
	//echo implode(", ", $csv_columns) . "<br />";
}

	$str_length = 80;

	if (isset($_GET['first'])) {
		$str_length = 262;
	} else {
		$str_length = 80;
	}

	// PARSE QUERY RESULTS INTO FIELDS
 
	foreach ($results as $result) {

		$parts = $result;
		$techID = process($parts[0]);
		$firstName = process($parts[1]);
		$lastName = process($parts[2]);
		$userName = process($parts[3]);
		$priPhone = process($parts[4]);
		$priEmail = process($parts[5]);
		$secEmail = process($parts[6]);
		$city = process($parts[7]);
		$state = process($parts[8]);
		$zipCode = process($parts[9]);
        $country = process($parts[10]);
                $StatusPassDrug  = process($parts[123]);
                $DatePassDrug  = format_date(process($parts[122]));
                /*13700*/
                $StatusPassSilverDrug  = process($parts[131]);
                $DatePassSilverDrug  = format_date(process($parts[130]));
                /*end 13700*/
		$password = process($parts[11]);
		$regDate = format_date(process($parts[12]));
                //issue_13085
                $FSPlusOneReg = process($parts[124]);
                if($FSPlusOneReg == 1)
                {
                    $FSPlusOneDateReg =  format_date(process($parts[125]));
                }
                else
                {
                    $FSPlusOneDateReg =  "";
                }
                //end issue_13085
		$w9Info = Core_Tech::getW9Info($techID);
        $w9 = format_date($w9Info['DateChanged']);
		//$w9 = format_date(process($parts[13]));
		$acceptTermsDate = format_date(process($parts[14]));
		$flsID = process($parts[15]);
		$flsStatus = process($parts[16]);
	  	$flsCSP = process($parts[17]);
	  	$bgReq = format_date(process($parts[18]));
		$bgPass = format_date(process($parts[19]));
	  	$IMACs = process($parts[20]);
	 	$svcCalls = process($parts[21]);
	 	$noShows = process($parts[22]);
	  	$skillsField = str_truncate(process($parts[23]), $str_length);
	  	$dellCerts = process($parts[24]);
	  	$MCSE = process($parts[25]);
	  	$CCNA = process($parts[26]);
	  	$APlus = process($parts[27]);
	  	$certifications = str_truncate(process($parts[28]), $str_length);
	  	$experience = str_truncate(process($parts[29]), $str_length);

	  	$resume = process($parts[30]);

		$skills = array();
        
        $skills[] = array(process($parts[130]), process($parts[131]));//13978
        $skills[] = array(process($parts[126]), process($parts[127]));//13978
		$skills[] = array(process($parts[31]), process($parts[32]));
		$skills[] = process($parts[111]);
		$skills[] = array(process($parts[33]), process($parts[34]));
		$skills[] = array(process($parts[35]), process($parts[36]));
		$skills[] = array(process($parts[37]), process($parts[38]));
		$skills[] = array(process($parts[39]), process($parts[40]));
		$skills[] = process($parts[112]);
		$skills[] = array(process($parts[41]), process($parts[42]));
		$skills[] = array(process($parts[43]), process($parts[44]));
		$skills[] = array(process($parts[45]), process($parts[46]));
		$skills[] = array(process($parts[47]), process($parts[48]));
        $skills[] = array(process($parts[132]), process($parts[133])); //14121
		$skills[] = process($parts[113]);
        $skills[] = array(process($parts[118]), process($parts[119]));
		$skills[] = array(process($parts[49]), process($parts[50]));
        $skills[] = array(process($parts[126]), process($parts[127]));
		$skills[] = array(process($parts[51]), process($parts[52]));
		$skills[] = array(process($parts[53]), process($parts[54]));
		$skills[] = array(process($parts[55]), process($parts[56]));
		$skills[] = array(process($parts[57]), process($parts[58]));
        $skills[] = array(process($parts[120]), process($parts[121]));
		$skills[] = array(process($parts[59]), process($parts[60]));
		$skills[] = array(process($parts[61]), process($parts[62]));
		$skills[] = array(process($parts[63]), process($parts[64]));
		$skills[] = array(process($parts[65]), process($parts[66]));
		$skills[] = process($parts[114]);
		$skills[] = array(process($parts[67]), process($parts[68]));
		$skills[] = array(process($parts[69]), process($parts[70]));
		$skills[] = array(process($parts[71]), process($parts[72]));
		$skills[] = array(process($parts[73]), process($parts[74]));
		$skills[] = array(process($parts[75]), process($parts[76]));
		$skills[] = array(process($parts[77]), process($parts[78]));
		$skills[] = array(process($parts[79]), process($parts[80]));
		$skills[] = array(process($parts[81]), process($parts[82]));
		$skills[] = array(process($parts[83]), process($parts[84]));
		$skills[] = array(process($parts[85]), process($parts[86]));
        $skills[] = array(process($parts[87]), process($parts[88]));
        $more = 2;
                
		$compTIA = process($parts[87+$more]);
		$client_denied = process($parts[88+$more]);
		$loggedOnDate = format_date(process($parts[89+$more]));
		$acceptTerms = process($parts[90+$more]);

//		$lastWORun = format_date(process($parts[88]));
//		$lastWORunFLS = format_date(process($parts[89]));
		$lastWORun = format_date($lastWORunByTech[$techID]["main"]);
		$lastWORunFLS = format_date($lastWORunByTech[$techID]["FLS"]);

//		$lastWOBid = format_date(process($parts[90]));
//		$lastWOBidFLS = format_date(process($parts[91]));
		$lastWOBid = format_date($lastWOBidByTech[$techID]["main"]);
		$lastWOBidFLS = format_date($lastWOBidByTech[$techID]["FLS"]);
		$fsCert = process($parts[95+$more]);
		$flsCSPDate = format_date(process($parts[96+$more]));
		$referredBy = process($parts[95]);//14145
		$fsCertDate = format_date(process($parts[98+$more]));

		$private = process($parts[99+$more]);
		$w2 = process($parts[100+$more]);
		$dispatch = process($parts[101+$more]);
		$Pay_Company = process($parts[102+$more]);
		$payee = process($parts[103+$more]);

		//$EmailContactOptOut = process($parts[103]);
		//$AllowText = process($parts[104]);
		//$Deactivated = process($parts[104+$more]);
		//$DeactivatedReason = process($parts[105+$more]);
		$Deactivated = ($parts[102]);//14035
		$DeactivatedReason = ($parts[103]);//14035
		
		$preferance = number_format(process($parts[106+$more]),0) . "%";
		$numPrefRatings = process($parts[107+$more]);
		$performance = number_format(process($parts[108+$more]),0) . "%";
		$numPerfRatings = process($parts[109+$more]);
		$backOuts = process($parts[110+$more]);
		$bgPassFull = format_date(process($parts[115+$more]));
		if (!empty($certByTech[$techID][2]))
			$devry = $certByTech[$techID][2]->getDate();
		else
			$devry = NULL;
		if (!empty($certByTech[$techID][3]))
			$tbc = $certByTech[$techID][3]->getDate();
		else
			$tbc = NULL;
		if (!empty($devry)) 
			$devry = $devry->toString('mm/dd/YY');
		else
			$devry = NULL;
		if (!empty($tbc)) 
			$tbc = $tbc->toString('mm/dd/YY');
		else
			$tbc = NULL;

	    $_i9Status = $parts[116+$more];
        $_i9Date = $parts[117+$more];
        $usAuthorized="";
        if($_i9Status==1){$usAuthorized='Pending';}
        if($_i9Status==2){$usAuthorized=format_date($_i9Date);}
        if(!empty($certByTech[$techID][45])){
            $ACert = $certByTech[$techID][45]->getNumber();
        }
        
        if(!empty($certByTech[$techID][36])){
            $PDI = $certByTech[$techID][36]->getNumber();
        }
        
        if(!empty($certByTech[$techID][37])){
            $Server = $certByTech[$techID][37]->getNumber();
        }
		
        if(!empty($certByTech[$techID][38])){
            $Network = $certByTech[$techID][38]->getNumber();
        }

		$DeactivatedReason = getReasonNameById($DeactivatedReason);//14035	

		$wmid = $parts[132];

	if (!isset($_GET['first'])) {
		// IF IT'S NOT A DOWNLOAD, DISPLAY THE RESULTS

		echo "<tr class='resultRow'>";
		echo "<td class='leftRow'><a href='/admin/wosViewTechProfile.php?TechID=$techID'>$techID</a></td>";
		echo "<td>$firstName</td>";
		echo "<td>$lastName</td>";
		echo "<td>$userName</td>";
		echo "<td>$priPhone</td>";
		echo "<td>$priEmail</td>";
		echo "<td>$secEmail</td>";
		echo "<td>$city</td>";
		echo "<td>$state</td>";
		echo "<td>$zipCode</td>";
        echo "<td>$country</td>";
		echo "<td>$password</td>";
		echo "<td>$regDate</td>";
		echo "<td>$w9</td>";
		echo "<td>$referredBy</td>";
                //issue_13085
                echo "<td>$FSPlusOneDateReg</td>";
                //end issue_13085
		echo "<td>$acceptTerms</td>";
		echo "<td>$acceptTermsDate</td>";
		echo "<td>$wmid</td>";
		echo "<td>$flsID</td>";
		echo "<td>$flsStatus</td>";
		echo "<td>$flsCSP</td>";
		echo "<td>$flsCSPDate</td>";
		echo "<td>$fsCert</td>";
		echo "<td>$fsCertDate</td>";
		echo "<td>$bgReq</td>";
		echo "<td>$bgPass</td>";
		echo "<td>$bgPassFull</td>";
                echo "<td>$StatusPassDrug</td>";
                echo "<td>$DatePassDrug</td>";
                /*13700*/
                 echo "<td>$StatusPassSilverDrug</td>";
                echo "<td>$DatePassSilverDrug</td>";
                /*end 13700*/
        echo "<td>$usAuthorized</td>";        
		echo "<td>$devry</td>";
		echo "<td>$tbc</td>";
		echo "<td>$loggedOnDate</td>";
		echo "<td>$lastWOBid</td>";
		echo "<td>$lastWORun</td>";
		echo "<td>$lastWOBidFLS</td>";
		echo "<td>$lastWORunFLS</td>";
		echo "<td>$Deactivated</td>";
		echo "<td>$DeactivatedReason</td>";
		echo "<td>$client_denied</td>";
		echo "<td>$IMACs</td>";
		echo "<td>$svcCalls</td>";
		echo "<td>$noShows</td>";
		echo "<td>$backOuts</td>";
		echo "<td width='80'><nobr>$skillsField</nobr></td>";
		echo "<td>$dellCerts</td>";
		echo "<td>$MCSE</td>";
		echo "<td>$CCNA</td>";
		echo "<td>$APlus</td>";
                
                echo "<td>$ACert</td>";
                
		echo "<td>$compTIA</td>";
                
                echo "<td>$PDI</td>";
                echo "<td>$Server</td>";
                echo "<td>$Network</td>";
                
		echo "<td width='80'><nobr>$certifications</nobr></td>";
		echo "<td width='80'><nobr>$experience</nobr></td>";

		echo "<td width='80'>$preferance</td>";
		echo "<td width='80'>$numPrefRatings</td>";
		echo "<td width='80'>$performance</td>";
		echo "<td width='80'>$numPerfRatings</td>";

		foreach ($skills as $skill) {
			if (is_array($skill)) {
				echo "<td>" . $skill[0] . "</td>";
				echo "<td>" . $skill[1] . "</td>";
			}
			else
				echo "<td>" . $skill . "</td>";
		}


		echo "<td>$private</td>";
		echo "<td>$w2</td>";
		echo "<td>$dispatch</td>";
		echo "<td>$payee</td>";

		echo "<td class='rightRow'>$resume</td>";
		echo "</tr>";
	} else {
		// WRITE THE RESULTS TO CSV FILE

		$formattedFields = array($techID, $firstName, $lastName, $userName, $priEmail, $secEmail, $priPhone, $city, $state, $zipCode,$country, $password, $regDate, $w9, $referredBy, $FSPlusOneDateReg, $acceptTerms, $acceptTermsDate, $WMID, $flsID, $flsStatus, $flsCSP, $flsCSPDate, $fsCert, $fsCertDate, $bgReq, $bgPass, $bgPassFull, $usAuthorized, $devry, $tbc, $StatusPassDrug, $DatePassDrug,$StatusPassSilverDrug ,$DatePassSilverDrug,$IMACs, $svcCalls, $noShows, $backOuts, $skillsField, $dellCerts, $MCSE, $CCNA, $APlus,$ACert, $compTIA,$PDI,$Server,$Network, $certifications, $experience, $resume, $client_denied, $loggedOnDate, $lastWORun, $lastWORunFLS, $lastWOBid, $lastWOBidFLS, $preferance, $numPrefRatings, $performance, $numPerfRatings
		//, $private, $w2, $dispatch, $payee
		);
	foreach ($skills as $skill) {
			if (is_array($skill))
				$formattedFields = array_merge($formattedFields, $skill);
			else
				$formattedFields[] = $skill;
		}

        $formattedFields[] = $private;
        $formattedFields[] = $w2;
        $formattedFields[] = $dispatch;
        $formattedFields[] = $Pay_Company;
        $formattedFields[] = $payee;
        $formattedFields[] = $Deactivated;
        $formattedFields[] = $DeactivatedReason;

		//echo implode(", ", $formattedFields) . "<br /><br />";
		fputcsv($fp, $formattedFields);
	}
}

	if (!isset($_GET['first'])) {
		// IF NOT A DOWNLOAD, DISPLAY PAGINATION
?>
<tr class="pagination">
	<td colspan="10" class="left">
  <?php
		$next_page = $page + 1;
		$prev_page = $page - 1;

		$sort_params = "&sort=$sortByParam&dir=$sortDir";

		if ($page_block > 1) {
			$prev_block = $first_page - $Page_Block_Size;
			echo "<a href='$Current_Page?page=$prev_block$sort_params&t=".rand(444, 1000000)."'><img src='/widgets/js/datatables/images/back_enabled.jpg' /></a>";
		}

		if ($page > 1) {
			echo "<a href='$Current_Page?page=$prev_page$sort_params&t=".rand(444, 1000000)."'><img src='/widgets/js/datatables/images/back_disabled.jpg' /></a>";
		}

		for ($i=$first_page; $i <= $last_page; $i++) {
			if ($page != $i) {
				echo "<a href='$Current_Page?page=$i$sort_params&t=".rand(444, 1000000)."'>$i</a> ";
			} else {
				echo "$i ";
			}
		}

		if ($page < $Max_Page) {
			echo "<a href='$Current_Page?page=$next_page$sort_params&t=".rand(444, 1000000)."'><img src='/widgets/js/datatables/images/forward_disabled.jpg' /></a>";
		}

		if ($page_block < $total_blocks) {
			$next_block = $last_page + 1;
			echo "<a href='$Current_Page?page=$next_block$sort_params&t=".rand(444, 1000000)."'><img src='/widgets/js/datatables/images/forward_enabled.jpg' /></a>";
		}
	?>

  </td>
  <td colspan="<?= $column_count - 10 ?>" class="right"></td>
</tr>
<tr class="resultBottom">
				<td class="left" colspan="10">
        Results <?= $offset + 1 ?> - <?=  $offset + $net_result_count; ?> of <?= $result_count ?>
        </td>
        <td class="right" colspan="<?= $column_count - 10 ?>"></td>
			</tr>
<?php
	echo "</table>";

// ==================================================
// EMAIL BLAST
/*
	$gross_results_list = format_list($gross_results);

	$email_results = queryTechs("PrimaryEmail", "TechID in ($gross_results_list)");
*/
	$email_results_list = implode(", ", $email_results);
	$email_results_list = str_replace("`", "", $email_results_list);
?>

<form id="emailBlast" name="emailBlast" method="post" action="/Email_Form_admin.php" target="_blank">
	<input name="emailList" id="emailList" value="<?= $email_results_list ?>" type="hidden" />
  <input name="vSubmit" id="vSubmit" value="Email Blast" type="submit">

</form>


<?php
// END EMAIL BLAST


		echo "</div>";
		} // end if (!isset GET first)

		if (isset($_GET['first'])) {
			// CLOSE THE CSV FILE AND SEND THE CONTENTS

			fclose($fp);

			echo file_get_contents("/tmp/$csv_filename");

			rm("/tmp/$csv_filename");  // delete the file
		}
	}

}
else
{
?>


<!-- SEARCH CRITERIA -->

<div id="search">
            <?
        $arrayTags = $_SESSION['SearchClientCredentials'];
        unset($_SESSION['SearchClientCredentials']);//13841
        $arrayTagsexists = array();
        foreach ($arrayTags as $tag)
            {
            if (!empty($tag['yes_no']) || !empty($tag['greater_date']) || !empty($tag['less_date']))
            {
                $arrayTagsexists[$tag['fstag_id']] = array( 'yes_no' => $tag['yes_no'], 'greater_date' => $tag['greater_date'], 'less_date' => $tag['less_date']);			
            }
        }
        ?>
<h1>Advanced Tech Search</h1>
<form id="search_form" name="search_form" action="<?= $Current_Page ?>?page=1" method="post">
<!--
  <p class="submit"><input type="submit" /></p>
-->
<div id="atsTop">GENERAL</div>
<div class="col1">
<dl>
	<dt>Display Option</dt>
	<dd class="techid">
    	<?php displayOptionDropDown();?>
    </dd>
</dl>
<dl>
  <dt>Tech ID</dt>
	<dd class="techid"><input name="TechID" id="TechID" /></dd>

  <dt>Last Name</dt>
	<dd class="lastname"><input name="LastName" id="LastName" /></dd>
  <dt>First Name</dt>
	<dd class="firstname"><input name="FirstName" id="FirstName" /></dd>
  <dt>Username</dt>
	<dd class="username"><input name="Username" id="Username" /></dd>
  <dt>Password</dt>
    <dd class="techid"><input name="Password" id="Password" /></dd>
</dl>
<dl>
  <dt>Email</dt>
	<dd class="email"><select name="EmailType">
      <option selected="selected">Any</option>
    	<option>Primary</option>
      <option>Secondary</option>
    </select></dd>

	<dd class="email">
    <input name="Email" id="Email" /></dd>

  <dt>City</dt>
	<dd class="city"><input name="City" id="City" /></dd>
  <dt>State</dt>
	<dd class="state"><select name="State" id="State" />
  <option value="">Select State</option>

  <?php
	$common = new Core_Api_CommonClass;
	$states = $common->getStatesArray('US');		

	foreach ($states->data as $code => $name) {
		echo "<option value=\"$code\">$name</option>";
	}
		
	?>
  </select>  </dd>
  <dt>Country</dt>
    <dd class="techid"><select name="Country" id="Country" onchange="onChangeCountry()"><?=$countriesHtml?></select></dd>

  <dt>Zip Code</dt>
	<dd class="zipcode">
  	<input name="ZipCode" id="ZipCode" />
    <?= ZipCodeSelect() ?>
  </dd>
  <dt>Prim Phone (contains)</dt>
    <dd class="techid"><input name="PrimaryPhone" id="PrimaryPhone" /></dd>
  <dt>Sec Phone (contains)</dt>
    <dd class="techid"><input name="SecondaryPhone" id="SecondaryPhone" /></dd>
  <dt>SMS (contains)</dt>
    <dd class="techid"><input name="SMS_Number" id="SMS_Number" /></dd>  
  <dt>Work Market Tech</dt>
    <dd class="techid"><?= yesNoSelect("isWMTech"); ?></dd>  
  <dt>Work Market Tech ID#</dt>
    <dd class="techid"><input name="WMID" id="WMID" /></dd>  
</dl>
</div>
<div class="col2">

<!-- DATES -->
<dl>
	<dt>Accept ICA</dt>
  <dd class="accept_terms">
    <select name="AcceptTerms" id="AcceptTerms">
    	<option value="" selected="selected">Any</option>
      <option value="Yes">Yes</option>
      <option value="No">No</option>
      <option value="X">Yes (w/ blank date)</option>
    </select>
    Date <?= dateFields("AcceptTerms") ?>
  </dd>

  <dt>FS Certification</dt>
  <dd class="date">
  	<?= yesNoSelect("FSCert"); ?>
    Date <?= dateFields("FSCert") ?>
  </dd>

<?php 
	$HP_Html = "<option value=\"\">Show All</option><option value=\"Any HP Cert\">Any HP Cert</option><option value=\"HP Cert # HP-Q01\">HP Cert # HP-Q01</option><option value=\"HP Cert # HP-H08\">HP Cert # HP-H08</option>";
?>
<dt>HP Certification</dt>
<dd class="hp_cert">
  <select id="HP_CertNum" name="HP_CertNum" ><?=$HP_Html?></select>
</dd>

<dt>Samsung Certification</dt>
<dd class="date">
	<?= yesNoSelect("SamsungCert"); ?>
</dd>

<dt>Copier Skills</dt>
  <dd class="date">
     <select name="CopierSkills" id="CopierSkills">
     	<option value="">Any&nbsp;&nbsp;</option>
      <option value="Pass">Pass</option>
      <option value="Fail">Fail</option>
      <option value="No">No</option>
     </select>
     Date <?= dateFields("CopierSkills") ?>
  </dd>
  
  <dt>W-9 Submitted</dt>
	<dd class="date">
  	<?= yesNoSelect("W9"); ?>
    Date <?= dateFields("W9_Date_Rec") ?>
	</dd>

  <dt>Basic BC Requested</dt>
  <dd class="date">
		<?= yesNoSelect("Bg_Test_Req_Lite"); ?>
    Date <?= dateFields("BgReq") ?>
  </dd>

  <dt>Basic BC Passed</dt>
  <dd class="date">
     <select name="Bg_Test_Pass_Lite" id="Bg_Test_Pass_Lite">
     	<option value="">Any&nbsp;&nbsp;</option>
      <option value="Pass">Pass</option>
      <option value="Fail">Fail</option>
      <option value="X">Blank</option>
     </select>
     Date <?= dateFields("BgPass") ?>
  </dd>

  <dt>Comprehensive BC Passed</dt>
  <dd class="date">
     <select name="Bg_Test_Pass_Full" id="Bg_Test_Pass_Full">
     	<option value="">Any&nbsp;&nbsp;</option>
      <option value="Pass">Pass</option>
      <option value="Fail">Fail</option>
      <option value="X">Blank</option>
     </select>
     Date <?= dateFields("BgPassFull") ?>
  </dd>

   <dt>Gold Drug Test (9 Panel)</dt><!--13700 -->
 <dd class="date">
     <select name="DrugPassed" id="DrugPassed">
     	<option value="">Any&nbsp;&nbsp;</option>
      <option value="Pass">Pass</option>
      <option value="Fail">Fail</option>
      <option value="X">Blank</option>
     </select>
     Date <?= dateFields("BgDrugPassed") ?>
 </dd>

 <!--13700 -->
  <dt>Silver Drug Test (5 Panel)</dt>
 <dd class="date">
     <select name="SilverDrugPassed" id="SilverDrugPassed">
     	<option value="">Any&nbsp;&nbsp;</option>
      <option value="Pass">Pass</option>
      <option value="Fail">Fail</option>
      <option value="X">Blank</option>
     </select>
     Date <?= dateFields("BgSilverDrugPassed") ?>
 </dd>
 
  <dt>FS-Mobile</dt>
  <dd class="date">
  	<?= yesNoSelect("FSMobile"); ?>
    Date <?= dateFields("FSMobile") ?>
  </dd>
 <!--13932,14056 -->
   
<!--13932,14056 -->
  <dt>Last WO Bid</dt>
  <dd class="date">
  	<select name="LastWOBidType" id="LastWOBidType">

    	<option value="Any">Any</option>
    	<option>FLS</option>
      <option>Main</option>
    </select>
    Date <?= dateFields("LastWOBid") ?>
  </dd>

  <dt>Last WO Run</dt>
  <dd class="date">
  	<select name="LastWORunType" id="LastWORunType">
    	<option value="Any">Any</option>
    	<option>FLS</option>
      <option>Main</option>
    </select>
    Date <?= dateFields("LastWORun") ?>
  </dd>
  
  <dt>FLS CSP</dt>
  <dd class="date">
  	<?= yesNoSelect("FLSCSP_Rec", "FLSCSP_Rec"); ?>
    Date <?= dateFields("FLSCSP_RecDate"); ?>
  </dd>

  <dt>FS-PlusOne</dt>
  <dd class="date">
  	<?= yesNoSelect("FSPlusOneReg", "FSPlusOneReg"); ?>
    Date <?= dateFields("FSPlusOneDateReg"); ?>
  </dd>
    <!--030-->
    <dt>Profile Picture</dt>
    <dd class="date">
        <select name="ProfilePicture" id="ProfilePicture">
            <option value="">Any</option>
            <option value="1">Approved</option>
            <option value="2">Pending</option>
            <option value="3">No</option>
        </select>
    </dd>
    <!--030--> 
  <dt>Registration Date</dt>
	<dd class="date"><?= dateFields("RegDate") ?></dd>

  <dt>Logged On Date </dt>
	<dd class="date"><?= dateFields("LoggedOnDate") ?></dd>
	
  <dt>Tech Source </dt>
	<dd class="">
		<select name="TechSourceFilter" id="TechSourceFilter" />
			<option value="">Any&nbsp;&nbsp;</option>
<?php

if(!empty($techSourceList)){
    foreach($techSourceList as $source){
        echo "<option value=\"$source\">$source</option>";
    }
}
?>            
		</select>
	</dd>
	
</dl>
</div>
<div class="atsHeading"></div>
<div class="col1">
<dl>
  <dt>Push 2 Tech</dt>
  <dd class="w9">
  	<?=yesNoSelect("Push2Tech"); ?>
  </dd>

  <dt>SMS Agree Date</dt>
	<dd class="date"><?= dateFields("SMS_AgreeDate") ?></dd>

  <dt>Tech Source</dt>
  <dd class="tech_source_select">
  	<select name="TechSourceSelect" id="TechSourceSelect">
      <option value="">Any</option>
      <option value="State Job Bank">State Job Bank</option>
      <option value="Beyond.com">Beyond.com</option>
      <option value="Craigslist">Craigslist</option>
      <option value="Dice">Dice</option>
      <option value="Monster">Monster</option>
      <option value="Just Tech Jobs">Just Tech Jobs</option>
      <option value="LinkedIn">LinkedIn</option>
      <option value="AOL Search">AOL Search</option>
      <option value="Google Search">Google Search</option>
      <option value="Yahoo Search">Yahoo Search</option>
      <option value="MSN Search">MSN Search</option>
      <option value="OnForce">OnForce</option>
      <option value="Blog">Blog (please specify)</option>
      <option value="School">School (please specify)</option>
      <option value="Referral">Referral (please specify)</option>
      <option value="Other">Other (please specify)</option>
     </select>
  </dd>

  <dt></dt>
  <dd class="tech_source">
     <input name="TechSource" id="TechSource" />
  </dd>
  
  <dt>Unsubscribe Avail Work Email</dt>
  <dd class="UnsubscribeAvailWorkEmail">
    <select class="yes_no" id="UnsubscribeAvailWorkEmail" name="UnsubscribeAvailWorkEmail">
		<option value="">Any&nbsp;&nbsp;</option>
		<option value="1">Yes</option>
		<option value="0">No</option>
    </select>
  </dd>

  <dt>Unsubscribe Avail Work SMS</dt>
  <dd class="UnsubscribeAvailWorkSMS">
    <select class="yes_no" id="UnsubscribeAvailWorkSMS" name="UnsubscribeAvailWorkSMS">
		<option value="">Any&nbsp;&nbsp;</option>
		<option value="0">Yes</option>
		<option value="1">No</option>
	 </select>
  </dd>

  <dt>Deactivated</dt>
  <dd class="Deactivated">
  	<?= yesNoSelect("Deactivated"); ?>
  </dd>

  <dt>Deactivated Reason</dt>
  <dd class="Deactivate_Reason">
  	<select name="DeactivatedReason" id="DeactivatedReason">
        <option selected="selected" value="">Select one&nbsp;&nbsp;</option>
        <option value="Any">Any</option>
        <option value="">-------------</option>

  <?php  
	$deac_reasons = Core_TechDeactivationReason::getByCode();

	foreach ($deac_reasons as $code => $name) {
		echo "<option value='$name'>$name</option>";
	}
	?>

    </select>
  </dd>
  
  <dt>Client Denied</dt>
  <dd class="client_denied">
  	<select name="ClientDenied" id="ClientDenied">
    	<option selected="selected" value="">Select one&nbsp;&nbsp;</option>
    	<option value="Any">Any Client</option>
      <option value="None">No Clients</option>
      <option value="">-------------</option>

  <?php
	$db = Zend_Registry::get('DB');
	$select = $db->select();
	$select->from(Core_Database::TABLE_CLIENT_DENIED_TECHS, array("Company_ID"))
		->distinct()
		->where("IFNULL(Company_ID,'') <> ''")
		->order(array("Company_ID"));
	$companyList = $db->fetchCol($select);

	foreach ($companyList as $id) {
		echo "<option value='$id'>$id</option>";
	}
	?>

    </select>
  </dd>
  <!--018-->
  <dt>Hide Bids / Hide Tech</dt>
  <dd class="HideBids">
  	<?= yesNoSelect("HideBids"); ?>
  </dd>
  <!--018-->
  <dt>FLS ID</dt>
  <dd class="fls_id">
     <input name="FLSID" id="FLSID" \>
  </dd>

  <dt>FLS Status</dt>
  <dd class="fls_status">
     <select name="FLSstatus" id="FLSstatus">
     	<option value="" selected="selected">Any</option>
      <option value="Admin Denied">Admin Denied</option>
      <option value="Duplicate">Duplicate</option>
      <option value="Hold">Hold</option>
      <option value="Registered">Registered</option>
      <option value="Re-Train">Re-Train</option>
      <option value="Tech Passed">Tech Passed</option>
      <option value="Trained">Trained</option>
    </select>
  </dd>
  <!--14056-->
  <dt>NACI Gov Clearance</dt>
  <dd class="EMCcertifications">
  <?= yesNoSelectnew("NACI"); ?> Date <?= dateFields("NACI") ?>
  </dd> 
  <!--14056-->
    <!--13932-->
  <dt>Interim Gov Clearance</dt>
  <dd class="EMCcertifications">
      <?= yesNoSelectnew("InGovClearance"); ?>
    Date <?= dateFields("InGovClearance") ?>
  </dd>
  <dt>Secret Gov Clearance</dt><!-- 14056-->
  <dd class="EMCcertifications">
      <?= yesNoSelectnew("FullGovClearance"); ?>
    Date <?= dateFields("FullGovClearance") ?>
  </dd>
  <dt>Top Secret Gov Clearance</dt>  <!--14056-->
  <dd class="EMCcertifications">
      <?= yesNoSelectnew("TopGovClearance"); ?>
    Date <?= dateFields("TopGovClearance") ?>
  </dd>
  <!--14056-->
   <dt>Top Secret SCI Gov Clearance</dt>
  <dd class="EMCcertifications">
      <?= yesNoSelectnew("TopSecretSCIGovClearance"); ?>
    Date <?= dateFields("TopSecretSCIGovClearance") ?>
  </dd>
  <!--14056-->
  <!--13932-->
  <dt>Electro-Mechanical Cert</dt>
  <dd class="EMCcertifications">
      <?= yesNoSelect("EMCcertifications"); ?>
    Date <?= dateFields("EMCcertifications") ?>
  </dd>
  <!--624-->
      
      
      <div class ="divcertifications_24" style="display:none;">
          <dt>Computer Plus Certification</dt>
          <dd class="certifications_24">
              <select class="yes_no" id="certifications_24" name="certifications_24">
                  <option value="">Any&nbsp;&nbsp;</option>
                  <option value="1">Pass</option>
                  <option value="-1">Fail</option>
                  <option value="0">No</option>
              </select>
              Date <?= dateFields("certifications_24",6) ?>
          </dd>
      </div>
      <div class ="divcertifications_31" style="display:none;">
  <dt>Craftmatic Certified</dt>
  <dd class="certifications_31">
      <?= yesNoSelect("certifications_31"); ?>
              Date <?= dateFields("certifications_31",6) ?>
  </dd>
      </div>
      <div class ="divcertifications_25" style="display:none;">
          <dt>CSC Deskside Support Tech</dt>
          <dd class="certifications_25">
              <?= yesNoSelect("certifications_25"); ?>
              Date <?= dateFields("certifications_25",6) ?>
  </dd>
      </div>
      <div class ="divcertifications_44" style="display:none;">
          <dt>Endeavor Certified</dt>
          <dd class="EndeavorCertified">
              <?= yesNoSelect("certifications_44"); ?>
              Date <?= dateFields("certifications_44",6) ?>
          </dd>
      </div>
      <div class ="divcertifications_23" style="display:none;">
          <dt>Flextronics Contractor</dt>
          <dd class="FlextronicsContractor">
              <?= yesNoSelect("certifications_23"); ?>
              Date <?= dateFields("certifications_23",6) ?>
          </dd>
      </div>
      <div class ="divcertifications_30" style="display:none;">
          <dt>Flextronics Recruit</dt>
          <dd class="FlextronicsRecruit">
              <?= yesNoSelect("certifications_30"); ?>
              Date <?= dateFields("certifications_30",6) ?>
          </dd>
      </div>
      <div class ="divcertifications_22" style="display:none;">
          <dt>Flextronics Screened</dt>
          <dd class="FlextronicsScreened">
              <?= yesNoSelect("certifications_22"); ?>
              Date <?= dateFields("certifications_22",6) ?>
          </dd>
      </div>
      <!-- 13837-->          
      <div class ="divcertifications_20" style="display:none;">
  <dt>LMS Preferred Technicians</dt>
  <dd class="certifications_20">
      <?= yesNoSelect("certifications_20"); ?>
              Date <?= dateFields("certifications_20",6) ?>
  </dd>
      </div>
      <div class ="divcertifications_21" style="display:none;">
  <dt>LMS+ Key Contractors</dt>
  <dd class="certifications_21">
      <?= yesNoSelect("certifications_21"); ?>
              Date <?= dateFields("certifications_21",6) ?>
  </dd>
      </div>
      <div class ="divcertifications_19" style="display:none;">
          <dt>NCR Badged Technicians</dt>
          <dd class="certifications_19">
              <?= yesNoSelect("certifications_19"); ?>
              Date <?= dateFields("certifications_19",6) ?>
  </dd>
      </div>
      
      <div class ="divcertifications_26" style="display:none;">
  <dt>ServRight  Brother Certified</dt>
  <dd class="certifications_26">
      <?= yesNoSelect("certifications_26"); ?>
              Date <?= dateFields("certifications_26",6) ?>
  </dd>
      </div>
      <div class ="divcertifications_32" style="display:none;">
          <dt>TechFORCE ICA Accepted</dt>
          <dd class="TechFORCEICAAccepted">
              <?= yesNoSelect("certifications_32"); ?>
              Date <?= dateFields("certifications_32",6) ?>
  </dd>
      </div>
      <div class ="divcertifications_5" style="display:none;">
          <dt>Trapollo Medical Certified</dt>
          <dd class="certifications_5">
              <?= yesNoSelect("certifications_5"); ?>
              Date <?= dateFields("certifications_5",6) ?>
          </dd>
      </div>
      <div class ="divcertifications_18" style="display:none;">
          <dt>Vonage Plus</dt>
          <dd class="certifications_18">
              <?= yesNoSelect("certifications_18"); ?>
              Date <?= dateFields("certifications_18",6) ?>
          </dd>
      </div>
      <?php
        $FSTagClass = new Core_Api_FSTagClass();
        $FSTagList_ForCompany = $FSTagClass->getFSTagList_ForCompany(1,'',1,0);//13840
      ?>
        <style>
            #contenttags select{
                width: 60px;
            }
        </style>
 <!--13837--><!--13840-->  
      <div id="contenttags">
                            <?php                        
                     
                            if (!empty($FSTagList_ForCompany))
                            {
                                foreach ($FSTagList_ForCompany as $item)
                                {                                  
                                    if ($item["id"]=="19")
                                     {?>          
                                     <div class ="itemtags_19" style="display:none;">
                                                                <dt>GGE Tech Evaluation</dt>
                                                                <dd class="certifications_24">
                                                                        <select class="yes_no" id="GGETechEvaluation" name="GGETechEvaluation">
                                                                                <option value="">Any&nbsp;&nbsp;</option>
                                                                                <option value="Pass">Pass (on first or second attempt)</option>
                                                                                <option value="Fail-1">Fail-1</option>
                                                                                <option value="Fail-2">Fail-2</option>
                                                                                <option value="Pending">Pending</option>
                                                                        </select>
                                                                        Date <?= dateFields("GGETechEvaluation",8) ?>
                                                                </dd>
                                    </div>
                                    <?php                  
                                    }
                                     else if($item["id"]=="22")
                                    {?>
                                    <div class ="itemtags_22" style="display:none;">
                                        <dt>CompuCom Certified</dt>
                                        <dd>
                                                <select name="CompuComCertified" class="yes_no" id="CompuComCertified">
                                                    <option value="">Any&nbsp;&nbsp;&nbsp;</option>  
                                                    <option value="Y">Yes</option>    
                                                    <option value="N">No</option>
                                                    <option value="T">Technician</option>
                                                    <option value="A">Analyst</option>
                                                    <option value="S">Specialist</option>
                                                </select>
                                            Date <?= dateFields("CompuComCertified") ?>
                                        </dd>
                                    </div>                          
                                    <?php 
                                    }
                                    else if($item["id"]=="25")                                        
                                    {    
                                    ?>
                                    <div class ="itemtags_25" style="display:none;">
                                        <dt>Purple Technician</dt>
                                        <dd class="PurpleTechnician">
                                                <select  class="yes_no" id="PurpleTechnician" name="PurpleTechnician">
                                                    <option value="">Any&nbsp;&nbsp;&nbsp;</option>  
                                                    <option value="Yes">Yes</option>    
                                                    <option value="No">No</option>
                                                    <option value="Certified">Verified</option>
                                                    <option value="Unverified">Unverified</option>
                                                </select>
                                                Date <?= dateFields("PurpleTechnician") ?>
                                        </dd>
                                    </div> 
                                    <?php 
                                    }
                                    else if($item["id"]=="28")
                                    {?>
                                    <div class ="itemtags_28" style="display:none;">
                                        <dt>Adjustable Bed </dt>
                                        <dd class="KeyCertifiedContractor">
                                                <select  class="yes_no" id="KeyCertifiedContractor" name="KeyCertifiedContractor">
                                                    <option value="">Any&nbsp;&nbsp;&nbsp;</option>  
                                                    <option value="Yes">Yes</option>    
                                                    <option value="No">No</option>
                                                    <option value="KeyContractor">Certified</option>
                                                    <option value="CertifiedTech">Expert</option>                                               
                                                </select>
                                                Date <?= dateFields("KeyCertifiedContractor") ?>
                                        </dd>
                                    </div>                      
                                    <?php 
                                    }
                                    else 
                                        {
                                        ?>
                                            <div class ="itemtags_<?= $item["id"] ?>" style="<?= empty($arrayTagsexists[$item["id"]]) ? "display:none;" : "display:'';" ?>">
                                                <dt><?= $item["Title"] ?></dt>
                                        <dd class="tags_<?= $item["id"] ?>">
                                            <input type="hidden" value="<?= $item["id"] ?>" name="tags[]" />
                                            <!--13931-->  
                                                    <?= yesNoSelectLevel("tagsitem_" . $item["id"], 
                                                            '', 
                                                            1, 
                                                            empty($arrayTagsexists[$item["id"]])?"":$arrayTagsexists[$item["id"]]['yes_no'],
                                                            "yes_no",
                                                            $item["HasMultiTagLevels"],
                                                            $item["TagLevelsData"]); ?>
                                                    Date <?= dateFields("tagsDate_" . $item["id"], 8, empty($arrayTagsexists[$item["id"]])?"":$arrayTagsexists[$item["id"]]['greater_date'], empty($arrayTagsexists[$item["id"]])?"":$arrayTagsexists[$item["id"]]['less_date']) ?>
                                        </dd>
                                        </div>
                                        <?php
                                        }//
                                }
                            }
                            ?>
      </div>    
       <!-- End 13837-->  
      <dt>Client Credentials</dt>
      <dd class="certifications_5">
          <select id="clientcredentials" style="width:315px;">
              <option value="">Select a Client Credential</option>
              <?php if(!empty($FSTagList_ForCompany)){
			      //$excArray = array('23','24','26','27','35','36');
                  $excNameArray = array('CompuComAnalyst',
                                      'CompuComSpecialist',
                                      'PurpleTechnicianCertified',
                                      'ErgoMotionKeyContractor',
                                      'BlackBoxCabling',
                                      'BlackBoxTelecom'
                                  );
      //22->CompuComTechnician
      //25->PurpleTechnicianUnverified
      //28->ErgoMotionCertifiedTechnician
                  foreach($FSTagList_ForCompany as $item){
                      if(empty($arrayTagsexists[$item["id"]]))
                      {
                          if(!in_array($item["Name"],$excNameArray)) 
                          {
                                if($item["Name"]=="CompuComTechnician")
                                {
                                    echo '<option value="'.$item["id"].'" >CompuCom Certified</option>';
                                }else if($item["Name"]=="PurpleTechnicianUnverified")
                                {
                                    echo '<option value="'.$item["id"].'" >Purple Technician</option>';                            
                                }else if($item["Name"]=="ErgoMotionCertifiedTechnician")
                                {
                                    echo '<option value="'.$item["id"].'" >Adjustable Bed</option>';
                                }
                                else
                                {
                                    echo '<option value="'.$item["id"].'" >'.$item["Title"].'</option>';
                                } 
                          }
                      }
              }
              }
              ?>
    <!-- End 13840-->          
    </select>
          <script type="text/javascript">
          function displaycredentical() {
              $(".itemtags_"+$(this).val()).css('display','');
              if($(this).val()!="")
              {
                  $("select#clientcredentials option:selected").remove();
              }
          }
          
      $(document).ready(function() {
          $("#clientcredentials").change(displaycredentical);
      });
          </script>
  </dd>
      <!--end 624-->
  <!--

  TechPreferred wasn't wired up, the results didn't enter the query.

  <dt>Tech Preferred</dt>
  <dd class="tech_preferred">
  	<?= yesNoSelect('TechPreferred'); ?>
  </dd>
  -->
</dl>
</div>
<div class="col2">
<dl>
  <!-- Certifications -->

  <dt>Dell Certifications</dt>
  <dd class="certifications">
  	<select name="DellCert" id="DellCert">
    	<option selected="selected" value="">Any&nbsp;&nbsp;</option>    
		  <option value="DCSE">Dell Certified Systems Expert</option>
          <option value="DCSNP">Dell Certified Storage Network Professional</option>
		  <option value="Other">Other</option>
		  <option value="None">None</option>
    </select>
  </dd>
    <dt>EMC Certifications</dt>
    <dd class="certifications">
        <?= Loademccert("EMCCert","EMCCert");?>
        
        Date <?= dateFields("EMCCert") ?>
    </dd>
  <dt>Other Certifications</dt>
  <dd class="other_certifications">
  	<input name="Certifications" id="Certifications" />
     <?= blankSelect("Certifications") ?>

  </dd>
  <dt>Skills</dt>
  <dd class="skills">
  	<input name="Skills" id="Skills" />
     <?= blankSelect("Skills") ?>
  </dd>
  <!-- Experience -->
  <dt>Specific Experience</dt>
  <dd class="specific_experience">
  	<input name="SpecificExperience" id="SpecificExperience" />
     <?= blankSelect("Experience") ?>
  </dd>
  <dt>MCSE</dt>
  <dd class="certification">
  	<?= yesNoSelect("MCSE"); ?>
  </dd>
  <dt>CCNA</dt>
  <dd class="certification">
  	<?= yesNoSelect("CCNA"); ?>
  </dd>
  <dt>A+</dt>
  <dd class="certification">
  	<?= yesNoSelect("APlus"); ?>
  </dd>
  <dt>CompTIA PDI+</dt>
  <dd class="certificationscbx">
      <?= yesNoSelect("certifications_36"); ?>
    Date <?= dateFields("certifications_36") ?>
  </dd>
  <dt>CompTIA Server+</dt>
  <dd class="certificationscbx">
      <?= yesNoSelect("certifications_37"); ?>
    Date <?= dateFields("certifications_37") ?>
  </dd>
  <dt>CompTIA Network+</dt>
  <dd class="certificationscbx">
      <?= yesNoSelect("certifications_38"); ?>
    Date <?= dateFields("certifications_38") ?>
  </dd>
  <dt>CompTIA (6-digit #)</dt>
  <dd class="comp_tia"><input name="CompTIA" id="CompTIA" />
     <?= blankSelect("CompTIA") ?>
    </dd>
  <!-- Resume -->
  <dt>Resume</dt>
  <dd class="resume">
  	<?= yesNoSelect("Resume"); ?>
  </dd>
  <dt>DeVry Student</dt>
  <dd class="resume">
  	<?= yesNoSelect("certifications_2"); ?>
    Date <?= dateFields("certifications_2") ?>
  </dd>
  <dt>Blue Ribbon Technician</dt>
  <dd class="resume">
  	<?= yesNoSelect("certifications_4"); ?>
    Date <?= dateFields("certifications_4") ?>
  </dd>
  <dt>Technician Boot Camp</dt>
  <dd class="resume">
      <select name="BootCamp" id="BootCamp">
        <option selected="selected" value="">Any</option>
        <option value="1">Yes</option>
        <option value="0">No</option>
        <option value="3">Boot Camp</option>
        <option value="43">Boot Camp Plus</option>
      </select>
    Date <?= dateFields("BootCampcertifications") ?>
</dd>
<dt>US Verified</dt>
  <dd class="USAuthorized">
    <select name="USAuthorized" id="USAuthorized">
        <option value="0">Any&nbsp;&nbsp;</option>    
        <option value="1">Pending</option>
        <option value="2">Authorized</option>
        <option value="3">Pending or Auth</option>
        <option value="4">Non Confirmation</option>
    </select>

    Date <?= dateFields("USAuthorizedDate") ?>
  </dd>
  <!--13840 remove --> 
  
  <dt>Black Box Cabling</dt>
  <dd class="EndeavorCertified">
     <select name="BlackBoxCabling" id="BlackBoxCabling">
      <option value="">Any&nbsp;&nbsp;</option>
	  <option value="Yes">Yes</option>    
      <option value="No">No</option>
      <option value="Helper">Helper (C1)</option>
      <option value="Advanced">Advanced (C2)</option>
      <option value="Lead">Lead (C3)</option>
	  <option value="Pending">Pending</option>
     </select>
     Date <?= dateFields("BlackBoxCabling") ?>
  </dd>
 <dt>Black Box Telecom</dt>
  <dd class="EndeavorCertified">
     <select name="BlackBoxTelecom" id="BlackBoxTelecom">
      <option value="">Any&nbsp;&nbsp;</option>
	  <option value="Yes">Yes</option>    
      <option value="No">No</option>
      <option value="Helper">Helper (T1)</option>
      <option value="Advanced">Advanced (T2)</option>    
	  <option value="Pending">Pending</option>
     </select>
     Date <?= dateFields("BlackBoxTelecom") ?>
  </dd> 
</dl>
  </div>

<div class="atsHeading">ISO</div>
<div class="col1">
<dl>
  <dt>Private</dt>
  <dd class="private">
  	<?= yesNoSelect("Private"); ?>
  </dd>

  <dt>W2 Tech</dt>
  <dd class="w2">
  	<?= yesNoSelect("W2"); ?>
  </dd>

  <dt>Dispatch</dt>
  <dd class="dispatch">
  	<?= yesNoSelect("Dispatch"); ?>
  </dd>
  
 <dt>Pay Company</dt>
	<dd class="techid"><?=YesNoSelect('Pay_Company')?></dd>
  </dl>

</div>
<div class="col2">
<dl>
 <dt>ISO ID</dt>
	<dd class="techid"><input name="ISO_Affiliation_ID_M" id="ISO_Affiliation_ID_M" /></dd>
 <dt>ISO Tech Only</dt>
	<dd class="techid"><?=YesNoSelect('ISO_Tech_Only')?></dd>
 <dt>ISO Company Name</dt>
	<dd class="techid">
		<select id="ISO_Affiliation_ID" name="ISO_Affiliation_ID" >
			<?php
                echo "<option value=\"\" >None</option>";
                foreach ($isos as $id => $name) {
                    echo "<option value=\"$id\">$name</option>";
                }
            ?> 
		</select>   
    </dd>      
</dl>
</div>
<div class="atsHeading">RATINGS &amp; CALLS</div>
<div class="col1">
<dl>
  <dt>Preference </dt>
	<dd class="techid"><input name="PreferencePercent" id="PreferencePercent" /></dd>
  <dt># Pref. Ratings</dt>
	<dd class="techid"><input name="SATRecommendedTotal" id="SATRecommendedTotal" /></dd>
   <dt>Performance </dt>
	<dd class="techid"><input name="PerformancePercent" id="PerformancePercent" /></dd>
 <dt># Perf. Ratings</dt>
	<dd class="techid"><input name="SATPerformanceTotal" id="SATPerformanceTotal" /></dd>
</dl>
</div>
<div class="col1">
<dl>
 <dt>Qty IMACs</dt>
	
<dd class="techid"><?=filterFields('Qty_IMAC_Calls')?></dd>  <!--13988-->
 <dt>Qty Service Calls</dt>

   <dd class="techid"><?=filterFields('Qty_FLS_Service_Calls')?> </dd>  <!--13988-->   
 <dt>No Shows</dt>

    <dd class="techid"><?=filterFields('No_Shows')?></dd>      <!--13988-->   
 <dt>Backouts</dt>
	
    
    <dd class="techid"><?=filterFields('Back_Outs')?></dd>         <!--13988-->   
</dl>
</div>

<div class="atsHeading">SKILLS</div>
  <div id="bottom">
  <!-- SAT -->

<!-- SPECIFIC SKILLS & RATINGS -->
<dl class="ratings" id="ratings_1">
<!--13735--> 
<dt>&nbsp</dt>
<dd>
    <span class="leftSkill"><u>Rated</u></span><span class="MidSkill"></span><span class="rightSkill"><u>Rating</u></span><span class="rightSkill"><u>FS-Expert</u></span>
</dd> 
<?= skillRatingFSExpertForm('Alarm Systems', 'AlarmSystems', 'AlarmSystemsSelfRating','AlarmSystemsFSExpert') ?><!-- 13978 -->
<?= skillRatingFSExpertForm('Audio / Video (A/V)','Electronics', 'electronicsSelfRating','electronicsFSExpert') ?>
<?= skillRatingFSExpertForm("Adjustable Bed Repair", "AdjustableBedRepair", "AdjustableBedRepairSelfRating","AdjustableBedRepairFSExpert"); ?>
<?= skillRatingFSExpertForm('Apple brand PCs/Laptops', 'Apple', 'appleSelfRating','appleFSExpert') ?>
<?= skillRatingFSExpertForm('ATM', 'atm', 'atmSelfRating','atmFSExpert') ?>
<?= skillRatingFSExpertForm('CAT5 Cabling', 'Cabling', 'cablingSelfRating','cablingFSExpert') ?>
<?= skillRatingFSExpertForm('CCTV / Surveillance', 'CCTV', 'CCTVSelfRating', 'CCTVFSExpert') ?>
<?= skillRatingFSExpertForm('Central Office Cabling', 'CentralOfficeCabling', 'CentralOfficeCablingSelfRating','CentralOfficeCablingFSExpert') ?>
<?= skillRatingFSExpertForm('Construction', 'Construction', 'constructionSelfRating','constructionFSExpert') ?>
<?= skillRatingFSExpertForm('Copiers', 'Copiers', 'CopiersSelfRating','CopiersFSExpert') ?>

<?= skillRatingFSExpertForm('Digital Signage', 'DigitalSignage', 'DigitalSignageSelfRating', 'DigitalSignageFSExpert') ?>
<?= skillRatingFSExpertForm('DSL/DMARC/T1', 'Dsl', 'DslSelfRating', 'DslFSExpert') ?>   <!--13875--> 
<?= skillRatingFSExpertForm('Electrical Wiring', 'Electrical', 'electricalSelfRating','electricalFSExpert') ?>
<?= skillRatingFSExpertForm('Electrician - Licensed/Certified', 'ElectricianCertified', 'ElectricianCertifiedSelfRating','ElectricianCertifiedFSExpert') ?><!--14121-->
<?= skillRatingFSExpertForm('Electro-Mechanical Binding Eq.', 'electroMech', 'electroMechSelfRating','electroMechFSExpert') ?>
<?= skillRatingFSExpertForm('Enterprise Storage', 'EnterpriseStorage', 'EnterpriseStorageSelfRating','EnterpriseStorageFSExpert') ?>
<?= skillRatingFSExpertForm('Fiber Cabling', 'FiberCabling', 'FiberCablingSelfRating', 'FiberCablingFSExpert') ?>
<?= skillRatingFSExpertForm('Flat Panel TV', 'FlatPanelTV', 'FlatPanelTVSelfRating', 'FlatPanelTVFSExpert') ?>
<?= skillRatingFSExpertForm('General Wiring', 'GeneralWiring', 'GeneralWiringSelfRating','GeneralWiringFSExpert') ?>
<?= skillRatingFSExpertForm('Home Networking', 'Networking', 'networkingSelfRating','networkingFSExpert') ?>
<?= skillRatingFSExpertForm('Kiosk', 'Kiosk', 'KioskSelfRating','KioskFSExpert') ?>

</dl>
<dl class="ratings" id="ratings_2">
<!--13735--> 
<dt>&nbsp</dt>
<dd>
    <span class="leftSkill"><u>Rated</u></span><span class="MidSkill"></span><span class="rightSkill"><u>Rating</u></span><span class="rightSkill"><u>FS-Expert</u></span>
</dd> 
<?= skillRatingFSExpertForm('Low Voltage', 'LowVoltage', 'LowVoltageSelfRating', 'LowVoltageFSExpert') ?>
<?= skillRatingFSExpertForm('Mobile Devices', 'MobileDevices', 'MobileDevicesSelfRating', 'MobileDevicesFSExpert') ?>
<?= skillRatingFSExpertForm('PC - Desktop / Laptop', 'Desktop', 'desktopSelfRating','desktopFSExpert') ?> <!--13675-->
<?= skillRatingFSExpertForm('POS', 'pos', 'posSelfRating', 'posFSExpert') ?>
<?= skillRatingFSExpertForm('Printers', 'Printers', 'PrintersSelfRating', 'PrintersFSExpert') ?>
<?= skillRatingFSExpertForm('RFID', 'RFID', 'RFIDSelfRating','RFIDFSExpert') ?>
<?= skillRatingFSExpertForm('Routers / Switches', 'Routers', 'routersSelfRating', 'routersFSExpert') ?>
<?= skillRatingFSExpertForm('Safes/Security Cabinets', 'savesSecCabinets', 'savesSecCabinetsSelfRating', 'savesSecCabinetsFSExpert') ?>
<?= skillRatingFSExpertForm('Satellite Install', 'Satellite', 'SatelliteSelfRating', 'SatelliteFSExpert') ?>
<?= skillRatingFSExpertForm('Scanners', 'Scanners', 'ScannersSelfRating','ScannersFSExpert') ?>
<?= skillRatingFSExpertForm('Server Hardware', 'Servers', 'ServersSelfRating','ServersFSExpert') ?>
<?= skillRatingFSExpertForm('Server Software', 'ServerSoftware', 'ServerSoftwareSelfRating', 'ServerSoftwareFSExpert') ?>
<?= skillRatingFSExpertForm('Site Survey', 'SiteSurvey', 'SiteSurveySelfRating', 'SiteSurveyFSExpert') ?>
<?= skillRatingFSExpertForm('Tel. - Non-VoIP', 'Telephony', 'telephonySelfRating', 'telephonyFSExpert') ?>
<?= skillRatingFSExpertForm('Tel. - VoIP', 'TelephonyVoIP', 'TelephonyVoIPSelfRating', 'TelephonyVoIPFSExpert') ?>
<?= skillRatingFSExpertForm('Wireless Networking', 'Wireless', 'WirelessSelfRating', 'WirelessFSExpert') ?>
<?= skillRatingForm('Residential', 'Residential', 'ResidentialSelfRating') ?>
<?= skillRatingForm('Commercial', 'Commercial', 'CommercialSelfRating') ?>
<?= skillRatingForm('Government', 'Government', 'GovernmentSelfRating') ?>
<!--13735--> 
 <dt>Language Skills</dt>
 <dd>
    <select name='LanguageSkills' id='LanguageSkills' class='skill'>
        <option value='0' selected='selected'>Any</option>
        <?php $core = new Core_TechLanguages();
        $languages = $core->getLanguages();
        if(!empty($languages)){
            foreach($languages as $language){
        ?>  
            <option value='<?= $language["id"] ?>'><?= $language["name"] ?></option>
        <?php    }
        } ?>
    </select>
    
    <select name='LanguageSkillsRating' id='LanguageSkillsRating' class='rating'>
        <option value='0'>Any&nbsp;&nbsp;</option>
        <option value='0'>Either</option>
        <option value='1'>Comp</option>
        <option value='2'>Fluent</option>
     </select>
  </dd>   
</dl>
</div>
<div class="atsHeading">CABLING &amp; TELEPHONY</div>
<div id="cabling" class="col1">
<dl>
<?=checkBoxForm("Can read wiring diagrams?", "skills_2")?>
<?=checkBoxForm("Have experience installing surveillance DVR/Cam systems?", "skills_3")?>
<?=checkBoxForm("Skilled Cat5 and/or fiber cabling?", "skills_41")?>
<?=checkBoxForm("Performed 40+ hrs of Cat 5 cabling contracts in the past 6 months?", "skills_42")?>
<?=checkBoxForm("Own or can source, ladders, cable, and cable testers?", "equipment_26")?>
<?=checkBoxForm("Have required licenses for low voltage work?", "equipment_29")?>
<?=checkBoxForm("Cable TV Co., Telco, and/or CLEC work experience?", "skills_45")?>    
<?=checkBoxForm("Do you have advanced cable skills (including lacing and stitching)?", "skills_46")?>  
<?=checkBoxForm("Has worked in a Battery Distribution Fuse Bay?", "skills_47")?>  
<?=checkBoxForm("Has experience reading MOPs (Methods of Procedures)?", "skills_48")?>      
<?=checkBoxForm("Experience with voice/data equip. room install & build out", "skills_53")?>  
<?=checkBoxForm("Experience in single/multi-mode termination and testing", "skills_54")?>  
<?=checkBoxForm("Experience using a laptop for basic switch configuration", "skills_55")?>
<?=checkBoxForm("Can operate scissor lift and extensive experience w/ ladder", "skills_49")?>
<?=checkBoxForm("Experience w/ multiple cable installation methods", "skills_51")?>
<?=checkBoxForm("Can install sleeves, firestops, J-Hooks, Panduit, Wire Mold raceways", "skills_52")?>
<?=checkBoxForm("Have a Vehicle?", "equipment_27")?>
<!--<?=checkBoxForm("Have a truck to carry ladders and cable?", "equipment_28")?>-->
<?=checkBoxForm("Corded masonry drill hammer and bits up to 1\" diameter", "equipment_20")?>
<?=checkBoxForm("Rotary coax cable stripper for RG-59 and RG-6 cable", "equipment_19")?>
<?=checkBoxForm("Rechargeable CCTV installer's LCD 4", "equipment_21")?>
<?=checkBoxForm("Volt/Ohm meter", "equipment_22")?>
<?=checkBoxForm("Ladder 6'", "equipment_5")?>
<?=checkBoxForm("Ladder 12'", "equipment_6")?>
<?=checkBoxForm("Ladder 20'+", "equipment_7")?>
<?=checkBoxForm("Tone Generator and Wand", "equipment_8")?>
<?=checkBoxForm("Butt Set", "equipment_9")?>
<?=checkBoxForm("Digital VOM Meter", "equipment_10")?>
<?=checkBoxForm("Continuity Tester", "equipment_11")?>
<?=checkBoxForm("Punch Tool", "equipment_24")?>
<!--<?=checkBoxForm("Punch Tool with 66 blade", "equipment_12")?>
<?=checkBoxForm("Punch Tool with 110 blade", "equipment_13")?>
<?=checkBoxForm("Punch Tool with Bix blade", "equipment_14")?>
<?=checkBoxForm("Punch Tool with Krone blade", "equipment_15")?>-->
<?=checkBoxForm("Crimp Tool with RJ11", "equipment_16")?>
<?=checkBoxForm("Crimp Tool with RJ45", "equipment_17")?>
<?=checkBoxForm("50' Fish Tape", "equipment_18")?>
<?=checkBoxForm("Laptop (Microsoft XP or newer)", "equipment_1")?>
<?=checkBoxForm("Set of screwdrivers", "equipment_23")?>
<?=checkBoxForm("Cable", "equipment_31")?>
<?=checkBoxForm("Cable Testers", "equipment_32")?>
<?=checkBoxForm("Cable Certifier", "equipment_33")?>
<?=checkBoxForm("Lacing Needle", "equipment_34")?>
<?=checkBoxForm("Wire Wrap Gun", "equipment_35")?>

<?=checkBoxForm("Hand Tools", "equipment_36")?>
<?=checkBoxForm("Cordless Drill", "equipment_37")?>
<?=checkBoxForm("Personal Protection Equipment (Gloves, Hard Hat, Boots)", "equipment_38")?>
<?=checkBoxForm("Machine Label Maker (P-Touch, Bradley, etc.)", "equipment_39")?>
<?=checkBoxForm("Gofer Poles (or Glow Rods)", "equipment_40")?>
<?=checkBoxForm("Fiber Optic dB Loss Meter & Light Source", "equipment_41")?>
<?=checkBoxForm("Fiber Optic Termination Kit", "equipment_42")?>

</dl>
</div>

<div id="telephony" class="col2">
<dl>
<dt>&nbsp;</dt>
<dd>
<span class="leftSkill">Exp</span><span class="rightSkill">6 mon</span>
</dd>
<?=checkBoxForm("Experience in installing OR maintaining (break-fix) telephony systems", "skills_4")?>
<?=checkBoxForm("Installed or performed \"break-fix\" on a Server OR VOIP based telephony system", "skills_6")?>

<?=checkBoxForm("Understands telecom color code, experienced w/ termination jacks & patch panels", "skills_50")?>
<?=checkBoxForm("Experience w/ DMARC rooms & equipment", "skills_56")?>
<?=checkBoxForm("Experience with basic PBX / Key system operation", "skills_57")?>

<dt class="skillHeading">Avaya Small Systems</dt>
<dd>&nbsp;

</dd>
<?=checkBoxSkillsForm("Partner or Merlin", "skills_8", "skills_9", "TelephonySkills")?>
<?=checkBoxSkillsForm("IP Office", "skills_10", "skills_11", "TelephonySkills")?>
<dt class="skillHeading">PBX</dt>
<dd>&nbsp;

</dd>
<?=checkBoxSkillsForm("Definity", "skills_12", "skills_13", "TelephonySkills")?>
<?=checkBoxSkillsForm("8300/8500/8700", "skills_14", "skills_15", "TelephonySkills")?>
<?=checkBoxSkillsForm("Gateway 250/350", "skills_16", "skills_17", "TelephonySkills")?>
<dt class="skillHeading">Nortel Small Systems</dt>
<dd>&nbsp;

</dd>
<?=checkBoxSkillsForm("Norstar", "skills_18", "skills_19", "TelephonySkills")?>
<?=checkBoxSkillsForm("BMS", "skills_20", "skills_21", "TelephonySkills")?>
<dt class="skillHeading">PBX</dt>
<dd>&nbsp;

</dd>
<?=checkBoxSkillsForm("Meridian", "skills_22", "skills_23", "TelephonySkills")?>
<?=checkBoxSkillsForm("CS 1000/1500", "skills_24", "skills_25", "TelephonySkills")?>
<dt class="skillHeading">Cisco</dt>
<dd>&nbsp;

</dd>
<?=checkBoxSkillsForm("Call Manager", "skills_26", "skills_27", "TelephonySkills")?>
<dt class="skillHeading">Siemens (ROLM)</dt>
<dd>&nbsp;

</dd>
<?=checkBoxSkillsForm("9751", "skills_28", "skills_29", "TelephonySkills")?>
<?=checkBoxSkillsForm("Hicom", "skills_30", "skills_31", "TelephonySkills")?>
<?=checkBoxSkillsForm("Hipath", "skills_32", "skills_44", "TelephonySkills")?>
<?=checkBoxSkillsForm("NEC", "skills_33", "skills_34", "TelephonySkills")?>
<?=checkBoxSkillsForm("InterTel", "skills_35", "skills_36", "TelephonySkills")?>
<?=checkBoxSkillsForm("Mitel", "skills_37", "skills_38", "TelephonySkills")?>
<?=checkBoxSkillsForm("Toshiba", "skills_39", "skills_40", "TelephonySkills")?>
</dl>
  </div>

<div class="atsHeading">INTERNAL FIELDS</div>
<div class="col1">
<dl>
 <dt>MRA Compliant</dt>
 	<dd><?= yesNoSelect("MRA_Compliant"); ?></dd>
 <dt>CDM</dt>
 	<dd><?= yesNoSelect("CDM"); ?></dd>
 <dt>L1</dt>
 	<dd><?= yesNoSelect("L1"); ?></dd>
 <dt>L2</dt>
 	<dd><?= yesNoSelect("L2"); ?></dd>
 <dt>L3</dt>
 	<dd><?= yesNoSelect("L3"); ?></dd>
 <dt>L4</dt>
 	<dd><?= yesNoSelect("L4"); ?></dd>
 <dt>DSCE General</dt>
 	<dd><?= yesNoSelect("DELL_DCSE_Reg"); ?></dd>
 <dt>DCSE Soft Skills</dt>
 	<dd><?= yesNoSelect("DCSE_Soft_Skills"); ?></dd>
 <dt>DCSE Lead</dt>
 	<dd><?= yesNoSelect("DCSE_Lead"); ?></dd>
 <dt>VA</dt>
 	<dd><?= yesNoSelect("VA"); ?></dd>
</dl>
</div>
<div class="col2">
<dl>
  <dt>SSA</dt>
  <dd>
  	<?= yesNoSelect("SSA"); ?>
  </dd>
  <dt>Secret</dt>
  <dd>
  	<?= yesNoSelect("Secret"); ?>
  </dd>
  <dt>Top Secret</dt>
  <dd>
  	<?= yesNoSelect("Top_Secret"); ?>
  </dd>
  <dt>General 1</dt>
  <dd>
  	<?= yesNoSelect("General1"); ?>
  </dd>
  <dt>General 2</dt>
  <dd>
  	<?= yesNoSelect("General2"); ?>
  </dd>
  <dt>General 3</dt>
  <dd>
  	<?= yesNoSelect("General3"); ?>
  </dd>
  <dt>General 4</dt>
  <dd>
  	<?= yesNoSelect("General4"); ?>
  </dd>
  <dt>General 5</dt>
  <dd>
  	<?= yesNoSelect("General5"); ?>
  </dd>
  <dt>General 6</dt>
  <dd>
  	<?= yesNoSelect("General6"); ?>
  </dd>
</dl>
</div>

  <p class="submit"><input type="submit" /></p>
</form>

<script type="text/javascript">
function onChangeCountry() {
    $.ajax({
        url         : '/widgets/dashboard/js/state-info/',
        dataType    : 'json',
        data        : {country: $("#Country").val()},
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            if (data) {
                //AUTOFILL
				var option = '';
				option += '<option selected="selected" value="">Select State</option>';
                for(index in data) {
					option += '<option value="' + index + '">' + data[index] + '</option>';
				}
				$("#State").html(option);
				state = this.filters().state();
				if (state) $("#State").val(state);
			}
        }
    });	
}

function TechSourceSelect() {
	return $("select#TechSourceSelect");
}

function TechSource() {
	return $("input#TechSource");
}

$('document').ready(function() {
	$.datepicker.setDefaults({
		dateFormat: 'yy-mm-dd'
	});
	$('.showCal').datepicker();
	$('input#W9_Date_RecGreater').datepicker();
	$('input#W9_Date_RecLess').datepicker();
	$('input#RegDateGreater').datepicker();
	$('input#RegDateLess').datepicker();
	$('input#LoggedOnDateGreater').datepicker();
	$('input#LoggedOnDateLess').datepicker();
	$('input#LastWOBidGreater').datepicker();
	$('input#LastWOBidLess').datepicker();
	$('input#LastWORunGreater').datepicker();
	$('input#LastWORunLess').datepicker();
	$('input#BgReqGreater').datepicker();
	$('input#BgReqLess').datepicker();
	$('input#BgPassFullGreater').datepicker();
	$('input#BgPassFullLess').datepicker();
	$('input#BgPassGreater').datepicker();
	$('input#BgPassLess').datepicker();
	$('input#AcceptTermsGreater').datepicker();
	$('input#AcceptTermsLess').datepicker();
	$('input#FSCertLess').datepicker();
	$('input#FSCertGreater').datepicker();
	$('input#FLSCSP_RecDateLess').datepicker();

	$('input#FLSCSP_RecDateGreater').datepicker();
	$('input#SMS_AgreeDateGreater').datepicker();
	$('input#SMS_AgreeDateLess').datepicker();
    $('input#USAuthorizedDateGreater').datepicker();
    $('input#USAuthorizedDateLess').datepicker();

	TechSourceSelect().change(function() {
		switch(TechSourceSelect().attr("value"))
		{
			case "Blog":
			case "School":
			case "Referral":
			case "Other":
				TechSource().attr("value","");
				TechSource().focus();
				break;
			case "Any":
			case undefined:
				TechSource().attr("value","");
				break;
			default:
				TechSource().attr("value", TechSourceSelect().attr("value"));
		}
	});
});

</script>

<?php
}

if (!isset($_GET['first'])) {
?>

</div>

<!--- End Content --->
<?php
	require ("../footer.php");
}
?>
