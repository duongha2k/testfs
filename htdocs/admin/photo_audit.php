<?php $page = 'admin'; ?>
<?php $option = 'techs'; ?>
<?php $selected = 'photoAudit'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php

require_once("../library/caspioAPI.php");


checkAdminLogin2();


$httpReferer = (isset($_GET['referer']) ? urldecode($_GET['referer']) : $_SERVER["HTTP_REFERER"]);
$saveReferer = $_SESSION["saveReferer"];
if ($_SESSION["saveReferer"] != $httpReferer)
    $_SESSION["saveReferer"] = $httpReferer;

require_once("../headerStartSession.php");
require_once dirname(__FILE__) . '/../../includes/modules/common.init.php';
?>
<script type="text/javascript" src="/widgets/js/jquery.validate.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.curvycorners.js"></script>
<link rel="stylesheet" type="text/css" href="/widgets/js/star-rating/jquery.rating.css" />

<style type="text/css">@import url(../library/ui.datepicker.css);</style>
<script type="text/javascript" src="../library/ui.datepicker.js"></script>

<script type="text/javascript" src="/widgets/js/fancybox/jquery.easing.pack.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-mod.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.jqEasyCharCounter.js"></script>
<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>

<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>

<script type="text/javascript" src="js/photo_audit.js"></script>
<link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" media="all" />

<center>
    <div style="width:60%;">
        <form action="" name="frmPhotoAudit" id="frmPhotoAudit">


            <table width="100%" style="padding-top: 20px;">
                <tr>
                    <td colspan="2" align="center">
                        <h4>Picture Audit</h4>
                    </td>
                </tr>
            </table>
            <table width="100%" id="tableSearch" cellspacing="15" cellpadding="2">
                <tr>
                    <td align="right" style="padding-right: 15px;">
                        Date Uploaded: >=
                    </td>
                    <td>
                        <input type="text" id="txtDateUpdatedForm" name="txtDateUpdatedForm"/> 
                    </td>
                </tr>
                <tr>
                    <td align="right" style="padding-right: 15px;">
                        <=
                    </td>
                    <td>
                        <input type="text" id="txtDateUpdatedTo" name="txtDateUpdatedTo"/> 
                    </td>
                </tr>
                <tr>
                    <td align="right" style="padding-right: 15px;">
                        Pending Only
                    </td>
                    <td>
                        <input type="checkbox" checked="true" id="chkPending" name="chkPending"/> 
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input value="Search" class="link_button middle2_button" type="button" id="cmdSearch" />
                    </td>
                </tr>
            </table>
            <div id="divResult" style="display:none;">

            </div>
        </form>
    </div>
</center>
<script>
    var _photo_audit = photo_audit.CreateObject({
        id:'_photo_audit' 
    });
</script>
<div id="wr">
</div>
<?php require ("../footer.php"); ?>