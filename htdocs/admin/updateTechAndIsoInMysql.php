<?php
// Updates ISO and ISO tech information
require_once("library/quickbooks.php");
if (!isset($_POST["TechID"]) || !is_numeric($_POST["TechID"])) die();
$TechID = $_POST["TechID"];
copyTechProfile(array($TechID));
copyTechBankInfo(array($TechID));
?>
<script type="text/javascript"> 
	setTimeout('close()',100);
</script> 