<?php $page = 'admin'; ?>
<?php $option = 'dell_Info'; ?>
<?php $selected = 'Dell'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?PHP require_once("../library/openSSL.php"); ?>

<?php
if (isset($_POST["PW"])) {

	$privateKey = getDellPrivateKey($_POST["PW"]);
	if (!$privateKey) {
		header("Location: dell_InfoLogin.php");
		//die();
	} else{
		$_SESSION["DellInfoPW"] = $_POST["PW"];
		header("Location: dell_Info_Report.php");
	}
}

?>

<br/><br/>
<div style="text-align: center">
	<?=$msg?>
	<form name="login" method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
		Enter password to view Dell VA tech information:<br/><br/>
		<input name="PW" type="password" type="text" />
		<input name="submit" type="submit" value="Submit"/>
	</form>	
</div>

<!--- End Content --->
<?php require ("../footer.php"); ?>