<?php
//This page is will log all admins that login by inserting into Caspio using SOAP.
require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');

try {
// Library calls
//require_once("../library/caspioAPI.php");

// Retrieve values from login
$UserName = $_REQUEST['UserName'];
$Password = $_REQUEST['Password'];
//$Company_ID = $_REQUEST['Company_ID'];

$authData = array('login'=>$UserName, 'password'=>$Password);
//$errors = Core_Api_Error::getInstance();


$user = new Core_Api_AdminUser();
$user->checkAuthentication($authData);

if (!$user->isAuthenticate()){
	header("Location: /admin/?login=false");
	return API_Response::fail();
	exit;
} 


//header("Location:http://www.google.com/");


// Insert Login History into TR_Client_Login_History
// $insertWorkorders = caspioInsert("TR_Client_Login_History", "UserName, DateTime", "'$UserName', '$DateTime'");

$user->loginHistory($user->getLogin());

require_once("../headerStartSession.php");

//Set logged in session
/*$cookie_path = "/";
$cookie_timeout = "0";
$cookie_domain = ".fieldsolutions.com";
session_set_cookie_params($cookie_timeout, $cookie_path, $cookie_domain);
session_name(fsTemplate);
session_start();*/

$_SESSION['loggedIn']="yes";
$_SESSION['loggedInAs']="admin";
$_SESSION['UserName']=$UserName;

if($user->isAccounting()){
	$_SESSION['isAccounting'] = "yes";
}

if($user->isFsPlusOne()){
	$_SESSION['isFsplusone'] = "yes";
}

// Library calls
//require_once("../library/caspioAPI.php");
//$result = caspioSelectAdv("TR_Admin", "Password", "UserName = '{$_SESSION["UserName"]}'", "", false, "`", "^|%", false);
$_SESSION["UserPassword"] = trim($Password);

// Get Subdomain
/*
$urlParts = explode('.', $_SERVER['HTTP_HOST']);
$template = $urlParts[0];
$siteTemplate = $_SESSION['template'];

$checkIndexDisplay = $template;

if($siteTemplate == ""){
	$_SESSION['template']=$template;
	$siteTemplate = $_SESSION['template'];
}else if($siteTemplate != $template){
	$_SESSION['template']=$template;
	$siteTemplate = $_SESSION['template'];
}
*/
}catch (SoapFault $fault){
//FTP Failure code

//smtpMail("getClientLoginHistory", "admin@fieldsolutions.com", "cmcgarry@fieldsolutions.com,gbailey@fieldsolutions.com", "getClientLoginHistory Error", "$fault", "$fault", "getClientLoginHistory.php");
}

$forceSSL = $siteTemplate != "dev";

if (!empty($_SESSION['redirectURL']) ) {
    $redirectTo = $_SESSION['redirectURL'];
    $_SESSION['_prev_redirectURL'] = $_SESSION['redirectURL'];
    unset($_SESSION['redirectURL']);
    header('Location: http'.((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || $forceSSL ? 's' : '').'://'.$_SERVER['HTTP_HOST']. $redirectTo);    
} else {
    header('Location: http'.((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || $forceSSL ? 's' : '').'://'.$_SERVER['HTTP_HOST'].'/admin/dashboard.php');
}

?>
