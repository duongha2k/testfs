<?php $page = 'admin'; ?>
<?php $option = 'ISO'; ?>
<?php $selected = 'ISOList'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>

<!-- Add Content Here -->

<?php
//	ini_set("display_errors", 1);
	require_once("../library/mySQL.php");
	$msg = "";
	$proceed = false;
	if (isset($_POST["submit"])) {
//		print_r($_POST);
		if (array_key_exists("RoutingNum", $_POST) && 
			array_key_exists("AccountNum", $_POST) &&
			stripos($_POST["RoutingNum"], "x") === FALSE && 
			stripos($_POST["AccountNum"], "x") === FALSE) {
			$proceed = true;
		}
		else {
			// bad account info
			$msg = "Your account # and/or routing # doesn't appear to be valid";
		}
	}
	if (isset($_POST["submit"]) && $proceed) {
		// Update tech info
		require_once("../library/openSSL.php");
		require_once("../library/caspioAPI.php");
		$result = mysqlFetchAssoc(mysqlQuery("SELECT Company_Name FROM iso WHERE UNID = '{$_POST['TechID']}'"));
		$parts = $result[0];
		$Company_Name = mysqlEscape($parts["Company_Name"]);  // Using Company_Name as Last_Name
		$firstName = "' '";
		$ISO_ID = $_POST['TechID'];
		
		if ($_POST["PaymentMethod"] != "Direct Deposit") {
			// Ignore/clear other info when not direct deposit
			$insertQuery = "INSERT INTO ISOBankInfo (ISOID, FirstName, LastName, PaymentMethod, AgreeTerms, DepositType, BankName, BankAddress1, BankAddress2, BankCity, BankState, BankZip, BankCountry, AccountName, RoutingNum, AccountNum, MaskedRoutingNum, MaskedAccountNum, DateChange) VALUES ('$ISO_ID', $firstName, '$Company_Name', '{$_POST['PaymentMethod']}', '0', 'None', '', '', '', '', '', '', '', '', '', '', '', '', NOW()) ON DUPLICATE KEY UPDATE ISOID = '$ISO_ID', FirstName = $firstName, LastName = '$Company_Name', PaymentMethod = '{$_POST['PaymentMethod']}', AgreeTerms = '0', DepositType = '', BankName = '', BankAddress1 = '', BankAddress2 = '', BankCity = '', BankState = '', BankZip = '', BankCountry = '', AccountName = '', RoutingNum = '', AccountNum = '', MaskedRoutingNum = '', MaskedAccountNum = ''";
		}
				
		else {
			$publicKey = getBankingPublicKey();
			if (!$publicKey) {
				echo "Unable to save information at this time.<br/>";
				die();
			}
			$repeat = strlen($_POST["RoutingNum"]) - 4;
			$maskedRoutingNum = mysqlEscape(str_repeat("X", $repeat) . substr($_POST["RoutingNum"], -4));
			$repeat = strlen($_POST["AccountNum"]) - 4;
			$maskedAccountNum = mysqlEscape(str_repeat("X", $repeat) . substr($_POST["AccountNum"], -4));
			$routingNum = mysqlEscape(encryptData($_POST["RoutingNum"], $publicKey));
			$accountNum = mysqlEscape(encryptData($_POST["AccountNum"], $publicKey));
	//		$agreeTerms = (isset($_POST['AgreeTerms']) && $_POST['AgreeTerms'] == 1 ? "1" : "0");
			$agreeTerms = ($_POST["PaymentMethod"] == "Direct Deposit" ? "1" : "0");
			$BankName = mysqlEscape($_POST["BankName"]);
			$BankAddress1 = mysqlEscape($_POST["BankAddress1"]);
			$BankAddress2 = mysqlEscape($_POST["BankAddress2"]);
			$BankCity = mysqlEscape($_POST["BankCity"]);
			$BankState = "(SELECT sl.id FROM states_list AS sl WHERE sl.Abbreviation = '" . mysqlEscape($_POST["BankState"]) . "' AND sl.Country = '" . mysqlEscape($_POST["BankCountry"]) . "' LIMIT 1)";
			$BankZip = mysqlEscape($_POST["BankZip"]);
			$BankCountry = "(SELECT c.id FROM Countries AS c WHERE c.Code = '" . mysqlEscape($_POST["BankCountry"]) . "' LIMIT 1)";
			$AccountName = mysqlEscape($_POST["AccountName"]);
			
			$insertQuery = "INSERT INTO ISOBankInfo (ISOID, FirstName, LastName, PaymentMethod, AgreeTerms, DepositType, BankName, BankAddress1, BankAddress2, BankCity, BankState, BankZip, BankCountry, AccountName, RoutingNum, AccountNum, MaskedRoutingNum, MaskedAccountNum, DateChange) VALUES ('$ISO_ID', $firstName, '$Company_Name', '{$_POST['PaymentMethod']}', '$agreeTerms', '{$_POST['DepositType']}', '$BankName', '$BankAddress1', '$BankAddress2', '$BankCity', $BankState, '$BankZip', $BankCountry, '$AccountName', '$routingNum', '$accountNum', '$maskedRoutingNum', '$maskedAccountNum', NOW()) ON DUPLICATE KEY UPDATE ISOID = '$ISO_ID', FirstName = $firstName, LastName = '$Company_Name', PaymentMethod = '{$_POST['PaymentMethod']}', AgreeTerms = '$agreeTerms', DepositType = '{$_POST['DepositType']}', BankName = '$BankName', BankAddress1 = '$BankAddress1', BankAddress2 = '$BankAddress2', BankCity = '$BankCity', BankState = $BankState, BankZip = '$BankZip', BankCountry = $BankCountry, AccountName = '$AccountName', RoutingNum = '$routingNum', AccountNum = '$accountNum', MaskedRoutingNum = '$maskedRoutingNum', MaskedAccountNum = '$maskedAccountNum'";
			
		
		}
		
		//echo "<br /><br />" . $insertQuery . "<br /><br />";
		
		if (mysqlQuery($insertQuery)) {
			$msg = "Your information has been updated";
			require_once("../library/quickbooks.php");
			$techID = array($_POST['TechID']);
			copyISOProfile($techID);
			copyISOBankInfo($techID);
		}
	//echo (mysql_error());
	}
	
	// get tech info
	$countryId = 0;
	$stateId = 0;
	$techID = $_GET["v"];
	$ISO_ID = $techID;
	if (isset($_GET["v"]) && is_numeric($techID)) {
		//echo "<b>ID: </b>" . $ISO_ID;
		$techInfo = mysqlFetchAssoc(mysqlQuery("SELECT *, c.Code AS CAbbr, sl.Abbreviation AS SAbbr FROM ISOBankInfo LEFT JOIN Countries AS c ON c.id = BankCountry LEFT JOIN states_list AS sl ON sl.id = BankState WHERE ISOID = '$ISO_ID'"));
		//echo "<br /><br />TechInfo:" . $techInfo;
	if ($techInfo) {
			$techInfo = $techInfo[0];
			$countryId = $techInfo["BankCountry"];
			$countryAbbr = $techInfo["CAbbr"];
			$stateId = $techInfo["BankState"];
			$stateAbbr = $techInfo["SAbbr"];

/*			$privateKey = getBankingPrivateKey("tempPassword");
			if (!$privateKey) {
				echo "Bad private key.<br/>";
				die();
			}
	
			$dData = decryptData($techInfo["RoutingNum"], $privateKey);
			
			echo htmlentities($dData);*/
			
		}
		else
			$techInfo = array();
	}


	$common = new Core_Api_CommonClass;
	$countryOptions = $common->getCountries();
	$countriesHTML = "<option value=\"\"" . (empty($countryAbbr) ? "selected=\"selected\"" : "") . ">Select Country</option>";
	foreach ($countryOptions->data as $country) {
		$countriesHTML .= "<option value=\"$country->Code\"" . ($countryAbbr == $country->Code ? "selected=\"selected\"" : "") . ">$country->Name</option>";
	}

	if (empty($countryAbbr)) $countryAbbr = 'US';
	$statesOptions = $common->getStatesArray($countryAbbr);
	$statesHTML = "<option value=\"\"" . (empty($stateAbbr) ? "selected=\"selected\"" : "") . ">Select State</option>";
	foreach ($statesOptions->data as $code => $name) {
		$statesHTML .= "<option value=\"$code\"" . ($stateAbbr == $code ? "selected=\"selected\"" : "") . ">$name</option>";
	}
?>
<style  TYPE="text/css">
div#main td {
	text-align: left;
}

.label{ font-size:12px; color:#385C7E;}
.required {
	font-size: 12px;
	color: #FF0000;
}
.footer {
	text-align: center;
	background-color: #BACBDF; 
	border-top: #385C7E solid medium; 
	padding: 5px;
}
.caspioButton
{
/*Back Button Attributes*/
color: #ffffff;
font-size: 12px;
font-family: Verdana;
font-style: normal;
font-weight: bold;
text-align: center;
vertical-align: middle;
border-color: #5496C6;
border-style: solid;
border-width: 1px;
background-color: #032D5F;
/* Forced by default to add space between buttons */
width: auto;
height: auto;
margin: 0 3px;
}

</style>

<br /><br />

<div align="center" id="main">
	<?=$msg?>
	<form id="BankingInfo" name="BankingInfo" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>?v=<?=$techID?>" onSubmit="return validate()">
	<input id="TechID" name="TechID" type="hidden" value="<?=$techID?>" />
	<table id="BankingInfoTable" cellpadding="0" cellspacing="0" border="0" style="border:#385C7E solid medium; width: 500px">
		<tr>
			<td>
				<table cellpadding="0" cellspacing="5">
					<tr>
						<td class="label">Payment Method</td>
						<td>
							<select id="PaymentMethod" name="PaymentMethod" onChange="paymentMethodChange()">
								<option value="Check" <?php echo ($techInfo["PaymentMethod"] == "Check" ? "selected=\"selected\"" : "")?>>Check</option>
								<option value="Direct Deposit" <?php echo ($techInfo["PaymentMethod"] == "Direct Deposit" ? "selected=\"selected\"" : "")?>>Direct Deposit</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2"><hr /></td>
					</tr>
					<tr>
						<td colspan="2" class="label" style="font-weight: bold;">
I hereby authorize Field Solutions, LLC to initiate automatic deposits to my account at the financial institution named below. Further, I agree not to hold Field Solutions, LLC responsible for any delay or loss of funds due to incorrect or incomplete information supplied by me or by my financial institution or due to an error on the part of my financial institution in depositing funds to my account. This agreement will remain in effect until Field Solutions, LLC receives a written notice of cancellation from me or my financial institution, or until I submit a new direct deposit form to Field Solutions.</td>
					</tr>
					<tr>
						<td colspan="2" class="label">
							<input id="AgreeTerms" name="AgreeTerms" <?php echo ($techInfo["AgreeTerms"] == 1 ? "checked=\"checked\"" : "")?> type="checkbox" value="1" />
							I Agree
						</td>
					</tr>
					<tr>
						<td colspan="2" class="label">&nbsp;
							
						</td>
					</tr>
					<tr>
						<td class="label">Account Type</td>
						<td>
							<input id="DepositTypeChecking" name="DepositType" type="radio" value="Checking" <?php echo ($techInfo["DepositType"] == "Checking" ? "checked=\"checked\"" : "")?>/>Checking&nbsp;
							<input id="DepositTypeSavings" name="DepositType" type="radio" value="Savings" <?php echo ($techInfo["DepositType"] == "Savings" ? "checked=\"checked\"" : "")?>/>Savings&nbsp;
							<input id="DepositTypeNone" name="DepositType" type="radio" value="None" <?php echo ($techInfo["DepositType"] != "Savings" && $techInfo["DepositType"] != "Checking" ? "checked=\"checked\"" : "")?>/>None
						</td>
					</tr>
					<tr>
						<td class="label">Bank Name</td>
						<td><input id="BankName" name="BankName" type="text" size="30" value="<?=$techInfo["BankName"]?>" /></td>
					</tr>
					<tr>
						<td class="label">Address 1</td>
						<td><input id="BankAddress1" name="BankAddress1" type="text" size="25" value="<?=$techInfo["BankAddress1"]?>" /></td>
					</tr>
					<tr>
						<td class="label">Address 2</td>
						<td><input id="BankAddress2" name="BankAddress2" type="text" size="25" value="<?=$techInfo["BankAddress2"]?>" /></td>
					</tr>
					<tr>
						<td class="label">City</td>
						<td><input id="BankCity" name="BankCity" type="text" size="25" value="<?=$techInfo["BankCity"]?>" /></td>
					</tr>
					<tr>
						<td class="label">State</td>
						<td>
							<select id="BankState" name="BankState">
								<?=$statesHTML?>
							</select>
						</td>
					</tr>
					<tr>
						<td class="label">Zip</td>
						<td><input id="BankZip" name="BankZip" type="text" size="15" value="<?=$techInfo["BankZip"]?>" /></td>
					</tr>
					<tr>
						<td class="label">Country</td>
						<td>
							<select id="BankCountry" name="BankCountry" onchange="onChangeCountry()">
								<?=$countriesHTML?>
							</select>
						</td>
					</tr>
					<tr>
						<td class="label">Name on Account</td>
						<td><input id="AccountName" name="AccountName" type="text" size="25" value="<?=$techInfo["AccountName"]?>" /></td>
					</tr>
					<tr>
						<td class="label">Routing#</td>
						<td><input id="RoutingNum" name="RoutingNum" type="text" size="10" maxlength="9" value="<?=$techInfo["MaskedRoutingNum"]?>" /></td>
					</tr>
					<tr>
						<td class="label">Account#</td>
						<td><input id="AccountNum" name="AccountNum" type="text" size="17" maxlength="17" value="<?=$techInfo["MaskedAccountNum"]?>" /></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="footer" style="text-align: center">
				<input id="submit" name="submit" type="submit" value="Update" class="caspioButton" />
			</td>
		</tr>
	</table>
	
	</form>
	

<script type="text/javascript">
function paymentMethodChange() {
	frm = document.forms.BankingInfo;
	if (frm.PaymentMethod.value == "Check") {
		frm.AgreeTerms.checked = false;
		document.getElementById("DepositTypeChecking").checked = false;
		document.getElementById("DepositTypeSavings").checked = false;
		document.getElementById("DepositTypeNone").checked = false;
		frm.BankName.value = "";
		frm.BankAddress1.value = "";
		frm.BankAddress2.value = "";
		frm.BankCity.value = "";
		frm.BankState.selectedIndex = 0;
		frm.BankZip.value = "";
		frm.BankCountry.selectedIndex = 0;
		frm.AccountName.value = "";
		frm.RoutingNum.value = "";
		frm.AccountNum.value = "";
	}
}

function validate() {
	USTerritories = {"United States" : 1, "United States Minor Outlying Islands" : 1, "Guam" : 1, "Northern Mariana Islands" : 1, "Puerto Rico" : 1, "Virgin Islands (U.S.)" : 1};
	frm = document.forms.BankingInfo;
	if (frm.PaymentMethod.value != "Direct Deposit") return true;
	if (!frm.AgreeTerms.checked) {
		frm.AgreeTerms.focus();
		alert("Please accept the direct deposit terms");
		return false;
	}
	if (!document.getElementById("DepositTypeChecking").checked && !document.getElementById("DepositTypeSavings").checked) {
		alert("Please select an account type");
		return false;
	}
	if (frm.BankName.value == "") {
		frm.BankName.focus();
		alert("Please enter the bank's name");
		return false;
	}
	if (frm.BankAddress1.value == "") {
		frm.BankAddress1.focus();
		alert("Please enter the bank's address");
		return false;
	}
	if (frm.BankCity.value == "") {
		frm.BankCity.focus();
		alert("Please enter the bank's city");
		return false;
	}
	if (frm.BankZip.value == "") {
		frm.BankZip.focus();
		alert("Please enter the bank's zip code");
		return false;
	}
	if (frm.AccountName.value == "") {
		frm.AccountName.focus();
		alert("Please enter the name on the account");
		return false;
	}
	country = frm.BankCountry.options[frm.BankCountry.selectedIndex].innerHTML;
	state = frm.BankState.options[frm.BankState.selectedIndex].innerHTML;
	routing = frm.RoutingNum.value;
	requiredLength = (USTerritories[country] == 1 ? 9 : 8);
	if (state.indexOf("Select") != -1) {
		frm.BankState.focus();
		alert("Please select a valid state");
		return false;
	}
	if (country.indexOf("Select") != -1 || country == "") {
		frm.BankCountry.focus();
		alert("Please select a valid country");
		return false;
	}
	if (routing.length != requiredLength || !isNumeric(routing)) {
		frm.RoutingNum.focus();
		alert("Routing number should be a " + requiredLength + " digit number for the selected country");
		return false;
	}
	if (frm.AccountNum.value == accountMask) {
		frm.AccountNum.focus();
		alert("Please reenter your entire account number");
		return false;
	}
	return true;
}

var accountMask = document.forms.BankingInfo.AccountNum.value;

function onChangeCountry() {
    $.ajax({
        url         : '/widgets/dashboard/js/state-info/',
        dataType    : 'json',
        data        : {country: $("#BankCountry").val()},
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            if (data) {
                //AUTOFILL
		prev = $("#BankState").val();
		if (!prev) prev = "";
				var option = '';
				option += '<option ' + (prev == "" ? "selected=\"selected\"" : "") + ' value="">Select State</option>';
                for(index in data) {
					option += '<option value="' + index + '" ' + (prev == index ? "selected=\"selected\"" : "") + '>' + data[index] + '</option>';
				}
				$("#BankState").html(option);
			}
        }
    });	
}


</script>

</div>

<!--- End Content --->
<?php require ("../footer.php"); ?>
		
