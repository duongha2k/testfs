<?php
$page = 'admin';
$option = "fsplusone";
$selected = "invoicePoints";
require_once("../header.php");
require_once("../navBar.php");
require_once("../library/caspioAPI.php");

$db = Core_Database::getInstance();
$select = $db->select();

$sort = empty($_REQUEST['sort']) ? 0 : $_REQUEST['sort'];

$select->from(array('p' => 'FSPlusOnePoints'), array('FS_ID', 'FSPlusOneID', 'actionID', 'WIN_NUM', 'points', 'applied', 'dateCreated', 'dateUpdated'))
->joinLeft(array('a' => 'FSPlusOneActions'), 'actionID = `a`.id', array('action'))
->where('`p`.usertype = ?', 'tech')
->order('FS_ID ASC')
->order('dateCreated ASC');

$result = $db->fetchAll($select);

$techMap = array();
$techMapAvailable = array();
$techMapLifetime = array();
$techMapAdjustment = array();
$techMapApplied = array();

$currentID = 0;
$available = 0;
$applied = 0;
$adjustment = 0;
$lifetime = 0;
$lifetimeTotal = 0;
foreach ($result as $row){
	if ($row['FS_ID'] == 0) continue;
	if ($row['FS_ID'] != $currentID){
		$techMap[$row['FS_ID']] = array();
		if ($currentID != 0) {
			$techMapApplied[$currentID] = $applied;
			$techMapAdjustment[$currentID] = $adjustment;
			$techMapAvailable[$currentID] = $available;
			$techMapLifetime[$currentID] = $lifetime;
			$lifetimeTotal += $lifetime;
			$available = 0;
			$applied = 0;
			$adjustment = 0;
			$lifetime = 0;
		}
		$currentID = $row['FS_ID'];
	}
	$points = intval($row['points']);
	$isAdj = false;
	$isAdj = empty($row['actionID']);
	if ($row['applied'] == 1) $applied += $points;
	else $available += $points;
	if ($isAdj) {
		$adjustment += $points;
		$action = 'Invoice Adjustment';
	}
	else {
		$lifetime += $points;
		$action = $row['action'];
	}

	$techMap[$row['FS_ID']][] = array(
		'FS_ID' => $row['FS_ID'],
		'dateCreated' => $row['dateCreated'],
		'points' => $row['points'],
		'applied' => $row['applied'],
		'WIN_NUM' => $row['WIN_NUM'],
		'action' => $action);
}
$techMapApplied[$currentID] = $applied;
$techMapAdjustment[$currentID] = $adjustment;
$techMapAvailable[$currentID] = $available;
$techMapLifetime[$currentID] = $lifetime;
$lifetimeTotal += $lifetime;

//print_r($techMapAvailable);die();

unset($result);
unset($row);

$map = NULL;
switch ($sort) {
	case 1:
		arsort($techMapLifetime);
		$map = $techMapLifetime;
	case 2:
		arsort($techMapAvailable);
		$map = $techMapAvailable;
		break;
	default:
		$map = $techMap;
}
?>
<script type='text/javascript'>
function changeSort() {
	document.location = '<?=$_SERVER['PHP_SELF']?>?sort=' + $('#sortSelect').val();
}
</script>
Sort by:<select id='sortSelect' onchange="changeSort()">
	<option value='0' <?=$sort == 0 ? "selected" : ''?>>Tech ID</option>
	<option value='1' <?=$sort == 1 ? "selected" : ''?>>Lifetime</option>
	<option value='2' <?=$sort == 2 ? "selected" : ''?>>Available</option>
</select>
<br/><br/>
<?="Total Lifetime: $lifetimeTotal"?>
<br/><br/>
<?php
foreach ($map as $tech => $value) {
	$points = $techMap[$tech];
?>
<table>
<tr><td>Tech ID</td><td>Date Created</td><td>Points</td><td>Applied?</td><td>WIN#</td><td>Action</td></tr>
<?php
	foreach ($points as $row) {
?>
<tr><td><?=$row['FS_ID']?></td><td><?=$row['dateCreated']?></td><td><?=$row['points']?></td><td><?=$row['applied']?></td><td><?=$row['WIN_NUM']?></td><td><?=$row['action']?></td></tr>
<?php
	}
?>
<tr><td colspan='6'><hr/></td></tr>
<tr><td colspan='6'>Applied: <?=$techMapApplied[$tech]?></td></tr>
<tr><td colspan='6'>Adjustment: <?=$techMapAdjustment[$tech]?></td></tr>
<tr><td colspan='6'>Lifetime: <?=$techMapLifetime[$tech]?></td></tr>
<tr><td colspan='6'>Available: <?=$techMapAvailable[$tech]?></td></tr>
</table>
<br/><br/>
<?php
}
?>
