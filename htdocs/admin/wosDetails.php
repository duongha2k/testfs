<?php
    $page   = 'admin';
    $option = 'dashboard';
    require_once ("../headerStartSession.php");


    $_GET['id'] = ( !empty($_GET['id']) ) ? $_GET['id'] : NULL;
    if ( empty($_GET['id']) ) {
        header( 'Location: http'.($_SERVER['HTTPS']=='on' ? 's':'').'://'.$_SERVER['HTTP_HOST'].'/admin/wos.php?v='.$Company_ID );
    }

    $siteTemplate = ( !empty($_SESSION['template']) ) ? $_SESSION['template'] : NULL;

    require '../header.php';


    $_SERVER['HTTPS'] = ( !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ) ? 'on' : 'off';
    $loggedIn   = ( !empty($_SESSION['loggedIn']) ) ? $_SESSION['loggedIn'] : NULL ;
    $Company_ID = ( !empty($_SESSION['Company_ID']) ) ? $_SESSION['Company_ID'] : NULL;

    if($loggedIn!="yes"){
        header( 'Location: http'.($_SERVER['HTTPS']=='on' ? 's':'').'://'.$_SERVER['HTTP_HOST'].'/clients/logIn.php' ); // Redirect to logIn.php
    }

    //$toolButtons = array('FindWorkOrder', 'CreateWorkOrder', 'QuickAssign', 'QuickApprove');
    require '../navBar.php';
    //require 'includes/adminCheck.php';
    //require 'includes/PMCheck.php';

    $navBarState = isset($_COOKIE['wosDetailsNavBarState']) ? $_COOKIE['wosDetailsNavBarState'] : '';
    if (!$navBarState) {
        $navBarState = 'closed';
    }
?>

<script type="text/javascript">
    window._company = '<?=$_GET['v']?>';
    //  use for load images and save it in browser cache only!!!
    var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
    var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";
</script>

<script type="text/javascript" src="/widgets/js/jquery.min.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetAdminDashboard.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetAdminWODetails.js?9720121"></script>
<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
<script type="text/javascript" src="/widgets/js/functions.js"></script>
<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<link rel="stylesheet" href="/widgets/css/popup.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.formatCurrency-1.4.0.js"></script>
<script type="text/javascript" src="wosDetails.js"></script>
<script type="text/javascript">
    function gotoTab(name) {
        location = location.toString().replace('<?=$_SERVER['SCRIPT_NAME']?>', '/admin/wos.php') + '&tab=' + name;
        return false;
    }
    /**
     *  calendarInit
     *
     *  Display filter post handler
     */
    function calendarInit() {
        $.map(['edit_StartDate', 'edit_EndDate','edit_DateNotified', 'edit_DateApproved', 'edit_DateInvoiced', 'edit_DatePaid'], function( id ) {
            //$('#' + roll.container().id + ' #' + id).focus( function() {
                //$('#calendar_div').css('z-index', ++roll.zindex);
                $('#'+id).calendar({dateFormat:'MDY/'});
            //});
        });
    }
    /**
     * calendarMove
     *
     * @param event  event
     * @access public
     * @return void
     */
    function calendarMove( input ) {
        $('#calendar_div').css('top', $(input).offset().top + $(input).height() + 6 + 'px');
        $('#calendar_div').css('left', $(input).offset().left + 'px');
        popUpCal.showFor(input);
    }
    var roll;
    var detailsWidget;
    var navWidget;
    $(document).ready(function() {
        roll = new FSPopupRoll();
        detailsWidget = new FSWidgetWODetails({container:'detailsContainer',tab:'index',win:"<?=$_GET['id']?>",reftab:"<?=$_GET['tab']?>"},roll);
        detailsWidget.show({tab:'index',params:{company:window._company,container:'detailsContainer',win:"<?=$_GET['id']?>",tab:"<?=$_GET['tab']?>"}});

    });

    function checkCalculation()
    {
		if (!$('#edit_Approved')[0].checked ) {
			alert('Must be aproved.');
			return false;
		}

		return true;
    }
</script>
<!-- Main content -->

    <div id="detailsContainer" >




<?php
/*** End Content ***/
require ("../footer.php");
?>
<script>
      var _wosDetails = null;
</script>    
