<?php
$page = 'admin';
$option = 'client';
$selected = 'clientlist';
require ("../header.php");
require ("../navBar.php");
checkAdminLogin2();

?>

<script type="text/javascript" src="/widgets/js/jquery.min.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetClientDetails.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetManager.js"></script>
<script type="text/javascript">
var wManager = new FSWidgetManager();
</script>
<script type="text/javascript" src="/widgets/js/FSPopupAbstract.js"></script>
<script type="text/javascript" src="/widgets/js/FSPopupRoll.js"></script>
<script type="text/javascript" src="/widgets/js/functions.js"></script>
<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
<link rel="stylesheet" href="/widgets/css/popup.css" type="text/css" />
<script type="text/javascript" src="/widgets/js/jquery.formatCurrency-1.4.0.js"></script>

<!--901-->
<style type="text/css">@import url(../library/ui.datepicker.css);</style>
<script type="text/javascript" src="../library/ui.datepicker.js"></script>
<!--end 901-->
<script type="text/javascript">
var __image__ = new Image(); __image__.src = "/widgets/images/wait.gif";
var __image_bg__ = new Image(); __image_bg__.src = "/widgets/images/wait_small.gif";

var roll;
var detailsWidget;

$('document').ready(function(){
    roll = new FSPopupRoll();
    <?php if(isset($_POST['SearchForm'])):?>
        detailsWidget = new FSWidgetClientDetails({container:'detailsContainer',tab:'adminlist'},roll);
        params = {};
        opt = {};
        <?php foreach($_POST['SearchForm'] as $param=>$val):?>
            <?php if(!empty($val)):?>
            params['<?=$param?>'] = '<?=$val?>';
            opt['<?=$param?>'] = '<?=$val?>';
            <?php endif;?>
        <?php endforeach;?>
        detailsWidget.setParams(opt);
        detailsWidget.show({params:params});
    <?php else:?>
        detailsWidget = new FSWidgetClientDetails({container:'detailsContainer',tab:'adminsearch'},roll);
        detailsWidget.show({params:{}});
    <?php endif;?>
    // add to manager
    wManager.add(detailsWidget, 'mainContainer');
    wManager._widgets.mainContainer = detailsWidget;
});
</script>

<div align="center">
<br/>
<div id="detailsContainer"></div>
</div>
<?php require ("../footer.php"); ?>