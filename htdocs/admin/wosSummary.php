<?php header("Content-Type: text/html; charset=utf-8");?>
<?php $page = 'admin'; ?>
<?php $option = 'reports'; ?>
<?php $selected = 'wosSummary'; ?>

<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<!-- Add Content Here -->
<?php
//	ini_set("display_errors", 1);
	require_once("../library/caspioAPI.php");
	checkAdminLogin2();

	if (!isset($_POST["Client"]) || $_POST["Client"] == "FLS") {
		$mainTable = "FLS_Work_Orders";			
		$projectTable = "FLS_Projects";
		$projectIDColumn = "ProjectNo";
		$projectNameColumn = "ProjectName";
		$payApproveColumn = "PayApprove";
		$startDateColumn = "PjctStartDate";
		$deactivatedQuery = "ISNULL(Deactivated, '') = ''";
		$payAmtColumn = "PayAmt";
		$kickBackQuery = "AND Kickback = '0'";
		$paidQuery = "AND Paid = 'Yes'";
		$techCompleteColumn = "TechComplete";
		$projectQuery = ($_POST["Project"] != "All" ? "AND Project_ID = '{$_POST['Project']}'" : "");
	}
	else {
		$mainTable = "Work_Orders";
		$projectTable = "TR_Client_Projects";
		$projectIDColumn = "Project_ID";
		$projectNameColumn = "Project_Name";
		$payApproveColumn = "Approved";
		$startDateColumn = "StartDate";
		$deactivatedQuery = "Deactivated = '0'";
		$payAmtColumn = "PayAmount";
		$kickBackQuery = "";
		$paidQuery = "AND TechPaid = '1'";
		$techCompleteColumn = "TechMarkedComplete";
		$projectQuery = ($_POST["Project"] != "All" ? "AND Project_ID = '{$_POST['Project']}'" : "AND Company_ID = '{$_POST['Client']}'");
	}

	if (isset($_POST["search"])) {
		if (!is_numeric($_POST["Project"]) && $_POST["Project"] != "All") die();	
							
		$dateQuery = ($_POST["startDateGT"] != "" ? "AND $startDateColumn >= '{$_POST['startDateGT']}'" : "") . ($_POST["startDateLT"] != "" ? " AND $startDateColumn <= '{$_POST['startDateLT']}'" : "");
		
		$criteria = "$deactivatedQuery $kickBackQuery $projectQuery $dateQuery";
				
		$html = "<table>";
		
		// WH11171078,'778504'

		// get approve stats
		$info = caspioSelect($mainTable, "$payApproveColumn, SUM(CAST($payAmtColumn AS money))", "$criteria GROUP BY $payApproveColumn", "$payApproveColumn ASC");
			
		$approvedInfo = array();
		foreach ($info as $parts) {
			$parts = explode(",", $parts);
			$approvedInfo[($parts[0] != "True" ? 0 : 1)] = $parts[1];
		}
		if (!isset($approvedInfo[0])) $approvedInfo[0] = 0.00;
		if (!isset($approvedInfo[1])) $approvedInfo[1] = 0.00;
		
		$unapproved = sprintf("$" . "%.2f<br/>", (isset($approvedInfo[0]) ? $approvedInfo[0] : 0.00));
		$approved = sprintf("$" . "%.2f<br/>", (isset($approvedInfo[1]) ? $approvedInfo[1] : 0.00));
		
		$html .= "<tr><td>Unapproved</td><td class=\"value\">$unapproved</td></tr>";
		$html .="<tr><td>Approved</td><td class=\"value\">$approved</td></tr>";
			
		// get paid stats
		$paidInfo = caspioSelect($mainTable, "SUM(CAST($payAmtColumn AS money))", "$criteria $paidQuery", "");
		
		$paid = sprintf("$" . "%.2f<br/>", (isset($paidInfo[0]) ? $paidInfo[0] : 0.00));
//		$unpaid = sprintf("$" . "%.2f<br/>", (isset($paidInfo[1]) ? $paidInfo[1] : 0.00));
		
		$html .= "<tr><td>Paid</td><td class=\"value\">$paid</td></tr>";
	
		// get invoice stats
		$info = caspioSelect($mainTable, "Invoiced, SUM(CAST($payAmtColumn AS money))", "$criteria GROUP BY Invoiced", "Invoiced ASC");
		
		$invoicedInfo = array();
		foreach ($info as $parts) {
			$parts = explode(",", $parts);
			$invoicedInfo[($parts[0] == "False" ? 0 : 1)] = $parts[1];
		}
		if (!isset($invoicedInfo[0])) $invoicedInfo[0] = 0.00;
		if (!isset($invoicedInfo[1])) $invoicedInfo[1] = 0.00;

		$uninvoiced = sprintf("$" . "%.2f<br/>", (isset($invoicedInfo[0]) ? $invoicedInfo[0] : 0.00));
		$invoiced = sprintf("$" . "%.2f<br/>", (isset($invoicedInfo[1]) ? $invoicedInfo[1] : 0.00));
		
		$html .= "<tr><td>Invoiced</td><td class=\"value\">$invoiced</td></tr>";
		
		// get complete stats
		$totalCount = caspioSelect("$mainTable", "COUNT($techCompleteColumn)", "$criteria GROUP BY $techCompleteColumn", "$techCompleteColumn ASC");
		
		$incomplete = (isset($totalCount[0]) ? $totalCount[0] : 0);
		$complete = (isset($totalCount[1]) ? $totalCount[1] : 0);
		$total = $incomplete + $complete;
		
		$completePercent = ($total > 0 ? sprintf("%.2f%%", (($complete / $total) * 100)) : "N/A");
		
		$html .= "<tr><td>Total number of WO</td><td class=\"value\">$total</td></tr>";
		$html .= "<tr><td>Total completed</td><td class=\"value\">$complete</td></tr>";
		$html .= "<tr><td>%Complete</td><td class=\"value\">$completePercent</td></tr>";
		$html .= "</table>";
		
	}

	// Create Project List
	$projectList = caspioSelectAdv($projectTable, "$projectNameColumn, $projectIDColumn", (!isset($_POST["Client"]) || $_POST["Client"] == "FLS" ? "" : "Project_Company_ID = '{$_POST['Client']}'"), "$projectNameColumn ASC", false, "`", "|");		
	$projectHtml = "<option value=\"All\"" . (!isset($_POST["Project"]) || $_POST["Project"] == "All" ? "selected=\"selected\"" : "") . ">All Projects</option>";
	if (sizeof($projectList) > 0 && $projectList[0] != "") {
		foreach ($projectList as $project) {
			$project = explode("|", $project);
			$name = trim($project[0], "`");
			$id = trim($project[1], "`");
			$projectHtml .= "<option value=\"$id\"" . ($_POST["Project"] == $id ? "selected=\"selected\"" : "") . ">$name</option>";		
		}
	}
	
	// Create Client List
	$clientList = caspioSelectAdv("TR_Client_List", "DISTINCT CompanyName, Company_ID", "ShowOnReports = '1' AND Company_ID <> ''", "CompanyName ASC", false, "`", "|");		
	$clientHtml = "<option value=\"FLS\"" . (!isset($_POST["Project"]) || $_POST["Project"] == "All" ? "selected=\"selected\"" : "") . ">FLS</option>";
	foreach ($clientList as $client) {
		$client = explode("|", $client);
		$name = trim($client[0], "`");
		$id = trim($client[1], "`");
		$clientHtml .= "<option value=\"$id\"" . ($_POST["Client"] == $id ? "selected=\"selected\"" : "") . ">$name</option>";		
	}
	
?>

<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('#startDateGT').calendar({dateFormat: 'MDY-'});
    $('#startDateLT').calendar({dateFormat: 'MDY-'});
  });
</script>

<style  TYPE="text/css">
.label{ font-size:12px; color:#385C7E;}
td { padding: 0px 5px; }
.value {
	text-align: right;
}
</style>

<br /><br />

<div align="center">
<h1>Work Order Summary</h1>
<?=$html?>
<form name="flsWOSummary" method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
<table border="0" cellpadding="10" cellspacing="10">
	<tr>
		<td>

			<table cellpadding="0" cellspacing="0" border="0" style="border:#385C7E solid medium;">
				<tr>
					<td>
						<table cellpadding="0" cellspacing="5">
							<tr>
								<td class="label">
									Client
								</td>
								<td>
									<select id="Client" name="Client" onChange="document.forms.flsWOSummary.Project.selectedIndex=0;document.forms.flsWOSummary.submit()">
										<?=$clientHtml?>
									</select>
								</td>
							</tr>
							<tr>
								<td class="label">
									Start Date (>=)
								</td>
								<td>
									<input id="startDateGT" name="startDateGT" type="text" size="15"  value="<?php echo $_POST["startDateGT"]?>"/>
								</td>
							</tr>
							<tr>
								<td class="label">
									Start Date (<=)
								</td>
								<td>
									<input id="startDateLT" name="startDateLT" type="text" size="15" value="<?php echo $_POST["startDateLT"]?>"/>
								</td>
							</tr>
							<tr>
								<td class="label">
									Project
								</td>
								<td>
									<select id="Project" name="Project">
										<?=$projectHtml?>
									</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="text-align:center; background-color:#BACBDF; border-top:#385C7E solid medium; padding:5px;">
						<input id="send" name="search" type="submit" value="Search" style="background-color:#385C7E; color:#FFFFFF; font-size:12px; font-weight:bold;" />
					</td>
				</tr>
			</table>

		</td>
	</tr>
</table>
</form>
</div>
<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
