<?php $page = "admin"; ?>
<?php $option = "pricing"; ?>
<?php $selected = "editPricingRule"; 
	require_once("../library/pricing.php");
	if (!empty($_GET['clone'])) {
		Core_Pricing::cloneRule($_GET['clone']);
		header('Location: ' . $_SERVER['PHP_SELF']);
		die();
	}
?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php			
	initPricingJS();
	if ($_POST) {
		if (!empty($_POST['PricingRuleID']) || !empty($_POST['AddDescription'])) {
			$id = !empty($_POST['PricingRuleID']) ? $_POST['PricingRuleID'] : 0;
			$description = !empty($_POST['AddDescription']) ? $_POST['AddDescription'] : $_POST['Description'];
			Core_Pricing::saveRule($id, array('Description'=>$description));
		}
	}
?>
<style type="text/css" title="currentStyle">
			@import "/widgets/js/datatables/css/demo_page.css";
			@import "/widgets/js/datatables/css/demo_table.css";
			@import "/widgets/js/datatables/css/datatable_theme.css";
		</style>

<script  src="/widgets/js/datatables/js/jquery.dataTables.js"></script>

<script type="text/javascript" src="../library/php.namespaced.min.js"></script>

<script type="text/javascript">
//	var inputValues = {"2" : 110.00, "3" : 5, "4" : "TRUE"};
	
	function displayCalc(data) {
		$("#calculations").html(data);
	}
	
	function runCalculation() {
//		$("#inputFields").html("http://www.fieldsolutions.com/library/pricingCalculate.php?id=1&values=" + $P.serialize(inputValues));
		var combinedFields = combineInputValues();
		var inputFields = {};
		if (combinedFields)
			inputFields = {"values" : $P.serialize(combinedFields)};
//		$("#test").html("https://www.fieldsolutions.com/library/pricingSampleCalculate.php?values=" + $P.serialize(combinedFields));
		$.get("../library/pricingSampleCalculate.php", inputFields, displayCalc);
	}

	function loadChargeTypeList(id) {
		$("#pricingRulesTable_wrapper").hide();
			$("#chargeTypeList").attr("src", "viewEditChargeTypes.php?id="+id + "&rand=" + Math.random() * 5000);
			$("#applyWhenHelperLeft").change(showApplyHelperRight);
			$("#applyWhenHelperCreate").click(createApplyWhen);
			showApplyHelperRight();
			applyWhen = $("#applyWhenHelper");
		applyWhen.show();
		$("#PricingRuleID").val(id);
		$("#RuleID").html(id);
		$("#Description").val(rules[id]['Description']);
		$("#ParentID").html(rules[id]['ParentID']);
		$("#CloneDate").html(rules[id]['CloneDate']);
		$("#ruleDetails").show();
	}
	
	function showApplyHelperRight() {
		fieldID = $("#applyWhenHelperLeft").attr("value");
		type = pricingFieldList[fieldID][2];
		if (type == "Yes/No") {
			$("#applyWhenHelperRightYesNo").css("display", "");
			$("#applyWhenHelperRightText").css("display", "none");
		}
		else {
			$("#applyWhenHelperRightYesNo").css("display", "none");
			$("#applyWhenHelperRightText").css("display", "");
		}
	}
	
	function createApplyWhen() {
		fieldID = $("#applyWhenHelperLeft").attr("value");
		operator = $("#applyWhenHelperOperator").attr("value");
		type = pricingFieldList[fieldID][2];
		result = "@pricingField:" + fieldID + operator;
		rightValue = (type == "Yes/No" ? $("#applyWhenHelperRightYesNo").attr("value") : $("#applyWhenHelperRightText").attr("value"));
		if (rightValue == undefined)
			rightValue = "";
		result += "\"" + rightValue + "\"";
		$("#applyWhenHelperResult").attr("value", result);
	}
	
	function resizeFrame(width, height, me) {
		myFrame = document.getElementById("chargeTypeList");
		try {
			// FF version
//			myFrame.style.width = (width + 40) + "px";
			myFrame.style.height = (height + 20) + "px";
		}
		catch (e) {
			// IE version
			try {
//				document.all.myFrame.style.width = (width + 40) + "px";
				document.all.myFrame.style.height = (height) + "px";
			}
			catch (e) {
			}
		}
		window.scrollTo(0,0);
		runCalculation();
	}
	
	function addRule() {
		$("#ruleForm").submit();
	}
</script>
<style type="text/css">
	#chargeTypeList {
		width: 95%;
		height: 100%;
	}
	.totalCalculated {
		font-weight: bold;
	}
</style>
<br/>
<input type="button" id="downloadButton" value="Download" class="link_button download_button" onclick="document.location.href = '/widgets/admindashboard/client/list-pricing-rule/download/csv/all/1'">
<br/>
<div align="center">
<form id="ruleForm" method="post" action="<?=$_SERVER['PHP_SELF']?>">
<table cellspacing='1' id='pricingRulesTable' class='tablesorter'><thead><tr><th>Rule ID</th><th>Description</th><th>Parent ID</th><th>Clone Date</th><th></th></tr></thead><tbody>
<?php
	$rules = Core_Pricing::getRules();
	foreach ($rules as $id=>$rule) {
?>
		<tr>
        	<td><?=$id?></td><td><?=htmlentities($rule['Description'])?></td><td><?=$rule['ParentID']?></td><td><?=$rule['CloneDate']?></td><td><a href="javascript:void(0)" onclick="document.location = '<?=$_SERVER['PHP_SELF']?>?clone=<?=$id?>'">Clone Rule</a></td>
        </tr>
<?php
	}
?>
		<tr>
        	<td>0</td><td><input type="text" id="AddDescription" name="AddDescription" /></td><td></td><td></td><td><a href="javascript:addRule()">Add</a></td>
        </tr>
</tbody></table>
</form>
<script type="text/javascript">
	var rules = <?=json_encode($rules)?>;
	var table;
	$(document).ready(function() {
		table = $('#pricingRulesTable').dataTable({
				"aoColumnDefs": [
					{ "bSortable": false, "aTargets": [ 4 ]},												
					{ "bSearchable": false, "aTargets": [ 4 ]},												
					{ "bUseRendered": false, "aTargets": [ 0 ]},
				],
				"aaSorting": [[0, 'asc']],
				"iDisplayLength": 100,
				"fnRowCallback": function(row, data, index, indexFull) { 
						if (data[0] == 0) return row;
						$(row).children().eq(0).html("<a href='javascript:loadChargeTypeList(" + data[0] + ")'>" + data[0] + "</a>"); 
						return row; 
				}
				
				
		});
	});
</script>
	<div id="ruleDetails" style="display:none">
        <form action="<?=$_SERVER['PHP_SELF']?>" onSubmit="" method="post" id="UpdateForm" name="UpdateForm">
            <input type="hidden" id="PricingRuleID" name="PricingRuleID" value="" />
            <table class="form_table" cellspacing="0">
                <col class="col1" />
                <col class="col2" />
                <tr>
                    <td class="label_field">Rule ID</td>
                    <td id="RuleID"></td>
                </tr>			
                <tr>
                    <td class="label_field">Description</td>
                    <td><input type="text" maxlength="255" class="input_text" id="Description" name="Description" value="" /></td>
                </tr>			
                <tr>
                    <td class="label_field">Parent ID</td>
                    <td id="ParentID"></td>
                </tr>			
                <tr>
                    <td class="label_field">Clone Date</td>
                    <td id="CloneDate"></td>
                </tr>			
                <tr align="center" style="background-color:#BACBDF;">
                    <td colspan="2" style="border-top:2px solid #032D5F;">
                        <input type="button" value="Cancel" onclick="document.location='<?=$_SERVER['PHP_SELF']?>';"/>
                        <input type="Submit" value="Submit" />
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <br/>
    <div id="applyWhenHelper" style="display: none">
    	<hr />
       	<?php 
/*			ini_set("display_errors",1);
			preg_match_all("(@pricingField:([\d]+))", "@pricingField:20==TRUE && @pricingField:25==FALSE", $sub, PREG_SET_ORDER | PREG_OFFSET_CAPTURE);
			print_r($sub[0]);echo "<br/><br/>";
			print_r($sub[1]);*/
		?>

    	Apply When Helper<br/>
    	<?=createSelectPricingFields("applyWhenHelperLeft")?>
        &nbsp;&nbsp;
        <select id="applyWhenHelperOperator">
        	<option value="==">=</option>
            <option value="!=">NOT =</option>
            <option value="&gt;=">&gt;=</option>
            <option value="&lt;=">&lt;=</option>
            <option value="&gt;">&gt;</option>
            <option value="&lt;">&lt;</option>
		</select>
        &nbsp;&nbsp;
        <select id="applyWhenHelperRightYesNo" style="display: none">
        	<option value="TRUE">Yes</option>
            <option value="FALSE">No</option>
        </select>
        <input id="applyWhenHelperRightText" type="text" style="display: none; width: 53px" />
        <input id="applyWhenHelperCreate" type="button" value="Create"/>
        <br/>
        <br/>
        Result:
        <input id="applyWhenHelperResult" type="text" style="width: 300px" />
        <br/>
        <hr />
        <br/>
        <br/>
    </div>
    <iframe name="chargeTypeList" id="chargeTypeList" src="../blank.html">
    </iframe>
</div>

<div id="calculationBlock">
	<div id="inputFields">
    </div>
    <div id="calculations">
    </div>
</div>
<div id="test"></div>
<?php require ("../footer.php"); ?>