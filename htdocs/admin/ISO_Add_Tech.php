<?php $page = 'admin'; ?>
<?php $option = 'ISO'; ?>
<?php $selected = 'addISO_Tech'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php

require_once("../library/caspioAPI.php");


checkAdminLogin2();
	

	$httpReferer = (isset($_GET['referer']) ? urldecode($_GET['referer']) : $_SERVER["HTTP_REFERER"]);
	$saveReferer = $_SESSION["saveReferer"];
	if ($_SESSION["saveReferer"] != $httpReferer)
		$_SESSION["saveReferer"] = $httpReferer;

require_once("../headerStartSession.php");
require_once dirname(__FILE__).'/../../includes/modules/common.init.php';

?>
<script type="text/javascript" src="/widgets/js/jquery.validate.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.curvycorners.js"></script>
<link rel="stylesheet" type="text/css" href="/widgets/js/star-rating/jquery.rating.css" />

<script type="text/javascript" src="/widgets/js/fancybox/jquery.easing.pack.js"></script>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox-mod.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.jqEasyCharCounter.js"></script>
<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidget.js"></script>
<script type="text/javascript" src="/widgets/js/FSWidgetDashboard.js"></script>
<script type="text/javascript" src="/widgets/js/FSTechProfile.js"></script>
<link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" media="all" />

<script type="text/javascript">
var techRegValidator = "";
$(document).ready(function(){

//	showLoader();
	profile = new FSTechProfile({container: 'wr', tab: 'admincreate'});
	profile.show({tab: 'admincreate', params: {container: 'wr'}});
		
	var techID = '<?=$_SESSION['TechID'];?>';

});//end document.ready

	function submitBusinessProfile(form){
		if($("#deleteResume").attr("checked") == true){
			deleteResume();
		}
		
		if($("#resumeUpload").val() != ""){

			$.ajaxFileUpload({
		        url             : "/widgets/dashboard/do/save-business-profile/?techID="+$("#BizProfTechID").val(),
		        secureuri       : false,
		        fileElementId   : 'resumeUpload',
		        dataType        : 'json',
		        cache           : false,
		        success         : function (data, status, xhr) {
					var submitData = $(form).serialize();

				  	$("#yesResume").show();
					//$("#noResume").hide();
					$("#resumeFile").html("<a href='/widgets/dashboard/do/file-tech-documents-download/filename/"+data.uploadResult.encodedFilename+"/"+data.uploadResult.fileName+">"+data.uploadResult.displayName+"</a>");
					$("#resumeUpload").val("");
				  	$.ajax({
						type: "POST",
						url: "/widgets/dashboard/do/save-business-profile/",
				        dataType    : 'json',
				        cache       : false,
						data: submitData,
						success: function (data) {
						
								var messageHtml = "<h2>Update Complete</h2>";
								messageHtml += "<p>Your Business Profile has been successfully updated.</p>";
								messageHtml += "<b>Thank you!</b>";
								
								hideLoader();
								
								$("<div></div>").fancybox({
										'width': 350,
										'height' : 125,
										'content' : messageHtml,
										'autoDimensions' : false,
										'centerOnScroll' : true
								}).trigger('click');
						}
					});
		        },
		        error:function(data, status, xhr){
		        	$("#yesResume").hide();
					//$("#noResume").show();
		        	var messageHtml = "<h2>Update Error</h2>";
					messageHtml += "<p>We encounted an error when attempting to update your business profile.</p>";
					messageHtml += "<p>Please check the data you entered and try again.</p>";
					messageHtml += "<b>Thank you!</b>";
					
					hideLoader();
					
					$("<div></div>").fancybox({
							'width': 350,
							'height' : 125,
							'content' : messageHtml,
							'autoDimensions' : false,
							'centerOnScroll' : true
					}).trigger('click');
		        }
		    });

		}else{
			var submitData = $(form).serialize();
		  	
		  	$.ajax({
				type: "POST",
				url: "/widgets/dashboard/do/save-business-profile/",
		        dataType    : 'json',
		        cache       : false,
				data: submitData,
				success: function (data) {
						
						var messageHtml = "<h2>Update Complete</h2>";
						messageHtml += "<p>Your Tech Profile has been successfully updated.</p>";
						messageHtml += "<b>Thank you!</b>";
						
						hideLoader();
						
						$("<div></div>").fancybox({
								'width': 350,
								'height' : 125,
								'content' : messageHtml,
								'autoDimensions' : false,
								'centerOnScroll' : true
						}).trigger('click');
				}
			});
		}
	}

	function submitPaymentInfo(form){

		if($('input[name="RoutingNum"]').val().search("X") != "-1"){
			$(form)[0]['RoutingNum'] = null;
		}

		if($('input[name="AccountNum"]').val().search("X") != "-1"){
			$(form)[0]['AccountNum'] = null;
		}
		
		var submitData = $(form).serialize();

	  	$.ajax({
			type: "POST",
			url: "/widgets/dashboard/do/submit-payment-info/",
	        dataType    : 'json',
	        cache       : false,
			data: submitData,
			success: function (data) {
			
					var messageHtml = "<h2>Update Complete</h2>";
					messageHtml += "<p>Your Payment Info has been successfully updated.</p>";
					messageHtml += "<b>Thank you!</b>";
					
					hideLoader();
					
					$("<div></div>").fancybox({
							'width': 350,
							'height' : 125,
							'content' : messageHtml,
							'autoDimensions' : false,
							'centerOnScroll' : true
					}).trigger('click');
			},
			error: function (data){
				
			}
		});
	}

	function submitUpdateTechInfo(form){
		if($("#badgePhotoUpload").val() != ""){
			
			$.ajaxFileUpload({
		        url             : "/widgets/dashboard/do/save-business-profile/?techID="+$("#basicRegTechID").val(),
		        secureuri       : false,
		        fileElementId   : 'badgePhotoUpload',
		        dataType        : 'json',
		        cache           : false,
		        success         : function (data, status, xhr) {
					var submitData = $(form).serialize();
					$("#badgeUpload").hide();
					$("#saveBadgePhoto").hide();
					$("#deleteBadgePhoto").show();
					$("#badgePhotoImage").attr("src","https://s3.amazonaws.com/tech-docs/profile-pics/"+data.uploadResult.fileName);					
					$("#badgePhotoUpload").val("");
					
				  	$.ajax({
						type: "POST",
						url: "/widgets/dashboard/do/save-business-profile/",
				        dataType    : 'json',
				        cache       : false,
						data: submitData,
						success: function (data) {
						
								var messageHtml = "<h2>Update Complete</h2>";
								messageHtml += "<p>Your Business Profile has been successfully updated.</p>";
								messageHtml += "<b>Thank you!</b>";
								
								hideLoader();
								
								$("<div></div>").fancybox({
										'width': 350,
										'height' : 125,
										'content' : messageHtml,
										'autoDimensions' : false,
										'centerOnScroll' : true
								}).trigger('click');
						}
					});
		        },
		        error:function(data, status, xhr){
		        	var messageHtml = "<h2>Update Error</h2>";
					messageHtml += "<p>We encounted an error when attempting to update your business profile.</p>";
					messageHtml += "<p>Please check the data you entered and try again.</p>";
					messageHtml += "<b>Thank you!</b>";
					
					hideLoader();
					$("#badgeUpload").show();
					$("#deleteBadgePhoto").hide();
					$("#saveBadgePhoto").show();
					$("<div></div>").fancybox({
							'width': 350,
							'height' : 125,
							'content' : messageHtml,
							'autoDimensions' : false,
							'centerOnScroll' : true
					}).trigger('click');
		        }
		    });

		}else{
		  	var submitData = $(form).serialize();
				$.ajax({
					type: "POST",
					url: "/widgets/dashboard/do/update-tech-info/",
			        dataType    : 'json',
			        cache       : false,
					data: submitData,
					success: function (data) {
							
							var messageHtml = "<h2>Update Complete</h2>";
							messageHtml += "<p>Your Tech Profile has been successfully updated.</p>";
							messageHtml += "<b>Thank you!</b>";
							
							hideLoader();
							
							$("<div></div>").fancybox({
									'width': 350,
									'height' : 125,
									'content' : messageHtml,
									'autoDimensions' : false,
									'centerOnScroll' : true
							}).trigger('click');
					}
				});
		}
  }


function savePaymentInfo(){
	$("#paymentInfo").submit();
}

function saveBusinessProfile(){
	$("#businessProfile").submit();
}


function showLoader(){
	profile.showLoader();
}

function hideLoader(){
	profile.hideLoader();
}

function deleteResume(){
	showLoader();
	$.ajax({
		type: "POST",
		url: '/widgets/dashboard/do/delete-tech-file/',
        dataType    : 'json',
        cache       : false,
		data: {file:$('body').data("resume"), type: "resume"},
		success: function (data) {
			hideLoader();
			$("#yesResume").hide();
			$("#resumeFile").html("");
		 },
	    error:function(data, status, xhr){
			hideLoader();
			$("#yesResume").show();
			//$("#noResume").hide();
		 }
	});
}

function deleteBadgePhoto(){
	showLoader();
	$.ajax({
		type: "POST",
		url: '/widgets/dashboard/do/delete-tech-file/',
        dataType    : 'json',
        cache       : false,
		data: {file:$('body').data("profilePic"), type: "profile pic"},
		success: function (data) {
			hideLoader();
			$("#badgePhotoImage").attr("src","/widgets/images/profileAnon.jpg" );
			$("#badgeUpload").show();
			$("#deleteBadgePhoto").hide();
			$("#saveBadgePhoto").show();
		 },
	    error:function(data, status, xhr){
			hideLoader();
			$("#badgeUpload").hide();
			$("#deleteBadgePhoto").show();
			$("#saveBadgePhoto").hide();
		 }
	});
}

function setPaymentInfo(){
	
	if($("#paymentAddressSame").attr("checked") == true){
		$("#paymentInfo, #BankCountry").val($('body').data("country"));
		$("#paymentInfo, #BankAddress1").val($('body').data("address1"));
		$("#paymentInfo, #BankAddress2").val($('body').data("address2"));
		$("#paymentInfo, #BankCity").val($('body').data("city"));
		$("#paymentInfo, #BankState").val($('body').data("state"));
		$("#paymentInfo, #BankZip").val($('body').data("zip"));
	}else{
		$("#paymentInfo, #BankCountry").val("");
		$("#paymentInfo, #BankAddress1").val("");
		$("#paymentInfo, #BankAddress2").val("");
		$("#paymentInfo, #BankCity").val("");
		$("#paymentInfo, #BankState").val("");
		$("#paymentInfo, #BankZip").val("");
	}
}

function applyFancybox(message){
	$("<div></div>").fancybox({
		'width': 350,
		'height' : 125,
		'content' : message,
		'autoDimensions' : false,
		'centerOnScroll' : true
	}).trigger('click');
}
function saveBadgePhoto(){
	
	if($("#badgePhotoUpload").val() != ""){
		
		$.ajaxFileUpload({
	        url             : "/widgets/dashboard/do/save-business-profile/?techID="+$("#basicRegTechID").val(),
	        secureuri       : false,
	        fileElementId   : 'badgePhotoUpload',
	        dataType        : 'json',
	        cache           : false,
	        success         : function (data, status, xhr) {
				
				$("#badgeUpload").hide();
				$("#saveBadgePhoto").hide();
				$("#deleteBadgePhoto").show();
				$("#badgePhotoImage").attr("src","/widgets/images/profileAnonPending.jpg" );					
				$("#badgePhotoUpload").val("");
				var messageHtml = "<h2>Update Complete</h2>";
				messageHtml += "<p>Your Business Profile has been successfully updated.</p>";
				messageHtml += "<b>Thank you!</b>";
				
				hideLoader();
				applyFancybox(messageHtml);
				
	        },
	        error:function(data, status, xhr){
	        	var messageHtml = "<h2>Update Error</h2>";
				messageHtml += "<p>We encounted an error when attempting to update your business profile.</p>";
				messageHtml += "<p>Please check the data you entered and try again.</p>";
				messageHtml += "<b>Thank you!</b>";
				
				hideLoader();
				$("#badgeUpload").show();
				$("#deleteBadgePhoto").hide();
				$("#saveBadgePhoto").show();
				applyFancybox(messageHtml);
	        }
	    });

	}
}

function saveEquipment(){
	showLoader();
	var sendData = [];
	var telephonyData = [];
	$("input[name='basicEquipment[]']").each(function(){
		if($(this).attr("checked") == true){
			sendData.push({column:this.value,value:"1"});
		}else{
			sendData.push({column:this.value,value:"0"});
		}
	});

	$("input[name='cablingEquipment[]']").each(function(){
		if($(this).attr("checked") == true){
			sendData.push({column:this.value,value: this.value,checked:"1"});
		}else{
			sendData.push({column:this.value,value: this.value,checked:"0"});
		}
	});

	$("input[name='telephonySkills[]']").each(function(){
		if($(this).attr("checked") == true){
			telephonyData.push({column:this.value,value: this.value,checked:"1"});
		}else{
			telephonyData.push({column:this.value,value: this.value,checked:"0"});
		}
	});

	if($("#vehicleImage").val() != "" && $("#vehicleID").val() != ""){
		$.ajax({
			type: "POST",
			url: '/widgets/dashboard/do/delete-tech-file/',
	        dataType    : 'json',
	        cache       : false,
			data: {file:$("#vehicleCurrentImage").val(), type: "profile pic"},
			success: function (data) {
				
			 },
		    error:function(data, status, xhr){
				
			 }
		});
	}
	
	if($("#vehicleImage").val() != ""){
		
		$.ajaxFileUpload({
	        url             : "/widgets/dashboard/do/save-equipment/?techID="+$("#basicRegTechID").val()+"&vehicleDesc="+$("#vehicleDesc").val(),
	        secureuri       : false,
	        fileElementId   : 'vehicleImage',
	        dataType        : 'json',
	        cache           : false,
	        success         : function (data, status, xhr) {
			
				$.ajax({
					type: "POST",
					url: '/widgets/dashboard/do/save-equipment/',
			        dataType    : 'json',
			        cache       : false,
					data: {data:JSON.stringify(sendData), 
						   techID: $("#basicRegTechID").val(), 
						   otherTools: $("#Tools").val(), 
						   telephonyData: JSON.stringify(telephonyData)
						   },
					success: function (data) {
						hideLoader();
						var messageHtml = "<h2>Update Complete</h2>";
						messageHtml += "<p>Your Business Profile has been successfully updated.</p>";
						messageHtml += "<b>Thank you!</b>";
						applyFancybox(messageHtml);
						
					 },
				    error:function(data, status, xhr){
						hideLoader();
						var messageHtml = "<h2>Update Error</h2>";
						messageHtml += "<p>We encounted an error when attempting to update your business profile.</p>";
						messageHtml += "<p>Please check the data you entered and try again.</p>";
						messageHtml += "<b>Thank you!</b>";
						applyFancybox(messageHtml);
					 }
				});
	        },
	        error:function(data, status, xhr){
	        	var messageHtml = "<h2>Update Error</h2>";
				messageHtml += "<p>We encounted an error when attempting to update your business profile.</p>";
				messageHtml += "<p>Please check the data you entered and try again.</p>";
				messageHtml += "<b>Thank you!</b>";
				
				hideLoader();
				applyFancybox(messageHtml);
	        }
	    });

	}else{

		$.ajax({
			type: "POST",
			url: '/widgets/dashboard/do/save-equipment/',
	        dataType    : 'json',
	        cache       : false,
			data: {data:JSON.stringify(sendData), 
				   techID: $("#basicRegTechID").val(), 
				   otherTools: $("#Tools").val(),
				   telephonyData: JSON.stringify(telephonyData)
				   },
			success: function (data) {
				hideLoader();
				var messageHtml = "<h2>Update Complete</h2>";
				messageHtml += "<p>Your Business Profile has been successfully updated.</p>";
				messageHtml += "<b>Thank you!</b>";
				applyFancybox(messageHtml);
				
			 },
		    error:function(data, status, xhr){
				hideLoader();
				var messageHtml = "<h2>Update Error</h2>";
				messageHtml += "<p>We encounted an error when attempting to update your business profile.</p>";
				messageHtml += "<p>Please check the data you entered and try again.</p>";
				messageHtml += "<b>Thank you!</b>";
				applyFancybox(messageHtml);
			 }
		});
	}
}

function editSection(tableID){
	
	if(tableID != "programCertifications" && tableID != "myCertifications"){
	
		$.each($("#"+tableID+" input[id*='Input']"),function(){
			if($(this).attr("id") != "essintialCertNumInput" && $(this).attr("id") != "essintialCertDateInput" ){
			
				var divID = $(this).attr("id").replace("Input","");
				var dateVal = $("#"+tableID+" div[id*="+divID+"Date]").html();
			
				$(this).css("display","table-cell").calendar({dateFormat:'MDY/'}).val(dateVal);
			}
			
		});
		$.each($("#"+tableID+" input"),function(){
			$(this).css("display","table-cell");
		});
		
		$.each($("#"+tableID+" div[id*=Label]"),function(){
			$(this).hide();
		});
		
		$("#"+tableID+" div[id*='Date']").hide();
		
	}else if(tableID == "programCertifications"){
		$.each($("#"+tableID+" tr"),function(){
			var divID = $(this).attr("id");
			var certVal = $("#"+divID+"NumDisplay").html();
			if(isNaN(certVal)){
				certVal = "";
			}
			$("#"+divID+"NumDisplay").hide();
			$("#"+divID+"NumInput").val(certVal).css("display","table-cell");

		});
	}else{
		$.each($("#"+tableID+" tr[id*='Cert']"),function(){
				
				if($(this).attr("id") != "aPlusCert"){
					var divID = $(this).attr("id");
					
					var certVal = $("#"+divID+"NumDisplay").html();
					if(certVal.search("<img") != "-1"){
						certVal = "";
					}
		
					$("#"+divID+"NumDisplay").hide();
					$("#"+divID+"NumInput").val(certVal).css("display","table-cell");
	
					var dateVal = $("#"+divID+"DateDisplay").html();
					$("#"+divID+"DateDisplay").hide();
					$("#"+divID+"DateInput").css("display","table-cell").calendar({dateFormat:'MDY/'}).val(dateVal);
				}else{
					$("#aPlusCertNumDisplay").hide();
					$("#APlus").show();
				}
				
		});
	}

	$("#"+tableID+"Header div[class='editSection']").each(function(){
		$(this).hide();
	});

	$("#"+tableID+"Header div[class='cancelSaveSection']").each(function(){
		$(this).show();
	});
	
}

function saveSection(formID){
	var form = $("#"+formID);

	var submitData = $(form).serialize();
	var formCheckbox = $("#"+formID + " input[type='checkbox']");
		
	formCheckbox.each(function(index) {
		checked = $(this).attr('checked') ? "1" : "0";
		submitData += "&" + $(this).attr('id') + "=" + checked;
	});

	showLoader();
	
	$.ajax({
		type: "POST",
		url: "/widgets/dashboard/do/update-tech-info/",
        dataType    : 'json',
        cache       : false,
		data: submitData,
		success: function (data) {
				
				var messageHtml = "<h2>Update Complete</h2>";
				messageHtml += "<p>Your Tech Profile has been successfully updated.</p>";
				messageHtml += "<b>Thank you!</b>";
				hideLoader();
				applyFancybox(messageHtml);
		}
	});
}

function cancelSection(tableID){
	if(tableID != "programCertifications" && tableID != "myCertifications"){
		$.each($("#"+tableID+" input[id*='Input']"),function(){
			if($(this).attr("id") != "essintialCertNumInput" && $(this).attr("id") != "essintialCertDateInput"){
				var divID = $(this).attr("id").replace("Input","");
				
				var dateVal = $(this).val();
				
				$("#"+tableID+" div[id*="+divID+"Date]").html(dateVal);
				$(this).hide();
			}
		});
		
		$.each($("#"+tableID+" input"),function(){
			$(this).hide();
		});

		$.each($("#"+tableID+" div[id*=Label]"),function(){
			$(this).css("display","table-cell");
		});
		
		$("#"+tableID+" div[id*='Date']").show();
		
	}else if(tableID == "programCertifications"){
		$.each($("#"+tableID+" tr"),function(){
			var divID = $(this).attr("id");
			var certVal = $("#"+divID+"NumInput").val();
			
			if(isNaN(certVal) || certVal == ""){
				$("#"+divID+"NumDisplay").show();
			}else{
				$("#"+divID+"NumDisplay").html(certVal);
			}
			
			$("#"+divID+"NumInput").css("display","none");

		});
	}else{
		$.each($("#"+tableID+" tr"),function(){
			if($(this).attr("id") != "aPlusCert"){
				var divID = $(this).attr("id");
				
				var certVal = $("#"+divID+"NumInput").val();
				if(isNaN(certVal) || certVal == ""){
					$("#"+divID+"NumDisplay").show();
				}else{
					$("#"+divID+"NumDisplay").html(certVal);
					$("#"+divID+"NumDisplay").show();
				}
	
				$("#"+divID+"NumInput").css("display","none");
	
				var dateVal = $("#"+divID+"DateInput").val();
				$("#"+divID+"DateDisplay").show().html(dateVal);
				$("#"+divID+"DateInput").css("display","none");
			}else{
				if($("#APlus").attr("checked") ==  true){
					$("#aPlusCertNumDisplay").html("");
					$("#aPlusCertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));
				}else{
					$("#aPlusCertNumDisplay").html("");
				}
				$("#aPlusCertNumDisplay").show();
				$("#APlus").hide();
			}
		});
	}

	$("#"+tableID+"Header div[class='editSection']").each(function(){
		$(this).show();
	});

	$("#"+tableID+"Header div[class='cancelSaveSection']").each(function(){
		$(this).hide();
	});
}


</script>

<style type="text/css">

.required{
	padding: 0;
	color: #000;
}
.error{
	padding: 0;
	line-height: 12px;
	
}
*{
	margin: 0;
	padding: 0;
	border: none;
	text-decoration: none;
	outline: none;
}
#wr{
	margin: 20px auto;
	width: 740px;
	text-align: left;
}
#acc dt, #acc dd{
	/*width: 100%;*/
}
.sectionHeading{
	cursor: pointer;
	display: block;
	padding: 3px 10px;
	background: #E1E1E1;
	color: #000;
	border: solid 1px #666;
	height: 20px;
	font-weight: bold;
	font-size: 14px;
	text-align:left;
}

#acc dt.act{
	background: #E1E1E!;
}

#acc dd{
	display: none;
	padding: 3px 10px;
}

.label{
	text-align: left;
	font-weight:bold;

}

td.field input.invalid, td.field select.invalid, tr.errorRow td.field input, tr.errorRow td.field select {
    background-color: #FFFFD5;
    border: 2px solid red;
    color: red;
    margin: 0;
}
tr td.field div.formError {
    color: #FF0000;
    display: none;
}
tr.errorRow td.field div.formError {
    display: block;
    font-weight: normal;
}
.invalid, label.invalid {
    color: red;
}
.invalid a {
    color: #336699;
    font-size: 12px;
    text-decoration: underline;
}
#section{
	border-right: 1px solid #000;
	border-left: 1px solid #000;
	border-bottom: 1px solid #000;
}
#reqStar{
	color: rgb(255, 0, 0); 
	font-size: 12px; 
	font-family: Verdana; 
	margin-left: 2px;
}

.round {

    border-radius: 3px; 
    -webkit-border-radius: 3px; 
    -moz-border-radius: 3px; 
    -moz-border-radius: 10px 10px 10px 10px;
    -moz-box-shadow: 2px 2px 2px #888888;
    background: url("/widgets/css/images/section_background.gif") repeat-y scroll left top transparent;
    border: 2px solid #DDDDDD;
    color: #FFF;
} 

#subDivider{
width: 710px; 
height: 25px; 
background-color: #E1E1E1; 
border: 1px solid #000;
font-weight: bold;
padding-top: 5px;
}

table.tablesorter thead tr th, table.tablesorter tfoot tr th {
    background-color: #4790D4;
    color: #FFF;
    border: 1px solid #000;
    font-size: 8pt;
    padding: 4px;
}
table.tablesorter{
	border: 1px solid #000;
	width: 485px;
}
table.tablesorter tbody td{
border: 1px solid #000;

}

table th{
	text-align: center;
}

div#loader{
display: none;
width:100px;
height: 100px;
position: fixed;
top: 50%;
left: 50%;
background:url(/widgets/images/loader.gif) no-repeat center #fff;
text-align:center;
padding:10px;
font:normal 16px Tahoma, Geneva, sans-serif;
border:1px solid #666;
margin-left: -50px;
margin-top: -50px;
z-index:2;
overflow: auto;
}

#socialNetworks{
	margin-right: auto; 
	margin-left: auto; 
	text-align: center;
	margin-top: 5px;
}

#socialNetworks span{
	margin: 0 13px 0 13px;
}

.telephony3Column{
width: 100%
}
table.telephony3Column td{
	width: 33%;
}

.toolsResources{
	width: 100%;
}
.toolsResources table{
	
}
.subSectionCol{
	width: 150px;
}

.cancelUpdateButton, .updateButton{
margin-top: 0px;
padding: 0px;
}

a.title{
   background: url("/widgets/css/images/bullet_toggle_plus.png") no-repeat scroll -4px -9px transparent;
    cursor: pointer;
    display: block;
    float: left;
    height: 20px;
    width: 20px;
    margin-right: 5px;
   }
dt.act a.title{
	 background: url("/widgets/css/images/bullet_toggle_minus.png") no-repeat scroll -4px -9px transparent;
    cursor: pointer;
    display: block;
   /* padding-left: 23px; */
    float: left;
    height: 20px;
    width: 20px;
    margin-right: 5px;
}

a.bgCheck{
	display: block;
    height: 35px;
    overflow: hidden;
    width: 100px;
     margin-left:auto;
    margin-right:auto;
}

a.bgCheck:hover img{
	margin-top: -33px;
}

a.uploadW9{
	display: block;
    height: 24px;
    overflow: hidden;
    width: 85px;
    margin-left:auto;
    margin-right:auto;
}

a.uploadW9:hover img{
	margin-top: -20px;
}

</style>

<div id="loader">
	Loading...
</div>
<div id="dasboardContainer" class="wide_content">

<div id="wr">
</div>
<?php require ("../footer.php"); ?>