<?php $page = admin; ?>
<?php $option = techs; ?>
<?php $selected = copyPrefTechs; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php 
	checkAdminLogin2();

// ================================================================================
// This code is used to copy Preferrred Techs from one client over to another. GB |
// ================================================================================

//ini_set("display_errors","stdout");

require_once("../library/caspioAPI.php");
require ("../headerStartSession.php");

echo "<div id='copyTechs' align='center'><br /><br /><b>Copy Preferred Techs</b></div>";
echo "<div id='copyTechs' align='center'><br /><br />
<b>Copying techs may take several minutes. Please do not close (or navigate away) from this window until the copy has completed.<br />
The system will display the number of techs copied once it's done.</b><br />
<br /></div>";

$companyList = caspioSelectAdv("Client_Preferred_Techs", "DISTINCT CompanyID", "", "CompanyID ASC", false, "`", "|");		

$fromCompanyHtml = "<option value=\"None\" selected=\"selected\">From Company</option>";
$toCompanyHtml = "<option value=\"None\" selected=\"selected\">To Company</option>";
foreach ($companyList as $company) {
	$company = explode("|", $company);
	$id = trim($company[0], "`");
	
	$fromCompanyHtml .= "<option value=\"$id\">$id</option>";
	$toCompanyHtml .= "<option value=\"$id\">$id</option>";
}

$toList = caspioSelectAdv("TR_Client_List", "DISTINCT Company_ID", "", "Company_ID ASC", false, "`", "|");		
$toCompanyHtml = "<option value=\"None\" selected=\"selected\">To Company</option>";
foreach ($toList as $toCompany) {
	$toCompany = explode("|", $toCompany);
	$toID = trim($toCompany[0], "`");
	
	$fromCompanyHtml .= "<option value=\"$toID\">$toID</option>";
	$toCompanyHtml .= "<option value=\"$toID\">$toID</option>";
}


if (isset($_POST["submit"])) {

	set_time_limit(600);
	
	$fromCompany = $_POST["FromCompany"];
	$toCompany = $_POST["ToCompany"];

	"<p>Copying, please wait...</p>";
	
	// Get the techs that are already there for the destination so we don't create duplicates.
	$currentTechs = caspioSelectAdv("Client_Preferred_Techs_GB", "Tech_ID", "CompanyID = '$toCompany'", "Tech_ID ASC", FALSE, "'", "|");
//	print_r($currentTechs);
	
	$results = caspioSelectAdv("Client_Preferred_Techs", "Tech_ID, PreferLevel", "CompanyID = '$fromCompany'", "Tech_ID ASC", FALSE, "'", "|");
	echo "<p>Attempted to copy " . sizeof($results) . " preferred techs.</p>";
	flush();
	
	$dupes = 0;
	$copied = 0;
	
	foreach ($results as $result) {
		$parts = explode("|", $result);
		$techID = trim($parts[0], "`");		
		$preferLevel = trim($parts[1], "`");	
		
		echo "<p>$techID</p>";
		$isFound = in_array($techID, $currentTechs, true);
		echo "<p>Is Fouund: $isFound</p>";
		echo "<p>2:" . in_array($techID, $currentTechs, false) . "</p>";
		if (!in_array($techID, $currentTechs, false)) {
//			caspioInsert("Client_Preferred_Techs_GB", "Tech_ID, PreferLevel, CompanyID", "'$techID', '$preferLevel', '$toCompany'");
			echo "Tech_ID, PreferLevel, CompanyID = '$techID', '$preferLevel', '$toCompany'";
			$copied++;
		}else{
			$dupes++;
		}
	}

	// $results = caspioSelectAdv("Client_Preferred_Techs", "Tech_ID, PreferLevel", "CompanyID = '$toCompany'", "Tech_ID ASC", FALSE, "'", "|");
	
	//echo "<p>" . sizeof($results) . " preferred techs copied.</p>";
	echo "<p>Copy is Complete!</p>";
	echo "<p>$copied techs copied.</p>";
	echo "<p>$dupes duplicate techs found in $toCompany and not copied!</p>";
}

?>

<div id="projectSearch">
<form id="copyPrefTechs" name="copyPrefTechs" action="<?=$_SERVER['../PHP_SELF']?>" method="post">
  <div align="center">
    <select id="FromCompany" name="FromCompany">
      <?=$fromCompanyHtml?>
    </select>
    
    <select id="ToCompany" name="ToCompany">
      <?=$toCompanyHtml?>
    </select>
    
    <input id="submit" name="submit" type="submit" value="Copy" class="caspioButton" />
  </div>
</form>
</div>

<?php

// Commented out the following to keep it from running by accident.  Uncomment to use this code.

/*

$results = caspioSelectAdv("Client_Preferred_Techs", "Tech_ID, PreferLevel", "CompanyID = 'CTS'", "Tech_ID ASC", FALSE, "'", "|");

echo "<p>" . sizeof($results) . "</p>";

	foreach ($results as $result) {
		$parts = explode("|", $result);
		
		$techID = trim($parts[0], "`");		
		$preferLevel = trim($parts[1], "`");		

		caspioInsert("Client_Preferred_Techs", "Tech_ID, PreferLevel, CompanyID", "'$techID', '$preferLevel', 'RFT'");
		}
		

$results = caspioSelectAdv("Client_Preferred_Techs", "Tech_ID, PreferLevel", "CompanyID = 'RFT'", "Tech_ID ASC", FALSE, "'", "|");

echo "<p>" . sizeof($results) . "</p>";

*/

?>
