<!-- Add Content Here -->
<script type="text/javascript" src="/widgets/js/jquery.validate.js"></script>
<script type="text/javascript" src="/widgets/js/jquery.autotab.js"></script>
<link rel="stylesheet" href="/widgets/css/main.css" type="text/css" />
<link rel="stylesheet" href="/templates/www/main.css" type="text/css" />
<link rel="stylesheet" href="/widgets/css/details.css" type="text/css" />
<link rel="stylesheet" href="/widgets/css/blueprint/screen.css" type="text/css" media="screen, projection" />
<link rel="stylesheet" href="/widgets/css/blueprint/print.css" type="text/css" media="print" /> 
<script type="text/javascript">

$(document).ready(function(){
	$("body").css("width", "850px").css("min-width", "850px");
});

</script>
<script type="text/javascript">
 $(document).ready(function(){
 	
	jQuery.validator.addMethod("numberCheck", function( value, element ) {
		if(value.substring(0,1) != "X" && isNaN(value) == true){
			return false;
		}else{
			return true;
		}
			
	},"This field must contain numbers only");
	
	$("#w9Info").validate({
		invalidHandler: function(e, validator) {
			var errors = validator.numberOfInvalids();
		
			if (errors) {
				var message = errors == 1
					? 'You missed 1 field. It has been highlighted below'
					: 'You missed ' + errors + ' fields.  They have been highlighted below';
				$("div.error span").html(message);
				$("div.error").show();
			} else {
				$("div.error").hide();
			}
		},
		onkeyup: false,
		submitHandler: function(form) {
			$("div.error").hide();
			showLoader();
			ajaxRequest(form);
		},
		rules: {
			SSN1:{
				numberCheck: true
			},
			SSN2:{
				numberCheck: true
			},
			SSN3:{
				numberCheck: true
			},
			EIN1:{
				numberCheck: true
			},
			EIN2:{
				numberCheck: true
			}
		},
		errorPlacement: function(error,element){
             error.appendTo(element.parent().parent().next().find("div.status"));
		},
		errorClass: "invalid",
		debug:true
	});

  function ajaxRequest(form){
	  	var submitData = $(form).serialize();
	  	
			$.ajax({
				type: "POST",
				url: "/techs/ajax/submitW9Info.php",
		        dataType    : 'json',
		        cache       : false,
				data: submitData,
				success: function (data) {
					
					var onClosed = "";
					if(data.success == "success"){
						var messageHtml = "<h2>Your W9 Info has been updated successfully.</h2>";
						messageHtml += "<b>Thank you!</b>";
						$("#w9Content").html(messageHtml);
						setTimeout(function(){
							parent.$.fancybox.close(); 
						},1000);
						parent.location.reload();
					}else{
						var messageHtml = "<h2 style='color: #FF0000;'>W9 Error</h2>";
						messageHtml += "<p>There was a problem updating your W9 info. Please close this window and try again.</p> ";
						$("#w9Content").html(messageHtml);
						setTimeout(function(){
							parent.$.fancybox.close(); 
						},1000);
					}
					hideLoader();
				}
			});
  }
 
  $(".resize").vjustify();
  $("div.buttonSubmit").hoverClass("buttonSubmitHover");
  
  if ($.browser.safari) {
    $("body").addClass("safari");
  }
  
  //$("input.primaryPhone").mask("(999) 999-9999");
 // $("input.zipcode").mask("99999");
  


  // toggle optional billing address
  var subTableDiv = $("div.subTableDiv");
  var toggleCheck = $("input.toggleCheck");
  toggleCheck.is(":checked")
  	? subTableDiv.hide()
	: subTableDiv.show();
  $("input.toggleCheck").click(function() {
      if (this.checked == true) {
        subTableDiv.slideUp("medium");
        $("form").valid();
      } else {
        subTableDiv.slideDown("medium");
      }
  });

	/*
  $("input[name=TIN]").click(function(){
		if($("input[name=TIN]:checked").attr("id") == "SSNRadio"){
			$("#SSNInput").show();
			$("#EINInput").hide();
			$("#SSN1, #SSN2, #SSN3").addClass("required");
			$("#EIN1, #EIN2").removeClass("required");
		}else if($("input[name=TIN]:checked").attr("id") == "EINRadio"){
			$("#EINInput").show();
			$("#EIN1, #EIN2").addClass("required");
			$("#SSNInput").hide();
			$("#SSN1, #SSN2, #SSN3").removeClass("required");
		}
	});
	*/
  $("#SSN1, #EIN1").keyup(function(e){
		$this = $(e.target);
		if($this.attr("id") == "SSN1" && $("input[name=FTC]:checked").val() != "C Corp"){
			$("#SSNRadio").attr("checked","checked");
			$("#SSN1, #SSN2, #SSN3").addClass("required");
			$("#EIN1, #EIN2").removeClass("required");
			$("#EINRadio").removeClass("required");
			$("#EIN1, #EIN2").val("");
		}else{
			$("#EINRadio").attr("checked","checked");
			$("#EIN1, #EIN2").addClass("required");
			$("#SSN1, #SSN2, #SSN3").removeClass("required");
			$("#SSNRadio").removeClass("required");
			$("#SSN1, #SSN2, #SSN3").val(""); 
		}
	});

  $("input[name=FTC]").click(function(){
		if($("input[name=FTC]:checked").attr("id") == "FTCOther"){
			$("#FTCOtherDisplay").show();
			$("#FTCOtherInput").addClass("required");
		}else{
			$("#FTCOtherDisplay").hide();
			$("#FTCOtherInput").removeClass("required");
		}

		if($("input[name=FTC]:checked").val() == "C Corp"){
			$("#EIN1, #EIN2").addClass("required");
			$("#SSN1, #SSN2, #SSN3").removeClass("required");
			$("#EINRadio").attr("checked","checked");
			$("#SSN1, #SSN2, #SSN3").val("");
		}else{
			$("#SSN1, #SSN2, #SSN3").addClass("required");
			$("#EIN1, #EIN2").removeClass("required");
			$("#SSNRadio").attr("checked","checked");
			$("#EIN1, #EIN2").val("");
		}

		if($("input[name=FTC]:checked").val() == "LLC"){
			$("#LLCTax").addClass("required");
		}else{
			$("#LLCTax").removeClass("required");
		}
	});
	 var loaderVisible = false;
	  function showLoader(){
		  if(!loaderVisible){
			  $("div#loader").fadeIn("fast");
			  loaderVisible = true;
		  }
	  }
	  
	  function hideLoader(){
		  if(loaderVisible){
			  var loader = $("div#loader");
			  loader.stop();
			  loader.fadeOut("fast");
			  loaderVisible = false;
		  }
	  }

	  var techID = '<?=$_SESSION['TechID'];?>';
	  
		$.ajax({
			type: "POST",
			url: '/techs/ajax/getW9Info.php',
	        dataType    : 'json',
	        cache       : false,
			data: {id:techID},
			success: function (data) {
				for(index in data.w9Info){
					if(index == "FederalTaxClassification"){
						$('.FTC').each(function(){
							if($(this).val() == data.w9Info[index]){
								$(this).attr("checked",true);
								if(data.w9Info[index] == "Other"){
									$("#FTCOther").click();
									$("#FTCOtherInput").val(data.w9Info['FTCOther']);
								}
							}
						});
					}else if(index == "ExemptPayee"){
						if(data.w9Info[index] == "0"){
							$("#ExemptPayee").attr("checked",false);
						}else{
							$("#ExemptPayee").attr("checked",true);
						}
					}else if(index == "MaskedSSN" && data.w9Info[index] != "" && data.w9Info[index] != null){
						var ssn = data.w9Info[index].toString();
						$("#SSN1").val(ssn.substring(0,3));
						$("#SSN2").val(ssn.substring(3,5));
						$("#SSN3").val(ssn.substring(5,9));
						$("#SSNRadio").click();
					}else if(index == "MaskedEIN" && data.w9Info[index] != "" && data.w9Info[index] != null){
						console.log(data.w9Info[index]);
						var ein = data.w9Info[index].toString();
						$("#EIN1").val(ein.substring(0,2));
						$("#EIN2").val(ein.substring(3,9));
						$("#EINRadio").click();
					}else{
						$("#"+index).val(data.w9Info[index]);
					}
				}
			 },
		    error:function(data, status, xhr){
			 }
		});

});

$.fn.vjustify = function() {
    var maxHeight=0;
    $(".resize").css("height","auto");
    this.each(function(){
        if (this.offsetHeight > maxHeight) {
          maxHeight = this.offsetHeight;
        }
    });
    this.each(function(){
        $(this).height(maxHeight);
        if (this.offsetHeight > maxHeight) {
            $(this).height((maxHeight-(this.offsetHeight-maxHeight)));
        }
    });
};

$.fn.hoverClass = function(classname) {
	return this.hover(function() {
		$(this).addClass(classname);
	}, function() {
		$(this).removeClass(classname);
	});
};

</script>
