//-------------------------------------------------------------
//-------------------------------------------------------------
wosDetails.Instances = null;
//-------------------------------------------------------------
wosDetails.CreateObject = function(config) {
    var obj = null;
    if (wosDetails.Instances != null) {
        obj = wosDetails.Instances[config.id];
    }
    if (obj == null) {
        if (wosDetails.Instances == null) {
            wosDetails.Instances = new Object();
        }
        obj = new wosDetails(config);
        wosDetails.Instances[config.id] = obj;
    }
    obj.init();

    return obj;
}
//-------------------------------------------------------------
function wosDetails(config) {
    var Me = this;
    this.id = config.id;   
    this.detailsWidget=config.detailsWidget;
    this.params = config.params;
    this.roll= new FSPopupRoll();
    this.rollMsg= new FSPopupRoll();
    this._redirect = false;
    //------------------------------------------------------------
    this.LoadingAjax = function(ContentId){
        jQuery(ContentId).html("<center><img src='/widgets/images/wait.gif' border='0' /></center>");
    } 
    //------------------------------------------------------------
    this.ClosePopup=function(){      
        Me.roll.hide();
    }
    //------------------------------------------------------------
    this.showPopup = function(width,height,title,el){
        var content = "<div id='comtantPopupID'><div style='text-align:center;height:150px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
        var opt = {
            width       : width,
            height      : height,
            position    : 'middle',
            title       : title,
            body        : content
        };
        Me.roll.showNotAjax(el, el, opt);
    }
    //------------------------------------------------------------
    this.viewTechBtnHidden_onclick=function(){
        Me.showPopup(600, 230,'Select a Technician for assignment:',this );
        Me.params.siteNumber = jQuery("#edit_SiteNumber").val();
        Me.params.Project_ID = jQuery("#edit_Project_ID").val();
        $.ajax({
            url : "/widgets/dashboard/details/list-tech-in-site",
            data : Me.params,
            cache : false,
            type : 'POST',
            dataType : 'html',
            context : this,
            error : function (xhr, status, error) {
                $('body').css('cursor', '');
                if ( xhr.responseText === "authentication required" ) {
                    document.location = "/clients/logIn.php";
                } else {
                    FSWidget.fillContainer("Sorry, but error occured. Not results found.", this);
                }
                this.tabOpenXhrRequest = null;
            },
            success : function(data, status, xhr) {
                jQuery("#comtantPopupID").html(data);
                jQuery("#closePopup").unbind("click",Me.ClosePopup)
                .bind("click",Me.ClosePopup);
                jQuery(".openTechProfile").unbind("click",Me.openTechProfile)
                .bind("click",Me.openTechProfile);
                jQuery("#cmdAssign").unbind("click",Me.cmdAssign)
                .bind("click",Me.cmdAssign);
            }
        });
    }
    //-----------------------------------------------------------
    this.showMessages = function(data,el) {
        var html = '';
        if (Me._redirect && data.success) { //always refresh page on success
                window.location.reload();
        }
        if (data.success) {
            if(typeof(data.msg) == 'undefined') '<div class="success"><ul><li>Success!</li></ul></div>';
            html += data.msg
            if (data.success == 1)
                html+='<div class="tCenter"><input type="button" class="link_button_popup" id="closePopup" value="Ok" /></div>';
            else if (data.success == 2){
                html+= data.button;
            }
        }else {
            html+='<div class="errors"><ul>';
            for (i=0;i<data.errors.length;i++) {
                if(typeof data.errors[i] === 'object')
                    html+='<li>'+data.errors[i].message+'</li>';
                else
                    html+='<li>'+data.errors[i]+'</li>';
            }
            html+='</ul></div>';
            html+='<div class="tCenter"><input type="button" class="link_button_popup" id="closePopup" value="Ok" /></div>';
        }
        jQuery("#closePopup").unbind("click",Me.ClosePopup)
                .bind("click",Me.ClosePopup);
        Me.roll.autohide(false);
        var opt = {
            width       : 400,
            height      : '',
            position    : 'middle',
            title       : '',
            body        : html
        };
        if (data.success) {
            opt.width = 300;
        }
        return Me.roll.showNotAjax(el, el, opt);
    }
    //------------------------------------------------------------
    this.showMsgErorr = function(err){
        jQuery("#divError").html(err);
        jQuery("#divError").show();
        setTimeout(function(){
            jQuery("#divError").hide();
        },2000);
    }
    //------------------------------------------------------------
    this.cmdAssign = function(){
        var techid = "";
        jQuery(".AssignTech").each(function(){
            if(jQuery(this).is(":checked"))
            {
                techid = jQuery(this).val();
                return false;
            }    
        })
        if(techid == ""){
            Me.showMsgErorr("Please select a Technician for assignment");
            return false;
        }
        Me.params.techid = techid;
        $.ajax({
            url : "/widgets/dashboard/details/do-tech-in-site",
            data : Me.params,
            cache : false,
            type : 'POST',
            context : this,
            error : function (xhr, status, error) {
                $('body').css('cursor', '');
                if ( xhr.responseText === "authentication required" ) {
                    document.location = "/clients/logIn.php";
                } else {
                    FSWidget.fillContainer("Sorry, but error occured. Not results found.", this);
                }
                this.tabOpenXhrRequest = null;
            },
            success : function(data) {
                if(data.success == 1){
                    Me.saveApprove();
                }
                else if(data.success == 2){
                    data.msg = "<div class='displayNone colorRed errormsg' id='divError'></div><div>Note: This Work Order is already assigned.</div>";
                    data.button = '<div class="tCenter"><input type="button" class="link_button_popup" id="closePopup" value="Cancel" />&nbsp;&nbsp;&nbsp;<input type="button" class="link_button_popup" id="saveApprove" value="Re-Assign" /></div>';
                    Me.showMessages(data,null);
                    jQuery("#saveApprove").unbind("click",Me.saveApprove)
                    .bind("click",Me.saveApprove);
                    jQuery("#closePopup").unbind("click",Me.ClosePopup)
                    .bind("click",Me.ClosePopup);
                }
            }
        });
    }
    //------------------------------------------------------------
    this.saveApprove = function(){

        $.ajax({
            url : "/widgets/dashboard/do/assign?"
                +"techs["+Me.params.win+"]=" +Me.params.techid
                +"&assignClientDeniedTech=true&"
                +"&agreeds["+Me.params.win+"]="+jQuery("#edit_PayMax").val()
                +"&qtydevices["+Me.params.win+"]="+jQuery("#edit_Qty_Devices").val()
                +"&amountsrep["+Me.params.win+"]="+jQuery("#edit_Amount_Per").val(),
            data : Me.params,
            cache : false,
            type : 'POST',
            context : this,
            error : function (xhr, status, error) {
                $('body').css('cursor', '');
                if ( xhr.responseText === "authentication required" ) {
                    document.location = "/clients/logIn.php";
                } else {
                    FSWidget.fillContainer("Sorry, but error occured. Not results found.", this);
                }
                this.tabOpenXhrRequest = null;
            },
            success : function(data) {
                if(data.error == 0){
                    data.success = 1;
                    Me._redirect = true;
                    Me.showMessages(data,null);
                }
                else{
                    Me.showMsgErorr(data.msg);
                }
            }
        });
    }
    //------------------------------------------------------------
    this.openTechProfile = function(){
        var TechID = jQuery(this).attr("TechID");
        openLightBoxPage('/clients/wosViewTechProfile.php?simple=0&TechID='+TechID+'&v='+Me.params.company+'&popup=1',1000,600);
    }
    //------------------------------------------------------------  
    this.saveTechToSiteImg_onclick=function(){       
        var sitenumber=jQuery("#edit_SiteNumber").val();
        if(typeof(Me.roll)=="undefined")
        {
            Me.roll= new FSPopupRoll();
        }
        
        Me.roll.autohide(false);
        var html = '<div>Site Number required to save</div><div class="tCenter"><input type="button" class="link_button_popup" id="closePopup" value="Ok" /></div>';
        var opt = {
            width       : 300,
            height      : '',
            position    : 'middle',
            title       : '',
            body        : html
        };
        if (sitenumber == '') {
            Me.roll.showNotAjax(null, null, opt);
            jQuery("#closePopup").unbind("click",Me.ClosePopup)
            .bind("click",Me.ClosePopup);
            return false;
        }
        $.ajax({
            url         : '/widgets/dashboard/sitelist/save-tech-to-site',
            dataType    : 'json',
            data        : {
                SiteNumber: $("#edit_SiteNumber").val(),
                SiteName: $("#edit_SiteName").val(),
                Site_Contact_Name: $("#edit_Site_Contact_Name").val(),
                Address: $("#edit_Address").val(),
                City: $("#edit_City").val(),
                State: $("#edit_State").val(),
                Zipcode: $("#edit_Zipcode").val(),
                Country: $("#edit_Country").val(),
                SitePhone: $("#edit_SitePhone").val(),
                SiteFax: $("#edit_SiteFax").val(),
                SiteEmail: $("#edit_SiteEmail").val(),
                Project_ID: $("#edit_Project_ID").val(),
                Tech_ID:$("#edit_Tech_ID").val()
            },
            cache       : false,
            type        : 'POST',
            context     : this,
            success     : function (data, status, xhr) {
                if (data) {
                    
                    var html = '<div>Site saved to project site list</div><div class="tCenter"><input type="button" class="link_button_popup" id="closePopup" value="Ok" /></div>';
                    var opt = {
                        width       : 300,
                        height      : '',
                        position    : 'middle',
                        title       : '',
                        body        : html
                    };
                    Me.roll.showNotAjax(null, null, opt);
                    jQuery("#closePopup").unbind("click",Me.ClosePopup)
                    .bind("click",Me.ClosePopup);    
                    jQuery(".viewTechBtnHidden").show();
                    jQuery(".viewTechBtnHiddenView").hide();
                }
            }
        });	
       
    }  
    this.cmdsaveSite = function(){
        var SaveTechToSite = jQuery(this).attr("SaveTechToSite");
        var techid = jQuery(this).attr("techid");
        var fname = jQuery(this).attr("fname");
        var lname = jQuery(this).attr("lname");
        if(SaveTechToSite == '1'){
            data.success = 2;
            data.msg = "Would you like to save FS Tech ID# "+techid+" "+fname+" "+lname+" along with this Site?";
            data.button = '<div class="tCenter"><input type="button" class="link_button_sm" id="cmdSaveTechtoSite" value="Save Tech to Site" />&nbsp;&nbsp;&nbsp;<input type="button" class="link_button_sm" id="cmdSaveSiteOnly" value="Save Site Only" /></div>';
            Me.showMessages(data,this);
            jQuery("#cmdSaveTechtoSite").unbind("click",Me.cmdSaveTechtoSite)
            .bind("click",Me.cmdSaveTechtoSite);
            jQuery("#cmdSaveSiteOnly").unbind("click",Me.cmdSaveSiteOnly)
            .bind("click",Me.cmdSaveSiteOnly);
        }else
            detailsWidget.saveSite();
    }
    this.cmdSaveSiteOnly = function(){
        Me.roll.hide();
        detailsWidget.saveSite();
    }
    this.cmdSaveTechtoSite = function(){
        Me.roll.hide();
        detailsWidget.saveSite(Me.saveTechToSiteImg_onclick);
    }
    //------------------------------------------------------------
    this.init = function() {        
        jQuery(document).ready(function(){
            jQuery(".saveTechToSiteImg")
            .unbind("click",Me.saveTechToSiteImg_onclick)
            .bind("click",Me.saveTechToSiteImg_onclick);
            jQuery(".viewTechBtnHidden")
            .unbind("click",Me.viewTechBtnHidden_onclick)
            .bind("click",Me.viewTechBtnHidden_onclick); 
            jQuery(".cmdsaveSite")
            .unbind("click",Me.cmdsaveSite)
            .bind("click",Me.cmdsaveSite);
        });
       
    }

}