<?
$page = 'admin';
$option = 'client';
$selected = 'fstagsList';

require ("../header.php");
require ("../navBar.php");

checkAdminLogin2();
?>
<script src="/widgets/js/FSWidget.js"></script>
<script src="/widgets/js/FSPopupAbstract.js"></script>
<script src="/widgets/js/FSPopupRoll.js"></script>
<script type="text/javascript" src="/widgets/js/ajaxfileupload.js"></script>
<script src="/widgets/js/base64.js"></script>
<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<!--931-->
<?php script_nocache("/admin/js/fstags.js"); ?>
<!--end 931-->
<script>
    function cbLinkHover(obj, css) {
        try {
            obj.style.cssText = css;
        } catch (e) {
        }
    }
    function cbButtonHover(obj, css) {
        try {
            obj.style.cssText = css;
        } catch (e) {
        }
    }

    function setadminval(el)
    {
        if($(el).find('option:selected').val()=="")
        {
            $('#adminusername').val("");
        }
        else
        {
            $('#adminusername').val($(el).find('option:selected').text());
        }
    }
</script>

<?
$search = new Core_Api_AdminClass;
//get companies
$fieldsList = array('Company_ID', 'CompanyName');
$companies = $search->getCompanies($_SESSION["UserName"], $_SESSION["UserPassword"]
        , $fieldsList, 'CompanyName', 'CompanyName');
$companies = $companies->data;

$db = Zend_Registry::get('DB');
$select = $db->select();
$select->from(Core_Database::TABLE_CLIENTS, array("Company_ID", "CompanyName"))
        ->distinct()
        ->order(array("CompanyName"));
$companyList = $db->fetchPairs($select);
$companyHtml = "";
foreach ($companyList as $id => $name)
{
    if (!empty($_GET["v"]))
    {
        $companyHtml .= "<option value=\"$id\" " . ($_GET["v"] == $id ? "selected=\"selected\"" : "") . " textname=\"$name - $id\">$name - $id</option>";
    } else
    {
        $companyHtml .= "<option value=\"$id\" textname=\"$name - $id\">$name - $id</option>";
    }
}

$AdminUser = new Core_Api_AdminUser();
$AdminUserList = $AdminUser->getAdminUserList();
if (!empty($AdminUserList))
{
    foreach ($AdminUserList as $AdminUser)
    {
        $adminoption .= "<option value='" . $AdminUser['AdminID'] . "'>" . $AdminUser['UserName'] . "</option>";
    }
}
?>
<style>
    .col1
    {
        text-align: left;
        vertical-align: middle;
        width: auto;
        white-space: nowrap;
        padding: 2px 5px 2px 5px;
        color: #032D5F;
        font-size: 12px;
        font-family: Verdana;
        font-style: normal;
        font-weight: normal;
    }
    .col2
    {
        text-align: left;
        vertical-align: top;
        width: auto;
        padding: 5px 10px 5px 10px;
        white-space: normal;
    }
    .trstyle
    {
        background-color: #FFFFFF;
        padding: 7px;
    }
    .cbxstyle
    {
        color: #000000;
        font-size: 12px;
        font-family: Verdana;
        font-style: normal;
        font-weight: normal;
    }

</style>
<div id="divsearch" align="center">
    <h1>Browse FS-Tags List</h1>
    <a href="fstags.php?type=Client">Create New Tag</a>
    <table cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <tr>
                <td>
                    <table cellspacing="0" style="border-color: #032D5F; border-width: 2px; border-style: solid; border-collapse: collapse;width: 100%;">
                        <tbody>
                            <tr class="trstyle">
                                <td class="col1">
                                    System Visibility
                                </td>
                                <td class="col2">
                                    <!--874-->
                                    <select class="cbxstyle" name="cbxSystemVisibility" id="cbxSystemVisibility" style="width:250px;">
                                        <option value="">Any</option>
                                        <option value="1">Selected Client(s) Only</option>
                                        <option value="2">All Client Users</option>
                                        <option value="3">GPM Only</option>
                                    </select> 
                                    <!--end 874-->
                                </td>
                            </tr>
                            <tr class="trstyle">
                                <td class="col1">
                                    Select Client
                                </td>
                                <td class="col2">
                                    <select class="cbxstyle" id="cbxclient" name="cbxclient">
                                        <option value="">Select Client</option>
<?= $companyHtml ?>
                                    </select> 
                                </td>
                            </tr>
                            <!--13912-->                            
                            <tr class="trstyle">
                                <td class="col1">Client (non-GPM) Edits</td>
                                <td class="col2">
                                    <select class="cbxstyle" name="cbxCanNonGPMEdit" id="cbxCanNonGPMEdit" style="width:250px;">
                                        <option value="">Any</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select> 
                                </td>
                            </tr>
                            <tr class="trstyle">
                                <td class="col1">GPM Edits</td>
                                <td class="col2">
                                    <select class="cbxstyle" name="cbxCanGPMEdit" id="cbxCanGPMEdit" style="width:250px;">
                                        <option value="">Any</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select> 
                                </td>
                            </tr>
                            <tr class="trstyle">
                                <td class="col1">Tech Visibility</td>
                                <td class="col2">
                                    <select class="cbxstyle" name="cbxtechHide" id="cbxtechHide" style="width:250px;">
                                        <option value="">Any</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                        <option value="2">Tagged Only</option>
                                    </select> 
                                </td>
                            </tr>
                            <tr class="trstyle">
                                <td class="col1">Tech Edits</td>
                                <td class="col2">
                                    <select class="cbxstyle" name="cbxCanTechEdit" id="cbxCanTechEdit" style="width:250px;">
                                        <option value="">Any</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select> 
                                </td>
                            </tr>
                            <!--End 13912-->  
                            <tr class="trstyle">
                                <td class="col1">
                                    Tag Title
                                </td>
                                <td class="col2">
                                    <input type="text" id="tagtitle" name="tagtitle" style="width:250px;">
                                </td>
                            </tr>   
                             <!-- 13931 -->
                            <tr class="trstyle">
                                <td class="col1">
                                    Multi-Level
                                </td>
                                <td class="col2">                                   
                                    <select class="cbxmultipletaglevels" name="cbxmultipletaglevels" id="cbxmultipletaglevels" style="width:250px;">
                                        <option value="">Any</option>
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                    </select> 
                                </td>
                            </tr>
                            <!-- End 13931 -->
                            <tr class="trstyle">
                                <td class="col1">
                                    Added by User
                                </td>
                                <td class="col2">
                                    <select class="cbxstyle" id="cbxadminuser" name="cbxadminuser" style="width:250px;" 
                                            onchange="setadminval(this);">
                                        <option value="">Select Client</option>
<?= $adminoption ?>
                                    </select>
                                    <input type="hidden" id="adminusername" name="adminusername" value="">
                                </td>
                            </tr>
                            <tr class="trstyle">
                                <td class="col1">
                                    Active
                                </td>
                                <td class="col2">
                                    <select class="cbxstyle" id ="cbxactive" name="cbxactive" style="width:250px;">
                                        <option value="">Any</option>
                                        <option value="1" selected>Yes</option>
                                        <option value="0">No</option>
                                    </select>
                                </td>
                            </tr>
                            <tr style="background-color: #FFFFFF;">
                                <td style="padding: 5px 0px 5px 0px; text-align: center; vertical-align: middle; border-color: #032D5F; border-width: 2px; border-style: solid; background-color: #5091CB;" colspan="2">
                                    <input type="submit" id="cmdSearchSubmit" value="Submit" onmouseout="cbButtonHover(this,'color: #ffffff; font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold; text-align: center; vertical-align: middle; border-color: #5091CB; border-style: solid; border-width: 1px; background-color: #032D5F;  width: auto; height: auto; margin: 0 3px;');" onmouseover="cbButtonHover(this,'color: #FFFFFF; font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold; text-align: center; vertical-align: middle; border-style:solid; border-width: 1px; border-color: #032D5F; background-color: #083194;  width: auto; height: auto; margin: 0 3px;');" style="color: rgb(255, 255, 255); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold; text-align: center; vertical-align: middle; border-color: rgb(80, 145, 203); border-style: solid; border-width: 1px; background-color: rgb(3, 45, 95); width: auto; height: auto; margin: 0pt 3px;" id="searchID">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div id="divsearchresult" style="display: none;"></div>
<div id="divedittag" style="display: none;"></div>

<script>
    jQuery(window).ready(function(){
       
        
        function changePage(){
            var page = jQuery(this).attr("page");
            getHtml(page,'Title','asc');
        }
        function sortable(){
            var sortfield = jQuery(this).attr("sort");
            var sortdir = jQuery(this).attr("sortdir");
            getHtml(1,sortfield,sortdir);
        }
        function getHtml(page,sortfield,sortdir){
            var cbxSystemVisibility = $("#cbxSystemVisibility").val();
            var cbxclient = $("#cbxclient").val();
            var adminusername = $("#adminusername").val();
            var tagtitle = $("#tagtitle").val();
            var activity = $("#cbxactive").val();
            //931
            var multipletaglevels = $("#cbxmultipletaglevels").val();
            //end 931
            //13874
            var techHide = $("#cbxtechHide").val();        
            //13912
            var canNonGPMEdit = $("#cbxCanNonGPMEdit").val(); 
            var canGPMEdit = $("#cbxCanGPMEdit").val(); 
            var canTechEdit = $("#cbxCanTechEdit").val(); 
            var dataForm = "";
            dataForm += "SystemVisibility="+cbxSystemVisibility;
            dataForm += "&client="+cbxclient;
            dataForm += "&adminusername="+adminusername; 
            dataForm += "&tagtitle="+tagtitle; 
            dataForm += "&activity="+activity; 
            dataForm += "&techHide="+techHide; 
            dataForm += "&canNonGPMEdit="+canNonGPMEdit; 
            dataForm += "&canGPMEdit="+canGPMEdit; 
            dataForm += "&canTechEdit="+canTechEdit;  //End 13912
            dataForm += "&page="+page; 
            dataForm += "&sortfield="+sortfield; 
            dataForm += "&sortdir="+sortdir; 
            //931
            dataForm += "&multipletaglevels="+multipletaglevels;
            //931
            $.ajax({        
                url         : '/widgets/admindashboard/client/searchtag/',
                dataType    : 'html',
                data        : dataForm,
                async       : false,
                type        : 'post',
                success     : function (data, status, xhr) {
                    //               $("#divsearch").css("display","none");
                    $("#divsearchresult").html(data);
                    $("#divsearchresult").css("display","");
                    $("#divsearch").css("display","none");
                    //874
                    $("#divedittag").css("display","none");
                    //end 874
                    jQuery(".paginatorLinkActiv").unbind("click").bind("click",changePage);
                    jQuery(".sortable").unbind("click").bind("click",sortable);
                }
            });
        }
        jQuery('#cmdSearchSubmit').click(function(){
            getHtml(1, 'Title', 'asc'); 
        });
    });
</script>
<?php require ("../footer.php"); ?>