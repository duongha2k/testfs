<?php
require_once("../headerStartSession.php");
require_once("../library/smtpMail.php");
require_once("../library/caspioAPI.php");
require_once("../library/contactSettings.php");
checkAdminLogin2();
/*
!!!!!!!!!!!!!!!!! IMPORTANT !!!!!!!!!!!!!!!!!!!!!!!
Inputs using either GET method or curl GET/POST should be encoded using:
	for PHP: urlencode(string)
	for Javascript: encodeURIComponent(string)

Inputs using POST should be decoded using stripslashes
*/

// check for well formed request and ignores if not

$useTemplate = isset($_POST["useTemplate"]);

if (!$useTemplate &&
	(!isset($_POST["vFromName"]) ||
	!isset($_POST["vFromEmail"]) ||
	!isset($_POST["vSubject"]) ||
	!isset($_POST["vMessage"])))
	die();

$result = "Success";

/*if (!isset($_SERVER['HTTP_REFERER']) || parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) != "fieldsolutions.com")
	$result = "Failure: Called from unknown host - " . $_SERVER['HTTP_REFERER'];*/

$calledFrom = (empty($_SERVER['HTTP_REFERER']) ? "Unknown" : $_SERVER['HTTP_REFERER']);

@$caller = urldecode($_POST["caller"]);

$projectID = $_POST["projectID"];
$importDate = $_POST["importDate"];

@$parts = explode("/", $importDate);

@$month = $parts[0];
@$date = $parts[1];
@$year = $parts[2];

$dateQuery = ($month != "" && $date != "" && $year != "" ? "AND DATEPART(mm, DateEntered) = '$month' AND DATEPART(dd, DateEntered) = '$date' AND DATEPART(yyyy, DateEntered) = '$year'" : "");

$footer = "Client";

if ($result == "Success") {
	// send mail, if no problems so far

	if (!$useTemplate) {
		$vFromName = urldecode($_POST["vFromName"]);
		$vFromEmail = urldecode($_POST["vFromEmail"]);
		$vSubject = stripslashes(urldecode($_POST["vSubject"]));

		$vMessage = stripslashes(urldecode($_POST["vMessage"]));

		$html_message = $vMessage;
		$html_message = str_replace(chr(13) . chr(10), '<br/>', $html_message);

		// add message footer
		switch ($footer) {
			case "FLS":
				$html_message.="<br/><br/><br/><br/>Note: If you have not yet been through online training to be activated as a Tech with the FLS program<br/>and/or do not have a Badge and IDs yet, please do not respond to this email, you are not eligible.<br/>To get yourself eligible, make sure that you have gone through training beginning, at the link below and<br/>that you have your Badge and IDs.<br/><br/><a href='http://www.flsupport.com/3.html'>FLS online training</a>";

				$html_message.="<br/><br/>Have you visited the new Tech Community website? Click <a href='http://www.mytechnicianspace.com/'>here</a> for fun and information on the tech community";

				$html_message .= "<br/><br/>You are receiving this email as a registered technician on www.fieldsolutions.com.<br>Click <a href='http://www.fieldsolutions.com/unsubscribe/'>here</a> to stop receiving future e-mails from us.";
				break;
			case "Client":
				$html_message.="<br/><br/>Have you visited the new Tech Community website? Click <a href='http://www.mytechnicianspace.com/'>here</a> for fun and information on the tech community";

				$html_message .= "<br/><br/>You are receiving this email as a registered technician on www.fieldsolutions.com.<br>Click <a href='http://www.fieldsolutions.com/unsubscribe/'>here</a> to stop receiving future e-mails from us.";
			default:
				break;
		}

//		$eList = caspioSelect("Work_Orders", "(SELECT ISNULL(PrimaryEmail, '') FROM TR_Master_List WHERE TR_Master_List.TechID = Work_Orders.Tech_ID)", "ISNULL(Work_Orders.Tech_ID, '') <> '' AND Project_ID = '$projectID' $dateQuery", "");

		$eList = caspioSelect("Work_Orders", "DISTINCT (SELECT ISNULL(PrimaryEmail, '') FROM TR_Master_List WHERE TR_Master_List.TechID = Work_Orders.Tech_ID)", "Deactivated = '0' AND ISNULL(Work_Orders.Tech_ID, '') <> '' AND Project_ID = '$projectID' $dateQuery", "");

		$eList = implode(",", str_replace("'", "", $eList));

        // vince debugging
        $contact = new ContactSettings();
        $doNotEmail = $contact->getEmailList();
        $emailList = $contact->filterEmail($emailList);

		// send copy to self?
		$eList .= (isset($_POST["copySelf"]) ? ",$vFromEmail" : "");

//		$eList = "codem01@gmail.com";

		if ($eList != "")
			smtpMailLogReceived($vFromName, $vFromEmail, $eList, $vSubject, $vMessage, $html_message, $caller);
	}
	else {
		// Get Project info
		$project = caspioSelectAdv("TR_Client_Projects", "From_Email, Resource_Coordinator, Project_Name", "Project_ID = '$projectID'", "", false, "`", "|");
			
		$projectFields = explode("|", str_replace("`", "", $project[0]));
		
		$vFromEmail = $projectFields[0];	// From Email address
		$vFromName = $projectFields[1];		// From Name
		$projectName = $projectFields[2];   // Project Name
	
		$vSubject = "Project Assigned: $projectName"; // Subject Line
		
		$projectWOs = caspioSelect("Work_Orders", "TB_UNID, Tech_ID, (SELECT ISNULL(PrimaryEmail, '') FROM TR_Master_List WHERE TR_Master_List.TechID = Work_Orders.Tech_ID), Company_Name", "Deactivated = '0' AND ISNULL(Work_Orders.Tech_ID, '') <> '' AND Project_ID = '$projectID' $dateQuery", "");
		
		foreach ($projectWOs as $wo) {
			$fields = getFields($wo);
			$tbUNID = $fields[0];
			$techID = $fields[1];
			$eList = $fields[2];
			$company = $fields[3];
			
//			$eList = "codem01@gmail.com";
			
			if ($eList != "") {
				$vMessage = "Dear Field Solutions Technician,/r/r Congratulations you have been awarded work from $company. /r/r Please click on the link below and review the work order information (You may be required to sign-in)./r/r You need to indicate that you have reviewed the work order info as well as any project related documentation./r/r By clicking on this box you are confirming to $company that you are ready to do the work listed in the work order./r/r You will also be receiving another auto generated email 48 hours before your install.  You will be required to revisit the website and click on another box confirming your intentions to be at the install./r/r If you do not do this, $company will activate the backup for the install./r/r https://www.fieldsolutions.com/techs/wosWorkOrderReviewed.php?tbUNID=$tbUNID&techID=$techID/r/r/r/r Thank you for your support,/r/r Field Solutions Management";
	
				$html_message = "Dear Field Solutions Technician,<br><p>Congratulations you have been awarded work from $company. </p><p>Please click on the link below and review the work order information (You may be required to sign-in).</p><p>You need to indicate that you have reviewed the work order info as well as any project related documentation.</p><p>By clicking on this box you are confirming to $company that you are ready to do the work listed in the work order.</p><p>You will also be receiving another auto generated email 48 hours before your install.  You will be required to revisit the website and click on another box confirming your intentions to be at the install.</p><p>If you do not do this, $company will activate the backup for the install.</p><p><a href='https://www.fieldsolutions.com/techs/wosWorkOrderReviewed.php?tbUNID=$tbUNID&techID=$techID'>I have reviewed all Work Order Details and documentation.</a></p><br><p>Thank you for your support,</p><p>Field Solutions Management	</p>";	

				smtpMailLogReceived($vFromName, $vFromEmail, $eList, $vSubject, $vMessage, $html_message, $caller);
			}
		}

	}
		
	echo "<br><br><b>Your email has been sent!</b>";
	echo "<p><input type=\"button\" value=\"Close Window\" onclick=\"window.close();\" /></p>";
}
?>	

