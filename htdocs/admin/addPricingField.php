<?php $page = 'admin'; ?>
<?php $option = 'pricing'; ?>
<?php $selected = 'addPricingField'; ?>
<?php require ("../header.php"); ?>
<?php require ("../navBar.php"); ?>
<?php 
//	ini_set("display_errors", 1);
	require_once("../library/pricing.php");
	$tableID = (!isset($_GET["id"]) || $_GET["id"]  == 0 ? TABLEID_MAIN : TABLEID_FLS);
	$mainTable = "work_orders";
	$FLSTable = "FLS_Work_Orders";
	$tableName = ($tableID == TABLEID_MAIN ? $mainTable : $FLSTable);
	
	if (isset($_POST["submit"])) {
		// process form
		$fieldToAdd = $_POST["fieldToAdd"];
		$displayName = $_POST["displayName"];
		Core_Pricing::createField($displayName, $fieldToAdd);
	}
	
//	$tableFields = caspioGetTableDesign($tableName, FALSE);
	$tableFields = Core_Database::getFieldList($mainTable);

	$db = Core_Database::getInstance();
	$select = $db->select();
	$assignedFieldsList = Core_Pricing::getFields();
	// find already assigned fields
	$assigendFields = array();
	foreach ($assignedFieldsList as $field) {
		$assignedFields[$field[2]] = 1;
	}
	
	// find unassigned fields
	$unassignedFields = array();
	$fieldOptions = "";
	foreach ($tableFields as $field) {
		$fieldName = $field;
		if (array_key_exists($fieldName, $assignedFields))
			// ignore already assigned fields
			continue;
		$unassignedFields[] = $fieldName;		
	}
	sort($unassignedFields);
	
	foreach ($unassignedFields as $fieldName) {
		$fieldOptions .= "<option value=\"$fieldName\">$fieldName</option>";
	}
	
?>
<!-- Add Content Here -->

<br /><br />

<div align="center">
	<form name="addPricingField" id="addPricingField" method="post" action="<?=$_SERVER['PHP_SELF']?>?id=<?=$tableID?>">
    	<table>
        	<tr>
            	<td>Table:</td>
                <td>
                    <select id="tableID" name="tableID">
                        <option value="0" <?php echo ($tableID == TABLEID_MAIN ? "selected=\"selected\"" : "");?>>Main</option>
                        <!--<option value="1" <?php echo ($tableID == TABLEID_FLS ? "selected=\"selected\"" : "");?>>FLS</option>-->
                    </select>
				</td>
			</tr>
            <tr>
            	<td>
                	Field To Add:
                </td>
            	<td>
                    <select name="fieldToAdd">
                        <?=$fieldOptions?>
                    </select>
                </td>
            </tr>
            <tr>
            	<td>
                	Descriptive Name:
                </td>
            	<td>
                    <input name="displayName" type="text" size="30" />
                </td>
            </tr>
            <tr>
            	<td colspan="2" style="text-align: center"><input name="submit" type="submit" value="Submit" /></td>
            </tr>
	</form>
</div>

<script type="text/javascript">
	function changeTable() {
		var id = document.getElementById("tableID").selectedIndex;
		document.location.replace("<?=$_SERVER['PHP_SELF']?>?id=" + id);
	}
	
	$(document).ready(function () {
		$("#tableID").change(changeTable);
	});
</script>
	

<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->
		
