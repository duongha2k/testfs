<?php
$page     = 'admin';
$option   = 'wos';
$selected = 'wosSearchPay';
require ("../header.php");
require_once("../library/caspioAPI.php");
checkAdminLogin();
?>

<link rel="stylesheet" href="../library/jquery/calendar/jquery-calendar.css" type="text/css" />
<link rel="stylesheet" href="/widgets/css/main.css" type="text/css" />
<script type="text/javascript" src="../library/jquery/calendar/jquery-calendar.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        // Init calendar
        $.map(['find_DateEntered_from','find_DateEntered_to','find_StartDate_to','find_StartDate_from','find_date_invoiced_from',
               'find_date_invoiced_to','find_date_paid_from','find_date_paid_to','find_date_approved_from','find_date_approved_to'], function (id) {
               
            $('#'+id).calendar({dateFormat:'MDY/'});
            $('#'+id).focus( function() {
                this.blur();
                $('#calendar_div').css('top', $(this).offset().top + 'px');
                $('#calendar_div').css('left', $(this).offset().left + $(this).width() + 7 + 'px');
                popUpCal.showFor(this);
            });
            // init buttons
            $('#' + id + '_img').click(function () {
                var id = $(this).attr('id').replace(/_img$/, '');
                var e  = document.getElementById(id);
                if ( e ) e.focus();
            });
        });
        
        //send find form
        $('#FindWorkOrderForm').submit(function(event) {
            event.preventDefault();
            var idx, l, wd;
            var query = '';
            var formData = $(this).serializeArray();
            if ( opener ) {
                /* if opener isn't wos.php page open it */
                l = opener.location.toString().replace(/\?.*$/, '');
                if ( -1 === l.search(/wos\.php$/i) ) {
                    for ( idx in formData ) {
                        if ( formData[idx].value ) {
                            query += '&' + formData[idx].name + '=' + encodeURIComponent(formData[idx].value).replace(/\~/g, '%7E').replace(/\!/g, '%21').replace(/\'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29');
                        }
                    }
                    
                    l  = opener.location.protocol + '//' + opener.location.host;
                    l += '/admin/wos.php?tab=all&search=1';
                    l += query + "&version=sp";
                    opener.location = l;

                } else {        //  opener == wos.php
                    wd = opener.wd;
                    opener._search   = true;

                    wd.resetFilters();
                    
                    opener.$('#dashboardVersion').val('sp');

                    opener.window._company = $('#find_Company_ID').val();
                    for ( idx in formData ) {
                        if ( !formData[idx].value ) {
                                wd.filters().set('_'+formData[idx].name, null);
                        } else {
                                wd.filters().set('_'+formData[idx].name, formData[idx].value);
                        }
                    }
                    
                    wd.show({tab:'all', params:wd.getParams()});
                }
	            window.close();
            } else {
                for ( idx in formData ) {
                    if ( formData[idx].value ) {
                        query += '&' + formData[idx].name + '=' + encodeURIComponent(formData[idx].value).replace(/\~/g, '%7E').replace(/\!/g, '%21').replace(/\'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29');
                    }
                }

                l  = location.protocol + '//' + location.host;
                l += '/admin/wos.php?tab=all&search=1';
                l += query;
                //alert(l);
                location = l;
            }
            return false;
        });
		$("#find_win").focus();
    });
</script>


<div class="popup_form_holder">
    <h1>Search/Pay Work Orders</h1>
<form action="/" method="post" id="FindWorkOrderForm" name="FindWorkOrderForm">
<input type="hidden" name="full_search" value="1" />
<?
//818
$AdminClass = new Core_Api_AdminClass;
$fieldsList = array('Company_ID','CompanyName');
$companies = $AdminClass->getCompanies($_SESSION['UserName'],$_SESSION['UserPassword'],$fieldsList,'CompanyName','CompanyName');

$fieldsList     = array('Project_ID','Project_Name','Active');
$projectsState  = 'all';
$projects = $AdminClass->getProjects($_SESSION['UserName'],$_SESSION['UserPassword'],$fieldsList,null,null,$projectsState,'Project_Name');

$call_types = $AdminClass->getCallTypes();

$fieldsList = array('Category_ID','Category');
$categories = $AdminClass->getWOCategories($_SESSION['UserName'],$_SESSION['UserPassword'],$fieldsList);
//end 818
?>
<table class="form_table" cellspacing="0">
    <col class="col1" />
    <col class="col2" />
    <tr>
        <td class="label_field">Unique ID</td>
        <td><input type="text" value="" id="find_win" name="win" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">Company</td>
        <td>
            <select id="find_Company_ID" name="company_id">
                <option value="0">Select Company</option>
                <?
                //818
                foreach($companies->data as $company)
                {
                    ?>
                    <option value="<?=$company->Company_ID?>"><?=$company->CompanyName?></option>
                    <?
                }
                //end 818
                ?>
            </select>
        </td>
    </tr>
    <?
    $WoTagClass = new Core_Api_WoTagClass();
    $CommonClass = new Core_Api_CommonClass();

    $ClientStatusList = $WoTagClass->getClientStatusList();
    $ClientRelationShipList = $WoTagClass->getClientRelationShipList();
    $SegmentList = $WoTagClass->getSegmentList();
    $OriginalSellerList = $WoTagClass->getOriginalSellerList();
    $ExecutiveList = $WoTagClass->getExecutiveList();
    $ClientServiceDirectors = $CommonClass->getClientServiceDirectors();
    $AccountManagers = $CommonClass->getAccountManagers();
    $EntityTypeList = $WoTagClass->getEntityTypeList();
    $ServiceTypeList = $WoTagClass->getServiceTypeList();
    $SystemStatusList = $WoTagClass->getSystemStatusList();
    
    ?>
    <tr>
        <td class="label_field">Client Status</td>
        <td>
            <select id="find_Client_Status" name="Client_Status">
                <option value="">Select Client Status</option>
                <?
                if (!empty($ClientStatusList))
                {
                    foreach ($ClientStatusList as $ClientStatus)
                    {
                        ?>
                        <option value="<?= $ClientStatus['wo_tag_item_id'] ?>"><?= $ClientStatus['wo_tag_item_name'] ?></option>
                        <?
                    }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td class="label_field">Client Relationship</td>
        <td>
            <select id="find_Client_Relationship" name="Client_Relationship">
                <option value="">Select Client Relationship</option>
                <?
                if (!empty($ClientRelationShipList))
                {
                    foreach ($ClientRelationShipList as $ClientRelationShip)
                    {
                        ?>
                        <option value="<?= $ClientRelationShip['wo_tag_item_id'] ?>"><?= $ClientRelationShip['wo_tag_item_name'] ?></option>
                        <?
                    }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td class="label_field">Segment</td>
        <td>
            <select id="find_Segment" name="Segment">
                <option value="">Select Segment</option>
                <?
                if (!empty($SegmentList))
                {
                    foreach ($SegmentList as $Segment)
                    {
                        ?>
                        <option value="<?= $Segment['wo_tag_item_id'] ?>"><?= $Segment['wo_tag_item_name'] ?></option>
                        <?
                    }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td class="label_field">Original Seller</td>
        <td>
            <select id="find_Original_Seller" name="Original_Seller">
                <option value="">Select Orig. Seller</option>
                <?
                if (!empty($OriginalSellerList))
                {
                    foreach ($OriginalSellerList as $OriginalSeller)
                    {
                        ?>
                        <option value="<?= $OriginalSeller['wo_tag_item_id'] ?>"><?= $OriginalSeller['wo_tag_item_name'] ?></option>
                        <?
                    }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td class="label_field">Executive</td>
        <td>
            <select id="find_Executive" name="Executive">
                <option value="">Select Executive</option>
                <?
                if (!empty($ExecutiveList))
                {
                    foreach ($ExecutiveList as $Executive)
                    {
                        ?>
                        <option value="<?= $Executive['wo_tag_item_id'] ?>"><?= $Executive['wo_tag_item_name'] ?></option>
                        <?
                    }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td class="label_field">Client Service Dr.</td>
        <td>
            <select id="find_CSD" name="CSD">
                <option value="">Select CSD</option>
                <?
                if (!empty($ClientServiceDirectors))
                {
                    foreach ($ClientServiceDirectors as $CSD)
                    {
                        ?>
                        <option value="<?= $CSD['id'] ?>"><?= $CSD['name'] ?></option>
                        <?
                    }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td class="label_field">Account Manager</td>
        <td>
            <select id="find_AM" name="AM">
                <option value="">Select AM</option>
                <?
                if (!empty($AccountManagers))
                {
                    foreach ($AccountManagers as $AM)
                    {
                        ?>
                        <option value="<?= $AM['id'] ?>"><?= $AM['name'] ?></option>
                        <?
                    }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td class="label_field">Legal Entity</td>
        <td>
            <select id="find_Entity" name="Entity">
                <option value="">Select Entity</option>
                <?
                if (!empty($EntityTypeList))
                {
                    foreach ($EntityTypeList as $EntityType)
                    {
                        ?>
                        <option value="<?= $EntityType['wo_tag_item_id'] ?>"><?= $EntityType['wo_tag_item_name'] ?></option>
                        <?
                    }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td class="label_field">Service Type</td>
        <td>
            <select id="find_Service_Type" name="Service_Type">
                <option value="">Select Service Type</option>
                <?
                if (!empty($ServiceTypeList))
                {
                    foreach ($ServiceTypeList as $ServiceType)
                    {
                        ?>
                        <option value="<?= $ServiceType['wo_tag_item_id'] ?>"><?= $ServiceType['wo_tag_item_name'] ?></option>
                        <?
                    }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td class="label_field">System Status</td>
        <td>
            <select id="find_System_Status" name="System_Status">
                <option value="">Select System Status</option>
                <?
                if (!empty($SystemStatusList))
                {
                    foreach ($SystemStatusList as $SystemStatus)
                    {
                        ?>
                        <option value="<?= $SystemStatus['wo_tag_item_id'] ?>"><?= $SystemStatus['wo_tag_item_name'] ?></option>
                        <?
                    }
                }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td class="label_field">Created by (Username)</td>
        <td><input type="text" value="" id="find_Username" name="username" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">FS Tech ID</td>
        <td><input type="text" value="" id="tech" name="tech" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">FLS ID</td>
        <td><input type="text" value="" id="find_FLS_ID" name="fls_id" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">Work Order ID</td>
        <td><input type="text" value="" id="find_WO_ID" name="client_wo" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">PO</td>
        <td><input type="text" value="" id="find_PO" name="po" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">Visible to Techs?</td>
        <td>
            <input type="radio" value="1" name="show_to_tech" id="find_ShowTechs_1">Yes
            <input type="radio" value="0" name="show_to_tech" id="find_ShowTechs_2">No
            <input type="radio" value="" name="show_to_tech" checked id="find_ShowTechs_3">Any
        </td>
    </tr>
    <tr>
        <td class="label_field">Date Created (&gt;=)</td>
        <td>
            <input type="text" style="cursor: pointer;" class="input_text input_date" name="date_entered_from" id="find_DateEntered_from" value="" />&#160;
            <img id="find_DateEntered_from_img" src="/widgets/images/calendar.gif" style="position:relative; top:4px; cursor: pointer;" />
        </td>
    </tr>
    <tr>
        <td class="label_field">Date Created (&lt;=)</td>
        <td>
            <input type="text" style="cursor: pointer;" class="input_text input_date" name="date_entered_to" id="find_DateEntered_to" value="" />&#160;
            <img id="find_DateEntered_to_img" src="/widgets/images/calendar.gif" style="position:relative; top:4px; cursor: pointer;" />
        </td>
    </tr>
    <tr>
        <td class="label_field">Work Start Date (&gt;=)</td>
        <td>
            <input type="text" style="cursor: pointer;" class="input_text input_date" name="date_start" id="find_StartDate_from" value="" />&#160;
            <img id="find_StartDate_from_img" src="/widgets/images/calendar.gif" style="position:relative; top:4px; cursor: pointer;" />
        </td>
    </tr>
    <tr>
        <td class="label_field">Work Start Date (&lt;=)</td>
        <td>
            <input type="text" style="cursor: pointer;" class="input_text input_date" name="date_end" id="find_StartDate_to" value="" />&#160;
            <img id="find_StartDate_to_img" src="/widgets/images/calendar.gif" style="position:relative; top:4px; cursor: pointer;" />
        </td>
    </tr>

    <tr><td colspan="2"><hr></td></tr>
    <!--818-->
    <tr>
        <td class="label_field">Call Type</td>
        <td>
            <select id="find_Type_ID" name="call_type">
                <option value="0">Select Call Type</option>
                <?
                foreach($call_types->data as $key => $val)
                {
                    ?>
                    <option value="<?=$key?>"><?=$val?></option>
                    <?
                }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td class="label_field">Project Name</td>
        <td>
            <select id="find_Project_ID" name="projects">
                <option value="0">Select Project</option>
                <?
                foreach($projects->data as $project)
                {
                    ?>
                    <option value="<?=$project->Project_ID?>"><?=$project->Project_Name?></option>
                    <?
                }
                ?>
            </select>
        </td>
    </tr>
    <tr>
        <td class="label_field">WO Category</td>
        <td>
            <select id="find_WO_Category" name="wo_category">
                <option value="0">Select Category</option>
                <?
                foreach($categories->data as $cat)
                {
                    ?>
                    <option value="<?=$cat->Category?>"><?=$cat->Category?></option>
                    <?
                }
                ?>
            </select>
        </td>
    </tr>
    <!--end 818-->
    <tr>
        <tr>
        <td class="label_field">Route</td>
        <td><input type="text" value="" id="find_Route" name="route" maxlength="9"></td>
    </tr>
        <td class="label_field">Headline</td>
        <td><input type="text" value="" id="find_Headline" name="headline" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">Site Name</td>
        <td><input type="text" value="" id="find_SiteName" name="site_name" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">City</td>
        <td><input type="text" value="" id="find_City" name="city" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">State</td>
        <td><input type="text" value="" id="find_State" name="state" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">Zip</td>
        <td><input type="text" value="" id="find_Zipcode" name="zip" maxlength="255"></td>
    </tr>

    <tr><td colspan="2"><hr></td></tr>

    <tr>
        <td class="label_field">Tech FName</td>
        <td><input type="text" value="" id="find_Tech_FName" name="tech_fname" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">Tech LName</td>
        <td><input type="text" value="" id="find_Tech_LName" name="tech_lname" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">Tech Email (contains)</td>
        <td><input type="text" value="" id="find_TechEmail" name="tech_email_contain" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">Work Market Tech</td>
        <td>
            <select id="wm_tech_select" name="wm_tech_select">
                <option value="">Any</option>
                <option value="1">Yes</option>
                <option value="0">No</option>
            </select>
        </td>
    </tr>
    <tr>
        <td class="label_field">Work Market Tech ID#</td>
        <td><input type="text" value="" id="wm_tech_id" name="wm_tech_id" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">Work Market Ticket#</td>
        <td><input type="text" value="" id="wm_assignment_id" name="wm_assignment_id" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">Reminder Call?</td>
        <td>
            <input type="radio" value="1" name="tech_called" id="tech_called_1">Yes
            <input type="radio" value="0" name="tech_called" id="tech_called_2">No
            <input type="radio" value="" name="tech_called" checked id="tech_called_3">Any
        </td>
    </tr>
    <tr>
        <td class="label_field">Complete?</td>
        <td>
            <input type="radio" value="1" name="tech_complete" id="find_tech_complete_1">Yes
            <input type="radio" value="0" name="tech_complete" id="find_tech_complete_2">No
            <input type="radio" value="" name="tech_complete" checked id="find_tech_complete_3">Any
        </td>
    </tr>

    <tr>
        <td class="label_field">Pay ISO</td>
        <td>
            <select name="Pay_ISO" name="filter_Pay_ISO" >
                <option value="" >Any</option>
                <option value="1" >Yes</option>
                <option value="0" >No</option>
            </select>
        </td>
    </tr>
    <?php
        $common = new Core_Api_CommonClass();
        $isoOptions = $common->getISOsArray();
	$isos = $isoOptions->data;
    ?>
    <tr>
        <td class="label_field">ISO Company Name</td>
        <td>
            <select name="ISO_Company_Name" name="filter_ISO_Company_Name" >
                <option value="" >Any</option>
                <?php
                echo "<option value=\"\" >None</option>";
                foreach ($isos as $id => $name) {
                    echo "<option value=\"$id\">$name</option>";
                }
                        ?>
            </select>
        </td>
    </tr>
    <tr>
        <td class="label_field">ISO ID</td>
        <td>
            <input type="text" name="ISO_ID" id="filter_ISO_ID" />
        </td>
    </tr>
    
    <tr><td colspan="2"><hr></td></tr>

    <tr>
        <td class="label_field">TB Pay?</td>
        <td>
            <input type="radio" value="1" name="tb_pay" id="find_tb_pay_1">Yes
            <input type="radio" value="0" name="tb_pay" id="find_tb_pay_2">No
            <input type="radio" value="" name="tb_pay" checked id="find_tb_pay_3">Any
        </td>
    </tr>
    <tr>
        <td class="label_field">Pay Type</td>
        <td>
            <select id="Amount_Per" name="Amount_Per" style="width:100px;">
                <option value="">Any</option>
                <option value="Hour">Hour</option>
                <option value="Site">Site</option>
                <option value="Device">Device</option>
                <option value="Visit">Visit</option>
            </select>
        </td>
    </tr>
    <tr>
        <td class="label_field Pay Max">Pay Max (&gt;=)</td>
        <td><input type="text" value="" id="find_PayMax_from" name="pay_max_from" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">and Pay Max (&lt;=)</td>
        <td><input type="text" value="" id="find_PayMax_to" name="pay_max_to" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">Net Pay Amount (&gt;=)</td>
        <td><input type="text" value="" id="find_Net_Pay_Amount_from" name="net_pay_amount_from" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">and Net Pay Amount (&lt;=)</td>
        <td><input type="text" value="" id="find_Net_Pay_Amount_to" name="net_pay_amount_to" maxlength="255"></td>
    </tr>
    <tr>
        <td class="label_field">Abort $ Amount</td>
        <td>
            <input type="checkbox" value="1" name="AbortFee" id="AbortFee">
        </td>
    </tr>
    <tr>
        <td class="label_field">Extra Pay Amount?</td>
        <td>
            <input type="checkbox" value="1" name="extra_pay" id="extra_pay">
        </td>
    </tr>
    
    <tr>
        <td class="label_field">Approved?</td>
        <td>
            <input type="radio" value="1" name="approved" id="find_approved_1">Yes
            <input type="radio" value="0" name="approved" id="find_approved_2">No
            <input type="radio" value="" name="approved" checked id="find_approved_3">Any
        </td>
    </tr>
    <tr>
        <td class="label_field">Date Approved (&gt;=)</td>
        <td>
            <input type="text" style="cursor: pointer;" class="input_text input_date" name="date_approved_from" id="find_date_approved_from" value="" />&#160;
            <img id="find_date_approved_from_img" src="/widgets/images/calendar.gif" style="position:relative; top:4px; cursor: pointer;" />
        </td>
    </tr>
    <tr>
        <td class="label_field">and Date Approved (&lt;=)</td>
        <td>
            <input type="text" style="cursor: pointer;" class="input_text input_date" name="date_approved_to" id="find_date_approved_to" value="" />&#160;
            <img id="find_date_approved_to_img" src="/widgets/images/calendar.gif" style="position:relative; top:4px; cursor: pointer;" />
        </td>
    </tr>
    <tr>
        <td class="label_field">Paid?</td>
        <td>
            <input type="radio" value="1" name="tech_paid" id="find_tech_paid_1">Yes
            <input type="radio" value="0" name="tech_paid" id="find_tech_paid_2">No
            <input type="radio" value="" name="tech_paid" checked id="find_tech_paid_3">Any
        </td>
    </tr>
    <tr>
        <td class="label_field">Date Paid (&gt;=)</td>
        <td>
            <input type="text" style="cursor: pointer;" class="input_text input_date" name="date_paid_from" id="find_date_paid_from" value="" />&#160;
            <img id="find_date_paid_from_img" src="/widgets/images/calendar.gif" style="position:relative; top:4px; cursor: pointer;" />
        </td>
    </tr>
    <tr>
        <td class="label_field">and Date Paid (&lt;=)</td>
        <td>
            <input type="text" style="cursor: pointer;" class="input_text input_date" name="date_paid_to" id="find_date_paid_to" value="" />&#160;
            <img id="find_date_paid_to_img" src="/widgets/images/calendar.gif" style="position:relative; top:4px; cursor: pointer;" />
        </td>
    </tr>
    <tr>
        <td class="label_field">Invoiced</td>
        <td>
            <input type="radio" value="1" name="invoiced" id="invoiced_1">Yes
            <input type="radio" value="0" name="invoiced" id="invoiced_2">No
            <input type="radio" value="" name="invoiced" checked id="invoiced_3">Any
        </td>
    </tr>
    <tr>
        <td class="label_field">Date Invoiced (&gt;=)</td>
        <td>
            <input type="text" style="cursor: pointer;" class="input_text input_date" name="date_invoiced_from" id="find_date_invoiced_from" value="" />&#160;
            <img id="find_date_invoiced_from_img" src="/widgets/images/calendar.gif" style="position:relative; top:4px; cursor: pointer;" />
        </td>
    </tr>
    <tr>
        <td class="label_field">and Date Invoiced (&lt;=)</td>
        <td>
            <input type="text" style="cursor: pointer;" class="input_text input_date" name="date_invoiced_to" id="find_date_invoiced_to" value="" />&#160;
            <img id="find_date_invoiced_to_img" src="/widgets/images/calendar.gif" style="position:relative; top:4px; cursor: pointer;" />
        </td>
    </tr>
        
    <tr>
        <td class="label_field">Out of Scope?</td>
        <td>
            <input type="radio" value="1" name="out_of_scope" id="out_of_scope_1">Yes
            <input type="radio" value="0" name="out_of_scope" id="out_of_scope_2">No
            <input type="radio" value="" name="out_of_scope" checked id="out_of_scope_3">Any
        </td>
    </tr>
    
    <tr>
        <td class="label_field">Update_Requested</td>
        <td>
            <input type="radio" value="1" name="update_req" id="update_req_1">Yes
            <input type="radio" value="0" name="update_req" id="update_req_2">No
            <input type="radio" value="" name="update_req" checked id="update_req_3">Any
        </td>
    </tr>
    
    <tr>
        <td class="label_field">Deduct Fee from Tech</td>
        <td>
            <input type="radio" value="1" name="pcnt_deduct" id="pcnt_deduct_1">Yes
            <input type="radio" value="0" name="pcnt_deduct" id="pcnt_deduct_2">No
            <input type="radio" value="" name="pcnt_deduct" checked id="pcnt_deduct_3">Any
        </td>
    </tr>
    <tr>
        <td class="label_field">Deactivated</td>
        <td>
            <input type="radio" value="1" name="deactivated" id="find_deactivated_1">Yes
            <input type="radio" value="0" name="deactivated" id="find_deactivated_2">No
            <input type="radio" value="" name="deactivated" checked id="find_deactivated_3">Any
            
            <select id="deactivation_code" name="deactivation_code" style="color: rgb(0, 0, 0); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal;"><option value="" selected="selected">Select Deactivation Code</option><option value="Duplicate Work Order">Duplicate Work Order</option><option value="EU cxl - maximum call attempts">EU cxl - maximum call attempts</option><option value="No bidding techs (within time needed)">No bidding techs (within time needed)</option><option value="Not approved">Not approved</option><option value="Not Sourced">Not Sourced</option><option value="Reassigned with a new work order">Reassigned with a new work order</option><option value="Recruiting work order">Recruiting work order</option><option value="Rescheduled work">Rescheduled work</option><option value="Sourced elsewhere">Sourced elsewhere</option><option value="Tech no showed">Tech no showed</option><option value="Work order cancelled">Work order cancelled</option><option value="Work order declined">Work order declined</option><option value="Work order kicked back">Work order kicked back</option></select>
        </td>
    </tr>
    <tr>
        <td class="label_field">Pricing Ran</td>
        <td>
            <input type="radio" value="1" name="pricing_ran" id="pricing_ran_1">Yes
            <input type="radio" value="0" name="pricing_ran" id="pricing_ran_2">No
            <input type="radio" value="" name="pricing_ran" checked id="pricing_ran_3">Any
        </td>
    </tr>
    
    <tr><td colspan="2"><hr></td></tr>
    
    <tr>
        <td class="label_field">Preference (&gt;=)</td>
        <td>
            <input type="text" style="cursor: pointer;" class="input_text input_date" name="satrecommended_from" id="satrecommended_from" value="" />
        </td>
    </tr>
    
    <tr>
        <td class="label_field">Performance (&gt;=)</td>
        <td>
            <input type="text" style="cursor: pointer;" class="input_text input_date" name="satperformance_from" id="satperformance_from" value="" />
        </td>
    </tr>
    
    <tr class="submit_raw">
        <td colspan="2" align="center"><input type="submit" name="Go" id="Go" value="Search" class="input_button" /></td>
    </tr>
</table>
</form>
</div>
<?php require("../footer.php");?>