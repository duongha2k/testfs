<?php $page = 'admin'; ?>
<?php $option = 'client'; ?>
<?php $selected = 'clientprofile'; ?>

<?php require ("../header.php"); ?>
<script src="/widgets/js/FSWidget.js"></script>
<script src="/widgets/js/FSPopupAbstract.js"></script>
<script src="/widgets/js/FSPopupRoll.js"></script>
<script src="/widgets/js/base64.js"></script>
<?php require ("../navBar.php"); ?>
<?php
checkAdminLogin2();
/* test */
/*
$api = new Core_Api_Class;
$userapi = new Core_Api_User;
$data=Array(
    "CompanyName" => "DELL Inc.1",
    "EIN" => "11-1122333",
    "CompanyDescription" => "US a bc", 
    "ContactName" => "David Valento", 
    "BusinessAddress" => "Address Test2", 
    "BusinessCity" => "City Test", 
    "BusinessState" => "KY",
    "BusinessZip" => "Zip test", 
    "Country" => "US",
    "ContactPhone1" => "512-657-6675", 
    "ContactPhone2" => "512-657-6678", 
    "Email1" => "David_Valento@Dell.Com",
    "Email2" => "",
    "WebsiteURL" => "www.Dell.com" 
);
$dataExt = Array (
    "FSClientServiceDirectorId" => 1,
    "FSAccountManagerId" => 5,
    "File1URL" => "010X053OD9y_4f4603eb05b47.gif", 
    "File2URL" => "205_768_4f4603ec9aa0c.gif",
    "File3URL" => "300_768_4f4603ed5d991_4f46831738a33.gif"
);
//$userapi->editRepClientAccessValue(473,44,1);
//$api->updateProfileForRepClientFromAdminSide($data,$dataExt,473,"gbailey");
//$userapi->WriteAccessForRepClientFromAdmin(473,'gbailey',null);
*/  
/* test:end */

?>
<!--- Add Content Here --->

<script type="text/javascript" src="https://bridge.caspio.net/scripts/e1.js"></script>
<script>
    function cbLinkHover(obj, css) {
        try {
            obj.style.cssText = css;
        } catch (e) {
        }
    }
    function cbButtonHover(obj, css) {
        try {
            obj.style.cssText = css;
        } catch (e) {
        }
    }

</script>
<?php
$search = new Core_Api_AdminClass;
//get companies
$fieldsList = array('Company_ID', 'CompanyName');
//576
$companies = $search->getCompanies($_SESSION["UserName"], $_SESSION["UserPassword"]
        , $fieldsList, 'CompanyName', 'CompanyName');
$companies = $companies->data;//
?>

<div align="center">

    <h1>Client profile</h1>

    <table cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <tr>
                <td>
                    <table cellspacing="0" style="border-color: #032D5F; border-width: 2px; border-style: solid; border-collapse: collapse;width: 100%;">
                        <tbody>
                            <tr style="background-color: #FFFFFF; padding: 7px;">
                                <td style="text-align: left; vertical-align: top; width: auto; white-space: nowrap; padding: 2px 5px 2px 5px; color: #032D5F; font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal;">
                                    <label for="Value1_1">Company:</label>
                                </td>
                                <td style="text-align: left; vertical-align: top; width: auto; padding: 5px 10px 5px 10px; white-space: normal;">
                                    <select style="color: #000000; font-size: 12px; font-family: Verdana; font-style: normal; font-weight: normal;" name="Value1_1" id="cbxCompany">
                                        <option value="">Select Company</option>
                                        <?php foreach ($companies as $item) {
                                            ?>
                                            <option value="<?= $item->Company_ID ?>"> <?= $item->CompanyName ?> </option>
                                            <?php
                                        }
                                        ?>
                                    </select> 
                                </td>
                            </tr>
                            <tr style="background-color: #FFFFFF;">
                                <td style="padding: 5px 0px 5px 0px; text-align: center; vertical-align: middle; border-color: #032D5F; border-width: 2px; border-style: solid; background-color: #5091CB;" colspan="2">
                                    <input type="submit" id="cmdSubmit" value="Submit" onmouseout="cbButtonHover(this,'color: #ffffff; font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold; text-align: center; vertical-align: middle; border-color: #5091CB; border-style: solid; border-width: 1px; background-color: #032D5F;  width: auto; height: auto; margin: 0 3px;');" onmouseover="cbButtonHover(this,'color: #FFFFFF; font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold; text-align: center; vertical-align: middle; border-style:solid; border-width: 1px; border-color: #032D5F; background-color: #083194;  width: auto; height: auto; margin: 0 3px;');" style="color: rgb(255, 255, 255); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold; text-align: center; vertical-align: middle; border-color: rgb(80, 145, 203); border-style: solid; border-width: 1px; background-color: rgb(3, 45, 95); width: auto; height: auto; margin: 0pt 3px;" id="searchID">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <script>
        jQuery(window).ready(function(){
            jQuery('#cmdSubmit').click(function(event){
                var roll = new FSPopupRoll();
                var company_id = jQuery("#cbxCompany").children("option:selected").val();
                var el = this;
                if(company_id == "")
                {
                    alert('Please choose company.')            
                }
                else
                {
                    $.ajax({
                        type: "POST",
                        url: "/widgets/admindashboard/client/getclientcompany?company_id="+company_id,
                        data: "",
                        success:function( html ) {
                            var data = eval("("+html+")");
                            if(data.client_id == null)
                            {
                                alert('Not Recoreds.');
                            }
                            else if(data.client_id > 0)
                            {
                                window.location = "/admin/admin_client_profile.php?type=Client&client_id="+data.client_id;
                            }
//                            else
//                            {
//                                var companyName = jQuery("#cbxCompany").children("option:selected").html();
//                                var form_ = '<form  name="form1">'
//                                    +'<table border="0" cellpadding="5" cellspacing="5" width="100%">'
//                    
//                                    +' <tr>'
//                                    +' <tr>'
//                                    +'  <td width="50px" align="right" >' 
//                                    +'   Company:'
//                                    +'  </td>'
//                                    +'  <td >' 
//                                    +'    '+companyName
//                                    +'  </td>'
//                                    +' </tr>'
//                                    +' <tr>'
//                                    +'   <td width="50px"  align="right">' 
//                                    +'    Client:'    
//                                    +'   </td>'
//                                    +'   <td>' 
//                                    +'     <select id="cbxClient" >'+Base64.decode(data.options)+'</select>'
//                                    +'   </td>'
//                                    +' </tr>'
//                                    +' <tr>'
//                                    +'   <td colspan="2" align="center">'
//                                    +'     <input type="button" value="Submit" class="link_button" id="cmdSubmitClient">'
//                                    +'   </td>'
//                                    +' </tr>'
//                                    +' </table>'
//                                    +' </form>';
//                                
//                                roll.autohide(false);
//                                var opt = {
//                                    width       : 350,
//                                    height      : 120,
//                                    title       : 'Select a client',
//                                    body        : form_
//                                };
//                                roll.showNotAjax(el, event, opt);   
//                                jQuery("#cmdSubmitClient").click(function(){
//                                   var client_id = jQuery("#cbxClient").children("option:selected").val(); 
//                                   window.location = "/admin/admin_client_profile.php?type=Client&client_id="+client_id;
//                                });
//                            }
                        
                        }
                    });
                }
            });
          
        });
    </script>
    <!--- End Content --->
    <?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->

