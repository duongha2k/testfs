<?php
require_once ("headerStartSession.php");

/*
 * Added auth check with allowed page list.
  * 
 * @author Alex Che
 */

// TODO Alex Che Remove auth check logic to right place in refactor auth logic
$cfgAlowwedPages = new Zend_Config_Xml(APPLICATION_PATH.'/../../includes/configs/cfg.anonymous.pages.xml');
$allowedPages = $cfgAlowwedPages->get('pages')->toArray();
$allowedPages = isset($allowedPages['item']) ? $allowedPages['item'] : array();

$part = explode('/', trim($_SERVER['SCRIPT_NAME'], '/'));
$part = isset($part[0]) ? $part[0] : '';

if ($loggedIn == "yes" && $loggedInAs == "tech") {
    require "header_techs.php";
} else if($loggedIn == "yes" && $loggedInAs == "customers") {
    require "header_customers.php";
} else if($loggedIn == "yes" && $loggedIn){
    require "header_clients.php";
} else {
    // no auth
    if($part == 'customers')
    {
        require "header_clients.php";
    }    
    else 
     if (!in_array($part, array('admin', 'clients', 'techs', 'customers', 'unsubscribe')) || in_array(trim($_SERVER['SCRIPT_NAME'], '/'), $allowedPages) ) {
        require "header_clients.php";
    } else {
        ob_clean();
        @header('Location: /?r=' . urlencode($_SERVER['REQUEST_URI']));
        print '<script>document.location = "/";</script>';
        exit;
    }
}

