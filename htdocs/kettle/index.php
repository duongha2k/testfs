<?php
/*********************************************/
$valid_types = array('csv', 'txt', 'xls');
$max_file_size = 16 * 1048576; // 16 MB
//$upload_dir = '/mnt/services/a2720/app/uploads/kettle/';
$upload_dir = '../../../../uploads/kettle/';

//$kettle_script_path = 'opt/pentaho/data-integration/kitchen.sh /file mnt/home/iberezouski/kettle/job_xls.kjb'; // path to the Kettle script
/*********************************************/


if ( $_SERVER['REQUEST_METHOD'] == 'POST' ) {

    $name = addslashes(trim($_POST['name']));
    $hash = addslashes(trim($_POST['hash']));
    $mail = addslashes(trim($_POST['mail']));
    $clientID = addslashes(trim($_POST['clientid']));

    if (empty($name) || empty($hash) || empty($mail) || !isset($_FILES["userfile"])) {
        echo "Error: All parameters required";
    } else {

    if (isset($_FILES["userfile"])) {
        if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {
            $filename = $_FILES['userfile']['tmp_name'];
            $ext = substr($_FILES['userfile']['name'],1+strrpos($_FILES['userfile']['name'], "."));
            if (filesize($filename) > $max_file_size) {
                echo 'Error: File size > 64K.';
            } elseif (!in_array($ext, $valid_types)) {
                echo 'Error: Invalid file type.';
            } else {
                //upload file into the current dir
                if (move_uploaded_file($filename, $upload_dir.$_FILES['userfile']['name'])) {
       executeKettleScript($name, $hash, $mail, substr($_FILES['userfile']['name'],0,strrpos($_FILES['userfile']['name'], ".")), $clientID);
                } else {
                    echo 'Error: moving file failed.';
                }
            }
        } else {
            echo "Error: empty file.";
        }
    }
    }
}

function executeKettleScript($name, $hash, $mail, $fileName, $clientID) {
	if ( strtolower($clientID) == 'fls') {
    	exec('php kettleScriptFls.php "'.$fileName.'" '.$name.' '.$hash.' '.$mail.' FLS');
	} else {
    	exec('php kettleScript.php "'.$fileName.'" '.$name.' '.$hash.' '.$mail.' '.$clientID);
	}

    echo '<b>Request has been accepted. Report will be sent to '.$mail.'</b><br /><br />';

}
?>
<html>
    <head>
    </head>
    <body>
        <table border="0">
        <form action="index.php" method="post" enctype="multipart/form-data">
            <tr><td>File : </td><td><input type="file" name="userfile" value="" id="" /></td></tr>
            <tr><td>Name : </td><td><input type="text" name="name" value="" id="" /></td></tr>
            <tr><td>Password : </td><td><input type="password" name="hash" value="" id="" /></td></tr>
            <tr><td>Mail : </td><td><input type="text" name="mail" value="" id="" /></td></tr>
            <tr><td>Client ID : </td><td><input type="text" name="clientid" value="" id="" /></td></tr>

            <tr><td colspan="2"><input type="submit" value="Submit"/></td></tr>
        </form>
        </table>
    </body>
</html>
