<?php
    $params   = $_SERVER['argv'];

    $fileName = $params[1];
    $name     = $params[2];
    $hash     = $params[3];
    $mail     = $params[4];
    $clientID = $params[5];
    $owner = $params[6];

    $kettle_script_path = 'KETTLE_HOME=\'/opt/pentaho/\' /opt/pentaho/data-integration/kitchen.sh /file /opt/pentaho/kettle/job_xls.kjb'; // path to the Kettle script tst
	

    $command = ''.$kettle_script_path.' "'.$fileName.'" '.$name.' '.$hash.' '.$mail.' '.$clientID.' '.$owner.' /norep';

    function shutdown() {
        posix_kill(posix_getpid(), SIGHUP);
    }

    if ($pid = pcntl_fork())
        return;     // Parent

    @ob_end_clean(); // Discard the output buffer and close

    fclose(STDIN);  // Close all of the standard
    fclose(STDOUT); // file descriptors as we
    fclose(STDERR); // are running as a daemon.

    register_shutdown_function('shutdown');

    if (posix_setsid() < 0)
        return;

    if ($pid = pcntl_fork())
        return;     // Parent

    $log = array();
    exec($command,$log);


    $f = fopen('csvUpload/log.txt','a+');
    foreach ($log as $row) {
        fwrite($f,$row.'\n');
    }
    fclose($f);
    // Now running as a daemon. This process will even survive
    // an apachectl stop.
    return;

?>

