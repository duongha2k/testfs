<?php
	require_once("ivr/ivrheader.php");
	ini_set("display_errors",1);
	$_SESSION["loggedIn"] = "yes";
	require_once("library/caspioAPI.php");
	require_once("ivr/library/ivrLib.php");
	require_once("ivr/library/reminderCallBGLib.php");
	require_once("library/smtpMail.php");
	require_once("library/woContactLog_ivr.php");
	
	$msg = "";
	
	set_time_limit(600);
	
	function runCall($callType, $woList) {
		$groupedCallsUniqueID = array();
		$groupedCallsCompanyID = array();
		$currentTech = -1;
		$numWO = sizeof($woList);
		$processed = 0;
		$currentStartDate = -1;
		$currentStartTime = -1;
		foreach ($woList as $info) {
			echo "Pending $callType: ";
			if ($info == "") {
				echo "<div style=\"margin: 0px 0px 0px 15px\">None</div><br/>";
				continue;
			}
			$parts = explode("|", $info);
			$uniqueID = trim($parts[0], "`");
			$techID = trim($parts[1], "`");
			$primaryPhone = trim($parts[2], "`");
			$secondaryPhone = trim($parts[3], "`");
			$primPhoneType = trim($parts[4], "`");
			$secondPhoneType = trim($parts[5], "`");
			$emergencyPhone = trim($parts[6], "`");
			$companyID = trim($parts[7], "`");
			$startDate = trim($parts[8], "`");
			$startTime = trim($parts[9], "`");
			
			echo "<div style=\"margin: 0px 0px 0px 15px\">WOUNID: $uniqueID</div>";	
			echo "<div style=\"margin: 0px 0px 0px 15px\">Company: $companyID</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">TechID: $techID</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Primary Phone: $primaryPhone</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Secondary Phone: $secondaryPhone</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Primary Type Phone: $primPhoneType</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Secondary Type Phone: $secondPhoneType</div>";
			echo "<div style=\"margin: 0px 0px 0px 15px\">Emergency Phone: $emergencyPhone</div>";									
			echo "<div style=\"margin: 0px 0px 0px 15px\">Start Date: $startDate</div>";									
			echo "<div style=\"margin: 0px 0px 0px 15px\">Start Time: $startTime</div>";									
				
			if ($callType != LOG_CALLTYPE_EMERGENCY_DISPATCH_BACK_OUT_CALL && $callType != LOG_CALLTYPE_EMERGENCY_DISPATCH_NOT_ON_SITE_CALL) {
				$phone = ($callType == LOG_CALLTYPE_1_HR_CALL && $primPhoneType != "Cell" && $secondaryPhone != "" && $secondPhoneType == "Cell" ? $secondaryPhone : $primaryPhone);
			}
			else {
				$phone = $emergencyPhone;
			}
						
			$phone = filter_var($phone, FILTER_SANITIZE_NUMBER_INT);
			
			$phone = str_replace(array("-", ".", " ", "(", ")"), array("", "", "", "", ""), $phone);
			
			echo "<br/><div style=\"margin: 0px 0px 0px 15px\">Using number: $phone</div><br/>";
			@ob_flush();
			@flush();
			
			// Testing purposes
//			if ($phone != "5122728328" || $phone != "8326878350")
//				$phone = "5122728328";
//			if ($companyID != "CBD" && $companyID != "suss" && $companyID != "BW" && $companyID != "FS")
//				continue;
			
			$phone = "+1" . $phone;
			echo "<div style=\"margin: 0px 0px 0px 15px\">Dialing $phone</div>";
			
			$processed++;
			
			if ($callType != LOG_CALLTYPE_EMERGENCY_DISPATCH_BACK_OUT_CALL && $callType != LOG_CALLTYPE_EMERGENCY_DISPATCH_NOT_ON_SITE_CALL) {
				if ($currentTech == -1 || ($callType != LOG_CALLTYPE_1_HR_CALL && $currentTech == $techID) || ($callType == LOG_CALLTYPE_1_HR_CALL && ($currentStartDate == $startDate && $currentStartTime == $startTime && $currentTech == $techID)) ) {
					$groupedCallsUniqueID[] = $uniqueID;
					$groupedCallsCompanyID[] = $companyID;
					$currentTech = $techID;
					if ($callType == LOG_CALLTYPE_1_HR_CALL) {
						$currentStartDate = $startDate;
						$currentStartTime = $startTime;
					}
					$currentPhone = $phone;
				}
				
				if ($processed == $numWO || ($callType != LOG_CALLTYPE_1_HR_CALL && $currentTech != $techID) || ($callType == LOG_CALLTYPE_1_HR_CALL && ($currentStartDate != $startDate || $currentStartTime != $startTime || $currentTech != $techID)) ) {
					$groupedCalls = implode(",", $groupedCallsUniqueID);
					$groupedCompanyID = implode(",", $groupedCallsCompanyID);
					echo "<br/>**** CALL: TechID:$currentTech Phone:$currentPhone WOs:$groupedCalls**** <br/>";
					reminderCallBG(implode(",", $groupedCallsUniqueID), implode(",", $groupedCallsCompanyID), $currentPhone, $callType, $currentTech);
					if ($processed == $numWO && (($callType != LOG_CALLTYPE_1_HR_CALL && $currentTech != $techID) || ($callType == LOG_CALLTYPE_1_HR_CALL && ($currentStartDate != $startDate || $currentStartTime != $startTime || $currentTech != $techID))) ) {
						echo "<br/>**** CALL: TechID:$techID Phone:$phone WOs:$uniqueID **** <br/>";
						reminderCallBG($uniqueID, $companyID, $phone, $callType, $techID);
					}
					else {
						$groupedCallsUniqueID = array($uniqueID);
						$groupedCallsCompanyID = array($companyID);
						$currentTech = $techID;
						if ($callType == LOG_CALLTYPE_1_HR_CALL) {
							$currentStartDate = $startDate;
							$currentStartTime = $startTime;
						}
						$currentPhone = $phone;
					}
				}
			}
			else {
				reminderCallBG($uniqueID, $companyID, $phone, $callType, $techID);
			}						
		}
		echo "<hr/>";
	}
		
	// find work orders needing reminder calls
		
	$fieldList = "TB_UNID, Tech_ID, PrimaryPhone, SecondaryPhone, PrimPhoneType, SecondPhoneType, EmergencyPhone, Company_ID, StartDate, StartTime";
						
	$emergencyDisptachNotOnSiteCalls = getReminderEmergencyDispatchNotOnSite($fieldList);
	$emergencyDisptachBackOutCalls = getReminderEmergencyDispatchBackOut($fieldList);
	$oneHrCalls = getReminder1Hr($fieldList);
	$twentyFourHrCalls = getReminder24Hr($fieldList);
	$acceptanceCalls = getReminderAcceptance($fieldList);
	$notCompleteCalls = getReminderNotMarkedComplete($fieldList);
	$incompleteCalls = getReminderIncomplete($fieldList);

	runCall(LOG_CALLTYPE_EMERGENCY_DISPATCH_BACK_OUT_CALL, $emergencyDisptachBackOutCalls);
	runCall(LOG_CALLTYPE_EMERGENCY_DISPATCH_NOT_ON_SITE_CALL, $emergencyDisptachNotOnSiteCalls);
	runCall(LOG_CALLTYPE_1_HR_CALL, $oneHrCalls);
	runCall(LOG_CALLTYPE_24_HR_CALL, $twentyFourHrCalls);
	runCall(LOG_CALLTYPE_ACCEPTANCE_CALL, $acceptanceCalls);
	runCall(LOG_CALLTYPE_NOT_MARKED_COMPLETE_CALL, $notCompleteCalls);
	runCall(LOG_CALLTYPE_INCOMPLETE_CALL, $incompleteCalls);
?>