<?php

//Detect the browser
// put in lowercase to aid searching
$userBrowser = strtolower($_SERVER['HTTP_USER_AGENT']); 
//Detect acceptable document types
$userAccept= strtolower($_SERVER['HTTP_ACCEPT']);

function checkidentity($fromThis,$identities){
//this function iterates through the array identities matching to see if in the $fromThis string
// returns true if found otherwise false
  foreach ($identities as $identity) {
	if (stristr($fromThis,$identity)){
	  //found
	  return true;
	}
  }
  //not found
  return false;
}

if (stristr($userAccept,'wml')) {
   // This can accept wml (Wireless Meta Language files) so let's assume its WAP)
   // trouble is accept can contain wildcards ... but here we go
   $ub="WML";
   }
else {
	// Lets look at the browser
	//specify an array of identities to match against
	$wapidentity = array('wapbrowser','up.browser','up/4','mib','cellphone','go.web',
						 'nokia','panasonic','wap','wml-browser','wml', 'polaris',
						 );// can add other identities to this list
	$pcidentity = array(
			'mozilla','gecko','opera','omniweb','msie','konqueror','safari',
			'netpositive' ,'lynx' ,'elinks' ,'links' ,'w3m' ,'webtv' ,'amaya' ,
			'dillo' ,'ibrowse' ,'icab' ,'crazy browser' ,'internet explorer' 
			); // can add other identities to this list
	$pspidentity= array('PlayStation Portable'); //can add other identities to this list
	$iPhoneidentity= array('iphone'); //can add other identities to this list
	if (checkidentity($userBrowser,$wapidentity)){
	  $ub="WML";
	  }
	elseif (checkidentity($userBrowser,$iPhoneidentity)){
	  $ub="iphone";
	  }
	elseif (checkidentity($userBrowser,$pcidentity)){
	  $ub= "PC";
	  }
	else {
	  $ub="WML"; // can't find anything else so let's hope it is WML 
	}
}


//Code for redirecting based upon the results
if ($ub == 'WML') {
	header("Location: ". "http://wap.fieldsolutions.com/wap");
	exit;
}




/* switch ($ub){
  case 'WML':
*/


/*
	exit;
	  
  case 'iphone':
    header("Location: ". "http://wap.fieldsolutions.com"); // Chnage this later
  exit;
  default:
  	//  case 'PC':
   header("Location: ". "http://wap.fieldsolutions.com");
}
*/


?>