<?php

$url = $_SERVER['HTTP_HOST'];
$_root = $_SERVER['DOCUMENT_ROOT'];
require_once($_root . "/headerStartSession.php");

if($_SESSION['loggedIn'] == "yes"){

    Header("location: http://$url/unsubscribe");
    exit;
}

require($_root . "/header.php");
require($_root . "/navBar.php");

?>
<link rel="stylesheet" type="text/css" href="unsubscribe.css" />
<script type="text/javascript" language="javascript">
        function cbButtonHover(obj,CSSclass) {
                try {
                        obj.className=CSSclass;
                } catch (e) {}
        }
</script>
<script type="text/javascript">
$(document).ready(function () {
  $('#techLoginForm').submit(function(event) {
                event.preventDefault();

          var formData = $(this).serializeArray();

          var idx;
          var query = "";

          for ( idx in formData ) {
           if ( formData[idx].value ) {
               query += '&' + formData[idx].name + '=' + encodeURIComponent(formData[idx].value).replace(/\~/g, '
%7E').replace(/\!/g, '%21').replace(/\'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29');
           }
       }

           l  = location.protocol + '//' + location.host;
           l += '/unsubscribe/setTechLoginSession.php?v=true';
           l += query;

           location = l;
        });

        var reqLogin = '<?=$_REQUEST['login']?>';

        if(reqLogin == "false"){
                $("#authFailed").show();
        }else if(reqLogin == "notactivated"){
                $("#notActivated").show();
        }
        else{
                $("#authFailed").hide();
                $("#notActivated").hide();
        }

});
function sendActivateEmail(email){
        jQuery.ajax({
                type: "POST",
                url: "/techs/ajax/sendActivateEmail.php",
                dataType    : 'json',
                cache       : false,
                data: {email: email},
                success: function (data) {

                jQuery("#notActivated").html("An email has been sent to your primary email address.");


                }
        });
}
</script>


<br style="clear: both;">

<div style="width: 800px; height: 400px;">
<div align="center" style="width: 100%;" >
<p id="authFailed" class="error">
Authentication failed. The information you have provided cannot be authenticated. Check your login information and try again.
</p>

<p id="notActivated" class="error">Your login information is incorrect or your account is not active. Please try re-entering your user name and password. New registrants <a href="javascript:void(0);" onclick="sendActivateEmail('<?=$_REQUEST['email']?>');">Click Here</a> to have the verification email sent again. If you need assistance, please contact Support at
        952-288-2500 or support@fieldsolutions.com.</p>

<form style="margin: 0px;" action="http://<?=$url?>/unsubscribe/logIn.php" method="post" id="techLoginForm" >
<input type="hidden" name="pathname" value=""/>
<input type="hidden" name="PrevPageID" value="0"/>
<table  cellpadding="0" cellspacing="0" border="0">
<tr>
        <td>
                <table  cellspacing="0"  class="cbFormTable">
        <tr class="cbFormTableRow">
                <td colspan="2" class="cbHTMLBlockContainer">

                <b>Please login below to change your subscription:</b><p>

                <a href="http://www.fieldsolutions.com/misc/lostPassword.php">Forgot User Name and/or Password?</a>

                </td>
        </tr>
        <tr class="cbFormTableEvenRow">
                <td  class="cbFormLabelCell  cbFormLabel" >

                <label for="xip_UserName" >Tech User Name</label>

                </td>
                <td  class="cbFormFieldCell">

                <input type="text" class="cbFormTextField" name="xip_UserName" id="xip_UserName" size="25" maxlength="255" value=""/>

                </td>
        </tr>
        <tr class="cbFormTableRow">
                <td  class="cbFormLabelCell  cbFormLabel" >

                <label for="xip_Password" >Tech Password</label>

                </td>
                <td  class="cbFormFieldCell">

                <input type="password" class="cbFormPassword" name="xip_Password" id="xip_Password" size="25" maxlength="255" value=""/>
                </td>
        </tr>
        <tr class="cbFormTableEvenRow">
                <td colspan="2" class="cbLoginButtonContainer">
                <input type="submit" name="xip_datasrc_TR_Master_List" value="Login"  class="cbLoginButton" onmouseover="cbButtonHover(this,'cbLoginButton_hover');" onmouseout="cbButtonHover(this,'cbLoginButton');" id="xip_datasrc_TR_Master_List" />
                </td>
        </tr>
        </table>

        </td>
</tr>
</table>

</form>

<script type="text/javascript" language="javascript">

if(document.getElementById){if(document.getElementById("techLoginForm")&&document.getElementById("techLoginForm").xip_UserName!= null){try{document.getElementById("techLoginForm").xip_UserName.focus();}catch(v_e){}}else{try{document.getElementById("xip_UserName").focus();}catch(v_e){}}}

</script>

</div>

</div>