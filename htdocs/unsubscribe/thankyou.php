<?php

$url = $_SERVER['HTTP_HOST'];
$_root = $_SERVER['DOCUMENT_ROOT'];
require_once($_root . "/headerStartSession.php");
require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');

if($_SESSION['loggedIn'] != "yes"){
        // redirect to login
    header("Location: http://{$url}/unsubscribe/logIn.php");
    exit();
} else {
    //echo "<pre>"; print_r($_SESSION); echo "</pre>";
    if($_SESSION['loggedInAs'] != "tech"){
        header("Location: http://{$url}/logOut.php");
                exit();
        } else {
                // they're a tech. its okay. continue.
        }
}

require($_root . "/header.php");
require($_root . "/navBar.php");

?>
<link rel="stylesheet" type="text/css" href="unsubscribe.css" />
<br style="clear: both;"/>

Changes to your Subscription Status have been saved. Thank you.

