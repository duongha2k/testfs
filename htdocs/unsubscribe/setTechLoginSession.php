<?php
//This page is will log all clients that login by inserting into Caspio using SOAP.
require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');

try {

        // Retrieve values from login
        $UserName = $_REQUEST['xip_UserName'];
        $Password = $_REQUEST['xip_Password'];

        $authData = array('login'=>$UserName, 'password'=>$Password);

        $user = new Core_Api_TechUser();
        $user->checkAuthentication($authData);

        if (!$user->isAuthenticate() || !$user->isActivated()){
                $redirectUrl = empty($_REQUEST['r']) ? '' : "&r=" . urlencode($_REQUEST['r']);
                if(!$user->isActivated()){
                        header("Location: /unsubscribe/logIn.php?login=notactivated&email=".$user->getEmail() . $redirectUrl);
                }else{
                        header("Location: /unsubscribe/logIn.php?login=false" . $redirectUrl);
                }
                return API_Response::fail();
        }

        $TechID = $user->getTechID();
        $Date_TermsAccepted = strtotime($user->getDateAccepted());
        $FLSID = $user->getFlsID();
        $FLSCSP_Rec= $user->getFlsCpRec();
        $ISO_ID= $_REQUEST['ISO_ID'];

        $Refresh= $_REQUEST['Refresh'];

require_once("../headerStartSession.php");

//$techLoginMessageViewed = $_SESSION['techLoginMessageViewed'];
//$techLoginMessageViewed = isset($_SESSION['techLoginMessageViewed']) ? $_SESSION['techLoginMessageViewed'] : null;
$_SESSION['loggedIn']="yes";
$_SESSION['loggedInAs']="tech";
$_SESSION['TechID']=$TechID;
$_SESSION['FLSID']=$FLSID;
$_SESSION['FLSCSP_Rec']=$FLSCSP_Rec;
$_SESSION["FSCert"] = "";
$_SESSION['ISO_ID']=$ISO_ID;
$_SESSION["UserName"]=$UserName;

// Library calls
//require_once("../library/caspioAPI.php");

//$user = new Core_Api_TechUser();
//$user->load(array('login'=> $_SESSION["UserName"]));
//$_SESSION["UserPassword"] = $user->getPassword();

// Insert Login History into Login_History
//$insertWorkorders = caspioInsert("Login_History", "UserName, DateTime, UserType", "'$UserName', GETDATE(), 'Tech'",false);
//$user->loginHistory($UserName);


// Get Subdomain
$urlParts = explode('.', $_SERVER['HTTP_HOST']);
$template = $urlParts[0];
$siteTemplate = $_SESSION['template'];

$checkIndexDisplay = $template;

if($siteTemplate == ""){
        $_SESSION['template']=$template;
        $siteTemplate = $_SESSION['template'];
}else if($siteTemplate != $template){
        $_SESSION['template']=$template;
        $siteTemplate = $_SESSION['template'];
}

}catch (SoapFault $fault){
//FTP Failure code


//smtpMail("getClientLoginHistory", "admin@fieldsolutions.com", "cmcgarry@fieldsolutions.com,gbailey@fieldsolutions.com", "getClientLoginHistory Error", "$fault", "$fault", "getClientLoginHistory.php");
}


// LOGIN REDIRECTS
//$techLoginMessageViewed = $_COOKIE["techLoginMessageViewed"];
//$techLoginMessageViewed = isset($_COOKIE["techLoginMessageViewed"]) ? $_COOKIE["techLoginMessageViewed"] : null;

// MM/DD/YYYY HH:MM:SS

$_SESSION["acceptedICA"] = ($Date_TermsAccepted == "" || $Date_TermsAccepted < strtotime('03/03/2008 10:00:00 PM') ? "No" : "Yes");


Header("Location: http://{$_SERVER['HTTP_HOST']}/unsubscribe/");
exit;
?>