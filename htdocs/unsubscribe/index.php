<?php

$url = $_SERVER['HTTP_HOST'];
$_root = $_SERVER['DOCUMENT_ROOT'];
require_once($_root . "/headerStartSession.php");
require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');

$db = Zend_Registry::get('DB');

/*
 * check if they're logged in.
 * if not redirect to the unsubscribe centric login page
 * it will redirect them back here after they log in.
 */
if($_SESSION['loggedIn'] != "yes"){
    // redirect to login
	require($_root . "/header.php");
//    header("Location: techs/logIn.php");
    exit();
} else {
    //echo "<pre>"; print_r($_SESSION); echo "</pre>";
    if($_SESSION['loggedInAs'] != "tech"){
        header("Location: http://{$url}/logOut.php");
        exit();
    } else {
        // they're a tech. its okay. continue.
        // getting saved settings though.
        //echo "<pre>"; print_r($_SESSION); echo "</pre>";
        //$tech = new Core_Tech($_SESSION['TechID']);
        $tech = new API_Tech();
        $tech->lookupID($_SESSION['TechID'], null, true);
        //echo "<pre>"; print_r($tech); echo "</pre>";
        //echo "<pre>info - "; print_r($info); echo "</pre>";
    }
}


/*
 * processing request parameters.
 * required: tech , the TechID to unsubscribe.
 *
 */
if(isset($_REQUEST) && !empty($_REQUEST) ){
    if(isset($_REQUEST['tech']) ){
        $update = array();
        if($_REQUEST['unsubscribe']['site']){
            $info = array(
                "id" => $tech->TechID,
                "name" => $tech->Firstname . " " . $tech->Lastname,
                "email" => $tech->PrimaryEmail,
                "phone" => $tech->PrimaryPhone,
                "reason" => $_REQUEST['EditRecordSubscribeChangeReason']
            );

            //echo "<br />Reaching Site";
            $mail = new Core_EmailForUnsubscribe();
            //$mail->setInfo($info);
            $mail->sendMail($info);

            //deactivate user
            $update["Deactivated"] = $_REQUEST['unsubscribe']['site'];
            $update["Deactivate_Reason"] = $_REQUEST['EditRecordSubscribeChangeReason'];
            //echo "<pre>Update Site: "; print_r($n1); echo "</pre>";
        }else{
        	$update["Deactivated"] = 0;
        }

        if($_REQUEST['unsubscribe']['email'] ){
            $update[ 'EmailContactOptOut'] = 1;
            //echo "<br />Reaching Email";
            //echo "<pre>"; print_r($n2); echo "</pre>";
        }else{
        	$update[ 'EmailContactOptOut'] = 0;
        }

        if($_REQUEST['unsubscribe']['sms'] ){
            //$update[ 'SMS_AgreeDate' ] = null;
            //$update[ 'CellProvider'  ] = null;
            //$update[ 'SMS_Number'      ] = null;
            $update[ 'AllowText'] = 0;
            //echo "<br >Reaching SMS";
            //echo "<pre>"; print_r($n3); echo "</pre>";
        }else{
        	$update[ 'AllowText'] = 1;
        }
 
        if(!empty($update)){
            $n1 = $db->update("TechBankInfo", $update, "TechID = {$_REQUEST['tech']}" );
        }

        header("location: http://{$url}/unsubscribe/thankyou.php");
        exit;
    }
    //echo "<pre>"; print_r($_REQUEST); echo "</pre>";
}

require($_root . "/header.php");
require($_root . "/navBar.php");

?>
<script type="text/javascript" src="/widgets/js/fancybox/jquery.fancybox.js"></script>


<link rel="stylesheet" href="/widgets/js/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
<link rel="stylesheet" type="text/css" href="unsubscribe.css" />
<script type="text/javascript" language="javascript">
        function fancyAlert(msg) {
                jQuery.fancybox({
                        'modal': true,
                        'content' : "<div style='margin: 1px; width: 240px;'>"+msg+"<div style='text-align:right;margin-top:10px;'><input style='margin:10px;padding:0px;' type='button' onclick='jQuery.fancybox.close();' value='Ok'></div></div>"
                });
        }

        function fancyConfirm(msg, callback) {
                var ret;
                jQuery.fancybox({
                        modal: true,
                        content: "<div style='margin: 1px; width: 260px;'>"+msg+"<div style='text-align; right;margin-top:10px;'><input type='button' id='fancyConfirm_cancel' class='button' style='margin: 10px; padding: 0px;' value='Cancel'> <input type='button' id='fancyConfirm_ok' class='button'  style='margin: 10px; padding: 0px;' value='Ok'></div></div>",
                        onComplete: function() {
                                jQuery("#fancyConfirm_cancel").click(function() {
                                        ret = false;
                                        jQuery.fancybox.close();
                                })
                                jQuery("#fancyConfirm_ok").click(function() {
                                        ret = true;
                                        jQuery.fancybox.close();
                                })
                        },
                        onClosed: function() {
                                callback.call(this, ret);
                        }
                });
        }

        function fancyConfirm_text() {
                if (document.getElementById('unsubscribe_site').checked == true) {
                fancyConfirm("<div align='center'><font style='size: +1; color: red;'><b>Attention!</b></font></div><div>By closing your FieldSolutions account, you will no longer be visible to clients for future work in the FieldSolutions system. You will not have access to the system to close out existing work, and once your account is closed, you will not be paid for any work you have done that is not yet approved for payment by the client. <br /><br />Do you wish to continue with closing your account?<br />", function(ret) {
                        //alert(ret)
                        if(ret){
                                document.getElementById('unsubscribe_site').checked = true;
                        } else {
                                document.getElementById('unsubscribe_site').checked = false;
                        }
                        return ret;
                })
                } else {
                        document.getElementById('unsubscribe_site').checked = false;
                }
        }

    function cbButtonHover(obj,CSSclass) {
        try { obj.className=CSSclass; } catch (e) {}
    }

    function validateSubmit() {
        var site = document.getElementById('unsubscribe_site');
        //fancyAlert( site.checked );
        if (site.checked == true ){
            return fancyConfirm_text();
        } else {
            return true;
        }
    }

    function validateSubmit() {
        var site = document.getElementById('unsubscribe_site');

        if(site.checked){
        fancyConfirm("<div align='center'><font style='size: +1; color: red;'><b>Attention!</b></font></div><div>By closing your FieldSolutions account, you will no longer be visible to clients for future work in the FieldSolutions system. You will not have access to the system to close out existing work, and once your account is closed, you will not be paid for any work you have done that is not yet approved for payment by the client. <br /><br />Do you wish to continue with closing your account?<br />", function(ret) {
                if(ret){
                        document.getElementById('unsubscribe_form').submit();
                } else {
                        //document.getElementById('unsubscribe_site').checked = false;
                        fancyAlert('Form not submitted');

                }
                return ret;
        })
        } else {
                document.getElementById('unsubscribe_form').submit();
        }
    }

</script>
<br style="clear: both;"/>

<div align="center" id="shadowbox">
<form  style="margin: 0px;" action="<?=$_SERVER['PHP_SELF']?>" method="post" id="unsubscribe_form" >

<table  cellpadding="0" cellspacing="0" border="0">
<tr>
        <td>

        <table cellspacing="0"  class="cbFormTable">
        <tr class="cbFormTableRow">
                <td class="cbHTMLBlockContainer">
                        Not <?=$_SESSION['UserName']?>? <a href="http://<?=$url?>/logOut.php">Click Here</a>
                </td>
        </tr>
        <tr class="cbFormTableEvenRow">
                <td class="cbHTMLBlockContainer">
                <b>Change Your Subscription Status Below:</b><br><br>
                <font color ="red"><b>Important Note:</b></font> If you choose to
                unsubscribe from Available Work Emails <br /> and/or SMS Messages,
                you will no longer be contacted by FieldSolutions' clients <br />
                regarding work opportunities.
                </td>
        </tr>
        <tr class="cbFormTableRow">
                <td class="">

        <table>
        <tr class="cbFormTableRow">
                <td  class="cbFormLabelCell  cbFormLabel" >
                        <label for="EditEmailRecordAcceptTerms" ></label>
                </td>
                <td  class="cbFormFieldCell">
                        <span class="cbFormData">
                        <input type="checkbox" name="unsubscribe[email]" id="unsubscribe_email" value="1" <? if(is_object($tech) && $tech->EmailContactOptOut == 1){ ?>checked="checked"<? } ?> />
                        <label for="EditEmailRecordAcceptTerms0" >Unsubscribe from FieldSolutions Available Work Emails</label><br/>
                </td>
        </tr>
        <tr class="cbFormTableRow">
        <td  class="cbFormLabelCell  cbFormLabel" >
            <label for="EditSMSRecordAcceptTerms" ></label>
        </td>
        <td  class="cbFormFieldCell">
            <span class="cbFormData">
            <input type="checkbox" name="unsubscribe[sms]" id="unsubscribe_sms" value="1" <? if(is_object($tech) && $tech->AllowText != 1){ ?>checked="checked"<? } ?> />
            <label for="EditSMSRecordAcceptTerms0" >Unsubscribe from FieldSolutions Available Work SMS Messages</label><br/>
                </td>
        </tr>
        <tr class="cbFormTableRow">
        <td  class="cbFormLabelCell  cbFormLabel" >
            <label for="EditSiteRecordAcceptTerms" ></label>
        </td>
        <td  class="cbFormFieldCell">
            <span class="cbFormData">
            <input type="checkbox" name="unsubscribe[site]" id="unsubscribe_site" value="1" onclick="return fancyConfirm_text();" <? if(is_object($tech) && $tech->Deactivated == 1){ ?>checked="checked"<? } ?> >
            <label for="EditSiteRecordAcceptTerms0" >Unsubscribe from ALL FieldSolutions Emails and close my FieldSolutions account</label><br/>
                </td>
        </tr>
        </table>

                </td>
        </tr>
        <tr class="cbFormTableEvenRow">
                <td class="cbHTMLBlockContainer">
                <b>If Unsubscribing, please let us know why so we may improve our service:</b>
                </td>
        </tr>
        <tr class="cbFormTableRow">
                <td class="cbFormNestedTableContainer" >

                <table  class="cbFormNestedTable" cellspacing="0">
                <tr>
                        <td  class="cbFormLabelCell  cbFormLabel">
                        <label for="EditRecordSubscribeChangeReason" >&nbsp;</label>
                        </td>
                </tr>
                <tr>
                        <td  class="cbFormFieldCell">
                        <textarea rows="10" cols="50" class="cbFormTextArea"  onkeyup="cbTextAreaHelper.displayTip(this, 64000, null, null, null, 0);" onchange="cbTextAreaHelper.displayTip(this, 64000, null, null, null, 0);" onkeydown="return cbTextAreaHelper.controlKeyDown(event, this, 64000);" onkeypress="return cbTextAreaHelper.controlKeyPress(event, this, 64000);" name="EditRecordSubscribeChangeReason" id="EditRecordSubscribeChangeReason"></textarea>
                        </td>
                </tr>
                </table>
                </td>
        </tr>

        <tr class="cbFormTableEvenRow">
                <td class="cbUpdateButtonContainer">
                <a href="javascript: void(0);" onClick="return validateSubmit();" id="confirm" class="cbUpdateButton" >Update</a>

                </td>
        </tr>
        </table>

        <input type="hidden" name="tech" value="<?=$_SESSION['TechID']?>" />
        <input type="hidden" name="UpdateRecord" value="1"/>
        <input type="hidden" id="EditRecordPrimaryEmail" name="EditRecordPrimaryEmail" value="vroine@fieldsolutions.com"/>
        <input type="hidden" name="cpipage" value="1"/>

        </td>
</tr>
</table>

<input type="hidden" name="PrevPageID" value="4"/>
</form>

<script type="text/javascript" language="javascript">
        function f_CBRecalculate(){
                if(typeof cbTextAreaHelper!='undefined'&&document.getElementById('caspioform')!=null)for(var b=document.getElementsByTagName('textarea'),a=0;a<b.length;a++){if(b[a].name.search(/EditRecord|InsertRecord|value[0-9]+_[0-9]+/gi)!=-1)try{b[a].onchange()}catch(c){}
                }else setTimeout(f_CBRecalculate,300)}setTimeout(f_CBRecalculate,100);
</script>
<script type="text/javascript" language="javascript">
        if(document.getElementById){
                if(document.getElementById("caspioform")&&document.getElementById("caspioform").EditRecordAcceptTerms1!= null){
                        try{
                                document.getElementById("caspioform").EditRecordAcceptTerms1.focus();
                        }catch(v_e){}
                }else{
                        try{
                                document.getElementById("EditRecordAcceptTerms1").focus();
                        }catch(v_e){}
                }
        }
</script>


</div>