<?php
/**
 * API for FieldSolutions.com web services
 */
 
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'On');

require_once('includes/settings.php');
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('soap.wsdl_cache_ttl', '0');



// incomming data
$fields = array();
$fields[1]['name']    = 'WO_ID';
$fields[1]['value']   = 'API_Test503';
 
$fields[2]['name']    = 'Approved';
$fields[2]['value']   = 'False';
 
 
$fields[3]['name']    = 'City';
 
$fields[3]['value']   = 'Henderson';
 

 
$fields[4]['name']    = 'StartDate';
 
$fields[4]['value']   = '8/19/2009';
 

 
$fields[5]['name']    = 'EndDate';
 
$fields[5]['value']   = '8/19/2009';
 

 
$fields[6]['name']    = 'StartTime';
 
$fields[6]['value']   = '1:37 PM';
 
 
$fields[7]['name']    = 'EndTime';
 
$fields[7]['value']   = '2:13 PM';
 

 
$fields[8]['name']    = 'State';
 
$fields[8]['value']   = 'MT';
 
 
$fields[9]['name']    = 'Zipcode';
 
$fields[9]['value']   = '59866';
 

 
$fields[10]['name'] = 'Type_ID';
 
$fields[10]['value'] = '2';
 

 
$fields[11]['name'] = 'PayMax';
 
$fields[11]['value'] = '1';
 

 
$fields[12]['name'] = 'Amount_Per';
 
$fields[12]['value'] = 'Site';
 
 
$fields[13]['name'] = 'ShowTechs';
 
$fields[13]['value'] = 'True';

$fields[14]['name'] = 'Project_ID';
 
$fields[14]['value'] = '538';

// create SOAP client
$client = new SoapClient(WSDL_URL, array('cache_wsdl'=>WSDL_CACHE_NONE));



// create XML file contains WO data
$dom = new DOMDocument("1.0");
$root = $dom->createElement('wo');
foreach ($fields as $key=>$field) {
    $name = $dom->createAttribute('fieldName');
    $name->appendChild($dom->createTextNode($field['name']));

    $value = $dom->createAttribute('fieldValue');
    $value->appendChild($dom->createTextNode($field['value']));

    $parameter = $dom->createElement('parameter');
    $parameter->appendChild($name);
    $parameter->appendChild($value);

    $root->appendChild($parameter);
}
$dom->appendChild($root);
$result = $dom->saveXML();



$params['url'] = $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
$params['xml'] = $result;



$workOrders = $client->createWorkOrder($userData, $params);



echo "<pre>";
var_dump($workOrders);