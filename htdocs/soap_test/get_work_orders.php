<?php
/**
 * API for FieldSolutions.com web services
 */
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'On');

require_once('includes/settings.php');
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('soap.wsdl_cache_ttl', '0');

$client = new SoapClient(WSDL_URL, array('cache_wsdl'=>WSDL_CACHE_NONE));

$params = array();
$params['fieldList'] = 'Project_ID';
$params['criteria']['ProjectID'] = '39';
$params['criteria']['StartDate'] = '';
//$params['criteria']['TimeStampFrom'] = '07/10/2009 10:10';
//$params['criteria']['TimeStampTo'] = '07/10/2009 15:10';

/**
 * Available TimeStampDescription
 */

/*
Work Order Created
Work Order Published
Work Order Assigned
Work Order Reviewed
Work Order Confirmed
Deliverables Uploaded
Work Order Marked: Completed
Checked In Updated
Checked Out Updated
*/

$params['criteria']['TimeStampDescription'] = '';

$params['url'] = $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];

$workOrders = $client->getWorkOrdersList($userData, $params);
echo "<pre>";
var_dump($workOrders);