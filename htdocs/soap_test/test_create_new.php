<?php
define("FLSVersion", 1);
abstract class API_Abstract_WorkOrder {

	static $internalFields = array('PIC1','PIC2','PIC3','USERNAME','PASSWORD','PIC1_FROMTECH','PIC2_FROMTECH',
    	                        'PIC3_FROMTECH','PAID','PAYAPPROVE','CHECK_NUMBER','MOVEDTOMYSQL','ATTACHEDFILE',
    	                        'USEMYRECRUITEMAILSETTINGS','PRICINGRULEID','CALCULATEDINVOICE','PRICINGCALCULATIONTEXT',
    	                        'TECHNICIANFEE','PRICINGRULEAPPLIED','URGENT','FSSOURCINGSUPPORT','PRICINGRAN',
    	                        'REMINDERUSEWOSETTING','COMPANY_NAME','COMPANY_ID','CALCULATEDTOTALTECHPAY','PAYAMOUNT_FINAL',
    	                        'PRICINGACCUMULATOR', 'PRICINGACCUMULATOR2', 'FS_INTERNALCOMMENTS', "DEACTIVATED_DUMMYTECHFLAG", "TBPAY");
								
	static $readOnlyFields = array('PIC1','PIC2','PIC3','USERNAME','PASSWORD','PIC1_FROMTECH','PIC2_FROMTECH',
    	                            'PIC3_FROMTECH','PAID','PAYAPPROVE','CHECK_NUMBER','MOVEDTOMYSQL','ATTACHEDFILE',
    	                            'USEMYRECRUITEMAILSETTINGS','PRICINGRULEID','CALCULATEDINVOICE','PRICINGCALCULATIONTEXT',
    	                            'TECHNICIANFEE','PRICINGRULEAPPLIED','URGENT','FSSOURCINGSUPPORT','PRICINGRAN',
    	                            'REMINDERUSEWOSETTING','COMPANY_NAME','COMPANY_ID','CALCULATEDTOTALTECHPAY','PAYAMOUNT_FINAL',
    	                            'PRICINGACCUMULATOR', 'PRICINGACCUMULATOR2',
									'TB_UNID', 
									'WIN_NUM', 
									'DATEENTERED', 
									'TECH_FNAME', 
									'TBPAY', 
									'TECHCOMMENTS', 
									'TECH_LNAME', 
									'TECHPAID', 
									'DATEPAID', 
									'PAYAMOUNT', 
									'QTY_APPLICANTS', 
									'INVOICED', 
									'DATEINVOICED', 
									'DATEAPPROVED', 
									'WORKORDERREVIEWED', 
									'TECHCHECKEDIN_24HRS', 
									'DATE_IN', 
									'DATE_OUT', 
									'TIME_IN', 
									'TIME_OUT', 
									'TECHEMAIL', 
									'TECHPHONE', 
									'LASTMODBY', 
									'LASTMODDATE', 
									'DATE_COMPLETED', 
									'LATITUDE', 
									'LONGITUDE', 
									'AUTOBLASTONPUBLISH', 
									'RECRUITMENTFROMEMAIL', 
									'CALCULATEDTOTALTECHPAY', 
									'TECHMILES', 
									'TECHPHONEEXT', 
									'REMINDERACCEPTANCES', 
									'REMINDER24HR', 
									'REMINDER1HR', 
									'REMINDERCHECKINOUT', 
									'REMINDERMARKNOTCOMPLETE', 
									'REMINDERINCOMPLETE', 
									'ISPROJECTAUTOASSIGN', 
									'ISWORKORDERFIRSTBIDDER', 
									'MINIMUMSELFRATING', 
									'MAXIMUMALLOWABLEDISTANCE', 
									'REMINDERALL');
								
	/** 
	 * 
	 * @var int	 */
	var $WIN_NUM;


	/** 
	 * 
	 * @var string	 */
	var $WO_ID;


	/** 
	 * 
	 * @var string	 */
	var $DateEntered;


	/** 
	 * 
	 * @var string	 */
	var $StartDate;


	/** 
	 * 
	 * @var string	 */
	var $EndDate;


	/** 
	 * 
	 * @var string	 */
	var $Description;


	/** 
	 * 
	 * @var string	 */
	var $Requirements;


	/** 
	 * 
	 * @var string	 */
	var $Tools;


	/** 
	 * 
	 * @var string	 */
	var $PayMax;


	/** 
	 * 
	 * @var string	 */
	var $Address;


	/** 
	 * 
	 * @var string	 */
	var $City;


	/** 
	 * 
	 * @var string	 */
	var $State;


	/** 
	 * 
	 * @var string	 */
	var $Zipcode;


	/** 
	 * 
	 * @var string	 */
	var $Tech_FName;


	/** 
	 * 
	 * @var boolean	 */
	var $Approved;


	/** 
	 * 
	 * @var string	 */
	var $SpecialInstructions;


	/** 
	 * 
	 * @var string	 */
	var $SiteName;


	/** 
	 * 
	 * @var string	 */
	var $ClientComments;


	/** 
	 * 
	 * @var boolean	 */
	var $ShowTechs;


	/** 
	 * 
	 * @var int	 */
	var $Tech_ID;


	/** 
	 * 
	 * @var boolean	 */
	var $TechMarkedComplete;


	/** 
	 * 
	 * @var string	 */
	var $TechComments;


	/** 
	 * 
	 * @var string	 */
	var $MissingComments;


	/** 
	 * 
	 * @var string	 */
	var $Tech_LName;


	/** 
	 * 
	 * @var boolean	 */
	var $TechPaid;


	/** 
	 * 
	 * @var string	 */
	var $DatePaid;



	/** 
	 * 
	 * @var float	 */
	var $PayAmount;


	/** 
	 * 
	 * @var string	 */
	var $SiteNumber;


	/** 
	 * 
	 * @var string	 */
	var $Region;


	/** 
	 * 
	 * @var boolean	 */
	var $CheckedIn;


	/** 
	 * 
	 * @var string	 */
	var $Checkedby;


	/** 
	 * 
	 * @var string	 */
	var $CheckInNotes;


	/** 
	 * 
	 * @var string	 */
	var $StartTime;


	/** 
	 * 
	 * @var boolean	 */
	var $StoreNotified;


	/** 
	 * 
	 * @var string	 */
	var $Notifiedby;


	/** 
	 * 
	 * @var string	 */
	var $DateNotified;


	/** 
	 * 
	 * @var string	 */
	var $NotifyNotes;


	/** 
	 * 
	 * @var string	 */
	var $SitePhone;


	/** 
	 * 
	 * @var string	 */
	var $SiteEmail;


	/** 
	 * 
	 * @var string	 */
	var $SiteFax;


	/** 
	 * 
	 * @var string	 */
	var $PO;


	/** 
	 * 
	 * @var string	 */
	var $General1;


	/** 
	 * 
	 * @var string	 */
	var $General2;


	/** 
	 * 
	 * @var string	 */
	var $General3;


	/** 
	 * 
	 * @var string	 */
	var $General4;


	/** 
	 * 
	 * @var string	 */
	var $General5;


	/** 
	 * 
	 * @var string	 */
	var $General6;


	/** 
	 * 
	 * @var string	 */
	var $General7;


	/** 
	 * 
	 * @var string	 */
	var $General8;


	/** 
	 * 
	 * @var string	 */
	var $General9;


	/** 
	 * 
	 * @var string	 */
	var $General10;


	/** 
	 * 
	 * @var int	 */
	var $Qty_Applicants;


	/** 
	 * 
	 * @var boolean	 */
	var $Invoiced;


	/** 
	 * 
	 * @var string	 */
	var $DateInvoiced;


	/** 
	 * 
	 * @var string	 */
	var $DateApproved;


	/** 
	 * 
	 * @var string	 */
	var $Ship_Contact_Info;


	/** 
	 * 
	 * @var boolean	 */
	var $WorkOrderReviewed;


	/** 
	 * 
	 * @var boolean	 */
	var $Deactivated;


	/** 
	 * 
	 * @var boolean	 */
	var $TechCheckedIn_24hrs;


	/** 
	 * 
	 * @var boolean	 */
	var $TechCalled;


	/** 
	 * 
	 * @var string	 */
	var $CalledBy;


	/** 
	 * 
	 * @var string	 */
	var $CallDate;


	/** 
	 * 
	 * @var int	 */
	var $NoShow_Tech;


	/** 
	 * 
	 * @var string	 */
	var $Project_Name;


	/** 
	 * 
	 * @var int	 */
	var $Project_ID;


	/** 
	 * 
	 * @var string	 */
	var $Time_In;


	/** 
	 * 
	 * @var string	 */
	var $Time_Out;


	/** 
	 * 
	 * @var string	 */
	var $TechEmail;


	/** 
	 * 
	 * @var string	 */
	var $TechPhone;


	/** 
	 * 
	 * @var string	 */
	var $Date_Assigned;


	/** 
	 * 
	 * @var boolean	 */
	var $Update_Requested;


	/** 
	 * 
	 * @var string	 */
	var $Update_Reason;


	/** 
	 * 
	 * @var float	 */
	var $Additional_Pay_Amount;


	/** 
	 * 
	 * @var string	 */
	var $Add_Pay_AuthBy;


	/** 
	 * 
	 * @var boolean	 */
	var $Work_Out_of_Scope;


	/** 
	 * 
	 * @var string	 */
	var $OutOfScope_Reason;


	/** 
	 * 
	 * @var boolean	 */
	var $Site_Complete;


	/** 
	 * 
	 * @var string	 */
	var $Add_Pay_Reason;


	/** 
	 * 
	 * @var int	 */
	var $BackOut_Tech;


	/** 
	 * 
	 * @var string	 */
	var $EndTime;


	/** 
	 * 
	 * @var string	 */
	var $lastModBy;


	/** 
	 * 
	 * @var string	 */
	var $lastModDate;


	/** 
	 * 
	 * @var string	 */
	var $approvedBy;


	/** 
	 * 
	 * @var string	 */
	var $Date_Completed;


	/** 
	 * 
	 * @var string	 */
	var $Deactivated_Reason;


	/** 
	 * 
	 * @var string	 */
	var $Amount_Per;


	/** 
	 * 
	 * @var float	 */
	var $OutofScope_Amount;


	/** 
	 * 
	 * @var int	 */
	var $Qty_Devices;


	/** 
	 * 
	 * @var string	 */
	var $Site_Contact_Name;


	/** 
	 * 
	 * @var string	 */
	var $StartRange;


	/** 
	 * 
	 * @var string	 */
	var $Duration;


	/** 
	 * 
	 * @var int	 */
	var $Type_ID;


	/** 
	 * 
	 * @var string	 */
	var $DeactivatedDate;


	/** 
	 * 
	 * @var string	 */
	var $DeactivatedBy;


	/** 
	 * 
	 * @var int	 */
	var $Latitude;


	/** 
	 * 
	 * @var int	 */
	var $Longitude;


	/** 
	 * 
	 * @var string	 */
	var $Shipped_By;


	/** 
	 * 
	 * @var string	 */
	var $Part_No;


	/** 
	 * 
	 * @var string	 */
	var $Part_Desciption;


	/** 
	 * 
	 * @var string	 */
	var $RMA_No;


	/** 
	 * 
	 * @var string	 */
	var $Tracking_No;


	/** 
	 * 
	 * @var string	 */
	var $Part_ETA;


	/** 
	 * 
	 * @var string	 */
	var $Return_Part_Instructions;


	/** 
	 * 
	 * @var float	 */
	var $Total_Estimated_WO_Value;


	/** 
	 * 
	 * @var float	 */
	var $Tech_Bid_Amount;


	/** 
	 * 
	 * @var boolean	 */
	var $AutoBlastOnPublish;


	/** 
	 * 
	 * @var string	 */
	var $RecruitmentFromEmail;


	/** 
	 * 
	 * @var float	 */
	var $calculatedTechHrs;


	/** 
	 * 
	 * @var float	 */
	var $baseTechPay;


	/** 
	 * 
	 * @var float	 */
	var $TripCharge;


	/** 
	 * 
	 * @var float	 */
	var $MileageReimbursement;


	/** 
	 * 
	 * @var float	 */
	var $MaterialsReimbursement;


	/** 
	 * 
	 * @var string	 */
	var $TechnicianFeeComments;


	/** 
	 * 
	 * @var boolean	 */
	var $ExpediteFee;


	/** 
	 * 
	 * @var boolean	 */
	var $AbortFee;


	/** 
	 * 
	 * @var boolean	 */
	var $AfterHoursFee;


	/** 
	 * 
	 * @var float	 */
	var $OtherFSFee;


	/** 
	 * 
	 * @var int	 */
	var $TechMiles;


	/** 
	 * 
	 * @var float	 */
	var $AbortFeeAmount;


	/** 
	 * 
	 * @var string	 */
	var $WO_Category;


	/** 
	 * 
	 * @var int	 */
	var $WO_Category_ID;


	/** 
	 * 
	 * @var string	 */
	var $Headline;


	/** 
	 * 
	 * @var int	 */
	var $TechPhoneExt;


	/** 
	 * 
	 * @var boolean	 */
	var $ReminderAcceptance;


	/** 
	 * 
	 * @var boolean	 */
	var $Reminder24Hr;


	/** 
	 * 
	 * @var boolean	 */
	var $Reminder1Hr;


	/** 
	 * 
	 * @var boolean	 */
	var $CheckInCall;


	/** 
	 * 
	 * @var boolean	 */
	var $ReminderNotMarkComplete;


	/** 
	 * 
	 * @var boolean	 */
	var $ReminderIncomplete;


	/** 
	 * 
	 * @var string	 */
	var $DateIncomplete;


	/** 
	 * 
	 * @var string	 */
	var $ProjectManagerName;


	/** 
	 * 
	 * @var string	 */
	var $ProjectManagerPhone;


	/** 
	 * 
	 * @var string	 */
	var $ProjectManagerEmail;


	/** 
	 * 
	 * @var string	 */
	var $ResourceCoordinatorName;


	/** 
	 * 
	 * @var string	 */
	var $ResourceCoordinatorPhone;


	/** 
	 * 
	 * @var string	 */
	var $ResourceCoordinatorEmail;


	/** 
	 * 
	 * @var string	 */
	var $EmergencyName;


	/** 
	 * 
	 * @var string	 */
	var $EmergencyPhone;


	/** 
	 * 
	 * @var string	 */
	var $EmergencyEmail;


	/** 
	 * 
	 * @var string	 */
	var $TechnicalSupportName;


	/** 
	 * 
	 * @var string	 */
	var $TechnicalSupportPhone;


	/** 
	 * 
	 * @var string	 */
	var $TechnicalSupportEmail;


	/** 
	 * 
	 * @var string	 */
	var $CheckInOutName;


	/** 
	 * 
	 * @var string	 */
	var $CheckInOutNumber;


	/** 
	 * 
	 * @var string	 */
	var $CheckInOutEmail;


	/** 
	 * 
	 * @var boolean	 */
	var $isProjectAutoAssign;


	/** 
	 * 
	 * @var boolean	 */
	var $isWorkOrdersFirstBidder;


	/** 
	 * 
	 * @var int	 */
	var $MinutesRemainPublished;


	/** 
	 * 
	 * @var int	 */
	var $MinimumSelfRating;


	/** 
	 * 
	 * @var int	 */
	var $MaximumAllowableDistance;


	/** 
	 * 
	 * @var boolean	 */
	var $ReminderAll;


	/** 
	 * 
	 * @var boolean	 */
	var $CheckOutCall;


	/** 
	 * 
	 * @var boolean	 */
	var $ACSInvoiced;


	/** 
	 * 
	 * @var string	 */
	var $ACSDateInvoiced;


	/** 
	 * 
	 * @var boolean	 */
	var $SMSBlast;


	/** 
	 * 
	 * @var boolean	 */
	var $P2TPreferredOnly;


	/** 
	 * 
	 * @var boolean	 */
	var $P2TAlreadyAssigned;


	/** 
	 * 
	 * @var string	 */
	var $Date_In;


	/** 
	 * 
	 * @var string	 */
	var $Date_Out;


	/** 
	 * 
	 * @var boolean	 */
	var $CallClosed;


	/** 
	 * 
	 * @var boolean	 */
	var $ContactHD;


	/** 
	 * 
	 * @var string	 */
	var $HDPOC;


	/** 
	 * 
	 * @var boolean	 */
	var $Lead;


	/** 
	 * 
	 * @var boolean	 */
	var $Assist;


	/** 
	 * 
	 * @var boolean	 */
	var $SignOffSheet_Required;


	/** 
	 * 
	 * @var string	 */
	var $Date_Checked_in;


	/** 
	 * 
	 * @var string	 */
	var $ExtraTime;


	/** 
	 * 
	 * @var boolean	 */
	var $Unexpected_Steps;


	/** 
	 * 
	 * @var boolean	 */
	var $Incomplete_Paperwork;


	/** 
	 * 
	 * @var boolean	 */
	var $Paperwork_Received;


	/** 
	 * 
	 * @var string	 */
	var $DeactivationCode;
	
	public function __construct($id) {
		$this->WIN_NUM = $id;
	}
	
	public function toXMLElement($dom, $fieldList = NULL) {
		if (!is_array($fieldList))
			$fieldList = $this;
		$root = $dom->createElement('WO');
		foreach ($this as $key => $value) {
			if (!array_key_exists($key, $fieldList)) continue;
			$parameter = $dom->createElement($key, htmlspecialchars($value, ENT_QUOTES));
			$root->appendChild($parameter);
		}
		$dom->appendChild($root);
		return $root;
	}
	
	public static function workOrderWithResult($result) {
	    $workOrder = new API_WorkOrder(NULL);
	    if (!empty($result)) {
		    foreach ($result as $k=>$val) {
		    	$field = $k == "TB_UNID" ? "WIN_NUM" : $k;
				if (!array_key_exists($field, $workOrder)) continue;
				$workOrder->$field = $result[$k];
		    }
	    }
		return $workOrder;
	}

	public static function toXMLWithArray($array, $param = NULL) {
		if ($param == NULL)
			$param = array();
		$fieldList = array_key_exists("fieldList", $param) ? $param["fieldList"] : NULL;
		$single = array_key_exists("single", $param) ? $param["single"] : false;
		if ($fieldList != NULL)
			$fieldList = array_flip(explode(",", $fieldList));
		$dom = new DOMDocument("1.0");
		if (!$single) {
			$root = $dom->createElement("WOS");
			$dom->appendChild($root);
		}
		else
			$root = $dom;
	    if (!empty($array[0])) {
			foreach ($array as $key=>$value) {
				$root->appendChild($value->toXMLElement($dom, $fieldList));
			}
	    }
		return $dom->saveXML();
	}

/*	private static function workOrderWithArray($array) {
	    $wo = new API_WorkOrder($value["TB_UNID"]);
	    foreach ($value as $k=>$val) {
	        if (!in_array( strtoupper($k), self::$internalFields)) {
		    	$field = $k == "TB_UNID" ? "WIN_NUM" : $k;
				if ($val == "True") $val = true;
				if ($val == "False") $val = false;
				$wo->$field = $val;
	        }
	    }
		return $wo;
	}*/
}

if (!defined("FLSVersion")):

/**
 * class API_WorkOrder
 * @package API_WorkOrder
 */
class API_WorkOrder extends API_Abstract_WorkOrder {
}

else:

/**
 * class API_WorkOrder
 * @package API_WorkOrder
 */
class API_WorkOrder extends API_Abstract_WorkOrder {
	/** 
	 * 
	 * @var boolean	 */
	var $ShortNotice;

	/** 
	 * 
	 * @var boolean	 */
	var $PreCall_1;

	/** 
	 * 
	 * @var boolean	 */
	var $PreCall_2;

	/** 
	 * 
	 * @var boolean	 */
	var $FLS_OOS;

	/** 
	 * 
	 * @var boolean	 */
	var $Unexpected_Steps;

	/** 
	 * 
	 * @var string	 */
	var $Unexpected_Desc;

	/** 
	 * 
	 * @var string	 */
	var $AskedBy;


	/** 
	 * 
	 * @var string	 */
	var $AskedBy_Name;

	/** 
	 * 
	 * @var string	 */
	var $BU1FLSID;


	/** 
	 * 
	 * @var string	 */
	var $BU1Info;

	/** 
	 * 
	 * @var string	 */
	var $BU2FLSID;


	/** 
	 * 
	 * @var string	 */
	var $BU2Info;

	/** 
	 * 
	 * @var string	 */
	var $FLS_ID;

	/** 
	 * 
	 * @var string	 */
	var $SourceByDate;
}

endif;

















/**
 * API for FieldSolutions.com web services
 */
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'On');

require_once('includes/settings.php');
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('soap.wsdl_cache_ttl', '0');

// incomming data

$wo = new API_WorkOrder();
$wo->WO_ID = "APITest_0308_02";
$wo->StartDate = "3/12/2010";
$wo->Description = "Test SR for Bpel testing";
$wo->Address = "223 AREQUES AVE";
$wo->City = "SUNNYVALE";
$wo->State = "CA";
$wo->Zipcode = "94086";
$wo->Sitename = "STAPLES, INC-SPL0012";
$wo->StartTime = "10:15 PM";
$wo->Ship_Contact_Info = NULL;
$wo->Project_Name = "Service Call";
$wo->TechEmail = "narayan.raykar@us.fujitsu.com";
$wo->Duration = NULL;
$wo->Type_ID = 1;
$wo->WIN_NUM = 82211;

// create SOAP client
$client = new SoapClient(WSDL_URL, array('cache_wsdl'=>WSDL_CACHE_NONE, 'trace' => 1));

// create XML file contains WO data
/*$dom = new DOMDocument("1.0");
$root = $dom->createElement('WO');
foreach ($fields as $key=>$field) {
    $parameter = $dom->createElement($field['name'], $field['value']);
    $root->appendChild($parameter);
}
$dom->appendChild($root);
$result = $dom->saveXML();



$params['url'] = $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
$params['xml'] = $result;*/

//$workOrders = $client->createWorkOrder("fls-api", "FlsApi09$", false, $wo);
$workOrders = $client->updateWorkOrder("fls-api", "FlsApi09$", 82211, $wo);
echo $client->__getLastRequest();
//$workOrders = $client->assignWorkOrder("codem01", "cLutch44", 69716, 19806, 20.00);

echo "<pre>";
var_dump($workOrders);