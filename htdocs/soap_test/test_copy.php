<?php
/**
 * API for FieldSolutions.com web services
 */
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'On');

require_once('includes/settings.php');
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('soap.wsdl_cache_ttl', '0');

$fields = array();

$field['WO_ID']  = 'APICopyTest6';

// create SOAP client
$client = new SoapClient(WSDL_URL, array('cache_wsdl'=>WSDL_CACHE_NONE));

// create XML file contains WO data
$dom = new DOMDocument("1.0");
$root = $dom->createElement('wo');

$value = $dom->createAttribute('WO_ID');
$value->appendChild($dom->createTextNode($field['WO_ID']));

$as = $dom->createElement('parameter');
$as->appendChild($value);

$root->appendChild($as);

$dom->appendChild($root);
$result = $dom->saveXML();

$params['TB_UNID'] = '55013';
$params['url'] = $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
$params['xml'] = $result;

/*
$xml = simplexml_load_string($result);
echo "<pre>";
var_dump($xml->parameter['WO_ID']);
*/

$workOrders = $client->copyWorkOrder($userData, $params);
echo "<pre>";
var_dump($workOrders);