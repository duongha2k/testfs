<?php
/**
 * API for FieldSolutions.com web services
 */
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'On');

require_once('includes/settings.php');
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('soap.wsdl_cache_ttl', '0');

// create SOAP client
$clientO = new SoapClient("https://api.fieldsolutions.com:417/api/client/wsdl", array('cache_wsdl'=>WSDL_CACHE_NONE));
$client = new SoapClient(WSDL_URL, array('cache_wsdl'=>WSDL_CACHE_NONE));

$t0 = time();

//$workOrders = $client->assignWorkOrder("codem01", "cLutch44", 48092, 15527, 175.00);

//$workOrders = $client->updateWorkOrder("codem01", "cLutch44", 7103, $result);

//$workOrders = $client->approveWorkOrder("codem01", "cLutch44", 7103);

//$workOrders = $client->deactivateWorkOrder("codem01", "cLutch44", 7103, 11, "");

$workOrders = $client->getWorkOrders("codem01", "cLutch44", "WIN_NUM", "", "", "", array("Tech_ID" => "19806"));
$workOrdersO = $clientO->getWorkOrders("codem01", "cLutch44", "WIN_NUM", "", "", "", array("Tech_ID" => "19806"));

$tf = time();

echo "$tf<br/>";
echo "$t0<br/>";
echo $tf - $t0 . "<br/>";

//$workOrders = $client->updateWorkOrder("core-api", "Scrimsha", 7103, $result); //58054

echo "<pre>";
var_dump($workOrders);
var_dump($workOrdersO);