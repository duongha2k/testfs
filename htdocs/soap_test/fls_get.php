<?php
/**
 * API for FieldSolutions.com web services
 */
 
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'On');

require_once('includes/settings_fls.php');
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('soap.wsdl_cache_ttl', '0');

// incomming data
$fields = array();

$fields[1]['name']    = 'Duration';
$fields[1]['value']   = 3;

// create SOAP client
$client = new SoapClient(WSDL_URL, array('cache_wsdl'=>WSDL_CACHE_NONE));

// create XML file contains WO data
$dom = new DOMDocument("1.0");
$root = $dom->createElement('FLSWO');
foreach ($fields as $key=>$field) {
    $parameter = $dom->createElement($field['name'], $field['value']);
    $root->appendChild($parameter);
}
$dom->appendChild($root);
$result = $dom->saveXML();


$params['url'] = $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
$params['xml'] = $result;

echo htmlentities($result);

//$workOrders = $client->getWorkOrder("codem01", "cLutch44", 140585);
$timeStampFrom = '10/25/2009 1:38';
$timeStampTo = '';
$timeStampType = 'Incomplete';

$filters = array();
$filters["ShortNotice"] = "False";
	
$workOrders = $client->getWorkOrders("codem01", "cLutch44", 
						"", 
						$timeStampFrom, 
						$timeStampTo,
						$timeStampType, 
						$filters);

echo "<pre>";
var_dump($workOrders);