<?php
/**
 * API for FieldSolutions.com web services
 */
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'On');

require_once('includes/settings_fls.php');
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('soap.wsdl_cache_ttl', '0');

$fields = array();

//$fields[1]['name']    = 'Tech_ID';
//$fields[1]['value']   = '19806';

$fields[1]['name']    = 'Description';
$fields[1]['value']   = 'Test';

//$fields[2]['name']    = 'TechMarkedComplete';
//$fields[2]['value']   = 'False';

/*$fields[2]['name']    = 'AbortFeeAmount';
$fields[2]['value']   = '15.00';

$fields[3]['name']    = 'Approved';
$fields[3]['value']   = 'True';*/

//$fields[2]['name']    = 'PayMax';
//$fields[2]['value']   = '1.00';

/*$fields[3]['name']    = 'Amount_Per';
$fields[3]['value']   = 'Site';*/


// create SOAP client
$client = new SoapClient(WSDL_URL, array('cache_wsdl'=>WSDL_CACHE_NONE));



// create XML file contains WO data
$dom = new DOMDocument("1.0");
$root = $dom->createElement('WO');
foreach ($fields as $key=>$field) {
    $parameter = $dom->createElement($field['name'], $field['value']);
    $root->appendChild($parameter);
}
$dom->appendChild($root);
$result = $dom->saveXML();



$params['url'] = $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
$params['xml'] = $result;

$t0 = microtime();

//$workOrders = $client->updateWorkOrder("codem01", "cLutch44", 7103, $result);
$filter = array();
$filter["Addedby"] = "co";
$workOrders = $client->getWorkOrders("codem01", "cLutch44", "WorkOrderID, Addedby", "", "", "", $filter);

$tf = microtime();

echo "DHDH: $tf - $t0 = " . $tf - $t0 . " ms<br/>";
//$workOrders = $client->updateWorkOrder("core-api", "Scrimsha", 7103, $result); //58054

echo "<pre>";
var_dump($workOrders);