<?php
/**
 * API for FieldSolutions.com web services
 */
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'On');

require_once('includes/settings.php');
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('soap.wsdl_cache_ttl', '0');

$fields = array();

$fields[1]['name']    = 'Status';
$fields[1]['value']   = 'In_Accounting';

$fields[2]['name']    = 'Invoiced';
$fields[2]['value']   = 'False';

$fields[3]['name']    = 'TechPaid';
$fields[3]['value']   = 'True';

/*$fields[2]['name']    = 'Deactivated_Reason';
$fields[2]['value']   = 'jus in case';

$fields[3]['name']    = 'Headline';
$fields[3]['value']   = 'qweqwe qwe';

$fields[4]['name']    = 'WO_Category_ID';
$fields[4]['value']   = '5';
*/

// create SOAP client
$client = new SoapClient(WSDL_URL, array('cache_wsdl'=>WSDL_CACHE_NONE));

// create XML file contains WO data
$dom = new DOMDocument("1.0");
$root = $dom->createElement('wo');

foreach ($fields as $key=>$field) {
    $name = $dom->createAttribute('fieldName');
    $name->appendChild($dom->createTextNode($field['name']));

    $value = $dom->createAttribute('fieldValue');
    $value->appendChild($dom->createTextNode($field['value']));

    $parameter = $dom->createElement('parameter');
    $parameter->appendChild($name);
    $parameter->appendChild($value);

    $root->appendChild($parameter);
}

$dom->appendChild($root);
$result = $dom->saveXML();

$params['TB_UNID'] = '43541';
$params['url'] = $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
$params['xml'] = $result;

$workOrders = $client->setWorkOrderStatus($userData, $params);
echo "<pre>";
var_dump($workOrders);