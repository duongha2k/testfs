<?php
/**
 * API for FieldSolutions.com web services
 */
 
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'On');

require_once('includes/settings_fls.php');
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('soap.wsdl_cache_ttl', '0');

// incomming data
$fields = array();

$fields[1]['name']    = 'ProjectName';
$fields[1]['value']   = "API Test Project";

// create SOAP client
$client = new SoapClient(WSDL_URL, array('cache_wsdl'=>WSDL_CACHE_NONE));

// create XML file contains WO data
$dom = new DOMDocument("1.0");
$root = $dom->createElement('FLSWO');
foreach ($fields as $key=>$field) {
    $parameter = $dom->createElement($field['name'], $field['value']);
    $root->appendChild($parameter);
}
$dom->appendChild($root);

print_r($dom);
$result = $dom->saveXML();

$params['url'] = $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
$params['xml'] = $result;

echo htmlentities($result);

$t = $client->__getTypes();
var_dump($t);

/**
 * class API_FlsWorkOrder
 * @package API_FlsWorkOrder
 */
class API_FlsWorkOrder {
	/** 
	 * 
	 * @var int	 */
	var $WorkOrderID;

	/** 
	 * 
	 * @var string	 */
	var $WorkOrderNo;


	/** 
	 * 
	 * @var string	 */
	var $FLSTechID;


	/** 
	 * 
	 * @var string	 */
	var $TechName;


	/** 
	 * 
	 * @var string	 */
	var $DateEntered;


	/** 
	 * 
	 * @var string	 */
	var $DateCompleted;


	/** 
	 * 
	 * @var string	 */
	var $StartTime;


	/** 
	 * 
	 * @var string	 */
	var $EndTime;


	/** 
	 * 
	 * @var string	 */
	var $Comments;


	/** 
	 * 
	 * @var string	 */
	var $Parts;


	/** 
	 * 
	 * @var boolean	 */
	var $CallClosed;


	/** 
	 * 
	 * @var boolean	 */
	var $PartsOk;


	/** 
	 * 
	 * @var boolean	 */
	var $TechComplete;


	/** 
	 * 
	 * @var boolean	 */
	var $PayApprove;


	/** 
	 * 
	 * @var string	 */
	var $PartNos;


	/** 
	 * 
	 * @var string	 */
	var $QtyParts;


	/** 
	 * 
	 * @var string	 */
	var $Waybill;


	/** 
	 * 
	 * @var string	 */
	var $RA;


	/** 
	 * 
	 * @var string	 */
	var $Paid;


	/** 
	 * 
	 * @var string	 */
	var $DatePaid;


	/** 
	 * 
	 * @var string	 */
	var $CallType;


	/** 
	 * 
	 * @var string	 */
	var $ProjectName;


	/** 
	 * 
	 * @var string	 */
	var $FLSComments;


	/** 
	 * 
	 * @var float	 */
	var $PayAmt;


	/** 
	 * 
	 * @var string	 */
	var $SerialNo;


	/** 
	 * 
	 * @var boolean	 */
	var $ContactHD;


	/** 
	 * 
	 * @var string	 */
	var $HDPOC;


	/** 
	 * 
	 * @var string	 */
	var $SerialNoOut;


	/** 
	 * 
	 * @var string	 */
	var $SiteName;


	/** 
	 * 
	 * @var string	 */
	var $Address1;


	/** 
	 * 
	 * @var string	 */
	var $City;


	/** 
	 * 
	 * @var string	 */
	var $State;


	/** 
	 * 
	 * @var string	 */
	var $Zip;


	/** 
	 * 
	 * @var boolean	 */
	var $Invoiced;


	/** 
	 * 
	 * @var string	 */
	var $DateInvoiced;


	/** 
	 * 
	 * @var string	 */
	var $PjctStartDate;


	/** 
	 * 
	 * @var string	 */
	var $PjctStartTime;


	/** 
	 * 
	 * @var string	 */
	var $DateImported;


	/** 
	 * 
	 * @var string	 */
	var $MisssingInfo;


	/** 
	 * 
	 * @var string	 */
	var $Deactivated;


	/** 
	 * 
	 * @var string	 */
	var $SiteList;


	/** 
	 * 
	 * @var string	 */
	var $DateApproved;


	/** 
	 * 
	 * @var string	 */
	var $TroubleShoot;


	/** 
	 * 
	 * @var string	 */
	var $TechEmail;


	/** 
	 * 
	 * @var boolean	 */
	var $Kickback;


	/** 
	 * 
	 * @var string	 */
	var $SitePhone;


	/** 
	 * 
	 * @var string	 */
	var $Addedby;


	/** 
	 * 
	 * @var string	 */
	var $Email;


	/** 
	 * 
	 * @var string	 */
	var $Phone;


	/** 
	 * 
	 * @var boolean	 */
	var $Lead;


	/** 
	 * 
	 * @var boolean	 */
	var $Assist;


	/** 
	 * 
	 * @var string	 */
	var $BU1FLSID;


	/** 
	 * 
	 * @var string	 */
	var $BU1Info;


	/** 
	 * 
	 * @var string	 */
	var $BU2FLSID;


	/** 
	 * 
	 * @var string	 */
	var $BU2Info;


	/** 
	 * 
	 * @var string	 */
	var $ASST;


	/** 
	 * 
	 * @var string	 */
	var $LastModDateTime;


	/** 
	 * 
	 * @var string	 */
	var $LastModBy;


	/** 
	 * 
	 * @var boolean	 */
	var $UpdateRequested;


	/** 
	 * 
	 * @var string	 */
	var $WhoRequested;


	/** 
	 * 
	 * @var string	 */
	var $ReasonRequested;


	/** 
	 * 
	 * @var string	 */
	var $FileAttachment;


	/** 
	 * 
	 * @var boolean	 */
	var $FaxReceived;


	/** 
	 * 
	 * @var string	 */
	var $RecruitedBy;


	/** 
	 * 
	 * @var string	 */
	var $SourceDate;


	/** 
	 * 
	 * @var boolean	 */
	var $TechCalled;


	/** 
	 * 
	 * @var string	 */
	var $DateTimeCalled;


	/** 
	 * 
	 * @var string	 */
	var $CalledBy;


	/** 
	 * 
	 * @var string	 */
	var $TechFName;


	/** 
	 * 
	 * @var string	 */
	var $TechLName;


	/** 
	 * 
	 * @var boolean	 */
	var $ShortNotice;


	/** 
	 * 
	 * @var boolean	 */
	var $IncompleteFax;


	/** 
	 * 
	 * @var string	 */
	var $TB_ID;


	/** 
	 * 
	 * @var int	 */
	var $Qty_Applicants;


	/** 
	 * 
	 * @var string	 */
	var $PayApprovedBy;


	/** 
	 * 
	 * @var int	 */
	var $NoShow_Tech;


	/** 
	 * 
	 * @var boolean	 */
	var $SignOffNotNeeded;


	/** 
	 * 
	 * @var string	 */
	var $RecruitedTime;


	/** 
	 * 
	 * @var boolean	 */
	var $TechCheckedIn_48hrs;


	/** 
	 * 
	 * @var boolean	 */
	var $WorkOrderReviewed;


	/** 
	 * 
	 * @var string	 */
	var $Date_Assigned;


	/** 
	 * 
	 * @var int	 */
	var $Project_ID;


	/** 
	 * 
	 * @var string	 */
	var $TechPhone;


	/** 
	 * 
	 * @var boolean	 */
	var $DeclinedWorkOrder;


	/** 
	 * 
	 * @var string	 */
	var $CallManagementArea;


	/** 
	 * 
	 * @var string	 */
	var $Checked_in_By;


	/** 
	 * 
	 * @var string	 */
	var $Import_File_Name;


	/** 
	 * 
	 * @var int	 */
	var $BackOut_Tech;


	/** 
	 * 
	 * @var string	 */
	var $Date_Checked_in;


	/** 
	 * 
	 * @var float	 */
	var $Duration;


	/** 
	 * 
	 * @var float	 */
	var $PayAmount_Final;


	/** 
	 * 
	 * @var string	 */
	var $Penalty_Type;


	/** 
	 * 
	 * @var string	 */
	var $AttachedFile;


	/** 
	 * 
	 * @var boolean	 */
	var $PreCall_2;


	/** 
	 * 
	 * @var string	 */
	var $CalledBy_2;


	/** 
	 * 
	 * @var string	 */
	var $DateTimeCalled_2;


	/** 
	 * 
	 * @var boolean	 */
	var $OOS;


	/** 
	 * 
	 * @var string	 */
	var $ExtraTime;


	/** 
	 * 
	 * @var boolean	 */
	var $Unexpected_Steps;


	/** 
	 * 
	 * @var string	 */
	var $Unexpected_Desc;


	/** 
	 * 
	 * @var string	 */
	var $AskedBy;


	/** 
	 * 
	 * @var string	 */
	var $AskedBy_Name;


	/** 
	 * 
	 * @var float	 */
	var $CalculatedDuration;

}


$wo = new API_FlsWorkOrder();
//$wo->WorkOrderID = 141374;
$wo->WorkOrderNo = "Test049430";
$wo->FLSComments = "testAutomatic SR Notes.Duplicate Locations exists in Install Base.Please capture the correct location.

 

Dummy Serial Number exists in Install Base.Please capture the correct Serial Number..Test.XEROX 8860 PrinterREG1OTHER";
$wo->Duration = "2.0";
//$wo->PayAmt = 2.00;
var_dump($wo);
//$workOrders = $client->updateWorkOrder("codem01", "cLutch44", 140931, $result);
$workOrders = $client->createWorkOrder("codem01", "cLutch44", $wo);
//$workOrders = $client->createWorkOrder("codem01", "cLutch44", $wo);

//$workOrders = $client->updateWorkOrder("codem01", "cLutch44", $wo);
//$workOrders = $client->getAllProjects("codem01", "cLutch44");

$filter = array();
$filter["Addedby"] = "codem01";
//$workOrders = $client->getWorkOrder("codem01", "cLutch44", 141374);

echo "<pre>";
var_dump($workOrders);