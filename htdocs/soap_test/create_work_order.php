<?php
/**
 * API for FieldSolutions.com web services
 */
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'On');

require_once('includes/settings.php');
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('soap.wsdl_cache_ttl', '0');



// incomming data
$fields = array();
$fields[1]['name']    = 'WO_ID';
$fields[1]['value']   = '2009-08-17_test';

$fields[2]['name']    = 'Project_ID';
$fields[2]['value']   = '534';

$fields[3]['name']    = 'Approved';
$fields[3]['value']   = '0';

$fields[4]['name']    = 'Headline';
$fields[4]['value']   = '';



// create SOAP client
$client = new SoapClient(WSDL_URL, array('cache_wsdl'=>WSDL_CACHE_NONE));



// create XML file contains WO data
$dom = new DOMDocument("1.0");
$root = $dom->createElement('wo');
foreach ($fields as $key=>$field) {
    $name = $dom->createAttribute('fieldName');
    $name->appendChild($dom->createTextNode($field['name']));

    $value = $dom->createAttribute('fieldValue');
    $value->appendChild($dom->createTextNode($field['value']));

    $parameter = $dom->createElement('parameter');
    $parameter->appendChild($name);
    $parameter->appendChild($value);

    $root->appendChild($parameter);
}
$dom->appendChild($root);
$result = $dom->saveXML();



$params['url'] = $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
$params['xml'] = $result;



$workOrders = $client->createWorkOrder($userData, $params);



echo "<pre>";
var_dump($workOrders);