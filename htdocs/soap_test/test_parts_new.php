<?php
/**
 * API for FieldSolutions.com web services
 */
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'On');

require_once('includes/settings.php');
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('soap.wsdl_cache_ttl', '0');

$fields = array();

/**
 * class API_Part
 * @package API_Part
 */

abstract class API_Part {
	/** 
	 * 
	 * @var string	 */
	var $PartNum;


	/** 
	 * 
	 * @var string	 */
	var $ModelNum;


	/** 
	 * 
	 * @var string	 */
	var $SerialNum;


	/** 
	 * 
	 * @var string	 */
	var $Description;


	/** 
	 * 
	 * @var string	 */
	var $Carrier;


	/** 
	 * 
	 * @var string	 */
	var $TrackingNum;


	/** 
	 * 
	 * @var string	 */
	var $ETA;
}

class API_PartEntry {
	
	/** 
	 * 
	 * @var int	 */
	var $id;

	/** 
	 * 
	 * @var boolean	 */
	var $Consumable;	

	/** 
	 * array of API_Part
	 * @var API_NewPart	 */
	var $newPart;
	
	/** 
	 * array of API_Part
	 * @var API_ReturnPart	 */
	var $returnPart;
}

class API_NewPart {
	/** 
	 * 
	 * @var string	 */
	var $ShipTo;


	/** 
	 * 
	 * @var string	 */
	var $ShippingAddress;

	/** 
	 * 
	 * @var string	 */
	var $PartNum;


	/** 
	 * 
	 * @var string	 */
	var $ModelNum;


	/** 
	 * 
	 * @var string	 */
	var $SerialNum;


	/** 
	 * 
	 * @var string	 */
	var $Description;


	/** 
	 * 
	 * @var string	 */
	var $Carrier;


	/** 
	 * 
	 * @var string	 */
	var $TrackingNum;


	/** 
	 * 
	 * @var string	 */
	var $ETA;
}

/**
 * class API_ReturnPart
 * @package API_Part
 * @subpackage API_ReturnPart
 */
class API_ReturnPart {
	/** 
	 * 
	 * @var string	 */
	var $RMA;


	/** 
	 * 
	 * @var string	 */
	var $Instructions;

	/** 
	 * 
	 * @var string	 */
	var $PartNum;


	/** 
	 * 
	 * @var string	 */
	var $ModelNum;


	/** 
	 * 
	 * @var string	 */
	var $SerialNum;


	/** 
	 * 
	 * @var string	 */
	var $Description;


	/** 
	 * 
	 * @var string	 */
	var $Carrier;


	/** 
	 * 
	 * @var string	 */
	var $TrackingNum;


	/** 
	 * 
	 * @var string	 */
	var $ETA;
	
}

// create SOAP client
$client = new SoapClient(WSDL_URL, array('cache_wsdl'=>WSDL_CACHE_NONE));

/*$newPartEntry = new API_PartEntry();
$newPart = new API_NewPart();
$returnPart = new API_ReturnPart();

$newPart->PartNum = "PN1234New";
$returnPart->PartNum = "PN1234Return";

$newPartEntry->newPart = $newPart;
$newPartEntry->returnPart = $returnPart;
var_dump($newPartEntry);*/

//print_r(new SoapVar($newPartEntry, SOAP_ENC_OBJECT, "API_PartEntry"));

$filter = array();
//$workOrders = $client->createPartEntry("codem01", "cLutch44", "7103", $newPartEntry);
//$workOrders = $client->getPartEntry("fls-api", "FlsApi09$", "83476", 5030);
$workOrders = $client->getParts("fls-api", "FlsApi09$", "83476");
$tf = microtime();

echo "dsddsDHDH: $tf - $t0 = " . $tf - $t0 . " ms<br/>";
//$workOrders = $client->updateWorkOrder("core-api", "Scrimsha", 7103, $result); //58054

echo "<pre>";
var_dump($workOrders);