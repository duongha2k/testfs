<?php
require_once("../library/caspioAPI.php");

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'On');

require_once('includes/settings.php');
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('soap.wsdl_cache_ttl', '0');

$fields = array();

// create SOAP client
$client = new SoapClient('https://api.fieldsolutions.com/api/client/wsdl/5', array('cache_wsdl'=>WSDL_CACHE_NONE, 'trace' => 1, 'classmap' => array("API_WorkOrderFilter"=>"API_WorkOrderFilter", "API_TechWorkOrder" => "API_TechWorkOrder", "API_Timestamp" => "API_Timestamp", "API_WorkOrder" => "API_WorkOrder")));

$t0 = microtime();

$wo = new API_WorkOrder();
$wo->CheckedIn = true;
$wo->Headline = "Test do not bid 2";
//$client = new Core_Api_Class;

//$z = $client->getBids('codem01','CLutch44v3', 286583, 0, 0, false, array());
$filters = new API_WorkOrderFilter();
$filters->WO_ID = 'custom';
$z = $client->getWorkOrders('codem01', 'CLutch44v2', 'WIN_NUM', NULL, NULL, NULL, $filters);
//$z = $client->getWorkOrders('codem01', 'CLutch44v2', 224779);

//$z = $client->getBids('codem01','CLutch44v3', 286583);
//$z = $client->getTechFileLinks('codem01', 'CLutch44v3', 326740);
//$z = $client->setWOVisitDisabled('codem01', 'CLutch44v3', 5733, 1);
//$z = $client->createWOVisit('codem01', 'CLutch44v3', 224779, array('StartDate' => '05/21/2013', 'TechArrivalInstructions' => 'test api update'));
//$z = $client->updateWOVisit('codem01', 'CLutch44v3', 55735, array('TechArrivalInstructions' => 'test api update 2'));
//var_dump($z);die();
//var_dump($z->data[0]->__Visits);die();

//echo $client->__getLastRequest();
echo $client->__getLastResponse();
die();
