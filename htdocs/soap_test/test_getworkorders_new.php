<?php
session_start();
class API_Timestamp {
//	private $fieldsList = "DateTime_Stamp,WO_UNID,Username,Description,Client_Name,Project_Name,Customer_Name,TechID,Tech_Name,Company_ID";
/*	private static $fieldMap = array(
				"DateTime_Stamp" => "dateTime",
				"WO_UNID" => "winNum",
				"Username" => "username",
				"Description" => "description",
				"Client_Name" => "clientName",
				"Project_Name" => "projectName",
				"Customer_Name" => "customerName",
				"TechID" => "techId",
				"Tech_Name" => "techName",
				"Company_ID" => "companyId"
				);*/

	/**
	 *
	 * @var string	 */
	public $dateTime;

	/**
	 *
	 * @var int	 */
	public $winNum;

	/**
	 *
	 * @var string	 */
	public $username;

	/**
	 *
	 * @var string	 */
	public $description;

	/**
	 *
	 * @var string	 */
	public $clientName;

	/**
	 *
	 * @var string	 */
	public $projectName;

	/**
	 *
	 * @var string	 */
	public $customerName;

	/**
	 *
	 * @var int	 */
	public $techId;

	/**
	 *
	 * @var int	 */
	public $techName;

	/**
	 *
	 * @var string	 */
	public $companyId;

	private function fieldList() {
		return array_keys(self::$fieldMap);
	}

	public static function getTimestamps($winNum, $companyId, $sort = "") {
		$winNum = Core_Caspio::caspioEscape($winNum);
		$companyId = Core_Caspio::caspioEscape($companyId);
		$list = Core_Caspio::caspioSelectAdv(TABLE_WORK_ORDERS, self::fieldList(), "WO_UNID = '$winNum' AND Company_ID = '$companyId'", $sort);
		$tsList = array();
		if (empty($list[0])) return $tsList;
		foreach ($list as $ts) {
			$tsObj = new API_Timestamp();
			foreach ($fieldMap as $dbField=>$classField) {
				$tsObj->$classField = $ts[$dbField];
			}
			$tsList[] = $tsObj;
		}
		return $tsList;
	}
}
class API_String {
	/**
	 *
	 * @var string	 */
	var $value;
}
class API_UpdateTimeStampFilter {
	/**
	 *
	 * @var boolean	 */
	var $matchAll;

	/**
	 *
	 * @var API_String[]	 */
	var $fields;
}

class API_WorkOrderFilter {
    /**
     * TB_UNID
     *
     * @var int
     * @access public
     */
    var $TB_UNID;
    /**
     * Tech
     *
     * @var string
     * Example:
     *      integer Tech ID
     *      "FirstName"
     *      "Lastname"
     *      "Firstname Lastname"
     * @access public
     */
    var $Tech;
    /**
     * Call_Type
     *
     * @var integer
     * @access public
     */
    var $Call_Type;
    /**
     * Parts_Presents
     *
     * @var boolean
     * @access public
     */
    var $Parts_Presents = false;
    /**
     * Docs_Presents
     *
     * @var boolean
     * @access public
     */
    var $Docs_Presents = false;
	/**
	 *
	 * @var int	 */
	var $Project_ID;

	/**
	 *
	 * @var string	 */
	var $StartDateFrom;

	/**
	 *
	 * @var string	 */
	var $StartDateTo;

	/**
	 *
	 * @var string	 */
	var $EndDateFrom;

	/**
	 *
	 * @var string	 */
	var $EndDateTo;

	/**
	 *
	 * @var string	 */
	var $WO_ID;

	/**
	 *
	 * @var string	 */
	var $PO;

	/**
	 *
	 * @var boolean	 */
	var $ShowTechs;

	/**
	 *
	 * @var string	 */
	var $Region;

	/**
	 *
	 * @var string	 */
	var $SiteName;

	/**
	 *
	 * @var string	 */
	var $SiteNumber;

	/**
	 *
	 * @var string	 */
	var $City;

	/**
	 *
	 * @var string	 */
	var $State;

	/**
	 *
	 * @var string	 */
	var $Zipcode;

	/**
	 *
	 * @var boolean	 */
	var $Update_Requested;

	/**
	 *
	 * @var boolean	 */
	var $CheckedIn;

	/**
	 *
	 * @var boolean	 */
	var $WorkOrderReviewed;

	/**
	 *
	 * @var boolean	 */
	var $TechCheckedIn_24hrs;

	/**
	 *
	 * @var boolean	 */
	var $TechMarkedComplete;

	/**
	 *
	 * @var boolean	 */
	var $Site_Complete;

	/**
	 *
	 * @var boolean	 */
	var $Approved;

	/**
	 *
	 * @var boolean	 */
	var $Additional_Pay_Amount;

	/**
	 *
	 * @var boolean	 */
	var $Work_Out_of_Scope;

	/**
	 *
	 * @var boolean	 */
	var $TechPaid;

	/**
	 *
	 * @var string	 */
	var $Tech_FName;

	/**
	 *
	 * @var string	 */
	var $Tech_LName;

	/**
	 *
	 * @var int	 */
	var $Tech_ID;

	/**
	 *
	 * @var int	 */
	var $Sourced;

	/**
	 *
	 * @var boolean	 */
	var $Deactivated;

	/**
	 *
	 * @var API_UpdateTimeStampsFilter	 */
	var $UpdateTS;

	var $WO_State;
	var $Distance;
	var $Company_ID;
	var $StoreNotified;
	var $Paperwork_Received;
	var $Lead;
	var $Assist;
	var $FLS_ID;
	var $Short_Notice;
	/**
	 *
	 * @var string	 */
	var $DatePaidFrom;

	/**
	 *
	 * @var string	 */
	var $DatePaidTo;

}


/**
 * API for FieldSolutions.com web services
 */
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'On');

require_once('includes/settings.php');
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('soap.wsdl_cache_ttl', '0');

$fields = array();

//$fields[1]['name']    = 'Tech_ID';
//$fields[1]['value']   = '19806';

$fields[1]['name']    = 'Description';
$fields[1]['value']   = 'Test';

//$fields[2]['name']    = 'TechMarkedComplete';
//$fields[2]['value']   =











'False';

/*$fields[2]['name']    = 'AbortFeeAmount';
$fields[2]['value']   = '15.00';

$fields[3]['name']    = 'Approved';
$fields[3]['value']   = 'True';*/

//$fields[2]['name']    = 'PayMax';
//$fields[2]['value']   = '1.00';

/*$fields[3]['name']    = 'Amount_Per';
$fields[3]['value']   = 'Site';*/


class API_WorkOrder {
	/**
	 *
	 * @var int	 */
	var $WIN_NUM;


	/**
	 *
	 * @var string	 */
	var $WO_ID;


	/**
	 *
	 * @var string	 */
	var $DateEntered;


	/**
	 *
	 * @var string	 */
	var $StartDate;


	/**
	 *
	 * @var string	 */
	var $EndDate;


	/**
	 *
	 * @var string	 */
	var $Description;


	/**
	 *
	 * @var string	 */
	var $Requirements;


	/**
	 *
	 * @var string	 */
	var $Tools;


	/**
	 *
	 * @var string	 */
	var $PayMax;


	/**
	 *
	 * @var string	 */
	var $Address;


	/**
	 *
	 * @var string	 */
	var $City;


	/**
	 *
	 * @var string	 */
	var $State;


	/**
	 *
	 * @var string	 */
	var $Zipcode;


	/**
	 *
	 * @var string	 */
	var $Tech_FName;


	/**
	 *
	 * @var boolean	 */
	var $Approved;


	/**
	 *
	 * @var string	 */
	var $SpecialInstructions;


	/**
	 *
	 * @var string	 */
	var $SiteName;


	/**
	 *
	 * @var string	 */
	var $ClientComments;


	/**
	 *
	 * @var boolean	 */
	var $ShowTechs;


	/**
	 *
	 * @var int	 */
	var $Tech_ID;


	/**
	 *
	 * @var boolean	 */
	var $TechMarkedComplete;


	/**
	 *
	 * @var string	 */
	var $TechComments;


	/**
	 *
	 * @var string	 */
	var $MissingComments;


	/**
	 *
	 * @var string	 */
	var $Tech_LName;


	/**
	 *
	 * @var boolean	 */
	var $TechPaid;


	/**
	 *
	 * @var string	 */
	var $DatePaid;


	/**
	 *
	 * @var float	 */
	var $PayAmount;


	/**
	 *
	 * @var string	 */
	var $SiteNumber;


	/**
	 *
	 * @var string	 */
	var $Region;


	/**
	 *
	 * @var boolean	 */
	var $CheckedIn;


	/**
	 *
	 * @var string	 */
	var $Checkedby;


	/**
	 *
	 * @var string	 */
	var $CheckInNotes;


	/**
	 *
	 * @var string	 */
	var $StartTime;


	/**
	 *
	 * @var boolean	 */
	var $StoreNotified;


	/**
	 *
	 * @var string	 */
	var $Notifiedby;


	/**
	 *
	 * @var string	 */
	var $DateNotified;


	/**
	 *
	 * @var string	 */
	var $NotifyNotes;


	/**
	 *
	 * @var string	 */
	var $SitePhone;


	/**
	 *
	 * @var string	 */
	var $SiteEmail;


	/**
	 *
	 * @var string	 */
	var $SiteFax;


	/**
	 *
	 * @var string	 */
	var $PO;


	/**
	 *
	 * @var string	 */
	var $General1;


	/**
	 *
	 * @var string	 */
	var $General2;


	/**
	 *
	 * @var string	 */
	var $General3;


	/**
	 *
	 * @var string	 */
	var $General4;


	/**
	 *
	 * @var string	 */
	var $General5;


	/**
	 *
	 * @var string	 */
	var $General6;


	/**
	 *
	 * @var string	 */
	var $General7;


	/**
	 *
	 * @var string	 */
	var $General8;


	/**
	 *
	 * @var string	 */
	var $General9;


	/**
	 *
	 * @var string	 */
	var $General10;


	/**
	 *
	 * @var int	 */
	var $Qty_Applicants;


	/**
	 *
	 * @var boolean	 */
	var $Invoiced;


	/**
	 *
	 * @var string	 */
	var $DateInvoiced;


	/**
	 *
	 * @var string	 */
	var $DateApproved;


	/**
	 *
	 * @var string	 */
	var $Ship_Contact_Info;


	/**
	 *
	 * @var boolean	 */
	var $WorkOrderReviewed;


	/**
	 *
	 * @var boolean	 */
	var $Deactivated;


	/**
	 *
	 * @var boolean	 */
	var $TechCheckedIn_24hrs;


	/**
	 *
	 * @var boolean	 */
	var $TechCalled;


	/**
	 *
	 * @var string	 */
	var $CalledBy;


	/**
	 *
	 * @var string	 */
	var $CallDate;


	/**
	 *
	 * @var int	 */
	var $NoShow_Tech;


	/**
	 *
	 * @var string	 */
	var $Project_Name;


	/**
	 *
	 * @var int	 */
	var $Project_ID;


	/**
	 *
	 * @var string	 */
	var $Time_In;


	/**
	 *
	 * @var string	 */
	var $Time_Out;


	/**
	 *
	 * @var string	 */
	var $TechEmail;


	/**
	 *
	 * @var string	 */
	var $TechPhone;


	/**
	 *
	 * @var string	 */
	var $Date_Assigned;


	/**
	 *
	 * @var boolean	 */
	var $Update_Requested;


	/**
	 *
	 * @var string	 */
	var $Update_Reason;


	/**
	 *

	 * @var float	 */
	var $Additional_Pay_Amount;


	/**
	 *
	 * @var string	 */
	var $Add_Pay_AuthBy;


	/**
	 *
	 * @var boolean	 */
	var $Work_Out_of_Scope;


	/**
	 *
	 * @var string	 */
	var $OutOfScope_Reason;


	/**
	 *
	 * @var boolean	 */
	var $Site_Complete;


	/**
	 *
	 * @var string	 */
	var $Add_Pay_Reason;


	/**
	 *
	 * @var int	 */
	var $BackOut_Tech;


	/**
	 *
	 * @var string	 */
	var $EndTime;


	/**
	 *
	 * @var string	 */
	var $lastModBy;


	/**
	 *
	 * @var string	 */
	var $lastModDate;


	/**
	 *
	 * @var string	 */
	var $approvedBy;


	/**
	 *
	 * @var string	 */
	var $Date_Completed;


	/**
	 *
	 * @var string	 */
	var $Deactivated_Reason;


	/**
	 *
	 * @var string	 */
	var $Amount_Per;


	/**
	 *
	 * @var float	 */
	var $OutofScope_Amount;


	/**
	 *
	 * @var int	 */
	var $Qty_Devices;


	/**
	 *
	 * @var string	 */
	var $Site_Contact_Name;


	/**
	 *
	 * @var string	 */
	var $StartRange;


	/**
	 *
	 * @var string	 */
	var $Duration;


	/**
	 *
	 * @var int	 */
	var $Type_ID;


	/**
	 *
	 * @var string	 */
	var $DeactivatedDate;


	/**
	 *
	 * @var string	 */
	var $DeactivatedBy;


	/**
	 *
	 * @var int	 */
	var $Latitude;


	/**
	 *
	 * @var int	 */
	var $Longitude;


	/**
	 *
	 * @var string	 */
	var $Shipped_By;


	/**
	 *
	 * @var string	 */
	var $Part_No;


	/**
	 *
	 * @var string	 */
	var $Part_Desciption;


	/**
	 *
	 * @var string	 */
	var $RMA_No;


	/**
	 *
	 * @var string	 */
	var $Tracking_No;


	/**
	 *
	 * @var string	 */
	var $Part_ETA;


	/**
	 *
	 * @var string	 */
	var $Return_Part_Instructions;


	/**
	 *
	 * @var float	 */
	var $Total_Estimated_WO_Value;


	/**
	 *
	 * @var float	 */
	var $Tech_Bid_Amount;


	/**
	 *
	 * @var boolean	 */
	var $AutoBlastOnPublish;


	/**
	 *
	 * @var string	 */
	var $RecruitmentFromEmail;


	/**
	 *
	 * @var float	 */
	var $calculatedTechHrs;


	/**
	 *
	 * @var float	 */
	var $baseTechPay;


	/**
	 *
	 * @var float	 */
	var $TripCharge;


	/**
	 *
	 * @var float	 */
	var $MileageReimbursement;


	/**
	 *
	 * @var float	 */
	var $MaterialsReimbursement;


	/**
	 *
	 * @var string	 */
	var $TechnicianFeeComments;


	/**
	 *
	 * @var boolean	 */
	var $ExpediteFee;


	/**
	 *
	 * @var boolean	 */
	var $AbortFee;


	/**
	 *
	 * @var boolean	 */
	var $AfterHoursFee;


	/**
	 *
	 * @var float	 */
	var $OtherFSFee;


	/**
	 *
	 * @var int	 */
	var $TechMiles;


	/**
	 *
	 * @var float	 */
	var $AbortFeeAmount;


	/**
	 *
	 * @var string	 */
	var $WO_Category;


	/**
	 *
	 * @var int	 */
	var $WO_Category_ID;


	/**
	 *
	 * @var string	 */
	var $Headline;


	/**
	 *
	 * @var int	 */
	var $TechPhoneExt;


	/**
	 *
	 * @var boolean	 */
	var $ReminderAcceptance;


	/**
	 *
	 * @var boolean	 */
	var $Reminder24Hr;


	/**
	 *
	 * @var boolean	 */
	var $Reminder1Hr;


	/**
	 *
	 * @var boolean	 */
	var $CheckInCall;


	/**
	 *
	 * @var boolean	 */
	var $ReminderNotMarkComplete;


	/**
	 *
	 * @var boolean	 */
	var $ReminderIncomplete;


	/**
	 *
	 * @var string	 */
	var $DateIncomplete;


	/**
	 *
	 * @var string	 */
	var $ProjectManagerName;


	/**
	 *
	 * @var string	 */
	var $ProjectManagerPhone;


	/**
	 *
	 * @var string	 */
	var $ProjectManagerEmail;


	/**
	 *
	 * @var string	 */
	var $ResourceCoordinatorName;


	/**
	 *
	 * @var string	 */
	var $ResourceCoordinatorPhone;


	/**
	 *
	 * @var string	 */
	var $ResourceCoordinatorEmail;


	/**
	 *
	 * @var string	 */
	var $EmergencyName;


	/**
	 *
	 * @var string	 */
	var $EmergencyPhone;


	/**
	 *
	 * @var string	 */
	var $EmergencyEmail;


	/**
	 *
	 * @var string	 */
	var $TechnicalSupportName;


	/**
	 *
	 * @var string	 */
	var $TechnicalSupportPhone;


	/**
	 *
	 * @var string	 */
	var $TechnicalSupportEmail;


	/**
	 *
	 * @var string	 */
	var $CheckInOutName;


	/**
	 *
	 * @var string	 */
	var $CheckInOutNumber;


	/**
	 *
	 * @var string	 */
	var $CheckInOutEmail;


	/**
	 *
	 * @var boolean	 */
	var $isProjectAutoAssign;


	/**
	 *
	 * @var boolean	 */
	var $isWorkOrdersFirstBidder;


	/**
	 *
	 * @var int	 */
	var $MinutesRemainPublished;


	/**
	 *
	 * @var int	 */
	var $MinimumSelfRating;


	/**
	 *
	 * @var int	 */
	var $MaximumAllowableDistance;


	/**
	 *
	 * @var boolean	 */
	var $ReminderAll;


	/**
	 *
	 * @var boolean	 */
	var $CheckOutCall;


	/**
	 *
	 * @var boolean	 */
	var $ACSInvoiced;


	/**
	 *
	 * @var string	 */
	var $ACSDateInvoiced;


	/**
	 *
	 * @var boolean	 */
	var $SMSBlast;


	/**
	 *
	 * @var boolean	 */
	var $P2TPreferredOnly;


	/**
	 *
	 * @var boolean	 */
	var $P2TAlreadyAssigned;


	/**
	 *
	 * @var string	 */
	var $Date_In;


	/**
	 *
	 * @var string	 */
	var $Date_Out;


	/**
	 *
	 * @var boolean	 */
	var $CallClosed;


	/**
	 *
	 * @var boolean	 */
	var $ContactHD;


	/**
	 *
	 * @var string	 */
	var $HDPOC;


	/**
	 *
	 * @var boolean	 */
	var $Lead;


	/**
	 *
	 * @var boolean	 */
	var $Assist;


	/**
	 *
	 * @var boolean	 */
	var $SignOffSheet_Required;


	/**
	 *
	 * @var string	 */
	var $Date_Checked_in;


	/**
	 *
	 * @var string	 */
	var $ExtraTime;


	/**
	 *
	 * @var boolean	 */
	var $Unexpected_Steps;


	/**
	 *
	 * @var boolean	 */
	var $Incomplete_Paperwork;


	/**
	 *
	 * @var boolean	 */
	var $Paperwork_Received;


	/**
	 *
	 * @var string	 */
	var $DeactivationCode;

	var $Pic1_FromTech;
	var $Pic2_FromTech;
	var $Pic3_FromTech;
	var $ReturnPartCount;
	var $NewPartCount;

	/**
	 *
	 * @var string	 */
	var $Status;
	var $ShortNotice;

/**
	 *
	 * @var boolean	 */
	var $PreCall_1;
	/**
	 *
	 * @var boolean	 */
	var $PreCall_2;
	/**
	 *
	 * @var boolean	 */
	var $FLS_OOS;
	/**
	 *
	 * @var string	 */
	var $Unexpected_Desc;
	/**
	 *
	 * @var string	 */
	var $AskedBy;
	/**
	 *
	 * @var string	 */
	var $AskedBy_Name;
	/**
	 *
	 * @var string	 */
	var $BU1FLSID;
	/**
	 * TODO What field in DB referrenced to?
	 * @var string	 */
	var $BU1Info;
	/**
	 * TODO What field in DB referrenced to?
	 * @var string	 */
	var $BU2FLSID;
	/**
	 *
	 * @var string	 */
	var $BU2Info;
	/**
	 *
	 * @var string	 */
	var $FLS_ID;
	/**
	 *
	 * @var string	 */
	var $SourceByDate;

    /**
     * PartsProcessed
     *
     * @var int
     * @access public
     */
    var $PartsProcessed;


}
/**
 * API for FieldSolutions.com web services
 */
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'On');

require_once('includes/settings.php');
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('soap.wsdl_cache_ttl', '0');

$fields = array();

//$fields[1]['name']    = 'Tech_ID';
//$fields[1]['value']   = '19806';

$fields[1]['name']    = 'Description';
$fields[1]['value']   = 'Test';

//$fields[2]['name']    = 'TechMarkedComplete';
//$fields[2]['value']  'False';

/*$fields[2]['name']    = 'AbortFeeAmount';
$fields[2]['value']   = '15.00';

$fields[3]['name']    = 'Approved';
$fields[3]['value']   = 'True';*/

//$fields[2]['name']    = 'PayMax';
//$fields[2]['value']   = '1.00';

/*$fields[3]['name']    = 'Amount_Per';
$fields[3]['value']   = 'Site';*/


// create SOAP client
$client = new SoapClient(WSDL_URL, array('cache_wsdl'=>WSDL_CACHE_NONE, 'trace' => 1, 'classmap' => array("API_WorkOrderFilter"=>"API_WorkOrderFilter", "API_WorkOrder" => "API_WorkOrder", "API_Timestamp" => "API_Timestamp")));



// create XML file contains WO data
/*$dom = new DOMDocument("1.0");
$root = $dom->createElement('WO');
foreach ($fields as $key=>$field) {
    $parameter = $dom->createElement($field['name'], $field['value']);
    $root->appendChild($parameter);
}
$dom->appendChild($root);
$result = $dom->saveXML();



$params['url'] = $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
$params['xml'] = $result;*/

$t0 = microtime();

//$workOrders = $client->updateWorkOrder("codem01", "cLutch44", 7103, $result);


$filter = new API_WorkOrderFilter();

//$filter->StartDateFrom = "07/1/2010";
//$filter->StartDateTo = "07/1/2010";
//$filter->State = "TX";
//$filter->Project_ID = 38;

//$filter->WO_State = "work done";
//$filter->UpdateTS = NULL;

//$filter->Tech_ID = 9999984;

//var_dump($filter);

//$filter->WO_ID = "APITest62";

//$filter->Company_ID = "CBD";

//print_r(new SoapParam($filter, "API_WorkOrderFilter"));

$count = 0;
//$workOrders = $client->getWorkOrders("codem01|pmContext=FLS", "CLutch44", "", "", "", $filter, 0, 10);

//$workOrders = $client->getWorkOrders("codem01", "CLutch44", "", "", "", "", $filter, 0, 10);

$workOrders = $client->deactivateWorkOrder("codem01", "CLutch44", 73181, 1, "Work order kicked back");

//$workOrders = $client->getWinNum("codem01", "cLutch24", "ApiTest62");
//$workOrders = $client->getTimestampsWithWinNum("codem01", "cLutch24", 7103, 0, 0, "");


/*print "<pre>";
foreach ( $workOrders->data as $k => $wo) {
    print "{$wo->StartDate}\n";
}
print "</pre>";exit;*/
/*$_SESSION["SoapSession"] = $client->_cookies[session_name()][0];
print_r($client->_cookies[session_name()][0]);
echo "<br/>";

die();*/

$wo = new API_WorkOrder();
$wo->WO_ID = "APITest62";
//$wo->WO_ID = "APICreateTest8262010-" . time();
$wo->Headline = "Test do not bid";
$wo->WO_Category_ID = 9;
/*$wo->StartDate = '08/03/2010';
$wo->EndDate = '08/03/2010';
$wo->City = "Henderson";
$wo->State = "MT";*/
$wo->Zipcode = "59866";
/*$wo->StartRange = "7-10 AM";
$wo->Duration = "1 hr";
$wo->StartTime = "7:00 AM";
$wo->EndTime = "8:00 AM";*/
$wo->StartDate = "10/3/2010";
//$wo->Tech_Bid_Amount = 10.00;
$wo->Zipcode = 59866;
$wo->PayMax  = 20.00;
$wo->Amount_Per = "Site";
//$wo->Headline = "Test do not bid";
//$wo->WO_Category_ID = 13;
$wo->Type_ID = 1;
$wo->Tech_ID = 19806;
//$wo->Status = "completed";

$wo->Project_ID = 38;

/*$wo->TripCharge = 120.00;
$wo->MileageCharge = 36.00;
$wo->MaterialsCharge = 56.00;
$wo->AbortFeeAmount = 0.00;
$wo->Additional_Pay_Amount = 29.21;*/
//$wo->Approved = false;
//$wo->Project_Name = "Skynet";

//$workOrders = $client->updateWorkOrder("codem01", "cLutch24", 80938, $wo);

//$filter = new API_WorkOrderFilter;
//$filter->WO_State = "work done";

$workOrders = $client->createWorkOrder("codem01", "CLutch44", true, $wo);
//$workOrders = $client->getWorkOrder("codem01", "cLutch24", 1);

//$workOrders = $client->getTimestampsWithWinNum("codem01", "cLutch24", 1, 0, 0, "");

//$workOrders = $client->updateWorkOrder("codem01", "cLutch24", 1, $wo);



//$workOrders = $client->getWOCategories("codem01", "CLutch44");

//$workOrders = $client->getWorkOrders("codem01|pmContext=HXC", "cLutch24", "", "", "", "", $filter, 0, 100, "", "");
//$workOrders = $client->getWorkOrder("codem01|pmContext=suss", "cLutch24", 222910);

//$workOrders = $client->assignWorkOrder("miriam.block", "warecorp", 222910, 19806, 23.50);

//$workOrders = $client->approveWorkOrder("miriam.block", "warecorp", 222910, $result);

//$workOrders = $client->approveWorkOrder("codem01", "cLutch24", 48090, $result);
//$workOrders = $client->assignWorkOrder("codem01", "CLutch44", 234720, 9999984, 10);

//$workOrders = $client->getTimestampsWithWinNum("codem01", "cLutch24", 7103, 0, 20, "DateTime_Stamp");

//$workOrders = $client->test();

//$workOrders = $client->techLookup("codem01", "CLutch24", 9999984);
//$workOrders = $client->getWorkOrder("codem01", "cLutch24", 66389);

//$workOrders = $client->getWorkOrders("codem01", "cLutch24", '', "", "", "", $filter, 0, 4, "", "Status ASC");

/*$wo = new API_WorkOrder();
$wo->Tech_ID = 19806;
$wo->Tech_Bid_Amount = 900.00;*/

//$workOrders = $client->updateWorkOrder("codem01", "cLutch24", 66389, $wo);

//$workOrders = $client->getWorkOrder("codem01|pmContext=FLS", "cLutch44", 206523);

header("content-type: text/xml");
//echo $client->__getLastRequest();
echo $client->__getLastResponse();
/*die();

$tf = microtime();

echo "DHDH: $tf - $t0 = " . $tf - $t0 . " ms<br/>";
//$workOrders = $client->updateWorkOrder("core-api", "Scrimsha", 7103, $result); //58054

echo "<pre>";
var_dump($workOrders);*/
