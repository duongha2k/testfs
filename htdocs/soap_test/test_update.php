<?php
/**
 * API for FieldSolutions.com web services
 */
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'On');

require_once('includes/settings.php');
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('soap.wsdl_cache_ttl', '0');

$fields = array();

//$fields[1]['name']    = 'Tech_ID';
//$fields[1]['value']   = '19806';

$fields[1]['name']    = 'Tech_Bid_Amount';
$fields[1]['value']   = '1.00';

/*$fields[2]['name']    = 'PayMax';
$fields[2]['value']   = '1.00';

$fields[3]['name']    = 'Amount_Per';
$fields[3]['value']   = 'Site';*/


// create SOAP client
$client = new SoapClient(WSDL_URL, array('cache_wsdl'=>WSDL_CACHE_NONE));



// create XML file contains WO data
$dom = new DOMDocument("1.0");
$root = $dom->createElement('wo');
foreach ($fields as $key=>$field) {
    $name = $dom->createAttribute('fieldName');
    $name->appendChild($dom->createTextNode($field['name']));

    $value = $dom->createAttribute('fieldValue');
    $value->appendChild($dom->createTextNode($field['value']));

    $parameter = $dom->createElement('parameter');
    $parameter->appendChild($name);
    $parameter->appendChild($value);

    $root->appendChild($parameter);
}
$dom->appendChild($root);
$result = $dom->saveXML();



$params['TB_UNID'] = '55013';
$params['url'] = $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];
$params['xml'] = $result;



$workOrders = $client->updateWorkOrder($userData, $params);


echo "<pre>";
var_dump($workOrders);