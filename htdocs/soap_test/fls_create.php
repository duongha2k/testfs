<?php

class Core_FlsApi_Class
{

	private $internalFields = array('CHECK_NUMBER','MOVEDTOMYSQL', 'DEACTIVATED_DUMMYTECHFLAG', 'TBCOMMENTS');

	private $readOnlyFields = array('WORKORDERID', 'TECHNAME', 'DATEENTERED',
	'DATEPAID', 'INVOICED', 'DATEINVOICED', 'DATEIMPORTED', 'DATEAPPROVED', 'TECHEMAIL', 'ADDEDBY', 
	'EMAIL', 'PHONE', 'LASTMODDATETIME', 'LASTMODBY', 'TECHFNAME', 'TECHLNAME', 'TB_ID', 
	'QTY_APPLICANTS', 'PAYAPPROVEDBY', 'WORKORDERREVIEWED', 'DATE_ASSIGNED',
	'TECHPHONE', 'CALCULATEDDURATION', 'PAYAMOUNT_FINAL');

	/**
	 * create Work Order
	 *
	 * @param string $username
	 * @param string $password
	 * @param API_FlsWorkOrder $workorder
	 * @return API_Response
	 */
	public function createWorkOrder($username, $password, $workorder)
	{
		$woXML = API_FlsWorkOrder::toXml($workorder);
		
       	$logger = Core_FlsApi_Logger::getInstance();
		$logger->setUrl('createWorkOrder');
        $logger->setLogin($username);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
        
		$authData = array('login'=>$username, 'password'=>$password);
		$params = array('xml'=>$woXML);
		$errors = Core_Api_Error::getInstance();

		if (empty($params['xml']))
			$errors->addError(2, 'XML parameter is empty but required');

		$err = $errors->getErrors();
		
		if (!empty($err)) {
			return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);
		}
			
		$user = new Core_FlsApi_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);
			
        $woData = $this->getDataFromXML($params['xml']);

        $this->excludeFields($woData);

		if (empty($woData['CallType'])) {
			$woData['CallType'] = "Service Call";
		}
				
		$combiner = new Core_FlsCombiner_WorkOrder($woData);
		$combiner->setCompanyId($user->getCompanyId());
		$combiner->setCompanyName($user->getCompanyName());
		$combiner->setUserName($user->getLogin());
		if ($combiner->combine()) {
			$woData = $combiner->getData();
		} else {
		    $errors->addErrors('6', $combiner->getErrors());
			return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);
		}

        date_default_timezone_set('US/Central');
		$dateToday = date('m/d/Y h:i:s A');
        $woData['DateEntered'] = $dateToday;
		$woData['LastModDateTime'] = $dateToday;
		$woData['LastModBy'] = $user->getLogin();
		$woData['Addedby'] = $user->getLogin();
		
		if (!empty($woData["FLSTechID"])) {
			$woData["RecruitedBy"] = $user->getLogin();
			$woData["RecruitedTime"] = $dateToday;
		}
		
		if (!empty($woData["PjctStartDate"]) && $woData["CallType"] == "Install") {
			// auto calc sourced by date and auto mark short notice
			$numWeekDays = $this->numberWeekDays(date("m/d/Y"), $woData["PjctStartDate"]);

			if ($numWeekDays <= 5)
				$woData["ShortNotice"] = "True";

			$startDateTS = strtotime(date("m/d/Y"));
			$endDateTS = strtotime($woData);
			
			$sourceByDateTS = $this->calculateSourceByDate($startDateTS, $endDateTS, $woData['DateEntered'], $numWeekDays);
						
			$woData["SourceDate"] = date("m/d/Y", $sourceByDateTS);
		}
		
		if (!empty($woData["PayApprove"]) && $woData["PayApprove"] == "True") {
			$woData["DateApproved"] = $dateToday;
			$woData["PayApprovedBy"] = $user->getLogin();
		}
		
		if (!empty($woData["Deactivated"])) {
			$woData["Deactivated"] = $woData["Deactivated"] == "Yes" ? "Yes" : NULL;
		}

		$factory = new Core_FlsValidation_Factory(Core_Validation_Factory::VALIDATION_TYPE_CREATE);
	    $validation = $factory->getValidationObject($woData);


        if ($validation->validate() === false) {
            $errors->addErrors('6', $validation->getErrors());
			return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);
		}


		if (empty($woData['WorkOrderNo'])) $woData['WorkOrderNo'] = "";
        $fieldsList = implode(',',array_keys($woData));
		
        $result = Core_Caspio::caspioInsert(TABLE_FLS_WORK_ORDERS, $fieldsList, $woData);
		
		if (!$result) {
    		$err = $errors->getErrors();
    		if (!empty($err))
				return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);
		    $errors->addError(7, 'Incorrect SQL request');
			return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);
		}
				
		$res = Core_Caspio::caspioSelectAdv(TABLE_FLS_WORK_ORDERS, 'WorkOrderID', "ISNULL(WorkOrderNo,'') = '" . Core_Caspio::caspioEscape($woData['WorkOrderNo']) . "' AND DateEntered = '{$woData["DateEntered"]}' AND Addedby = '{$woData["Addedby"]}'", '');
		if (empty($res[0]['WorkOrderID'])) {
		    $err = $errors->getErrors();
    		if (!empty($err))
				return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);
			$errors->addError(7, 'Incorrect SQL request');
			return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);
		}

		$wo_id = (int)trim($res[0]['WorkOrderID'], "`'");

		Core_FlsTimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Created", $user->getCompanyId(), $user->getCompanyName(), $woData['ProjectName'], "", "", "");

		if (!empty($woData["FLSTechID"])){
			Core_FlsTimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Assigned", $user->getCompanyId(), $user->getCompanyName(), $woData['ProjectName'], "", "", "");
                        $NoticeClass = new Core_Api_NoticeClass();
                        $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_ASSIGNED);
                }
        
		if (!empty($woData["TechComplete"]) && $woData["TechComplete"] == "True")
			Core_FlsTimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Marked: Complete", $user->getCompanyId(), $user->getCompanyName(), $woData['ProjectName'], "", "", "");

		if (!empty($woData["PayApprove"]) && $woData["PayApprove"] == "True"){
			Core_FlsTimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Approved", $user->getCompanyId(), $user->getCompanyName(), $woData['ProjectName'], "", "", "");
                        $NoticeClass = new Core_Api_NoticeClass();
                $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_APPROVED);
                }

		if (!empty($woData["Kickback"]) && $woData["Kickback"] == "True")
			Core_FlsTimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Kickback", $user->getCompanyId(), $user->getCompanyName(), $woData['ProjectName'], "", "", "");

		if (!empty($woData["Deactivated"]) && $woData["Deactivated"] == "Yes")
			Core_FlsTimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Deactivated", $user->getCompanyId(), $user->getCompanyName(), $woData['ProjectName'], "", "", "");

		if (!empty($woData["DeclinedWorkOrder"]) && $woData["DeclinedWorkOrder"] == "True")
			Core_FlsTimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Declined", $user->getCompanyId(), $user->getCompanyName(), $woData['ProjectName'], "", "", "");

//		$result = $this->makeWorkOrderXML(array('WorkOrderID'=>$wo_id));

		return new API_Response(true, NULL, array(new API_FlsWorkOrder($wo_id)));
	}

	/**
	 * update Work Order
	 *
	 * @param string $username
	 * @param string $password
	 * @param API_FlsWorkOrder $workorder
	 * @return API_Response
	 */
	public function updateWorkOrder($username, $password, $workorder)
	{
		$woXML = API_FlsWorkOrder::toXml($workorder);

		$woData = $this->getDataFromXML($woXML);
		$workOrderID = $woData["WorkOrderID"];

       	$logger = Core_FlsApi_Logger::getInstance();
		$logger->setUrl('updateWorkOrder ' . $workOrderID);
        $logger->setLogin($user);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
        
		$authData = array('login'=>$username, 'password'=>$password);
		
		$user = new Core_FlsApi_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);
        
		$params = array();
		$params['WorkOrderID'] = $workOrderID;
		$params['xml'] = $woXML;
		
		$errors = Core_Api_Error::getInstance();
		if (empty($params['WorkOrderID']))
			$errors->addError(2, 'WorkOrderID parameter is empty but required');

		if (empty($params['xml']))
			$errors->addError(2, 'XML parameter is empty but required');

		$err = $errors->getErrors();
		if (!empty($err))
			return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);
		
		return $this->updateWorkOrderPostXMLParse($user, $params, $woData);
	}
	
	private function updateWorkOrderPostXMLParse($user, $params, $woData) {
		$techInfo = NULL;
		$projectInfo = NULL;
		
	    $errors = Core_Api_Error::getInstance();
		
		$wo_id = trim($params['WorkOrderID'],"`'");

		$criteria = 'WorkOrderID = '.$wo_id;

        $curWO = Core_Caspio::caspioSelectAdv(TABLE_FLS_WORK_ORDERS, Core_Caspio::getTableFieldsList(TABLE_FLS_WORK_ORDERS), $criteria, '');

        if (empty($curWO[0])) {
            $err = $errors->getErrors();
    		if (!empty($err))
				return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);

        	$errors->addError(7, 'Work Order id '.$wo_id.' does not exist');
			return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);
        }
        $curWO = $curWO[0];
        
        $updatedFields = array();
        foreach ($curWO as $key=>$val) {
            if (isset($woData[$key]) && $woData[$key]!==null) {
                $updatedFields[$key] = $woData[$key];
            } else {
                $updatedFields[$key] = $val;
            }
        }
        
		if (!empty($woData["ProjectName"])) {
			// clear project id if project name is set
			$updatedFields["Project_ID"] = "";
		}

        // If invalid field is passed
        $exceptionConverter = new Core_Caspio_ExceptionConverter();
        if (!$exceptionConverter->columnValidate(TABLE_FLS_WORK_ORDERS, array_keys($woData))){
            $errors->addError(6, 'Invalid field "'.$exceptionConverter->getNotValidColumn().'"');
			return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);
        }

        $this->excludeFields($updatedFields);
        
        $combiner = new Core_FlsCombiner_WorkOrder($updatedFields);
		$combiner->setCompanyId($user->getCompanyId());
		$combiner->setCompanyName($user->getCompanyName());
		$combiner->setUserName($user->getLogin());
        if ($combiner->combine()) {
			$updatedFields = $combiner->getData();
		} else {
		    $errors->addErrors('6', $combiner->getErrors());
			return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);
		}
												
		date_default_timezone_set('US/Central');
		$curTime = date('m/d/Y h:i:s A');

		$updatedFields['LastModDateTime'] = $curTime;
		$updatedFields['LastModBy'] = $user->getLogin();
		
		$techAssignedOrReassigned = false;
								
		if (!empty($curWO["FLSTechID"]) && empty($updatedFields["FLSTechID"])) {
			// work order unassigned
			$updatedFields["TechName"] = "";
			$updatedFields["TB_ID"] = NULL;
			$updatedFields["TechPhone"] = "";
			$updatedFields["WorkOrderReviewed"] = "False";
			$updatedFields["TechCheckedIn_48hrs"] = "False";
			$updatedFields["RecruitedBy"] = "";
			$updatedFields["Date_Assigned"] = NULL;
		}
		else if (!empty($updatedFields["FLSTechID"]) && (empty($curWO["FLSTechID"]) || $curWO["FLSTechID"] != $updatedFields["FLSTechID"])) {
			// work order reassigned
			$updatedFields["RecruitedBy"] = $user->getLogin();
			$updatedFields["RecruitedTime"] = $curTime;
			$techAssignedOrReassigned = true;
		}
		
		if ($updatedFields["TechCalled"] == "True" && empty($updatedFields["CalledBy"])) {
			// tech called marked
			$updatedFields["CalledBy"] = $user->getLogin();
			$updatedFields["DateTimeCalled"] = $curTime;
		}
		
		if ($updatedFields["PayApprove"] == "True" && $curWO["PayApprove"] != $updatedFields["PayApprove"]) {
			$updatedFields["DateApproved"] = $curTime;
			$updatedFields["PayApprovedBy"] = $user->getLogin();
		}
		
		// Calculate SoureByDate
		
		if (!empty($updatedFields["PjctStartDate"]) && $updatedFields["CallType"] == "Install") {
			// auto calc sourced by date and auto mark short notice
			$numWeekDays = $this->numberWeekDays(date("m/d/Y"), $updatedFields["PjctStartDate"]);

			if ($numWeekDays <= 5)
				$updatedFields["ShortNotice"] = "True";

			$startDateTS = strtotime(date("m/d/Y"));
			$endDateTS = strtotime($curWO["PjctStartDate"]);
			
			$sourceByDateTS = $this->calculateSourceByDate($startDateTS, $endDateTS, $curWO['DateEntered'], $numWeekDays);
						
			$updatedFields["SourceDate"] = date("m/d/Y", $sourceByDateTS);
		}		
						
		$updatedFields["Deactivated"] = $updatedFields["Deactivated"] == "Yes" ? "Yes" : NULL;

		// End field cleanup. Now validating.
							
		$factory = new Core_FlsValidation_Factory(Core_FlsValidation_Factory::VALIDATION_TYPE_UPDATE);
	    $validation = $factory->getValidationObject($updatedFields);

	    if ($validation->validate() === false) {
            $errors->addErrors('6', $validation->getErrors());
			return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);
        }
		
		// End field validation. Now updating.

		$fieldsList = implode(',',array_keys($updatedFields));
				
		$result = Core_Caspio::caspioUpdate(TABLE_FLS_WORK_ORDERS, $fieldsList, $updatedFields, $criteria);

		if (!$result) {
    		$err = $errors->getErrors();
    		if (!empty($err))
				return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);

		    $errors->addError(7, 'Incorrect SQL request');
			return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);
		}		
		
		// End Updating, now send emails and do timestamps
				
        if ($techAssignedOrReassigned) {
			// Send Tech Assigned Email
            $mail  = new Core_FlsEmailForAssignedTech();
			$mail->setTech($updatedFields['TB_ID']);
            $mail->setWorkOrder($wo_id);
            $mail->setProject($updatedFields['Project_ID']);
            $mail->setCompanyId($user->getCompanyId());
            $mail->setCompanyName($user->getCompanyName());
            $mail->sendMail();
			Core_FlsTimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Assigned", $user->getCompanyId(), $user->getCompanyName(), $updatedFields['ProjectName'], "", "", "");			
                        $NoticeClass = new Core_Api_NoticeClass();
                        $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_ASSIGNED);
        }
		
        if (!empty($updatedFields["FLSTechID"]) && $updatedFields['TechComplete'] != $curWO['TechComplete']) {
			// Send Tech Complete Unchecked Email
			$val = $updatedFields['TechComplete']=='True' ? "Completed" : "Not Completed";
	        Core_FlsTimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Marked: $val", $user->getCompanyId(), $user->getCompanyName(), $updatedFields['ProjectName'], "", "", "");
                if($val == 'Completed'){
                    $NoticeClass = new Core_Api_NoticeClass();
                    $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_WORKDONE);
                }
			if ($updatedFields['TechComplete'] == "False") {
		        Core_FlsTimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Marked Incomplete", $user->getCompanyId(), $user->getCompanyName(), $updatedFields['ProjectName'], "", "", "");
	            $woIncompleteMail = new Core_Mail_WoStatus_FlsIncomplete();
				if ($techInfo == NULL)
					$techInfo = new Core_Tech((int)$updatedFields["TB_ID"]);
				if ($projectInfo == NULL) {
					if (!empty($updatedFields["Project_ID"])) {
						$projectInfo = new Core_FlsProject((int)$updatedFields["Project_ID"]);
						$projectInfo->setData();
					}
				}
				
		        $woIncompleteMail->assign('WorkOrderID', $wo_id);
		        $woIncompleteMail->assign('WorkOrderNo', $updatedFields["WorkOrderNo"]);
		        $woIncompleteMail->assign('missingInfo', $updatedFields["MissingInfo"]);

				$woIncompleteMail->setTech($techInfo);
				$woIncompleteMail->setProject($projectInfo);
                
	            $woIncompleteMail->send();
			}
        }
		
		// Find changed fields
        $changedFields = false;
        if (!empty($updatedFields['FLSTechID'])){
            $bufArr = array('PjctStartDate','PjctStartTime','Address1','City','State','Zip','PayAmt',
                            'FLSComments');
            $woChangedMail = new Core_Mail_WoStatus_FlsChange();
            foreach ($curWO as $key=>$val) {
                if (!empty($updatedFields[$key]) && $val != $updatedFields[$key]) {
                    if (in_array($key,$bufArr)) {
                        $woChangedMail->assign($key, $updatedFields[$key]);
                    	$changedFields = true;
                    }
                }
            }
        }				
		if (!empty($updatedFields['FLSTechID']) && $changedFields) {
			// Send Work Order Changed Email
			if ($techInfo == NULL)
				$techInfo = new Core_Tech((int)$updatedFields["TB_ID"]);
			if ($projectInfo == NULL) {
				$projectInfo = new Core_FlsProject((int)$updatedFields["Project_ID"]);
				$projectInfo->setData();
			}
			$woChangedMail->setTech($techInfo);
			$woChangedMail->setProject($projectInfo);
//			$woChangedMail->loadProject($updatedFields['Project_ID']);
//            $woChangedMail->loadTech($updatedFields['Tech_ID']);
            $woChangedMail->assign('WorkOrderNo', $updatedFields['WorkOrderNo']);
            $woChangedMail->assign('WorkOrderID', $wo_id);
            $woChangedMail->assign('PayApproved', $updatedFields['PayApproved']);
            $woChangedMail->assign('LastModBy', $updatedFields['LastModBy']);
                
            $woChangedMail->send();
		}
				
		if ($updatedFields["PayApprove"] == "True" && $curWO["PayApprove"] != $updatedFields["PayApprove"]){
			Core_FlsTimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Approved", $user->getCompanyId(), $user->getCompanyName(), $updatedFields['ProjectName'], "", "", "");
                        $NoticeClass = new Core_Api_NoticeClass();
                $NoticeClass->createWOChangeStatusLog($wo_id,Core_Api_NoticeClass::WOStatusID_FOR_APPROVED);
                        }

		if ($updatedFields["Kickback"] == "True" && $curWO["Kickback"] != $updatedFields["Kickback"])
			Core_FlsTimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Kickback", $user->getCompanyId(), $user->getCompanyName(), $updatedFields['ProjectName'], "", "", "");

		if ($updatedFields["Deactivated"] == "Yes" && $curWO["Deactivated"] != $updatedFields["Deactivated"])
			Core_FlsTimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Deactivated", $user->getCompanyId(), $user->getCompanyName(), $updatedFields['ProjectName'], "", "", "");

		if ($updatedFields["DeclinedWorkOrder"] == "True" && $curWO["DeclinedWorkOrder"] != $updatedFields["DeclinedWorkOrder"])
			Core_FlsTimeStamp::createTimeStamp($wo_id, $user->getLogin(), "Work Order Declined", $user->getCompanyId(), $user->getCompanyName(), $updatedFields['ProjectName'], "", "", "");
				
//        $res = $this->makeWorkOrderXML(array('WorkOrderID'=>$wo_id));

		return new API_Response(true, NULL, array(new API_FlsWorkOrder($wo_id)));
	}
	
/**
	 * assign work order
	 *
	 * @param string $username
	 * @param string $password
	 * @param int $workOrderID
	 * @param int $flsTechID
	 * @param float $payAmt
	 * @return API_Response
	 */
 	public function assignWorkOrder($username, $password, $workOrderID, $flsTechID, $payAmt)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('assignWorkOrder ' . $workOderID);
        $logger->setLogin($username);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
        
		$authData = array('login'=>$username, 'password'=>$password);
		
		$user = new Core_FlsApi_User();
		$user->checkAuthentication($authData);

		if (!$user->isAuthenticate())
			return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);
		
		$params = array('WorkOrderID'=>$workOrderID);
		$queryFieldList = array('FLSTechID'=>$flsTechID, 'PayAmt'=>$payAmt);
		return $this->updateWorkOrderPostXMLParse($user, $params, $queryFieldList);
	}	

	/**
	 * approve work order
	 *
	 * @param string $username
	 * @param string $password
	 * @param int $workOrderID
	 * @return API_Response
	 */
 	public function approveWorkOrder($username, $password, $workOrderID)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('approveWorkOrder ' . $workOrderID);
        $logger->setLogin($username);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
        
		$authData = array('login'=>$username, 'password'=>$password);
		
		$username = new Core_FlsApi_User();
		$username->checkAuthentication($authData);

		if (!$username->isAuthenticate())
			return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);
		        
		$params = array('WorkOrderID'=>$workOrderID);
		$queryFieldList = array();
		$queryFieldList["PayApprove"] = "True";
		$queryFieldList["Invoiced"] = "False";
		$queryFieldList["TechComplete"] = "True";
		return $this->updateWorkOrderPostXMLParse($username, $params, $queryFieldList);
	}
		
	/**
	 * mark work order incomplete
	 *
	 * @param string $username
	 * @param string $password
	 * @param int $workOrderID
	 * @param string $comments
	 * @return API_Response
	 */
 	public function markWorkOrderIncomplete($username, $password, $workOrderID, $comments)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('markWorkOrderIncomplete ' . $workOrderID);
        $logger->setLogin($username);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
        
		$authData = array('login'=>$username, 'password'=>$password);
		
		$username = new Core_FlsApi_User();
		$username->checkAuthentication($authData);

		if (!$username->isAuthenticate())
			return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);
		
		$params = array('WorkOrderID'=>$workOrderID);
		$queryFieldList = array();
		$queryFieldList["TechComplete"] = "False";
		$queryFieldList["MissingInfo"] = $comments;
		$queryFieldList["PayApprove"] = "False";  
		return $this->updateWorkOrderPostXMLParse($username, $params, $queryFieldList);
	}
	
	/**
	 * kickback work order
	 *
	 * @param string $username
	 * @param string $password
	 * @param int $workOrderID
	 * @return API_Response
	 */
 	public function kickbackWorkOrder($username, $password, $workOrderID)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('deactivateWorkOrder ' . $workOrderID);
        $logger->setLogin($username);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
        
		$authData = array('login'=>$username, 'password'=>$password);
		
		$username = new Core_FlsApi_User();
		$username->checkAuthentication($authData);

		if (!$username->isAuthenticate())
			return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);
        
		$params = array('WorkOrderID'=>$workOrderID);
		$queryFieldList = array();
		$queryFieldList["Kickback"] = "True";
		return $this->updateWorkOrderPostXMLParse($username, $params, $queryFieldList);
	}
	
	/**
	 * deactivate work order
	 *
	 * @param string $username
	 * @param string $password
	 * @param int $workOrderID
	 * @return API_Response
	 */
 	public function deactivateWorkOrder($username, $password, $workOrderID)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('deactivateWorkOrder ' . $workOrderID);
        $logger->setLogin($username);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
        
		$authData = array('login'=>$username, 'password'=>$password);
		
		$username = new Core_FlsApi_User();
		$username->checkAuthentication($authData);

		if (!$username->isAuthenticate())
			return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);
        
		$params = array('WorkOrderID'=>$workOrderID);
		$queryFieldList = array();
		$queryFieldList["Deactivated"] = "Yes";
		return $this->updateWorkOrderPostXMLParse($username, $params, $queryFieldList);
	}

	/**
	 * decline work order
	 *
	 * @param string $username
	 * @param string $password
	 * @param int $workOrderID
	 * @return API_Response
	 */
 	public function declineWorkOrder($username, $password, $workOrderID)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('deactivateWorkOrder ' . $workOrderID);
        $logger->setLogin($username);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
        
		$authData = array('login'=>$username, 'password'=>$password);
		
		$username = new Core_FlsApi_User();
		$username->checkAuthentication($authData);

		if (!$username->isAuthenticate())
			return new API_Response(false, $this->errorsToAPIErrors(Core_Api_Error::getInstance()->getErrorArray()), NULL);
        
		$params = array('WorkOrderID'=>$workOrderID);
		$queryFieldList = array();
		$queryFieldList["DeclinedWorkOrder"] = "True";
		return $this->updateWorkOrderPostXMLParse($username, $params, $queryFieldList);
	}
	
	/**
	 * Get Work Order
	 *
	 * @param string $username
	 * @param string $password
	 * @param int $workOrderID
	 * @return string
	 */
	public function getWorkOrder($username, $password, $workOrderID)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getWorkOrder ' . $workOrderID);
        $logger->setLogin($username);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
        
		$authData = array('login'=>$username, 'password'=>$password);
		$errors = Core_Api_Error::getInstance();

		if (empty($workOrderID))
			$errors->addError(2, 'Work Order ID parameter is empty but required');

		$err = $errors->getErrors();
		if (!empty($err))
			return $err;

		$wo_id = Core_Caspio::caspioEscape((int)$workOrderID);

		$username = new Core_FlsApi_User();
		$username->checkAuthentication($authData);

		if (!$username->isAuthenticate())
			return $errors->getErrors();
            
		$fieldList = $this->getTableDesign(TABLE_FLS_WORK_ORDERS);

		$criteria = "WorkOrderID = '$wo_id'";

		$result = Core_Caspio::caspioSelectAdv(TABLE_FLS_WORK_ORDERS, $fieldList, $criteria, '');

    	$err = $errors->getErrors();
		if (!empty($err))
			return $err;

		$xml = $this->makeWorkOrderXML($result[0]);

		return $xml;
	}
	
	/**
	 * get list of work orders
	 *
	 * @param string $username
	 * @param string $password
	 * @param string $fields
	 * @param string $timeStampFrom
	 * @param string $timeStampTo
	 * @param string $timeStampType
	 * @param array $filters
	 * @return string
	 */
	public function getWorkOrders($username, $password, $fields, $timeStampFrom, $timeStampTo, $timeStampType, $filters)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getWorkOrders');
        $logger->setLogin($username);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
        
		$authData = array('login'=>$username, 'password'=>$password);
		$errors = Core_Api_Error::getInstance();

		$username = new Core_FlsApi_User();
		$username->checkAuthentication($authData);

		if (!$username->isAuthenticate())
			return $errors->getErrors();
        $companyID = $username->getCompanyId();

		$result = $this->getFilteredWorkOrderData($companyID, 
													$fields, 
													$timeStampFrom, 
													$timeStampTo, 
													$timeStampType, 
													$filters);
				
		$err = $errors->getErrors();
		if (!empty($err))
			return $err;

		$xml = $this->makeWorkOrdersXML($result);
		
		return $xml;
	}
	
	/**
	 * Function for getting all Projects for current usre
	 *
	 * @param string $username
	 * @param string $password
	 * @return string
	 */
	public function getAllProjects($username, $password)
	{
       	$logger = Core_Api_Logger::getInstance();
		$logger->setUrl('getAllProjects');
        $logger->setLogin($username);
		$logger->addLog(LOGGER_ERROR_NO, null, 'SOAP request');
        
		$authData = array('login'=>$username, 'password'=>$password);
		$errors = Core_Api_Error::getInstance();

		$username = new Core_FlsApi_User();
		$username->checkAuthentication($authData);

		if (!$username->isAuthenticate())
			return $errors->getErrors();
            
		$fieldsList = 'ProjectNo, ProjectName';
		$companyId  = $username->getCompanyId();
		$criteria   = "";

		$result = Core_Caspio::caspioSelectAdv(TABLE_FLS_PROJECTS, $fieldsList, $criteria, '');

		$err = $errors->getErrors();
		if (!empty($err))
			return $err;

	    $xml = $this->makeProjectsXML($result);
		return $xml;
	}
	
	private function makeProjectsXML($result)
	{
		$dom = new DOMDocument("1.0");
		$root = $dom->createElement('Projects');
	    if (!empty($result[0])) {
		    foreach ($result as $key=>$value) {
                $pj = $dom->createElement('Project');

                $id = $dom->createAttribute('ID');
                $name = $dom->createAttribute('NAME');

                $id->appendChild($dom->createTextNode($value['Project_ID']));
                $name->appendChild($dom->createTextNode($value[' Project_Name']));

                $pj->appendChild($id);
                $pj->appendChild($name);

                $root->appendChild($pj);
            }
	    } else {
//			$root->appendChild($dom->createTextNode('No results'));
	    }
	    $dom->appendChild($root);
		return $dom->saveXML();
	}

	private function makeWorkOrdersXML($result)
	{
		$dom = new DOMDocument("1.0");
		$root = $dom->createElement('FLSWOS');
	    if (!empty($result[0])) {
		    foreach ($result as $key=>$value) {
			    $wo = $dom->createElement('FLSWO');
			    foreach ($value as $k=>$val) {
			        if (!in_array( strtoupper($k), $this->internalFields)) {
    			    	$field = $k == 'TB_UNID' ? $dom->createElement('WIN_NUM') :$dom->createElement($k);
    			    	$field->appendChild($dom->createTextNode($val));
    			    	$wo->appendChild($field);
			        }
			    }
			    $root->appendChild($wo);
		    }
	    }
	    $dom->appendChild($root);
		return $dom->saveXML();
	}

	private function makeWorkOrderXML($result)
	{
		$dom = new DOMDocument("1.0");
		$wo = $dom->createElement('FLSWO');
   		if (!empty($result)) {
		    foreach ($result as $k=>$val) {
		        if (!in_array(strtoupper($k), $this->internalFields)) {
    			    $field = $k == 'TB_UNID' ? $dom->createElement('WIN_NUM') :$dom->createElement($k);
			    	
			    	$field->appendChild($dom->createTextNode( htmlspecialchars($val, ENT_QUOTES) ));
			    	$wo->appendChild($field);
		        }
		    }
   		}
   		$dom->appendChild($wo);
		return $dom->saveXML();
	}

	private function getTableDesign($tableName)
	{
		$columns = Core_Caspio::caspioGetTableDesign($tableName);
        $fieldList = array();

        foreach ($columns as $info) {
	        $fieldInfo = explode(",",$info);
	        $fieldList[] = trim($fieldInfo[0]);
        }

        $fieldList = implode(',', $fieldList);

        return $fieldList;
	}

	private function excludeFields(&$fieldsList)
	{
	    foreach ($fieldsList as $key=>$field) {
	    	if ( in_array( strtoupper($key), $this->readOnlyFields) ) {
	    		unset($fieldsList[$key]);
	    	}
	    }
	}

	private function getDataFromXML($xml)
	{
	    $result = array();
		$dom = new DOMDocument("1.0");
		$dom->loadXML($xml);
		$nodes = $dom->getElementsByTagName('FLSWO')->item(0)->childNodes;

		foreach ($nodes as $node) 
		{
			$result[$node->nodeName] = $node->nodeValue;
		}

		return $result;
	}
		
	private function numberWeekDays($startDate, $endDate) {			
			$startDateTS = strtotime($startDate);
			$endDateTS = strtotime($endDate);

			$startDayOfWeek = date("w", $startDateTS);
			$endDayOfWeek = date("w", $endDateTS);
			
			$daysToNextSunday = 7 - $startDayOfWeek;
			$daysToPreviousSunday = $endDayOfWeek;
			$nextSundayTS = strtotime("+$daysToNextSunday days", $startDateTS);
			$previousSundayTS = strtotime("-$daysToPreviousSunday days", $endDateTS);
			
			$numWeekDays = 0;
			
			if ($nextSundayTS < $endDateTS) {
				// date range is more than one week
				$startWeekNumWeekDays = 5 - ($startDayOfWeek - 1 <= -1 ? 0 : $startDayOfWeek - 1);
				$endWeekNumWeekDays = ($endDayOfWeek == 6 ? 5 : $endDayOfWeek);
				
				$numDaysBetweenFirstAndLastWeek = ($previousSundayTS - $nextSundayTS) / 60 / 60 / 24 / 7 * 5;
				
				echo "$startWeekNumWeekDays : $endWeekNumWeekDays : $numDaysBetweenFirstAndLastWeek : " . date("m/d/Y", $nextSundayTS) . " : "  . date("m/d/Y", $previousSundayTS) .  "<br/>";
				$numWeekDays = $startWeekNumWeekDays + $endWeekNumWeekDays + $numDaysBetweenFirstAndLastWeek;
			}
			else {
				$startWeekNumWeekDays = 5 - ($startDayOfWeek - 1 <= -1 ? 0 : $startDayOfWeek - 1);
				$endWeekNumWeekDaysNotIncluded = 5 - ($endDayOfWeek == 6 ? 5 : $endDayOfWeek);
				echo "$startWeekNumWeekDays : $endWeekNumWeekDaysNotIncluded<br/>";
				$numWeekDays = $startWeekNumWeekDays - $endWeekNumWeekDaysNotIncluded;
			}
			return $numWeekDays;
	}
	
	private function calculateSourceByDate($startDateTS, $endDateTS, $dateEntered, $numWeekDays) {
		$sourceByDateTS = $endDateTS; // use Project Start date by default
			
		if ($numWeekDays > 5 && $numWeekDays < 16) {
			// use Date record created
			$sourceByDateTS = strtotime(date("m/d/Y", strtotime($dateEntered)));
				
			// Can't count start day so subtract, initally start with Project Date
			$sourceByDateTS = strtotime("-1 day", $sourceByDateTS);
		}
				
		if ($numWeekDays < 6)
			$sourceByDateTS = strtotime(date("m/d/Y", strtotime($this->workOrderData["DateEntered"])));
		else if ($numWeekDays > 5 && $numWeekDays < 16) {
			for ($i = 0; $i <= 5; ++$i) {
				$dayNum = date("w", $sourceByDateTS);
				if ($dayNum == 0 || $dayNum == 6)
					--$i;
				$sourceByDateTS = strtotime("+1 days", $sourceByDateTS);
			}
		}
		else {
			$sourceByDateTS = strtotime("-1 days", $sourceByDateTS);
			for ($i = 10; $i >= 1; $i--) {
				$dayNum = date("w", $sourceByDateTS);
				if ($dayNum == 0 || $dayNum == 6)
					++$i;
			}
			$sourceByDateTS = strtotime("-1 days", $sourceByDateTS);
		}
		
		return $sourceByDateTS;			
	}
	
	private function getFilteredWorkOrderData($companyID, $fields, $timeStampFrom, $timeStampTo, $timeStampType, $criteria)
	{
		$fieldList = !empty($fields) ? $fields : Core_Caspio::getTableFieldsList(TABLE_FLS_WORK_ORDERS);
		$fieldList = str_replace(' ', '', $fieldList);
		
		$criteriaArr = array();
		if (!empty($timeStampFrom) || !empty($timeStampTo) || !empty($timeStampType)) 
		{
			$crit = 'Company_ID = \''.Core_Caspio::caspioEscape($companyID).'\'';
			
			if (!empty($timeStampType)) {
				$crit .= ' AND Description LIKE \'%'.Core_Caspio::caspioEscape($timeStampType).'%\'';
			}
			if (!empty($timeStampFrom)) {
				$crit .= ' AND DateTime_Stamp > \''.Core_Caspio::caspioEscape($timeStampFrom).'\'';
			}
			if (!empty($timeStampTo)) {
				$crit .= ' AND DateTime_Stamp < \''.Core_Caspio::caspioEscape($timeStampTo).'\'';
			}
			
			$timeStamps = Core_Caspio::caspioSelectAdv(TABLE_FLS_WORK_ORDER_TIMESTAMPS, 'WO_UNID', $crit, '');

			if (!empty($timeStamps)) {
				$res = '';
				foreach ($timeStamps as $rec) {
					$res .= ','.$rec['WO_UNID'];
				}
				$criteriaArr[] = 'WorkOrderID IN ('.trim($res, ',').')';
			} else {
				return '';
			}
		}
		
        if (!empty($criteria['WorkOrderNumber'])) {
            $criteriaArr[] = $this->searchCriteria("WorkOrderNum", $criteria['WorkOrderNum'], "String"); 
        }

        if (!empty($criteria['ProjectName'])) {
            $criteriaArr[] = $this->searchCriteria("ProjectName", $criteria['ProjectName'], "String"); 
        }

        if (!empty($criteria['SiteName'])) {
            $criteriaArr[] = $this->searchCriteria("SiteName", $criteria['SiteName'], "String"); 
        }

		if (!empty($criteria['FLSTechID'])) {
            $criteriaArr[] = $this->searchCriteria("FLSTechID", $criteria['FLSTechID'], "Number"); 
		}

        if (!empty($criteria['TechName'])) {
            $criteriaArr[] = $this->searchCriteria("TechName", $criteria['TechName'], "String"); 
        }

		if (!empty($criteria['PjctStartDateFrom'])) {
            $criteriaArr[] = $this->searchCriteria("PjctStartDate", $criteria['PjctStartDateFrom'], "DateGT"); 
		}        

		if (!empty($criteria['PjctStartDateTo'])) {
            $criteriaArr[] = $this->searchCriteria("PjctStartDate", $criteria['PjctStartDateTo'], "DateLT"); 
		}        

		if (!empty($criteria['SourceDateFrom'])) {
            $criteriaArr[] = $this->searchCriteria("SourceDate", $criteria['SourceDateFrom'], "DateGT"); 
		}        

		if (!empty($criteria['SourceDateTo'])) {
            $criteriaArr[] = $this->searchCriteria("SourceDate", $criteria['SourceDateTo'], "DateGT"); 
		}        

        if (!empty($criteria['PayApprove'])) { 
            $criteriaArr[] = $this->searchCriteria("PayApprove", $criteria['PayApprove'], "Boolean"); 
        }

        if (!empty($criteria['PenaltyType'])) { 
            $criteriaArr[] = $this->searchCriteria("PenaltyType", $criteria['PenaltyType'], "String"); 
        }

        if (!empty($criteria['Addedby'])) { 
            $criteriaArr[] = $this->searchCriteria("Addedby", $criteria['Addedby'], "String"); 
        }

        if (!empty($criteria['Lead'])) { 
            $criteriaArr[] = $this->searchCriteria("Lead", $criteria['Lead'], "Boolean"); 
        }

        if (!empty($criteria['Assist'])) { 
            $criteriaArr[] = $this->searchCriteria("Assist", $criteria['Assist'], "Boolean"); 
        }

        if (!empty($criteria['Kickback'])) { 
            $criteriaArr[] = $this->searchCriteria("Kickback", $criteria['Kickback'], "Boolean"); 
        }

        if (!empty($criteria['City'])) { 
            $criteriaArr[] = $this->searchCriteria("City", $criteria['City'], "String"); 
        }

        if (!empty($criteria['State'])) { 
            $criteriaArr[] = $this->searchCriteria("State", $criteria['State'], "String"); 
        }

        if (!empty($criteria['UpdateRequested'])) { 
            $criteriaArr[] = $this->searchCriteria("UpdateRequested", $criteria['UpdateRequested'], "Boolean"); 
        }

        if (!empty($criteria['TechCalled'])) { 
            $criteriaArr[] = $this->searchCriteria("TechCalled", $criteria['TechCalled'], "Boolean"); 
        }

        if (!empty($criteria['ShortNotice'])) { 
            $criteriaArr[] = $this->searchCriteria("ShortNotice", $criteria['ShortNotice'], "Boolean"); 
        }

        if (!empty($criteria['FaxReceived'])) { 
            $criteriaArr[] = $this->searchCriteria("FaxReceived", $criteria['FaxReceived'], "Boolean"); 
        }

        if (!empty($criteria['IncompleteFax'])) { 
            $criteriaArr[] = $this->searchCriteria("IncompleteFax", $criteria['IncompleteFax'], "Boolean"); 
        }

        if (!empty($criteria['Qty_Applicants'])) { 
            $criteriaArr[] = $this->searchCriteria("Qty_Applicants", $criteria['Qty_Applicants'], "NumberGT"); 
        }

        if (!empty($criteria['DeclinedWorkOrder'])) { 
            $criteriaArr[] = $this->searchCriteria("DeclinedWorkOrder", $criteria['DeclinedWorkOrder'], "Boolean"); 
        }

        if (!empty($criteria['Deactivated'])) { 
			if ($criteria['Deactivated'] == "Yes")
	            $criteriaArr[] = "Deactivated = 'Yes'";
			else
	            $criteriaArr[] = "ISNULL(Deactivated,'') = ''";
        }
                
//		$criteriaArr[] = 'Company_ID = \''.Core_Caspio::caspioEscape($companyID).'\'';
		$criteria = implode(' AND ', $criteriaArr);
		return Core_Caspio::caspioSelectAdv(TABLE_FLS_WORK_ORDERS, $fieldList, $criteria, '');
	}
	
	private function searchCriteria($field, $value, $type) {
		$ret = "";
		switch ($type) {
			case "String":
	            $ret = "$field LIKE '%" . Core_Caspio::caspioEscape($value). "%'";
				break;
			case "Boolean":
				$value = ($value == "True" ? "'1'" : "'0'");
				$ret = "$field = " . $value;
				break;
			case "NumberGT":
			case "DateGT":
				$ret = "$field >= '" . Core_Caspio::caspioEscape($value) . "'";
				break;
			case "DateLT":
				$ret = "$field <= '" . Core_Caspio::caspioEscape($value) . "'";
				break;
			default:
				$ret = "$field = '" . Core_Caspio::caspioEscape($value) . "'";
				break;				
		}
		return $ret;
	}
	
	private function errorsToAPIErrors($errors) {
		$ret = array();
		foreach ($errors as $key => $val) {
			$ret[] = new API_Error($val["error_id"], $val["id"], $val["description"]);
		}
		return $ret;
	}
	
}

/**
 * class API_FlsWorkOrder
 * @package API_FlsWorkOrder
 */
class API_FlsWorkOrder {
	/** 
	 * 
	 * @var int	 */
	var $WorkOrderID;


	/** 
	 * 
	 * @var string	 */
	var $WorkOrderNo;


	/** 
	 * 
	 * @var string	 */
	var $FLSTechID;


	/** 
	 * 
	 * @var string	 */
	var $TechName;


	/** 
	 * 
	 * @var string	 */
	var $DateEntered;


	/** 
	 * 
	 * @var string	 */
	var $DateCompleted;


	/** 
	 * 
	 * @var string	 */
	var $StartTime;


	/** 
	 * 
	 * @var string	 */
	var $EndTime;


	/** 
	 * 
	 * @var string	 */
	var $Comments;


	/** 
	 * 
	 * @var string	 */
	var $Parts;


	/** 
	 * 
	 * @var boolean	 */
	var $CallClosed;


	/** 
	 * 
	 * @var boolean	 */
	var $PartsOk;


	/** 
	 * 
	 * @var boolean	 */
	var $TechComplete;


	/** 
	 * 
	 * @var boolean	 */
	var $PayApprove;


	/** 
	 * 
	 * @var string	 */
	var $PartNos;


	/** 
	 * 
	 * @var string	 */
	var $QtyParts;


	/** 
	 * 
	 * @var string	 */
	var $Waybill;


	/** 
	 * 
	 * @var string	 */
	var $RA;


	/** 
	 * 
	 * @var string	 */
	var $Paid;


	/** 
	 * 
	 * @var string	 */
	var $DatePaid;


	/** 
	 * 
	 * @var string	 */
	var $CallType;


	/** 
	 * 
	 * @var string	 */
	var $ProjectName;


	/** 
	 * 
	 * @var string	 */
	var $FLSComments;


	/** 
	 * 
	 * @var float	 */
	var $PayAmt;


	/** 
	 * 
	 * @var string	 */
	var $SerialNo;


	/** 
	 * 
	 * @var boolean	 */
	var $ContactHD;


	/** 
	 * 
	 * @var string	 */
	var $HDPOC;


	/** 
	 * 
	 * @var string	 */
	var $SerialNoOut;


	/** 
	 * 
	 * @var string	 */
	var $SiteName;


	/** 
	 * 
	 * @var string	 */
	var $Address1;


	/** 
	 * 
	 * @var string	 */
	var $City;


	/** 
	 * 
	 * @var string	 */
	var $State;


	/** 
	 * 
	 * @var string	 */
	var $Zip;


	/** 
	 * 
	 * @var boolean	 */
	var $Invoiced;


	/** 
	 * 
	 * @var string	 */
	var $DateInvoiced;


	/** 
	 * 
	 * @var string	 */
	var $PjctStartDate;


	/** 
	 * 
	 * @var string	 */
	var $PjctStartTime;


	/** 
	 * 
	 * @var string	 */
	var $DateImported;


	/** 
	 * 
	 * @var string	 */
	var $MisssingInfo;


	/** 
	 * 
	 * @var string	 */
	var $Deactivated;


	/** 
	 * 
	 * @var string	 */
	var $SiteList;


	/** 
	 * 
	 * @var string	 */
	var $DateApproved;


	/** 
	 * 
	 * @var string	 */
	var $TroubleShoot;


	/** 
	 * 
	 * @var string	 */
	var $TechEmail;


	/** 
	 * 
	 * @var boolean	 */
	var $Kickback;


	/** 
	 * 
	 * @var string	 */
	var $SitePhone;


	/** 
	 * 
	 * @var string	 */
	var $Addedby;


	/** 
	 * 
	 * @var string	 */
	var $Email;


	/** 
	 * 
	 * @var string	 */
	var $Phone;


	/** 
	 * 
	 * @var boolean	 */
	var $Lead;


	/** 
	 * 
	 * @var boolean	 */
	var $Assist;


	/** 
	 * 
	 * @var string	 */
	var $BU1FLSID;


	/** 
	 * 
	 * @var string	 */
	var $BU1Info;


	/** 
	 * 
	 * @var string	 */
	var $BU2FLSID;


	/** 
	 * 
	 * @var string	 */
	var $BU2Info;


	/** 
	 * 
	 * @var string	 */
	var $ASST;


	/** 
	 * 
	 * @var string	 */
	var $LastModDateTime;


	/** 
	 * 
	 * @var string	 */
	var $LastModBy;


	/** 
	 * 
	 * @var boolean	 */
	var $UpdateRequested;


	/** 
	 * 
	 * @var string	 */
	var $WhoRequested;


	/** 
	 * 
	 * @var string	 */
	var $ReasonRequested;


	/** 
	 * 
	 * @var string	 */
	var $FileAttachment;


	/** 
	 * 
	 * @var boolean	 */
	var $FaxReceived;


	/** 
	 * 
	 * @var string	 */
	var $RecruitedBy;


	/** 
	 * 
	 * @var string	 */
	var $SourceDate;


	/** 
	 * 
	 * @var boolean	 */
	var $TechCalled;


	/** 
	 * 
	 * @var string	 */
	var $DateTimeCalled;



	/** 
	 * 
	 * @var string	 */
	var $CalledBy;


	/** 
	 * 
	 * @var string	 */
	var $TechFName;


	/** 
	 * 
	 * @var string	 */
	var $TechLName;


	/** 
	 * 
	 * @var boolean	 */
	var $ShortNotice;


	/** 
	 * 
	 * @var boolean	 */
	var $IncompleteFax;


	/** 
	 * 
	 * @var string	 */
	var $TB_ID;


	/** 
	 * 
	 * @var int	 */
	var $Qty_Applicants;


	/** 
	 * 
	 * @var string	 */
	var $PayApprovedBy;


	/** 
	 * 
	 * @var int	 */
	var $NoShow_Tech;


	/** 
	 * 
	 * @var boolean	 */
	var $SignOffNotNeeded;


	/** 
	 * 
	 * @var string	 */
	var $RecruitedTime;


	/** 
	 * 
	 * @var boolean	 */
	var $TechCheckedIn_48hrs;


	/** 
	 * 
	 * @var boolean	 */
	var $WorkOrderReviewed;


	/** 
	 * 
	 * @var string	 */
	var $Date_Assigned;


	/** 
	 * 
	 * @var int	 */
	var $Project_ID;


	/** 
	 * 
	 * @var string	 */
	var $TechPhone;


	/** 
	 * 
	 * @var boolean	 */
	var $DeclinedWorkOrder;


	/** 
	 * 
	 * @var string	 */
	var $CallManagementArea;


	/** 
	 * 
	 * @var string	 */
	var $Checked_in_By;


	/** 
	 * 
	 * @var string	 */
	var $Import_File_Name;


	/** 
	 * 
	 * @var int	 */
	var $BackOut_Tech;


	/** 
	 * 
	 * @var string	 */
	var $Date_Checked_in;


	/** 
	 * 
	 * @var float	 */
	var $Duration;


	/** 
	 * 
	 * @var float	 */
	var $PayAmount_Final;


	/** 
	 * 
	 * @var string	 */
	var $Penalty_Type;


	/** 
	 * 
	 * @var string	 */
	var $AttachedFile;


	/** 
	 * 
	 * @var boolean	 */
	var $PreCall_2;


	/** 
	 * 
	 * @var string	 */
	var $CalledBy_2;


	/** 
	 * 
	 * @var string	 */
	var $DateTimeCalled_2;


	/** 
	 * 
	 * @var boolean	 */
	var $OOS;


	/** 
	 * 
	 * @var string	 */
	var $ExtraTime;


	/** 
	 * 
	 * @var boolean	 */
	var $Unexpected_Steps;


	/** 
	 * 
	 * @var string	 */
	var $Unexpected_Desc;


	/** 
	 * 
	 * @var string	 */
	var $AskedBy;


	/** 
	 * 
	 * @var string	 */
	var $AskedBy_Name;


	/** 
	 * 
	 * @var float	 */
	var $CalculatedDuration;

	public function __construct($id) {
		$this->WorkOrderID = $id;
	}
	
	public static function toXml($wo) {
		$dom = new DOMDocument("1.0");
		$root = $dom->createElement('FLSWO');
		foreach ($wo as $key => $value) {
			if ($value == NULL) continue;
			$parameter = $dom->createElement($key, $value);
			$root->appendChild($parameter);
		}
		$dom->appendChild($root);
		return $dom->saveXML();
	}
}

class API_Error {
    /**
	 * error code
     * @var int */
    var $code;

    /**
     * error code
     * @var string */
    var $type;

    /**
     * error message
     * @var string */
    var $message;
	
	public function __construct($code, $type, $message) {
		$this->code = $code;
		$this->type = $type;
		$this->message = $message;
	}
}

class API_Response {
    /**
     * true means call completed successfully and false means one or more errors occurred
     * @var boolean */
    var $success;

    /**
     * array of one or more error objects
     * @var array */
    var $errors;
   
    /**
     * array of one or more work order objects (for create / update only WorkOrderID is filled) or one or more project objects for getAllProjects
     * @var array */
    var $data;

	public function __construct($success, $errors, $data) {
		$this->success = $success;
		$this->errors = $errors;
		$this->data = $data;
	}
}