<?php
/**
 * API for FieldSolutions.com web services
 */
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'On');

require_once('includes/settings.php');
ini_set("soap.wsdl_cache_enabled", "0");
ini_set('soap.wsdl_cache_ttl', '0');

$client = new SoapClient(WSDL_URL, array('cache_wsdl'=>WSDL_CACHE_NONE));


$params = array();
$params['fieldList'] = '';  // not required
$params['TB_UNID'] = '49486';
$params['url'] = $_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF'];

$workOrders = $client->getWorkOrder($userData, $params);
echo "<pre>";
var_dump($workOrders);