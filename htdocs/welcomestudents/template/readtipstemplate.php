<div id="ReadTipsTemplate" style="display: none;">
    <div style="font-size: 20px;">TIPS FOR TECHS</div>
    <hr style="background: #1F497D;">
    <br>
    <div style="float: left;width: 50%;">
        <div style="font-weight: bold;">Complete your Profile</div>
        <br>
        <ul style="list-style: inherit; margin-left: 40px;">
            <li>Clients almost always review a technician's profile to assess if the tech is qualified for a given work order</li>
            <li>Most important profile categories to complete are your skill ratings, tools and equipment, and resume </li>
        </ul>
        <br>
        <div style="font-weight: bold;">Set up Payment</div>
        <br>
        <ul style="list-style: inherit; margin-left: 40px;">
            <li>You must have a W9 on file with FieldSolutions in order to be paid. W9 is located in the top right of your tech homepage</li>
            <li>Sign up for Direct Deposit to receive pay deposited to your banking account, up a week earlier than the default option of being mailed a check</li>
        </ul>
        <br>
        <div style="font-weight: bold;">Complete a Background Check </div>
        <br>
        <ul style="list-style: inherit; margin-left: 40px;">
            <li>Available through FieldSolutions for $19</li>
            <li>Background checks are required for 50% of FieldSolutions work and many clients prefer to assign a technician who has a background check on file  </li>
        </ul>
        <br>
    </div>
    <div style="float: left;width: 45%;padding-left: 20px;">
        <div style="font-weight: bold;">Complete Client Certifications</div>
        <br>
        <ul style="list-style: inherit; margin-left: 40px;">
            <li>FieldSolutions offers a range of free certifications that cover specific technical skills as well as industry softskills </li>
            <li>Having completed a client certification makes you eligible for that client's work and tags your profile as client preferred</li>
        </ul>
        <br>
        <div style="font-weight: bold;">When bidding, add a short note in the bid comment box, such as:</div>
        <br>
        <ul style="list-style: inherit; margin-left: 40px;">
            <li>Your experience with the given scope of work, ie: years of experience or  relevant work history</li>
            <li>An explanation of your bid rate, ie: if compensation is required for travel</li>
            <li>Miscellaneous notes, such as if you are interested in negotiating  rate based on being assigned multiple work orders in the area</li>
        </ul>
        <br>
        <div style="margin-left: 40px;">
            <a href="/techSignup.php?devry=1" style="text-decoration: underline;font-weight: bold;">Get Started Now</a>
        </div>
    </div>
</div>