<div id="ReadQuickTemplate" style="display: none;">
    <div style="font-size: 20px;">QUICK STEPS FOR GETTING WORK</div>
    <hr style="background: #1F497D;">
    <br>
    <div style="font-weight: bold;">1) Create your Technician Profile</div>
    <br>
    <ul style="list-style: inherit; margin-left: 40px;">
        <li>Register with FieldSolutions at <a target="_blank"  href="http://www.fieldsolutions.com" >www.FieldSolutions.com</a>. Basic contact information is required to create your profile</li>
        <li>Highlight your skills and expertise in your technician profile.  The more you add, the more likely you will be selected for work </li>
    </ul>
    <br>
    <div style="font-weight: bold;">2) View our Current Postings of Available Work</div>
    <br>
    <ul style="list-style: inherit; margin-left: 40px;">
        <li>Sign into your technician account and view available work orders on your dashboard</li>
        <li>Download FieldSolutions iPhone and Android apps to view work while your in the field</li>
        <li>Receive work alert emails and text messages when new work is published nearby</li>
    </ul>
    <br>
    <div style="font-weight: bold;">3) Apply for Available Work Orders</div>
    <br>
    <ul style="list-style: inherit; margin-left: 40px;">
        <li>Simply enter the rate you want for performing that work and submit to be considered by the buyer</li>
        <li>Add comments or questions when you apply in order to communicate any unique requirements or costs to the coordinator </li>
    </ul>
    <br>
    <div style="margin-left: 40px;">
        <a href="/techSignup.php?devry=1" style="text-decoration: underline;font-weight: bold;">Get Started Now</a>
    </div>
</div>