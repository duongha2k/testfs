<?php
include ("../headerStartSession.php");

$siteTemplate = $_SESSION['template'];
include ("../headerSimple.php");
?>

<body>

    <div id="mainContainer">
        <div id="headerSlider" style="padding-bottom:10px;float: left;width: 1240px;">
            <div id="headerRight" style="float: right;width: 70%;">
                <div id="topNavBar" style="border: 0px;width: 100%;">
                    <ul>
                        <?php // if($displayHome!="no"){   ?>   
                        <?php if (false) { //If Logged In   ?>

                            <?php
                            if ($loggedInAs == "tech") { //Logged in as customer
                                echo '<li><a href="http://' . $siteTemplate . '.fieldsolutions.com/logOut.php">Logout</a></li>';
                                echo '<li><a href="../dashboard.php">Home</a></li>';
                            }
                            ?>

                        <?php } else { // Not logged in or could be admin  ?>

                            <script type="text/javascript">
                                //<![CDATA[

                                jQuery.cookie ("Tech_Source", "<?= $siteTemplate ?>", { expires: 7, path: "/", domain: ".fieldsolutions.com" });

                                var remember_default = "tech";

                                jQuery.noConflict();

                                function ajaxRequestFormData (form, login_key) {
                                    jQuery("#" + form + "LoginForm :input").attr ("disabled", "disabled");
                                    jQuery.ajax ({
                                        type: "GET",
                                        url: "/ajax/RequestLoginKey.php",
                                        dataType: "json",
                                        cache: false,
                                        data: { "login_type": form, "login_key": login_key },
                                        success: function (data, status, error) {
                                            jQuery("#" + form + "LoginForm :input").removeAttr('disabled');

                                            if (status == "success" && data.success) {
                                                var key = data.results["login_key"];
                                                var form_data = data.results["login_data"];

                                                jQuery.cookie (form + "_key", key, { expires: 3650, path: "/" });

                                                if (typeof (form_data) != "undefined") {
                                                    var element = jQuery("#" + form + "LoginForm");
                                                    if (typeof (element.deserialize) == "undefined") element.deserialize = deserialize;
                                                    element.deserialize (
                                                    Base64.decode (form_data)
                                                );

                                                    //Delay submission of form to prevent breaking in
                                                    //some browsers (loaded data is not sent if submitted
                                                    //immediately).
                                                    //if (document.referrer.indexOf ("dashboard.php") == -1) {
                                                    if (document.referrer == "") {
                                                        setTimeout (function () {
                                                            jQuery("#" + form + "LoginForm").submit ();
                                                        }, 250);
                                                    }
                                                }

                                                jQuery("#" + form + "_key").val (key);
                                            }
                                            else {
                                                jQuery ("#" + form + "ErrorMsg").css({display: "block"});
                                                jQuery ("#" + form + "ErrorMsg").text ("");

                                                for (var i = 0; i < data.errors.length; i++)
                                                    jQuery ("#" + form + "ErrorMsg").append (data.errors[i]);
                                            }
                                        },
                                        error: function (data, status, error) {
                                            //console.log (data, status, error);
                                            jQuery ("#" + form + "LoginForm :input").removeAttr('disabled');
                                            jQuery ("#" + form + "ErrorMsg").css({display: "block"});
                                            jQuery ("#" + form + "ErrorMsg").text ("Failed to connect to server.  This feature may be temporarily disabled for security purposes.");
                                        }
                                    });
                                };

                                function rememberChkBxOnChange (event) {
                                    var form = event.data ? event.data["form"] : remember_default;
                                    var login_key = jQuery.cookie (form + "_key");
                                    var checkbox = jQuery ("#" + form + "_Remember");

                                    if (login_key == null || login_key == "") {
                                        login_key = jQuery("#" + form + "_key").val ();
                                    }

                                    if (login_key == "" && checkbox.is (":checked")) {
                                        ajaxRequestFormData (form, login_key);
                                    }
                                    else {
                                        jQuery.cookie (form + "_key", "", { expires: 3650, path: "/" });
                                    }

                                    return true;
                                }

                                function formOnSubmit (event) {
                                    var form = event.data ? event.data["form"] : remember_default;
                                    var checkbox = jQuery ("#" + form + "_Remember");
                                    var login_data = null;

                                    if (checkbox.is (":checked")) {
                                        login_data = Base64.encode (jQuery("#" + form + "LoginForm").serialize ());

                                        jQuery ("#" + form + "_data").val (login_data);
                                    }

                                    return true;
                                }

                                jQuery(document).ready(function() {
                                    var tech_key = jQuery.cookie ("tech_key");

                                    if (tech_key != null && tech_key != "") {
                                        if (document.referrer.indexOf ("/techs") != -1) {
                                            jQuery.cookie ("teck_key", "", { expires: 3650, path: "/" });
                                        }
                                        else {
                                            jQuery("#tech_Remember").attr("checked", true);
                                            jQuery("#tech_key").val (tech_key);
                                            ajaxRequestFormData ("tech", tech_key);
                                        }
                                    }

                                    jQuery("#techLoginForm").submit (formOnSubmit);
                                    jQuery("#tech_Remember").change (rememberChkBxOnChange);
                                });
                                //]]>
                            </script>

                            <li>
                                    <!--link rel="stylesheet" href="http://<?php echo $siteTemplate; ?>.fieldsolutions.com/wp/wp-content/themes/fieldsolutions/style.css?20120919" type="text/css" media="screen" /-->
                                <script type="text/javascript" src="/widgets/js/jquery.deserialize.js"></script>
                                <!--a href="http://<?php echo $siteTemplate; ?>.fieldsolutions.com/welcometechs/dashboard.php">Home</a-->
                                <div class="login_box" style="padding-left: 10px;">
                                    <form id="techLoginForm" name="techLoginForm" method="post" action="/techs/setTechLoginSession.php?devry=1"  style="background-color: transparent; margin: 0px;">

                                        <div class="box_header">
                                            <table width="230" border="0">
                                                <tr valign="top">
                                                    <td><span>Technician Sign In</span></td>
                                                    <td>
    <!--                                                        <input type="checkbox" name="Remember" id="tech_Remember" style="margin: 0;" />&nbsp;
                                                        Remember Me</br>-->
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--span>Technician Sign In</span>
                                            <span style="color: #4A8CBD; font-size: 10px; line-height: 12px; vertical-align: top; margin-left: 10px;">
                                                    <input type="checkbox" name="Remember" id="tech_Remember" style="margin: 0;" />&nbsp;
                                                    Remember Me &nbsp;&nbsp;&nbsp;&nbsp;
                                            </span-->
                                        </div>
                                        <p id="techAuthFailed" style="color: rgb(255, 0, 0); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold; text-align: left; vertical-align: middle; margin-left: 5px; display: none;">Authentication failed. The information you have provided cannot be authenticated. Check your login information and try again.</p>
                                        <p id="techNotActivated" style="color: rgb(255, 0, 0); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold; text-align: left; vertical-align: middle; margin-left: 5px; display: none;">Your login information is incorrect or your account is not active. Please try re-entering your user name and password. New registrants <a href="javascript:void(0);" onclick="sendActivateEmail('<?= $_REQUEST['email'] ?>');">click here</a> to have the verification email sent again. If you need assistance, please contact support at
                                            952-288-2500 or support@fieldsolutions.com.</p>
                                        <p id="techErrorMsg" style="color: rgb(255, 0, 0); font-size: 12px; font-family: Verdana; font-style: normal; font-weight: bold; text-align: left; vertical-align: middle; margin-left: 5px; display: none;"></p>
                                        <div class="clr">
                                            <div class="u_name"><input class="techFormInput" type="text" value="" maxlength="255" id="T_UserName" name="UserName" /><br />user name</div>
                                            <div class="u_pass"><input class="techFormInput" type="password" value="" maxlength="255" id="T_Password" name="Password" /><br />password</div>
                                            <input type="submit" id="" value="" name="" class="button" style='background: url("/widgets/css/images/button_bl.gif") no-repeat scroll 0 0 transparent;' />
                                            <input type="hidden" name="V" value="true" />
                                            <input type="hidden" name="page" value="home" />
                                            <input type="hidden" name="r" value="<?= htmlentities($_GET['r']) ?>">
                                            <input type="hidden" id="tech_key" name="tech_key" value="" />
                                            <input type="hidden" id="tech_data" name="tech_data" value="" />
                                            <input type="hidden" name="login_page" value="/devry/login.php" />
                                        </div>
                                        <a href="https://<?= $siteTemplate ?>.fieldsolutions.com/misc/lostPassword.php">Forgot Username or Password ?</a><br />
                                        <a href="/techSignup.php?devry=1">Register Now!</a>
                                    </form>
                                </div>
                                <div style="width: 230px; height: 85px; position: relative; padding-right: 12px; float:right; text-align:right; vertical-align: bottom; border-right: 1px solid #D0D0D0;">
                                    <div style="position: absolute; bottom: 0; right: 12px; color: #9f9f9f; font: normal 11px/16px Arial, Helvetica, sans-serif;">
                                        Contact FieldSolutions:<br/>
                                        Email: <a href="mailto:support@FieldSolutions.com">support@FieldSolutions.com</a><br/>
                                        Phone: 952-288-2500 Option 1<br/>
                                    </div>
                                </div>
                                <div style="clear: both;"></div>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div id="headerLogo" style='background-image: url("/widgets/images/logo_devry.jpg");background-size: 148px auto;
                 margin-left: 10px;
                 margin-top: 5px;'></div>
        </div>
        <div style="clear: both;"></div>
        <script>
            jQuery(window).ready(function(){
                var reqLogin = '<?= $_REQUEST['value'] ?>';
               
                if(reqLogin == "false"){
                    $("#techAuthFailed").show();
                }else if(reqLogin == "notactivated"){
                    $("#techNotActivated").show();
                }	
                else{
                    $("#techAuthFailed").hide();
                    $("#techNotActivated").hide();
                }

            });
        </script>