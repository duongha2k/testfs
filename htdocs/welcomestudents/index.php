<?php $page = 'techs'; ?>
<?php $option = 'home'; ?>
<?php
require ("header.php");
?>
<?php require ("navBar.php"); ?>
<style>
    #topNavBar li a{
        color: #4A8CBD;
        text-decoration: underline;
    }
    #topNavBar li a:hover{
        color: #4A8CBD;
        text-decoration: none;
        background: #FFFFFF;
    }
    ul{
        list-style-type:disc;
    }
    ul li{
        padding-bottom: 7px;

    }
    
    .backgorundBottom{
        padding:0 38px;
        height:126px;
        width:100%;
        background-repeat: no-repeat;
        background-image: url("/widgets/images/devry_right.jpg");
        background-size: 100% 126px;
        -webkit-background-size:100% 126px;
        -moz-background-size:100% 126px;
        -o-background-size:100% 126px;
        -ms-background-size:100% 126px;
        -ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='/widgets/images/devry_right.jpg',sizingMethod='scale')";
    }
    .login_Top_center{
        background-image: url("/widgets/images/devry_right_B.png");
        background-position: center top;
        background-repeat: repeat-x;
        height: 126px;
        margin-top: 25px;
    }
    .login_Top_right {
        background-image: url("/widgets/images/devry_right_R.png");
        background-position: right top;
        background-repeat: no-repeat;
        height: 126px;
    }

    .login_Top_left {
        background-image: url("/widgets/images/devry_right_L.png");
        background-position: left top;
        background-repeat: no-repeat;
        height: 126px;
    }
</style>
<script type="text/javascript" src="js/login.js"></script>
<table width="1240px" cellspacing="0" cellpadding="0">
    <tr>
        <td style="padding: 10px;" valign="top">
            <div style='height:450px;width:315px;background-repeat: no-repeat;background-image: url("/widgets/images/left_login_devry.jpg");background-size: 315px 450px;'>
                <div style="padding: 20px;">
                    <h2 style="font-size: 16px;color: #F69630;">DeVry Internship Program</h2>
                    <div style="padding-top: 7px;"><span style="font-weight: bold;">DeVry University is partnering with FieldSolutions </span>to give students the opportunity to learn and work as real technicians on real jobs with real pay. Gain experience and build the professional 'soft skills' necessary for success in the electronics service industry, all while earning class credits that contribute to your graduation requirements!</div>
                    <div style="padding-top: 7px;">This collaborative program provides students with:</div>
                    <ul  style="list-style: inherit; margin-left: 14px;padding-top: 7px;">
                        <li style="font-weight: bold;">
                            Professional etiquette and 'soft skills' training 
                        </li>
                        <li>Senior-level faculty <span style="font-weight: bold;">support and assistance with job placement and program logistics</span></li>
                        <li><span style="font-weight: bold;">Get paid </span>while gaining real-world experience and class credits!</li>
                        <li><span style="font-weight: bold;">Resume-building </span>- FieldSolutions provides access to free training, discounted industry certifications, and experience working for the world's largest technology manufacturers and service companies</li>
                    </ul>

                </div>
            </div> 
            <div>
                <a target="_blank" href="http://www.fieldsolutions.com" >
                    <img  style="border:0px;width: 314px;" src="/widgets/images/learnmore_fs_devry.jpg" style="cursor: pointer;" />
                </a>
            </div>
        </td>
        <td valign="top" width="925px">
            <h2 style="font-size: 13px;color: #F69630;padding-top:33px;">About FieldSolutions</h2>
            <br>
            <div>
                <div style="float: left;">
                    FieldSolutions (<a target="_blank"  href="http://www.fieldsolutions.com" >www.FieldSolutions.com</a>) is a leader in technology field service
                    <br>and committed to expanding the work opportunities available to independent
                </div>
                <div style="float: left;margin-top: -13px;">
                    <a href="/techSignup.php?welcome=1&devry=1"><img style="border: 0px;" src="/widgets/images/devry_register_now.jpg" style="cursor: pointer;" />    </a>
                </div>
                <div style="clear: both;"></div>
                <div>
                    contractor technicians. Our on-line service management system and fast payment programs provide work to over 2000 technicians each week. FieldSolutions offers a performance rewards program and continuous training and skill development  opportunities. 
                </div>
                <div style="padding-top: 7px;">
                    FieldSolutions is dedicated to working with DeVry to teach and train the next generation of field service technicians. 
                </div>
                <br>
                <h2 style="font-size: 13px;color: #F69630;">What's in it for YOU?</h2>
                <div style="padding-top: 7px;">
                    Besides the benefit of gaining real-world experience in on-site technology service work, FieldSolutions provides you with a range of conveniences and efficiency tools designed to expedite the field service work.
                </div>
                <ul style="list-style: inherit; margin-left: 60px;padding-top: 7px;">
                    <li>Receive job alerts when work becomes available in your area</li>
                    <li>Sign up for free services like FieldSolutions' mobile apps that make you more efficient</li>
                    <li>Sign up for the FS-PlusOne Rewards program to earn cash bonuses for doing a great job as well as earning more skills and certifications</li>
                    <li>Complete your skills profile to get more work </li>
                    <li>Opportunities available for long term field service staffing</li>
                </ul>
                <div class="login_Top_center">
                    <div class="login_Top_right">
                        <div class="login_Top_left">
                            <br>
                            <div style="float: left;width:50%;padding-left: 43px;">
                                <h2 style="font-size: 13px;color: #F69630;">Get Started!</h2>
                                <div style="padding-top: 7px;">1) Register and create your Technician Profile</div>
                                <div>2) View Available Tech Jobs in your area</div>
                                <div>3) Apply for work you are qualified and interested in</div>
                            </div>
                            <div style="float: left;width:40%;padding-top: 10px;">
                                <div><a target="_blank" style="text-decoration: underline;" href="http://www.screencast.com/t/EabxqPsvm">Watch the Getting Started Tutorial</a></div>
                                <div style="padding-top: 7px;"><a id="cmdReadQuick" style="text-decoration: underline;" href="javascript:;">Read Quick Steps for Getting Work  </a></div>
                                <div style="padding-top: 7px;"><a id="cmdReadTips" style="text-decoration: underline;" href="javascript:;">Read Tips for Techs</a></div>
                                <div style="padding-top: 7px;"><a style="text-decoration: underline;" href="/techSignup.php?devry=1">REGISTER NOW</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </td>
    </tr>
</table>
<?php include ("template/readquicktemplate.php"); ?>
<?php include ("template/readtipstemplate.php"); ?>
<?php require ("../footer.php"); ?>

<script>
    var _login = login.CreateObject({id:"_login"});
</script>
