<?php
require_once("headerStartSession.php");
?>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">


<head>
<title>Field Solutions - Quality Field Sourcing</title>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<link rel="shortcut icon" href="/favicon.ico" />
<meta name="keywords" content="POS, Computer, Computer Repair, Technicians, Field Service, Retail Technicians, Home Computer Repair Technicians, Troubleshooting, PC Technicians" />
<meta name="description" content="Our focus is matching technicians seeking work with clients seeking technicians" />
<meta http-equiv="distribution" content="global" />
<meta http-equiv="copyright" content=" -2008" />
<meta http-equiv="url" content="http://www.fieldsolutions.com" />
<meta name="author" content="Gerald Bailey" />
<meta name="author" content="Collin McGarry" />
<meta name="author" content="Trung Ngo" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-2" />
<script src="/widgets/js/jquery.min.js"></script>
<script src="/widgets/js/jquery.appear.min.js"></script>
<script src="/library/dataValidation.js?v=3"></script>
<script src="/library/jquery/interface_floatingWindows.js"></script>
<script  src="/widgets/js/jquery.cookie.js"></script>
<script  src="/library/jquery/calendar/jquery-calendar.js"></script>
<script  src="/widgets/js/jquery.formatCurrency-1.4.0.js"></script>
<!--script type="text/javascript" src="/widgets/js/functions.js?07242012"></script-->
<?php script_nocache ("/widgets/js/functions.js"); ?>
<?php script_nocache ("/widgets/js/global.js"); ?>
<script type="text/javascript" src="/widgets/js/base64.js"></script>
<script type="text/javascript" src="/library/jquery/jquery.bt.min.js"></script>
<!--[if IE]><script src="/library/jquery/excanvas.js" type="text/javascript" charset="utf-8"></script><![endif]-->

<link rel="stylesheet" type ="text/css" href="/templates/www/floatingWindow.css" />
<link rel="stylesheet" href="/library/jquery/calendar/jquery-calendar.css" type="text/css" />
<link rel="stylesheet" type ="text/css" href="/templates/<?=$siteTemplate?>/main.css?24012011" />
<link rel="stylesheet" type="text/css" href="/widgets/css/main.css?04032011" />
<!-- link rel=stylesheet type ="text/css" href="https://www.fieldsolutions.com/wp/wp-content/themes/fieldsolutions/style.css" / -->
<link rel="stylesheet" type="text/css" href="/widgets/css/popup.css" />
<link rel="stylesheet" type="text/css" href="/widgets/css/details.css?22022011" />
<?php script_nocache ("/widgets/js/FSPopupAbstract.js"); ?>
<?php script_nocache ("/widgets/js/FSPopupRoll.js"); ?>
<?php script_nocache ("/widgets/js/FSWidget.js"); ?>


<?php 
    if (!empty($includeStyles)) {
        foreach ($includeStyles as $incStyle) :
        ?><link rel="stylesheet" type="text/css" href="<?=$incStyle?>" /><?php
        endforeach;
    }
    
?>


<!-- iPhone specific information -->
<!--<link rel="stylesheet" type="text/css" media="screen and (device-width:320px or device-width:480" href="/iphone.css")>-->
<!-- End iPhone specific information -->

<script type="text/javascript">
	var serverDateTime = "<?=$serverDateTime?>";
</script>
<script type="text/javascript" src="/widgets/js/tiny_mce/tiny_mce.js"></script>
<script>
var wd;             //  Widgets object
var roll;           //  Rollover object
var popup;          //  Rollover object
var progressBar;    //  Progress bar object

$(document).ready(function() {

    roll        = new FSPopupRoll();
    //progressBar = new ProgressBar();

    //progressBar.key('approve');
    //progressBar.adapter(new ProgressBarAdapterPopupSpin(progressBar, null));

});
</script> 
</head>
<script>
    var _global = global.CreateObject({id:"_global",PHP_SELF:'<?= $_SERVER['HTTP_REFERER'] ?>',company:'<?= empty($_REQUEST['v'])?$_SESSION['PM_Company_ID']:$_REQUEST['v'] ?>'});  
</script> 
