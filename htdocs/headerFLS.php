<?php
// Set site URL
$siteUrl = "http://fieldsolutions.com";

?>

<!doctype html public "-//W3C//DTD XHTML 1.0 Transitional//EN" "DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<title>Field Solutions, LLC - Quality Field Sourcing</title>
<meta name="keywords" content="POS, Computer, Computer Repair, Technicians, Field Service, Retail Technicians, Home Computer Repair Technicians, Troubleshooting, PC Technicians" />
<meta name="description" content="Our focus is matching technicians seeking work with clients seeking technicians" />
<meta http-equiv="distribution" content="global" />
<meta http-equiv="copyright" content=" -2008" />
<meta http-equiv="url" content="http://www.fieldsolutions.com" />
<meta name="author" content="Gerald Bailey" />
<meta name="author" content="Collin McGarry" />
<meta name="author" content="Trung Ngo" />

<script type="text/javascript" src="<?php echo $siteUrl;?>/library/jquery/jquery-1.2.1.pack.js"></script>

<link rel=stylesheet type ="text/css" href="<?php echo $siteUrl;?>/templates/fls/main.css" />


<!-- iPhone specific information -->

<!-- End iPhone specific information -->

</head>

<body>

<div id="mainContainer">

	<div id="headerSlider">
		<div id="headerRight" align="right">
			<div id="topNavBar">
			<ul>
			<li><a href="/index.php">Home</a></li>
			<li><a href="/content/aboutUs.php">About Us</a></li>
			<li><a href="/content/contactUs.php">Contact Us</a></li>
			<li><a href="http://MyTechnicianSpace.com" target="_blank">Tech Community</a></li>
			</ul>
			</div>
		</div>
		<div id="headerLogo"></div>
	</div>
	
	