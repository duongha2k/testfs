<?php
	if (!$argc)
		require_once(realpath(dirname(__FILE__) . "/../") . "/headerStartSession.php");
	require_once("caspioAPI.php");
	
	function createProjectTimeStamp($id, $user) {
		if (!is_numeric($id)) return;
/*		$projectInfo = caspioSelectAdv("TR_Client_Projects", "Project_Name, ReminderAll, ReminderAcceptance, Reminder24Hr, Reminder1Hr, CheckInCall, CheckOutCall, ReminderNotMarkComplete, ReminderIncomplete, Project_Company_ID, AutoSMSOnPublish, ACSApplyTo, PcntDeduct", "Project_ID = '$id'", "", false, "`", "|", false);
		if (!$projectInfo || $projectInfo[0] == "") return;

		$info = explode("|", $projectInfo[0]);
		$projectName = trim($info[0], "`");
		$reminderAll = (trim($info[1], "`") == "True" ? 1 : 0);
		$reminderAcceptance = (trim($info[2], "`") == "True" ? 1 : 0);
		$reminder24Hr = (trim($info[3], "`") == "True" ? 1 : 0);
		$reminder1Hr = (trim($info[4], "`") == "True" ? 1 : 0);
		$checkInCall = (trim($info[5], "`") == "True" ? 1 : 0);
		$checkOutCall = (trim($info[6], "`") == "True" ? 1 : 0);
		$reminderNotMarkComplete = (trim($info[7], "`") == "True" ? 1 : 0);
		$reminderIncomplete = (trim($info[8], "`") == "True" ? 1 : 0);
		$companyID = trim($info[9], "`");
		$SMSBlast = (trim($info[10], "`") == "True" ? 1 : 0);
		$applyTo = trim($info[11], "`");
		$pcntDeduct = (trim($info[12], "`") == "True" ? 1 : 0);
		if (!is_numeric($applyTo))
			$applyTo = "DEFAULT";
		else 
			$applyTo = "'$applyTo'";
			
		
		// project ACS timestamp
		caspioInsert("ProjectACSChangeLog", "DateChanged, Project_ID, Project_Name, UserName, ReminderAll, ReminderAcceptance, Reminder24Hr, Reminder1Hr, CheckInCall, CheckOutCall, ReminderNotMarkComplete, ReminderIncomplete, Company_ID, SMSBlast, ApplyTo, PcntDeduct", "GETDATE(), '$id', '". caspioEscape($projectName) . "', '$user', '$reminderAll', '$reminderAcceptance', '$reminder24Hr', '$reminder1Hr', '$checkInCall', '$checkOutCall', '$reminderNotMarkComplete', '$reminderIncomplete', '$companyID', '$SMSBlast', $applyTo, '$pcntDeduct'", false);
		return array("projectName" => $projectName, "reminderAll" => $reminderAll, "reminderAcceptance" => $reminderAcceptance, "reminder24Hr" => $reminder24Hr, "reminder1Hr" => $reminder1Hr, "checkInCall" => $checkInCall, "checkOutCall" => $checkOutCall, "reminderNotMarkComplete" => $reminderNotMarkComplete, "reminderIncomplete" => $reminderIncomplete, "SMSBlast" => $SMSBlast, "applyTo" => $applyTo, "pcntDeduct" => $pcntDeduct );*/
		return Core_TimeStamp::createProjectTimeStamp($id, $user);
	}
?>
