<?php
	set_time_limit(900);
	require_once("caspioAPI.php");
	require_once("smtpMail.php");
	require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/parts/manager.php');
	require_once($_SERVER['DOCUMENT_ROOT'] . '/includes/parts/Auth.php');

	/* Global WO Import Vars */	
	$errorCount = 0;
	$errorHtml = "";
	$warningHtml = "";
	
	function addPart($partEntry) {
		@$newPart = $partEntry["NewPart"];
		foreach ($newPart as $key => $val) {
			$newPart[$key] = htmlentities($val, ENT_QUOTES);
		}	
		if (empty($partEntry["WO_ID"])) return FALSE;
		$woId = $partEntry["WO_ID"];
		$result = caspioSelectAdv("Work_Orders", "TB_UNID", "WO_ID = '$woId'", "",false, "`", "|", false);
		if (!$result || $result[0] == "") return FALSE;
		$woId = $result[0];
		
		$consumable = !empty($newPart["Consumable"]) && $newPart["Consumable"] == "1" ? "True" : "False";
						
		@$xml='<PartEntry><Consumable>'.$consumable.'</Consumable><NewPart><PartNum>'.$newPart['PartNum'].'</PartNum><ModelNum>'.$newPart['ModelNum'].'</ModelNum>'
			.'<SerialNum>'.$newPart['SerialNum'].'</SerialNum><Description>'.$newPart['Description'].'</Description>'
			.'<Carrier>'.$newPart['Carrier'].'</Carrier><TrackingNum>'.$newPart['TrackingNum'].'</TrackingNum><ETA>'.$newPart['ETA'].'</ETA>'
			.'<ShipTo>'.$newPart['ShipTo'].'</ShipTo><ShippingAddress>'.$newPart['ShippingAddress'].'</ShippingAddress></NewPart>';
		if ($consumable=='False') {
			@$returnPart = $partEntry["ReturnPart"];
			foreach ($returnPart as $key => $val) {
				$returnPart[$key] = htmlentities($val, ENT_QUOTES);
			}	
			@$xml.='<ReturnPart><PartNum>'.$returnPart['PartNum'].'</PartNum><ModelNum>'.$returnPart['ModelNum'].'</ModelNum>'
			. '<SerialNum>'.$returnPart['SerialNum'].'</SerialNum><Description>'.$returnPart['Description'].'</Description>'
			. '<Carrier>'.$returnPart['Carrier'].'</Carrier><TrackingNum>'.$returnPart['TrackingNum'].'</TrackingNum><ETA>'.$returnPart['ETA'].'</ETA>'
			. '<RMA>'.$returnPart['RMA'].'</RMA><Instructions>'.$returnPart['Instructions'].'</Instructions></ReturnPart>';
		}
		$xml.='</PartEntry>';
					
		$auth = new PartsManagerAuth();
		$auth->setLogin($_SESSION['UserName'])
			 ->setRole(PartsManagerAuth::AUTH_CLIENT);
		$authData = $auth->getAuthData();
		$login = $authData['login'];
		$password = $authData['password'];

		$manager = new PartsManager();
		$manager->setWoId($woId)
				->setUser($authData['login'])
				->setHash($authData['password'])
				->setPmContext($_GET["v"])
				->setRole(PartsManager::PARTS_MANAGER_CLIENT);
		$result = $manager->createPart($xml);
				
	    $xml = simplexml_load_string($result);
				
		if (!empty($xml->error))
			return FALSE;
		
		return TRUE;

	}
	
	function generateError($msg) {
		global $errorHtml, $errorCount;
		$errorHtml .= "$msg<br/>";
		$errorCount++;
	}
	
	function addColumnIfNotPresent($columnName, &$importColumns, $columnMap) {
		if (!array_key_exists($columnName, $columnMap))
			$importColumns[] = $columnName;
	}
	
	function parseWOImport() {
		global $errorCount, $errorHtml, $FIELD_EXISTS, $warningHtml;
		$cachedMustEqual = array();
		if (isset($_POST["submit"])) {		
			// determine import type
			if ($_POST["importType"] == "FLS") {
				// Define FLS data validation
				$flsColumnMustEqual = array();
				$flsFieldPairValidator = array();
				$flsColumnMustEqual["Paid"] = "No";
				$flsColumnMustEqual["PayApprove"] = "0";
//				$flsColumnMustEqual["DateImported"] = array(date("m/d/Y"), date("n/j/Y"), date("m/j/Y"), date("n/d/Y"));
				$flsColumnMustEqual["DateImported"] = array(date("m/d/Y"));
				$flsColumnMustEqual["Lead"] = array("0", "1");
				$flsColumnMustEqual["Assist"] = array("0", "1");
				$flsColumnMustEqual["CallType"] = array("Service Call", "Install");
				//$flsColumnMustEqual["PayAmt"] = new patternMatch("/^(\d+)[.](\d{2})$/", "0.00");
//				$flsColumnMustEqual["PjctStartTime"] = new patternMatch("/^(\d{1,2}):(\d{2})\s(AM|PM)$/", "H:MM AM/PM");
				$flsColumnMustEqual["Zip"] = new patternMatch("/^.{4,}$/", "5 or more characters", true);

				$flsColumnMustEqual["Addedby"] = new patternMatch("/^.+$/", "Not Blank", false);

				$flsFieldPairValidator[] = new TwoFieldValidator("FLS_Projects", "ProjectName", "ProjectNo", "ProjectName", "Project_ID");

//				$flsFieldPairValidator["Project_ID"] = $flsFieldPairValidator["ProjectName"];			

				$tableName = "FLS_Work_Orders";
				$columnMustEqual = $flsColumnMustEqual;
				$columnFieldPairValidator = $flsFieldPairValidator;
			}
			else {
			// Define main site data validation
//				$partClient = new SoapClient("https://api.fieldsolutions.com:417/api/client/wsdl", array('cache_wsdl'=>WSDL_CACHE_NONE));
				$mainColumnMustEqual = array();
				$mainFieldPairValidator = array();

				$mainColumnMustEqual["Shipped_By"] = array("FedEx", "UPS", "USPS", "DHL");
				$mainColumnMustEqual["Return_Part_Shipped_By"] = $mainColumnMustEqual["Shipped_By"];
				$mainColumnMustEqual["Part_ShipTo"] = array("Site", "Tech", "Other");
//				$mainColumnMustEqual["Return_Part_ShipTo"] = $mainColumnMustEqual["Shipped_By"];

				$mainColumnMustEqual["Approved"] = "0";
				$mainColumnMustEqual["Invoiced"] = "0";
				$mainColumnMustEqual["TechPaid"] = "0";
				$mainColumnMustEqual["Company_Name"] = $FIELD_EXISTS;
				$mainColumnMustEqual["WO_Category"] = $FIELD_EXISTS;
				$mainColumnMustEqual["WO_ID"] = $FIELD_EXISTS;
//				$mainColumnMustEqual["Company_ID"] = $FIELD_EXISTS;
				$mainColumnMustEqual["Company_ID"] = $_GET["v"];
				$mainColumnMustEqual["Type_ID"] = array("1", "2", "3");
				$mainColumnMustEqual["Amount_Per"] = array("Site", "Hour", "Device");
				$mainColumnMustEqual["StartTime"] = new patternMatch("/^(\d{1,2}):(\d{2})\s(AM|PM)$/", "H:MM AM/PM", true);
				$mainColumnMustEqual["EndTime"] = $mainColumnMustEqual["StartTime"];
				//$mainColumnMustEqual["Tech_Bid_Amount"] = new patternMatch("/^(\d+)[.](\d{2})$/", "0.00");
//				$blank = new patternMatch("/^$/", "Blank");
//				ini_set("display_errors", 1);
				$globalPattern["NOT_BLANK"] = new patternMatch("/^.+$/", "Not Blank", false);
				$globalPattern["MONEY_GREATER_THAN_ZERO"] = new patternMatch("/^(?(?!0[.]00)(\d+)[.](\d{2}))$/", "0.00 and Greater Than Zero", false);
				$globalPattern["WHOLE_NUMBER_GREATER_THAN_ZERO"] = new patternMatch("/^(?!0+)(\d+)$/", "Whole Number Greater Than Zero", false);
				$globalPattern["AMOUNT_PER_DEVICE"] = new patternMatch("/^Device$/", "'Device'", false);
//				$globalPattern["PHONE_WITH_OPTIONAL_EXTENSION"] = new patternMatch("(/^[(]?(\d{3})[)]?[^0-9]*(\d{3})[^0-9]*(\d{4})$/|/^[(]?(\d{3})[)]?[^0-9]*(\d{3})[^0-9]*(\d{4})[^0-9]+([0-9]*)[^0-9]*([0-9]*)$/)", "###-###-#### or ###-###-#### x ####", false);
				$globalPattern["PHONE_WITH_OPTIONAL_EXTENSION"] = new patternMatch("/^([(]?(\d{3})[)]?[^0-9]*(\d{3})[^0-9]*(\d{4})|[(]?(\d{3})[)]?[^0-9]*(\d{3})[^0-9]*(\d{4})[^0-9]+([0-9]*)[^0-9]*([0-9]*))$/", "###-###-#### or ###-###-#### x ####", TRUE);
				$globalPattern["PHONE_WITHOUT_EXTENSION"] = new patternMatch("/^[(]?(\d{3})[)]?[^0-9]*(\d{3})[^0-9]*(\d{4})$/", "###-###-####", TRUE);
				$globalPattern["SITE_PHONE_STRICT_WITH_OPTIONAL_EXTENSION"] = new patternMatch("/^([(](\d{3})[)]\s(\d{3})[-](\d{4})|[(](\d{3})[)]\s(\d{3})[-](\d{4})[^0-9]+[0-9]*)$/", "(###) ###-#### or (###) ###-#### x ####", true);

				$globalPattern["BOOLEAN_TRUE"] = new patternMatch("/^1$/", "equal to '1'", false);

				$mainColumnMustEqual["SitePhone"] = $globalPattern["SITE_PHONE_STRICT_WITH_OPTIONAL_EXTENSION"];
				$mainColumnMustEqual["PayMax"] = $globalPattern["MONEY_GREATER_THAN_ZERO"];
				$mainColumnMustEqual["Zipcode"] = new patternMatch("/^.{4,}$/", "5 or more characters", true);
				$mainColumnMustEqual["Headline"] = $globalPattern["NOT_BLANK"];
				$mainColumnMustEqual["ProjectManagerPhone"] = $globalPattern["PHONE_WITH_OPTIONAL_EXTENSION"];
				$mainColumnMustEqual["ResourceCoordinatorPhone"] = $globalPattern["PHONE_WITH_OPTIONAL_EXTENSION"];
				$mainColumnMustEqual["EmergencyPhone"] = $globalPattern["PHONE_WITHOUT_EXTENSION"];
				$mainColumnMustEqual["TechnicalSupportPhone"] = $globalPattern["PHONE_WITH_OPTIONAL_EXTENSION"];
				$mainColumnMustEqual["CheckInOutNumber"] = $globalPattern["PHONE_WITH_OPTIONAL_EXTENSION"];
				
				// if tech ID is not blank then Tech_Bid_Amount must be money value > 0
				$mainFieldPairValidator[] = new CaseThenValidator("isProjectAutoAssign", "StartDate", array("\$globalPattern[\"BOOLEAN_TRUE\"]" => $globalPattern["NOT_BLANK"]));
				$mainFieldPairValidator[] = new CaseThenValidator("isProjectAutoAssign", "StartTime", array("\$globalPattern[\"BOOLEAN_TRUE\"]" => $globalPattern["NOT_BLANK"]));
				$mainFieldPairValidator[] = new CaseThenValidator("isProjectAutoAssign", "EndDate", array("\$globalPattern[\"BOOLEAN_TRUE\"]" => $globalPattern["NOT_BLANK"]));
				$mainFieldPairValidator[] = new CaseThenValidator("isProjectAutoAssign", "EndTime", array("\$globalPattern[\"BOOLEAN_TRUE\"]" => $globalPattern["NOT_BLANK"]));
				$mainFieldPairValidator[] = new CaseThenValidator("isProjectAutoAssign", "City", array("\$globalPattern[\"BOOLEAN_TRUE\"]" => $globalPattern["NOT_BLANK"]));
				$mainFieldPairValidator[] = new CaseThenValidator("isProjectAutoAssign", "State", array("\$globalPattern[\"BOOLEAN_TRUE\"]" => $globalPattern["NOT_BLANK"]));
				$mainFieldPairValidator[] = new CaseThenValidator("isProjectAutoAssign", "Zipcode", array("\$globalPattern[\"BOOLEAN_TRUE\"]" => $globalPattern["NOT_BLANK"]));

				$mainFieldPairValidator[] = new CaseThenValidator("Tech_ID", "Tech_Bid_Amount", array("\$globalPattern[\"NOT_BLANK\"]" => $globalPattern["MONEY_GREATER_THAN_ZERO"]));
				$mainFieldPairValidator[] = new CaseThenValidator("FLS_ID", "Tech_Bid_Amount", array("\$globalPattern[\"NOT_BLANK\"]" => $globalPattern["MONEY_GREATER_THAN_ZERO"]));
//				$mainFieldPairValidator["Tech_Bid_Amount"] = $mainFieldPairValidator["Tech_ID"];
				
				$mainFieldPairValidator[] = new CaseThenValidator("Amount_Per", "Qty_Devices", array("\$globalPattern[\"AMOUNT_PER_DEVICE\"]" => $globalPattern["WHOLE_NUMBER_GREATER_THAN_ZERO"]));

				$mainFieldPairValidator[] = new CaseThenValidator("TripCharge", "Add_Pay_AuthBy", array("\$globalPattern[\"NOT_BLANK\"]" => $globalPattern["NOT_BLANK"]));
				$mainFieldPairValidator[] = new CaseThenValidator("TripCharge", "Add_Pay_Reason", array("\$globalPattern[\"NOT_BLANK\"]" => $globalPattern["NOT_BLANK"]));

				$mainFieldPairValidator[] = new CaseThenValidator("MileageReimbursement", "Add_Pay_AuthBy", array("\$globalPattern[\"NOT_BLANK\"]" => $globalPattern["NOT_BLANK"]));
				$mainFieldPairValidator[] = new CaseThenValidator("MileageReimbursement", "Add_Pay_Reason", array("\$globalPattern[\"NOT_BLANK\"]" => $globalPattern["NOT_BLANK"]));

				$mainFieldPairValidator[] = new CaseThenValidator("MaterialsReimbursement", "Add_Pay_AuthBy", array("\$globalPattern[\"NOT_BLANK\"]" => $globalPattern["NOT_BLANK"]));
				$mainFieldPairValidator[] = new CaseThenValidator("MaterialsReimbursement", "Add_Pay_Reason", array("\$globalPattern[\"NOT_BLANK\"]" => $globalPattern["NOT_BLANK"]));

				$mainFieldPairValidator[] = new CaseThenValidator("Additional_Pay_Amount", "Add_Pay_AuthBy", array("\$globalPattern[\"NOT_BLANK\"]" => $globalPattern["NOT_BLANK"]));
				$mainFieldPairValidator[] = new CaseThenValidator("Additional_Pay_Amount", "Add_Pay_Reason", array("\$globalPattern[\"NOT_BLANK\"]" => $globalPattern["NOT_BLANK"]));
				
				$authPayAutoFillColumns = array("TripCharge", "MileageReimbursement", "MaterialsReimbursement", "Additional_Pay_Amount");
				$authPayAutoFillColumns = array_combine($authPayAutoFillColumns, $authPayAutoFillColumns);

				$mainFieldPairValidator[] = new TwoFieldValidator("TR_Client_List", "CompanyName", "Company_ID", "Company_Name", "Company_ID");
//				$mainFieldPairValidator["Company_ID"] = $mainFieldPairValidator["Company_Name"];
				$mainFieldPairValidator[] = new TwoFieldValidator("TR_Client_Projects", "Project_ID", "Project_Name", "Project_ID", "Project_Name");
				$mainFieldPairValidator[] = new TwoFieldValidator("WO_Categories", "Category_ID", "Category", "WO_Category_ID", "WO_Category");
//				$mainFieldPairValidator["Project_Name"] = $mainFieldPairValidator["Project_ID"];

				$tableName = "Work_Orders";
				$columnMustEqual = $mainColumnMustEqual;
				$columnFieldPairValidator = $mainFieldPairValidator;				
			}
						
						
			if (!isset($_FILES["importFile"]["tmp_name"]) || !file_exists($_FILES["importFile"]["tmp_name"])) {
				echo "No file uploaded";
				die();
			}
	
			$columns = caspioGetTableDesign($tableName, FALSE);
			
			$tableColumns = array();
			foreach ($columns as $info) {
			$emailCount = 0;
				$fieldInfo = explode(",",$info);
				$fieldName = $fieldInfo[0];
				$fieldType = $fieldInfo[1];
				$fieldUnique = $fieldInfo[2];
				$tableColumns[$fieldName] = new caspioFieldProperties($fieldType, $fieldUnique);
			}
			
			if ($_FILES["importFile"]["size"] > 1500000) {
				// file too large
				echo "File too large (limit 1.5 MB)";
				die();
			}
				
			$handle = fopen($_FILES["importFile"]["tmp_name"], "r");
			$importColumns = fgetcsv($handle);
			
			$ignoreColumnList = array();
			if ($_POST["importType"] != "FLS") {
				$newPartColumnMap = array("Part_Consumable" => "Consumable", "Part_No" => "PartNum", "Part_ModelNum" => "ModelNum", "Part_SerialNum" => "SerialNum", "Part_Desciption" => "Description", "Part_Description" => "Description", "Shipped_By" => "Carrier", "Tracking_No" =>  "TrackingNum", "Part_ETA" => "ETA", "Part_ShipTo" => "ShipTo", "Part_ShippingAddress" => "ShippingAddress");
				$returnPartColumnMap = array("Return_Part_No" => "PartNum", "Return_Part_ModelNum" => "ModelNum", "Return_Part_SerialNum" => "SerialNum", "Return_Part_Description" => "Desciption", "Return_Part_Description" => "Description", "Return_Part_Shipped_By" => "Carrier", "Return_Part_Tracking_No" =>  "TrackingNum", "Return_Part_ETA" => "ETA", "RMA_No" => "RMA", "Return_Part_Instructions" => "Instructions");
				$ignoreColumnList = array_merge($newPartColumnMap, $returnPartColumnMap);
				
				foreach ($importColumns as $key => $val) {
					// get rid of extra spaces on column names
					$importColumns[$key] = trim($val);
				}
				// set up project info autofill
				$columnMap = array_flip($importColumns);
								
				// fix old column name
				if (array_key_exists("CheckInNumber", $columnMap)) {
					$importColumns[$columnMap["CheckInNumber"]] = "CheckInOutNumber";
					$columnMap["CheckInOutNumber"] = $columnMap["CheckInNumber"];
				}
								
				// create columns if it doesn't exist
				addColumnIfNotPresent("DateEntered", $importColumns, $columnMap);
				addColumnIfNotPresent("Username", $importColumns, $columnMap);
				
				if ($_GET["v"] == "FLS") {
					addColumnIfNotPresent("SourceByDate", $importColumns, $columnMap);
					addColumnIfNotPresent("ShortNotice", $importColumns, $columnMap);
				}

				if (!array_key_exists("ProjectManagerName", $columnMap))
					$importColumns[] = "ProjectManagerName";
				if (!array_key_exists("ProjectManagerPhone", $columnMap))
					$importColumns[] = "ProjectManagerPhone";
				if (!array_key_exists("ProjectManagerEmail", $columnMap))
					$importColumns[] = "ProjectManagerEmail";
				
				if (!array_key_exists("ResourceCoordinatorName", $columnMap))
					$importColumns[] = "ResourceCoordinatorName";
				if (!array_key_exists("ResourceCoordinatorPhone", $columnMap))
					$importColumns[] = "ResourceCoordinatorPhone";
				if (!array_key_exists("ResourceCoordinatorEmail", $columnMap))
					$importColumns[] = "ResourceCoordinatorEmail";

				if (!array_key_exists("EmergencyName", $columnMap))
					$importColumns[] = "EmergencyName";
				if (!array_key_exists("EmergencyPhone", $columnMap))
					$importColumns[] = "EmergencyPhone";
				if (!array_key_exists("EmergencyEmail", $columnMap))
					$importColumns[] = "EmergencyEmail";

				if (!array_key_exists("TechnicalSupportName", $columnMap))
					$importColumns[] = "TechnicalSupportName";
				if (!array_key_exists("TechnicalSupportPhone", $columnMap))
					$importColumns[] = "TechnicalSupportPhone";
				if (!array_key_exists("TechnicalSupportEmail", $columnMap))
					$importColumns[] = "TechnicalSupportEmail";
					
				if (!array_key_exists("CheckInOutName", $columnMap))
					$importColumns[] = "CheckInOutName";
				if (!array_key_exists("CheckInOutNumber", $columnMap))
					$importColumns[] = "CheckInOutNumber";
				if (!array_key_exists("CheckInOutEmail", $columnMap))
					$importColumns[] = "CheckInOutEmail";
					

				if (!array_key_exists("ReminderAll", $columnMap))
					$importColumns[] = "ReminderAll";
				if (!array_key_exists("ReminderAcceptance", $columnMap))
					$importColumns[] = "ReminderAcceptance";
				if (!array_key_exists("SMSBlast", $columnMap))
					$importColumns[] = "SMSBlast";
				if (!array_key_exists("Reminder24Hr", $columnMap))
					$importColumns[] = "Reminder24Hr";
				if (!array_key_exists("Reminder1Hr", $columnMap))
					$importColumns[] = "Reminder1Hr";
				if (!array_key_exists("CheckInCall", $columnMap))
					$importColumns[] = "CheckInCall";
				if (!array_key_exists("CheckOutCall", $columnMap))
					$importColumns[] = "CheckOutCall";
				if (!array_key_exists("ReminderNotMarkComplete", $columnMap))
					$importColumns[] = "ReminderNotMarkComplete";
				if (!array_key_exists("ReminderIncomplete", $columnMap))
					$importColumns[] = "ReminderIncomplete";
					
				addColumnIfNotPresent("isProjectAutoAssign", $importColumns, $columnMap);
				addColumnIfNotPresent("MinutesRemainPublished", $importColumns, $columnMap);
				addColumnIfNotPresent("isWorkOrdersFirstBidder", $importColumns, $columnMap);
				addColumnIfNotPresent("P2TPreferredOnly", $importColumns, $columnMap);
				addColumnIfNotPresent("MinimumSelfRating", $importColumns, $columnMap);
				addColumnIfNotPresent("MaximumAllowableDistance", $importColumns, $columnMap);
				addColumnIfNotPresent("Date_Assigned", $importColumns, $columnMap);
				
				addColumnIfNotPresent("SignOffSheet_Required", $importColumns, $columnMap);
					
				addColumnIfNotPresent("PayMax", $importColumns, $columnMap);
				addColumnIfNotPresent("Amount_Per", $importColumns, $columnMap);

				addColumnIfNotPresent("Tech_Bid_Amount", $importColumns, $columnMap);
				addColumnIfNotPresent("PcntDeduct", $importColumns, $columnMap);
				
				$needsAuth = false;
				foreach ($authPayAutoFillColumns as $val) {
					if (array_key_exists($val, $columnMap))
						$needsAuth = true;
				}
				
				if ($needsAuth) {
					addColumnIfNotPresent("Add_Pay_AuthBy", $importColumns, $columnMap);
					addColumnIfNotPresent("Add_Pay_Reason", $importColumns, $columnMap);
				}
				
				$columnMap = array_flip($importColumns);
					
				$autofillColumns = array("Project_ID" => $columnMap["Project_ID"], 
				"ProjectManagerName" => $columnMap["ProjectManagerName"], 
				"ProjectManagerPhone" => $columnMap["ProjectManagerPhone"], 
				"ProjectManagerEmail" => $columnMap["ProjectManagerEmail"],
				
				"ResourceCoordinatorName" => $columnMap["ResourceCoordinatorName"], 
				"ResourceCoordinatorPhone" => $columnMap["ResourceCoordinatorPhone"], 
				"ResourceCoordinatorEmail" => $columnMap["ResourceCoordinatorEmail"], 
				
				"EmergencyName" => $columnMap["EmergencyName"], 
				"EmergencyPhone" => $columnMap["EmergencyPhone"], 
				"EmergencyEmail" => $columnMap["EmergencyEmail"], 
				
				"TechnicalSupportName" => $columnMap["TechnicalSupportName"], 
				"TechnicalSupportPhone" => $columnMap["TechnicalSupportPhone"], 
				"TechnicalSupportEmail" => $columnMap["TechnicalSupportEmail"], 

				"CheckInOutName" => $columnMap["CheckInOutName"], 
				"CheckInOutNumber" => $columnMap["CheckInOutNumber"], 
				"CheckInOutEmail" => $columnMap["CheckInOutEmail"],
				
				"ReminderAll" => $columnMap["ReminderAll"], 
				"ReminderAcceptance" => $columnMap["ReminderAcceptance"], 
				"SMSBlast" => $columnMap["SMSBlast"], 
				"Reminder24Hr" => $columnMap["Reminder24Hr"], 
				"Reminder1Hr" => $columnMap["Reminder1Hr"], 
				"CheckInCall" => $columnMap["CheckInCall"], 
				"CheckOutCall" => $columnMap["CheckOutCall"], 
				"ReminderNotMarkComplete" => $columnMap["ReminderNotMarkComplete"], 
				"ReminderIncomplete" => $columnMap["ReminderIncomplete"],
				
				"isProjectAutoAssign"  => $columnMap["isProjectAutoAssign"],
				"MinutesRemainPublished"  => $columnMap["MinutesRemainPublished"],
				"isWorkOrdersFirstBidder"  => $columnMap["isWorkOrdersFirstBidder"],
				"P2TPreferredOnly"  => $columnMap["P2TPreferredOnly"],
				"MinimumSelfRating"  => $columnMap["MinimumSelfRating"],
				"MaximumAllowableDistance"  => $columnMap["MaximumAllowableDistance"],
				"SignOffSheet_Required"  => $columnMap["SignOffSheet_Required"],
				
				"PayMax" => $columnMap["PayMax"],
				"Amount_Per" => $columnMap["Amount_Per"],
				
				"Tech_Bid_Amount" => $columnMap["Tech_Bid_Amount"],
				
				"FixedBid" => "LookupOnly",
				"PcntDeduct"  => $columnMap["PcntDeduct"]
				);
				
				$forcedColumns = array("Project_ID" => false, 
				"ProjectManagerName" => false, 
				"ProjectManagerPhone" => false, 
				"ProjectManagerEmail" => false,
				
				"ResourceCoordinatorName" => false, 
				"ResourceCoordinatorPhone" => false, 
				"ResourceCoordinatorEmail" => false, 
			
				"EmergencyName" => false, 
				"EmergencyPhone" => false, 
				"EmergencyEmail" => false, 
				
				"TechnicalSupportName" => false, 
				"TechnicalSupportPhone" => false, 
				"TechnicalSupportEmail" => false, 

				"CheckInOutName" => false, 
				"CheckInOutNumber" => false, 
				"CheckInOutEmail" => false,
				
				"ReminderAll" => true,
				"ReminderAcceptance" => true, 
				"SMSBlast" => true, 
				"Reminder24Hr" => true, 
				"Reminder1Hr" => true, 
				"CheckInCall" => true, 
				"CheckOutCall" => true, 
				"ReminderNotMarkComplete" => true, 
				"ReminderIncomplete" => true,

				"isProjectAutoAssign" => true,
				"MinutesRemainPublished" => true,
				"isWorkOrdersFirstBidder" => true,
				"P2TPreferredOnly" => true,
				"MinimumSelfRating" => true,
				"MaximumAllowableDistance" => true,
				
				"SignOffSheet_Required" => false,
				
				"PayMax" => "FixedBid",
				"Amount_Per" => "FixedBid",
				
				"Tech_Bid_Amount" => "FixedBid",
				
				"PcntDeduct" => true
				);
				
				$projectInfo = new ProjectInfoAutofill($autofillColumns, $forcedColumns);
			}
						
			// match import file columns to table columns and clean-up import column names
			$importColumnNum = sizeof($importColumns);
			
			$htmlTable = "<table cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;</td>";
			$columnList = array();
			for ($i = 0; $i < $importColumnNum; $i++) {
				$importColumns[$i] = trim($importColumns[$i]);
/*				if ($importColumns[$i] == "CheckInNumber")
					$importColumns[$i] = "CheckInOutNumber";*/
					
				$name = $importColumns[$i];
							
				if (!array_key_exists($name, $tableColumns) && !array_key_exists($name, $ignoreColumnList)) {
					$htmlTable .= "<td class=\"errorCell\" title=\"Unrecognized column: $name\"><a name=\"$errorCount\">$name</a></td>";
					generateError("Unrecognized column: <a href=\"#$errorCount\">$name</a>");
				}
				else if (array_key_exists($name, $columnList)) {
					// duplicate column
					$htmlTable .= "<td class=\"errorCell\" title=\"Duplicate column: $name\"><a name=\"$errorCount\">$name</a></td>";
					generateError("Duplicate column: <a href=\"#$errorCount\">$name</a>");
				}
				else {
					$htmlTable .= "<td>$name</td>";
					$columnList[$name] = 1;
				}
				foreach ($columnFieldPairValidator as $val)
				// initialize with index of columns
					$val->setIndex($name, $i);
			}
	
			$htmlTable .= "</tr>";
			
			checkMissingMustEqualColumns($columnMustEqual, $importColumns, $ignoreColumnList);	
			
			$currRow = 1;
			$woidList = array();
			$insertValues = array();
			$emailList = array();

			$projectList = array();
			$myCompanyName = "";
						
			$partsList = array();
			echo "<br/>checking ... ";
			while (!feof($handle)) {
				$importValues = fgetcsv($handle);
				if (sizeof($importValues) == 1) {
					// blank row not at end of file
					if (!feof($handle)) {
						$htmlTable .= "<tr><td>$currRow</td><td class=\"errorCell\"colspan=\"$importColumnNum\" title=\"Row is blank\"><a name=\"$errorCount\">&nbsp;</a></td>";
						generateError("Row <a href=\"#$errorCount\">$currRow</a>: Row is blank");
					}
					$currRow++;
					continue;
				}
				
				$importValuesNum = sizeof($importValues);
				echo "$currRow ... ";
				ob_flush();
				flush();

				for ($i = $importValuesNum; $i < $importColumnNum; $i++) {
					// fill in blank values
					$importValues[$i] = "DEFAULT";
				}
				
				$partEntry = NULL;
				$importValuesNum = sizeof($importValues);

				$htmlTable .= "<tr><td>$currRow</td>";
		
				if ($_POST["importType"] != "FLS") {
					// auto fill columns
					$importValues[$columnMap["DateEntered"]] = date("m/d/y H:i:s");
					$importValues[$columnMap["Username"]] = $_SESSION['UserName'];
					
	
					foreach ($authPayAutoFillColumns as $val) {
						if (!array_key_exists($val, $columnMap)) continue;
						if (!empty($importValues[$columnMap[$val]])) {
							$importValues[$columnMap["Add_Pay_AuthBy"]] = $_SESSION['UserName'];
							$importValues[$columnMap["Add_Pay_Reason"]] = "from import";
							break;
						}
						else {
							$importValues[$columnMap["Add_Pay_AuthBy"]] = "";
							$importValues[$columnMap["Add_Pay_Reason"]] = "";
						}
					}


					if ($_GET["v"] == "FLS") {
						if (!empty($importValues[$columnMap["StartDate"]])) {
							$numWeekDays = FLSCalculation::numberWeekDays(date("m/d/Y"), $importValues[$columnMap["StartDate"]]);
							$importValues[$columnMap["ShortNotice"]] = $numWeekDays <= 5 ? 1 : 0;
								
							if ($importValues[$columnMap["Type_ID"]] == 1) {
								$startDateTS = strtotime(date("m/d/Y"));
								$endDateTS = strtotime($importValues[$columnMap["StartDate"]]);
	
								$importValues[$columnMap["SourceByDate"]] = date("m/d/Y", FLSCalculation::calculateSourceByDate($startDateTS, $endDateTS, $importValues[$columnMap["DateEntered"]], $numWeekDays));
							}
							else {
								$importValues[$columnMap["SourceByDate"]] = "";
							}
						}
						else {
							$importValues[$columnMap["ShortNotice"]] = 0;
							$importValues[$columnMap["SourceByDate"]] = "";
						}
					}

					// auto fill from project
					$importValues = $projectInfo->autofillColumns($importValues);

					if ($importValues[$columnMap["Tech_ID"]] != "" || !empty($importValues[$columnMap["FLS_ID"]])) {
						$importValues[$columnMap["Date_Assigned"]] = date("m/d/y H:i:s");
					}
					else {
						$importValues[$columnMap["Tech_Bid_Amount"]] = "";
						$importValues[$columnMap["Date_Assigned"]] = "";
					}
				}
								
/*				foreach ($columnFieldPairValidator as $val) {
					$val->resetValue();
				}*/
																
				for ($i = 0; $i < $importColumnNum; $i++) {
					if ($i >= $importValuesNum) {
						// number columns more then number of values
						$importValues[$i] = "DEFAULT";
						$htmlTable .= "<td>&nbsp;</td>";
						continue;
					}
					
					$newPartColumnExists = array_key_exists($importColumns[$i], $newPartColumnMap);
					$returnPartColumnExists = array_key_exists($importColumns[$i], $returnPartColumnMap);
					if ($newPartColumnExists || $returnPartColumnExists) {
						if ($partEntry == NULL)
							$partEntry = array();
						if ($newPartColumnExists) {
							// map old parts fields to parts manager
							$column = $newPartColumnMap[$importColumns[$i]];
							if (!isset($partEntry["NewPart"]))
								$partEntry["NewPart"] = array();
							$partEntry["NewPart"][$column] = $importValues[$i];
						}
						else {
							$column = $returnPartColumnMap[$importColumns[$i]];
							if (!isset($partEntry["ReturnPart"]))
								$partEntry["ReturnPart"] = array();
							$partEntry["ReturnPart"][$column] = $importValues[$i];
						}
							
						$output = ($importValues[$i] == "" ? "&nbsp;" : $importValues[$i]);
						if (!checkMustEqual($importColumns[$i], $importValues[$i], $columnMustEqual)) {
							// verify any must equal values
							if (is_object($columnMustEqual[$importColumns[$i]])) {
								$columnPattern = $columnMustEqual[$importColumns[$i]];
								$htmlTable .= "<td class=\"errorCell\" title=\"{$importColumns[$i]} must be in this format: " . $columnPattern->getDescription() . "\"><a name=\"$errorCount\">$output</a></td>";
	
								generateError("Row <a href=\"#$errorCount\">$currRow</a>: {$importColumns[$i]} must be in this format: " . $columnPattern->getDescription());							
							}
							else if (is_array($columnMustEqual[$importColumns[$i]])) {
								$validValues = (array_key_exists($importColumns[$i], $cachedMustEqual) ? $cachedMustEqual[$importColumns[$i]] : $cachedMustEqual[$importColumns[$i]] = implode(" OR ", $columnMustEqual[$importColumns[$i]]));
								$htmlTable .= "<td class=\"errorCell\" title=\"{$importColumns[$i]} must equal $validValues\"><a name=\"$errorCount\">$output</a></td>";
								generateError("Row <a href=\"#$errorCount\">$currRow</a>: {$importColumns[$i]} must equal $validValues");
							}
							else {
								$htmlTable .= "<td class=\"errorCell\" title=\"{$importColumns[$i]} must equal {$columnMustEqual[$importColumns[$i]]}\"><a name=\"$errorCount\">$output</a></td>";
								generateError("Row <a href=\"#$errorCount\">$currRow</a>: {$importColumns[$i]} must equal {$columnMustEqual[$importColumns[$i]]}");
							}
						}
						else
							$htmlTable .= "<td>$output</td>";

						unset($importValues[$i]);
						continue;
					}

					if (!array_key_exists($importColumns[$i], $tableColumns)) {
						// non-matching column - don't check values
						$htmlTable .= "<td>{$importValues[$i]}</td>";
						continue;
					}
					
					$column = $tableColumns[$importColumns[$i]];					
					
					// auto format
					//FORMAT DOLLAR AMOUNTS
					
					if (($importColumns[$i] == "Tech_Bid_Amount" || $importColumns[$i] == "PayMax") && !empty($importValues[$i])) {
						$formatted_Amount = number_format($importValues[$i], 2, ".", "");
						$importValues[$i] = $formatted_Amount;
					}
					
					// end auto format
							
					$output = ($importValues[$i] == "" ? "&nbsp;" : $importValues[$i]);

					$twoFieldError = false;
					foreach ($columnFieldPairValidator as $key => $val) {
						// run two field validators if both fields ready
						$twoFieldReady = $val->readyToValidate($i);
//						echo "{$importColumns[$i]} - " . ($twoFieldReady ? "TRUE" : "FALSE") . "<br/>";
						if (!$twoFieldReady) continue;
						$validator = $val;
						if (!$validator->validate($importValues, $globalPattern)) {
							if (!$twoFieldError)
								$htmlTable .= "<td class=\"errorCell\"><a name=\"$errorCount\">$output</a></td>";
							$twoFieldError = true;
							$validator->generateErrorMsg($currRow, $errorCount, $importValues, $globalPattern);
						}
					}
															
					if ($twoFieldError) {
					 continue;
					}
					else if (!checkMustEqual($importColumns[$i], $importValues[$i], $columnMustEqual)) {
						// verify any must equal values
						if (is_object($columnMustEqual[$importColumns[$i]])) {
							$columnPattern = $columnMustEqual[$importColumns[$i]];
							$htmlTable .= "<td class=\"errorCell\" title=\"{$importColumns[$i]} must be in this format: " . $columnPattern->getDescription() . "\"><a name=\"$errorCount\">$output</a></td>";

							generateError("Row <a href=\"#$errorCount\">$currRow</a>: {$importColumns[$i]} must be in this format: " . $columnPattern->getDescription());							
						}
						else if (is_array($columnMustEqual[$importColumns[$i]])) {
							$validValues = (array_key_exists($importColumns[$i], $cachedMustEqual) ? $cachedMustEqual[$importColumns[$i]] : $cachedMustEqual[$importColumns[$i]] = implode(" OR ", $columnMustEqual[$importColumns[$i]]));
							$htmlTable .= "<td class=\"errorCell\" title=\"{$importColumns[$i]} must equal $validValues\"><a name=\"$errorCount\">$output</a></td>";
							generateError("Row <a href=\"#$errorCount\">$currRow</a>: {$importColumns[$i]} must equal $validValues");
						}
						else {
							$htmlTable .= "<td class=\"errorCell\" title=\"{$importColumns[$i]} must equal {$columnMustEqual[$importColumns[$i]]}\"><a name=\"$errorCount\">$output</a></td>";
							generateError("Row <a href=\"#$errorCount\">$currRow</a>: {$importColumns[$i]} must equal {$columnMustEqual[$importColumns[$i]]}");
						}
					}
					else if (!$column->checkValue($importValues[$i])) {
						// verify data type correctness
						generateError("Row <a href=\"#$errorCount\">$currRow</a>: {$importColumns[$i]} datatype mismatch (" . $column->getType() . ")");
						$htmlTable .= "<td class=\"errorCell\" title=\"{$importColumns[$i]} datatype mismatch (" . $column->getType() . ")\"><a name=\"$errorCount\">$output</a></td>";
					}
					else {
						// Main site duplicate WO_ID
						if ($importColumns[$i] == "WO_ID") {
							if (array_key_exists($importValues[$i], $woidList)) {
								generateError("Row <a href=\"#$errorCount\">$currRow</a>: WO_ID found more than once ($importValues[$i])");
								$htmlTable .= "<td class=\"errorCell\" title=\"WO_ID found more than once\"><a name=\"$errorCount\">$output</a></td>";
							}
							else {
								$woidList[$importValues[$i]] = $currRow;
								$htmlTable .= "<td>$output</td>";
							}
						}
						else {
							$htmlTable .= "<td>$output</td>";
						}
					}
//					if ($importColumns[$i] == "Tools") {
						$importValues[$i] = ($importValues[$i] == "" ? "DEFAULT" : "'" . mb_convert_encoding(caspioEscape($importValues[$i]), "UTF-8") . "'");
/*					}


					else {
						$importValues[$i] = ($importValues[$i] == "" ? "NULL" : "'" . caspioEscape($importValues[$i]) . "'");
					}*/

					if ($importColumns[$i] == "Project_Name" || $importColumns[$i] == "ProjectName") {
						$projectList[trim($importValues[$i],"'")] = 0;
					}

					/*******************************************/
					// FORMAT VALUES
										
					// FORMAT ZIP IF ONLY 4 CHAR (MAIN SITE)
					if ($importColumns[$i] == "Zipcode") {
						$getZipcode=$importValues[$i];
						if(strlen($getZipcode)==6){
							$getZipcode=ltrim($getZipcode, "'");
							$importValues[$i]="'0".$getZipcode;
						}
					}

					// FORMAT ZIP IF ONLY 4 CHAR (FLS)
					if ($importColumns[$i] == "Zip") {
						$getZipcode=$importValues[$i];
						if(strlen($getZipcode)==6){
							$getZipcode=ltrim($getZipcode, "'");
							$importValues[$i]="'0".$getZipcode;
						}
					}

					// truncate headline
					if ($importColumns[$i] == "Headline") {
						$headline = trim($importValues[$i],"'");
						$headline = substr($headline, 0, 40);
						$importValues[$i] = "'".$headline."'";
					}
								
					//PayAmt (FLS)
					if ($importColumns[$i] == "PayAmt") {
						$PayAmt = trim($importValues[$i],"'");
						$formatted_PayAmt = number_format($PayAmt, 2);
						$importValues[$i] = "'" . $PayAmt . "'";
					}

					// END FORMAT VALUES
					/**************************************/
					// BEGIN EMAIL VALUES IF TECH WAS ASSIGNED TO WO
					 
					// SETUP EMAIL VALUES
					if ($importColumns[$i] == "WO_ID"){$WO=trim($importValues[$i],"'");}
					
					if ($importColumns[$i] == "Project_ID"){$Project_ID=trim($importValues[$i],"'");}
					
					if ($importColumns[$i] == "WorkOrderNo"){$WO=trim($importValues[$i],"'");}//FLS
					
					if ($importColumns[$i] == "Company_Name") 
					{
						$CompanyName=trim($importValues[$i],"'");
						$myCompanyName = $CompanyName;
					}
					
					if ($importColumns[$i] == "SiteName"){$SiteName=trim($importValues[$i],"'");}
					if ($importColumns[$i] == "Address"){$Address=trim($importValues[$i],"'");}
					if ($importColumns[$i] == "City"){$City=trim($importValues[$i],"'");}
					if ($importColumns[$i] == "State"){$State=trim($importValues[$i],"'");}
					if ($importColumns[$i] == "Zipcode"){$Zipcode=trim($importValues[$i],"'");}
					if ($importColumns[$i] == "Zip"){$Zipcode=trim($importValues[$i],"'");}//FLS
					if ($importColumns[$i] == "Description"){$Description=trim($importValues[$i],"'");}
					if ($importColumns[$i] == "FLSComments"){$Description=trim($importValues[$i],"'");}//FLS
					
					
					//GET TECH INFO if ID EXISTS
					if ($_POST["importType"] == "FLS") {
					
						if ($importColumns[$i] == "TechName") {$TechNameField=$i;}
						
						if ($importColumns[$i] == "FLSTechID") {						
							$addToEmailList="no";
							$insertTechName="no";

							if($importValues[$i]!='DEFAULT'){
								$techID = trim($importValues[$i],"'");
								
								$addToEmailList="yes";
								$insertTechName="yes";
								
// Query for Techs and place in array
$getTech = caspioSelect("TR_Master_List", "FirstName, LastName, PrimaryEmail, Address1, Address2, City, State, ZipCode, PrimaryPhone, SecondaryPhone, FirstName, LastName, Deactivated, ''", "FLSID = '$techID'", "", false);
								
// Get data from array
foreach ($getTech as $getTechOrder) {
$techfields = getFields($getTechOrder);
$GetTechName = $techfields[0]." ".$techfields[1];
$PrimaryEmail = $techfields[2];
$TechAddress1 = $techfields[3];
$TechAddress2 = $techfields[4];
$TechCity = $techfields[5];
$TechState = $techfields[6];
$TechZipCode = $techfields[7];
$TechPrimaryPhone = $techfields[8];
$TechSecondaryPhone = $techfields[9];
$TechFirstName = $techfields[10];
$TechLastName = $techfields[11];
if($techfields[12] == "True") {
	generateError("The technician you're attempting to assign no longer has an active profile with Field Solutions.  Sorry for any issues this may cause you. <b>FLS ID: " . $techID . " (" . $TechFirstName . " " . $TechLastName . ")</b>" );
}
else if($techfields[13] == $techID) {
	$warningHtml .= "You are about to assign work to a technician that has been \"denied access\" by your company. Please re-confirm your choice of technician. <b>FLS ID: " . $techID . " (" . $TechFirstName . " " . $TechLastName . ")</b><br/>";
}
}
							}
						}
					}
					else{
	
if ($importColumns[$i] == "Tech_FName") {$Tech_FNameField=$i;}
if ($importColumns[$i] == "Tech_LName") {$Tech_LNameField=$i;}
	
						$colVal = trim($importValues[$i],"'");
						if (($importColumns[$i] == "Tech_ID" || $importColumns[$i] == "FLS_ID") && @$addToEmailList != "yes") {
							$addToEmailList="no";
							$insertTechName="no";
						
							$techID = trim($importValues[$i],"'");
							if($importValues[$i]!='DEFAULT' && $techID != "" && is_numeric($techID)){
								$addToEmailList="yes";
								$insertTechName="yes";
								
								$techCrit = $importColumns[$i] == "FLS_ID" ? "FLSID = '$techID'" : "TechID = '$techID'";
								$colName = $importColumns[$i] == "FLS_ID" ? "FLS ID" : "Tech ID";
							
// Query for Techs and place in array
$getTech = caspioSelectAdv("TR_Master_List", "FirstName, LastName, PrimaryEmail, Address1, Address2, City, State, ZipCode, PrimaryPhone, SecondaryPhone, FirstName, LastName, Deactivated, (SELECT Deactivated_Techs_By_Clients.TechID FROM Deactivated_Techs_By_Clients WHERE Company_ID = '{$_GET['v']}' AND Deactivated_Techs_By_Clients.TechID = TR_Master_List.TechID), FLSID, AcceptTerms", $techCrit, "", false, "`", "|^",false);
							
// Get data from array
foreach ($getTech as $getTechOrder) {
$techfields = explode("|^", $getTechOrder);
	foreach ($techfields as $k=>$tf) {
		$techfields[$k] = trim($tf, "`");
	}
$GetTechName = $techfields[0]." ".$techfields[1];
$PrimaryEmail = $techfields[2];
$TechAddress1 = $techfields[3];
$TechAddress2 = $techfields[4];
$TechCity = $techfields[5];
$TechState = $techfields[6];
$TechZipCode = $techfields[7];
$TechPrimaryPhone = $techfields[8];
$TechSecondaryPhone = $techfields[9];
$TechFirstName = $techfields[10];
$TechLastName = $techfields[11];
//echo $techfields[12];
if($techfields[12] == "True") {

	generateError("The technician you're attempting to assign no longer has an active profile with Field Solutions.  Sorry for any issues this may cause you. <b>$colName: " . $techID . " (" . $TechFirstName . " " . $TechLastName . ")</b>" );
}
else if($techfields[13] == $techID) {
	$warningHtml .= "You are about to assign work to a technician that has been \"denied access\" by your company. Please re-confirm your choice of technician. <b>$colName: " . $techID . " (" . $TechFirstName . " " . $TechLastName . ")</b><br/>";
}
else if(strtolower($techfields[15]) != "yes") {
	generateError("The technician you're attempting to assign has not accepted terms of usage.  Sorry for any issues this may cause you. <b>$colName: " . $techID . " (" . $TechFirstName . " " . $TechLastName . ")</b>" );
}
else if ($_GET["v"] == "FLS" && empty($techfields[14])) {
	$warningHtml .= "You are about to assign work to a technician that does not have a FLS ID. Please re-confirm your choice of technician.<b>$colName: " . $techID . " (" . $TechFirstName . " " . $TechLastName . ")</b><br/>";
}
}
							}
						}
					}
					// END EMAIL VALUES IF TECH WAS ASSIGNED TO WO
					/********************************************/
				}

						
				$htmlTable .= "</tr>";

				
				/*********************************************/
				// Insert Tech name if exists
				if ($_POST["importType"] == "FLS") {
				
				if($insertTechName=='yes'){
					//echo "FLS Tech Name: ". $GetTechName . "column #". $TechNameField ."<br>";
					$importValues[$TechNameField]="'".$GetTechName."'";
					$insertTechName='no';
					$GetTechName='';
				}
	
				}else{			
					if($insertTechName=='yes'){

						//echo "Tech Name: ". $FirstName . " ". $LastName . "<br>";
//						$importValues[$Tech_FNameField]="'".$FirstName."'";
//						$importValues[$Tech_LNameField]="'".$LastName."'";
						$insertTechName='no';
						$FirstName='';
						$LastName='';
					}
				}
	
				/*********************************************/
				
				/**********************************************/
				// ADD TO EMAIL LIST IF TECH ID EXISTS
				
				if($addToEmailList=='yes'){
					$emailList[$emailCount] = array("PrimaryEmail"=>$PrimaryEmail, "FirstName"=>$TechFirstName, "LastName"=>$TechLastName, "WO"=>$WO, "SiteName"=>$SiteName, "Address"=>$Address, "City"=>$City, "State"=>$State, "Zipcode"=>$Zipcode, "Description"=>$Description, "techID"=>$techID, "Company_Name"=>$CompanyName, "TechAddress1" =>$TechAddress1, "TechAddress2" => $TechAddress2, "TechCity" => $TechCity, "TechState" => $TechState, "TechZipCode" => $TechZipCode, "TechPrimaryPhone" => $TechPrimaryPhone, "TechSecondaryPhone" =>$TechSecondaryPhone, "CompanyName" =>$CompanyName, "Project_ID" =>$Project_ID);

$emailCount=$emailCount+1;
$addToEmailList='no';
				}
				
				/**********************************************/
				// save WO_ID for part
				if ($partEntry != NULL) {
					$partEntry["WO_ID"] = trim($importValues[$columnMap["WO_ID"]], "'");
					$partsList[$currRow] = $partEntry;
				}
				
				if ($errorCount == 0) {
					$insertValues[$currRow] = $importValues;
				}				
				
				$currRow++;	
			}

//			$tableName = $tableName . "_importTest"; // test
			
			if (sizeof($woidList) > 0) {
				$woidList = "'" . implode("', '", array_flip($woidList)) . "'";
				$duplicates = caspioSelect($tableName, "WO_ID", "WO_ID IN ($woidList)", "");
				if (sizeof($duplicates) > 0 && $duplicates[0] != "") {
					generateError("WO_IDs already exist: " . implode(", ", $duplicates));
				}
			}
			
			if ($currRow - 2 > 200) {
				// limit to 200 per load
				generateError("Limt of 200 work orders per load. Please split your work orders into more than one CSV file of 200 or less work orders.");
			}
			
			echo "<br/>";
			echo "Checking {$_FILES["importFile"]["name"]} for errors ... <br/><br/>";
			if ($errorCount > 0) {
				echo "*** The following errors must be fixed before work orders can be imported ***<br/><br/>";
				echo "Common errors are explained <a href=\"/content/importWOHelp.php\" target=\"_blank\">here.</a><br/><br/>";
				echo $errorHtml;
				echo "<br/>*** End of error list ***<br/><br/>";
				if (!empty($warningHtml))
					echo "<br/>$warningHtml<br/>";
				echo $htmlTable;
			}
			else {
				if (!empty($warningHtml))
					echo "<br/>$warningHtml<br/>";
				echo "No errors found. Importing work orders ...<br/><br/>";
				
				if (sizeof($partsList) > 0) {
					foreach ($importColumns as $key=>$val) {
						if (array_key_exists($val, $newPartColumnMap))
							unset($importColumns[$key]);
						if (array_key_exists($val, $returnPartColumnMap))
							unset($importColumns[$key]);						
					}
				}
				
				$insertColumns = implode(",", $importColumns);
								
//				die();
				
				
				// Send emails to techs  
				foreach ($emailList as $getRow) {
					$WO=$getRow['WO'];
					$SiteName=$getRow['SiteName'];
					$FirstName=$getRow['FirstName'];
					$LastName=$getRow['LastName'];
					$Description=$getRow['Description'];
					$City=$getRow['City'];
					$State=$getRow['State'];
					$Zipcode=$getRow['Zipcode'];
					$PrimaryEmail=$getRow['PrimaryEmail'];
					$techID=$getRow['techID'];
					$TechAddress1 = $getRow['TechAddress1'];
					$TechAddress2 = $getRow['TechAddress2'];
					$TechCity = $getRow['TechCity'];
					$TechState = $getRow['TechState'];
					$TechZipCode = $getRow['TechZipCode'];
					$TechPrimaryPhone = $getRow['TechPrimaryPhone'];
					$TechSecondaryPhone = $getRow['TechSecondaryPhone'];
					$CompanyName=$getRow['CompanyName'];
					$Project_ID=$getRow['Project_ID'];
					
					echo "<br/><!--Sending Emails ....-->";
					ob_flush();
					flush();
					if ($_POST["importType"] != "FLS")
						Blast_Email($WO, $SiteName, $FirstName, $LastName, $Description, $City, $State, $Zipcode, $PrimaryEmail, $techID, $TechAddress1,$TechAddress2,$TechCity,$TechState,$TechZipCode,$TechPrimaryPhone,$TechSecondaryPhone, $CompanyName, $Project_ID);
				}
				
				echo sizeof($emailList) . " emails sent<br/><br/>";

				$index = 1;
				$errorMsg = "";
				$loadCount = 0;
			
//				echo '<table>';
				// INSERT INTO DB
				foreach ($insertValues as $key=>$row) {
					$insertRow = implode(",", $row);
//					print_r(explode(",", $insertColumns));
//					print_r(explode(",", $insertRow));
//					print_r($partsList);
//					die();
//					echo '<tr><td>';
					echo "Loading row $index ... ";
					ob_flush();
					flush();
					try {
						caspioInsert($tableName, $insertColumns, $insertRow, false);
						if (array_key_exists($key, $partsList))
							addPart($partsList[$key]);
						echo "Success";
						$loadCount++;
					}
					catch (SoapFault $fault) {
						echo "<span style=\"color: #FF0000\">Fail</span>";
						$errorMsg .= "QUERY: INSERT INTO $tableName ($insertColumns) VALUES ($insertRow).\n\nERROR MSG: $fault";
					}
					echo "<br/>";
//					echo '</td></tr>';
					$index++;
				}
//				echo '</table>';
				
				if ($errorMsg != "")
					smtpMailLogReceived("WO Import Script", "nobody@fieldsolutions.com", "tngo@fieldsolutions.com", "An Insert Failed", $errorMsg, $errorMsg, "WO Import Script");
								
				echo "$loadCount work orders loaded<br/><br/>";
				
				if ($_POST["importType"] == "Main") {
					// notify ops of imports
					$projectList = implode(", ", array_keys($projectList));
					$message = "Company: $myCompanyName
					User: " . $_SESSION['UserName'] . "
					Date: " . date("m/d/Y") . "
					Projects: $projectList
					Total WOs: $loadCount";
					smtpMailLogReceived("WO Import Script", "nobody@fieldsolutions.com", "WOUploads@fieldsolutions.com", "Work Orders Imported for $myCompanyName", $message, nl2br($message), "WO Import Script");
				}
				else {
					// notify ops of imports
					$projectList = implode(", ", array_keys($projectList));
					$message = "Company: FLS (FLS Section)
					User: " . $_SESSION['UserName'] . "
					Date: " . date("m/d/Y") . "
					Projects: $projectList
					Total WOs: $loadCount";
					smtpMailLogReceived("WO Import Script", "nobody@fieldsolutions.com", "WOUploads@fieldsolutions.com", "Work Orders Imported for FLS", $message, nl2br($message), "WO Import Script");
				}				
				
				// run after import scripts
				if ($_POST["importType"] == "Main") {
					exec("(/usr/bin/php -f " . realpath(dirname(__FILE__) . "/../") . "/availableWOGeocode.php) > /dev/null 2>&1 &");
				}
				else {
					exec("(/usr/bin/php -f " . realpath(dirname(__FILE__) . "/../") . "/flsAfterImport.php) > /dev/null 2>&1 &");

				}
			}
		}
	}



function Blast_Email($WO, $SiteName, $FirstName, $LastName,$Description, $City, $State, $Zipcode, $PrimaryEmail, $techID, $TechAddress1,$TechAddress2,$TechCity,$TechState,$TechZipCode,$TechPrimaryPhone,$TechSecondaryPhone, $CompanyName,$Project_ID){
if ($_POST["importType"] == "FLS") {
//	$CompanyName='Fujitsu';
	// FLS emails when assigned handled by background script
	return;
}

// Get Project info
	$project = caspioSelect("TR_Client_Projects", "From_Email, Resource_Coordinator, Project_Name", "Project_ID = '$Project_ID'", false);
	
	// Populate array with project info
	foreach ($project as $order) {
		
		$projectFields = getFields($order);
		
		$vFromEmail = $projectFields[0];	// From Email address
		$vFromName = $projectFields[1];		// From Name
		$projectName = $projectFields[2];   // Project Name
	
		$vSubject = "Project Assigned: $projectName"; // Subject Line
	}
	

if ($City == "DEFAULT")
	$City = "";
if ($State == "DEFAULT")
	$State = "";
if ($ZipCode == "DEFAULT")
	$ZipCode = "";

//$vFromEmail = "orders@fieldsolutions.com";
//$vFromName = "Field Solutions";
//$vSubject = "Work Order in $City, $State $ZipCode";


// Send Email to the Tech
$message = "		
Dear $FirstName $LastName,

Congratulations, you have been awarded work from $CompanyName.

Please click on the link below and review the work order information. Note:
You may be required to login to your account if you are not already.

You must indicate that you have reviewed the work order info as well as any project related documentation. To review the project related documentation, open up your work order.  Across the top of the work order is a light blue bar with Print and Project Documents. Clicking on Project Documents will open a screen where you can download the documents needed for this work order.

Refer to section 5 - Tech has reviewed all Work Order Details, which is the same page you view when clicking on a work order in the system to update it.

In section 5, be sure to check the box confirming to $company
that you are ready to perform the work listed in the work order.

You will also be receiving another auto generated email 48 hours before your install. You will be required to revisit the website and same section to click on another box confirming your intentions to be at the install.

If you do not do this, $CompanyName will activate the backup for the
install.

<a href='https://www.fieldsolutions.com/techs/wosWorkOrderReviewed.php?tbUNID=$tbUNID&techID=$techID'>I have reviewed all Work Order Details and documentation.</a>

Your Contact Information:

$TechAddress1 $TechAddress2
$TechCity, $TechState  $TechZipCode

Primary Phone: $TechPrimaryPhone  
Secondary Phone: $TechSecondaryPhone  
Email: $PrimaryEmail

<a href='https://www.fieldsolutions.com/techs/profile.php'>Update Contact Information</a>

Thank you for your support,
Field Solutions Management";




		
$htmlmessage = nl2br($message);


// execute mail function
smtpMailLogReceived($vFromName, $vFromEmail, $PrimaryEmail, $vSubject, $message, $htmlmessage, "WO Import Script");
//smtpMailLogReceived($vFromName, $vFromEmail, "codem01@gmail.com", $vSubject, $message, $htmlmessage, "WO Import Script");
//smtpMailLogReceived($vFromName, $vFromEmail, "collinmcgarry@gmail.com", $vSubject, $message, $htmlmessage, "WO Import Script");
}


/* Support function for value checking */
	class MyEnum {
	}
	
	$FIELD_EXISTS = new MyEnum();
	
	class caspioFieldProperties {
		var $type, $unique;
		
		public function caspioFieldProperties($t, $u) {
			$this->type = $t;
			$this->unique = $u;
		}
		public function checkValue($value) {
			$result = false;
//			if ($value == "DEFAULT") return true;
			switch ($this->type) {
				case "ShortText":
				case "LongText":
					$result = $this->checkText($value);
					break;
				case "AutoNumber":


				case "Number":
					$result = $this->checkNumber($value);
					break;
				case "Date/Time":
					$result = $this->checkDateTime($value);
					break;
				case "Yes/No":
					$result = $this->checkYesNo($value);
					break;					
				case "File":
					$result = $this->checkFile($value);
					break;
			}
			return $result;
		}
		
		public function getType() {
			return $this->type;
		}
		
		protected function checkText($value) {
			// long and short text
			return true;
		}
		protected function checkNumber($value) {
			// number and autonumber
			return (is_numeric($value) || $value == "");
		}
		protected function checkDateTime($value) {
			// date/time
			if ($value == "") return true;
			$parse = date_parse($value); 
			return ($parse && $parse["error_count"] == 0 && checkdate($parse["month"], $parse["day"], $parse["year"]));
		}
		protected function checkYesNo($value) {
			// Yes / No
//			$value = trim($value);
			return ($value == "0" || $value == "1" || $value == "");
		}
		protected function checkFile($value) {
			// File
			return true;
		}
	}

	class TwoFieldValidator {
		var $nameMap, $index;
		var $col1, $col2;
		
		public function TwoFieldValidator($validFieldsTable, $vfc1, $vfc2, $c1, $c2) {
			$this->col1 = $c1;
			$this->col2 = $c2;
			$results = caspioSelectAdv($validFieldsTable, "$vfc1, $vfc2", "", "",false, "`", "|");
			$this->nameMap = array();
			foreach ($results as $row) {
				$fields = str_replace("`", "", $row);
				$fields = explode("|", $fields);
				$this->nameMap[$fields[0]] = $fields[1];
			}
			$this->index = array();
		}

		public function generateErrorMsg($row, $errorCount, $importValues, $globalPattern) {
			$name = $importValues[$this->index[$this->col1]];
			$id = $importValues[$this->index[$this->col2]];
			generateError("Row <a href=\"#$errorCount\">$row</a>: Values for " . $this->col1 . " and " . $this->col2 . " do not match ('$name' / '$id')");
		}

		public function resetIndex() {
			$this->index = array();
		}

		public function readyToValidate($currentColumn) {
			return $currentColumn == max($this->index[$this->col1], $this->index[$this->col2]);
		}

		public function setIndex($name, $index) {
//			echo "try set $name for {$this->col1} {$this->col2}<br/>";
			if ($name != $this->col1 && $name != $this->col2) return false;
//			echo "setting<br/>";
			$this->index[$name] = $index;
			return true;
		}

		public function validate($importValues, $globalPattern) {
//			echo $this->nameMap[$this->value[$this->col1]] . " " . $this->value[$this->col2];
			$valCol1 = str_replace("''", "'", trim($importValues[$this->index[$this->col1]], "'"));
			$valCol2 = str_replace("''", "'", trim($importValues[$this->index[$this->col2]], "'"));
//			print_r($this->nameMap[$valCol1]);
//			echo "validating: {$this->col1} - $valCol1, {$this->col2} - $valCol2<br/>";
			return (array_key_exists($valCol1, $this->nameMap) && $this->nameMap[$valCol1] == $valCol2);
		}
	}
	
	class CaseThenValidator {
		var $index;
		var $ifCol, $thenCol;
		var $comparison;
		
		public function CaseThenValidator($ifCol, $thenCol, $comparison) {
			$this->ifCol = $ifCol;
			$this->thenCol = $thenCol;
			$this->comparison = $comparison;
			$this->index = array();
		}

		public function generateErrorMsg($row, $errorCount, $importValues, $globalPattern) {
			$name = $this->index[$this->ifCol];
			$id = $this->index[$this->thenCol];
			$errorMsg = "Row <a href=\"#$errorCount\">$row</a>:  ";
			foreach ($this->comparison as $ifPattern => $thenPattern) {
				eval("\$ifPattern = " . $ifPattern . ";");
				$errorMsg .= "When {$this->ifCol} is {$ifPattern->getDescription()} then, {$this->thenCol} must be in this format: {$thenPattern->getDescription()}<br/>";
				generateError($errorMsg);
			}
		}

		public function resetIndex() {
			$this->index = array();
		}

		public function readyToValidate($currentColumn) {
			return (array_key_exists($this->ifCol, $this->index) && array_key_exists($this->thenCol, $this->index) && $currentColumn == max($this->index[$this->ifCol], $this->index[$this->thenCol]));
		}

		public function setIndex($col, $index) {

//			echo "try set $col for {$this->ifCol} {$this->thenCol}<br/>";
			if ($col != $this->ifCol && $col != $this->thenCol) return false;
//			echo "setting<br/>";
			$this->index[$col] = $index;
			return true;
		}

		public function validate($importValues, $globalPattern) {
			$valIfCol = trim($importValues[$this->index[$this->ifCol]], "'");
			$valThenCol = trim($importValues[$this->index[$this->thenCol]], "'");
			$valIfCol = $valIfCol == "DEFAULT" ? "" : $valIfCol;
			$valThenCol = $valThenCol == "DEFAULT" ? "" : $valThenCol;
//			echo "validating: {$this->ifCol} - $valIfCol, {$this->thenCol} - $valThenCol<br/>";
			// get patternMacth to use based on if column value or ignore if not provided
			$validator = null;
			foreach ($this->comparison as $ifPattern => $thenPattern) {
				eval("\$ifPattern = " . $ifPattern . ";");
				if ($validator != null) break;
				if ($ifPattern->checkValue($valIfCol))
					$validator = $thenPattern;
			}
			return ($validator == null || $validator->checkValue($valThenCol));
		}
	}
	
	class patternMatch {
		var $pattern;
		var $description;
		var $matchBlank;
		public function patternMatch($p, $d, $matchBlank = TRUE) {
			$this->pattern = $p;
			$this->description = $d;
			$this->matchBlank = $matchBlank;
		}
		public function getDescription() {
			return $this->description;
		}
		public function checkValue($value) {
			return (($this->matchBlank && $value == "") || preg_match($this->pattern, $value) == 1);
		}
	}
	

	function checkMustEqual($name, $value, $column) {
		global $FIELD_EXISTS;
		// returns false if bad data
		$checkRequired = array_key_exists($name, $column);
		
		if ($checkRequired && $column[$name] !== $FIELD_EXISTS && is_object($column[$name])) {
			return $column[$name]->checkValue($value);
		}
		if ($checkRequired && is_array($column[$name])) {
			$compareStr = array();
			foreach ($column[$name] as $validVal)
				$compareStr[] = "\"$value\" == \"$validVal\"";
			$compareStr = implode(" || ", $compareStr);
			eval("\$result = ($compareStr);");
			return $result;
		}
		return (!$checkRequired || $value == $column[$name] || $column[$name] === $FIELD_EXISTS);
	}
	
	function checkMissingMustEqualColumns($column, $import, $ignoreList) {
		$columnMap = array_flip($import);
		foreach ($column as $name => $value) {
			if (!array_key_exists($name, $columnMap) && !array_key_exists($name, $ignoreList))
				generateError("Column list error: Missing column $name");
		}
	}
	
	class ProjectInfoAutofill {
		var $columnList;
		var $columnListProject;
		var $columnMap;
		var $forcedColumns;
		var $cachedProjectInfo;
		public function ProjectInfoAutofill($columnMap, $forcedColumns) {
			$woToProjectFieldMap = array("ProjectManagerName" => "Project_Manager",
			"ProjectManagerPhone" => "Project_Manager_Phone",
			"ProjectManagerEmail" => "Project_Manager_Email",

			"ResourceCoordinatorName" => "Resource_Coordinator",
			"ResourceCoordinatorPhone" => "Resource_Coord_Phone",
			"ResourceCoordinatorEmail" => "Resource_Coord_Email",

			"SMSBlast" => "AutoSMSOnPublish",
			
			"PayMax" => "Default_Amount",
			"Tech_Bid_Amount" => "Default_Amount"
			);
		
			
			$columnMapProject = $columnMap;
			
			
			$this->columnList = array_keys($columnMap);
			$this->columnListProject = $this->columnList;
			
			foreach ($this->columnListProject as $index => $fieldName) {
				if (array_key_exists($fieldName, $woToProjectFieldMap))
					$this->columnListProject[$index] = $woToProjectFieldMap[$fieldName];
			}

			$this->columnMap = $columnMap;
			$this->forcedColumns = $forcedColumns;
			$this->cachedProjectInfo = array();
		}
				
		public function autofillColumns($importValues) {
			if (!is_array($importValues)) return $importValues;
			if (!array_key_exists("Project_ID", $this->columnMap)) return $importValues;
			$projectID = $importValues[$this->columnMap["Project_ID"]];
			if (!is_numeric($projectID)) return $importValues;
			if (!array_key_exists($projectID, $this->cachedProjectInfo))
				$info = caspioSelectAdv("TR_Client_Projects", implode(", ", $this->columnListProject), "Project_ID = '$projectID'", "",false, "`", "|", false);
			else
				$info = $cachedProjectInfo[$projectID];
			if ($info[0] == "") return $importValues;
			$info = explode("|", $info[0]);
			$index = 0;
			$lookup = array();
			foreach ($this->columnMap as $key => $val) {
				if ($val == "LookupOnly") {
					$lookup[$key] = trim($info[$index], "`");
				}
				++$index;
			}
			
			$index = 0;
			foreach ($this->columnMap as $key => $val) {
				if ($key == "Project_ID" || $val == "LookupOnly") {
					$index++;
					continue;
				}
				// fill column if blank
				if (!array_key_exists($val, $importValues))
					$importValues[$val] = "DEFAULT";
				
				$forced = (array_key_exists($key, $this->forcedColumns) ? $this->forcedColumns[$key] : false);
				$fixedBid = false;
				if (is_string($forced)) {
					$fixedBid = $lookup[$forced] == "True";
					$forced = false;
				}
				
				$projectValue = trim($info[$index], "`");
				if ($projectValue == "False")
					$projectValue = "0";
				else if ($projectValue == "True")
					$projectValue = "1";
				else if ($projectValue == "NULL")
					$projectValue = "";
					
				$importValues[$val] = ($forced || $importValues[$val] == "" || $importValues[$val] == "DEFAULT" || $importValues[$val] == "NULL" ?  $projectValue : $importValues[$val]);
				
				if ($fixedBid && $key == "PayMax") {
					// force agree amount to paymax
					$importValues[$this->columnMap["Tech_Bid_Amount"]] = $importValues[$val];
				}					

				$index++;
			}
			return $importValues;
		}
		
	}
	
	class FLSCalculation {
		public static function numberWeekDays($startDate, $endDate) {			
			$startDateTS = strtotime($startDate);
			$endDateTS = strtotime($endDate);

			$startDayOfWeek = date("w", $startDateTS);
			$endDayOfWeek = date("w", $endDateTS);
			
			$daysToNextSunday = 7 - $startDayOfWeek;
			$daysToPreviousSunday = $endDayOfWeek;
			$nextSundayTS = strtotime("+$daysToNextSunday days", $startDateTS);
			$previousSundayTS = strtotime("-$daysToPreviousSunday days", $endDateTS);
			
			$numWeekDays = 0;
			
			if ($nextSundayTS < $endDateTS) {
				// date range is more than one week
				$startWeekNumWeekDays = 5 - ($startDayOfWeek - 1 <= -1 ? 0 : $startDayOfWeek - 1);
				$endWeekNumWeekDays = ($endDayOfWeek == 6 ? 5 : $endDayOfWeek);
				
				$numDaysBetweenFirstAndLastWeek = ($previousSundayTS - $nextSundayTS) / 60 / 60 / 24 / 7 * 5;
				
				$numWeekDays = $startWeekNumWeekDays + $endWeekNumWeekDays + $numDaysBetweenFirstAndLastWeek;
			}
			else {
				$startWeekNumWeekDays = 5 - ($startDayOfWeek - 1 <= -1 ? 0 : $startDayOfWeek - 1);
				$endWeekNumWeekDaysNotIncluded = 5 - ($endDayOfWeek == 6 ? 5 : $endDayOfWeek);
				$numWeekDays = $startWeekNumWeekDays - $endWeekNumWeekDaysNotIncluded;
			}
			return $numWeekDays;
		}
	
		public static function calculateSourceByDate($startDateTS, $endDateTS, $dateEntered, $numWeekDays) {
			$sourceByDateTS = $endDateTS; // use Project Start date by default
				
			if ($numWeekDays > 5 && $numWeekDays < 16) {
				// use Date record created
				$sourceByDateTS = strtotime(date("m/d/Y", strtotime($dateEntered)));
					
				// Can't count start day so subtract, initally start with Project Date
				$sourceByDateTS = strtotime("-1 day", $sourceByDateTS);
			}
					
			if ($numWeekDays < 6)
				$sourceByDateTS = strtotime(date("m/d/Y", strtotime($dateEntered)));
			else if ($numWeekDays > 5 && $numWeekDays < 16) {
				for ($i = 0; $i <= 5; ++$i) {
					$dayNum = date("w", $sourceByDateTS);
					if ($dayNum == 0 || $dayNum == 6)
						--$i;
					$sourceByDateTS = strtotime("+1 days", $sourceByDateTS);
				}
			}
			else {
				$sourceByDateTS = strtotime("-1 days", $sourceByDateTS);
				for ($i = 10; $i >= 1; $i--) {
					$dayNum = date("w", $sourceByDateTS);
					if ($dayNum == 0 || $dayNum == 6)
						++$i;
					$sourceByDateTS = strtotime("-1 days", $sourceByDateTS);
				}
			}
			
			return $sourceByDateTS;			
		}
	}

?>