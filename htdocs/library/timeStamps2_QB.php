<?php
	require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');

	if (empty($argc))
		require_once(realpath(dirname(__FILE__) . "/../") . "/headerStartSession.php");
	require_once("caspioAPI.php");
//	require_once("smtpMail.php");
//	require_once ("mySQL.php");

	$LoggedInAs = $_SESSION['loggedInAs'];
	$UserName = $_SESSION['UserName'];
	$timeStampCookie = "timeStampQueue" . session_id();

	processQueuedTimeStamp();

	function Create_TimeStamp($woID, $userName, $Description, $companyID, $clientName, $projectName, $customerName, $TechID, $TechName) {
		date_default_timezone_set('US/Central');
		$dateTimeStamp = date("Y-m-d H:i:s");
/*		$clientName = caspioEscape($clientName);
		$projectName = caspioEscape($projectName);
		$customerName = caspioEscape($customerName);
		$TechName = caspioEscape($TechName);*/

		//$fieldList = "WO_UNID, DateTime_Stamp, Username, Description, Company_ID, Client_Name, Project_Name, Customer_Name, TechID, Tech_Name";
		//$valueList = "'$woID', '$dateTimeStamp', '$userName', '$Description', '$companyID', '$clientName', '$projectName', '$customerName', '$TechID', '$TechName'";
		//echo "<p>$valueList</p><br />";

		$fields = array("WIN_NUM" => $woID,
			"DateTime_Stamp" => $dateTimeStamp,
			"Username" => $userName,
			"Description" => $Description,
			"Customer_Name" => $customerName,
			"Tech_ID" => $TechID,
			"Tech_Name" => $TechName);

		$db = Zend_Registry::get('DB');

		try {
			$result = $db->insert("timestamps", $fields);
//			$result = caspioInsert("Work_Order_TimeStamps", $fieldList, $valueList, false);
		} catch (SoapFault $fault) {
//			smtpMail("Timestamp Normal", "no-replies@fieldsolutions.us", "tngo@fieldsolutions.com", "Time stamp normal", "$fault -- " . print_r($valuesList, true), "$fault -- "  . print_r($valuesList, true), "timeStamps2.php");
		}
	}

	function processQueuedTimeStamp() {
		global $timeStampCookie, $UserName;
//		ini_set("display_errors", 1);
		if (!isset($_COOKIE[$timeStampCookie])) return;
		$currentQueue = unserialize(str_replace('\"', '"',$_COOKIE[$timeStampCookie]));
		if ($currentQueue[sizeof($currentQueue) - 1] != "end") return;
		if (!setcookie($timeStampCookie, "", time() - 3600, "/")) {
			?>
            <script type="text/javascript">
				var expireDate = new Date;
				expireDate.setDate(expireDate.getDate() - 1);
				document.cookie = "<?=$timeStampCookie?>" + "=;path=/;expires="+ expireDate.toGMTString();
			</script>
            <?php
		}
//		print_r($_COOKIE);
		if (isset($_COOKIE[$timeStampCookie])) {
			foreach ($currentQueue as $timeStampInfo) {
				if ($timeStampInfo == "end") continue;
				$info = unserialize($timeStampInfo);
//				print_r($info);
				Create_TimeStamp(caspioEscape($info["woID"]), caspioEscape($UserName), caspioEscape($info["description"]), caspioEscape($info["companyID"]), caspioEscape($info["clientName"]), caspioEscape($info["projectName"]), caspioEscape($info["customerName"]), caspioEscape($info["techID"]), caspioEscape($info["techName"]));
			}
		}
	}

	function initializeTimeStampsJS() {
		global $timeStampCookie;
	?>
	<script type="text/javascript" src="../library/php.namespaced.min.js"></script>
	<script type="text/javascript">
		var timeStampCookie = "<?=$timeStampCookie?>";
		function queueCreateTimeStamp(woID, description, companyID, clientName, projectName, customerName, techID, techName) {
			timeStampObj = timeStampParametize(woID, description, companyID, clientName, projectName, customerName, techID, techName);
			timeStampObj = $P.serialize(timeStampObj);
			var expireDate = new Date;
			expireDate.setSeconds(expireDate.getSeconds() + 45);
			var currentQueue = getTimeStampQueue();
			currentQueue.push(timeStampObj);
//			alert(timeStampCookie + "=" + $P.serialize(currentQueue) + ";expires="+ expireDate.toGMTString());
			document.cookie = timeStampCookie + "=" + escape($P.serialize(currentQueue)) + ";path=/;expires="+ expireDate.toGMTString();
		}

		function getTimeStampQueue() {
			if (document.cookie.length > 0) {
				c_start = document.cookie.indexOf(timeStampCookie + "=");
				if (c_start != -1) {
					c_start = c_start + timeStampCookie.length + 1;
					c_end = document.cookie.indexOf(";", c_start);
					if (c_end == -1) c_end = document.cookie.length;
					var queue = $P.unserialize(unescape(document.cookie.substring(c_start,c_end)));
					var retArray = new Array();
					for (i in queue) {
						retArray[i] = queue[i];
					}
					return retArray;
				}
			}
			return new Array();
		}

		function endQueueTimeStamp() {
			var currentQueue = getTimeStampQueue();
			currentQueue.push("end");
			var expireDate = new Date;
			expireDate.setSeconds(expireDate.getSeconds() + 45);
//			alert(timeStampCookie + "=" + $P.serialize(currentQueue) + ";expires="+ expireDate.toGMTString());
			document.cookie = timeStampCookie + "=" + escape($P.serialize(currentQueue)) + ";path=/;expires="+ expireDate.toGMTString();
		}

		function timeStampParametize(woID, description, companyID, clientName, projectName, customerName, techID, techName) {
			return {"woID" : woID, "description" : description, "companyID" : companyID, "clientName" : clientName, "projectName" : projectName, "customerName" : customerName, "techID" : techID, "techName" : techName};
		}

	</script>
    <?php
	}
?>
