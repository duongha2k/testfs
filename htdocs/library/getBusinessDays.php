<?php


$getYear = date('Y', strtotime('now'));//get the year
$holidays=array("$getYear-12-25","$getYear-12-26","$getYear-01-01");

// This functions counts ahead a specified number of business days skipping holidays


//remove variables after done testing
$dayCount=14;
$approveDate="2007-12-25";

echo getCountBusinessDays($approveDate,$dayCount,$holidays);

function getCountBusinessDays($startDate,$dayCount,$holidays){

$endDate = date('Y-m-d', strtotime('+14 days'));
echo "$endDate<br>";

$workdays = 0 ;
    while( $startDate <= $endDate )
    {
        if( date('w', $startDate ) != 6 && date( 'w', $startDate) != 0 )
        {
            $workdays++ ;
        }
		else{
			$endDate++;
		}
        $startDate += 86400 ;
		if($workdays != $dayCount){};
    }
    return $workdays ;

}


//The function returns the no. of business days between two dates and it skips the holidays
function getWorkingDays($startDate,$endDate,$holidays){
    //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
    //We add one to inlude both dates in the interval.
    $days = (strtotime($endDate) - strtotime($startDate)) / 86400 + 1;

    $no_full_weeks = floor($days / 7);
    $no_remaining_days = fmod($days, 7);

    //It will return 1 if it's Monday,.. ,7 for Sunday
    $the_first_day_of_week = date("N",strtotime($startDate));
    $the_last_day_of_week = date("N",strtotime($endDate));

    //The two can't be equal because the $no_remaining_days (the interval between $the_first_day_of_week and $the_last_day_of_week) is at most 6
    //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
    if ($the_first_day_of_week < $the_last_day_of_week){
        if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
        if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
    }
    else{
        if ($the_first_day_of_week <= 6) $no_remaining_days--;
        //In the case when the interval falls in two weeks, there will be a Sunday for sure
        $no_remaining_days--;
    }

    //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
    $workingDays = $no_full_weeks * 5 + $no_remaining_days;

    //We subtract the holidays
    foreach($holidays as $holiday){
        $time_stamp=strtotime($holiday);
        //If the holiday doesn't fall in weekend
        if (strtotime($startDate) <= $time_stamp && $time_stamp <= strtotime($endDate) && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
            $workingDays--;
    }

    return $workingDays;
}
// Example:
//echo getWorkingDays("2007-12-22","2008-01-06",$holidays)
// => will return 8

?>