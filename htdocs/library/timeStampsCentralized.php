<?php
	/* 
		requires:
			- TSType as JS var to auto record original fields
	*/
	if (@!$argc)
		require_once(realpath(dirname(__FILE__) . "/../") . "/headerStartSession.php");
	require_once("caspioAPI.php");
	require_once("smtpMail.php");
//	ini_set("display_errors",1);
	
	$LoggedInAs = $_SESSION['loggedInAs'];
	$UserName = $_SESSION['UserName'];
	$timeStampCookieCentralized = "WOOriginalFields" . session_id();
	// dequeue 1: means timestamps completed but possible that dp form returned with errors
	$dequeue = 0;
			
	processTimeStampsCentralized();

	if (!function_exists("Create_TimeStamp")):
	function Create_TimeStamp($woID, $userName, $Description, $companyID, $clientName, $projectName, $customerName, $TechID, $TechName) {
				
		$dateTimeStamp = date("Y-m-d H:i:s");
		$clientName = caspioEscape($clientName);
		$projectName = caspioEscape($projectName);
		$customerName = caspioEscape($customerName);
		$TechName = caspioEscape($TechName);
				
		$fieldList = "WO_UNID, DateTime_Stamp, Username, Description, Company_ID, Client_Name, Project_Name, Customer_Name, TechID, Tech_Name";
		$valueList = "'$woID', '$dateTimeStamp', '$userName', '$Description', '$companyID', '$clientName', '$projectName', '$customerName', '$TechID', '$TechName'";
		//echo "<p>$valueList</p><br />";
		$result = caspioInsert("Work_Order_TimeStamps", $fieldList, $valueList, false);
	}
	endif;
	
	function processTimeStampsAfterCreate($id) {
		global $timeStampCookieCentralized, $UserName;
//		echo "<!-- " . print_r(unserialize($_COOKIE[$timeStampCookie]), true) . " -->";
//		if (empty($_COOKIE[$timeStampCookie])) return;
		$list = unserialize(str_replace('\"', '"',$_COOKIE[$timeStampCookieCentralized]));
		$newValues = $list;
		$newValues["TB_UNID"] = $id;
		$list = $newValues;
		$_COOKIE[$timeStampCookieCentralized] = serialize($list);
		processTimeStampsCentralized();
	}
	
	function isNewOrChanged($field, $originalFields, $updatedFields, $create) {
		return $create || ($originalFields[$field] != $updatedFields[$field]);
	}
	
	function processTimeStampsCentralized() {
		global $timeStampCookieCentralized, $UserName, $dequeue;
//		ini_set("display_errors", 1);
/*		if (!setcookie($timeStampCookie, "", time() - 3600, "/")) {
			?>
            <script type="text/javascript">
				var expireDate = new Date;
				expireDate.setDate(expireDate.getDate() - 1);
				document.cookie = "<?=$timeStampCookie?>" + "=;path=/;expires="+ expireDate.toGMTString();
			</script>
            <?php
		}*/
//		print_r($_COOKIE);
		if (isset($_COOKIE[$timeStampCookieCentralized]) && $_COOKIE[$timeStampCookieCentralized] != "") {
			$list = unserialize(str_replace('\"', '"',$_COOKIE[$timeStampCookieCentralized]));
			$valuesList = $list;
			if (!is_array($valuesList)) return;
			$originalFields = array();
			
			// Check for timestamps
			
			$create = $valuesList[$timeStampCookieCentralized] == "Create";
//			$prefix = $create ? "InsertRecord" : "EditRecord";
			foreach ($valuesList as $key => $val) {
//				if (strpos($key, $prefix) === FALSE || $val == "SYSDATE") continue;
//				if ($val == "SYSDATE") continue;
//				$newKey = explode($prefix, $key, 2);
//				$newKey = $newKey[1];
				$originalFields[$key] = $val;
			}
			if ($create) {
				if (!array_key_exists("TB_UNID", $originalFields)) {
					// requeue until given a WorkOrderID
					// setcookie($timeStampCookie, $_COOKIE[$timeStampCookie], time() + 3600, "/");
					return;
				}
/*				$originalFields["TechComplete"] = "False";
				$originalFields["TechComplete"] = "False";
				$originalFields["PayApprove"] = "False";
				$originalFields["Kickback"] = "False";
				$originalFields["Deactivated"] = "";
				$originalFields["DeclinedWorkOrder"] = "False";*/
			}
			
			$criteria = "TB_UNID = '" . caspioEscape($originalFields["TB_UNID"]) . "'";
			unset($originalFields[$timeStampCookieCentralized]);
			
			$fieldsList = array_keys($originalFields);
			$fieldsMap = array_flip($fieldsList);
			
//			echo $criteria;
			try {
				$result = caspioSelectAdv("Work_Orders", implode(",",$fieldsList), $criteria, "TB_UNID DESC", false, "`", "|", false);
			} catch (SoapFault $fault) {
				smtpMail("Timestamp Centeralized", "no-replies@fieldsolutions.us", "tngo@fieldsolutions.com", "Time stamp centralized", "$fault -- " . print_r($valuesList, true), "$fault -- "  . print_r($valuesList, true), "timeStampsCentralized.php");
				return FALSE;
			}
			
			if (!$result) return FALSE;
			
			$updatedFields = array();
			$values = explode("|", $result[0]);
			foreach ($values as $index => $value) {
				$val = trim($value, "`");
				if ($val == "NULL")
					$val = "";
				$updatedFields[$fieldsList[$index]] = $val;
			}
			
			$wo_id = $originalFields["TB_UNID"];
			
/*			if ($create)
				Create_TimeStamp($wo_id, $UserName, "Work Order Created", "FLS", "FLS", $updatedFields["ProjectName"], "", "", "");
			
//			echo $wo_id;
//			print_r($result);
//			die();
				
/*			if (!empty($updatedFields["FLSTechID"]) && isNewOrChanged("FLSTechID", $originalFields, $updatedFields, $create))
				Create_TimeStamp($wo_id, $UserName, "Work Order Assigned", "FLS", "FLS", $updatedFields["ProjectName"], "", "", "");
				
	        if (!empty($updatedFields["FLSTechID"]) && isNewOrChanged("TechComplete", $originalFields, $updatedFields, $create)) {
				$val = $updatedFields['TechComplete']=='True' ? "Completed" : "Not Completed";
				Create_TimeStamp($wo_id, $UserName, "Work Order Marked: $val", "FLS", "FLS", $updatedFields["ProjectName"], "", "", "");
				if ($updatedFields['TechComplete'] == "False")
					Create_TimeStamp($wo_id, $UserName, "Work Order Marked Incomplete", "FLS", "FLS", $updatedFields["ProjectName"], "", "", "");
			}									
	
			if ($updatedFields["PayApprove"] == "True" && isNewOrChanged("PayApprove", $originalFields, $updatedFields, $create))
				Create_TimeStamp($wo_id, $UserName, "Work Order Approved", "FLS", "FLS", $updatedFields["ProjectName"], "", "", "");
				
			if ($updatedFields["Kickback"] == "True" && isNewOrChanged("Kickback", $originalFields, $updatedFields, $create))
				Create_TimeStamp($wo_id, $UserName, "Work Order Kickback", "FLS", "FLS", $updatedFields["ProjectName"], "", "", "");

			if ($updatedFields["Deactivated"] == "Yes" && isNewOrChanged("Deactivated", $originalFields, $updatedFields, $create))
				Create_TimeStamp($wo_id, $UserName, "Work Order Deactivated", "FLS", "FLS", $updatedFields["ProjectName"], "", "", "");

			if ($updatedFields["DeclinedWorkOrder"] == "True" && isNewOrChanged("DeclinedWorkOrder", $originalFields, $updatedFields, $create))
				Create_TimeStamp($wo_id, $UserName, "Work Order Declined", "FLS", "FLS", $updatedFields["ProjectName"], "", "", "");*/
			
/*			if ($updatedFields["DeclinedWorkOrder"] == "True" && isNewOrChanged("DeclinedWorkOrder", $originalFields, $updatedFields, $create))
				Create_TimeStamp($wo_id, $UserName, "Work Order Declined", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields["ProjectName"], "", "", "");*/

/*			if ($updatedFields["Deactivated"] == "True" && isNewOrChanged("Deactivated", $originalFields, $updatedFields, $create))
				Create_TimeStamp($wo_id, $UserName, "Work Order Deactivated", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields["Project_Name"], "", "", "");*/
			
				
/*	------------------------------------------------------------------------------------------------------

	if (showTechs != showTechsField().attr("checked") && showTechsField().attr("checked") == true) {
		// Work Order Published
		queueCreateTimeStamp(woID, "Work Order Published", Company_ID, clientName, projectName, "", "", "");
	}
		
	if (getTechChange) {
		// Assigned
		queueCreateTimeStamp(woID, "Work Order Assigned", Company_ID, clientName, projectName, "", "", "");
	}
	
	if (requireACSReset) {
		// Reset ACS calls made
		$("#cbParamVirtual42").attr("value", 1);
//		queueCreateTimeStamp(woID, "Reset ACS Calls", Company_ID, clientName, projectName, "", "", "");
//		window.open("acsResetReminderCalls.php?id=" + document.forms.caspioform.GetTB_UNID.value);s
	}
		
	if (oldWorkOrderReviewed != $("#EditRecordWorkOrderReviewed").attr("checked") && $("#EditRecordWorkOrderReviewed").attr("checked")) {
		// Reviewed
		queueCreateTimeStamp(woID, "Work Order Reviewed", Company_ID, clientName, projectName, "", "", "");
	}
	if (oldWorkOrderConfirmed != $("#EditRecordTechCheckedIn_24hrs").attr("checked") && $("#EditRecordTechCheckedIn_24hrs").attr("checked")) {
		// Confirmed
		queueCreateTimeStamp(woID, "Work Order Confirmed", Company_ID, clientName, projectName, "", "", "");
	}
	
	if (oldTechMarkedComplete != techComplete) {
		// Tech Mark Complete
		var oldVal = (oldTechMarkedComplete ? "Completed" : "Not Completed")
		var newVal = (document.forms.caspioform.EditRecordTechMarkedComplete.checked ? "Completed" : "Not Completed")
		queueCreateTimeStamp(woID, "Work Order Marked: " + newVal, Company_ID, clientName, projectName, "",  "", "");
		if (!techComplete)
			
			var Stamp = new Date();
			var v_TimeStamp;
			Hours = Stamp.getHours()
			Mins = Stamp.getMinutes();
								
			if (Mins < 10) {Mins = "0" + Mins;}
			v_TimeStamp = ('' + (Stamp.getMonth() + 1) +"/"+Stamp.getDate()+ "/"+Stamp.getFullYear() + ' ' + Hours + ":" + Mins);
			$("#EditRecordDateIncomplete").attr("value", v_TimeStamp);
			
			// Incomplete
			queueCreateTimeStamp(woID, "Work Order Marked Incomplete", Company_ID, clientName, projectName, "",  "", "");
	}
	
	if ($("#EditRecordAttachedFile").attr("value") != undefined) {
		// Attached File
		queueCreateTimeStamp(woID, "File Attached", Company_ID, clientName, projectName, "",  "", "");
	}

	if (oldApproved != $("#EditRecordApproved").attr("checked") && $("#EditRecordApproved").attr("checked")) {
		// Approved
		queueCreateTimeStamp(woID, "Work Order Approved", Company_ID, clientName, projectName, "",  "", "");
	}
		
	if (oldDeactivated != $("#EditRecordDeactivated").attr("checked") && $("#EditRecordDeactivated").attr("checked")) {
		// Deactivated
		queueCreateTimeStamp(woID, "Work Order Deactivated", Company_ID, clientName, projectName, "",  "", "");
	}

	if (oldAbortFee != $("#EditRecordAbortFee").attr("checked")) {
		// Abort Fee
		queueCreateTimeStamp(woID, "Abort Fee " + ($("#EditRecordAbortFee").attr("checked") ? "Checked" : "Un-checked") , Company_ID, clientName, projectName, "",  "", "");
	}*/				

			$changed = array();
			$val = array();
			foreach ($updatedFields as $key => $value) {
				if ($key == "Project_Name") continue;
				if (isNewOrChanged($key, $originalFields, $updatedFields, $create)) {
					$dateTest = strtotime($originalFields[$key]);
					if ($dateTest) {
						$originalFields[$key] = date("m/d/Y h:i:s A", $dateTest);
						$updatedFields[$key] = date("m/d/Y h:i:s A", strtotime($value));
						if (!isNewOrChanged($key, $originalFields, $updatedFields, $create)) continue;
					}
					$changed[] = $key;
					$val[] = $value;
				}
			}
			
			if (sizeof($changed) > 0) {
/*				echo "Changed: " . print_r($changed, true) . " | " . print_r($val, true);
				print_r($originalFields);
				print_r($updatedFields);*/
				
				sort($changed);
				$changed = implode(",", $changed);
				Create_TimeStamp($wo_id, $UserName, "Work Order Updated: $changed", $updatedFields["Company_ID"], $updatedFields["Company_Name"], $updatedFields["Project_Name"], "", "", "");
			}
			
			$dequeue = 1;
		}
	}
	
	function initializeTimeStampsJSCentralized() {
		global $timeStampCookieCentralized, $dequeue;
	?>
	<script type="text/javascript" src="/library/php.namespaced.min.js"></script>
	<script type="text/javascript">
		var timeStampCookieCentralized = "<?=$timeStampCookieCentralized?>";
		var valuesList = new Array();
		var updateValuesList = new Array();
		var typeList = new Array();
		var prefix = "";
		var dequeue = "<?=$dequeue?>";
		
		$(document).ready(function () {
			try {
				test = TSType;
				recordOriginalFieldValues(TSType);
			}
			catch (e) { }
			test = getCookieTS(timeStampCookieCentralized);
			if (test != undefined || test != "")
					setCookieTS(timeStampCookieCentralized, "", 1);
		});
			
		function addToValuesList(index) {
			myId = $(this).attr("id") ? $(this).attr("id") : $(this).attr("name");
			if (myId == undefined) return;
			key = myId;
			type = $(this).attr("type");
			if (key.search(prefix) != 0 && key != "mywoid") return;
			if (key.search("static") != -1) return;
			if (key == "mywoid") key = "TB_UNID";
			else {
				key = key.split(prefix);
				key = key[1];
			}
//			typeList[key] = type;
			if (type != undefined && type.toLowerCase() == "checkbox") {
				if ($(this).attr("value") == "Y")
					value = $(this).attr("checked") ? "True" : "False";
				else
					value = $(this).attr("checked") ? $(this).val() : "";
			}
			else
				value = $(this).val();
			valuesList[key] = value;
		}
		
		function recordOriginalFieldValues(type) {
			previousTS = getCookieTS(timeStampCookieCentralized);
			previousValuesList = previousTS ? $P.unserialize(previousTS) : false;
			if (type != "Create") {
				type = "Edit";
				prefix = "EditRecord";
			}
			else
				prefix = "InsertRecord";
			if (previousTS && dequeue == "1") {
				valuesList = previousValuesList;
//				alert(valuesList["Deactivated"]);
			}
			else {
				valuesList[timeStampCookieCentralized] = type;
				$(":input").each(addToValuesList);
			}			
//			valuesList[timeStampCookie + "TypeList"] = typeList;
		}		
		
		function queueTimeStampsCentralized() {
			originalValuesList = valuesList;
			
			valuesList = new Array();
			valuesList[timeStampCookieCentralized] = originalValuesList[timeStampCookieCentralized];
			
			$(":input").each(addToValuesList);
			
			changedValuesList = new Array();
			for (values in valuesList) {
				if (valuesList[values] == originalValuesList[values] && valuesList[values] != "SYSDATE") continue;
				changedValuesList[values] = originalValuesList[values];
			}
			
			changedValuesList[timeStampCookieCentralized] = originalValuesList[timeStampCookieCentralized];
			changedValuesList["TB_UNID"] = $("#GetTB_UNID").val();
			changedValuesList["Company_ID"] = originalValuesList["Company_ID"];
			changedValuesList["Company_Name"] = originalValuesList["Company_Name"];
			changedValuesList["Project_Name"] = originalValuesList["Project_Name"];
			
			s = $P.serialize(changedValuesList);
			setCookieTS(timeStampCookieCentralized, s, 1);
		}

		function setCookieTS(c_name,value,expiredays)
		{
			var exdate = new Date();
			exdate.setDate(exdate.getDate()+expiredays);
			document.cookie = c_name+ "=" +escape(value) +
			((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
		}
		
		function getCookieTS(c_name)
		{
			if (document.cookie.length>0)
			  {
			  c_start=document.cookie.indexOf(c_name + "=");
			  if (c_start!=-1)
				{
				c_start=c_start + c_name.length+1;
				c_end=document.cookie.indexOf(";",c_start);
				if (c_end==-1) c_end=document.cookie.length;
				return unescape(document.cookie.substring(c_start,c_end));
				}
			  }
			return "";
		}
	</script>
    <?php
	}
?>
