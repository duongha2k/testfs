<?php
	require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');
	Core_Caspio::init();

/* --------------------- CASPIO API related Library ---------------------------- */
	if (session_id() == "" && empty($argc)) {
		// no session vars and not called from cmd line
		require_once(realpath(dirname(__FILE__) . "/../") . "/headerStartSession.php");
	}
	if (session_id() == "" || !array_key_exists("loggedIn", $_SESSION) || $_SESSION['loggedIn'] != 'yes') {
		if (empty($argc)) {
			// not called from cmd line
			if (headers_sent())
				die();
			header("Location: /");
			die();
		}
	}


	function exceptionHandler($ex) {
		global $caspioPassword, $caspioProfile;
		if (ini_get("display_errors") == 1) {
			echo str_replace(array($caspioPassword, $caspioProfile), "***", $ex->__toString());
		}
		else {
//			echo ""
		}
		die();
	}

	//set_exception_handler('exceptionHandler');

	//Caspio Bridge WS API WSDL file (Profile has the Required SSL Security UNCHECKED).
	$wsdl = "https://bridge.caspio.net/ws/api.asmx?wsdl";

	//Caspio Bridge account name
	$caspioName = "mgkassoc";

	//Web service profile
	$caspioProfile = "EmailBlaster";

	//Web service profile password
	$caspioPassword = "Rocking2011!";

	// init SOAP Client
/*	try {
		$client = new SoapClient($wsdl);
	} catch (SoapFault $fault) {
//		echo "<p>Error initializing Soap Client : $fault</p>";
	}*/

	function getCurrentCaspioServer() {
		// create a new cURL resource
		$ch = curl_init();

		// set URL and other appropriate options
		$options = array(CURLOPT_URL => 'http://bridge.caspio.net/whichserver.htm',
						 CURLOPT_HEADER => false,
						 CURLOPT_RETURNTRANSFER => true
						);

		curl_setopt_array($ch, $options);

		// grab URL and pass it to the browser
		$server = curl_exec($ch);

		// close cURL resource, and free up system resources
		curl_close($ch);

		return "<p>Current Caspio Server: " . $server . "</p>";
	}

	function caspioEscape($str) {
		return Core_Caspio::caspioEscape($str);
	}

	function getFields($row) {
	/* Takes a single row returned by a select and returns an array of Fields */
		$row = str_replace("'", "", $row); // gets rid of quotes
		return explode(",", $row);
	}

	function caspioSelect($tableName, $fieldList, $criteria, $sortBy, $catchErrors = TRUE) {
	/* 	Performs a SELECT query on the CASPIO Database
			$tableName: Table to search
			$fieldList: Comma seperated list of fields returned when match found
			$criteria: Search critera
			$sortBy: Sort critera
		Returns array of records found
	*/
		
		return caspioSelectAdv($tableName, $fieldList, $criteria, $sortBy, false, "", "", $cathErrors);

	}

	function caspioSelectView($tableName, $fieldList, $criteria, $sortBy, $catchErrors = TRUE) {
	/* 	Performs a SELECT query on the CASPIO Database
			$tableName: View to search
			$fieldList: Comma seperated list of fields returned when match found
			$criteria: Search critera
			$sortBy: Sort critera
		Returns array of records found
	*/
		return caspioSelectAdv($tableName, $fieldList, $criteria, $sortBy, true, "", "", $catchErrors);
	}

	function caspioSelectAdv($tableName, $fieldList, $criteria, $sortBy, $isView = FALSE,$delimitString = "'", $delimitField = ",", $catchErrors = TRUE) {
	/* 	Performs a SELECT query on the CASPIO Database
			$tableName: View to search
			$fieldList: Comma seperated list of fields returned when match found
			$criteria: Search critera
			$sortBy: Sort critera
		Returns array of records found
	*/
		Core_Caspio::checkAllowedTable($tableName);
		
		try {
			$results = Core_Caspio::caspioSelectAdv($tableName, $fieldList, $criteria, $sortBy, false);
			foreach ($results as $k=>$v) {
				$results[$k] = implode($delimitField, $v);
			}
			return $results;
		}
		catch (Exception $fault) {
			//SOAP fault handling
			if (!$catchErrors) {
				throw $fault;
			}
		}
		return false;
	}

	/* Performs an Insert query on the CASPIO Database
		$tableName: Table to update
		$fieldList: Comma seperated list of fields to update
		$valueList: Comma seperated list of value to set fields*/
	function caspioInsert($tableName, $fieldList, $valueList, $catchErrors = TRUE)  {
		$result = Core_Caspio::caspioInsert($tableName, $fieldList, $valueList);
		if ($result === false && !$catchErrors) {
			throw new Core_Exception("Caspio Exception: " . Core_Api_Error::getInstance());
		}
		return $result;
	}

	/* Performs an UPDATE query on the CASPIO Database
		$tableName: Table to update
		$fieldList: Comma seperated list of fields to update
		$valueList: Comma seperated list of value to set fields
		$criteria: Update critera
	*/
	function caspioUpdate($tableName, $fieldList, $valueList, $criteria, $catchErrors = TRUE)  {
		$result = Core_Caspio::caspioUpdate($tableName, $fieldList, $valueList, $criteria);
		if ($result === false && !$catchErrors) {
			throw new Core_Exception("Caspio Exception: " . Core_Api_Error::getInstance());
		}
		return $result;
	}

	function caspioProximitySearchByCoordinatesRaw($tableName, $isView, $latitude, $longitude, $refLatitudeField, $refLongitudeField, $proximityCriteria, $units, $fieldList,  $additionalCriteria, $orderBy, $includeCalculatedDistance, $fieldDelimiter = "", $stringDelimiter = "", $catchErrors = TRUE) {
	/* Example:
		caspioProximitySearchByReferenceRaw($tableName, false, $latitude, $longitude, "Zipcode",
			$zipTable, false, "ZIPcode", "Latitude", "Longitude", $distance,"3",$fieldList, "",
			"TechID", true, "","");
	*/
		global $caspioName, $caspioProfile, $caspioPassword, $client;

		Core_Caspio::checkAllowedTable($tableName);
		
		return Core_Caspio::caspioProximitySearchByCoordinatesRaw(
        $tableName,                 //  ObjectName
        $isView,                    //  IsView
        $latitude,                  //  Latitude
        $longitude,                 //  Longitude
        $refLatitudeField,          //  LatitudeField
        $refLongitudeField,         //  LongitudeField
        $proximityCriteria,         //  ProximityCriteria
        $units,                     //  Units
        $fieldList,                 //  FieldList
        $additionalCriteria,        //  AdditionalCriteria
        $orderBy,                   //  OrderBy
        $includeCalculatedDistance, //  IncludeCalculatedDistance
        $fieldDelimiter,
        $stringDelimiter,
        $catchErrors);
	}

	/* Performs a DELETE query on the CASPIO Database
		$tableName: Table to update
		$criteria: Delete critera*/
	function caspioDelete($tableName, $criteria, $catchErrors = TRUE)  {
		$result = Core_Caspio::caspioDelete($tableName, $criteria);
		if ($result === false && !$catchErrors) {
			throw new Core_Exception("Caspio Exception: " . Core_Api_Error::getInstance());
		}
		return $result;
	}

	function caspioGetTableDesign($tableName, $catchErrors = TRUE) {
		return Core_Caspio::caspioGetTableDesign($this->getMysqlTableFromCaspio($tableName));
	}



	function checkAdminLogin() {
		if ($_SESSION['loggedIn'] != 'yes' || $_SESSION['loggedInAs'] != 'admin') header('Location:./');
		return FALSE;
	}

	function checkLogin($type) {
		if ($_SESSION['loggedIn'] != 'yes' || $_SESSION['loggedInAs'] != $type) header('Location:./');
		return FALSE;
	}


	function caspioDownloadFile($fileName, $catchErrors = TRUE)  {
	/* Performs an UPDATE query on the CASPIO Database
		$tableName: Table to update
		$fieldList: Comma seperated list of fields to update
		$valueList: Comma seperated list of value to set fields
		$criteria: Update critera
	*/
		global $caspioName, $caspioProfile, $caspioPassword, $client;
		try
		{
			return $client->DownloadFileByName($caspioName, $caspioProfile, $caspioPassword, $fileName);
		}
		catch (SoapFault $fault)
		{
			//SOAP fault handling
			if (!$catchErrors) {
				$fault->faultstring .= " " . getCurrentCaspioServer();
				throw $fault;
			}
//			else
//				echo "<p>Error in caspioUpdate($tableName, $fieldList, $valueList, $criteria): $fault</p>";
		}
		return false;
	}
