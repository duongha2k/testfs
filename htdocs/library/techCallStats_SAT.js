	var hideAction = null;
	var hideDelay = 500;
	function showDetails(data, textStatus) {
		if (data != "") {
			parts = data.split("|");
			imacs = parts[0];
			serviceCalls = parts[1];
			backOuts = parts[2];
			noShows = parts[3];
			recommendedAvg = parts[4] == "NULL" ? 0 : parseFloat(parts[4]);
			recommendedTotal = parts[5];
			performanceAvg = parts[6] == "NULL" ? 0 : parseFloat(parts[6]);
			performanceTotal = parts[7];
			total = parseInt(imacs, 10) + parseInt(serviceCalls, 10);
			html = "<table><thead><tr><td>&nbsp;</td><td>Stats Last 12 Months</td><td>Total</td></tr></thead>";
			html += "<tr><td colspan='2'>IMAC Calls</td><td>" + imacs + "</td></tr>";
			html += "<tr><td colspan='2'>Service Calls</td><td>" + serviceCalls + "</td></tr>";
			html += "<tr><td colspan='2'>Back Outs</td><td>" + backOuts + "</td></tr>";
			html += "<tr><td colspan='2'>No Shows</td><td>" + noShows + "</td></tr>";
			html += "<tr><td colspan='2'>Preference</td><td>" + recommendedAvg.toFixed(0) + "% (" + recommendedTotal + ")</td></tr>";
			html += "<tr><td colspan='2'>Performance</td><td>" + performanceAvg.toFixed(0) + "% (" + performanceTotal + ")</td></tr>";
//			html += "<tr><td colspan='2'></td></tr><tfoot><tr><td colspan='2'>Total of " + total + "</td></tr></tfoot>";
			html += "<tr><td colspan='3'>&nbsp;</td></tr>";
			html += "<tr><td><div class='ratingBox' style='background-color: #AAA; width: 10px; height: 10px;'></div></td><td colspan='2' style='font-size: 9px; text-align: left'>New tech</td></tr>";
			html += "<tr><td><div class='ratingBox' style='background-color: #0F0; width: 10px; height: 10px;'></div></td><td colspan='2' style='font-size: 9px; text-align: left'>10+ jobs, rating 95%+, &lt; 3 no shows, &lt; 3 back outs</td></tr>";
			html += "<tr><td><div class='ratingBox' style='background-color: #FF0; width: 10px; height: 10px;'></div></td><td colspan='2' style='font-size: 9px; text-align: left'>5+ jobs</td></tr>";
			html += "<tr><td><div class='ratingBox' style='background-color: #F00; width: 10px; height: 10px;'></div></td><td colspan='2' style='font-size: 9px; text-align: left'>2+ no shows OR rating &lt; 90%</td></tr>";
			html += "</table>";
		}
		else {
			html = "No call information available.";
		}
		$('#ratingDetailsBox').html(html);
	}
	
	function showDetailsBox() {
		resetHideDetailsBox();
		var myid = (this.id.split("myRating",2))[1];
		var pos = $(this).offset();
		$.get("/ajax/techCallStats_SAT.php", { id: myid }, showDetails);
		var height = $('#ratingDetailsBox').height();
		$('#ratingDetailsBox').css({top: pos.top - (height / 2), left: pos.left + 30, display: "none"});
		$('#ratingDetailsBox').html("<img src='/images/loading.gif' />");
		$('#ratingDetailsBox').fadeIn("slow");
	}
	
	function delayedHideDetailsBox() {
		if (hideAction) clearTimeout(hideAction);
		hideAction = setTimeout("hideDetailsBox();", hideDelay);
	}
	
	function hideDetailsBox() {
		$('#ratingDetailsBox').fadeOut("slow");
	}
	
	function resetHideDetailsBox() {
		clearTimeout(hideAction);
	}
	
	$(document).ready(
		function () {
			$('body').append("<div id='ratingDetailsBox'></div>");
			$('.ratingBox').hover(showDetailsBox, delayedHideDetailsBox);
			$('#ratingDetailsBox').hover(resetHideDetailsBox, delayedHideDetailsBox);
		}
	);
