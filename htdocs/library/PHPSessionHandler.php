<?php
require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');

class Core_PHPSessionHandler {

	const DB_HOST = "fieldsolutionsdb.cd3kywrhh0ei.us-east-1.rds.amazonaws.com";
	const DB_USER = "fsolutions";
	const DB_PASSWORD = "volume55nourish";
	const DB_NAME = "php_session";
	const SESSION_TABLE = "php_session.PHPSESSION";

    // The database connection
	private $connection;

	public function __construct() {
//		session_set_save_handler(array($this, "sessionOpen"),
//                         array($this, "sessionClose"),
//                         array($this, "sessionRead"),
//                         array($this, "sessionWrite"),
//                         array($this, "sessionDestroy"),
//                         array($this, "sessionGC"));
	}

	public function __destruct() {
		@session_write_close();
	}

	private function showerror( )
	{
//		die("Error " . mysql_errno( ) . " : " . mysql_error( ));
	}

/*	private static function switchDatabase() {
		if (!empty($this->$oldDB))
			$this->$oldDB = mysql_query("SELECT DATABASE()");
		mysql_select_db($this->DB_NAME);
	}

	private static function restoreDatabase() {
		if (!empty($this->$oldDB))
			mysql_select_db($this->$oldDB);
		$this->$oldDB = null;
	}*/

	// Returns current time as a number.
    // Used for recording the last session access.

    private function getMicroTime( )
    {
		// microtime( ) returns the number of seconds
		// since 0:00:00 January 1, 1970 GMT as a
		// microsecond part and a second part.
		// e.g.: 0.08344800 1000952237
		// Convert the two parts into an array
		$mtime = explode(" ", microtime( ));

		// Return the addition of the two parts
		// e.g.: 1000952237.08344800
		return($mtime[1] + $mtime[0]);
    }

    // The session open handler called by PHP whenever
    // a session is initialized. Always returns true.

    public function sessionOpen($database_name, $table_name)
    {
		if (!($this->connection = @mysql_pconnect(self::DB_HOST,
                                           self::DB_USER,
                                           self::DB_PASSWORD)))
			$this->showerror( );



		if (!mysql_select_db(self::DB_NAME, $this->connection))
			$this->showerror( );

		return true;
	}

    // This function is called whenever a session_start( )
    // call is made and reads the session variables
    // Returns "" when a session is not found
    //         (serialized)string - session exists

    public function sessionRead($sess_id)
    {
		// Formulate a query to find the session
		// identified by $sess_id
		$search_query =
			"SELECT * FROM " .  self::SESSION_TABLE . "
				WHERE session_id = '$sess_id'";

		// Execute the query
		if (!($result = @ mysql_query($search_query,
                                    $this->connection)))
			$this->showerror( );

		if(mysql_num_rows($result) == 0)
		// No session found - return an empty string
			return "";
		else
		{
			// Found a session - return the serialized string
			$row = mysql_fetch_array($result);
			return $row["session_variable"];
		}
	}

	// This function is called when a session is initialized
	// with a session_start( ) call, when variables are
	// registered or unregistered, and when session variables
	// are modified. Returns true on success.

	public function sessionWrite($sess_id, $val)
	{
		$val = mysql_real_escape_string($val);
		$time_stamp = $this->getMicroTime( );

		$search_query =
			"SELECT session_id FROM " .  self::SESSION_TABLE . "
			WHERE session_id = '$sess_id'";

		// Execute the query
		if (!($result = @ mysql_query($search_query,
									$this->connection)))
			$this->showerror( );

		if(mysql_num_rows($result) == 0)
		{
			// No session found, insert a new one
			$insert_query =
				"INSERT INTO " .  self::SESSION_TABLE . "
				(session_id, session_variable, last_accessed)
				VALUES ('$sess_id', '$val', $time_stamp)";

			if (!mysql_query($insert_query,
				$this->connection))
				$this->showerror( );
		}
		else
		{
			// Existing session found - Update the
			// session variables
			$update_query =
				"UPDATE " .  self::SESSION_TABLE . "
				SET session_variable = '$val',
				last_accessed = $time_stamp
				WHERE session_id = '$sess_id'";

			if (!mysql_query($update_query,
						  $this->connection))
				$this->showerror( );
		}
		return true;
	}

	// This function is executed on shutdown of the session.
	// Always returns true.

	public function sessionClose($sess_id = null)
	{
		return true;
	}

	// This is called whenever the session_destroy( )
	// function call is made. Returns true if the session
	// has successfully been deleted.

	public function sessionDestroy($sess_id)
	{

		$delete_query =
			"DELETE FROM " .  self::SESSION_TABLE . "
			WHERE session_id = '$sess_id'";

		if (!($result = @ mysql_query($delete_query,
									$this->$connection)))
			$this->showerror( );

		return true;
	}


	// This function is called on a session's start up with
	// the probability specified in session.gc_probability.
	// Performs garbage collection by removing all sessions
	// that haven't been updated in the last $max_lifetime
	// seconds as set in session.gc_maxlifetime.
	// Returns true if the DELETE query succeeded.

	public function sessionGC($max_lifetime)
	{
		$time_stamp = $this->getMicroTime( );

		$delete_query =
			"DELETE FROM " .  self::SESSION_TABLE . "
				WHERE last_accessed < ($time_stamp - $max_lifetime)";

		if (!($result = @ mysql_query($delete_query,
									$this->$connection)))
			$this->showerror( );

		return true;
	}

}
