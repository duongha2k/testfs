<?php
	// Sends email in the background. Called by smtpMailLogReceivedBG
	require_once("recruitmentEmailLib.php");
	if (!isset($argv[1])) die();
//	sleep($argv[8]);
//	smtpMailLogReceived($argv[1], $argv[2], $argv[3], $argv[4], $argv[5], $argv[6], $argv[7]);
	
	$argv[1] = unserialize($argv[1]);
	$argv[5] = unserialize($argv[5]);
	$argv[6] = unserialize($argv[6]);
	$argv[7] = unserialize($argv[7]);
	
	try {
		$info = blastRecruitmentEmailMainSite($argv[1], $argv[2], $argv[3], $argv[4], $argv[5], $argv[6], $argv[7], $argv[8], $argv[9]);
		//$info = array(123, array('77062', '78653'));
		
		if ($argv[7] === TRUE && !empty($argv[8])) { // emails from email with stats
			$emailSent = $info[0];
			$badZip = $info[1];
			
			$msg = "Emails sent: " . $emailSent . "\n";
	
//			$msg .= print_r($argv,true) . "\n";
			
			if ($argv[5]) {
				$msg .= "Copy sent to: {$argv[8]}\n";
			}
	
			if (sizeof($badZip) > 0) {
				$badZip = "'" . implode("', '", $badZip) . "'";
				$msg .= "One or more Work Orders contain an invalid zipcode. The invalid zip codes are: $badZip";
			}
			

                        $apicom = new Core_Api_CommonClass();
                        if($apicom->isStringHTML($msg))
                            $htmlmsg = $msg;
                        else
                            $htmlmsg = nl2br($msg);
                        
			smtpMailLogReceived("FS Recruitment Email", "no-replies@fieldsolutions.us", "{$argv[8]}, tngo@fieldsolutions.com", "Recruitment Emails Sent", $msg, $htmlmsg, "Recruitment Email");

			$numWO = sizeof($argv[1]);
			if ($numWO > 1) {
				$msg = "# WO - $numWO\nSent: $emailSent\nWIN INFO - " . print_r($argv[1], true);
				smtpMail("FS Recruitment Email", "no-replies@fieldsolutions.us", "tngo@fieldsolutions.com, gbailey@fieldsolutions.com", "Recruitment Emails Sent", $msg, $htmlmsg, "Recruitment Email");
			}
			
		}
	} catch (SoapFault $fault) {
		$msg = print_r($argv,true) . " $fault";
		smtpMailLogReceived("FS Recruitment Email", "no-replies@fieldsolutions.us", "tngo@fieldsolutions.com", "Recruitment Email Errors", $msg, $msg, "Recruitment Email");
	}
	
	die();
?>
