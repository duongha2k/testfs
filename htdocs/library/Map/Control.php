<?php
/**
 * @author Sergey Petkevich
 * The class used for the mapping service
 */
class Map_Control
{    protected $params;
    protected $queryString;
    protected $session;
    protected $mapid;
    protected $exception;

    public function __construct($params = null)
    {        if (is_array($params)) {
            $this->params = $params;
        }
    }

    public function setQueryString($query)
    {        $this->queryString = $query;
    }

    public function getParams()
    {        return $this->params;
    }

    /**
     * $this->session will point to $_SESSION
     */
    public function setSession(&$session)
    {        $this->session = &$session;
    }


    public function getSession()
    {        return $this->session;
    }

    public function saveParams()
    {        $this->mapid = md5(uniqid('map'));

        $this->session['map'][$this->mapid] = serialize($this->params);
        return $this->mapid;
    }

    public function getRedirectParams()
    {        $redir = '';

        if ($this->queryString) {
        	if (!empty($this->exception)) {                $arrQueryString = explode('&', $this->queryString);
                foreach ($arrQueryString as $key => &$val) {                    if (strpos($val, $this->exception.'=') === 0) {                        unset($arrQueryString[$key]);
                    }
                }
                $this->queryString = implode('&', $arrQueryString);
        	}
            $redir .= $this->queryString . '&';
        }

        $redir .= 'mapid=' . $this->mapid;

        return $redir;
    }

    public function setException($newVal)
    {        $this->exception = $newVal;
    }

    /**
     *  Get search params from the session
     */
    public function getParamsById($id)
    {
        if (isset($this->session['map'][$id])) {
            return unserialize($this->session['map'][$id]);
        } else {
            return null;
        }
    }

    public static function getColorMarkers()
    {        $res = array();
        $res['green']  = 'Green';
        $res['red']    = 'Red';
        $res['yellow'] = 'Yellow';

        return $res;
    }

}
?>