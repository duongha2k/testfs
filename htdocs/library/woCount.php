<?php
	require_once("{$_SERVER['DOCUMENT_ROOT']}/library/caspioAPI.php");

//	$today = date("m/d/Y");

	$woAllOpen = 0;
	$woThisMonth = 0;
	$woThisWeek = 0;
	$woToday = 0;

	function getWOWeekMonthAllCount($id, $idType) {
		global $woAllOpen, $woThisMonth, $woThisWeek, $woToday;
		if ($idType != "Company_ID" && $idType != "Tech_ID") return;
		$id = caspioEscape($id);

		if (!array_key_exists("WOCountCacheTime", $_SESSION)) {
			$_SESSION["WOCountCacheTime"] = 0;
			$_SESSION["WOCountCacheIDType"] = "";
		}
		if (time() - $_SESSION["WOCountCacheTime"] < 15 && $_SESSION["WOCountCacheIDType"] == $idType) {
			$woAllOpen = $_SESSION["WOCountCacheAllOpen"];
			$woThisMonth = $_SESSION["WOCountCacheThisMonth"];
			$woThisWeek = $_SESSION["WOCountCacheThisWeek"];
			return;
		}

		$dayOfWeek = date("w"); // 0 = Sunday, 6 = Saturday

		$startWeek = date("Y-m-d", strtotime("-$dayOfWeek days")); // Sunday
		$endWeek = date("Y-m-d", strtotime("This Saturday")); // Saturday

		$startMonth = date("Y-m-01");
		$endMonth = date("Y-m-d", strtotime("$startMonth + 1 month - 1 day"));
		$dateSort = "(CASE WHEN StartDate >= '$startWeek' AND StartDate <= '$endWeek' THEN 2 WHEN StartDate >= '$startMonth' AND StartDate <= '$endMonth' THEN 1 ELSE 0 END)";
		$db = Zend_Registry::get('DB');
		$sql = "SELECT $dateSort AS dateSort, COUNT(WIN_NUM) FROM work_orders WHERE $idType=? AND Deactivated = 0 GROUP BY dateSort ORDER BY dateSort DESC";
		$countMap = $db->fetchPairs($sql, array($id));

		//$count = caspioSelectAdv("Work_Orders", "count(TB_UNID), $dateSort", "$idType = '$id' AND Deactivated = '0' GROUP BY $dateSort", "$dateSort DESC", false, "`", "|", false);

		for ($i = 2; $i >= 0; $i--) {
			$woCount = array_key_exists($i, $countMap) ? $countMap[$i] : 0;
			switch ($i) {
				case 2:
					$woThisWeek = $woCount;
					break;
				case 1:
					$woThisMonth = $woCount + $woThisWeek;
					break;
				default:
					$woAllOpen = $woCount + $woThisMonth;
					break;
			}
		}

		$_SESSION["WOCountCacheTime"] = time();
		$_SESSION["WOCountCacheIDType"] = $idType;
		$_SESSION["WOCountCacheAllOpen"] = $woAllOpen;
		$_SESSION["WOCountCacheThisMonth"] = $woThisMonth;
		$_SESSION["WOCountCacheThisWeek"] = $woThisWeek;
	}
?>