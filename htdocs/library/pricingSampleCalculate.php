<?php 
	require_once(realpath(dirname(__FILE__) . "/../") . "/headerStartSession.php");
//	ini_set("display_errors", 1);
	require_once("pricing.php");
	$calcMatrix = FALSE;
	$calcResults = FALSE;
	$id = NULL;

	if (!isset($_GET["id"]) || !is_numeric($_GET["id"])) {
		if (!isset($_SESSION["calcMatrixParam"])) die();
		// get calculation passed via session
		$calcMatrix = $_SESSION["calcMatrixParam"];
	}
	else {
		// get charge types by id
		$id = $_GET["id"];
		$chargeTypes = getChargeTypes($id);
		if (!$chargeTypes) die();
		$calcMatrix = createPricingCalculationMatrix($chargeTypes);
		$_SESSION["calcMatrixParam"] = $calcMatrix;
	}
	if (!$calcMatrix) die();
	$inputFieldsHTMLArray = NULL;
	$values = (isset($_GET["values"]) ? unserialize(str_replace('\"', '"', $_GET["values"])) : NULL);
	
	$inputFieldsHTMLArray = createInputFields($calcMatrix["inputFieldList"], $values);
?>
<br/><br/>
<div align="center">
    Initial Values
    <table>
    <?php
        foreach ($inputFieldsHTMLArray as $desc=>$html) {
    ?>
        <tr><td><?=$desc?></td><td><?=$html?></td></tr>
    <?php
        }
    ?>
        <tr><td colspan="2" style="text-align:center"><input id="calculateBtn" name="calculateBtn" type="button" value="Calculate" onclick="runCalculation()" /></td></tr>
    </table>
</div>
<?php
	if (isset($_GET["values"])) {
		$calcResults = performPricingCalculation($calcMatrix, $values, $id);
		if ($calcResults)
			echo nl2br($calcResults[1]);
//		print_r($calcResults[0]);
//		saveCalculatedValuesToWo("0", $calcResults);
	}
?>
