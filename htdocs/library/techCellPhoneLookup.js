	var hideActionTechCP = null;
	var hideDelayTechCP = 500;
	var techCPBox = "#techCellPhoneLookupBox";
	function showDetailsTechCP(data, textStatus) {
		if (data != "") {
			html = "Cell: " + data;
		}
		else {
			html = "No Cell Phone";
		}
		$(techCPBox).html(html);
	}
	
	function showDetailsBoxTechCP() {
		resetHideDetailsBoxTechCP();
		var myid = $(this).attr("id").split("Tech")[1];
		var pos = $(this).offset();
		$.get("/ajax/techCellPhoneLookup.php", { id: myid }, showDetailsTechCP);
		var height = $(techCPBox).height();
		$(techCPBox).css({top: pos.top - (height / 2), left: pos.left + 30, display: "none"});
		$(techCPBox).html("<img src='/images/loading.gif' />");
		$(techCPBox).fadeIn("slow");
	}
	
	function delayedHideDetailsBoxTechCP() {
		hideActionTechCP = setTimeout("hideDetailsBoxTechCP();", hideDelayTechCP);
	}
	
	function hideDetailsBoxTechCP() {
		$(techCPBox).fadeOut("slow");
	}
	
	function resetHideDetailsBoxTechCP() {
		clearTimeout(hideActionTechCP);
	}
	
	$(document).ready(
		function () {
			$('body').append("<div id='techCellPhoneLookupBox'></div>");
			$('.techCellPhoneLookup').hover(showDetailsBoxTechCP, delayedHideDetailsBoxTechCP);
			$(techCPBox).hover(resetHideDetailsBoxTechCP, delayedHideDetailsBoxTechCP);
		}
	);
