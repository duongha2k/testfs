<?php
	require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');
	require_once("contactSettings.php");
	require_once(dirname(__FILE__) . "/../../wdata/application/modules/dashboard/views/helpers/DisplayDate.php");

// Main Site Recruitment Email Library

//	ini_set("display_erorrs", 1);
	set_time_limit(3600);
	if (!isset($argc))
		require_once("{$_SERVER['DOCUMENT_ROOT']}/headerStartSession.php");

	require_once("caspioAPI.php");
	require_once("smtpMail.php");
	require_once("googleMapAPI.php");
	require_once("timeStamps2.php");
	require_once(realpath(dirname(__FILE__) . "/../") . "/includes/SMS_Send.php");

	define("ID_TYPE_WO_ID", 0);
	define("ID_TYPE_TB_UNID", 1);
	define("RECRUITMENT_BLAST_FROM_EMAIL_DEFAULT", "no-replies@fieldsolutions.us");

	// Project Level Recruitment Blast Settings
	define("AUTO_BLAST_SETTING_PROJECT", "(SELECT AutoBlastOnPublish FROM projects WHERE projects.Project_ID = work_orders.Project_ID)");
	define("RECRUITMENT_BLAST_FROM_EMAIL_PROJECT", "(SELECT RecruitmentFromEmail FROM projects WHERE projects.Project_ID = work_orders.Project_ID)");

	// Determines which settings to use
//	define("WHICH_RECRUITMENT_EMAIL_SETTING", "(CASE WHEN UseMyRecruitEmailSettings = '1' THEN 'WO_LEVEL' WHEN (SELECT UseMyRecruitEmailSettings FROM TR_Client_Projects WHERE TR_Client_Projects.Project_ID = Work_Orders.Project_ID) = '1' THEN 'PROJECT_LEVEL' ELSE 'DEFAULT' END)");

	define("WHICH_RECRUITMENT_EMAIL_SETTING", "(CASE WHEN (EXISTS (SELECT projects.Project_ID FROM projects WHERE projects.Project_ID = work_orders.Project_ID)) THEN 'PROJECT_LEVEL' ELSE 'DEFAULT' END)");


//	define("WHICH_AUTO_BLAST_SETTING", "(CASE WHEN UseMyRecruitEmailSettings = '1' THEN AutoBlastOnPublish WHEN (SELECT UseMyRecruitEmailSettings FROM TR_Client_Projects WHERE TR_Client_Projects.Project_ID = Work_Orders.Project_ID) = '1' THEN " . AUTO_BLAST_SETTING_PROJECT . " ELSE 'TRUE' END)");
	define("WHICH_AUTO_BLAST_SETTING", "(CASE WHEN (EXISTS (SELECT projects.Project_ID FROM projects WHERE projects.Project_ID = work_orders.Project_ID)) THEN " . AUTO_BLAST_SETTING_PROJECT . " ELSE 1 END)");
//	define("WHICH_RECRUITMENT_BLAST_FROM_EMAIL", "(CASE WHEN Work_Orders.UseMyRecruitEmailSettings = '1' THEN RecruitmentFromEmail WHEN (SELECT UseMyRecruitEmailSettings FROM TR_Client_Projects WHERE TR_Client_Projects.Project_ID = Work_Orders.Project_ID) = '1' THEN " . RECRUITMENT_BLAST_FROM_EMAIL_PROJECT . " ELSE '" . RECRUITMENT_BLAST_FROM_EMAIL_DEFAULT . "' END)");
	define("WHICH_RECRUITMENT_BLAST_FROM_EMAIL", "(CASE WHEN (EXISTS (SELECT UseMyRecruitEmailSettings FROM projects WHERE projects.Project_ID = work_orders.Project_ID)) THEN " . RECRUITMENT_BLAST_FROM_EMAIL_PROJECT . " ELSE '" . RECRUITMENT_BLAST_FROM_EMAIL_DEFAULT . "' END)");

	// Fields for Recruitment Email
	//define("RECRUITMENT_EMAIL_FIELDS", "Description, City, State, Zipcode, StartDate, StartTime, EndTime, PayMax, SpecialInstructions, Amount_Per, Qty_Devices, StartRange, Duration, Latitude, Longitude, " . WHICH_AUTO_BLAST_SETTING . ", " . WHICH_RECRUITMENT_BLAST_FROM_EMAIL . ", WIN_NUM, Headline, IFNULL((SELECT Abbr FROM wo_categories WHERE WO_Category_ID = Category_ID), ''), (CASE WHEN ReminderAll = '1' THEN ReminderAll ELSE SMSBlast END), isProjectAutoAssign, Requirements, Tools, Project_Name, Company_Name, PcntDeduct, PcntDeductPercent");
        //075
        define("RECRUITMENT_EMAIL_FIELDS", "Description, City, State, Zipcode, StartDate, StartTime, EndTime, PayMax, SpecialInstructions, Amount_Per, Qty_Devices, StartRange, Duration, Latitude, Longitude, " . WHICH_AUTO_BLAST_SETTING . ", WorkAvailableEmailFrom, WIN_NUM, Headline, IFNULL((SELECT Abbr FROM wo_categories WHERE WO_Category_ID = Category_ID), ''), (CASE WHEN ReminderAll = '1' THEN ReminderAll ELSE SMSBlast END), isProjectAutoAssign, Requirements, Tools, Project_Name, Company_Name, PcntDeduct, PcntDeductPercent, Project_ID, WorkOrderOwner, RecruitmentFromEmail, Country");
        //end 075

	function queueRecruitmentEmail($id, $companyID) {
		// queues a WO UNID to be blasted
		if (!isset($_SESSION["queuedRecruitmentEmail"]))
			$_SESSION["queuedRecruitmentEmail"] = array();
		if (!array_key_exists($companyID, $_SESSION["queuedRecruitmentEmail"]))
			$_SESSION["queuedRecruitmentEmail"][$companyID] = array();
		$_SESSION["queuedRecruitmentEmail"][$companyID][] = $id;

	}

	function processQueuedRecruitmentEmail() {
		if (!isset($argc) && isset($_SESSION["queuedRecruitmentEmail"]) && is_array($_SESSION["queuedRecruitmentEmail"]) ) {
			$queuedItems = $_SESSION["queuedRecruitmentEmail"];
			foreach ($queuedItems as $companyID => $idList) {
//				echo "companyID = $companyID   ";
//				print_r($idList);
				$woInfo = getWOForRecruitmentEmailById($idList);
//				print_r($woInfo);
//				echo "<br/>";
				if ($woInfo)
					blastRecruitmentEmailMainSiteBG($woInfo, $companyID, "", 50, TRUE);
			}
		}
		if (!isset($argc))
			unset($_SESSION["queuedRecruitmentEmail"]);
	}

	function passActiveBlastSettingsToJS($info) {
		// converts results from getBlastSettingById to JS vars
		$activeBlastLevel = getBlastLevelDisplayName($info["ActiveBlastLevel"]);
		$autoBlastOnPublish = ($info["AutoBlastOnPublish"] ? "Yes" : "No");
		$blastFromEmailAddress = htmlentities($info["FromEmail"]);
		$ret = "<script type=\"text/javascript\">var activeBlastLevel = \"$activeBlastLevel\"; var autoBlastOnPublish = \"$autoBlastOnPublish\"; var blastFromEmailAddress = \"$blastFromEmailAddress\";</script>";
		return $ret;
	}

	function getBlastLevelDisplayName($name) {
		// get descriptive name for a Blast Level
		$text = "";
		switch ($name) {
			case "WO_LEVEL":
				$text = "Using Work Order Settings";
				break;
			case "PROJECT_LEVEL":
				$text = "Using Project Settings";
				break;
			default:
				$text = "Using Default Settings";
		}
		return $text;
	}

	function getBlastSettingById($id, $type = ID_TYPE_TB_UNID) {
		// get blast setting for a WO
		$type = ($type == ID_TYPE_TB_UNID ? "WIN_NUM" : "WO_ID");
		$db = Zend_Registry::get('DB');
		$sql = "SELECT " . WHICH_RECRUITMENT_EMAIL_SETTING . ", " . WHICH_AUTO_BLAST_SETTING . ", " . WHICH_RECRUITMENT_BLAST_FROM_EMAIL . " FROM work_orders WHERE $type=?";
		$wo = $db->fetchRow($sql, array($id), Zend_Db::FETCH_NUM);
		//$wo = caspioSelectAdv("Work_Orders", WHICH_RECRUITMENT_EMAIL_SETTING . ", " . WHICH_AUTO_BLAST_SETTING . ", " . WHICH_RECRUITMENT_BLAST_FROM_EMAIL, "$type  = '$id'", "", false, "`", "|", false);
		if (!$wo) return false;
		$ret = array();
		$ret["ActiveBlastLevel"] = $wo[0];
		$ret["AutoBlastOnPublish"] = $wo[1];
		$ret["FromEmail"] = $wo[2];
		$ret["FromEmail"] = ($ret["FromEmail"] == "" ? RECRUITMENT_BLAST_FROM_EMAIL_DEFAULT : $ret["FromEmail"]);
		return $ret;
	}

	function getWOForRecruitmentEmailByCriteria($criteria) {
		$db = Zend_Registry::get('DB');
		$sql = "SELECT " . RECRUITMENT_EMAIL_FIELDS . " FROM work_orders " . (empty($criteria) ? "" : "WHERE " . $criteria);
		return $db->fetchAll($sql, array(), Zend_Db::FETCH_NUM);
	}

	function getWOForRecruitmentEmailById($idList, $type = ID_TYPE_TB_UNID, $ignoreShowTech = FALSE, $mustBeProject = TRUE) {
		$showTech = "AND ShowTechs = '1'";
		if ($ignoreShowTech)
			$showTech = "";
		$projectCriteria = "AND IFNULL(Project_ID, '')  <> ''";
		if (!$mustBeProject)
			$projectCriteria = "";
		$type = ($type == ID_TYPE_TB_UNID ? "WIN_NUM" : "WO_ID");
		// gets WOs to blast recruitment emails to based on wo ids
		if (!is_array($idList) || !count($idList))
			return false;

		//$idList = implode(",", $idList);
		//$idList = caspioEscape($idList);

		$db = Zend_Registry::get('DB');
		$sql = "SELECT " . RECRUITMENT_EMAIL_FIELDS . " FROM work_orders WHERE $type IN (" . $db->quote($idList) . ") AND IFNULL(Tech_ID, '') = '' $projectCriteria AND Invoiced = '0' AND Deactivated = '0' $showTech";      
		return $db->fetchAll($sql, array(), Zend_Db::FETCH_NUM);
		//$woList = caspioSelectAdv("Work_Orders", RECRUITMENT_EMAIL_FIELDS, "$type IN ($idList) AND ISNULL(Tech_ID, '') = '' $projectCriteria AND Invoiced = '0' AND Deactivated = '0' $showTech", "", false, "`", "|", false);
		//return (isset($woList[0]) && $woList[0] == "" ? FALSE : $woList);
	}

	function getWOForRecruitmentEmailByProject($projectID, $state = "") {
		// gets WOs to blast recruitment emails to based on project and state
		$db = Zend_Registry::get('DB');
		$sql = "SELECT " . RECRUITMENT_EMAIL_FIELDS . " FROM work_orders WHERE " . ($state != "" ? "State = '$state' AND " : "") . "Project_ID = ? AND IFNULL(Tech_ID, '') = '' AND Invoiced = '0' AND Deactivated = '0' AND ShowTechs = '1'";
		return $db->fetchAll($sql, array($projectID), Zend_Db::FETCH_NUM);

		//$projectID = caspioEscape($projectID);
		//$woList = caspioSelectAdv("Work_Orders", RECRUITMENT_EMAIL_FIELDS, ($state != "" ? "State = '$state' AND " : "") . "Project_ID = '$projectID' AND ISNULL(Tech_ID, '') = '' AND Invoiced = '0' AND Deactivated = '0' AND ShowTechs = '1'", "", false, "`", "|", false);
		//return (isset($woList[0]) && $woList[0] == "" ? FALSE : $woList);
	}

	function blastRecruitmentEmailMainSiteBG($woList, $companyID, $comments = "", $distance = "50", $copySelf = FALSE, $manualBlast = FALSE, $returnBadZip = FALSE, $alternateFromEmail = "", $user = "") {
	//	 mail("vroine@fieldsolutions.com", "debugging", $woList);

        // vince debugging
/*        $contact = new ContactSettings();
        $doNotEmail = $contact->getEmailList();
        $woList = $contact->filterEmail($woList);*/

		$woList = serialize($woList);
		$copySelf = serialize($copySelf);
		$manualBlast = serialize($manualBlast);
		$returnBadZip = serialize($returnBadZip);
		if (!empty($_SESSION)) $user = $_SESSION["UserName"];
		exec("(/usr/bin/php -f " . dirname(__FILE__) . "/blastRecruitmentEmailMainSiteBG.php " . escapeshellarg($woList) . " " . escapeshellarg($companyID) . " " . escapeshellarg($comments) . " " . escapeshellarg($distance) . " " . escapeshellarg($copySelf) . " " . escapeshellarg($manualBlast) . " " . escapeshellarg($returnBadZip) . " " . escapeshellarg($alternateFromEmail) . " " . escapeshellarg($user) . ") > /dev/null 2>&1 &");
	}
	function blastRecruitmentEmailMainSite($woList, $companyID, $comments = "", $distance = "50", $copySelf = FALSE, $manualBlast = FALSE, $returnBadZip = FALSE, $alternateFromEmail = "", $user = "", $importId = "",$EmailBlastRadiusPreferredOnly = 0) {
		$db = Zend_Registry::get('DB');
		$details = new Core_Api_WosClass ();
		$client_topcomment = false;

		/* Blasts Recruitment Email to WOs in woList. WOs need to share the same company. Sample woList query:

		$woList = caspioSelectAdv("Work_Orders", "ISNULL(Description, ''), ISNULL(City, ''), ISNULL(State, ''), ISNULL(Zipcode, ''), ISNULL(CONVERT(VARCHAR, StartDate, 101), ''), ISNULL(StartTime, ''), ISNULL(EndTime, ''), ISNULL(PayMax, ''), ISNULL(SpecialInstructions, ''), Amount_Per, Qty_Devices, ISNULL(StartRange, ''), ISNULL(Duration, ''), ISNULL(Latitude, ''), ISNULL(Longitude, '')", ($state != "" ? "State = '$state' AND " : "") . "Project_ID = '$projectID' AND ISNULL(Tech_ID, '') = '' AND Invoiced = '0' AND Deactivated = '0' AND ShowTechs = '1'", "", false, "`", "|");*/

		if (!is_array($woList))
			return FALSE;
		if ($comments == "") {
			$comments = "------
			To apply for this and other opportunities, log in to your technician profile at <a href='https://www.fieldsolutions.com'>www.fieldsolutions.com</a>
			<br /> ------";
		}
		else {
			$client_topcomment = true;
			$comments = nl2br ($comments . "\n\n");
		}
			
		$size = sizeof($woList);
		if ($size == 0 || ($size == 1 && $woList[0] == ""))
			// No work orders found
			return 0;

		if (!empty($_SESSION)) $user = $_SESSION["UserName"];

		$emailSent = 0;

		$footer = "";

		if ($companyID == "FLS") {
			$footer = "\n\n\n\nNote: If you have not yet been through online training to be activated as a Tech with the FLS program\nand/or do not have a Badge and IDs yet, please do not respond to this email, you are not eligible.\nTo get yourself eligible, make sure that you have gone through training beginning, at the link below and<br/>that you have your Badge and IDs.\n\n<a href='http://www.flsupport.com/3.html'>FLS online training</a>";
		}

        $footer .= "\n\nHave you visited the new Tech Community website? Click <a href='http://groups.google.com/group/mytechnicianspace/'>here</a> for fun and information on the tech community";

		$footer .= "\n\nYou are receiving this email as a registered technician on www.fieldsolutions.com.\nClick <a href='http://www.fieldsolutions.com/unsubscribe/'>here</a> to stop receiving future e-mails from us.";

		$techFields = "DISTINCT PrimaryEmail, TechID";

		// find work orders in project
		$zipCodeLongLatMap = array();
		$proximityZipMap = array();
		$techEmailMap = array();

		// get project information
//			$projectInfo = caspioSelectAdv("TR_Client_Projects", "TOP 1 from_email", "Project_ID = '$projectID'", "", false, "`", "|");
//			$fromEmail = trim($projectInfo[0], "`");
		$badZip = array();

		// loop through work orders in project
		$techAlreadySent = array(0);

		foreach ($woList as $info) {
			$description = $info[0];
			$city = $info[1];
			$state = $info[2];
			$zipcode = $info[3];
			$startDate = $info[4];
			$startTime = $info[5];
			$endTime = $info[6];
			$payMax = $info[7];
			$specialInstructions = $info[8];
			$amountPer = $info[9];
			$qtyDevices = ($amountPer == "Device" ? "(" . $info[10] . " device(s))" : "");
			$startRange = $info[11];
			$duration = $info[12];
			$woLat = $info[13];
			$woLong = $info[14];
			$autoBlastOnPublish = $info[15] == 1;
			$fromEmail = $info[16];
			$wo_id = $info[17];
			$headline = $info[18];
			$category = $info[19];
			$SMSBlast = $info[20];
			$autoAssign = $info[21] == 1;
			$requirements = $info[22];
			$tools = $info[23];
			$projectName = $info[24];
			$companyName = $info[25];
			$PcntDeduct = $info[26] == 1;
            $PcntDeductPercent = $info[27];
            $ProjectID = $info[28];
                        $WorkOrderOwner = $info[29];
                        $RecruitmentFromEmail = $info[30];
                        //075
                        $country = $info[31];
                        //end 075
			if ($fromEmail == ""){
                            $workAvailableEmailFrom="";
                            $ApiWosClass = new Core_Api_WosClass();
                            $FutureWOInfo = $ApiWosClass->getFutureWOInfo($WorkOrderOwner);
                            //$ApiWosClass->getFutureWOInfo($WorkOrderOwner);
                            $FutureWorkAvailableEmailFrom = $FutureWOInfo['WorkAvailableEmailFrom'];
                            if(!empty($FutureWorkAvailableEmailFrom))
                            {
                                $workAvailableEmailFrom= $FutureWorkAvailableEmailFrom;
				}
                            else if(!empty($RecruitmentFromEmail))
                            {
                                $workAvailableEmailFrom = $RecruitmentFromEmail;
			}
                            else
                            {
                                $workAvailableEmailFrom = RECRUITMENT_BLAST_FROM_EMAIL_DEFAULT;
                            }
				
                            $fromEmail = $workAvailableEmailFrom;
			}

//			if ($alternateFromEmail != "")
//				$fromEmail = $alternateFromEmail;
			
			//override from email with info from future_wo_info table
			/*$future = new Core_FutureWorkOrderInfo();
			$row = $future->find($user)->toArray();
			if(count($row) > 0){
				$fromEmail = $row['WorkAvailableEmailFrom'];
			}*/

			if (!$autoBlastOnPublish && !$manualBlast && !$autoAssign) continue;

			$locationLine = "";
			if ($city == "" && $zipcode == "")
				$locationLine = "";
			else if ($city == "")
				$locationLine = "$zipcode - ";
			else if ($zipcode == "")
				$locationLine = "$city - ";
			else
				$locationLine = "$city $zipcode - ";

			$subjectLine = ($autoAssign ? "RUSH: " : "WIN#: {$wo_id} NEW ");

			if ($category == "" || $headline == "")
				$subjectLine .= "Work Available in your Area";
			else
				$subjectLine .= "$category - " . $locationLine . "$headline";

			//$deductMsg = $PcntDeduct ? "A 10% service fee will be deducted from your final total payment amount from this work order." : "NO service fee will be deducted from this work order";
                        if(empty($PcntDeductPercent) || $PcntDeductPercent == 0)
                        {
                            $PcntDeductPercentMsg = "No service fee will be deducted from your final total payment amount from this work order.";
                        }
                        else if($PcntDeductPercent == 1)
                        {
                            $PcntDeductPercentMsg = "You will be paid outside of the FieldSolutions Platform for this work order.";
                        }
                        else
                        {
                            $PcntDeductPercentMsg = "A ".($PcntDeductPercent * 100)."% service fee will be deducted from your final total payment amount from this work order.";
                        }
			$p2tmsg = $autoAssign ? "THIS WORK ORDER WILL BE ASSIGNED IMMEDIATELY TO THE FIRST BIDDER AT THE OFFER PRICE.

FS-RUSH(TM) is FieldSolutions' automatic assignment tool, which assigns work to the first matching bidder.

" : "";
			$message = $p2tmsg . "WIN#: $wo_id Go to <a href='https://www.fieldsolutions.com/techs/wosDetails.php?id=$wo_id'>https://www.fieldsolutions.com/techs/wosDetails.php?id=$wo_id</a> to apply!
			
Multiple Work Orders in your area are available within the same project. <a href='https://www.fieldsolutions.com/techs/wos.php?tab=techavailable&date_start=" . date("m/d/Y") . "'>Click here</a> to view all of your Available Work.

$subjectLine
Location: $city, $state, $zipcode
";

/*
Date: $startDate
	Start Time: $startTime
	End Time: $endTime
	Est On Site Arrival Time: $startRange
	Duration: $duration
*/

$visits = $details->GetWOVisitList ($wo_id);
$date_helper = new Dashboard_View_Helper_DisplayDate ();

$index = 1;
foreach ($visits as $visit) {
	$message .= "\nVisit #$index:";
	$message .= "\nStart Type: " . ($visit["StartTypeID"] == 1 ? "Firm Start Time" : "Start Time Window");
	$message .= "\nWork Start: " . $date_helper->displayDate ('n/j/Y', $visit["StartDate"]) . " " . $visit["StartTime"];
	$message .= "\nWork End: " . $date_helper->displayDate ('n/j/Y', $visit["EndDate"]) . " " . $visit["EndTime"];
	$message .= "\nDuration: " . $details->getDurationDesc ($visit["EstimatedDuration"]);
	$message .= "\nArrival Instructions: " . $visit["TechArrivalInstructions"];

	$index++;
}

$message .= "
	\nPay: $payMax PER $amountPer $qtyDevices
	$PcntDeductPercentMsg

	Description:
";

        $message = nl2br($message);
        $apicom = new Core_Api_CommonClass();
        if ($apicom->isStringHTML($description))
        {
            $message .= $description;
        } else
        {
            $message .= nl2br($description);
        }


        $message .= "Required Tools:<br/>";
        if ($apicom->isStringHTML($requirements))
        {
            $message .= $requirements;
        } else
        {
            $message .= nl2br($requirements);
        }

        $message .= "Special Instructions:<br/>";
        if ($apicom->isStringHTML($description))
        {
            $message .= $specialInstructions;
        } else
        {
            $message .= nl2br($specialInstructions);
        }

			// Proximity search around work order zipcode
			if (!array_key_exists($zipcode, $proximityZipMap)) {

				// Lookup work order zipcode's latitude and longitude
				if (!array_key_exists($zipcode, $zipCodeLongLatMap)) {
					if (($woLat != "" && $woLong != "") && ($woLat != "0" && $woLong != "0")) {
						// check against zip code table
						$sql = "SELECT Latitude,Longitude FROM zipcodes WHERE ZIPCode=? AND Latitude IS NOT NULL AND Longitude IS NOT NULL";
						$centerZipLookup = $db->fetchRow($sql, array($zipcode), Zend_Db::FETCH_NUM);
						//$centerZipLookup = caspioSelectAdv("USA_ZIPCodes", "TOP 1 Latitude, Longitude", "ZIPCode = '$zipcode'", "");
						if ($centerZipLookup) {
							$diffLat = abs($woLat - $centerZipLookup[0]);
							$diffLong = abs($woLong - $centerZipLookup[1]);
							if ($diffLat > 1 || $diffLong > 1) {
								$errorMsg = "Geocode difference of one degree or greater detected.

								Work order details
								UNID: $wo_id
								USA_ZIPCode Coordinate: {$centerZipLookup[0]}, {$centerZipLookup[1]}
								Geocoded Coordinate: $woLat, $woLong
								Difference: $diffLat, $diffLong
								Location: $city, $state, $zipcode

								";
								smtpMailLogReceived("Recruitment Email Blast Library", "no-replies@fieldsolutions.us", "codem01@gmail.com, support@fieldsolutions.com", "Work Order with Bad Geocoding Detected", $errorMsg, nl2br($errorMsg), "Recruitment Email");
//								smtpMailLogReceived("Recruitment Email Blast Library", "no-replies@fieldsolutions.us", "codem01@gmail.com", "Work Order with Bad Geocoding Detected", $errorMsg, nl2br($errorMsg), "Recruitment Email");
//								continue;
								$woLat = $centerZipLookup[0];
								$woLong = $centerZipLookup[1];
								try {
									$db->update('work_orders', array('Latitude' => $woLat, 'Longitude' => $woLong), $db->quoteInto('WIN_NUM=?', $wo_id));
									//caspioUpdate("Work_Orders", "Latitude, Longitude", "'$woLat', '$woLong'", "TB_UNID = '$wo_id'", false);
								}
								catch (SoapFault $fault){
									smtpMail("Recruitment Email Blast Library", "no-replies@fieldsolutions.us", "tngo@fieldsolutions.com", "Lat / Long update failed", "$fault", "$fault", "recruitmentEmailLib.php");
								}
							}
						}
						$latitude = $woLat;
						$longitude = $woLong;
					}
					else {
						$sql = "SELECT Latitude,Longitude FROM zipcodes WHERE ZIPCode=? AND Latitude IS NOT NULL AND Longitude IS NOT NULL";
						$centerZipLookup = $db->fetchRow($sql, array($zipcode), Zend_Db::FETCH_NUM);
						//$centerZipLookup = caspioSelectAdv("USA_ZIPCodes", "TOP 1 Latitude, Longitude", "ZIPCode = '$zipcode'", "");
						if ($centerZipLookup) {
							$latitude = $centerZipLookup[0];
							$longitude = $centerZipLookup[1];
						}
						else if ($coord = geocodeAddress($zipcode)) {
							$latitude = $coord[0];
							$longitude = $coord[1];
						}
						else {
							// bad zipcode
//								echo "<div align=\"center\"><br/>Work Order Zip Code is not valid: $zipcode ... ignoring this Work Order</div>";
							$badZip[] = $zipcode;
							continue;
						}
					}
					$zipCodeLongLatMap[$zipcode][0] = $latitude;
					$zipCodeLongLatMap[$zipcode][1] = $longitude;
				}
				else {
					// cached zipcode long / lat
					$latitude = $zipCodeLongLatMap[$zipcode][0];
					$longitude = $zipCodeLongLatMap[$zipcode][1];
				}
				$ISOIDs = $db->fetchCol("SELECT UNID FROM iso WHERE Client_ID=?", array($companyID));
				$deniedTechs = $db->fetchCol("SELECT TechID FROM client_denied_techs WHERE Company_ID=?", array($companyID));
				// Do proximity search using latitude and longitude
				//14024
                                //075
				$select = $db->select();
				$select->from(Core_Database::TABLE_TECH_BANK_INFO, array('PrimaryEmail', 'TechID'))
					->distinct()
					->where("(Private = '0'" . (count($ISOIDs)? ' OR ISO_Affiliation_ID IN (' . implode(',', $ISOIDs) . ')': '') . ") AND HideBids != '1' AND IFNULL(PrimaryEmail, '') <> '' AND AcceptTerms = 'Yes' AND Deactivated != '1' AND EmailContactOptOut != '1'" . (count($deniedTechs)? ' AND TechID NOT IN ('. implode(',', $deniedTechs) . ')' : ''). " AND Country='".$country."'");
				//end 075
                                
				if (!empty($importId)) $select->where("NOT EXISTS (SELECT 1 FROM " . Core_Database::TABLE_IMPORT_TECH_EMAILED . " WHERE importId = ?)", $importId);
				$Core_Tech = new Core_Tech();
				if ($EmailBlastRadiusPreferredOnly == 1) {
					$listTechIDPreferred = $Core_Tech->getPreferredTechIds($companyID);
					if (!empty($listTechIDPreferred)) $select->where("TechID in (?)", $listTechIDPreferred);
				}
				$select = Core_Database::whereProximity($select, $latitude, $longitude, "Latitude", "Longitude", $distance);
				$techList = $db->fetchAll($select);
                                
                                //14024
				if (!empty($importId)) {
					$select = $db->select();
					$select->from(Core_Database::TABLE_TECH_BANK_INFO, array('PrimaryEmail', 'TechID'))
						->distinct()
						->where("(Private = '0'" . (count($ISOIDs)? ' OR ISO_Affiliation_ID IN (' . implode(',', $ISOIDs) . ')': '') . ") AND HideBids != '1' AND IFNULL(PrimaryEmail, '') <> '' AND AcceptTerms = 'Yes' AND Deactivated != '1' AND EmailContactOptOut != '1'" . (count($deniedTechs)? ' AND TechID NOT IN ('. implode(',', $deniedTechs) . ')' : ''));

					$select = Core_Database::whereProximity($select, $latitude, $longitude, "Latitude", "Longitude", $distance);
					$totalTechList = $db->fetchAll($select);
				}
				else
					$totalTechList = $techList;

				$excludedCount = ($totalTechList ? sizeof($totalTechList) : 0) - ($techList ? sizeof($techList) : 0);
				$techList = array($techList, $excludedCount);

//				$techList = caspioProximitySearchByCoordinatesRaw("TR_Master_List", false, $latitude, $longitude, "Latitude", "Longitude", "<= $distance", "3", $techFields, "AND (Private = '0'" . (count($ISOIDs)? ' OR ISO_Affiliation_ID IN (' . implode(',', $ISOIDs) . ')': '') . ") AND ISNULL(PrimaryEmail, '') <> '' AND AcceptTerms = 'Yes' AND Deactivated != '1'" . (count($deniedTechs)? ' AND TechID NOT IN ('. implode(',', $deniedTechs) . ')' : ''), "", false, "|", "`");
				//$techList = caspioProximitySearchByCoordinatesRaw("TR_Master_List", false, $latitude, $longitude, "Latitude", "Longitude", "<= $distance", "3", $techFields, "AND (Private = '0' OR (SELECT Client_ID FROM ISO WHERE UNID = ISO_Affiliation_ID) = '$companyID') AND ISNULL(PrimaryEmail, '') <> '' AND AcceptTerms = 'Yes' AND Deactivated != '1' AND NOT EXISTS (SELECT TR_Master_List.TechID FROM Deactivated_Techs_By_Clients WHERE Company_ID = '$companyID' AND Deactivated_Techs_By_Clients.TechID = TR_Master_List.TechID)", "", false, "|", "`");

				$proximityZipMap[$zipcode] = $techList;
			}
			else {
				// cached proximity search
				$techList = $proximityZipMap[$zipcode];
			}

			$techEmailMap = array();
			$techIDList = array();
			$techsEmailed = array();
			$excludedCount = $techList[1];
                        
			try {
				// Determine which techs already emailed by this import, filter out and mark remaining emailes as sent. Do all as one atomic action.
				if (!empty($importId)) {
					$select = $db->select();
					$select->from(Core_Database::TABLE_IMPORT_TECH_EMAILED, array('TechID'))
						->where("TechID NOT IN (?)", $techAlreadySent)
						->where("importId = ?", $importId);
					$db->beginTransaction();
					$filterOut = $db->fetchCol($select);
					if (!empty($filterOut)) {
						// filter out techs already emailed
						foreach ($filterOut as $id) {
							$techAlreadySent[$id] = true;
						}
					}
				}
                                //713
                                $Core_Api_Class = new Core_Api_Class();
                                $additionalWO = $Core_Api_Class->getWorkOrderAdditionalFields($wo_id);
                                $isDevryeligible =0;
                                if(!empty($additionalWO['AllowDeVryInternstoObserveWork']) || !empty($additionalWO['AllowDeVryInternstoPerformField']))
                                {
                                    $isDevryeligible = 1;
                                }
                                //end 713
                                //14146
                                $techAlreadySent = array();
                                //end 14146
				foreach ($techList[0] as $info) {
					$email = $info['PrimaryEmail'];
					$toId = $info['TechID'];
					if (array_key_exists($toId, $techAlreadySent)) {
						++$excludedCount;
						continue;
					}
                                        $TechClass = new Core_Api_TechClass();
                                        $hasDeVryTag = Core_Api_TechClass::hasDeVryTag($toId);
                                        $doesTechNeedToReceiveBlashEmailAndSMS = $TechClass->doesTechNeedToReceiveBlashEmailAndSMS($wo_id,$toId);
                                        if($doesTechNeedToReceiveBlashEmailAndSMS)
                                        {
                                            $techAlreadySent[$toId] = true;
                                            $techIDList[] = $toId;
                                            $techEmailMap["$email|$fromEmail"] = $message;
                                            $techsEmailed[] = "($importId, $toId)";
                                        }
				}
                                
				if (!empty($importId)) {
					// mark tech emailed
					if (!empty($techIDList))
						$db->query("INSERT INTO " . Core_Database::TABLE_IMPORT_TECH_EMAILED . " (importId, TechID) VALUES " . implode(",", $techsEmailed) . "");
					$db->commit();
				}
			}
			catch (Exception $e) {
				if (!empty($importId))
					$db->rollBack();
			}
			if ($copySelf) {
				if (!empty($alternateFromEmail)) // alternate email now used for copy self only
					$techEmailMap["$alternateFromEmail|$fromEmail"] = $message;
				else
					$techEmailMap["$fromEmail|$fromEmail"] = $message;
			}
			$thisWOEmailSent = 0;
			// cached proximity search
                            
                                
			$batchedList = array();
			$mapsize = sizeof($techEmailMap);
			$index = 0;
			foreach ($techEmailMap as $toFromAddr=>$message) {
				$toFromAddr = explode("|", $toFromAddr);
				$eList = $toFromAddr[0];
				$replyTo = $toFromAddr[1];
				$fromEmail =  $toFromAddr[1];//"new_work@fieldsolutions.com";
				$batchedList[] = $eList;
				$index++;

				if ($index % 200 == 0 || $mapsize == $index || $eList == $fromEmail) {
					//$message = "$comments\n\n$message $footer";
					//If client provided client, move to top.  If default comment
					//move to bottom.
					if ($client_topcomment)
						$message = "$comments $message $footer";
					else
						$message = "$message $comments $footer";
					if ($fromEmail == RECRUITMENT_BLAST_FROM_EMAIL_DEFAULT)
						$message = /*"DO NOT REPLY TO THIS EMAIL\n\n" .*/ $message;

					$htmlmessage = str_replace("------", "<hr/>", $message);

					if ($eList == $fromEmail) {
		//				echo $htmlmessage;
	//					smtpMailLogReceived("Call Coordinator", $fromEmail, $eList, $subjectLine, $message, $htmlmessage, "Recruitment Email");
						smtpMailLogReceived("Field Solutions", $fromEmail, $eList, $subjectLine, $message, $htmlmessage, "Recruitment Email", $replyTo);
					}
//					else {

					
/*				echo "*** BEGIN MESSAGE *** <br/>";
				echo "From: Call Coordinator &lt;$fromEmail&gt;<br/><br/>";
				echo "To: $eList<br/><br/>";
				echo "Subject: $subjectLine<br/><br/>";
				echo $htmlmessage;
				echo "<br/>*** END MESSAGE <br/>";*/

						smtpMailLogReceived("Field Solutions", $fromEmail, implode(",",$batchedList), $subjectLine, $message, $htmlmessage, "Recruitment Email", $replyTo);
						$batchedList = array();
//					}
				}
//				smtpMailLogReceived("Call Coordinator", $fromEmail, "testp2t@fieldsolutions.com", $subjectLine, $message, $htmlmessage, "Recruitment Email");

				$emailSent++;
				$thisWOEmailSent++;
			}

			if (sizeof($techIDList) > 0 && $SMSBlast) {
				$smsRecipientList = implode(",", getSMSEmailFromTechID($techIDList));
				$sms_message = "View more WOs from same project online or on FS-Mobile - WIN# $wo_id- $category- $locationLine $headline";
				$subjectLine2 = ($autoAssign ? "RUSH: " : "WIN#: {$wo_id} NEW ");

				$sms_htmlmessage = nl2br($sms_message);
//				smtpMail("Call Coordinator", $fromEmail, $smsRecipientList, $subjectLine, $sms_message, $sms_htmlmessage, "Recruitment Email");
				smtpMail("Field Solutions", $fromEmail, $smsRecipientList, $subjectLine2, $sms_message, $sms_htmlmessage, "Recruitment Email");
			}
			$thisWOEmailSent += $excludedCount;
			Create_TimeStamp($wo_id, caspioEscape($user), caspioEscape("Blast: $subjectLine ($thisWOEmailSent sent)"), $companyID, caspioEscape($companyName), caspioEscape($projectName), "", "", "");
		}


//		print_r($techEmailMap);

/*		echo "<div align=\"center\"><br/>";
		if ($copySelf) {
			echo "Copy sent to: $fromEmail<br/>";
		}
		echo "Emails sent: " . sizeof($techEmailMap) . "<br/>";
		echo "</div>";*/

		if (sizeof($badZip) > 0) {
			$badZipText = "'" . implode("', '", $badZip) . "'";
			$message = "One or more Work Orders contain an invalid zipcode. The invalid zip codes are: $badZipText";
//			echo $message;
			smtpMailLogReceived("Field Solutions", $fromEmail, "codem01@gmail.com", "Invalid Work Order Zipcode", $message, $message, "Recruitment Email");
		}

		if ($returnBadZip)
			return array($emailSent, $badZip);
		return $emailSent;
                
	}
	if (empty($argc)) {
?>

<script type="text/javascript">
 var blastFromAddressDefault = "<?=RECRUITMENT_BLAST_FROM_EMAIL_DEFAULT?>";
</script>
<?php
}
?>
