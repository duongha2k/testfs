<?php
	function getW9PrivateKey($password) {
		return getPrivateKey(realpath(dirname(__FILE__) . "/../../") . "/keys/w9_key.pri", $password);
	}

	function getW9PublicKey() {
		return getPublicKey(realpath(dirname(__FILE__) . "/../../") . "/keys/w9_key.pub");
	}

	function getBankingPrivateKey($password) {
		return getPrivateKey(realpath(dirname(__FILE__) . "/../../") . "/keys/key.pri", $password);
	}

	function getBankingPublicKey() {
		return getPublicKey(realpath(dirname(__FILE__) . "/../../") . "/keys/key.pub");
	}

	function getDellPrivateKey($password) {
		return getPrivateKey(realpath(dirname(__FILE__) . "/../../") . "/keys/dell_key.pri", $password);
	}

	function getDellPublicKey() {
		return getPublicKey(realpath(dirname(__FILE__) . "/../../") . "/keys/dell_key.pub");
	}
	
	

	function getPrivateKey($fn, $password) {
		if (!file_exists($fn)) return false;
		if (isset($password))
			$privkey = openssl_get_privatekey(file_get_contents($fn), $password);
		else
			$privkey = openssl_get_privatekey(file_get_contents($fn));
		if (!$privkey) return false;
		if (!openssl_pkey_export($privkey, $privatekey)) return false;
		return $privatekey;
	}

	function getPublicKey($fn) {
		if (!file_exists($fn)) return false;
		return file_get_contents($fn);
	}
	
	function encryptData($data, $key) {
		if (function_exists (openssl_public_encrypt) && openssl_public_encrypt($data, $encryptedData, $key)) return $encryptedData;
		return false;
	}
	
	function decryptData($data, $key) {
		if (function_exists (openssl_public_encrypt) && openssl_private_decrypt($data, $decryptedData, $key)) return $decryptedData;
		return false;
	}
?>