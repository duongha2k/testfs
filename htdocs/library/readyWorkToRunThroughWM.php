<?php
	if (!isset($argv[1])) die();
	$win_num = $argv[1];
	$assignment_id = $argv[2];

	if (!empty($win_num) && !empty($assignment_id)) {
		$dir = dirname(__FILE__);
		require_once($dir . "/bootstrapAPI.php");
		$wm = new Core_WorkMarket($win_num, $assignment_id);
		$wm->pullAssignmentInfo();
	}
exit();
