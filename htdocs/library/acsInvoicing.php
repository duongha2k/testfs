<?php
//	$page = "login";
//	ini_set("display_errors",1);
	require_once("caspioAPI.php");
	define("BUNDLED_ITEMS", 8);
	
	define("ACS_SELECTION", "ReminderAll, ReminderAcceptance, SMSBlast, Reminder24Hr, Reminder1Hr, CheckInCall, CheckOutCall, ReminderNotMarkComplete, ReminderIncomplete");
	define("NUM_ACS_SELECTED_ITEMS", "(CASE WHEN ReminderAll = '1' THEN " . BUNDLED_ITEMS . " ELSE CAST(ReminderAcceptance AS int) + CAST(SMSBlast AS int) + CAST(Reminder24Hr AS int) + CAST(Reminder1Hr AS int) + CAST(CheckInCall AS int) + CAST(CheckOutCall AS int) +  CAST(ReminderNotMarkComplete AS int) + CAST(ReminderIncomplete AS int) END)");
	define("WO_ACS_LOG_EXISTS", "(EXISTS (SELECT TOP 1 id FROM WOACSChangeLog WHERE WO_UNID = TB_UNID AND " . NUM_ACS_SELECTED_ITEMS . " > 0))");
	define("WO_ACS_HIGH_WATER_MARK", "(CASE WHEN " . WO_ACS_LOG_EXISTS . " THEN (SELECT TOP 1 " . NUM_ACS_SELECTED_ITEMS . " FROM WOACSChangeLog WHERE WO_UNID = TB_UNID ORDER BY " . NUM_ACS_SELECTED_ITEMS . " DESC) ELSE " . NUM_ACS_SELECTED_ITEMS . " END)");

	define("WO_ACS_HIGH_WATER_MARK_WHICH", "(CASE WHEN " . WO_ACS_LOG_EXISTS . " THEN (SELECT TOP 1 id FROM WOACSChangeLog WHERE WO_UNID = TB_UNID ORDER BY " . NUM_ACS_SELECTED_ITEMS . " DESC) ELSE 0 END)");
	
	define("HW_REMINDER_ALL", "(CASE WHEN " . WO_ACS_HIGH_WATER_MARK_WHICH . " != 0 THEN (SELECT ReminderAll FROM WOACSChangeLog WHERE id = " . WO_ACS_HIGH_WATER_MARK_WHICH . ") ELSE ReminderAll END)");
	define("HW_REMINDER_ACCEPTANCE", "(CASE WHEN " . WO_ACS_HIGH_WATER_MARK_WHICH . " != 0 THEN (SELECT ReminderAcceptance FROM WOACSChangeLog WHERE id = " . WO_ACS_HIGH_WATER_MARK_WHICH . ") ELSE ReminderAcceptance END)");
	define("HW_REMINDER_SMSBLAST", "(CASE WHEN " . WO_ACS_HIGH_WATER_MARK_WHICH . " != 0 THEN (SELECT SMSBlast FROM WOACSChangeLog WHERE id = " . WO_ACS_HIGH_WATER_MARK_WHICH . ") ELSE SMSBlast END)");
	define("HW_REMINDER_24HR", "(CASE WHEN " . WO_ACS_HIGH_WATER_MARK_WHICH . " != 0 THEN (SELECT Reminder24Hr FROM WOACSChangeLog WHERE id = " . WO_ACS_HIGH_WATER_MARK_WHICH . ") ELSE Reminder24Hr END)");
	define("HW_REMINDER_1HR", "(CASE WHEN " . WO_ACS_HIGH_WATER_MARK_WHICH . " != 0 THEN (SELECT Reminder1Hr FROM WOACSChangeLog WHERE id = " . WO_ACS_HIGH_WATER_MARK_WHICH . ") ELSE Reminder1Hr END)");
	define("HW_CHECK_IN_CALL", "(CASE WHEN " . WO_ACS_HIGH_WATER_MARK_WHICH . " != 0 THEN (SELECT CheckInCall FROM WOACSChangeLog WHERE id = " . WO_ACS_HIGH_WATER_MARK_WHICH . ") ELSE CheckInCall END)");
	define("HW_CHECK_OUT_CALL", "(CASE WHEN " . WO_ACS_HIGH_WATER_MARK_WHICH . " != 0 THEN (SELECT CheckOutCall FROM WOACSChangeLog WHERE id = " . WO_ACS_HIGH_WATER_MARK_WHICH . ") ELSE CheckOutCall END)");
	define("HW_REMINDER_NOT_MARK_COMPLETE", "(CASE WHEN " . WO_ACS_HIGH_WATER_MARK_WHICH . " != 0 THEN (SELECT ReminderNotMarkComplete FROM WOACSChangeLog WHERE id = " . WO_ACS_HIGH_WATER_MARK_WHICH . ") ELSE ReminderNotMarkComplete END)");
	define("HW_REMINDER_INCOMPLETE", "(CASE WHEN " . WO_ACS_HIGH_WATER_MARK_WHICH . " != 0 THEN (SELECT ReminderIncomplete FROM WOACSChangeLog WHERE id = " . WO_ACS_HIGH_WATER_MARK_WHICH . ") ELSE ReminderIncomplete END)");
	
	define("ACS_COST_BUNDLE", "ISNULL((SELECT TOP 1 IVRPriceBundle FROM TR_Client_Projects WHERE TR_Client_Projects.Project_ID = Work_Orders.Project_ID), 0.00)");
	define("ACS_COST_EACH", "ISNULL((SELECT TOP 1 IVRPriceEach FROM TR_Client_Projects WHERE TR_Client_Projects.Project_ID = Work_Orders.Project_ID), 0.00)");
	define("CALCULATED_ACS_COST", "(CASE WHEN " . WO_ACS_HIGH_WATER_MARK . " = " . BUNDLED_ITEMS . " THEN " . ACS_COST_BUNDLE . " ELSE " . WO_ACS_HIGH_WATER_MARK . " * " . ACS_COST_EACH . " END)");
	

//	print_r(caspioSelectAdv("Work_Orders", CALCULATED_ACS_COST, "TB_UNID = '7103'", "", false, "|", "`", false));
		
?>