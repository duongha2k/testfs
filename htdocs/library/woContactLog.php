<?php
	require_once ("caspioAPI.php");
	function logEmailContact($createdBy, $companyId, $tbuniId, $contact, $subject, $message) {
		$createdBy = caspioEscape($createdBy);
		$companyId = caspioEscape($companyId);
		$contact = caspioEscape($contact);
		$subject = caspioEscape($subject);
		$message = caspioEscape($message);
		
		caspioInsert("WO_Contact_Log", "Type, Created_Date, Created_By, Contact, Subject, Message, Company_ID, WorkOrder_ID", "'Email', GETDATE(), '$createdBy', '$contact', '$subject', '$message', '$companyId', '$tbuniId'");
	}
?>