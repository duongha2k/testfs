<?php
	if (empty($argc))
		require_once(realpath(dirname(__FILE__) . "/../") . "/headerStartSession.php");
	require_once("caspioAPI.php");

	function createWOACSTimeStamp($id) {
		if (!is_numeric($id)) return;
		$db = Zend_Registry::get('DB');
	    $info = $db->fetchRow("SELECT ReminderAll, ReminderAcceptance, Reminder24Hr, Reminder1Hr, CheckInCall, CheckOutCall, ReminderNotMarkComplete, ReminderIncomplete, SMSBlast FROM work_orders WHERE WIN_NUM=?", array($id), Zend_Db::FETCH_ASSOC);
		
		if (!empty($info)) {
		    $toInsert = $info;
		    $toInsert['WO_UNID'] = $id;
		    $toInsert['DateChanged'] = new Zend_Db_Expr('NOW()');
		    $db->insert('WOACSChangeLog', $toInsert);
		}
	}
?>
