<?php

    require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');

    $cfgSite = Zend_Registry::get('CFG_SITE');

    $googleMapApiKey = $cfgSite->get("site")->get("google_map_key");
    $googleMapClientId = $cfgSite->get("site")->get("google_map_client_id");

	function geocodeAddress($address) {
		global $googleMapApiKey, $googleMapClientId;
		// create a new cURL resource
		$ch = curl_init();
		$address = urlencode($address);
		// set URL and other appropriate options
		$options = array(CURLOPT_URL => "http://maps.google.com/maps/geo?q=$address&output=csv&client=$googleMapClientId&sensor=false",
						 CURLOPT_HEADER => false,
						 CURLOPT_RETURNTRANSFER => true
						);
		
		curl_setopt_array($ch, $options);
		
		// grab URL and pass it to the browser
		$response = curl_exec($ch);
		
		// close cURL resource, and free up system resources
		curl_close($ch);
		
		if ($response) {
			$info = explode(",", $response);
			if ($info[0] == "200") {
				$lat = $info[2];
				$long = $info[3];
				return array($lat, $long);
			}
		}		
		return false;
	}

	function googleMapApiJS($onReadyFunction = NULL) {
		global $googleMapApiKey, $googleMapClientId;
?>
    <!--script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=<?=$googleMapApiKey?>" type="text/javascript"></script-->
    <script src="https://maps-api-ssl.google.com/maps?file=api&v=2&client=<?=$googleMapClientId?>&sensor=false" type="text/javascript"></script>
    <script type="text/javascript">
    
    var map = null;
    var geocoder = null;
	var defaultMapZoomOut = 3;
	var defaultMapZoomIn = 6;
	var onPointReturned = null;
	var onPointReturnedFailed = null;
    
    function initialize() {
		try {
			if (GBrowserIsCompatible()) {
				map = new GMap2(document.getElementById("map_canvas"));
				map.setCenter(new GLatLng(39.8333, -98.5833), defaultMapZoomOut);
				map.addControl(new GLargeMapControl());
				geocoder = new GClientGeocoder();
				$(window).unload(GUnload);
			}
		} 
		catch (e) {}
    }
    
    function mapShowAddress() {
        address = $(this).attr("value");
		processAddress(address);
    }
	
	function processAddress(address) {
        if (geocoder) {
            geocoder.getLatLng(
                address,
                function(point) {
                    if (!point) {
                        map.clearOverlays();
                        map.setCenter(new GLatLng(39.8333, -98.5833), defaultMapZoomOut);
						if (onPointReturnedFailed != null) onPointReturnedFailed();
    //					alert(address + " not found");
                    } else {
                        map.clearOverlays();
                        map.setCenter(point, defaultMapZoomIn);
                        var marker = new GMarker(point);
                        map.addOverlay(marker);
    //					marker.openInfoWindowHtml(point.lat() + " " + point.lng());
						if (onPointReturned != null) onPointReturned(point);
                    }
                }
            );
        }
	}
    
    </script>

<?php
        if (!$onReadyFunction) {
?>
	<script type="text/javascript">
        $(document).ready(function() {
            // Google maps API setup
            initialize();
        });
    </script>
<?php
		}
		else {
?>
	<script type="text/javascript">
        $(document).ready(<?=$onReadyFunction?>);
    </script>
<?php
		}
	}
?>
