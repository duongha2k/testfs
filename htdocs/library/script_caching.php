<?php

function script_nocache ($script, $return_only = false) {
	$output = "";

	$widgets_pos = stripos ($script, "/widgets");
	if ($widgets_pos === false) {
		$fname = $_SERVER["DOCUMENT_ROOT"] . $script;
	}
	else {
		$fname = substr ($_SERVER["DOCUMENT_ROOT"], 0, -7) . $script;
	}

	if (file_exists ($fname)) {
		$timestamp = filemtime ($fname);
		$output = "<script type='text/javascript' src='$script?$timestamp'></script>";
	}
	else {
		$output = "Missing file: " . $script;
	}

	if (!$return_only)
		echo ($output);

	return $output;
}

?>