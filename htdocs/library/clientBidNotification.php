<?php
//	ini_set("display_errors",1);
	require_once("caspioAPI.php");
	require_once("smtpMail.php");
	function sendBidNotificationToClient($companyID, $projectID, $winNum, $clientWONum, $techFName, $techLName, $bidAmount, $comments, $techID = "unknown", $per = false) {
		if (!is_numeric($projectID)) return;
//		$info = caspioSelectAdv("TR_Client_Projects", "NotifyOnBid, To_Email", "Project_ID = '$projectID'", "", false, "`", "|", false);
		$db = Zend_Registry::get('DB');
		$sql = "SELECT NotifyOnBid, BidNotificationEmails FROM projects WHERE Project_ID = ?";
		$info = $db->fetchRow($sql, array($projectID), Zend_Db::FETCH_NUM);
		$notifyOnBid = $info[0] == 1;
		$clientEmail = $info[1];
		
		$wo = new Core_WorkOrder($winNum);
//		$wo->setData($winNum);
		$woClientEmail = $wo->getBidNotificationEmailTo();
		if($woClientEmail != "" && $woClientEmail != NULL){
			$clientEmail = $woClientEmail;
		}
		
		/*
		$future = Core_FutureWorkOrderInfo();
		$row = $future->find($info[2]);
		if(count($row) > 0){
			$row = $row->toArray();
			$clientEmail = $row[0]['BidNotificationEmailTo'];
		}*/

		if ($notifyOnBid || !empty($woClientEmail)) {
//			$subject = "ACTION: Bid Received: WIN# $winNum - Client WO ID $clientWONum";
//
//			$message = "You have received a bid on work order WIN# $winNum - Client WO ID $clientWONum\n\n";
//
//			$message .= "Technician $techFName $techLName (ID# $techID) has bid \$$bidAmount " . (!$per ? "" : "per $per ") . "with the following comment: $comments\n\n";
//
//			$message .= "To review this bid click here: https://www.fieldsolutions.com/clients/wosApplicant.php?v=$companyID&WorkOrderID=$winNum\n\n";
//
//			$message .= "REMINDER: You must be logged into Field Solutions site for the link to work correctly.\n\n";
//
//			$message .= "Thank you,\n";
//			$message .= "Your FieldSolutions Team\n\n";

			/*
			$subject = "ACTION: Bid Received on $clientWONum work order";
			$message = "You have received a bid on work order $clientWONum

Technician $techFName $techLName has bid \$$bidAmount with comments of $comments

To review this bid click here: <a href=\"https://www.fieldsolutions.com/clients/wosApplicant.php?v=$companyID&WorkOrderID=$winNum\">https://www.fieldsolutions.com/clients/wosApplicant.php?v=$companyID&WorkOrderID=$winNum</a>
REMINDER: (You must be logged into Field Solutions site for the link to work correctly)

Thank you,
Field Solutions";
			//*/
//			$clientEmail = "tngo@fieldsolutions.com";
                        
                        $Project_Name = $wo->getProject_Name();
                        $Headline = $wo->getHeadline();
                        $Region = $wo->getRegion();
                        $Route = $wo->getRoute();
                        $SiteName = $wo->getSiteName();
                        $SiteNumber = $wo->getSiteNumber();
                        $SiteAddress = $wo->getAddress()."\n". $wo->getCity().", ".$wo->getState()." ".$wo->getZipcode();
                        $startdate = $wo->getStartDate();
                        if(!empty($startdate))
                        {
                        $StartDate = date_format(new DateTime($startdate), "m/d/y");
                        }
                        else
                        {
                            $StartDate="";
                        }
                        $StartTime = $wo->getStartTime();

                        $techInfo = Core_Tech::getProfile($techID, true, API_Tech::MODE_TECH, $companyID);
                        $PrimaryPhoneStatus = $techInfo['PrimaryPhone'];
                        $SecondaryPhoneStatus = $techInfo['SecondaryPhone'];
                        $PrimaryEmail = $techInfo['PrimaryEmail'];
                        $subject = "ACTION: Bid Received: WIN# $winNum - Client WO ID $clientWONum";

			$message = "You have received a bid on work order WIN# $winNum - Client WO ID $clientWONum\n\n";
                        $message .= "Project: $Project_Name\n";
                        $message .= "Headline: $Headline\n";
                        $message .= "Region: $Region\n";
                        $message .= "Route: $Route\n";
                        $message .= "Site Name: $SiteName\n";
                        $message .= "Site #: $SiteNumber\n";
                        $message .= "Site Address: $SiteAddress\n";
                        $message .= "Start Date/Time: $StartDate $StartTime\n\n";

                        $message .= "Bid Amount: $".$bidAmount. (!$per ? "" : " per $per ")."\n";
                        $message .= "Bid Comments: $comments\n\n";

                        $message .= "FS Tech ID#: $techID\n";
                        $message .= "Tech Name: $techFName $techLName\n";
                        $message .= "Tech Phone (Day): $PrimaryPhoneStatus\n";
                        $message .= "Tech Phone (Evening): $SecondaryPhoneStatus\n";
                        $message .= "Tech Email: $PrimaryEmail\n\n";

			$message .= "To review this bid click here: https://www.fieldsolutions.com/clients/wosApplicant.php?v=$companyID&WorkOrderID=$winNum\n\n";

			$message .= "REMINDER: You must be logged into Field Solutions site for the link to work correctly.\n\n";

			$message .= "Thank you,\n";
			$message .= "Your FieldSolutions Team\n\n";
//                      Log bid notification email for test
//                        if (!empty($_SESSION)) $user = $_SESSION["UserName"];
//                        Core_Database::insert("WO_Contact_Log",
//                            array("Created_Date"=>date("Y-m-d g:i:s"),
//                                "Created_By"=>$user,
//                                "Type"=>"Email",
//                                "Contact"=>"Bid Notification Email",
//                                "Subject"=>$subject,
//                                "Message"=>nl2br($message),
//                                "Company_ID"=>$companyID,
//                                "WorkOrder_ID"=>$winNum
//                                )
//                            );

			smtpMail("FieldSolutions", "nobody@fieldsolutions.com", "$clientEmail", $subject, $message, nl2br($message), "Client Bid Notification");
		}
	}

?>
