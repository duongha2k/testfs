<?php
require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');


class ContactSettings {
//14024
    public function checkEmail($email){
        $db = Zend_Registry::get('DB');
        $result = $db->fetchAll("
        /* contactSettings::checkEmail */
        SELECT PrimaryEmail, Deactivated, EmailContactOptOut
        FROM TechBankInfo
        WHERE PrimaryEmail = '{$email}'
            AND EmailContactOptOut != '1'
            AND Deactivated  != '1'
            AND HideBids != '1'
        ");
        if(!empty($result['PrimaryEmail'])){
            //echo "<pre>"; print_r($result); echo "</pre>";
            return $result['PrimaryEmail'];
        } else {
            return false;
        }
    }


    public function filterEmail($eList){
            //echo "<br />Vince Debugging ";
        $eListArrayBase = explode(",", $eList);
        $eListArray = array();
        foreach($eListArrayBase as $emailElement){
            $emailCheck = $this->checkEmail($emailElement);
            if($emailCheck == $emailElement){
                $eListArray[] = $emailElement;
            }
        }
        $eList = implode($eListArray, ",");

        ob_start();

        //echo "<br />Email Filter";
        //echo "<pre>DNE list: "; print_r($this->doNotEmailList); echo "</pre>";
        //echo "<pre>eList: "; print_r($eList); echo "</pre>";

        $message = ob_get_clean();
        mail("seraphimpunk@gmail.com", "contactSettings::filterEmail", $message);
        //die("vince is debugging");

        return $eList;
    }
    
    public function getEmailList(){
                $db = Zend_Registry::get('DB');

                $result = $db->fetchAll("
                /* getEmailList Query */
                SELECT s.TechID, t.PrimaryEmail
                FROM TechBankInfo_settings s
                JOIN TechBankInfo t ON s.TechID = t.TechID
                WHERE setting = 'email'
                        AND value = '1'
                ");
                echo "<pre>Email - "; print_r($result); echo "</pre>";

                return $result;
        }

        public function getPhoneList(){

                $db = Zend_Registry::get('DB');

                $result = $db->fetchAll("
                /* getEmailList Query */
                SELECT s.TechID, t.PrimaryPhone
                FROM TechBankInfo_settings s
                JOIN TechBankInfo t ON s.TechID = t.TechID
                WHERE setting = 'sms'
                        AND value = '1'
                ");
                echo "<pre>SMS - "; print_r($result); echo "</pre>";

                return $result;
        }
    

}


?>