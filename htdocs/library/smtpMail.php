<?php
	require_once ("mySQLEmail.php");
	
	class PearMailWrapper {
		public function send($a, $b, $c) { }
	}
	
	// Send via SMTP
	
	$smtp = new PearMailWrapper;
		
	function contains_bad_str($str_to_test) {
		$bad_strings = array(
					"content-type:"
					,"mime-version:"
					,"multipart/mixed"
			,"Content-Transfer-Encoding:"
					,"bcc:"
			,"cc:"
			,"to:"
		);
	  
		foreach($bad_strings as $bad_string) {
			if(eregi($bad_string, strtolower($str_to_test))) {
				return $bad_string;
			}
		}
		return FALSE;
	}

	function createMessage($txt, $html, $message) {
	/*	Creates a Mail_Mime Object containing the text and html message
			$txt: text part of email
			$html: html part of email
			$message: a Mail_Mime Object create using: new Mail_Mime()
		Returns Mail_Mime Object
	*/
		$message->setTXTBody($txt);
		$message->setHTMLBody($html);
	}
	
	function mailerLog($fromName, $fromEmail, $eList, $subject, $message, $result, $calledFrom, $caller) {
		return;
	}
	
	
/*	function smtpMailBatch($fromName, $fromEmail, $eList, $subject, $txt, $html, $caller) {
		require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');
		$dbInfo = Zend_Registry::get("DB_Info");
		if ($dbInfo["dbname"] !== "gbailey_technicianbureau")
			$eList = "systemtest@fieldsolutions.us";
		$data = array("fromName" => $fromName, "fromEmail" => $fromEmail, "eList" => $eList, "subject" => $subject, "caller" => $caller, "HTTP_REFERER" => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "");
		$msgContent = array("txt" => $txt, "html" => $html);
		$data = mysql_real_escape_string(serialize($data));
		$msgContent = mysql_real_escape_string(serialize($msgContent));
		$numMsg = sizeof(explode(",", $eList));
		mysqlQueryEmail("INSERT INTO EmailBatch (data, msgContent, numEmail) VALUES ('$data', '$msgContent', $numMsg)");
	}*/
        
        //14110
	function smtpMailBatch($fromName, $fromEmail, $eList, $subject, $txt, $html, $caller, $replyTo = "", $ccList = "")
        {
            require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');

            //--- utf fix
            $txt = utf8_encode($txt);
            $html = utf8_encode($html);

            $dbInfo = Zend_Registry::get("DB_Info");
            if ($dbInfo["dbname"] !== "gbailey_technicianbureau")
            {
                $txt .= "\n\n[ To: " . $eList . " ]";
                $html .= "\n\n[ To: " . $eList . " ]";
                $eList = "systemtest@fieldsolutions.us,jcintron@fieldsolutions.com";
            }
            $eListArray = explode(",", $eList);
            $eListArray = array_chunk($eListArray, 200);
            foreach ($eListArray as $list)
            {
                $eList = implode(",", $list);

                $data = array("fromName" => $fromName, "fromEmail" => $fromEmail, "eList" => $eList, "subject" => $subject, "caller" => $caller, "HTTP_REFERER" => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : "", "replyTo" => $replyTo, "ccList" => $ccList);
                $msgContent = array("txt" => $txt, "html" => $html);
                $data = mysql_real_escape_string(serialize($data));
                $msgContent = mysql_real_escape_string(serialize($msgContent));
                $numMsg = sizeof(explode(",", $eList));
        //                        try {
                mysqlQueryEmail("INSERT INTO EmailBatch (data, msgContent, numEmail) VALUES ('$data', '$msgContent', $numMsg)");
        //                        } catch (Exception $e) {
        //                                echo $e;
        //                        }
            }
        }
        //end 14110
        
	function smtpMail($fromName, $fromEmail, $eList, $subject, $txt, $html, $caller, $replyTo = "") {
	/*	Sends an email using SMTP
			$fromName: Sender name
			$fromEmail: Sender address
			$eList: Comma separated reciepent addresses
			$subject: Subject of email
			$txt: text message
			$html: html message
			$caller: who is sending mail (ie. authenicated username)
	*/        
        //--- send emails
		smtpMailBatch($fromName, $fromEmail, $eList, $subject, $txt, $html, $caller, $replyTo);
	
	}
        
        //14110
	function smtpMailLogReceived($fromName, $fromEmail, $eList, $subject, $txt, $html, $caller, $replyTo = "", $ccList = "") {
	/*	Sends an email using SMTP and logs when email is opened
			$fromName: Sender name
			$fromEmail: Sender address
			$eList: Comma separated reciepent addresses
			$subject: Subject of email
			$txt: text message
			$html: html message
			$caller: who is sending mail (ie. authenicated username)
	*/        
        //--- send email
		smtpMailBatch($fromName, $fromEmail, $eList, $subject, $txt, $html, $caller, $replyTo, $ccList);
	}
        //end 14110
        
	function smtpMailLogReceivedBG($fromName, $fromEmail, $eList, $subject, $txt, $html, $caller, $delimiter) {
		// Sends email in background, every 60 seconds. Use for large # receipients
		// eList is an array

		if (is_array($eList)) $eList = implode(",", $eList);
		smtpMailBatch($fromName, $fromEmail, $eList, $subject, $txt, $html, $caller);
	}

	function reportError($fromName, $subject, $message, $caller) {
	// call to report automated script errors
		smtpMail($fromName, "nobody@fieldsolutions.com", "codem01@gmail.com, gbailey@fieldsolutions.com", $subject, "$message", "$message", $caller);
	}
	
	function returnBlockedAddresses($list) {
	// checks array of emails against blocked addresses and returns emails on blocked list
		$matches = mysqlFetchAssocEmail(mysqlQuery("SELECT email FROM blockedAddresses WHERE block = '1' AND email IN ('" . implode("','", $list) . "')"));
		$blocked = array();
		if ($matches) {
			foreach ($matches as $row)
				$blocked[$row["email"]] = 1;
		}
		return $blocked;
	}
		
	function parseBounces() {
		return;
	}
?>
