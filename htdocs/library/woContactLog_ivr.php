<?php
	require_once ("caspioAPI.php");

	function logIVRContact($createdBy, $companyId, $tbuniId, $contact, $subject, $message) {
		// Queue		
		$_SESSION["IVRContactLog"] = array($createdBy, $companyId, $tbuniId, $contact, $subject, $message);
	}
	
	function proccessQueuedIVRContactLog() {
		if (!isset($_SESSION["IVRContactLog"])) return;
		$log = $_SESSION["IVRContactLog"];
		unset($_SESSION["IVRContactLog"]);
		if (!is_array($log)) return;
		logIVRContactActual($log[0], $log[1], $log[2], $log[3], $log[4], $log[5]);
	}
	
	function logIVRContactActual($createdBy, $companyId, $tbuniId, $contact, $subject, $message) {
		$log = new Core_ContactLog();
		$data = array(
			'Type' => 'Phone',
			'Created_Date' => new Zend_Db_Expr('UTC_TIMESTAMP()'),
			'Created_By' => $createdBy,
			'Contact' => $contact,
			'Subject' => $subject,
			'Message' => $message,
			'Company_ID' => $companyId,
			'WorkOrder_ID' => $tbuniId
		);
        $result = $log->insert($data);
	}

?>