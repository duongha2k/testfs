<?php
// Rackspace version
	require_once ("Mail.php");
	require_once ("Mail/mime.php");
	require_once ("mySQL.php");
	
	// Send via SMTP
	$SMTPMailHost = "localhost";
	$SMTPMailUsername = "admin";
	$SMTPMailPassword = "GetBusy2009!";
	
	$smtp = Mail::factory('smtp',
		array ('host' => $SMTPMailHost,
		'auth' => true,
		'username' => $SMTPMailUsername,
		'password' => $SMTPMailPassword));
		
	function contains_bad_str($str_to_test) {
		$bad_strings = array(
					"content-type:"
					,"mime-version:"
					,"multipart/mixed"
			,"Content-Transfer-Encoding:"
					,"bcc:"
			,"cc:"
			,"to:"
		);
	  
		foreach($bad_strings as $bad_string) {
			if(eregi($bad_string, strtolower($str_to_test))) {
				return $bad_string;
			}
		}
		return FALSE;
	}

	function createMessage($txt, $html, $message) {
	/*	Creates a Mail_Mime Object containing the text and html message
			$txt: text part of email
			$html: html part of email
			$message: a Mail_Mime Object create using: new Mail_Mime()
		Returns Mail_Mime Object
	*/
		$message->setTXTBody($txt);
		$message->setHTMLBody($html);
	}
	
	function mailerLog($fromName, $fromEmail, $eList, $subject, $message, $result, $calledFrom, $caller) {
		$fromName = mysqlEscape($fromName);
		$fromEmail = mysqlEscape($fromEmail);
		$subject = mysqlEscape($subject);
		$message = mysqlEscape($message);
		$result = mysqlEscape($result);
		$calledFrom = ($calledFrom == "" ? "Unknown" : mysqlEscape($calledFrom));
		$caller = ($caller == "" ? "Unknown" : mysqlEscape($caller));
		$query = "INSERT INTO SMTPEmailLog (timestamp, received, vFromName, vFromEmail, vSubject, eList, vMessage, result, calledFrom, caller) VALUES (NOW(), '0', '$fromName', '$fromEmail', '$subject', '$eList', '$message', '$result', '$calledFrom', '$caller')";
		mysqlQuery($query);
		return mysql_insert_id();
	}
	
/*	function mailerLogReceived($fromName, $fromEmail, $eList, $subject, $message, $result, $calledFrom, $caller) {
		$fromName = mysqlEscape($fromName);
		$fromEmail = mysqlEscape($fromEmail);
		$subject = mysqlEscape($subject);
		$message = mysqlEscape($message);
		$result = mysqlEscape($result);
		$calledFrom = ($calledFrom == "" ? "Unknown" : mysqlEscape($calledFrom));
		$caller = ($caller == "" ? "Unknown" : mysqlEscape($caller));
		$rand = rand();
		$time = time() + "$rand";
		caspioInsert("SMTPEmailLog", "timestamp, findIdHack, received, vFromName, vFromEmail, vSubject, eList, vMessage, result, calledFrom, caller", "GetDate(), '$time', '0', '$fromName', '$fromEmail', '$subject', '$eList', '$message', '$result', '$calledFrom', '$caller'");
		$id = caspioSelect("SMTPEmailLog", "TOP 1 id", "findIdHack = '$time'", "");
		return $id[0];
	}	*/
        //14110
	function smtpMail($fromName, $fromEmail, $eList, $subject, $txt, $html, $caller, $replyTo = "", $ccList="") {
	/*	Sends an email using SMTP
			$fromName: Sender name
			$fromEmail: Sender address
			$eList: Comma separated reciepent addresses
			$subject: Subject of email
			$txt: text message
			$html: html message
			$caller: who is sending mail (ie. authenicated username)
	*/
	
		global $smtp;
		$from = "$fromName <$fromEmail>";
		$result = "";
		if (contains_bad_str($from) || contains_bad_str($subject)) {
			$result = "Possible email injection attempt";
			mailerLog($fromName, $fromEmail, $eList, $subject, $txt, $result, $_SERVER['HTTP_REFERER'], $caller);
			echo $result;
			die();
		}
		
		$recp =	explode(",", $eList);
		
		parseBounces();
		$blocked = returnBlockedAddresses($recp);

		foreach ($recp as $to) {
			if (trim($to) == "") {
				$result = "Blank to address";				
				mailerLog($fromName, $fromEmail, $eList, $subject, $txt, $result, $_SERVER['HTTP_REFERER'], $caller);
				continue;
			}
			if (array_key_exists($to, $blocked)) {
				// log blocked email address
				$result = "blocked email address";
				mailerLog($fromName, $fromEmail, $to, $subject, $txt, $result, $_SERVER['HTTP_REFERER'], $caller);			
				continue;
			}
			if (empty($replyTo)) $replyTo = $from;
                        //14110
                        if(!empty($ccList))
                        {
                            $extraheaders = array("To" => "$to", "From"=>"$from", "Subject"=>"$subject","Return-Path"=>"bounces@fieldsolutions.us","Reply-To" => $replyTo, "Cc"=>$ccList);
                        }
                        else
                        {
                            $extraheaders = array("To" => "$to", "From"=>"$from", "Subject"=>"$subject","Return-Path"=>"bounces@fieldsolutions.us","Reply-To" => $replyTo);
                        }
                        //end 14110
			$message = new Mail_mime();
			createMessage($txt, $html, $message);
                        
                        
			$body = $message->get();
			$headers = $message->headers($extraheaders);
			$mail = $smtp->send($to, $headers, $body);
			if (PEAR::isError($mail)) {
				$result = "Error sending to: $to";
				mailerLog($fromName, $fromEmail, $eList, $subject, $txt, $result, $_SERVER['HTTP_REFERER'], $caller);
//				echo $result;
//				die();
			}
		}
		$result = "Success";
		mailerLog($fromName, $fromEmail, $eList, $subject, $txt, $result, $_SERVER['HTTP_REFERER'], $caller);
	}
        //end 14110
        
	function smtpMailLogReceived($fromName, $fromEmail, $eList, $subject, $txt, $html, $caller) {
	/*	Sends an email using SMTP and logs when email is opened
			$fromName: Sender name
			$fromEmail: Sender address
			$eList: Comma separated reciepent addresses
			$subject: Subject of email
			$txt: text message
			$html: html message
			$caller: who is sending mail (ie. authenicated username)
	*/
	
		global $smtp;
		$from = "$fromName <$fromEmail>";
		$result = "";
		if (contains_bad_str($from) || contains_bad_str($subject)) {
			$result = "Possible email injection attempt";
			mailerLog($fromName, $fromEmail, $eList, $subject, $txt, $result, $_SERVER['HTTP_REFERER'], $caller);
			echo $result;
			die();
		}				
		$recp =	explode(",", $eList);
		
		parseBounces();
		$blocked = returnBlockedAddresses($recp);
		foreach ($recp as $to) {
			$result = "Success";
			if (trim($to) == "") {
				$result = "Blank to address";				
				mailerLog($fromName, $fromEmail, $eList, $subject, $txt, $result, $_SERVER['HTTP_REFERER'], $caller);
				continue;
			}
			if (array_key_exists($to, $blocked)) {
				// log blocked email address
				$result = "blocked email address";
				mailerLog($fromName, $fromEmail, $to, $subject, $txt, $result, $_SERVER['HTTP_REFERER'], $caller);			
				continue;
			}
			$id = mailerLog($fromName, $fromEmail, $to, $subject, $txt, $result, $_SERVER['HTTP_REFERER'], $caller);
			$img = "<img src='https://www.fieldsolutions.com/logView.php?id=$id'/>";
			$message = new Mail_mime();
			createMessage($txt, $html . $img, $message);
			$body = $message->get();
			$extraheaders = array("To"=>"$to", "From"=>"$from", "Subject"=>"$subject","Return-Path"=>"bounces@fieldsolutions.us");
			$headers = $message->headers($extraheaders);
			$mail = $smtp->send($to, $headers, $body);
			if (PEAR::isError($mail)) {
				$result = "Error sending to: $to";
				mailerLog($fromName, $fromEmail, $eList, $subject, $txt, $result, $_SERVER['HTTP_REFERER'], $caller);
//				echo $result;
//				die();
			}
		}
	}

	function smtpMailLogReceivedBG($fromName, $fromEmail, $eList, $subject, $txt, $html, $caller, $delimiter) {
		// Sends email in background, every 60 seconds. Use for large # receipients
		// eList is an array
		$eList = array_chunk($eList, 500);

		$delay = 0;
		foreach ($eList as $list) {
			$chunk = implode(",", str_replace($delimiter, "", $list));
			exec("(/usr/bin/php -f " . dirname(__FILE__) . "/smtpBG.php " . escapeshellarg($fromName) . " " . escapeshellarg($fromEmail) . " " . escapeshellarg($chunk) . " " . escapeshellarg($subject) . " " . escapeshellarg($txt) . " " . escapeshellarg($html) . " " . escapeshellarg($caller) . " " . escapeshellarg($delay) . ") > /dev/null 2>&1 &");
			$delay += 60;
		}
	}

	function reportError($fromName, $subject, $message, $caller) {
	// call to report automated script errors
		smtpMail($fromName, "nobody@fieldsolutions.com", "codem01@gmail.com, gbailey@fieldsolutions.com", $subject, "$message", "$message", $caller);
	}
	
	function returnBlockedAddresses($list) {
	// checks array of emails against blocked addresses and returns emails on blocked list
		$matches = mysqlFetchAssoc(mysqlQuery("SELECT email FROM blockedAddresses WHERE block = '1' AND email IN ('" . implode("','", $list) . "')"));
		$blocked = array();
		if ($matches) {
			foreach ($matches as $row)
				$blocked[$row["email"]] = 1;
		}
		return $blocked;
	}
		
	function parseBounces() {
		global $SMTPMailHost;
//		return;
//		$t0 = time();
		
		// throttle parsing
		
		$lastRan = mysqlQuery("SELECT lastRan + INTERVAL 600 SECOND < NOW() FROM parserLastRan LIMIT 0, 1");
		if (!$lastRan) return;
		if (mysql_num_rows($lastRan) > 0) {
			$lrDate = mysql_fetch_row($lastRan);
			if ($lrDate[0] == 1) 
				mysqlQuery("UPDATE parserLastRan SET lastRan = NOW() LIMIT 1");
			else
				return;
		}
		else
			mysqlQuery("INSERT INTO parserLastRan (lastRan) VALUES (NOW())");
			
		$mbox = imap_open ("{" . $SMTPMailHost . ":143/imap/novalidate-cert}INBOX", "bounces", "GetBusy2009!");
		
		$imap_obj = imap_check($mbox);
		
//		imap_clearflag_full($mbox, "39418:39458", "\\Seen");
		$match = imap_search($mbox, "UNSEEN");
//		var_dump($match);
		if (!$match) {
			imap_close($mbox);
			return;
		}
		$first_unread_msg_id = $match[0];
		$last_unread_msg_id = $match[sizeof($match) - 1];
		imap_setflag_full($mbox, implode(",", $match), "\\Seen");
								
		$dsm = array();
		$qmail = 0;
		$sgf = 0;
		$rgf = 0;
		$unk = 0;
		$warn = 0;
		$bounceList = array();
		
		// get msg_ids that need to be parsed
		$insertBouncesParsed = "";
		for ($i = $first_unread_msg_id; $i < $last_unread_msg_id; $i++) {
			// parse bounce message
//			$msg_id = $currentMsg["msg_id"];
//			$uidl = $currentMsg["uidl"];
			$msg_id = $i;
				
//			$insertBouncesParsed .= ($insertBouncesParsed != "" ? "," : "") . "('$uidl')";
							
			$headers = imap_header($mbox, $msg_id);

			$body = imap_body($mbox, $msg_id);

			$start = stripos($body, "Final-Recipient: rfc822;") + 24;
			$end = strpos($body, "\n", $start) - $start - 1;
			$dsm["Final-Recipent"] = trim(substr($body, $start, $end), " <>");
			$dsm["Status"] = "5.0.0";

			if ($headers->fromaddress == "MAILER-DAEMON@mail1.fieldsolutions.com (Mail Delivery System)") {
				// server generated failure
				$sgf++;
			}
			else if ($headers->return_pathaddress == "<>" && strpos($dsm["Final-Recipent"], "@") !== FALSE) {
				// remote generated failure
				$rgf++;
			}
			else {
				$start = stripos($body, "To:");
				if ($start !== FALSE) {
					$start += 3;
					$end = strpos($body, "\n", $start) - $start - 1;
					$dsm["Final-Recipent"] = trim(substr($body, $start, $end), " <>");
					$dsm["Status"] = "5.0.0";
				}
				if (array_key_exists("Final-Recipent", $dsm) && strpos($dsm["Final-Recipent"], "@") !== FALSE) {
					$rgf ++;
				}
				else {
					$unk++;
				}
			}
					
			$addrParts = explode("@", $dsm["Final-Recipent"]);
					
			if (array_key_exists(1, $addrParts) && $addrParts[1] != "fieldsolutions.com")
				$bounceList[$dsm["Final-Recipent"]] = 1;
		}
	
	
	// Parsed bounces info
//		print_r($bounceList);
//		echo "<br/><br/>";*/
		
//		echo "Unknown: $unk Qmail: $qmail RGF: $rgf SGF: $sgf Warn: $warn<br/><br/>";
		
		imap_close($mbox);
			
		if (sizeof($bounceList) > 0) {
			// add addresses to list of blocked emails
			$insertBlockedAddresses = "";
			foreach ($bounceList as $email => $val) {
				if (strpos($email, "fieldsolutions.com") !== FALSE) continue; // ignore fieldsolutions bounces
				$insertBlockedAddresses .= ($insertBlockedAddresses != "" ? "," : "") . "(NOW(), '1', NOW(), '$email')";
			}
			$insertBlockedAddresses = "INSERT INTO blockedAddresses (blockedDate, block, modifiedDate, email) VALUES " . $insertBlockedAddresses;
			mysqlQuery($insertBlockedAddresses);
		}
					
//		$tf = time();
//		echo $tf - $t0 . " secs";
	}
?>