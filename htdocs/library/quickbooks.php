<?php
	require_once("timeStamps2_QB.php");
	require_once("caspioAPI.php");
	require_once("mySQL.php");

	define("ISOID_PREFIX", "ISO-");

	function resultsToArray(&$value, $key, $delimiter = "'", $delimiterColumn = ",") {
		// converts capsio results to array
		$value = explode($delimiterColumn, $value);
		array_walk($value, 'trimDelimiter', $delimiter);
	}

	function resultsToArray2(&$value, $key, $delimiter = "'") {
		// converts capsio results to array
		if (is_array($delimiter)) {
			$delimiterColumn = $delimiter[1];
			$delimiter = $delimiter[0];
		}
		$value = explode($delimiterColumn, $value);
		array_walk($value, 'trimDelimiter', $delimiter);
	}


	function trimDelimiter(&$value, $key, $delimiter) {
		// removes string delimiter
		$value = trim($value, $delimiter);
	}

	function isInvalidArray($obj) {
		return ($obj == NULL || !is_array($obj) || sizeof($obj) == 0);
	}

	function copyBillableToMysql($workOrders, $type) {
		// each workorder requires -
		//		WO_ID, Tech_ID, PayAmount, Unique ID (int)
		if (isInvalidArray($workOrders)) return false;
		$insertValues = array();
		foreach ($workOrders as $order) {
			if (!array_key_exists(0, $order) || !array_key_exists(1, $order) || !array_key_exists(2, $order) || !array_key_exists(3, $order)) continue; // ignore WO with missing info
			$wo_id = mysqlEscape($order[0]);
			$member_id = $order[1];
			$pay_amount = $order[2];
			$caspio_unique_id = $order[3];
			$bill_date = $order[4];
			$insertValues[] = "('$wo_id', '$member_id', '$bill_date', '$pay_amount', '0', '$caspio_unique_id', '$type', NOW(), 'Caspio')";
		}
		if (sizeof($insertValues) > 0) {
			// insert billable WOs into mySQL
			$insertQuery = "INSERT INTO quickbooks_bills (wo_id, member_id, bill_date, pay_amount, is_paid, caspio_unique_id, caspio_type, last_update, last_update_by) VALUES " . implode(",", $insertValues) . " ON DUPLICATE KEY UPDATE wo_id = VALUES(wo_id), member_id = VALUES(member_id), bill_date = VALUES(bill_date), pay_amount = VALUES(pay_amount), is_paid = VALUES(is_paid), caspio_unique_id = VALUES(caspio_unique_id), caspio_type = VALUES(caspio_type), last_update = VALUES(last_update), last_update_by = VALUES(last_update_by)";
			return mysqlQuery($insertQuery);
		}
	}

	function copyPaidToCaspio($workOrders) {
		$db = Zend_Registry::get('DB');
		// each workorder requires -
		//		caspio_unique_id, caspio_type, paid_amount, paid_date, check_number
		if (isInvalidArray($workOrders)) return false;
		$updateValues = array();
		foreach ($workOrders as $order) {
			if (!array_key_exists(0, $order) || !array_key_exists(1, $order) || !array_key_exists(2, $order) || !array_key_exists(3, $order) || !array_key_exists(4, $order)) continue; // ignore WO with missing info
			$caspio_unique_id = $order[0];
			$caspio_type = $order[1];
			$paid_amount = $order[2];
			$paid_date = $order[3];
			$check_number = $order[4];
			print_r($order);
			if ($caspio_type == 0) {
				// main site
/*				$updateQuery = "UPDATE Work_Order SET TechPaid = '1', DatePaid = '$paid_date', PayAmount_Final = '$paid_amount', Check_Number = '$check_number' WHERE TB_UNID = '$caspio_unique_id'<br/>";
				echo $updateQuery;*/
				$woInfo = $db->fetchCol("SELECT Company_ID, Company_Name, Project_Name FROM work_orders WHERE WIN_NUM=?", array($caspio_unique_id));
				if ($woInfo) {
					Create_TimeStamp($caspio_unique_id, "Accounting", "Work Order Paid", $woInfo['Company_ID'], $woInfo['Company_Name'], $woInfo['Project_Name'], "", "", "");
					Create_TimeStamp($caspio_unique_id, "Accounting", "Work Order Complete", $woInfo['Company_ID'], $woInfo['Company_Name'], $woInfo['Project_Name'], "", "", "");
				}
				$db->update('work_orders', array('TechPaid' => 1, 'DatePaid' => $paid_date, 'PayAmount_Final' => $paid_amount, 'Check_Number' => $check_number, 'Status' => 'completed'), $db->quoteInto('WIN_NUM=?', $caspio_unique_id));
				//caspioUpdate("Work_Orders", "TechPaid, DatePaid, PayAmount_Final, Check_Number", "'1', '$paid_date', '$paid_amount', '$check_number'", "TB_UNID = '$caspio_unique_id'", false);
			}
			else {
				// FLS
/*				$updateQuery = "UPDATE FLS_Work_Order SET Paid = 'Yes', DatePaid = '$paid_date', PayAmount_Final = '$paid_amount', Check_Number = '$check_number' WHERE WorkOrderID = '$caspio_unique_id'<br/>";
				echo $updateQuery;*/
				caspioUpdate("FLS_Work_Orders", "Paid, DatePaid, PayAmount_Final, Check_Number", "'Yes', '$paid_date', '$paid_amount', '$check_number'", "WorkOrderID = '$caspio_unique_id'", false);
			}
		}
		return true;
	}

	function copyTechBankInfo($techIDList, $updateOnly = TRUE, $ISO = FALSE) {
		$tech = new Core_Tech;
		return $tech->copyTechBankInfo($techIDList, $updateOnly, $ISO);
		// copies any banking info changes to quickbook tables
	}

	function copyTechProfile($techIDList, $updateOnly = TRUE) {
		$tech = new Core_Tech;
		return $tech->copyTechProfile($techIDList, $updateOnly);
	}

	function appendISO($isoIDList) {
/*		foreach ($isoIDList as $key=>$value) {
			$isoIDList[$key] = ISOID_PREFIX . $value;
		}*/
		return $isoIDList;
	}

	function copyISOBankInfo($isoIDList, $updateOnly = TRUE) {
		if (isInvalidArray($isoIDList)) return false;
//		$isoIDList = appendISO($isoIDList);
		copyTechBankInfo($isoIDList, $updateOnly, TRUE);
	}

	function copyISOProfile($isoIDList, $updateOnly = TRUE) {
		// copies tech profile changes to quickbooks
		if (isInvalidArray($isoIDList)) return false;
		$isoIDListMysql = "'" . implode("','", $isoIDList) . "'";
		if ($updateOnly) {
			// restrict to only records currently in members table
			$result = mysqlQuery("SELECT member_id FROM quickbooks_iso WHERE member_id IN ($isoIDListMysql)");
			$list = array();
			while ($row = mysql_fetch_row($result)) {
				$list[] = $row[0];
			}
			if (sizeof($list) == 0) return true; // no records to update
			$isoIDListCaspio = "'" . implode("','", $list) . "'";
		}
		else
			$isoIDListCaspio = "'" . implode("','", $isoIDList) . "'";
		$result = mysqlQuery("SELECT UNID, Company_Name, Contact_Name, Contact_Phone, Contact_Email, Address_1, Address_2, City, State, Zip FROM iso WHERE UNID IN ($isoIDListCaspio)", "", false, "`", "|", false);

		$profiles = array();
		while ($row = mysql_fetch_row($result)) {
			$isoID = trim($row[0], "`");
			$companyName = mysqlEscape(trim($row[1], "`"));
			$firstName = "";
			$lastName = mysqlEscape(trim($row[2], "`"));
			$primaryPhone = mysqlEscape(trim($row[3], "`"));
			$primaryEmail = mysqlEscape(trim($row[4], "`"));
			$address1 = mysqlEscape(trim($row[5], "`"));
			$address2 = mysqlEscape(trim($row[6], "`"));
			$city = mysqlEscape(trim($row[7], "`"));
			$state = mysqlEscape(trim($row[8], "`"));
			$zip = mysqlEscape(trim($row[9], "`"));
			$profiles[] = "('$isoID', '$companyName', '$firstName', '$lastName', '$primaryPhone', '$primaryEmail', '$address1', '$address2', '$city', '$state', '$zip', NOW(), 'Caspio')";
		}
		if (sizeof($profiles) == 0) return false;
		$profiles = implode(",", $profiles);

		$insertQuery = "INSERT INTO quickbooks_iso (member_id, company_name, firstname, lastname, phone, email, address1, address2, city, state, zip, last_update, last_update_by) VALUES $profiles ON DUPLICATE KEY UPDATE member_id = VALUES(member_id), company_name = VALUES(company_name), firstname = VALUES(firstname), lastname = VALUES(lastname), phone = VALUES(phone), email = VALUES(email), address1 = VALUES(address1), address2 = VALUES(address2), city = VALUES(city), state = VALUES(state), zip = VALUES(zip), last_update = VALUES(last_update), last_update_by = VALUES(last_update_by)";

//		echo $insertQuery;

		return mysqlQuery($insertQuery);
	}

	function copyClientProfile($companyIDList) {
		// copies client profile changes to quickbooks

		if (isInvalidArray($companyIDList)) return false;
		$companyIDList = "'" . implode("','", $companyIDList) . "'";

		$profiles = caspioSelectAdv("TR_Client_List", "Company_ID, ClientID, CompanyName, ContactName, Email1, BusinessAddress, BusinessCity, BusinessState, BusinessZip, ContactPhone1, Country", "Company_ID IN ($companyIDList)", "PrimaryContact DESC", false, "`", "|");

		if (sizeof($profiles) == 0 || $profiles[0] == "") return false;

		$primaryProfiles = array();

		foreach ($profiles as $row) {
			$row = explode("|", $row);
			$companyID = trim($row[0], "`");
			if (array_key_exists($companyID, $primaryProfiles)) continue;
			// get only distinct company
			$clientID = mysqlEscape(trim($row[1], "`"));
			$companyName = mysqlEscape(trim($row[2], "`"));
			$name = explode(" ", trim($row[3], "`"));
			$firstName = mysqlEscape($name[0]);
			$lastName = (array_key_exists(1, $name) ? mysqlEscape($name[1]) : "");
			$email1 = mysqlEscape(trim($row[4], "`"));
			$businessAddress = mysqlEscape(trim($row[5], "`"));
			$businessCity = mysqlEscape(trim($row[6], "`"));
			$businessState = mysqlEscape(trim($row[7], "`"));
			$businessZip = mysqlEscape(trim($row[8], "`"));
			$contactPhone1 = mysqlEscape(trim($row[9], "`"));
			$country = mysqlEscape(trim($row[10], "`"));
			$primaryProfiles[$companyID] = "('$clientID', '$companyName', '$firstName', '$lastName', '$email1', '$businessAddress', '$businessCity', '$businessState', '$businessZip', '$contactPhone1', '$country' , NOW(), 'Caspio')";
		}

		if (sizeof($primaryProfiles) == 0) return false;

		$primaryProfiles = implode(",", $primaryProfiles);

		$insertQuery = "INSERT INTO clients (client_id, company_name, contact_firstname, contact_lastname, contact_email, address1, city, state, zip, phone, country, last_update, last_update_by) VALUES $primaryProfiles ON DUPLICATE KEY UPDATE client_id = VALUES(client_id), company_name = VALUES(company_name), contact_firstname = VALUES(contact_firstname), contact_lastname = VALUES(contact_lastname), contact_email = VALUES(contact_email), address1 = VALUES(address1), city = VALUES(city), state = VALUES(state), zip = VALUES(zip), country = VALUES(country), phone = VALUES(phone), last_update = VALUES(last_update), last_update_by = VALUES(last_update_by)";

		return mysqlQuery($insertQuery);
	}

	function getRequiredTechInfo() {
		// copies any tech profile required to process invoiced WOs
		$missingTechs = mysqlQuery("SELECT DISTINCT member_id FROM quickbooks_bills WHERE quickbooks_bills.caspio_sync = '0' AND quickbooks_bills.last_update_by = 'Caspio' AND NOT EXISTS (SELECT member_id FROM quickbooks_members WHERE quickbooks_bills.member_id = quickbooks_members.member_id)");

		$techList = array();
		while ($row = mysql_fetch_row($missingTechs)) {
			$techList[] = $row[0];
		}
		copyTechProfile($techList, FALSE);
		copyTechBankInfo($techList, FALSE);

		copyTechsToMysql($techList);

//		$missingISOs = mysqlQuery("SELECT DISTINCT quickbooks_members.iso_affiliation_id FROM quickbooks_members LEFT JOIN quickbooks_bills ON quickbooks_members.member_id = quickbooks_bills.member_id WHERE quickbooks_bills.caspio_sync = '0' AND quickbooks_bills.last_update_by = 'Caspio' AND quickbooks_members.iso_affiliation_id IS NOT NULL AND quickbooks_members.iso_affiliation_id <> ''");
		$missingISOs = mysqlQuery("SELECT DISTINCT m.iso_affiliation_id FROM  quickbooks_bills JOIN quickbooks_members AS m ON m.member_id = quickbooks_bills.member_id WHERE quickbooks_bills.caspio_sync = '0' AND quickbooks_bills.last_update_by = 'Caspio' AND m.iso_affiliation_id IS NOT NULL AND m.iso_affiliation_id <> '' AND NOT EXISTS (SELECT member_id FROM quickbooks_iso WHERE member_id = m.iso_affiliation_id)");

		$isoList = array();
		while ($row = mysql_fetch_row($missingISOs)) {
			$isoList[] = $row[0];
		}

		copyISOProfile($isoList, FALSE);
		copyISOBankInfo($isoList, FALSE);

		copyISOsToMysql($isoList);

//		$missingBanking = mysqlQuery("SELECT DISTINCT member_id FROM bills WHERE bills.caspio_sync = '0' AND bills.last_update_by = 'Caspio' AND NOT EXISTS (SELECT member_id FROM members WHERE bills.member_id = members.member_id AND (members.payment_method != 'Direct Deposit' AND members.payment_method != 'Check'))");

	}

	function copyTechsToMysql($techList) {
		if (isInvalidArray($techList)) return false;
		copyTechProfile($techList);
		copyTechBankInfo($techList);

		mysqlQuery("UPDATE quickbooks_members SET quickbooks_members.payment_method = 'Check' WHERE quickbooks_members.payment_method != 'Direct Deposit' AND quickbooks_members.payment_method != 'Check'");
		return true;
	}

	function copyISOsToMysql($isoList) {
		if (isInvalidArray($isoList)) return false;
		copyISOProfile($isoList);
		copyISOBankInfo($isoList);

		mysqlQuery("UPDATE quickbooks_iso SET quickbooks_iso.payment_method = 'Check' WHERE quickbooks_iso.payment_method != 'Direct Deposit' AND quickbooks_iso.payment_method != 'Check'");
		return true;
	}

/*	function getRequiredClients() {
		// copies any tech profile required to process invoiced WOs
		$missingTechs = mysqlQuery("SELECT member_id FROM bills WHERE caspio_sync = '0' AND last_update_by = 'Caspio' AND NOT EXISTS (SELECT members.member_id FROM members WHERE members.members_id = bills.members)");
		$techList = array();
		while ($row = mysql_fetch_row($missingTechs)) {
			$techList[] = $row[0];
		}
		copyTechProfile($techList);
	}*/

?>
