<?php
	class FSSession {
		public static function validateSessionParams($paramMapping, $params = null) {
			if ($params == null) $params = $_GET;
			if (!is_array($paramMapping) || sizeof($paramMapping) == 0) return true;
			$valid = true;
			$qs = array();
			foreach ($paramMapping as $k=>$v) {
				if ($params[$k] == $_SESSION[$v]) continue;
				$params[$k] = $_SESSION[$v];
				$valid = false;
			}
			foreach ($params as $k=>$v) {
				$qs[$k] = $k . "=" . $v;
			}
			if ($valid) return true;
	                header("Location: " . $_SERVER["PHP_SELF"] . "?" . implode("&",$qs));
			return false;
		}
	}
?>
