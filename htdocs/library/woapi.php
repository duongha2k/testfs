<?php
require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');
if (!defined("WOAPI_URL") ) {
    $cfgDb = new Zend_Config_Xml(APPLICATION_PATH.'/../../includes/configs/service.xml', 'service');
    define("WOAPI_URL", $cfgDb->url);
}
?>