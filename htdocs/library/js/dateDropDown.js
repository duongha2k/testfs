
	function filterDate() {
		try {
			test = currYr;
		} catch (e) {
			currYr = "";
		}

	$("#DateRange").attr("checked")
	$("select option:selected").each(function () {
		switch ($(this).text()) {
			case "Show All":
				showAll();
				break;		
			case "Today":
				showToday();
				break;		
			case "This Week":
				showWeek();
				break;
			case "Last Week":
				showLastWeek();
				break;
			case "This Month":
				showMonth();
				break;
			case "Last Month":
				lastMonth();
				break;
			case "Q1" + currYr:
				Q1();
				break;
			case "Q2" + currYr:
				Q2();
				break;
			case "Q3" + currYr:
				Q3();
				break;
			case "Q4" + currYr:
				Q4();
				break;
			case "H1" + currYr:
				H1();
				break;
			case "H2" + currYr:
				H2();
				break;
			case "YTD" + currYr:
				YTD();
				break;
		}
	});

	}

	function showAll() {
		$("#StartDate").attr("value", "");
		$("#EndDate").attr("value", "");
	}
	
	function showToday() {
		$("#StartDate").attr("value", Today);
		$("#EndDate").attr("value", Today);
	}
	
	function showWeek() {
		$("#StartDate").attr("value", BOW);
		$("#EndDate").attr("value", EOW);
	}

	function showLastWeek() {
		$("#StartDate").attr("value", BOLW);
		$("#EndDate").attr("value", EOLW);
	}

	function showMonth() {
		$("#StartDate").attr("value", BOM);
		$("#EndDate").attr("value", EOM);
	}

	function lastMonth() {
		$("#StartDate").attr("value", PrevBOM);
		$("#EndDate").attr("value", PrevEOM);
	}

	function Q1() {
		$("#StartDate").attr("value", Q1Start);
		$("#EndDate").attr("value", Q1End);
	}

	function Q2() {
		$("#StartDate").attr("value", Q2Start);
		$("#EndDate").attr("value", Q2End);
	}

	function Q3() {
		$("#StartDate").attr("value", Q3Start);
		$("#EndDate").attr("value", Q3End);
	}

	function Q4() {
		$("#StartDate").attr("value", Q4Start);
		$("#EndDate").attr("value", Q4End);
	}

	function H1() {
		$("#StartDate").attr("value", H1Start);
		$("#EndDate").attr("value", H1End);
	}

	function H2() {
		$("#StartDate").attr("value", H2Start);
		$("#EndDate").attr("value", H2End);
	}

	function YTD() {
		$("#StartDate").attr("value", YTDStart);
		$("#EndDate").attr("value", YTDEnd);
	}
