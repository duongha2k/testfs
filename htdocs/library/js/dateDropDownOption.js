
	function filterDate($DateRange,$StartDate,$EndDate) {            
		try {
			test = currYr;
		} catch (e) {
			currYr = "";
		}
	$($DateRange+" option:selected").each(function () {                
		switch ($(this).text()) {
			case "Show All":
				showAll($StartDate,$EndDate);
				break;		
			case "Today":
				showToday($StartDate,$EndDate);
				break;		
			case "This Week":
				showWeek($StartDate,$EndDate);
				break;
			case "Last Week":
				showLastWeek($StartDate,$EndDate);
				break;
			case "This Month":
				showMonth($StartDate,$EndDate);
				break;
			case "Last Month":
				lastMonth($StartDate,$EndDate);
				break;
			case "Q1" + currYr:
				Q1($StartDate,$EndDate);
				break;
			case "Q2" + currYr:
				Q2($StartDate,$EndDate);
				break;
			case "Q3" + currYr:
				Q3($StartDate,$EndDate);
				break;
			case "Q4" + currYr:
				Q4($StartDate,$EndDate);
				break;
			case "H1" + currYr:
				H1($StartDate,$EndDate);
				break;
			case "H2" + currYr:
				H2($StartDate,$EndDate);
				break;
			case "YTD" + currYr:
				YTD($StartDate,$EndDate);
				break;
		}
	});

	}

	function showAll($StartDate,$EndDate) {
                
		$($StartDate).attr("value", "");
		$($EndDate).attr("value", "");
	}
	
	function showToday($StartDate,$EndDate) {
		$($StartDate).attr("value", Today);
		$($EndDate).attr("value", Today);
	}
	
	function showWeek($StartDate,$EndDate) {
		$($StartDate).attr("value", BOW);
		$($EndDate).attr("value", EOW);
	}

	function showLastWeek($StartDate,$EndDate) {
		$($StartDate).attr("value", BOLW);
		$($EndDate).attr("value", EOLW);
	}

	function showMonth($StartDate,$EndDate) {
		$($StartDate).attr("value", BOM);
		$($EndDate).attr("value", EOM);
	}

	function lastMonth($StartDate,$EndDate) {
		$($StartDate).attr("value", PrevBOM);
		$($EndDate).attr("value", PrevEOM);
	}

	function Q1($StartDate,$EndDate) {
		$($StartDate).attr("value", Q1Start);
		$($EndDate).attr("value", Q1End);
	}

	function Q2($StartDate,$EndDate) {
		$($StartDate).attr("value", Q2Start);
		$($EndDate).attr("value", Q2End);
	}

	function Q3($StartDate,$EndDate) {
		$($StartDate).attr("value", Q3Start);
		$($EndDate).attr("value", Q3End);
	}

	function Q4($StartDate,$EndDate) {
		$($StartDate).attr("value", Q4Start);
		$($EndDate).attr("value", Q4End);
	}

	function H1($StartDate,$EndDate) {
		$($StartDate).attr("value", H1Start);
		$($EndDate).attr("value", H1End);
	}

	function H2($StartDate,$EndDate) {
		$($StartDate).attr("value", H2Start);
		$($EndDate).attr("value", H2End);
	}

	function YTD($StartDate,$EndDate) {
		$($StartDate).attr("value", YTDStart);
		$($EndDate).attr("value", YTDEnd);
	}
