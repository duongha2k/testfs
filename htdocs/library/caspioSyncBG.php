<?php
if (!isset($argv[1])) die();

require_once("bootstrapAPI.php");
$tableCaspio = $argv[1];
$idColumnCaspio = $argv[2];
$idList = unserialize($argv[3]);
$delete = unserialize($argv[4]);

Core_Caspio::init(); //May be it should be moved to bootstrap
Core_Database_CaspioSync::sync($tableCaspio, $idColumnCaspio, $idList, $delete);
