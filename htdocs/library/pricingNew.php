<?php
//	ini_set("display_errors",1);
//	require_once("bootstrapAPI.php");
//	require_once("caspioAPI.php");
//	require_once("smtpMail.php");
	define("TABLEID_MAIN", 0);
	define("TABLEID_FLS", 1);
	define("PROJECT_LEVEL_PRICING_RULE", "(SELECT IFNULL(p.PricingRuleID,0) FROM projects AS p WHERE p.Project_ID = work_orders.Project_ID)");
	define("CLIENT_LEVEL_PRICING_RULE", "(SELECT IFNULL(c.PricingRuleID,0) FROM clients AS c WHERE c.Company_ID = work_orders.Company_ID ORDER BY c.Admin DESC, c.ClientID LIMIT 1)");
	define("WHICH_PRICING_RULE", "(CASE WHEN IFNULL(PricingRuleID, 0) <> 0 THEN PricingRuleID WHEN IFNULL(Project_ID,'') <> '' AND " . PROJECT_LEVEL_PRICING_RULE . " <> 0 THEN " . PROJECT_LEVEL_PRICING_RULE . " ELSE " . CLIENT_LEVEL_PRICING_RULE . " END)");

//	print_r($pricingFieldList);

	$pricingFieldList = array();
	$pricingFieldListJS = array();
	$fieldDesign = array();
	$rulesAssignedJS = array();

	initPricing();

	function initPricing() {
		global $pricingFieldList, $pricingFieldListJS, $fieldDesign, $rulesAssignedJS;

		$fieldDesign = Core_Pricing::getFieldDesign();

		// Get fields available to perform pricing calculations on
		$pricingFieldList = Core_Pricing::getFields();

		foreach ($pricingFieldList as $rule => $field) {
			$displayName = $field[0];
			$actualName = $field[1];
			$type = $field[2];
			$pricingFieldListJS[] = "$rule : new Array(\"$displayName\", \"$actualName\", \"$type\")";
		}
		$pricingFieldListJS = "var pricingFieldList = {" . implode(",", $pricingFieldListJS) . "};\n";
		$pricingRules = getPricingRulesAssigned();

//		$rulesAssigned = array();
		foreach ($pricingRules as $rule) {
			if ($rule == "") continue;
			$rulesAssignedJS[] = "{$rule[0]} : " . ($rule[1] == 0 ? "false" : "true");
		}

		$rulesAssignedJS = "var rulesAssigned = {" . implode(",", $rulesAssignedJS) . "};\n";
	}

	function getPricingRulesAssigned() {
		return array("");
//		return caspioSelectAdv("Pricing_Rules", "PricingRuleID, CASE WHEN (EXISTS (SELECT TOP 1 Pricing_Rules.PricingRuleID FROM TR_Client_List WHERE Pricing_Rules.PricingRuleID = TR_Client_List.PricingRuleID) OR EXISTS (SELECT TOP 1 Pricing_Rules.PricingRuleID FROM TR_Client_Projects WHERE Pricing_Rules.PricingRuleID = TR_Client_Projects.PricingRuleID) OR EXISTS (SELECT TOP 1 Pricing_Rules.PricingRuleID FROM Work_Orders WHERE Pricing_Rules.PricingRuleID = Work_Orders.PricingRuleID)) THEN 1 ELSE 0 END", "", "", false, "`", "|", false);
	}

	function initPricingJS() {
		global $pricingFieldListJS, $rulesAssignedJS;
?>
		<script type="text/javascript">
			<?=$pricingFieldListJS?>
			<?=$rulesAssignedJS?>
			function getPricingField(id) {
				try {
					return pricingFieldList[id];
				}
				catch (e) {
					return false;
				}
			}
			function getPricingFieldDisplayName(id) {
				try {
					return pricingFieldList[id][0];
				}
				catch (e) {
					return false;
				}
			}
			function getPricingFieldActualName(id) {
				try {
					return pricingFieldList[id][1];
				}
				catch (e) {
					return false;
				}
			}
			function getPricingFieldType(id) {
				try {
					return pricingFieldList[id][2];
				}
				catch (e) {
					return false;
				}
			}
			function createSelectPricingFields(tagName, selectedValue, defaultOption) {
				if (defaultOption == undefined) {
					defaultOption = "";
				}
				if (selectedValue == undefined) {
					selectedValue = "";
				}
				pricingFieldSelect = "";
				for (key in pricingFieldList) {
					field = pricingFieldList[key];
					isSelected = (selectedValue == key ? "selected=\"selected\"" : "");
					pricingFieldSelect += "<option value=\"" + key + "\"" + isSelected + ">" + field[0] + "</option>";
				}
				return "<select id=\"" + tagName + "\"name=\"" + tagName + "\">" + defaultOption + pricingFieldSelect + "</select>";
			}
			function createSelectCalculationOrder(tagName, maxValue, selectedValue, defaultOption) {
				if (defaultOption == undefined) {
					defaultOption = "";
				}
				if (selectedValue == undefined) {
					selectedValue = "";
				}
				calcOrderSelect = "";
				if (selectedValue > maxValue)
					selectedValue = maxValue;
				for (var i=0; i <= maxValue; i++) {
					isSelected = (selectedValue == i ? "selected=\"selected\"" : "");
					calcOrderSelect += "<option value=\"" + i + "\"" + isSelected + ">" + i + "</option>";
				}
				return "<select id=\"" + tagName + "\"name=\"" + tagName + "\">" + defaultOption + calcOrderSelect + "</select>";
			}

			// sample calculation functions
			var inputValuesArray = null;
			function addField(index) {
				var myFieldID = this.name.split("sampleCalc")[1];
				inputValuesArray[myFieldID] = this.value;
			}
			function combineInputValues() {
				inputValuesArray = new Array();
				var field = $(".sampleCalcField");
				if (field.length == 0) return false;
				field.each(addField);
				return inputValuesArray;
			}
			function alertRuleAssigned() {
				alert("This rule is not editable because it has already been assigned");
				return false;
			}
			function queuePricingCalculation(woID) {
				var expireDate = new Date;
				expireDate.setDate(expireDate.getDate() + 1);
				document.cookie = "queuePricingCalculation<?=session_id()?>" + "=" + escape(woID) + ";path=/;expires="+ expireDate.toGMTString();
			}
		</script>
<?php
	}

	function createSelectPricingFields($tagName, $selectedValue = "",  $defaultOption = "", $tableID = TABLEID_MAIN) {
		global $pricingFieldList;
		// constructs select tag for selecting a pricing field
		$pricingFieldSelect = "";
		foreach ($pricingFieldList as $key=>$field) {
			$isSelected = ($selectedValue == $field[0] ? "selected=\"selected\"" : "");
			$pricingFieldSelect .= "<option value=\"$key\" $isSelected>" . $field[0] . "</option>";
		}
		return "<select id=\"$tagName\" name=\"$tagName\">$defaultOption" . $pricingFieldSelect . "</select>";
	}

	function createInputFields($inputFields, $defaultFields = NULL) {
		// creates html input elements for each input field
		if (!is_array($defaultFields))
			$defaultFields = array();
		if (!is_array($inputFields)) return FALSE;
		$htmlArray = array();
		foreach ($inputFields as $key=>$value) {
			$defaultValue = (array_key_exists($key, $defaultFields) ? $defaultFields[$key] : NULL);
			$fieldType = Core_Pricing::getFieldType($key);
			$fieldDisplayName = Core_Pricing::getFieldDisplayName($key);
//			echo "$fieldActualName $fieldType";
			switch ($fieldType) {
/*				case "ShortText":
				case "LongText":
				case "AutoNumber":
				case "Number":
				case "Date/Time":
				case "File":*/
				case 'tinyint': //'Yes/No'
					//FIXME bool?
					if ($defaultValue == "TRUE")
						$htmlArray[$fieldDisplayName] = "<select id=\"sampleCalc$key\" name=\"sampleCalc$key\" class=\"sampleCalcField\"><option value=\"TRUE\" selected=\"selected\">Yes</option><option value=\"FALSE\">No</option>";
					else
						$htmlArray[$fieldDisplayName] = "<select id=\"sampleCalc$key\" name=\"sampleCalc$key\" class=\"sampleCalcField\"><option value=\"TRUE\">Yes</option><option value=\"FALSE\" selected=\"selected\">No</option>";
					break;
				default:
					$defaultValue = ($defaultValue == NULL ? "0.00" : $defaultValue);
					$htmlArray[$fieldDisplayName] = "<input id=\"sampleCalc$key\" name=\"sampleCalc$key\" class=\"sampleCalcField\" value=\"$defaultValue\" size=\"5\"/>";
			}
		}
		return $htmlArray;
	}

	function getPricingField($id, $tableID = TABLEID_MAIN) {
		// returns a pricing field's actual and display names based on the pricing field ID
		global $pricingFieldList;
		return (array_key_exists($id, $pricingFieldList) ? $pricingFieldList[$id] : FALSE);
	}

	function getPricingFieldDisplayName($id, $tableID = TABLEID_MAIN) {
		// returns a pricing field's display names based on the pricing field ID
		global $pricingFieldList;
		return (array_key_exists($id, $pricingFieldList) ? $pricingFieldList[$id][0] : FALSE);
	}

	function getPricingFieldActualName($id, $tableID = TABLEID_MAIN) {
		// returns a pricing field's actual names based on the pricing field ID
		global $pricingFieldList;
		return (array_key_exists($id, $pricingFieldList) ? $pricingFieldList[$id][1] : FALSE);
	}

	function getPricingFieldType($id, $tableID = TABLEID_MAIN) {
		// returns a pricing field's actual names based on the pricing field ID
		global $pricingFieldList;
		return (array_key_exists($id, $pricingFieldList) ? $pricingFieldList[$id][2] : FALSE);
	}

	function moneyFormat($val) {
		return number_format($val, 2, ".", "");
	}

	function getChargeTypes($pricingRuleID) {
		// gets all charge types for a pricing rule
		return Core_Pricing::getChargeTypes($pricingRuleID);
	}

	function clonePricingRule($pricingRuleID) {
		return Core_Pricing::cloneRule($pricingRuleID);
			}

	function getNextCalculationOrder($pricingRuleID) {
		// gets the next available calculation order value
		return Core_Pricing::getNextCalculationOrder($pricingRuleID);
	}

	function findApplyWhenFields($applyWhen) {
		return Core_Pricing::findApplyWhenFields($applyWhen);
		}

	function chargeTypeCalculate($chargeType, $inputValues) {
		// process a charge type and return the calculated result
		return Core_Pricing::chargeTypeCalculate($chargeType, $inputValues);

			}

	function createPricingCalculationMatrix($chargeTypeList) {
		return Core_Pricing::createCalculationMatrix($chargeTypeList);
				}

	function performPricingCalculation($calculationMatrix, $inputValues, $pricingRuleID = "") {
		return Core_Pricing::performCalculation($calculationMatrix, $inputValues, $pricingRuleID);
			}

	function saveCalculatedValuesToWO($unid, $calcResults) {
		Core_Pricing::saveCalculatedValuesToWO($unid, $calcResults);
		}

	function queuePricingCalculation($unid) {
		if (!isset($_SESSION["queuePricingCalculation"]))
			$_SESSION["queuePricingCalculation"] = array();
		$_SESSION["queuePricingCalculation"][] = $unid;
	}

	function pricingErrorMsg($message) {
		smtpMail("Pricing Error", "nobody@fieldsolutions.com", "codem01@gmail.com", "Pricing Error", $message, $message, "Pricing Error");
	}

	function runPricingCalculation($unid) {
		Core_Pricing::runCalculation($unid);
		}

	function runPricingCalculationUsingChargeType($unid, $ruleID, $chargeTypes) {
		return Core_Pricing::runCalculationUsingChargeType($unid, $ruleID, $chargeTypes);
		}

	function processPricingCalculationQueue($ignoreApproved = FALSE) {
		setcookie("queuePricingCalculation", "", time() - 3600, "/");
		if (isset($_COOKIE["queuePricingCalculation" . session_id()])) {
			if (!isset($_SESSION["queuePricingCalculation"]))
				$_SESSION["queuePricingCalculation"] = array();
			$_SESSION["queuePricingCalculation"][] = $_COOKIE["queuePricingCalculation" . session_id()];

		}
		if (isset($_SESSION["queuePricingCalculation"])) {
//			$t0 = time();
			$queue = $_SESSION["queuePricingCalculation"];
			unset($_SESSION["queuePricingCalculation"]);
			foreach ($queue as $unid) {
				runPricingCalculation($unid);
			}
//			$tf = time();
//			echo $tf - $t0 . " secs";
		}
	}

	function getInputValuesFromWO($unid, $inputFieldList) {
		return Core_Pricing::getInputValuesFromWO($unid, $inputFieldList);
		}

	function batchPricingCalculationWithList($woList) {
		$batchErrorMessage = "";
		$cachedChargeTypes = array();

//		print_r($woList);
//		$ranList = array();
		$ranCount = 0;
		foreach ($woList as $wo) {
			$pricingRuleID = $wo[0];
			$unid = $wo[1];
			if ($pricingRuleID == "" || $pricingRuleID == 0) {
				$batchErrorMessage .= "No pricing rule attached to WO UNID: $unid<br/>";
				continue;
			}

			if (!array_key_exists($pricingRuleID, $cachedChargeTypes)) {
				$cachedChargeTypes[$pricingRuleID] = Core_Pricing::getChargeTypes($pricingRuleID);
				if (!$cachedChargeTypes[$pricingRuleID]) {
					$batchErrorMessage .= "Unable to find Charge Types for Rule: $pricingRuleID<br/>";
					continue;
				}
			}
			$chargeTypes = $cachedChargeTypes[$pricingRuleID];

			if (Core_Pricing::runCalculationUsingChargeType($unid, $pricingRuleID, $chargeTypes))
				$ranCount++;
//			$ranList[] = $unid;
		}
		if ($batchErrorMessage != "")
			pricingErrorMsg($batchErrorMessage);

		$totalWO = ($woList[0] == "" ? 0 : sizeof($woList));
		return array($totalWO, $ranCount, $batchErrorMessage);
//		if (sizeof($ranList) > 0)
//			caspioUpdate("Work_Orders", "PricingRan", "'1'", "TB_UNID IN ('" . implode("','", $ranList) . "')", false);
	}

	function batchPricingCalculation($sendReport = FALSE) {
		$db = Zend_Registry::get('DB');

		$sql = "SELECT " . Core_Pricing::WHICH_PRICING_RULE . ", WIN_NUM FROM work_orders WHERE Approved = '1' AND Invoiced = '0' AND PricingRan = '0' AND Deactivated = 0";
		$woList = $db->fetchAll($sql, array(), Zend_Db::FETCH_NUM);
		//$woList = caspioSelectAdv("Work_Orders", WHICH_PRICING_RULE . ", TB_UNID", "Approved = '1' AND Invoiced = '0' AND PricingRan = '0'", "", false, "`", "|", false);

		$result = batchPricingCalculationWithList($woList);

		if ($sendReport) {
			$message = "Total WOs: {$result[0]}
			Calc Ran: {$result[1]}
			Errors:

			{$result[2]}";
			$messageHTML = nl2br($message);
			smtpMail("Pricing Invoice Calculation", "nobody@fieldsolutions.com", "tngo@fieldsolutions.com, jsinning@fieldsolutions.com", "Invoice Calculations Ran", $message, $messageHTML, "Invoice Calculation");
		}

		return $result;
	}

?>