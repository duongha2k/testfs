<?php
	if (!isset($argv[1])) die();
	$winNum = $argv[1];
	$techId = $argv[2];
	$bid = $argv[3];
	$comments = $argv[4];
	$techFName = $argv[5];
	$techLName = $argv[6];
        
	if (!empty($techId)  && !empty($winNum)) {
		$dir = dirname(__FILE__);
		require ($dir . "/../clients/autoassign/includes/common.php");
		require_once($dir . "/caspioAPI.php");
		require_once($dir . "/timeStamps2.php");
		require ($dir . "/../clients/autoassign/includes/AutoAssign.php");
                
                $techClass = new Core_Api_TechClass();
                $isTechHideBid = $techClass->isTechHideBids($techId);
                if(!$isTechHideBid)
                {
		AutoAssign::assignFirstBidTech($winNum,$techId,$bid);

		require_once($dir . "/clientBidNotification.php");

/*		$winNum = $_GET['woId'];
		$bid = $_GET['bidAmount'];
		$comments = $_GET['comments'];
		$techFName = $_GET['firstName'];
		$techLName = $_GET['lastName'];*/
//		$info = caspioSelectAdv("Work_Orders", "Company_ID, Project_ID, WO_ID", "TB_UNID = '$winNum'", "", false, "`", "|", false);
		$db = Zend_Registry::get('DB');
		$sql = "SELECT Company_ID, Project_ID, WO_ID, Amount_Per FROM work_orders WHERE WIN_NUM=?";
		$result = $db->fetchRow($sql, array($winNum), Zend_Db::FETCH_NUM);
		$companyID = $result[0];
		$projectID = $result[1];
		$clientWONum = $result[2];
		$amount_per = $result[3];

		sendBidNotificationToClient($companyID, $projectID, $winNum, $clientWONum, $techFName, $techLName, $bid, substr($comments, 0, 150), $techId, $amount_per);
	}
	}
exit();
