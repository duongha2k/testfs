<?php
	require_once("../headerSimple.php");
	require_once("../library/caspioAPI.php");
//	if (parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST) != $_SERVER['HTTP_HOST']) die();
	if (!isset($_GET["id"]) || !is_numeric($_GET["id"]) || !isset($_GET["type"])) die();
	$id = $_GET["id"];
	$type = caspioEscape($_GET["type"]);
	// build dropdown of ranking source 
	$sourceList = caspioSelect("TR_Tech_Rankings", "DISTINCT RankingSource", "TechID = '$id'", "RankingSource ASC", false);
	$list = "";
	foreach ($sourceList as $row) {
		$row = trim($row, "'");
		$list .= "<option " . ($type == $row ? "selected='selected' " : "") . "value='$row'>$row</option>";
	}
	
	// build table of ratings
	$source = caspioSelect("TR_Tech_Rankings", "CONVERT(varchar, RankingDate, 101),  RankingSource, OverallRanking", "TechID = '$id' AND RankingSource = '$type'", "RankingDate DESC", false);
	if (sizeof($source) == 1 && $source[0] == "") die();
	$output = "<table id='ratingDetailsTable'><thead><tr><td>Date</td><td>Source</td><td>Rating</td></tr></thead><tbody>";
	foreach ($source as $row) {
		$row = explode(",", $row);
		$row[0] = trim($row[0], "'");
		$row[1] = trim($row[1], "'");
		$output .= "<tr><td>{$row[0]}</td><td>{$row[1]}</td><td>{$row[2]}</td></tr>";
	}
	$output .= "</tbody></table>";
?>

<style type="text/css">
	body {
		text-align: center;
	}
	
	#ratingDetailsTable {
		text-align: center;
		cursor: default;
		margin: 0px auto;
	}

	#ratingDetailsTable thead {
		background-color: #333399;
		color: #FFFFFF;
		font-weight: bold;
	}
	
	#ratingDetailsTable thead td {
		padding: 0px 40px;
	}
</style>

<script type="text/javascript">
	function sourceTypeChange() {
		document.location.replace("<?php echo $_SERVER['PHP_SELF']?>?id=<?=$id?>&type=" + this.value);
	}

	$(document).ready(
		function () {
			$('#myType').change(sourceTypeChange);
		}
	);
	
</script>

<body>
	Rating Type:
	<select id="myType" name="myType">
		<?=$list?>
	</select>
	
<?=$output?>

</body>
</html>