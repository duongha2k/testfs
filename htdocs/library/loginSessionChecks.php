<?php
	function checkAdminLogin2() {
		checkLogin2("admin");
	}

	function checkClientLogin() {
		checkLogin2("client");
	}

	function checkTechLogin() {
		checkLogin2("tech");
	}

	function checkAccountingLogin() {
		checkLogin2("admin");
	}

	function checkStaffLogin() {
		checkLogin2("staff");
	}

	function checkCustomerLogin() {
		checkLogin2("customers");
	}

	function checkFLSAdminLogin() {
		checkLogin2("FLSAdmin");
	}

	function checkFLSCCLogin() {
		checkLogin2("FLSCC");
	}

	function checkFLSManagerLogin() {
		checkLogin2("FLSManager");
	}

	function checkFLSInstallDeskLogin() {
		checkLogin2("FLSInstallDesk");
	}

	function checkLogin2($type) {
		if (isset($_SESSION['loggedIn']) && $_SESSION['loggedIn'] != 'yes' || isset($_SESSION['loggedInAs']) && $_SESSION['loggedInAs'] != $type) {

			// Modified by Sergey Petkevich (Warecorp)
            if ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
//                $_SESSION['redirectURL'] = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'];
            } else {
//                $_SESSION['redirectURL'] = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'];
            }

			$redir = dirname($_SERVER['REQUEST_URI']);
			$level = substr_count($redir, "/");
			if ($level > 1) {
				$redir = $redir . str_repeat("/../", $level - 1);
			}
			$_SESSION['redirectURL'] = $_SERVER['REQUEST_URI'];
			header('Location: ' . $redir);
			die();
		}
	}
?>
