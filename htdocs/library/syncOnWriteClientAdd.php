<?php
$company_id = $_GET["company_id"];
require_once("caspioAPI.php");

//require_once("{$_SERVER['DOCUMENT_ROOT']}/library/bootstrapAPI.php");
//ini_set("display_errors",1);
$tableReal = TABLE_CLIENT_LIST;

Core_Database_CaspioSync::sync($tableReal, "ClientID", array($_GET["id"]));

$db = Zend_Registry::get("DB");

$companyList = $db->fetchCol("SELECT DISTINCT Company_ID FROM clients WHERE Company_ID = ". $db->quoteInto("?", $company_id) . " AND NOT EXISTS (SELECT Project_ID FROM projects WHERE Project_Company_ID = Company_ID AND Project_Name = 'Default Project')");
if (sizeof($companyList) == 0) die();

foreach ($companyList as $co) {
	if (empty($co)) continue;	
	$db->insert(Core_Database::TABLE_PROJECTS, array("Project_Name" => "Default Project", "Active" => 1, "CreatedBy" => "fs_system", "From_Email" => "no-replies@fieldsolutions.us", "To_Email" => "no-replies@fieldsolutions.us", "WO_Category_ID" => 26, "Headline" => "General Assignment", "Project_Company_ID" => $co, "AutoBlastOnPublish" => 1));
}
