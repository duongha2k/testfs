<?php
	require_once("caspioAPI.php");
	require_once("projectTimeStamps.php");
	require_once("woACSTimeStamps.php");
	
	function acsUpdateChanges($id, $user, $create, $choice) {
		$projectInfo = createProjectTimeStamp($id, $user);
		if (!$create && $choice != 3) {
			// update ACS changes in project WOs
			$reminderAll = $projectInfo["reminderAll"];
			$reminderAcceptance = $projectInfo["reminderAcceptance"];
			$reminder24Hr = $projectInfo["reminder24Hr"];
			$reminder1Hr = $projectInfo["reminder1Hr"];
			$checkInCall = $projectInfo["checkInCall"];
			$checkOutCall = $projectInfo["checkOutCall"];
			$reminderNotMarkComplete = $projectInfo["reminderNotMarkComplete"];
			$reminderIncomplete = $projectInfo["reminderIncomplete"];
			$SMSBlast = $projectInfo["SMSBlast"];
			$applyTo = $projectInfo["applyTo"];
			$applyTo = trim($applyTo, "'");
			$applyTo = $applyTo == "DEFAULT" ? "" : $applyTo;
			if ($choice != $applyTo) die();
	
			$criteria = "Project_ID = '$id' AND TechMarkedComplete = '0' AND ACSInvoiced = '0' AND Approved = '0' AND Deactivated = '0'";
				
			if ($choice == 2) {
				// update only currently unassigned
				$criteria = "IFNULL(Tech_ID, '') = '' AND " . $criteria;
			}
			
	//		echo "$criteria";
//			$woList = caspioSelectAdv("Work_Orders", "TB_UNID", $criteria, "", false, "`", "|", false);
			$db = Zend_Registry::get('DB');
			$sql = "SELECT WIN_NUM FROM work_orders WHERE $criteria";
			$woList = $db->fetchAll($sql, array(), Zend_Db::FETCH_NUM);
			if (count($woList) > 0) {
//				$numUpdated = caspioUpdate("Work_Orders", "ReminderAll, ReminderAcceptance, Reminder24Hr, Reminder1Hr, CheckInCall, CheckOutCall, ReminderNotMarkComplete, ReminderIncomplete, SMSBlast", "'$reminderAll', '$reminderAcceptance', '$reminder24Hr', '$reminder1Hr', '$checkInCall', '$checkOutCall', '$reminderNotMarkComplete', '$reminderIncomplete', '$SMSBlast'", $criteria, false);
					
				$fields = array("ReminderAll" => $reminderAll, 
								"ReminderAcceptance" => $reminderAcceptance, 
								"Reminder24Hr" => reminder24Hr, 
								"Reminder1Hr" => $reminder1Hr, 
								"CheckInCall" => $checkInCall, 
								"CheckOutCall" => $checkOutCall, 
								"ReminderNotMarkComplete" => $reminderNotMarkComplete, 
								"ReminderIncomplete" => $reminderIncomplete, 
								"SMSBlast" => $SMSBlast );
				$numUpdated = $db->update('work_orders', $fields, $criteria);
				
				foreach ($woList as $unid) {
					if ($unid == "") continue;
					createWOACSTimeStamp($unid);
				}
			}
		}
		return $numUpdated;
	}
?>