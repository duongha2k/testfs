	var hideAction = null;
	var hideDelay = 500;
	function showDetails(data, textStatus) {
		if (data != "") {
			parts = data.split("|");
			imacs = parts[0];
			serviceCalls = parts[1];
			backOuts = parts[2];
			noShows = parts[3];
			recommendedAvg = parts[4] == "NULL" ? 0 : parseFloat(parts[4]);
			recommendedTotal = parts[5];
			performanceAvg = parts[6] == "NULL" ? 0 : parseFloat(parts[6]);
			performanceTotal = parts[7];
			total = parseInt(imacs, 10) + parseInt(serviceCalls, 10);
			html = "<table><thead><tr><td>Stats Last 12 Months</td><td>Total</td></tr></thead>";
			html += "<tr><td>IMAC Calls</td><td>" + imacs + "</td></tr>";
			html += "<tr><td>Service Calls</td><td>" + serviceCalls + "</td></tr>";
			html += "<tr><td>Back Outs</td><td>" + backOuts + "</td></tr>";
			html += "<tr><td>No Shows</td><td>" + noShows + "</td></tr>";
			html += "<tr><td>Preference</td><td>" + recommendedAvg.toFixed(1) + " (" + recommendedTotal + ")</td></tr>";
			html += "<tr><td>Performance</td><td>" + performanceAvg.toFixed(1) + " (" + performanceTotal + ")</td></tr>";
//			html += "<tr><td colspan='2'></td></tr><tfoot><tr><td colspan='2'>Total of " + total + "</td></tr></tfoot>";
			html += "</table>";
		}
		else {
			html = "No call information available.";
		}
		$('#ratingDetailsBox').html(html);
	}
	
	function showDetailsBox() {
		resetHideDetailsBox();
		var myid = (this.id.split("myRating",2))[1];
		var pos = $(this).offset();
		$.get("/ajax/techCallStats.php", { id: myid }, showDetails);
		var height = $('#ratingDetailsBox').height();
		$('#ratingDetailsBox').css({top: pos.top - (height / 2), left: pos.left + 30, display: "none"});
		$('#ratingDetailsBox').html("<img src='/images/loading.gif' />");
		$('#ratingDetailsBox').fadeIn("slow");
	}
	
	function delayedHideDetailsBox() {
		hideAction = setTimeout("hideDetailsBox();", hideDelay);
	}
	
	function hideDetailsBox() {
		$('#ratingDetailsBox').fadeOut("slow");
	}
	
	function resetHideDetailsBox() {
		clearTimeout(hideAction);
	}
	
	$(document).ready(
		function () {
			$('body').append("<div id='ratingDetailsBox'></div>");
			$('.ratingBox').hover(showDetailsBox, delayedHideDetailsBox);
			$('#ratingDetailsBox').hover(resetHideDetailsBox, delayedHideDetailsBox);
		}
	);
