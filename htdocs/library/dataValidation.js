function isValidTime(val) {
	if (val == null || val == "") return true;
	validFormat = /^(\d{1,2}):(\d{2})\s(AM|PM)$/;
	
	parts = val.match(validFormat);
	if (parts == null) return false;
	hour = parseInt(parts[1], 10);
	if (hour < 10 && parts[1].charAt(0) == "0") return false; // reject leading-zero
	minute = parseInt(parts[2], 10);
	return (hour >= 1 && hour <= 12 && minute >= 0 && minute <= 59);
}

function isValidTime24(val) {
	if (val == null || val == "") return true;
	validFormat = /^(\d{1,2}):(\d{2})$/;
	
	parts = val.match(validFormat);
	if (parts == null) return false;
	hour = parseInt(parts[1], 10);
	if (hour < 10 && hour != 0 && parts[1].charAt(0) == "0") return false; // reject leading-zero
	minute = parseInt(parts[2], 10);
	return (hour >= 0 && hour <= 23 && minute >= 0 && minute <= 59);
}

function isValidElapsedTime(val) {
	if (val == null || val == "") return true;
	validFormat = /^(\d{1,2}):(\d{2})$/;
	
	parts = val.match(validFormat);
	if (parts == null) return false;
	hour = parseInt(parts[1], 10);
	if (hour < 10 && parts[1].charAt(0) == "0") return true; // reject leading-zero
	minute = parseInt(parts[2], 10);
	return (hour >= 1 && hour <= 12 && minute >= 0 && minute <= 59);
}

function massageTime(val) {
	if (val == null || val == "") return val;
	validFormat = /^(\d{1,2})(?::?(\d{2}))?\s*(AM|PM|Am|Pm|aM|pM|am|pm)$/;
	parts = val.match(validFormat);
	if (parts == null) return val;
	hour = parseInt(parts[1], 10);
	if (hour < 10 && parts[1].charAt(0) == "0") parts[1] = hour; // remove leading-zero
	parts[2] = (parts[2] == undefined || parts[2] == "" ? "00" : parts[2]); 
	parts[3] = parts[3].toUpperCase(); // force AM/PM
	return parts[1] + ":" + parts[2] + " " + parts[3];
}

function massageTime24(val) {
	if (val == null || val == "") return val;
	val = val.toUpperCase();
	validFormat = /^(\d{1,2})(?::?(\d{0,2}))?\s*(AM|PM|A|P)?$/;
	parts = val.match(validFormat);
	if (parts == null) return val;
	hour = parseInt(parts[1], 10);
//	if (hour < 10 && parts[1].charAt(0) == "0") parts[1] = hour; // remove leading-zero
	parts[2] = (parts[2] == undefined || parts[2] == "" ? "00" : parts[2]); 
	parts[3] = (parts[3] == undefined ? "" : parts[3].toUpperCase()); // force AM/PM
	if ((parts[3] == "PM" || parts[3] == "P") && hour < 12)
		// adjust for PM
		hour += 12;
	parts[1] = hour;
	return parts[1] + ":" + parts[2];
}

function isValidPhone(val) {
	if (val == null || val == "") return true;
	validFormat = /^[(](\d{3})[)]\s(\d{3})[-](\d{4})$/;
	parts = val.match(validFormat);
	return (parts != null);
}

function massagePhone(val) {
	if (val == null || val == "") return val;
	validFormat = /^[(]?(\d{3})[)]?(?:\s|-|.)*(\d{3})[-\s.]*(\d{4})$/;
	parts = val.match(validFormat);
	if (parts == null) return val;
	return "(" + parts[1] + ") " + parts[2] + "-" + parts[3];
}

function isValidMoney(val) {
	if (val == null || val == "") return true;
	validFormat = /^[-]{0,1}(\d+)[.](\d{2})$/;
	parts = val.match(validFormat);
	return (parts != null);
}

function massageMoney(val) {
	if (val == null || val == "") return val;
	val = val.replace(",", "");
	validFormat = /^(-){0,1}[$]{0,1}(\d+)(?:.|.(\d{0,2}))?$/;
	parts = val.match(validFormat);
	if (parts == null) return val;
	sign = (parts[1] == "-" ? "-" : "");
	dollar = parts[2];
	cents = (parts[3] == undefined || parts[3] == "" ? "00" : parts[3]);
	cents = (cents.length == 1 ? cents + "0" : cents);
	val = sign + dollar + "." + cents;
	return val;
}

function isNumeric(val) {
	if (val == null || val == "") return true;
	validFormat = /^(\d+)$/;
	parts = val.match(validFormat);
	return (parts != null);
}

function isValidEmail(val) {
	if (val == null || val == "" || typeof(val) != 'string') return true;
	//validFormat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	//parts = val.match(validFormat);
	//return (parts != null);
    
    var re = new RegExp("^\\w+((-\\w+)|(\\.\\w+))*\\@[A-Za-z0-9]+((\\.|-)[A-Za-z0-9]+)*\\.[A-Za-z0-9]{2,4}$"); 
    return (val.search(re) != -1); 

//    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
//    if(reg.test(val) == false) {
// 
//      return false;
//    }
//    else
//    {
//       return true;
//    }
   
}


function isValidSSN(val) {
	if (val == null || val == "") return true;
    validFormat = /^([0-6]\d{2}|7[0-6]\d|77[0-2])([ \-]?)(\d{2})\2(\d{4})$/;
    if (!validFormat.test(val)) { return false; }
    var temp = val;
    if (val.indexOf("-") != -1) { temp = (val.split("-")).join(""); }
    if (val.indexOf(" ") != -1) { temp = (val.split(" ")).join(""); }
    if (temp.substring(0, 3) == "000") { return false; }
    if (temp.substring(3, 5) == "00") { return false; }
    if (temp.substring(5, 9) == "0000") { return false; }
    return true;
}

function massageSSN(val) {
	if (val == null || val == "" || isValidSSN(val)) return val;
	validFormat = /^(\d{1,3})(\d{1,2})(\d{1,4})/;
	parts = val.match(validFormat);
	if (parts == null) return val;			 
	return parts[1] + "-" + parts[2] + "-" + parts[3];
}
function isValidDate(val) {
	if (val == null || val == "") return true;
	validFormat = /^(\d{1,2})[\s\.\/-](\d{1,2})[\s\.\/-](\d{1,4})$/
	parts = val.match(validFormat);
	return (parts != null);
}

function massageDate(val) {
	if (val == null || val == "" || isValidDate(val)) return val;
//	validFormat = /^(\d{1,2})[\s\.\/-](\d{1,2})[\s\.\/-](\d{1,4})$/
	validFormat = /^(\d{1,2})(\d{1,2})(\d{1,4})/;
	parts = val.match(validFormat);
	if (parts == null) return val;
	return parts[1] + "/" + parts[2] + "/" + parts[3];
}