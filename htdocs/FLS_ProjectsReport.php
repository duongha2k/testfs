<?php	//FLS_ProjectsReport.php
		// This report was created by GB in order to test how many projects per project
		// for comparision with the FLS Migration.

require_once("library/caspioAPI.php");

ini_set("display_errors", 1);
set_time_limit(1600);


$FLS_Projects = caspioSelectAdv("FLS_Projects", "ProjectNo, ProjectName", "","", false, "`", "|^",false);

echo "<table border=\"1\">";
echo "<tr style=\"font-weight:bold;\"><td>Project ID</td><td>Project</td><td align=\"right\">WO's</td><td align=\"right\">Active</td></tr>";
$sub1 = 0;
$sub2 = 0;

foreach ($FLS_Projects as $project) {	
	$fields = explode("|^", $project);
	
	$ProjectID = trim($fields[0], "`"); // Remove quotes	
	$Project = trim($fields[1], "`"); // Remove quotes	

	$FLS_Orders = caspioSelectAdv("FLS_Work_Orders", "count(*)", "Project_ID = '$ProjectID'","", false, "`", "|^",false);
	//$qty = trim($FLS_Orders[0], "`"); // Remove quotes	
	$Active = caspioSelectAdv("FLS_Work_Orders", "COUNT(*)", "ISNULL(Deactivated, '') != 'Yes' AND ISNULL(Kickback, '0') = '0' AND ISNULL(DeclinedWorkOrder, '0') = '0' AND ISNULL(PayApprove, '0') = '0' AND Project_ID = '$ProjectID'", "", false, "`", "|^",false);
	$sub2 += $Active[0];
	if ($Active[0] > 0) {
		echo "<tr><td><b>$ProjectID</b></td><td><b>$Project</b></td><td align=\"right\"><b>".number_format($FLS_Orders[0])."</b></td><td align=\"right\"><b>".number_format($Active[0])."</b></td></tr>";
	$sub1 += $FLS_Orders[0];
	} else {
	//	echo "<tr><td>$ProjectID</td><td>$Project</td><td align=\"right\">".number_format($FLS_Orders[0])."</td><td align=\"right\">".number_format($Active[0])."</td></tr>";
	}
	ob_flush();
	flush();
}
echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td align=\"right\"><b>".number_format($sub1)."</b></td><td align=\"right\"><b>".number_format($sub2)."</b></td></tr>";
echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td align=\"right\">&nbsp;</td><td align=\"right\">&nbsp;</td></tr>";

$FLS_Orders = caspioSelectAdv("FLS_Work_Orders", "count(*)", "ISNULL(Project_ID, '') = ''","", false, "`", "|^",false);
$Active = caspioSelectAdv("FLS_Work_Orders", "COUNT(*)", "ISNULL(Deactivated, '') != 'Yes' AND ISNULL(Kickback, '0') = '0' AND ISNULL(DeclinedWorkOrder, '0') = '0' AND ISNULL(PayApprove, '0') = '0' AND ISNULL(Project_ID, '') = ''", "", false, "`", "|^",false);

echo "<tr><td>&nbsp;</td><td>No Project Assigned</td><td align=\"right\">".number_format($FLS_Orders[0])."</td><td align=\"right\">".number_format($Active[0])."</td></tr>";
$sub1 += $FLS_Orders[0];
$sub2 += $Active[0];
echo "<tr><td>&nbsp;</td><td>&nbsp;</td><td align=\"right\"><b>".number_format($sub1)."</b></td><td align=\"right\"><b>".number_format($sub2)."</b></td></tr>";

echo "</table>";

?>