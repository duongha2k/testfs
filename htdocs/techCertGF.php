<?php
	ini_set("display_errors", 1);
	require_once("library/caspioAPI.php");

	$mainQuery = "SELECT Tech_ID, Count(TB_UNID) FROM Work_Orders WHERE StartDate >= '2008-01-01 00:00:00' AND TechPaid = '1' GROUP BY Tech_ID";
	$flsQuery = "SELECT TL.TechID, Count(WorkOrderID) FROM FLS_Work_Orders AS FWO LEFT JOIN TR_Master_List AS TL ON TL.FLSID = FWO.FLSTechID WHERE TL.TechID <> NULL AND PjctStartDate >= '2008-01-01 00:00:00' AND Paid = 'Yes' GROUP BY TL.TechID";
	
//	$mainSiteTechs = caspioSelectAdv("Work_Orders", "TOP 5 Tech_ID, Count(WO_ID)", "StartDate >= '2008-01-01 00:00:00' AND TechPaid = '1' GROUP BY Tech_ID", "", false, "`", "|", false);
//	$flsSiteTechs = caspioSelectAdv("Call_Type", "TechID = 0, Count = 0", "id = 0 UNION ", "", false, "`", "|", false);
//	echo "SELECT tid, SUM(num) FROM ($mainQuery UNION $flsQuery) GROUP BY tid";

	$combinedList = caspioSelectAdv("Call_Type", "0,0", "id = 0 UNION ($mainQuery UNION ALL $flsQuery)", "", false, "`", "|", false);
		
	$countList = array();
	foreach ($combinedList as $info) {
		$info = explode("|", $info);
		if (array_key_exists($info[0], $countList))
			$countList[$info[0]] += $info[1];
		else
			$countList[$info[0]] = $info[1];
	}
	
	$fiveOrMore = array();
	foreach ($countList as $id => $count) {
		if ($count < 5) continue;
		$fiveOrMore[] = $id;
	}
	
	print_r(caspioUpdate("TR_Master_List_Test", "FS_Cert_Expiration_Date, FS_Cert_Test_Pass", "'09/09/2099', '1'", "TechID IN ('" . implode("','",$fiveOrMore) . "')", false));
	
	die();
?>