<?php
// Created on: 07-25-08 
// Author: GB
// Description: Receives Project ID from datapage


try {

require_once("../../library/caspioAPI.php");

//$projectID = "18510";
$projectID = $_REQUEST['projectID'];

//$records = caspioSelect("TR_Client_Projects", "Project_ID, Project_Name, Description, Requirements, Tools, SpecialInstructions, Default_Amount, Amount_Per, Qty_Devices", "Project_ID = '$projectID'", false);
$records = caspioSelectAdv("TR_Client_Projects", "Project_ID, Project_Name, Description, Requirements, Tools, SpecialInstructions, Default_Amount, Amount_Per, Qty_Devices", "Project_ID = '$projectID'", "", false, "`", "|^",false);

// Counts # of records returned
$count = sizeof($records);

// Populate array with workorders
foreach ($records as $order) {
	
	$fields = explode("|^", $order);
	
	// Get data from array and set values
	$projectID = trim($fields[0], "`");
	
	if ($fields[1] == ""){
		$ProjectName = "Not Found";
	}else{
		if($fields[1] != 'NULL'){
			$ProjectName = trim($fields[1], "`");
		} else {
			$ProjectName = "";
		}
		
		if($fields[2] != 'NULL'){
			$Description = trim($fields[2], "`");
		}else{
			$Description = "";
		}
		
		if($fields[3] != 'NULL'){
			$Requirements = trim($fields[3], "`");
		} else {
			$Requirements = "";
		}
		
		if($fields[4] != 'NULL'){
			$Tools = trim($fields[4], "`");
		} else {
			$Tools = "";
		}
		
		if($fields[5] != 'NULL'){
			$Instructions = trim($fields[5], "`");
		} else {
			$Instructions = "";
		}
		
		if($fields[6] != 'NULL'){
			$ClientOffer = trim($fields[6], "`");
		} else {
			$ClientOffer = 0;
		}
		
		if($fields[7] != 'NULL'){
			$AmountPer = trim($fields[7], "`");
		} else {
			$AmountPer = 0;
		}
		
		if($fields[8] != 'NULL'){
			$Devices = trim($fields[8], "`");
		} else {
			$Devices = 0;
		}
	}

	echo "$ProjectName|^$Description|^$Requirements|^$Tools|^$Instructions|^$ClientOffer|^$AmountPer|^$Devices";	
	
	}
}catch (SoapFault $fault) {

smtpMail("Project Assigned to Project", "nobody@fieldsolutions.com", "gbailey@fieldsolutions.com", "Tech Assigned Project Script Error", "$fault", "$fault", "projectLookup.php");
	
}
	
?>
 
