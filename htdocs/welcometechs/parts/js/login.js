/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



//-------------------------------------------------------------
//-------------------------------------------------------------
//-------------------------------------------------------------
login.Instances = null;
//-------------------------------------------------------------
login.CreateObject = function(config) {
    var obj = null;
    if (login.Instances != null) {
        obj = login.Instances[config.id];
    }
    if (obj == null) {
        if (login.Instances == null) {
            login.Instances = new Object();
        }
        obj = new login(config);
        login.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function login(config) {
    var Me = this;
    this.id = config.id;
    this.width = ((jQuery(window).width() * 90) / 100) - 10;
    this.height =(jQuery(window).height() * 80) / 100;
    this.roll = new FSPopupRoll();
    //------------------------------------------------------------
    this.hideLoader = function()
    {
        $.fancybox.close();
    }
    
    //------------------------------------------------------------
    this.showLoader = function(showCloseButton)
    {
        var content = "<div style='width:"+Me.width+"px;' id='comtantPopupID'><div style='text-align:center;height:"+Me.height+"px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :showCloseButton,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'content': content
        }
        ).trigger('click');
        

    }
    //------------------------------------------------------------
    this.showLoaderNew = function(width,height,showCloseButton)
    {
        var content = "<div style='width:"+width+"px;' id='comtantPopupID'><div style='text-align:center;height:"+height+"px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :showCloseButton,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'content': content
        }
        ).trigger('click');
        

    }
    //------------------------------------------------------------
    this.cmdReadQuick_click = function(){
        Me.showLoaderNew(700,410,true);
        jQuery("#comtantPopupID").html(jQuery("#ReadQuickTemplate").html());
    }
    //------------------------------------------------------------
    this.cmdReadTips_click = function(){
        Me.showLoaderNew(700,410,true);
        jQuery("#comtantPopupID").html(jQuery("#ReadTipsTemplate").html());
    }
	//------------------------------------------------------------
    this.cmdReadWork_click = function(){
        Me.showLoaderNew(700,410,true);
        jQuery("#comtantPopupID").html(jQuery("#ReadWorkTemplate").html());
    }
    //------------------------------------------------------------
    this.buildEvent = function()
    {
		jQuery("#cmdReadWork").unbind("click",Me.cmdReadWork_click).bind("click",Me.cmdReadWork_click);
        jQuery("#cmdReadQuick").unbind("click",Me.cmdReadQuick_click).bind("click",Me.cmdReadQuick_click);
        jQuery("#cmdReadTips").unbind("click",Me.cmdReadTips_click).bind("click",Me.cmdReadTips_click);
    }
    //------------------------------------------------------------
    this.init = function() {
        Me.buildEvent();
    }
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
}

