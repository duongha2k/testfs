<?php

if ($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') exit();
require_once("../../../headerStartSession.php");
require_once("../../../library/caspioAPI.php");
require_once('../../../includes/parts/manager.php');
require_once('../../../includes/parts/Auth.php');

if ($_SESSION["loggedIn"]!='yes' || $_SESSION["loggedInAs"]!='client') {exit();}
$woId   = (int)$_POST['woId'];
$partId = (int)$_POST['partId'];
if (empty($woId)) {exit();}

$auth = new PartsManagerAuth();
$auth->setLogin($_SESSION['UserName'])
     ->setRole(PartsManagerAuth::AUTH_CLIENT);
$authData = $auth->getAuthData();
$login = $authData['login'];
$password = $authData['password'];

$manager = new PartsManager();
$manager->setWoId($woId)
        ->setUser($authData['login'])
        ->setHash($authData['password'])
        ->setPmContext(trim($_POST['pmContext']))
        ->setRole(PartsManager::PARTS_MANAGER_CLIENT);
$result = $manager->removePart($partId);

$errors = array();
$xml = simplexml_load_string($result);
$res = '';
if (!empty($xml->error)) {
    foreach ($xml as $er) {
    	$errors[] = '<br/>Error : '.$er->description;
    }
} elseif(empty($xml)) {
    $res = $partId;
}
echo json_encode(array('errors'=>$errors, 'res'=>$res));
exit();

function getAuthData() {
    $auth = new Auth();
    $auth->setLogin($_SESSION['UserName']);
    return $auth->getAuthData();
}