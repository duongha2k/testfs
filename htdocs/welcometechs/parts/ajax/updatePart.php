<?php
if ($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') exit();
require_once("../../../headerStartSession.php");
require_once("../../../library/caspioAPI.php");
require_once('../../../includes/parts/manager.php');
require_once('../../../includes/parts/Auth.php');

if ($_SESSION["loggedIn"]!='yes' || $_SESSION["loggedInAs"]!='client') {exit();}
$woId = (int)$_POST['woId'];
if (empty($woId)) {exit();}

$auth = new PartsManagerAuth();
$auth->setLogin($_SESSION['UserName'])
     ->setRole(PartsManagerAuth::AUTH_CLIENT);
$authData = $auth->getAuthData();
$login = $authData['login'];
$password = $authData['password'];

$partId = (int)$_POST['partId'];
$consumable = (int)$_POST['Consumable'];
if ($consumable == 1) {
	$consumable = 'True';
} else {
    $consumable = 'False';
}
$params = array_map("formatData", $_POST);
$xml='<PartEntry><Consumable>'.$consumable.'</Consumable><NewPart><PartNum>'.$params['PartNum'].'</PartNum><ModelNum>'.$params['ModelNum'].'</ModelNum>'
    .'<SerialNum>'.$params['SerialNum'].'</SerialNum><Description>'.$params['Description'].'</Description>'
    .'<Carrier>'.$params['Carrier'].'</Carrier><TrackingNum>'.$params['TrackingNum'].'</TrackingNum><ETA>'.$params['ETA'].'</ETA>'
    .'<ShipTo>'.$params['ShipTo'].'</ShipTo><ShippingAddress>'.$params['ShippingAddress'].'</ShippingAddress></NewPart>';
if ($consumable=='False') {
$xml.='<ReturnPart><PartNum>'.$params['returnPartNum'].'</PartNum><ModelNum>'.$params['returnModelNum'].'</ModelNum>'
    . '<SerialNum>'.$params['returnSerialNum'].'</SerialNum><Description>'.$params['returnDescription'].'</Description>'
    . '<Carrier>'.$params['returnCarrier'].'</Carrier><TrackingNum>'.$params['returnTrackingNum'].'</TrackingNum><ETA>'.$params['returnETA'].'</ETA>'
    . '<RMA>'.$params['returnRMA'].'</RMA><Instructions>'.$params['returnInstructions'].'</Instructions></ReturnPart>';
}
$xml.='</PartEntry>';

$manager = new PartsManager();
$manager->setWoId($woId)
        ->setUser($authData['login'])
        ->setHash($authData['password'])
        ->setPmContext(trim($_POST['pmContext']))
        ->setRole(PartsManager::PARTS_MANAGER_CLIENT);
$result = $manager->updatePart($partId, $xml);
$errors = array();
$xml = simplexml_load_string($result);
if (!empty($xml->error)) {
    foreach ($xml as $er) {
    	$errors[] = '<br/>Error : '.$er->description;
    }
}
echo json_encode(array('errors'=>$errors, 'res'=>$params['PartNum']));exit();
function formatData($data){return strip_tags(trim($data));}