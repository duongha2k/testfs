<?php
if ($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') exit();
$woId = $_POST['woId'];
$partNum = (int)$_POST['partNum']+1;
$partColorNum = (int)$_POST['color'];
if ($partColorNum%2==0) $partColor = 'tryellow'; else $partColor = 'trblue';
$resHTML = '<tr id="newPartLine'.$partNum.'" name="partNew" valign="top">
<td class="'.$partColor.'" id="NewPartLline'.$partNum.'" >
    <div id="partExistNew'.$partNum.'"><table cellpadding="4" ">
	<colgroup width="82"></colgroup>
	<colgroup width="126"></colgroup>
	<colgroup width="76"></colgroup>
	<colgroup width="126"></colgroup>
    <tr>
        <td>Part#:</td>
        <td><input tabindex="1" id="newPartNumber'.$partNum.'" name="PartNum" onChange="newPartReturnAutoFill(\'Number'.$partNum.'\');" onKeyUp="newPartReturnAutoFill(\'Number'.$partNum.'\');"  style="width:120px;" type="text" /></td>
        <td>Model#:</td>
        <td><input id="newPartModel'.$partNum.'" name="ModelNum" onChange="newPartReturnAutoFill(\'Model'.$partNum.'\');" onKeyUp="newPartReturnAutoFill(\'Model'.$partNum.'\');" type="text" style="width:120px;" /></td>
      </tr>
    <tr> 
	<td>Serial#:</td>
        <td><input id="newPartSerial'.$partNum.'" name="SerialNum"  type="text" style="width:120px;" /></td>
    
        <td>Est.Arrival:</td>
        <td><input class="datepicker" name="ETA" style="width:120px;" type="text" /></td>
		</tr>
		<tr>
        <td>Carrier:</td>
        <td><select id="newPartCarrier'.$partNum.'" name="Carrier" onChange="newPartReturnAutoFill(\'Carrier'.$partNum.'\');"><option value="Fedex">FedEx</option><option value="UPS">UPS</option><option value="USPS">USPS</option><option value="DHL">DHL</option><option value="Pilot">Pilot</option><option value="Ceva">Ceva</option></select></td>
		<td>Tracking#:</td>
        <td><input id="newPartTracking'.$partNum.'" style="width:120px;" name="TrackingNum" onChange="newPartReturnAutoFill(\'Tracking'.$partNum.'\');" onKeyUp="newPartReturnAutoFill(\'Tracking'.$partNum.'\');" type="text" /></td>
    </tr>
    <tr>
        <td>Info:</td>
        <td colspan="3"><input id="newPartDescription'.$partNum.'" name="Description" style="width:326px;" onChange="newPartReturnAutoFill(\'Description'.$partNum.'\');" onKeyUp="newPartReturnAutoFill(\'Description'.$partNum.'\');" type="text"  /></td>
    </tr>
    <tr>
        <td>Ship to:</td>
        <td><select name="ShipTo"  onChange="shippingAddressEnabled(\'New'.$partNum.'\');" id="shipToNew'.$partNum.'"><option value="Site">Site</option><option value="Tech">Tech</option><option value="Fedex Hold">Fedex Hold</option><option value="UPS Store Hold">UPS Store Hold</option><option value="USPS Hold">USPS Hold</option><option value="Other">Other</option></select></td>
        <td colspan="2">
            <input type="text" style="width:198px;" name="ShippingAddress" id="shippingAddressNew'.$partNum.'" value=""  disabled/>
        </td>
    </tr>
    <tr>
        <td>Consumable:</td>
		<td><input id="consumableNew'.$partNum.'" name="Consumable" value="1" onChange="consumable(this,\'New'.$partNum.'\');" type="checkbox"/></td>
		<td>Delete part:</td><td> <input name="DeleteNewPart" id="DeletenewPartLine'.$partNum.'" onChange="deletePart(this,\'New'.$partNum.'\');" type="checkbox" /></td>
	</tr>
    </table></div>
</td>
<td class="'.$partColor.'" id="NewPartLline'.$partNum.'" width="50%">
    <div id="partReturnNew'.$partNum.'"><table cellpadding="4">
	<colgroup width="82"></colgroup>
	<colgroup width="126"></colgroup>
	<colgroup width="76"></colgroup>
	<colgroup width="126"></colgroup>
    <tr>
        <td>Part#:</td>
        <td><input name="returnPartNum" style="width:120px;" id="newPartReturnNumber'.$partNum.'"  type="text" /></td>
        <td>Model#:</td>
        <td><input name="returnModelNum" style="width:120px;" id="newPartReturnModel'.$partNum.'" type="text" /></td>
     </tr>
    <tr>   
	<td>Serial#:</td>
        <td><input name="returnSerialNum" style="width:120px;" id="newPartReturnSerial'.$partNum.'" type="text" /></td>
    
        <td>Est.Arrival:</td>
        <td><input name="returnETA" style="width:120px;" class="datepicker" type="text" /></td>
     </tr>
<tr>	 <td>Carrier:</td>
        <td><select name="returnCarrier" id="newPartReturnCarrier'.$partNum.'"><option value="Fedex">FedEx</option><option value="UPS">UPS</option><option value="USPS">USPS</option><option value="DHL">DHL</option><option value="Pilot">Pilot</option><option value="Ceva">Ceva</option></select></td>
		<td>Tracking#:</td>
        <td><input name="returnTrackingNum" style="width:120px;" id="newPartReturnTracking'.$partNum.'" type="text" /></td>
    </tr>
    <tr>
        <td>Info:</td>
        <td colspan="3"><input name="returnDescription" type="text" id="newPartReturnDescription'.$partNum.'" style="width:326px;" /></td>
    </tr>
    <tr>
        <td>RMA:</td>
        <td colspan="3"><input name="returnRMA" type="text" id="newPartReturnInfo" style="width:326px;" /></td>
    </tr>
    <tr>
        <td>Return instructions:</td>
        <td colspan="3"><input name="returnInstructions" type="text" id="shippingAddress1" value="" style="width:326px;" /></td>
    </tr>
    </table></div>
</td>
</tr>
<tr id="addNewPartButton'.($partNum-1).'">
<td>
				<div style="float:left;">
					<div class="mapperServiceButtotn"><a href="" onClick="addNewPart('.$partNum.', '.($partColorNum+1).'); return false;">Add Another Part</a></div>
				</div>
			</td>
			<td>
				<div style="float:right;">
					<div class="mapperServiceButtotn"><a href="" onClick="updateParts(' . $woId . '); return false;">Save All Changes and Close</a></div>
					<div class="mapperServiceButtotn"><a href="" onClick="return confirmation();">Close without Saving Changes</a></div>
				</div>
			</td>
</tr>';
echo $resHTML;exit();