<?php
if ($_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') exit();
require_once("../../../headerStartSession.php");
require_once("../../../library/caspioAPI.php");
require_once('../../../includes/parts/manager.php');
require_once('../../../includes/parts/Auth.php');

if ($_SESSION["loggedIn"]!='yes' || $_SESSION["loggedInAs"]!='client') {exit();}
$woId = (int)$_POST['woId'];
$companyID = $_POST['companyID'];
if (empty($woId)) {exit();}

$auth = new PartsManagerAuth();
$auth->setLogin($_SESSION['UserName'])
     ->setRole(PartsManagerAuth::AUTH_CLIENT);
$authData = $auth->getAuthData();
$login = $authData['login'];
$password = $authData['password'];

$manager = new PartsManager();
$manager->setWoId($woId)
        ->setUser($authData['login'])
        ->setHash($authData['password'])
		->setPmContext(trim($companyID))
        ->setRole(PartsManager::PARTS_MANAGER_CLIENT);

$parts = simplexml_load_string($manager->getAllParts());

$newPart = 0;
$returnPart = 0;

if (!empty($parts)) {
	foreach ($parts as $part) {
		if(!empty($part['id'])) {
            if (is_object($part->NewPart)) {
				++$newPart;
			}
            if (is_object($part->ReturnPart) && $part->Consumable != "True") {
				++$returnPart;
			}
		}
	}
}
echo json_encode(array("count" => array("new" => $newPart, "return" => $returnPart))); exit();