<?php
    $_GET['v'] = ( isset($_GET['v']) ) ? $_GET['v'] : NULL;
?>
<link rel="stylesheet" href="/css/simpleDropdown/style.css" type="text/css" media="screen"/>
<!--[if lte IE 7]>
	<link rel="stylesheet" type="text/css" href="css/simpleDropdown/ie.css" media="screen" />
<![endif]-->
<script type="text/javascript" src="/library/jquery/jquery.dropdownPlain.js"></script>
<script type="text/javascript">
	function openMapping(url) {
		newwindow=window.open(url,'mapper','height=650, width=1200, left=100, top=100, resizable=yes, scrollbars=yes, toolbar=no,status=no');
		if (window.focus) {
			newwindow.focus();
		}
	}
	function openCreateWO(url) {
		var newwindow=window.open(url,'','height=700, width=820, left=100, top=100, resizable=yes, scrollbars=yes, toolbar=no,status=no');
		if (window.focus) {newwindow.focus()}
	}	
	
</script>

<?php $siteTemplate = $_SESSION['template'];?>
<?php 
    if ( isset($_SESSION['ISO_ID']) ) {
        $ISO_ID = $_SESSION['ISO_ID']; 
    } else {
        $ISO_ID = NULL;
    }

    if ( isset($_SESSION['UserType']) ) {
        $UserType = $_SESSION["UserType"];
    } else {
        $UserType = NULL;
    }
?>

<!-- TOP NAVBAR -->
<ul id="maintab" class="dropdown">
<?php if ( $page == "login" ) {?>
<!--<li class="selected"><a href="/index2.php">Login</a></li>-->
<?php } ?>

<?php if ( $page == "adminlogin" ) {?>
<!--Admin/Staff Login-->
<?php if ( $option == "adminLogin" ){echo '<li class="">';}else{echo '<li>';}?>
<a href="/staff/index.php">Staff</a></li>
<?php if ( $option == "accounting" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/accounting/index.php">Accounting</a></li>
<?php if ( $option == "admin" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/admin/index.php">Admin</a></li>
<?php } ?>


<?php if($loggedIn=="yes" && $loggedInAs=="Customer"){ ?>
<!-- CUSTOMER NAVBAR TOP -->
<?php if ( $option == "profile" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/customers/profile.php">My Profile</a></li>
<?php if ( $option == "createQuickTicket" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/customers/create_QuickTicket.php">Create Quick Ticket</a></li>
<?php if ( $option == "projectStatus" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/customers/projectTimes.php?v=<?=$_GET["v"]?>">Quick View</a></li>
<?php } ?>


<?php if($loggedIn=="yes" && $loggedInAs=="tech"){ ?>
<?php if ( $page == "techs" || $page == "contactus") {?>
<!-- TECH NAVBAR TOP -->
<?php if ( $option == "profile" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/profile.php">My Profile</a></li>


<?php
if ( $option == "banking" ){
	echo '<li class="selected">';
}else{
	echo '<li>';
}?>

<?php
if($ISO_ID ==""){
echo "<a href='https://".$siteTemplate.".fieldsolutions.com/techs/banking.php'>Banking</a></li>";
} else {
echo "</li>";
}
?>

<!--a href="https://<?php //echo $siteTemplate;?>.fieldsolutions.com/techs/banking.php">Banking</a></li-->

<?php if ( $option == "skills" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/skills.php">Skills</a></li>
<?php if ( $option == "experience" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/experience.php">Experience</a></li>
<?php if ( $option == "certifications" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/certifications.php">Certifications</a></li>
<?php if ( $option == "media" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/media.php">Video / Audio</a></li>
<?php if ( $option == "equipment" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/cablingSkills.php">Equipment</a></li>
<?php if ( $option == "wos" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/techs/wosAssigned.php">Work Orders</a></li>
<?php if ( $option == "backgroundChecks" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/techs/backgroundChecks.php">Background Checks</a></li>

<?php
if ($siteTemplate != "ruo") {

if ( $option == "training" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/techs/FAQs/index.php">Help</a></li>
<?php if ( $option == "more" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/techFLS.php">FLS</a></li>
<?php } ?>
<?php } ?>
<?php } ?>


<?php if($loggedIn=="yes" && $loggedInAs=="client"){ ?>
<?php if ( $page == "clients" || $page == "contactus") {?>
<!-- CLIENTS -->
<?php if ($UserType == "") { ?>
<?php if ( $option == "techs" ){echo '<li style="margin-left: 2px" class="selected">';}else{echo '<li style="margin-left: 2px">';}?>
<a href="/clients/techs.php?v=<?=$_GET["v"]?>&newSearch=1" target="_blank">Find Techs</a>

	<ul class="submenu">
		<li><a href="/clients/techs.php?v=<?=$_GET["v"]?>&newSearch=1" target="_blank">Find All Techs</a></li>
		<li><a href="/clients/techs_cabling.php?v=<?=$_GET["v"]?>&newSearch=1" target="_blank">Find Cabling &amp; CCTV Techs</a></li>
		<li><a href="/clients/techs_telephony.php?v=<?=$_GET["v"]?>&newSearch=1" target="_blank">Find Telephony Techs</a></li>
		<li><a href="/clients/projectBlast.php?v=<?=$_GET["v"]?>" target="_blank">Send Project Email</a></li>
		<li><a href="/clients/recruitmentEmail.php?v=<?=$_GET["v"]?>" target="_blank">Send Recruitment Email</a></li>
		<?php if ( $_GET["v"] == "FLS" ):?>
		<li><a href="/clients/FLS/importFLSBadge.php?v=<?=$_GET["v"]?>" target="_blank">FLS ID Upload</a></li>
		<?php endif; ?>
	</ul>

</li>
<?php } ?>

<?php if ($UserType == "") { ?>

<?php if ( $option == "wos" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/clients/wos.php?v=<?php echo $_GET["v"]?>">My Work Orders</a>

	<ul class="submenu">
		<li><a href="/clients/wos.php?v=<?php echo $_GET["v"]?>">My Dashboard</a></li>
		<li><a href="javascript:openCreateWO('createWO.php?v=<?=$_GET["v"]?>')">Create a Work Order</a></li>
		<li><a href="javascript:openPopup('/clients/wosView.php?v=<?php echo $_GET["v"]?>', null, 750)">Find a Work Order</a></li>
		<li><a href="/clients/recruitmentEmail.php?v=<?=$_GET["v"]?>">Send Recruitment Email</a></li>
		<li><a href="/clients/projectBlast.php?v=<?=$_GET["v"]?>">Send Project Email</a></li>
		<li><a href="/clients/importWO.php?v=<?php echo $_GET["v"]?>">Upload Work Orders</a></li>

<?php } else {?>
<?php if ( $option == "wos" ){echo '<li class="selected">';}else{echo '<li>';}?>
 <a href="/clients/wosView.php?v=<?php echo $_GET["v"]?>">My Work Orders</a>

	<ul class="submenu">
		<li><a href="/clients/createWO.php?v=<?php echo $_GET["v"]?>">Create a Work Order</a></li>
		<li><a href="/clients/wosView.php?v=<?php echo $_GET["v"]?>">Find a Work Order</a></li>
		<li><a href="/clients/paperworkReceived.php?v=<?php echo $_GET["v"]?>">Paperwork Received</a></li>
<?php } ?>

<?php
	require_once("library/caspioAPI.php");
	$result = caspioSelect("TR_Client_List", "ProjectManager", "UserName = '{$_SESSION["UserName"]}'", "", false);
	if (sizeof($result) == 1 && $result[0] == "True")
		echo "<li><a href='GPM_FindWO.php?v=".$_GET['v']."'>Find WIN#</a></li>";
?>

	</ul>

</li>
<?php
	if (isset($_SESSION["tbAdminLevel"])) {
		$_COOKIE["tbAdminLevel"] = $_SESSION["tbAdminLevel"];
		$getAdminLevel = $_SESSION["tbAdminLevel"];
		unset($_SESSION["tbAdminLevel"]);
	}
	else
		$getAdminLevel = ( isset($_COOKIE["tbAdminLevel"]) ) ? $_COOKIE["tbAdminLevel"] : NULL;
	if ( $getAdminLevel == "Yes"):
	if ( $option == "projects") {echo '<li class="selected">';}else{echo '<li>';}?>
	<a href="/clients/projectsView.php?v=<?=$_GET["v"]?>&Start=&nbsp;&End=Z&Active=1">My Projects</a>
	<ul class="submenu">
		<li><a href="/clients/projectsView.php?v=<?php echo $_GET["v"]?>&Start=&nbsp;&End=Z&Active=1">View / Edit Projects</a></li>
		<li><a href="/clients/projectsCreate_new.php?v=<?php echo $_GET["v"]?>">Add Project</a></li>
	</ul>

</li>
<?php  endif;?>

<?php if ($UserType == "") { ?>
<?php if ( $option == "fsmapper" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="javascript:openMapping('mapping/?v=<?=$_GET["v"]?>')">FS-Mapper</a>
</li>
<?php } ?>

<?php
/* REVERSE ORDER TO WHAT IS DISPLAYED DUE TO FLOAT RIGHT */

	if ( $option == "profile" ){echo '<li id="mainLinkProfile" class="selected">';}else{echo '<li id="mainLinkProfile">';}?>
<a href="/clients/profile.php">My Client Profile</a></li>

<?php
	if ( $getAdminLevel == "Yes"):
	if ( $option == "customers" && $getAdminLevel == "Yes"){echo '<li id="mainLinkCustomers" class="selected">';}else{echo '<li id="mainLinkCustomers">';}?>
	<a href="/clients/customersView.php?v=<?=$_GET["v"]?>">Customers</a>
	<ul class="submenu">
		<li><a href="/clients/customersView.php?v=<?php echo $_GET["v"]?>">View / Edit Customers</a></li>
		<li><a href="/clients/customersCreate.php?v=<?php echo $_GET["v"]?>">Add Customer</a></li>
	</ul>

</li>
<?php endif; ?>

<?php

	if ( $getAdminLevel == "Yes"):
	if ( $option == "users" ){echo '<li id="mainLinkUsers" class="selected">';}else{echo '<li id="mainLinkUsers">';}?>
	<a href="/clients/usersView.php?v=<?=$_GET["v"]?>">Users</a>
	<ul class="submenu">
		<li><a href="/clients/usersView.php?v=<?php echo $_GET["v"]?>">View / Edit Users</a></li>
		<li><a href="/clients/usersCreate.php?v=<?php echo $_GET["v"]?>">Add User</a></li>
	</ul>

</li>
<?php endif; ?>

<?php if ($UserType == "Manager") {?>
	<?php if ( $option == "reports" ){echo '<li id="mainLinkReports" class="selected">';}else{echo '<li id="mainLinkReports">';}?>
    <a href="#">Reports &nbsp;&nbsp;&nbsp;&nbsp;</a>
<?php } else {?>
	<?php if ( $option == "reports" ){echo '<li id="mainLinkReports" class="selected">';}else{echo '<li id="mainLinkReports">';}?>
    <a href="#">Reports</a>
<?php } ?>

        <ul class="submenu">
		<?php if ($UserType == "") { ?>
            <li><a href="/clients/projectTimes.php?v=<?php echo $_GET["v"]?>">Project Status with Deliverables</a></li>
            <li><a href="/clients/Metrics_Reports.php?v=<?php echo $_GET["v"]?>">FS-Metrics Reports</a></li>
            <li><a href="/clients/wosUnassigned.php?v=<?php echo $_GET["v"]?>">Unassigned Work Orders</a></li>
            <li><a href="/clients/SAT_List.php?v=<?php echo $_GET["v"]?>">Tech Site Satisfaction Survey List</a></li>
            <li><a href="/clients/confirmReport.php?v=<?php echo $_GET["v"]?>">Tech Review and Confirm</a></li>
            <li><a href="/clients/techsDeniedEdit.php?v=<?php echo $_GET["v"]?>">Client Denied Techs</a></li>
            <li><a href="/clients/techsClientPreferredEdit.php?v=<?php echo $_GET["v"]?>">Client Preferred Techs</a></li>
            <li><a href="/clients/techCalled.php?v=<?php echo $_GET["v"]?>">Tech Called Report</a></li>
            <li><a href="/clients/wosView_OOS.php?v=<?php echo $_GET["v"]?>">Out of Scope Report</a></li>
            <li><a href="/clients/tech_Address_Report.php?v=<?php echo $_GET["v"]?>">Tech WO Address Report</a></li>
		<?php } else if ($UserType == "Manager") {?>
          		<li><a href="/clients/FLS/Financials.php?v=<?php echo $_GET["v"]?>">Financials</a></li>
	            <li><a href="/clients/FLS/projectTimes_FLS.php?v=<?php echo $_GET["v"]?>">Project Status with Deliverables</a></li>
		<?php } ?>
    <?php if ( $_GET["v"] == "FLS" && $UserType != "Manager" && $UserType != "Install Desk"):?>
            <li><a href="/clients/FTXS_Reports.php?v=<?php echo $_GET["v"]?>">FAI Reports</a></li>
    <?php endif;?>
    	</ul>
 </li>


<?php } ?>
<?php } ?>


<?php if($loggedIn=="yes" && $loggedInAs=="staff"){ ?>
<?php if ( $page == "staff" ) {?>
<!-- STAFF NAVBAR TOP -->
<?php if ( $option == "profile" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/staff/profile.php">My Profile</a></li>
<?php if ( $option == "staff" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/staff/staff.php">Staff</a></li>
<?php if ( $option == "projects" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/staff/projects.php">Manage Projects</a></li>
<?php if ( $option == "cibs" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/staff/cibs.php">CIBs</a></li>
<?php if ( $option == "techs" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/staff/techView.php">Techs</a></li>
<!--?php if ( $option == "help" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="https://<!-?php echo $siteTemplate;?>.fieldsolutions.com/content/contactUs.php">Help</a></li>
-->
<!--<?php if ( $option == "wosFLS" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/staff/wosFLS.php">FLS Work Orders</a></li>-->
<?php } ?>
<?php } ?>


<?php if($loggedIn=="yes" && $loggedInAs=="admin"){ ?>
<?php if ( $page == "admin" ) {?>
<!-- ADMIN NAVBAR TOP -->
<?php if ( $option == "techs" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/adminTechSearch.php">Techs</a></li>
<?php if ( $option == "client" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/adminClientList.php">Client</a></li>
<?php if ( $option == "ISO" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/ISO_List.php">ISO</a></li>
<?php if ( $option == "wos" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/admin/wos.php">Work Orders</a></li>
<?php if ( $option == "admin" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/adminUpdate.php">Admin</a></li>
<?php if ( $option == "staff" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/adminStaffUpdate.php">Staff</a></li>
<?php if ( $option == "reports" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/adminReports.php">Reports</a></li>
<?php if ( $option == "dbTesting" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="/admin/dbTesting.php">D&B Testing</a></li>
<?php if ( $option == "pricing" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/adminPricing.php">Pricing</a></li>
<?php } ?>
<?php } ?>

<?php if ( $page == "aboutus" ) {?>
<!-- ABOUT US NAVBAR TOP -->
<?php if ( $option == "aboutus" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/wp/our-services/">Our Business</a></li>
<?php if ( $option == "missionvision" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/wp/mission-values/">Mission & Values</a></li>
<?php if ( $option == "ourClients" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/wp/our-clients/">Our Clients</a></li>
<?php if ( $option == "ourMembers" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/wp/our-members/">Our Members</a></li>
<?php if ( $option == "thedifference" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/wp/fieldsolutions-difference/">FieldSolutions Difference</a></li>
<?php if ( $option == "management" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/wp/management-team/">Management Team</a></li>
<?php if ( $option == "newsAndEvents" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/wp/news-and-events/">News and Events</a></li>
<?php if ( $option == "aboutContactUs" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/wp/contact-us/">Contact Us</a></li>
<?php } ?>


<?php if ( $page == "sales" ) {?>
<!-- SALES NAVBAR TOP -->
<li class="selected"><a href="<?php echo $siteUrl;?>/sales/dashboard.php">Sales Dashboard</a></li>
<?php } ?>

<?php if ( $page == "accounting" ) {?>
<!-- SALES NAVBAR TOP -->
<li class="selected"><a href="<?php echo $siteUrl;?>/accounting/index.php">Accounting</a></li>
<?php } ?>
</ul>

<?php 
    if ( isset($_SESSION['loggedIn']) ) {
        $loggedIn = $_SESSION['loggedIn'];
    } else {
        $loggedIn = NULL;
    }
?>

<div id="tabcontent" class="clearfix">



<?php if ($page == "techs" && $option == "certifications") {?>
<ul class="selected">
<?php if ($selected == "technical" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/certifications.php">Technical Certifications</a></li>
<?php if ($selected == "FSCertification" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/Training/FieldSolutions/FS_Certification.php">Field Solutions Certification</a></li>
</ul>
<?php } ?>

<?php if ($page == "login"){?>
<!--Login -->
<!--
<?php if ( $page == "login" ){echo '<ul class="selected">';}else{echo '<ul>';}?>
<li><a href="<?php echo $siteUrl;?>/techs/index.php">Tech</a></li>
<li><a href="<?php echo $siteUrl;?>/clients/index.php">Client</a></li>
<li><a href="<?php echo $siteUrl;?>/staff/index.php">Staff</a></li>
<li><a href="<?php echo $siteUrl;?>/admin/index.php">Admin</a></li>
</ul>
-->
<?php } ?>



<?php if ($page == "techs"){?>


<?php if ($option=="wos"){?>
<!--TECH 2nd NAVBAR -->


<ul class="selected">
<?php if ($selected == "wosAssigned" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="wosAssigned.php?v=<?php echo $_GET["v"]; ?>">Assigned Work</a></li>
<?php if ($selected == "wosAvailable" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="wosAvailable.php?v=<?php echo $_GET["v"]; ?>">Available Work</a></li>
<?php if ($selected == "wosViewApplied" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="wosViewAppliedAll.php?v=<?php echo $_GET["v"]; ?>">My Applied WOs</a></li>
<!--<li><a href="wosFLSAssigned.php">FLS Work Orders</a></li>-->
<!--<li><nobr><a id="dbTypeLink" href="wosFLSAssigned.php" style="display: none">FLS WOs</a></nobr></li>-->
</ul>



<!--<li><nobr><a href="javascript:showNextWeek()">Next Week's WOs</a> (<?=$woNextWeek?>)</nobr></li>-->
<!--<li><nobr><a href="javascript:showNextWeek()">Next Week's WOs</a></nobr></li>-->
<?php } ?>

<?php if ($option=="equipment"){?>
<!--TECH 2nd NAVBAR equipment -->
<ul class="selected">
<?php if ($selected == "equipMisc" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="equipment.php">Basic</a></li>
<?php if ($selected == "equipCabling" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="cablingSkills.php?>">Cabling &amp; CCTV</a></li>
<?php if ($selected == "equipTelephony" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="telephonySkills.php?>">Telephony</a></li>
</ul>

<!--<li><nobr><a href="javascript:showNextWeek()">Next Week's WOs</a> (<?=$woNextWeek?>)</nobr></li>-->
<!--<li><nobr><a href="javascript:showNextWeek()">Next Week's WOs</a></nobr></li>-->
<?php } ?>

<?php if ($option=="wosFLS"){?>
<!--TECH 2nd NAVBAR FLS WORKORDERS-->
<ul class="selected">
<?php if ($selected == "wosFLSAssigned" ){echo '<li class="selected">';}else{echo '<li>';}?><!--<a href="wosFLSAssigned.php?v=<?php echo $_GET["v"]; ?>">Assigned Work</a></li>-->
<?php if ($selected == "wosFLSAvailable" ){echo '<li class="selected">';}else{echo '<li>';}?><!--<a href="wosFLSAvailable.php?v=<?php echo $_GET["v"]; ?>">Available Work</a></li>-->
<?php if ($selected == "wosViewApplied" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="wosViewAppliedAll.php">My Applied WOs</a></li>
<li><a href="wosAssigned.php">FieldSolutions WOs</a></li>
<!--<li><nobr><a id="dbTypeLink" href="wosAssigned.php" style="display: none">Field Solutions</a></nobr></li>-->
</ul>

<!--<li><nobr><a href="javascript:showNextWeek()">Next Week's WOs</a> (<?=$woNextWeek?>)</nobr></li>-->
<!--<li><nobr><a href="javascript:showNextWeek()">Next Week's WOs</a></nobr></li>-->
<?php } ?>

<!-- TECHS - TRAINING -->

<?php if ($option == "training"){?>
<ul class="selected">
 <!--  <?php if ( $selected == "support" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/content/contactUs.php">Technician Support</a></li>-->

 <?php if ( $selected == "support" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/FAQs/index.php">FAQ / Support</a></li>

    <?php if ( $selected == "fs" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/Training/FS_Training.php">Field Solutions How-To's</a></li>
    <?php if ( $selected == "FSTraining" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/techs/Training/FieldSolutions/FieldSolutionsTraining.php">Field Solutions Online Tutorial</a></li>
    <?php if ( $selected == "fls" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="http://www.flsupport.com/2.html" target="_blank">FLS How-To's <span class="offsite">(Off-Site)</a></a></li>
</ul>

<?php } ?>

<!-- TECHS - MORE -->
<?php if ($option == "backgroundChecks"){?>
<ul class="selected">
	<?php if ($display == "yes"){?>
        <?php if ( $selected == "backgroundChecks" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="backgroundChecks.php">Instructions</a></li>
        <?php if ( $selected == "backgroundPolicy" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="backgroundPolicy.php">Policy</a></li>
        <?php if ( $selected == "backgroundFAQ" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="background_FAQ.php">FAQ</a></li>
    <?php } ?>
</ul>

<?php } ?>
<?php } ?>



<?php if ($page == "clients"){?>
<!--CLIENTS 2nd NAVBAR -->

<?php if ($option == "techs"){?>
<div id="subSectionLabel" style="" class="clearfix">
<!-- TECHS -->
<ul class="selected">
<?php if ($selected == "find" ):?>
<li><a href="/clients/techs.php">Find All Techs</a></li>
<?php elseif ( $selected == "findCabling" ):?>
<li><a href="/clients/techs_cabling.php?v=<?=$_GET["v"]?>&newSearch=1">Find Cabling &amp; CCTV Techs</a></li>
<?php elseif ( $selected == "findTelephony" ):?>
<li><a href="/clients/techs_telephony.php?v=<?=$_GET["v"]?>&newSearch=1">Find Telephony Techs</a></li>
<?php elseif ($selected == "recruitmentemail" ):?>
<li><a href="/clients/recruitmentEmail.php?v=<?=$_GET["v"]?>">Send Recruitment Email</a></li>
<?php elseif ($selected == "flsidupload"):?>
<li><a href="/clients/FLS/importFLSBadge.php?v=<?=$_GET["v"]?>">FLS ID Upload</a></li>
<?php endif;?>
</ul>
</div>
<?php } ?>

<?php if ($option == "wos"){?>
<!-- WORKORDERS -->
<div id="subSectionLabel" style="margin-left:1px;" class="clearfix">
<ul class="selected">
<?php if ($selected == "createWO" ):?>
<li><a href="createWO.php?v=<?php echo $_GET["v"]?>">Create a Work Order</a></li>
<?php elseif ($selected == "wosView" ):?>
<li><a href="wosView.php?v=<?php echo $_GET["v"]?>">Find a Work Order</a></li>
<?php elseif ( $selected == "recruitmentemail" ):?>
<li><a href="/clients/recruitmentEmail.php?v=<?=$_GET["v"]?>">Send Recruitment Email</a></li>
<?php elseif ( $selected == "projectBlast" ):?>
<li><a href="/clients/projectBlast.php?v=<?=$_GET["v"]?>">Send Project Email</a></li>
<?php elseif ($selected == "importWO" ):?>
<li><a href="importWO.php?v=<?php echo $_GET["v"]?>">Upload Work Orders</a></li>
<?php elseif ($selected == "FindWIN" ):?>
<li><a href="GPM_FindWO.php?v=<?= $_GET['v']?>">Find WIN#</a></li>
<?php elseif ($selected == "paperworkReceived" ):?>
<li><a href="paperworkReceived.php">Paperwork Received</a></li>
<?php else:?>
<li><a href="wos.php?v=<?php echo $_GET["v"]?>">My Dashboard</a></li>
<li>
	<?php if ($_GET["v"] !== 'FLS') : // #11208 ?>
	<div class="tCenter">
        <div class="topToolBtn">
			<input type="button" id="FindWorkOrderBtn" value="Find a Work Order" class="link_button" />
		</div>
        <div class="topToolBtn">
			<input type="button" id="CreateWorkOrderBtn" value="Create a Work Order" class="link_button middle_button" />
		</div>
        <div class="topToolBtn">
			<input type="button" id="QuickAssignBtn" value="Quick Assign" class="link_button" />
		</div>
        <div class="topToolBtn">
			<input type="button" id="QuickApproveBtn" value="Quick Approve" class="link_button" />
		</div>
	</div>
	<?php endif; ?>
</li>
<?php endif;?>
</ul>
</div>
<?php } ?>

<?php if ($option == "reports"){?>
<div style="margin-top:1px;" class="clearfix">

<!-- REPORTS -->
<ul class="selected">
<?php if ($selected == "confirmReport" ):?>
<li><a href="confirmReport.php?v=<?php echo $_GET["v"]?>">Tech Review and Confirm</a></li>
<?php elseif ( $selected == "techsDeniedEdit" ):?>
<li><a href="techsDeniedEdit.php?v=<?php echo $_GET["v"]?>">Client Denied Techs</a></li>
<?php elseif ( $selected == "techsPreferredEdit" ):?>
<li><a href="techsClientPreferredEdit.php?v=<?php echo $_GET["v"]?>">Client Preferred Techs</a></li>
<?php elseif ($selected == "techCalled" ):?>
<li><a href="techCalled.php?v=<?php echo $_GET["v"]?>">Tech Called Report</a></li>
<?php elseif ($selected == "wosView_OOS" ):?>
<li><a href="wosView_OOS.php?v=<?php echo $_GET["v"]?>">Out of Scope Report</a></li>
<?php elseif ($selected == "wosUnassigned" ):?>
<li><a href="wosUnassigned.php?v=<?php echo $_GET["v"]?>">Unassigned Work Orders</a></li>
<?php elseif ($selected == "techAddressReport" ):?>
<li><a href="tech_Address_Report.php?v=<?php echo $_GET["v"]?>">Tech WO Address Report</a></li>
<?php elseif ($selected == "projectTimesReport" ):?>
<li><a href="projectTimes.php?v=<?php echo $_GET["v"]?>">Project Status with Deliverables</a></li>
<?php elseif ($selected == "FTXSReport" ):?>
<li><a href="projectTimes.php?v=<?php echo $_GET["v"]?>">FTXS Reports</a></li>
<?php endif;?>
</ul>
</div>
<?php } ?>

<?php if ($option == "projects"){?>
<!-- PROJECTS -->
<div id="subSectionLabel">
<ul class="selected">
<?php if ($selected == "projectsView" ):?>
<li><a href="projectsView.php?v=<?php echo $_GET["v"]?>">View / Edit Projects</a></li>
<?php elseif ( $selected == "projectsCreate" ):?>
<li><a href="projectsCreate_new.php?v=<?php echo $_GET["v"]?>">Add Project</a></li>
<?php endif;?>
</ul>
</div>
<?php } ?>

<?php if ($option == "users"){?>
<!-- USERS -->
<div id="subSectionLabel">
<ul class="selected">
<?php if ($selected == "usersView" ):?>
<li><a href="usersView.php?v=<?php echo $_GET["v"]?>">View / Edit Users</a></li>
<?php elseif ( $selected == "usersCreate" ):?>
<li><a href="usersCreate.php?v=<?php echo $_GET["v"]?>">Add User</a></li>
<?php endif;?>
</ul>
</div>
<?php } ?>

<?php if ($option == "customers"){?>
<!-- CUSTOMERS -->
<ul class="selected">
<?php if ($selected == "customersView" ):?>
<li><a href="customersView.php?v=<?php echo $_GET["v"]?>">View / Edit Customers</a></li>
<?php elseif ( $selected == "customersCreate" ):?>
<li><a href="customersCreate.php?v=<?php echo $_GET["v"]?>">Add Customer</a></li>
<?php endif;?>
</ul>
<?php } ?>

<!-- END OF CLIENTS -->
<?php } ?>



<?php if ($page == "staff"){?>
<!--Staff 2nd bar-->
<?php if ( $page == "staff" ){echo '<ul class="selected">';}else{echo '<ul>';}?>
<!--
<?php if ( $option == "profile" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/staff/profile.php">My Profile</a></li>
-->
</ul>
<?php } ?>



<?php if($loggedIn=="yes" && $loggedInAs=="admin"){ ?>
<?php if ($page == "admin"){?>
<!--ADMIN 2nd NAVBAR-->

<?php if ($option == "techs"){?>
<ul class="selected">
<?php if ( $selected == "search" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/adminTechSearch.php">Tech Search</a></li>
<?php if ( $selected == "searchCabling" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/adminTechSearch_cabling.php">Tech Cabling Search</a></li>
<?php if ( $selected == "findTelephony" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/adminTechSearch_telephony.php">Tech Telephony Search</a></li>
<?php if ( $selected == "advanced" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/advancedTechSearch.php">Adv Tech Search</a></li>
<?php if ( $selected == "emaillist" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/adminTechEmailList.php">Tech Email List</a></li>
<?php if ( $selected == "updatetechratings" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/adminUpdateTechRatings.php">Update Tech Ratings</a></li>
<?php if ( $selected == "blastalltechs" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/blastAllTechs.php">Blast All Techs</a></li>
<?php if ( $selected == "flsidrecycler" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/flsIDRecycler.php">Recycle FLS IDs</a></li>
<?php if ( $selected == "flsidupload" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/importFLSBadge.php">FLS ID Upload</a></li>
<?php if ( $selected == "Client Denied" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/ClientDenied.php">Client Denied</a></li>
<?php if ( $selected == "DupeCleanup" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/TechDupeCleanup.php">Dupe Cleanup</a></li>
<?php if ( $selected == "Admin Denied" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/techsAdminDenied.php">Admin Denied</a></li>
<?php if ( $selected == "loginhistory" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/adminTechLoginHistory.php?type=Tech">Tech Login History</a></li>
<?php if ( $selected == "copyPrefTechs" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="/admin/copyPrefTechs.php?type=Tech">Copy Pref. Techs</a></li>
</ul>
<?php } ?>

<?php if ($option == "ISO"){?>
<ul class="selected">
<?php if ( $selected == "ISOList" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="ISO_List.php">ISO List</a></li>
<?php if ( $selected == "addISO" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="ISO_Add.php">Add ISO</a></li>
<?php if ( $selected == "addISO_Tech" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="ISO_Add_Tech.php">Add ISO Tech</a></li>
</ul>
<?php } ?>

<?php if ($option == "client"){?>
<ul class="selected">
<?php if ( $selected == "clientlist" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="adminClientList.php">Client List</a></li>
<?php if ( $selected == "addClient" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="adminAddClient.php">Add Client</a></li>
<?php if ( $selected == "loginhistory" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="adminClientLoginHistory.php?type=Client">Client Login History</a></li>
<?php if ( $selected == "projectList" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="adminClientProjectList.php?type=Client">Client Project List</a></li>
<?php if ( $selected == "customerList" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="adminClientCustomerList.php?type=Client">Client Customer List</a></li>
</ul>
<?php } ?>

<?php if ($option == "wos"){?>
<ul class="selected">
<!-- REPORTS -->
<Script language="JavaScript">
<!--
function goto(me) {
	var index=me.selectedIndex;
	if (me.options[index].value != "0") {location=me.options[index].value;}}
-->
</script>

<?php
	require("includes/getWOCount.php");
?>
<select id="WOoptions" name="WOoptions" size="1" onChange="filterDate()">
<option value="">Select</option>
<option value="showWeek">This Week's WOs&nbsp;&nbsp;(<?=$woThisWeek?>)</option>
<option value="showMonth">This Month's WOs&nbsp;&nbsp;(<?=$woThisMonth?>)</option>
<option value="showAllOpen">All Open WOs&nbsp;&nbsp;(<?=$woAllOpen?>)</option>
</select>

<select name="select" onChange="goto(this)" size="1">
<option value="">Field Solutions Links</option>
<option <?php if ( $selected == "wosSearchPay" ){echo 'selected="selected"';}?> value="wosSearchPay.php">Search/Pay Work Orders</option>
<option <?php if ( $selected == "wosSearchApplicants" ){echo 'selected="selected"';}?> value="wosSearchApplicants.php">Search Work Order Applicants</option>
<option <?php if ( $selected == "wosISOTechWOReport" ){echo 'selected="selected"';}?> value="isoTechWorkOrderReport.php">ISO Tech WO Report</option>
</select>

<select name="select" onChange="goto(this)" size="1">
<option value="">FLS Links</option>
<option <?php if ( $selected == "flsWorkOrders" ){echo 'selected="selected"';}?> value="flsWorkOrders.php">Work Orders</option>
<option <?php if ( $selected == "FLSwosSearch" ){echo 'selected="selected"';}?> value="wosSearch_FLS_Applicants.php">Work Applicants</option>
<option <?php if ( $selected == "flsWoDetailExport" ){echo 'selected="selected"';}?> value="flsWoDetailExport.php">Work Order Details Export</option>
<option <?php if ( $selected == "wosISOFLSTechWOReport" ){echo 'selected="selected"';}?> value="isoTechFLSWorkOrderReport.php">ISO Tech WO Report</option>
</select>
<div style="margin-left:565px; margin-top:-20px;" class="clearfix">
<?php if ( $selected == "projectWOSearch" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="projectWOSearch.php?t=Blast">Project Email Blast</a></li>
</div>
</ul>
<?php } ?>

<?php if ($option == "admin"){?>
<ul class="selected">
<?php if ( $selected == "adminUpdate" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="adminUpdate.php">View / Update Addmins</a></li>
<?php if ( $selected == "addAdmin" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="adminAdminAdd.php">Add Admin</a></li>
</ul>
<?php } ?>

<?php if ($option == "staff"){?>
<ul class="selected">
<?php if ( $selected == "staffUpdate" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="adminStaffUpdate.php">View / Update Staff</a></li>
<?php if ( $selected == "addstaff" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="adminStaffAdd.php">Add Staff</a></li>
</ul>
<?php } ?>

<?php if ($option == "reports"){?>
<ul class="selected">
<?php if ( $selected == "financialsFLS" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="financials_FLS.php">FLS Financials</a></li>
<?php if ( $selected == "financialsMain" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="financials_MainSite.php">Main Site Financials</a></li>
<?php if ( $selected == "SMTPEmailLog" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="SMTPEmailLog.php">Email Log</a></li>
<?php if ( $selected == "SMTPBlockedEmail" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="SMTPBlockedEmail.php">Blocked Email Addresses</a></li>
<?php if ( $selected == "addStaff" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="AddStaff.php">Add Staff Profile</a></li>
<?php if ( $selected == "wosSummary" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="wosSummary.php">Work Order Summary</a></li>
<?php if ( $selected == "unassigned" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="unassigned.php">Unassigned Work Orders</a></li>
<?php if ( $selected == "deactivated" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="deactivatedWorkOrders.php">Deactivated Work Orders</a></li>
<?php if ( $selected == "preferredReport" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="preferredTechsReport.php">Preferred Techs Report</a></li>
<?php if ( $selected == "filledReport" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="fillRateReport.php">Fill Rate Report</a></li>
<?php if ( $selected == "acsProjectReport" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="acsProjectReport.php">P2T / ACS Projects Report</a></li>
<?php if ( $selected == "noCellPhoneReport" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="noCellPhoneReport.php">Techs No Cell Phone Report</a></li>
<?php if ( $selected == "techTimeReport" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="techTimeReport.php">Tech Times</a></li>
<?php if ( $selected == "acsServicesDeliveredReport" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="acsServicesDeliveredReport.php">ACS Services Delivered</a></li>
<?php if ( $selected == "techProfileDellDownload" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="techProfileDell.php">Tech Profile Dell Download</a></li>
<!--?php if ( $selected == "IVR_Log" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="IVR_Log.php">IVR Log</a></li-->
</ul>
<?php } ?>

<?php if ($option == "pricing"){?>
<ul class="selected">
<?php if ( $selected == "addPricingField" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="addPricingField.php">Add Field</a></li>
<?php if ( $selected == "editPricingField" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="editPricingField.php">View / Edit Fields</a></li>
<?php if ( $selected == "editPricingRule" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="viewEditPricingRules.php">View / Edit Rules</a></li>
<?php if ( $selected == "assignRuleClient" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="adminClientList.php">Assign Default Rule to Client</a></li>
<?php if ( $selected == "assignRuleProject" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="adminClientProjectList.php?type=Client">Assign Rule to Project</a></li>
<?php if ( $selected == "assignRuleProject" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="wos.php">Assign Rule to WO</a></li>
<?php if ( $selected == "pricingRulesAssignedReport" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="pricingRulesAssigned.php">Pricing Rules Assigned Report</a></li></ul>
<?php } ?>

<?php } ?>
<?php } ?>


<?php if ($page == "staff"){?>
<?php if ($option == "techs"){?>
<ul class="selected">
<?php if ( $selected == "techview" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="techView.php">View Technicians</a></li>
<?php if ( $selected == "techviewratings" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="techViewRatings.php">View Technician Ratings</a></li>
<?php if ( $selected == "techaddratings" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="techAddRatings.php">Add Technician Ratings</a></li>
</ul>
<?php } ?>
<?php } ?>


<?php if ($page == "accounting"){?>
<?php if ($option == "accounting"){?>
<ul class="selected">
<?php if ( $selected == "Billing" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="index.php">Billing</a></li>
<?php if ( $selected == "Banking" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="banking.php">Banking</a></li>
</ul>
<?php } ?>
<?php } ?>

<?php if ($page == "sales"){?>
<!--Sales -->
<ul class="selected">

<?php if ( $option == "techs" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/sales/salesTechs.php">Techs</a></li>
<?php if ( $option == "clients" ){echo '<li class="selected">';}else{echo '<li>';}?><a href="<?php echo $siteUrl;?>/sales/salesClients.php">Client</a></li>
<?php if ( $option == "wos" ){echo '<li class="selected">';}else{echo '<li>';}?>
<a href="<?php echo $siteUrl;?>/sales/salesWos.php">Work Orders</a></li>
</ul>
<?php } ?>

</div>

