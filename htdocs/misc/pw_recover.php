<?php
/* pw recover php
 * pre: type - client || tech
 *      email - valid email address.
 * post: send the user an email w/ links for recovery.
 *
 */

require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');
session_start();

// checking include path for Zend_Registry
//var_dump(get_include_path());
include_once("pw.conf");

$redirect = false;
$debug = false;
if(!isset($_REQUEST['email'])){
    // badly formed data
    $redirect = true;
} else {
    $email = strtolower($_REQUEST['email']);
    echo $debug? "<pre>Email: $email</pre>":null;
}

if(!isset($_REQUEST['type'])){
    // do nothing
    // error out, badly formed data
    $redirect = true;
} else {
    $type = $_REQUEST['type'];
    $table = $pw_conf[ $type ]['table'];
    $email_field = $pw_conf[ $type ]['email'];
    echo $debug ? "<pre>Type: {$_REQUEST['type']}</pre>":null;
}

if($redirect == true){
    Header("location: {$pw_url}/misc/lostPassword.php");
    exit();
}

// nothing committed to session, b/c user is intended to go to the email.
// and click the link through from the email.

$db = Zend_Registry::get('DB');
$select = $db->select();
$select->from( $table, array("LOWER(".$email_field.")", "Password", $pw_conf[ $type ]['greeting']) )
        ->where( "LOWER(".$email_field .") = ". $db->quoteInto("?", $email) );
if($type == "tech"){
$select->where("Deactivated != 1");
}
$result = $db->fetchRow($select);

?>
<?php require ("../header.php"); ?><!-- ../ only if in sub-dir -->
<?php require ("../navBar.php"); ?><!-- ../ only if in sub-dir -->

<h1><?=ucfirst($type)?> Password Retrieval</h1>
<?php
if($debug){ ?><pre><? print_r($result); ?></pre><? }
if($result === false){
    // email not found
    ?><div align='center'>Email not found</div><?php
} else {
        if(in_array($email, $result)){
                ?><div align="center">An email has been sent to <?=$email?>. Please follow the instructions to retrieve your password.</div><?

                // generate a code.
                //$reset_code = uniqid();
                $reset_code = md5( date("Ymd") . $result['Password'] );

                $mail = new Core_EmailForForgotPassword();
                $mail->setClientType($type);
                $mail->setContactName($result['ContactName']);
                $mail->setCode($reset_code);

                //$mail->sendMail($email);
                $mail->sendMail($email);

        }
        //print_r($result);
}
?>

<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->