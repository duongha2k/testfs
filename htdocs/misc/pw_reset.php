<?php

/*
 *
 * page to reset password
 * pre: session validation boolean, client id in the reset variable.
 * post: accept entered passwords for validation and change.
 */

require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');
session_start();

include_once('pw.conf');

$redirect = false;
if(!isset($_SESSION['reset_validated']) || $_SESSION['reset_validated'] != true ){
    // bad data.
    $_SESSION['message'][] = "Identity Not Validated";
    $redirect = true;
} else {
    // allow continue on the page
    $type = $_SESSION['type'];
}

if(!isset($_REQUEST['reset'])){
    // badly formed data
    if(!isset($_SESSION['id']) || empty($_SESSION['id']) ) {
        $_SESSION['message'][] = "Attempted to reset unspecified account";
        $redirect = true;
    }
} else {
    $id = $_REQUEST['reset'];
    //echo "<pre>ID: $id</pre>";
    $_SESSION['id'] = $id;
}

if ( $redirect == true ){
    Header( "location: {$pw_url}/misc/lostPassword.php" );
    exit;
}

?>
<?php require ("../header.php"); ?><!-- ../ only if in sub-dir -->
<?php require ("../navBar.php"); ?><!-- ../ only if in sub-dir -->
<style type="text/css">
.lh_150 {
    width: 120px;
    float: left;
}
.lh_250 {
    width: 200px;
    float: left;
}

.lh_c {
    text-align: left;
    clear: both;
}

.lh_m {
    width: 360px;
    padding: 5px;
    border-left: 1px solid black;
    border-right: 1px solid black;
    margin-left: auto;
    margin-right: auto;
}

.link_button {
    background: url("/widgets/images/button.png") no-repeat scroll left top transparent;
    border: medium none;
    /* color: #385C7E; */
    color: black;
    font: 10px/10px Verdana,Arial,Helvetica,sans-serif;
    margin: 0 3px;
    padding: 4px 0 8px;
    width: 96px;
}

.link_button a {
    color: black;
}
</style>

<h1>Password Recovery</h1>
<div align="center">

<?php
if(isset($_SESSION['message']) && !empty($_SESSION['message']) ){
    ?><div style="color: red; font-weight: bold;">
    <?
    if(is_array($_SESSION['message'])){
    foreach($_SESSION['message'] as $msg){ ?>
        <div><?=$msg?></div>
    <? }
    } else { // not arrray
    ?>  <div><?=$_SESSION['message']?></div> <?
    }
    ?></div><?
    unset($_SESSION['message']);
}
?>

<div align="center" style="width: 400px; margin-top: 10px;">
<div>Passwords must be between 8-15 characters and include at least one letter and one number.</div>
<form method="post" action="pw_action.php">
<input type="hidden" name="id" value="<?=$id?>" />
<input type="hidden" name="type" value="<?=$type?>" />
<div style="margin-top: 10px;">
        <div style="width: 150px; float: left; text-align: left;">New password:</div>
        <div> <input name="pw_1" value="" type="password" autocomplete="off" /> </div>
</div>
<div style="margin-top: 5px;">
        <div style="width: 150px; float: left; text-align: left;">Confirm password:</div>
        <div> <input type="password" name="pw_2" value="" autocomplete="off" /> </div>
</div>
<div style="margin-top: 5px;" align="center">
        <input type="submit" value="Submit" class="link_button" />
        <input type="button" onclick="window.location='<?=$pw_url?>/misc/pw_validate.php';" value="Back" class="link_button" />
    </div>
</form>

</div>

</div>
<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->