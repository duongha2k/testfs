<?php

/* pw_action.php
 *
 * action page to reset password
 * pre: session validation boolean, new password / confirm password details.
 * post: password changed for id. display confirmation
 */

require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');
session_start();
require_once("pw.conf");
$redirect = false;

//echo "<pre>"; print_r($search_region); echo "</pre>";
if(!isset($_SESSION['reset_validated']) || $_SESSION['reset_validated'] != true ){
    // bad data.
    Header( "location: {$pw_url}/misc/lostPassword.php" );
    exit;
} else {
    // allow continue on the page
    $type = $_SESSION['type'];

    $pw1 = $_REQUEST['pw_1'];
    $pw2 = $_REQUEST['pw_2'];
    $alpha_pattern = "/[a-zA-Z]/";
    $num_pattern = "/[0-9]/";

    if( strlen( $pw1 ) < 8 ){
        $redirect = true;
        $_SESSION['message'][] = "Password Too Short";
    }

    if( strlen( $pw1 ) > 15 ){
        $redirect = true;
        $_SESSION['message'][] = "Password Too Long";
    }

    if( !preg_match( $alpha_pattern, $pw1 )){
        $redirect = true;
        $_SESSION['message'][] = "Your password must contain at least one letter.";
    }

    if( !preg_match( $num_pattern, $pw1 )){
        $redirect = true;
        $_SESSION['message'][] = "Your password must contain at least one number.";

    }

    if( $pw1 != $pw2 ){
        $redirect = true;
        $_SESSION['message'][] = "Password and Confirmation Password Do Not Match";
            }

    if( empty($pw1) ){
        $redirect = true;
        $_SESSION['message'] = array();
        $_SESSION['message'][] = "Password Empty";
    }

    if( empty($pw2) ){
        $redirect = true;
        $_SESSION['message'][] = "Confirmation Password Empty";
    }

    if( $redirect == true ) {
        Header( "location: {$pw_url}/misc/pw_reset.php" );
        exit;
    }

    // redirect out if bad data by now.
    // save changed password to client.
    $db = Zend_Registry::get('DB');
    $data = array(
        'Password' => $pw1,
    );
    //echo "<pre>"; print_r($search_region[ $type ]['id'] ); echo "</pre>";
    //echo "<pre>Field: "; print_r($search_region[ $type ]['field'] ); echo "</pre>";
    //echo "<pre>Data: "; print_r($data); echo "</pre>";
    // echo "<pre>Session: "; print_r($_SESSION); echo "</pre>";

    $where = $pw_conf[ $type ]['id'] . " = " . $_SESSION['id'];
    //$where = 'ClientID = 18';
    //echo "<pre>Where: "; print_r($where); echo "</pre>";

    //$n = $db->update($search_region[ $type ]['table'], $data, $where);
    //echo "<pre>"; print_r($search_region[ $type ]['table']); echo "</pre>";
    //$n = $db->update("saturntest.clients", $data, 'ClientID = 18');
    $n = $db->update($pw_conf[ $type ]['table'], $data, $where);

}

?>


<?php require ("../header.php"); ?><!-- ../ only if in sub-dir -->
<?php require ("../navBar.php"); ?><!-- ../ only if in sub-dir -->
<!--- Add Content Here --->

<h1>Password Recovery</h1>

<div align="center" style="margin-top: 10px;">
<? if($n == 0){ ?>
<div> Password not saved. </div>
<? } else { ?>
<div> Password saved. Please log in at <a href="http://www.fieldsolutions.com">www.fieldsolutions.com</a>.</div>
        <?php
        /*
        $body = "<html>
        Your password has been updated. <br />
        If you haven't changed it, contact customer service. <br />

        Field Solutions Team

        </html>
        ";
        //$to = $_SESSION['email'];
        $to = "vroine@fieldsolutions.com";
        $from = "Field Solutions <no-reply@fieldsolutions.us>";

        $headers = "From: " . $from . "\r\n";
        $headers .= "Reply-to: " . $from . "\r\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1 \r\n";
        $subject = "Field Solutions - Password Reset";

        mail($to, $subject, $body, $headers);
        */
        session_destroy();
        ?>
<? } ?>

</div>
<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->