<?php

/*
 *
 * page to reset password
 * pre: email & md5 hash
 * post: list of user accounts, choose which to reset the password for
 *    uses an md5 hash of the found account's password to verify.
 *    so that it doesn't need to insert a temp hash into a table somewhere.
 *    still considered secure, since to falsify it, they would need the password
 *    and be able to md5 hash it.
 */

require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');
session_start();

$redirect   = false;
$user_valid = false;
$debug      = false;
// config file. search region assoc array.
include_once('pw.conf');
if($debug){
        echo "<pre>SESSION: "; print_r($_SESSION); echo "</pre>";
}

if(isset($_REQUEST['type'])){
    if( !in_array($_REQUEST['type'], array("client", "tech","customer") )){
        $redirect = true;
    }
}

if(isset($_REQUEST['code'])){
    // possibly scrub data, remove nonalphanum characters.
}

if(isset($_REQUEST['email'])){
    // possibly scrub data, remove non email valid characters.
}

if(isset($_SESSION['reset_validated']) && $_SESSION['reset_validated'] == true ) {
        if( empty($_SESSION['code']) && empty($_SESSION['email']) ){
                echo ($debug)?"<pre>Session: reset</pre>":null;
                unset($_SESSION['reset_validated']);
                $redirect = true;
        } else {
                echo ($debug)?"<pre>Session: laizze-faire</pre>":null;
                if(isset($_REQUEST['type']) && !empty($_REQUEST['type'])){
                    $type = $_REQUEST['type'];
                } else {
                    $type = $_SESSION['type'];
                }
                if(isset($_REQUEST['code'])){
                    $code = $_REQUEST['code'];
                } else {
                    $code = $_SESSION['code'];
                }
                if(isset($_REQUEST['email'])){
                    $email = $_REQUEST['email'];
                } else {
                    $email = $_SESSION['email'];
                }
        }

        echo ($debug)?"<pre>Case: Jabberwoky</pre>":null;
        echo ($debug)?"<pre>Email: $email</pre>":null;
        echo ($debug)?"<pre>Code: $code</pre>":null;
        echo ($debug)?"<pre>Type: $type</pre>":null;
        echo ($debug)?"<pre>Verfied: {$_SESSION['reset_validated']}</pre>":null;
} else {
        if(!isset($_REQUEST['email'])){
            // badly formed data
            $redirect = true;
        } else {
            $email = $_REQUEST['email'];
            echo ($debug)?"<pre>Email: $email</pre>":null;
        }

        if(!isset($_REQUEST['code'])){
            // bad data
            $redirect = true;
        } else {
            $code = $_REQUEST['code'];
            echo ($debug)?"<pre>Code: $code</pre>":null;
        }

        if(!isset($_REQUEST['type'])){
            // bad data
            $redirect = true;
        } else {
            $type = $_REQUEST['type'];
            echo ($debug)?"<pre>Type: $type</pre>":null;
        }
        echo ($debug)?"<pre>Case: Cerrado</pre>":null;
        echo ($debug)?"<pre>Verfied: {$_SESSION['reset_validated']}</pre>":null;
}

if($redirect == true){
    Header("location: {$pw_url}/misc/lostPassword.php");
    exit();
}

?>
<?php require ("../header.php"); ?><!-- ../ only if in sub-dir -->
<?php require ("../navBar.php"); ?><!-- ../ only if in sub-dir -->
<style type="text/css">
.lh_150 {
    width: 120px;
    float: left;
}

.lh_250 {
    width: 200px;
    float: left;
}

.lh_c {
    text-align: left;
    clear: both;
}

.lh_m {
    width: 360px;
    padding: 5px;
    border-left: 1px solid black;
    border-right: 1px solid black;
    margin-left: auto;
    margin-right: auto;
}

.link_button {
    background: url("/widgets/images/button.png") no-repeat scroll left top transparent;
    border: medium none;
    /* color: #385C7E; */
    color: black;
    font: 10px/10px Verdana,Arial,Helvetica,sans-serif;
    margin: 0 3px;
    padding: 4px 0 8px;
    width: 96px;
}

.link_button a {
    color: black;
}
</style>
<div align="center" style="">
<h1>Password Recovery</h1>
<?php

$db = Zend_Registry::get('DB');
// confirm who they are in their search area
$select = $db->select();
$select->from( $pw_conf[ $type ]['table'] )
    ->where( $pw_conf[ $type ]['email'] ." = ". $db->quoteInto("?", $email) )
    ->where( "MD5( CONCAT('" . date("Ymd") . "', Password)) = " . $db->quoteInto("?", $code) );
if ($type == "tech"){
$select->where("Deactivated != 1");
}
$result = $db->fetchAll($select);

/*
 * checking the email and code from the email.
 * pulls up the user's information. if its empty, send back with an error.
 */

if(!empty($result)){
    $user_valid = true;
    $_SESSION['reset_validated'] = true;
    $_SESSION['type'] = $type;
    $_SESSION['email'] = $email;
    $_SESSION['code'] = $code;

    /* query for all of the accounts tied to the user of this type */
    $select = $db->select();
    $select->from( $pw_conf[ $type ]['table'] )
        ->where( $pw_conf[ $type ]['email'] ." = ". $db->quoteInto("?", $email) );
    if($type == "tech"){
        //$select->where("Deactivated != 1");
    }
    $result = $db->fetchAll($select);

    foreach($result as $row){

        switch($type){
            case "tech":
                $contact_name = $row['FirstName'] . " " . $row['LastName'];
                break;
            default:
                $contact_name = $row[ $pw_conf[ $type ]['contact'] ];
                break;
        }

    ?>
        <div class="lh_m" style="height: 5px; background-color: #dddddd; margin-top: 5px;">
        .
        </div>
        <div class="lh_m" style="height: 80px;">
            <div class="lh_c">
                <div class="lh_150">Name:</div>
                <div class="lh_250"><? if(empty($contact_name)){ ?><em>blank</em><? } else { ?><?=$contact_name?><? } ?> </div>
            </div>
            <div class="lh_c">
                <div class="lh_150">Username:</div>
                <div class="lh_250"><?=$row[ $pw_conf[ $type ]['name'] ]?></div>
            </div>

            <div class="lh_c">
                <div class="lh_150">Email Address: </div>
                <div class="lh_250"><?=$row[ $pw_conf[ $type ]['email'] ]?></div>
            </div>

            <? if( !empty ($row[ $pw_conf[ $type ]['company'] ])){ ?>
            <div class="lh_c">
                <div class="lh_150">Company: </div>
                <div class="lh_250"><?=$row[ $pw_conf[ $type ]['company'] ]?></div>
            </div>
            <? } ?>
        </div>
        <? if ($row['Deactivated']){ ?>
        <div class="lh_m" style="margin-top: -25px;">
            <h3 style="color: red;">Attention!</h3>
This technician account is restricted. You may call 952-288-2500, option 1 if you think this is in error. Any earned payments (client approved) at the time of restriction will be paid as normal. Payments for any future work to this Taxpayer ID # (under any FS technician ID#) will be withheld. Any communications to FieldSolutions clients or FieldSolutions technicians will be prosecuted as allowed by law as a violation of your signed confidential information and independent contractors agreement.
        </div>
        <? } else { ?>
        <div class="lh_m">
            <div class="link_button">
                <a href="pw_reset.php?reset=<?=$row[ $pw_conf[ $type ]['id'] ]?>">reset password</a>
            </div>
        </div>
        <? } ?>
        <br style="clear: both;" />
    <?
        if($debug){
             echo "<pre>"; print_r($row); echo "</pre>";
        }
    } // end foreach
} else {

        ?>
        <div style="margin-top: 20px;">
        Invalid Reset Link <br />
        The Link you are using is invalid or expired.<br />
        </div>
        <?
    if($debug){
        echo "<pre>"; print_r($_SESSION); echo "</pre>";
    }
}

?>
</div>
<!--- End Content --->
<?php require ("../footer.php"); ?><!-- ../ only if in sub-dir -->