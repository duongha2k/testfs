<?php
	$page = "login";
	require_once("header.php");
	require_once("navBar.php");
	require_once("library/smtpMail.php");
	$message = "Location: " . base64_decode($_GET["r"]);
	smtpMailLogReceived("Page Unavailable Script", "nobody@fieldsolutions.com", "tngo@fieldsolutions.com", "Page Unavailable Reached", $message, $message, "Page Unavailable Script");
?>
<br/>
<br/>
<div style="text-align:center">
	Sorry. This page is unavailable.
</div>
<?php require ("footer.php"); ?>