/**
 * FSWidgetWODetails
 *
 * @param Object|null params
 * @access public
 * @return void
 */
function FSWidgetISODetails ( params, roll ) {
    FSWidget.call(this, params);

    this.params.WIDGET.avaliableTabs = {
        update : {module:'admindashboard',controller:'iso',action:'update'},
        create : {module:'admindashboard',controller:'iso',action:'create'},
        list   : {module:'admindashboard',controller:'iso',action:'list'},
        search   : {module:'admindashboard',controller:'iso',action:'search'}
    };
    this._option = 0;
    this._roll = roll;
    this._customfunctions = params.customfunctions || {};

    this._redirect =false;
    this.success = false;
    this._redirectTo = '/admin/ISO_List.php';
}

FSWidgetISODetails.prototype = new FSWidget();
FSWidgetISODetails.prototype.constructor = FSWidgetISODetails;

FSWidgetISODetails.NAME          = 'FSWidgetISODetails';
FSWidgetISODetails.VERSION       = '0.1';
FSWidgetISODetails.DESCRIPTION   = 'Class FSWidgetISODetails';

FSWidgetISODetails.prototype.roll = function() {
    return this._roll;
}

/**
 *  show
 *  Function display widget
 *  @param Object|null options
 */
FSWidgetISODetails.prototype.show = function(options) {
    var p = options || {tab:this.currentTab,params:this.getParams()};
    var queryString = '/';
    var _url;

    // TODO params needs be reconfigured
    if ( p.params !== undefined && p.params !== null && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params ) {
            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] === true || p.params[key] === false ) {
                if ( p.params[key] === false ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] !== null && p.params[key] !== "" && p.params[key] !== 'none' ) {
                queryString += key + '/' + p.params[key] + '/';
            } else {
                delete p.params[key];
            }
        }
    } else {
        p.params = {};
    }

    _url = this._makeUrl(this.currentTab, queryString);
    this.showPreloader();

    if ( this.tabOpenXhrRequest ) {
        this.tabOpenXhrRequest.abort();
        this.tabOpenXhrRequest = null;
    }

    this.tabOpenXhrRequest = $.ajax({
        url : _url,
        data : p.params,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/clients/logIn.php";
            } else {
                FSWidget.fillContainer("Sorry, but an error occurred. No results found.", this);
            }
            this.tabOpenXhrRequest = null;
            
        },
        success : function(data, status, xhr) {
            $('body').css('cursor', '');
            FSWidget.fillContainer(data, this);
            this.prepareForm();
            if (typeof(calendarInit) == 'function') calendarInit();
        }
    });

}

FSWidgetISODetails.prototype.prepareForm = function() {
//		detailsWidget.onChangeCountry();
}


FSWidgetISODetails.prototype.showPreloader = function() {
    if (this._customfunctions.showPreloader !== undefined && typeof this._customfunctions.showPreloader === 'function' ) {
        this._customfunctions.showPreloader();
    }else{
        $('body').css('cursor', 'wait');
        var img = "<div class='tCenter' ><img style='margin:10% auto;' alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></div>";
        FSWidget.fillContainer(img, this);
    }
}

FSWidgetISODetails.prototype.updateProfileDetails = function() {
    opt = this._option;
    this._option = 0;
    this.showStartPopup();
    this._redirect =false;
    this._updatestatus = {success:1, errors:[]};
    this._activeupload = 0;
    this._redirectTo = '/clients/dashboard.php';
    params = $('#UpdateClientForm').serialize();
    params += '&Company_ID='+window._company;
    $.ajax({
        url         : '/widgets/dashboard/client/do-update/',
        dataType    : 'json',
        data        : params,
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            this._updatestatus = data;
            if (data.success) {
                this._uploadsCounter = 0;
                this.params.client_id = data.client_id;

                this._redirect = false;

                if ( $('#Company_Logo').val() ) {
                    this._uploadsCounter++;
                    this.asyncUploadFile( 'Company_Logo', 'update' );
                }                
                
                if ( this._uploadsCounter <= 0 ) {
                    if(opt == 0)
                        this.showMessages(this._updatestatus);
                    else
                        this.callUpdateOptionsScript(opt);
                }
            }else{
                this.showMessages(data);
            }
        }
    });
}

FSWidgetISODetails.prototype.showStartPopup = function() {
    var html = "<div class=\"tCenter\"><img alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></div>";
    this._roll.autohide(false);
    var opt = {
        width       : 300,
        height      : '',
        position    : 'middle',
        title       : 'Processing',
        body        : html
    };
    return this._roll.showNotAjax(null, null, opt);
}

FSWidgetISODetails.prototype.showMessages = function(data, successMsg, autohide) {
    
    var html = '';
    if (this._redirect || data.success) { //always refresh page on success
        if (window.opener) {try{window.opener.reloadTabFrame();} catch (e) {}}
        window.location = this._redirectTo;
    }
    if (data.success) {
		if (!successMsg)
        html += '<div class="success"><ul><li>Success!</li></ul></div>';
		else
			html = msg;
    }else{
        html+='<div class="errors"><ul>';
        for (i=0;i<data.errors.length;i++) {
            if(typeof data.errors[i] === 'object')
                html+='<li>'+data.errors[i].message+'</li>';
            else
                html+='<li>'+data.errors[i]+'</li>';
        }
        html+='</ul></div>';
    }
	if (!successMsg)
    html+='<div class="tCenter"><input type="button" class="link_button_popup" onclick="detailsWidget.roll().hide();" value="Ok" /></div>';
	if (autohide == undefined) autohide = false;
    this._roll.autohide(autohide);

    var opt = {};
    opt.width    = 400;
    opt.height   = '';
    opt.position = 'middle';
    opt.title    = '';
    opt.body     = html;

    if (data.success) {
        opt.width = 300;
    }
    return this._roll.showNotAjax(null, null, opt);
}

/**
 *  getParams
 *
 *  Function collect all params for make request
 *  return object
 */
FSWidgetISODetails.prototype.getParams = function () {
    params = {};
    if(typeof(this.params.WOFILTERS.ClientID)== 'string') params['ClientID'] = this.params.WOFILTERS.ClientID;
    if(typeof(this.params.WOFILTERS.Password)== 'string') params['Password'] = this.params.WOFILTERS.Password;
    if(typeof(this.params.WOFILTERS.Admin)== 'string') params['Admin'] = this.params.WOFILTERS.Admin;
    if(typeof(this.params.WOFILTERS.AcceptTerms)== 'string') params['AcceptTerms'] = this.params.WOFILTERS.AcceptTerms;
    if(typeof(this.params.WOFILTERS.AccountEnabled)== 'string') params['AccountEnabled'] = this.params.WOFILTERS.AccountEnabled;
    if(typeof(this.params.WOFILTERS.ShowOnReports)== 'string') params['ShowOnReports'] = this.params.WOFILTERS.ShowOnReports;
    if(typeof(this.params.WOFILTERS.ProjectManager)== 'string') params['ProjectManager'] = this.params.WOFILTERS.ProjectManager;
    if(typeof(this.params.WOFILTERS.WebsiteURL)== 'string') params['WebsiteURL'] = this.params.WOFILTERS.WebsiteURL;
    if(typeof(this.params.WOFILTERS.ContactPhone1)== 'string') params['ContactPhone1'] = this.params.WOFILTERS.ContactPhone1;
    if(typeof(this.params.WOFILTERS.ContactPhone2)== 'string') params['ContactPhone2'] = this.params.WOFILTERS.ContactPhone2;
    if(typeof(this.params.WOFILTERS.Email1)== 'string') params['Email1'] = this.params.WOFILTERS.Email1;
    if(typeof(this.params.WOFILTERS.Email2)== 'string') params['Email2'] = this.params.WOFILTERS.Email2;
    if(typeof(this.params.WOFILTERS.ContactName)== 'string') params['ContactName'] = this.params.WOFILTERS.ContactName;
    if(typeof(this.params.WOFILTERS.CompanyName)== 'string') params['CompanyName'] = this.params.WOFILTERS.CompanyName;
    if(typeof(this.params.WOFILTERS.UserType)== 'string') params['UserType'] = this.params.WOFILTERS.UserType;
    if(typeof(this.params.WOFILTERS.Company_ID)== 'string') params['Company_ID'] = this.params.WOFILTERS.Company_ID;


    if(typeof(this.params.WOFILTERS.sortBy)== 'string') params['sortBy'] = this.params.WOFILTERS.sortBy;
    if(typeof(this.params.WOFILTERS.sortDir)== 'string')params['sortDir'] = this.params.WOFILTERS.sortDir;
    if(typeof(this.params.WOFILTERS.letter)== 'string') params['letter'] = this.params.WOFILTERS.letter;
    if(typeof(this.params.WOFILTERS.page)== 'string')   params['page'] = this.params.WOFILTERS.page;
    if(typeof(this.currentTab)== 'string')              params['tab'] = this.currentTab;
    if(typeof(this.params.WOFILTERS.page)== 'string')   params['page'] = this.params.WOFILTERS.page;
    if(typeof(this.params.WOFILTERS.win)== 'string')    params['win'] = this.params.WOFILTERS.win;

    return params;
}

FSWidgetISODetails.prototype.setParams = function (params)
{
    if ( !params || typeof params !== 'object' ) return;

    if (typeof(params.sortBy)  != 'undefined' ) this.params.WOFILTERS.sortBy = params.sortBy;
    if (typeof(params.sortDir) != 'undefined' ) this.params.WOFILTERS.sortDir = params.sortDir;
    if (typeof(params.letter)  != 'undefined' ) this.params.WOFILTERS.letter = params.letter;
    if (typeof(params.page)    != 'undefined' ) this.params.WOFILTERS.page = params.page;
    if (typeof(params.company) != 'undefined' ) this.params.WOFILTERS.company = params.company;
    if (typeof(params.win)     != 'undefined' ) this.params.WOFILTERS.win = params.win;

    if (typeof(params.Password)       != 'undefined' ) this.params.WOFILTERS.Password = params.Password;
    if (typeof(params.Admin)          != 'undefined' ) this.params.WOFILTERS.Admin = params.Admin;
    if (typeof(params.AcceptTerms)    != 'undefined' ) this.params.WOFILTERS.AcceptTerms = params.AcceptTerms;
    if (typeof(params.AccountEnabled) != 'undefined' ) this.params.WOFILTERS.AccountEnabled = params.AccountEnabled;
    if (typeof(params.ShowOnReports)  != 'undefined' ) this.params.WOFILTERS.ShowOnReports = params.ShowOnReports;
    if (typeof(params.ProjectManager) != 'undefined' ) this.params.WOFILTERS.ProjectManager = params.ProjectManager;
    if (typeof(params.WebsiteURL)     != 'undefined' ) this.params.WOFILTERS.WebsiteURL = params.WebsiteURL;
    if (typeof(params.ContactPhone1)  != 'undefined' ) this.params.WOFILTERS.ContactPhone1 = params.ContactPhone1;
    if (typeof(params.ContactPhone2)  != 'undefined' ) this.params.WOFILTERS.ContactPhone2 = params.ContactPhone2;
    if (typeof(params.Email1)         != 'undefined' ) this.params.WOFILTERS.Email1 = params.Email1;
    if (typeof(params.Email2)         != 'undefined' ) this.params.WOFILTERS.Email2 = params.Email2;
    if (typeof(params.ContactName)    != 'undefined' ) this.params.WOFILTERS.ContactName = params.ContactName;
    if (typeof(params.CompanyName)    != 'undefined' ) this.params.WOFILTERS.CompanyName = params.CompanyName;
    if (typeof(params.ClientID)       != 'undefined' ) this.params.WOFILTERS.ClientID = params.ClientID;
    if (typeof(params.UserType)       != 'undefined' ) this.params.WOFILTERS.UserType = params.UserType;
    if (typeof(params.Company_ID)     != 'undefined' ) this.params.WOFILTERS.Company_ID = params.Company_ID;

}

FSWidgetISODetails.prototype.onChangeCountry = function () {
    var country = $("#Country").val();
    if(country != "US" && country != "CA") 
    {
        jQuery("#requiredPhone").css("display","none");
    }
    else
    {
        jQuery("#requiredPhone").css("display","");
    }

    if(country != "US" && country != "CA" && country != "MX") 
    {
        jQuery("#requiredState").css("display","none");
    }
    else
    {
        jQuery("#requiredState").css("display","");
    }

    $.ajax({
        url         : '/widgets/dashboard/js/state-info/',
        dataType    : 'json',
        data        : {country: $("#Country").val()},
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            if (data) {
                //AUTOFILL
				var option = '';
				option += '<option selected="selected" value="">Select State</option>';
                for(index in data) {
					option += '<option value="' + index + '">' + data[index] + '</option>';
				}
				$("#State").html(option);
				if (this._queueSiteState) $('#State').val(this._queueSiteState);
				this._queueSiteState = null;
			}
        }
    });	
    
    $.ajax({
            type: "POST",
            url: "/widgets/dashboard/do/country-require-zip",
            dataType    : 'json',
            cache       : false,
            data: 'countrycode='+jQuery("#Country").children('option:selected').val(),
            success: function (data) {
                var requireZip = data.success;
                if(requireZip)
                {
                    jQuery("#requiredZip").css("display","");
}
                else
                {
                    jQuery("#requiredZip").css("display","none");
                }   
            }
    });    
}

FSWidgetISODetails.prototype.isValidContactPhone = function(val, allowExt) {
	if (val == null || val == "") return true;
	valStrip = val.replace(/[^0-9]/g, "");
	if (valStrip.length == 10) {
		val = massagePhone(val);
		return isValidPhone(val);
	}
	else {
		if (!allowExt) return false;
		validFormat = /^[(]?(\d{3})[)]?(?:\s|-|.)*(\d{3})[-\s.]*(\d{4})[^0-9]+([0-9]*)[^0-9]*([0-9]*)$/;
		parts = val.match(validFormat);
		return !(parts == null);
	}
}
FSWidgetISODetails.prototype.validEmailList = function(list) {
	if (list == null || list == "") return false;
	list = list.replace(/\s/g);
	listParts = list.split(",");
	valid = true;
	for (index in listParts) {
		email = listParts[index];
		if (!isValidEmail(email)) {
			valid = false;
		}
	}
	return valid;
}
FSWidgetISODetails.prototype.validateCreateSubmit = function(role,evt) {
    if (role == null || role == "") return false;
    this._redirect = false;
    var el = this;
    if(this.success) return true;
    
    this.success = false;
    $.ajax({
            type: "POST",
            url: "/widgets/dashboard/do/country-require-zip",
            dataType    : 'json',
            cache       : false,
            data: 'countrycode='+jQuery("#Country").children('option:selected').val(),
            success: function (data) {
                var requireZip = data.success;
    updatestatus = {success:1, errors:[]};
    
    if($('#Company_Name').val() == '') updatestatus.errors.push('Company Name is empty but required');
    if($('#Address_1').val() == '') updatestatus.errors.push('Address is empty but required');
                if($('#Zip').val() == '' && requireZip) updatestatus.errors.push('Zip / Postal Code is empty but reqired');
    if($('#City').val() == '') updatestatus.errors.push('City is empty but reqired');
    if($('#State').val() == '' && ($('#Country').val() == 'US' || $('#Country').val() == 'CA' || $('#Country').val() == 'MX')) updatestatus.errors.push('State is empty but reqired');
    if($('#Country').val() == '') updatestatus.errors.push('Country is empty but reqired');
    if($('#Contact_Name').val() == '') updatestatus.errors.push('Contact Name is empty but reqired');
                if($('#Contact_Phone').val() == '' && ($('#Country').val() == 'US' || $('#Country').val() == 'CA')) updatestatus.errors.push('Contact Phone is empty but reqired');
    if($('#Contact_Email').val() == '') updatestatus.errors.push('Contact Email is empty but reqired');
	
                    if (!el.isPhoneValid($('#Contact_Phone').val()) && ($('#Country').val() == 'US' || $('#Country').val() == 'CA')) {
		$('#Contact_Phone').val("");
		updatestatus.errors.push('Contact Phone must ###-###-#### or ###-###-#### x ####');
	}

    if(updatestatus.errors.length <= 0)
                {    
                    el.success = true;
                    jQuery(evt).submit();
                }
                else
                {    
    updatestatus.success = 0;
                    el.showMessages(updatestatus);
                }
            }
    });
    return false;
}

FSWidgetISODetails.prototype.isPhoneValid = function(val) {
	if (val == null || val == "") return true;
	validFormat = /^([(]?(\d{3})[)]?[^0-9]*(\d{3})[^0-9]*(\d{4})|[(]?(\d{3})[)]?[^0-9]*(\d{3})[^0-9]*(\d{4})[^0-9]+([0-9]*)[^0-9]*([0-9]*))$/;
	parts = val.match(validFormat);
	return !(parts == null);
}

FSWidgetISODetails.prototype.validateUpdateSubmit = function(role) {
    if (role == null || role == "") return false;
    this._redirect = false;
    updatestatus = {success:1, errors:[]};
    if(role == 'client'){
        if($('#ContactName').val() == '') updatestatus.errors.push('Contact Name is empty but reqired');
        if($('#CompanyName').val() == '') updatestatus.errors.push('Company Name is empty but reqired');
        if($('#ContactPhone1').val() == '') updatestatus.errors.push('Primary Phone is empty but reqired');
        if($('#Email1').val() == '') updatestatus.errors.push('Email Address is empty but reqired');
        if($('#Password').val() == '') updatestatus.errors.push('Desired Password is empty but reqired');
    }

    if($('#Username').val() == '') updatestatus.errors.push('Desired Username is empty but reqired');
    if(!this.isValidContactPhone($('#ContactPhone1').val(),true) && $('#ContactPhone1').val()!='')
        updatestatus.errors.push('Primary Phone must be a valid 10 digit number with optional extension similar to ###-###-#### x ####');
    if(!this.isValidContactPhone($('#ContactPhone2').val(),true) && $('#ContactPhone2').val()!='')
        updatestatus.errors.push('Secondary Phone must be a valid 10 digit number with optional extension similar to ###-###-#### x ####');
    if(!this.validEmailList($('#Email1').val()) && $('#Email1').val()!='')
        updatestatus.errors.push('Primary Email Address is not a valid email address');
    if(!this.validEmailList($('#Email2').val()) && $('#Email2').val()!='')
        updatestatus.errors.push('Secondary Email Address is not a valid email address');

    if(updatestatus.errors.length <= 0)
        return true;
    updatestatus.success = 0;
    this.showMessages(updatestatus);
    return false;
}
FSWidgetISODetails.prototype.validateSearchSubmit = function() {
    this._redirect = false;
    updatestatus = {success:1, errors:[]};
    if(!this.isValidContactPhone($('#ContactPhone1').val(),true) && $('#ContactPhone1').val()!='') updatestatus.errors.push('Primary Phone must be a valid 10 digit number with optional extension similar to ###-###-#### x ####');
    if(!this.isValidContactPhone($('#ContactPhone2').val(),true) && $('#ContactPhone2').val()!='') updatestatus.errors.push('Secondary Phone must be a valid 10 digit number with optional extension similar to ###-###-#### x ####');
    if(!this.validEmailList($('#Email1').val()) && $('#Email1').val()!='') updatestatus.errors.push('Email Address is not a valid email address');
    if(updatestatus.errors.length <= 0)
        return true;
    updatestatus.success = 0;
    this.showMessages(updatestatus);
    return false;
}

FSWidgetISODetails.prototype.validateClientUpdateSubmit = function() {
    return true;
}