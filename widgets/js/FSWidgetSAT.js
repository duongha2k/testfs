/**
 * FSWidgetSAT
 *
 * @param Object|null params
 * @access public
 * @return void
 * @author Artem Sukharev
 */
function FSWidgetSAT ( params ) {
    FSWidget.call(this, params);

    this.waitImage = new Image();
    this.waitImage.src = "/widgets/images/progress.gif";     //  pre load wait image
    this.waitImage.width = 190;
    this.waitImage.height = 14;

    this.params.baseUrl = document.location.protocol + '//' + document.location.hostname + '/widgets/dashboard/popup/sat-form/';
    this.params.container = 'plhFSWidgetSAT';
}

FSWidgetSAT.prototype = new FSWidget();
FSWidgetSAT.prototype.constructor = FSWidgetSAT;

FSWidgetSAT.NAME          = 'FSWidgetSAT';
FSWidgetSAT.VERSION       = '0.1';
FSWidgetSAT.DESCRIPTION   = 'Class FSWidgetSAT';

FSWidgetSAT.prototype.showPreloader = function() {
    $('body').css('cursor', 'wait');
    var img = "<div class='tCenter' ><img style='margin:10% auto;' alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></div>";
    FSWidget.fillContainer(img, this);
}

FSWidgetSAT.prototype.show = function() {
    
    this.showPreloader();
    data = this.params.data || {};

    if ( this.tabOpenXhrRequest ) {
        this.tabOpenXhrRequest.abort();
        this.tabOpenXhrRequest = null;
    }

    this.tabOpenXhrRequest = jQuery.ajax({
        url : this.params.baseUrl,
        data : data,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/clients/logIn.php";
            } else {
                FSWidget.fillContainer("Sorry, but an error occurred. No results found.", this);
            }
            this.tabOpenXhrRequest = null;
        },
        success : function(data, status, xhr) {
            $('body').css('cursor', '');
            FSWidget.fillContainer(data, this)
        }
    });
}

FSWidgetSAT.prototype.doRequest = function(options) {

    //$('#edit_PreferredStatus1')
    //form = $('#'+options.form);
    //735
    data = {id : this.params.data.id, techID : this.params.data.techID, companyID : this.params.data.companyID, SATRecommended : null, SATPerformance : null, PreferredStatus : null, update : true, FSExpertId: null, chkskillcategory: null}
    $('#'+options.form+' input:radio[name="SATRecommended"]').each(function(){
        if ( $(this).attr('checked') ) data.SATRecommended = $(this).val();
    })
    $('#'+options.form+' input:radio[name="SATPerformance"]').each(function(){
        if ( $(this).attr('checked') ) data.SATPerformance = $(this).val();
    })
    $('#'+options.form+' input:radio[name="PreferredStatus"]').each(function(){
        if ( $(this).attr('checked') ) data.PreferredStatus = $(this).val();
    })

    $('#'+options.form+' input:hidden[name="FSExpertId"]').each(function(){
        data.FSExpertId = $(this).val();
    })
    $('#'+options.form+' input:checkbox[name="chkskillcategory"]').each(function(){
        if ( $(this).attr('checked') ) data.chkskillcategory = $(this).val();
    })
    
    if ( this.tabOpenXhrRequest ) {
        this.tabOpenXhrRequest.abort();
        this.tabOpenXhrRequest = null;
    }

	this.showPreloader();
    this.tabOpenXhrRequest = jQuery.ajax({
        url : this.params.baseUrl,
        data : data,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/clients/logIn.php";
            } else {
                FSWidget.fillContainer("Sorry, but an error occurred. No results found.", this);
            }
            this.tabOpenXhrRequest = null;
        },
        success : function(data, status, xhr) {
            $('body').css('cursor', '');
            FSWidget.fillContainer(data, this);
        }
    });

    return false;
}