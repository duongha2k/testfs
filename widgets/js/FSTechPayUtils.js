function FSTechPayUtils() {
}

FSTechPayUtils.prototype.calculate = function(values) {
	var baseTechPay = new Number(values.baseTechPay);
	var OutofScope_Amount = new Number(values.OutofScope_Amount);
	var TripCharge = new Number(values.TripCharge);
	var MileageReimbursement = new Number(values.MileageReimbursement);
	var MaterialsReimbursement = new Number(values.MaterialsReimbursement);
	var Additional_Pay_Amount = new Number(values.Additional_Pay_Amount);
	var AbortFeeAmount = new Number(values.AbortFeeAmount);
	var calculatedTechHrs = new Number(values.calculatedTechHrs);

	if (!values.ReimbursableExpenseThroughFSApproved_Final || isNaN(values.ReimbursableExpenseThroughFSApproved_Final)) values.ReimbursableExpenseThroughFSApproved_Final = 0;
	
	var reimburseableExpense = new Number(values.ReimbursableExpenseThroughFSApproved_Final);


	var PayAmount;
	if (AbortFeeAmount != 0) {
		PayAmount = AbortFeeAmount;
	} else {
		PayAmount = baseTechPay;
		PayAmount += OutofScope_Amount + TripCharge + MileageReimbursement + MaterialsReimbursement + Additional_Pay_Amount + reimburseableExpense;
	}

	PayAmount *= 100;
	PayAmount = new String(Math.round(PayAmount));
	PayAmount = PayAmount.substring(0, PayAmount.length - 2) + "." + PayAmount.substring(PayAmount.length - 2);

	return PayAmount;
}

FSTechPayUtils.prototype.formatAmount = function(value) {
	if (value === undefined || value === null) return "0.00";
	value = new String(value);
	if (!value.match(/^-?\d*\.?\d*$/)) return value;

	var length = value.length;
	if (length == 0) return "0.00";
	var pos = value.lastIndexOf('.');
	if (pos >= 0) {
		var addZero = 2 - (length - pos - 1);
		while (addZero > 0) {
			value += "0";
			addZero--;
		}
	} else {
		value += ".00";
	}
	if (pos == 0) {
		value = "0" + value;
	}
	return value;
}
