function FSMyTechs( wd, roll ) {
	this._wd = wd;
	this._roll = roll;
	this._html = '';
	this._container = null;
}

FSMyTechs.prototype.container = function() {
	if (this._container == null) {
		var container = document.createElement('div');
        container.style.display = 'none';
        $('body')[0].appendChild(container);
        this._container = container;
	}
	return this._container;
}

FSMyTechs.prototype.buildHtml = function () {
	var _url = this._wd._makeUrl('mytechs', '/company/' + this._wd.filters().company());

    jQuery.ajax({
        url : _url,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        success : function(data, status, xhr) {
            $(this.container()).html(data);
        },
        error : function (xhr, status, error) {
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/clients/logIn.php";
            }
        }
    });
};

FSMyTechs.prototype.doFind = function()
{
        var win = $("#fMTrdWIN").val();
        var clientWOrd = $("#fMTClientWO").val();
        var zip = $("#fMTZip").val();
        var distance = $("#fMTDistance").val();
        //var isDash = $("#isDash").val();
        var containerName = $("#containerName").val();
        var isDash = 1;

        var wd = this._wd;
        var newwindow;

    if ((zip.length > 0 || win.length > 0 || clientWOrd.length > 0) && distance > 0)
    {
                if(isDash == '1'){
            $(document).ready(function()
            {
                if(isPM)
                {
                    openPopupWin('/clients/findMyTechs.php?v=' + wd.filters().company() + '&win=' + escape(win) + '&clientWO=' + escape(clientWOrd) + '&zip=' + escape(zip) + '&distance=' + escape(distance) + '&c=' + escape(containerName),"MyTech");
                }
                else
                {
                        openLightBoxPage( '/clients/findMyTechs.php?v=' + wd.filters().company() + '&win=' + escape(win) + '&clientWO=' + escape(clientWOrd) + '&zip=' + escape(zip) + '&distance=' + escape(distance) + '&c=' + escape(containerName), 1000, 800 );
                        }
                    });
                } else {
                        newwindow = window.open('/clients/findMyTechs.php?v=' + wd.filters().company() + '&win=' + escape(win) + '&clientWO=' + escape(clientWOrd) + '&zip=' + escape(zip) + '&distance=' + escape(distance),'name','height=600, width=950, left=100, top=100, resizable=yes, scrollbars=yes, toolbar=no,status=no');
                }

                if(isDash != '1'){
                        newwindow.focus();
                }
                this._roll.hide();
        }
}

FSMyTechs.prototype.prepare = function() {
	if ($('#edit_Zipcode').length && $('#edit_Zipcode').val().length > 0) {
		// set defoult zip code
		$('#fMTZip').val($('#edit_Zipcode').val() );
	}
	$("#fWOrdMessage").html('');
}
FSMyTechs.prototype.hideLoading = function() {
    jQuery("#comtantPopupID").hide();
    jQuery("#iframemytech").show();
}

