function FSProjectSortTool( sortSelect, directionSelect, wd, roll, defaultSortTool )
{
    if (wd !== undefined || roll !== undefined){

    if ( sortSelect === undefined )             sortSelect = null;
    if ( directionSelect === undefined )        directionSelect = null;

    if ( !(wd instanceof FSWidgetProject) ) {
        throw new Error('wd should be instance of FSWidgetDashboard');
    }
    if ( !(roll instanceof FSPopupRoll) ) {
        throw new Error('roll should be instance of FSPopupRoll');
    }

    this._sortSelect        = null;
    this._directionSelect   = null;
    this._currentSort       = 'None';
    this._currentDerection  = 'asc';
    this._toolInited        = false;
    this._wd                = wd;
    this._roll              = roll;
    this._currentSortTool   = null;

    this._closeBtn          = null;

    this.setUsedTool(defaultSortTool);

    this._fullSortPopup     = {
        _init       : false,

        _field1ID   : null,
        _field1Val  : null,
        _field1Dir  : 'asc',

        _field2ID   : null,
        _field2Val  : null,
        _field2Dir  : 'asc',

        _field3ID   : null,
        _field3Val  : null,
        _field3Dir  : 'asc',

        _html       : null,

        _inUse      : false
    };

    var $this = this;
    this.cancelHadler = function () {
        $this.cancelSort();
    }

    if ( sortSelect && directionSelect ) {
        if ( typeof sortSelect === 'string' ) {
            this._sortSelect = document.getElementById(sortSelect);
        } else {
            this._sortSelect = sortSelect;
        }
        if ( typeof directionSelect === 'string' ) {
            this._directionSelect = document.getElementById(directionSelect);
        } else {
            this._directionSelect = directionSelect;
        }
    }

    this._sort = {};
    this._sort_defaults = {};
    
    this.prepareItems();
    this.initSortTool();
    this.prepareUsedTool();
    }
}

FSProjectSortTool.NAME          = 'FSProjectSortTool';
FSProjectSortTool.VERSION       = '0.1';
FSProjectSortTool.DESCRIPTION   = 'Class WidgetSortTool: Sort Tool for Dashboard';

FSProjectSortTool.prototype.name = function()
{
    return this.constructor.NAME;
}

FSProjectSortTool.prototype.version = function()
{
    return this.constructor.VERSION;
}

FSProjectSortTool.prototype.description = function()
{
    return this.constructor.DESCRIPTION;
}

FSProjectSortTool.prototype.prepareUsedTool = function ()
{
    if ( this.getUsedTool() === 'quick' ) {
        if ( !this.fullInUsage() && this._fullSortPopup._init ) {
            this.sort(this._fullSortPopup._field1Val);
            this.direction(this._fullSortPopup._field1Dir);

            var i;

            for (i=0; i<this._sortSelect.options.length; ++i) {
                if ( this._sortSelect.options[i].value === this.sort() ) {
                    this._sortSelect.options[i].selected = true;
                    break;
                }
            }
            for (i=0; i<this._directionSelect.options.length; ++i) {
                if ( this._directionSelect.options[i].value === this.direction() ) {
                    this._directionSelect.options[i].selected = true;
                    break;
                }
            }
        }
    } else if ( this.getUsedTool() === 'full' ) {
        this._fullSortPopup._field1Val = this._currentSort;
        this._fullSortPopup._field1Dir = this._currentDerection;
        this._fullSortPopup._field2Val = null;
        this._fullSortPopup._field2Dir = 'asc';
        this._fullSortPopup._field3Val = null;
        this._fullSortPopup._field3Dir = 'asc';

        this._sortSelect.options[0].selected = true;
        this._directionSelect.options[0].selected = true;
    }
}

FSProjectSortTool.prototype.fullInUsage = function ( bool )
{
    if ( bool !== undefined && bool !== null ) {
        this._fullSortPopup._inUse = ( bool ) ? true : false;
    }
    return this._fullSortPopup._inUse;
}

FSProjectSortTool.prototype.setUsedTool = function ( toolName )
{
    if ( typeof toolName !== 'string' || typeof toolName === 'string' && toolName !== 'full' ) {
        this._currentSortTool = 'quick';
    } else {
        this._currentSortTool = 'full';
    }
}

FSProjectSortTool.prototype.getUsedTool = function ()
{
    return this._currentSortTool;
}

FSProjectSortTool.prototype.sort = function ( val )
{
    if ( val !== undefined ) {
        this._currentSort = val;
    }
    return this._currentSort;
}

FSProjectSortTool.prototype.direction = function ( val )
{
    if ( val !== undefined ) {
        this._currentDerection = val;
    }
    return this._currentDerection;
}

FSProjectSortTool.prototype.setFullSortVal = function ( value, num )
{
    nam = parseInt(num, 10);

    if ( value === undefined || num < 1 || num > 3 )
        return;

    switch ( num ) {
        case 1: this._fullSortPopup._field1Val = value; break;
        case 2: this._fullSortPopup._field2Val = value; break;
        case 3: this._fullSortPopup._field3Val = value; break;
    }
}

FSProjectSortTool.prototype.setFullSortDir = function ( value, num )
{
    nam = parseInt(num, 10);

    if ( value === undefined || num < 1 || num > 3 )
        return;

    switch ( num ) {
        case 1: this._fullSortPopup._field1Dir = value; break;
        case 2: this._fullSortPopup._field2Dir = value; break;
        case 3: this._fullSortPopup._field3Dir = value; break;
    }
}

FSProjectSortTool.prototype.update = function ()
{
    if ( this._sortSelect && this._directionSelect ) {
        this.sort(this._sortSelect.options[this._sortSelect.selectedIndex].value);
        this.direction(this._directionSelect.options[this._directionSelect.selectedIndex].value);
    }
}

FSProjectSortTool.prototype.initSortTool = function () {
    if ( !this._sortSelect || !this._directionSelect || !this._wd )
        return;

    /* Save old quick sort when user going to other tab */
    //var oldSort = this.sort();
    //var oldDir = this.direction();
    /* Save old quick sort when user going to other tab */

    this._toolInited = true;
    this._sortSelect.options.length = 0;
    this._directionSelect.options.length = 0;

    this._sortSelect.options[0] = new Option('None', '', false, (this._currentSortTool === 'full') ? true : false);

    var i = 1;
    $this = this;
    /* Build new sorting items */
    $.each(this._sort[this._wd.currentTab], function (idx, val) {
        $this._sortSelect.options[i++] = new Option(val, idx, false, ($this._sort_defaults[$this._wd.currentTab] && $this._sort_defaults[$this._wd.currentTab].sort === idx && $this._currentSortTool === 'quick') ? true : false);
    });
    /* Build new sorting items */

    /* Build new direction items */
    if (this._sort_defaults[$this._wd.currentTab] && this._sort_defaults[$this._wd.currentTab].dir == 'desc') {
        this._directionSelect.options[0] = new Option('Ascending', 'asc', false, false);
        this._directionSelect.options[1] = new Option('Descending', 'desc', true, true);
    } else {
        this._directionSelect.options[0] = new Option('Ascending', 'asc', true, true);
        this._directionSelect.options[1] = new Option('Descending', 'desc', false, false);
    }
    /* Build new direction items */

    /* Restore old sort */
    //if ( oldSort ) {
        //for ( i = 0; i < this._sortSelect.options.length; ++i ) {
            //if ( oldSort === this._sortSelect.options[i].value ) {
                //this._sortSelect.options[i].selected = true;
            //}
        //}
        //for ( i = 0; i < this._directionSelect.options.length; ++i ) {
            //if ( oldDir === this._directionSelect.options[i].value ) {
                //this._directionSelect.options[i].selected = true;
            //}
        //}
    //}
    /* Restore old sort */

    this.update();
}

FSProjectSortTool.prototype.initSortPopup = function ()
{
	FSWidget.filterActivate(true);

    var i;
    var select1 = document.getElementById('_sort_select_' + this._fullSortPopup._field1ID);
    var select2 = document.getElementById('_sort_select_' + this._fullSortPopup._field2ID);
    var select3 = document.getElementById('_sort_select_' + this._fullSortPopup._field3ID);

    select1.options.length = select2.options.length = select3.options.length = 0;

    select2.options[0] = new Option('None', '', true, true);
    select3.options[0] = new Option('None', '', true, true);

    var $this = this;

    i = 0;
    $.each(this._sort[$this._wd.currentTab], function (idx, val) {
        select1.options[i++] = new Option(
            val,
            idx,
            ($this._sort_defaults[$this._wd.currentTab] && $this._sort_defaults[$this._wd.currentTab].sort === idx) ? true : false,
            ( $this._fullSortPopup._field1Val !== null ) ?
                (( idx === $this._fullSortPopup._field1Val ) ? true : false) :
                (( $this._sort_defaults[$this._wd.currentTab] && $this._sort_defaults[$this._wd.currentTab].sort === idx ) ? true : false));
    });

    i = 1;
    $.each(this._sort[$this._wd.currentTab], function (idx, val) {
        select2.options[i++] = new Option(
            val,
            idx,
            ($this._sort_defaults[$this._wd.currentTab] && $this._sort_defaults[$this._wd.currentTab].sort === idx) ? true : false,
            ( $this._fullSortPopup._field2Val !== null ) ?
                (( idx === $this._fullSortPopup._field2Val ) ? true : false) : false);
    });

    i = 1;
    $.each(this._sort[$this._wd.currentTab], function (idx, val) {
        select3.options[i++] = new Option(
            val,
            idx,
            ($this._sort_defaults[$this._wd.currentTab] && $this._sort_defaults[$this._wd.currentTab].sort === idx) ? true : false,
            ( $this._fullSortPopup._field3Val !== null ) ?
                (( idx === $this._fullSortPopup._field3Val ) ? true : false) : false);
    });

    if ( this._fullSortPopup._field1Dir === null || this._fullSortPopup._field1Dir === 'asc' ) {
        document.getElementById('_direction_asc_' + this._fullSortPopup._field1ID).checked = true;
    } else {
        document.getElementById('_direction_desc_' + this._fullSortPopup._field1ID).checked = true;
    }
    if ( this._fullSortPopup._field2Dir === null || this._fullSortPopup._field2Dir === 'asc' ) {
        document.getElementById('_direction_asc_' + this._fullSortPopup._field2ID).checked = true;
    } else {
        document.getElementById('_direction_desc_' + this._fullSortPopup._field2ID).checked = true;
    }
    if ( this._fullSortPopup._field3Dir === null || this._fullSortPopup._field3Dir === 'asc' ) {
        document.getElementById('_direction_asc_' + this._fullSortPopup._field3ID).checked = true;
    } else {
        document.getElementById('_direction_desc_' + this._fullSortPopup._field3ID).checked = true;
    }

    $(this._roll.closeBtn()).bind('click', this.cancelHadler);

    this.updateFullSort(select1);
}

FSProjectSortTool.prototype.getSort = function ( position )
{
    switch ( position ) {
        case 1:
            return ( this.fullInUsage() ) ? this._fullSortPopup._field1Val : this.sort();
        case 2:
            return ( this.fullInUsage() ) ? this._fullSortPopup._field2Val : null;
        case 3:
            return ( this.fullInUsage() ) ? this._fullSortPopup._field3Val : null;
        default:
            return null;
    }
}

FSProjectSortTool.prototype.getDirection = function ( position )
{
    switch ( position ) {
        case 1:
            return ( this.fullInUsage() && this._fullSortPopup._field1Val ) ? this._fullSortPopup._field1Dir : this.direction();
        case 2:
            return ( this.fullInUsage() && this._fullSortPopup._field2Val ) ? this._fullSortPopup._field2Dir : null;
        case 3:
            return ( this.fullInUsage() && this._fullSortPopup._field3Val ) ? this._fullSortPopup._field3Dir : null;
        default:
            return null;
    }
}

FSProjectSortTool.prototype.switchSortTools = function( tool )
{
    var oldTool = this.getUsedTool();
    this.setUsedTool(tool);

    if ( oldTool !== this.getUsedTool() ) {
        this.prepareUsedTool();
    }
}

/**
 *  applySort
 *
 *  @param string type quick|full SortTool type
 *  @return FALSE always
 */
FSProjectSortTool.prototype.applySort = function ( type )
{
	FSWidget.filterActivate(false);

    if ( typeof type === 'string' && type.toLowerCase() === 'quick' ) {
        this.switchSortTools('quick');
        this.fullInUsage(false);
        this.update();
        this._wd.show({
            tab     : this._wd.currentTab,
            params  : this._wd.getParams()
        });
    } else if ( typeof type === 'string' && type.toLowerCase() === 'full' ) {
        this.fullInUsage(true);
        this._wd.show({
            tab     : this._wd.currentTab,
            params  : this._wd.getParams()
        });
        this._roll.hide();
    }

    $(this._roll.closeBtn()).unbind('click', this.cancelHadler);

    return false;
}

FSProjectSortTool.prototype.cancelSort = function ()
{
    this._roll.hide();
    if ( !this.fullInUsage() ) {
        this.switchSortTools("quick");
    }

    if ( this._closeBtn !== null ) {
        $(this._closeBtn).remove();
        this._closeBtn = null;
    }

    FSWidget.filterActivate(false);

    $(this._roll.closeBtn()).unbind('click', this.cancelHadler);

    return false;
}

FSProjectSortTool.prototype.updateFullSort = function ( currentSortSelect )
{
    var select1 = document.getElementById('_sort_select_' + this._fullSortPopup._field1ID);
    var select2 = document.getElementById('_sort_select_' + this._fullSortPopup._field2ID);
    var select3 = document.getElementById('_sort_select_' + this._fullSortPopup._field3ID);

    $.each([select1, select2, select3], function (idx, item) {
        for ( var i=1; i<item.options.length; ++i ) {
            item.options[i].disabled = false;
        }
    });

    select2.options[select1.selectedIndex+1].disabled = true;
    select3.options[select1.selectedIndex+1].disabled = true;
    if ( select2.selectedIndex !== 0 ) {
        select1.options[select2.selectedIndex-1].disabled = true;
        select3.options[select2.selectedIndex].disabled = true;
    }
    if ( select3.selectedIndex !== 0 ) {
        select1.options[select3.selectedIndex-1].disabled = true;
        select2.options[select3.selectedIndex].disabled = true;
    }

    var first   = new RegExp(this._fullSortPopup._field1ID+'$');
    var second  = new RegExp(this._fullSortPopup._field2ID+'$');
    var third   = new RegExp(this._fullSortPopup._field3ID+'$');

    if ( first.test(currentSortSelect.id) ) {
        this.setFullSortVal(currentSortSelect.value, 1);
    } else if ( second.test(currentSortSelect.id) ) {
        this.setFullSortVal(currentSortSelect.value, 2);
    } else if ( third.test(currentSortSelect.id) ) {
        this.setFullSortVal(currentSortSelect.value, 3);
    }
}

FSProjectSortTool.prototype.prepareItems = function (version) {
    version = version || 'Full';
    if(version === undefined) return;
}
