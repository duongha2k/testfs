
//-----------------------------------------------------------------------------
/**
 * @fileoverview "Thin" MVC framework for basic code organization.  Primarily
 *	acts as an event-driven model while providing a simple controller for
 *	managing bindings and a view for mapping to existing DOM objects.
 *
 *	Dependencies: jQuery 1.4.2, FSCoreClasses.js
 * 
 * @author Jason K. Flaherty
 */

//-----------------------------------------------------------------------------
/**
 * @class Data model for managing collections of key/value pairs.  Fields are 
 *	stored as instances of Property which triggers change and assign events
 *	when set.  Triggers an update event once when a field is set or multiple 
 *	fields are set simultaneously.  A noUpdate() method is provided for fields
 *	that should not trigger an update event.
 *
 *	Event methods:
 *	onupdate (key, value)
 *	onupdate (object) - returns all contents on update of multiple fields
 * @extends BaseClass
 */
var FSCollectionModel = (function () {
	var _toString = Object.prototype.toString;

	/**
	 * @constructs
	 * @param {...} arguments Any number of field names to initialize
	 */
	var FSCollectionModel = function () {
		var fields = Array.prototype.slice.call(arguments, 0);
		var _data = {};
		var _update_count = 0;
		var _update_lock = false;
		var _change_handler = function () {
			_update_count++;
		};
		var keys;

		//---------------------------------------------------------------------
		/**
		 * Sets the value for a given key or may set multiple fields from a
		 *	given object or assiociative array.  Triggers an update event when
		 *	a value or values have been changed.
		 * @param {string|object} key May be a key to a field or an object
		 *	containing key/value pairs for mass-update
		 * @param {mixed} data New value of field
		 * @return Returns the resulting value of the update.  Returns true
		 *	on successful mass-update and false for invalid arguments.
		 * @type mixed
		 */
		this.val = function (key, data) {
			//Maintain a count of changed fields
			if (!_update_lock) _update_count = 0;

			if (_toString.call (key) == "[object String]") {

				//Update single key/value pair
				if (typeof data != "undefined") {
					//Data provided, so set value of property
					if (typeof _data[key] != "function") {
						//Create new instance of Property if none exists
						_data[key] = new Property (data, this);
						_data[key].listen ("change", _change_handler, this);
					}
					else {
						//Set value of existing property
						data = _data[key] (data);
					}

					//Trigger update event if one exists, we are not in mass
					//update and there were in fact changes made
					if (!_update_lock && typeof this.onupdate == "function" 
							&& _update_count > 0)
						this.onupdate.bind (this) (key, (_data[key])());
				}
				else {
					//No data provided, return the current value
					if (typeof _data[key] != "function") {
						//Property doesn't exist, create it
						_data[key] = new Property (data, this);
						_data[key].listen ("change", _change_handler, this);
					}
					else {
						//Return the current value
						data = _data[key] ();
					}
				}

				return data;
			}
			else if (_toString.call (key) == "[object Object]") {

				//Mass update
				_update_lock = true;

				//Iterate all keys, recursively setting each property
				if (Object.keys) keys = Object.keys (key);
				else keys = IE_Object_keys (key);

				jQuery.each (keys, function (_, index) {
					this.val (index, key[index]);
				}.bind (this));
				_update_lock = false;

				//Trigger update event if one exists and changes were made
				if (typeof this.onupdate == "function" && _update_count > 0)
					this.onupdate.bind (this) (this.toObject());

				return true;
			}

			return false;
		};

		//---------------------------------------------------------------------
		/**
		 * Prevents changes of the given field to trigger update events
		 * @param {string} key Field name
		 */
		this.noUpdate = function (key) {
			if (typeof _data[key] != "undefined") {
				_data[key].halt ("change", 0);
			}
		};

		//---------------------------------------------------------------------
		/**
		 * Returns the actual Property object containing the field.  Used for
		 *	listening on field-level events
		 * @param {string} key Field name
		 * @return See Property class for details
		 * @type Property
		 */
		this.getProperty = function (key) {
			return _data[key];
		};

		//---------------------------------------------------------------------
		/**
		 * Returns an array containing all keys of existing fields
		 * @return Array of field names
		 * @type array
		 */
		this.keys = function () {
			var keys;

			if (Object.keys) keys = Object.keys (_data);
			else keys = IE_Object_keys (_data);

			return keys;
		};

		//---------------------------------------------------------------------
		/**
		 * Returns an array containing the values of all existing fields
		 * @return Array of field values
		 * @type array
		 */
		this.values = function () {
			var result = new Array ();
			var _data_reup = _data;

			jQuery.each (this.keys(), function (_, key) {
				result.push (_data_reup[key]());
			});

			return result;
		};

		//---------------------------------------------------------------------
		/**
		 * Converts entire collection into an object
		 * @return Object containing key/value pairs
		 * @type object
		 */
		this.toObject = function () {
			var result = {};
			var _data_reup = _data;

			jQuery.each (this.keys(), function (_, key) {
				result[key] = _data_reup[key]();
			});

			return result;
		};

		//Shorthand version of getProperty method
		this._ = this.getProperty;

		//Initialize fields for any keys passed as arguments to constructor
		for (var i = 0; i < fields.length; i++) {
			this.val (fields[i]);
		}
	};

	FSCollectionModel.prototype = new BaseClass();

	return FSCollectionModel;
}) ();

//-----------------------------------------------------------------------------
/**
 * @class Simple view class for mapping keys to DOM elements getting/setting
 *	element values/contents.
 * @extends BaseClass
 */
var FSView = (function () {
	var _combine_mappings;

	//The following functions return a formatter function that accepts a value,
	//processes and returns it.  These may be used to process content as it's
	//passed to the view.
	var _formatters = {
		//Default formatter, simply passes back value untouched.
		none: function () {
			return function (value) { return value; };
		},

		//Formats with dollar sign, accepts number of decimal places (default 
		//is 2), a default value if no value is passed and a value in place 
		//of zero.
		money: function (decimal, no_value, zero) {
			decimal = typeof decimal == "number" ? decimal : 2;

			return function (value) {
				if (typeof value == "undefined") return no_value || "";
				if (value == 0 && typeof zero != "undefined") return zero;

				return "$" + value.toFixed (decimal);
			};
		},

		//Like "money" but rounds the value to the nearest dollar.
		money_rounded: function (decimal, no_value, zero) {
			decimal = typeof decimal == "number" ? decimal : 2;

			return function (value) {
				if (typeof value == "undefined") return no_value || "";
				if (value == 0 && typeof zero != "undefined") return zero;

				return "$" + Math.round(value).toFixed (decimal);
			};
		},

		//Appends percent sign to value.  Accepts number of decimal places
		//(default is 0), a value in place of zero and a value in place of
		//100% (1.0)
		percent: function (decimal, zero, full) {
			decimal = typeof decimal == "number" ? decimal : 0;

			return function (value) {
				if (typeof value == "undefined") return zero || "0%";
				if (typeof value != "number") value = parseFloat (value);
				if (value == 0.0 && typeof zero != "undefined") return zero;
				if (value == 1.0 && typeof full != "undefined") return full;

				return (value * 100).toFixed (decimal) + "%";
			}
		},

		//Accepts a key mapping to a select field and converts the given
		//option value to it's corresponding text value.
		select: function (select) {
			return function (value) {
				return jQuery.trim (jQuery (this.getElement (
					select, 
					"option[value='" + value + "']"
				)[0]).text ());
			}
		},

		select_aggregate: function (select) {
			return function (value) {
				var elements = this.getElement (select);
				var result = 0;

				jQuery (elements).each (function () {
					var value = jQuery.trim (jQuery (this).val ());

					value = value != "" ? value : "0.0";

					result += parseFloat (value);
				});

				return result;
			}
		},

		//Accepts a delimiter and a default value if no values are provided.
		//Formatter joins all arguments using the delimiter value
		join: function (delimiter, no_value) {
			return function () {
				var args = Array.prototype.slice.call(arguments, 0);
				if (args[0] === false && typeof no_value != "undefined") return no_value;
				return args.join (delimiter);
			};
		},

		//Accepts multiple formatter functions.  Returns a formatter that calls
		//the first function argument and uses each subsequent function
		//argument to format any arguments passed to the first.
		//See FSSuggestedTechPay.js for an example.
		composite: function (formatter) {
			var arg_format = Array.prototype.slice.call(arguments, 1);

			return function () {
				var args = Array.prototype.slice.call(arguments, 0);

				for (var i = 0; i < args.length; i++) {
					args[i] = arg_format[i].call (this, args[i]);
				}

				return formatter.apply (this, args);
			};
		}
	};

	/**
	 * @constructs
	 * @param {object} mapping Key/value pairs mapping fields to jQuery selectors
	 * @param {object} formatting Key/function pairs for post-processing of content
	 */
	var FSView = function (mapping, formatting) {
		formatting = formatting || {};

		//---------------------------------------------------------------------
		/**
		 * Returns an array of existing keys defined in current view
		 * @return Array of keys
		 * @type array
		 */
		this.getKeys = function () {
			var keys;

			if (Object.keys) keys = Object.keys (mapping);
			else keys = IE_Object_keys (mapping);

			return Object.keys (mapping);
		};

		//---------------------------------------------------------------------
		/**
		 * Returns the element for the given key from jQuery.  An optional
		 *	second argument allows for an extra selector beyond that defined
		 *	in the view mapping.
		 * @param {string} key Description
		 * @param {string} selector Optional. Description
		 * @return An array of elements returned by jQuery
		 * @type array
		 */
		this.getElement = function (key, selector) {
			key = mapping[key];
			if (selector) key = key + " " + selector;
			return jQuery(key);
		};

		//---------------------------------------------------------------------
		/**
		 * Informs the caller if the key for the given element is an input
		 * @param {string} element Key of element to test
		 * @return True if input, select, option, button or textarea
		 * @type boolean
		 */
		this.isInput = function (element) {
			element = this.getElement (element);

			return 	jQuery(element).is ("input") || 
					jQuery(element).is ("select") || 
					jQuery(element).is ("option") || 
					jQuery(element).is ("button") || 
					jQuery(element).is ("textarea");
		};

		//---------------------------------------------------------------------
		this.format = function (type) {
			var args = Array.prototype.slice.call(arguments, 1);

			if (typeof _formatters[type] == "function")
				return _formatters[type].apply (this, args);
			else
				return function (value) { return value; };
		};

		//---------------------------------------------------------------------
		/**
		 * Sets the value or content for the element of a given key if a value
		 *	is provided.  Otherwise, returns the value or content for that
		 *	element.  Auomatically checks for inputs, checkboxes or content.
		 *	No values are returned for content elements such as DIV or SPAN.
		 * @param {string} element Mapping key of element
		 * @param {mixed} value Optional. Populates value or content
		 * @param {mixed} extra Optional. Additional info passed to format function
		 * @return Description
		 * @type mixed
		 */
		this.val = function (element, value, extra) {
			var is_value = typeof value != "undefined";
			var is_input = this.isInput (element);
			var is_checkbox;
			var format, elements, result;

			if (typeof formatting[element] == "function")
				format = formatting[element];
			else
				format = function (value) { return value; };

			element = this.getElement (element);

			if (is_input) {
				is_checkbox = jQuery(element).attr ("type") == "checkbox";

				if (is_checkbox) {
					if (is_value) {
						value = !!value;	//convert to boolean

						if (value)
							jQuery (element).attr ("checked", "checked");
						else
							jQuery (element).removeAttr ("checked");
					}
					else
						value = jQuery(element).is(':checked') ? 1 : 0;
				}
				else {
					if (is_value) jQuery(element).val (format (value, extra));
					else value = jQuery(element).val ();
					/*else {
						elements = jQuery(element);
						result = false;

						elements.each (function () {
							var value = jQuery (this).val ();

							if (!isNaN (parseFloat (value))) {
								if (!result) result = 0.0;
								result += parseFloat (value);
							}
							else {
								if (!result) result = "";
								result += value;
							}
						});
						value = result ? result : "";
					}*/
				}
			}
			else {
				if (is_value) jQuery(element).text (format (value, extra));
			}

			return value;
		};

		//---------------------------------------------------------------------
		/**
		 * Allows child classes to combine or override mappings
		 * @param {object} mappings1 New mappings
		 * @param {object} mappings2 Original mappings
		 * @return Object containing combined mappings
		 * @type object
		 */
		this.combineMappings = _combine_mappings;

		//Clean up formatting table and bind all functions to "this"
		for (var key in formatting) {
			if (typeof formatting[key] == "function")
				formatting[key] = formatting[key].bind (this);
			else
				delete formatting[key];
		}
	};

	_combine_mappings = function (mappings1, mappings2) {
		var mappings = {}

		for (var key in mappings1) { mappings[key] = mappings1[key]; }
		for (var key in mappings2) { mappings[key] = mappings2[key]; }

		return mappings;
	};

	FSView.prototype = new BaseClass ();
	FSView.combineMappings = _combine_mappings;

	return FSView;
}) ();

//-----------------------------------------------------------------------------
/**
 * @class Controller providing bindings of DOM elements to Model
 */
var FSController = (function () {
	/**
	 * @constructs
	 * @param {FSView} view Instance of View class containing DOM bindings
	 */
	var FSController = function (view) {
		//---------------------------------------------------------------------
		/**
		 * Binds a value from a model to an element in a view.  An optional
		 *	trigger element may be provided where one element triggers the
		 *	event but another contains the data.
		 * @param {string} event Event triggered in DOM element
		 * @param {FSModel} model Object containing data
		 * @param {string} key Key or selector of data
		 * @param {string} element View element to be binded
		 * @param {string} trigger Optional. View element triggering the event
		 *	if different than the first element
		 */
		this.bind = function (event, model, key, element, trigger) {
			var property = model._(key);	//Get property object
			var value = view.val (element);	//Get value from view

			//If value already exists in form write to model w/o events
			if (typeof value != "undefined")
				property.quietSet (view.val (element));

			//If no trigger element provided, element is the trigger
			if (typeof trigger == "undefined" || trigger == null) {
				trigger = view.getElement (element);
			}
			else {
				trigger = view.getElement (trigger);
			}

			//Bind only input elements to model
			if (view.isInput (element)) {
				jQuery(trigger).bind (event, function () {
					//Older browsers won't capture upvalues past 1 level
					var element_reup = element;
					var property_reup = property;
					var key_reup = key;

					//Short delay allows DOM to update
					setTimeout (function () {
						value = view.val (element_reup);

						//Write user input to model
						if (typeof property_reup != "undefined") {
							model.val (key_reup, value);

							//Trigger model update event manually since values
							//are set directly and don't trigger automatically
							//model.trigger ("update", key_reup, value);
						}
					}, 500);
				});
			}

			//Bind change event from model to view
			property.listen ("change", function (value, current, ref) {
				view.val (element, value);
				return value;
			}, this);
		}
	};

	FSController.prototype = new BaseClass ();

	return FSController;
}) ();

