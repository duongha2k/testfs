/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

//-------------------------------------------------------------
//-------------------------------------------------------------
global.Instances = null;
//-------------------------------------------------------------
global.CreateObject = function(config) {
    var obj = null;
    if (global.Instances != null) {
        obj = global.Instances[config.id];
    }
    if (obj == null) {
        if (global.Instances == null) {
            global.Instances = new Object();
        }
        obj = new global(config);
        global.Instances[config.id] = obj;
    }
    obj.init();

    return obj;
}
//-------------------------------------------------------------
function global(config) {
    var Me = this;
    this.id = config.id;   
    this.roll = null;
    this.width = ((jQuery(window).width() * 80) / 100) - 10;
    this.height =(jQuery(window).height() * 80) / 100;
    this.PHP_SELF = encodeURIComponent(config.PHP_SELF);
    this.showback = typeof(config.showback) == 'undefined'? 0 : parseInt(config.showback);
    this.data = "";
    this.url = "";
    this.company = config.company;
    this.onSuccess = function(data) {};
    this.loggedInAs = config.loggedInAs;
    this.hasPasswordUpdate = config.hasPasswordUpdate;
    this.userid = config.userid;
    this.password = config.password;
    this.username = config.username;
    this.lname = config.lname;
    this.fname = config.fname;
    //------------------------------------------------------------
    this.LoadingAjax = function(ContentId){
        jQuery(ContentId).html("<center><img src='/widgets/images/wait.gif' border='0' /></center>");
    } 
    //------------------------------------------------------------
    this.ClosePopup=function(){      
        Me.roll.hide();
    }
    //------------------------------------------------------------
    this.showAjax = function(event,url,data,onSuccess){
        if(typeof(event) != 'undefined'){
            if(jQuery(event).parent().find("#loading-button-ajax").length <= 0){
                jQuery(event).after("<img id='loading-button-ajax' src='/widgets/images/button_loading.gif' />")
            }
            jQuery(event).css("display","none");
            jQuery(event).parent().find("#loading-button-ajax").css("display","");
        }
        var el= event;
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success:function( data ) {
                onSuccess(data);
                jQuery(el).parent().find("#loading-button-ajax").hide();
                jQuery(el).css("display","");
            },
            error:function(error) {
                alert('error; ' + eval(error));
            }
        });
    }
    //------------------------------------------------------------
    this.show = function(event){
        Me.showAjax(event,Me.url,Me.data,Me.onSuccess);
    }
    //------------------------------------------------------------
    this.showPopup = function(width,height,title,el){
        var content = "<div id='comtantPopupID'><div style='text-align:center;height:150px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
        var opt = {
            width       : width,
            height      : height,
            position    : 'middle',
            title       : title,
            body        : content
        };
        Me.roll.showNotAjax(el, el, opt);
    }
    //------------------------------------------------------------
    this.showPopupNew = function(parames,id,title,el){
        if(typeof(Me.roll) == 'undefined') Me.roll = new FSPopupRoll();
        var content = "<div id='"+id+"'><div style='text-align:center;height:150px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
        var opt = parames;
        opt.body = content;
        Me.roll.showNotAjax(el, el, opt);
    }
    //------------------------------------------------------------
    this.hideLoader = function()
    {
        $.fancybox.close();
        jQuery("#fancybox-close").unbind("click").click(function(){
            $.fancybox.close();
        });
    }
    //------------------------------------------------------------
    this.showLoaderSaving = function()
    {
        var content = "<div style='width:200px;height:200px;' id='comtantPopupID'><div style='text-align:center;height:200px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Saving...</div></div>";
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :false,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'content': content
        }
        ).trigger('click');
    

    }
    this.showLoaderLoading = function()
    {
        var content = "<div style='width:200px;height:200px;' id='comtantPopupID'><div style='text-align:center;height:200px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :false,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'content': content
        }
        ).trigger('click');
    

    }
    //------------------------------------------------------------
    this.showLoader = function(width,height,showCloseButton)
    {
        var content = "<div style='width:"+width+"px;height:"+height+"px;' id='comtantPopupID'><div style='text-align:center;height:"+height+"px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :showCloseButton,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'content': content
        }
        ).trigger('click');
        

    }
    this._redirect = false;
    //------------------------------------------------------------
    this.showMessages = function(data) {
        var html = '';
        Me.roll = new FSPopupRoll();
        if (Me._redirect && data.success) { //always refresh page on success
                    
        }
        if (data.success) {
            html += '<div class="success"><ul><li>Success!</li></ul></div>';
        }else{
            html+='<div class="errors"><ul>';
            for (i=0;i<data.errors.length;i++) {
                if(typeof data.errors[i] === 'object')
                    html+='<li>'+data.errors[i].message+'</li>';
                else
                    html+='<li>'+data.errors[i]+'</li>';
            }
            html+='</ul></div>';
        }
        html+='<div class="tCenter"><input type="button" class="link_button_popup" onclick="_global.roll.hide();" value="Ok" /></div>';
        Me.roll.autohide(false);
        var opt = {
            width       : 400,
            height      : '',
            position    : 'middle',
            title       : '',
            body        : html
        };
        if (data.success) {
            opt.width = 300;
        }
        return Me.roll.showNotAjax(null, null, opt);
    }
    //------------------------------------------------------------
    this.eventOpenProfileTech_click = function(){
        var rel = jQuery(this).attr("href");

        if (window == window.top) {
            rel += "&simple=1&back=0&popup=1";
            $("<div></div>").fancybox(
            {
                'autoDimensions' : true,
                'width' : 1015,
                "height": "90%",
                'transitionIn': 'none',
                'transitionOut' : 'none',
                'type': 'iframe',
                'href': rel
            }
            ).trigger('click');
        }
        else {
                if(Me.showback){
                    rel += "&back=1";
                }else{
                    rel += "&back=0";
                }
                rel += "&simple=1&popup=1&prev="+encodeURIComponent(window.location);
                parent.$("<div></div>").fancybox({
                        "autoDimensions": false,
                        "width": 1015,
                        "height": "90%",
                        "transitionIn": "none",
                        "transitionOut": "none",
                        "type": "iframe",
                        "href": rel
                }).trigger("click");
        }
        
        return false;
    }
    //------------------------------------------------------------
    this.eventOpenProfileTech = function(){
        jQuery(".cmdOpenTechProfileShow").unbind("click");
        jQuery(".cmdOpenTechProfileShow").unbind("click",Me.eventOpenProfileTech_click)
        .bind("click",Me.eventOpenProfileTech_click);
    }
    //------------------------------------------------------------
    this.accessControlClient = function(access_id,before,success,restricted){
        if(typeof(before) == 'function'){
            before();
        }
        $.post("/widgets/dashboard/client/accessctl/", {
            rt:'iframe',
            attributeId:access_id
        }, 
        function (response) {
            if(response=="1") {
                if(typeof(success) == 'function'){
                    success();
                }
            } 
            else {
                if(typeof(restricted) == 'function'){
                    restricted();
                }
                Attention(response);
            }                        
        });   
    }
    //------------------------------------------------------------
    this.buildEventAccessControl = function(object,access_id,before,success){
        jQuery(object).unbind("click");
        jQuery(object).attr("onclick","");
        jQuery(object).click(function(){
            Me.accessControlClient(access_id,before,success);
        });
    }
    //------------------------------------------------------------
    this.buildEvent = function(){
        
    }
    //------------------------------------------------------------
    this.buildEventNotices = function(){
        jQuery(".MyNoticesSettings")
        .unbind('click',Me.MyNoticesSettings1_click)
        .bind('click',Me.MyNoticesSettings1_click);  
        jQuery(".MyNoticesTechProfile")
        .unbind('click',Me.MyNoticesTechProfile_click)
        .bind('click',Me.MyNoticesTechProfile_click); 
        jQuery(".MyNoticesBid")
        .unbind('click',Me.MyNoticesBid_click)
        .bind('click',Me.MyNoticesBid_click); 
        jQuery("#MyNoticesSeeMore")
        .unbind('click',Me.MyNoticesSeeMore_click)
        .bind('click',Me.MyNoticesSeeMore_click);
        jQuery(".MyNoticesQandA")
        .unbind('click',Me.MyNoticesQandA_click)
        .bind('click',Me.MyNoticesQandA_click);
    }
    this.MyNoticesQandA_click = function(){
        Me.clearTimeoutNotices();
        var win = jQuery(this).attr("win");
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :true,
            'width':Me.width,
            'height':Me.height,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'type':"iframe",
            'href':'/clients/clientquestionandanswer.php?simple=1&notice=1&win='+win+'&backNotices='+Me.backNotices+"&backclick="+Base64.encode('window.parent._global.MyNoticesViewAllNoticesBack();')
        }
        ).trigger('click');
    }
    this.MyNoticesBid_click = function(){
        Me.clearTimeoutNotices();
        var win = jQuery(this).attr("win");
        $("<div></div>").fancybox(
        {
            'autoDimensions' : false,
            'showCloseButton' :true,
            'width':Me.width,
            'height':Me.height,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'type':"iframe",
            'href':'/clients/wosApplicant.php?v='+Me.company+'&sb=0&simple=1&ClientID=1713&WorkOrderID='+win+'&backNotices='+Me.backNotices+"&backclick="+Base64.encode('window.parent._global.MyNoticesViewAllNoticesBack();')
        }
        ).trigger('click');
    }
    this.MyNoticesTechProfile_click = function(){
        Me.clearTimeoutNotices();
        var techid = jQuery(this).attr("techid");
        $("<div></div>").fancybox(
        {
            'autoDimensions' : false,
            'showCloseButton' :true,
            'width':Me.width,
            'height':Me.height,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'type':"iframe",
            'href':'/clients/wosViewTechProfile.php?simple=1&back=0&popup=1&TechID='+techid+'&v='+_company+'&backNotices='+Me.backNotices+"&backclick="+Base64.encode('window.parent._global.MyNoticesViewAllNoticesBack();')
        }
        ).trigger('click');
    }
    //------------------------------------------------------------
    this.loadingNotices = false;
    this.backNotices = 0;
    this.mainLinkNotices_click = function(){
        Me.backNotices = 0;
        jQuery("#MyNoticesMenu").css("left",(jQuery(window).width() - jQuery("#MyNoticesMenu").width() - 30) + "px");
        
        jQuery("#MyNoticesMenu")
        .unbind('mouseover',Me.MyNotices_mouseover)
        .bind('mouseover',Me.MyNotices_mouseover);
//        if(jQuery.trim(jQuery("#MyNoticesMenu").html()) != "" || Me.loadingNotices){
//            return false;
//        }
        jQuery("#MyNoticesMenu").html("<div style='text-align:center;height:150px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div>");
        jQuery("#MyNoticesMenu").css("visibility","visible");
        var _onSuccess = function(data){
            jQuery("#MyNoticesMenu").html(data);
            Me.buildEventNotices();  
            jQuery("#MyNoticesViewAllNotices")
                .unbind('click',Me.MyNoticesViewAllNotices_click)
                .bind('click',Me.MyNoticesViewAllNotices_click);
            jQuery('body').unbind('click',Me.mainLinkNotices_blur)
            .bind('click',Me.mainLinkNotices_blur);
            jQuery("#mainLinkNotices").html("Inbox ");
        }
        Me.data = "menu=1";
        Me.url = "/widgets/dashboard/client/my-notices";
        Me.onSuccess = _onSuccess;
        Me.loadingNotices = true;
        Me.show();
    }
    this.MyNoticesSettings1_click = function(){
        Me.clearTimeoutNotices();
        // call My Setting
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :true,
            'width':Me.width,
            'height':Me.height,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'type':"iframe",
            'href':'/clients/profile.php?v='+Me.company+'&tabid=clientMySettings&simple=1&notice=1&backNotices='+Me.backNotices+"&backclick="+Base64.encode('window.parent._global.MyNoticesViewAllNoticesBack();')
        }
        ).trigger('click');
    }
    //------------------------------------------------------------
    this.autoResize = function(width,height){
        if(width != null){
           $('#fancybox-content').width(width); 
           $('#fancybox-wrap').width(width+20); 
        }
        if(height != null){
           $('#fancybox-content').height(height); 
           $('#fancybox-wrap').height(height+20); 
        }
        $.fancybox.resize();
    }
    //------------------------------------------------------------
    this.pageNotices = 1;
    this.hiddenHtmlNotices = "";
    this.MyNoticesViewAllNoticesBack = function(){
        Me.showLoader(Me.width,Me.height,true);
        jQuery("#comtantPopupID").html(Me.hiddenHtmlNotices);
        jQuery("#comtantPopupID #contentMynotices0").height(Me.height - 60);
        Me.buildEventNotices();
        jQuery("#MyNoticesSeeMore").css("display","");
        jQuery("#MyNoticesSeeMore").parent().find("#loading-button-ajax").css("display","none");
    }
    this.MyNoticesViewAllNotices_click = function(){
        Me.backNotices = 1;
        Me.pageNotices = 1;
        jQuery("#MyNoticesMenu").css("visibility","hidden");
        Me.data = "pageNotices="+Me.pageNotices+"&menu=0";
        Me.url = "/widgets/dashboard/client/my-notices";
        var _onSuccess = function(data){
            jQuery("#comtantPopupID").html(data);
            jQuery("#comtantPopupID #contentMynotices0").height(Me.height - 60);
            Me.buildEventNotices();
            Me.hiddenHtmlNotices = data;
            jQuery("#mainLinkNotices").html("Inbox ");
        }
        Me.onSuccess = _onSuccess;
        Me.showLoader(Me.width,Me.height,true);
        Me.show();
    }
    this.MyNoticesSeeMore_click = function(){
        Me.backNotices = 1;
        Me.pageNotices++;
        Me.data = "pageNotices="+Me.pageNotices+"&menu=0&ajax=1";
        Me.url = "/widgets/dashboard/client/my-notices";
        var _onSuccess = function(data){
            jQuery("#contentMynoticesTable0125 tbody").append(data);
            Me.buildEventNotices();
            Me.hiddenHtmlNotices = jQuery("#comtantPopupID").html();
            jQuery("#mainLinkNotices").html("Inbox ");
        }
        Me.onSuccess = _onSuccess;
        Me.show(jQuery(this));
    }
    //------------------------------------------------------------
    this.clearTimeoutNotices = function(){
      jQuery("#MyNoticesMenu")
        .unbind('mouseout',Me.mainLinkNotices_mouseout); 
      jQuery('body').unbind('click',Me.mainLinkNotices_blur);
    }
    this.MyNotices_mouseover = function(){
        Me.clearTimeoutNotices();
        jQuery("#MyNoticesMenu")
        .unbind('mouseout',Me.mainLinkNotices_mouseout)
        .bind('mouseout',Me.mainLinkNotices_mouseout);
    }
    this.timeoutNotices = null;
    this.mainLinkNotices_blur = function(){
            jQuery("#MyNoticesMenu").css("visibility","hidden");
        jQuery("#MyNoticesMenu")
            .unbind('mouseout',Me.mainLinkNotices_mouseout); 
        jQuery('body').unbind('click',Me.mainLinkNotices_blur);
    }
    this.mainLinkNotices_mouseout = function(){

        jQuery('body').unbind('click',Me.mainLinkNotices_blur)
        .bind('click',Me.mainLinkNotices_blur);
    }
    //------------------------------------------------------------
	this.onIdle = function() {
		try {
			if (_global.mainLinkNoticeTimer) clearTimeout(_global.mainLinkNoticeTimer);
		} catch (e) {}
	}
	
	this.onActive = function() {
		_global.getCountNoticeActual();
		_global.setGetCountNoticeTimer();
	}
	
	var mainLinkNoticeTimer = null;
	var getCountNoticeInterval = 300000;
	this.getCountNoticeActual = function(){
			if ($.data(document,'idleTimer') == 'idle') return;
			_global.data = "";
			_global.url = "/widgets/dashboard/client/getcountnoticesnotviewyet";
			var _onSuccess = function(data){
				jQuery("#mainLinkNotices").html(data.success);
				_global.getCountNotices_NotViewYet();
			}
			_global.onSuccess = _onSuccess;
			_global.show(jQuery(this));
	}
	this.setGetCountNoticeTimer = function() {
		try {
			if (_global.mainLinkNoticeTimer) clearTimeout(_global.mainLinkNoticeTimer);
		} catch (e) {}
		if ($.data(document,'idleTimer') == 'idle') return;
		_global.mainLinkNoticeTimer = setTimeout(_global.getCountNoticeActual, getCountNoticeInterval);
	}
	
    this.getCountNotices_NotViewYet = function(){
        if(jQuery("#mainLinkNotices").length > 0){
			if (!$( document ).data("idleTimer")) {
				try {
					$.idleTimer(getCountNoticeInterval);
					$(document).bind("idle.idleTimer", _global.onIdle);
					$(document).bind("active.idleTimer", _global.onActive);
				} catch (e) { 
					// idle timer not ready
					setTimeout(_global.getCountNotices_NotViewYet, 10000);
					return;
				}
			}
			this.setGetCountNoticeTimer();
        }
    }
    //------------------------------------------------------------
    this.perNotices = function(){
        jQuery("#mainLinkNotices")
        .unbind('click',Me.mainLinkNotices_click)
        .bind('click',Me.mainLinkNotices_click);
        Me.getCountNotices_NotViewYet();
    }
    //------------------------------------------------------------
    this.validPasword = function(username,password,comfrimps,fname,lname,oldpassword){
        fname = fname.toLowerCase();
        lname = lname.toLowerCase();
        username = username.toLowerCase();
        var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/;
        var error = []
        error['error'] = 0;
        error['id'] = [];
        error['msg'] = [];
        var i = 0;
        if(password == ""){
            error['error'] = 1;
            error['id'][i] = 8;
            error['msg'][i] = "Please input password";
            i++;
        }
        if(typeof(oldpassword) != 'undefined' && oldpassword == password){
            error['error'] = 1;
            error['id'][i] = 1;
            error['msg'][i] = "Must be different than your existing password";
            i++;
        }
        if(password.length < 8 || password.length > 15){
            error['error'] = 1;
            error['id'][i] = 3;
            error['msg'][i] = "Password must have 8-15 characters";
            i++;
        }
        if(!re.test(password)){
            error['error'] = 1;
            error['id'][i] = 4;
            error['msg'][i] = "Password must contain 1 lowercase letter, 1 uppercase letter, and 1 number";
            i++;
        }
        if(password.toLowerCase().indexOf("password") >= 0 ){
            error['error'] = 1;
            error['id'][i] = 5;
            error['msg'][i] = "Must not contain the text: 'password'";
            i++;
        }
        if((password.toLowerCase().indexOf(fname) >= 0 && jQuery.trim(fname) != "")  || (password.toLowerCase().indexOf(lname) >= 0 && jQuery.trim(lname) != "")){
            error['error'] = 1;
            error['id'][i] = 6;
            error['msg'][i] = "Password must not contain your first or last name";
            i++;
        }
        if(password.toLowerCase().indexOf(username) >= 0 && jQuery.trim(username) != ""){
            error['error'] = 1;
            error['id'][i] = 7;
            error['msg'][i] = "Password must not contain your username";
            i++;
        }
        if(typeof(clientOrTech) != "undefined")
        {

        if(clientOrTech === "client" && password != comfrimps){
        	//console.log("HERE");
            error['error'] = 1;
            error['id'][i] = 2;
            error['msg'][i] = "Passwords must match";
            i++;
        }
        }
        return error;
    }
    //------------------------------------------------------------
    this.cmdUpdatePasswordSecurity = function(){
        if(!Me.checkPasswordGui()){
            return false;
        }
        var _data = jQuery("#frmupdatepasswordsecurity").serialize();
        Me.data = _data+"&loggedInAs="+Me.loggedInAs+"&userid="+Me.userid;
        Me.url = "/widgets/dashboard/popup/do-update-password-security";
        var _onSuccess = function(data){
            Me.hideLoader();
        }
        Me.onSuccess = _onSuccess;
        Me.show(this);
    }
    this.checkPasswordGui = function(){
        var password = jQuery("#frmupdatepasswordsecurity #password").val();
        var comfirmps = jQuery("#frmupdatepasswordsecurity #Confirmpassword").val();
        jQuery("#frmupdatepasswordsecurity #divError").html("");
        jQuery("#frmupdatepasswordsecurity #password").css("border","1px solid black");
        jQuery("#frmupdatepasswordsecurity #Confirmpassword").css("border","1px solid black");
        var errors = Me.validPasword(Me.username,password,comfirmps,Me.fname,Me.lname,Me.password);
        if(errors['error'] == 1){
            jQuery("#frmupdatepasswordsecurity #divError").html(errors["msg"][0]);
            if(errors['id'][0] == 2){
                jQuery("#frmupdatepasswordsecurity #Confirmpassword").css("border","2px solid red");
                //jQuery("#frmupdatepasswordsecurity #Confirmpassword").focus();
            }else{
                jQuery("#frmupdatepasswordsecurity #password").css("border","2px solid red");
                //jQuery("#frmupdatepasswordsecurity #password").focus();
            }
            return false;
        }
        return true;
        }
    //------------------------------------------------------------
    this.callUpdatePassWordSecurity = function(){
        if(Me.hasPasswordUpdate == 0){
            return false;
        }
        Me.data = "loggedInAs="+Me.loggedInAs;
        Me.url = "/widgets/dashboard/popup/update-password-security";
        var _onSuccess = function(data){
            jQuery("#comtantPopupID").html(data);
            jQuery("#comtantPopupID").width('auto');
            jQuery("#comtantPopupID").height('auto');
            jQuery("#frmupdatepasswordsecurity #cmdSubmitDPS").unbind('click',Me.cmdUpdatePasswordSecurity)
                .bind('click',Me.cmdUpdatePasswordSecurity);
            jQuery("#frmupdatepasswordsecurity #password").unbind('blur',Me.checkPasswordGui)
                .bind('blur',Me.checkPasswordGui);
            jQuery("#frmupdatepasswordsecurity #Confirmpassword").unbind('blur',Me.checkPasswordGui)
                .bind('blur',Me.checkPasswordGui);    
        }
        Me.onSuccess = _onSuccess;
        Me.showLoader(450,310,false);
        Me.show();
    }
    //------------------------------------------------------------
    this.init = function() {
        if(typeof(FSPopupRoll) != 'undefined'){
            Me.roll = new FSPopupRoll();
        }
        Me.perNotices();
    }
    //------------------------------------------------------------
    this.autoResize = function(width,height,exW,exH){
        if(width != null){
           $('#fancybox-content').width(width); 
           $('#fancybox-wrap').width(width+20); 
        }
        if(height != null){
            if(typeof(exH) == 'undefined') exH = 20;
            $('#fancybox-content').height(height+exH); 
            $('#fancybox-wrap').height(height+exH); 
        }
        $.fancybox.resize();
    }
}
