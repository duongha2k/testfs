/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var DetailRateTool = {
    imgChose:'<div sizeimg="" idcbx="IDCBX" valuecbx="VALUECBX" class="classClickRateFS" hover="true" style="cursor:pointer;position: relative;width:24px;height:26px;float: left;"><img style="position: relative;z-index: 0;" src="/widgets/images/ico_havex23.png" style="cursor:pointer;" /><div style="position: absolute;z-index: 1;top:4px;[PADDINGLEFT]">HTML</div></div>',
    imgNotChose:'<div sizeimg="" idcbx="IDCBX" valuecbx="VALUECBX"  class="classClickRateFS" hover="false" style="cursor:pointer;position: relative;width:24px;height:26px;float: left;"><img style="position: relative;z-index: 0;" src="/widgets/images/ico_nonex23.png" style="cursor:pointer;" /><div style="position: absolute;z-index: 1;top:4px;[PADDINGLEFT]">HTML</div></div>',
    imgRemove:'<img class="imageRemove classClickRemoveRateFS" src="/widgets/images/ico_closex9.png" style="cursor:pointer;padding-right:2px;" />',
    divReset:'<a href="javascript:;" class="classClickResetRateFS" style="cursor:pointer;padding-left:2px;" >(reset)</a>',
    classSelect : '.selfRatingTool',
    cbxselectSkill : "#cbxselectSkill",
    ContentID : '',
    selectZero : "0",
    AfterClickChange:function() {
        
    },
    SetParam: function(param)
    {
        var el = this;
        if(typeof(param) != 'undefined')
        {
            if(typeof(param.ContentID) != 'undefined') el.ContentID = param.ContentID;
            
            if(typeof(param._class) != 'undefined'){
                el.classSelect = (el.ContentID == ""?"":el.ContentID + " ") + param._class;
            }
            else
            {
                el.classSelect = (el.ContentID == ""?"":el.ContentID + " ") + el.classSelect;
            }   
            if(typeof(param._cbxselectSkill) != 'undefined'){
                el.cbxselectSkill = (el.ContentID == ""?"":el.ContentID + " ") + param._cbxselectSkill;
            }
            else
            {
                el.cbxselectSkill = (el.ContentID == ""?"":el.ContentID + " ") + el.cbxselectSkill;
            }
        }    
    },
    Onit: function(param)
    {
        var el = this;
        el.SetParam(param);
        el.OnitBuildRating();
        el.configSelectOne();
    },
    OnitBuildRating: function()
    {
        var el = this;
        jQuery(el.classSelect).each(function(){
            var idCbx = jQuery(this).attr("id");
            var labelEl = jQuery(this).parent().children('label[for='+idCbx+']');
            var labelElLable = jQuery(this).parent().children('label[for='+idCbx+'Lable]');
            if(jQuery(this).parent().children(".imageRemove").length <= 0)
            {    
                jQuery(labelElLable).before(el.imgRemove);
            }
            if(jQuery(this).parent().children(".classClickResetRateFS").length <= 0)
            {    
                jQuery(labelElLable).html(jQuery(labelElLable).html()+el.divReset);
            }
            var html = "<div class='div_"+idCbx+"'>";
            var callSelect = $(this)[0];
            var selected = "";
            var HasSelect = false;
            for (var i=0; i< callSelect.options.length; ++i ) {
                var sel = jQuery(callSelect.options[i]).html();
                var _value = callSelect.options[i].value;
                if ( callSelect.options[i].selected) {
                    selected = callSelect.options[i].value;
                }
                var paddingleft = "left:8px;";
                if(sel >=10)
                {
                    paddingleft = "left:5px;";
                }    
                if(i > 0)
                {
                    if(selected == el.selectZero)
                    {
                        html += el.imgNotChose.replace('HTML',sel)
                        .replace('IDCBX', idCbx)
                        .replace('[PADDINGLEFT]', paddingleft)
                        .replace('VALUECBX', _value);
                    }
                    else
                    {    
                        if(HasSelect)
                        {
                            html += el.imgNotChose.replace('HTML',sel)
                            .replace('IDCBX', idCbx)
                            .replace('[PADDINGLEFT]', paddingleft)
                            .replace('VALUECBX', _value);
                        }   
                        else
                        {
                            html += el.imgChose.replace('HTML',sel)
                            .replace('IDCBX', idCbx)
                            .replace('[PADDINGLEFT]', paddingleft)
                            .replace('VALUECBX', _value);
                        }  
                        if(selected != "")
                        {
                            HasSelect = true;
                        } 
                    }    
                }    
            }
            html += '</div><div style="clear:both;"></div>';
            jQuery(labelEl).after(html);
            jQuery(this).hide();
            if(HasSelect)
            {
                jQuery(this).parent().show();
                // remove combox
                var idli = jQuery(this).parent().attr("id");
                if(jQuery(el.cbxselectSkill).length > 0)
                {
                    jQuery(el.cbxselectSkill).children("option[value='#"+idli+"']").remove();
                }    
            }
        });
        jQuery(".classClickRateFS").click(function(){
            el.OnchangeSlider(this,el); 
        });
        //735
        jQuery(".classClickFSexpert").click(function(){
            el.OnExpertchange(this,el); 
        });
        
    },

    OnExpertchange : function(event,el)
    {
        var TechSelfRatingColumn = jQuery(event).attr('TechSelfRatingColumn');
        var ischeck = $("#feexpert"+TechSelfRatingColumn).is(":checked");
        
        if(ischeck)
        {
            jQuery(event).children('img').attr('src','/widgets/images/ico_nonex23.png'); 
            jQuery(event).children('div').html("x");
            jQuery(event).children('div').css("color","black");
            $("#feexpert"+TechSelfRatingColumn).removeAttr("checked");
        }
        else
        {
            jQuery(event).children('img').attr('src','/widgets/images/ico_havex23.png'); 
            jQuery(event).children('div').html("x");
            jQuery(event).children('div').css("color","white");
            $("#feexpert"+TechSelfRatingColumn).attr("checked","checked");
        }
        
        if(typeof(el.AfterClickChange) == 'function')
        {
            el.AfterClickChange();
        }    
    }, //end 735

    OnchangeSlider : function(event,el)
    {
        var idCbx = jQuery(event).attr('idcbx');
        var valueCbx = jQuery(event).attr('valuecbx');
        var stateSelect = $("#"+idCbx)[0];
        for (var i=0; i < stateSelect.options.length; ++i ) {
            if ( stateSelect.options[i].value == valueCbx ) {
                stateSelect.options[i].selected = true;
                break;
            }
        }
        var isHave = true;
        jQuery(event).parent().children('.classClickRateFS').each(function(){
            if(isHave)
            {    
                jQuery(this).children('img').attr('src','/widgets/images/ico_havex23.png'); 
            }
            else
            {
                jQuery(this).children('img').attr('src','/widgets/images/ico_nonex23.png'); 
            }  
            if(jQuery(this).attr('valuecbx') == valueCbx)
            {
                isHave = false;
            }    
        });
        
        if(typeof(el.AfterClickChange) == 'function')
        {
            el.AfterClickChange();
        }    
    },
    OnitBuildEventChooseSkill:function()
    {
        var el = this;
        jQuery(el.cbxselectSkill).unbind('change',el.OnchangeChooseSkill)
        .bind('change',el.OnchangeChooseSkill); 
        jQuery(".classClickRemoveRateFS").click(function(){
            el.OnRemoveSliderSkill(this,el,true); 
        });   
        jQuery(".classClickResetRateFS").click(function(){
            el.OnResetSliderSkill(this,el,true); 
        });
    },
    OnchangeChooseSkill: function()
    {
        var callSelect = $(this)[0];
        var selected = "";
        var val = "";
        jQuery(this).children('option').each(function(){
            if(jQuery(this).is(':selected'))
            {
                selected = jQuery(this).val();
                val = jQuery(this).html();
                jQuery(this).remove();
                jQuery(window).focus();
                return false;
            }    
        });
        if(selected != "")
        {
            jQuery(selected).show();
            jQuery(selected).attr("optionvalue",selected);
            jQuery(selected).attr("optionhtml",val);
        }    
        DetailRateTool.configSelectOne();
    },OnResetSliderSkill:function(event,el,isRunAfterClickChange)
    {      
        event = jQuery(event).parent();
        el.setSelectSkillNull(event,el);
        if(typeof(el.AfterClickChange) == 'function' && isRunAfterClickChange)
        {
            el.AfterClickChange();
        }
    },
    OnRemoveSliderSkill:function(event,el,isRunAfterClickChange)
    {
        jQuery(el.cbxselectSkill).append('<option sort="'+jQuery(event).parent().attr('optionhtml')+'" value="'
            +jQuery(event).parent().attr('optionvalue')
            +'">'+jQuery(event).parent().attr('optionhtml')+'</option>');
        jQuery(event).parent().hide();
        
        jQuery(el.cbxselectSkill+">option")
        .tsort({
            order:'asc',
            attr:'sort'
        });
        
		filterActive = false;
		$(event).parent().find(".selfRatingTool").each(function() {
			if ($(this).val() != 0)
				filterActive = true;
		});
        el.setSelectSkillNull(event,el);
        el.configSelectOne();
        if(typeof(el.AfterClickChange) == 'function' && isRunAfterClickChange && filterActive)
        {
            el.AfterClickChange();
        }
    },
    setSelectSkillNull: function(event,el)
    {
        jQuery(event).parent().children('select').each(function() {
            var stateSelect = jQuery(this)[0];
            for (var i=0; i < stateSelect.options.length; ++i ) {
                if ( stateSelect.options[i].value == el.selectZero ) {
                    stateSelect.options[i].selected = true;
                    break;
                }
            }
        });
        jQuery(event).parent().find('.classClickRateFS').each(function(){

            jQuery(this).children('img').attr('src','/widgets/images/ico_nonex23.png'); 
        });
        //735
        jQuery(event).parent().find("input").removeAttr("checked");
//        jQuery(event).parent().find('.classClickFSexpert').each(function(){
//            var TechSelfRatingColumn = jQuery(this).attr('TechSelfRatingColumn');
//            jQuery(this).children('img').attr('src','/widgets/images/ico_nonex23.png'); 
//            jQuery(this).children('div').html("");
//            $("#feexpert"+TechSelfRatingColumn).removeAttr("checked");
//            
//        });
        //end 735

    },
    clearAll: function()
    {
        var el = this;
        var classClickRemoveRateFS = (el.ContentID == ""?"":el.ContentID + " ") + ".classClickRemoveRateFS";
        jQuery(classClickRemoveRateFS).each(function(){
            if(jQuery(this).parent().css('display') != 'none')
            {
                jQuery(el.cbxselectSkill).append('<option sort="'+jQuery(this).parent().attr('optionhtml')
                    +'" value="'
                    +jQuery(this).parent().attr('optionvalue')
                    +'">'+jQuery(this).parent().attr('optionhtml')+'</option>');
                jQuery(this).parent().hide();
                el.setSelectSkillNull(this,el);
            }
        });
        jQuery(el.cbxselectSkill+">option")
        .tsort({
            order:'asc',
            attr:'sort'
        });
    },
    setValue: function(cbx,val)
    {
        var el = this;
        var isHave = false;
        var stateSelect = $("#"+cbx)[0];
        for (var i=0; i < stateSelect.options.length; ++i ) {
            if ( stateSelect.options[i].value == val ) {
                stateSelect.options[i].selected = true;
                isHave = true;
                break;
            }
        }
        if(!isHave) return false;
        jQuery(el.ContentID+" #div_"+cbx).find(".classClickRateFS").each(function(){
            if(isHave)
            {    
                jQuery(this).children('img').attr('src','/widgets/images/ico_havex23.png'); 
            }
            else
            {
                jQuery(this).children('img').attr('src','/widgets/images/ico_nonex23.png'); 
            }  
            if(jQuery(this).attr('valuecbx') == val)
            {
                isHave = false;
            }  
        });
    },configSelectOne : function()
    {
        var lengthNone = 0;
        jQuery("#srskills").find("li[class!='cbxSkill']").each(function(){
           if(jQuery(this).css("display") == "none")
           {
               lengthNone++;
           }    
        });
        if(lengthNone ==
            jQuery("#srskills").find("li[class!='cbxSkill']").length || jQuery("#srskills").find("li[class!='cbxSkill']").length <= 0)
        {
            jQuery(this.cbxselectSkill).find('option[value=""]').html("Select A Skill Category");
        }    
        else
        {
            jQuery(this.cbxselectSkill).find('option[value=""]').html("Select Another Skill Category");
        }    
    }
}


