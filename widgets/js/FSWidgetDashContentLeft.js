/**
 * FSWidgetDashContentLeft
 */
function FSWidgetDashContentLeft ( params ) {
    FSWidget.call(this, params);

    this.waitImage = new Image();
    this.waitImage.src = "/widgets/images/progress.gif";     //  pre load wait image
    this.waitImage.width = 190;
    this.waitImage.height = 14;

    this.params.baseUrl = document.location.protocol + '//' + document.location.hostname + '/widgets/dashboard/sections/dash-content-left';
    this.params.container = params.container;
	
    this.objMain = params.objMain;
    this.checkMinWidth = params.checkMinWidth;
    this.dr = params.dr;

    if (!this.checkMinWidth) this.checkMinWidth = function() {};
    try {
	this.detailsWidget = detailsWidget;
    } catch (e) {
	detailsWidget = {};
	detailsWidget.show = this.openCreateWorkOrder;
    }
}

FSWidgetDashContentLeft.prototype = new FSWidget();
FSWidgetDashContentLeft.prototype.constructor = FSWidgetDashContentLeft;

FSWidgetDashContentLeft.NAME          = 'FSWidgetDashContentLeft';
FSWidgetDashContentLeft.VERSION       = '0.1';
FSWidgetDashContentLeft.DESCRIPTION   = 'Class FSWidgetDashContentLeft';

FSWidgetDashContentLeft.prototype.showPreloader = function() {
    $('body').css('cursor', 'wait');
    var img = "<div class='tCenter' ><img style='margin:10% auto;' alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></div>";
    FSWidget.fillContainer(img, this);
}

FSWidgetDashContentLeft.prototype.show = function() {
    
    this.showPreloader();
    data = this.params.data || {};

    if ( this.tabOpenXhrRequest ) {
        this.tabOpenXhrRequest.abort();
        this.tabOpenXhrRequest = null;
    }

    this.tabOpenXhrRequest = jQuery.ajax({
        url : this.params.baseUrl,
        data : data,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/clients/logIn.php";
            } else {
                FSWidget.fillContainer("Sorry, but an error occurred. No results found.", this);
            }
            this.tabOpenXhrRequest = null;
        },
        success : function(data, status, xhr) {
            $('body').css('cursor', '');
            FSWidget.fillContainer(data, this)
        }
    });
}

FSWidgetDashContentLeft.prototype.showSidebar = function (){
	$("#separatorLeftOutside").hide();
	$("#separatorLeftInside").show();
	this.objMain.addClass('use-sidebar-left');
	this.objMain.removeClass('sidebar-at-left');
	$("#dashcentercontent").attr("id", "dashcentercontent");
	$.cookie('sidebar-pref2', 'use-sidebar-left', { expires: 30 });
};

FSWidgetDashContentLeft.prototype.hideSidebar = function (){
	if(objMain.hasClass('use-sidebar-left')){
		$("#separatorLeftOutside").show();
		$("#separatorLeftInside").hide();
		objMain.removeClass('use-sidebar-left');
		objMain.addClass('sidebar-at-left');
		$.cookie('sidebar-pref2', null, { expires: 30 });
	}
};

FSWidgetDashContentLeft.prototype.separatorClick = function(e){
	if ( this.objMain.hasClass('use-sidebar-left') ){
		this.hideSidebar();
	}
	else {
		this.showSidebar();
	}
	this.checkMinWidth();
};

FSWidgetDashContentLeft.prototype.createAssignBtnClick = function(){
	dr.hideSidebar();
	detailsWidget.show({tab:'create',params:{company:window._company,container:'detailsContainer',tab:"new", project:$('#Project_ID option:selected').val(), techID:$('#techID').val() }});
	checkMinWidth();
};

FSWidgetDashContentLeft.prototype.findAssignBtnClick = function(){
//	dr.hideSidebar();
	findWorkOrder.doFind('1');
//	checkMinWidth();
};

FSWidgetDashContentLeft.prototype.openCreateWorkOrder = function(params) {
	params = params.params;
	document.location = '/clients/createWO.php?v=' + params.company + '&project_id=' + params.project + '&techID=' + params.techID;
	return false;
}
