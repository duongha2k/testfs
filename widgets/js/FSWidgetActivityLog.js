function FSWidgetActivityLog(roll, win, woid)
{
    if ( ! roll instanceof FSPopupRoll ) {
        throw new Error('roll should be the object instance of FSPopupRoll');
    }
    win     = win  || null;
    woid    = woid || null;

    var rand            = Math.floor(Math.random()*10001);
    this._roll          = roll;
    this._win           = win;
    this._woid          = woid;
    this._html          = null;
    this._logID         = '_active_log_' + rand;
    this._winFieldId    = '_active_log_win_' + rand;
    this._woidFieldId   = '_active_log_woid_' + rand;
    this._url           = '/widgets/dashboard/popup/activity-log/'

    this._loadedWin     = null;
    this._loadedWoid    = null;
}

/**
 *  logContainerId
 *
 *  return string ID of Activity Log Container
 */
FSWidgetActivityLog.prototype.logContainerId = function ()
{
    return this._logID;
}

/**
 *  winFieldId
 *
 *  @return string ID of WIN# Field
 */
FSWidgetActivityLog.prototype.winFieldId = function ()
{
    return this._winFieldId;
}

/**
 *  woidFieldId
 *
 *  @return string ID of WO ID Field
 */
FSWidgetActivityLog.prototype.woidFieldId = function ()
{
    return this._woidFieldId;
}

FSWidgetActivityLog.prototype.doRequest = function ()
{
    if ( this._win || this._woid ) {

        $('#' + this.logContainerId()).html('<center><img src="'+ __image__.src +'" title="Please wait..." /></center>');

        var params = {company : window._company};
        if ( this._win ) {
            params.win = this._win;
        } else if ( this._woid ) {
            params.woid = this._woid;
        }

        $.ajax({
            url         : this._url,
            data        : params,
            dataType    : 'json',
            cache       : false,
            type        : 'post',
            context     : this,
            success     : function (data, status, xhr) {
                $('#' + this.winFieldId()).val(data.win);
                $('#' + this.woidFieldId()).val(data.woid);
                $('#' + this.logContainerId()).html(data.html);

                this._loadedWin     = data.win;
                this._loadedWoid    = data.woid;
            },
            error       : function (xhr, status, error) {
                if ( xhr.responseText === "authentication required" ) {
                    document.location = "/clients/logIn.php";
                } else {
                    $('#' + this.logContainerId()).html('<br /><span>Sorry, but error occurred</span><br />');
                    this._loadedWin     = null;
                    this._loadedWoid    = null;
                }
            }
        });
    }
}

/**
 *  initActivityLogTool
 *
 *  Roll display Handler
 *
 *  @return void
 */
FSWidgetActivityLog.prototype.initActivityLogTool = function ()
{
    $('#' + this.logContainerId()).css('maxHeight', (this._roll.height() - 70) + 'px');
    $('#' + this.logContainerId()).css('overflowY', 'auto');

    this._loadedWin     = null;
    this._loadedWoid    = null;

    var __this = this;
    $('#' + this.winFieldId()).blur(function (event) {
        __this._woid = null;
        __this._win  = this.value;
        if ( __this._loadedWin !== __this._win ) {
            __this.doRequest();
        }
    });
    $('#' + this.woidFieldId()).blur(function (event) {
        __this._woid = this.value;
        __this._win  = null;
        if ( __this._loadedWoid !== __this._woid ) {
            __this.doRequest();
        }
    });
    
    //VALIDATORS
	var vWIN = new LiveValidation(this.winFieldId(),LVDefaults);
	vWIN.add( Validate.Numericality, { onlyInteger: true } );
}

/**
 *  buildHtml
 *
 *  @return string HTML for Roll pop-up
 */
FSWidgetActivityLog.prototype.buildHtml = function ()
{
    if ( this._html === null ) {
        var html;
        html  = "<table class='' width='100%' cellspacing='0' cellpadding='0'>";

        html += "<tr><td>";
        html += "<strong>WIN#</strong> <input size='7' type='text' name='"+this.winFieldId()+"' id='"+this.winFieldId()+"' value='' />";
        html += " or ";
        html += "<strong>Client ID:</strong> <input size='35' type='text' name='"+this.woidFieldId()+"' id='"+this.woidFieldId()+"' value='' />";
        html += "</td></tr>";

        html += "<tr><td>";
        html += "<div id='" + this.logContainerId() + "' class='activity_log'><br /></div>";
        html += "</td></tr>";

        html += "<tr><td align='center'>";
        html += "<input type='button' class='link_button_popup' value='Done' onclick='roll.hide()' />";
        html += "</td></tr>";

        html += "</table>";

        this._html = html;
    }

    return this._html;
}

FSWidgetActivityLog.prototype.autoLoad = function() {
    this._win  = arguments[1].params.win;
    this.doRequest();
}
