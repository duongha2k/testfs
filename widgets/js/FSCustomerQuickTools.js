
function FSCustomerQuickTools (params) {
	FSWidgetDashboard.call (this, params);

	if (typeof params != "undefined") {
		this.setParams (params);
		this.show ();
	}
}

FSCustomerQuickTools.prototype				= new FSWidgetDashboard ();
FSCustomerQuickTools.prototype.constructor	= FSCustomerQuickTools;
FSCustomerQuickTools.NAME					= 'FSCustomerQuickTools';
FSCustomerQuickTools.VERSION				= '0.1';
FSCustomerQuickTools.DESCRIPTION			= 'Class FSCustomerQuickTools';


FSCustomerQuickTools.prototype.setParams = function (params) {
	this.currentTab		= params.tab;

	this._container		= params.container;
    this._company		= params.company;
	this._roll			= params.roll;
	this._project		= params.project;
	this._cert			= params.cert;
	this.params			= params;
};

FSCustomerQuickTools.prototype.show = function (params) {
	jQuery.ajax({
		url			: "/widgets/dashboard/customer/quick-tools/company/" + this._company + "/project/" + this._project,
		type		: 'POST',
		data		: params,
		dataType	: 'html',
		context		: this,
		error		: function (data, status, error) {
			//console.log (data, status, error);
		},
		success		: function(data, status, error) {
			FSWidget.fillContainer(data, this);
		}
	});
};

FSCustomerQuickTools.prototype.approve = function (site, tech_id) {
	jQuery.ajax({
		url : "/widgets/dashboard/customer/quick-tools-approve/company/" + this._company + 
			"/project/" + this._project + "/site/" + escape(site) + "/tech/" + tech_id,
		type : 'POST',
		dataType : 'json',
		context : this,
		error : function (data, status, error) {
			//console.log (data, status, error);
		},
		success : function(data, status, error) {
			if (data.success == "1") {
				this.show ();
			}
			else {
				//console.log (data, status, error);
			}
		}
	});
};

FSCustomerQuickTools.prototype.sort = function (event) {
	var opt = {
		width   : 325,
		height  : 225,
		title   : "Sort Techs...",
		url		: "/widgets/dashboard/popup/filter-quickticket/company/" + this._company + "/project/" + this._project,
		context : this.sortTool(),
		handler : this.sortTool().initSortPopup,
		body    : this.sortTool().makeFullSortPopup()
	};

	this._roll.showAjax (event.target, event, opt);
};

FSCustomerQuickTools.prototype.resetFilters = function (roll) {
	this.filters().city (null);
	this.filters().contractorId ("");
	this.filters().lastName ("");
	this.filters().siteInfo1 (null);
	this.filters().siteInfo2 (null);
	this.filters().displayContractors (null);

	FSWidgetDashboard.prototype.resetFilters.call (this, roll);
};

FSCustomerQuickTools.prototype.getParams = function (withPage) {
	var params = FSWidgetDashboard.prototype.getParams.call (this, roll);

	params["siteinfo1"] = this.filters().siteInfo1 ();
	params["siteinfo2"] = this.filters().siteInfo2 ();
	params["displaycontractors"] = this.filters().displayContractors ();
	params["contractor_id"] = this.filters().contractorId ();
	params["lastname"] = this.filters().lastName ();

	return params;
};

FSCustomerQuickTools.prototype.applyFilters = function (roll) {
	var element;

    this.filters().city($($('#' + roll.container().id + ' #filterCity')[0]).val());
    this.filters().contractorId($($('#' + roll.container().id + ' #filtercontractor_id')[0]).val());
    this.filters().lastName($($('#' + roll.container().id + ' #filterLastName')[0]).val());

	element = $('#' + roll.container().id + ' #filterSiteInfo1')[0];
	if (typeof element != "undefined") {
		for (var i = 0; i < element.options.length; ++i ) {
			if (element.options[i].selected) {
				this.filters().siteInfo1 ($(element.options[i]).val());
				break;
			}
		}
	}

	element = $('#' + roll.container().id + ' #filterSiteInfo2')[0];
	if (typeof element != "undefined") {
		for (var i = 0; i < element.options.length; ++i ) {
			if (element.options[i].selected) {
				this.filters().siteInfo2 ($(element.options[i]).val());
				break;
			}
		}
	}

	element = $('#' + roll.container().id + ' #filterDisplayContractors')[0];
	if (typeof element != "undefined") {
		for (var i = 0; i < element.options.length; ++i ) {
			if (element.options[i].selected) {
				this.filters().displayContractors ($(element.options[i]).val());
				break;
			}
		}
	}

	FSWidgetDashboard.prototype.applyFilters.call (this, roll);
};

FSCustomerQuickTools.prototype.prepareFilters = function (roll) {
	var element;

    $($('#' + roll.container().id + ' #filterCity')[0]).val (this.filters().city());
    $($('#' + roll.container().id + ' #filtercontractor_id')[0]).val (this.filters().contractorId());
    $($('#' + roll.container().id + ' #filterLastName')[0]).val (this.filters().lastName());

    var element = $('#' + roll.container().id + ' #filterSiteInfo1')[0];
	if (typeof element != "undefined") {
		for (var i = 0; i < element.options.length; ++i ) {
			if ( element.options[i].value == this.filters().siteInfo1() ) {
				element.options[i].selected = true;
			if (this.filters().siteInfo1() != 'none')
					this.showFilterByName('sectionSiteInfo1');
				break;
			}
		}
	}

    var element = $('#' + roll.container().id + ' #filterSiteInfo2')[0];
	if (typeof element != "undefined") {
		for (var i = 0; i < element.options.length; ++i ) {
			if ( element.options[i].value == this.filters().siteInfo2() ) {
				element.options[i].selected = true;
			if (this.filters().siteInfo2() != 'none')
					this.showFilterByName('sectionSiteInfo2');
				break;
			}
		}
	}

    var element = $('#' + roll.container().id + ' #filterDisplayContractors')[0];
	if (typeof element != "undefined") {
		for (var i = 0; i < element.options.length; ++i ) {
			if ( element.options[i].value == this.filters().displayContractors() ) {
				element.options[i].selected = true;
			if (this.filters().displayContractors() != 'none')
					this.showFilterByName('sectionDisplayContractors');
				break;
			}
		}
	}

	FSWidgetDashboard.prototype.prepareFilters.call (this, roll);
};

FSCustomerQuickTools.prototype.editContractor = function (element, tech_id, contractor_id, name, site) {
	var last_state = false;
	var state_select;
	var roll = this._roll;
	var container;

	roll.autohide (false);
	roll.showNotAjax (element, null, {
		width		: 400,
		height		: 300,
		title		: 'Edit Contractor Information',
		context		: this,
		body		: jQuery("#editContent").html()
	});

	container = jQuery(roll.container());
	state_select = container.find ("#statesSelect");
	state_select.change (function () {
		var value = state_select.val();
		var element_id = "#state_" + value;

		container.find (element_id).show();
		if (last_state !== false) container.find (last_state).hide();

		last_state = element_id;
	});

	container.find ("#editTechID").val (tech_id);
	container.find ("#editContractorID").val (contractor_id);
	container.find ("#editContractorName").text (name);
};

FSCustomerQuickTools.prototype.updateContractor = function (data) {
	var url = "/widgets/dashboard/customer/quick-tools-update-contractor/company/" + _company + "/project/" + _project;

	jQuery.ajax({
		url			: url,
		type		: 'POST',
		dataType	: 'json',
		data		: data,
		context		: this,
		error		: function (data, status, error) {
			//console.log ("ERROR", data, status, error);
		},
		success		: function (data, status, error) {
			this._roll.hide();
			this.show();
		}
	});
};

FSCustomerQuickTools.loadSites = function (select, container) {
	var url = "/widgets/dashboard/customer/quick-tools-get-sites/company/" + _company + "/project/" + _project;

	jQuery.ajax({
		url			: url,
		type		: 'GET',
		dataType	: 'json',
		context		: this,
		error		: function (data, status, error) {
			//console.log (data, status, error);
		},
		success		: function (data, status, error) {
			var state_select = typeof select == "string" ? jQuery("#" + select) : jQuery(select);
			var site_select = typeof container == "string" ? jQuery("#" + container) : jQuery(container);
			var last_state = false;
			var element;

			if (data.success == 1) {
				jQuery.each (data.sites, function () {
					if (this.State != last_state) {
						state_select.append ($("<option />").val (this.State).text (this.StateFull));
						last_state = this.State;

						element = jQuery (document.createElement ("div"))
							.attr ("id", "state_" + last_state).hide();
						element.css ("font-size: 10px; line-height: 16px;");
						site_select.append (element);
					}

					element.append ($('<input type="radio" name="SiteNumber" />').val (this.SiteNumber));
					element.append (" " + this.Address + ", " + this.City + ", " + this.State + " " + this.Zipcode);
					element.append ($("<br />"));
				});

				last_state = false;

				state_select.change (function (event) {
					var class_name = ".state_" + state_select.val();

					jQuery(class_name).show();
					if (last_state !== false) jQuery(last_state).hide();

					last_state = class_name;
				});
			}
			else {
				//console.log (data, status, error);
			}
		}
	});
};

FSCustomerQuickTools.prototype.loadSites = FSCustomerQuickTools.loadSites;

FSCustomerQuickTools.prototype.showFilterByName = function (val) {
	//override and do nothing
};



