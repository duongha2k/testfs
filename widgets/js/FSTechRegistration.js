 $(document).ready(function(){ 	
    function validPassword(){
        var username = $('#username').val();
        var password = $('#password').val();
        var comfrimps = $('#passwordconfirm').val();
        var fname = $('#FName').val();
        var lname = $('#LName').val();
        var err = _global.validPasword(username,password,comfrimps,fname,lname);

        jQuery.validator.addMethod("password", function( value, element ) {
            if($('body').data('password') == $("#password").val()){
                return true;
            }else{
                if(err["error"] && err['id'][0] != 2){    
                    return false;
                }
                return true;
            }
        }, err["msg"][0]);
        
        jQuery.validator.addMethod("passwordconfirm", function( value, element ) {
            if($('body').data('passwordconfirm') == $("#passwordconfirm").val()){
                return true;
            }else{
                if(err["error"] && err['id'][0] == 2){    
                    return false;
                }
                return true;
            }
        }, err["msg"][0]);
    }
    $("#password, #passwordconfirm").change(function(e){
        validPassword();
    }); 
    
    //14057
    $('#SMS_Number').keyup(function(e){
      if(e.keyCode == 13){
            var proVal = $("#CellProvider").val().length;
            if(proVal===0){
             $("#srvProvider").removeAttr("style");   
             $("#srvProvider").html('<span id="reqStar">*</span><span>Service Provider:</span>');
             $("#CellProvider").addClass("required invalid");
        }
      }else{
          var smsVal = $("#SMS_Number").val().length; 
          if(smsVal===0){
            $('#CellProvider option:first-child').attr("selected", "selected");
            $("#srvProvider").html('<span>Service Provider:</span>');
            $("#CellProvider").removeClass("required invalid");
            $("#srvProvider").css("font-weight","normal");  
          }else{
             $("#srvProvider").removeAttr("style");   
             $("#srvProvider").html('<span id="reqStar">*</span><span>Service Provider:</span>');
             $("#CellProvider").addClass("required invalid");
          }
      } 
    });
   //14057
    
      
	jQuery.validator.addMethod("phoneTen", function( val, element ) {
		if ($("#country").val() != 'US' && $("#country").val() != 'CA') return true;
		if (val != null && val != "") {
			// massage phone number
			validFormat = /^[(]?(\d{3})[)]?(?:\s|-|.)*(\d{3})[-\s.]*(\d{4})$/;
			parts = val.match(validFormat);
			if (parts != null)
				$(element).val(parts[1] + "-" + parts[2] + "-" + parts[3]);
			else return false;
		}     
		return true; 
	}, "Phone number must be ###-###-####, or change selected country");

	jQuery.validator.addMethod("phoneMX", function( val, element ) {
		if ($("#country").val() != 'MX') return true;
		if (val != null && val != "") {
			// massage phone number
			validFormat = /^\+52 (\d{3} \d{3} \d{4}|\d{2} \d{4} \d{4})$/;
			parts = val.match(validFormat);
			if (parts == null)
				return false;
		}
		return true;
	}, "Phone number must be +52 ## #### #### or +52 ### ### ####, or change selected country");
        
        jQuery.validator.addMethod("phoneBS", function( val, element ) {
		if ($("#country").val() != 'BS') return true;
		if (val != null && val != "") {
			// massage phone number
			validFormat = /^\+44 (\d{2})[)]?(?:\s|-|.)*(\d{4})[-\s.]*(\d{5})$/;
			parts = val.match(validFormat);
			if (parts == null)
				return false;
		}
		return true;
	}, "Phone number must be +44 ## #### ####, or change selected country");
        
	jQuery.validator.addMethod("password", function( value, element ) {
		return true;
	}, "Password must be at least 8 characters and contain one letter and one number.");

        jQuery.validator.addMethod("mustAgree", function(value, element) {
		if ($("input[name=termsOfUse]:checked", "#techRegistration").val() != "Yes"){
			return false;
		}else{
			return true;
		}

	}, "You must agree to the Terms Of Use before registering");
	
	jQuery.validator.addMethod("iacAgree", function(value, element) {
		if ($("input[name=IAC]:checked", "#techRegistration").val() != "1"){
			return false;
		}else{
			return true;
		}

	}, "You must agree to the Independent Contractor Agreement before registering");
	
	jQuery.validator.addMethod("privacyAgree", function(value, element) {
		if ($("input[name=privacyStatement]:checked", "#techRegistration").val() != "1"){
			return false;
		}else{
			return true;
		}

	}, "You must agree to the Privacy Statement before registering");
	
	jQuery.validator.addMethod("initialCheck", function( value, element ) {
		var firstLastInit = $("#FirstName").val().substring(0,1).toLowerCase() + $("#LastName").val().substring(0,1).toLowerCase();
		if(firstLastInit != $("#DigitalSignature").val().toLowerCase()){
			return false;
		}else{
			return true;
		}
	},"Initials do not match the First and Last Name entered. Please reenter.");
	
	jQuery.validator.addMethod("usernameCheck", function(value, element) {
		if ($("#username").hasClass('invalid') == true){
			return false;
		}else{
			return true;
		}

	}, "");
	
	jQuery.validator.messages.required = "";
	
	$("#techRegistration").validate({
		invalidHandler: function(e, validator) {
                        $.ajax({
				type: "POST",
				url: "/widgets/dashboard/publish/country-require-zip",
                                dataType    : 'json',
                                cache       : false,
                                data: 'countrycode='+jQuery("#country").children('option:selected').val(),
                                success: function (data) {
                                    if(!data.success)
                                    {
                                        jQuery("#zip").removeClass("required"); 
                                        jQuery("#zip").removeClass("invalid"); 
                                    }
                                    else
                                    {
                                        jQuery("#zip").addClass("required");
                                        if( jQuery("#zip").val()  == "")jQuery("#zip").addClass("invalid"); 
                                    }
			var errors = validator.numberOfInvalids();
		
			if (errors) {
				var message = errors == 1
					? 'You missed 1 field. It has been highlighted below'
					: 'You missed ' + errors + ' fields.  They have been highlighted below';
				$("div.error span").html(message);
				$("div.error").show();
			} else {
				$("div.error").hide();
			}
                                }
			});
                        validPassword();
			
		},
		onkeyup: false,
		submitHandler: function(form) {
			$("div.error").hide();
			ajaxRequest(form);
		},
		rules: {
			PrimaryPhone: {
				phoneTen: true,
//				phoneMX: true,
//                                phoneBS: true,
				remote: {
					url: "/widgets/dashboard/do/tech-phone-not-taken",
					type: "post",
					data: {}
				}
			},
			SecondaryPhone: {
				phoneTen: true//,
//				phoneMX: true,
//                                phoneBS:true,
				/*remote: {
					url: "/widgets/dashboard/do/tech-phone-not-taken",
					type: "post",
					data: {}
				}*/
			},
			SMS_Number: {
				phoneTen: true
//				phoneMX: true,
//                                phoneBS:true
			},
			captcha:{
				required: true,
				remote: "/ajax/process.php"
			},
			/*primaryEmail: {
				email: true
			},*/
			confPrimaryEmail: {
				email: true,
				equalTo: "#primaryEmail"
			},
			secondaryEmail: {
				email: true
			},
			password: {
				password: true
			},
			passwordconfirm: {
				passwordconfirm: true
			},
			termsOfUse:{
				mustAgree: true
			},
			IAC:{
				iacAgree: true
			},
			privacyStatement:{
				privacyAgree: true
			},
			DigitalSignature:{
				initialCheck: true
			},
			username:{
				required:true,
				usernameCheck: true
			}
		},
		messages: {
			captcha: "Correct captcha is required. Click the captcha to generate a new one",
			primaryEmail: {
				email: "Please enter a valid email address"//,
				//remote: jQuery.validator.format("{0} is already taken, please enter a different address.")	
			},
			confPrimaryEmail:{
				equalTo: "Please confirm that the address you entered is the same as above"
			},
			privacyStatement: "You must agree to the Privacy Statement before registering",
			termsOfUse: {
				mustAgree: "You must agree to the Terms Of Use before registering"
			},
			IAC: "You must agree to the Independent Contractor Agreement before registering",
			FTC: "Please choose an option for your Federal Tax Classification",
			PrimaryPhone: {
				remote: "The phone number is already taken, please enter a different phone number."
			}/*,
			SecondaryPhone: {
				remote: "The phone number is already taken, please enter a different phone number."
			}*/			
		},
		errorPlacement: function(error,element){
			if($(element).attr("id") == "DigitalSignature"){
				error.appendTo(element.parent().parent().parent().next().find("div.status"));
			}else{
				error.appendTo(element.parent().parent().next().find("div.status"));
			}
		},
		errorClass: "invalid",
		debug:true
	});

  function ajaxRequest(form){
	  	var submitData = $(form).serialize();
	  	var w9Complete = true;
	  	var w9CompleteArr =  ["FirstName","LastName", "Address1", "City", "State", "ZipCode", "DigitalSignature"];   
	  	$("input.W9").each(function(){
	  		if($.inArray($(this).attr("name"),w9CompleteArr) > -1){
	  			if($(this).val() == null || $(this).val() == ""){
	  				w9Complete = false;
	  			}
	  		}
	  	});
	  	
	  	if($("#SSN1").val() == "" && $("#EIN").val() == ""){
	  		w9Complete = false;
	  	}
	  	
	  	if($("input[name=FTC]:checked").val() == ""){
	  		w9Complete = false;
	  	}

	  		showLoader();
	  		
			$.ajax({
				type: "POST",
				url: "/ajax/techRegister.php",
		        dataType    : 'json',
		        cache       : false,
				data: submitData,
				success: function (data, status, error) {
					//console.log ("success", data, status, error);
					
					var onClosed = "";
					if(data.success == "success"){
						var messageHtml = "<h2>Registration Complete</h2>";
						var redirect = "/";
						
						if ($("body").data("iso_id")) {
							messageHtml += "<p>Thank you for registering as an Independent Service Organization Technician. Please log on to www.FieldSolutions.com and fill out your Technician Profile, being sure to add information on your work experience, industry certifications, areas of expertise, tools, etc.</p>";
							messageHtml += "<p>Because clients base their decisions to assign work orders on information contained within your Profile, a detailed Profile is crucial to your success with FieldSolutions. If you are not part of an Independent Service Organization, please contact support at support@fieldsolutions.com or 952-288-2500 Option 1.</p>";
						}
						else {
							messageHtml += "<p>A confirmation was sent to: <b>"+data['email']+"</b> which you should receive shortly. ";
							messageHtml += "After you acknowledge that notification, you can go to www.FieldSolutions.com as a registered technician and view all of our open work orders and postings.</p>";
							messageHtml += "<p>Your Confirmation email will provide a link to bring you back to your profile so you can complete the remaining sections.";
							messageHtml += "These remaining sections are crucial because 1) they will help you win more work orders and 2) they inform us how you want your payments received.</p>";
						
							if(w9Complete == false){
								messageHtml += "<p style='font-weight:bold; color:red;'>Please note that your W9 info is not complete. You can enter the remaining info on your profile page.</p>";
							}
						}
						
						messageHtml += "<b>Thank you!</b>";

						if (jQuery.cookie ("Tech_Source") != null)
							redirect = "http://www.fieldsolutions.com";

						$("<div></div>").fancybox({
							'width': 550,
							'height' : 300,
							'content' : messageHtml,
							'autoDimensions' : false,
							'centerOnScroll' : true,
							'onClosed' : function(){document.location = redirect;}
						}).trigger('click');
						
					}else if(data.error == "email exists"){
						var messageHtml = "<h2 style='color: #FF0000;'>Registration Error</h2>";
						messageHtml += "<p>There is Tech registered with Field Solutions with the same email address.</p> ";
						
						$("<div></div>").fancybox({
							'width': 550,
							'height' : 300,
							'content' : messageHtml,
							'autoDimensions' : false,
							'centerOnScroll' : true
					}).trigger('click');
					}
					hideLoader();
				},
				error: function (data, status, error) {
					//console.log ("error", data, status, error);
					hideLoader();
				}
			});
  }
  
  var loaderVisible = false;
  function showLoader(){
	  if(!loaderVisible){
		  $("div#loader").fadeIn("fast");
		  loaderVisible = true;
	  }
  }
  
  function hideLoader(){
	  if(loaderVisible){
		  var loader = $("div#loader");
		  loader.stop();
		  loader.fadeOut("fast");
		  loaderVisible = false;
	  }
  }
 
  $(".resize").vjustify();
  $("div.buttonSubmit").hoverClass("buttonSubmitHover");
  
  if ($.browser.safari) {
    $("body").addClass("safari");
  }
  
  //$("input.primaryPhone").mask("(999) 999-9999");
 // $("input.zipcode").mask("99999");
  


  // toggle optional billing address
  var subTableDiv = $("div.subTableDiv");
  var toggleCheck = $("input.toggleCheck");
  toggleCheck.is(":checked")
  	? subTableDiv.hide()
	: subTableDiv.show();
  $("input.toggleCheck").click(function() {
      if (this.checked == true) {
        subTableDiv.slideUp("medium");
        $("form").valid();
      } else {
        subTableDiv.slideDown("medium");
      }
  });


});

$.fn.vjustify = function() {
    var maxHeight=0;
    $(".resize").css("height","auto");
    this.each(function(){
        if (this.offsetHeight > maxHeight) {
          maxHeight = this.offsetHeight;
        }
    });
    this.each(function(){
        $(this).height(maxHeight);
        if (this.offsetHeight > maxHeight) {
            $(this).height((maxHeight-(this.offsetHeight-maxHeight)));
        }
    });
};

$.fn.hoverClass = function(classname) {
	return this.hover(function() {
		$(this).addClass(classname);
	}, function() {
		$(this).removeClass(classname);
	});
};
