function FSFindWorkOrder( wd, roll ) {
	this._wd = wd;
	this._roll = roll;
	this._html = '';
	this._container = null;
}

FSFindWorkOrder.prototype.container = function() {
	if (this._container == null) {
		var container = document.createElement('div');
        container.style.display = 'none';
        $('body')[0].appendChild(container);
        this._container = container;
	}
	return this._container;
}

FSFindWorkOrder.prototype.buildHtml = function () {
	var _url = this._wd._makeUrl('findworkorder', '/company/' + this._wd.filters().company());

    jQuery.ajax({
        url : _url,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        success : function(data, status, xhr) {
            $(this.container()).html(data);
        },
        error : function (xhr, status, error) {
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/clients/logIn.php";
            }
        }
    });
};

FSFindWorkOrder.prototype.doFind = function(isSideBarFlag) {
    //036
    var win = "";
    var clientWOrd = "";
    var project = $("#fWOrdProject").val();
    var techId = "";
    var isDash = $("#isDash").val();
    var zipCode = "";//13938
	var WO_ID_Mode = 0;

    if(isSideBarFlag != null && isSideBarFlag == '1'){
            win = $("#wfWOrdWIN").val();
            clientWOrd = $("#wfWOrdClientWO").val();
            techId = $("#wfWOrdTechId").val();
            zipCode = $("#fWOrdZipCode").val();//13938
			WO_ID_Mode = $("#wfWOrdWO_ID_Mode").val();
    }
    else
    {
        win = $("#fWOrdWIN").val();
        clientWOrd = $("#fWOrdClientWO").val();
        techId = $("#fWOrdTechId").val();
        zipCode = $("#fWOrdZipCode").val();//13938    
		WO_ID_Mode = $("#fWOrdWO_ID_Mode").val();
    }

    var wd = this._wd;
    var responseHandler = function(data)
    {		
        if (data.error == 0)
        {
            if(data.redirect=='wodetails')
            {
                //14100
                document.location = '/clients/wosDetails.php?v=' + wd.filters().company() + '&id=' + escape(data.wins) + '&techID=' + techId + '&zipCode=' + zipCode;
                //end 14100
            }
            else
            {
                if (wd.filters().container() == null) {
                    wd.filters().win(data.wins);
                    var p = wd.getParams();
                    p.tab = 'all';
                    FSFindWorkOrder.prototype.showOnWos({tab:'all',params:p});
                }
                else
                {
                    var aw = data.wins;
                    wd.filters().win(aw);
                    wd.show({tab: 'all', params:wd.getParams()}); 
                }
            }
        }
        else
        {
            $("#fWOrdMessage").text('Work order not found');
            setTimeout(function () { $("#fWOrdMessage").text(''); }, 5000);
        }
    };

    if (win.length > 0 || clientWOrd.length > 0)
    {
        if(typeof(isDash) == "undefined" || (isSideBarFlag != null && isSideBarFlag == '1'))
        {
            if (win.length > 0)
            {
                $.ajax({
                    url: "/widgets/dashboard/popup/get-wo-info/",
                    type: 'POST',
                    data: {
                        company: this._wd.filters().company(),
                        WIN_NUM: win
                    },
                    success: responseHandler,
                    dataType: 'json',
                    context: this
                });
            }
            else 
            {
                $.ajax({
                    url: "/widgets/dashboard/popup/get-wo-info/",
                    type: 'POST',
                    data: {
                        company: this._wd.filters().company(),
                        WO_ID: clientWOrd,
						WO_ID_Mode: WO_ID_Mode
                    },
                    success: responseHandler,
                    dataType: 'json',
                    context: this
                });
            }
        }
        else
        {
            if (win.length > 0)
            {
                $.ajax({
                    url: "/widgets/dashboard/popup/get-wo-info/",
                    type: 'POST',
                    data: {
                        company: this._wd.filters().company(),
                        WIN_NUM: win
                    },
                    success: responseHandler,
                    dataType: 'json',
                    context: this
                });
            }
            else if( clientWOrd.length > 0)
            {
                 $.ajax({
                    url: "/widgets/dashboard/popup/get-wo-info/",
                    type: 'POST',
                    data: {
                        company: this._wd.filters().company(),
                        WO_ID: clientWOrd,
						WO_ID_Mode: WO_ID_Mode						
                    },
                    success: responseHandler,
                    dataType: 'json',
                    context: this
                });
            }
            else 
            {
                wd.resetFilters();
                if (project != '0') wd.filters().setProject([project]);
                if (techId.length > 0) wd.filters().tech(techId);
                if (zipCode.length > 0) wd.filters().zip(zipCode);//13938
                if (clientWOrd.length > 0) wd.filters().clientWO(clientWOrd);
                if (WO_ID_Mode.length > 0) wd.filters().WO_ID_Mode(WO_ID_Mode);
                if (wd.filters().container() == null) {
                        var p = wd.getParams();
                        p.tab = 'all';
                        this.showOnWos({tab:'all',params:p});
                } else {
                        wd.show({tab: 'all', params:wd.getParams()});

                }
                this._roll.hide();
            }
        }
    }
    else if ( project != '0' || techId.length > 0 || zipCode.length > 0)
    {
        wd.resetFilters();
        if (project != '0') wd.filters().setProject([project]);
        if (techId.length > 0) wd.filters().tech(techId);
        if (zipCode.length > 0) wd.filters().zip(zipCode);//13938
        if (wd.filters().container() == null) {
                var p = wd.getParams();
                p.tab = 'all';
                this.showOnWos({tab:'all',params:p});
        } else {
                wd.show({tab: 'all', params:wd.getParams()});

        }
        this._roll.hide();
    }
    //end 036
}

FSFindWorkOrder.prototype.prepare = function() {
	$("#fWOrdMessage").html('');
}



FSFindWorkOrder.prototype.showOnWos = function(options) {
    var p = options || {};
    var queryString = '';
    if ( p.params !== undefined && p.params !== null && typeof p.params === 'object' ) {
        var key;
        
        for ( key in p.params ) {
            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                }
                //036
                else
                {
                    if(queryString != '') queryString += '&';
                    queryString += key + '=' + p.params[key];
                }
                //end 036
            } else if ( p.params[key] === true || p.params[key] === false ) {
                if ( p.params[key] === false ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] !== undefined && p.params[key] !== null && p.params[key].toLowerCase() !== 'none' ) {
                if(queryString != '') queryString += '&';
                queryString += key + '=' + p.params[key];
            } else {
                delete p.params[key];
            }
        }
    } else p.params = {};
    try {
    	var company = p.params["company"] ? p.params["company"] : "default";
    	if ($('#PMCompany').length > 0) {
    		company = $('#PMCompany').val();
    	}
    	document.location = '/clients/wos.php?v=' + company + '&' +queryString;
    } catch ( e ) {
        return false;
    }
}
