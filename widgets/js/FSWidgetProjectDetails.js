/**
 * FSWidgetWODetails
 *
 * @param Object|null params
 * @access public
 * @return void
 */

function FSWidgetProjectDetails ( params, roll ) {
    FSWidget.call(this, params);

    this.params.POPUP.avaliablePopups = {
        updateOptions   : {module:'dashboard',controller:'sections',action:'update-options'},
        quickassigntool : {module:'dashboard',controller:'project',action:'quickassigntool'},
        filedownload    : {module:'dashboard',controller:'do',action:'file-project-documents-download'},
        documents       : {module:'dashboard',controller:'project',action:'project-documents'}
    };
    this.params.WIDGET.avaliableTabs = {
        update : {module:'dashboard',controller:'project',action:'update'},
        create : {module:'dashboard',controller:'project',action:'create'}
    };
    this._option = 0;

    this._roll = roll;

    this.uploadFields = ['Project_Docs','Project_Other_Files', 'Project_Logo'];
    this.waitImage = new Image();
    this.waitImage.src = "/widgets/images/progress.gif";     //  pre load wait image
    this.waitImage.width = 190;
    this.waitImage.height = 14;

    this._activeupload = 0;
    this._uploadsCounter = 0;
    this._updatestatus = {success:1, errors:[]};
    this._redirect = false;
    this._reftab = params.reftab;

    this._customfunctions = params.customfunctions || {};
    
    this.formType = params.tab;
    this.fromEmail = null;
    this.allowSubmit = this.formType == "create";
    this.windowSettings = "menubar=0, width=400, height=400, location=0, toolbar=0";
    this.oldReminderAll = false;
    this.oldSMSBlast = false;
    this.oldReminderAcceptance = false;
    this.oldReminder24Hr = false;
    this.oldReminder1Hr = false;
    this.oldCheckInCall = false;
    this.oldCheckOutCall = false;
    this.oldReminderNotMarkComplete = false;
    this.oldReminderIncomplete = false;

    this.oldIsProjectAutoAssign = false;
    this.oldIsWorkOrdersFirstBidder = false;
    this.oldP2TPreferredOnly = false;
    this.oldMinutesRemainPublished = "";
    this.oldMinimumSelfRating = "";
    this.oldMaximumAllowableDistance = "";
    this.blastFromAddressDefault = 'no-replies@fieldsolutions.us';
	
	this.autoSubmitAfterP2TAuthorize = false;
	this.acceptedP2T = false;

	this._enablePushWM = false;
}

FSWidgetProjectDetails.prototype = new FSWidget();
FSWidgetProjectDetails.prototype.constructor = FSWidgetProjectDetails;

FSWidgetProjectDetails.NAME          = 'FSWidgetWODetails';
FSWidgetProjectDetails.VERSION       = '0.1';
FSWidgetProjectDetails.DESCRIPTION   = 'Class FSWidgetWODetails';

FSWidgetProjectDetails.prototype.roll = function() {
    return this._roll;
}

/**
 *  open
 */
FSWidgetProjectDetails.prototype.open = function(options) {
    var p = options || {};
    var queryString = '/';
    var _url;
    var noSSL = false;

    if ( p.params !== undefined && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params )
            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                } else if ( key === 'projects' ) {
                    var ids = '';
                    for ( var pr=0; pr < p.params[key].length; ++pr ) {
                        ids += p.params[key][pr];
                        if ( pr + 1 < p.params[key].length ) {
                            ids += '|';
                        }
                    }
                    queryString += 'projects/' + ids + '/';
                }
            } else if (p.params[key] === false || p.params[key] === true) {
                /* ok */
            } else if ( p.params[key] !== null && p.params[key] !== "" && p.params[key].toLowerCase() !== 'none' ) {
                if (key === "download") {
                    noSSL = true;
                } else if ( key === 'start_date' || key === 'end_date' ) {
                    p.params[key] = p.params[key].replace(/\//g, '_');
                }
                queryString += key + '/' + p.params[key] + '/';
            }
    }
    try {
        _url = this._makeUrl(p.tab, queryString, noSSL);
    } catch ( e ) {
        return false;
    }
    return location = _url;
}

/**
 *  show
 *
 *  Function display widget
 *
 *  @param Object|null options
 */

FSWidgetProjectDetails.prototype.show = function(options) {
    var p = options || {tab:this.currentTab,params:this.getParams()};

    var queryString = '/';
    var _url;

    // TODO params needs be reconfigured
    if ( p.params !== undefined && p.params !== null && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params ) {
            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] === true || p.params[key] === false ) {
                if ( p.params[key] === false ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] !== null && p.params[key] !== "" && p.params[key] !== 'none' ) {
                queryString += key + '/' + p.params[key] + '/';
            } else {
                delete p.params[key];
            }
        }
    } else {
        p.params = {};
    }

    _url = this._makeUrl(this.currentTab, queryString);

    this.showPreloader();

    if ( this.tabOpenXhrRequest ) {
        this.tabOpenXhrRequest.abort();
        this.tabOpenXhrRequest = null;
    }

    this.tabOpenXhrRequest = $.ajax({
        url : _url,
        data : p.params,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/clients/logIn.php";
            } else {
                FSWidget.fillContainer("Sorry, but an error occurred. No results found.", this);
            }
            this.tabOpenXhrRequest = null;
            
        },
        success : function(data, status, xhr) {
            $('body').css('cursor', '');
            FSWidget.fillContainer(data, this);
            this.prepareForm();
            
            this.EventWorkOrderSettings();
            this.buildEventMenu();
        }
    });

}
FSWidgetProjectDetails.prototype.prepareForm = function() {
    $("#Default_Amount").blur(this.massageMyMoney);
    $("#Default_Amount").blur();
    $("#Mod0CancelRecord").click(function() {backClicked = true;});
    
    $("#EnableSiteStatusReporting").click(function() {NoShowComfirm = true;});
    
    $("#isWorkOrdersFirstBidder_0").click(this.toggleFirstBidder);
    $("#isWorkOrdersFirstBidder_1").click(this.toggleFirstBidder);
    $("#P2TPreferredOnly_0").click(this.togglePreferredOnly);
    $("#P2TPreferredOnly_1").click(this.togglePreferredOnly);
    var formatTimeFunction = function(value)
    {
        if (value == null || value == "") return "";
        value = value.toUpperCase();
        var validFormat = /^(\d{1,2})(AM|PM)$/;
        var parts = value.match(validFormat);
        if (parts != null)
        {
            value = parts[1]+":00 " +parts[2];
            return value;
        }    
        validFormat = /^(\d*)(AM|PM)$/;
        parts = value.match(validFormat);
        if (parts != null)
        {
            value = parts[1]+" " +parts[2];
            return value;
        } 
        validFormat = /^(\d{1,2}):(\d{2})(AM|PM)$/;
        parts = value.match(validFormat);
        if (parts != null)
        {
            value = parts[1]+":"+parts[2]+" " +parts[3];
            return value;
        } 
        return value;
    }
    $("#StartTime").blur(function(){
        jQuery(this).val(formatTimeFunction(jQuery(this).val()));
    });
    $("#EndTime").blur(function(){
        jQuery(this).val(formatTimeFunction(jQuery(this).val()));
    });
    if(this.formType == "create"){

    }else{
        $("#acsAuthBy").html($("#ACSAuthBy").val());
        this.oldReminderAll = $("#ReminderAll").attr("checked");
        this.oldSMSBlast = $("#AutoSMSOnPublish").attr("checked");
        this.oldReminderAcceptance = $("#ReminderAcceptance").attr("checked");
        this.oldReminder24Hr = $("#Reminder24Hr").attr("checked");
        this.oldReminder1Hr = $("#Reminder1Hr").attr("checked");
        this.oldCheckInCall = $("#CheckInCall").attr("checked");
        this.oldCheckOutCall = $("#CheckOutCall").attr("checked");
        this.oldReminderNotMarkComplete = $("#ReminderNotMarkComplete").attr("checked");
        this.oldReminderIncomplete = $("#ReminderIncomplete").attr("checked");

        this.oldIsProjectAutoAssign = $("#isProjectAutoAssign").attr("checked");
        this.oldIsWorkOrdersFirstBidder = $("#isWorkOrdersFirstBidder").attr("checked");
        this.oldP2TPreferredOnly = $("#P2TPreferredOnly").attr("checked");
        this.oldMinutesRemainPublished = $("#MinutesRemainPublished").val();
        this.oldMinimumSelfRating = $("#MinimumSelfRating").val();
        this.oldMaximumAllowableDistance = $("#MaximumAllowableDistance").val();

        minutesRemainPublished = $("#MinutesRemainPublished").val();
        if (minutesRemainPublished != "") {
            days = Math.floor(minutesRemainPublished / 1440.0);
            minutesRemainPublished -= days * 1440;
            hours = Math.floor(minutesRemainPublished / 60.0);
            minutesRemainPublished -= hours * 60;
            minutes = minutesRemainPublished;
            if(isNaN(days))days = 0;
            if(isNaN(hours))hours = 0;
            if(isNaN(minutes))minutes = 0;
            $("#daysRemainPublished").attr("value", days);
            $("#hoursRemainPublished").attr("value", hours);
            $("#minutesRemainPublished_virt").attr("value", minutes);
        }
    }

    oldPcntDeduct = $("#PcntDeduct").attr("checked");

    $("#daysRemainPublished").change(this.updateMinutesRemainPublished);
    $("#hoursRemainPublished").change(this.updateMinutesRemainPublished);
    $("#minutesRemainPublished_virt").change(this.updateMinutesRemainPublished);
    $("#isProjectAutoAssign").click(this.toggleP2T);
    $("#PushWM").click(this.togglePushWM);

    $("#Client_ID").change(this.ajaxFunction);
    $("#Client_ID").change(this.ajaxFunction);
    $("#acceptReplies").click(this.acceptReplyClick);
    $("#ReminderAll").click(this.IVRToggleAll);

    //732
    $("#SiteContactReminderCall").click(
        function()
        {
            if($(this).is(":checked"))
            {
                $("#SiteContactReminderCalllabel").css("display","");
            }
            else
            {
                $("#SiteContactReminderCalllabel").css("display","none");
            }
        }
    );
    //end 732
    if ($("#isProjectAutoAssign").attr("checked"))
        this.toggleP2T();
    else
        this.toggleP2TFields(false);

    this.IVRToggleAll_InitForm();

    this.fromEmail = $("#RecruitmentFromEmail");
    this.fromEmail.focus(this.recruitmentEmailFocus);
    this.fromEmail.blur(this.recruitmentEmailBlur);
    $("#CreateProjectForm").submit(this.validateSubmit);

    this.recruitmentEmailBlur();
}

FSWidgetProjectDetails.prototype.showPreloader = function() {
    if (this._customfunctions.showPreloader !== undefined && typeof this._customfunctions.showPreloader === 'function' ) {
        this._customfunctions.showPreloader();
    }else{
        $('body').css('cursor', 'wait');
        var img = "<div class='tCenter' ><img style='margin:10% auto;' alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></div>";
        FSWidget.fillContainer(img, this);
    }
}

FSWidgetProjectDetails.prototype.updateDetails = function() {
    opt = this._option;
    this._option = 0;
    this.showStartPopup();
    this._redirect =false;
    this._updatestatus = {success:1, errors:[]};
    this._activeupload = 0;

	//Include all FS-RUSH cred/cert selections in form
	FSMultiselectControl.submit ();

    params = $('#CreateProjectForm').serialize();
    params += '&Project_Company_ID='+window._company;
    params += '&ACSApplyTo='+opt;
    
    params += "&Description="+encodeURIComponent(tinyMCE.get('Description').getContent());
    params += "&Requirements="+encodeURIComponent(tinyMCE.get('Requirements').getContent());
    params += "&SpecialInstructions="+encodeURIComponent(tinyMCE.get('SpecialInstructions').getContent());
    params += "&SpecialInstructions_AssignedTech="+encodeURIComponent(tinyMCE.get('SpecialInstructions_AssignedTech').getContent());
    
	this._doneWithUpdate = false;
    $.ajax({
        url         : '/widgets/dashboard/project/do-update/',
        dataType    : 'json',
        data        : params,
        cache       : false,
        type        : 'post',
        context     : this,
		error		: function (data, status, xhr) {
			//console.log (data, status, xhr);
		},
        success     : function (data, status, xhr) {
            this._updatestatus = data;
            if (data.success)
            {
                this._uploadsCounter = 0;
                this.params.project_id = data.project_id;

                this._redirect = false;
                fileFields = $('#UploadPrjDocsNew input:file');
                fileFieldCount= fileFields.length;
                for ( var i = 1; i <= fileFieldCount; i++ )
                {
                    if ( $('#prjDoc'+i).val() && $('#prjDoc'+i).val() != '' )
                    {
                        this._uploadsCounter++;
                        this.asyncUploadFile('prjDoc'+i,  'update', opt);
                    }
                }
                
                if ( $('#Project_Logo').val())
                {
                    this._uploadsCounter++;
                    this.asyncUploadFile( 'Project_Logo', 'update', opt );
                }
                if ( $('#CustomSignOff').val() ) {
                    this._uploadsCounter++;
                    this.asyncUploadFile( 'CustomSignOff', 'update', opt );
                }

                //no files to upload
                if ( this._uploadsCounter <= 0 )
                {
                    if(opt == 0)
                        this.showMessages(this._updatestatus);
//                    else
//                        this.callUpdateOptionsScript(opt, data.prjold);
                }
                
                this.callUpdateOptionsScript(opt, data.prjold, data.oldexpen);
            }
            else
            {
				//console.log (data, status, xhr);
                this.showMessages(data);
            }
        },
		error	: function (data, status, xhr) {
			//console.log (data, status, xhr);
		}
    });
}

FSWidgetProjectDetails.prototype.createWO = function() {
    this.showStartPopup();
    this._updatestatus = {success:1, errors:[]};

	//Include all FS-RUSH cred/cert selections in form
	FSMultiselectControl.submit ();

    params = $('#CreateProjectForm').serialize();
    params += '&Project_Company_ID='+window._company;
    params += "&Description="+encodeURIComponent(tinyMCE.get('Description').getContent());
    params += "&Requirements="+encodeURIComponent(tinyMCE.get('Requirements').getContent());
    params += "&SpecialInstructions="+encodeURIComponent(tinyMCE.get('SpecialInstructions').getContent());
    params += "&SpecialInstructions_AssignedTech="+encodeURIComponent(tinyMCE.get('SpecialInstructions_AssignedTech').getContent());
    $.ajax({
        url         : '/widgets/dashboard/project/do-create/',
        dataType    : 'json',
        data        : params,
        cache       : false,
        type        : 'post',
        context     : this,
		error		: function (data, status, xhr) {
			console.log (data, status, xhr);
		},
        success     : function (data, status, xhr) {
            this._updatestatus = data;
            if (data.success) {
                this._uploadsCounter = 0;
                this.params.project_id = data.project_id;
                
                this._redirect = false;
                fileFields = $('#UploadPrjDocsNew input:file');
                fileFieldCount= fileFields.length;
                for ( var i = 1; i <= fileFieldCount; i++ ) {
                    if ( $('#prjDoc'+i).val() && $('#prjDoc'+i).val() != '' ) {
                        this._uploadsCounter++;
                        this.asyncUploadFile('prjDoc'+i,  'update' );
                    }
                }
                if ( $('#Project_Logo').val() ) {
                    this._uploadsCounter++;
                    this.asyncUploadFile( 'Project_Logo', 'update' );
                }
                if ( $('#CustomSignOff').val() ) {
                    this._uploadsCounter++;
                    this.asyncUploadFile( 'CustomSignOff', 'update' );
                }

                //no files to upload
                if ( this._uploadsCounter <= 0 ) {
                    this.showMessages(this._updatestatus);
                }
            }else{
                this.showMessages(this._updatestatus);
            }
        }
    });
}

FSWidgetProjectDetails.prototype.showStartPopup = function() {
    var html = "<div class=\"tCenter\"><img alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></div>";
    this._roll.autohide(false);
    var opt = {
        width       : 300,
        height      : '',
        position    : 'middle',
        title       : 'Processing',
        body        : html
    };
    return this._roll.showNotAjax(null, null, opt);
}

FSWidgetProjectDetails.prototype.showMessages = function(data) {
    var html = '';
    if (this._redirect || data.success) { //always refresh page on success
		if (this._doneWithUpdate === false || this._uploadsCounter > 0) return;
        if (window.opener) {try{window.opener.reloadTabFrame();} catch (e) {}}
        window.location = '/clients/projectsView.php?v='+window._company;
    }
    if (data.success) {
        html += '<div class="success"><ul><li>Success!</li></ul></div>';
    }else{
        html+='<div class="errors"><ul>';
        for (i=0;i<data.errors.length;i++) {
            if(typeof data.errors[i] === 'object')
                html+='<li>'+data.errors[i].message+'</li>';
            else
                html+='<li>'+data.errors[i]+'</li>';
        }
        html+='</ul></div>';
    }
    html+='<div class="tCenter"><input type="button" class="link_button_popup" onclick="detailsWidget.roll().hide();" value="Ok" /></div>';
    if(typeof(this._roll) == 'undefined') this._roll = detailsWidget._roll;
    this._roll.autohide(false);
    var opt = {
        width       : 400,
        height      : '',
        position    : 'middle',
        title       : '',
        body        : html
    };
    if (data.success) {
        opt.width = 300;
    }
    return this._roll.showNotAjax(null, null, opt);
}

FSWidgetProjectDetails.prototype.asyncUploadFile = function(id, action, option){
	descr = '';
    if($('#Descr'+id).val() != undefined) descr = encodeURIComponent($('#Descr'+id).val());
    if (typeof (option) == "undefined") option = 3;
	var uploadUrl = '/widgets/dashboard/project/ajax-upload-file';
	
    $.ajaxFileUpload({
        url           : uploadUrl + "/project_id/"+this.params.project_id+"/file/"+id+"/company/"+window._company+"/prj_act/"+action+"/descr/"+descr+"/option/"+option,
        secureuri     : false,
        fileElementId : id,
        dataType      : 'json',
        context       : this,
        cache         : false,
        success       : function (data, status, xhr) {
            this.context._uploadsCounter--;
            if ( !data.success ) {
                this.context._updatestatus.success = 0;
                if ( !this.context._updatestatus.errors ) {
                    this.context._updatestatus.errors = [];
                }
                for (i=0;i<data.errors.length;i++) {
                    this.context._updatestatus.errors.push(data.errors[i]);
                }
            }
            
            if ( this.context._uploadsCounter <= 0 ) {
                this.context.showMessages(this.context._updatestatus);
            }
        },
        error:function(data, status, xhr){
            this._uploadsCounter--;
            if ( !data.success ) {
                this._updatestatus.success = 0;
                if ( !this._updatestatus.errors ) {
                    this._updatestatus.errors = [];
                }
                this._updatestatus.errors.push('Upload failed');
            }
            if ( this._uploadsCounter <= 0 ) {
                this.showMessages(this._updatestatus);
            }
        }
    });
}

FSWidgetProjectDetails.prototype.initPhone = function(){
	var formatPhone = function() {
		if (this.value == null || this.value == "") return;
		validFormat = /^[(]?(\d{3})[)]?(?:\s|-|.)*(\d{3})[-\s.]*(\d{4})$/;
		parts = this.value.match(validFormat);
		if (parts == null) return;
		this.value = "(" + parts[1] + ") " + parts[2] + "-" + parts[3];
        };

	$.map(['edit_SitePhone', 'edit_SiteFax'], function( id ) {
		$('#'+id).each(formatPhone).blur(formatPhone);
	});
}

FSWidgetProjectDetails.prototype.confirmRemoveFile = function( obj, file_name )
{
    if ( $(obj).attr('checked') ) {
        if ( confirm('Are you sure you wish to remove the file "' + file_name + '"?') ) {
            $(obj).attr('checked', true);
        } else {
            $(obj).attr('checked', false);
        }
    }
}

/**
 *  getParams
 *
 *  Function collect all params for make request
 *  return object
 */
FSWidgetProjectDetails.prototype.getParams = function () {
    return {
        'tab'               : this.currentTab,
        'company'           : window._company,
        'win'               : this.params.win
    }
}

FSWidgetProjectDetails.prototype.ajaxFunction = function() {
	// lookup client information
    var clientID = $('#Client_ID').val();
	$.get("ajax/clientLookup.php", {"clientID" : clientID},
		function(data) {
			var getClientInfo = data.split(",");
			$('#Client_Company_Name').val(getClientInfo[0]);
			$('#Client_Name').val(getClientInfo[1]);
			$('#Client_Email').val(getClientInfo[2]);
		}
	);
}

FSWidgetProjectDetails.prototype.acceptReplyClick = function() {
	if (!$('#acceptReplies').attr('checked'))
		detailsWidget.fromEmail.attr("value", detailsWidget.blastFromAddressDefault);
}

FSWidgetProjectDetails.prototype.recruitmentEmailBlur = function() {
	if (detailsWidget.fromEmail.attr("value") == undefined)
    {
		detailsWidget.fromEmail.attr("value", detailsWidget.blastFromAddressDefault);
    }   
    //13991 
    if(detailsWidget.fromEmail.attr("value")!="" && detailsWidget.validEmailList(jQuery("#RecruitmentFromEmail").val()))
    {
		$('#acceptReplies').attr("checked", true);
}
    else if(detailsWidget.fromEmail.attr("value")=="")
    {
        detailsWidget.fromEmail.attr("value", detailsWidget.blastFromAddressDefault);
    }
    else
    {
       $('#acceptReplies').removeAttr("checked"); 
    }
    //end 13991
	if (detailsWidget.fromEmail.attr("value") == detailsWidget.blastFromAddressDefault )
    {
		$('#acceptReplies').attr("checked", false);
    }
	     //else
		//$('#acceptReplies').attr("checked", true); //13991   
        
}

FSWidgetProjectDetails.prototype.recruitmentEmailFocus = function() {
	if (detailsWidget.fromEmail.attr("value") == detailsWidget.blastFromAddressDefault)
		detailsWidget.fromEmail.attr("value", "");
        
             
   
	//$('#acceptReplies').attr("checked", true);   //13991
    
}

FSWidgetProjectDetails.prototype.massageMyMoney = function() {
	this.value = massageMoney(this.value);
}

FSWidgetProjectDetails.prototype.P2TAuthorize = function() {
	if (this.didP2TChange()) {
		this.acceptedP2T = true;
		if (this.autoSubmitAfterP2TAuthorize) {
			roll.hide();
			this.validateSubmit();
			return;
		}
	}
	roll.hide();
}

FSWidgetProjectDetails.prototype.showP2TStatement = function () {
    roll.autohide(false);
	$("#closeBtn").attr("value", this.didP2TChange() ? "I Accept" : "Close");
    var opt = {
        width       : 400,
        height      : '',
        title       : 'P2T&trade; Statement of Fees',
        context     : detailsWidget,
        position    : 'middle',
        body        : $("#p2tStatement").html()
    };
    roll.showNotAjax(null, null, opt);
}

FSWidgetProjectDetails.prototype.showACSUpdateOptions = function() {
	//updateOptionsWindow = window.open("/clients/acsUpdateOptions.php?id="+$('#Project_ID').val(),"ACSUpdateOptions",this.windowSettings);
}

//FSWidgetProjectDetails.prototype.updateP2T = function() {
FSWidgetProjectDetails.prototype.updateOptions = function(abr) {
    if(abr == '') return;
    roll.autohide(false);
    //14125
    var isCopy = $("#isCopy").val();
    //763
    var checkProjectAutoBlast = abr.indexOf("Project Auto Blast Settings") != -1 ? true : false; 
    //end 763
    //380
    //$("#optionMessage").html('How would you like to apply '+abr+' options changes?');
    //763
    if(isCopy==="0")
    {
        if($("#closeouttab").hasClass("current") == false || this.onlyCommSettingsUpdate() == true || checkProjectAutoBlast == true || this.numDocsChanged() == true){
                $("#optionMessage").html('How would you like to apply these changes?');
                //end 380
                var opt = {
                    width       : 400,
                    height      : '',
                    title       : 'Update Options',//(abr == 'acs' ? "ACS" : "P2T&trade;") +' Update Options',
                    context     : detailsWidget,
                    position    : 'middle',
                    //body        : $('#'+abr+'UpdateOptionsContainer').html()
                    body        : $('#PorjectUpdateOptionsContainer').html()
                };
                roll.showNotAjax(null, null, opt);
        }
    }
    else{
    	return (this.formType == "create") ? detailsWidget.createWO() : detailsWidget.updateDetails();
    }
	//updateP2TWindow = window.open("/clients/p2tUpdateOptions.php?id="+$('#Project_ID').val(),"P2TUpdateOptions",this.windowSettings);
}

FSWidgetProjectDetails.prototype.numDocsChanged = function(){
	var numDocsSelect = $("#SignOffSheet_Required");
	var currVal = numDocsSelect.val();
	var defaultValue;
	numDocsSelect.find('option').each(function (i, o) {
		if (o.defaultSelected) {
			defaultValue = o.value;
			if(defaultValue != currVal)displayPopup = true;
		 }
	});
	return displayPopup;
}

FSWidgetProjectDetails.prototype.onlyCommSettingsUpdate = function(){
	//check to see if the only change is to the checkbox that allows custom communication settings
	var displayPopup = false;
	$("#tableContactCenter :input").each(function(){
		
		if($(this).val() != '' && $(this).val() != $(this).attr('defaultValue') && $(this).attr('name') != "AllowCustomCommSettings" 
			&& $(this).attr('name') != "EmailBlastRadius" && $(this).attr('name') != "QuickTicketID"  && $(this).attr('name') != "ReceiveTechQAMessageNotificationEmails"){
			displayPopup = true;
		}
		
		if($(this).attr("name") == "EmailBlastRadius" || $(this).attr("name") == "QuickTicketID"){
			var currVal = $(this).val();
			var defaultValue;
			$(this).find('option').each(function (i, o) {
			  if (o.defaultSelected) {
			    defaultValue = o.value;
			    if(defaultValue != currVal)displayPopup = true;
			  }
			});
		}
	
	});
		
	return displayPopup;
}
//380
//FSWidgetProjectDetails.prototype.callUpdateOptionsScript = function (opt) {
//    abr = '';
//    if (this.didP2TChange()) abr = 'p2t';
//    else if (this.didACSChange()) abr = 'acs';
//    if(abr == '') return;
//    $.ajax({
//        url      : abr+'UpdateProjectWOs_1.php',
//        dataType : 'json',
//        data     : {id:this.params.project_id, choice:opt},
//        type     : 'get',
//        success  : function (data, status, xhr) {
//            detailsWidget.showMessages({success:1, errors:[]});
//        }
//    });
//}
FSWidgetProjectDetails.prototype.callUpdateOptionsScript = function (opt,prjold,oldexpen ) {
    var el = this;
    $.ajax({
        url      : 'ProjectUpdateProjectWOs.php',
        dataType : 'json',
        data     : {id:this.params.project_id, choice:opt, prj:prjold, expen:oldexpen},
        type     : 'post',
		error  : function (data, status, xhr) {
			//console.log (data, status, xhr);
		},
        success  : function (data, status, xhr) {
		el._doneWithUpdate = true;
                el.showMessages({success:1, errors:[]});
        }
    });
}
//end 380

FSWidgetProjectDetails.prototype.updateMinutesRemainPublished = function() {
	d =	$("#daysRemainPublished").val();
	h = $("#hoursRemainPublished").val();
	m = $("#minutesRemainPublished_virt").val();

	d = parseInt(d, 10);
	h = parseInt(h, 10);
	m = parseInt(m, 10);

	if (isNaN(d)) {
		d = 0;
		$("#daysRemainPublished").val(d);
	}

	if (isNaN(h)) {
		h = 0;
		$("#hoursRemainPublished").val(h);
	}

	if (isNaN(m)) {
		m = 0;
		$("#minutesRemainPublished_virt").val(m);
	}

	remain = d *1440 + h * 60 + m;
	$("#MinutesRemainPublished").attr("value", remain);
}

FSWidgetProjectDetails.prototype.isValidContactPhone = function(val, allowExt) {
	if (val == null || val == "") return true;
	valStrip = val.replace(/[^0-9]/g, "");
	if (valStrip.length == 10) {
		val = massagePhone(val);
		return isValidPhone(val);
	}
	else {
		if (!allowExt) return false;
		validFormat = /^[(]?(\d{3})[)]?(?:\s|-|.)*(\d{3})[-\s.]*(\d{4})[^0-9]+([0-9]*)[^0-9]*([0-9]*)$/;
		parts = val.match(validFormat);
		return !(parts == null);
	}
}

FSWidgetProjectDetails.prototype.toggleFirstBidder = function() {
    $("#isWorkOrdersFirstBidder").attr("checked", $('input[name=P2TPreferredOnly]:checked').val());
}

FSWidgetProjectDetails.prototype.togglePreferredOnly = function() {
	$("#P2TPreferredOnly").attr("checked", $('input[name=isWorkOrdersFirstBidder]:checked').val());
}

FSWidgetProjectDetails.prototype.IVRToggleAll = function() {
	var allChecked = $("#ReminderAll").attr("checked") ? true : false;
	if (allChecked) {
		$("#ReminderAcceptance").attr("checked", true);
		$("#AutoSMSOnPublish").attr("checked", true);
		$("#Reminder24Hr").attr("checked", true);
		$("#Reminder1Hr").attr("checked", true);
		$("#CheckInCall").attr("checked", true);
		$("#CheckOutCall").attr("checked", true);
		$("#ReminderNotMarkComplete").attr("checked", true);
		$("#ReminderIncomplete").attr("checked", true);
        $("#ReminderCustomHrChecked").attr("checked", false);
        $("#ReminderCustomHrChecked_2").attr("checked", false);
        $("#ReminderCustomHrChecked_3").attr("checked", false);
	}
    else
    {
        $("#ReminderAcceptance").attr("checked", false);
        $("#AutoSMSOnPublish").attr("checked", false);
        $("#Reminder24Hr").attr("checked", false);
        $("#Reminder1Hr").attr("checked", false);
        $("#CheckInCall").attr("checked", false);
        $("#CheckOutCall").attr("checked", false);
        $("#ReminderNotMarkComplete").attr("checked", false);
        $("#ReminderIncomplete").attr("checked", false);
        $("#ReminderCustomHrChecked").attr("checked", false);        
        $("#ReminderCustomHrChecked_2").attr("checked", false);
        $("#ReminderCustomHrChecked_3").attr("checked", false);
	}

	$("#ReminderAcceptance").attr("disabled", allChecked);
	$("#AutoSMSOnPublish").attr("disabled", allChecked);
	$("#Reminder24Hr").attr("disabled", allChecked);
	$("#Reminder1Hr").attr("disabled", allChecked);
	$("#CheckInCall").attr("disabled", allChecked);
	$("#CheckOutCall").attr("disabled", allChecked);
	$("#ReminderNotMarkComplete").attr("disabled", allChecked);
	$("#ReminderIncomplete").attr("disabled", allChecked);
    $("#ReminderCustomHrChecked").attr("disabled", allChecked);
    $("#ReminderCustomHrChecked_2").attr("disabled", allChecked);
    $("#ReminderCustomHrChecked_3").attr("disabled", allChecked);
}
FSWidgetProjectDetails.prototype.IVRToggleAll_InitForm = function() {
    var allChecked = $("#ReminderAll").attr("checked") ? true : false;
    if (allChecked) {
        $("#ReminderAcceptance").attr("checked", true);
        $("#AutoSMSOnPublish").attr("checked", true);
        $("#Reminder24Hr").attr("checked", true);
        $("#Reminder1Hr").attr("checked", true);
        $("#CheckInCall").attr("checked", true);
        $("#CheckOutCall").attr("checked", true);
        $("#ReminderNotMarkComplete").attr("checked", true);
        $("#ReminderIncomplete").attr("checked", true);
        $("#ReminderCustomHrChecked").attr("checked", false);
        $("#ReminderCustomHrChecked_2").attr("checked", false);
        $("#ReminderCustomHrChecked_3").attr("checked", false);
    }

    $("#ReminderAcceptance").attr("disabled", allChecked);
    $("#AutoSMSOnPublish").attr("disabled", allChecked);
    $("#Reminder24Hr").attr("disabled", allChecked);
    $("#Reminder1Hr").attr("disabled", allChecked);
    $("#CheckInCall").attr("disabled", allChecked);
    $("#CheckOutCall").attr("disabled", allChecked);
    $("#ReminderNotMarkComplete").attr("disabled", allChecked);
    $("#ReminderIncomplete").attr("disabled", allChecked);
    $("#ReminderCustomHrChecked").attr("disabled", allChecked);
    $("#ReminderCustomHrChecked_2").attr("disabled", allChecked);
    $("#ReminderCustomHrChecked_3").attr("disabled", allChecked);
}

FSWidgetProjectDetails.prototype.toggleP2T = function() {
	var val = $("#isProjectAutoAssign").attr("checked") ? true : false;
	detailsWidget.toggleP2TFields(val);

    if(val && !$("#ReminderAll").attr("checked")) {
        $("#ReminderAll").attr("disabled", false);
        $("#ReminderAll").attr("checked", false);

        $("#ReminderAcceptance").attr("checked", val);
        $("#AutoSMSOnPublish").attr("checked", val);
        $("#Reminder24Hr").attr("checked", val);
        $("#Reminder1Hr").attr("checked", val);

        $("#ReminderAcceptance").attr("disabled", false);
        $("#AutoSMSOnPublish").attr("disabled", false);
        $("#Reminder24Hr").attr("disabled", false);
        $("#Reminder1Hr").attr("disabled", false);
        $("#CheckInCall").attr("disabled", false);
        $("#CheckOutCall").attr("disabled", false);
        $("#ReminderNotMarkComplete").attr("disabled", false);
        $("#ReminderIncomplete").attr("disabled", false);
    }

}

FSWidgetProjectDetails.prototype.toggleP2TFields = function(val) {
    $("#isWorkOrdersFirstBidder_0").attr("disabled", !val);
    $("#isWorkOrdersFirstBidder_1").attr("disabled", !val);
    $("#P2TPreferredOnly_0").attr("disabled", !val);
    $("#P2TPreferredOnly_1").attr("disabled", !val);

	$("#daysRemainPublished").attr("disabled", !val);
	$("#hoursRemainPublished").attr("disabled", !val);
	$("#minutesRemainPublished_virt").attr("disabled", !val);

	$("#MinimumSelfRating").attr("disabled", !val);
	$("#MaximumAllowableDistance").attr("disabled", !val);
	//$("#CredentialCertification").attr("disabled", !val); /*13329*/
	//$("#PublicCertification").attr("disabled", !val); /*13329*/
	//$("#FSExpert").attr("disabled", !val); /*13329*/
	//$("#IndustryCertification").attr("disabled", !val); /*13329*/
	$("#PushWM").attr("disabled", !val || $("#PushWM").attr("wmAllow") != 1);

	$("#NotifyOnAutoAssign").attr("disabled", !val);
	if (!val) {
		$("#NotifyOnAutoAssign").attr("checked", false);
	}
    this.togglePushWM();
}

FSWidgetProjectDetails.prototype.togglePushWM = function() {
	if (!$("#isProjectAutoAssign").attr("checked")) return;
	if (!$("#PushWM").attr('checked')) {
		$("#PushWMCheckedMsg").hide();
		$("#radio3").attr('disabled', false);
		$("#radio4").attr('disabled', false); // Any Matching Tech
		//$("#CredentialCertification").attr('disabled', false);
		//$("#PublicCertification").attr('disabled', false);
		//$("#FSExpert").attr('disabled', false);
		//$("#IndustryCertification").attr('disabled', false);
		return;
	}
	$("#PushWMCheckedMsg").show();
	$("#radio3").attr('disabled', true);
	$("#radio4").click().attr('disabled', true); // Any Matching Tech
	//$("#CredentialCertification").attr('disabled', true).attr ("selectedIndex", 0); // Client Cred off
	//$("#PublicCertification").attr('disabled', true).attr ("selectedIndex", 0);
	//$("#FSExpert").attr('disabled', true).attr ("selectedIndex", 0);
	//$("#IndustryCertification").attr('disabled', true).attr ("selectedIndex", 0);
}

FSWidgetProjectDetails.prototype.didP2TChange = function() {
	return this.oldIsProjectAutoAssign != $("#isProjectAutoAssign").attr("checked") || this.oldIsWorkOrdersFirstBidder != $("#isWorkOrdersFirstBidder").attr("checked") ||
        this.oldP2TPreferredOnly != $("#P2TPreferredOnly").attr("checked") || this.oldMinutesRemainPublished != $("#MinutesRemainPublished").val() || this.oldMinimumSelfRating != $("#MinimumSelfRating").val() || this.oldMaximumAllowableDistance != $("#MaximumAllowableDistance").val();
}

FSWidgetProjectDetails.prototype.didACSChange = function() {
	return this.oldReminderAll != $("#ReminderAll").attr("checked") || this.oldReminderAcceptance != $("#ReminderAcceptance").attr("checked") ||
		this.oldSMSBlast != $("#AutoSMSOnPublish").attr("checked") || this.oldReminder24Hr != $("#Reminder24Hr").attr("checked") ||
		this.oldReminder1Hr != $("#Reminder1Hr").attr("checked") || this.oldCheckInCall != $("#CheckInCall").attr("checked") ||
		this.oldCheckOutCall != $("#CheckOutCall").attr("checked") || this.oldReminderNotMarkComplete != $("#ReminderNotMarkComplete").attr("checked") ||
		this.oldReminderIncomplete != $("#ReminderIncomplete").attr("checked");
}

FSWidgetProjectDetails.prototype.validEmailList = function(list) {
	if (list == null || list == "") return false;
	list = list.replace(/\s/g);
	listParts = list.split(",");
	valid = true;
	for (index in listParts) {
		email = listParts[index];
		if (!isValidEmail(email)) {
			valid = false;
		}
	}
	return valid;
}

FSWidgetProjectDetails.prototype.validateSubmit = function() {
	// validate required ACS fields
	
	if (backClicked) return true;
	var isACSEnabled = $("#ReminderAll").attr("checked") || ($("#ReminderAcceptance").attr("checked") || $("#AutoSMSOnPublish").attr("checked") || $("#Reminder24Hr").attr("checked") || $("#Reminder1Hr").attr("checked") || $("#CheckInCall").attr("checked") || $("#CheckOutCall").attr("checked") || $("#ReminderNotMarkComplete").attr("checked") || $("#ReminderIncomplete").attr("checked") || $("#AutoSMSOnPublish").attr("checked"));
	var isP2TEnabled = $("#isProjectAutoAssign").attr("checked");
	var EmergencyName = $("#EmergencyName");
	var EmergencyEmail = $("#EmergencyEmail");
	var EmergencyPhone = $("#EmergencyPhone");
	var ProjectManager = $("#Project_Manager");
	var ProjectManagerEmail = $("#Project_Manager_Email");
	var ProjectManagerPhone = $("#Project_Manager_Phone");
	var ServiceTypeId = $("#ServiceTypeId").attr("value");  //new
	var RCPhone = $("#Resource_Coord_Phone");
	var TechnicalSupportPhone = $("#TechnicalSupportPhone");
	var CheckInOutPhone = $("#CheckInOutNumber");

	var P2TNotificationEmail = $("#P2TNotificationEmail");
	
	var NotifyOnAssign = $("#NotifyOnAssign").attr("checked");
	var AssignmentNotificationEmail = $("#AssignmentNotificationEmail");
	
	var NotifyOnBid = $("#NotifyOnBid").attr("checked");
	var BidNotificationEmails = $("#BidNotificationEmails");
	
	var NotifyOnWorkDone = $("#NotifyOnWorkDone").attr("checked");
	var WorkDoneNotificationEmails = $("#WorkDoneNotificationEmails");
	var NotifyOnAutoAssign = $("#NotifyOnAutoAssign").attr("checked");
	var AutoSMSOnPublish = $("#AutoSMSOnPublish").attr("checked");
	
    var ReminderCustomHrChecked = $("#ReminderCustomHrChecked").attr("checked");//13483
    var ReminderCustomHr = $("#ReminderCustomHr");//13483
    var ReminderCustomHrChecked_2 = $("#ReminderCustomHrChecked_2").attr("checked");//13483
    var ReminderCustomHr_2 = $("#ReminderCustomHr_2");//13483
    var ReminderCustomHrChecked_3 = $("#ReminderCustomHrChecked_3").attr("checked");//13483
    var ReminderCustomHr_3 = $("#ReminderCustomHr_3");//13483

    //13691
    var ACSNotifiPOC = $("#ACSNotifiPOC");
    var ACSNotifiPOCEmail = $("#ACSNotifiPOC_Email");
    var ACSNotifiPOCPhone = $("#ACSNotifiPOC_Phone");    
    //end 13691
    //14125
    var Project_Name= $("#Project_Name");
    if (Project_Name.val() == "" ) {
        alert("The Project_Name is required.");
        Project_Name.focus();
        return false;
	}
    if(typeof(_data)!= "undefined" &&  $("#ServiceTypeId").attr("checked") !=1)
    { 
        var jsons = eval("("+Base64.decode(_data)+")");
        var ServiceTypeIdnew = (typeof($("#ServiceTypeId").val()) == 'undefined'?'':$("#ServiceTypeId").val());
        var ServiceTypeIdold = ((jsons[$("#ServiceTypeId").attr("name")]==null)?'':jsons[$("#ServiceTypeId").attr("name")]);
        var FSClientServiceDirectorIdnew = (typeof($("#FSClientServiceDirectorId").val()) == 'undefined'?'':$("#FSClientServiceDirectorId").val());
        var FSClientServiceDirectorIdold = ((jsons[$("#FSClientServiceDirectorId").attr("name")]==null)?'':jsons[$("#FSClientServiceDirectorId").attr("name")]) ;
        
        if((ServiceTypeIdnew != ServiceTypeIdold) && (FSClientServiceDirectorIdnew == FSClientServiceDirectorIdold))
        {
            var msg =   "<div style='padding-top:5px;padding-bottom:10px;'>\n\
                            Service Type selection is changed. Don&rsquo;t forget to change the project CSD if necessary.\n\
                            \n\
                        </div>\n\
                        <div>\n\
                            <table border='0' width='100%'>\n\
                                <tr>\n\
                                    <td style='width:50%;text-align:right;'>\n\
                                        <input id='checkMarkcompletedCancel' class='link_button link_button_popup' type='button' value='Go Back'>\n\
                                    </td>\n\
                                    <td style='width:50%;text-align:left;'>\n\
                                        <input id='checkMarkcompletedSave' class='link_button_popup' type='button' value='Save'>\n\
                                    </td>\n\
                                </tr>\n\
                            </table>\n\
                        </div>";
            $("<div></div>").fancybox(
                    {
                            'autoDimensions' : false,
                            'width'	: 300,
                            'height' : 80,
                            'transitionIn': 'none',
                            'transitionOut' : 'none',
                            'content' : msg
                    }
            ).trigger('click');
            $("#checkMarkcompletedCancel").click(function(){
                $.fancybox.close();
            $("#FSClientServiceDirectorId").focus();
            });
            $("#checkMarkcompletedSave").click(function(){
                $.fancybox.close();
                $("#ServiceTypeId").attr("checked","1");
                $("#formSubmit").click();
            });
            return false;
        }

    }
	if (isACSEnabled) {
		// missing required contact fields
        //13691 - Project Manager Name/Phone/Email should NOT be required anymore when ACS settings are enabled. ACS Notification POC Name/Phone/Email SHOULD be required when ACS settings are enabled

		if (ACSNotifiPOC.val() == "") {
			alert("The ACS Notification POC Name is required for ACS");
			ACSNotifiPOC.focus();
			return false;
		}
		if (ACSNotifiPOCPhone.val() == "") {
			alert("The ACS Notification POC Phone is required for ACS");
			ACSNotifiPOCPhone.focus();
			return false;
		}
		if (ACSNotifiPOCEmail.val() == "") {
			alert("The ACS Notification POC Email is required for ACS");
			ACSNotifiPOCEmail.focus();
			return false;
		}
        //end 13691
/*		if (EmergencyName.val() == "" && AutoSMSOnPublish==false) {
			alert("The Emergency Name is required for ACS");
			EmergencyName.focus();
			return false;
		}
		if (EmergencyPhone.val() == "" && AutoSMSOnPublish==false) {
			alert("The Emergency Number is required for ACS");
			EmergencyPhone.focus();
			return false;
		}
		if (EmergencyEmail.val() == "" && AutoSMSOnPublish==false) {
			alert("The Emergency Email is required for ACS");
			EmergencyEmail.focus();
			return false;
		}*/
	}
	
	if (!this.isValidContactPhone(ProjectManagerPhone.val(), true)) {
		alert("The Project Manager Number must be a valid 10 digit number with optional extension similar to ###-###-#### x ####");
		ProjectManagerPhone.focus();
		return false;
	}
	if (!this.isValidContactPhone(RCPhone.val(), true)) {
		alert("The RC Number must be a valid 10 digit number with optional extension similar to ###-###-#### x ####");
		RCPhone.focus();
		return false;
	}
	if (!this.isValidContactPhone(EmergencyPhone.val())) {
		alert("The Emergency Number must be a valid 10 digit number similar to ###-###-####");
		EmergencyPhone.focus();
		return false;
	}
	if (!this.isValidContactPhone(TechnicalSupportPhone.val(), true)) {
		alert("The Technical Support Number must be a valid 10 digit number with optional extension similar to ###-###-#### x ####");
		TechnicalSupportPhone.focus();
		return false;
	}
	if (!this.isValidContactPhone(CheckInOutPhone.val(), true)) {
		alert("The Check in / out Number must be a valid 10 digit number with optional extension similar to ###-###-#### x ####");
		CheckInOutPhone.focus();
		return false;
	}
    /************/
    if (NotifyOnAutoAssign) {
        if (P2TNotificationEmail.val() == "" || !this.validEmailList(P2TNotificationEmail.val())) {
            alert("The P2T Notification Email is not a valid email address");
            P2TNotificationEmail.focus();
            return false;
        }
    }
    if (NotifyOnAssign) {
        if (AssignmentNotificationEmail.val() == "" || !this.validEmailList(AssignmentNotificationEmail.val())) {
            alert("The Assignment Notification Email is not a valid email address");
            AssignmentNotificationEmail.focus();
            return false;
        }
    }
    
    if (NotifyOnBid) {
        if (BidNotificationEmails.val() == "" || !this.validEmailList(BidNotificationEmails.val())) {
            alert("The Bid Notification Email is not a valid email address");
            BidNotificationEmails.focus();
            return false;
        }
    }
    
    if (NotifyOnWorkDone) {
        if (WorkDoneNotificationEmails.val() == "" || !this.validEmailList(WorkDoneNotificationEmails.val())) {
            alert("The Work Done Notification Email is not a valid email address");
            WorkDoneNotificationEmails.focus();
            return false;
        }
    }
    var data = {success:0,errors:[]};
    var i = 0;
    var isCallType = $("#edit_Type_ID").val();
    if(isCallType=="")
    {
        data.errors[i] =  "Call Type empty but required.";
        i++;
    }
    var isServiceType = $("#ServiceTypeId").val();
    if(isServiceType=="")
    {
        data.errors[i] =  "Service type empty but required.";
        i++;
    }
     var isFSClientServiceDirector = $("#FSClientServiceDirectorId").val();
    if(isFSClientServiceDirector=="")
    {
        data.errors[i] =  "CSD empty but required.";
        i++;
    }
    
    if(jQuery.trim(jQuery("#From_Email").val()) == "")
    {
        data.errors[i] =  "Send System-Generated Emails From empty but required.";
        i++;
    } 
    
     //13964                 .attr("checked") !=1)   
    // if (!this.validEmailList(jQuery("#ServiceTypeId").attr("value")!="") {
         
     //   data.errors[i] =  "Service Type empty but required.";
       // i++;
  //  }
  //13991
   
   if($('#acceptReplies').attr('checked')==true && jQuery.trim(jQuery("#RecruitmentFromEmail").val()) == "")  {
        data.errors[i] =  "Auto Blast From Email To empty but required.";
        i++;
    }
  
    if(!this.validEmailList(jQuery("#RecruitmentFromEmail").val()))  {
        data.errors[i] =  "Auto Blast From Email is not a valid email address.";
        i++;
    }
    //end 13991
    if(jQuery.trim(jQuery("#To_Email").val()) == "")
    {
        data.errors[i] =  "Send System-Generated Emails To empty but required.";
        i++;
    }
    if (!this.validEmailList(jQuery("#From_Email").val()) && jQuery.trim(jQuery("#From_Email").val()) != "") {
        data.errors[i] =  "Send System-Generated Emails From is not a valid email address.";
        i++;
    }
    if (!this.validEmailList(jQuery("#To_Email").val()) && jQuery.trim(jQuery("#To_Email").val()) != "") {
        data.errors[i] =  "Send System-Generated Emails To is not a valid email address.";
        i++;
//        return false;
    }
    //13483
    if((ReminderCustomHrChecked && ReminderCustomHr.val() == "") || (ReminderCustomHrChecked_2 && ReminderCustomHr_2.val() == "") || (ReminderCustomHrChecked_3 && ReminderCustomHr_3.val() == "")) 
    {
        data.errors[i] =  "Custom Reminder Call empty but required.";
        i++;
    }
    
    if((ReminderCustomHr.val() != "" && isNaN(ReminderCustomHr.val())) || (ReminderCustomHr_2.val() != "" && isNaN(ReminderCustomHr_2.val())) || (ReminderCustomHr_3.val() != "" && isNaN(ReminderCustomHr_3.val())) )
    {
        data.errors[i] =  "Custom Reminder Call is invalid.";
        i++;   
    }
    
    var num=0;
    if(ReminderCustomHr.val() != "" && !isNaN(ReminderCustomHr.val()))
    {
        num = parseInt(ReminderCustomHr.val(), 10);
        if(num <=0 )
        {
            data.errors[i] =  "Custom Reminder Call is invalid.";
            i++;
        }
    }

    if(ReminderCustomHr_2.val() != "" && !isNaN(ReminderCustomHr_2.val()))
    {
        num = parseInt(ReminderCustomHr_2.val(), 10);
        if(num <=0 )
        {
            data.errors[i] =  "Custom Reminder Call is invalid.";
            i++;
        }
    }

    if(ReminderCustomHr_3.val() != "" && !isNaN(ReminderCustomHr_3.val()))
    {
        num = parseInt(ReminderCustomHr_3.val(), 10);
        if(num <=0 )
        {
            data.errors[i] =  "Custom Reminder Call is invalid.";
            i++;
        }
    }
    
    if(i > 0)
    {
        this.showMessages(data);
        return false;
    }    
/*	
	if (isP2TEnabled && (formType == "create" || this.didP2TChange())) {
		if (!this.acceptedP2T) {
			this.autoSubmitAfterP2TAuthorize = true;
			this.showP2TStatement();
			return false;
		}
	}
   //meo
if (ServiceTypeId.attr("value") == "") {
            alert("The Service Type is required");
            ServiceTypeId.focus();
            return false;
        }        */
	if (!this.allowSubmit)
        {
           var strtitle = this.didProjectChange();
            if (strtitle != '')
            {
                this.updateOptions(strtitle);
			return false;
        }
		}
       
	$("#PcntDeduct").attr("disabled", false);

	// end validation
	if (!isACSEnabled) {
		$("#ACSAuthBy").attr("value", "");
		$("#ACSAuthDate").attr("value", "");
	}
	$("#ReminderAll").attr("disabled", false);

	$("#MinimumSelfRating").attr("disabled", false);
	$("#MaximumAllowableDistance").attr("disabled", false);
    return (this.formType == "create") ? detailsWidget.createWO() : detailsWidget.updateDetails();
}
FSWidgetProjectDetails.prototype.confirmRemoveFile = function( obj, file_name )
{
    if ( $(obj).attr('checked') ) {
        if ( confirm('Are you sure you wish to remove the file "' + file_name + '"?') ) {
            $(obj).attr('checked', true);
        } else {
            $(obj).attr('checked', false);
        }
    }
}

FSWidgetProjectDetails.prototype.buildEventMenu = function()
{
    jQuery("#tabsHolder li").click(function(){
       var href = jQuery(this).children('a').attr("href");
       if(typeof(href) == 'undefined') return;
       var hrefOld = jQuery("#tabsHolder li a.current").attr("href");
       jQuery("#tabsHolder li").removeClass("currenttab"); 
       jQuery("#tabsHolder li a").removeClass("current");
       jQuery(this).addClass("currenttab"); 
       jQuery(this).children('a').addClass("current");
       jQuery(hrefOld).hide();
       
       jQuery(href).show();
    });
}
//380
FSWidgetProjectDetails.prototype.didProjectChange = function() {
    var titles = '';
    if(typeof(_data)!= "undefined")
    { 
        var jsons = eval("("+Base64.decode(_data)+")");
        var jsonsexpenseReporting = eval("("+Base64.decode(_dataexpenseReporting)+")");
        
        //Basic Information
        var endtimecompare = false;
        var starttimecompare = false;
        if($("#EndTime").val() != "" && jsons[$("#EndTime").attr("name")] != null)
            endtimecompare = $("#EndTime").val() != jsons[$("#EndTime").attr("name")];
        //380
        if($("#StartTime").val() != "" && jsons[$("#StartTime").attr("name")] != null)
            starttimecompare = $("#StartTime").val() != jsons[$("#StartTime").attr("name")];
        //end 380
        var StartRange = $("#StartRange").val();
        StartRange = typeof(StartRange)== 'undefined'?"":StartRange;
        var Duration = $("#Duration").val();
        Duration = typeof(Duration)== 'undefined'?"":Duration;
        //806
        //734
        //var Description = tinyMCE.get('Description').getContent();
        //var Requirements = tinyMCE.get('Requirements').getContent();
//        var SpecialInstructions = tinyMCE.get('SpecialInstructions').getContent();
        //var SpecialInstructions_AssignedTech = tinyMCE.get('SpecialInstructions_AssignedTech').getContent();
        var isdecription = $("#isdescripttion").val();
        var EstimatedDuration = $("#EstimatedDuration").val()==''?0:parseFloat($("#EstimatedDuration").val());
        var jsonEstimatedDuration = jsons[$("#EstimatedDuration").attr("name")]==null?0:parseFloat(jsons[$("#EstimatedDuration").attr("name")]);
        //end 734
        if($("#Active").attr('checked')!= jsons[$("#Active").attr("name")] ||
            $("#PO").val() != ((jsons[$("#PO").attr("name")]==null)?'':jsons[$("#PO").attr("name")]) || 
            $("#WO_Category_ID").val() != (jsons[$("#WO_Category_ID").attr("name")]==null?8:jsons[$("#WO_Category_ID").attr("name")]) ||
            $("#Headline").val() != jsons[$("#Headline").attr("name")] || 
            $("#edit_Type_ID").val() != jsons[$("#edit_Type_ID").attr("name")] || 
            this.datecompare($("#StartDate").val(),jsons[$("#StartDate").attr("name")]) ||
            starttimecompare || 
            this.datecompare($("#EndDate").val(),jsons[$("#EndDate").attr("name")]) ||
            endtimecompare || 
            StartRange != ((jsons[$("#StartRange").attr("name")]==null)?'':jsons[$("#StartRange").attr("name")]) ||
            Duration != ((jsons[$("#Duration").attr("name")]==null)?'':jsons[$("#Duration").attr("name")]) || 
            //719
            $("#StartType").val() != jsons[$("#StartType").attr("name")] ||
            $("#WorkStartTime").val() != (jsons[$("#WorkStartTime").attr("name")]==null?"":jsons[$("#WorkStartTime").attr("name")]) ||
            $("#WorkEndTime").val() != (jsons[$("#WorkEndTime").attr("name")]==null?"":jsons[$("#WorkEndTime").attr("name")]) ||
            EstimatedDuration != jsonEstimatedDuration ||
            $("#TechArrivalInstructions").val() != (jsons[$("#TechArrivalInstructions").attr("name")]==null?"":jsons[$("#TechArrivalInstructions").attr("name")]) ||
            //end 719
            //734
            //$(Description).text() != ((jsons["Description"]==null)?'':$(jsons["Description"]).text()) ||
            //$(Requirements).text() != ((jsons["Requirements"]==null)?'':$(jsons["Requirements"]).text()) || 
            //$(SpecialInstructions).text() != ((jsons["SpecialInstructions"]==null)?'':$(jsons["SpecialInstructions"]).text()) ||
            //$(SpecialInstructions_AssignedTech).text() != ((jsons["SpecialInstructions_AssignedTech"]==null)?'':$(jsons["SpecialInstructions_AssignedTech"]).text()) || 
            //end 734
            isdecription == 1 ||
            $("#General1").val() != ((jsons["General1"]==null)?'':jsons["General1"]) ||
            $("#General2").val() != ((jsons["General2"]==null)?'':jsons["General2"]) ||
            $("#General3").val() != ((jsons["General3"]==null)?'':jsons["General3"]) || 
            $("#General4").val() != ((jsons["General4"]==null)?'':jsons["General4"]) ||
            $("#General5").val() != ((jsons["General5"]==null)?'':jsons["General5"]) || 
            $("#General6").val() != ((jsons["General6"]==null)?'':jsons["General6"]) ||
            $("#General7").val() != ((jsons["General7"]==null)?'':jsons["General7"]) || 
            $("#General8").val() != ((jsons["General8"]==null)?'':jsons["General8"]) ||
            $("#General9").val() != ((jsons["General9"]==null)?'':jsons["General9"]) || 
            $("#General10").val() != ((jsons["General10"]==null)?'':jsons["General10"]))
        {
            titles += '"Basic Information", ';
        }
        
        //Push2Tech™ auto-assignment
        if(($("#isProjectAutoAssign").attr('checked')?1:0) != jsons[$("#isProjectAutoAssign").attr("name")] || 
            $("#MinutesRemainPublished").val() != (jsons[$("#MinutesRemainPublished").attr("name")]==null?'':jsons[$("#MinutesRemainPublished").attr("name")]) ||
            ($('input[name=isWorkOrdersFirstBidder]:checked').val() == "1" ? 1:0) != jsons['isWorkOrdersFirstBidder'] ||
            ($('input[name=P2TPreferredOnly]:checked').val() == "1" ? 1:0) != jsons["P2TPreferredOnly"]||
            $("#CredentialCertification").val() != ((jsons[$("#CredentialCertification").attr("name")]==null)?'0':jsons[$("#CredentialCertification").attr("name")]) || 
            //TODO: Set credential selections
			$("#MinimumSelfRating").val() != (jsons[$("#MinimumSelfRating").attr("name")]==null?0:jsons[$("#MinimumSelfRating").attr("name")]) ||
            $("#MaximumAllowableDistance").val() != (jsons[$("#MaximumAllowableDistance").attr("name")]==null?25:jsons[$("#MaximumAllowableDistance").attr("name")]) || 
            ($("#NotifyOnAutoAssign").is(':checked')?1:0)  != jsons[$("#NotifyOnAutoAssign").attr("name")] ||
            $("#P2TNotificationEmail").val() != ((jsons[$("#P2TNotificationEmail").attr("name")]==null)?'':jsons[$("#P2TNotificationEmail").attr("name")]))
        {
            titles += '"Push2Tech&trade;", ';
        }
        //Tech Pay
        if($("#Default_Amount").val() != ((jsons[$("#Default_Amount").attr("name")]==null)?'':jsons[$("#Default_Amount").attr("name")]) || 
            $("#Amount_Per").val() != ((jsons[$("#Amount_Per").attr("name")]==null)?'':jsons[$("#Amount_Per").attr("name")]) ||
            ($("#FixedBid").attr('checked')?1:0) != jsons[$("#FixedBid").attr("name")] || 
            ($("#PcntDeduct").attr('checked')?1:0) != jsons[$("#PcntDeduct").attr("name")] ||
            $("#PcntDeductPercent").val() != jsons[$("#PcntDeductPercent").attr("name")] || 
            ($("#EnableExpenseReportingTechPay").attr('checked')?1:0) != jsons[$("#EnableExpenseReportingTechPay").attr("name")] || //14091
            $("#Qty_Devices").val() != ((jsons[$("#Qty_Devices").attr("name")]==null)?'':jsons[$("#Qty_Devices").attr("name")]))
        {
            titles += '"Tech Pay", ';
        }
        var onchange = false;
        for(var j= 0; j < jsonsexpenseReporting.length; j++)
        {
            var oldbcatid = jsonsexpenseReporting[j]["bcatid"];
            var oldrequire_doc_upl = jsonsexpenseReporting[j]["require_doc_upl"];
            var oldreimbursable_thru_fs = jsonsexpenseReporting[j]["reimbursable_thru_fs"];
            var oldbcatdesc = jsonsexpenseReporting[j]["bcatdesc"];
            oldbcatdesc = oldbcatdesc==null?"":oldbcatdesc;
            jQuery(".tableContentWDSGUI tr.trPeyTech").each(function(){
                var bcatid = jQuery(this).find("input:hidden[name='WorkOrderSettingsTechPay[bcatid][]']").val();
                var bcatname = jQuery(this).find("input[name='WorkOrderSettingsTechPay[bcatname][]']").val();
                
                
                if(bcatid == 0 && jQuery.trim(bcatname) != "") 
                {
                    onchange = true;
                }
                if(bcatid == oldbcatid)
                {
                    var reimbursable_thru_fs = jQuery(this).find("input[name='WorkOrderSettingsTechPay[reimbursable_thru_fs][]']").val();
                    var require_doc_upl = jQuery(this).find("input[name='WorkOrderSettingsTechPay[require_doc_upl][]']").val();
                    var bcatdesc = jQuery(this).find("input[name='WorkOrderSettingsTechPay[bcatdesc][]']").val();
                   if(oldrequire_doc_upl != require_doc_upl || oldreimbursable_thru_fs != reimbursable_thru_fs || oldbcatdesc != bcatdesc) 
                   {
                       onchange = true;
                   }    
                   return false;
                } 
                if(onchange) return true;
            });
        }    
        
        if (onchange)
        {
            titles += '"Tech Pay", ';
        }
        
         $("WorkOrderSettingsTechPay[bcatname]").each(function(){
            var item= $(this).attr("itemid");
            if($(".reimbursable_thru_fs[itemid='"+item+"']").attr('checked'))
            {
                sum += parseFloat($(".tech_claim[itemid='"+item+"']").html());
            }
        });
        
        //Documents & Parts
        CustomSignOff = jsons["CustomSignOff"] == null?"":jsons["CustomSignOff"];
        if(typeof($("#CustomSignOff").val())=="undefined")
        {
            CustomSignOff="";
        }
        
        if($("#SignOffSheet_Required").val() != jsons[$("#SignOffSheet_Required").attr("name")] || 
            ($("#PartManagerUpdate_Required").attr('checked')?1:0) != jsons[$("#PartManagerUpdate_Required").attr("name")] ||
            $("#External_Files").val() != ((jsons[$("#External_Files").attr("name")]==null)?'':jsons[$("#External_Files").attr("name")]) ||
            (typeof($("#CustomSignOff").val())=="undefined"?"":$("#CustomSignOff").val()) != CustomSignOff ||
            $("input[name='CustomSignOff_Rm']").attr ("checked") == true)
        {
            titles += '"Documents & Parts", ';
        }
        //380
        var i =0;
        $(".indentTop5").each(function()
        {
            i+=1;
            var file = $("#prjDoc"+i).val();
            if(typeof(file)!= "undefined" && $("#prjDoc"+i).val() !='')
            {
                titles += '"Documents & Parts", ';
            }
        });
        $(".FromTech_Remove").each(function()
        {
            if($(this).attr('checked'))
            {
                titles += '"Documents & Parts", ';
            }
        });
        // end 380
        //Work Order Contacts
        if(
            ($("#Project_Manager").val() != ((jsons[$("#Project_Manager").attr("name")] == null)?'': jsons[$("#Project_Manager").attr("name")])) || 
            ($("#Project_Manager_Phone").val() != ((jsons[$("#Project_Manager_Phone").attr("name")]==null)?'':jsons[$("#Project_Manager_Phone").attr("name")])) || 
            ($("#Project_Manager_Email").val() != ((jsons[$("#Project_Manager_Email").attr("name")]==null)?'':jsons[$("#Project_Manager_Email").attr("name")])) || 
            ($("#Resource_Coordinator").val() != ((jsons[$("#Resource_Coordinator").attr("name")]==null)?'':jsons[$("#Resource_Coordinator").attr("name")])) || 
            ($("#Resource_Coord_Phone").val() != ((jsons[$("#Resource_Coord_Phone").attr("name")]==null)?'':jsons[$("#Resource_Coord_Phone").attr("name")]))|| 
            ($("#Resource_Coord_Email").val() != ((jsons[$("#Resource_Coord_Email").attr("name")]==null)?'':jsons[$("#Resource_Coord_Email").attr("name")]))|| 
            ($("#EmergencyName").val() != ((jsons[$("#EmergencyName").attr("name")]==null)?'':jsons[$("#EmergencyName").attr("name")]))|| 
            ($("#EmergencyPhone").val() != ((jsons[$("#EmergencyPhone").attr("name")]==null)?'':jsons[$("#EmergencyPhone").attr("name")]))|| 
            ($("#EmergencyEmail").val() != ((jsons[$("#EmergencyEmail").attr("name")]==null)?'':jsons[$("#EmergencyEmail").attr("name")]))|| 
            //13691
            ($("#ACSNotifiPOC").val() != ((jsons[$("#ACSNotifiPOC").attr("name")]==null)?'':jsons[$("#ACSNotifiPOC").attr("name")]))|| 
            ($("#ACSNotifiPOC_Phone").val() != ((jsons[$("#ACSNotifiPOC_Phone").attr("name")]==null)?'':jsons[$("#ACSNotifiPOC_Phone").attr("name")]))|| 
            ($("#ACSNotifiPOC_Email").val() != ((jsons[$("#ACSNotifiPOC_Email").attr("name")]==null)?'':jsons[$("#ACSNotifiPOC_Email").attr("name")]))|| 
            //End 13691
            ($("#TechnicalSupportName").val() != ((jsons[$("#TechnicalSupportName").attr("name")]==null)?'':jsons[$("#TechnicalSupportName").attr("name")])) || 
            ($("#TechnicalSupportPhone").val() != ((jsons[$("#TechnicalSupportPhone").attr("name")]==null)?'':jsons[$("#TechnicalSupportPhone").attr("name")])) || 
            ($("#TechnicalSupportEmail").val() != ((jsons[$("#TechnicalSupportEmail").attr("name")]==null)?'':jsons[$("#TechnicalSupportEmail").attr("name")]))|| 
            ($("#CheckInOutName").val() != ((jsons[$("#CheckInOutName").attr("name")]==null)?'':jsons[$("#CheckInOutName").attr("name")]))|| 
            ($("#CheckInOutNumber").val() != ((jsons[$("#CheckInOutNumber").attr("name")]==null)?'':jsons[$("#CheckInOutNumber").attr("name")])) || 
            ($("#CheckInOutEmail").val() != ((jsons[$("#CheckInOutEmail").attr("name")]==null)?'':jsons[$("#CheckInOutEmail").attr("name")])))
        {
            titles += '"Work Order Contacts", ';
        }
        //Project Auto Blast Settings
        var jsonAutoBlastOnPublish = jsons[$("#AutoBlastOnPublish").attr("name")];
        var AutoBlastOnPublish = $("#AutoBlastOnPublish").is(':checked')?"1":"0";
        if(jsonAutoBlastOnPublish==null || jsonAutoBlastOnPublish==false) jsonAutoBlastOnPublish="0";
        
        var jsonEmailBlastRadiusPreferredOnly = jsons[$("#EmailBlastRadiusPreferredOnly").attr("name")];
        var EmailBlastRadiusPreferredOnly = $("#EmailBlastRadiusPreferredOnly").is(':checked')?"1":"0";
        if(jsonEmailBlastRadiusPreferredOnly==null || jsonEmailBlastRadiusPreferredOnly==false) jsonEmailBlastRadiusPreferredOnly="0";
        
        var jsonNotifyOnBid = jsons[$("#NotifyOnBid").attr("name")];
        var NotifyOnBid = $("#NotifyOnBid").is(':checked')?"1":"0";
        if(jsonNotifyOnBid==null || jsonNotifyOnBid==false) jsonNotifyOnBid="0";
        
        var jsonNotifyOnAssign = jsons[$("#NotifyOnAssign").attr("name")];
        var NotifyOnAssign = $("#NotifyOnAssign").is(':checked')?"1":"0";
        if(jsonNotifyOnAssign==null || jsonNotifyOnAssign==false) jsonNotifyOnAssign="0";
        
        var jsonNotifyOnWorkDone = jsons[$("#NotifyOnWorkDone").attr("name")];
        var NotifyOnWorkDone = $("#NotifyOnWorkDone").is(':checked')?"1":"0";
        if(jsonNotifyOnWorkDone==null || jsonNotifyOnWorkDone==false) jsonNotifyOnWorkDone="0";
        
        //763
        var jsonReceiveWorkAcceptedByTechEmail = jsons[$("#ReceiveWorkAcceptedByTechEmail").attr("name")];
        var ReceiveWorkAcceptedByTechEmail = $("#ReceiveWorkAcceptedByTechEmail").is(':checked')?"1":"0";
        if(jsonReceiveWorkAcceptedByTechEmail==null || jsonReceiveWorkAcceptedByTechEmail==false) jsonReceiveWorkAcceptedByTechEmail="0";
        
        var jsonReceiveWorkConfirmedByTechEmail = jsons[$("#ReceiveWorkConfirmedByTechEmail").attr("name")];
        var  ReceiveWorkConfirmedByTechEmail= $("#ReceiveWorkConfirmedByTechEmail").is(':checked')?"1":"0";
        if(jsonReceiveWorkConfirmedByTechEmail==null || jsonReceiveWorkConfirmedByTechEmail==false) jsonReceiveWorkConfirmedByTechEmail="0";
        
        var jsonReceiveTechCheckedInEmail = jsons[$("#ReceiveTechCheckedInEmail").attr("name")];
        var ReceiveTechCheckedInEmail = $("#ReceiveTechCheckedInEmail").is(':checked')?"1":"0";
        if(jsonReceiveTechCheckedInEmail==null || jsonReceiveTechCheckedInEmail==false) jsonReceiveTechCheckedInEmail="0";
        
        var jsonReceiveTechCheckedOutEmail = jsons[$("#ReceiveTechCheckedOutEmail").attr("name")];
        var ReceiveTechCheckedOutEmail = $("#ReceiveTechCheckedOutEmail").is(':checked')?"1":"0";
        if(jsonReceiveTechCheckedOutEmail==null || jsonReceiveTechCheckedOutEmail==false) jsonReceiveTechCheckedOutEmail="0";
        
        //end 763
        if((
			AutoBlastOnPublish != jsonAutoBlastOnPublish || 
            ($("#EmailBlastRadius").val() != jsons[$("#EmailBlastRadius").attr("name")] && jsons[$("#EmailBlastRadius").attr("name")] != null)  || 
            EmailBlastRadiusPreferredOnly != jsonEmailBlastRadiusPreferredOnly || 
            //($("#acceptReplies").is(':checked')?1:0) != jsons[$("#acceptReplies").attr("name")] || 
            ($("#RecruitmentFromEmail").val() != jsons[$("#RecruitmentFromEmail").attr("name")] && jsons[$("#RecruitmentFromEmail").attr("name")] != null) || 
            ($("#From_Email").val() != jsons[$("#From_Email").attr("name")] && jsons[$("#From_Email").attr("name")] != null) || 
            ($("#To_Email").val() != jsons[$("#To_Email").attr("name")] && jsons[$("#To_Email").attr("name")] != null) || 
            NotifyOnBid != jsonNotifyOnBid || 
            $("#BidNotificationEmails").val() != ((jsons[$("#BidNotificationEmails").attr("name")]==null)?'':jsons[$("#BidNotificationEmails").attr("name")]) || 
            NotifyOnAssign != jsonNotifyOnAssign || 
            ($("#AssignmentNotificationEmail").val() != jsons[$("#AssignmentNotificationEmail").attr("name")] && jsons[$("#AssignmentNotificationEmail").attr("name")] != null) || 
           NotifyOnWorkDone != jsonNotifyOnWorkDone || 
            $("#WorkDoneNotificationEmails").val() != ((jsons[$("#WorkDoneNotificationEmails").attr("name")]==null)?'':jsons[$("#WorkDoneNotificationEmails").attr("name")]) ||
            //763
            ReceiveWorkAcceptedByTechEmail != jsonReceiveWorkAcceptedByTechEmail || 
            $("#WorkAcceptedByTechEmailTo").val() != ((jsons[$("#WorkAcceptedByTechEmailTo").attr("name")]==null)?'':jsons[$("#WorkAcceptedByTechEmailTo").attr("name")]) ||
            ReceiveWorkConfirmedByTechEmail != jsonReceiveWorkConfirmedByTechEmail || 
            $("#WorkConfirmedByTechEmailTo").val() != ((jsons[$("#WorkConfirmedByTechEmailTo").attr("name")]==null)?'':jsons[$("#WorkConfirmedByTechEmailTo").attr("name")]) ||
            ReceiveTechCheckedInEmail != jsonReceiveTechCheckedInEmail || 
            $("#TechCheckedInEmailTo").val() != ((jsons[$("#TechCheckedInEmailTo").attr("name")]==null)?'':jsons[$("#TechCheckedInEmailTo").attr("name")])||
            ReceiveTechCheckedOutEmail != jsonReceiveTechCheckedOutEmail || 
            $("#TechCheckedOutEmailTo").val() != ((jsons[$("#TechCheckedOutEmailTo").attr("name")]==null)?'':jsons[$("#TechCheckedOutEmailTo").attr("name")])
        //end 763
		))
        {
            titles += '"Project Auto Blast Settings", ';
        }
        //Automated Contact Services
        if($("#ReminderAll").is(':checked'))
        {
            var jsonReminderAll= jsons[$("#ReminderAll").attr("name")];
            var ReminderAll = $("#ReminderAll").is(':checked')?"1":"0";
            if(jsonReminderAll==null || jsonReminderAll==false) jsonReminderAll="0";
            if(  ReminderAll != jsonReminderAll)
            {
            titles += '"Automated Contact Services", ';
        }
        }
        else
        {
            var jsonReminderAcceptance = jsons[$("#ReminderAcceptance").attr("name")];
            var ReminderAcceptance = $("#ReminderAcceptance").is(':checked')?"1":"0";
            if(jsonReminderAcceptance==null || jsonReminderAcceptance==false) jsonReminderAcceptance="0";
            
            var jsonAutoSMSOnPublish= jsons[$("#AutoSMSOnPublish").attr("name")];
            var AutoSMSOnPublish = $("#AutoSMSOnPublish").is(':checked')?"1":"0";
            if(jsonAutoSMSOnPublish==null || jsonAutoSMSOnPublish==false) jsonAutoSMSOnPublish="0";
            
            var jsonReminder24Hr= jsons[$("#Reminder24Hr").attr("name")];
            var Reminder24Hr = $("#Reminder24Hr").is(':checked')?"1":"0";
            if(jsonReminder24Hr==null || jsonReminder24Hr==false) jsonReminder24Hr="0";
            
            var jsonReminder1Hr= jsons[$("#Reminder1Hr").attr("name")];
            var Reminder1Hr = $("#Reminder1Hr").is(':checked')?"1":"0";
            if(jsonReminder1Hr==null || jsonReminder1Hr==false) jsonReminder1Hr="0";
            
            var jsonCheckInCall= jsons[$("#CheckInCall").attr("name")];
            var CheckInCall = $("#CheckInCall").is(':checked')?"1":"0";
            if(jsonCheckInCall==null || jsonCheckInCall==false) jsonCheckInCall="0";
            
            var jsonCheckOutCall= jsons[$("#CheckOutCall").attr("name")];
            var CheckOutCall = $("#CheckOutCall").is(':checked')?"1":"0";
            if(jsonCheckOutCall==null || jsonCheckOutCall==false) jsonCheckOutCall="0";
            
            var jsonReminderNotMarkComplete= jsons[$("#ReminderNotMarkComplete").attr("name")];
            var ReminderNotMarkComplete = $("#ReminderNotMarkComplete").is(':checked')?"1":"0";
            if(jsonReminderNotMarkComplete==null || jsonReminderNotMarkComplete==false) jsonReminderNotMarkComplete="0";
            
            var jsonReminderIncomplete= jsons[$("#ReminderIncomplete").attr("name")];
            var ReminderIncomplete = $("#ReminderIncomplete").is(':checked')?"1":"0";
            if(jsonReminderIncomplete==null || jsonReminderIncomplete==false) jsonReminderIncomplete="0";

            if(ReminderAcceptance != jsonReminderAcceptance ||
                AutoSMSOnPublish != jsonAutoSMSOnPublish ||
                Reminder24Hr != jsonReminder24Hr ||
                Reminder1Hr != jsonReminder1Hr ||
                CheckInCall != jsonCheckInCall ||
                CheckOutCall != jsonCheckOutCall ||
                ReminderNotMarkComplete != jsonReminderNotMarkComplete ||
                ReminderIncomplete != jsonReminderIncomplete)
            {
               titles += '"Automated Contact Services", '; 
            }
        }
        //QuickTicket
        if($("#QuickTicketID").val() != jsons[$("#QuickTicketID").attr("name")])
        {
            titles += '"QuickTicket", ';
        }
        //DeVry
         if(
            ($("#chkDeVryperform").is(":checked") != jsons[$("#chkDeVryperform").attr("name")] && jsons[$("#chkDeVryperform").attr("name")] != null) || 
            ($("#chkDeVryobserve").is(":checked") != jsons[$("#chkDeVryobserve").attr("name")] && jsons[$("#chkDeVryobserve").attr("name")] != null)
            )
        {
            titles += '"DeVry", ';
		}
    }
    
    if(titles !='')
    {
        titles = titles.substring(0,titles.length - 2);
    }
    return titles;
    //806
}

FSWidgetProjectDetails.prototype.datecompare = function(date1, date2) 
{
    
    if((date1 != 'undefined' && date1 != '' && date1 != null) && (date2 != 'undefined' && date2 != '' && date2 != null))
    {
        var sdate=date1.split("/");
    
        var ddate=date2.split("-");
    //380
    if(sdate[2] == ddate[0] && sdate[1] == ddate[2] && sdate[0] == ddate[1] )
        return false;
    else
        return true;
    }
    else
        return false;
    //end 380
}
//end 380

FSWidgetProjectDetails.prototype.EventWorkOrderSettings = function()
{
    jQuery("#cmdAddanotherCustomLabel")
        .unbind("click",this.cmdAddanotherCustomLabel)
        .bind("click",this.cmdAddanotherCustomLabel);
    jQuery("#EnableExpenseReportingTechPay")
        .unbind("click",this.EnableExpenseReportingTechPay)
        .bind("click",this.EnableExpenseReportingTechPay);  
    this.EnableExpenseReportingTechPay(1);  
    this.EventCheckBoxList("masterCategoryAll","masterCategory");
    this.EventCheckBoxList("masterReimbursableAll","masterReimbursable");
    this.EventCheckBoxList("masterRequireDocumentAll","masterRequireDocument");
    jQuery("input:text[name='WorkOrderSettingsTechPay[bcatname][]']")
    .unbind("keyup",FSWidgetProjectDetails.prototype.onchangeWorkOrderSettingsTechPay)
    .bind("keyup",FSWidgetProjectDetails.prototype.onchangeWorkOrderSettingsTechPay);
    this.configFileUpload();
    var content = "Track and report on status of a site requiring multiple work orders in the same high-volume roll-out project. Use FieldSolutions' \"Project Site List\" to ensure Site #s are consistent.";
    $('#ESSReportingHelp').bt(content,
    {
        trigger:'click',
        positions:'top',
        fill: 'white',
        cssStyles:{
            color:'black'
		}
	});
	//13691
	 var btstr2 = " "+
		"The <span style='text-decoration: underline'>Project Manager</span> is in charge of managing the project on the FieldSolutions system. </br>" +
		"The <span style='text-decoration: underline'>Resource Coordinator</span> is considered the Project Manager's 'backup' and is not a required field. </br>" +
		"The <span style='text-decoration: underline'>ACS Notification POC</span> is the first point of contact for email and phone notifications when technicians do not respond to pre-start reminder calls or back out of a work order. Technicians do not have visibility to the ACS Notification POC phone number or email address. These fields are required for projects utilizing the ACS system. </br>" +
		"The <span style='text-decoration: underline'>Emergency Contact</span> is a technician-facing point of contact for urgent issues. </br>" +
		"Use the <span style='text-decoration: underline'>Technical Support</span> contact to provide technicians with access to an internal or external Help Desk for onsite technical issues. </br>" + 
		"If required for the project, notify technicians to call the <span style='text-decoration: underline'>Check In/Check Out</span> phone number when they arrive on site (they will still need to check in on the FieldSolutions system. </br></br>"+
		"Note that you cannot enter extensions into the Phone# field - it must be a direct line only. If an extension is required, enter the extension behind the Contact's name (Ex: Jane Doe - Ext. 123). </br>"
		;
    $('#contactcontrols').bt(btstr2,
    {

        trigger:'click',
        positions:'bottom',
        fill: 'white',
        cssStyles:{
            color:'black',width: '400px',height: '350px'
        }
    }); 
    //End 13691

    //732
    var btstr732 = "The Site Contact Reminder Call can be set to a custom number of hours before the tech is scheduled to arrive on-site.<br/>If selected, an automated phone call will be made to the Site Phone # listed in each work order, with results (confirmed / backed out) added to Section 5 of the Work Order.";
    $('#SiteContactReminderCallinfo').bt(btstr732,
    {
        trigger:'click',
        positions:'top',
        fill: 'white',
        cssStyles:
        {
            color:'black',width: '270px',height: '120px'
        }
    }); 
    // End 732
    var btstr="FieldSolutions has partnered with DeVry University to deploy the &lsquo;Technician Field Service Real-Life Internship&rsquo; as a credit-bearing program for DeVry University students. <a href='/docs/FieldSolutions_DeVry_University_Press_Release.pdf' target='blank'>Click here</a> for more program details.<br/>Simply check the box to make your Work Orders available either for observation or work under the Internship Program.";
    $('#techworkcontrols').bt(btstr,
    {
        trigger:'click',
        positions:'top',
        fill: 'white',
        cssStyles:{
            color:'black'
		}
    }); 
}

FSWidgetProjectDetails.prototype.EventCheckBoxList = function(all,item)
{
    jQuery("#"+all).unbind("click",this.checkAll).bind("click",FSWidgetProjectDetails.prototype.checkAll);
    jQuery("."+item).unbind("click",this.checkItem).bind("click",FSWidgetProjectDetails.prototype.checkItem);
    FSWidgetProjectDetails.prototype.configCheckMaster(item);
}
FSWidgetProjectDetails.prototype.checkAll = function()
{
    var itemid = jQuery(this).attr("id");
    var _id = itemid;
    itemid = itemid.replace("All","");
    var checked = jQuery(this).is(":checked");
    jQuery("."+itemid).each(function(){
        var disabled = jQuery(this).attr("disabled");
        if(!disabled)
        {
            var ischeckCustem = true;
            if(_id == "masterCategoryAll"){
                var el = jQuery(this).parent()
                    .children("input[name='WorkOrderSettingsTechPay[bcatname][]']");
                if(jQuery.trim(jQuery(el).val()) == ""
                    && jQuery(el).attr("type") == "text")
            {
                    ischeckCustem = false;
                }
            }

            if(checked && ischeckCustem)
            {
                jQuery(this).attr("checked",true);
                jQuery(this).parent().children("input:hidden[checked='true']").val("1");
            }
            else
            {
                jQuery(this).removeAttr("checked");
                jQuery(this).parent().children("input:hidden[checked='true']").val("0");
            }  
        }
    });
    FSWidgetProjectDetails.prototype.configCheckMaster(itemid,true);
}
FSWidgetProjectDetails.prototype.checkItem = function()
{
    var item = jQuery(this).attr("class");
    FSWidgetProjectDetails.prototype.configCheckMaster(item);
    //376
    if(item == "masterCategory" && jQuery(this).is(":checked"))
    {
        jQuery(this).parent().parent().find(".masterReimbursable").attr("checked","checked");
        jQuery(this).parent().parent().find("input:hidden[name='WorkOrderSettingsTechPay[reimbursable_thru_fs][]']").val("1");
    }
    if(item == "masterCategory"){
        var checked = jQuery(this).is(":checked");
        jQuery(this).parent().parent().find("input").each(function(){
            if(jQuery(this).attr('class') != "masterCategory") 
            {
                if((jQuery(this).attr('type') == "text" || jQuery(this).attr('type') == "checkbox")
                    && !checked)
                {
                    if(jQuery(this).attr('type') == "text")
                        jQuery(this).attr("readonly",true); 
                    else
                        jQuery(this).attr("disabled","disabled");
                } 
                else 
                {
                    jQuery(this).removeAttr("disabled");
                    jQuery(this).removeAttr("readonly");
                    if(jQuery(this).attr("class") != "masterRequireDocument" &&
						jQuery(this).attr("class") != "masterReimbursable")
                        jQuery(this).attr("checked",true);
                } 
            }
        });
    }
    //end 376
    if(jQuery(this).is(":checked"))
    {
        
        if(jQuery(this).parent().attr("class")== "label_field")
        {
            jQuery(this).parent().children("input:hidden").each(function()
            {
                if($(this).attr('name') == 'WorkOrderSettingsTechPay[ischecked][]')
                    $(this).val("1")
            });
    }  
    else
    {
            jQuery(this).parent().children("input:hidden[checked=true]").val("1");
    }    
}
    else
    {
        if(jQuery(this).parent().attr("class")== "label_field")
        {
            jQuery(this).parent().children("input:hidden").each(function()
            {
                if($(this).attr('name') == 'WorkOrderSettingsTechPay[ischecked][]')
                    $(this).val("0")
            });
        }
        else
        {
            jQuery(this).parent().children("input:hidden[checked=true]").val("0");
        }
    }
    //430
}
FSWidgetProjectDetails.prototype.configCheckMaster = function(item,all)
{
    var itemChecked = 0;
    var itemDisabled = 0;
    jQuery("."+item).each(function(){
        var checked = jQuery(this).is(":checked");
        var el = jQuery(this).parent().parent();
        var disabled = jQuery(this).attr("disabled");
        var isCustomer = false;
        if(item == "masterCategory")
        {
            var _el = jQuery(this).parent()
                    .children("input[name='WorkOrderSettingsTechPay[bcatname][]']");
                if(jQuery.trim(jQuery(_el).val()) == ""
                    && jQuery(_el).attr("type") == "text")
                {
                    isCustomer = true;
                }
        }    
        if((checked && !disabled) || isCustomer)
        {
            itemChecked++;
        } 
        if(disabled)
        {
            itemDisabled++;
        }    
        if(item == "masterCategory" && jQuery("#EnableExpenseReportingTechPay").is(":checked") && all)
        {
            jQuery(el).find("input").each(function(){
                if((jQuery(this).attr('type') == "text" || jQuery(this).attr('type') == "checkbox")
                    && jQuery(this).attr('class') != "masterCategory" && !checked)
                {
                    if(jQuery(this).attr('type') == "text")
                        jQuery(this).attr("readonly",true); 
                    else
                        jQuery(this).attr("disabled","disabled");
                } 
                else if(checked)
                {
                    jQuery(this).removeAttr("disabled");
                    jQuery(this).removeAttr("readonly");
                    if(jQuery(this).attr("class") != "masterRequireDocument" &&
						jQuery(this).attr("class") != "masterReimbursable")
                    jQuery(this).attr("checked",true);
                }    
            });
        }    
    });
    if(item == "masterCategory" && jQuery("#EnableExpenseReportingTechPay").is(":checked"))
    {    
        if(itemChecked == 0)
        {
            jQuery("#masterReimbursableAll").attr("disabled","disabled");
            jQuery("#masterRequireDocumentAll").attr("disabled","disabled");
        } 
        else
        {
            jQuery("#masterReimbursableAll").removeAttr("disabled");
            jQuery("#masterRequireDocumentAll").removeAttr("disabled");
        }
        FSWidgetProjectDetails.prototype.configCheckMaster("masterReimbursable");
        FSWidgetProjectDetails.prototype.configCheckMaster("masterRequireDocument");
    }
    if(itemChecked + itemDisabled == jQuery("."+item).length && itemChecked > 0)
    {
        jQuery("#"+item+"All").attr("checked",true);
    } 
    else
    {
        jQuery("#"+item+"All").attr("checked",false);
    }   
}
FSWidgetProjectDetails.prototype.EnableExpenseReportingTechPay = function(fb)
{
    var hname = jQuery("#EnableExpenseReportingTechPay").attr("hname");
    var hphone = jQuery("#EnableExpenseReportingTechPay").attr("hphone");
    var hemail = jQuery("#EnableExpenseReportingTechPay").attr("hemail");
    var data = {
        success:0,
        errors:['<div style="color:black;font-weight:normal;">For more information about Expense Reporting, contact your Account Manager <strong>'+hname+'</strong> at <strong>'+hphone+'</strong> or <strong>'+hemail+'</strong>.</div>']};
    var IsPM = jQuery("#EnableExpenseReportingTechPay").attr("ispm");

    if(IsPM!=1 && fb != 1)
    {
        if(jQuery("#EnableExpenseReportingTechPay").is(":checked"))
            FSWidgetProjectDetails.prototype.showMessages(data);
    }
        
    var checked = jQuery("#EnableExpenseReportingTechPay").is(":checked");
    
    jQuery("#WorkOrderSettingsTechPay input").each(function() {
        var id = jQuery(this).attr("id");
        
        if(id != "EnableExpenseReportingTechPay" 
            && id != "WorkOrderSettingsTechPayCancel"
            && id != "WorkOrderSettingsTechPaySubmit"
            && id != 'action')
        {    
//            if(checked)
//                jQuery(this).removeAttr("disabled");
//            else
//                jQuery(this).attr("disabled","disabled");
            //376
            if(checked)
            {
                if(jQuery(this).attr('type') == 'checkbox')
                    jQuery(this).removeAttr("disabled");
                else
                    jQuery(this).removeAttr("readonly"); 
            } 
            else if(jQuery(this).attr("class")!="masterCategorychecked")
                if(jQuery(this).attr('type') == 'checkbox' )
                    jQuery(this).attr("disabled","disabled");
                else
                    jQuery(this).attr("readonly","true");
            }
            //end 376
    });
    if(checked)
    {
        jQuery("#cmdAddanotherCustomLabel")
            .unbind("click",FSWidgetProjectDetails.prototype.cmdAddanotherCustomLabel)
            .bind("click",FSWidgetProjectDetails.prototype.cmdAddanotherCustomLabel);
       FSWidgetProjectDetails.prototype.configCheckMaster("masterCategory",true);
    }
    else
        jQuery("#cmdAddanotherCustomLabel").unbind("click");
}
FSWidgetProjectDetails.prototype.cmdAddanotherCustomLabel = function()
{
//430
    var html = '<tr class="trPeyTech">\
                        <td  style="padding-bottom: 2px; padding-top: 2px;">\
                            <input type="checkbox" value="1"  class="masterCategory" />&nbsp;\
                            <input type="text" style="background-image:url(\'/widgets/css/images/CustomCategory.png\')" name="WorkOrderSettingsTechPay[bcatname][]" readonly/>\
                            <input type="hidden" checked="true" value="0" name="WorkOrderSettingsTechPay[ischecked][]" />\
                            <input type="hidden" value="0" name="WorkOrderSettingsTechPay[bcatid][]" />\
                            <input type="hidden" value="custom" name="WorkOrderSettingsTechPay[bcattype][]" />\
                        </td>\
                        <td>\
                        <input name="WorkOrderSettingsTechPay[bcatdesc][]" type="text" maxlength="20" readonly/>\
                        </td>\
                        <td >\
                            <input type="checkbox" value="1"  class="masterReimbursable" disabled/>\
                            <input type="hidden" checked="true" value="0" name="WorkOrderSettingsTechPay[reimbursable_thru_fs][]"/>\
                        </td>\
                        <td>\
                            <input type="checkbox" value="1"   class="masterRequireDocument" disabled/>\
                            <input type="hidden" checked="true" value="0" name="WorkOrderSettingsTechPay[require_doc_upl][]"  />\
                        </td>\
                    </tr>';
    jQuery("#WorkOrderSettingsTechPay .tableContentWDSGUI tbody")
        .append(html);
    FSWidgetProjectDetails.prototype.EventCheckBoxList("masterCategoryAll","masterCategory");
    FSWidgetProjectDetails.prototype.EventCheckBoxList("masterReimbursableAll","masterReimbursable");
    FSWidgetProjectDetails.prototype.EventCheckBoxList("masterRequireDocumentAll","masterRequireDocument");  
    jQuery("input:text[name='WorkOrderSettingsTechPay[bcatname][]']")
    .unbind("keyup",FSWidgetProjectDetails.prototype.onchangeWorkOrderSettingsTechPay)
    .bind("keyup",FSWidgetProjectDetails.prototype.onchangeWorkOrderSettingsTechPay);
}
FSWidgetProjectDetails.prototype.onchangeWorkOrderSettingsTechPay = function()
{
    var val = jQuery(this).val();
    if(val == "")
    {
        jQuery(this).css("background-image","url('/widgets/css/images/CustomCategory.png'");
    }
    else
    {
        jQuery(this).css("background-image","");
    }    
}
FSWidgetProjectDetails.prototype.configFileUpload = function(){
    if(!jQuery.browser.mozilla && !jQuery.browser.msie) return false;
    var btnBrowse = jQuery('.divUpload');
    if(btnBrowse.length > 0){
        btnBrowse.each(function(pos, _btn){
            jQuery(_btn).parent().append('<div class="jQUpload" style="width:292px;"><input readonly="true" type="text" value="" style="vertical-align: middle;width:220px;" /> <img align="absmiddle" src="/widgets/images/Btn_0.png" /></div>');
            var btn = jQuery(_btn).parent().find('.jQUpload');
            //var wi = jQuery(btn).children("input").width()+jQuery(btn).children("img").width();
            //jQuery(btn).width(wi+15);
            jQuery(_btn).hover(
                function () {
                    $(btn).children("img").attr("src","/widgets/images/Btn_M.png");
                }, 
                function () {
                    $(btn).children("img").attr("src","/widgets/images/Btn_0.png");
                }
            );

            var divInput = jQuery(_btn);
            var coords = jQuery(btn).position();		
            divInput.css({
                'overflow': 'hidden',				
                'width': jQuery(btn).outerWidth()+68,
                'height': jQuery(btn).outerHeight(),
                'position': 'absolute',
                'top': coords.top - 24,
                'left': coords.left				
            });
            divInput.find(':first').css({
                'font-size': '50px',
                'position': 'absolute',					
                'height': jQuery(btn).outerHeight(),
                'opacity': 0.01,
                'top': 0,
                'right': 0,
                'margin': 0
            }).bind('change', function(){
                if(jQuery(btn).parent().find('input[type="text"]').length > 0){
                    jQuery(btn).parent().find('input[type="text"]').val(jQuery(this).val());
                }
            });			
            jQuery(btn).data('divInput',divInput);			
            jQuery(btn).after(divInput);
            jQuery(btn).css("display","block");
            jQuery(btn).click(function(){
                jQuery(divInput).children("input:file").click(); 
            });
            
            jQuery('.divUpload').height(0);
        });
    }
}

FSWidgetProjectDetails.prototype.updateSignOffDisplay = function () {
	var signoff_enabled = jQuery("#SignOff_Disabled").attr ("checked");
	var custom_enabled = (jQuery("input[name=CustomSignOff_Enabled]:checked").val() == "1");

	if (signoff_enabled && custom_enabled) {
		jQuery("input[name=CustomSignOff]").show ();
	}
	else {
		jQuery("input[name=CustomSignOff]").hide ();
	}

	if (signoff_enabled) {
		jQuery(".signoffoptions_display").show ();
		jQuery("input[name=CustomSignOff_Enabled]").removeAttr ("disabled");
	}
	else {
		jQuery("input[name=CustomSignOff_Enabled]").attr ("disabled","disabled");
		jQuery(".signoffoptions_display").hide ();
	}
};

FSWidgetProjectDetails.prototype.enableAutoAssign = function (is_enabled) {
	if (is_enabled) {
		if (this._enablePushWM) jQuery ("#PushWM").removeAttr ("disabled");
		jQuery ("#isWorkOrdersFirstBidder_1").removeAttr ("disabled");
		jQuery ("#isWorkOrdersFirstBidder_0").removeAttr ("disabled");
		jQuery ("#P2TPreferredOnly_1").removeAttr ("disabled");
		jQuery ("#P2TPreferredOnly_0").removeAttr ("disabled");
		jQuery ("#CredentialCertification").removeAttr ("disabled");
		jQuery ("#PublicCertification").removeAttr ("disabled");
		jQuery ("#FSExpert").removeAttr ("disabled");
		jQuery ("#IndustryCertification").removeAttr ("disabled");
		jQuery ("#MinimumSelfRating").removeAttr ("disabled");
		jQuery ("#MaximumAllowableDistance").removeAttr ("disabled");
		jQuery ("#NotifyOnAutoAssign").removeAttr ("disabled");
		jQuery ("#P2TNotificationEmail").removeAttr ("disabled");
	}
	else {
		jQuery ("#PushWM").attr ('disabled','disabled');
		jQuery ("#isWorkOrdersFirstBidder_1").attr ('disabled','disabled');
		jQuery ("#isWorkOrdersFirstBidder_0").attr ('disabled','disabled');
		jQuery ("#P2TPreferredOnly_1").attr ('disabled','disabled');
		jQuery ("#P2TPreferredOnly_0").attr ('disabled','disabled');
		jQuery ("#CredentialCertification").attr ('disabled','disabled');
		jQuery ("#PublicCertification").attr ('disabled','disabled');
		jQuery ("#FSExpert").attr ('disabled','disabled');
		jQuery ("#IndustryCertification").attr ('disabled','disabled');
		jQuery ("#MinimumSelfRating").attr ('disabled','disabled');
		jQuery ("#MaximumAllowableDistance").attr ('disabled','disabled');
		jQuery ("#NotifyOnAutoAssign").attr ('disabled','disabled');
		jQuery ("#P2TNotificationEmail").attr ('disabled','disabled');
	}
};

FSWidgetProjectDetails.prototype.enableClientPushWM = function (is_enabled) {
	if (is_enabled == "0") {
		this._enablePushWM = false;
	}
	else if (is_enabled == "1") {
		this._enablePushWM = true;
	}
	else {
		this._enablePushWM = !!is_enabled;
	}
};

