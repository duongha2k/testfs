function ProgressBarAdapterLine( provider, container ) {
    ProgressBarAdapterAbstract.call(this, provider, container);

    this.bar = document.createElement('div');

    /* Options dependent on image and bar  */
    this.barWidth  = 300;   //  px
    this.barHeight = 26;    //  px
    this.imgWidth  = 1498;  //  px
    this.imgHeight = 24;    //  px
}

ProgressBarAdapterLine.prototype = new ProgressBarAdapterAbstract;
ProgressBarAdapterLine.prototype.constructor = ProgressBarAdapterLine;

ProgressBarAdapterLine.NAME         = "ProgressBarAdapterLine";
ProgressBarAdapterLine.VERSION      = "0.1";
ProgressBarAdapterLine.DESCRIPTION  = "Line progress bar";

ProgressBarAdapterLine.prototype.show = function ()
{
    if ( this._container === null || this._container === undefined ) return;

    //console.log('show');
    $(this.bar).css('display', '');
    if ( this.provider().label() != this.label() ) {
        this.label(this.provider().label());
        this.percent(this.provider().percent());

        $(this.bar).text(this.label());

        var pos = -1 * this.imgWidth + Math.ceil(this.barWidth * this.percent() / 100);
        $(this.bar).css('background-position', pos+'px 50%');
    }
}
ProgressBarAdapterLine.prototype.hide = function ()
{
    //console.log('hide');
    $(this.bar).text('');
    $(this.bar).css('display', 'none');
    this.label('');
    this.percent('');
}
ProgressBarAdapterLine.prototype.prepare = function ()
{
    if ( this._container === null || this._container === undefined ) return;

    //console.log('prepare');
    $(this.bar).css('width', this.barWidth+'px');
    $(this.bar).css('height', this.barHeight+'px');
    $(this.bar).css('line-height', this.barHeight+'px');
    $(this.bar).css('background-image', "url('/widgets/images/processbar/linesolid/linebody.png')");
    $(this.bar).css('background-position', '-'+this.imgWidth+'px 50%');  //  dependent on image !!!
    $(this.bar).css('background-repeat', 'no-repeat');
    $(this.bar).css('display', 'none');
    $(this.bar).css('text-align', 'center');
    this.container().appendChild(this.bar);
}
ProgressBarAdapterLine.prototype.postfree = function ()
{
    if ( this._container === null || this._container === undefined ) return;

    //console.log('postfree');
    $(this._container).html('');
}

