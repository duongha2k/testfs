/**
 * FSWidgetDashboard
 *
 * @param Object|nul
 * @access public
 * @return void
 */
function FSWidgetDashboard( params ) {
    FSWidget.call(this, params);

    this.params.WIDGET.avaliableTabs = {
        created         : {
            module:'dashboard',
            controller:'index',
            action:'created'
        },
        published       : {
            module:'dashboard',
            controller:'index',
            action:'published'
        },
        assigned        : {
            module:'dashboard',
            controller:'index',
            action:'assigned'
        },
        quickview        : {
            module:'dashboard',
            controller:'customer',
            action:'quickview'
        },
        approved        : {
            module:'dashboard',
            controller:'index',
            action:'approved'
        },
        inaccounting    : {
            module:'dashboard',
            controller:'index',
            action:'inaccounting'
        },
        incomplete      : {
            module:'dashboard',
            controller:'index',
            action:'incomplete'
        },
        completed       : {
            module:'dashboard',
            controller:'index',
            action:'completed'
        },
        all             : {
            module:'dashboard',
            controller:'index',
            action:'all'
        },
        deactivated     : {
            module:'dashboard',
            controller:'index',
            action:'deactivated'
        }, 
        //https://dev.fieldsolutions.com/widgets/dashboard/customer/quickview/
        quickview 		: {
            module:'dashboard', 
            controller:'customer', 
            action:'quickview'
        },
        customeredit    :
        {
            module      :'dashboard',
            controller  :'customer',
            action      :'update'
        },
        configurator       :
        {
            module      :'dashboard',
            controller  :'customer',
            action      :'Configurator'
        },
        quickviewopen   :
        {
            module      :'dashboard',
            controller  :'customer',
            action      :'quickview'
        },
        quickviewclose  :
        {
            module      :'dashboard',
            controller  :'customer',
            action      :'quickview'
        }
        
    };

    this.params.POPUP.avaliablePopups = {
woinformation  :
        {
            module      :'dashboard', 
            controller  :'customer', 
            action      :'section1'
        },
        starttime       :
        {
            module      :'dashboard',
            controller  :'customer',
            action      :'start-time'
        },
        techinfo        :
        {
            module      :'dashboard',
            controller  :'customer',
            action      :'tech-info'
        },
        techcomment     :
        {
            module      :'dashboard',
            controller  :'customer',
            action      :'techcomment'
        },
        documents       :
        {
            module      :'dashboard',
            controller  :'customer',
            action      :'tech-documents'
        },
        wostage         :
        {
            module      :'dashboard',
            controller  :'customer',
            action      :'wostage'
        }
    }
	
	try {
		this._saveStateCallback = params.saveStateCallback ? params.saveStateCallback : function() {};
	} catch (e) {}
}

FSWidgetDashboard.prototype = new FSWidget();
FSWidgetDashboard.prototype.constructor = FSWidgetDashboard;

FSWidgetDashboard.NAME          = 'FSWidgetDashboard';
FSWidgetDashboard.VERSION       = '0.1';
FSWidgetDashboard.DESCRIPTION   = 'Class FSWidgetDashboard';

FSWidgetDashboard.prototype.resetFormIE = function (roll)
{
    roll.body($(this.filters().container()).html());
    calendarInit();
}
/**
 *  htmlFilters
 *
 *  Function return html for filters popup
 *
 *  return string
 */


FSWidgetDashboard.prototype.htmlFilters = function () {
    if ( !this.filters().isInitialized() ) {
        var _url;
        try {
            _url = this._makeUrl('filters', '/tab/'+this.currentTab+'/company/'+this.filters().company());
        } catch ( e ) {
            return false;
        }
        if(typeof(this.params.ajaxSuccess) == 'function')
        {
            this.params.ajaxSuccess();
        }
        //this.params.ajaxSuccess = null;
        jQuery.ajax({
            url : _url,
            cache : false,
            type : 'POST',
            dataType : 'html',
            context : this,
            success : function(data, status, xhr) {
                if(typeof(this.params.ajaxSuccess) == 'function')
                {
                    var _el = this;
                    var fun = function(_el,data)
                    {
                        if ( _el.filters().container() === null )
                            _el.filters()._makeContainer();
                        $(_el.filters().container()).html(data);
                        _el.filters().setInitialized();
                    };
                    _el.params.ajaxSuccess(fun,_el,data);
                }
                else
                {
                if ( this.filters().container() === null )
                    this.filters()._makeContainer();
                $(this.filters().container()).html(data);
                this.filters().setInitialized();
                }
                
              
            },
            error : function (xhr, status, error) {
                if ( xhr.responseText === "authentication required" ) {
                    //document.location = "/clients/logIn.php";
                }
            }
        });
    }
}

//FSWidgetDashboard.prototype.ajaxSuccess = function()
//{
//    
//}
/**
 *  resetFilters
 *
 *  @param FSPopupAbstract roll
 *  @return void
 */
FSWidgetDashboard.prototype.resetFilters = function (roll)
{
    this.filters().techId(null);
    this.filters().dateStart('');
    this.filters().dateEnd('');
    this.filters().zip('');
    this.filters().state('');
    this.filters().country('');

    this.filters().username('');
    this.filters().headline('');
    this.filters().techEmail('');
    this.filters().woCategory('');
    this.filters().dateEnteredFrom('');
    this.filters().dateEnteredTo('');
    this.filters().dateApprovedFrom('');
    this.filters().dateApprovedTo('');
    this.filters().dateInvoicedFrom('');
    this.filters().dateInvoicedTo('');
    this.filters().datePaidFrom('');
    this.filters().datePaidTo('');
    this.filters().payMaxFrom(null);
    this.filters().payMaxTo(null);
    this.filters().netPayAmountFrom(null);
    this.filters().netPayAmountTo(null);
    this.filters().tbPay(null);

    this.filters().setProject([]);
    this.filters().tech('');
    this.filters().contractor('');
    this.filters().region('');

    this.filters().fullSearch(false);
    this.filters().win(null);
    this.filters().clientWO(null);       
    this.filters().po('');
    this.filters().showToTech(null);
    this.filters().siteName(null);
    this.filters().siteNumber(null);
    this.filters().city(null);
    this.filters().updateRequested(null);
    this.filters().storeNotified(null);
    this.filters().checkedIn(null);
    this.filters().checkedOut(null);
    this.filters().reviewedWO(null);
    this.filters().timeZone(null);
    this.filters().techConfirmed(null);
    this.filters().techComplete(null);
    this.filters().techCalled(null);
    this.filters().siteComplete(null);
    this.filters().approved(null);
    this.filters().extraPay(null);
    this.filters().outOfScope(null);
    this.filters().techPaid(null);
    this.filters().techFName(null);
    this.filters().techLName(null);
    this.filters().showSourced(null);
    this.filters().showNotSourced(null);
    this.filters().paperReceived(null);
    this.filters().lead(null);
    this.filters().assist(null);
    this.filters().deactivated(null);
    this.filters().shortNotice(null);
    this.filters().flsId(null);
    this.filters().distance(null);
    this.filters().pricingRan(null);
    this.filters().invoiced(null);
    this.filters().pcntDeduct(null);
    this.filters().deactivatedReason(null);
    this.filters().satrecommendedFrom(null);
    this.filters().satperformanceFrom(null);
    this.filters().showToTech(null);
    
    this.filters().deactivatedReason(null);
    this.filters().deactivationCode(null);
    
    this.filters().Route("");
}
/**
 *  applyFilters
 *
 *  @return void
 */
FSWidgetDashboard.prototype.applyFilters = function (roll) {
    this.params.isBid = 0;
    FSWidget.filterActivate(false);

    var i, stateSelect, callSelect;

    var projects = $('#' + roll.container().id + ' input[name="filterProjects[]"]').serializeArray();
    var _p = [];
    for ( i=0; i < projects.length; ++i ) {
        _p.push( projects[i].value );
    }
    this.filters().setProject(_p);

    this.filters().dateStart($('#' + roll.container().id + ' #filterStartDate')[0].value);
    this.filters().dateEnd($('#' + roll.container().id + ' #filterEndDate')[0].value);
    this.filters().zip($('#' + roll.container().id + ' #filterZip')[0].value);
    this.filters().po($('#' + roll.container().id + ' #filterPO')[0].value);

    stateSelect = $('#'+roll.container().id+' #filterState')[0];
    for ( i=0; i< stateSelect.options.length; ++i ) {
        if ( stateSelect.options[i].selected ) {
            this.filters().state($(stateSelect.options[i]).val());
            break;
        }
    }

    countrySelect = $('#'+roll.container().id+' #filterCountry')[0];
    for ( i=0; i< countrySelect.options.length; ++i ) {
        if ( countrySelect.options[i].selected ) {
            this.filters().country($(countrySelect.options[i]).val());
            break;
        }
    }

    callSelect = $('#'+roll.container().id+' #filterCallType')[0];
    for ( i=0; i< callSelect.options.length; ++i ) {
        if ( callSelect.options[i].selected ) {
            this.filters().callType(callSelect.options[i].value);
            break;
        }
    }

    this.filters().tech($('#' + roll.container().id + ' #filterAssignedTech')[0].value);
    this.filters().region($('#' + roll.container().id + ' #filterRegion')[0].value);

    this.filters().Route($('#' + roll.container().id + ' #filterRoute').val());
    
    roll.hide();

    var p = this.getParams();
    this.show({
        tab:this.currentTab,
        params:p
    });
}
/**
 *  prepareFilters
 */
FSWidgetDashboard.prototype.prepareFilters = function(roll, params) {

    FSWidget.filterActivate(true);

    var i, stateSelect, callSelect, progectsCheckboxes;

    progectsCheckboxes = $('#' + roll.container().id + ' input[name="filterProjects[]"]');
    for ( i=0; i < progectsCheckboxes.length; ++i ) {
        if ( $.inArray(progectsCheckboxes[i].value, this.filters().getProjects()) > -1 ) {
            progectsCheckboxes[i].checked = true;
        } else {
            progectsCheckboxes[i].checked = false;
        }
    }
    stateSelect = $('#' + roll.container().id + ' #filterState')[0];
    for ( i=0; i < stateSelect.options.length; ++i ) {
        if ( stateSelect.options[i].value == this.filters().state() ) {
            stateSelect.options[i].selected = true;
            break;
        }
    }

    countrySelect = $('#' + roll.container().id + ' #filterCountry')[0];
    for ( i=0; i < countrySelect.options.length; ++i ) {
        if ( countrySelect.options[i].value == this.filters().country() ) {
            countrySelect.options[i].selected = true;
            break;
        }
    }
	
	$('#' + roll.container().id + ' #filterCountry').change();

    $('#' + roll.container().id + ' #filterZip')[0].value = this.filters().zip();
    $('#' + roll.container().id + ' #filterStartDate')[0].value = this.filters().dateStart();
    $('#' + roll.container().id + ' #filterEndDate')[0].value = this.filters().dateEnd();
    $('#' + roll.container().id + ' #filterAssignedTech')[0].value = this.filters().tech();
    $('#' + roll.container().id + ' #filterRegion')[0].value = this.filters().region();
    $('#' + roll.container().id + ' #filterRoute')[0].value = this.filters().Route();
    $('#' + roll.container().id + ' #filterPO')[0].value = this.filters().po();

    callSelect = $('#' + roll.container().id + ' #filterCallType')[0];
    for ( i=0; i < callSelect.options.length; ++i ) {
        if ( callSelect.options[i].value == this.filters().callType() ) {
            callSelect.options[i].selected = true;
            break;
        }
    }

    //VALIDATORS
	var vZIP = new LiveValidation('filterZip',LVDefaults);
    vZIP.add( Validate.Format, {
        pattern: /^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$/
    } );
	var vTID = new LiveValidation('filterAssignedTech',LVDefaults);
    vTID.add( Validate.Numericality, {
        onlyInteger: true
    } );
}
/**
 *  showPreloader
 *
 *  Function displays widget preloader
 *
 *  @param Object|null options
 */
FSWidgetDashboard.prototype.showPreloader = function() {
    $('body').css('cursor', 'wait');
    this.waitImage = new Image();
    this.waitImage.src = "/widgets/images/wait.gif";
    var img  = '<table width="100%" class="gradBox_table_inner">';
        img += '<tr class="table_head"><th>&#160;<br />&#160;</th></tr>';
        img += '<tr><td>&#160<br />&#160;</td></tr>';
        img += '<tr>';
        img += "<td><img style='margin:0 40%;' alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></td>";
        img += '</tr>';
        img += '<tr"><td>&#160<br />&#160;</td></tr>';
        img += '<tr class="paginator"><td class="tCenter">&#160;</td></tr>';
        img += '</table>';
    FSWidget.fillContainer(img, this);
}
/**
 *  show
 *
 *  Function displays widget
 *
 *  @param Object|null options
 */
FSWidgetDashboard.prototype.show = function(options) {
    if ( FSWidget.isFilterActive() ) return;

    var p = options || {
        tab:this.currentTab,
        params:this.getParams()
    };
    var queryString = '/';
    var _url;

    // TODO params needs be reconfigured
    if ( p.params !== undefined && p.params !== null && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params ) {

            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] === true || p.params[key] === false ) {
                if ( p.params[key] === false ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] !== null && p.params[key] !== "" && ( (typeof(p.params[key]) == 'string' && p.params[key].toLowerCase() !== 'none') || typeof(p.params[key]) == 'number') ) {
                //queryString += key + '/' + p.params[key] + '/';
            } else {
                delete p.params[key];
            }
        }
    } else {
        p.params = {};
    }

    try {
        if ( p.tab !== undefined && p.tab !== null ) {
            this.currentTab = p.tab;
            _url = this._makeUrl(p.tab, queryString);
            var idx;
            for ( idx in this.params.WIDGET.avaliableTabs ) {
                $('#'+idx).toggleClass('current', idx === this.currentTab);
            }
        } else if ( p.popup !== undefined && p.popup !== null ) {
            _url = this._makeUrl(p.popup, queryString);
        }
    } catch ( e ) {
        return false;
    }
    
    if (this.sortTool() !== null ) {
    	if (typeof(p.params.page) == undefined ) {
    		this.sortTool().setPage(1);
    	} else {
    		this.sortTool().setPage(p.params.page);
    	}
    }

    this.showPreloader();

    if ( this.tabOpenXhrRequest ) {
        this.tabOpenXhrRequest.abort();
        this.tabOpenXhrRequest = null;
    }

    version = $('#dashboardVersion').val();
    if(version != '' && version == 'Lite'){
	version = "Lite";
        p.params.version = version;
        $('#deactivated').addClass("displayNone");
    } else {
        version = "Full";
//        var defVer = FSWidget.getCookie("dashboardVersionDefault");
        p.params.version = version;
        $('#deactivated').removeClass("displayNone");
    }

    $('#dashboardVersion').val(version);
    FSWidget.saveCookie("dashboardVersionDefault", version);
    
    // get page size
    if ($('#pageSizeSelector') ) {
    	p.params.size = $('#pageSizeSelector').val();
    }
    
    var TechBid = 0;
    if(typeof(this.params.isBid) != 'undefined')
    {
       TechBid = this.params.isBid;   
    }
    p.params.TechBid = TechBid;
    //this.sortTool().prepareItems(version);
    //this.sortTool().initSortTool();
    //alert(this.params.ajaxSuccess);

    this.tabOpenXhrRequest = jQuery.ajax({
        url : _url,
        data : p.params,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) {
                //document.location = "/clients/logIn.php";
            } else {
                FSWidget.fillContainer("Sorry, but an error occurred. No results found.", this);
            }
            this.tabOpenXhrRequest = null;
        },
        success : function(data, status, xhr) {
            if(typeof(this.params.ajaxSuccess) == 'function')
            {
                var _el = this;
                var fun = function(_el,data)
                {
            $('body').css('cursor', '');
                    FSWidget.fillContainer(data, _el);
                    this.tabOpenXhrRequest = null;
                    if (this.currentTab == 'assigned') {
                        $(".rePublishCheckbox").click(function() {
                            var id = $(_el).attr('id');
                            var matches = /^rePublishCheckbox_(\d+)$/.exec(id);
                            var win = matches[1];

                            if ($(_el).attr('checked')) {
                                $("#rePublishOptions_" + win).removeClass('displayNone');
                            } else {
                                $("#rePublishOptions_" + win).addClass('displayNone');
                            }
                        });
                    }
                    _el.resizeFrame();
                    _el._saveStateCallback();
                };
                this.params.ajaxSuccess(fun,_el,data);
            }
            else
            {
                $('body').css('cursor', '');
            FSWidget.fillContainer(data, this);
            this.tabOpenXhrRequest = null;
            if (this.currentTab == 'assigned') {
                $(".rePublishCheckbox").click(function() {
                    var id = $(this).attr('id');
                    var matches = /^rePublishCheckbox_(\d+)$/.exec(id);
                    var win = matches[1];

                    if ($(this).attr('checked')) {
                        $("#rePublishOptions_" + win).removeClass('displayNone');
                    } else {
                        $("#rePublishOptions_" + win).addClass('displayNone');
                    }
                });
            }
			this.resizeFrame();
			this._saveStateCallback();
        }
            
        }
    });
}
/**
 *  open
 */
FSWidgetDashboard.prototype.open = function(options) {
    var p = options || {};
    var queryString = '/';
    var _url;
    var noSSL = false;

    if ( p.params !== undefined && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params )
            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                } else if ( key === 'projects' ) {
                    var ids = '';
                    for ( var pr=0; pr < p.params[key].length; ++pr ) {
                        ids += p.params[key][pr];
                        if ( pr + 1 < p.params[key].length ) {
                            ids += '|';
                        }
                    }
                    queryString += 'projects/' + ids + '/';
                }
            } else if (p.params[key] === false || p.params[key] === true) {
                /* ok */
            } else if ( p.params[key] !== null && p.params[key] !== "" && p.params[key].toLowerCase() !== 'none' ) {
                if (key === "download") {
                    noSSL = true;
                } else if ( key === 'start_date' || key === 'end_date' || key === 'date_start' || key === 'date_end') {
                    p.params[key] = p.params[key].replace(/\//g, '_');
                }
                queryString += key + '/' + p.params[key] + '/';
            }
    }
    try {
        _url = this._makeUrl(p.tab, queryString, noSSL);
    } catch ( e ) {
        return false;
    }
    return location = _url;
}
/**
 *  getReloadUrl
 *
 *  Return url string for safety reload current page
 *  and save all active options
 *
 *  @return string
 */
FSWidgetDashboard.prototype.getReloadUrl = function ( forUrl )
{
    var idx;
    var params    = this.getParams();
    var queryPart = '';
    var url;

    for ( idx in params ) {
        if ( params[idx] === "" || params[idx] === null || params[idx] === undefined || params[idx] === 'none' ) {
            delete params[idx];
            continue;
        }
        if ( params[idx] instanceof Array && !params[idx].length ) {
            delete params[idx];
            continue;
        }

        if ( idx === 'start_date' || idx === 'end_date' ) {
            queryPart += '&' + idx + '=' + encodeURIComponent(params[idx].replace(/\//g, '_')).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
        } else if ( idx === 'projects' ) {
            for ( var i = 0; i < params[idx].length; ++i ) {
                queryPart += '&projects[]=' + encodeURIComponent(params[idx][i]).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
            }
        } else if ( idx === 'company' ) {
            queryPart += '&v=' + encodeURIComponent(params[idx]).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');

        } else {
            queryPart += '&' + idx + '=' + encodeURIComponent(params[idx]).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
        }
    }

    url = location.toString();
    if ( queryPart.length > 0 ) {
        queryPart = queryPart.replace('&', '?');
        url = url.replace(/\?.*$/, queryPart);
        if ( url.indexOf('?') === -1 ) {
            url += queryPart;
        }
    }

    return (( !forUrl ) ?  url : encodeURIComponent(url).replace(/\~/g, '%7E').replace(/\!/g, '%21').replace(/\'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29'));
}
/**
 *  parseUrl
 *
 *  set parameters from URL
 */
FSWidgetDashboard.prototype.parseUrl = function ()
{
    var query       = location.toString().replace(/^[^?]+/, '').replace('?', '').replace('#.*$', '');
    var obj         = {};
    var separator   = '&';
    var i           = 0;
    var token       = [];

    if ( !query )
        return obj;
    query = query.split(separator);

    for ( i = 0; i < query.length; ++i ) {
        token = query[i].split('=');

        if ( token.length === 1 ) {
            obj[token[0]] = null;
        } else if ( token.length === 2 ) {
			token[1] = decodeURIComponent(token[1]);
            if ( obj[token[0]] === undefined ) {
                obj[token[0]] = token[1];
            } else {
                if ( obj[token[0]] instanceof Array ) {
                    obj[token[0]].push(token[1]);
                } else {
                    obj[token[0]] = [obj[token[0]], token[1]];
                }
            }
        }
    }

    if ( obj.start_date !== undefined ) {
        obj.start_date = obj.start_date.replace(/_/g, '/');
    }
    if ( obj.end_date !== undefined ) {
        obj.end_date = obj.end_date.replace(/_/g, '/');
    }

    if ( obj.date_start !== undefined ) {
        obj.date_start = obj.date_start.replace(/_/g, '/');
    }
    if ( obj.date_end !== undefined ) {
        obj.date_end = obj.date_end.replace(/_/g, '/');
    }

    return obj;
}
/**
 *  getParams
 *
 *  Function collect all params for make request
 *
 *  return object
 */
FSWidgetDashboard.prototype.getParams = function (withPage) {
	var params = {
        'tech_id'           : this.filters().techId(),
        'full_search'        : this.filters().fullSearch(),
        'distance'          : this.filters().distance(),
        'tab'               : this.currentTab,
        'company'           : this.filters().company(),
        'projects'          : this.filters().getProjects(),
        'state'             : this.filters().state(),
        'country'             : this.filters().country(),
        'zip'               : this.filters().zip(),
        'start_date'        : this.filters().dateStart(),
        'StandardOffset'         : this.filters().timeZone(),      
        'end_date'          : this.filters().dateEnd(),
        'tech'              : this.filters().tech(),
        'contractor'        : this.filters().contractor(),
        'region'            : this.filters().region(),
        'call_type'         : this.filters().callType(),
        'win'               : this.filters().win(),
        'client_wo'         : this.filters().clientWO(),
        'po'                : this.filters().po(),
        'show_to_tech'      : this.filters().showToTech(),
        'site_name'         : this.filters().siteName(),
        'site_number'       : this.filters().siteNumber(),
        'city'              : this.filters().city(),
        'update_req'        : this.filters().updateRequested(),
        'store_notify'      : this.filters().storeNotified(),
        'checked_in'        : this.filters().checkedIn(),
        'checked_out'        : this.filters().checkedOut(),
        'reviewed_wo'       : this.filters().reviewedWO(),
        'tech_confirmed'    : this.filters().techConfirmed(),
        'tech_complete'     : this.filters().techComplete(),
        'site_complete'     : this.filters().siteComplete(),
        'approved'          : this.filters().approved(),
        'extra_pay'         : this.filters().extraPay(),
        'out_of_scope'      : this.filters().outOfScope(),
        'tech_paid'         : this.filters().techPaid(),
        'tech_fname'        : this.filters().techFName(),
        'tech_lname'        : this.filters().techLName(),
        'show_sourced'      : this.filters().showSourced(),
        'show_not_sourced'  : this.filters().showNotSourced(),
        'paper_received'    : this.filters().paperReceived(),
        'lead'              : this.filters().lead(),
        'assist'            : this.filters().assist(),
        'deactivated'       : this.filters().deactivated(),
        'deactivation_code' : this.filters().deactivationCode(),
        'fls_id'            : this.filters().flsId(),
        'route'           : this.filters().Route(),
        'short_notice'      : this.filters().shortNotice(),
        'sort1'             : this.sortTool() !== null ? this.sortTool().getSort(1) : null,
        'dir1'              : this.sortTool() !== null ? this.sortTool().getDirection(1) : null,
        'sort2'             : this.sortTool() !== null ? this.sortTool().getSort(2) : null,
        'dir2'              : this.sortTool() !== null ? this.sortTool().getDirection(2) : null,
        'sort3'             : this.sortTool() !== null ? this.sortTool().getSort(3) : null,
        'dir3'              : this.sortTool() !== null ? this.sortTool().getDirection(3) : null
        };
		if (typeof(withPage) != 'undefined' && withPage) {
			params.page = this.sortTool() !== null ? this.sortTool().getPage() : null;
		}
		return params;
    }
FSWidgetDashboard.prototype.setParamsSession = function (params)
{
    if ( !params || typeof params !== 'object' ) return;
    this.filters().po((params.po)?params.po:'');
    this.filters().setProject(params.projects);
    this.filters().state(params.state);
    this.filters().country(params.country);
    this.filters().zip((params.zip)?params.zip:'');

    this.filters().dateStart((params.start_date)?params.start_date:'');
    this.filters().dateEnd((params.end_date)?params.end_date:'');
    this.filters().region((params.region)?params.region:'');
    this.filters().tech((params.tech)?params.tech:'');
    this.filters().callType((params.call_type)?params.call_type:'');
}
    FSWidgetDashboard.prototype.setParams = function (params)
    {
        if ( !params || typeof params !== 'object' ) return;

        this.filters().techId(params.tech_id);
        this.filters().contractor(params.contractor);
        this.filters().fullSearch(params.full_search);
        this.filters().win(params.win);
        this.filters().clientWO(params.client_wo);
        this.filters().po((params.po)?params.po:'');
        this.filters().showToTech(params.show_to_tech);
        this.filters().siteName(params.site_name);
        this.filters().siteNumber(params.site_number);
        this.filters().city(params.city);
        this.filters().updateRequested(params.update_req);
        this.filters().storeNotified(params.store_notify);
        this.filters().checkedIn(params.checked_in);
        this.filters().checkedOut(params.checked_out);
        this.filters().reviewedWO(params.reviewed_wo);
        this.filters().timeZone(params.StandardOffset);
        this.filters().techConfirmed(params.tech_confirmed);
        this.filters().techComplete(params.tech_complete);
        this.filters().siteComplete(params.site_complete);
        this.filters().approved(params.approved);
        this.filters().extraPay(params.extra_pay);
        this.filters().outOfScope(params.out_of_scope);
        this.filters().techPaid(params.tech_paid);
        this.filters().techFName(params.tech_fname);
        this.filters().techLName(params.tech_lname);
        this.filters().showSourced(params.show_sourced);
        this.filters().showNotSourced(params.show_not_sourced);
        this.filters().paperReceived(params.paper_received);
        this.filters().lead(params.lead);
        this.filters().assist(params.assist);
        this.filters().deactivated(params.deactivated);
		try {
			if (params['projects[]'] instanceof Array || typeof params['projects[]'] === 'string') {
	        	this.filters().setProject(params['projects[]']);
			}
			else
	        	this.filters().setProject(params.projects);
		} catch (e) {
	        	this.filters().setProject(params.projects);
		}
        this.filters().state(params.state);
        this.filters().country(params.country);
        this.filters().zip((params.zip)?params.zip:'');
        //FIXME We should use date_start/date_end or start_date/end_date. Both are using now.
        this.filters().dateStart(params.date_start?params.date_start: (params.start_date?params.start_date:''));
        this.filters().dateEnd(params.date_end?params.date_end: (params.end_date?params.end_date:''));

        this.filters().tech(params.tech?params.tech:'');
        this.filters().region(params.region?params.region:'');
        this.filters().callType(params.call_type);
        this.filters().shortNotice(params.short_notice);
        this.filters().flsId(params.fls_id);
        this.filters().distance(params.distance);
        this.filters().Route(params.route);

    if ( this.sortTool() && ( params.sort1 || params.sort2 || params.sort3 ) ) {
        if ( params.sort2 || params.sort3 ) {
            //  Full
            this.sortTool().switchSortTools('full');
            this.sortTool().setFullSortVal(params.sort1, 1);
            this.sortTool().setFullSortVal(params.sort2, 2);
            this.sortTool().setFullSortVal(params.sort3, 3);
            this.sortTool().setFullSortDir(params.dir1, 1);
            this.sortTool().setFullSortDir(params.dir2, 2);
            this.sortTool().setFullSortDir(params.dir3, 3);
            this.sortTool().fullInUsage(true);
        } else {
            this.sortTool().switchSortTools('quick');
            this.sortTool().sort(params.sort1);
            this.sortTool().direction(params.dir1);
            this.sortTool().fullInUsage(false);
            this.sortTool().initSortTool();
        }
    }

}

FSWidgetDashboard.prototype.changeDasboaardVersion = function (el){
    this.sortTool().prepareItems($(el).val());
    this.sortTool().initSortTool();
    this.show({
        tab:this.currentTab,
        params:this.getParams()
    })
}

FSWidgetDashboard.prototype.onChangeCountry = function () {
    $.ajax({
        url         : '/widgets/dashboard/js/state-info/',
        dataType    : 'json',
        data        : {
            country: $("#filterCountry").val()
        },
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            if (data) {
                //AUTOFILL
				var option = '';
				option += '<option selected="selected" value="">Select State</option>';
                for(index in data) {
					option += '<option value="' + index + '">' + data[index] + '</option>';
				}
				$("#filterState").html(option);
				state = this.filters().state();
				if (state) $("#filterState").val(state);
			}
        }
    });	
}

FSWidgetDashboard.prototype.openDashboard = function() {
	url = '/clients/wos.php?v=' + window._company;
	if ($("#isDash").val() != '1') {
		location.href = 'dashboard.php?v=' + window._company + '&isDash=1&goto=dashboard';
		return false;
	}
	hideLeftSidebar();
	hideSidebar();
	$('#detailsContainer').html('');
	$('#dashcentercontent').css('width', '100%');
	this.openFramePage(url);
	return false;
}

FSWidgetDashboard.prototype.openFramePage = function(url) {
	if (url == 'dashboard') {
		this.openDashboard();
		return;
	}
	if (url.indexOf('?') == -1) url + '?';
	$('#detailsContainer').html("");
	$('<iframe id="dashFrame"/>').attr('src', url + '&isDash=1').attr('width', '100%').appendTo($('#detailsContainer'));
		$('iframe').load(function(){
			//Get Body of iframe
			var slider = $('iframe').contents().find('a.title');
			//Bind event
			slider.click(function(){
				//get new height
				var newHeight = $('iframe').contents().find('body').height();
				//resize frame
				$('iframe').attr('height', newHeight);
				
			});
		});
}