/**
 * FSWidgetWODetails
 *
 * @param Object|null params
 * @access public
 * @return void
 */
function FSWidgetClientDetails ( params, roll ) {
    FSWidget.call(this, params);

    this.params.POPUP.avaliablePopups = {
        contactLogDetails  : {module:'dashboard',controller:'popup',action:'contact-log-details'}
    };
    this.params.WIDGET.avaliableTabs = {
        adminupdate : {module:'admindashboard',controller:'client',action:'update'},
        admincreate : {module:'admindashboard',controller:'client',action:'create'},
        adminlist   : {module:'admindashboard',controller:'client',action:'list'},
        adminpricingrulelist   : {module:'admindashboard',controller:'client',action:'list-pricing-rule'},
        adminsearch : {module:'admindashboard',controller:'client',action:'search'},
        update      : {module:'dashboard',controller:'client',action:'user-update'},
        create      : {module:'dashboard',controller:'client',action:'user-create'},
        list        : {module:'dashboard',controller:'client',action:'user-list'},
        listadmin   : {module:'admindashboard',controller:'client',action:'user-list'},
        clientedit  : {module:'dashboard',controller:'client',action:'update'},
        addContact  : {module:'dashboard',controller:'client',action:'add-contact'},
        viewLog     : {module:'dashboard',controller:'client',action:'view-contact-log'},
        //'list-customers'     : {module:'dashboard',controller:'client',action:'list-customers'}
    };
    this._option = 0;
    this._roll = roll;
    this._customfunctions = params.customfunctions || {};

    this._redirect =false;
    this._redirectTo = '/admin/adminClientList.php';
}

FSWidgetClientDetails.prototype = new FSWidget();
FSWidgetClientDetails.prototype.constructor = FSWidgetClientDetails;

FSWidgetClientDetails.NAME          = 'FSWidgetWODetails';
FSWidgetClientDetails.VERSION       = '0.1';
FSWidgetClientDetails.DESCRIPTION   = 'Class FSWidgetWODetails';

FSWidgetClientDetails.prototype.roll = function() {
    return this._roll;
}

/**
 *  show
 *  Function display widget
 *  @param Object|null options
 */
FSWidgetClientDetails.prototype.show = function(options) {
    var p = options || {tab:this.currentTab,params:this.getParams()};
    var queryString = '/';
    var _url;

    // TODO params needs be reconfigured
    if ( p.params !== undefined && p.params !== null && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params ) {
            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] === true || p.params[key] === false ) {
                if ( p.params[key] === false ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] !== null && p.params[key] !== "" && p.params[key] !== 'none' ) {
                queryString += key + '/' + p.params[key] + '/';
            } else {
                delete p.params[key];
            }
        }
    } else {
        p.params = {};
    }

    _url = this._makeUrl(this.currentTab, queryString);
    this.showPreloader();

    if ( this.tabOpenXhrRequest ) {
        this.tabOpenXhrRequest.abort();
        this.tabOpenXhrRequest = null;
    }

    this.tabOpenXhrRequest = $.ajax({
        url : _url,
        data : p.params,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/clients/logIn.php";
            } else {
                FSWidget.fillContainer("Sorry, but an error occurred. No results found.", this);
            }
            this.tabOpenXhrRequest = null;
            
        },
        success : function(data, status, xhr) {
            $('body').css('cursor', '');
            FSWidget.fillContainer(data, this);
            this.prepareForm();
            if (typeof(calendarInit) == 'function') calendarInit();
        }
    });

}

FSWidgetClientDetails.prototype.prepareForm = function() {
}


FSWidgetClientDetails.prototype.showPreloader = function() {
    if (this._customfunctions.showPreloader !== undefined && typeof this._customfunctions.showPreloader === 'function' ) {
        this._customfunctions.showPreloader();
    }else{
        $('body').css('cursor', 'wait');
        var img = "<div class='tCenter' ><img style='margin:10% auto;' alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></div>";
        FSWidget.fillContainer(img, this);
    }
}

FSWidgetClientDetails.prototype.updateProfileDetails = function() {
    opt = this._option;
    this._option = 0;
    this.showStartPopup();
    this._redirect =false;
    this._updatestatus = {success:1, errors:[]};
    this._activeupload = 0;
    //970
    this._redirectTo = '/clients/dashboard.php?v='+window._company;
    //end 970
    params = $('#UpdateClientForm').serialize();
    params += '&Company_ID='+window._company;
    $.ajax({
        url         : '/widgets/dashboard/client/do-update/',
        dataType    : 'json',
        data        : params,
        cache       : false,
        type        : 'post',
        context     : this,
		error		: function (xhr, status, error) {
			//console.log (xhr, status, error);
		},
        success     : function (data, status, xhr) {
            this._updatestatus = data;
            //alert(data);
            if (data.success) {
                this._uploadsCounter = 0;
                this.params.client_id = data.client_id;

                this._redirect = false;

                if ( $('#Company_Logo').val() ) {
                    this._uploadsCounter++;
                    this.asyncUploadFile( 'Company_Logo', 'update' );
                }                
                
                if ( $('#CustomSignOff').val() ) {
                    this._uploadsCounter++;
                    this.asyncUploadFile( 'CustomSignOff', 'update' );
                }                
                
                if ( this._uploadsCounter <= 0 ) {
                    if(opt == 0)
                        this.showMessages(this._updatestatus);
                    else
                        this.callUpdateOptionsScript(opt);
                }
            }else{
                this.showMessages(data);
            }
        }
    });
}

FSWidgetClientDetails.prototype.showStartPopup = function() {
    var html = "<div class=\"tCenter\"><img alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></div>";
    this._roll.autohide(false);
    var opt = {
        width       : 300,
        height      : '',
        position    : 'middle',
        title       : 'Processing',
        body        : html
    };
    return this._roll.showNotAjax(null, null, opt);
}

FSWidgetClientDetails.prototype.showMessages = function(data, successMsg, autohide) {
    
    var html = '';
    if (this._redirect || data.success) { //always refresh page on success
        if (window.opener) {try{window.opener.reloadTabFrame();} catch (e) {}}
        window.location = this._redirectTo;
    }
    if (data.success) {
		if (!successMsg)
        html += '<div class="success"><ul><li>Success!</li></ul></div>';
		else
			html = msg;
    }else{
        html+='<div class="errors"><ul>';
        for (i=0;i<data.errors.length;i++) {
            if(typeof data.errors[i] === 'object')
                html+='<li>'+data.errors[i].message+'</li>';
            else
                html+='<li>'+data.errors[i]+'</li>';
        }
        html+='</ul></div>';
    }
	if (!successMsg)
    html+='<div class="tCenter"><input type="button" class="link_button_popup" onclick="detailsWidget.roll().hide();" value="Ok" /></div>';
	if (autohide == undefined) autohide = false;
    this._roll.autohide(autohide);

    var opt = {};
    opt.width    = 400;
    opt.height   = '';
    opt.position = 'middle';
    opt.title    = '';
    opt.body     = html;

    if (data.success) {
        opt.width = 300;
    }
    return this._roll.showNotAjax(null, null, opt);
}

FSWidgetClientDetails.prototype.asyncUploadFile = function(id, action){
	descr = '';
	var uploadUrl = '/widgets/dashboard/client/ajax-upload-file';
	
    $.ajaxFileUpload({
        url           : uploadUrl + "/file/"+id+"/company/"+window._company+"/clt_act/"+action+"/descr/"+descr,
        secureuri     : false,
        fileElementId : id,
        dataType      : 'json',
        context       : this,
        cache         : false,
        success       : function (data, status, xhr) {
            this.context._uploadsCounter--;
            if ( !data.success ) {
                this.context._updatestatus.success = 0;
                if ( !this.context._updatestatus.errors ) {
                    this.context._updatestatus.errors = [];
                }
                for (i=0;i<data.errors.length;i++) {
                    this.context._updatestatus.errors.push(data.errors[i]);
                }
            }
            
            if ( this.context._uploadsCounter <= 0 ) {
                this.context.showMessages(this.context._updatestatus);
            }
        },
        error:function(data, status, xhr){
            this._uploadsCounter--;
            if ( !data.success ) {
                this._updatestatus.success = 0;
                if ( !this._updatestatus.errors ) {
                    this._updatestatus.errors = [];
                }
                this._updatestatus.errors.push('Upload failed');
            }
            if ( this._uploadsCounter <= 0 ) {
                this.showMessages(this._updatestatus);
            }
        }
    });
}

FSWidgetClientDetails.prototype.removeClient = function(id) {
    if (confirm("Are you sure you want delete this CLient?")) {
    p = {params:this.getParams()};
    $.ajax({
        url         : '/widgets/admindashboard/client/remove',
        dataType    : 'json',
        data        : 'id='+id,
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            this._updatestatus = data;
            if (data.success) {
                this.show({params:p.params});
            } else {
                this._redirect = false;
                this._updatestatus = data;
                this.showMessages(this._updatestatus);
            }
        }
    });
    }
}

FSWidgetClientDetails.prototype.confirmRemoveFile = function( obj, file_name )
{
    if ( $(obj).attr('checked') ) {
        if ( confirm('Are you sure you wish to remove the file "' + file_name + '"?') ) {
            $(obj).attr('checked', true);
        } else {
            $(obj).attr('checked', false);
        }
    }
}

/**
 *  getParams
 *
 *  Function collect all params for make request
 *  return object
 */
FSWidgetClientDetails.prototype.getParams = function () {
    params = {};
    if(typeof(this.params.WOFILTERS.ClientID)== 'string') params['ClientID'] = this.params.WOFILTERS.ClientID;
    if(typeof(this.params.WOFILTERS.Password)== 'string') params['Password'] = this.params.WOFILTERS.Password;
    if(typeof(this.params.WOFILTERS.Admin)== 'string') params['Admin'] = this.params.WOFILTERS.Admin;
    if(typeof(this.params.WOFILTERS.AcceptTerms)== 'string') params['AcceptTerms'] = this.params.WOFILTERS.AcceptTerms;
    if(typeof(this.params.WOFILTERS.AccountEnabled)== 'string') params['AccountEnabled'] = this.params.WOFILTERS.AccountEnabled;
    if(typeof(this.params.WOFILTERS.ShowOnReports)== 'string') params['ShowOnReports'] = this.params.WOFILTERS.ShowOnReports;
    if(typeof(this.params.WOFILTERS.ProjectManager)== 'string') params['ProjectManager'] = this.params.WOFILTERS.ProjectManager;
    if(typeof(this.params.WOFILTERS.WebsiteURL)== 'string') params['WebsiteURL'] = this.params.WOFILTERS.WebsiteURL;
    if(typeof(this.params.WOFILTERS.ContactPhone1)== 'string') params['ContactPhone1'] = this.params.WOFILTERS.ContactPhone1;
    if(typeof(this.params.WOFILTERS.ContactPhone2)== 'string') params['ContactPhone2'] = this.params.WOFILTERS.ContactPhone2;
    if(typeof(this.params.WOFILTERS.Email1)== 'string') params['Email1'] = this.params.WOFILTERS.Email1;
    if(typeof(this.params.WOFILTERS.Email2)== 'string') params['Email2'] = this.params.WOFILTERS.Email2;
    if(typeof(this.params.WOFILTERS.ContactName)== 'string') params['ContactName'] = this.params.WOFILTERS.ContactName;
    if(typeof(this.params.WOFILTERS.CompanyName)== 'string') params['CompanyName'] = this.params.WOFILTERS.CompanyName;
    if(typeof(this.params.WOFILTERS.UserType)== 'string') params['UserType'] = this.params.WOFILTERS.UserType;
    if(typeof(this.params.WOFILTERS.Company_ID)== 'string') params['Company_ID'] = this.params.WOFILTERS.Company_ID;


    if(typeof(this.params.WOFILTERS.sortBy)== 'string') params['sortBy'] = this.params.WOFILTERS.sortBy;
    if(typeof(this.params.WOFILTERS.sortDir)== 'string')params['sortDir'] = this.params.WOFILTERS.sortDir;
    if(typeof(this.params.WOFILTERS.letter)== 'string') params['letter'] = this.params.WOFILTERS.letter;
    if(typeof(this.params.WOFILTERS.page)== 'string')   params['page'] = this.params.WOFILTERS.page;
    if(typeof(this.currentTab)== 'string')              params['tab'] = this.currentTab;
    if(typeof(this.params.WOFILTERS.page)== 'string')   params['page'] = this.params.WOFILTERS.page;
    if(typeof(this.params.WOFILTERS.win)== 'string')    params['win'] = this.params.WOFILTERS.win;
    if(typeof(this.params.WOFILTERS.Route)== 'string')    params['route'] = this.params.WOFILTERS.route;
//13901
    if(typeof(this.params.WOFILTERS.logindateGreater)== 'string') params['logindateGreater'] = this.params.WOFILTERS.logindateGreater;
    if(typeof(this.params.WOFILTERS.logindateLess)== 'string') params['logindateLess'] = this.params.WOFILTERS.logindateLess;
//end 13901
    return params;
}

FSWidgetClientDetails.prototype.setParams = function (params)
{
    if ( !params || typeof params !== 'object' ) return;

    if (typeof(params.sortBy)  != 'undefined' ) this.params.WOFILTERS.sortBy = params.sortBy;
    if (typeof(params.sortDir) != 'undefined' ) this.params.WOFILTERS.sortDir = params.sortDir;
    if (typeof(params.letter)  != 'undefined' ) this.params.WOFILTERS.letter = params.letter;
    if (typeof(params.page)    != 'undefined' ) this.params.WOFILTERS.page = params.page;
    if (typeof(params.company) != 'undefined' ) this.params.WOFILTERS.company = params.company;
    if (typeof(params.win)     != 'undefined' ) this.params.WOFILTERS.win = params.win;

    if (typeof(params.Password)       != 'undefined' ) this.params.WOFILTERS.Password = params.Password;
    if (typeof(params.Admin)          != 'undefined' ) this.params.WOFILTERS.Admin = params.Admin;
    if (typeof(params.AcceptTerms)    != 'undefined' ) this.params.WOFILTERS.AcceptTerms = params.AcceptTerms;
    if (typeof(params.AccountEnabled) != 'undefined' ) this.params.WOFILTERS.AccountEnabled = params.AccountEnabled;
    if (typeof(params.ShowOnReports)  != 'undefined' ) this.params.WOFILTERS.ShowOnReports = params.ShowOnReports;
    if (typeof(params.ProjectManager) != 'undefined' ) this.params.WOFILTERS.ProjectManager = params.ProjectManager;
    if (typeof(params.WebsiteURL)     != 'undefined' ) this.params.WOFILTERS.WebsiteURL = params.WebsiteURL;
    if (typeof(params.ContactPhone1)  != 'undefined' ) this.params.WOFILTERS.ContactPhone1 = params.ContactPhone1;
    if (typeof(params.ContactPhone2)  != 'undefined' ) this.params.WOFILTERS.ContactPhone2 = params.ContactPhone2;
    if (typeof(params.Email1)         != 'undefined' ) this.params.WOFILTERS.Email1 = params.Email1;
    if (typeof(params.Email2)         != 'undefined' ) this.params.WOFILTERS.Email2 = params.Email2;
    if (typeof(params.ContactName)    != 'undefined' ) this.params.WOFILTERS.ContactName = params.ContactName;
    if (typeof(params.CompanyName)    != 'undefined' ) this.params.WOFILTERS.CompanyName = params.CompanyName;
    if (typeof(params.ClientID)       != 'undefined' ) this.params.WOFILTERS.ClientID = params.ClientID;
    if (typeof(params.UserType)       != 'undefined' ) this.params.WOFILTERS.UserType = params.UserType;
    if (typeof(params.Company_ID)     != 'undefined' ) this.params.WOFILTERS.Company_ID = params.Company_ID;
    if (typeof(params.Route)          != 'undefined' ) this.params.WOFILTERS.route = params.Route;
    
    //13901
    if (typeof(params.logindateGreater)          != 'undefined' ) this.params.WOFILTERS.logindateGreater = params.logindateGreater;
    if (typeof(params.logindateLess)          != 'undefined' ) this.params.WOFILTERS.logindateLess = params.logindateLess;
    //end 13901
    
}
FSWidgetClientDetails.prototype.isValidContactPhone = function(val, allowExt) {
	if (val == null || val == "") return true;
	valStrip = val.replace(/[^0-9]/g, "");
	if (valStrip.length == 10) {
		val = massagePhone(val);
		return isValidPhone(val);
	}
	else {
		if (!allowExt) return false;
		validFormat = /^[(]?(\d{3})[)]?(?:\s|-|.)*(\d{3})[-\s.]*(\d{4})[^0-9]+([0-9]*)[^0-9]*([0-9]*)$/;
		parts = val.match(validFormat);
		return !(parts == null);
	}
}
FSWidgetClientDetails.prototype.validEmailList = function(list) {
	if (list == null || list == "") return false;
	list = list.replace(/\s/g);
	listParts = list.split(",");
	valid = true;
	for (index in listParts) {
		email = listParts[index];
		if (!isValidEmail(email)) {
			valid = false;
		}
	}
	return valid;
}

FSWidgetClientDetails.prototype.validatePassword = function(val) {
	if (val == null || val == "") return true;
	validFormat = /^(?=.*[A-Z]|[a-z])(?=.*\d)(.{8,15})$/;
	parts = val.match(validFormat);
	return (parts != null); 
}

FSWidgetClientDetails.prototype.validateCreateSubmit = function(role) {
    if (role == null || role == "") return false;
    this._redirect = false;
    updatestatus = {success:1, errors:[]};
    var username = $('#UserName').val();
    var Me = this;
    
    if($('#ContactName').val() == '') updatestatus.errors.push('Contact Name is empty but reqired');
    if($('#ContactPhone1').val() == '') updatestatus.errors.push('Primary Phone is empty but reqired');
    if($('#Email1').val() == '') updatestatus.errors.push('Email Address is empty but reqired');
    if($('#UserName').val() == '') updatestatus.errors.push('Desired Username is empty but reqired');
    if($('#Password').val() == '') updatestatus.errors.push('Desired Password is empty but reqired');
    if(!this.validatePassword($('#Password').val())) updatestatus.errors.push('Password must be between 8-15 characters and contain at least one letter and one number.');
    if($('#PasswordConf').val() == '') updatestatus.errors.push('Confirm Password is empty but reqired');
    if(!this.isValidContactPhone($('#ContactPhone1').val(),true) && $('#ContactPhone1').val()!='')
        updatestatus.errors.push('Primary Phone must be a valid 10 digit number with optional extension similar to ###-###-#### x ####');
    if(!this.isValidContactPhone($('#ContactPhone2').val(),true) && $('#ContactPhone2').val()!='')
        updatestatus.errors.push('Secondary Phone must be a valid 10 digit number with optional extension similar to ###-###-#### x ####');
    if(!this.validEmailList($('#Email1').val()) && $('#Email1').val()!='')
        updatestatus.errors.push('Primary Email Address is not a valid email address');
    if(!this.validEmailList($('#Email2').val()) && $('#Email2').val()!='')
        updatestatus.errors.push('Secondary Email Address is not a valid email address');
    if($('#Password').val() != $('#PasswordConf').val()) updatestatus.errors.push('Values in Desired Password and Confirm Password are not the same');

    if(role == 'admin'){
        if($('#Company_ID').val() == '') updatestatus.errors.push('Company ID is empty but reqired');
        if($('#CompanyName').val() == '') updatestatus.errors.push('Company Name is empty but reqired');
        if($('#Country').val() == 0) updatestatus.errors.push('Country is empty but reqired');
        //13741 
        //if($('#ClientStatusId option:selected').val() == '') updatestatus.errors.push('Client Status is empty but reqired');
        //13759
        if($('#ClientRelationshipId option:selected').val() == '') updatestatus.errors.push('Client Relationship is empty but reqired');
        
        if($('#SegmentId option:selected').val() == '') updatestatus.errors.push('Segment is empty but reqired');
        if($('#OriginalSellerId option:selected').val() == '') updatestatus.errors.push('OriginalSellerId is empty but reqired');
        if($('#ExecutiveId option:selected').val() == '') updatestatus.errors.push('Executive is empty but reqired');
        if($('#FSAccountManagerId option:selected').val() == '') updatestatus.errors.push('Account Manager is empty but reqired');
        if($('#FSClientServiceDirectorId option:selected').val() == '') updatestatus.errors.push('Client Service Director is empty but reqired');
        //if($('#EntityTypeId option:selected').val() == '') updatestatus.errors.push('Entity is empty but reqired');
        if($('#ServiceTypeId option:selected').val() == '') updatestatus.errors.push('Service Type is empty but reqired');
        //end 13759
        //end 13741
    }

    //--- validate username
    var isValid=0; var err="";
    $.ajax({        
        url         : '/widgets/admindashboard/client/validusername/',
        dataType    : 'json',
        data        : {UserName: username},
        async       : false,
        type        : 'post',
        success     : function (data, status, xhr) {
            isValid = data.ok;
            err = data.errors[0];
        }
    });        
    
    if(isValid==0){
        updatestatus.errors.push(err);
    }
        
    if(updatestatus.errors.length <= 0)
        return true;
    updatestatus.success = 0;
    this.showMessages(updatestatus);

    return false;
}

FSWidgetClientDetails.prototype.validateCreateSubmit_1 = function(data) {
    alert('err: ');
    return false;
}



FSWidgetClientDetails.prototype.validateUpdateSubmit = function(role) {
    
    var username = $('#UserName').val();
        var password = $('#Password').val();
        var comfrimps = $('#passwordconfirmtext').val();
        var namearr = $('#ContactName').val().split(' ');
        
        var fname = namearr[0];
        var lname = namearr[namearr.length-1];
        
        var oldpass = $('#oldpass').val();
        if(oldpass!=password)
        {
            var err = _global.validPasword(username,password,comfrimps,fname,lname);
                                    
            if(err['error']==1)
            {
                $('#trerr').css("display", "");
                $('#errmsg').html(err['msg'][0]);
                if(err['id'][0]==2)
                {
                    $('#passwordconfirmtext').css('border','2px solid red');
                    $('#Password').css('border','1px solid black');
                }
                else
                {
                    $('#Password').css('border','2px solid red');
                    $('#passwordconfirmtext').css('border','1px solid black');
                }
                return false;
            }
            else
            {
                $('#trerr').css("display", "none");
                $('#Password').css('border','1px solid black');
                $('#passwordconfirmtext').css('border','1px solid black');
            }
        }
        
    if (role == null || role == "") return false;
    this._redirect = false;
    updatestatus = {success:1, errors:[]};
    if(role == 'client'){
        if($('#ContactName').val() == '') updatestatus.errors.push('Contact Name is empty but reqired');
        if($('#CompanyName').val() == '') updatestatus.errors.push('Company Name is empty but reqired');
        if($('#ContactPhone1').val() == '') updatestatus.errors.push('Primary Phone is empty but reqired');
        if($('#Email1').val() == '') updatestatus.errors.push('Email Address is empty but reqired');
        if($('#Password').val() == '') updatestatus.errors.push('Desired Password is empty but reqired');
    }

    if($('#Username').val() == '') updatestatus.errors.push('Desired Username is empty but reqired');
    if(!this.isValidContactPhone($('#ContactPhone1').val(),true) && $('#ContactPhone1').val()!='')
        updatestatus.errors.push('Primary Phone must be a valid 10 digit number with optional extension similar to ###-###-#### x ####');
    if(!this.isValidContactPhone($('#ContactPhone2').val(),true) && $('#ContactPhone2').val()!='')
        updatestatus.errors.push('Secondary Phone must be a valid 10 digit number with optional extension similar to ###-###-#### x ####');
    if(!this.validEmailList($('#Email1').val()) && $('#Email1').val()!='')
        updatestatus.errors.push('Primary Email Address is not a valid email address');
    if(!this.validEmailList($('#Email2').val()) && $('#Email2').val()!='')
        updatestatus.errors.push('Secondary Email Address is not a valid email address');

    if(updatestatus.errors.length <= 0)
        return true;
    updatestatus.success = 0;
    this.showMessages(updatestatus);
    return false;
}
FSWidgetClientDetails.prototype.validateSearchSubmit = function() {
    this._redirect = false;
    updatestatus = {success:1, errors:[]};
    if(!this.isValidContactPhone($('#ContactPhone1').val(),true) && $('#ContactPhone1').val()!='') updatestatus.errors.push('Primary Phone must be a valid 10 digit number with optional extension similar to ###-###-#### x ####');
    if(!this.isValidContactPhone($('#ContactPhone2').val(),true) && $('#ContactPhone2').val()!='') updatestatus.errors.push('Secondary Phone must be a valid 10 digit number with optional extension similar to ###-###-#### x ####');
    if(!this.validEmailList($('#Email1').val()) && $('#Email1').val()!='') updatestatus.errors.push('Email Address is not a valid email address');
    if(updatestatus.errors.length <= 0)
        return true;
    updatestatus.success = 0;
    this.showMessages(updatestatus);
    return false;
}

FSWidgetClientDetails.prototype.validateClientUpdateSubmit = function() {
    return true;
}

FSWidgetClientDetails.prototype.addContact = function(company, win, isLB) {
    this.showStartPopup();
    this._updatestatus = {success:1, errors:[]};
    this._redirectTo = '/clients/wosViewContactLog.php?v='+company+'&WO_ID='+win;
	if (isLB)
		this._redirectTo = document.location.href;
    params = $('#addContactForm').serialize();
    $.ajax({
        url         : '/widgets/dashboard/client/add-contact/',
        dataType    : 'json',
        data        : params,
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
			msg = "<div class='success'><ul><li>Contact Added</li></ul></div>";
            if($('#Type1').get(0).checked){
/*                eList = $('#Contact').val();
                if ($('#Copy').checked) eList += "," + $('#fromEmail').val();
                return document.location.replace("/smtp_quick_send.php?vFromName="+encodeURIComponent($('#Created_By').val())+
                    "&vFromEmail="+encodeURIComponent($('#fromEmail').val())+
                    "&vSubject="+encodeURIComponent($('#Subject').val())+
                    "&eList="+encodeURIComponent(eList)+
                    "&vMessage="+encodeURIComponent($('#Message').val())+"&Caller=CRM");*/
				msg = "<div class='success'><ul><li>Emails Sent</li></ul></div>";
            }
            this._redirect = false;
            this.showMessages(data, msg, true);
        }
    });
}

FSWidgetClientDetails.prototype.updateSignOffDisplay = function () {
	var signoff_enabled = jQuery("input[name=SignOff_Disabled]:checked").val() == "0";
	var custom_enabled = jQuery("input[name=CustomSignOff_Enabled]:checked").val() == "1";

	if (signoff_enabled && custom_enabled) {
		jQuery("#container_upload").show ();
	}
	else {
		jQuery("#container_upload").hide ();
	}

	if (signoff_enabled) {
		jQuery("input[name=CustomSignOff_Enabled]").removeAttr ("disabled");
	}
	else {
		jQuery("input[name=CustomSignOff_Enabled]").attr ("disabled", "disabled");
	}
};

