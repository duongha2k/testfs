/**
 * FSWidgetWODetails
 *
 * @param Object|null params
 * @access public
 * @return void
 */
function FSWidgetCustomerDetails ( params, roll ) {
    FSWidget.call(this, params);

    this.params.POPUP.avaliablePopups = {
        contactLogDetails  : {module:'dashboard',controller:'popup',action:'contact-log-details'}
    };
    this.params.WIDGET.avaliableTabs = {
		update      : {module:'dashboard',controller:'client',action:'customer-update'},
        create      : {module:'dashboard',controller:'client',action:'customer-create'},
        list        : {module:'dashboard',controller:'client',action:'customer-list'},
        customeredit  : {module:'dashboard',controller:'customer',action:'update'}
    };
    this._option = 0;
    this._roll = roll;
    this._customfunctions = params.customfunctions || {};

    this._redirect =false;
    this._redirectTo = '/admin/adminClientList.php';
}

FSWidgetCustomerDetails.prototype = new FSWidget();
FSWidgetCustomerDetails.prototype.constructor = FSWidgetCustomerDetails;

FSWidgetCustomerDetails.NAME          = 'FSWidgetCustomerDetails';
FSWidgetCustomerDetails.VERSION       = '0.1';
FSWidgetCustomerDetails.DESCRIPTION   = 'Class FSWidgetCustomerDetails';

FSWidgetCustomerDetails.prototype.roll = function() {
    return this._roll;
}

/**
 *  show
 *  Function display widget
 *  @param Object|null options
 */
FSWidgetCustomerDetails.prototype.show = function(options) {
    var p = options || {tab:this.currentTab,params:this.getParams()};
    var queryString = '/';
    var _url;

    // TODO params needs be reconfigured
    if ( p.params !== undefined && p.params !== null && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params ) {
            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] === true || p.params[key] === false ) {
                if ( p.params[key] === false ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] !== null && p.params[key] !== "" && p.params[key] !== 'none' ) {
                //queryString += key + '/' + p.params[key] + '/';
            } else {
                delete p.params[key];
            }
        }
    } else {
        p.params = {};
    }

    _url = this._makeUrl(this.currentTab, queryString);
    this.showPreloader();

    if ( this.tabOpenXhrRequest ) {
        this.tabOpenXhrRequest.abort();
        this.tabOpenXhrRequest = null;
    }

    this.tabOpenXhrRequest = $.ajax({
        url : _url,
        data : p.params,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/quickticket/logIn.php";
            } else {
            
				
			FSWidget.fillContainer("Sorry, but an error occurred. No results found. 1", this);
				
            }
			
            this.tabOpenXhrRequest = null;
            
        },
        success : function(data, status, xhr) {
            $('body').css('cursor', '');
            FSWidget.fillContainer(data, this);
            this.prepareForm();
			//this.initPhone();
			//this.autofillCustomerDetails(p.params.customer, true);
        }
    });

}

FSWidgetCustomerDetails.prototype.prepareForm = function() {
}


FSWidgetCustomerDetails.prototype.showPreloader = function() {
    if (this._customfunctions.showPreloader !== undefined && typeof this._customfunctions.showPreloader === 'function' ) {
        this._customfunctions.showPreloader();
    }else{
        $('body').css('cursor', 'wait');
        var img = "<div class='tCenter' ><img style='margin:10% auto;' alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></div>";
        FSWidget.fillContainer(img, this);
    }
}

FSWidgetCustomerDetails.prototype.updateProfileDetails = function() {
    opt = this._option;
    this._option = 0;
    this.showStartPopup();
    this._redirect =false;
    this._updatestatus = {success:1, errors:[]};
    this._activeupload = 0;
    this._redirectTo = '/quickticket/dashboard.php';
    params = $('#UpdateCustomerForm').serialize();
    params += '&Customer_ID='+window._customer;
	params += '&Client_ID='+window._company;
    $.ajax({
        url         : '/widgets/dashboard/customer/do-update/',
        dataType    : 'json',
        data        : params,
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            this._updatestatus = data;
            if (data.success) {
                this._uploadsCounter = 0;
                this.params.customer_id = data.customer_id;

                this._redirect = false;

                if ( $('#Company_Logo').val() ) {
                    this._uploadsCounter++;
                    this.asyncUploadFile( 'Company_Logo', 'update' );
                }                
                
                if ( this._uploadsCounter <= 0 ) {
                    if(opt == 0)
                        this.showMessages(this._updatestatus);
                    else
                        this.callUpdateOptionsScript(opt);
                }
            }else{
                this.showMessages(data);
            }
        }
    });
};

FSWidgetCustomerDetails.prototype.showStartPopup = function() {
    var html = "<div class=\"tCenter\"><img alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></div>";
    this._roll.autohide(false);
    var opt = {
        width       : 300,
        height      : '',
        position    : 'middle',
        title       : 'Processing',
        body        : html
    };
    return this._roll.showNotAjax(null, null, opt);
}

FSWidgetCustomerDetails.prototype.showMessages = function(data) {
    
    var html = '';
    if (this._redirect || data.success) { //always refresh page on success
        if (window.opener) {try{window.opener.reloadTabFrame();} catch (e) {}}
        window.location = this._redirectTo;
    }
    if (data.success) {
        html += '<div class="success"><ul><li>Success!</li></ul></div>';
    }else{
        html+='<div class="errors"><ul>';
        for (i=0;i<data.errors.length;i++) {
            if(typeof data.errors[i] === 'object')
                html+='<li>'+data.errors[i].message+'</li>';
            else
                html+='<li>'+data.errors[i]+'</li>';
        }
        html+='</ul></div>';
    }
    html+='<div class="tCenter"><input type="button" class="link_button_popup" onclick="detailsWidget.roll().hide();" value="Ok" /></div>';
    this._roll.autohide(false);

    var opt = {};
    opt.width    = 400;
    opt.height   = '';
    opt.position = 'middle';
    opt.title    = '';
    opt.body     = html;

    if (data.success) {
        opt.width = 300;
    }
    return this._roll.showNotAjax(null, null, opt);
}

FSWidgetCustomerDetails.prototype.asyncUploadFile = function(id, action){
	descr = '';
	var uploadUrl = '/widgets/dashboard/customer/ajax-upload-file';
	
    $.ajaxFileUpload({
        url           : uploadUrl + "/file/"+id+"/company/"+window._company+"/clt_act/"+action+"/descr/"+descr,
        secureuri     : false,
        fileElementId : id,
        dataType      : 'json',
        context       : this,
        cache         : false,
        success       : function (data, status, xhr) {
            this.context._uploadsCounter--;
            if ( !data.success ) {
                this.context._updatestatus.success = 0;
                if ( !this.context._updatestatus.errors ) {
                    this.context._updatestatus.errors = [];
                }
                for (i=0;i<data.errors.length;i++) {
                    this.context._updatestatus.errors.push(data.errors[i]);
                }
            }
            
            if ( this.context._uploadsCounter <= 0 ) {
                this.context.showMessages(this.context._updatestatus);
            }
        },
        error:function(data, status, xhr){
            this._uploadsCounter--;
            if ( !data.success ) {
                this._updatestatus.success = 0;
                if ( !this._updatestatus.errors ) {
                    this._updatestatus.errors = [];
                }
                this._updatestatus.errors.push('Upload failed');
            }
            if ( this._uploadsCounter <= 0 ) {
                this.showMessages(this._updatestatus);
            }
        }
    });
}

FSWidgetCustomerDetails.prototype.removeCustomer = function(id) {
    if (confirm("Are you sure you want delete this CLient?")) {
    p = {params:this.getParams()};
    $.ajax({
        url         : '/widgets/admindashboard/customer/remove',
        dataType    : 'json',
        data        : 'id='+id,
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            this._updatestatus = data;
            if (data.success) {
                this.show({params:p.params});
            } else {
                this._redirect = false;
                this._updatestatus = data;
                this.showMessages(this._updatestatus);
            }
        }
    });
    }
}

FSWidgetCustomerDetails.prototype.confirmRemoveFile = function( obj, file_name )
{
    if ( $(obj).attr('checked') ) {
        if ( confirm('Are you sure you wish to remove the file "' + file_name + '"?') ) {
            $(obj).attr('checked', true);
        } else {
            $(obj).attr('checked', false);
        }
    }
}

/**
 *  getParams
 *
 *  Function collect all params for make request
 *  return object
 */
FSWidgetCustomerDetails.prototype.getParams = function () {
    params = {};
    if(typeof(this.params.WOFILTERS.CustomerID)== 'string') params['CustomerID'] = this.params.WOFILTERS.CustomerID;
    if(typeof(this.params.WOFILTERS.Password)== 'string') params['Password'] = this.params.WOFILTERS.Password;
    if(typeof(this.params.WOFILTERS.ContactPhone1)== 'string') params['ContactPhone1'] = this.params.WOFILTERS.ContactPhone1;
    if(typeof(this.params.WOFILTERS.Email1)== 'string') params['Email1'] = this.params.WOFILTERS.Email1;
    if(typeof(this.params.WOFILTERS.ContactName)== 'string') params['ContactName'] = this.params.WOFILTERS.ContactName;
    if(typeof(this.params.WOFILTERS.CompanyName)== 'string') params['CompanyName'] = this.params.WOFILTERS.CompanyName;
    if(typeof(this.params.WOFILTERS.UserType)== 'string') params['UserType'] = this.params.WOFILTERS.UserType;
    if(typeof(this.params.WOFILTERS.Company_ID)== 'string') params['Company_ID'] = this.params.WOFILTERS.Company_ID;


    if(typeof(this.params.WOFILTERS.sortBy)== 'string') params['sortBy'] = this.params.WOFILTERS.sortBy;
    if(typeof(this.params.WOFILTERS.sortDir)== 'string')params['sortDir'] = this.params.WOFILTERS.sortDir;
    if(typeof(this.params.WOFILTERS.letter)== 'string') params['letter'] = this.params.WOFILTERS.letter;
    if(typeof(this.params.WOFILTERS.page)== 'string')   params['page'] = this.params.WOFILTERS.page;
    if(typeof(this.currentTab)== 'string')              params['tab'] = this.currentTab;
    if(typeof(this.params.WOFILTERS.page)== 'string')   params['page'] = this.params.WOFILTERS.page;
    if(typeof(this.params.WOFILTERS.win)== 'string')    params['win'] = this.params.WOFILTERS.win;

    return params;
}

FSWidgetCustomerDetails.prototype.setParams = function (params)
{
    if ( !params || typeof params !== 'object' ) return;

    if (typeof(params.sortBy)  != 'undefined' ) this.params.WOFILTERS.sortBy = params.sortBy;
    if (typeof(params.sortDir) != 'undefined' ) this.params.WOFILTERS.sortDir = params.sortDir;
    if (typeof(params.letter)  != 'undefined' ) this.params.WOFILTERS.letter = params.letter;
    if (typeof(params.page)    != 'undefined' ) this.params.WOFILTERS.page = params.page;
    if (typeof(params.company) != 'undefined' ) this.params.WOFILTERS.company = params.company;
    if (typeof(params.win)     != 'undefined' ) this.params.WOFILTERS.win = params.win;

    if (typeof(params.Password)       != 'undefined' ) this.params.WOFILTERS.Password = params.Password;
    if (typeof(params.ContactPhone1)  != 'undefined' ) this.params.WOFILTERS.ContactPhone1 = params.ContactPhone1;
    if (typeof(params.Email1)         != 'undefined' ) this.params.WOFILTERS.Email1 = params.Email1;
    if (typeof(params.ContactName)    != 'undefined' ) this.params.WOFILTERS.ContactName = params.ContactName;
    if (typeof(params.CompanyName)    != 'undefined' ) this.params.WOFILTERS.CompanyName = params.CompanyName;
    if (typeof(params.CustomerID)       != 'undefined' ) this.params.WOFILTERS.CustomerID = params.CustomerID;
    if (typeof(params.Company_ID)     != 'undefined' ) this.params.WOFILTERS.Company_ID = params.Company_ID;

}
FSWidgetCustomerDetails.prototype.isValidContactPhone = function(val, allowExt) {
	if (val == null || val == "") return true;
	valStrip = val.replace(/[^0-9]/g, "");
	if (valStrip.length == 10) {
		val = massagePhone(val);
		return isValidPhone(val);
	}
	else {
		if (!allowExt) return false;
		validFormat = /^[(]?(\d{3})[)]?(?:\s|-|.)*(\d{3})[-\s.]*(\d{4})[^0-9]+([0-9]*)[^0-9]*([0-9]*)$/;
		parts = val.match(validFormat);
		return !(parts == null);
	}
}
FSWidgetCustomerDetails.prototype.validEmailList = function(list) {
	if (list == null || list == "") return false;
	list = list.replace(/\s/g);
	listParts = list.split(",");
	valid = true;
	for (index in listParts) {
		email = listParts[index];
		if (!isValidEmail(email)) {
			valid = false;
		}
	}
	return valid;
}
FSWidgetCustomerDetails.prototype.validateCreateSubmit = function(role) {
    if (role == null || role == "") return false;
    this._redirect = false;
    updatestatus = {success:1, errors:[]};
    
    if($('#ContactName').val() == '') updatestatus.errors.push('Contact Name is empty but reqired');
    if($('#ContactPhone1').val() == '') updatestatus.errors.push('Primary Phone is empty but reqired');
    if($('#Email1').val() == '') updatestatus.errors.push('Email Address is empty but reqired');
	if($('#Username').val() == '') updatestatus.errors.push('Desired Username is empty but reqired');
    if($('#Password').val() == '') updatestatus.errors.push('Desired Password is empty but reqired');
    if($('#PasswordConf').val() == '') updatestatus.errors.push('Confirm Password is empty but reqired');
    if(!this.isValidContactPhone($('#ContactPhone1').val(),true) && $('#ContactPhone1').val()!='')
        updatestatus.errors.push('Primary Phone must be a valid 10 digit number with optional extension similar to ###-###-#### x ####');
    if(!this.validEmailList($('#Email1').val()) && $('#Email1').val()!='')
        updatestatus.errors.push('Primary Email Address is not a valid email address');
    if($('#Password').val() != $('#PasswordConf').val()) updatestatus.errors.push('Values in Desired Password and Confirm Password are not the same');

    
    if(updatestatus.errors.length <= 0)
        return true;
    updatestatus.success = 0;
    this.showMessages(updatestatus);

    return false;
}
FSWidgetCustomerDetails.prototype.validateUpdateSubmit = function(role) {
    if (role == null || role == "") return false;
    this._redirect = false;
    updatestatus = {success:1, errors:[]};
    if(role == 'customers'){
        if($('#ContactName').val() == '') updatestatus.errors.push('Contact Name is empty but reqired');
        if($('#CompanyName').val() == '') updatestatus.errors.push('Company Name is empty but reqired');
        if($('#ContactPhone1').val() == '') updatestatus.errors.push('Primary Phone is empty but reqired');
        if($('#Email1').val() == '') updatestatus.errors.push('Email Address is empty but reqired');
        if($('#Password').val() == '') updatestatus.errors.push('Desired Password is empty but reqired');
    }

    if(!this.isValidContactPhone($('#ContactPhone1').val(),true) && $('#ContactPhone1').val()!='')
        updatestatus.errors.push('Primary Phone must be a valid 10 digit number with optional extension similar to ###-###-#### x ####');
    if(!this.validEmailList($('#Email1').val()) && $('#Email1').val()!='')
        updatestatus.errors.push('Primary Email Address is not a valid email address');
    
    if(updatestatus.errors.length <= 0)
        return true;
    updatestatus.success = 0;
    this.showMessages(updatestatus);
    return false;
}
FSWidgetCustomerDetails.prototype.validateSearchSubmit = function() {
    this._redirect = false;
    updatestatus = {success:1, errors:[]};
    if(!this.isValidContactPhone($('#ContactPhone1').val(),true) && $('#ContactPhone1').val()!='') updatestatus.errors.push('Primary Phone must be a valid 10 digit number with optional extension similar to ###-###-#### x ####');
    if(!this.validEmailList($('#Email1').val()) && $('#Email1').val()!='') updatestatus.errors.push('Email Address is not a valid email address');
    if(updatestatus.errors.length <= 0)
        return true;
    updatestatus.success = 0;
    this.showMessages(updatestatus);
    return false;
}

FSWidgetCustomerDetails.prototype.validateCustomerUpdateSubmit = function() {
    return true;
}

FSWidgetCustomerDetails.prototype.addContact = function(company, win) {
    this.showStartPopup();
    this._updatestatus = {success:1, errors:[]};
    this._redirectTo = '/quickticket/wosViewContactLog.php?v='+company+'&WO_ID='+win;
    params = $('#addContactForm').serialize();
    $.ajax({
        url         : '/widgets/dashboard/customer/add-contact/',
        dataType    : 'json',
        data        : params,
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            if($('#Type1').checked){
                eList = $('#Contact').val();
                if ($('#Copy').checked) eList += "," + $('#fromEmail').val();
                return document.location.replace("/smtp_quick_send.php?vFromName="+encodeURIComponent($('#Created_By').val())+
                    "&vFromEmail="+encodeURIComponent($('#fromEmail').val())+
                    "&vSubject="+encodeURIComponent($('#Subject').val())+
                    "&eList="+encodeURIComponent(eList)+
                    "&vMessage="+encodeURIComponent($('#Message').val())+"&Caller=CRM");
            }
                    this._redirect = false;
            this.showMessages(data);
        }
    })
};

