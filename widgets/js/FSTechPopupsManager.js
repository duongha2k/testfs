function FSTechPopupsManager(params) {
    this.params = params || {};

    if ( this.params.baseUrl === undefined ) {
        this.params.baseUrl = document.location.protocol + '//' + document.location.hostname;
    }

    var todayDate = new Date();

    this.popups = {
        notifications : {url: '/techs/notifications.php', defaults: {name: 'Notifications'}},
        'bg-checks' : {url: '/techs/backgroundChecks.php', defaults: {name: 'Order a Background Check'}},
        'submitW9' : {url: '/techs/w9.php', defaults: {name: 'Submit Your W-9 Form'}},
        'FSPP' : {url:'/techs/Training/FS_PaymentPolicy.php', defaults: {name: 'Field Solutions Payment Policy'}},
        'get-started' : {url:'/techSignup.php', defaults: {name: 'Get to Work in 3 Steps'}},
        'myProfile' : {url:'/techs/profile.php', defaults: {name: 'My Basic Profile'}},
        'myPicture' : {url:'/techs/media.php', defaults: {name: 'My Picture'}},
        'skills' : {url:'/techs/skills.php', defaults: {name: 'Skills'}},
        'experience' : {url:'/techs/experience.php', defaults: {name: 'Experience'}},
        'certifications' : {url:'/techs/certifications.php', defaults: {name: 'Certifications'}},
        'FS101' : {url:'/techs/Training/FieldSolutions/FS_Certification.php', defaults: {name: ''}},
        'FLS' : {url:'/techs/techFLS.php', defaults: {name: 'FLS'}},
        'help' : {url:'/techs/FAQs/index.php', defaults: {name: 'Help'}},
        'equipment' : {url:'/techs/equipment.php', defaults: {name: 'Equipment'}},
        'cablingSkills' : {url:'/techs/cablingSkills.php', defaults: {name: ''}},
        'telephony' : {url:'/techs/telephonySkills.php', defaults: {name: ''}},
        'assignedWO' : {url:'/techs/wos.php?tab=techassigned', defaults: {name: ''}},
        'incompleteWO' : {url:'/techs/wos.php?tab=techincomplete', defaults: {name: ''}},
        'workdoneWO' : {url:'/techs/wos.php?tab=techworkdone', defaults: {name: ''}},
        'completeWO' : {url:'/techs/wos.php?tab=techcomplete', defaults: {name: ''}},
        'approvedWO' : {url:'/techs/wos.php?tab=techapproved', defaults: {name: ''}},
        'inaccountingWO' : {url:'/techs/wos.php?tab=techinaccounting', defaults: {name: ''}},
        'paidWO' : {url:'/techs/wos.php?tab=techpaid', defaults: {name: ''}},
        'appliedWO' : {url:'/techs/wosViewAppliedAll.php', defaults: {name: ''}},
        'availableWO' : {url:'/techs/wos.php?tab=techavailable&date_start=' + (todayDate.getMonth() + 1) + '/' + todayDate.getDate() + '/' + todayDate.getFullYear(), defaults: {name: 'Work Orders'}},
        'allWO' : {url:'/techs/wos.php?tab=techall', defaults: {name: 'Work Orders'}},
        'banking' : {url:'/techs/banking.php', defaults: {name: 'Direct Deposit Enrollment'}},
        'todo' : {url: '/techs/dashboard.php', defaults: {name: 'To Do'}},
        'ICA' : {url: '/content/Field%20Solutions%20Independent%20Contractor%20Agreement.pdf', defaults: {name: 'Independent Contractor Agreement'}},
        'CoC' : {url: '/content/Members_Code_of_Conduct.pdf', defaults: {name: 'Code of Conduct'}},
        'privacypolicy' : {url: '/content/Privacy_Policy.pdf', defaults: {name: 'Privacy Policy'}},
        'termsofuse' : {url: '/content/Terms_Use.pdf', defaults: {name: 'Terms of Use'}},
        'community' : {url: 'http://groups.google.com/group/mytechnicianspace', defaults: {name: 'Tech Community'}},
        'aboutus' : {url: '/wp/our-services/?view=1', defaults: {name: 'About Us'}},
        'woDetails' : {url: '/techs/wosDetails.php',defaults: {name: 'Work Order'}},
        'DirectDeposit' : {url: '/techs/techAlertDetails.php?UNID=27',defaults: {name: 'Direct Deposit'}}
    };
    this.popupsdefaults = {
        name : '',
        height : 400,
        width: 750,
        left: 100,
        top: 100,
        resizable: 'yes',
        scrollbars: 'yes',
        toolbar: 'no',
        status: 'no',
        menubar: 'no',
        location: 'no',
        directories: 'no',
        queryString: ''
    };

    FSTechPopupsManager.prototype.openPopupWin = function(name,paramsObj) {
        if (this.popups[name] === null || this.popups[name] === undefined) {
            alert('Not Implemented popup!');
            return false;
        }


        var params = paramsObj || {};

        var options = this.popupsdefaults;
        //we need to allow url overwriting, so we added it to options.
        options['url'] = this.popups[name].url;
        var key;
        for (key in options) {
            if (params[key] !== null && params[key] !== undefined) {
                options[key]=params[key];
            }else if(this.popups[name].defaults[key] !== null && this.popups[name].defaults[key] !== undefined) {
                options[key] = this.popups[name].defaults[key];
            }
        }
        /*
        headername = name || this.defaults.params['name'];
        options = options || {};
        options.height      = ( typeof options.height === 'number' ) ? options.height : 500;
        options.width       = ( typeof options.width === 'number' ) ? options.width : 655;
        options.left        = ( typeof options.left === 'number' ) ? options.left : 100;
        options.top         = ( typeof options.top === 'number' ) ? options.top : 100;
        options.resizable   = ( typeof options.resizable === 'string' && options.resizable.toLowerCase() === 'no' ) ? 'no' : 'yes';
        options.scrollbars  = ( typeof options.scrollbars === 'string' && options.scrollbars.toLowerCase() === 'no' ) ? 'no' : 'yes';
        options.toolbar     = ( typeof options.toolbar === 'string' && options.toolbar.toLowerCase() === 'yes' ) ? 'yes' : 'no';
        options.status      = ( typeof options.status === 'string' && options.status.toLowerCase() === 'yes' ) ? 'yes' : 'no';
        options.menubar     = ( typeof options.menubar === 'string' && options.menubar.toLowerCase() === 'yes' ) ? 'yes' : 'no';
        options.location    = ( typeof options.location === 'string' && options.location.toLowerCase() === 'yes' ) ? 'yes' : 'no';
        options.directories = ( typeof options.directories === 'string' && options.directories.toLowerCase() === 'yes' ) ? 'yes' : 'no';

        var checkedBoxes = $('#widgetContainer input[name="approveCheckbox"]').serializeArray();
        if ( checkedBoxes.length && confirm("Approve checked work orders before continuing?") ) {
            doApprove();
        }*/

	var wndName = "";
//	try {
//		wndName = options.name;
//		wndName = wndName.replace(/[^a-z]|[^0-9]/ig, "");
//	} catch (e) { }
        newwindow = window.open( options.url+options.queryString, wndName, 'height='+options.height+',width='+options.width+',left='+options.left+',top='+options.top+',resizable='+options.resizable+',scrollbars='+options.scrollbars+',toolbar='+options.toolbar+',status='+options.status+',directories='+options.directories+',location='+options.location+',menubar='+options.menubar);

        if ( window.focus ) newwindow.focus();
        return newwindow;
    }
}
