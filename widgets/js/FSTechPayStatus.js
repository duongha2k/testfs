function FSTechPayStatus(params, roll ) {
        FSWidgetDashboard.call(this, params);

        this.params.POPUP.avaliablePopups = {
            status : {module:'dashboard',controller:'tech',action:'status-popup'}
        };
        this.params.WIDGET.avaliableTabs = {
            index : {module:'dashboard',controller:'tech',action:'get-pay-status'}
        };
	this._roll = roll;
	//this._html = '';
	//this._container = null;
}


FSTechPayStatus.prototype = new FSWidgetDashboard();
FSTechPayStatus.prototype.constructor = FSTechPayStatus;

FSTechPayStatus.NAME          = 'FSTechPayStatus';
FSTechPayStatus.VERSION       = '0.1';
FSTechPayStatus.DESCRIPTION   = 'Class FSTechPayStatus';

FSTechPayStatus.prototype.prepare = function() {
    $.map(['psStartDate', 'psEndDate','psPaymentDateFrom','psPaymentDateTo'], function( id ) {
        $('#'+id).calendar({dateFormat:'MDY/'});
    });
    $('#calendar_div').css('z-index', 1000);
}

/**
 *  show
 *
 *  Function display widget
 *
 *  @param Object|null options
 */
FSTechPayStatus.prototype.show = function(options) {

    //if ( FSWidget.isFilterActive() ) return;

    p = options || {tab:this.currentTab,params:this.getParams()};

    var queryString = '/';
    var _url;

    // TODO params needs be reconfigured
    if ( p.params !== undefined && p.params !== null && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params ) {
            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] === true || p.params[key] === false ) {
                if ( p.params[key] === false ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] !== undefined && p.params[key] !== null && p.params[key] !== "" && p.params[key] !== 'none' ) {
                //queryString += key + '/' + p.params[key] + '/';
            } else {
                delete p.params[key];
            }
        }
    } else {
        p.params = {};
    }

    try {
        if ( p.tab !== undefined && p.tab !== null ) {
            this.currentTab = p.tab;
            _url = this._makeUrl(p.tab, queryString);
            var idx;
            for ( idx in this.params.WIDGET.avaliableTabs ) {
                $('#'+idx).toggleClass('current', idx === this.currentTab);
            }
        } else if ( p.popup !== undefined && p.popup !== null ) {
            _url = this._makeUrl(p.popup, queryString);
        }
    } catch ( e ) {
        return false;
    }
    $('body').css('cursor', 'wait');
    var img = "<img style='margin:0 40%;' alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' />";
    FSWidget.fillContainer(img, this);

    if ( this.tabOpenXhrRequest ) {
        this.tabOpenXhrRequest.abort();
        this.tabOpenXhrRequest = null;
    }

//    version = $('#dashboardVersion').val();
//    if(version != '' && version == 'Lite'){
//	version = "Lite";
//        p.params.version = version;
//        $('#deactivated').addClass("displayNone");
//    } else {
      //  version = "Full";
//        var defVer = FSWidget.getCookie("dashboardVersionDefault");
        p.params.version = "Full";
//        $('#deactivated').removeClass("displayNone");
 //   }

//    $('#dashboardVersion').val(version);
//    setCookie("dashboardVersionDefault", version);
    this.tabOpenXhrRequest = jQuery.ajax({
        url : _url + '?XDEBUG_SESSION_START=1',
        data : p.params,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/techs/logIn.php";
            } else {
                FSWidget.fillContainer("Sorry, but an error occurred. No results found.", this);
            }
            this.tabOpenXhrRequest = null;
        },
        success : function(data, status, xhr) {
            $('body').css('cursor', '');
            FSWidget.fillContainer(data, this)
            //calendarInit();
        }
    });

}

FSTechPayStatus.prototype.container = function() {
	if (this._container == null) {
		var container = document.createElement('div');
        container.style.display = 'none';
        $('body')[0].appendChild(container);
        this._container = container;
	}
	return this._container;
}

FSTechPayStatus.prototype.buildHtml = function () {
	var _url = this._makeUrl('status');

    jQuery.ajax({
        url : _url,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        success : function(data, status, xhr) {
            $(this.container()).html(data);
        },
        error : function (xhr, status, error) {
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/techs/logIn.php";
            }
        }
    });
};

FSTechPayStatus.prototype.updateFields = function() {
	var win = $("#psWIN").val();
	var StartDateFrom = $("#psStartDate").val();
	var StartDateTo = $("#psEndDate").val();
	var DatePaidFrom = $("#psPaymentDateFrom").val();
    var DatePaidTo = $("#psPaymentDateTo").val();

    if (win.length>0) {
        $("#psWIN").removeAttr("disabled");
        $("#psStartDate").val('').attr("disabled",true);
        $("#psEndDate").val('').attr("disabled",true);
        $("#psPaymentDateFrom").val('').attr("disabled",true);
        $("#psPaymentDateTo").val('').attr("disabled",true);
    }else if (StartDateFrom.length>0 || StartDateTo.length>0) {
        $("#psStartDate").removeAttr("disabled");
        $("#psEndDate").removeAttr("disabled");
        $("#psWIN").val('').attr("disabled",true);
        $("#psPaymentDateFrom").val('').attr("disabled",true);
        $("#psPaymentDateTo").val('').attr("disabled",true);
    }else if (DatePaidFrom.length>0 || DatePaidTo.length>0) {
        $("#psWIN").val('').attr("disabled",true);
        $("#psStartDate").val('').attr("disabled",true);
        $("#psEndDate").val('').attr("disabled",true);
        $("#psPaymentDateFrom").removeAttr("disabled");
        $("#psPaymentDateTo").removeAttr("disabled");
    }else{
        $("#psWIN").removeAttr("disabled");
        $("#psStartDate").removeAttr("disabled");
        $("#psEndDate").removeAttr("disabled");
        $("#psPaymentDateFrom").removeAttr("disabled");
        $("#psPaymentDateTo").removeAttr("disabled");
    }
}

FSTechPayStatus.prototype.doFind = function() {
	var win = $("#psWIN").val();
	var StartDateFrom = $("#psStartDate").val();
	var StartDateTo = $("#psEndDate").val();
	var DatePaidFrom = $("#psPaymentDateFrom").val();
    var DatePaidTo = $("#psPaymentDateTo").val();

        if (win.length>0 || StartDateFrom.length>0 || StartDateTo.length >0 || DatePaidFrom.length >0 || DatePaidTo.length >0) {
            var url = 'techPayStatus.php?t=1';
            if (win.length>0) {
                url +='&win='+win;
            }
            if (StartDateFrom.length>0) {
                url +='&StartDateFrom='+StartDateFrom;
            }
            if (StartDateTo.length>0) {
                url +='&StartDateTo='+StartDateTo;
            }
            if (DatePaidFrom.length>0) {
                url +='&DatePaidFrom='+DatePaidFrom;
            }
            if (DatePaidTo.length>0) {
                url +='&DatePaidTo='+DatePaidTo;
            }
            openPopupWin(url);
            this._roll.hide();
        }
}
