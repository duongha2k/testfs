
//-----------------------------------------------------------------------------
/**
 * @fileoverview Suggested Tech Pay Tool - 
 *
 *	Dependencies: jQuery 1.4.2, FSCoreClasses.js, FSModelViewController.js
 *		FSFlyOutPanel.js
 * 
 * @author Jason K. Flaherty
 */

//-----------------------------------------------------------------------------
/**
 * @class Data model containing all relevant data for calculating suggested
 *	tech pay.
 * @extends FSCollectionModel
 */
var SuggestedTechPayModel = (function () {
	var _parent = FSCollectionModel;

	var SuggestedTechPayModel = function () {
		_parent.call (this,
			"win_num",
			"status",
			"date_entered",
			"pcnt_deduct",
			"show_techs",
			"zipcode",
			"category_id",
			"start_date",
			"start_time",
			"duration",
			"geography",
			"lead_time",
			"day_part",
			"amount_per",
			"visits",
			"devices",
			"result",
			"result_low",
			"result_high"
		);

		this.noUpdate ("geography");
		this.noUpdate ("lead_time");
		this.noUpdate ("day_part");
		this.noUpdate ("pcnt_deduct");
		this.noUpdate ("result");
		this.noUpdate ("result_low");
		this.noUpdate ("result_high");
	};

	SuggestedTechPayModel.prototype = new _parent ();

	return SuggestedTechPayModel;
}) ();

//-----------------------------------------------------------------------------
/**
 * @class View for rendering Suggested Tech Pay Tool and all of it's static
 *	content.
 * @extends FSFlyOutView
 */
var SuggestedTechPayView = (function () {
	var _parent = FSFlyOutView;
	var _email = "DataAnalytics@fieldsolutions.com";
	var _subject = escape ("Official Tech Pay Guide Inquiry");
	var _body = "";
	var _content = "";

	//-------------------------------------------------------------------------
	var _open_shadowbox = function (message, autoclose) {
		var params = {
			"width": 350,
			"height": 80,
			"content": message,
			"autoDimensions": false,
			"centerOnScroll": true
		};

		if (autoclose) params["oncomplete"] = function() {
			setTimeout (function() {
				$.fancybox.close(); 
			}, 2000);
		};

		jQuery ("<div></div>").fancybox (params).trigger ("click");
	};

	//-------------------------------------------------------------------------
	var _show_meeting_request = function () {
		_open_shadowbox (jQuery("#suggested_pay_request").html ());
	};

	//-------------------------------------------------------------------------
	var SuggestedTechPayView = function (mapping, formatting) {
		_parent.call (this, 
			this.combineMappings (mapping || {}, {
				amount: "#suggested_pay_amount",
				amount_per: "#edit_Amount_Per",
				zipcode: "#edit_Zipcode",
				category_id: "#edit_WO_Category_ID",
				show_techs: "#edit_ShowTechs",
				start_date: ".StartDate",
				start_time: ".StartTime",
				end_Date: ".EndDate",
				end_time: ".EndTime",
				duration: ".EstimatedDuration",
				visits: "#edit_Qty_Visits",
				devices: "#edit_Qty_Devices",
				client_offer: "#edit_PayMax",
				pcnt_deduct: "#edit_PcntDeductPercent",
				panel_label: "#suggested_pay_tab #suggested_pay_amount_label",
				panel_info: "#suggested_pay_panel #stp_info",
				panel_geography: "#suggested_pay_panel #display_geography",
				panel_category: "#suggested_pay_panel #display_category",
				panel_leadtime: "#suggested_pay_panel #display_leadtime",
				panel_daypart: "#suggested_pay_panel #display_daypart",
				panel_duration: "#suggested_pay_panel #display_duration",
				panel_pcntdeduct_row: "#suggested_pay_panel #pcntdeduct_row",
				panel_pcntdeduct: "#suggested_pay_panel #display_pcntdeduct",
				panel_result: "#suggested_pay_panel #display_result",
				panel_range: "#suggested_pay_panel #display_range",
				panel_suggested: "#suggested_pay_panel #display_suggested",
				panel_button: "#suggested_pay_panel #apply_button",
				request_button: "#suggested_pay_request #request_button",
				contact_name: "#suggested_pay_panel #contact_name"
			}),
			this.combineMappings (formatting, {
				panel_category: function (value) {
					var text = jQuery.trim (jQuery (this.getElement (
						"category_id", 
						"option[value='" + value + "']"
					)[0]).text ());

					switch (text) {
						case "Apple brand PCs / Laptops":
							text = "Apple PCs / Laptops";
							break;
						case "Electro-Mechanical Binding Eq.":
							text = "Electro-Mech. Binding";
							break;
						case "Safes/Security Cabinets":
							text = "Safe/Security Cabinet";
					}

					return text;
				},
				panel_duration: this.format ("select", "duration"),
				panel_pcntdeduct: this.format ("percent", 0, "0%", "Paid Outside FS"),
				client_offer: function (value) {
					if (typeof value != "undefined" && value > 0.0)
						return value.toFixed (2);
					else
						return "";
				},
				amount: this.format (
					"composite",
					this.format ("join", " / ", "--"),
					this.format ("money_rounded", 0, false, false),
					this.format ("none")
				),
				panel_result: this.format (
					"composite",
					this.format ("join", "  /  ", "Unable to Estimate"),
					this.format ("money_rounded", 0, false, false),
					this.format ("none")
				),
				panel_range: this.format (
					"composite",
					this.format ("join", "   to   ", "Unable to Estimate"),
					this.format ("money_rounded", 0, false, false),
					this.format ("money_rounded", 0)
				),
				panel_suggested: this.format (
					"composite",
					this.format ("join", "  /  ", "Unable to Estimate"),
					this.format ("money_rounded", 0, false, false),
					this.format ("none")
				)
			})
		);

		//---------------------------------------------------------------------
		_body = "%0D%0A%0D%0A\n\n" + escape (this.val ("contact_name"));
		_content = "The Official Tech Pay Guide (&quot;OTG&quot;) is available \
			upon request (<a href='javascript:SuggestedTechPayView.showMeetingRequest();'>request a meeting</a>).\
			<br/><br/>a.	FieldSolutions' tech pay tool analyzes a work order's \
			information iteratively to calculate the benchmark tech pay offer \
			based on the most recent data in the OTG. Want to learn more about \
			data analytics at FieldSolutions \
			(<a href='mailto:" + _email + "?subject=" + _subject + "'>ask a question</a>)?";

		this.getElement ("panel_info").bt (_content, {
			trigger: "click",
			positions: "top",
			fill: "white",
			positions: ['left'],
			cssStyles: {
				color: "black",
				textAlign: "justify"
			}
		});
	}

	SuggestedTechPayView.prototype = new _parent ();
	SuggestedTechPayView.openShadowBox = _open_shadowbox;
	SuggestedTechPayView.showMeetingRequest = _show_meeting_request;

	return SuggestedTechPayView;
}) ();

//-----------------------------------------------------------------------------
/**
 * @class Contains all business logic for Suggested Tech Pay Tool.
 * @extends FSFlyOutController
 */
var SuggestedTechPayCtrl = (function () {
	var _parent = FSFlyOutController;

	//-------------------------------------------------------------------------
	var _send_request = function () {
		jQuery.fancybox.close();
		jQuery.ajax ({
			"type": "GET",
			"dataType" : "json",
			"url": "/widgets/dashboard/details/send-otg-meeting-request",
			"cache" : false,
			"success": function (data, status, message) {
				var message;

				if (data.success)
					message = "<div style='text-align:center;'>Thank you!  You're request has been sent.</div>";
				else
					message = "<div style='text-align:center;color:#8f0000'>Sorry, there was an error sending your request.</div>";

				SuggestedTechPayView.openShadowBox (message, true);
			},
			"error": function (xhr, status, message) {
				var message = "<div style='text-align:center;color:#8f0000'>Sorry, there was an error sending your request.</div>";

				SuggestedTechPayView.openShadowBox (message, true);
			}
		});
	};

	//-------------------------------------------------------------------------
	var SuggestedTechPayCtrl = function (view, model) {
		_parent.call (this, view);

		var calculations = {
			amount_per_hour: function (value) { return value; },
			amount_per_site: function (value, duration) { return value * parseFloat (duration); },
			amount_per_device: function (value, duration, visits, devices) { return value * parseFloat (duration) / devices; },
			amount_per_visit: function (value, duration, visits, devices) { return value * parseFloat (duration) / visits; }
		};

		var button_enabled = false;

		//Private methods
		var _calculate, _calculate_final, _update, _update_panel, _load;
		var _send_request, _activate_button, _apply_result, _add_duration;

		//---------------------------------------------------------------------
		_calculate = function () {
			var pcnt_deduct = 1.0 - (model.val ("pcnt_deduct") != 1.0 ? model.val ("pcnt_deduct") : 0.0);
			var result = model.val ("result");
			var result_low = model.val ("result_low");
			var result_high = model.val ("result_high");

			if (pcnt_deduct > 0.0) {
				result /= pcnt_deduct;
				result_low /= pcnt_deduct;
				result_high /= pcnt_deduct;
			}
			else {
				result = 0.0;
				result_low = 0.0;
				result_high = 0.0;
			}

			//Simultaneous set adjusted results in model and pass to update
			_update_panel (
				model.val ("result", result), 
				model.val ("result_low", result_low), 
				model.val ("result_high", result_high)
			);
		};

		//---------------------------------------------------------------------
		_calculate_final = function (value) {
			var amount_per = (model.val ("amount_per") || "hour").toLowerCase ();
			var duration = parseFloat (model.val ("duration") || 1);
			var visits = parseInt (model.val ("visits") || 1);
			var devices = parseInt (model.val ("devices") || 1);

			return calculations["amount_per_" + amount_per] (Math.round(value), duration, visits, devices);
		};

		//---------------------------------------------------------------------
		//Called whenever a field is updated and, if the required data is
		//present, calls server to get remaining data and result
		_update = function () {
			if (model.val ("zipcode") && model.val ("category_id")
				&& model.val ("date_entered") && model.val ("status")) {

				_load (function (data, status, xhr) {
					model.val ({
						geography: data["geography"],
						lead_time: data["lead_time"],
						day_part: data["day_part"],
						result: typeof data["result"] != "undefined" ? parseFloat (data["result"]) : 0,
						result_low: typeof data["result_low"] != "undefined" ? parseFloat (data["result_low"]) : 0,
						result_high: typeof data["result_high"] != "undefined" ? parseFloat (data["result_high"]) : 0
					});

					view.val ("panel_label", "Benchmark Tech Pay:");

					_activate_button ();
					_calculate ();
				});
			}
		};

		//---------------------------------------------------------------------
		//Updates all content in fly-out panel
		_update_panel = function (value, low, high) {
			var amount_per, unit_value;

			amount_per = (model.val ("amount_per") || "hour").toLowerCase ();
			unit_value = _calculate_final (value);

			view.val ("panel_result", value, "hour");
			view.val ("panel_range", low, high);
			view.val ("panel_suggested", unit_value, amount_per);
			view.val ("amount", unit_value, amount_per);
			view.val ("panel_category", model.val("category_id"));
			view.val ("panel_duration", model.val("duration"));
		};

		//---------------------------------------------------------------------
		//Ajax call to get all data from server
		_load = function (callback) {
			jQuery.ajax ({
				"type": "GET",
				"dataType" : "json",
				"url": "/widgets/dashboard/details/get-tech-pay-data",
				"cache" : false,
				"context" : this,
				"data": model.toObject(),
				"success": callback,
				"error": function (xhr, status, message) {
					//console.log ("error!", xhr, status, message);
				}
			});
		};

		//---------------------------------------------------------------------
		//Applies result to Client Offer field, opens section 4 where the field
		//is located and scrolls to it.
		_apply_result = function () {
			var value = model.val ("result");
			var unit_value = _calculate_final (value);
			var section4;

			view.val ("client_offer", Math.round(unit_value));

			jQuery('.sectionBox').each (function () {
				var section = jQuery(this).find ("#sec4");

				if (section.length > 0) {
					if (section.attr ("title") == "Expand Section") {
						section.attr ("title", "Expand Section")
						section.click ();
					}

					section4 = section;
				}
			});

			jQuery("html, body").animate ({
				scrollTop: jQuery(section4).offset().top
			}, 500);

			view.getElement ("client_offer").focus ();
		};

		//---------------------------------------------------------------------
		//Enables and binds apply button once result has been acquired
		_activate_button = function () {
			if (!button_enabled) {
				button_enabled = true;

				view.getElement ("panel_button").removeClass ("link_button_disabled");
				view.getElement ("panel_button").addClass ("link_button");
				view.getElement ("panel_button").bind ("click", _apply_result);
			}
		};

		//---------------------------------------------------------------------
		_add_duration = function (element) {
			jQuery(element).bind ("change", function () {
				var result = 0.0;

				view.getElement ("duration").each (function () {
					var value = jQuery.trim (jQuery (this).val ());

					value = value != "" ? value : "0.0";
					result += parseFloat (value);
				});

				model._("duration").quietSet (result);
				view.val ("panel_duration", result);
				_update_panel (model.val ("result"), model.val ("result_low"), model.val ("result_high"));
			});

			model.val ("visits", view.val ("visits"));
		};

		//---------------------------------------------------------------------
		//Work order form bindings
		this.bind ("change", model, "zipcode", "zipcode");
		this.bind ("change", model, "category_id", "category_id");
		this.bind ("change", model, "show_techs", "show_techs");
		this.bind ("change", model, "start_date", "start_date");
		this.bind ("blur", model, "start_time", "start_time");
		this.bind ("blur", model, "amount_per", "amount_per");
		this.bind ("change", model, "visits", "visits");
		this.bind ("blur", model, "devices", "devices");
		this.bind ("blur", model, "duration", "duration", "start_date");
		this.bind ("blur", model, "duration", "duration", "start_time");
		this.bind ("blur", model, "duration", "duration", "end_Date");
		this.bind ("blur", model, "duration", "duration", "end_time");
		//this.bind ("change", model, "duration", "duration");

		//Panel output bindings
		this.bind ("change", model, "geography", "panel_geography");
		this.bind ("change", model, "category_id", "panel_category");
		this.bind ("change", model, "lead_time", "panel_leadtime");
		this.bind ("change", model, "day_part", "panel_daypart");
		this.bind ("change", model, "duration", "panel_duration");
		this.bind ("change", model, "pcnt_deduct", "panel_pcntdeduct");
		//this.bind ("change", model, "result", "amount");

		model._("duration").listen ("change", function (value, current, ref) {
			view.val ("panel_duration", model.val ("duration"));
		});

		_add_duration (view.getElement ("duration"));

		//Model update
		model.listen ("update", _update, model);

		//Initial update in case data is already present
		//_update ();

		view.show ();

		this.addDuration = _add_duration;

		//Public update method, private _update function triggered by model.
		//Allows outside code to update all data, ex: Project change
		this.update = function () {
			model.val ({
				show_techs: view.val ("show_techs"),
				zipcode: view.val ("zipcode"),
				category_id: view.val ("category_id"),
				start_date: view.val ("start_date"),
				start_time: view.val ("start_time"),
				duration: view.val ("duration"),
				amount_per: view.val ("amount_per"),
				visits: view.val ("visits"),
				devices: view.val ("devices"),
				pcnt_deduct: parseFloat (view.val ("pcnt_deduct") != "" ? view.val ("pcnt_deduct") : "0")
			});
		};
	};

	SuggestedTechPayCtrl.prototype = new _parent ();
	SuggestedTechPayCtrl.sendMeetingRequest = _send_request;

	return SuggestedTechPayCtrl;
}) ();
