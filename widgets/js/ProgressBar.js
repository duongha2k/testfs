function ProgressBar() {

    this._label                 = '';       //  progress' bar label
    this._done                  = true;     //  is progress done?
    this._percent               = 0;        //  how many is done?

    this._url                   = '/widgets/dashboard/do/progress-bar/';
    this._updateInterval        = 300;
    this._intervalId            = null;
    this._adapter               = null;
    this._requestComplete       = true;
    this._key                   = null;
}

ProgressBar.prototype.label     = function () { return this._label; }
ProgressBar.prototype.done      = function () { return (this._done)? true : false; }
ProgressBar.prototype.percent   = function () { return (!this._percent) ? 0 : this._percent; }

/**
 *  url
 *
 *  Setter|Getter request's url
 *  @param string link
 *  @return string
 */
ProgressBar.prototype.url = function ( link )
{
    if ( typeof link === 'string' && link.length ) {
        this._url = link;
    }
    return this._url;
}
/**
 *  updateInterval
 *
 *  Setter|Getter request update interval
 *  @param int milliseconds
 *  @return int
 */
ProgressBar.prototype.updateInterval = function ( milliseconds )
{
    if ( typeof milliseconds === "number" && milliseconds >= 0 ) {
        this._updateInterval = parseInt(milliseconds, 10);
    }
    return this._updateInterval;
}
/**
 *  adapter
 *
 *  Setter|Getter progress' bar draw adapter
 *  @param ProgressBarAdapterAbstract adapter
 *  @return ProgressBarAdapterAbstract
 */
ProgressBar.prototype.adapter = function ( adapter )
{
    if ( adapter instanceof ProgressBarAdapterAbstract ) {
        this._adapter = adapter;
    }
    return this._adapter;
}
/**
 *  requestComplete
 *
 *  Seter|Getter request's status indicator
 *  @param boolean bool
 *  @return boolean
 */
ProgressBar.prototype.requestComplete = function ( bool )
{
    if ( bool !== undefined && bool !== null ) {
        if ( bool ) {
            this._requestComplete = true;
        } else {
            this._requestComplete = false;
        }
    }
    return this._requestComplete;
}
/**
 *  key
 *
 *  Setter|Getter of key
 *  @param string key
 *  @return string
 */
ProgressBar.prototype.key = function ( key )
{
    if ( key !== undefined && typeof key === 'string' && key.length ) {
        this._key = key;
    }
    return this._key;
}
/**
 *  run
 *
 *  Start progress bar execution
 */
ProgressBar.prototype.run = function ()
{
    //console.log('run');
    this._label             = '';
    this._requestComplete   = true;
    this._done              = false;

    var self = this;
    this._intervalId = setInterval(function () { self.update.call(self); }, this._updateInterval);

    this._adapter.activate();
}
/**
 *  stop 
 */
ProgressBar.prototype.stop = function ()
{
    //console.log('stop');
    this._done  = true;
    this._label = '';

    clearInterval(this._intervalId);
    this._intervalId = null;

    this._adapter.reset();
}
/**
 *  update
 *
 *  Function can called from run function anly!
 *  @return void
 */
ProgressBar.prototype.update = function ()
{
    //console.log('update');
    if ( this._intervalId && this._requestComplete && !this._done && this._adapter && this._url && this._key ) {
        this._requestComplete = false;
        $.ajax({
            async    : true,
            url      : this.url()+'key/'+this.key()+'/',
            type     : "post",
            dataType : "json",
            cache    : false,
            context  : this,
            success  : function (data, status, xhr) {
                this._label     = ( data.label   !== undefined ) ? data.label   : "";
                this._percent   = ( data.percent !== undefined ) ? parseInt(data.percent, 10) : 0;
                this._done      = ( data.done    !== undefined ) ? data.done    : true;
                this.requestComplete(true);

                if ( this.done() ) this.stop();
            },
            error    : function (xhr, status, error) {
                this.stop();
            }
        });
    } else if ( !this.requestComplete() && !this.done() ) {
        //  skip, wait prev request
    } else {
        //  stop, some error was occurred.
        this.stop();
    }
}

