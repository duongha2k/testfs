/**
 * FSTechDashboard
 *
 * @param Object|null params
 * @access public
 * @return void
 */
function FSTechDashboard ( params ) {
    FSWidgetDashboard.call(this, params);
	
    this.params.POPUP.avaliablePopups = {};
    this.params.WIDGET.avaliableTabs = {
        techavailable     : {module:'dashboard',controller:'tech-dashboard',action:'available'},
        techassigned      : {module:'dashboard',controller:'tech-dashboard',action:'assigned'},
        techworkdone      : {module:'dashboard',controller:'tech-dashboard',action:'workdone'},
        techapproved      : {module:'dashboard',controller:'tech-dashboard',action:'approved'},
        //techinaccounting  : {module:'dashboard',controller:'tech-dashboard',action:'inaccounting'},
        techpaid          : {module:'dashboard',controller:'tech-dashboard',action:'paid'},
        techincomplete    : {module:'dashboard',controller:'tech-dashboard',action:'incomplete'},
        techall           : {module:'dashboard',controller:'tech-dashboard',action:'all'}
    };
    
    if ( this.params.POPUP === undefined )
        this.params.POPUP = {};
    this.params.POPUP.avaliablePopups = {
        filters         : {module:'dashboard',controller:'popup',action:'tech-filters'},
        bidsearch       : {module:'dashboard',controller:'popup',action:'bid-search-wo'},
        bidmake         : {module:'dashboard',controller:'do',action:'make-a-bid'},
        netamount       : {module:'dashboard',controller:'popup',action:'net-amount'},
        incomments      : {module:'dashboard',controller:'popup',action:'incomments'},
        starttime       : {module:'dashboard',controller:'popup',action:'tech-start-time'},
        percentcompletion       : {module:'dashboard',controller:'popup',action:'percentcompletion'},
        withdraw        : {module:'dashboard',controller:'popup',action:'bid-withdraw'},
        withdrawmake    : {module:'dashboard',controller:'do',action:'make-a-withdraw'}
    }
    this.filters()._techId = window._techId;
}

FSTechDashboard.prototype = new FSWidgetDashboard();
FSTechDashboard.prototype.constructor = FSTechDashboard;

FSTechDashboard.NAME          = 'FSTechDashboard';
FSTechDashboard.VERSION       = '0.1';
FSTechDashboard.DESCRIPTION   = 'Class FSTechDashboard';

/**
 *  prepareFilters
 */
FSTechDashboard.prototype.prepareFilters = function(roll, params) {

    FSWidget.filterActivate(true);

    var i, stateSelect, callSelect;

    stateSelect = $('#' + roll.container().id + ' #filterState')[0];
    for ( i=0; i < stateSelect.options.length; ++i ) {
        if ( stateSelect.options[i].value == this.filters().state() ) {
            stateSelect.options[i].selected = true;
            break;
        }
    }
	
    countrySelect = $('#' + roll.container().id + ' #filterCountry')[0];
    for ( i=0; i < countrySelect.options.length; ++i ) {
        if ( countrySelect.options[i].value == this.filters().country() ) {
            countrySelect.options[i].selected = true;
            break;
        }
    }
	
	$('#' + roll.container().id + ' #filterCountry').change();
	
    $('#' + roll.container().id + ' #Distance')[0].value = this.filters().distance();
    $('#' + roll.container().id + ' #filterZip')[0].value = this.filters().zip();
    $('#' + roll.container().id + ' #filterStartDate')[0].value = this.filters().dateStart();
    $('#' + roll.container().id + ' #filterEndDate')[0].value = this.filters().dateEnd();

    callSelect = $('#' + roll.container().id + ' #filterCallType')[0];
    for ( i=0; i < callSelect.options.length; ++i ) {
        if ( callSelect.options[i].value == this.filters().callType() ) {
            callSelect.options[i].selected = true;
            break;
        }
    }

    //VALIDATORS
	var vZIP = new LiveValidation('filterZip',LVDefaults);
	vZIP.add( Validate.Format, {pattern: /^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$/} );
}

/**
 *  applyFilters
 *
 *  @return void
 */
FSTechDashboard.prototype.applyFilters = function (roll) {

    FSWidget.filterActivate(false);

    var i, stateSelect, callSelect;

    this.filters().dateStart($('#' + roll.container().id + ' #filterStartDate')[0].value);
    this.filters().dateEnd($('#' + roll.container().id + ' #filterEndDate')[0].value);
    this.filters().zip($('#' + roll.container().id + ' #filterZip')[0].value);
    this.filters().distance($('#' + roll.container().id + ' #Distance')[0].value);

    stateSelect = $('#'+roll.container().id+' #filterState')[0];
    for ( i=0; i< stateSelect.options.length; ++i ) {
        if ( stateSelect.options[i].selected ) {
            this.filters().state($(stateSelect.options[i]).val());
            break;
        }
    }

    countrySelect = $('#'+roll.container().id+' #filterCountry')[0];
    for ( i=0; i< countrySelect.options.length; ++i ) {
        if ( countrySelect.options[i].selected ) {
            this.filters().country($(countrySelect.options[i]).val());
            break;
        }
    }

    callSelect = $('#'+roll.container().id+' #filterCallType')[0];
    for ( i=0; i< callSelect.options.length; ++i ) {
        if ( callSelect.options[i].selected ) {
            this.filters().callType(callSelect.options[i].value);
            break;
        }
    }

    roll.hide();

    var p = this.getParams();
    this.show({tab:this.currentTab,params:p});
}

FSTechDashboard.prototype.showPreloader = function() {
    $('body').css('cursor', 'wait');
    var img  = '<table width="100%" class="gradBox_table_inner">';
        img += '<tr class="table_head"><th>&#160;<br />&#160;</th></tr>';
        img += '<tr><td>&#160<br />&#160;</td></tr>';
        img += '<tr>';
        img += "<td><img style='margin:0 40%;' alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></td>";
        img += '</tr>';
        img += '<tr"><td>&#160<br />&#160;</td></tr>';
        img += '<tr class="tech_paginator"><td class="tCenter">&#160;</td></tr>';
        img += '</table>';
    FSWidget.fillContainer(img, this);
}

FSTechDashboard.prototype.onChangeCountry = function () {
    $.ajax({
        url         : '/widgets/dashboard/js/state-info/',
        dataType    : 'json',
        data        : {country: $("#filterCountry").val()},
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            if (data) {
                //AUTOFILL
				var option = '';
				option += '<option selected="selected" value="">Select State</option>';
                for(index in data) {
					option += '<option value="' + index + '">' + data[index] + '</option>';
				}
				$("#filterState").html(option);
				state = this.filters().state();
				if (state) $("#filterState").val(state);
			}
        }
    });	
}
FSTechDashboard.prototype.QABox = function (rel, win) {
    var content = "<div id='comtantPopupID'><div style='text-align:center;height:380px;width:900px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
    $("<div></div>").fancybox(
    {
        'autoDimensions' : true,
        'showCloseButton' :true,
        'hideOnOverlayClick' : false,
        'enableEscapeButton':false,
        'content': content
    }
    ).trigger('click');
            
    $.ajax({
        type: "POST",
        url: rel,
        data: "",
        context     : this,
        success:function( html ) {
            jQuery("#comtantPopupID").html(html);
            jQuery("#cmdPostQuestion")
            .unbind("click",this.PostQuestion)
            .bind("click",this.PostQuestion); 
                jQuery("#qaapplybid")
                .unbind("click",this.ApplyonQA)
                .bind("click",this.ApplyonQA); 
            
                jQuery("#qawidthdrawbid")
                .unbind("click",this.opentechwidthdrawQA)
                .bind("click",this.opentechwidthdrawQA); 
            $("#QAbox"+win).html("Q&amp;A");//13714
        },
        error:function(error) {
            alert('error; ' + eval(error));
        }
    });
}
FSTechDashboard.prototype.PostQuestion = function (msg) {
    var data = jQuery("#frmPostQuestion").serialize();
    if($("#frmPostQuestion").find("#question").val()!="")
    {
     $.ajax({
        type: "POST",
        url: '/widgets/dashboard/popup/postquestion',
        data: data,
        context : this,
        success:function( html ) {
            jQuery("#comtantPopupID").html("<div style='width:200px;'>"+html+"</div>");
            $('#fancybox-content').width(400);
            $('#fancybox-wrap').width(420);
            $.fancybox.resize();
        },
        error:function(error) {
            alert('error; ' + eval(error));
        }
    });
}
}

FSTechDashboard.prototype.ApplyonQA  = function ()
    {
        win = $(this).attr('win');
        techid = $(this).attr('techid');
        var opt = {
            url         : "/widgets/dashboard/popup/bid-search-wo/",
            width       : 450,
            height      : "",
            data        : 'win='+win+"&tech_id="+techid,
            title       : "",
            body        : "",
            zindex      : 9999,
            successHander: wosTechDetails.prototype.eventApplyWo
        };
        roll.showAjax($("#winnum"), $("#winnum"), opt);
    }
    
FSTechDashboard.prototype.opentechwidthdrawQA  = function ()
    {
        win = $(this).attr('win');
        techid = $(this).attr('techid');
        var strtitle ="<div style='font-size: 13px;width:200px;'>Review Application <a class=ajaxLink href=wosDetails.php?id="+win+"><strong>WIN# "+win+"</strong></a></div>";
        var opt = {
            url         : "/widgets/dashboard/popup/bid-withdraw-qa/",
            width       : 450,
            height      : "",
            data        : 'win='+win+"&tech_id="+techid,
            title       : strtitle,
            body        : "",
            zindex      : 9999
//            successHander: eventApplyWo
        };
        roll.showAjax($("#winnum"), $("#winnum"), opt);
    };
     

//019
FSTechDashboard.prototype.showtempmsg = function()
{
    $("<div></div>").fancybox(
        {
            'autoDimensions' : false,
            'width' : 350,
            'height' : 30,
            'transitionIn': 'none',
            'transitionOut' : 'none',
            'content': 'Your access to this Work Order has been restricted.',
        }).trigger('click');
    setTimeout (function () {$.fancybox.close();}, 2000);
}

//end 019
