/**
 * FSWidgetDashboard
 *
 * @param Object|null params
 * @access public
 * @return void
 */
function FSWidgetDashboard( params ) {
    FSWidget.call(this, params);

    this.params.WIDGET.avaliableTabs = {
            created         : {module:'admindashboard',controller:'index',action:'created'},
            published       : {module:'admindashboard',controller:'index',action:'published'},
            assigned        : {module:'admindashboard',controller:'index',action:'assigned'},
            workdone        : {module:'admindashboard',controller:'index',action:'workdone'},
            approved        : {module:'admindashboard',controller:'index',action:'approved'},
            inaccounting    : {module:'admindashboard',controller:'index',action:'inaccounting'},
            incomplete      : {module:'admindashboard',controller:'index',action:'incomplete'},
            completed       : {module:'admindashboard',controller:'index',action:'completed'},
            all             : {module:'admindashboard',controller:'index',action:'all'},
            deactivated     : {module:'admindashboard',controller:'index',action:'deactivated'}
    };

    this.params.POPUP.avaliablePopups = {
        filters         : {module:'admindashboard',controller:'popup',action:'filters'},
        details         : {module:'dashboard',controller:'sections',action:'section7'},
        woinformation   : {module:'dashboard',controller:'sections',action:'section1'},
        woinstructions  : {module:'dashboard',controller:'sections',action:'section2'},
        techjobsite     : {module:'dashboard',controller:'sections',action:'section6'},
        activitylog     : {module:'dashboard',controller:'popup',action:'activity-log'},
        techinfo        : {module:'dashboard',controller:'popup',action:'tech-info'},
        'final'         : {module:'dashboard',controller:'sections',action:'final'},
        'finalread'     : {module:'dashboard',controller:'sections',action:'final-read'},
        starttime       : {module:'admindashboard',controller:'sections',action:'start-time'},
        documents       : {module:'dashboard',controller:'sections',action:'documents'},
        filedownload    : {module:'dashboard',controller:'do',action:'file-wos-documents-download'},
        incompletereason: {module:'dashboard',controller:'sections',action:'incomplete-reason'},
        quickassigntool : {module:'dashboard',controller:'sections',action:'quickassigntool'},
        findworkorder   : {module:'dashboard',controller:'popup',action:'findworkorder'},
        quickapprove	: {module:'dashboard',controller:'popup',action:'approve'}
    }
    
    /* Sort tool */
    //this._sortTool = null;
    this.sortTool = function ( sort ) {
        if ( sort !== undefined && sort !== null && sort instanceof FSWidgetAdminSortTool ) {
            this._sortTool = sort;
        }
        return this._sortTool;
    }
    /*- Sort tool -*/
}

FSWidgetDashboard.prototype = new FSWidget();
FSWidgetDashboard.prototype.constructor = FSWidgetDashboard;

FSWidgetDashboard.NAME          = 'FSWidgetAdminDashboard';
FSWidgetDashboard.VERSION       = '0.1';
FSWidgetDashboard.DESCRIPTION   = 'Class FSWidgetDashboard';

FSWidgetDashboard.prototype.resetFormIE = function (roll)
{
    roll.body($(this.filters().container()).html());
    calendarInit();
}
/**
 *  htmlFilters
 *
 *  Function return html for filters popup
 *
 *  return string
 */
FSWidgetDashboard.prototype.htmlFilters = function () {
    if ( !this.filters().isInitialized() ) {
        var _url;
        try {
            if(this.filters().loaddasboard()==1)
            {
                _url = this._makeUrl('filters', '/tab/'+this.currentTab+'/company/'+this.filters().company()+'/loaddasboard/'+this.filters().loaddasboard());
            }
            else
            {
            _url = this._makeUrl('filters', '/tab/'+this.currentTab+'/company/'+this.filters().company());
            }
        } catch ( e ) {
            return false;
        }

        jQuery.ajax({
            url : _url,
            cache : false,
            type : 'POST',
            dataType : 'html',
            context : this,
            success : function(data, status, xhr) {
                if ( this.filters().container() === null )
                    this.filters()._makeContainer();
                $(this.filters().container()).html(data);
                this.filters().setInitialized();
            },
            error : function (xhr, status, error) {
                if ( xhr.responseText === "authentication required" ) {
                    document.location = "/clients/logIn.php";
                }
            }
        });
    }
}
/**
 *  resetFilters
 *
 *  @param FSPopupAbstract roll
 *  @return void
 */
FSWidgetDashboard.prototype.resetFilters = function (roll)
{
    this.filters().techId(null);
    this.filters().dateStart('');
    this.filters().dateEnd('');
    this.filters().zip('');
    this.filters().state('');

    this.filters().username('');
    this.filters().headline('');
    this.filters().techEmail('');
    this.filters().techEmailContain(null);
    this.filters().woCategory('');
    this.filters().dateEnteredFrom('');
    this.filters().dateEnteredTo('');
    this.filters().dateApprovedFrom('');
    this.filters().dateApprovedTo('');
    this.filters().dateInvoicedFrom('');
    this.filters().dateInvoicedTo('');
    this.filters().datePaidFrom('');
    this.filters().datePaidTo('');
    this.filters().payMaxFrom(null);
    this.filters().payMaxTo(null);
    this.filters().netPayAmountFrom(null);
    this.filters().netPayAmountTo(null);
    this.filters().tbPay(null);

    this.filters().setProject([]);
    this.filters().tech('');
    this.filters().region('');

    this.filters().fullSearch(false);
    this.filters().win(null);
    this.filters().clientWO(null);       
    this.filters().po('');
    this.filters().showToTech(null);
    this.filters().siteName(null);
    this.filters().siteNumber(null);
    this.filters().city(null);
    this.filters().updateRequested(null);
    this.filters().storeNotified(null);
    this.filters().checkedIn(null);
    this.filters().reviewedWO(null);
    this.filters().techConfirmed(null);
    this.filters().techComplete(null);
    this.filters().techCalled(null);
    this.filters().siteComplete(null);
    this.filters().approved(null);
    this.filters().extraPay(null);
    this.filters().outOfScope(null);
    this.filters().techPaid(null);
    this.filters().techFName(null);
    this.filters().techLName(null);
    this.filters().showSourced(null);
    this.filters().showNotSourced(null);
    this.filters().paperReceived(null);
    this.filters().lead(null);
    this.filters().assist(null);
    this.filters().deactivated(null);
    this.filters().shortNotice(null);
    this.filters().flsId(null);
    this.filters().distance(null);
    this.filters().pricingRan(null);
    this.filters().invoiced(null);
    this.filters().pcntDeduct(null);
    this.filters().deactivatedReason(null);
    this.filters().satrecommendedFrom(null);
    this.filters().satperformanceFrom(null);
    this.filters().showToTech(null);
    this.filters().loaddasboard(null);
    this.filters().deactivatedReason(null);
    this.filters().deactivationCode(null);
    this.filters().Route("");
    
    
    this.filters().Client_Status("");
    this.filters().Client_Relationship("");
    this.filters().Segment("");
    this.filters().Original_Seller("");
    this.filters().Executive("");
    this.filters().CSD("");
    this.filters().AM("");
    this.filters().Entity("");
    this.filters().Service_Type("");
    this.filters().System_Status("");
    
    this.filters().Pay_ISO("");
    this.filters().ISO_Company_Name("");
    this.filters().ISO_ID("");
    this.filters().AbortFee("");
	
	this.filters().wmTechSelect("");
	this.filters().wmTechId(null);
	this.filters().wmAssignmentId(null);
	
}
/**
 *  applyFilters
 *
 *  @return void
 */
FSWidgetDashboard.prototype.applyFilters = function (roll) {

    FSWidget.filterActivate(false);

    var i, stateSelect, callSelect;

    var projects = $('#' + roll.container().id + ' input[name="filterProjects[]"]').serializeArray();
    var _p = [];
    for ( i=0; i < projects.length; ++i ) {
        _p.push( projects[i].value );
    }
    this.filters().setProject(_p);

    this.filters().dateStart($('#' + roll.container().id + ' #filterStartDate')[0].value);
    this.filters().dateEnd($('#' + roll.container().id + ' #filterEndDate')[0].value);
    this.filters().zip($('#' + roll.container().id + ' #filterZip')[0].value);
    this.filters().po($('#' + roll.container().id + ' #filterPO')[0].value);
    
    var filterDeVry = $('#'+roll.container().id+' #filterDeVry');
	this.filters().Devry(filterDeVry.attr('checked') ? filterDeVry.val() : '');

    stateSelect = $('#'+roll.container().id+' #filterState')[0];
    for ( i=0; i< stateSelect.options.length; ++i ) {
        if ( stateSelect.options[i].selected ) {
            this.filters().state($(stateSelect.options[i]).val());
            break;
        }
    }

    callSelect = $('#'+roll.container().id+' #filterCallType')[0];
    for ( i=0; i< callSelect.options.length; ++i ) {
        if ( callSelect.options[i].selected ) {
            this.filters().callType(callSelect.options[i].value);
            break;
        }
    }

    this.filters().tech($('#' + roll.container().id + ' #filterAssignedTech')[0].value);
    this.filters().region($('#' + roll.container().id + ' #filterRegion')[0].value);
    this.filters().Route($('#' + roll.container().id + ' #filterRoute').val());

    roll.hide();

    var p = this.getParams();
    this.show({tab:this.currentTab,params:p});
}
/**
 *  prepareFilters
 */
FSWidgetDashboard.prototype.prepareFilters = function(roll, params) {

    FSWidget.filterActivate(true);

    var i, stateSelect, callSelect, progectsCheckboxes;

    progectsCheckboxes = $('#' + roll.container().id + ' input[name="filterProjects[]"]');
    for ( i=0; i < progectsCheckboxes.length; ++i ) {
        if ( $.inArray(progectsCheckboxes[i].value, this.filters().getProjects()) > -1 ) {
            progectsCheckboxes[i].checked = true;
        } else {
            progectsCheckboxes[i].checked = false;
        }
    }
    stateSelect = $('#' + roll.container().id + ' #filterState')[0];
    for ( i=0; i < stateSelect.options.length; ++i ) {
        if ( stateSelect.options[i].value == this.filters().state() ) {
            stateSelect.options[i].selected = true;
            break;
        }
    }
    
    $('#' + roll.container().id + ' #filterZip')[0].value = this.filters().zip();
    $('#' + roll.container().id + ' #filterStartDate')[0].value = this.filters().dateStart();
    $('#' + roll.container().id + ' #filterEndDate')[0].value = this.filters().dateEnd();
    $('#' + roll.container().id + ' #filterAssignedTech')[0].value = this.filters().tech();
    $('#' + roll.container().id + ' #filterRegion')[0].value = this.filters().region();
    $('#' + roll.container().id + ' #filterPO')[0].value = this.filters().po();
    $('#' + roll.container().id + ' #filterRoute')[0].value = this.filters().Route();

    var filterDeVry = $('#' + roll.container().id + ' #filterDeVry');
	filterDeVry.attr('checked', this.filters().Devry() == 1);
    
    callSelect = $('#' + roll.container().id + ' #filterCallType')[0];
    for ( i=0; i < callSelect.options.length; ++i ) {
        if ( callSelect.options[i].value == this.filters().callType() ) {
            callSelect.options[i].selected = true;
            break;
        }
    }
    
    //VALIDATORS
	var vZIP = new LiveValidation('filterZip',LVDefaults);
	vZIP.add( Validate.Format, {pattern: /^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$/} );
	var vTID = new LiveValidation('filterAssignedTech',LVDefaults);
	vTID.add( Validate.Numericality, {onlyInteger: true} );
}
/**
 *  showPreloader
 *
 *  Function displays widget preloader
 *
 *  @param Object|null options
 */
FSWidgetDashboard.prototype.showPreloader = function() {
    $('body').css('cursor', 'wait');
    var img  = '<table width="100%" class="gradBox_table_inner">';
        img += '<tr class="table_head"><th>&#160;<br />&#160;</th></tr>';
        img += '<tr><td>&#160<br />&#160;</td></tr>';
        img += '<tr>';
        img += "<td><img style='margin:0 40%;' alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></td>";
        img += '</tr>';
        img += '<tr"><td>&#160<br />&#160;</td></tr>';
        img += '<tr class="paginator"><td class="tCenter">&#160;</td></tr>';
        img += '</table>';
    FSWidget.fillContainer(img, this);
}
/**
 *  show
 *
 *  Function displays widget
 *
 *  @param Object|null options
 */
FSWidgetDashboard.prototype.show = function(options) {

    if ( FSWidget.isFilterActive() ) return;

    var p = options || {};
    var queryString = '/';
    var _url;

    // TODO params needs be reconfigured
    if ( p.params !== undefined && p.params !== null && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params ) {
            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] === true || p.params[key] === false ) {
                if ( p.params[key] === false ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] !== undefined && p.params[key] !== null && p.params[key] !== "" && p.params[key].toLowerCase() !== 'none' ) {
                //queryString += key + '/' + p.params[key] + '/';
            } else {
                delete p.params[key];
            }
        }
    } else {
        p.params = {};
    }

    try {
        if ( p.tab !== undefined && p.tab !== null ) {
            this.currentTab = p.tab;
            _url = this._makeUrl(p.tab, queryString);
            var idx;
            for ( idx in this.params.WIDGET.avaliableTabs ) {
                $('#'+idx).toggleClass('current', idx === this.currentTab);
            }
        } else if ( p.popup !== undefined && p.popup !== null ) {
            _url = this._makeUrl(p.popup, queryString);
        }
    } catch ( e ) {
        return false;
    }

    this.showPreloader();

    if ( this.tabOpenXhrRequest ) {
        this.tabOpenXhrRequest.abort();
        this.tabOpenXhrRequest = null;
    }

    version = $('#dashboardVersion').val();

    if(version != '' && version == 'sp'){
    	version = "sp";
        p.params.version = version;
        $('#deactivated').addClass("displayNone");
    } else {
        version = "Full";
//        var defVer = FSWidget.getCookie("dashboardVersionDefault");
        p.params.version = version;
        $('#deactivated').removeClass("displayNone");
    }

    $('#dashboardVersion').val(version);
    setCookie("dashboardVersionDefault", version);
    
    // get page size
    if ($('#pageSizeSelector') ) {
    	p.params.size = $('#pageSizeSelector').val();
    }

    this.tabOpenXhrRequest = jQuery.ajax({
        url : _url,
        data : p.params,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/admin/logIn.php";
            } else {
                FSWidget.fillContainer("Sorry, but an error occurred. No results found.", this);
            }
            this.tabOpenXhrRequest = null;
        },
        success : function(data, status, xhr) {
            $('body').css('cursor', '');
            FSWidget.fillContainer(data, this);
            this.tabOpenXhrRequest = null;
            if (this.currentTab == 'assigned') {
                $(".rePublishCheckbox").click(function() {
                    var id = $(this).attr('id');
                    var matches = /^rePublishCheckbox_(\d+)$/.exec(id);
                    var win = matches[1];

                    if ($(this).attr('checked')) {
                        $("#rePublishOptions_" + win).removeClass('displayNone');
                    } else {
                        $("#rePublishOptions_" + win).addClass('displayNone');
                    }
                });
            }
        }
    });
}
/**
 *  open
 */
FSWidgetDashboard.prototype.open = function(options) {
    var p = options || {};
    var queryString = '/';
    var _url;
    var noSSL = false;

    if ( p.params !== undefined && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params ){
            //  Filter for empty parameters
	        if (key === "wo_category") {
                    var keyval = p.params[key] ;
                    if(keyval === null || keyval ==="")
                    {}
                    else
                    {
	        	p.params[key] = p.params[key].replace(/\//g, '_');
                    }
	            /* ok */
	        }
            if ( p.params[key] instanceof Array ) {
            	
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                } else if ( key === 'projects' ) {
                    var ids = '';
                    for ( var pr=0; pr < p.params[key].length; ++pr ) {
                        ids += p.params[key][pr];
                        if ( pr + 1 < p.params[key].length ) { 
                            ids += '|'; 
                        }
                    }
                    queryString += 'projects/' + ids + '/';
                }
            } else if (p.params[key] === false || p.params[key] === true) {
                /* ok */
            } else if ( p.params[key] !== undefined && p.params[key] !== null && p.params[key] !== "" && p.params[key].toLowerCase() !== 'none' ) {
                if (key === "download") {
                    noSSL = true;
                } else if ( key === 'start_date' || key === 'end_date' || key === 'date_start' || key === 'date_end' || key.indexOf("date_") == 0) {
                    p.params[key] = p.params[key].replace(/\//g, '_');
                }
                queryString += key + '/' + p.params[key] + '/';
            }
        }
    }
    try {
        _url = this._makeUrl(p.tab, queryString, noSSL);
    } catch ( e ) {
        return false;
    }
   
    return location = _url;
}
/**
 *  getReloadUrl
 *
 *  Return url string for safety reload current page
 *  and save all active options
 *
 *  @return string
 */
FSWidgetDashboard.prototype.getReloadUrl = function ( forUrl )
{
    var idx;
    var params    = this.getParams();
    var queryPart = '';
    var url;

    for ( idx in params ) {
        if ( params[idx] === "" || params[idx] === null || params[idx] === undefined || params[idx] === 'none' ) {
            delete params[idx];
            continue;
        }
        if ( params[idx] instanceof Array && !params[idx].length ) {
            delete params[idx];
            continue;
        }

        if ( idx === 'start_date' || idx === 'end_date' ) {
            queryPart += '&' + idx + '=' + encodeURIComponent(params[idx].replace(/\//g, '_')).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
        } else if ( idx === 'projects' ) {
            for ( var i = 0; i < params[idx].length; ++i ) {
                queryPart += '&projects=' + encodeURIComponent(params[idx][i]).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
            }
        } else if ( idx === 'company' ) {
            queryPart += '&v=' + encodeURIComponent(params[idx]).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');

        } else {
            queryPart += '&' + idx + '=' + encodeURIComponent(params[idx]).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
        }
    }

    url = location.toString();
    if ( queryPart.length > 0 ) {
        queryPart = queryPart.replace('&', '?');
        url = url.replace(/\?.*$/, queryPart);
        if ( url.indexOf('?') === -1 ) {
            url += queryPart;
        }
    }

    return (( !forUrl ) ?  url : encodeURIComponent(url).replace(/\~/g, '%7E').replace(/\!/g, '%21').replace(/\'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29'));
}
/**
 *  parseUrl
 *
 *  set parameters from URL
 */
FSWidgetDashboard.prototype.parseUrl = function ()
{
    var query       = location.toString().replace(/^[^?]+/, '').replace('?', '').replace('#.*$', '');
    var obj         = {};
    var separator   = '&';
    var i           = 0;
    var token       = [];

    if ( !query )
        return obj;
    query = query.split(separator);

    for ( i = 0; i < query.length; ++i ) {
        token = query[i].split('=');

        if ( token.length === 1 ) {
            obj[token[0]] = null;
        } else if ( token.length === 2 ) {
			token[1] = decodeURIComponent(token[1]);
            if ( obj[token[0]] === undefined ) {
                obj[token[0]] = token[1];
            } else {
                if ( obj[token[0]] instanceof Array ) {
                    obj[token[0]].push(token[1]);
                } else {
                    obj[token[0]] = [obj[token[0]], token[1]];
                }
            }
        }
    }

    if ( obj.start_date !== undefined ) {
        obj.start_date = obj.start_date.replace(/_/g, '/');
    }
    if ( obj.end_date !== undefined ) {
        obj.end_date = obj.end_date.replace(/_/g, '/');
    }
    if ( obj.date_start !== undefined ) {
        obj.date_start = obj.date_start.replace(/_/g, '/');
    }
    if ( obj.date_end !== undefined ) {
        obj.date_end = obj.date_end.replace(/_/g, '/');
    }
    if ( obj.date_entered_from !== undefined ) {
        obj.date_entered_from = obj.date_entered_from.replace(/_/g, '/');
    }
    if ( obj.date_entered_to !== undefined ) {
        obj.date_entered_to = obj.date_entered_to.replace(/_/g, '/');
    }
    if ( obj.date_approved_from !== undefined ) {
        obj.date_approved_from = obj.date_approved_from.replace(/_/g, '/');
    }
    if ( obj.date_approved_to !== undefined ) {
        obj.date_approved_to = obj.date_approved_to.replace(/_/g, '/');
    }
    if ( obj.date_invoiced_from !== undefined ) {
        obj.date_invoiced_from = obj.date_invoiced_from.replace(/_/g, '/');
    }
    if ( obj.date_invoiced_to !== undefined ) {
        obj.date_invoiced_to = obj.date_invoiced_to.replace(/_/g, '/');
    }
    if ( obj.date_paid_from !== undefined ) {
        obj.date_paid_from = obj.date_paid_from.replace(/_/g, '/');
    }
    if ( obj.date_paid_to !== undefined ) {
        obj.date_paid_to = obj.date_paid_to.replace(/_/g, '/');
    }
    return obj;
}
/**
 *  getParams
 *
 *  Function collect all params for make request
 *
 *  return object
 */
FSWidgetDashboard.prototype.getParams = function () {
    return {
        'tech_id'           : this.filters().techId(),
        'full_seach'        : this.filters().fullSearch(),
        'distance'          : this.filters().distance(),
        'tab'               : this.currentTab,
        'Amount_Per'        : this.filters().amount_per(),
        'company'           : this.filters().company(),
        'projects'          : this.filters().getProjects(),
        'state'             : this.filters().state(),
        'zip'               : this.filters().zip(),
        'start_date'        : this.filters().dateStart(),
        'devry'             : this.filters().Devry(),

        'username'           : this.filters().username(),
        'headline'           : this.filters().headline(),
        'tech_email'         : this.filters().techEmail(),
        'tech_email_contain' : this.filters().techEmailContain(),
        'wo_category'        : this.filters().woCategory(),
        'date_entered_from'  : this.filters().dateEnteredFrom(),
        'date_entered_to'    : this.filters().dateEnteredTo(),
        'date_approved_from' : this.filters().dateApprovedFrom(),
        'date_approved_to'   : this.filters().dateApprovedTo(),
        'date_invoiced_from' : this.filters().dateInvoicedFrom(),
        'date_invoiced_to'   : this.filters().dateInvoicedTo(),
        'date_paid_from'     : this.filters().datePaidFrom(),
        'date_paid_to'       : this.filters().datePaidTo(),
        'pay_max_from'       : this.filters().payMaxFrom(),
        'pay_max_to'         : this.filters().payMaxTo(),
        'net_pay_amount_from': this.filters().netPayAmountFrom(),
        'net_pay_amount_to'  : this.filters().netPayAmountTo(),
        'tb_pay'             : this.filters().tbPay(),

        'end_date'          : this.filters().dateEnd(),
        'tech'              : this.filters().tech(),
        'region'            : this.filters().region(),
        'call_type'         : this.filters().callType(),
		'wm_tech_select'	: this.filters().wmTechSelect(),
		'wm_tech_id'	: this.filters().wmTechId(),
		'wm_assignment_id'	: this.filters().wmAssignmentId(),
        'win'               : this.filters().win(),
        'client_wo'         : this.filters().clientWO(),       
        'po'                : this.filters().po(),
        'show_to_tech'      : this.filters().showToTech(),
        'site_name'         : this.filters().siteName(),
        'site_number'       : this.filters().siteNumber(),
        'city'              : this.filters().city(),
        'update_req'        : this.filters().updateRequested(),
        'store_notify'      : this.filters().storeNotified(),
        'checked_in'        : this.filters().checkedIn(),
        'reviewed_wo'       : this.filters().reviewedWO(),
        'tech_confirmed'    : this.filters().techConfirmed(),
        'tech_complete'     : this.filters().techComplete(),
        'tech_called'       : this.filters().techCalled(),
        'site_complete'     : this.filters().siteComplete(),
        'approved'          : this.filters().approved(),
        'extra_pay'         : this.filters().extraPay(),
        'out_of_scope'      : this.filters().outOfScope(),
        'tech_paid'         : this.filters().techPaid(),
        'tech_fname'        : this.filters().techFName(),
        'tech_lname'        : this.filters().techLName(),
        'show_sourced'      : this.filters().showSourced(),
        'show_not_sourced'  : this.filters().showNotSourced(),
        'paper_received'    : this.filters().paperReceived(),
        'lead'              : this.filters().lead(),
        'assist'            : this.filters().assist(),
        'deactivated'       : this.filters().deactivated(),
        'fls_id'            : this.filters().flsId(),
        'short_notice'      : this.filters().shortNotice(),
        'pricing_ran'       : this.filters().pricingRan(),
        'invoiced'       	: this.filters().invoiced(),
        'pcnt_deduct'       : this.filters().pcntDeduct(),
        'deactivated_reason': this.filters().deactivatedReason(),
        'deactivation_code' : this.filters().deactivationCode(),
        'satrecommended_from': this.filters().satrecommendedFrom(),
        'satperformance_from': this.filters().satperformanceFrom(),
        
        'Client_Status': this.filters().Client_Status(),
        'Client_Relationship': this.filters().Client_Relationship(),
        'Segment': this.filters().Segment(),
        'Original_Seller': this.filters().Original_Seller(),
        'Executive': this.filters().Executive(),
        'CSD': this.filters().CSD(),
        'AM': this.filters().AM(),
        'Entity': this.filters().Entity(),
        'Service_Type': this.filters().Service_Type(),
        'System_Status': this.filters().System_Status(),
        'loaddasboard': this.filters().loaddasboard(),
        'AbortFee': this.filters().AbortFee(),
        //end 622
        'Pay_ISO': this.filters().Pay_ISO(),
        'ISO_Company_Name': this.filters().ISO_Company_Name(),
        'ISO_ID': this.filters().ISO_ID(),
        
        'route'             : this.filters().Route(),
        'sort1'             : this.sortTool() !== null ? this.sortTool().getSort(1) : null,
        'dir1'              : this.sortTool() !== null ? this.sortTool().getDirection(1) : null,
        'sort2'             : this.sortTool() !== null ? this.sortTool().getSort(2) : null,
        'dir2'              : this.sortTool() !== null ? this.sortTool().getDirection(2) : null,
        'sort3'             : this.sortTool() !== null ? this.sortTool().getSort(3) : null,
        'dir3'              : this.sortTool() !== null ? this.sortTool().getDirection(3) : null
        }
    }

    FSWidgetDashboard.prototype.setParams = function (params)
    {
        if ( !params || typeof params !== 'object' ) return;
        
        this.filters().techId(params.tech_id);
        this.filters().fullSearch(params.full_search);
        this.filters().win(params.win);
        this.filters().clientWO(params.client_wo);       
        this.filters().po((params.po)?params.po:'');
        this.filters().showToTech(params.show_to_tech);
        this.filters().siteName(params.site_name);
        this.filters().siteNumber(params.site_number);
        this.filters().city(params.city);
        this.filters().updateRequested(params.update_req);
        this.filters().storeNotified(params.store_notify);
        this.filters().checkedIn(params.checked_in);
        this.filters().reviewedWO(params.reviewed_wo);
        this.filters().techConfirmed(params.tech_confirmed);
        this.filters().techComplete(params.tech_complete);
        this.filters().techCalled(params.tech_called);
        this.filters().siteComplete(params.site_complete);
        this.filters().approved(params.approved);
        this.filters().extraPay(params.extra_pay);
        this.filters().outOfScope(params.out_of_scope);
        this.filters().techPaid(params.tech_paid);
        this.filters().techFName(params.tech_fname);
        this.filters().techLName(params.tech_lname);
        this.filters().showSourced(params.show_sourced);
        this.filters().showNotSourced(params.show_not_sourced);
        this.filters().paperReceived(params.paper_received);
        this.filters().lead(params.lead);
        this.filters().assist(params.assist);
        this.filters().deactivated(params.deactivated);
        this.filters().setProject(params.projects);
        this.filters().state(params.state);
        this.filters().zip((params.zip)?params.zip:'');
        this.filters().dateStart(params.date_start?params.date_start:'');
        this.filters().dateEnd(params.date_end?params.date_end:'');

        this.filters().username(params.username);
        this.filters().headline(params.headline);
        this.filters().techEmail(params.tech_email);
        this.filters().woCategory(params.wo_category);
        this.filters().dateEnteredFrom(params.date_entered_from?params.date_entered_from:'');
        this.filters().dateEnteredTo(params.date_entered_to?params.date_entered_to:'');
        this.filters().dateApprovedFrom(params.date_approved_from?params.date_approved_from:'');
        this.filters().dateApprovedTo(params.date_approved_to?params.date_approved_to:'');
        this.filters().dateInvoicedFrom(params.date_invoiced_from?params.date_invoiced_from:'');
        this.filters().dateInvoicedTo(params.date_invoiced_to?params.date_invoiced_to:'');
        this.filters().datePaidFrom(params.date_paid_from?params.date_paid_from:'');
        this.filters().datePaidTo(params.date_paid_to?params.date_paid_to:'');
        this.filters().payMaxFrom(params.pay_max_from);
        this.filters().payMaxTo(params.pay_max_to);
        this.filters().netPayAmountFrom(params.net_pay_amount_from);
        this.filters().netPayAmountTo(params.net_pay_amount_to);
        this.filters().tbPay(params.tb_pay);

        this.filters().tech(params.tech?params.tech:'');
        this.filters().region(params.region?params.region:'');
        this.filters().callType(params.call_type);
        this.filters().wmTechSelect(params.wm_tech_select);
        this.filters().wmTechId(params.wm_tech_id);
        this.filters().wmAssignmentId(params.wm_assignment_id);
        this.filters().shortNotice(params.short_notice);
        this.filters().flsId(params.fls_id);
        this.filters().distance(params.distance);
        this.filters().pricingRan(params.pricing_ran);
        this.filters().invoiced(params.invoiced);
        this.filters().pcntDeduct(params.pcnt_deduct);
        this.filters().deactivatedReason(params.deactivated_reason);
        this.filters().satrecommendedFrom(params.satrecommended_from);
        this.filters().loaddasboard(params.loaddasboard);
        this.filters().Route(params.route);
        
        this.filters().Client_Status(params.Client_Status);
        this.filters().Client_Relationship(params.Client_Relationship);
        this.filters().Segment(params.Segment);
        this.filters().Original_Seller(params.Original_Seller);
        this.filters().Executive(params.Executive);
        this.filters().CSD(params.CSD);
        this.filters().AM(params.AM);
        this.filters().Entity(params.Entity);
        this.filters().Service_Type(params.Service_Type);
        this.filters().System_Status(params.System_Status);
        this.filters().satperformanceFrom(params.satperformance_from);
        this.filters().Pay_ISO(params.Pay_ISO);
        this.filters().ISO_Company_Name(params.ISO_Company_Name);
        this.filters().ISO_ID(params.ISO_ID);
        this.filters().AbortFee(params.AbortFee);
        
    if ( this.sortTool() && ( params.sort1 || params.sort2 || params.sort3 ) ) {
        if ( params.sort2 || params.sort3 ) {
            //  Full
            this.sortTool().switchSortTools('full');
            this.sortTool().setFullSortVal(params.sort1, 1);
            this.sortTool().setFullSortVal(params.sort2, 2);
            this.sortTool().setFullSortVal(params.sort3, 3);
            this.sortTool().setFullSortDir(params.dir1, 1);
            this.sortTool().setFullSortDir(params.dir2, 2);
            this.sortTool().setFullSortDir(params.dir3, 3);
            this.sortTool().fullInUsage(true);
        } else {
            this.sortTool().switchSortTools('quick');
            this.sortTool().sort(params.sort1);
            this.sortTool().direction(params.dir1);
            this.sortTool().fullInUsage(false);
            this.sortTool().initSortTool();
        }
    }
}

