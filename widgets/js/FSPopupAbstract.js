/**
 * FSPopupAbstract  
 * 
 * Abstract Class
 *
 * @param object|null $params 
 * @access public
 * @return void
 */
function FSPopupAbstract(params) {
    this.params = params || {};

    if ( this.params.width === undefined || this.params.width === null || typeof this.params.width !== 'number' )
        this.params.width = 300;
    if ( this.params.height === undefined || this.params.height === null || typeof this.params.height !== 'number' )
        this.params.height = 400;
    if ( this.params.top === undefined || this.params.top === null || typeof this.params.top !== 'number' )
        this.params.top = 0;
    if ( this.params.left === undefined || this.params.left === null || typeof this.params.left !== 'number' )
        this.params.left = 0;

    /**
     *  if not null timer is setted
     *  @var ID
     */
    this._timer = null;
    /**
     *  how long wait before close popup
     *  @var int, sec
     */
    this._timer_wait = 1;
    /**
     *  is need auto hide popup
     *  @var boolean
     */
    this._autohide = false;
    /**
     *  Number of delay seconds
     */
    this._delay     = 1.1;
    /**
     *  ID of setTimeout function
     */
    this._delay_id  = null;
    /**
     *  Set/Get delay
     *
     *  @param int d
     *  @return int
     */
    this.delay = function ( d ) {
        if ( typeof d === 'number' ) {
            this._delay = d;
        }
        return this._delay;
    }
    /**
     *  isUserNoWaitDelay
     */
    this.isUserNoWaitDelay = function () {
        if ( this._delay_id !== null ) {
            clearTimeout(this._delay_id);
        }
        this._delay_id = null;
    }
    /**
     *  timer
     *
     *  Set/Get timer option
     *  @param mixed id
     *  @return mixed
     */
    this.timer = function ( id ) {
        if ( id !== undefined ) {
            this._timer = id;
        }
        return this._timer;
    }
    /**
     *  timerWait
     *
     *  Set/Get timer wait time
     *  @param int sec, seconds
     *  @return int
     */
    this.timerWait = function (sec) {
        if ( typeof sec === 'number' ) {
            this._timer_wait = sec;
        }
        return this._timer_wait;
    }
    /**
     *  autohide
     *
     *  Set/Get autohide option
     *  @param mixed hide
     *  @return mixed
     */
    this.autohide = function ( hide ) {
        if ( hide !== undefined && hide !== null ) {
            if ( hide ) {
                this._autohide = true;
            } else {
                this._autohide = false;
            }
        }
        return this._autohide;
    }

    this.params._popup      = {};
    this.params._popup._c   = null;  //  container
    this.params._popup._hT  = null;  //  header title
    this.params._popup._hB  = null;  //  header close button
    this.params._popup._b   = null;  //  body
    
    /**
     *  header 
     *
     *  @param html|null h Popup title content
     *  @return Node Header div
     */
    this.header = function (h) {
        if ( h !== undefined && h !== null )
            $(this.params._popup._hT).html(h);
        return this.params._popup._hT;
    }
    /**
     *  closeBtn
     *
     *  Return refference to div with Roll
     *  close button
     *
     *  @return object Div
     */
    this.closeBtn = function() {
        return this.params._popup._hB;
    }
    /**
     *  body 
     *
     *  @param html|null h Popup body content
     *  @return Node Body div
     */
    this.body = function (h) {
        if ( h !== undefined && h !== null )
            $(this.params._popup._b).html(h);
        return this.params._popup._b;
    }
    /**
     *  container
     *
     *  @return Node Popup container div
     */
    this.container = function () {
        return this.params._popup._c;
    }

    /**
     *  top
     *
     *  @param int t - top popup distance, px from browser window
     *  @return int, current top position in px
     */
    this.top = function (t) {
        if ( typeof t === 'number' ) {
            this.params.top = t;
            $(this.container()).css('top', t);
        }
        return this.params.top;
    }
    /**
     *  left
     *
     *  @param int l - left popup distance, px from browser window
     *  @return int, current left position in px
     */
    this.left = function (l) {
        if ( typeof l === 'number' ) {
            this.params.left = l;
            $(this.container()).css('left', l);
        }
        return this.params.left;
    }
    /**
     *  width
     *
     *  @param int w - popup width, px
     *  @return current width, px
     */
    this.width = function (w) {
        if ( typeof w === 'number' ) {
            this.params.width = w;
            $(this.container()).css('width', w);
        } else if ( typeof w === 'string' && w === '' ) {
            $(this.container()).css('width', '');
            this.params.width = parseInt($(this.container()).width(), 10);
        }
        return this.params.width;
    }
    /**
     *  height
     *
     *  @param int h - popup height, px
     *  @return int, current popup height, px
     */
    this.height = function (h) {
        if ( typeof h === 'number' ) {
            this.params.height = h;
            $(this.container()).css('height', h);
        } else if ( typeof h === 'string' && h === '' ) {
            $(this.container()).css('height', '');
            this.params.height = parseInt($(this.container()).height(), 10);
        }
        return this.params.height;
    }

    this._mkPopupContainer();
}

FSPopupAbstract.NAME = 'FSPopupAbstract';
FSPopupAbstract.VERSION = '0.1';
FSPopupAbstract.DESCRIPTION = 'Abstract class FSPopupAbstract';

/**
 *  name
 *
 *  @return string Class name
 */
FSPopupAbstract.prototype.name = function () {
    return this.constructor.NAME;
}
/**
 *  version
 *
 *  @return string Class version
 */
FSPopupAbstract.prototype.version = function () {
    return this.constructor.VERSION;
}
/**
 *  description
 *
 *  @return string Class description
 */
FSPopupAbstract.prototype.description = function () {
    return this.constructor.DESCRIPTION;
}
/**
 *  init
 *
 *  Function call when poopup container is created.
 *  It can be used for init container after pasted into DOM
 *
 *  @return void
 */
FSPopupAbstract.prototype.init = function () {
}
/**
 *  show
 *
 *  @abstract
 */
FSPopupAbstract.prototype.show = function() {
    throw new Error("Call 'show' method from Abstaract class. You must implement it in concrete class");
}
/**
 *  hide
 *
 *  @abstract
 */
FSPopupAbstract.prototype.hide = function () {
    throw new Error("Call 'hide' method from Abstaract class. You must implement it in concrete class");
}
/**
 *  CSS z-index
 *  Global parameter for all popups, int
 */
FSPopupAbstract.prototype.zindex = 100;
/**
 * _mkPopupContainer
 *
 * Find popup's container in DOM, and create it if not found
 *
 * @param string|Node|null id Container id or DOM Node or null for create it
 * @return boolean
 */
FSPopupAbstract.prototype._mkPopupContainer = function(popup) {
    
    var popup = this.params._popup;

    var id          = '_popup_' + Math.floor(Math.random()*1000001);
    
    var container   = document.createElement('div');
    var cHeaderT    = document.createElement('div');
    var cHeaderB    = document.createElement('div');
    var cBody       = document.createElement('div');

    $(container).attr('id', id);
    $(container).css('display', 'none');
    $(cBody).attr('id', id+'_body');
    $(cBody).addClass('popup-body');
    $(cHeaderT).attr('id', id+'_header_title');
    $(cHeaderT).addClass('popup-header-title');
    $(cHeaderB).attr('id', id+'_header_button');
    $(cHeaderB).addClass('popup-header-button');

    var roll = this;
    $(cHeaderB).click(function () { roll.hide(); FSWidget.filterActivate(false); });

    if ( this.name() === 'FSPopupRoll' ) {
        $(container).addClass('popup-container-roll');
    } else if ( this.name() === 'FSPopupPopup' ) {
        $(container).addClass('popup-container-roll');
    } else if ( this.name() === 'FSTechSchedule' ) {
        $(container).addClass('popup-container-roll');
    } else if ( this.name() === 'FSWidgetAppliedWork' ) {
        $(container).addClass('popup-container-roll');
        
        //$(container).draggable();
        //$(container).draggable("option", "opacity", 0.35);
        //$(container).resizable();
        
    }
    $(container).css('top', this.top());
    $(container).css('left', this.left());
    $(container).css('width', this.width());
    $(container).css('height', this.height());
    //alert(id);

    container.appendChild(cHeaderT);
    container.appendChild(cHeaderB);
    container.appendChild(cBody);
    $(document).ready( function() {
        $('body')[0].appendChild(container);
    });
    popup._c  = container;
    popup._hB = cHeaderB;
    popup._hT = cHeaderT;
    popup._b  = cBody;

    this.init();
}

