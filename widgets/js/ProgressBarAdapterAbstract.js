function ProgressBarAdapterAbstract( _provider, _container )
 {
    this.provider = function ()
    {
        return _provider;
    }
    
    this._interval      = 50;       //  milliseconds
    this._active        = false;
    this._intervalID    = null;
    this._label         = '';       //  pregress' bar message|label
    this._percent       = 0;        //  0..100
    this._container     = null;     //  container for draw progress bar

    this.container(_container);
}

ProgressBarAdapterAbstract.NAME         = "ProgressBarAdapterAbstract";
ProgressBarAdapterAbstract.VERSION      = "0.1";
ProgressBarAdapterAbstract.DESCRIPTION  = "Abstract class for drawing progress bars";

ProgressBarAdapterAbstract.prototype.name = function ()
{
    return this.constructor.NAME;
}
ProgressBarAdapterAbstract.prototype.version = function ()
{
    return this.constructor.VERSION;
}
ProgressBarAdapterAbstract.prototype.description = function ()
{
    return this.constructor.DESCRIPTION;
}
ProgressBarAdapterAbstract.prototype.label = function ( label )
{
    if ( typeof label === 'string' || typeof label === 'number' ) {
        this._label = label.toString();
    }
    return this._label;
}
ProgressBarAdapterAbstract.prototype.percent = function ( percent )
{
    if ( typeof percent === 'string' || typeof percent === 'number' ) {
        var p = parseInt(percent, 10);
        this._percent = ( p < 0 ) ? 0 : ( p > 100 ) ? 100 : p;  //  0..100
    }
    return this._percent;
}
ProgressBarAdapterAbstract.prototype.container = function ( c ) {
    if ( typeof c === 'object' ) {
        this._container = c;
    } else if ( typeof c === 'string' && c.length ) {
        var e = document.getElementById(c);
        if ( e ) {
            this._container = e;
        } else {
            this._container = null;
        }
    } else if ( c === null ) {
        this._container = null;
    }
    return this._container;
}
ProgressBarAdapterAbstract.prototype.show = function ()
{
    throw new Error('"show" is abstract function, you must implement it');
}
ProgressBarAdapterAbstract.prototype.hide = function ()
{
    throw new Error('"hide" is abstaract function, you must implement it');
}
ProgressBarAdapterAbstract.prototype.reset = function ()
{
    //console.log('reset');
    if ( this._active ) {
        this.hide();
        this.postfree();

        clearInterval(this._intervalID);
        this._intervalID = null;
        this._interval = 50;
        this._active = false;
        this._label = '';
        this._percent = 0;
    }
}
ProgressBarAdapterAbstract.prototype.activate = function ()
{
    //console.log('activate');
    if ( !this._active ) {
        this.prepare();
        this._active = true;
        var self = this;
        this._intervalID = setInterval( function () { self.show.call(self); }, this._interval );
    }
}
ProgressBarAdapterAbstract.prototype.prepare = function ()
{
    //  empty body in abstract class
}
ProgressBarAdapterAbstract.prototype.postfree = function ()
{
    //  empty body in abstract class
}

/**
 * ProgressBarAdapterAbstract 
 * 
 * @param _provider $_provider 
 * @param _container  $_container  
 * @access public
 * @return void
 */
/**
 * ProgressBarAdapterAbstract 
 * 
 * @param _provider $_provider 
 * @param _container  $_container  
 * @access public
 * @return void
 */
