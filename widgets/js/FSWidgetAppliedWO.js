/**
 * FSWidgetAppliedWO Class
 *
 * Widget for Applied Wor Order
 *
 * @param params array
 * @author Alex Scherba
 */
function FSWidgetAppliedWO( params ) {
    this.params = params || {};
    /**
     * init base url for Ajax requests
     */
    if ( this.params.baseUrl === undefined ) {
        this.params.baseUrl = document.location.protocol + '//' + document.location.hostname + '/widgets/';
    }

    this.waitImage = new Image();
    this.waitImage.src = "/widgets/images/wait.gif";     //  pre load wait image

    this._module     = 'dashboard';
    this._controller = 'tech';
    this._action     = 'applied-wo';
}
FSWidgetAppliedWO.NAME           = 'FSWidgetAppliedWO';
FSWidgetAppliedWO.DESCRIPTION    = 'FSWidgetAppliedWO widget class';
FSWidgetAppliedWO.VERSION        = '0.1';
/**
 *  Function return class name
 *  @return string
 */
FSWidgetAppliedWO.prototype.name = function() {
    return this.constructor.NAME;
}
/**
 *  Function return class version
 *  @return string
 */
FSWidgetAppliedWO.prototype.version = function() {
    return this.constructor.VERSION;
}
/**
 *  Function return class description
 *  @return string
 */
FSWidgetAppliedWO.prototype.description = function() {
    return this.constructor.DESCRIPTION;
}
/**
 *  show widgets in container
 *  @return void
 *  @author Alex Scherba
 */
FSWidgetAppliedWO.prototype.show = function () {
    this.showPreloader();
    
    _data = this.params;
    try { _url = this._makeUrl('');}
    catch ( e ) { return; }
    _container = this.params.container
    jQuery.ajax({
        url: _url,
        cache: true,
        type: 'POST',
        dataType: 'html',
        data: _data,
        context: this,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) document.location = "/clients/logIn.php";
            else this.fillContainer("Sorry, but an error occurred. No results found.");
        },
        success : function(data, status, xhr) {
            this.fillContainer(data);
        }
    });
    $('body').css('cursor', '');
}

/**
 *  Function make full valid URL
 *  @param queryString string Additional query string
 *  @param forceNoSSL boolean
 *  @return string
 *  @author Alex Scherba
 */
FSWidgetAppliedWO.prototype._makeUrl = function(queryString, forceNoSSL) {
    queryString = queryString || '/';
    queryString = ( queryString.substring(0,1) !== '/' ) ? '/'+queryString : queryString;
    queryString = ( queryString.substring(queryString.length-1,queryString.length) !== '/' ) ? queryString+'/' : queryString;
    bu = this.params.baseUrl;
    if (forceNoSSL) bu = bu.replace("https", "http");
    var url = bu + (( bu.substring(bu.length-1,bu.length) === '/' ) ? "" : "/");

    return url + this._module + '/' + this._controller + '/' + this._action + queryString;
}
/**
 *  insert html to DOM
 *
 *  @param html string
 */
FSWidgetAppliedWO.prototype.fillContainer = function(html) {
    var _container;
    if ( typeof html === 'string' && html.length > 0 ) {
        if ( typeof this.params.container === 'string' )
            _container = $('#'+this.params.container);
        if ( _container !== null )
            $(_container).html(html);
    }
}
/**
 *  Function displays widget preloader
 */
FSWidgetAppliedWO.prototype.showPreloader = function() {
    $('body').css('cursor', 'wait');
    var img = "<img style='margin:20px 44%;' alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px'/>";
    this.fillContainer(img);
}
/**
 * get Change Bid HTML
 * @param bid string
 * @param comments string
 * @return cbHtml string
 */
FSWidgetAppliedWO.prototype.getChangeBidHtml = function(bid, comments) {
    var cbHtml = "<form method='POST' action='#'><table cellspacing='0'><col class='col1'/><col class='col2' />";
    cbHtml += "<tr><td class='label_field' colspan=2 id='appliedErrors'></td></tr>";
    cbHtml += "<tr><td class='label_field'>Bid Amount: </td><td><input id='edit_BidAmount' value='"+bid+"' type='text'/></td></tr>";
    cbHtml += "<tr><td class='label_field' colspan=2>Comments: <br/><textarea id='edit_Comments'>"+comments+"</textarea></td></tr>";
    cbHtml += "<tr><td class='label_field' colspan=2><br/><input type='button' value='Update' onclick='wd.updateBid()'/></td></tr>";
    cbHtml += "</table></form>";
    return cbHtml;
}
/**
 * Update Bid
 *  @return cbHtml string
 */
FSWidgetAppliedWO.prototype.updateBid = function() {
    newBid = $('#edit_BidAmount').val();
    newComments = $('#edit_Comments').val();
    win = this.params.win;
    jQuery.ajax({
        url: '/widgets/dashboard/tech/update-bid/',
        type: 'POST',
        dataType: 'json',
        data: {win:win, BidAmount:newBid, Comments:newComments},
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) document.location = "/clients/logIn.php";
        },
        success : function(data, status, xhr) {
            if (data.success == 1) {
                $('#BidAmount').html(newBid);
                $('#Comments').html(newComments);
                roll.hide();
            } else {
                $('#appliedErrors').css('color', '#FF0000');
                $.each(data.errors, function(key, value) {$('#appliedErrors').append(value);});
                setTimeout(function(){$('#appliedErrors').hide(2000)},3000);
            }
        }
    });
}