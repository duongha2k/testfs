/**
 * FSWidgetWODetails
 *
 * @param Object|null params
 * @access public
 * @return void
 */
function FSWidgetWODetails ( params, roll ) {
    FSWidget.call(this, params);

    this.params.POPUP.avaliablePopups = {
        quickassigntool : {module:'dashboard',controller:'sections',action:'quickassigntool'},
        filedownload    : {module:'dashboard',controller:'do',action:'file-wos-documents-download'},
        documents       : {module:'dashboard',controller:'details',action:'project-documents'},
        bcatdocuments   : {module:'dashboard',controller:'sections',action:'bcat-documents'}
    };
    this.params.WIDGET.avaliableTabs = {
        index : {module:'dashboard',controller:'details',action:'index'},
        create : {module:'dashboard',controller:'details',action:'create'},
        navigation : {module:'dashboard',controller:'details',action:'navigation'},
		mytechs : {module:'dashboard', controller:'sections', action:'mytechs'}
		
    };
    this._roll = roll;

    this.waitImage = new Image();
    this.waitImage.src = "/widgets/images/progress.gif";     //  pre load wait image
    this.waitImage.width = 190;
    this.waitImage.height = 14;

    this._activeupload = 0;
    this._uploadsCounter = 0;
    this._updatestatus = {success:1, errors:[]};
    this._redirect = false;
    this._reftab = params.reftab;
    this._wosDetails = wosDetails.CreateObject({id:'_wosDetails_Instance'}); ;

    this._customfunctions = params.customfunctions || {};
	
	this._queueSiteState = null;

	this._enablePushWM = false;
}

FSWidgetWODetails.prototype = new FSWidget();
FSWidgetWODetails.prototype.constructor = FSWidgetWODetails;

FSWidgetWODetails.NAME          = 'FSWidgetWODetails';
FSWidgetWODetails.VERSION       = '0.1';
FSWidgetWODetails.DESCRIPTION   = 'Class FSWidgetWODetails';

FSWidgetWODetails.prototype.roll = function() {
    return this._roll;
}

/**
 *  open
 */
FSWidgetWODetails.prototype.open = function(options) {
    var p = options || {};
    var queryString = '/';
    var _url;
    var noSSL = false;

    if ( p.params !== undefined && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params )
            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                } else if ( key === 'projects' ) {
                    var ids = '';
                    for ( var pr=0; pr < p.params[key].length; ++pr ) {
                        ids += p.params[key][pr];
                        if ( pr + 1 < p.params[key].length ) {
                            ids += '|';
                        }
                    }
                    queryString += 'projects/' + ids + '/';
                }
            } else if (p.params[key] === false || p.params[key] === true) {
                /* ok */
            } else if ( p.params[key] !== null && p.params[key] !== "" && p.params[key].toLowerCase() !== 'none' ) {
                if (key === "download") {
                    noSSL = true;
                } else if ( key === 'start_date' || key === 'end_date' ) {
                    p.params[key] = p.params[key].replace(/\//g, '_');
                }
                queryString += key + '/' + p.params[key] + '/';
            }
    }
    try {
        _url = this._makeUrl(p.tab, queryString, noSSL);
    } catch ( e ) {
        return false;
    }
    return location = _url;
}

/**
 *  show
 *
 *  Function display widget
 *
 *  @param Object|null options
 */

FSWidgetWODetails.prototype.show = function(options) {
    //if ( FSWidget.isFilterActive() ) return;
	
    var p = options || {tab:this.currentTab,params:this.getParams()};

    var queryString = '/';
    var _url;

    // TODO params needs be reconfigured
    if ( p.params !== undefined && p.params !== null && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params ) {
            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] === true || p.params[key] === false ) {
                if ( p.params[key] === false ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] !== null && p.params[key] !== "" && p.params[key] !== 'none' ) {
                //queryString += key + '/' + p.params[key] + '/';
            } else {
                delete p.params[key];
            }
        }
    } else {
        p.params = {};
    }

    try {
        if ( p.tab !== undefined && p.tab !== null ) {
            this.currentTab = p.tab;
            _url = this._makeUrl(p.tab, queryString);
            var idx;
            for ( idx in this.params.WIDGET.avaliableTabs ) {
                $('#'+idx).toggleClass('current', idx === this.currentTab);
            }
        } else if ( p.popup !== undefined && p.popup !== null ) {
            _url = this._makeUrl(p.popup, queryString);
        }
    } catch ( e ) {
        return false;
    }

    this.showPreloader();

    if ( this.tabOpenXhrRequest ) {
        this.tabOpenXhrRequest.abort();
        this.tabOpenXhrRequest = null;
    }

    version = $('#dashboardVersion').val();
    if(version != '' && version == 'Lite'){
	version = "Lite";
        p.params.version = version;
        $('#deactivated').addClass("displayNone");
    } else {
        version = "Full";
//        var defVer = FSWidget.getCookie("dashboardVersionDefault");
        p.params.version = version;
        $('#deactivated').removeClass("displayNone");
    }
    $('#dashboardVersion').val(version);
    setCookie("dashboardVersionDefault", version);
    this.tabOpenXhrRequest = $.ajax({
        url : _url,
        data : p.params,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/clients/logIn.php";
            } else {
                FSWidget.fillContainer("Sorry, but an error occurred. No results found.", this);
            }
            this.tabOpenXhrRequest = null;
        },
        success : function(data, status, xhr) {
            $('body').css('cursor', '');
            FSWidget.fillContainer(data, this)
            calendarInit();
            this.initCurrency();
            this.initPhone();
            
            this._wosDetails.detailsWidget = this;
            this._wosDetails.params = p.params;
            _wosDetails = this._wosDetails;
            this._wosDetails.buildEvent();
            
			$("#workOrderOwner").change(function() {
				detailsWidget.autofillCommInfo($("#workOrderOwner").val(), true);
			});

            if(p.params.project != null){
            	this.autofillProjectInfo(p.params.project, true);
            }
            
            if(p.params.techID != null){
            	this.autofillTechInfo(p.params.techID);
            }
			this.resizeFrame();
			try {
				if (this.publish == undefined) {}
				else if (this.publish == 1)
					$('#edit_ShowTechs').attr("checked", true)
				else
					$('#edit_ShowTechs').removeAttr("checked");
				this.publish = undefined;
			} catch (e) {}
			try {
				if (p.sectionTab == undefined)
					return;
				$('#' + p.sectionTab + 'tab').click();
			} catch (e) {}
                       
        }
    });

}

FSWidgetWODetails.prototype.showPreloader = function() {
    if (this._customfunctions.showPreloader !== undefined && typeof this._customfunctions.showPreloader === 'function' ) {
        this._customfunctions.showPreloader();
    }else{
        $('body').css('cursor', 'wait');
        var img = "<div class='tCenter' ><img style='margin:10% auto;' alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></div>";
        FSWidget.fillContainer(img, this);
    }
}
FSWidgetWODetails.prototype.doFind = function() {
	var win = $('#fMTrdWIN').val();
	var clientWOrd = $('#fMTClientWO').val();
	var zip = $('#fMTZip').val();
	var distance = $('#fMTDistance').val();
	var isDash = $('#isDash').val();


	var wd = this._wd;
	var newwindow;

	if ((zip.length > 0 || win.length > 0 || clientWOrd.length > 0) && distance > 0) {
		if(isDash == '1'){
			$(document).ready(function() {
			hideSidebar();
			$('#mytechs').html('');
			$('<iframe />').attr('src', '/clients/findMyTechs.php?v=' + wd.filters().company() + '&win=' + escape(win) + '&clientWO=' + escape(clientWOrd) + '&zip=' + escape(zip) + '&distance=' + escape(distance)).attr("width", "100%").attr('height', '100%').appendTo($('#detailsContainer', window.parent.document));
			});
		} else {
			newwindow = window.open('/clients/findMyTechs.php?v=' + wd.filters().company() + '&win=' + escape(win) + '&clientWO=' + escape(clientWOrd) + '&zip=' + escape(zip) + '&distance=' + escape(distance),'name','height=600, width=950, left=100, top=100, resizable=yes, scrollbars=yes, toolbar=no,status=no');
		}
		if(isDash != '1'){
			newwindow.focus();
		}
		this._roll.hide();
	}
	
}
FSWidgetWODetails.prototype.copyWO = function() {
		this.showCopyWOPopup();
	
	   /* this.showStartPopup();
	    this._redirect = false;
	    var popup = openPopupWin('', '_blank', {}, true);
	    $.ajax({url: '/widgets/dashboard/do/copy-work-order/',
	            cache: false,
	            dataType: 'json',
	            data: {win:this.params.win, company: window._company},
	            type: 'post',
	            context: this,
	            success     : function (data, status, xhr) {
	                if (data.success) {
	                	popup.location = '/clients/wosDetails.php?v='+window._company+'&id='+data.data;
	                	popup.focus();
	                    this._roll.hide();
	                }else{
	                	popup.close();
	                    this.showMessages(data);
	                }
	            }
	        });
	        */
}

FSWidgetWODetails.prototype.copyWOProceed = function(keepStartDate, keepAssignedTech, copyToCompany, copyToProject) {
	this.showStartPopup();
    this._redirect = false;
//    var popup = openPopupWin('', '_blank', {}, true);
    var redirCompany = "";
    
    
    $.ajax({url: '/widgets/dashboard/do/copy-work-order/',
            cache: false,
            dataType: 'json',
            data: {win:this.params.win, company: window._company, keepStartDate: keepStartDate, keepAssignedTech: keepAssignedTech, copyToCompany: copyToCompany, copyToProject: copyToProject},
            type: 'post',
            context: this,
            success     : function (data, status, xhr) {
                if (data.success) {
                	if(copyToCompany !== undefined){
                		location = '/clients/wosDetails.php?v='+copyToCompany+'&id='+data.data+'&tab=full';
                	}else{
                		location = '/clients/wosDetails.php?v='+window._company+'&id='+data.data+'&tab=full';
                	}
//                	popup.focus();
                    this._roll.hide();
//                    window.close();
                }else{
//                	popup.close();
                    this.showMessages(data);
                }
            }
        });
}

FSWidgetWODetails.prototype.updateDetails = function() {
    //if(!this.onBluer(jQuery("#edit_Route"))) return;
    var el = this;

    detailObject.onInit.checkApproved({
        TechClaim:'tech_claim',
        bcatapproved:'bcatapproved',
        isApproved:'edit_Approved',
        el:this,
        onSubmit: function(el){
            
            el.showStartPopup();
            el._redirect =true;
            el._updatestatus = {success:1, errors:[]};
            el._activeupload = 0;

			FSMultiselectControl.submit ();

			params = $('#updateDetailsForm').serialize();
			params.company = window._company;
            if(typeof(tinyMCE.get('edit_Description')) != 'undefined')
                params += "&Description="+encodeURIComponent(tinyMCE.get('edit_Description').getContent());
            if(typeof(tinyMCE.get('edit_Requirements')) != 'undefined')
                params += "&Requirements="+encodeURIComponent(tinyMCE.get('edit_Requirements').getContent());
            if(typeof(tinyMCE.get('edit_SpecialInstructions')) != 'undefined')
                params += "&SpecialInstructions="+encodeURIComponent(tinyMCE.get('edit_SpecialInstructions').getContent());
            if(typeof(tinyMCE.get('edit_Ship_Contact_Info')) != 'undefined')
                params += "&Ship_Contact_Info="+encodeURIComponent(tinyMCE.get('edit_Ship_Contact_Info').getContent());
            
            if(typeof(el.ServiceSchedule) != 'undefined' && el.ServiceSchedule != "") params += el.ServiceSchedule != ""?"&ServiceSchedule="+Base64.encode(el.ServiceSchedule):"";
            if(typeof(el.AllWOVisitList) != 'undefined' && el.AllWOVisitList != "") params += el.AllWOVisitList != ""?"&AllWOVisitList="+Base64.encode(el.AllWOVisitList):"";
            
            
			$.ajax({
				url         : '/widgets/dashboard/details/update/',
				dataType    : 'json',
				data        : params,
				cache       : false,
				type        : 'post',
						context     : el,
				success     : function (data, status, xhr) {
					this._updatestatus = data;
					if (data.success) {
						this.params.win = data.win;
						this._uploadsCounter = 0;

						fileFields = $('#UploadWOPicsNew input:file');
						fileFieldCount= fileFields.length;
						for ( var i = 1; i <= fileFieldCount; i++ ) {
							if ( $('#PicWO'+i).val() && $('#PicWO'+i).val() != '' ) {
								this._uploadsCounter++;
								this.asyncUploadFile('WO'+i);
							}
						}

						fileFields = $('#UploadTechPicsNew input:file');
						fileFieldCount= fileFields.length;
						for ( var i = 1; i <= fileFieldCount; i++ ) {
							if ( $('#PicTech'+i).val() && $('#PicTech'+i).val() != '' ) {
								this._uploadsCounter++;
								this.asyncUploadFile('Tech'+i);
							}
						}

						var signoffFile = $("#UploadCustomSignOff input:file").val();

						if (signoffFile) {
							this._uploadsCounter++;
							this.asyncUploadFile ("CustomSignOff");
						}

						//no files to upload
						if ( this._uploadsCounter <= 0 ) {
							this.showMessages(this._updatestatus);
						}
					}else{
						this.showMessages(this._updatestatus);
					}
				},
				error     : function (data, status, xhr) {
					//console.log (data, status, xhr);
				}
			});
		}
    });
}

FSWidgetWODetails.prototype.autofillTechInfo = function (tid){
	if(!tid){
		return;
	}
	 $.ajax({
	        url         : '/widgets/dashboard/popup/get-tech-info/',
	        dataType    : 'json',
	        data        : {tech:tid,company:window._company},
	        cache       : false,
	        type        : 'post',
	        context     : this,
	        success     : function (data, status, xhr) {
	            if (data) {
	                //AUTOFILL
                        jQuery(".cmdtechScheduleImg").attr("src","/widgets/css/images/Btn_ViewTechSchedule.png");
                        jQuery(".cmdtechScheduleImg").unbind("click").bind("click",showTechSched);
                        jQuery(".cmdtechScheduleImg").css("cursor","pointer");
                        jQuery(".saveTechToSiteImg").attr("src","/widgets/css/images/Btn_SaveTech.png");
                        jQuery(".saveTechToSiteImg").unbind("click").bind("click",_wosDetails.saveTechToSiteImg_onclick);
                        jQuery(".saveTechToSiteImg").css("cursor","pointer");
	            	$('#edit_Tech_ID').val( data.techInfo.TechID );
	            	$('#edit_Tech_FName').val( data.techInfo.fName );
	            	$('#edit_Tech_LName').val( data.techInfo.lName );
	            	$('#edit_TechPhone').val( data.techInfo.phone );
	            	$('#edit_TechEmail').val( data.techInfo.email );
	            	$('#edit_FLS_ID').val( data.techInfo.FLSID );
	               
	            }else{
                        jQuery(".saveTechToSiteImg").attr("src","/widgets/css/images/Btn_SaveTech_g.png");
                        jQuery(".saveTechToSiteImg").unbind("click");
                        jQuery(".cmdtechScheduleImg").css("cursor","default");
                        jQuery(".saveTechToSiteImg").css("cursor","default");
                        jQuery(".cmdtechScheduleImg").unbind("click");
                        jQuery(".cmdtechScheduleImg").attr("src","/widgets/css/images/Btn_ViewTechSchedule_g.png");
                    }
                        //end if data

	        }//end success
	    });//end ajax request
}

FSWidgetWODetails.prototype.autofillCommInfo = function (username, woOwner) {
    //14052
    var pid=$('#edit_Project_ID').val();
    if (!pid || pid=='') {
        return;
    }
    $.ajax({
        url         : '/widgets/dashboard/js/project-info/',
        dataType    : 'json',
        data        : {username:$("#workOrderOwner").val(), id:pid,company:window._company,siteNumber:jQuery("#edit_SiteNumber").val()},
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
		if (data) {             
                for(var index in data) {
                    var element = $('#edit_'+index);
                    var value = data[index]?data[index]:'';
                    if (index == 'ProjectManagerName' || index == 'ProjectManagerEmail' || index == 'ProjectManagerPhone' 
                        || index == 'ResourceCoordinatorName' || index == 'ResourceCoordinatorEmail' || index == 'ResourceCoordinatorPhone' 
                        || index == 'ACSNotifiPOC'|| index == 'ACSNotifiPOC_Phone' || index == 'ACSNotifiPOC_Email' 
                        || index == 'EmergencyName' || index == 'EmergencyPhone' || index == 'EmergencyEmail' 
                        || index == 'TechnicalSupportName' || index == 'TechnicalSupportPhone' || index == 'TechnicalSupportEmail'
                        || index == 'CheckInOutName' || index == 'CheckInOutNumber' || index == 'CheckInOutEmail')
                        {				
                            if (value) $(element).val(value);
                        }                  
                }
		}
		}
    });
};// End 14052

FSWidgetWODetails.prototype.autofillProjectInfo = function (pid, isDash, onSuccess, woOwner) {
    if (!pid) {
        return;
    }
    var companyid=window._company;
    $.ajax({
        url         : '/widgets/dashboard/js/project-info/',
        dataType    : 'json',
        data        : {username:$("#workOrderOwner").val(), id:pid,company:window._company,siteNumber:jQuery("#edit_SiteNumber").val()},
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            if (data) {
                
            	//AUTOFILL
				try {oldProj = oldProject;} catch (e) {oldProj = "";}
                
                for(var index in data) {
					var element = $('#edit_'+index);
                    var value = data[index]?data[index]:'';
                    //732
                    if (index == 'SiteContactReminderCall') {
                        if (value==1) {
                            var SiteContactReminderCallHours = data['SiteContactReminderCallHours'];
                            $("#SiteContactReminderCalllabel").append(": " + SiteContactReminderCallHours + " hours before start");
                        }
                    }
                    //end 732
                    //--- 13713
                    if (index == 'AllowDeVryInternstoObserveWork') {
                        if (value==1) {
                            $('#edit1_AllowDeVryInternstoObserveWork').attr('checked',true);
                        } else {
                            $('#edit1_AllowDeVryInternstoObserveWork').attr('checked',false);
                        }
                    }
                    if (index == 'AllowDeVryInternstoPerformField') {
                        if (value==1) {
                            $('#edit1_AllowDeVryInternstoPerformField').attr('checked',true);
                        } else {
                            $('#edit1_AllowDeVryInternstoPerformField').attr('checked',false);
                        }
                    }
                    //--- end 13713
 //13763
                                        if (index == 'WorkAcceptedByTechEmailTo') {
						if (value) $(element).val(value);
						$("#edit_ReceiveWorkAcceptedByTechEmail").attr('checked', $(element).val() ? true : false);
					}
                                        if (index == 'WorkConfirmedByTechEmailTo') {
						if (value) $(element).val(value);
						$("#edit_ReceiveWorkConfirmedByTechEmail").attr('checked', $(element).val() ? true : false);
					}
                                        if (index == 'TechCheckedInEmailTo') {
						if (value) $(element).val(value);
						$("#edit_ReceiveTechCheckedInEmail").attr('checked', $(element).val() ? true : false);
					}
                                        if (index == 'TechCheckedOutEmailTo') {
						if (value) $(element).val(value);
						$("#edit_ReceiveTechCheckedOutEmail").attr('checked', $(element).val() ? true : false);
					}
                    
                    //end 13763
                    
					if (index == 'RecruitmentFromEmail') {
						if (value) $(element).val(value);
					}
					if (index == 'BidNotificationEmails') {
						if (value) $(element).val(value);
						$("#edit_NotifyOnBid").attr('checked', $(element).val() ? true : false);
					}
					if (index == 'AssignmentNotificationEmail') {
						if (value) $(element).val(value);
						$("#edit_NotifyOnAssign").attr('checked', $(element).val() ? true : false);
					}
                    if (index == 'ReceiveTechQAMessageNotification') {
                        if (value) $(element).val(value);
                        $("#edit_ReceiveTechQAMessageNotification").attr('checked', $(element).val() ? true : false);
                    }
					if (index == 'WorkDoneNotificationEmails') {
						if (value) $(element).val(value);
						$("#edit_NotifyOnWorkDone").attr('checked', $(element).val() ? true : false);
					}
					if (index == 'P2TNotificationEmail') {
						if (value) $(element).html(value);
						$("#edit_NotifyOnWorkDone").attr('checked', $(element).html() ? true : false);
					}
					if (index == 'ClientCredential') {
                        var isProjectAutoAssign = data["isProjectAutoAssign"];
						//for (index2 in value) {
                            if(companyid=='MDS')
                            {
                            value = '<select size="1" disabled="disabled"  id="ClientCredential" >   <option selected="selected" value="">Select a Client Credential</option>    <option value="5">Trapollo Medical Certified</option>   <option value="18">Vonage Plus</option>  <option value="31">Craftmatic Certified</option> </select><input type="hidden" name="CredentialCertificationId" id="CredentialCertificationId" />';
                            }
                        if(companyid=='COMPU')
                        {
                            value = '<select size="1" disabled="disabled"  id="ClientCredential" >   <option selected="selected" value="">Select a Client Credential</option>    <option value="33">CompuCom Technician</option> <option value="34">CompuCom Analyst</option> <option value="35">CompuCom Specialist</option> </select><input type="hidden" name="CredentialCertificationId" id="CredentialCertificationId" />';
                        }
					}
                    //13376
					
					if(index == 'SignOffSheet_Required'){
						var numDocsReq = value;
						$('#SignOffSheet_Required').val(value);
						
					}
                   
                    if(index == 'Project_ID')
                    {
                        $.ajax({
                            url         : '/widgets/dashboard/details/expense-reporting-tech/',
                            data        : "Project_ID="+value,
                            cache       : false,
                            type        : 'post',
                            context     : this,
                            success     : function (data, status, xhr) {
                                //376
                                if(data=="")
                                {
                                    $("#ExpenseReporting_collaspleLabel").css("display","none");
                                    $("#ExpenseReporting_collasple").css("display","none");
                                    jQuery("#divExpenseReportingLine").hide();
                                }
                                else
                                {
                                    $("#ExpenseReporting_collaspleLabel").css("display","");
                                    $(".sectionBoxNew").attr('class','sectionBoxNew sectionOpen')
                                    jQuery("#ExpenseReporting_collasple").html(data);
                                    $("#ExpenseReporting_collasple").css("display","block");
                                    jQuery("#divExpenseReportingLine").show();
                                }
                                //end 376
                            }});
                        
                    }     
                    if(index == 'StartDate'
                        || index == 'StartTime'
                        || index == 'EndDate'
                        || index == 'EndTime'
                        || index == 'StartRange'
                        || index == 'Duration'
                        || index == 'Amount_Per'
                        || index == 'Qty_Devices'
                        || index == 'Qty_Visits'        
                )
                    {
                        	if (jQuery.trim (value) != "") jQuery(element).val(value);
	                        if(index == 'Amount_Per' && value == 'Visit')
	                        {
	                            $('#perLabel').html('per Visit');
	                            $('#perLabel2').html('per Visit');
                            //$('#DeviceQuantity').html('# of Visits');
	                            $('#basetechpay').html('$/Visit x #Visits');
                            $("#edit_Qty_Devices").removeAttr("readonly");
	                        }
                        else if(index == 'Amount_Per'){
                            //$('#DeviceQuantity').html('Device Quantity');
                        }
                        else if(index == 'Amount_Per')
	                    	{
	                             $('#perLabel').html('per '+$('#edit_Amount_Per option:selected').val());
	                            $('#perLabel2').html('per '+$('#edit_Amount_Per option:selected').val());
	                            $('#basetechpay').html('Base Tech Pay:');
	                        }
                    	}
                    else if(index == "Description"){

                        if($.trim(oldProj) != "NewRequest" && typeof(tinyMCE.get('edit_Description')) != 'undefined' && 
							$.trim(tinyMCE.get('edit_Description').getContent()) == "") 
								tinyMCE.get('edit_Description').setContent(value);
                      }
                      else if(index == "Requirements"){
                        if ($.trim(oldProj) != "NewRequest" && typeof(tinyMCE.get('edit_Requirements')) != 'undefined' && 
							$.trim(tinyMCE.get('edit_Requirements').getContent()) == "")
								tinyMCE.get('edit_Requirements').setContent(value);
                      }
                      else if(index == "SpecialInstructions"){
                         if ($.trim(oldProj) != "NewRequest" && typeof(tinyMCE.get('edit_SpecialInstructions')) != 'undefined' && 
							$.trim(tinyMCE.get('edit_SpecialInstructions').getContent()) == "")
						 		tinyMCE.get('edit_SpecialInstructions').setContent(value);
                      }
                      else if(index == "SpecialInstructions_AssignedTech"){
                            if ($.trim(oldProj) != "NewRequest" && typeof(tinyMCE.get('edit_Ship_Contact_Info')) != 'undefined' && 
							$.trim(tinyMCE.get('edit_Ship_Contact_Info').getContent()) == "")
								tinyMCE.get('edit_Ship_Contact_Info').setContent(value);
                    }
					else if(index == 'PcntDeductPercent')
                    {
                        if(value == "0" || value == "")
                        {
                            jQuery("#lablePcntDeductPercent").html('Deduct FS Service Fee from Tech');
                            value = "0";
                    }
                        else if(value*100 == 100)
                        {
                            jQuery("#lablePcntDeductPercent").html('This Work Order will be paid outside of the FieldSolutions Platform');
                        }
                        else{
                            jQuery("#lablePcntDeductPercent").html('Deduct '+ value*100+'% FS Service Fee from Tech');
                        }  
                        jQuery("#edit_PcntDeductPercent").val(value);
                    }    
                    else if(index == 'PcntDeduct')
                    {
                        if(data['PcntDeductPercent'] != "" && data['PcntDeductPercent'] != "0")
                        {
                            jQuery("#edit_PcntDeduct").attr("checked",true);
                            jQuery("#PcntDeductV2").attr("checked",true);
                        }    
                        else
                        {
                            jQuery("#edit_PcntDeduct").attr("checked",false);
                            jQuery("#PcntDeductV2").attr("checked",false);
                        }    
                    }    
                    else if(index == 'TechsInSite'){
                        if(value){
                            jQuery(".viewTechBtnHidden").show();
                            jQuery(".viewTechBtnHiddenView").hide();
                        }else{
                            jQuery(".viewTechBtnHidden").hide();
                            jQuery(".viewTechBtnHiddenView").show();
                        }
                    }
                    else if(index == 'ReminderCustomHrChecked' || index == 'ReminderCustomHrChecked_2' || index == 'ReminderCustomHrChecked_3')
                    {
                        if(value!=='0' && value!=='') {
                            element.attr('checked','checked');
                        }else{
                            element.removeAttr('checked');
                        }
                    }
                    else if(index == 'ReminderCustomHr' || index == 'ReminderCustomHr_2' || index == 'ReminderCustomHr_3')
                    {
                        if(value!=='0' && value!='') {
                            jQuery("#" + index).html(value+' hour before start time reminder');
                            jQuery("#tr"+index).css('display','table-row');
                        }
                        else
                        {
                            jQuery("#tr"+index).css('display','none');
                        }
                    }
                    else if (jQuery(element).length >0 && index != 'Project_ID') {
                        if (element.attr('type') == 'checkbox') {
                            if(value!=='0') {
                                element.attr('checked','checked');
                            }else{
                                element.removeAttr('checked');
                            }
                            
                        }else if (element.get(0).tagName=='INPUT' || element.get(0).tagName=='SELECT' || element.get(0).tagName=='TEXTAREA'){
							if (element.attr ("type") != "radio") {
								var elemVal;
								try {
									elemVal = $.trim(element.val() );
								} catch (e) {
									elemVal = element.val();
									var elemStr = new String(elemVal);
									elemVal = elemStr.replace(/^\s+|\s+$/g, '');
								}
								if (elemVal == ' ') {
									elemVal = '';
								}
								if(index=="Type_ID" && value!="")
								{
									element.val(value);
								}
								if(this.currentTab == "index"){
									//if (oldProj != "Default Project"  && (index != "Headline" || _company != "FLS" || elemVal != "NEW-REQUEST")) continue;
								}else{
								}
								
								//976
								if(index ="PO")
								{
								element.val(value);
								}
								else if (woOwner === undefined && $.trim(oldProj) != "NewRequest" && elemVal != "" && elemVal != 0.00 && (index != "Headline" || _company != "FLS"  || elemVal != "NEW-REQUEST")) continue;                                                   
								//end 976
								element.val(value);
								element.change();
							}
                        } else {
							if(index == "ClientCredential" && data["CredentialCertificationId"] != null){
                                    jQuery("#ClientCredential").val(data["CredentialCertificationId"]);
                                    jQuery("#CredentialCertificationId").val(data["CredentialCertificationId"]);
                            }
							else {
								element.html(value);
							}
                        }
                    }
                }
                if(this._wosDetails != null){
                    this._wosDetails.setEditAmountPerContent();
                    if(this.currentTab == "create") {
                        this._wosDetails.autoFillProjectVisit(data);
                        this._wosDetails.editQtyDevices_onChange();
                    }
                }
                if(isDash == true){
                	$('#edit_Project_ID option:selected').val( data.Project_ID ).attr('selected',true);

                }
                
                //update missing fields.
                if (data.isWorkOrdersFirstBidder != "1") {
                    $("#edit_isnotWorkOrdersFirstBidder").attr('checked','checked');
                    $("#edit_isWorkOrdersFirstBidder").removeAttr('checked');
                }else{
                    $("#edit_isnotWorkOrdersFirstBidder").removeAttr('checked');
                    $("#edit_isWorkOrdersFirstBidder").attr('checked','checked');
                }
                if (data.P2TPreferredOnly != "1") {
                    $("#edit_notP2TPreferredOnly").attr('checked','checked');
                    $("#edit_P2TPreferredOnly").removeAttr('checked');
                }else{
                    $("#edit_notP2TPreferredOnly").removeAttr('checked');
                    $("#edit_P2TPreferredOnly").attr('checked','checked');
                }
                if (data.FixedBid != "1") {
                    $("#edit_FixedBidNotice").addClass('displayNone');
                }else{
                    $("#edit_FixedBidNotice").removeClass('displayNone');
                }

                // Set Up Accept replies
                if ( data['RecruitmentFromEmail'] != 'no-replies@fieldsolutions.us' ) {
                    $("#edit_AcceptReplies").attr('checked','checked');
                } else {
                    $("#edit_AcceptReplies").removeAttr('checked');
                }

                //002
                if ( data['isProjectAutoAssign'] == '1' )
                {
					this.enableAutoAssign (true);

					if (typeof data["RequiredCerts"] != "undefined") {
						FSMultiselectControl.deselectAll ();

						setTimeout (function () {
							for (type in data["RequiredCerts"]) {
								for (value in data["RequiredCerts"][type]) {
									cert_controls[type].selectOption (
										value, 
										cert_controls[type].getLevel (data["RequiredCerts"][type][value])
									);
								}
							}
						}, 250);
					}

					this.requireAddress (true);
                    
                    $("#lblWorkStart").html('<label class="required">Work Start<span>*</span></label>');
                    $("#lblWorkEnd").html('<label class="required">Work End<span>*</span></label>');
                    
                    el = $('#sec4');
                    var i = 0;
                    if(!el.parents(".sectionBox").hasClass("sectionOpen")){
                        el.parent().next(".sectionInner").slideDown("slow");
                        el.parents(".sectionBox").addClass("sectionOpen");
                        i++;
                    }
                    if (!$('#updateDetailsForm').find(".sectionBox").hasClass('sectionOpen')) {
                       $('#all_btn').attr('title','Expand Entire Work Order').children('.all_btn').addClass('btnClosed').removeClass('btnOpened');
                       i++;
                    }
                    if(!$("#pushtotech_collasple").hasClass("sectionOpen")){
                        $("#pushtotech_collasple").slideDown("slow");
                        $("#pushtotech_collasple").addClass("sectionOpen");
                        $(this).parents(".sectionBox2").addClass("sectionOpen");
                        $("#pushtotech_collasple").parents().children(".sectionBoxP2t").addClass("sectionOpen");
                    }
                }
                else
                {
					this.enableAutoAssign (false);

					FSMultiselectControl.deselectAll ();

					this.requireAddress (false);

                    if(data['StartTimeRequired']===1)
                    {
                        $("#lblWorkStart").html('<label class="required">Work Start<span>*</span></label>');
					}
                    if(data['EndTimeRequired']===1)
                    {
                        $("#lblWorkEnd").html('<label class="required">Work End<span>*</span></label>');
					}
				}
                //end 002

				if (data["SignOff_Disabled"] != "1") {
					$("#edit_SignOff_Disabled").attr ("checked", "checked");

					if (data["CustomSignOff"] != null && data["CustomSignOff_Enabled"]) {
						$("#CustomSignOff_Link").attr ("href", data["CustomSignOffLink"]);
						$("#CustomSignOff_Link").text (data["CustomSignOffName"]);
						$("#CustomSignOff_Filename").val (data["CustomSignOff"]);
						$("#edit_CustomSignOff_Enabled[value='1']").attr("checked", "checked");
					}
				else {
						$("#PicCustomSignOff").val ("");
						$("#edit_CustomSignOff_Enabled[value='0']").attr("checked", "checked");
				}
				}
				else {
					$("#edit_SignOff_Disabled").removeAttr ("checked");
				}

				this.updateSignOffDisplay ();

                //P2T Time remaining
                var minutes = data['MinutesRemainPublished'];
				$("#edit_MinutesRemainPublished").val(minutes);
                $("#edit_P2TM").val(minutes%60);
                minutes -= minutes%60;
                $("#edit_P2TH").val(minutes%1440/60);
                minutes -= minutes%1440;
                $("#edit_P2TD").val(minutes/1440);
				
				count = data['SiteCount'];
				this.selectSiteButtonEnable(count && count > 0);
                //--- 13329      
                var  certId = data['CredentialCertificationId'];
                var certName = "";
                if(certId==20) certName = 'LMS Preferred Technician';
                if(certId==21) certName = 'LMS+ Key Contractor';           
                $("#CredentialCertificationName").html(certName); 
                $("#edit_CredentialCertificationId").val(certId);
                //--- 13329 end                
                //13916
                if (woOwner !== undefined)
                {
                    var qtyVisits = data['Qty_Visits'];	               
                    if (qtyVisits == null){ qtyVisits = '';}
                    var techArr = data['TechArrivalInstructions'];	
                    $("#edit_Qty_Visits").val(qtyVisits);
                    $("#edit_TechArrivalInstructions").val(techArr);
                }
			}else{

            }
            // fill Automated Contact Services
            var totalACSServices = 0;
            jQuery("#contentACSServices .ACSServices").each(function(){
               if(jQuery(this).is(":checked")){
                   totalACSServices++;
               }
			});
            var totalClientEmailNotification = 0;
            jQuery("#contentClientEmailNotification .ClientEmailNotification").each(function(){
               if(jQuery(this).is(":checked")){
                   totalClientEmailNotification++;
               }
            });
            jQuery("#totalACSServices").html(totalACSServices);
            jQuery("#totalClientEmailNotification").html(totalClientEmailNotification);
            //end Automated Contact Services

			if (typeof paytool_ctrl != "undefined" && paytool_ctrl != null) paytool_ctrl.update ();	//13916

			try { saveCurrentProject();} catch (e) {}
			if(typeof(onSuccess) == 'function'){
				onSuccess();
			}
        }
    });
};

FSWidgetWODetails.prototype.createWO = function() {
	

    this.showStartPopup();
    this._updatestatus = {success:1, errors:[]};

	FSMultiselectControl.submit ();

    params = $('#createDetailsForm').serialize();
    params.company = window._company;
    
    if(typeof(tinyMCE.get('edit_Description')) != 'undefined')
                params += "&Description="+encodeURIComponent(tinyMCE.get('edit_Description').getContent());
    if(typeof(tinyMCE.get('edit_Requirements')) != 'undefined')
        params += "&Requirements="+encodeURIComponent(tinyMCE.get('edit_Requirements').getContent());
    if(typeof(tinyMCE.get('edit_SpecialInstructions')) != 'undefined')
        params += "&SpecialInstructions="+encodeURIComponent(tinyMCE.get('edit_SpecialInstructions').getContent());
    if(typeof(tinyMCE.get('edit_Ship_Contact_Info')) != 'undefined')
        params += "&Ship_Contact_Info="+encodeURIComponent(tinyMCE.get('edit_Ship_Contact_Info').getContent());

    if(typeof(this.ServiceSchedule) != 'undefined'  && this.ServiceSchedule != "") params += this.ServiceSchedule != ""?"&ServiceSchedule="+Base64.encode(this.ServiceSchedule):"";
    
    $.ajax({
        url         : '/widgets/dashboard/details/do-create/',
        dataType    : 'json',
        data        : params,
        cache       : false,
        type        : 'post',
        context     : this,
		error		: function (data, status, xhr) {
			//console.log (data, status, xhr);
		},
        success     : function (data, status, xhr) {
            this._updatestatus = data;
            if (data.success) {
                this._redirect = true;
                this.params.win = data.win;
                
                fileFields = $('#UploadWOPicsNew input:file');
                fileFieldCount= fileFields.length;
                for ( var i = 1; i <= fileFieldCount; i++ ) {
                    if ( $('#PicWO'+i).val() && $('#PicWO'+i).val() != '' ) {
                        this._uploadsCounter++;
                        this.asyncUploadFile('WO'+i);
                    }
                }

                var signoffFile = $("#UploadCustomSignOff input:file").val();

                if (signoffFile) {
                	this._uploadsCounter++;
                	this.asyncUploadFile ("CustomSignOff");
                }

                if ( this._uploadsCounter <= 0 ) {
                    this.showMessages(this._updatestatus);
                }
            }else{
                this.showMessages(this._updatestatus);
            }
        }
    });
}

FSWidgetWODetails.prototype.showStartPopup = function() {
    var html = "<div class=\"tCenter\"><img alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></div>";
    this._roll.autohide(false);
    var opt = {
        width       : 300,
        height      : '',
        position    : 'middle',
        title       : 'Processing',
        body        : html
    };
    return this._roll.showNotAjax(null, null, opt);
}

FSWidgetWODetails.prototype.changeCopyWOSubmitBtn = function(){
	if($("#assignedTech").attr("checked") == true){
		$("#copyWOSubmit").val("Create & Assign").css({'background':'url("/widgets/images/button_middle.png") no-repeat scroll left top transparent','width':'140px'});
	}else{
		$("#copyWOSubmit").val("Create").css({'background':'url("/widgets/images/button_popup.png") no-repeat scroll left top transparent', 'width':'60px'});
	}
}

FSWidgetWODetails.prototype.showCopyWOPopup = function() {
	var params = this.getParams();
	    $.ajax({
	        url         : '/widgets/dashboard/do/global-pm-drop-down/',
	        dataType    : 'json',
	        data        : params,
	        cache       : false,
	        type        : 'post',
	        context     : this,
	        success     : function (data, status, xhr) {
	            this._updatestatus = data;

	            if (data) {
	            	var html = '<div class="tCenter"><table style="text-align: left; margin-right:auto; margin-left: auto;"><tbody>';
	            	
						if(data.isPM == "true"){
							html += '<tr><td colspan="2" align="left">Select Company</td></tr>';
							html += '<tr><td colspan="2"><select id="copyToCompany" onchange="detailsWidget.onChangeDropDownCompany($('+"'#copyToCompany option:selected'"+').val() );">';
								for(index in data.companies){
									html += '<option value="' + data.companies[index] + '"';
									if(data.companies[index] == params.company){
										html += "selected='selected'";
									}
									html += ' >' + index + '</option>';
								}
							html += '</select></td></tr>';
							
							html += '<tr><td colspan="2" align="left">Select Project</td></tr>';
						    html += '<tr><td colspan="2"><div id="dropDownProjects"><select id="copyToProject" ">';
								for(index in data.projects){
										html += '<option value="' + data.projects[index]  + '"';
										if(data.projects[index] == data.defaultProject){
											html += "selected='selected'";
										}
										html += ' >' + index  + '</option>';
								}
								html += '</div></select></td></tr>';		
						  }
						   
					html += '<tr><td>Keep Start Date:</td><td><input type="checkbox" class="link_button_popup" name="startDate" id="startDate" /></td></tr>';
					html += '<tr><td>Keep Assigned Tech:</td><td><input type="checkbox" class="link_button_popup" onclick="javascript:detailsWidget.changeCopyWOSubmitBtn();"  name="assignedTech" id="assignedTech" /></td></tr>';
					html += '<tr><td colspan="2" align="center"><input type="button" class="link_button_popup" id="copyWOSubmit"  onclick="detailsWidget.copyWOProceed($('+"'#startDate'"+').attr('+"'checked'"+'), $('+"'#assignedTech'"+').attr('+"'checked'"+'), $('+"'#copyToCompany option:selected'"+').val(), $('+"'#copyToProject option:selected'"+').val() );" value="Create" /></td></tr>';
					html += '</tbody><table></div>';
						
				    this._roll.autohide(false);
				    var opt = {
				        width       : 300,
				        height      : '',
				        position    : 'middle',
				        title       : 'Options for Copy Work Order',
				        body        : html
				    };
				    ret = this._roll.showNotAjax(null, null, opt);
					$(detailsWidget._roll.container()).width($(detailsWidget._roll.container()).find('.popup-body table').width());
					return ret;
	            }
	    	},
	    	error:function(data, status, xhr){
	    		//console.log(data);
	    	}
	    });
}

FSWidgetWODetails.prototype.createWOTabSwitch = function(tab) {
	this._sectionTab = tab;
	this.publish = $('#edit_ShowTechs').attr("checked") ? 1 : 0;
	$('#edit_ShowTechs').removeAttr("checked");
	this.createWO();
}

FSWidgetWODetails.prototype.setPublish = function(publish) {
	try {
		if (publish == undefined)
			return;
		this.publish = publish;
	} catch (e) {}
}

FSWidgetWODetails.prototype.showMessages = function(data) {
    var html = '';
    if (data.success) {
        html += '<div class="success"><ul><li>Success!</li></ul></div>';

        if (window.opener) {try{window.opener.reloadTabFrame();} catch (e) {} }
//        window.location = '/clients/wosDetails.php?v='+window._company+'&tab='+this._reftab+'&id='+this.params.win;
		win = data.win;
		if (this.currentTab == 'create')
			window.location.replace('/clients/wosDetails.php?v='+window._company+'&id='+this.params.win+'&sectionTab='+this._sectionTab+'&publish=' + this.publish);
		else
			window.location.replace(location.href);
//			window.location.reload();
//	this.goBack();
    }else{
		if (this.currentTab == 'create') {
			this._sectionTab = undefined;
			try {
				if (this.publish == undefined) {}
				else if (this.publish == 1)
					$('#edit_ShowTechs').attr("checked", true)
				else
					$('#edit_ShowTechs').removeAttr("checked");
				this.publish = undefined;
			} catch (e) {}
		}
		
        html+='<div class="errors"><ul>';
        for (i=0;i<data.errors.length;i++) {
            if(typeof data.errors[i] === 'object')
                html+='<li>'+data.errors[i].message+'</li>';
            else
                html+='<li>'+data.errors[i]+'</li>';
        }
        html+='</ul></div>';
    }
    html+='<div class="tCenter"><input type="button" class="link_button_popup" onclick="detailsWidget.roll().hide();" value="Ok" /></div>';
    this._roll.autohide(false);
    var opt = {
        width       : 400,
        height      : '',
        position    : 'middle',
        title       : '',
        body        : html
    };
    if (data.success) {
        opt.width = 300;
    }
    return this._roll.showNotAjax(null, null, opt);
}

FSWidgetWODetails.prototype.asyncUploadFile = function(id){
    descr = '';
    if($('#PicDescr'+id).val() != undefined) descr = encodeURIComponent($('#PicDescr'+id).val());
    $.ajaxFileUpload({
        url             : "/widgets/dashboard/details/ajax-upload-file/win/"+this.params.win+"/file/" + id + "/company/" + window._company+"/descr/"+descr+"",
        secureuri       : false,
        fileElementId   : 'Pic'+id,
        dataType        : 'json',
        context         : this,
        cache           : false,
        success         : function (data, status, xhr) {
 	
            this.context._uploadsCounter--;
            if ( !data.success ) {
                this.context._updatestatus.success = 0;
                if ( !this.context._updatestatus.errors ) {
                    this.context._updatestatus.errors = [];
                }
                for (i=0;i<data.errors.length;i++) {
                    this.context._updatestatus.errors.push(data.errors[i]);
                }
            }
            if ( this.context._uploadsCounter <= 0) {
                this.context.showMessages(this.context._updatestatus);
            }
        },
        error:function(data, status, xhr){
            this.context._uploadsCounter--;
        }
    });
}

/*
FSWidgetWODetails.prototype.uploadFile = function(id){
    $.ajaxFileUpload({
        url             : "/widgets/dashboard/details/ajax-upload-file/win/"+this.params.win+"/file/" + id + "/company/" + window._company,
        secureuri       : false,
        fileElementId   : "Pic" + id,
        dataType        : 'json',
        context         : this,
        cache           : false,
        success         : function (data, status, xhr) {
            this.context._activeupload++;
            for (this.context._activeupload;this.context._activeupload<4;this.context._activeupload++) {
                if ($('#Pic'+this.context._activeupload).val()) {
                  this.context.uploadFile(this.context._activeupload);
                  break;
                }
            }

            if (!data.success) {
                this.context._updatestatus.success = 0;
                if (!this.context._updatestatus.errors) {
                    this.context._updatestatus.errors = [];
                }
                for (i=0;i<data.errors.length;i++) {
                    this.context._updatestatus.errors.push(data.errors[i]);
                }
            }

            if (this.context._activeupload > 3) {
                this.context.showMessages(this.context._updatestatus);
            }
        }
    });
}*/

FSWidgetWODetails.prototype.initCurrency = function(){
    function formatCurrency_WO()
    {
        $(this).formatCurrency({groupDigits:false, symbol:'', negativeFormat: '-%n'});
    }
    $.map(['edit_PayMax', 'edit_Tech_Bid_Amount', 'edit_baseTechPay', 'edit_OutofScope_Amount', 'edit_TripCharge', 'edit_MileageReimbursement', 'edit_MaterialsReimbursement', 'edit_AbortFeeAmount', 'edit_Additional_Pay_Amount', 'edit_PayAmount'], function( id ) {
        $('#'+id).unbind('blur',formatCurrency_WO).bind('blur', formatCurrency_WO);
        $('#'+id).formatCurrency({groupDigits:false, symbol:'', negativeFormat: '-%n'});
    });
}

FSWidgetWODetails.prototype.updateSourceBy = function() {
    /* Begin Sourceby date code - CM  */
    var startDate = new Date($("#edit_StartDate").val());
    if (!startDate)
        return;

    today = new Date();
    var weekDays = 0;
    while (today <=startDate) {
        if(today.getDay() >= 1 && today.getDay()<=5) weekDays++;
        today.setDate(today.getDate() +1);
    }

    if(weekDays <=5) {
        $("#edit_ShortNotice").attr("checked","checked");
    }else{
        $("#edit_ShortNotice").removeAttr("checked");
    }

    if(weekDays < 16) {
        sourceByDate = new Date();
    }else{
        sourceByDate = new Date(startDate);
        /* Can't count start day so subtract, initally start with Project Date */
        sourceByDate.setDate(sourceByDate.getDate() -1);
    }

    if($("#edit_Type_ID option:selected").val() == 1) {
        var todayMonth = sourceByDate.getMonth();
        todayMonth++;
        var formatSourceByDate = todayMonth+"/"+sourceByDate.getDate()+"/"+sourceByDate.getFullYear();
        $("#edit_SourceByDate").html(formatSourceByDate);
    }
}

FSWidgetWODetails.prototype.updateFinalPay = function( name ) {
	var obj = this;

	var cancelAction = function () {
        return false;
    };

    var doFinalPayUpdate = function () {
		if(name === "section6") {
	        var user_name = $("#workOrderOwner :selected").text().split (" - ")[0];
			$("#edit_Add_Pay_AuthBy").val(user_name);
		}
		else
			$("#edit_Add_Pay_AuthBy").val(name);
        obj.updateFinalPay();
    };
	
	var recalcBasePay = function() {
		$("#edit_baseTechPay").val("");
		doFinalPayUpdate();
	};
	
    if ( name != 'undefined' ) {
        $('#edit_AbortFee').unbind('change').change(function() {
            $("span.required_abortFee").each(function() {
                this.style.display = $('#edit_AbortFee').attr("checked") ? 'inline' : 'none';
            });
            doFinalPayUpdate();
        });

		$.map(['edit_calculatedTechHrs','edit_Amount_Per','edit_Qty_Devices','edit_Tech_Bid_Amount','edit_Qty_Devices_Display', 'edit_Qty_Visits_Display'], function( id ) {
			$('#'+id).unbind('change').change(recalcBasePay);
            if(id == "edit_Qty_Devices" && typeof(_wosDetails) != 'undefined' && _wosDetails != null) _wosDetails.buildEaventWidgetWODetails();
		});

		$.map(['edit_TechMarkedComplete','edit_CallClosed', 'edit_baseTechPay', 'edit_OutofScope_Amount','edit_TripCharge','edit_MileageReimbursement','edit_MaterialsReimbursement','edit_AbortFeeAmount','edit_Additional_Pay_Amount'], function( id ) {
            $('#'+id).unbind('change').change(doFinalPayUpdate);
        });
		
		if(name === "section6")
			recalcBasePay();
    }
    //TODO: isInvoiced
    var mult = $("#edit_Amount_Per option:selected").val();
    if ( mult == "Device")
    {
        mult = parseFloat($("#edit_Qty_Devices").val());
    }
    else if (mult == "Visit") {
        mult = parseFloat($("#edit_Qty_Visits_Display").val());
    }
    else
    {
        if ( mult == "Hour" )
        {
            mult = parseFloat(($("#edit_calculatedTechHrs").val()=="")?0:$("#edit_calculatedTechHrs").val());
        }
        else {mult = 1;}
    }
    //13985
    $("#agreedPay7").html($("#edit_Tech_Bid_Amount").val());
    $("#agreedPay7").toNumber().formatCurrency({groupDigits:false, symbol:'', negativeFormat: '-%n'});
    $.map(['edit_OutofScope_Amount', 'edit_TripCharge', 'edit_MileageReimbursement', 'edit_MaterialsReimbursement', 'edit_AbortFeeAmount', 'edit_Additional_Pay_Amount'], function( id ) {                                  
        if ($('#'+id).val() != null && $('#'+id).val() !="") {
        var user_name = $("#workOrderOwner :selected").text().split (" - ")[0];		
		$("#edit_Add_Pay_AuthBy").val(user_name);              
	}                              
     });
    //End 13985
    if ($("#edit_baseTechPay").val() == "") {
        $("#edit_baseTechPay").val(mult*$("#edit_Tech_Bid_Amount").val());
        $("#edit_baseTechPay").formatCurrency({groupDigits:false, symbol:'', negativeFormat: '-%n'});
    }
//376
//        if (($("#edit_MissingComments").val() == "" && !$("#edit_TechMarkedComplete").attr("checked")) /*|| isInvoiced*/)
//	        return;
//end 376
    //Update baseTechPay
    var sum = 0;

    if ($("#edit_AbortFee").attr("checked"))
        sum = $("#edit_AbortFeeAmount").val() ? parseFloat($("#edit_AbortFeeAmount").val()):0;
    else
    {
        sum = $("#edit_baseTechPay").val() ? parseFloat($("#edit_baseTechPay").val()):0;

        $.map(['edit_OutofScope_Amount','edit_TripCharge','edit_MileageReimbursement','edit_MaterialsReimbursement','edit_Additional_Pay_Amount'],function(id) {
        sum+=$('#'+id).val()?parseFloat($('#'+id).val()):0;
        });
    }

    //376
    sum += $("#ReimbursableExpenseThroughFSApproved").html() ? parseFloat($("#ReimbursableExpenseThroughFSApproved").html()):0;
    //end 376
    /* Maybe API can handle?
    var sumDeduct = $("#edit_PcntDeduct").attr("checked") ? sum * 0.9 : sum;
    
    $('#edit_Net_Pay_Amount').val(sum);
    $('#edit_Net_Pay_Amount').formatCurrency({groupDigits:false, symbol:'', negativeFormat: '-%n'});*/
//        if($('#edit_PayAmount').val()=="")
//    {
    $('#edit_PayAmount').val(sum);
    $('#edit_PayAmount').formatCurrency({groupDigits:false, symbol:'', negativeFormat: '-%n'});
//    }
};

FSWidgetWODetails.prototype.makePayFieldsReadOnly = function() {
	// make pricing fields ready only if invoiced

	$("#edit_Tech_Bid_Amount").attr("readonly", true);
	$("#edit_Amount_Per").attr("disabled", true);
	$("#edit_Qty_Devices").attr("readonly", true);

	$("#edit_Work_Out_of_Scope").click(function(){return false;});
	$("#edit_OutofScope_Amount").attr("readonly", true);
	$("#edit_OutOfScope_Reason").attr("readonly", true);

	$("#edit_TripCharge").attr("readonly", true);
	$("#edit_MileageReimbursement").attr("readonly", true);
	$("#edit_MaterialsReimbursement").attr("readonly", true);
	$("#edit_Additional_Pay_Amount").attr("readonly", true);

	$("#edit_AbortFee").click(function(){return false;});
	$("#edit_AbortFeeAmount").attr("readonly", true);
	$("#edit_Add_Pay_AuthBy").attr("readonly", true);
	$("#edit_Add_Pay_Reason").attr("readonly", true);

	$("#edit_calculatedTechHrs").attr("readonly", true);
	$("#edit_baseTechPay").attr("readonly", true);
}

FSWidgetWODetails.prototype.initPhone = function(){
	var formatPhone = function() {
		if (this.value == null || this.value == "") return;
		country = $("#edit_Country").val();
		if (country != 'US' && country != 'CA') return;
		validFormat = /^[(]?(\d{3})[)]?(?:\s|-|.)*(\d{3})[-\s.]*(\d{4})$/;
		parts = this.value.match(validFormat);
		if (parts == null) return;
		this.value = "(" + parts[1] + ") " + parts[2] + "-" + parts[3];
        };

	$.map(['edit_SitePhone', 'edit_SiteFax'], function( id ) {
		$('#'+id).each(formatPhone).blur(formatPhone);
	});

	$("#prjdocs table").removeClass("popup_table");
}

FSWidgetWODetails.prototype.confirmRemoveFile = function( obj, file_name )
{
    if ( $(obj).attr('checked') ) {
        if ( confirm('Are you sure you wish to remove the file "' + file_name + '"?') ) {
            $(obj).attr('checked', true);
        } else {
            $(obj).attr('checked', false);
        }
    }
}

/**
 *  getParams
 *
 *  Function collect all params for make request
 *
 *  return object
 */
FSWidgetWODetails.prototype.getParams = function () {
    return {
        'tab'               : this.currentTab,
        'company'           : window._company,
        'win'               : this.params.win
        };
    };


FSWidgetWODetails.prototype.onChangeCountry = function () {
    //041
    var Requirecountry = {"US":"US","CA":"CA","MX":"MX"};

    if(Requirecountry.hasOwnProperty($("#edit_Country option:selected").val()))
    {
        $("#edit_State").removeAttr("disabled");
        if($("#edit_isProjectAutoAssign").is(":checked"))
        {
            $("#lblState").html("<label class='required'>State/Province<span>*</span></label>");
        }
        else
        {
            $("#lblState").html("<label>State/Province</label>");
        }
    }
    else
    {
        $("#edit_State option").each(function()
        {
            if ($(this).val() == '')
            {
                $(this).attr("selected", "selected");
            }
        });

        $("#edit_State").attr("disabled","disabled");
        if($("#edit_isProjectAutoAssign").is(":checked"))
        {
            $("#lblState").html("<label>State/Province</label>");
        }
         return false;
    }
    //end 041
    
    $.ajax({
        url         : '/widgets/dashboard/js/state-info/',
        dataType    : 'json',
        data        : {country: $("#edit_Country").children("option:selected").val()},
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            if (data) {
                //AUTOFILL
				var option = '';
				option += '<option selected="selected" value="">Select State</option>';
                for(index in data) {
					option += '<option value="' + index + '">' + data[index] + '</option>';
				}
				$("#edit_State").html(option);
				if (this._queueSiteState) $('#edit_State').val(this._queueSiteState);
				this._queueSiteState = null;
			}
        }
    });	
}

FSWidgetWODetails.prototype.onChangeDropDownCompany = function (company) {
    $.ajax({
        url         : '/widgets/dashboard/do/global-pm-project/',
        dataType    : 'json',
        data        : {company: company},
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            if (data) {
                //AUTOFILL
				var option = '';
				for(index in data.projects){
						option += '<option value="' + data.projects[index]  + '"';
						option += ' >' + index  + '</option>';
				}
				$("#copyToProject").html(option);
			}
        }
    });	
}

FSWidgetWODetails.prototype.saveSite = function (onsuccess) {
    this._roll.autohide(true);
    var html = '<div>Site Number required to save</div><div class="tCenter"><input type="button" class="link_button_popup" onclick="detailsWidget.roll().hide();" value="Ok" /></div>';
    var opt = {
        width       : 300,
        height      : '',
        position    : 'middle',
        title       : '',
        body        : html
    };
    if ($("#edit_SiteNumber").val() == '') {
	this._roll.showNotAjax(null, null, opt);
	return;
    }
    this.showStartPopup();
    $.ajax({
        url         : '/widgets/dashboard/sitelist/save-site',
        dataType    : 'json',
        data        : {
		SiteNumber: $("#edit_SiteNumber").val(),
		SiteName: $("#edit_SiteName").val(),
		Site_Contact_Name: $("#edit_Site_Contact_Name").val(),
		Address: $("#edit_Address").val(),
		City: $("#edit_City").val(),
		State: $("#edit_State").val(),
		Zipcode: $("#edit_Zipcode").val(),
		Country: $("#edit_Country").val(),
		SitePhone: $("#edit_SitePhone").val(),
		SiteFax: $("#edit_SiteFax").val(),
		SiteEmail: $("#edit_SiteEmail").val(),
		Project_ID: ($("#isUserTypeManager").length <= 0)?$("#edit_Project_ID option:selected").val():3149
	},
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
		if (data) {
                    if(typeof(this.siteLookup) != 'undefined'){
                        $("#btn_selectsite").click(this.siteLookup);
                        $("#img_selectsite").attr('src', '/widgets/css/images/btn_orange_selectsite.png');
                        $("#selectsite_msg").hide ();
                    }
                    jQuery("#btn_selectsite").css("cursor","pointer");
                    if(typeof(onsuccess) == 'function'){
                        onsuccess();
                        this._roll.hide();
                        return false;
                    }
		    var html = '<div>Site saved to project site list</div><div class="tCenter"><input type="button" class="link_button_popup" onclick="detailsWidget.roll().hide();" value="Ok" /></div>';
		    var opt = {
		        width       : 300,
		        height      : '',
		        position    : 'middle',
		        title       : '',
		        body        : html
		    };
			this._roll.showNotAjax(null, null, opt);
		}
        }
    });	
}

FSWidgetWODetails.prototype.autoFillSite = function () {
    $.ajax({
        url         : '/widgets/dashboard/sitelist/get-site',
        dataType    : 'json',
        data        : {
		SiteNumber: $("#edit_SiteNumber").val(),
                Project_ID: ($("#isUserTypeManager").length <= 0)?$("#edit_Project_ID option:selected").val():3149,
		company: window._company
	},
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
		if (data) {
			for (index in data) {
                                var value = data[index]?data[index]:'';
                                if(index == 'TechsInSite'){
                                    if(value){
                                        jQuery(".viewTechBtnHidden").show();
                                        jQuery(".viewTechBtnHiddenView").hide();
                                        if($("#isUserTypeManager").length <= 0)
	                                        _wosDetails.viewTechBtnHidden_onclick();
                                    }else{
                                        jQuery(".viewTechBtnHidden").hide();
                                        jQuery(".viewTechBtnHiddenView").show();
                                    }
                                    continue;
                                }
				if (index == 'State' || index == 'Project_ID' || index == 'SiteNumber' || index == 'Country') continue;
				var element = $('#edit_'+index);
				element.val(value);
				element.change();
			}

			if ($('#edit_Country').val() != data['Country'] && typeof (data['Country']) != "undefined") {
				$('#edit_Country').val(data['Country']);
				this.onChangeCountry();
			}

			this._queueSiteState = data['State'];
			if (data["State"] != "")
				$('#edit_State').val(data['State']);
			}
        }
    });	
}

FSWidgetWODetails.prototype.initSiteNumberAutoComplete = function () {
	$( "#edit_SiteNumber" ).autocomplete({
		source: function (request, callback) {
			$.ajax({
					url: '/widgets/dashboard/sitelist/site-search',
					data: {term: request['term'], project_id: $("#edit_Project_ID  option:selected").val(), company: window._company},
					type: 'POST',
					dataType: 'json',
					error: function() {
					callback({});
				},
				success: function(data) {
					callback(data);
				}
			});
		},
		select:
			function(event, ui) {
				$("#edit_SiteNumber").val(ui.item.value);
				$("#edit_SiteNumber").blur();
				detailsWidget.autoFillSite();
			},
			change: function (event, ui) { detailsWidget.autoFillSite(); }
		});
}

FSWidgetWODetails.prototype.siteLookup = function() {
    if(typeof(this.typeSite) == 'undefined') this.typeSite = 0;
    openPopupWin('siteView.php?v=' + window._company + '&project_id=' + $("#edit_Project_ID option:selected").val() + '&select_mode=' + 1+"&type="+this.typeSite);
}

FSWidgetWODetails.prototype.selectSiteButtonEnable = function(flag,notassign) {
        if(typeof(notassign) == 'undefined') notassign = false;
        this.notassign = notassign;
	$("#btn_selectsite").unbind('click');
	if (!flag) {
		$("#btn_selectsite").click(function () {});
		$("#img_selectsite").attr('src', '/widgets/css/images/btn_orange_selectsite_disabled.png');
		$("#selectsite_msg").show ();
                $("#btn_selectsite").css("cursor","default");
	}
	else {
		$("#btn_selectsite").click(detailsWidget.siteLookup);
		$("#img_selectsite").attr('src', '/widgets/css/images/btn_orange_selectsite.png');
		$("#selectsite_msg").hide ();
                jQuery("#btn_selectsite").css("cursor","pointer");
	}
}

FSWidgetWODetails.prototype.showPanel = function(panelId){
	$(panelId + 'tab').parent().siblings().each(
		function () {
			$(this).children().first().removeClass('current'); 
		}
	);
	$(panelId + 'tab').addClass('current');
	$(panelId + 'tab').parent().addClass('currenttab');
	$('.contentbox').hide();
	$(panelId).show();
        if(panelId == "#docsparts"){
            jQuery("#tdTechDocuments").hide();
            jQuery("#tdTechDocumentsListFile").hide();
        }else{
            jQuery("#tdTechDocuments").show();
            jQuery("#tdTechDocumentsListFile").show();
}
}

FSWidgetWODetails.prototype.showPanelSection = function(panelId){
	var sectionId;

	detailsWidget.showPanel(panelId);
	if ($(panelId).find('.sectionOpen')){
		$(panelId + ' .sectionOpen').find('a.title').trigger('click');
	}
	
	for (var i = 1; i < arguments.length; i++) {
		sectionId = arguments[i];
		$(sectionId + 'title').trigger('click');
	}
//	$(panelId).children('div').show();
}

FSWidgetWODetails.prototype.attachSection = function(source, destination) {
	source.children().detach().appendTo(destination);
}

FSWidgetWODetails.prototype.techTabInit = function() {
	detailsWidget.attachSection(this._section4, $('#techasgn'));
	this._section4 = $('#techasgn');
}

FSWidgetWODetails.prototype.commCenterTabInit = function() {
	detailsWidget.attachSection(this._section3, $('#contacts'));
	this._section3 = $('#contacts');
}

FSWidgetWODetails.prototype.docsPartsTabInit = function() {
	detailsWidget.attachSection(this._wodocsupload, $('#wodocslist'));
	this._wodocsupload = $('#wodocslist');
	detailsWidget.attachSection(this._techdocsupload, $('#techdocslist'));
	this._techdocsupload = $('#techdocslist');
}

FSWidgetWODetails.prototype.workOrderTabInit = function() {
	detailsWidget.attachSection(this._section3, $('#section3real'));
	detailsWidget.attachSection(this._section4, $('#section4'));
	detailsWidget.attachSection(this._section5, $('#section5'));
	detailsWidget.attachSection(this._section6, $('#section6'));
	detailsWidget.attachSection(this._section7, $('#section7'));
	detailsWidget.attachSection(this._section8, $('#section8'));
	detailsWidget.attachSection(this._wodocsupload, $('#uploadwodocs'));
	detailsWidget.attachSection(this._techdocsupload, $('#uploadtechdocs'));
	this._section3 = $('#section3real');
	this._section4 = $('#section4');
	this._section5 = $('#section5');
	this._section6 = $('#section6');
	this._section7 = $('#section7');
	this._section8 = $('#section8');
	this._wodocsupload = $('#uploadwodocs');
	this._techdocsupload = $('#uploadtechdocs');
}

FSWidgetWODetails.prototype.closeOutTabInit = function() {
	detailsWidget.attachSection(this._section5, $('#closeOut5'));
	detailsWidget.attachSection(this._section6, $('#closeOut6'));
	detailsWidget.attachSection(this._section7, $('#closeOut7'));
	detailsWidget.attachSection(this._section8, $('#closeOut8'));
	detailsWidget.attachSection(this._wodocsupload, $('#uploadwodocs'));
	detailsWidget.attachSection(this._techdocsupload, $('#uploadtechdocs'));
	this._section5 = $('#closeOut5');
	this._section6 = $('#closeOut6');
	this._section7 = $('#closeOut7');
	this._section8 = $('#closeOut8');
	this._wodocsupload = $('#uploadwodocs');
	this._techdocsupload = $('#uploadtechdocs');
}

FSWidgetWODetails.prototype.initWOTabs = function() {
	this._section3 = $('#section3real');
	this._section4 = $('#section4');
	this._section5 = $('#section5');
	this._section6 = $('#section6');
	this._section7 = $('#section7');
	this._section8 = $('#section8');
	this._wodocsupload = $('#uploadwodocs');
	this._techdocsupload = $('#uploadtechdocs');
	this.showPanel("#wod");
	$("iframe.mytechs").appear(function() {
        $.post("/widgets/dashboard/client/accessctl/", {
            rt:'iframe',
            attributeId:48
        }, 
        function (response) {
            if(response=="1") {
		$("iframe.mytechs").attr({
			width: "100%", 
			src:"/clients/findMyTechs.php?v=" + window._company + "&win=" + detailsWidget.getWIN() + "&distance=50", 
			frameborder: "0", 
			allowTransparency: "true"
		});
		$($("iframe.mytechs").get(0).contentDocument).ready(FSWidgetWODetails.prototype.hideSpinner);
		$("iframe.mytechs").load(function() 
		{
			$("iframe.mytechs").prev().css("display", "none");
			var height = ($("iframe.mytechs").contents().find("div.inner10").height()+ 60)+"px";
			$("iframe.mytechs").attr("height", height);
			$("iframe.mytechs").contents().find("div.footer_menu").css("display", "none");
			$("iframe.mytechs").contents().find("div#content").css("padding", "0");
			$("iframe.mytechs").contents().find("th").css("padding", "2px");
			$("iframe.mytechs").contents().find("td").css("padding", "2px");
			
		});
            } 
            else {
                jQuery("#mytechs").html(response);
            }                        
	});
		
    });
	$("iframe.techglance").appear(function() {
		$("iframe.techglance").attr({
			width: "100%",
			src:"/clients/wosViewTechProfileSimple.php?simple=1&cbResetParam=1&v=" + window._company + "&TechID=" + $('#edit_Tech_ID').val(), 
			frameborder: "0", 
			allowTransparency: "true"
		});
		$($("iframe.techglance").get(0).contentDocument).ready(FSWidgetWODetails.prototype.hideSpinner);
		$("iframe.techglance").load(this.resizeTechGlance);
	});
	$("iframe.contactlog").appear(function() {	
		$('iframe.contactlog').attr({
			width: "100%", 
			src: "/clients/wosViewContactLog.php?v=" + window._company + "&WO_ID=" + detailsWidget.getWIN(),
			frameborder: "0", 
			allowTransparency: "true"
		});
		$("iframe.contactlog").load(function() 
			{
//				this.resizeContactLog();
			});
	});
	//Show
	$("iframe.applicants").appear(function() {
		$("iframe.applicants").attr({
			width: "100%",  
			src:"/clients/wosApplicant.php?v=" + window._company + "&sb=0&ClientID=" + detailsWidget.getFormField('ClientID') + "&WorkOrderID=" + detailsWidget.getWIN(), 
			frameborder: "0", 
			allowTransparency: "true"
		});
		$("iframe.applicants").load(function() 
			{
				$("iframe.applicants").prev().css("display", "none");
				var height4 = ($("iframe.applicants").contents().find("body").height() + 45) + "px";
				$("iframe.applicants").attr("height", height4);
				
			});
	});	
	$("iframe.findtechs").appear(function() {
		$('iframe.findtechs').attr({
			width: "100%", 
			height: "1420px",
			src: "/clients/techs.php?isDash=1&v=" + window._company + "&newSearch=1&defaultZip=" + detailsWidget.getFormField('Zipcode'),
			frameborder: "0", 
			allowTransparency: "true", 
			seamless: "seamless"
		});
		$("iframe.findtechs").load(function() 
			{
				$("iframe.findtechs").prev().css("display", "none");
				var height5 = ($("iframe.findtechs").contents().find("body").height() + 75) + "px";
				$("iframe.findtechs").attr("height", height5);
				$("iframe.findtechs").contents().find("div.formBox").css("width", "800px");
				$("iframe.findtechs").contents().find("div.formBoxFooter").css("width", "800px");
				});
	});
	$("iframe.partsmgr").appear(function() {
		$('iframe.partsmgr').attr({
			width: "100%", 
			height: "620px",
			src: "/clients/parts/list.php?v=" + window._company + "&woId=" + detailsWidget.getWIN(), 
			frameborder: "0", 
			allowTransparency: "true", 
			seamless: "seamless"
		});
		$("iframe.partsmgr").load(function() 
			{
				$("iframe.partsmgr").prev().css("display", "none");
				var height3 = ($("iframe.partsmgr").contents().find("body").height() + 50) + "px";
				$("iframe.partsmgr").attr("height", height3);
				$("iframe.partsmgr").contents().find("col").removeAttr("width");
				$("iframe.partsmgr").contents().find("col").css("width", "100px");
				$("iframe.partsmgr").contents().find("table.gradBox_table_inner").removeAttr("width");
				$("iframe.partsmgr").contents().find("table.gradBox_table_inner").css("width", "800px");
				$("iframe.partsmgr").contents().find("div:first").removeAttr("align");
				$("iframe.partsmgr").contents().find("input:text").css("width", "120px");
				$("iframe.partsmgr").contents().find("input[name=ShippingAddress]").css("width", "198px");
				$("iframe.partsmgr").contents().find("input[name=Description]").css("width", "326px");
				$("iframe.partsmgr").contents().find("input[name=returnDescription]").css("width", "320px");
			$("iframe.partsmgr").contents().find("input[name=returnRMA]").css("width", "320px");
				$("iframe.partsmgr").contents().find("input[name=returnInstructions]").css("width", "320px");
				$("iframe.partsmgr").contents().find("th").css("color", "#ffffff");
				$("iframe.partsmgr").contents().find("td.tryellow").css("background-color", "#ffffff");
			});
			
	});
}
FSWidgetWODetails.prototype.resizeContactLog = function(){
    $("iframe.contactlog").prev().css("display", "none");
    $("iframe.contactlog").contents().find("col").removeAttr("width");
    $("iframe.contactlog").contents().find("col").css("width", "100px");
    $("iframe.contactlog").contents().find("table.gradBox_table_inner").removeAttr("width");
    $("iframe.contactlog").contents().find("table.gradBox_table_inner").css("width", "780px");
    $("iframe.contactlog").contents().find("div:first").removeAttr("align");
    $("iframe.contactlog").contents().find("br:first").remove();
    var height3 = ($("iframe.contactlog").contents().find("body").height()) + "px";
    $("iframe.contactlog").attr("height", height3);
}
FSWidgetWODetails.prototype.cancelDetails = function() {
	this.goBack();
}

FSWidgetWODetails.prototype.goBack = function() {
	if (document.referrer) {
		if (document.referrer.indexOf('wos.php')) {
			location.replace("/clients/wos.php?v=" + window._company);
		}
		else
			document.location = document.referrer;
	}
	else {
		location.replace("/clients/wos.php?v=" + window._company);
//		window.history.back();
//		setTimeout("history.back()", 700);
	}
}

FSWidgetWODetails.prototype.getFormField = function(field) {
	return $("#updateDetailsForm input[name='" + field + "']").val();
}

FSWidgetWODetails.prototype.getWIN = function() {
	return $("#updateDetailsForm input[name='WIN_NUM']").val();
}

FSWidgetWODetails.prototype.showSpinner = function(tab) {
	tab.siblings().append("<div class='woSpinner'></div>");
}

FSWidgetWODetails.prototype.hideSpinner = function() {
	$(".woSpinner").remove();
	
}

FSWidgetWODetails.prototype.resizeTechGlance = function() {
	$("iframe.techglance").prev().css("display", "none");
	var h = $("iframe.techglance").contents().find("body").height();
	var height2 = (h + 125)+"px";
	if (h <= 500) {
		setTimeout(this.resizeTechGlance, 500);
		return;
	}
	$("iframe.techglance").contents().find("body").css("width", "800px");
	$("iframe.techglance").attr("height", height2);
	//$("iframe.techglance").contents().find("form#caspioform + div").css("display", "none");
}

FSWidgetWODetails.prototype.resizeFrameSection = function(name) {
	try {
		me = $("iframe." + name, document);
		h = $("body", me.get(0).contentDocument).height();
		me.attr('width', '100%').attr('height', h+40);
	} catch (e) {}
}
/* 13778
FSWidgetWODetails.prototype.onBluer = function(event) {
    var val = jQuery.trim(jQuery(event).val());
    var parts = val.length; 
    if(parts > 9)
    {
        $("<div></div>").fancybox(
                {
                    'autoDimensions' : false,
                    'width': 300,
                    'height' : 70 ,
                    'transitionIn': 'none',
                    'transitionOut' : 'none',
                    'content' : '<div style="color:red;padding-top:10px;">The Route field is limited to 9 characters.</div>'
                }
            ).trigger('click');
         return false;       
    }    
    return true;
}
*/
FSWidgetWODetails.prototype.sendEmailBlast = function(url,el) {
    var EmailBlastRadiusValue = jQuery("#edit_EmailBlastRadius").html();
    var EmailBlastRadiusPreferredOnly = jQuery("#edit_EmailBlastRadiusPreferredOnly").val();
    if(EmailBlastRadiusValue == "") EmailBlastRadiusValue = 50;
    var html = "<table width='100%'>"
            html += "<tr>";
            html +=     "<td align='left'>";
            html +=     "Blast Radius (miles) ";
            html +=     "</td>";
            html +=     "<td align='left'>";
            html +=     '<select id="EmailBlastRadius" name="EmailBlastRadius">\
                            <option value="10">10</option>\
                            <option value="25">25</option>\
                            <option selected="selected" value="50">50</option>\
                            <option value="75">75</option>\
                            <option value="100">100</option>\
                            <option value="150">150</option>\
                            <option value="250">250</option>\
                        </select>';
            html +=     "</td>";
            html += "</tr>";
            html +=     "<td align='left'>";
            html +=     "Preferred Techs Only ";
            html +=     "</td>";
            html +=     "<td align='left'>";
            html +=     '<input type="checkbox" value="1" id="EmailBlastRadiusPreferredOnly" >';
            html +=     "</td>";
            html += "</tr>";
            html += "</tr>";
            html +=     "<td align='center' conspan='2'>";
            html +=     '<input type="button" value="Send Email"id="cmdSendEmailBlast" >&nbsp;&nbsp;';
            html +=     '<input type="button" value="Cancel" id="cmdCancelEmailBlast" >';
            html +=     "</td>";
            html += "</tr>";
            html +="</table>";
            
        this._roll.autohide(false);
        var opt = {
            width       : 200,
            height      : '',
            //position    : 'middle',
            title       : 'Email Blast Radius',
            body        : html
        };
        var evnt = this;    
        this._roll.showNotAjax(el, el, opt);
        jQuery("#EmailBlastRadius").val(EmailBlastRadiusValue);
        if(EmailBlastRadiusPreferredOnly == 1)
        {
            jQuery("#EmailBlastRadiusPreferredOnly").attr("checked",true);
        }
        jQuery("#cmdSendEmailBlast").unbind("click");
        jQuery("#cmdSendEmailBlast").click(function() {
            var _url = url + "&EmailBlastRadius="+jQuery("#EmailBlastRadius").val()+"&EmailBlastRadiusPreferredOnly="+(jQuery("#EmailBlastRadiusPreferredOnly").is(":checked")?1:0);
            openPopupWin(_url);
        });
        jQuery("#cmdCancelEmailBlast").click(function() {
           evnt._roll.hide();
        })
        
}

this.WIN_NUM = 0;
FSWidgetWODetails.prototype.QABox = function () {
        WIN_NUM = $("#ContactlogBtn").attr('win_num');
    var rel = '/widgets/dashboard/popup/clientquestionandanswer?WIN_NUM=' + WIN_NUM;
    var content = "<div id='comtantPopupID'><div style='text-align:center;height:380px;width:900px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
    $("<div></div>").fancybox(
    {
        'autoDimensions' : true,
        'showCloseButton' :true,
        'hideOnOverlayClick' : false,
        'enableEscapeButton':false,
        'content': content
    }
    ).trigger('click');

    $.ajax({
        type: "POST",
        url: rel,
        data: "",
        context     : this,
        success:function( html ) {
            jQuery("#comtantPopupID").html(html);
            jQuery("#cmdPostQuestion")
            .unbind("click",this.clientPostQuestion)
            .bind("click",this.clientPostQuestion); 
            
            jQuery(".techprofileforquestion")
                .unbind("click",this.loadprofileQuestion)
                .bind("click",this.loadprofileQuestion);
                
            jQuery(".bidinfoQuestion")
                .unbind("click",this.loadbidinfoQuestion)
                .bind("click",this.loadbidinfoQuestion);
            $("#ContactlogBtn").html('Q&amp;A Log');
        },
        error:function(error) {
            alert('error; ' + eval(error));
        }
    });
}
//------------------------------------------------------------
FSWidgetWODetails.prototype.loadprofileQuestion = function(){
        var techid = jQuery(this).attr("techid");
        var comid = jQuery(this).attr("comid");
        $("<div></div>").fancybox(
        {
            'autoDimensions' : false,
            'showCloseButton' :true,
            'width':1010,
            'height':800,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'type':"iframe",
            'href':'/clients/wosViewTechProfile.php?simple=1&back=0&popup=1&TechID='+techid+'&v='+comid+'&backNotices=1&backclick='+Base64.encode('window.parent.detailsWidget.QABox('+ WIN_NUM+');')
        }
        ).trigger('click');
    }
//------------------------------------------------------------
FSWidgetWODetails.prototype.loadbidinfoQuestion = function(){
        var comid = jQuery(this).attr("comid");
        
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :true,
            'width':1010,
            'height':800,
            'hideOnOverlayClick' : false,
            'enableEscapeButton':false,
            'type':"iframe",
            'href':'/clients/wosApplicant.php?simple=1&v='+comid+'&sb=0&WorkOrderID='+WIN_NUM+'&backNotices=1&backclick='+Base64.encode('window.parent.detailsWidget.QABox('+WIN_NUM+');')
        }
        ).trigger('click');
    }
//------------------------------------------------------------
FSWidgetWODetails.prototype.PostQuestion = function () {
    var data = jQuery("#frmPostQuestion").serialize();
    $.ajax({
        type: "POST",
        url: '/widgets/dashboard/popup/postquestion',
        data: data,
        context : this,
        success:function( html ) {
            jQuery("#comtantPopupID").html("<div style='width:200px;'>"+html+"</div>");
            $('#fancybox-content').width(400);
            $('#fancybox-wrap').width(420);
            $.fancybox.resize();
        },
        error:function(error) {
            alert('error; ' + eval(error));
        }
    });
}
//------------------------------------------------------------
FSWidgetWODetails.prototype.clientPostQuestion = function () {
    var data = jQuery("#frmPostQuestion").serialize();
    if(data != ""){
        data += "&techName="+jQuery("#frmPostQuestion").find("#techlist option:selected").attr('techname');
    }
    if($("#frmPostQuestion").find("#question").val()!="")
    {
    $.ajax({
        type: "POST",
        url: '/widgets/dashboard/popup/clientpostquestion',
        data: data,
        context : this,
        success:function( html ) {
            jQuery("#comtantPopupID").html("<div style='width:200px;'>"+html+"</div>");
            $('#fancybox-content').width(400);
            $('#fancybox-wrap').width(420);
            $.fancybox.resize();
        },
        error:function(error) {
            alert('error; ' + eval(error));
        }
    });
}
}
FSWidgetWODetails.prototype.ContactlogBtnchangename = function () {
    
    jQuery("#ContactlogBtn").html("Q&amp;A Log");
}

FSWidgetWODetails.prototype.updateSignOffDisplay = function () {
	var signoff_enabled = jQuery("#edit_SignOff_Disabled").attr ("checked");
	var custom_enabled = (jQuery("input[name=CustomSignOff_Enabled]:checked").val() == "1");

	if (signoff_enabled && custom_enabled) {
		if ($("#signoff_filelink").text () != "") {
			$("#UploadCustomSignOff #container_remove").show ();
			$("#UploadCustomSignOff #container_upload").hide ();
		}
		else {
			$("#UploadCustomSignOff #container_remove").hide ();
			$("#UploadCustomSignOff #container_upload").show ();
		}
	}
	else {
		$("#UploadCustomSignOff #container_remove").hide ();
		$("#UploadCustomSignOff #container_upload").hide ();
	}

	if (signoff_enabled) {
		jQuery(".signoffoptions_display").show ();
		jQuery("input[name=CustomSignOff_Enabled]").removeAttr ("disabled");
	}
	else {
		jQuery("input[name=CustomSignOff_Enabled]").attr ("disabled","disabled");
		jQuery(".signoffoptions_display").hide ();
	}
};

FSWidgetWODetails.prototype.enableAutoAssign = function (is_enabled) {
	if (is_enabled) {
		if (this._enablePushWM) jQuery ("#edit_PushWM").removeAttr ("disabled");
		jQuery ("#edit_P2TPreferredOnly").removeAttr ("disabled");
		jQuery ("#edit_notP2TPreferredOnly").removeAttr ("disabled");
		jQuery ("#edit_isnotWorkOrdersFirstBidder").removeAttr ("disabled");
		jQuery ("#edit_isWorkOrdersFirstBidder").removeAttr ("disabled");
		jQuery ("#ClientCredential").removeAttr ("disabled");
		jQuery ("#CredentialCertification").removeAttr ("disabled");
		jQuery ("#PublicCertification").removeAttr ("disabled");
		jQuery ("#FSExpert").removeAttr ("disabled");
		jQuery ("#IndustryCertification").removeAttr ("disabled");
		jQuery ("#edit_NotifyOnAutoAssign").removeAttr ("disabled");
		jQuery ("#edit_MinimumSelfRating").removeAttr ("disabled");
		jQuery ("#edit_MaximumAllowableDistance").removeAttr ("disabled");
		jQuery ("#edit_P2TNotificationEmailTo").removeAttr ("disabled");
		jQuery ("#edit_P2TD").removeAttr ("disabled");
		jQuery ("#edit_P2TH").removeAttr ("disabled");
		jQuery ("#edit_P2TM").removeAttr ("disabled");
	}
	else {
		jQuery ("#edit_PushWM").attr ('disabled','disabled');
		jQuery ("#edit_P2TPreferredOnly").attr ('disabled','disabled');
		jQuery ("#edit_notP2TPreferredOnly").attr ('disabled','disabled');
		jQuery ("#edit_isnotWorkOrdersFirstBidder").attr ('disabled','disabled');
		jQuery ("#edit_isWorkOrdersFirstBidder").attr ('disabled','disabled');
		jQuery ("#ClientCredential").attr ('disabled','disabled');
		jQuery ("#CredentialCertification").attr ('disabled','disabled');
		jQuery ("#PublicCertification").attr ('disabled','disabled');
		jQuery ("#FSExpert").attr ('disabled','disabled');
		jQuery ("#IndustryCertification").attr ('disabled','disabled');
		jQuery ("#edit_NotifyOnAutoAssign").attr ('disabled','disabled');
		jQuery ("#edit_MinimumSelfRating").attr ('disabled','disabled');
		jQuery ("#edit_MaximumAllowableDistance").attr ('disabled','disabled');
		jQuery ("#edit_P2TNotificationEmailTo").attr ('disabled','disabled');
		jQuery ("#edit_P2TD").attr ('disabled','disabled');
		jQuery ("#edit_P2TH").attr ('disabled','disabled');
		jQuery ("#edit_P2TM").attr ('disabled','disabled');
	}
};

FSWidgetWODetails.prototype.requireAddress = function (is_required) {
	if (is_required) {
		$("#lblCity").html('<label class="required">City<span>*</span></label>');
		$("#lblCountry").html('<label class="required">Country<span>*</span></label>');
		$("#lblAddress").html('<label class="required">Site Address<span>*</span></label>');
		//041
		var Requirecountry = {"US":"US","CA":"CA","MX":"MX"};

		if(Requirecountry.hasOwnProperty($("#edit_Country option:selected").val()))
		{
		$("#lblState").html('<label class="required">State/Province<span>*</span></label>');
			$("#edit_State").removeAttr("disabled");
		}
		else
		{
			$("#lblState").html('<label>State/Province</label>');
			$("#edit_State").attr("disabled","disabled");
		}
	}
	else {
		$("#lblCity").html('<label>City</label>');
		$("#lblCountry").html('<label>Country</label>');
		$("#lblState").html('<label>State/Province</label>');
		$("#lblAddress").html('<label>Site Address</label>');
	}
};

FSWidgetWODetails.prototype.enableClientPushWM = function (is_enabled) {
	if (is_enabled == "0") {
		this._enablePushWM = false;
	}
	else if (is_enabled == "1") {
		this._enablePushWM = true;
	}
	else {
		this._enablePushWM = !!is_enabled;
	}
};



