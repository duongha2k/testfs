
//-----------------------------------------------------------------------------
/**
 * @class Control class for single select boxes, making them multi-select by
 *	removing selected options, placing them in a display and allowing the user
 *	to cycle through extended options/levels.
 * @author Jason K. Flaherty
 */
var FSMultiselectControl = (function () {
	var FSMultiselectControl = function (settings) {
		this.selected = {};				//list of selected options
		this.levels = new Array ();		//available levels per option
		this.select = null;				//select element (jQuery)
		this.display = null;			//element to display selected options (jQuery)
		this.output = false;			//name of output (string or false)

		if (typeof settings != "undefined") {
			if (typeof settings.selected != "undefined") {
				for (value in settings.selected) {
					this.selectOption (value, settings.selected[value]);
				}
			}

			if (typeof settings.levels != "undefined") {
				for (value in settings.levels) {
					this.addLevel (value, settings.levels[value]);
				}
			}

			if (typeof settings.select != "undefined") {
				this.setSelect (settings.select);
			}

			if (typeof settings.display != "undefined") {
				this.setDisplay (settings.display);
			}
		}

		this.is_changed = false;

		FSMultiselectControl.instances.push (this);
	};

	FSMultiselectControl.form = false;
	FSMultiselectControl.disabled = false;
	FSMultiselectControl.instances = new Array ();

	FSMultiselectControl.setForm = function (form) {
		FSMultiselectControl.form = jQuery (form);
	};

	FSMultiselectControl.submit = function () {
		var form = FSMultiselectControl.form;
		var disabled = FSMultiselectControl.disabled;
		var instances = FSMultiselectControl.instances;
		var instance, name, count, selection, level;
		var input_value, input_level;

		if (form && !disabled) {
			for (var index = 0; index < instances.length; index++) {
				instance = instances[index];

				//if (instance.is_changed) {
					name = instance.output;
					count = 1;

					for (var value in instance.selected) {
						input_value = jQuery ("input[name='" + name + "_id_" + count + "']");
						input_level = jQuery ("input[name='" + name + "_level_" + count + "']");

						selection = instance.selected[value];
						level = selection.level >= 0 ? instance.levels[selection.level].value : -1;

						if (input_value.length == 0) {
							input_value = jQuery ("<input/>").attr ({
								type: "hidden",
								name: name + "_id_" + count,
								value: value
							});

							input_level = jQuery ("<input/>").attr ({
								type: "hidden",
								name: name + "_level_" + count,
								value: level
							});

							form.append (input_value);
							form.append (input_level);
						}
						else {
							input_value.attr ({
								name: name + "_id_" + count,
								value: value
							});
							input_level.attr ({
								name: name + "_level_" + count,
								value: level
							});
						}

						count++;
					}
				//}
			}

			//allow submit only once
			//FSMultiselectControl.disabled = true;
		}
	};

	FSMultiselectControl.deselectAll = function () {
		for (var i = 0; i < FSMultiselectControl.instances.length; i++) {
			FSMultiselectControl.instances[i].deselectAll ();
		}
	};

	return FSMultiselectControl;
}) ();

FSMultiselectControl.prototype.addLevel = function (value, text) {
	this.levels.push ({ "value": value, "text": text });

	return this;
};

FSMultiselectControl.prototype.getLevel = function (value) {
	var index = false;

	for (var i = 0; i < this.levels.length; i++) {
		if (this.levels[i].value == value) index = i;
	}

	return index;
};

FSMultiselectControl.prototype.setSelect = function (element) {
	var inst_ref = this;

	this.select = jQuery (element);

	if (!this.output) this.output = this.select.attr ("name") || false;

	this.select.change (function () {
		inst_ref.selectOption (jQuery (this).attr ("value"), 0);
	});

	return this;
};

FSMultiselectControl.prototype.setDisplay = function (element) {
	this.display = jQuery (element);

	return this;
};

FSMultiselectControl.prototype.setOutput = function (name) {
	this.output = name;

	return this;
};

FSMultiselectControl.prototype.selectOption = function (value, level) {
	var inst_ref = this;
	var selection = this.selected[value] || false;
	var option = this.select.find ("option[value='" + value + "']");
	var display;

	if (option.val () == this.select.find (":first-child").val ()) return this;	//IE fix

	if (option.length > 0) {
		if (!selection) {
			selection = {
				"value": value,
				"text": option.text ()
			};
		}

		if (typeof level != "undefined") selection.level = level;

		//Add new entry in display
		display = this.appendDisplay (selection)
		selection.display = display;
		this.display.append (display.main);

		//Set event handler to remove option
		display.deselect.click (function () {
			inst_ref.deselectOption (value);
		});

		//Set event handler to switch level
		display.level.click (function () {
			inst_ref.selectOption (value, ++selection.level);
		});

		//Remove option from select box
		option.remove();
	}
	else if (selection) {
		if (typeof level != "undefined") {
			if (level >= this.levels.length) level = 0;
			selection.level = level;

			//switch display to show new level
			this.displayLevel (selection.display.level, selection.level);
		}
		else if (typeof selection.level == "undefined") {
			selection.level = 0;
			this.displayLevel (selection.display.level, selection.level);
		}
	}

	this.selected[value] = selection;
	this.is_changed = true;

	this.select.attr ("selectedIndex", 0);	//IE fix

	return this;
};

FSMultiselectControl.prototype.deselectOption = function (value) {
	var selection = this.selected[value] || false;
	var element;

	if (selection) {
		//Add back to select box, preferably in the original order (?)
		element = new Option (); //selection.text, value);
		jQuery (element).text (selection.text);
		jQuery (element).val (value);
		element = this.select.append (element);

		//Remove from display
		selection.display.main.remove ();

		//Remove from list of selected options
		selection.level = -1;
		this.is_changed = true;
	}

	return this;
};

FSMultiselectControl.prototype.deselectAll = function () {
	for (selection in this.selected) {
		this.deselectOption (this.selected[selection].value);
	}
};

FSMultiselectControl.prototype.appendDisplay = function (selection) {
	var content, content_deselect, content_text, content_level;

	content = jQuery ("<div></div>").attr ("class", "multiselect_control");
	content_deselect = jQuery ("<span></span>")
		.attr ("class", "ms_close_box");
		//.css ({"background-color": "#c0c0c0", "width": "20px"});
	content_text = jQuery ("<span></span>")
		.attr ("class", "ms_selection")
		.text (" " + this.select.find ("option[value='" + selection.value + "']").text());
	content_level = jQuery ("<span></span>")
		.attr ("class", "ms_level");
	this.displayLevel (content_level, selection.level);
	content.append (content_deselect);
	content.append (content_text);
	content.append (content_level);

	return {
		"main": content,
		"text": content_text,
		"level": content_level,
		"deselect": content_deselect
	};
};

FSMultiselectControl.prototype.displayLevel = function (element, level) {
	element.html (" (<a class='ms_level_link' href='javascript:;'>" + this.levels[level].text + "</a>)");
};
