function FSTechFindWorkOrder( roll ) {
	this._roll = roll;
	this._html = '';
	this._container = null;
}

FSTechFindWorkOrder.prototype.container = function() {
	if (this._container == null) {
		var container = document.createElement('div');
        container.style.display = 'none';
        $('body')[0].appendChild(container);
        this._container = container;
	}
	return this._container;
}

FSTechFindWorkOrder.prototype.buildHtml = function () {
	var _url = '/widgets/dashboard/tech/findworkorder/';

    jQuery.ajax({
        url : _url,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        success : function(data, status, xhr) {
            $(this.container()).html(data);
        },
        error : function (xhr, status, error) {
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/techs/logIn.php";
            }
        }
    });
};

FSTechFindWorkOrder.prototype.doFind = function() {
    var wo  = $("#fWOrdClientWO").val();
	var win = $("#fWOrdWIN").val();
	if (win.length > 0 || wo.length > 0) {
        $("#fWOimage").removeClass('displayNone');
        $("#fWOrdMessage").text('');
		$.ajax({
                url: '/widgets/dashboard/tech/findworkorder/',
                type: 'POST',
                timeout: 20000,
                data: {
                    win: win,
                    wo: wo
                },
                dataType : 'json',
                context: this,
                success: function(data) {
                	$("#fWOimage").addClass('displayNone');
                    if (data && !data.errors && data.success) {
                        var win_location = '';
                        if (data.data[0].Tech_ID != null) {
                            win_location = 'wosDetails.php?id=' + escape(data.data[0].WIN_NUM) + '&v=' + escape(data.data[0].Tech_ID);
                        }else{//fix for old details page
                            win_location = 'wosDetails.php?id=' + escape(data.data[0].WIN_NUM);
                        }
                        window.location = win_location;
//                        roll.hide();
                    } else if (data && data.errors) {
                        var errortext = '';
                        for (i=0;i<data.errors.length;i++) {
                            if (i>0) {
                                errortext+='<br />';
                            }
                            errortext+=data.errors[i].message;
                        }
                        if(data.errors[0].code==8)
                        {
                            $("#fWOrdMessage").css('color','red');
                        $("#fWOrdMessage").text(errortext);
                        }
                        else
                        {
                            $("#fWOrdMessage").text(errortext);
                        }
                        
                        setTimeout(function () { $("#fWOrdMessage").text(''); }, 5000);
                    } else {
                    	$("#fWOrdMessage").text('Your request has timed out. Please try again.');
                    	setTimeout(function () { $("#fWOrdMessage").text(''); }, 8000);
                    }
                    
                },
                error : function (xhr, status, error) {
                    if ( xhr.responseText === "authentication required" ) {
                        document.location = "/techs/logIn.php";
                    } else {
                    	$("#fWOimage").addClass('displayNone');
                    	$("#fWOrdMessage").text('Your request has timed out. Please try again.');
                    	setTimeout(function () { $("#fWOrdMessage").text(''); }, 8000);
                    }
                }
      });
	}
}

FSTechFindWorkOrder.prototype.prepare = function() {
	$("#fWOrdMessage").html('');
}
