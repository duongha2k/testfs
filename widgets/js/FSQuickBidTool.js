function FSQuickBidTool(wd, roll)
{
    if ( !wd instanceof FSWidget ) {
        throw new Error("The first parameter should be a Dashboard object");
    }
    if ( !roll instanceof FSPopupAbstract ) {
        throw new Error("The second parameter should be a Popup object");
    }
    this._img = new Image();
    this._img.src = '/widgets/images/wait_small.gif';
    this._appliedImg = '/widgets/images/check_green.gif';

    this._wd    = wd;
    this._roll  = roll;
    this._btn;

    this._messages = [];

    /* Default values for Amount and Comments fields in Bid form */
    this._amountValue     = '$ Enter Amount';
    this._commentsValue   = 'Enter Comments';
}

FSQuickBidTool.NAME = "FSQuickBidTool";
FSQuickBidTool.VERSION = "0.1";
FSQuickBidTool.DESCRIPTION = "Tool make Techs bids for";

FSQuickBidTool.prototype.name           = function()    {return this.constructor.NAME;}
FSQuickBidTool.prototype.version        = function()    {return this.constructor.VERSION;}
FSQuickBidTool.prototype.description    = function()    {return this.constructor.DESCRIPTION;}

FSQuickBidTool.prototype.button = function( btn )
{
    this._btn = btn;
}

FSQuickBidTool.prototype.addMessage = function(msg)
{
    this._messages.push(msg);
}
FSQuickBidTool.prototype.clearMessages = function()
{
    this._messages = [];
}
FSQuickBidTool.prototype.hasMessages = function()
{
    return ( this._messages.length > 0 );
}
FSQuickBidTool.prototype.showErrors = function()
{
    var idx, text = "";
    if ( this.hasMessages() ) {
        text += "<ul class='errors'>";
        for ( idx in this._messages ) {
        	if (typeof(this._messages[idx]) == 'function') {
        		continue;
        	}
            text += '<li class="error">' + this._messages[idx] + '</li>';
        }
        text += "</ul>";
        $('#' + $(this._roll.container()).attr('id') + ' #messages').html(text);
    }
}
FSQuickBidTool.prototype.showMessages = function()
{
    var idx, text = "";
    if ( this.hasMessages() ) {
        for ( idx in this._messages ) {
        	if (typeof(this._messages[idx]) == 'function') {
        		continue;
        	}
            text += this._messages[idx];
        }
        $('#' + $(this._roll.container()).attr('id') + ' #messages').html(text);
    }
}
FSQuickBidTool.prototype.hideMessages = function()
{
    $('#' + $(this._roll.container()).attr('id') + ' #messages').html('');
}

FSQuickBidTool.prototype.body = function()
{
    var html = "";
    
    html += " <div> ";
    html += " <div id='messages'></div> ";
    html += " <form method='post' style='padding-top: 7px; padding-bottom: 6px;' action='/' onsubmit='return quickBid.searchWO(this.win)'> ";

    html += " <span><strong>WIN #</strong></span> ";
    html += " <input type='text' name='win' value='Enter WIN#' onfocus='if (this.value==\"Enter WIN#\") this.value=\"\"' onblur='if (this.value == \"\") this.value = \"Enter WIN#\"' /> ";
    html += " <input type='submit' value='Go' class='link_button_popup'> ";
    
    html += " </form> ";
    html += " </div> ";

    return html;
}

FSQuickBidTool.prototype.searchWO = function( winObj, el )
{
    var _win = $.trim(winObj.value);
    if(_win == '') _win = winObj;

    this.clearMessages();

    if ( _win.length === 0 || _win == 'Enter WIN#') {
        this.addMessage('Please enter a WIN#');
    } else if ( -1 === _win.search(/^\d+$/) ) {
        this.addMessage('WIN# not found or is no longer available');
    }

    if ( this.hasMessages() ) { //  Errors
        this.showErrors();
    } else {
        this.addMessage('<center><img src="' + this._img.src + '" alt="" /></center>');
        this.showMessages();
        jQuery.ajax({
            url         : this._wd._makeUrl('bidsearch', '/'),
            cache       : false,
            type        : 'POST',
            dataType    : 'html',
            context     : this,
            data        : {win : _win, tech_id : window._techId},
            success     : function(data, status, xhr) {
                this._roll.autohide(false);
                var opt = {
                    width       : 500,
                    height      : '',
                    title       : '',
                    body        : data,
                    context     : this,
                    wTop : 20,
                    position : 'top',
                    postHandler : this.initBidForm
                };
                if(el instanceof Object)
                    this._roll.showNotAjax(el, null, opt);
                else{
                    if(!this._btn) this._btn = this.eventbutton;
                    this._roll.showNotAjax(this._btn, null, opt);
                    if(typeof(this.afterBid) == "function") this.afterBid();
                }
            },
            error       : function (xhr, status, error) {
                if ( xhr.responseText === "authentication required" ) {
                    document.location = "/techs/logIn.php";
                } else {
                    this.clearMessages();
                    this.hideMessages();
                    this.addMessage(xhr.responseText);
                    this.showErrors();
                }
            }
        });
    }

    return false; // prevent submit the search WO form
}

//415
FSQuickBidTool.prototype.WithdrawWO = function( winObj, el )
{
    var _win = $.trim(winObj.value);
    if(_win == '') _win = winObj;

    this.clearMessages();

    if ( _win.length === 0 || _win == 'Enter WIN#') {
        this.addMessage('Please enter a WIN#');
    } else if ( -1 === _win.search(/^\d+$/) ) {
        this.addMessage('WIN# not found or is no longer available');
    }

    if ( this.hasMessages() ) { //  Errors
        this.showErrors();
    } else {
        this.addMessage('<center><img src="' + this._img.src + '" alt="" /></center>');
        this.showMessages();
        jQuery.ajax({
            url         : this._wd._makeUrl('withdraw', '/'),
            cache       : false,
            type        : 'POST',
            dataType    : 'html',
            context     : this,
            data        : {win : _win, tech_id : window._techId},
            success     : function(data, status, xhr) {
                //this._roll.autohide(false);
                var opt = {
                    width       : 350,
                    height      : '',
                    title       : '<span style="font-size: 13px;">Review Application � </span><a class="ajaxLink" href="wosDetails.php?id='+_win+'"><strong>WIN# '+_win+'</strong></a>',
                    body        : data,
                    context     : this,
                    wTop : 20,
                    postHandler : this.initBidForm
                };
                if(el instanceof Object)
                    this._roll.showNotAjax(el, null, opt);
                else{
                    if(!this._btn) this._btn = this.eventbutton;
                    this._roll.showNotAjax(this._btn, null, opt);
                    if(typeof(this.afterBid) == "function") this.afterBid();
                }
            },
            error       : function (xhr, status, error) {
                if ( xhr.responseText === "authentication required" ) {
                    document.location = "/techs/logIn.php";
                } else {
                    this.clearMessages();
                    this.hideMessages();
                    this.addMessage(xhr.responseText);
                    this.showErrors();
                }
            }
        });
    }

    return false; // prevent submit the search WO form
}
//end 415

FSQuickBidTool.prototype.initBidForm = function()
{
    var amountValue = this._amountValue;
    var commentsValue = this._commentsValue;

    var amountField = $('#' + $(this._roll.container()).attr('id') + ' #amount');
    var commentsField = $('#' + $(this._roll.container()).attr('id') + ' #comments');

    if (amountField.val() == '') {
    	amountField.val(amountValue);
	}
    amountField.focus(function() {if ( this.value == amountValue ) this.value = '';});
    amountField.blur(function () {if ( 
        this.value == '' ) this.value = amountValue
        else{
            var v = parseFloat($.trim(this.value));
            if ( isNaN(v) ) {
                this.value = '0.00';
            } else {
                v = parseInt( Math.round(v*100) ) / 100;
                v = ( -1 === v.toString().search(/\./) ) ? v.toString() + '.00' : v.toString();
                if ( v.substr(v.search(/\./)).length == 1 ) {
                    v += '00';
                } else if ( v.substr(v.search(/\./)).length == 2 ) {
                    v += '0';
                }
                this.value = v;
            }
        }
    });
    amountField.change(function () { 
        if ( this.value !== amountValue && this.value !== '' ) {
            var v = parseFloat($.trim(this.value));
            if ( isNaN(v) ) {
                this.value = '0.00';
            } else {
                v = parseInt( Math.round(v*100) ) / 100;
                v = ( -1 === v.toString().search(/\./) ) ? v.toString() + '.00' : v.toString();
                if ( v.substr(v.search(/\./)).length == 1 ) {
                    v += '00';
                } else if ( v.substr(v.search(/\./)).length == 2 ) {
                    v += '0';
                }
                this.value = v;
            }
        }
    });

    //detailObject.onInit.amountField(amountField);

    
    if (commentsField.val() == '') {
        commentsField.val(commentsValue);
    }
        
    commentsField.focus(function() {if ( this.value == commentsValue ) this.value = '';});
    commentsField.blur(function () {if ( this.value == '' ) this.value = commentsValue});

    var $this = this;
    $('#' + $(this._roll.container()).attr('id') + ' #doBidForm').submit(function ( event ) {return $this.makeABid(this, event);});
    $('#' + $(this._roll.container()).attr('id') + ' #doWithdrawForm').submit(function ( event ) {return $this.makeAWithdraw(this, event);});
}

FSQuickBidTool.prototype.makeABid = function ( form, event )
{
    event.preventDefault();

    var agreeCheck      = form.agree;
    var understandCheck = form.understand;
    var amountField     = form.amount;
    var commentsField   = form.comments;
    var winField        = form.win;
    var withdrawField   = form.withdraw;
    var withdrawFieldvalue;
    var amount;
    var comments;

    this.clearMessages();
    if(typeof(withdrawField)!="undefined")
        withdrawFieldvalue = jQuery(withdrawField).is(":checked")?1:0;
    else
        withdrawFieldvalue = 0;
    
    if(withdrawFieldvalue==0)
    {
    if ( amountField.value === this._amountValue )      amount   = null;
    else                                                amount   = parseFloat($.trim(amountField.value));
    if ( commentsField.value == this._commentsValue )   comments = "";
    else                                                comments = $.trim(commentsField.value);

    if ( !agreeCheck.checked || !understandCheck.checked ) {
        this.addMessage('Please check both boxes below to confirm that you agree to and understand the statements below.');
    }
    if ( amount === null ) {
        this.addMessage('Amount is required and cannot be empty');
    } else if ( isNaN(amount)) {
        this.addMessage('You set incorrect value for Amount');
    } else if ( amount<=0 ) {
        this.addMessage('Amount must be greater than zero.');
    }
    }
    if ( this.hasMessages() )
    {     //  Errors
        this.showErrors();
    }
    else
    {
        if(withdrawFieldvalue==0)
        {
        this.addMessage('<center><img src="' + this._img.src + '" alt="" /></center>');
        this.showMessages();
        
        jQuery.ajax({
            url         : this._wd._makeUrl('bidmake', '/'),
            cache       : false,
            type        : 'POST',
            dataType    : 'json',
            context     : this,
            data        : {
                win         : winField.value,
                tech_id     : window._techId,//this._wd.filters().techId(),
                agree       : agreeCheck.value,
                understand  : understandCheck.value,
                amount      : amount,
                comments    : comments
            },
            success     : function(data, status, xhr) {
                resultText = '';
                if(data.success == 1){
                    resultText ='Done';
                    el = $('#quickApply'+winField.value);
                    if(el instanceof Object){
                            el.parent().append("<a style=\"cursor: pointer;\" id=\"quickApply"+winField.value+"\" onclick=\"quickWithdraw(this, '"+winField.value+"');\" title=\"Review Application\">\n\
<img src='"+this._appliedImg+ "'></a>");
                        el.remove();
                        el = document.getElementById('quickApplyImg'+winField.value);
                        if(el) this._btn = el;
                    }
                } else {
                    resultText = '<strong class="error">';
                    $.each(data.errors, function(key, value) {
                        resultText += value+'</br>';
                    });
                    resultText += '</strong>';

                    el = document.getElementById('quickApply'+winField.value);
                    if(el) this._btn = el;
                    
                }
                this._roll.autohide(true);
                var opt = {
                    width       : 550,
                    height      : '',
                    title       : 'Quick Bid',
                    body        : resultText
                };
                if(!this._btn) this._btn = this.eventbutton;
                this._roll.showNotAjax(this._btn, null, opt);
                if(typeof(this.afterBid) == "function") this.afterBid();

            },
            error       : function (xhr, status, error) {
                if ( xhr.responseText === "authentication required" ) {
                    document.location = "/techs/logIn.php";
                } else {
                    this.clearMessages();
                    this.hideMessages();
                    this.addMessage(xhr.responseText);
                    this.showErrors();
                }
            }
        });
    }
        else
        {
            this.addMessage('<center> <img src="' + this._img.src + '" alt="" /></center>');
            this.showMessages();

            jQuery.ajax({
                url         : this._wd._makeUrl('withdrawmake', '/'),
                cache       : false,
                type        : 'POST',
                dataType    : 'json',
                title       : '<span style="font-size: 13px;">Review Application � </span><a class="ajaxLink" href="wosDetails.php?id='+winField.value+'"><strong>WIN# '+winField.value+'</strong></a>',
                context     : this,
                data        : {
                    win             : winField.value,
                    amount          : amount,
                    comments        : comments,
                    agreewithdraw   : withdrawFieldvalue
                },
                success     : function(data, status, xhr) {
                    resultText = '';
                    if(data.success == 1){
                        resultText ='<span style="padding:10px 10px 10px 10px;"> Your application has been withdrawn.</span>';
                        el = $('#quickApply'+winField.value);
                        if(el instanceof Object){
                            el.parent().append('<img src="'+this._appliedImg+'" id="quickApplyImg'+winField.value+'" title="Application Withdrawn">');
                            el.remove();
                            el = document.getElementById('quickApplyImg'+winField.value);
                            if(el) this._btn = el;
                        }
                    } else {
                        resultText = '<strong class="error">';
                        $.each(data.errors, function(key, value) {
                            resultText += value+'</br>';
                        });
                        resultText += '</strong>';

                        el = document.getElementById('quickApply'+winno);
                        if(el) this._btn = el;
                    
                    }
                agreewithdrawField = typeof(agreewithdrawField) == 'undefined'?false:agreewithdrawField;    
                if(agreewithdrawField)
                {
                    //showLightBoxMsg(resultText, 500,200,false,9000);
                    var content = "<div style='width:250px;height:30px;' id='comtantPopupID'>"+resultText+"</div>";
                    $("<div></div>").fancybox({
                        'autoDimensions' : true,
                        'showCloseButton' :true,
                        'hideOnOverlayClick' : false,
                        'content': content,
                        'onComplete': function () {
                            setTimeout('$.fancybox.close()', 2000);
                        }
                    }).trigger('click');
                }
                this._roll.hide();
                },
                error       : function (xhr, status, error) {
                    if ( xhr.responseText === "authentication required" ) {
                        document.location = "/techs/logIn.php";
                    } else {
                        this.clearMessages();
                        this.hideMessages();
                        this.addMessage(xhr.responseText);
                        this.showErrors();
                    }
                }
            }); 
        }
        
    }

    return false;
}
FSQuickBidTool.prototype.makeAWithdraw = function ( winno,amount,comments,agreewithdraw)
{
    var amountField     = amount;
    var commentsField   = comments;
    var winField        = winno;
    var agreewithdrawField = agreewithdraw

    var amount;

    this.clearMessages();
    
    if ( amountField == "" )
        amount   = null;
    else
        amount   = parseFloat($.trim(amountField));
    
    if ( commentsField == "" )
        comments = "";
    else
        comments = $.trim(commentsField);

    if ( amount === null ) {
        this.addMessage('Amount is required and cannot be empty');
    } else if ( isNaN(amount)  ) {
        this.addMessage('You set incorrect value for Amount');
    } else if ( amount<=0 ) {
        this.addMessage('Amount must be greater than zero.');
    }

    if ( this.hasMessages() ) {     //  Errors
        this.showErrors();
    } else {
        this.addMessage('<center> <img src="' + this._img.src + '" alt="" /></center>');
        this.showMessages();
        
        jQuery.ajax({
            url         : this._wd._makeUrl('withdrawmake', '/'),
            cache       : false,
            type        : 'POST',
            dataType    : 'json',
            title       : '<span style="font-size: 13px;">Review Application � </span><a class="ajaxLink" href="wosDetails.php?id='+winno+'"><strong>WIN# '+winno+'</strong></a>',
            context     : this,
            data        : {
                win             : winField,
                amount          : amount,
                comments        : comments,
                agreewithdraw   : agreewithdrawField
            },
            success     : function(data, status, xhr) {
                resultText = '';
                if(data.success == 1){
                    if(agreewithdrawField)
                    {
                        resultText ='<span style="padding:10px 10px 10px 10px;"> Your application has been withdrawn.</span>';
                    el = $('#quickApply'+winno);
                    if(el instanceof Object){
                            el.parent().append('<img src="'+this._appliedImg+'" id="quickApplyImg'+winno+'" title="Application Withdrawn">');
                        el.remove();
                        el = document.getElementById('quickApplyImg'+winno);
                        if(el) this._btn = el;
                    }
                    }
                    else
                        {
                            resultText ='<span style="padding:10px 10px 10px 10px;"> Your application has been update.</span>';
                        }
                } else {
                    resultText = '<strong class="error">';
                    $.each(data.errors, function(key, value) {
                        resultText += value+'</br>';
                    });
                    resultText += '</strong>';

                    el = document.getElementById('quickApply'+winno);
                    if(el) this._btn = el;
                    
                }

                    //showLightBoxMsg(resultText, 500,200,false,9000);
                    var content = "<div style='width:250px;height:30px;' id='comtantPopupID'>"+resultText+"</div>";
                    $("<div></div>").fancybox({
                        'autoDimensions' : true,
                        'showCloseButton' :true,
                        'width'       : 200,
                        'height'      : "",
                        'hideOnOverlayClick' : false,
                        'content': content,
                        'onComplete': function () {
                            $("#fancybox-wrap").css('width','270px');
                            $.fancybox.resize();
                            $.fancybox.center();
                            setTimeout('$.fancybox.close()', 2000);
                        }
                    }).trigger('click');
                this._roll.hide();
            },
            error       : function (xhr, status, error) {
                if ( xhr.responseText === "authentication required" ) {
                    document.location = "/techs/logIn.php";
                } else {
                    this.clearMessages();
                    this.hideMessages();
                    this.addMessage(xhr.responseText);
                    this.showErrors();
                }
            }
        });
    }

    return false;
}
