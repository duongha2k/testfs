function FSTechSchedule( params ) {
    params = params || {};
    
    if ( params.width === undefined || params.width === null || typeof params.width !== 'number' ) params.width = 750;
    if ( params.height === undefined || params.height === null || typeof params.height !== 'number' ) params.height = 400;
    //params.position = 'middle';

    FSPopupRoll.call(this, params);

    this.waitImage = new Image();
    this.waitImage.src = "/widgets/images/progress.gif";     //  pre load wait image
    this.waitImage.width = 190;
    this.waitImage.height = 14;
    
}

FSTechSchedule.prototype = new FSPopupRoll;
FSTechSchedule.prototype.constructor = FSTechSchedule;

FSTechSchedule.NAME = 'FSTechSchedule';
FSTechSchedule.VERSION = '0.1';
FSTechSchedule.DESCRIPTION = 'Class FSTechSchedule';

FSTechSchedule.prototype.init = function () {
    
}

FSTechSchedule.prototype.showCal = function( calObj, input, format )
{
    var inpValue = $.trim($(input).val());
    if ( inpValue == format ) {
        $(input).val('');
    }
    calObj.showFor(input);
}

FSTechSchedule.prototype.show = function (element, event, params) {
    $('body').css('cursor', 'wait');

    params = params || {};
    params.position = 'middle';
    params.preHandler = this.getContent;
    params.title = '';

    var img = "<div class='tCenter' ><img style='margin:10% auto;' alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></div>";
    this.body(img);

    //  Set wait image indicator and popup width to image's width
    //    left = (parseInt(this.params.width) - parseInt(this.waitImage.width)) / 2
    //    this.body("<div style='position:relative; left:"+left+"px;'><img src='"+this.waitImage.src+"' alt='loading...' width='"+this.waitImage.width+"px' /></div>");

    //  We can call any preHandler function before set popup's body or title
    //  preHandler is called with 2 params:
    //      1 - current object
    //      2 - object params, which set as param into show function
    if ( params.preHandler !== undefined && typeof params.preHandler === 'function' ) { // call preHeandler
        if ( params.context !== undefined && typeof params.context === 'object' )
            params.preHandler.call(params.context, this, params);
        else
            params.preHandler(this, params);
    }

    var width;
    var height;
    if ( typeof params.width === 'number' )  { width = Math.ceil(params.width); }
    else                                     { width = this.params.width; }
    if ( typeof params.height === 'number' ) { height = Math.ceil(params.height); }
    else                                     { height = ''; }

    this.width(params.width);
    this.height(params.height);

    var html                = document.documentElement;
    params.viewportHeight   = window.innerHeight || ( html && html.clientHeight ) || document.body.clientHeight;
    params.viewportWidth    = window.innerWidth  || ( html && html.clientWidth )  || document.body.clientWidth;
    params.vScroll          = $(html).scrollTop()  || $(document.body).scrollTop();
    params.hScroll          = $(html).scrollLeft() || $(document.body).scrollLeft();

    if (!params.position) params.position = 'relative';
    //handle position
    if (params.position == 'middle') {
        params.eTop = (params.viewportHeight - this.height())/2;
        params.eLeft = (params.viewportWidth - this.width())/2;
        $(this.container()).css('position', 'fixed');
    }else{
        params.eTop             = Math.ceil($(element).offset().top);
        params.eLeft            = Math.ceil($(element).offset().left);
        $(this.container()).css('position', 'absolute');
    }

    $(this.header()).css('width', this.width() - 20); //  20 pixels for roll close button

    /* pop-up top position */
    if ( params.eTop + this.height() > params.vScroll + params.viewportHeight ) {
        if ( this.height() > params.viewportHeight ) {
            this.top(params.vScroll + 35);
        } else {
            this.top(params.vScroll + params.viewportHeight - this.height() - 35);
        }
    } else {
        this.top(params.eTop);
    }
    /* pop-up left position */
    if ( params.eLeft + this.width() > params.hScroll + params.viewportWidth ) {
        this.left(params.hScroll + params.viewportWidth - this.width() - 35);
    } else {
        this.left(params.eLeft);
    }

    this.autohide(params.autohide);

    if ( typeof params.title === 'string' ) { // set container title
        this.header(params.title);
    }
    if ( typeof params.body === 'string' ) { // st container body
        this.body(params.body);
    }

    //  We can call any handler function before display popup but after set popup's body or title
    //  Handler is called with 2 params:
    //      1 - current object
    //      2 - object params, which set as param into show function
    if ( params.handler !== undefined && typeof params.handler === 'function' ) { // call heandler
        if ( params.context !== undefined && typeof params.context === 'object' )
            params.handler.call(params.context, this, params);
        else
            params.handler(this, params);
    }

    /**
     *  set autohide handler
     */
    if ( this.autohide() ) {
        $(this.container()).bind('mouseenter mouseleave', {roll:this}, this.hideOnMouseOver);
    }

    $(this.container()).css('z-index', ++FSPopupRoll.prototype.zindex);
    $(this.container()).css('display', '');

    //  We can call any postHandler function after display popup
    //  postHandler is called with 2 params:
    //      1 - current object
    //      2 - object params, which set as param into show function
    if ( params.postHandler !== undefined && typeof params.postHandler === 'function' ) { // call  postHeandler
        if ( params.context !== undefined && typeof params.context === 'object' )
            params.postHandler.call(params.context, this, params);
        else
            params.postHandler(this, params);
    }

    $('body').css('cursor', '');
}

FSTechSchedule.prototype.getContent = function () {
    var schedule = arguments[0];
    var params  = arguments[1] || {};
    var _data = params.data || {};
    var _url = document.location.protocol + '//' + document.location.hostname + '/widgets/dashboard/popup/tech-schedule/';

    jQuery.ajax({
        async: true,
        url: _url,
        cache: false,
        type: 'POST',
        dataType: 'html',
        data: _data,
        context : schedule,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/clients/logIn.php";
            } else {
                this.body(xhr.responseText);
            }
        },
        success : function(data, status, xhr) {
            if ( typeof data === 'string' ) { // st container body
                this.body(data);
            }
            $('body').css('cursor', '');
        }
    });
}
FSTechSchedule.prototype.showPrint = function ( data ) {
    var body = $('#printScheduleContent').contents().find('body');
    body.html( $('#mainScheduleContent').html() );
    body.css('font-family', 'Arial,Helvetica,sans-serif');
    body.css('font-size', '12px');
   // $('#printScheduleContent').contents().find('title').html('Tech Shedule');
    var iframe = document.frames ? document.frames['printScheduleContent'] : document.getElementById('printScheduleContent');
    var ifWin = iframe.contentWindow || iframe;
    iframe.focus();
    ifWin.print();

    return false;
}


