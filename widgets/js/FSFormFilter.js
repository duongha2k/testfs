function FSFormFilter()
{
    this._filters = [];
}

FSFormFilter.prototype.isRegistered = function( obj )
{
    var idx;

    for ( idx in this._filters ) {
        if ( this._filters[idx]._object == obj ) {
            return true;
        }
    }
    return false;
}

FSFormFilter.prototype.addAsEmpty = function( obj, str )
{
    var idx;
    var value;
    var state = false;

    if ( this._filters.length ) {
        for ( idx in this._filters ) {
            if ( this._filters[idx]._object == obj ) {
                if ( this._filters[idx]._as_empty ) {
                    this._filters[idx]._as_empty.push(str);
                    state = true;
                } else {
                    this._filters[idx]._as_empty = [str];
                    state = true;
                }
            }
        }
        if ( !state ) {
            this._filters.push({_object : obj, _as_empty : [str]});
        }
    } else {
        this._filters.push({_object : obj, _as_empty : [str]});
    }
}

FSFormFilter.prototype.findFilters = function( obj )
{
    var idx;

    for ( idx in this._filters ) {
        if ( this._filters[idx]._object == obj ) {
            return this._filters[idx];
        }
    }
    return null;
}

FSFormFilter.prototype.getValue = function( obj )
{
    var idx;
    var value = $(obj).val();
    var filterItem = this.findFilters(obj);
    
    if ( null != filterItem && filterItem._as_empty instanceof Array ) {
        for ( idx in filterItem._as_empty ) {
            if ( filterItem._as_empty[idx] == value ) {
                value = "";
            }
        }
    }
    return value;
}

