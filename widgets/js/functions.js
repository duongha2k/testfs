function openFramePage(url, w, h) {
    try {
        if (window.parent.document && window.parent.document != document) {
            p = window.parent.document;
            if (!w) w = '100%';
            if (!h) h = '100%';
            if (url.indexOf('?') == -1) url + '?';
            $('#dashFrame', p).attr("width", w).attr('height', h).attr('src', url + '&isDash=1');
            return;
        }
    } catch (e) {}
    //		openPopup(url,w,h);
    document.location = url;
}
	
function openLightBoxPage(url, w, h) {
    try {
        $.fancybox.showActivity();
        if (!w) w = 880;
        if (!h) h = 400;
        $("<div></div>").fancybox({
            'href': url,
            'width': w,
            'height': h,
            'type': 'iframe',
            'centerOnScroll': true,
            'onComplete': function () {
                $.fancybox.hideActivity();
            }
        }).trigger('click');
        return;
    } catch (e) {}
    openPopup(url,w,h);
}
	
function showLightBoxMsg(msg, w, h, autoClose, delay) {
    try {
        if (!w) w = 880;
        if (!h) h = 400;
        if (autoClose == undefined) autoClose = true;
        if (delay == undefined) delay = 2000;
        $("<div></div>").fancybox({
            'content': msg,
            'width': w,
            'height': h,
            'onComplete': function () {
                if(autoClose != false)setTimeout('$.fancybox.close()', delay);
            }
        }).trigger('click');
        return;
    } catch (e) {}
    alert(msg);
}	
/**
 * openPopupWin
 *
 * @param string url
 * @param string name
 * @param object options
 * @access public
 * @return object New Window refference
 */
function openPopupWin( url, name, options, noFocus ) {
    name = name || '_blank';
    options = options || {};
    options.height      = ( typeof options.height === 'number' ) ? options.height : 400;
    options.width       = ( typeof options.width === 'number' ) ? options.width : 880;
    options.left        = ( typeof options.left === 'number' ) ? options.left : 100;
    options.top         = ( typeof options.top === 'number' ) ? options.top : 100;
    options.resizable   = ( typeof options.resizable === 'string' && options.resizable.toLowerCase() === 'no' ) ? 'no' : 'yes';
    options.scrollbars  = ( typeof options.scrollbars === 'string' && options.scrollbars.toLowerCase() === 'no' ) ? 'no' : 'yes';
    options.toolbar     = ( typeof options.toolbar === 'string' && options.toolbar.toLowerCase() === 'yes' ) ? 'yes' : 'no';
    options.status      = ( typeof options.status === 'string' && options.status.toLowerCase() === 'yes' ) ? 'yes' : 'no';
    options.menubar     = ( typeof options.menubar === 'string' && options.menubar.toLowerCase() === 'yes' ) ? 'yes' : 'no';
    options.location    = ( typeof options.location === 'string' && options.location.toLowerCase() === 'yes' ) ? 'yes' : 'no';
    options.directories = ( typeof options.directories === 'string' && options.directories.toLowerCase() === 'yes' ) ? 'yes' : 'no';

    var checkedBoxes = $('#widgetContainer input[name="approveCheckbox"]').serializeArray();
    if ( checkedBoxes.length && confirm("Approve checked work orders before continuing?") ) {
        doApprove();
    }

    newwindow = window.open( url, name, 'height='+options.height+',width='+options.width+',left='+options.left+',top='+options.top+',resizable='+options.resizable+',scrollbars='+options.scrollbars+',toolbar='+options.toolbar+',status='+options.status+',directories='+options.directories+',location='+options.location+',menubar='+options.menubar );

    if ( !noFocus && newwindow && newwindow.focus ) newwindow.focus();
    return newwindow;
}
//Alias
function openPopup(url,w,h) {
    openPopupWin(url,null,{
        width:w,
        height:h
    });
}

function setCookie( name, value, expires, path, domain, secure ) {
    var today = new Date();
    today.setTime( today.getTime() );
    if ( expires )	{
        expires = expires * 1000 * 60 * 60 * 24;
    }else{
        expires = 180 * 1000 * 60 * 60 * 24; //six month
    }
    var expires_date = new Date( today.getTime() + (expires) );
    document.cookie = name + "=" +escape( value ) +
        ( ( expires ) ? ";expires=" + expires_date.toGMTString() : "" ) + //expires.toGMTString()
    ( ( path ) ? ";path=" + path : "" ) +
        ( ( domain ) ? ";domain=" + domain : "" ) +
        ( ( secure ) ? ";secure" : "" );
}

function getCookie(theCookie) {
    var search = theCookie + "="
    var returnvalue = "";
    if (document.cookie.length > 0)
    {
        offset = document.cookie.indexOf(search);
        // if cookie exists
        if (offset != -1)
        {
            offset += search.length;
            // set index of beginning of value
            end = document.cookie.indexOf(";", offset);
            // set index of end of cookie value
            if (end == -1) end = document.cookie.length;
            returnvalue=unescape(document.cookie.substring(offset, end));
        }
    }
    return returnvalue;
}





//SETTING UP OUR POPUP
//0 means disabled; 1 means enabled;
var popupStatus = 0;

//loading popup with jQuery magic!
function loadPopup(){
    //loads popup only if it is disabled
    if(popupStatus==0){
        $("#backgroundPopup").css({
            "opacity": "0.7"
        });
        $("#backgroundPopup").fadeIn("slow");
        $("#popupContact").fadeIn("slow");
        popupStatus = 1;
    }
}

//disabling popup with jQuery magic!
function disablePopup(){
    //disables popup only if it is enabled
    if(popupStatus==1){
        $("#backgroundPopup").fadeOut("slow");
        $("#popupContact").fadeOut("slow");
        popupStatus = 0;
        jQuery("#popupContact").remove();
        jQuery("#backgroundPopup").remove();
    }
}

function padNumber (number, length) {
    var str = number.toString();

    while (str.length < length)
        str = "0" + str;

    return str;
}

//Output format: 2009-12-18 10:54:50.546 as expected by jquery.dateFormat
function getDateUTC () {
	var date = new Date ();
	var y = date.getUTCFullYear ();
	var m = padNumber (date.getUTCMonth (), 2);
	var d = padNumber (date.getUTCDate (), 2);
	var hr = padNumber (date.getUTCHours (), 2);
	var min = padNumber (date.getUTCMinutes (), 2);
	var sec = padNumber (date.getUTCSeconds (), 2);
	var ms = padNumber (date.getUTCMilliseconds (), 3);

	return y + "-" + m + "-" + d + "T" + hr + ":" + min + ":" + sec + "." + ms + "Z";
}

function getDateByOffset (offset) {
    var date = new Date ();
    var utc = date.getTime () + (date.getTimezoneOffset () * 60000);
    var ms = utc + (3600000*offset);
    var cst = new Date (ms);

    return cst;

}

function getDateCST () {
	return getDateByOffset (-5);
}

//centering popup
function centerPopup($msg){
    //request data for centering
    //request data for centering
    jQuery('body').append('<div id="popupContact">'  
        +"<style>"
        +"#backgroundPopup {"
        +"z-index:9999;"
        +"background: none repeat scroll 0 0 #000000;"
        +"border: 1px solid #CECECE;"
        +"display: none;"
        +"height: 100%;"
        +" left: 0;"
        +" position: fixed;"
        +" top: 0;"
        +" width: 100%;"
        +"}"
        +"#popupContact {"
        +"  background: none repeat scroll 0 0 #FFFFFF;"
        +"   border: 2px solid #CECECE;"
        +"   display: none;"
        +"   font-size: 13px;"
        +"  z-index: 99999;"
        +"   padding: 12px;"
        +"  position: fixed;"
        +"  width: 408px;"
        +"}"
        +"#popupContact h1 {"
        +"  border-bottom: 1px dotted #D3D3D3;"
        +"   color: #6FA5FD;"
        +"   font-size: 22px;"
        +"   font-weight: 700;"
        +"   margin-bottom: 20px;"
        +"   padding-bottom: 2px;"
        +"  text-align: left;"
        +"}"
        +"#popupContactClose {"
        +"   color: #6FA5FD;"
        +"   display: block;"
        +"   font-size: 14px;"
        +"   font-weight: 700;"
        +"   line-height: 14px;"
        +"    position: absolute;"
        +"   right: 6px;"
        +"  top: 4px;"
        +"}"
        +"</style>"
        +'<a id="popupContactClose"><div id="_popup_569643_header_button" class="popup-header-button"> </div></a>  '
        +'<center style="color:red;padding-bottom:12px;">Attention!</center>  '
        +'<span id="contactArea" style="color:red;">  '
        + $msg
        +'</span>  '
        +'</div>  '
        +'<div id="backgroundPopup"></div>');
    
    var windowWidth = document.documentElement.clientWidth;
    var windowHeight = document.documentElement.clientHeight;
    var popupHeight = $("#popupContact").height();
    var popupWidth = $("#popupContact").width();
    //centering
    var top = (windowHeight/2-popupHeight/2) + jQuery(window).scrollTop();
    $("#popupContact").css({
        "position": "absolute",
        "top": top,
        "left": windowWidth/2-popupWidth/2
    });
    //only need force for IE6
	
    $("#backgroundPopup").css({
        "height": windowHeight
    });
	
}

function Attention($msg)
{
	if ($msg == null || $msg == '') return;
    centerPopup($msg);
    //load popup
    loadPopup();
    //CLOSING POPUP
    //Click the x event!
    $("#popupContactClose").click(function(){
        disablePopup();
    });
    //Press Escape event!
    $(document).keypress(function(e){
        if(e.keyCode==27 && popupStatus==1){
            disablePopup();
        }
    });
}
    /**
    * for TN
    */
    var lang = {
    requitePhone:
        {
        valid : 'Click to notify FULLNAME that this Phone # is Invalid',
        invalid: 'Invalid Phone # Reported',
        techinvalid : 'This Phone # has been marked as Invalid. Please edit the phone # or click the red icon to mark it as valid.'
    }
}

function doNothing() {
    return false;
}
	
function disableLinks(root) {
    root.each(function(index, element) {
        txt = $(element).text();
        //			element.unbind('click').attr('onclick', '').click(doNothing);
        $(element).parent().html(txt);
    });				
}
	
function bindTechDeactivatedMsg(element) {
    $(element).unbind('click').attr('onclick', '').click(function(event) {
        event.preventDefault();
        event.stopImmediatePropagation();
        showTechDeactivatedMsg();
    });
}
	
function showTechDeactivatedMsg(closeCallBack) {
    if (closeCallBack == null)
        closeCallBack = function() {
        };
    $("<div></div>").fancybox({
        'autoDimensions' : false,
        'width'	: 420,
        'height' : 200,
        'transitionIn': 'none',
        'transitionOut' : 'none',
        'hideOnOverlayClick' : false,
        'onClosed' : closeCallBack,
        'content' : "<div style='text-align: center; font-size: 18px; color: #d00; margin-bottom: 10px'>Attention!</div>This technician account is restricted. You may call 952-288-2500, option 1 if you think this is in error. Any earned payments (client approved) at the time of restriction will be paid as normal. Payments for any future work to this Taxpayer ID # (under any FS technician ID#) will be withheld. Any communications to FieldSolutions clients or FieldSolutions technicians will be prosecuted as allowed by law as a violation of your signed confidential information and independent contractors agreement."
    }).trigger('click');
}
	
//824
function formatPhone ()
{
    if (this.value == null || this.value == "" || this.value.length>10) return;
    var validFormat = /^[(]?(\d{3})[)]?(?:\s|-|.)*(\d{3})[-\s.]*(\d{4})$/;
    var parts = this.value.match(validFormat);
    if (parts == null) return;
    this.value = "(" + parts[1] + ") " + parts[2] + "-" + parts[3];
};
//end 824

    //415
  function  showMgsWithDraw ()
    {
        var msg = "Are you sure you want to withdraw your Application for this Work Order?";
        var apprvebutton = "Continue Updating";
        
        var content = "<div style='width:400px;height:auto;' id='comtantPopupID'>\
                <div>"+msg+"<br></div>\
                <div>\
                    <table border='0' width='100%'>\
                        <tr>\
                            <td style='width:50%;text-align:right;'>\
                                <input id='WithDrawOkCancel' class='link_button middle2_button' type='button' value='Cancel'>\
                            </td>\
                            <td style='width:50%;text-align:left;'>\
                                <input id='WithDrawOk' class='link_button middle2_button' type='button' value='//"+apprvebutton+"'>\
                            </td>\
                        </tr>\
                    </table>\
                </div>";
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :true,
            'hideOnOverlayClick' : false,
            'content': content
        }
        ).trigger('click');
        jQuery("#WithDrawOk").click(function(){
            parames.onSubmit(parames.el);
            $.fancybox.close();
        });   
        jQuery("#WithDrawOkCancel").click(function(){
            $.fancybox.close(); 
        });
    }
/**
 * for function TN
 */
var detailObject = {};    
window.detailObject = detailObject;

detailObject.onInit = {
    /**
     * Client view of areas that need the indicator if phone number has been marked as �Invalid�
     * phonetype = 1 is primary, = 2 secondary
     */
    _index : 0,
    RequitePhone : function (
        el,
        phone,
        techid,
        requite,
        phonetype,
        inputType,
        FullName)
    {

        var img = "/widgets/images/ico_phonex16.png";
        var title = lang.requitePhone.valid.replace('FULLNAME',Base64.decode(FullName));
        var hasClick = true;
        switch (requite)
        {
            case "1":
                img = "/widgets/images/ico_phonex16.png";
                title = lang.requitePhone.valid.replace('FULLNAME',Base64.decode(FullName));
                hasClick = true;
                break;
            case "2":
                img = "/widgets/images/ico_Notphonex16.png";
                title = lang.requitePhone.invalid;
                hasClick = false;
                break;
        }
        
        var imageAccessPhone = '&nbsp;<img onclick="javascript:detailObject.onInit.RequitePhoneClick(this,\''+techid+'\',\''+phonetype+'\',\''+FullName+'\',\''+phone+'\')" class="imgClassReQuitePhone imgClassReQuitePhone_'+techid+'_'+phonetype+'" align="absmiddle" src="'+img+'" title="'
            +title
            +'"/>';
        if((requite == "2" && !hasClick) || jQuery.trim(phone) == "")
        {
            imageAccessPhone = "";
        }    
        
        var elImg = null;
        if(inputType == 'html')
        {
            $(el).html(phone + imageAccessPhone);
            elImg = jQuery(el+" img");
        }
        else if(inputType == 'text')
        {
            jQuery(el).parent().children(".imgClassReQuitePhone").remove();
            $(el).val(phone);
            jQuery(el).after(imageAccessPhone);
            elImg = jQuery(el).parent().children(".imgClassReQuitePhone");
        }
    },
    /**
     *RequitePhoneForTech
     *phonetype = 1 is primary, = 2 secondary
     *usertype= 2 for Tech. = 3 for Admin
     */
    RequitePhoneForTech : function (el,
    phone,
    techid,
    requite,
    phonetype,
    inputType,
    usertype,
    hasclick,
    postbackid)
    {
        var img = "/widgets/images/ico_phonex16.png";
        var title = lang.requitePhone.valid;
        var hasClick = false;
        switch (requite)
        {
            case "1":
                img = "/widgets/images/ico_phonex16.png";
                title = lang.requitePhone.valid;
                hasClick = false;
                break;
            case "2":
                img = "/widgets/images/ico_Notphonex16.png";
                title = lang.requitePhone.techinvalid;
                hasClick = hasclick;
                break;
        }
        var imageAccessPhone = "";
        if(requite == "2")
        {
            imageAccessPhone = '&nbsp;<img postbackid="'+postbackid+'" class="imgClassReQuitePhoneTech imgClassReQuitePhoneTech_'+techid+'_'+phonetype+'" align="absmiddle" src="'+img+'" title="'
                +title
                +'"/>';
        }
        if(phone == "" || phone == null)
        
        {
            imageAccessPhone = "";
        }
        var elImg = null;
        if(inputType == 'html')
        {
            $(el).html(phone + imageAccessPhone);
            elImg = jQuery(el+" img");
        }
        else if(inputType == 'text')
        {
            jQuery(el).parent().children(".imgClassReQuitePhoneTech").remove();
            $(el).val(phone);
            jQuery(el).after(imageAccessPhone);
            elImg = jQuery(el).parent().children(".imgClassReQuitePhoneTech");
        }
        
        if(hasClick && elImg != null)
        {
            jQuery(elImg).click(function(){
                   
                 detailObject.onInit.RequitePhoneClickForTech(this,techid,phonetype,usertype);
                
            });
        }
        
    },
    /**
     *RequitePhoneForTech
     *phonetype = 1 is primary, = 2 secondary
     *usertype= 2 for Tech. = 3 for Admin
     */
    RequitePhoneForAdmin : function (el,
    phone,
    techid,
    requite,
    phonetype,
    inputType,
    usertype,
    hasclick,
    postbackid)
    {
        var img = "/widgets/images/ico_phonex16.png";
        var title = lang.requitePhone.valid;
        var hasClick = false;
        switch (requite)
        {
            case "1":
                img = "/widgets/images/ico_phonex16.png";
                title = lang.requitePhone.valid;
                hasClick = false;
                break;
            case "2":
                img = "/widgets/images/ico_Notphonex16.png";
                title = lang.requitePhone.techinvalid;
                hasClick = hasclick;
                break;
        }
        var imageAccessPhone = "";
        if(requite == "2")
        {
            imageAccessPhone = '&nbsp;<img postbackid="'+postbackid+'" class="imgClassReQuitePhoneTech imgClassReQuitePhoneAdmin_'+techid+'_'+phonetype+'" align="absmiddle" src="'+img+'" title="'
                +title
                +'"/>';
        }
        if(phone == "" || phone == null)
        
        {
            imageAccessPhone = "";
        }
        var elImg = null;
        if(inputType == 'html')
        {
            $(el).html(phone + imageAccessPhone);
            elImg = jQuery(el+" img");
        }
        else if(inputType == 'text')
        {
            jQuery(el).parent().children(".imgClassReQuitePhoneTech").remove();
            $(el).val(phone);
            jQuery(el).after(imageAccessPhone);
            elImg = jQuery(el).parent().children(".imgClassReQuitePhoneTech");
        }
        
        if(hasClick && elImg != null)
        {
            jQuery(elImg).click(function(){
                   
                 detailObject.onInit.RequitePhoneClickForAdmin(this,techid,phonetype,usertype);
                
            });
        }
        
    },
    /**
     * RequitePhoneClick
     */
    _rollx : null,
    timeout : null,
    RequitePhoneClick:function(el,techid,phonetype,FullName,phone){
        FullName = Base64.decode(FullName);
        var obj = this;
        if(typeof(detailObject.onInit._rollx) != 'undefined' && detailObject.onInit._rollx != null)
        {
            detailObject.onInit._rollx.hide();
        }    
        detailObject.onInit._rollx = new FSPopupRoll();
  
        var html = "<table width='100%'>"
            html += "<tr>";
            html +=     "<td colspan='2'>";
            html +=     "Click Submit to notify "+FullName+" that "+phone+" is an invalid phone #.";
            html +=     "</td>";
            html += "</tr>";
            html += "<tr>";
            html +=     "<td align='center'>";
            html +=     "<input type='button' class='link_button middle2_button' id='cmdRequiredPhoneCancel' value='Cancel' />";
            html +=     "</td>";
            html +=     "<td align='center'>";
            html +=     "<input type='button' class='link_button middle2_button' id='cmdRequiredPhoneSubmit' value='Submit' />";
            html +=     "</td>";
            html += "</tr>";
            html +="</table>";
            
        detailObject.onInit._rollx.autohide(false);
        var opt = {
            width       : 300,
            height      : '',
            //position    : 'middle',
            title       : '',
            body        : html
        };
            
        detailObject.onInit._rollx.showNotAjax(el, el, opt);
        
        jQuery("#cmdRequiredPhoneCancel").click(function() {
            detailObject.onInit._rollx.hide();
        });
        jQuery("#cmdRequiredPhoneSubmit").click(function() { 
            
            obj.showLoader();
            $.ajax({
                url         : '/widgets/dashboard/client/updaterequitephone/',
                //dataType    : 'json',
                data        : 'id='+techid+'&phonetype='+phonetype,
                cache       : false,
                type        : 'POST',
                success     : function (data, status, xhr) {
                    obj.hideLoader();
                    detailObject.onInit._rollx.hide();
                    switch (data)
                    {
                        case "1":
                            jQuery(el).attr('src','/widgets/images/ico_phonex16.png');
                            jQuery(el).attr("title",lang.requitePhone.valid.replace('FULLNAME',FullName));
                            break;
                        case "2":
                            jQuery(el).hide();
                            jQuery('.imgClassReQuitePhone_'+techid+'_'+phonetype).each(function(){
                                jQuery(this).hide();
                            });
                            detailObject.onInit.showThanksPhonePopupForClient(el,FullName,phone);
                            break;  
                    }
                },
                error       : function ( xhr, status, error ) {
                    //detailObject.onInit._roll.hide();
                }
            });
        });
        
    },
    /**
     * RequitePhoneClickForTech
     */
    RequitePhoneClickForAdmin : function(el,techid,phonetype,usertype){
        var obj = this;
        this.showLoader();
        $.ajax({
            url         : '/widgets/admindashboard/tech/updaterequitephone/',
            //dataType    : 'json',
            data        : 'id='+techid+'&phonetype='+phonetype+'&usertype='+usertype,
            cache       : false,
            type        : 'POST',
            success     : function (data, status, xhr) {
                obj.hideLoader();
                var postbackid = jQuery(el).attr("postbackid");
                switch (data)
                {
                    case "1":
                        jQuery(el).remove();
                        jQuery('.imgClassReQuitePhoneAdmin_'+techid+'_'+phonetype).each(function(){
                            jQuery(this).remove();
                        });
                            
                        // msg thanks
                        detailObject.onInit.showThanksPhonePopup();
                        break;
                    case "2":
                        jQuery(el).attr('src','/widgets/images/ico_Notphonex16.png');
                        jQuery(el).attr("title",lang.requitePhone.techinvalid);
                        jQuery(postbackid).attr('src','/widgets/images/ico_Notphonex16.png');
                        jQuery(postbackid).attr("title",lang.requitePhone.invalid);
                        
                        break;  
                }
            },
            error       : function ( xhr, status, error ) {
                //detailObject.onInit._roll.hide();
            }
        });
    },
    /**
     * RequitePhoneClickForTech
     */
    RequitePhoneClickForTech : function(el,techid,phonetype,usertype){
        var obj = this;
        this.showLoader();
        $.ajax({
            url         : '/widgets/dashboard/tech/updaterequitephone/',
            //dataType    : 'json',
            data        : 'id='+techid+'&phonetype='+phonetype+'&usertype='+usertype,
            cache       : false,
            type        : 'POST',
            success     : function (data, status, xhr) {
                obj.hideLoader();
                var postbackid = jQuery(el).attr("postbackid");
                switch (data)
                {
                    case "1":
                        jQuery(el).remove();
                        jQuery('.imgClassReQuitePhoneTech_'+techid+'_'+phonetype).each(function() {
                            jQuery(this).remove();
                        });
                        // msg thanks
                        detailObject.onInit.showThanksPhonePopup();
                        break;
                    case "2":
                        jQuery(el).attr('src','/widgets/images/ico_Notphonex16.png');
                        jQuery(el).attr("title",lang.requitePhone.techinvalid);
                        jQuery(postbackid).attr('src','/widgets/images/ico_Notphonex16.png');
                        jQuery(postbackid).attr("title",lang.requitePhone.invalid);
                        
                        break;  
                }
            },
            error       : function ( xhr, status, error ) {
                //detailObject.onInit._roll.hide();
            }
        });
    },
    /**
     * showStartPopup
     */
    loaderVisible : false,
    showLoader: function()
    {
        if(!this.loaderVisible){
            $("div#loader").fadeIn("fast");
            this.loaderVisible = true;
        } 
    },
    hideLoader : function(){
        if(this.loaderVisible){
            var loader = $("div#loader");
            loader.stop();
            loader.fadeOut("fast");
            this.loaderVisible = false;
        }
    },
    _roll : null,
    showStartPopup : function() {
        detailObject.onInit._roll = new FSPopupRoll();
        if(typeof(window.parent) != 'undefined')
        { 
            detailObject.onInit._roll = new window.parent.FSPopupRoll();
        }
        var waitImage = new Image();
        waitImage.src = "/widgets/images/progress.gif";     //  pre load wait image
        waitImage.width = 190;
        waitImage.height = 14;
        var html = "<div class=\"tCenter\"><img alt='Wait...' src='"
            + waitImage.src
            +"' width='"+waitImage.width+"px' \n\
            height='"+waitImage.height+"px' /></div>";
        
        detailObject.onInit._roll.autohide(false);
        var opt = {
            width       : 300,
            height      : '',
            position    : 'middle',
            title       : 'Processing',
            body        : html
        };
        return this._roll.showNotAjax(null, null, opt);
    }
    ,
    applyFancybox:function(message){
        var t = $("<div id='Fancybox'></div>").fancybox({
            'width': 350,
            'height' : 125,
            'content' : message,
            'autoDimensions' : false,
            'centerOnScroll' : true
        });
        t.trigger('click');
    },
    hideFancybox : function(){
            jQuery("#fancybox-close").click();
    },
    showThanksPhonePopup : function() {
        var messageHtml = "<h2>Update Complete</h2>";
        messageHtml += "<p>Your Profile has been successfully updated.</p>";
        messageHtml += "<b>Thank you!</b>";
			
        detailObject.onInit.hideLoader();
        detailObject.onInit.applyFancybox(messageHtml);	
        if(detailObject.onInit.timeout != null && typeof(detailObject.onInit.timeout) != 'undefined')
        {
            clearTimeout(detailObject.onInit.timeout);
        }
        detailObject.onInit.timeout = setTimeout(function() {
            detailObject.onInit.hideFancybox();
        },3000);
    },
    showThanksPhonePopupForClient : function(el,FullName,phone) {
        el = this;
        if(typeof(window.parent) != 'undefined')
        { 
            el = window.parent.detailObject.onInit;
        }
        var html = "<div class=\"tleft\">"+FullName+" has been notified that "+phone+" is an invalid phone #. <br><br>Thank you!<br><br> Your FieldSolutions Team.</div>";
        
        el.hideLoader();
        el.applyFancybox(html);
        el.loaderVisible = true;
        
        if(el.timeout != null && typeof(el.timeout) != 'undefined')
        {
            clearTimeout(el.timeout);
        }
        el.timeout = setTimeout(function() {
            el.hideFancybox();
        },3000);
    },

    parterJsonMyschedule : function(data)
    {
        if(jQuery.trim(data) == "") return "";
        var json = eval("("+data+")");
        var techid = "";
        for(var i = 0;i < json.length;i++)
        {
            techid += ","+json[i].WINNUM + "-"+json[i].workstatus;
    }
        if(techid != "") 
        {
            techid = techid.substring(1,techid.length);
        }
        return techid;
    },
    showMyscheduleDownload : function(el) {
        detailObject.onInit._roll = new FSPopupRoll();

        var html = "<form method='post' target='_blank' action='/techs/ajax/getFileCalendarICS.php?filename=MySchedule' id='frmDownloadSchedule'><div style='padding-bottom:5px;'>Download iCalendar files (*.ics) to add your FieldSolutions work to your Internet Calendar. Supported calendars include Microsoft Outlook, Google Calendar, Apple iCal, Yahoo! Calendar, and many more.</div>";
        html += "<div style='padding-bottom:5px;'>Select the Work Orders you wish to download:</div>";
        html += "<input type='hidden' value='' name='techids' id='hidScheldueTechids'>";
        html += "<div style='padding-bottom:5px;'><input id='chkAssignedWorkOrders' value='1' type='checkbox' checked />&nbsp;&nbsp;Assigned Work Orders</div>";
        html += "<div style='padding-bottom:5px;'><input id='chkAppliedWorkOrders' value='1' type='checkbox' checked />&nbsp;&nbsp;Applied Work Orders</div>";
        html += "<div style='text-align:center;'><input value='Download' type='button' id='cmdDownloadMyscheduleDownload' class ='link_button download_button cmdDownload' />&nbsp;&nbsp;&nbsp;<input type='button' value='Cancel' id='cmdCancelMyscheduleDownload'  class='link_button' style='background: url(\"/widgets/images/button_popup.png\") no-repeat scroll left top transparent;width:60px;' /></div></form>";
        detailObject.onInit._roll.autohide(false);
        var opt = {
            width       : 300,
            height      : '',
            //position    : 'middle',
            title       : 'Download My Schedule',
            body        : html
};
        this._roll.showNotAjax(el, el, opt);
        jQuery("#cmdCancelMyscheduleDownload").click(function(){
            detailObject.onInit._roll.hide();
        });
        jQuery("#cmdDownloadMyscheduleDownload").click(function(){
            var chkAssignedWorkOrders = jQuery("#chkAssignedWorkOrders").is(':checked');
            var chkAppliedWorkOrders = jQuery("#chkAppliedWorkOrders").is(':checked');
            if(chkAssignedWorkOrders && chkAppliedWorkOrders)
            {
                $.ajax({
                    url         : '/widgets/dashboard/tech-dashboard/assignedworkics/',
                    cache       : false,
                    type        : 'POST',
                    success     : function (data, status, xhr) {
                        var techid = detailObject.onInit.parterJsonMyschedule(data);
                        $.ajax({
                            url         : '/widgets/dashboard/tech-dashboard/appliedworkics/',
                            data        :"assign="+detailObject.onInit.parterJsonMyschedule(data),
                            cache       : false,
                            type        : 'POST',
                            success     : function (data, status, xhr) {
                                var techid2 = detailObject.onInit.parterJsonMyschedule(data);
                                if(techid != "") techid2 += (techid2!=''?",":'') +techid;
                                jQuery("#hidScheldueTechids").val(techid2);
                                jQuery("#frmDownloadSchedule").submit();
                            },
                            error       : function ( xhr, status, error ) {
                            }
                        });
                    },
                    error       : function ( xhr, status, error ) {
                    }
                });
            }
            else if(chkAssignedWorkOrders)
            {
                $.ajax({
                    url         : '/widgets/dashboard/tech-dashboard/assignedworkics/',
                    cache       : false,
                    type        : 'POST',
                    success     : function (data, status, xhr) {
                        var techid = detailObject.onInit.parterJsonMyschedule(data);
                        jQuery("#hidScheldueTechids").val(techid);
                        jQuery("#frmDownloadSchedule").submit();
                    },
                    error       : function ( xhr, status, error ) {
                    }
                });
            }  
            else if(chkAppliedWorkOrders)
            {
                $.ajax({
                    url         : '/widgets/dashboard/tech-dashboard/appliedworkics/',
                    cache       : false,
                    type        : 'POST',
                    success     : function (data, status, xhr) {
                        var techid = detailObject.onInit.parterJsonMyschedule(data);
                        jQuery("#hidScheldueTechids").val(techid);
                        jQuery("#frmDownloadSchedule").submit();
                    },
                    error       : function ( xhr, status, error ) {
                    }
                });
            } 
        });
    },
    showMyscheduleDownloadAssigned : function(el) {
        detailObject.onInit._roll = new FSPopupRoll();
        
        var tab  = jQuery(el).attr("tab");
        var filename = "MyAssignedSchedule";
        var url = '/widgets/dashboard/tech-dashboard/assignedworkics/';
        var title = "Assigned";
        switch (tab)
        { 
            case 'techassigned':
                url = '/widgets/dashboard/tech-dashboard/assignedworkics/';
                break;
            case 'techavailable':
                url = '/widgets/dashboard/tech-dashboard/availableworkics/';
                filename = "MyAvailableSchedule";
                title = "Available";
                break;    
        }
        var html = "<form method='post' target='_blank' action='/techs/ajax/getFileCalendarICS.php?filename="+filename+"' id='frmDownloadScheduleAssigned'><div style='padding-bottom:5px;'>Download iCalendar files (*.ics) to add your FieldSolutions work to your Internet Calendar. Supported calendars include Microsoft Outlook, Google Calendar, Apple iCal, Yahoo! Calendar, and many more.</div>";
        html += "<input type='hidden' value='' name='techids' id='hidScheldueTechidsAssigned'>";
        html += "<div style='text-align:center;'><input value='Download' type='button' id='cmdDownloadMyscheduleDownloadAssign' class ='link_button download_button cmdDownload' />&nbsp;&nbsp;&nbsp;<input type='button' value='Cancel' id='cmdCancelMyscheduleDownloadAssign'  class='link_button' style='background: url(\"/widgets/images/button_popup.png\") no-repeat scroll left top transparent;width:60px;' /></div></form>";
        detailObject.onInit._roll.autohide(false);
        var opt = {
            width       : 300,
            height      : '',
            //position    : 'middle',
            title       : 'Download My '+title+' Schedule',
            body        : html
        };
        this._roll.showNotAjax(el, el, opt);
        jQuery("#cmdCancelMyscheduleDownloadAssign").click(function(){
            detailObject.onInit._roll.hide();
        });
        
        jQuery("#cmdDownloadMyscheduleDownloadAssign").click(function(){
            $.ajax({
                url         : url,
                cache       : false,
                type        : 'POST',
                success     : function (data, status, xhr) {
                    var techid = detailObject.onInit.parterJsonMyschedule(data);
                    jQuery("#hidScheldueTechidsAssigned").val(techid);
                    jQuery("#frmDownloadScheduleAssigned").submit();
                },
                error       : function ( xhr, status, error ) {
                }
            });
        });
            
    },
    showMyscheduleCalendar : function(el) {
       $("<div></div>").fancybox(
            {
                    'autoDimensions' : false,
                    'width'	: 900,
                    'height' : 550,
                    'transitionIn': 'none',
                    'transitionOut' : 'none',
                    'type': 'iframe',
                    'href': el
            }
        ).trigger('click');
    },
    checkApproved : function(parames) {        
        if(typeof(parames) == 'undefined') return false; 
        if(typeof(parames.onSubmit) != 'function') return false;
        if(typeof(parames.TechClaim) == 'undefined') return false;
        if(typeof(parames.bcatapproved) == 'undefined') return false;
        if(typeof(parames.isApproved) == 'undefined') return false;
        if(typeof(parames.el) == 'undefined') return false;
        if(jQuery("."+parames.TechClaim).length <= 0){ 
            parames.onSubmit(parames.el); 
        }else{
            var ok = true;
            jQuery("."+parames.TechClaim).each(function(){
            var itemid = jQuery(this).attr("itemid");
            var _TechClaim = parseFloat(jQuery(this).html());
            if(_TechClaim > 0 && !jQuery("."+parames.bcatapproved+"[itemid='"+itemid+"']").is(":checked"))
            {
                ok = false;
                return false;
            }    
            });
            if(parames.isApproved != "") 
            {
                if(!jQuery("#"+parames.isApproved).is(":checked")) ok = true;
            } 
            if(ok)
            {
                parames.onSubmit(parames.el);
            } 
            else
            {
                //941
                if(typeof($("#alreadyApproved").val()) == "undefined" || $("#alreadyApproved").val() == 0 )
                {
                    this.showMgsApproved(parames);
                }
                else
                {
                    parames.onSubmit(parames.el);
                }       
                //end 941
            }
        }
    },
    showMgsApproved : function(parames)
    {        
        //376
        //var msg = "Please ensure that all Technician Expense Claims have been reviewed before Approving this Work Order.<br>";
        var msg = "All Technician Expense Claims have not been Approved for this Work Order. If you would like to review the Claims, click the 'Cancel' button. Otherwise, click 'Approve Work Order'.<br>";
        var apprvebutton = "Approve Work Order";
        
        if(typeof(parames.wins) != 'undefined')
        {
            var wins = parames.wins.split(",");
            if(wins.length <= 1)
            {
                //msg = "Please ensure that all Technician Expense Claims have been reviewed before Approving this Work Order: WIN#"+parames.wins+"<br>";
                msg = "All Technician Expense Claims have not been Approved for this Work Order. If you would like to review the Claims, click the 'Cancel' button. Otherwise, click 'Approve Work Order'.<br>";
                apprvebutton = 'Approve Work Order';
            }
            else
            {
                //msg = "Please ensure that all Technician Expense Claims have been reviewed before Approving these Work Orders:<br> ";
                msg = "All Technician Expense Claims have not been Approved for these Work Orders. If you would like to review the Claims, click the 'Cancel' button. Otherwise, click 'Approve Work Orders'.<br>";
                for(var i = 0; i < wins.length;i++)
                {
                    msg += "WIN#"+wins[i]+"<br> "
                } 
                apprvebutton = 'Approve Work Orders';
                //msg = msg.substring(0, msg.length-2);
            }    
            
        }    
        var content = "<div style='width:400px;height:auto;' id='comtantPopupID'>\
                  <div>"+msg+"<br></div>\
                  <div><table border='0' width='100%'>\
                  <tr>\
                  <td style='width:50%;text-align:right;'>\
                  <input id='checkApprovedCancel' class='link_button middle2_button' type='button' value='Cancel'>\
                  </td>\
                  <td style='width:50%;text-align:left;'>\
                  <input id='checkApprovedOk' class='link_button middle2_button' type='button' value='"+apprvebutton+"'>\
                    </td>\
                  </tr>\
                  </table>\
                </div>";
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :true,
            'hideOnOverlayClick' : false,
            'content': content
        }
        ).trigger('click');
        jQuery("#checkApprovedOk").click(function(){
            parames.onSubmit(parames.el);
            $.fancybox.close();
        });   
        jQuery("#checkApprovedCancel").click(function(){
            $.fancybox.close(); 
        });
    }
    ,
    checkApprovedButton : function(parames) {
        
        var wos = parames.wos;        
        var event = this;
        $.ajax({
            url         : "/widgets/dashboard/js/check-approve/",
            dataType    : 'json',
            data        : {wo_ids : wos},
            cache       : false,
            type        : 'post',
            success     : function (data, status, xhr) {
                if(data.win == "")  
                    parames.onSubmit(parames.el);
                else
                {
                   var wins = data.win.split(",");
                   if(wins.length > 0)
                   {
                       parames.wins = data.win;
                   }    
                   event.showMgsApproved(parames); 
                }       
            }
        });
    },
    rollWarning : null,
    showPopup : function(width,height,title,el){
        this.rollWarning = new FSPopupRoll();
        var content = "<div id='comtantPopupID'><div style='text-align:center;height:150px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
        var opt = {
            width       : width,
            height      : height,
            position    : 'middle',
            title       : title,
            body        : content
        };
        this.rollWarning.showNotAjax(el, el, opt);
    },
    warningAssignWO : function(el,wins,techid,onsuccess,type,onajax){
          if(jQuery.trim(wins) == "" || jQuery.trim(techid) == ""){
              if(typeof(onsuccess) == 'function'){
                  onsuccess();
              }
              return false;
          }
          if(typeof(type) == 'undefined') type = 1;
          $.ajax({
            url : "/widgets/dashboard/popup/warning-assign-wo",
            data : "wins="+wins+"&techid="+techid+"&type="+type,
            cache : false,
            type : 'POST',
            dataType : 'html',
            context : this,
            error : function (xhr, status, error) {
                $('body').css('cursor', '');
                if ( xhr.responseText === "authentication required" ) {
                    document.location = "/clients/logIn.php";
                } else {
                    jQuery("#comtantPopupID").html("Sorry, but an error occurred. No results found.");
                }
            },
            success : function(data, status, xhr) {
                if ( xhr.responseText === "data empty xml" ) {
                    if(typeof(onsuccess) == 'function'){
                        onsuccess();
                    }
                    return false;
                }
                if(typeof(onajax) == 'function'){
                    onajax();
                }
                this.showPopup(750, 350,'<center style="color:red;font-weight: bold;">Warning!</center>',el );
                jQuery("#comtantPopupID").html(data);
                var event = this;
                jQuery("#cmdAssignWarning").click(function(){
                    if(typeof(onsuccess) == 'function'){
                        onsuccess();
                        event.rollWarning.hide();
                    }
                });
                jQuery("#cmdCancelWarning").click(function(){
                    event.rollWarning.hide();
                });
            }
        });
    },amountField : function(amountField, blur){
        jQuery(amountField).keydown(function (event) { 
                var v = this.value;
                var isNotBackspaceOrDelete = event.keyCode != 46 
                    && event.keyCode != 8 
                    && event.keyCode != 110 
                    && event.keyCode != 190;
                var not = ((event.keyCode == 110 || event.keyCode == 190) && v.indexOf(".") >= 0);
                var isInteger = (event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105);
                if((isNotBackspaceOrDelete && !isInteger) || not) {
                    event.preventDefault();
                }
            }).keyup(function(event){
                var isNotBackspaceOrDelete =  event.keyCode != 8 && event.keyCode != 110 && event.keyCode != 190;

                if(!isNotBackspaceOrDelete){
                    return false;
                }
                var v = this.value;
                if(v.indexOf(".") >= 0 && (v.length > v.indexOf(".") + 2)){
                    v = parseInt( Math.floor(v*100) ) / 100;
                    v = ( -1 === v.toString().search(/\./) ) ? v.toString() + '.00' : v.toString();
                    if ( v.substr(v.search(/\./)).length == 1 ) {
                        v += '00';
                    } else if ( v.substr(v.search(/\./)).length == 2 ) {
                        v += '0';
                    }
                    this.value = v;
                }else{
                    var _v = parseFloat($.trim(this.value));
                    if ( isNaN(_v) ) {
                        this.value = "";
                    }else{
                        var not = "";
                        if(v.indexOf(".") + 1 == v.length){
                            not = ".";
                        }
                        this.value = _v+not;
                    }
                }
            });
            if(blur){
                jQuery(amountField).blur(function () {
                    var v = parseFloat($.trim(this.value));
                    if ( isNaN(v) ) {
                        this.value = '0.00';
                    } else {
                        v = parseInt( Math.round(v*100) ) / 100;
                        v = ( -1 === v.toString().search(/\./) ) ? v.toString() + '.00' : v.toString();
                        if ( v.substr(v.search(/\./)).length == 1 ) {
                            v += '00';
                        } else if ( v.substr(v.search(/\./)).length == 2 ) {
                            v += '0';
                        }
                        this.value = v;
                    }
                });
            }
    }, watermark : function(){
        function watermark(event){
            var defaultText = jQuery(this).attr("title");
            if(this.value.length == 0 && event.type == "blur")
            {
                defaultText = defaultText.replace(/ /g,"")
                            .replace(/\./g,"_")
                            .replace(/\//g,"_")
                            .replace(/\(/g,"")
                            .replace(/:/g,"_")
                            .replace(/#/g,"x")
                            .replace(/-/g,"_")
                            .replace(/@/g,"_")
                            .replace(/\)/g,"").toLowerCase();
                jQuery(this).css("background-image","url(/widgets/images/watermark/"+defaultText+".png)");
            }
            if(event.type == "focus")
            {
                jQuery(this).css("background-image","");
            }
            if(event.type == "change")
            {
                jQuery(this).css("background-image","");
            }
        }
        jQuery(".inputwatermark").each(function(){
           if(jQuery(this).val() == ""){
                var title = jQuery(this).attr("title");
                title = title.replace(/ /g,"")
                            .replace(/\./g,"_")
                            .replace(/\//g,"_")
                            .replace(/-/g,"_")
                            .replace(/\(/g,"")
                            .replace(/:/g,"_")
                            .replace(/#/g,"x")
                            .replace(/@/g,"_")
                            .replace(/\)/g,"").toLowerCase();
                jQuery(this).css("background-image","url(/widgets/images/watermark/"+title+".png)");
           }
        });
        jQuery(".inputwatermark").unbind("focus",watermark).bind("focus",watermark);
        jQuery(".inputwatermark").unbind("blur",watermark).bind("blur",watermark);
        jQuery(".inputwatermark").unbind("change",watermark).bind("change",watermark);
    },
    ConfirmApproveWithFinalTotalTechByZero : function(afterConfirm,FinalTotalTech, checkedApprove){
        if(FinalTotalTech > 0 || !checkedApprove){
            if(typeof(afterConfirm) == 'function'){
                afterConfirm();
            }
            $.fancybox.close();
            return false;
        }
        var content = "<div style='width:400px;height:auto;'>\
                  <div style='padding-bottom:10px;'>This Work Order has $0 for tech pay. Please confirm.<br></div>\
                  <div><table border='0' width='100%'>\
                  <tr>\
                  <td style='width:50%;text-align:right;padding-right:3px;'>\
                  <input id='cmdApprovedConfirm' class='link_button middle2_button' type='button' value='Confirm'>\
                  </td>\
                  <td style='width:50%;text-align:left;padding-left:3px;'>\
                  <input id='cmdApprovedChange' class='link_button middle2_button' type='button' value='Change'>\
                    </td>\
                  </tr>\
                  </table>\
                </div>";
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :true,
            'hideOnOverlayClick' : false,
            'content': content
        }
        ).trigger('click');
        jQuery("#cmdApprovedChange").click(function(){
            $.fancybox.close();
        });   
        jQuery("#cmdApprovedConfirm").click(function(){
            if(typeof(afterConfirm) == 'function'){
                afterConfirm();
            }
            $.fancybox.close();
        });
    }
};

function attachFSRushInfoText (jqueryObj) {
	var btstr_fsrush="FS-RUSH&trade; (formerly Push2Tech&trade;) allows you to auto assign technicians based on custom restrictions, and is often used for fast-response calls. Choose parameters up-front and the system automatically assigns a tech that bids on the job based on your selections. <a href='/docs/FS-RUSH_Guide_2013-09-05.pdf' target='_blank'>Click here</a> to refer to the FieldSolutions FS-RUSH&trade; Guide.";
	jqueryObj.bt(btstr_fsrush,
	{
		trigger:'click',
		positions:'top',
		fill: 'white',
		cssStyles:{
			width: '300px',
			color:'black'
		}
	});
};

function attachWMInfoText(jqueryObj) {
	var btstr_pushwm="FieldSolutions has partnered with Work Market to provide our clients with a larger pool of technicians ready to accept RUSH work orders. This may yield faster technician response times and less waiting for you.<br/><br/>FieldSolutions' FS-RUSH&trade; Auto-Assignment Tool has been modified  to seamlessly run the Work Market system by assigning a work order to the first qualified technician that accepts.<br/><br/>If a work order is accepted by a Work Market technician, you continue to manage the work and are invoiced through the FieldSolutions system as you would normally. There are no added fees or changes in payment terms in the event a technician is dispatched through FieldSolutions on the Work Market system. The work order will be invoiced within that week's FieldSolutions invoice.<br/><br/><a href='/docs/FS-RUSH_FAQ_2013-09-12.pdf' target='_blank'>Click here</a> or contact your Account Manager, listed on the Client Profile tab of your Settings page, for more information.";
	jqueryObj.bt(btstr_pushwm,
	{
		trigger:'click',
		positions:'left',
		fill: 'white',
		cssStyles:{
			width: '300px',
			color:'black'
		}
	});
};
