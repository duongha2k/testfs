function FSWidget(params) {
    this.params = params || {};

    if ( this.params.baseUrl === undefined ) {
        this.params.baseUrl = document.location.protocol + '//' + document.location.hostname + '/widgets/';
    }

    if ( this.params.WIDGET === undefined )
        this.params.WIDGET = {};

    this.params.WIDGET.avaliableTabs = {};

    if ( this.params.POPUP === undefined )
        this.params.POPUP = {};
    
    this.params.POPUP.avaliablePopups = {};

    this.waitImage = new Image();
    this.waitImage.src = "/widgets/images/wait.gif";     //  pre load wait image

    this.currentTab = this.params.tab || 'workdone'; //  Default tab value is 'workdone' if in params not set

    this.tabOpenXhrRequest = null;  //  Xhr request for update WDD

    /* Sort tool */
    this._sortTool = null;
    this.sortTool = function ( sort ) {
        if ( sort !== undefined && sort !== null && sort instanceof FSWidgetSortTool ) {
            this._sortTool = sort;
        }
        return this._sortTool;
    }
    /*- Sort tool -*/
    /* View Deactivaton Reason tool */
    this._deactivationTool = null;
    this.deactivationTool = function ( deactivation ) {
        if ( deactivation instanceof FSWidgetDeactivationTool ) {
            this._deactivationTool = deactivation;
        }
        return this._deactivationTool;
    }
    /*- View Deactivaton Reason tool -*/

    if ( this.params.WOFILTERS === undefined )
        this.params.WOFILTERS = {};

    this.params.WOFILTERS._initialized_     = false;    // For init use initFilters() function
    this.params.WOFILTERS._full_search      = false;    //  This is not filter parameter, but Flag only
    this.params.WOFILTERS._company          = null;
    this.params.WOFILTERS._techId           = null;
    this.params.WOFILTERS._state            = null;
    this.params.WOFILTERS._country            = null;
    this.params.WOFILTERS._zip              = null;
    this.params.WOFILTERS._projects         = [];
    this.params.WOFILTERS._date_start       = null;
    this.params.WOFILTERS._date_end         = null;

    this.params.WOFILTERS._username            = null;
    this.params.WOFILTERS._headline            = null;
    this.params.WOFILTERS._wo_category         = null;
    this.params.WOFILTERS._date_entered_from   = null;
    this.params.WOFILTERS._date_entered_to     = null;
    this.params.WOFILTERS._date_approved_from  = null;
    this.params.WOFILTERS._date_approved_to    = null;
    this.params.WOFILTERS._date_invoiced_from  = null;
    this.params.WOFILTERS._date_invoiced_to    = null;
    this.params.WOFILTERS._date_paid_from      = null;
    this.params.WOFILTERS._date_paid_to        = null;
    this.params.WOFILTERS._pay_max_from        = null;
    this.params.WOFILTERS._pay_max_to          = null;
    this.params.WOFILTERS._net_pay_amount_from = null;
    this.params.WOFILTERS._net_pay_amount_to   = null;
    this.params.WOFILTERS._tb_pay   = null;

    this.params.WOFILTERS._tech             = null;
    this.params.WOFILTERS._contractor       = null;
    this.params.WOFILTERS._region           = null;
    this.params.WOFILTERS._call_type        = null;
    this.params.WOFILTERS._wm_tech_select   = null;
    this.params.WOFILTERS._wm_tech_id       = null;
    this.params.WOFILTERS._wm_assignment_id = null;
    this.params.WOFILTERS._win              = null;
    this.params.WOFILTERS._client_wo        = null;
    this.params.WOFILTERS._WO_ID_Mode        = null;
    this.params.WOFILTERS._po               = null;
    this.params.WOFILTERS._show_to_tech     = null;
    this.params.WOFILTERS._site_name        = null;
    this.params.WOFILTERS._site_number      = null;
    this.params.WOFILTERS._site_status      = null;
    this.params.WOFILTERS._city             = null;
    this.params.WOFILTERS._update_req       = null;
    this.params.WOFILTERS._store_notify     = null;
    this.params.WOFILTERS._checked_in       = null;
    this.params.WOFILTERS._checked_out       = null;
    this.params.WOFILTERS._reviewed_wo      = null;
    this.params.WOFILTERS._tech_confirmed   = null;
    this.params.WOFILTERS._tech_complete    = null;
    this.params.WOFILTERS._tech_called      = null;
    this.params.WOFILTERS._site_complete    = null;
    this.params.WOFILTERS._approved         = null;
    this.params.WOFILTERS._extra_pay        = null;
    this.params.WOFILTERS._out_of_scope     = null;
    this.params.WOFILTERS._tech_paid        = null;
    this.params.WOFILTERS._tech_fname       = null;
    this.params.WOFILTERS._tech_lname       = null;
    this.params.WOFILTERS._tech_email       = null;
    this.params.WOFILTERS._tech_email_contain= null;
    this.params.WOFILTERS._show_sourced     = null;
    this.params.WOFILTERS._show_not_sourced = null;
    this.params.WOFILTERS._paper_received   = null;
    this.params.WOFILTERS._lead             = null;
    this.params.WOFILTERS._assist           = null;
    this.params.WOFILTERS._deactivated      = null;
    this.params.WOFILTERS._short_notice     = null;
    this.params.WOFILTERS._fls_id           = null;
    this.params.WOFILTERS._distance         = null;
    this.params.WOFILTERS._pricing_ran      = null;
    this.params.WOFILTERS._invoiced         = null;
    this.params.WOFILTERS._pcnt_deduct       = null;
    this.params.WOFILTERS._deactivated_reason  = null;
    this.params.WOFILTERS._deactivation_code  = null;
    this.params.WOFILTERS._satrecommended_from  = null;
    this.params.WOFILTERS._satperformance_from  = null;
    //13892
    this.params.WOFILTERS._StandardOffset         = null;
    //622
    this.params.WOFILTERS._loaddasboard  = null;
    this.params.WOFILTERS._AbortFee  = null;
    
    this.params.WOFILTERS._password  = null;
    this.params.WOFILTERS._admin  = null;
    this.params.WOFILTERS._accept_terms  = null;
    this.params.WOFILTERS._account_enabled  = null;
    this.params.WOFILTERS._show_on_reports  = null;
    this.params.WOFILTERS._project_manager  = null;
    this.params.WOFILTERS._website_url  = null;
    this.params.WOFILTERS._contact_phone1  = null;
    this.params.WOFILTERS._contact_phone2  = null;
    this.params.WOFILTERS._email1  = null;
    this.params.WOFILTERS._email2  = null;
    this.params.WOFILTERS._contact_name  = null;
    this.params.WOFILTERS._company_name  = null;
    this.params.WOFILTERS._client_id  = null;
    this.params.WOFILTERS._user_type  = null;
    this.params.WOFILTERS._company_id  = null;
    
    this.params.WOFILTERS._container        = null;
    this.params.WOFILTERS._devry        = null;
    this.params.WOFILTERS._Audit        = null;
    this.params.WOFILTERS._AuditNeeded    = null;
    this.params.WOFILTERS._AuditComplete   = null;   
    this.params.WOFILTERS._Owner    = null;
    this.params.WOFILTERS._dateRange = "";
    this.params.WOFILTERS._route        = "";
    this.params.WOFILTERS._amount_per        = "";

    this.params.WOFILTERS._siteinfo1 = null;
    this.params.WOFILTERS._siteinfo2 = null;
    this.params.WOFILTERS._displaycontractors = null;
    this.params.WOFILTERS._contractor_id = null;
    this.params.WOFILTERS._lastname = null;

    this.params.WOFILTERS._Client_Status        = "";
    this.params.WOFILTERS._Client_Relationship  = "";
    this.params.WOFILTERS._Segment    = "";
    this.params.WOFILTERS._Original_Seller   = "";
    this.params.WOFILTERS._Executive    = "";
    this.params.WOFILTERS._CSD     = "";
    this.params.WOFILTERS._AM     = "";
    this.params.WOFILTERS._Entity        = "";
    this.params.WOFILTERS._Service_Type        = "";
    this.params.WOFILTERS._System_Status        = "";
    this.params.WOFILTERS._Pay_ISO       = "";
    this.params.WOFILTERS._ISO_Company_Name        = "";
    this.params.WOFILTERS._ISO_ID        = "";
    //732
    this.params.WOFILTERS._SiteContactBackedOutChecked = "";
    //end 732
    this.params.WOFILTERS._techPaperwork        = "";
     
    this.params.WOFILTERS.set = function( field, value )
    {
        switch ( field ) {
            case '_short_notice'        : return this.shortNotice(value);
            case '_tech_id'             : return this.techId(value);
            case '_fls_id'              : return this.flsId(value);
            case '_full_search'         : return this.fullSearch(value);
            case '_state'               : return this.state(value);
            case '_country'               : return this.country(value);
            case '_zip'                 : return this.zip(value);
            case '_projects'            : return this.setProject(value);
            case '_date_start'          : return this.dateStart(value);
            case '_date_end'            : return this.dateEnd(value);
            
            case '_username'            : return this.username(value);
            case '_headline'            : return this.headline(value);
            case '_wo_category'         : return this.woCategory(value);
            case '_date_entered_from'   : return this.dateEnteredFrom(value);
            case '_date_entered_to'     : return this.dateEnteredTo(value);
            case '_date_approved_from'  : return this.dateApprovedFrom(value);
            case '_date_approved_to'    : return this.dateApprovedTo(value);
            case '_date_invoiced_from'  : return this.dateInvoicedFrom(value);
            case '_date_invoiced_to'    : return this.dateInvoicedTo(value);
            case '_date_paid_from'      : return this.datePaidFrom(value);
            case '_date_paid_to'        : return this.datePaidTo(value);
            case '_pay_max_from'        : return this.payMaxFrom(value);
            case '_pay_max_to'          : return this.payMaxTo(value);
            case '_net_pay_amount_from' : return this.netPayAmountFrom(value);
            case '_net_pay_amount_to'   : return this.netPayAmountTo(value);
            case '_tb_pay'              : return this.tbPay(value);

            case '_tech'                : return this.tech(value);
            case '_contractor'          : return this.contractor(value);
            case '_region'              : return this.region(value);
            case '_call_type'           : return this.callType(value);
            case '_wm_tech_select'           : return this.wmTechSelect(value);
            case '_wm_tech_id'           : return this.wmTechId(value);
            case '_wm_assignment_id'           : return this.wmAssignmentId(value);
            case '_win'                 : return this.win(value);
            case '_client_wo'           : return this.clientWO(value);
            case '_WO_ID_Mode'           : return this.WO_ID_Mode(value);
            case '_po'                  : return this.po(value);
            case '_show_to_tech'        : return this.showToTech(value);
            case '_site_name'           : return this.siteName(value);
            case '_site_number'         : return this.siteNumber(value);
            case '_site_status'         : return this.siteStatus(value);
            case '_StandardOffset'      : return this.timeZone(value);//13892
            case '_city'                : return this.city(value);
            case '_update_req'          : return this.updateRequested(value);
            case '_store_notify'        : return this.storeNotified(value);
            case '_checked_in'          : return this.checkedIn(value);
            case '_checked_out'          : return this.checkedOut(value);
            case '_reviewed_wo'         : return this.reviewedWO(value);
            case '_tech_confirmed'      : return this.techConfirmed(value);
            case '_tech_complete'       : return this.techComplete(value);
            case '_tech_called'			: return this.techCalled(value);
            case '_site_complete'       : return this.siteComplete(value);
            case '_approved'            : return this.approved(value);
            case '_extra_pay'           : return this.extraPay(value);
            case '_out_of_scope'        : return this.outOfScope(value);
            case '_tech_paid'           : return this.techPaid(value);
            case '_tech_fname'          : return this.techFName(value);
            case '_tech_lname'          : return this.techLName(value);
            case '_tech_email'          : return this.techEmail(value);
            case '_tech_email_contain'  : return this.techEmailContain(value);
            case '_show_sourced'        : return this.showSourced(value);
            case '_show_not_sourced'    : return this.showNotSourced(value);
            case '_paper_received'      : return this.paperReceived(value);
            case '_lead'                : return this.lead(value);
            case '_assist'              : return this.assist(value);
            case '_deactivated'         : return this.deactivated(value);
            case '_distance'            : return this.distance(value);
            
            case '_pricing_ran'			: return this.pricingRan(value);
            case '_invoiced'			: return this.invoiced(value);
            case '_pcnt_deduct'			: return this.pcntDeduct(value);
            case '_deactivated_reason'  : return this.deactivatedReason(value);
            case '_deactivation_code'    : return this.deactivationCode(value);
            case '_satrecommended_from'	: return this.satrecommendedFrom(value);
            case '_satperformance_from'	: return this.satperformanceFrom(value);
            //622
            case '_loaddasboard'	: return this.loaddasboard(value);
            case '_AbortFee'            : return this.AbortFee(value);
            //end622

            case '_password'            : return this.Password(value);
            case '_admin'               : return this.Admin(value);
            case '_accept_terms'        : return this.AcceptTerms(value);
            case '_account_enabled'     : return this.AccountEnabled(value);
            case '_show_on_reports'     : return this.ShowOnReports(value);
            case '_project_manager'     : return this.ProjectManager(value);
            case '_website_url'         : return this.WebsiteURL(value);
            case '_contact_phone1'      : return this.ContactPhone1(value);
            case '_contact_phone2'      : return this.ContactPhone2(value);
            case '_email1'              : return this.Email1(value);
            case '_email2'              : return this.Email2(value);
            case '_contact_name'        : return this.ContactName(value);
            case '_company_name'        : return this.CompanyName(value);
            case '_client_id'           : return this.ClientID(value);
            case '_user_type'           : return this.UserType(value);
            case '_company_id'           : return this.Company_ID(value);
            case '_devry'           : return this.Devry(value);     
            case '_Audit'           : return this.Audit(value);  
            case '_AuditComplete'           : return this.AuditComplete(value);  
            case '_AuditNeeded'           : return this.AuditNeeded(value);  
            case '_Owner'           : return this.Owner(value);  
            case '_dateRange'           : return this.dateRange(value);
            case '_route'           : return this.Route(value);  
            case '_Amount_Per'           : return this.Amount_Per(value);    

            case '_siteinfo1' : return this.siteInfo1(value);
            case '_siteinfo2' : return this.siteInfo2(value);
            case '_displaycontractors' : return this.displayContractors(value);
            case '_contractor_id' : return this.contractorId(value);
            case '_lastname' : return this.lastName(value);
                
            case '_Client_Status'           : return this.Client_Status(value);
            case '_Client_Relationship'           : return this.Client_Relationship(value);
            case '_Segment'           : return this.Segment(value);
            case '_Original_Seller'           : return this.Original_Seller(value);
            case '_Executive'           : return this.Executive(value);
            case '_CSD'           : return this.CSD(value);
            case '_AM'           : return this.AM(value);
            case '_Entity'           : return this.Entity(value);
            case '_Service_Type'           : return this.Service_Type(value);
            case '_System_Status'           : return this.System_Status(value); 
                
            case '_Pay_ISO'           : return this.Pay_ISO(value);
            case '_ISO_Company_Name'           : return this.ISO_Company_Name(value);
            case '_ISO_ID'           : return this.ISO_ID(value);   
            case '_techPaperwork'           : return this.techPaperwork(value); 
            //732
            case '_SiteContactBackedOutChecked'           : return this.techPaperwork(value); 
            //end 732
                
        }
        return null;
    }
    //13892
     this.params.WOFILTERS.timeZone = function ( str ){
        if ( str !== undefined ) this._StandardOffset = str;
        return this._StandardOffset;
    }
    // End 13892
    this.params.WOFILTERS.siteStatus = function ( str ){
        if ( str !== undefined ) this._site_status = str;
        return this._site_status;
    }
    
    this.params.WOFILTERS.techPaperwork = function ( str ){
        if ( str !== undefined ) this._techPaperwork = str;
        return this._techPaperwork;
    }
    
    this.params.WOFILTERS.ISO_ID = function ( str ){
        if ( str !== undefined ) this._ISO_ID = str;
        return this._ISO_ID;
    }
    
    this.params.WOFILTERS.ISO_Company_Name = function ( str ){
        if ( str !== undefined ) this._ISO_Company_Name = str;
        return this._ISO_Company_Name;
    }
    
    this.params.WOFILTERS.Pay_ISO = function ( str ){
        if ( str !== undefined ) this._Pay_ISO = str;
        return this._Pay_ISO;
    }
    
    this.params.WOFILTERS.System_Status = function ( str ){
        if ( str !== undefined ) this._System_Status = str;
        return this._System_Status;
    }
    
    
    this.params.WOFILTERS.Service_Type = function ( str ){
        if ( str !== undefined ) this._Service_Type = str;
        return this._Service_Type;
    }
    
    this.params.WOFILTERS.Entity = function ( str ){
        if ( str !== undefined ) this._Entity = str;
        return this._Entity;
    }
    
    
    this.params.WOFILTERS.AM = function ( str ){
        if ( str !== undefined ) this._AM = str;
        return this._AM;
    }
    
    this.params.WOFILTERS.CSD = function ( str ){
        if ( str !== undefined ) this._CSD = str;
        return this._CSD;
    }
    
    
    this.params.WOFILTERS.Executive = function ( str ){
        if ( str !== undefined ) this._Executive = str;
        return this._Executive;
    }
    
    this.params.WOFILTERS.Original_Seller = function ( str ){
        if ( str !== undefined ) this._Original_Seller = str;
        return this._Original_Seller;
    }
    
    this.params.WOFILTERS.Segment = function ( str ){
        if ( str !== undefined ) this._Segment = str;
        return this._Segment;
    }
    
    this.params.WOFILTERS.Client_Relationship = function ( str ){
        if ( str !== undefined ) this._Client_Relationship = str;
        return this._Client_Relationship;
    }
    this.params.WOFILTERS.Client_Status = function ( str ){
        if ( str !== undefined ) this._Client_Status = str;
        return this._Client_Status;
    }
    
    this.params.WOFILTERS.Route = function ( str ){
	    if ( str !== undefined ) this._route = str;
	    return this._route;
	}
    this.params.WOFILTERS.Amount_Per = function ( str ){
	    if ( str !== undefined ) this._amount_per = str;
	    return this._amount_per;
	}    
    this.params.WOFILTERS.Devry = function ( str ){
	    if ( str !== undefined ) this._devry = str;
	    return this._devry;
	}
    this.params.WOFILTERS.Owner = function ( str ){
	    if ( str !== undefined ) this._Owner = str;
	    return this._Owner;
	}
    this.params.WOFILTERS.dateRange = function ( str ){
	    if ( str !== undefined ) this._dateRange = str;
	    return this._dateRange;
	}
    this.params.WOFILTERS.AuditNeeded = function ( str ){
	    if ( str !== undefined ) this._AuditNeeded = str;
	    return this._AuditNeeded;
	}
    this.params.WOFILTERS.AuditComplete = function ( str ){
	    if ( str !== undefined ) this._AuditComplete = str;
	    return this._AuditComplete;
	}    
    this.params.WOFILTERS.Audit = function ( str ){
	    if ( str !== undefined ) this._Audit = str;
	    return this._Audit;
	}    
    this.params.WOFILTERS.Password = function ( str ){
	    if ( str !== undefined ) this._password = str;
	    return this._password;
	}
    this.params.WOFILTERS.Admin = function ( str ){
	    if ( str !== undefined ) this._admin = str;
	    return this._admin;
	}
    this.params.WOFILTERS.AcceptTerms = function ( str ){
	    if ( str !== undefined ) this._accept_terms = str;
	    return this._accept_terms;
	}
    this.params.WOFILTERS.AccountEnabled = function ( str ){
	    if ( str !== undefined ) this._account_enabled = str;
	    return this._account_enabled;
	}
    this.params.WOFILTERS.ShowOnReports = function ( str ){
	    if ( str !== undefined ) this._show_on_reports = str;
	    return this._show_on_reports;
	}
    this.params.WOFILTERS.ProjectManager = function ( str ){
	    if ( str !== undefined ) this._project_manager = str;
	    return this._project_manager;
	}
    this.params.WOFILTERS.WebsiteURL = function ( str ){
	    if ( str !== undefined ) this._website_url = str;
	    return this._website_url;
	}
    this.params.WOFILTERS.ContactPhone1 = function ( str ){
	    if ( str !== undefined ) this._contact_phone1 = str;
	    return this._contact_phone1;
	}
    this.params.WOFILTERS.ContactPhone2 = function ( str ){
	    if ( str !== undefined ) this._contact_phone2 = str;
	    return this._contact_phone2;
	}
    this.params.WOFILTERS.Email1 = function ( str ){
	    if ( str !== undefined ) this._email1 = str;
	    return this._email1;
	}
    this.params.WOFILTERS.Email2 = function ( str ){
	    if ( str !== undefined ) this._email2 = str;
	    return this._email2;
	}
    this.params.WOFILTERS.ContactName = function ( str ){
	    if ( str !== undefined ) this._contact_name = str;
	    return this._contact_name;
	}
    this.params.WOFILTERS.CompanyName = function ( str ){
	    if ( str !== undefined ) this._company_name = str;
	    return this._company_name;
	}
    this.params.WOFILTERS.ClientID = function ( str ){
	    if ( str !== undefined ) this._client_id = str;
	    return this._client_id;
	}
    this.params.WOFILTERS.UserType = function ( str ){
	    if ( str !== undefined ) this._user_type = str;
	    return this._user_type;
	}
    this.params.WOFILTERS.Company_ID = function ( str ){
	    if ( str !== undefined ) this._company_id = str;
	    return this._company_id;
	}
    

	this.params.WOFILTERS.showToTech = function ( str )
	{
	    if ( str !== undefined ) {
	        this._show_to_tech = str;
	    }
	    return this._show_to_tech;
	}
    this.params.WOFILTERS.satrecommendedFrom = function ( str )
    {
        if ( str !== undefined ) {
            this._satrecommended_from = str;
        }
        return this._satrecommended_from;
    }
    //622
    this.params.WOFILTERS.loaddasboard = function ( str )
    {
        if ( str !== undefined ) {
            this._loaddasboard = str;
        }
        return this._loaddasboard;
    }
    this.params.WOFILTERS.AbortFee = function ( str )
    {
        if ( str !== undefined ) {
            this._AbortFee = str;
        }
        return this._AbortFee;
    }
    
    //end 622
    this.params.WOFILTERS.satperformanceFrom = function ( str )
    {
        if ( str !== undefined ) {
            this._satperformance_from = str;
        }
        return this._satperformance_from;
    }
    this.params.WOFILTERS.deactivatedReason = function ( str )
    {
        if ( str !== undefined ) {
            this._deactivated_reason = str;
        }
        return this._deactivated_reason;
    }
    this.params.WOFILTERS.deactivationCode = function ( str )
    {
        if ( str !== undefined ) {
            this._deactivation_code = str;
        }
        return this._deactivation_code;
    }
    this.params.WOFILTERS.pcntDeduct = function ( str )
    {
        if ( str !== undefined ) {
            this._pcnt_deduct = str;
        }
        return this._pcnt_deduct;
    }
    this.params.WOFILTERS.techCalled = function ( str )
    {
        if ( str !== undefined ) {
            this._tech_called = str;
        }
        return this._tech_called;
    }
    this.params.WOFILTERS.pricingRan = function ( str )
    {
        if ( str !== undefined ) {
            this._pricing_ran = str;
        }
        return this._pricing_ran;
    }
    this.params.WOFILTERS.invoiced = function ( str )
    {
        if ( str !== undefined ) {
            this._invoiced = str;
        }
        return this._invoiced;
    }
    this.params.WOFILTERS.deactivated = function ( str )
    {
        if ( str !== undefined ) {
            this._deactivated = str;
        }
        return this._deactivated;
    }
    this.params.WOFILTERS.assist = function ( str )
    {
        if ( str !== undefined ) {
            this._assist = str;
        }
        return this._assist;
    }
    this.params.WOFILTERS.lead = function ( str )
    {
        if ( str !== undefined ) {
            this._lead = str;
        }
        return this._lead;
    }
    this.params.WOFILTERS.paperReceived = function ( str )
    {
        if ( str !== undefined ) {
            this._paper_received = str;
        }
        return this._paper_received;
    }
    this.params.WOFILTERS.showNotSourced = function ( str )
    {
        if ( str !== undefined ) {
            this._show_not_sourced = str;
        }
        return this._show_not_sourced;
    }
    this.params.WOFILTERS.showSourced = function ( str )
    {
        if ( str !== undefined ) {
            this._show_sourced = str;
        }
        return this._show_sourced;
    }
    this.params.WOFILTERS.techLName = function ( str )
    {
        if ( str !== undefined ) {
            this._tech_lname = str;
        }
        return this._tech_lname;
    }
    this.params.WOFILTERS.techEmail = function ( str )
    {
        if ( str !== undefined ) {
            this._tech_email = str;
        }
        return this._tech_email;
    }
    this.params.WOFILTERS.techEmailContain = function ( str )
    {
        if ( str !== undefined ) {
            this._tech_email_contain = str;
        }
        return this._tech_email_contain;
    }
    this.params.WOFILTERS.techFName = function ( str )
    {
        if ( str !== undefined ) {
            this._tech_fname = str;
        }
        return this._tech_fname;
    }
    this.params.WOFILTERS.techPaid = function ( str )
    {
        if ( str !== undefined ) {
            this._tech_paid = str;
        }
        return this._tech_paid;
    }
    this.params.WOFILTERS.outOfScope = function ( str )
    {
        if ( str !== undefined ) {
            this._out_of_scope = str;
        }
        return this._out_of_scope;
    }
    this.params.WOFILTERS.extraPay = function ( str )
    {
        if ( str !== undefined ) {
            this._extra_pay = str;
        }
        return this._extra_pay;
    }
    this.params.WOFILTERS.approved = function ( str )
    {
        if ( str !== undefined ) {
            this._approved = str;
        }
        return this._approved;
    }
    this.params.WOFILTERS.siteComplete = function ( str )
    {
        if ( str !== undefined ) {
            this._site_complete = str;
        }
        return this._site_complete;
    }
    this.params.WOFILTERS.techConfirmed = function ( str )
    {
        if ( str !== undefined ) {
            this._tech_confirmed = str;
        }
        return this._tech_confirmed;
    }
    this.params.WOFILTERS.techComplete = function ( str )
    {
        if ( str !== undefined ) {
            this._tech_complete = str;
        }
        return this._tech_complete;
    }
    this.params.WOFILTERS.reviewedWO = function ( str )
    {
        if ( str !== undefined ) {
            this._reviewed_wo = str;
        }
        return this._reviewed_wo;
    }
    this.params.WOFILTERS.checkedIn = function ( str )
    {
        if ( str !== undefined ) {
            this._checked_in = str;
        }
        return this._checked_in;
    }
    this.params.WOFILTERS.checkedOut = function ( str )
    {
        if ( str !== undefined ) {
            this._checked_out = str;
        }
        return this._checked_out;
    }
    this.params.WOFILTERS.storeNotified = function ( str )
    {
        if ( str !== undefined ) {
            this._store_notify = str;
        }
        return this._store_notify;
    }
    this.params.WOFILTERS.updateRequested   = function ( str )
    {
        if ( str !== undefined ) {
            this._update_req = str;
        }
        return this._update_req;
    }
    this.params.WOFILTERS.city  = function ( str )
    {
        if ( str !== undefined ) {
            this._city = str;
        }
        return this._city;
    }
    this.params.WOFILTERS.siteNumber    = function ( str )
    {
        if ( str !== undefined ) {
            this._site_number = str;
        }
        return this._site_number;
    }
    this.params.WOFILTERS.siteName    = function ( str )
    {
        if ( str !== undefined ) {
            this._site_name = str;
        }
        return this._site_name;
    }
    this.params.WOFILTERS.po    = function ( str )
    {
        if ( str !== undefined ) {
            this._po = str;
        }
        return this._po;
    }
    this.params.WOFILTERS.clientWO = function ( wo )
    {
        if ( wo !== undefined ) {
            this._client_wo = wo;
        }
        return this._client_wo;
    }
    this.params.WOFILTERS.WO_ID_Mode = function ( wo )
    {
        if ( wo !== undefined ) {
            this._WO_ID_Mode = wo;
        }
        return this._WO_ID_Mode;
    }
    this.params.WOFILTERS.win = function( num )
    {
        if ( num !== undefined ) {
            this._win = num;
        }
        return this._win;
    }
    this.params.WOFILTERS.shortNotice = function ( short_notice )
    {
        if ( short_notice !== undefined ) {
            this._short_notice = short_notice;
        }
        return this._short_notice;
    }
    this.params.WOFILTERS.flsId = function ( fls_id )
    {
        if ( fls_id !== undefined )
        {
            this._fls_id = fls_id;
        }
        return this._fls_id;
    }
    
    //732
    
    this.params.WOFILTERS.SiteContactBackedOutChecked = function ( SiteContactBackedOutChecked )
    {
        if ( SiteContactBackedOutChecked !== undefined )
        {
            this._SiteContactBackedOutChecked = SiteContactBackedOutChecked;
        }
        return this._SiteContactBackedOutChecked;
    }
    //end 732
    
    this.params.WOFILTERS.fullSearch = function( full_seach )
    {
        if ( full_seach !== undefined ) {
            if ( typeof full_seach === 'string' ) {
                full_seach = full_seach.toLowerCase().replace(/^\s+/, '').replace(/\s+$/, '');
                if ( full_seach === 'true' || full_seach === '1' )
                    this._full_search = true;
                else
                    this._full_search = false;
            } else if ( typeof full_seach === 'number' ) {
                this._full_search = ( full_seach ) ? true : false;
            } else if ( full_seach ) {
                this._full_search = true;
            } else {
                this._full_search = false;
            }
            this._full_search = full_seach;
        }
        return this._full_search;
    }

    /**
     *  _makeContainer
     */
    this.params.WOFILTERS._makeContainer = function () {
        var id = '_filters_' + Math.floor(Math.random()*1000001);
        var container   = document.createElement('div');

        container.id = id;
        container.style.display = 'none';
        $('body')[0].appendChild(container);

        this._container = container;
    }
    /**
     *  @return DOM object
     */
    this.params.WOFILTERS.container = function () {
        return this._container;
    }
    /**
     *  @return DOM object
     */
    this.params.WOFILTERS.amount_per = function () {
        return this._amount_per;
    }
    /**
     *  isInitialized
     */
    this.params.WOFILTERS.isInitialized = function ()
    {
        return this._initialized_;
    }
    /**
     *  setInitialized
     */
    this.params.WOFILTERS.setInitialized = function () {
        this._initialized_ = true;
    }
    /**
     *  company
     *
     *  get CompanyId Filter   Only for read!!!
     *
     *  @return string|null
     *  WARNING!!! function depend on PMCompany select
     */
    this.params.WOFILTERS.company = function () {
        if ( typeof window._company === 'string') {
            this._company = window._company;
        }
        return this._company;
    }
    /**
     *  techId
     *
     *  get techId Filter   Only for read!!!
     *
     *  @return string|null
     *  WARNING!!! function depend on PMCompany select
     */
    this.params.WOFILTERS.techId = function () {
        if ( !this._techId && typeof window._techId === 'string' ) {
            this._techId = window._techId;
        }
        return this._techId;
    }
    /**
     *  state
     *
     *  get/set Project State Filter
     *
     *  @param string|null state
     *  @return string
     */
    this.params.WOFILTERS.state = function ( state ) {
        if ( state !== undefined && typeof state === 'string' ) {
            this._state = state;
        }
        return this._state;
    }
    /**
     *  country
     *
     *  get/set Project Country Filter
     *
     *  @param string|null country
     *  @return string
     */
    this.params.WOFILTERS.country = function ( country ) {
        if ( country !== undefined && typeof country === 'string' ) {
            this._country = country;
        }
        return this._country;
    }
    /**
     *  zip
     *
     *  get/set Project ZIP Filter
     *
     *  @param string|null zip
     *  @return string
     */
    this.params.WOFILTERS.zip = function ( zip ) {
        if ( zip !== undefined ) {
            this._zip = zip;
        }
        return this._zip;
    }
    /**
     *  distance
     *
     *  get/set Distance Filter
     *
     *  @param string|null zip
     *  @return string
     */
    this.params.WOFILTERS.distance = function ( distance ) {
        if ( distance !== undefined ) {
            this._distance = distance;
        }
        return this._distance;
    }
    /**
     *  addProject
     *
     *  add Project Filter
     *
     *  @param string|array project
     *  @return array
     */
    this.params.WOFILTERS.addProject = function ( project ) {
        if ( typeof project === 'number' || typeof project === 'string' ) {
            if ( !jQuery.inArray(project, this._projects) ) {
                this._projects.push(project);
            }
        } else if ( project instanceof Array ) {
            var self = this;
            jQuery.each(project, function (idx, val) { self.addProject(val); });
        }
        return this._projects;
    }
    /**
     *  setProject
     *
     *  set Projects Filter but clear before
     *
     *  @param string|array project
     *  @return array
     */
    this.params.WOFILTERS.setProject = function ( project ) {
        if ( typeof project === 'number' || typeof project === 'string' ) {
            this._projects = [project];
        } else if ( project instanceof Array ) {
            this._projects = project;
        }
        return this._projects;
    }
    /**
     *  remProject
     *
     *  rem Projects' Filter item
     *
     *  @param string|array project
     *  @return array
     */
    this.params.WOFILTERS.remProject = function ( project ) {
        if ( typeof project === 'number' || typeof project === 'string' ) {
            this._projects = jQuery.grep(this._projects, function (e) { return e != project; })
        } else if ( project instanceof Array ) {
            for ( var i=0; i < project.length; ++i ) {
                this._projects = this.remProject( project[i] );
            }
        }
        return this._projects;
    }
    this.params.WOFILTERS.getProjects = function () {
        return this._projects;
    }
    /**
     *  dateStart
     *
     *  get/set DateStart Filter
     *
     *  @param string|null date_start
     *  @return string
     */
    this.params.WOFILTERS.dateStart = function ( date_start ) {
        if ( date_start !== undefined && typeof date_start === 'string' )
            this._date_start = date_start;
        return this._date_start;
    }
    /**
     *  dateEnd
     *
     *  get/set DateEnd Filter
     *
     *  @param string|null date_end
     *  #return string
     */
    this.params.WOFILTERS.dateEnd = function ( date_end ) {
        if ( date_end !== undefined && typeof date_end === 'string' )
            this._date_end = date_end;
        return this._date_end;
    }
    /**
     *  net_pay_amount_to
     *
     *  get/set netPayAmountTo Filter
     *
     *  @param string|null net_pay_amount_to
     *  #return string
     */
    this.params.WOFILTERS.netPayAmountTo = function ( net_pay_amount_to ) {
        if ( net_pay_amount_to !== undefined && typeof net_pay_amount_to === 'string' )
            this._net_pay_amount_to = net_pay_amount_to;
        return this._net_pay_amount_to;
    }
    /**
     *  tb_pay
     *
     *  get/set tbPay Filter
     *
     *  @param string|null tb_pay
     *  #return string
     */
    this.params.WOFILTERS.tbPay = function ( tb_pay ) {
        if ( tb_pay !== undefined && typeof tb_pay === 'string' )
            this._tb_pay = tb_pay;
        return this._tb_pay
    }
    /**
     *  net_pay_amount_from
     *
     *  get/set NetPayAmountFrom Filter
     *
     *  @param string|null net_pay_amount_from
     *  #return string
     */
    this.params.WOFILTERS.netPayAmountFrom = function ( net_pay_amount_from ) {
        if ( net_pay_amount_from !== undefined && typeof net_pay_amount_from === 'string' )
            this._net_pay_amount_from = net_pay_amount_from;
        return this._net_pay_amount_from;
    }
    /**
     *  pay_max_to
     *
     *  get/set PayMaxTo Filter
     *
     *  @param string|null pay_max_to
     *  #return string
     */
    this.params.WOFILTERS.payMaxTo = function ( pay_max_to ) {
        if ( pay_max_to !== undefined && typeof pay_max_to === 'string' )
            this._pay_max_to = pay_max_to;
        return this._pay_max_to;
    }
    /**
     *  pay_max_from
     *
     *  get/set pay_max_from Filter
     *
     *  @param string|null pay_max_from
     *  #return string
     */
    this.params.WOFILTERS.payMaxFrom = function ( pay_max_from ) {
        if ( pay_max_from !== undefined && typeof pay_max_from === 'string' )
            this._pay_max_from = pay_max_from;
        return this._pay_max_from;
    }
    /**
     *  date_paid_to
     *
     *  get/set date_paid_to Filter
     *
     *  @param string|null date_paid_to
     *  #return string
     */
    this.params.WOFILTERS.datePaidTo = function ( date_paid_to ) {
        if ( date_paid_to !== undefined && typeof date_paid_to === 'string' )
            this._date_paid_to = date_paid_to;
        return this._date_paid_to;
    }
    /**
     *  date_paid_from
     *
     *  get/set date_paid_from Filter
     *
     *  @param string|null date_paid_from
     *  #return string
     */
    this.params.WOFILTERS.datePaidFrom = function ( date_paid_from ) {
        if ( date_paid_from !== undefined && typeof date_paid_from === 'string' )
            this._date_paid_from = date_paid_from;
        return this._date_paid_from;
    }
    /**
     *  date_invoiced_to
     *
     *  get/set date_invoiced_to Filter
     *
     *  @param string|null date_invoiced_to
     *  #return string
     */
    this.params.WOFILTERS.dateInvoicedTo = function ( date_invoiced_to ) {
        if ( date_invoiced_to !== undefined && typeof date_invoiced_to === 'string' )
            this._date_invoiced_to = date_invoiced_to;
        return this._date_invoiced_to;
    }
    /**
     *  date_invoiced_from
     *
     *  get/set date_invoiced_from Filter
     *
     *  @param string|null date_invoiced_from
     *  #return string
     */
    this.params.WOFILTERS.dateInvoicedFrom = function ( date_invoiced_from ) {
        if ( date_invoiced_from !== undefined && typeof date_invoiced_from === 'string' )
            this._date_invoiced_from = date_invoiced_from;
        return this._date_invoiced_from;
    }
    /**
     *  date_approved_to
     *
     *  get/set date_approved_to Filter
     *
     *  @param string|null date_approved_to
     *  #return string
     */
    this.params.WOFILTERS.dateApprovedTo = function ( date_approved_to ) {
        if ( date_approved_to !== undefined && typeof date_approved_to === 'string' )
            this._date_approved_to = date_approved_to;
        return this._date_approved_to;
    }
    /**
     *  date_approved_from
     *
     *  get/set date_approved_from Filter
     *
     *  @param string|null date_approved_from
     *  #return string
     */
    this.params.WOFILTERS.dateApprovedFrom = function ( date_approved_from ) {
        if ( date_approved_from !== undefined && typeof date_approved_from === 'string' )
            this._date_approved_from = date_approved_from;
        return this._date_approved_from;
    }
    /**
     *  date_entered_to
     *
     *  get/set date_entered_to Filter
     *
     *  @param string|null date_entered_to
     *  #return string
     */
    this.params.WOFILTERS.dateEnteredTo = function ( date_entered_to ) {
        if ( date_entered_to !== undefined && typeof date_entered_to === 'string' )
            this._date_entered_to = date_entered_to;
        return this._date_entered_to;
    }
    /**
     *  date_entered_from
     *
     *  get/set date_entered_from Filter
     *
     *  @param string|null date_entered_from
     *  #return string
     */
    this.params.WOFILTERS.dateEnteredFrom = function ( date_entered_from ) {
        if ( date_entered_from !== undefined && typeof date_entered_from === 'string' )
            this._date_entered_from = date_entered_from;
        return this._date_entered_from;
    }
    /**
     *  wo_category
     *
     *  get/set wo_category Filter
     *
     *  @param string|null wo_category
     *  #return string
     */
    this.params.WOFILTERS.woCategory = function ( wo_category ) {
        if ( wo_category !== undefined && typeof wo_category === 'string' )
            this._wo_category = wo_category;
        return this._wo_category;
    }
    /**
     *  username
     *
     *  get/set username Filter
     *
     *  @param string|null username
     *  #return string
     */
    this.params.WOFILTERS.username = function ( username ) {
        if ( username !== undefined && typeof username === 'string' )
            this._username = username;
        return this._username;
    }
    /**
     *  headline
     *
     *  get/set headline Filter
     *
     *  @param string|null headline
     *  #return string
     */
    this.params.WOFILTERS.headline = function ( headline ) {
        if ( headline !== undefined && typeof headline === 'string' )
            this._headline = headline;
        return this._headline;
    }
    /**
     *  tech
     *
     *  @return string
     */
    this.params.WOFILTERS.tech = function ( tech ) {
        if ( typeof tech === 'string' )
            this._tech = tech;
        return this._tech;
    }
    /**
     *  tech
     *
     *  @return string
     */
    this.params.WOFILTERS.contractor = function ( contractor ) {
        if ( typeof contractor === 'string' )
            this._contractor = contractor;
        return this._contractor;
    }
    /**
     *  region
     *
     *  @return string
     */
    this.params.WOFILTERS.region = function ( region ) {
        if ( typeof region === 'string' )
            this._region = region;
        return this._region;
    }
    /**
     *  callType
     *
     *  @return string
     */
    this.params.WOFILTERS.callType = function ( call ) {
        if ( typeof call === 'string' )
            this._call_type = call;
        return this._call_type;
    }
    /**
     *  wm_tech_select
     *
     *  @return string
     */
    this.params.WOFILTERS.wmTechSelect = function ( info ) {
        if ( typeof info === 'string' )
            this._wm_tech_select = info;
        return this._wm_tech_select;
    }
    /**
     *  wm_tech_id
     *
     *  @return string
     */
    this.params.WOFILTERS.wmTechId = function ( info ) {
        if ( typeof info === 'string' )
            this._wm_tech_id = info;
        return this._wm_tech_id;
    }
    /**
     *  wm_assignment_id
     *
     *  @return string
     */
    this.params.WOFILTERS.wmAssignmentId = function ( info ) {
        if ( typeof info === 'string' )
            this._wm_assignment_id = info;
        return this._wm_assignment_id;
    }

    /**
     *  siteInfo1
     *
     *  @return string
     */
    this.params.WOFILTERS.siteInfo1 = function ( value ) {
        if ( typeof value === 'string' )
            this._siteinfo1 = value;
        return this._siteinfo1;
    }
    /**
     *  siteInfo2
     *
     *  @return string
     */
    this.params.WOFILTERS.siteInfo2 = function ( value ) {
        if ( typeof value === 'string' )
            this._siteinfo2 = value;
        return this._siteinfo2;
    }
    /**
     *  displayContractors
     *
     *  @return string
     */
    this.params.WOFILTERS.displayContractors = function ( value ) {
        if ( typeof value === 'string' )
            this._displaycontractors = value;
        return this._displaycontractors;
    }
    /**
     *  contractorId
     *
     *  @return string
     */
    this.params.WOFILTERS.contractorId = function ( value ) {
        if ( typeof value === 'string' )
            this._contractor_id = value;
        return this._contractor_id;
    }
    /**
     *  lastName
     *
     *  @return string
     */
    this.params.WOFILTERS.lastName = function ( value ) {
        if ( typeof value === 'string' )
            this._lastname = value;
        return this._lastname;
    }

    /**
     *  Check params
     */
    if ( typeof this.params.baseUrl !== "string" || this.params.baseUrl.length < 6 )
        throw new Error("You should set correct 'baseUrl' for AJAX requests: '"+this.params.baseUrl+"'");
    if ( typeof this.params.container === "string" && this.params.container.length < 1)
        throw new Error("Yor should set correct container for widget showing");
    if ( window.jQuery === undefined )
        throw new Error("You shold use jQuery library: see www.jquery.com");
}

FSWidget.NAME           = 'FSWidget';
FSWidget.DESCRIPTION    = 'Abstract widget class';
FSWidget.VERSION        = '0.1';

FSWidget.filterActive   = false;
FSWidget.filterActivate = function ( bool ) {
    if ( bool !== undefined && bool !== null ) {
        FSWidget.filterActive = ( bool ) ? true : false;
    }
}
FSWidget.isFilterActive = function () {
    return ( FSWidget.filterActive ) ? true : false;
}


FSWidget.saveCookie = function (name, value, expires)
{
        var expireDate;
        if (expires == undefined) {
                expireDate = new Date;
                expireDate.setMonth(expireDate.getMonth()+6);
        }
        else expireDate = new Date(expires);

        document.cookie = name + "=" + value + ";expires=" + expireDate.toGMTString() + ";path=/";
}

FSWidget.getCookie = function(theCookie)
{
        var search = theCookie + "="
        var returnvalue = "";
        if (document.cookie.length > 0)
        {
                offset = document.cookie.indexOf(search);
                // if cookie exists
                if (offset != -1)
                {
                        offset += search.length;
                        // set index of beginning of value
                        end = document.cookie.indexOf(";", offset);
                        // set index of end of cookie value
                        if (end == -1) end = document.cookie.length;
                        returnvalue=unescape(document.cookie.substring(offset, end));
                }
        }
        return returnvalue;
}

/**
 *  name
 *
 *  Function return class name
 *
 *  @return string
 */
FSWidget.prototype.name = function() {
    return this.constructor.NAME;
}
/**
 *  version
 *
 *  Function return class version
 *
 *  @return string
 */
FSWidget.prototype.version = function() {
    return this.constructor.VERSION;
}
/**
 *  description
 *
 *  Function return class description
 *
 *  @return string
 */
FSWidget.prototype.description = function() {
    return this.constructor.DESCRIPTION;
}
/**
 *  Get Filters object
 *
 *  @return object
 */
FSWidget.prototype.filters = function () {
    return this.params.WOFILTERS;
}
/**
 *  initFilters
 *
 *  Abstract function for init filters
 */
FSWidget.prototype.initFilters = function () {
    throw new Error("You can't call abstract function");
}
/**
 *  applyFilters
 *
 *  Function set all widget filters into they params
 *  @abstract
 *  @return void
 */
FSWidget.prototype.applyFilters = function () {
    throw new Error("You can't call abstract function");
}
/**
 *  showPreloader
 *
 *  Abstract function for display Widget
 */
FSWidget.prototype.showPreloader = function() {
    throw new Error("You can't call abstract function");
}
/**
 *  show
 *
 *  Abstract function for display Widget
 */
FSWidget.prototype.show = function(options) {
    throw new Error("You can't call abstract function");
}
/**
 *  open
 *
 *  Abstaract function for open new window
 */
FSWidget.prototype.open = function(options) {
    throw new Error("You can't call abstract function");
}
/**
 *  _isValidTab
 *
 *  Function check: is given tab correct?
 *
 *  @param string tab
 *  @return boolean
 */
FSWidget.prototype._isValidTab = function(tab) {
    if ( typeof tab !== 'string' || tab.length < 2 )
        return false;
    var idx;
    var tabs = this.params.WIDGET.avaliableTabs;
    for ( idx in tabs ) {
        if ( idx === tab )
            return true;
    }
    return false;
}
FSWidget.prototype._isValidPopup = function (popup) {
    if ( typeof popup !== 'string' || popup.length < 2 )
        return false;
    var idx;
    var popups = this.params.POPUP.avaliablePopups;
    
    for ( idx in popups )
        if ( idx === popup )
            return true;
    return false;
}
/**
 *  _makeUrl
 *
 *  Function make full valid URL
 *
 *  @param string tab Valid tab name
 *  @param string queryString Additional query string
 *  @return string
 *  @throw Error
 */
FSWidget.prototype._makeUrl = function(tab, queryString, forceNoSSL) {
    if (!forceNoSSL) forceNoSSL = false;
    if ( this._isValidTab(tab) )
        var route = this.params.WIDGET.avaliableTabs[tab];
    else if ( this._isValidPopup(tab) )
        var route = this.params.POPUP.avaliablePopups[tab];
    else
        throw new Error('Incorrect tab or popup option: "'+tab+'"');

    queryString = queryString || '/';
    queryString = ( queryString.substring(0,1) !== '/' ) ? '/'+queryString : queryString;
    queryString = ( queryString.substring(queryString.length-1,queryString.length) !== '/' ) ? queryString+'/' : queryString;
    bu = this.params.baseUrl;
    if (forceNoSSL) bu = bu.replace("https", "http");
    var url = bu + (( bu.substring(bu.length-1,bu.length) === '/' ) ? "" : "/");

    return url + route.module + '/' + route.controller + '/' + route.action + queryString;
}
/**
 *  fillContainer
 *
 *  insert html to DOM
 *
 *  @param string html
 *  @param FSWidgetDashboard wd
 *  return void
 */
FSWidget.fillContainer = function(html, wd) {
    if ( typeof html === 'string' && html.length > 0 ) {
        if ( typeof wd.params.container === 'string' )
            wd.params.container = $('#'+wd.params.container);
        if ( wd.params.container !== null ){
            $(wd.params.container).html(html);
        }
    }
}
/**
 *  getWidgetHtml
 *
 *  draw popup handler
 *
 *  @param FSPopupRoll object
 *  @param object params setted into link
 *  @return void
 */
FSWidget.prototype.getWidgetHtml = function () {
    var roll    = arguments[0];
    var params  = arguments[1] || {};
    roll.__persistantParams = params;

    var _data = params.params || {};
    var _url;

    try {
        _url = this._makeUrl(params.popup, '');
    } catch ( e ) {
        return;
    }

    var _async = ( params.async === false ) ? false : true;

    jQuery.ajax({
        async: _async,
        url: _url,
        cache: true,
        type: 'POST',
        dataType: 'html',
        data: _data,
        context : roll,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/clients/logIn.php";
            } else {
                this.body(xhr.responseText);
            }
        },
        success : function(data, status, xhr) {
            var params = this.__persistantParams || {};
            delete this.__persistantParams;

            //  We can call any handler function before display popup but after set popup's body or title
            //  Handler is called with 2 params:
            //      1 - current object
            //      2 - object params, which set as param into show function
            if ( params.handler !== undefined && typeof params.handler === 'function' ) { // call heandler
                if ( params.context !== undefined && typeof params.context === 'object' )
                    params.handler.call(params.context, this, params);
                else
                    params.handler(this, params);
            }

            $(this.header()).css('width', this.width() - 20); //  20 pixels for roll close button

            if ( typeof data === 'string' ) { // st container body
                this.body(data);
            }

            this.width(params.width);
            this.height(params.height);

            /* pop-up top position */
            var _top, _left;
            if ( params.top === undefined ) {
                if ( params.eTop + this.height() > params.vScroll + params.viewportHeight ) {
                    if ( this.height() > params.viewportHeight ) {
                        _top = params.vScroll + 5;
                    } else {
                        _top = params.vScroll + params.viewportHeight - this.height() - (( params.hScroll ) ? 20 : 30);
                        if ( _top < 0 ) _top = 0;
                    }
                } else {
                    _top = params.eTop;
                }
            } else {
                _top = params.top;
            }
            /* pop-up left position */
            if ( params.left === undefined ) {
                if ( params.eLeft + this.width() > params.hScroll + params.viewportWidth ) {
                    _left = params.hScroll + params.viewportWidth - this.width() - (( params.vScroll ) ? 40 : 50);
                    if ( _left < 0 ) _left = 0;
                } else {
                    _left = params.eLeft;
                }
            } else {
                _left = params.left;
            }
			this.width(params.width-1); // chrome fix
            this.top(_top);
            this.left(_left);
			setTimeout("roll.width(" + params.width + ")", 50); // chrome fix
            /****/

            //  We can call any postHandler function after display popup
            //  postHandler is called with 2 params:
            //      1 - current object
            //      2 - object params, which set as param into show function
            if ( params.postHandler !== undefined && typeof params.postHandler === 'function' ) { // call  postHeandler
                if ( params.context !== undefined && typeof params.context === 'object' )
                    params.postHandler.call(params.context, this, params);
                else
                    params.postHandler(this, params);
            }

            $('body').css('cursor', '');
        }
    });
}
/**
 *  getParams
 *
 *  Abstract function
 */
FSWidget.prototype.getParams = function() {
    throw new Error('You can\'t call abstract function from '+this.name()+' class');
}

FSWidget.prototype.showFinalPopup = function(target, win, company, client_id) {
	client_id = typeof client_id == "undefined" ? false : client_id;
	techPayUtils = new FSTechPayUtils();

	var title = client_id ? "Tech Pay Financials: <span style='font: 10px Arial, Helvetica, sans-serif;'>WIN# " + win + " - Client ID " + client_id + "</span>"
		: "Tech Pay Financials";

	roll.delay(0);
	roll.autohide(false);
	roll.show(target, null, {
		popup:'final',
		title: title,
		width:540,
		preHandler:this.getWidgetHtml,
		context:this,
		params:{win: win, companyId: company},
		postHandler: function() {
			$("#payFinanForm").ajaxForm({dataType: 'json', success: function(data) {
				if (data.error == 0) {
					roll.hide();
					reloadTabFrame();
				} else {
		            $('#woErrorMsg').html(data.msg);
		            $('#woErrorMsg').show('fast',function(){
		                setTimeout(function () {  $('#woErrorMsg').hide(500); }, 3000);
		            });
				}
			}, beforeSubmit: function() {
		        progressBar.key('updatepayfinan');
		        progressBar.run();
		        return true;
			}});
			$("#payFinanForm .calculate").change(function() {
				var thisObj = $(this);
				if (!thisObj.hasClass('noformat')) {
					thisObj.val(techPayUtils.formatAmount(thisObj.val()));
				}

				var values = {};
				$("#payFinanForm .calculate").each(function(index, element) {
					values[$(element).attr('name')] = $(element).val();
				});
				
				values["ReimbursableExpenseThroughFSApproved_Final"] = parseFloat($('#ReimbursableExpenseThroughFSApproved_Final').html());

				var payAmount = techPayUtils.calculate(values);
				$("#payFinan_PayAmount").val(payAmount);
				$("#payFinan_PayAmount_Output").html("<span>" + techPayUtils.formatAmount(payAmount).valueOf() + "</span>");
			});

			$("#payFinanForm .calculateHrs").change(function() {
				if ($("#Approve_Amount_Per").val() != "Hour") return;
				var bidAmount = $("#Tech_Bid_Amount").text();
				bidAmount = String(bidAmount);
				var parts = bidAmount.split(" / ");
				bidAmount = parseFloat(parts[0]) * ($("#calculatedTechHrs").val()*1);	
				$("#baseTechPay").val(bidAmount).change(); 				
			});

			if (client_id) $(roll.header()).css('width', '95%');
		}
	});
}
FSWidget.prototype.showFinalreadPopup = function(target, win, company, client_id) {
	client_id = typeof client_id == "undefined" ? false : client_id;

	var title = client_id ? "Tech Pay Financials: <span style='font: 10px Arial, Helvetica, sans-serif;'>WIN# " + win + " - Client ID " + client_id + "</span>"
		: "Tech Pay Financials";

	roll.delay(0);
	roll.autohide(false);
	roll.show(target, null, {
		popup:'finalread',
		title: title,
		width:540,
		preHandler:this.getWidgetHtml,
		context:this,
		params:{win: win, companyId: company},
		postHandler: function() {
			$("#payFinanreadForm").ajaxForm({dataType: 'json', success: function(data) {
				if (data.error == 0) {
					roll.hide();
					reloadTabFrame();
				} else {
		            $('#woErrorMsg').html(data.msg);
		            $('#woErrorMsg').show('fast',function(){
		                setTimeout(function () {  $('#woErrorMsg').hide(500); }, 3000);
		            });
				}
			}, beforeSubmit: function() {
		        progressBar.key('updatepayfinan');
		        progressBar.run();
		        return true;
			}});

			if (client_id) $(roll.header()).css('width', '95%');
		}
	});
}
FSWidget.prototype.resizeFrame = function() {
	try {
		if (window.parent.document) {
			$("#dashFrame", window.parent.document).width('100%');
			$("#dashFrame", window.parent.document).height($(document).height()+40);
			$(document).resize(function(){
				$("#dashFrame", window.parent.document).height($(document).height()+40);
			});
		}
	} catch (e) {}
}
