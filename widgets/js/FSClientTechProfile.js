/**
 * FSClientTechProfile
 *
 * @param Object|null params
 * @access public
 * @return void
 */
function FSClientTechProfile ( params ) {
    FSWidgetDashboard.call(this, params);

    this.params.POPUP.avaliablePopups = {};
    this.params.WIDGET.avaliableTabs = {
    	client: {module:'dashboard',controller:'do',action:'get-tech-info'}
    };
    
	switch (this.params.tab) {
		case 'client':
			this._type = 'client';
			break;
	}
	
    this._techId = this.params.techId;
	try {
		this._win = this.params.win;
	}
	catch (e) {
		this._win = '';
	}
	this.loaderVisible = false;
	this._expertiseInfo = "";
	this._equipmentInfo = "";
	this._equipmentInfo2 = "";
	this._logBookSortDirection = "DESC";
	this._company_id = this.params.companyID;
	this._client_id = this.params.clientID;
	this._content = {};
	this._current_target = null;
	this.resetCommentsSort();
}

FSClientTechProfile.prototype = new FSWidgetDashboard();
FSClientTechProfile.prototype.constructor = FSClientTechProfile;

FSClientTechProfile.NAME          = 'FSClientTechProfile';
FSClientTechProfile.VERSION       = '0.1';
FSClientTechProfile.DESCRIPTION   = 'Class FSClientTechProfile';

FSClientTechProfile.prototype.userType = function() {
	return this._type;
};

/**
 *  show
 *
 *  Function displays widget
 *
 *  @param Object|null options
 */
FSClientTechProfile.prototype.show = function(options) {

	me = this;
    var p = options || {tab:this.currentTab,params:this.getParams()};
    var queryString = '/';
    var _url;

    // TODO params needs be reconfigured
    if ( p.params !== undefined && p.params !== null && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params ) {

            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] === true || p.params[key] === false ) {
                if ( p.params[key] === false ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] !== undefined && p.params[key] !== null && p.params[key] !== "" && ( (typeof(p.params[key]) == 'string' && p.params[key].toLowerCase() !== 'none') || typeof(p.params[key]) == 'number') ) {
                //queryString += key + '/' + p.params[key] + '/';
            } else {
                delete p.params[key];
            }
        }
    } else {
        p.params = {};
    }

    try {
        if ( p.tab !== undefined && p.tab !== null ) {
            this.currentTab = p.tab;
            _url = this._makeUrl(p.tab, queryString);
            var idx;
            for ( idx in this.params.WIDGET.avaliableTabs ) {
                $('#'+idx).toggleClass('current', idx === this.currentTab);
            }
        } else if ( p.popup !== undefined && p.popup !== null ) {
            _url = this._makeUrl(p.popup, queryString);
        }
    } catch ( e ) {
        return false;
    }
    
    this.showPreloader();

    if ( this.tabOpenXhrRequest ) {
        this.tabOpenXhrRequest.abort();
        this.tabOpenXhrRequest = null;
    }
    
    // get page size
    if ($('#pageSizeSelector') ) {
    	p.params.size = $('#pageSizeSelector').val();
    }
	
	p.params['id'] = this._techId;
	p.params['type'] = this._type;
	p.params['companyID'] = this._company_id;
	
    this.tabOpenXhrRequest = jQuery.ajax({
        url : _url,
        data : p.params,
        cache : false,
        type : 'POST',
        dataType : 'json',
        context : this,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/" + this._type + "/logIn.php";
            } else {
                FSWidget.fillContainer("Sorry, but error occurred. No results found.", this);
            }
            this.tabOpenXhrRequest = null;
        },
        success : function(data, status, xhr) {
            $('body').css('cursor', '');
            this.loadTechGenInfo(data.techInfo);
            this._expertiseInfo = data.techInfo.expertiseInfo;
            this._equipmentInfo = data.techInfo.equipmentInfo;
            this._equipmentInfo2 = data.techInfo.equipmentInfo2;
			this.loadTechExpertiseInfo();
			this.loadTechLikeInfo();
            //13910
            this.loadTechNetLikeInfo();
			//end 13910
			//14105
			this.loadTechWebsite(data.techInfo);
			//End 14105 
			this.loadTechFiles(data.techInfo.techFiles);
			this.loadTechSocialNetworks(data.techInfo);
			this.loadTech12MonthStats(data.techInfo);
			this.loadTechCertifications(data.techInfo);
			//this.loadTechSchedule();
        }
    });
};

FSClientTechProfile.prototype.loadTechLogbookWoInfo = function(company, sort){
	var me = this;
	var content_title = (typeof company == "undefined") ? "logbook_FS" : "logbook_" + company;
	var table = $("#fsLogbookTable2 tbody");
	var offset = 0, limit = 25, delay = 2000;

	(typeof sort == "undefined") ? sort = "" : sort = sort ;
	content_title += sort;

	var write_row = function(index, value, title){
					var ratings = "<span>";
					var amount = "";
					
					if(value.SATPerformance == null) value.SATPerformance = 0;
					var performanceInt = parseFloat(value.SATPerformance);
/*					for(i=1; i < 6; i++){
						ratings += "<input type='radio' class='starRating' name='logBookRating" + logBookRow + "' " + (performanceInt == i ? "checked='checked'" : "") + " value='' />";
					}*/
					var starArr = [];

		if (me._current_target == title) {
					for (i=1; i<6; i++) {
						var $tempInput = $('<input type="radio" />').attr('name',"logBookRating"+logBookRow).attr('class','starRating').attr("disabled", "disabled").val('');
						if(performanceInt == i)
							$tempInput.attr('checked', true);
						starArr.push($tempInput);
					}
		}

					++logBookRow;

					if(value.StartDate == null || value.StartDate == "")value.StartDate = "";
					if(value.Tech_Bid_Amount == null || value.Tech_Bid_Amount == "")value.Tech_Bid_Amount = "";
					if(value.woCategoryLabel == null || value.woCategoryLabel == "")value.woCategoryLabel = "";
					if(value.Headline == null || value.Headline == "")value.Headline = "";

					if (value.Tech_Bid_Amount) {
						amount += value.Tech_Bid_Amount;
						if (value.Amount_Per) {
							amount += " Per " + value.Amount_Per;
						}
					}

		if (me._current_target == title) {
					$('#fsLogbookTable2').find("tbody")
						.append($('<tr>')
				.append($("<td style='width: 55px;'>").text(value.StartDate))
				.append($("<td style='width: 100px;'>").text(amount))
				.append($("<td style='width: 130px; text-align: left;'>").text(value.woCategoryLabel))
				.append($("<td style='width: 242px; text-align: left;'>").text(value.Headline))
				.append($("<td style='width: 90px;'>").append.apply($("<td style='width: 90px;'>"),$.isArray(starArr) ? starArr : [starArr])));

//					tableRows += "<tr><td>"+value.StartDate+"</td><td>$"+value.Tech_Bid_Amount+"/"+value.Amount_Per+"</td><td>"+value.woCategoryLabel+"</td><td>"+value.Headline+"</td><td>"+ratings+"</td></tr>";
		}
	};

	var loader = function (title) {
		offset += limit;
		$.ajax({
			type: "POST",
			url: '/widgets/dashboard/client/get-tech-logbook-info/',
	        dataType    : 'json',
	        cache       : true,
	        async		: true,
			data: {id:me._techId, Company_ID:company, offset: offset, limit: limit, sort: sort, sortDirection:me._logBookSortDirection},
			success: function (data) {
				var title_reup = title;
				var tableRows;
				if (data != null && data != ""){
					me._content[title_reup] = me._content[title_reup].concat (data);
					logBookRow = 0;
					$.each(data, function (index, value) {
						write_row (index, value, title_reup);
					});

					$("input.starRating").rating();
					setTimeout (function () {
						loader (title_reup);
					}, delay);
            	}
			}
				});
	};

	$("#fsLogbookColumns2").html("");
	table.html("");
	me.showLoader();
	if (sort != "" || typeof this._content[content_title] == "undefined") {
		$.ajax({
			type: "POST",
			url: '/widgets/dashboard/client/get-tech-logbook-info/',
	        dataType    : 'json',
	        cache       : true,
	        async		: true,
			data: {id:me._techId, Company_ID:company, offset: offset, limit: limit, sort: sort, sortDirection:me._logBookSortDirection},
			success: function (data) {
				var columnHeadings = '<th onclick="techProfile.loadTechLogbookWoInfo(&quot;&quot;, &quot;StartDate&quot;);" class="columnLink" style="width: 55px;">Date</th><th onclick="techProfile.loadTechLogbookWoInfo(&quot;&quot;, &quot;Tech_Bid_Amount&quot;);" class="columnLink" style="width: 102px;">Tech Pay</th><th onclick="techProfile.loadTechLogbookWoInfo(&quot;&quot;, &quot;woCategoryLabel&quot;);" class="columnLink" style="width: 130px;">Skill</th><th onclick="techProfile.loadTechLogbookWoInfo(&quot;&quot;, &quot;Headline&quot;);" class="columnLink" style="width: 238px;">Headline</th><th onclick="techProfile.loadTechLogbookWoInfo(&quot;&quot;, &quot;SATPerformance&quot;);" class="columnLink" id="logbookHeaderCellLast" style="width: 89px;">Rating</th>';
				var title_reup = content_title;
				var tableRows;
				me.hideLoader();
				me._current_target = title_reup;	//remember which table is currently loading.
				$("#fsLogbookTable2").show();
				$("#fsLogbookColumns2").show();
				if (data == null || data == ""){
					tableRows = "<tr><td colspan='5' style='text-align: center;'>No Data At This Time</td></tr>";
				}else{
					me._content[title_reup] = data;
					logBookRow = 0;
					$.each(data, function (index, value) {
						write_row (index, value, title_reup);
					});

					setTimeout (function () {
						loader (title_reup);
					}, delay);
			}
			
			$("#fsLogbookTable").hide();
			$("#fsLogbookHeading").parent().hide();
			$("#fsLogbookColumns2").html(columnHeadings);
//			$("#fsLogbookTable tbody").html(tableRows);
			$("#fsLogbookTable2").tableScroll({height:350, containerClass:'jobHistory'});
			if($(".tablescroll_head").length != 0)$(".tablescroll_head").css("width", "100%");
			$("input.starRating").rating();
				$("#logbookHeaderCellLast").css("width", "90px");
		}
	});
	
	(this._logBookSortDirection == "DESC") ? this._logBookSortDirection = "ASC" : this._logBookSortDirection = "DESC";
	}
	else {
		var columnHeadings = '<th onclick="techProfile.loadTechLogbookWoInfo(&quot;&quot;, &quot;StartDate&quot;);" class="columnLink" style="width: 55px;">Date</th><th onclick="techProfile.loadTechLogbookWoInfo(&quot;&quot;, &quot;Tech_Bid_Amount&quot;);" class="columnLink" style="width: 102px;">Tech Pay</th><th onclick="techProfile.loadTechLogbookWoInfo(&quot;&quot;, &quot;woCategoryLabel&quot;);" class="columnLink" style="width: 130px;">Skill</th><th onclick="techProfile.loadTechLogbookWoInfo(&quot;&quot;, &quot;Headline&quot;);" class="columnLink" style="width: 238px;">Headline</th><th onclick="techProfile.loadTechLogbookWoInfo(&quot;&quot;, &quot;SATPerformance&quot;);" class="columnLink" id="logbookHeaderCellLast" style="width: 89px;">Rating</th>';
		var tableRows;
		me._current_target = content_title;
		$("#fsLogbookTable2").show();
		$("#fsLogbookColumns2").show();
		if (me._content[content_title] == null || me._content[content_title] == ""){
			tableRows = "<tr><td colspan='5' style='text-align: center;'>No Data At This Time</td></tr>";
		}else{
			logBookRow = 0;
			$.each(me._content[content_title], function (index, value) {
				write_row (index, value, content_title);
			});
		}
	
		$("#fsLogbookTable").hide();
		$("#fsLogbookHeading").parent().hide();
		$("#fsLogbookColumns2").html(columnHeadings);
		//$("#fsLogbookTable tbody").html(tableRows);
		$("#fsLogbookTable2").tableScroll({height:350, containerClass:'jobHistory'});
		if($(".tablescroll_head").length != 0)$(".tablescroll_head").css("width", "100%");
		$("input.starRating").rating();
		$("#logbookHeaderCellLast").css("width", "90px");
		me.hideLoader();
	}
};

FSClientTechProfile.prototype.loadTechLogbookStats = function(interval){
	$("#fsLogbookColumns").html("");
	$("#fsLogbookTable tbody").html("");
	me = this;
	me.showLoader();
	$.ajax({
		type: "POST",
		url: '/widgets/dashboard/client/get-tech-profile-stats/',
        dataType    : 'json',
        cache       : false,
		data: {id:this._techId, interval:interval},
		error: function (x, y, z) {
			//console.log (x, y, z);
		},
		success: function (data) {
			me.hideLoader();
			if(typeof interval == "undefined")interval = "";
			var columnHeadings = "<th style='width: 195px;'>Skill Category</th><th style='width: 155px;'>Work Orders</th><th style='width: 133px;'>Backouts</th><th style='width: 145px;'>No Shows</th><th style='width: 140px;'>Preference</th><th style='width: 125px;'>Performance</th>";
			
			var tableRows = "";
			var totalJobs = 0;
			var totalBackOuts = 0;
			var totalNoShows = 0;
			var totalPreference = 0;
			var totalPerformance = 0;
			var totalRatings = 0;
			var totalRatingsDivisor = 0;
			var sortArray = [];

			$.each(data,function(index,value){
				var serviceImacJobs = parseFloat(value.imac)+parseFloat(value.service);
				sortArray.push ({index: index, serviceImacJobs: serviceImacJobs});
			});
			sortArray.sort (function (element1, element2) {
				return element2.serviceImacJobs - element1.serviceImacJobs;
			});

			$.each(sortArray,function(sortindex, sortvalue){
				var index = sortvalue.index;
				var value = data[index];
				var prefAvg, perfAvg;
				var serviceImacJobs = sortvalue.serviceImacJobs; //parseFloat(value.imac)+parseFloat(value.service);

				prefAvg = parseFloat(value.preferenceAvg) || 0;
				prefAvg = prefAvg.toFixed(0) || 0;
				perfAvg = parseFloat(value.performanceAvg) || 0;
				perfAvg = perfAvg.toFixed(0) || 0;
				tableRows += "<tr><td style='text-align: left;  width: 153px;'>"+(value.Category || "")+"</td><td style='width: 70px;'>"+(parseFloat(serviceImacJobs) || "0")+"</td><td style='width: 70px;'>"+(value.backOuts || "0")+"</td><td style='width: 70px;'>"+(value.noShows || "0")+"</td><td style='width: 70px;'>"+prefAvg+"%</td><td style='width: 70px;'>"+perfAvg+"%</td></tr>";
				totalJobs += serviceImacJobs;
				totalBackOuts += parseFloat(value.backOuts) || 0;
				totalNoShows += parseFloat(value.noShows) || 0;
				if(value.preferenceTotal >= 1){
					totalPreference += Math.round(parseFloat(value.preferenceAvg));
					totalPerformance += Math.round(parseFloat(value.performanceAvg));
					totalRatingsDivisor++;
				}
				totalRatings += parseFloat(value.preferenceTotal) || 0;
			});

			totalPreference = totalPreference.toFixed(0)/totalRatingsDivisor.toFixed(0);
			totalPreference = totalPreference.toFixed(0);
			if(isNaN(totalPreference)) totalPreference = 0;
			if(totalPreference > 0)totalPreference = totalPreference + "%";
			
			totalPerformance = totalPerformance.toFixed(0)/totalRatingsDivisor.toFixed(0);
			totalPerformance = totalPerformance.toFixed(0);
			
			if(isNaN(totalPerformance)) totalPerformance = 0;
			if(totalPerformance > 0)totalPerformance = totalPerformance + "%";
			
			tableRows += "<tr><td style='font-weight:bold;text-align: left;'>Total Work Orders</td><td style='font-weight:bold;'>"+totalJobs+"</td><td style='font-weight:bold;'>"+totalBackOuts+"</td><td style='font-weight:bold;'>"+totalNoShows+"</td><td style='font-weight:bold;'>"+totalPreference+"</td><td style='font-weight:bold;'>"+totalPerformance+"</td></tr>";
			$("#fsLogbookTable2").hide();
			$("#fsLogbookColumns2").hide();
			$("#fsLogbookTable").show();
			$("#fsLogbookColumns").html(columnHeadings);
			$("#fsLogbookTable tbody").html(tableRows);
			if($(".tablescroll_head").length != 0)$(".tablescroll_head").css("width", "100%");
		}
	});
};

FSClientTechProfile.prototype.loadIsoInfo = function (isoId){
	$.ajax({
		type: "POST",
		url: '/widgets/dashboard/do/get-tech-iso-info/',
        dataType    : 'json',
        cache       : false,
		data: {isoId: isoId},
		success: function (data) {
			$("#isoName").html(data.isoInfo.Company_Name || "");
			$("#isoDispatcher").html(data.isoInfo.Contact_Name || "");
			$("#isoPhoneNum").html(data.isoInfo.Contact_Phone || "");
			$(".isoRow").show();
			
			$("#contactInfoDisplay #isoName").html(data.isoInfo.Company_Name || "");
			$("#contactInfoDisplay #isoContact").html("Contact: "+(data.isoInfo.Contact_Name || ""));

			if (data.isoInfo.Address_1) {
				$("#contactInfoDisplay #techIsoStreet").html(data.isoInfo.Address_1);
			}
			if (data.isoInfo.City) {
				$("#contactInfoDisplay #techIsoCity").html(data.isoInfo.City+", "+(data.isoInfo.State || "")+" "+(data.isoInfo.Zip || ""));
			}
			$("#contactInfoDisplay #techIsoPhone").html(data.isoInfo.Contact_Phone || "");
			$(".contactIsoInfo").show();
			
		}
	});
};

FSClientTechProfile.prototype.likeTech = function(){
	$.ajax({
		type: "POST",
		url: '/widgets/dashboard/do/like-tech/',
        dataType    : 'json',
        cache       : false,
		
        data: {id:this._techId, clientId: this._client_id}, 
		success: function (data) {                  
                        $("#numNetLikes").html(""+data.totalData.numNetLikes+"");
                        if(data.totalData.numNetLikes < 0 )
                        {
                            $("#numNetLikes").css('color', "#FF2525");  
                            $("#imageThumbs").html("<img src='/widgets/images/thumbs-down.png' width='17' height='22'/>");//14027
                        }
                        else
                        {
                            $("#numNetLikes").css('color', "#333333");     
                            $("#imageThumbs").html("<img src='/widgets/images/thumbs-up.png' width='17' height='22'/>");//14027
                        }
                 
			if(data.likeData.liked == true){
				$("#likeButton").html("<a onclick='techProfile.unlikeTech();'> Unlike</a>");
			}
		}
	});
};

FSClientTechProfile.prototype.unlikeTech = function(){
	$.ajax({
		type: "POST",
		url: '/widgets/dashboard/do/unlike-tech/',
        dataType    : 'json',
        cache       : false,
		
        data: {id:this._techId, clientId: this._client_id},
		success: function (data) {
                     $("#numNetLikes").html(""+data.totalData.numNetLikes+"");
                        if(data.totalData.numNetLikes < 0 )
                        {
                            $("#numNetLikes").css('color', "#FF2525");  
                            $("#imageThumbs").html("<img src='/widgets/images/thumbs-down.png' width='17' height='22'/>");//14027
                        }
                        else
                        {
                            $("#numNetLikes").css('color', "#333333");     
                            $("#imageThumbs").html("<img src='/widgets/images/thumbs-up.png' width='17' height='22'/>");//14027
                        }
                 
			if(data.likeData.liked == false){
				$("#likeButton").html("<a onclick='techProfile.likeTech();'> Like</a>");
			}
		}
	});
};

FSClientTechProfile.prototype.loadTechLikeInfo = function(){
	$.ajax({
		type: "POST",
		url: '/widgets/dashboard/do/get-tech-like-info/',
        dataType    : 'json',
        cache       : false,
		data: {id:this._techId, clientId: this._client_id},
		success: function (data) {
			if (data.success != "error") {
			$("#numLikes").html("("+data.likeData.numLikes+")");
			if(data.likeData.liked == true){
				$("#likeButton").html("<a onclick='techProfile.unlikeTech();'> Unlike</a>");
			}else{
				$("#likeButton").html("<a onclick='techProfile.likeTech();'> Like</a>");
			}
		}
		}
	});
};  

//13910
FSClientTechProfile.prototype.loadTechNetLikeInfo = function(){
    $.ajax({
        type: "POST",
        url: '/widgets/dashboard/do/get-tech-net-like-info/',
        dataType    : 'json',
        cache       : false,
        data: {id:this._techId},
        success: function (data) {
            if (data.success != "error") {
            $("#numNetLikes").html(""+data.likeData.numNetLikes+"");
            if(data.likeData.numNetLikes < 0 )
            {
               
                $("#numNetLikes").css('color', "#FF2525");  
            }
            else
            {
                   $("#numNetLikes").css('color', "#333333");     
            }
            //remove
        }
        }
    });
};


FSClientTechProfile.prototype.loadTechGenInfo = function(techInfo){
	me = this;
        var techStatusprefer = 0;
        var techStatusdenined = 0;
	for(index in techInfo){
		if(techInfo[index] == "null" || techInfo[index] == null){
			techInfo[index] = "";
		}
							
		if (index == "Bg_Test_Pass_Lite" || index == "Bg_Test_Pass_Full") {
			if (techInfo[index] == true){
				$("#"+index).append($("<img />").attr("src","/widgets/images/check_green.gif"));
				if(index == "Bg_Test_Pass_Lite"){
                                if(techInfo['Bg_Test_ResultsDate_Lite']!="")
                                {
                                    var part = techInfo['Bg_Test_ResultsDate_Lite'].split(/[- :]/);
									//14108
									if(part.length > 3){
										bgLiteDate = new Date(part[0], part[1]-1, part[2]);
										month = bgLiteDate.getMonth()+1;
										$("#Bg_Test_ResultsDate_Lite").html("("+month+"/"+bgLiteDate.getDate()+"/"+bgLiteDate.getFullYear()+")");
									}
									//End 14108
					}
				}else{
//					var part = techInfo['BG_Test_ResultsDate_Full'].split(/[- :]/);
//					bgFullDate = new Date(part[0], part[1]-1, part[2]);
//					month = bgFullDate.getMonth()+1;
					$("#Bg_Test_ResultsDate_Full").html("(" + techInfo.credentialInfo['bgCheckFullDate']  + ")");
				}
			}
			else if (techInfo[index] == "") $("#"+index).html('');
			else $("#"+index).html('');
		}
                //700 panel
                else if(index == "DrugPassed")
                {
                    if(techInfo[index] == "1" )
                    {
						$("#DrugPassedpanel").append($("<img />").attr("src","/widgets/images/check_green.gif"));
									
						try{
							var partDatePassDrug = techInfo['DatePassDrug'].split(/[- :]/);
							//14108
							
							if(partDatePassDrug.length >= 3){
								$("#drugTestDatepanel").html(partDatePassDrug[1]+"/" + partDatePassDrug[2]+"/"+partDatePassDrug[0]);
							}
							//End 14108
						} catch (e) {
						}
									
						if(techInfo['SilverDrugPassed'] != "1")
						{
								$("#SilverDrugPassedpanel").append($("<img />").attr("src","/widgets/images/check_green.gif"));
								try
								{
									var partDatePassDrug = techInfo['DatePassDrug'].split(/[- :]/);
									//14108
									if(partDatePassDrug.length >= 3){
										$("#DatePassedSilverDrugpanel").html(partDatePassDrug[1]+"/" + partDatePassDrug[2]+"/"+partDatePassDrug[0]);
									}
									//End 14108
								} catch (e) {}
						}
					}else{
						
                    }
				}
                else if(index == "SilverDrugPassed"){
                    if(techInfo['SilverDrugPassed'] == "1"){
						$("#SilverDrugPassedpanel").append($("<img />").attr("src","/widgets/images/check_green.gif"));
						try{
							var partDatePassedSilverDrug = techInfo['DatePassedSilverDrug'].split(/[- :]/);
							//14108
							if(partDatePassedSilverDrug.length >= 3){
								$("#DatePassedSilverDrugpanel").html(partDatePassedSilverDrug[1]+"/" + partDatePassedSilverDrug[2]+"/"+partDatePassedSilverDrug[0]);
							}
							//End 14108
						} catch (e) {}
					}
                }
                //end 700
                else if(index == "isPreferred" && techInfo[index] == true){
                        techStatusprefer = 1;
		}else if(index == "isDenied" && techInfo[index] == true){
			techStatusdenined = 1;
		}else if(index == "ISO_Affiliation_ID" && techInfo[index] != ""){
			this.loadIsoInfo(techInfo[index]);
		}else if(index == "FLS_Photo_ID"){
			divID = index;
        	if(techInfo[index] == 1)$("#"+divID+" span[id*='StatusDisplay']").append($("<img />").attr("src","/widgets/images/check_green.gif"));
        	$("#FLS_Photo_IDHidden").val(techInfo[index]);
		}
                else if(index == "EMCCert")
                        {
                            for(var j= 0; j< techInfo[index].length;j++)
                            {
                                var _date  = techInfo[index][j].date;
                                if(jQuery.trim(_date) == "") 
                                    _date = "&nbsp;";
                                else
                                {
                                    var part = techInfo[index][j].date.split(/[- :]/);
									//14108
									if(part.length >= 3){
										bgLiteDate = new Date(part[0], part[1]-1, part[2]);
										var _month = bgLiteDate.getMonth()+1;
										var _day = bgLiteDate.getDate();
										var _smonth = _month < 10? "0"+_month.toString(): _month.toString();
										var _sday = _day < 10? "0"+_day.toString(): _day.toString();
										_date = _smonth + "/" + _sday + "/" + bgLiteDate.getFullYear();
									}
									//End 14108
                                }
                                jQuery('#EMCCertDisplay').append('<div>'+techInfo[index][j].label+'</div>');
                                jQuery('#EMCCertDateDisplay').append('<div>' + _date + '</div>');
                            }
                        }
                else if(index == "PrimaryPhone")
                    {
                        detailObject.onInit.RequitePhone(
                            "#"+index+"Label"
                            ,techInfo[index]
                            ,techInfo['TechID']
                            ,techInfo['PrimaryPhoneStatus']
                            ,1
                            ,'html'
                            ,Base64.encode(techInfo['Firstname']+ ' ' +techInfo['Lastname'])); 
                        detailObject.onInit.RequitePhone(
                            "#techDayPhone"
                            ,techInfo[index]
                            ,techInfo['TechID']
                            ,techInfo['PrimaryPhoneStatus']
                            ,1
                            ,'html'
                            ,Base64.encode(techInfo['Firstname']+ ' ' +techInfo['Lastname']));     
                }
                else if(index == "CopierSkillsAssessment")
                {
                    jQuery(".cmdCopiesSkillView").attr("techid",techInfo["TechID"]);
                    if(techInfo[index] == 1)
                    {    
                        $("#"+index+"Display").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                    }
                    else
                    {
                        var el = jQuery(".cmdCopiesSkillView").parent();
                        var html = jQuery(".cmdCopiesSkillView").html();
                        jQuery(el).html(html);
    
                    }    
                }  
                else if(index == "CopierSkillsAssessmentDate")
                {
                    if(techInfo["CopierSkillsAssessment"] == 1)
                    {    
                        $("#"+index+"Display").html(techInfo[index]);
                    }
                }
                else if(index == "SecondaryPhone")
                {
                     detailObject.onInit.RequitePhone(
                            "#"+index+"Label"
                            ,techInfo[index]
                            ,techInfo['TechID']
                            ,techInfo['SecondaryPhoneStatus']
                            ,2
                            ,'html'
                            ,Base64.encode(techInfo['Firstname']+ ' ' +techInfo['Lastname']));   
                            
                }
                else{
			if(techInfo[index].toString().match(/[!\\]/g))techInfo[index] = techInfo[index].toString().replace(/\\/g, '');
			element = $("#"+index);
			if (element.is(':checkbox') && (techInfo[index] == true || techInfo[index] == 1))
				element.attr('checked', true);
			else if (element.is('textarea'))
				element.html(techInfo[index]);
			else if (element.is ('img'))
				element.attr ('src', "/widgets/images/checklist_true.jpg");
			else
				element.val(techInfo[index]);
			$("#"+index+"Label").html(techInfo[index]);
		}
	}
        if(techStatusdenined == 1)
        {
            $("#isDenied").attr("checked", "checked");
			$("#deniedLabel")
                            .css('color', "#F69630")
                            .css("font-weight", "bold")
                            .css("text-decoration","underline")
                            .css("cursor","pointer");
	
	}
        else if (techStatusprefer == 1)
        {
            $("#isPreferred").attr("checked", "checked");
				$("#preferredLabel").css('color', "#F69630").css("font-weight", "bold");
        }
			
	if( (techInfo.EquipExperience == "" || techInfo.EquipExperience == null) && (techInfo.SiteRef == "" || techInfo.SiteRef == null) && (techInfo.SpecificExperience  == "" || techInfo.SpecificExperience == null)){
		$("#BusinessStatementSection").html("<div style='text-align:center;'>No Information Provided</div>");
	}
	
	 $(".BusinessStatement").expander({
			slicePoint: 75,
			expandPrefix: ' ',
			expandText: '(read more...)'
		});
	
	//Info Section 
	$("#contactInfoDisplay #techName").html("<h1 style='color: #000;'>"+techInfo.Firstname+" "+techInfo.Lastname+"</h1>");
	$("#contactInfoDisplay #techStreet").html(techInfo.Address1);
	$("#contactInfoDisplay #techCityStateZip").html(techInfo.City+", "+techInfo.State+" "+techInfo.Zipcode);
	
        detailObject.onInit.RequitePhone(
                            "#contactInfoDisplay #techDayPhone"
                            ,techInfo.PrimaryPhone
                            ,techInfo.TechID
                            ,techInfo.PrimaryPhoneStatus
                            ,1
                            ,'html'
                            ,Base64.encode(techInfo.Firstname+ ' ' +techInfo.Lastname));
                            
	//$("#contactInfoDisplay #techDayPhone").html(techInfo.PrimaryPhone);
	if(techInfo.SecondaryPhone != "" || techInfo.SecondaryPhone != null){
                detailObject.onInit.RequitePhone(
                            "#contactInfoDisplay #techEvePhone"
                            ,techInfo.SecondaryPhone
                            ,techInfo.TechID
                            ,techInfo.SecondaryPhoneStatus
                            ,2
                            ,'html'
                            ,Base64.encode(techInfo.Firstname+ ' ' +techInfo.Lastname)); 
		//$("#contactInfoDisplay #techEvePhone").html(techInfo.SecondaryPhone);
		$("#techEvePhoneRow").show();
	}
	$("#contactInfoDisplay #techEmail").html(techInfo.PrimaryEmail);
	if(techInfo.SecondaryEmail != "" || techInfo.SecondaryEmail != null){
		$("#contactInfoDisplay #techSecondaryEmail").html(techInfo.SecondaryEmail);
		$("#techSecondaryEmailRow").show();
	}
	$("#contactInfoDisplay #techSMS").html(techInfo.SMS_Number);
	
	$("#contactInfoDisplay #techDateRegistered").html(techInfo.RegDate);
	$("#contactInfoDisplay #techLastLogin").html(techInfo.lastLoginDate);
	
	var tags = {};
	if(techInfo.credentialInfo != null){
		if (techInfo.credentialInfo['bgCheckFullStatus'] == 1 && techInfo.credentialInfo['drugTestStatus'] == 1) {
			today = new Date();
			yearago = new Date(today.getFullYear() - 1, today.getMonth(), today.getDate());
			bgDate = $.datepicker.parseDate("mm/dd/yy",techInfo.credentialInfo['bgCheckFullDate']);
			dtDate = $.datepicker.parseDate("mm/dd/yy",techInfo.credentialInfo['drugTestDate']);
			dellMRACompliantDate = bgDate > dtDate ? bgDate : dtDate;
                        rolhovertext = "";
			if (bgDate >= yearago || dtDate >= yearago) {
				imgSrc = "/widgets/images/Dell_MRA_Compliant.png";
				dellMRACompliantStatus = "Passed";
                                rolhovertext = "class='hoverText' bt-xtitle='Dell MRA Compliant'";
			}
			else {
				imgSrc = "/widgets/images/Dell_MRA_Lapsed.png";
				dellMRACompliantStatus = "Lapsed";
				dellMRACompliantDate = new Date(dellMRACompliantDate.getFullYear() + 1, dellMRACompliantDate.getMonth(), dellMRACompliantDate.getDate() + 1);
                                date = dellMRACompliantDate.getMonth() + 1 + "/" + dellMRACompliantDate.getDate() + "/" + dellMRACompliantDate.getFullYear();
                                rolhovertext = "class='hoverText' bt-xtitle='Dell MRA Compliance Lapsed: "+ date + "'";
			}
			dellMRACompliantDate = dellMRACompliantDate.getMonth() + 1 + "/" + dellMRACompliantDate.getDate() + "/" + dellMRACompliantDate.getFullYear();
			$("#DellMRACompliantStatus").html(dellMRACompliantStatus + " - " + dellMRACompliantDate + "<img width='50' src='" + imgSrc + "'/>");
			tags["dellTag"] = "<img width='50' src='" + imgSrc + "' "+ rolhovertext + "/>";
		}
		if (techInfo.credentialInfo['bgCheckFullStatus'] == 1 && techInfo.credentialInfo['drugTestStatus'] == 1) {
                    dellMRACompliantBreakDown =  "Basic BC: " + (techInfo.credentialInfo['bgCheckStatus'] == 1 ? techInfo.credentialInfo['bgCheckDate'] : "") + "<br/>";
                    dellMRACompliantBreakDown += "Comp BC: " + (techInfo.credentialInfo['bgCheckFullStatus'] == 1 ? techInfo.credentialInfo['bgCheckFullDate'] : "") + "<br/>";
                    dellMRACompliantBreakDown += "DT: " + (techInfo.credentialInfo['drugTestStatus'] == 1 ? techInfo.credentialInfo['drugTestDate'] : "");
                    $("#DellMRACompliantDate").html(dellMRACompliantBreakDown);
		}
                if(techInfo.certificationsInfo['BlueRibbonTechnicianCertStatus'] == 1)
                {
                    var BlueRibbonTechnicianCertdatestr;
		
                    if(techInfo.certificationsInfo['BlueRibbonTechnicianDate'] !='' && techInfo.certificationsInfo['BlueRibbonTechnicianDate'] !=null)
                    {
                        BlueRibbonTechnicianCertdatestr =  ": " + techInfo.certificationsInfo['BlueRibbonTechnicianDate'];
                    }
                    else
                    {
                        BlueRibbonTechnicianCertdatestr='';
                    }
                    tags["BlueRibbonTechnicianStatusDisplay"] = "<img style='margin-left: 5px; height:33px;' src='/widgets/images/BRT_Tag.png' class='hoverText' bt-xtitle='Blue Ribbon Technician " + BlueRibbonTechnicianCertdatestr + "'/ >";
                    $("#BlueRibbonTechnicianCertStatus").html("<img src='/widgets/images/check_green.gif'/>");
                }
		if(techInfo.certificationsInfo['DeVryCertStatus'] == 1){
			tags["devryCert"] =  "<img id='devryCert' width='60' src='/widgets/images/DeVry.png' class='hoverText' bt-xtitle='DeVry Intern' />";
		}
                
                //for issue 13761 without issue 13624
                
//                if(techInfo.certificationsInfo['BlackBoxCablingHelperCertStatus'] == 1)
//                {
//                    var datestrC1;
//
//                    if(techInfo.certificationsInfo['BlackBoxCablingHelperDate'] !='' && techInfo.certificationsInfo['BlackBoxCablingHelperDate'] !=null)
//                    {
//                        datestrC1 =  ": " + techInfo.certificationsInfo['BlackBoxCablingHelperDate'];
//                    }
//                    else
//                    {
//                        datestrC1='';
//                    }
//                    tags["BlackBoxCablingHelperCertStatus"] = "<img style='margin-left: 5px; height:33px;' src='/widgets/images/BTC1.png' class='hoverText' bt-xtitle='Black Box Cabling - Helper (C1)" + datestrC1 + "'/ >";
//                }
//                
//                if(techInfo.certificationsInfo['BlackBoxCablingAdvancedCertStatus'] == 1)
//                {
//                    var datestrC2;
//
//                    if(techInfo.certificationsInfo['BlackBoxCablingAdvancedDate'] !='' && techInfo.certificationsInfo['BlackBoxCablingAdvancedDate'] !=null)
//                    {
//                        datestrC2 =  ": " + techInfo.certificationsInfo['BlackBoxCablingAdvancedDate'];
//                    }
//                    else
//                    {
//                        datestrC2='';
//                    }
//                    tags["BlackBoxCablingAdvancedCertStatus"] = "<img style='margin-left: 5px; height:33px;' src='/widgets/images/BTC2.png' class='hoverText' bt-xtitle='Black Box Cabling - Advanced (C2)" + datestrC2 + "'/ >";
//                }
//                
//                if(techInfo.certificationsInfo['BlackBoxCablingLeadCertStatus'] == 1)
//                {
//                    var datestrC3;
//
//                    if(techInfo.certificationsInfo['BlackBoxCablingLeadDate'] !='' && techInfo.certificationsInfo['BlackBoxCablingLeadDate'] !=null)
//                    {
//                        datestrC3 =  ": " + techInfo.certificationsInfo['BlackBoxCablingLeadDate'];
//                    }
//                    else
//                    {
//                        datestrC3='';
//                    }
//                    tags["BlackBoxCablingLeadCertStatus"] = "<img style='margin-left: 5px; height:33px;' src='/widgets/images/BTC3.png' class='hoverText' bt-xtitle='Black Box Cabling - Lead (C3)" + datestrC3 + "'/ >";
//                }
//                
//                if(techInfo.certificationsInfo['BlackBoxTelecomHelperCertStatus'] == 1)
//                {
//                    var datestrT1;
//
//                    if(techInfo.certificationsInfo['BlackBoxTelecomHelperDate'] !='' && techInfo.certificationsInfo['BlackBoxTelecomHelperDate'] !=null)
//                    {
//                        datestrT1 =  ": " + techInfo.certificationsInfo['BlackBoxTelecomHelperDate'];
//                    }
//                    else
//                    {
//                        datestrT1='';
//                    }
//                    tags["BlackBoxTelecomHelperCertStatus"] = "<img style='margin-left: 5px; height:33px;' src='/widgets/images/BTT1.png' class='hoverText' bt-xtitle='Black Box Telecom  - Helper (T1)" + datestrT1 + "'/ >";
//                }
//                
//                if(techInfo.certificationsInfo['BlackBoxTelecomAdvancedCertStatus'] == 1)
//                {
//                    var datestrT2;
//
//                    if(techInfo.certificationsInfo['BlackBoxTelecomAdvancedDate'] !='' && techInfo.certificationsInfo['BlackBoxTelecomAdvancedDate'] !=null)
//                    {
//                        datestrT2 =  ": " + techInfo.certificationsInfo['BlackBoxTelecomAdvancedDate'];
//                    }
//                    else
//                    {
//                        datestrT2='';
//                    }
//                    tags["BlackBoxTelecomAdvancedCertStatus"] = "<img style='margin-left: 5px; height:33px;' src='/widgets/images/BTT2.png' class='hoverText' bt-xtitle='Black Box Telecom  - Advanced (T2)" + datestrT2 + "'/ >";
//                }
                //end 13761
                
                if(techInfo.certificationsInfo['TBCCertStatus'] == 1)
                {
                    var BlueRibbonTechnicianCertdatestr;

                    if(techInfo.certificationsInfo['TBCDate'] !='' && techInfo.certificationsInfo['TBCDate'] !=null)
                    {
                        TBCdatestr =  ": " + techInfo.certificationsInfo['TBCDate'];
                    }
                    else
                    {
                        TBCdatestr='';
                    }
                    tags["TBCCertStatusDisplay"] = "<img style='margin-left: 5px; height:33px;' src='/widgets/images/Boot_Tag.png' class='hoverText' bt-xtitle='Boot Camp Certified" + TBCdatestr + "'/ >";
					$("#TBCCertStatus").html("<img src='/widgets/images/Boot_Tag.png' width=18px/>");
					$("#BootCampCertifiedlabel").html("Boot Camp Certified");
                }
                else if(techInfo.certificationsInfo['BootCampPlusCertStatus'] == 1)
                {
                    var BlueRibbonTechnicianCertdatestr;

                    if(techInfo.certificationsInfo['BootCampPlusDate'] !='' && techInfo.certificationsInfo['BootCampPlusDate'] !=null)
                    {
                        TBCdatestr =  ": " + techInfo.certificationsInfo['BootCampPlusDate'];
                    }
                    else
                    {
                        TBCdatestr='';
                    }
                    tags["TBCCertStatusDisplay"] = "<img style='margin-left: 5px; height:33px;' src='/widgets/images/Boot_Tag_PLUS.png' class='hoverText' bt-xtitle='Boot Camp Plus" + TBCdatestr + "'/ >";
                    $("#TBCCertStatus").html("<img src='/widgets/images/Boot_Tag_PLUS.png' width='24px'/>");
                    $("#BootCampCertifiedlabel").html("Boot Camp Plus");
                    
                }

		if (techInfo.credentialInfo['fsMobileDate'] != null) {
			tags["fsMobile"] = "<img src='/widgets/images/fs_mobile_36x32.png' style='margin-left: 20px;' class='hoverText' bt-xtitle='I Have FS-Mobile' />";
			$("#fsMobileStatus").html ("<img src='/widgets/images/fs_mobile_36x14.png' />");
		}

		if (isPM || _company.toUpperCase() == "MDS") {
		if(techInfo.TrapolloMedicalCertifiedStatus==1)
        {
            //851
            imgSrc = "/widgets/images/Trap4.png";
            $("#TMCStatusDisplay").html("<img style='width: 22px; height: 15px;' src='" + imgSrc + "'/>");
            //end 851
            $("#TMCStatusChk").attr('checked', true);
            $("#TMCDate").html(techInfo.TrapolloMedicalCertifiedDate);
            $("#TMCDateTxt").val(techInfo.TrapolloMedicalCertifiedDate);
            $("#TMCStatusHidden").val(1);  
            $("#TMCDateHidden").val(techInfo.TrapolloMedicalCertifiedDate);         
        }
		}

         if(techInfo.NCRBadgedTechnicianStatus==1)
            {
                if($("#ClientisNCRorGPM").val()==1)
                {
                tags["NCRBTCertStatusDisplay"] = "<img style='margin-left: 5px; height:31px;' src='/widgets/images/NCRBadgedTechTag.png' class='hoverText' bt-xtitle='NCR Badged Technician'/>";
                }
                $("#NCRBadgedTechnicianStatusDisplay").html("<img src='/widgets/images/NCRBadgedTechTag.png' height='15px' />");
                $("#NCRBadgedTechnicianStatusChk").attr('checked', true);
                $("#NCRBadgedTechnicianDate").html(techInfo.NCRBadgedTechnicianDate);
                $("#NCRBadgedTechnicianDatetxt").val(techInfo.NCRBadgedTechnicianDate);
            }
            else
            {
                $("#NCRBadgedTechnicianStatusDisplay").html("");
            } 
        //371
            if (isPM || _company.toUpperCase() == "CPSS") {
            		if(techInfo.certificationsInfo['ComputerPlusCertStatus'] == 1){
            			var ComputerPlusdatestr;
            
            			if(techInfo.certificationsInfo['ComputerPlusDate'] !=''){
            				ComputerPlusdatestr = "- " + techInfo.certificationsInfo['ComputerPlusDate'];
            			}
            			else{
            				ComputerPlusdatestr='';
            			}
            
            			tags["ComputerPlusCertStatusDisplay"] = "<img style='margin-left: 5px; height:20px;' src='/widgets/images/ComputerPlusTag.png' class='hoverText' bt-xtitle='Computer Plus Certified " + ComputerPlusdatestr + "'  />";
            		}
          }
        //end 371

            //367
        if (isPM || _company.toUpperCase() == "FATT")
        {
            if(techInfo.certificationsInfo['FlextronicsRecruitCertStatus'] == 1)
            {
                var FlextronicsScreeneddatestr;

                if(techInfo.certificationsInfo['FlextronicsRecruitDate'] !='')
                {
                    FlextronicsScreeneddatestr =  ": " + techInfo.certificationsInfo['FlextronicsRecruitDate'];
                }
                else
                {
                    FlextronicsScreeneddatestr='';
                }
                tags["FlextronicsRecruitStatusDisplay"] = "<img style='margin-left: 5px; height:20px;' src='/widgets/images/FlexRecruitTag_new.png' class='hoverText' bt-xtitle='Flex ATT Recruit" + FlextronicsScreeneddatestr + "' />";
            }
        	if(techInfo.certificationsInfo['FlextronicsScreenedCertStatus'] == 1)
        {
            var FlextronicsScreeneddatestr;
            
            if(techInfo.certificationsInfo['FlextronicsScreenedDate'] !='')
            {
               FlextronicsScreeneddatestr =  ": " + techInfo.certificationsInfo['FlextronicsScreenedDate'];
            }
            else
            {
                FlextronicsScreeneddatestr='';
            }
            tags["FlextronicsScreenedStatusDisplay"] = "<img style='margin-left: 5px; height:23px;' src='/widgets/images/FlexScreenedTag_new.png' class='hoverText' bt-xtitle='Flex ATT Screened" + FlextronicsScreeneddatestr + "' />";
        }
        if(techInfo.certificationsInfo['FlextronicsContractorCertStatus']==1)
        {
            var FlextronicsContractordatestr;
            
            if(techInfo.certificationsInfo['FlextronicsContractorDate'] !='')
            {
               FlextronicsContractordatestr = ": " + techInfo.certificationsInfo['FlextronicsContractorDate'];
            }
            else
            {
                FlextronicsContractordatestr='';
            }
            tags["FlextronicsContractorStatusDisplay"] = "<img style='margin-left: 5px; height:23px;' src='/widgets/images/FlexContractorTag_new.png' class='hoverText' bt-xtitle='Flex ATT Contractor" + FlextronicsContractordatestr + "'  />";
        }
        }
        //end 367
        if (isPM || _company.toUpperCase() == "SERV") {
			if(techInfo.certificationsInfo['ServRightBrotherCertStatus']==1)
            {
                //735
                imgSrc = "/widgets/images/Bro-2.png";
                //end 735
                $("#ServRightBrotherStatusDiv").html("<img style='height:15px;' src='" + imgSrc + "'/>");
                $("#ServRightBrotherStatusInput").attr('checked', true);
                $("#ServRightBrotherHidden").val(1);
                tags["ServRightBrotherStatus3Display"] = "<img style='margin-left: 5px; height:20px;' src='/widgets/images/SRBrotag.png' class='hoverText' bt-xtitle='ServRight - Brother Certified' />";        
            }
		}
        
        if(techInfo.certificationsInfo['CSCDesksideSupportTechCertStatus'] == 1)
        {
            if(isPM || _company.substr(0,3).toUpperCase() == "CSC")
            {
            tags["CSCDesksideSupportTechStatusDisplay"] = "<img style='margin-left: 5px; height:18px;' src='/widgets/images/csc.png' class='hoverText' bt-xtitle='CSC Deskside Support Tech' />";
            $("#CSCDesksideSupportTechStatusDisplay").html("<img src='/widgets/images/csc.png' height='15px' />");
            }
            $("#CSCDesksideSupportTechStatusChk").attr('checked', true);
            $("#CSCDesksideSupportTechDate").html(techInfo.certificationsInfo['CSCDesksideSupportTechDate']);
            $("#CSCDesksideSupportTechDatetxt").val(techInfo.certificationsInfo['CSCDesksideSupportTechDate']);
        }
        if (isPM || _company.toUpperCase() == "TECF")
        {
            if(techInfo.certificationsInfo['TechForceICAgreementCertStatus'] == 1)
            {
                dateTECF = techInfo.certificationsInfo['TechForceICAgreementDate'];
		tags["TECFCert"] =  "<img id='TECFCert' height='31' src='/widgets/images/TF_Tag.png' class='hoverText' bt-xtitle='Accepted TechFORCE Independent Contractor Agreement: "+dateTECF+"'/>";
            }
        }
        var drugTestStatus = 0, bgCheckStatus = 0, bgCheckFullStatus = 0, w9Status = 0,
            interimGovtStatus = 0, fullGovtStatus = 0, topGovtStatus = 0;

        
            if(techInfo.certificationsInfo['PurpleTechnicianCertifiedCertStatus'] == 1)
            {
                var PurpleTechnicianCertifiedCertdatestr;

                if(techInfo.certificationsInfo['PurpleTechnicianCertifiedDate'] !='' && techInfo.certificationsInfo['PurpleTechnicianCertifiedDate'] !=null)
                {
                    PurpleTechnicianCertifiedCertdatestr =  ": " + techInfo.certificationsInfo['PurpleTechnicianCertifiedDate'];
                }
                else
                {
                    PurpleTechnicianCertifiedCertdatestr='';
                }
            if (_company.toUpperCase() == "PURP")
            {
                tags["PurpleTechnicianCertifiedCert"] = "<img style='margin-left: 5px; height:33px;' src='/widgets/images/Certified_Purple_Technician.png' class='hoverText' bt-xtitle='Verified Purple Technician" + PurpleTechnicianCertifiedCertdatestr + "'/ >";
            }
            else
            {
                tags["PurpleTechnicianCertifiedCert"] = "<img style='margin-left: 5px; height:33px;' src='/widgets/images/Certified_Purple_Technician.png' class='hoverText' bt-xtitle='Verified Sign Language Competency" + PurpleTechnicianCertifiedCertdatestr + "'/ >";
            }
        }
        else
        {
            if (isPM || _company.toUpperCase() == "PURP")
            {
                if(techInfo.certificationsInfo['PurpleTechnicianUnverifiedCertStatus'] == 1)
                {
                    var PurpleTechnicianUnverifieddatestr;

                    if(techInfo.certificationsInfo['PurpleTechnicianUnverifiedDate'] !='' && techInfo.certificationsInfo['PurpleTechnicianUnverifiedDate'] !=null)
                    {
                        PurpleTechnicianUnverifieddatestr =  ": " + techInfo.certificationsInfo['PurpleTechnicianUnverifiedDate'];
                    }
                    else
                    {
                        PurpleTechnicianUnverifieddatestr='';
                    }
                    tags["PurpleTechnicianUnverified"] = "<img style='margin-left: 5px; height:33px;' src='/widgets/images/Unverifired_Purple_Technician.png' class='hoverText' bt-xtitle='Unverified Purple Technician" + PurpleTechnicianUnverifieddatestr + "'/ >";
                }
            }
        }
        if (isPM || _company.toUpperCase() == "COMPU")
        {
            if(techInfo.certificationsInfo['CompuComAnalystCertStatus'] == 1)
            {
                var CompuComAnalystdatestr;

                if(techInfo.certificationsInfo['CompuComAnalystDate'] !='' && techInfo.certificationsInfo['CompuComAnalystDate'] !=null)
                {
                    CompuComAnalystdatestr =  ": " + techInfo.certificationsInfo['CompuComAnalystDate'];
                }
                else
                {
                    CompuComAnalystdatestr='';
                }
                tags["CompuComAnalystStatusDisplay"] = "<img style='margin-left: 5px; height:33px;' src='/widgets/images/C2.png' class='hoverText' bt-xtitle='CompuCom Analyst" + CompuComAnalystdatestr + "'/ >";
            }
            if(techInfo.certificationsInfo['CompuComTechnicianCertStatus'] == 1)
            {
                var CompuComTechniciandatestr;

                if(techInfo.certificationsInfo['CompuComTechnicianDate'] !='' && techInfo.certificationsInfo['CompuComTechnicianDate'] !=null)
                {
                    CompuComTechniciandatestr =  ": " + techInfo.certificationsInfo['CompuComTechnicianDate'];
                }
                else
                {
                    CompuComTechniciandatestr='';
                }
                tags["CompuComTechnicianStatusDisplay"] = "<img style='margin-left: 5px; height:33px;' src='/widgets/images/C1.png' class='hoverText' bt-xtitle='CompuCom Technician" + CompuComTechniciandatestr + "'/ >";
            }
            if(techInfo.certificationsInfo['CompuComSpecialistCertStatus'] == 1)
            {
                var CompuComSpecialistdatestr;

                if(techInfo.certificationsInfo['CompuComSpecialistDate'] !='' && techInfo.certificationsInfo['CompuComSpecialistDate'] !=null)
                {
                    CompuComSpecialistdatestr =  ": " + techInfo.certificationsInfo['CompuComSpecialistDate'];
                }
                else
                {
                    CompuComSpecialistdatestr='';
                }
                tags["CompuComSpecialistStatusDisplay"] = "<img style='margin-left: 5px; height:33px;' src='/widgets/images/C3.png' class='hoverText' bt-xtitle='CompuCom Specialist" + CompuComSpecialistdatestr + "'/ >";
            }
        }

            if(techInfo.VonagePlusStatus==1)
            {
                tags["VonagePlusStatusDisplay"] = "<img style='margin-left: 5px;' src='/widgets/images/VonagePlus.png' class='hoverText' bt-xtitle='Vonage Plus Technician' />";
                $("#VonagePlusStatusDisplay").html("<img src='/widgets/images/VonagePlusTag.png'/>");
                $("#VonagePlusStatusChk").attr('checked', true);
                $("#VonagePlusDate").html(techInfo.VonagePlusDate);
                $("#VOPDatetxt").val(techInfo.VonagePlusDate);
            }
            else
            {
                $("#VonagePlusStatusDisplay").html("");
            } 

        tags = this.loadCertificationsInfoTab(techInfo.certificationsInfo,tags);
        $.each(techInfo.credentialInfo, function(key,value)
        {
            if(key=='drugTestStatus') drugTestStatus=value;
            if(key=='bgCheckStatus') bgCheckStatus=value;
            if(key=='bgCheckFullStatus') bgCheckFullStatus=value;
            if(key=='w9Status') w9Status=value;
            if(key=='interimGovtStatus') interimGovtStatus=value;
            if(key=='fullGovtStatus') fullGovtStatus=value;
            if(key=='topGovtStatus') topGovtStatus=value;
            //14056
            if(key=='topSCIGovtStatus') topSCIGovtStatus=value;
            if(key=='NACIGovtStatus') NACIGovtStatus=value;
            if(key.search("Status") != -1)
            {
                    var divID = key.replace("Status","");
                if(divID == "flsId" || divID == "flsWhse")
                {
                            $("#"+divID+" span[id*='StatusDisplay']").html(value);
                }
                else if(divID == "flsCSP"){
                    	if(value == 1)$("#"+divID+" span[id*='StatusDisplay']").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                }
                else if(value == 1){
                    	$("#"+divID+" td[id*='Status']").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                    }else if(value == 2){
                        $("#"+divID+" td[id*='Status']").html('Unverified');//14056
                    }
                        
                    if(divID == "flsCSP")$("#flsCSPHidden").val(value);
            }
            //700 
            if(key=='drugTestDate')
            {
                if(drugTestStatus==1)
                {
                    $("#DrugPassed").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                    $("#DrugPassedDate").html("("+value+")");
                }
            }
            //end 700
            if(key=='topSCIGovtDate') {                               
                if(topSCIGovtStatus==1)
                {
                    $("#topSCIGovtStatusLabel").append($("<img />").attr("src","/widgets/images/check_green.gif")); 
                    $("#topSCIGovtDate").html(value);
                }else if(topSCIGovtStatus==2)
                    {
                       $("#topSCIGovtStatusLabel").html('Unverified');
                       $("#topSCIGovtDate").html(value);
                    }
            }           
            else if(key=='bgCheckDate') {if(bgCheckStatus==1) $("#"+key).html(value);}
            else if(key=='bgCheckFullDate') {if(bgCheckFullStatus==1) $("#"+key).html(value);}
            else if(key=='w9Date') {if(w9Status==1) $("#"+key).html(value);}
            else if(key=='interimGovtDate') {if(interimGovtStatus!=0) $("#"+key).html(value);}
            else if(key=='fullGovtDate') {if(fullGovtStatus!=0) $("#"+key).html(value);}
            else if(key=='topGovtDate') {if(topGovtStatus!=0) $("#"+key).html(value);}           
           
            else if(key.search("Date") != -1)
            {
        		var divID = key.replace("Date","");

            	if(divID == "flsCSP" || divID == "flsWhse"){
                    $("#"+divID+" span[id*="+key+"Display]").html(value);
            	}
                else{
                    $("#"+divID+" div[id*="+key+"]").html(value);
            	}
            }
            if(key=='NACIGovtDate') {                               
                if(NACIGovtStatus==1)
                {
                    imgSrc = "/widgets/images/check_green.gif";
                    $("#NACICertStatus").html("<img src='" + imgSrc + "'/>");                    
                    $("#NACIDate").html(value);
                }else if(NACIGovtStatus==2)
                    {
                       $("#NACIStatusLabel").html('Unverified');
                       $("#NACIDate").html(value);
                    }
            } // End 14056
		});

        
        //700 upper left
        if(techInfo['DatePassedSilverDrug']!="")
        {
            if(techInfo['SilverDrugPassed']==1)
            {
                $("#SilverDrugPassed").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                var partDatePassedSilverDrug = techInfo['DatePassedSilverDrug'].split(/[- :]/);
				//14108
				if(partDatePassedSilverDrug.length >= 3){
					$("#DatePassedSilverDrug").html("("+partDatePassedSilverDrug[1]+"/" + partDatePassedSilverDrug[2]+"/"+partDatePassedSilverDrug[0]+")");
				}
				//End 14108
			}
        }
        else
        {
            if(techInfo['DrugPassed']==1)
            {
                $("#SilverDrugPassed").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                var partDatePassedSilverDrug = techInfo['DatePassDrug'].split(/[- :]/);
				//14108
				if(partDatePassedSilverDrug.length >= 3){
					$("#DatePassedSilverDrug").html("("+partDatePassedSilverDrug[1]+"/" + partDatePassedSilverDrug[2]+"/"+partDatePassedSilverDrug[0]+")");
				}
                //End 14108
            }
        }
	// end 700
    }
	// for GGETechEvaluationStatusDisplay 
        if(isPM || _company.toUpperCase() == "VELO") {
            jQuery("#GGETechEvaluationDateInputFirst").attr("val",techInfo.GGE1stAttemptDate);
            jQuery("#GGETechEvaluationDateInputSecond").attr("val",techInfo.GGE2ndAttemptDate);
            if(techInfo.GGE1stAttemptStatus == "Pending") {
                jQuery("#CbxGGETechEvaluationFirst").attr("val",techInfo.GGE1stAttemptStatus);
            }
            else if(techInfo.GGE1stAttemptStatus == "Fail" || techInfo.GGE1stAttemptStatus == "Pass"){
                jQuery("#CbxGGETechEvaluationFirst").attr("val",techInfo.GGE1stAttemptScore);
            }
            if(techInfo.GGE2ndAttemptStatus == "Pending") {
                jQuery("#CbxGGETechEvaluationSecond").attr("val",techInfo.GGE2ndAttemptStatus);
            }
            else if(techInfo.GGE2ndAttemptStatus == "Fail" || techInfo.GGE2ndAttemptStatus == "Pass"){
                jQuery("#CbxGGETechEvaluationSecond").attr("val",techInfo.GGE2ndAttemptScore);
            }
	           
            if(techInfo.GGE1stAttemptStatus == "Pass" || techInfo.GGE2ndAttemptStatus == "Pass" )
            {
                var _score = 0;
                var _imgGGE = "<img  width='15px' height='15px' src='/widgets/images/gge.png'/>";
                var dateGGE = ""; 
                if(techInfo.GGE1stAttemptStatus == "Pass") {
                    jQuery("#GGETechEvaluationVals").html(techInfo.GGE1stAttemptDate);
                    _score = techInfo.GGE1stAttemptScore;
                    dateGGE = techInfo.GGE1stAttemptDate;
                }
                else if(techInfo.GGE2ndAttemptStatus == "Pass") {
                    _score = techInfo.GGE2ndAttemptScore;
                    dateGGE = techInfo.GGE2ndAttemptDate;
                    jQuery("#GGETechEvaluationVals").html(techInfo.GGE2ndAttemptDate);
                }
                tags["GGETechEvaluationStatusDisplayImg"] = "<img  width='36px' height='36px' src='/widgets/images/gge.png' class='hoverText' bt-xtitle='GGE Tech Evaluation - "+dateGGE+"'/>";
                jQuery("#GGETechEvaluationStatusDisplay").html(_imgGGE+"Pass ("+_score+"/20)");
            } else if(techInfo.GGE1stAttemptStatus == "Pending" && techInfo.GGE2ndAttemptStatus == "Pending")    
            {
                jQuery("#divGGETechEvaluationSecond").hide();
                jQuery("#GGETechEvaluationFirst").html("Grade Pending");
                jQuery("#GGETechEvaluationSecond").html("Grade Pending");
                jQuery("#divGGETechEvaluationSecond").show();
                jQuery("#GGETechEvaluationVals").html(techInfo.GGE1stAttemptDate+"<br>"+techInfo.GGE2ndAttemptDate);
            } else if(techInfo.GGE1stAttemptStatus == "Pending" && (techInfo.GGE2ndAttemptStatus == null || techInfo.GGE2ndAttemptStatus == "" ||  techInfo.GGE2ndAttemptStatus == "N/A"))    
            {
                jQuery("#divGGETechEvaluationSecond").hide();
                jQuery("#GGETechEvaluationFirst").html("Grade Pending");
                jQuery("#GGETechEvaluationVals").html(techInfo.GGE1stAttemptDate);
            } else if(techInfo.GGE1stAttemptStatus == "Fail" && (techInfo.GGE2ndAttemptStatus == null || techInfo.GGE2ndAttemptStatus == ""|| techInfo.GGE2ndAttemptStatus == "N/A"))
            {
                jQuery("#divGGETechEvaluationSecond").hide();
                jQuery("#GGETechEvaluationFirst").html("Fail ("+techInfo.GGE1stAttemptScore+"/20)");
                jQuery("#GGETechEvaluationVals").html(techInfo.GGE1stAttemptDate);
            } else if(techInfo.GGE1stAttemptStatus == "Fail" && techInfo.GGE2ndAttemptStatus == "Pending"){
                jQuery("#GGETechEvaluationFirst").html("Fail ("+techInfo.GGE1stAttemptScore+"/20)");
                jQuery("#GGETechEvaluationSecond").html("Grade Pending");
                jQuery("#GGETechEvaluationVals").html(techInfo.GGE1stAttemptDate+"<br>"+techInfo.GGE2ndAttemptDate);
            }else if(techInfo.GGE1stAttemptStatus == "Fail" && techInfo.GGE2ndAttemptStatus == "Fail"){
                jQuery("#GGETechEvaluationFirst").html("Fail ("+techInfo.GGE1stAttemptScore+"/20)");
                jQuery("#GGETechEvaluationSecond").html("Fail ("+techInfo.GGE2ndAttemptScore+"/20)");
                jQuery("#GGETechEvaluationVals").html(techInfo.GGE1stAttemptDate+"<br>"+techInfo.GGE2ndAttemptDate);
            }else{
                jQuery("#GGETechEvaluationStatusDisplay").html("");
                jQuery("#GGETechEvaluationDateInputFirst").attr("val","");
                jQuery("#GGETechEvaluationDateInputSecond").attr("val","");
            }  
            if(techInfo.GGE1stAttemptStatus == "Fail" 
                && (techInfo.GGE2ndAttemptStatus == "Fail" || techInfo.GGE2ndAttemptStatus == "Pending" ))
            {
                jQuery("#cmdGGETechEvaluationAllow").unbind("click",me.cmdGGETechEvaluationAllow).bind("click",me.cmdGGETechEvaluationAllow);
                jQuery("#cmdGGETechEvaluationAllow").attr("techid",techInfo.TechID);
                jQuery("#cmdGGETechEvaluationAllow").attr("allow",1);
            }    
        }
	if(techInfo.trainingInfo != null){
		$.each(techInfo.trainingInfo, function(key,value){

			if(key.search("CertStatus") != -1 && value == 1){
				var divID = key.replace("Status","");
				if(techInfo.trainingInfo[divID+"Num"] != "" && techInfo.trainingInfo[divID+"Num"] != null){
					$("#"+divID+" span[id*=CertNumDisplay]").html(techInfo.trainingInfo[divID+"Num"]);
				}else{
					$("#"+divID+" span[id*=CertNumDisplay]").append($("<img />").attr("src","/widgets/images/check_green.gif"));
				}

				if(techInfo.trainingInfo[divID+"Date"] != "")
					$("#"+divID+" span[id*=CertDateDisplay]").html(techInfo.trainingInfo[divID+"Date"]);
			}
		});
	}
	//830
	if(techInfo.I9Status == 1){
		$("#WorkAuthorizationValue").html("");
		$("#WorkAuthorizationStatus").html ("<img src='/widgets/images/USPending_Tag.png' width='24' height='16' class='hoverText' bt-xtitle='Authorized to perform services in the United States: 03/05/2012'/>"); //children('img').attr("src","/widgets/images/USPending_20x62.png");
		$("#WorkAuthorizationStatus").children('img').css("display","");
	}else if(techInfo.I9Status == 2){
		$("#WorkAuthorizationValue").html(techInfo.I9Date);
		$("#USAuthorized").html("<a target='_blank' href='/techs/ajax/work_authorization_pdf.php?techid="+techInfo.TechID+"'>US Verified</a>");
		$("#WorkAuthorizationStatus").html ("<img id='i9Cert2' src='/widgets/images/USAuthorized_20x62.png' height='16' class='hoverText' bt-xtitle='Authorized to perform services in the United States: 03/05/2012'/>"); //children('img').attr("src","/widgets/images/USAuthorized_20x62.png");
		$("#WorkAuthorizationStatus").children('img').css("display","");
	} 
        //end 830
        //367
//	if (isPM || _company.toUpperCase() == "FATT") {
//            if(techInfo.certificationsInfo['FlextronicsContractorCertStatus'] == 1)
//            {
//                tags["FlextronicsContractorTag"] =  "<img style='padding-left:5px;' height='25px' src='/widgets/images/Flex Contractor Tag.png'/>";
//            }    
//        }
//end 367
	if (isPM || _company.toUpperCase() == "LMS") {
            if(techInfo.certificationsInfo['LMSPlusKeyCertStatus'] == 1)
            {
                tags["LMSPlusKeyCertTag"] =  "<img style='padding-left:5px;' height='25px' src='/widgets/images/LMS Plus.png' bt-xtitle='LMS+ Key Contractor' class='hoverText'/>";
            }    
            if(techInfo.certificationsInfo['LMSPreferredCertStatus'] == 1)
            {
                tags["LMSPreferredCertTag"] =  "<img style='padding-left:5px;' height='25px' src='/widgets/images/LMS Preferred.png' bt-xtitle='LMS Preferred Technician' class='hoverText' />";
            } 
        }
	var count = 0;
	var tagHtml = "";
	for(key in tags){
		tagHtml += tags[key];
		count++;
	};

	$("#tags").html(tagHtml);
	
	if(count > 0){
		for(key in tags){
			if(key != "dellTag")$("#"+key).css("margin-left", "5px");
		};
	}

	jQuery(".hoverText").bt({
		trigger: "hover",
		titleSelector: "attr('bt-xtitle')",
		fill: 'white',
		cssStyles: {color: 'black', fontWeight: 'bold', width: 'auto'},
		animate:true
	});
        //735
        jQuery(".hoverTextnew").bt({
            titleSelector: "attr('bt-xtitle')",
            fill: 'white',
            cssStyles: {color: 'black', width: '300px'},
            animate:true
        });
        
        $("#expertinfocontrols").unbind('click').bind('click', function(){
                var content = "<div id='comtantPopupID'><div style='text-align:center;height:380px;width:900px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
                $("<div></div>").fancybox(
                {
                    'showCloseButton' :true,
                    'width' : 800,
                    'height' : "auto",
                    'content': content
                }
                ).trigger('click');

                $.ajax({
                type: "POST",
                url: "/widgets/dashboard/popup/expert-list",
                data: "",
                success:function( html ) {
                    var content = html;
                    $("#comtantPopupID").html(content);
                }
            });
        });
        //735
};

FSClientTechProfile.prototype.updateTechDenied = function(action, clientID){
	$.ajax({
		type: "POST",
		url: '/widgets/dashboard/do/update-tech-denied/',
        dataType    : 'json',
        cache       : false,
		data: {id:this._techId, companyID:this._company_id, clientID:clientID, "do":action},
		success: function (data) {
			if(action == "remove"){
				$("#deniedLabel").removeAttr("style");
			}else{
				$("#deniedLabel").css('color', "#F69630").css("font-weight", "bold");
			}
		}
	});
};

FSClientTechProfile.prototype.updateTechPreferred = function(action){
	$.ajax({
		type: "POST",
		url: '/widgets/dashboard/do/update-tech-preferred/',
        dataType    : 'json',
        cache       : false,
        data: {id:this._techId, companyID:this._company_id, "do":action},
		success: function (data) {
        	if(action == "remove"){
        		$("#preferredLabel").removeAttr("style");
        	}else{
        		$("#preferredLabel").css('color', "#F69630").css("font-weight", "bold");
        	}
		}
	});
};

FSClientTechProfile.prototype.removecheckTechPreferred = function(action){
//    this.updateTechPreferred(action);
    if(action == "remove"){
        $("#isPreferred").attr('checked',false);
        $("#preferredLabel").removeAttr("style");
    }
    
    
};
//FSClientTechProfile.prototype.loadTechSchedule = function(){
//	$.ajax({
//		type: "POST",
//		url: '/widgets/dashboard/do/get-tech-schedule/',
//        dataType    : 'json',
//        cache       : false,
//        data: {id:this._techId},
//		success: function (data) {
//        	var tableRows = "";
//        	if(data.techData.length >= 1){
//	        	$.each(data.techData,function(index,value){
//	        		tableRows += "<tr><td style='width: 140px;'>"+(value.StartDate || "")+" "+(value.StartTime || "")+"</td><td>"+(value.EndDate || "")+" "+(value.EndTime || "")+"</td><td>"+(value.Headline || "")+"</td><td>"+(value.City || "")+"</td><td style='width: 75px;'>"+(value.State || "")+"</td><td style='width: 75px;'>"+(value.Zipcode || "")+"</td></tr>";
//	        	});
//        	}else{
//        		tableRows += "<tr><td colspan='8' style='text-align: center;'>No Schedule Data</td></tr>";
//        	}
//        	$("#techSchedule tbody").html(tableRows);
//		}
//	});
//};
FSClientTechProfile.prototype.loadTechSchedule = function(titleschedule){
    var me = this;
    var content_title = titleschedule;
    var event = $("#centerContent #fstechScheduleTable2");
    var offset = 0, limit = 25, delay = 2000;
    var techid = this._techId;
    var write_row = function(index, value)
    {
            var tbody = event;
            if($(event).find("tbody").length <= 0)
            {
                $(event).append("tbody");
                
            }   
            
            tbody = $(event).children("tbody"); 

            $(tbody).append($('<tr>')
                .append($("<td width='130px'>").text((value.StartDate || "")+" "+(value.StartTime || "")))
                .append($("<td width='130px' style='text-align: left;'>").text((value.EndDate || "")+" "+(value.EndTime || "")))
                .append($("<td width='110px' style='text-align: left;'>").text((value.Headline || "")))
                .append($("<td width='110px' style='text-align: left;'>").text((value.City || "")))
                .append($("<td width='75px;'>").text((value.State || "")))
                .append($("<td width='75px;'>").text((value.Zipcode || ""))));

    };
    
    var loader = function (title, techid)
    {
        offset += limit;
        $.ajax({
                type: "POST",
                url: '/widgets/dashboard/do/get-tech-schedule/',
                dataType    : 'json',
                cache       : false,
//                async       : true,
                data: {id:techid, offset: offset, limit: limit},
                success: function (data)
                {
                    var title_reup = title;
                    var tableRows;
                    if (data.success=='success' && data.techData.length > 0 && data.techData != null && data.techData !="")
                    {
                        me._content[title_reup] = me._content[title_reup].concat (data.techData);
                        logBookRow = 0;
                        $.each(data.techData, function (index, value) {
                                write_row (index, value, title_reup);
                        });

                           setTimeout (function () {
                                    loader (title_reup, techid);
                            }, delay);
                    }
                }
        });
    };

    if (typeof(this._content[content_title]) == "undefined")
    {
        me.showLoader();
	$.ajax({
		type: "POST",
		url: '/widgets/dashboard/do/get-tech-schedule/',
        dataType    : 'json',
        cache       : false,
//            async       : true,
            data: {id:techid, offset: offset, limit: limit},
            success: function (data)
            {
                    //me._content[content_title] = "1";
                    var columnHeadings = '<th width="130px">Start Date/Time</th><th width="130px">End Date/Time</th><th width="110px">Headline</th><th width="110px">City</th><th width="75px">State</th><th width="75px">Zip</th>';
                    var title_reup = content_title;
                    var tableRows;
                    me.hideLoader();
                    me._current_target = title_reup;	//remember which table is currently loading.
                    $(event).show();
                    $(event).show();
                    var header = $(event);
                    if($(event).find("thead").length <= 0)
                    {
                        $(event).append("thead");
                    }    
                    header = $(event).children("thead");
                    jQuery(header).html(columnHeadings);
                    if (data.success == 'success' && data.techData.length > 0 && data.techData != null && data.techData !="")
                    {
                        me._content[title_reup] = data.techData;
                            logBookRow = 0;
	        	$.each(data.techData,function(index,value){
                                    write_row (index, value, title_reup);
	        	});
                            
                            setTimeout (function () {
                                    loader (title_reup, techid);
                            }, delay);
                            
                    }
                    else
                    {
                            tableRows = "<tr><td colspan='8' style='text-align: center;'>No Schedule Data</td></tr>";

        	}

//                    $("#fstechScheduleTable").hide();
//                    $("#fsLogbookHeading").parent().hide();
                    
                    
                    
                    $(event).tableScroll({height:350, containerClass:'jobHistory'});
                    if($(".tablescroll_head").length != 0)$(".tablescroll_head").css("width", "100%");
                   // if($(".tablescroll_wrapper").length != 0)$(".tablescroll_wrapper").css("width", "100%");
                    $("#centerContent #fstechScheduleHeaderCellLast").css("width", "90px");
                    
//                    $('.fstechSchedule .tablescroll_wrapper').scroll(function (e) { 
//                        alert('d');
//                    });

		}
	        	});
    }
    else
    {
                var columnHeadings = '<th width="130px">Start Date/Time</th><th width="130px">End Date/Time</th><th width="110px">Headline</th><th width="110px">City</th><th width="75px">State</th><th width="75px">Zip</th>';
		var tableRows;
		me._current_target = content_title;
		$(event).show();
		if (me._content[content_title] == null || me._content[content_title] == ""){
			tableRows = "<tr><td colspan='5' style='text-align: center;'>No Data At This Time</td></tr>";
        	}else{
			logBookRow = 0;
			$.each(me._content[content_title], function (index, value) {
				write_row (index, value, content_title);
			});
		}
                var header = $(event);
                if($(event).find("thead").length <= 0)
                {
                    $(event).append("thead");
        	}
                header = $(event).children("thead");
		$(header).html(columnHeadings);

		$(event).tableScroll({height:350, containerClass:'jobHistory'});
		if($(".tablescroll_head").length != 0)$(".tablescroll_head").css("width", "100%");
		$("#centerContent #fstechScheduleHeaderCellLast").css("width", "90px");
		me.hideLoader();
		}
    
};
//943
FSClientTechProfile.prototype.loadTech12MonthStats = function(techInfo){
	var preferenceLifetime, preferenceTotal;
	var performanceLifetime, performanceTotal;
	var backOutsLT, noShowsLT, totalsLT;	

	$.get("/ajax/techCallStats_SAT.php", {id: this._techId, companyid: this._company_id}, function(data){
		parts = data.split("|");
                //12month
		monthsimacs = parts[0];
		monthsserviceCalls = parts[1];
		
		monthsbackOuts = parts[2];
		monthsnoShows = parts[3];
	
		monthsrecommendedAvg = parts[4] == "NULL" ? 0 : parseFloat(parts[4]);
		monthsrecommendedTotal = parts[5];
	
		monthsperformanceAvg = parts[6] == "NULL" ? 0 : parseFloat(parts[6]);
		monthsperformanceTotal = parts[7];
	
		monthstotal = parseInt(monthsimacs, 10) + parseInt(monthsserviceCalls, 10);
		//12month by company
                monthscompanyimacs = parts[8];
		monthscompanyserviceCalls = parts[9];
	
		monthscompanybackOuts = parts[10];
		monthscompanynoShows = parts[11];
	
		monthscompanyrecommendedAvg = parts[12] == "NULL" ? 0 : parseFloat(parts[12]);
		monthscompanyrecommendedTotal = parts[13];
                
		monthscompanyperformanceAvg = parts[14] == "NULL" ? 0 : parseFloat(parts[14]);
		monthscompanyperformanceTotal = parts[15];
                
		monthscompanytotal = parseInt(monthscompanyimacs, 10) + parseInt(monthscompanyserviceCalls, 10);
                
                //lifetime
                lifetimeimacs = parts[16];
		lifetimeserviceCalls = parts[17];
                
		lifetimebackOuts = parts[18];
		lifetimenoShows = parts[19];
                
		lifetimerecommendedAvg = parts[20] == "NULL" ? 0 : parseFloat(parts[20]);
		lifetimerecommendedTotal = parts[21];
                
		lifetimeperformanceAvg = parts[22] == "NULL" ? 0 : parseFloat(parts[22]);
		lifetimeperformanceTotal = parts[23];
                
		lifetimetotal = parseInt(lifetimeimacs, 10) + parseInt(lifetimeserviceCalls, 10);
                //lifetime by company
                lifetimecompanyimacs = parts[24];
		lifetimecompanyserviceCalls = parts[25];
                
		lifetimecompanybackOuts = parts[26];
		lifetimecompanynoShows = parts[27];
                
		lifetimecompanyrecommendedAvg = parts[28] == "NULL" ? 0 : parseFloat(parts[28]);
		lifetimecompanyrecommendedTotal = parts[29];
                
		lifetimecompanyperformanceAvg = parts[30] == "NULL" ? 0 : parseFloat(parts[30]);
		lifetimecompanyperformanceTotal = parts[31];
                
		lifetimecompanytotal = parseInt(lifetimecompanyimacs, 10) + parseInt(lifetimecompanyserviceCalls, 10);
                //company name
                CompanyName = parts[32];
                
		$("#total12Months").html("<span title='FieldSolutions Work Orders'>"+monthstotal+"<span><span title='"+CompanyName+" Work Orders'> ("+monthscompanytotal+")<span>");
		$("#backOuts12Months").html("<span title='FieldSolutions Work Orders'>"+monthsbackOuts+"<span><span title='"+CompanyName+" Work Orders'> ("+monthsbackOuts+")<span>");
		$("#noShows12Months").html("<span title='FieldSolutions Work Orders'>"+monthsnoShows+"<span><span title='"+CompanyName+" Work Orders'> ("+monthsnoShows+")<span>");
		if(isNaN(monthsrecommendedAvg)) monthsrecommendedAvg = 0;
		$("#preference12Months").html(monthsrecommendedAvg.toFixed(0) + "% (" + monthsrecommendedTotal + ")");
		if(isNaN(monthsperformanceAvg)) monthsperformanceAvg = 0;
		$("#performance12Months").html(monthsperformanceAvg.toFixed(0) + "% (" + monthsperformanceTotal + ")");
                
                
                $("#totalLifetime").html("<span title='FieldSolutions Work Orders'>"+lifetimetotal+"<span><span title='"+CompanyName+" Work Orders'> ("+lifetimecompanytotal+")<span>");
		$("#backOutsLifetime").html("<span title='FieldSolutions Work Orders'>"+lifetimebackOuts+"<span><span title='"+CompanyName+" Work Orders'> ("+lifetimecompanybackOuts+")<span>");
		$("#noShowsLifetime").html("<span title='FieldSolutions Work Orders'>"+lifetimenoShows+"<span><span title='"+CompanyName+" Work Orders'> ("+lifetimecompanynoShows+")<span>");
		if(isNaN(lifetimerecommendedAvg)) recommendedAvg = 0;
		$("#preferenceLifetime").html(lifetimerecommendedAvg.toFixed(0) + "% (" + lifetimerecommendedTotal + ")");
		if(isNaN(lifetimeperformanceAvg)) lifetimeperformanceAvg = 0;
		$("#performanceLifetime").html(lifetimeperformanceAvg.toFixed(0) + "% (" + lifetimeperformanceTotal + ")");
                
	});
};
//end 943
FSClientTechProfile.prototype.loadTechSocialNetworks = function(techInfo){
	if(techInfo.socialNetworks != null){
		if(techInfo.socialNetworks.length >= 1){
			$.each(techInfo.socialNetworks, function(key,value){
				if(value.URL != ""){
					var link = "";
					switch(value.id){
						case "1":
							link = "http://twitter.com/";
							break;
						case "2":
							link = "http://www.facebook.com/";
							break;
						case "3":
							link = "http://www.linkedin.com/";
							break;
					}
					$("span[id="+value.name+"]").show();
					$("span[id="+value.name+"] a").attr("href", link+value.URL);
				}else{
					$("span[id="+value.name+"]").css("opacity", "0.35");
					$("span[id="+value.name+"] a").attr("target", "");
				}
			});
		}
	}else{
		$("#Twitter").css("opacity", "0.35");
		$("#Facebook").css("opacity", "0.35");
		$("#LinkedIn").css("opacity", "0.35");
		$("#Twitter a").attr("target", "");
		$("#Facebook a").attr("target", "");
		$("#LinkedIn a").attr("target", "");
	}
	
	if(techInfo["PrimaryEmail"] != ""){
		$("#Email a").attr("href","mailto:"+techInfo["PrimaryEmail"]);
		$("#Email").bt({
			titleSelector: "attr('bt-xtitle')",
			fill: 'white',
			cssStyles: {color: 'black', fontWeight: 'bold', width: 'auto'},
			animate:true
		});
	}
	
	if(techInfo["smsAddress"] != "" && techInfo['SMS_AgreeDate'] != ""){
		$("#SMS a").attr("href","mailto:"+techInfo["smsAddress"]);
		 $("#SMS").bt({
				titleSelector: "attr('bt-xtitle')",
				fill: 'white',
				cssStyles: {color: 'black', fontWeight: 'bold', width: 'auto'},
				animate:true
			});
	}else{
		$("#SMS a").attr("href","javascript:void(0);").attr("target","_self");
		$("#SMS a").css("opacity", "0.35");
	}
};
FSClientTechProfile.prototype.ArrIdCer = ["ErgoMotionKeyContractor", "ErgoMotionCertifiedTechnician","EndeavorCertified"];
FSClientTechProfile.prototype.ArrIdCerTab = ["ErgoMotionKeyContractor", "ErgoMotionCertifiedTechnician","EndeavorCertified"];

FSClientTechProfile.prototype.loadCertificationsInfoTab = function(certificationsInfo,tabs){
    $.each(this.ArrIdCerTab, function(key,value){
        var image = jQuery("#"+value).attr("image");
        var text = jQuery("#"+value).attr("text");
        var checkcp = false;
        if(jQuery("#"+value).length > 0){
            checkcp = true;
        }
       
        if ((isPM || _company.toUpperCase() == "ERG") && checkcp)
        {
            if(certificationsInfo[value+'CertStatus'] == 1)
            {
                var CraftmaticCertifieddatestr;

                if(certificationsInfo[value+'Date'] !='' && certificationsInfo[value+'Date'] !=null)
                {
                    CraftmaticCertifieddatestr =  ": " + certificationsInfo[value+'Date'];
                }
                else
                {
                    CraftmaticCertifieddatestr='';
                }
                var style = "";
                if(image == "/widgets/images/CertifiedTechnician.png"){
                    style = 'margin-left: 5px; height:33px;';
                }else{
                    style = 'margin-left: 5px; height:33px;';
                }
                tabs[value+"StatusDisplayTab"] = "<img style='"+style+"' src='"+image+"' class='hoverText' bt-xtitle='"+text + CraftmaticCertifieddatestr + "'/ >";
            }
        }
    });
    return tabs;
}
FSClientTechProfile.prototype.loadTechCertificationsNew = function(certificationsInfo){
    $.each(this.ArrIdCer, function(key,value){
       if(certificationsInfo[value+"CertStatus"] == 1){
           var Status = certificationsInfo[value+"CertStatus"];
           var Date = certificationsInfo[value+"Date"];
           var image = jQuery("#"+value).attr("image");
           if(typeof(image) == 'undefined' || image == ""){
               image = "/widgets/images/check_green.gif";
           }
           var styleimg = jQuery("#"+value).attr("styleimg");
           if(typeof(styleimg) == 'undefined'){
               styleimg = "";
           }
           if(Status == 1){
                jQuery("#"+value+"Status").html($("<img />").attr("style",styleimg).attr("src",image));
                jQuery("#"+value+"Date").html(Date);
           }
       } 
       // for special index == ?? 
    });
}
FSClientTechProfile.prototype.loadTechCertifications = function(techInfo){
	if(techInfo.certificationsInfo != null){
        var certInfo = techInfo.certificationsInfo;
        var PurpleTechCertifiedCertStatusValue = certInfo['PurpleTechnicianCertifiedCertStatus'];
        
		$.each(techInfo.certificationsInfo, function(key,value){
			if(key != "aPlusStatus"){
				if(key == "DeVryCertStatus" && value == 1){
					$("#DeVryCertNumDisplay").html("<img id='devryCert' height='15' src='/widgets/images/DeVry.png'/>");
                }else if(key=="bicsiStatus" && value==1) {
                    $("#bicsiCertIconDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                    $("#bicsiCertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif").attr("align","absmiddle"));
                }else if(key=="dellDcseStatus" && value==1) {
                    $("#dellDcseCertIconDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));    
                    $("#dellDcseCertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif").attr("align","absmiddle"));    
                }else if(key=="compTiaCertNum" && value != null && value != "") {
                    $("#compTiaCertIconDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));            
                    $("#compTiaCertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif").attr("align","absmiddle"));            
                    $("#compTiaCertNumDisplay").append(value); 
                    
                }else if(key=="ccnaStatus" && value==1) {                    
                    $("#ccnaCertIconDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));                                                   
                    $("#ccnaCertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif").attr("align","absmiddle"));
                }
                else if(key=="MCSACertStatus" && value==1) {                    
                    $("#MCSACertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif").attr("align","absmiddle"));
                }
                else if(key=="MCPCertStatus" && value==1) {
                    $("#MCPCertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif").attr("align","absmiddle"));
                }
                else if(key=="FlextronicsContractorCertStatus" && value==1) {
                    //13624
                    var _num = techInfo.certificationsInfo.FlextronicsContractorCertNum;
                    if(_num==null || _num=='null'){
                        _num='';
                    }
                    _num = jQuery.trim(_num);
                    var _html = '<table border="0">';
                        _html += '<tbody><tr>';
                        _html += '   <td style="padding-left:0px;width:44%;"></td>';
                        _html += '     <td align="center" style="text-align:left;width:5%"' + '>';  
                        _html += '             <img style="height: 19px;" src="/widgets/images/FlexContractorTag_new.png"> ';
                         _html += '      </td>';
                        _html += '     <td align="left" '+(_num != ''?'style="text-align: left;"':'')+'>';
                         _html += '        <div style="padding-top: 3px;">'+_num+'</div>';
                         _html += '     </td>';
                         _html += '      </tr></tbody></table>';
                    $("#FlextronicsContractorStatus").append(_html);   
                }else if(key=="FlextronicsScreenedCertStatus" && value==1) {
                    $("#FlextronicsScreenedStatus").append('<img style="padding-left: 4px; height: 19px;" src="/widgets/images/FlexScreenedTag_new.png">');                                                   
                }else if(key=="FlextronicsRecruitCertStatus" && value==1) {
                    $("#FlextronicsRecruitStatus").append('<img style="width: 22px; height: 19px;" src="/widgets/images/FlexRecruitTag_new.png">');                                                   
                    }else if(key=="CraftmaticCertifiedCertStatus" && value==1) {
                    $("#CraftmaticCertifiedStatus").append('<img style="width: 19px; height: 19px;" src="/widgets/images/CraftmaticTag.png">');
                }else if(key=="ComputerPlusCertStatus" && value==1) {
                    $("#ComputerPlusStatus").append($("<img />").css("width","22px").css("height","15px").attr("src","/widgets/images/ComputerPlusTag.png"));                                                   
                }
                
                else if(key=="CompuComTechnicianCertStatus" && value==1) {
                    $("#CompuComCertifiedStatus").append($("<img />").css("width","22px").css("height","15px").attr("src","/widgets/images/C1.png"));
                    $("#CompuComCertifiedtitle").html("CompuCom Technician");
                    $('#CompuComCertifiedStatusHidden').val(33);
                }
                else if(key=="CompuComAnalystCertStatus" && value==1) {
                    $("#CompuComCertifiedStatus").append($("<img />").css("width","22px").css("height","15px").attr("src","/widgets/images/C2.png"));
                    $("#CompuComCertifiedtitle").html("CompuCom Analyst");
                    $('#CompuComCertifiedStatusHidden').val(34);
                }
                else if(key=="CompuComSpecialistCertStatus" && value==1) {
                    $("#CompuComCertifiedStatus").append($("<img />").css("width","22px").css("height","15px").attr("src","/widgets/images/C3.png"));
                    $("#CompuComCertifiedtitle").html("CompuCom Specialist");
                    $('#CompuComCertifiedStatusHidden').val(35);
                }
                else if(key=="PurpleTechnicianCertifiedCertStatus" && value==1) {
                    $("#PurpleTechnicianCertifiedStatus").append($("<img />").css("width","22px").attr("src","/widgets/images/Certified_Purple_Technician.png"));
                    $("#PurpleTechnicianCertifiedtitle").html("Verified Purple Technician");
                    $('#PurpleTechnicianCertifiedStatusHidden').val(40);
                    
                    $("#VerifiedSignLanguageCompetencyStatus").append($("<img />").css("width","22px").attr("src","/widgets/images/Certified_Purple_Technician.png"));
                }
                else if(key=="PurpleTechnicianUnverifiedCertStatus" && value==1 && PurpleTechCertifiedCertStatusValue!=1) {
                    $("#PurpleTechnicianCertifiedStatus").append($("<img />").css("width","22px").attr("src","/widgets/images/Unverifired_Purple_Technician.png"));
                    $("#PurpleTechnicianCertifiedtitle").html("Unverified Purple Technician");
                    $('#PurpleTechnicianCertifiedStatusHidden').val(39);
                }
                else if(key=="TechForceICAgreementCertStatus" && value==1) {
                	$("#TechForceCredentialStatus").append($("<img />").css("width","22px").css("height","15px").attr("src","/widgets/images/TF_Tag.png"));
                }
                else if(key=="TechForceICAgreementDate") {
                    $("#TechForceCredentialDateDisplay").text (value);
                }else if(key=="mcseStatus" && value==1) {
                    $("#mcseCertIconDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif")); 
                    $("#mcseCertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif").attr("align","absmiddle")); 
                    
				}else if(key.search("CertNum") != -1){
					var divID = key.replace("Num","");
					if(value != null && value != ""){
                                                $("#"+divID+" .CertNumDisplayNew").append($("<img />").attr("src","/widgets/images/check_green.gif").attr("align","absmiddle"));
						$("#"+divID+" span[id*=CertNumDisplay]").append(value);
					}else{
						//$("#"+divID+" span[id*=CertNumDisplay]").append($("<img />").attr("src","/widgets/images/check_red.gif"));
					}
				}
                                if(value == "1" && key.search("CertStatus") != -1){
                                    var divID = key.replace("CertStatus","");
                                    switch (divID)
                                    {
                                        case "LMSPlusKey":
                                            $("#"+divID+"StatusLabel").html($("<img />").attr("src","/widgets/images/LMS Plus.png").css("height","13px").css("width","22px"));       
                                            break;
                                        case "LMSPreferred":
                                            $("#"+divID+"StatusLabel").html($("<img />").attr("src","/widgets/images/LMS Preferred.png").css("height","13px").css("width","22px"));        
                                            break;    
                                        default:
                                    $("#"+divID+"StatusLabel").append($("<img />").attr("src","/widgets/images/check_green.gif"));       
                                            break;
                                    } 
                                }
				if(key.search("Date") != ""){
					var divID = key.replace("Date", "");
					$("#"+divID+"Cert span[id*=CertDateDisplay]").html(value);
                                        $("#"+divID+"Cert span[id*=DateDisplay]").html(value);
                                        $("#"+divID+"Date").html(value);
                                        $("#"+divID+"DateDisplay").html(value);
                                        if(divID=='CompuComTechnician' || divID=='CompuComAnalyst' || divID=='CompuComSpecialist')
                                        {
                                            $("#CompuComCertifiedDateDisplay").html(value);
				}
                                        if(divID=='PurpleTechnicianUnverified' || divID=='PurpleTechnicianCertified' )
                                        {
                                            $("#PurpleTechnicianCertifiedDateDisplay").html(value);
                                            if(divID=='PurpleTechnicianCertified')
                                            {
                                                $("#VerifiedSignLanguageCompetencyValue").html(value);
				}
				}
				}
			}else{
				if(value == "1"){
					$("#aPlusCertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));
					$("#APlus").attr("checked",true);
				}
			}
		});
                this.loadTechCertificationsNew(techInfo.certificationsInfo);
	}
};

FSClientTechProfile.prototype.loadTechExpertiseInfo = function(){
	expertiseInfo = this._expertiseInfo;
	
	if(expertiseInfo != null){
		if(expertiseInfo.length >= 1){
			var starCounter = 1;
			$.each(expertiseInfo, function(key,value){
				var starArr = [];
				
				for(i=0; i<5; i++){
					var $tempInput = $('<input type="radio" />').attr('name',"star"+starCounter).attr('class','starRating').attr("disabled", "disabled").val('');
					if(value.selfRating == i+1)
						$tempInput.attr('checked', true);
					starArr.push($tempInput);
				}
				
				var newCatArr = ["28","29", "30", "31"];
				var categoryHtml = "";
				
				if($.inArray(value.Category_ID, newCatArr) > -1){
					categoryHtml = value.Category+ "<img name='New' src='/images/new.gif' alt='New'>";
				}else{
					categoryHtml = value.Category;
				}
				//735
                                var starstr = "";
                                if(value.FSExpertEnable=="1")
                                {
                                    if(value.FSExpert==1)
                                    {
                                        starstr = "<img height=\"16px\" bt-xtitle=\"FS-Expert: <b>"+value.Category+"</b>\n\
                                                    <div style='padding-left:15px;padding-top:10px;'>\n\
                                                        <ul>\n\
                                                            <li style='list-style:disc outside none;'>"+value.Content+"</li>\n\
                                                        </ul>\n\
                                                    </div>\" class=\"hoverTextnew\" src=\"/widgets/images/star.png\">";
                                    }
                                    else
                                    {
                                        starstr = "<img height=\"16px\" bt-xtitle=\"FS-Expert: <b>"+value.Category+"</b>\n\
                                                    <div style='padding-left:15px;padding-top:10px;'>\n\
                                                        <ul>\n\
                                                            <li style='list-style:disc outside none;'>"+value.Content+"</li>\n\
                                                        </ul>\n\
                                                    </div>\" class=\"hoverTextnew\" src=\"/widgets/images/disablestar.png\">";
                                    }
                                }
                                //end 735
				$('#techExpertiseTable').find("tbody")
					.append($('<tr>')
						.append($("<td>").text(value.Category))
						.append($("<td>").text(value.total))
                                                //735
                                                .append($("<td>").append(starstr))
                                                //end 735
						.append($("<td>").append.apply($("<td>"),$.isArray(starArr) ? starArr : [starArr])));
				
				starCounter++;
			});
		}
	}
        //735
	jQuery(".hoverTextnew").bt({
            titleSelector: "attr('bt-xtitle')",
            fill: 'white',
            cssStyles: {color: 'black', width: '300px'},
            animate:true
        });
        //end 735
	/*
	if(expertiseInfo != null){
		if(expertiseInfo.length >= 1){
			$.each(expertiseInfo, function(key,value){
				var starArr = [];
				if (value.selfRating == 0 || !value.selfRating)
					value.selfRating = 0;
				for(i=0; i<6; i++){
					if(i == 0){
						if(value.selfRating == 0){
							var $tempInput = $("<span/>").html("None").css("margin-left", "6px").append($('<input type="radio"/>').attr('name',value.TechSelfRatingColumn).attr('checked','checked').attr("disabled", "disabled").attr('class','starRating').val("None"));
						}else{
							var $tempInput = $("<span/>").html("None").css("margin-left", "6px").append($('<input type="radio"/>').attr('name',value.TechSelfRatingColumn).attr("disabled", "disabled").attr('class','starRating').val("None"));
						}
					}else{
						if(value.selfRating == i){
							var $tempInput = $("<span/>").html(i).css("margin-left", "6px").append($('<input type="radio"/>').attr('name',value.TechSelfRatingColumn).attr('checked','checked').attr('class','starRating').attr("disabled", "disabled").val(i));
						}else{
							var $tempInput = $("<span/>").html(i).css("margin-left", "6px").append($('<input type="radio"/>').attr('name',value.TechSelfRatingColumn).attr("disabled", "disabled").attr('class','starRating').val(i));
						}
					}
					starArr.push($tempInput);
				}
			
				var newCatArr = ["28","29", "30", "31"];
				var categoryHtml = "";
				
				if($.inArray(value.Category_ID, newCatArr) > -1){
					categoryHtml = value.Category+ "<img name='New' src='/images/new.gif' alt='New'>";
				}else{
					categoryHtml = value.Category;
				}
			
				$('#techExpertiseTable').find("tbody")
					.append($('<tr>')
						.append($("<td style='text-align: left;'>").html(categoryHtml))
						.append($("<td>").text(value.total))
						.append($("<td style='text-align: center'>").append.apply($("<td>"),$.isArray(starArr) ? starArr : [starArr])));
			});
		}
		
	}*/

	$("input.starRating").rating();
	
	this.loadTechEquipmentInfo();
};

FSClientTechProfile.prototype.loadTechEquipmentInfo = function(){
	equipmentInfo = this._equipmentInfo;
	equipmentInfo2 = this._equipmentInfo2;
	
	if(equipmentInfo != null){
		if(equipmentInfo.length >= 1){
			$.each(equipmentInfo, function(key,value){
			
				//$("#"+value).attr("checked","checked");
				$("#"+value).attr("src", "/widgets/images/checklist_true.jpg");
				
				if(value == "Ladder"){
					$("#ladderMisc").show();
				}
				if(value == "PunchTool"){
					$("#punchToolMisc").show();
				}
				if(value == "Vehicle"){
					$("#vehicleMisc").show();
				}
			});
		}
	}
	
	if(equipmentInfo2 != null){
		$.each(equipmentInfo2, function(key,value){
			if (value.Tools) $("#otherToolsLabel").css ("display", "");
			$("#Tools").html(value.Tools);
		});
	}
};
//14105
FSClientTechProfile.prototype.loadTechWebsite = function(techInfo){
        if(techInfo.MyWebSite != "" && techInfo.MyWebSite.length > 1){
           var url = techInfo.MyWebSite;
            var urls = url.split("//");
            if(urls[0]=='http:' || urls[0]=='https:'){
              $("#MyWebSite").html("<a href='"+techInfo.MyWebSite+"' target='_blank'>My Website</a>"); 
            }else{
              $("#MyWebSite").html("<a href='http://"+techInfo.MyWebSite+"' target='_blank'>My Website</a>"); 
            }
        }else{
          $("#MyWebSite").html("My Website"); 
        }
};
//End 14105
FSClientTechProfile.prototype.loadTechFiles = function(techFiles){
	
	if(techFiles != null){
		if(techFiles.length >= 1){
			$.each(techFiles, function(key,value){
					
				if(value.fileType == "Resume"){
					if(value.filePath != ""){
						$("#resumeFile").html("<a href='/widgets/dashboard/do/file-tech-documents-download/filename/"+value.encodedFilename+"/"+value.filePath+"'>Resume</a>");
						$("#contactInfoDisplay #techResume").html("<a href='/widgets/dashboard/do/file-tech-documents-download/filename/"+value.encodedFilename+"/"+value.filePath+"'>Resume</a>");
					}
				}

				if(value.fileType == "Profile Pic"){
					if(value.approved == "0"){
						$("#badgePhotoImage").attr("src","/widgets/images/profileAnonPending.jpg" );
					}else{
						$("#badgePhotoImage").attr("src","https://s3.amazonaws.com/tech-docs/profile-pics/"+value.filePath );
					}
					$("#deleteBadgePhoto").show();
					$("#badgeUpload").hide();
					$("#saveBadgePhoto").hide();
				}
				
				if(value.fileType == "Vehicle Image"){
					$("#vehicleMisc").show();
					$("#vehicleImage").attr("src", "https://s3.amazonaws.com/tech-docs/vehicle-pics/"+value.filePath);
					if(value.description != null && value.description != ""){
						$("#vehicleDesc").html(value.description);
					}
				} 
				
			});
		}
	}
};

FSClientTechProfile.prototype.showLoader = function(){
	  if(!this.loaderVisible){
		  $("div#loader").fadeIn(100);
		  this.loaderVisible = true;
	  }
};

FSClientTechProfile.prototype.hideLoader = function(){
	  if(this.loaderVisible){
		  var loader = $("div#loader");
		  loader.stop();
		  loader.fadeOut("fast");
		  this.loaderVisible = false;
	  }
};
FSClientTechProfile.prototype.loadTechCommentsInfo = function(){
	sort = '';
	if (this._commentsSort != '') sort = this._commentsSort + ' ' + this._commentsSortDir;
	$.ajax({
		type: "POST",
		url: '/widgets/dashboard/comments/get-tech-comments/',
        dataType    : 'html',
        cache       : false,
		data: {id:this._techId, sort: sort},
		success: function (data) {
			$("#techCommentsTable").html(data);
		}
	});
};

FSClientTechProfile.prototype.commentHelpful = function(id, flag){
	$.ajax({
		type: "POST",
		url: '/widgets/dashboard/comments/set-helpful/',
        dataType    : 'html',
        cache       : false,
		data: {id:id, flag: flag},
		success: function (data) {
			if (!data) return;
			if (flag === null) {
				$("#helpfulYes" + id).removeClass('helpfulHighlight');
				$("#helpfulNo" + id).removeClass('helpfulHighlight');
				$("#helpfulYes" + id).attr('onclick', '').unbind('click').click(function() {techProfile.commentHelpfulYes(id);});
				$("#helpfulNo" + id).attr('onclick', '').unbind('click').click(function() {techProfile.commentHelpfulNo(id);});
			}
			else {
				on = flag == 1 ? 'Yes' : 'No';
				off = flag == 1 ? 'No' : 'Yes';
				$("#helpful" + on + id).addClass('helpfulHighlight');
				$("#helpful" + off + id).removeClass('helpfulHighlight');
				if (on == 'Yes') {
					$("#helpful" + on + id).attr('onclick', '').unbind('click').click(function() {techProfile.commentHelpfulClear(id);});
					$("#helpful" + off + id).attr('onclick', '').unbind('click').click(function() {techProfile.commentHelpfulNo(id);});
				}
				else {
					$("#helpful" + off + id).attr('onclick', '').unbind('click').click(function() {techProfile.commentHelpfulYes(id);});
					$("#helpful" + on + id).attr('onclick', '').unbind('click').click(function() {techProfile.commentHelpfulClear(id);});
				}
			}
		}
	});
};

FSClientTechProfile.prototype.commentHelpfulClear = function(id){
	this.commentHelpful(id, null);
};

FSClientTechProfile.prototype.commentHelpfulYes = function(id){
	this.commentHelpful(id, 1);
};
FSClientTechProfile.prototype.commentHelpfulNo = function(id){
	this.commentHelpful(id, 0);
};

FSClientTechProfile.prototype.commentReportAbuse = function(id){
	if ($("#reportAbuse" + id).attr('disabled')) return;
	$.ajax({
		type: "POST",
		url: '/widgets/dashboard/comments/report-abuse/',
        dataType    : 'html',
        cache       : false,
		data: {id: id},
		success: function (data) {
			if (!data) return;
			$("<div></div>").fancybox(
				{
					'autoDimensions' : false,
					'width'	: 300,
					'height' : 125,
					'transitionIn': 'none',
					'transitionOut' : 'none',
					'content' : "Thank you for reporting the abuse.  We will look into the matter.<br/><br/>Your Field Solutions Team"
				}
			).trigger('click');
			$("#reportAbuse" + id).attr('disabled', true).addClass('reportedAbuse').attr('onclick', '').unbind('click').click(function() {techProfile.commentReportAbuse(id);});
		}
	});
};

FSClientTechProfile.prototype.loadWoCommentsInfo = function(){
	
	if (this._commentsSort != '') sort = this._commentsSort + ' ' + this._commentsSortDir;
	$.ajax({
		type: "POST",
		url: '/widgets/dashboard/comments/get-wo-comments/',
        dataType    : 'html',
        cache       : false,
		data: {id:this._techId, win: this._win, sort: sort},
		success: function (data) {
			$("#workOrderCommentsTable").html(data);
                        $("#workOrderCommentsTableSectionSeven").html(data);
		}
	});
};

FSClientTechProfile.prototype.editComments = function(id, clientID){

	if(clientID == this._client_id){
		$("<div></div>").fancybox(
			{
				'type' : 'iframe',
				'href' : '/widgets/dashboard/comments/update?id=' + id,
				'autoDimensions' : false,
				'width'	: 420,
				'height' : 300,
				'transitionIn': 'none',
				'transitionOut' : 'none'
			}
		).trigger('click');	
	}
};

FSClientTechProfile.prototype.addComments = function(win){
		if (!win) win = '';
		$("<div></div>").fancybox(
			{
				'type' : 'iframe',
				'href' : '/widgets/dashboard/comments/update?techID=' + this._techId + '&WIN_NUM=' + win,
				'autoDimensions' : false,
				'width'	: 420,
				'height' : 300,
				'transitionIn': 'none',
				'transitionOut' : 'none'
			}
		).trigger('click');			
};

FSClientTechProfile.prototype.commentsSort = function(column, type){
	sort = '';
	if (this._commentsSort == column) this._commentsSortDir =  this._commentsSortDir == 'DESC' ? 'ASC' : 'DESC';
	else {
		this._commentsSort = column;
		switch (this._commentsSort) {
			case 'skillratings':
				this.commentsSortDir = 'DESC';
			break;
			default:
				this._commentsSortDir = "ASC";
		}
	}
	if (type == 0)
		this.loadTechCommentsInfo();
	else
		this.loadWoCommentsInfo();
};

FSClientTechProfile.prototype.resetCommentsSort = function(){
	this._commentsSort = "comment";
	this._commentsSortDir = "DESC";
};

FSClientTechProfile.prototype.saveFsTrustedTech = function(){
	$.ajax({
		type: "POST",
		url: '/widgets/dashboard/do/like-tech/',
        dataType    : 'json',
        cache       : false,
		data: {id:this._techId, clientId: this._client_id},
		success: function (data) {
			$("#numLikes").html("("+data.likeData.numLikes+")");
			if(data.likeData.liked == true){
				$("#likeButton").html("<a onclick='techProfile.unlikeTech();'> Unlike</a>");
			}
		}
	});
};

FSClientTechProfile.prototype.editSectionNew = function(){
    $.each(this.ArrIdCer,function(index,value){
        var edit = jQuery("#"+value).attr("edit");
        if(typeof(edit) == 'undefined') edit = 1;
        if(edit){
            if(value == 'ErgoMotionCertifiedTechnician')
            {
            jQuery("#"+value+"Status").hide();
                jQuery("#"+value+"StatusInput").hide();
                if(jQuery.trim(jQuery("#"+value+"Status").html()) != ""){
                    jQuery("#"+value+"StatusInput").attr("checked","true");
                }
                jQuery("#"+value+"DateDisplay").hide();
                jQuery("#"+value+"DateInput").val(jQuery("#"+value+"DateDisplay").html());
                $("#"+value+"DateInput").css("display","inline").calendar({dateFormat:'MDY/'});
                jQuery("#"+value+"DateInput").hide();
            }
            else
            {
                jQuery("#"+value+"Status").hide();
            jQuery("#"+value+"StatusInput").show();
            if(jQuery.trim(jQuery("#"+value+"Status").html()) != ""){
                jQuery("#"+value+"StatusInput").attr("checked","true");
            }
            jQuery("#"+value+"DateDisplay").hide();
            jQuery("#"+value+"DateInput").show();
            jQuery("#"+value+"DateInput").val(jQuery("#"+value+"DateDisplay").html());
            $("#"+value+"DateInput").css("display","inline").calendar({dateFormat:'MDY/'});
        }
        }
    });
}
FSClientTechProfile.prototype.editSection = function(tableID){		
		if(tableID == "clientCredentials"){
        idArr = ["flsId", "flsWhse", "flsCSP", "FLS_Photo_ID", "LMSPreferred", 
                 "LMSPlusKey", "FlextronicsScreened", "FlextronicsContractor", 
                 "ServRightBrother", "FlextronicsRecruit", "CraftmaticCertified",
                 "CompuComCertified", "PurpleTechnicianCertified"];
            
            if(jQuery("#cmdGGETechEvaluationAllow").attr("allow") == 1) 
                jQuery("#divGGETechEvaluationAllow").show(); 
            jQuery("#GGETechEvaluationStatusDisplay").hide(); 
            jQuery("#GGETechEvaluationStatusLabel").show();
            jQuery("#GGETechEvaluationInput").show();
            jQuery("#GGETechEvaluationVals").hide();
            jQuery("#GGETechEvaluationDateInputFirst").val(jQuery("#GGETechEvaluationDateInputFirst").attr("val"));
            jQuery("#GGETechEvaluationDateInputSecond").val(jQuery("#GGETechEvaluationDateInputSecond").attr("val"));
            jQuery("#cmdGGETechEvaluationAllow").unbind("click",this.cmdGGETechEvaluationAllow).bind("click",this.cmdGGETechEvaluationAllow);
            $(".calendarInput").css("display","inline").calendar({dateFormat:'MDY/'});
           
            jQuery("#CbxGGETechEvaluationFirst").val(jQuery("#CbxGGETechEvaluationFirst").attr("val"));
            jQuery("#CbxGGETechEvaluationSecond").val(jQuery("#CbxGGETechEvaluationSecond").attr("val"));
            var  TMCStatus = jQuery("#TMCStatusHidden").val();  
            var  TMCDate = jQuery("#TMCDateHidden").val();  
                 
            if(TMCStatus!=null && TMCStatus==1)
            {
                $("#TMCStatusChk").attr('checked', true);                
            }

            $("#VOPDatetxt").val(jQuery("#VonagePlusDate").html());
            $("#NCRBadgedTechnicianDatetxt").val(jQuery("#NCRBadgedTechnicianDate").html());
            if(jQuery.trim(jQuery("#VonagePlusStatusDisplay").html()) != "")
            {
                $("#VonagePlusStatusChk").attr('checked', true);                
            }
            if(jQuery.trim(jQuery("#NCRBadgedTechnicianStatusDisplay").html()) != "")
            {
                $("#NCRBadgedTechnicianStatusChk").attr('checked', true);                
            }
            if(TMCDate!=null)
            {
                $("#TMCDateTxt").val(TMCDate);
            }
            
        jQuery("#CompuComCertifiedStatus").hide();
        jQuery("#CraftmaticCertifiedStatus").hide();
            jQuery("#FlextronicsRecruitStatus").hide();
            jQuery("#FlextronicsScreenedStatus").hide();
            jQuery("#FlextronicsContractorStatus").hide();
            jQuery("#FlextronicsContractorNumStatusInput").show();
            jQuery("#VonagePlusStatusDisplay").css("display","none"); 
            jQuery("#VonagePlusDate").css("display","none"); 
            jQuery("#VonagePlusStatusChk").css("display","inline"); 
            jQuery("#VOPDatetxt").css("display","inline"); 
            $("#VOPDatetxt").css("display","inline").calendar({dateFormat:'MDY/'}).val();
            
            jQuery("#NCRBadgedTechnicianStatusDisplay").css("display","none"); 
            jQuery("#NCRBadgedTechnicianDate").css("display","none"); 
            jQuery("#NCRBadgedTechnicianStatusChk").css("display","inline"); 
            jQuery("#NCRBadgedTechnicianDatetxt").css("display","inline"); 
            $("#NCRBadgedTechnicianDatetxt").css("display","inline").calendar({dateFormat:'MDY/'}).val();
            
            jQuery("#TMCStatusDisplay").css("display","none");      
            jQuery("#TMCDate").css("display","none");      
            jQuery("#TMCStatusChk").css("display","inline");      
            jQuery("#TMCDateTxt").css("display","inline");      
            jQuery("#TMCDateTxt" ).datepicker({dateFormat: 'mm/dd/yy'});
            if($("#noCSCDesksideSupportTech").val() == '1')
            {
                $("#CSCDesksideSupportTechStatusDisplay").css("display","none"); 
                $("#CSCDesksideSupportTechDate").css("display","none"); 
                $("#CSCDesksideSupportTechStatusChk").css("display","inline"); 
                if(jQuery.trim(jQuery("#CSCDesksideSupportTechStatusDisplay").html()) != "")
                {
                    $("#CSCDesksideSupportTechStatusChk").attr('checked', true);                
                }
                $("#CSCDesksideSupportTechDatetxt").val(jQuery("#CSCDesksideSupportTechDate").html());
                $("#CSCDesksideSupportTechDatetxt").css("display","inline").calendar({dateFormat:'MDY/'}).val();     
            }
          
            jQuery("#BlackBoxCablingNumInput").show();
            jQuery("#BlackBoxCablingStatus").hide();
            jQuery("#BlackBoxTelecomNumInput").show();
            jQuery("#BlackBoxTelecomStatus").hide();
            jQuery("#BlackBoxCablingInput").show();
            jQuery("#BlackBoxCablingDateShow").hide();
            jQuery("#BlackBoxTelecomInput").show();
            jQuery("#BlackBoxTelecomDateShow").hide();
                if($("#BlackBoxCablingNumInput").val()!="pending")
                {
                $("#BlackBoxCablingInput").val($.trim($("#BlackBoxCablingDateShow").html()));
                }
            else
            {
                var BlackBoxCablingnamelabel = $("#BlackBoxCablingnamelabel").html();
                var BlackBoxCablingid = $("#BlackBoxCablingid").val();
                BlackBoxCablingnamelabel = BlackBoxCablingnamelabel + " - Pending - " + $("#BlackBoxCablingStatuscol select option[value='"+BlackBoxCablingid+"']").html();
                $("#BlackBoxCablingnamelabel").html(BlackBoxCablingnamelabel);
            }
                $("#BlackBoxCablingInput").css("display","table-cell").calendar({dateFormat:'MDY/'}).val();
            $("#BlackBoxCablingNumInput").unbind('change').bind('change',function(){
                if($(this).val()!='pending' && $(this).val()!='')
                {
                    $("#BlackBoxCablingInput").val($.datepicker.formatDate('mm/dd/yy', new Date()));
                }
                else
                {
                    $("#BlackBoxCablingInput").val('');
                }
            })
            
            if($("#BlackBoxTelecomNumInput").val()!="pending")
            {
                $("#BlackBoxTelecomInput").val($.trim($("#BlackBoxTelecomDateShow").html()));
            }
            else
            {
                var BlackBoxTelecomnamelabel = $("#BlackBoxTelecomnamelabel").html();
                var BlackBoxTelecomid = $("#BlackBoxTelecomid").val();
                BlackBoxTelecomnamelabel = BlackBoxTelecomnamelabel + " - Pending - " + $("#BlackBoxTelecomStatuscol select option[value='"+BlackBoxTelecomid+"']").html();
                $("#BlackBoxTelecomnamelabel").html(BlackBoxTelecomnamelabel);
                
            }
                $("#BlackBoxTelecomInput").css("display","table-cell").calendar({dateFormat:'MDY/'}).val();
            $("#BlackBoxTelecomNumInput").unbind('change').bind('change',function(){
                if($(this).val()!='pending' && $(this).val()!='')
                {
                    $("#BlackBoxTelecomInput").val($.datepicker.formatDate('mm/dd/yy', new Date()));
                }
                else
                {
                    $("#BlackBoxTelecomInput").val('');
                }
            })

			$.each(idArr,function(index,value){
					currId = value;
					if(currId == "flsId" || currId == "flsWhse"){
						
						var certVal = $("#"+currId+"StatusDisplay").html();
						$("#"+currId+"StatusDisplay").html("");
						$("#"+currId+"StatusInput").show().val(certVal);

						if(currId == "flsWhse"){
							var dateVal = $("#"+currId+"DateDisplay").html();
							$("#"+currId+"DateDisplay").hide();
							$("#"+currId+"DateInput").css("display","inline").calendar({dateFormat:'MDY/'}).val(dateVal);
						}
						
					}else{
						currVal = $("#"+currId+"Hidden").val();
                                                if(typeof(currVal) != 'undefined')
                                                {
						(currVal == 0) ? currVal = "" : currVal = "checked";
                                                }   
                                                else
                                                {
                                                    currVal = jQuery.trim($("#"+currId+"StatusLabel").html())!= ""?"checked":"";
                                                }    

						$("#"+currId+"StatusDisplay").html("");
                                                $("#"+currId+"StatusLabel").hide();
                                                $("#"+currId+"StatusDiv").hide();
						$("#"+currId+"StatusInput").show().attr("checked", currVal);
						
						if(currId !== "FLS_Photo_ID"){
							var dateVal = $("#"+currId+"DateDisplay").html();
							$("#"+currId+"DateDisplay").hide();
							$("#"+currId+"DateInput").css("display","inline").calendar({dateFormat:'MDY/'}).val(dateVal);
						}
					}
			});
			
			$(".editSection").hide();
			$("#cancelSaveBtns").show();
            // for new GUI
            this.editSectionNew();
            //13624            
            $(".divFSTagImage").hide();
            $(".divFSTagDate").hide();
            $(".chkFSTag").show();
            $(".txtFSTagDate").show();
            $(".cbxHasMultiTagLevels").show(); // 13931
            jQuery(".txtFSTagDate" ).datepicker({dateFormat: 'mm/dd/yy'});
            //end 13624
          
		}// end of if(tableID == "clientCredentials")

		$("#"+tableID+"Header div[class='editSection']").each(function(){
			$(this).hide();
		});

		$("#"+tableID+"Header div[class='cancelSaveSection']").each(function(){
			$(this).show();
		});
                if(jQuery.trim(jQuery("#FlextronicsContractorStatus").html()) != "")
                {
                    $("#FlextronicsContractorStatusInput").attr('checked', true);                
                }
                if(jQuery.trim(jQuery("#FlextronicsScreenedStatus").html()) != "")
                {
                    $("#FlextronicsScreenedStatusInput").attr('checked', true);                
                }
                if(jQuery.trim(jQuery("#FlextronicsRecruitStatus").html()) != "")
                {
                    $("#FlextronicsRecruitStatusInput").attr('checked', true);                
                }
                if(jQuery.trim(jQuery("#CraftmaticCertifiedStatus").html()) != "")
                {
                    $("#CraftmaticCertifiedStatusInput").attr('checked', true);                
                }

    if(jQuery.trim(jQuery("#CompuComCertifiedStatusHidden").val()) != "")
    {
        $("#CompuComCertifiedStatusInput").val(jQuery("#CompuComCertifiedStatusHidden").val());
    }
    jQuery("#PurpleTechnicianCertifiedStatus").hide();
    if(jQuery.trim(jQuery("#PurpleTechnicianCertifiedStatusHidden").val()) != "")
    {
        $("#PurpleTechnicianCertifiedStatusInput").val(jQuery("#PurpleTechnicianCertifiedStatusHidden").val());
    }

    $("#FlextronicsContractorNumStatusInput").val(jQuery("#FlextronicsContractorStatus div").html());
};
FSClientTechProfile.prototype.cancelSectionNew = function(){
    $.each(this.ArrIdCer,function(index,value){
        var edit = jQuery("#"+value).attr("edit");
        if(typeof(edit) == 'undefined') edit = 1;
        if(edit){
            jQuery("#"+value+"Status").show();
            jQuery("#"+value+"StatusInput").hide();
            jQuery("#"+value+"DateDisplay").show();
            jQuery("#"+value+"DateInput").hide();
        }
    });
}
FSClientTechProfile.prototype.cancelSection = function(tableID){
	if(tableID == "clientCredentials"){
		idArr = ["flsId", "flsWhse", "flsCSP", "FLS_Photo_ID","LMSPreferred","LMSPlusKey",
                        "FlextronicsScreened","FlextronicsContractor","ServRightBrother","FlextronicsRecruit","CraftmaticCertified",
                "CompuComCertified", "PurpleTechnicianCertified"];
        //393        
        jQuery("#divGGETechEvaluationAllow").hide();
        jQuery("#GGETechEvaluationStatusDisplay").show(); 
        jQuery("#GGETechEvaluationStatusLabel").hide();
        jQuery("#GGETechEvaluationInput").hide();
        jQuery("#GGETechEvaluationVals").show();
        
        jQuery("#TMCStatusDisplay").css("display","inline");      
        jQuery("#TMCDate").css("display","inline");      
        jQuery("#TMCStatusChk").css("display","none");      
        jQuery("#TMCDateTxt").css("display","none");      
        
        jQuery("#VonagePlusStatusDisplay").show();
        jQuery("#VonagePlusDate").show();
        jQuery("#VonagePlusStatusChk").hide();
        jQuery("#VOPDatetxt").hide();
        
        jQuery("#FlextronicsRecruitStatus").show();
        jQuery("#CraftmaticCertifiedStatus").show();
        jQuery("#CompuComCertifiedStatus").show();
        jQuery("#PurpleTechnicianCertifiedStatus").show();
        jQuery("#FlextronicsScreenedStatus").show();
        jQuery("#FlextronicsContractorStatus").show();
        jQuery("#FlextronicsContractorNumStatusInput").hide();
        
        jQuery("#NCRBadgedTechnicianStatusDisplay").css("display","inline"); 
        jQuery("#NCRBadgedTechnicianDate").css("display","inline"); 
        jQuery("#NCRBadgedTechnicianStatusChk").css("display","none"); 
        jQuery("#NCRBadgedTechnicianDatetxt").css("display","none"); 
        jQuery("#CSCDesksideSupportTechStatusDisplay").css("display","inline"); 
        jQuery("#CSCDesksideSupportTechDate").css("display","inline"); 
        jQuery("#CSCDesksideSupportTechStatusChk").css("display","none"); 
        jQuery("#CSCDesksideSupportTechDatetxt").css("display","none"); 
        
        //761
        jQuery("#BlackBoxCablingNumInput").hide();
        jQuery("#BlackBoxCablingStatus").show();
        jQuery("#BlackBoxTelecomNumInput").hide();
        jQuery("#BlackBoxTelecomStatus").show();
        jQuery("#BlackBoxCablingInput").hide();
        jQuery("#BlackBoxCablingDateShow").show();
        jQuery("#BlackBoxTelecomInput").hide();
        jQuery("#BlackBoxTelecomDateShow").show();
        $("#BlackBoxCablingnamelabel").html("Black Box Cabling");
        $("#BlackBoxTelecomnamelabel").html("Black Box Telecom");
        
        //end761
        jQuery("#BlackBoxCablingNumInput").hide();
        jQuery("#BlackBoxCablingStatus").show();
        jQuery("#BlackBoxTelecomNumInput").hide();
        jQuery("#BlackBoxTelecomStatus").show();
        jQuery("#BlackBoxCablingInput").hide();
        jQuery("#BlackBoxCablingDateShow").show();
        jQuery("#BlackBoxTelecomInput").hide();
        jQuery("#BlackBoxTelecomDateShow").show();
        $("#BlackBoxCablingnamelabel").html("Black Box Cabling");
        $("#BlackBoxTelecomnamelabel").html("Black Box Telecom");

        jQuery("#BlackBoxCablingNumInput").hide();
        jQuery("#BlackBoxCablingStatus").show();
        jQuery("#BlackBoxTelecomNumInput").hide();
        jQuery("#BlackBoxTelecomStatus").show();
        jQuery("#BlackBoxCablingInput").hide();
        jQuery("#BlackBoxCablingDateShow").show();
        jQuery("#BlackBoxTelecomInput").hide();
        jQuery("#BlackBoxTelecomDateShow").show();
        $("#BlackBoxCablingnamelabel").html("Black Box Cabling");
        $("#BlackBoxTelecomnamelabel").html("Black Box Telecom");

			$.each(idArr,function(index,value){
				currId = value;
				if(currId == "flsId" || currId == "flsWhse"){
					
					var certVal = $("#"+currId+"StatusInput").val();
					$("#"+currId+"StatusDisplay").html(certVal);	
					$("#"+currId+"StatusInput").hide();

					if(currId == "flsWhse"){
						var dateVal = $("#"+currId+"DateInput").val();
						$("#"+currId+"DateInput").hide();
						$("#"+currId+"DateDisplay").show().html(dateVal);
					}
					
				}else{
					currVal = $("#"+currId+"Hidden").val();
                                       
                                        $("#"+currId+" .StatusLabel").hide();
					$("#"+currId+"StatusInput").hide();
                                        $("#"+currId+"StatusLabel").show();
                                        $("#"+currId+"StatusDiv").show();
					if(currVal == 1)  $("#"+currId+"StatusDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));
					
					if(currId !== "FLS_Photo_ID"){
						var dateVal = $("#"+currId+"DateInput").val();
						$("#"+currId+"DateInput").hide();
						$("#"+currId+"DateDisplay").show().html(dateVal);
					}
					}
		});
			$(".editSection").show();
			$("#cancelSaveBtns").hide();
                this.cancelSectionNew();     
            //13624
            $(".divFSTagImage").show();
            $(".divFSTagDate").show();
            $(".chkFSTag").hide();
            $(".txtFSTagDate").hide();             
            //end 13624        
            $(".cbxHasMultiTagLevels").hide();  //13931
	} //end of if(tableID == "clientCredentials")
};

FSClientTechProfile.prototype.saveTechInfo = function(form){
	var submitArr = [];
	var submitString = "";
	me = this;
	$.each($(":input", "#"+form),function(index,value)
        {
            if($(this).attr("type") == "checkbox")
            {
                if($(this).attr("checked") !== false)
                {
                    submitArr[$(this).attr("name")] = "1";
		}
                else
                {
                    submitArr[$(this).attr("name")] = "0";
		}
            }else if($(this).attr("type") !== "hidden")
            {
                submitArr[$(this).attr("name")] = $(this).val();
		if( $(this).attr("name") == 'FLSID')
                {
                    submitArr['FLSBadge'] = $(this).val() != "" ? "1" : "0";
                    submitArr['FLSstatus'] = $(this).val() != "" ? "Registered" : "Trained";
                }
            }
            if($(this).attr("name")=="BlackBoxCablingid" || $(this).attr("name")=="BlackBoxTelecomid")
            {
                submitArr[$(this).attr("name")] = $(this).val();
            }
            if($(this).attr("name")=="BlackBoxCablingid" || $(this).attr("name")=="BlackBoxTelecomid")
            {
                submitArr[$(this).attr("name")] = $(this).val();
            }
            if($(this).attr("name")=="BlackBoxCablingid" || $(this).attr("name")=="BlackBoxTelecomid")
            {
                submitArr[$(this).attr("name")] = $(this).val();
            }
	});

	for(index in submitArr){
		submitString += index+"="+submitArr[index]+"&";
	}
	submitString += "techID="+this._techId;
        
    var TMCDate = $("#TMCDateTxt").val();
    if(TMCDate!=null && TMCDate!='')
    {
        var isValid = true;
        try {
            $.datepicker.parseDate('mm/dd/yy', TMCDate);
        } catch (e) {
            isValid =false;
        };
        if(!isValid)        
        {            
            alert('Trapollo Medical Certified Date is invalid.');
            return;
        }
            
    }    
	me.showLoader();
	
     
	$.ajax({
		type: "POST",
		url: "/techs/ajax/saveBusinessProfile.php",
        dataType    : 'json',
        cache       : false,
		data: submitString,
		success: function (data) {
			me.hideLoader();
			window.location.reload();
		},
		error: function (xhr, status, error) {
			me.hideLoader();
			//console.log (xhr, status, error);
		}
	});

};

FSClientTechProfile.prototype.cmdGGETechEvaluationAllow = function() {
    FSClientTechProfile.prototype.showLoader();
    $.ajax({
        type: "POST",
        url: "/widgets/dashboard/do/gge-tech-evaluation-allow/",
        dataType    : 'json',
        cache       : false,
        data: "techID="+jQuery(this).attr("techid"),
        success: function (data) {	
            jQuery("#cmdGGETechEvaluationAllow").hide();
            jQuery("#CbxGGETechEvaluationSecond").val("");
            jQuery("#CbxGGETechEvaluationSecond").attr("val","");
            FSClientTechProfile.prototype.hideLoader();
            /*var messageHtml = "<h2>Update Complete</h2>";
            messageHtml += "<p>Allow another attempt has been successfully updated.</p>";
            messageHtml += "<b>Thank you!</b>";
            FSClientTechProfile.prototype.applyFancybox(messageHtml);*/
        }
    });
}
FSClientTechProfile.prototype.applyFancybox = function(message){
    $("<div></div>").fancybox({
        'width': 350,
        'height' : 125,
        'content' : message,
        'autoDimensions' : false,
        'centerOnScroll' : true
    }).trigger('click');
}
