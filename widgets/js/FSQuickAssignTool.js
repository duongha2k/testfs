function FSQuickAssignTool( wd ) {
    //if ( !(wd instanceof FSWidgetDashboard) )   wd = null;

    this._wd          = wd;
    this._winNum      = null;
    this._techID      = null;
    this._toolInited  = false;
    this.initTool();
}

/**
 *
 */
FSQuickAssignTool.prototype.getWidgetHtml = function ( ) {
    var roll    = arguments[0];
    var params  = arguments[1] || {};

    if (this._wd != null) this._wd.getWidgetHtml(roll, params);
}

/**
 *
 */
FSQuickAssignTool.prototype.initEvents = function ( ) {
    $('#AssignTechName').empty();
    $('#AssignTechEmail').empty();
    $('#AssignTechEmail2Container').css('display','none');
    $('#AssignTechEmail2').empty();
    $('#AssignTechPhone').empty();
    $('#AssignTechPhone2Container').css('display','none');
    $('#AssignTechPhone2').empty();
    $('#MaxPay').empty();
}
/**
 *
 */
FSQuickAssignTool.prototype.reload = function ( ) {
    self._wd.show({
        tab:self._wd.currentTab,
        params:self._wd.getParams()
        });
}

/**
 *
 */
FSQuickAssignTool.prototype.assign = function (reload) {
    reload = reload||false;
    var techs = {};
    var agreeds = {};
    var amountsrep = {};
    var qtydevices = {};

    var company = pmContext != undefined ? pmContext : "";

    winNum = $('#Assign_WIN_NUM').val();
    tech = $('#techId').val();
    agreed = $('#PayMax').val();
    amountper = $('#Amount_Per').val();
    qtydevice = $('#Qty_Devices').val();

    techs[winNum] = tech;
    agreeds[winNum] = agreed;
    amountsrep[winNum] = amountper;
    qtydevices[winNum] = qtydevice;
    progressBar.key('assign');
    progressBar.run();
    var self = this;
    $.getJSON("/widgets/dashboard/do/assign",{
        techs:techs, 
        agreeds:agreeds, 
        amountsrep:amountsrep, 
        qtydevices:qtydevices, 
        company:company, 
        assignClientDeniedTech:true
    },function(data){
        if (data.error == 1) {
            $('#woErrorMsg').html(data.msg);
            $('#woErrorMsg').show('fast',function(){
                setTimeout(function () {
                    $('#woErrorMsg').hide(500);
                }, 4000);
            });
        } else {
            self._wd.show();
            roll.hide();
        }
    });
}

FSQuickAssignTool.prototype.getMassAssignInfo = function () {
    var techs = {};
    var agreeds = {};
	var wins = [];
    $.each($('#widgetContainer input[name="assignCheckBox"]'), function (idx, item){
	if ($(item).attr('checked'))
		wins.push($(item).val());
    });
	
    $.each($('#widgetContainer input[name="publishedWinNum"]'), function (idx, item){
        winNum = $(item).val();
        tech   = $('#'+winNum+'_TechId').val();
        agreed = $('#'+winNum+'_TechAgreed').val();

        if(tech != '' && agreed != '') {
            techs[winNum]   = tech;
            agreeds[winNum] = agreed;
        }
    });
	
	return {techs: techs, agreeds: agreeds, wins: wins};
}

FSQuickAssignTool.prototype.massAssign = function ( callBack ) {

    var company = pmContext != undefined ? pmContext : "";

	info = this.getMassAssignInfo();
	techs = info['techs'];
	agreeds = info['agreeds'];
    if(techs.length == 0 || agreeds.length ==0) return;

    progressBar.key('assign');
    progressBar.run();
    var self = this;
	
	if (callBack == undefined) {
		callBack = function(data){
	        self._wd.show({
	            tab:self._wd.currentTab,
	            params:self._wd.getParams()
		    });
		};
	}

    $.getJSON("/widgets/dashboard/do/assign",{
        techs:techs, 
        agreeds:agreeds, 
        company: company
    }, callBack
    );
}

FSQuickAssignTool.prototype.massPublish = function () {
	var wos = [];
	$.each($('.rePublishCheckbox'), function(idx, item) {
		if (item.checked) wos.push($(item).val());
	});

	var company = pmContext != undefined ? pmContext : "";

       	var publishOptions = {};
	var wosCount = wos.length;
	for (var i = 0; i < wosCount; i++) {
		var wId = wos[i];
		var value = $("#rePublishOptions_" + wId).val();
		if (value) publishOptions[wId] = value;
	}

    if ( wos.length ) {
        progressBar.key('publish');
        progressBar.run();
        var self = this;
        $.ajax({
            url         : '/widgets/dashboard/do/publish/',
            dataType    : 'json',
            data        : {
                wo_ids : wos, 
                publishOptions: publishOptions, 
                company: company
            },
            cache       : false,
            type        : 'post',
            context     : this._wd,
            success     : function (data, status, xhr) {
                self._wd.show({
                    tab:this.currentTab, 
                    params:self._wd.getParams()
                    });
            },
            error       : function ( xhr, status, error ) {
                self._wd.show({
                    tab:this.currentTab, 
                    params:self._wd.getParams()
                    });
            }
        });
    }
}

/**
 *
 */
FSQuickAssignTool.prototype.initTool = function () {
    this._toolInited = true;
}

/**
 *
 */
FSQuickAssignTool.prototype.getTechInfo = function (el) {
    techId = $(el).val();
    this['shownClientDeniedMsg'] = false;
    if (techId == '' || techId == 'undefined') return;
    $('#'+el.id+'_Progress').removeClass("displayNone");
    $('#AssignTechName').empty();
    $('#AssignTechEmail').empty();
    $('#AssignTechEmail2Container').css('display','none');
    $('#AssignTechEmail2').empty();
    $('#AssignTechPhone').empty();
    $('#AssignTechPhone2Container').css('display','none');
    $('#AssignTechPhone2').empty();

    // get techInfo
    $.getJSON("/widgets/dashboard/popup/get-tech-info/", {
        tech: techId, 
        company : this._wd.filters().company()
        }, function(data) {
        $('#'+el.id+'_Progress').addClass("displayNone");
      
        if (data.error == 1) {
            $('#techErrorMsg').html(data.msg);
            $('#techErrorMsg').show('fast',function(){
                setTimeout(function () {
                    $('#techErrorMsg').hide(500);
                }, 3000);
            });
        } else {
        	
        	if (data.techInfo.isDenied && this['shownClientDeniedMsg'] != true) {
        		this._redirect = 0;
        		this['shownClientDeniedMsg'] = true;
        		$('#techErrorMsg').html('You are about to assign work to a technician that has been "denied access" by your company. Please re-confirm your choice of technician. Tech ID: '+techId+' ('+data.techInfo.fName+' '+data.techInfo.lName+')');
                $('#techErrorMsg').show('fast',function(){
                    setTimeout(function () {
                        $('#techErrorMsg').hide(500);
                    }, 3000);
                });
            } 
            $('#AssignTechName').html(data.techInfo.fName+' '+data.techInfo.lName);
            $('#AssignTechEmail').html(data.techInfo.email);
            if (data.techInfo.email2) {
	            $('#AssignTechEmail2Container').css('display','');
	            $('#AssignTechEmail2').html(data.techInfo.email2);
            }else{
	            $('#AssignTechEmail2Container').css('display','none');
	            $('#AssignTechEmail2').empty();
            }
            detailObject.onInit.RequitePhone("#AssignTechPhone"
                , data.techInfo.phone
                ,data.techInfo.TechID
                ,data.techInfo.PrimaryPhoneStatus
                ,1
                ,'html'
                ,Base64.encode(data.techInfo.fName + ' ' +data.techInfo.lName));

            if (data.techInfo.phone2) {
	            $('#AssignTechPhone2Container').css('display','');
                detailObject.onInit.RequitePhone("#AssignTechPhone2"
                    , data.techInfo.phone2
                    ,data.techInfo.TechID
                    ,data.techInfo.SecondaryPhoneStatus
                    ,2
                    ,'html'
                    ,Base64.encode(data.techInfo.fName + ' ' +data.techInfo.lName));
            }else{
                $('#AssignTechPhone2Container').css('display','none');
                $('#AssignTechPhone2').empty();
            }
        }
    });
}
/**
 *
 */
FSQuickAssignTool.prototype.showTechInfo = function (el) {
    techId = $(el).val();
    $('#'+el.id+'_Box').css('display', 'block');
    $('#'+el.id+'_Html').empty();
    $('#'+el.id+'_Progress').css('display', 'block');
    // get techInfo
    $.getJSON("/widgets/dashboard/popup/get-tech-info/", {
        tech: techId, 
        company: this._wd.filters().company()
        }, function(data){
        $('#'+el.id+'_Progress').css('display', 'none');
        if (data.error == 1) {
            $('#'+el.id+'_Html').html(data.msg);
        } else {
            $('#'+el.id+'_Html').html('First Name: '+data.techInfo.fName+'<br/>Last Name: '+data.techInfo.lName);
        }
    });
    $('#'+el.id+'_Html').empty();
    setTimeout(function () {
        $('#'+el.id+'_Box').css('display', 'none');
    }, 3000);
}
/**
 *
 */
FSQuickAssignTool.prototype.getWoInfo = function (el) {
    elVal  = el.value;
    elName = el.name;
    if (elVal == '' || elVal == 'undefined') return null;
    $('#'+el.id+'_Progress').removeClass("displayNone");
    // get first & last names for tech
    $this = this;
    $.getJSON("/widgets/dashboard/popup/get-wo-info/?"+elName+"="+elVal, {
        company:wd.filters().company()
        }, function(data){
        $('#'+el.id+'_Progress').addClass("displayNone");
        if (data.error == 1) {
            $('#woErrorMsg').html(data.msg);
            $('#woErrorMsg').show('fast',function(){
                setTimeout(function () {
                    $('#woErrorMsg').hide(500);
                }, 3000);
            });
        } else {
            if(data.woInfo.WIN_NUM != '' && data.woInfo.WIN_NUM != 'undefined')
                $('#Assign_WIN_NUM').val(data.woInfo.WIN_NUM);
            if(data.woInfo.WO_ID != '' && data.woInfo.WO_ID != 'undefined')
                $('#Assign_WO_ID').val(data.woInfo.WO_ID);
            if(data.woInfo.PayMax != '' && data.woInfo.PayMax != 'undefined')
                $('#PayMax').val(data.woInfo.PayMax);
            if(data.woInfo.Amount_Per != '' && data.woInfo.Amount_Per != 'undefined') {
                $('#Amount_Per option').removeAttr('selected');
                $("#Amount_Per option[value='"+data.woInfo.Amount_Per+"']'").attr('selected','selected');
            
                if(data.woInfo.Qty_Devices != '' && data.woInfo.Qty_Devices != 'undefined' && data.woInfo.Amount_Per=='Device') {
                    $this.changeAmountPer('Device');
                }
            }
        }
    });
}
FSQuickAssignTool.prototype.changeAmountPer = function (id) {
    if(id == 'Device'){
        if(document.getElementById('Qty_Devices') != null) return;
        var t = document.createElement("input");
        t.type = "text";
        t.maxLength = 4;
        t.size = 4;
        t.id = "Qty_Devices";
        t.name = "Qty_Devices";

        var tLabel = document.createElement("label");
        tLabel.id = 'Qty_Devices_label';
        tLabel.innerHTML = ' Device Quantity ';

        p = document.getElementById("quickAssignAgreed");
        p.appendChild(tLabel);
        tLabel.appendChild(t);
    } else {
        if($('#Qty_Devices')) $('#Qty_Devices_label').remove();
    }
}
FSQuickAssignTool.prototype.getApproveWoInfo = function (el) {
	techPayUtils = new FSTechPayUtils();

	elVal  = el.value;
    elName = el.name;
    if (elVal == '' || elVal == 'undefined') return null;
    $('#'+el.id+'_Progress').removeClass("displayNone");
    
    $.getJSON("/widgets/dashboard/popup/get-full-wo-info/?"+elName+"="+elVal, {
        company:wd.filters().company()
        }, function(data){
console.log (data);
        $('#'+el.id+'_Progress').addClass("displayNone");
        if (data.error == 1) {
            $('#woErrorMsg').html(data.msg);
            $('#woErrorMsg').show('fast',function(){
                setTimeout(function () {
                    $('#woErrorMsg').hide(500);
                }, 3000);
            });
        } else {
            if(data.woInfo.WIN_NUM != '' && data.woInfo.WIN_NUM != 'undefined')
                $('#Approve_WIN_NUM').val(data.woInfo.WIN_NUM);
            if(data.woInfo.WO_ID != '' && data.woInfo.WO_ID != 'undefined')
                $('#Approve_WO_ID').val(data.woInfo.WO_ID);

            switch (data.woInfo.Status) {
            case 'created':
            case 'published':
                $('#woErrorMsg').html("This work order is not assigned");
                $('#woErrorMsg').show('fast',function(){
                        setTimeout(function () {
                            $('#woErrorMsg').hide(500);
                        }, 3000);
                });
            	return;
            case 'in accounting':
            case 'completed':
            case 'deactivated':
                $('#woErrorMsg').html("This work order has been completed");
                $('#woErrorMsg').show('fast',function(){
                        setTimeout(function () {
                            $('#woErrorMsg').hide(500);
                        }, 3000);
                });
            	return;
            }

	    data.woInfo.calculatedTechHrs = (data.woInfo.calculatedTechHrs === null || data.woInfo.calculatedTechHrs === undefined) ? "" : data.woInfo.calculatedTechHrs;
	    data.woInfo.Add_Pay_Reason = (data.woInfo.Add_Pay_Reason === null || data.woInfo.Add_Pay_Reason === undefined) ? "" : data.woInfo.Add_Pay_Reason;
	    data.woInfo.Amount_Per = (data.woInfo.Amount_Per === null || data.woInfo.Amount_Per === undefined) ? "" : data.woInfo.Amount_Per;
        $("#Approve_Qty_Visits").val(data.woInfo.Qty_Visits);    
        if(data.woInfo.Amount_Per=='Visit')
        {
            $("#Approve_PayMax").text(data.woInfo.PayMax + ' / Visit' );
            $("#Approve_Tech_Bid_Amount").text(techPayUtils.formatAmount(data.woInfo.Tech_Bid_Amount) + ' / Visit' );
            //jQuery("#htmlApproveTotalHours").html("# of Visits:");
            
            $("#Approve_Qty_Devices").show();
            $("#Approve_calculatedTechHrs").hide();
            $("#Approve_Qty_Visits").removeAttr("disabled");
        }
        else if(data.woInfo.Amount_Per=='Device')
        {
            jQuery("#htmlApproveTotalHours").html("Device Quantity:");
            $("#Approve_Qty_Devices").val(data.woInfo.Qty_Devices);
            $("#Approve_Qty_Devices").show();
            $("#Approve_calculatedTechHrs").hide();
            $("#Approve_Qty_Visits").attr("disabled", "disabled");
        }  
        else
        {
           jQuery("#htmlApproveTotalHours").html("Total Hours:"); 
           $("#Approve_calculatedTechHrs").val(data.woInfo.calculatedTechHrs);
           $("#Approve_Qty_Devices").hide();
            $("#Approve_calculatedTechHrs").show();
            $("#Approve_Qty_Visits").attr("disabled", "disabled");
        }    
        if(data.woInfo.Amount_Per!='Visit')
        {
            $("#Approve_PayMax").text(data.woInfo.PayMax + (data.woInfo.Amount_Per? ' / ' + data.woInfo.Amount_Per: ''));            
            $("#Approve_Tech_Bid_Amount").text(techPayUtils.formatAmount(data.woInfo.Tech_Bid_Amount) + (data.woInfo.Amount_Per? ' / ' + data.woInfo.Amount_Per: ''));

        }
            
            
            
            $("#Approve_baseTechPay").val(techPayUtils.formatAmount(data.woInfo.baseTechPay));
            //863
            $("#Approve_ExpenseReporting").html(data.ExpenseReporting);
            //end 863
            $("#Approve_OutofScope_Amount").val(techPayUtils.formatAmount(data.woInfo.OutofScope_Amount));
            $("#Approve_TripCharge").val(techPayUtils.formatAmount(data.woInfo.TripCharge));
            //$("#Approve_TripCharge").val(techPayUtils.formatAmount(data.woInfo.Approve_TripCharge));
            $("#Approve_MileageReimbursement").val(techPayUtils.formatAmount(data.woInfo.MileageReimbursement));
            $("#Approve_MaterialsReimbursement").val(techPayUtils.formatAmount(data.woInfo.MaterialsReimbursement));
            $("#Approve_Additional_Pay_Amount").val(techPayUtils.formatAmount(data.woInfo.Additional_Pay_Amount));
            $("#Approve_AbortFeeAmount").val(techPayUtils.formatAmount(data.woInfo.AbortFeeAmount));
            
            $("#Approve_PayAmount").val(data.woInfo.PayAmount);
            //376
            var Approved_ReimbursableExpenseThroughFSApproved=$("#Approved_ReimbursableExpenseThroughFSApproved").html();
            var totalPayAmountOut = 0;  
            try
            {
                totalPayAmountOut=parseFloat(Approved_ReimbursableExpenseThroughFSApproved) ;
            }
            catch(ex)
            {
                totalPayAmountOut=0;
            }
            if(isNaN(totalPayAmountOut))              
                totalPayAmountOut=0;
            var PayAmount=parseFloat(data.woInfo.PayAmount);
            if(isNaN(PayAmount))
                PayAmount=0;
            totalPayAmountOut = totalPayAmountOut + PayAmount;
            totalPayAmountOut=totalPayAmountOut.toFixed(2);    
            $("#Approve_PayAmount_Out").html(totalPayAmountOut);
            //end 376
            
            $("#Approve_Add_Pay_Reason").val(data.woInfo.Add_Pay_Reason);
            $("#Approve_company").val(wd.filters().company());
            $("#Approve_Amount_Per").val(data.woInfo.Amount_Per);
            if(data.woInfo.AuditNeeded == 1)
                {
                    jQuery("#AuditNeeded1").attr("checked",true);
                    jQuery("#AuditComplete1").removeAttr('disabled');
                }
                //376
            if(data.woInfo.AuditComplete == 1) jQuery("#AuditComplete1").attr("checked",true);
            $("#Approve_approveForm .calculate").change(function() {
                    FSQuickAssignTool.prototype.setApproveCal(this);
            });
        }
    });
}
FSQuickAssignTool.prototype.setApproveCal = function(el)
{
    var thisObj = $(el);
    if (!thisObj.hasClass('noformat')) {
            thisObj.val(techPayUtils.formatAmount(thisObj.val()));
    }

    var values = {};
    $("#Approve_approveForm .calculate").each(function(index, element) {
            values[$(element).attr('name')] = $(element).val();
    });

    var payAmount = techPayUtils.calculate(values);
    var ReiApproved = 0;
    var  Approved_ReimbursableExpenseThroughFSApproved=jQuery("#Approved_ReimbursableExpenseThroughFSApproved").html();    
    ReiApproved = parseFloat(Approved_ReimbursableExpenseThroughFSApproved);
    if(isNaN(ReiApproved))              
                ReiApproved=0;
    var nPayAmount=parseFloat(payAmount);
    if (isNaN(nPayAmount))
        nPayAmount=0;
    payAmount = nPayAmount+ ReiApproved;
    payAmount=payAmount.toFixed(2);
    $("#Approve_PayAmount").val(payAmount);
    $("#Approve_PayAmount_Out").html(techPayUtils.formatAmount(payAmount).valueOf());
}
//376
