/**
 * FSTechMapper
 *
 * @param Object|null params
 * @access public
 * @return void
 */
function FSTechMapper ( params ) {
    FSWidgetDashboard.call(this, params);

    this.params.POPUP.avaliablePopups = {};
    this.params.WIDGET.avaliableTabs = {
    	client: {module:'dashboard',controller:'do',action:'get-tech-info'}
    };
    
	switch (this.params.tab) {
		case 'client':
			this._type = 'client';
			break;
	}
	
	try {
		this._win = this.params.win;
	}
	catch (e) {
		this._win = '';
	}
	
	this._goomap = "";
	this._mapContainer = this.params.mapContainer;
	this._techMarkers = [];
	this._techMarkerIcon = '/images/map/tech_marker_green_one.png';
	this._woMarkerIcon = '/images/map/wo_marker_blue_one.png';
	this._woClusterIcon = '/images/map/wo_cluster_blue.png';
	this._techClusterIcon = '/images/map/wo_cluster_green.png';
	this._woMarkers = [];
	this._woMarkerCluster;
	this._techMarkerPopupContent = {};
	this._woMarkerPopupContent = [];
	this._markerPopups = [];
	this._currentPopup;
	this._companyID = this.params.companyID;
	this._polygon;
	this._polygonPath = "";
	this._polygonMarkers = [];
	this._polygonClickListener = "";
	this._currSearchParams = "";
	this._allEmailList;
	this._isFls = this.params.isFls;
	this._allProjects = "";
	this._loadMoreTechsPage = "";
	this._loadMoreTechsShowing = "";
	this._woViewCount = "";
	this._projectMappingOption1 = false;
	this._mapAroundWosPage = "";
	this._mappedAroundWos = false;
	this.getAllProjects();
	this._projectMapped = false;
	this._clusterClicked = false;
	
	
};

FSTechMapper.prototype = new FSWidgetDashboard();
FSTechMapper.prototype.constructor = FSTechMapper;

FSTechMapper.NAME          = 'FSTechMapper';
FSTechMapper.VERSION       = '0.1';
FSTechMapper.DESCRIPTION   = 'Class FSTechMapper';

FSTechMapper.prototype.initMap = function(dashboardWos){
	var initMapOptions = {
			zoom: 4,
			center: new google.maps.LatLng(38, -97),
			panControl: true,
			zoomControl: true,
			scaleControl: true,
			streetViewControl: false,
			mapTypeControl: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	
    this._goomap = new google.maps.Map(document.getElementById(this._mapContainer), initMapOptions);

    if(dashboardWos !== "" && dashboardWos !== null && dashboardWos !== undefined){
    	me = this;
    	google.maps.event.addListenerOnce(this._goomap, 'idle', function(){
    		me.mapWorkOrders(false, dashboardWos);
    	});
    }
    
    mcOptions = {styles: [{
 		height: 37,
 		url: this._techClusterIcon,
 		width: 26
 		},
 		{
 		height: 37,
 		url: this._techClusterIcon,
 		width: 26
 		},
 		{
 		height: 37,
 		url: this._techClusterIcon,
 		width: 26
 		},
 		{
 		height: 37,
 		url: this._techClusterIcon,
 		width: 26
 		},
 		{
 		height: 37,
 		url: this._techClusterIcon,
 		width: 26
 		}], maxZoom: '11', gridSize: 20};
	this._techMarkerCluster = new MarkerClusterer(this._goomap, "", mcOptions);
	google.maps.event.addListener(this._techMarkerCluster, 'clusterclick',function(){me._clusterClicked = true;});
};

FSTechMapper.prototype.getLatLng = function(results){
	var geocoderGeoComponent;
	for (var i in results) {
		geocoderGeoComponent = results[i].geometry;
		latLng = geocoderGeoComponent['location'];
		return latLng;
	}
};

FSTechMapper.prototype.clearTechMarkers = function(){
	me = this;
	
	this._mapAroundWosPage = "";
	this._loadMoreTechsPage = "";
	this._loadMoreTechsShowing = "";
	
	
	if(this._techMarkerCluster){
		this._techMarkerCluster.clearMarkers();
	}
	
	if(this._techMarkers.length >= 1){
		if(me._currentPopup != null){
			me._currentPopup.close();
			me._currentPopup = null;
		}
		$.each(me._techMarkers,function(index,marker){
			marker.setMap(null);
			marker = null;
		});
		me._techMarkers = [];
	}
	
};

FSTechMapper.prototype.clearWoMarkers = function(){
	me = this;
	if(this._woMarkerCluster){
		this._woMarkerCluster.clearMarkers();
	}
	
	if(this._woMarkers.length >= 1){
		if(me._currentPopup != null){
			me._currentPopup.close();
			me._currentPopup = null;
		}
		$.each(me._woMarkers,function(index,marker){
			
			marker.setMap(null);
			marker = null;
		});
		me._woMarkers = [];
	}
	
	$("#dlCallList").attr("class", "btnDisabledGreySmall").attr("disabled", "disabled");
};

FSTechMapper.prototype.resetSelections = function(mapperFormOnly){
	$("#mapWorkOrders .mapHeaderLink").click();
	//Clear Work Order Filters

	var today = new Date();
	$("#thisWeekPopup input[name='startDate']").val(today.getMonth() + 1 +"/"+today.getDate()+"/"+today.getFullYear());
	
	//toDate = new Date(today.getFullYear(), today.getMonth() +1, today.getDate() + 7);
	toDate = new Date(today.getTime() + (7*24*60*60*1000));
	$("#thisWeekPopup input[name='endDate']").val((toDate.getMonth()+1)+"/"+toDate.getDate()+"/"+toDate.getFullYear());
	$("#dateRangeLabel").text("");
	
	$("#Project").val("");
	$("#allProjectsLabel").html("All");
	
	$("input[name='allProjectsCheck']").each(function(){
			$(this).attr("checked", "");
	});
	
	
	$("#WorkOrder").val("Published");
	$("#woStageLabel").html("Published");
	$("#woStage").val("Published");
	
	$("#Route").val("");
	$("#routeInputLabel").html("");
	$("#RoutePopupInput").val("");
	
	$("#Region").val("");
	$("#regionInputLabel").html("");
	$("#RegionPopupInput").val("");
	
	$("#WinNum").val("");
	$("#WO_ID").val("");
	
	
	//clear the side search bar
	if(mapperFormOnly !==true){
		$("#searchForm :input").each(function(){
	    	if( $(this).is(":checkbox") == true && $(this).is(":checked") == true )$(this).attr("checked",false);
	    	if($(this).attr("id") == "state"){
	    		$(this).val("");
			}
			if($(this).is(":text") == true){
				$(this).val("");
			}
			
	        if($(this).attr("id") == "Country"){
	        	$(this).val("");
	        }
	        
	        if($(this).attr("type") == "select-one" && $(this).attr("id") != "Country" && $(this).attr("id") != "cbxselectSkill"){
				if($(this).attr("id") != "ProximityDistance"){
					//$(this).val("0").trigger("change");
	            }else if(jQuery.trim(jQuery("#ProximityZipCode").val()) == ""){
					//$("body").data("clearSearch", true);
					//$(this).val("50").trigger("change");
				}
			}
		});
	}
	
	$("#seeMoreTechs").hide();
};

FSTechMapper.prototype.clearAll = function(woOnly){

	if(woOnly != true)this.clearTechMarkers();
	this.clearWoMarkers();
	
	if(this._polygon != "" && this._polygon != null)this.clearPolygon();
	this.resetSelections();
	this.initMap();
	
	$("#dlCallList").attr("class", "btnDisabledGreySmall").attr("disabled", "disabled");
};

FSTechMapper.prototype.addMarker = function(info,type){
	curLatLng = new google.maps.LatLng(info.Latitude,info.Longitude);
	var me = this;
	
	var marker = new google.maps.Marker({
		position: curLatLng,
		map: this._goomap,
		icon: (type == "tech") ? this._techMarkerIcon : this._woMarkerIcon
	});
	
	(type == "wo") ? marker.setValues({"WIN_NUM":info.WIN_NUM, "Zip":info.Zipcode, "TechID": info.TechID, "Lat":info.Latitude, "Long":info.Longitude}) : marker.setValues({"TechID":info.TechID,"Email":info.PrimaryEmail});

	var popupContent = (type === "tech") ? this._techMarkerPopupContent[info.TechID] : this._woMarkerPopupContent[info.WIN_NUM];
	var popup = new InfoBubble({
		maxWidth: (type === "tech") ? 525 : 535,
		minHeight: (type === "tech") ? 180 : 260,
		padding: (type === "tech") ? "10px 0 10px 0" :"10px",
		content:popupContent
		});

	google.maps.event.addListener(marker, "click", function(){
		if(me._currentPopup != null){
			me._currentPopup.close();
			me._currentPopup = null;
		}
		popup.open(me._goomap,marker);
		if(type === "wo"){
			techId = this.get("TechID");
			techMapper.woPopupAssignCheck(techId);
			if(techId !== "" && techId !== undefined){
				techMapper.popupTechSearch(techId, "wo");
			}
			
			winNum = this.get("WIN_NUM");
			if(winNum !== "" && winNum !== undefined)techMapper.getBidInfo(winNum);
		}
		
		if(type === "tech"){
			techId = this.get('TechID');
			if(techId !== "" && techId !== undefined)techMapper.popupTechSearch(techId, "tech");
		}

		me._currentPopup = popup;
	});

	google.maps.event.addListener(popup,"closeclick", function() {
		me._currentPopup = null;
	});
	
	if(type === "tech"){ 
		this._techMarkers.push(marker);
		this._techMarkerCluster.addMarker(marker);
	}else{
		this._woMarkers.push(marker);
	}
};

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

FSTechMapper.prototype.woPopupAssignCheck = function(techId, blur){
	if(blur === "")blur = false;
	if(blur === false){
		if(techId !== "" && techId !== undefined){
			$("#woInfoButtons #btnWoAssign").addClass('btnDisabledGreySmall').click(function(){
				return false;
			});
		}else{
			$("#woInfoButtons #btnWoAssign").removeClass('btnDisabledGreySmall').click(function(){
				techMapper.assignTech($(this).parent().parent().parent().parent().parent());
			});
		}
	}else{
		if(techId !== "" && techId !== undefined){
			$("#woInfoButtons #btnWoAssign").removeClass('btnDisabledGreySmall').click(function(){
				techMapper.assignTech($(this).parent().parent().parent().parent().parent());
			});
		}
	}
	
};


FSTechMapper.prototype.orgByLatLong = function(techs){
	var techsArr = {};
	var count = 0;
	$.each(techs,function(idx, tech){
		i = tech.Latitude+tech.Longitude;
		if(techsArr[i] == undefined || techsArr[i] == null){
			techsArr[i] = {};
			count = 0;
		}
		techsArr[i][count] = tech;
		count++;
	});
	
	var techsRet = [];
	$.each(techsArr,function(idx,tech){
		var curLng;
		var curSize = Object.size(tech);
		$.each(tech,function(i,t){
			if(curSize >= 1 && i == 0){
				curLng = parseFloat(t.Longitude);
			}else if(curSize > 1 && i == 1){
				curLng = parseFloat(t.Longitude) + (.000065 + .000650);
			}else if(curSize > 1 && i > 1){
				curLng = curLng + (.000065 + .000650);
			}
			t.Longitude = curLng;
			techsRet.push(t);
		});	
	});
	return techsRet;
};

FSTechMapper.prototype.mapTechs = function(techs, qs, loadMore){
	
	if(techs == "" || techs == null) return false;
	techs = this.orgByLatLong(techs);
	//Parse the the current search criteria
	var keyValues = qs.split(/&/);
	var qsArr = [];
	var geocoder;
	
	$.each(keyValues,function(index,val){
		var key = val.split(/=/);
	    qsArr[key[0]] = key[1];
	});

	if(techs.length > 1){
		if(loadMore != true)me.clearTechMarkers();
			$.each(techs,function(index,value){
				me.addMarker(value, "tech");
		});
		if($("#btnProjMappingHighlight").is(":visible")){
			$("#btnProjMappingHighlight").attr("class", "link_button_sm").removeAttr("disabled");
		}else{
			$("#btnProjMappingHighlight:hidden").attr("class", "link_button_sm").removeAttr("disabled");
		}
		if(qsArr['Country'] != "" || qsArr['ProximityZipCode'] != "" || qsArr['state'] != "" || qsArr['City'] != ""){
			var address = qsArr['City']+" "+qsArr['state']+" "+qsArr['Country']+" "+qsArr['ProximityZipCode'];
			geocoder = new google.maps.Geocoder();
			me = this;
			var zoom = 10;
			if(qsArr['state'] != "" && qsArr['City'] == ""){
				zoom = 7;
			}
			geocoder.geocode({"address":address},
				function(results,status){
					if (status == google.maps.GeocoderStatus.OK) {
						var latLng = me.getLatLng(results);
						me._goomap.setCenter(latLng);
						me._goomap.setZoom(zoom);
					}
			});
		}else{
			this.fitMarkersToScreen();
		}
	}else{
		this.clearTechMarkers();
		
		tmpLatLng = new google.maps.LatLng(techs[0].Latitude, techs[0].Longitude);
		
		this.addMarker(techs[0],"tech");
		this._goomap.setCenter(tmpLatLng);
		this._goomap.setZoom(10);
	}
	
};

FSTechMapper.prototype.mapWorkOrders = function(rerun,dashWos){
	var searchParams = '';
	me = this;	
	var $inputs = $('#mapperForm input');
	
	if(rerun !== true){
		
		if(dashWos === "" || dashWos === null){
			$inputs.each(function(i, el) {
			    if ($(el).val()) {
			    	if (($(el).attr('type') != 'checkbox' && $(el).attr('type') != 'button' && $(el).attr('type') != 'radio') || $(el).attr('checked') == true) {
			             searchParams += $(el).attr('name') + '=' +$(el).val() +'&';
			         }
			     }
			 });
		}else{
			searchParams += "TB_UNID="+dashWos+"&";
			google.maps.event.clearListeners(this._goomap, 'idle');
		}
	}else{
		searchParams = this._currSearchParams;
	}
	
	this._currSearchParams = searchParams;
	searchParams += "company="+this._companyID;
	showLoader();
	
	 $.ajax({
         type: "POST",
         url: '/widgets/mapping/index/findwo/',
         dataType: "json",
         data: searchParams,
         success: function(res) {
		 	me.clearWoMarkers();
		 	if(res.data.length === 0){
		 		alert("Your search returned 0 Work Orders. Please adjust your Work Order filters.");
		 		$("#mapWoSubmit").trigger("click");
		 		setTimeout('hideLoader()',500);
		 	}else{
			 	$.each(res.data,function(k,wo){
			 		$.each(wo,function(k,v){
			 			me.setWoPopup(v);
			 		});
			 	});
			 	
			 	setTimeout('hideLoader()',1000);
			 	
			 	if(rerun == true){
	 				me._goomap.setCenter(new google.maps.LatLng(38, -97));
	 				me._goomap.setZoom(4);
	 			}
			 	mcOptions = {styles: [{
			 		height: 37,
			 		url: me._woClusterIcon,
			 		width: 26
			 		},
			 		{
			 		height: 37,
			 		url: me._woClusterIcon,
			 		width: 26
			 		},
			 		{
			 		height: 37,
			 		url: me._woClusterIcon,
			 		width: 26
			 		},
			 		{
			 		height: 37,
			 		url: me._woClusterIcon,
			 		width: 26
			 		},
			 		{
			 		height: 37,
			 		url: me._woClusterIcon,
			 		width: 26
			 		}], maxZoom: '11', gridSize: 5};
			 	me._woMarkerCluster = new MarkerClusterer(me._goomap, me._woMarkers, mcOptions);
			 	google.maps.event.addListener(me._woMarkerCluster, 'clusterclick',function(){me._clusterClicked = true;});
			 	me.fitMarkersToScreen();
			 	$("#cmdFindMapTechs").attr("class", "link_button_sm").removeAttr("disabled");
			 	$("#findMapTechs .mapHeaderLink").click();
		 	}
	 	}
	 });
};

FSTechMapper.prototype.fitMarkersToScreen = function(){
	var LatLngList = [];
	var bounds = new google.maps.LatLngBounds();
	
	if(this._techMarkers.length >=1){
 		$.each(this._techMarkers,function(i,marker){
 			LatLngList.push(marker.getPosition());
 		});
	}
	
	if(this._woMarkers.length >= 1){
 		$.each(this._woMarkers,function(i,marker){
 			LatLngList.push(marker.getPosition());
 		});
	}
 		
 	for(var i = 0, LtLgLen = LatLngList.length; i < LtLgLen; i++){
 		bounds.extend(LatLngList[i]);
 	}
 	if(LatLngList.length === 1){
 		this._goomap.setCenter(bounds.getCenter());
 		this._goomap.setZoom(10);
 	}else{
 		this._goomap.fitBounds(bounds);
 	}
};

FSTechMapper.prototype.showTechProfile = function(techID){
	if(techID != ""){ 
			parent.$("<div></div>").fancybox({
				"autoDimensions": false,
				"width": 1015,
				"height": "90%",
				"transitionIn": "none",
				"transitionOut": "none",
				"type": "iframe",
				"href": "/clients/wosViewTechProfileSimple.php?TechID=" + techID + "&v=" + this._companyID + "&popup=1"
			}).trigger("click");
	}
};

FSTechMapper.prototype.initPolygon = function(){
	
	if(this._polygon != null){
		if(techMapper._polygonClickListener === null)
			this._polygonClickListener = google.maps.event.addListener(this._goomap, 'click', $.proxy(this.addPolygonPoint,this));
	}else{
	
		this._polygon = new google.maps.Polygon({
			strokeWeight: 3,
			fillColor: '#555FF'
		});
		this._polygonPath = new google.maps.MVCArray;
		this._polygon.setMap(this._goomap);
		this._polygon.setPaths(new google.maps.MVCArray([this._polygonPath]));
		
		this._polygonClickListener = google.maps.event.addListener(this._goomap, 'click', $.proxy(this.addPolygonPoint,this));
	}
	this.updateBundleButtons();
};

FSTechMapper.prototype.polygonCheck = function(){
	if(this._polygon == null || this._polygonMarkers.length < 3){ return false; }else{ return true; };
};

FSTechMapper.prototype.updateBundleButtons = function(event){
	var inviteBtn = false;
	var assignBtn = false;
	if (this.polygonCheck()) {
		wos = this.getWOMarkers("WIN_NUM");
		techs = this.getTechMarkers("TechID");
		if (wos.length > 0 || techs.length > 0) {
			assignBtn = true;
		}
		if (wos.length > 0 && techs.length > 0) {
			inviteBtn = true;
		}
	}
	
	if (inviteBtn)
		this.enableBundleInviteButtons();
	else
		this.disableBundleInviteButtons();
	if (assignBtn)
		this.enableBundleAssignButtons();
	else
		this.disableBundleAssignButtons();
}

FSTechMapper.prototype.updateBundleViewAssignButtons = function(event){
	var inviteBtn = false;
	var assignBtn = false;
	var info = assignTool.getMassAssignInfo();
	var wosCount = 0;
	var techsCount = 0;
	var wos = info['wins'];
	var techs = info['techs'];

	$("input[name=assignCheckBox]:checked").each(function() {
		try {
			++wosCount;
			if (techs[$(this).val()] != '' && techs[$(this).val()] != undefined)
				++techsCount;
		} catch (e) {
		}
	});

	if (wosCount > 0) {
		inviteBtn = true;
	}
	if (wosCount > 0 && techsCount > 0) {
		assignBtn = true;
	}
	if (inviteBtn)
		me.enableBundleInviteViewAssignButtons();
	else
		me.disableBundleInviteViewAssignButtons();
	if (assignBtn)
		me.enableBundleAssignViewAssignButtons();
	else
		me.disableBundleAssignViewAssignButtons();
}

FSTechMapper.prototype.disableBundleInviteButtons = function(event){
	element = $("#highlightAreaContent .bundleInviteBtn")
	element.removeClass('link_button_sm').addClass("btnDisabledGreySmall").val("Bundle & Invite");
}

FSTechMapper.prototype.enableBundleInviteButtons = function(event){
	element = $("#highlightAreaContent .bundleInviteBtn");
	element.removeClass('btnDisabledGreySmall').addClass("link_button_sm").val("Bundle & Invite");
}
FSTechMapper.prototype.disableBundleAssignButtons = function(event){
	element = $("#highlightAreaContent .bundleAssignBtn");
	element.removeClass('link_button_sm').addClass("btnDisabledGreySmall").val("Assign");
}

FSTechMapper.prototype.enableBundleAssignButtons = function(event){
	element = $("#highlightAreaContent .bundleAssignBtn");
	element.removeClass('btnDisabledGreySmall').addClass("link_button_sm").val("Assign");
}

FSTechMapper.prototype.disableBundleInviteViewAssignButtons = function(event){
	element = $("#viewAssignContent .bundleInviteBtn");
	element.removeClass('link_button_sm').addClass("btnDisabledGreySmall").val("Bundle & Invite");
}

FSTechMapper.prototype.enableBundleInviteViewAssignButtons = function(event){
	element = $("#viewAssignContent .bundleInviteBtn");
	element.removeClass('btnDisabledGreySmall').addClass("link_button_sm").val("Bundle & Invite");
}
FSTechMapper.prototype.disableBundleAssignViewAssignButtons = function(event){
	element = $("#viewAssignContent .bundleAssignBtn");
	element.removeClass('link_button_sm').addClass("btnDisabledGreySmall").val("Assign");
}

FSTechMapper.prototype.enableBundleAssignViewAssignButtons = function(event){
	element = $("#viewAssignContent .bundleAssignBtn");
	element.removeClass('btnDisabledGreySmall').addClass("link_button_sm").val("Assign");
}

FSTechMapper.prototype.addPolygonPoint = function(event){
	if(this._clusterClicked === true){this._clusterClicked = false; return;}
	
	this._polygonPath.insertAt(this._polygonPath.length, event.latLng);
	
	var marker = new google.maps.Marker({
		position: event.latLng,
		map: this._goomap,
		draggable: true
	});
	this._polygonMarkers.push(marker);
	marker.setTitle("#"+this._polygonPath.length);
	
	me = this;
	google.maps.event.addListener(marker, 'click', function(){
		marker.setMap(null);
		for(var i=0, I = me._polygonMarkers.length; i < I && me._polygonMarkers[i] != marker; ++i);
		me._polygonMarkers.splice(i,1);
		me._polygonPath.removeAt(i);
		me.updateBundleButtons();
	});
	
	google.maps.event.addListener(marker, 'dragend', function(){
		for(var i=0, I = me._polygonMarkers.length; i < I && me._polygonMarkers[i] != marker; ++i);
		me._polygonPath.setAt(i, marker.getPosition());
		me.updateBundleButtons();
	});
	this.updateBundleButtons();
	
	if(this.polygonCheck() != false && this.getMappedWos() != false){
		$("#dlCallList").attr("class", "link_button_sm").removeAttr("disabled");
	}else{
		$("#dlCallList").attr("class", "btnDisabledGreySmall").attr("disabled", "disabled");
	}
};

FSTechMapper.prototype.clearPolygon = function(){
	me = this;
	
	if(this._polygonMarkers.length >= 1){
		$.each(me._polygonMarkers,function(index,value){
			me._polygonMarkers[index].setMap(null);
		});
	}
	
	if(this._polygon){this._polygon.setMap(null);this._polygon = null;this._polygonPath = null;}
	if(this._polygonClickListener)google.maps.event.removeListener(this._polygonClickListener);
	this.updateBundleButtons();
	$("#dlCallList").attr("class", "btnDisabledGreySmall").attr("disabled", "disabled");
};

FSTechMapper.prototype.thisWeekHtml = function(){
	var html;	

	var today = new Date();
	
	var startDate = $("#StartDate").val() != "" ? $("#StartDate").val() : today.getMonth() + 1 +"/"+today.getDate()+"/"+today.getFullYear();
	
	toDate = new Date(today.getTime() + (7*24*60*60*1000));
	var endDate = $("#EndDate").val() != "" ? $("#EndDate").val() : (toDate.getMonth()+1) +"/"+toDate.getDate()+"/"+toDate.getFullYear();
	if(dashboardEndDate === "")$("#mapperForm input[name='EndDate']").val((toDate.getMonth()+1)+"/"+toDate.getDate()+"/"+toDate.getFullYear());
	
	html = "<table id='thisWeekPopup' style='width: 100%; table-layout:fixed;'>";
	html += "<col width='80px' /><col width='155px' />";
	html += "<tr><td width='45%' style='text-align:right;'>Start Date Range:</td><td><select name='thisWeekRadio' onchange='techMapper.thisWeekSelectChange(this);'>";
	html += "<option value='thisWeek'>This Week</option>";
	html += "<option value='todTom'>Today and Tomorrow</option>";
	html += "<option value='twoWeeks'>Next Two Weeks</option>";
	html += "<option value='fourWeeks'>Next Four Weeks</option>";
	html += "<option value='allFuture'>All Future</option></select></td></tr>";
	html += "<tr class='thisWeekRange' style=''><td></td><td><input type='text' style='width: 65px;' name='startDate' id='startDate' value='"+startDate+"' /> To <input type='text' style='width: 65px;' name='endDate' value='"+endDate+"' id='endDate' /></td></tr>";
	html += "</table>";
	
	return html;
};

FSTechMapper.prototype.thisWeekSelectChange = function(el){
	var thisWeekSel = $(el).val();
	$("#thisWeekPopup input[name='startDate']").val("");
	$("#thisWeekPopup  input[name='endDate']").val("");
	var today = new Date();
	$("#thisWeekPopup input[name='startDate']").val(today.getMonth()+1+"/"+today.getDate()+"/"+today.getFullYear());
	switch (thisWeekSel){
		case "thisWeek":
			toDate = new Date(today.getTime() + (7*24*60*60*1000));
			$("#thisWeekPopup  input[name='endDate']").val( (toDate.getMonth()+1)+"/"+toDate.getDate()+"/"+toDate.getFullYear());
			break;
		case "todTom":
			toDate = new Date(today.getTime() + (1*24*60*60*1000));
			$("#thisWeekPopup  input[name='endDate']").val((toDate.getMonth()+1)+"/"+toDate.getDate()+"/"+toDate.getFullYear());
			break;
		case "twoWeeks":
			toDate = new Date(today.getTime() + (14*24*60*60*1000));
			$("#thisWeekPopup  input[name='endDate']").val((toDate.getMonth()+1)+"/"+toDate.getDate()+"/"+toDate.getFullYear());
			break;
		case "fourWeeks":
			toDate = new Date(today.getTime() + (28*24*60*60*1000));
			$("#thisWeekPopup  input[name='endDate']").val((toDate.getMonth()+1)+"/"+toDate.getDate()+"/"+toDate.getFullYear());
			break;
		case "allFuture":
			//$("#mapperForm input[name='EndDate']").val("");
			break;
		case "custom":
			$("#thisWeekPopup input[name='startDate']").val("");
			$("#thisWeekPopup  input[name='endDate']").val("");
			break;
		case "startDateRange":
			//$("#mapperForm input[name='StartDate']").val($("#startDate").val());
			//$("#thisWeekPopup  input[name='endDate']").val($("#endDate").val());
			break;
	};
};

FSTechMapper.prototype.thisWeekSubmit = function(){
	var startDate = $("#thisWeekPopup input[name='startDate']").val();
	var endDate = $("#thisWeekPopup  input[name='endDate']").val();
	
	$("#dateRangeLabel").text(startDate+" to "+endDate);
	
	$("#StartDate").val(startDate);
	$("#EndDate").val(endDate);
};

FSTechMapper.prototype.clearFiltersPopup = function(){
var html;
	
	html = "<table>";
	html += "<tr><td colspan='2'>Which filters are you clearing?</td></tr>";
	html += "<tr><td colspan='2'><input type='checkbox' name='clearFiltersCheck' value='Techs' /> Techs</td></tr>";
	html += "<tr><td colspan='2'><input type='checkbox' name='clearFiltersCheck' value='Wos' /> Work Orders</td></tr>";
	html += "<tr><td><input type='button' class='link_button_sm' value='Cancel' id='cancelDateRange' onclick='roll.hide();'></td><td><input type='button' class='link_button_sm' value='Clear Filters' onclick='techMapper.clearFiltersSubmit();' id='clearFiltersSubmit'></td></tr>";
	html += "</table>";
	
	return html;
};

FSTechMapper.prototype.clearFiltersSubmit = function(){
	me = this;
	var techsChk = false;
	var wosChk = false;
	$.each($("input[name='clearFiltersCheck']:checked"), function(){
		if($(this).val() == "Techs")techsChk = true; 
		if($(this).val() == "Wos") wosChk = true;
	});
	
	if(techsChk == true && wosChk == true){
		me.resetSelections();
		me.clearAll(true);
	}else if(techsChk == true){
		me.resetSelections();
		me.clearTechMarkers();
		clearListFilters();
	}else if(wosChk == true){
		me.clearWoMarkers();
	}
	
	roll.hide();
};

FSTechMapper.prototype.emailBlastPopup = function(){
	var html;
		
		html = "<table>";
		html += "<tr><td colspan='2'>Send Email Blast</td></tr>";
		html += "<tr><td colspan='2'>Send an email to all mapped Technicians or only Technicians in the highlighted area?</td></tr>";
		html += "<tr><td><input type='button' class='link_button_sm' value='All Techs' id='blastAllTechs' onclick='techMapper.emailBlastSubmit(this);'></td><td><input type='button' class='link_button_sm' value='Highlighted Area' onclick='techMapper.emailBlastSubmit(this);' id='blastHighlightedOnly'></td></tr>";
		html += "</table>";
		
		return html;
};

FSTechMapper.prototype.emailBlastSubmit = function(el){
	var techs = [];
	me = this;
	
	var currId = $(el).attr("id");
		
	if(currId == "blastHighlightedOnly"){
		techs = this.getTechMarkers("Email");
		
		techsString = techs.join(",");
		$("#emailList").val(techsString);
	}else{
		if(this._allEmailList != "" || this._allEmailList != null)$("#emailList").val(this._allEmailList);
	}
	roll.hide();
	blastTechs();
	
};

FSTechMapper.prototype.woStageHtml = function(){
	var html;
	
	var woStageVal = $("#WorkOrder").val();
	
	var woStageOptions = new Object;
	
	woStageOptions['Published'] = "Published";
	woStageOptions['Created'] = "Created";
	woStageOptions['Assigned'] = "Assigned";
	woStageOptions['Work_Done'] = "Work Done";
	woStageOptions['Incomplete'] = "Incomplete";
	woStageOptions['Completed'] = "Completed";
	woStageOptions['Deactivated'] = "Deactivated";
	woStageOptions['All'] = "All";
	var woStageSel = "";
	
	html = "<table id='woStagePopup' style='width: 260px;table-layout:fixed;'>";
	html += "<col width='120px' /><col width='140px' />";
	html += "<tr><td width='45%' style='text-align:right;'>Work Order Stage:</td><td><select name='woStage' id='woStage'>";
	$.each(woStageOptions, function(key,val){
		if(woStageVal == key){woStageSel = "SELECTED";}else{woStageSel = "";}
		html += "<option value='"+key+"' "+woStageSel+">"+val+"</option>";
	});
	
	html += "</select></td></tr></table>";
	
	return html;
};

FSTechMapper.prototype.woStageSubmit = function(){
	var woStageVals = [];
	var woStageLabel;
	
	var woStageVal = $("#woStage").val();

	if(woStageVal == "All"){
		$("#WorkOrder").val("Published,Created,Assigned,Work_Done,Incomplete,Completed,Deactivated");
		$("#woStageLabel").html("Multiple Stages");
	}else{
		$("#WorkOrder").val(woStageVal);
		woStageLabel = (woStageVal == "Work_Done") ? "Work Done" : woStageVal;
		$("#woStageLabel").html(woStageLabel);
	}	
	return;
};

FSTechMapper.prototype.routePopupHtml = function(){
	var html; 
	var routeVal = $("#Route").val();
	
	html = '<table style="width: 260px;table-layout:fixed;">';
	html += "<col width='120px' /><col width='140px' />";
	html +=	'<tr><td width="45%" style="text-align:right;">Bundle:</td><td><input type="text" size="25" id="RoutePopupInput" name="RoutePopupInput" value="'+routeVal+'" /></td></tr>';
	html += '<tr><td><br /></td></tr></table>';
	html +=	'<table style="width: 300px;text-align:center;"><tr><td><input type="button" class="link_button_sm" value="Cancel" id="cancelRoutePopup" onclick="roll.hide();">';
	html += '<input type="button" class="link_button_sm" value="Map Work Orders" id="submitDateRange" onclick="techMapper.selectWorkOrdersSubmit();" style="margin-left:5px;"></td>';
	html += '</table>';
	
	return html;
};

FSTechMapper.prototype.routePopupSubmit = function(){
	var routeVal = $("#RoutePopupInput").val();
	
	if(routeVal == null || routeVal == "")return;
	
	$("#Route").val(routeVal);
	$("#routeInputLabel").html(routeVal);
	
};

FSTechMapper.prototype.winWoidPopupHtml = function(){
	
	var html; 
	var winNum = $("#WinNum").val();
	var woId = $("#WO_ID").val();
	
	html = '<table style="width: 260px;table-layout:fixed;">';
	html += "<col width='120px' /><col width='140px' />";
	html +=	'<tr><td width="45%" style="text-align:right;">WIN#:</td><td><input type="text" size="25" id="winPopupInput" name="winPopupInput" value="'+winNum+'" /></td></tr>';
	html +=	'<tr><td width="45%" style="text-align:right;">Client WO ID:</td><td><input type="text" size="25" id="woidPopupInput" name="woidPopupInput" value="'+woId+'" /></td></tr>';
	html += '</table>';
	
	return html;
};

FSTechMapper.prototype.winWoidSubmit = function(){
	var winNumVal = $("#winPopupInput").val();
	var woidVal = $("#woidPopupInput").val();

	//if( (winNumVal === null || winNumVal === "") && (woidVal === null || woidVal === "") )return;

	$("#WinNum").val(winNumVal);
	$("#WO_ID").val(woidVal);
};

FSTechMapper.prototype.regionPopupHtml = function(){
	var html; 
	var regionVal = $("#Region").val();
	
	html = '<table style="width: 260px;table-layout:fixed;">';
	html += "<col width='120px' /><col width='140px' />";
	html +=	'<tr><td width="45%" style="text-align:right;">Region:</td><td><input type="text" size="25" id="RegionPopupInput" name="RegionPopupInput" value="'+regionVal+'" /></td></tr>';
	html += '</table>';
	
	return html;
};

FSTechMapper.prototype.regionPopupSubmit = function(){
	var regionVal = $("#RegionPopupInput").val();
	
	if(regionVal == null || regionVal == "")return;
	
	$("#Region").val(regionVal);
	$("#regionInputLabel").html(regionVal);
};

FSTechMapper.prototype.allProjectsHtml = function(){
	var projectsArr = [];
	var currSelProjects = $("#allProjects #Project").val();
	var currProjArr = [];
	var html = "";
	var selAll = "checked";
	if(currSelProjects != "" && currSelProjects.indexOf(",") > -1){
		 selAll = "";
		 currProjArr = currSelProjects.split(","); 
	}else if(currSelProjects != "" && currSelProjects.indexOf(",") == -1){
		selAll = "";
		currProjArr.push(currSelProjects);
	}
		 
	 html += "<table style='width: 260px;table-layout:fixed;'>";
	 html += "<col width='120px' /><col width='140px' />";
	 
 	 html += "<tr><td style='text-align:right;'>Select Project(s):</td><td><input name='allProjectsCheck' id='allProjectsCheck' type='checkbox' rel='All' value='All' onclick='techMapper.allProjectsSelectAll(this);' "+selAll+"/>All</td></tr></table>";
 	 html += "<div id='projectsDiv' style='height: 135px; overflow-y: scroll;'><table style='width: 260px;padding-left: 9px;' > <col width='120px' /><col width='140px' />";
 	 $.each(this._allProjects, function(key,project){
 		 if(project.Active === true){
 			var checked = "";
 			if($.inArray(project.Project_ID,currProjArr) > -1) checked = "checked";
 			 html += "<tr><td></td><td><input name='allProjectsCheck' onclick='techMapper.allProjectsSelectAll(this);' type='checkbox' rel='"+project.Project_Name+"' value='"+project.Project_ID+"' "+checked+"/>"+project.Project_Name+"</td></tr>";
 		 }
 	 });
 	 html += "</table></div>";
  	 return html;  
};

FSTechMapper.prototype.projectMappingPopupHtml = function(){
	var html;
	
	html = "<form name='projectMappingForm' id='projectMappingForm'><table id='projectMappingPopup' style='width: 260px;table-layout:fixed;line-height:18px;'>";
	html += "<col width='10px' /><col width='140px' />";
	html += "<tr><td colspan='2' style='text-align:leftt;'>For Project Mapping, make a selection below:</td></tr>";
	html += "<tr><td style='vertical-align:top;padding-top: 4px;'><input type='radio' name='projectMappingOption' value='1' onclick='if($(this).attr(\"checked\") == true) techMapper._projectMappingOption1 = true;' checked></td><td>Use left sidebar to select and map techs around all mapped Work Orders</td></tr>";
	html += "<tr><td style='vertical-align:top;padding-top: 4px;'><input type='radio' name='projectMappingOption' value='2' onclick='if($(this).attr(\"checked\") == true) techMapper._projectMappingOption1 = false;'></td><td>Display all nearby Technicians assigned to Work Orders in the selected Project(s)</td></tr>";
	//html += "<tr><td style='vertical-align:top;padding-top: 4px;'><input type='radio' name='projectMappingOption' value='3' onclick='if($(this).attr(\"checked\") == true) techMapper._projectMappingOption1 = false;'></td><td>Display all Technicians that have Bid on Work Orders in the selected Project(s)</td></tr>";
	html += "<tr><td colspan='2'><input type='button' class='link_button_sm' value='Cancel' id='btnProjectMappingCancel' onclick=''>&nbsp;&nbsp;<input type='button' class='link_button_sm' value='Map Techs' id='btnProjectMappingSubmit' onclick='techMapper.projectMappingSubmit();'></td></tr>";
	html += "</table></form>";
	
	return html;
};

FSTechMapper.prototype.allProjectsSelectAll = function(el){
	if($(el).attr("id") == "allProjectsCheck"){
		if($(el).is(":checked")){
			$.each($('#projectsDiv input[name="allProjectsCheck"]'), function (idx, item){
				$(item).attr("checked", true);
			});
		}else{
			$.each($('#projectsDiv input[name="allProjectsCheck"]'), function (idx, item){
				$(item).attr("checked", false);
			});
		}
	}else{
		$("#allProjectsCheck").attr("checked", false);
	}
};

FSTechMapper.prototype.getAllProjects = function(){
	
	$.ajax({
        type: "POST",
        url: '/widgets/dashboard/popup/findworkorder/',
        dataType    : 'json',
        cache       : false,
        context		: this,
        data: {company: this._companyID, mapper: true},
        success: function(projects){
        	this._allProjects = projects;
        }
	 });	
};

FSTechMapper.prototype.allProjectsSubmit = function(){
	var allProjVals = [];
	var allProjLabel;
	
	$.each($("input[name='allProjectsCheck']:checked"), function(){
		if($(this).val() != "All")allProjVals.push($(this).val());
	});
	
	$("#allProjects #Project").val(allProjVals);
	
};

FSTechMapper.prototype.selectWorkOrdersPopup = function(){
	var html = "";
	
	html += this.allProjectsHtml();
	html += this.woStageHtml();
	html += this.thisWeekHtml();
	html += this.winWoidPopupHtml();
	html += this.regionPopupHtml();
	html += this.routePopupHtml();
	
	return html;
};

FSTechMapper.prototype.selectWorkOrdersSubmit = function(){
	this.allProjectsSubmit();
	this.woStageSubmit();
	this.thisWeekSubmit();
	this.regionPopupSubmit();
	this.routePopupSubmit();
	this.winWoidSubmit();
	this.mapWorkOrders(false,"");
	roll.hide();
};


FSTechMapper.prototype.setWoPopup = function(woInfo){
	var popupHtml = "";
	var amountPer = ['Site', 'Hour', 'Device', 'Visit'];
	var assignTechId = "";
	var assignTechDisable = "";
	if(woInfo.Tech_Bid_Amount === null || woInfo.Tech_Bid_Amount === ""){
		agreedAmt = '<input type="text" size="2" id="AgreedAmount" name="AgreedAmount" value="" /> per: <select name="Amount_Per" size="1" id="Amount_Per" onchange="techMapper.changeAmountPer(this.value)">';
		
		$.each(amountPer, function(i,v){
			agreedAmt += (woInfo.Amount_Per === v) ? "<option value='"+v+"' selected='selected'>"+v+"</option>" : "<option value='"+v+"'>"+v+"</option>";
		});
		
		//agreedAmt += (woInfo.Amount_Per === "Device" || woInfo.Amount_Per === "Visit") ? 'value="'+woInfo.Qty_Devices+'" /></span>' : ' /></span>'; 
		if(woInfo.Amount_Per === "Device" || woInfo.Amount_Per === "Visit"){
			agreedAmt += '</select><br /><span id="Qty_Devices"><span id="Qty_DevicesLabel">'+woInfo.Amount_Per+' Quantity</span><input id="QtyDevicesInput" type="text" size="2" name="Qty_Devices" ';
			agreedAmt += (woInfo.Amount_Per === "Device") ? 'value="'+woInfo.Qty_Devices+'" /></span>' : 'value="'+woInfo.NumOfVisits+'" disabled="disabled"/></span>';
		}else{
			agreedAmt += '</select><br /><span id="Qty_Devices" style="display:none;"><span id="Qty_DevicesLabel"></span><input id="QtyDevicesInput" type="text" size="2" name="Qty_Devices" /></span>';
		}
	}else{
		agreedAmt =  "<input type='hidden' name='AgreedAmount' value='"+woInfo.Tech_Bid_Amount+"' id='AgreedAmount' />"+woInfo.Tech_Bid_Amount;
	}
	
	var clientOfferContent = (woInfo.PayMax === null || woInfo.PayMax === "") ? "<input type='text' size='15' name='clientWoOffer' />" : woInfo.PayMax+" Per "+woInfo.Amount_Per;
	if(woInfo.TechID != null && woInfo.TechID != ""){
		assignTechId = woInfo.TechID; 
		assignTechDisable = 'btnDisabledGreySmall';
	}
	
	var route = (woInfo.Route == null || woInfo.Route == "") ? "" : woInfo.Route; 
	
	popupHtml += "<div class='container' style='width: 270px; height: 235px; font-size: 11px;'>";
	popupHtml += "<form name='woPopupForm' id='woPopupForm'>";
	popupHtml += "<table id='woInfo' width='100%'><col width='125' />";
	popupHtml += "<tr><td><span class='woTableLabel'>WIN#: </span><input type='hidden' name='winNum' id='woPopupWinNum' value='"+woInfo.WIN_NUM+"' /><span id='winNum'>"+woInfo.WIN_NUM+"</span></td><td>Client ID: "+woInfo.WO_ID+"</td></tr>";
	popupHtml += "<tr><td><span class='woTableLabel'>Status: </span></td><td>"+woInfo.Status+"</td></tr>";
	popupHtml += "<tr><td><span class='woTableLabel'>Project: </span></td><td>"+woInfo.Project_Name+"</td></tr>";
	popupHtml += "<tr><td style='vertical-align: top;'><span class='woTableLabel'>Headline: </span></td><td>"+woInfo.Headline+"</td></tr>";
	popupHtml += "<tr><td><span class='woTableLabel'>Bundle: </span></td><td>"+route+"</td></tr>";
	popupHtml += "<tr><td style='vertical-align: top;'><span class='woTableLabel'>Location: </span></td><td>"+woInfo.Location+"</td></tr>";
	popupHtml += "<tr><td><span class='woTableLabel'>Start Date: </span></td><td>"+woInfo.StartDate+"</td></tr>";
	popupHtml += "<tr><td><span class='woTableLabel'>Client $ Offer: </span></td><td>"+clientOfferContent+"</td></tr>";
	popupHtml += "<tr><td><span class='woTableLabel'>Assign FS-Tech ID#: </span></td><td><input type='text' size='15' name='techId' id='techId' value='"+assignTechId+"' onblur='if(this.value != \"\"){techMapper.woPopupAssignCheck(this.value,true);}techMapper.popupTechSearch(this.value, \"wo\");' /></td></tr>";
	popupHtml += "<tr><td><span class='woTableLabel'>Name: </span></td><td><span id='techName'></span></td></tr>";
	popupHtml += "<tr><td><span class='woTableLabel'>Phone: </span></td><td><span id='techPhone'></span></td></tr>";
	popupHtml += "<tr><td><span class='woTableLabel'>Email: </span></td><td><span id='techEmail'></span></td></tr>";
	popupHtml += "<tr><td><span class='woTableLabel'>Agreed $ Amount: </span></td><td>"+agreedAmt+"</td></tr>";
	popupHtml += "<table id='woInfoButtons' style='margin-left: auto; margin-right: auto;'>";
	popupHtml += "<tr><td><input type='button' class='link_button_sm' value='View Bidders' id='viewBidders' onclick='techMapper.viewBidders(\""+woInfo.WIN_NUM+"\");'></td><td><input type='button' class='link_button_sm' value='Use Zip Code' id='useZipCode' onclick='techMapper.useZipOnSearch(\""+woInfo.Zipcode+"\");'></td></tr>";
	popupHtml += "<tr><td><input type='button' class='link_button_sm' value='Map Bidders' id='mapBidders' onclick=''></td><td><input type='button' value='Assign' id='btnWoAssign' name='btnWoAssign' class='link_button_sm " + assignTechDisable + "' ></td></tr></table>";
	popupHtml += "</table></form></div>";
	
	this._woMarkerPopupContent[woInfo.WIN_NUM] = popupHtml;
	this.addMarker(woInfo, "wo");
	
};

FSTechMapper.prototype.bundleInviteClick = function(winList, emails) {
	var wins = winList == undefined ? $("#bundleName").data('wins') : winList;
	var eList = emails == undefined ? $("#bundleName").data('eList') : emails;
	$.cookie("blastEmail", eList, {path: '/'});
	$.cookie("bundleInviteWins", wins, {path: '/'});
	$.cookie("bundleName", $("#bundleName").val(), {path: '/'});
	roll.hide();
	openLightBoxPage('/Email_Form_Client.php');
}

FSTechMapper.prototype.bundleOnlyClick = function(winList) {
	var wins = winList == undefined ? $("#bundleName").data('wins') : winList;
	if ($("#bundleName").val() == "") {
//		showLightBoxMsg("Bundle name is required.", 30, 30, true);
//		return;
	}
        $.ajax({
                type: "POST",
                url: "/widgets/dashboard/client/do-bundle/",
                dataType    : 'json',
                cache       : false,
                data: {
			name: $("#bundleName").val(),
                        wins: wins
                },
                success: function (data) {
			roll.hide();
			showLightBoxMsg("Bundle name saved.", 30, 30, true);
                }
        });
}

FSTechMapper.prototype.showNoTechInAreaMessage = function() {
	msg = "<div>You must highlight an area around the techs you want to invite to bid on these Work Orders.</div>";
	showLightBoxMsg(msg, 100, 50, true, 3000);
}

FSTechMapper.prototype.viewBidders = function(win){
	var href = "wosApplicant.php?sb=0&ClientID=&v="+this._companyID+"&WorkOrderID="+win;
	
	$("<div></div>").fancybox(
			{
				'type' : 'iframe',
				'autoDimensions' : false,
				'width' : 850,
				'height' : 400,
				'transitionIn': 'none',
				'transitionOut' : 'none',
				'href': href
			}
			).trigger('click');
	
};

FSTechMapper.prototype.changeAmountPer = function (id) {
    if(id === 'Device' || id === 'Visit'){
        $("#Qty_Devices").show();
        $("#Qty_DevicesLabel").text(id + " Quantity" );
        if(id === 'Visit'){
        	$('#QtyDevicesInput').attr("disabled","disabled");
        }else{
        	$('#QtyDevicesInput').attr("disabled","");
        }
    } else {
    	 $("#Qty_Devices").hide();
    }
};

FSTechMapper.prototype.getBidInfo = function(win){
	if(win == '' || win == 'undefined') return false;
	
	$.getJSON('/widgets/dashboard/client/get-wo-bids-by-win', {
		win:win
	},function(data){
		if(data.error == 1){
			//could not find bids
			$("#woInfoButtons #mapBidders").addClass('btnDisabledGreySmall').attr("onclick", "");
			return false;
		}else{
			$("#woInfoButtons #mapBidders").val("Map Bidders ("+data.length+")");
			var techArr = [];
			$.each(data,function(k,v){
				techArr.push(v);
			});
			var techString = techArr.join(",");
			$("#woInfoButtons #mapBidders").click(function(){
				techMapper.mapBidders(techArr);
			});
		}
	});
};

FSTechMapper.prototype.mapBidders = function(techs){
	this.clearTechMarkers();
	this.setTechPopups(techs);
	$(".seeMore").hide();
	$("#projectMappingWoBidders").show();
};
                       
FSTechMapper.prototype.popupTechSearch = function(techId,type){
    if (techId === '' || techId === 'undefined') return;   
    // get techInfo
    $.getJSON("/widgets/dashboard/popup/get-tech-info/", {
        tech: techId, 
        company : this._companyID
        }, function(data) {
      
        if (data.error == 1) {
            $('#techErrorMsg').html(data.msg);
            $('#techErrorMsg').show('fast',function(){
                setTimeout(function () {
                    $('#techErrorMsg').hide(500);
                }, 3000);
            });
        } else {
        	
        	if (data.techInfo.isDenied && this['shownClientDeniedMsg'] != true) {
        		this._redirect = 0;
        		this['shownClientDeniedMsg'] = true;
        		$('#techErrorMsg').html('You are about to assign work to a technician that has been "denied access" by your company. Please re-confirm your choice of technician. Tech ID: '+techId+' ('+data.techInfo.fName+' '+data.techInfo.lName+')');
                $('#techErrorMsg').show('fast',function(){
                    setTimeout(function () {
                        $('#techErrorMsg').hide(500);
                    }, 3000);
                });
            } 
        	if(type === "wo"){
        		$('#woInfo #techName').html(data.techInfo.fName+' '+data.techInfo.lName);
        		$('#woInfo #techEmail').html(data.techInfo.email);
        	}
            
        	el = (type === "wo") ? "#woInfo #techPhone" : "#techInfo #phoneTag" ;
            
            detailObject.onInit.RequitePhone(el
                , data.techInfo.phone
                ,data.techInfo.TechID
                ,data.techInfo.PrimaryPhoneStatus
                ,1
                ,'html'
                ,Base64.encode(data.techInfo.fName + ' ' +data.techInfo.lName));
            
            detailObject.onInit.RequitePhone("#techInfo #phoneTagEve"
                    , data.techInfo.phone2
                    ,data.techInfo.TechID
                    ,data.techInfo.SecondaryPhoneStatus
                    ,1
                    ,'html'
                    ,Base64.encode(data.techInfo.fName + ' ' +data.techInfo.lName));
            
            
        }
    });
};

FSTechMapper.prototype.assignTech = function (form) {
    var techs = {};
    var agreeds = {};
    var amountsrep = {};
    var qtydevices = {};
    me = this;
    var company = this._companyID;

    winNum = $(form).find("#woPopupWinNum").val();
    tech = $(form).find("#techId").val();
    agreed = $(form).find('#AgreedAmount').val();
    amountper = $(form).find('#Amount_Per').val();
    qtydevice = $(form).find('#QtyDevicesInput').val();

    techs[winNum] = tech;
    agreeds[winNum] = agreed;
    amountsrep[winNum] = amountper;
    qtydevices[winNum] = qtydevice;
    //progressBar.key('assign');
    //progressBar.run();
    var self = this;
    showLoader();
    $.getJSON("/widgets/dashboard/do/assign",{
        techs:techs, 
        agreeds:agreeds, 
        amountsrep:amountsrep, 
        qtydevices:qtydevices, 
        company:company, 
        assignClientDeniedTech:true
    },function(data){
    	setTimeout('hideLoader()',1000);
        if (data.error == 1) {
            alert(data.msg);
            /*$('#woErrorMsg').show('fast',function(){
                setTimeout(function () {
                    $('#woErrorMsg').hide(500);
                }, 4000);
            });
            */
        } else {
        	me.mapWorkOrders(true);
            roll.hide();
        }
    });
};

FSTechMapper.prototype.getViewAssignInfo = function() {
	info = assignTool.getMassAssignInfo();
	var techCount = {};
	techs = info['techs'];
	if (techs.length == 0) return;
	var winList = [];
	for (wo in techs) {
		// count number of times tech assigned to a work order
		try {
			if (techCount[techs[wo]] == undefined) techCount[techs[wo]] = 0;
			++techCount[techs[wo]];
		} catch (e) {
			techCount[techs[wo]] = 1;
		}
		winList.push(wo);
	}
	msg = [];
	for (id in techCount) {
		wo = "Work Order" + (techCount[id] > 1 ? "s" : "");
		msg.push(techCount[id] + " " + wo + " to:<br/><br/>FS-Tech ID#" + id + " - " + $("#techName_" + id).html());
	}
	return {'winList': winList, 'msg': msg, 'winsCheckedOnly': info['wins']};
};

FSTechMapper.prototype.confirmViewAssign = function(event) {
	var info = techMapper.getViewAssignInfo();
	winList = info['winList'];
	msg = info['msg'];
	if (winList.length == 0) return;
                roll.showAjax(this, event, {
                        url: "/widgets/dashboard/client/bundle-assign/",
                        width: 270,
                        height: 135 + (85 * (winList.length - 1)),
                        title: "Bundle & Assign Work Orders",
                        paddingTop: 50,
                        data: {
                                wosList: msg,
                                wins: winList
                        }
                });
};


FSTechMapper.prototype.massAssign = function(){
	
	var techs = {};
    var agreeds = {};
    var routes = {};

    var company = pmContext != undefined ? pmContext : "";

    $.each($('#widgetContainer input[name="publishedWinNum"]'), function (idx, item){
        winNum = $(item).val();
        tech   = $('#'+winNum+'_TechId').val();
        agreed = $('#'+winNum+'_TechAgreed').val();
        if($("#ViewAssignRoute").val() !== ""){
        	routes[winNum] = $("#ViewAssignRoute").val();
    	}

        if(tech != '' && agreed != '') {
            techs[winNum]   = tech;
            agreeds[winNum] = agreed;
        }
    });

    if(techs.length == 0 || agreed.length ==0) return;

    progressBar.key('assign');
    progressBar.run();
    var self = this;

    $.getJSON("/widgets/dashboard/do/assign",{
        techs:techs, 
        agreeds:agreeds, 
        routes: routes,
        company: company
    }, function(data){
        self.massAssignCallback();
    });

};

FSTechMapper.prototype.massAssignCallback = function(info){
	info = assignTool.getMassAssignInfo();
	techs = info['techs'];
	agreeds = info['agreeds'];
	var allAssigned = true;
	var assignedWos = [];
	me = this;
	$.fancybox.close();

	$.each($('#widgetContainer input[name="publishedWinNum"]'), function (idx, item){
		 winNum = $(item).val();
		 (techs[winNum] == undefined || agreeds[winNum] == undefined) ? allAssigned = false : assignedWos.push(winNum);
	 });
	 
	 //If any wos weren't assigned, remove assigned wo markers and redraw screen
	 if(allAssigned == false){
		 var removeIndex;
		$.each(this._woMarkers,function(idx,item){
			if( $.inArray(item.get("WIN_NUM"), assignedWos) != -1 ){me._woMarkerCluster.removeMarker(me._woMarkers[idx]); removeIndex = idx;}
		}); 
		this._woMarkers.splice(removeIndex,1);
		this.viewAssign();
	 }else if(allAssigned == true){
		 if(this._woMarkers.length > assignedWos.length){
			 var removeIndex;
				$.each(this._woMarkers,function(idx,item){
					if( $.inArray(item.get("WIN_NUM"), assignedWos) != -1 ){me._woMarkerCluster.removeMarker(me._woMarkers[idx]); removeIndex = idx;}
				}); 
				this.initPolygon();
				this._woMarkers.splice(removeIndex,1);
				this._goomap.setCenter(new google.maps.LatLng(38, -97));
 				this._goomap.setZoom(4);
			 $("#highlightArea .mapHeaderLink").click();
		 }else{
			 this.clearWoMarkers();
			 this.clearPolygon();
			 this.resetSelections();
			 this._goomap.setZoom(4);
			 $("#mapWorkOrders .mapHeaderLink").click();
		 }
	 }
};


FSTechMapper.prototype.viewAssign = function(){
	me = this;
	var wos = this.getWOMarkers("WIN_NUM");
	
	wosSubmit = wos.join("|");
	
	$.ajax({
		type: "POST",
		url: "/widgets/dashboard/view-assign/view/",
		dataType    : 'html',
		cache       : false,
		data: {
			company: me._companyID,
			win_nums: wosSubmit
		},
		success: function (data) {
			$("#viewAssignView").html(data);
			me.loadViewWo(2);
			me.loadViewTech(1);
		}
	});
};

FSTechMapper.prototype.useZipOnSearch = function(zip){
	if (zip === '' || zip === 'undefined' || zip === null ||  zip === 'null') return;
	
	$("#ProximityZipCode").val(zip);
	if(this._currentPopup != null){
		this._currentPopup.close();
		this._currentPopup = null;
	}
	$("#findMapTechs .mapHeaderLink").click();
	runSearch();
};

FSTechMapper.prototype.techPopupHeader = function(tech){
	var popupHtml;
	
	popupHtml = "<div class='container' style='margin-left: 15px; margin-right: auto; width: 390px;font-size: 11px;'>";
	
	popupHtml += "<div class='span-4 border'>";
	popupHtml += "<table id='techInfo'>";
	popupHtml += "<tr><td><h1>"+tech.FirstName+" "+tech.LastName+"</h1></td></tr>";
	popupHtml += "<tr><td><br /></td></tr>";
	popupHtml += "<tr><td><h1>Contact Information</h1></td></tr>";
	return popupHtml;
};

FSTechMapper.prototype.dlCallListPopupHtml = function(){
	var popupHtml;
	
	popupHtml = "<div class='container' style='margin-right: auto; width:400px;font-size: 11px;'>";
	
	popupHtml += "<div class='span-9'>";
	popupHtml += "<table id='dlCallListInfo' style='line-height:15px;'>";
	popupHtml += "<tr><td>FieldSolutions \"Call List\" provides you a list of Work Orders, each with a list of nearby Technicians that match your filter criteria.</td></tr><br />";
	popupHtml += "<tr><td><ul style='list-style-type: disc;margin-left:10px;'><li>To download a \"Call List\" for a Work Order, highlight the area around the Work Order and the surrounding techs and click the \"Download\" button.</li><br />";
	popupHtml += "<li>To download Call Lists for multiple Work Orders at once, use the Project Mapping tool in Step 2: <i>Find &amp; Map Techs</i> and highlight the area around the <i>Work Orders</i> and techs you wish to download.</li><br />";
	popupHtml += "<li>To download a list of Work Orders only, highlight the area around the Work Orders you'd like to download and select \"Work Orders Only\" below</li><br />";
	popupHtml += "</ul></td></tr>";
	popupHtml += "<tr><td><div style='text-align: left'>Select your download format:<br/><br/><input id='woCallList' type='radio' value='CallList' name='WoCallListOption' checked='checked' />Work Order \"Call List(s)\"<br/><input id='woOnly' type='radio' value='WoOnly' name='WoCallListOption' />Work Orders Only</div></td></tr>";
	popupHtml += "<tr><td><div style='text-align: left'><br/><input id='assignWosBtn' type='button' class='link_button middle2_button' value='Cancel' onclick='roll.hide()' />&nbsp;<input id='assignWosBtn' type='button' class='link_button middle2_button' value='Download' onclick='techMapper.dlCallListSubmit();'  /></div></td></tr>";
	popupHtml += "</table></div></div>";
	return popupHtml;
};

FSTechMapper.prototype.dlCallListSubmit = function (){
	
	//if(this.polygonCheck() == false) return false;
	
	var currSelected = $("#dlCallListInfo input[name='WoCallListOption']:checked").attr("id");
	var techs = "";
	var wos = "";
	var qs = "";
	me = this;
	roll.hide();
	
	if(currSelected == "woCallList"){
		//If wo call list, pass both techs and wos
		if(this._mappedAroundWos === true){
			wosArray = [];
			wosQS = "";
			$.each(this._woMarkers,function(k,w){
				if(me._polygon.containsLatLng(w.getPosition())){
					currWos = "win-"+w.get("WIN_NUM")+"[]=";
					wosPos = w.getPosition();
					$.each(me._techMarkers,function(k,t){
						if(me._polygon.containsLatLng(t.getPosition())){
							distance = google.maps.geometry.spherical.computeDistanceBetween(t.getPosition(),wosPos);
							if(distance / 1609.34 <= 50)wosArray.push(currWos+t.get("TechID"));
						}
					});
				}
			});
			qs = "/dashboard/client/mapper-download/?"+wosArray.join("&")+"&mappedAroundWos=true";
		}else{
			techs = this.getMappedTechs();
			wos = this.getMappedWos();
			qs = "/dashboard/client/mapper-download/?wos="+wos+"&techs="+techs;
		}
		
	}else{
		//pass wos only
		wos = this.getMappedWos();
		qs = "/dashboard/client/mapper-download/?wos="+wos;
	}
	qs += (this._mapAroundWosPage != "") ? "&projectMapping=true" : "&projectMapping=false";
	location = this.params.baseUrl + qs;
	return true;
};

FSTechMapper.prototype.getMappedTechs = function(){
	
	if(this.polygonCheck() == false) return false;
	
	var techs = this.getTechMarkers("TechID");
		
	var techReturn;
	if(techs.length === 0){
		
		techReturn = false;
	}else{
		techReturn = (techs.length > 1) ? techs.join("|") : techs[0];
	}
	
	return techReturn;
};

FSTechMapper.prototype.getMappedWos = function(){
	
	if(this.polygonCheck() == false) return false;
	
	var wos = this.getWOMarkers("WIN_NUM");
		
	var wosReturn;

	if(wos.length === 0){
		wosReturn = false;
	}else{
		wosReturn = (wos.length > 1) ? wos.join("|") : wos[0];
	}
	
	return wosReturn;
};

FSTechMapper.prototype.techPopupCallStatsSection = function(data, tech, techLength){
	var popupHtml;
	var preferenceLifetime;
	var preferenceTotal;
	var performanceLifetime;
	var performanceTotal;
	
	parts = techLength > 1 ? data.techStats[tech.TechID].split("|") : data.techStats[0].split("|");
		
	imacs = parts[0];
	serviceCalls = parts[1];
	backOuts = parts[2];
	noShows = parts[3];
	recommendedAvg = parts[4] == "NULL" ? 0 : parseFloat(parts[4]);
	recommendedTotal = parts[5];
	performanceAvg = parts[6] == "NULL" ? 0 : parseFloat(parts[6]);
	performanceTotal = parts[7];
	total = parseInt(imacs, 10) + parseInt(serviceCalls, 10);
	
	if(isNaN(recommendedAvg)) recommendedAvg = 0;
	if(isNaN(performanceAvg)) performanceAvg = 0;
	prefLifeTmp = tech['PreferencePercent'] != "" && tech['PreferencePercent'] != null ? tech['PreferencePercent'] : 0;
	preferenceLifetime = isNaN(prefLifeTmp) ? 0 : parseFloat(prefLifeTmp);
	
	prefTotTmp = tech['SATRecommendedTotal'] != "" && tech['SATRecommendedTotal'] != null ? tech['SATRecommendedTotal'] : 0;
	preferenceTotal = isNaN(prefTotTmp) ? 0 : parseFloat(prefTotTmp);
	
	perfLifeTmp = tech['PerformancePercent'] != "" && tech['PerformancePercent'] != null ? tech['PerformancePercent'] : 0;
	performanceLifetime = isNaN(perfLifeTmp) ? 0 : parseFloat(perfLifeTmp);
	
	perfTotTmp = tech['SATPerformanceTotal'] != "" && tech['SATPerformanceTotal'] != null ? tech['SATPerformanceTotal'] : 0;
	performanceTotal = isNaN(perfTotTmp) ? 0 : parseFloat(perfTotTmp);
	
	backOutsLT = parseFloat(tech['Back_Outs']);
	if(isNaN(backOutsLT)) backOutsLT = 0;
	
	noShowsLT = parseFloat(tech['No_Shows']);
	if(isNaN(noShowsLT)) noShowsLT = 0;
	
	totalsLT = parseInt(tech['Qty_IMAC_Calls']) + parseInt(tech['Qty_FLS_Service_Calls']);
	if(isNaN(totalsLT)) totalsLT = 0;
	
	popupHtml = "<div class='span-6 last'><table>";
	popupHtml += "<tr><td><h1 style='text-align: left;'>At-A-Glance</h1></td><td colspan=2 style='padding-left: 15px;'>FS-Tech ID#: <a href='javascript:void(0);' onclick='techMapper.showTechProfile(\""+tech.TechID+"\");'>"+tech.TechID+"</a></td></tr>";
	
	if(this._companyID === "FLS" && tech.FLSID !== ""){
		popupHtml += "<tr><td>FLSID#: "+tech.FLSID+"</td></tr>";
	}
	popupHtml += "<tr><td><br /></td></tr>";
	popupHtml += "<tr><td colspan='3'><h1 style='text-align: center;'>Work History</h1></td></tr>";
	popupHtml += "<tr><td></td><td><h2 style='text-decoration: underline;'>12 Months</h2></td><td><h2 style='text-decoration: underline;'>Lifetime</h2></td></tr>";	
	popupHtml += "<tr><td>Total Calls:</td><td id='total12Months'>"+total+"</td><td id='totalLifetime'>"+totalsLT+"</td></tr>";
	popupHtml += "<tr><td>Back Outs:</td><td id='backOuts12Months'>"+backOuts+"</td><td id='backOutsLifetime'>"+backOutsLT+"</td></tr>";
	popupHtml += "<tr><td>No Shows:</td><td id='noShows12Months'>"+noShows+"</td><td id='noShowsLifetime'>"+noShowsLT+"</td></tr>";
	popupHtml += "<tr><td>Preference:</td><td id='preference12Months'>"+recommendedAvg.toFixed(0) + "% (" + recommendedTotal + ")"+"</td><td id='preferenceLifetime'>"+preferenceLifetime.toFixed(0) + "% ("+ preferenceTotal +")"+"</td></tr>";
	popupHtml += "<tr><td>Performance:</td><td id='performance12Months'>"+performanceAvg.toFixed(0) + "% (" + performanceTotal + ")"+"</td><td id='performanceLifetime'>"+performanceLifetime.toFixed(0) + "% ("+ performanceTotal +")"+"</td></tr>";	
	popupHtml += "<tr><td colspan='3'>";
	return popupHtml;
};

FSTechMapper.prototype.setTechPopups = function(techInfo, loadMore){
	if(techInfo == null || techInfo == "")return false;
	
	var preferenceLifetime, preferenceTotal;
	var performanceLifetime, performanceTotal;
	var backOutsLT, noShowsLT, totalsLT;	
	var backOuts, noShows, total, recommendedAvg, recommendedTotal, performanceAvg, performanceTotal;
	var popupHtml;
	me = this;
	
	var submitTechs = {};
	var techLength = techInfo.length;
	
	cleanTechInfo = {};
	$.each(techInfo, function(k,tech){
		$.each(tech,function(i,t){
			if(tech[i]===null || tech[i]==="" || tech[i]===undefined)tech[i] = "";
		});
		cleanTechInfo[k] = tech;
	});
	
	$.each(techInfo,function(key, val){
		submitTechs[key] = val.TechID;
	});
	
	 $.ajax({
         type: "POST",
         url: '/ajax/techCallStats_SAT.php',
         dataType    : 'json',
         cache       : false,
         data: {id: $.param(submitTechs), mapper:true},
         success: function (data) {

        	 $.each(cleanTechInfo, function(k,tech){
        		 	popupHtml = me.techPopupHeader(tech);
        		 	
        		 	
        			if(tech.ISO_Affiliation_ID != "" && tech.ISO_Affiliation_ID != null){
        		 	
	           			 $.ajax({
	           		         type: "POST",
	           		         url: '/widgets/dashboard/client/get-iso-info/',
	           		         dataType    : 'json',
	           		         cache       : false,
	           		         data: {isoId: tech.ISO_Affiliation_ID},
	           		         success: function (isoData) {
	           		        	popupHtml += "<tr><td style='text-decoration: underline;'>ISO</td></tr>";
	           		        	popupHtml += "<tr><td>"+isoData.Address_1+"</td></tr>";
	           		        	popupHtml += "<tr><td>Contact: "+isoData.Contact_Name+"</td></tr>";
	           		        	popupHtml += "<tr><td>"+isoData.Contact_Phone+"</td></tr>";
	           		        	popupHtml += "<tr><td><br /></td></tr>";
	           		        	popupHtml += "<tr><td style='text-decoration: underline;'>Technician</td></tr>";
	      
	                			popupHtml += "<tr><td>"+tech.Address1+"</td></tr>";
	                			popupHtml += "<tr><td>"+tech.City+","+tech.State+" "+tech.Zip+"</td></tr>";
	                			popupHtml += "<tr><td></td></tr>";
	                			popupHtml += "<tr><td>Day: <span id='phoneTag'></span></td></tr>";
	                			if(tech.SecondaryPhone != "")popupHtml += "<tr><td>Eve: <span id='phoneTagEve'></span></td></tr>";
	                			popupHtml += "<tr><td><a href='mailto:"+tech.PrimaryEmail+"'>"+tech.PrimaryEmail+"</a></td></tr>";
	                			if(tech.SecondaryEmail != null)popupHtml += "<tr><td><a href='mailto:"+tech.SecondaryEmail+"'>"+tech.SecondaryEmail+"</a></td></tr>";
	                			popupHtml += "</table></div>";
	                		
	                			popupHtml += me.techPopupCallStatsSection(data, tech, techLength);
	                		
	         	            /* removing likes and comments from the popup for V1
	            				popupHtml += "<table>";
	            				popupHtml += "<tr><td><img src='/widgets/images/thumbs-up.png' width='17' height='22' /></td>";	
	            				popupHtml += "<td>Likes <span id='numLikes'></span><span id='likeButton'></span></td>";	
	            				popupHtml += "<td><img src='/widgets/images/thought-bubble.png' width='17' height='22' /></td>";			
	            				popupHtml += "<td><a href='javascript:techProfile.addComments()'>Comment on Tech</a></td></tr></table>"
	            				;	*/
	                			
	            				popupHtml += "</td></tr></table></div>";	
	            				me._techMarkerPopupContent[tech.TechID] = popupHtml;
	           		         }
	           			 });
        	 			
        			}else{
		        		popupHtml += "<tr><td><br /></td></tr>";
		        		popupHtml += "<tr><td>"+tech.Address1+"</td></tr>";
		        		popupHtml += "<tr><td>"+tech.City+","+tech.State+" "+tech.Zip+"</td></tr>";
		        		popupHtml += "<tr><td></td></tr>";
		        		popupHtml += "<tr><td>Day: <span id='phoneTag'></span></td></tr>";
		        		if(tech.SecondaryPhone != "")popupHtml += "<tr><td>Eve: <span id='phoneTagEve'></span></td></tr>";
		        		popupHtml += "<tr><td><a href='mailto:"+tech.PrimaryEmail+"'>"+tech.PrimaryEmail+"</a></td></tr>";
		        		popupHtml += "<tr><td><a href='mailto:"+tech.SecondaryEmail+"'>"+tech.SecondaryEmail+"</a></td></tr>";
		        		popupHtml += "</table></div>";
		        		
		        		
		        		popupHtml += me.techPopupCallStatsSection(data, tech, techLength);
		    			
		    			/* removing likes and comments from the popup for V1
		    				popupHtml += "<table>";
		    				popupHtml += "<tr><td><img src='/widgets/images/thumbs-up.png' width='17' height='22' /></td>";	
		    				popupHtml += "<td>Likes <span id='numLikes'></span><span id='likeButton'></span></td>";	
		    				popupHtml += "<td><img src='/widgets/images/thought-bubble.png' width='17' height='22' /></td>";			
		    				popupHtml += "<td><a href='javascript:techProfile.addComments()'>Comment on Tech</a></td></tr></table>"
		    				*/;				
		    			popupHtml += "</td></tr></table></div>";	
		    			me._techMarkerPopupContent[tech.TechID] = popupHtml;
        			}
        	 });

        	 
        	 if(currTab == "list"){
					$("#listView").show();
					$("#mapView").hide();
					$("body").data("currentSearchResults", cleanTechInfo);
					if(mapLoaded === true)me.mapTechs(cleanTechInfo, me.getCurrentSearchString(), loadMore);
				}else{
					$("#listView").hide();
					$("#mapView").show();
					if(mapLoaded === false){
						techMapper.initMap();
						mapLoaded = true;
					}
					me.mapTechs(cleanTechInfo, me.getCurrentSearchString(), loadMore);
					currentTechsMapped = true;
				}
        	 if($(".techTabs").attr("id") != "listTechs")hideLoader();
         }
	 });
};

FSTechMapper.prototype.loadMoreTechs = function(){
	var qs = $("body").data('currentQueryString', qs);
	me = this;
	
	if(this._loadMoreTechsPage === "" || this._loadMoreTechsPage === undefined){
		this._loadMoreTechsShowing = 100;
		this._loadMoreTechsPage = 2;
	}

	qs = qs+"&page="+this._loadMoreTechsPage;
	
	showLoader();
	$.ajax({
		type: "POST",
		url: "/widgets/dashboard/client/client-tech-search/",
    	dataType	: 'json',
   	 	cache		: false,
		data		: qs,
		success: function (data) {
			me._loadMoreTechsShowing =  parseInt(data.mapView.length) + me._loadMoreTechsShowing;

			$("#techsShowing").text(me._loadMoreTechsShowing);
			$("#techsTotal").text(data.mapViewTotal);
			if(data.mapViewTotal === me._loadMoreTechsShowing)$("#seeMoreTechs").hide();
			
			me.setTechPopups(data.mapView, true);
			
			hideLoader();			
			try {
				currEmails = $("#emailList").val();
				me._allEmailList = emailList;
				$("#emailList").val(currEmails+emailList);
			} catch (e) {
				$("#emailList").val("");
			}
			++me._loadMoreTechsPage;
		}
	});
};


FSTechMapper.prototype.loadViewWo = function(page) {
	if (page == undefined) page = 2;
	me = this;
	var wos = this.getWOMarkers("WIN_NUM");
		
	wosSubmit = wos.join("|");
	if(page == 2)this._woViewCount = wos.length;
	$.ajax({
		type: "POST",
		url: "/widgets/dashboard/view-assign/view-wo/",
		dataType    : 'html',
		cache       : false,
		data: {
			company: me._companyID,
			win_nums: wosSubmit,
			page: page,
			mapper: true
		},
		success: function (data) {
			techMapper.updateBundleViewAssignButtons();
			$("input[name='assignCheckBox']").unbind('click').bind('click',techMapper.updateBundleViewAssignButtons);
			$("input[name='techId']").unbind('change').bind('change',techMapper.updateBundleViewAssignButtons);
			if (data == "") return;
			woRows = $("#viewAssignWos > table > tbody");
			woRows.html(woRows.html() + data);
			this._woViewCount -= 20;
			if(this._woViewCount > 20)setTimeout("techMapper.loadViewWo(" + (++page) + ")", 1000);
		}
	});		
};

FSTechMapper.prototype.loadViewTech = function(page, qs) {
	if (page == undefined) {
		page = 1;
		$("#viewAssignTechCount").text(0);
	}
	me = this;
	var techs = this.getTechMarkers("TechID");
	techsSubmit = techs.join("|");
	queryString = "v="+me._companyID+"&techIds="+techsSubmit+"&renderAssignSelect="+true+"&page="+page+"&"+qs;
	/*if(qs != "")*/showLoader();
	$.ajax({
		type: "POST",
		url: "/widgets/dashboard/client/client-tech-search/",
		dataType    : 'json',
		cache       : false,
		data: queryString,
		success: function (data) {
			/*if(qs != "")*/hideLoader();
			if (data.mapView.length == 0) return;
			count = data.mapView.length;
			$("#viewAssignTechCount").text(count);
			techRows = $("#viewAssignTechs");
			techRows.html(data.listView);
		}
	});		
};

FSTechMapper.prototype.getWOMarkers = function(field) {
	me = this;
	var wos = [];
	$.each(this._woMarkers,function(k,v){
		if(me._polygon.containsLatLng(v.getPosition()))wos.push(v.get(field));
	});
	return wos;
}

FSTechMapper.prototype.getTechMarkers = function(field) {
	me = this;
	var techs = [];
	$.each(this._techMarkers,function(k,v){
		if(me._polygon.containsLatLng(v.getPosition()))techs.push(v.get(field));
	});
	return techs;
}

FSTechMapper.prototype.getCurrentSearchString = function(){
	var QS = "";
	
	var submitData = $('#searchForm').serializeArray();
	var tmpString = "";
	for(i in submitData){
		if(submitData[i].value != "0" && submitData[i].name != "emailList"){
			tmpString = submitData[i].name +"="+submitData[i].value+"&";
			QS += tmpString;
		}
	}
	
	return QS;
}

FSTechMapper.prototype.mapTechsAroundWos = function(more){
	var QS = this.getCurrentSearchString();
	me = this;
	
	//if($("body").data('currentQueryString') != undefined && $("body").data('currentQueryString') != ""){
	//	QS = $("body").data('currentQueryString');
	//}else{
		
	QS += "sort=" + $('body').data("sort") + "&order=" + $('body').data("order");
	//}
	
	var wos = [];
	$.each(this._woMarkers,function(k,v){
		wos.push(v.get("Lat")+"+"+v.get("Long"));
	});
	latLongs = wos.join(",");
	
	QS += "&latLongs="+latLongs;
	if(more === true){
		if(this._mapAroundWosPage === "" || this._mapAroundWosPage === undefined)this._mapAroundWosPage = 2;
		QS +="&mappedWosPage="+this._mapAroundWosPage;
	}
	
	showLoader();
	$.ajax({
		type: "POST",
		url: "/widgets/dashboard/client/client-tech-search/",
		dataType    : 'json',
		cache       : false,
		data: QS,
		success: function (data) {
			if(data.mapView === null){
				alert("No techs matched your criteria.");
				hideLoader();
				return false;
			}
			me.setTechPopups(data.mapView, false);
			$(".seeMore").hide();
			$("#projectMappingMultWos").show();
			$("#seeMoreTechs").show();
			$("#techsShowing").text(data.mapViewTotal);
			me._projectMapped = true;
			me._mappedAroundWos = true;
			if(more === true)++me._mapAroundWosPage;
			//hideLoader();
		}
	});	
};

FSTechMapper.prototype.projectMappingSubmit = function(){
	var QS = "";
	var me = this;
	var currentOption = $("#projectMappingPopup input[name='projectMappingOption']:checked").val();
	roll.hide();
	
	if(currentOption == "1"){
		this.mapTechsAroundWos();
	}else if(currentOption == "2"){
		var projects = $("#allProjects #Project").val();
		showLoader();
		$.ajax({
			type: "POST",
			url: "/widgets/dashboard/client/get-techs-by-project/",
			dataType    : 'json',
			cache       : false,
			data: {projects:projects},
			success: function (data) {
				me.setTechPopups(data.mapView, false);
				$(".seeMore").hide();
				$("#projectMappingWoAssigned").show();
				$("#techsShowing").text(data.mapView.length);
				$("#techsTotal").text(data.mapViewTotal);
				me._projectMapped = true;
				
			}
		});	
	}else{
		var projects = $("#allProjects #Project").val();
		showLoader();
		$.ajax({
			type: "POST",
			url: "/widgets/dashboard/client/get-techs-by-bids/",
			dataType    : 'json',
			cache       : false,
			data: {projects:projects},
			success: function (data) {
				me.setTechPopups(data.mapView, false);
				$(".seeMore").hide();
				$("#projectMappingProjBidders").show();
				$("#techsShowing").text(data.mapView.length);
				$("#techsTotal").text(data.mapViewTotal);
				me._projectMapped = true;
			}
		});
	}
};