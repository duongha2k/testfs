/**
 * FSWidgetProjcet
 *
 * @param Object|null params
 * @access public
 * @return void
 */
function FSWidgetProject( params ) {
	
	this.params.WOFILTERS = {};
	
    this.params.WOFILTERS.sortBy = null;
    this.params.WOFILTERS.company = null;
    this.params.WOFILTERS.sortDir = null;
    this.params.WOFILTERS.letter = null;
    this.params.WOFILTERS.hideInactive = null;
    this.params.WOFILTERS.page = null;
    this.params.WOFILTERS.download = null;
 	
    FSWidget.call(this, params);

    this.params.WIDGET.avaliableTabs = {
        list         : {module:'dashboard',controller:'project',action:'list'}
    };

    this.params.POPUP.avaliablePopups = {
    }
}

FSWidgetProject.prototype = new FSWidget();
FSWidgetProject.prototype.constructor = FSWidgetProject;

FSWidgetProject.NAME          = 'FSWidgetProject';
FSWidgetProject.VERSION       = '0.1';
FSWidgetProject.DESCRIPTION   = 'Class FSWidgetProject';

FSWidgetProject.prototype.resetFormIE = function (roll)
{
    roll.body($(this.filters().container()).html());
    calendarInit();
}


/**
 *  showPreloader
 *
 *  Function displays widget preloader
 *
 *  @param Object|null options
 */
FSWidgetProject.prototype.showPreloader = function() {
    $('body').css('cursor', 'wait');
    this.waitImage = new Image();
    this.waitImage.src = "/widgets/images/wait.gif";
    var img  = '<table width="100%" class="gradBox_table_inner">';
        img += '<tr class="table_head"><th>&#160;<br />&#160;</th></tr>';
        img += '<tr><td>&#160<br />&#160;</td></tr>';
        img += '<tr>';
        img += "<td><img style='margin:0 40%;' alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></td>";
        img += '</tr>';
        img += '<tr"><td>&#160<br />&#160;</td></tr>';
        img += '<tr class="paginator"><td class="tCenter">&#160;</td></tr>';
        img += '</table>';
    FSWidget.fillContainer(img, this);
}
/**
 *  show
 *
 *  Function displays widget
 *
 *  @param Object|null options
 */
FSWidgetProject.prototype.show = function(options) {
    if ( FSWidget.isFilterActive() ) return;

    var p = options || {params:this.getParams()};
    var queryString = '/';
    var _url;
    
    p.tab = 'list';

    // TODO params needs be reconfigured
    if ( p.params !== undefined && p.params !== null && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params ) {

            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] === true || p.params[key] === false ) {
                if ( p.params[key] === false ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] !== null && p.params[key] !== "" /*&& p.params[key].toLowerCase() !== 'none'*/ ) {
                //queryString += key + '/' + p.params[key] + '/';
            } else {
                delete p.params[key];
            }
        }
    } else {
        p.params = {};
    }

    try {
        if ( p.tab !== undefined && p.tab !== null ) {
            this.currentTab = p.tab;
            _url = this._makeUrl(p.tab, queryString);
            var idx;
            for ( idx in this.params.WIDGET.avaliableTabs ) {
                $('#'+idx).toggleClass('current', idx === this.currentTab);
            }
        } else if ( p.popup !== undefined && p.popup !== null ) {
            _url = this._makeUrl(p.popup, queryString);
        }
    } catch ( e ) {
        return false;
    }

    this.showPreloader();

    if ( this.tabOpenXhrRequest ) {
        this.tabOpenXhrRequest.abort();
        this.tabOpenXhrRequest = null;
    }

    this.tabOpenXhrRequest = jQuery.ajax({
        url : _url,
        data : p.params,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/clients/logIn.php";
            } else {
                FSWidget.fillContainer("Sorry, but an error occurred. No results found.", this);
            }
            this.tabOpenXhrRequest = null;
        },
        success : function(data, status, xhr) {
            $('body').css('cursor', '');
            FSWidget.fillContainer(data, this);
        }
    });
}
/**
 *  open
 */
FSWidgetProject.prototype.open = function(options) {
    var p = options || {};
    var queryString = '/';
    var _url;
    var noSSL = false;

    if ( p.params !== undefined && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params ) {
        	if (p.params[key] === undefined)	continue;
            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                } else if ( key === 'projects' ) {
                    var ids = '';
                    for ( var pr=0; pr < p.params[key].length; ++pr ) {
                        ids += p.params[key][pr];
                        if ( pr + 1 < p.params[key].length ) {
                            ids += '|';
                        }
                    }
                    queryString += 'projects/' + ids + '/';
                }
            } else if (p.params[key] === false || p.params[key] === true) {
                /* ok */
            } else if ( p.params[key] !== null && p.params[key] !== "" /*&& p.params[key].toLowerCase() !== 'none'*/ ) {
                if (key === "download") {
                    noSSL = true;
                } else if ( key === 'start_date' || key === 'end_date' || key === 'date_start' || key === 'date_end') {
                    p.params[key] = p.params[key].replace(/\//g, '_');
                }
                queryString += key + '/' + p.params[key] + '/';
            }
        }
    }
    try {
        _url = this._makeUrl(p.tab, queryString, noSSL);
    } catch ( e ) {
        return false;
    }

    return location = _url;
}
/**
 *  getReloadUrl
 *
 *  Return url string for safety reload current page
 *  and save all active options
 *
 *  @return string
 */
FSWidgetProject.prototype.getReloadUrl = function ( forUrl )
{
    var idx;
    var params    = this.getParams();
    var queryPart = '';
    var url;

    for ( idx in params ) {
        if ( params[idx] === "" || params[idx] === null || params[idx] === undefined || params[idx] === 'none' ) {
            delete params[idx];
            continue;
        }
        if ( params[idx] instanceof Array && !params[idx].length ) {
            delete params[idx];
            continue;
        }

        if ( idx === 'start_date' || idx === 'end_date' ) {
            queryPart += '&' + idx + '=' + encodeURIComponent(params[idx].replace(/\//g, '_')).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
        } else if ( idx === 'projects' ) {
            for ( var i = 0; i < params[idx].length; ++i ) {
                queryPart += '&projects=' + encodeURIComponent(params[idx][i]).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
            }
        } else if ( idx === 'company' ) {
            queryPart += '&v=' + encodeURIComponent(params[idx]).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');

        } else {
            queryPart += '&' + idx + '=' + encodeURIComponent(params[idx]).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
        }
    }

    url = location.toString();
    if ( queryPart.length > 0 ) {
        queryPart = queryPart.replace('&', '?');
        url = url.replace(/\?.*$/, queryPart);
        if ( url.indexOf('?') === -1 ) {
            url += queryPart;
        }
    }

    return (( !forUrl ) ?  url : encodeURIComponent(url).replace(/\~/g, '%7E').replace(/\!/g, '%21').replace(/\'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29'));
}
/**
 *  parseUrl
 *
 *  set parameters from URL
 */
FSWidgetProject.prototype.parseUrl = function ()
{
    var query       = location.toString().replace(/^[^?]+/, '').replace('?', '').replace('#.*$', '');
    var obj         = {};
    var separator   = '&';
    var i           = 0;
    var token       = [];

    if ( !query )
        return obj;
    query = query.split(separator);

    for ( i = 0; i < query.length; ++i ) {
        token = query[i].split('=');

        if ( token.length === 1 ) {
            obj[token[0]] = null;
        } else if ( token.length === 2 ) {
			token[1] = decodeURIComponent(token[1]);
            if ( obj[token[0]] === undefined ) {
                obj[token[0]] = token[1];
            } else {
                if ( obj[token[0]] instanceof Array ) {
                    obj[token[0]].push(token[1]);
                } else {
                    obj[token[0]] = [obj[token[0]], token[1]];
                }
            }
        }
    }

    if ( obj.start_date !== undefined ) {
        obj.start_date = obj.start_date.replace(/_/g, '/');
    }
    if ( obj.end_date !== undefined ) {
        obj.end_date = obj.end_date.replace(/_/g, '/');
    }

    if ( obj.date_start !== undefined ) {
        obj.date_start = obj.date_start.replace(/_/g, '/');
    }
    if ( obj.date_end !== undefined ) {
        obj.date_end = obj.date_end.replace(/_/g, '/');
    }

    return obj;
}
/**
 *  getParams
 *
 *  Function collect all params for make request
 *
 *  return object
 */
FSWidgetProject.prototype.getParams = function () {
    return {
        'sortBy'             : this.params.WOFILTERS.sortBy,
        'sortDir'              : this.params.WOFILTERS.sortDir,
        'letter'				: this.params.WOFILTERS.letter,
        'hideInactive'			: this.params.WOFILTERS.hideInactive,
        'page'	: this.params.WOFILTERS.page,
        'company' : this.params.WOFILTERS.company
        }
    }

    FSWidgetProject.prototype.setParams = function (params)
    {
        if ( !params || typeof params !== 'object' ) return;

        if (typeof(params.sortBy) != 'undefined' ) {
        	this.params.WOFILTERS.sortBy = params.sortBy;
        }
        
        if (typeof(params.sortDir) != 'undefined' ) {
        	this.params.WOFILTERS.sortDir = params.sortDir;
        }
        
        if (typeof(params.letter) != 'undefined' ) {
        	this.params.WOFILTERS.letter = params.letter;
        }
        
        if (typeof(params.hideInactive) != 'undefined' ) {
        	this.params.WOFILTERS.hideInactive = params.hideInactive;
        }
        
        if (typeof(params.page) != 'undefined' ) {
        	this.params.WOFILTERS.page = params.page;
        }
        
        if (typeof(params.company) != 'undefined' ) {
        	this.params.WOFILTERS.company = params.company;
        }
        
}