
//-----------------------------------------------------------------------------
/**
 * @fileoverview Core functionality for MVC framework, mainly dealing with
 *	event handling.  Includes IE fixes.
 *
 *	Dependencies: jQuery 1.4.2
 * 
 * @author Jason K. Flaherty
 */

//-----------------------------------------------------------------------------
//IE fixes
var IE_Object_keys = function (object) {
	var result = [];

	for(var name in object) {
		if (object.hasOwnProperty (name)) result.push (name);
	}

	return result;
}

if (!Function.prototype.bind) {
	Function.prototype.bind = function (oThis) {
		var aArgs = Array.prototype.slice.call(arguments, 1), 
			fToBind = this, 
			fNOP = function () {},
			fBound = function () {
			return fToBind.apply(this instanceof fNOP && oThis
				? this
				: oThis,
				aArgs.concat(Array.prototype.slice.call(arguments)));
			};

		fNOP.prototype = this.prototype;
		fBound.prototype = new fNOP();
 
		return fBound;
	};
}


//-----------------------------------------------------------------------------
/**
 * @class Instantiates a function object that may act as a getter/setter
 *	method, record field, validator or any other application that requires
 *	control of set values.  Triggers "assign" event when values are
 *	assigned and "change" events when the assigned value differs from the
 *	current value.
 *
 *	Event methods:
 *	onassign (value, current, reference)
 *	onchange (value, current, reference)
 */
var Property = (function () {
	return function () {
		var Property, isSet, quietSet, _func;
		var _value = arguments[0];
		var _reference = arguments[1];
		var _parent = new BaseClass ();
		var keys;

		/**
		 * @constructs
		 * @param {mixed} value Value to initialize property.  Triggers no 
		 *	events.
		 * @param {object} reference Passed to event handlers, providing a
		 *	reference to the object that the property originally belonged to
		 *	when context (this) may differ (ex: field of a collection may 
		 *	trigger the event of a DOM element).
		 */
		Property = function () {
			return _func.apply (Property, arguments);
		};

		//Polymorphism doesn't work on function objects for some browsers
		//and requires the following hack
		if (Object.keys) keys = Object.keys (_parent);
		else keys = IE_Object_keys (_parent);

		jQuery.each (keys, function (key, method) {
			Property[method] = _parent[method];
		});

		//---------------------------------------------------------------------
		/**
		 * Informs caller if property has been previously set/initialized.
		 * @return Returns true if property has a value
		 * @type boolean
		 */
		isSet = function () {
			return (typeof _value != "undefined");
		};

		//---------------------------------------------------------------------
		/**
		 * Sets value without triggering any events.
		 * @param {mixed} value New value to set to property
		 */
		quietSet = function (value) {
			_value = value;
		};

		//---------------------------------------------------------------------
		/**
		 * Getter/setter function returned by constructor.  Accepts a value to
		 *	set property to and returns the current value.  The value returned
		 *	may be different than the one passed if overridden by an event
		 *	handler.
		 * @private
		 * @param {mixed} value New value to set to property
		 * @return Result value set in property
		 * @type mixed
		 */
		_func = function (value) {
			var temp;

			if (typeof value != "undefined") {
				if (typeof this.onassign == "function")
					temp = this.onassign (value, _value, _reference);

				if (value !== _value) {
					if (typeof this.onchange == "function") {
						temp = this.onchange (value, _value, _reference);
					}
				}

				if (typeof temp != "undefined") value = temp;
				_value = value;
			}

			return _value;
		};

		//---------------------------------------------------------------------
		/**
		 * Returns the reference set in the constructor, if one was provided.
		 * @return Imprinted reference
		 * @type object
		 */
		Property.getReference = function () {
			return _reference;
		};

		Property.isSet = isSet;
		Property.quietSet = quietSet;

		return Property;
	};
}) ();

//-----------------------------------------------------------------------------
/**
 * @class Provides base functionality for objects to listen for events or
 *	halt listening.  Events may be manually triggered as well.  Uses the
 *	Multicast class for "stacking" handlers.
 */
var BaseClass = (function () {
	var BaseClass = function () {
		//---------------------------------------------------------------------
		/**
		 * Associates an "onevent" method to a Multicast object, allowing for
		 *	handlers to be stacked and later called in the order they were
		 *	added during the specified event.
		 * @param {string} event Type of event (ex: "change" or "click")
		 * @param {function} handler Function to handle event
		 * @param {object} context Instance to bind handler to
		 */
		this.listen = function (event, handler, context) {
			var _self = this;
			var method = "on" + event;
			var caller = _self[method];

			//Use object triggering the event by default
			if (typeof context == "undefined") context = this;

			handler = handler.bind (context);

			if (typeof caller == "undefined") {
				//No caller, create new method and associate with Multicast
				//call() method.
				caller = new MultiCast (handler);
				caller.bind (this);
				_self[method] = caller.call
			}
			else if (typeof caller == "function") {
				if (typeof caller.multicast != "undefined") {
					//Multicast instance already exists, just pass new handler
					caller.multicast.listen (handler);
				}
				else {
					//Method already exists, create instantiate Multicast
					//object and pass both old handler and new handler to it
					//so that original is preserved
					caller = new MultiCast (caller);
					caller.bind (this);
					caller.listen (handler)
					_self[method] = caller.call
				}
			}
		};

		//---------------------------------------------------------------------
		/**
		 * Manually triggers handlers associated with an event.
		 * @param {string} event Type of event (ex: "change" or "click")
		 * @param {...} arguments Any extra arguments are passed to handlers
		 */
		this.trigger = function (event) {
			var _self = this;
			var method = "on" + event;
			var caller = _self[method];
			var args = Array.prototype.slice.call (arguments, 1);

			if (typeof caller == "function") {
				caller.apply (this, args);
			}
		};

		//---------------------------------------------------------------------
		/**
		 * Removes handler without affecting other handlers associated with the
		 *	event.
		 * @param {string} event Type of event (ex: "change" or "click")
		 * @param {function} handler handler Function to handle event
		 */
		this.halt = function (event, handler) {
			var _self = this;
			var method = "on" + event;
			var caller = _self[method];

			if (typeof caller == "function" && caller.multicast) {
				caller.multicast.ignore (handler);
			}
		};
	};

	return BaseClass;
}) ();

//-----------------------------------------------------------------------------
/**
 * @class Function container, allowing multiple functions to be called at 
 * 	once, each receiving the original parameters passed.
 */
var MultiCast = (function () {
	var _resultmethod_onchange;

	//-------------------------------------------------------------------------
	/**
	 * @constructs
	 * @param {...} arguments Any number of listener functions
	 */
	var MultiCast = function () {
		var _self = this;
		var init_args = Array.prototype.slice.call(arguments, 0);

		this._listeners = new Array ();
		this._binding = false;
		this.resultMethod = new Property ("chain");
		this.resultMethod.onchange = _resultmethod_onchange;

		//---------------------------------------------------------------------
		/**
		 * Calls all function contained in the current instance, passing any
		 *	arguments to this method to each function.  Any return values from
		 *	these functions are appended to this argument list and passed to
		 *	eacg subsequent function.
		 * @param {...} arguments Arguments to pass to contained functions
		 * @return Last return value from function in set (undefined is ignored)
		 *	is returned from this method
		 * @type mixed
		 */
		this.call = function () {
			var args, result, temp;

			for (var i = 0; i < _self._listeners.length; i++) {
				args = Array.prototype.slice.call(arguments, 0);
				if (typeof (result) != "undefined") args.push (result);

				if (_self._binding === false) {
					temp = _self._listeners[i].apply (this, args);
				}
				else {
					temp = _self._listeners[i].apply (_self._binding, args);
				}

				if (typeof temp != "undefined") result = temp;
			}

			return result;
		};

		this.call.multicast = this;

		jQuery.each (init_args, function (key, listener) {
			if (typeof listener != "undefined")
				this.listen (listener);
		}.bind (this));
	};

	//-------------------------------------------------------------------------
	/**
	 * Adds a listener function to Multicast container.
	 * @param {function} listener Function to be added to listen for call
	 * @throws {TypeError} Parameter 1 must be a function
	 * @return Returns the Multicast object, allowing for call chains
	 * @type Multicast
	 */
	MultiCast.prototype.listen = function (listener) {
		if (typeof listener == "function") {
			//if (listener.multicast && listener.multicast != this) {
			//	listener.multicast.combine (pvt);
			//}
			//else {
				if (typeof listener.ignore != "function") {
					listener.ignore = function () {
						this.ignore (listener);
					}.bind (this);
				}

				this._listeners.push (listener);
			//}
		}
		else {
			throw new TypeError ("Multicast.listen(): Parameter 1 " +
				"must be a function");
		}

		return this;
	};

	//-------------------------------------------------------------------------
	/**
	 * Removes listener function from Multicast container
	 * @param {function} listener Function to be removed
	 * @throws {TypeError} Parameter 1 must be a function
	 * @return Returns the Multicast object, allowing for call chains
	 * @type Multicast
	 */
	MultiCast.prototype.ignore = function (listener) {
		var index = false;

		if (typeof listener == "function") {
			for (var i = 0; i < this._listeners.length; i++) {
				if (this._listeners[i] == listener) index = i;
			}

			if (index !== false) this._listeners.splice (index, 1);
		}
		else if (typeof listener == "number") {
			this._listeners.splice (listener, 1);
		}
		else {
			throw new TypeError ("Multicast.ignore(): Parameter 1 " +
				"must be a function or a numerical index");
		}

		return this;
	};

	//-------------------------------------------------------------------------
	/**
	 * Combines two multicasts, allowing each to control the same container.
	 * @param {object} mcast_private Private members of target Multicast
	 */
	MultiCast.prototype.combine = function (mcast_private) {
		for (var i = 0; i < mcast_private._listeners.length; i++) {
			this._listeners.push (mcast_private._listeners[i]);
		}

		mcast_private._listeners = this._listeners;
	};

	//-------------------------------------------------------------------------
	/**
	 * Returns the number of listener functions in Multicast container.
	 * @return Length of container
	 * @type int
	 */
	MultiCast.prototype.count = function () {
		return this._listeners.length;
	};

	//-------------------------------------------------------------------------
	/**
	 * Binds an object to all calls in Multicast.
	 * @param {object} Object to bind to all function calls in Multicast
	 * @return Returns the Multicast object, allowing for call chains
	 * @type Multicast
	 */
	MultiCast.prototype.bind = function (object) {
		this._binding = object;

		return this;
	};

	//-------------------------------------------------------------------------
	/**
	 * Selects a result handler method when resultMethod property is set.
	 * @memberOf Multicast.prototype
	 * @param {String} value Name of method
	 * @return Formatted name of method
	 * @type String
	 */
	_resultmethod_onchange = function (value) {
		return (value | "chain").toString ().toLowerCase ();
	};

	return MultiCast;
}) ();
