function FSTechWorkOrderToolBar(params, roll ) {
        FSWidget.call(this, params);

        this.params.POPUP.avaliablePopups = {
            print        : {module:'dashboard',controller:'tech',action:'print-popup'},
            documents    : {module:'dashboard',controller:'tech',action:'documents'},
            filedownload : {module:'dashboard',controller:'tech',action:'file-wos-documents-download'}
        };
        this.params.WIDGET.avaliableTabs = {
            index        : {module:'dashboard',controller:'tech',action:'get-print-layout'}
        };
	this._roll = roll;
}


FSTechWorkOrderToolBar.prototype = new FSWidget();
FSTechWorkOrderToolBar.prototype.constructor = FSTechWorkOrderToolBar;

FSTechWorkOrderToolBar.NAME          = 'FSTechWorkOrderToolBar';
FSTechWorkOrderToolBar.VERSION       = '0.1';
FSTechWorkOrderToolBar.DESCRIPTION   = 'Class FSTechWorkOrderToolBar';

FSTechWorkOrderToolBar.prototype.printHtml = function(params) {
    
    html = '<form name="printWOForm" id="printWOForm" method="post" action="/">';
    html += '<div class="filter_popup"><b>WIN#: '+params.win+' | '+params.WO_ID+' | '+params.category+' | '+params.headline+' </b>';
    html += '<br />Select an option for printing.<br /><br /><div style="padding-left:20px;">';
    html += '<input type="hidden" id="printWO_win" name="win" value="'+params.win+'" />';
	if (jQuery("input[name=signoff_disabled]").val() != "1") {
		params.visitcount = parseInt(params.visitcount);
		if(params.visitcount > 1){
			for(var i= 1; i<= params.visitcount;i++){
				html += '<div style="padding-bottom: 8px;"><input type="checkbox" class="printWO_Sign" name="Sign" visit='+i+' value="1" /><label for="printWO_Sign"> Print Basic Sign off Page (Visit #'+i+')</label></div>';
			}
		}else{
			html += '<div style="padding-bottom: 8px;"><input type="checkbox" class="printWO_Sign" name="Sign" visit="1" value="1" /><label for="printWO_Sign"> Print Basic Sign off Page</label></div>';
		}
	}
	else {
		html += '<div style="padding-bottom: 8px; color: #A0A0A0;"><input type="checkbox" class="printWO_Sign" name="Sign" visit="1" value="1" disabled="disabled" /><label for="printWO_Sign"> Print Basic Sign off Page</label></div>';
	}
    html += '<div style="padding-bottom: 8px;"><input type="checkbox" id="printWO_WO" name="WO" value="1" /><label for="printWO_WO"> Print Work Order &nbsp;<span style="color: #000000;">(Not a signoff sheet)</span></label></div>';
    html += '<div style="padding-bottom: 8px;"><input type="checkbox" id="printWO_Map" name="Map" value="1" /><label for="printWO_Map"> Print Work Order with Map <span style="color: #000000;">(Not a signoff sheet)</span></label></div>';
    html += '</div><br /><div style="text-align:center;"><input type="submit" class="link_button_popup" value="Print" onclick="toolBar.showPrint(); return false;" />';
    html += '</div></form>';
    return html;
}

FSTechWorkOrderToolBar.prototype.showPrint = function() {
    data = $("#printWOForm").serialize();
    if ($("#printWO_WO").attr('checked')) {
        var _data = data;
        if($("#printWO_Map").attr('checked')){
            _data = _data.replace('Map=1', 'Map=0');
        }
        openPopupWin('/widgets/dashboard/tech/print-wo/?'+_data,null,{menubar:'yes'});
    }
    if ($("#printWO_Map").attr('checked')) {
        openPopupWin('/widgets/dashboard/tech/print-wo/?'+data,null,{menubar:'yes'});
    }
    if ($(".printWO_Sign:checked").length>0) {
        jQuery(".printWO_Sign").each(function(){
            if(jQuery(this).is(":checked")){
               var visit = jQuery(this).attr("visit");
               openPopupWin('/widgets/dashboard/tech/print-sign/?'+data+"&visit="+visit,null,{menubar:'yes'});  
            }
        })
    }
    if ($("#printWO_Sign").attr('checked')) {
        openPopupWin('/widgets/dashboard/tech/print-sign/?'+data,null,{menubar:'yes'});
    }
    roll.hide();
}
/**
 *  open
 */
FSTechWorkOrderToolBar.prototype.open = function(options) {
    var p = options || {};
    var queryString = '/';
    var _url;
    var noSSL = false;

    if ( p.params !== undefined && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params )
            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                } else if ( key === 'projects' ) {
                    var ids = '';
                    for ( var pr=0; pr < p.params[key].length; ++pr ) {
                        ids += p.params[key][pr];
                        if ( pr + 1 < p.params[key].length ) {
                            ids += '|';
                        }
                    }
                    queryString += 'projects/' + ids + '/';
                }
            } else if (p.params[key] === false || p.params[key] === true) {
                /* ok */
            } else if ( p.params[key] !== undefined && p.params[key] !== null && p.params[key] !== "" && p.params[key].toLowerCase() !== 'none' ) {
                if (key === "download") {
                    noSSL = true;
                } else if ( key === 'start_date' || key === 'end_date' ) {
                    p.params[key] = p.params[key].replace(/\//g, '_');
                }
                queryString += key + '/' + p.params[key] + '/';
            }
    }
    try {
        _url = this._makeUrl(p.tab, queryString, noSSL);
    } catch ( e ) {
        return false;
    }
    
    return location = _url;
}
