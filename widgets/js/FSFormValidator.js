function FSFormValidator()
{
    this._regex     = [];
    this._checked   = [];
    this._notempty  = [];

    this._filters   = null;

    this._errors = [];
}

function ruleItem(obj, rule, msg) {
    this.obj    = obj;
    this.rule   = rule;
    this.msg    = msg;
}

FSFormValidator.prototype.registerFilter = function( obj )
{
    if ( obj instanceof FSFormFilter ) {
        this._filters = obj;
    }
}

FSFormValidator.prototype.addRegex = function(obj, rule, msg)
{
    this._regex.push(new ruleItem(obj, rule, msg));
}

FSFormValidator.prototype.addChecked = function(obj, bool, msg)
{
    this._checked.push(new ruleItem(obj, bool, msg));
}

FSFormValidator.prototype.notEmpty = function(obj, msg)
{
    this._notempty.push(new ruleItem(obj, null, msg));
}

FSFormValidator.prototype.clearRules = function()
{
    this._regex     = [];
    this._checked   = [];
    this._notempty  = [];
}

FSFormValidator.prototype.clearErrors = function()
{
    this._errors = [];
}

FSFormValidator.prototype.isValid = function()
{
    var idx;
    this.clearErrors();

    for ( idx = 0; idx < this._regex.length; ++idx ) {
        this._checkRegex(this._regex[idx]);
    }
    for ( idx = 0; idx < this._checked.length; ++idx ) {
        this._checkChecked(this._checked[idx]);
    }
    for ( idx = 0; idx < this._notempty.length; ++idx ) {
        this._checkNotEmpty(this._notempty[idx]);
    }

    return (this._errors.length === 0);
}

FSFormValidator.prototype.getErrors = function()
{
    return this._errors;
}

FSFormValidator.prototype._checkRegex = function( r )
{
    var value;
    if ( typeof r.obj === 'string' ) {
        r.obj = $(r.obj);
    }
    if( typeof r.obj === 'object'){
        if ( !r.rule instanceof RegExp ) {
            r.rule = new RegExp(r.rule, "");
        }

        if ( this._filters != null && this._filters.isRegistered(r.obj) ) {
            value = this._filters.getValue(r.obj);
        } else {
            value = r.obj.val();
        }

        if ( value && !r.rule.test(value) && typeof(r.msg) === "string" && r.msg.length > 0 ) {
            this._errors.push(r.msg);
        }
    }
}

FSFormValidator.prototype._checkChecked = function( r )
{
    if ( typeof r.obj === 'string' ) {
        r.obj = $(r.obj);
    }
    if( typeof r.obj === 'object'){
        if ( !r.obj.attr('checked') && r.rule && typeof(r.msg) === "string" && r.msg.length > 0 ) {
            this._errors.push(r.msg);
        } else if ( r.obj.attr('checked') && !r.rule && typeof(r.msg) === "string" && r.msg.length > 0 ) {
            this._errors.push(r.msg);
        }
    }
}

FSFormValidator.prototype._checkNotEmpty = function( r )
{
    var value;

    if ( typeof r.obj === 'string' ) {
        r.obj = $(r.obj);
    }
    if( typeof r.obj === 'object'){
        if ( this._filters != null && this._filters.isRegistered(r.obj) ) {
            value = this._filters.getValue(r.obj);
        } else {
            value = r.obj.val();
        }

        if ( $.trim(value.toString()) == "" && typeof(r.msg) === "string" && r.msg.length > 0 ) {
            this._errors.push(r.msg);
        }
    }
}

