/**
 * FSWidgetWODetails
 *
 * @param Object|null params
 * @access public
 * @return void
 */
function FSWidgetAdminProjectDetails ( params, roll ) {
    FSWidget.call(this, params);

    this.params.POPUP.avaliablePopups = {
    };
    this.params.WIDGET.avaliableTabs = {
        pricingRule : {module:'admindashboard',controller:'project',action:'pricing-rule'},
    };
    this._roll = roll;
    this.uploadFields = [];
    this.waitImage = new Image();
    this.waitImage.src = "/widgets/images/progress.gif";     //  pre load wait image
    this.waitImage.width = 190;
    this.waitImage.height = 14;

    this._activeupload = 0;
    this._uploadsCounter = 0;
    this._updatestatus = {success:1, errors:[]};
    this._redirect = false;
    this._reftab = params.reftab;

    this._customfunctions = params.customfunctions || {};
    
    this.formType = params.tab;
    this.fromEmail = null;
    this.allowSubmit = this.formType == "create";
    this.windowSettings = "menubar=0, width=400, height=400, location=0, toolbar=0";
    this.oldReminderAll = false;
    this.oldSMSBlast = false;
    this.oldReminderAcceptance = false;
    this.oldReminder24Hr = false;
    this.oldReminder1Hr = false;
    this.oldCheckInCall = false;
    this.oldCheckOutCall = false;
    this.oldReminderNotMarkComplete = false;
    this.oldReminderIncomplete = false;

    this.oldIsProjectAutoAssign = false;
    this.oldIsWorkOrdersFirstBidder = false;
    this.oldP2TPreferredOnly = false;
    this.oldMinutesRemainPublished = "";
    this.oldMinimumSelfRating = "";
    this.oldMaximumAllowableDistance = "";
    this.blastFromAddressDefault = 'no-replies@fieldsolutions.us';
}

FSWidgetAdminProjectDetails.prototype = new FSWidget();
FSWidgetAdminProjectDetails.prototype.constructor = FSWidgetAdminProjectDetails;

FSWidgetAdminProjectDetails.NAME          = 'FSWidgetWODetails';
FSWidgetAdminProjectDetails.VERSION       = '0.1';
FSWidgetAdminProjectDetails.DESCRIPTION   = 'Class FSWidgetWODetails';

FSWidgetAdminProjectDetails.prototype.roll = function() {
    return this._roll;
}

/**
 *  open
 */
FSWidgetAdminProjectDetails.prototype.open = function(options) {
    var p = options || {};
    var queryString = '/';
    var _url;
    var noSSL = false;

    if ( p.params !== undefined && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params )
            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                } else if ( key === 'projects' ) {
                    var ids = '';
                    for ( var pr=0; pr < p.params[key].length; ++pr ) {
                        ids += p.params[key][pr];
                        if ( pr + 1 < p.params[key].length ) {
                            ids += '|';
                        }
                    }
                    queryString += 'projects/' + ids + '/';
                }
            } else if (p.params[key] === false || p.params[key] === true) {
                /* ok */
            } else if ( p.params[key] !== null && p.params[key] !== "" && p.params[key].toLowerCase() !== 'none' ) {
                if (key === "download") {
                    noSSL = true;
                } else if ( key === 'start_date' || key === 'end_date' ) {
                    p.params[key] = p.params[key].replace(/\//g, '_');
                }
                queryString += key + '/' + p.params[key] + '/';
            }
    }
    try {
        _url = this._makeUrl(p.tab, queryString, noSSL);
    } catch ( e ) {
        return false;
    }
    return location = _url;
}

/**
 *  show
 *
 *  Function display widget
 *
 *  @param Object|null options
 */

FSWidgetAdminProjectDetails.prototype.show = function(options) {
    var p = options || {tab:this.currentTab,params:this.getParams()};

    var queryString = '/';
    var _url;

    // TODO params needs be reconfigured
    if ( p.params !== undefined && p.params !== null && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params ) {
            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] === true || p.params[key] === false ) {
                if ( p.params[key] === false ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] !== null && p.params[key] !== "" && p.params[key] !== 'none' ) {
                queryString += key + '/' + p.params[key] + '/';
            } else {
                delete p.params[key];
            }
        }
    } else {
        p.params = {};
    }

    _url = this._makeUrl(this.currentTab, queryString);

    this.showPreloader();

    if ( this.tabOpenXhrRequest ) {
        this.tabOpenXhrRequest.abort();
        this.tabOpenXhrRequest = null;
    }

    this.tabOpenXhrRequest = $.ajax({
        url : _url,
        data : p.params,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/admin/logIn.php";
            } else {
                FSWidget.fillContainer("Sorry, but an error occurred. No results found.", this);
            }
            this.tabOpenXhrRequest = null;
            
        },
        success : function(data, status, xhr) {
            $('body').css('cursor', '');
            FSWidget.fillContainer(data, this);
            this.prepareForm();
        }
    });

}
FSWidgetAdminProjectDetails.prototype.prepareForm = function() {
    $("#Default_Amount").blur(this.massageMyMoney);
    $("#Default_Amount").blur();
    $("#Mod0CancelRecord").click(function() { backClicked = true; });
    
    $("#radio1").click(this.toggleFirstBidder);
    $("#radio2").click(this.toggleFirstBidder);
    $("#radio3").click(this.togglePreferredOnly);
    $("#radio4").click(this.togglePreferredOnly);
    
    if(this.formType == "create"){

    }else{
        $("#acsAuthBy").html($("#ACSAuthBy").val());
        this.oldReminderAll = $("#ReminderAll").attr("checked");
        this.oldSMSBlast = $("#AutoSMSOnPublish").attr("checked");
        this.oldReminderAcceptance = $("#ReminderAcceptance").attr("checked");
        this.oldReminder24Hr = $("#Reminder24Hr").attr("checked");
        this.oldReminder1Hr = $("#Reminder1Hr").attr("checked");
        this.oldCheckInCall = $("#CheckInCall").attr("checked");
        this.oldCheckOutCall = $("#CheckOutCall").attr("checked");
        this.oldReminderNotMarkComplete = $("#ReminderNotMarkComplete").attr("checked");
        this.oldReminderIncomplete = $("#ReminderIncomplete").attr("checked");

        this.oldIsProjectAutoAssign = $("#isProjectAutoAssign").attr("checked");
        this.oldIsWorkOrdersFirstBidder = $("#isWorkOrdersFirstBidder").attr("checked");
        this.oldP2TPreferredOnly = $("#P2TPreferredOnly").attr("checked");
        this.oldMinutesRemainPublished = $("#MinutesRemainPublished").val();
        this.oldMinimumSelfRating = $("#MinimumSelfRating").val();
        this.oldMaximumAllowableDistance = $("#MaximumAllowableDistance").val();

        minutesRemainPublished = $("#MinutesRemainPublished").val();
        if (minutesRemainPublished != "") {
            days = Math.floor(minutesRemainPublished / 1440.0);
            minutesRemainPublished -= days * 1440;
            hours = Math.floor(minutesRemainPublished / 60.0);
            minutesRemainPublished -= hours * 60;
            minutes = minutesRemainPublished;
            if(isNaN(days))days = 0;
            if(isNaN(hours))hours = 0;
            if(isNaN(minutes))minutes = 0;
            $("#daysRemainPublished").attr("value", days);
            $("#hoursRemainPublished").attr("value", hours);
            $("#minutesRemainPublished_virt").attr("value", minutes);
        }
    }

    oldPcntDeduct = $("#PcntDeduct").attr("checked");

    $("#daysRemainPublished").change(this.updateMinutesRemainPublished);
    $("#hoursRemainPublished").change(this.updateMinutesRemainPublished);
    $("#minutesRemainPublished_virt").change(this.updateMinutesRemainPublished);
    $("#isProjectAutoAssign").click(this.toggleP2T);

    $("#Client_ID").change(this.ajaxFunction);
    $("#Client_ID").change(this.ajaxFunction);
    $("#acceptReplies").click(this.acceptReplyClick);
    $("#ReminderAll").click(this.IVRToggleAll);

    if ($("#isProjectAutoAssign").attr("checked"))
        this.toggleP2T();
    else
        this.toggleP2TFields(false);

    this.IVRToggleAll();

    this.fromEmail = $("#RecruitmentFromEmail");
    this.fromEmail.focus(this.recruitmentEmailFocus);
    this.fromEmail.blur(this.recruitmentEmailBlur);
    $("#CreateProjectForm").submit(this.validateSubmit);

    this.recruitmentEmailBlur();
}

FSWidgetAdminProjectDetails.prototype.showPreloader = function() {
    if (this._customfunctions.showPreloader !== undefined && typeof this._customfunctions.showPreloader === 'function' ) {
        this._customfunctions.showPreloader();
    }else{
        $('body').css('cursor', 'wait');
        var img = "<div class='tCenter' ><img style='margin:10% auto;' alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></div>";
        FSWidget.fillContainer(img, this);
    }
}

FSWidgetAdminProjectDetails.prototype.updateDetails = function() {
    this.showStartPopup();
    this._redirect =false;
    this._updatestatus = {success:1, errors:[]};
    this._activeupload = 0;
    params = $('#CreateProjectForm').serialize();
    params += '&Project_Company_ID='+window._company;
    $.ajax({
        url         : '/widgets/admindashboard/project/do-update/',
        dataType    : 'json',
        data        : params,
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            this._updatestatus = data;
            if (data.success) {
                this._uploadsCounter = 0;
                this.params.project_id = data.project_id;

                this._redirect = true;
                for (var i in this.uploadFields ) {
                    if ( $('#'+this.uploadFields[i]).val() ) {
                        this._uploadsCounter++;
                        this.asyncUploadFile( this.uploadFields[i], 'update' );
                    }
                }

                //no files to upload
                if ( this._uploadsCounter <= 0 ) {
                    this.showMessages(this._updatestatus);
                }
            }else{
                this.showMessages(data);
            }
        }
    });
}

FSWidgetAdminProjectDetails.prototype.showStartPopup = function() {
    var html = "<div class=\"tCenter\"><img alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></div>";
    this._roll.autohide(false);
    var opt = {
        width       : 300,
        height      : '',
        position    : 'middle',
        title       : 'Processing',
        body        : html
    };
    return this._roll.showNotAjax(null, null, opt);
}

FSWidgetAdminProjectDetails.prototype.showMessages = function(data) {
    var html = '';
    if (this._redirect || data.success) { //always refresh page on success
        if (window.opener) {try{window.opener.reloadTabFrame();} catch (e) {}}
        window.location = '/admin/adminClientProjectList.php?v='+window._company;
    }
    if (data.success) {
        html += '<div class="success"><ul><li>Success!</li></ul></div>';
    }else{
        html+='<div class="errors"><ul>';
        for (i=0;i<data.errors.length;i++) {
            if(typeof data.errors[i] === 'object')
                html+='<li>'+data.errors[i].message+'</li>';
            else
                html+='<li>'+data.errors[i]+'</li>';
        }
        html+='</ul></div>';
    }
    html+='<div class="tCenter"><input type="button" class="link_button_popup" onclick="detailsWidget.roll().hide();" value="Ok" /></div>';
    this._roll.autohide(false);
    var opt = {
        width       : 400,
        height      : '',
        position    : 'middle',
        title       : '',
        body        : html
    };
    if (data.success) {
        opt.width = 300;
    }
    return this._roll.showNotAjax(null, null, opt);
}

FSWidgetAdminProjectDetails.prototype.initPhone = function(){
	var formatPhone = function() {
		if (this.value == null || this.value == "") return;
		validFormat = /^[(]?(\d{3})[)]?(?:\s|-|.)*(\d{3})[-\s.]*(\d{4})$/;
		parts = this.value.match(validFormat);
		if (parts == null) return;
		this.value = "(" + parts[1] + ") " + parts[2] + "-" + parts[3];
        };

	$.map(['edit_SitePhone', 'edit_SiteFax'], function( id ) {
		$('#'+id).each(formatPhone).blur(formatPhone);
	});
}

FSWidgetAdminProjectDetails.prototype.confirmRemoveFile = function( obj, file_name )
{
    if ( $(obj).attr('checked') ) {
        if ( confirm('Are you sure you wish to remove the file "' + file_name + '"?') ) {
            $(obj).attr('checked', true);
        } else {
            $(obj).attr('checked', false);
        }
    }
}

/**
 *  getParams
 *
 *  Function collect all params for make request
 *
 *  return object
 */
FSWidgetAdminProjectDetails.prototype.getParams = function () {
    return {
        'tab'               : this.currentTab,
        'company'           : window._company,
        'win'               : this.params.win
    }
}

FSWidgetAdminProjectDetails.prototype.ajaxFunction = function() {
	// lookup client information
    var clientID = $('#Client_ID').val();
	$.get("ajax/clientLookup.php", {"clientID" : clientID},
		function(data) {
			var getClientInfo = data.split(",");
			$('#Client_Company_Name').val(getClientInfo[0]);
			$('#Client_Name').val(getClientInfo[1]);
			$('#Client_Email').val(getClientInfo[2]);
		}
	);
}

FSWidgetAdminProjectDetails.prototype.acceptReplyClick = function() {
	if (!$('#acceptReplies').attr('checked'))
		detailsWidget.fromEmail.attr("value", detailsWidget.blastFromAddressDefault);
}

FSWidgetAdminProjectDetails.prototype.recruitmentEmailBlur = function() {
	if (detailsWidget.fromEmail.attr("value") == undefined)
		detailsWidget.fromEmail.attr("value", detailsWidget.blastFromAddressDefault);

	if (detailsWidget.fromEmail.attr("value") == detailsWidget.blastFromAddressDefault)
		$('#acceptReplies').attr("checked", false);
	else
		$('#acceptReplies').attr("checked", true);
}

FSWidgetAdminProjectDetails.prototype.recruitmentEmailFocus = function() {
	if (detailsWidget.fromEmail.attr("value") == detailsWidget.blastFromAddressDefault)
		detailsWidget.fromEmail.attr("value", "");
	$('#acceptReplies').attr("checked", true);
}

FSWidgetAdminProjectDetails.prototype.massageMyMoney = function() {
	this.value = massageMoney(this.value);
}

FSWidgetAdminProjectDetails.prototype.showACSUpdateOptions = function() {
	//updateOptionsWindow = window.open("/clients/acsUpdateOptions.php?id="+$('#Project_ID').val()+"&user="+currentUser,"ACSUpdateOptions",this.windowSettings);
}

FSWidgetAdminProjectDetails.prototype.updateP2T = function() {
	//updateP2TWindow = window.open("/clients/p2tUpdateOptions.php?id="+$('#Project_ID').val()+"&user="+currentUser,"P2TUpdateOptions",this.windowSettings);
}

FSWidgetAdminProjectDetails.prototype.updateMinutesRemainPublished = function() {
	d =	$("#daysRemainPublished").val();
	h = $("#hoursRemainPublished").val();
	m = $("#minutesRemainPublished_virt").val();

	d = parseInt(d, 10);
	h = parseInt(h, 10);
	m = parseInt(m, 10);

	if (isNaN(d)) {
		d = 0;
		$("#daysRemainPublished").val(d);
	}

	if (isNaN(h)) {
		h = 0;
		$("#hoursRemainPublished").val(h);
	}

	if (isNaN(m)) {
		m = 0;
		$("#minutesRemainPublished_virt").val(m);
	}

	remain = d *1440 + h * 60 + m;
	$("#MinutesRemainPublished").attr("value", remain);
}

FSWidgetAdminProjectDetails.prototype.isValidContactPhone = function(val, allowExt) {
	if (val == null || val == "") return true;
	valStrip = val.replace(/[^0-9]/g, "");
	if (valStrip.length == 10) {
		val = massagePhone(val);
		return isValidPhone(val);
	}
	else {
		if (!allowExt) return false;
		validFormat = /^[(]?(\d{3})[)]?(?:\s|-|.)*(\d{3})[-\s.]*(\d{4})[^0-9]+([0-9]*)[^0-9]*([0-9]*)$/;
		parts = val.match(validFormat);
		return !(parts == null);
	}
}

FSWidgetAdminProjectDetails.prototype.toggleFirstBidder = function() {

    $("#isWorkOrdersFirstBidder").attr("checked", $("#radio1").attr("checked"));
    alert($("#isWorkOrdersFirstBidder").attr("checked"));
}

FSWidgetAdminProjectDetails.prototype.togglePreferredOnly = function() {
	$("#P2TPreferredOnly").attr("checked", $("#radio3").attr("checked"));
    alert($("#P2TPreferredOnly").attr("checked"));
}

FSWidgetAdminProjectDetails.prototype.IVRToggleAll = function() {
	var allChecked = $("#ReminderAll").attr("checked") ? true : false;
	if (allChecked) {
		$("#ReminderAcceptance").attr("checked", false);
		$("#AutoSMSOnPublish").attr("checked", false);
		$("#Reminder24Hr").attr("checked", false);
		$("#Reminder1Hr").attr("checked", false);
		$("#CheckInCall").attr("checked", false);
		$("#CheckOutCall").attr("checked", false);
		$("#ReminderNotMarkComplete").attr("checked", false);
		$("#ReminderIncomplete").attr("checked", false);
	}

	$("#ReminderAcceptance").attr("disabled", allChecked);
	$("#AutoSMSOnPublish").attr("disabled", allChecked);
	$("#Reminder24Hr").attr("disabled", allChecked);
	$("#Reminder1Hr").attr("disabled", allChecked);
	$("#CheckInCall").attr("disabled", allChecked);
	$("#CheckOutCall").attr("disabled", allChecked);
	$("#ReminderNotMarkComplete").attr("disabled", allChecked);
	$("#ReminderIncomplete").attr("disabled", allChecked);
}

FSWidgetAdminProjectDetails.prototype.toggleP2T = function() {
	var val = $("#isProjectAutoAssign").attr("checked") ? true : false;
	detailsWidget.toggleP2TFields(val);

    if(val && !$("#ReminderAll").attr("checked")) {
        $("#ReminderAll").attr("disabled", false);
        $("#ReminderAll").attr("checked", false);

        $("#ReminderAcceptance").attr("checked", val);
        $("#AutoSMSOnPublish").attr("checked", val);
        $("#Reminder24Hr").attr("checked", val);
        $("#Reminder1Hr").attr("checked", val);

        $("#ReminderAcceptance").attr("disabled", false);
        $("#AutoSMSOnPublish").attr("disabled", false);
        $("#Reminder24Hr").attr("disabled", false);
        $("#Reminder1Hr").attr("disabled", false);
        $("#CheckInCall").attr("disabled", false);
        $("#CheckOutCall").attr("disabled", false);
        $("#ReminderNotMarkComplete").attr("disabled", false);
        $("#ReminderIncomplete").attr("disabled", false);
    }

}

FSWidgetAdminProjectDetails.prototype.toggleP2TFields = function(val) {
    $("#radio1").attr("disabled", !val);
    $("#radio2").attr("disabled", !val);
    $("#radio3").attr("disabled", !val);
    $("#radio4").attr("disabled", !val);

	$("#daysRemainPublished").attr("disabled", !val);
	$("#hoursRemainPublished").attr("disabled", !val);
	$("#minutesRemainPublished_virt").attr("disabled", !val);

	$("#MinimumSelfRating").attr("disabled", !val);
	$("#MaximumAllowableDistance").attr("disabled", !val);

	$("#NotifyOnAutoAssign").attr("disabled", !val);
	if (!val)
		$("#NotifyOnAutoAssign").attr("checked", false);
}

FSWidgetAdminProjectDetails.prototype.didP2TChange = function() {
	return this.oldIsProjectAutoAssign != $("#isProjectAutoAssign").attr("checked") || this.oldIsWorkOrdersFirstBidder != $("#isWorkOrdersFirstBidder").attr("checked") ||
        this.oldP2TPreferredOnly != $("#P2TPreferredOnly").attr("checked") || this.oldMinutesRemainPublished != $("#MinutesRemainPublished").val() || this.oldMinimumSelfRating != $("#MinimumSelfRating").val() || this.oldMaximumAllowableDistance != $("#MaximumAllowableDistance").val();
}

FSWidgetAdminProjectDetails.prototype.didACSChange = function() {
	return this.oldReminderAll != $("#ReminderAll").attr("checked") || this.oldReminderAcceptance != $("#ReminderAcceptance").attr("checked") ||
		this.oldSMSBlast != $("#AutoSMSOnPublish").attr("checked") || this.oldReminder24Hr != $("#Reminder24Hr").attr("checked") ||
		this.oldReminder1Hr != $("#Reminder1Hr").attr("checked") || this.oldCheckInCall != $("#CheckInCall").attr("checked") ||
		this.oldCheckOutCall != $("#CheckOutCall").attr("checked") || this.oldReminderNotMarkComplete != $("#ReminderNotMarkComplete").attr("checked") ||
		this.oldReminderIncomplete != $("#ReminderIncomplete").attr("checked");
}

FSWidgetAdminProjectDetails.prototype.validEmailList = function(list) {
	if (list == null || list == "") return false;
	list = list.replace(/\s/g);
	listParts = list.split(",");
	valid = true;
	for (index in listParts) {
		email = listParts[index];
		if (!isValidEmail(email)) {
			valid = false;
		}
	}
	return valid;
}

FSWidgetAdminProjectDetails.prototype.validateSubmit = function() {
	// validate required ACS fields
	if (backClicked) return true;
	var isACSEnabled = $("#ReminderAll").attr("checked") || ($("#ReminderAcceptance").attr("checked") || $("#AutoSMSOnPublish").attr("checked") || $("#Reminder24Hr").attr("checked") || $("#Reminder1Hr").attr("checked") || $("#CheckInCall").attr("checked") || $("#CheckOutCall").attr("checked") || $("#ReminderNotMarkComplete").attr("checked") || $("#ReminderIncomplete").attr("checked") || $("#AutoSMSOnPublish").attr("checked"));
	var isP2TEnabled = $("#isProjectAutoAssign").attr("checked");
	var EmergencyName = $("#EmergencyName");
	var EmergencyEmail = $("#EmergencyEmail");
	var EmergencyPhone = $("#EmergencyPhone");
	var ProjectManager = $("#Project_Manager");
	var ProjectManagerEmail = $("#Project_Manager_Email");
	var ProjectManagerPhone = $("#Project_Manager_Phone");

	var RCPhone = $("#Resource_Coord_Phone");
	var TechnicalSupportPhone = $("#TechnicalSupportPhone");
	var CheckInOutPhone = $("#CheckInOutNumber");

	var P2TNotificationEmail = $("#P2TNotificationEmail");
	var NotifyOnAssign = $("#NotifyOnAssign").attr("checked");
	var NotifyOnAutoAssign = $("#NotifyOnAutoAssign").attr("checked");
	var AssignmentNotificationEmail = $("#AssignmentNotificationEmail");

	if (isACSEnabled) {
		// missing required contact fields
		if (ProjectManager.val() == "") {
			alert("The Project Manager Name is reguired for ACS");
			ProjectManager.focus();
			return false;
		}
		if (ProjectManagerPhone.val() == "") {
			alert("The Project Manager Phone is reguired for ACS");
			ProjectManagerPhone.focus();
			return false;
		}
		if (ProjectManagerEmail.val() == "") {
			alert("The Project Manager Email is reguired for ACS");
			ProjectManagerEmail.focus();
			return false;
		}
		if (EmergencyName.val() == "") {
			alert("The Emergency Name is reguired for ACS");
			EmergencyName.focus();
			return false;
		}
		if (EmergencyPhone.val() == "") {
			alert("The Emergency Number is reguired for ACS");
			EmergencyPhone.focus();
			return false;
		}
		if (EmergencyEmail.val() == "") {
			alert("The Emergency Email is reguired for ACS");
			EmergencyEmail.focus();
			return false;
		}
	}
    
	if (!this.isValidContactPhone(ProjectManagerPhone.val(), true)) {
		alert("The Project Manager Number must be a valid 10 digit number with optional extension similar to ###-###-#### x ####");
		ProjectManagerPhone.focus();
		return false;
	}
	if (!this.isValidContactPhone(RCPhone.val(), true)) {
		alert("The RC Number must be a valid 10 digit number with optional extension similar to ###-###-#### x ####");
		RCPhone.focus();
		return false;
	}
	if (!this.isValidContactPhone(EmergencyPhone.val())) {
		alert("The Emergency Number must be a valid 10 digit number similar to ###-###-####");
		EmergencyPhone.focus();
		return false;
	}
	if (!this.isValidContactPhone(TechnicalSupportPhone.val(), true)) {
		alert("The Technical Support Number must be a valid 10 digit number with optional extension similar to ###-###-#### x ####");
		TechnicalSupportPhone.focus();
		return false;
	}
	if (!this.isValidContactPhone(CheckInOutPhone.val(), true)) {
		alert("The Check in / out Number must be a valid 10 digit number with optional extension similar to ###-###-#### x ####");
		CheckInOutPhone.focus();
		return false;
	}
    /************/
    if (NotifyOnAutoAssign) {
        if (P2TNotificationEmail.val() == "" || !this.validEmailList(P2TNotificationEmail.val())) {
            alert("The P2T Notification Email is not a valid email address");
            P2TNotificationEmail.focus();
            return false;
        }
    }
    if (NotifyOnAssign) {
        if (AssignmentNotificationEmail.val() == "" || !this.validEmailList(AssignmentNotificationEmail.val())) {
            alert("The Assignment Notification Email is not a valid email address");
            AssignmentNotificationEmail.focus();
            return false;
        }
    }
    /**************/
	if (!this.allowSubmit) {
		if (this.didP2TChange()) {
            /**
             * @todo update proect WOs with p2t options
             */
			//this.updateP2T();
			//return false;
		}
	}
	if (!this.allowSubmit) {
		if (this.didACSChange()) {
			// change in ACS options
            /**
             * @todo update proect WOs with acs options
             */
			//this.showACSUpdateOptions();
			//return false;
		}
	}
	$("#PcntDeduct").attr("disabled", false);

	// end validation
	if (!isACSEnabled) {
		$("#ACSAuthBy").attr("value", "");
		$("#ACSAuthDate").attr("value", "");
	}
	$("#ReminderAll").attr("disabled", false);

	$("#MinimumSelfRating").attr("disabled", false);
	$("#MaximumAllowableDistance").attr("disabled", false);
    return (this.formType == "create") ? detailsWidget.createWO() : detailsWidget.updateDetails();
}