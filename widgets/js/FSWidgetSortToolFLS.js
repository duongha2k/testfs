function FSWidgetSortToolFLS( sortSelect, directionSelect, wd, roll, defaultSortTool, useSessionDefault ) {
    FSWidgetSortTool.call(this, sortSelect, directionSelect, wd, roll, defaultSortTool, useSessionDefault );
    this.prepareItems();
    this.initSortTool();
}

FSWidgetSortToolFLS.prototype               = new FSWidgetSortTool;
FSWidgetSortToolFLS.prototype.constructor   = FSWidgetSortToolFLS;

FSWidgetSortToolFLS.NAME          = 'FSWidgetSortToolFLS';
FSWidgetSortToolFLS.VERSION       = '0.1';
FSWidgetSortToolFLS.DESCRIPTION   = 'Class FSWidgetSortToolFLS: Sort Tool for FLS Dashboard only';


FSWidgetSortToolFLS.prototype.prepareItems = function(version) {
	FSWidgetSortTool.prototype.prepareItems.call(this);	
    version = version || 'Lite';
    if(version === undefined) return;
    if(version=='Lite'){
        this._sort.created = {
            'win'           : 'WIN #',
            'clientid'      : 'Client ID',
            'startdate'     : 'Start Date',
            'StandardOffset'      : 'Time Zone',//13892
            'hours'         : 'Total Hours',
            'hl'            : 'Headline',
            'loc'           : 'Location',
            'project'       : 'Project',
            'site'          : 'Site',
            'parts'         : 'Return Parts'
        };
        this._sort.published = {
            'win'           : 'WIN #',
            'clientid'      : 'Client ID',
            'startdate'     : 'Start Date',
            'StandardOffset'      : 'Time Zone',//13892
            'hours'         : 'Total Hours',
            'hl'            : 'Headline',
            'loc'           : 'Location',
            'project'       : 'Project',
            'site'          : 'Site',
            'loc1'          : 'Street',
            'loc2'          : 'City, State',
            'zip'           : 'Zip Code',
            'parts'         : 'Return Parts'
        };
        this._sort.assigned = {
            'win'           : 'WIN #',
            'clientid'      : 'Client ID',
            'startdate'     : 'Start Date',
            'StandardOffset'      : 'Time Zone',//13892
            'enddate'       : 'End Date',
            'hours'         : 'Total Hours',
            'hl'            : 'Headline',
            'loc'           : 'Location',
            'project'       : 'Project',
            'site'          : 'Site',
            'region'        : 'Region',
            'loc1'          : 'Site Street',
            'loc2'          : 'Site City, State',
            'zip'           : 'Site Zip Code',
            'techid'        : 'Tech ID',
            'techname'      : 'Tech Name',
            'parts'         : 'Return Parts'
        };
        this._sort.workdone = {
            'win'           : 'WIN #',
            'clientid'      : 'Client ID',
            'startdate'     : 'Start Date',
            'StandardOffset'      : 'Time Zone',//13892
            'hours'         : 'Total Hours',
            'timesinceworkdone'         : 'Days Old',
            'hl'            : 'Headline',
            'loc'           : 'Location',
            'project'       : 'Project',
            'site'          : 'Site',
            'techid'        : 'Tech ID',
            'techname'      : 'Tech Name',
            'parts'         : 'Return Parts'
        };
        this._sort.incomplete = {
            'win'           : 'WIN #',
            'clientid'      : 'Client ID',
            'startdate'     : 'Start Date',
            'StandardOffset'      : 'Time Zone',//13892
            'hours'         : 'Total Hours',
            'hl'            : 'Headline',
            'loc'           : 'Location',
            'project'       : 'Project',
            'site'          : 'Site',
            'region'        : 'Region',
            'techid'        : 'Tech ID',
            'techname'      : 'Tech Name',
            'parts'         : 'Return Parts'
        }
        this._sort.completed = {
            'approveddate'  : 'Approved Date',
            'paid'          : 'Paid Date',
            'win'           : 'WIN #',
            'clientid'      : 'Client ID',
            'startdate'     : 'Start Date',
            'StandardOffset'      : 'Time Zone',//13892
            'hours'         : 'Total Hours',
            'hl'            : 'Headline',
            'loc'           : 'Location',
            'techid'        : 'Tech ID',
            'techname'      : 'Tech Name',
            'parts'         : 'Return Parts'
        }
        this._sort.all = {
            'stage'         : 'Stage',
            'win'           : 'WIN #',
            'clientid'      : 'Client ID',
            'startdate'     : 'Start Date',
            'StandardOffset'      : 'Time Zone',//13892
            'hours'         : 'Total Hours',
            'hl'            : 'Headline',
            'loc'           : 'Location',
            'project'       : 'Project',
            'site'          : 'Site',
            'region'        : 'Region',
            'techid'        : 'Tech ID',
            'techname'      : 'Tech Name'
        };
        this._sort.quickview = {
            'stage'         : 'Stage',
            'win'           : 'WIN#',
            'clientid'      : 'Client ID',
            'startdate'     : 'Start Date',
            'StandardOffset'      : 'Time Zone',//13892
            'hours'         : 'Hours',
            'hl'            : 'Headline',
            'loc'           : 'Location',
            'project'       : 'Project',
            'site'          : 'Site',
            'region'        : 'Region',
            'route'         : 'Route',
            'techid'        : 'Tech ID',
            'techname'      : 'Tech Name',
            'docs'          : 'Documents',
            'parts'         : 'Return Parts',
            'clientpo'      : 'Client PO#'
        };
        if(!window._isFLSManager ){
            this._sort.created.paymax       = 'Agreed';
            this._sort.published.paymax     = 'Agreed';
            this._sort.assigned.bid         = 'Agreed';
            this._sort.workdone.finalPay    = 'Final';
            this._sort.incomplete.finalPay  = 'Final';
            this._sort.completed.finalPay   = 'Final';
            this._sort.all.paymax           = 'Max';
            this._sort.all.bid              = 'Agreed';
            this._sort.all.finalPay         = 'Final';
        }

    } else if(version=='Full') {
        this._sort.created = {
            'win'           : 'WIN#',
            'clientid'      : 'Client ID',
            'clientpo'      : 'Client PO',
            'startdate'     : 'Start Date',
            'StandardOffset'      : 'Time Zone',//13892
            'hours'         : 'Total Hours',
            'hl'            : 'Headline',
            'loc'           : 'Location',
            'project'       : 'Project',
            'site'          : 'Site',
            'region'        : 'Region',
            'lead'          : 'Lead',
            'assist'        : 'Assist',
            'payMax'        : 'Offer $',
            'basis'         : 'Tech Pay Basis'
        };
        this._sort.published = {
            'bidcount'      : 'Number of bids',
            'win'           : 'WIN#',
            'clientid'      : 'Client ID',
            'clientpo'      : 'Client PO',
            'startdate'     : 'Start Date',
            'StandardOffset'      : 'Time Zone',//13892
            'hours'         : 'Hours',
            'hl'            : 'Headline',
            'loc'           : 'Location',
            'project'       : 'Project',
            'site'          : 'Site',
            'region'        : 'Region',
            'loc1'          : 'Street',
            'loc2'          : 'City State',
            'zip'           : 'Site Zip Code',
            'lead'          : 'Lead',
            'assist'        : 'Assist',
            'payMax'        : 'Offer $',
            'basis'         : 'Tech Pay Basis'
        };
        this._sort.assigned = {
            'win'           : 'WIN#',
            'clientid'      : 'Client ID',
            'clientpo'      : 'Client PO',
            'startdate'     : 'Start Date',
            'StandardOffset'      : 'Time Zone',//13892
            'enddate'       : 'End Date',
            'hours'         : 'Hours',
            'accepted'      : 'Accepted',
            'confirmed'     : 'Confirmed',
            'checkedin'     : 'Checked In',
            'hl'            : 'Headline',
            'loc'           : 'Location',
            'project'       : 'Project',
            'site'          : 'Site',
            'region'        : 'Region',
            'loc1'          : 'Site Street',
            'loc2'          : 'Site City, State',
            'zip'           : 'Site Zip Code',
            'techid'        : 'Tech ID',
            'techname'      : 'Tech Name',
            'lead'          : 'Lead',
            'assist'        : 'Assist',
            'payMax'        : 'Offer $',
            'basis'         : 'Tech Pay Basis'
        };

        this._sort.workdone = {
            'win'           : 'WIN#',
            'clientid'      : 'Client ID',
            'clientpo'      : 'Client PO',
            'startdate'     : 'Start Date',
            'checkin'       : 'Check-in Time',
            'StandardOffset'      : 'Time Zone',//13892
            'hours'         : 'Hours',
            'timesinceworkdone'         : 'Days Old',            
            'hl'            : 'Headline',
            'loc'           : 'Location',
            'project'       : 'Project',
            'site'          : 'Site',
            'region'        : 'Region',
            'techid'        : 'Tech ID',
            'techname'      : 'Tech Name',
            'lead'          : 'Lead',
            'assist'        : 'Assist',
            'bid'           : 'Agreed $',
            'final'         : 'Final $',
            'basis'         : 'Tech Pay Basis',
            'docs'          : 'Documents',
            'parts'         : 'Return Parts',
            'paperwork'     : 'Paperwork'
        };
        this._sort.incomplete = {
            'win'           : 'WIN#',
            'clientid'      : 'Client ID',
            'clientpo'      : 'Client PO',
            'startdate'     : 'Start Date',
            'checkin'       : 'Check-in Time',
            'StandardOffset'      : 'Time Zone',//13892
            'hours'         : 'Hours',
            'hl'            : 'Headline',
            'loc'           : 'Location',
            'project'       : 'Project',
            'site'          : 'Site',
            'region'        : 'Region',
            'techid'        : 'Tech ID',
            'techname'      : 'Tech Name',
            'lead'          : 'Lead',
            'assist'        : 'Assist',
            'bid'           : 'Bid $',
            'final'         : 'Final $',
            'docs'          : 'Documents',
            'parts'         : 'Return Parts',
            'paperwork'     : 'Paperwork'
        };
        this._sort.completed = {
            'win'           : 'WIN#',
            'clientid'      : 'Client ID',
            'clientpo'      : 'Client PO',
            'approveddate'  : 'Approved Date',
            'paid'          : 'Paid Date',
            'invoice'       : 'Invoice Date',
            'startdate'     : 'Start Date',
            'StandardOffset'      : 'Time Zone',//13892
            'hours'         : 'Hours',
            'hl'            : 'Headline',
            'loc'           : 'Location',
            'project'       : 'Project',
            'site'          : 'Site',
	    'zip'           : 'Site Zip Code',//13962
	    'st'            : 'Site State',//13962   
            'region'        : 'Region',
            'techid'        : 'Tech ID',
            'techname'      : 'Tech Name',
            'lead'          : 'Lead',
            'assist'        : 'Assist',
            'final'         : 'Final $',
            'docs'          : 'Documents',
            'parts'         : 'Return Parts'
        };
        this._sort.all = {
            'stage'         : 'Stage',
            'win'           : 'WIN#',
            'clientid'      : 'Client ID',
            'clientpo'      : 'Client PO',
            'startdate'     : 'Start Date',
            'StandardOffset'      : 'Time Zone',//13892
            'hours'         : 'Hours',
            'hl'            : 'Headline',
            'loc'           : 'Location',
            'project'       : 'Project',
            'site'          : 'Site',
            'region'        : 'Region',
            'techid'        : 'Tech ID',
            'techname'      : 'Tech Name',
            'lead'          : 'Lead',
            'assist'        : 'Assist',
            'paymax'        : 'Max',
            'bid'           : 'Agreed',
            'final'         : 'Final $',
            'docs'          : 'Documents',
            'parts'         : 'Return Parts'
        };
        this._sort.deactivated = {
            'win'           : 'WIN#',
            'clientid'      : 'Client ID',
            'clientpo'      : 'Client PO',
            'startdate'     : 'Start Date',
            'checkin'       : 'Check-in Time',
            'StandardOffset'      : 'Time Zone',//13892
            'hours'         : 'Hours',
            'hl'            : 'Headline',
            'loc'           : 'Location',
            'project'       : 'Project',
            'site'          : 'Site',
            'region'        : 'Region',
            'techid'        : 'Tech ID',
            'techname'      : 'Tech Name',
            'lead'          : 'Lead',
            'assist'        : 'Assist',
            'bid'           : 'Agreed $',
            'final'         : 'Final $',
            'basis'         : 'Tech Pay Basis',
            'docs'          : 'Documents',
            'parts'         : 'Return Parts'
        };
        this._sort.quickview = {
            'stage'         : 'Stage',
            'win'           : 'WIN#',
            'clientid'      : 'Client ID',
            'startdate'     : 'Start Date',
            'StandardOffset'      : 'Time Zone',//13892
            'hours'         : 'Hours',
            'hl'            : 'Headline',
            'loc'           : 'Location',
            'project'       : 'Project',
            'site'          : 'Site',
            'region'        : 'Region',
            'route'         : 'Route',
            'techid'        : 'Tech ID',
            'techname'      : 'Tech Name',
            'docs'          : 'Documents',
            'parts'         : 'Return Parts',
            'clientpo'      : 'Client PO#'
        };
        if(!window._isFLSManager ){
            this._sort.created.paymax       = 'Agreed';
            this._sort.published.paymax     = 'Agreed';
            this._sort.assigned.bid         = 'Agreed';
            this._sort.workdone.bid         = 'Agreed';
            this._sort.workdone.finalPay    = 'Final';
            this._sort.incomplete.bid       = 'Bid';
            this._sort.incomplete.finalPay  = 'Final';
            this._sort.completed.finalPay   = 'Final';
            this._sort.all.paymax           = 'Agreed';
            this._sort.all.bid              = 'Agreed';
            this._sort.all.finalPay         = 'Final';
            this._sort.deactivated.bid      = 'Agreed';
            this._sort.deactivated.finalPay = 'Final';
        }
    }
/*    this._sort_defaults = {
        created          : {sort: 'startdate', dir: 'asc'},
        published        : {sort: 'startdate', dir: 'asc'},
        assigned         : {sort: 'startdate', dir: 'asc'},
        workdone         : {sort: 'startdate', dir: 'asc'},
        incomplete       : {sort: 'startdate', dir: 'desc'},
        all              : {sort: 'startdate', dir: 'desc'},
        deactivated      : {sort: 'startdate', dir: 'desc'},
        completed        : {sort: 'startdate', dir: 'asc'},
        techavailable    : {sort: 'start', dir: 'asc'},
        techassigned     : {sort: 'start', dir: 'asc'},
        techworkdone     : {sort: 'start', dir: 'asc'},
        techapproved     : {sort: 'start', dir: 'desc'},
        techpaid         : {sort: 'start', dir: 'desc'},
        techincomplete   : {sort: 'start', dir: 'asc'},
        techall          : {sort: 'start', dir: 'desc'},
        techschedule     : {sort: 'start', dir: 'desc'}
    };*/
}