/**
 * FSWidgetDashboard
 *
 * @param Object|nul
 * @access public
 * @return void
 */
function FSWidgetDashboard( params ) {
    FSWidget.call(this, params);

    this.params.WIDGET.avaliableTabs = {
        created         : {
            module:'dashboard',
            controller:'index',
            action:'created'
        },
        published       : {
            module:'dashboard',
            controller:'index',
            action:'published'
        },
        assigned        : {
            module:'dashboard',
            controller:'index',
            action:'assigned'
        },
        workdone        : {
            module:'dashboard',
            controller:'index',
            action:'workdone'
        },
        approved        : {
            module:'dashboard',
            controller:'index',
            action:'approved'
        },
        inaccounting    : {
            module:'dashboard',
            controller:'index',
            action:'inaccounting'
        },
        incomplete      : {
            module:'dashboard',
            controller:'index',
            action:'incomplete'
        },
        completed       : {
            module:'dashboard',
            controller:'index',
            action:'completed'
        },
        all             : {
            module:'dashboard',
            controller:'index',
            action:'all'
        },
        deactivated     : {
            module:'dashboard',
            controller:'index',
            action:'deactivated'
        }, 
        rightside 		: {
            module:'dashboard', 
            controller:'sections', 
            action:'dash-content-right'
        }
        , 
        todayswork 		: {
            module:'dashboard', 
            controller:'index', 
            action:'todayswork'
        }
    };

    this.params.POPUP.avaliablePopups = {
        filters         : {
            module:'dashboard',
            controller:'popup',
            action:'filters'
        },
        details         : {
            module:'dashboard',
            controller:'sections',
            action:'section7'
        },
        woinformation   : {
            module:'dashboard',
            controller:'sections',
            action:'section1'
        },
        woinstructions  : {
            module:'dashboard',
            controller:'sections',
            action:'section2'
        },
        techjobsite     : {
            module:'dashboard',
            controller:'sections',
            action:'section6'
        },
        activitylog     : {
            module:'dashboard',
            controller:'popup',
            action:'activity-log'
        },
        techinfo        : {
            module:'dashboard',
            controller:'popup',
            action:'tech-info'
        },
        'final'         : {
            module:'dashboard',
            controller:'sections',
            action:'final'
        },
        'finalread'     : {
            module:'dashboard',
            controller:'sections',
            action:'final-read'
        },
        starttime       : {
            module:'dashboard',
            controller:'sections',
            action:'start-time'
        },
        documents       : {
            module:'dashboard',
            controller:'sections',
            action:'tech-documents'
        },
        filedownload    : {
            module:'dashboard',
            controller:'do',
            action:'file-wos-documents-download'
        },
        incompletereason: {
            module:'dashboard',
            controller:'sections',
            action:'incomplete-reason'
        },
        quickassigntool : {
            module:'dashboard',
            controller:'sections',
            action:'quickassigntool'
        },
        findworkorder   : {
            module:'dashboard',
            controller:'popup',
            action:'findworkorder'
        },
        mytechs   		: {
            module:'dashboard',
            controller:'popup',
            action:'mytechs'
        },
        quickapprove	: {
            module:'dashboard',
            controller:'popup',
            action:'approve'
        },
        techcomments       : {
            module:'dashboard',
            controller:'sections',
            action:'tech-comments'
        },
        bcatdocuments       : {
            module:'dashboard',
            controller:'sections',
            action:'bcat-documents'
		},
		quicktools : {
            module:'dashboard',
            controller:'customer',
            action:'quick-tools-popup'
		}
    }
	
	try {
		this._saveStateCallback = params.saveStateCallback ? params.saveStateCallback : function() {};
	} catch (e) {}
	
	this._project_filter_default = [];
}

FSWidgetDashboard.prototype = new FSWidget();
FSWidgetDashboard.prototype.constructor = FSWidgetDashboard;

FSWidgetDashboard.NAME          = 'FSWidgetDashboard';
FSWidgetDashboard.VERSION       = '0.1';
FSWidgetDashboard.DESCRIPTION   = 'Class FSWidgetDashboard';

FSWidgetDashboard.prototype.resetFormIE = function (roll)
{
    roll.body($(this.filters().container()).html());
    calendarInit();
}
/**
 *  htmlFilters
 *
 *  Function return html for filters popup
 *
 *  return string
 */


FSWidgetDashboard.prototype.htmlFilters = function () {
    if ( !this.filters().isInitialized() ) {
        var _url;
        try {
            _url = this._makeUrl('filters', '/tab/'+this.currentTab+'/company/'+this.filters().company());
        } catch ( e ) {
            return false;
        }
        if(typeof(this.params.ajaxSuccess) == 'function')
        {
            this.params.ajaxSuccess();
        }
        // this.params.ajaxSuccess = null;
        jQuery.ajax({
            url : _url,
            cache : false,
            type : 'POST',
            dataType : 'html',
            context : this,
            success : function(data, status, xhr) {
                if(typeof(this.params.ajaxSuccess) == 'function')
                {
                    var _el = this;
                    var fun = function(_el,data)
                    {
                        if ( _el.filters().container() === null )
                            _el.filters()._makeContainer();
                        $(_el.filters().container()).html(data);
                        _el.filters().setInitialized();
                    };
                    _el.params.ajaxSuccess(fun,_el,data);
                }
                else
                {
                if ( this.filters().container() === null )
                    this.filters()._makeContainer();
                $(this.filters().container()).html(data);
                this.filters().setInitialized();
                }
                if(typeof($(".hoverText").bt) != 'undefined'){
                $(".hoverText").bt({
                    titleSelector: "attr('bt-xtitle')",
                    fill: 'white',
                    cssStyles: {color: 'black', fontWeight: 'bold', width: '300px'},
                    animate:true
                });   
                }
                //====
                
            },
            error : function (xhr, status, error) {
                if ( xhr.responseText === "authentication required" ) {
                    document.location = "/clients/logIn.php";
                }
            }
        });
    }
}
FSWidgetDashboard.prototype.AuditNeededOnlick = function()
{
    var _event = this;
     var winnum = jQuery(this).attr("winnum");
     var AuditNeeded = jQuery(this).attr("auditneeded");
    $.ajax({
                url:'/widgets/dashboard/index/auditcomplete/',
                cache:false,
                type:'POST',
                data: {'WIN_NUM':winnum},
                success: function(data) {
                    if(data.success)
                    {
                        jQuery(_event).unbind("click");
                        jQuery(_event).children("img").attr("src","/widgets/images/Green_Flag.png");

//                        AuditNeeded = dateFormat(Date(AuditNeeded), "mm/dd/yyyy");
//                        var AuditCompleteDatetime = data.AuditCompleteDate.split(' ');
//                        var AuditCompletedate = AuditCompleteDatetime[0].split('-');
//                        var AuditCompletetime = AuditCompleteDatetime[1].split(':');
//                        var _time = '';
//                        var _ampm = '';
//                        if(AuditCompletetime[0]>12)
//                        {
//                            _time = AuditCompletetime[0] - 12;
//                            _ampm = 'PM';
//                        }
//                        else if(AuditCompletetime[0]==0)
//                        {
//                            _time = 12;
//                            _ampm = 'AM';
//                        }
//                            
//                        var AuditComplete = AuditCompletedate[1] + '-' + AuditCompletedate [2] + '-' + AuditCompletedate[0] + ' ' + _time + ':' + AuditCompletetime[1] + ' ' + _ampm + ' CST';
                        var AuditComplete = data.AuditCompleteDate + ' CST';
                        //var AuditComplete = dateFormat(Date(AuditCompleteDate), "mm/dd/yyyy");

                        jQuery(_event).children("img").attr("bt-xtitle","Audit Needed: "+ AuditNeeded +" <br>Audit Complete: " + AuditComplete);
                    }    
                }
            });
}
//FSWidgetDashboard.prototype.ajaxSuccess = function()
//{
//    
//}
/**
 *  resetFilters
 *
 *  @param FSPopupAbstract roll
 *  @return void
 */
FSWidgetDashboard.prototype.resetFilters = function (roll)
{
    this.filters().techId(null);
    this.filters().dateStart('');
    this.filters().dateEnd('');
    this.filters().zip('');
    this.filters().state('');
    this.filters().country('');

    this.filters().username('');
    this.filters().headline('');
    this.filters().techEmail('');
    this.filters().woCategory('');
    this.filters().dateEnteredFrom('');
    this.filters().dateEnteredTo('');
    this.filters().dateApprovedFrom('');
    this.filters().dateApprovedTo('');
    this.filters().dateInvoicedFrom('');
    this.filters().dateInvoicedTo('');
    this.filters().datePaidFrom('');
    this.filters().datePaidTo('');
    this.filters().payMaxFrom(null);
    this.filters().payMaxTo(null);
    this.filters().netPayAmountFrom(null);
    this.filters().netPayAmountTo(null);
    this.filters().tbPay(null);

    this.filters().setProject(this._project_filter_default);
    this.filters().tech('');
    this.filters().region('');
    //13892
    this.filters().timeZone('');
    this.filters().fullSearch(false);
    this.filters().win(null);
    this.filters().clientWO(null);       
    this.filters().WO_ID_Mode(null);       
    this.filters().po('');
    this.filters().showToTech(null);
    this.filters().siteName(null);
    this.filters().siteNumber(null);
    this.filters().city(null);
    this.filters().updateRequested(null);
    this.filters().storeNotified(null);
    this.filters().checkedIn(null);
    this.filters().reviewedWO(null);
    this.filters().techConfirmed(null);
    this.filters().techComplete(null);
    this.filters().techCalled(null);
    this.filters().siteComplete(null);
    this.filters().approved(null);
    this.filters().extraPay(null);
    this.filters().outOfScope(null);
    this.filters().techPaid(null);
    this.filters().techFName(null);
    this.filters().techLName(null);
    this.filters().showSourced(null);
    this.filters().showNotSourced(null);
    this.filters().paperReceived(null);
    this.filters().lead(null);
    this.filters().assist(null);
    this.filters().deactivated(null);
    this.filters().shortNotice(null);
    this.filters().flsId(null);
    this.filters().distance(null);
    this.filters().pricingRan(null);
    this.filters().invoiced(null);
    this.filters().pcntDeduct(null);
    this.filters().deactivatedReason(null);
    this.filters().satrecommendedFrom(null);
    this.filters().satperformanceFrom(null);
    this.filters().showToTech(null);
    
    this.filters().deactivatedReason(null);
    this.filters().deactivationCode(null);
    
    this.filters().Route("");
    
    this.filters().AbortFee("");
    this.filters().techPaperwork("");
    this.filters().siteStatus("");
    //732
    this.filters().SiteContactBackedOutChecked("");
    //end732
}

FSWidgetDashboard.prototype.setProjectDefault = function(projects) {
	// default for when resetFilters or restoreProjectDefault called
	this._project_filter_default = projects;
}

FSWidgetDashboard.prototype.restoreProjectDefault = function() {
	// restore default for project on filter form
	$.each(this._project_filter_default, function (index, value) {
		$('#filterForm' + ' input:checkbox[value=' + value + ']').attr('checked', true);
	});
}

FSWidgetDashboard.prototype.resetFilterForm = function(roll) {
	// resets filter form and restores default project filter
	$('#' + roll.container().id + ' #filterForm')[0].reset();
	if ( $.browser.msie ) { this.resetFormIE(roll); }
	this.restoreProjectDefault();
}

/**
 *  applyFilters
 *
 *  @return void
 */
FSWidgetDashboard.prototype.applyFilters = function (roll) {
    this.params.isBid = 0;
    this.resetFilters();//14151
    FSWidget.filterActivate(false);

    var i, stateSelect, callSelect;

    var projects = $('#' + roll.container().id + ' input[name="filterProjects[]"]').serializeArray();
    var _p = [];
    for ( i=0; i < projects.length; ++i ) {
        _p.push( projects[i].value );
    }
    this.filters().setProject(_p);

    this.filters().dateStart($($('#' + roll.container().id + ' #filterStartDate')[0]).val());
	this.filters().dateEnd($($('#' + roll.container().id + ' #filterEndDate')[0]).val());
    this.filters().zip($($('#' + roll.container().id + ' #filterZip')[0]).val());
    this.filters().po($($('#' + roll.container().id + ' #filterPO')[0]).val());

    stateSelect = $('#'+roll.container().id+' #filterState')[0];
     
	if (typeof stateSelect != "undefined") {
		for ( i=0; i< stateSelect.options.length; ++i ) {
			if ( stateSelect.options[i].selected ) {
				this.filters().state($(stateSelect.options[i]).val());
               
				break;
			}
		}
	}

    countrySelect = $('#'+roll.container().id+' #filterCountry')[0];
	if (typeof countrySelect != "undefined") {
		for ( i=0; i< countrySelect.options.length; ++i ) {
			if ( countrySelect.options[i].selected ) {
				this.filters().country($(countrySelect.options[i]).val());
				break;
			}
		}
    }

    callSelect = $('#'+roll.container().id+' #filterCallType')[0];
	if (typeof callSelect != "undefined") {
		for ( i=0; i< callSelect.options.length; ++i ) {
			if ( callSelect.options[i].selected ) {
				this.filters().callType(callSelect.options[i].value);
				break;
			}
		}
    }
    //13892
    callSelect = $('#' + roll.container().id + ' #StandardOffset')[0];    
    if (typeof callSelect != "undefined") {
		for ( i=0; i< callSelect.options.length; ++i ) {
			if ( callSelect.options[i].selected ) {
				this.filters().timeZone(callSelect.options[i].value);
				break;
			}
		}
    }            
    // End 13892

    this.filters().tech($($('#' + roll.container().id + ' #filterAssignedTech')[0]).val());
    this.filters().region($($('#' + roll.container().id + ' #filterRegion')[0]).val());

    this.filters().Route($('#' + roll.container().id + ' #filterRoute').val());
   
    this.filters().Audit($('#' + roll.container().id + ' #Audit').val());
    this.filters().Owner($('#' + roll.container().id + ' #Owner').val());
    this.filters().dateRange($('#' + roll.container().id + ' #dateRange').val());

    this.filters().siteStatus($('#' + roll.container().id + ' #filterSiteStatus').val());
    
    this.filters().techPaperwork($('#' + roll.container().id + ' #techPaperwork').val());
    //017
    this.filters().shortNotice($('#' + roll.container().id + ' #shortnotice option:selected').val());
    //end 017
    roll.hide();

    var p = this.getParams();
    this.show({
        tab:this.currentTab,
        params:p
    });
}
/**
 *  prepareFilters
 */
FSWidgetDashboard.prototype.prepareFilters = function(roll, params) {

    FSWidget.filterActivate(true);

    var i, stateSelect, callSelect, progectsCheckboxes;

    $('#' + roll.container().id + ' #moreFilters').remove();
    var clone = $('#' + roll.container().id + ' #moreFiltersOrig').clone();
    clone.attr('id', 'moreFilters').show();
    $('#' + roll.container().id + ' #moreFiltersOrig').parent().append(clone);

    progectsCheckboxes = $('#' + roll.container().id + ' input[name="filterProjects[]"]');
	if (typeof progectsCheckboxes != "undefined") {
    for ( i=0; i < progectsCheckboxes.length; ++i ) {
        if ( $.inArray(progectsCheckboxes[i].value, this.filters().getProjects()) > -1 ) {
            progectsCheckboxes[i].checked = true;
        } else {
            progectsCheckboxes[i].checked = false;
        }
    }
	}

    stateSelect = $('#' + roll.container().id + ' #filterState')[0];
	if (typeof stateSelect != "undefined") {
    for ( i=0; i < stateSelect.options.length; ++i ) {
        if ( stateSelect.options[i].value == this.filters().state() ) {
            stateSelect.options[i].selected = true;
//	    if (this.filterValueIsSet(this.filters().state()))
//	            this.showFilterByName('sectionState');
            break;
        }
    }
	}
    //13892    
    stateSelect = $('#' + roll.container().id + ' #StandardOffset')[0];
	if (typeof stateSelect != "undefined") {
    for ( i=0; i < stateSelect.options.length; ++i ) {
        if ( stateSelect.options[i].value == this.filters().timeZone() ) {
            stateSelect.options[i].selected = true;	  
            break;
        }
    }
    }

    countrySelect = $('#' + roll.container().id + ' #filterCountry')[0];
	if (typeof countrySelect != "undefined") {
    for ( i=0; i < countrySelect.options.length; ++i ) {
        if ( countrySelect.options[i].value == this.filters().country() ) {
            countrySelect.options[i].selected = true;
	    if (this.filterValueIsSet(this.filters().country()))
	            this.showFilterByName('sectionCountry');
            break;
        }
    }
	}
	
	$('#' + roll.container().id + ' #filterCountry').change();

    $($('#' + roll.container().id + ' #filterZip')[0]).val (this.filters().zip());
    $($('#' + roll.container().id + ' #filterStartDate')[0]).val (this.filters().dateStart());
    $($('#' + roll.container().id + ' #filterEndDate')[0]).val (this.filters().dateEnd());
    $($('#' + roll.container().id + ' #filterAssignedTech')[0]).val (this.filters().tech());
    $($('#' + roll.container().id + ' #filterRegion')[0]).val (this.filters().region());
    $($('#' + roll.container().id + ' #filterRoute')[0]).val (this.filters().Route());
    $($('#' + roll.container().id + ' #filterPO')[0]).val (this.filters().po());

	if (this.filterValueIsSet(this.filters().zip()))
            this.showFilterByName('sectionZip');
//	if (this.filterValueIsSet(this.filters().tech()))
//            this.showFilterByName('sectionAssignedTech');
	if (this.filterValueIsSet(this.filters().region()))
            this.showFilterByName('sectionRegion');
	if (this.filterValueIsSet(this.filters().Route()))
            this.showFilterByName('sectionRoute');
	if (this.filterValueIsSet(this.filters().po()))
            this.showFilterByName('sectionClientpo');

    callSelect = $('#' + roll.container().id + ' #filterCallType')[0];
	if (typeof callSelect != "undefined") {
		for ( i=0; i < callSelect.options.length; ++i ) {
			if ( callSelect.options[i].value == this.filters().callType() ) {
				callSelect.options[i].selected = true;
			if (this.filters().callType() != 'none')
					this.showFilterByName('sectionCalltype');
				break;
			}
		}
	}
   
    callSelect = $('#' + roll.container().id + ' #Audit')[0];
	if (typeof callSelect != "undefined") {
		for ( i=0; i < callSelect.options.length; ++i ) {
			if ( callSelect.options[i].value == this.filters().Audit() ) {
				callSelect.options[i].selected = true;
			if (this.filterValueIsSet(this.filters().Audit()))
					this.showFilterByName('sectionAudit');
				break;
			}
		}
	}

    callSelect = $('#' + roll.container().id + ' #Owner')[0];
	if (typeof callSelect != "undefined") {
		for ( i=0; i < callSelect.options.length; ++i ) {
			if ( callSelect.options[i].value == this.filters().Owner() ) {
				callSelect.options[i].selected = true;
			if (this.filterValueIsSet(this.filters().Owner()))
					this.showFilterByName('sectionOwner');
				break;
			}
		}
	}

    callSelect = $('#' + roll.container().id + ' #dateRange')[0];
	if (typeof callSelect != "undefined") {
		for ( i=0; i < callSelect.options.length; ++i ) {
			if ( callSelect.options[i].value == this.filters().dateRange() ) {
				callSelect.options[i].selected = true;
				break;
			}
		}
	}

    callSelect = $('#' + roll.container().id + ' #techPaperwork')[0];
    for ( i=0; i < callSelect.options.length; ++i ) {
        if ( callSelect.options[i].value == this.filters().techPaperwork() ) {
            callSelect.options[i].selected = true;
            if (this.filterValueIsSet(this.filters().techPaperwork()))
	            this.showFilterByName('sectiontechPaperwork');
            break;
        }
    }
    
    callSelect = $('#' + roll.container().id + ' #filterSiteStatus')[0];
    for ( i=0; i < callSelect.options.length; ++i ) {
        if ( callSelect.options[i].value == this.filters().siteStatus() ) {
            callSelect.options[i].selected = true;
            if (this.filterValueIsSet(this.filters().siteStatus()))
	            this.showFilterByName('sectionSiteStatus');
            break;
        }
    }
    //13892
     callSelect = $('#' + roll.container().id + ' #StandardOffset')[0];
    for ( i=0; i < callSelect.options.length; ++i ) {
        if ( callSelect.options[i].value == this.filters().timeZone() ) {
            callSelect.options[i].selected = true;
            if (this.filterValueIsSet(this.filters().timeZone()))
	            this.showFilterByName('sectionTimeZone');
            break;
        }
    }
    // End 13892
    //VALIDATORS
	if (typeof LVDefaults != "undefined") {
		var vZIP = new LiveValidation('filterZip',LVDefaults);
		vZIP.add( Validate.Format, {
			pattern: /^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$/
		} );
		var vTID = new LiveValidation('filterAssignedTech',LVDefaults);
		vTID.add( Validate.Numericality, {
			onlyInteger: true
		} );
	}
        
        //017
        callSelect = $('#' + roll.container().id + ' #shortnotice')[0];
        if (typeof callSelect != "undefined")
        {
            for ( i=0; i < callSelect.options.length; ++i )
            {
                if ( callSelect.options[i].value == this.filters().shortNotice() )
                {
                    callSelect.options[i].selected = true;
                    break;
}
            }
        } 
        //end 017
}
/**
 *  showPreloader
 *
 *  Function displays widget preloader
 *
 *  @param Object|null options
 */
FSWidgetDashboard.prototype.showPreloader = function() {
    $('body').css('cursor', 'wait');
    this.waitImage = new Image();
    this.waitImage.src = "/widgets/images/wait.gif";
    var img  = '<table width="100%" class="gradBox_table_inner">';
        img += '<tr class="table_head"><th>&#160;<br />&#160;</th></tr>';
        img += '<tr><td>&#160<br />&#160;</td></tr>';
        img += '<tr>';
        img += "<td><img style='margin:0 40%;' alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></td>";
        img += '</tr>';
        img += '<tr"><td>&#160<br />&#160;</td></tr>';
        img += '<tr class="paginator"><td class="tCenter">&#160;</td></tr>';
        img += '</table>';
    FSWidget.fillContainer(img, this);
}
/**
 *  show
 *
 *  Function displays widget
 *
 *  @param Object|null options
 */
FSWidgetDashboard.prototype.show = function(options) {
    if ( FSWidget.isFilterActive() ) return;

    var p = options || {
        tab:this.currentTab,
        params:this.getParams()
    };

	// ignore date range depending on tab

	if (p.params.dateRange != 'start') {
		switch (this.currentTab) {
			case 'published':
				if (p.params.dateRange == 'assigned' || p.params.dateRange == 'workdone' || p.params.dateRange == 'approved' || p.params.dateRange == 'inoviced') {
					p.params.dateRange = 'start';
					p.params.start_date = '';
					p.params.end_date = '';
				}
				break;
			case 'assigned':
				if (p.params.dateRange == 'workdone' || p.params.dateRange == 'approved' || p.params.dateRange == 'inoviced') {
					p.params.dateRange = 'start';
					p.params.start_date = '';
					p.params.end_date = '';
				}
				break;
			case 'workdone':
				if (p.params.dateRange == 'approved' || p.params.dateRange == 'inoviced') {
					p.params.dateRange = 'start';
					p.params.start_date = '';
					p.params.end_date = '';
				}
				break;
			case 'approved':
				if (p.params.dateRange == 'inoviced') {
					p.params.dateRange = 'start';
					p.params.start_date = '';
					p.params.end_date = '';
				}
				break;
		}		
	}


	
    var queryString = '/';
    var _url;

    // TODO params needs be reconfigured
    if ( p.params !== undefined && p.params !== null && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params ) {

            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] === true || p.params[key] === false ) {
                if ( p.params[key] === false ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] !== undefined && p.params[key] !== null && p.params[key] !== "" && ( (typeof(p.params[key]) == 'string' && p.params[key].toLowerCase() !== 'none') || typeof(p.params[key]) == 'number') ) {
                //queryString += key + '/' + p.params[key] + '/';
            } else {
                delete p.params[key];
            }
        }
    } else {
        p.params = {};
    }

    try {
        if ( p.tab !== undefined && p.tab !== null ) {
            this.currentTab = p.tab;
            _url = this._makeUrl(p.tab, queryString);
            var idx;
            for ( idx in this.params.WIDGET.avaliableTabs ) {
                $('#'+idx).toggleClass('current', idx === this.currentTab);
            }
        } else if ( p.popup !== undefined && p.popup !== null ) {
            _url = this._makeUrl(p.popup, queryString);
        }
    } catch ( e ) {
        return false;
    }
    
    if (this.sortTool() !== null ) {
    	if (typeof(p.params.page) == undefined ) {
    		this.sortTool().setPage(1);
    	} else {
    		this.sortTool().setPage(p.params.page);
    	}
    }

    this.showPreloader();

    if ( this.tabOpenXhrRequest ) {
        this.tabOpenXhrRequest.abort();
        this.tabOpenXhrRequest = null;
    }

    version = $('#dashboardVersion').val();
    if(version != '' && version == 'Lite'){
	version = "Lite";
        p.params.version = version;
        $('#deactivated').addClass("displayNone");
    } else {
        version = "Full";
//        var defVer = FSWidget.getCookie("dashboardVersionDefault");
        p.params.version = version;
        $('#deactivated').removeClass("displayNone");
    }

    $('#dashboardVersion').val(version);
    FSWidget.saveCookie("dashboardVersionDefault", version);
    
    // get page size
    if ($('#pageSizeSelector') ) {
    	p.params.size = $('#pageSizeSelector').val();
    }
    
    var TechBid = 0;
    if(typeof(this.params.isBid) != 'undefined')
    {
       TechBid = this.params.isBid;   
    }
    //13369
    var IsSortTechIDFromLink = "";
    if(typeof(this.params.IsSortTechIDFromLink) != 'undefined')
    {
       IsSortTechIDFromLink = this.params.IsSortTechIDFromLink;   
    }
    // end 13369
    p.params.TechBid = TechBid;
    //13369
    p.params.IsSortTechIDFromLink = IsSortTechIDFromLink;
    // end 13369
    //this.sortTool().prepareItems(version);
    //this.sortTool().initSortTool();
    //alert(this.params.ajaxSuccess);
    var _event = this;
    this.tabOpenXhrRequest = jQuery.ajax({
        url : _url,
        data : p.params,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/clients/logIn.php";
            } else {
                FSWidget.fillContainer("Sorry, but an error occurred. No results found.", this);
            }
            this.tabOpenXhrRequest = null;
        },
        success : function(data, status, xhr) {
            //036
            if (this.sortTool() !== null )
                this.sortTool().saveSortSession();
            //end 036
            if(typeof(this.params.ajaxSuccess) == 'function')
            {
                var _el = this;
                var fun = function(_el,data)
                {
            $('body').css('cursor', '');
                    FSWidget.fillContainer(data, _el);
                    this.tabOpenXhrRequest = null;
                    if (this.currentTab == 'assigned') {
                        $(".rePublishCheckbox").click(function() {
                            var id = $(_el).attr('id');
                            var matches = /^rePublishCheckbox_(\d+)$/.exec(id);
                            var win = matches[1];

                            if ($(_el).attr('checked')) {
                                $("#rePublishOptions_" + win).removeClass('displayNone');
                            } else {
                                $("#rePublishOptions_" + win).addClass('displayNone');
                            }
                        });
                    }
                    _el.resizeFrame();
                    _el._saveStateCallback();
             
                    $(".hoverText").bt({
                        titleSelector: "attr('bt-xtitle')",
                        fill: 'white',
                        cssStyles: {color: 'black', fontWeight: 'bold', width: '300px'},
                        animate:true
                    });   
                    $(".AuditNeeded").unbind("click",_event.AuditNeededOnlick).bind("click",_event.AuditNeededOnlick);
                    _el.bindEvent();
                    //====
                };
                this.params.ajaxSuccess(fun,_el,data);
            }
            else
            {
                $('body').css('cursor', '');
            FSWidget.fillContainer(data, this);
            this.tabOpenXhrRequest = null;
            if (this.currentTab == 'assigned') {
                $(".rePublishCheckbox").click(function() {
                    var id = $(this).attr('id');
                    var matches = /^rePublishCheckbox_(\d+)$/.exec(id);
                    var win = matches[1];

                    if ($(this).attr('checked')) {
                        $("#rePublishOptions_" + win).removeClass('displayNone');
                    } else {
                        $("#rePublishOptions_" + win).addClass('displayNone');
                    }
                });
            }
			this.resizeFrame();
			this._saveStateCallback();
            this.bindEvent();            
        }
   
        $(".hoverText").bt({
            titleSelector: "attr('bt-xtitle')",
            fill: 'white',
            cssStyles: {color: 'black', fontWeight: 'bold', width: '300px'},
            animate:true
        });   
        $(".AuditNeeded").unbind("click",_event.AuditNeededOnlick).bind("click",_event.AuditNeededOnlick);
        
        //====
        }
    });
}
/**
 *  open
 */
FSWidgetDashboard.prototype.open = function(options) {
    var p = options || {};
    var queryString = '/';
    var _url;
    var noSSL = false;

    if ( p.params !== undefined && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params )
            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                } else if ( key === 'projects' ) {
                    var ids = '';
                    for ( var pr=0; pr < p.params[key].length; ++pr ) {
                        ids += p.params[key][pr];
                        if ( pr + 1 < p.params[key].length ) {
                            ids += '|';
                        }
                    }
                    queryString += 'projects/' + ids + '/';
                }
            } else if (p.params[key] === false || p.params[key] === true) {
                /* ok */
            } else if ( p.params[key] !== undefined && p.params[key] !== null && p.params[key] !== "" && p.params[key].toLowerCase() !== 'none' ) {
                if (key === "download") {
                    noSSL = true;
                } else if ( key === 'start_date' || key === 'end_date' || key === 'date_start' || key === 'date_end') {
                    p.params[key] = p.params[key].replace(/\//g, '_');
                }
                queryString += key + '/' + p.params[key] + '/';
            }
    }
    try {
        _url = this._makeUrl(p.tab, queryString, noSSL);
    } catch ( e ) {
        return false;
    }

    return (p.params['download'] == 'json') ? _url : location = _url;
    	
    //return location = _url;
}
/**
 *  getReloadUrl
 *
 *  Return url string for safety reload current page
 *  and save all active options
 *
 *  @return string
 */
FSWidgetDashboard.prototype.getReloadUrl = function ( forUrl )
{
    var idx;
    var params    = this.getParams();
    var queryPart = '';
    var url;

    for ( idx in params ) {
        if ( params[idx] === "" || params[idx] === null || params[idx] === undefined || params[idx] === 'none' ) {
            delete params[idx];
            continue;
        }
        if ( params[idx] instanceof Array && !params[idx].length ) {
            delete params[idx];
            continue;
        }

        if ( idx === 'start_date' || idx === 'end_date' ) {
            queryPart += '&' + idx + '=' + encodeURIComponent(params[idx].replace(/\//g, '_')).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
        } else if ( idx === 'projects' ) {
            for ( var i = 0; i < params[idx].length; ++i ) {
                queryPart += '&projects[]=' + encodeURIComponent(params[idx][i]).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
            }
        } else if ( idx === 'company' ) {
            queryPart += '&v=' + encodeURIComponent(params[idx]).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');

        } else {
            queryPart += '&' + idx + '=' + encodeURIComponent(params[idx]).replace(/!/g, '%21').replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29').replace(/\*/g, '%2A').replace(/%20/g, '+');
        }
    }

    url = location.toString();
    if ( queryPart.length > 0 ) {
        queryPart = queryPart.replace('&', '?');
        url = url.replace(/\?.*$/, queryPart);
        if ( url.indexOf('?') === -1 ) {
            url += queryPart;
        }
    }

    return (( !forUrl ) ?  url : encodeURIComponent(url).replace(/\~/g, '%7E').replace(/\!/g, '%21').replace(/\'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29'));
}
/**
 *  parseUrl
 *
 *  set parameters from URL
 */
FSWidgetDashboard.prototype.parseUrl = function ()
{
    var query       = location.toString().replace(/^[^?]+/, '').replace('?', '').replace('#.*$', '');
    var obj         = {};
    var separator   = '&';
    var i           = 0;
    var token       = [];

    if ( !query )
        return obj;
    query = query.split(separator);

    for ( i = 0; i < query.length; ++i ) {
        token = query[i].split('=');

        if ( token.length === 1 ) {
            obj[token[0]] = null;
        } else if ( token.length === 2 ) {
			token[1] = decodeURIComponent(token[1]);
            if ( obj[token[0]] === undefined ) {
                obj[token[0]] = token[1];
            } else {
                if ( obj[token[0]] instanceof Array ) {
                    obj[token[0]].push(token[1]);
                } else {
                    obj[token[0]] = [obj[token[0]], token[1]];
                }
            }
        }
    }

    if ( obj.start_date !== undefined ) {
        obj.start_date = obj.start_date.replace(/_/g, '/');
    }
    if ( obj.end_date !== undefined ) {
        obj.end_date = obj.end_date.replace(/_/g, '/');
    }

    if ( obj.date_start !== undefined ) {
        obj.date_start = obj.date_start.replace(/_/g, '/');
    }
    if ( obj.date_end !== undefined ) {
        obj.date_end = obj.date_end.replace(/_/g, '/');
    }

    return obj;
}
/**
 *  getParams
 *
 *  Function collect all params for make request
 *
 *  return object
 */
FSWidgetDashboard.prototype.getParams = function (withPage) {
	var params = {
        'tech_id'           : this.filters().techId(),
        'Owner'      : this.filters().Owner(),
        'dateRange'      : this.filters().dateRange(),
        'Audit'      : this.filters().Audit(),
        'AuditNeeded'      : this.filters().AuditNeeded(),
        'AuditComplete'      : this.filters().AuditComplete(),
        'full_search'        : this.filters().fullSearch(),
        'distance'          : this.filters().distance(),
        'tab'               : this.currentTab,
        'deactivaAll'       : this.deactivaAll,
        'company'           : this.filters().company(),
        'projects'          : this.filters().getProjects(),
        'state'             : this.filters().state(),
        'country'             : this.filters().country(),
        'zip'               : this.filters().zip(),
        'start_date'        : this.filters().dateStart(),
        //13892
        'StandardOffset'        : this.filters().timeZone(),
        'end_date'          : this.filters().dateEnd(),
        'tech'              : this.filters().tech(),
        'region'            : this.filters().region(),
        'call_type'         : this.filters().callType(),
        'win'               : this.filters().win(), 
        'client_wo'         : this.filters().clientWO(),
        'po'                : this.filters().po(),
        'show_to_tech'      : this.filters().showToTech(),
        'site_name'         : this.filters().siteName(),
        'site_number'       : this.filters().siteNumber(),
        'site_status'       : this.filters().siteStatus(),
        'techPaperwork'       : this.filters().techPaperwork(),
        'city'              : this.filters().city(),
        'update_req'        : this.filters().updateRequested(),
        'store_notify'      : this.filters().storeNotified(),
        'checked_in'        : this.filters().checkedIn(),
        'reviewed_wo'       : this.filters().reviewedWO(),
        'tech_confirmed'    : this.filters().techConfirmed(),
        'tech_complete'     : this.filters().techComplete(),
        'site_complete'     : this.filters().siteComplete(),
        'approved'          : this.filters().approved(),
        'extra_pay'         : this.filters().extraPay(),
        'out_of_scope'      : this.filters().outOfScope(),
        'tech_paid'         : this.filters().techPaid(),
        'tech_fname'        : this.filters().techFName(),
        'tech_lname'        : this.filters().techLName(),
        'show_sourced'      : this.filters().showSourced(),
        'show_not_sourced'  : this.filters().showNotSourced(),
        'paper_received'    : this.filters().paperReceived(),
        'lead'              : this.filters().lead(),
        'assist'            : this.filters().assist(),
        'deactivated'       : this.filters().deactivated(),
        'deactivation_code' : this.filters().deactivationCode(),
        'fls_id'            : this.filters().flsId(),
        'route'           : this.filters().Route(),
        'WO_ID_Mode'        : this.filters().WO_ID_Mode(),//14139
        //732
        'SiteContactBackedOutChecked'           : this.filters().SiteContactBackedOutChecked(),
        //end 732
        'short_notice'      : this.filters().shortNotice(),
        'sort1'             : this.sortTool() !== null ? this.sortTool().getSort(1) : null,
        'dir1'              : this.sortTool() !== null ? this.sortTool().getDirection(1) : null,
        'sort2'             : this.sortTool() !== null ? this.sortTool().getSort(2) : null,
        'dir2'              : this.sortTool() !== null ? this.sortTool().getDirection(2) : null,
        'sort3'             : this.sortTool() !== null ? this.sortTool().getSort(3) : null,
        'dir3'              : this.sortTool() !== null ? this.sortTool().getDirection(3) : null
        };
		if (typeof(withPage) != 'undefined' && withPage) {
			params.page = this.sortTool() !== null ? this.sortTool().getPage() : null;
		}
		return params;
    }
FSWidgetDashboard.prototype.setParamsSession = function (params)
{
    if ( !params || typeof params !== 'object' ) return;
    this.filters().po((params.po)?params.po:'');
    this.filters().setProject(params.projects);
    this.filters().state(params.state);
    this.filters().country(params.country);
    this.filters().zip((params.zip)?params.zip:'');

    this.filters().dateStart((params.start_date)?params.start_date:'');
    this.filters().dateEnd((params.end_date)?params.end_date:'');
    this.filters().timeZone((params.StandardOffset)?params.StandardOffset:'');//13892
    this.filters().region((params.region)?params.region:'');
    this.filters().tech((params.tech)?params.tech:'');
    this.filters().callType((params.call_type)?params.call_type:'');
    this.filters().AuditNeeded(typeof(params.AuditNeeded)!= 'undefined'?params.AuditNeeded:null);//360
    this.filters().AuditComplete(typeof(params.AuditComplete) != 'undefined'?params.AuditComplete:null);//360
    this.filters().Owner(typeof(params.Owner) != 'undefined'?params.Owner:null);//360
    this.filters().dateRange(typeof(params.dateRange) != 'undefined'?params.dateRange:null);
    //017
    this.filters().shortNotice(typeof(params.short_notice) != 'undefined'?params.short_notice:null);
    //end017
}
    FSWidgetDashboard.prototype.setParams = function (params)
    {
        if ( !params || typeof params !== 'object' ) return;
        this.filters().Owner(params.Owner);
        this.filters().dateRange(params.dateRange);
        this.filters().AuditNeeded(params.AuditNeeded);
        this.filters().AuditComplete(params.AuditComplete);
        this.filters().techId(params.tech_id);
        this.filters().fullSearch(params.full_search);
        this.filters().win(params.win); 
        this.filters().clientWO(params.client_wo);
        this.filters().po((params.po)?params.po:'');
        this.filters().showToTech(params.show_to_tech);
        this.filters().siteName(params.site_name);
        this.filters().siteNumber(params.site_number);
        this.filters().techPaperwork(params.techPaperwork);
        this.filters().city(params.city);
        this.filters().updateRequested(params.update_req);
        this.filters().storeNotified(params.store_notify);
        this.filters().checkedIn(params.checked_in);
        this.filters().reviewedWO(params.reviewed_wo);
        this.filters().techConfirmed(params.tech_confirmed);
        this.filters().techComplete(params.tech_complete);
        this.filters().siteComplete(params.site_complete);
        this.filters().approved(params.approved);
        this.filters().extraPay(params.extra_pay);
        this.filters().outOfScope(params.out_of_scope);
        this.filters().techPaid(params.tech_paid);
        this.filters().techFName(params.tech_fname);
        this.filters().techLName(params.tech_lname);
        this.filters().showSourced(params.show_sourced);
        this.filters().showNotSourced(params.show_not_sourced);
        this.filters().paperReceived(params.paper_received);
        this.filters().lead(params.lead);
        this.filters().assist(params.assist);
        this.filters().deactivated(params.deactivated);
		try {
			if (params['projects[]'] instanceof Array || typeof params['projects[]'] === 'string') {
	        	this.filters().setProject(params['projects[]']);
			}
			else
	        	this.filters().setProject(params.projects);
		} catch (e) {
	        	this.filters().setProject(params.projects);
		}
        this.filters().state(params.state);
        this.filters().country(params.country);
        this.filters().zip((params.zip)?params.zip:'');
        //FIXME We should use date_start/date_end or start_date/end_date. Both are using now.
        this.filters().dateStart(params.date_start?params.date_start: (params.start_date?params.start_date:''));
        this.filters().dateEnd(params.date_end?params.date_end: (params.end_date?params.end_date:''));
        this.filters().timeZone(params.StandardOffset);//13892
        this.filters().tech(params.tech?params.tech:'');
        this.filters().region(params.region?params.region:'');
        this.filters().callType(params.call_type);
        this.filters().shortNotice(params.short_notice);
        this.filters().flsId(params.fls_id);
        this.filters().distance(params.distance);
        this.filters().Route(params.route);
        this.filters().siteStatus(params.site_status);
        this.filters().WO_ID_Mode(params.WO_ID_Mode);//14139
        //732
        this.filters().SiteContactBackedOutChecked(params.SiteContactBackedOutChecked);
        //end 732
    if ( this.sortTool() && ( params.sort1 || params.sort2 || params.sort3 ) ) {
		$(this.sortTool()._sortSelect).val(params.sort1);
		$(this.sortTool()._directionSelect).val(params.dir1);
        if ( params.sort2 || params.sort3 ) {
            //  Full
            this.sortTool().switchSortTools('full');
            this.sortTool().setFullSortVal(params.sort1, 1);
            this.sortTool().setFullSortVal(params.sort2, 2);
            this.sortTool().setFullSortVal(params.sort3, 3);
            this.sortTool().setFullSortDir(params.dir1, 1);
            this.sortTool().setFullSortDir(params.dir2, 2);
            this.sortTool().setFullSortDir(params.dir3, 3);
            this.sortTool().fullInUsage(true);
        } else {
            this.sortTool().switchSortTools('quick');
            this.sortTool().sort(params.sort1);
            this.sortTool().direction(params.dir1);
            this.sortTool().fullInUsage(false);
			if (!this.sortTool()._useSessionDefault)
	            this.sortTool().initSortTool();
			else
				this.sortTool().update();
			
        }
    }

}

FSWidgetDashboard.prototype.changeDasboaardVersion = function (el){
    this.sortTool().prepareItems($(el).val());
    this.sortTool().initSortTool();
    this.show({
        tab:this.currentTab,
        params:this.getParams()
    })
}

FSWidgetDashboard.prototype.onChangeCountry = function () {
    $.ajax({
        url         : '/widgets/dashboard/js/state-info/',
        dataType    : 'json',
        data        : {
            country: $("#filterCountry").val()
        },
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            if (data) {
                //AUTOFILL
				var option = '';
				option += '<option selected="selected" value="">Select State</option>';
                for(index in data) {
					option += '<option value="' + index + '">' + data[index] + '</option>';
				}
				$("#filterState").html(option);
				state = this.filters().state();

				if (state) $("#filterState").val(state);
               
			}
                    
                        $(".hoverText").bt({
                            titleSelector: "attr('bt-xtitle')",
                            fill: 'white',
                            cssStyles: {color: 'black', fontWeight: 'bold', width: '300px'},
                            animate:true
                        });   
                        //====
        }
    });	
}

FSWidgetDashboard.prototype.openDashboard = function() {
	url = '/clients/wos.php?v=' + window._company;
	if ($("#isDash").val() != '1') {
		location.href = 'dashboard.php?v=' + window._company + '&isDash=1&goto=dashboard';
		return false;
	}
	hideLeftSidebar();
	hideSidebar();
	$('#detailsContainer').html('');
	$('#dashcentercontent').css('width', '100%');
	this.openFramePage(url);
	return false;
}

FSWidgetDashboard.prototype.openFramePage = function(url) {
	if (url == 'dashboard') {
		this.openDashboard();
		return;
	}
	if (url.indexOf('?') == -1) url + '?';
	$('#detailsContainer').html("");
	$('<iframe id="dashFrame"/>').attr('src', url + '&isDash=1').attr('width', '100%').appendTo($('#detailsContainer'));
		$('iframe').load(function(){
			//Get Body of iframe
			var slider = $('iframe').contents().find('a.title');
			//Bind event
			slider.click(function(){
				//get new height
				var newHeight = $('iframe').contents().find('body').height();
				//resize frame
				$('iframe').attr('height', newHeight);
				
			});
		});
}
FSWidgetDashboard.prototype.saveFilterTech = function(){
    if(window._search || this.returnAllSearch) this.seachOut = true;
    this.paramsOll = this.getParams();
    this.returnAllSearch = false;
    //this.resetFilters();
}
FSWidgetDashboard.prototype.UndoSortTech = function(){
    if(typeof(this.paramsOll) != 'undefined')
        this.setParams(this.paramsOll);
    if(this.seachOut)
       {
           this.seachOut = false;
           this.returnAllSearch = true;
           this.currentTab = "all";
        return "all";
       }
    else
        return this.currentTab;
}
FSWidgetDashboard.prototype.getAttentionID = function(tab){
    var _AttentionID = {
        "created":14
        ,"published":15
        ,"assigned":16
        ,"workdone":17
        ,"incomplete":18
        ,"completed":19
        ,"all":20
        ,"deactivated":21};
    var AttentionID = 17;
    $.each(_AttentionID, function (index, value) {
        if(index == tab){
            AttentionID = value;
            return false;
        }
    });
    return AttentionID;
}
FSWidgetDashboard.prototype.bindEvent = function(){
    if(typeof(dashboardwo) != 'undefined'){
    _dashboardwo = dashboardwo.CreateObject({
        id:"_dashboardwo",
        detailsWidget:this
    });
}
}
FSWidgetDashboard.prototype.filterSelected = function(){
	this.showFilterByName($('#moreFilters').val());
}

FSWidgetDashboard.prototype.showFilterByName = function(val){
	var section = $("#" + val).show().detach();
	$('#sectionProject').parent().append(section);
	$("#moreFilters option[value='"+val+"']").remove();
	var h = $('#filterForm').parent().parent().height();
	if (h == 0) h = $('#filterForm').height() + 15;
	$('#filterForm').parent().parent().css('height', h + 28 + "px");
}

FSWidgetDashboard.prototype.filterValueIsSet = function (val) {
	return val != '' && val != null && val != undefined;
}
//819
FSWidgetDashboard.prototype.DownloadDocumentButton = function () {
    
    var countSelected = 0;
    
    if($("#chkDownloadDocumentMaster").is(":checked"))
    {
        countSelected = $("#chkDownloadDocumentMaster").attr("counttechfiles");
    }
    else
    {
        countSelected=$('.chkDownloadDocumentItem[VXSelectItem=true]:checked').size();
    }

    if(countSelected<=0)
    {
        return;
    }
        
    if(countSelected>1)
    {
        var html='<div>Download tech documents for selected Work Orders to:</div>';
        html+='<div style="padding-top:8px;"><input type="radio" class="classSelectedSubFolder" name="radFolder" checked="checked" value="1">One sub-folder per Work Order</div>';
        html+='<div style="padding-top:5px;"><input type="radio" name="radFolder" value="2">One folder for all documents</div>';
        html+='<div class="tCenter" style="padding-top:8px;"><input type="button" class="link_button_popup" onclick="roll.hide();" value="Cancel" />';
        html+='<input type="button" class="link_button download_button downloadDocumentForSelectedWos" value="Download" id="downloadDocumentForSelectedWos"></div>';
        var opt = {};
        opt.width    = 400;
        opt.height   = '';
        opt.position = 'middle';
        opt.title    = 'Multiple Document Download Options';
        opt.body     = html;
        roll.autohide(false);
        roll.showNotAjax(this, null, opt);         
        $(".downloadDocumentForSelectedWos")
        .unbind("click",FSWidgetDashboard.prototype.downloadDocumentForSelectedWos)
        .bind("click",FSWidgetDashboard.prototype.downloadDocumentForSelectedWos);
    }
    else
    {
        FSWidgetDashboard.prototype.downloadDocumentForSelectedWos();
    }
}

FSWidgetDashboard.prototype.downloadDocumentForSelectedWos = function () {
    var IsSelectedSubFolder=$(".classSelectedSubFolder").is(":checked");

    var lstWinNum="";
    var lstClientID="";
    var winCount = 0;
    
    if($("#chkDownloadDocumentMaster").is(":checked"))
    {
        lstWinNum = $("#chkDownloadDocumentMaster").attr("winnums");
        lstClientID = $("#chkDownloadDocumentMaster").attr("woidlist");
        lstClientID = lstClientID.replace(/[^a-zA-Z0-9,]/g,'')
        winCount = $("#chkDownloadDocumentMaster").attr("counttechfiles");
    }
    else
    {
    $('.chkDownloadDocumentItem[VXSelectItem=true]:checked').each(function()
    {
        lstWinNum += $(this).attr("value")+",";
        lstClientID += $(this).attr("ClientID").replace(/[^a-zA-Z0-9]/g,'')+",";
        winCount += 1;
    });

    if(lstWinNum.length>0)
        lstWinNum=lstWinNum.substr(0,lstWinNum.length-1) ;
    }
        
    if(winCount==1)
    {
        IsSelectedSubFolder = false;
    }
    var urlpost="";
    urlpost="/widgets/dashboard/do/file-all-wos-all-documents-download/win/"+encodeURIComponent(lstWinNum)+"/woid/"+encodeURIComponent(lstClientID)+"/issub/"+IsSelectedSubFolder;
    $(".aDownloadDocumentButton").attr("href",urlpost);
    FSWidgetDashboard.prototype.CallClickEvent("aDownloadDocumentButton",document);
    roll.hide();
}

FSWidgetDashboard.prototype.CallClickEvent = function (obj,Document) {
    if (typeof (Document) == "undefined")
    {
        Document = document;
    }

    if (typeof (obj) == "string") {
        obj = Document.getElementById(obj);
    }

    if (obj == null) return;

    try
    {
        obj.focus();
    }
    catch(ex)
    {
    //do nothing
    }

    if (obj.dispatchEvent)
    {
        var e = Document.createEvent("MouseEvents");

        e.initEvent("click", true, true);
        obj.dispatchEvent(e);

    }
    else
    {
        obj.click();
    }
}

FSWidgetDashboard.prototype.chkDownloadDocumentItem = function () {
    var count=$("#chkDownloadDocumentMaster").attr("counttechfiles");
    var  countSelected=$('.chkDownloadDocumentItem[VXSelectItem=true]:checked').size();
    if(count==countSelected)
    {
        $(".chkDownloadDocumentMaster").attr("checked",true);
    }
    else
    {
        $(".chkDownloadDocumentMaster").attr("checked",false);    
    }
    if(countSelected>0)
    {
        $(".DownloadDocumentButton").css("cursor","pointer");
    }
    else
    {
        $(".DownloadDocumentButton").css("cursor","");
    }
}

FSWidgetDashboard.prototype.chkDownloadDocumentMaster = function () {
    var checked=$(this).is(":checked");

    $(".chkDownloadDocumentItem[VXSelectItem=true]").each(function(){
        $(this).attr("checked",checked);
    });
    var  countSelected=$('.chkDownloadDocumentItem[VXSelectItem=true]:checked').size();
    if(countSelected>0)
    {
        $(".DownloadDocumentButton").css("cursor","pointer");
    }
    else
    {
        $(".DownloadDocumentButton").css("cursor","");
    }
}
//end819
