/**
 * FSTechProfile
 *
 * @param Object|null params
 * @access public
 * @return void
 */
function FSTechProfile ( params ) {
    FSWidgetDashboard.call(this, params);

    this.params.POPUP.avaliablePopups = {};
    this.params.WIDGET.avaliableTabs = {
        profile : {
            module:'dashboard',
            controller:'tech',
            action:'update'
        },
        adminprofile : {
            module:'admindashboard',
            controller:'tech',
            action:'update'
        },
        admincreate: {
            module:'admindashboard', 
            controller:'tech',
            action:'create'
        }
    };
    
    switch (this.params.tab) {
        case 'profile':
            this._type = 'techs';
            break;
        case 'adminprofile':
            this._type = 'admin';
            break;
        case 'admincreate':
            this._type = 'admin';
            this._create = true;
            break;
    }
	
    this._techId = this.params.techId;
    this.loaderVisible = false;
}

FSTechProfile.prototype = new FSWidgetDashboard();
FSTechProfile.prototype.constructor = FSTechProfile;

FSTechProfile.NAME          = 'FSTechProfile';
FSTechProfile.VERSION       = '0.1';
FSTechProfile.DESCRIPTION   = 'Class FSTechProfile';

FSTechProfile.prototype.userType = function() {
    return this._type;
}

/**
 *  show
 *
 *  Function displays widget
 *
 *  @param Object|null options
 */
FSTechProfile.prototype.show = function(options) {

    me = this;
    var p = options || {
        tab:this.currentTab,
        params:this.getParams()
    };
    var queryString = '/';
    var _url;

    // TODO params needs be reconfigured
    if ( p.params !== undefined && p.params !== null && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params ) {

            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] === true || p.params[key] === false ) {
                if ( p.params[key] === false ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] !== undefined && p.params[key] !== null && p.params[key] !== "" && ( (typeof(p.params[key]) == 'string' && p.params[key].toLowerCase() !== 'none') || typeof(p.params[key]) == 'number') ) {
            //queryString += key + '/' + p.params[key] + '/';
            } else {
                delete p.params[key];
            }
        }
    } else {
        p.params = {};
    }

    try {
        if ( p.tab !== undefined && p.tab !== null ) {
            this.currentTab = p.tab;
            _url = this._makeUrl(p.tab, queryString);
            var idx;
            for ( idx in this.params.WIDGET.avaliableTabs ) {
                $('#'+idx).toggleClass('current', idx === this.currentTab);
            }
        } else if ( p.popup !== undefined && p.popup !== null ) {
            _url = this._makeUrl(p.popup, queryString);
        }
    } catch ( e ) {
        return false;
    }
    
    this.showPreloader();

    if ( this.tabOpenXhrRequest ) {
        this.tabOpenXhrRequest.abort();
        this.tabOpenXhrRequest = null;
    }
    
    // get page size
    if ($('#pageSizeSelector') ) {
        p.params.size = $('#pageSizeSelector').val();
    }
    
    
    //this.sortTool().prepareItems(version);
    //this.sortTool().initSortTool();
	
    p.params['techID'] = this._techId;
	
    this.tabOpenXhrRequest = jQuery.ajax({
        url : _url,
        data : p.params,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/" + this._type + "/logIn.php";
            } else {
                FSWidget.fillContainer("Sorry, but error occurred. No results found.", this);
            }
            this.tabOpenXhrRequest = null;
        },
        success : function(data, status, xhr) {
            $('body').css('cursor', '');
            FSWidget.fillContainer(data, this);
            this.initForm();
            if (!me._create)
                me.loadTechInfo();
            if (me._create && me._type == 'admin') {
                $("#AcceptTerms").attr('checked', true);
            }
            $(".showOnEdit").hide();
            me.hideLoader();
            me.openBasicReg();
            if (me._create)
                me.editBasicRegistration();
        }
    });
}

FSTechProfile.prototype.initForm = function() {
    me = this;
    $("#BusinessStatment").jqEasyCounter({
        'maxChars' : 600,
        'maxCharsWarning' : 585,
        'msgFontSize' : '12px',
        'msgTextAlign' : 'left',
        'msgWarningColor' : '#F00',
        'msgAppendMethod' : 'insertBefore'
    });
	
    if($("#acc").length > 0){
        $("#acc dt").click(function(){
            var sel = $(this);
			
            if (me._create && sel.attr('id') != 'basicRegistration' && sel.attr('id') != 'adminUsage') {
                me.saveBasicRegistration();
                return;
            }

            sel.addClass("act current");

            sel.parent().children("dd").each(function(){
                if($(this).is(":visible") && !$(this).prev("dt").hasClass("current")){
                    if($(this).prev("dt").hasClass("act")){
                        $(this).prev("dt").removeClass("act");
                    }
                    $(this).slideUp(300);
                }
            });

            sel.next().slideToggle(300, function(){
                if(!$(this).is(":visible")){
                    $(this).prev("dt").removeClass("act");
                }
                sel.removeClass("current");
            });

            return false;
        });
    }

    $("#businessProfile").validate({
        invalidHandler: function(e, validator) {
            $.ajax({
                type: "POST",
                url: "/widgets/dashboard/do/country-require-zip",
                dataType    : 'json',
                cache       : false,
                data: 'countrycode='+jQuery("#Country").children('option:selected').val(),
                success: function (data) {
                    if(!data.success)
                    {
                        jQuery("#Zipcode").removeClass("required"); 
                        jQuery("#Zipcode").removeClass("invalid"); 
                    }
                    else
                    {
                        jQuery("#Zipcode").addClass("required"); 
                    }
                    var errors = validator.numberOfInvalids();
	
                    if (errors) {
                        var message = errors == 1
                        ? 'You missed 1 field. It has been highlighted below'
                        : 'You missed ' + errors + ' fields.  They have been highlighted below';
                        $("div.error span").html(message);
                        $("div.error").show();
                    } else {
                        $("div.error").hide();
                    }
                }
            });
        },
        onkeyup: false,
        submitHandler: function(form) {
            $("div.error").hide();
            me.showLoader();
            submitBusinessProfile(form);
        },
        rules: {},
        messages: {},
        errorPlacement: function(error,element){
            error.appendTo(element.parent().parent().parent().next().find("div.status"));
        },
        errorClass: "invalid",
        debug:true
    });

    jQuery.validator.addMethod("password", function( value, element ) {
            return true;
    }, "New Password Rules: Your password must be at least 8 characters long.");

    jQuery.validator.addMethod("paymentNumCheck", function( value, element ) {
        if(value.substring(0,4) != "XXXX" && isNaN(value) == true){
            return false;
        }else{
            return true;
        }
			
    },"This field must contain numbers only");

    jQuery.validator.addMethod("routingLenCheck", function( value, element ) {
        if (value.substring(0,4) != "XXXX" && value.length != 9) {
            return false;
        }else{
            return true;
        }
    },"The Routing Number must be 9 digits");

    jQuery.validator.addMethod("emailEqualTo", function( value, element ) {
        if($('body').data('email') == $("#PrimaryEmail").val()){
            return true;
        }else{
            return false;
        }
		
    }, "Please confirm that the address you entered is the same as above");

    jQuery.validator.addMethod("phoneTen", function( val, element ) {
        if ($("#Country").val() != 'US' && $("#Country").val() != 'CA') return true;
        if (val != null && val != "") {
            // massage phone number
            validFormat = /^[(]?(\d{3})[)]?(?:\s|-|.)*(\d{3})[-\s.]*(\d{4})$/;
            parts = val.match(validFormat);
            if (parts != null)
                $(element).val(parts[1] + "-" + parts[2] + "-" + parts[3]);
            else return false;
        }
        return true;
    }, "Phone number must be ###-###-####, or change selected country");

    jQuery.validator.addMethod("phoneMX", function( val, element ) {
        if ($("#Country").val() != 'MX') return true;
        if (val != null && val != "") {
            // massage phone number
            validFormat = /^\+52 (\d{3} \d{3} \d{4}|\d{2} \d{4} \d{4})$/;
            parts = val.match(validFormat);
            if (parts == null)
                return false;
        }
        return true;
    }, "Phone number must be +52 ## #### #### or +52 ### ### ####, or change selected country");
    
    jQuery.validator.addMethod("phoneBS", function( val, element ) {
        if ($("#Country").val() != 'BS') return true;
        if (val != null && val != "") {
            // massage phone number
            validFormat = /^\+44 (\d{2})[)]?(?:\s|-|.)*(\d{4})[-\s.]*(\d{5})$/;
            parts = val.match(validFormat);
            if (parts == null)
                return false;
        }
        return true;
    }, "Phone number must be +44 ## #### ####, or change selected country");
        
    jQuery.validator.addMethod("requireIfFilledTextPhone", function( value, element ) {
        if ($("#SMS_Number").val() != "" && value == "") return false;
        return true;
    }, "Please select a Service Provider");
	
    jQuery.validator.addMethod("pwEqualTo", function( value, element ) {
            if($('body').data('password') == $("#Password").val()){
            return true;
        }else{
                if($("#Password").val() == $("#passwordconfirm").val()){
                    return true;
                }else{
            return false;
        }
            }
		
    }, "Please enter the same password as above.");
		
    
		
    jQuery.validator.addMethod("pwFormat", function( value, element ) {
        if ($(element).data('default') == value) return true;
        if (value == null || value == "") return true;
        //validFormat = /^(?=.*[A-Z]|[a-z])(?=.*\d)(.{8,15})$/;
        var validFormat = /^(?=.*\d)(?=.*[a-z]|[A-Z]).{8,15}$/;
        var parts = value.match(validFormat);
        if (parts == null) {
            //$(element).val("");
            return false;
        }
        return true;
    }, "Password must be between 8-15 characters and contain one letter and one number.");

    techRegValidator = $("#techRegistration").validate({
        invalidHandler: function(e, validator) {
            $.ajax({
                type: "POST",
                url: "/widgets/dashboard/do/country-require-zip",
                dataType    : 'json',
                cache       : false,
                data: 'countrycode='+jQuery("#Country").children('option:selected').val(),
                success: function (data) {
                    if(!data.success)
                    {
                        jQuery("#Zipcode").removeClass("required"); 
                        jQuery("#Zipcode").removeClass("invalid"); 
                    }
                    else
                    {
                        jQuery("#Zipcode").addClass("required"); 
                    }
                    var errors = validator.numberOfInvalids();
		
                    if (errors) {
                        var message = errors == 1
                        ? 'You missed 1 field. It has been highlighted below'
                        : 'You missed ' + errors + ' fields.  They have been highlighted below';
                        $("div.error span").html(message);
                        $("div.error").show();
                    } else {
                        $("div.error").hide();
                    }
                }
            });
        },
        onkeyup: false,
        submitHandler: function(form) {
            $("div.error").hide();
            me.showLoader();
            if (!me._create)
            {
                submitUpdateTechInfo(form);
                
            }
            else
            {
                me.submitUpdateTechInfo(form);
               
            }
        },
        rules: {
            PrimaryPhone: {
                phoneTen: true,
                //				phoneMX: true,
                //                                phoneBS: true,
                remote: {
                    url: "/widgets/dashboard/do/tech-phone-not-taken",
                    type: "post",
                    data: {
                        TechID: me._techId
                    }
                }
            },
            SecondaryPhone: {
                phoneTen: true,
                //				phoneMX: true,
                //                                phoneBS: true,
                remote: {
                    url: "/widgets/dashboard/do/tech-phone-not-taken",
                    type: "post",
                    data: {
                        TechID: me._techId
                    }
                }
            },
            SMS_Number: {
                phoneTen: true,
                phoneMX: false
            },
            PrimaryEmail: {
                email: true
            },
            confPrimaryEmail: {
                email: true,
                emailEqualTo: true
            },
            secondaryEmail: {
                email: true
            },
            UserName: {
                remote: {
                    url: "/widgets/dashboard/do/tech-user-not-taken",
                    type: "post",
                    data: {
                        TechID: me._techId
                    }
                }
            },
            Password: {
                password: true,
                pwFormat: true
            },

            passwordconfirm: {
                pwEqualTo: true
            },
            CellProvider: {
                requireIfFilledTextPhone: true
            }
        },
        messages: {
            passwordconfirm: {
                required: " ",
                pwEqualTo: "Please enter the same password as above"	
            },
            primaryEmail: {
                email: "Please enter a valid email address",
                remote: jQuery.validator.format("{0} is already taken, please enter a different address.")	
            },
            confPrimaryEmail:{
                emailEqualTo: "Please confirm that the address you entered is the same as above"
            },
            UserName: {
                remote: jQuery.validator.format("{0} already taken")
            },
            PrimaryPhone: {
                remote: "The phone number is already taken, please enter a different phone number."
            },
            SecondaryPhone: {
                remote: "The phone number is already taken, please enter a different phone number."
            }
        },
        errorPlacement: function(error,element){
            error.appendTo(element.parent().parent().parent().next().find("div.status"));
        },
        errorClass: "invalid",
        debug:true
    });


    $("#paymentInfo").validate({
        invalidHandler: function(e, validator) {
            var errors = validator.numberOfInvalids();
		
            if (errors) {
                var message = errors == 1
                ? 'You missed 1 field. It has been highlighted below'
                : 'You missed ' + errors + ' fields.  They have been highlighted below';
                $("div.error span").html(message);
                $("div.error").show();
            } else {
                $("div.error").hide();
            }
        },
        onkeyup: false,
        submitHandler: function(form) {
            $("div.error").hide();
            me.showLoader();
            submitPaymentInfo(form);
        },
        rules: {
            primaryEmail: {
                email: true
            },
            RoutingNum: {
                paymentNumCheck: true,
                routingLenCheck: true,
                required: true
            },
            AccountNum: {
                paymentNumCheck: true,
                required: true
            }
        },
        messages: {
            primaryEmail: {
                email: "Please enter a valid email address",
                remote: jQuery.validator.format("{0} is already taken, please enter a different address.")	
            },
            RoutingNum:{
                required: ""
            },
            AccountNum: {
                required: ""
            }
        },
        errorPlacement: function(error,element){
            error.appendTo(element.parent().parent().parent().find("div.status"));
             
            if($(element).attr("name") == "AgreeTerms"){
                $(element).parent().addClass('invalid');
            }
        },
        success:function(label){
            if(label[0].htmlFor == "AgreeTerms" && $("#paymentAgree").hasClass("invalid")){
                $("#paymentAgree").removeClass("invalid");
            }	
        },
        errorClass: "invalid",
        debug:true
    });
		
		
    $(".certForms").click(function(){
        if($(this).attr('rel') == "insurance"){
            var messageHtml = "<h2>FieldSolutions Insurance Coverage</h2>";
            messageHtml += "<p>FieldSolutions insures our clients against risk of work on-site. As such, we never put you through the hassle of producing your insurance overages, we don't verify whether your payments are current; and most important we don't charge you a fee like other systems for coverages. Insurance coverage is just one reason FieldSolutions is the most preferred place to get technology field work. Remind your clients that you want to get your work and paid through FieldSolutions.</p>";
			
            $("<div></div>").fancybox(
            {
                'autoDimensions' : false,
                'width'	: 420,
                'height' : 225,
                'transitionIn': 'none',
                'transitionOut' : 'none',
                'content' : messageHtml
            }
            ).trigger('click');
				
        }else{
					
            $("<div></div>").fancybox(
            {
                'autoDimensions' : false,
                'width'	: 900,
                'height' : 850,
                'transitionIn': 'none',
                'transitionOut' : 'none',
                'type': 'iframe',
                'href': $(this).attr('rel')
            }
            ).trigger('click');
        }
				
    });
        $(".blueRibbon").click(function(){
            var messageHtml = "<h2 style='text-decoration:underline'><center>Blue Ribbon Techs</center></h2>";
            messageHtml += "<p style='padding:0px'>FieldSolutions has partnered with Blue Ribbon Techs to redefine the way techs, clients, and platforms maintain and access Technician Background Checks, Drug Tests, and other credentials. To this end, we have moved our background check and drug test credentials to Blue Ribbon Techs so that FieldSolutions&rsquo; techs can get the benefit of this great new way to save money and get qualified for work faster.</p>";
            messageHtml += "<p style='padding:0px'>BRT&rsquo;s guiding principle is that technicians should not have to repeatedly spend time and money for the same credentials time and time again. Instead, you can focus on what matters to you &mdash; getting work, making money, and growing your business.</p>";
            messageHtml += "<p style='padding:0px'>Register at <a href='http://www.BlueRibbonTechs.com' target='_blank'>www.BlueRibbonTechs.com</a> today, and don&rsquo;t forget to add FieldSolutions as an authorized client so FieldSolutions&rsquo; clients can see your credentials! If you&rsquo;ve already registered with BRT and are not seeing the Blue Ribbon Techs &lsquo;Tag&rsquo;, log into BRT and make sure FieldSolutions is an authorized client.</p>";
            messageHtml += "<p style='padding:0px'>For more information check our FAQ or contact <a href='mailto:support@FieldSolutions.com'>support@FieldSolutions.com</a> or <a href='mailto:support@blueribbontechs.com'>support@blueribbontechs.com</a>.</p>";
			
            $("<div></div>").fancybox({
                'autoDimensions' : false,
                'width'	: 640,
                'height' : 300,
                'transitionIn': 'none',
                'transitionOut' : 'none',
                'content' : messageHtml
            }).trigger('click');
        });
        
        $(".bootCamp").click(function(){
            var messageHtml = "<h2 style='text-decoration:underline'><center>Technician Boot Camp Certification</center></h2>";
            messageHtml += "<p style='padding:0px'>FieldSolutions has partnered with Blue Ribbon Techs to redefine the way techs, clients, and platforms maintain and access Technician Background Checks, Drug Tests, and other credentials. To this end, we have moved our background check and drug test credentials to Blue Ribbon Techs so that FieldSolutions&rsquo; techs can get the benefit of this great new way to save money and get qualified for work faster.</p>";
            messageHtml += "<p style='padding:0px'>Only FieldSolutions has arranged exclusive early access to Blue Ribbon Techs&rsquo; Boot Camp Certification focusing on the soft skills needed to succeed as a field service technician. We believe that this certificate will soon be required by many FieldSolutions clients. As an added bonus, this industry-standard certification is available FREE to FieldSolutions techs through the end of 2012</p>";
            messageHtml += "<p style='padding:0px'>To get started, register at <a href='http://www.BlueRibbonTechs.com' target='_blank'>www.BlueRibbonTechs.com</a> and begin the Technician Boot Camp Certification Process. Don&rsquo;t forget to add FieldSolutions as an authorized platform so clients can see your credentials!</p>";
            messageHtml += "<p style='padding:0px'>For more information check our FAQ or contact <a href='mailto:support@FieldSolutions.com'>support@FieldSolutions.com</a> or <a href='mailto:support@blueribbontechs.com'>support@blueribbontechs.com</a>.</p>";

            $("<div></div>").fancybox({
                'autoDimensions' : false,
                'width'	: 640,
                'height' : 300,
                'transitionIn': 'none',
                'transitionOut' : 'none',
                'content' : messageHtml
            }).trigger('click');
        });
    $("#Vehicle").bind("click",function(){
					
        if($(this).attr("checked") == true){
            $("#vehicleMisc").show();
        }else{
            $("#vehicleMisc").hide();
        }
    });
			
    $("#Ladder").bind("click",function(){
					
        if($(this).attr("checked") == true){
            $("#ladderMisc").show();
        }else{
            $("#ladderMisc").hide();
        }
    });
			
    $("#PunchTool").bind("click",function(){
					
        if($(this).attr("checked") == true){
            $("#punchToolMisc").show();
        }else{
            $("#punchToolMisc").hide();
        }
    });
			
    $("#PaymentMethod").change(function(){
        if($(this).val() == "Direct Deposit"){
            $("#paymentAgree").show();
            $("#AgreeTerms").addClass("required");
        }else{
            $("#paymentAgree").hide();
            $("#AgreeTerms").removeClass("required");
        }
    });	
				
    $("#BG_Test_ResultsDate_Full").change(me.updateBasicBC);
    $("#Bg_Test_Pass_Full").change(me.updateBasicBC);
    $("#Bg_Test_ReqDate_Full").change(me.updateBasicBC);
    $("#Bg_Test_Req_Full").change(me.updateBasicBC);
}

FSTechProfile.prototype.updateBasicBC = function() {
    basicDate = $.datepicker.parseDate("yy-mm-dd",$("#Bg_Test_ResultsDate_Lite").val());
    compDate = $.datepicker.parseDate("yy-mm-dd",$("#BG_Test_ResultsDate_Full").val());
    if (compDate >= basicDate) {
        $("#Bg_Test_ResultsDate_Lite").val($("#BG_Test_ResultsDate_Full").val());
        $("#Bg_Test_Pass_Lite").val($("#Bg_Test_Pass_Full").val());
        $("#Bg_Test_ReqDate_Lite").val($("#Bg_Test_ReqDate_Full").val());
        $("#Bg_Test_Req_Lite").val($("#Bg_Test_Req_Full").val());
    }
}

FSTechProfile.prototype.loadTechInfo = function()
{
    me = this;

    if (!this._create)
    {
        $.ajax({
            type: "POST",
            url: '/widgets/dashboard/do/get-tech-info/',
            dataType    : 'json',
            cache       : false,
            data: {
                id:this._techId, 
                type:this._type
            },
            success     : function (data)
            {
                for(index in data.techInfo)
                {
                    if(data.techInfo[index] == "null" || data.techInfo[index] == null)
                    {
                        data.techInfo[index] = "";
                    }

                    if(index == "PaymentMethod")
                    {
                        if(data.techInfo[index] == "Direct Deposit")
                        {
                            $("#paymentAgree").show();
                            $("#AgreeTerms").addClass("required").attr('checked', true);
                        }
                        else
                        {
                            $("#paymentAgree").hide();
                            $("#AgreeTerms").removeClass("required").attr('checked', false);
                        }
                    }
										
                    if(index == "MaskedRoutingNum")
                    {
                        $("#RoutingNum").val(data.techInfo[index]);
                        $("#RoutingNumLabel").html(data.techInfo[index]);
                    }
                    else if(index == "MaskedAccountNum")
                    {
                        $("#AccountNum").val(data.techInfo[index]);
                        $("#AccountNumLabel").html(data.techInfo[index]);
                    }
                    else if(index == "PrimaryPhone")
                    {
                        detailObject.onInit.RequitePhoneForAdmin(
                            "#"+index+"Label",
                            data.techInfo[index],
                            data.techInfo['TechID'],
                            data.techInfo['PrimaryPhoneStatus'],
                            1,
                            'html',
                            2,
                            true,
                            "#imgReQuitePhone .imgClassReQuitePhoneTech"
                            );  
                        detailObject.onInit.RequitePhoneForAdmin(
                            "#"+index,
                            data.techInfo[index],
                            data.techInfo['TechID'],
                            data.techInfo['PrimaryPhoneStatus'],
                            1,
                            'text',
                            2,
                            true,
                            "#imgReQuitePhone .imgClassReQuitePhoneTech"
                            );     
                    }
                    else if(index == "SecondaryPhone")
                    {
                        detailObject.onInit.RequitePhoneForAdmin(
                            "#"+index+"Label",
                            data.techInfo[index],
                            data.techInfo['TechID'],
                            data.techInfo['SecondaryPhoneStatus'],
                            2,
                            'html',
                            2,
                            true,
                            "#imgSecondaryPhoneStatus .imgClassReQuitePhoneTech"
                            );  
                        detailObject.onInit.RequitePhoneForAdmin(
                            "#"+index,
                            data.techInfo[index],
                            data.techInfo['TechID'],
                            data.techInfo['SecondaryPhoneStatus'],
                            2,
                            'text',
                            2,
                            true,
                            "#imgSecondaryPhoneStatus .imgClassReQuitePhoneTech"
                            );     
                    }
                    else if(index == "CopierSkillsAssessment")
                    {
                        jQuery(".cmdCopiesSkillView").attr("techid",data.techInfo["TechID"]);
                        if(data.techInfo[index] == 1)
                        {    
                            $("#"+index+"Display").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                        }
                        else
                        {
                            var el = jQuery(".cmdCopiesSkillView").parent();
                            var html = jQuery(".cmdCopiesSkillView").html();
                            jQuery(el).html(html);

                        }
                    }  
                    else if(index == "CopierSkillsAssessmentDate")
                    {
                        if(data.techInfo["CopierSkillsAssessment"] == 1)
                        {    
                            $("#"+index+"Display").html(data.techInfo[index]);
                        }
                    }
                    else if(index == "Country")
                    {
                        //041
                        var cntry = "";
                        var bankCntry = "";
                        for(countries in data.countries)
                        {
                            if(data.countries[countries].Code==data.techInfo[index])
                            {
                                var cntry = data.countries[countries].Name;
                                var bankCntry = data.countries[countries].id;
                        }
                        }
                        $("#"+index).val(data.techInfo[index]);
                        $("#"+index+"Label").html(cntry);
                        
                        var Requirecountry = {"US":"US","CA":"CA","MX":"MX"};

                        if(Requirecountry.hasOwnProperty(data.techInfo[index]))
                        {
                            $("#State").removeClass("required"); 
                            $("#State").addClass("required");
                            $("#State").removeAttr("disabled");
                            $("#tdstatelabel").html("<span id='reqStar'>*</span>State/Province:");
                    }
                        else
                        {
                            $("#State").removeClass("required"); 
                            $("#State").removeClass("invalid");
                            $("#State").attr("disabled","disabled");
                            $("#tdstatelabel").html("<span id='reqStar'>&nbsp;</span>State/Province:");
                        }
                        //end 041
                    }
                    else if (index == "Bg_Test_Pass_Lite" || index == "Bg_Test_Pass_Full"
                        || index == 'DrugTest_Pass') {
                        if (data.techInfo[index] == true) $("#"+index).val('Pass');
                        else if (data.techInfo[index] == "") $("#"+index).val('None');
                        else $("#"+index).val('Fail');
                    }					
                    else if(index == "EMCCert")
                    {
                        for(var j= 0; j< data.techInfo[index].length;j++)
                        {
                            var _date  = data.techInfo[index][j].date;
                            if(jQuery.trim(_date) == "") _date = "&nbsp;";
                            else
                            {
                                var part = data.techInfo[index][j].date.split(/[- :]/);
                                bgLiteDate = new Date(part[0], part[1]-1, part[2]);

                                var _month = bgLiteDate.getMonth()+1;
                                var _day = bgLiteDate.getDate();
                                var _smonth = _month < 10? "0"+_month.toString(): _month.toString();
                                var _sday = _day < 10? "0"+_day.toString(): _day.toString();

                                _date= _smonth + "/" + _sday + "/" + bgLiteDate.getFullYear();
                            }
                            //_date = $.datepicker.formatDate('mm/dd/yy', new Date(data.techInfo[index][j].date));
                            jQuery('#EMCCertDisplay').append('<div>'+data.techInfo[index][j].name+'</div>');
                            jQuery('#EMCCertDateDisplay').append('<div>'+_date+'</div>');
                        //                                                    if(typeof(_GXEMCCert) != 'undefined')
                        //                                                    {
                        //                                                        _GXEMCCert.techid = data.techInfo['TechID'];
                        //                                                    }    
                        }
                    }
                    else if(index == 'GGE1stAttemptStatus'){
                        if (data.techInfo['GGE1stAttemptStatus'] == "Pass" || data.techInfo['GGE1stAttemptStatus'] == "Fail") 
                        {    
                                jQuery("#GGETechEvaluationFirst").html(data.techInfo['GGE1stAttemptStatus']+" ("+data.techInfo['GGE1stAttemptScore']+"/20)"); 
                                jQuery("#CbxGGETechEvaluationFirst").val(data.techInfo['GGE1stAttemptScore']);
                        }
                        else if(data.techInfo['GGE1stAttemptStatus'] == "Pending") {
                                jQuery("#GGETechEvaluationFirst").html(data.techInfo['GGE1stAttemptStatus']); 
                                jQuery("#CbxGGETechEvaluationFirst").val(data.techInfo['GGE1stAttemptStatus']);
                        } 
                        jQuery("#GGETechEvaluationDateInputFirst").attr("val",data.techInfo['GGE1stAttemptDate']);
                        jQuery("#GGETechEvaluationDateInputSecond").attr("val",data.techInfo['GGE2ndAttemptDate']);
                        jQuery("#GGETechEvaluationDate").html(
                            data.techInfo['GGE1stAttemptDate']
                            +"<br>"
                            +data.techInfo['GGE2ndAttemptDate']);
                        if (data.techInfo['GGE2ndAttemptStatus'] == "Pass" || data.techInfo['GGE2ndAttemptStatus'] == "Fail") {
                                jQuery("#GGETechEvaluationSecond").html(data.techInfo['GGE2ndAttemptStatus']+" ("+data.techInfo['GGE2ndAttemptScore']+"/20)"); 
                                jQuery("#CbxGGETechEvaluationSecond").val(data.techInfo['GGE2ndAttemptScore']);
                        }
                        else if(data.techInfo['GGE2ndAttemptStatus'] == "Pending") {
                                jQuery("#GGETechEvaluationSecond").html(data.techInfo['GGE2ndAttemptStatus']); 
                                jQuery("#CbxGGETechEvaluationSecond").val(data.techInfo['GGE2ndAttemptStatus']);
                        }
                        if(data.techInfo['GGE1stAttemptStatus'] == "Fail" 
                                && (data.techInfo['GGE2ndAttemptStatus']== "Pending" || data.techInfo['GGE2ndAttemptStatus']== "Fail"))
                        {
                            //jQuery("#divGGETechEvaluationAllow").show();
                            jQuery("#divGGETechEvaluationAllow").attr("allow","1");
                            jQuery("#divGGETechEvaluationAllow").attr("techid",data.techInfo['TechID']);
                            jQuery("#cmdGGETechEvaluationAllow").unbind("click",me.cmdGGETechEvaluationAllow).bind("click",me.cmdGGETechEvaluationAllow);
                        }        
                    }
                    // issue_13085
                    else if(index == 'FSPlusOneDateReg') 
                    {
                        if(data.techInfo['FSPlusOneReg']==1)
                        {
                            element = $("#"+index);
                            element.val(data.techInfo[index]);
                        }
                        else
                        {
                            element.val('');
                        }
                    }
                    //end  issue_13085
                    //13346
                    else  if((index=='AllowText'))
                    {
                        element = $("#UnAllowText");
                        if((data.techInfo[index] == 0||data.techInfo[index] != true )&& element.is(':checkbox'))
                        {
                            element.attr('checked', true);
                            
                        }
                        else
                        {
                            element.attr('checked', false);
                            
                        }
                    }   //end 13346    
                       //13700
                        else if(index == "SilverDrugPassed")
                        {   
                        if(data.techInfo[index] == 1)
                        {    
                            $("#SilverDrugstatus").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                            $("#SilverDrugsdate").html($.format.date(data.techInfo['DatePassedSilverDrug'], 'MM/dd/yyyy'));                 
                        }
                        else
			{
                            $("#SilverDrugsdate").html(data.techInfo['DatePassedSilverDrug']);
                            $("#SilverDrugsdate").html($.format.date(data.techInfo['DatePassedSilverDrug'], 'MM/dd/yyyy')); 
			}
                        } 
                      //End 13700	
                    //018
                    else if(index=="HideBids")
                        {
                        if(data.techInfo[index]==1)
                            {
                            $("#HideBids").attr("checked", true);
                            var username = data.techInfo["HideBidsByUserName"];
                            var today = $.format.date(data.techInfo["HideBidsDate"], 'MM/dd/yyyy');
                            var todayhideval = $.format.date(data.techInfo["HideBidsDate"], 'yyyy-MM-dd');
                            $("#hidebidtechinfo").html(today + " &ndash; " + username); 
                            $("#hidebidtechdate").val(todayhideval);
                            $("#hidebidtechuser").val(username);
                            //$("#HideBidTechlabel").html("Hide Bids / Hide Techs"); //14023
                            }
                            }
                    //end 018
                    else{
                        element = $("#"+index);
                        if (element.is(':checkbox') && (data.techInfo[index] == true || data.techInfo[index] == 1))
                            element.attr('checked', true);
                        else if (element.is('textarea'))
                            element.html(data.techInfo[index])
                        else
                        {
                            if(index =='Password')
                            {
                            element.val(data.techInfo[index]);
                                $("#oldpass").val(data.techInfo[index]);
                                $("#PasswordLabel_").html(data.techInfo[index]);
                            }
                            else
                            {
                                element.val(data.techInfo[index]);
                            }
                        }
                        $("#"+index+"Label").html(data.techInfo[index]);
                    }
                   
                }
                if(data.techInfo.CellProvider != "")
                {
                    $("#CellProvider").val(data.techInfoCellProvider);
                    optionTxt = $("#CellProvider" + " option[value='" + data.techInfo.CellProvider + "']").text();
                    if (optionTxt != '')
                        $("#CellProvider" + "Label").html(optionTxt);
                    else
                        $("#CellProvider" + "Label").html(data.techInfo[index]);
                }
                
                if(data.techInfo.State != "")
                {
                    var bankState = "";
                    $("#State option").each(function(){
                        if(data.techInfo.State == $(this).val())
                        {
                            bankState = $(this).html();
                        }
                    });
	
                    $("#BankState option").each(function()
                    {
                        if(bankState == $(this).html())
                        {
                            bankState = $(this).val();
                        }
                    });
                }
                else
                {
                    var bankState = "";
                }

                if(data.techInfo.SecondaryPhone == "" || data.techInfo.SecondaryPhone == null)
                {
                    $("#SecondaryPhone").removeClass("required");
                }
				
                $("#headerTechInfo").html(data.techInfo.Firstname+" "+data.techInfo.Lastname+" - Tech ID "+me._techId);
                $('body').data("techName",data.techInfo.Firstname+" "+data.techInfo.Lastname);
                $('body').data("country",bankCntry);
                $('body').data("address1",data.techInfo.Address1);
                $('body').data("address2",data.techInfo.Address2);
                $('body').data("city", data.techInfo.City);
                $('body').data("state", bankState);			
                $('body').data("zip", data.techInfo.Zipcode);
                $('body').data("email",data.techInfo.PrimaryEmail);
                /*				$('body').data("password",'<?=$_SESSION['UserPassword'];?>');
				
				$("#username").val("<?=$_SESSION['UserName'];?>");
				$("#usernameLabel").html("<?=$_SESSION['UserName'];?>");
				
				$("#password").val("<?=$_SESSION['UserPassword'];?>");
				$("#passwordLabel").html("<?=$_SESSION['UserPassword'];?>");*/

                if(data.techInfo.State)
                {
                    $("#State").val(data.techInfo.State).attr('selected',true);
                }

                if(data.techInfo.Country)
                {
                    $("#Country").val(data.techInfo.Country).attr('selected',true);
                }
                else
                {
                    $("#Country").val("US").attr('selected',true);
                }
				
                $("#yesResume").hide();
                $("#noResume").show();
                $("#deleteBadgePhoto").hide();
                $("#badgeUpload").show();

                if(data.techInfo.techFiles != null)
                {
                    if(data.techInfo.techFiles.length >= 1)
                    {
                        $.each(data.techInfo.techFiles, function(key,value)
                        {
                            if(value.fileType == "Resume")
                            {
                                $('body').data("resume", value.filePath);
                                $("#yesResume").show();
                                $("#noResume").hide();
								//$("#resumeFile").html(value.displayName);
                                $("#resumeFile").html("<a href='/widgets/dashboard/do/file-tech-documents-download/filename/"+value.encodedFilename+"/"+value.filePath+"'>"+value.displayName+"</a>");
                            }
		
                            if(value.fileType == "Profile Pic")
                            {
                                if(value.approved == "0")
                                {
                                    //$("#badgePhotoImage").attr("src","/widgets/images/profileAnonPending.jpg" );
                                    jQuery("#divApprovePhoto").show();
                                    jQuery("#badgePhotoImagePending").show();
                                    $('body').data("profilePic", value.filePath);
                                }
                                else
                                {
                                    //$("#badgePhotoImage").attr("src","https://s3.amazonaws.com/tech-docs/profile-pics/"+value.filePath );
                                    $('body').data("profilePic", value.filePath);
                                    jQuery("#badgePhotoImagePending").hide();
                                    jQuery("#divApprovePhoto").hide();
                                }
                                jQuery("#divApprovePhoto").children('#imgApprovePhotoBadge').attr('techid',data.techInfo.TechID);
                                $("#badgePhotoImage").attr("src","https://s3.amazonaws.com/tech-docs/profile-pics/"+value.filePath );
                                $("#deleteBadgePhoto").show();
                                $("#badgeUpload").hide();
                                $("#saveBadgePhoto").hide();
                            }
							
                            if(value.fileType == "Vehicle Image")
                            {
                                $("#vehicleMisc").show();
                                $("#vehicleID").val(value.id);
                                $("#vehicleCurrentImage").val(value.filePath);
                                if(value.description != null && value.description != "")
                                {
                                    $("#vehicleDesc").val(value.description);
                                }
                            } 
                        });
                    }
                }

                if(data.techInfo.expertiseInfo != null)
                {
                    if(data.techInfo.expertiseInfo.length >= 1)
                    {
                        $.each(data.techInfo.expertiseInfo, function(key,value)
                        {
                            var starArr = [];
                            //								if (value.selfRating == 0 || !value.selfRating)
                            //									value.selfRating = 0;
                            for(i=0; i<6; i++)
                            {
                                if(i == 0)
                                {
                                    if(!value.selfRating)
                                    {
                                        var $tempInput = $("<span/>").html("None").css("margin-left", "6px").append($('<input type="radio"/>').attr('name',value.TechSelfRatingColumn).val("None"));
                                    }
                                    if(value.selfRating == 0)
                                    {
                                        var $tempInput = $("<span/>").html("None").css("margin-left", "6px").append($('<input type="radio"/>').attr('name',value.TechSelfRatingColumn).attr('checked','checked').val("None"));
                                    }
                                    else
                                    {
                                        var $tempInput = $("<span/>").html("None").css("margin-left", "6px").append($('<input type="radio"/>').attr('name',value.TechSelfRatingColumn).val("None"));
                                    }
                                }
                                else
                                {
                                    if(value.selfRating == i)
                                    {
                                        var $tempInput = $("<span/>").html(i).css("margin-left", "6px").append($('<input type="radio"/>').attr('name',value.TechSelfRatingColumn).attr('checked','checked').val(i));
                                    }
                                    else
                                    {
                                        var $tempInput = $("<span/>").html(i).css("margin-left", "6px").append($('<input type="radio"/>').attr('name',value.TechSelfRatingColumn).val(i));
                                    }
                                }
                                starArr.push($tempInput);
                            }
							
                            var newCatArr = ["28","29", "30", "31"];
                            var categoryHtml = "";
								
                            if($.inArray(value.Category_ID, newCatArr) > -1)
                            {
                                categoryHtml = value.Category+ "<img name='New' src='/images/new.gif' alt='New'>";
                            }
                            else
                            {
                                categoryHtml = value.Category;
                            }
                            //13735
                            var expertHtml = "";
                            var optSelected1 = "";
                            var optSelected2 = "";
                            var optName="";                            
                            optName ="FSExpert_" + value.FSExpertId;
                            if(value.FSExpert == 1)
                            {
                                optSelected1 = "selected='selected'";
                            }else
                            {
                                optSelected2 = "selected='selected'";                         
                            }                                                   
                            if(value.FSExpertEnable == 1)
                            {                                
                                expertHtml = "<select id='"+ optName +"'name='"+ optName +"'><option id='1' value='1'" + optSelected1 +">FS-Expert</option><option id='0' value='0'" + optSelected2 + ">No</option></select>";
                            }
                            else
                            {
                                expertHtml = "N/A";
                            }			   		
                            $('#techExpertiseTable').find("tbody")
                            .append($('<tr>')
                                .append($("<td>").html(categoryHtml))
                                .append($("<td>").text(value.total).attr("align","center"))
                                .append($("<td>").append.apply($("<td>"),$.isArray(starArr) ? starArr : [starArr]))                              
                                .append($("<td style='width: 105px;'>").html(expertHtml)));                            
                        });
                    }
                    $.getScript('/widgets/js/star-rating/jquery.rating.js');
                }
                 jQuery(".hoverTextnew").bt({
                        titleSelector: "attr('bt-xtitle')",
                        fill: 'white',
                        cssStyles: {color: 'black', width: '300px'},
                        animate:true
                     });
                 //end 13735
                if(data.techInfo.equipmentInfo != null)
                {
                    if(data.techInfo.equipmentInfo.length >= 1)
                    {
                        $.each(data.techInfo.equipmentInfo, function(key,value)
                        {
                            $("#"+value).attr("checked","checked");
                            if(value == "Ladder")
                            {
                                $("#ladderMisc").show();
                            }
                            if(value == "PunchTool")
                            {
                                $("#punchToolMisc").show();
                            }
                        });
                    }
                }

                if(data.techInfo.equipmentInfo2 != null)
                {
                    $.each(data.techInfo.equipmentInfo2, function(key,value)
                    {
                        $("#Tools").val(value.Tools);
                    });
                }

                if(data.techInfo.socialNetworks != null)
                {
                    if(data.techInfo.socialNetworks.length >= 1)
                    {
                        $.each(data.techInfo.socialNetworks, function(key,value)
                        {
                            $("input[name="+value.name+value.id+"]").val(value.URL);
                        });
                    }
                }

                if(data.techInfo.credentialInfo != null)
                {
                    if (data.techInfo.credentialInfo['bgCheckFullStatus'] == 1 && data.techInfo.credentialInfo['drugTestStatus'] == 1) 
                    {
                        today = new Date();
                        yearago = new Date(today.getFullYear() - 1, today.getMonth(), today.getDate());
                        bgDate = $.datepicker.parseDate("mm/dd/yy",data.techInfo.credentialInfo['bgCheckFullDate']);
                        dtDate = $.datepicker.parseDate("mm/dd/yy",data.techInfo.credentialInfo['drugTestDate']);
                        dellMRACompliantDate = bgDate > dtDate ? bgDate : dtDate;
                        if (bgDate >= yearago || dtDate >= yearago)
                        {
                            imgSrc = "/widgets/images/check_green.gif";
                            dellMRACompliantStatus = "Passed";
                        }
                        else
                        {
                            imgSrc = "/widgets/images/red_cross.gif";
                            dellMRACompliantStatus = "Lapsed";
                            dellMRACompliantDate = new Date(dellMRACompliantDate.getFullYear() + 1, dellMRACompliantDate.getMonth(), dellMRACompliantDate.getDate() + 1);
                        }
                        dellMRACompliantDate = dellMRACompliantDate.getMonth() + 1 + "/" + dellMRACompliantDate.getDate() + "/" + dellMRACompliantDate.getFullYear();
                        $("#DellMRACompliantStatus").html("<img src='" + imgSrc + "'/>" + dellMRACompliantStatus + " - " + dellMRACompliantDate);
                    }

                    dellMRACompliantBreakDown =  "Basic BC: " + (data.techInfo.credentialInfo['bgCheckStatus'] == 1 ? data.techInfo.credentialInfo['bgCheckDate'] : "") + "<br/>";
                    dellMRACompliantBreakDown += "Comp BC : " + (data.techInfo.credentialInfo['bgCheckFullStatus'] == 1 ? data.techInfo.credentialInfo['bgCheckFullDate'] : "") + "<br/>";
                    dellMRACompliantBreakDown += "DT      : " + (data.techInfo.credentialInfo['drugTestStatus'] == 1 ? data.techInfo.credentialInfo['drugTestDate'] : "");
                    $("#DellMRACompliantDate").html(dellMRACompliantBreakDown);
                    //13932
                    //056
                    imgSrc = "/widgets/images/check_green.gif";
                    if(data.techInfo.credentialInfo["NACIGovtStatus"] != 0 && data.techInfo.credentialInfo["NACIGovtStatus"] !== null)
                    {
                        if(data.techInfo.credentialInfo["NACIGovtStatus"] == 1)
                        {
                            $("#publicCredentials #NACIStatusLabel").html("<img src='" + imgSrc + "'/>");
                        }
                        else if(data.techInfo.credentialInfo["NACIGovtStatus"] == 2)
                        {
                            $("#publicCredentials #NACIStatusLabel").html("Unverified");
                        }
                            
                        $("#publicCredentials #NACIDate").html(data.techInfo.credentialInfo['NACIGovtDate']);							
                        $("#publicCredentials #NACIInput").val(data.techInfo.credentialInfo['NACIGovtDate']);
                        $("#publicCredentials #NACIVal").val(data.techInfo.credentialInfo['NACIGovtDate']);
                    }
                    else
                    {
                        $("#publicCredentials #NACIStatusLabel").html("");
                    }
                    $("#publicCredentials #NACIStaVal").val(data.techInfo.credentialInfo['NACIGovtStatus']);
                    $("#publicCredentials #NACICertStatus option").each(function() {
                        if ($(this).val() === data.techInfo.credentialInfo["NACIGovtStatus"])
                        {
                            $(this).attr("selected", "selected");
                        }
                    });
                    
                    if(data.techInfo.credentialInfo["interimGovtStatus"] != 0 && data.techInfo.credentialInfo["interimGovtStatus"] !== null)
                    {
                        if(data.techInfo.credentialInfo["interimGovtStatus"] == 1)
                        {
                            $("#publicCredentials #interimGovtStatusLabel").html("<img src='" + imgSrc + "'/>");
                        }   
                        else if(data.techInfo.credentialInfo["interimGovtStatus"] == 2)
                        {
                            $("#publicCredentials #interimGovtStatusLabel").html("Unverified");

                        }
                        $("#publicCredentials #interimGovtDate").html(data.techInfo.credentialInfo['interimGovtDate']);
                        $("#publicCredentials #interimGovtInput").val(data.techInfo.credentialInfo['interimGovtDate']);
                        $("#publicCredentials #interimGovtVal").val(data.techInfo.credentialInfo['interimGovtDate']);
                    }
                    else
                    {
                        $("#publicCredentials #interimGovtStatusLabel").html("");                          
                    }
                    $("#publicCredentials #interimGovtStaVal").val(data.techInfo.credentialInfo['interimGovtStatus']);
                    $("#publicCredentials #interimGovtCertStatus option").each(function() {
                        if ($(this).val() === data.techInfo.credentialInfo["interimGovtStatus"])
                        {
                            $(this).attr("selected", "selected");
                        }
                    });
                    
                    if(data.techInfo.credentialInfo["topGovtStatus"] != 0 && data.techInfo.credentialInfo["topGovtStatus"] !== null)
                    {
                        if(data.techInfo.credentialInfo["topGovtStatus"] == 1)
                        {
                            $("#publicCredentials #topGovtStatusLabel").html("<img src='" + imgSrc + "'/>");
                        }                      
                        else if(data.techInfo.credentialInfo["topGovtStatus"] == 2)
                        {
                            $("#publicCredentials #topGovtStatusLabel").html("Unverified");
                        }
                        $("#publicCredentials #topGovtDate").html(data.techInfo.credentialInfo['topGovtDate']);
                        $("#publicCredentials #topGovtInput").val(data.techInfo.credentialInfo['topGovtDate']);
                        $("#publicCredentials #topGovtVal").val(data.techInfo.credentialInfo['topGovtDate']);
                    }
                    else
                    {
                        $("#publicCredentials #topGovtStatusLabel").html("");                          
                    }
                    $("#publicCredentials #topGovtStaVal").val(data.techInfo.credentialInfo['topGovtStatus']);
                    $("#publicCredentials #topGovtCertStatus option").each(function() {
                        if ($(this).val() === data.techInfo.credentialInfo["topGovtStatus"])
                        {
                            $(this).attr("selected", "selected");
                        }
                    });
                    
                    if(data.techInfo.credentialInfo["fullGovtStatus"] != 0 && data.techInfo.credentialInfo["fullGovtStatus"] !== null)
                    {
                        if(data.techInfo.credentialInfo["fullGovtStatus"] == 1)
                        {
                            $("#publicCredentials #fullGovtStatusLabel").html("<img src='" + imgSrc + "'/>");
                        }
                        else if(data.techInfo.credentialInfo["fullGovtStatus"] == 2)
                        {
                            $("#publicCredentials #fullGovtStatusLabel").html("Unverified");
                        }
                        $("#publicCredentials #fullGovtDate").html(data.techInfo.credentialInfo['fullGovtDate']);							
                        $("#publicCredentials #fullGovtInput").val(data.techInfo.credentialInfo['fullGovtDate']);
                        $("#publicCredentials #fullGovtVal").val(data.techInfo.credentialInfo['fullGovtDate']);
                    }
                    else
                    {
                        $("#publicCredentials #fullGovtStatusLabel").html("");
                    }
                    $("#publicCredentials #fullGovtStaVal").val(data.techInfo.credentialInfo['fullGovtStatus']);
                    $("#publicCredentials #fullGovtCertStatus option").each(function() {
                        if ($(this).val() === data.techInfo.credentialInfo["fullGovtStatus"])
                        {
                            $(this).attr("selected", "selected");
                        }
                    });
                    
                    if(data.techInfo.credentialInfo["topSCIGovtStatus"] != 0 && data.techInfo.credentialInfo["topSCIGovtStatus"] !== null)
                    {
                        if(data.techInfo.credentialInfo["topSCIGovtStatus"] == 1)
                        {
                            $("#publicCredentials #topSCIGovtStatusLabel").html("<img src='" + imgSrc + "'/>");
                        }
                        else if(data.techInfo.credentialInfo["topSCIGovtStatus"] == 2)
                        {
                            $("#publicCredentials #topSCIGovtStatusLabel").html("Unverified");
                        }
                        $("#publicCredentials #topSCIGovtDate").html(data.techInfo.credentialInfo['topSCIGovtDate']);							
                        $("#publicCredentials #topSCIGovtInput").val(data.techInfo.credentialInfo['topSCIGovtDate']);
                        $("#publicCredentials #topSCIGovtVal").val(data.techInfo.credentialInfo['topSCIGovtDate']);
                    }
                    else
                    {
                        $("#publicCredentials #topSCIGovtStatusLabel").html("");
                    }
                    $("#publicCredentials #topSCIGovtStaVal").val(data.techInfo.credentialInfo['topSCIGovtStatus']);
                    $("#publicCredentials #topSCIGovtStatus option").each(function() {
                        if ($(this).val() === data.techInfo.credentialInfo["topSCIGovtStatus"])
                        {
                            $(this).attr("selected", "selected");
                        }
                    });
                    //end 056
                    //13932      
                    if(data.techInfo.TrapolloMedicalCertifiedStatus==1)
                    {
                        imgSrc = "/widgets/images/check_green.gif";
                        $("#TMCStatusDisplay").html("<img src='" + imgSrc + "'/>");
                        $("#TMCStatusChk").attr('checked', true);
                        $("#TMCDate").html(data.techInfo.TrapolloMedicalCertifiedDate);
                        $("#TMCDateTxt").val(data.techInfo.TrapolloMedicalCertifiedDate);
                    }
                    if(data.techInfo.VonagePlusStatus==1)
                    {
                        imgSrc = "/widgets/images/check_green.gif";
                        $("#VonagePlusStatusLabel").html("<img src='" + imgSrc + "'/>");
                        $("#VonagePlusStatusChk").attr('checked', true);
                        $("#VonagePlusDate").html(data.techInfo.VonagePlusDate);
                        $("#VonagePlusInput").val(data.techInfo.VonagePlusDate);
                    }
                    else
                    {
                        $("#VonagePlusStatusLabel").html("");
                    }  
                    if(data.techInfo.BlueRibbonTechnicianStatus == 1)
                    {
                        $("#blueRibbondate").html(data.techInfo.BlueRibbonTechnicianDate);
                        $("#blueRibbonStatus").append(
                                                $("<img />").attr("src","/widgets/images/check_green.gif")
                                            );
                    }
                    else
                    {
                        $("#blueRibbondate").html();
                        $("#blueRibbonStatus").html();
                    }
                    
                    //13624
                    if(data.techInfo.certificationsInfo.TBCCertStatus == 1)
                    {
                        $(".bootCamp").html("Boot Camp Certified");
                        $("#bootCampDate").html(data.techInfo.certificationsInfo['TBCDate']);
                        $("#bootCampStatus").html(
                            $("<img />").attr("src","/widgets/images/Boot_Tag.png")
                            .attr("width","18")
                        );
                    }  else if(data.techInfo.certificationsInfo.BootCampPlusCertStatus == 1) {
                        $(".bootCamp").html("Boot Camp Plus");
                        $("#bootCampDate").html(data.techInfo.certificationsInfo['BootCampPlusDate']);
                        $("#bootCampStatus").html(
                            $("<img />").attr("src","/widgets/images/Boot_Tag_PLUS.png")
                            .attr("width","24")
                        );                          
                    } else {
                        $("#bootCampdate").html();
                        $("#bootCampStatus").html();
                    }                    

                    if(data.techInfo.NCRBadgedTechnicianStatus==1)
                    {
                        imgSrc = "/widgets/images/check_green.gif";
                        $("#NCRBadgedTechnicianStatusLabel").html("<img src='" + imgSrc + "'/>");
                        $("#NCRBadgedTechnicianStatusChk").attr('checked', true);
                        $("#NCRBadgedTechnicianDate").html(data.techInfo.NCRBadgedTechnicianDate);
                        $("#NCRBadgedTechnicianInput").val(data.techInfo.NCRBadgedTechnicianDate);
                    }
                    else
                    {
                        $("#NCRBadgedTechnicianStatusLabel").html("");
                    }
                    if(data.techInfo.I9RequestedDate)
                        $("#USAuthorizationRequestDate").val(data.techInfo.I9RequestedDate);
                    if(data.techInfo.I9Date)
                        $("#I9DateAdministered").val(data.techInfo.I9Date);
                    if(data.techInfo.I9Status == 2)
                        $("#I9ResultsDownload").show();
                    //056
                    var obmitarr = ["NACIGovtStatus",
                                    "NACIGovtDate",
                                    "interimGovtStatus",
                                    "interimGovtDate",
                                    "topGovtStatus",
                                    "topGovtDate",
                                    "fullGovtStatus",
                                    "fullGovtDate",
                                    "topSCIGovtStatus",
                                    "topSCIGovtDate"];
                    $.each(data.techInfo.credentialInfo, function(key,value){
                        if($.inArray(key, obmitarr)<0)
                        {
                            if(key.search("Status") != -1){
                                var divID = key.replace("Status","");
                                if(divID == "flsId" || divID == "flsWhse")
                                {
                                    if(divID == "flsId")
                                    {
                                        $("#flsIdStatus").html(value);
                                    }
                                    else
                                    {
                                    $("#"+divID+" td[id*='Status']").html(value);
                                }
                                }
                                else if(value == 1)
                                {
                                    $("#"+divID+" td[id*='Status']").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                                }
                                else if(key == "bgCheckStatus" && value != 1)
                                {
                                    $("#bgCheckDate").parent().html("<a class='bgCheck' href='javascript:void(0);' rel='/techs/ajax/lightboxPages/backgroundChecks.php'><img width='100' height='65' src='/widgets/images/order-bg-check.png'></a>");
                                    $(".bgCheck").click(function()
                                    {
                                        $("<div></div>").fancybox(
                                        {
                                            'autoDimensions' : false,
                                            'width'	: 900,
                                            'height' : 550,
                                            'transitionIn': 'none',
                                            'transitionOut' : 'none',
                                            'type': 'iframe',
                                            'href': $(this).attr('rel')
                                        }
                                        ).trigger('click');
                                    });
                                }
                                else if(key == "w9Status" && value != 1)
                                {
                                    $("#w9Date").parent().html("<a class='uploadW9' href='javascript:void(0);' rel='/techs/ajax/lightboxPages/w9.php'><img width='85' height='45' src='/widgets/images/upload-w9.png'></a>");
                                    $(".uploadW9").click(function()
                                    {
                                        $("<div></div>").fancybox(
                                        {
                                            'autoDimensions' : false,
                                            'width'	: 900,
                                            'height' : 550,
                                            'transitionIn': 'none',
                                            'transitionOut' : 'none',
                                            'type': 'iframe',
                                            'href': $(this).attr('rel')
                                        }
                                        ).trigger('click');
                                    });
                                }
                            }
                        }
                        if (key == "fsMobileDate") {
                            if (value != null)
                                $("#fsMobileStatus").html ("<img src='/widgets/images/fs_mobile_36x14.png' />");
                            else
                                $("#fsMobileStatus").empty ();
                        }
                        if($.inArray(key, obmitarr)<0)
                        {
                            if(key.search("Date") != -1){
                                var divID = key.replace("Date","");
                                $("#"+divID+" div[id*="+key+"]").html(value);
                            }
                        }
                    });
                    //end 056
                    if(data.techInfo.FLSTestDatePass != null)
                    {
                        $("#flsIdDate").html(data.techInfo.FLSTestDatePass);
                    }
                }

                if(data.techInfo.trainingInfo != null)
                {
                    $.each(data.techInfo.trainingInfo, function(key,value)
                    {
                        if(key.search("CertNum") != -1)
                        {
                            var divID = key.replace("Num","");
							
                            if(value != null && value != "" && value != "null")
                            {
                                $("#"+divID+" span[id*=CertNumDisplay]").html(value);
                            }
                            else
                            {
                            //$("#"+divID+" span[id*=CertNumDisplay]").append($("<img />").attr("src","/widgets/images/check_red.gif"));
                            }
                        }
						
                        if(key == "essintialCertDate")
                        {
                            $("#essintialCertDateDisplay").html(value);
                        }

                        //                        "hallmarkCertStatus":"1",
                        //                        "hallmarkCertNum":"",
                        //                        "hallmarkCertDate":"02\/07\/2012",
                        if(key == "hallmarkCertStatus")
                        {
                            if(value>0)
                            {
                                $("#hallmarkCertStatus").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                            }
                        }
                        
                        if(key == "hallmarkCertDate")
                        {
                            $("#hallmarkCertDateDisplay").html(value);
                        }
                        //                        "starbucksCertStatus":"1",
                        //                        "starbucksCertDate":null,
                        if(key == "starbucksCertStatus")
                        {
                            if(value>0)
                            {
                                $("#starbucksCertStatus").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                            }
                        }
                        
                        if(key == "starbucksCertDate")
                        {
                            $("#starbucksCertDateDisplay").html(value);
                        }
                        //                        "mauricesCertStatus":"1",
                        //                        "mauricesCertDate":null,                        
                        if(key == "mauricesCertStatus")
                        {
                            if(value>0)
                            {
                                $("#mauricesCertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                            }
                        }
                        
                        if(key == "mauricesCertDate")
                        {
                            $("#mauricesCertDateDisplay").html(value);
                        }
                        //                        "hpCertNum":"",
                        //                        "hpCertStatus":"0"
                        //                        if(key == "hpCertStatus")
                        //                        {
                        //                            if(value>0)
                        //                            {
                        //                                $("#hpCertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                        //                            }
                        //                        }
                        
                        //                        if(key == "")
                        //                        {
                        //                            $("#hpCertDateDisplay").html(value);
                        //                        }
                        //                        "electroMechCertStatus":"1",
                        //                        "electroMechCertDate":"01\/05\/2012",
                        if(key == "electroMechCertStatus")
                        {
                            if(value>0)
                            {
                                $("#electroMechCertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                            }
                        }
                        
                        if(key == "electroMechCertDate")
                        {
                            $("#electroMechCertDateDisplay").html(value);
                        }
                    });
                }

                if(data.techInfo.certificationsInfo != null)
                {
                	$.each(data.techInfo.certificationsInfo, function(key,value)
                    {
                    	if(key.search("CertNum") > 0)
                        {
                            var divID = key.replace("Num","");
                            if(value != null && value != "")
                            {
                                $("#"+divID+" span[id*=CertNumDisplay]").append(value);
                            }
                            else
                            {
                            //$("#"+divID+" span[id*=CertNumDisplay]").append($("<img />").attr("src","/widgets/images/check_red.gif"));
                            }
                        }
                        //13367
                        if(key.search("CertNum") > 0){
                            var divID = key.replace("CertNum","");
                            $("#"+divID+" div[id*=NumLabel]").html(value);
                        }
                        if(key.search("Date") > 0){
                            var divID = key.replace("Date", "");
                            //056
                            if (divID == 'NACI') divID = 'adminInternalInfo #NACI';
                            //end 056
                            $("#"+divID+"Cert span[id*=CertDateDisplay]").html(value);
                            $("#"+divID+"Date").html(value);
                            $("#"+divID+"Input").val(value);
                        }

                        if((key == 'aPlusStatus' || key.search("CertStatus") > 0) && value == "1")
                        {
                            var divID = key.replace("CertStatus","");
                            if (key == 'aPlusStatus') divID = 'aPlus';
                            //056
                            if (divID == 'NACI') divID = 'adminInternalInfo #NACI';
                            //end 056
                            var checkBoxElement = $("#"+(divID == 'aPlus' ? 'APlus' : divID));
                            if (checkBoxElement.attr('type') == 'checkbox') 
                            {
                                $("#"+divID+"CertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                                checkBoxElement.attr("checked",true);
                            }
                  
                            $("#"+divID+"StatusLabel").html($("<img />").attr("src","/widgets/images/check_green.gif"));
                            $("#"+divID+"CertNumDisplay").html($("<img />").attr("src","/widgets/images/check_green.gif"));
                            
                            $("#"+divID+"CertStatus").attr("checked",true);
                            $("#"+divID+"StatusChk").attr("checked",true);
                            $("#"+divID+"CertStatusEx1").attr("checked",true);

                        }
                        else if(key == 'mcseStatus'  && value == "1")
                        {
                            var divID = key.replace("Status","");
                            $("#"+divID+"CertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));                            
                        } 
                        else if(key == 'ccnaStatus'  && value == "1")
                        {
                            var divID = key.replace("Status","");
                            $("#"+divID+"CertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));                            
                        }  
                        else if(key == 'dellDcseStatus'  && value == "1")
                        {
                            var divID = key.replace("Status","");
                            $("#"+divID+"CertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));                            
                        }  
                        else if(key == 'bicsiStatus'  && value == "1")
                        {
                            var divID = key.replace("Status","");
                            $("#"+divID+"CertNumDisplay").append($("<img />").attr("src","/widgets/images/check_green.gif"));                            
                        } 
                        else if(key == 'compTiaStatus'  && value == "1")
                        {
                            var divID = key.replace("Status","");
                            $("#"+divID+"CertNumDisplay").prepend($("<img />").attr("src","/widgets/images/check_green.gif"));                            
                        } 
                        if((key=="CompuComTechnicianCertStatus" || key=="CompuComAnalystCertStatus" || key=="CompuComSpecialistCertStatus") && value==1) {
                            var CompuComText = "&nbsp;";
                            var CompuComValue = "";
                            if(key == "CompuComTechnicianCertStatus") { CompuComText += "Technician"; CompuComValue = 33; }
                            else if(key == "CompuComAnalystCertStatus") { CompuComText += "Analyst"; CompuComValue = 34; }
                            else if(key == "CompuComSpecialistCertStatus") { CompuComText += "Specialist"; CompuComValue = 35; }
                            $("#CompuComCertifiedStatusLabel").append($("<img />").attr("src","/widgets/images/check_green.gif")).append( CompuComText);
                            jQuery("#CompuComCertifiedStatusInput").val(CompuComValue);
                        } if((key=="CompuComTechnicianDate" || key=="CompuComAnalystDate" || key=="CompuComSpecialistDate")) {
                            jQuery("#CompuComCertifiedDate").html(value);
                        }

//console.log (key, value);
                        if (key == "TechForceICAgreementDate") {
                        	$("#TechForceCredentialDate").text (value);
                        }

                        if (key == "TechForceICAgreementCertStatus" && value == 1) {
	                        $("#TechForceCredentialStatusLabel").append($("<img />").attr("src","/widgets/images/check_green.gif"));
                        }
                        
                        if((key == "PurpleTechnicianCertifiedCertStatus" || key == "PurpleTechnicianUnverifiedCertStatus") && value == 1){
                            $("#PurpleTechnicianStatusLabel")
                                .append($("<img />").attr("src","/widgets/images/check_green.gif"));
                            var selected = 0;  
                            var textSkilllangue = "";
                            if(key == "PurpleTechnicianCertifiedCertStatus"){
                                selected = 40;
                                textSkilllangue = " Certified";
                            }else{
                                selected = 39;
                                textSkilllangue = " Unverified";
                            } 
                            $("#PurpleTechnicianStatusLabel").append(textSkilllangue);
                            jQuery("#PurpleTechnicianStatusInput").val(selected);
                            jQuery("#PurpleTechniciantitle").html(textSkilllangue + ' Purple Technician');
                        }
                        
                        if (key == "PurpleTechnicianCertifiedDate" || key == "PurpleTechnicianUnverifiedDate") {
                                $("#PurpleTechnicianDate").text(value);
                        }
                    });
                }
                
                $(".showOnEdit").hide();
                if (data.W9Info['DateChanged'])
                {
                    $("#W9LastSubmitted").html('Last W-9 submitted ' + $.format.date(data.W9Info['DateChanged'], 'MM/dd/yyyy'));
                }
                
                if (data.W9Info['PreviousVersion'])
                    $("#PreviousVersion").show();
                else
                    $("#PreviousVersion").hide();
                
                me.saveDefaults();
                $("#FLSCSP_Rec").attr("checked", data.techInfo.FLSCSP_Rec == '1');
                $(".attachCal").datepicker();
                me.hideLoader();
            //				me.openBasicReg();				
            }
        });
    }
}

//018
FSTechProfile.prototype.loadhidetechinfo = function(el) {
    if($(el).is(":checked"))
    {
        var username = $("#hidebidtechuser").val();
        var today = $.format.date(Date(), 'MM/dd/yyyy');
        var todayhideval = $.format.date(Date(), 'yyyy-MM-dd');
        $("#hidebidtechinfo").html(today + " &ndash; " + username);
        $("#hidebidtechdate").val(todayhideval);
        //$("#HideBidTechlabel").html("Hide Bids / Hide Tech");//14023 remove
    }
    else
    {
        $("#hidebidtechinfo").html("");
        $("#hidebidtechdate").val("");
        //$("#HideBidTechlabel").html("Hide Bids");//14023
    }
                        
}
//end 018

FSTechProfile.prototype.saveDefaults = function() {
    $('#Password').data('default',$('#Password').val());
    $("#Bg_Test_Pass_Lite").data('default', $("#Bg_Test_Pass_Lite").attr('checked'));
    $("#Bg_Test_Pass_Full").data('default', $("#Bg_Test_Pass_Full").attr('checked'));
    $("#FLSCSP_Rec").data('default', $("#FLSCSP_Rec").attr('checked'));
}

FSTechProfile.prototype.openBasicReg = function() {
    $("#basicRegistration").trigger('click');
}

FSTechProfile.prototype.showLoader = function(){
    if(!this.loaderVisible){
        $("div#loader").fadeIn("fast");
        this.loaderVisible = true;
    }
}

FSTechProfile.prototype.hideLoader = function(){
    if(this.loaderVisible){
        var loader = $("div#loader");
        loader.stop();
        loader.fadeOut("fast");
        this.loaderVisible = false;
    }
}

FSTechProfile.prototype.showPreloader = function() {
    this.showLoader();
/*    $('body').css('cursor', 'wait');
    var img  = '<table width="100%" class="gradBox_table_inner">';
        img += '<tr class="table_head"><th>&#160;<br />&#160;</th></tr>';
        img += '<tr><td>&#160<br />&#160;</td></tr>';
        img += '<tr>';
        img += "<td><img style='margin:0 40%;' alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></td>";
        img += '</tr>';
        img += '<tr"><td>&#160<br />&#160;</td></tr>';
        img += '<tr class="tech_paginator"><td class="tCenter">&#160;</td></tr>';
        img += '</table>';
    FSWidget.fillContainer(img, this);*/
	
}

FSTechProfile.prototype.onChangeCountry = function () {
    var country = $("#Country").val();
    if(country != "US" && country != "CA") 
    {
        jQuery("#PrimaryPhone").removeClass("invalid");
        jQuery("#SecondaryPhone").removeClass("invalid");
        jQuery(".status").each(function() {
            if(jQuery(this).children('label').attr("for") == "PrimaryPhone"
                || jQuery(this).children('label').attr("for") == "SecondaryPhone" )
                {
                jQuery(this).children('label').remove();
            }
        });
        jQuery(".reqPhone").each(function() {
            jQuery(this).css("display","none");
        });
    }
    else
    {
        jQuery(".reqPhone").each(function() {
            jQuery(this).css("display","");
        });
    }  
    //041
    var Requirecountry = {"US":"US","CA":"CA","MX":"MX"};
    
    if(Requirecountry.hasOwnProperty($("#Country option:selected").val()))
    {
        $("#State").removeClass("required"); 
        $("#State").addClass("required");
        $("#State").removeAttr("disabled");
        $("#tdstatelabel").html("<span id='reqStar'>*</span>State/Province:");
    }
    else
    {
        $("#State").removeClass("required"); 
        $("#State").removeClass("invalid");
        $("#State").attr("disabled","disabled");
        $("#tdstatelabel").html("<span id='reqStar'>&nbsp;</span>State/Province:");
        
    }
    //end 041
    $.ajax({
        url         : '/widgets/dashboard/js/state-info/',
        dataType    : 'json',
        data        : {
            country: $("#Country").val()
        },
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            if (data) {
                //AUTOFILL
                var option = '';
                option += '<option selected="selected" value="">Select State</option>';
                for(index in data) {
                    option += '<option value="' + index + '">' + data[index] + '</option>';
                }
                $("#State").html(option);
                state = this.filters().state();
                if (state) $("#State").val(state);
            }
        }
    });	
    $.ajax({
        type: "POST",
        url: "/widgets/dashboard/do/country-require-zip",
        dataType    : 'json',
        cache       : false,
        data: 'countrycode='+jQuery("#Country").children('option:selected').val(),
        success: function (data) {
            var requireZip = data.success;
            if(requireZip)
            {
                jQuery(".reqZip").each(function() {
                    jQuery(this).css("display","");
                });
            }   
            else
            {
                jQuery("#Zipcode").removeClass("invalid");
                jQuery(".reqZip").each(function() {
                    jQuery(this).css("display","none");
                });
            }   
        }
    });  
}

FSTechProfile.prototype.editBasicRegistration = function(){
    $("#techRegistration div[id*=Label], #techRegistration div[class='editSection']").each(function(){
        $(this).hide();
    });

    $("#techRegistration div[id*=Input], #techRegistration div[class='cancelSaveSection']").each(function(){
        $(this).show();
    });
	
    $(".ReadOnlyField").show();
    $(".showOnEdit").show();
    if($("#oldpass").val() != $("#Password").val())
    {
        $(".showOnEditadmin").show();
    }
    
    $("#badgeUpload").show();
}

FSTechProfile.prototype.saveBasicRegistration = function(){
    if(jQuery("#Password").val() != jQuery("#oldpass").val())
    {
        var err = _global.validPasword(
                    jQuery("#UserNameLabel").html(),
                    jQuery("#Password").val(),
                    jQuery("#passwordconfirm").val(),
                    jQuery("#Firstname").val(),
                    jQuery("#Lastname").val());
            
        if(err['error']==1)
            {
                $('#trerr').css("display", "");
                $('#errmsg').html(err['msg'][0]);
                if(err['id'][0]==2)
                {
                    $('#passwordconfirmtext').css('border','2px solid red');
                    $('#Password').css('border','1px solid black');
                }
                else
                {
                    $('#Password').css('border','2px solid red');
                    $('#passwordconfirmtext').css('border','1px solid black');
                }
                return false;
            }
            else
            {
                $('#trerr').css("display", "none");
                $('#Password').css('border','1px solid black');
                $('#passwordconfirmtext').css('border','1px solid black');
        }
    }
    $.ajax({
        type: "POST",
        url: "/widgets/dashboard/do/country-require-zip",
        dataType    : 'json',
        cache       : false,
        data: 'countrycode='+jQuery("#Country").children('option:selected').val(),
        success: function (data) {
            if(!data.success)
            {
                jQuery("#Zipcode").removeClass("required"); 
                jQuery("#Zipcode").removeClass("invalid"); 
            }
            else
            {
                jQuery("#Zipcode").addClass("required"); 
            }
            if($('body').data('email') == $("#PrimaryEmail").val()){
                $("#PrimaryEmail").removeClass("required");
                $("#confPrimaryEmail").removeClass("required");
            }

            if($('body').data('password') == $("#password").val()){
                $("#password").removeClass("required");
                $("#passwordconfirm").removeClass("required");
            }
            var country = $("#Country").val();
	
            if(country != "US" && country != "CA") 
            {
                $("#PrimaryPhone").removeClass("required");
                $("#SecondaryPhone").removeClass("required");
            }
            else
            {
                $("#PrimaryPhone").addClass("required");
                $("#SecondaryPhone").addClass("required");
            }    
            //041
            var Requirecountry = {"US":"US","CA":"CA","MX":"MX"};

            if(Requirecountry.hasOwnProperty($("#Country option:selected").val()))
            {
                $("#State").removeClass("required"); 
                $("#State").addClass("required");
                $("#State").removeAttr("disabled");
                $("#tdstatelabel").html("<span id='reqStar'>*</span>State/Province:");
            }
            else
            {
                $("#State").removeClass("required"); 
                $("#State").removeClass("invalid");
                $("#State").attr("disabled","disabled");
                $("#tdstatelabel").html("<span id='reqStar'>&nbsp;</span>State/Province:");
            }
            //end 041
                    
            if (this._create && this._type == 'admin')
                this.saveAdminInternalInfo();
            else
                $("#techRegistration").submit();
        }
    });
}

FSTechProfile.prototype.cancelBasicRegistration = function(){
    if (this._create) {
        this.cancelUpdate();
        return;
    }
    $("#techRegistration div[id*=Label], #techRegistration div[class='editSection']").each(function(){
        $(this).show();
    });
    $("#techRegistration div[id*=Input], #techRegistration div[class='cancelSaveSection'], #techRegistration div[class='status']").each(function(){
        $(this).hide();
    });
    $(".showOnEdit").hide();
    $("#badgeUpload").hide();
    $(".showOnEditadmin").hide();
    techRegValidator.hideErrors();
    $(".error").hide();
}

FSTechProfile.prototype.saveAdminInternalInfo = function() {
    me = this;
    var form = "#adminInternalInfo";

    var submitData = $(form).serialize();
	
    if (this._create && this._type == 'admin'){
        if (!$("#techRegistration").valid()) return false;
        submitData += $("#techRegistration").serialize();
    }
		
    if ($("#Bg_Test_Pass_Lite").data('default') != $("#Bg_Test_Pass_Lite").val())
        submitData += "&BG_Test_ResultsBy_Lite=1";
    if ($("#Bg_Test_Pass_Full").data('default') != $("#Bg_Test_Pass_Full").val())
        submitData += "&BG_Test_ResultsBy_Full=1";
    if ($("#FLSCSP_Rec").data('default') != $("#FLSCSP_Rec").attr('checked'))
        submitData += "&FLSCSP_RecBy=1";
    var formCheckbox = $("#adminInternalInfo input[type='checkbox']");
		
    formCheckbox.each(function(index) {
        checked = $(this).attr('checked') ? "1" : "0";
        submitData += "&" + $(this).attr('id') + "=" + checked;
    });

    this.showLoader();
	
    if (this._create && this._type == 'admin')
    {
        this.submitUpdateTechInfo(null, submitData);
    }
    else {
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/do/save-admin-internal-info/",
            dataType    : 'json',
            cache       : false,
            data: submitData,
            success: function (data) {
                me.saveDefaults();					
                var messageHtml = "<h2>Update Complete</h2>";
                messageHtml += "<p>Your Tech Profile has been successfully updated.</p>";
                messageHtml += "<b>Thank you!</b>";
                me.hideLoader();
                applyFancybox(messageHtml);
                window.location.reload();
            }
        });
    }
}

FSTechProfile.prototype.cancelPaymentInfo = function() {
    }

FSTechProfile.prototype.cancelUpdate = function() {
    history.back();
}

FSTechProfile.prototype.submitUpdateTechInfo = function(form, fData){
    me = this;
    if($("#badgePhotoUpload").val() && $("#badgePhotoUpload").val() != ""){
			
        $.ajaxFileUpload({
            url             : "/widgets/dashboard/do/save-business-profile/?techID="+$("#basicRegTechID").val(),
            secureuri       : false,
            fileElementId   : 'badgePhotoUpload',
            dataType        : 'json',
            cache           : false,
            success         : function (data, status, xhr) {
                var submitData = fData == undefined ? $(form).serialize() : fData;
                $("#badgeUpload").hide();
                $("#saveBadgePhoto").hide();
                $("#deleteBadgePhoto").show();
                $("#badgePhotoImage").attr("src","https://s3.amazonaws.com/tech-docs/profile-pics/"+data.uploadResult.fileName);					
                $("#badgePhotoUpload").val("");
				
                $.ajax({
                    type: "POST",
                    url: "/widgets/dashboard/do/save-business-profile/",
                    dataType    : 'json',
                    cache       : false,
                    data: submitData,
                    success: function (data) {
                        me.saveDefaults();
                        var messageHtml = "<h2>Update Complete</h2>";
                        messageHtml += "<p>Your Business Profile has been successfully updated.</p>";
                        messageHtml += "<b>Thank you!</b>";
		
                        hideLoader();
						
                        $("<div></div>").fancybox({
                            'width': 350,
                            'height' : 125,
                            'content' : messageHtml,
                            'autoDimensions' : false,
                            'centerOnScroll' : true
                        }).trigger('click');
                    }
                });
            },
            error:function(data, status, xhr){
                var messageHtml = "<h2>Update Error</h2>";
                messageHtml += "<p>We encounted an error when attempting to update your business profile.</p>";
                messageHtml += "<p>Please check the data you entered and try again.</p>";
                messageHtml += "<b>Thank you!</b>";
				
                hideLoader();
                $("#badgeUpload").show();
                $("#deleteBadgePhoto").hide();
                $("#saveBadgePhoto").show();
                $("<div></div>").fancybox({
                    'width': 350,
                    'height' : 125,
                    'content' : messageHtml,
                    'autoDimensions' : false,
                    'centerOnScroll' : true
                }).trigger('click');
            }
        });
    }else{
        myUrl = this._create ? 'create-tech' : 'update-tech-info';
        var submitData = fData == undefined ? $(form).serialize() : fData;
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/do/" + myUrl + "/",
            dataType    : 'json',
            cache       : false,
            data: submitData,
            success: function (data) {
					
                if (me._create) {
                    window.location.href = 'wosViewTechProfile.php?TechID=' + data.id;
                }

                me.saveDefaults();
                var messageHtml = "<h2>Update Complete</h2>";
                messageHtml += "<p>Your Tech Profile has been successfully updated.</p>";
                messageHtml += "<b>Thank you!</b>";
						
                hideLoader();
							
                $("<div></div>").fancybox({
                    'width': 350,
                    'height' : 125,
                    'content' : messageHtml,
                    'autoDimensions' : false,
                    'centerOnScroll' : true
                }).trigger('click');
            }
        });
    }
}
FSTechProfile.prototype.cmdGGETechEvaluationAllow = function() {
    $.ajax({
        type: "POST",
        url: "/widgets/dashboard/do/gge-tech-evaluation-allow/",
        dataType    : 'json',
        cache       : false,
        data: "techID="+jQuery(this).parent().attr("techid"),
        success: function (data) {	
            jQuery("#cmdGGETechEvaluationAllow").hide();
            jQuery("#divGGETechEvaluationAllow").attr("allow",'0');
            jQuery("#CbxGGETechEvaluationSecond").val("");
            var messageHtml = "<h2>Update Complete</h2>";
            messageHtml += "<p>Allow another attempt has been successfully updated.</p>";
            messageHtml += "<b>Thank you!</b>";
            FSTechProfile.prototype.hideLoader();
            applyFancybox(messageHtml);
        }
    });
}
