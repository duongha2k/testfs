function FSPopupRoll(params) {
    FSPopupAbstract.call(this, params);

    this.waitImage = new Image();
    this.waitImage.src = "/widgets/images/wait.gif";
}

FSPopupRoll.prototype = new FSPopupAbstract;
FSPopupRoll.prototype.constructor = FSPopupRoll;

FSPopupRoll.NAME = 'FSPopupRoll';
FSPopupRoll.VERSION = '0.1';
FSPopupRoll.DESCRIPTION = 'Class FSPopupRoll';

/**
 *  show
 *
 *  Function display roll popup
 *
 *  @param Node element Link when mouse was clicked
 *  @param Event event Event object
 *  @param object params Optional params for widget
 *      params.width        - roll width, int
 *      params.height       - roll height, int
 *      params.title        - roll title text, html
 *      params.body         - roll body text, html
 *      params.delay        - roll displaying delay, seconds
 *      params.preHandler   - Handler function, it will be called before any the Roll initialization
 *      params.handler      - Handler function, it will be called after all the Roll initializations, but before display
 *      params.postHandler  - Handler function, it will be called after the Roll displayed
 *      params.context      - Object, if set all Handlers will be call in context of this object
 */
FSPopupRoll.prototype.show = function (element, event, params) {

    params = params || {};

    if(typeof(params.obj) != "undefined")
    {
        element = $("#"+params.obj);
    }
    /** - DELAY - **/
    if ( typeof params.delay === 'number' && params.delay > 0 || this.delay() > 0 ) {
        var self = this;
        $(element).bind('mouseleave', function () {
            self.isUserNoWaitDelay();
        });
        
        var delay = this.delay(params.delay);
        this.delay(0);
        params.delay = 0;
        this._delay_id = setTimeout(function () {
            self.show(element, event, params)
        }, delay * 1000);
        
        
        return;
    } else {
        $(element).unbind('mouseleave');
    }
    /** - DELAY - **/
    $('body').css('cursor', 'wait');
    if ( typeof params.title === 'string' ) { // set container title
        this.header(params.title);
    }

    //  Set wait image indicator and popup width to image's width
    this.body("<img src='"+this.waitImage.src+"' alt='loading...' width='"+this.waitImage.width+"px' />");
    this.width(this.waitImage.width);
    this.height('');

    if ( typeof params.width === 'number' )  {
        params.width = Math.ceil(params.width);
    } else {
        params.width = this.params.width;
    }
    if ( typeof params.height === 'number' ) {
        params.height = Math.ceil(params.height);
    } else {
        params.height = '';
    }
    if ( typeof params.left === 'number' ) {
        params.left = Math.ceil(params.left);
    }
    if ( typeof params.top === 'number' ) {
        params.top = Math.ceil(params.top);
    }

    params.eTop             = Math.ceil($(element).offset().top);
    params.eLeft            = Math.ceil($(element).offset().left);
    params.eHeight          = Math.ceil($(element).height());
    params.eWidth           = Math.ceil($(element).width());
    var html                = document.documentElement;
    params.viewportHeight   = window.innerHeight || ( html && html.clientHeight ) || document.body.clientHeight;
    params.viewportWidth    = window.innerWidth  || ( html && html.clientWidth )  || document.body.clientWidth;
    params.vScroll          = $(html).scrollTop()  || $(document.body).scrollTop();
    params.hScroll          = $(html).scrollLeft() || $(document.body).scrollLeft();

    $(this.header()).css('width', this.width() - 20); //  20 pixels for roll close button

    /* pop-up top position */
    var _top, _left;
    if ( params.top === undefined ) {
        if ( params.eTop + this.height() > params.vScroll + params.viewportHeight ) {
            if ( this.height() > params.viewportHeight ) {
                _top = params.vScroll + 5;
            } else {
                _top = params.vScroll + params.viewportHeight - this.height() - (( params.hScroll ) ? 20 : 30);
                if ( _top < 0 ) _top = 0;
            }
        } else {
            _top = params.eTop;
        }
    } else {
        _top = params.top;
    }
    /* pop-up left position */
    if ( params.left === undefined ) {
        if ( params.eLeft + this.width() > params.hScroll + params.viewportWidth ) {
            _left = params.hScroll + params.viewportWidth - this.width() - (( params.vScroll ) ? 40 : 50);
            if ( _left < 0 ) _left = 0;
        } else {
            _left = params.eLeft;
        }
    } else {
        _left = params.left;
    }
    this.top(_top);
    this.left(_left);
    /****/

    this.autohide(params.autohide);
    this.zindex = FSPopupRoll.prototype.zindex;
    if ( params.zindex != undefined && params.zindex != null && typeof params.zindex === 'number' )
    {
        this.zindex = params.zindex;
    }else{
        FSPopupRoll.prototype.zindex++;
    }    
    //  Display popup
    $(this.container()).css('z-index', ++this.zindex);
    $(this.container()).css('display', '');

    /**
     *  set autohide handler
     */
    if ( this.autohide() ) {
        $(this.container()).bind('mouseenter mouseleave', {
            roll:this
        }, this.hideOnMouseOver);
        
    }


    //  We can call any preHandler function before set popup's body or title
    //  preHandler is called with 2 params:
    //      1 - current object
    //      2 - object params, which set as param into show function
    if ( params.preHandler !== undefined && typeof params.preHandler === 'function' ) { // call preHeandler
        if ( params.context !== undefined && typeof params.context === 'object' )
            params.preHandler.call(params.context, this, params);
        else
            params.preHandler(this, params);
    }
    $('body').css('cursor', '');
}
/**
 *  showNotAjax
 *
 *  Function display roll popup
 *
 *  @param Node element Link when mouse was clicked
 *  @param Event event Event object
 *  @param object params Optional params for widget
 *      params.width        - roll width, int
 *      params.height       - roll height, int
 *      params.title        - roll title text, html
 *      params.body         - roll body text, html
 *      params.preHandler   - Handler function, it will be called before any the Roll initialization
 *      params.handler      - Handler function, it will be called after all the Roll initializations, but before display
 *      params.postHandler  - Handler function, it will be called after the Roll displayed
 *      params.context      - Object, if set all Handlers will be call in context of this object
 */
FSPopupRoll.prototype.showNotAjax = function (element, event, params) {
    $('body').css('cursor', 'wait');

    params = params || {};

    //  We can call any preHandler function before set popup's body or title
    //  preHandler is called with 2 params:
    //      1 - current object
    //      2 - object params, which set as param into show function
    if ( params.preHandler !== undefined && typeof params.preHandler === 'function' ) { // call preHeandler
        if ( params.context !== undefined && typeof params.context === 'object' )
            params.preHandler.call(params.context, this, params);
        else
            params.preHandler(this, params);
    }

    var width;
    var height;
    if ( typeof params.width === 'number' )  {
        width = Math.ceil(params.width);
    }
    else                                     {
        width = this.params.width;
    }
    if ( typeof params.height === 'number' ) {
        height = Math.ceil(params.height);
    }
    else                                     {
        height = '';
    }


    


    this.width(params.width);
    this.height(params.height);

    var html                = document.documentElement;
    params.viewportHeight   = window.innerHeight || ( html && html.clientHeight ) || document.body.clientHeight;
    params.viewportWidth    = window.innerWidth  || ( html && html.clientWidth )  || document.body.clientWidth;
    params.vScroll          = $(html).scrollTop()  || $(document.body).scrollTop();
    params.hScroll          = $(html).scrollLeft() || $(document.body).scrollLeft();
    if (!params.position) params.position = 'relative';
    //handle position
    if (params.position == 'middle') {
        params.eTop = (params.viewportHeight - this.height())/2;
        params.eLeft = (params.viewportWidth - this.width())/2;
        $(this.container()).css('position', 'fixed');
    } else if (params.position == 'top') {
        params.eTop = params.wTop;
        params.eLeft = (params.viewportWidth - this.width())/2;
        $(this.container()).css('position', 'fixed');
    }else{
      var eleOffset = $(element).offset();
      var topOffset = eleOffset.top;
      var leftOffset = eleOffset.left;
	params.eTop = Math.ceil(topOffset);
       params.eLeft = Math.ceil(leftOffset);
/*
        params.eTop             = Math.ceil($(element).offset().top);
        params.eLeft            = Math.ceil($(element).offset().left);
*/  
      $(this.container()).css('position', 'absolute');
    }
    if(typeof(params.paddingleft) != 'undefined')
    {
        params.eLeft = params.eLeft + params.paddingleft;
    } 
    if(typeof(params.paddingTop) != 'undefined')
    {
        params.eTop = params.eTop + params.paddingTop;
    } 
    $(this.header()).css('width', this.width() - 20); //  20 pixels for roll close button

    /* pop-up top position */
    if ( params.eTop + this.height() > params.vScroll + params.viewportHeight ) {
        if ( this.height() > params.viewportHeight ) {
            this.top(params.vScroll + 35);
        } else {
            this.top(params.vScroll + params.viewportHeight - this.height() - 35);
        }
    } else {
        this.top(params.eTop);
    }
    /* pop-up left position */
    if ( params.eLeft + this.width() > params.hScroll + params.viewportWidth ) {
        this.left(params.hScroll + params.viewportWidth - this.width() - 35);
    } else {
        this.left(params.eLeft);
    }

    this.autohide(params.autohide);

    if ( typeof params.title === 'string' ) { // set container title
        this.header(params.title);
    }
    
    if ( typeof params.body === 'string' ) { // st container body
        this.body(params.body);
    }

    //  We can call any handler function before display popup but after set popup's body or title
    //  Handler is called with 2 params:
    //      1 - current object
    //      2 - object params, which set as param into show function
    if ( params.handler !== undefined && typeof params.handler === 'function' ) { // call heandler
        if ( params.context !== undefined && typeof params.context === 'object' )
            params.handler.call(params.context, this, params);
        else
            params.handler(this, params);
    }

    /**
     *  set autohide handler
     */
    if ( this.autohide() ) {
        $(this.container()).bind('mouseenter mouseleave', {
            roll:this
        }, this.hideOnMouseOver);
        
    }
    this.zindex = FSPopupRoll.prototype.zindex;
    if ( params.zindex != undefined && params.zindex != null && typeof params.zindex === 'number' )
    {
        this.zindex = params.zindex;
    }else{
        FSPopupRoll.prototype.zindex++;
    }
    $(this.container()).css('z-index', ++this.zindex);
    $(this.container()).css('display', '');

    //  We can call any postHandler function after display popup
    //  postHandler is called with 2 params:
    //      1 - current object
    //      2 - object params, which set as param into show function
    if ( params.postHandler !== undefined && typeof params.postHandler === 'function' ) { // call  postHeandler
        if ( params.context !== undefined && typeof params.context === 'object' )
            params.postHandler.call(params.context, this, params);
        else
            params.postHandler(this, params);
    }

    $('body').css('cursor', '');
}
FSPopupRoll.prototype.showAjax = function (element, event, params) {
    var divContent = "divContent"+Math.round(Math.random()*100000);
    params.body = "<div id='"+divContent+"'><div style='text-align:center;'><img src='"+this.waitImage.src+"' alt='loading...' width='"+this.waitImage.width+"px' /></div></div>";
    this.showNotAjax(element, event, params);
    $.ajax({
        url : params.url,
        data : params.data,
        cache : false,
        type : 'POST',
        success : function(data) {
            jQuery("#"+divContent).html(data);
            if(typeof(params.successHander) == 'function'){
                params.successHander();
            }
        }
    });  
}

FSPopupRoll.prototype.showNotAjaxwithloading = function (element, event, params) {
    $('body').css('cursor', 'wait');

    params = params || {};
this.body("<img src='"+this.waitImage.src+"' alt='loading...' width='"+this.waitImage.width+"px' />");
    this.width(this.waitImage.width);
    this.height('');
    //  We can call any preHandler function before set popup's body or title
    //  preHandler is called with 2 params:
    //      1 - current object
    //      2 - object params, which set as param into show function
    if ( params.preHandler !== undefined && typeof params.preHandler === 'function' ) { // call preHeandler
        if ( params.context !== undefined && typeof params.context === 'object' )
            params.preHandler.call(params.context, this, params);
        else
            params.preHandler(this, params);
    }

    var width;
    var height;
    if ( typeof params.width === 'number' )  {
        width = Math.ceil(params.width);
    }
    else                                     {
        width = this.params.width;
    }
    if ( typeof params.height === 'number' ) {
        height = Math.ceil(params.height);
    }
    else                                     {
        height = '';
    }





    this.width(params.width);
    this.height(params.height);

    var html                = document.documentElement;
    params.viewportHeight   = window.innerHeight || ( html && html.clientHeight ) || document.body.clientHeight;
    params.viewportWidth    = window.innerWidth  || ( html && html.clientWidth )  || document.body.clientWidth;
    params.vScroll          = $(html).scrollTop()  || $(document.body).scrollTop();
    params.hScroll          = $(html).scrollLeft() || $(document.body).scrollLeft();

    if (!params.position) params.position = 'relative';
    //handle position
    if (params.position == 'middle') {
        params.eTop = (params.viewportHeight - this.height())/2;
        params.eLeft = (params.viewportWidth - this.width())/2;
        $(this.container()).css('position', 'fixed');
    } else if (params.position == 'top') {
        params.eTop = params.wTop;
        params.eLeft = (params.viewportWidth - this.width())/2;
        $(this.container()).css('position', 'fixed');
    }else{
      var eleOffset = $(element).offset();
      var topOffset = eleOffset.top;
      var leftOffset = eleOffset.left;
	params.eTop = Math.ceil(topOffset);
       params.eLeft = Math.ceil(leftOffset);
/*
        params.eTop             = Math.ceil($(element).offset().top);
        params.eLeft            = Math.ceil($(element).offset().left);
*/  
      $(this.container()).css('position', 'absolute');
    }
    if(typeof(params.paddingleft) != 'undefined')
    {
        params.eLeft = params.eLeft + params.paddingleft;
    } 
    if(typeof(params.paddingTop) != 'undefined')
    {
        params.eTop = params.eTop + params.paddingTop;
    } 
    $(this.header()).css('width', this.width() - 20); //  20 pixels for roll close button

    /* pop-up top position */
    if ( params.eTop + this.height() > params.vScroll + params.viewportHeight ) {
        if ( this.height() > params.viewportHeight ) {
            this.top(params.vScroll + 35);
        } else {
            this.top(params.vScroll + params.viewportHeight - this.height() - 35);
        }
    } else {
        this.top(params.eTop);
    }
    /* pop-up left position */
    if ( params.eLeft + this.width() > params.hScroll + params.viewportWidth ) {
        this.left(params.hScroll + params.viewportWidth - this.width() - 35);
    } else {
        this.left(params.eLeft);
    }

    this.autohide(params.autohide);

    if ( typeof params.title === 'string' ) { // set container title
        this.header(params.title);
    }
    
    if ( typeof params.body === 'string' ) { // st container body
        this.body(params.body);
    }

    //  We can call any handler function before display popup but after set popup's body or title
    //  Handler is called with 2 params:
    //      1 - current object
    //      2 - object params, which set as param into show function
    if ( params.handler !== undefined && typeof params.handler === 'function' ) { // call heandler
        if ( params.context !== undefined && typeof params.context === 'object' )
            params.handler.call(params.context, this, params);
        else
            params.handler(this, params);
    }

/**
     *  set autohide handler
     */
    if ( this.autohide() ) {
        $(this.container()).bind('mouseenter mouseleave', {
            roll:this
        }, this.hideOnMouseOver);
        
    }
    this.zindex = FSPopupRoll.prototype.zindex;
    if ( params.zindex != undefined && params.zindex != null && typeof params.zindex === 'number' )
    {
        this.zindex = params.zindex;
    }else{
        FSPopupRoll.prototype.zindex++;
    }
    $(this.container()).css('z-index', ++this.zindex);
    $(this.container()).css('display', '');

    //  We can call any postHandler function after display popup
    //  postHandler is called with 2 params:
    //      1 - current object
    //      2 - object params, which set as param into show function
    if ( params.postHandler !== undefined && typeof params.postHandler === 'function' ) { // call  postHeandler
        if ( params.context !== undefined && typeof params.context === 'object' )
            params.postHandler.call(params.context, this, params);
        else
            params.postHandler(this, params);
    }

    $('body').css('cursor', '');
}



/**
 *  updateContent
 *
 *  Update displayed roll
 */
FSPopupRoll.prototype.updateContent = function (element, event, params) {
    params = params || {};

    //  We can call any preHandler function before set popup's body or title
    //  preHandler is called with 2 params:
    //      1 - current object
    //      2 - object params, which set as param into show function
    if ( params.preHandler !== undefined && typeof params.preHandler === 'function' ) { // call preHeandler
        if ( params.context !== undefined && typeof params.context === 'object' )
            params.preHandler.call(params.context, this, params);
        else
            params.preHandler(this, params);
    }

    if ( typeof params.title === 'string' ) { // set container title
        this.header(params.title);
    }
    if ( typeof params.body === 'string' ) { // st container body
        this.body(params.body);
    }

    //  We can call any handler function before display popup but after set popup's body or title
    //  Handler is called with 2 params:
    //      1 - current object
    //      2 - object params, which set as param into show function
    if ( params.handler !== undefined && typeof params.handler === 'function' ) { // call heandler
        if ( params.context !== undefined && typeof params.context === 'object' )
            params.handler.call(params.context, this, params);
        else
            params.handler(this, params);
    }


    //  We can call any postHandler function after display popup
    //  postHandler is called with 2 params:
    //      1 - current object
    //      2 - object params, which set as param into show function
    if ( params.postHandler !== undefined && typeof params.postHandler === 'function' ) { // call  postHeandler
        if ( params.context !== undefined && typeof params.context === 'object' )
            params.postHandler.call(params.context, this, params);
        else
            params.postHandler(this, params);
    }
}
/**
 *  hide
 *
 *  Hide roll popup
 *
 *  @param Object options Optional
 *      options.preHandler  - handler function, which will be called before hide the roll popup
 *      options.postHandler - handler function, which will be called after hide the roll popup
 *      options.context     - object, which will be set as global context for called handlers
 */
FSPopupRoll.prototype.hide = function (options) {
    options = options || {};

    if ( options.preHandler !== undefined && typeof options.preHandler === 'function' ) {
        if ( options.context !== undefined && typeof options.context === 'object' ) {
            options.preHandler.call(options.context, this, options);
        } else {
            options.preHandler(this, options);
        }
    }

    $(this.container()).css('display', 'none');
    this.body('');
    this.header('');
    $(this.closeBtn()).css('display', '');

    /**
     *  if close roll, clear all previous binds,
     *  and each roll has autohide == false by default
     */
    if ( this.autohide() ) {
        this.autohide(false);
        $(this.container()).unbind('mouseenter mouseleave');
    }

    if ( options.postHandler !== undefined && typeof options.postHandler === 'function' ) {
        if ( options.context !== undefined && typeof options.context === 'object' ) {
            options.postHandler.call(options.context, this, options);
        } else {
            options.postHandler(this, options);
        }
    }

    $('body').css('cursor', '');
}
/**
 *  init
 *
 *  Call once  when container will be created in DOM
 *
 *  @return void
 */
FSPopupRoll.prototype.init = function () {
    //  no any additional initialization
}
/**
 *  hideOnMouseOver
 *
 *  Handler for Rolls for mouseOut event,
 *  it can be set in init method
 *
 *  @param Event e Event object
 *  @return void
 */
FSPopupRoll.prototype.hideOnMouseOver = function (e) {
    // this - Div Node, Roll container
    switch ( e.type.toLowerCase() ) {
        case 'mouseenter' :
            if ( e.data.roll.timer() !== null ) {
                clearTimeout(e.data.roll.timer());
            }
            e.data.roll.timer(null);
            break;
        case 'mouseleave' :
            e.data.roll.timer(setTimeout(function () {
                e.data.roll.hide();
            }, e.data.roll.timerWait()*1000));
            break;
    }
}
