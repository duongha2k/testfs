/**
 * FSWidgetDashContentRight
 */
function FSWidgetDashContentRight ( params ) {
    FSWidget.call(this, params);

    this.waitImage = new Image();
    this.waitImage.src = "/widgets/images/progress.gif";     //  pre load wait image
    this.waitImage.width = 190;
    this.waitImage.height = 14;

    this.params.baseUrl = document.location.protocol + '//' + document.location.hostname + '/widgets/dashboard/sections/dash-content-right';
    this.params.container = params.container;
	
    this.objMain = params.objMain;
    this.checkMinWidth = params.checkMinWidth;

    try {
	this.showrightpanel = params.showrightpanel;
    } catch (e) {
	this.showrightpanel = null;
    }
}

FSWidgetDashContentRight.prototype = new FSWidget();
FSWidgetDashContentRight.prototype.constructor = FSWidgetDashContentRight;

FSWidgetDashContentRight.NAME          = 'FSWidgetDashContentRight';
FSWidgetDashContentRight.VERSION       = '0.1';
FSWidgetDashContentRight.DESCRIPTION   = 'Class FSWidgetDashContentRight';

FSWidgetDashContentRight.prototype.showPreloader = function() {
    $('body').css('cursor', 'wait');
    var img = "<div class='tCenter' ><img style='margin:10% auto;' alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></div>";
    FSWidget.fillContainer(img, this);
}

FSWidgetDashContentRight.prototype.show = function() {
    
    this.showPreloader();
//    data = this.params.data || {};
    data = {companyId: this.params.company, showrightpanel: this.showrightpanel};

    if ( this.tabOpenXhrRequest ) {
        this.tabOpenXhrRequest.abort();
        this.tabOpenXhrRequest = null;
    }

    this.tabOpenXhrRequest = jQuery.ajax({
        url : this.params.baseUrl,
        data : data,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/clients/logIn.php";
            } else {
                FSWidget.fillContainer("Sorry, but an error occurred. No results found.", this);
            }
            this.tabOpenXhrRequest = null;
        },
        success : function(data, status, xhr) {
            $('body').css('cursor', '');
            FSWidget.fillContainer(data, this)
        }
    });
}

FSWidgetDashContentRight.prototype.showSidebar = function (){
       $("#separatorOutside").hide();
       $("#separatorInside").show();
       this.objMain.addClass('use-sidebar-right');
       this.objMain.removeClass('sidebar-at-right');
       $("#dashcentercontent").attr("id", "dashcentercontent");
       $.cookie('sidebar-pref2', 'use-sidebar-right', { expires: 30 });
};

FSWidgetDashContentRight.prototype.hideSidebar = function (){
	if(this.objMain.hasClass('use-sidebar-right')){
		$("#separatorOutside").show();
		$("#separatorInside").hide();
		this.objMain.removeClass('use-sidebar-right');
		this.objMain.addClass('sidebar-at-right');
		$.cookie('sidebar-pref2', null, { expires: 30 });
	}
};

FSWidgetDashContentRight.prototype.separatorClick = function(e){
	if ( this.objMain.hasClass('use-sidebar-right') ){
		this.hideSidebar();
	}
	else {
		this.showSidebar();
	}
	this.checkMinWidth();
};
FSWidgetDashContentRight.prototype.showRecentWO = function(win){
//	this.hideSidebar();
//	detailsWidget.show({tab:'index',params:{company:window._company,container:'detailsContainer',tab:"new", win: win}});
//	this.checkMinWidth();
}
