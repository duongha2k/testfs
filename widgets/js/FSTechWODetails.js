function FSTechWODetails( win, tech, key )
{
    this._tech      = tech;
    this._win       = win;
    this._key       = key;
    this._rotes     = {};
    this._wo        = {};
    this._messages  = [];
    this._msgBoxId  = 'messagesBox';
    this._modified   = false;
    this._isClaimingConfirm = true;
    this._iswarningTechClaim = true;
    this._iswarningmarkdone = false;
    this.builRoutes();
    this.getWO();

    /* preload progress bar image for save if in browser cache */
    var img = new Image();
    img.src = '/widgets/images/progress.gif';
}

FSTechWODetails.prototype.builRoutes = function()
{
    this._rotes = {
        getwodata   : '/widgets/dashboard/js/prepare-wo/get/1/win/' + this._win + '/key/' + this._key + '/',
        updatewo    : '/widgets/dashboard/tech-wo-details/update-wo/win/' + this._win + '/',
        getNumberFsPart    : '/widgets/dashboard/tech-wo-details/get-number-fs-park/'
    };
};

FSTechWODetails.prototype.getWO = function()
{
    $.ajax({
        url : this._rotes.getwodata,
        type : 'POST',
        dataType : 'json',
        context : this,
        error : function (xhr, status, error) {
            this._wo = {};
        },
        success : function(data, status, xhr) {
            this._wo = data;
        }
    });
};

FSTechWODetails.prototype.updateWO = function( params )
{
    var context = params.context || this;
    var errorHandler = params.onerror || function (xhr, status, error) {
        if ( xhr.responseText === "authentication required" ) {
            document.location = "/clients/logIn.php";
        } else {
            this.clearMessages();
            this.addMessage(xhr.responseText);
            this.getMBox().html(this.getErrors());
        }
    };
    var successHandler = params.onsuccess || function (data, status, xhr) {
        this.hideMessages();
        location.reload();
        //window.close(); // see issue #12323 for more details @author Artem Sukharev
    };
    var options = params.options || {};
    var validator = params.validator || null;

    this.clearMessages();
    this.hideMessages();
    var isUploadFile = 0;
    var isTechMarkedComplete = $('#TechWOWorkPlaceRight #TechMarkedComplete').is(":checked");
    if(isTechMarkedComplete) isTechMarkedComplete = "True"; else isTechMarkedComplete = "False";
    var isSignOffSheet = jQuery("#HidSignOffSheet_Required").val();
    if((isSignOffSheet == 1 ) && (isTechMarkedComplete == "True"))
    {    
        jQuery("#UploadGreenTechPicsNew").find("input:file").each(function(){
            if(jQuery(this).val() != "")
            {
                isUploadFile = 1;
                return false;
            }    
        });
        if(isUploadFile == 0)
        {
            jQuery(".UploadGreenTechPicsNew").each(function(){
                if(!jQuery(this).find('input:checkbox').is(":checked"))
                    {
                        isUploadFile = 1;
                        return false;
                    }   
            });
        }
        if(isUploadFile == 0)
        {
            this.addMessage("Document Upload is required and cannot be empty");
        }    
    }
    else
    {
        isUploadFile = 1;
    }    
    var el = this;
    el.displayProgress();
    $.ajax({
        url : this._rotes.getNumberFsPart,
        type : 'POST',
        dataType : 'json',
        data : "key=" + this._key + "&win="+this._win,
        error : function (xhr, status, error) {
            el.setMessages("Service Error.");
            el.getMBox().html(el.getErrors());
        },
        success : function(data, status, xhr) {
            var ManagerUpdate_Required = jQuery("#HidPartManagerUpdate_Required").val();
            var HidSignOffSheet_Required = jQuery("#HidSignOffSheet_Required").val();
            if(data.success == 0 && typeof(ManagerUpdate_Required) != 'undefined' && (isTechMarkedComplete == "True"))
            {
                el.setMessages(data.error);
                el.getMBox().html(el.getErrors());
            }    
            else if (( null === validator || validator.isValid() ) 
                && (isUploadFile == 1 || typeof(HidSignOffSheet_Required) == 'undefined')) {
                el.displayProgress();
                $.ajax({
                    url         : el._rotes.updatewo,
                    type        : 'POST',
                    dataType    : 'html',
                    data        : options,
                    context     : context,
                    error       : errorHandler,
                    success     : successHandler
                });
            } else {
                if(isUploadFile == 1)
                {    
                    el.setMessages(validator.getErrors());
                }
                el.getMBox().html(el.getErrors());
            }
        }
    });
    
};

/**
 *  @param object form
 *  @param bool updateGreenBox
 */
FSTechWODetails.prototype.getCheckDates = function( form, updateGreenBox )
{
    updateGreenBox = ( updateGreenBox !== undefined && updateGreenBox !== null ) ? updateGreenBox : true;

    var o = {
        prevCheckIn  : undefined,
        prevCheckOut : undefined,
        CheckIn      : undefined,
        CheckOut     : undefined,
        TotalSeconds : undefined
    };

    var datePattern  = /^(\d+\/\d+\/\d+)/i;
    var timePattern1 = /^(1[0-2]|0?\d)\s*(am|pm)$/i;
    var timePattern2 = /^(1[0-2]|0?\d)\:([0-5]\d)\s*(am|pm)$/i;
    var result;
    
    var Date_In = $.trim(form.Date_In.value);
    var Time_In = $.trim(form.Time_In.value);
    var Date_Out = $.trim(form.Date_Out.value);
    var Time_Out = $.trim(form.Time_Out.value);

    var numOfVisits = jQuery("#tableVisitEdit .trVisitEdit").length;
    var i=0;
    var totalSeconds = 0;
    for(i=1;i<=numOfVisits;i++)
    {
        var iDate_In = jQuery("#tableVisitEdit #CheckInDate"+i.toString()).val();
        iDate_In = $.trim(iDate_In);
        var iTime_In = jQuery("#tableVisitEdit #CheckInTime"+i.toString()).val();
        iTime_In = $.trim(iTime_In);
        var iDate_Out = jQuery("#tableVisitEdit #CheckOutDate"+i.toString()).val();
        iDate_Out = $.trim(iDate_Out);
        var iTime_Out = jQuery("#tableVisitEdit #CheckOutTime"+i.toString()).val();
        iTime_Out = $.trim(iTime_Out);
        
        if ( !datePattern.test(iDate_In) )  {iDate_In = false;}
        if ( !timePattern1.test(iTime_In) && !timePattern2.test(iTime_In) )  {iTime_In = false;}
        if ( !datePattern.test(iDate_Out) ) {iDate_Out = false;}
        if ( !timePattern1.test(iTime_Out) && !timePattern2.test(iTime_Out) ) {iTime_Out = false;}
        
        var iCheckIn = (iDate_In && iTime_In) ? new Date(iDate_In + " " + iTime_In) : null;
        var iCheckOut = (iDate_Out && iTime_Out) ? new Date(iDate_Out + " " + iTime_Out) : null;
        
        if ( iCheckIn != null && iCheckOut != null ) {
            totalSeconds += (iCheckOut - iCheckIn) / 1000;
        }
    }
    o.TotalSeconds = totalSeconds;
    
    /* Feel Step 3 Green Box */
    if ( updateGreenBox ) {
        $('#TechWOWorkPlaceRight #Date_In').val(($.trim(Date_In)) ? Date_In : 'MM/DD/YYYY');
        $('#TechWOWorkPlaceRight #Date_Out').val(($.trim(Date_Out)) ? Date_Out : 'MM/DD/YYYY');
        $('#TechWOWorkPlaceRight #Time_In').val(($.trim(Time_In)) ? Time_In : 'H:MM AM/PM');
        $('#TechWOWorkPlaceRight #Time_Out').val(($.trim(Time_Out)) ? Time_Out : 'H:MM AM/PM');
    }
    /* Feel Step 3 Green Box */

    if ( this._wo.Date_In == null ) this._wo.Date_In = '';
    if ( (result = this._wo.Date_In.match(datePattern)) != null ) {
        this._wo.Date_In = result[1];
    } else {
        this._wo.Date_In = '';
    }
    if ( this._wo.Date_Out == null ) this._wo.Date_Out = '';
    if ( (result = this._wo.Date_Out.match(datePattern)) != null ) {
        this._wo.Date_Out = result[1];
    } else {
        this._wo.Date_Out = '';
    }
    if ( this._wo.Time_In == null ) this._wo.Time_In = '';
    if ( this._wo.Time_Out == null ) this._wo.Time_Out = '';

    if ( !datePattern.test(Date_In) )  {Date_In = false;}
    if ( !timePattern1.test(Time_In) && !timePattern2.test(Time_In) )  {Time_In = false;}
    if ( !datePattern.test(Date_Out) ) {Date_Out = false;}
    if ( !timePattern1.test(Time_Out) && !timePattern2.test(Time_Out) ) {Time_Out = false;}

    o.prevCheckIn   = (this._wo.Date_In && this._wo.Time_In) ? new Date(this._wo.Date_In+" "+this._wo.Time_In) : null;
    o.prevCheckOut  = (this._wo.Date_Out && this._wo.Time_Out) ? new Date(this._wo.Date_Out+" "+this._wo.Time_Out) : null;
    o.CheckIn       = (Date_In && Time_In) ? new Date(Date_In + " " + Time_In) : null;
    o.CheckOut      = (Date_Out && Time_Out) ? new Date(Date_Out + " " + Time_Out) : null;

    return o;
};

FSTechWODetails.prototype.fullUpdateRequest = function(form)
{
    jQuery("#Date_In").val(jQuery("#CheckInDate1").val());
    jQuery("#Time_In").val(jQuery("#CheckInTime1").val());
    jQuery("#Date_Out").val(jQuery("#CheckOutDate1").val());
    jQuery("#Time_Out").val(jQuery("#CheckOutTime1").val());
    var o = this.getCheckDates(form);
    var CheckIn = o.CheckIn;
    var CheckOut = o.CheckOut;
    var prevCheckIn = o.prevCheckIn;
    var prevCheckOut = o.prevCheckOut;
    var TotalSeconds = o.TotalSeconds;

    //13882
    //if((CheckIn!=null || CheckOut!=null) && (CheckIn==null || CheckOut==null)){
	if ($("#edit_TechMarkedComplete").is(":checked")) {
	    if (jQuery("#CheckInDate1").val() == "" || jQuery("#CheckInTime1").val() == "" || 
	    	jQuery("#CheckOutDate1").val() == "" || jQuery("#CheckOutTime1").val() == "") {
	        alert('You set incorrect Check In/Out times. Please, check again');
        this._isClaimingConfirm = false;
	        return false;
	    }
	} else {
        if(jQuery("#WO_STAGE").val() != 'Assigned' || $("#edit_TechMarkedComplete").is(":checked")){
            if (jQuery("#CheckInDate1").val() == "" || jQuery("#CheckInTime1").val() == "") {
                alert('You set incorrect Check In/Out times. Please, check again');
                return false;
            }            
        }
	}
    //end 13882


    if ($("#Unexpected_Steps").attr("checked") && $("#Unexpected_Desc").val() == ''){
        $("#Unexpected_Desc").focus();
        alert('Please enter in a description of what was unexpected or undocumented.');
        this._isClaimingConfirm = false;
        return false;
    }else if ($("#Unexpected_Steps").attr("checked") && $("#AskedBy").val() == ''){
        $("#AskedBy").focus();
        alert('Please enter who asked you to perform these steps.');
        this._isClaimingConfirm = false;
        return false;
    }else if ($("#Unexpected_Steps").attr("checked") && $("#AskedBy_Name").val() == ''){
        $("#AskedBy_Name").focus();
        alert('Please enter the name of the person asking you to perform this work.');
        this._isClaimingConfirm = false;
        return false;
    }
    if ($('#edit_TechMarkedComplete').attr('checked') && ($('#woForm #TechComments').val() == 'Close out comments are required' || $('#woForm #TechComments').val() == '')) {
        alert('Close out comments are required');
        $('#woForm #TechComments').focus();
        this._isClaimingConfirm = false;
        return false;

    }

    if ( CheckIn != null && CheckOut != null && ( prevCheckIn == null || prevCheckOut == null || CheckIn.toString() != prevCheckIn.toString() || CheckOut.toString() != prevCheckOut.toString() ) ) {
        //result = (CheckOut - CheckIn) / 1000;
        result = TotalSeconds;
        if ( result <= 0) {
            if(jQuery("#WO_STAGE").val() != 'Assigned' || $("#edit_TechMarkedComplete").is(":checked")){
                alert("You set incorrect Check In/Out times. Please, check again");
            }
        } else {
            var seconds = result % 60;
            var minutes = (result - seconds) / 60 % 60;
            var hours   = (result - seconds - minutes * 60) / 3600;
            var query   = "You are claiming ";
            if ( hours == 0 )       { /* skip */ } 
            else                    {query += hours.toString() + " hour" + (( hours > 1 ) ? "s " : " ");}
            if ( minutes == 0 )     { /* skip */ }
            else                    {query += "and " + minutes.toString() + " minute" + (( minutes > 1) ? "s " : " ");}
            query += "of time on site for this work order. Please Confirm this entry by clicking OK";

            var event = this;
            if(!event._iswarningmarkdone)
            {
            if ( confirm( query ) )
                {
                    if(!$("#edit_TechMarkedComplete").is(":checked") && this._modified)
                    {
                        var content = "\
                    <div style='width:500px;height:auto;' id='warningTechClaim'>\
                        <div>Warning: You have not marked this work order as Complete at the top of Section 2.<br><br></div>\
                        <div>\
                            <table border='0' width='100%'>\
                                <tr>\
                                    <td style='width:50%;text-align:right;'>\
                                        <input id='checkMarkcompletedCancel' class='link_button middle2_button' type='button' value='Cancel'>\
                                    </td>\
                                    <td style='width:50%;text-align:left;'>\
                                        <input id='checkMarkcompletedContinue' class='link_button middle2_button' type='button' value='Continue'>\
                                    </td>\
                                </tr>\
                            </table>\
                        </div>\
                    </div>";
                        $("<div></div>").fancybox(
                        {
                            'autoDimensions' : true,
                            'showCloseButton' :true,
                            'hideOnOverlayClick' : false,
                            'content': content
                        }
                        ).trigger('click');

                        jQuery("#checkMarkcompletedContinue").click(function(){
                            $.fancybox.close();
                            event._isClaimingConfirm = true;
                            event._iswarningmarkdone = true;
                            document.getElementById('woDetailsFormSubmitBtn').click();
                            return true;
                        });
                        jQuery("#checkMarkcompletedCancel").click(function(){
                            $.fancybox.close();
                            event._isClaimingConfirm = false;
                            event._iswarningmarkdone = false;
                        });
                    }
                    else
                    {
                        event._isClaimingConfirm = true;
                        return true;
                    }
                }
            }   
            else
            {
                    this._isClaimingConfirm = true;
                return true;
        }
        }
        this._isClaimingConfirm = false;
        return false;
    }
    this._isClaimingConfirm = true;
    return true;
}

/**
 *  @param obj form
 *  @param bool updateGreenBox
 */
FSTechWODetails.prototype.calcTechHrs = function(form, updateGreenBox)
{
    var o = this.getCheckDates(form, updateGreenBox);
    var CheckIn = o.CheckIn;
    var CheckOut = o.CheckOut;
    var prevCheckIn = o.prevCheckIn;
    var prevCheckOut = o.prevCheckOut;

    if ( CheckIn != null && CheckOut != null && ( prevCheckIn == null || prevCheckOut == null || CheckIn.toString() != prevCheckIn.toString() || CheckOut.toString() != prevCheckOut.toString() ) ) {
        result = (CheckOut - CheckIn);
        
        if ( result <= 0) {
            //  skip
        } else {
            result = result / 3600000;
            result = Math.round( result * 100 ) / 100;
            return result;
        }
    }
    return 0;
}

FSTechWODetails.prototype.updateTechHrsValue = function( form, box, updateGreenBox )
{
    var toAdd = this.calcTechHrs(form, updateGreenBox);
    var old = parseFloat(this._wo.calculatedTechHrs);

    if ( toAdd <= 0 ) {
        if ( !this._wo.calculatedTechHrs ) {
            return $(box).text('');
        } else {
            return ( this._wo.calculatedTechHrs != 0 ) ? $(box).text( this._wo.calculatedTechHrs + ' hours' ) : $(box).text('');
        }
    }
    if ( isNaN(old) ) {
        old = 0;
    }
//    s = old + toAdd;
//    return ( s != 0 ) ? $(box).text( old + toAdd + ' hours' ) : $(box).text('');
    s = toAdd;
    return ( s != 0 ) ? $(box).text( toAdd + ' hours' ) : $(box).text('');
}

FSTechWODetails.prototype.showCal = function( calObj, input, format )
{
    var inpValue = $.trim($(input).val());
    if ( inpValue == format ) {
        $(input).val('');
    }
    calObj.showFor(input);
}

FSTechWODetails.prototype.hasMessages = function() 
{
    return (this._messages.length > 0);
}

FSTechWODetails.prototype.clearMessages = function()
{
    return this._messages = [];
}

FSTechWODetails.prototype.addMessage = function( msg )
{
    return this._messages.push(msg);
}

FSTechWODetails.prototype.setMessages = function( msgs )
{
    if ( msgs instanceof Array ) {
        this._messages = msgs;
    } else {
        this._messages = [msgs];
    }
}

FSTechWODetails.prototype.getErrors = function()
{
    var output, idx;
    output  = '<ul class="errors">';
    for ( idx = 0; idx < this._messages.length; ++idx ) {
        output += '<li class="error">' + this._messages[idx] + '</li>';
    }
    output += '</ul>';

    return output;
}

FSTechWODetails.prototype.displayErrors = function()
{
    this.getMBox().html(this.getErrors());
}

FSTechWODetails.prototype.displayMessages = function()
{
    this.getMBox().html(this.getMessages());
}

FSTechWODetails.prototype.displayProgress = function()
{
    var output = "";
    output += "<center>";
    output += "<img src='/widgets/images/progress.gif' alt='Loading...'><br />";
    output += "<span>Loading...</span>";
    output += "</center>";
    this.getMBox().html(output);
}
FSTechWODetails.prototype.hideMessages = function()
{
    this.getMBox().html('');
}

FSTechWODetails.prototype.getMessages = function()
{
    var output, idx;
    output  = '<ul class="messages">';
    for ( idx = 0; idx < this._errors.length; ++idx ) {
        output += '<li class="message">' + this._errors[idx] + '</li>';
    }
    output += '</ul>';

    return output;
}

FSTechWODetails.prototype.getMBox = function()
{
    return $(this._msgBoxId);
}

FSTechWODetails.prototype.setMBox = function( box )
{
    this._msgBoxId = box;
}

FSTechWODetails.prototype.dynClearFill = function( obj, msg )
{
    msg = msg || $(obj).val();

    if ( $.trim($(obj).val()) === "" ) {
        $(obj).val(msg);
    } else if ( $.trim($(obj).val()) === msg ) {
        $(obj).val("");
    }
}
FSTechWODetails.prototype.createBid = function( data, onSuccess )
{
    $.ajax({
        url : '/widgets/dashboard/tech/create-bid/',
        type : 'POST',
        dataType : 'json',
        data : data,
        context : this,
        error : function (xhr, status, error) {
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/techs/logIn.php";
            } else {
                this.clearMessages();
                this.addMessage(xhr.responseText);
                this.getMBox().html(this.getErrors());
            }
        },
        success : function(data, status, xhr) {
            if (data.success) {
                onSuccess( data );
            } else {
                this.clearMessages();
                this.hideMessages();
                obj = this;
                $.each(data.errors, function(index, value) {
                    obj.addMessage(value);
                });
                this.displayErrors();
            }
        }
    });
}
FSTechWODetails.prototype.createwithdrawBid = function( data, onSuccess )
{
    $.ajax({
        url : '/widgets/dashboard/tech/create-withdrawbid/',
        type : 'POST',
        dataType : 'json',
        data : data,
        context : this,
        error : function (xhr, status, error) {
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/techs/logIn.php";
            } else {
                this.clearMessages();
                this.addMessage(xhr.responseText);
                this.getMBox().html(this.getErrors());
            }
        },
        success : function(data, status, xhr) {
                this.clearMessages();
                this.hideMessages();
                obj = this;
                $.each(data.errors, function(index, value) {
                    obj.addMessage(value);
                });
                this.displayErrors();
                $("#techwithdraw").attr('style','display:none');
                $("#techwithdraw").attr('checked',false);
                $("#techwithdrawlabel").attr('style','display:none');
            $('#workPlaceBoxes').show();
            $('#workPlaceBoxesApply').hide();
//            showLightBoxMsg('<span style="padding:10px 10px 10px 10px;"> Your application has been withdrawn.</span>');
            var content = "<div style='width:250px;height:30px;' id='comtantPopupID'>Your application has been withdrawn.</div>";
                    $("<div></div>").fancybox({
                        'autoDimensions' : true,
                        'showCloseButton' :true,
                        'hideOnOverlayClick' : false,
                        'content': content,
                        'onComplete': function () {
                            setTimeout('$.fancybox.close()', 2000);
            }
                    }).trigger('click');
        }
    });
}
FSTechWODetails.prototype.confirmRemoveFile = function( obj, file_name )
{
    if ( $(obj).attr('checked') ) {
        if ( confirm('Are you sure you wish to remove the file "' + file_name + '"?') ) {
            $(obj).attr('checked', true);
        } else {
            $(obj).attr('checked', false);
        }
    }
}
FSTechWODetails.prototype.confirmRemoveFileImg = function( obj, file_name )
{
    if ( confirm('Are you sure you wish to remove the file "' + file_name + '"?') ) {
        $(obj).parent().children("input:checkbox").attr('checked', 'checked');
        $(obj).parent().parent().hide();
        this._modified = true;
    } else {
    }
}
FSTechWODetails.prototype.warningTechClaim = function(formSubmit){
    if(woDetails._formSubmit) return true;
    //confirm('')
//    var items = "";
//    if($("#spanrequiredocupl") != "undefined")
//    {
//        items = $("#tablerequiredocupl").find("a").attr("class","ajaxLink").length;
//    }
    var isShow = false;
  
    $(".masterReimbursable").each(function() {
        if(jQuery(this).val() != "")
        {
            var el  = jQuery(this).parent().parent().find(".tableFile"); 
            var lengthFile = jQuery(el).find(".linkdownloadfile").length;   
            if(el.length > 0 && lengthFile <= 0)
            {    
                var isFileUpload = true;
                jQuery(el).find("input:file").each(function(){
                    if(jQuery(this).val() != "")
                    {
                        isFileUpload = false;
                        return false; 
                    }    
                });
                isShow = isFileUpload;
            }
        }
        if(isShow) return false;
    });
    //376
    if(isShow)
    {
        var content = "\
            <div style='width:400px;height:auto;' id='warningTechClaim'>\
                <div>Warning: You have not uploaded documents for all of your Expense Reporting Claims. If you do not upload documents the client may not approve your claims.<br></div>\
                <div>\
                    <table border='0' width='100%'>\
                        <tr>\
                            <td style='width:50%;text-align:right;'>\
                                <input id='checkApprovedCancel' class='link_button middle2_button' type='button' value='Cancel'>\
                            </td>\
                            <td style='width:50%;text-align:left;'>\
                                <input id='warningTechClaimContinue' class='link_button middle2_button' type='button' value='Continue'>\
                            </td>\
                        </tr>\
                    </table>\
                </div>\
            </div>";
        $("<div></div>").fancybox(
            {
                'autoDimensions' : true,
                'showCloseButton' :true,
                'hideOnOverlayClick' : false,
                'content': content
            }
        ).trigger('click');
            
        jQuery("#warningTechClaimContinue").click(function(){
            if(!formSubmit)
            {
                this._iswarningTechClaim = true;
                woDetails.updateWOTech();
            }
            else
            {
                woDetails._formSubmit = true;
                woDetails._iswarningTechClaim = true;
                document.getElementById('woDetailsFormSubmitBtn').click();
            }    
            $.fancybox.close(); 
            
        });
        jQuery("#checkApprovedCancel").click(function(){
            $.fancybox.close(); 
        });
        this._iswarningTechClaim = false;
        return false;
    }
    this._iswarningTechClaim = true;
    return true;
}
FSTechWODetails.prototype.updateWOTech = function()
{
    var el = jQuery("#messagesBoxUpdate");
    if(jQuery("#TechWODetailsSection4_slider #TechComments").val() == "" 
        || jQuery("#TechWODetailsSection4_slider #TechComments").val() == "Close out comments are required")
    {
        jQuery(el).show();
        jQuery(el).find("li").html("Tech Comments is required and cannot be empty");
        return false;
    }
    
    var isUploadFile = 0;
    //358
    var isTechMarkedComplete = $('#TechWODetailsSection4_slider #edit_TechMarkedComplete').is(":checked");
    if(isTechMarkedComplete) isTechMarkedComplete = "True"; else isTechMarkedComplete = "False";
    var isSignOffSheet = jQuery("#TechWODetailsSection4_slider #HidSignOffSheet_RequiredUpdate").val();

    if((isSignOffSheet == 1) && (isTechMarkedComplete =='True'))// end 358
    {    
        jQuery("#TechWODetailsSection4_slider #UploadTechPicsNew").find("input:file").each(function(){
            if(jQuery(this).val() != "")
            {
                isUploadFile = 1;
                return false;
            }    
        });
        
        if(isUploadFile == 0)
        {
            jQuery("#TechWODetailsSection4_slider .UploadTechPicsNew").each(function(){
                if(!jQuery(this).find('input:checkbox').is(":checked"))
                    {
                        isUploadFile = 1;
                        return false;
                    }   
            });
        }    
    }
    else
    {
        isUploadFile = 1;
    }
    
    if(isUploadFile == 0)
    {
        jQuery(el).show();
        jQuery(el).find("li").html("Document Upload is required and cannot be empty");
        return false;
    }
    
    $.ajax({
        url : "/widgets/dashboard/tech-wo-details/get-number-fs-park/",
        type : 'POST',
        dataType : 'json',
        data : "key=" + woDetails._key + "&win="+woDetails._win,
        content : this,
        error : function (xhr, status, error) {
            this.setMessages("Service Error.");
            this.getMBox().html(el.getErrors());
        },
        success : function(data, status, xhr) {
            if(data.success == 0 && (isTechMarkedComplete =='True'))
            {
                jQuery(el).show();
                jQuery(el).find("li").html(data.error);
                return false;
            }  
            document.getElementById('woDetailsFormSubmitBtn').click();
        }
    });
}

FSTechWODetails.prototype.updateWOTechNew = function()
{
	var timein_input, timeout_input, datein_input, dateout_input, break_loop;
	var time_test = new RegExp (/(0?\d|1[0-2]):(0\d|[0-5]\d) (AM|PM)/i);
	//var date_test = new RegExp (/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/);
	var date_test = new RegExp (/^\d{1,2}\/\d{1,2}\/\d{4}$/); //(/^\d{2}\/\d{2}\/\d{4}$/);
	var xor = function (a, b) { return (a != "" || b != "") && !(a != "" && b != ""); };
	var i = 1;
    var el = jQuery("#messagesBoxUpdate");
    var errmsg = "";

    if(jQuery("#TechComments").val() == "" 
        || jQuery("#TechComments").val() == "Close out comments are required")
    {
//        jQuery(el).show();
//        jQuery(el).find("li").html("Tech Comments is required and cannot be empty");
//        return false;
errmsg = "Tech Comments is required and cannot be empty";
    }
    
	do {
		break_loop = true;

		timein_input = jQuery.trim (jQuery ("#CheckInTime" + i).val ());
		timeout_input = jQuery.trim (jQuery ("#CheckOutTime" + i).val ());
		datein_input = jQuery.trim (jQuery ("#CheckInDate" + i).val ());
		dateout_input = jQuery.trim (jQuery ("#CheckOutDate" + i).val ());

		timein_input = timein_input ? timein_input : "";
		timeout_input = timeout_input ? timeout_input : "";
		datein_input = datein_input ? datein_input : "";
		dateout_input = dateout_input ? dateout_input : "";

		if (!xor (timein_input, datein_input)) {
			if (datein_input != "") {
				if (!date_test.test (datein_input)) {
					errmsg = "Check In date formatting is not MM/DD/YYYY";
					break;
				}
				if (!time_test.test (timein_input)) {
					errmsg = "Check In time formatting is not H:MM AM/PM";
					break;
				}
				break_loop = false;
			}
		}
		else {
			errmsg = "Check In time and date must be entered together";
			break;
		}

		if (!xor (timeout_input, dateout_input)) {
			if (dateout_input != "") {
				if (!date_test.test (dateout_input)) {
					errmsg = "Check Out date formatting is not MM/DD/YYYY";
					break;
				}
				if (!time_test.test (timeout_input)) {
					errmsg = "Check Out time formatting is not H:MM AM/PM";
					break;
				}
				break_loop = false;
			}
		}
		else {
			errmsg = "Check Out time and date must be entered together";
			break;
		}

		if (dateout_input != "" && datein_input == "") {
			errmsg = "Please enter Check In date/time before saving Check Out";
			break;
		}

		i++;
	} while (break_loop == false);

    var isUploadFile = 0;
    var isTechMarkedComplete = $('#edit_TechMarkedComplete').is(":checked");
    if(isTechMarkedComplete) isTechMarkedComplete = "True"; else isTechMarkedComplete = "False";
    var dupeDocsArr = [];
    var savedDocsArr = [];
    
    var isSignOffSheet = jQuery("#SignOffSheet_Required").is(":checked");
    if(isSignOffSheet) isSignOffSheet = "True"; else isSignOffSheet = "False";
    var totalSignOffReq = $('#HidSignOffSheet_RequiredUpdate').val();
  
    if((isSignOffSheet == 'True') && (isTechMarkedComplete =='True'))// end 358
    {    
    	
        jQuery("[id=UploadTechPicsNew]").find("input:file").each(function(){
            if(jQuery(this).val() != "")
            {
                isUploadFile = 1;
                dupeDocsArr.push(jQuery(this).val());
            }
        });
        
        jQuery("[id=documentandpart]").find(".linkdownloadfile").each(function(){
        	savedDocsArr.push(jQuery(this).val());
        });

        if(totalSignOffReq - (dupeDocsArr.length + savedDocsArr.length) > 0){
        	 FSTechWODetails.prototype.showerrmess("The Uploaded Documents provided are less than the required amount.");
             return false;
        }
        
        if(isUploadFile == 0)
        {
            jQuery("#tablerequiredocupl .linkdownloadfile").each(function(){
                if(!jQuery(this).find('input:checkbox').is(":checked"))
                    {
                        isUploadFile = 1;
                        return false;
                    }   
            });
        }
        
        if(isUploadFile == 0)
        {
            jQuery("#documentandpart .linkdownloadfile").each(function(){
                if(!jQuery(this).find('input:checkbox').is(":checked"))
                    {
                        isUploadFile = 1;
                        return false;
                    }   
            });
        }
        
    }
    else
    {
        isUploadFile = 1;
    }
    
    if(isUploadFile == 0)
    {
//        jQuery(el).show();
//        jQuery(el).find("li").html("Documents & Parts is required and cannot be empty");
//        return false;
        errmsg = "Document Upload is required and cannot be empty";
    }
    if(errmsg!="")
    {
        jQuery(el).find("li").html(errmsg);
        FSTechWODetails.prototype.showerrmess(errmsg);
        return false;
    }
    jQuery("[id=UploadTechPicsNew]").find("input:file").each(function(){
        if(jQuery(this).val() != "")
        {
        	dupeDocsArr.push(jQuery(this).val());
        }
    });
    if(dupeDocsArr.length >=1){
    	$.ajax({
            url : "/widgets/dashboard/tech-wo-details/check-saved-dupe-docs/",
            type : 'POST',
            data : "win="+woDetails._win+"&docNames="+dupeDocsArr.join(","),
            dataType : 'json',
            error : function (xhr, status, error) {
                el.setMessages("Service Error.");
                el.getMBox().html(el.getErrors());
            },
            success : function(data, status, xhr) {
            	
                if(data.isDupe == true){
                	FSTechWODetails.prototype.showerrmess("Please remove any duplicate documents before proceeding.");
                	return false;
                }
                
                $.ajax({
                    url : "/widgets/dashboard/tech-wo-details/get-number-fs-park/",
                    type : 'POST',
                    dataType : 'json',
                    data : "key=" + woDetails._key + "&win="+woDetails._win,
                    error : function (xhr, status, error) {
                        el.setMessages("Service Error.");
                        el.getMBox().html(el.getErrors());
                    },
                    success : function(data, status, xhr) {
                        if(data.success == 0 && (isTechMarkedComplete =='True'))
                        {
                            jQuery(el).find("li").html(data.error);
                            FSTechWODetails.prototype.showerrmess(data.error);
                            return false;
                        }  
                        
                        document.getElementById('woDetailsFormSubmitBtn').click();
                    }
                });
           }
        });
    }else{
    	$.ajax({
            url : "/widgets/dashboard/tech-wo-details/get-number-fs-park/",
            type : 'POST',
            dataType : 'json',
            data : "key=" + woDetails._key + "&win="+woDetails._win,
            error : function (xhr, status, error) {
                el.setMessages("Service Error.");
                el.getMBox().html(el.getErrors());
            },
            success : function(data, status, xhr) {
                if(data.success == 0 && (isTechMarkedComplete =='True'))
                {
                    jQuery(el).find("li").html(data.error);
                    FSTechWODetails.prototype.showerrmess(data.error);
                    return false;
                }  
                
                document.getElementById('woDetailsFormSubmitBtn').click();
            }
        });
    }
}

FSTechWODetails.prototype.checkSavedDupeDocs = function(docs){
	
}

FSTechWODetails.prototype.showerrmess = function(errmsg)
{
//    var html = "";
    var el = jQuery("#messagesBoxUpdate");
    jQuery(el).find("li").html(errmsg);
    jQuery(el).show();
    
    if($("#bulletSection2").attr('alt') == 'Expand Section')
    {
        toggleTabV2(2);
        uparrowbtn(2); 
    }
    var pos = $("#bulletSection2").offset();
    window.scrollTo(pos.left,pos.top);
    
//    html =jQuery(el).html() + '<br/><div class="tCenter"><input type="button" class="link_button_popup" onclick="roll.hide();" value="Ok" /></div>';
//    roll.autohide(false);
//    var opt = {
//        width       : 400,
//        height      : '',
//        position    : 'middle',
//        title       : '',
//        body        : html
//    };
//
//    return roll.showNotAjax(null, null, opt);
    
//    var content = "\
//            <div style='width:400px;height:auto;' id='warningTechClaim'>\
//                <div>//"+ jQuery(el).html() +"</div>\
//                <div>\
//                    <table border='0' width='100%'>\
//                        <tr>\
//                            <td style='width:50%;text-align:right;'>\
//                                <input id='checkApprovedCancel' class='link_button middle2_button' type='button' value='Cancel'>\
//                            </td>\
//                            <td style='width:50%;text-align:left;'>\
//                                <input id='warningTechClaimContinue' class='link_button middle2_button' type='button' value='Continue'>\
//                            </td>\
//                        </tr>\
//                    </table>\
//                </div>\
//            </div>//";
//        $("<div></div>").fancybox(
//            {
//                'autoDimensions' : true,
//                'showCloseButton' :true,
//                'hideOnOverlayClick' : false,
//                'content': content
//            }
//        ).trigger('click');
}

FSTechWODetails.prototype.modified = function(val)
{
    this._modified = val;
}
FSTechWODetails.prototype.gettotalduration = function(form)
{
    var o = this.getCheckDates(form);
    var TotalSeconds = o.TotalSeconds;
    result = TotalSeconds;
    if ( result <= 0)
    {
    }
    else
    {
        var seconds = result % 60;
        var minutes = (result - seconds) / 60 % 60;
        var hours   = (result - seconds - minutes * 60) / 3600;
        var query   = "";
        
        if ( hours == 0 )       { /* skip */ } 
        else                    {
            query += hours.toString() + " hour" + (( hours > 1 ) ? "s " : " ");
        }
        
        if ( minutes == 0 )     { /* skip */ }
        else if(minutes >0 && hours ==0)
        {
            query += minutes.toString() + " minute" + (( minutes > 1) ? "s " : " ");
        }
        else
        {
            query += "and " + minutes.toString() + " minute" + (( minutes > 1) ? "s " : " ");
        }
        if(query !="")
        {
            $("#spntotalduration").html(query);
            $("span#techwoDisplayTotalDuration").html(query);
        }
   }

}