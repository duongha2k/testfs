function FSWidgetDeactivationTool(wd, roll)
{
    if ( !(wd instanceof FSWidgetDashboard) ) {
        throw new Error('wd should be instance of FSWidgetDashboard');
    }
    if ( !(roll instanceof FSPopupRoll) ) {
        throw new Error('roll should be instance of FSPopupRoll');
    }
    
    this._wd                = wd;
    this._roll              = roll;
    this._html              = null;
    this._win               = null;
    var $this = this;
    this.cancelHandler = function () {
        $this.hideRoll();
    }
}
FSWidgetDeactivationTool.prototype.initAutofill = function ()
{
	$("#win").change(function(event){
		$("#woid").val('');
		$("#reason_select").val('');
		$("#reason").val('');
		$("#updatebutton").attr('disabled', 'disabled');
		$("#updatebutton").addClass('disabled');
		$('#DRprogressimage').css('display','inline-block');
		$.ajax({
			url: "/widgets/dashboard/popup/get-wo-info/",
			type: 'POST',
			context: document.body,
			dataType: "json",
			data: {
				WIN_NUM: $("#win").val(),
				company: wd.filters().company()			
			},
			success: function(data) {
				//fill the form
				if (data.woInfo && data.woInfo.Deactivated) {
					$("#win").val(data.woInfo.WIN_NUM);
					this._win = data.woInfo.WIN_NUM;
					$("#woid").val(data.woInfo.WO_ID);
					$("#reason_select").val(data.woInfo.DeactivationCode);
					$("#reason").val(data.woInfo.Deactivated_Reason);
					$("#updatebutton").removeAttr('disabled');
					$("#updatebutton").removeClass('disabled');
				}else{
					$("#win").val('');
				}
				$('#DRprogressimage').css('display','none');
			}
		})
	});
	$("#woid").change(function(event){
		$("#win").val('');
		$("#reason_select").val('');
		$("#reason").val('');
		$("#updatebutton").attr('disabled', 'disabled');
		$("#updatebutton").addClass('disabled');
		$('#DRprogressimage').css('display','inline-block');
		$.ajax({
			url: "/widgets/dashboard/popup/get-wo-info/",
			type: 'POST',
			context: document.body,
			dataType: "json",
			data: {
				WO_ID: $("#woid").val(),
				company: wd.filters().company()
			},
			success: function(data) {
				//fill the form
				if (data.woInfo && data.woInfo.Deactivated) {
					$("#win").val(data.woInfo.WIN_NUM);
					this._win = data.woInfo.WIN_NUM;
					$("#woid").val(data.woInfo.WO_ID);
					$("#reason_select").val(data.woInfo.DeactivationCode);
					$("#reason").val(data.woInfo.Deactivated_Reason);
					$("#updatebutton").removeAttr('disabled');
					$("#updatebutton").removeClass('disabled');
				}else{
					$("#woid").val('');
				}
				$('#DRprogressimage').css('display','none');
			}
		})
	});
	$(this._roll.closeBtn()).click(this.cancelHandler);
	if (this._win) {
		$('#win').val(this._win).change();
	}
	
	var vWIN = new LiveValidation('win',LVDefaults);
	vWIN.add( Validate.Numericality, { onlyInteger: true } );
	var vWOID = new LiveValidation('woid',LVDefaults);
	vWOID.add( Validate.Presence);
}
FSWidgetDeactivationTool.prototype.setWin = function (win) {
	this._win = win;
}

FSWidgetDeactivationTool.prototype.hideRoll = function () {
	this._win = null;
	this._roll.hide();
	$(this._roll.closeBtn()).unbind('click',this.cancelHandler);
}

FSWidgetDeactivationTool.prototype.updateReason = function () {
	$('#DTmessagetd').html('');
	$('#DRprogressimage').css('display','inline-block');
	$.ajax({
		url: "/widgets/dashboard/do/update-deactivation-reason/",
		type: 'POST',
		context: document.body,
		dataType: "json",
		data: {
			win: $("#win").val(),
			code: $("#reason_select option:selected").val(),
			reason: $("#reason").val(),
			company: wd.params.WOFILTERS.company()
		},
		success: function(data) {
			if (data.result>0) {
				//SUCCESS
				$('#DTmessagetd').addClass('success');
				$('#DTmessagetd').removeClass('error');
				$('#DTmessagetd').html('Success!');
				wd.show({ tab:wd.currentTab, params:wd.getParams() });
				wd.deactivationTool().cancelHandler();
			}else{
				$('#DTmessagetd').removeClass('success');
				$('#DTmessagetd').addClass('error');
				$('#DTmessagetd').html(data.message);
			}
			$('#DRprogressimage').css('display','none');
		}
	});
}
/**
 *  getPopupHTML
 *
 *  Build html for Full Sort Tool
 *
 *  @return string HTML
 */
FSWidgetDeactivationTool.prototype.getPopupHTML = function () 
{
    if ( !this._html ) {
        var html;
        html  = "<table class='deactivation_t' width='100%' cellspacing='0' cellpadding='0'>";
        
		html += "<col width='30%' />";
		html += "<col width='70%' />";
		
        html += "<tr>";
        html += "<td colspan='2' id='DTmessagetd'></td>";
        html += "</tr>";

        html == "<form name='deactivationReasonForm' method='post' action='/'>";

        html += "<tr>";
        html += "<td>WIN #</td><td><input type='text' id='win' name='win' /> or</td>";
        html += "</tr>";
        
        html += "<tr>";
        html += "<td>Client WO ID</td><td><input type='text' id='woid' name='woid' /></td>";
        html += "</tr>";
        
        html += "<tr>";
        html += "<td>Reason Code</td><td><select name='reason_select' id='reason_select' ><option value=\"Select Deactivation Code\">Select Deactivation Code</option><option value=\"Work order kicked back\">Work order kicked back</option><option value=\"Work order declined\">Work order declined</option><option value=\"Not Sourced\">Not Sourced</option><option value=\"No bidding techs (within time needed)\">No bidding techs (within time needed)</option><option value=\"Not approved\">Not approved</option><option value=\"Reassigned with a new work order\">Reassigned with a new work order</option><option value=\"Recruiting work order\">Recruiting work order</option><option value=\"Rescheduled work\">Rescheduled work</option><option value=\"Sourced elsewhere\">Sourced elsewhere</option><option value=\"Tech no showed\">Tech no showed</option><option value=\"Work order cancelled\">Work order cancelled</option><option value=\"Duplicate Work Order\">Duplicate Work Order</option><option value=\"EU cxl - maximum call attempts\">EU cxl - maximum call attempts</option></select></td>";
        html += "</tr>";
        
        html += "<tr>";
        html += "<td colspan='2'>Deactivation Reason</td>";
        html += "</tr>";
        
        html += "<tr>";
        html += "<td colspan='2'><textarea id='reason' name='reason'></textarea><img id='DRprogressimage' src='/widgets/images/wait_small.gif' alt='Loading...' style='display:none;' /></td>";
        html += "</tr>";
        
        html += "<tr>";
        html += "<td colspan='2'>";
        html += "<input type='button' id='updatebutton' value='Update' class='link_button_popup disabled' disabled='disabled' onclick='return wd.deactivationTool().updateReason();' />";
        html += "<input type='button' value='Cancel' class='link_button_popup' onclick='wd.deactivationTool().hideRoll();' />";
        html += "</td>";
        html += "</tr>";

        html += "</form>";

        html += "</table>";

        this._html = html;
    }
    return this._html;
}

