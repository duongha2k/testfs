function FSWidgetAdminSortTool( sortSelect, directionSelect, wd, roll, defaultSortTool )
{
    if (wd !== undefined || roll !== undefined){


    if ( sortSelect === undefined )             sortSelect = null;
    if ( directionSelect === undefined )        directionSelect = null;

    if ( !(wd instanceof FSWidgetDashboard) ) {
        throw new Error('wd should be instance of FSWidgetDashboard');
    }
    if ( !(roll instanceof FSPopupRoll) ) {
        throw new Error('roll should be instance of FSPopupRoll');
    }

    this._sortSelect        = null;
    this._directionSelect   = null;
    this._currentSort       = 'None';
    this._currentDerection  = 'asc';
    this._toolInited        = false;
    this._wd                = wd;
    this._roll              = roll;
    this._currentSortTool   = null;

    this._closeBtn          = null;

    this.setUsedTool(defaultSortTool);

    this._fullSortPopup     = {
        _init       : false,

        _field1ID   : null,
        _field1Val  : null,
        _field1Dir  : 'asc',

        _field2ID   : null,
        _field2Val  : null,
        _field2Dir  : 'asc',

        _field3ID   : null,
        _field3Val  : null,
        _field3Dir  : 'asc',

        _html       : null,

        _inUse      : false
    };

    var $this = this;
    this.cancelHadler = function () {
        $this.cancelSort();
    }

    if ( sortSelect && directionSelect ) {
        if ( typeof sortSelect === 'string' ) {
            this._sortSelect = document.getElementById(sortSelect);
        } else {
            this._sortSelect = sortSelect;
        }
        if ( typeof directionSelect === 'string' ) {
            this._directionSelect = document.getElementById(directionSelect);
        } else {
            this._directionSelect = directionSelect;
        }
    }

    this._sort = {};
    
    
    this._sort.created = {
            'win'           : 'WIN #',
            'start'         : 'Start Date',
            'project'       : 'Project',
            'route'         : 'Route',
            'site'          : 'Site',
            'city'          : 'City',
            'state'        : 'State',
            'zip'           : 'Zip',
            'paymax'		: 'Pay'
    };
	this._sort.published = {
	        'win'           : 'WIN #',
	        'start'         : 'Start Date',
	        'project'       : 'Project',
                'route'         : 'Route',
	        'site'          : 'Site',
	        'city'          : 'City',
	        'state'        : 'State',
	        'zip'           : 'Zip',
            'paymax'		: 'Pay',
            'bidcount'		: 'Qty_Applicants'
    };
	this._sort.deactivated = this._sort.published;
	
    this._sort.assigned = {
            'win'           : 'WIN #',
            'start'         : 'Start Date',
            'project'       : 'Project',
            'route'         : 'Route',
            'site'          : 'Site',
            'city'          : 'City',
            'state'         : 'State',
            'zip'           : 'Zip',
            'paymax'		: 'Pay',
            'techname' 		: 'Tech Name',
            'techphone' 	: 'Tech Phone',
            'techemail' 	: 'Tech Email',
            'accepted' 		: 'Tech Accepted',
            'confirmed' 	: 'Tech Confirmed'
    };
    this._sort.workdone = this._sort.assigned;
    this._sort.incomplete = this._sort.assigned;
    
    this._sort.approved = {
        'win'           : 'WIN #',
        'start'         : 'Start Date',
        'project'       : 'Project',
        'route'         : 'Route',
        'site'          : 'Site',
        'city'          : 'City',
        'state'         : 'State',
        'zip'           : 'Zip',
        'paymax'		: 'Pay',
        'techname' 		: 'Tech Name',
        'techphone' 	: 'Tech Phone',
        'techemail' 	: 'Tech Email',
        'accepted' 		: 'Tech Accepted',
        'confirmed' 	: 'Tech Confirmed',
        'final' 	: 'Pay Amount'
    };
    
    this._sort.all = this._sort.approved;
    
    this._sort.inaccounting = this._sort.approved;
    
    this._sort.completed = {
            'win'           : 'WIN #',
            'start'         : 'Start Date',
            'project'       : 'Project',
            'route'         : 'Route',
            'site'          : 'Site',
            'city'          : 'City',
            'state'         : 'State',
            'zip'           : 'Zip',
            'paymax'		: 'Pay',
            'techname' 		: 'Tech Name',
            'techphone' 	: 'Tech Phone',
            'techemail' 	: 'Tech Email',
            'accepted' 		: 'Tech Accepted',
            'confirmed' 	: 'Tech Confirmed',
            'final' 	: 'Pay Amount',
            'completed' : 'Complete Date',
            'invoice' : 'Invoice Date',
            'paid' : 'Paid Date'
        };

    
    this._sort_defaults = {
        created          : {sort: 'startdate', dir: 'asc'},
        published        : {sort: 'startdate', dir: 'asc'},
        assigned         : {sort: 'startdate', dir: 'asc'},
        approved         : {sort: 'startdate', dir: 'asc'},
        inaccounting         : {sort: 'startdate', dir: 'asc'},
        workdone         : {sort: 'start', dir: 'asc'},
        incomplete       : {sort: 'start', dir: 'desc'},
        all              : {sort: 'start', dir: 'desc'},
        deactivated      : {sort: 'start', dir: 'desc'},
        completed        : {sort: 'approved', dir: 'desc'},
        techavailable    : {sort: 'start', dir: 'asc'},
        techassigned     : {sort: 'start', dir: 'asc'},
        techworkdone     : {sort: 'start', dir: 'asc'},
        techapproved     : {sort: 'start', dir: 'desc'},
        //techinaccounting : {sort: 'start', dir: 'asc'},
        techpaid         : {sort: 'start', dir: 'desc'},
        techincomplete   : {sort: 'start', dir: 'asc'},
        techall          : {sort: 'start', dir: 'desc'}
    };

    this.initSortTool();
    this.prepareUsedTool();
    }
}

FSWidgetAdminSortTool.NAME          = 'FSWidgetAdminSortTool';
FSWidgetAdminSortTool.VERSION       = '0.1';
FSWidgetAdminSortTool.DESCRIPTION   = 'Class WidgetSortTool: Sort Tool for Dashboard';

FSWidgetAdminSortTool.prototype.name = function()
{
    return this.constructor.NAME;
}

FSWidgetAdminSortTool.prototype.version = function()
{
    return this.constructor.VERSION;
}

FSWidgetAdminSortTool.prototype.description = function()
{
    return this.constructor.DESCRIPTION;
}

FSWidgetAdminSortTool.prototype.prepareUsedTool = function ()
{
    if ( this.getUsedTool() === 'quick' ) {
        if ( !this.fullInUsage() && this._fullSortPopup._init ) {
            this.sort(this._fullSortPopup._field1Val);
            this.direction(this._fullSortPopup._field1Dir);

            var i;

            for (i=0; i<this._sortSelect.options.length; ++i) {
                if ( this._sortSelect.options[i].value === this.sort() ) {
                    this._sortSelect.options[i].selected = true;
                    break;
                }
            }
            for (i=0; i<this._directionSelect.options.length; ++i) {
                if ( this._directionSelect.options[i].value === this.direction() ) {
                    this._directionSelect.options[i].selected = true;
                    break;
                }
            }
        }
    } else if ( this.getUsedTool() === 'full' ) {
        this._fullSortPopup._field1Val = this._currentSort;
        this._fullSortPopup._field1Dir = this._currentDerection;
        this._fullSortPopup._field2Val = null;
        this._fullSortPopup._field2Dir = 'asc';
        this._fullSortPopup._field3Val = null;
        this._fullSortPopup._field3Dir = 'asc';

        this._sortSelect.options[0].selected = true;
        this._directionSelect.options[0].selected = true;
    }
}

FSWidgetAdminSortTool.prototype.fullInUsage = function ( bool )
{
    if ( bool !== undefined && bool !== null ) {
        this._fullSortPopup._inUse = ( bool ) ? true : false;
    }
    return this._fullSortPopup._inUse;
}

FSWidgetAdminSortTool.prototype.setUsedTool = function ( toolName )
{
    if ( typeof toolName !== 'string' || typeof toolName === 'string' && toolName !== 'full' ) {
        this._currentSortTool = 'quick';
    } else {
        this._currentSortTool = 'full';
    }
}

FSWidgetAdminSortTool.prototype.getUsedTool = function ()
{
    return this._currentSortTool;
}

FSWidgetAdminSortTool.prototype.sort = function ( val )
{
    if ( val !== undefined ) {
        this._currentSort = val;
    }
    return this._currentSort;
}

FSWidgetAdminSortTool.prototype.direction = function ( val )
{
    if ( val !== undefined ) {
        this._currentDerection = val;
    }
    return this._currentDerection;
}

FSWidgetAdminSortTool.prototype.setFullSortVal = function ( value, num )
{
    nam = parseInt(num, 10);

    if ( value === undefined || num < 1 || num > 3 )
        return;

    switch ( num ) {
        case 1: this._fullSortPopup._field1Val = value; break;
        case 2: this._fullSortPopup._field2Val = value; break;
        case 3: this._fullSortPopup._field3Val = value; break;
    }
}

FSWidgetAdminSortTool.prototype.setFullSortDir = function ( value, num )
{
    nam = parseInt(num, 10);

    if ( value === undefined || num < 1 || num > 3 )
        return;

    switch ( num ) {
        case 1: this._fullSortPopup._field1Dir = value; break;
        case 2: this._fullSortPopup._field2Dir = value; break;
        case 3: this._fullSortPopup._field3Dir = value; break;
    }
}

FSWidgetAdminSortTool.prototype.update = function ()
{
    if ( this._sortSelect && this._directionSelect ) {
        this.sort(this._sortSelect.options[this._sortSelect.selectedIndex].value);
        this.direction(this._directionSelect.options[this._directionSelect.selectedIndex].value);
    }
}

FSWidgetAdminSortTool.prototype.initSortTool = function () {
    if ( !this._sortSelect || !this._directionSelect || !this._wd )
        return;

    /* Save old quick sort when user going to other tab */
    //var oldSort = this.sort();
    //var oldDir = this.direction();
    /* Save old quick sort when user going to other tab */

    this._toolInited = true;
    this._sortSelect.options.length = 0;
    this._directionSelect.options.length = 0;

    this._sortSelect.options[0] = new Option('None', '', false, (this._currentSortTool === 'full') ? true : false);

    var i = 1;
    $this = this;
    /* Build new sorting items */
    $.each(this._sort[this._wd.currentTab], function (idx, val) {
        $this._sortSelect.options[i++] = new Option(val, idx, false, ($this._sort_defaults[$this._wd.currentTab] && $this._sort_defaults[$this._wd.currentTab].sort === idx && $this._currentSortTool === 'quick') ? true : false);
    });
    /* Build new sorting items */

    /* Build new direction items */
    if (this._sort_defaults[$this._wd.currentTab] && this._sort_defaults[$this._wd.currentTab].dir == 'desc') {
        this._directionSelect.options[0] = new Option('Ascending', 'asc', false, false);
        this._directionSelect.options[1] = new Option('Descending', 'desc', true, true);
    } else {
        this._directionSelect.options[0] = new Option('Ascending', 'asc', true, true);
        this._directionSelect.options[1] = new Option('Descending', 'desc', false, false);
    }
    /* Build new direction items */

    /* Restore old sort */
    //if ( oldSort ) {
        //for ( i = 0; i < this._sortSelect.options.length; ++i ) {
            //if ( oldSort === this._sortSelect.options[i].value ) {
                //this._sortSelect.options[i].selected = true;
            //}
        //}
        //for ( i = 0; i < this._directionSelect.options.length; ++i ) {
            //if ( oldDir === this._directionSelect.options[i].value ) {
                //this._directionSelect.options[i].selected = true;
            //}
        //}
    //}
    /* Restore old sort */

    this.update();
}

FSWidgetAdminSortTool.prototype.initSortPopup = function ()
{
	FSWidget.filterActivate(true);

    var i;
    var select1 = document.getElementById('_sort_select_' + this._fullSortPopup._field1ID);
    var select2 = document.getElementById('_sort_select_' + this._fullSortPopup._field2ID);
    var select3 = document.getElementById('_sort_select_' + this._fullSortPopup._field3ID);

    select1.options.length = select2.options.length = select3.options.length = 0;

    select2.options[0] = new Option('None', '', true, true);
    select3.options[0] = new Option('None', '', true, true);

    var $this = this;

    i = 0;
    $.each(this._sort[$this._wd.currentTab], function (idx, val) {
        select1.options[i++] = new Option(
            val,
            idx,
            ($this._sort_defaults[$this._wd.currentTab] && $this._sort_defaults[$this._wd.currentTab].sort === idx) ? true : false,
            ( $this._fullSortPopup._field1Val !== null ) ?
                (( idx === $this._fullSortPopup._field1Val ) ? true : false) :
                (( $this._sort_defaults[$this._wd.currentTab] && $this._sort_defaults[$this._wd.currentTab].sort === idx ) ? true : false));
    });

    i = 1;
    $.each(this._sort[$this._wd.currentTab], function (idx, val) {
        select2.options[i++] = new Option(
            val,
            idx,
            ($this._sort_defaults[$this._wd.currentTab] && $this._sort_defaults[$this._wd.currentTab].sort === idx) ? true : false,
            ( $this._fullSortPopup._field2Val !== null ) ?
                (( idx === $this._fullSortPopup._field2Val ) ? true : false) : false);
    });

    i = 1;
    $.each(this._sort[$this._wd.currentTab], function (idx, val) {
        select3.options[i++] = new Option(
            val,
            idx,
            ($this._sort_defaults[$this._wd.currentTab] && $this._sort_defaults[$this._wd.currentTab].sort === idx) ? true : false,
            ( $this._fullSortPopup._field3Val !== null ) ?
                (( idx === $this._fullSortPopup._field3Val ) ? true : false) : false);
    });

    if ( this._fullSortPopup._field1Dir === null || this._fullSortPopup._field1Dir === 'asc' ) {
        document.getElementById('_direction_asc_' + this._fullSortPopup._field1ID).checked = true;
    } else {
        document.getElementById('_direction_desc_' + this._fullSortPopup._field1ID).checked = true;
    }
    if ( this._fullSortPopup._field2Dir === null || this._fullSortPopup._field2Dir === 'asc' ) {
        document.getElementById('_direction_asc_' + this._fullSortPopup._field2ID).checked = true;
    } else {
        document.getElementById('_direction_desc_' + this._fullSortPopup._field2ID).checked = true;
    }
    if ( this._fullSortPopup._field3Dir === null || this._fullSortPopup._field3Dir === 'asc' ) {
        document.getElementById('_direction_asc_' + this._fullSortPopup._field3ID).checked = true;
    } else {
        document.getElementById('_direction_desc_' + this._fullSortPopup._field3ID).checked = true;
    }

    $(this._roll.closeBtn()).bind('click', this.cancelHadler);

    this.updateFullSort(select1);
}

FSWidgetAdminSortTool.prototype.getSort = function ( position )
{
    switch ( position ) {
        case 1:
            return ( this.fullInUsage() ) ? this._fullSortPopup._field1Val : this.sort();
        case 2:
            return ( this.fullInUsage() ) ? this._fullSortPopup._field2Val : null;
        case 3:
            return ( this.fullInUsage() ) ? this._fullSortPopup._field3Val : null;
        default:
            return null;
    }
}

FSWidgetAdminSortTool.prototype.getDirection = function ( position )
{
    switch ( position ) {
        case 1:
            return ( this.fullInUsage() && this._fullSortPopup._field1Val ) ? this._fullSortPopup._field1Dir : this.direction();
        case 2:
            return ( this.fullInUsage() && this._fullSortPopup._field2Val ) ? this._fullSortPopup._field2Dir : null;
        case 3:
            return ( this.fullInUsage() && this._fullSortPopup._field3Val ) ? this._fullSortPopup._field3Dir : null;
        default:
            return null;
    }
}

FSWidgetAdminSortTool.prototype.switchSortTools = function( tool )
{
    var oldTool = this.getUsedTool();
    this.setUsedTool(tool);

    if ( oldTool !== this.getUsedTool() ) {
        this.prepareUsedTool();
    }
}

/**
 *  applySort
 *
 *  @param string type quick|full SortTool type
 *  @return FALSE always
 */
FSWidgetAdminSortTool.prototype.applySort = function ( type )
{
	FSWidget.filterActivate(false);

    if ( typeof type === 'string' && type.toLowerCase() === 'quick' ) {
        this.switchSortTools('quick');
        this.fullInUsage(false);
        this.update();
        this._wd.show({
            tab     : this._wd.currentTab,
            params  : this._wd.getParams()
        });
    } else if ( typeof type === 'string' && type.toLowerCase() === 'full' ) {
        this.fullInUsage(true);
        this._wd.show({
            tab     : this._wd.currentTab,
            params  : this._wd.getParams()
        });
        this._roll.hide();
    }

    $(this._roll.closeBtn()).unbind('click', this.cancelHadler);

    return false;
}

FSWidgetAdminSortTool.prototype.cancelSort = function ()
{
    this._roll.hide();
    if ( !this.fullInUsage() ) {
        this.switchSortTools("quick");
    }

    if ( this._closeBtn !== null ) {
        $(this._closeBtn).remove();
        this._closeBtn = null;
    }

    FSWidget.filterActivate(false);

    $(this._roll.closeBtn()).unbind('click', this.cancelHadler);

    return false;
}

FSWidgetAdminSortTool.prototype.updateFullSort = function ( currentSortSelect )
{
    var select1 = document.getElementById('_sort_select_' + this._fullSortPopup._field1ID);
    var select2 = document.getElementById('_sort_select_' + this._fullSortPopup._field2ID);
    var select3 = document.getElementById('_sort_select_' + this._fullSortPopup._field3ID);

    $.each([select1, select2, select3], function (idx, item) {
        for ( var i=1; i<item.options.length; ++i ) {
            item.options[i].disabled = false;
        }
    });

    select2.options[select1.selectedIndex+1].disabled = true;
    select3.options[select1.selectedIndex+1].disabled = true;
    if ( select2.selectedIndex !== 0 ) {
        select1.options[select2.selectedIndex-1].disabled = true;
        select3.options[select2.selectedIndex].disabled = true;
    }
    if ( select3.selectedIndex !== 0 ) {
        select1.options[select3.selectedIndex-1].disabled = true;
        select2.options[select3.selectedIndex].disabled = true;
    }

    var first   = new RegExp(this._fullSortPopup._field1ID+'$');
    var second  = new RegExp(this._fullSortPopup._field2ID+'$');
    var third   = new RegExp(this._fullSortPopup._field3ID+'$');

    if ( first.test(currentSortSelect.id) ) {
        this.setFullSortVal(currentSortSelect.value, 1);
    } else if ( second.test(currentSortSelect.id) ) {
        this.setFullSortVal(currentSortSelect.value, 2);
    } else if ( third.test(currentSortSelect.id) ) {
        this.setFullSortVal(currentSortSelect.value, 3);
    }
}

/**
 *  makeFullSortPopup
 *
 *  Build html for Full Sort Tool
 *
 *  @return string HTML
 */
FSWidgetAdminSortTool.prototype.makeFullSortPopup = function ()
{
    if ( !this._fullSortPopup._init ) {
        this._fullSortPopup._init = true;

        var html;

        this._fullSortPopup._field1ID = '_fullSort_' + Math.floor(Math.random()*10001);
        this._fullSortPopup._field2ID = '_fullSort_' + Math.floor(Math.random()*10001);
        this._fullSortPopup._field3ID = '_fullSort_' + Math.floor(Math.random()*10001);

        html  = "<table class='sort_table' width='100%' cellspacing='0' cellpadding='0'>";

        html == "<form name='fullSortToolForm' method='post' action='/'>";
		html += "<col width=50%>";
		html += "<col width=50%>";
        html += "<tr>";
        html += "<td colspan='2'>Sort by</td>";
        html += "</tr>";

        html += "<tr class='even'>";
        html += "<td><select onchange='wd.sortTool().updateFullSort(this);' name='_sort_select_"+ this._fullSortPopup._field1ID +"' id='_sort_select_"+ this._fullSortPopup._field1ID +"' /></td>";
        html += "<td>";
        html += "<input type='radio' onclick='wd.sortTool().setFullSortDir(this.value, 1);' name='_direction_"+this._fullSortPopup._field1ID+"' id='_direction_asc_"+this._fullSortPopup._field1ID+"' value='asc' /> Ascending";
        html += "<br />";
        html += "<input type='radio' onclick='wd.sortTool().setFullSortDir(this.value, 1);'name='_direction_"+this._fullSortPopup._field1ID+"' id='_direction_desc_"+this._fullSortPopup._field1ID+"' value='desc' /> Descending";
        html += "</td>";
        html += "</tr>";

        html += "<tr>";
        html += "<td colspan='2'>Then by</td>";
        html += "</tr>";

        html += "<tr class='even'>";
        html += "<td><select onchange='wd.sortTool().updateFullSort(this)' name='_sort_select_"+ this._fullSortPopup._field2ID +"' id='_sort_select_"+ this._fullSortPopup._field2ID +"' /></td>";
        html += "<td>";
        html += "<input type='radio' onclick='wd.sortTool().setFullSortDir(this.value, 2);'name='_direction_"+this._fullSortPopup._field2ID+"' id='_direction_asc_"+this._fullSortPopup._field2ID+"' value='asc' /> Ascending";
        html += "<br />";
        html += "<input type='radio' onclick='wd.sortTool().setFullSortDir(this.value, 2);'name='_direction_"+this._fullSortPopup._field2ID+"' id='_direction_desc_"+this._fullSortPopup._field2ID+"' value='desc' /> Descending";
        html += "</td>";
        html += "</tr>";

        html += "<tr>";
        html += "<td colspan='2'>Then by</td>";
        html += "</tr>";

        html += "<tr class='even'>";
        html += "<td><select onchange='wd.sortTool().updateFullSort(this)' name='_sort_select_"+ this._fullSortPopup._field3ID +"' id='_sort_select_"+ this._fullSortPopup._field3ID +"' /></td>";
        html += "<td>";
        html += "<input type='radio' onclick='wd.sortTool().setFullSortDir(this.value, 3);'name='_direction_"+this._fullSortPopup._field3ID+"' id='_direction_asc_"+this._fullSortPopup._field3ID+"' value='asc' /> Ascending";
        html += "<br />";
        html += "<input type='radio' onclick='wd.sortTool().setFullSortDir(this.value, 3);'name='_direction_"+this._fullSortPopup._field3ID+"' id='_direction_desc_"+this._fullSortPopup._field3ID+"' value='desc' /> Descending";
        html += "</td>";
        html += "</tr>";

        html += "<tr>";
        html += "<td colspan='2' align='center' class='submit_buttons'>";
        html += "<input type='button' value='Ok' class='link_button_popup' onclick='return wd.sortTool().applySort(\"full\");' />";
        html += "<input type='button' value='Cancel' class='link_button_popup' onclick='return wd.sortTool().cancelSort();' />";
        html += "</td>";
        html += "</tr>";

        html += "</form>";

        html += "</table>";

        this._fullSortPopup._html = html;
    }

    return this._fullSortPopup._html;
}

