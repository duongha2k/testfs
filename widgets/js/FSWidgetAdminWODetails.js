/**
 * FSWidgetWODetails
 *
 * @param Object|null params
 * @access public
 * @return void
 */
function FSWidgetWODetails ( params, roll ) {
    FSWidget.call(this, params);

    this.params.POPUP.avaliablePopups = {
        filedownload    : {module:'admindashboard',controller:'do',action:'file-wos-documents-download'},
        documents       : {module:'admindashboard',controller:'details',action:'project-documents'},
        bcatdocuments       : {
            module:'dashboard',
            controller:'sections',
            action:'bcat-documents'
        }
    };
    this.params.WIDGET.avaliableTabs = {
        index : {module:'admindashboard',controller:'details',action:'index'},
        navigation : {module:'admindashboard',controller:'details',action:'navigation'}
    };
    this._roll = roll;

    this.waitImage = new Image();
    this.waitImage.src = "/widgets/images/progress.gif";     //  pre load wait image
    this.waitImage.width = 190;
    this.waitImage.height = 14;

    this._activeuploads = 0;
    this._updatestatus = {success:1, errors:[]};
    this._redirect = false;
    this._reftab = params.reftab;

    this._customfunctions = params.customfunctions || {};
}

FSWidgetWODetails.prototype = new FSWidget();
FSWidgetWODetails.prototype.constructor = FSWidgetWODetails;

FSWidgetWODetails.NAME          = 'FSWidgetWODetails';
FSWidgetWODetails.VERSION       = '0.1';
FSWidgetWODetails.DESCRIPTION   = 'Class FSWidgetWODetails';

FSWidgetWODetails.prototype.roll = function() {
    return this._roll;
}

/**
 *  open
 */
FSWidgetWODetails.prototype.open = function(options) {
    var p = options || {};
    var queryString = '/';
    var _url;
    var noSSL = false;

    if ( p.params !== undefined && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params )
            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                } else if ( key === 'projects' ) {
                    var ids = '';
                    for ( var pr=0; pr < p.params[key].length; ++pr ) {
                        ids += p.params[key][pr];
                        if ( pr + 1 < p.params[key].length ) {
                            ids += '|';
                        }
                    }
                    queryString += 'projects/' + ids + '/';
                }
            } else if (p.params[key] === false || p.params[key] === true) {
                /* ok */
            } else if ( p.params[key] !== null && p.params[key] !== "" && p.params[key].toLowerCase() !== 'none' ) {
                if (key === "download") {
                    noSSL = true;
                } else if ( key === 'start_date' || key === 'end_date' ) {
                    p.params[key] = p.params[key].replace(/\//g, '_');
                }
                queryString += key + '/' + p.params[key] + '/';
            }
    }
    try {
        _url = this._makeUrl(p.tab, queryString, noSSL);
    } catch ( e ) {
        return false;
    }
    return location = _url;
}

/**
 *  show
 *
 *  Function display widget
 *
 *  @param Object|null options
 */

FSWidgetWODetails.prototype.show = function(options) {
    //if ( FSWidget.isFilterActive() ) return;

    var p = options || {tab:this.currentTab,params:this.getParams()};

    var queryString = '/';
    var _url;

    // TODO params needs be reconfigured
    if ( p.params !== undefined && p.params !== null && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params ) {
            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] === true || p.params[key] === false ) {
                if ( p.params[key] === false ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] !== null && p.params[key] !== "" && p.params[key] !== 'none' ) {
                //queryString += key + '/' + p.params[key] + '/';
            } else {
                delete p.params[key];
            }
        }
    } else {
        p.params = {};
    }

    try {
        if ( p.tab !== undefined && p.tab !== null ) {
            this.currentTab = p.tab;
            _url = this._makeUrl(p.tab, queryString);
            var idx;
            for ( idx in this.params.WIDGET.avaliableTabs ) {
                $('#'+idx).toggleClass('current', idx === this.currentTab);
            }
        } else if ( p.popup !== undefined && p.popup !== null ) {
            _url = this._makeUrl(p.popup, queryString);
        }
    } catch ( e ) {
        return false;
    }

    this.showPreloader();

    if ( this.tabOpenXhrRequest ) {
        this.tabOpenXhrRequest.abort();
        this.tabOpenXhrRequest = null;
    }

    version = $('#dashboardVersion').val();
    if(version != '' && version == 'Lite'){
	version = "Lite";
        p.params.version = version;
        $('#deactivated').addClass("displayNone");
    } else {
        version = "Full";
//        var defVer = FSWidget.getCookie("dashboardVersionDefault");
        p.params.version = version;
        $('#deactivated').removeClass("displayNone");
    }

    $('#dashboardVersion').val(version);
    setCookie("dashboardVersionDefault", version);
    this.tabOpenXhrRequest = jQuery.ajax({
        url : _url,
        data : p.params,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/admin/logIn.php";
            } else {
                FSWidget.fillContainer("Sorry, but an error occurred. No results found.", this);
            }
            this.tabOpenXhrRequest = null;
        },
        success : function(data, status, xhr) {
            $('body').css('cursor', '');
            FSWidget.fillContainer(data, this)
            calendarInit();
             _wosDetails= wosDetails.CreateObject({
                id:'_wosDetails_Instance',
                detailsWidget:this,
                params: p.params
            });    
            $("#edit_SiteNumber").unbind("change",this.autoFillSite).bind("change",this.autoFillSite);
        }
    });

}

FSWidgetWODetails.prototype.showPreloader = function() {
    if (this._customfunctions.showPreloader !== undefined && typeof this._customfunctions.showPreloader === 'function' ) {
        this._customfunctions.showPreloader();
    }else{
        $('body').css('cursor', 'wait');
        var img = "<div class='tCenter' ><img style='margin:10% auto;' alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></div>";
        FSWidget.fillContainer(img, this);
    }
}

FSWidgetWODetails.prototype.updateFinalPay = function( name ) {
    var cancelAction = function () {
        return false;
    };

    var doFinalPayUpdate = function () {
        $("#edit_Add_Pay_AuthBy").val(name);
        obj.updateFinalPay();
    };
	
	var recalcBasePay = function() {
		$("#edit_baseTechPay").val("");
		doFinalPayUpdate();
	};
	
    if ( name != undefined ) {
        obj = this;

        $('#edit_AbortFee').unbind('change').change(function() {
            $("span.required_abortFee").each(function() {
                this.style.display = $('#edit_AbortFee').attr("checked") ? 'inline' : 'none';
            });
            doFinalPayUpdate();
        });

		$.map(['edit_calculatedTechHrs','edit_Amount_Per','edit_Qty_Devices','edit_Tech_Bid_Amount'], function( id ) {
			$('#'+id).unbind('change').change(recalcBasePay);
		});

		$.map(['edit_TechMarkedComplete','edit_CallClosed', 'edit_baseTechPay', 'edit_OutofScope_Amount','edit_TripCharge','edit_MileageReimbursement','edit_MaterialsReimbursement','edit_AbortFeeAmount','edit_Additional_Pay_Amount'], function( id ) {
            $('#'+id).unbind('change').change(doFinalPayUpdate);
        });
	return; // don't run calc on ready to avoid issue with net pay showing something different than what's in the db
    }
    //TODO: isInvoiced
    var mult = $("#edit_Amount_Per option:selected").val();
    if ( mult == "Device" || mult == "Visit")
    {
        mult = parseFloat($("#edit_Qty_Devices").val());
    }
    else
    {
        if ( mult == "Hour" )
        {
            mult = parseFloat(($("#edit_calculatedTechHrs").val()=="")?0:$("#edit_calculatedTechHrs").val());
        }
        else {mult = 1;}}
            if ($("#edit_baseTechPay").val() == "")
            {
	            $("#edit_baseTechPay").val(mult*$("#edit_Tech_Bid_Amount").val());
	            $("#edit_baseTechPay").formatCurrency({groupDigits:false, symbol:'', negativeFormat: '-%n'});
            }

        if (($("#edit_MissingComments").val() == "" && !$("#edit_TechMarkedComplete").attr("checked")) /*|| isInvoiced*/)
	        return;

        //Update baseTechPay
        var sum = 0;

        if ($("#edit_AbortFee").attr("checked"))
            sum = $("#edit_AbortFeeAmount").val() ? parseFloat($("#edit_AbortFeeAmount").val()):0;
        else
        {
            sum = $("#edit_baseTechPay").val() ? parseFloat($("#edit_baseTechPay").val()):0;

	        $.map(['edit_OutofScope_Amount','edit_TripCharge','edit_MileageReimbursement','edit_MaterialsReimbursement','edit_Additional_Pay_Amount'],function(id) {
	        sum+=$('#'+id).val()?parseFloat($('#'+id).val()):0;
	        });
        }

        /* Maybe API can handle?*/
//        if($('#edit_PayAmount').val()=="")
//    {
		var expRepReimburse = parseFloat ($("#edit_Expense_Report_Reimbursement").val ());

		$('#edit_PayAmount').val(sum + expRepReimburse);
        $('#edit_PayAmount').formatCurrency({groupDigits:false, symbol:'', negativeFormat: '-%n'});
		deductPercent = parseFloat($("#edit_PcntDeductPercent").val());
		deductPercent = isNaN(deductPercent) || !deductPercent ? 0 : deductPercent;
	    var sumDeduct = (sum + expRepReimburse) * (1 - deductPercent);

        $('#edit_Net_Pay_Amount').val(sumDeduct);
        $('#edit_Net_Pay_Amount').formatCurrency({groupDigits:false, symbol:'', negativeFormat: '-%n'});
//    }
}

FSWidgetWODetails.prototype.updateDetails = function() {
    this.showStartPopup();
    this._redirect =false;
    this._updatestatus = {success:1, errors:[]};
    params = $('#updateDetailsForm').serialize();
    $.ajax({
        url         : '/widgets/admindashboard/details/update/',
        dataType    : 'json',
        data        : params,
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            this._updatestatus = data;
            if (data.success) {
                for (i=1;i<4;i++) {
                    if ($('#Pic'+i).val()) {
                      this.uploadFile(i);
                    }
                }
                if (this._activeuploads == 0) {
                    this.showMessages(this._updatestatus);
                }
                //Refresh dashboard
                if (window.opener && window.opener.wd) {
                	var wd = window.opener.wd;
                	wd.show({tab: wd.currentTab, params: wd.getParams()});
                }
            }else{
                this.showMessages(this._updatestatus);
            }
        },
        error      : function (data, status, xhr) {
        	//console.log (data, status, xhr);
        }
    });
}

FSWidgetWODetails.prototype.autofillProjectInfo = function (pid) {
    if (!pid) {
        return;
    }
    $.ajax({
        url         : '/widgets/admindashboard/js/project-info-ex/',
        dataType    : 'json',
        data        : {id:pid},
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            if (data) {
                //AUTOFILL
                for(index in data) {
                    var element = $('#edit_'+index);
                    var value = data[index]?data[index]:'';
                    if(index == 'PcntDeductPercent')
                    {
                        if(value == "0" || value == "")
                        {
                            jQuery("#lablePcntDeductPercent").html('Deduct FS Service Fee from Tech');
                            value = "0";
                        }  
                        else if(value*100 == 100)
                        {
                            jQuery("#lablePcntDeductPercent").html('This Work Order will be paid outside of the FieldSolutions Platform');
                        }
                        else{
                            jQuery("#lablePcntDeductPercent").html('Deduct '+ value*100+'% FS Service Fee from Tech');
                        }  
                        jQuery("#edit_PcntDeductPercent").val(value);
                    }    
                    else if(index == 'PcntDeduct')
                    {
                        if(data['PcntDeductPercent'] != "" && data['PcntDeductPercent'] != "0")
                        {
                            jQuery("#edit_PcntDeduct").attr("checked",true);
                            jQuery("#PcntDeductV2").attr("checked",true);
                        }    
                        else
                        {
                            jQuery("#edit_PcntDeduct").attr("checked",false);
                            jQuery("#PcntDeductV2").attr("checked",false);
                        }    
                    }    
                    else if (element.size()>0 && index != 'Project_ID') {
                        if (element.attr('type') == 'checkbox') {
                            if(value!=='False') {
                                element.attr('checked','checked');
                            }else{
                                element.removeAttr('checked');
                            }

                        }else if (element.get(0).tagName=='INPUT' || element.get(0).tagName=='SELECT' || element.get(0).tagName=='TEXTAREA'){
                            element.val(value);
                            element.change();
                        }else{
                            element.html(value);
                        }
                    }
                    count = data['SiteCount'];
                    this.selectSiteButtonEnable(count && count > 0);
                }
            }
        }
    });
    
    $.ajax({
        url         : '/widgets/admindashboard/js/project-info/',
        dataType    : 'json',
        data        : {id:pid},
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            if (data) {
                //AUTOFILL
                for(index in data) {
                    var element = $('#edit_'+index);
                    var value = data[index]?data[index]:'';
                    if (element.size()>0 && index != 'Project_ID') {
                        if (element.attr('type') == 'checkbox') {
                            if(value!=='False') {
                                element.attr('checked','checked');
                            }else{
                                element.removeAttr('checked');
                            }

                        }else if (element.get(0).tagName=='INPUT' || element.get(0).tagName=='SELECT' || element.get(0).tagName=='TEXTAREA'){
                            element.val(value);
                            element.change();
                        }else{
                            element.html(value);
                        }
                    }
                }
                //update missing fields.
                if (data.isWorkOrdersFirstBidder === 'False') {
                    $("#edit_isnotWorkOrdersFirstBidder").attr('checked','checked');
                    $("#edit_isWorkOrdersFirstBidder").removeAttr('checked');
                }else{
                    $("#edit_isnotWorkOrdersFirstBidder").removeAttr('checked');
                }
                if (data.isProjectAutoAssign === 'False') {
                    $("#edit_isProjectAutoAssign").removeAttr('checked');
                }else{
                    $("#edit_isProjectAutoAssign").attr('checked','checked');
                }
                if (data.NotifyOnAutoAssign === 'False') {
                    $("#edit_NotifyOnAutoAssign").removeAttr('checked');
                }else{
                    $("#edit_NotifyOnAutoAssign").attr('checked','checked');
                }
                if (data.P2TPreferredOnly === 'False') {
                    $("#edit_notP2TPreferredOnly").attr('checked','checked');
                    $("#edit_P2TPreferredOnly").removeAttr('checked');
                }else{
                    $("#edit_notP2TPreferredOnly").removeAttr('checked');
                }
                if (data.FixedBid === 'False') {
                    $("#edit_FixedBidNotice").addClass('displayNone');
                }else{
                    $("#edit_FixedBidNotice").removeClass('displayNone');
                }

                // Set Up Accept replies
                if ( data['RecruitmentFromEmail'] != 'no-replies@fieldsolutions.us' ) {
                    $("#edit_AcceptReplies").attr('checked','checked');
                } else {
                    $("#edit_AcceptReplies").removeAttr('checked');
                }

                //P2T Time remaining
                var minutes = data['MinutesRemainPublished'];
                $("#edit_P2TM").val(minutes%60);
                minutes -= minutes%60;
                $("#edit_P2TH").val(minutes%1440/60);
                minutes -= minutes%1440;
                $("#edit_P2TD").val(minutes/1440);

            }

        }
    });
}

FSWidgetWODetails.prototype.createWO = function() {
    this.showStartPopup();
    this._updatestatus = {success:1, errors:[]};
    params = $('#createDetailsForm').serialize();
    $.ajax({
        url         : '/widgets/admindashboard/details/do-create/',
        dataType    : 'json',
        data        : params,
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            this._updatestatus = data;
            if (data.success) {
                this._redirect = true;
                this.params.win = data.win;

                for (i=1;i<4;i++) {
                    if ($('#Pic'+i).val()) {
                        //update wo param.
                      this.uploadFile(i);
                    }
                }
                if (this._activeuploads == 0) {
                    this.showMessages(this._updatestatus);
                }
            }else{
                this.showMessages(this._updatestatus);
            }
        }
    });
}

FSWidgetWODetails.prototype.showStartPopup = function() {
    var html = "<div class=\"tCenter\"><img alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></div>";
    this._roll.autohide(false);
    var opt = {
        width       : 300,
        height      : '',
        position    : 'middle',
        title       : 'Processing',
        body        : html
    };
    return this._roll.showNotAjax(null, null, opt);
}

FSWidgetWODetails.prototype.showMessages = function(data) {
    var html = '';
    if (this._redirect || data.success) { //always refresh page on success
        window.location = '/admin/wosDetails.php?v='+window._company+'&tab='+this._reftab+'&id='+this.params.win;
    }
    if (data.success) {
        html += '<div class="success"><ul><li>Success!</li></ul></div>';
    }else{
        html+='<div class="errors"><ul>';
        for (i=0;i<data.errors.length;i++) {
            html+='<li>'+data.errors[i]+'</li>';
        }
        html+='</ul></div>';
    }
    html+='<div class="tCenter"><input type="button" class="link_button_popup" onclick="detailsWidget.roll().hide();" value="Ok" /></div>';
    this._roll.autohide(false);
    var opt = {
        width       : 400,
        height      : '',
        position    : 'middle',
        title       : '',
        body        : html
    };
    if (data.success) {
        opt.width = 300;
    }
    return this._roll.showNotAjax(null, null, opt);
}
FSWidgetWODetails.prototype.uploadFile = function(id){
    this._activeuploads++;
    $.ajaxFileUpload({
        url             : "/widgets/admindashboard/details/ajax-upload-file/win/"+this.params.win+"/file/" + id + "/",
        secureuri       : false,
        fileElementId   : "Pic" + id,
        dataType        : 'json',
        context         : this,
        cache           : false,
        success         : function (data, status, xhr) {
            this.context._activeuploads--;
            if (!data.success) {
                this.context._updatestatus.success = 0;
                if (!this.context._updatestatus.errors) {
                    this.context._updatestatus.errors = [];
                }
                for (i=0;i<data.errors.length;i++) {
                    this.context._updatestatus.errors.push(data.errors[i]);
                }
            }

            if (this.context._activeuploads == 0) {
                this.context.showMessages(this.context._updatestatus);
            }
        }
    });
}

FSWidgetWODetails.prototype.siteLookup = function() {
	//openPopupWin('siteView.php?v=' + window._company + '&project_id=' + $("#edit_Project_ID").val() + '&select_mode=' + 1,'MyWindow');
        
        newwindowsiteLookup=window.open( 'siteView.php?v=' + window._company + '&project_id=' + $("#edit_Project_ID").val() + '&select_mode=' + 1
            , 'MyWindow'
            , 'height=400'
            +',width=700'
            +',left=100'
            +',top=100'
        );
        newwindowsiteLookup.focus();
}
FSWidgetWODetails.prototype.selectSiteButtonEnable = function(flag) {
	$("#btn_selectsite").unbind('click');
	if (!flag) {
		$("#btn_selectsite").click(function () {});
		$("#img_selectsite").attr('src', '/widgets/css/images/btn_orange_selectsite_disabled.png');
		$("#selectsite_msg").show ();
	}
	else {
		$("#btn_selectsite").click(detailsWidget.siteLookup);
		$("#img_selectsite").attr('src', '/widgets/css/images/btn_orange_selectsite.png');
		$("#selectsite_msg").hide ();
	}
}

FSWidgetWODetails.prototype.autoFillSite = function () {
    $.ajax({
        url         : '/widgets/dashboard/sitelist/get-site',
        dataType    : 'json',
        data        : {
            SiteNumber: $("#edit_SiteNumber").val(),
            Project_ID: $("#edit_Project_ID").val(),
            company: window._company
        },
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            if (data) {
                for (index in data) {
                    var value = data[index]?data[index]:'';
                    if (index == 'State' || index == 'Project_ID' || index == 'SiteNumber' || index == 'Country') continue;
                    var element = $('#edit_'+index);
                    element.val(value);
                    element.change();
                }
                $('#edit_State').val(data['State']);
                this._queueSiteState = data['State'];
                $('#edit_Country').val(data['Country']);
                FSWidgetWODetails.prototype.onChangeCountry();
            }
        }
    });	
}
FSWidgetWODetails.prototype.onChangeCountry = function () {
    $.ajax({
        url         : '/widgets/dashboard/js/state-info/',
        dataType    : 'json',
        data        : {country: $("#edit_Country").children("option:selected").val()},
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            if (data) {
                //AUTOFILL
				var option = '';
				option += '<option selected="selected" value="">Select State</option>';
                for(index in data) {
					option += '<option value="' + index + '">' + data[index] + '</option>';
				}
				$("#edit_State").html(option);
				if (this._queueSiteState) $('#edit_State').val(this._queueSiteState);
				this._queueSiteState = null;
			}
        }
    });	
}

FSWidgetWODetails.prototype.initSiteNumberAutoComplete = function () {
    $( "#edit_SiteNumber" ).autocomplete({
        source: function (request, callback) {
            $.ajax({
                url: '/widgets/dashboard/sitelist/site-search',
                data: {
                    term: request['term'], 
                    project_id: $("#edit_Project_ID").val(), 
                    company: window._company
                    },
                type: 'POST',
                dataType: 'json',
                error: function() {
                    callback({});
                },
                success: function(data) {
                    callback(data);
                }
            });
        },
        select:
        function(event, ui) {
            $("#edit_SiteNumber").val(ui.item.value);
            $("#edit_SiteNumber").blur();
            detailsWidget.autoFillSite();
        },
        change: function (event, ui) {
            FSWidgetWODetails.prototype.autoFillSite();
        }
    });
}
FSWidgetWODetails.prototype.saveSite = function (onsuccess) {
    this._roll.autohide(true);
    var html = '<div>Site Number required to save</div><div class="tCenter"><input type="button" class="link_button_popup" onclick="detailsWidget.roll().hide();" value="Ok" /></div>';
    var opt = {
        width       : 300,
        height      : '',
        position    : 'middle',
        title       : '',
        body        : html
    };
    if ($("#edit_SiteNumber").val() == '') {
	this._roll.showNotAjax(null, null, opt);
	return;
    }
    this.showStartPopup();
    $.ajax({
        url         : '/widgets/dashboard/sitelist/save-site',
        dataType    : 'json',
        data        : {
		SiteNumber: $("#edit_SiteNumber").val(),
		SiteName: $("#edit_SiteName").val(),
		Site_Contact_Name: $("#edit_Site_Contact_Name").val(),
		Address: $("#edit_Address").val(),
		City: $("#edit_City").val(),
		State: $("#edit_State").val(),
		Zipcode: $("#edit_Zipcode").val(),
		Country: $("#edit_Country").val(),
		SitePhone: $("#edit_SitePhone").val(),
		SiteFax: $("#edit_SiteFax").val(),
		SiteEmail: $("#edit_SiteEmail").val(),
		Project_ID: $("#edit_Project_ID").val()
	},
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
		if (data) {
                    if(typeof(onsuccess) == 'function'){
                        onsuccess();
                        this._roll.hide();
                        return false;
                    }
		    var html = '<div>Site saved to project site list</div><div class="tCenter"><input type="button" class="link_button_popup" onclick="detailsWidget.roll().hide();" value="Ok" /></div>';
		    var opt = {
		        width       : 300,
		        height      : '',
		        position    : 'middle',
		        title       : '',
		        body        : html
		    };
			this._roll.showNotAjax(null, null, opt);
		}
        }
    });	
}

