
//-----------------------------------------------------------------------------
/**
 * @fileoverview Simple fly-out module.  Works on right-side only
 *
 *	Dependencies: jQuery 1.4.2, FSCoreClasses.js, FSModelViewController.js
 * 
 * @author Jason K. Flaherty
 */

//-----------------------------------------------------------------------------
/**
 * @class View sets default elements and provides functionality to display
 *	and make adjustments.
 * @extends FSView
 */
var FSFlyOutView = (function () {
	var _parent = FSView;

	var FSFlyOutView = function (mappings, formatting) {
		var _height = 219;

		_parent.call (this, 
			this.combineMappings (mappings, {
				container: "#suggested_pay_panel",
				panel: "#suggested_pay",
				tab: "#suggested_pay_tab",
				arrows: ".suggested_pay_arrow"
			}),
			formatting
		);

		this.show = function () {
			var panel_pos1 = (jQuery (window).width () - 30);

			this.getElement ("panel").css ({
				"display": "block",
				"position": "fixed",
				"left": panel_pos1 + "px",
				"top": _height + "px"
			});
		};

		this.setHeight = function (value) {
			this.getElement ("panel").css ({ "top": value + "px" });

			_height = value;
		};
	};

	FSFlyOutView.prototype = new _parent ();

	return FSFlyOutView;
}) ();

//-----------------------------------------------------------------------------
/**
 * @class Places module and binds click events for fly-out actions
 * @extends FSController
 */
var FSFlyOutController = (function () {
	var _parent = FSController;

	var FSFlyOutController = function (view) {
		var window_w;
		var panel;
		var panel_tab;
		var panel_tab_arrows;
		var panel_pos1;
		var panel_pos2;

		_parent.call (this, view);

		//Event handlers
		var _flyout_handler, _flyin_handler;

		_flyout_handler = function (event) {
			panel_pos2 = panel_pos1 - view.getElement("container").width ();
			panel_tab.unbind ("click");
			panel.stop ().animate ({ 'left': panel_pos2 + 'px' }, 300, function () {
				panel_tab_arrows.attr ("src", "/widgets/images/popup/arrow_blue_right_9x16.png");
				panel_tab.click (_flyin_handler);
			});
		}

		_flyin_handler = function (event) {
			panel_tab.unbind ("click");
			panel.stop ().animate ({ 'left': panel_pos1 + 'px' }, 300, function () {
				panel_tab_arrows.attr ("src", "/widgets/images/popup/arrow_blue_left_9x16.png");
				panel_tab.click (_flyout_handler);
			});
		}

		if (view) {
			window_w			= jQuery (window).width ();
			panel				= view.getElement("panel");
			panel_tab			= view.getElement("tab");
			panel_tab_arrows	= view.getElement("arrows");
			panel_pos1			= (window_w - 30);

			panel_tab.click (_flyout_handler);
		}
	};

	FSFlyOutController.prototype = new _parent ();

	return FSFlyOutController;
}) ();

