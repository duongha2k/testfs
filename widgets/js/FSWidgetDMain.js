/**
 * FSWidgetDMain
 *
 * @param Object|null params
 * @access public
 * @return void
 */
function FSWidgetDMain ( params ) {
    FSWidgetDashboard.call(this, params);

    this.params.POPUP.avaliablePopups = {};
    this.params.WIDGET.avaliableTabs = {
        navigation      :   {module:'dashboard',controller:'landing',action:'navigation'},
        published       :   {module:'dashboard',controller:'landing',action:'published'},
        assigned        :   {module:'dashboard',controller:'landing',action:'assigned'},
        workdone        :   {module:'dashboard',controller:'landing',action:'workdone'},
        alertemails     :   {module:'dashboard',controller:'landing',action:'alertemails'},
        workdonesmall   :   {module:'dashboard',controller:'landing',action:'workdone-small'}
    };
}

FSWidgetDMain.prototype = new FSWidgetDashboard();
FSWidgetDMain.prototype.constructor = FSWidgetDMain;

FSWidgetDMain.NAME          = 'FSWidgetDMain';
FSWidgetDMain.VERSION       = '0.1';
FSWidgetDMain.DESCRIPTION   = 'Class FSWidgetDMain';

/**
 *  show
 *
 *  Function display widget
 *
 *  @param Object|null options
 */
FSWidgetDashboard.prototype.show = function(options) {

    if ( FSWidget.isFilterActive() ) return;

    var p = options || {tab:this.currentTab,params:this.getParams()};
    var queryString = '/';
    var _url;

    // TODO params needs be reconfigured
    if ( p.params !== undefined && p.params !== null && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params ) {
            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] === true || p.params[key] === false ) {
                if ( p.params[key] === false ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] !== null && p.params[key] !== "" && p.params[key].toLowerCase() !== 'none' ) {
                //queryString += key + '/' + p.params[key] + '/';
            } else {
                delete p.params[key];
            }
        }
    } else {
        p.params = {};
    }

    try {
        if ( p.tab !== undefined && p.tab !== null ) {
            this.currentTab = p.tab;
            _url = this._makeUrl(p.tab, queryString);
            var idx;
            for ( idx in this.params.WIDGET.avaliableTabs ) {
                $('#'+idx).toggleClass('current', idx === this.currentTab);
            }
        } else if ( p.popup !== undefined && p.popup !== null ) {
            _url = this._makeUrl(p.popup, queryString);
        }
    } catch ( e ) {
        return false;
    }

    $('body').css('cursor', 'wait');
    var img = "<img style='margin:0 40%;' alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' />";
    FSWidget.fillContainer(img, this);

    if ( this.tabOpenXhrRequest ) {
        this.tabOpenXhrRequest.abort();
        this.tabOpenXhrRequest = null;
    }

    version = $('#dashboardVersion').val();
    if(version != '' && version == 'Lite'){
	version = "Lite";
        p.params.version = version;
        $('#deactivated').addClass("displayNone");
    } else {
        version = "Full";
//        var defVer = FSWidget.getCookie("dashboardVersionDefault");
        p.params.version = version;
        $('#deactivated').removeClass("displayNone");
    }

    $('#dashboardVersion').val(version);
    setCookie("dashboardVersionDefault", version);

    this.tabOpenXhrRequest = jQuery.ajax({
        url : _url,
        data : p.params,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/clients/logIn.php";
            } else {
                FSWidget.fillContainer("Sorry, but an error occurred. No results found.", this);
            }
            this.tabOpenXhrRequest = null;
        },
        success : function(data, status, xhr) {
            $('body').css('cursor', '');
            FSWidget.fillContainer(data, this);
            this.tabOpenXhrRequest = null;
            if (this.currentTab == 'assigned') {
                $(".rePublishCheckbox").click(function() {
                    var id = $(this).attr('id');
                    var matches = /^rePublishCheckbox_(\d+)$/.exec(id);
                    var win = matches[1];

                    if ($(this).attr('checked')) {
                        $("#rePublishOptions_" + win).removeClass('displayNone');
                    } else {
                        $("#rePublishOptions_" + win).addClass('displayNone');
                    }
                });
            }
        }
    });
}

