/**
 * FSWidgetCreateQuickTicket
 *
 * @param Object|null params
 * @access public
 * @return void
 * @author Alex Scherba
 */
function FSWidgetCreateQuickTicket ( params, roll ) {
    FSWidget.call(this, params);

    this.params.POPUP.avaliablePopups = {
        filedownload    : {module:'dashboard',controller:'do',action:'file-wos-documents-download'},
        documents       : {module:'dashboard',controller:'details',action:'project-documents'}
    };
    this.params.WIDGET.avaliableTabs = {
        create : {module:'dashboard',controller:'customer',action:'create'}
    };
    this._roll = roll;

    this.waitImage = new Image();
    this.waitImage.src = "/widgets/images/progress.gif";     //  pre load wait image
    this.waitImage.width = 190;
    this.waitImage.height = 14;

    this._activeupload = 0;
    this._uploadsCounter = 0;
    this._updatestatus = {success:1, errors:[]};
    this._redirect = false;
    this._reftab = params.reftab;

    this._customfunctions = params.customfunctions || {};
}

FSWidgetCreateQuickTicket.prototype = new FSWidget();
FSWidgetCreateQuickTicket.prototype.constructor = FSWidgetCreateQuickTicket;

FSWidgetCreateQuickTicket.NAME          = 'FSWidgetCreateQuickTicket';
FSWidgetCreateQuickTicket.VERSION       = '0.1';
FSWidgetCreateQuickTicket.DESCRIPTION   = 'Class FSWidgetCreateQuickTicket';

FSWidgetCreateQuickTicket.prototype.roll = function() {
    return this._roll;
}

/**
 *  open
 */
FSWidgetCreateQuickTicket.prototype.open = function(options) {
    var p = options || {};
    var queryString = '/';
    var _url;
    var noSSL = false;

    if ( p.params !== undefined && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params )
            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                } else if ( key === 'projects' ) {
                    var ids = '';
                    for ( var pr=0; pr < p.params[key].length; ++pr ) {
                        ids += p.params[key][pr];
                        if ( pr + 1 < p.params[key].length ) {
                            ids += '|';
                        }
                    }
                    queryString += 'projects/' + ids + '/';
                }
            } else if (p.params[key] === false || p.params[key] === true) {
                /* ok */
            } else if ( p.params[key] !== null && p.params[key] !== "" && p.params[key].toLowerCase() !== 'none' ) {
                if (key === "download") {
                    noSSL = true;
                } else if ( key === 'start_date' || key === 'end_date' ) {
                    p.params[key] = p.params[key].replace(/\//g, '_');
                }
                queryString += key + '/' + p.params[key] + '/';
            }
    }
    try {
        _url = this._makeUrl(p.tab, queryString, noSSL);
    } catch ( e ) {
        return false;
    }
    return location = _url;
}

/**
 *  show
 *
 *  Function display widget
 *
 *  @param Object|null options
 */

FSWidgetCreateQuickTicket.prototype.show = function(options) {
    var p = options || {tab:this.currentTab,params:this.getParams()};
    var queryString = '/';
    var _url;

    // TODO params needs be reconfigured
    if ( p.params !== undefined && p.params !== null && typeof p.params === 'object' ) {
        var key;
        for ( key in p.params ) {
            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] === true || p.params[key] === false ) {
                if ( p.params[key] === false ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] !== null && p.params[key] !== "" && p.params[key] !== 'none' ) {
                //queryString += key + '/' + p.params[key] + '/';
            } else {
                delete p.params[key];
            }
        }
    } else {
        p.params = {};
    }

    try {
        if ( p.tab !== undefined && p.tab !== null ) {
            this.currentTab = p.tab;
            _url = this._makeUrl(p.tab, queryString);
            var idx;
            for ( idx in this.params.WIDGET.avaliableTabs ) {
                $('#'+idx).toggleClass('current', idx === this.currentTab);
            }
        } else if ( p.popup !== undefined && p.popup !== null ) {
            _url = this._makeUrl(p.popup, queryString);
        }
    } catch ( e ) {
        return false;
    }

    this.showPreloader();

    if ( this.tabOpenXhrRequest ) {
        this.tabOpenXhrRequest.abort();
        this.tabOpenXhrRequest = null;
    }

    this.tabOpenXhrRequest = $.ajax({
        url : _url,
        data : p.params,
        cache : false,
        type : 'POST',
        dataType : 'html',
        context : this,
        error : function (xhr, status, error) {
            $('body').css('cursor', '');
            if ( xhr.responseText === "authentication required" ) {
                document.location = "/quickticket/logIn.php";
            } else {
                FSWidget.fillContainer("Sorry, but an error occurred. No results found.", this);
            }
            this.tabOpenXhrRequest = null;
        },
        success : function(data, status, xhr) {
            $('body').css('cursor', '');
            FSWidget.fillContainer(data, this)
            calendarInit();
            this.initCurrency();
            this.initPhone();
			
			if(p.params.project != null){
            	this.autofillProjectInfo(p.params.project, true);
            }
            
            if(p.params.techID != null){
            	this.autofillTechInfo(p.params.techID);
            }
           
            jQuery("#cmdChooseSite").unbind("click",this.openChooseSite).bind("click",this.openChooseSite);
        }
    });

}


FSWidgetCreateQuickTicket.prototype.autoFillSite = function () {
    $.ajax({
        url         : '/widgets/dashboard/sitelist/get-site-customer',
        dataType    : 'json',
        data        : {
		SiteNumber: $("#edit_SiteNumber").val(),
		Project_ID: $("#edit_Project_ID").val(),
		company: window._company
	},
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
		if (data) {
			for (index in data) {
				if (index == 'State' || index == 'Project_ID' || index == 'SiteNumber' || index == 'Country') continue;
				var element = $('#edit_'+index);
				var value = data[index]?data[index]:'';
				element.val(value);
				element.change();
			}
			$('#edit_State').val(data['State']);
			this._queueSiteState = data['State'];
			$('#Country').val(data['Country']);
			this.onChangeCountry();
		}
        }
    });	
}
FSWidgetCreateQuickTicket.prototype.onChangeCountry = function () {
    $.ajax({
        url         : '/widgets/dashboard/js/state-info/',
        dataType    : 'json',
        data        : {country: $("#Country").children("option:selected").val()},
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            if (data) {
                //AUTOFILL
				var option = '';
				option += '<option selected="selected" value="">Select State</option>';
                for(index in data) {
					option += '<option value="' + index + '">' + data[index] + '</option>';
				}
				$("#edit_State").html(option);
				if (this._queueSiteState) $('#edit_State').val(this._queueSiteState);
				this._queueSiteState = null;
			}
        }
    });	
}

FSWidgetCreateQuickTicket.prototype.openChooseSite = function()
{
    if(jQuery.trim($("#edit_Project_ID").val()) == "")
    {
        var data = {success:0,errors:['Please select a project.']};
        detailsWidget.showMessages(data);
    }   
    else
    {
        openPopupWin('siteView.php?v=' + window._company + '&project_id=' + $("#edit_Project_ID").val() + '&select_mode=' + 1);
    }      
}
FSWidgetCreateQuickTicket.prototype.showPreloader = function() {
    if (this._customfunctions.showPreloader !== undefined && typeof this._customfunctions.showPreloader === 'function' ) {
        this._customfunctions.showPreloader();
    }else{
        $('body').css('cursor', 'wait');
        var img = "<div class='tCenter' ><img style='margin:10% auto;' alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></div>";
        FSWidget.fillContainer(img, this);
    }
}

/**
 * Fill create form with project data
 * @param pid int - project id
 * @author Alex Scherba
 */
FSWidgetCreateQuickTicket.prototype.autofillProjectInfo = function (pid) {
    if (!pid) return;
    $.ajax({
        url         : '/widgets/dashboard/js/project-info/',
        dataType    : 'json',
        data        : {id:pid,company:window._company},
        cache       : false,
        type        : 'post',
        success     : function (data, status, xhr) {
            if (!data) return;
            //fill create form with project data
            for(index in data) {
                var element = $('#edit_'+index);
                var value = data[index]?data[index]:'';
                if (element.size()>0 && index != 'Project_ID') {
                    if (element.attr('type') == 'checkbox') {
                        if(value!=='0') {
                            element.attr('checked','checked');
                        }else{
                            element.removeAttr('checked');
                        }
                    } else if (element.get(0).tagName=='INPUT' || element.get(0).tagName=='SELECT' || element.get(0).tagName=='TEXTAREA'){
                        //if (element.val() != "" && (index != "Headline" || _company != "FLS" || element.val() != "NEW-REQUEST")) continue;
                        element.val(value);
                    } else{
                        element.html(value);
                    }
                }
            }
        }
    });
}

FSWidgetCreateQuickTicket.prototype.createWO = function() {
    this.showStartPopup();
    this._updatestatus = {success:1, errors:[]};
    params = $('#createDetailsForm').serialize();
    params.company = window._company;
    $.ajax({
        url         : '/widgets/dashboard/customer/do-create/',
        dataType    : 'json',
        data        : params,
        cache       : false,
        type        : 'post',
        context     : this,
        success     : function (data, status, xhr) {
            this._updatestatus = data;
            if (data.success) {
                this._redirect = true;
                this.params.win = data.win;
                this._uploadsCounter = 0;
                for ( var i = 1; i < 4; i++ ) {
                    if ( $('#Pic'+i).val() ) {
                        this._uploadsCounter++;
                        this.asyncUploadFile( i );
                    }
                }
                //no files to upload
                if ( this._uploadsCounter <= 0 ) {
                    this.showMessages(this._updatestatus);
                }
            }else{
                this.showMessages(this._updatestatus);
            }
        }
    });
}

FSWidgetCreateQuickTicket.prototype.showStartPopup = function() {
    var html = "<div class=\"tCenter\"><img alt='Wait...' src='"+this.waitImage.src+"' width='"+this.waitImage.width+"px' height='"+this.waitImage.height+"px' /></div>";
    this._roll.autohide(false);
    var opt = {
        width       : 300,
        height      : '',
        position    : 'middle',
        title       : 'Processing',
        body        : html
    };
    return this._roll.showNotAjax(null, null, opt);
}

FSWidgetCreateQuickTicket.prototype.showMessages = function(data) {
    var html = '';
    if (this._redirect || data.success) { //always refresh page on success
        if (window.opener) {try{window.opener.reloadTabFrame();} catch (e) {} }
//        window.location = '/customers/create_QuickTicket.php?v='+window._company+'&tab='+this._reftab+'&id='+this.params.win;
        window.location = '/quickticket/wos.php?v='+window._companyid+'&t='+window._company;
    }
    if (data.success) {
        html += '<div class="success"><ul><li>Success!</li></ul></div>';
    }else{
        html+='<div class="errors"><ul>';
        for (i=0;i<data.errors.length;i++) {
            if(typeof data.errors[i] === 'object')
                html+='<li>'+data.errors[i].message+'</li>';
            else
                html+='<li>'+data.errors[i]+'</li>';
        }
        html+='</ul></div>';
    }
    html+='<div class="tCenter"><input type="button" class="link_button_popup" onclick="detailsWidget.roll().hide();" value="Ok" /></div>';
    this._roll.autohide(false);
    var opt = {
        width       : 400,
        height      : '',
        position    : 'middle',
        title       : '',
        body        : html
    };
    if (data.success) {
        opt.width = 300;
    }
    return this._roll.showNotAjax(null, null, opt);
}

FSWidgetCreateQuickTicket.prototype.asyncUploadFile = function(id){
    $.ajaxFileUpload({
        url             : "/widgets/dashboard/customer/ajax-upload-file/win/"+this.params.win+"/file/" + id + "/company/" + window._company,
        secureuri       : false,
        fileElementId   : "Pic" + id,
        dataType        : 'json',
        context         : this,
        cache           : false,
        success         : function (data, status, xhr) {
            this.context._uploadsCounter--;
            if ( !data.success ) {
                this.context._updatestatus.success = 0;
                if ( !this.context._updatestatus.errors ) {
                    this.context._updatestatus.errors = [];
                }
                for (i=0;i<data.errors.length;i++) {
                    this.context._updatestatus.errors.push(data.errors[i]);
                }
            }
            if ( this.context._uploadsCounter <= 0 ) {
                this.context.showMessages(this.context._updatestatus);
            }
        },
        error:function(data, status, xhr){
            this.context._uploadsCounter--;
        }
    });
}

FSWidgetCreateQuickTicket.prototype.uploadFile = function(id){
    $.ajaxFileUpload({
        url             : "/widgets/dashboard/details/customer/win/"+this.params.win+"/file/" + id + "/company/" + window._company,
        secureuri       : false,
        fileElementId   : "Pic" + id,
        dataType        : 'json',
        context         : this,
        cache           : false,
        success         : function (data, status, xhr) {
            this.context._activeupload++;
            for (this.context._activeupload;this.context._activeupload<4;this.context._activeupload++) {
                if ($('#Pic'+this.context._activeupload).val()) {
                  this.context.uploadFile(this.context._activeupload);
                  break;
                }
            }

            if (!data.success) {
                this.context._updatestatus.success = 0;
                if (!this.context._updatestatus.errors) {
                    this.context._updatestatus.errors = [];
                }
                for (i=0;i<data.errors.length;i++) {
                    this.context._updatestatus.errors.push(data.errors[i]);
                }
            }

            if (this.context._activeupload > 3) {
                this.context.showMessages(this.context._updatestatus);
            }
        }
    });
}

FSWidgetCreateQuickTicket.prototype.initCurrency = function(){
    $.map(['edit_PayMax', 'edit_Tech_Bid_Amount', 'edit_baseTechPay', 'edit_OutofScope_Amount', 'edit_TripCharge', 'edit_MileageReimbursement', 'edit_MaterialsReimbursement', 'edit_AbortFeeAmount', 'edit_Additional_Pay_Amount', 'edit_PayAmount'], function( id ) {
        $('#'+id).unbind('blur').bind('blur', function(){
            $(this).formatCurrency({groupDigits:false, symbol:'', negativeFormat: '-%n'});
        })
        $('#'+id).formatCurrency({groupDigits:false, symbol:'', negativeFormat: '-%n'});
    });
}

FSWidgetCreateQuickTicket.prototype.updateSourceBy = function() {
    /* Begin Sourceby date code - CM  */
    var startDate = new Date($("#edit_StartDate").val());
    if (!startDate)
        return;

    today = new Date();
    var weekDays = 0;
    while (today <=startDate) {
        if(today.getDay() >= 1 && today.getDay()<=5) weekDays++;
        today.setDate(today.getDate() +1);
    }

    if(weekDays <=5) {
        $("#edit_ShortNotice").attr("checked","checked");
    }else{
        $("#edit_ShortNotice").removeAttr("checked");
    }

    if(weekDays < 16) {
        sourceByDate = new Date();
    }else{
        sourceByDate = new Date(startDate);
        /* Can't count start day so subtract, initally start with Project Date */
        sourceByDate.setDate(sourceByDate.getDate() -1);
    }

    if($("#edit_Type_ID option:selected").val() == 1) {
        var todayMonth = sourceByDate.getMonth();
        todayMonth++;
        var formatSourceByDate = todayMonth+"/"+sourceByDate.getDate()+"/"+sourceByDate.getFullYear();
        $("#edit_SourceByDate").html(formatSourceByDate);
    }
}

FSWidgetCreateQuickTicket.prototype.makePayFieldsReadOnly = function() {
	// make pricing fields ready only if invoiced

	$("#edit_Tech_Bid_Amount").attr("readonly", true);
	$("#edit_Amount_Per").attr("disabled", true);
	$("#edit_Qty_Devices").attr("readonly", true);

	$("#edit_Work_Out_of_Scope").click(function(){return false;});
	$("#edit_OutofScope_Amount").attr("readonly", true);
	$("#edit_OutOfScope_Reason").attr("readonly", true);

	$("#edit_TripCharge").attr("readonly", true);
	$("#edit_MileageReimbursement").attr("readonly", true);
	$("#edit_MaterialsReimbursement").attr("readonly", true);
	$("#edit_Additional_Pay_Amount").attr("readonly", true);

	$("#edit_AbortFee").click(function(){return false;});
	$("#edit_AbortFeeAmount").attr("readonly", true);
	$("#edit_Add_Pay_AuthBy").attr("readonly", true);
	$("#edit_Add_Pay_Reason").attr("readonly", true);

	$("#edit_calculatedTechHrs").attr("readonly", true);
	$("#edit_baseTechPay").attr("readonly", true);
}

FSWidgetCreateQuickTicket.prototype.initPhone = function(){
	var formatPhone = function() {
	var country = jQuery("#Country").val();
                
	if (this.value == null || this.value == "") return;
	if (country == "US" || country == "CA")
	{
		validFormat = /^[(]?(\d{3})[)]?(?:\s|-|.)*(\d{3})[-\s.]*(\d{4})$/;
		parts = this.value.match(validFormat);
		if (parts == null) return;
		this.value = "(" + parts[1] + ") " + parts[2] + "-" + parts[3];
	}
};

	$.map(['edit_SitePhone', 'edit_SiteFax'], function( id ) {
		$('#'+id).each(formatPhone).blur(formatPhone);
	});
}

FSWidgetCreateQuickTicket.prototype.confirmRemoveFile = function( obj, file_name )
{
    if ( $(obj).attr('checked') ) {
        if ( confirm('Are you sure you wish to remove the file "' + file_name + '"?') ) {
            $(obj).attr('checked', true);
        } else {
            $(obj).attr('checked', false);
        }
    }
}

/**
 *  getParams
 *
 *  Function collect all params for make request
 *
 *  return object
 */
FSWidgetCreateQuickTicket.prototype.getParams = function () {
return {
    'tab'               : this.currentTab,
    'company'           : window._company,
    'win'               : this.params.win
    }
}
