function FSWidgetTechSite( params, roll,element ) {
    FSWidget.call(this, params);
    this._option = 0;
    this._roll = roll;
    this._redirect =false;
    this._redirectTo = '';
    this._element = element;
    this._url   = '/widgets/dashboard/popup/techsinsite/';
    this._techinfourl   = '/widgets/dashboard/popup/sorttechinfo/';
    this._savetechsiteurl   = '/widgets/dashboard/popup/savetechsite/';
}

FSWidgetTechSite.prototype = new FSWidget();
FSWidgetTechSite.prototype.constructor = FSWidgetTechSite;

FSWidgetTechSite.NAME          = 'FSWidgetTechSite';
FSWidgetTechSite.VERSION       = '0.1';
FSWidgetTechSite.DESCRIPTION   = 'Class FSWidgetTechSite';

FSWidgetTechSite.prototype.roll = function() {
    return this._roll;
}
//------------------------------------------------------
FSWidgetTechSite.prototype.doRequest = function () {
    $.ajax({
            url         : this._url,
            data        : this.params,
            dataType    : 'json',
            cache       : false,
            type        : 'post',
            context     : this,
            success     : function (data, status, xhr) {
                $('#divShowTechsInSite').html(data.html);
            },
            error       : function (xhr, status, error) {
                if ( xhr.responseText === "authentication required" ) {
                    document.location = "/clients/logIn.php";
                } else {
                    $('#divShowTechsInSite').html('<br /><span>Sorry, but error occurred</span><br />');
                }
            }
    });
}
//------------------------------------------------------
FSWidgetTechSite.prototype.techIDChange = function (element) {
    var techID = element.value;
    var rowid = element.getAttribute('rowid');
     $.ajax({
            url         : this._techinfourl,
            data        : {TechID:techID},
            dataType    : 'json',
            cache       : false,
            type        : 'post',
            context     : this,
            success     : function (data, status, xhr) {
                if(data.techInfo != null)
                {
                    $('#FLSID_row'+rowid).val(data.techInfo.FLSID);
                    $('#FirstName_row'+rowid).html(data.techInfo.FirstName);
                    $('#LastName_row'+rowid).html(data.techInfo.LastName);
                    $('#PrimaryPhone_row'+rowid).html(data.techInfo.PrimaryPhone);
                    $('#PrimaryEmail_row'+rowid).html(data.techInfo.PrimaryEmail);
                    $('#spanDeleteSiteTech_row'+rowid).css("display", "inline"); 
                }
                else
                {
                    $('#FLSID_row'+rowid).val('');
                    $('#FirstName_row'+rowid).html('');
                    $('#LastName_row'+rowid).html('');
                    $('#PrimaryPhone_row'+rowid).html('');
                    $('#PrimaryEmail_row'+rowid).html('');
                    $('#spanDeleteSiteTech_row'+rowid).css("display", "none"); 
                }
  
            },
            error       : function (xhr, status, error) {
                if ( xhr.responseText === "authentication required" ) {
                    document.location = "/clients/logIn.php";
                } else {
                    $('#divShowTechsInSite').html('<br /><span>Sorry, but error occurred</span><br />');
                }
            }
    });
}
//------------------------------------------------------
FSWidgetTechSite.prototype.FLSIDChange = function (element) {
    var techID = 0;
    var FLSID = element.value;
    var rowid = element.getAttribute('rowid');
     $.ajax({
            url         : this._techinfourl,
            data        : {TechID:techID,FLSID:FLSID},
            dataType    : 'json',
            cache       : false,
            type        : 'post',
            context     : this,
            success     : function (data, status, xhr) {
                if(data.techInfo != null)
                {
                    $('#techid_row'+rowid).val(data.techInfo.techid);                    
                    $('#FirstName_row'+rowid).html(data.techInfo.FirstName);
                    $('#LastName_row'+rowid).html(data.techInfo.LastName);
                    $('#PrimaryPhone_row'+rowid).html(data.techInfo.PrimaryPhone);
                    $('#PrimaryEmail_row'+rowid).html(data.techInfo.PrimaryEmail);
                    $('#spanDeleteSiteTech_row'+rowid).css("display", "inline");
                }
                else
                {
                    $('#techid_row'+rowid).val('');
                    $('#FirstName_row'+rowid).html('');
                    $('#LastName_row'+rowid).html('');
                    $('#PrimaryPhone_row'+rowid).html('');
                    $('#PrimaryEmail_row'+rowid).html('');
                    $('#spanDeleteSiteTech_row'+rowid).css("display", "none");
                }
  
            },
            error       : function (xhr, status, error) {
                if ( xhr.responseText === "authentication required" ) {
                    document.location = "/clients/logIn.php";
                } else {
                    $('#divShowTechsInSite').html('<br /><span>Sorry, but error occurred</span><br />');
                }
            }
    });
}
//------------------------------------------------------
FSWidgetTechSite.prototype.save = function () {
    var roll = this.roll();
    var params = $('#techsiteForm').serialize();
    $.ajax({
        url         : this._savetechsiteurl,
        dataType    : 'json',
        data        : params,
        cache       : false,
        type        : 'post',
        success     : function (data, status, xhr) {
                roll.hide();
                window.location.reload();
        }
    });
}
//------------------------------------------------------
FSWidgetTechSite.prototype.addtechs = function () {
    var roll = this.roll();
    var objs = $('#techsiteForm input[name^="techid"]');
    var recursiveDecoded = decodeURIComponent($.param(objs));
    // techid[]=15527&techid[]=53497
    var n=recursiveDecoded.split("&"); 
    var techids='';
    var ntechs = 0;
    for(var i=0;i<n.length;i++)
    {
        var techid = n[i].replace("techid[]=",""); 
        techid = techid.replace(" ",""); 
        if(techid!='') {
            techids =   techids=='' ? techid : techids +','+techid;
            ntechs++;
        }
    }
    var strShowTechSite = '+Add Techs to this Site';
    if(ntechs==1) strShowTechSite = '1 Tech';
    else if(ntechs > 1) strShowTechSite = n.length.toString() + ' Techs';
    $('#AddSiteTechIDs').val(techids);
    $('#spanShowTechSite').html("<a id='ShowTechSite' onclick='showTechSite(this,\""+techids+"\")' class='ajaxLink'>"+strShowTechSite+"</a>");
    roll.hide();
}
//------------------------------------------------------
FSWidgetTechSite.prototype.removeRow = function (rowid) {
    $('#trow'+rowid).remove();
}
//------------------------------------------------------
FSWidgetTechSite.prototype.addRow = function (companyID) {    
    var rows = $('#tblsitetechs tr'); 
    var rowcount = rows.length;
    var rowid = rowcount - 1;
    var odd = rowid % 2;
    var trClass = 'border_td odd_tr';
    if(odd==1) trClass = 'border_td even_tr';
    var newrow = "<tr class='"+ trClass + "' id='trow"+rowid+"'>";
    newrow += "<td><input type='text' onchange='techSiteWidget.techIDChange(this);' value='' size='6' name='techid[]' rowid='"+rowid+"' id='techid_row"+rowid+"'></td>";
    if(companyID=='FLS')
    {
        newrow += "<td><input type='text' onchange='techSiteWidget.FLSIDChange(this);' value='' size='6' name='FLSID[]' rowid='"+rowid+"' id='FLSID_row"+rowid+"'></td>";
    }
    newrow += "<td><span id='FirstName_row"+rowid+"'></span></td>";
    newrow += "<td><span id='LastName_row"+rowid+"'></span></td>";
    newrow += "<td><span id='PrimaryPhone_row"+rowid+"'></span></td>";
    newrow += "<td><span id='PrimaryEmail_row"+rowid+"'></span></td>";
    newrow += "<td><span id='spanDeleteSiteTech_row"+rowid+"' style='display:none'><a  class='ajaxLink' onclick='techSiteWidget.removeRow("+rowid+");'>Delete</a></span></td>";
    newrow += "</tr>";
    $('#tblsitetechs').append(newrow);
    
}


