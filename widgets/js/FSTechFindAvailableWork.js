/**
 * FSTechFindAvailableWork
 *
 * @param Object|null params
 * @access public
 * @return void
 */
function FSTechFindAvailableWork ( params ) {
    FSWidgetDashboard.call(this, params);
    this.params.WIDGET.avaliableTabs  = {
        techavailable : {module:'dashboard',controller:'tech-dashboard',action:'available'}
    };

    if ( this.params.POPUP === undefined ) this.params.POPUP = {};
    this.params.POPUP.avaliablePopups = {
        filters : {module:'dashboard',controller:'popup',action:'find-available-work'}
    }
    //this.filters()._techId = window._techId;
}

FSTechFindAvailableWork.prototype = new FSWidgetDashboard();
FSTechFindAvailableWork.prototype.constructor = FSTechFindAvailableWork;

FSTechFindAvailableWork.NAME          = 'FSTechFindAvailableWork';
FSTechFindAvailableWork.VERSION       = '0.1';
FSTechFindAvailableWork.DESCRIPTION   = 'Class FSTechFindAvailableWork';

/**
 *  prepareFilters
 */
FSTechFindAvailableWork.prototype.prepareFilters = function(roll, params) {
    FSWidget.filterActivate(true);

    var i, stateSelect, callSelect;
    stateSelect = $('#' + roll.container().id + ' #filterState')[0];
    for ( i=0; i < stateSelect.options.length; ++i ) {
        if ( stateSelect.options[i].value == this.filters().state() ) {
            stateSelect.options[i].selected = true;
            break;
        }
    }

    countrySelect = $('#' + roll.container().id + ' #filterCountry')[0];
    for ( i=0; i < countrySelect.options.length; ++i ) {
        if ( countrySelect.options[i].value == this.filters().country() ) {
            countrySelect.options[i].selected = true;
            break;
        }
    }
	
	$('#' + roll.container().id + ' #filterCountry').change();
	
    $('#' + roll.container().id + ' #filterZip')[0].value = techInfo.Zipcode;
    $('#' + roll.container().id + ' #filterStartDate')[0].value = window._currentDate;
    curDate = new Date(window._currentDate);
    endDateDef = new Date(curDate.getTime() + 1209600000); // today plus 2 weeks
    $('#' + roll.container().id + ' #filterEndDate')[0].value = (endDateDef.getMonth()+1)+'/'+endDateDef.getDate()+'/'+endDateDef.getFullYear();

    $('#' + roll.container().id + ' #filterRoute')[0].value = this.filters().Route();
/*    $('#' + roll.container().id + ' #Distance')[0].value = ( this.filters().distance() == null ) ? '' : this.filters().distance();
    $('#' + roll.container().id + ' #filterZip')[0].value = ( this.filters().zip() == null ) ? '' : this.filters().zip();
    $('#' + roll.container().id + ' #filterStartDate')[0].value = ( this.filters().dateStart() == null ) ?  window._currentDate : this.filters().dateStart();
    $('#' + roll.container().id + ' #filterEndDate')[0].value = ( this.filters().dateEnd() == null ) ? '' : this.filters().dateEnd();
*/
    //VALIDATORS
	//var vZIP = new LiveValidation('filterZip',LVDefaults);
	//vZIP.add( Validate.Format, {pattern: /^(?!0{5})(\d{5})(?!-?0{4})(-?\d{4})?$/} );
}

/**
 *  applyFilters
 *
 *  @return void
 */
FSTechFindAvailableWork.prototype.applyFilters = function (roll) {
    FSWidget.filterActivate(false);

    var i, stateSelect, callSelect;

    //this.filters().dateCreated($('#' + roll.container().id + ' #filterDateCreated')[0].value);
    this.filters().dateStart($('#' + roll.container().id + ' #filterStartDate')[0].value);
    this.filters().dateEnd($('#' + roll.container().id + ' #filterEndDate')[0].value);
    this.filters().zip($('#' + roll.container().id + ' #filterZip')[0].value);
    this.filters().distance($('#' + roll.container().id + ' #Distance')[0].value);
    //this.filters().city($('#' + roll.container().id + ' #City')[0].value);
    //this.filters().win($('#' + roll.container().id + ' #WIN_NUM')[0].value);
    this.filters().Route($('#' + roll.container().id + ' #filterRoute').val()); 

    stateSelect = $('#'+roll.container().id+' #filterState')[0];
    
    for ( i=0; i< stateSelect.options.length; ++i ) {
        if ( stateSelect.options[i].selected ) {
            this.filters().state($(stateSelect.options[i]).val());
            break;
        }
    }

    countrySelect = $('#'+roll.container().id+' #filterCountry')[0];
    for ( i=0; i< countrySelect.options.length; ++i ) {
        if ( countrySelect.options[i].selected ) {
            this.filters().country($(countrySelect.options[i]).val());
            break;
        }
    }
	
    roll.hide();
    var p = this.getParams();
    this.show({tab:this.currentTab,params:p});
}

FSTechFindAvailableWork.prototype.show = function(options) {
    if ( FSWidget.isFilterActive() ) return;

    var p = options || {};
    var queryString = '';
    if ( p.params !== undefined && p.params !== null && typeof p.params === 'object' ) {
        var key;
        
        for ( key in p.params ) {
            //  Filter for empty parameters
            if ( p.params[key] instanceof Array ) {
                if ( p.params[key].length === 0 ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] === true || p.params[key] === false ) {
                if ( p.params[key] === false ) {
                    delete p.params[key];
                }
            } else if ( p.params[key] !== undefined && p.params[key] !== null && p.params[key].toLowerCase() !== 'none' ) {
                if(queryString != '') queryString += '&';
                queryString += key + '=' + p.params[key];
            } else {
                delete p.params[key];
            }
        }
    } else p.params = {};
    try {
        //openPopupWin('/techs/wos.php?'+queryString);
        window.open('/techs/wos.php?'+queryString,'_self'); //13951
    } catch ( e ) {
        return false;
    }
}
