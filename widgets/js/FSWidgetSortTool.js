function FSWidgetSortTool( sortSelect, directionSelect, wd, roll, defaultSortTool, useSessionDefault )
{
    if (wd !== undefined || roll !== undefined){

        if ( sortSelect === undefined )             sortSelect = null;
        if ( directionSelect === undefined )        directionSelect = null;

        if ( !(wd instanceof FSWidgetDashboard) ) {
            throw new Error('wd should be instance of FSWidgetDashboard');
        }
        if ( !(roll instanceof FSPopupRoll) ) {
            throw new Error('roll should be instance of FSPopupRoll');
        }

        this._sortSession = {};
        this._useSessionDefault = useSessionDefault ? true : false;
        this._sortSelect        = null;
        this._directionSelect   = null;
        this._currentSort       = 'None';
        this._currentDerection  = 'asc';
        this._toolInited        = false;
        this._wd                = wd;
        this._roll              = roll;
        this._currentSortTool   = null;

        this._closeBtn          = null;

        this._page              = null;
    
        this.setUsedTool(defaultSortTool);
    
        this._fullSortPopup     = {
            _init       : false,

            _field1ID   : null,
            _field1Val  : null,
            _field1Dir  : 'asc',

            _field2ID   : null,
            _field2Val  : null,
            _field2Dir  : 'asc',

            _field3ID   : null,
            _field3Val  : null,
            _field3Dir  : 'asc',

            _html       : null,

            _inUse      : false,
        
            _page       : null
        };

        var $this = this;
        this.cancelHadler = function () {
            $this.cancelSort();
        }

        if ( sortSelect && directionSelect ) {
            if ( typeof sortSelect === 'string' ) {
                this._sortSelect = document.getElementById(sortSelect);
            } else {
                this._sortSelect = sortSelect;
            }
            if ( typeof directionSelect === 'string' ) {
                this._directionSelect = document.getElementById(directionSelect);
            } else {
                this._directionSelect = directionSelect;
            }
        }

        this._sort = {};
        this._sort_defaults = {};
    
        this.prepareItems();
        this.initSortTool();
        this.prepareUsedTool();
    }
}

FSWidgetSortTool.NAME          = 'FSWidgetSortTool';
FSWidgetSortTool.VERSION       = '0.1';
FSWidgetSortTool.DESCRIPTION   = 'Class WidgetSortTool: Sort Tool for Dashboard';

FSWidgetSortTool.prototype.name = function()
{
    return this.constructor.NAME;
}

FSWidgetSortTool.prototype.version = function()
{
    return this.constructor.VERSION;
}

FSWidgetSortTool.prototype.description = function()
{
    return this.constructor.DESCRIPTION;
}
FSWidgetSortTool.prototype.setParamsSession = function (params)
{
    if ( !params || typeof params !== 'object' ) return;
    this.filters().po((params.po)?params.po:'');
    this.filters().setProject(params.projects);
    this.filters().state(params.state);
    this.filters().country(params.country);
    this.filters().zip((params.zip)?params.zip:'');

    this.filters().dateStart((params.start_date)?params.start_date:'');
    this.filters().dateEnd((params.end_date)?params.end_date:'');
    this.filters().timeZone((params.StandardOffset)?params.StandardOffset:'');//13892
    this.filters().region((params.region)?params.region:'');
    this.filters().tech((params.tech)?params.tech:'');
    this.filters().callType((params.call_type)?params.call_type:'');
}
FSWidgetSortTool.prototype.prepareUsedTool = function ()
{
    if ( this.getUsedTool() === 'quick' ) {
        if ( !this.fullInUsage() && this._fullSortPopup._init ) {
            this.sort(this._fullSortPopup._field1Val);

            this.direction(this._fullSortPopup._field1Dir);

            var i;

            if (this._sortSelect) {
                for (i=0; i<this._sortSelect.options.length; ++i) {
                    if ( this._sortSelect.options[i].value === this.sort() ) {
                        this._sortSelect.options[i].selected = true;
                        break;
                    }
                }
            }
            if (this._directionSelect) {
                for (i=0; i<this._directionSelect.options.length; ++i) {
                    if ( this._directionSelect.options[i].value === this.direction() ) {
                        this._directionSelect.options[i].selected = true;
                        break;
                    }
                }
            }
        }
    } else if ( this.getUsedTool() === 'full' ) {
        this._fullSortPopup._field1Val = this._currentSort;
        this._fullSortPopup._field1Dir = this._currentDerection;
        this._fullSortPopup._field2Val = null;
        this._fullSortPopup._field2Dir = 'asc';
        this._fullSortPopup._field3Val = null;
        this._fullSortPopup._field3Dir = 'asc';
        var i;

        if (this._sortSelect) {
            for (i=0; i<this._sortSelect.options.length; ++i) {
                if ( this._sortSelect.options[i].value === this._currentSort ) {
                    this._sortSelect.options[i].selected = true;
                    break;
                }
            }
        }
        if (this._directionSelect) {
            for (i=0; i<this._directionSelect.options.length; ++i) {
                if ( this._directionSelect.options[i].value === this._currentDerection ) {
                    this._directionSelect.options[i].selected = true;
                    break;
                }
            }
        }
//        this._sortSelect.options[0].selected = true;
//        this._directionSelect.options[0].selected = true;
    }
}

FSWidgetSortTool.prototype.fullInUsage = function ( bool )
{
    if ( bool !== undefined && bool !== null ) {
        this._fullSortPopup._inUse = ( bool ) ? true : false;
    }
    return this._fullSortPopup._inUse;
}

FSWidgetSortTool.prototype.setUsedTool = function ( toolName )
{
    if ( typeof toolName !== 'string' || typeof toolName === 'string' && toolName !== 'full' ) {
        this._currentSortTool = 'quick';
    } else {
        this._currentSortTool = 'full';
    }
}

FSWidgetSortTool.prototype.getUsedTool = function ()
{
    return this._currentSortTool;
}

FSWidgetSortTool.prototype.sort = function ( val )
{
    if ( val !== undefined ) {
        this._currentSort = val;
    }
    return this._currentSort;
}

FSWidgetSortTool.prototype.direction = function ( val )
{
    if ( val !== undefined ) {
        this._currentDerection = val;
    }
    return this._currentDerection;
}

FSWidgetSortTool.prototype.setFullSortVal = function ( value, num )
{
    nam = parseInt(num, 10);

    if ( value === undefined || num < 1 || num > 3 )
        return;

    switch ( num ) {
        case 1:
            this._fullSortPopup._field1Val = value;
            break;
        case 2:
            this._fullSortPopup._field2Val = value;
            break;
        case 3:
            this._fullSortPopup._field3Val = value;
            break;
    }
}

FSWidgetSortTool.prototype.setFullSortDir = function ( value, num )
{
    nam = parseInt(num, 10);

    if ( value === undefined || num < 1 || num > 3 )
        return;

    switch ( num ) {
        case 1:
            this._fullSortPopup._field1Dir = value;
            break;
        case 2:
            this._fullSortPopup._field2Dir = value;
            break;
        case 3:
            this._fullSortPopup._field3Dir = value;
            break;
    }
}

FSWidgetSortTool.prototype.update = function ()
{
    if ( this._sortSelect && this._directionSelect ) {
        curSort = this._currentSort;
        this.sort(this._sortSelect.options[this._sortSelect.selectedIndex].value);
//        if(this._currentSort == 'bidcount' && curSort != 'bidcount') {
//            this.direction('desc');
//            this._directionSelect.options[1].selected = true;
//        }
//        else
       this.direction(this._directionSelect.options[this._directionSelect.selectedIndex].value);
    }
}

FSWidgetSortTool.prototype.onChangeSort = function ()
{
    if ( this._sortSelect && this._directionSelect ) {
        curSort = this._currentSort;
        this.sort(this._sortSelect.options[this._sortSelect.selectedIndex].value);
        if(this._currentSort == 'bidcount' && curSort != 'bidcount') {
            this.direction('desc');
            this._directionSelect.options[1].selected = true;
        }
        else if(this._currentSort == 'timesinceworkdone' && curSort != 'timesinceworkdone')
        {
            this.direction('desc');
            this._directionSelect.options[1].selected = true;
        }
        else
            this.direction(this._directionSelect.options[this._directionSelect.selectedIndex].value);
    }
}

FSWidgetSortTool.prototype.initSortTool = function () {
    if ( !this._sortSelect || !this._directionSelect || !this._wd )
        return;

    this._toolInited = true;
    this._sortSelect.options.length = 0;
    this._directionSelect.options.length = 0;

    var i = 0;
    $this = this;
    /* Build new sorting items */
    $.each(this._sort[this._wd.currentTab], function (idx, val) {
        $this._sortSelect.options[i++] = new Option(val, idx, false, ($this._sort_defaults[$this._wd.currentTab] && $this._sort_defaults[$this._wd.currentTab].sort === idx && $this._currentSortTool === 'quick') ? true : false);
    });
    /* Build new sorting items */

    /* Build new direction items */
    if (this._sort_defaults[$this._wd.currentTab] && this._sort_defaults[$this._wd.currentTab].dir == 'desc') {
        this._directionSelect.options[0] = new Option('Ascending', 'asc', false, false);
        this._directionSelect.options[1] = new Option('Descending', 'desc', true, true);
    } else {
        this._directionSelect.options[0] = new Option('Ascending', 'asc', true, true);
        this._directionSelect.options[1] = new Option('Descending', 'desc', false, false);
    }
    /* Build new direction items */

    this.update();
}

FSWidgetSortTool.prototype.initSortPopup = function ()
{
    FSWidget.filterActivate(true);

    var i;
    var select1 = document.getElementById('_sort_select_' + this._fullSortPopup._field1ID);
    var select2 = document.getElementById('_sort_select_' + this._fullSortPopup._field2ID);
    var select3 = document.getElementById('_sort_select_' + this._fullSortPopup._field3ID);

    select1.options.length = select2.options.length = select3.options.length = 0;

    select2.options[0] = new Option('None', '', true, true);
    select3.options[0] = new Option('None', '', true, true);

    var $this = this;

    i = 0;
    $.each(this._sort[$this._wd.currentTab], function (idx, val) {
        select1.options[i++] = new Option(
            val,
            idx,
            ($this._sort_defaults[$this._wd.currentTab] && $this._sort_defaults[$this._wd.currentTab].sort === idx) ? true : false,
            ( $this._fullSortPopup._field1Val !== null ) ?
            (( idx === $this._fullSortPopup._field1Val ) ? true : false) :
            (( $this._sort_defaults[$this._wd.currentTab] && $this._sort_defaults[$this._wd.currentTab].sort === idx ) ? true : false));
    });

    i = 1;
    $.each(this._sort[$this._wd.currentTab], function (idx, val) {
        select2.options[i++] = new Option(
            val,
            idx,
            ($this._sort_defaults[$this._wd.currentTab] && $this._sort_defaults[$this._wd.currentTab].sort === idx) ? true : false,
            ( $this._fullSortPopup._field2Val !== null ) ?
            (( idx === $this._fullSortPopup._field2Val ) ? true : false) : false);
    });

    i = 1;
    $.each(this._sort[$this._wd.currentTab], function (idx, val) {
        select3.options[i++] = new Option(
            val,
            idx,
            ($this._sort_defaults[$this._wd.currentTab] && $this._sort_defaults[$this._wd.currentTab].sort === idx) ? true : false,
            ( $this._fullSortPopup._field3Val !== null ) ?
            (( idx === $this._fullSortPopup._field3Val ) ? true : false) : false);
    });

    if ( this._fullSortPopup._field1Dir === null || this._fullSortPopup._field1Dir === 'asc' ) {
        document.getElementById('_direction_asc_' + this._fullSortPopup._field1ID).checked = true;
    } else {
        document.getElementById('_direction_desc_' + this._fullSortPopup._field1ID).checked = true;
    }
    if ( this._fullSortPopup._field2Dir === null || this._fullSortPopup._field2Dir === 'asc' ) {
        document.getElementById('_direction_asc_' + this._fullSortPopup._field2ID).checked = true;
    } else {
        document.getElementById('_direction_desc_' + this._fullSortPopup._field2ID).checked = true;
    }
    if ( this._fullSortPopup._field3Dir === null || this._fullSortPopup._field3Dir === 'asc' ) {
        document.getElementById('_direction_asc_' + this._fullSortPopup._field3ID).checked = true;
    } else {
        document.getElementById('_direction_desc_' + this._fullSortPopup._field3ID).checked = true;
    }

    $(this._roll.closeBtn()).bind('click', this.cancelHadler);

    this.updateFullSort(select1);
}

FSWidgetSortTool.prototype.getSortSession = function ()
{
    return this._sortSession;
}

FSWidgetSortTool.prototype.setSortSession = function (sort)
{
    this._sortSession = sort;
}

FSWidgetSortTool.prototype.saveSortSession = function ()
{
    this._sortSession[this._wd.currentTab] = {
        'full' : this.fullInUsage(),
        'sort' : this.getSort(1),
        'sort2' : this.getSort(2),
        'sort3' : this.getSort(3),
        'dir' : this.getDirection(1),
        'dir2' : this.getDirection(2),
        'dir3' : this.getDirection(3)
    };
}

FSWidgetSortTool.prototype.getSort = function ( position )
{
    switch ( position ) {
        case 1:
            return ( this.fullInUsage() ) ? this._fullSortPopup._field1Val : this.sort();
        case 2:
            return ( this.fullInUsage() ) ? this._fullSortPopup._field2Val : null;
        case 3:
            return ( this.fullInUsage() ) ? this._fullSortPopup._field3Val : null;
        default:
            return null;
    }
}

FSWidgetSortTool.prototype.getDirection = function ( position )
{
    switch ( position ) {
        case 1:
            return ( this.fullInUsage() && this._fullSortPopup._field1Val ) ? this._fullSortPopup._field1Dir : this.direction();
        case 2:
            return ( this.fullInUsage() && this._fullSortPopup._field2Val ) ? this._fullSortPopup._field2Dir : null;
        case 3:
            return ( this.fullInUsage() && this._fullSortPopup._field3Val ) ? this._fullSortPopup._field3Dir : null;
        default:
            return null;
    }
}

FSWidgetSortTool.prototype.getPage = function ( )
{
    return (typeof(this._page) == 'undefined' || this._page == null ) ? 1 : this._page;
}

FSWidgetSortTool.prototype.setPage = function (page )
{
    this._page = page;
}

FSWidgetSortTool.prototype.switchSortTools = function( tool )
{
    var oldTool = this.getUsedTool();
    this.setUsedTool(tool);

    if ( oldTool !== this.getUsedTool() ) {
        this.prepareUsedTool();
    }
}

/**
 *  applySort
 *
 *  @param string type quick|full SortTool type
 *  @return FALSE always
 */
FSWidgetSortTool.prototype.applySort = function ( type )
{
    FSWidget.filterActivate(false);
    
    if ( typeof type === 'string' && type.toLowerCase() === 'quick' ) {
        this.switchSortTools('quick');
        this.fullInUsage(false);
        this.onChangeSort();
        this._wd.show({
            tab     : this._wd.currentTab,
            params  : this._wd.getParams()
        });
    } else if ( typeof type === 'string' && type.toLowerCase() === 'full' ) {
        this.fullInUsage(true);
        this._wd.show({
            tab     : this._wd.currentTab,
            params  : this._wd.getParams()
        });
        this._roll.hide();
        $(this._sortSelect).val(this._fullSortPopup._field1Val);
        $(this._directionSelect).val(this._fullSortPopup._field1Dir);
    }

    $(this._roll.closeBtn()).unbind('click', this.cancelHadler);
    this.saveSortSession();

    return false;
}

FSWidgetSortTool.prototype.cancelSort = function ()
{
    this._roll.hide();
    if ( !this.fullInUsage() ) {
        this.switchSortTools("quick");
    }

    if ( this._closeBtn !== null ) {
        $(this._closeBtn).remove();
        this._closeBtn = null;
    }

    FSWidget.filterActivate(false);

    $(this._roll.closeBtn()).unbind('click', this.cancelHadler);

    return false;
}

FSWidgetSortTool.prototype.updateFullSort = function ( currentSortSelect )
{
    var select1 = document.getElementById('_sort_select_' + this._fullSortPopup._field1ID);
    var select2 = document.getElementById('_sort_select_' + this._fullSortPopup._field2ID);
    var select3 = document.getElementById('_sort_select_' + this._fullSortPopup._field3ID);

    $.each([select1, select2, select3], function (idx, item) {
        for ( var i=1; i<item.options.length; ++i ) {
            item.options[i].disabled = false;
        }
    });

    select2.options[select1.selectedIndex+1].disabled = true;
    select3.options[select1.selectedIndex+1].disabled = true;
    if ( select2.selectedIndex !== 0 ) {
        select1.options[select2.selectedIndex-1].disabled = true;
        select3.options[select2.selectedIndex].disabled = true;
    }
    if ( select3.selectedIndex !== 0 ) {
        select1.options[select3.selectedIndex-1].disabled = true;
        select2.options[select3.selectedIndex].disabled = true;
    }

    var first   = new RegExp(this._fullSortPopup._field1ID+'$');
    var second  = new RegExp(this._fullSortPopup._field2ID+'$');
    var third   = new RegExp(this._fullSortPopup._field3ID+'$');

    if ( first.test(currentSortSelect.id) ) {
        this.setFullSortVal(currentSortSelect.value, 1);
    } else if ( second.test(currentSortSelect.id) ) {
        this.setFullSortVal(currentSortSelect.value, 2);
    } else if ( third.test(currentSortSelect.id) ) {
        this.setFullSortVal(currentSortSelect.value, 3);
    }
   var val = jQuery(currentSortSelect).val();
   if(val.toLowerCase() == 'bidcount' || val.toLowerCase() == 'techcheckedin_24hrs' || val.toLowerCase() == 'checkedin' )
    {
        jQuery(currentSortSelect)
        .parent().parent().find('input').each(function(){
            if(jQuery(this).attr('value') == 'desc')
            {
                jQuery(this).attr('checked','checked');
                jQuery(this).focus();
                if ( first.test(currentSortSelect.id) ) {
                    wd.sortTool().setFullSortDir('desc', 1);
                } else if ( second.test(currentSortSelect.id) ) {
                    wd.sortTool().setFullSortDir('desc', 2);
                } else if ( third.test(currentSortSelect.id) ) {
                    wd.sortTool().setFullSortDir('desc', 3);
                }
                return false;
            }
        })
                
    }
    else
    {
        if(val.toLowerCase() == 'timesinceworkdone' )
        {
            jQuery(currentSortSelect)
            .parent().parent().find('input').each(function(){
                if(jQuery(this).attr('value') == 'desc')
                {
                    jQuery(this).attr('checked','checked');
                    jQuery(this).focus();
                    if ( first.test(currentSortSelect.id) ) {
                        wd.sortTool().setFullSortDir('desc', 1);
                    } else if ( second.test(currentSortSelect.id) ) {
                        wd.sortTool().setFullSortDir('desc', 2);
                    } else if ( third.test(currentSortSelect.id) ) {
                        wd.sortTool().setFullSortDir('desc', 3);
}
                    return false;
                }
            })

        }
    }
}

/**
 *  makeFullSortPopup
 *
 *  Build html for Full Sort Tool
 *
 *  @return string HTML
 */
FSWidgetSortTool.prototype.makeFullSortPopup = function ()
{
    if ( !this._fullSortPopup._init ) {
        this._fullSortPopup._init = true;

        var html;

        this._fullSortPopup._field1ID = '_fullSort_' + Math.floor(Math.random()*10001);
        this._fullSortPopup._field2ID = '_fullSort_' + Math.floor(Math.random()*10001);
        this._fullSortPopup._field3ID = '_fullSort_' + Math.floor(Math.random()*10001);

        html  = "<table class='sort_table' width='100%' cellspacing='0' cellpadding='0'>";

        html == "<form name='fullSortToolForm' method='post' action='/'>";
        html += "<col width=50%>";
        html += "<col width=50%>";
        html += "<tr>";
        html += "<td colspan='2'>Sort by</td>";
        html += "</tr>";

        html += "<tr class='even'>";
        html += "<td><select class='sortToolTN' onchange='wd.sortTool().updateFullSort(this);' name='_sort_select_"+ this._fullSortPopup._field1ID +"' id='_sort_select_"+ this._fullSortPopup._field1ID +"' /></td>";
        html += "<td>";
        html += "<input type='radio' onclick='wd.sortTool().setFullSortDir(this.value, 1);' name='_direction_"+this._fullSortPopup._field1ID+"' id='_direction_asc_"+this._fullSortPopup._field1ID+"' value='asc' /> Ascending";
        html += "<br />";
        html += "<input type='radio' onclick='wd.sortTool().setFullSortDir(this.value, 1);'name='_direction_"+this._fullSortPopup._field1ID+"' id='_direction_desc_"+this._fullSortPopup._field1ID+"' value='desc' /> Descending";
        html += "</td>";
        html += "</tr>";

        html += "<tr>";
        html += "<td colspan='2'>Then by</td>";
        html += "</tr>";

        html += "<tr class='even'>";
        html += "<td><select class='sortToolTN'  onchange='wd.sortTool().updateFullSort(this)' name='_sort_select_"+ this._fullSortPopup._field2ID +"' id='_sort_select_"+ this._fullSortPopup._field2ID +"' /></td>";
        html += "<td>";
        html += "<input type='radio' onclick='wd.sortTool().setFullSortDir(this.value, 2);'name='_direction_"+this._fullSortPopup._field2ID+"' id='_direction_asc_"+this._fullSortPopup._field2ID+"' value='asc' /> Ascending";
        html += "<br />";
        html += "<input type='radio' onclick='wd.sortTool().setFullSortDir(this.value, 2);'name='_direction_"+this._fullSortPopup._field2ID+"' id='_direction_desc_"+this._fullSortPopup._field2ID+"' value='desc' /> Descending";
        html += "</td>";
        html += "</tr>";

        html += "<tr>";
        html += "<td colspan='2'>Then by</td>";
        html += "</tr>";

        html += "<tr class='even'>";
        html += "<td><select class='sortToolTN'  onchange='wd.sortTool().updateFullSort(this)' name='_sort_select_"+ this._fullSortPopup._field3ID +"' id='_sort_select_"+ this._fullSortPopup._field3ID +"' /></td>";
        html += "<td>";
        html += "<input type='radio' onclick='wd.sortTool().setFullSortDir(this.value, 3);'name='_direction_"+this._fullSortPopup._field3ID+"' id='_direction_asc_"+this._fullSortPopup._field3ID+"' value='asc' /> Ascending";
        html += "<br />";
        html += "<input type='radio' onclick='wd.sortTool().setFullSortDir(this.value, 3);'name='_direction_"+this._fullSortPopup._field3ID+"' id='_direction_desc_"+this._fullSortPopup._field3ID+"' value='desc' /> Descending";
        html += "</td>";
        html += "</tr>";

        html += "<tr>";
        html += "<td colspan='2' align='center' class='submit_buttons'>";
        html += "<input type='button' value='Ok' class='link_button_popup' onclick='return wd.sortTool().applySort(\"full\");' />";
        html += "<input type='button' value='Cancel' class='link_button_popup' onclick='return wd.sortTool().cancelSort();' />";
        html += "</td>";
        html += "</tr>";

        html += "</form>";

        html += "</table>";

        this._fullSortPopup._html = html;
    }

    return this._fullSortPopup._html;
}

FSWidgetSortTool.prototype.setMyDashboardDefaultByTabSingle = function(tab, sort, dir) {
    this._myDashboardDefaultByTab[tab] = {
        'sort' : sort, 'dir' : dir
    };
}

FSWidgetSortTool.prototype.setMyDashboardDefaultByTab = function(sort) {
    // used for default quicksort
    this._myDashboardDefaultByTab = [];
    this.setMyDashboardDefaultByTabSingle('created', sort.defaultCreatedSort, sort.defaultCreatedSortDir);
    this.setMyDashboardDefaultByTabSingle('published', sort.defaultPublishedSort, sort.defaultPublishedSortDir);
    this.setMyDashboardDefaultByTabSingle('assigned', sort.defaultAssignedSort, sort.defaultAssignedSortDir);
    this.setMyDashboardDefaultByTabSingle('workdone', sort.defaultWorkDoneSort, sort.defaultWorkDoneSortDir);
    this.setMyDashboardDefaultByTabSingle('incomplete', sort.defaultIncompleteSort, sort.defaultIncompleteSortDir);
    this.setMyDashboardDefaultByTabSingle('completed', sort.defaultCompletedSort, sort.defaultCompletedSortDir);
    this.setMyDashboardDefaultByTabSingle('all', sort.defaultAllSort, sort.defaultAllSortDir);
    this.setMyDashboardDefaultByTabSingle('deactivated', sort.defaultDeactivatedSort, sort.defaultDeactivatedSortDir);
        this.setMyDashboardDefaultByTabSingle('todayswork', sort.defaultTodaysworkSort, sort.defaultTodaysworkSortDir);
}

FSWidgetSortTool.prototype.getMyDashboardDefaultSort = function(tab) {
//    if (!this._useSortSession) {
        my_defaults = { 'completed' : 'desc', 'all' : 'desc', 'deactivated' : 'desc' };
        return {
            sort: this._myDashboardDefaultByTab == undefined ? 'startdate' : this._myDashboardDefaultByTab[tab].sort, 
            dir: this.getMyDashboardDefaultSortDir(tab, my_defaults[tab] ? my_defaults[tab] : 'asc')
        };
//    }
//    return this._sortSession[tab];
}

FSWidgetSortTool.prototype.getMyDashboardDefaultSortDir = function(tab, defaultDir) {
    if (this._myDashboardDefaultByTab == undefined || this._myDashboardDefaultByTab[tab].dir == null || this._myDashboardDefaultByTab[tab].dir == "") return defaultDir;
    return this._myDashboardDefaultByTab[tab].dir == 0 ? 'asc' : 'desc';
}

FSWidgetSortTool.prototype.getMyDashboardDefaultSortSession = function(tab, defaultDir) {
    return this._sortSession[tab] ? this._sortSession[tab] : {
        sort1: 'startdate',
        dir1: defaultDir
    };
}

FSWidgetSortTool.prototype.prepareItems = function (version) {
    version = version || 'Full';
    if(version === undefined) return;

    this._sort.workdone = {
        'win'           : 'WIN#',
        'clientid'      : 'Client ID',
        'clientpo'      : 'Client PO',
        'startdate'     : 'Start Date',
        'checkin'       : 'Check-in Time',
        'StandardOffset'      : 'Time Zone',//13892
        'hours'         : 'Hours',
        'timesinceworkdone'         : 'Days Old', 
        'hl'            : 'Headline',
        'loc'           : 'Location',
        'project'       : 'Project',
        'site'          : 'Site',
        'region'        : 'Region',
        'route'         : 'Route',
        'techid'        : 'Tech ID',
        'techname'      : 'Tech Name',
        'bid'           : 'Agreed $',
        'final'         : 'Final $',
        'basis'         : 'Tech Pay Basis',
        'AuditNeeded'         : 'Work Order Audit',
        'docs'          : 'Documents',
        'parts'         : 'Return Parts'
    };
    this._sort.incomplete = {
        'win'           : 'WIN#',
        'clientid'      : 'Client ID',
        'clientpo'      : 'Client PO',
        'startdate'     : 'Start Date',
        'checkin'       : 'Check-in Time',
        'StandardOffset'      : 'Time Zone',//13892
        'hours'         : 'Hours',
        'hl'            : 'Headline',
        'loc'           : 'Location',
        'project'       : 'Project',
        'site'          : 'Site',
        'region'        : 'Region',
        'route'         : 'Route',
        'techid'        : 'Tech ID',
        'techname'      : 'Tech Name',
        'bid'           : 'Bid $',
        'final'         : 'Final $',
        'AuditNeeded'         : 'Work Order Audit',
        'docs'          : 'Documents',
        'parts'         : 'Return Parts'
    };
    this._sort.created = {
        'win'           : 'WIN#',
        'clientid'      : 'Client ID',
        'clientpo'      : 'Client PO',
        'startdate'     : 'Start Date',
        'StandardOffset'      : 'Time Zone',//13892
        'hours'         : 'Total Hours',
        'hl'            : 'Headline',
        'loc'           : 'Location',
        'project'       : 'Project',
        'site'          : 'Site',
        'region'        : 'Region',
        'route'         : 'Route',
        'payMax'        : 'Offer $',
        'basis'         : 'Tech Pay Basis',
        'AuditNeeded'         : 'Work Order Audit'
    };
    this._sort.published = {
        'bidcount'      : 'Number of bids',
        'win'           : 'WIN#',
        'clientid'      : 'Client ID',
        'clientpo'      : 'Client PO',
        'startdate'     : 'Start Date',
        'StandardOffset'      : 'Time Zone',//13892
        'hours'         : 'Hours',
        'hl'            : 'Headline',
        'loc'           : 'Location',
        'project'       : 'Project',
        'site'          : 'Site',
        'region'        : 'Region',
        'route'         : 'Route',
        'loc1'          : 'Street',
        'city'          : 'City',
        'st'            : 'State',
        'zip'           : 'Site Zip Code',
        'payMax'        : 'Offer $',
        'basis'         : 'Tech Pay Basis',
        'AuditNeeded'         : 'Work Order Audit'
    };
    this._sort.assigned = {
        'win'           : 'WIN#',
        'clientid'      : 'Client ID',
        'clientpo'      : 'Client PO',
        'startdate'     : 'Start Date',
        'StandardOffset'      : 'Time Zone',//13892
        'enddate'       : 'End Date',
        'hours'         : 'Hours',
        'accepted'      : 'Accepted',
        'confirmed'     : 'Confirmed',
        'checkedin'     : 'Checked In',
        'hl'            : 'Headline',
        'loc'           : 'Location',
        'project'       : 'Project',
        'site'          : 'Site',
        'region'        : 'Region',
        'route'         : 'Route',
        'loc1'          : 'Site Street',
        'city'          : 'Site City',
        'st'            : 'Site State',
        'zip'           : 'Site Zip Code',
        'techid'        : 'Tech ID',
        'techname'      : 'Tech Name',
        'payMax'        : 'Offer $',
        'basis'         : 'Tech Pay Basis',
        'AuditNeeded'         : 'Work Order Audit'
    };
    this._sort.completed = {
        'win'           : 'WIN#',
        'clientid'      : 'Client ID',
        'clientpo'      : 'Client PO',
        'approveddate'  : 'Approved Date',
        'paid'          : 'Paid Date',
        'invoice'       : 'Invoice Date',
        'startdate'     : 'Start Date',
        'StandardOffset'      : 'Time Zone',//13892
        'hours'         : 'Hours',
        'hl'            : 'Headline',
        'loc'           : 'Location',
        'project'       : 'Project',
        'site'          : 'Site',
        'zip'           : 'Site Zip Code',//13962
        'st'            : 'Site State',//13962        
        'region'        : 'Region',
        'route'         : 'Route',
        'techid'        : 'Tech ID',
        'techname'      : 'Tech Name',
        'final'         : 'Final $',
        'AuditNeeded'         : 'Work Order Audit',
        'docs'          : 'Documents',
        'parts'         : 'Return Parts'
    };
    this._sort.all = {
        'stage'         : 'Stage',
        'win'           : 'WIN#',
        'clientid'      : 'Client ID',
        'clientpo'      : 'Client PO',
        'startdate'     : 'Start Date',
        'StandardOffset'      : 'Time Zone',//13892
        'hours'         : 'Hours',
        'hl'            : 'Headline',
        'loc'           : 'Location',
        'project'       : 'Project',
        'site'          : 'Site',
        'region'        : 'Region',
        'route'         : 'Route',
        'techid'        : 'Tech ID',
        'techname'      : 'Tech Name',
        'paymax'        : 'Max',
        'bid'           : 'Agreed',
        'final'         : 'Final $',
        'AuditNeeded'         : 'Work Order Audit',
        'docs'          : 'Documents',
        'parts'         : 'Return Parts'
    };
    this._sort.todayswork = {
        'stage'         : 'Stage',
        'win'           : 'WIN#',
        'clientid'      : 'Client ID',
        'clientpo'      : 'Client PO',
        'startdate'     : 'Start Date',
        'StandardOffset': 'Time Zone',//13892
        'accepted'      : 'Accepted',//13944
        'confirmed'     : 'Confirmed',
        'checkedin'     : 'Checked In',        
        'hours'         : 'Hours',
        'hl'            : 'Headline',
        'loc'           : 'Location',
        'project'       : 'Project',
        'site'          : 'Site',
        'region'        : 'Region',
        'route'         : 'Route',
        'techid'        : 'Tech ID',
        'techname'      : 'Tech Name',
        'paymax'        : 'Max',
        'bid'           : 'Agreed',
        'final'         : 'Final $',
        'AuditNeeded'         : 'Work Order Audit',
        'docs'          : 'Documents',
        'parts'         : 'Return Parts'
    };
    this._sort.deactivated = {
        'win'           : 'WIN#',
        'clientid'      : 'Client ID',
        'clientpo'      : 'Client PO',
        'startdate'     : 'Start Date',
        'checkin'       : 'Check-in Time',
        'StandardOffset': 'Time Zone',//13892
        'hours'         : 'Hours',
        'hl'            : 'Headline',
        'loc'           : 'Location',
        'project'       : 'Project',
        'site'          : 'Site',
        'region'        : 'Region',
        'route'         : 'Route',
        'techid'        : 'Tech ID',
        'techname'      : 'Tech Name',
        'bid'           : 'Agreed $',
        'final'         : 'Final $',
        'basis'         : 'Tech Pay Basis',
        'AuditNeeded'         : 'Work Order Audit',
        'docs'          : 'Documents',
        'parts'         : 'Return Parts'
    };

    /*************tech side***************/
    this._sort.techavailable = {
        'win'        : 'WIN #',
        'hl'         : 'Headline',
        'route'      : 'Route',
        'start'      : 'Start Date',
        'starttime'  : 'Start Time',
        'city'       : 'City',
        'st'         : 'State',
        'zip'        : 'Zipcode',
        'payMax'     : 'Offer',
        'distance'   : 'Distance',
        'deduct'     : 'Fee Deducted'
    };
    this._sort.techassigned = {
        'win'        : 'WIN #',
        'clientid'   : 'Client WO#',
        'hl'         : 'WO Headline',
        'route'      : 'Route',
        'sitenum'    : 'Site #',
        'site'       : 'Site Name',
        'start'      : 'Start Date',
        'enddate'      : 'End Date',
        'starttime'  : 'Start Time',
        'city'       : 'City',
        'st'         : 'State',
        'zip'        : 'Zipcode',
        'distance'   : 'Distance',
        'bid'        : 'Agreed',
        'deduct'     : 'Fee Deducted'
    };
    this._sort.techworkdone     = {
        'win'        : 'WIN #',
        'clientid'   : 'Client WO#',
        'hl'         : 'WO Headline',
        'route'      : 'Route',
        'sitenum'    : 'Site #',
        'site'       : 'Site Name',
        'start'      : 'Start Date',
        'enddate'      : 'End Date',
        'starttime'  : 'Start Time',
        'city'       : 'City',
        'st'         : 'State',
        'zip'        : 'Zipcode',
        'payMax'     : 'Agreed',
        'deduct'     : 'Fee Deducted'
    };
    this._sort.techapproved     = {
        'win'        : 'WIN #',
        'clientid'   : 'Client WO#',
        'hl'         : 'WO Headline',
        'route'      : 'Route',
        'sitenum'    : 'Site #',
        'site'       : 'Site Name',
        'start'      : 'Start Date',
        'starttime'  : 'Start Time',
        'city'       : 'City',
        'st'         : 'ST',
        'zip'        : 'Zip',
        'netpay'     : 'Net Pay $',
        'deduct'     : 'Free Deducted',
        'approveddate':'Approved Date'
    };
    this._sort.techpaid         = {
        'win'        : 'WIN #',
        'clientid'   : 'Client WO#',
        'hl'         : 'WO Headline',
        'route'      : 'Route',
        'sitenum'    : 'Site #',
        'site'       : 'Site Name',
        'approveddate':'Approved Date',
        'city'       : 'City',
        'st'         : 'ST',
        'zip'        : 'Zip',
        'paid'       : 'Paid Date',
        'netpay'     : 'Net Pay $',
        'deduct'     : 'Free Deducted'
    };
    this._sort.techincomplete   = {
        'win'        : 'WIN #',
        'clientid'   : 'Client ID',
        'hl'         : 'Headline',
        'route'      : 'Route',
        'site'       : 'Site Name',
        'sitenum'    : 'Site Num',
        'start'      : 'Start Date',
        'starttime'  : 'Start Time',
        'city'       : 'City',
        'st'         : 'State',
        'zip'        : 'Zipcode',
        'netpay'     : 'Pay Amount',
        'incompleted': 'Marked Incomplete'
    };
    this._sort.techall          = {
        'win'       : 'WIN #',
        'clientid'  : 'Client WO',
        'stage'     : 'WO Status',
        'hl'        : 'Headline',
        'route'      : 'Route',
        'sitenum'   : 'Site #',
        'site'      : 'Site Name',
        'city'      : 'City',
        'st'        : 'State',
        'zip'       : 'Zipcode',
        'paid'      : 'Paid Date',
        'netpay'    : 'Net Pay',
        'deduct'    : 'Fee Deducted'
    };
    this._sort.techschedule     = {
        'win'       : 'WIN #',
        'hl'        : 'Headline',
        'start'     : 'Start Date',
        'starttime' : 'Start Time',
        'city'      : 'City',
        'st'        : 'State',
        'zip'       : 'Zipcode',
        'deduct'    : 'Fee Deducted'
    };
    this._sort.quickviewopen = {
        'win'           : 'WIN#',
        'clientid'      : 'Client ID',
        'startdate'     : 'Start Date',
        'StandardOffset'      : 'Time Zone',//13892
        'hours'         : 'Hours',
        'techcheckedin_24hrs' : 'Confirmed',
        'checkedin'     : 'Checked In',
        'checkedout'    : 'Checked Out',
        'hl'            : 'Headline',
        'loc'           : 'Location',
        'project'       : 'Project',
        'sitenum'       : 'Site #',
        'site'          : 'Site Name',
        'region'        : 'Region',
        'route'         : 'Route',
        'techid'        : 'Tech ID',
        'techname'      : 'Tech Name',
        'docs'          : 'Documents',
        'parts'         : 'Return Parts',
        'clientpo'      : 'Client PO#'
    };
    this._sort.quickviewclose = {
        'win'           : 'WIN#',
        'clientid'      : 'Client ID',
        'startdate'     : 'Start Date',
        'StandardOffset'      : 'Time Zone',//13892
        'hours'         : 'Hours',
        'techcheckedin_24hrs' : 'Confirmed',
        'checkedin'     : 'Checked In',
        'checkedout'    : 'Checked Out',
        'hl'            : 'Headline',
        'loc'           : 'Location',
        'project'       : 'Project',
        'sitenum'       : 'Site #',
        'site'          : 'Site Name',
        'region'        : 'Region',
        'route'         : 'Route',
        'techid'        : 'Tech ID',
        'techname'      : 'Tech Name',
        'docs'          : 'Documents',
        'parts'         : 'Return Parts',
        'clientpo'      : 'Client PO#'
    };
    this._sort.quicktools = {
        'SiteInfo1'     : 'DSC Zone',
        'SiteInfo2'     : 'DSC Market',
        'City'          : 'DSC City',
        'State'         : 'DSC State',
        'Zipcode'       : 'DSC Zip'
    };
    var sort = 'startdate';
    var dir = '';
    
    if(typeof(dataSort) != 'undefined')
    {
        if(dataSort.sort1 != "" && typeof(dataSort.sort1) != 'undefined')
        {
            sort = dataSort.sort1;
            dir = dataSort.dir1;
        }

    }
    this._sort_defaults = {
        created          : this.getMyDashboardDefaultSort('created'),
        published        : this.getMyDashboardDefaultSort('published'), 
        assigned         : this.getMyDashboardDefaultSort('assigned'), 
        workdone         : this.getMyDashboardDefaultSort('workdone'), 
        incomplete       : this.getMyDashboardDefaultSort('incomplete'), 
        all              : this.getMyDashboardDefaultSort('all'), 
        deactivated      : this.getMyDashboardDefaultSort('deactivated'), 
        completed        : this.getMyDashboardDefaultSort('completed'), 
        todayswork       : this.getMyDashboardDefaultSort('todayswork'), 
        techavailable    : {
            sort: 'start', 
            dir: 'asc'
        },
        techassigned     : {
            sort: 'start', 
            dir: 'asc'
        },
        techworkdone     : {
            sort: 'start', 
            dir: 'asc'
        },
        techapproved     : {
            sort: 'start', 
            dir: 'desc'
        },
        techpaid         : {
            sort: 'paid', 
            dir: 'desc'
        },
        techincomplete   : {
            sort: 'start', 
            dir: 'asc'
        },
        techall          : {
            sort: 'paid', 
            dir: 'desc'
        },
        techschedule     : {
            sort: 'start', 
            dir: 'desc'
        },quickviewopen  : {
            sort: 'startdate', 
            dir: 'asc'
        },quickviewclose  : {
            sort: 'startdate', 
            dir: 'desc'
        },quicktools : {
            sort: 'SiteInfo1',
            dir: 'asc'
        }
    };
}
