function ProgressBarAdapterPopupSpin (provider, container) {
    ProgressBarAdapterAbstract.call(this, provider, container);

    this._makePopup = function () {
        var _container  = document.createElement('div');
        var _body       = document.createElement('div');
        var _footer     = document.createElement('div');

        _container.style.width              = '150px';
        _container.style.zIndex             = '9999';
        _container.style.top                = '200px';
        _container.style.position           = 'absolute';
        _container.style.border             = '1px solid #000';
        _container.style.backgroundColor    = '#fff';
        _container.style.left               = Math.ceil($('body').width()/2 - parseInt(_container.style.width,10)/2) + 'px';
        _container.style.backgroundImage    = 'url("/widgets/images/wait_small.gif")';
        _container.style.backgroundRepeat   = 'no-repeat';
        _container.style.backgroundPosition = '50% 65%';

        _body.style.height                  = '124px';
        _body.style.lineHeight              = '37px';
        _body.style.textAlign               = 'center';

        _footer.style.height                = '24px';
        _footer.style.backgroundColor       = '#fff';
        _footer.style.backgroundImage       = 'url("/widgets/images/processbar/linesolid/linebody.png")';
        _footer.style.backgroundRepeat      = 'no-repeat';

        _container.appendChild(_body);
        _container.appendChild(_footer);

        return _container;
    }
    this.body = function () {
        return this.bar.firstChild;
    }
    this.footer = function () {
        return this.bar.lastChild;
    }

    this.imgWidth   = 1498;
    this.imgHeight  = 24;
    this.bar        = this._makePopup();
    this.barWidth   = parseInt(this.bar.style.width, 10);
    this.barHeight  = parseInt(this.bar.style.height, 10);
}

ProgressBarAdapterPopupSpin.prototype = new ProgressBarAdapterAbstract;
ProgressBarAdapterPopupSpin.prototype.constructor = ProgressBarAdapterPopupSpin;

ProgressBarAdapterPopupSpin.NAME        = "ProgressBarAdapterPopupSpin";
ProgressBarAdapterPopupSpin.VERSION     = "0.1";
ProgressBarAdapterPopupSpin.DESCRIPTION = "Popup Progress Bar with wait spinner";

/**
 *  show
 */
ProgressBarAdapterPopupSpin.prototype.show = function () {
    //console.log('show');

    $(this.bar).css('display', '');
    if ( this.provider().label() != this.label() ) {
        this.label(this.provider().label());
        this.percent(this.provider().percent());

        $(this.body()).html(this.label());

        var pos = -1 * this.imgWidth + Math.ceil(this.barWidth * this.percent() / 100);
        $(this.footer()).css('background-position', pos+'px 50%');
    }
}
/**
 *  hide
 */
ProgressBarAdapterPopupSpin.prototype.hide = function () {
    //console.log('hide');

    $(this.bar).css('display', 'none');
    $(this.body()).text('');
    this.label('');
    this.percent(0);
}
/**
 *  prepare
 */
ProgressBarAdapterPopupSpin.prototype.prepare = function () {
    //console.log('prepare');

    $(this.footer()).css('background-position', '-'+this.imgWidth+'px 50%');  //  dependent on image !!!
    document.body.appendChild(this.bar);
}
/**
 *  postfree 
 */
ProgressBarAdapterPopupSpin.prototype.postfree = function () {
    //console.log('postfree');
    document.body.removeChild(this.bar);
}

