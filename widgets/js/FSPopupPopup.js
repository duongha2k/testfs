function FSPopupPopup(params) {
    FSPopupAbstract.call(this, params);
    
    this.waitImage = new Image();
    this.waitImage.src = "/widgets/images/wait.gif";
    /**
     *  Number of delay seconds
     */
    this._delay     = 0;
    /**
     *  ID of setTimeout function
     */
    this._delay_id  = null;
    /**
     *  Set/Get delay
     *
     *  @param int d
     *  @return int
     */
    this.delay = function ( d ) {
        if ( typeof d === 'number' ) {
            this._delay = d;
        }
        return this._delay;
    }
    /**
     *  isUserNoWaitDelay
     */
    this.isUserNoWaitDelay = function () {
        if ( this._delay_id !== null ) {
            clearTimeout(this._delay_id);
        }
        this._delay_id = null;
    }
}

FSPopupPopup.prototype = new FSPopupAbstract;
FSPopupPopup.prototype.constructor = FSPopupPopup;

FSPopupPopup.NAME = 'FSPopupPopup';
FSPopupPopup.VERSION = '0.1';
FSPopupPopup.DESCRIPTION = 'Class for display popups like "alert()"';


/**
 *  show
 *
 *  Function display roll popup
 *
 *  @param Node element Link when mouse was clicked
 *  @param Event event Event object
 *  @param object params Optional params for widget
 *      params.width        - roll width, int
 *      params.height       - roll height, int
 *      params.title        - roll title text, html
 *      params.body         - roll body text, html
 *      params.delay        - roll displaying delay, seconds
 *      params.preHandler   - Handler function, it will be called before any the Roll initialization
 *      params.handler      - Handler function, it will be called after all the Roll initializations, but before display
 *      params.postHandler  - Handler function, it will be called after the Roll displayed
 *      params.context      - Object, if set all Handlers will be call in context of this object
 */
FSPopupPopup.prototype.show = function (element, event, params) {


    params = params || {};

    /** - DELAY - **/
    if ( typeof params.delay === 'number' && params.delay > 0 || this.delay() > 0 ) {
        var self = this;
        $(element).bind('mouseleave', function () { self.isUserNoWaitDelay(); });
        var delay = this.delay(params.delay);
        this.delay(0); params.delay = 0;
        this._delay_id = setTimeout(function () { self.show(element, event, params) }, delay * 1000);
        return;
    } else {
        $(element).unbind('mouseleave');
    }
    /** - DELAY - **/

    $('body').css('cursor', 'wait');
    $(this.container()).css('height', '');

    if ( typeof params.title === 'string' ) { // set container title
        this.header(params.title);
    }

    //  Set wait image indicator and popup width to image's width
    this.body("<img src='"+this.waitImage.src+"' alt='loading...' width='"+this.waitImage.width+"px' />");
    this.width(this.waitImage.width);

    if ( typeof params.width === 'number' )  {
        params.width = Math.ceil(params.width);
    } else {
        params.width = this.params.width;
    }
    if ( typeof params.height === 'number' ) {
        params.height = Math.ceil(params.height);
    }
    
    //var vW = $('body').width();
    //var vH = $('body').height();
    //var fW = $(document).width();
    //var fH = $(document).height();
    var eT = Math.ceil($(element).offset().top);
    var eL = Math.ceil($(element).offset().left);

    $(this.header()).css('width', $(this.container()).width()-20); //  20 pixels for roll close button
    this.top(eT);
    this.left(eL);
    this.autohide(params.autohide);

    //  Display popup
    $(this.container()).css('z-index', ++FSPopupPopup.prototype.zindex);
    $(this.container()).css('display', '');

    /**
     *  set autohide handler
     */
    if ( this.autohide() ) {
        $(this.container()).bind('mouseenter mouseleave', {roll:this}, this.hideOnMouseOver);
    }


    //  We can call any preHandler function before set popup's body or title
    //  preHandler is called with 2 params:
    //      1 - current object
    //      2 - object params, which set as param into show function
    if ( params.preHandler !== undefined && typeof params.preHandler === 'function' ) { // call preHeandler
        if ( params.context !== undefined && typeof params.context === 'object' )
            params.preHandler.call(params.context, this, params);
        else
            params.preHandler(this, params);
    }

}


/**
 *  hide
 *
 *  Hide roll popup
 *
 *  @param Object options Optional
 *      options.preHandler  - handler function, which will be called before hide the roll popup
 *      options.postHandler - handler function, which will be called after hide the roll popup
 *      options.context     - object, which will be set as global context for called handlers
 */
FSPopupPopup.prototype.hide = function (options) {
    options = options || {};

    if ( options.preHandler !== undefined && typeof options.preHandler === 'function' ) {
        if ( options.context !== undefined && typeof options.context === 'object' ) {
            options.preHandler.call(options.context, this, options);
        } else {
            options.preHandler(this, options);
        }
    }

    $(this.container()).css('display', 'none');
    this.body('');
    this.header('');

    /**
     *  if close roll, clear all previous binds,
     *  and each roll has autohide == false by default
     */
    if ( this.autohide() ) {
        this.autohide(false);
        $(this.container()).unbind('mouseenter mouseleave');
    }

    if ( options.postHandler !== undefined && typeof options.postHandler === 'function' ) {
        if ( options.context !== undefined && typeof options.context === 'object' ) {
            options.postHandler.call(options.context, this, options);
        } else {
            options.postHandler(this, options);
        }
    }

    $('body').css('cursor', '');
}

/**
 *  init
 *
 *  Call once  when container will be created in DOM
 *
 *  @return void
 */
FSPopupPopup.prototype.init = function () {
    //  no any additional initialization
}

/**
 *  hideOnMouseOver
 *
 *  Handler for Rolls for mouseOut event,
 *  it can be set in init method
 *
 *  @param Event e Event object
 *  @return void
 */
FSPopupPopup.prototype.hideOnMouseOver = function (e) {
    // this - Div Node, Roll container
    switch ( e.type.toLowerCase() ) {
        case 'mouseenter' :
            if ( e.data.roll.timer() !== null ) {
                clearTimeout(e.data.roll.timer());
            }
            e.data.roll.timer(null);
            break;
        case 'mouseleave' :
            e.data.roll.timer(setTimeout(function () { e.data.roll.hide(); }, e.data.roll.timerWait()*1000));
            break;
    }
}
