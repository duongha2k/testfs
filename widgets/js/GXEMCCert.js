/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



//-------------------------------------------------------------
//-------------------------------------------------------------
//-------------------------------------------------------------
GXEMCCert.Instances = null;
//-------------------------------------------------------------
GXEMCCert.CreateObject = function(config) {
    var obj = null;
    if (GXEMCCert.Instances != null) {
        obj = GXEMCCert.Instances[config.id];
    }
    if (obj == null) {
        if (GXEMCCert.Instances == null) {
            GXEMCCert.Instances = new Object();
        }
        obj = new GXEMCCert(config);
        GXEMCCert.Instances[config.id] = obj;
    }

    obj.init();

    return obj;
}
//-------------------------------------------------------------
function GXEMCCert(config) {
    var Me = this;
    this.id = config.id;
    this.techid = config.techid;
    this.cbxindex = 0;
    this.today = config.today;
    
    this.cmdEMCCert = function()
    {
        var content = "<div id='divContentEMC' style='width:430px;'><div style='height:100px;text-align:center;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>";
        $("<div></div>").fancybox(
        {
            'autoDimensions' : true,
            'showCloseButton' :true,
            'hideOnOverlayClick' : false,
            'width' : 430,
            'height' : 200,
            'content': content
        }
        ).trigger('click');
        var data = "";
        if(typeof(Me.techid) != 'undefined')
        {
            data = 'techid='+Me.techid;
        }    
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/popup/emc-cert",
            data: data,
            success:function( html ) {
                jQuery("#divContentEMC").html(html);
                jQuery(".datetimeEMCCert").datepicker({
                    dateFormat: 'mm/dd/yy'
                });
                jQuery('#cmdAddNewEMC').unbind('click', Me.cmdAddNewEMC)
                .bind('click', Me.cmdAddNewEMC);
                jQuery('#cmdSaveEMC').unbind('click', Me.cmdSaveEMC)
                .bind('click', Me.cmdSaveEMC);
                
                Me.cbxindex = jQuery('#hidNumCbx').val();
                jQuery('.cbxEMCCert').unbind('change', Me.cbxEMCCert_onchange)
                .bind('change', Me.cbxEMCCert_onchange);
                jQuery('.cbxEMCCert').unbind('click', Me.cbxEMCCert_click)
                .bind('click', Me.cbxEMCCert_click); 
                jQuery('#cmdCancelEMC').unbind('click', Me.cmdCancelEMC)
                .bind('click', Me.cmdCancelEMC); 
                
                jQuery('.cbxEMCCert').each(function() {
                    jQuery(this).find('option').tsort({
                        order:'asc',
                        attr:'sort'
                    }); 
                });
            }
        });     
    }
    //------------------------------------------------------------
    this.cmdAddNewEMC = function()
    {
        var html = jQuery("#hidTrMEC").val();
        Me.cbxindex++;
        html = html.replace('[cbxEMCCertBH]',Me.cbxindex);
        jQuery("#tableEMC").find('tbody').append(html);
        jQuery(".datetimeEMCCert").datepicker({
            dateFormat: 'mm/dd/yy'
        });
        var el = jQuery('.cbxEMCCert[cbxid='+Me.cbxindex+']');
        
        jQuery('.cbxEMCCert').each(function(){
            var cbxid = jQuery(this).attr('cbxid');
            var val = jQuery(this).val();
            if(cbxid != Me.cbxindex && val!= "")
            {
                jQuery(el).find("option[value='"+val+"']").remove();
            }    
        });
        if(jQuery('.cbxEMCCert').length >= jQuery("#hidNumCbxOption").val())
        {
            jQuery('#cmdAddNewEMC').hide();
        }    
        jQuery('.cbxEMCCert').unbind('change', Me.cbxEMCCert_onchange)
        .bind('change', Me.cbxEMCCert_onchange);
        jQuery('.cbxEMCCert').unbind('click', Me.cbxEMCCert_click)
        .bind('click', Me.cbxEMCCert_click);        
        jQuery(el).find('option').tsort({
            order:'asc',
            attr:'sort'
        }); 
    }
    //------------------------------------------------------------
    this.cbxEMCCert_onchange = function()
    {
        var el = this;
        var cbxidCurrent = jQuery(this).attr('cbxid');
        var valCurrent = jQuery(this).val();
        var htmlCurrent = jQuery(this).find('option:selected').html();
        
        if(valCurrent == Me.cbxValue) return false;
        
        jQuery('.cbxEMCCert').each(function(){
            var cbxid = jQuery(this).attr('cbxid');
            var val = jQuery(this).val();
            if(cbxid != cbxidCurrent && valCurrent != "")
            {
                jQuery(this).find("option[value='"+valCurrent+"']").remove();
            }
            if(cbxid != cbxidCurrent && valCurrent != Me.cbxValue && Me.cbxValue != "")
            {
                jQuery(this).append('<option sort="'+Me.cbxHtml+'" value="'+Me.cbxValue+'">'+Me.cbxHtml+'</option>');
            }  
            jQuery(this).find('option').tsort({
                order:'asc',
                attr:'sort'
            }); 
        });
    }
    this.cbxValue = "";
    this.cbxHtml = "";
    this.cbxEMCCert_click = function()
    {
        Me.cbxValue = jQuery(this).val();
        Me.cbxHtml = jQuery(this).find('option:selected').html();
    }
    //------------------------------------------------------------
    this.cmdSaveEMC = function()
    {
        // check valid
        jQuery("#EMCError").html("");
        var data = "";
        var el = null;
        var i = 0;
        jQuery("#tableEMC tbody tr").each(function(){
            var value = jQuery(this).find('.cbxEMCCert').val();
            var date = jQuery(this).find('.datetimeEMCCert').val();
            jQuery(this).find('.datetimeEMCCert').removeClass('errorQT');
            jQuery(this).find('.cbxEMCCert').removeClass('errorQT');
            if(jQuery.trim(value) == "")
            {
                jQuery(this).find('.cbxEMCCert').addClass('errorQT');
                if(i == 0) el = jQuery(this).find('.cbxEMCCert');
                i++;
            }  
            if(jQuery.trim(date) == "")
            {
                jQuery(this).find('.datetimeEMCCert').addClass('errorQT');
                if(i== 0) el = jQuery(this).find('.datetimeEMCCert');
                i++;
            }
            data += ',{"value":"'+Base64.encode(value)+'","date":"'+Base64.encode(date)+'"}';
        });
        if(i>0)
        {
            jQuery("#EMCError").html('You missed '+i+' fields. They have been highlighted below');
            jQuery(el).focus();
            return false;
        }    
        
        //save
        if(data != "")
        {
            data = data.substring(1, data.length);
        }    
        var te = "";
        if(typeof(Me.techid) != 'undefined')
        {
            te += '&techid='+Me.techid;
        } 
        $.ajax({
            type: "POST",
            url: "/widgets/dashboard/popup/emc-cert",
            data: 'json='+data+te,
            success:function( html ) {
                window.location.reload();
            }
        });   
    }
    //------------------------------------------------------------
    this.cmdCancelEMC = function()
    {
        $.fancybox.close();
    }
    this.data = [];
    //-----------------------------------------------------------
    this.parserDataEMC = function()
    {
        editingSection['cred'] = true;
        var json = "";
        jQuery(".EMCCertDateInput").each(function()
        {
            json += ',{"key":"'+jQuery(this).attr("idemc")+'","value":"'+jQuery(this).val()+'"}';
        });    
        if(json != "") json = "["+json.substring(1,json.length)+"]";
        jQuery("#EMC").val(json);
    }
    //-----------------------------------------------------------
    this.buildData = function(data,index)
    {
        jQuery("#cbxEMC").html(data.htmlEMC);
        for(var j= 0; j< data.techInfo[index].length;j++)
        {
            var _date  = data.techInfo[index][j].date;
            if(jQuery.trim(_date) == "") _date = "&nbsp;";
            else
            {
                var part = data.techInfo[index][j].date.split(/[- :]/);
                bgLiteDate = new Date(part[0], part[1]-1, part[2]);
                var _month = bgLiteDate.getMonth()+1;
                var _day = bgLiteDate.getDate();
                var _smonth = _month < 10? "0"+_month.toString(): _month.toString();
                var _sday = _day < 10? "0"+_day.toString(): _day.toString();
                                    
                var _date= _smonth + "/" + _sday + "/" + bgLiteDate.getFullYear();
            }
            jQuery('#EMCCertDisplay').append('<div class="EMCCertDisplay" idemc="'+data.techInfo[index][j].name+'">'+data.techInfo[index][j].name+'</div>');
            jQuery('#EMCCertDisplay').append('<div style="display:none;" class="EMCCertDisplayLable"><img textemc="'+data.techInfo[index][j].label+'" idemc="'+data.techInfo[index][j].name+'" style="cursor: pointer;" class="cmdDeleteEMC" src="/widgets/images/Remove-an-EMC-Cert_icon.png"/>&nbsp;'+data.techInfo[index][j].label+'</div>');
            jQuery('#EMCCertDateDisplay').append('<div class="EMCCertDateDisplay" idemc="'+data.techInfo[index][j].name+'">'+_date+'</div>');
            jQuery('#EMCCertDateDisplay').append('<input size="15" idemc="'+data.techInfo[index][j].name+'" style="display:none;margin:1px;" type="text" class="EMCCertDateInput" value="'+_date+'">');
            $(".EMCCertDateInput").calendar({dateFormat:'MDY/'});
            jQuery("#cbxEMC option[value='"+data.techInfo[index][j].name+"']").remove();
            var dt = [];
            dt["name"] = data.techInfo[index][j].name;
            dt["date"] = _date;
            Me.data[j] = dt;
        }
        
       
       jQuery("#cmdEMCCert").attr("href","https://education.emc.com/guest/certification/default.aspx");
       
       jQuery("#cbxEMC").unbind("change",Me.cbxEMCOnchange).bind("change",Me.cbxEMCOnchange);
       jQuery(".cmdDeleteEMC").unbind("click",Me.cmdDeleteEMC).bind("click",Me.cmdDeleteEMC);
    }
    this.cmdDeleteEMC = function()
    {
        var idemc = jQuery(this).attr("idemc");
        var textemc = jQuery(this).attr("textemc");
        jQuery("#cbxEMC").append("<option value='"+idemc+"'>"+textemc+"</option>");
        jQuery(this).parent().remove();
        jQuery(".EMCCertDateInput[idemc='"+idemc+"']").remove();
        jQuery(".EMCCertDisplay[idemc='"+idemc+"']").remove();
        jQuery(".EMCCertDateDisplay[idemc='"+idemc+"']").remove();
        
    }
    this.cbxEMCOnchange = function()
    {
        var name = jQuery(this).val();
        var value = jQuery(this).children("option:selected").html();
        if(name == "") return false;
        jQuery('#EMCCertDisplay').append('<div class="EMCCertDisplay EMCCertDisplayNew"><img idemc="'+name+'" textemc="'+value+'" style="cursor: pointer;" class="cmdDeleteEMC" src="/widgets/images/Remove-an-EMC-Cert_icon.png"/>&nbsp;'+value+'</div>');
        jQuery('#EMCCertDateDisplay').append('<input size="15" idemc="'+name+'" style="margin:1px;" type="text" class="EMCCertDateInput" value="'+Me.today+'">');
        jQuery("#cbxEMC option[value='"+name+"']").remove();
        $(".EMCCertDateInput").calendar({dateFormat:'MDY/'});
        jQuery(".cmdDeleteEMC").unbind("click",Me.cmdDeleteEMC).bind("click",Me.cmdDeleteEMC);
        jQuery(this).children("option[value='']").attr("selected","selected");
    }
    this.myCertificationsEditLink = function()
    {
        jQuery("#myCertifications").css("width","600px");
        jQuery("#myCertifications").find("td").each(function(){
           jQuery(this).attr("align","left"); 
        });
        jQuery("#myCertifications #thfir").attr("width","150px");
        jQuery("#myCertifications #thsec").attr("width","300px");
        jQuery("#EMCCertDisplay .EMCCertDisplayLable").show();
        jQuery("#EMCCertDisplay .EMCCertDisplay").hide();
        jQuery("#EMCCertDisplay .EMCCertDisplayNew").show();
    }
    this.myCertificationscancelLink = function()
    {
        jQuery("#myCertifications").css("width","485px");
        jQuery("#myCertifications").find("tr").each(function(){
           jQuery(this).find("td").each(function(i) {
               if(i > 0)
                    jQuery(this).attr("align","center"); 
           });
        });
        jQuery("#myCertifications #thfir").removeAttr("width");
        jQuery("#myCertifications #thsec").removeAttr("width");
        jQuery("#EMCCertDisplay .EMCCertDisplayLable").hide();
        jQuery("#EMCCertDisplay .EMCCertDisplay").show();
        jQuery("#EMCCertDisplay .EMCCertDisplayNew").hide();
    }
    this.init = function() {
    // config 
        
            jQuery('#myCertificationsEditLink').unbind('click', Me.myCertificationsEditLink)
              .bind('click', Me.myCertificationsEditLink);
             jQuery('#myCertificationscancelLink').unbind('click', Me.myCertificationscancelLink)
              .bind('click', Me.myCertificationscancelLink);
    }
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
//------------------------------------------------------------
}


