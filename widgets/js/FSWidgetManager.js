function FSWidgetManager( )
{
    this._widgets = {length:0};
    this._byKeyRef = {};
}

/**
 *  add
 *
 *  @param FSWidget widget
 *  @param string key = null
 */
FSWidgetManager.prototype.add = function (/* FSWidget */ widget, key /* = null */)
{
    try {
        var idx;
        if ( widget instanceof FSWidget ) {
            /* Fill this._widgets array */
            if ( this._widgets.length > 0 ) {
                for ( idx in this._widgets ) {
                    if ( this._widgets[idx] === widget || idx === widget.params.container) {
                        throw new Error('Widget already in Stack');
                    }
                }
            }
            this._widgets[widget.params.container] = widget;
            this._widgets.length++;

            /* Key reference */
            var keyPattern = /^[a-z]([a-z0-9]+)?/i;
            if ( typeof key === 'string' && keyPattern.test(key) ) {
                for ( idx in this._byKeyRef ) {
                    if ( idx === key ) {
                        throw new ReferenceError("Key already in use: '" + key + "'");
                    }
                }
                this._byKeyRef[key] = this._widgets[widget.params.container];
            } else if ( key !== undefined ) {
                throw new TypeError('Incorrect key type, should be a string of latin letters and numbers, but "' + key + '" given');
            }

            return true;
        } else {
            throw new Error('parameter should be instance of FSWidget class');
        }
    } catch ( e ) {
        if ( e instanceof TypeError || e instanceof ReferenceError ) {
            throw e;
        }
        return false;
    }
}

/**
 *  get
 *
 *  @param DOMNode|key
 */
FSWidgetManager.prototype.get = function ( link )
{
    var idx, e = link;
    if ( !link ) return null;
    if ( this._widgets.length > 0 ) {
        if ( typeof link === 'string' ) {
            if ( this._byKeyRef[link] !== undefined ) {
                return this._byKeyRef[link];
            }
        } else if ( typeof link === 'object' ) {
            while ( true ) {
                e = $(e).parent()[0];
                if (!e) {
                    break;
                }
                if (!e.id || e.id.length == 0) {
                    continue;
                }
                for ( idx in this._widgets ) {
                    if ( idx == e.id) {
                        return this._widgets[idx];
                    }
                }
            }
        }
    }else{ //supporting old wd.
        return wd;
    }
    return null;
}

