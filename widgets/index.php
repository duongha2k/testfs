<?php

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../wdata/application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    realpath(APPLICATION_PATH . '/../../includes/classes'),
    get_include_path(),
)));

/*
// TODO Che Debug
$_SERVER['REQUEST_URI'] = str_replace('/dashboard/', '/admindashboard/', $_SERVER['REQUEST_URI']);
$_SERVER['SCRIPT_NAME'] = str_replace('/dashboard/', '/admindashboard/', $_SERVER['SCRIPT_NAME']);
*/

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

// TODO Che Must to rewrite to more clear code
try {
    $autoloader = Zend_Loader_Autoloader::getInstance();
    $autoloader->registerNamespace('Core_');
    $autoloader->registerNamespace('API_');
    $autoloader->registerNamespace('Salesforce_');
    $autoloader->registerNamespace('PHPExcel_');
    $autoloader->registerNamespace('WorkMarket');
    $autoloader->registerNamespace('Browscap');
    $application->bootstrap()
                ->run();
    
} catch(Core_Auth_Exception $exc) {
    // no authorisation
    $frontController = $application->getBootstrap()->getResource('FrontController');
    $request = $frontController->getRequest(); 
    if ($request->isXmlHttpRequest() ) {
        echo 'authentication required';
        exit;
    } else {
        // start session back
        @session_start();
        $goBack = $request->getRequestUri();
        if (! (isset($_SESSION['_prev_redirectURL']) && $goBack == $_SESSION['_prev_redirectURL']) ) {
            $_SESSION['redirectURL'] = $goBack;
        }
        $redirectTo = '';
        if (mb_strstr($request->getRequestUri(), '/admindashboard/') !== false) {
            $redirectTo = '/admin/';
        } else if (mb_strstr($request->getRequestUri(), '/dashboard/tech') !== false) {
            $redirectTo = '/techs/';
        } else {
             $redirectTo = '/clients/';
        }
    }
    $responce = $frontController->getResponse();
    $responce->clearAllHeaders();
    $responce->setRedirect($redirectTo);
    $responce->sendResponse();
} catch (Zend_Exception $exc) {
    if (constant('APPLICATION_ENV') != 'production') {
        echo $exc;
    } else {
//        echo 'Unhandled error. Code: ' . $exc->getCode();
		error_log("unhandled: " . $exc->getMessage());
		try {
			$notifySender = new Core_Mail();
			$notifySender->setFromName("FieldSolutions");
			$notifySender->setFromEmail("nobody@fieldsolutions.com");
			$notifySender->setToEmail("tngo@fieldsolutions.com");
			$notifySender->setSubject("Unhandled Error");
			$notifySender->setBodyText($exc->getMessage());
			$notifySender->send();
		} catch (Exception $e) {}
    }   

    if (!($exc instanceof Core_Exception) ) {
        Core_Exception::makeExceptionLog($exc);
    }
}
