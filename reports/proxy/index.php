<?php
require_once 'config.php';

require_once(realpath(dirname(__FILE__)) . "/../../htdocs/library/PHPSessionHandler.php");
$phpSessionHandler = new Core_PHPSessionHandler();

session_name('fsTemplate');
session_start();
if (isset($_GET['autoreport']) && $_GET['autoreport'] == 'r3p0rtaUt0') {
	$_SESSION['loggedIn'] = 'yes';
	$_SESSION['loggedInAs'] = 'admin';
}
if (empty($_SESSION['loggedIn']) || $_SESSION['loggedIn'] != 'yes' || ($_SESSION['loggedInAs'] != 'client' && $_SESSION['loggedInAs'] != 'accounting' && $_SESSION['loggedInAs'] != 'admin' && $_SESSION['loggedInAs'] != 'customers') || ($_SESSION['loggedInAs'] == 'client' && empty($_SESSION['Company_ID']) )) {
	header("Location: /");
	exit;
}

header('Cache-Control: must-revalidate, post-check=0, pre-check=0');

$requestUri = $_SERVER['REQUEST_URI'];
if (strpos($requestUri, $proxyPath) !== 0) {
	throw new Exception("Invalid configuration");
}
$requestUri = substr($requestUri, strlen($proxyPath));
$getPos = strpos($requestUri, '?');
if ($getPos !== false) {
	$requestUri = substr($requestUri, 0, $getPos);
}

$_GET['userid'] = $biUserid;
$_GET['password'] = $biPassword;

switch ($_SESSION['loggedInAs']) {
	case 'accounting':
		$_GET['solution'] = $solutionAccounting;
		break;
	case 'admin':
		$_GET['solution'] = $solutionAdmin;
		break;
	case 'customers':
		$_GET['solution'] = $solutionCustomer;
		break;
	default:
		$_GET['solution'] = $solution;
}
$_GET['path'] = $path;
if ($_SESSION['loggedInAs'] == 'client') {
	$_GET['Company_ID'] = $_SESSION['Company_ID'];
	if (isset($_SESSION['PM_Company_ID']) && $_SESSION['PM_Company_ID']) {
		$_GET['Company_ID'] = $_SESSION['PM_Company_ID'];
	}
}
if ($_SESSION['loggedInAs'] == 'accounting' && isset($_COOKIE['updateDate'])) {
	$_GET['updateDate'] = $_COOKIE['updateDate'];
}
if ($_SESSION['loggedInAs'] == 'customers') {
	$_GET['Company_ID'] = $_SESSION['Company_ID'];
	$_GET['CustomerID'] = $_SESSION['CustomerID'];
}
unset($_POST['Company_ID']);

$params = array();
foreach ($_GET as $param => $value) {
	$params[] = urlencode($param) . '=' . urlencode($value);
}

if (preg_match('/^\/getImage/', $requestUri)) $requestUri = '/../../' . $requestUri;
$requestUri = $reportsPath . $requestUri . '?' . implode('&', $params);

$ch = curl_init($requestUri);

$hears = array(
    'Accept-Language: en-us,en;q=0.8',
    'Accept-Encoding: gzip,deflate',
    'Accept-Charset: utf-8;q=0.7,*;q=0.7'
);

curl_setopt($ch, CURLOPT_HTTPHEADER, $hears);

curl_setopt($ch, CURLOPT_HEADERFUNCTION, 'read_header');

if (count($_POST) > 0) {
	curl_setopt ($ch, CURLOPT_POST, true);
	curl_setopt ($ch, CURLOPT_POSTFIELDS, $_POST);
}

//curl_exec($ch);
//curl_close($ch);
ob_start();

curl_exec($ch);
curl_close($ch);

$buffer = ob_get_contents();
$buffer = str_replace('<link type="text/css" rel="stylesheet" href="/http://reports.saturn.fieldsolutions.com:8080/pentaho/', '<link type="text/css" rel="stylesheet" href="http://reports.saturn.fieldsolutions.com:8080/pentaho/', $buffer);
ob_end_clean();
echo $buffer;

function read_header($ch, $string)
{
    $length = strlen($string);
	if (strncmp($string, 'content-disposition', 19) == 0) {
		header($string);
	}
    if (strncmp($string, 'Content-Type', 12) == 0) {
		header($string);
	}
	if (strncmp($string, 'Content-Length', 14) == 0) {
		header($string);
	}

	return $length;
}
