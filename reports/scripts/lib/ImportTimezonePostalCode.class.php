<?php
require_once 'ImportLongList.class.php';

class ImportTimezinePostalCode extends ImportLongList {
	public function __construct(Zend_Db_Adapter_Abstract $db) {
		parent::__construct($db, 'Timezone_PostalCode', 'zipcodes', 'id', array(), array('DST'), 2000);

		unset($this->insertFields['id']);
		$this->insertFields['PostalCode'] = 'ZIPCode';
	}

	protected function insertToDest($data) {
		if ($this->db->fetchOne("SELECT ZIPCode FROM {$this->destinationTable} WHERE ZIPCode=?", array($data['ZIPCode']))) {
			$this->db->update($this->destinationTable, $data, 'ZIPCode=' . $this->db->quote($data['ZIPCode']));
		} else {
			$this->db->insert($this->destinationTable, $data);
		}
	}

}
