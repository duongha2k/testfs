<?php
require_once 'ImportAbstract.class.php';

class ImportShippingCarriers extends ImportAbstract {
	public function __construct(Zend_Db_Adapter_Abstract $db) {
		parent::__construct($db, 'Shipping_Carriers', 'shipping_carriers', 'UNID');
	}
}
