<?php
require_once 'ImportLongList.class.php';

class ImportWOFix2 extends ImportLongList {
	public function __construct(Zend_Db_Adapter_Abstract $db, $statusFile) {

		$datesToConvert = array(
			'DateEntered',
			'StartDate',
			'EndDate',
			'DatePaid',
			'DateNotified',
			'DateInvoiced',
			'DateApproved',
			'CallDate',
			'Date_Assigned',
			'lastModDate',
			'Date_Completed',
			'DeactivatedDate',
			'DateIncomplete',
			'ACSDateInvoiced',
			'Date_In',
			'Date_Out',
			'Date_Checked_in',
			'SourceByDate'
		);

		$booleanFields = array(
			'Approved',
			'ShowTechs',
			'TechMarkedComplete',
			'TechPaid',
			'CheckedIn',
			'StoreNotified',
			'Invoiced',
			'WorkOrderReviewed',
			'Deactivated',
			'TechCheckedIn_24hrs',
			'TechCalled',
			'Update_Requested',
			'Work_Out_of_Scope',
			'Site_Complete',
			'MovedToMysql',
			'AutoBlastOnPublish',
			'ExpediteFee',
			'AbortFee',
			'AfterHoursFee',
			'Urgent',
			'FSSourcingSupport',
			'PricingRan',
			'ReminderAcceptance',
			'Reminder24Hr',
			'Reminder1Hr',
			'CheckInCall',
			'ReminderNotMarkComplete',
			'ReminderIncomplete',
			'ReminderUseWOSetting',
			'isProjectAutoAssign',
			'isWorkOrdersFirstBidder',
			'ReminderAll',
			'CheckOutCall',
			'ACSInvoiced',
			'SMSBlast',
			'P2TPreferredOnly',
			'P2TAlreadyAssigned',
			'CallClosed',
			'ContactHD',
			'ShortNotice',
			'Lead',
			'Assist',
			'SignOffSheet_Required',
			'Unexpected_Steps',
			'PreCall_2',
			'Incomplete_Paperwork',
			'Paperwork_Received',
			'PreCall_1',
			'FLS_OOS',
			'PartsProcessed',
			'PcntDeduct',
			'TBPay',
			'UseMyRecruitEmailSettings'
		);
		parent::__construct($db, 'Work_Orders', 'work_orders', 'WIN_NUM', $datesToConvert, $booleanFields, 500);

		$this->fieldListMap['Status'] = Core_WorkOrder_Status::getStatusQueryAsName();
		$this->insertFields['TB_UNID'] = 'WIN_NUM';
		$this->insertFields['Status'] = 'Status';
	}

	public function run() {
		$result = Core_Caspio::caspioSelectAdv($this->sourceTable, "MIN(TB_UNID),MAX(TB_UNID)", '', '', false);
		$errors = Core_Api_Error::getInstance();
		if ($errors->hasErrors()) {
			throw new Exception("Caspio errors:\n" . $errors->getErrors());
		}

		$min = $result[0][0];
		$max = $result[0][1];

		if ($min == 0 && $max == 0) {
			echo "No data";
			return;
		}

		echo "Max: $max\n";

		do {
			$result = $this->selectFromSource($min, $min + $this->step);

			foreach ($result as &$row) {
				$this->insertToDest($row);
			}

			$min += $this->step;
		} while ($min <= $max);
	}

	protected function selectFromSource($min = null, $max = null) {
		$where = array();
		if ($min !== null) {
			$where[] = "TB_UNID >= $min";
		}
		if ($max !== null) {
			$where[] = "TB_UNID < $max";
		}

		$result = Core_Caspio::caspioSelectWithFieldListMap($this->sourceTable, $this->fieldListMap, implode(' AND ', $where), '');
		$errors = Core_Api_Error::getInstance();
		if ($errors->hasErrors()) {
			throw new Exception("Caspio errors:\n" . $errors->getErrors());
		}

		echo "Min: $min, Max: $max\n";
		if ($result === null) return array();
		foreach ($result as &$row) {
			$row = $this->sourceArray2Dest($row);
		}
		return $result;
	}
}
