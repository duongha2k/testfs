<?php
require_once 'ImportAbstract.class.php';

class ImportCustomers extends ImportAbstract {
	public function __construct(Zend_Db_Adapter_Abstract $db) {
		parent::__construct($db, 'Customers', 'customers', 'UNID');
	}
}
