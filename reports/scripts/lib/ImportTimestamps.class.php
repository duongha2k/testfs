<?php
require_once 'ImportLongList.class.php';

class ImportTimestamps extends ImportLongList {
	public function __construct(Zend_Db_Adapter_Abstract $db) {
		$dateFields = array(
			'DateTime_Stamp'
		);

		parent::__construct($db, TABLE_WORK_ORDER_TIMESTAMPS, 'timestamps', 'id', $dateFields, array(), 5000);

		$this->insertFields['WO_UNID'] = 'WIN_NUM';
	}

	protected function insertToDest($data) {
		if ($data['Username'] === null) $data['Username'] = '';

		parent::insertToDest($data);
	}

	public function run() {
		$result = Core_Caspio::caspioSelectAdv($this->sourceTable, "MIN({$this->destinationPK}),MAX({$this->destinationPK})", '', '', false);
		$errors = Core_Api_Error::getInstance();
		if ($errors->hasErrors()) {
			throw new Exception("Caspio errors:\n" . $errors->getErrors());
		}

		$min = $result[0][0];
		$max = $result[0][1];

		if ($min == 0 && $max == 0) {
			echo "No data";
			return;
		}

		echo "Max: $max\n";

		$maxImported = $this->db->fetchOne("SELECT MAX({$this->destinationPK}) FROM {$this->destinationTable}");
		if ($min < $maxImported) $min = $maxImported + 1;

		do {
			$result = $this->selectFromSource($min, $min + $this->step);

			foreach ($result as &$row) {
				$this->insertToDest($row);
			}

			$min += $this->step;
		} while ($min <= $max);
	}
}
