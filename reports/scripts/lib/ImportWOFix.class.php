<?php
class ImportWOFix {
	private $db;
	private $statusFile;

	private $fieldListMap;
	private $insertFields;
	private $datesToConvert;
	private $booleanFields;


	public function __construct(Zend_Db_Adapter_Abstract $db, $statusFile) {
		$this->db = $db;
		$this->statusFile = $statusFile;

		$sourceFields = $this->getSourceDBFields();
		$sourceFields = array_combine($sourceFields, $sourceFields);
		$destinationFields = $this->getDestinationDBFields();
		$destinationFields = array_combine($destinationFields, $destinationFields);

		$mapArray = array (
			'TB_UNID' => 'WIN_NUM',
			//'Pic1_FromTech' => 'PIC1_FromTech',
			//'Pic2_FromTech' => 'PIC2_FromTech',
			//'Pic3_FromTech' => 'PIC3_FromTech',
		);

		$this->fieldListMap = array_combine($sourceFields, $sourceFields);
		$this->fieldListMap['Status'] = Core_WorkOrder_Status::getStatusQueryAsName();

		$this->insertFields = array_intersect($destinationFields, $sourceFields);
		$this->insertFields += $mapArray;
		$this->insertFields['Status'] = 'Status';

		$this->datesToConvert = array(
			'DateEntered',
			'StartDate',
			'EndDate',
			'DatePaid',
			'DateNotified',
			'DateInvoiced',
			'DateApproved',
			'CallDate',
			'Date_Assigned',
			'lastModDate',
			'Date_Completed',
			'DeactivatedDate',
			'DateIncomplete',
			'ACSDateInvoiced',
			'Date_In',
			'Date_Out',
			'Date_Checked_in',
			'SourceByDate'
		);

		$this->booleanFields = array(
			'Approved',
			'ShowTechs',
			'TechMarkedComplete',
			'TechPaid',
			'CheckedIn',
			'StoreNotified',
			'Invoiced',
			'WorkOrderReviewed',
			'Deactivated',
			'TechCheckedIn_24hrs',
			'TechCalled',
			'Update_Requested',
			'Work_Out_of_Scope',
			'Site_Complete',
			'MovedToMysql',
			'AutoBlastOnPublish',
			'ExpediteFee',
			'AbortFee',
			'AfterHoursFee',
			'Urgent',
			'FSSourcingSupport',
			'PricingRan',
			'ReminderAcceptance',
			'Reminder24Hr',
			'Reminder1Hr',
			'CheckInCall',
			'ReminderNotMarkComplete',
			'ReminderIncomplete',
			'ReminderUseWOSetting',
			'isProjectAutoAssign',
			'isWorkOrdersFirstBidder',
			'ReminderAll',
			'CheckOutCall',
			'ACSInvoiced',
			'SMSBlast',
			'P2TPreferredOnly',
			'P2TAlreadyAssigned',
			'CallClosed',
			'ContactHD',
			'ShortNotice',
			'Lead',
			'Assist',
			'SignOffSheet_Required',
			'Unexpected_Steps',
			'PreCall_2',
			'Incomplete_Paperwork',
			'Paperwork_Received',
			'PreCall_1',
			'FLS_OOS',
			'PartsProcessed',
			'PcntDeduct',
			'TBPay',
			'UseMyRecruitEmailSettings'
		);
	}

	public function run() {
		$firstDate = $this->loadCurrentRequestDatetime();
		if (!$firstDate) {
			$firstDate = $this->getMinDatetime();
		}

		echo "Import nulls\n";
		$woData = $this->getWOByUpdateDate(null, null);
		foreach ($woData as &$source) {
			$source = $this->sourceArray2Dest($source);
		}

		foreach ($woData as $source) {
			$winNum = $source['WIN_NUM'];
			if ($this->db->fetchOne("SELECT WIN_NUM FROM work_orders WHERE WIN_NUM=?", array($winNum))) {
				$this->db->update('work_orders', $source, 'WIN_NUM=' . $this->db->quote($winNum));
			} else {
				$this->db->insert('work_orders', $source);
			}
		}


		$until = new Zend_Date();
		$until->addMonth(2);
		while ($firstDate->isEarlier($until)) {
			$this->saveCurrentRequestDatetime($firstDate);

			$lastDate = clone $firstDate;
			$lastDate->addDay(1);

			echo "First date: " . $firstDate . ", last date: " . $lastDate . "\n";

			$woData = $this->getWOByUpdateDate($firstDate, $lastDate);
			if (count($woData) > 0) {
				foreach ($woData as &$source) {
					$source = $this->sourceArray2Dest($source);
				}

				foreach ($woData as $source) {
					$winNum = $source['WIN_NUM'];
					if ($this->db->fetchOne("SELECT WIN_NUM FROM work_orders WHERE WIN_NUM=?", array($winNum))) {
						$this->db->update('work_orders', $source, 'WIN_NUM=' . $this->db->quote($winNum));
					} else {
						$this->db->insert('work_orders', $source);
					}
				}
				$firstDate = $lastDate;
			} else {
				$firstDate = $this->getMinDatetime($lastDate);
			}
		}
	}

	protected function loadCurrentRequestDatetime() {
		if (file_exists($this->statusFile)) {
			$result = file_get_contents($this->statusFile);
			if ($result === false) throw new Exception('Load status file ' . $this->statusFile . ' failed');
			return new Zend_Date($result, Zend_Date::ISO_8601);
		}
		return null;
	}

	protected function saveCurrentRequestDatetime($datetime) {
		if ($datetime instanceof Zend_Date) {
			$datetime = $datetime->toString(Zend_Date::ISO_8601);
		}
		file_put_contents($this->statusFile, $datetime);
	}

	protected function getDestinationDBFields() {
		$result = $this->db->fetchAll('DESCRIBE work_orders');
		$fields = array();
		foreach ($result as $row) {
			$fields[] = $row['Field'];
		}
		return $fields;
	}


	protected function getSourceDBFields() {
		$columns = Core_Caspio::caspioGetTableDesign('Work_Orders');
		$fieldList = array();

		foreach ($columns as $info) {
			$fieldInfo = explode(",",$info);
			$fieldList[] = trim($fieldInfo[0]);
		}

		return $fieldList;
	}

	protected function getWOByUpdateDate($firstDate, $lastDate) {
		$errors = Core_Api_Error::getInstance();
		if ($firstDate === null) {
			$result = Core_Caspio::caspioSelectWithFieldListMap('Work_Orders', $this->fieldListMap, "lastModDate IS NULL AND (StartDate IS NULL OR StartDate = '')", '');
		} else {
			$result = Core_Caspio::caspioSelectWithFieldListMap('Work_Orders', $this->fieldListMap, "lastModDate IS NULL AND StartDate>='" . $firstDate->toString('M/d/yyyy') . "' AND StartDate < '" . $lastDate->toString('M/d/yyyy') . "'", '');
		}
		if ($errors->hasErrors()) {
			throw new Exception("Caspio errors:\n" . $errors);
		}
		return $result;
	}


	protected function getMinDatetime(Zend_Date $notLower = null) {
		$result = Core_Caspio::caspioSelectAdv('Work_Orders', 'MIN(StartDate)', 'lastModDate IS NULL' . ($notLower? " AND StartDate > '" . $notLower->toString('M/d/yyyy h:mm:ss a') . "'" : ''), '');
		$errors = Core_Api_Error::getInstance();
		if ($errors->hasErrors()) {
			throw new Exception("Caspio errors:\n" . $errors->getErrors());
		}
		$firstRaw = $result[0];
		$val = reset($firstRaw);

		$date = new Zend_Date($val, 'M/d/yyyy', 'en');
		return $date;
	}

	protected function sourceArray2Dest($source) {
		$result = array();

		foreach ($this->insertFields as $skey => $dkey) {
			$result[$dkey] = $source[$skey];
		}

		foreach ($this->datesToConvert as $field) {
			if ($result[$field]) {
				$date = new Zend_Date($result[$field], 'M/d/yyyy h:mm:ss a', 'en');
				$result[$field] = $date->toString('yyyy-MM-dd HH:mm:ss');
			}
		}

		foreach ($this->booleanFields as $field) {
			if ($result[$field] == 'True') {
				$result[$field] = true;
			} else {
				$result[$field] = false;
			}
		}

		return $result;
	}
}
