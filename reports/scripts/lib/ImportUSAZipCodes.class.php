<?php
require_once 'ImportLongList.class.php';

class ImportUSAZipCodes extends ImportLongList {
	public function __construct(Zend_Db_Adapter_Abstract $db) {
		parent::__construct($db, 'USA_ZipCodes', 'zipcodes', 'ZIPCode', array(), array(), 200);
	}
}
