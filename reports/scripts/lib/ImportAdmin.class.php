<?php
require_once 'ImportAbstract.class.php';

class ImportAdmin extends ImportAbstract {
	public function __construct(Zend_Db_Adapter_Abstract $db) {
		parent::__construct($db, 'TR_Admin', 'admins', 'AdminID');
	}
}
