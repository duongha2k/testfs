<?php
require_once 'ImportAbstract.class.php';

class ImportClientDeniedTechs extends ImportAbstract {
	public function __construct(Zend_Db_Adapter_Abstract $db) {
		parent::__construct($db, 'Deactivated_Techs_By_Clients', 'client_denied_techs', 'id');
	}
}
