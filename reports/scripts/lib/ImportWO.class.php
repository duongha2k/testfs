<?php
class ImportWO {
	private $db;
	private $statusFile;

	private $fieldListMap;
	private $insertFields;
	private $datesToConvert;
	private $booleanFields;


	public function __construct(Zend_Db_Adapter_Abstract $db, $statusFile) {
		$this->db = $db;
		$this->statusFile = $statusFile;

		$sourceFields = $this->getSourceDBFields();
		$sourceFields = array_combine($sourceFields, $sourceFields);
		$destinationFields = $this->getDestinationDBFields();
		$destinationFields = array_combine($destinationFields, $destinationFields);

		$mapArray = array (
			'TB_UNID' => 'WIN_NUM',
			//'Pic1_FromTech' => 'PIC1_FromTech',
			//'Pic2_FromTech' => 'PIC2_FromTech',
			//'Pic3_FromTech' => 'PIC3_FromTech',
		);

		$this->fieldListMap = array_combine($sourceFields, $sourceFields);
		$this->fieldListMap['Status'] = Core_WorkOrder_Status::getStatusQueryAsName();

		$this->insertFields = array_intersect($destinationFields, $sourceFields);
		$this->insertFields += $mapArray;
		$this->insertFields['Status'] = 'Status';

		$this->datesToConvert = array(
			'DateEntered',
			'StartDate',
			'EndDate',
			'DatePaid',
			'DateNotified',
			'DateInvoiced',
			'DateApproved',
			'CallDate',
			'Date_Assigned',
			'lastModDate',
			'Date_Completed',
			'DeactivatedDate',
			'DateIncomplete',
			'ACSDateInvoiced',
			'Date_In',
			'Date_Out',
			'Date_Checked_in',
			'SourceByDate'
		);

		$this->booleanFields = array(
			'Approved',
			'ShowTechs',
			'TechMarkedComplete',
			'TechPaid',
			'CheckedIn',
			'StoreNotified',
			'Invoiced',
			'WorkOrderReviewed',
			'Deactivated',
			'TechCheckedIn_24hrs',
			'TechCalled',
			'Update_Requested',
			'Work_Out_of_Scope',
			'Site_Complete',
			'MovedToMysql',
			'AutoBlastOnPublish',
			'ExpediteFee',
			'AbortFee',
			'AfterHoursFee',
			'Urgent',
			'FSSourcingSupport',
			'PricingRan',
			'ReminderAcceptance',
			'Reminder24Hr',
			'Reminder1Hr',
			'CheckInCall',
			'ReminderNotMarkComplete',
			'ReminderIncomplete',
			'ReminderUseWOSetting',
			'isProjectAutoAssign',
			'isWorkOrdersFirstBidder',
			'ReminderAll',
			'CheckOutCall',
			'ACSInvoiced',
			'SMSBlast',
			'P2TPreferredOnly',
			'P2TAlreadyAssigned',
			'CallClosed',
			'ContactHD',
			'ShortNotice',
			'Lead',
			'Assist',
			'SignOffSheet_Required',
			'Unexpected_Steps',
			'PreCall_2',
			'Incomplete_Paperwork',
			'Paperwork_Received',
			'PreCall_1',
			'FLS_OOS',
			'PartsProcessed',
			'PcntDeduct',
			'TBPay',
			'UseMyRecruitEmailSettings'
		);
	}

	public function run() {
		$firstDate = $this->loadCurrentRequestDatetime();
		if (!$firstDate) {
			$firstDate = $this->getMinDatetime();
		}

		while ($firstDate->isEarlier(new Zend_Date())) {
			$this->saveCurrentRequestDatetime($firstDate);

			$lastDate = clone $firstDate;
			$lastDate->addDay(1);

			echo "First date: " . $firstDate . ", last date: " . $lastDate . "\n";

			$woData = $this->getWOByUpdateDate($firstDate, $lastDate);
			if (count($woData) > 0) {
				foreach ($woData as &$source) {
					$source = $this->sourceArray2Dest($source);
				}

				foreach ($woData as $source) {
					$winNum = $source['WIN_NUM'];
					if ($this->db->fetchOne("SELECT WIN_NUM FROM work_orders WHERE WIN_NUM=?", array($winNum))) {
						$this->db->update('work_orders', $source, 'WIN_NUM=' . $this->db->quote($winNum));
					} else {
						$this->db->insert('work_orders', $source);
					}
				}
				$firstDate = $lastDate;
			} else {
				$firstDate = $this->getMinDatetime($lastDate);
			}
		}
	}

	protected function loadCurrentRequestDatetime() {
		if (file_exists($this->statusFile)) {
			$result = file_get_contents($this->statusFile);
			if ($result === false) throw new Exception('Load status file ' . $this->statusFile . ' failed');
			return new Zend_Date($result, Zend_Date::ISO_8601);
		}
		return null;
	}

	protected function saveCurrentRequestDatetime($datetime) {
		if ($datetime instanceof Zend_Date) {
			$datetime = $datetime->toString(Zend_Date::ISO_8601);
		}
		file_put_contents($this->statusFile, $datetime);
	}

	protected function getDestinationDBFields() {
		$result = $this->db->fetchAll('DESCRIBE work_orders');
		$fields = array();
		foreach ($result as $row) {
			$fields[] = $row['Field'];
		}
		return $fields;
	}


	protected function getSourceDBFields() {
		$columns = Core_Caspio::caspioGetTableDesign('Work_Orders');
		$fieldList = array();

		foreach ($columns as $info) {
			$fieldInfo = explode(",",$info);
			$fieldList[] = trim($fieldInfo[0]);
		}

		return $fieldList;
	}

	protected function getWOByUpdateDate(Zend_Date $firstDate, Zend_Date $lastDate) {
		$errors = Core_Api_Error::getInstance();
		$result = Core_Caspio::caspioSelectWithFieldListMap('Work_Orders', $this->fieldListMap, "lastModDate>='" . $firstDate->toString('M/d/yyyy h:mm:ss a') . "' AND lastModDate < '" . $lastDate->toString('M/d/yyyy h:mm:ss a') . "'", '');
		if ($errors->hasErrors()) {
			throw new Exception("Caspio errors:\n" . $errors);
		}
		return $result;
	}


	protected function getMinDatetime(Zend_Date $notLower = null) {
		$result = Core_Caspio::caspioSelectAdv('Work_Orders', 'MIN(lastModDate)', ($notLower? "lastModDate > '" . $notLower->toString('M/d/yyyy h:mm:ss a') . "'" : ''), '');
		$errors = Core_Api_Error::getInstance();
		if ($errors->hasErrors()) {
			throw new Exception("Caspio errors:\n" . $errors->getErrors());
		}
		$firstRaw = $result[0];
		$val = reset($firstRaw);

		$date = new Zend_Date($val, 'M/d/yyyy', 'en');
		return $date;
	}

	protected function sourceArray2Dest($source) {
		$result = array();

		foreach ($this->insertFields as $skey => $dkey) {
			$result[$dkey] = $source[$skey];
		}

		foreach ($this->datesToConvert as $field) {
			if ($result[$field]) {
				$date = new Zend_Date($result[$field], 'M/d/yyyy h:mm:ss a', 'en');
				$result[$field] = $date->toString('yyyy-MM-dd HH:mm:ss');
			}
		}

		foreach ($this->booleanFields as $field) {
			if ($result[$field] == 'True') {
				$result[$field] = true;
			} else {
				$result[$field] = false;
			}
		}

		return $result;
	}
}

//Following classes ported from Caspio version of Core/WorkOrder/Status.php
class Core_WorkOrder_Status {
			// Created - ShowTechs = '0' AND ISNULL(Tech_ID, 0) = 0 AND Deactivated = '0'
			// Published - ShowTechs = '1' AND ISNULL(Tech_ID, 0) = 0 AND Deactivated = '0' AND ISNULL(FLS_ID,'') = ''
			// Assigned - ISNULL(Tech_ID, 0) != 0 AND Deactivated = '0' AND TechMarkedComplete = '0' AND ISNULL(MissingComments,'') = ''
			// Work Done - TechMarkedComplete = '1' AND Approved = '0' AND TechPaid = '0' AND Invoiced = '0' AND Deactivated = '0'
			// Approved - Approved = '1' AND Invoiced = '0' AND Deactivated = '0'
			// Incomplete - TechMarkedComplete = '0' AND Approved = '0' AND TechPaid = '0' AND Invoiced = '0' AND ISNULL(MissingComments,'') = '' AND Deactivated = '0'
			// In Accounting - Invoiced = '1' AND TechPaid = '0' AND Deactivated = '0'
			// Completed - TechPaid = '1' AND Deactivated = '0'
			// All - Deactivated = '0'
			// Deactivated - Deactivated = '1'

	// cache status query
	private static $stateQuery = null;
	private static $stateQueryName = null;

	private static $lifecycleStates = null;
	private static $customStates = null;
	private static $allStates = null;

	const LIFECYCLE_STATE = 0;
	const CUSTOM_STATE = 1;

	private static $idToNameMap;
	private static $nameToIdMap;
	private static $currentId = 0;

	private static function init() {
			if (self::$lifecycleStates !== null) return;

			self::$lifecycleStates = array();
			self::$customStates = array();

			self::stateDefine(WO_STATE_CREATED, "ShowTechs = 0 AND ISNULL(Tech_ID, 0) = 0 AND Deactivated = 0 AND Approved = 0");
			self::stateDefine(WO_STATE_PUBLISHED, "ShowTechs = 1 AND ISNULL(Tech_ID, 0) = 0 AND Deactivated = 0 AND ISNULL(FLS_ID,'') = ''");
			self::stateDefine(WO_STATE_ASSIGNED, "ISNULL(Tech_ID, 0) != 0 AND Deactivated = 0 AND TechMarkedComplete = 0 AND ISNULL(CAST (MissingComments AS VARCHAR(MAX)),'') = ''");
			self::stateDefine(WO_STATE_WORK_DONE, "TechMarkedComplete = 1 AND Approved = 0 AND TechPaid = 0 AND Invoiced = 0 AND Deactivated = 0");
			self::stateDefine(WO_STATE_INCOMPLETE, "TechMarkedComplete = 0 AND Approved = 0 AND TechPaid = 0 AND Invoiced = 0 AND ISNULL(CAST (MissingComments AS VARCHAR(MAX)),'') != '' AND Deactivated = 0");
			self::stateDefine(WO_STATE_APPROVED, "Approved = 1 AND Invoiced = 0 AND Deactivated = 0");
			self::stateDefine(WO_STATE_IN_ACCOUNTING, "Invoiced = 1 AND TechPaid = 0 AND Deactivated = 0");
			self::stateDefine(WO_STATE_COMPLETED, "TechPaid = 1 AND Deactivated = 0");
			self::stateDefine(WO_STATE_DEACTIVATED, "Deactivated = 1");

			self::stateDefine(WO_STATE_ALL_ACTIVE, "Deactivated = 0", self::CUSTOM_STATE);
			self::stateDefine(WO_STATE_COMPLETED_TAB, "Approved = 1 AND Deactivated = 0", self::CUSTOM_STATE);
			self::stateDefine(WO_STATE_TECH_PAID_TAB, "Invoiced = 1 AND Deactivated = 0", self::CUSTOM_STATE);

			self::$allStates = array_merge(self::$lifecycleStates, self::$customStates);
	}

	private static function stateDefine($name, $filter, $type = self::LIFECYCLE_STATE) {
		$id = self::$currentId;
		++self::$currentId;
		self::$idToNameMap[$id] = $name;
		self::$nameToIdMap[$name] = $id;
		$query = WorkOrderStateQuery::define($id, $name, $filter);
		if ($type == self::LIFECYCLE_STATE)
			self::$lifecycleStates[$name] = $query;
		else
			self::$customStates[$name] = $query;
	}

	public static function getStatusQuery() {
		self::init();
		// return sql query for getting the life cycle state of a work order as id
		if (self::$stateQuery !== null) return self::$stateQuery;
		$query = "(CASE ";
		foreach (self::$lifecycleStates as $state) {
			$query .= "WHEN ({$state->getFilter()}) THEN {$state->getId()} ";
		}
		$query .= "ELSE -1 END)"; // else case should not happen
		self::$stateQuery = $query;
		return $query;
	}

	public static function getStatusQueryAsName() {
		self::init();
		// return sql query for getting the life cycle state of a work order as name
		if (self::$stateQueryName !== null) return self::$stateQueryName;
		$query = "(CASE ";
		foreach (self::$lifecycleStates as $state) {
			$query .= "WHEN ({$state->getFilter()}) THEN '{$state->getName()}' ";
		}
		$query .= "ELSE '' END)"; // else case should not happen
		self::$stateQueryName = $query;
		return $query;
	}

	public static function getStateConditionsQueryWithName($name) {
		self::init();
		// return where clause for a status / use to find WOs with a particular life cycle or custom status
		return array_key_exists($name, self::$allStates) ? self::$allStates[$name]->getFilter() : "";
	}

	public static function getNameWithId($id) {
		return array_key_exists($id, self::$idToNameMap) ? self::$idToNameMap[$id] : "";
	}

	public static function getIdWithName($name) {
		return array_key_exists($name, self::$nameToIdMap) ? self::$nameToIdMap[$name] : "";
	}
}

class WorkOrderStateQuery {
	protected $id;
	protected $name;
	protected $filter;

	private function __construct() {}

	public static function define($id, $name, $filter) {
		$state = new self();
		$state->id = $id;
		$state->name = $name;
		$state->filter = $filter;
		return $state;
	}

	public function getId() {
		return $this->id;
	}

	public function getName() {
		return $this->name;
	}

	public function getFilter() {
		return $this->filter;
	}
}
