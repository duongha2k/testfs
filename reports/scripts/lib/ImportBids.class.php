<?php
require_once 'ImportAbstract.class.php';

class ImportBids extends ImportAbstract
{
	public function __construct(Zend_Db_Adapter_Abstract $db) {
		parent::__construct($db, 'Work_Order_Applicants', Core_Database::TABLE_WORK_ORDER_BIDS, 'id');
	}

	public function run() {

        $lastCaspioID = $this->getLastInserterDestinationPK();
        $offset = 10000;
        $result = null;

        try { $result = $this->selectFromSource( $lastCaspioID + 1, $lastCaspioID + $offset ); }
        catch (Exception $ex ) { print $ex->getMessage(); exit(0); }

        if ( !$result ) { print "There are'n records to processing."; exit; }

		foreach ($result as &$row) {

            $row['caspio_id'] = $row['id']; $row['id'] = null;

            $row['DeactivatedTech'] = ( $row['DeactivatedTech'] == 'True' ) ? 1 : 0;
            $row['Hide'] = ( $row['Hide'] == 'True' ) ? 1 : 0;

            $row['Bid_Date'] = ( '' == trim($row['Bid_Date']) ) ? '0000-00-00 00:00:00' : $row['Bid_Date'];
            $row['BidModifiedDate'] = ( '' == trim($row['BidModifiedDate']) ) ? $row['Bid_Date'] : $row['BidModifiedDate'];
            
            /* convert dates format from 'M/d/yyyy h:mm:ss a' to 'yyyy-MM-dd hh:mm:ss' UTC */
            $tz = date_default_timezone_get();
            date_default_timezone_set('UTC');

            if ( '0000-00-00 00:00:00' != $row['Bid_Date'] ) {
                $row['Bid_Date'] = date("Y-m-d H:i:s",strtotime($row['Bid_Date']));
            }
            if ( '0000-00-00 00:00:00' != $row['Bid_Date'] ) {
                $row['BidModifiedDate'] = date("Y-m-d H:i:s",strtotime($row['BidModifiedDate']));
            }

            $row['WorkOrderID'] = empty($row['WorkOrderID']) ? 0 : $row['WorkOrderID'];
            $row['WorkOrderNum'] = empty($row['WorkOrderNum']) ? '' : $row['WorkOrderNum'];
            $row['TechID'] = empty($row['TechID']) ? 0 : $row['TechID'];
            $row['Tech_FName'] = empty($row['Tech_FName']) ? '' : $row['Tech_FName'];
            $row['Tech_LName'] = empty($row['Tech_LName']) ? '' : $row['Tech_LName'];
            $row['Company_ID'] = empty($row['Company_ID']) ? '' : $row['Company_ID'];
            $row['Comments'] = empty($row['Comments']) ? '' : $row['Comments'];
            $row['ClientEmail'] = empty($row['ClientEmail']) ? '' : $row['ClientEmail'];
            $row['CreatedByUser'] = empty($row['CreatedByUser']) ? '' : $row['CreatedByUser'];

            /* insert to DB */
            try { $this->insertToDest($row); }
            catch (Exception $ex) { print $ex->getMessage()."\n"; print_r($row); }
		}
	}

    protected function getLastInserterDestinationPK() {
        $query = $this->db->select();
        $query->from($this->destinationTable, array('MAX(caspio_id)'));
        $id = $this->db->fetchOne($query);
        return $id ? $id : 0;
    }

	protected function selectFromSource($min = null, $max = null) {
		$where = array();
		if ($min !== null) {
			$where[] = "$this->destinationPK >= $min";
		}
		if ($max !== null) {
			$where[] = "$this->destinationPK < $max";
		}

		$result = Core_Caspio::caspioSelectWithFieldListMap($this->sourceTable, $this->fieldListMap, implode(' AND ', $where), 'id');
		$errors = Core_Api_Error::getInstance();
		if ($errors->hasErrors()) {
			throw new Exception("Caspio errors:\n" . $errors->getErrors());
		}

		echo "Min: $min, Max: $max\n";
		if ($result === null) return array();
		foreach ($result as &$row) {
			$row = $this->sourceArray2Dest($row);
		}
		return $result;
	}

    protected function insertToDest($data) {
		if ($this->db->fetchOne("SELECT caspio_id FROM {$this->destinationTable} WHERE caspio_id = ?", array($data['caspio_id']))) {
			$this->db->update($this->destinationTable, $data, ' caspio_id = ' . $this->db->quote($data['caspio_id']));
		} else {
			$this->db->insert($this->destinationTable, $data);
		}
	}
}
