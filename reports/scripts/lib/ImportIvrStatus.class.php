<?php
require_once 'ImportLongList.class.php';

class ImportIvrStatus extends ImportLongList {
	public function __construct(Zend_Db_Adapter_Abstract $db) {
		$dateFields = array(
			'LastCalledTime'
		);

		$booleanFields = array(
			'Success'
		);

		parent::__construct($db,'IVR_Log', 'ivr_status', 'WOUNID', $dateFields, $booleanFields, 5000);

		$this->insertFields = $this->destinationFields;

		$this->db->query("TRUNCATE " . $this->destinationTable);
	}

	protected function selectFromSource($min = null, $max = null) {
		echo "Min: $min, max: $max\n";
		$result = Core_Caspio::caspioSelectAdv($this->sourceTable, "WOUNID,CallType,MAX(CalledTime),COUNT(id)", "Old=0 AND WOUNID >= $min AND WOUNID < $max GROUP BY WOUNID,CallType", '', false);
		$errors = Core_Api_Error::getInstance();
		if ($errors->hasErrors()) {
			throw new Exception("Caspio errors:\n" . $errors->getErrors());
		}

		$redialEntries = array();

		if (!$result) return array();
		foreach ($result as $row) {
			$WIN_NUM = $row[0];
			if (!$WIN_NUM) continue;

			$CallType = $row[1];
			$LastCalledTime = $row[2];
			$RedialCount = $row[3];

			$redialEntries[$WIN_NUM][$CallType] = array('WIN_NUM' => $WIN_NUM, 'CallType' => $CallType, 'LastCalledTime' => $LastCalledTime, 'RedialCount' => $RedialCount, 'Success' => false);
		}
		if (!count($redialEntries)) return array();

		$winsToSelect = array_keys($redialEntries);

		$result = Core_Caspio::caspioSelectAdv($this->sourceTable, "DISTINCT WOUNID, CallType", "Old=0 AND WOUNID IN (" . implode(',', $winsToSelect) . ") AND Result LIKE 'Success%' GROUP BY WOUNID,CallType", '', false);
		$errors = Core_Api_Error::getInstance();
		if ($errors->hasErrors()) {
			throw new Exception("Caspio errors:\n" . $errors->getErrors());
		}
		if ($result) {
			foreach ($result as $row) {
				$WIN_NUM = $row[0];
				if (!$WIN_NUM) continue;

				$CallType = $row[1];
				$redialEntries[$WIN_NUM][$CallType]['Success'] = true;
			}
		}
		$result = array();
		foreach ($redialEntries as $typeEntries) {
			foreach ($typeEntries as $entry) {
				$result[] = $this->sourceArray2Dest($entry);
			}
		}

		return $result;
	}

	protected function insertToDest($data) {
		$this->db->insert($this->destinationTable, $data);
	}
}
