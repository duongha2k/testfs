<?php
class ImportTechsInfo extends ImportLongList {
	public function __construct(Zend_Db_Adapter_Abstract $db) {
		parent::__construct($db, 'TR_Master_List', 'TechBankInfo', 'TechID', array(), array('Deactivated'), 5000);

		$this->fieldListMap = array(
			'TechID',
			'FirstName',
			'LastName',
			'PrimaryPhone',
			'SecondaryPhone',
			'PrimPhoneType',
			'SecondPhoneType',
			'PrimaryPhoneExt',
			'SecondaryPhoneExt',
			'PrimaryEmail',
			'No_Shows',
			'Back_Outs',
			'Address1',
			'Address2',
			'State',
			'City',
			'ZipCode',
			'UserName',
			'Password',
			'Latitude',
			'Longitude',
			'FLSID',
			'FLSTech',
			'SMS_Number',
			'Deactivated'
		);

		$this->fieldListMap = array_combine($this->fieldListMap, $this->fieldListMap);
		$this->insertFields = $this->fieldListMap;
	}

	protected function insertToDest($data) {
		foreach ($data as $key => &$value) {
			if ($value === null) $value = '';;
		}

		if ($this->db->fetchOne("SELECT TechID FROM {$this->destinationTable} WHERE TechID=?", array($data['TechID']))) {
			$this->db->update($this->destinationTable, $data, 'TechID=' . $this->db->quote($data['TechID']));
		} else {
			$emptyFields = array(
				'PaymentMethod' => 'Check',
				'BankName' => '',
				'BankAddress1' => '',
				'BankAddress2' => '',
				'BankCity' => '',
				'BankState' => '',
				'BankZip' => '',
				'BankCountry' => '',
				'AccountNum' => '',
				'RoutingNum' => '',
				'DateChange' => new Zend_Db_Expr('NOW()'),
				'AgreeTerms' => '',
				'AccountName' => '',
				'DepositType' => '',
				'MaskedAccountNum' => '',
				'MaskedRoutingNum' => '',
				'Birthdate' => '',
			);

			$data = array_merge($data, $emptyFields);

			$this->db->insert($this->destinationTable, $data);
		}
	}

}
