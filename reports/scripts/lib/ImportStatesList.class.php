<?php
require_once 'ImportAbstract.class.php';

class ImportStatesList extends ImportAbstract {
	public function __construct(Zend_Db_Adapter_Abstract $db) {
		parent::__construct($db, 'States_List', 'states_list', '');
	}

	public function run() {
		$this->db->query('TRUNCATE ' . $this->destinationTable);

		$result = $this->selectFromSource();
		foreach ($result as &$row) {
			$this->db->insert($this->destinationTable, $row);
		}
	}
}
