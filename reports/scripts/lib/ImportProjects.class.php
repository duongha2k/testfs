<?php
require_once 'ImportLongList.class.php';

class ImportProjects extends ImportLongList {
	public function __construct(Zend_Db_Adapter_Abstract $db) {
		$dateFields = array(
			'ACSAuthDate'
		);

		$booleanFields = array(
			'Active',
			'ShowOnReports',
			'UseMyRecruitEmailSettings',
			'AutoBlastOnPublish',
			'AutoSMSOnPublish',
			'Push2Tech',
			'Reminder24Hr',
			'Reminder1Hr',
			'ReminderAcceptance',
			'ReminderNotMarkComplete',
			'isProjectAutoAssign',
			'isWorkOrdersFirstBidder',
			'ReminderIncomplete',
			'ReminderAll',
			'CheckInCall',
			'CheckOutCall',
			'P2TPreferredOnly',
			'NotifyOnBid',
			'OOS_Trigger',
			'InstallDesk_Trigger',
			'SignOffSheet_Required',
			'NotifyOnAssign',
			'NotifyOnAutoAssign',
			'FixedBid',
			'PcntDeduct',
		);

		parent::__construct($db, 'TR_Client_Projects', 'projects', 'Project_ID', $dateFields, $booleanFields, 200);
	}


	public function run() {
		$result = Core_Caspio::caspioSelectAdv($this->sourceTable, "MIN({$this->destinationPK}),MAX({$this->destinationPK})", '', '', false);
		$errors = Core_Api_Error::getInstance();
		if ($errors->hasErrors()) {
			throw new Exception("Caspio errors:\n" . $errors->getErrors());
		}

		$min = $result[0][0];
		$max = $result[0][1];

		if ($min == 0 && $max == 0) {
			echo "No data";
			return;
		}

		echo "Max: $max\n";

		do {
			if ($min <= 3404 && $min + $this->step > 3404) { //Hack for project 3404
				$result = $this->selectFromSource($min, 3404);

				foreach ($result as &$row) {
					$this->insertToDest($row);
				}

				$this->import3404();

				$result = $this->selectFromSource(3405, $min + $this->step);

				foreach ($result as &$row) {
					$this->insertToDest($row);
				}
			} else {
				$result = $this->selectFromSource($min, $min + $this->step);

				foreach ($result as &$row) {
					$this->insertToDest($row);
				}

			}
			$min += $this->step;
		} while ($min <= $max);
	}

	private function import3404() {
		echo "Import 3404\n";

		$select = $this->fieldListMap;
		unset($select['Description']);
		$result = Core_Caspio::caspioSelectWithFieldListMap($this->sourceTable, $select, $this->destinationPK . '=3404', '');
		$errors = Core_Api_Error::getInstance();
		if ($errors->hasErrors()) {
			throw new Exception("Caspio errors:\n" . $errors->getErrors());
		}
		$row = $result[0];

$description = <<<EOF
Schedule service with site contact as listed on Work order document.  Advise Rhombus when service call will take place.

Tech must also take digital photos of proposed display unit.
• Mock Screen from Reception Area
• Behind monitor showing media player installed with the power button accessible (facing down)
• Seating areas in respect to the installed screen
• Close up of port on modem or router (with the Seatac TAGGED data cable plugged into
it)
• Panduit from monitor to outlet
• Any wiring in ceiling or I.T. Room installed by tech
• Wireless access point (behind monitor and/or plugged into modem/router)

After you arrive onsite -  you MUST call Enqii
1-866-387-7667 and provide the following information:
 Project: Network you are providing the service for: (WHCTV, KCTV  - contact Rhombus for Network.)
 Your name and contact details
 The site code, name, and address
 Type of Connectivity: DHCP or Static
 MAC address of the media player
 Issue you are there to resolve

Once complete - obtain checkout code from Enqii and put on the work order page that is attached.

MUST BE SUBMITTED WITHIN 24 HOURS OF COMPLETION:
Completed work order with Enqii checkout code
All required photos
CT Connectivity file download (if Enqii requests)
EOF;

		$row['Description'] = $description;

		$row = $this->sourceArray2Dest($row);
		$this->insertToDest($row);
	}
}
