<?php
require_once 'ImportAbstract.class.php';

class ImportLongList extends ImportAbstract {
	protected $step;

	public function __construct(Zend_Db_Adapter_Abstract $db, $sourceTable, $destinationTable, $destinationPK, $dateFields = array(), $booleanFields = array(), $step = 100) {
		parent::__construct($db, $sourceTable, $destinationTable, $destinationPK, $dateFields, $booleanFields);

		$this->step = $step;
	}

	public function run() {
		$result = Core_Caspio::caspioSelectAdv($this->sourceTable, "MIN({$this->destinationPK}),MAX({$this->destinationPK})", '', '', false);
		$errors = Core_Api_Error::getInstance();
		if ($errors->hasErrors()) {
			throw new Exception("Caspio errors:\n" . $errors->getErrors());
		}

		$min = $result[0][0];
		$max = $result[0][1];

		if ($min == 0 && $max == 0) {
			echo "No data";
			return;
		}

		echo "Max: $max\n";

		do {
			$result = $this->selectFromSource($min, $min + $this->step);

			foreach ($result as &$row) {
				$this->insertToDest($row);
			}

			$min += $this->step;
		} while ($min <= $max);
	}
}
