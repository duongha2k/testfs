<?php
require_once 'ImportAbstract.class.php';

class ImportWOCategories extends ImportAbstract {
	public function __construct(Zend_Db_Adapter_Abstract $db) {
		parent::__construct($db, 'WO_Categories', 'wo_categories', 'Category_ID');
	}
}
