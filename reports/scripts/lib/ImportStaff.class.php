<?php
require_once 'ImportAbstract.class.php';

class ImportStaff extends ImportAbstract {
	public function __construct(Zend_Db_Adapter_Abstract $db) {
		parent::__construct($db, 'TR_Staff', 'staff', 'StaffID', array(), array('Active'));
	}
}
