<?php
require_once 'ImportAbstract.class.php';

class ImportDeactivationCodes extends ImportAbstract {
	public function __construct(Zend_Db_Adapter_Abstract $db) {
		parent::__construct($db, 'Deactivation_Codes', 'deactivation_codes', 'ID');
	}
}
