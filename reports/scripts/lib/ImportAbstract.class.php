<?php
class ImportAbstract {
	protected $db;
	protected $sourceTable;
	protected $destinationTable;
	protected $destinationPK;

	protected $fieldListMap;
	protected $insertFields;

	protected $dateFields;
	protected $booleanFields;

	protected $sourceFields;
	protected $destinationFields;

	public function __construct(Zend_Db_Adapter_Abstract $db, $sourceTable, $destinationTable, $destinationPK, $dateFields = array(), $booleanFields = array()) {
		$this->db = $db;
		$this->sourceTable = $sourceTable;
		$this->destinationTable = $destinationTable;
		$this->destinationPK = $destinationPK;

		$this->dateFields = $dateFields;
		$this->booleanFields = $booleanFields;

		$sourceFields = $this->getSourceDBFields();
		$sourceFields = array_combine($sourceFields, $sourceFields);
		$destinationFields = $this->getDestinationDBFields();
		$destinationFields = array_combine($destinationFields, $destinationFields);
		$this->sourceFields = $sourceFields;
		$this->destinationFields = $destinationFields;

		$this->fieldListMap = array_combine($sourceFields, $sourceFields);

		$this->insertFields = array_intersect($destinationFields, $sourceFields);
	}

	public function run() {
		$result = $this->selectFromSource();
		foreach ($result as &$row) {
			$this->insertToDest($row);
		}
	}

	protected function selectFromSource($min = null, $max = null) {
		$where = array();
		if ($min !== null) {
			$where[] = "$this->destinationPK >= $min";
		}
		if ($max !== null) {
			$where[] = "$this->destinationPK < $max";
		}

		$result = Core_Caspio::caspioSelectWithFieldListMap($this->sourceTable, $this->fieldListMap, implode(' AND ', $where), '');
		$errors = Core_Api_Error::getInstance();
		if ($errors->hasErrors()) {
			throw new Exception("Caspio errors:\n" . $errors->getErrors());
		}

		echo "Min: $min, Max: $max\n";
		if ($result === null) return array();
		foreach ($result as &$row) {
			$row = $this->sourceArray2Dest($row);
		}
		return $result;
	}

	protected function insertToDest($data) {
		if ($this->db->fetchOne("SELECT {$this->destinationPK} FROM {$this->destinationTable} WHERE {$this->destinationPK}=?", array($data[$this->destinationPK]))) {
			$this->db->update($this->destinationTable, $data, $this->destinationPK . '=' . $this->db->quote($data[$this->destinationPK]));
		} else {
			$this->db->insert($this->destinationTable, $data);
		}
	}

	protected function getSourceDBFields() {
		$columns = Core_Caspio::caspioGetTableDesign($this->sourceTable);
		$fieldList = array();

		foreach ($columns as $info) {
			$fieldInfo = explode(",",$info);
			$fieldList[] = trim($fieldInfo[0]);
		}

		return $fieldList;
	}

	protected function getDestinationDBFields() {
		$result = $this->db->fetchAll('DESCRIBE ' . $this->destinationTable);
		$fields = array();
		foreach ($result as $row) {
			$fields[] = $row['Field'];
		}
		return $fields;
	}

	protected function sourceArray2Dest($source) {
		$result = array();

		foreach ($this->insertFields as $skey => $dkey) {
			$result[$dkey] = $source[$skey];
		}

		foreach ($this->dateFields as $field) {
			if ($result[$field]) {
				$date = new Zend_Date($result[$field], 'M/d/yyyy h:mm:ss a', 'en');
				$result[$field] = $date->toString('yyyy-MM-dd HH:mm:ss');
			}
		}

		foreach ($this->booleanFields as $field) {
			if ($result[$field] == 'True') {
				$result[$field] = true;
			} else {
				$result[$field] = false;
			}
		}

		return $result;
	}
}
