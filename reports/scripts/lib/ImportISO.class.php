<?php
require_once 'ImportAbstract.class.php';

class ImportISO extends ImportAbstract {
	public function __construct(Zend_Db_Adapter_Abstract $db) {
		parent::__construct($db, 'ISO', 'iso', 'UNID');
	}
}
