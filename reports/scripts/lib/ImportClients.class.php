<?php
require_once 'ImportLongList.class.php';

class ImportClients extends ImportLongList {
	public function __construct(Zend_Db_Adapter_Abstract $db) {
		$dateFields = array(
			'RegDate',
			'ClientExpiration',
		);

		$booleanFields = array(
			'AcceptTerms',
			'AccountEnabled',
			'Admin',
			'ShowOnReports',
			'PrimaryContact',
			'ProjectManager',
		);

		parent::__construct($db, 'TR_Client_List', 'clients', 'ClientID', $dateFields, $booleanFields, 250);
	}
}
