<?php
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../wdata/application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    realpath(APPLICATION_PATH . '/../../includes/classes'),
    get_include_path(),
)));


/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);


$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->registerNamespace('Core_');
$autoloader->registerNamespace('API_');
$application->bootstrap(array('Defines'));     

$cfgSite = new Zend_Config_Xml(APPLICATION_PATH.'/../../includes/configs/cfg.site.xml');
defined('WEBSITE_LIB_PATH') || define('WEBSITE_LIB_PATH', $cfgSite->get("site")->get("website_lib_path"));

$db = Zend_Db::factory('MYSQLI', array(
            'host'       => 'saturntestrestore.cd3kywrhh0ei.us-east-1.rds.amazonaws.com',
            'username'   => 'saturntest',
            'password'   => 'galaxian85probe',
            'dbname'     => 'saturntest'
        ));
Zend_Db_Table::setDefaultAdapter($db);

$yesterdayDate  = date("Y-m-d", time() - 86400);
//$yesterdayDate  = date("Y-m-d");//test
$beginyesterdayDate = "$yesterdayDate 00:00:00";
$endyesterdayDate = "$yesterdayDate 23:59:59";

$select = $db->select()
        ->from(array('pal'=>'phone_audit_log'),
                    array('techid','auditdate','techphonevalue','phonetype'))
        ->join(array('tbi'=>Core_Database::TABLE_TECH_BANK_INFO)
                        ,'tbi.TechID=pal.techid'
                        ,array('FirstName','LastName')
        )
        ->where("pal.actionid=1 and pal.auditdate >= '$beginyesterdayDate'")
        ->order("pal.techid");                    
                
$records = $db->fetchAll($select);

$toEmail = "support@fieldsolutions.com";
$fromName = "no-replies@fieldsolutions.com";
$fromEmail = "no-replies@fieldsolutions.com";
$subject = "Reported Invalid Phone Numbers.";

$body="Content:\n";
if(!empty($records))
{    
    $body.="The following Techs have had their phone number reported as invalid from beginning of yesterday:
Tech ID# Last Name First Name Day/Evening Phone Number\n";
    for($i=0;$i<count($records);$i++)
    {
        $record = $records[$i];
        $techID = $record['techid'];
        $lastName = $record['LastName'];
        $firstName = $record['FirstName'];
        $ptypeId = $record['phonetype']; 
        $ptype="";
        if($ptypeId==1) $ptype="Day";
        if($ptypeId==2) $ptype="Evening";
        $telephone = $record['techphonevalue'];                 
        $body.="$techID $lastName $firstName $ptype $telephone\n";        
    }
    //print_r($records);
}
else
{
    $body.="There was no reported invalid phone numbers from beginning of yesterday.\n";
}
$body.="\n
Thank you.
FieldSolutions Support Team
support@fieldsolutions.com 
\n";

//echo($body); //test
//--- email
$mail = new Zend_Mail();
$mail->setBodyText($body);
$mail->setFrom($fromEmail, $fromName);
$mail->addTo($toEmail, $toEmail);
$mail->setSubject($subject);
$mail->send();

            
?>
