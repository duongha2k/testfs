<?php
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../wdata/application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    realpath(APPLICATION_PATH . '/../../includes/classes'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->registerNamespace('Core_');
$autoloader->registerNamespace('API_');
$application->bootstrap();

require_once 'Core/Api/Class.php';

require_once 'config.php';
require_once 'lib/ImportClients.class.php';
require_once 'lib/ImportProjects.class.php';
require_once 'lib/ImportShippingCarriers.class.php';
require_once 'lib/ImportStatesList.class.php';
require_once 'lib/ImportWOCategories.class.php';
require_once 'lib/ImportUSAZipCodes.class.php';
require_once 'lib/ImportTimezonePostalCode.class.php';
require_once 'lib/ImportTimestamps.class.php';
require_once 'lib/ImportAdmin.class.php';
require_once 'lib/ImportTechsInfo.class.php';
require_once 'lib/ImportIvrStatus.class.php';
require_once 'lib/ImportDeactivationCodes.class.php';
require_once 'lib/ImportCustomers.class.php';
require_once 'lib/ImportStaff.class.php';
require_once 'lib/ImportClientDeniedTechs.class.php';
require_once 'lib/ImportISO.class.php';
require_once 'lib/ImportWO.class.php';

/*
echo "Import work orders\n";
$importWO = new ImportWO($db, $statusFile);
$importWO->run();

echo "Import clients\n";
$import = new ImportClients($db);
$import->run();

echo "Import projects\n";
$import = new ImportProjects($db);
$import->run();

echo "Import shipping carriers\n";
$import = new ImportShippingCarriers($db);
$import->run();

echo "Import states list\n";
$import = new ImportStatesList($db);
$import->run();

echo "Import WO categories\n";
$import = new ImportWOCategories($db);
$import->run();

echo "Import USA Zip codes\n";
$import = new ImportUSAZipCodes($db);
$import->run();
echo "Import Timezone_PostalCode\n";
$import = new ImportTimezinePostalCode($db);
$import->run();

echo "Import timestamps\n";
$import = new ImportTimestamps($db);
$import->run();

echo "Import admins\n";
$import = new ImportAdmin($db);
$import->run();

echo "Import techs\n";
$import = new ImportTechsInfo($db);
$import->run();

echo "Import IVR status\n";
$import = new ImportIvrStatus($db);
$import->run();

echo "Import deactivation codes\n";
$import = new ImportDeactivationCodes($db);
$import->run();

echo "Import customers\n";
$import = new ImportCustomers($db);
$import->run();

echo "Import staff\n";
$import = new ImportStaff($db);
$import->run();

echo "Import client denied techs\n";
$import = new ImportClientDeniedTechs($db);
$import->run();

echo "Import ISO\n";
$import = new ImportISO($db);
$import->run();
*/

require_once 'lib/ImportWOFix2.class.php';

echo "Fix work orders\n";
$importWO = new ImportWOFix2($db, $statusFile);
$importWO->run();
