<?php
// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../wdata/application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    realpath(APPLICATION_PATH . '/../../includes/classes'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);


$autoloader = Zend_Loader_Autoloader::getInstance();
$autoloader->registerNamespace('Core_');
$autoloader->registerNamespace('API_');
$application->bootstrap();

require_once 'Core/Api/Class.php';
require_once 'config.php';

require_once 'lib/ImportBids.class.php';

set_time_limit(0);
ini_set('memory_limit','512M');


$cacheDir = realpath(APPLICATION_PATH.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'var'.DIRECTORY_SEPARATOR.'cache');

//$fp = fopen($cacheDir.'/importBid.lock', 'w');
//if (flock($fp, LOCK_EX)) {
//    print "Locked\n";
//} else {
//    print "Cannot lock\n";
//}
//sleep(20);
//flock($fp, LOCK_UN);

$fp = fopen($cacheDir.'/importBid.lock', 'w');
if (flock($fp, LOCK_EX|LOCK_NB)) {
    echo "Import bids\n";
    $importBids = new ImportBids($db);
    try { $importBids->run(); }
    catch (Exception $ex) {
        print $ex->getMessage()."\n";
    }
} else {
    print "Didn't quite get the lock. Script is running.\n";
}
fclose($fp);


