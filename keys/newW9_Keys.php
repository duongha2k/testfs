<?php
ini_set("display_errors", 1);

if (!array_key_exists(1, $argv)) {
	echo "Enter a password to encode the private key. \n";
	die();
}

$password = $argv[1];

$dn = array("countryName" => 'US', "stateOrProvinceName" => 'MN', "localityName" => 'Saint Cloud', "organizationName" => 'FieldSolutions', "organizationalUnitName" => 'FieldSolutions', "commonName" => 'FieldSolutions', "emailAddress" => 'user@domain.com');
$numberofdays = 0;

$privkey = openssl_pkey_new();
$csr = openssl_csr_new($dn, $privkey);
$sscert = openssl_csr_sign($csr, null, $privkey, $numberofdays);
openssl_x509_export($sscert, $publickey);
openssl_pkey_export($privkey, $privatekey, $password);

//$privatekey2 = openssl_get_privatekey($privatekey, "MyPassword");

file_put_contents("w9_key.pri", $privatekey);
file_put_contents("w9_key.pub", $publickey);

?>
