<?php
ini_set("display_errors", 1);

if (!array_key_exists(1, $argv) || !array_key_exists(2, $argv)) {
	echo "Usage: setPKeyPW.php CurrentPassword NewPassword \n";
	die();
}

$passwordOld = $argv[1];
$password = $argv[2];

//$dn = array("countryName" => 'US', "stateOrProvinceName" => 'MN', "localityName" => 'Saint Cloud', "organizationName" => 'FieldSolutions', "organizationalUnitName" => 'FieldSolutions', "commonName" => 'FieldSolutions', "emailAddress" => 'user@domain.com');
$numberofdays = 0;

$privkey = openssl_get_privatekey(file_get_contents("key.pri"), $passwordOld);

if (!$privkey) {
	echo "Invalid current password.\n";
	die();
}

if (!openssl_pkey_export($privkey, $privatekey, $password)) {
	echo "Unable to create key.\n";
	die();
}

file_put_contents("key.pri", $privatekey);
echo "Password Set.\n";

?>