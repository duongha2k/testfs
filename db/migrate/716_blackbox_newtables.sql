
-- BlackBoxCabling_Test
DROP TABLE IF EXISTS `BlackBoxCabling_Test`;

CREATE TABLE `BlackBoxCabling_Test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tech_id` int(11) NOT NULL,
  `date_taken` datetime NOT NULL,
  `Level` int(11) NOT NULL DEFAULT '0',
  `C1RT_1` tinyint(4) NOT NULL DEFAULT '0',
  `C1RT_2` tinyint(4) NOT NULL DEFAULT '0',
  `C1RT_3` tinyint(4) NOT NULL DEFAULT '0',
  `C1RT_4` tinyint(4) NOT NULL DEFAULT '0',
  `C1RT_5` tinyint(4) NOT NULL DEFAULT '0',
  `C1RE_1` tinyint(4) NOT NULL DEFAULT '0',
  `C1RE_2` tinyint(4) NOT NULL DEFAULT '0',
  `C2RT_1` tinyint(4) NOT NULL DEFAULT '0',
  `C2RT_2` tinyint(4) NOT NULL DEFAULT '0',
  `C2RT_3` tinyint(4) NOT NULL DEFAULT '0',
  `C2RT_4` tinyint(4) NOT NULL DEFAULT '0',
  `C2RT_5` tinyint(4) NOT NULL DEFAULT '0',
  `C2RT_6` tinyint(4) NOT NULL DEFAULT '0',
  `C2RT_7` tinyint(4) NOT NULL DEFAULT '0',
  `C2RT_8` tinyint(4) NOT NULL DEFAULT '0',
  `C2RT_9` tinyint(4) NOT NULL DEFAULT '0',
  `C2RT_10` tinyint(4) NOT NULL DEFAULT '0',
  `C2RT_11` tinyint(4) NOT NULL DEFAULT '0',
  `C2RE_1` tinyint(4) NOT NULL DEFAULT '0',
  `C2RE_2` tinyint(4) NOT NULL DEFAULT '0',
  `C2RE_3` tinyint(4) NOT NULL DEFAULT '0',
  `C2RE_4` tinyint(4) NOT NULL DEFAULT '0',
  `C3RT_1` tinyint(4) NOT NULL DEFAULT '0',
  `C3RT_2` tinyint(4) NOT NULL DEFAULT '0',
  `C3RT_3` tinyint(4) NOT NULL DEFAULT '0',
  `C3RT_4` tinyint(4) NOT NULL DEFAULT '0',
  `C3RE_1` tinyint(4) NOT NULL DEFAULT '0',
  `C3RE_2` tinyint(4) NOT NULL DEFAULT '0',
  `C3RE_3` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- BlackBoxCabling_Test
DROP TABLE IF EXISTS `BlackBoxTelephony_Test`;

CREATE TABLE `BlackBoxTelephony_Test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tech_id` int(11) NOT NULL,
  `date_taken` datetime NOT NULL,
  `Level` int(11) NOT NULL DEFAULT '0',
  `T1RT_1` tinyint(4) NOT NULL DEFAULT '0',
  `T1RT_2` tinyint(4) NOT NULL DEFAULT '0',
  `T1RT_3` tinyint(4) NOT NULL DEFAULT '0',
  `T1RE_1` tinyint(4) NOT NULL DEFAULT '0',
  `T1RE_2` tinyint(4) NOT NULL DEFAULT '0',
  `T1RE_3` tinyint(4) NOT NULL DEFAULT '0',
  `T1RE_4` tinyint(4) NOT NULL DEFAULT '0',
  `T1RE_5` tinyint(4) NOT NULL DEFAULT '0',
  `T1RE_6` tinyint(4) NOT NULL DEFAULT '0',
  `T2RT_1` tinyint(4) NOT NULL DEFAULT '0',
  `T2RT_2` tinyint(4) NOT NULL DEFAULT '0',
  `T2RT_3` tinyint(4) NOT NULL DEFAULT '0',
  `T2RT_4` tinyint(4) NOT NULL DEFAULT '0',
  `T2RT_5` tinyint(4) NOT NULL DEFAULT '0',
  `T2RT_6` tinyint(4) NOT NULL DEFAULT '0',
  `T2RE_1` tinyint(4) NOT NULL DEFAULT '0',
  `T2RE_2` tinyint(4) NOT NULL DEFAULT '0',  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- certifications
INSERT INTO certifications (id,`name`,label,description,hide,groupid)
VALUES (46,'BlackBoxCablingHelper','Helper (C1)','Black Box Cabling – Helper (C1)',0,'BlackBoxCabling');
INSERT INTO certifications (id,`name`,label,description,hide,groupid)
VALUES (47,'BlackBoxCablingAdvanced','Advanced (C2)','Black Box Cabling – Advanced (C2)',0,'BlackBoxCabling');
INSERT INTO certifications (id,`name`,label,description,hide,groupid)
VALUES (48,'BlackBoxCablingLead','Lead (C3)','Black Box Cabling – Lead (C3)',0,'BlackBoxCabling');

INSERT INTO certifications (id,`name`,label,description,hide,groupid)
VALUES (49,'BlackBoxTelecomHelper','Helper (T1)','Black Box Telecom – Helper (T1)',0,'BlackBoxTelecom');
INSERT INTO certifications (id,`name`,label,description,hide,groupid)
VALUES (50,'BlackBoxTelecomAdvanced','Advanced (T2)','Black Box Telecom – Advanced (T2)',0,'BlackBoxTelecom');




