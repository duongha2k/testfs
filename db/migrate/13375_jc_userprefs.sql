CREATE TABLE IF NOT EXISTS `future_wo_info` (
  `ClientUserName` varchar(255) NOT NULL,
  `ProjectManagerName` varchar(255) DEFAULT NULL,
  `ProjectManagerPhone` varchar(255) DEFAULT NULL,
  `ProjectManagerEmail` varchar(255) DEFAULT NULL,
  `ResourceCoordinatorName` varchar(255) DEFAULT NULL,
  `ResourceCoordinatorPhone` varchar(255) DEFAULT NULL,
  `ResourceCoordinatorEmail` varchar(255) DEFAULT NULL,
  `EmergencyName` varchar(255) NOT NULL,
  `EmergencyPhone` varchar(255) NOT NULL,
  `EmergencyEmail` varchar(255) NOT NULL,
  `CheckInOutName` varchar(255) NOT NULL,
  `CheckInOutNumber` varchar(255) NOT NULL,
  `CheckInOutEmail` varchar(255) NOT NULL,
  `TechnicalSupportName` varchar(255) NOT NULL,
  `TechnicalSupportPhone` varchar(255) NOT NULL,
  `TechnicalSupportEmail` varchar(255) NOT NULL,
  `SystemGeneratedEmailTo` varchar(255) NOT NULL,
  `SystemGeneratedEmailFrom` varchar(255) NOT NULL,
  `WorkAvailableEmailFrom` varchar(255) NOT NULL,
  `BidNotificationEmailTo` varchar(255) NOT NULL,
  `P2TNotificationEmailTo` varchar(255) NOT NULL,
  `WorkAssignedEmailTo` varchar(255) NOT NULL,
  `WorkDoneEmailTo` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  PRIMARY KEY (`ClientUserName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE work_orders CHANGE Pic1_FromTech SystemGeneratedEmailTo VARCHAR(255) NULL;
ALTER TABLE work_orders CHANGE Pic2_FromTech SystemGeneratedEmailFrom VARCHAR(255) NULL;
ALTER TABLE work_orders CHANGE Pic3_FromTech BidNotificationEmailTo VARCHAR(255) NULL;
ALTER TABLE work_orders CHANGE Pic1 P2TNotificationEmailTo TEXT NULL;
ALTER TABLE work_orders CHANGE Pic2 WorkAssignedEmailTo TEXT NULL;
ALTER TABLE work_orders CHANGE Pic3 WorkDoneEmailTo TEXT NULL;
ALTER TABLE work_orders CHANGE SiteName1 WorkAvailableEmailFrom TEXT NULL;
ALTER TABLE work_orders CHANGE RMA_No WorkOrderOwner VARCHAR(255) NULL;

ALTER TABLE projects ADD AllowCustomCommSettings TINYINT (1) DEFAULT 0 NOT NULL;

ALTER TABLE work_order_import_kettle ADD work_order_owner varchar(100);

UPDATE clients SET UserType = 'Admin' WHERE Admin = '1';