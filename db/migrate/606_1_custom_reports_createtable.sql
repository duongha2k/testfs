-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Host: saturntestrestore2.cd3kywrhh0ei.us-east-1.rds.amazonaws.com
-- Generation Time: Aug 15, 2013 at 05:23 AM
-- Server version: 5.1.63
-- PHP Version: 5.3.2-1ubuntu4.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `saturntest`
--

-- --------------------------------------------------------

--
-- Table structure for table `custom_reports`
--

CREATE TABLE IF NOT EXISTS `custom_reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rpt_name` varchar(255) NOT NULL,
  `table` varchar(255) NOT NULL,
  `companyid` varchar(255) NOT NULL,
  `creation_date` datetime DEFAULT NULL,
  `lastupdated_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `last_run_date` datetime DEFAULT NULL,
  `sort_1` varchar(255) DEFAULT NULL,
  `sort_dir_1` varchar(10) DEFAULT NULL,
  `sort_2` varchar(255) DEFAULT NULL,
  `sort_dir_2` varchar(10) DEFAULT NULL,
  `sort_3` varchar(255) DEFAULT NULL,
  `sort_dir_3` varchar(10) DEFAULT NULL,
  `CreateBidReport` tinyint(4) NOT NULL DEFAULT '0',
  `RRDEnable` tinyint(4) NOT NULL DEFAULT '0',
  `RRDEmailTo` varchar(255) DEFAULT NULL,
  `RRDEmailFromName` varchar(255) DEFAULT 'FieldSolutions',
  `RRDEmailFrom` varchar(255) DEFAULT 'no-replies@fieldsolutions.com',
  `RRDEmailSubject` varchar(255) DEFAULT 'FieldSolutions Report: ',
  `RRDEmailMessage` varchar(1000) NOT NULL,
  `RRDEmailSchedule` varchar(20) NOT NULL DEFAULT 'Daily' COMMENT 'Daily,Weekdays,Weekly,Monthly',
  `RRDTimeOfDay` varchar(20) NOT NULL DEFAULT '12:00 AM',
  `RRDTimeZone` varchar(20) DEFAULT 'EST',
  `RRDWeeklyDays` varchar(255) DEFAULT NULL,
  `RRDMonthlyScheduleOptions` int(11) NOT NULL DEFAULT '1' COMMENT '1: Day Num, 2:Day Order',
  `RRDMonthlyScheduleDayNum` varchar(20) DEFAULT NULL,
  `RRDMonthlyScheduleDayOrder` int(11) DEFAULT NULL COMMENT '1,2,3,4',
  `RRDMonthlyScheduleDayOrderDay` int(11) DEFAULT NULL COMMENT '1,2,3,4,5,6,7',
  `last_modified_by_name` varchar(255) DEFAULT NULL,
  `created_by_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
