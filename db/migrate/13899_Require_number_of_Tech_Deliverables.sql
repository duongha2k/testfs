ALTER TABLE `work_orders` MODIFY COLUMN `SignOffSheet_Required` INT(3);
ALTER TABLE `projects` MODIFY COLUMN `SignOffSheet_Required` INT(3);
ALTER TABLE `future_wo_info` ADD SignOffSheet_Required INT(3);