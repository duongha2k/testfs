CREATE TABLE IF NOT EXISTS `social_networks` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

INSERT INTO `social_networks` (`id`, `name`, `label`) VALUES
(1, 'Twitter', 'Follow Me On Twitter!'),
(2, 'Facebook', 'Visit Me On Facebook!'),
(3, 'LinkedIn', 'LinkedIn');
Create Table `tech_social_networks`(
id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
TechID INT(11) NOT NULL,
SocialNetworkID INT(5) NOT NULL,
URL VARCHAR(255) NOT NULL
dateCreated DATETIME,
dateUpdated DATETIME
)


CREATE TABLE IF NOT EXISTS `techFiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `techID` int(10) NOT NULL,
  `fileType` varchar(255) NOT NULL,
  `filePath` varchar(255) NOT NULL,
  `displayName` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `approved` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15020 ;



ALTER TABLE `TechBankInfo` ADD `InterimSecClearDate` DATETIME NULL AFTER `InterimSecClear` 

ALTER TABLE `TechBankInfo` ADD `TopSecClearDate` DATETIME NULL AFTER `TopSecClear` 

ALTER TABLE `TechBankInfo` ADD `FullSecClearDate` DATETIME NULL AFTER `FullSecClear` 

ALTER TABLE `TechBankInfo` ADD `FLSBadgeDate` DATETIME NULL AFTER `FLSBadge` 

ALTER TABLE `TechBankInfo` ADD `NCR_Basic_Cert_Date` DATETIME NULL AFTER `NCR_Basic_Cert` 

ALTER TABLE `TechBankInfo` ADD `FLSWhseDate` DATETIME NULL AFTER `FLSWhse` 

ALTER TABLE `TechBankInfo` ADD `MCSE_Cert_Number` VARCHAR( 255 ) NULL AFTER `MCSE` 

ALTER TABLE `TechBankInfo` ADD `MCSE_Date` DATETIME NULL AFTER `MCSE_Cert_Number` 

ALTER TABLE `TechBankInfo` ADD `CCNA_Cert_Number` VARCHAR( 255 ) NULL AFTER `CCNA` , 
ADD `CCNA_Date` DATETIME NULL AFTER `CCNA_Cert_Number`		

ALTER TABLE `TechBankInfo` ADD `DELL_DCSE_Cert_Number` VARCHAR( 255 ) NULL AFTER `DELL_DCSE_Reg` , 
ADD `DELL_DCSE_Date` DATETIME NULL AFTER `DELL_DCSE_Cert_Number` 	+

ALTER TABLE `TechBankInfo` ADD `BICSI_Cert_Number` VARCHAR( 255 ) NULL AFTER `BICSI` , 
ADD `BICSI_Date` DATETIME NULL AFTER `BICSI_Cert_Number` 	

ALTER TABLE `TechBankInfo` ADD `CompTIA_Date` DATETIME NULL AFTER `CompTIA` 

ALTER TABLE `TechBankInfo` ADD `Starbucks_Cert_Num` VARCHAR( 255 ) NULL AFTER `Starbucks_Cert` 

ALTER TABLE `TechBankInfo` ADD `Hallmark_Cert_Num` VARCHAR( 255 ) NULL AFTER `Cert_Hallmark_POS` 

ALTER TABLE `TechBankInfo` ADD `Maurices_Cert_Num` VARCHAR( 255 ) NULL AFTER `Cert_Maurices_POS` 

ALTER TABLE `TechBankInfo` ADD `Essintial_Cert_Num` VARCHAR( 255 ) NULL AFTER `EssintialCert` 

ALTER TABLE `TechBankInfo` ADD `Samsung_Cert_Num` VARCHAR( 255 ) NULL AFTER `SamsungCert` 

ALTER TABLE `TechBankInfo` ADD `Electro_Mechanical_Cert_Num` VARCHAR( 255 ) NULL AFTER `ServRight_ElectroMech_Cert` 

ALTER TABLE `TechBankInfo` ADD `CopiersSelfRating` INT( 10 ) NULL AFTER `PrintersSelfRating` 

ALTER TABLE `TechBankInfo` ADD `electroMechSelfRating` INT( 10 ) NULL AFTER `WirelessSelfRating` 

ALTER TABLE `TechBankInfo` ADD `appleSelfRating` INT( 10 ) NULL AFTER `electroMechSelfRating` 

ALTER TABLE `TechBankInfo` ADD `savesSecCabinetsSelfRating` INT( 10 ) NULL AFTER `appleSelfRating` 

ALTER TABLE `TechBankInfo` ADD `BusinessStatement` TEXT(800) NULL; 

ALTER TABLE `TechBankInfo` ADD `Activation_Code` VARCHAR(255) NOT NULL 

ALTER TABLE `TechBankInfo` ADD `Activation_Confirmed` tinyint(1) NOT NULL 

INSERT INTO `wo_categories` (`Category_ID`, `Category`, `Abbr`, `TechSelfRatingColumn`) VALUES
(27, 'Printers', 'Printers', 'PrintersSelfRating'),
(28, 'Copiers', 'Copiers', 'CopiersSelfRating'),
(29, 'Electro-Mechanical Binding Eq.', 'ElecMechEq', 'electroMechSelfRating'),
(30, 'Safes/Security Cabinets', 'SafesSecurityCabinets', 'savesSecCabinetsSelfRating'),
(31, 'Apple brand PCs/Laptops ', 'Apple', 'appleSelfRating');


CREATE TABLE IF NOT EXISTS `equipment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `label` varchar(70) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

INSERT INTO `equipment` (`id`, `name`, `label`, `description`) VALUES
(1, 'Laptop', 'Laptop (Microsoft XP or newer)', NULL),
(2, 'CellPhone', 'Cell Phone', NULL),
(3, 'Truck', 'Truck', NULL),
(4, 'Cabling Equipment', 'Cabling Equipment', NULL),
(5, 'Ladder_6', 'Ladder 6''', NULL),
(6, 'Ladder_12', 'Ladder 12''', NULL),
(7, 'Ladder_20Plus', 'Ladder 20''+', NULL),
(8, 'ToneGeneratorAndWand', 'Tone Generator and Wand', NULL),
(9, 'ButtSet', 'Butt Set', NULL),
(10, 'DigitalVOMMeter', 'Digital VOM Meter', NULL),
(11, 'ContinuityTester', 'Continuity Tester', NULL),
(12, 'PunchTool66', 'Punch Tool with 66 blade', NULL),
(13, 'PunchTool110', 'Punch Tool with 110 blade', NULL),
(14, 'PunchToolBix', 'Punch Tool with Bix blade', NULL),
(15, 'PunchToolKrone', 'Punch Tool with Krone blade', NULL),
(16, 'CrimpToolRJ11', 'Crimp Tool with RJ11', NULL),
(17, 'CrimpToolRJ45', 'Crimp Tool with RJ45', NULL),
(18, 'FishTape50', '50‚Äô Fish Tape', NULL),
(19, 'RotaryCoaxCable', 'Rotary coax cable stripper for RG-59 and RG-6 cable', NULL),
(20, 'CordMasonryDrillHammer', 'Corded masonry drill hammer and bits up to 1" diameter', NULL),
(21, 'RechargeCCTV', 'Rechargeable CCTV installer''s LCD 4', NULL),
(22, 'VoltOhmMeter', 'Volt/Ohm meter', NULL),
(23, 'Screwdrivers', 'Set of screw drivers', NULL),
(24, 'PunchTool', 'Punch down Tool', NULL),
(25, 'DigitalCam', 'Digital Camera', NULL),
(26, 'OwnOrSourceCablingEquipment', 'Do you own, or can you source, ladders, cable, and cable testers?', NULL),
(27, 'Vehicle', 'Vehicle', NULL),
(28, 'TruckCarryLaddersCable', 'Vehicle can transport ladders & supplies', NULL),
(29, 'PossessAllLicenses', 'Do you possess all the licenses required in your city/state to perform', NULL),
(30, 'PanelTruck', 'Panel Truck', NULL),
(31, 'Cable', 'Cable', NULL),
(32, 'CableTesters', 'Cable Testers', NULL);


CREATE TABLE IF NOT EXISTS `skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `label` varchar(180) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

INSERT INTO `skills` (`id`, `name`, `label`, `description`) VALUES
(2, 'ReadWiringDiagrams', 'Can you read wiring diagrams?', NULL),
(3, 'ExpInstallSurv', 'Do you have experience installing surveillance DVR/Cam systems?', NULL),
(4, 'InstallingTelephony', 'I have experience in installing OR maintaining (break-fix) telephony systems.', NULL),
(5, 'MaintainingTelephony', '', NULL),
(6, 'ServerTelephony', 'I have installed or performed "break-fix" activity on a Server OR VOIP based telephony system.', NULL),
(7, 'VOIPTelephony', '', NULL),
(8, 'AvayaPartnerMerlin', 'Avaya Small Systems Partner or Merlin', NULL),
(9, 'AvayaPartnerMerlin6mo', '', NULL),
(10, 'AvayaIPOffice', 'Avaya Small Systems IP Office', NULL),
(11, 'AvayaIPOffice6mo', '', NULL),
(12, 'AvayaDefinity', 'PBX Definity', NULL),
(13, 'AvayaDefinity6mo', '', NULL),
(14, 'AvayaPBX8300etc', 'PBX 8300/8500/8700', NULL),
(15, 'AvayaPBX8300etc6mo', '', NULL),
(16, 'AyayaGateway', 'PBX Gateway 250/350', NULL),
(17, 'AyayaGateway6mo', '', NULL),
(18, 'NortelNorstar', 'Nortel Small Systems Norstar', NULL),
(19, 'NortelNorstar6mo', '', NULL),
(20, 'NortelBMS', 'Nortel Small Systems BMS', NULL),
(21, 'NortelBMS6mo', '', NULL),
(22, 'NortelMeridian', 'PBX Meridian', NULL),
(23, 'NortelMeridian6mo', '', NULL),
(24, 'NortelCS', 'PBX CS 1000/1500', NULL),
(25, 'NortelCS6mo', '', NULL),
(26, 'CiscoCM', 'Cisco Call Manager', NULL),
(27, 'CiscoCM6mo', '', NULL),
(28, 'Siemens9751', 'Siemens (ROLM) 9751', NULL),
(29, 'Siemens97516mo', '', NULL),
(30, 'SiemensHicom', 'Siemens (ROLM) Hicom', NULL),
(31, 'SiemensHicom6mo', '', NULL),
(32, 'SiemensHipath', 'Siemens (ROLM) Hipath', NULL),
(33, 'NEC', 'NEC', NULL),
(34, 'NEC6mo', '', NULL),
(35, 'InterTel', 'InterTel', NULL),
(36, 'InterTel6mo', '', NULL),
(37, 'Mltel', 'Mltel', NULL),
(38, 'Mltel6mo', '', NULL),
(39, 'Toshiba', 'Toshiba', NULL),
(40, 'Toshiba6mo', '', NULL),
(41, 'SkilledCat5Fiber', 'Do you do consider yourself skilled with Cat 5 and or fiber cabling?', NULL),
(42, '40HrsLast6Months', 'Have you performed over 40 hours of Cat 5 cabling contracts in the past 6 months?', NULL),
(44, 'SiemensHipath6mo', 'Siemens (ROLM) Hipath 6 Months', NULL);


ALTER TABLE cell_carriers DROP PRIMARY KEY;
ALTER TABLE cell_carriers ADD COLUMN id INT(20) AUTO_INCREMENT NOT NULL FIRST, ADD PRIMARY KEY(id);

CREATE TABLE IF NOT EXISTS `W9Info` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `TechID` int(10) DEFAULT NULL,
  `FirstName` varchar(255) DEFAULT NULL,
  `MI` varchar(10) DEFAULT NULL,
  `LastName` varchar(255) NOT NULL,
  `BusinessName` varchar(255) DEFAULT NULL,
  `FederalTaxClassification` varchar(100) DEFAULT NULL,
  `LLCTaxClass` varchar(2) DEFAULT NULL,
  `FTCOther` varchar(250) DEFAULT NULL,
  `SSN` blob,
  `MaskedSSN` varchar(255) DEFAULT NULL,
  `EIN` blob,
  `MaskedEIN` varchar(255) DEFAULT NULL,
  `DigitalSignature` varchar(20) DEFAULT NULL,
  `ExemptPayee` tinyint(1) NOT NULL,
  `Address1` varchar(255) NOT NULL,
  `Address2` varchar(255) NOT NULL,
  `City` varchar(40) CHARACTER SET utf8 NOT NULL,
  `State` varchar(10) CHARACTER SET utf8 NOT NULL,
  `ZipCode` varchar(15) CHARACTER SET utf8 NOT NULL,
  `AccountNumbers` varchar(255) DEFAULT NULL,
  `DateChanged` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=98 ;

CREATE TABLE IF NOT EXISTS `tech_skill` (
  `TechID` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  PRIMARY KEY (`TechID`,`skill_id`),
  KEY `skill_id` (`skill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `tech_skill`
  ADD CONSTRAINT `tech_skill_ibfk_1` FOREIGN KEY (`skill_id`) REFERENCES `skills` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE TABLE IF NOT EXISTS `tech_equipment` (
  `TechID` int(11) NOT NULL,
  `equipment_id` int(11) NOT NULL,
  PRIMARY KEY (`TechID`,`equipment_id`),
  KEY `equipment_id` (`equipment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `tech_equipment`
  ADD CONSTRAINT `tech_equipment_ibfk_1` FOREIGN KEY (`equipment_id`) REFERENCES `equipment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
