

/*--- fsexperts --- */
DROP TABLE IF EXISTS `fsexperts`;

CREATE TABLE `fsexperts` (
  `Id` int(11) NOT NULL,
  `Code` varchar(50) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Content` varchar(2000) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `FSExpertEnable` tinyint(1) DEFAULT NULL,
  `Abbreviation` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*Data for the table `fsexperts` */

insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (1,'ATM','ATM','Repair and maintenance of ATM machines and pin pads, including software upgrades and regular maintenance service calls.',1,'ATM');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (2,'CAT5','CAT5 Cabling','Installation, repair, and troubleshooting of Cat5 (or similar) cabling including in wall, open and dropped ceilings, conduit/raceway, and server rooms/closets.',1,'CAB');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (3,'CCTV','CCTV / Surveillance','Installation and repair of CCTV and surveillance equipment including: video cameras (wall and ceiling mounted), door triggers, POS integration, video monitors, and DVRs.',1,'CCTV');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (4,'Copiers','Copiers','Installation, repair, maintenance, and troubleshooting of multi-function copiers, including large floor and desktop models such as Xerox, Konica, HP, Toshiba, Oki, Ricoh, and more.',1,'COP');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (5,'Desktop','Desktop / Laptop','Installation and repair of desktop and laptop computers, including: virus removal, software installation/upgrades, break/fix services, memory and hard drive replacements and more.',1,'PC');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (7,'Signage','Digital Signage','Installation and repair of partial and full digital signage units, including: part swap-outs and new system installs (monitors, video players, PC’s, audio speakers and associated cabling).',1,'SIGN');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (8,'DSL','DSL / D-Mark / T1','Installation, repair, and troubleshooting of T1 & DSL lines, as well as troubleshooting and repair of DMARC/PunchDown Block/Patch Panels.',1,'DSL');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (9,'EnterpriseStorage','Enterprise Storage','Repair, maintenance and upgrades to online (disk array) and backup (tape library) storage solutions on brands such as: Sun Microsystems, Dell, Microsoft, EMC, HP, Hitachi, IBM, NetApp and Novell.',1,'STOR');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (10,'Kiosk','Kiosk','Installation, troubleshooting, and repair of self-standing/box kiosks, including: PC, keyboard, printer, display screen, touch screen and audio/speaker.',1,'KIO');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (11,'LowVolt','Low Voltage','Installation of low voltage wiring that does not require licensed electricians including: lighting, speaker wires, thermostats, wall mounted time clocks, and other small hard wired devices.',1,'LVOL');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (13,'POS','Point of Sale','Installation, repair, and troubleshooting of Point of Sale equipment including: scanners, printers, hand-held devices, display arms, and cash drawers, including software refresh, imaging, and memory upgrades.',1,'POS');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (14,'Printers','Printers','Installation, maintenance, troubleshooting, and repair of single and multi-function printers, including:laser, color, inkjet, dot matrix, and bar code. Brands include Xerox, Konica, HP, Brother, Epson, Lexmark, Dell, IBM, Tektronix and NEC.',1,'PRT');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (15,'Routers','Routers / Switches','Installation, troubleshooting, and repair of routers and switches and associated cabling.',1,'ROUT');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (16,'ServrHW','Server Hardware','Installation, maintenance, upgrades, and repair of server hardware including: Cisco, HP, IBM, Dell, Sun, Apple and Legacy.',1,'SERV');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (17,'Tele','Telephony - Non-VoIP','Installation, maintenance, troubleshooting, and repair of phone systems and cabling in Telco rooms. Brands include: Avaya, PBX, Nortel, Cisco, Siemens, NEC, InterTel, Mitel and Toshiba.',1,'TEL');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (19,'VoIP','Telephony - VoIP','Installation, troubleshooting, and repair of wiring/jacks/VoIP routers such as Vonage. May be residential.',1,'TELV');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (20,'Wireless','Wireless Networking','Installation, troubleshooting, and repair of wireless devices and networking such as: routers and modems.',1,'WNET');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (29,'Elec','Elec','',0,'Elec');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (30,'Const','Const','',0,'Const');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (31,'A/V','A/V','',0,'A/V');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (32,'Printer','Printer','',0,'Printer');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (33,'Fiber','Fiber','',0,'Fiber');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (34,'Wiring','Wiring','',0,'Wiring');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (35,'RFID','RFID','',0,'RFID');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (36,'Satlite','Satlite','',0,'Satlite');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (37,'Survey','Survey','',0,'Survey');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (38,'Gen','Gen','',0,'Gen');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (39,'ElecMechEq','ElecMechEq','',0,'ElecMechEq');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (40,'SafesSecurityCabinets','SafesSecurityCabinets','',0,'SafesSecurityCabinets');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (41,'Apple','Apple','',0,'Apple');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (42,'MobileDevices','MobileDevices','',0,'MobileDevices');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (43,'FlatPanelTV','FlatPanelTV','',0,'FlatPanelTV');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (44,'CentralOfficeCabling','CentralOfficeCabling','',0,'CentralOfficeCabling');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (45,'AdjustableBedRepair','AdjustableBedRepair','',0,'AdjustableBedRepair');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (46,'Network','Home Networking',NULL,0,'Network');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (47,'Scanners','Scanners',NULL,0,'Scanners');
insert  into `fsexperts`(`Id`,`Code`,`Title`,`Content`,`FSExpertEnable`,`Abbreviation`) values (48,'ServrSW','Server Software',NULL,0,'ServrSW');



/*--- fsexpert_techs ---*/
DROP TABLE IF EXISTS `fsexpert_techs`;

CREATE TABLE `fsexpert_techs` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `FSExpertId` int(11) NOT NULL,
  `TechId` int(11) NOT NULL,
  `Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/*--- wo_categories ---*/
INSERT INTO wo_categories (Category_ID,Category,Abbr,TechSelfRatingColumn) VALUES (37,'Scanners','Scanners','ScannersSelfRating');

/*--- TechBankInfo ---*/
ALTER TABLE TechBankInfo ADD COLUMN ScannersRepairYN tinyint(1) DEFAULT 0;
ALTER TABLE TechBankInfo ADD COLUMN ScannersSelfRating int DEFAULT 0;

/*--- fstags ---*/
UPDATE fstags SET ExistingImagePath ='/widgets/images/Bro-2.png' WHERE Name = 'ServRightBrother';
UPDATE fstags SET ExistingImagePath ='/widgets/images/Adjustable-Bed-Certified-2.png' WHERE Name = 'ErgoMotionKeyContractor';
UPDATE fstags SET ExistingImagePath ='/widgets/images/Adjustable-Bed-Expert-2.png' WHERE Name = 'ErgoMotionCertifiedTechnician';



