-- phpMyAdmin SQL Dump
-- version 2.11.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 13, 2010 at 09:27 AM
-- Server version: 5.0.91
-- PHP Version: 5.2.14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `fs_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `UNID` int(10) unsigned NOT NULL auto_increment,
  `Company_ID` varchar(40) default NULL,
  `Customer_Name` varchar(255) default NULL,
  `Contact_Name` varchar(255) default NULL,
  `Contact_Email` varchar(255) default NULL,
  `Contact_Phone` varchar(255) default NULL,
  `Username` varchar(255) default NULL,
  `Password` varchar(255) default NULL,
  `FS_Client_Name` varchar(255) default NULL,
  PRIMARY KEY  (`UNID`),
  KEY `Username` (`Username`),
  KEY `Password` (`Password`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

