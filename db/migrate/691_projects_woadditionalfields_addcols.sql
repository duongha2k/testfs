
ALTER TABLE projects ADD COLUMN ACSNotifiPOC varchar(255);
ALTER TABLE projects ADD COLUMN ACSNotifiPOC_Phone varchar(255);
ALTER TABLE projects ADD COLUMN ACSNotifiPOC_Email varchar(255);

ALTER TABLE work_orders_additional_fields ADD COLUMN ACSNotifiPOC varchar(255);
ALTER TABLE work_orders_additional_fields ADD COLUMN ACSNotifiPOC_Phone varchar(255);
ALTER TABLE work_orders_additional_fields ADD COLUMN ACSNotifiPOC_Email varchar(255);

ALTER TABLE future_wo_info ADD COLUMN ACSNotifiPOC varchar(255);
ALTER TABLE future_wo_info ADD COLUMN ACSNotifiPOC_Phone varchar(255);
ALTER TABLE future_wo_info ADD COLUMN ACSNotifiPOC_Email varchar(255);


