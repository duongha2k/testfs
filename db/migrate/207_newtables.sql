/*
SQLyog Enterprise - MySQL GUI v7.14 
MySQL - 5.1.33-community : Database - saturn
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


/*Table structure for table `qtview_project` */

DROP TABLE IF EXISTS `qtview_project`;

CREATE TABLE `qtview_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qtview_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `qtview_project` */

/*Table structure for table `qtviews` */

DROP TABLE IF EXISTS `qtviews`;

CREATE TABLE `qtviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Company_ID` varchar(40) DEFAULT NULL,
  `CustomerName` varchar(255) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `qtviews` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
