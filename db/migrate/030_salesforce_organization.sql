CREATE TABLE IF NOT EXISTS `salesforce_organization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organization_id` varchar(20) CHARACTER SET utf8 NOT NULL,
  `company_id` varchar(8) CHARACTER SET utf8 NOT NULL,
  `api_username` varchar(40) CHARACTER SET utf8 NOT NULL,
  `token_key` varchar(20) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `organization_id` (`organization_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;
