﻿insert into wo_categories(Category_ID, Category, Abbr, TechSelfRatingColumn)
values(32, 'Enterprise Storage', 'EnterpriseStorage', 'EnterpriseStorageSelfRating');

ALTER TABLE TechBankInfo ADD EnterpriseStorage tinyint(1)  NOT NULL DEFAULT '0';
ALTER TABLE TechBankInfo ADD EnterpriseStorageSelfRating int(10);
