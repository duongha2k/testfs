-- phpMyAdmin SQL Dump
-- version 2.11.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 05, 2010 at 11:37 AM
-- Server version: 5.0.91
-- PHP Version: 5.2.14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `fs_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `AdminID` int(10) unsigned NOT NULL auto_increment,
  `Fname` varchar(255) default NULL,
  `Lname` varchar(255) default NULL,
  `UserName` varchar(255) default NULL,
  `Password` varchar(255) default NULL,
  `Email` varchar(255) default NULL,
  PRIMARY KEY  (`AdminID`),
  UNIQUE KEY `UserName` (`UserName`),
  KEY `Password` (`Password`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=48 ;

