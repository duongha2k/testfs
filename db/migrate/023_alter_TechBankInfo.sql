ALTER TABLE `TechBankInfo` CHANGE `PrimaryPhone` `PrimaryPhone` varchar(40) NULL;
ALTER TABLE `TechBankInfo` CHANGE `SecondaryPhone` `SecondaryPhone` varchar(40) NULL;
ALTER TABLE `TechBankInfo` CHANGE `PrimPhoneType` `PrimPhoneType` varchar(40) NULL; 
ALTER TABLE `TechBankInfo` CHANGE `SecondPhoneType` `SecondPhoneType` varchar(40) NULL;
ALTER TABLE `TechBankInfo` CHANGE `PrimaryPhoneExt` `PrimaryPhoneExt` varchar(40) NULL;
ALTER TABLE `TechBankInfo` CHANGE `SecondaryPhoneExt` `SecondaryPhoneExt` varchar(40) NULL;
ALTER TABLE `TechBankInfo` CHANGE `No_Shows` `No_Shows` INT DEFAULT '0'; 
ALTER TABLE `TechBankInfo` CHANGE `Back_Outs` `Back_Outs` INT DEFAULT '0';
