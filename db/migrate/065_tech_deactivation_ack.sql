CREATE TABLE IF NOT EXISTS `tech_deactivation_ack` (
  `TechID` int(11) NOT NULL,
  `date` datetime NOT NULL,
  KEY `TechID` (`TechID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;