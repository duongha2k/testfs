<?php
require_once dirname(__FILE__).'/../../includes/modules/common.init.php';

$fieldList = array("id", "Created_Date", "Created_By", "Type", "Contact", "Subject", "Message", "Company_ID", "WorkOrder_ID");
$fieldListMap = array_combine($fieldList, $fieldList);

$maxId = 340000;
$step = 1000;

$startIndex = 0;
$db = Core_Database::getInstance();

while($startIndex < $maxId) {
    
    $stopIndex =  $startIndex + $step;

    $ascLogs = Core_Caspio::caspioSelectWithFieldListMap(Core_Database::TABLE_WO_CONTACT_LOG, $fieldListMap, 'id >= ' .(int)$startIndex . ' AND id < ' . (int)$stopIndex, "");
    
    if (is_array($ascLogs) ) {
        foreach ($ascLogs as $reminder) {
            
            $select = $db->select();
            $select->from(Core_Database::TABLE_WO_CONTACT_LOG, $fieldList)
                   ->where('id = ?', (int)$reminder['id']);
        
            $result = $db->fetchRow($select);     
            
            if (empty($result)) {
                $toInsert = $reminder;
                if(empty($toInsert['WorkOrder_ID']) || $toInsert['WorkOrder_ID']==0) continue;
                foreach ($toInsert as &$value) {
                    if ($value === 'True') {
                        $value = 1;
                    } elseif ($value === 'False') {
                        $value = 0;
                    }
                }
                
                $date = new Zend_Date($toInsert['Created_Date'], 'MM/dd/YYYY hh:mm:ss a');
                $toInsert['Created_Date'] = $date->toString('YYYY-MM-dd HH:mm:ss');
                try {
                    $db->insert(Core_Database::TABLE_WO_CONTACT_LOG, $toInsert);
                } catch (Zend_Exception $e) {
                    print "Error";
                    print_r($toInsert);
                    print_r($e->getMessage());
                    if(strpos($e->getMessage(), "1452")){
                    	continue;
                    }else{
                    	exit;
                    }
                }
                print 'Inserting: ' . $toInsert['id'] . "\n";
            } else {
                print 'Skip: ' . $reminder['id'] . "\n";
            }
        }
    }
    
    $startIndex = $stopIndex;
}
