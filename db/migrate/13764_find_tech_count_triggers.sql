CREATE TABLE IF NOT EXISTS `tech_wo_category_count` (
  `Tech_ID` int(11) NOT NULL,
  `WO_Category_ID` int(11) NOT NULL,
  `completed` int(11) NOT NULL,
  PRIMARY KEY (`Tech_ID`,`WO_Category_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP
	TRIGGER `work_orders_after_update`;
DROP
	TRIGGER `work_orders_after_insert`;

DELIMITER $$
CREATE
	TRIGGER `work_orders_after_insert` AFTER INSERT
	ON `work_orders`
	FOR EACH ROW BEGIN
		IF NEW.Tech_ID > 0 AND NEW.Approved = 1 THEN
			INSERT INTO tech_wo_category_count (Tech_ID, WO_Category_ID, completed) VALUES (NEW.Tech_ID, NEW.WO_Category_ID, 1) ON DUPLICATE KEY UPDATE completed = completed + 1;
		END IF;
    END$$
CREATE
	TRIGGER `work_orders_after_update` AFTER UPDATE
	ON `work_orders`
	FOR EACH ROW BEGIN
		IF (NEW.Tech_ID <=> OLD.Tech_ID AND NEW.WO_Category_ID <=> OLD.WO_Category_ID AND NEW.Approved <=> OLD.Approved) THEN
			SET @DONOTHING = 888;
		ELSE
			# decrease counts for old tech or wo category
			IF OLD.Tech_ID > 0 AND OLD.Approved = 1 THEN
				UPDATE tech_wo_category_count SET completed = completed - 1 WHERE Tech_ID = OLD.Tech_ID AND WO_Category_ID = OLD.WO_Category_ID;
			END IF;
			IF NEW.Tech_ID > 0 AND NEW.Approved = 1 THEN
				INSERT INTO tech_wo_category_count (Tech_ID, WO_Category_ID, completed) VALUES (NEW.Tech_ID, NEW.WO_Category_ID, 1) ON DUPLICATE KEY UPDATE completed = completed + 1;
			END IF;
		END IF;
    END$$
DELIMITER ;

INSERT INTO tech_wo_category_count (Tech_ID, WO_Category_ID, completed) (SELECT Tech_ID, WO_Category_ID, COUNT(WIN_NUM) FROM work_orders AS w WHERE Approved = 1 AND Tech_ID IS NOT NULL GROUP BY Tech_ID, WO_Category_ID ) ON DUPLICATE KEY UPDATE completed = VALUES(completed);