
ALTER TABLE projects ADD COLUMN RequiredStartDateTime tinyint DEFAULT 0;
ALTER TABLE projects ADD COLUMN RequiredEndDateTime tinyint DEFAULT 0;
