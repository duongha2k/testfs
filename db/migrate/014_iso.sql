-- phpMyAdmin SQL Dump
-- version 2.11.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 13, 2010 at 10:11 AM
-- Server version: 5.0.91
-- PHP Version: 5.2.14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `fs_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `iso`
--

CREATE TABLE IF NOT EXISTS `iso` (
  `UNID` int(10) unsigned NOT NULL auto_increment,
  `Company_Name` varchar(255) default NULL,
  `Address_1` varchar(255) default NULL,
  `Address_2` varchar(255) default NULL,
  `City` varchar(255) default NULL,
  `State` varchar(255) default NULL,
  `Zip` varchar(255) default NULL,
  `Contact_Name` varchar(255) default NULL,
  `Contact_Phone` varchar(255) default NULL,
  `Contact_Email` varchar(255) default NULL,
  `Country` varchar(255) default NULL,
  `Company_Desc` longtext,
  `Website` varchar(255) default NULL,
  `Client_ID` varchar(255) default NULL,
  PRIMARY KEY  (`UNID`),
  KEY `Client_ID` (`Client_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=116 ;
