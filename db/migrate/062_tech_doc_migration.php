<?php
ini_set('error_reporting', E_ALL);
error_reporting(E_ALL);
ini_set('log_errors',TRUE);
ini_set('html_errors',FALSE);
ini_set('error_log','/home/p6356/phpNewErrors.log');
ini_set('display_errors',FALSE);
require_once dirname(__FILE__).'/../modules/common.init.php';

$cfgAmazon = new Zend_Config_Xml(APPLICATION_PATH.'/../../includes/configs/cfg.amazon.xml');

$db = Zend_Db::factory('Pdo_Mysql', array(
            'host'       => 'fieldsolutionsdb.cd3kywrhh0ei.us-east-1.rds.amazonaws.com',
            'username'   => 'fsolutions',
            'password'   => 'volume55nourish',
            'dbname'     => 'gbailey_technicianbureau'
		));

Zend_Db_Table::setDefaultAdapter($db);
        
defined('S3_TECHS_DOCS_DIR')  || define('S3_TECHS_DOCS_DIR', $cfgAmazon->get('s3')->get('bukets')->get('techs'));
$errors = Core_Api_Error::getInstance();

function uploadTechFileAWS($data, $caspioFileName, $techID){
		
		if(empty($data) || empty($techID)) return NULL;
		$fileName = pathinfo($caspioFileName, PATHINFO_BASENAME);
		$fileType = "";
		$fileS3Dir = "";
		$file = array();
		$returnArray = array();
		$hpCert = false;
		
		if($data['resumeUpload']){
			$fileType = "Resume";
			$fileS3Dir = "/resumes";
			$file["fileUpload"] = $data['resumeUpload'];
		}elseif($data['badgePhotoUpload']){
			$fileType = "Profile Pic";
			$fileS3Dir = "/profile-pics";
			//$fileS3Dir = "/testing";
			$file["fileUpload"] = $data['badgePhotoUpload'];
		}elseif($data['HP_CertProof']){
			$fileType = "HP Cert Proof";
			$fileS3Dir = "/hp-cert-proof";
			$file['fileUpload'] = $data['HP_CertProof'];
			@unlink($data['HP_CertProof']);
		}else{
			return false;
		}
		
		$error = Core_Api_Error::getInstance();
		$files = new Core_File();
			
		$displayName = $fileName;
        $fName = uniqid($fileName.'_');
        $extension = pathinfo($caspioFileName, PATHINFO_EXTENSION);
        $fName = $fName . '.' . $extension;
            
       	$uploadRes = $files->uploadFile($fName, $file["fileUpload"], S3_TECHS_DOCS_DIR.$fileS3Dir);

	     if(!$uploadRes){
	         $error->addError(6, 'Upload file '.$fName.' failed');
	         $errorsDetails = $error->getErrors();
	         error_log('FileUploading: (tech:Resume:uploadFileAWS) ' . implode('|', $errorsDetails));
	         return false;
	     }else{
	     	//add file data to techField table
	     	error_log("SUCCESS");
	     	
	     	$dbTest = Core_Database::getInstance();
	     		
	     	$insertArray = array(
					'techID' => $techID,
					'fileType' => $fileType,
					'filePath' => $fName,
					'displayName' => $displayName,
					'dateCreated' => date("Y-m-d H:i:s"),
					'dateUpdated' => date("Y-m-d H:i:s")
				);
					
			if($fileType == "Profile Pic"){
				$insertArray["approved"] = "1";
			}

	     	$result = true;
			try {
				$dbTest->insert("techFiles", $insertArray);
				$returnArray['fileName'] = $fName;
				$returnArray['encodedFilename'] = base64_encode(urlencode($fName));
				$returnArray['displayName'] = $displayName;
			} catch (Exception $e) { error_log($e->getMessage()); $result = false; }
			
	     		
	     }
	    if ( $error->getErrors() ) {
	        return false;
	     }else{
	     	return true;
	     }
		//@unlink($file['fileUpload']);	
		//return $returnArray;	
}

//For Profile Badges

$select = $db->select();
$select->from(Core_Database::TABLE_TECH_BANK_INFO, array("TechID", "Picture"))
       ->where('Picture <> ""' )
       ->order(array("TechID ASC"));

$result = Core_Database::fetchAll($select);
if (!empty($result)){
 
    foreach ($result as $f) {
        $file_name = trim($f['Picture']);
        if (empty ($file_name) || $file_name=='NULL') continue;
        $sendData = array();
        
  		error_log("TECH ID: ".$f['TechID']);
  		
        $caspioFileData = Core_Caspio::caspioDownloadFile( $file_name ); // get file from Caspio
		
        
		$sendData['badgePhotoUpload'] = $caspioFileData;
        
       // if(Core_File::fileExists(trim($file_name,'/'), S3_CLIENTS_DOCS_DIR) == true){
        //    fwrite($logger, "\r\n [".date('r')."] -- File $file_name has already uploaded on AWS");
       //     continue;
       // }
        
        $caspioErrors = $errors->getErrors();
        if (!empty($caspioErrors)){
            foreach ($caspioErrors as $ce) error_log("CASPIO ERROR: ".$ce);
        }
        
        // put file on S3
        if(!uploadTechFileAWS($sendData, $file_name, $f['TechID'])){
            $flag = false;
           error_log("File Upload Error- TechID: ".$f['TechID']." File name: ".$file_name);
        } else {
        	error_log("UPLOADED");
        }
    }
    
} else {
   // fwrite($logger, "\r\n [".date('r')."] -- Finished!!!");
    exit;
}



//For Tech Resumes

$select = $db->select();
$select->from(Core_Database::TABLE_TECH_BANK_INFO, array("TechID", "Resume"))
       ->where('Resume <> ""' )
       ->order(array("TechID ASC"));

$result = Core_Database::fetchAll($select);

if (!empty($result)){
 
    foreach ($result as $f) {
        $file_name = trim($f['Resume']);
        if (empty ($file_name) || $file_name=='NULL') continue;
        $sendData = array();
        
  		error_log("TECH ID: ".$f['TechID']);
  		
        $caspioFileData = Core_Caspio::caspioDownloadFile( $file_name ); // get file from Caspio
		
        
		$sendData['resumeUpload'] = $caspioFileData;
        /*
        if(Core_File::fileExists(trim($file_name,'/'), S3_CLIENTS_DOCS_DIR) == true){
            fwrite($logger, "\r\n [".date('r')."] -- File $file_name has already uploaded on AWS");
            continue;
        }
        */
        $caspioErrors = $errors->getErrors();
        if (!empty($caspioErrors)){
            foreach ($caspioErrors as $ce) error_log("CASPIO ERROR: ".$ce);
        }
        
        // put file on S3
        if(!uploadTechFileAWS($sendData, $file_name, $f['TechID'])){
            $flag = false;
           error_log("File Upload Error- TechID: ".$f['TechID']." File name: ".$file_name);
        } else {
        	error_log("UPLOADED");
        }
    }
    
} else {
   // fwrite($logger, "\r\n [".date('r')."] -- Finished!!!");
    exit;
}

//For HP Certs

$select = $db->select();
$select->from(Core_Database::TABLE_TECH_BANK_INFO, array("TechID", "HP_CertProof"))
       ->where('HP_CertProof <> ""' )
       ->order(array("TechID ASC"));

$result = Core_Database::fetchAll($select);

if (!empty($result)){
 
    foreach ($result as $f) {
        $file_name = trim($f['HP_CertProof']);
        if (empty ($file_name) || $file_name=='NULL') continue;
        $sendData = array();
        
  		error_log("TECH ID: ".$f['TechID']);
  		
        $caspioFileData = Core_Caspio::caspioDownloadFile( $file_name ); // get file from Caspio
		
        
		$sendData['HP_CertProof'] = $caspioFileData;
        /*
        if(Core_File::fileExists(trim($file_name,'/'), S3_CLIENTS_DOCS_DIR) == true){
            fwrite($logger, "\r\n [".date('r')."] -- File $file_name has already uploaded on AWS");
            continue;
        }
        */
        $caspioErrors = $errors->getErrors();
        if (!empty($caspioErrors)){
            foreach ($caspioErrors as $ce) error_log("CASPIO ERROR: ".$ce);
        }
        
        // put file on S3
        if(!uploadTechFileAWS($sendData, $file_name, $f['TechID'])){
            $flag = false;
           error_log("File Upload Error- TechID: ".$f['TechID']." File name: ".$file_name);
        } else {
        	error_log("UPLOADED");
        }
    }
    
} else {
   // fwrite($logger, "\r\n [".date('r')."] -- Finished!!!");
    exit;
}
exit;

?>