
-- wo_tags
insert into wo_tags(id,wo_tag_code,wo_tag_name) values(1,'Segment','Segment');
insert into wo_tags(id,wo_tag_code,wo_tag_name) values(2,'ClientStatus','Client Status');
insert into wo_tags(id,wo_tag_code,wo_tag_name) values(3,'ClientRelationship','Client Relationship');
insert into wo_tags(id,wo_tag_code,wo_tag_name) values(4,'ServiceType','Service Type');
insert into wo_tags(id,wo_tag_code,wo_tag_name) values(5,'OriginalSeller','Original Seller (OS)');
insert into wo_tags(id,wo_tag_code,wo_tag_name) values(6,'Executive','Executive');
insert into wo_tags(id,wo_tag_code,wo_tag_name) values(7,'SystemStatus','System Status');
insert into wo_tags(id,wo_tag_code,wo_tag_name) values(8,'EntityType','Entity Type');

-- wo_tag_items
insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('Segment','OEM','OEM',1,1);
insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('Segment','Tier1','Tier1',2,1);
insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('Segment','Large','Large',3,1);
insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('Segment','Major','Major',4,1);
insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('Segment','SMB','SMB',5,1);

insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('ClientStatus','Prospect','Prospect',1,1);
insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('ClientStatus','Active','Active',2,1);
insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('ClientStatus','Inactive','Inactive',3,1);
insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('ClientStatus','Lost','Lost',4,1);
insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('ClientStatus','Dead','Dead',5,1);

insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('ClientRelationship','Platform','Platform',1,1);
insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('ClientRelationship','Core','Core',2,1);
insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('ClientRelationship','Strategic','Strategic',3,1);

insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('ServiceType','FullService','Full Service',1,1);
insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('ServiceType','SelfService','Self Service',2,1);
insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('ServiceType','StrategicSolutions','Strategic Solutions',3,1);
insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('ServiceType','Warranty','Warranty',4,1);
insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('ServiceType','FieldStaff','Field Staff',5,1);

insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('OriginalSeller','Ross Nehrt','Ross Nehrt',1,1);

insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('Executive','Marty Reader','Marty Reader (MR)',1,1);

insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('SystemStatus','Active','Active',1,1);
insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('SystemStatus','Restricted','Restricted',2,1);

insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('EntityType','FieldSolutions','FieldSolutions',1,1);
insert into wo_tag_items(wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values('EntityType','FieldStaff','FieldStaff',2,1);

-- projects
ALTER TABLE projects ADD COLUMN ServiceTypeId int DEFAULT 14 COMMENT '14: Full Service';
ALTER TABLE projects ADD COLUMN EntityTypeId int DEFAULT 23 COMMENT '23: FieldSolutions';
ALTER TABLE projects ADD COLUMN FSClientServiceDirectorId int ;
ALTER TABLE projects ADD COLUMN FSAccountManagerId int ;

-- work_orders_additional_fields
ALTER TABLE work_orders_additional_fields ADD COLUMN ServiceTypeId int ;
ALTER TABLE work_orders_additional_fields ADD COLUMN EntityTypeId int ;
ALTER TABLE work_orders_additional_fields ADD COLUMN FSClientServiceDirectorId int ;
ALTER TABLE work_orders_additional_fields ADD COLUMN FSAccountManagerId int ;

-- cliprofile_action_log
ALTER TABLE cliprofile_action_log ADD COLUMN company_id varchar(50);
ALTER TABLE cliprofile_action_log ADD COLUMN from_site varchar(50);

-- wo_tag_items
insert into wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
values(41,'SystemStatus','PartialRestricted','Partial Restriction',3,1);

-- wo_tag_items 02/15/2013
UPDATE wo_tag_items SET active=0 WHERE wo_tag_code='OriginalSeller' AND wo_tag_item_code='Ross Nehrt';

INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
VALUES(45,'OriginalSeller','Mike Kraemer','Mike Kraemer',1,1);
INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
VALUES(46,'OriginalSeller','Shad Follmer','Shad Follmer',2,1);
INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
VALUES(47,'OriginalSeller','Marty Reader','Marty Reader',3,1);
INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
VALUES(48,'OriginalSeller','Noel Schewe','Noel Schewe',4,1);
INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
VALUES(49,'OriginalSeller','Dave Garner','Dave Garner',5,1);

INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
VALUES(50,'OriginalSeller','Stephanie Bledsoe','Stephanie Bledsoe',6,1);
INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
VALUES(51,'OriginalSeller','Kevin Joyce','Kevin Joyce',7,1);
INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
VALUES(52,'OriginalSeller','David Baggett','David Baggett',8,1);
INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
VALUES(53,'OriginalSeller','Cary Goldman','Cary Goldman',9,1);
INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
VALUES(54,'OriginalSeller','Robin Haggerty','Robin Haggerty',10,1);

INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
VALUES(55,'OriginalSeller','Aaron Acker','Aaron Acker',11,1);
INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
VALUES(56,'OriginalSeller','Incoming Sales','Incoming Sales',12,1);

INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active) 
VALUES(57,'ServiceType','Internal','Internal',6,1);




