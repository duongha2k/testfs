CREATE TABLE IF NOT EXISTS `pricing_fields` (
  `PricingFieldID` int(11) NOT NULL AUTO_INCREMENT,
  `DisplayName` varchar(255) NOT NULL,
  `ActualName` varchar(255) NOT NULL,
  PRIMARY KEY (`PricingFieldID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;


CREATE TABLE IF NOT EXISTS `pricing_rules` (
  `PricingRuleID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(255) NOT NULL,
  `ParentID` int(11) DEFAULT NULL,
  `CloneDate` datetime DEFAULT NULL,
  PRIMARY KEY (`PricingRuleID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `charge_types` (
  `ChargeTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(255) NOT NULL,
  `Percentage` decimal(10,2) DEFAULT NULL,
  `FlatFee` decimal(10,2) DEFAULT NULL,
  `PerX` decimal(10,2) DEFAULT NULL,
  `InputField` int(11) NOT NULL,
  `OutputField` int(11) NOT NULL,
  `CalculationOrder` int(11) NOT NULL DEFAULT '0',
  `Min` decimal(10,2) DEFAULT NULL,
  `Max` decimal(10,2) DEFAULT NULL,
  `PricingRuleID` int(11) NOT NULL,
  `ApplyWhen` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ChargeTypeID`),
  KEY `PricingRuleID` (`PricingRuleID`),
  KEY `InputField` (`InputField`),
  KEY `OutputField` (`OutputField`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;