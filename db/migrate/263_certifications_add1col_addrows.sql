
ALTER TABLE `certifications`  ADD `groupid` VARCHAR(50) NULL DEFAULT NULL;

insert into certifications(`name`,label,description,hide,groupid)
values ('EMCApD','EMCApD - Application Developer','EMCApD - Application Developer',0,'EMC');
insert into certifications(`name`,label,description,hide,groupid)
values ('EMCBA','EMCBA - Backup Recovery','EMCBA - Backup Recovery',0,'EMC');
insert into certifications(`name`,label,description,hide,groupid)
values ('EMCCA','EMCCA - Cloud Architect','EMCCA - Cloud Architect',0,'EMC');
insert into certifications(`name`,label,description,hide,groupid)
values ('EMCCIS','EMCCIS - Cloud Infrastructure and Services','EMCCIS - Cloud Infrastructure and Services',0,'EMC');
insert into certifications(`name`,label,description,hide,groupid)
values ('EMCDCA','EMCDCA - Data Center Architect','EMCDCA - Data Center Architect',0,'EMC');
insert into certifications(`name`,label,description,hide,groupid)
values ('EMCIE','EMCIE - Implementation Engineer','EMCIE - Implementation Engineer',0,'EMC');
insert into certifications(`name`,label,description,hide,groupid)
values ('EMCISA','EMCISA - Information Storage and Management','EMCISA - Information Storage and Management',0,'EMC');
insert into certifications(`name`,label,description,hide,groupid)
values ('EMCPE','EMCPE - Platform Engineer','EMCPE - Platform Engineer',0,'EMC');
insert into certifications(`name`,label,description,hide,groupid)
values ('EMCSA','EMCSA - Storage Administrator','EMCSA - Storage Administrator',0,'EMC');
insert into certifications(`name`,label,description,hide,groupid)
values ('EMCSyA','EMCSyA - System Administrator','EMCSyA - System Administrator',0,'EMC');
insert into certifications(`name`,label,description,hide,groupid)
values ('EMCTA','EMCTA - Technology Architect','EMCTA - Technology Architect',0,'EMC');

select * from certifications

