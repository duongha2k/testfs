-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Host: saturntestrestore2.cd3kywrhh0ei.us-east-1.rds.amazonaws.com
-- Generation Time: Aug 15, 2013 at 05:32 AM
-- Server version: 5.1.63
-- PHP Version: 5.3.2-1ubuntu4.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `saturntest`
--

-- --------------------------------------------------------

--
-- Table structure for table `custom_reports_tables`
--

CREATE TABLE IF NOT EXISTS `custom_reports_tables` (
  `table_code` varchar(50) NOT NULL,
  `table_title` varchar(255) DEFAULT NULL,
  `display_order` int(11) DEFAULT NULL,
  `special_where` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`table_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `custom_reports_tables`
--

INSERT INTO `custom_reports_tables` (`table_code`, `table_title`, `display_order`, `special_where`) VALUES
('projects', 'Projects', 4, 'Project_Company_ID = ''{Company_ID}'''),
('TechBankInfo', 'Tech Bank Info', 1, NULL),
('TechBankInfoWorkOrders', 'TechBankInfo --> Work_Orders', 6, 'Company_ID = ''{Company_ID}'''),
('timestamps', 'Timestamps', 5, 'WIN_NUM IN (SELECT WIN_NUM FROM work_orders WHERE Company_ID=''{Company_ID}'')'),
('WorkOrdersTimestamps', 'Work_Orders --> timestamps', 7, 'Company_ID = ''{Company_ID}'''),
('WorkOrdersVisits', 'Work_Orders --> visits', 8, 'Company_ID = ''{Company_ID}'''),
('WorkordersWorkordersbids', 'Work Orders --> Work Orders Bids', 9, 'Company_ID = ''{Company_ID}'''),
('work_orders', 'Work Order', 2, 'Company_ID = ''{Company_ID}'''),
('work_orders__bids', 'Work Orders Bids', 3, 'Company_ID = ''{Company_ID}''');
