insert into certifications(id,`name`,label,description,hide,groupid)
values(39,'PurpleTechnicianUnverified','Unverified Purple Technician','Unverified Purple Technician',0,'Purple');

insert into certifications(id,`name`,label,description,hide,groupid)
values(40,'PurpleTechnicianCertified','Verified Purple Technician','Verified Purple Technician',0,'Purple');

CREATE TABLE `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `displayorder` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `languages` */

insert  into `languages`(`id`,`code`,`name`,`displayorder`) values (1,NULL,'Arabic',1);
insert  into `languages`(`id`,`code`,`name`,`displayorder`) values (2,NULL,'French',2);
insert  into `languages`(`id`,`code`,`name`,`displayorder`) values (3,NULL,'German',3);
insert  into `languages`(`id`,`code`,`name`,`displayorder`) values (4,NULL,'Italian',4);
insert  into `languages`(`id`,`code`,`name`,`displayorder`) values (5,NULL,'Chinese',5);
insert  into `languages`(`id`,`code`,`name`,`displayorder`) values (6,NULL,'Japanese',6);
insert  into `languages`(`id`,`code`,`name`,`displayorder`) values (7,NULL,'Russian',7);
insert  into `languages`(`id`,`code`,`name`,`displayorder`) values (8,NULL,'Spanish',8);
insert  into `languages`(`id`,`code`,`name`,`displayorder`) values (9,NULL,'American Sign Language',9);

CREATE TABLE `tech_languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `TechID` int(11) NOT NULL,
  `LangID` int(11) NOT NULL,
  `SpeakerLevel` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

