ALTER TABLE `work_orders` CHANGE `Total_Estimated_WO_Value` `Total_Estimated_WO_Value` DECIMAL(10,2) NULL DEFAULT NULL;
ALTER TABLE `work_orders` CHANGE `Tech_Bid_Amount` `Tech_Bid_Amount` DECIMAL(10,2) NULL DEFAULT NULL;

