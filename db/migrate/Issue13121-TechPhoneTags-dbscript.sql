-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Host: saturntestrestore.cd3kywrhh0ei.us-east-1.rds.amazonaws.com
-- Generation Time: Mar 02, 2012 at 12:00 AM
-- Server version: 5.1.57
-- PHP Version: 5.3.2-1ubuntu4.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

USE `saturntest1`

--
-- Database: `saturntest`
--

-- --------------------------------------------------------

--
-- Table structure for table `phone_audit_log`
--
DROP TABLE IF EXISTS `phone_audit_log`;

CREATE TABLE IF NOT EXISTS `phone_audit_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auditdate` datetime DEFAULT NULL,
  `phonetype` int(11) DEFAULT NULL,
  `techphonevalue` varchar(50) DEFAULT NULL,
  `actionid` int(11) DEFAULT NULL,
  `techid` varchar(11) DEFAULT NULL,
  `reportingclient` varchar(200) DEFAULT NULL,
  `reportinguser` varchar(50) DEFAULT NULL,
  `reportingusertype` int(11) DEFAULT NULL,
  `reportingclientid` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

-- --------------------------------------------------------

--
-- Table structure for table `tech_ext`
--
DROP TABLE IF EXISTS `tech_ext`;

CREATE TABLE IF NOT EXISTS `tech_ext` (
  `TechID` varchar(11) NOT NULL,
  `PrimaryPhoneStatus` int(11) DEFAULT NULL,
  `SecondaryPhoneStatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`TechID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
