
-- TechBankInfo - insert columns
ALTER TABLE TechBankInfo ADD COLUMN AlarmSystemsSelfRating int;
ALTER TABLE TechBankInfo ADD COLUMN AlarmSystemsYN tinyint DEFAULT 0;

-- wo_categories - Alarm Systems
INSERT INTO wo_categories(Category_ID,Category,Abbr,TechSelfRatingColumn)
VALUES(38,'Alarm Systems','AlarmSystems','AlarmSystemsSelfRating');

-- fsexperts
INSERT INTO fsexperts (Id,Code,Title,FSExpertEnable,Abbreviation)
VALUES(49,'AlarmSystems','Alarm Systems',0,'AlarmSystems');




