/*
SQLyog Enterprise - MySQL GUI v7.14 
MySQL - 5.1.33-community : Database - saturn
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `work_orders_ext` */

DROP TABLE IF EXISTS `work_orders_additional_fields`;

CREATE TABLE `work_orders_additional_fields` (
  `WINNUM` int(11) NOT NULL,
  `AuditNeeded` tinyint(1) DEFAULT '0',
  `AuditNeededBy` varchar(20) DEFAULT NULL,
  `AuditNeededDate` datetime DEFAULT NULL,
  `AuditComplete` tinyint(1) DEFAULT '0',
  `AuditCompleteBy` varchar(255) DEFAULT NULL,
  `AuditCompleteDate` datetime DEFAULT NULL,
  PRIMARY KEY (`WINNUM`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
