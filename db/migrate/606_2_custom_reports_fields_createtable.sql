-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Host: saturntestrestore2.cd3kywrhh0ei.us-east-1.rds.amazonaws.com
-- Generation Time: Aug 15, 2013 at 05:25 AM
-- Server version: 5.1.63
-- PHP Version: 5.3.2-1ubuntu4.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `saturntest`
--

-- --------------------------------------------------------

--
-- Table structure for table `custom_reports_fields`
--

CREATE TABLE IF NOT EXISTS `custom_reports_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rep_id` int(11) NOT NULL,
  `fieldname` varchar(255) NOT NULL,
  `fieldcaption` varchar(255) NOT NULL,
  `display_order` int(11) NOT NULL,
  `isdisplayed` tinyint(4) NOT NULL DEFAULT '1',
  `issum` tinyint(4) NOT NULL DEFAULT '0',
  `iscount` tinyint(4) NOT NULL DEFAULT '0',
  `issortby` tinyint(4) NOT NULL DEFAULT '0',
  `isgroupby` tinyint(4) NOT NULL DEFAULT '0',
  `groupid` int(11) NOT NULL DEFAULT '1',
  `isavg` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
