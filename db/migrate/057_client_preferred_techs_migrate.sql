CREATE TABLE client_preferred_techs (
	id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,
	Tech_ID INT(10),
	PreferLevel INT(10),
	CompanyID VARCHAR(255)
);