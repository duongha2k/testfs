/*
SQLyog Enterprise - MySQL GUI v7.14 
MySQL - 5.1.33-community : Database - saturn
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


USE `saturntest`;

/*Table structure for table `ass_question_items` */

DROP TABLE IF EXISTS `ass_question_items`;

CREATE TABLE `ass_question_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prefix` varchar(20) DEFAULT NULL,
  `descr` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ass_question_id` int(11) NOT NULL,
  `is_answer_key` int(11) NOT NULL DEFAULT '0',
  `presentation_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=76 DEFAULT CHARSET=latin1;

/*Data for the table `ass_question_items` */

insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (1,'A.','Digital',1,0,0);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (2,'B.','Analog',1,0,1);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (3,'C.','Digital & Analog',1,0,2);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (4,'','Canon',2,0,3);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (5,'','Ricoh/Savin/Lanier',2,0,4);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (6,'','Konica Minolta',2,0,5);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (7,'','Xerox',2,0,6);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (8,'','OCE',2,0,7);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (9,'','Kyocera/Mita',2,0,8);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (10,'','Gestetner',2,0,9);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (11,'','Toshiba',2,0,10);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (12,'','Pitney Bowes',2,0,11);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (13,'','Sharp',2,0,12);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (14,'','Panasonic',2,0,13);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (15,'A.','0-1 Year',3,0,14);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (16,'B.','1-4 Years',3,0,15);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (17,'C.','5-10 Years',3,0,16);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (18,'D.','10+ Years',3,0,17);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (19,'','Cleaning (rollers, doc feeder, sorters, trays, transport deck, paper path)',4,0,18);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (20,'','Developer Replacement',4,0,19);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (21,'','Drum/Cleaning Blade Replacement',4,0,20);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (22,'','Fuser Roller Replacement / Fuser Rebuild',4,0,21);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (23,'','Paper Tray Roller Replacement',4,0,22);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (24,'','Board Replacement',4,0,23);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (25,'','Firmware Updates',4,0,24);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (26,'','Networking Troubleshooting',4,0,25);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (27,'','Scanning Troubleshooting',4,0,26);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (28,'','Fax Troubleshooting',4,0,27);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (29,'A.','Image Exposure, Charging, Developing, Fixing, Transfer, Drum Cleaning',5,0,28);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (30,'B.','Charging, Image Exposure, Developing, Transfer, Fixing, Drum Cleaning',5,1,29);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (31,'C.','Charging, Developing, Image Exposure, Fixing, Transfer, Drum Cleaning',5,0,30);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (32,'D.','Charging, Developing, Image Exposure, Transfer, Drum Cleaning, Fixing',5,0,31);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (33,'A.','Copiers have more parts and can take significantly more time to disassemble/reassemble.',6,1,32);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (34,'B.','Copiers are all multifunction devices and printers may not be.',6,0,33);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (35,'C.','Copiers use more paper.',6,0,34);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (36,'D.','There is no real difference.',6,0,35);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (37,'A.','Scan Glass',7,1,36);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (38,'B.','Mirrors',7,0,37);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (39,'C.','Drum',7,0,38);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (40,'D.','Fuser',7,0,39);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (41,'E.','A and C',7,1,40);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (42,'A.','Run a test copy, review the line on the paper, and install a new fusing unit.',8,0,41);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (43,'B.','Run a test copy without the toner cartridge installed.',8,0,42);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (44,'C.','Run a test copy, stop the copy as it is passing the drum unit, and visually inspect drum & fusing roller.',8,1,43);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (45,'D.','Run a test copy and ask the End User for help.',8,0,44);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (46,'A.','Feed Rollers',9,0,45);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (47,'B.','Drum',9,0,46);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (48,'C.','Original Glass',9,0,47);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (49,'D.','Mirrors',9,0,48);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (50,'E.','C and D',9,1,49);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (51,'A.','Drum',10,0,50);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (52,'B.','Feed Rollers',10,0,51);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (53,'C.','Original Glass',10,0,52);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (54,'D.','Transfer / Separation Corona Assembly',10,0,53);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (55,'E.','Cleaning Blade',10,0,54);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (56,'F.','Developer',10,0,55);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (57,'G.','Main Charge Corona Assembly',10,0,56);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (58,'H.','D and G',10,1,57);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (59,'I.','All of the Above',10,0,58);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (60,'A.','Review tray 1 and tray 2 to see why they are performing properly.',11,0,59);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (61,'B.','Inspect the pick-off fingers in the fusing unit.',11,0,60);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (62,'C.','Jig the door, and visually inspect paper exiting tray three.',11,0,61);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (63,'D.','Visually inspect paper is loaded properly in tray 3.',11,0,62);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (64,'E.','C and D',11,1,63);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (65,'F.','All of the Above',11,0,64);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (66,'A.','Check to see if the line has a dial tone.',12,0,65);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (67,'B.','Send a test fax to another working fax device.',12,0,66);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (68,'C.','Call the fax number to verify it is a valid line.',12,0,67);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (69,'D.','Check to see if the line is plugged into the correct port.',12,0,68);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (70,'E.','Use an analog telephone to dial out on the line.',12,0,69);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (71,'F.','All of the above.',12,1,70);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (72,'A.','Check the transport belt',13,0,71);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (73,'B.','Clean tray 1 feed rollers',13,0,72);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (74,'C.','Run a test page through the document feeder.',13,1,73);
insert  into `ass_question_items`(`id`,`prefix`,`descr`,`ass_question_id`,`is_answer_key`,`presentation_order`) values (75,'D.','Disconnect the sorter.',13,0,74);

/*Table structure for table `ass_questions` */

DROP TABLE IF EXISTS `ass_questions`;

CREATE TABLE `ass_questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descr` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `parentid` int(11) NOT NULL DEFAULT '0',
  `isscored` int(11) NOT NULL DEFAULT '0',
  `presentation_order` int(11) NOT NULL DEFAULT '0',
  `question_type_id` int(11) NOT NULL COMMENT '1:survey; 2:quiz ',
  `question_item_type_id` int(11) NOT NULL COMMENT '1: option question,one selection allowed; 2: contains YMquestions multi selections allowed ',
  `ass_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `ass_questions` */

insert  into `ass_questions`(`id`,`descr`,`parentid`,`isscored`,`presentation_order`,`question_type_id`,`question_item_type_id`,`ass_id`,`section_id`) values (1,'Select the type of Copiers you have worked on.',0,0,0,1,1,1,0);
insert  into `ass_questions`(`id`,`descr`,`parentid`,`isscored`,`presentation_order`,`question_type_id`,`question_item_type_id`,`ass_id`,`section_id`) values (2,'Select ALL Brands of Copiers you have experience repairing.',0,0,10,1,2,1,0);
insert  into `ass_questions`(`id`,`descr`,`parentid`,`isscored`,`presentation_order`,`question_type_id`,`question_item_type_id`,`ass_id`,`section_id`) values (3,'How many Years of experience do you have working on Copiers?',0,0,20,1,1,1,0);
insert  into `ass_questions`(`id`,`descr`,`parentid`,`isscored`,`presentation_order`,`question_type_id`,`question_item_type_id`,`ass_id`,`section_id`) values (4,'Select ALL the Repairs/Services you have performed on Copiers.',0,0,30,1,2,1,0);
insert  into `ass_questions`(`id`,`descr`,`parentid`,`isscored`,`presentation_order`,`question_type_id`,`question_item_type_id`,`ass_id`,`section_id`) values (5,'What is the order of the “Copy Process”?  Select the proper order from the answers below.',0,1,40,2,1,1,0);
insert  into `ass_questions`(`id`,`descr`,`parentid`,`isscored`,`presentation_order`,`question_type_id`,`question_item_type_id`,`ass_id`,`section_id`) values (6,'What is the main difference between a printer and Digital Copier?  Select All that apply.',0,1,50,2,1,1,0);
insert  into `ass_questions`(`id`,`descr`,`parentid`,`isscored`,`presentation_order`,`question_type_id`,`question_item_type_id`,`ass_id`,`section_id`) values (7,'What would you check if a Digital Copier is making lines on copies when using the document feeder (handler) on a digital copier, but not on the prints from the machine?',0,1,60,2,1,1,0);
insert  into `ass_questions`(`id`,`descr`,`parentid`,`isscored`,`presentation_order`,`question_type_id`,`question_item_type_id`,`ass_id`,`section_id`) values (8,'How would you determine what is causing a black line without taking the drum unit or the fusing unit out of the machine?',0,1,70,2,1,1,0);
insert  into `ass_questions`(`id`,`descr`,`parentid`,`isscored`,`presentation_order`,`question_type_id`,`question_item_type_id`,`ass_id`,`section_id`) values (9,'What would you clean if a Digital Copier was making light copies when placed on the original scan glass but not when printing?',0,1,80,2,1,1,0);
insert  into `ass_questions`(`id`,`descr`,`parentid`,`isscored`,`presentation_order`,`question_type_id`,`question_item_type_id`,`ass_id`,`section_id`) values (10,'What would you clean if a Digital Copier was making light copies when printing?',0,1,90,2,1,1,0);
insert  into `ass_questions`(`id`,`descr`,`parentid`,`isscored`,`presentation_order`,`question_type_id`,`question_item_type_id`,`ass_id`,`section_id`) values (11,'The Digital Copier is jamming out of the third tray, but not out of tray one or tray two.  What would you check to resolve the issue?',0,1,100,2,1,1,0);
insert  into `ass_questions`(`id`,`descr`,`parentid`,`isscored`,`presentation_order`,`question_type_id`,`question_item_type_id`,`ass_id`,`section_id`) values (12,'The Fax on a Digital Copier is not working.  What would you check to resolve the issue?',0,1,110,2,1,1,0);
insert  into `ass_questions`(`id`,`descr`,`parentid`,`isscored`,`presentation_order`,`question_type_id`,`question_item_type_id`,`ass_id`,`section_id`) values (13,'If the document feeder (handler) is jamming, what is one of the first things you would do upon arriving onsite with the troubled Digital Copier?',0,1,120,2,1,1,0);

/*Table structure for table `assessments` */

DROP TABLE IF EXISTS `assessments`;

CREATE TABLE IF NOT EXISTS `assessments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descr` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `passing_score` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `assessments`
--

INSERT INTO `assessments` (`id`, `descr`, `passing_score`) VALUES
(1, 'Copier Repair Skills Assessment', 7);

/*Table structure for table `tech_ass` */

DROP TABLE IF EXISTS `tech_ass`;

CREATE TABLE `tech_ass` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tech_id` int(11) NOT NULL,
  `ass_id` int(11) DEFAULT NULL,
  `ispassed` int(11) DEFAULT NULL,
  `passed_date` datetime DEFAULT NULL,
  `tryingtimes` int(11) DEFAULT NULL,
  `passed_answerkeys` varchar(255) DEFAULT NULL,
  `passed_score` int(11) DEFAULT NULL,
  `last_score` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `tech_ass` */

/*Table structure for table `tech_ass_answers` */

DROP TABLE IF EXISTS `tech_ass_answers`;

CREATE TABLE `tech_ass_answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tech_id` int(11) NOT NULL,
  `ass_question_id` int(11) NOT NULL,
  `ass_question_item_id` int(11) NOT NULL,
  `date_taken` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `tech_ass_answers` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
