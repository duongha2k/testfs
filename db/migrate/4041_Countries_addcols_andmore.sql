
-- add columns 
ALTER TABLE Countries ADD COLUMN Region varchar(255);
ALTER TABLE Countries ADD COLUMN `Disable` tinyint DEFAULT 0;

-- insert rows for new countries
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (243,'AR','Argentina',1,1,'Central / South America');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (244,'CB','Caribbean',4,0,'Central / South America');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (245,'CR','Costa Rica',5,1,'Central / South America');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (246,'VE','Venezuela',6,1,'Central / South America');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (247,'AT','Austria',1,1,'EMEA');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (248,'BE','Belgium',2,1,'EMEA');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (249,'DK','Denmark',3,1,'EMEA');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (250,'FR','France',5,1,'EMEA');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (251,'DE','Germany',6,1,'EMEA');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (252,'IE','Ireland',7,1,'EMEA');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (253,'IT','Italy',8,1,'EMEA');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (254,'NL','Netherlands',9,1,'EMEA');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (255,'SP','Spain',10,1,'EMEA');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (256,'SE','Sweden',11,1,'EMEA');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (257,'CH','Switzerland',12,1,'EMEA');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (258,'TR','Turkey',13,1,'EMEA');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (259,'UK','United Kingdom',14,1,'EMEA');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (260,'AU','Australia',1,1,'APAC');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (261,'CN','China',2,1,'APAC');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (262,'HK','Hong Kong',3,1,'APAC');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (263,'IN','India',4,1,'APAC');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (264,'JP','Japan',5,1,'APAC');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (265,'MY','Malaysia',6,1,'APAC');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (266,'PH','Phillipines',7,1,'APAC');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (267,'SG','Singapore',8,1,'APAC');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (268,'KR','South Korea',9,1,'APAC');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (269,'TW','Taiwan',10,1,'APAC');
insert  into `Countries`(`id`,`Code`,`Name`,`DisplayPriority`,`ZipRequired`,`Region`) values (270,'TH','Thailand',11,1,'APAC');

-- update data
UPDATE  Countries SET Region = 'North America' WHERE Code IN('US','CA','MX');
UPDATE  Countries SET Region = 'Central / South America' WHERE Code IN('BS','BR','CB');
UPDATE  Countries SET `Disable` = '0',Region = 'Central / South America',DisplayPriority = '4' WHERE Code ='KY';
UPDATE  Countries SET DisplayPriority = '1' WHERE Code ='US';
UPDATE  Countries SET DisplayPriority = '2' WHERE Code ='CA';
UPDATE  Countries SET DisplayPriority = '3' WHERE Code ='MX';
UPDATE  Countries SET DisplayPriority = '4' WHERE Region ='Central / South America';

-- TechBankInfo - update Country
UPDATE TechBankInfo SET Country='UK' WHERE Country='EN';

-- work_orders - update Country
UPDATE work_orders SET Country='UK' WHERE Country='EN';

-- states_list - delete state for Brazil, Cayman Islands, Bahamas (BR, KY, BS)
DELETE FROM states_list WHERE Country IN ('BR','KY','BS');

 






