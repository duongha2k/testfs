UPDATE TechBankInfo SET Bg_Test_ResultsDate_Lite = BG_Test_ResultsDate_Full,
Bg_Test_Pass_Lite = Bg_Test_Pass_Full,
Bg_Test_ReqDate_Lite = Bg_Test_ReqDate_Full,
Bg_Test_Req_Lite = Bg_Test_Req_Full WHERE BG_Test_ResultsDate_Full >= Bg_Test_ResultsDate_Lite OR (BG_Test_ResultsDate_Full IS NOT NULL AND BG_Test_ResultsDate_Lite IS NULL);