-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Host: saturntestrestore.cd3kywrhh0ei.us-east-1.rds.amazonaws.com
-- Generation Time: Jul 23, 2012 at 09:01 PM
-- Server version: 5.1.57
-- PHP Version: 5.3.2-1ubuntu4.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `saturntest`
--

-- --------------------------------------------------------

--
-- Table structure for table `FLS_Test`
--

CREATE TABLE IF NOT EXISTS `ComputersPlus_Test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tech_id` int(11) NOT NULL,
  `date_taken` datetime DEFAULT NULL,
  `score` int(11) NOT NULL,
  `a1` int(1) NOT NULL,
  `a2` int(1) NOT NULL,
  `a3` int(1) NOT NULL,
  `a4` int(1) NOT NULL,
  `a5` int(1) NOT NULL,
  `a6` int(1) NOT NULL,
  `a7` int(1) NOT NULL,
  `a8` int(1) NOT NULL,
  `a9` int(1) NOT NULL,
  `a10` int(1) NOT NULL,
  `a11` int(1) NOT NULL,
  `a12` int(1) NOT NULL,
  `a13` int(1) NOT NULL,
  `a14` int(1) NOT NULL,
  `a15` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tech_id` (`tech_id`,`date_taken`,`score`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

Insert Into certifications(`id`,`name`,`label`,`description`,`hide`)
values(24,'ComputerPlus','Computer Plus Certification','Computer Plus Certification',0)


