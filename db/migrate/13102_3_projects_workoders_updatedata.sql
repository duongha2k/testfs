-- projects
UPDATE projects
SET WO_Category_ID = 27 WHERE Project_ID IN (SELECT Project_ID FROM PROJECT_13012 WHERE NewCategory='Printers');

UPDATE projects
SET WO_Category_ID = 28 WHERE Project_ID IN (SELECT Project_ID FROM PROJECT_13012 WHERE NewCategory='Copiers');

-- work_orders
UPDATE work_orders
SET WO_Category_ID = 27 WHERE Project_ID IN (SELECT Project_ID FROM PROJECT_13012 WHERE NewCategory='Printers');

UPDATE work_orders
SET WO_Category_ID = 28 WHERE Project_ID IN (SELECT Project_ID FROM PROJECT_13012 WHERE NewCategory='Copiers');

-- work_orders 
UPDATE work_orders 
SET WO_Category_ID = 27 WHERE DateEntered <= DATE_SUB(NOW(), INTERVAL 1 YEAR) AND WO_Category_ID = 11 
 AND Project_ID IN (Select Project_ID From projects WHERE WO_Category_ID != 11);

UPDATE work_orders SET WO_Category_ID = 27 WHERE DateEntered > DATE_SUB(NOW(), INTERVAL 1 YEAR) 
 AND WIN_NUM IN (SELECT WIN_NUM FROM WO_13012 WHERE NewCategory='Printers' AND ProjectID=work_orders.Project_ID);

UPDATE work_orders SET WO_Category_ID = 28 WHERE DateEntered > DATE_SUB(NOW(), INTERVAL 1 YEAR) 
 AND WIN_NUM IN (SELECT WIN_NUM FROM WO_13012 WHERE NewCategory='Copier' AND ProjectID=work_orders.Project_ID);






