/*Table structure for table `clicompanies_additional_fields` */

DROP TABLE IF EXISTS `clicompanies_additional_fields`;

CREATE TABLE `clicompanies_additional_fields` (
  `companyid` varchar(255) NOT NULL,
  `enable_expense_reporting` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`companyid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `clicompanies_bcats` */

DROP TABLE IF EXISTS `clicompanies_bcats`;

CREATE TABLE `clicompanies_bcats` (
  `bcatid` int(11) NOT NULL AUTO_INCREMENT,
  `companyid` varchar(255) NOT NULL,
  `bcatname` varchar(255) DEFAULT NULL,
  `bcatdesc` varchar(255) DEFAULT NULL,
  `ischecked` tinyint(4) DEFAULT '0',
  `reimbursable_thru_fs` tinyint(4) DEFAULT '0',
  `require_doc_upl` tinyint(4) DEFAULT '0',
  `bcattype` varchar(20) NOT NULL,
  `display_order` int(11) DEFAULT '0',
  PRIMARY KEY (`bcatid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `clicompanies_additional_fields` */

DROP TABLE IF EXISTS `projects_additional_fields`;

CREATE TABLE `projects_additional_fields` (
  `projectid` int NOT NULL,
  `enable_expense_reporting` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`projectid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `projects_bcats` */

DROP TABLE IF EXISTS `projects_bcats`;

CREATE TABLE `projects_bcats` (
  `bcatid` int(11) NOT NULL AUTO_INCREMENT,
  `projectid` int NOT NULL,
  `bcatname` varchar(255) DEFAULT NULL,
  `bcatdesc` varchar(255) DEFAULT NULL,
  `ischecked` tinyint(4) DEFAULT '0',
  `reimbursable_thru_fs` tinyint(4) DEFAULT '0',
  `require_doc_upl` tinyint(4) DEFAULT '0',
  `bcattype` varchar(20) NOT NULL,
  `display_order` int(11) DEFAULT '0',
  PRIMARY KEY (`bcatid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `work_orders_bcats` */

DROP TABLE IF EXISTS `work_orders_bcats`;

CREATE TABLE `work_orders_bcats` (
  `bcatid` int(11) NOT NULL AUTO_INCREMENT,
  `winid` int(11) NOT NULL,
  `bcatname` varchar(255) DEFAULT NULL,
  `bcatdesc` varchar(255) DEFAULT NULL,
  `ischecked` tinyint(4) DEFAULT '0',
  `reimbursable_thru_fs` tinyint(4) DEFAULT '0',
  `require_doc_upl` tinyint(4) DEFAULT '0',
  `bcattype` varchar(20) NOT NULL,
  `display_order` int(11) DEFAULT '0',
  `tech_claim` float DEFAULT NULL,
  `approved` tinyint(4) DEFAULT NULL,
  `approved_amount` float DEFAULT NULL,
  PRIMARY KEY (`bcatid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


ALTER TABLE work_orders_additional_fields ADD COLUMN enable_expense_reporting tinyint;

ALTER TABLE files ADD COLUMN uplfilename varchar(255);
ALTER TABLE files ADD COLUMN wobcatid int;






