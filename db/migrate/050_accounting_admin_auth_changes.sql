ALTER TABLE `admins` ADD COLUMN `accounting` TINYINT(1) NOT NULL; 
INSERT INTO `admins` (Fname, Lname, UserName, Password, Email, accounting) 
			VALUES ('Ali', 'McCray', 'amccray', 'florida', 'amccray@fieldsolutions.com', '1');

UPDATE admins SET accounting = '1'  WHERE Fname = 'Gerald' AND Lname = 'Bailey';
UPDATE admins SET accounting = '1'  WHERE Fname = 'John' AND Lname = 'Sinning';
UPDATE admins SET accounting = '1'  WHERE Fname = 'Teddy' AND Lname = 'Mueller';
UPDATE admins SET accounting = '1'  WHERE Fname = 'Doug' AND Lname = 'Parson';