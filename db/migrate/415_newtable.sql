/*
SQLyog Enterprise - MySQL GUI v7.14 
MySQL - 5.1.33-community : Database - saturn
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `work_orders_techwithdrawbids` */

DROP TABLE IF EXISTS `work_orders_techwithdrawbids`;

CREATE TABLE `work_orders_techwithdrawbids` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `WorkOrderID` int(11) DEFAULT NULL,
  `TechID` int(11) DEFAULT NULL,
  `WithDrawDate` datetime DEFAULT NULL,
  `LastBid_Amount` varchar(255) DEFAULT NULL,
  `LastBid_Date` datetime DEFAULT NULL,
  `LastBid_Comments` varchar(255) DEFAULT NULL,
  `LastBid_DeactivatedTech` tinyint(4) DEFAULT NULL,
  `LastBid_Hide` tinyint(4) DEFAULT NULL,
  `LastBid_CreateByUser` varchar(255) DEFAULT NULL,
  `LastBid_caspio_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
