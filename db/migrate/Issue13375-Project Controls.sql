ALTER TABLE `projects` ADD `Owner` VARCHAR( 255 ) NULL DEFAULT NULL ,
ADD INDEX ( `Owner` )

CREATE TABLE IF NOT EXISTS `projects_users_allowed_createwo` (
  `Project_ID` int(11) NOT NULL,
  `UserName` varchar(255) NOT NULL,
  PRIMARY KEY (`Project_ID`,`UserName`),
  KEY `Project_ID` (`Project_ID`),
  KEY `UserName` (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='list of users allowed to create wo for a project';

CREATE TABLE IF NOT EXISTS `dashboard_settings` (
  `client_id` int(11) NOT NULL,
  `filter` int(11) NOT NULL DEFAULT '0' COMMENT '0 = none, 1 = my projects, 2 = select project, 3 = work orders',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '0 = default, 1 = hold, 2 = own default ',
  `defaultCreatedSort` varchar(30) DEFAULT NULL,
  `defaultCreatedSortDir` tinyint(4) DEFAULT NULL,
  `defaultPublishedSort` varchar(30) DEFAULT NULL,
  `defaultPublishedSortDir` tinyint(4) DEFAULT NULL,
  `defaultAssignedSort` varchar(30) DEFAULT NULL,
  `defaultAssignedSortDir` tinyint(4) DEFAULT NULL,
  `defaultWorkDoneSort` varchar(30) DEFAULT NULL,
  `defaultWorkDoneSortDir` tinyint(4) DEFAULT NULL,
  `defaultIncompleteSort` varchar(30) DEFAULT NULL,
  `defaultIncompleteSortDir` tinyint(4) DEFAULT NULL,
  `defaultCompletedSort` varchar(30) DEFAULT NULL,
  `defaultCompletedSortDir` tinyint(4) DEFAULT NULL,
  `defaultDeactivatedSort` varchar(30) DEFAULT NULL,
  `defaultDeactivatedSortDir` tinyint(4) DEFAULT NULL,
  `defaultAllSort` varchar(30) DEFAULT NULL,
  `defaultAllSortDir` tinyint(4) DEFAULT NULL,
  `view` int(11) NOT NULL DEFAULT '0' COMMENT '0 = full, 1 = lite',
  `dbState` text,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `dashboard_settings_project` (
  `client_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`client_id`,`project_id`),
  KEY `client_id` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `dashboard_settings_work_order` (
  `client_id` int(11) NOT NULL,
  `filter` int(11) NOT NULL COMMENT '0 = none, 1 = created, 2 = published, 4 = assigned, 8 = approved (bitwise flag)',
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
