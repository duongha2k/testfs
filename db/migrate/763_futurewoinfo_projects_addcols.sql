
ALTER TABLE future_wo_info ADD COLUMN WorkAcceptedByTechEmailTo varchar(255);
ALTER TABLE future_wo_info ADD COLUMN WorkConfirmedByTechEmailTo varchar(255);
ALTER TABLE future_wo_info ADD COLUMN TechCheckedInEmailTo varchar(255);
ALTER TABLE future_wo_info ADD COLUMN TechCheckedOutEmailTo varchar(255);

ALTER TABLE  projects ADD COLUMN WorkAcceptedByTechEmailTo varchar(255);
ALTER TABLE  projects ADD COLUMN WorkConfirmedByTechEmailTo varchar(255);
ALTER TABLE  projects ADD COLUMN TechCheckedInEmailTo varchar(255);
ALTER TABLE  projects ADD COLUMN TechCheckedOutEmailTo varchar(255);

ALTER TABLE  projects ADD COLUMN ReceiveWorkAcceptedByTechEmail tinyint;
ALTER TABLE  projects ADD COLUMN ReceiveWorkConfirmedByTechEmail tinyint;
ALTER TABLE  projects ADD COLUMN ReceiveTechCheckedInEmail tinyint;
ALTER TABLE  projects ADD COLUMN ReceiveTechCheckedOutEmail tinyint;

