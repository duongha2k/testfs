-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Host: saturntestrestore2.cd3kywrhh0ei.us-east-1.rds.amazonaws.com
-- Generation Time: Aug 15, 2013 at 05:31 AM
-- Server version: 5.1.63
-- PHP Version: 5.3.2-1ubuntu4.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `saturntest`
--

-- --------------------------------------------------------

--
-- Table structure for table `custom_reports_filters_selmulti`
--

CREATE TABLE IF NOT EXISTS `custom_reports_filters_selmulti` (
  `filter_id` int(11) NOT NULL,
  `filter_value` varchar(255) NOT NULL,
  PRIMARY KEY (`filter_id`,`filter_value`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
