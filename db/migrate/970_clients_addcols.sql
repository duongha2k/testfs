

-- clients - Periods: Last90Days, Last12Months, Lifetime
ALTER TABLE clients ADD COLUMN DeniedTech_NoShowGreater int DEFAULT 0;
ALTER TABLE clients ADD COLUMN DeniedTech_NoShowPeriod varchar(50) DEFAULT '';
ALTER TABLE clients ADD COLUMN DeniedTech_BackOutGreater int DEFAULT 0;
ALTER TABLE clients ADD COLUMN DeniedTech_BackOutPeriod varchar(50) DEFAULT '';
ALTER TABLE clients ADD COLUMN DeniedTech_PreferenceRatingSmaller float DEFAULT 0;
ALTER TABLE clients ADD COLUMN DeniedTech_PreferenceRatingPeriod varchar(50) DEFAULT '';
ALTER TABLE clients ADD COLUMN DeniedTech_PerformanceRatingSmaller float DEFAULT 0;
ALTER TABLE clients ADD COLUMN DeniedTech_PerformanceRatingPeriod varchar(50) DEFAULT '';

-- comments
ALTER TABLE comments ADD COLUMN GeneratedByFSSystem tinyint DEFAULT 0;

