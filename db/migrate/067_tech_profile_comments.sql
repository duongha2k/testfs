CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `comment` text NOT NULL,
  `WIN_NUM` int(11) DEFAULT NULL,
  `TechID` int(11) NOT NULL,
  `dateChanged` datetime NOT NULL,
  `type` int(11) NOT NULL,
  `clientID` int(10) unsigned NOT NULL,
  `classification` int(11) NOT NULL,
  `reportAbuseByClientID` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `WIN_NUM` (`WIN_NUM`),
  KEY `TechID` (`TechID`),
  KEY `clientID` (`clientID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `comments_helpful` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `ClientID` int(10) unsigned NOT NULL,
  `helpful` tinyint(1) NOT NULL,
  `comment_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ClientID` (`ClientID`),
  KEY `comment_id` (`comment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `comments_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` int(11) NOT NULL,
  `originalComment` text CHARACTER SET utf8 NOT NULL,
  `date` datetime NOT NULL,
  `comment_id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;
