CREATE TABLE IF NOT EXISTS `likes` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
  `TechID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

UPDATE `wo_categories` SET `Category` = 'Apple brand PCs / Laptops ' WHERE `wo_categories`.`Category_ID` =31;