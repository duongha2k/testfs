
-- TechBankInfo
ALTER TABLE TechBankInfo ADD COLUMN AdjustableBedRepairYN tinyint DEFAULT 0;
ALTER TABLE TechBankInfo ADD COLUMN AdjustableBedRepairSelfRating int DEFAULT NULL;

-- wo_categories
INSERT INTO wo_categories(Category_ID,Category,Abbr,TechSelfRatingColumn)
VALUES (36,'Adjustable Bed Repair','AdjustableBedRepair','AdjustableBedRepairSelfRating');


