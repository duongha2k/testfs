<?php

require_once(dirname(__FILE__) . '/../../includes/modules/common.init.php');

require_once(dirname(__FILE__) . "/../../htdocs/library/caspioAPI.php");

$user = 'vlad_warecorp';
$password = 'warecorp';


$companyList = caspioSelectAdv("TR_Client_List", "DISTINCT CompanyName, Company_ID", "ISNULL(Company_ID, '') <> '' AND ShowOnReports = '1'", "CompanyName ASC", false, "`", "|");

$companies = array();

//$companies[0] = 'SUSS';

foreach ($companyList as $company) {
	$company = explode("|", $company);
	$name = trim($company[0], "`");
	$id = trim($company[1], "`");
	$companies[] = $id;
}



$api = new Core_Api_Class();


//$api->techPrefer($user.'|pmContext=SUSS', $password, 36807); exit;

foreach ($companies as $company) {
    echo 'Processing company: ' . $company . "\n";
    
    $fieldListMap = array();
    $fieldListMap['id'] = 'id';
    $fieldListMap['Tech_ID'] = 'Tech_ID';
    
    $criteria = "CompanyID='" . Core_Caspio::caspioEscape($company) . "'";
    
    $result = Core_Caspio::caspioSelectWithFieldListMap(TABLE_CLIENT_PREFERRED_TECHS, $fieldListMap, $criteria, '');

    $techs = array();
    $toDelete = array();
    
    if (is_null($result ) ) {
        continue;
    }
    if (!is_array($result)) {
        var_dump($result);
        exit;
    }
    
    foreach ($result as $res) {
        if (in_array($res['Tech_ID'], $techs)) {
            $toDelete[] = $res['id'];
        } else {
            $techs[] = $res['Tech_ID'];
        }
    }
    
    foreach ($toDelete as $ti) {
        removeRecord($ti, $company);
    }

}


function removeRecord($id, $company)
{
    echo 'Remove: #' . $id . "\n";
    //echo "id = '$id' AND CompanyID = '$company'" . "\n";
    $result = Core_Caspio::caspioDelete(TABLE_CLIENT_PREFERRED_TECHS, "id = '$id' AND CompanyID = '$company'");
}

/*
$projects = $api->getAllProjects($user, $password);


$fieldListMap = array();
$fieldListMap['id'] = 'id';
$fieldListMap['Tech_ID'] = 'id';
$fieldListMap['CompanyID'] = 'id';
$fieldListMap['count'] = "COUNT(*)";

$records = array();

$result = Core_Caspio::caspioSelectWithFieldListMap(TABLE_MASTER_LIST, $fieldListMap, $criteria, '');

*/