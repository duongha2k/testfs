
ALTER TABLE custom_reports ADD COLUMN last_autosend_date  datetime DEFAULT NULL;

-- 2013-08-27
-- custom_reports
ALTER TABLE custom_reports ADD COLUMN report_type  int DEFAULT 1;

-- custom_reports_filters
ALTER TABLE custom_reports_filters ADD COLUMN time_tz  varchar(50);
