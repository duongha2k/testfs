CREATE INDEX Reminder1Hr ON work_orders (Reminder1Hr);
CREATE INDEX ReminderAll ON work_orders (ReminderAll);
CREATE INDEX Reminder24Hr ON work_orders (Reminder24Hr);
CREATE INDEX ReminderNotMarkComplete ON work_orders (ReminderNotMarkComplete);
CREATE INDEX ReminderIncomplete ON work_orders (ReminderIncomplete);
CREATE INDEX ReminderAcceptance ON work_orders (ReminderAcceptance);
CREATE INDEX CheckInCall ON work_orders (CheckInCall);
CREATE INDEX CheckOutCall ON work_orders (CheckOutCall);
