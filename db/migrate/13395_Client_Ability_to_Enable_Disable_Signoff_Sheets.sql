
ALTER TABLE `gbailey_technicianbureau`.`clients` ADD COLUMN `CustomSignOff_Enabled` TINYINT NOT NULL DEFAULT 1  AFTER `DateOfLastPasswordChanged` , ADD COLUMN `SignOff_Disabled` TINYINT NOT NULL DEFAULT 0  AFTER `CustomSignOff_Enabled` ;


ALTER TABLE `gbailey_technicianbureau`.`projects` ADD COLUMN `CustomSignOff_Enabled` TINYINT NOT NULL DEFAULT 1  AFTER `SiteContactReminderCallHours` , ADD COLUMN `SignOff_Disabled` TINYINT NOT NULL DEFAULT 0  AFTER `CustomSignOff_Enabled` ;


ALTER TABLE `gbailey_technicianbureau`.`work_orders` ADD COLUMN `CustomSignOff_Enabled` TINYINT NOT NULL DEFAULT 1  AFTER `Qty_Visits` , ADD COLUMN `SignOff_Disabled` TINYINT NOT NULL DEFAULT 0  AFTER `CustomSignOff_Enabled` ;



