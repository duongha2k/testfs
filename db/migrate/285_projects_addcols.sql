
ALTER TABLE projects
	ADD COLUMN BidNotificationEmails VARCHAR(255) NULL DEFAULT NULL,
	ADD COLUMN NotifyOnWorkDone tinyint(1) NOT NULL DEFAULT '0',
	ADD COLUMN WorkDoneNotificationEmails VARCHAR(255) NULL DEFAULT NULL,
	ADD COLUMN PartManagerUpdate_Required tinyint(1) NOT NULL DEFAULT '0';

ALTER TABLE work_orders
	ADD COLUMN PartManagerUpdate_Required tinyint(1) NOT NULL DEFAULT '0';
