
DROP TABLE IF EXISTS projects_rush_options;
CREATE TABLE  `projects_rush_options` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`project_id` INT NOT NULL ,
`type` ENUM(  'client',  'public',  'expert',  'industry' ) NOT NULL ,
`option_id` INT NOT NULL
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  `projects_rush_options` ADD INDEX  `project_id` (  `project_id` );
ALTER TABLE  `projects_rush_options` ADD  `level` ENUM(  'preferred',  'required' ) NOT NULL;
ALTER TABLE `projects_rush_options` CHANGE COLUMN `option_id` `option_id` VARCHAR(32) NOT NULL  ;
ALTER TABLE `projects_rush_options` CHANGE COLUMN `level` `level` INT(11) NULL  ;

DROP TABLE IF EXISTS work_orders_rush_options;
CREATE TABLE  `work_orders_rush_options` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`WIN_NUM` INT NOT NULL ,
`type` ENUM(  'public',  'client',  'expert',  'industry' ) NOT NULL ,
`option_id` INT NOT NULL ,
INDEX (  `WIN_NUM` )
) ENGINE = INNODB CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE  `work_orders_rush_options` ADD  `level` ENUM(  'preferred',  'required' ) NOT NULL;
ALTER TABLE `work_orders_rush_options` CHANGE COLUMN `option_id` `option_id` VARCHAR(32) NOT NULL  ;
ALTER TABLE `work_orders_rush_options` CHANGE COLUMN `level` `level` INT(11) NULL  ;

--ALTER TABLE `clients` ADD COLUMN `contextPushWMEnabled` TINYINT NOT NULL DEFAULT 0  AFTER `DeniedTech_PerformanceRatingPeriod` ;

