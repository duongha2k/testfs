<?php
require_once dirname(__FILE__).'/../../includes/modules/common.init.php';


$mappingFieldName = array(
	'AtLeast40HrsLast6Months' => '40HrsLast6Months',
	'AvayaGateway' => 'AyayaGateway',
	'AvayaGateway6mo' => 'AyayaGateway6mo',
	'FishTape50' => '50.',
	'HaveTruck' => 'TruckCarryLaddersCable',
	'LowVoltageLicensed' => 'PossessAllLicenses',
	'NortelIBMS6mo' => 'NortelBMS6mo',
	'MItel' => 'Mltel',
	'MItel6mo' => 'Mltel6mo',
	'OwnOrSourceCablingEquipment' => array('Cable', 'CableTesters', 'OwnOrSourceCablingEquipment')
);

$mappingTableName = array(
	'TR_Master_List' => 'TechBankInfo',
	'Tech_Cabling_Skills' => 'tech_equipment',
	'Tech_Telephony_Skills' => 'tech_skills'
);
$table = array(
	'TR_Master_List' => array('TechID','Laptop','CellPhone','DigitalCam','Ladder'),
	'Tech_Cabling_Skills' => array('TechID','HaveTruck','OwnOrSourceCablingEquipment','Ladder_6','Ladder_12','Ladder_20Plus','ToneGeneratorAndWand','ButtSet','DigitalVOMMeter','ContinuityTester','PunchTool66','PunchTool110','PunchToolBix','PunchToolKrone','CrimpToolRJ11','CrimpToolRJ45','FishTape50','RotaryCoaxCable','CordMasonryDrillHammer','RechargeCCTV','VoltOhmMeter','ReadWiringDiagrams','ExpInstallSurv', 'LowVoltageLicensed'),
	'Tech_Telephony_Skills' => array('TechID','Laptop','Screwdrivers','PunchTool','ToneGeneratorAndWand','ButtSet','InstallingTelephony','MaintainingTelephony','ServerTelephony','VOIPTelephony','AvayaPartnerMerlin','AvayaPartnerMerlin6mo','AvayaIPOffice','AvayaIPOffice6mo','AvayaDefinity','AvayaDefinity6mo','AvayaPBX8300etc','AvayaPBX8300etc6mo','AvayaGateway','AvayaGateway6mo','NortelNorstar','NortelNorstar6mo','NortelBMS','NortelIBMS6mo','NortelMeridian','NortelMeridian6mo','NortelCS','NortelCS6mo','CiscoCM','CiscoCM6mo','Siemens9751','Siemens97516mo','SiemensHicom','SiemensHicom6mo','SiemensHipath','NEC','NEC6mo','InterTel','InterTel6mo','MItel','MItel6mo','Toshiba','Toshiba6mo')
);


$tableInfoCaspio = Core_Caspio::caspioGetTableDesign('Tech_Cabling_Skills');
$table['Tech_Cabling_Skills'] = array();
foreach ($tableInfoCaspio as $info) {
	$columnInfo = explode(",",$info);
	if ($columnInfo[0] == 'UNID') continue;
//	$dbColumns[$columnInfo[0]] = $columnInfo[1];
	$table['Tech_Cabling_Skills'][] = $columnInfo[0];
}

$tableInfoCaspio = Core_Caspio::caspioGetTableDesign('Tech_Telephony_Skills');
$table['Tech_Telephony_Skills'] = array();
foreach ($tableInfoCaspio as $info) {
	$columnInfo = explode(",",$info);
	if ($columnInfo[0] == 'UNID') continue;
//	$dbColumns[$columnInfo[0]] = $columnInfo[1];
	$table['Tech_Telephony_Skills'][] = $columnInfo[0];
}

foreach ($table as $t => $fieldList) {

//print $t;

$idField = $fieldList[0];
$fieldListMap = array_combine($fieldList, $fieldList);

$maxId = 70000;
$step = 1000;

$startIndex = 1;
$db = Core_Database::getInstance();
//$tm = strtolower($t);

$tm = $mappingTableName[$t];

$select = $db->select();
$select->from('equipment', array('name', 'id'));
$equipmentList = $db->fetchPairs($select);

$select = $db->select();
$select->from('skills', array('name', 'id'));
$skillsList = $db->fetchPairs($select);

while($startIndex < $maxId) {
    $stopIndex =  $startIndex + $step;
	echo $t ."\n";
    $items = Core_Caspio::caspioSelectWithFieldListMap($t, $fieldListMap, $idField . ' >= ' .(int)$startIndex . ' AND ' . $idField . '  < ' . (int)$stopIndex, "$idField DESC");
	$errors = Core_Api_Error::getInstance();
	print_r($errors->getErrors());
//	print_r($items);

    if (is_array($items) ) {
        foreach ($items as $item) {
            
            $select = $db->select();
            $select->from('TechBankInfo', array('TechID'))
                   ->where($idField . ' = ?', (int)$item[$idField]);
//		echo $select; die();
        
            $result = $db->fetchRow($select);     
            
            if (!empty($result)) {

	                $toInsert = $item;
	                foreach ($toInsert as &$value) {
	                    if ($value === 'True') {
	                        $value = 1;
	                    } elseif ($value === 'False') {
	                        $value = 0;
	                    }
	                }
	                $newToInsert = array();
	                foreach ($toInsert as $k=>$v) {
	                        if (array_key_exists($k, $mappingFieldName)) {
								if (is_array($mappingFieldName[$k])) {
									foreach ($mappingFieldName[$k] as $f) {
										$newToInsert[$f] = $v;
									}
								}
								else
	                                $newToInsert[$mappingFieldName[$k]] = $v;
							}
	                        else
	                                $newToInsert[$k] = $v;
	                }
			$toInsert = $newToInsert;
		$mapping = null;
                switch ($t) {
                        case 'TR_Master_List':
			$mapping = 1;
                        break;
                        case 'Tech_Cabling_Skills':
                        case 'Tech_Telephony_Skills':
			$mapping = 1;
                        break;
                }

/*		if (isset($toInsert['CloneDate'])) {
			$date = new Zend_Date($toInsert['CloneDate'], 'MM/dd/YYYY hh:mm:ss a');
			$toInsert['CloneDate'] = $date->toString('YYYY-MM-dd HH:mm:ss');
		}*/
                
//                $date = new Zend_Date($toInsert['CalledTime'], 'MM/dd/YYYY hh:mm:ss a');
//                $toInsert['CalledTime'] = $date->toString('YYYY-MM-dd HH:mm:ss');
//				if (is_null($toInsert['ParentCall'])) $toInsert['ParentCall'] = $toInsert['id'];
			print_r($toInsert);
			if (empty($mapping)) {
				echo "Syncing $tm: "; print_r($toInsert);
				$id = $toInsert['TechID'];
				unset($toInsert['TechID']);
                try {
//					$db->update($tm, $toInsert, "TechID = '$id'");
                } catch (Zend_Exception $e) {
                    print "Error --- $tm -- $k ";
                    print_r($toInsert);
                    exit;
				}
//	                    $db->insert($tm, $toInsert);
			}
			else {
				foreach ($toInsert as $k=>$v) {
					if (array_key_exists($k, $equipmentList)) {
						$tm = 'tech_equipment';
						$mappingField = 'equipment_id';
						$mapping = $equipmentList;
					}
					else if (array_key_exists($k, $skillsList)) {
						$tm = 'tech_skill';
						$mappingField = 'skill_id';
						$mapping = $skillsList;
					}
					else {
						continue;
					}
					if ($v == 1) {
						$in = array('TechID' => $item[$idField], $mappingField => $mapping[$k]);
						echo "Inserting into $tm --- $k: "; print_r($in);
						// try insert
				                try {
							$db->insert($tm, array('TechID' => $item[$idField], $mappingField => $mapping[$k]));
				                } catch (Zend_Exception $e) {
							if ($e->getCode() == 23000)
								echo "Already exists --- $tm --- $k\n";
							else {
					                    print "Error --- $tm -- $k ";
					                    print_r($toInsert);
					                    exit;
							}
				                }
					}
					else {
//						echo "Deleting from $tm: $mappingField = $mapping[$k]\n";
//						$db->insert($tm, array('TechID' => $item[$idField], $mappingField => ));
						// try delete
					}
				}
			}

//                print 'Inserting: ' . $toInsert[$idField] . "\n";
            } else {
                print 'Skip: ' . $item[$idField] . "\n";
            }
        }
    }
    
    $startIndex = $stopIndex;
}
}
