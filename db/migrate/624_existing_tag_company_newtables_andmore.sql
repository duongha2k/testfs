
-- existing_tag_company
DROP TABLE IF EXISTS `existing_tag_company`;

CREATE TABLE `existing_tag_company` (
  `tag_name` varchar(50) DEFAULT NULL,
  `company_id` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `existing_tag_company` */

insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('TrapolloMedicalCertified','MDS');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('NCRBT','NCR');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('LMSPreferred','LMS');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('LMSPlusKey','LMS');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('FlextronicsContractor','FATT');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('FlextronicsScreened','FATT');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('ComputerPlus','CPSS');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('CSCDesksideSupportTech','CSC1');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('CSCDesksideSupportTech','CSC2');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('CSCDesksideSupportTech','CSC3');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('CSCDesksideSupportTech','CSC4');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('CSCDesksideSupportTech','CSC4FS');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('CSCDesksideSupportTech','CSCR');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('CSCDesksideSupportTech','CSCT');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('ServRightBrother','SERV');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('GGETechEvaluation','VELO');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('FlextronicsRecruit','FATT');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('TechForceICAgreement','TECF');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('CompuComTechnician','COMPU');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('CompuComAnalyst','COMPU');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('CompuComSpecialist','COMPU');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('PurpleTechnicianUnverified','PURP');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('PurpleTechnicianCertified','PURP');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('ErgoMotionKeyContractor','MDS');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('ErgoMotionKeyContractor','ERG');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('ErgoMotionCertifiedTechnician','MDS');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('ErgoMotionCertifiedTechnician','ERG');
insert  into `existing_tag_company`(`tag_name`,`company_id`) values ('EndeavorCertified','ENDV');

-- fstags
DROP TABLE IF EXISTS `fstags`;

CREATE TABLE `fstags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(255) DEFAULT NULL,
  `VisibilityId` int(11) DEFAULT NULL COMMENT '1: Selected Client(s) Only; 2: All Client Users',
  `AddedByUser` varchar(255) DEFAULT NULL,
  `Active` tinyint(4) DEFAULT NULL,
  `Name` varchar(40) NOT NULL,
  `Source` varchar(40) NOT NULL,
  `ExistingImagePath` varchar(255) DEFAULT NULL,
  `IsExistingTag` tinyint(4) DEFAULT NULL,
  `AddedDate` datetime DEFAULT NULL,
  `TagArt` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

/*Data for the table `fstags` */

insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (1,'FS-Mobile',2,'system',1,'FS_Mobile','TechBankInfo','/widgets/images/fs_mobile_36x32.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (2,'Dell MRA Compliant',2,'system',1,'Dell_MRA_Compliant','TechBankInfo','/widgets/images/Dell_MRA_Compliant.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (3,'Dell MRA Lapsed',2,'system',1,'Dell_MRA_Lapsed','TechBankInfo','/widgets/images/Dell_MRA_Lapsed.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (4,'US I-9 Verified',2,'system',1,'US_I9_Verified','tech_ext','/widgets/images/USVerified_Tag.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (5,'US I-9 Verification Pending',2,'system',1,'US_I9_Pending','tech_ext','/widgets/images/USPending_Tag.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (6,'DeVry Student',2,'system',1,'DeVry','certifications','/widgets/images/DeVry.png\r',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (7,'Vonage Plus ',2,'system',1,'VonagePlus','certifications','/widgets/images/VonagePlusTag.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (10,'Trapollo Medical Certified',1,'system',1,'TrapolloMedicalCertified','certifications','/widgets/images/check_green.gif',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (11,'NCR Badged Technician',1,'system',1,'NCRBT','certifications','/widgets/images/NCRBadgedTechTag.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (12,'LMS Preferred Technician',1,'system',1,'LMSPreferred','certifications','/widgets/images/LMS Preferred.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (13,'LMS+Key Contractor',1,'system',1,'LMSPlusKey','certifications','/widgets/images/LMS Plus.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (14,'Flextronics Contractor',1,'system',1,'FlextronicsContractor','certifications','/widgets/images/FlexContractorTag_new.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (15,'Flextronics Screened',1,'system',1,'FlextronicsScreened','certifications','/widgets/images/FlexScreenedTag_new.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (16,'Computer Plus Certification',1,'system',1,'ComputerPlus','certifications','/widgets/images/ComputerPlusTag.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (17,'CSC Deskside Support Tech',1,'system',1,'CSCDesksideSupportTech','certifications','/widgets/images/csc.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (18,'ServRight - Brother Certified',1,'system',1,'ServRightBrother','certifications','/widgets/images/SRBrotag.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (19,'GGE Techs Certified',1,'system',1,'GGETechEvaluation','certifications','/widgets/images/gge.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (20,'Flextronics Recruit',1,'system',1,'FlextronicsRecruit','certifications','/widgets/images/FlexRecruitTag_new.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (21,'TechFORCE ICA Certified',1,'system',1,'TechForceICAgreement','certifications','/widgets/images/TF_Tag.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (22,'CompuCom Technician',1,'system',1,'CompuComTechnician','certifications','/widgets/images/C1.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (23,'CompuCom Analyst',1,'system',1,'CompuComAnalyst','certifications','/widgets/images/C2.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (24,'CompuCom Specialist',1,'system',1,'CompuComSpecialist','certifications','/widgets/images/C3.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (25,'Unverified Purple Technician',1,'system',1,'PurpleTechnicianUnverified','certifications','/widgets/images/Unverifired_Purple_Technician.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (26,'Verified Purple Technician',1,'system',1,'PurpleTechnicianCertified','certifications',' /widgets/images/Certified_Purple_Technician.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (27,'Adjustable Bed Certified',1,'system',1,'ErgoMotionKeyContractor','certifications','/widgets/images/KeyContractor.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (28,'Adjustable Bed Expert',1,'system',1,'ErgoMotionCertifiedTechnician','certifications','/widgets/images/CertifiedTechnician.png',1,'2013-01-26 00:00:00',NULL);
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (29,'Endeavor Certified Technicians',1,'system',1,'EndeavorCertified','certifications','/widgets/images/EndeavorTag.png',1,'2013-01-26 00:00:00',NULL);


-- fstag_clients

DROP TABLE IF EXISTS `fstag_clients`;

CREATE TABLE `fstag_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `FSTagId` int(11) DEFAULT NULL,
  `CompanyId` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

/*Data for the table `fstag_clients` */

insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (3,10,'MDS');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (4,11,'NCR');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (5,12,'LMS');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (6,13,'LMS');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (7,14,'FATT');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (8,15,'FATT');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (9,16,'CPSS');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (10,17,'CSC1');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (11,17,'CSC2');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (12,17,'CSC3');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (13,17,'CSC4');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (14,17,'CSC4FS');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (15,17,'CSCR');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (16,17,'CSCT');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (17,18,'SERV');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (18,19,'VELO');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (19,20,'FATT');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (20,21,'TECF');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (21,22,'COMPU');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (22,23,'COMPU');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (23,24,'COMPU');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (24,25,'PURP');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (25,26,'PURP');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (26,27,'MDS');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (27,27,'ERG');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (28,28,'MDS');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (29,28,'ERG');
insert  into `fstag_clients`(`id`,`FSTagId`,`CompanyId`) values (30,29,'ENDV');


DROP TABLE IF EXISTS `fstag_techs`;

CREATE TABLE IF NOT EXISTS `fstag_techs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `FSTagId` int(11) DEFAULT NULL,
  `TechId` int(11) DEFAULT NULL,
  `Number` varchar(100) DEFAULT NULL,
  `DateExpired` date DEFAULT NULL,
  `Date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=90870 ;


-- fstags
ALTER TABLE fstags ADD COLUMN DisplayOnClientCredentials tinyint DEFAULT 1;

UPDATE fstags SET DisplayOnClientCredentials=0 WHERE `Name` IN ('FS_Mobile','DeVry','US_I9_Verified','US_I9_Pending','NACI');

-- fstag->ncrBasic
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (30,'NCR Basic Certification',1,'system',1,'ncrBasic','calculation','',1,'2013-03-12 00:00:00','');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (30,'NCR');
-- fstag->FLS_Photo_ID
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (31,'FLS Photo ID',1,'system',1,'FLS_Photo_ID','TechBankInfo','',1,'2013-03-12 00:00:00','');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (31,'FLS');
-- fstag->flsCSP
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (32,'FLS Computer Security Policy',1,'system',1,'flsCSP','TechBankInfo','',1,'2013-03-12 00:00:00','');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (32,'FLS');
-- fstag->FLSWhse
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (33,'FLS Warehouse ID',1,'system',1,'FLSWhse','TechBankInfo','',1,'2013-03-12 00:00:00','');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (33,'FLS');
-- fstag->FLSID
insert  into `fstags`(`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`ExistingImagePath`,`IsExistingTag`,`AddedDate`,`TagArt`) values (34,'FLS Contractor ID',1,'system',1,'FLSID','TechBankInfo','',1,'2013-03-12 00:00:00','');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (34,'FLS');




