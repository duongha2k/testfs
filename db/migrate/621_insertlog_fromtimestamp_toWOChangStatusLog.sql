

INSERT INTO  WOChangeStatusLog(WIN_NUM,WOStatusID,DateTime_Stamp)
SELECT WIN_NUM,1 AS WOStatusID,DateTime_Stamp
FROM `timestamps`
WHERE Description = 'Work Order Published';

INSERT INTO  WOChangeStatusLog(WIN_NUM,WOStatusID,DateTime_Stamp)
SELECT WIN_NUM,2 AS WOStatusID,DateTime_Stamp
FROM `timestamps`
WHERE Description = 'Work Order Assigned';

INSERT INTO  WOChangeStatusLog(WIN_NUM,WOStatusID,DateTime_Stamp)
SELECT WIN_NUM,3 AS WOStatusID,DateTime_Stamp
FROM `timestamps`
WHERE Description = 'Work Order Marked: Completed';

INSERT INTO  WOChangeStatusLog(WIN_NUM,WOStatusID,DateTime_Stamp)
SELECT WIN_NUM,4 AS WOStatusID,DateTime_Stamp
FROM `timestamps`
WHERE Description = 'Work Order Approved';
