
ALTER TABLE TechBankInfo ADD COLUMN ElectricianCertifiedSelfRating int;
ALTER TABLE TechBankInfo ADD COLUMN ElectricianCertifiedYN tinyint;

INSERT INTO wo_categories (Category_ID,Category,Abbr,TechSelfRatingColumn)
VALUES (39,'Electrician - Licensed/Certified','ElectricianCertified', 'ElectricianCertifiedSelfRating');

INSERT INTO fsexperts(Id,`Code`,Title,Content,FSExpertEnable,Abbreviation)
VALUES (50,'ElectricianCertified','Electrician - Licensed/Certified','',0,'ElectricianCertified');

