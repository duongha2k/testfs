ALTER TABLE `work_orders` ADD INDEX ( `Invoiced` );
ALTER TABLE `work_orders` ADD INDEX ( `Deactivated` );
ALTER TABLE `work_orders` ADD INDEX ( `WorkOrderReviewed` );
ALTER TABLE `work_orders` ADD INDEX ( `TechMarkedComplete` );
