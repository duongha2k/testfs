INSERT INTO `states_list` (`State`, `Abbreviation`, `Country`) VALUES ('Aguascalientes', 'AG', 'MX'),
('Baja California', 'BJ', 'MX'),
('Baja California Sur', 'BS', 'MX'),
('Campeche', 'CP', 'MX'),
('Chiapas', 'CH', 'MX'),
('Chihuahua', 'CI', 'MX'),
('Coahuila', 'CU', 'MX'),
('Colima', 'CL', 'MX'),
('Distrito Federal', 'DF', 'MX'),
('Durango', 'DG', 'MX'),
('Guanajuato', 'GJ', 'MX'),
('Guerrero', 'GR', 'MX'),
('Hidalgo', 'HG', 'MX'),
('Jalisco', 'JA', 'MX'),
('Mexico', 'EM', 'MX'),
('Michoacan', 'MH', 'MX'),
('Morelos', 'MR', 'MX'),
('Nayarit', 'NA', 'MX'),
('Nuevo Leon', 'NL', 'MX'),
('Oaxaca', 'OA', 'MX'),
('Puebla', 'PU', 'MX'),
('Queretaro', 'QA', 'MX'),
('Quintana Roo', 'QR', 'MX'),
('San Luis Potosi', 'SL', 'MX'),
('Sinaloa', 'SI', 'MX'),
('Sonora', 'SO', 'MX'),
('Tabasco', 'TA', 'MX'),
('Tamaulipas', 'TM', 'MX'),
('Tlaxcala', 'TL', 'MX'),
('Veracruz', 'VZ', 'MX'),
('Yucatan', 'YC', 'MX'),
('Zacatecas', 'ZT', 'MX');
