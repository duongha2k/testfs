
ALTER TABLE fstags ADD COLUMN CanNonGPMEdit tinyint(1) DEFAULT 0;
ALTER TABLE fstags ADD COLUMN CanGPMEdit tinyint(1) DEFAULT 1;
ALTER TABLE fstags ADD COLUMN CanTechEdit tinyint(1) DEFAULT 0;

UPDATE fstags SET CanNonGPMEdit=1 WHERE VisibilityId=1;

ALTER TABLE fstags MODIFY COLUMN HideFromTechs int;



