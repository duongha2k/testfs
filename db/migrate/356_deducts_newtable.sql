/*
SQLyog Enterprise - MySQL GUI v7.14 
MySQL - 5.1.33-community : Database - saturn
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `deducts` */

DROP TABLE IF EXISTS `deducts`;

CREATE TABLE `deducts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `DeductPcnt` float DEFAULT NULL,
  `DeductPcntDescription` varchar(50) DEFAULT NULL,
  `Active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `deducts` */

insert  into `deducts`(`id`,`DeductPcnt`,`DeductPcntDescription`,`Active`) values (1,0,'No Tech Deduction',1);
insert  into `deducts`(`id`,`DeductPcnt`,`DeductPcntDescription`,`Active`) values (2,0.05,'5% Tech Deduction',1);
insert  into `deducts`(`id`,`DeductPcnt`,`DeductPcntDescription`,`Active`) values (3,0.1,'10% Tech Deduction',1);
insert  into `deducts`(`id`,`DeductPcnt`,`DeductPcntDescription`,`Active`) values (4,1,'100% Tech Deduction',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
