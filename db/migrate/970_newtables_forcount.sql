
DROP TABLE IF EXISTS `tech_perf_count`;

CREATE TABLE `tech_perf_count` (
  `Tech_ID` int(11) NOT NULL,
  `Performance_Total` int(11) NOT NULL DEFAULT '0',
  `Performance_Good` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Tech_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;


DROP TABLE IF EXISTS `tech_preference_count`;

CREATE TABLE `tech_preference_count` (
  `Tech_ID` int(11) NOT NULL,
  `Preference_Total` int(11) NOT NULL DEFAULT '0',
  `Preference_Good` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Tech_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;


-- tech_perf_total - insert
INSERT INTO tech_perf_count(Tech_ID,Performance_Total)
(
 SELECT work_orders.Tech_ID,Count(*) AS `Performance_Total`
 FROM work_orders
 WHERE (SATPerformance > 0) AND (Approved = 1) 
 AND (work_orders.Tech_ID > 0) AND (work_orders.Tech_ID < 20000)
 GROUP BY work_orders.Tech_ID
 ORDER BY work_orders.Tech_ID
);

INSERT INTO tech_perf_count(Tech_ID,Performance_Total)
(
 SELECT work_orders.Tech_ID,Count(*) AS `Performance_Total`
 FROM work_orders
 WHERE (SATPerformance > 0) AND (Approved = 1) 
 AND (work_orders.Tech_ID >= 20000) AND (work_orders.Tech_ID < 30000)
 GROUP BY work_orders.Tech_ID
 ORDER BY work_orders.Tech_ID
);

INSERT INTO tech_perf_count(Tech_ID,Performance_Total)
(
 SELECT work_orders.Tech_ID,Count(*) AS `Performance_Total`
 FROM work_orders
 WHERE (SATPerformance > 0) AND (Approved = 1) 
 AND (work_orders.Tech_ID >= 30000) AND (work_orders.Tech_ID < 40000)
 GROUP BY work_orders.Tech_ID
 ORDER BY work_orders.Tech_ID
);

INSERT INTO tech_perf_count(Tech_ID,Performance_Total)
(
 SELECT work_orders.Tech_ID,Count(*) AS `Performance_Total`
 FROM work_orders
 WHERE (SATPerformance > 0) AND (Approved = 1) 
 AND (work_orders.Tech_ID >= 30000) AND (work_orders.Tech_ID < 40000)
 GROUP BY work_orders.Tech_ID
 ORDER BY work_orders.Tech_ID
);

INSERT INTO tech_perf_count(Tech_ID,Performance_Total)
(
 SELECT work_orders.Tech_ID,Count(*) AS `Performance_Total`
 FROM work_orders
 WHERE (SATPerformance > 0) AND (Approved = 1) 
 AND (work_orders.Tech_ID >= 40000) AND (work_orders.Tech_ID < 50000)
 GROUP BY work_orders.Tech_ID
 ORDER BY work_orders.Tech_ID
);

INSERT INTO tech_perf_count(Tech_ID,Performance_Total)
(
 SELECT work_orders.Tech_ID,Count(*) AS `Performance_Total`
 FROM work_orders
 WHERE (SATPerformance > 0) AND (Approved = 1) 
 AND (work_orders.Tech_ID >= 50000) AND (work_orders.Tech_ID < 60000)
 GROUP BY work_orders.Tech_ID
 ORDER BY work_orders.Tech_ID
);

INSERT INTO tech_perf_count(Tech_ID,Performance_Total)
(
 SELECT work_orders.Tech_ID,Count(*) AS `Performance_Total`
 FROM work_orders
 WHERE (SATPerformance > 0) AND (Approved = 1) 
 AND (work_orders.Tech_ID >= 60000) 
 GROUP BY work_orders.Tech_ID
 ORDER BY work_orders.Tech_ID
);

-- tech_perf_total - update
UPDATE tech_perf_count SET Performance_Good = (SELECT Count(*) AS `Performance_Good`
			FROM work_orders
			WHERE (SATPerformance >=2) AND (Approved = 1) AND (Tech_ID = tech_perf_count.Tech_ID) 
			GROUP BY Tech_ID)
WHERE tech_perf_count.Tech_ID < 10000;
UPDATE tech_perf_count SET Performance_Good = (SELECT Count(*) AS `Performance_Good`
			FROM work_orders
			WHERE (SATPerformance >=2) AND (Approved = 1) AND (Tech_ID = tech_perf_count.Tech_ID) 
			GROUP BY Tech_ID)
WHERE tech_perf_count.Tech_ID >= 10000 tech_perf_count.Tech_ID < 20000;
UPDATE tech_perf_count SET Performance_Good = (SELECT Count(*) AS `Performance_Good`
			FROM work_orders
			WHERE (SATPerformance >=2) AND (Approved = 1) AND (Tech_ID = tech_perf_count.Tech_ID) 
			GROUP BY Tech_ID)
WHERE tech_perf_count.Tech_ID >= 20000 tech_perf_count.Tech_ID < 30000;
UPDATE tech_perf_count SET Performance_Good = (SELECT Count(*) AS `Performance_Good`
			FROM work_orders
			WHERE (SATPerformance >=2) AND (Approved = 1) AND (Tech_ID = tech_perf_count.Tech_ID) 
			GROUP BY Tech_ID)
WHERE tech_perf_count.Tech_ID >= 30000 tech_perf_count.Tech_ID < 40000;
UPDATE tech_perf_count SET Performance_Good = (SELECT Count(*) AS `Performance_Good`
			FROM work_orders
			WHERE (SATPerformance >=2) AND (Approved = 1) AND (Tech_ID = tech_perf_count.Tech_ID) 
			GROUP BY Tech_ID)
WHERE tech_perf_count.Tech_ID >= 40000 tech_perf_count.Tech_ID < 50000;
UPDATE tech_perf_count SET Performance_Good = (SELECT Count(*) AS `Performance_Good`
			FROM work_orders
			WHERE (SATPerformance >=2) AND (Approved = 1) AND (Tech_ID = tech_perf_count.Tech_ID) 
			GROUP BY Tech_ID)
WHERE tech_perf_count.Tech_ID >= 50000;

-- tech_perf_total - insert: 
INSERT INTO tech_preference_count(Tech_ID,Preference_Total)
(
  SELECT work_orders.Tech_ID,Count(*) AS `Preference_Total`
  FROM work_orders
  WHERE (SATRecommended > 0) AND (Approved = 1) 
  AND (work_orders.Tech_ID > 0) AND (work_orders.Tech_ID < 20000) 
  GROUP BY work_orders.Tech_ID
  ORDER BY work_orders.Tech_ID
);

INSERT INTO tech_preference_count(Tech_ID,Preference_Total)
(
  SELECT work_orders.Tech_ID,Count(*) AS `Preference_Total`
  FROM work_orders
  WHERE (SATRecommended > 0) AND (Approved = 1) 
  AND (work_orders.Tech_ID >=20000) AND (work_orders.Tech_ID < 30000) 
  GROUP BY work_orders.Tech_ID
  ORDER BY work_orders.Tech_ID
);

INSERT INTO tech_preference_count(Tech_ID,Preference_Total)
(
  SELECT work_orders.Tech_ID,Count(*) AS `Preference_Total`
  FROM work_orders
  WHERE (SATRecommended > 0) AND (Approved = 1) 
  AND (work_orders.Tech_ID >=30000) AND (work_orders.Tech_ID < 40000) 
  GROUP BY work_orders.Tech_ID
  ORDER BY work_orders.Tech_ID
);

INSERT INTO tech_preference_count(Tech_ID,Preference_Total)
(
  SELECT work_orders.Tech_ID,Count(*) AS `Preference_Total`
  FROM work_orders
  WHERE (SATRecommended > 0) AND (Approved = 1) 
  AND (work_orders.Tech_ID >=40000) AND (work_orders.Tech_ID < 50000) 
  GROUP BY work_orders.Tech_ID
  ORDER BY work_orders.Tech_ID
);

INSERT INTO tech_preference_count(Tech_ID,Preference_Total)
(
  SELECT work_orders.Tech_ID,Count(*) AS `Preference_Total`
  FROM work_orders
  WHERE (SATRecommended > 0) AND (Approved = 1) 
  AND (work_orders.Tech_ID >=50000) AND (work_orders.Tech_ID < 60000) 
  GROUP BY work_orders.Tech_ID
  ORDER BY work_orders.Tech_ID
);

INSERT INTO tech_preference_count(Tech_ID,Preference_Total)
(
  SELECT work_orders.Tech_ID,Count(*) AS `Preference_Total`
  FROM work_orders
  WHERE (SATRecommended > 0) AND (Approved = 1) 
  AND (work_orders.Tech_ID >=60000)
  GROUP BY work_orders.Tech_ID
  ORDER BY work_orders.Tech_ID
);

-- tech_perf_total - update: 
UPDATE tech_preference_count SET Preference_Good = (SELECT Count(*) AS `Preference_Good`
			FROM work_orders
			WHERE (SATRecommended >=3) AND (Approved = 1) AND (Tech_ID = tech_preference_count.Tech_ID) 
			GROUP BY Tech_ID)
WHERE tech_preference_count.Tech_ID > 0 AND tech_preference_count.Tech_ID < 20000;

UPDATE tech_preference_count SET Preference_Good = (SELECT Count(*) AS `Preference_Good`
			FROM work_orders
			WHERE (SATRecommended >=3) AND (Approved = 1) AND (Tech_ID = tech_preference_count.Tech_ID) 
			GROUP BY Tech_ID)
WHERE tech_preference_count.Tech_ID >= 20000 AND tech_preference_count.Tech_ID < 30000;

UPDATE tech_preference_count SET Preference_Good = (SELECT Count(*) AS `Preference_Good`
			FROM work_orders
			WHERE (SATRecommended >=3) AND (Approved = 1) AND (Tech_ID = tech_preference_count.Tech_ID) 
			GROUP BY Tech_ID)
WHERE tech_preference_count.Tech_ID >= 30000 AND tech_preference_count.Tech_ID < 40000;

UPDATE tech_preference_count SET Preference_Good = (SELECT Count(*) AS `Preference_Good`
			FROM work_orders
			WHERE (SATRecommended >=3) AND (Approved = 1) AND (Tech_ID = tech_preference_count.Tech_ID) 
			GROUP BY Tech_ID)
WHERE tech_preference_count.Tech_ID >= 40000 AND tech_preference_count.Tech_ID < 50000;


UPDATE tech_preference_count SET Preference_Good = (SELECT Count(*) AS `Preference_Good`
			FROM work_orders
			WHERE (SATRecommended >=3) AND (Approved = 1) AND (Tech_ID = tech_preference_count.Tech_ID) 
			GROUP BY Tech_ID)
WHERE tech_preference_count.Tech_ID >= 50000 AND tech_preference_count.Tech_ID < 60000;

UPDATE tech_preference_count SET Preference_Good = (SELECT Count(*) AS `Preference_Good`
			FROM work_orders
			WHERE (SATRecommended >=3) AND (Approved = 1) AND (Tech_ID = tech_preference_count.Tech_ID) 
			GROUP BY Tech_ID)
WHERE tech_preference_count.Tech_ID >= 60000







