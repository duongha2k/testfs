
ALTER TABLE TechBankInfo ADD COLUMN HideBids tinyint DEFAULT 0;
ALTER TABLE TechBankInfo ADD COLUMN HideBidsDate datetime;
ALTER TABLE TechBankInfo ADD COLUMN HideBidsByUserName varchar(255);

CREATE INDEX HideBids ON TechBankInfo (HideBids);

