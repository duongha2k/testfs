
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- Table structure for table `operations_staff`
DROP TABLE IF EXISTS `operations_staff`;
CREATE TABLE IF NOT EXISTS `operations_staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupid` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- Dumping data for table `operations_staff`
INSERT INTO `operations_staff` (`id`, `groupid`, `name`, `email`, `phone`) VALUES
(1,1,'Aaron Acker','aacker@fieldsolutions.com','952-955-9033'),
(2,1,'David Baggett','dbaggett@fieldsolutions.com','512-789-5333'),
(3,1,'Stephanie Bledsoe','sbledsoe@fieldsolutions.com','402-817-4833'),
(4,1,'Shad Follmer','sfollmer@fieldsolutions.com','952-288-2507'),
(5,1,'Cary Goldman','cgoldman@fieldsolutions.com	','224-513-5080'),
(6,1,'Kevin Joyce','kjoyce@fieldsolutions.com','843-476-5311'),
(7,1,'Mike Kraemer','mkraemer@fieldsolutions.com','320-230-2090'),
(8,1,'Marty Reader','mreader@fieldsolutions.com','952-288-2506'),
(9,2,'Aaron Acker','aacker@fieldsolutions.com','952-955-9033'),
(10,2,'Stephanie Bledsoe','sbledsoe@fieldsolutions.com','402-817-4833'),
(11,2,'Dave Valento','dvalento@fieldsolutions.com','512-506-8962');


-- insert if not exists
insert into clients_ext(ClientId,FSClientServiceDirectorId,FSAccountManagerId)
select clients.ClientID,0,0
From clients 
LEFT JOIN clients_ext ON clients_ext.ClientId = clients.ClientID
WHERE clients_ext.ClientId IS NULL
and clients.CompanyName in ('Alta InfoServ','BG Global Networks','Computer Integrated Services LLC','Ecolab','Perot','Pyramid Technology Services','Replay Systems','RL Williams Company','Technology Services Group - Louisiana','Xerox GIS','Appollo Systems','BBVS-GC','Belmont Technologies','Black Box Customer Services Solutions Team','Black Box Global Network Services',
'Black Box Logos Communications','Black Box Voice Services South','Cardinal Pointe Communications','Catalina Marketing','COTG Xerox','DELL Inc Self Service','DELL Inc.','Flying Eagles','HMS Technologies, Inc','IT Express','Laser Technologies','Manna Distribution Services','Manna Distribution Services-FS','PharmaSafe LLC','Response Link','SabeRex Group LTD','Virteva',
'Wireless Ronin Technologies','Xerox - NARS','Xerox Corporation','Zip Express Installation','4Liberty Inc','AmeriPride Services Inc','Apex Computer Systems Inc','Certified Parts','DeployNet Inc','Diebold','Digital Controls Corporation-FS','Fujitsu Inside Wiring','i3gcorp','iPort Media','iTech Solutions','NCR','NCR-FS','NCR-FS to SS','NEC Display Solutions','Relational Technology Solutions',
'Richard Fleischman and Associates','Vision Technologies Inc.','Visual Sound, Inc.','Wincor','Access POS, LLC','ACD Reps','Acecom Inc-SA','Advanced Service Technologies','Agilysys','Akibia','Applied Business Communications','Atlantic Webs, Inc.','Automated Security Alert','B2B Computers','Bailiwick','Bluewater','CineMassive Displays','Cokeva Digital Technology','Computer1 Products','ComputerPlus Sales & Service, Inc.','CSC-1AZ','CSC-2IL','CSC-4NC1',
'CSC-5NC2','CSR Technology Group','Dart Telecom','DataCap Direct','Datamax Services','Dataprise','Digital Tech','Direct POS','Dolomite Technology','DTS Varietech','DTT Surveillance','Endeavor','Equus Computer Systems, Inc.','First Line Support','Flextronics','Fujitsu Transactions Solutions','Gant Solutions LLC','Global Techs International Inc','Graybow','Halifax Corporation','IkkanTek Computer Services','Infinity Technology Solutions','InTegriLogic Corp','IT decision Management',
'IT Definitive','i-team','ITSCI','John Ryan','Manna Distribution Services-SS','Maratona Communications Inc.','Master Technology Group','Merchandising Technologies Inc','Met-Tech Corp','Mustang Micro','National Network Solutions','Nationwide SCS','NCR-SS','Network Access Products','New Millennium Technologies, LLC','Onsite Technology Services','Outreach Technology','People To Go','Peoples Computer Services, Inc.','PosSystem.ca','R&R Solutions Inc','RedDotNet','Reliable IT',
'Retail Echelon Installation Group','Retail Echelon Installation Group 2','Rhombus Services','Royal Communications','RTGPOS','Sarcom','Service USA','ServiceKey LLC','ServRight','SIS Consulting','Storeworks Technologies','Suisse Associates','Systems Integration LLC','TDX Tech','Tech Force','Technology Services Worldwide','Tekcetera Inc','Terix','Tolt Service Group','Trans Alarm','TurnKey Kiosks','Velociti Inc','Velocity','Verdigrid','VS Networks','WAND Corporation','Worldlink','Zap Consulting',
'Ziyad','Zotek Enterprises');


-- assign CSD
update clients_ext set FSClientServiceDirectorId=1 where ClientId in (select ClientID From clients where 
CompanyName in ('SabeRex Group LTD'));

update clients_ext set FSClientServiceDirectorId=5 where ClientId in (select ClientID From clients where 
CompanyName in ('Flying Eagles','Laser Technologies','iPort Media','iTech Solutions','ACD Reps','Acecom Inc-SA',
'CineMassive Displays','Cokeva Digital Technology','Computer1 Products','Direct POS','Dolomite Technology','Gant Solutions LLC',
'IkkanTek Computer Services','Infinity Technology Solutions','IT decision Management','Met-Tech Corp','National Network Solutions',
'Nationwide SCS','New Millennium Technologies, LLC','Outreach Technology','Peoples Computer Services, Inc.','PosSystem.ca','Sarcom',
'Suisse Associates','Systems Integration LLC','Technology Services Worldwide','Verdigrid','VS Networks',
'Zap Consulting','Ziyad'));

update clients_ext set FSClientServiceDirectorId=2 where ClientId in (select ClientID From clients where 
CompanyName in ('Replay Systems','Catalina Marketing','DELL Inc Self Service','DELL Inc.','HMS Technologies, Inc',
'Apex Computer Systems Inc','B2B Computers','ServRight','Tolt Service Group'));

update clients_ext set FSClientServiceDirectorId=6 where ClientId in (select ClientID From clients where 
CompanyName in ('PharmaSafe LLC','Xerox - NARS','Xerox Corporation'));

update clients_ext set FSClientServiceDirectorId=8 where ClientId in (select ClientID From clients where 
CompanyName in ('Alta InfoServ','BG Global Networks','Computer Integrated Services LLC','Ecolab','Pyramid Technology Services','RL Williams Company','Xerox GIS',
'Cardinal Pointe Communications','COTG Xerox','Response Link','i3gcorp','NEC Display Solutions','Advanced Service Technologies','Agilysys','Akibia',
'Atlantic Webs, Inc.','ComputerPlus Sales & Service, Inc.','CSR Technology Group','Dart Telecom','Dataprise','Digital Tech','Endeavor','Flextronics',
'Global Techs International Inc','Halifax Corporation','InTegriLogic Corp','IT Definitive','i-team','Mustang Micro','Onsite Technology Services',
'R&R Solutions Inc','RedDotNet','Reliable IT','Retail Echelon Installation Group','Retail Echelon Installation Group 2','Royal Communications',
'ServiceKey LLC','SIS Consulting','Tech Force','Terix','Velocity'));

update clients_ext set FSClientServiceDirectorId=7 where ClientId in (select ClientID From clients where 
CompanyName in ('Perot','IT Express','Fujitsu Inside Wiring','NCR','NCR-FS','NCR-FS to SS','Richard Fleischman and Associates','Wincor','Access POS, LLC','DTS Varietech',
'First Line Support','Fujitsu Transactions Solutions','NCR-SS','Service USA','Worldlink'));

update clients_ext set FSClientServiceDirectorId=4 where ClientId in (select ClientID From clients where 
CompanyName in ('Technology Services Group - Louisiana','Appollo Systems','BBVS-GC','Belmont Technologies','Black Box Customer Services Solutions Team','Black Box Global Network Services','Black Box Logos Communications','Black Box Voice Services South',
'Manna Distribution Services','Manna Distribution Services-FS','Virteva','Wireless Ronin Technologies','Zip Express Installation','4Liberty Inc','Certified Parts','DeployNet Inc','Diebold','Digital Controls Corporation-FS','Relational Technology Solutions',
'Vision Technologies Inc.','Visual Sound, Inc.','Applied Business Communications','Automated Security Alert','Bailiwick','Bluewater','CSC-1AZ','CSC-2IL','CSC-4NC1','CSC-5NC2','DataCap Direct','Datamax Services','DTT Surveillance','Equus Computer Systems, Inc.',
'Graybow','John Ryan','Manna Distribution Services-SS','Master Technology Group','Merchandising Technologies Inc','Network Access Products','People To Go','Rhombus Services','Storeworks Technologies','TDX Tech','Tekcetera Inc','Trans Alarm','TurnKey Kiosks',
'Velociti Inc','WAND Corporation'));

update clients_ext set FSClientServiceDirectorId=3 where ClientId in (select ClientID From clients where 
CompanyName in ('RTGPOS'));

-- assign AM
update clients_ext set FSAccountManagerId=9 where ClientId in (select ClientID From clients where 
CompanyName in ('Xerox GIS','Appollo Systems','BBVS-GC','Belmont Technologies','Black Box Customer Services Solutions Team','Black Box Global Network Services','Black Box Logos Communications','Black Box Voice Services South','Cardinal Pointe Communications','Catalina Marketing','COTG Xerox',
'DELL Inc Self Service','DELL Inc.','Flying Eagles','HMS Technologies, Inc','IT Express','Laser Technologies','Manna Distribution Services','Manna Distribution Services-FS','PharmaSafe LLC','Response Link','SabeRex Group LTD','Virteva','Wireless Ronin Technologies','Xerox - NARS',
'Xerox Corporation','Zip Express Installation'));

update clients_ext set FSAccountManagerId=11 where ClientId in (select ClientID From clients where 
CompanyName in ('4Liberty Inc','AmeriPride Services Inc','Apex Computer Systems Inc','Certified Parts','DeployNet Inc','Diebold','Digital Controls Corporation-FS','Fujitsu Inside Wiring','i3gcorp','iPort Media','iTech Solutions','NCR','NCR-FS','NCR-FS to SS','NEC Display Solutions','Relational Technology Solutions',
'Richard Fleischman and Associates','Vision Technologies Inc.','Visual Sound, Inc.','Wincor'));

update clients_ext set FSAccountManagerId=10 where ClientId in (select ClientID From clients where 
CompanyName in ('Access POS, LLC','ACD Reps','Acecom Inc-SA','Advanced Service Technologies','Agilysys','Akibia','Applied Business Communications','Atlantic Webs, Inc.','Automated Security Alert','B2B Computers','Bailiwick','Bluewater','CineMassive Displays','Cokeva Digital Technology','Computer1 Products','ComputerPlus Sales & Service, Inc.',
'CSC-1AZ','CSC-2IL','CSC-4NC1','CSC-5NC2','CSR Technology Group','Dart Telecom','DataCap Direct','Datamax Services','Dataprise','Digital Tech','Direct POS','Dolomite Technology','DTS Varietech','DTT Surveillance','Endeavor','Equus Computer Systems, Inc.','First Line Support','Flextronics','Fujitsu Transactions Solutions','Gant Solutions LLC',
'Global Techs International Inc','Graybow','Halifax Corporation','IkkanTek Computer Services','Infinity Technology Solutions','InTegriLogic Corp','IT decision Management','IT Definitive','i-team','ITSCI','John Ryan','Manna Distribution Services-SS','Maratona Communications Inc.','Master Technology Group','Merchandising Technologies Inc',
'Met-Tech Corp','Mustang Micro','National Network Solutions','Nationwide SCS','NCR-SS','Network Access Products','New Millennium Technologies, LLC','Onsite Technology Services','Outreach Technology','People To Go','Peoples Computer Services, Inc.','PosSystem.ca','R&R Solutions Inc','RedDotNet','Reliable IT','Retail Echelon Installation Group',
'Retail Echelon Installation Group 2','Rhombus Services','Royal Communications','RTGPOS','Sarcom','Service USA','ServiceKey LLC','ServRight','SIS Consulting','Storeworks Technologies','Suisse Associates','Systems Integration LLC','TDX Tech','Tech Force','Technology Services Worldwide','Tekcetera Inc','Terix','Tolt Service Group','Trans Alarm','TurnKey Kiosks','Velociti Inc','Velocity','Verdigrid',
'VS Networks','WAND Corporation','Worldlink','Zap Consulting','Ziyad','Zotek Enterprises'));










