
-- new column
ALTER TABLE TechBankInfo ADD COLUMN NumComments int DEFAULT 0;

-- data for NumComments
UPDATE TechBankInfo SET NumComments = (SELECT Count(*) AS NumComments FROM `comments`
WHERE (`comments`.clientID IS NOT NULL) AND (`comments`.TechID = TechBankInfo.TechID))

