<?php
require_once dirname(__FILE__).'/../../includes/modules/common.init.php';

$fieldList = array("id", "DateChanged", "Project_ID", "Company_ID", "Project_Name", "UserName", "ReminderAll", "ReminderAcceptance", "Reminder24Hr", "Reminder1Hr", "CheckInCall", "CheckOutCall", "ReminderNotMarkComplete", "ReminderIncomplete", "SMSBlast", "ApplyTo", "PcntDeduct");
$fieldListMap = array_combine($fieldList, $fieldList);

$maxId = 9000;
$step = 10000;

$startIndex = 0;
$db = Core_Database::getInstance();

while($startIndex < $maxId) {
    
    $stopIndex =  $startIndex + $step;
    
    $ascLogs = Core_Caspio::caspioSelectWithFieldListMap("ProjectACSChangeLog", $fieldListMap, 'id >= ' .(int)$startIndex . ' AND id < ' . (int)$stopIndex, "");
        
    if (is_array($ascLogs) ) {
        foreach ($ascLogs as $reminder) {
            
            $select = $db->select();
            $select->from("ProjectACSChangeLog", $fieldList)
                   ->where('id = ?', (int)$reminder['id']);
        
            $result = $db->fetchRow($select);     
            
            if (empty($result)) {
                $toInsert = $reminder;
                foreach ($toInsert as &$value) {
                    if ($value === 'True') {
                        $value = 1;
                    } elseif ($value === 'False') {
                        $value = 0;
                    }
                }
                
                $date = new Zend_Date($toInsert['DateChanged'], 'MM/dd/YYYY hh:mm:ss a');
                $toInsert['DateChanged'] = $date->toString('YYYY-MM-dd HH:mm:ss');
                if (is_null($toInsert['UserName'])) {
                    $toInsert['UserName'] = '';
                }
                try {
                    $db->insert('ProjectACSChangeLog', $toInsert);
                } catch (Zend_Exception $e) {
                    print "Error";
                    print_r($toInsert);
                    exit;
                }
                print 'Inserting: ' . $toInsert['id'] . "\n";
            } else {
                print 'Skip: ' . $reminder['id'] . "\n";
            }
        }
    }
    
    $startIndex = $stopIndex;
}
