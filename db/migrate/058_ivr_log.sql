CREATE TABLE IF NOT EXISTS `ivr_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `WOUNID` int(10) NOT NULL,
  `TechID` int(10) NOT NULL,
  `CallType` enum('Check In / Out','1-HR Reminder','Incomplete Reminder','Emergency Dispatch Reminder - Back out','Acceptance Reminder','24-HR Reminder','Not Marked Complete Reminder') NOT NULL,
  `CalledTime` datetime NOT NULL,
  `Result` varchar(255) NOT NULL,
  `CallLengthSeconds` smallint(4) NOT NULL,
  `Old` tinyint(1) NOT NULL,
  `ParentCall` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `WOUNID` (`WOUNID`),
  KEY `CallType` (`CallType`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='port of IVR_Log from Caspio' AUTO_INCREMENT=1 ;
