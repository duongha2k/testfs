DELIMITER $$

DROP TRIGGER /*!50032 IF EXISTS */ `comments_after_insert`$$

CREATE
    TRIGGER `comments_after_insert` AFTER INSERT
    ON `comments`
    FOR EACH ROW BEGIN
	IF NEW.TechID > 0 THEN
		UPDATE TechBankInfo SET NumComments = (SELECT Count(*) AS NumComments FROM `comments`
			WHERE (`comments`.clientID IS NOT NULL) AND (`comments`.TechID = TechBankInfo.TechID))
		WHERE TechBankInfo.TechID = NEW.TechID;
	END IF;
    END$$

DELIMITER ;
