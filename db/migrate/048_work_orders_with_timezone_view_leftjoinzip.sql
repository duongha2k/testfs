DROP VIEW IF EXISTS work_orders_with_timezone_view_leftjoinzip;

CREATE VIEW work_orders_with_timezone_view_leftjoinzip AS
SELECT
	wo.WIN_NUM,
	wo.WO_ID,
	wo.DateEntered,
	wo.StartDate,
	wo.EndDate,
	wo.Description,
	wo.Requirements,
	wo.Tools,
	wo.PayMax,
	wo.Address,
	wo.City,
	wo.State,
	wo.Zipcode,
	wo.Approved,
	wo.SpecialInstructions,
	wo.SiteName,
	wo.Username,
	wo.ShowTechs,
	wo.Tech_ID,
	wo.TechMarkedComplete,
	wo.MissingComments,
	wo.StartTime,
	wo.SitePhone,
	wo.SiteEmail,
	wo.SiteFax,
	wo.Invoiced,
	wo.WorkOrderReviewed,
	wo.Deactivated,
	wo.TechCheckedIn_24hrs,
	wo.NoShow_Tech,
	wo.Project_Name,
	wo.Company_Name,
	wo.Company_ID,
	wo.Time_In,
	wo.Time_Out,
	wo.Date_Assigned,
	wo.Update_Requested,
	wo.Update_Reason,
	wo.BackOut_Tech,
	wo.EndTime,
	wo.Amount_Per,
	wo.Tech_Bid_Amount,
	wo.Headline,
	wo.ReminderAcceptance,
	wo.Reminder24Hr,
	wo.Reminder1Hr,
	wo.CheckInCall,
	wo.ReminderNotMarkComplete,
	wo.reminderIncomplete,
	wo.DateIncomplete,
	wo.ProjectManagerName,
	wo.ProjectManagerPhone,
	wo.ProjectManagerEmail,
	wo.ResourceCoordinatorName,
	wo.ResourceCoordinatorPhone,
	wo.ResourceCoordinatorEmail,
	wo.EmergencyName,
	wo.EmergencyPhone,
	wo.EmergencyEmail,
	wo.TechnicalSupportName,
	wo.TechnicalSupportPhone,
	wo.TechnicalSupportEmail,
	wo.CheckInOutName,
	wo.CheckInOutNumber,
	wo.CheckInOutEmail,
	wo.ReminderAll,
	wo.CheckOutCall,
	wo.Status,
	t.FirstName,
	t.LastName,
	t.PrimaryPhone,
	t.SecondaryPhone,
	t.PrimPhoneType,
	t.SecondPhoneType,
	t.PrimaryPhoneExt,
	t.SecondaryPhoneExt,
	z.DST,
	z.StandardOffset
FROM work_orders AS wo
INNER JOIN TechBankInfo AS t ON t.id = (SELECT id FROM TechBankInfo WHERE CAST(Tech_ID AS char) = TechID LIMIT 1)
LEFT JOIN zipcodes AS z ON wo.Zipcode = z.ZIPCode
