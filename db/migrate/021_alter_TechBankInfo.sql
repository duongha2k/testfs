ALTER TABLE `TechBankInfo` ADD `PrimaryEmail` VARCHAR( 70 ) CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `TechBankInfo` ADD `No_Shows` INT NOT NULL DEFAULT '0';
ALTER TABLE `TechBankInfo` ADD `Back_Outs` INT NOT NULL DEFAULT '0';
ALTER TABLE `TechBankInfo` ADD `Address1` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `TechBankInfo` ADD `Address2` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `TechBankInfo` ADD `State` VARCHAR( 10 ) CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `TechBankInfo` ADD `City` VARCHAR( 40 ) CHARACTER SET utf8 COLLATE utf8_general_ci;
ALTER TABLE `TechBankInfo` ADD `ZipCode` VARCHAR( 15 ) CHARACTER SET utf8 COLLATE utf8_general_ci;