/*
SQLyog Enterprise - MySQL GUI v7.14 
MySQL - 5.1.33-community : Database - saturntest
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

USE `saturntest1`;

/*Table structure for table `cliaccess_atribs` */
DROP TABLE IF EXISTS `cliaccess_atribs`;

CREATE TABLE IF NOT EXISTS `cliaccess_atribs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL,
  `label` varchar(70) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `presentation_order` int(11) DEFAULT NULL,
  `levelid` int(11) DEFAULT NULL,
  `childs` int(11) DEFAULT NULL,
  `display` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=47 ;

--
-- Dumping data for table `cliaccess_atribs`
--
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(1, 'ControlPanel', 'Control Panel', 0, 40, 0, 2, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(2, 'CreatePublishWorkOrders', 'Create/Publish Work Orders', 0, 0, 0, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(3, 'AssignWorkOrders', 'Assign Work Orders', 0, 10, 0, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(4, 'ControlPanel_MyRecentWork', 'My Recent Work', 1, 70, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(5, 'ControlPanel_MyRecentTechs', 'My Recent Techs', 1, 80, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(6, 'FindTechs', 'Find Techs', 0, 90, 0, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(7, 'WorkOrders', 'Work Orders', 0, 100, 0, 10, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(8, 'ApproveWorkOrders', 'Approve Work Orders', 0, 20, 0, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(9, 'FindAWorkOrder', 'Find A Work Order', 0, 30, 0, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(10, 'WorkOrders_SendRecruitmentEmail', 'Send Recruitment Email', 7, 130, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(11, 'WorkOrders_SendBackUpTechEmail', 'Send Back-Up Tech Email', 7, 140, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(12, 'WorkOrders_SendProjectEmail', 'Send Project Email', 7, 150, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(13, 'WorkOrders_UploadWorkOrders', 'Upload Work Orders', 7, 160, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(14, 'WorkOrders_CreatedTab', 'Created Tab', 7, 170, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(15, 'WorkOrders_PublishedTab', 'Published Tab', 7, 180, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(16, 'WorkOrders_AssignedTab', 'Assigned Tab', 7, 190, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(17, 'WorkOrders_WorkDoneTab', 'Work Done Tab', 7, 200, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(18, 'WorkOrders_IncompleteTab', 'Incomplete Tab', 7, 210, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(19, 'WorkOrders_CompletedTab', 'Completed Tab', 7, 220, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(20, 'WorkOrders_AllTab', 'All Tab', 7, 230, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(21, 'WorkOrders_DeactivatedTab', 'Deactivated Tab', 7, 240, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(22, 'MyProjects', 'My Projects', 0, 250, 0, 3, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(23, 'MyProjects_ViewEditProjects', 'View / Edit Projects', 22, 260, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(24, 'MyProjects_AddProject', 'Add Project', 22, 270, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(25, 'MyProjects_ImportSiteList', 'Import Site List', 22, 280, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(26, 'FSMapper', 'FS-Mapper', 0, 290, 0, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(27, 'Reports', 'Reports', 0, 300, 0, 10, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(28, 'Reports_ProjectManagementReports', 'Project Management Reports', 27, 310, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(29, 'Reports_FSMetricsReports', 'FS-Metrics Reports', 27, 320, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(30, 'Reports_UnassignedWorkOrders', 'Unassigned Work Orders', 27, 330, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(31, 'Reports_TechnicianSLACompliance', 'Technician SLA Compliance', 27, 340, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(32, 'Reports_TechReviewAndConfirm', 'Tech Review and Confirm', 27, 350, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(33, 'Reports_ClientDeniedTechs', 'Client Denied Techs', 27, 360, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(34, 'Reports_ClientPreferredTechs', 'Client Preferred Techs', 27, 370, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(35, 'Reports_TechCalledReport', 'Tech Called Report', 27, 380, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(36, 'Reports_OutOfScopeReport', 'Out of Scope Report', 27, 390, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(37, 'Reports_TechWOAddressReport', 'Tech WO Address Report', 27, 400, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(38, 'Users', 'Users', 0, 410, 0, 2, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(39, 'Users_EditUsers', 'Edit Users', 38, 420, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(40, 'Users_AddUser', 'Add User', 38, 430, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(41, 'QuickTicket', 'QuickTicket', 0, 440, 0, 2, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(42, 'QuickTicket_EditQuickTicketUsers', 'Edit QuickTicket Users', 41, 450, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(43, 'QuickTicket_AddQuickTicketUsers', 'Add QuickTicket Users', 41, 460, 1, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(44, 'EditMyClientProfile', 'Edit My Client Profile', 0, 470, 0, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(45, 'GlobalGadgets ', 'Global Gadgets ', 0, 480, 0, 0, 1);
INSERT INTO `cliaccess_atribs` (`id`, `name`, `label`, `parent_id`, `presentation_order`, `levelid`, `childs`, `display`) VALUES(46, 'Reports_TechSiteSatisSurveyList', 'Tech Site Satisfaction Survey List', 27, 341, 1, 0, 1);

/*Table structure for table `cliaccessctl` */

DROP TABLE IF EXISTS `cliaccessctl`;

CREATE TABLE IF NOT EXISTS `cliaccessctl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(11) NOT NULL,
  `cliaccess_atrib_id` int(11) NOT NULL,
  `cliaccess_atrib_value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `clients_ext` */

DROP TABLE IF EXISTS `clients_ext`;

CREATE TABLE IF NOT EXISTS `clients_ext` (
  `ClientId` int(11) NOT NULL,
  `FSClientServiceDirectorId` int(11) NOT NULL,
  `FSAccountManagerId` int(11) NOT NULL,
  `File1URL` varchar(255) DEFAULT NULL,
  `File2URL` varchar(255) DEFAULT NULL,
  `File3URL` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ClientId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `cliprofile_action_log` */

DROP TABLE IF EXISTS `cliprofile_action_log`;

CREATE TABLE IF NOT EXISTS `cliprofile_action_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `action` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `logdate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Table structure for table `operations_staff` */

DROP TABLE IF EXISTS `operations_staff`;

CREATE TABLE IF NOT EXISTS `operations_staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupid` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `operations_staff`
--

INSERT INTO `operations_staff` (`id`, `groupid`, `name`, `email`, `phone`) VALUES(1, 1, 'Stephanie Bledsoe', 'sbled@fs.com', '555-888-5588');
INSERT INTO `operations_staff` (`id`, `groupid`, `name`, `email`, `phone`) VALUES(2, 1, 'Dave Valento', 'dave@fs.com', '555-777-5577');
INSERT INTO `operations_staff` (`id`, `groupid`, `name`, `email`, `phone`) VALUES(3, 1, 'Aaron Acker', 'aron@fs.com', '555-666-5566');
INSERT INTO `operations_staff` (`id`, `groupid`, `name`, `email`, `phone`) VALUES(4, 2, 'Shad Follmer\r\n', 'shad@fs.com', '555-555-5555');
INSERT INTO `operations_staff` (`id`, `groupid`, `name`, `email`, `phone`) VALUES(5, 2, 'David Baggett', 'David@fs.com', '555-444-5544');
INSERT INTO `operations_staff` (`id`, `groupid`, `name`, `email`, `phone`) VALUES(6, 2, 'Mike Kraemer', 'mike@fs.com', '555-333-5533');


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
