
SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- Table structure for table `operations_staff`
DROP TABLE IF EXISTS `operations_staff`;
CREATE TABLE IF NOT EXISTS `operations_staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupid` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- Dumping data for table `operations_staff`
INSERT INTO `operations_staff` (`id`, `groupid`, `name`, `email`, `phone`) VALUES
(1,2,'Aaron Acker','aacker@fieldsolutions.com','952-955-9033'),
(2,2,'David Baggett','dbaggett@fieldsolutions.com','512-789-5333'),
(3,2,'Stephanie Bledsoe','sbledsoe@fieldsolutions.com','402-817-4833'),
(4,2,'Shad Follmer','sfollmer@fieldsolutions.com','952-288-2507'),
(5,2,'Cary Goldman','cgoldman@fieldsolutions.com	','224-513-5080'),
(6,2,'Kevin Joyce','kjoyce@fieldsolutions.com','843-476-5311'),
(7,2,'Mike Kraemer','mkraemer@fieldsolutions.com','320-230-2090'),
(8,2,'Marty Reader','mreader@fieldsolutions.com','952-288-2506'),
(9,1,'Aaron Acker','aacker@fieldsolutions.com','952-955-9033'),
(10,1,'Stephanie Bledsoe','sbledsoe@fieldsolutions.com','402-817-4833'),
(11,1,'Dave Valento','dvalento@fieldsolutions.com','512-506-8962');

-- insert if not exists
insert into clients_ext(ClientId,FSClientServiceDirectorId,FSAccountManagerId)
select clients.ClientID,0,0
From clients 
LEFT JOIN clients_ext ON clients_ext.ClientId = clients.ClientID
WHERE clients_ext.ClientId IS NULL
and clients.CompanyName in ('4 Points','4Liberty Inc','A.I. Technologies, Inc.','Access POS, LLC','ACD Reps','Acecom Inc-SA','Acxiom','Adacel Systems','Advanced Computer Concepts, LLC','Advanced Service Technologies','Advanced Technology Services','Agilysys','Aimwick','Air Innovation Resources','Akibia','Allied Digital','Alta InfoServ','AmeriPride Services Inc','AMS.NET','Apex Computer Systems Inc','Applied Business Communications','Appollo Systems','Asset Recovery Corp.','Atlantic Webs, Inc.','Automated Security Alert','B2B Computers','Bailiwick','BBVS-AL','BBVS-GC','Belmont Technologies','BG Global Networks','Black Box','Black Box - GET','Black Box Cabling - North','Black Box Customer Services Solutions Team','Black Box Global Network Services','Black Box Johnson City','Black Box Logos Communications',
'Black Box North Carolina','Black Box Voice Services South','Black Box Voice Svs N.','Bluewater','Boston Media Cast LLC','Cardinal Pointe Communications','Catalina Marketing','Celergy Networks','Centrek Distribution Services','Certified Parts','Chameleon Power Inc','CineMassive Displays','Cokeva Digital Technology','Communication Technology Services','Comm-Works','Compucom','Compuspar Group','Computer Integrated Services LLC','Computer Sciences Corporation','Computer1 Products','ComputerPlus Sales & Service, Inc.','Continental Film','Continuant','Core Technology Solutions','COTG Xerox','Crestview Communications','Cross Telecom','CrossCom','CRS Inc.','CSC-1AZ','CSC-2IL','CSC-3IN','CSC-4NC1','CSC-5NC2','CSC-6QUE','CSC-7ONT','CSC-Toyota','CSR Technology Group','Dart Telecom','DataCap Direct','Datamax Services',
'Dataprise','DataStarUSA','DC Tech','DecisionOne','DELL Inc Self Service','DELL Inc.','Delta FRO','DeployNet Inc','DFW Technology Inc','Diebold','DiGiPOS Store Solutions','Digital Controls Corporation','Digital Controls Corporation-FS','Digital Tech','Direct POS','Dolomite Technology','DTS Varietech','DTT Surveillance','EarGlue by Accurate Communications','Ecolab','ECR Software','Endeavor','EnPointe','Equus Computer Systems, Inc.','Federated Service','FieldSolutions','First Line Support','Fix and Forget','Flextronics','Flying Eagles','FS-Core Technology Solutions','FS-Fujitsu Transactions Solutions','FS-John Ryan','Fujitsu Inside Wiring','Fujitsu Transactions Solutions','GA Services LLC','Gant Solutions LLC','Global Techs International Inc','Graybow','Halifax - Wincor','Halifax - XONG','Halifax Corporation','Halifax Sun Trust','HMS Technologies, Inc','Home Security Plus','i3gcorp','IBM Global Technology Services','IkkanTek Computer Services','Infinity Technology Solutions','Inhouse Contracting','InTegriLogic Corp','Intelinet Services','Intellys','iPort Media','IT decision Management','IT Definitive','IT Express','IT Management','i-team','iTech Solutions','ITSCI','John Ryan','KaiserComm Inc.','KBS','KIMSAN Technologies','Kodak','LAN Operations','Laser Technologies','Logistics International, LLC','Manna Distribution Services','Manna Distribution Services-FS','Manna Distribution Services-SS','Maratona Communications Inc.','Master Technology Group','McCusker Service Group Inc - MSGI','Merchandising Technologies Inc','Merrill & Associates Inc','Met-Tech Corp','Millenium Systems and Parts','Morgan Birge','mPayGateway','MPD Parts','Mustang Micro','National Network Solutions','National Retail Services Group','Nationwide SCS','NCR','NCR-FS','NCR-FS to SS','NCR-SS','Near Data','NEC Display Solutions','NECAM Corporation of America','Network Access Products','Network Guidance 2.0','New Millennium Technologies, LLC','Non-Utilized Time','NuFocus','Onsite Technology Services','OUI Managed Services / ScanSource','Outreach Technology','Outsource International Ltd.','Paragon Micro','Parallel Technologies','PC Cash Drawer','People To Go','Peoples Computer Services, Inc.','Perot','PharmaSafe LLC','Phoinix Group','PlumChoice Inc','Pomeroy IT Solutions','POS Plus','Posiflex','PosSystem.ca','ProITco','Pump It Up','Pure Choice','Pyramid Technology Services','R&R Solutions Inc','Radiant Systems','RedDotNet','Relational Technology Solutions','Reliable IT','Replay Systems','Response Link','Retail Deployment Solutions','Retail Echelon Installation Group','Retail Echelon Installation Group 2','RF Techno','Rhombus Services','Richard Fleischman and Associates','RL Williams Company','RollUsOut','Royal Communications','RTGPOS','SabeRex Group LTD','Sarcom','SenSource','Sentinel Technologies','Service USA','ServiceKey LLC','ServRight','Share Tech','SIGMAnet','SIS Consulting','Smith Micro Technologies Inc','Solid Systems Computer Services Ltd','Spot Cast','Starnes-Oswalt','Storeworks Technologies','Stratacache','Suisse Associates','Supply Chain Services','SwiftPoint','Synacom','Systems Integration LLC','TDX Tech','TeamOne Repair','Tech Force','Technology Services Group - Louisiana','Technology Services Worldwide','Tekcetera Inc','Tekserve','Telecomm Wizards','Terix','Tidel','Tolt Service Group','Trans Alarm','Transcend Communications','TurnKey Kiosks','United Service Source, Inc.','Valcom','Velocha, LLC','Velociti Inc',
'Velocity','Verdigrid','Virteva','Vision POS','Vision Technologies Inc.','Visual Sound, Inc.','VoicePro/Sater Group','VS Networks','WAND Corporation','Wincor','Wireless Ronin Technologies','Wisconsin Independent Network','Worldlink','Xerox - NARS','Xerox Corporation','Xerox GIS','Xterprise Inc.','Yoh-SciAtl','Zap Consulting','Zip Express Installation','Zip2Tech','Ziyad','Zones, Inc.','Zotek Enterprises'
);

-- assign CSD
update clients_ext set FSClientServiceDirectorId=9 where ClientId in (select ClientID From clients where 
CompanyName in ('AMS.NET','Appollo Systems','BBVS-AL','BBVS-GC','Belmont Technologies','Black Box','Black Box - GET','Black Box Cabling - North','Black Box Customer Services Solutions Team','Black Box Global Network Services','Black Box Johnson City','Black Box Logos Communications','Black Box North Carolina','Black Box Voice Services South','Black Box Voice Svs N.','Cardinal Pointe Communications','Catalina Marketing','COTG Xerox','DecisionOne','DELL Inc Self Service','DELL Inc.','Flying Eagles','HMS Technologies, Inc','Home Security Plus','IT Express','Laser Technologies','Manna Distribution Services','Manna Distribution Services-FS','MPD Parts','Parallel Technologies','PharmaSafe LLC','Response Link','SabeRex Group LTD','Solid Systems Computer Services Ltd','Supply Chain Services',
'Tidel','Virteva','Wireless Ronin Technologies','Xerox - NARS','Xerox Corporation','Zip Express Installation'
));

update clients_ext set FSClientServiceDirectorId=10 where ClientId in (select ClientID From clients where 
CompanyName in ('A.I. Technologies, Inc.','Access POS, LLC','ACD Reps','Acecom Inc-SA','Acxiom','Advanced Computer Concepts, LLC','Advanced Service Technologies','Advanced Technology Services','Agilysys','Aimwick','Air Innovation Resources','Akibia','Allied Digital','Applied Business Communications','Asset Recovery Corp.','Atlantic Webs, Inc.','Automated Security Alert','B2B Computers','Bailiwick','Bluewater','Boston Media Cast LLC','Centrek Distribution Services','Chameleon Power Inc','CineMassive Displays','Cokeva Digital Technology','Communication Technology Services','Computer Sciences Corporation','Computer1 Products','ComputerPlus Sales & Service, Inc.','Core Technology Solutions','Crestview Communications','Cross Telecom','CrossCom','CRS Inc.','CSC-1AZ','CSC-2IL','CSC-3IN','CSC-4NC1','CSC-5NC2','CSC-6QUE','CSC-7ONT',
'CSC-Toyota','CSR Technology Group','Dart Telecom','DataCap Direct','Datamax Services','Dataprise','DataStarUSA','DC Tech','DFW Technology Inc','Digital Controls Corporation','Digital Tech','Direct POS','Dolomite Technology','DTS Varietech','DTT Surveillance','EarGlue by Accurate Communications','ECR Software','Endeavor','EnPointe','Equus Computer Systems, Inc.','Federated Service','First Line Support','Fix and Forget','Flextronics','Fujitsu Transactions Solutions','GA Services LLC','Gant Solutions LLC','Global Techs International Inc','Graybow','Halifax - Wincor','Halifax Corporation','Halifax Sun Trust','IkkanTek Computer Services','Infinity Technology Solutions','InTegriLogic Corp','Intelinet Services','Intellys','IT decision Management','IT Definitive','i-team','ITSCI','John Ryan','KBS','KIMSAN Technologies','Kodak',
'Logistics International, LLC','Manna Distribution Services-SS','Maratona Communications Inc.','Master Technology Group','McCusker Service Group Inc - MSGI','Merchandising Technologies Inc','Merrill & Associates Inc','Met-Tech Corp','Morgan Birge','Mustang Micro','National Network Solutions','National Retail Services Group','Nationwide SCS','NCR-SS','Near Data','Network Access Products','New Millennium Technologies, LLC','NuFocus','Onsite Technology Services','OUI Managed Services / ScanSource','Outreach Technology','PC Cash Drawer','People To Go','Peoples Computer Services, Inc.','PlumChoice Inc','Pomeroy IT Solutions','Posiflex','PosSystem.ca','Pump It Up','Pure Choice','R&R Solutions Inc','Radiant Systems','RedDotNet','Reliable IT','Retail Deployment Solutions','Retail Echelon Installation Group','Retail Echelon Installation Group 2','RF Techno','Rhombus Services','Royal Communications','RTGPOS','Sarcom','Sentinel Technologies','Service USA','ServiceKey LLC','ServRight','SIGMAnet','SIS Consulting','Storeworks Technologies','Suisse Associates','SwiftPoint','Systems Integration LLC','TDX Tech','Tech Force','Technology Services Worldwide','Tekcetera Inc','Tekserve','Terix','Tolt Service Group','Trans Alarm','Transcend Communications',
'TurnKey Kiosks','United Service Source, Inc.','Valcom','Velocha, LLC','Velociti Inc','Velocity','Verdigrid','Vision POS','VoicePro/Sater Group','VS Networks','WAND Corporation','Wisconsin Independent Network','Worldlink','Xterprise Inc.','Zap Consulting','Ziyad','Zotek Enterprises'
));

update clients_ext set FSClientServiceDirectorId=11 where ClientId in (select ClientID From clients where 
CompanyName in ('4 Points','4Liberty Inc','Adacel Systems','AmeriPride Services Inc','Apex Computer Systems Inc','Celergy Networks','Certified Parts','Comm-Works','Compucom','Compuspar Group','Continental Film','DeployNet Inc','Diebold','DiGiPOS Store Solutions','Digital Controls Corporation-FS','FS-Core Technology Solutions','FS-Fujitsu Transactions Solutions','Fujitsu Inside Wiring','i3gcorp',
'IBM Global Technology Services','iPort Media','iTech Solutions','KaiserComm Inc.','Millenium Systems and Parts','NCR','NCR-FS','NCR-FS to SS','NEC Display Solutions','NECAM Corporation of America','Network Guidance 2.0','Outsource International Ltd.','Phoinix Group','POS Plus','Relational Technology Solutions','Richard Fleischman and Associates',
'Smith Micro Technologies Inc','Stratacache','Vision Technologies Inc.','Visual Sound, Inc.','Wincor'
));


-- assign AM
update clients_ext set FSAccountManagerId=1 where ClientId in (select ClientID From clients where 
CompanyName in ('SabeRex Group LTD'));

update clients_ext set FSAccountManagerId=2 where ClientId in (select ClientID From clients where 
CompanyName in ('Apex Computer Systems Inc','B2B Computers','Catalina Marketing','Compucom','CRS Inc.','DELL Inc Self Service','DELL Inc.',
'DFW Technology Inc','HMS Technologies, Inc','Replay Systems','ServRight','Stratacache','Tolt Service Group'
));

update clients_ext set FSAccountManagerId=3 where ClientId in (select ClientID From clients where 
CompanyName in ('Air Innovation Resources','AMS.NET','Fix and Forget','PlumChoice Inc','Pure Choice','RTGPOS'
));

update clients_ext set FSAccountManagerId=4 where ClientId in (select ClientID From clients where 
CompanyName in ('4Liberty Inc','Advanced Computer Concepts, LLC','AmeriPride Services Inc','Applied Business Communications','Appollo Systems','Asset Recovery Corp.','Automated Security Alert','Bailiwick','BBVS-AL','BBVS-GC','Belmont Technologies','Black Box','Black Box - GET','Black Box Cabling - North','Black Box Customer Services Solutions Team','Black Box Global Network Services','Black Box Johnson City','Black Box Logos Communications','Black Box North Carolina','Black Box Voice Services South','Black Box Voice Svs N.','Bluewater',
'Celergy Networks','Centrek Distribution Services','Certified Parts','Comm-Works','Compuspar Group','Computer Sciences Corporation','Continuant','Crestview Communications','Cross Telecom','CrossCom','CSC-1AZ','CSC-2IL','CSC-3IN','CSC-4NC1','CSC-5NC2','CSC-6QUE','CSC-7ONT','CSC-Toyota','DataCap Direct','Datamax Services','DeployNet Inc','Diebold','Digital Controls Corporation','Digital Controls Corporation-FS','DTT Surveillance','Equus Computer Systems, Inc.','FS-John Ryan','Graybow','Home Security Plus','John Ryan','KaiserComm Inc.','Manna Distribution Services','Manna Distribution Services-FS','Manna Distribution Services-SS','Master Technology Group','Merchandising Technologies Inc','Merrill & Associates Inc','Millenium Systems and Parts','Morgan Birge','Network Access Products','Network Guidance 2.0','OUI Managed Services / ScanSource','Outsource International Ltd.','Parallel Technologies','People To Go','Relational Technology Solutions',
'Rhombus Services','Smith Micro Technologies Inc','Storeworks Technologies','TDX Tech','Technology Services Group - Louisiana','Tekcetera Inc','Trans Alarm','TurnKey Kiosks','United Service Source, Inc.','Velociti Inc','Virteva','Vision Technologies Inc.','Visual Sound, Inc.','VoicePro/Sater Group','WAND Corporation','Wireless Ronin Technologies','Zip Express Installation','Zones, Inc.'
));

update clients_ext set FSAccountManagerId=5 where ClientId in (select ClientID From clients where 
CompanyName in ('ACD Reps','Acecom Inc-SA','Aimwick','CineMassive Displays','Cokeva Digital Technology','Computer1 Products','Direct POS','Dolomite Technology','Flying Eagles','Gant Solutions LLC','IkkanTek Computer Services','Infinity Technology Solutions','Intelinet Services','iPort Media','IT decision Management','iTech Solutions',
'Laser Technologies','Met-Tech Corp','National Network Solutions','Nationwide SCS','Near Data','New Millennium Technologies, LLC','Outreach Technology','Peoples Computer Services, Inc.','PosSystem.ca','Sarcom','Suisse Associates','Systems Integration LLC','Technology Services Worldwide','Valcom','Verdigrid','VS Networks','Wisconsin Independent Network','Zap Consulting','Ziyad'
));

update clients_ext set FSAccountManagerId=6 where ClientId in (select ClientID From clients where 
CompanyName in ('PharmaSafe LLC','Xerox - NARS','Xerox Corporation'
));

update clients_ext set FSAccountManagerId=7 where ClientId in (select ClientID From clients where 
CompanyName in ('4 Points','Access POS, LLC','DataStarUSA','DC Tech','DTS Varietech','First Line Support','FS-Fujitsu Transactions Solutions','Fujitsu Inside Wiring','Fujitsu Transactions Solutions','IT Express','McCusker Service Group Inc - MSGI','National Retail Services Group','NCR','NCR-FS','NCR-FS to SS','NCR-SS','NECAM Corporation of America','Perot','Richard Fleischman and Associates',
'Service USA','Starnes-Oswalt','Tekserve','Wincor','Worldlink'
));

update clients_ext set FSAccountManagerId=8 where ClientId in (select ClientID From clients where 
CompanyName in ('A.I. Technologies, Inc.','Acxiom','Adacel Systems','Advanced Service Technologies','Advanced Technology Services','Agilysys','Akibia','Allied Digital','Alta InfoServ','Atlantic Webs, Inc.','BG Global Networks','Boston Media Cast LLC','Cardinal Pointe Communications','Chameleon Power Inc','Communication Technology Services','Computer Integrated Services LLC','ComputerPlus Sales & Service, Inc.',
'Continental Film','Core Technology Solutions','COTG Xerox','CSR Technology Group','Dart Telecom','Dataprise','DecisionOne','DiGiPOS Store Solutions','Digital Tech','EarGlue by Accurate Communications','Ecolab','ECR Software','Endeavor','EnPointe','Federated Service','Flextronics','FS-Core Technology Solutions','GA Services LLC','Global Techs International Inc','Halifax - Wincor','Halifax Corporation','Halifax Sun Trust',
'i3gcorp','IBM Global Technology Services','InTegriLogic Corp','Intellys','IT Definitive','i-team','KBS','KIMSAN Technologies','Kodak','mPayGateway','MPD Parts','Mustang Micro','NEC Display Solutions','NuFocus','Onsite Technology Services','Paragon Micro','PC Cash Drawer','Phoinix Group','Pomeroy IT Solutions','POS Plus','Posiflex','ProITco','Pump It Up','Pyramid Technology Services','R&R Solutions Inc','Radiant Systems','RedDotNet',
'Reliable IT','Response Link','Retail Deployment Solutions','Retail Echelon Installation Group','Retail Echelon Installation Group 2','RF Techno','RL Williams Company','RollUsOut','Royal Communications','SenSource','Sentinel Technologies','ServiceKey LLC','SIGMAnet','SIS Consulting','Supply Chain Services','SwiftPoint','Synacom','TeamOne Repair','Tech Force',
'Terix','Tidel','Transcend Communications','Velocha, LLC','Velocity','Vision POS','Xerox GIS','Xterprise Inc.','Zip2Tech'
));








