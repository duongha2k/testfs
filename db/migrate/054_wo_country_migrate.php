<?php
require_once dirname(__FILE__).'/../../includes/modules/common.init.php';

$sql = "SELECT WIN_NUM, State FROM `work_orders` WHERE Country = 'US' AND State IS NOT NULL AND State NOT IN ('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY', '') ORDER BY WIN_NUM";
$db = Zend_Registry::get('DB');
$wo = $db->fetchAll($sql);

$stateList = array('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY');
$intStates = array(
"BC" => "CA",
"MB" => "CA",
"NB" => "CA",
"NL" => "CA",
"NT" => "CA",
"NS" => "CA",
"NU" => "CA",
"ON" => "CA",
"PE" => "CA",
"QC" => "CA",
"SK" => "CA",
"YT" => "CA",
"AG" => "MX",
"BJ" => "MX",
"BS" => "MX",
"CP" => "MX",
"CH" => "MX",
"CI" => "MX",
"CU" => "MX",
"CL" => "MX",
"DF" => "MX",
"DG" => "MX",
"GJ" => "MX",
"GR" => "MX",
"HG" => "MX",
"JA" => "MX",
"EM" => "MX",
"MH" => "MX",
"NA" => "MX",
"NL" => "MX",
"OA" => "MX",
"PU" => "MX",
"QA" => "MX",
"QR" => "MX",
"SL" => "MX",
"SI" => "MX",
"SO" => "MX",
"TA" => "MX",
"TM" => "MX",
"TL" => "MX",
"VZ" => "MX",
"YC" => "MX",
"ZT" => "MX"
);

$distinct = array();

//echo $db->query("UPDATE work_orders SET Country = ?", array("US"));
//die();

foreach ($wo as $w) {
	$state = trim($w['State']);
	if (!in_array($state, $stateList)) {
		$distinct[$state] = $state;
		@$country = $intStates[$state];
		if (!empty($country)) {
			$sql = "UPDATE work_orders SET State = '$state', Country = '$country' WHERE WIN_NUM = {$w['WIN_NUM']}";
			$db->query($sql);
			echo "$sql\n";
	}
	}
	else if ($state != $w['State']) {
		$sql = "UPDATE work_orders SET State = '$state' WHERE WIN_NUM = {$w['WIN_NUM']}";
		$db->query($sql);
		echo "$sql\n";
	}
}

