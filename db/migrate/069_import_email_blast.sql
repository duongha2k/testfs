
CREATE TABLE IF NOT EXISTS `work_order_import` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `file` (`file`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `import_tech_emailed` (
  `importId` int(11) NOT NULL,
  `TechID` int(11) NOT NULL,
  KEY `importId` (`importId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `import_tech_emailed`
  ADD CONSTRAINT `import_tech_emailed_ibfk_1` FOREIGN KEY (`importId`) REFERENCES `work_order_import` (`id`) ON DELETE CASCADE;
