-- equipment for 761 and 757
INSERT INTO equipment(id,`name`,label,description)
VALUES(36,'HandTools','Hand Tools','Hand Tools');
INSERT INTO equipment(id,`name`,label,description)
VALUES(37,'CordlessDrill','Cordless Drill','Cordless Drill');
INSERT INTO equipment(id,`name`,label,description)
VALUES(38,'ProtectionEquipment','Personal Protection Equipment','Personal Protection Equipment (Gloves, Hard Hat, Boots)');
INSERT INTO equipment(id,`name`,label,description)
VALUES(39,'MachineLabelMaker','Machine Label Maker','Machine Label Maker (P-Touch, Bradley, etc.)');
INSERT INTO equipment(id,`name`,label,description)
VALUES(40,'GoferPoles','Gofer Poles','Gofer Poles (or Glow Rods)');
INSERT INTO equipment(id,`name`,label,description)
VALUES(41,'FiberOpticdBLossMeter','Fiber Optic dB Loss Meter & Light Source','Fiber Optic dB Loss Meter & Light Source');
INSERT INTO equipment(id,`name`,label,description)
VALUES(42,'FiberOpticTerminationKit','Fiber Optic Termination Kit','Fiber Optic Termination Kit');

-- skills for 761 and 757
INSERT INTO skills(id,`name`,label)
VALUES(49,'OperationScissorLiftExperience','Do you know how to operate a scissor lift and have extensive experience using a ladder?');
INSERT INTO skills(id,`name`,label)
VALUES(50,'UnderstandTelecomColorCodeExperience','Do you understand the telecom color code and have experience with termination jacks and patch panels?');
INSERT INTO skills(id,`name`,label)
VALUES(51,'InstallationMultipleCableExperience','Do you have experience with multiple cable installation methods (installing in conduit using fish tape, pull string, or shop vac)?');
INSERT INTO skills(id,`name`,label)
VALUES(52,'InstallingSleevesRacewaysExperience','Do you have experience installing sleeves firestops, J-Hooks, Panduit, and Wire Mold raceways?');
INSERT INTO skills(id,`name`,label)
VALUES(53,'InstallationVoiceDataEquipmentExperience','Do you have experience with voice and data equipment room installation and build out (ex: data cabinets and racks, plywood, 66/110 blocks, patch panels, etc)?');
INSERT INTO skills(id,`name`,label)
VALUES(54,'SingleAndMultiModeTerminationTestingExp','Do you have experience in single-mode and multi-mode termination and testing?');
INSERT INTO skills(id,`name`,label)
VALUES(55,'UsingLaptopForSwitchConfigurationExp','Do you have experience using a laptop for basic switch configuration (HyperTerminal, Terra Term, etc.)?');
INSERT INTO skills(id,`name`,label)
VALUES(56,'DMARCRoomsEquipmentExperience','Do you have experience with DMARC rooms and equipment such as Rj-21X, Smart jacks, lightning protectors, and cross connect fields?');
INSERT INTO skills(id,`name`,label)
VALUES(57,'OperationPBXExperience','Do you have experience with basic PBX / Key system operation (ex: general programming, station card and CO line card replacement, station / extension assignment)?');

