CREATE TABLE IF NOT EXISTS `ISOBankInfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ISOID` int(11) NOT NULL,
  `FirstName` varchar(40) NOT NULL,
  `LastName` varchar(40) NOT NULL,
  `PaymentMethod` varchar(20) NOT NULL,
  `AgreeTerms` tinyint(1) NOT NULL,
  `AccountName` varchar(50) NOT NULL,
  `DepositType` varchar(20) NOT NULL,
  `BankName` varchar(50) NOT NULL,
  `BankAddress1` varchar(50) NOT NULL,
  `BankAddress2` varchar(50) NOT NULL,
  `BankCity` varchar(40) NOT NULL,
  `BankState` int(3) NOT NULL,
  `BankZip` varchar(20) NOT NULL,
  `BankCountry` int(3) NOT NULL,
  `AccountNum` tinyblob NOT NULL,
  `RoutingNum` tinyblob NOT NULL,
  `DateChange` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `MaskedAccountNum` varchar(20) NOT NULL,
  `MaskedRoutingNum` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ISOID` (`ISOID`),
  KEY `FirstName` (`FirstName`),
  KEY `LastName` (`LastName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


# Copy iso bank info to new table
INSERT IGNORE INTO ISOBankInfo (ISOID, FirstName, LastName, PaymentMethod, AgreeTerms, DepositType, BankName, BankAddress1, BankAddress2, BankCity, BankState, BankZip, BankCountry, AccountName, RoutingNum, AccountNum, MaskedRoutingNum, MaskedAccountNum, DateChange) SELECT SUBSTR(TechID, 5), FirstName, LastName, PaymentMethod, AgreeTerms, DepositType, BankName, BankAddress1, BankAddress2, BankCity, BankState, BankZip, BankCountry, AccountName, RoutingNum, AccountNum, MaskedRoutingNum, MaskedAccountNum, DateChange FROM TechBankInfo WHERE TechID LIKE 'ISO%'

CREATE TABLE IF NOT EXISTS `quickbooks_iso` (
  `id` int(16) NOT NULL AUTO_INCREMENT,
  `member_id` int(16) NOT NULL,
  `list_id` char(36) DEFAULT NULL,
  `edit_sequence` char(16) DEFAULT NULL,
  `company_name` char(41) DEFAULT NULL,
  `firstname` char(25) NOT NULL,
  `middlename` char(5) DEFAULT NULL,
  `lastname` char(25) NOT NULL,
  `phone` char(21) NOT NULL,
  `email` varchar(1023) NOT NULL,
  `address1` char(41) NOT NULL,
  `address2` char(41) DEFAULT NULL,
  `city` char(31) NOT NULL,
  `country` char(31) NOT NULL,
  `state` char(21) NOT NULL,
  `zip` char(13) NOT NULL,
  `payment_method` char(31) NOT NULL,
  `ach_account_name` char(31) DEFAULT NULL,
  `ach_bank_name` char(31) DEFAULT NULL,
  `ach_bank_city` char(31) NOT NULL,
  `ach_bank_state` char(31) DEFAULT NULL,
  `ach_bank_zip` char(31) DEFAULT NULL,
  `ach_bank_country` char(31) DEFAULT NULL,
  `ach_account_type` char(31) DEFAULT NULL,
  `ach_routing_number` tinyblob,
  `ach_account_number` tinyblob,
  `last_update` datetime NOT NULL,
  `last_update_by` char(16) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `member_id` (`member_id`),
  KEY `last_update_by` (`last_update_by`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;


# Copy iso bank info to new table
INSERT IGNORE INTO quickbooks_iso (member_id, list_id, edit_sequence, company_name, firstname, middlename, lastname, phone, email, address1, address2, city, country, state, zip, payment_method, ach_account_name, ach_bank_name, ach_bank_city, ach_bank_state, ach_bank_zip, ach_bank_country, ach_account_type, ach_routing_number, ach_account_number, last_update, last_update_by ) SELECT SUBSTR(member_id, 5), list_id, edit_sequence, company_name, firstname, middlename, lastname, phone, email, address1, address2, city, country, state, zip, payment_method, ach_account_name, ach_bank_name, ach_bank_city, ach_bank_state, ach_bank_zip, ach_bank_country, ach_account_type, ach_routing_number, ach_account_number, last_update, last_update_by FROM quickbooks_members WHERE member_id LIKE 'ISO%'
DELETE FROM quickbooks_members WHERE member_id IN (SELECT CONCAT('ISO-', CAST(member_id AS CHAR)) FROM quickbooks_iso)

DELETE FROM TechBankInfo WHERE TechID LIKE 'ISO%';

ALTER TABLE `TechBankInfo` CHANGE `TechID` `TechID` INT( 11 ) NOT NULL
ALTER TABLE `quickbooks_members` CHANGE `member_id` `member_id` INT( 16 ) NOT NULL

ALTER TABLE `tech_ext` CHANGE `TechID` `TechID` INT( 11 ) NOT NULL
