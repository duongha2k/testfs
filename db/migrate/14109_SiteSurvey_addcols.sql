
ALTER TABLE SiteSurvey ADD COLUMN S4Q14a_BOutlet1_Unplug_VMeter smallint(6) DEFAULT NULL;
ALTER TABLE SiteSurvey ADD COLUMN S4Q14b_BOutlet2_Unplug_VMeter smallint(6) DEFAULT NULL;
ALTER TABLE SiteSurvey ADD COLUMN S4Q14c_BOutlet3_Unplug_VMeter smallint(6) DEFAULT NULL;
ALTER TABLE SiteSurvey ADD COLUMN S4Q14d_BOutlet4_Unplug_VMeter smallint(6) DEFAULT NULL;
ALTER TABLE SiteSurvey ADD COLUMN S4Q14e_BOutlet5_Unplug_VMeter smallint(6) DEFAULT NULL;

ALTER TABLE SiteSurvey ADD COLUMN S10Q1_AirportInFrontofSecu_Checked tinyint;



