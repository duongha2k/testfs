-- admins
ALTER TABLE admins ADD COLUMN MustChangePassword tinyint DEFAULT 1;
ALTER TABLE admins ADD COLUMN DateOfLastPasswordChanged datetime DEFAULT NULL;

-- clients
ALTER TABLE clients ADD COLUMN MustChangePassword tinyint DEFAULT 1;
ALTER TABLE clients ADD COLUMN DateOfLastPasswordChanged datetime DEFAULT NULL;

-- TechBankInfo
ALTER TABLE TechBankInfo ADD COLUMN MustChangePassword tinyint DEFAULT 0;
ALTER TABLE TechBankInfo ADD COLUMN DateOfLastPasswordChanged datetime DEFAULT NULL;

-- Update 'MustChangePassword' field in TechBankInfo
Update TechBankInfo Set MustChangePassword = 1;

-- sys_vars
CREATE TABLE `sys_vars` (
  `ImplementPasswordToolDate` datetime DEFAULT NULL
  `ImpPswTool_SentEmailToClient` tinyint DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;