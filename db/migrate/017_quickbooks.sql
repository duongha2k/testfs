-- phpMyAdmin SQL Dump
-- version 2.11.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 14, 2010 at 10:42 AM
-- Server version: 5.0.91
-- PHP Version: 5.2.14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `quickbooks`
--

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

CREATE TABLE IF NOT EXISTS `quickbooks_bills` (
  `id` int(16) NOT NULL auto_increment,
  `edit_sequence` char(16) default NULL,
  `wo_id` char(20) NOT NULL,
  `member_id` char(41) NOT NULL,
  `bill_date` date NOT NULL,
  `pay_amount` float(8,2) NOT NULL,
  `pay_terms` char(31) default NULL,
  `is_paid` tinyint(1) NOT NULL default '0',
  `transaction_id` char(36) default NULL,
  `transaction_number` int(10) default NULL,
  `transaction_line_id` char(36) default NULL,
  `paid_amount` float(8,2) default NULL,
  `paid_date` date default NULL,
  `check_number` char(11) default NULL,
  `caspio_sync` tinyint(4) NOT NULL default '0',
  `last_update` datetime NOT NULL,
  `last_update_by` char(16) NOT NULL,
  `caspio_unique_id` int(11) NOT NULL,
  `caspio_type` tinyint(4) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `caspio_unique_id` (`caspio_unique_id`,`caspio_type`),
  KEY `transaction_id` (`transaction_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=98708 ;

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE IF NOT EXISTS `quickbooks_clients` (
  `id` int(16) NOT NULL auto_increment,
  `list_id` char(36) default NULL,
  `edit_sequence` char(16) default NULL,
  `client_id` int(16) default NULL,
  `company_name` char(41) NOT NULL,
  `contact_firstname` char(25) NOT NULL,
  `contact_lastname` char(25) NOT NULL,
  `contact_email` varchar(1023) NOT NULL,
  `address1` char(41) NOT NULL,
  `address2` char(41) default NULL,
  `city` char(31) NOT NULL,
  `country` char(31) NOT NULL,
  `state` char(21) NOT NULL,
  `zip` char(13) NOT NULL,
  `phone` char(21) NOT NULL,
  `fax` char(21) default NULL,
  `last_update` datetime NOT NULL,
  `last_update_by` char(16) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `client_id` (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE IF NOT EXISTS `quickbooks_members` (
  `id` int(16) NOT NULL auto_increment,
  `member_id` varchar(16) NOT NULL,
  `list_id` char(36) default NULL,
  `edit_sequence` char(16) default NULL,
  `company_name` char(41) default NULL,
  `iso_affiliation_id` int(11) default NULL,
  `firstname` char(25) NOT NULL,
  `middlename` char(5) default NULL,
  `lastname` char(25) NOT NULL,
  `phone` char(21) NOT NULL,
  `email` varchar(1023) NOT NULL,
  `address1` char(41) NOT NULL,
  `address2` char(41) default NULL,
  `city` char(31) NOT NULL,
  `country` char(31) NOT NULL,
  `state` char(21) NOT NULL,
  `zip` char(13) NOT NULL,
  `payment_method` char(31) NOT NULL,
  `ach_account_name` char(31) default NULL,
  `ach_bank_name` char(31) default NULL,
  `ach_bank_city` char(31) NOT NULL,
  `ach_bank_state` char(31) default NULL,
  `ach_bank_zip` char(31) default NULL,
  `ach_bank_country` char(31) default NULL,
  `ach_account_type` char(31) default NULL,
  `ach_routing_number` tinyblob,
  `ach_account_number` tinyblob,
  `last_update` datetime NOT NULL,
  `last_update_by` char(16) NOT NULL,
  `paycard_report_state` tinyint(4) NOT NULL default '0',
  `ssn` tinyblob NOT NULL,
  `ein` tinyblob NOT NULL,
  `which_as_taxid` tinyint(4) NOT NULL,
  `w9_company_name` varchar(41) default NULL,
  `w9_name` varchar(60) default NULL,
  `w9_info` tinyblob NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `member_id` (`member_id`),
  KEY `iso_affiliation_id` (`iso_affiliation_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4587 ;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_log`
--

CREATE TABLE IF NOT EXISTS `quickbooks_log` (
  `quickbooks_log_id` int(10) unsigned NOT NULL auto_increment,
  `quickbooks_ticket_id` int(10) unsigned default NULL,
  `msg` text NOT NULL,
  `log_datetime` datetime NOT NULL,
  PRIMARY KEY  (`quickbooks_log_id`),
  KEY `quickbooks_ticket_id` (`quickbooks_ticket_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=433363 ;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_queue`
--

CREATE TABLE IF NOT EXISTS `quickbooks_queue` (
  `quickbooks_queue_id` int(10) unsigned NOT NULL auto_increment,
  `quickbooks_ticket_id` int(10) unsigned default NULL,
  `qb_action` varchar(32) NOT NULL,
  `ident` varchar(32) NOT NULL,
  `extra` text NOT NULL,
  `priority` tinyint(3) unsigned NOT NULL,
  `qb_status` char(1) NOT NULL default 'q',
  `msg` text NOT NULL,
  `enqueue_datetime` datetime NOT NULL,
  `dequeue_datetime` datetime default NULL,
  PRIMARY KEY  (`quickbooks_queue_id`),
  KEY `qb_action` (`qb_action`,`ident`),
  KEY `priority` (`priority`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=199376 ;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_ticket`
--

CREATE TABLE IF NOT EXISTS `quickbooks_ticket` (
  `quickbooks_ticket_id` int(10) unsigned NOT NULL auto_increment,
  `qb_username` varchar(40) NOT NULL,
  `ticket` varchar(32) NOT NULL,
  `processed` int(10) unsigned NOT NULL,
  `bills_total` float(10,2) NOT NULL default '0.00',
  `number_of_bills` int(11) NOT NULL default '0',
  `num_paid` int(11) NOT NULL default '0',
  `amount_paid` float(10,2) NOT NULL default '0.00',
  `num_new_techs` int(11) NOT NULL default '0',
  `num_mod_techs` int(11) NOT NULL default '0',
  `lasterror_num` varchar(16) NOT NULL,
  `lasterror_msg` varchar(255) NOT NULL,
  `ipaddr` varchar(15) NOT NULL,
  `write_datetime` datetime NOT NULL,
  `touch_datetime` datetime NOT NULL,
  PRIMARY KEY  (`quickbooks_ticket_id`),
  KEY `qb_username` (`qb_username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1010 ;

-- --------------------------------------------------------

--
-- Table structure for table `quickbooks_user`
--

CREATE TABLE IF NOT EXISTS `quickbooks_user` (
  `qb_username` varchar(40) NOT NULL,
  `qb_password` varchar(40) NOT NULL,
  `write_datetime` datetime NOT NULL,
  `touch_datetime` datetime NOT NULL,
  PRIMARY KEY  (`qb_username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

