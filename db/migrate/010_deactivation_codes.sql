CREATE TABLE IF NOT EXISTS `deactivation_codes` (
  `ID` int(10) unsigned NOT NULL default '0',
  `Reason` varchar(255) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
