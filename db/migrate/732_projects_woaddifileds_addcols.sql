

ALTER TABLE projects ADD COLUMN SiteContactReminderCall tinyint; 
ALTER TABLE projects ADD COLUMN SiteContactReminderCallHours int; 

ALTER TABLE work_orders_additional_fields ADD COLUMN SiteContactBackedOutChecked tinyint(1);
ALTER TABLE work_orders_additional_fields ADD COLUMN NotifiedTime varchar(255);




