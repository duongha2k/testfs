
-- wo_tags
INSERT INTO wo_tags(id,wo_tag_code,wo_tag_name)
VALUES (9,'CompletionStatus','Completion Status');

-- wo_tag_items
INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active)
VALUES (25,'CompletionStatus','Complete1stVisit','Complete – 1st Visit',1,1);

INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active)
VALUES (26,'CompletionStatus','CompleteRevisit','Complete – Revisit',2,1);

INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active)
VALUES (27,'CompletionStatus','CompleteReassigned','Complete – Reassigned',3,1);

INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active)
VALUES (28,'CompletionStatus','CompleteRescheduled','Complete – Rescheduled',4,1);

INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active)
VALUES (29,'CompletionStatus','IncompleteSiteOrClientDriven_SiteTurnAwayOrAbort','Incomplete (Site / Client Driven) - Site Turn-Away / Abort',5,1);

INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active)
VALUES (30,'CompletionStatus','IncompleteSiteOrClientDriven_EndUserIssue','Incomplete (Site / Client Driven) - End-User Issue',6,1);

INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active)
VALUES (31,'CompletionStatus','IncompleteSiteOrClientDriven_HardwareRelated','Incomplete (Site / Client Driven) - Hardware Related ',7,1);

INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active)
VALUES (32,'CompletionStatus','IncompleteSiteOrClientDriven_OutofScopeWork','Incomplete (Site / Client Driven) - Out of Scope Work ',8,1);

INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active)
VALUES (33,'CompletionStatus','IncompleteTechDriven_DelayedArrival','Incomplete (Tech Driven) – Delayed Arrival',9,1);

INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active)
VALUES (34,'CompletionStatus','IncompleteTechDriven_PerformanceIssue','Incomplete (Tech Driven) – Performance Issue',10,1);

INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active)
VALUES (35,'CompletionStatus','IncompleteTechDriven_InternalIssue','Incomplete (Tech Driven) – Internal Issue',11,1);

INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active)
VALUES (36,'CompletionStatus','Incomplete_NaturalCausesOrForceMajeure','Incomplete – Natural Causes / Force Majeure',12,1);

INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active)
VALUES (37,'CompletionStatus','ChangeOrCancelSiteOrClientDriven','Change / Cancel (Site / Client Driven)',13,1);

INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active)
VALUES (38,'CompletionStatus','ChangeOrCancelTechDriven_NoShow','Change / Cancel (Tech Driven) – No Show',14,1);

INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active)
VALUES (39,'CompletionStatus','ChangeOrCancelTechDriven_BackOut','Change / Cancel (Tech Driven) – Back Out',15,1);

INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active)
VALUES (40,'CompletionStatus','ChangeOrCancelInternalIssue','Change / Cancel (Internal Issue)',16,1);

-- work_order_tagitem_values
DROP TABLE IF EXISTS `work_order_tagitem_values`;

CREATE TABLE `work_order_tagitem_values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `WIN_NUM` int(11) NOT NULL,
  `wo_tag_item_id` int(11) NOT NULL,
  `value_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- work_order_timeperform_results
DROP TABLE IF EXISTS `work_order_timeperform_results`;

CREATE TABLE `work_order_timeperform_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `WIN_NUM` int(11) NOT NULL,
  `wovisit_id` int(11) NOT NULL,
  `start_result_id` int(11) NOT NULL COMMENT '1: On Time; 2: Early; 3: Late-Minor; 4: Late-Major',
  `end_result_id` int(11) NOT NULL COMMENT '1: Ontime; 2: Late-Minor; 3: Late-Major',
  `duration_result_id` int(11) NOT NULL COMMENT '1: Under Duration; 2: On Plan; 3: Over Duration',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


