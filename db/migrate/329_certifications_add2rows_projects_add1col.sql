

Insert Into certifications(`id`,`name`,`label`,`description`,`hide`)
values(20,'LMSPreferred','LMS Preferred Technician','LMS Preferred Technician',0);

Insert Into certifications(`id`,`name`,`label`,`description`,`hide`)
values(21,'LMSPlusKey','LMS+ Key Contractor','LMS+ Key Contractor',0);

ALTER TABLE `projects`  ADD `CredentialCertificationId` int NULL DEFAULT NULL;

ALTER TABLE `work_orders`  ADD `CredentialCertificationId` int NULL DEFAULT NULL;

