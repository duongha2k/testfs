-- phpMyAdmin SQL Dump
-- version 3.3.2deb1
-- http://www.phpmyadmin.net
--
-- Host: saturntestrestore2.cd3kywrhh0ei.us-east-1.rds.amazonaws.com
-- Generation Time: Aug 15, 2013 at 05:28 AM
-- Server version: 5.1.63
-- PHP Version: 5.3.2-1ubuntu4.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `saturntest`
--

-- --------------------------------------------------------

--
-- Table structure for table `custom_reports_filters`
--

CREATE TABLE IF NOT EXISTS `custom_reports_filters` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rep_id` int(11) NOT NULL,
  `fieldcode` varchar(255) NOT NULL,
  `filter_at` int(11) NOT NULL COMMENT '1:template, 2:run time, 3:run time with default value',
  `datetime_relative_data_ref` varchar(255) DEFAULT NULL COMMENT 'AsEntered, RunTimeDate, CurrentYear, LastYear, CurrentQuater, LastQuarter',
  `datetime_range_from` datetime DEFAULT NULL,
  `datetime_range_to` datetime DEFAULT NULL,
  `string_matching` varchar(255) DEFAULT NULL COMMENT 'ExactMatch, BeginsWith, EndsWith, Contains',
  `string_value` varchar(255) DEFAULT NULL,
  `bool_value` varchar(1) DEFAULT NULL COMMENT 't:true, f:false, a:any',
  `numeric_base_value` decimal(10,2) DEFAULT NULL,
  `numeric_range_from` decimal(10,2) DEFAULT NULL,
  `numeric_range_to` decimal(10,2) DEFAULT NULL,
  `datetime_range_int_from` int(11) DEFAULT NULL,
  `datetime_range_int_to` int(11) DEFAULT NULL,
  `time_from` varchar(20) DEFAULT NULL,
  `time_to` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
