-- phpMyAdmin SQL Dump
-- version 2.11.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 13, 2010 at 09:54 AM
-- Server version: 5.0.91
-- PHP Version: 5.2.14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `fs_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `client_denied_techs`
--

CREATE TABLE IF NOT EXISTS `client_denied_techs` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `TechID` int(10) unsigned default NULL,
  `ClientID` int(10) unsigned default NULL,
  `Comments` varchar(255) default NULL,
  `Company_ID` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `TechID` (`TechID`),
  KEY `Company_ID` (`Company_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3046 ;
