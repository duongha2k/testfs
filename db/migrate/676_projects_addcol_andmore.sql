
-- projects
ALTER TABLE projects ADD COLUMN EnableSiteStatusReporting tinyint DEFAULT 0;

-- work_order_tagitem_values
ALTER TABLE work_order_tagitem_values ADD COLUMN DateAdded datetime;

-- sites
ALTER TABLE sites ADD COLUMN `Status_ID` int DEFAULT 1;
ALTER TABLE sites ADD COLUMN `StartDate` datetime;
ALTER TABLE sites ADD COLUMN `CompleteDate` datetime;
ALTER TABLE sites ADD COLUMN `CanceledDate` datetime;
ALTER TABLE sites ADD COLUMN `RescheduledDate` datetime;

-- wo_tag_items
ALTER TABLE wo_tag_items ADD COLUMN item_sortdesc VARCHAR(50);
ALTER TABLE wo_tag_items ADD COLUMN item_owner VARCHAR(50);
ALTER TABLE wo_tag_items ADD COLUMN item_details VARCHAR(50);

update wo_tag_items set item_sortdesc='Complete',item_owner='Technician',item_details='1st Visit' where wo_tag_item_code='Complete1stVisit';
update wo_tag_items set item_sortdesc='Complete',item_owner='Technician',item_details='Revisit' where wo_tag_item_code='CompleteRevisit';
update wo_tag_items set item_sortdesc='Complete',item_owner='Technician',item_details='Reassigned' where wo_tag_item_code='CompleteReassigned';
update wo_tag_items set item_sortdesc='Complete',item_owner='Technician',item_details='Rescheduled' where wo_tag_item_code='CompleteRescheduled';
update wo_tag_items set item_sortdesc='Incomplete',item_owner='Site / Client',item_details='Site Turn-Away / Abort' where wo_tag_item_code='IncompleteSiteOrClientDriven_SiteTurnAwayOrAbort';
update wo_tag_items set item_sortdesc='Incomplete',item_owner='Site / Client',item_details='End-User Issue' where wo_tag_item_code='IncompleteSiteOrClientDriven_EndUserIssue';
update wo_tag_items set item_sortdesc='Incomplete',item_owner='Site / Client',item_details='Hardware Related' where wo_tag_item_code='IncompleteSiteOrClientDriven_HardwareRelated';
update wo_tag_items set item_sortdesc='Incomplete',item_owner='Site / Client',item_details='Out of Scope Work' where wo_tag_item_code='IncompleteSiteOrClientDriven_OutofScopeWork';
update wo_tag_items set item_sortdesc='Incomplete',item_owner='Technician',item_details='Delayed Arrival' where wo_tag_item_code='IncompleteTechDriven_DelayedArrival';
update wo_tag_items set item_sortdesc='Incomplete',item_owner='Technician',item_details='Performance Issue' where wo_tag_item_code='IncompleteTechDriven_PerformanceIssue';
update wo_tag_items set item_sortdesc='Incomplete',item_owner='Technician',item_details='Internal Issue' where wo_tag_item_code='IncompleteTechDriven_InternalIssue';
update wo_tag_items set item_sortdesc='Incomplete',item_owner='Force Majeure',item_details='' where wo_tag_item_code='Incomplete_NaturalCausesOrForceMajeure';
update wo_tag_items set item_sortdesc='Change / Cancel',item_owner='Site / Client',item_details='' where wo_tag_item_code='ChangeOrCancelSiteOrClientDriven';
update wo_tag_items set item_sortdesc='Change / Cancel',item_owner='Technician',item_details='No Show' where wo_tag_item_code='ChangeOrCancelTechDriven_NoShow';
update wo_tag_items set item_sortdesc='Change / Cancel',item_owner='Technician',item_details='Back Out' where wo_tag_item_code='ChangeOrCancelTechDriven_BackOut';
update wo_tag_items set item_sortdesc='Change / Cancel',item_owner='Internal Issue',item_details='' where wo_tag_item_code='ChangeOrCancelInternalIssue';

INSERT INTO wo_tag_items (id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active,item_sortdesc,item_owner,item_details)
VALUES(42,'CompletionStatus','Scheduled','Scheduled',1,1,'Scheduled','Site / Client','');



