-- phpMyAdmin SQL Dump
-- version 2.11.10.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 13, 2010 at 09:38 AM
-- Server version: 5.0.91
-- PHP Version: 5.2.14

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `fs_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `StaffID` int(10) unsigned NOT NULL auto_increment,
  `Fname` varchar(255) default NULL,
  `Lname` varchar(255) default NULL,
  `UserName` varchar(255) default NULL,
  `Password` varchar(255) default NULL,
  `PrimaryPhone` varchar(255) default NULL,
  `CellPhone` varchar(255) default NULL,
  `OtherPhone` varchar(255) default NULL,
  `Fax` varchar(255) default NULL,
  `PrimaryEmail` varchar(255) default NULL,
  `SecondaryEmail` varchar(255) default NULL,
  `AIM_BuddyName` varchar(255) default NULL,
  `Active` tinyint(1) NOT NULL default '0',
  `ScheduledTimeOff` longtext,
  PRIMARY KEY  (`StaffID`),
  KEY `UserName` (`UserName`),
  KEY `Password` (`Password`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=73 ;
