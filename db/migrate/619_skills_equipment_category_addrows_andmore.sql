-- skills
insert into skills(id,`name`,label)
values(45,'CableTVTelcoCLECExperience','Do you have Cable TV Co., Telco, and/or CLEC work experience?');

insert into skills(id,`name`,label)
values(46,'AdvCableLacStitchSkills','Do you have advanced cable skills (including lacing and stitching)?');

insert into skills(id,`name`,label)
values(47,'BatteryDistributionFuseBay','Have you ever worked in a Battery Distribution Fuse Bay?');

insert into skills(id,`name`,label)
values(48,'ReadingMOPsExperience','Do you have experience reading MOPs (Methods of Procedures)?');

-- equipment
insert into equipment(id,`name`,label,description)
values(34,'LacingNeedle','Lacing Needle','Lacing Needle'); 

insert into equipment(id,`name`,label,description)
values(35,'WireWrapGun','Wire Wrap Gun','Wire Wrap Gun'); 

-- wo_category
insert into wo_categories(Category_ID,Category,Abbr,TechSelfRatingColumn)
values(35,'Central Office Cabling','CentralOfficeCabling','CentralOfficeCablingSelfRating');

-- TechBankInfo
ALTER TABLE TechBankInfo ADD COLUMN CentralOfficeCablingSelfRating int;

