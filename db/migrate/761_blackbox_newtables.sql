
-- BlackBoxCabling_Test
DROP TABLE IF EXISTS `BlackBoxCabling_Test`;

CREATE TABLE `BlackBoxCabling_Test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tech_id` int(11) NOT NULL,
  `date_taken` datetime NOT NULL,
  `Level` int(11) NOT NULL DEFAULT '0',
  `C1RT_1` tinyint(4) NOT NULL DEFAULT '0',
  `C1RT_2` tinyint(4) NOT NULL DEFAULT '0',
  `C1RT_3` tinyint(4) NOT NULL DEFAULT '0',
  `C1RT_4` tinyint(4) NOT NULL DEFAULT '0',
  `C1RT_5` tinyint(4) NOT NULL DEFAULT '0',

  `C1RE_1` tinyint(4) NOT NULL DEFAULT '0',
  `C1RE_2` tinyint(4) NOT NULL DEFAULT '0',

  `C2RT_1` tinyint(4) NOT NULL DEFAULT '0',
  `C2RT_2` tinyint(4) NOT NULL DEFAULT '0',
  `C2RT_3` tinyint(4) NOT NULL DEFAULT '0',
  `C2RT_4` tinyint(4) NOT NULL DEFAULT '0',
  `C2RT_5` tinyint(4) NOT NULL DEFAULT '0',
  `C2RT_6` tinyint(4) NOT NULL DEFAULT '0',
  `C2RT_7` tinyint(4) NOT NULL DEFAULT '0',
  `C2RT_8` tinyint(4) NOT NULL DEFAULT '0',
  `C2RT_9` tinyint(4) NOT NULL DEFAULT '0',
  `C2RT_10` tinyint(4) NOT NULL DEFAULT '0',
  `C2RT_11` tinyint(4) NOT NULL DEFAULT '0',

  `C2RE_1` tinyint(4) NOT NULL DEFAULT '0',
  `C2RE_2` tinyint(4) NOT NULL DEFAULT '0',
  `C2RE_3` tinyint(4) NOT NULL DEFAULT '0',
  `C2RE_4` tinyint(4) NOT NULL DEFAULT '0',

  `C3RT_1` tinyint(4) NOT NULL DEFAULT '0',
  `C3RT_2` tinyint(4) NOT NULL DEFAULT '0',
  `C3RT_3` tinyint(4) NOT NULL DEFAULT '0',
  `C3RT_4` tinyint(4) NOT NULL DEFAULT '0',

  `C3RE_1` tinyint(4) NOT NULL DEFAULT '0',
  `C3RE_2` tinyint(4) NOT NULL DEFAULT '0',
  `C3RE_3` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- BlackBoxCabling_Test
DROP TABLE IF EXISTS `BlackBoxTelephony_Test`;

CREATE TABLE `BlackBoxTelephony_Test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tech_id` int(11) NOT NULL,
  `date_taken` datetime NOT NULL,
  `Level` int(11) NOT NULL DEFAULT '0',

  `T1RT_1` tinyint(4) NOT NULL DEFAULT '0',
  `T1RT_2` tinyint(4) NOT NULL DEFAULT '0',
  `T1RT_3` tinyint(4) NOT NULL DEFAULT '0',
  `T1RT_4` tinyint(4) NOT NULL DEFAULT '0',
  `T1RT_5` tinyint(4) NOT NULL DEFAULT '0',

  `T1RE_1` tinyint(4) NOT NULL DEFAULT '0',
  `T1RE_2` tinyint(4) NOT NULL DEFAULT '0',
  `T1RE_3` tinyint(4) NOT NULL DEFAULT '0',
  `T1RE_4` tinyint(4) NOT NULL DEFAULT '0',
  `T1RE_5` tinyint(4) NOT NULL DEFAULT '0',
  `T1RE_6` tinyint(4) NOT NULL DEFAULT '0',

  `T2RT_1` tinyint(4) NOT NULL DEFAULT '0',
  `T2RT_2` tinyint(4) NOT NULL DEFAULT '0',
  `T2RT_3` tinyint(4) NOT NULL DEFAULT '0',
  `T2RT_4` tinyint(4) NOT NULL DEFAULT '0',
  `T2RT_5` tinyint(4) NOT NULL DEFAULT '0',
  `T2RT_6` tinyint(4) NOT NULL DEFAULT '0',
  `T2RT_7` tinyint(4) NOT NULL DEFAULT '0',
  `T2RT_8` tinyint(4) NOT NULL DEFAULT '0',
  `T2RT_9` tinyint(4) NOT NULL DEFAULT '0',
  `T2RT_10` tinyint(4) NOT NULL DEFAULT '0',
  `T2RT_11` tinyint(4) NOT NULL DEFAULT '0',
  `T2RT_12` tinyint(4) NOT NULL DEFAULT '0',

  `T2RE_1` tinyint(4) NOT NULL DEFAULT '0',
  `T2RE_2` tinyint(4) NOT NULL DEFAULT '0',  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- certifications
INSERT INTO certifications (id,`name`,label,description,hide,groupid)
VALUES (46,'BlackBoxCablingHelper','Helper (C1)','Black Box Cabling – Helper (C1)',0,'BlackBoxCabling');
INSERT INTO certifications (id,`name`,label,description,hide,groupid)
VALUES (47,'BlackBoxCablingAdvanced','Advanced (C2)','Black Box Cabling – Advanced (C2)',0,'BlackBoxCabling');
INSERT INTO certifications (id,`name`,label,description,hide,groupid)
VALUES (48,'BlackBoxCablingLead','Lead (C3)','Black Box Cabling – Lead (C3)',0,'BlackBoxCabling');

INSERT INTO certifications (id,`name`,label,description,hide,groupid)
VALUES (49,'BlackBoxTelecomHelper','Helper (T1)','Black Box Telecom – Helper (T1)',0,'BlackBoxTelecom');
INSERT INTO certifications (id,`name`,label,description,hide,groupid)
VALUES (50,'BlackBoxTelecomAdvanced','Advanced (T2)','Black Box Telecom – Advanced (T2)',0,'BlackBoxTelecom');

-- equipment for 761 and 757
INSERT INTO equipment(id,`name`,label,description)
VALUES(36,'HandTools','Hand Tools','Hand Tools');
INSERT INTO equipment(id,`name`,label,description)
VALUES(37,'CordlessDrill','Cordless Drill','Cordless Drill');
INSERT INTO equipment(id,`name`,label,description)
VALUES(38,'ProtectionEquipment','Personal Protection Equipment','Personal Protection Equipment (Gloves, Hard Hat, Boots)');
INSERT INTO equipment(id,`name`,label,description)
VALUES(39,'MachineLabelMaker','Machine Label Maker','Machine Label Maker (P-Touch, Bradley, etc.)');
INSERT INTO equipment(id,`name`,label,description)
VALUES(40,'GoferPoles','Gofer Poles','Gofer Poles (or Glow Rods)');
INSERT INTO equipment(id,`name`,label,description)
VALUES(41,'FiberOpticdBLossMeter','Fiber Optic dB Loss Meter & Light Source','Fiber Optic dB Loss Meter & Light Source');
INSERT INTO equipment(id,`name`,label,description)
VALUES(42,'FiberOpticTerminationKit','Fiber Optic Termination Kit','Fiber Optic Termination Kit');

-- skills for 761 and 757
INSERT INTO skills(id,`name`,label)
VALUES(49,'OperationScissorLiftExperience','Do you know how to operate a scissor lift and have extensive experience using a ladder?');
INSERT INTO skills(id,`name`,label)
VALUES(50,'UnderstandTelecomColorCodeExperience','Do you understand the telecom color code and have experience with termination jacks and patch panels?');
INSERT INTO skills(id,`name`,label)
VALUES(51,'InstallationMultipleCableExperience','Do you have experience with multiple cable installation methods (installing in conduit using fish tape, pull string, or shop vac)?');
INSERT INTO skills(id,`name`,label)
VALUES(52,'InstallingSleevesRacewaysExperience','Do you have experience installing sleeves firestops, J-Hooks, Panduit, and Wire Mold raceways?');
INSERT INTO skills(id,`name`,label)
VALUES(53,'InstallationVoiceDataEquipmentExperience','Do you have experience with voice and data equipment room installation and build out (ex: data cabinets and racks, plywood, 66/110 blocks, patch panels, etc)?');
INSERT INTO skills(id,`name`,label)
VALUES(54,'SingleAndMultiModeTerminationTestingExp','Do you have experience in single-mode and multi-mode termination and testing?');
INSERT INTO skills(id,`name`,label)
VALUES(55,'UsingLaptopForSwitchConfigurationExp','Do you have experience using a laptop for basic switch configuration (HyperTerminal, Terra Term, etc.)?');
INSERT INTO skills(id,`name`,label)
VALUES(56,'DMARCRoomsEquipmentExperience','Do you have experience with DMARC rooms and equipment such as Rj-21X, Smart jacks, lightning protectors, and cross connect fields?');
INSERT INTO skills(id,`name`,label)
VALUES(57,'OperationPBXExperience','Do you have experience with basic PBX / Key system operation (ex: general programming, station card and CO line card replacement, station / extension assignment)?');



