
ALTER TABLE `custom_reports` 
ADD COLUMN `RRDDailyOptions` INT(11) NULL DEFAULT '1'  AFTER `report_type` , 
ADD COLUMN `RRDDailyEveryDaySpaceOutNum` INT(11) NULL DEFAULT '1'  AFTER `RRDDailyOptions` , 
ADD COLUMN `RRDDailyEveryWeekday` INT(11) NULL DEFAULT '1' COMMENT '1 for Mon, 2 for Tue,...,7 for Sun'  AFTER `RRDDailyEveryDaySpaceOutNum` , 
ADD COLUMN `RRDWeeklySpaceOutNum` INT(11) NULL DEFAULT '1'  AFTER `RRDDailyEveryWeekday` , 
ADD COLUMN `RRDMonthlyDayNumSpaceOutNum` INT(11) NULL DEFAULT '1'  AFTER `RRDWeeklySpaceOutNum` , 
ADD COLUMN `RRDMonthlyDayOrderSpaceOutNum` INT(11) NULL DEFAULT '1'  AFTER `RRDMonthlyDayNumSpaceOutNum` ;

