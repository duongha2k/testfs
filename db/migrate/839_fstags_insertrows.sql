
-- fstags
insert into `fstags` (`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`AddedDate`,`TagArt`) 
values (35,'Black Box Cabling',1,'system',1,'BlackBoxCabling','SpecialCase','2013-01-26 00:00:00',NULL);

insert into `fstags` (`id`,`Title`,`VisibilityId`,`AddedByUser`,`Active`,`Name`,`Source`,`AddedDate`,`TagArt`) 
values (36,'Black Box Telecom',1,'system',1,'BlackBoxTelecom','SpecialCase','2013-01-26 00:00:00',NULL);

-- fstag_clients
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (35,'BBAL');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (35,'BBGC');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (35,'BBGE');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (35,'BBCN');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (35,'BCSST');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (35,'BBGN');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (35,'BBJC');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (35,'BBLC');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (35,'BBNSG');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (35,'BBNC');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (35,'BBS');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (35,'BB');

insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (36,'BBAL');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (36,'BBGC');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (36,'BBGE');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (36,'BBCN');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (36,'BCSST');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (36,'BBGN');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (36,'BBJC');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (36,'BBLC');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (36,'BBNSG');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (36,'BBNC');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (36,'BBS');
insert  into `fstag_clients`(`FSTagId`,`CompanyId`) values (36,'BB');

