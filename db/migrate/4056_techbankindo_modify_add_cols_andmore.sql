

-- modify columns
ALTER TABLE TechBankInfo MODIFY COLUMN NACI int;
ALTER TABLE TechBankInfo MODIFY COLUMN FullSecClear int;
ALTER TABLE TechBankInfo MODIFY COLUMN TopSecClear int;
ALTER TABLE TechBankInfo MODIFY COLUMN InterimSecClear int;

-- add new columns
ALTER TABLE TechBankInfo ADD COLUMN TopSecSCIClear int DEFAULT 0;
ALTER TABLE TechBankInfo ADD COLUMN NACIDate datetime;
ALTER TABLE TechBankInfo ADD COLUMN TopSecSCIClearDate datetime;

-- data for NACI,NACIDate column
UPDATE TechBankInfo SET NACI=1 WHERE TechID IN (SELECT TechID FROM tech_certification WHERE certification_id=6);
UPDATE TechBankInfo SET NACIDate=(SELECT `date` FROM tech_certification WHERE certification_id=6 AND TechID=TechBankInfo.TechID GROUP BY TechID) WHERE TechID IN (SELECT TechID FROM tech_certification WHERE certification_id=6);

-- update ALL existing clearances from Yes to Lapsed 
UPDATE TechBankInfo SET NACI=2 WHERE NACI=1;
UPDATE TechBankInfo SET FullSecClear=2 WHERE FullSecClear=1;
UPDATE TechBankInfo SET TopSecClear=2 WHERE TopSecClear=1;
UPDATE TechBankInfo SET InterimSecClear=2 WHERE InterimSecClear=1;
UPDATE TechBankInfo SET TopSecSCIClearDate=2 WHERE TopSecSCIClearDate=1;




