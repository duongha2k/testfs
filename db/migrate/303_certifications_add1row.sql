

Insert Into certifications(`id`,`name`,`label`,`description`,`hide`)
values(18,'VonagePlus','Vonage Plus','Vonage Plus',0)


CREATE TABLE IF NOT EXISTS `project_certification` (
  `Project_ID` int(11) NOT NULL,
  `certification_id` int(11) NOT NULL,
  PRIMARY KEY (`Project_ID`,`certification_id`),
  KEY `certification_id` (`certification_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `project_certification`
  ADD CONSTRAINT `project_certification_ibfk_1` FOREIGN KEY (`certification_id`) REFERENCES `certifications` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

CREATE TABLE IF NOT EXISTS `work_order_certification` (
  `WIN_NUM` int(11) NOT NULL,
  `certification_id` int(11) NOT NULL,
  PRIMARY KEY (`WIN_NUM`,`certification_id`),
  KEY `certification_id` (`certification_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `work_order_certification`
  ADD CONSTRAINT `work_order_certification_ibfk_1` FOREIGN KEY (`certification_id`) REFERENCES `certifications` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;