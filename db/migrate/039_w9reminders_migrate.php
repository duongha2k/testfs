<?php
require_once dirname(__FILE__).'/../../includes/modules/common.init.php';

$fieldList = array('id', 'TechID', 'EmailsSent', 'DateLastSent');
$fieldListMap = array_combine($fieldList, $fieldList);

$w9Reminders = Core_Caspio::caspioSelectWithFieldListMap("W9Reminders", $fieldListMap, '', "");

$db = Core_Database::getInstance();

foreach ($w9Reminders as $reminder) {
    $select = $db->select();
    $select->from("W9Reminders", $fieldList)
           ->where('id = ?', $reminder['id']);

    $result = $db->fetchRow($select);     
    
    if (empty($result)) {
        $toInsert = $reminder;
        $date = new Zend_Date($toInsert['DateLastSent'], 'MM/dd/YYYY hh:mm:ss a');
        $toInsert['DateLastSent'] = $date->toString('YYYY-MM-dd HH:mm:ss');
        $db->insert('W9Reminders', $toInsert);
        print 'Inserting: ' . $toInsert['id'] . "\n";
    }
}
