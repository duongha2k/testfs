
/* Table structure for table `fstag_levels` */

DROP TABLE IF EXISTS `fstag_levels`;

CREATE TABLE `fstag_levels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fstag_id` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `TagArt` varchar(255) NOT NULL,
  `DisplayOrder` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

/* fstags */
ALTER TABLE fstags ADD COLUMN HasMultiTagLevels tinyint(1) DEFAULT 0;

/* fstag_techs */
ALTER TABLE fstag_techs ADD COLUMN FSTagLevelId int;
