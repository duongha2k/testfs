
-- clicompanies_bcats
DELETE FROM clicompanies_bcats WHERE bcatname='Gasoline';

UPDATE clicompanies_bcats SET bcatname = 'Airfare / Train Tickets' WHERE bcatname = 'Airfare';
UPDATE clicompanies_bcats SET bcatname = 'Rental Vehicle / Gas' WHERE bcatname = 'Car Rental';
UPDATE clicompanies_bcats SET bcatname = 'Hotel / Lodging' WHERE bcatname = 'Hotel';
UPDATE clicompanies_bcats SET bcatname = 'Materials / Supplies' WHERE bcatname = 'Miscellaneous Supplies';
UPDATE clicompanies_bcats SET bcatname = 'Parking / Tolls' WHERE bcatname = 'Parking';
UPDATE clicompanies_bcats SET bcatname = 'Postage / Shipping / Freight' WHERE bcatname = 'Postage / Freight';
UPDATE clicompanies_bcats SET bcatname = 'Bus / Taxi / Subway' WHERE bcatname = 'Train/Taxi/Shuttle/Tolls';

INSERT INTO clicompanies_bcats(companyid,bcatname,bcatdesc,ischecked,reimbursable_thru_fs,require_doc_upl,bcattype)
SELECT cbcat.companyid, 'Meals','',0,0,0,'fixed'
FROM clicompanies_bcats cbcat
WHERE 'Meals' NOT IN (SELECT bcatname FROM clicompanies_bcats WHERE companyid=cbcat.companyid)
GROUP BY cbcat.companyid;

INSERT INTO clicompanies_bcats(companyid,bcatname,bcatdesc,ischecked,reimbursable_thru_fs,require_doc_upl,bcattype)
SELECT cbcat.companyid, 'Entertainment','',0,0,0,'fixed'
FROM clicompanies_bcats cbcat
WHERE 'Entertainment' NOT IN (SELECT bcatname FROM clicompanies_bcats WHERE companyid=cbcat.companyid)
GROUP BY cbcat.companyid;

INSERT INTO clicompanies_bcats(companyid,bcatname,bcatdesc,ischecked,reimbursable_thru_fs,require_doc_upl,bcattype)
SELECT cbcat.companyid, 'Miscellaneous','',0,0,0,'fixed'
FROM clicompanies_bcats cbcat
WHERE 'Miscellaneous' NOT IN (SELECT bcatname FROM clicompanies_bcats WHERE companyid=cbcat.companyid)
GROUP BY cbcat.companyid;

-- projects_bcats
DELETE FROM projects_bcats WHERE bcatname='Gasoline';

UPDATE projects_bcats SET bcatname = 'Airfare / Train Tickets' WHERE bcatname = 'Airfare';
UPDATE projects_bcats SET bcatname = 'Rental Vehicle / Gas' WHERE bcatname = 'Car Rental';
UPDATE projects_bcats SET bcatname = 'Hotel / Lodging' WHERE bcatname = 'Hotel';
UPDATE projects_bcats SET bcatname = 'Materials / Supplies' WHERE bcatname = 'Miscellaneous Supplies';
UPDATE projects_bcats SET bcatname = 'Parking / Tolls' WHERE bcatname = 'Parking';
UPDATE projects_bcats SET bcatname = 'Postage / Shipping / Freight' WHERE bcatname = 'Postage / Freight';
UPDATE projects_bcats SET bcatname = 'Bus / Taxi / Subway' WHERE bcatname = 'Train/Taxi/Shuttle/Tolls';

INSERT INTO projects_bcats(projectid,bcatname,bcatdesc,ischecked,reimbursable_thru_fs,require_doc_upl,bcattype)
SELECT pbcat.projectid, 'Meals','',0,0,0,'fixed'
FROM projects_bcats pbcat
WHERE 'Meals' NOT IN (SELECT bcatname FROM projects_bcats WHERE projectid=pbcat.projectid)
GROUP BY pbcat.projectid;

INSERT INTO projects_bcats(projectid,bcatname,bcatdesc,ischecked,reimbursable_thru_fs,require_doc_upl,bcattype)
SELECT pbcat.projectid, 'Entertainment','',0,0,0,'fixed'
FROM projects_bcats pbcat
WHERE 'Entertainment' NOT IN (SELECT bcatname FROM projects_bcats WHERE projectid=pbcat.projectid)
GROUP BY pbcat.projectid;

INSERT INTO projects_bcats(projectid,bcatname,bcatdesc,ischecked,reimbursable_thru_fs,require_doc_upl,bcattype)
SELECT pbcat.projectid, 'Miscellaneous','',0,0,0,'fixed'
FROM projects_bcats pbcat
WHERE 'Miscellaneous' NOT IN (SELECT bcatname FROM projects_bcats WHERE projectid=pbcat.projectid)
GROUP BY pbcat.projectid;

-- work_orders_bcats
DELETE FROM work_orders_bcats WHERE bcatname='Gasoline';

UPDATE work_orders_bcats SET bcatname = 'Airfare / Train Tickets' WHERE bcatname = 'Airfare';
UPDATE work_orders_bcats SET bcatname = 'Rental Vehicle / Gas' WHERE bcatname = 'Car Rental';
UPDATE work_orders_bcats SET bcatname = 'Hotel / Lodging' WHERE bcatname = 'Hotel';
UPDATE work_orders_bcats SET bcatname = 'Materials / Supplies' WHERE bcatname = 'Miscellaneous Supplies';
UPDATE work_orders_bcats SET bcatname = 'Parking / Tolls' WHERE bcatname = 'Parking';
UPDATE work_orders_bcats SET bcatname = 'Postage / Shipping / Freight' WHERE bcatname = 'Postage / Freight';
UPDATE work_orders_bcats SET bcatname = 'Bus / Taxi / Subway' WHERE bcatname = 'Train/Taxi/Shuttle/Tolls';







