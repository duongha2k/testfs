/*
SQLyog Enterprise - MySQL GUI v7.14 
MySQL - 5.1.33-community : Database - saturn
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*Table structure for table `gge_test` */

DROP TABLE IF EXISTS `gge_test`;

CREATE TABLE `gge_test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tech_id` int(11) DEFAULT NULL,
  `testtimes` int(11) NOT NULL,
  `date_taken` datetime DEFAULT NULL,
  `a1_answer` varchar(255) DEFAULT NULL,
  `a2_answer` varchar(255) DEFAULT NULL,
  `a3_answer` varchar(255) DEFAULT NULL,
  `a4_answer` varchar(255) DEFAULT NULL,
  `a5_answer` varchar(255) DEFAULT NULL,
  `a6_answer` varchar(255) DEFAULT NULL,
  `a7_answer` varchar(255) DEFAULT NULL,
  `a8_answer` varchar(255) DEFAULT NULL,
  `a9_answer` varchar(255) DEFAULT NULL,
  `a10_answer` varchar(255) DEFAULT NULL,
  `a11_answer` varchar(255) DEFAULT NULL,
  `a12_answer` varchar(255) DEFAULT NULL,
  `a13_answer` varchar(255) DEFAULT NULL,
  `a14_answer` varchar(255) DEFAULT NULL,
  `a15_answer` varchar(255) DEFAULT NULL,
  `a16_answer` varchar(255) DEFAULT NULL,
  `a17_answer` varchar(255) DEFAULT NULL,
  `a18_answer` varchar(255) DEFAULT NULL,
  `a19_answer` varchar(255) DEFAULT NULL,
  `a20_answer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `gge_test` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;

ALTER TABLE tech_ext ADD COLUMN GGE1stAttemptStatus VARCHAR(50);
ALTER TABLE tech_ext ADD COLUMN GGE1stAttemptScore INT DEFAULT 0;
ALTER TABLE tech_ext ADD COLUMN GGE2ndAttemptStatus VARCHAR(50);
ALTER TABLE tech_ext ADD COLUMN GGE2ndAttemptScore INT DEFAULT 0;

INSERT INTO certifications (id,`name`,label,description,hide) 
VALUES (29,'GGETechEvaluation','GGE Tech Evaluation','GGE Tech Evaluation',0);





