
ALTER TABLE clients_ext ADD COLUMN AllowDeVryInternstoObserveWork tinyint DEFAULT 0;
ALTER TABLE clients_ext ADD COLUMN AllowDeVryInternstoPerformField tinyint DEFAULT 0;

ALTER TABLE projects ADD COLUMN AllowDeVryInternstoObserveWork tinyint;
ALTER TABLE projects ADD COLUMN AllowDeVryInternstoPerformField tinyint;

ALTER TABLE work_orders_additional_fields ADD COLUMN AllowDeVryInternstoObserveWork tinyint;
ALTER TABLE work_orders_additional_fields ADD COLUMN AllowDeVryInternstoPerformField tinyint;





