<?php
require_once dirname(__FILE__).'/../../includes/modules/common.init.php';

$fieldList = array("id", "Tech_ID", "PreferLevel", "CompanyID");
$fieldListMap = array_combine($fieldList, $fieldList);

$maxId = 340000;
$step = 1000;

$startIndex = 0;
$db = Core_Database::getInstance();

while($startIndex < $maxId) {
    
    $stopIndex =  $startIndex + $step;

    $techs = Core_Caspio::caspioSelectWithFieldListMap(Core_Database::TABLE_CLIENT_PREFERRED_TECHS, $fieldListMap, 'id >= ' .(int)$startIndex . ' AND id < ' . (int)$stopIndex, "");
    
    if (is_array($techs) ) {
        foreach ($techs as $tech) {
            
            $select = $db->select();
            $select->from(Core_Database::TABLE_CLIENT_PREFERRED_TECHS, $fieldList)
                   ->where('id = ?', (int)$tech['id']);
        
            $result = $db->fetchRow($select);     
            
            if (empty($result)) {
                $toInsert = $tech;
                //if(empty($toInsert['WorkOrder_ID']) || $toInsert['WorkOrder_ID']==0) continue;
                foreach ($toInsert as &$value) {
                    if ($value === 'True') {
                        $value = 1;
                    } elseif ($value === 'False') {
                        $value = 0;
                    }
                }
                
               // $date = new Zend_Date($toInsert['Created_Date'], 'MM/dd/YYYY hh:mm:ss a');
              //  $toInsert['Created_Date'] = $date->toString('YYYY-MM-dd HH:mm:ss');
                try {
                    $db->insert(Core_Database::TABLE_CLIENT_PREFERRED_TECHS, $toInsert);
                } catch (Zend_Exception $e) {
                    print "Error";
                    print_r($toInsert);
                    print_r($e->getMessage());
                    if(strpos($e->getMessage(), "1452")){
                    	continue;
                    }else{
                    	exit;
                    }
                }
                print 'Inserting: ' . $toInsert['id'] . "\n";
            } else {
                print 'Skip: ' . $reminder['id'] . "\n";
            }
        }
    }
    
    $startIndex = $stopIndex;
}
