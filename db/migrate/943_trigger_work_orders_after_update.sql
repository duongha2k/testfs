DELIMITER $$

DROP TRIGGER /*!50032 IF EXISTS */ `work_orders_after_update`$$

CREATE
    TRIGGER `work_orders_after_update` AFTER UPDATE ON `work_orders` 
    FOR EACH ROW BEGIN
	IF (NEW.Tech_ID <=> OLD.Tech_ID AND NEW.WO_Category_ID <=> OLD.WO_Category_ID AND NEW.Approved <=> OLD.Approved AND NEW.Type_ID <=> OLD.Type_ID) THEN
		SET @DONOTHING = 888;
	ELSE		
		IF OLD.Tech_ID > 0 AND OLD.Approved = 1 THEN
			UPDATE tech_wo_category_count SET completed = completed - 1 WHERE Tech_ID = OLD.Tech_ID AND WO_Category_ID = OLD.WO_Category_ID;
			UPDATE TechBankInfo SET 
				Qty_IMAC_Calls = Qty_IMAC_Calls - (CASE WHEN OLD.TYPE_ID = 1 THEN 1 ELSE 0 END),
				Qty_FLS_Service_Calls = Qty_FLS_Service_Calls - (CASE WHEN OLD.TYPE_ID = 2 THEN 1 ELSE 0 END)
			WHERE TechID = OLD.Tech_ID;
		END IF;
		IF NEW.Tech_ID > 0 AND NEW.Approved = 1 THEN
			INSERT INTO tech_wo_category_count (Tech_ID, WO_Category_ID, completed) VALUES (NEW.Tech_ID, NEW.WO_Category_ID, 1) ON DUPLICATE KEY UPDATE completed = completed + 1;
			UPDATE TechBankInfo SET 
				Qty_IMAC_Calls = Qty_IMAC_Calls + (CASE WHEN NEW.TYPE_ID = 1 THEN 1 ELSE 0 END),
				Qty_FLS_Service_Calls = Qty_FLS_Service_Calls + (CASE WHEN NEW.TYPE_ID = 2 THEN 1 ELSE 0 END)
			WHERE TechID = NEW.Tech_ID;
		END IF;
	END IF;
	IF (NEW.BackOut_Tech <=> OLD.BackOut_Tech) THEN
		SET @DONOTHING = 888;
	ELSE		
		IF OLD.BackOut_Tech > 0 THEN
			UPDATE TechBankInfo SET 
				Back_Outs = Back_Outs - 1
			WHERE TechID = OLD.BackOut_Tech;
		END IF;
		IF NEW.BackOut_Tech > 0 THEN
			UPDATE TechBankInfo SET 
				Back_Outs = Back_Outs + 1
			WHERE TechID = NEW.BackOut_Tech;
		END IF;
	END IF;
	IF (NEW.NoShow_Tech <=> OLD.NoShow_Tech) THEN
		SET @DONOTHING = 888;
	ELSE		
		IF OLD.NoShow_Tech > 0 THEN
			UPDATE TechBankInfo SET 
				No_Shows = No_Shows - 1
			WHERE TechID = OLD.NoShow_Tech;
		END IF;
		IF NEW.NoShow_Tech > 0 THEN
			UPDATE TechBankInfo SET 
				No_Shows = No_Shows + 1
			WHERE TechID = NEW.NoShow_Tech;
		END IF;
	END IF;
	
	-- for 13943
	IF ((NEW.Tech_ID <> OLD.Tech_ID OR NEW.Approved <> OLD.Approved) AND (NEW.Type_ID=1 OR NEW.Type_ID=2) AND (OLD.Type_ID=1 OR OLD.Type_ID=2)) THEN
		IF OLD.Tech_ID > 0 AND OLD.Approved = 1 THEN
			UPDATE  tech_company_wo_count SET TotalWosCompany = TotalWosCompany - 1 WHERE Tech_ID = OLD.Tech_ID AND Company_ID = OLD.Company_ID;
		END IF;
		IF NEW.Tech_ID > 0 AND NEW.Approved = 1 THEN
			INSERT INTO tech_company_wo_count (Tech_ID, Company_ID, TotalWosCompany) VALUES (NEW.Tech_ID, NEW.Company_ID, 1) ON DUPLICATE KEY UPDATE TotalWosCompany = TotalWosCompany + 1;
		END IF;
	END IF;
	-- end 13943
    END;
$$

DELIMITER ;

