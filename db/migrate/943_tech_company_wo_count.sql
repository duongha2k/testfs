
-- tech_company_wo_count
DROP TABLE IF EXISTS `tech_company_wo_count`;

CREATE TABLE `tech_company_wo_count` (
  `Tech_ID` int(11) NOT NULL,
  `Company_ID` varchar(255) NOT NULL,
  `TotalWosCompany` int(11) DEFAULT NULL,
  PRIMARY KEY (`Tech_ID`,`Company_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;

-- Type_ID index of work_orders
CREATE INDEX Type_ID ON work_orders (Type_ID) USING BTREE;


-- data for tech_company_wo_count
INSERT INTO tech_company_wo_count(Tech_ID,Company_ID,TotalWosCompany) 
select Tech_ID,Company_ID,Count(WIN_NUM) AS TotalWosCompany from work_orders
where Tech_ID > 0 AND  Approved=1 AND (Type_ID=1 OR Type_ID=2)  group by Tech_ID,Company_ID


