ALTER TABLE `clicompanies_additional_fields` ADD `PushWMEnabled` TINYINT( 1 ) NOT NULL DEFAULT '0';
ALTER TABLE `projects` ADD `PushWM` TINYINT( 1 ) NOT NULL DEFAULT '0';
ALTER TABLE `work_orders_additional_fields` ADD `PushWM` TINYINT( 1 ) NOT NULL DEFAULT '0';
ALTER TABLE `TechBankInfo` ADD `WMID` INT NULL DEFAULT NULL , ADD INDEX ( `WMID` );
ALTER TABLE `work_orders_additional_fields` ADD `WMAssignmentID` INT NULL DEFAULT NULL , ADD INDEX ( `WMAssignmentID` );

ALTER TABLE `files` ADD `WMFileID` VARCHAR( 75 ) NULL DEFAULT NULL, ADD INDEX ( `WMFileID` );
ALTER TABLE `projects_files` ADD `WMFileID` VARCHAR( 75 ) NULL DEFAULT NULL, ADD INDEX ( `WMFileID` );

CREATE TABLE IF NOT EXISTS `workmarket_offer_processed` (
  `id` int(11) NOT NULL,
  `WMAssignmentID` int(11) NOT NULL DEFAULT '0',
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `WMID` (`WMAssignmentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `workmarket_note_processed` (
  `id` int(11) NOT NULL,
  `WMAssignmentID` int(11) NOT NULL DEFAULT '0',
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`,`WMAssignmentID`),
  KEY `WMAssignmentID` (`WMAssignmentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `workmarket_question_processed` (
  `id` int(11) NOT NULL,
  `WMAssignmentID` int(11) NOT NULL DEFAULT '0',
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `WMID` (`WMAssignmentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `workmarket_attachment_processed` (
  `id` int(11) NOT NULL,
  `WMAssignmentID` int(11) NOT NULL DEFAULT '0',
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `WMID` (`WMAssignmentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `workmarket_poll` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `datetimeUnix` int(11) NOT NULL DEFAULT '0',
  `modifiedCount` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `workmarket_question_wo_qa_mapping` (
  `id` int(11) NOT NULL,
  `WMAssignmentID` int(11) NOT NULL DEFAULT '0',
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `WMID` (`WMAssignmentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE `wo_categories` ADD `WMGroupID` INT NULL DEFAULT NULL;
UPDATE wo_categories SET WMGroupID = 1983 WHERE Category_ID IN (13,18); # ATM / Kiosk
UPDATE wo_categories SET WMGroupID = 1984 WHERE Category_ID IN (1,35,16,19,15); # Cabling
UPDATE wo_categories SET WMGroupID = 1985 WHERE Category_ID IN (8,10); # CCTV / Audio Video
UPDATE wo_categories SET WMGroupID = 1986 WHERE Category_ID IN (31,33,4); # Desktop / Laptop
UPDATE wo_categories SET WMGroupID = 1987 WHERE Category_ID IN (14,34,21); # Digital Signage
UPDATE wo_categories SET WMGroupID = 1988 WHERE Category_ID IN (6,7,25,20); # Networking
UPDATE wo_categories SET WMGroupID = 1989 WHERE Category_ID IN (36,30,3,26,23); # General / Site Prep
UPDATE wo_categories SET WMGroupID = 1990 WHERE Category_ID IN (9); # Point of Sale
UPDATE wo_categories SET WMGroupID = 1991 WHERE Category_ID IN (29,28,27,37); # Printers / Scanners / Copiers
UPDATE wo_categories SET WMGroupID = 1992 WHERE Category_ID IN (32,12,22); # Servers
UPDATE wo_categories SET WMGroupID = 1993 WHERE Category_ID IN (5,24); # Telephony
UPDATE wo_categories SET WMGroupID = 1994 WHERE Category_ID IN (2,17); # Wiring

CREATE INDEX PrimaryEmail ON TechBankInfo (PrimaryEmail);
