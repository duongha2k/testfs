CREATE TABLE `work_orders__bids` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `WorkOrderID` int(11) NOT NULL,
  `WorkOrderNum` varchar(255) NOT NULL,
  `TechID` int(11) NOT NULL,
  `Tech_FName` varchar(255) NOT NULL,
  `Tech_LName` varchar(255) NOT NULL,
  `Company_ID` varchar(255) default NULL,
  `BidAmount` varchar(255) NOT NULL,
  `Bid_Date` datetime NOT NULL,
  `BidModifiedDate` datetime NOT NULL,
  `Comments` varchar(255) NOT NULL,
  `ClientEmail` varchar(255) NOT NULL,
  `DeactivatedTech` tinyint(1) NOT NULL default '0',
  `CreatedByUser` varchar(255) default NULL,
  `Hide` tinyint(1) NOT NULL default '0',
  `caspio_id` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8