
-- notice_settings
DROP TABLE IF EXISTS `notice_settings`;

CREATE TABLE `notice_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) DEFAULT NULL,
  `see_notices_for` int(11) DEFAULT NULL COMMENT '0: for wos client own only, 1: for selected projects',
  `last_view_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


-- notice_settings_project
DROP TABLE IF EXISTS `notice_settings_project`;

CREATE TABLE `notice_settings_project` (
  `client_id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `WOChangeStatusLog`;

CREATE TABLE `WOChangeStatusLog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `WIN_NUM` int(11) NOT NULL,
  `WOStatusID` int(11) NOT NULL,
  `DateTime_Stamp` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `WIN_NUM` (`WIN_NUM`),
  KEY `WOStatusID` (`WOStatusID`),
  KEY `DateTime_Stamp` (`DateTime_Stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 CHECKSUM=1 DELAY_KEY_WRITE=1 ROW_FORMAT=DYNAMIC;


