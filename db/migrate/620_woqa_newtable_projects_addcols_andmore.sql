
-- wo_qa
DROP TABLE IF EXISTS `wo_qa`;

CREATE TABLE `wo_qa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Created_Date` datetime DEFAULT NULL,
  `Created_By` varchar(255) DEFAULT NULL,
  `Type` int(11) DEFAULT NULL,
  `Workorder_ID` int(11) DEFAULT NULL,
  `Tech_ID` int(11) DEFAULT NULL,
  `Tech_Name` varchar(255) DEFAULT NULL,
  `Client_ID` int(11) DEFAULT NULL,
  `Client_Name` varchar(255) DEFAULT NULL,
  `Company_ID` varchar(255) DEFAULT NULL,
  `Question_ID` int(11) DEFAULT NULL,
  `Message` varchar(2000) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- projects
ALTER TABLE projects ADD COLUMN ReceiveTechQAMessageNotification tinyint DEFAULT 0;
ALTER TABLE projects ADD COLUMN ReceiveTechQAMessageNotificationEmails varchar(255);

-- future_wo_info
ALTER TABLE future_wo_info ADD COLUMN TechQAMessageEmailTo varchar(255);

-- wo_qa_lastview
DROP TABLE IF EXISTS `wo_qa_lastview`;

CREATE TABLE `wo_qa_lastview` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Type` int(11) DEFAULT NULL COMMENT '1: for client, 2: for tech',
  `Client_ID` int(11) DEFAULT NULL,
  `Tech_ID` int(11) DEFAULT NULL,
  `Workorder_ID` int(11) DEFAULT NULL,
  `last_view_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- WOACSChangeLog
ALTER TABLE `WOACSChangeLog` CHANGE `WO_UNID` `WO_UNID` INT( 11 ) NOT NULL DEFAULT '0'

-- ProjectACSChangeLog
ALTER TABLE ProjectACSChangeLog ADD COLUMN ReminderCustomHr int; 		
ALTER TABLE ProjectACSChangeLog ADD COLUMN ReminderCustomHrChecked tinyint(1); 
ALTER TABLE ProjectACSChangeLog ADD COLUMN ReminderCustomHr_2 int;
ALTER TABLE ProjectACSChangeLog ADD COLUMN ReminderCustomHrChecked_2 tinyint(1);
ALTER TABLE ProjectACSChangeLog ADD COLUMN ReminderCustomHr_3 int;
ALTER TABLE ProjectACSChangeLog ADD COLUMN ReminderCustomHrChecked_3 tinyint(1);
ALTER TABLE ProjectACSChangeLog ADD COLUMN ChangedColumns varchar(1000);





