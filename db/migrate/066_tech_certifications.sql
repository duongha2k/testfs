CREATE TABLE IF NOT EXISTS `certifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `label` varchar(180) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `hide` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `certifications`
--

INSERT INTO `certifications` (`id`, `name`, `label`, `description`, `hide`) VALUES
(1, 'LowVoltageLicense', 'Do you possess all the licenses required in your city/state to perform low voltage work?', NULL, 0),
(2, 'DeVry', 'DeVry Student', NULL, 0),
(3, 'TBC', 'Technician Boot Camp', NULL, 1),
(4, 'BlueRibbonTechnician', 'Blue Ribbon Technician', NULL, 1);


CREATE TABLE IF NOT EXISTS `tech_certification` (
  `TechID` int(11) NOT NULL,
  `certification_id` int(11) NOT NULL,
  `number` varchar(100) DEFAULT NULL,
  `dateExpired` date DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`TechID`,`certification_id`),
  KEY `certification_id` (`certification_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tech_certification`
--
ALTER TABLE `tech_certification`
  ADD CONSTRAINT `tech_certification_ibfk_1` FOREIGN KEY (`certification_id`) REFERENCES `certifications` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;


