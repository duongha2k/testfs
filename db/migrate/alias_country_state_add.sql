CREATE TABLE IF NOT EXISTS `alias_country` (
  `alias` varchar(50) NOT NULL,
  `key` varchar(5) NOT NULL,
  PRIMARY KEY (`alias`,`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `alias_country` (`alias`, `key`) VALUES
('USA', 'US');

CREATE TABLE IF NOT EXISTS `alias_state` (
  `alias` varchar(255) CHARACTER SET utf8 NOT NULL,
  `key` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`alias`,`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
