
ALTER TABLE clients_tech_deduct ADD COLUMN PcntDeductPercent float DEFAULT 0;

ALTER TABLE projects ADD COLUMN PcntDeductPercent float DEFAULT 0;

ALTER TABLE work_orders ADD COLUMN PcntDeductPercent float DEFAULT 0;

UPDATE projects SET PcntDeductPercent=0.10 WHERE PcntDeduct=1;

UPDATE work_orders SET PcntDeductPercent=0.10 WHERE PcntDeduct=1;


