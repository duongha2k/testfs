
INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active,item_sortdesc,item_owner,item_details)
VALUES (43,'CompletionStatus','ChangeOrCancelForceMajeure','Change / Cancel (Force Majeure)',17,1,'Change / Cancel','Force Majeure','Force Majeure');

INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,active,item_sortdesc,item_owner,item_details)
VALUES (44,'CompletionStatus','ChangeOrCancelRescheduled','Change / Cancel - Rescheduled',18,1,'Change / Cancel','Rescheduled','Rescheduled');

-- wo_tag_items
ALTER TABLE wo_tag_items ADD COLUMN subdisplay_order FLOAT DEFAULT 10.0;

UPDATE wo_tag_items SET wo_tag_item_code='CompleteMultiVisit', wo_tag_item_name='Complete – Multi Visit',item_details='Multi-Visit' WHERE id=26;
UPDATE wo_tag_items SET wo_tag_item_code='IncompleteSiteOrClientDriven_SiteNotReady', wo_tag_item_name='Incomplete (Site / Client Driven) - Site Not Ready',item_details='Site Not Ready' WHERE wo_tag_item_code='IncompleteSiteOrClientDriven_EndUserIssue';

UPDATE wo_tag_items SET display_order=5,subdisplay_order=1.0 WHERE wo_tag_item_code='IncompleteSiteOrClientDriven_HardwareRelated';
UPDATE wo_tag_items SET display_order=5,subdisplay_order=2.0 WHERE wo_tag_item_code='IncompleteSiteOrClientDriven_OutofScopeWork';
UPDATE wo_tag_items SET display_order=5,subdisplay_order=3.0 WHERE wo_tag_item_code='IncompleteSiteOrClientDriven_SiteNotReady';
UPDATE wo_tag_items SET display_order=5,subdisplay_order=4.0 WHERE wo_tag_item_code='IncompleteSiteOrClientDriven_SiteTurnAwayOrAbort';

UPDATE wo_tag_items SET wo_tag_item_code='IncompleteTechDriven_PerformanceIssue', wo_tag_item_name='Incomplete (Tech Driven) – Performance Issue',item_details='Tech Performance Issue',display_order='9' WHERE wo_tag_item_code='IncompleteTechDriven_PerformanceIssue';
UPDATE wo_tag_items SET wo_tag_item_code='IncompleteTechDriven_BackOut', wo_tag_item_name='Incomplete (Tech Driven) – Back Out',item_details='Tech Back-Out',display_order='10' WHERE wo_tag_item_code='IncompleteTechDriven_DelayedArrival';
UPDATE wo_tag_items SET wo_tag_item_code='IncompleteTechDriven_NoShow', wo_tag_item_name='Incomplete (Tech Driven) – No Show',item_details='Tech No Show',display_order='11' WHERE wo_tag_item_code='IncompleteTechDriven_InternalIssue';
INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,subdisplay_order,active,item_sortdesc,item_owner,item_details)
VALUES (58,'CompletionStatus','IncompleteTechDriven_ProviderOpsIssue','Incomplete (Tech Driven) – Provider (Ops) Issue',12,1.0,1,'Incomplete','Provider','Provider (Ops) Issue');

INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,subdisplay_order,active,item_sortdesc,item_owner,item_details)
VALUES (59,'CompletionStatus','RescheduledSiteOrClientDriven_HardwareRelated','Rescheduled (Site / Client Driven) - Hardware Related',14,1.0,1,'Rescheduled','Site / Client','Hardware Related');
INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,subdisplay_order,active,item_sortdesc,item_owner,item_details)
VALUES (60,'CompletionStatus','RescheduledSiteOrClientDriven_OutofScopeWork','Rescheduled (Site / Client Driven) - Out of Scope Work',14,2.0,1,'Rescheduled','Site / Client','Out of Scope Work');
INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,subdisplay_order,active,item_sortdesc,item_owner,item_details)
VALUES (61,'CompletionStatus','RescheduledSiteOrClientDriven_SiteNotReady','Rescheduled (Site / Client Driven) - Site Not Ready',14,3.0,1,'Rescheduled','Site / Client','Site Not Ready');
INSERT INTO wo_tag_items(id,wo_tag_code,wo_tag_item_code,wo_tag_item_name,display_order,subdisplay_order,active,item_sortdesc,item_owner,item_details)
VALUES (62,'CompletionStatus','RescheduledSiteOrClientDriven_SiteTurnAwayOrAbort','Rescheduled (Site / Client Driven) - Site Turn-Away / Abort',14,4.0,1,'Rescheduled','Site / Client','Site Turn-Away / Abort');

UPDATE wo_tag_items SET wo_tag_item_code='RescheduledTechDriven_PerformanceIssue', wo_tag_item_name='Rescheduled (Tech Driven) – Performance Issue',item_sortdesc='Rescheduled',item_details='Tech Performance Issue',item_owner='Technician',display_order='16',subdisplay_order=1.0  WHERE wo_tag_item_code='ChangeOrCancelSiteOrClientDriven' OR wo_tag_item_code='RescheduledTechDriven_PerformanceIssue';
UPDATE wo_tag_items SET wo_tag_item_code='RescheduledTechDriven_BackOut', wo_tag_item_name='Rescheduled (Tech Driven) – Back Out',item_sortdesc='Rescheduled',item_details='Tech Back-Out',item_owner='Technician',display_order='16',subdisplay_order=2.0  WHERE wo_tag_item_code='ChangeOrCancelTechDriven_BackOut' OR wo_tag_item_code='RescheduledTechDriven_BackOut';
UPDATE wo_tag_items SET wo_tag_item_code='RescheduledTechDriven_NoShow', wo_tag_item_name='Rescheduled (Tech Driven) – No Show',item_sortdesc='Rescheduled',item_details='Tech No Show',item_owner='Technician',display_order='16',subdisplay_order=3.0  WHERE wo_tag_item_code='ChangeOrCancelTechDriven_NoShow' OR wo_tag_item_code='RescheduledTechDriven_NoShow';
UPDATE wo_tag_items SET wo_tag_item_code='RescheduledTechDriven_ProviderOpsIssue', wo_tag_item_name='Rescheduled (Tech Driven) – Provider (Ops) Issue',item_sortdesc='Rescheduled',item_details='Provider (Ops) Issue',item_owner='Technician',display_order='16',subdisplay_order=4.0  WHERE wo_tag_item_code='ChangeOrCancelRescheduled' OR wo_tag_item_code='RescheduledTechDriven_ProviderOpsIssue';


UPDATE wo_tag_items SET wo_tag_item_code='Rescheduled_NaturalCausesOrForceMajeure', wo_tag_item_name='Rescheduled – Natural Causes',item_sortdesc='Rescheduled',item_details='Natural Causes',item_owner='Force Majeure',display_order='21' WHERE wo_tag_item_code='ChangeOrCancelForceMajeure' OR wo_tag_item_code='Rescheduled_NaturalCausesOrForceMajeure';
UPDATE wo_tag_items SET wo_tag_item_code='CancelInternalIssue', wo_tag_item_name='Cancel',item_sortdesc='Cancel',item_details='Cancel',item_owner='Internal',display_order='22' WHERE wo_tag_item_code='ChangeOrCancelInternalIssue' OR wo_tag_item_code='CancelInternalIssue';

-- update itemname, sort-desc, owner, details
UPDATE wo_tag_items SET  wo_tag_item_name='Complete – 1<sup>st</sup> Visit',item_sortdesc='Complete',
item_owner='Tech / Provider',item_details='1<sup>st</sup> Visit' WHERE wo_tag_item_code='Complete1stVisit';
UPDATE wo_tag_items SET  wo_tag_item_name='Complete – Multi-Visit',item_sortdesc='Complete',
item_owner='Tech / Provider',item_details='Multi-Visit' WHERE wo_tag_item_code='CompleteMultiVisit';
UPDATE wo_tag_items SET  wo_tag_item_name='Complete – Reassigned',item_sortdesc='Complete',
item_owner='Tech / Provider',item_details='Reassigned' WHERE wo_tag_item_code='CompleteReassigned';
UPDATE wo_tag_items SET  wo_tag_item_name='Complete – Rescheduled',item_sortdesc='Complete',
item_owner='Tech / Provider',item_details='Rescheduled' WHERE wo_tag_item_code='CompleteRescheduled';

UPDATE wo_tag_items SET item_owner='Tech / Provider' WHERE wo_tag_item_code='IncompleteTechDriven_PerformanceIssue';
UPDATE wo_tag_items SET item_owner='Tech / Provider' WHERE wo_tag_item_code='IncompleteTechDriven_BackOut';
UPDATE wo_tag_items SET item_owner='Tech / Provider' WHERE wo_tag_item_code='IncompleteTechDriven_NoShow';
UPDATE wo_tag_items SET item_owner='Tech / Provider' WHERE wo_tag_item_code='IncompleteTechDriven_ProviderOpsIssue';
UPDATE wo_tag_items SET item_owner='Natural Causes',wo_tag_item_name='Incomplete – Natural Causes' WHERE wo_tag_item_code='Incomplete_NaturalCausesOrForceMajeure';

UPDATE wo_tag_items SET item_owner='Tech / Provider' WHERE wo_tag_item_code='RescheduledTechDriven_PerformanceIssue';
UPDATE wo_tag_items SET item_owner='Tech / Provider' WHERE wo_tag_item_code='RescheduledTechDriven_BackOut';
UPDATE wo_tag_items SET item_owner='Tech / Provider' WHERE wo_tag_item_code='RescheduledTechDriven_NoShow';
UPDATE wo_tag_items SET item_owner='Tech / Provider' WHERE wo_tag_item_code='RescheduledTechDriven_ProviderOpsIssue';
UPDATE wo_tag_items SET item_owner='Natural Causes',item_details='',wo_tag_item_name='Rescheduled – Natural Causes' WHERE wo_tag_item_code='Rescheduled_NaturalCausesOrForceMajeure';

UPDATE wo_tag_items SET item_owner='Site / Client',item_details='',item_sortdesc='Cancelled',wo_tag_item_name='Cancelled' WHERE wo_tag_item_code='CancelInternalIssue';

UPDATE wo_tag_items SET subdisplay_order=9 WHERE wo_tag_item_code='Scheduled';










