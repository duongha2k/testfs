<?php
require_once dirname(__FILE__).'/../../includes/modules/common.init.php';

$sql = 'select WIN_NUM, a.ServiceTypeId as "AddServiceTypeId", a.FSClientServiceDirectorId as "AddCSD", a.EntityTypeId as "AddEntityTypeId", w.Project_ID, p.ServiceTypeId as "ProjServiceTypeId", p.FSClientServiceDirectorId as "ProjCSD", p.EntityTypeId as "ProjEntityTypeId"
from work_orders w LEFT JOIN work_orders_additional_fields a ON w.WIN_NUM = a.WINNUM 
LEFT JOIN projects p ON w.Project_ID = p.Project_ID
LEFT JOIN clients c ON c.UserName = w.Username
where ( (a.ServiceTypeId IS NULL Or a.ServiceTypeId = "") AND p.ServiceTypeId IS NOT NULL)
OR ( (a.FSClientServiceDirectorId IS NULL OR a.FSClientServiceDirectorId = "") AND p.FSClientServiceDirectorId IS NOT NULL)
OR ( (a.EntityTypeId IS NULL OR a.EntityTypeId = "") AND p.EntityTypeId IS NOT NULL) LIMIT 5000';
$db = Zend_Registry::get('DB');
$wo = $db->fetchAll($sql);

$distinct = array();


foreach ($wo as $w) {
	$addFieldUpdateArr = array();
	if($w["AddServiceTypeId"] == "" && $w['ProjServiceTypeId'] != ""){
		$addFieldUpdateArr['ServiceTypeId'] = $w['ProjServiceTypeId'];
	}
	
	if($w["AddCSD"] == "" && $w['ProjCSD'] != ""){
		$addFieldUpdateArr['FSClientServiceDirectorId'] = $w['ProjCSD'];
	}
	
	if($w["AddEntityTypeId"] == "" && $w['ProjEntityTypeId'] != ""){
		$addFieldUpdateArr['EntityTypeId'] = $w['ProjEntityTypeId'];
	}
	print "WIN: ".$w['WIN_NUM']."\r\n";
	print_r($addFieldUpdateArr);
	print "\r\n";
	$crit = "WINNUM = '".$w['WIN_NUM']."'";
	$db->update("work_orders_additional_fields", $addFieldUpdateArr, $crit);
}
exit;

