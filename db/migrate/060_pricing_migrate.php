<?php
require_once dirname(__FILE__).'/../../includes/modules/common.init.php';

$table = array(
	'Pricing_Fields' => array('PricingFieldID', 'DisplayName', 'ActualName'),
	'Pricing_Rules' => array('PricingRuleID', 'Description', 'ParentID', 'CloneDate'),
	'Charge_Types' => array('ChargeTypeID', 'Description', 'Percentage', 'FlatFee', 'PerX', 'InputField', 'OutputField', 'CalculationOrder', 'Min', 'Max', 'PricingRuleID', 'ApplyWhen')
);

foreach ($table as $t => $fieldList) {

print $t;

$idField = $fieldList[0];
$fieldListMap = array_combine($fieldList, $fieldList);

$maxId = 9000;
$step = 10000;

$startIndex = 0;
$db = Core_Database::getInstance();
$tm = strtolower($t);

while($startIndex < $maxId) {
    
    $stopIndex =  $startIndex + $step;
    
    $ascLogs = Core_Caspio::caspioSelectWithFieldListMap($t, $fieldListMap, $idField . ' >= ' .(int)$startIndex . ' AND ' . $idField . '  < ' . (int)$stopIndex, "$idField ASC");

    if (is_array($ascLogs) ) {
        foreach ($ascLogs as $reminder) {
            
            $select = $db->select();
            $select->from($tm, $fieldList)
                   ->where($idField . ' = ?', (int)$reminder[$idField]);
        
            $result = $db->fetchRow($select);     
            
            if (empty($result)) {
                $toInsert = $reminder;
                foreach ($toInsert as &$value) {
                    if ($value === 'True') {
                        $value = 1;
                    } elseif ($value === 'False') {
                        $value = 0;
                    }
                }

				if (isset($toInsert['CloneDate'])) {
					$date = new Zend_Date($toInsert['CloneDate'], 'MM/dd/YYYY hh:mm:ss a');
					$toInsert['CloneDate'] = $date->toString('YYYY-MM-dd HH:mm:ss');
				}
                
//                $date = new Zend_Date($toInsert['CalledTime'], 'MM/dd/YYYY hh:mm:ss a');
//                $toInsert['CalledTime'] = $date->toString('YYYY-MM-dd HH:mm:ss');
//				if (is_null($toInsert['ParentCall'])) $toInsert['ParentCall'] = $toInsert['id'];
                try {
                    $db->insert($tm, $toInsert);
                } catch (Zend_Exception $e) {
                    print "Error";
                    print_r($toInsert);
                    exit;
                }
                print 'Inserting: ' . $toInsert[$idField] . "\n";
            } else {
                print 'Skip: ' . $reminder[$idField] . "\n";
            }
        }
    }
    
    $startIndex = $stopIndex;
}
}
