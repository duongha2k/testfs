
ALTER TABLE projects ADD COLUMN StartType int NOT NULL DEFAULT 1;
ALTER TABLE projects ADD COLUMN WorkStartTime varchar(255);
ALTER TABLE projects ADD COLUMN WorkEndTime varchar(255);
ALTER TABLE projects ADD COLUMN EstimatedDuration decimal(10,2);
ALTER TABLE projects ADD COLUMN TechArrivalInstructions varchar(255);
ALTER TABLE projects ADD COLUMN ReceiveReVisitNotification tinyint NOT NULL DEFAULT 0;
ALTER TABLE projects ADD COLUMN ReceiveReVisitNotificationEmails varchar(255);

DROP TABLE IF EXISTS `work_order_visits`;

CREATE TABLE `work_order_visits` (
  `WoVisitID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `WIN_NUM` int(11) NOT NULL,
  `WOVisitOrder` int(11) NOT NULL,
  `StartTypeID` int(11) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `StartTime` varchar(255) DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `EndTime` varchar(255) DEFAULT NULL,
  `EstimatedDuration` decimal(10,2) DEFAULT NULL,
  `TechArrivalInstructions` varchar(255) DEFAULT NULL,
  `Type` int(11) NOT NULL,
  `TechCheckInDate` date DEFAULT NULL,
  `TechCheckInTime` varchar(255) DEFAULT NULL,
  `CORCheckInDate` date DEFAULT NULL,
  `CORCheckInTime` varchar(255) DEFAULT NULL,
  `OptionCheckInOut` int(11) DEFAULT NULL,
  `TechCheckOutDate` date DEFAULT NULL,
  `TechCheckOutTime` varchar(255) DEFAULT NULL,
  `CORCheckOutDate` date DEFAULT NULL,
  `CORCheckOutTime` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`WoVisitID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

