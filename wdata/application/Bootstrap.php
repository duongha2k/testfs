<?php
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    /**
     * _initController
     *
     * @access protected
     * @return void
     */
    protected function _initController()
    {
        $this->bootstrap('FrontController');
        $front = $this->FrontController;

        if ( APPLICATION_ENV === 'development' ) {
            $front->setParam('noErrorHandler', true);
            $front->throwExceptions(true);
        }

        $modules = $front->getControllerDirectory();
        $front->registerPlugin(new Core_Modules_Loader($modules));

        $phpSessionHandler = new Core_PHPSessionHandler();
        
        return $front;
    }

    /**
     * _initRouter
     *
     * @access protected
     * @return void
     */
    protected function _initRouter()
    {
        $this->bootstrap('frontController');
        $front = $this->FrontController;

        $router = $front->getRouter();
        $router->removeDefaultRoutes();
        $router->addRoute(
            'default',
            new Zend_Controller_Router_Route(
                ':module/:controller/:action/*',
                array(
                    'module'        =>  'default',
                    'controller'    =>  'index',
                    'action'        =>  'index'
                )
            )
        );

        return $router;
    }

    /**
     * _initConfigs
     *
     * @access protected
     * @return void
     */
    protected function _initConfigs()
    {
        Zend_Registry::_unsetInstance();
    	$cache = $this->bootstrap('Cache')->getResource('Cache');
        $this->bootstrap('Defines');

        if ( FALSE === ($cfgSite = $cache->load('cfg_site_xml')) ) {
            $cfgSite = new Zend_Config_Xml(APPLICATION_PATH.'/../../includes/configs/cfg.site.xml');
            $cache->save($cfgSite, 'cfg_site_xml', array('configs'), LIFETIME_30DAYS);
        }

        Zend_Registry::set('CFG_SITE', $cfgSite);
        defined('WEBSITE_LIB_PATH') || define('WEBSITE_LIB_PATH', $cfgSite->get("site")->get("website_lib_path"));

		$cfgDb = $cache->load('cfg_db_xml');
		/**********/
		if (empty($cfgDb)) {
			$cfgDb = new Zend_Config_Xml(APPLICATION_PATH.'/../../includes/configs/cfg.db.xml', 'database');
			$cache->save($cfgDb, 'cfg_db_xml', array(), LIFETIME_30DAYS);
		}
		$db = Zend_Db::factory($cfgDb->type, array(
            'host'             => $cfgDb->host,
            'username'         => $cfgDb->username,
            'password'         => $cfgDb->password,
            'dbname'           => $cfgDb->name
		));
		Zend_Db_Table::setDefaultAdapter($db);
		Zend_Registry::set('DB', $db);
		Zend_Registry::set('DB_Info',  array(
            'host'             => $cfgDb->host,
            'username'         => $cfgDb->username,
            'password'         => $cfgDb->password,
            'dbname'           => $cfgDb->name
                ));
		$sql = "SET NAMES utf8";
		$db->query($sql);

    	if (!$db->isConnected()) {
		    throw new Core_Exception('No Db connection', 1, Core_Exception::CRITICAL_ERROR);
		}

        if ( FALSE === ($cfgAmazon = $cache->load('cfg_amazon_xml')) ) {
            $cfgAmazon = new Zend_Config_Xml(APPLICATION_PATH.'/../../includes/configs/cfg.amazon.xml');
            $cache->save($cfgAmazon, 'cfg_amazon_xml', array('configs'), LIFETIME_30DAYS);
        }
        defined('S3_ACCESS_KEY')        || define('S3_ACCESS_KEY', $cfgAmazon->get('s3')->get('accessKey'));
        defined('S3_SICRET_KEY')        || define('S3_SICRET_KEY', $cfgAmazon->get('s3')->get('secretKey'));
        defined('S3_CLIENTS_DOCS_DIR')  || define('S3_CLIENTS_DOCS_DIR', $cfgAmazon->get('s3')->get('bukets')->get('wos'));
        defined('S3_CLIENTS_LOGO_DIR')  || define('S3_CLIENTS_LOGO_DIR', $cfgAmazon->get('s3')->get('bukets')->get('clients_logo'));
        defined('S3_CLIENTS_LOGO_WWW')  || define('S3_CLIENTS_LOGO_WWW', $cfgAmazon->get('s3')->get('distibution')->get('clients_logo'));
        defined('S3_PROJECTS_DOCS_DIR') || define('S3_PROJECTS_DOCS_DIR', $cfgAmazon->get('s3')->get('bukets')->get('projects'));
        defined('S3_PROJECTS_LOGO_DIR') || define('S3_PROJECTS_LOGO_DIR', $cfgAmazon->get('s3')->get('bukets')->get('projects_logo'));
        defined('S3_PROJECTS_DOCS_WWW') || define('S3_PROJECTS_DOCS_WWW', $cfgAmazon->get('s3')->get('distibution')->get('projects_logo'));
        defined('S3_TECHS_DOCS_DIR')  || define('S3_TECHS_DOCS_DIR', $cfgAmazon->get('s3')->get('bukets')->get('techs'));
    }

    /**
     * _initDefines
     *
     * @access protected
     * @return void
     */
    protected function _initDefines()
    {
        require_once realpath(dirname(__FILE__).'/../../includes/modules/tables.php');

        defined('DASHBOARD_MODULE_PATH')
            || define('DASHBOARD_MODULE_PATH', APPLICATION_PATH.'/modules/dashboard/');

        defined('ADMINDASHBOARD_MODULE_PATH')
            || define('ADMINDASHBOARD_MODULE_PATH', APPLICATION_PATH.'/modules/admindashboard/');
            
        defined('MAPPING_MODULE_PATH')
            || define('MAPPING_MODULE_PATH', APPLICATION_PATH.'/modules/mapping/');

        defined('SITE_URL')         || define('SITE_URL', 'www.fieldsolutions.com');
        defined('LIFETIME_15MIN')   || define('LIFETIME_15MIN', 900);
        defined('LIFETIME_30MIN')   || define('LIFETIME_30MIN', 1800);
        defined('LIFETIME_45MIN')   || define('LIFETIME_45MIN', 2700);
        defined('LIFETIME_60MIN')   || define('LIFETIME_60MIN', 3600);
        defined('LIFETIME_1HOUR')   || define('LIFETIME_1HOUR', LIFETIME_60MIN);
        defined('LIFETIME_2HOURS')  || define('LIFETIME_2HOURS', 7200);
        defined('LIFETIME_5HOURS')  || define('LIFETIME_5HOURS', 18000);
        defined('LIFETIME_10HOURS') || define('LIFETIME_10HOURS', 36000);
        defined('LIFETIME_24HOURS') || define('LIFETIME_24HOURS', 84600);
        defined('LIFETIME_1DAY')    || define('LIFETIME_1DAY', LIFETIME_24HOURS);
        defined('LIFETIME_10DAYS')  || define('LIFETIME_10DAYS', 846000);
        defined('LIFETIME_20DAYS')  || define('LIFETIME_20DAYS', 1728000);
        defined('LIFETIME_30DAYS')  || define('LIFETIME_30DAYS', 2592000);
        defined('VAR_PATH')         || define('VAR_PATH', rtrim(realpath(APPLICATION_PATH.'/../var')).'/');
        defined('DOCUMENT_CONFIG')  || define('DOCUMENT_CONFIG', realpath(APPLICATION_PATH.'/../../includes/configs').'/');
        defined('SHORT_AUTH')       || define('SHORT_AUTH',true);


        defined('LOGGER_ERROR_YES') || define('LOGGER_ERROR_YES', 'yes');
        defined('LOGGER_ERROR_NO')  || define('LOGGER_ERROR_NO', 'no');

        #  If the system should use memcache, use false for dev environment
        defined('CACHE_USE_MEMCACHED')  || define('CACHE_USE_MEMCACHED', false);
        defined('CACHE_LIFE_TIME')      || define('CACHE_LIFE_TIME', 7200); // 2 hours
        #  If memcache is disabled
        defined('CACHE_DIR_FOR_FILES')  || define('CACHE_DIR_FOR_FILES', realpath(APPLICATION_PATH . '/../var/cache'));

        defined('UPLOAD_DIR')       || define('UPLOAD_DIR', APPLICATION_PATH.'/../../upload');
    }

    /**
     * _initModules 
     * 
     * @access protected
     * @return void
     */
    protected function _initModules()
    {
    }

    /**
     * _initCache
     *
     * @access protected
     * @return void
     */
    protected function _initCache()
    {
        $this->bootstrap('Defines');

        $frontendOptions = array(
            'lifetime'                  => LIFETIME_2HOURS,
            'automatic_serialization'   => true
        );
        $backendOptions = array(
            'cache_dir' => realpath(APPLICATION_PATH.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'var'.DIRECTORY_SEPARATOR.'cache')
        );

        $cache = Zend_Cache::factory('Core', 'File', $frontendOptions, $backendOptions);
//$cache->clean();
        return $cache;
    }

    /**
     * _initCaspio
     *
     * @access protected
     * @return void
     */
    protected function _initCaspio()
    {
    	Core_Caspio::init();
    }

    /**
     * _initAuthentication
     *
     * @access protected
     * @return void
     */
    protected function _initAuth()
    {
        $this->bootstrap(array('Caspio','Defines'));

        //  Name from headerStartSession.php
        session_name('fsTemplate');

        /* 
         * @author Alex Che
         * Fix for ssl download file in IE (only for dwonload pages) 
         * */
        if (isset($_SERVER['HTTP_USER_AGENT']) &&  (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)
            && !empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on'
            && strstr($_SERVER['REQUEST_URI'], '-download/') !== false
            ) {
                session_cache_limiter('private');
            }
        $auth = new Zend_Session_Namespace('Auth_User');

        if ( empty($_SESSION['UserName']) || $auth->login !== $_SESSION['UserName'] || strtolower(trim($_SESSION['loggedIn'])) !== 'yes' ) {
            $auth->login    = NULL;
            $auth->password = NULL;
            $auth->loggedInAs = NULL;
            $auth->isPM = FALSE;
        }
        if ( strtolower(trim($_SESSION['loggedIn'])) === 'yes') {
            $auth->login      = trim($_SESSION['UserName']);
			$auth->password = $_SESSION['Password'];
            $auth->loggedInAs = $_SESSION['loggedInAs'];
            $auth->companyId  = isset($_SESSION['CompanyId']) ? trim($_SESSION['CompanyId']) : null;
            $auth->userType   = (isset($_SESSION['UserType'])? trim($_SESSION['UserType']) : '');
            /**
             * Use global load method
             * @author Pavel Shutin
             */
            if ($_SESSION['loggedInAs'] == 'client') {
                $user = new Core_Api_User();
                $authData = array('login'=>$auth->login,'password'=>$auth->password);
                if ($_SESSION['PM_Company_ID']) {
                    $authData['login'] .='|pmContext='.$_SESSION['PM_Company_ID'];
                }
                $user->load($authData);
                $auth->password = $user->getPassword();
                $auth->name = $user->getContactName();
                $auth->isPM = $user->isPM();
		$auth->loggedInAs = 'client';
            }elseif ($_SESSION['loggedInAs'] == 'tech') {
                $user = new Core_Api_TechUser();
                $user->load(array('login'=>$auth->login));
                $auth->password = $user->getPassword();
		$auth->loggedInAs = 'tech';
            }elseif ($_SESSION['loggedInAs'] == 'admin') {
                $user = new Core_Api_AdminUser();
                $user->load(array('login'=>$auth->login));
                $auth->password = $user->getPassword();
		$auth->loggedInAs = 'admin';
            }elseif ($_SESSION['loggedInAs'] == 'customers') {
//		$customerID = $_SESSION['CustomerID'];
                $user = new Core_Api_CustomerUser();
                $user->load(array('login'=>$auth->login));
                $auth->password = $user->getPassword();
                $auth->companyId = $user->getCompanyId();
		$auth->loggedInAs = 'customers';
            }
        }

        /**
         * Close session because it is lock for ajax requests
         * @author Artem Sukharev
         */
        //session_write_close();
    }
}

