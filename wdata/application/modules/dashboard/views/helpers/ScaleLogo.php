<?php
class Dashboard_View_Helper_ScaleLogo extends Zend_View_Helper_Abstract
{
    const LOGO_TYPE_PROJECT = 1;
    const LOGO_TYPE_COMPANY = 2;
    /**
     * ScaleLogo
     * 
     * @param string $src
     * @param int $type
     * @param int $h
     * @param int $w
     * @param int $h_scale
     * @param int $w_scale
     * @access public
     * @return string
     */
    public function scaleLogo($filename, $type, $h = null, $w = null, $h_scale = null, $w_scale = null)
    {
        switch ($type) {
            case self::LOGO_TYPE_PROJECT:
                $file = file_get_contents(trim($filename));
                return $this->getImagElement($file,$h,$w,$h_scale,$w_scale);
                break;
            case self::LOGO_TYPE_COMPANY:
                $file = file_get_contents(trim($filename));
                return $this->getImagElement($file,$h,$w,$h_scale,$w_scale);
                break;
            default:
                break;
        }

    }

    private function getImagElement($content,$h = null, $w = null, $h_scale = null, $w_scale = null){
        $i = imagecreatefromstring($content);
        $img_h = imagesy($i);
        $img_w = imagesx($i);
        if($h < $img_h) {
            $res_h = $h;
            $res_w = round($h*$img_w/$img_h);
        }else{
            $res_h = $img_h;
            $res_w = $img_w;
        }
        imagedestroy($i);
        $content = base64_encode($content);
        $img = '<img height="'.$res_h.'" width="'.$res_w.'" src="data:image/jpg;base64,'.$content.'" />';
        return $img;
    }
}

