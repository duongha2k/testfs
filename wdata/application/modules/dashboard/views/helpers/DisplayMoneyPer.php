<?php

class Dashboard_View_Helper_displayMoneyPer extends Zend_View_Helper_Abstract {

    /**
     * displayMoney 
     * 
     * @param string|float $amount
     * @param string $currencySymbol
     * @access public
     * @return string
     */
    public function displayMoneyPer($amount, $currencySymbol = '', $per = null) {
        $per = strtolower(trim($per));
        $cash = $currencySymbol . sprintf('%.2f', $amount + 0);
        if ($cash == '0.00')
            return '--';
        if (!empty($per)) {
            return "$cash/$per";
        } else {
            return $cash;
        }
    }

}

