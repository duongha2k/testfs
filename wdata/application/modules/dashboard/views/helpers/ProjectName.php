<?php
class Dashboard_View_Helper_ProjectName extends Zend_View_Helper_Abstract
{
    public function projectName( $wo )
    {
        if ( $wo->Project_ID + 0 && !empty($wo->Project_Name) ) {
            return trim($wo->Project_Name);
        }
        return null;
    }
}

