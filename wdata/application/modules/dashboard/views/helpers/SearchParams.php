<?php
class Dashboard_View_Helper_SearchParams extends Zend_View_Helper_Abstract
{
    /**
     * searchParams
     * 
     * @param string $params
     * @access public
     * @return string
     */
    public function searchParams($params)
    {
        if (!is_array($params)) {
            return;
        }

        $parts = array();

        if (isset($params['win'])) {
            $parts[]='WIN# '.$params['win'];
        }

        if (isset($params['StartDateFrom']) && isset($params['StartDateTo'])) {
            $parts[]='Start Date from '.$params['StartDateFrom'].' to '.$params['StartDateTo'];
        }elseif(isset($params['StartDateFrom'])) {
            $parts[]='Start Date later than '.$params['StartDateFrom'];
        }elseif(isset($params['StartDateTo'])) {
            $parts[]='Start Date earlier than '.$params['StartDateTo'];
        }

        if (isset($params['DatePaidFrom']) && isset($params['DatePaidTo'])) {
            $parts[]='Payment Date from '.$params['DatePaidFrom'].' to '.$params['DatePaidTo'];
        }elseif(isset($params['DatePaidFrom'])) {
            $parts[]='Payment Date later than '.$params['DatePaidFrom'];
        }elseif(isset($params['DatePaidTo'])) {
            $parts[]='Payment Date earlier than '.$params['DatePaidTo'];
        }

        return implode(' and ',$parts);
    }
}

