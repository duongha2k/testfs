<?php
class Dashboard_View_Helper_FormErrors extends Zend_View_Helper_Abstract
{
    /**
     * formErrors
     * 
     * @param string|array $errors 
     * @param string $class CSS class name for <UL> block
     * @access public
     * @return string
     */
    public function formErrors($errors, $class = NULL)
    {
        $output = '';

        if ( empty($errors) )
            return $output;

    
        $output .= '<ul style="color:#f00; border:1px solid #f00;padding:5px;margin:10px 0;" class="'.((!empty($class) && is_string($class)) ? $class : 'errorMessages').'">';
        if ( is_string($errors) ) {
            $output .= '<li>'. $errors .'</li>';
        } else if ( is_array($errors) ) {
            foreach ( $errors as $e ) {
                if ( is_string($e) ) {
                    $output .= '<li>'. $e .'</li>';
                }
            }
        }
        $output .= '</ul>';

        return $output;
    }
}

