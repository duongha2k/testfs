<?php
class Dashboard_View_Helper_TruncateRow extends Zend_View_Helper_Abstract
{
    /**
     * truncateRow  
     * 
     * @param string $string Input String
     * @param int $lines Number of Lines to output
     * @param int $symbolsPerLine
     * @access public
     * @return string
     */
    public function truncateRow($string, $lines, $symbolsPerLine)
    {
        if (strlen($string) <= ($symbolsPerLine * $lines)) return $string;
        
        $words = explode(' ', $string);
        
        $sizeOfWords = sizeof($words);
        $outputLines = array( '' );
        $line = 0;

        for ($i = 0; $i < $sizeOfWords; $i++) {
            if ( strlen($words[$i]) > $symbolsPerLine ) {
                $outputLines[$line] = substr($words[$i], 0, $symbolsPerLine - 3) . "...";
                break;
            } 
            
            if ( strlen($outputLines[$line].$words[$i].' ') <= $symbolsPerLine ) {
                $outputLines[$line] .= $words[$i].' ';
            } else {
                $line++;
                $outputLines[$line] = $words[$i];
                if ( $line == ($lines - 1) ) {
                    $outputLines[$line] .= '...';
                    break;
                }
            }
        }
        return implode("\n", $outputLines);
    }
}

