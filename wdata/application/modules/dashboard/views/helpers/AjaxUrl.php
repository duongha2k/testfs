<?php
class Dashboard_View_Helper_AjaxUrl extends Zend_View_Helper_Abstract
{
    /**
     * ajaxUrl 
     * 
     * @param array $options 
     * @access public
     * @return string
     */
    public function ajaxUrl(array $options)
    {

        return 'var opt=wManager.get(this).getParams();opt.AccountEnabled = \''.$options['AccountEnabled'].'\'; opt.page=\''.$options['page'].'\';wManager.get(this).show({tab:\''.$options['tab'].'\',params:opt});return false;';
    }
}

