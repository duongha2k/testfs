<?php
class Dashboard_View_Helper_DisplayTotalHours extends Zend_View_Helper_Abstract
{
    /**
     * displayTotalHours
     * 
     * @param string $dateIn  Date in format 'n/j/Y H:i:s A' -> 12/8/2008 12:00:00 AM
     * @param string $timeIn  Time in format 'H:i:s A' -> 12:00:00 AM
     * @param string $dateOut Date in format 'n/j/Y H:i:s A' -> 12/8/2008 12:00:00 AM
     * @param string $timeOut Time in format 'H:i:s A' -> 12:00:00 AM
     * @access public
     * @return string|false
     */
    public function displayTotalHours($dateIn, $timeIn, $dateOut, $timeOut)
    {
/*        $inStamp  = strtotime(trim(substr($dateIn, 0, -12)).' '.$timeIn);
        $outStamp = strtotime(trim(substr($dateOut, 0, -12)).' '.$timeOut);
        $total = $outStamp - $inStamp;
        if ($total) {
            $ost = $total%3600;
            $result = (!$ost) ? ($total/3600).' hours' : (($total-$ost)/3600).' hours '.round($ost/60).' min';;
            return $result;
        }
        return '';*/

        $dateIn = date("Y-m-d", strtotime($dateIn));
        $dateOut = date("Y-m-d", strtotime($dateOut));
        $inStamp  = strtotime(trim($dateIn).' '.$timeIn);
        $outStamp = strtotime(trim($dateOut).' '.$timeOut);
        $total = $outStamp - $inStamp;
        if ($total) {
            $ost = $total%3600;
            $result = (!$ost) ? ($total/3600).' hours' : (($total-$ost)/3600).' hours '.round($ost/60).' min';;
            return $result;
        }
        return '';

    }
}

