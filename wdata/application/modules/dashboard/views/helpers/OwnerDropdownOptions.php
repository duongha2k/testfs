<?php
class Dashboard_View_Helper_OwnerDropdownOptions extends Zend_View_Helper_Abstract
{
    /**
     * displayDate 
     * 
     * @param string $clientList array of Core_Api_User
     * @param string $selectedUser string username
     * @access public
     * @return string html for owner dropdown
     */
    public function ownerDropdownOptions($clientList, $company, $selectedUser = NULL, $isGPM = false)
    {
		$html = "";
		if ($isGPM == true)
			$html .= "<option value=\"\" >Select A User</option>";
		$clientArr = array();
		$gpmArr = array();
		foreach ($clientList as $client) {
			if (($selectedUser != $client->UserName) && ((!$isGPM && $client->Company_ID != $company) || ($isGPM && $client->Company_ID != $company && !$client->isPM()) || !$client->AccountEnabled))
				continue;
			if (($selectedUser == $client->UserName) && ($client->Company_ID != $company && !$client->isPM())) // skip duplicate username from different clients
				continue;
			if ($isGPM && $client->isPM() && $client->Company_ID != $company)
				$gpmArr[$client->UserName] = $client->ContactName . " - " . $client->UserName;
			else
				$clientArr[$client->UserName] = $client->ContactName . " - " . $client->UserName;
		}
		if ($isGPM)
			asort($gpmArr);
		asort($clientArr);
		$html .= $this->optionsHtml($clientArr, $selectedUser);
		if (!empty($gpmArr)) {
			$html .= "<option value='' >----------------</option>";
			$html .= $this->optionsHtml($gpmArr, $selectedUser);
		}
		return $html;
    }
	
	private function optionsHtml($users, $selectedUser = NULL) {
		$html = "";
		foreach ($users as $k => $v) {
			$html .= "<option value=\"$k\" " . ($selectedUser == $k ? "selected=\"selected\"" : "") . ">$v</option>";
		}
		return $html;
	}
}

