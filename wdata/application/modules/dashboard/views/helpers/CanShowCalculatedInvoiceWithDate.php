<?php
class Dashboard_View_Helper_CanShowCalculatedInvoiceWithDate extends Zend_View_Helper_Abstract {
    
    public function canShowCalculatedInvoiceWithDate($date) {
        // used to check if should show calculated invoice amount to client
        try {
            $dateInvoiced = strtotime($date);
        } catch (Exception $e) {
            return false;
        }
        if (empty($dateInvoiced) || !$dateInvoiced || date("Ymd") <= date("Ymd", $dateInvoiced))
            // do not show calculated invoice until day after invoice date
            return false;
        return true;

    }    
}

