<?php
class Dashboard_View_Helper_DisplayAmountPer extends Zend_View_Helper_Abstract
{
    public function displayAmountPer($amountPer)
    {
        switch ( trim(strtolower($amountPer)) ) {
        case 'hour':
            return 'per hour';
	case 'site':
            return 'per site';
	case 'device':
             return 'per device';
        case 'visit':
             return 'per visit';    
        default :
            return $amountPer;
        }
    }
    public function displayAmountAvailable($amountPer)
    {
        switch ( trim(strtolower($amountPer)) ) {
        case 'visit':
             return '# of Visits';    
        default :
            return "Devices";
        }
    }
    public function displayAmountBidSearch($amountPer)
    {
        switch ( trim(strtolower($amountPer)) ) {
        case 'visit':
             return '# of visits';    
        default :
            return "# of devices";
        }
    }
    
}

