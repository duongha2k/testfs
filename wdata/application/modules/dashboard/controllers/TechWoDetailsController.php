<?php

class Dashboard_TechWoDetailsController extends Core_Controller_Action {

    const FILE_MAX_SIZE = 52428800; //50Mb

    public function init() {
        parent::init();
        $this->view->techID = !empty($_SESSION['TechID']) ? $_SESSION['TechID'] : NULL;
        $this->view->techFLSID = !empty($_SESSION['FLSID']) ? $_SESSION['FLSID'] : NULL;
    }

    public function navigationAction() {
        $request = $this->getRequest();
        $search = new Core_Api_TechClass;

        $client = new Core_Api_TechUser;
        $client->checkAuthentication(array('login' => $this->_login, 'password' => $this->_password));

        $win = $request->getParam('win', NULL);
        $key = $request->getParam('key', NULL);

        $this->view->wo = null;
        if (NULL !== $win) {
            $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');
            if (FALSE === ($wo = $cache->load($key))) {
                $paths = $this->getFrontController()->getControllerDirectory();
                if (!empty($paths['dashboard']) && file_exists($paths['dashboard'] . '/IndexController.php')) {
                    require_once $paths['dashboard'] . '/IndexController.php';
                    $fields = Dashboard_IndexController::FIELDS;
                } else {
                    $fields = NULL;
                }
                $wo = $search->getWorkOrder($this->_login, $this->_password, $win, $fields);
            }
            //Check if we have a valid WO.
            if ($wo->success && $wo->data[0]) {
                $this->view->wo = $wo->data[0];
            }
        }

        /* initializing $this->view->counters */
        $this->view->counters = array();
        $search->getWorkOrdersCountByStatus($this->_login, $this->_password, $this->view->counters);
        /* initializing $this->view->counters */
    }

    public function getNumberFsParkAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        require_once 'Core/Api/TechClass.php';
        
        $response = $this->getResponse();
        $request = $this->getRequest();
        $win = $request->getParam('win', NULL);
        $key = $request->getParam('key', NULL);

        $search = new Core_Api_TechClass();
        $success = 0;
        $error = "";
        if (NULL === $win) {
            $error =  'Request is incorrect, WIN# is required and cannot be empty';
        } else {
            $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');
            if (FALSE === ($wo = $cache->load($key))) {
                $paths = $this->getFrontController()->getControllerDirectory();
                if (!empty($paths['dashboard']) && file_exists($paths['dashboard'] . '/IndexController.php')) {
                    require_once $paths['dashboard'] . '/IndexController.php';
                    $fields = Dashboard_IndexController::FIELDS;
                } else {
                    $fields = NULL;
                }
                $wo = $search->getWorkOrder($this->_login, $this->_password, $win, $fields);
            }

            //Check if we have a valid WO.
            if (!$wo->success || !$wo->data[0]) {
                $this->_helper->viewRenderer->setNoRender();
                $error = Core_Api_TechClass::ERROR_WO_INCORRECT_ID;
            }
            else
            {
                //Do not cache project info.
//                if (true || FALSE === ($project = $cache->load($key . '_project'))) {
//                    $prj_id = $wo->success ? $wo->data[0]->Project_ID : '';
//                    $project = $search->getProjects($this->_login, $this->_password, array($prj_id), '');
//                }
                if (!empty($wo->data[0]->Project_ID))
                {    
                    if($wo->data[0]->PartManagerUpdate_Required == 1)
                    {
                            $authData = array('login' => $this->_login,
                                'password' => $this->_password);
                            $tech = new Core_Api_TechUser;
                            $tech->checkAuthentication($authData);
                            $TechID = $tech->getTechId();
                        if(API_Abstract_ClientWorkOrder::checkPartEmptriesTechUpdated($win,$TechID))
                            {
                            $error = "FS Parts Manager is required and cannot be empty";
                        }
                        else
                        {
                            $success = 1;
                        }    
                    }
                    else
                    {
                        $success = 1;
                    }    
                }
                else
                {
                    $success = 1;
                }    
            }    
        }
        
        $json_output = array('success' => $success,'error' => $error);
        $response->clearAllHeaders()->clearBody();
        $response->setHeader('Content-type', 'application/json');
        $response->setBody(json_encode($json_output));
    }

    public function detailsAction() {
        require_once 'Core/Api/TechClass.php';

        $response = $this->getResponse();
        $request = $this->getRequest();
        $win = $request->getParam('win', NULL);
        $key = $request->getParam('key', NULL);

        $search = new Core_Api_TechClass();

        if (NULL === $win) {
            $response->setHttpResponseCode(406);
            return $response->setBody('Request is incorrect, WIN# is required and cannot be empty');
        } else {
            //019
            $isRestrictedByClient = $search->isRestrictedByClient($_SESSION['TechID'],$win);
            if($isRestrictedByClient)
            {
                $this->_helper->viewRenderer->setNoRender();
                $json_output = array('success' => 0, 'error' => "<br/>Your access to this Work Order has been restricted.", 'work_place' => '', 'sections' => '');
                $response->clearAllHeaders()->clearBody();
                $response->setHeader('Content-type', 'application/json');
                $response->setBody(json_encode($json_output));
                return;
            }
            //end 019
            $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');
            if (FALSE === ($wo = $cache->load($key))) {
                $paths = $this->getFrontController()->getControllerDirectory();
                if (!empty($paths['dashboard']) && file_exists($paths['dashboard'] . '/IndexController.php')) {
                    require_once $paths['dashboard'] . '/IndexController.php';
                    $fields = Dashboard_IndexController::FIELDS;
                } else {
                    $fields = NULL;
                }
                $wo = $search->getWorkOrder($this->_login, $this->_password, $win, $fields);
            }

            //Check if we have a valid WO.
            if (!$wo->success || !$wo->data[0]) {
                $this->_helper->viewRenderer->setNoRender();
                $json_output = array('success' => 0, 'error' => Core_Api_TechClass::ERROR_WO_INCORRECT_ID, 'work_place' => '', 'sections' => '');
                $response->clearAllHeaders()->clearBody();
                $response->setHeader('Content-type', 'application/json');
                $response->setBody(json_encode($json_output));
                return;
            }

            //Do not cache project info.
            if (true || FALSE === ($project = $cache->load($key . '_project'))) {
                $prj_id = $wo->success ? $wo->data[0]->Project_ID : '';
                $project = $search->getProjects($this->_login, $this->_password, array($prj_id), '');
            }

            $tech = new API_Tech();
            $id = (!empty($_SESSION['TechID']) ) ? $_SESSION['TechID'] : NULL;
            if ($id) {
                $tech->lookupID($id);
                $cache->save($tech, $key . '_tech', array(), 120);
            }

            $files = new Core_Files();
            $select = $files->select();
            $select->where('WIN_NUM = ?', $win);
            $filesList = $files->fetchAll($select);

            if (!empty($project->data[0]->Project_ID)) {
                $prjFiles = new Core_ProjectsFiles();
                $prjselect = $prjFiles->select();
                $prjselect->where('Project_ID = ?', $project->data[0]->Project_ID);
                $prjFilesList = $prjFiles->fetchAll($prjselect);

                //pull info for External File in project table
                $db = Core_Database::getInstance();
                $extSelect = $db->select();
                $extSelect->from(Core_Database::TABLE_PROJECTS, array("External_Files"))
                        ->where("Project_ID = ?", $project->data[0]->Project_ID);
                $extFiles = Core_Database::fetchAll($extSelect);
            }
        }
        $woCopy = clone $wo;
        $wo = $wo->data[0];
        $work_place = 'Work PLace';
        $sections = 'Sections';

        $this->view->wo = $woCopy;
        $this->view->project = $project;
        $this->view->tech = $tech;
        $this->view->prjFiles = $prjFilesList;
        $this->view->prjExtFile = $extFiles;
        $this->view->files = $filesList;
        $this->view->readOnly = $wo->Status != 'assigned' && $wo->Status != 'work done' && $wo->Status != 'incomplete';
		
		if (!$wo->TechCheckedIn_24hrs && $wo->Status == 'assigned') {
			// code from ivr
			$this->view->hrsBeforeStart = 0;
			try {
				$pacificDSActive = $mountainDSActive = $centralDSActive = $easternDSActive = date("I", $now);
	
				$TIME_STARTTIME_CAST = "CAST(CONCAT(work_orders_with_timezone_view.StartDate, ' ', (CASE WHEN STR_TO_DATE(work_orders_with_timezone_view.StartTime, '%l:%i %p') IS NULL THEN '00:00' ELSE STR_TO_DATE(work_orders_with_timezone_view.StartTime, '%l:%i %p') END)) AS DATETIME)";
				$TIME_LOCALZONE_OFFSET = "(StandardOffset + (CASE WHEN DST = '1' THEN (CASE WHEN StandardOffset = -5 THEN $easternDSActive WHEN StandardOffset = -6 THEN $centralDSActive WHEN StandardOffset = -7 THEN $mountainDSActive ELSE $pacificDSActive END) ELSE 0 END))";
				$TIME_CURRENT_TIME_LOCAL = "UTC_TIMESTAMP() + INTERVAL " . $TIME_LOCALZONE_OFFSET . " HOUR";
				$TIME_STARTTIME_HOURS_BEFORE_START = "((UNIX_TIMESTAMP( " . $TIME_STARTTIME_CAST . ") - UNIX_TIMESTAMP(" . $TIME_CURRENT_TIME_LOCAL . ")) / 3600)";
				$db = Core_Database::getInstance();
				$select = $db->select()->from('work_orders_with_timezone_view', array(new Zend_Db_Expr($TIME_STARTTIME_HOURS_BEFORE_START)))->where('WIN_NUM = ?', $win);
				$this->view->hrsBeforeStart = $db->fetchOne($select);
			} catch (Exception $e) {
			}
		}


        //
        //  Sections
        //

        $errorMessages = new Core_Form_ErrorHandler('wodetails_' . $win);
        $cfields = array();
        if (trim($wo->ProjectManagerName) || trim($wo->ProjectManagerEmail) || trim($wo->ProjectManagerPhone)) {
            $output = '<span class="info_box">Project Manager</span> <br />';
            $output .= ( trim($wo->ProjectManagerName) ) ? $wo->ProjectManagerName . '<br />' : '';
            $output .= ( trim($wo->ProjectManagerEmail) || trim($wo->ProjectManagerPhone) ) ? trim($wo->ProjectManagerPhone . ' | ' . $wo->ProjectManagerEmail, ' |') : '';
            $cfields[] = $output;
        }
        if (trim($wo->EmergencyName) || trim($wo->EmergencyEmail) || trim($wo->EmergencyPhone)) {
            $output = '<span class="info_box">Emergency Contact</span> <br />';
            $output .= ( trim($wo->EmergencyName) ) ? $wo->EmergencyName . '<br />' : '';
            $output .= ( trim($wo->EmergencyEmail) || trim($wo->EmergencyPhone) ) ? trim($wo->EmergencyPhone . ' | ' . $wo->EmergencyEmail, ' |') : '';
            $cfields[] = $output;
        }
        if (trim($wo->ResourceCoordinatorName) || trim($wo->ResourceCoordinatorPhone) || trim($wo->ResourceCoordinatorEmail)) {
            $output = '<span class="info_box">Resource Coordinator</span> <br />';
            $output .= ( trim($wo->ResourceCoordinatorName) ) ? $wo->ResourceCoordinatorName . '<br />' : '';
            $output .= ( trim($wo->ResourceCoordinatorPhone) || trim($wo->ResourceCoordinatorEmail) ) ? trim($wo->ResourceCoordinatorPhone . ' | ' . $wo->ResourceCoordinatorEmail, ' |') : '';
            $cfields[] = $output;
        }
//        if ( trim($wo->CheckInOutName) || trim($wo->CheckInOutNumber) || trim($wo->CheckInOutEmail) ) {
//            $output  = '<span class="info_box">Check In / Check Out Name</span> <br />';
//            $output .= ( trim($wo->CheckInOutName) ) ? $wo->CheckInOutName.'<br />' : '';
//            $output .= ( trim($wo->CheckInOutNumber) || trim($wo->CheckInOutEmail) ) ? trim($wo->CheckInOutNumber.' | '.$wo->CheckInOutEmail, ' |') : '';
//            $cfields[] = $output;
//        }
        if (trim($wo->TechnicalSupportName) || trim($wo->TechnicalSupportPhone) || trim($wo->TechnicalSupportEmail)) {
            $output = '<span class="info_box">Technical Contact</span> <br />';
            $output .= ( trim($wo->TechnicalSupportName) ) ? $wo->TechnicalSupportName . '<br />' : '';
            $output .= ( trim($wo->TechnicalSupportPhone) || trim($wo->TechnicalSupportEmail) ) ? trim($wo->TechnicalSupportPhone . ' | ' . $wo->TechnicalSupportEmail, ' |') : '';
            $cfields[] = $output;
        }

        //--- wo -> bidscount
        $bidsCount = 0;
        $db = Core_Database::getInstance();
        $s = $db->select()
                ->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('*'))
                ->where(Core_Database::TABLE_WORK_ORDER_BIDS . '.WorkOrderID = ?', $wo->WIN_NUM)
                ->where(Core_Database::TABLE_WORK_ORDER_BIDS . '.TechID = ?', $id);
        $s->order("id desc");
        $res = $db->fetchAll($s);
        $lastBid = null;
        if (!empty($res)) {
            $bidsCount = 1;
            $lastBid = $res[0];
        }

        //--- data to view
        $this->view->cfields = $cfields;
        $this->view->messages = $errorMessages->getMessages();
        $errorMessages->clearMessages();
        $this->view->bidsCount = $bidsCount;
        $Core_Api_WosClass = new Core_Api_WosClass;
        $Core_Api_ProjectClass = new Core_Api_ProjectClass();
        $expenseReporting = $Core_Api_WosClass->getWorkOrderTurnedOnBCats_insertFromProjectIfNotExists($win);
        $expenseReportingTotal = $Core_Api_WosClass->getTotalAmountArrayForWOBCats($win);
        $this->view->expenseReportingTotal = $expenseReportingTotal;
        $this->view->expenseReporting = $expenseReporting;
        $this->view->enableExpenseReporting = $Core_Api_ProjectClass->enableExpenseReporting($wo->Project_ID);
        $Core_Api_WosClass = new Core_Api_WosClass; 
        $this->view->AllWOVisitList = $Core_Api_WosClass->GetAllWOVisitList($win,1);
        $this->view->AllWOVisitListClient = $Core_Api_WosClass->GetAllWOVisitList($win,1,1);

		$this->view->required_certs = $Core_Api_ProjectClass->loadP2TCertRequirements ($win, false);

		$FSTagClass = new  Core_Api_FSTagClass();
		$FSTagList = $FSTagClass->getFSTagList_forTech ($id);
		$this->view->fs_tags = array ();

		foreach ($FSTagList as $key => $record) {
			if ($record["HideFromTechs"] == 1)
				$this->view->fs_tags[$record["FSTagId"]] = $record["Title"];
		}
		$this->view->fs_tags["2"] = "Dell MRA Compliant";
		$this->view->fs_tags["6"] = "DeVry Student";
		$this->view->fs_tags["46"] = "Black Box Cabling - Helper (C1)";
		$this->view->fs_tags["47"] = "Black Box Cabling - Advanced (C2)";
		$this->view->fs_tags["48"] = "Black Box Cabling - Lead (C3)";
		$this->view->fs_tags["49"] = "Black Box Telecom - Helper (T1)";
		$this->view->fs_tags["50"] = "Black Box Telecom - Advanced (T2)";
		$this->view->fs_tags["64"] = "FieldSolutions CheckINN Hotel Savings Card";

		$fs_experts = new Core_Api_FSExpertClass ();
		$fs_experts_list = $fs_experts->getFSExperts (1);
		$fs_experts = array ();
		foreach ($fs_experts_list as $record) {
			$fs_experts[$record["Id"]] = $record["Title"];
		}

		$this->view->fs_experts = $fs_experts;
		//error_log (var_export ($this->view->fs_experts, true));

		$this->view->public_certs = array (
			"Bg_Test_Pass_Lite" => "Basic Background Check",
			"Bg_Test_Pass_Full" => "Comprehensive Background Check",
			"DrugPassed" => "Gold Drug Test - 9 Panel",
			"SilverDrugPassed" => "Silver Drug Test - 5 Panel",
			"FS_Mobile" => "FS-Mobile&trade;",
			"BRT" => "Blue Ribbon Technician",
			"TBC" => "Boot Camp Certified",
			"USAuthorized" => "US Verified",
			"NACI" => "NACI Gov. Security Clearance",
			"InterimSecClear" => "Interim Gov. Security Clearance",
			"FullSecClear" => "Secret Gov. Security Clearance",
			"TopSecClear" => "Top Secret Gov. Security Clearance",
			"TopSecSCIClear" => "Top Secret SCI Gov. Security Clearance"
		);

		$this->view->industry_certs = array (
			"MCSE" => "Microsoft Certified Systems Engineer (MCSE)",
			"MCSA" => "Microsoft Certified Solutions Associate (MCSA)",
			"MCP" => "Microsoft Certified Professional (MCP)",
			"CCNA" => "Cisco Certified Network Associate (CCNA)",
			"DellCert" => "Dell Certification",
			"HP_CertNum" => "HP Certification",
			"CompTIA" => "CompTIA Registered",
			"CompTIA_A_PLUS" => "CompTIA A+",
			"CompTIA_PDI_PLUS" => "CompTIA PDI+",
			"CompTIA_Server_PLUS" => "CompTIA Server+",
			"CompTIA_Network_PLUS" => "CompTIA Network+",
			"BICSI" => "BICSI Certification",
			"EMCApD" => "EMCApD - Application Developer",
			"EMCBA" => "EMCBA - Backup Recovery",
			"EMCCA" => "EMCCA - Cloud Architect",
			"EMCCIS" => "EMCCIS - Cloud Infrastructure and Services",
			"EMCDCA" => "EMCDCA - Data Center Architect",
			"EMCIE" => "EMCIE - Implementation Engineer",
			"EMCISA" => "EMCISA - Information Storage and Management",
			"EMCPE" => "EMCPE - Platform Engineer",
			"EMCSA" => "EMCSA - Storage Administrator",
			"EMCSyA" => "EMCSyA - System Administrator",
			"EMCTA" => "EMCTA - Technology Architect"
		);

		$sections = $this->view->render('tech-wo-details/sections.phtml');

        //
        //  Work PLace
        //

		$cfields = array();
        if (trim($wo->ProjectManagerName) || trim($wo->ProjectManagerEmail) || trim($wo->ProjectManagerPhone)) {
            $output = '<span class="info_box">Project Manager</span> <br />';
            $output .= ( trim($wo->ProjectManagerName) ) ? $wo->ProjectManagerName . '<br />' : '';
            $output .= ( trim($wo->ProjectManagerEmail) || trim($wo->ProjectManagerPhone) ) ? trim($wo->ProjectManagerPhone . ' | ' . $wo->ProjectManagerEmail, ' |') : '';
            $cfields[] = $output;
        }
        if (trim($wo->EmergencyName) || trim($wo->EmergencyEmail) || trim($wo->EmergencyPhone)) {
            $output = '<span class="info_box">Emergency Contact</span> <br />';
            $output .= ( trim($wo->EmergencyName) ) ? $wo->EmergencyName . '<br />' : '';
            $output .= ( trim($wo->EmergencyEmail) || trim($wo->EmergencyPhone) ) ? trim($wo->EmergencyPhone . ' | ' . $wo->EmergencyEmail, ' |') : '';
            $cfields[] = $output;
        }
        if (trim($wo->ResourceCoordinatorName) || trim($wo->ResourceCoordinatorPhone) || trim($wo->ResourceCoordinatorEmail)) {
            $output = '<span class="info_box">Resource Coordinator</span> <br />';
            $output .= ( trim($wo->ResourceCoordinatorName) ) ? $wo->ResourceCoordinatorName . '<br />' : '';
            $output .= ( trim($wo->ResourceCoordinatorPhone) || trim($wo->ResourceCoordinatorEmail) ) ? trim($wo->ResourceCoordinatorPhone . ' | ' . $wo->ResourceCoordinatorEmail, ' |') : '';
            $cfields[] = $output;
        }
        if (trim($wo->CheckInOutName) || trim($wo->CheckInOutNumber) || trim($wo->CheckInOutEmail)) {
            $output = '<span class="info_box">Check In / Check Out Name</span> <br />';
            $output .= ( trim($wo->CheckInOutName) ) ? $wo->CheckInOutName . '<br />' : '';
            $output .= ( trim($wo->CheckInOutNumber) || trim($wo->CheckInOutEmail) ) ? trim($wo->CheckInOutNumber . ' | ' . $wo->CheckInOutEmail, ' |') : '';
            $cfields[] = $output;
        }
        if (trim($wo->TechnicalSupportName) || trim($wo->TechnicalSupportPhone) || trim($wo->TechnicalSupportEmail)) {
            $output = '<span class="info_box">Technical Contact</span> <br />';
            $output .= ( trim($wo->TechnicalSupportName) ) ? $wo->TechnicalSupportName . '<br />' : '';
            $output .= ( trim($wo->TechnicalSupportPhone) || trim($wo->TechnicalSupportEmail) ) ? trim($wo->TechnicalSupportPhone . ' | ' . $wo->TechnicalSupportEmail, ' |') : '';
            $cfields[] = $output;
        }
        $ACSCheck = $project->data[0]->Reminder24Hr ||
                $project->data[0]->Reminder1Hr ||
                $project->data[0]->ReminderNotMarkComplete ||
                $project->data[0]->ReminderAcceptance ||
                $project->data[0]->CheckInCall ||
                $project->data[0]->CheckOutCall ||
                $project->data[0]->ReminderAll ||
                $project->data[0]->ReminderIncomplete;

        if ($ACSCheck)
            $cfields[] = '<span class="info_box">ACS Check In</span><br /> (888) 557-4556';

		$this->view->lastwobid = $lastBid;
        $this->view->cfields = $cfields;
        $this->view->messages = null;
        $this->view->sections = $sections;

        $work_place = $this->view->render('tech-wo-details/work-place.phtml');

        $this->_helper->viewRenderer->setNoRender();

        $json_output = array('success' => 1, 'error' => '', 'work_place' => $work_place,'Status'=>  strtolower($wo->Status));
        $response->clearAllHeaders()->clearBody();
        $response->setHeader('Content-type', 'application/json');
        $response->setBody(json_encode($json_output));
    }

    public function workPlaceAction() {
        require_once 'Core/Api/TechClass.php';

        $response = $this->getResponse();
        $request = $this->getRequest();
        $win = $request->getParam('win', NULL);
        $key = $request->getParam('key', NULL);
        $search = new Core_Api_TechClass();

        if (NULL === $win) {
            $response->setHttpResponseCode(406);
            return $response->setBody('Request is incorrect, WIN# is required and cannot be empty');
        } else {
            $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');
            if (FALSE === ($wo = $cache->load($key))) {
                $paths = $this->getFrontController()->getControllerDirectory();
                if (!empty($paths['dashboard']) && file_exists($paths['dashboard'] . '/IndexController.php')) {
                    require_once $paths['dashboard'] . '/IndexController.php';
                    $fields = Dashboard_IndexController::FIELDS;
                } else {
                    $fields = NULL;
                }
                $wo = $search->getWorkOrder($this->_login, $this->_password, $win, $fields);
            }

            //Check if we have a valid WO.
            if (!$wo->success || !$wo->data[0]) {
                $this->view->wo = null;
                return;
            }

            //Do not cache project info.

            error_log("**** A $key " . print_r($cache->load($key . '_tech'), true));
            if (true || FALSE === ($project = $cache->load($key . '_project'))) {
                $prj_id = $wo->success ? $wo->data[0]->Project_ID : '';
                $project = $search->getProjects($this->_login, $this->_password, array($prj_id), '');
            }
            if (FALSE === ($tech = $cache->load($key . '_tech')) || (property_exists($tech, "TechID") && empty($tech->TechID))) {
                $tech = new API_Tech();
                // $id = ( !empty($_SESSION['FLSID']) ) ? $_SESSION['FLSID'] : ( ( !empty($_SESSION['TechID']) ) ? $_SESSION['TechID'] : NULL );
                $id = (!empty($_SESSION['TechID']) ) ? $_SESSION['TechID'] : NULL;
                if ($id) {
                    $tech->lookupID($id);
                    error_log("*********** not cached");
                    $cache->save($tech, $key . '_tech', array(), 120);
                }
            } else {
                error_log("*********** cached");
            }
        }

        $woCopy = clone $wo;
        $wo = $wo->data[0];
        $cfields = array();
        if (trim($wo->ProjectManagerName) || trim($wo->ProjectManagerEmail) || trim($wo->ProjectManagerPhone)) {
            $output = '<span class="info_box">Project Manager</span> <br />';
            $output .= ( trim($wo->ProjectManagerName) ) ? $wo->ProjectManagerName . '<br />' : '';
            $output .= ( trim($wo->ProjectManagerEmail) || trim($wo->ProjectManagerPhone) ) ? trim($wo->ProjectManagerPhone . ' | ' . $wo->ProjectManagerEmail, ' |') : '';
            $cfields[] = $output;
        }
        if (trim($wo->EmergencyName) || trim($wo->EmergencyEmail) || trim($wo->EmergencyPhone)) {
            $output = '<span class="info_box">Emergency Contact</span> <br />';
            $output .= ( trim($wo->EmergencyName) ) ? $wo->EmergencyName . '<br />' : '';
            $output .= ( trim($wo->EmergencyEmail) || trim($wo->EmergencyPhone) ) ? trim($wo->EmergencyPhone . ' | ' . $wo->EmergencyEmail, ' |') : '';
            $cfields[] = $output;
        }
        if (trim($wo->ResourceCoordinatorName) || trim($wo->ResourceCoordinatorPhone) || trim($wo->ResourceCoordinatorEmail)) {
            $output = '<span class="info_box">Resource Coordinator</span> <br />';
            $output .= ( trim($wo->ResourceCoordinatorName) ) ? $wo->ResourceCoordinatorName . '<br />' : '';
            $output .= ( trim($wo->ResourceCoordinatorPhone) || trim($wo->ResourceCoordinatorEmail) ) ? trim($wo->ResourceCoordinatorPhone . ' | ' . $wo->ResourceCoordinatorEmail, ' |') : '';
            $cfields[] = $output;
        }
        if (trim($wo->CheckInOutName) || trim($wo->CheckInOutNumber) || trim($wo->CheckInOutEmail)) {
            $output = '<span class="info_box">Check In / Check Out Name</span> <br />';
            $output .= ( trim($wo->CheckInOutName) ) ? $wo->CheckInOutName . '<br />' : '';
            $output .= ( trim($wo->CheckInOutNumber) || trim($wo->CheckInOutEmail) ) ? trim($wo->CheckInOutNumber . ' | ' . $wo->CheckInOutEmail, ' |') : '';
            $cfields[] = $output;
        }
        if (trim($wo->TechnicalSupportName) || trim($wo->TechnicalSupportPhone) || trim($wo->TechnicalSupportEmail)) {
            $output = '<span class="info_box">Technical Contact</span> <br />';
            $output .= ( trim($wo->TechnicalSupportName) ) ? $wo->TechnicalSupportName . '<br />' : '';
            $output .= ( trim($wo->TechnicalSupportPhone) || trim($wo->TechnicalSupportEmail) ) ? trim($wo->TechnicalSupportPhone . ' | ' . $wo->TechnicalSupportEmail, ' |') : '';
            $cfields[] = $output;
        }
        $ACSCheck = $project->data[0]->Reminder24Hr ||
                $project->data[0]->Reminder1Hr ||
                $project->data[0]->ReminderNotMarkComplete ||
                $project->data[0]->ReminderAcceptance ||
                $project->data[0]->CheckInCall ||
                $project->data[0]->CheckOutCall ||
                $project->data[0]->ReminderAll ||
                $project->data[0]->ReminderIncomplete;

        if ($ACSCheck)
            $cfields[] = '<span class="info_box">ACS Check In</span><br /> (888) 557-4556';

        $this->view->cfields = $cfields;
        $this->view->tech = $tech;
        $this->view->wo = $woCopy;
        $this->view->project = $project;
    }

    public function sectionsAction() {
        require_once 'Core/Api/TechClass.php';

        $response = $this->getResponse();
        $request = $this->getRequest();
        $win = $request->getParam('win', NULL);
        $key = $request->getParam('key', NULL);
        $search = new Core_Api_TechClass();

        if (NULL === $win) {
            $response->setHttpResponseCode(406);
            return $response->setBody('Request is incorrect, WIN# is required and cannot be empty');
        } else {
            $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');
            if (FALSE === ($wo = $cache->load($key))) {
                $paths = $this->getFrontController()->getControllerDirectory();
                if (!empty($paths['dashboard']) && file_exists($paths['dashboard'] . '/IndexController.php')) {
                    require_once $paths['dashboard'] . '/IndexController.php';
                    $fields = Dashboard_IndexController::FIELDS;
                } else {
                    $fields = NULL;
                }
                $wo = $search->getWorkOrder($this->_login, $this->_password, $win, $fields);
            }

            //Check if we have a valid WO.
            if (!$wo->success || !$wo->data[0]) {
                $this->view->wo = null;
                return;
            }

            if (FALSE === ($project = $cache->load($key . '_project'))) {
                $prj_id = $wo->success ? $wo->data[0]->Project_ID : '';
                $project = $search->getProjects($this->_login, $this->_password, array($prj_id), '');
            }
        }

        $errorMessages = new Core_Form_ErrorHandler('wodetails_' . $win);

        $woCopy = clone $wo;
        $wo = $wo->data[0];
        $cfields = array();
        if (trim($wo->ProjectManagerName) || trim($wo->ProjectManagerEmail) || trim($wo->ProjectManagerPhone)) {
            $output = '<span class="info_box">Project Manager</span> <br />';
            $output .= ( trim($wo->ProjectManagerName) ) ? $wo->ProjectManagerName . '<br />' : '';
            $output .= ( trim($wo->ProjectManagerEmail) || trim($wo->ProjectManagerPhone) ) ? trim($wo->ProjectManagerPhone . ' | ' . $wo->ProjectManagerEmail, ' |') : '';
            $cfields[] = $output;
        }
        if (trim($wo->EmergencyName) || trim($wo->EmergencyEmail) || trim($wo->EmergencyPhone)) {
            $output = '<span class="info_box">Emergency Contact</span> <br />';
            $output .= ( trim($wo->EmergencyName) ) ? $wo->EmergencyName . '<br />' : '';
            $output .= ( trim($wo->EmergencyEmail) || trim($wo->EmergencyPhone) ) ? trim($wo->EmergencyPhone . ' | ' . $wo->EmergencyEmail, ' |') : '';
            $cfields[] = $output;
        }
        if (trim($wo->ResourceCoordinatorName) || trim($wo->ResourceCoordinatorPhone) || trim($wo->ResourceCoordinatorEmail)) {
            $output = '<span class="info_box">Resource Coordinator</span> <br />';
            $output .= ( trim($wo->ResourceCoordinatorName) ) ? $wo->ResourceCoordinatorName . '<br />' : '';
            $output .= ( trim($wo->ResourceCoordinatorPhone) || trim($wo->ResourceCoordinatorEmail) ) ? trim($wo->ResourceCoordinatorPhone . ' | ' . $wo->ResourceCoordinatorEmail, ' |') : '';
            $cfields[] = $output;
        }
        /* if ( trim($wo->CheckInOutName) || trim($wo->CheckInOutNumber) || trim($wo->CheckInOutEmail) ) {
          $output  = '<span class="info_box">Check In / Check Out Name</span> <br />';
          $output .= ( trim($wo->CheckInOutName) ) ? $wo->CheckInOutName.'<br />' : '';
          $output .= ( trim($wo->CheckInOutNumber) || trim($wo->CheckInOutEmail) ) ? trim($wo->CheckInOutNumber.' | '.$wo->CheckInOutEmail, ' |') : '';
          $cfields[] = $output;
          } */
        if (trim($wo->TechnicalSupportName) || trim($wo->TechnicalSupportPhone) || trim($wo->TechnicalSupportEmail)) {
            $output = '<span class="info_box">Technical Contact</span> <br />';
            $output .= ( trim($wo->TechnicalSupportName) ) ? $wo->TechnicalSupportName . '<br />' : '';
            $output .= ( trim($wo->TechnicalSupportPhone) || trim($wo->TechnicalSupportEmail) ) ? trim($wo->TechnicalSupportPhone . ' | ' . $wo->TechnicalSupportEmail, ' |') : '';
            $cfields[] = $output;
        }

        $this->view->cfields = $cfields;
        $this->view->wo = $woCopy;
        $this->view->project = $project;
        $this->view->messages = $errorMessages->getMessages();
        $errorMessages->clearMessages();
    }

    public function updateWoAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $request = $this->getRequest();
        $updater = new Core_Api_TechClass();

        $win = $request->getParam('win', NULL);

        $response = $this->getResponse();
        $response
                ->clearAllHeaders()
                ->clearBody();
        $output = '';

        $worder = $updater->getWorkOrder($this->_login, $this->_password, $win);
        if (!$worder->success) {
            $response->setHttpResponseCode(406);
            $output = $result->errors[0]->message;
            return;
        } else {
            $worder = $worder->data[0];
        }

        $wo = new API_TechWorkOrder();
        if ($request->has('WorkOrderReviewed')) {
            $wo->WorkOrderReviewed = $request->getParam('WorkOrderReviewed', ( $worder->WorkOrderReviewed ) ? 'True' : 'False');
            $wo->WorkOrderReviewed = ucfirst(strtolower($wo->WorkOrderReviewed));
        } else {
            $wo->WorkOrderReviewed = ( $worder->WorkOrderReviewed ) ? 'True' : 'False';
        }

        if ($request->has('TechCheckedIn_24hrs')) {
            $wo->TechCheckedIn_24hrs = $request->getParam('TechCheckedIn_24hrs', ( $worder->TechCheckedIn_24hrs ) ? 'True' : 'False');
            $wo->TechCheckedIn_24hrs = ucfirst(strtolower($wo->TechCheckedIn_24hrs));
        } else {
            //982
            $startdate = $worder->StartDate;
            $day1 = new Zend_Date($startdate, 'yyyy-MM-dd');

            if($day1->isEarlier(Zend_Date::now()->subDay(2)))
            {
               $wo->TechCheckedIn_24hrs = ( $worder->TechCheckedIn_24hrs ) ? 'True' : 'False';
            }
            else
            {
                $daynow = new Zend_Date(Zend_Date::now(), 'yyyy-MM-dd');
                if($daynow->isEarlier($day1->subDay(2)))
                {
                    $wo->TechCheckedIn_24hrs = ( $worder->TechCheckedIn_24hrs ) ? 'True' : 'False';
                }
                else
                {
                    $wo->TechCheckedIn_24hrs =  'True';
                }
            }
            //end 982
        }
        if ($request->has('TechMarkedComplete')) {
            $wo->TechMarkedComplete = ucfirst(strtolower($request->getParam('TechMarkedComplete', 'False')));
            $wo->CheckedIn = 'True';
            $wo->Date_In = trim($request->getParam('Date_In', ''));
            $wo->Time_In = strtoupper(trim($request->getParam('Time_In', '')));
            $wo->Date_Out = trim($request->getParam('Date_Out', ''));
            $wo->Time_Out = strtoupper(trim($request->getParam('Time_Out', '')));
            $wo->TechComments = trim($request->getParam('TechComments', ""));

            $loadStart = strtotime(substr(trim($worder->Date_In), 0, strpos(trim($worder->Date_In), ' ')) . ' ' . $worder->Time_In);
            $loadEnd = strtotime(substr(trim($worder->Date_Out), 0, strpos(trim($worder->Date_Out), ' ')) . ' ' . $worder->Time_Out);
            $upStart = strtotime($wo->Date_In . ' ' . $wo->Time_In);
            $upEnd = strtotime($wo->Date_Out . ' ' . $wo->Time_Out);
            if ($loadStart !== $upStart || $loadEnd !== $upEnd) {
                $isNeedUpdate = true;
//                $wo->calculatedTechHrs = round(0 + $worder->calculatedTechHrs + ( $upEnd - $upStart ) / 3600, 2);
                $wo->calculatedTechHrs = round(0 + ( $upEnd - $upStart ) / 3600, 2);
            }
        }

        $removefiles = $request->getParam('removefiles', null);
        $result = $updater->updateWorkOrder($this->_login, $this->_password, $win, $wo);
        if (!$result->success) {
            $response->setHttpResponseCode(406);
            $output = $result->errors[0]->message;
        } else {
            if (!empty($removefiles) && is_array($removefiles)) {
                $files = new Core_Files();
                $files->removeFiles($win, $removefiles);
            }
            $output = "Work Order has beed updated";
            // send email
            //Do not cache project info.
        }
        
        $response
                ->setHeader('Content-Type', 'text/html')
                ->setHeader('Content-Length', strlen($output))
                ->setBody($output);
    }

    public function fileDownloadAction() {
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();

        $filename = $request->getParam('filename', false);
        if ($filename !== false) {
            $filename = base64_decode($filename);
        }

        $response = $this->getResponse();
        $response->clearHeaders();
        $response->clearBody();
        $response
                ->setHeader('Content-Type', 'application/octet-stream', true)
                ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
                ->setHeader("Content-Disposition", "attachment; filename=\"$filename\"")
                ->setHeader('Last-Modified', date('D, d M Y H:i:s T'), true);

        $getter = new Core_Api_TechClass();
        $side = $request->getParam('side', null);
        $file = $getter->downloadFile($this->_login, $this->_password, $filename, $side);

        if ($file === NULL || $file instanceof API_Response) {
            $response->setHeader('Content-Length', 0, true)
                    ->setBody('');
        } else {
            //$file = base64_decode($file);
            $response->setHeader('Content-Length', strlen($file), true)
                    ->setBody($file);
        }
    }

    public function uploadFileAction() {
        $this->_helper->viewRenderer->setNoRender(true);

        $request = $this->getRequest();
        $response = $this->getResponse();
        $response
                ->clearHeaders()
                ->clearBody();

        $win = $request->getParam('win', NULL);
        $id = $request->getParam('file', NULL);
        $descr = $request->getParam('descr', '');

        if (
                !empty($_FILES["PicGreenTech{$id}"]) &&
                is_uploaded_file($_FILES["PicGreenTech{$id}"]['tmp_name']) &&
                is_readable($_FILES["PicGreenTech{$id}"]['tmp_name']) &&
                $_FILES["PicGreenTech{$id}"]['error'] == UPLOAD_ERR_OK &&
                filesize($_FILES["PicGreenTech{$id}"]['tmp_name']) <= self::FILE_MAX_SIZE &&
                filesize($_FILES["PicGreenTech{$id}"]['tmp_name']) > 0
        ) {
            $uploader = new Core_Api_TechClass();
            $fname = $_FILES["PicGreenTech{$id}"]['name'];
            if (strrpos($fname, '/') !== FALSE) {
                $fname = substr($fname, strrpos($fname, '/'));
            }

            /**
             * Generate unig name for file
             * @author Artem Sukharev
             * @see https://tickets.fieldsolutions.com/issues/12180
             */
            $info = pathinfo($fname);
            $fname = uniqid (str_replace (",", "", $info['filename']) . '_');
            $fname = $fname . '.' . $info['extension'];

            $fdata = file_get_contents($_FILES["PicGreenTech{$id}"]['tmp_name']);
            $result = $uploader->uploadFileAWS($this->_login, $this->_password, $win, $fname, $fdata, $descr);

            if (!$result->success) {
                $output = json_encode(array('success' => 0, 'message' => $result->errors[0]->message));
            } else {
                $output = json_encode(array('success' => 1, 'name' => $fname, 'filename' => urlencode(base64_encode('/' . $fname)), 'id' => $result, 'description' => $descr));
            }
        } else {
            $output = array('success' => 0);

            if (empty($_FILES["PicGreenTech{$id}"])) {
                $output['message'][] = 'File not uploaded';
            } else if (!is_uploaded_file($_FILES["PicGreenTech{$id}"]['tmp_name'])) {
                $output['message'][] = 'File not uploaded.';
            } else if (!is_readable($_FILES["PicGreenTech{$id}"]['tmp_name'])) {
                $output['message'][] = 'Uploaded file not readabale';
            } else if (filesize($_FILES["PicGreenTech{$id}"]['tmp_name']) > self::FILE_MAX_SIZE) {
                $output['message'][] = 'Uploaded file size too big. Please call resource coordinator for assistance.';
            } else if (filesize($_FILES["PicGreenTech{$id}"]['tmp_name']) == 0) {
                $output['message'][] = 'Uploaded file is empty.';
            } else if ($_FILES["PicGreenTech{$id}"]['error'] != UPLOAD_ERR_OK) {
                switch ($_FILES["PicGreenTech{$id}"]['error']) {
                    case UPLOAD_ERR_FORM_SIZE :
                        $output['message'][] = 'Uploaded file size too big. Please call resource coordinator for assistance.';
                        break;
                    case UPLOAD_ERR_PARTIAL :
                        $output['message'][] = 'The uploaded file was only partially uploaded';
                        break;
                    case UPLOAD_ERR_NO_FILE :
                    case UPLOAD_ERR_NO_TMP_DIR :
                    case UPLOAD_ERR_CANT_WRITE :
                    case UPLOAD_ERR_EXTENSION :
                        $output['message'][] = 'No file was uploaded';
                }
            }
            error_log('FileUploading: (techs:UFA, win:' . $win . ',id: ' . $id . ') ' . implode('|', $output['message']));

            $output = json_encode($output);
        }
        $response->setBody($output);
    }

    public function woFullUpdateAction() {
		$this->_helper->viewRenderer->setNoRender(true);
        $request = $this->getRequest();
        $win = $request->getParam('win', 0) + 0;
        $hidsavetype = $request->getParam('hidsavetype', 2);
        $errorMessages = new Core_Form_ErrorHandler('wodetails_' . $win);
		$validate_errors = Core_Api_Error::getInstance();
        $updater = new Core_Api_TechClass();
		$time_pattern1 = "/^(1[0-2]|0?\d)\s*(am|pm)$/i";
		$time_pattern2 = "/^(1[0-2]|0?\d)\:([0-5]\d)\s*(am|pm)$/i";

        $loadedWO = $updater->getWorkOrder($this->_login, $this->_password, $win);
        $toUpdateWO = new API_TechWorkOrder();

        if (!$loadedWO->success) {
            $errorMessages->addMessage('Update WO failed: Incorrect WIN# given');
            $this->_redirect($_SERVER['HTTP_REFERER']);
        } else {
            $loadedWO = $loadedWO->data[0];
        }

        $toUpdateWO->WorkOrderReviewed = ( $loadedWO->WorkOrderReviewed ) ? 'True' : 'False';
        $toUpdateWO->TechCheckedIn_24hrs = ( $loadedWO->TechCheckedIn_24hrs ) ? 'True' : 'False';

        if ($request->has('WorkOrderReviewed')) {
            $toUpdateWO->WorkOrderReviewed = ( strtolower($request->getParam('WorkOrderReviewed', 'false')) === 'true' ) ? 'True' : 'False';
        }
        if ($request->has('TechCheckedIn_24hrs')) {
            $toUpdateWO->TechCheckedIn_24hrs = ( strtolower($request->getParam('TechCheckedIn_24hrs' . 'false')) === 'true' ) ? 'True' : 'False';
        }

        $toUpdateWO->TechMarkedComplete = $request->getParam('TechMarkedComplete', 'false');
        $toUpdateWO->TechMarkedComplete = ( trim(strtolower($toUpdateWO->TechMarkedComplete)) === 'true' ) ? 'True' : 'False';
        $toUpdateWO->Date_In = trim($request->getParam('Date_In', ''));
        $toUpdateWO->Date_In = ( empty($toUpdateWO->Date_In) ) ? NULL : $toUpdateWO->Date_In;
        $toUpdateWO->Date_Out = trim($request->getParam('Date_Out', ''));
        $toUpdateWO->Date_Out = ( empty($toUpdateWO->Date_Out) ) ? NULL : $toUpdateWO->Date_Out;
        $toUpdateWO->Time_In = trim($request->getParam('Time_In', ''));
        $toUpdateWO->Time_In = ( empty($toUpdateWO->Time_In) ) ? NULL : $toUpdateWO->Time_In;
        $toUpdateWO->Time_Out = trim($request->getParam('Time_Out', ''));
        $toUpdateWO->Time_Out = ( empty($toUpdateWO->Time_Out) ) ? NULL : $toUpdateWO->Time_Out;

		if (!empty ($toUpdateWO->Time_In) && !preg_match ($time_pattern1, $toUpdateWO->Time_In) && !preg_match ($time_pattern2, $toUpdateWO->Time_In)) {
			$toUpdateWO->Time_In = "";
		}
		if (!empty ($toUpdateWO->Time_Out) && !preg_match ($time_pattern1, $toUpdateWO->Time_Out) && !preg_match ($time_pattern2, $toUpdateWO->Time_Out)) {
			$toUpdateWO->Time_Out = "";
		}

		if (
                $toUpdateWO->TechMarkedComplete === 'True' ||
                $toUpdateWO->Date_Out ||
                $toUpdateWO->Date_In ||
                $toUpdateWO->Time_Out ||
                $toUpdateWO->Time_In
        ) {
            $toUpdateWO->CheckedIn = 'True';
        }

        if ($request->has('ContactHD')) {
            $toUpdateWO->ContactHD = $request->getParam('ContactHD', '');
            $toUpdateWO->ContactHD = ( $toUpdateWO->ContactHD === '' ) ? NULL : ( ( $toUpdateWO->ContactHD == 1 ) ? 'True' : 'False' );
        }
        if ($request->has('HDPOC')) {
            $toUpdateWO->HDPOC = $request->getParam('HDPOC', NULL);
            $toUpdateWO->HDPOC = ( trim(strtolower($toUpdateWO->HDPOC)) !== 'notes on additional contacts' ) ? $toUpdateWO->HDPOC : NULL;
        }
        if ($request->has('TechMiles')) {
            $toUpdateWO->TechMiles = $request->getParam('TechMiles', 0);
            $toUpdateWO->TechMiles = ( trim(strtolower($toUpdateWO->TechMiles)) !== 'enter miles' ) ? $toUpdateWO->TechMiles : NULL;
        }
        if ($request->has('TechComments')) {
            $toUpdateWO->TechComments = $request->getParam('TechComments', NULL);
            $toUpdateWO->TechComments = ( trim(strtolower($toUpdateWO->TechComments)) !== 'comments are required' && trim(strtolower($toUpdateWO->TechComments)) !== 'close out comments are required' ) ? $toUpdateWO->TechComments : NULL;
        }

        /* FLS Version of Tech WO */
        if ($request->has('Unexpected_Steps')) {
            $toUpdateWO->Unexpected_Steps = ( $request->getParam('Unexpected_Steps', 0) + 0 ) ? 'True' : 'False';
            $toUpdateWO->Unexpected_Desc = $request->getParam('Unexpected_Desc', '');
            $toUpdateWO->AskedBy = $request->getParam('AskedBy', '');
            $toUpdateWO->AskedBy_Name = $request->getParam('AskedBy_Name', '');
        }
        /* FLS Version of Tech WO */

        $isNeedUpdate = true;

        $loadStart = strtotime(substr(trim($loadedWO->Date_In), 0, strpos(trim($loadedWO->Date_In), ' ')) . ' ' . $loadedWO->Time_In);
        $loadEnd = strtotime(substr(trim($loadedWO->Date_Out), 0, strpos(trim($loadedWO->Date_Out), ' ')) . ' ' . $loadedWO->Time_Out);
        $upStart = strtotime($toUpdateWO->Date_In . ' ' . $toUpdateWO->Time_In);
        $upEnd = strtotime($toUpdateWO->Date_Out . ' ' . $toUpdateWO->Time_Out);
//        if ($loadStart !== $upStart || $loadEnd !== $upEnd) {
//            $isNeedUpdate = true;
//            $toUpdateWO->calculatedTechHrs = round(0 + ( $upEnd - $upStart ) / 3600, 2);
//        }

        $removefiles = $request->getParam('removefiles', null);
   		 if(!empty($removefiles)){
            $files = new Core_Files();
            $files->removeFiles($win, $request->getParam('removefiles'));
         }
        if($request->has('CheckInDate1')){
            $i = 1;
            $Core_Api_WosClass = new Core_Api_WosClass;
            $requiredTechDateTime = 0;
            if($toUpdateWO->TechMarkedComplete === 'True'){
                $requiredTechDateTime = 1;
            }
            $er = false;
            while ($request->has('CheckInDate'.$i) && $i <= 100){
                if(!$request->has('CheckInDate'.$i)) break;
                $WoVisitID = $request->getParam('WoVisitID'.$i, 0);
                $CheckInDate = $request->getParam('CheckInDate'.$i, "");
                $CheckInDate = !empty($CheckInDate)?date_format(new DateTime($CheckInDate), "Y-m-d"):"";
                $CheckInTime = $request->getParam('CheckInTime'.$i, "");
                $CheckOutDate = $request->getParam('CheckOutDate'.$i, "");
                $CheckOutDate = !empty($CheckOutDate)?date_format(new DateTime($CheckOutDate), "Y-m-d"):"";
                $CheckOutTime = $request->getParam('CheckOutTime'.$i, "");

				if (!empty ($CheckInTime) && !preg_match ($time_pattern1, $CheckInTime) && !preg_match ($time_pattern2, $CheckInTime)) {
					$CheckInTime = "";
					$errorMessages->addMessage("Please enter a valid check in time");
					$isNeedUpdate = false;
				}
				if (!empty ($CheckOutTime) && !preg_match ($time_pattern1, $CheckOutTime) && !preg_match ($time_pattern2, $CheckOutTime)) {
					$CheckOutTime = "";
					$errorMessages->addMessage("Please enter a valid check out time");
					$isNeedUpdate = false;
				}

                $visittype = '';
                if(!empty($WoVisitID)){
					$VisitInfo = $Core_Api_WosClass->getWOVisitInfo($WoVisitID);
                    $visittype = $VisitInfo["Type"];
                }
                //Simply ignore visits that are completely empty
                if ((!empty($CheckInDate) && !empty($CheckInTime) && !empty($CheckOutDate) && !empty($CheckOutTime))
                     || (!empty($WoVisitID) && empty($CheckInDate) && empty($CheckInTime) && empty($CheckOutDate) && empty($CheckOutTime))   
                   ) {
	                $error = $Core_Api_WosClass->saveWOVisitCheckInOutForTech(
                            $WoVisitID, $win, $CheckInDate, $CheckInTime, $CheckOutDate, $CheckOutTime, $requiredTechDateTime);

                    if ($validate_errors->hasErrors() && !$er) {
                        $err_array = $validate_errors->getErrorArray();

		                	foreach ($err_array as $error_msg) {
		                		$errorMessages->addMessage($error_msg["description"]);
		                	}
		                    $isNeedUpdate = false;
		                    $er = true;
                    }
	           }
               //end 13882
               
                //--- 13583, remove visit if it is created by tech and check in/check out are empty.
                if($visittype == 2 && !empty($WoVisitID) && empty($CheckInDate) && empty($CheckInTime) && empty($CheckOutDate) && empty($CheckOutTime))
                {
                    $Core_Api_WosClass->removeWOVisit($WoVisitID);
                }
                
                //--- continue to loop
                $i++;
                $validate_errors->resetErrors();
            }
            $Core_Api_WosClass->syncNumVisits($win);
	}
        
        if($isNeedUpdate && $loadedWO->Approved == 0 )
        {
            $apiWosClass = new Core_Api_WosClass();
            $totalduration = $apiWosClass->getTotatDurationOnSite($win);
            $toUpdateWO->calculatedTechHrs = $totalduration;
            $isNeedUpdate = true;
        }

        if ($isNeedUpdate) {
            $result = $updater->updateWorkOrder($this->_login, $this->_password, $win, $toUpdateWO);
            // update Expense Reporting 
            $ExpenseReporting = $request->getParam('ExpenseReporting', array());
            if(!empty($ExpenseReporting))
            {
                $Core_Api_WosClass = new Core_Api_WosClass;
                
                for($i = 0;$i < count($ExpenseReporting["bcatid"]); $i++)
                {
                    $bcatid = $ExpenseReporting["bcatid"][$i];
                    if(!empty($ExpenseReporting["bcatid"][$i]))
                    {    
                        $techClaim = $ExpenseReporting["tech_claim"][$i];
                        $Core_Api_WosClass->updateTechClaimForWOBCat($bcatid,$techClaim);
                    }
                    // delete file
                    $RemoveExpenseReporting = $request->getParam('RemoveExpenseReporting'.$bcatid, array());
                    
                    if(!empty($RemoveExpenseReporting))
                    {
                        $files = new Core_Files();
                        $files->removeFiles($win, $RemoveExpenseReporting);
                    }    
                    // upload file
                    if (!empty($_FILES)) {
                        $msgs = array();
                        foreach ($_FILES as $k => $fileStruc) {
                            if (!strrpos($k, "ExpenseReporting".$bcatid."_"))
                                continue;
                            
                            if ($fileStruc['error'] == UPLOAD_ERR_OK &&
                                    is_uploaded_file($fileStruc['tmp_name']) &&
                                    filesize($fileStruc['tmp_name']) > 0
                            ) {
                                $fname = $fileStruc['name'];
                                $filename = $fname;
                                if (strrpos($fname, '/') !== FALSE) {
                                    $fname = substr($fname, strrpos($fname, '/'));
                                }
                                $info = pathinfo($fname);
								$fname = uniqid (str_replace (",", "", $info['filename']) . '_');
                                $fname = $fname . '.' . $info['extension'];

                                $fdata = file_get_contents($fileStruc['tmp_name']);

                                $file = new Core_File();
                                $uploadResult = $file->uploadFile($fname, $fdata, S3_CLIENTS_DOCS_DIR);
                                if(!$uploadResult){
                                    $errorMessages->addMessage( 'Upload file '.$fName.' failed');
                                }
                                else {
                                    $Core_Api_WosClass->addDocumentForWOBCat($win,$bcatid,$fname,$filename);
                                }
                            } else {

                                if ($fileStruc['error'] != UPLOAD_ERR_NO_FILE) {
                                    $errorMessages->addMessage('File "' . $fileStruc['name'] . '" upload failed');
                                } 
                                $errorsDetails = $errorMessages->getMessages();
                                //error_log('FileUploading: (tech:fWUA, win:' . $win . ',field: ' . $saveField . ') ' . implode('|', $errorsDetails));
                            }
                        }
                    }
                    
                   
            }
            if (!$result->success) {
                $errorMessages->addMessage($result->errors[0]->message);
                $errorMessages->addMessage((array) $toUpdateWO);
                $this->_redirect($_SERVER['HTTP_REFERER']);
            } elseif (!empty($removefiles) && is_array($removefiles)) {
                $files = new Core_Files();
                $files->removeFiles($win, $removefiles);
            }
        }
        if (!empty($_FILES)) {
            $msgs = array();

            foreach ($_FILES as $k => $fileStruc) {
                if (empty($fileStruc['tmp_name']) ||  strrpos($k, "ExpenseReporting"))
                    continue;
                $descr = trim($request->getParam('Descr' . $k, NULL));

                if ($fileStruc['error'] == UPLOAD_ERR_OK &&
                        is_uploaded_file($fileStruc['tmp_name']) &&
                        filesize($fileStruc['tmp_name']) > 0 &&
                        strlen($descr) <= 25
                ) {
                    $fname = $fileStruc['name'];
                    if (strrpos($fname, '/') !== FALSE) {
                        $fname = substr($fname, strrpos($fname, '/'));
                    }

                    /**
                     * Generate unig name for file
                     * @author Artem Sukharev
                     * @see https://tickets.fieldsolutions.com/issues/12180
                     */
                    $info = pathinfo($fname);
					$fname = uniqid (str_replace (",", "", $info['filename']) . '_');
                    $fname = $fname . '.' . $info['extension'];

                    $fdata = file_get_contents($fileStruc['tmp_name']);

                    $result = $updater->uploadFileAWS($this->_login, $this->_password, $win, $fname, $fdata, $descr);

                    if (!$result->success) {
                        $msgs[] = $result->errors[0]->message;
                    }
                } else {

                    if ($fileStruc['error'] != UPLOAD_ERR_NO_FILE) {
                        $errorMessages->addMessage('File "' . $fileStruc['name'] . '" upload failed');
                                } else if (!is_uploaded_file($fileStruc['tmp_name'])) {
                                    $errorMessages->addMessage('File "' . 'File not uploaded.');
                                } else if (filesize($fileStruc['tmp_name']) == 0) {
                                    $errorMessages->addMessage('File "' . 'Uploaded file is empty.');
                                } else if (strlen($descr) > 25) {
                                    $errorMessages->addMessage('Optional description for uploaded file should containes less then 25 characters.');
                                }
                                $errorsDetails = $errorMessages->getMessages();
                                //error_log('FileUploading: (tech:fWUA, win:' . $win . ',field: ' . $saveField . ') ' . implode('|', $errorsDetails));
                            }
                        }
                
                    }
                    
         		
            if (!empty($msgs)) {
                $msgs = array_unique($msgs);
                foreach ($msgs as $msg) {
                    $errorMessages->addMessage($msg);
                }
            }
        }
        
         
        $err = "";
        $this->messages = $errorMessages->getMessages();
        $errorMessages->clearMessages();
        if (!empty($this->messages))
        {
            foreach ($this->messages as $e)
            {
                if (is_string($e))
                {
                    $err .= '<li>' . $e . '</li>';
    }
            }
            echo "<script>
                window.parent.woDetails.showerrmess('$err');
                </script>";
        }else{
            echo "<script>
                if('$hidsavetype' == '1'){
                    window.parent._wosTechDetails.leavePageYes();
                }
                else{
                    window.parent._wosTechDetails.reloadPage();
                }
                </script>";
        }
        //$this->_redirect($_SERVER['HTTP_REFERER']);
    }
    
    public function checkSavedDupeDocsAction(){
    	$this->_helper->viewRenderer->setNoRender(true);
    	$request = $this->getRequest();
    	$docNames = explode(",",$request->getParam('docNames'));
    	$winNum = $request->getParam('win');
    	
    	$wosClass = new Core_Api_WosClass();
    	$savedDocs = $wosClass->getDocsForWOByWIN($winNum);
    	$isDupe = false;
    	if(!empty($savedDocs)){
    		foreach($savedDocs as $s){
    			$sDoc = substr_replace($s['path'], "", strpos($s['path'],"_"), strrpos($s['path'],".")).".".pathinfo($s['path'], PATHINFO_EXTENSION);
    			foreach($docNames as $d){
    				$d = str_replace("C:\\fakepath\\", "", $d);
    				if($d == $sDoc)$isDupe=true;
    			}
    		}
    	}
    	$json_output = array('isDupe' => $isDupe);
    	$response = $this->getResponse();
        $response->clearHeaders();
        $response->clearBody();
        $response->setHeader('Content-type', 'application/json');
        $response->setBody(json_encode($json_output));
    }
    

}
