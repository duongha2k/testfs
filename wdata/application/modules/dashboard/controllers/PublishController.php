<?php

class Dashboard_PublishController extends Zend_Controller_Action
{
    public function countryRequireZipAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $params = $this->getRequest()->getParams();

        $return = array('success' => false);

        if(isset($params["countrycode"]))
        {
            $Country = Core_Country::getCountryInfoByCode($params["countrycode"]);
            if(!empty($Country))
            {
                if($Country["ZipRequired"] == "1")
                {
                    $return = array('success' => true);
                } 
            }
        }
        
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                ->setBody($return);
    }
}
