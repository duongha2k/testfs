<?php
class Dashboard_CommentsController extends Core_Controller_Action
{
    public function init()
    {
        parent::init();

        $request = $this->getRequest();
        $auth = new Zend_Session_Namespace('Auth_User');
        $this->view->name = $auth->name;
        $this->view->company = $auth->companyId;
		$this->_company = empty($_SESSION['PM_Company_ID']) ? null : $_SESSION['PM_Company_ID'];
    }
	
	private function getSort($sort) {
		$sortCol = explode(" ", $sort);
		$sort = '';
		switch ($sortCol[0]) {
			case 'comment':
				$sort = 'date';
				break;
			case 'likes':
				$sort = 'classification';
				break;
			case 'rating':
				$sort = 'SATPerformance';
				break;
			case 'skillCategory':
				$sort = 'WO_Category';
				break;
			default:
				$sort = $sortCol[0];
		}
		return $sort . ' ' . $sortCol[1];
	}
	
	public function getWoCommentsAction() {
		$c = new Core_Comments;
        $params = $this->getRequest()->getParams();
		$user = new Core_Api_User();
        $clientInfo = $user->load(
            array(
                'login' => $this->_login.'|pmContext='.$this->_company,
                'password' => $this->_password
            )
        );
		if (empty($params['id']) || $params['id'] == 'null') $params['id'] = NULL;
		if (empty($params['win']) || $params['win'] == 'null') $params['win'] = NULL;
		$origSort = $params['sort'];
		if (empty($params['sort'])) $params['sort'] = NULL;
		else
			$params['sort'] = array($this->getSort($params['sort']));
		
		if (!empty($clientInfo))
			$this->view->comments = $c->getWorkOrderComments($params['id'], $clientInfo, $params['win'], array($params['sort']));
		else
			$this->view->comments = null;
        
        $clientID = $clientInfo->getClientId();    
        $classApi = new Core_Api_Class();
        $this->view->isGPM = $classApi->isGPM($clientID);    
		$this->view->clientID = $clientID;
        
        
		if (!empty($origSort)) {
			$s = explode(" ", $origSort);
			$this->view->sort = $s[0];
			$this->view->sortDir = $s[1];
		}
	}

	public function getTechCommentsAction() {
		$c = new Core_Comments;
        $params = $this->getRequest()->getParams();
		$user = new Core_Api_User();
        $clientInfo = $user->load(
            array(
                'login' => $this->_login.'|pmContext='.$this->_company,
                'password' => $this->_password
            )
        );
		$origSort = $params['sort'];
		if (empty($params['sort'])) $params['sort'] = NULL;
		else
			$params['sort'] = array($this->getSort($params['sort']));
		if (!empty($clientInfo))
			$this->view->comments = $c->getTechComments($params['id'], $clientInfo, array($params['sort']));
		else
			$this->view->comments = null;
       
        $this->view->pmContext = $this->_company;     
        $this->view->Company_ID = $clientInfo->getCompanyId();   
        $clientID = $clientInfo->getClientId();    
        $classApi = new Core_Api_Class();
        $this->view->isGPM = $classApi->isGPM($clientID);
		$this->view->clientID = $clientID;
		if (!empty($origSort)) {
			$s = explode(" ", $origSort);
			$this->view->sort = $s[0];
			$this->view->sortDir = $s[1];
		}
	}

	public function setHelpfulAction() {
		$user = new Core_Api_User();
        $clientInfo = $user->load(
            array(
                'login' => $this->_login.'|pmContext='.$this->_company,
                'password' => $this->_password
            )
        );
        $params = $this->getRequest()->getParams();
		$c = new Core_CommentsHelpful;
		if ($params['flag'] == "null") {
			$c->clearHelpful($params['id'], $user->getClientId());
		}
		else if ($params['flag'] == 1) {
			$c->helpful($params['id'], $user->getClientId());
		}
		else if ($params['flag'] == 0) {
			$c->notHelpful($params['id'], $user->getClientId());
		}
		$this->_helper->json(true);
	}

	public function reportAbuseAction() {
		$user = new Core_Api_User();
        $clientInfo = $user->load(
            array(
                'login' => $this->_login.'|pmContext='.$this->_company,
                'password' => $this->_password
            )
        );
        $params = $this->getRequest()->getParams();
		$c = new Core_Comments;
		$c->reportAbuse($params['id'], $user);
		$this->_helper->json(true);
	}

    public function updateAction()
    {
        $request   = $this->getRequest();

        $user = new Core_Api_User();
        $clientInfo = $user->load(
            array(
                'login' => $this->_login.'|pmContext='.$this->_company,
                'password' => $this->_password
            )
        );
        $this->view->id = $this->getRequest()->getParam('id', null);
        $this->view->techID = $this->getRequest()->getParam('techID', null);
        $this->view->WIN_NUM = $this->getRequest()->getParam('WIN_NUM', null);
		$c = new Core_Comments;
		if ($this->view->id != null) {
			$comment = $c->getComment($this->view->id, $clientInfo);
			if (!empty($comment))
				$this->view->comment = $comment;
			else
				$this->view->comment = new Core_Comment;
		}
    }
    

    public function doUpdateAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getRequest()->getParams();
		$c = new Core_Comments;
        $user = new Core_Api_User();
        $clientInfo = $user->load(
            array(
                'login' => $this->_login.'|pmContext='.$this->_company,
                'password' => $this->_password
            )
        );
		if (empty($params['WIN_NUM'])) $params['WIN_NUM'] = NULL;
		if (!empty($params['id'])) $c->edit($params['id'], $clientInfo, $params['comment'], $params['type'], $params['classification']);
		else $c->add($params['comment'], $clientInfo, $params['techID'], $params['WIN_NUM'], $params['type'], $params['classification']);
		
		echo "<div style='text-align: center'>Comment added</div>
<script type='text/javascript'>
		parent.commentDone();
</script>
";
    }
    
}
