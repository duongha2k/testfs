<?php

class Dashboard_CustomReportController extends Core_Controller_Action {

    private $_size;
    private $_page;

    function getFieldReportAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();
        $reportid = $request->getParam('reportid', "");
        $CusReportClass = new Core_Api_CusReportClass();
        $field = $CusReportClass->getListofFields_InTable($reportid);
        $this->_helper->json($field);
    }

    function doCreateReportAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $CusReportClass = new Core_Api_CusReportClass();
        $params = $this->getRequest()->getParams();

        $SelectFields = $params['collist'];
        $SelectFilters = $params['filterlist'];
        $extfields = $params["data"];
        $fieldCodeArrange = $params["arrangelist"];
        $fieldArray = array();
        foreach ($SelectFields as $item)
        {
            $fieldArray[$item["fieldcode"]] = $item["fieldname"];
        }

        $reportName = $params["reportname"];

//--- validate
        if (!empty($reportName))
        {
            $existReportName = $CusReportClass->exists_ReportName($params['company'], $reportName, '');
            if ($existReportName)
            {
                $this->_helper->json(array("success" => false, "error" => 'Report Name exists.'));
                return;
            }
        }

        $repID = $CusReportClass->add($params['company'], $params['reportname'], $params['reporttable'], $fieldArray, $SelectFilters);

        $grpID = 1;
        if ($repID > 0)
        {

            $saveReportColumn = $CusReportClass->saveReportFields($repID, $grpID, $extfields, $fieldCodeArrange);
            if (!empty($saveReportColumn))
            {
                $errstr = $saveReportColumn;
                $success = false;
            }
        }

        $this->_helper->json(array("success" => $fieldArray, "repID" => $repID));
    }

    function doUpdateReportAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $CusReportClass = new Core_Api_CusReportClass();
        $params = $this->getRequest()->getParams();

        $SelectFields = json_decode($params['SelectFields']);

        $fieldArray = array();

        foreach ($SelectFields as $item)
        {
            $fieldArray[$item->value] = $item->key;
        }
        $reportID = $params["report_id"];
        $companyID = $params["company"];
        $repName = $params["reportname"];
        $repTable = $params["reporttable"];

        $CusReportClass->edit($reportID, $companyID, $repName, $repTable, $fieldArray);

        $this->_helper->json(array("success" => $fieldArray));
    }

    function doDeleteReportAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $CusReportClass = new Core_Api_CusReportClass();
        $params = $this->getRequest()->getParams();
        $CusReportClass->delete($params["report_id"]);
        $this->_helper->json(array("success" => true));
    }

    function doAddFilterReportAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $CusReportClass = new Core_Api_CusReportClass();
        $params = $this->getRequest()->getParams();
        $reportid = $params["report_id"];
        $fieldid = $params["fieldid"];

        $key_value_array = $params["key_value_array"];

        if ($fieldid != "")
        {
            $CusReportClass->updateFilter($reportid, $fieldid, $key_value_array[0]);
        } else
        {
            $fieldcode = $key_value_array[0]['fieldcode'];
            $CusReportClass->addFilter($reportid, $key_value_array[0]);
            $CusReportClass->addReportColumn($reportid, $fieldcode);
        }

        $this->_helper->json(array("success" => true));
    }

    function loadReportAction()
    {

        $objparams = $this->getRequest()->getParams();
        $rptid = $objparams['rptid'];
        if (!empty($objparams['page']))
        {
            $this->_page = $objparams['page'];
        } else
        {
            $this->_page = 1;
        }

        if (!empty($objparams['size']))
        {
            $this->_size = $objparams['size'];
        } else
        {
            $this->_size = 50;
        }

        $txtTemplateName = $objparams['txtTemplateName'];
        $v = $objparams['v'];

        $Core_Report = new Core_Api_CusReportClass();

//--- save last_run_date
        $updateFields = array();
        $updateFields['last_run_date'] = date("Y-m-d H:i:s");

        $saveReport = $Core_Report->saveReport($rptid, $updateFields);

//--- fieldlist

        $FieldList = $Core_Report->getReportFieldList_forDisplay($rptid);

//--- filters
        $filters = array();
        foreach ($objparams as $k => $v)
        {
            if (strpos($k, 'Filter') !== false)
            {
                $part = explode('__', $k);
                $filterID = $part[0];
                $field = $part[1];
                if (!isset($filters[$filterID]))
                {
                    $filters[$filterID] = array();
                }
                $filters[$filterID][$field] = $v;
            }
        }

//--- sorts
        $sorts = array();
        if (!empty($objparams['Sort1__fieldcode']))
        {
            $sorts['sort_1'] = $objparams['Sort1__fieldcode'];
            if (!empty($objparams['Sort1__sortdir']))
            {
                $sorts['sort_dir_1'] = $objparams['Sort1__sortdir'];
            } else
            {
                $sorts['sort_dir_1'] = 'asc';
            }
        }

        if (!empty($objparams['Sort2__fieldcode']))
        {
            $sorts['sort_2'] = $objparams['Sort2__fieldcode'];
            if (!empty($objparams['Sort2__sortdir']))
            {
                $sorts['sort_dir_2'] = $objparams['Sort2__sortdir'];
            } else
            {
                $sorts['sort_dir_2'] = 'asc';
            }
        }

        if (!empty($objparams['Sort3__fieldcode']))
        {
            $sorts['sort_3'] = $objparams['Sort3__fieldcode'];
            if (!empty($objparams['Sort3__sortdir']))
            {
                $sorts['sort_dir_3'] = $objparams['Sort3__sortdir'];
            } else
            {
                $sorts['sort_dir_3'] = 'asc';
            }
        }

//--- groups
        $groups = array();
        foreach ($objparams as $k => $v)
        {
            if (strpos($k, 'chkgro') !== false)
            {
                $pos1 = strpos($k, '_');
                $fieldName = substr($k, $pos1 + 1);
                $groups[] = $fieldName;
            }
        }

//--- counts
        $counts = array();
        foreach ($objparams as $k => $v)
        {
            if (strpos($k, 'chkcou') !== false)
            {
                $pos1 = strpos($k, '_');
                $fieldName = substr($k, $pos1 + 1);
                $counts[] = $fieldName;
            }
        }

//--- sums
        $sums = array();
        foreach ($objparams as $k => $v)
        {
            if (strpos($k, 'chksum') !== false)
            {
                $pos1 = strpos($k, '_');
                $fieldName = substr($k, $pos1 + 1);
                $sums[] = $fieldName;
            }
        }

//--- query
        $countRows = 0;
        if ((!empty($groups) && count($groups) > 0) ||
                (!empty($counts) && count($counts) > 0) ||
                (!empty($sums) && count($sums) > 0)
        )
        {
            $BodyListALL = $Core_Report->runReport($rptid, $filters, 0, 0, false, null, $groups, $counts, $sums);
            if (!empty($BodyListALL))
            {
                $countRows = count($BodyListALL);
            }
        } else
        {
            $countRows = $Core_Report->runReport($rptid, $filters, 0, 0, true);
        }

        $BodyList = $Core_Report->runReport($rptid, $filters, $this->_size, ($this->_page - 1) * $this->_size, false, $sorts, $groups, $counts, $sums);

        $paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($BodyList, $countRows));

        $paginator->setItemCountPerPage($this->_size);
        $paginator->setCurrentPageNumber($this->_page);
        $this->view->paginator = $paginator;

        $this->view->FieldList = $FieldList;
        $this->view->size = $this->_size;
        $this->view->rptid = $rptid;
        $this->view->txtTemplateName = $txtTemplateName;
        $this->view->Company_ID = $v;
    }

    function doAddColReportAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $CusReportClass = new Core_Api_CusReportClass();
        $params = $this->getRequest()->getParams();
        $errstr = "";
        $success = true;
        $repID = $params["rptid"];
        $fieldCode = $params["colcode"];

        $addReportColumn = $CusReportClass->addReportColumn($repID, $fieldCode);
        if (!empty($addReportColumn))
        {
            $errstr = $addReportColumn;
            $success = false;
        }

        $this->_helper->json(array("success" => $success, "error" => $errstr));
    }

    function doDeleteColReportAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $CusReportClass = new Core_Api_CusReportClass();
        $params = $this->getRequest()->getParams();
        $errstr = "";
        $success = true;
        $repID = $params["rptid"];
        $fieldCode = $params["fieldcode"];
        $delReportColumn = $CusReportClass->deleteReportColumn($repID, $fieldCode);
        if (!empty($delReportColumn))
        {
            $errstr = $delReportColumn;
            $success = false;
        }

        $this->_helper->json(array("success" => $success, "error" => $errstr));
    }

    function doDeleteFilterReportAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $CusReportClass = new Core_Api_CusReportClass();
        $params = $this->getRequest()->getParams();
        $errstr = "";
        $success = true;
        $repID = $params["rptid"];
        $filterID = $params["filterID"];

        $delReportColumn = $CusReportClass->deleteReportFilter($repID, $filterID);
        if ($delReportColumn != 1)
        {
            $errstr = $delReportColumn;
            $success = false;
        }

        $this->_helper->json(array("success" => $success, "error" => $errstr));
    }

    function doSaveArrangeReportAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $CusReportClass = new Core_Api_CusReportClass();
        $params = $this->getRequest()->getParams();
        $errstr = "";
        $success = true;
        $repID = $params["rptid"];
        $fieldCode = $params["colcode"];
        $saveReportColumn = $CusReportClass->reArrangeColumns($repID, $fieldCode);
        if (!empty($saveReportColumn))
        {
            $errstr = $saveReportColumn;
            $success = false;
        }

        $this->_helper->json(array("success" => $success, "error" => $errstr));
    }

    function doGenerateReportAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $CusReportClass = new Core_Api_CusReportClass();
        $params = $this->getRequest()->getParams();
        $errstr = "";
        $success = true;
        $repID = $params["rptid"];
        $grpID = $params["grpid"];
        $fieldCode = $params["data"];
        $fieldCodeArrange = $params["arrangelist"];
        $reportName = $params["reportname"];

        $reportInfo = $CusReportClass->getReportInfo($repID);
        $companyID = $reportInfo['companyid'];
        $currentReportName = $reportInfo['rpt_name'];
//--- validate
        if (!empty($reportName))
        {
            $existReportName = $CusReportClass->exists_ReportName($companyID, $reportName, $currentReportName);
            if ($existReportName)
            {
                $this->_helper->json(array("success" => false, "error" => 'Report Name exists.'));
                return;
            }
        }
//--- update 
        $authUser = $_SESSION['Auth_User'];
        $loginName = $authUser['login'];
        $nameOfCurrentUser = $authUser['name'];
        $updateFields = array();
        if (!empty($reportName))
        {
            $updateFields['rpt_name'] = $reportName;
        }
        $updateFields['lastupdated_date'] = date("Y-m-d H:i:s");
        $updateFields['last_modified_by'] = $nameOfCurrentUser;

        if (count($updateFields) > 0)
        {
//lastupdated_date
            $saveReport = $CusReportClass->saveReport($repID, $updateFields);
        }

        $saveReportColumn = $CusReportClass->saveReportFields($repID, $grpID, $fieldCode, $fieldCodeArrange);
        if (!empty($saveReportColumn))
        {
            $errstr = $saveReportColumn;
            $success = false;
        }

        $this->_helper->json(array("success" => $success, "error" => $errstr));
    }

    public function doSaveReportFiltersAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $CusReportClass = new Core_Api_CusReportClass();
        $params = $this->getRequest()->getParams();

        $repID = $params["reportid"];
        $companyID = $params["company"];
        $filterList = $params['filterlist'];
        $sortList = $params['sortlist'];
        $formatList = $params['formatlist'];

        $avaiKeyArray = array('fieldcode', 'filter_at', 'datetime_relative_data_ref', 'datetime_range_from', 'datetime_range_to', 'string_matching', 'string_value', 'bool_value', 'numeric_base_value', 'numeric_range_from', 'numeric_range_to', 'datetime_range_int_from', 'datetime_range_int_to');

//--- save filter list
        if (!empty($repID) && !empty($filterList) && is_array($filterList) && count($filterList) > 0)
        {

            foreach ($filterList as $filter)
            {
                if (!empty($filter) && is_array($filter) && count($filter) > 0)
                {
                    $filterID = $filter['filterid'];
                    $updateFields = array();
                    foreach ($filter as $field => $value)
                    {
                        if (in_array($field, $avaiKeyArray))
                        {
                            $updateFields[$field] = $value;
                        }
                    }

                    if (count($updateFields) > 0)
                    {
                        $CusReportClass->updateFilter($repID, $filterID, $updateFields);
                    }
                }
            }// end of for
//echo("filterList: ");print_r($filterList);die(); 
        }// end of if
//--- save sort list
        if (!empty($repID) && !empty($sortList) && is_array($sortList) && count($sortList) > 0)
        {

            $updateFields = array();
            if (!empty($sortList['Sort1__fieldcode']))
            {
                $updateFields['sort_1'] = $sortList['Sort1__fieldcode'];
                if (!empty($sortList['Sort1__sortdir']))
                {
                    $updateFields['sort_dir_1'] = $sortList['Sort1__sortdir'];
                } else
                {
                    $updateFields['sort_dir_1'] = 'asc';
                }
            }
            if (!empty($sortList['Sort2__fieldcode']))
            {
                $updateFields['sort_2'] = $sortList['Sort2__fieldcode'];
                if (!empty($sortList['Sort2__sortdir']))
                {
                    $updateFields['sort_dir_2'] = $sortList['Sort2__sortdir'];
                } else
                {
                    $updateFields['sort_dir_2'] = 'asc';
                }
            }
            if (!empty($sortList['Sort3__fieldcode']))
            {
                $updateFields['sort_3'] = $sortList['Sort3__fieldcode'];
                if (!empty($sortList['Sort3__sortdir']))
                {
                    $updateFields['sort_dir_3'] = $sortList['Sort3__sortdir'];
                } else
                {
                    $updateFields['sort_dir_3'] = 'asc';
                }
            }
            if (count($updateFields) > 0)
            {
                $CusReportClass->saveReport($repID, $updateFields);
            }
//echo("sortList: ");print_r($sortList);die();            
        }

//--- save format list
        if (!empty($repID) && !empty($formatList) && is_array($formatList) && count($formatList) > 0)
        {

            $updFields = array();
            foreach ($formatList as $format)
            {
                $formatFieldCode = $format['fieldcode'];
                $fieldVal = $format['fieldval'];

                $parts = split('_', $formatFieldCode);
                $field = array();
                $field['fieldname'] = $parts[1];
                if ($parts[0] == 'chksum')
                {
                    $field['issum'] = $fieldVal;
                } else if ($parts[0] == 'chkcou')
                {
                    $field['iscount'] = $fieldVal;
                } else if ($parts[0] == 'chkgro')
                {
                    $field['isgroupby'] = $fieldVal;
                } else
                {
                    $field['isdisplayed'] = 1;
                }
                $updFields[] = $field;
            }
            $CusReportClass->saveReportFields($repID, 1, $updFields);
//echo("updFields: ");print_r($updFields);die();            
        }
    }

    public function loadProjectAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getRequest()->getParams();
        $ComID = $params["ComID"];
        $CusReportClass = new Core_Api_CusReportClass();
        $ProjectList = $CusReportClass->getPrjectList_ByCompanyID($ComID);


        $str = "";
        if (!empty($ProjectList))
        {
            $Projects = array();
            $i = 0;
            foreach ($ProjectList as $item)
            {
                $Projects[$i] = array("Project_ID" => $item['Project_ID'], "Project_Name" => $item['Project_Name'], "Project_Type" => "");
                $i++;
            }

            $str = $this->loadContenttoTable($Projects, "Project");
        }
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $response->setHeader('Content-Length', strlen($str), true);
        $response->setBody($str);
    }

//custom report V2
    public function loadFilterFieldAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getRequest()->getParams();
        $ComID = $params["ComID"];
        $TypeID = $params["TypeID"];
        $CusReportClass = new Core_Api_CusReportClass();

        $auth = new Zend_Session_Namespace('Auth_User');
        $client = new Core_Api_Class();
        $isPM = $client->isGPMByUserName($auth->login);
        $isGPM = $isPM;
        $colnoArr = array("fieldwoInfo" => 2, "fieldwoDates" => 2, "fieldsite" => 1, "fieldbid" => 1, "fieldtechnicians" => 1, "fieldCommunications" => 1, "fieldconfirmation" => 3, "partmanager" => 1, "wintag" => 2, "fieldPricing" => 3, "fieldadditional" => 1);
        switch ($TypeID)
        {
            case "Project":
                $resultList = $CusReportClass->getPrjectList_ByCompanyID($ComID);
                break;
            case "wostatus":
                $resultList = $CusReportClass->getWOStatusList();
                break;
            case "partmanager":
                $resultList = $CusReportClass->getFieldList_ByLocation(Core_Api_CusReportClass::FieldFilter_Location_WO_PartsManager, $ComID, $isGPM);
                break;
            case "fieldwoInfo":
                $resultList = $CusReportClass->getFieldList_ByLocation("Basic Work Order Information", $ComID, $isGPM);
                break;
            case "fieldwoDates":
                $resultList = $CusReportClass->getFieldList_ByLocation("Dates & Times", $ComID, $isGPM);
                break;
            case "fieldsite":
                $resultList = $CusReportClass->getFieldList_ByLocation(Core_Api_CusReportClass::FieldFilter_Location_WO_Site_Information, $ComID, $isGPM);
                break;
            case "fieldbid":
                $resultList = $CusReportClass->getFieldList_ByLocation(Core_Api_CusReportClass::FieldFilter_Location_WO_Bid_Information, $ComID, $isGPM);
                break;
            case "fieldtechnicians":
                $resultList = $CusReportClass->getFieldList_ByLocation(Core_Api_CusReportClass::FieldFilter_Location_WO_Technicians, $ComID, $isGPM);
                break;
            case "fieldCommunications":
                $resultList = $CusReportClass->getFieldList_ByLocation(Core_Api_CusReportClass::FieldFilter_Location_WO_Communications, $ComID, $isGPM);
                break;
            case "fieldconfirmation":
                $resultList = $CusReportClass->getFieldList_ByLocation(Core_Api_CusReportClass::FieldFilter_Location_WO_Confirmations_Close_Out, $ComID, $isGPM);
                break;
            case "wintag":
                $resultList = $CusReportClass->getFieldList_ByLocation(Core_Api_CusReportClass::FieldFilter_Location_WO_WinTags, $ComID, $isGPM);
                break;
            case "fieldPricing":
                $resultList = $CusReportClass->getFieldList_ByLocation(Core_Api_CusReportClass::FieldFilter_Location_WO_Pricing_Pay_Details, $ComID, $isGPM);
                break;
            case "fieldadditional":
                $resultList = $CusReportClass->getFieldList_ByLocation(Core_Api_CusReportClass::FieldFilter_Location_WO_Additional_Information, $ComID, $isGPM);
                break;
        }
//        print_r("resultList<pre>");
//
//        print_r($resultList);
//        print_r("</pre>");
        $str = "";
        if (!empty($resultList))
        {
            $result = array();
            $i = 0;
            $key1 = $TypeID . "_ID";
            $key2 = $TypeID . "_Name";
            $key3 = $TypeID . "_Type";

            if ($TypeID == "Project")
            {
                foreach ($resultList as $item)
                {
                    $result[$i] = array($key1 => $item['Project_ID'], $key2 => $item['Project_Name'], $key3 => "");
                    $i++;
                }
            } else if ($TypeID == "wostatus")
            {
                foreach ($resultList as $Key => $Val)
                {
                    $result[$i] = array($key1 => $Key, $key2 => $Val, $key3 => "");
                    $i++;
                }
            } else
            {
                foreach ($resultList as $item)
                {
                    if ($ComID != "FLS" && $item["FLSOnly"] == 0)
                    {
                        $result[$i] = array($key1 => $item['field_code'], $key2 => $item['field_title'], $key3 => $item['FilterType']);
                    } else if ($ComID == "FLS")
                    {
                        $result[$i] = array($key1 => $item['field_code'], $key2 => $item['field_title'], $key3 => $item['FilterType']);
                    }
                    $i++;
                }
            }
            if (!empty($result))
            {
                $str = $this->loadContenttoTable($result, $TypeID, $ComID, $colnoArr);
            }
        }

        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $response->setHeader('Content-Length', strlen($str), true);
        $response->setBody($str);
    }

    private function loadContenttoTable($content, $prefix, $ComID, $colnoArr)
    {
        if ($prefix == "Project" || $prefix == "wostatus")
        {
            $str = $this->build2colFieldTable($content, $prefix);
        } else
        {
            $ordersection = array("fieldwoInfo", "fieldsite", "fieldbid", "fieldtechnicians", "fieldCommunications", "fieldconfirmation", "fieldPricing", "wostatus", "fieldwoDates", "partmanager", "wintag", "fieldadditional");

            if ($prefix == "fieldconfirmation")
            {
                if ($ComID == "FLS")
                {
                    $rowarray = array("1" => 9, "2" => 9, "3" => 12);
                } else
                {
                    $rowarray = array("1" => 4, "2" => 6, "3" => 6);
                }
                $headerarr = array("1" => "Pre-Start Confirmations", "2" => "Client Notes", "3" => "Tech Close Out");
            } else if ($prefix == "fieldPricing")
            {
                $rowarray = array("1" => 5, "2" => 10, "3" => 14);
                $headerarr = array("1" => "Base Tech Pay", "2" => "Additional Tech Pay", "3" => "Final Tech Pay");
            } else if ($prefix == "wintag")
            {
                $rowarray = array("1" => 9, "2" => 5);
                $headerarr = array("1" => "Completion Status WIN-Tags", "2" => "On-Time & Quality WIN-Tags");
            } else if ($prefix == "fieldwoInfo")
            {
                $rowarray = array("1" => 13, "2" => 11);
                $headerarr = array("1" => "Basic Information", "2" => "Work Description");
            } else if ($prefix == "fieldwoDates")
            {
                $rowarray = array("1" => 10, "2" => 8);
                $headerarr = array("1" => "Scheduled Visit", "2" => "Actual Check In/Out Times");
            } else
            {
                $rowarray = array("1" => count($content));
            }

            if (in_array($prefix, $ordersection))
            {
                $content = $this->reOrder($content, $prefix);
            }
            $str = $this->build_N_colFieldTable($content, $prefix, $colnoArr[$prefix], $rowarray, $headerarr);
        }
        return $str;
    }

    private function build_N_colFieldTable($content, $prefix, $colno, $rowarray, $headerarr)
    {
        $str = "<table cellspacing='0' cellpadding='0' width='100%'>";
        if ($colno == 1)
        {
            $str .= "<col width='2%'>
                <col width='*'>";
        } else if ($colno == 2)
        {
            $str .= "<col width='2%'>
                <col width='48%'>
                <col width='2%'>
                <col width='48%'>";
        } else if ($colno == 3)
        {
            $str .= "<col width='2%'>
                <col width='28%'>
                <col width='2%'>
                <col width='28%'>
                <col width='2%'>
                <col width='28%'>";
        }

        if (!empty($headerarr))
        {
            if ($colno == 1)
            {
                $str .= "
                <tr>
                    <th colspan='2'>" . $headerarr["1"] . "</th>
                </tr>";
            } else if ($colno == 2)
            {
                $str .= "
                <tr>
                    <th colspan='2'>" . $headerarr["1"] . "</th>
                    <th colspan='2'>" . $headerarr["2"] . "</th>
                </tr>";
            } else if ($colno == 3)
            {
                $str .= "
                <tr>
                    <th colspan='2'>" . $headerarr["1"] . "</th>
                    <th colspan='2'>" . $headerarr["2"] . "</th>
                    <th colspan='2'>" . $headerarr["3"] . "</th>
                </tr>";
            }
        }
        $undisableItem = array("Qty_Applicants", "_BidPayMax", "_BidAmount_Per", "_NumofNewParts", "_NumofReturnParts", "_PartManagerUpdatedByTech");
        $maxrow = 0;
        foreach ($rowarray as $item)
        {
            if ($maxrow < $item)
                $maxrow = $item;
        }
        for ($i = 0; $i < $maxrow; $i++)
        {
            $disable = "";
            $str.="<tr>";
            $itemno = $i;

            for ($j = 0; $j < $colno; $j++)
            {
                if (!empty($rowarray) && $j > 0)
                {
                    $itemno += $rowarray[$j];
                }
                $fieldname = htmlspecialchars((empty($content[$itemno][$prefix . "_Name"]) ? "" : $content[$itemno][$prefix . "_Name"]), ENT_QUOTES);

                $str.="<td style='vertical-align:top;'>";
                if ($prefix == "fieldbid" || $prefix == "partmanager" || $prefix == "wintag")
                {
                    $disable = "disabled";
                }
                if (in_array($content[$itemno][$prefix . "_ID"], $undisableItem))
                {
                    $disable = "";
                }

                if (!empty($content[$itemno][$prefix . "_ID"]) && $i < $rowarray[$j + 1])
                {

                    $str.="<input type='checkbox' section='" . $prefix . "' id='" . $content[$itemno][$prefix . "_ID"] . "' " . $prefix . "id='" . $content[$itemno][$prefix . "_ID"] . "' " . $prefix . "name='" . $fieldname . "' " . $prefix . "_Type='" . $content[$itemno][$prefix . "_Type"] . "' " . $disable . ">";
                }

                $str.="</td>";
                $str.="<td style='vertical-align:top;'>";
                if (!empty($content[$itemno][$prefix . "_Name"]) && $i < $rowarray[$j + 1])
                {
                    $str.=$fieldname;
                }
                $str.="</td>";
            }
            $str.="</tr>";
        }
        $str.="</table>";
        return $str;
    }

    private function build2colFieldTable($content, $prefix)
    {
        $str = "<table cellspacing='0' cellpadding='0' width='100%'>
                <col width='5%'>
                <col width='45%'>
                <col width='5%'>
                <col width='45%'>";
        $rowno = round((count($content) / 2));
        for ($i = 0; $i <= $rowno - 1; $i++)
        {
            $disable = "";
            $str.="<tr>";
            $fieldname = htmlspecialchars((empty($content[$i][$prefix . "_Name"]) ? "" : $content[$i][$prefix . "_Name"]), ENT_QUOTES);
            $str.="<td style='vertical-align:top;'>";
            if ($prefix == "fieldbid" || $prefix == "partmanager" || $prefix == "fieldwoDates" || $prefix == "wintag")
            {
                $disable = "disabled";
            }
            if (!empty($content[$i][$prefix . "_ID"]))
            {
                if ($content[$i][$prefix . "_ID"] == "Qty_Applicants" || $content[$i][$prefix . "_ID"] == "_BidPayMax" || $content[$i][$prefix . "_ID"] == "_BidAmount_Per" || $content[$i][$prefix . "_ID"] == "_NumofNewParts" || $content[$i][$prefix . "_ID"] == "_NumofReturnParts" || $content[$i][$prefix . "_ID"] == "_PartManagerUpdatedByTech")
                {
                    $disable = "";
                }
                $str.="<input type='checkbox' section='" . $prefix . "' id='" . $content[$i][$prefix . "_ID"] . "' " . $prefix . "id='" . $content[$i][$prefix . "_ID"] . "' " . $prefix . "name='" . $fieldname . "' " . $prefix . "_Type='" . $content[$i][$prefix . "_Type"] . "' " . $disable . ">";
            }
            $str.="</td>";
            $str.="<td style='vertical-align:top;'>";
            if (!empty($content[$i][$prefix . "_Name"]))
            {
                $str.=$fieldname;
            }
            $str.="</td>";

            $str.="<td style='vertical-align:top;'>";
            if ($prefix == "fieldbid" || $prefix == "partmanager" || $prefix == "fieldwoDates" || $prefix == "wintag")
            {
                $disable = "disabled";
            }
            if (!empty($content[$i + $rowno][$prefix . "_ID"]))
            {
                if ($content[$i + $rowno][$prefix . "_ID"] == "Qty_Applicants" || $content[$i + $rowno][$prefix . "_ID"] == "_BidPayMax" || $content[$i + $rowno][$prefix . "_ID"] == "_BidAmount_Per" || $content[$i + $rowno][$prefix . "_ID"] == "_NumofNewParts" || $content[$i + $rowno][$prefix . "_ID"] == "_NumofReturnParts" || $content[$i + $rowno][$prefix . "_ID"] == "_PartManagerUpdatedByTech")
                {
                    $disable = "";
                }
                $str.="<input type='checkbox' section='" . $prefix . "' id='" . $content[$i + $rowno][$prefix . "_ID"] . "' " . $prefix . "id='" . $content[$i + $rowno][$prefix . "_ID"] . "' " . $prefix . "name='" . $content[$i + $rowno][$prefix . "_Name"] . "' " . $prefix . "_Type='" . $content[$i + $rowno][$prefix . "_Type"] . "' " . $disable . ">";
            }
            $str.="</td>";
            $str.="<td style='vertical-align:top;'>";
            if (!empty($content[$i + $rowno][$prefix . "_Name"]))
            {
                $str.=$content[$i + $rowno][$prefix . "_Name"];
            }
            $str.="</td>";
            $str.="</tr>";
        }
        $str.="</table>";
        return $str;
    }

    private function reOrder($content, $prefix)
    {
        $FieldOrder = array(
            "wostatus" => array(
                "created" => "0",
                "published" => "1",
                "assigned" => "2",
                "work_done" => "3",
                "incomplete" => "4",
                "approved " => "5",
                "in_accounting" => "6",
                "completed" => "7",
                "deactivated" => "8"),
            "fieldwoInfo" => array(
                "WIN_NUM" => "0",
                "WO_ID" => "1",
                "Status" => "2",
                "Project_Name" => "3",
                "Project_ID" => "4",
                "PO" => "5",
                "WorkOrderOwner" => "6",
                "Type_ID" => "7",
                "WO_Category_ID" => "8",
                "_AccountManager" => "9",
                "_ClientServicesDirector" => "10",
                "_ServiceType" => "11",
                "_LegalEntity" => "12",
                "Headline" => "13",
                "Description" => "14",
                "Requirements" => "15",
                "SpecialInstructions" => "16",
                "Ship_Contact_Info" => "17",
                "Qty_Devices" => "18",
                "Lead" => "19",
                "Assist" => "20",
                "Update_Reason" => "21",
                "Update_Requested" => "22",
                "ShortNotice" => "23"),
            "fieldwoDates" => array(
                "_VisitStartTypeID" => "0",
                "_VisitStartDate" => "1",
                "_VisitStartTime" => "2",
                "_VisitEndDate" => "3",
                "_VisitEndTime" => "4",
                "_VisitSiteTimeZone" => "5",
                "_VisitDayPart" => "6",
                "_VisitLeadTime" => "7",
                "_VisitEstimatedDuration" => "8",
                "Qty_Visits" => "9",
                "_VisitActualCheckInDate" => "10",
                "_VisitActualCheckInTime" => "11",
                "_VisitActualCheckInResult" => "12",
                "_VisitActualCheckOutDate" => "13",
                "_VisitActualCheckOutTime" => "14",
                "_VisitActualCheckOutResult" => "15",
                "_VisitActualCalculatedDuration" => "16",
                "_VisitActualDurationResult" => "17"),
            "fieldsite" => array(
                "SiteNumber" => "0",
                "SiteName" => "1",
                "Address" => "2",
                "City" => "3",
                "State" => "4",
                "Zipcode" => "5",
                "Country" => "6",
                "_Geography" => "7",
                "Region" => "8",
                "Route" => "9",
                "Latitude" => "10",
                "Longitude" => "11",
                "Site_Contact_Name" => "12",
                "SiteEmail" => "13",
                "SitePhone" => "14",
                "SiteFax" => "15"),
            "fieldbid" => array(
                "_TechID" => "0",
                "_BiddingTechFLSID" => "1",
                "_Tech_Fname" => "2",
                "_Tech_Lname" => "3",
                "_BiddingTechEmail" => "4",
                "_BiddingTechPhone" => "5",
                "_BiddingTechPreferred" => "6",
                "_BidPayMax" => "7",
                "_BidAmount_Per" => "8",
                "_BidAmount" => "9",
                "_Comments" => "10",
                "_Bid_Date" => "11",
                "_Hide" => "12",
                "Qty_Applicants" => "13"),
            "fieldtechnicians" => array(
                "Tech_ID" => "0",
                "FLS_ID" => "1",
                "Tech_FName" => "2",
                "Tech_LName" => "3",
                "_PrimaryEmail" => "4",
                "_SecondaryEmail" => "5",
                "_PrimaryPhone" => "6",
                "_SecondaryPhone" => "7",
                "_AssignedTechPreferred" => "8",
                "_AssignedTechDenied" => "9",
                "_TechBidAmount" => "10",
                "_TechAmount_Per" => "11",
                "bid_comments" => "12",
                "_TechBid_Date" => "13",
                "_TechBackedOut" => "14",
                "BackOut_Tech" => "15",
                "_BackOutFLSTechID" => "16",
                "_TechNoShowed" => "17",
                "NoShow_Tech" => "18",
                "_NoShowFLSTechID" => "19",
                "BU1FLSID" => "20",
                "Backup_FLS_ID" => "21",
                "BU1Info" => "22"),
            "fieldCommunications" => array(
                "_EmailBlastRadius" => "0",
                "ProjectManagerName" => "1",
                "ProjectManagerEmail" => "2",
                "ProjectManagerPhone" => "3",
                "ResourceCoordinatorName" => "4",
                "ResourceCoordinatorEmail" => "5",
                "ResourceCoordinatorPhone" => "6",
                "_ACSNotifiPOC" => "7",
                "_ACSNotifiPOC_Email" => "8",
                "_ACSNotifiPOC_Phone" => "9",
                "EmergencyName" => "10",
                "EmergencyEmail" => "11",
                "EmergencyPhone" => "12",
                "CheckInOutName" => "13",
                "CheckInOutEmail" => "14",
                "CheckInOutNumber" => "15",
                "TechnicalSupportName" => "16",
                "TechnicalSupportEmail" => "17",
                "TechnicalSupportPhone" => "18"),
            "fieldconfirmation" => array(
                "WorkOrderReviewed" => "0",
                "TechCheckedIn_24hrs" => "1",
                "StoreNotified" => "2",
                "Notifiedby" => "3",
                "NotifyNotes" => "4",
                "PreCall_1" => "5",
                "PreCall_1_Status" => "6",
                "PreCall_2" => "7",
                "PreCall_2_Status" => "8",
                "CheckedIn" => "9",
                "Checkedby" => "10",
                "CheckInNotes" => "11",
                "Incomplete_Paperwork" => "12",
                "Paperwork_Received" => "13",
                "MissingComments" => "14",
                "ClientComments" => "15",
                "SATRecommended" => "16",
                "SATPerformance" => "17",
                "TechMarkedComplete" => "18",
                "CallClosed" => "19",
                "ContactHD" => "20",
                "HDPOC" => "21",
                "Unexpected_Steps" => "22",
                "Unexpected_Desc" => "23",
                "AskedBy" => "24",
                "AskedBy_Name" => "25",
                "FLS_OOS" => "26",
                "Penalty_Reason" => "27",
                "TechMiles" => "28",
                "TechComments" => "29"),
            "wintag" => array(
                "_WINTagAdded" => "0",
                "_TotalNumOfWINTags" => "1",
                "_WINTagAddedDate" => "2",
                "_WINTagAddedBy" => "3",
                /*
                  "_WINTagComplete" => "4",
                  "_WINTagIncomplete" => "5",
                  "_WINTagRescheduled" => "6",
                  "_WINTagSiteCancelled" => "7",
                 */
                "_WINTag" => "4",
                "_WINTagOwner" => "5",
                "_WINTagDetails" => "6",
                "_WINTagNotes" => "7",
                "_WINTagBillable" => "8",
                "_WINTagCheckInResult" => "9",
                "_WINTagCheckOutResult" => "10",
                "_WINTagDurationResult" => "11",
                "_WINTagTechPreferenceRating" => "12",
                "_WINTagTechPerformanceRating" => "13"),
            "partmanager" => array(
                "_NumofNewParts" => "0",
                "_NumofReturnParts" => "1",
                "_PartManagerUpdatedByTech" => "2",
                "_NewPartPartNum" => "3",
                "_NewPartModelNum" => "4",
                "_NewPartSerialNum" => "5",
                "_NewPartEstArrival" => "6",
                "_NewPartCarrier" => "7",
                "_NewPartTrackingNum" => "8",
                "_NewPartInfo" => "9",
                "_NewPartShipsTo" => "10",
                "_NewPartShippingNotes" => "11",
                "_NewPartisConsumable" => "12",
                "_ReturnPartPartNum" => "13",
                "_ReturnPartModelNum" => "14",
                "_ReturnPartSerialNum" => "15",
                "_ReturnPartEstArrial" => "16",
                "_ReturnPartCarrier" => "17",
                "_ReturnPartTrackingNum" => "18",
                "_ReturnPartInfo" => "19",
                "_ReturnPartRMA" => "20",
                "_ReturnPartInstructions" => "21"),
            "fieldPricing" => array(
                "PayMax" => "0",
                "Tech_Bid_Amount" => "1",
                "Amount_Per" => "2",
                "calculatedTechHrs" => "3",
                "baseTechPay" => "4",
                "Additional_Pay_Amount" => "5",
                "OutofScope_Amount" => "6",
                "TripCharge" => "7",
                "MaterialsReimbursement" => "8",
                "MileageReimbursement" => "9",
                "AbortFee" => "10",
                "AbortFeeAmount" => "11",
                "Total_Reimbursable_Expense" => "12",
                "Add_Pay_AuthBy" => "13",
                "Add_Pay_Reason" => "14",
                "Approved" => "15",
                "approvedBy" => "16",
                "PayAmount" => "17",
                "PricingRuleID" => "18",
                "PricingCalculationText" => "19",
                "PricingRuleApplied" => "20",
                "PricingRan" => "21",
                "Invoiced" => "22",
                "calculatedInvoice" => "23",
                "PcntDeduct" => "24",
                "PcntDeductPercent" => "25",
                "Net_Pay_Amount" => "26",
                "TechPaid" => "27",
                "FS_InternalComments" => "28"),
            "fieldadditional" => array(
                "DateEntered" => "0",
                "Username" => "1",
                "_PublishedDate" => "2",
                "_PublishedBy" => "3",
                "SourceByDate" => "4",
                "Date_Assigned" => "5",
                "_AssignedBy" => "6",
                "DateNotified" => "7",
                "Notifiedby" => "8",
                "Date_Completed" => "9",
                "DateIncomplete" => "10",
                "_MarkedIncompleteBy" => "11",
                "DateApproved" => "12",
                "_approvedBy" => "13",
                "DateInvoiced" => "14",
                "DatePaid" => "15",
                "Deactivated" => "16",
                "DeactivatedDate" => "17",
                "DeactivatedBy" => "18",
                "DeactivationCode" => "19",
                "Deactivated_Reason" => "20",
                "lastModDate" => "21",
                "lastModBy" => "22")
        );
        $newContent = array();

        foreach ($FieldOrder as $keyorder => $valorder)
        {
            if ($keyorder == $prefix)
            {
                foreach ($valorder as $key => $val)
                {
                    foreach ($content as $item)
                    {
                        if ($item[$prefix . "_ID"] == $key)
                        {
                            array_push($newContent, $item);
                        }
                    }
                }
            }
        }
        return $newContent;
    }

    public function loadDropdownListAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getRequest()->getParams();
        $ComID = $params["ComID"];
        $Key = $params["Key"];

        $auth = new Zend_Session_Namespace('Auth_User');

        $client = new Core_Api_Class();
        $isPM = $client->isGPMByUserName($auth->login);
        $isGPM = $isPM;

        $CusReportClass = new Core_Api_CusReportClass();
        $FilterDropdownOptions = $CusReportClass->getFilterDropdownOptions($Key, $ComID, $isGPM);
        $str = "";
        if (!empty($FilterDropdownOptions))
        {
            foreach ($FilterDropdownOptions as $DropdownOptions)
            {
                $str .= "<option value='" . $DropdownOptions["Value"] . "' " . (!empty($DropdownOptions["Disable"]) ? $DropdownOptions["Disable"] : "") . ">" . $DropdownOptions["Caption"] . "</option>";
            }
        }
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $response->setHeader('Content-Length', strlen($str), true);
        $response->setBody($str);
    }

    public function reportListAction()
    {
        $objparams = $this->getRequest()->getParams();
        if (!empty($objparams['page']))
        {
            $this->_page = $objparams['page'];
        } else
        {
            $this->_page = 1;
        }

        if (!empty($objparams['size']))
        {
            $this->_size = $objparams['size'];
        } else
        {
            $this->_size = 50;
        }

        $v = $objparams['v'];

        $auth = new Zend_Session_Namespace('Auth_User');
        $client_login_old = $auth->login;

        $client = new Core_Api_Class();
        $isGPM = $client->isGPMByUserName($client_login_old);
        $owner = array("created_by" => $auth->login);
        $Core_Report = new Core_Api_CusReportClass();
        if (empty($objparams['owner']))
        {
            $owner = null;
        }

        $sort = empty($objparams['sort']) ? "" : $objparams['sort'];

        $countRows = sizeof($Core_Report->getList($v, 0, 0, $owner));

        $BodyList = $Core_Report->getList($v, $this->_size, ($this->_page - 1) * $this->_size, $owner, $sort);
        if (!empty($BodyList))
        {
            $paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($BodyList, $countRows));
            $paginator->setItemCountPerPage($this->_size);
            $paginator->setCurrentPageNumber($this->_page);

            $this->view->paginator = $paginator;
        } else
        {
            $this->view->paginator = null;
        }
        $this->view->size = $this->_size;
        $this->view->isGPM = $isGPM;
        $this->view->Company_ID = $v;
    }

    public function doCreateReportV2Action()
    {
        $this->_helper->viewRenderer->setNoRender();

        $CusReportClass = new Core_Api_CusReportClass();
        $params = $this->getRequest()->getParams();
        $SelectFields = $params['fieldArray'];
        $SelectFilters = $params['filterArray'];
//14100
        $recurringreportArray = $params['recurringreportArray'];
        if (isset($recurringreportArray['RRDWeeklyDays']) && is_array($recurringreportArray['RRDWeeklyDays']))
        {
            $recurringreportArray['RRDWeeklyDays'] = implode(',', $recurringreportArray['RRDWeeklyDays']);
        }
        if (isset($recurringreportArray['RRDDailyOptions']) && $recurringreportArray['RRDDailyOptions'] == '2')
        {
            $recurringreportArray['RRDDailyEveryWeekday'] = "1,2,3,4,5,6,7"; //n  
        }

        $reportName = $params["repName"];

        if (!empty($reportName))
        {
            $existReportName = $CusReportClass->exists_ReportName($params['companyID'], $reportName, '');
            if ($existReportName)
            {
                $this->_helper->json(array("success" => false, "error" => 'Please choose a unique Report Name.'));
                return;
            }
        }

        $repID = $CusReportClass->add2($params['companyID'], $params['repName'], $params['repTable'], $SelectFields, $SelectFilters, $params['reportType'], $recurringreportArray);

        $FieldOrder = array(
            "WIN_NUM", "WO_ID", "Project_ID", "PO", "WorkOrderOwner", "Type_ID", "WO_Category_ID", "_AccountManager",
            "_ClientServicesDirector", "_ServiceType", "_LegalEntity", "Headline", "Description", "Requirements", "SpecialInstructions",
            "Ship_Contact_Info", "Qty_Devices", "Lead", "Assist", "Update_Reason", "Update_Requested", "ShortNotice", "_VisitStartTypeID",
            "_VisitStartDate", "_VisitStartTime", "_VisitEndDate", "_VisitEndTime", "_VisitSiteTimeZone", "_VisitDayPart", "_VisitLeadTime",
            "_VisitEstimatedDuration", "Qty_Visits", "_VisitActualCheckInDate", "_VisitActualCheckInTime", "_VisitActualCheckInResult", "_VisitActualCheckOutDate",
            "_VisitActualCheckOutTime", "_VisitActualCheckOutResult", "_VisitActualCalculatedDuration", "_VisitActualDurationResult", "SiteNumber",
            "SiteName", "Address", "City", "State", "Zipcode", "Country", "_Geography", "Region", "Route", "Latitude", "Longitude", "Site_Contact_Name",
            "SiteEmail", "SitePhone", "SiteFax", "_TechID", "_BiddingTechFLSID", "_Tech_Fname", "_Tech_Lname", "_BiddingTechEmail", "_BiddingTechPhone",
            "_BiddingTechPreferred", "_BidPayMax", "_BidAmount_Per", "_BidAmount", "_Comments", "_Bid_Date", "_Hide", "Qty_Applicants", "Tech_ID",
            "FLS_ID", "Tech_FName", "Tech_LName", "_PrimaryEmail", "_SecondaryEmail", "_PrimaryPhone", "_SecondaryPhone", "_AssignedTechPreferred",
            "_AssignedTechDenied", "_TechBidAmount", "_TechAmount_Per", "bid_comments", "_TechBid_Date", "_TechBackedOut", "BackOut_Tech",
            "_BackOutFLSTechID", "_TechNoShowed", "NoShow_Tech", "_NoShowFLSTechID", "BU1FLSID", "Backup_FLS_ID", "BU1Info", "_EmailBlastRadius",
            "ProjectManagerName", "ProjectManagerEmail", "ProjectManagerPhone", "ResourceCoordinatorName", "ResourceCoordinatorEmail",
            "ResourceCoordinatorPhone", "_ACSNotifiPOC", "_ACSNotifiPOC_Email", "_ACSNotifiPOC_Phone", "EmergencyName", "EmergencyEmail",
            "EmergencyPhone", "CheckInOutName", "CheckInOutEmail", "CheckInOutNumber", "TechnicalSupportName", "TechnicalSupportEmail",
            "TechnicalSupportPhone", "WorkOrderReviewed", "TechCheckedIn_24hrs", "StoreNotified", "NotifyNotes", "PreCall_1",
            "PreCall_1_Status", "PreCall_2", "PreCall_2_Status", "CheckedIn", "Checkedby", "CheckInNotes", "Incomplete_Paperwork", "Paperwork_Received",
            "MissingComments", "ClientComments", "SATRecommended", "SATPerformance", "TechMarkedComplete", "CallClosed", "ContactHD", "HDPOC",
            "Unexpected_Steps", "Unexpected_Desc", "AskedBy", "AskedBy_Name", "FLS_OOS", "Penalty_Reason", "TechMiles", "TechComments", "_WINTagAdded",
            "_TotalNumOfWINTags", "_WINTagAddedDate", "_WINTagAddedBy", "_WINTag", "_WINTagOwner", "_WINTagDetails", "_WINTagNotes", "_WINTagBillable", "_WINTagCheckInResult",
            "_WINTagCheckOutResult", "_WINTagDurationResult", "_WINTagTechPreferenceRating", "_WINTagTechPerformanceRating", "_NumofNewParts",
            "_NumofReturnParts", "_PartManagerUpdatedByTech", "_NewPartPartNum", "_NewPartModelNum", "_NewPartSerialNum", "_NewPartEstArrival",
            "_NewPartCarrier", "_NewPartTrackingNum", "_NewPartInfo", "_NewPartShipsTo", "_NewPartShippingNotes", "_NewPartisConsumable",
            "_ReturnPartPartNum", "_ReturnPartModelNum", "_ReturnPartSerialNum", "_ReturnPartEstArrial", "_ReturnPartCarrier", "_ReturnPartTrackingNum",
            "_ReturnPartInfo", "_ReturnPartRMA", "_ReturnPartInstructions", "PayMax", "Tech_Bid_Amount", "Amount_Per", "calculatedTechHrs",
            "baseTechPay", "Additional_Pay_Amount", "OutofScope_Amount", "TripCharge", "MaterialsReimbursement", "MileageReimbursement", "AbortFee",
            "AbortFeeAmount", "Total_Reimbursable_Expense", "Add_Pay_AuthBy", "Add_Pay_Reason", "Approved", "approvedBy", "PayAmount", "PricingRuleID",
            "PricingCalculationText", "PricingRuleApplied", "PricingRan", "Invoiced", "calculatedInvoice", "PcntDeduct", "PcntDeductPercent",
            "Net_Pay_Amount", "TechPaid", "FS_InternalComments", "DateEntered", "Username", "_PublishedDate", "_PublishedBy", "SourceByDate",
            "Date_Assigned", "_AssignedBy", "DateNotified", "Notifiedby", "Date_Completed", "DateIncomplete", "_MarkedIncompleteBy", "DateApproved",
            "_approvedBy", "DateInvoiced", "DatePaid", "Deactivated", "DeactivatedDate", "DeactivatedBy", "DeactivationCode", "Deactivated_Reason",
            "lastModDate", "lastModBy");
        $rearrange = array();
        foreach ($FieldOrder as $tem)
        {
            foreach ($SelectFields as $val)
            {
                if ($tem == $val)
                {
                    array_push($rearrange, $val);
                }
            }
        }
        $CusReportClass->reArrangeColumns($repID, $rearrange);
        $this->_helper->json(array("success" => true, "repID" => $repID));
    }

    public function doUpdateReportV2Action()
    {
        $this->_helper->viewRenderer->setNoRender();

        $CusReportClass = new Core_Api_CusReportClass();
        $params = $this->getRequest()->getParams();

        $SelectFields = $params['fieldArray'];
        $SelectFilters = $params['filterArray'];
        $CreateBidsReport = $params['createBidReport'];

        $reportID = $params["repID"];
        $reportName = $params["repName"];

        $reportInfo = $CusReportClass->getReportInfo2($reportID);
        $currentReportName = $reportInfo['rpt_name'];

        //--- validate  
        if (!empty($reportName))
        {
            $existReportName = $CusReportClass->exists_ReportName($params['companyID'], $reportName, $currentReportName);
            if ($existReportName)
            {
                $this->_helper->json(array("success" => false, "error" => 'Please choose a unique Report Name.'));
                return;
            }
        }

        $CusReportClass->saveReport2($reportID, $reportName, $SelectFields, $SelectFilters, $CreateBidsReport, $params['reportType']);
        $this->_helper->json(array("success" => true, "repID" => $reportID));
    }

    public function doSaveRecurringReportV2Action()
    {

        $this->_helper->viewRenderer->setNoRender();

        $CusReportClass = new Core_Api_CusReportClass();
        $params = $this->getRequest()->getParams();

        $RRDParams = $params['RRDParams'];
        $reportID = $params["repID"];

        if (isset($RRDParams['RRDWeeklyDays']) && is_array($RRDParams['RRDWeeklyDays']))
        {
            $RRDParams['RRDWeeklyDays'] = implode(',', $RRDParams['RRDWeeklyDays']);
        }
        if ($RRDParams['RRDDailyOptions'] == '2')
        {
            $RRDParams['RRDDailyEveryWeekday'] = "1,2,3,4,5,6,7"; //n  
        }

        $repID = $CusReportClass->saveReport_RRD($reportID, $RRDParams);
        $this->_helper->json(array("success" => true, "repID" => $repID));
    }

    function reportViewAction()
    {
        $objparams = $this->getRequest()->getParams();


        $rptid = $objparams['rptid'];

        if (!empty($objparams['page']))
        {
            $this->_page = $objparams['page'];
        } else
        {
            $this->_page = 1;
        }

        if (!empty($objparams['size']))
        {
            $this->_size = $objparams['size'];
        } else
        {
            $this->_size = 50;
        }

        $v = $objparams['v'];
        $sorts = array();
        for ($i = 1; $i <= 3; $i++)
        {
            $sorts['sort_' . $i] = $objparams['sort_' . $i];
            if (!empty($objparams['sort_dir_' . $i]))
            {
                $sorts['sort_dir_' . $i] = $objparams['sort_dir_' . $i];
            } else
            {
                $sorts['sort_dir_' . $i] = 'asc';
            }
        }

        $Core_Report = new Core_Api_CusReportClass();
        $arrcol = $objparams['arrcol'];

        if (!empty($arrcol))
        {
            $defaultorder = array("WIN_NUM", "WO_ID", "Status", "Project_Name");
            $temorderarr = array();

            foreach ($defaultorder as $defaultval)
            {
                foreach ($arrcol as $id => $val)
                {
                    if ($defaultval == $val)
                    {
                        array_push($temorderarr, $val);
                        unset($arrcol[$id]);
                    }
                }
            }


            foreach ($arrcol as $id => $val)
            {
                array_push($temorderarr, $val);
            }
            $Core_Report->reArrangeColumns($rptid, $temorderarr);
        }

        if (!empty($sorts))
        {
            $Core_Report->saveSortInfo2($rptid, $sorts);
        } else
        {
            $sorts = $Core_Report->getSortInfo2($rptid);
        }
        //--- Filterlist
        $filterArray = array();
        foreach ($objparams['filterArray'] as $item)
        {
            $filterArray[$item[fieldcode]] = $item;
        }
        $FilterList = $Core_Report->getFilterList2($rptid);
        if (!empty($FilterList["Project_Name"]))
        {
            $filterArray["Project_Name"] = $FilterList["Project_Name"];
        }
        if (!empty($FilterList["Status"]))
        {
            $filterArray["Status"] = $FilterList["Status"];
        }
        //--- fieldlist
        $grocol = explode(",", $objparams['grocol']);
        $coucol = explode(",", $objparams['coucol']);
        $avgcol = explode(",", $objparams['avgcol']);
        $sumcol = explode(",", $objparams['sumcol']);
        if (empty($objparams['grocol']))
        {
            $grocol = null;
        }
        if (empty($objparams['coucol']))
        {
            $coucol = null;
        }
        if (empty($objparams['avgcol']))
        {
            $avgcol = null;
        }
        if (empty($objparams['sumcol']))
        {
            $sumcol = null;
        }
        //--- query
        $FieldList = $Core_Report->getReportHeader2($rptid, $coucol, $sumcol, $avgcol);
        $countRows = $Core_Report->runReport2($rptid, 0, 0, true, null, $filterArray);

        $BodyList = $Core_Report->runReport2($rptid, $this->_size, ($this->_page - 1) * $this->_size, false, $sorts, $filterArray);

        $paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($BodyList, $countRows));
        $paginator->setItemCountPerPage($this->_size);
        $paginator->setCurrentPageNumber($this->_page);

        $this->view->paginator = $paginator;
        $this->view->FieldList = $FieldList;
        $this->view->size = $this->_size;
        $this->view->rptid = $rptid;
        $this->view->Company_ID = $v;
        $this->view->countRows = $countRows;
    }

    public function loadComListAction()
    {
        $search = new Core_Api_Class();
        $company = $this->getRequest()->getParam('company');
        try
        {
            $data = $search->getGlobalPmCompanies($this->_login . '|pmContext=' . $company, $this->_password);
        } catch (Exception $e)
        {
            
        }

        $jsonContext = array("companies" => $data['companies']);
        $this->_helper->json($jsonContext);
    }

    public function loadDefaultProjectAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getRequest()->getParams();
        $ComID = $params["ComID"];
        $CusReportClass = new Core_Api_CusReportClass();

        $auth = new Zend_Session_Namespace('Auth_User');
        $client = new Core_Api_Class();
        $isPM = $client->isGPMByUserName($auth->login);
        $isGPM = $isPM;

        $resultList = $CusReportClass->getFilterList_ByLocation("Select Projects", $ComID, $isGPM);
        $projectname = array();
        if (!empty($resultList))
        {
            foreach ($resultList as $item)
            {
                array_push($projectname, $item["Project_Name"]);
            }
        }
        $jsonContext = array("projectname" => $projectname);
        $this->_helper->json($jsonContext);
    }

    public function runAutomailAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getRequest()->getParams();
        $date = $params["date"];
        $time = $params["time"];
        $CusReportClass = new Core_Api_CusReportClass2();
        $rerult = $CusReportClass->getReportListForAutoEmail($date, $time);
        $str = "<table width='100%' cellspacing='5' cellpadding='0' border='0' class='gradBox_table_inner'>
                    <tr class='table_head'>
                    <th>report Name</th>
                    <th>Last Run Date</th>
                    <th>Last Send Date</th>
                    <th>To Email</th>
                    <th>To Name</th>
                    <th>From Mail</th>
                    <th>Subject</th>
                    <th>Message</th>
                    <th>Time Of Day</th>
                    <th>Time Zone</th>
                    <th>Schedule</th>
                    <th>Daily Space Out</th>
                    <th>Dayly Every Week</th>
                    <th>Weekly Days</th>
                    <th>Weekly Space Out</th>
                    <th>Monthly Shcedule</th>
                    <th>Monthly Shcedule Day Num</th>
                    <th>Monthly Schedule Day Order</th>
                    <th>Monthly Schedule Day</th>
                    <th>Monthly Day Space Out</th>
                    <th>Monthly Dayorder Space Out</th>
                    </tr>";
        $i = 1;
        $TimezoneList = array("-10" => "Hawaii", "-9" => "Alaska", "-8" => "Pacific", "-7" => "Mountain", "-6" => "Central", "-5" => "Eastern");
        foreach ($rerult as $item)
        {
            $str.="<tr class='" . (($i % 2) ? "border_td odd_tr" : "border_td even_tr") . "'>";
            $str.="<td>" . $item['rpt_name'] . "</td>";
            $str.="<td>" . $item['last_run_date'] . "</td>";
            $str.="<td>" . $item['last_autosend_date'] . "</td>";
            $str.="<td>" . $item['RRDEmailTo'] . "</td>";
            $str.="<td>" . $item['RRDEmailFromName'] . "</td>";
            $str.="<td>" . $item['RRDEmailFrom'] . "</td>";
            $str.="<td>" . $item['RRDEmailSubject'] . "</td>";
            $str.="<td>" . $item['RRDEmailMessage'] . "</td>";
            $str.="<td>" . $item['RRDTimeOfDay'] . "</td>";

            $str.="<td>" . (!empty($TimezoneList[$item['RRDTimeZone']]) ? $TimezoneList[$item['RRDTimeZone']] : $item['RRDTimeZone']) . "</td>";
            $str.="<td>" . $item['RRDEmailSchedule'] . "</td>";
            $str.="<td>" . $item['RRDDailyEveryDaySpaceOutNum'] . "</td>";
            $str.="<td>" . $item['RRDDailyEveryWeekday'] . "</td>";
            $str.="<td>" . $item['RRDWeeklyDays'] . "</td>";
            $str.="<td>" . $item['RRDWeeklySpaceOutNum'] . "</td>";
            $str.="<td>" . $item['RRDMonthlyScheduleOptions'] . "</td>";
            $str.="<td>" . $item['RRDMonthlyScheduleDayNum'] . "</td>";
            $str.="<td>" . $item['RRDMonthlyScheduleDayOrderDay'] . "</td>";
            $str.="<td>" . $item['RRDMonthlyScheduleDayOrder'] . "</td>";
            $str.="<td>" . $item['RRDMonthlyDayNumSpaceOutNum'] . "</td>";
            $str.="<td>" . $item['RRDMonthlyDayOrderSpaceOutNum'] . "</td>";
            $str.="</tr>";
            $i++;
        }
        $str.="</table>";
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $response->setHeader('Content-Length', strlen($str), true);
        $response->setBody($str);
    }

}

?>
