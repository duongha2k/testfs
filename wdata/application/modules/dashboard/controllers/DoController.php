<?php

class Dashboard_DoController extends Core_Controller_Action
{
    public function init()
    {
        try {
            parent::init();
        } catch (Core_Auth_Exception $e) {
			if ($this->getRequest()->getActionName() != 'tech-phone-not-taken') {
				if ($this->getRequest()->getActionName() != 'file-wos-documents-download') throw $e;
            	$request = $this->getRequest();
				$winNum = $request->getParam('win', null);
				if (empty($winNum)) {
            	$companyId = $request->getParam('companyId', null);
            	$token = $request->getParam('token', null);
            	$sfUser = new Salesforce_Api_User($companyId, $token);
            	$isValid = $sfUser->isAuthentic();
            	$this->_login = $sfUser->getAPIUsername();
            	$this->_password = $sfUser->getAPIPassword();
				}
				else {
	            	$fId = $request->getParam('id', null);
	            	$token = $request->getParam('token', null);
					$isValid = md5($winNum . 'offSite#Access' . $fId) == $token;
					if (!$isValid) // allows token to expire for clients that download the file and puts in their system
						$isValid = md5($winNum . 'offSite#Access' . date('Ymd') . $fId) == $token;
					if (!$isValid)
						$isValid = md5($winNum . 'offSite#Access' . date('Ymd', strtotime('-1 day')) . $fId) == $token;
					$this->_login = 'dummy';
					$this->_password = 'dummy';
				}
				if (!$isValid) throw $e;
            	$allow = true;
        	}
		}

        //  This is constroller can be used for AJAX only!
        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            if ($this->getRequest()->getParam('download', false) !== 'csv') {
                // ok
            } else if ($this->getRequest()->getParam('download', false) !== 'file') {
                // ok
            } else if ($allow) {
                // ok
            } else {
                $this->_redirect('/');
            }
        }
    }

    public function makeABidAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();

        $return = array('success' => 0);

        $winNum = $request->getParam('win', 0) + 0;
        $bidAmount = $request->getParam('amount', 0) + 0;

        $agree = (int) $request->getParam('agree', 0);
        $comments = $request->getParam('comments', NULL);
        $understand = (int) $request->getParam('understand', 0);

        //if(empty($comments))   $return['errors'][] = 'Comments should not be empty';
        if(empty($agree))      $return['errors'][] = 'You should agreed the conditional statements';
        if(empty($understand)) $return['errors'][] = 'You should understand the conditional statements';

        if (empty($return['errors'])) {
            $techApi = new Core_Api_TechClass();
            $apiResponse = $techApi->createBid($this->_login, $this->_password, $winNum, $bidAmount, $comments);

            if ($apiResponse->success) {
                $return['success'] = 1;
            } else {
                foreach ($apiResponse->errors as $e) $return['errors'][]=$e->message;
            }
        }
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                ->setBody($return);
    }

    public function fileDownloadAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $showZero = false;

        $request = $this->getRequest();

        $filename = $request->getParam('filename', false);
        $encType = $request->getParam('type', 'base');
        $showAction = $request->getParam('sact', 'down');

        $mimeType = 'application/octet-stream';

        if ($filename !== false && $encType == 'base') {
            $filename = base64_decode($filename);
        } else {
            //$this->fileDownloadS3('wo_docs');
            $filename = urldecode(urldecode($filename));
        }

        if ($showAction == 'image') {
            $fileImageType = Core_Image::getImageTypeByFileName($filename);

            if (!empty($fileImageType)) {
                $mimeType = Core_Image::getMimeByImgType($fileImageType);
            } else {
                $showZero = true;
            }
        }

        $_f = preg_replace('/[^-.a-z0-9_\/]+/i', '_', $filename);
        $response = $this->getResponse();
        $response->clearHeaders();
        $response->clearBody();
        $response
                ->setHeader('Content-Type', $mimeType, true)
                ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
                ->setHeader('Content-Disposition', 'attachment; filename=' . substr($_f, strrpos($_f, '/')), true)
                ->setHeader('Last-Modified', date('D, d M Y H:i:s T'), true);

        $getter = new Core_Api_Class;
        $side = $request->getParam('side', null);

        $file = $getter->getFile($this->_login, $this->_password, $filename, $side);

        if ($file === NULL || $file instanceof API_Response || $showZero) {
            $response->setHeader('Content-Length', 0, true)
                    ->setBody('');
        } else {
            //$file = base64_decode($file);
            $response->setHeader('Content-Length', strlen($file), true)
                    ->setBody($file);
        }
    }

    
    /**
     * Download WOS documents
     */
    public function fileWosDocumentsDownloadAction()
    {
        $this->fileDownloadS3('wo_docs');
    }
    /**
     * Download Clients documents
     */
    public function fileClientsDocumentsDownloadAction()
    {
        $this->fileDownloadS3('clients_docs');
    }

    
    /**
     * Download project documents
     */
    public function fileProjectDocumentsDownloadAction()
    {
        $this->fileDownloadS3('project');
    }

    /**
     * Download tech documents
     */
	public function fileTechDocumentsDownloadAction()
	{
        $this->fileDownloadS3('tech_docs');
    }

    /**
     * Dwonload file from s3
     * @param string $type
     * @throws Zend_Exception
     */
    private function fileDownloadS3($type)
    {
        ini_set('memory_limit', '512M');
        if (empty($this->_login) || empty($this->_password)) {
            // no authentification
            throw new Core_Auth_Exception();
        }

        $this->_helper->viewRenderer->setNoRender();

        $showZero = false;

        $request = $this->getRequest();

        $filename = $request->getParam('filename', false);
        $encType = $request->getParam('type', 'base');
        $showAction = $request->getParam('sact', 'down');

		$win = $request->getParam('win', null);
		if (!empty($win)) {
			$fId = $request->getParam('id', null);
			if (!empty($fId)) {
				$db = Core_Database::getInstance();
				$select = $db->select();
				$select->from(Core_Database::TABLE_FILES, array('path'))
					->where('id = ? AND WIN_NUM ='.$win, $fId)->limit(1);
				$path = $db->fetchOne($select);

				$filename = empty($path) ? false : $path;
				$encType = '';
			}
		}

        $mimeType = 'application/octet-stream';
        if ($filename !== false && $encType == 'base') {
            $filename = urldecode(base64_decode($filename));
        }
        if ($showAction == 'image') {
            $fileImageType = Core_Image::getImageTypeByFileName($filename);
            if (!empty($fileImageType)) {
                $mimeType = Core_Image::getMimeByImgType($fileImageType);
            } else {
                $showZero = true;
            }
        }
        $response = $this->getResponse();
        $response->clearHeaders();
        $response->clearBody();

        $filename = ltrim($filename, '/');
        $file = null;

        switch ($type) {
            case 'wo_docs':
                $file = API_WorkOrder::getFile($filename);
                break;
            case 'clients_docs':
                $file = Core_Api_AdminClass::getClientFile($filename);
                break;
            case 'project':
                $getter = new Core_Api_ProjectClass();
                $file = $getter->getFile($filename);
                break;
            case 'tech_docs':
                $file = Core_Tech::getTechFileAWS($filename);
                //q$file = $getter->getFile($filename);
                break;
            default:
                throw new Zend_Exception('Wrong type');
                break;
        }

        if ($file === NULL || $file instanceof API_Response || $showZero) {
            // not found
            if (0 && $encType == 'base') { /* disables caspio logic */
                // check caspio for file
                $this->fileDownloadAction();
            } else {
                // not found
                $this->getResponse()->setHeader('HTTP/1.1', '404 Not Found');
                $this->getResponse()->setHeader('Status', '404 File not found');
                $this->getResponse()->setBody('<h1>404 File not found</h1>');
            }
        } else {
            if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)) {
                $filename = rawurlencode($filename);
            }
            $response
                    //->setHeader('Content-Type', $mimeType, true)
                    ->setHeader("Content-Type", "content=" . $mimeType . "; charset=utf-8", true)
                    ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
                    ->setHeader("Content-Disposition", "attachment; filename=\"$filename\"", true)
                    ->setHeader('Last-Modified', date('D, d M Y H:i:s T'), true)
                    ->setHeader('Content-Length', strlen($file), true);

            $response->setBody($file);
        }
    }


    public function updateDeactivationReasonAction() {
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $response->setHeader('Content-type', 'application/json');

        $win = $request->getParam('win', NULL);
        $code = $request->getParam('code', '');
        $company = $request->getParam('company', '');
        $reason = trim($request->getParam('reason', ''));

        if ($win) {
            $publisher = new Core_Api_Class;
            $result = $publisher->deactivateWorkOrder($this->_login . '|pmContext=' . $company, $this->_password, $win, $code, $reason);
            if ($result->success) {
                $body = '{"wo":"' . $win . '","result":"1","message":""}';
            } else {
                $body = '{"wo":"' . $win . '","result":"0","message":"' . addslashes($result->errors[0]->message) . '"}';
            }
            $response->setBody($body);
            $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');
            $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('work_orders'));    //  clean cache for WO by tag 'work_orders'
        } else {
            $response->setBody('{"wo":"","result":"0","message":"WIN is empty"}');
        }
    }

    public function receivePaperworkAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $search = new Core_Api_Class();
        $ids = $this->getRequest()->getParam('ids', array());
        $company = $this->getReqest()->getParam('company', null);
        foreach ($ids as $id) {
            $wo = $search->getWorkOrder($this->_login . '|pmContext=' . $company, $this->_password, $id);
            if ($wo->success && !empty($wo->data[0])) {
                $wo = $wo->data[0];
                $wo->Paperwork_Received = true;
                //create timestamp
                $search->updateWorkOrder($this->_login, $this->_password, $id, $wo);
                Core_TimeStamp::createTimeStamp($id, $this->_login, 'Work Order Updated: Paperwork_Received', $company, null, $wo->Project_Name, null, $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
            }
        }
    }

    public function publishAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        /* init progress bar */
        $progress = Core_ProgressBar::getInstance()->setUserLogin($this->_login);
        $data = array('percent' => 0, 'label' => 'Beginning...', 'done' => false);
        $progress->write('publish', json_encode($data));
        /* -- init progress bar -- */

        $request = $this->getRequest();
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $response->setHeader('Content-type', 'application/json');

        $wo_ids = $request->getParam('wo_ids', NULL);
        $publishOptions = $request->getParam('publishOptions', array());

        if (is_array($wo_ids) && ($countWOs = sizeof($wo_ids))) {
            $publisher = new Core_Api_Class;

            $company = $request->getParam('company', NULL);
            $context = empty($company) ? "" : '|pmContext=' . $company;

            $body = '{"wos":[';
            for ($i = 0; $i < $countWOs; ++$i) {
                $win = $wo_ids[$i];

                /* progress bar data */
                $data['percent'] = ceil(100 * $i / $countWOs);
                $data['label'] = "Publishing<br />WIN#{$win}...";
                $data['done'] = false;
                $progress->write('publish', json_encode($data));
                /* -- progress bar data -- */
                $additionalFields = array();
                if (isset($publishOptions[$win])) {
                    $techId = $this->getTechId($this->_login . $context, $this->_password, $win);
                    if ($techId === false) {
                        $body .= '{"wo":"' . $win . '","result":"0","message":"Getting tech ID failed"}';
                        continue;
                    }

                    
                    if ($publishOptions[$win] == 'NoShow') {
                        $additionalFields['NoShow_Tech'] = $techId;
                    }
                    if ($publishOptions[$win] == 'BackOut') {
                        $additionalFields['BackOut_Tech'] = $techId;
                    }
                    $additionalFields['sentworknotificationmail']=1;
                    //$result = $publisher->publishWorkOrder($this->_login . $context, $this->_password, $win, $additionalFields);
                } else {
                    $additionalFields['sentworknotificationmail']=1;
                    //$result = $publisher->publishWorkOrder($this->_login . $context, $this->_password, $win);
                }
                $result = $publisher->publishWorkOrder($this->_login . $context, $this->_password, $win, $additionalFields);
                if ($result->success) {
                    $body .= '{"wo":"' . $win . '","result":"1","message":""}';
                } else {
                    $error = $result->errors[0];
                    $body .= '{"wo":"' . $win . '","result":"0","message":"';
                    $body .= ( $error instanceof API_Error ) ? addslashes(str_replace(array("\n", "\r"), "", $error->message)) : '';
                    $body .= '"}';
                }
                $body .= ($i + 1 < $countWOs) ? ',' : '';

            }
            $body .= ']}';

            $response->setBody($body);
            $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');
            $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('work_orders'));    //  clean cache for WO by tag 'work_orders'
        } else {
            $response->setBody('{"wos":[],"result":"0","message":"WOs or Company is empty"}');
        }

        /* progress bar data */
        $data['percent'] = 0;
        $data['label'] = 'Done';
        $data['done'] = true;
        $progress->write('publish', json_encode($data));
        /* -- progress bar data -- */
    }

	private function getTechId($user, $pass, $winNum)
	{
        $api = new Core_Api_Class;
        $woInfo = $api->getWorkOrder($user, $pass, $winNum);

        if ($woInfo->success) {
			if (!empty($woInfo->data[0]->Tech_ID)) return $woInfo->data[0]->Tech_ID;
            return '';
        }

        return false;
    }

    public function approveAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        /* init progress bar */
        $progress = Core_ProgressBar::getInstance()->setUserLogin($this->_login);
        $data = array('percent' => 0, 'label' => 'Beginning...', 'done' => false);
        $progress->write('approve', json_encode($data));
        /* -- init progress bar -- */


        $request = $this->getRequest();
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $response->setHeader('Content-type', 'application/json');

        $wo_ids = $request->getParam('wo_ids', NULL);

        if (is_array($wo_ids) && ($countWOs = sizeof($wo_ids))) {
            $hasError = false;
            $payAmount = $request->getParam('payAmounts');
            foreach ($payAmount as $key => $value) {
                if (!is_numeric($value)) {
                    $response->setBody('{"wos":[],"result":"0","message":"At least one amount field is incorrect"}');
                    $hasError = true;
                }
            }
            if (!$hasError) {
                $approver = new Core_Api_Class;

                $company = $request->getParam('company');
                $context = empty($company) ? "" : '|pmContext=' . $company;

                $body = '{"wos":[';
                for ($i = 0; $i < $countWOs; ++$i) {
                    $win = $wo_ids[$i];

                    // progress bar data
                    $data['percent'] = ceil(100 * $i / $countWOs);
                    $data['label'] = "Approving<br />WIN#{$win}...";
                    $data['done'] = false;
                    $progress->write('approve', json_encode($data));
                    //-- progress bar data --
                    $result = $approver->approveWorkOrder($this->_login . $context, $this->_password, $win, isset($payAmount[$win]) ? $payAmount[$win] : null);
                    if ($result->success) {
                        $body .= '{"wo":"' . $win . '","result":"1","message":""}';
                    } else {
                        $error = $result->errors[0];
                        $body .= '{"wo":"' . $win . '","result":"0","message":"';
                        $body .= ( $error instanceof API_Error ) ? addslashes(str_replace(array("\n", "\r"), "", $error->message)) : '';
                        $body .= '"}';
                    }
                    $body .= ($i + 1 < $countWOs) ? ',' : '';
                }
                $body .= ']}';

                $response->setBody($body);
                $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');
                $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('work_orders'));    //  clean cache for WO by tag 'work_orders'
            }
        } else {
            $response->setBody('{"wos":[],"result":"0","message":"WOs or Company is empty"}');
        }

        /* progress bar data */
        $data['percent'] = 0;
        $data['label'] = 'Done';
        $data['done'] = true;
        $progress->write('approve', json_encode($data));

        /* -- progress bar data -- */
    }

    public function assignAction()
    {

        $this->_helper->viewRenderer->setNoRender();

        // init progress bar
        $progress = Core_ProgressBar::getInstance()->setUserLogin($this->_login);
        $data = array('percent' => 0, 'label' => 'Beginning...', 'done' => false);
        $progress->write('assign', json_encode($data));
        //-- init progress bar --

        $params = $this->getRequest()->getParams();

        $techs = $params['techs'];
        $agreeds = $params['agreeds'];
        $routes = $params['routes'];
        $amountsper = $this->getRequest()->getParam('amountsrep', null);
        $qtyDevices = $this->getRequest()->getParam('qtydevices', null);
        $assignClientDeniedTech = $params['assignClientDeniedTech'];

        $hasErrors = false;
        $arrErrs = array();
        foreach ($techs as $winNum => $tech) {
            if (!is_numeric($winNum) || !is_numeric($agreeds[$winNum])) {
                $jsonContext = json_encode(array('error' => 1
                        , 'msg' => 'One of the fields is not numeric'
                        ,'ext'=>$arrErrs));
                $hasErrors = true;
            }
        }

        $search = new Core_Api_Class;

        $company = $params['company'];
        $context = empty($company) ? "" : '|pmContext=' . $company;

        
        if (!$hasErrors) {
            $countWOs = sizeof($techs);
            $i = 0;
            $errMsg0='';
            $j = 0;
            foreach ($techs as $winNum => $tech) {
                $i++;
                // progress bar data
                $data['percent'] = ceil(100 * $i / $countWOs);
                $data['label'] = "Assigning<br />WIN #" . $winNum;
                $data['done'] = false;
                $progress->write('assign', json_encode($data));
                //--progress bar data--
                $assignResult = $search->assignWorkOrder(
                    $this->_login . $context,
                    $this->_password,
                    (int)$winNum,
                    $tech,
                    $agreeds[$winNum],
                    null,
                    $amountsper[$winNum],
                    $qtyDevices[$winNum],
                    $assigassnClientDeniedTech,
                    $routes[$winNum]);
                if ($assignResult->success) {
                    $arrErrs[]=array('errMsg'=>'','win'=>(int)$winNum,'tech'=>$tech);
                    //$jsonContext = json_encode(array('error' => 0));
                } else {
                    $hasErrors = true;
                    $errMsg0 = $assignResult->errors[0]->message;
                    $arrErrs[]=array('errMsg'=>$assignResult->errors[$j]->message,'win'=>(int)$winNum,'tech'=>$tech);
                    $j++;
                    /*
                    $jsonContext = json_encode(array(
                        'error' => 1
                        ,'msg' => $assignResult->errors[0]->message
                        ,"ext"=>$returnResult));
                     */
                }
                $i++;
            }
            if(!$hasErrors)
            {
                $jsonContext = json_encode(array('error' => 0));
        }
            else
            {
                $jsonContext = json_encode(array(
                        'error' => 1
                        ,'msg' => $errMsg0
                        ,'ext'=>$arrErrs)
                );                
            }
        }
        // progress bar data
        $data['percent'] = 0;
        $data['label'] = 'Done';
        $data['done'] = true;
        $progress->write('assign', json_encode($data));
        //-- progress bar data --

        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($jsonContext), true)
                ->setBody($jsonContext);
    }

    public function deactivateAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        // init progress bar
        $progress = Core_ProgressBar::getInstance()->setUserLogin($this->_login);
        $data = array('percent' => 0, 'label' => 'Beginning...', 'done' => false);
        $progress->write('deactivate', json_encode($data));
        //-- init progress bar --

        $params = $this->getRequest()->getParams();
        $wos = $params['wos'];
        $reason = $params['reason'];
        $code = $params['code'];

        $hasErrors = false;

        $search = new Core_Api_Class;

        $context = empty($company) ? "" : '|pmContext=' . $company;

        $countWOs = sizeof($wos);
        $i = 0;
        foreach ($wos as $winNum) {
            $i++;
            // progress bar data
            $data['percent'] = ceil(100 * $i / $countWOs);
            $data['label'] = "Deactivating<br />WIN #" . $winNum;
            $data['done'] = false;
            $progress->write('deactivate', json_encode($data));
            //--progress bar data--

            $deactivateResult = $search->deactivateWorkOrder(
                $this->_login . $context,
                $this->_password,
                (int)$winNum,
                $code,
                $reason);

            if ($deactivateResult->success) {
                $jsonContext = json_encode(array('error' => 0));
            } else {
                $jsonContext = json_encode(array('error' => 1, 'msg' => $deactivateResult->errors[0]->message));
            }
            $i++;
        }

        // progress bar data
        $data['percent'] = 0;
        $data['label'] = 'Done';
        $data['done'] = true;
        $progress->write('deactivate', json_encode($data));
        //-- progress bar data --

        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($jsonContext), true)
                ->setBody($jsonContext);
    }

    public function progressBarAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $failJson = '{"percent":0,"label":"","done":true}';

        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $response->setHeader('Content-type', 'application/json');

        $key = $this->getRequest()->getParam('key', null);
        $progress = Core_ProgressBar::getInstance()->setUserLogin($this->_login);

        $out = trim($progress->read($key));
        if ( empty($out) )  return $response->setBody($failJson);
        else                return $response->setBody($out);
    }

    public function globalPmDropDownAction() {
        $search = new Core_Api_Class();
        $company = $this->getRequest()->getParam('company');
        $defaultWin = $this->getRequest()->getParam('win');
        $isPM = false;

        try {
            $data = $search->getGlobalPmCompanies($this->_login . '|pmContext=' . $company, $this->_password);
            $isPM = $data['isPM'];
        } catch (Exception $e) {
            error_log("global pm error");
            error_log(print_r($e, true));
        }

        if ($isPM) {
            try {
                $projects = $search->getGlobalPmProjects($company, $defaultWin);
            } catch (Exception $e) {
                error_log("global pm dropdown error");
                error_log(print_r($e, true));
            }

            $jsonContext = array("isPM" => $isPM, "companies" => $data['companies'], "defaultProject" => $projects['defaultProject'], "projects" => $projects['projects']);
        } else {
            $jsonContext = array("isPM" => $isPM);
        }


        $this->_helper->json($jsonContext);
    	
    }

    public function globalPmProjectAction() {
        $search = new Core_Api_Class();
        $companyId = $this->getRequest()->getParam('company');

        try {
            $data = $search->getGlobalPmProjects($companyId);
        } catch (Exception $e) {
            error_log("global pm error");
            error_log(print_r($e, true));
        }
        $jsonContext = array("isPM" => "true", "projects" => $data['projects']);
        $this->_helper->json($jsonContext);
    }

    public function copyWorkOrderAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $win = $this->getRequest()->getParam('win');
        $company = $this->getRequest()->getParam('company');
        //13955
        //$keepStartDate = "true";//13909
        $keepStartDate = $this->getRequest()->getParam('keepStartDate');
        //End 13955
        $keepAssignedTech = $this->getRequest()->getParam('keepAssignedTech');
        $copyToCompanyId = $this->getRequest()->getParam('copyToCompany');
        $copyToProjectId = $this->getRequest()->getParam('copyToProject');

        $search = new Core_Api_Class();
        $wo = $search->getWorkOrder($this->_login . '|pmContext=' . $company, $this->_password, $win);
        if ($wo->success && !empty($wo->data)) {
            $wo = $wo->data[0];
            $className = get_class($wo);
            $new = clone $wo;

            $reset_fields = array('Approved', 'ClientComments', 'ShowTechs', 'Tech_ID', 'Tech_FName', 'Tech_LName', 'TechMarkedComplete',
                'TechComments', 'MissingComments', 'TechPaid', 'DatePaid', 'PayAmount', 'CheckedIn', 'Checkedby',
                'CheckInNotes', 'StoreNotified', 'NotifyNotes', 'Qty_Applicants', 'Invoiced', 'DateInvoiced', 'DateApproved', 'PayApprove', 'WorkOrderReviewed', 'Deactivated',
                'TechCheckedIn_24hrs', 'TechCalled', 'CalledBy', 'CallDate', 'NoShow_Tech', 'Time_In', 'Time_Out', 'TechEmail', 'TechPhone', 'Date_Assigned',
                'Update_Requested', 'Update_Reason', 'Additional_Pay_Amount', 'Add_Pay_AuthBy', 'Work_Out_of_Scope', 'OutOfScope_Reason', 'Site_Complete',
                'Add_Pay_Reason', 'BackOut_Tech', 'lastModBy', 'lastModDate', 'approvedBy', 'Date_Completed', 'Deactivated_Reason', 'OutofScope_Amount',
                'PayAmount_Final', 'Site_Contact_Name', 'Check_Number', 'MovedToMysql', 'Tech_Bid_Amount', 'pricingRuleID', 'calculatedInvoice', 'calculatedTotalTechPay',
                'calculatedTechHrs', 'baseTechPay', 'PricingCalculationText', 'TripCharge', 'MileageReimbursement', 'MaterialsReimbursement', 'TechnicianFee',
                'TechnicianFeeComments', 'ExpediteFee', 'AbortFee', 'AfterHoursFee', 'OtherFSFee', 'TechMiles', 'PricingRuleApplied', 'AbortFeeAmount', 'PricingRan',
                'DateIncomplete', 'ACSInvoiced', 'ACSDateInvoiced', 'P2TAlreadyAssigned', 'Date_In', 'Date_Out', 'CallClosed', 'ContactHD', 'HDPOC', 'ShortNotice',
                'BU1FLSID', 'BU1Info', 'FLS_ID', 'Date_Checked_in', 'ExtraTime', 'Unexpected_Steps', 'Unexpected_Desc', 'AskedBy', 'AskedBy_Name', 'PreCall_2',
                'Incomplete_Paperwork', 'Paperwork_Received', 'DeactivationCode', 'PreCall_1', 'FS_InternalComments', 'FLS_OOS', 'Penalty_Reason', 'SATRecommended',
                'SATPerformance', 'Backup_FLS_ID', 'PartsProcessed', 'NewPartCount', 'ReturnPartCount', 'PcntDeduct', 'Net_Pay_Amount', 'PreCall_1_Status',
                'PreCall_2_Status', 'bid_comments', 'StartDate', 'StartTime', 'EndDate', 'EndTime');

            $techFields = array('Tech_ID', 'Tech_FName', 'Tech_LName', 'TechEmail', 'TechPhone', 'Tech_Bid_Amount', 'TechComments');
            $startDate = array('StartDate', 'StartTime', 'EndDate', 'EndTime','Site_Contact_Name');//13909

           	if($keepStartDate == "true") $reset_fields = array_diff($reset_fields,$startDate);
           	if($keepAssignedTech == "true") $reset_fields = array_diff($reset_fields, $techFields);

            if (!empty($copyToCompanyId)) {
                array_push($reset_fields, "Company_ID", "Company_Name");
            }

            if (!empty($copyToProjectId)) {
                array_push($reset_fields, "Project_ID", "Project_Name");
            }
            //13955
            foreach ($reset_fields as $field) {
                if (property_exists($className, $field)) {
                    $new->$field = "";
            }
            }
            //23955

            $new->TB_UNID = NULL;
            $new->WO_ID = $new->WO_ID;

            //SAVE WO
            try {
                $return = $search->copyWorkOrder($this->_login . '|pmContext=' . $company, $this->_password, false, $new, true, $copyToCompanyId, $copyToProjectId);
            } catch (Exception $e) {
                error_log($e);
            }
            if ($return->success && !empty($return->data[0])) {
                $id = $return->data[0]->WIN_NUM;
                $wo = $return->data[0];
                $search->copyWorkOrderDocs($this->_login . '|pmContext=' . $company, $this->_password, $win, $return->data[0]->WIN_NUM);
                //disabling copy bid data 
                //$search->copyBidData($win, $return->data[0]->WIN_NUM);
                Core_TimeStamp::createTimeStamp($id, $this->_login, 'Work Order Created (Copy Work Order #' . $win . ')', $company, null, $wo->Project_Name, null, null, null);
                Core_TimeStamp::createWOACSTimeStamp($id, $company);
                $return->data = $id;
            }
            $response = $this->getResponse();
            $response->clearAllHeaders()->clearBody();
            $response->setHeader('Content-type', 'application/json');
            return $response->setBody(json_encode($return));
        }
    }

    public function updatepayfinanAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        /* init progress bar */
        $progress = Core_ProgressBar::getInstance()->setUserLogin($this->_login);
        $data = array('percent' => 0, 'label' => 'Beginning...', 'done' => false);
        $progress->write('updatepayfinan', json_encode($data));
        /* -- init progress bar -- */

        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $response->setHeader('Content-type', 'application/json');

        $params = $this->getRequest()->getParams();
        $winNum = $params['WIN_NUM'];
        $company = $params['company'];
        $context = empty($company) ? "" : '|pmContext=' . $company;

        $api = new Core_Api_Class;

        // progress bar data
        $data['percent'] = 0;
        $data['label'] = "Updating<br />WIN #" . $winNum;
        $data['done'] = false;
        $progress->write('updatepayfinan', json_encode($data));
        //--progress bar data--

        $woInfo = $api->getWorkOrder($this->_login . $context, $this->_password, $winNum);
        if ($woInfo->success) {
            if (!empty($woInfo->data[0]->WIN_NUM)) {
                // progress bar data
                $data['percent'] = isset($params['updateApproveButton']) ? 33 : 50;
                $data['label'] = "Updating<br />WIN #" . $winNum;
                $data['done'] = false;
                $progress->write('updatepayfinan', json_encode($data));
                //--progress bar data--

                $wo = $woInfo->data[0];

                $hasError = false;
                switch ($wo->Status) {
                    case WO_STATE_CREATED:
                    case WO_STATE_PUBLISHED:
                        $jsonContext = json_encode(array('error' => 1, 'msg' => 'This work order is not assigned'));
                        $hasError = true;
                        break;
                    case WO_STATE_IN_ACCOUNTING:
                    case WO_STATE_COMPLETED:
                    case WO_STATE_DEACTIVATED:
                        $jsonContext = json_encode(array('error' => 1, 'msg' => 'This work order has been completed'));
                        $hasError = true;
                        break;
                }

                if (!$hasError) {
                    $wo->baseTechPay = $params['baseTechPay'];
                    $wo->OutofScope_Amount = $params['OutofScope_Amount'];
                    if (!empty($params['OutofScope_Amount'])) {
                        $wo->Work_Out_of_Scope = 'True';
                        $wo->OutOfScope_Reason = 'quick approve';
                    }
                    $wo->TripCharge = $params['TripCharge'];
                    $wo->MileageReimbursement = $params['MileageReimbursement'];
                    $wo->MaterialsReimbursement = $params['MaterialsReimbursement'];
                    $wo->Additional_Pay_Amount = $params['Additional_Pay_Amount'];
                    $wo->AbortFeeAmount = $params['AbortFeeAmount'];
                    $wo->Qty_Devices = $params['Qty_Devices'];
                    $wo->Qty_Visits = $params['Qty_Visits'];
                    $wo->calculatedTechHrs = $params['calculatedTechHrs'];
                    if (!empty($params['Add_Pay_Reason']))
                        $wo->Add_Pay_Reason = $params['Add_Pay_Reason'];
                    else
                        $wo->Add_Pay_Reason = '';
                    $wo->PayAmount = floatval($params['PayAmount']);
                    $wo->Add_Pay_AuthBy = $this->_login;
                    if (floatval($params['AbortFeeAmount']) != 0) {
                        $wo->AbortFee = true;
                    } else {
                        $wo->AbortFee = false;
                    }
                    //376
                    $approved_amount1 = 0 ;
                    $expenseReportingTotal = $this->getRequest()->getParam('Approved_expenseReportingTotalTech',array());
                    if(!empty($expenseReportingTotal))
                    {
                        $Core_Api_WosClass = new Core_Api_WosClass;
                        for($i = 0;$i < count($expenseReportingTotal["Approved_bcatid"]);$i++)
                        {
                            if(!empty($expenseReportingTotal["Approved_bcatid"][$i]))
                            {
                                if($expenseReportingTotal["Approved_reimbursable_thru_fs_id"][$i])
                                    $approved_amount1 += $expenseReportingTotal["Approved_amount"][$i];
                            }    
                        }    
                    }   
                    $wo->Expensethrough = $approved_amount1;
                    if(isset($params["Approved"]))
                    $wo->Approved=$params["Approved"];                    
                    //end 376                  
                    $apiResponse = $api->updateWorkOrder($this->_login . $context, $this->_password, $wo->WIN_NUM, $wo, true);

                    //--- 13360
                    $apiResponse1 = $api->saveWorkOrderAuditData( 
                        $wo->WIN_NUM,
                        isset($params['AuditNeeded']),
                        $this->_login,
                        date("Y-m-d H:i:s"),
                        isset($params['AuditComplete']),
                        $this->_login,
                        date("Y-m-d H:i:s")                    
                    );
                    
                    //---
                    //376 update Expense Reporting:
                    $expenseReportingTotalTech = $this->getRequest()->getParam('Approved_expenseReportingTotalTech',array());
                    
                    if(!empty($expenseReportingTotalTech))
                    {
                        $Core_Api_WosClass = new Core_Api_WosClass;
                        for($i = 0;$i < count($expenseReportingTotalTech["Approved_bcatid"]);$i++)
                        {
                            if(!empty($expenseReportingTotalTech["Approved_bcatid"][$i]))
                            {
                                $bcatid = $expenseReportingTotalTech["Approved_bcatid"][$i];
                                $approved = $expenseReportingTotalTech["Approved_approved"][$i];
                                $reimbursable_thru_fs_id = $expenseReportingTotalTech["Approved_reimbursable_thru_fs_id"][$i];
                                $approved_amount = $expenseReportingTotalTech["Approved_amount"][$i];
                                $Core_Api_WosClass->updateDataForWOBCat($bcatid,$reimbursable_thru_fs_id,$approved,$approved_amount);                                
                            }    
                        }    
                    }    
                    if ($apiResponse->success) {
                        if (isset($params['updateApproveButton'])) {
                            $data['percent'] = 67;
                            $data['label'] = "Approving<br />WIN #" . $winNum;
                            $data['done'] = false;
                            $progress->write('updatepayfinan', json_encode($data));

                            $apiResponse = $api->approveWorkOrder($this->_login . $context, $this->_password, $wo->WIN_NUM);
                            if ($apiResponse->success) {
                                $jsonContext = json_encode(array('error' => 0));
                            } else {
                                $jsonContext = json_encode(array('error' => 1, 'msg' => 'Approve failed: ' . $apiResponse->errors[0]->message));
                            }
                        } else {
                            $jsonContext = json_encode(array('error' => 0));
                        }
                    } else {
                        if (preg_match('/^Add_Pay_Reason/', $apiResponse->errors[0]->message)) {
                            $jsonContext = json_encode(array('error' => 1, 'msg' => 'Comments required for additional pay'));
                        } else {
                            $jsonContext = json_encode(array('error' => 1, 'msg' => $apiResponse->errors[0]->message));
                        }
                    }
                    
                    
                }
            } else
                $jsonContext = json_encode(array('error' => 1, 'msg' => 'There is no such Work Order'));
        } else
            $jsonContext = json_encode(array('error' => 1, 'msg' => 'There is no such Work Order'));

        // progress bar data
        $data['percent'] = 100;
        $data['label'] = 'Done';
        $data['done'] = true;
        $progress->write('updatepayfinan', json_encode($data));
        //-- progress bar data --

        $response->setHeader('Content-Length', strlen($jsonContext), true)
                ->setBody($jsonContext);
    }

    public function saveBusinessProfileAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $request = $this->getRequest();
        $params = $request->getParams();
        $type = $params['type'];

        if ($type == 'tech')
            $id = $this->authTech();
        else
            $id = $this->authAdmin();

        $success = 'error';
        $_success = '';
        if (!$id) {

            $_success = array('success' => $success);
        } else {
            $MyWebSite = $params['MyWebSite'];
            $techID = $params['techID'];
            $Core_Api_TechClass = new Core_Api_TechClass;
            $Core_Api_TechClass->editMyWebSite($techID, $MyWebSite);
        }
        $re = $this->saveBusinessProfile($params,$_GET['techID']);

        if(empty($re))
        {
            $_success = array('success' => $success);
        }
        else
        {
            $_success = array('success' => '1','file'=>$re);
        }
//        
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $return = json_encode($_success);

        $response->setHeader('Content-Length', strlen($_success), true);
        $response->setBody($return);
    }

    private function saveBusinessProfile($params,$id) {
        try {
            $returnArr = array();
            if ($_FILES) {
                $returnArr['uploadResult'] = Core_Tech::uploadTechFileAWS($_FILES, $id);
            }

            if (!empty($_POST)) {
                $returnArr['uploadResult'] = Core_Tech::updateTechInfo($params);
            }
            return $returnArr;
        } catch (Exception $e) {
//			print_r($e);
        }
        return null;
    }

    public function submitPaymentInfoAction() {
        $request = $this->getRequest();
        $params = $request->getParams();

        $type = $params['type'];

        if ($type == 'tech')
            $id = $this->authTech();
        else
            $id = $this->authAdmin();

        $success = 'error';
		if ( !$id ) return $this->_helper->json(array('success'=>$success));

        if ($this->submitPaymentInfo($params))
            $success = "success";
        else
            $success = "error";

        $this->_helper->json(array('success' => $success));
    }

    private function submitPaymentInfo($params) {
        try {
            $result = Core_Tech::submitPaymentInfo($params);
            return $result;
        } catch (Exception $e) {
//			print_r($e);
        }
        return false;
    }

    public function createTechAction() {
        $request = $this->getRequest();
        $params = $request->getParams();

        $type = $params['type'];

//		if ($type == 'tech')
//	        $id = $this->authTech();
//		else
        $id = $this->authAdmin();

        $success = 'error';
		if ( !$id ) return $this->_helper->json(array('success'=>$success));
        if(empty($params["RegDate"])){
            $params["RegDate"] = date("Y-m-d");
        }        
        $newID = $this->createTech($params);
        if ($newID)
            $success = "success";
        else
            $success = "error";

        $this->_helper->json(array('success' => $success, 'id' => $newID));
    }

    public function updateTechInfoAction() {
        $request = $this->getRequest();
        $params = $request->getParams();

        //---
        $type = $params['type'];
        $techInfoBerfore = Core_Tech::getProfile($params['techID'], true, API_Tech::MODE_TECH, "");

        if ($type == 'tech')
            $id = $this->authTech();
        else
            $id = $this->authAdmin();
        $Core_Api_TechClass = new Core_Api_TechClass;
        $success = 'error';
		if ( !$id ) return $this->_helper->json(array('success'=>$success));

        
        if ($this->updateTechInfo($params))
        {
        	$success = '';
            $techInfoAfter = Core_Tech::getProfile($params['techID'], true, API_Tech::MODE_TECH, "");
            if(!empty($techInfoBerfore))
            {
               if($techInfoBerfore['PrimaryPhone'] !=  $techInfoAfter['PrimaryPhone'])
               {
                   $aClient = $Core_Api_TechClass->setPrimaryPhoneValid($params['techID'],1, $this->_login);
               }  
               if($techInfoBerfore['SecondaryPhone'] !=  $techInfoAfter['SecondaryPhone'])
               {
                   $aClient = $Core_Api_TechClass->setSecondaryPhoneValid($params['techID'],1, $this->_login);
               } 
            }
            //--- for TrapolloMedical Certified Status
            if(isset($params['TMCStatusChk']) && isset($params['TMCDateTxt']))
            {
                $techId = $params['techID'];
                $TMCStatus = $params['TMCStatusChk'];
                $TMCDate = $params['TMCDateTxt'];
                //$TMCDate = "2011-02-14";//test
                $valid=1;                
                if(!empty($TMCDate))
                {
                    if(!$this->isDateValid($TMCDate))
                    {
                        $valid=0;
                        $success = "error: TrapolloMedical Certified Status Date is invalid.";
        }    
                }
                
                if($valid==1)                    
                {
                    if(!empty($TMCDate))
                        $TMCDateDB = date_format(new DateTime($TMCDate),'Y-m-d'); 
                    $Core_Api_TechClass->updateTMC($techId,$TMCStatus,$TMCDateDB);
                    
//                    return $this->_helper->json(array('success' => 'success',
//                                            'section'=>'ClientCredentials',
//                                            'TMCStatus'=>$TMCStatus,
//                                            'TMCDate'=>$TMCDate
//                                        ));                            
                }

            }                        
            
            //--- for MACI Status
            if(isset($params['NACIDate']) && isset($params['NACICertStatus']))
            {
                $techId = $params['techID'];
                $NACIDate = $params['NACIDate'];
                $NACItatus = $params['NACICertStatus'];
                //$TMCDate = "2011-02-14";//test
                $valid=1;                
                if(!empty($NACIDate))
                {
                    if(!$this->isDateValid($NACIDate))
                    {
                        $valid=0;
                        $success = "error: NACI Status Date is invalid.";
                    }    
                }
                
                if($valid==1)                    
                {
                    if(!empty($NACIDate))
                        $NACIDateDB = date_format(new DateTime($NACIDate),'Y-m-d'); 
                        $techCert = new Core_TechCertification($techId,17,null,$NACIDateDB,null);
                        if($NACItatus==0)
                        {
                            $techCert->delete();
                        }
                        else
                        {
                            $techCert->save();                        
                        }
                }
            }
            else if(isset($params['NACICertStatus'])) {
                $NACItatus = $params['NACICertStatus'];
                $techCert = new Core_TechCertification($techId,17,null,null,null); 
                if($NACItatus==0)
                {
                    $techCert->delete();
                }
                else
                {
                    $techCert->save();                        
                }
                
            }
            // for VOP
            $success = $this->updateCert($params,'VonagePlusStatusChk','VonagePlusInput',"",18,'Vonage Plus');
            
            // for NCRBadgedTechnician
            $success = $this->updateCert($params,'NCRBadgedTechnicianStatusChk','NCRBadgedTechnicianInput',"",19,'NCR Badged Technician');

            // for LMSPreferred
            $success = $this->updateCert($params,'LMSPreferredStatusChk','LMSPreferredInput',"",20,'LMS Preferred Technician');

            // for LMSPreferred
            $success = $this->updateCert($params,'LMSPlusKeyStatusChk','LMSPlusKeyInput',"",21,'LMS+ Key Contractor');
            // for FlextronicsContractor  
            $success = $this->updateCert($params,'FlextronicsContractorStatusChk','FlextronicsContractorInput',"FlextronicsContractorNumInput",22,'Flextronics Contractor');
            // for FlextronicsScreened
            $success = $this->updateCert($params,'FlextronicsScreenedStatusChk','FlextronicsScreenedInput',"",23,'Flextronics Screened');
            
            $success = $this->updateCert($params,'CSCDesksideSupportTechStatusChk','CSCDesksideSupportTechInput',"",25,'CSC Deskside Support Tech');
            $success = $this->updateCert($params,'ServRightBrotherStatusChk','ServRightBrotherInput',"",26,'ServRight – Brother Certification');
            
            $success = $this->updateCert($params,'FlextronicsRecruitStatusChk','FlextronicsRecruitInput',"",30,'Flextronics Recruit');
            
            $success = $this->updateCert($params,'CraftmaticCertifiedStatusChk','CraftmaticCertifiedInput',"",31,'Craftmatic Certified');
            $params["CompuComCertifiedStatusInputCk1"] = 1;
            $params["CompuComCertifiedStatusInputCk0"] = 0;
            $this->updateCert($params,"CompuComCertifiedStatusInputCk0",'CompuComCertifiedInput',"",33,'CompuCom Certified');
            $this->updateCert($params,"CompuComCertifiedStatusInputCk0",'CompuComCertifiedInput',"",34,'CompuCom Certified');
            $this->updateCert($params,"CompuComCertifiedStatusInputCk0",'CompuComCertifiedInput',"",35,'CompuCom Certified');
            $this->updateCert($params,"CompuComCertifiedStatusInputCk1",'CompuComCertifiedInput',"",$params['CompuComCertifiedStatusInput'],'CompuCom Certified');

            $params["PurpleTechnicianCertifiedInputCk1"] = 1;
            $params["PurpleTechnicianCertifiedInputCk0"] = 0;
            $this->updateCert($params,"PurpleTechnicianCertifiedInputCk0",'PurpleTechnicianInput',"",39,'Purple Technician');
            $this->updateCert($params,"PurpleTechnicianCertifiedInputCk0",'PurpleTechnicianInput',"",40,'Purple Technician');
            $this->updateCert($params,"PurpleTechnicianCertifiedInputCk1",'PurpleTechnicianInput',"",$_POST['PurpleTechnicianStatusInput'],'Purple Technician');
        
            Core_Tech::updateCerts($params);
            
            
            $certificationsNew = $params["certificationsNew"];
            $FSTagClass = new  Core_Api_FSTagClass();
            //13931
            if(!empty($certificationsNew)){
                //FSTag_181_MultiLevelCbx
                //print_r($certificationsNew);die();//test
                
                foreach($certificationsNew as $_certificationid){
                    $Date = (!empty($params['certificationsNewDate_'.$_certificationid])?date_format(new DateTime($params['certificationsNewDate_'.$_certificationid]), "Y-m-d"):date("Y-m-d"));
                    if(!empty($params['certificationsNewStatus_'.$_certificationid])){
                        $FSTagClass->saveFSTagForTech(
                                $params['techID'],
                                $_certificationid,
                                $params['certificationsNewNumber_'.$_certificationid]
                                ,""
                                ,$Date);
                    } else if(!empty($params['FSTag_'.$_certificationid.'_MultiLevelCbx'])) {
                         $FSTagClass->saveFSTagForTech(
                                $params['techID'],
                                $_certificationid,
                                $params['certificationsNewNumber_'.$_certificationid]
                                ,""
                                ,$Date,$params['FSTag_'.$_certificationid.'_MultiLevelCbx']);
                                
                    }else{
                        $FSTagClass->deleteFSTagForTech($params['techID'],$_certificationid);
                    }
                }
            }
            //end 13931
            $BlackBoxtechID = $params['techID'];
            $BlackBoxCablingid = $params['BlackBoxCablingid'];
            $BlackBoxCablingcertifications = $params['BlackBoxCablingcertifications'];
            $BlackBoxCablingDate =!empty($params['BlackBoxCablingcertDate'])?date_format(new DateTime($params['BlackBoxCablingcertDate']), "Y-m-d"):date("Y-m-d");
            $BlackBoxTelecomid = $params['BlackBoxTelecomid'];
            $BlackBoxTelecomcertifications = $params['BlackBoxTelecomcertifications'];
            $BlackBoxTelecomDate =!empty($params['BlackBoxTelecomcertDate'])?date_format(new DateTime($params['BlackBoxTelecomcertDate']), "Y-m-d"):date("Y-m-d");
            
            if(empty($BlackBoxCablingcertifications))
            {
                $Core_Api_TechClass->denyBBCert($BlackBoxCablingid,$BlackBoxtechID);
            }
            else if($BlackBoxCablingcertifications=="pending")
            {
                if(empty($BlackBoxCablingid))
                {
                    $BlackBoxCablingid = 46;
                }
                $Core_Api_TechClass->saveBBPending($BlackBoxCablingid,$BlackBoxtechID,$BlackBoxCablingDate);
            }
            else
            {
                $Core_Api_TechClass->saveBBCert($BlackBoxCablingcertifications,$BlackBoxtechID,$BlackBoxCablingDate);
            }
            
            if(empty($BlackBoxTelecomcertifications))
            {
                $Core_Api_TechClass->denyBBCert($BlackBoxTelecomid,$BlackBoxtechID);
            }
            else if($BlackBoxTelecomcertifications=="pending")
            {
                if(empty($BlackBoxTelecomid))
                {
                    $BlackBoxTelecomid = 49;
                }
                $Core_Api_TechClass->saveBBPending($BlackBoxTelecomid,$BlackBoxtechID,$BlackBoxTelecomDate);
            }
            else
            {
                $Core_Api_TechClass->saveBBCert($BlackBoxTelecomcertifications,$BlackBoxtechID,$BlackBoxTelecomDate);
            }
            //13393
            if(isset($params['CbxGGETechEvaluationFirst']))
            {
                $dateFirst = !empty($params['GGETechEvaluationDateInputFirst'])
                    ?date_format(new DateTime($params['GGETechEvaluationDateInputFirst']), "Y-m-d")
                    :(!empty($params['CbxGGETechEvaluationFirst'])?date("Y-m-d"):"");
                $Core_Api_TechClass->saveFirstAttemptScoreOrStatus($params['techID'],$params['CbxGGETechEvaluationFirst'],$dateFirst);
            }
            if(isset($params['CbxGGETechEvaluationSecond']))
            {    
                $dateSecond = !empty($params['GGETechEvaluationDateInputSecond'])
                    ?date_format(new DateTime($params['GGETechEvaluationDateInputSecond']), "Y-m-d")
                    :(!empty($params['CbxGGETechEvaluationSecond'])?date("Y-m-d"):"");
                $Core_Api_TechClass->saveSecondAttemptScoreOrStatus($params['techID'],$params['CbxGGETechEvaluationSecond'],$dateSecond);
            }
            // send email
//            if(isset($params['CbxGGETechEvaluationFirst']))
//                Core_Api_Class::sendGGETechEvaluation($params['techID']);
            //13401  
            if(isset($params['MCSA_Cert_Number'])&&empty($params['MCSA_Cert_Number']))
                    $params['hidMCSACertStatusChk']=0;
            if(isset($params['MCP_Cert_Number'])&&empty($params['MCP_Cert_Number']))
                    $params['hidMCPCertStatusChk']=0;
            
            $success = $this->updateCert($params,'hidMCSACertStatusChk','MCSA_Date',"MCSA_Cert_Number",27,'Microsoft Certified Solutions Associate');
            $success = $this->updateCert($params,'hidMCPCertStatusChk','MCP_Date',"MCP_Cert_Number",28,'Microsoft Certified Professional');
            //end 13401
            //--- for TrapolloMedical Certified Status
            if(empty($success)&& isset($params['TMCStatusChk']) && isset($params['TMCDateTxt']))
            {
                $techId = $params['techID'];
                $TMCStatus = $params['TMCStatusChk'];
                $TMCDate = $params['TMCDateTxt'];
                //$TMCDate = "2011-02-14";//test
                $valid=1;                
                if(!empty($TMCDate))
                {
                    if(!$this->isDateValid($TMCDate))
                    {
                        $valid=0;
                        $success = "error: TrapolloMedical Certified Status Date is invalid.";
                    }    
                }
                
                if($valid==1)                    
                {
                    if(!empty($TMCDate))
                        $TMCDateDB = date_format(new DateTime($TMCDate),'Y-m-d'); 
                    $Core_Api_TechClass->updateTMC($techId,$TMCStatus,$TMCDateDB);
                    
                    return $this->_helper->json(array('success' => 'success',
                                            'section'=>'ClientCredentials',
                                            'TMCStatus'=>$TMCStatus,
                                            'TMCDate'=>$TMCDate
                                        ));                            
                }
                //echo($valid);die(' valid');
            }                                                
            //---
            if(empty($success)) $success = "success";
            
//echo("DoControler->endupdate updateTechInfo <pre>");print_r($params);echo("</pre>");die();    

        }  //end if ($this->updateTechInfo($params))
        else
        {
        	$success = "error";
        }

        $this->_helper->json(array('success' => $success));
    }
    
    public function updateCert($params,$_status,$_date,$_num,$id,$type)
    {
        $error = "";
        if(isset($params[$_status]) && isset($params[$_date]))
        {
            $techId = $params['techID'];
            $status = $params[$_status];
            $date = $params[$_date];
            $num = "";
            if(!empty($_num) && isset($params[$_num])) $num = $params[$_num];
            $valid=1;                
            if(!empty($date))
            {
                if(!$this->isDateValid($date))
                {
                    $valid = 0;
                    $error = $type." Status Date is invalid.";
                }    
            }

            if($valid==1)                    
            {
                if(!empty($date))
                    $date = date_format(new DateTime($date),'Y-m-d'); 
                $techCert = new Core_TechCertification($techId,$id,$num,$date,null);
                if($status==0)
                {
                    $techCert->delete();
                }
                else
                {
                    $techCert->save();                        
                }
            }
        }
        
        return $error;
       }        
	public function updateTechPreferredAction(){
		$request = $this->getRequest();
		$params  = $request->getParams();
                $authData = array('login'=>$this->_login . '|pmContext=' . $this->_companyId, 'password'=>$this->_password);
                $user = new Core_Api_User();
                $user->checkAuthentication($authData);
		$apiWosClass = new Core_Api_WosClass();
		$success = 'error';
		if($params['do'] == "remove")
                {
                    if($apiWosClass->ClientRemovePreferTech($params['id'],$user->getClientId(),$params['companyID']))
                   // if(Core_Tech::removePreferredTech($params['companyID'], $params['id']))
                    {
				$success = "success";
                    }
                    else
                    {
				$success = "error";
			}
		}
                else
                {
                    if($apiWosClass->ClientPreferTech($params['id'],$user->getClientId(),$params['companyID']))
                    //if(Core_Tech::addPreferredTech($params['companyID'], $params['id']))
                    {
				$success = "success";
                    }
                    else
                    {
				$success = "error";
			}
		}
		$this->_helper->json(array('success'=>$success));
	}
	
	public function updateTechDeniedAction(){
		$request = $this->getRequest();
		$params  = $request->getParams();
		
		$success = "error";
		if($params['do'] == "remove"){
                            $authData = array('login'=>$this->_login . '|pmContext=' . $this->_companyId, 'password'=>$this->_password);

                            $user = new Core_Api_User();
                            $user->checkAuthentication($authData);
                            $apiremovedeninedctech = new Core_Api_WosClass();
                            $apiremovedeninedctech->TechNoLongerClientDenied($params['id'],$user->getClientId(),$params['companyID']) ;
			if(Core_Tech::removeClientDeniedTech($params['id'], $params['companyID'])){
				$success = "success";
			}else{
				$success = "error";
			}
		}else{
			if(Core_Tech::clientDenyTech($params['id'], "", $params['companyID'], $params['clientID'])){
				$success = "success";
			}else{
				$success = "error";
			}
		}
		$this->_helper->json(array('success'=>$success));
	}
	
    private function createTech($params) {
        try {
            $result = Core_Tech::createTech($params);
            return $result;
        } catch (Exception $e) {
//			print_r($e);
        }
        return false;
    }

    private function updateTechInfo($params) {
    	$techID = $params['techID'];
		if (!empty($params['certificationsInfo'])) {
			$certs = $params['certificationsInfo'];
			unset($params['certificationsInfo']);
			$params['certificationsInfo'] = array();
			$certs = json_decode($certs);
			foreach ($certs as $info) {
				if (!empty($info->date)) {
					$info->date = new Zend_Date($info->date,'mm/dd/YYYY');
					$info->date = $info->date->toString('YYYY-mm-dd');
				} else {
					$info->date = NULL;
				}
				$params['certificationsInfo'][$info->column] = array("cert" => new Core_TechCertification($techID, $info->column, $info->number, $info->date), "checked" => $info->checked == 1);
			}
		}
        try {
        	$result = Core_Tech::updateTechInfo($params);
        	return $result;
        } catch (Exception $e) {
//			print_r($e);
        }
        return false;
    }

    public function deleteTechFileAction() {
        $request = $this->getRequest();
        $params = $request->getParams();

        $type = $params['type'];

        if ($type == 'tech')
            $id = $this->authTech();
        else
            $id = $this->authAdmin();

        $success = 'error';
		if ( !$id ) return $this->_helper->json(array('success'=>$success));

        if ($this->deleteTechFile($params))
            $success = "success";
        else
            $success = "error";

        $this->_helper->json(array('success' => $success));
    }

    private function deleteTechFile($params) {
        try {
            $result = Core_Tech::deleteTechFileAWS($params);
            return $result;
        } catch (Exception $e) {
            error_log($e->getMessage());
        }
        return false;
    }

	public function likeTechAction(){
		$request = $this->getRequest();
		$params  = $request->getParams();
		
		$techId = $params['id'];
		$clientId = $params['clientId'];
		
		$success = 'error';
		
		$data = Core_Tech::likeTech($techId, $clientId);
		$dataTotal = Core_Tech::getTechNetLikes($techId);//13910
                
		if($data){
			$success = "success";
		}else{
			$success = "error";
		}
		
		$this->_helper->json(array('success'=>$success, 'likeData' => $data,'totalData' => $dataTotal));//13910
	}
	
	public function unlikeTechAction(){
		$request = $this->getRequest();
		$params  = $request->getParams();
		
		$techId = $params['id'];
		$clientId = $params['clientId'];
		
		$success = 'error';
		
		$data = Core_Tech::unlikeTech($techId, $clientId);
		$dataTotal = Core_Tech::getTechNetLikes($techId);//13910
		if($data){
			$success = "success";
		}else{
			$success = "error";
		}
		
		$this->_helper->json(array('success'=>$success, 'likeData' => $data,'totalData' => $dataTotal));//13910
	}
	
	public function getTechLikeInfoAction(){
		$request = $this->getRequest();
		$params  = $request->getParams();
		
		$techId = $params['id'];
		$clientId = $params['clientId'];
		
		$success = 'error';
		
		$data = Core_Tech::getTechLikes($techId, $clientId);
		
		if($data){
			$success = "success";
		}else{
			$success = "error";
		}
		
		$this->_helper->json(array('success'=>$success, 'likeData' => $data));
	}
	
	public function getTechScheduleAction(){
		$request = $this->getRequest();
		$params  = $request->getParams();
		
		$techData = null;
    	$woData = null; 
    
	    $api = new Core_Api_Class();
	        
	    $techID = $params['id'];
	        
	   	$authData = array('login'=>$this->_login . '|pmContext=' . $this->_companyId, 'password'=>$this->_password);

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);
		
		if (!$user->isAuthenticate()) return false;
		
	    $woFilter = new API_AdminWorkOrderFilter();
	    $woFilter->StartDateFrom = date('m/d/Y', "1 January 1969");
	    $woFilter->Tech_ID = $techID;
	
          
        $woFilter->Deactivated=0;  // CH558  

	    $count = null;
		$countRows = 0;
		$page = isset ($params["offset"]) ? intval ($params["offset"]) : 0;
		$numRecords = isset ($params["limit"]) ? intval ($params["limit"]) : "";
		$sort = "StartDate DESC";
        try {
        	$result = API_AdminWorkOrderFilter::filter(null,
                                                      null,
                                                      null,
                                                      null,
                                                      null,
                                                      $woFilter, $sort, $numRecords, $page, $countRows);

            $woArray = API_AdminWorkOrder::makeWorkOrderArray($result->toArray(), new API_AdminWorkOrder(NULL));
                        
	        $returnArr = array();
	        foreach($woArray as $data){
	        	$d = get_object_vars($data);

	        	if (!empty($d["StartDate"])) {
		     	    $startDate = new Zend_Date($d["StartDate"], 'yyyy-MM-dd');
		     	    $d['StartDate'] = $startDate->toString('MM/dd/Y');
	        	}
	        	else {
	        		$d['StartDate'] = "";
	        	}
	        	if (!empty($d["EndDate"])) {
		        	$endDate = new Zend_Date($d["EndDate"], 'yyyy-MM-dd');
		     	    $d['EndDate'] = $endDate->toString('MM/dd/Y');
	        	}
	        	else {
	        		$d['EndDate'] = "";
	        	}
	     	    $returnArr[] = $d;
	        }
	        $this->_helper->json(array('success'=>"success", 'techData' => $returnArr));
        } catch (Exception $e) {
            $errors->addError(7, 'SQL Error: '. $e);
            error_log($e);
            $this->_helper->json(array('success'=>"error"));
       }
       
              /*
	    if ($wo->success && !empty($wo->data[0])) {
	        $woData = $wo->data;
	    }
	    */
	}

    public function saveEquipmentAction() {
        $request = $this->getRequest();
        $params = $request->getParams();

        $type = $params['type'];

        if ($type == 'tech')
            $id = $this->authTech();
        else
            $id = $this->authAdmin();

        $success = 'error';
		if ( !$id ) return $this->_helper->json(array('success'=>$success));

        if ($this->saveEquipment($params))
            $success = "success";
        else
            $success = "error";
        $this->_helper->json(array('success' => $success));
    }

    private function saveEquipment($params) {
        $id = $params['techID'];
        try {
            if (!empty($_FILES)) {
				if(isset($params['vehicleDesc']) && $params['vehicleDesc'] != "") $_FILES['description'] = $params['vehicleDesc'];
                $result = Core_Tech::uploadTechFileAWS($_FILES, $id);
            }

            if (!empty($params)) {
                $Core_Api_Class = new Core_Api_Class();
                $Core_Api_Class->saveLanguageSkils($id,$params['languegaSkills']);
                $result = Core_Tech::saveEquipment(json_decode($params['data']), $id, $params['otherTools'], json_decode($params['telephonyData']));
            }

            return $result;
        } catch (Exception $e) {
//			print_r($e);
        }
        return false;
    }

    public function getTechInfoAction() {
        $request = $this->getRequest();
        $params = $request->getParams();

        $type = $params['type'];
		$companyID = $params['companyID'];

        if ($type == 'tech')
            $id = $this->authTech();
        else if ($type == 'client')
            $id = $this->authClient();
        else
            $id = $this->authAdmin();

        //13851    
        $Tech = new Core_Tech();
        //$Tech->Reset_AdjustableBedExpert_Tag($params['id']);
        $Core_Api_FSTagClass = new Core_Api_FSTagClass();
        $Core_Api_FSTagClass->Update_AdjustableBedExpert($params['id']);
        //end 13851
        $success = 'error';
		if ( !$id ) return $this->_helper->json(array('success'=>$success));

		$techInfo = $this->getTechInfo($params, $type == 'admin' ? API_Tech::MODE_ADMIN : API_Tech::MODE_TECH, $companyID);
		$W9Info = Core_Tech::getW9Info($params['id']);

        if ($techInfo) 
        {
            $techInfo['PrimaryPhoneStatus'] = 1;
            $techInfo['SecondaryPhoneStatus'] = 1;
            $techInfo['I9Status'] = 0;
            $techInfo['ServRightBrotherStatusClicked'] = 0;
            $techInfo['I9Date'] = "";
            $techInfo['VonagePlusStatus']="0";
            $techInfo['VonagePlusDate']="";
            
            $techInfo['NCRBadgedTechnicianStatus']="0";
            $techInfo['NCRBadgedTechnicianDate']="";
            
            
            $techInfo['TrapolloMedicalCertifiedStatus']="0";
            $techInfo['TrapolloMedicalCertifiedDate']="";
            
            $techInfo['CopierSkillsAssessmentDate']="";
            $techInfo['CopierSkillsAssessment']="";
            $techInfo['CountryName']="";
            $techInfo['EMCCert'] = "";

            $Core_Api_TechClass = new Core_Api_TechClass();    
            $GGEStatusArray = $Core_Api_TechClass->getStatusArrayForGGECert($techInfo['TechID']);
            $techInfo['GGE1stAttemptStatus'] = $GGEStatusArray['GGE1stAttemptStatus'];
            $techInfo['GGE1stAttemptScore'] = $GGEStatusArray['GGE1stAttemptScore'];
            $techInfo['GGE1stAttemptDate'] = empty($GGEStatusArray['GGE1stAttemptDate'])?"":  date_format(new DateTime($GGEStatusArray['GGE1stAttemptDate']), "m/d/Y");
            $techInfo['GGE2ndAttemptStatus'] = $GGEStatusArray['GGE2ndAttemptStatus'];
            $techInfo['GGE2ndAttemptScore'] = $GGEStatusArray['GGE2ndAttemptScore'];
            $techInfo['GGE2ndAttemptDate'] = empty($GGEStatusArray['GGE2ndAttemptDate'])?"":  date_format(new DateTime($GGEStatusArray['GGE2ndAttemptDate']), "m/d/Y");;
            
            $conutry = Core_Country::getCountryInfoByCode($techInfo["Country"]);
            if(!empty($conutry))
            {
                $techInfo['CountryName'] = $conutry["Name"];
            }
            if (!empty($techInfo['TechID'])) {
            	$api = new Core_Api_TechClass;
            	$result = $api->getExts($techInfo['TechID']);
            	if (!empty($result['TechID'])) {
                    $techInfo['PrimaryPhoneStatus'] = $result['PrimaryPhoneStatus'];
                    $techInfo['SecondaryPhoneStatus'] = $result['SecondaryPhoneStatus'];
                    $techInfo['I9Status'] = $result['I9Status'];
                    $techInfo['I9Date'] = $result['I9Date'] == ""?"":  date_format(new DateTime($result['I9Date']), 'm/d/Y');
                    
                    $techInfo['I9ResultsEnterBy'] = $result['I9ResultsEnterBy'];
                    $techInfo['I9TestRequested'] = $result['I9TestRequested'];
                    $techInfo['I9RequestedDate'] = $result['I9RequestedDate'] == ""?"":  date_format(new DateTime($result['I9RequestedDate']), 'm/d/Y');
                }
                //--- TrapolloMedicalCertified
				$techCerti = new  Core_TechCertification($techInfo['TechID'],5);
				$techCertiRow = $techCerti->getRow();   
				if(!empty($techCertiRow))
				{
					$techInfo['TrapolloMedicalCertifiedStatus']="1";
					if(!empty($techCertiRow['date']))
					{
						$certDate = date_format(new DateTime($techCertiRow['date']),'m/d/Y') ;                         
						$techInfo['TrapolloMedicalCertifiedDate'] = $certDate;
					}
                }
                //--- NCRBadgedTechnicianStatus
                $techCerti = new  Core_TechCertification($techInfo['TechID'],19);
                $techCertiRow = $techCerti->getRow();   
                if(!empty($techCertiRow))
                {
                        $techInfo['NCRBadgedTechnicianStatus']="1";
                        if(!empty($techCertiRow['date']))
                        {
                            $certDate = date_format(new DateTime($techCertiRow['date']),'m/d/Y') ; 
                            $techInfo['NCRBadgedTechnicianDate'] = $certDate;
                    }
                }

                //--- VonagePlusStatusDisplay
                $techCerti = new  Core_TechCertification($techInfo['TechID'],18);
                $techCertiRow = $techCerti->getRow();   
                if(!empty($techCertiRow))
                {
                        $techInfo['VonagePlusStatus']="1";
                        if(!empty($techCertiRow['date']))
                        {
                            $certDate = date_format(new DateTime($techCertiRow['date']),'m/d/Y') ; 
                            $techInfo['VonagePlusDate'] = $certDate;
                        }
                    }
                    //------ Copies Skills
                $techAssessment = $api->getTechAssessment($techInfo['TechID'],1);
                $ispassed =  empty($techAssessment['ispassed'])? 0 : $techAssessment['ispassed'];
                $techInfo['CopierSkillsAssessment'] = $ispassed;
                $techInfo['CopierSkillsAssessmentDate'] = $techAssessment['passed_date'] == ""?"":  date_format(new DateTime($techAssessment['passed_date']), 'm/d/Y');;
                // for EMCCert
                
                $EMCListForTechnician = $api->getEMCListForTechnician($techInfo['TechID']);
                $techInfo['EMCCert'] = $EMCListForTechnician;
                
                // for ServRightBrotherStatus
                $Core_TechCertifications = new Core_TechCertifications;
                $techCerti = new  Core_TechCertification($techInfo['TechID'],26);
                $techCertiRow = $techCerti->getRow();  
                $techInfo['ServRightBrotherStatusClicked'] = ($Core_TechCertifications->getWentToBrotherSite($techInfo['TechID'])
                    && empty($techCertiRow))?1:0;
                //--- Blue Ribbon 
                $techCerti = new  Core_TechCertification($techInfo['TechID'],4);
                $techCertiRow = $techCerti->getRow();   
                if(!empty($techCertiRow))
                {
                    $techInfo['BlueRibbonTechnicianStatus']="1";
                    if(!empty($techCertiRow['date']))
                    {
                        $certDate = date_format(new DateTime($techCertiRow['date']),'m/d/Y') ; 
                        $techInfo['BlueRibbonTechnicianDate'] = $certDate;
            }
                }
                //--- Boot Camp
                $techCerti = new  Core_TechCertification($techInfo['TechID'],3);
                $techCertiRow = $techCerti->getRow();   
                if(!empty($techCertiRow))
                {
                    $techInfo['BootCampCertifiedStatus']="1";
                    if(!empty($techCertiRow['date']))
                    {
                        $certDate = date_format(new DateTime($techCertiRow['date']),'m/d/Y') ; 
                        $techInfo['BootCampCertifiedDate'] = $certDate;
                    }
                }
            }
            $hasDeVryTag=Core_Api_TechClass::hasDeVryTag($techInfo['TechID']);
            if($hasDeVryTag==1)
            {
                $apiTechClass = new Core_Api_TechClass();
                $apiTechClass->saveTechExts($_REQUEST['techID'], array("ReceAvaiWorkNotiEmaiAndSMSMess_forGroupWOonly" => 1,
                "ViewOnlyGroupWOonMyDash" => 1));
            }
            $success = "success";
        }
        else
            $success = "error";
        $Core_Api_TechClass = new Core_Api_TechClass;
        $datacbx = $Core_Api_TechClass->getEMCList();
        $option = '<option value="" sort="">Select an EMC Certification</option>';

        if (!empty($datacbx)) {
            foreach ($datacbx as $item) {
                    $option .= '<option   value="'.$item["name"].'">'.$item['label'].'</option>';
			}
        }
        
        //14041
        //--- country info
        $common = new Core_Api_CommonClass();
        $countries = $common->getCountries_1();
        
        $this->_helper->json(array('success' => $success, 'techInfo' => $techInfo, 'W9Info' => $W9Info,'htmlEMC'=>$option,'countries'=>$countries));
        //end 14041
    }

	public function getTechIsoInfoAction(){
		$request = $this->getRequest();
		$params  = $request->getParams();
		
		$isoId = $params['isoId'];
		
		$success = 'error';
		$iso = new Core_ISO();
		
		$iso->load($isoId);
		$data = $iso->toArray();
		if($data){
			$success = 'success';
		}else{
			$success = 'error';
		}
		
		$this->_helper->json(array('success'=>$success, 'isoInfo' => $data));
	}
	
	private function getTechInfo($params, $mode = API_Tech::MODE_TECH, $companyID) {
        try {
            //13735
            //$FSExpertClass = new Core_Api_FSExpertClass();
            //$FSExpertClass->applyAllFSExpertRules_ForTech($params['id']);
            //end 13735            
			$techInfo = Core_Tech::getProfile($params['id'], true, $mode, $companyID);
            $Core_Api_TechClass = new Core_Api_TechClass;
            $FLSTestDatePass = $Core_Api_TechClass->getFLSTestDatePass($techInfo['TechID']);
            $techInfo['FLSTestDatePass'] = $FLSTestDatePass;
            //---
            $techInfo['MyWebSite'] = "";
            
            $getExts = $Core_Api_TechClass->getExts($techInfo['TechID']);
            if (!empty($getExts['TechID'])) {
                $techInfo['MyWebSite'] = $getExts['MyWebSite'];
            }
            return $techInfo;
        } catch (Exception $e) {
//			print_r($e);
        }
        return false;
    }

    public function saveAdminInternalInfoAction() {
        $request = $this->getRequest();
        $params = $request->getParams();
        
        $auth = $this->authAdmin();
        $id = $params['techID'];

        $success = 'error';
		if ( !$auth ) return $this->_helper->json(array('success'=>$success));

        if (!empty($params['BG_Test_ResultsBy_Lite'])) {
            $params['BG_Test_ResultsBy_Lite'] = $this->_login;
        }
        if (!empty($params['BG_Test_ResultsBy_Full'])) {
            $params['BG_Test_ResultsBy_Full'] = $this->_login;
        }

        if (!empty($params['FLSCSP_RecBy'])) {
            $params['FLSCSP_RecBy'] = $this->_login;
        }
         //13346
        if(!isset($params['AllowText'])&&isset($params['UnAllowText']))
            $params['AllowText'] = (($params['UnAllowText']==1)||$params['UnAllowText']==true)?0:1;        
        //end 13346
        try {
        	$techInfo = Core_Tech::saveAdminInternalInfo($params, $id);
            $Core_Api_TechClass = new Core_Api_TechClass;
            $Core_Api_TechClass->editI9($id
                    ,$params['I9Status']
                    ,trim($params['I9DateAdministered']) != ""?$params['I9DateAdministered']:date('Y-m-d')
                    ,$params['I9TestRequested']
                    ,$params['USAuthorizationRequestDate']
                    ,$this->_login);
            if(isset($params['NACICertStatusEx1'])) {
                $NACItatus = $params['NACICertStatusEx1'];
                $techCert = new Core_TechCertification($id,6,null,null,null); 
                $row = $techCert->loadRow();
                if($NACItatus==0)
                {
                    $techCert->delete();
                }
                else if(empty($row))
                {
                    $techCert->save();                        
                }
                
            }
        } catch (Exception $e) {
//			print_r($e);
        }

        if ($techInfo)
            $success = "success";
        else
            $success = "error";

        $this->_helper->json(array('success' => $success));
    }

    private function authTech() {
        $authData = array('login' => $this->_login, 'password' => $this->_password);

        $user = new Core_Api_TechUser();
        $user->checkAuthentication($authData);
        return $user->isAuthenticate() ? $user->getTechId() : false;
    }

    private function authClient() {
        $authData = array('login' => $this->_login, 'password' => $this->_password);

        $user = new Core_Api_User();
        $user->checkAuthentication($authData);
        return $user->isAuthenticate();
    }

    private function authAdmin() {
        $authData = array('login' => $this->_login, 'password' => $this->_password);

        $user = new Core_Api_AdminUser();
        $user->checkAuthentication($authData);
        return $user->isAuthenticate();
    }

    public function techUserNotTakenAction() {
        $request = $this->getRequest();
        $user = $request->getParam('UserName', null);
        $techID = $request->getParam('TechID', null);
		if (!$user) $this->_helper->json(false);
        $result = !Core_Tech::userExists($user, NULL, false) || (!empty($techID) && Core_Tech::userExists($user, $techID, false));
        $this->_helper->json($result);
    }

    public function techPhoneNotTakenAction() {
        $request = $this->getRequest();
        $techID = $request->getParam('TechID', null);
        $phone = $request->getParam('PrimaryPhone', null);
        if (empty($phone))
            $phone = $request->getParam('SecondaryPhone', null);
		if (empty($phone)) $this->_helper->json(false);
        $result = !Core_Tech::phoneExists($phone, $techID);
        $this->_helper->json($result);
    }
	public function approvephotoAction() {
        $request = $this->getRequest();
        $params = $request->getParams();

        $auth = $this->authAdmin();
        $id = $params['techid'];

        $success = 'error';
        if (!$auth)
            return $this->_helper->json(array('success' => $success));

        if (!empty($params['BG_Test_ResultsBy_Lite'])) {
            $params['BG_Test_ResultsBy_Lite'] = $this->_login;
        }
        if (!empty($params['BG_Test_ResultsBy_Full'])) {
            $params['BG_Test_ResultsBy_Full'] = $this->_login;
        }

        if (!empty($params['FLSCSP_RecBy'])) {
            $params['FLSCSP_RecBy'] = $this->_login;
        }
        try {
             $Core_Api_TechClass =  new  Core_Api_TechClass;
             $Core_Api_TechClass->approvePhoto($id);   
             $success = "success";
        } catch (Exception $e) {
            $success = "error";
        }


        $this->_helper->json(array('success' => $success));
    }
    
    private function isDateValid($str)
    {
          $stamp = strtotime($str);
          if (!is_numeric($stamp))
             return false;
         
          //checkdate(month, day, year)
          if ( checkdate(date('m', $stamp), date('d', $stamp), date('Y', $stamp)) )
          {
             return true;
          }
          return false;
    } 
    public function countryRequireZipAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $params = $this->getRequest()->getParams();

        $return = array('success' => false);

        if(isset($params["countrycode"]))
        {
            $Country = Core_Country::getCountryInfoByCode($params["countrycode"]);
            if(!empty($Country))
            {
                if($Country["ZipRequired"] == "1")
                {
                    $return = array('success' => true);
                } 
            }
        }
        
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                ->setBody($return);
    }
    
    //--- 360
    public function updateauditAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $params = $this->getRequest()->getParams();
        $winNum = $params['WIN_NUM'];
        $auditNeeded = $params['AuditNeeded'];
        $auditComplete = $params['AuditComplete'];
        
        /* init progress bar */
        $progress = Core_ProgressBar::getInstance()->setUserLogin($this->_login);
        $data = array('percent' => 0, 'label' => 'Beginning...', 'done' => false);
        $progress->write('updateaudit', json_encode($data));
        /* -- init progress bar -- */
        
        //--- save audit
        $api = new Core_Api_Class;
        $apiResponse= $api->saveWorkOrderAuditData( 
                        $winNum,
                        isset($auditNeeded),
                        $this->_login,
                        date("Y-m-d H:i:s"),
                        isset($auditComplete),
                        $this->_login,
                        date("Y-m-d H:i:s")                    
        );
        
        $jsonContext = json_encode(array('error' => 0));
        
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($jsonContext), true);
        $response->setBody($jsonContext);
        
    }
    function ggeTechEvaluationAllowAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $Core_Api_TechClass = new Core_Api_TechClass;
        $params = $this->getRequest()->getParams();
        $Core_Api_TechClass->allowedAnotherAttemptForGGETest($params["techID"]);
        $Core_Api_TechClass->saveSecondAttemptScoreOrStatus($params["techID"],"");

}
    public function makeAWithdrawAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();
        $return = array('success' => 0);

        $winNum = $request->getParam('win', 0) + 0;
        $bidAmount = $request->getParam('amount', 0) + 0;
        $comments = $request->getParam('comments', NULL);
        $agreewithdraw = $request->getParam('agreewithdraw');
        $techApi = new Core_Api_TechClass();

        
        if ($agreewithdraw == 'true')
        {
            $apiResponse = $techApi->createWithdraw($this->_login, $this->_password, $winNum, $bidAmount, $comments);
        } else
        {
            $apiResponse = $techApi->updateBid($this->_login, $this->_password, $winNum, $bidAmount, $comments);
}

        if ($apiResponse->success)
        {
            $return['success'] = 1;
        } else
        {
            foreach ($apiResponse->errors as $e) $return['errors'][]=$e->message;
        }
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                ->setBody($return);
        
    }
    public function getTechidFlsAction() {
        $request = $this->getRequest();
        $params = $request->getParams();
        $id = $params['techid'];
        $tech = new API_Tech();
        $TechFLS = "";
        if(!empty($id)){
            $result = $tech->doTechLookup("TechID = '$id' AND Deactivated = '0'");
            $TechFLS = $tech->FLSID;
}
        $this->_helper->json(array('techflsid' => $TechFLS));
    }
    function saveCommentLangugeSkillAction(){
        $params = $this->getRequest()->getParams();
        $id = $params['techid'];
        $changeTextCer = $params['changeTextCer'];
        $states = $params['states'];
        if($states == "1"){
            $_states = "competent";
        }
        else{
            $_states = "fluent";
        }
        $return["success"] = 0;
        $comment = $params['comment'];
        // save skill
        $Core_Api_Class = new Core_Api_Class();
        $Core_Api_Class->saveLanguageSkils($id,$params['languegaSkills']);
        if(!empty($id) && $changeTextCer == "1"){
            $param["certifications"][0] = 39;
            $param['certificationsStatus_39'] = 1;
            $param['certificationsDate_39'] = date("m/d/Y");
            $param['techID'] = $id;
            Core_Tech::updateCerts($param);
            // delete 40
            $param["certifications"][0] = 40;
            $param['certificationsStatus_40'] = 0;
            $param['certificationsDate_40'] = date("m/d/Y");
            $param['techID'] = $id;
            Core_Tech::updateCerts($param);
            $return["success"] = 1;
        }
        // send email
        $techInfo = Core_Tech::getProfile($id, true, API_Tech::MODE_TECH, "");
        $fname = $techInfo["Firstname"];
        $lname = $techInfo["Lastname"];
        $toEmail = "asl@fieldsolutions.com"; 
        $fromName = "FieldSolutions";
        $fromEmail = "no-replies@fieldsolutions.com";

        $subject = "Tech Requesting Sign Language Fluency Verification  FS-Tech ID# $id - $fname $lname";
        $body = "FS-Tech ID# $id $fname $lname states that they are $_states at American Sign Language.

                Comments: $comment
                Thanks,
                Your FieldSolutions Team
                ";
        $mail = new Core_Mail();
        $mail->setBodyText($body);
        $mail->setFromName($fromName);
        $mail->setFromEmail($fromEmail);
        $mail->setToEmail($toEmail);
        $mail->setSubject($subject);
        $mail->send();
        $this->_helper->json($return);
    }
 
    public function getServerDateCstAction() {
        $serverDateCST = date("Y-m-d H:i:s");
        $this->_helper->json(array('ServerDateCST' => $serverDateCST));
    }
    
    public function apigetwosAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $request = $this->getRequest();
        
        $fromdate = $request->getParam('fromdate',"");
        $todate = $request->getParam('todate',"");
        $distance = $request->getParam('distance',"");
        $zipcode = $request->getParam('zipcode',"");
        
        $errs = array();
        $return="";
        
        if($fromdate == "")
        {
            $errs[] = array("err"=>"From date cannot be empty.");
}
        else
        {
            try
            {
                $fromdatetime = strtotime($fromdate);
                if(!$fromdatetime)
                {
                    $errs[] = array("err"=>"From date format must be YYYY-mm-dd.");
                }
            }
            catch(Exception $e)
            {
                $errs[] = array("err"=>"From date format must be YYYY-mm-dd.");
            }
        }
        
        if($todate == "")
        {
            $errs[] = array("err"=>"To date cannot be empty.");
        }
        else
        {
            try
            {
                $todatetime = strtotime($todate);
                if(!$todatetime)
                {
                    $errs[] = array("err"=>"To date format must be YYYY-mm-dd.");
                }
            }
            catch(Exception $e)
            {
                $errs[] = array("err"=>"To date format must be YYYY-mm-dd.");
            }
        }
        
        if($distance == "")
        {
            $errs[] = array("err"=>"Distance cannot be empty.");
        }
        else
        {
            if(!is_numeric($distance))
            {
               $errs[] = array("err"=>"Distance must be numeric."); 
            }
            else
            {
                if($distance<=0)
                {
                    $errs[] = array("err"=>"Distance must be greater than zero."); 
                }
            }
        }
        
        if($zipcode == "")
        {
            $errs[] = array("err"=>"Zip code cannot be empty.");
        }
        else
        {
            if(strlen($zipcode)!=5)
            {
                $errs[] = array("err"=>"Zip code incorrect format.");
            }
        }
        
        if(empty($errs))
        {
            $fromdatetime = strtotime($fromdate);
            $fromdate = date("Y-m-d",$fromdatetime);
            $$todatetime = strtotime($todate);
            $todate = date("Y-m-d",$$todatetime);
            $apiWos = new Core_Api_WosClass();
            $woslist = $apiWos->getAPIWOS($fromdate,$todate,$distance,$zipcode);
            
            if(!empty($woslist))
            {
                foreach($woslist as $wo)
                {
                    $return.="<wo>
                            <ObservationEligible>".($wo["AllowDeVryInternstoObserveWork"]==1?"Yes":"No")."</ObservationEligible>
                            <WIN>".$wo["WIN_NUM"]."</WIN>
                            <WorkOrderStatus>".$wo["Status"]."</WorkOrderStatus>
                            <ProjectName>".htmlspecialchars($wo["Project_Name"], ENT_QUOTES)."</ProjectName>
                            <WorkOrderHeadline>".htmlspecialchars($wo["Headline"], ENT_QUOTES)."</WorkOrderHeadline>
                            <SkillCategory>".htmlspecialchars($wo["WO_Category"], ENT_QUOTES)."</SkillCategory>
                            <StartType>".($wo["StartTypeID"]==1?"Firm Start Time":"Start Time Window")."</StartType>
                            <StartDate>".$wo["StartDate"]."</StartDate>
                            <StartTime>".$wo["StartTime"]."</StartTime>
                            <EndDate>".$wo["EndDate"]."</EndDate>
                            <EndTime>".$wo["EndTime"]."</EndTime>
                            <EstimatedDurationOnsite>".$wo["EstimatedDurationOnsite"]."</EstimatedDurationOnsite>
                            <SiteAddress>".htmlspecialchars($wo["Address"], ENT_QUOTES)."</SiteAddress>
                            <City>".htmlspecialchars($wo["City"], ENT_QUOTES)."</City>
                            <State>".htmlspecialchars($wo["State"], ENT_QUOTES)."</State>
                            <ZipCode>".$wo["Zipcode"]."</ZipCode>
                            <AssignedTechnicianID>".$wo["Tech_ID"]."</AssignedTechnicianID>
                            <AssignedTechnicianFirstName>".htmlspecialchars($wo["Tech_FName"], ENT_QUOTES)."</AssignedTechnicianFirstName>
                            <AssignedTechnicianLastName>".htmlspecialchars($wo["Tech_LName"], ENT_QUOTES)."</AssignedTechnicianLastName>
                            <AssignedTechnicianEmailAddress>".htmlspecialchars($wo["TechEmail"], ENT_QUOTES)."</AssignedTechnicianEmailAddress>
                            <AssignedTechnicianPhone>".$wo["TechPhone"]."</AssignedTechnicianPhone>
                            <WorkDescription>".htmlspecialchars($wo["Description"], ENT_QUOTES)."</WorkDescription>
                            <TechRequirementsandRequiredTools>".htmlspecialchars($wo["Requirements"], ENT_QUOTES)."</TechRequirementsandRequiredTools>
                            <SpecialInstructions>".htmlspecialchars($wo["SpecialInstructions"], ENT_QUOTES)."</SpecialInstructions>
                        </wo>";
                }
                $return="
                <result>
                    <wos>".$return."</wos>
                </result>";
            }
            else
            {
                $return="<result>no result</result>";
            }
        }
        else
        {
            $return="
            <result>
                <errors>";
            $i=0;
            foreach($errs as $err)
            {
                $return .="<error>". $err['err']."</error>";
            }
                    
                $return .="</errors></result>";
        }
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        //$return = json_encode($return);
        $response->setHeader('Content-type', 'application/xml');
        $response->setHeader('Content-Length', strlen($return), true)
                ->setBody($return);
    }
    
    //end 729
    //13819
    public function fileWosAllDocumentsDownloadAction()
    {
        ini_set('memory_limit', '512M');
        if (empty($this->_login) || empty($this->_password)) {
            // no authentification
            throw new Core_Auth_Exception();
        }
        
        $authData = array('login'=>$this->_login . '|pmContext=' . $this->_companyId, 'password'=>$this->_password);
        $user = new Core_Api_User();
        $user->checkAuthentication($authData);

        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();    
        $win = $request->getParam('win', null);
        $woid = $request->getParam('woid', null);
       
        if (!empty($win))
        {
            $files = new Core_Files();
            $filesList = $files->getTechDocList($win);
            $numberFile=0;
            if (!empty($filesList)){                
                $numberFile=count($filesList);
            }
            
            $response = $this->getResponse();
            $response->clearHeaders();
            $response->clearBody();
            $mimeType = 'application/octet-stream';
            if($numberFile==1)
            {
                $File=null;
                $filename="";

                foreach ($filesList as $file)
                {
                    $filename=$file['path'];
                    $filename= ltrim($filename, '/');
                    if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)) {
                            $filename = rawurlencode($file);
                    }
                    $File=$file;
                }
                
                $response->setHeader("Content-Type", "content=" . $mimeType . "; charset=utf-8", true)
                        ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
                        ->setHeader("Content-Disposition", "attachment; filename=\"$filename\"", true)
                        ->setHeader('Last-Modified', date('D, d M Y H:i:s T'), true)
                        ->setHeader('Content-Length', strlen($file), true)
                        ->setBody(API_WorkOrder::getFile($File['path']));
            }
            else if($numberFile>1)
            {
                $filename='TechDocs_WIN'.$win.'_ClientID'.$woid.'_'.date('Ymd').'.zip';
                $downloadFolder = realpath(APPLICATION_PATH . '/../../htdocs/clients/downloadtmp');

                if (!is_dir($downloadFolder . '/'.$this->_login.'/')) {
                    mkdir($downloadFolder . '/'.$this->_login.'/');
                }
                
                $filePath = $downloadFolder . '/'.$this->_login.'/' . $filename;
                
                unlink($filePath);
                //zip file
                $zip =new ZipArchive;               
              
                if ($zip->open($filePath, ZIPARCHIVE::CREATE) === true) {
                    foreach ($filesList as $file)
                    {
                        $filenameFileAdd=$file['path'];
                        $filenameFileAdd= ltrim($filenameFileAdd, '/');
                        $fileOpenWOS=API_WorkOrder::getFile($filenameFileAdd); 
                        $zip->addFromString($filenameFileAdd, $fileOpenWOS);
                    }    
                }
                    
                try{
                    $zip->close();                        
                    header("Content-Type: application/zip");  
                    header("Content-Length: " . filesize($filePath));
                    header("Content-Disposition: attachment; filename=\"".$filename."\""); 
                    readfile($filePath);
                }
                catch (Exception $e)
                {
                }
            }          
        }
    }
    
    //-------------------------------------------------------------------------------------------------------
    public function fileAllWosAllDocumentsDownloadAction()
    {
        ini_set('memory_limit', '812M');

        if (empty($this->_login) || empty($this->_password))
        {
            // no authentification
            throw new Core_Auth_Exception();
        }

        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        $win = $request->getParam('win', null);
        $arrWin = explode(",", $win);

        $woid = $request->getParam('woid', null);
        $arrwoid = explode(",", $woid);
        
        $issubfolder = $request->getParam('issub', null);
        $numberWin = count($arrWin);

        $authData = array('login' => $this->_login . '|pmContext=' . $this->_companyId, 'password' => $this->_password);
        $user = new Core_Api_User();
        $user->checkAuthentication($authData);


        $downloadFolder = realpath(APPLICATION_PATH . '/../../htdocs/clients/downloadtmp');
        if ($issubfolder=="true")
        {
            $filename = 'TechDocs_' . date('Ymd') . '.zip';
        } else
        {
            if ($numberWin == 1)
            {
                $filename = 'TechDocs_WIN' . $arrWin[0] . "_ClientID" . $arrwoid[0] . "_" . date('Ymd') . '.zip';
            } else
            {
                $filename = 'TechDocs_' . date('Ymd') . '.zip';
            }
        }
        
        if (!is_dir($downloadFolder . '/'.$this->_login.'/')) {
            mkdir($downloadFolder . '/'.$this->_login.'/');
        }
        $filePath = $downloadFolder . '/'.$this->_login.'/' . $filename;

        //zip file
        $zip = new ZipArchive();
        $files = new Core_Files();

        unlink($filePath);

        if ($zip->open($filePath, ZIPARCHIVE::CREATE) === true)
        {
            for ($i = 0; $i < $numberWin; $i++)
            {
                $winnum = $arrWin[$i];
                $clientid = $arrwoid[$i];
                $filesList = $files->getTechDocList($winnum);
                if (!empty($filesList))
                {
                    foreach ($filesList as $file)
                    {
                        $filenameFileAdd = $file['path'];
                        $filenameFileAdd = ltrim($filenameFileAdd, '/');
                        $fileOpenWOS = API_WorkOrder::getFile($filenameFileAdd);
                        if ($issubfolder=="true")
                        {
                            $subfoldername = "WIN" . $winnum . "_ClientID" . $clientid . "/";
                        } else
                        {
                            $subfoldername = "";
                        }
                        $zip->addFromString($subfoldername . $filenameFileAdd, $fileOpenWOS);
                    }
                }
            }

            try
            {
                $zip->close();
                header("Content-Type: application/zip");
                header("Content-Length: " . filesize($filePath));
                header("Content-Disposition: attachment; filename=\"" . $filename . "\"");
                readfile($filePath);
            } catch (Exception $e)
            {
                
            }
        }
    }
    //end 13819
}
