<?php
require_once 'API/WorkOrderFilter.php';
require_once 'Core/Api/Class.php';

class Dashboard_IndexController extends Core_Controller_Action
{
    const FIELDS = 'WIN_NUM,WO_ID,DateEntered,PO,ShowTechs,SourceByDate,ShortNotice,StartDate,StartTime,EndDate,EndTime,StartRange,Duration,Region,Route,Type_ID,Project_ID,WO_Category_ID,Headline,Site_Contact_Name,SiteName,SiteNumber,Project_Name,Address,City,State,Zipcode,SitePhone,SiteFax,SiteEmail,Update_Requested,Update_Reason,Description,Requirements,Tools,SpecialInstructions,Lead,Assist,SignOffSheet_Required,ProjectManagerName,ProjectManagerPhone,ProjectManagerEmail,ResourceCoordinatorName,ResourceCoordinatorPhone,ResourceCoordinatorEmail,EmergencyName,EmergencyPhone,EmergencyEmail,TechnicalSupportName,TechnicalSupportPhone,TechnicalSupportEmail,CheckInOutName,CheckInOutNumber,CheckInOutEmail,Tech_ID,Tech_FName,Tech_LName,NoShow_Tech,BackOut_Tech,FLS_ID,BU1FLSID,BU1Info,Backup_FLS_ID,Ship_Contact_Info,PayMax,Amount_Per,Qty_Devices,Tech_Bid_Amount,WorkOrderReviewed,TechCheckedIn_24hrs,CheckedIn,Checkedby,PreCall_1,PreCall_2,CheckInNotes,StoreNotified,Notifiedby,DateNotified,NotifyNotes,TechMarkedComplete,Date_In,Time_In,Date_Out,Time_Out,ContactHD,HDPOC,TechMiles,TechComments,AttachedFile,FLS_OOS,Unexpected_Steps,Unexpected_Desc,AskedBy,AskedBy_Name,Paperwork_Received,Incomplete_Paperwork,MissingComments,Site_Complete,CallClosed,PartsProcessed,ClientComments,Work_Out_of_Scope,OutofScope_Amount,OutofScope_Reason,TripCharge,MileageReimbursement,MaterialsReimbursement,Additional_Pay_Amount,Penalty_Reason,AbortFee,AbortFeeAmount,Add_Pay_AuthBy,Add_Pay_Reason,calculatedTechHrs,baseTechPay,PayAmount,Approved,TechPaid,DatePaid,Deactivated,DeactivationCode,Deactivated_Reason,General1,General2,General3,General4,General5,General6,General7,General8,General9,General10,FS_InternalComments,Username,Invoiced,Company_Name,Company_ID,TechEmail,TechPhone,TechPhoneExt,Date_Assigned,lastModBy,lastModDate,approvedBy,DateApproved,DeactivatedDate,DeactivatedBy,Latitude,Longitude,PricingRan,WO_Category,DateIncomplete,ReminderAcceptance,Reminder24Hr,Reminder1Hr,CheckInCall,CheckOutCall,ReminderNotMarkComplete,ReminderIncomplete,ReminderAll,SMSBlast,MinutesRemainPublished,MinimumSelfRating,MaximumAllowableDistance,isProjectAutoAssign,isWorkOrdersFirstBidder,P2TPreferredOnly,P2TAlreadyAssigned,SATRecommended,SATPerformance,ReturnPartCount,NewPartCount,Qty_Applicants,Status,DateInvoiced,Work_Out_of_Scope,OutOfScope_Reason,Status,PcntDeduct,PcntDeductPercent,Date_Completed,UpdatedPartCount,Qty_Visits,calculatedInvoice';
    
    private $_filter;
    private $_size;
    private $_page;
    private $_company;
    private $_tab;
	private $_visits;

    public function init()
    {
        parent::init();
        //  This is constroller can be used for AJAX only!
        $request = $this->getRequest();

        /* Check for ajax requests only, except 'csv' download */
        if ( !$request->isXmlHttpRequest() ) {
            if ( $this->getRequest()->getParam('download', false) !== 'csv' && $this->getRequest()->getParam('download', false) !== 'json' ) {
                $this->_redirect('/');
            }
        }
        /* Check for ajax requests only, except 'csv' download */

        $this->_company = ($request->getParam('company', NULL)!==NULL)?$request->getParam('company'):$this->_company;
        $s1=$request->getParam('sort1', NULL);
        $d1=$request->getParam('dir1', NULL);       
        if($s1=="timesinceworkdone")
        {
            if(strtoupper($d1) ==strtoupper("asc"))    
            {
                $d1="desc";
            }
            else
            {
                $d1="asc";
            }
        }
        $s2=$request->getParam('sort2', NULL);
        $d2=$request->getParam('dir2', NULL);       
        if($s1=="timesinceworkdone")
        {
            if(strtoupper($d2) ==strtoupper("asc"))    
            {
                $d2="desc";
            }
            else
            {
                $d2="asc";
            }
        }
        
        $s3=$request->getParam('sort3', NULL);
        $d3=$request->getParam('dir3', NULL);       
        if($s1=="timesinceworkdone")
        {
            if(strtoupper($d3) ==strtoupper("asc"))    
            {
                $d3="desc";
            }
            else
            {
                $d3="asc";
            }
        }
        
        $this->_sortStr = $this->_assembleSortStr(array(
            's1' => $s1, 'd1' => $d1,
            's2' => $s2, 'd2' => $d2,
            's3' => $s3, 'd3' => $d3
        ));

        
        /* Get Method when user download a CSV file */
        if ( $request->isGet() ) {
            if ( NULL !== ($projects = $request->getParam('projects', NULL)) ) {
                if ( is_string($projects) && strpos($projects, '|', 1) ) {
                    $projects = explode('|', $projects);
                    $request->setParam('projects', $projects);
                }
            }
            if ( NULL !== ($startdate = $request->getParam('start_date', NULL)) ) {
                $request->setParam('start_date', str_replace('_', '/', $startdate));
            }
            if ( NULL !== ($enddate = $request->getParam('end_date', NULL)) ) {
                $request->setParam('end_date', str_replace('_', '/', $enddate));
            }
        }
        /* Get Method when user download a CSV file */

        $projects           = $request->getParam('projects', NULL);
        $show_to_tech       = $request->getParam('show_to_tech', NULL);
        $update_req         = $request->getParam('update_req', NULL);
        $store_notify       = $request->getParam('store_notify', NULL);
        $checked_in         = $request->getParam('checked_in', NULL);
        $reviewed_wo        = $request->getParam('reviewed_wo', NULL);
        $tech_confirmed     = $request->getParam('tech_confirmed', NULL);
        $tech_complete      = $request->getParam('tech_complete', NULL);
        $site_complete      = $request->getParam('site_complete', NULL);
        $approved           = $request->getParam('approved', NULL);
        $out_of_scope       = $request->getParam('out_of_scope', NULL);
        $tech_paid          = $request->getParam('tech_paid', NULL);
        $show_sourced       = $request->getParam('show_sourced', NULL);
        $show_not_sourced   = $request->getParam('show_not_sourced', NULL);
        $paper_received     = $request->getParam('paper_received', NULL);
        $lead               = $request->getParam('lead', NULL);
        $assist             = $request->getParam('assist', NULL);
        $deactivated        = $request->getParam('deactivated', NULL);
        $wins               = $request->getParam('win', NULL); //360
        $audit              = $request->getParam('Audit', NULL); //360
        $auditNeeded        = $request->getParam('AuditNeeded', NULL); //360
        $auditComplete      = $request->getParam('AuditComplete', NULL); //360
        $owner              = $request->getParam('Owner', NULL);
        $dateRange              = $request->getParam('dateRange', NULL);
        $SiteContactBackedOutChecked = $request->getParam('SiteContactBackedOutChecked', NULL);//732


        //---
        if ( $show_not_sourced === NULL && $show_sourced === NULL )
            $sourced = NULL;
        else if ( $show_sourced + 0 > 0 && $show_not_sourced + 0 > 0 )
            $sourced = NULL;
        else if ( $show_not_sourced + 0 > 0  )
            $sourced = 'False';
        else
            $sourced = 'True';

        $this->_filters = new API_WorkOrderFilter;
        $this->_filters->Sourced                = $sourced;
        //$this->_filters->TB_UNID                = $request->getParam('win', NULL);
        $this->_filters->TB_UNID                = $wins;
        $this->_filters->Audit                  = $audit;
        $this->_filters->AuditNeeded            = $auditNeeded;        
        $this->_filters->AuditComplete          = $auditComplete;        
        $this->_filters->Owner                  = $owner;
        $this->_filters->Company_ID             = $this->_company;
        $this->_filters->Call_Type              = $request->getParam('call_type', NULL);
        $this->_filters->Project_ID             = ( is_array($projects) ) ? ( (count($projects) > 1) ? $projects : $projects[0] ) : $projects;
        $this->_filters->dateRange                  = $dateRange;
        $this->_filters->StartDateFrom          = $request->getParam('start_date', NULL);
        $this->_filters->StartDateTo            = $request->getParam('end_date', NULL);
        $this->_filters->WO_ID                  = $request->getParam('client_wo', NULL);
        $this->_filters->PO                     = $request->getParam('po', NULL);
        $this->_filters->ShowTechs              = ( NULL === $show_to_tech) ? NULL : ( ( $show_to_tech + 0 ) ? true : false );
        $this->_filters->Region                 = $request->getParam('region', NULL);
        $this->_filters->Route                 = $request->getParam('route', NULL);
        $this->_filters->SiteName               = $request->getParam('site_name', NULL);
        $this->_filters->SiteNumber             = $request->getParam('site_number', NULL);
        $this->_filters->City                   = $request->getParam('city', NULL);
        $this->_filters->State                  = $request->getParam('state', NULL);
        $this->_filters->Country                  = $request->getParam('country', NULL);
        $this->_filters->AbortFee                  = $request->getParam('AbortFee', NULL);//622
        $this->_filters->Zipcode                = $request->getParam('zip', NULL);
        $this->_filters->Update_Requested       = ( NULL === $update_req ) ? NULL : ( ( $update_req + 0 ) ? true : false );
        $this->_filters->CheckedIn              = ( NULL === $checked_in ) ? NULL : ( ( $checked_in + 0 ) ? true : false );
        $this->_filters->WorkOrderReviewed      = ( NULL === $reviewed_wo ) ? NULL : ( ( $reviewed_wo + 0 ) ? true : false );
        $this->_filters->TechCheckedIn_24hrs    = ( NULL === $tech_confirmed ) ? NULL : ( ( $tech_confirmed + 0 ) ? true : false );
        //732
        if(isset($SiteContactBackedOutChecked))
        {
            $this->_filters->SiteContactBackedOutChecked    = $SiteContactBackedOutChecked;
        }
        //end 732
        $this->_filters->TechMarkedComplete     = ( NULL === $tech_complete ) ? NULL : ( ( $tech_complete + 0 ) ? true : false );
        $this->_filters->Site_Complete          = ( NULL === $site_complete ) ? NULL : ( ( $site_complete + 0 ) ? true : false );
        $this->_filters->Approved               = ( NULL === $approved ) ? NULL : ( ( $approved + 0 ) ? true : false );
        $this->_filters->Additional_Pay_Amount  = $request->getParam('extra_pay', NULL);
        $this->_filters->Work_Out_of_Scope      = ( NULL === $out_of_scope ) ? NULL : ( ( $out_of_scope + 0 ) ? true : false );
        $this->_filters->TechPaid               = ( NULL === $tech_paid ) ? NULL : ( ( $tech_paid + 0 ) ? true : false );
        $this->_filters->Tech_FName             = $request->getParam('tech_fname', NULL);
        $this->_filters->Tech_LName             = $request->getParam('tech_lname', NULL);
        $this->_filters->Deactivated            = ( NULL === $deactivated ) ? NULL : ( ( $deactivated + 0 ) ? true : false );
        $this->_filters->StoreNotified          = ( NULL === $store_notify) ? NULL : ( ( $store_notify + 0 ) ? true : false );
        $this->_filters->Paperwork_Received     = ( NULL === $paper_received ) ? NULL : ( ( $paper_received + 0 ) ? true : false );
        $this->_filters->Lead                   = ( NULL === $lead ) ? NULL : ( ( $lead + 0 ) ? true : false );
        $this->_filters->Assist                 = ( NULL === $assist ) ? NULL : ( ( $assist + 0 ) ? true : false );
        //13892
        $this->_filters->StandardOffset = $request->getParam('StandardOffset', NULL);
        //end 13892
        $techPaperwork = $request->getParam('techPaperwork', NULL);
        if(!empty($techPaperwork)){
            if($request->getParam('techPaperwork', NULL)+0 == 1){
                $this->_filters->Paperwork_Received = true;
            }
            if($request->getParam('techPaperwork', NULL)+0 == 2){
                $this->_filters->Incomplete_Paperwork = true;
            }
        }
        if ( strtolower(trim($this->_company)) === 'fls' ) {
            $tech_id = $request->getParam('tech', NULL);
            $fls_id  = $request->getParam('fls_id', NULL);
            if ( !empty($tech_id) && empty($fls_id) )
                $this->_filters->FLS_ID         = $tech_id;
            else if ( !empty($fls_id) )
                $this->_filters->FLS_ID         = $fls_id;

            $short_notice = $request->getParam('short_notice', NULL);
            //017
            //$this->_filters->Short_Notice       = ( NULL === $short_notice ) ? NULL : ( ( $short_notice + 0 ) ? true : false );
            $this->_filters->Short_Notice       = ( NULL === $short_notice ) ? NULL : ($short_notice==1?"true":"false");
            //end 017
        } else {
            $this->_filters->Tech               = $request->getParam('tech', NULL);
        }
		
		$user = new Core_Api_User();
		$user = $user->load(
			array(
				'login' => $this->_login,
				'password' => $this->_password
			)
		);
		$client_id = $user->getClientId();
		$myDashboardSettings = Core_DashboardSettings::getSettings($client_id);
		if (!empty($myDashboardSettings)) {
			$myDashboardSettings = $myDashboardSettings['filter'];
			if ($myDashboardSettings == Core_DashboardSettings::FILTER_WORK_ORDER)
				$this->_filters->DisplayByClientUserAction = $client_id;
		}
                $this->view->client_id = $client_id;
        $this->_size    = ($request->getParam('size', empty($_COOKIE['page_size']) ? 25 : $_COOKIE['page_size'] )) + 0;
        $this->_size    = ( $this->_size <= 10 ) ? 10 : ( ( $this->_size >= 100 ) ? 100 : $this->_size );
        
        if ($request->getParam('size', null) != null) {
            $domain = '.' . preg_replace('/^www\./', '', $_SERVER['HTTP_HOST']);
            setcookie("page_size", $this->_size, time() + 3600 * 24 * 30, "/", $domain);
        }
        
        $this->_page    = ($request->getParam('page', 1)) + 0;
        $this->_page    = ( $this->_page < 1 ) ? 1 : $this->_page;

        $this->view->pageCurrent = $this->_page;
        $this->view->pageSize = $this->_size;

        $this->view->files        = new Core_Files();
        $this->view->company      = $this->_company;
        $this->view->tab          = $this->getRequest()->getActionName();
        $this->view->isFLSManager = ((strtolower($this->_userType)==='manager' || strtolower($this->_userType)==='install desk') && $this->_company === 'FLS');
        $this->view->dbVersion    = strtolower($request->getParam('version', 'full'));
        
        $auth = new Zend_Session_Namespace('Auth_User');
        $auth->dbVersion = $this->view->dbVersion;
        $this->keepSortAndFillter();
        $user = new Core_Api_User();
        $clientInfoLogin = $user->loadWithExt(
                array(
                    'login' => $this->_login,
                    'password' => $this->_password
                )
        );
        $apiClient = new Core_Api_Class();
        $isGPM = $apiClient->isGPM($clientInfoLogin->ClientID);
        $this->view->isGPM = $isGPM; 
        $this->view->login = $this->_login;
        $this->view->password = $this->_password;
        $WoTagClass = new Core_Api_WoTagClass();
        if(trim($request->getParam('site_status', "")) != ""){
            $aRWo = $WoTagClass->getWinNumArray_BySiteStatus($this->view->company,$request->getParam('site_status', ""));
            if(!empty($aRWo)){
                $this->_filters->TB_UNID = implode(",", $aRWo);
    }
            else{
                $this->_filters->TB_UNID = -232;
            }
        }

		$this->_visits = new Core_Api_WosClass();
        //14139        
        $this->_filters->WO_ID_Mode = 1;                    
	}

    public function createdAction()
    {
        $request = $this->getRequest();
        $search = new Core_Api_Class;
        $this->_filters->WO_State = WO_STATE_CREATED;

        /** Save workorders into CSV file **/
        if ( $request->getParam('download', false) === 'csv' ) {
            try {
                $this->_helper->viewRenderer->setNoRender(true);
				$csvAdapter = new Core_CSV_Adapter_Client(
                    $this->_login.'|pmContext='.$this->_company,
                    $this->_password,
                    $this->_filters,
                    self::FIELDS,
                    $this->_sortStr
                );
				
                $csvExport = new Core_CSV_Builder('client', 'created', $csvAdapter, defined('FLSVersion'), $this->view->isFLSManager);
                return $this->_outputCsv($csvExport);
            } catch (Core_CSV_Exception $e) {
                print 'error occurred';
                return false;
            }
        }
        /**  End section: Save created into CSV file  **/

        $wo = $search->getWorkOrders(
            $this->_login.'|pmContext='.$this->_company,
            $this->_password,
            self::FIELDS,
            '',
            '',
            '',
            $this->_filters,
            ($this->_page-1)*$this->_size,
            $this->_size,
            $countRows,
            $this->_sortStr,
            true,
            true
        );

		$win_nums = array();
		if ($wo->success) {
			foreach($wo->data as $_wo) {
				$win_nums[] = $_wo->WIN_NUM;
			}
		}

		$win_nums = array_unique ($win_nums);

        //13819
        $this->view->winNumsHaveFiles = $this->getWinNumsHaveFiles($win_nums);
        //end 13819

		if( $request->getParam("download",false) === 'json'){
        	$wins = array();
        	foreach($wo->data as $w){
        		$wins[] = $w->WIN_NUM;
        	}
        	$response = $this->getResponse();
        	$response->clearHeaders()->clearBody();
        	$return = json_encode($wins);

        	echo $return;
        	$this->_helper->viewRenderer->setNoRender(true);
        }else{
	        if ( $wo->success ) {
	            $paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($wo->data,$countRows));
	            $paginator->setItemCountPerPage($this->_size);
	            $paginator->setCurrentPageNumber($this->_page);
	            $this->view->paginator = $paginator;
				$this->view->wo_visits = $this->_visits->getNumOfVisits ($win_nums);
				$this->view->qa_unseencount = $this->_visits->getNum_ofUnseen_QA_fromTech ($this->_company, $win_nums);
	        }
	        $this->view->success    = $wo->success;
	        $this->view->errors     = $wo->errors;
        }
    }

    public function publishedAction()
    {
        $request = $this->getRequest();
        $this->_filters->WO_State = WO_STATE_PUBLISHED;

        $search = new Core_Api_Class;

        // Save workorders into CSV file
        if ( $request->getParam('download', false) === 'csv' ) {
            try {
                $csvAdapter = new Core_CSV_Adapter_Client(
                    $this->_login.'|pmContext='.$this->_company,
                    $this->_password,
                    $this->_filters,
                    self::FIELDS,
                    $this->_sortStr
                );
                $csvExport = new Core_CSV_Builder('client', 'published', $csvAdapter, defined('FLSVersion'), $this->view->isFLSManager);
                return $this->_outputCsv($csvExport);
            } catch (Core_CSV_Exception $e) {
                print 'error occurred';
                return false;
            }
        }
        //  End section: Save published into CSV file

        //--- add special filter
        $techBid=$_POST['TechBid'];
        if(!empty($techBid) && $techBid>0)
        {
            $api = new Core_Api_TechClass;         
            $companyId = $this->_filters->Company_ID;
            $projectId = $this->_filters->Project_ID;
            $techId = $techBid;
            $WOs = $api->getWOs_BidsByTechAndCompanyAndProject($techId,$companyId,$projectId);
            if(!empty($WOs))
            {
                $this->_filters->TB_UNID = $WOs;
                $this->view->techId = $techId;
				$db = Core_Database::getInstance();
				$select = $db->select();
				$select->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('WorkOrderID','BidAmount'))
					->where('TechID = ?', $techId)
					->where("WorkOrderID IN ($WOs)");
				$this->view->WO_Tech_Bid = $db->fetchPairs($select);
            }
            else
            {
                $this->_filters->TB_UNID = -123;
            }
            //echo("techBid: $techBid");
            //echo("<pre>");
            //print_r($this->_filters);die();
        }
        //echo("techBid: $techBid");
        //echo("<pre>");
        //print_r($this->_filters);die();
        
       
        //--- get workorders 
        $wo = $search->getWorkOrders(
            $this->_login.'|pmContext='.$this->_company,
            $this->_password,
            self::FIELDS,
            '',
            '',
            '',
            $this->_filters,
            ($this->_page-1)*$this->_size,
            $this->_size,
            $countRows,
            $this->_sortStr,
            true,
            true
        );

		$win_nums = array();
		if ($wo->success) {
			foreach($wo->data as $_wo) {
				$win_nums[] = $_wo->WIN_NUM;
			}
		}

		$win_nums = array_unique ($win_nums);

        //13819
        $this->view->winNumsHaveFiles = $this->getWinNumsHaveFiles($win_nums);
        //end 13819

   		 if( $request->getParam("download",false) === 'json'){
        	$wins = array();
        	foreach($wo->data as $w){
        		$wins[] = $w->WIN_NUM;
        	}
        	$response = $this->getResponse();
        	$response->clearHeaders()->clearBody();
        	$return = json_encode($wins);

        	echo $return;
        	$this->_helper->viewRenderer->setNoRender(true);
        }else{
        
	       if ( $wo->success ) {
	            $paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($wo->data,$countRows));
	            $paginator->setItemCountPerPage($this->_size);
	            $paginator->setCurrentPageNumber($this->_page);
	            $this->pullApplicantsCount($paginator);
	            $this->view->paginator = $paginator;
				$this->view->wo_visits = $this->_visits->getNumOfVisits ($win_nums);
				//$this->view->totals = $this->_visits->getTotal_ApReThFS ($win_nums);
				$this->view->qa_unseencount = $this->_visits->getNum_ofUnseen_QA_fromTech ($this->_company, $win_nums);
       		}
        	$this->view->success    = $wo->success;
        	$this->view->errors     = $wo->errors;
                $this->view->deactivaAll    = $request->getParam('deactivaAll', false);
        }
    }

    public function assignedAction()
    {
        $request = $this->getRequest();
        $this->_filters->WO_State = WO_STATE_ASSIGNED;

        $search = new Core_Api_Class;

        // Save workorders into CSV file
        if ( $request->getParam('download', false) === 'csv' ) {
            try {
                $csvAdapter = new Core_CSV_Adapter_Client(
                    $this->_login.'|pmContext='.$this->_company,
                    $this->_password,
                    $this->_filters,
                    self::FIELDS,
                    $this->_sortStr
                );

                $csvExport = new Core_CSV_Builder('client', 'assigned', $csvAdapter, defined('FLSVersion'), $this->view->isFLSManager, true);
                return $this->_outputCsv($csvExport);
            } catch (Core_CSV_Exception $e) {
                print 'error occurred';
                return false;
            }
        }
        //  End section: Save assigned into CSV file

        $wo = $search->getWorkOrders(
            $this->_login.'|pmContext='.$this->_company,
            $this->_password,
            self::FIELDS,
            '',
            '',
            '',
            $this->_filters,
            ($this->_page-1)*$this->_size,
            $this->_size,
            $countRows,
            $this->_sortStr,
            true,
            true
        );

		$techs = array();
		$win_nums = array();
		if ($wo->success) {
			foreach($wo->data as $_wo) {
				$techs[] = (defined('FLSVersion')) ? $_wo->FLS_ID : $_wo->Tech_ID;
				$win_nums[] = $_wo->WIN_NUM;
			}
		}

		$win_nums = array_unique ($win_nums);

        //13819
        $this->view->winNumsHaveFiles = $this->getWinNumsHaveFiles($win_nums);
        //end 13819

        $tech = new API_Tech;
        $techs = $tech->lookupID(array_unique($techs));
        
		if( $request->getParam("download",false) === 'json'){
        	$wins = array();
        	foreach($wo->data as $w){
        		$wins[] = $w->WIN_NUM;
        	}
        	$response = $this->getResponse();
        	$response->clearHeaders()->clearBody();
        	$return = json_encode($wins);

        	echo $return;
        	$this->_helper->viewRenderer->setNoRender(true);
        }else{
        
			if ( $wo->success ) {
				$paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($wo->data,$countRows));
				$paginator->setItemCountPerPage($this->_size);
				$paginator->setCurrentPageNumber($this->_page);
				$this->pullApplicantsCount($paginator);
				$this->pullWosTechCount($paginator,'assigned');
				$this->view->wo_visits = $this->_visits->getNumOfVisits ($win_nums);
				$this->view->qa_unseencount = $this->_visits->getNum_ofUnseen_QA_fromTech ($this->_company, $win_nums);

				$this->view->paginator = $paginator;
			}
	        $this->view->techs      = $techs;
	        $this->view->success    = $wo->success;
	        $this->view->errors     = $wo->errors;
        }
    }
    /**
     * workdoneAction
     *
     * Render data for 'workdone' tab
     *
     * @access public
     * @return void
     */
    public function workdoneAction()
    {
        $request = $this->getRequest();
        $this->_filters->WO_State = WO_STATE_WORK_DONE;

        $search = new Core_Api_Class;

        /** Save workorders into CSV file **/
        if ( $request->getParam('download', false) === 'csv' ) {
            try {
                $csvAdapter = new Core_CSV_Adapter_Client(
                    $this->_login.'|pmContext='.$this->_company,
                    $this->_password,
                    $this->_filters,
                    self::FIELDS,
                    $this->_sortStr
                );
                
                $csvExport = new Core_CSV_Builder('client', 'workdone', $csvAdapter, defined('FLSVersion'), $this->view->isFLSManager, true);
                return $this->_outputCsv($csvExport);
            } catch (Core_CSV_Exception $e) {
                print 'error occurred';
                return false;
            }
        }

        /**  End section: Save workdone into CSV file  **/
        $wo = $search->getWorkOrders(
            $this->_login.'|pmContext='.$this->_company,
            $this->_password,
            self::FIELDS,
            '',
            '',
            '',
            $this->_filters,
            ($this->_page-1)*$this->_size,
            $this->_size,
            $countRows,
            $this->_sortStr,
            true,
            true
        );

		$techs = array();
		$win_nums = array();
		if ($wo->success) {
			foreach($wo->data as $_wo) {
				$techs[] = (defined('FLSVersion')) ? $_wo->FLS_ID : $_wo->Tech_ID;
				$win_nums[] = $_wo->WIN_NUM;
			}
		}
        
		$win_nums = array_unique ($win_nums);

        //13819
        $this->view->winNumsHaveFiles = $this->getWinNumsHaveFiles($win_nums);
        //end 13819

        $tech = new API_Tech;
        $techs = $tech->lookupID(array_unique($techs));
        if( $request->getParam("download",false) === 'json'){
        	$wins = array();
        	foreach($wo->data as $w){
        		$wins[] = $w->WIN_NUM;
        	}
        	$response = $this->getResponse();
        	$response->clearHeaders()->clearBody();
        	$return = json_encode($wins);

        	echo $return;
        	$this->_helper->viewRenderer->setNoRender(true);
        }else{
	        if ( $wo->success ) {
				$last_view_dates = $this->_visits->getClient_LastViewQAWinDatetime ($this->_company, $win_nums);

	            $paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($wo->data,$countRows));
	            $paginator->setItemCountPerPage($this->_size);
	            $paginator->setCurrentPageNumber($this->_page);
                $this->pullWosTechCount($paginator,'workdone');
	            $this->view->paginator = $paginator;
				$this->view->wo_visits = $this->_visits->getNumOfVisits ($win_nums);
				$this->view->totals = $this->_visits->getTotal_ApReThFS ($win_nums);
				//$this->view->qa_notviewed is redundant, just check if $this->qa_unseencount is greater than zero.
				//$this->view->qa_notviewed = $this->_visits->HasQuestion_ClientNotViewYet ($this->_company, $win_nums);
				$this->view->qa_unseencount = $this->_visits->getNum_ofUnseen_QA_fromTech ($this->_company, $win_nums);
	        }
	        $this->view->techs      = $techs;
	        $this->view->success    = $wo->success;
	        $this->view->errors     = $wo->errors;
        }
    }

    public function incompleteAction()
    {
        $request = $this->getRequest();
        $this->_filters->WO_State = WO_STATE_INCOMPLETE;

        $search = new Core_Api_Class;

        /** Save workorders into CSV file **/
        if ( $request->getParam('download', false) === 'csv' ) {
            try {
                $csvAdapter = new Core_CSV_Adapter_Client(
                    $this->_login.'|pmContext='.$this->_company,
                    $this->_password,
                    $this->_filters,
                    self::FIELDS,
                    $this->_sortStr
                );

                $csvExport = new Core_CSV_Builder('client', 'incomplete', $csvAdapter, defined('FLSVersion'), $this->view->isFLSManager, true);
                return $this->_outputCsv($csvExport);
            } catch (Core_CSV_Exception $e) {
                print 'error occurred';
                return false;
            }
        }
        /**  End section: Save incomplete into CSV file  **/

        $wo = $search->getWorkOrders(
            $this->_login.'|pmContext='.$this->_company,
            $this->_password,
            self::FIELDS,
            '',
            '',
            '',
            $this->_filters,
            ($this->_page-1)*$this->_size,
            $this->_size,
            $countRows,
            $this->_sortStr,
            true,
            true
        );

		$techs = array();
		$win_nums = array();
		if ($wo->success) {
			foreach($wo->data as $_wo) {
				$techs[] = (defined('FLSVersion')) ? $_wo->FLS_ID : $_wo->Tech_ID;
				$win_nums[] = $_wo->WIN_NUM;
			}
		}

		$win_nums = array_unique ($win_nums);

        //13819
        $this->view->winNumsHaveFiles = $this->getWinNumsHaveFiles($win_nums);
        //end 13819

        $tech = new API_Tech;
        $techs = $tech->lookupID(array_unique($techs));
        
    if( $request->getParam("download",false) === 'json'){
        	$wins = array();
        	foreach($wo->data as $w){
        		$wins[] = $w->WIN_NUM;
        	}
        	$response = $this->getResponse();
        	$response->clearHeaders()->clearBody();
        	$return = json_encode($wins);

        	echo $return;
        	$this->_helper->viewRenderer->setNoRender(true);
        }else{
        	if ( $wo->success ) {
	            $paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($wo->data,$countRows));
	            $paginator->setItemCountPerPage($this->_size);
	            $paginator->setCurrentPageNumber($this->_page);
                $this->pullWosTechCount($paginator,'incomplete');
				$this->view->wo_visits = $this->_visits->getNumOfVisits ($win_nums);
				$this->view->totals = $this->_visits->getTotal_ApReThFS ($win_nums);
				$this->view->qa_unseencount = $this->_visits->getNum_ofUnseen_QA_fromTech ($this->_company, $win_nums);
	            $this->view->paginator = $paginator;
        	}
	        $this->view->techs      = $techs;
	        $this->view->success    = $wo->success;
	        $this->view->errors     = $wo->errors;
        }
    }

    public function completedAction()
    {
        $request = $this->getRequest();
        /* Completed state */
        $this->_filters->Approved          = 1;
        $this->_filters->Deactivated       = 0;
        /* Completed state */

        $search = new Core_Api_Class;

        /** Save workorders into CSV file **/
        if ( $request->getParam('download', false) === 'csv' ) {
            try {
                $csvAdapter = new Core_CSV_Adapter_Client(
                    $this->_login.'|pmContext='.$this->_company,
                    $this->_password,
                    $this->_filters,
                    self::FIELDS,
                    $this->_sortStr
                );

                $csvExport = new Core_CSV_Builder('client', 'completed', $csvAdapter, defined('FLSVersion'), $this->view->isFLSManager, true);
                return $this->_outputCsv($csvExport);
            } catch (Core_CSV_Exception $e) {
                print 'error occurred';
                return false;
            }
        }
        /**  End section: Save completed into CSV file  **/

        $wo = $search->getWorkOrders(
            $this->_login.'|pmContext='.$this->_company,
            $this->_password,
            self::FIELDS,
            '',
            '',
            '',
            $this->_filters,
            ($this->_page-1)*$this->_size,
            $this->_size,
            $countRows,
            $this->_sortStr,
            true,
            true,
			true
       );

		$techs = array();
		$win_nums = array();
		if ($wo->success) {
			foreach($wo->data as $_wo) {
				$techs[] = (defined('FLSVersion')) ? $_wo->FLS_ID : $_wo->Tech_ID;
				$win_nums[] = $_wo->WIN_NUM;
			}
		}

		$win_nums = array_unique ($win_nums);

        //13819
        $this->view->winNumsHaveFiles = $this->getWinNumsHaveFiles($win_nums);
        //end 13819

        $tech = new API_Tech;
        $techs = $tech->lookupID(array_unique($techs));
        
		if( $request->getParam("download",false) === 'json'){
        	$wins = array();
        	foreach($wo->data as $w){
        		$wins[] = $w->WIN_NUM;
        	}
        	$response = $this->getResponse();
        	$response->clearHeaders()->clearBody();
        	$return = json_encode($wins);

        	echo $return;
        	$this->_helper->viewRenderer->setNoRender(true);
        }else{
        	if ( $wo->success ) {
	            $paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($wo->data,$countRows));
	            $paginator->setItemCountPerPage($this->_size);
	            $paginator->setCurrentPageNumber($this->_page);
                $this->pullWosTechCount($paginator,'completed');
				$this->view->wo_visits = $this->_visits->getNumOfVisits ($win_nums);
				$this->view->totals = $this->_visits->getTotal_ApReThFS ($win_nums);
				$this->view->qa_unseencount = $this->_visits->getNum_ofUnseen_QA_fromTech ($this->_company, $win_nums);
	            $this->view->paginator = $paginator;
        	}
	        $this->view->techs      = $techs;
	        $this->view->success    = $wo->success;
	        $this->view->errors     = $wo->errors;
        }
    }

    public function allAction()
    {
    	$request = $this->getRequest();

        if ( !$request->getParam('full_search', false) ) {
            //  Do not use this filter for full search mode
            $this->_filters->WO_State = WO_STATE_ALL_ACTIVE;
        }
         //14139
        $woIdMode = trim($request->getParam('WO_ID_Mode', 0));      
        if (!empty($this->_filters->WO_ID) ) {
                $this->_filters->WO_ID_Mode =  $woIdMode;            
            }
          
        $search = new Core_Api_Class;

        // Save workorders into CSV file
        if ( $request->getParam('download', false) === 'csv' ) {
            try {
                $csvAdapter = new Core_CSV_Adapter_Client(
                    $this->_login.'|pmContext='.$this->_company,
                    $this->_password,
                    $this->_filters,
                    self::FIELDS,
                    $this->_sortStr
                );

                $csvExport = new Core_CSV_Builder('client', 'all', $csvAdapter, defined('FLSVersion'), $this->view->isFLSManager, true);
                return $this->_outputCsv($csvExport);
            } catch (Core_CSV_Exception $e) {
                print 'error occurred';
                return false;
            }
        }
        //  End section: Save all into CSV file

        $wo = $search->getWorkOrders(
            $this->_login.'|pmContext='.$this->_company,
            $this->_password,
            self::FIELDS,
            '',
            '',
            '',
            $this->_filters,
            ($this->_page-1)*$this->_size,
            $this->_size,
            $countRows,
            $this->_sortStr,
            true,
            true,
			true
        );

		$techs = array();
		$win_nums = array();
		if ($wo->success) {
			foreach($wo->data as $_wo) {
				$techs[] = (defined('FLSVersion')) ? $_wo->FLS_ID : $_wo->Tech_ID;
				$win_nums[] = $_wo->WIN_NUM;
			}
		}

		$win_nums = array_unique ($win_nums);

        //13819
        $this->view->winNumsHaveFiles = $this->getWinNumsHaveFiles($win_nums);
        //end 13819

        $tech = new API_Tech;
        $techs = $tech->lookupID(array_unique($techs));

        if( $request->getParam("download",false) === 'json'){
        	$wins = array();
        	foreach($wo->data as $w){
        		$wins[] = $w->WIN_NUM;
        	}
        	$response = $this->getResponse();
        	$response->clearHeaders()->clearBody();
        	$return = json_encode($wins);

        	echo $return;
        	$this->_helper->viewRenderer->setNoRender(true);
        }else{
        	if ( $wo->success ) {
				$apiWoTagClass = new Core_Api_WoTagClass();
	            $paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($wo->data,$countRows));
	            $paginator->setItemCountPerPage($this->_size);
	            $paginator->setCurrentPageNumber($this->_page);
				$this->view->wotag_class = $apiWoTagClass;
				$this->view->tag_status = $apiWoTagClass->getCompletionStatusForWin($win_nums);
                $this->pullWosTechCount($paginator,'all');
				$this->view->wo_visits = $this->_visits->getNumOfVisits ($win_nums);
				$this->view->totals = $this->_visits->getTotal_ApReThFS ($win_nums);
				$this->view->qa_unseencount = $this->_visits->getNum_ofUnseen_QA_fromTech ($this->_company, $win_nums);
	            $this->view->paginator = $paginator;
       		}

	        $this->view->techs      = $techs;
	        $this->view->success    = $wo->success;
	        $this->view->errors     = $wo->errors;
        } 
    }

    public function deactivatedAction()
    {
        $request = $this->getRequest();
        $this->_filters->WO_State = WO_STATE_DEACTIVATED;

        $search = new Core_Api_Class;

        // Save workorders into CSV file
        if ( $request->getParam('download', false) === 'csv' ) {
            try {
                $csvAdapter = new Core_CSV_Adapter_Client(
                    $this->_login.'|pmContext='.$this->_company,
                    $this->_password,
                    $this->_filters,
                    self::FIELDS,
                    $this->_sortStr
                );

                $csvExport = new Core_CSV_Builder('client', 'deactivated', $csvAdapter, defined('FLSVersion'), $this->view->isFLSManager, true);
                return $this->_outputCsv($csvExport);
            } catch (Core_CSV_Exception $e) {
                print 'error occurred';
                return false;
            }
        }
        //  End section: Save deactivated into CSV file

        $wo = $search->getWorkOrders(
            $this->_login.'|pmContext='.$this->_company,
            $this->_password,
            self::FIELDS,
            '',
            '',
            '',
            $this->_filters,
            ($this->_page-1)*$this->_size,
            $this->_size,
            $countRows,
            $this->_sortStr,
            true,
            true,
			true
        );

		$techs = array();
		$win_nums = array();
		if ($wo->success) {
			foreach($wo->data as $_wo) {
				$techs[] = (defined('FLSVersion')) ? $_wo->FLS_ID : $_wo->Tech_ID;
				$win_nums[] = $_wo->WIN_NUM;
			}
		}

		$win_nums = array_unique ($win_nums);

        //13819
        $this->view->winNumsHaveFiles = $this->getWinNumsHaveFiles($win_nums);
        //end 13819
        
        $tech = new API_Tech;
        $techs = $tech->lookupID(array_unique($techs));
        
		if( $request->getParam("download",false) === 'json'){
        	$wins = array();
        	foreach($wo->data as $w){
        		$wins[] = $w->WIN_NUM;
        	}
        	$response = $this->getResponse();
        	$response->clearHeaders()->clearBody();
        	$return = json_encode($wins);

        	echo $return;
        	$this->_helper->viewRenderer->setNoRender(true);
        }else{
        	if ( $wo->success ) {
	           	$paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($wo->data,$countRows));
	            $paginator->setItemCountPerPage($this->_size);
	            $paginator->setCurrentPageNumber($this->_page);
                $this->pullWosTechCount($paginator,'deactivated');
				$this->view->wo_visits = $this->_visits->getNumOfVisits ($win_nums);
				$this->view->totals = $this->_visits->getTotal_ApReThFS ($win_nums);
				$this->view->qa_unseencount = $this->_visits->getNum_ofUnseen_QA_fromTech ($this->_company, $win_nums);
	            $this->view->paginator = $paginator;
        	}

	        $this->view->techs      = $techs;
	        $this->view->success    = $wo->success;
	        $this->view->errors     = $wo->errors;
        } 
    }


    private function _outputCsv(Core_CSV_Builder $csvBuilder)
    {
        $this->_helper->viewRenderer->setNoRender();
        $response = $this->getResponse();
        $response->clearHeaders();
        $response->clearBody();
        $response->setHeader('Pragma', 'public')
                ->setHeader('Expires', '0')
                ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
                ->setHeader('Cache-Control', 'private')
                ->setHeader('Content-Type', 'text/csv')
                ->setHeader('Content-Disposition', 'attachment; filename='.$this->_company.'-'.ucfirst($csvBuilder->getTabName()).'-'.date('mdY').'.csv', true)
                ->setHeader('Content-Transfer-Encoding', 'binary');
        $response->sendResponse();

        //Flush buffers
        ob_end_flush();
        flush();

        $csvBuilder->printCSV();
        exit;
    }

    private function pullApplicantsCount($wos){
        if(empty($wos)) return;
        $ids = array();
        foreach($wos as $wo) $ids[] = $wo->WIN_NUM;
        //14018
        if(!empty($ids)){
            $db = Core_Database::getInstance();
            $s = $db->select();
            $s->from(array('wb'=>Core_Database::TABLE_WORK_ORDER_BIDS), 
                    array('WorkOrderID','c'=>'COUNT(wb.id)')
            );
            $s->joinLeft(array('te'=>Core_Database::TABLE_TECH_BANK_INFO),
                    'te.TechID = wb.TechID',
                    array()
            );
            $s->where('wb.WorkOrderID IN (?)', $ids);
            $s->where('wb.Company_ID = ?', $this->_company);
            $s->where('te.HideBids IS NULL OR te.HideBids = 0');
            $s->group('wb.WorkOrderID');
            //echo('pullApplicantsCount: <pre>');print_r($s->__toString());echo('</pre>');echo('<br/><br/>'); die();       
            $res = $db->fetchAll($s);
            $bidsCount = array();
            foreach ($res as $r) $bidsCount[$r['WorkOrderID']] = $r['c'];
        }
        //end 14018
        $this->view->bidsCount = (empty($bidsCount)) ? null : $bidsCount;
    }
    
    //--- 369
    private function pullWosTechCount($wos,$tab)
    {
        if(empty($wos)) return;
        
        $techids = array ();
        foreach($wos as $wo) 
        {
            if(!empty($wo->Tech_ID)) $techids[] = $wo->Tech_ID;
        }
        
        if(empty($techids)) return;

		$techids = implode (",", array_unique ($techids));

        $companyId = $this->_company;
        
        $criteria = "Tech_ID IN ($techids)";
        /*if(!empty($companyId)) {$criteria.=" AND Company_ID='$companyId'";}
        if($tab=="assigned")
        {
            $criteria .= " AND (Status = 'assigned')";
        }
        else if($tab=="workdone")
        {
            $criteria .= " AND (Status = 'work done')";
        }
        else if($tab=="incomplete")
        {
            $criteria .= " AND (Status = 'incomplete')";
        }        
        else if($tab=="completed")
        {
            $criteria .= " AND (Approved = 1) AND (Deactivated = 0)";
        }        
        else if($tab=="all")
        {
            $criteria .= " AND (Deactivated = 0)";
        }        
        else if($tab=="deactivated")
        {
            $criteria .= " AND (Status = 'deactivated')";
        }        
        else
        {
            return;
        }*/
        //$companyID, $timeStampFrom, $timeStampTo, $timeStampType, API_WorkOrderFilter $filters
        $where = API_WorkOrderFilter::assembleCriteria(
                $companyId, 
                '',
                '', 
                '', 
                $this->_filters);

        if(!empty($where)){
            $criteria .= " AND ".$where;
        }
        if(!empty($techids))
        {
            $db = Core_Database::getInstance();
            $select = $db->select();
            $select->from(Core_Database::TABLE_WORK_ORDERS,
                                array('Tech_ID','c'=>'COUNT(WIN_NUM)')
            );
            //13892
            $select->joinLeft(array("z"=>"zipcodes"), 
                    "z.ZIPCode=work_orders.Zipcode",
                    array()
            );     
            //end 13892                   
            $select->where($criteria);
            $select->group("Tech_ID");

            $res = $db->fetchAll($select);
            
            $wosTechCount = array();
            foreach ($res as $r) $wosTechCount[$r['Tech_ID']] = $r['c'];
        }        
        $this->view->wosTechCount = (empty($wosTechCount)) ? null : $wosTechCount;
    }
    
    private function keepSortAndFillter()
    {
        $request = $this->getRequest();
        $sort1 = $request->getParam('sort1','');
        $dir1 = $request->getParam('dir1','');
        $sort2 = $request->getParam('sort2','');
        $dir2 = $request->getParam('dir2','');
        $sort3 = $request->getParam('sort3','');
        $dir3 = $request->getParam('dir3','');
        
        
        $auth = new Zend_Session_Namespace('Auth_Sort');
        $auth->dir1 = $dir1;
        $auth->sort1 = $sort1;
        $auth->dir2 = $dir2;
        $auth->sort2 = $sort2;
        $auth->dir3 = $dir3;
        $auth->sort3 = $sort3;
        
        $auth = new Zend_Session_Namespace('Auth_Fillter');
        $start_date = $request->getParam('start_date','');
        $end_date = $request->getParam('end_date','');
        $state = $request->getParam('state','');
        $zip = $request->getParam('zip','');
        $tech = $request->getParam('tech','');
        $country = $request->getParam('country','');
        $po = $request->getParam('po','');
        $region = $request->getParam('region','');
        $call_type = $request->getParam('call_type','');
        $projects = $request->getParam('projects','');
        $route = $request->getParam('route','');
        
        $auth->start_date = $start_date;
        $auth->end_date = $end_date;
        $auth->state = $state;
        $auth->zip = $zip;
        $auth->tech = $tech;
        $auth->country = $country;
        $auth->po = $po;
        $auth->region = $region;
        $auth->route = $route;
        $auth->call_type = $call_type;
        $auth->projects = json_encode($projects);
        
    }
    
    //--- 360
    
    public function auditcompleteAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $request = $this->getRequest();
        $winNum = $request->getParam('WIN_NUM', null);
                
        $auditComplete = 1;
        $auditCompleteBy = $this->_login;
        $auditCompleteDate = date("Y-m-d H:i:s");
        $auditCompleteDate1 = date("m/d/Y h:i A");
        
        $return = array();
        
        if(!empty($winNum))
        {        
            $api= new Core_Api_Class();
            $api->updateWorkOrderAdditionalFields($winNum,
                array("AuditComplete"=>$auditComplete,
                    "AuditCompleteBy"=>$auditCompleteBy,
                    "AuditCompleteDate"=>$auditCompleteDate            
                )
            );
            
            $return['success'] = 1;
            $return['AuditCompleteDate'] = $auditCompleteDate1;
        }
        else
        {
            $return['success'] = -1;
            $return['AuditCompleteDate'] = '';
        }
        
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true);
        $response->setBody($return);
    }
    function getReasonAction(){
        $this->_helper->viewRenderer->setNoRender(true);
        $search = new Core_Api_Class;
        $reasons = $search->getDeactivationCodes();
        $deactivationCodes = array();
        $html = "";
        if ($reasons->success)
            $deactivationCodes = $reasons->data;
        $html .= '<select id="DeactivationCode" >
           <option value="" >Select Deactivation Code</option>';
        foreach ($deactivationCodes as $code){
            $html .= '<option title="'.$code.'" value="'.$code.'" >';
            $html .= $code;
            $html .= '</option>';
        }
        $html .= '</select>';
        echo $html;
    }
    function deactivationWosAction() {
        $wins = $this->getRequest()->getParam('wins', "");
        $deactivationCode = $this->getRequest()->getParam('deactivationCode', "");
        $companyId = $this->getRequest()->getParam('company', "");
        $isMassDeactivate = $this->getRequest()->getParam('massDeactivate', "");
        $arrwin = explode(",", $wins);
        $search = new Core_Api_Class;
        if (!empty($arrwin)) {
            foreach ($arrwin as $win) {
            	$errors = Core_Api_Error::getInstance();
        		$errors->resetErrors();

                $wo = $search->getWorkOrder(
                        $this->_login . '|pmContext=' . $companyId, $this->_password, $win
                );
                
                if ($wo->success && !empty($wo->data[0]->WIN_NUM)) {
                    $wo = $wo->data[0];
                    $wo->Deactivated = 1;
                    $wo->DeactivationCode = $deactivationCode;
                    $search->updateWorkOrder(
                            $this->_login . '|pmContext=' . $companyId, $this->_password, $wo->WIN_NUM, $wo, true, $isMassDeactivate);
}
            }
        }
       
        $this->_helper->json(array("success" => 1));
    }
    function todaysworkAction(){
        $this->view->addScriptPath(DASHBOARD_MODULE_PATH.'views/scripts');
        $request = $this->getRequest();

        $apiWosClass = new Core_Api_WosClass();
        $winIDs = $apiWosClass->getWinIDs_ForToday($this->_company);
        if(!empty($winIDs)) $this->_filters->TB_UNID = $winIDs;
        else $this->_filters->TB_UNID = '-1';

        $search = new Core_Api_Class;

        // Save workorders into CSV file
        if ( $request->getParam('download', false) === 'csv' ) {
            try {
                $csvAdapter = new Core_CSV_Adapter_Client(
                    $this->_login.'|pmContext='.$this->_company,
                    $this->_password,
                    $this->_filters,
                    self::FIELDS,
                    $this->_sortStr
                );

                $csvExport = new Core_CSV_Builder('client', 'today', $csvAdapter, defined('FLSVersion'), $this->view->isFLSManager, true);
                return $this->_outputCsv($csvExport);
            } catch (Core_CSV_Exception $e) {
                print 'error occurred';
                return false;
            }
        }
        //  End section: Save all into CSV file

        $wo = $search->getWorkOrders(
            $this->_login.'|pmContext='.$this->_company,
            $this->_password,
            self::FIELDS,
            '',
            '',
            '',
            $this->_filters,
            ($this->_page-1)*$this->_size,
            $this->_size,
            $countRows,
            $this->_sortStr,
            true,
            true
        );

		$techs = array();
		$win_nums = array();
		if ($wo->success) {
			foreach($wo->data as $_wo) {
				$techs[] = (defined('FLSVersion')) ? $_wo->FLS_ID : $_wo->Tech_ID;
				$win_nums[] = $_wo->WIN_NUM;
			}
		}

		$win_nums = array_unique ($win_nums);

        //13819
        $this->view->winNumsHaveFiles = $this->getWinNumsHaveFiles($win_nums);
        //end 13819
        
        $tech = new API_Tech;
        $techs = $tech->lookupID(array_unique($techs));

        if( $request->getParam("download",false) === 'json'){
        	$wins = array();
        	foreach($wo->data as $w){
        		$wins[] = $w->WIN_NUM;
        	}
        	$response = $this->getResponse();
        	$response->clearHeaders()->clearBody();
        	$return = json_encode($wins);

        	echo $return;
        	$this->_helper->viewRenderer->setNoRender(true);
        }else{
        	if ( $wo->success ) {
	            $paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($wo->data,$countRows));
	            $paginator->setItemCountPerPage($this->_size);
	            $paginator->setCurrentPageNumber($this->_page);
                $this->pullWosTechCount($paginator,'all');
                $this->pullApplicantsCount($paginator);
				$this->view->wo_visits = $this->_visits->getNumOfVisits ($win_nums);
				$this->view->totals = $this->_visits->getTotal_ApReThFS ($win_nums);
				$this->view->qa_unseencount = $this->_visits->getNum_ofUnseen_QA_fromTech ($this->_company, $win_nums);
	            $this->view->paginator = $paginator;
       		}

	        $this->view->techs      = $techs;
	        $this->view->success    = $wo->success;
	        $this->view->errors     = $wo->errors;
        } 
    }
    
    //13819
    private function getWinNumsHaveFiles($win_nums) //$filter)
    {
		$allWinNumsHaveFiles = array();

		if (count ($win_nums) > 0) {
			$coreFiles = new Core_Files();
			$filesCounts = $coreFiles->getFilesCountWithWOID($win_nums, Core_Files::TECH_FILE);

			if (is_array ($filesCounts)) {
				foreach ($filesCounts as $_wo) {
					if (isset ($_wo["file_count"]) && $_wo["file_count"] > 0) {
						$allWinNumsHaveFiles[$_wo["WIN_NUM"]] = array (
							"WIN_NUM" => $_wo["WIN_NUM"],
							"WO_ID" => $_wo["WO_ID"],
							"count" => $_wo["file_count"]
						);
					}
				}
			}
		}

		return $allWinNumsHaveFiles;
    }
}
