<?php

class Dashboard_TechSurveyController extends Core_Controller_Action {

    public function init()
    {
        parent::init();
        $this->_techID = $_SESSION['TechID'];
        $auth = new Zend_Session_Namespace('Auth_User');
        if ($auth->loggedInAs != 'tech' && $auth->loggedInAs != 'admin')
        {
            throw new Core_Auth_Exception('Authentication required');
        }
    }

    public function surveyAction()
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $Params = $request->getParams();
        $win = $Params['win'];
        $api_WosClass = new Core_Api_WosClass();
        $api_TechClass = new Core_Api_TechClass();
        $wo = $api_WosClass->getWOInfoforSurvey($win);
        $SurveyInfo = $api_TechClass->getSurveyByWinNum($win);
//        print_r("SurveyInfo<pre>");
//        print_r($SurveyInfo);
//        print_r("</pre>");die;
        if (!empty($SurveyInfo))
        {
            $this->view->SurveyInfo = $SurveyInfo;
        } else
        {
            $this->view->SurveyInfo = null;
        }
        $this->view->wo = $wo;
        $this->view->win = $win;
        $work_place = $this->view->render('tech-survey/survey.phtml');

        $this->_helper->viewRenderer->setNoRender();

        $json_output = array('success' => 1, 'error' => '', 'work_place' => $work_place);
        $response->clearAllHeaders()->clearBody();
        $response->setHeader('Content-type', 'application/json');
        $response->setBody(json_encode($json_output));
    }

    private function FileSQ($key)
    {
        $strlocation = array(
            "Sec1Q13_FrontofStore" =>
            array(
                "S" => "StoreInfo", "Q" => "13", "O" => 1
            ),
            "S3Q7a_BE_Bline_Pic" =>
            array(
                "S" => "FOHInv", "Q" => "7a", "O" => 1
            ),
            "S3Q7b_BE_Fline_Pic" =>
            array(
                "S" => "FOHInv", "Q" => "7b", "O" => 1
            ),
            "S3Q7c_BE_DThru_Pic" =>
            array(
                "S" => "FOHInv", "Q" => "7c", "O" => 1
            ),
            "S3Q7d_BE_Others_Pic1" =>
            array(
                "S" => "FOHInv", "Q" => "7d", "O" => 1
            ),
            "S3Q7d_BE_Others_Pic2" =>
            array(
                "S" => "FOHInv", "Q" => "7d", "O" => 2
            ),
            "S3Q7d_BE_Others_Pic3" =>
            array(
                "S" => "FOHInv", "Q" => "7d", "O" => 3
            ),
            "S3Q8b_GR_Bline_Pic1" =>
            array(
                "S" => "FOHInv", "Q" => "8b", "O" => 1
            ),
            "S3Q8b_GR_Bline_Pic2" =>
            array(
                "S" => "FOHInv", "Q" => "8b", "O" => 2
            ),
            "S3Q8b_GR_Bline_Pic3" =>
            array(
                "S" => "FOHInv", "Q" => "8b", "O" => 3
            ),
            "S3Q8b_GR_Bline_Pic4" =>
            array(
                "S" => "FOHInv", "Q" => "8b", "O" => 4
            ),
            "S3Q8b_GR_Bline_Pic5" =>
            array(
                "S" => "FOHInv", "Q" => "8b", "O" => 5
            ),
            "S3Q8b_GR_Bline_Pic6" =>
            array(
                "S" => "FOHInv", "Q" => "8b", "O" => 6
            ),
            "S3Q8b_GR_Bline_Pic7" =>
            array(
                "S" => "FOHInv", "Q" => "8b", "O" => 7
            ),
            "S3Q8b_GR_Bline_Pic8" =>
            array(
                "S" => "FOHInv", "Q" => "8b", "O" => 8
            ),
            "S3Q8b_GR_Bline_Pic9" =>
            array(
                "S" => "FOHInv", "Q" => "8b", "O" => 9
            ),
            "S3Q8b_GR_Bline_Pic10" =>
            array(
                "S" => "FOHInv", "Q" => "8b", "O" => 10
            ),
            "S3Q8c_GR_Fline_Pic1" =>
            array(
                "S" => "FOHInv", "Q" => "8c", "O" => 1
            ),
            "S3Q8c_GR_Fline_Pic2" =>
            array(
                "S" => "FOHInv", "Q" => "8c", "O" => 2
            ),
            "S3Q8c_GR_Fline_Pic3" =>
            array(
                "S" => "FOHInv", "Q" => "8c", "O" => 3
            ),
            "S3Q8c_GR_Fline_Pic4" =>
            array(
                "S" => "FOHInv", "Q" => "8c", "O" => 4
            ),
            "S3Q8c_GR_Fline_Pic5" =>
            array(
                "S" => "FOHInv", "Q" => "8c", "O" => 5
            ),
            "S3Q8c_GR_Fline_Pic6" =>
            array(
                "S" => "FOHInv", "Q" => "8c", "O" => 6
            ),
            "S3Q8c_GR_Fline_Pic7" =>
            array(
                "S" => "FOHInv", "Q" => "8c", "O" => 7
            ),
            "S3Q8c_GR_Fline_Pic8" =>
            array(
                "S" => "FOHInv", "Q" => "8c", "O" => 8
            ),
            "S3Q8c_GR_Fline_Pic9" =>
            array(
                "S" => "FOHInv", "Q" => "8c", "O" => 9
            ),
            "S3Q8c_GR_Fline_Pic10" =>
            array(
                "S" => "FOHInv", "Q" => "8c", "O" => 10
            ),
            "S3Q8d_GR_DThru_Pic1" =>
            array(
                "S" => "FOHInv", "Q" => "8d", "O" => 1
            ),
            "S3Q8d_GR_DThru_Pic2" =>
            array(
                "S" => "FOHInv", "Q" => "8d", "O" => 2
            ),
            "S3Q8d_GR_DThru_Pic3" =>
            array(
                "S" => "FOHInv", "Q" => "8d", "O" => 3
            ),
            "S3Q8d_GR_DThru_Pic4" =>
            array(
                "S" => "FOHInv", "Q" => "8d", "O" => 4
            ),
            "S3Q8d_GR_DThru_Pic5" =>
            array(
                "S" => "FOHInv", "Q" => "8d", "O" => 5
            ),
            "S3Q8d_GR_DThru_Pic6" =>
            array(
                "S" => "FOHInv", "Q" => "8d", "O" => 6
            ),
            "S3Q8d_GR_DThru_Pic7" =>
            array(
                "S" => "FOHInv", "Q" => "8d", "O" => 7
            ),
            "S3Q8d_GR_DThru_Pic8" =>
            array(
                "S" => "FOHInv", "Q" => "8d", "O" => 8
            ),
            "S3Q8d_GR_DThru_Pic9" =>
            array(
                "S" => "FOHInv", "Q" => "8d", "O" => 9
            ),
            "S3Q8d_GR_DThru_Pic10" =>
            array(
                "S" => "FOHInv", "Q" => "8d", "O" => 10
            ),
            "S3Q8e_GR_Others_Pic1" =>
            array(
                "S" => "FOHInv", "Q" => "8e", "O" => 1
            ),
            "S3Q8e_GR_Others_Pic2" =>
            array(
                "S" => "FOHInv", "Q" => "8e", "O" => 2
            ),
            "S3Q8e_GR_Others_Pic3" =>
            array(
                "S" => "FOHInv", "Q" => "8e", "O" => 3
            ),
            "S3Q8e_GR_Others_Pic4" =>
            array(
                "S" => "FOHInv", "Q" => "8e", "O" => 4
            ),
            "S3Q8e_GR_Others_Pic5" =>
            array(
                "S" => "FOHInv", "Q" => "8e", "O" => 5
            ),
            "S3Q8e_GR_Others_Pic6" =>
            array(
                "S" => "FOHInv", "Q" => "8e", "O" => 6
            ),
            "S3Q8e_GR_Others_Pic7" =>
            array(
                "S" => "FOHInv", "Q" => "8e", "O" => 7
            ),
            "S3Q8e_GR_Others_Pic8" =>
            array(
                "S" => "FOHInv", "Q" => "8e", "O" => 8
            ),
            "S3Q8e_GR_Others_Pic9" =>
            array(
                "S" => "FOHInv", "Q" => "8e", "O" => 9
            ),
            "S3Q8e_GR_Others_Pic10" =>
            array(
                "S" => "FOHInv", "Q" => "8e", "O" => 10
            ),
            "S4Q6_Pic" =>
            array(
                "S" => "FOHMsmt", "Q" => "6", "O" => 1
            ),
            "S4Q12_Pic1" =>
            array(
                "S" => "FOHMsmt", "Q" => "12", "O" => 1
            ),
            "S4Q12_Pic2" =>
            array(
                "S" => "FOHMsmt", "Q" => "12", "O" => 2
            ),
            "S4Q12_Pic3" =>
            array(
                "S" => "FOHMsmt", "Q" => "12", "O" => 3
            ),
            "S4Q12_Pic4" =>
            array(
                "S" => "FOHMsmt", "Q" => "12", "O" => 4
            ),
            "S4Q12_Pic5" =>
            array(
                "S" => "FOHMsmt", "Q" => "12", "O" => 5
            ),
            "S4Q12_Pic6" =>
            array(
                "S" => "FOHMsmt", "Q" => "12", "O" => 6
            ), "S4Q12_Pic7" =>
            array(
                "S" => "FOHMsmt", "Q" => "12", "O" => 7
            ), "S4Q12_Pic8" =>
            array(
                "S" => "FOHMsmt", "Q" => "12", "O" => 8
            ), "S4Q12_Pic9" =>
            array(
                "S" => "FOHMsmt", "Q" => "12", "O" => 9
            ), "S4Q12_Pic10" =>
            array(
                "S" => "FOHMsmt", "Q" => "12", "O" => 10
            ), "S4Q12_Pic11" =>
            array(
                "S" => "FOHMsmt", "Q" => "12", "O" => 11
            ), "S4Q12_Pic12" =>
            array(
                "S" => "FOHMsmt", "Q" => "12", "O" => 12
            ), "S4Q12_Pic13" =>
            array(
                "S" => "FOHMsmt", "Q" => "12", "O" => 13
            ), "S4Q12_Pic14" =>
            array(
                "S" => "FOHMsmt", "Q" => "12", "O" => 14
            ), "S4Q12_Pic15" =>
            array(
                "S" => "FOHMsmt", "Q" => "12", "O" => 15
            ), "S4Q12_Pic16" =>
            array(
                "S" => "FOHMsmt", "Q" => "12", "O" => 16
            ), "S4Q12_Pic17" =>
            array(
                "S" => "FOHMsmt", "Q" => "12", "O" => 17
            ), "S4Q12_Pic18" =>
            array(
                "S" => "FOHMsmt", "Q" => "12", "O" => 18
            ), "S4Q12_Pic19" =>
            array(
                "S" => "FOHMsmt", "Q" => "12", "O" => 19
            ),
            "S4Q12_Pic20" =>
            array(
                "S" => "FOHMsmt", "Q" => "12", "O" => 20
            ),
            "S5Q2b_Pic1" =>
            array(
                "S" => "FOHHotWater", "Q" => "2b", "O" => 1
            ),
            "S5Q2b_Pic2" =>
            array(
                "S" => "FOHHotWater", "Q" => "2b", "O" => 2
            ),
            "S5Q2b_Pic3" =>
            array(
                "S" => "FOHHotWater", "Q" => "2b", "O" => 3
            ),
            "S5Q2b_Pic4" =>
            array(
                "S" => "FOHHotWater", "Q" => "2b", "O" => 4
            ),
            "S5Q2b_Pic5" =>
            array(
                "S" => "FOHHotWater", "Q" => "2b", "O" => 5
            ),
            "S5Q2b_Pic6" =>
            array(
                "S" => "FOHHotWater", "Q" => "2b", "O" => 6
            ),
            "S5Q2b_Pic7" =>
            array(
                "S" => "FOHHotWater", "Q" => "2b", "O" => 7
            ),
            "S5Q2b_Pic8" =>
            array(
                "S" => "FOHHotWater", "Q" => "2b", "O" => 8
            ),
            "S5Q2b_Pic9" =>
            array(
                "S" => "FOHHotWater", "Q" => "2b", "O" => 9
            ),
            "S5Q2b_Pic10" =>
            array(
                "S" => "FOHHotWater", "Q" => "2b", "O" => 10
            ),
            "S5Q3b_Pic1" =>
            array(
                "S" => "FOHHotWater", "Q" => "3b", "O" => 1
            ),
            "S5Q3b_Pic2" =>
            array(
                "S" => "FOHHotWater", "Q" => "3b", "O" => 2
            ),
            "S5Q3b_Pic3" =>
            array(
                "S" => "FOHHotWater", "Q" => "3b", "O" => 3
            ),
            "S5Q3b_Pic4" =>
            array(
                "S" => "FOHHotWater", "Q" => "3b", "O" => 4
            ),
            "S5Q3b_Pic5" =>
            array(
                "S" => "FOHHotWater", "Q" => "3b", "O" => 5
            ),
            "S5Q3b_Pic6" =>
            array(
                "S" => "FOHHotWater", "Q" => "3b", "O" => 6
            ),
            "S5Q3b_Pic7" =>
            array(
                "S" => "FOHHotWater", "Q" => "3b", "O" => 7
            ),
            "S5Q3b_Pic8" =>
            array(
                "S" => "FOHHotWater", "Q" => "3b", "O" => 8
            ),
            "S5Q3b_Pic9" =>
            array(
                "S" => "FOHHotWater", "Q" => "3b", "O" => 9
            ),
            "S5Q3b_Pic10" =>
            array(
                "S" => "FOHHotWater", "Q" => "3b", "O" => 10
            ),
            "S5Q4b_Pic1" =>
            array(
                "S" => "FOHHotWater", "Q" => "4b", "O" => 1
            ),
            "S5Q4b_Pic2" =>
            array(
                "S" => "FOHHotWater", "Q" => "4b", "O" => 2
            ),
            "S5Q4b_Pic3" =>
            array(
                "S" => "FOHHotWater", "Q" => "4b", "O" => 3
            ),
            "S5Q4b_Pic4" =>
            array(
                "S" => "FOHHotWater", "Q" => "4b", "O" => 4
            ),
            "S5Q4b_Pic5" =>
            array(
                "S" => "FOHHotWater", "Q" => "4b", "O" => 5
            ),
            "S5Q4b_Pic6" =>
            array(
                "S" => "FOHHotWater", "Q" => "4b", "O" => 6
            ),
            "S5Q4b_Pic7" =>
            array(
                "S" => "FOHHotWater", "Q" => "4b", "O" => 7
            ),
            "S5Q4b_Pic8" =>
            array(
                "S" => "FOHHotWater", "Q" => "4b", "O" => 8
            ),
            "S5Q4b_Pic9" =>
            array(
                "S" => "FOHHotWater", "Q" => "4b", "O" => 9
            ),
            "S5Q4b_Pic10" =>
            array(
                "S" => "FOHHotWater", "Q" => "4b", "O" => 10
            ),
            "S6Q3_Pic" =>
            array(
                "S" => "FOHPlumb", "Q" => "3", "O" => 1
            ),
            "S6Q4_Pic" =>
            array(
                "S" => "FOHPlumb", "Q" => "4", "O" => 1
            ),
            "S6Q5_Pic" =>
            array(
                "S" => "FOHPlumb", "Q" => "5", "O" => 1
            )
            ,
            "S7Q5_WaterFilter_Pic" =>
            array(
                "S" => "BOHPlumb", "Q" => "5", "O" => 1
            ),
            "S8Q1_ElecPanel_Pic" =>
            array(
                "S" => "BOHElect", "Q" => "1", "O" => 1
            ),
            "S8Q2_Circuits_Pic1" =>
            array(
                "S" => "BOHElect", "Q" => "2", "O" => 1
            ),
            "S8Q2_Circuits_Pic2" =>
            array(
                "S" => "BOHElect", "Q" => "2", "O" => 2
            ),
            "S8Q2_Circuits_Pic3" =>
            array(
                "S" => "BOHElect", "Q" => "2", "O" => 3
            ),
            "S8Q2_Circuits_Pic4" =>
            array(
                "S" => "BOHElect", "Q" => "2", "O" => 4
            ),
            "S8Q2_Circuits_Pic5" =>
            array(
                "S" => "BOHElect", "Q" => "2", "O" => 5
            ),
            "S8Q3_PanelDiagram_Pic" =>
            array(
                "S" => "BOHElect", "Q" => "3", "O" => 1
            )
        );
        return $strlocation[$key];
    }

    private function FileZip($key)
    {
        $strzip = array(
            "S3Q7d_BE_Others_Zip" =>
            array(
                "S3Q7d_BE_Others_Pic1", "S3Q7d_BE_Others_Pic2", "S3Q7d_BE_Others_Pic3"
            ),
            "S3Q8b_GR_Bline_Zip" =>
            array(
                "S3Q8b_GR_Bline_Pic1", "S3Q8b_GR_Bline_Pic2", "S3Q8b_GR_Bline_Pic3", "S3Q8b_GR_Bline_Pic4", "S3Q8b_GR_Bline_Pic5", "S3Q8b_GR_Bline_Pic6", "S3Q8b_GR_Bline_Pic7", "S3Q8b_GR_Bline_Pic8", "S3Q8b_GR_Bline_Pic9", "S3Q8b_GR_Bline_Pic10"
            ),
            "S3Q8c_GR_Fline_Zip" =>
            array(
                "S3Q8c_GR_Fline_Pic1", "S3Q8c_GR_Fline_Pic2", "S3Q8c_GR_Fline_Pic3", "S3Q8c_GR_Fline_Pic4", "S3Q8c_GR_Fline_Pic5", "S3Q8c_GR_Fline_Pic6", "S3Q8c_GR_Fline_Pic7", "S3Q8c_GR_Fline_Pic8", "S3Q8c_GR_Fline_Pic9", "S3Q8c_GR_Fline_Pic10"
            ),
            "S3Q8d_GR_DThru_Zip" =>
            array(
                "S3Q8d_GR_DThru_Pic1", "S3Q8d_GR_DThru_Pic2", "S3Q8d_GR_DThru_Pic3", "S3Q8d_GR_DThru_Pic4", "S3Q8d_GR_DThru_Pic5", "S3Q8d_GR_DThru_Pic6", "S3Q8d_GR_DThru_Pic7", "S3Q8d_GR_DThru_Pic8", "S3Q8d_GR_DThru_Pic9", "S3Q8d_GR_DThru_Pic10"
            ),
            "S3Q8e_GR_Others_Zip" =>
            array(
                "S3Q8e_GR_Others_Pic1", "S3Q8e_GR_Others_Pic2", "S3Q8e_GR_Others_Pic3", "S3Q8e_GR_Others_Pic4", "S3Q8e_GR_Others_Pic5", "S3Q8e_GR_Others_Pic6", "S3Q8e_GR_Others_Pic7", "S3Q8e_GR_Others_Pic8", "S3Q8e_GR_Others_Pic9", "S3Q8e_GR_Others_Pic10"
            ),
            "S4Q12_Zip" =>
            array(
                "S4Q12_Pic1", "S4Q12_Pic2", "S4Q12_Pic3", "S4Q12_Pic4", "S4Q12_Pic5", "S4Q12_Pic6", "S4Q12_Pic7", "S4Q12_Pic8", "S4Q12_Pic9", "S4Q12_Pic10", "S4Q12_Pic11", "S4Q12_Pic12", "S4Q12_Pic13", "S4Q12_Pic14", "S4Q12_Pic15", "S4Q12_Pic16", "S4Q12_Pic17", "S4Q12_Pic18", "S4Q12_Pic19", "S4Q12_Pic20"
            ),
            "S5Q2b_Zip" =>
            array(
                "S5Q2b_Pic1", "S5Q2b_Pic2", "S5Q2b_Pic3", "S5Q2b_Pic4", "S5Q2b_Pic5", "S5Q2b_Pic6", "S5Q2b_Pic7", "S5Q2b_Pic8", "S5Q2b_Pic9", "S5Q2b_Pic10"
            ),
            "S5Q3b_Zip" =>
            array(
                "S5Q3b_Pic1", "S5Q3b_Pic2", "S5Q3b_Pic3", "S5Q3b_Pic4", "S5Q3b_Pic5", "S5Q3b_Pic6", "S5Q3b_Pic7", "S5Q3b_Pic8", "S5Q3b_Pic9", "S5Q3b_Pic10"
            ),
            "S5Q4b_Zip" =>
            array(
                "S5Q4b_Pic1", "S5Q4b_Pic2", "S5Q4b_Pic3", "S5Q4b_Pic4", "S5Q4b_Pic5", "S5Q4b_Pic6", "S5Q4b_Pic7", "S5Q4b_Pic8", "S5Q4b_Pic9", "S5Q4b_Pic10"
            ),
            "S8Q2_Circuits_Zip" =>
            array(
                "S8Q2_Circuits_Pic1", "S8Q2_Circuits_Pic2", "S8Q2_Circuits_Pic3", "S8Q2_Circuits_Pic4", "S8Q2_Circuits_Pic5"
            )
        );
        if ($key === "0")
        {
            return $strzip;
        } else
        {
            return $strzip[$key];
        }
    }

//    private function deleteFile($filesNames)
//    {
//        $files = new Core_Files();
//        $files->removeFiles($filesNames
//                , S3_TECHS_DOCS_DIR);
//    }

    public function loadNumcombo1($num)
    {
        $str = "";
        for ($i = 1; $i <= $num; $i++)
        {
            $str.="<option value = '" . $i . "' > " . $i . "</option>";
        }
        return $str;
    }

    public function doSubmitAction()
    {
        $response = $this->getResponse();
        $this->_helper->viewRenderer->setNoRender(true);
        $request = $this->getRequest();
        $Params = $request->getParams();
        $win = $Params['win'];
        $surveyID = $Params['surveyID'];
        $api_WosClass = new Core_Api_WosClass();
        $api_TechClass = new Core_Api_TechClass();
        $file = new Core_File();
        $wo = $api_WosClass->getWOInfoforSurvey($win);

        $site = $wo['SiteNumber'];
        if (!empty($_FILES))
        {
            $i = 1;
            foreach ($_FILES as $k => $fileStruc)
            {
                $FileSQ = $this->FileSQ($k);
                if ($fileStruc['error'] == UPLOAD_ERR_OK &&
                        is_uploaded_file($fileStruc['tmp_name']) &&
                        filesize($fileStruc['tmp_name']) > 0
                )
                {
                    $fname = $fileStruc['name'];
                    if (strrpos($fname, '/') !== FALSE)
                    {
                        $fname = substr($fname, strrpos($fname, '/'));
                    }

                    $info = pathinfo($fname);
                    $fname = "Site" . $site . "-WIN" . $win . "-" . $FileSQ["S"] . "-Q" . $FileSQ["Q"] . "-Pic" . $FileSQ["O"] . "." . $info['extension'];
                    $fdata = file_get_contents($fileStruc['tmp_name']);
                    
                    $uploadResult = $file->uploadFile($fname, $fdata, S3_TECHS_DOCS_DIR);
                    if ($uploadResult)
                    {
                        $Params[$k] = $fname;
                    }
                }
                $i++;
            }

            //create zip file;
            $Ziplist = $this->FileZip("0");

            $downloadFolder = realpath(APPLICATION_PATH . '/../../htdocs/clients/downloadtmp') . "/surveypiczip";

            if (!is_dir($downloadFolder))
            {
                mkdir($downloadFolder);
            }
            $downloadFolder = $downloadFolder . '/' . $this->_login;
            if (!is_dir($downloadFolder))
            {
                mkdir($downloadFolder);
            }

            $filecreate = array();

            foreach ($Ziplist as $key => $val)
            {
                $temarr = array();
                foreach ($val as $itemkey => $itemval)
                {
                    if ($_FILES[$itemval]['error'] == UPLOAD_ERR_OK &&
                            is_uploaded_file($_FILES[$itemval]['tmp_name']) &&
                            filesize($_FILES[$itemval]['tmp_name']) > 0
                    )
                    {
                        array_push($temarr, $itemval);
                    }
                }
                if (!empty($temarr))
                {
                    $filecreate[$key] = $temarr;
                }
            }

            foreach ($filecreate as $key => $val)
            {
                $filePath = $downloadFolder . '/' . $key . ".zip";
                try
                {
                    unlink($filePath);
                    $zip = new ZipArchive;
                    if ($zip->open($filePath, ZIPARCHIVE::CREATE) === true)
                    {
                        foreach ($val as $itemkey => $itemval)
                        {
                            if (!empty($_FILES[$itemval]))
                            {
                                $fileOpenWOS = file_get_contents($_FILES[$itemval]['tmp_name']);
                                $zip->addFromString($Params[$itemval], $fileOpenWOS);
                            }
                        }
                    }
                    $zip->close();

                    $Zname = "Site" . $site . "-WIN" . $win . "-" . $key . ".zip";
                    $file = new Core_File();
                    $uploadResult = $file->uploadFile($Zname, file_get_contents($filePath), S3_TECHS_DOCS_DIR);

                    if ($uploadResult)
                    {
                        $Params[$key] = $Zname;
                    }
                } catch (Exception $e)
                {
                    
                }
            }
        }

        $api_TechClass->saveSurvey($surveyID, $win, $this->_techID, $this->_login, $Params);

        $SurveyInfo = $api_TechClass->getSurveyByWinNum($win);
        $NewsurveyID = $SurveyInfo['SurveyID'];
        
        $PicArr = array("Sec1Q13_FrontofStore", "S3Q7a_BE_Bline_Pic", "S3Q7b_BE_Fline_Pic", "S3Q7c_BE_DThru_Pic", "S3Q7d_BE_Others_Pic1", "S3Q7d_BE_Others_Pic2", "S3Q7d_BE_Others_Pic3", "S3Q8b_GR_Bline_Pic1", "S3Q8b_GR_Bline_Pic2", "S3Q8b_GR_Bline_Pic3", "S3Q8b_GR_Bline_Pic4", "S3Q8b_GR_Bline_Pic5", "S3Q8b_GR_Bline_Pic6", "S3Q8b_GR_Bline_Pic7", "S3Q8b_GR_Bline_Pic8", "S3Q8b_GR_Bline_Pic9", "S3Q8b_GR_Bline_Pic10", "S3Q8c_GR_Fline_Pic1", "S3Q8c_GR_Fline_Pic2", "S3Q8c_GR_Fline_Pic3", "S3Q8c_GR_Fline_Pic4", "S3Q8c_GR_Fline_Pic5", "S3Q8c_GR_Fline_Pic6", "S3Q8c_GR_Fline_Pic7", "S3Q8c_GR_Fline_Pic8", "S3Q8c_GR_Fline_Pic9", "S3Q8c_GR_Fline_Pic10", "S3Q8d_GR_DThru_Pic1", "S3Q8d_GR_DThru_Pic2", "S3Q8d_GR_DThru_Pic3", "S3Q8d_GR_DThru_Pic4", "S3Q8d_GR_DThru_Pic5", "S3Q8d_GR_DThru_Pic6", "S3Q8d_GR_DThru_Pic7", "S3Q8d_GR_DThru_Pic8", "S3Q8d_GR_DThru_Pic9", "S3Q8d_GR_DThru_Pic10", "S3Q8e_GR_Others_Pic1", "S3Q8e_GR_Others_Pic2", "S3Q8e_GR_Others_Pic3", "S3Q8e_GR_Others_Pic4", "S3Q8e_GR_Others_Pic5", "S3Q8e_GR_Others_Pic6", "S3Q8e_GR_Others_Pic7", "S3Q8e_GR_Others_Pic8", "S3Q8e_GR_Others_Pic9", "S3Q8e_GR_Others_Pic10", "S4Q6_Pic", "S4Q12_Pic1", "S4Q12_Pic2", "S4Q12_Pic3", "S4Q12_Pic4", "S4Q12_Pic5", "S4Q12_Pic6", "S4Q12_Pic7", "S4Q12_Pic8", "S4Q12_Pic9", "S4Q12_Pic10", "S4Q12_Pic11", "S4Q12_Pic12", "S4Q12_Pic13", "S4Q12_Pic14", "S4Q12_Pic15", "S4Q12_Pic16", "S4Q12_Pic17", "S4Q12_Pic18", "S4Q12_Pic19", "S4Q12_Pic20", "S5Q2b_Pic1", "S5Q2b_Pic2", "S5Q2b_Pic3", "S5Q2b_Pic4", "S5Q2b_Pic5", "S5Q2b_Pic6", "S5Q2b_Pic7", "S5Q2b_Pic8", "S5Q2b_Pic9", "S5Q2b_Pic10", "S5Q3b_Pic1", "S5Q3b_Pic2", "S5Q3b_Pic3", "S5Q3b_Pic4", "S5Q3b_Pic5", "S5Q3b_Pic6", "S5Q3b_Pic7", "S5Q3b_Pic8", "S5Q3b_Pic9", "S5Q3b_Pic10", "S5Q4b_Pic1", "S5Q4b_Pic2", "S5Q4b_Pic3", "S5Q4b_Pic4", "S5Q4b_Pic5", "S5Q4b_Pic6", "S5Q4b_Pic7", "S5Q4b_Pic8", "S5Q4b_Pic9", "S5Q4b_Pic10", "S6Q3_Pic", "S6Q4_Pic", "S6Q5_Pic", "S7Q5_WaterFilter_Pic", "S8Q1_ElecPanel_Pic", "S8Q2_Circuits_Pic1", "S8Q2_Circuits_Pic2", "S8Q2_Circuits_Pic3", "S8Q2_Circuits_Pic4", "S8Q2_Circuits_Pic5", "S8Q3_PanelDiagram_Pic");
        $filePath = $downloadFolder . "/S0_AllPhotos_Zip.zip";
        unlink($filePath);
        $zip = new ZipArchive;
        if ($zip->open($filePath, ZIPARCHIVE::CREATE) === true)
        {
            foreach ($PicArr as $Pic)
            {
                if (!empty($SurveyInfo[$Pic]))
                {
                    $filecontent = $file->downloadFile($SurveyInfo[$Pic], S3_TECHS_DOCS_DIR);
                     
                    $zip->addFromString($SurveyInfo[$Pic], $filecontent);
                }
            }
            $zip->close();

            $Zname = "WIN" . $win . "-S0_AllPhotos_Zip.zip";
            $uploadResult = $file->uploadFile($Zname, file_get_contents($filePath), S3_TECHS_DOCS_DIR);
            if ($uploadResult)
            {
                $api_TechClass->uploadSurveySimple($NewsurveyID, array("S0_AllPhotos_Zip" => $Zname));
            }
        }
 
//        $json_output = array('success' => 1, 'error' => '');
//        $response->clearAllHeaders()->clearBody();
//        $response->setHeader('Content-type', 'application/json');
//        $response->setBody(json_encode($json_output));
    }

    public function doAllZipAction()
    {
        $response = $this->getResponse();
        $this->_helper->viewRenderer->setNoRender(true);
        $request = $this->getRequest();
        $Params = $request->getParams();
        $win = $Params['win'];
        $api_WosClass = new Core_Api_WosClass();
        $api_TechClass = new Core_Api_TechClass();
        $file = new Core_File();

        $SurveyInfo = $api_TechClass->getSurveyByWinNum($win);
        $surveyID = $SurveyInfo['SurveyID'];
        
        $PicArr = array("Sec1Q13_FrontofStore", "S3Q7a_BE_Bline_Pic", "S3Q7b_BE_Fline_Pic", "S3Q7c_BE_DThru_Pic", "S3Q7d_BE_Others_Pic1", "S3Q7d_BE_Others_Pic2", "S3Q7d_BE_Others_Pic3", "S3Q8b_GR_Bline_Pic1", "S3Q8b_GR_Bline_Pic2", "S3Q8b_GR_Bline_Pic3", "S3Q8b_GR_Bline_Pic4", "S3Q8b_GR_Bline_Pic5", "S3Q8b_GR_Bline_Pic6", "S3Q8b_GR_Bline_Pic7", "S3Q8b_GR_Bline_Pic8", "S3Q8b_GR_Bline_Pic9", "S3Q8b_GR_Bline_Pic10", "S3Q8c_GR_Fline_Pic1", "S3Q8c_GR_Fline_Pic2", "S3Q8c_GR_Fline_Pic3", "S3Q8c_GR_Fline_Pic4", "S3Q8c_GR_Fline_Pic5", "S3Q8c_GR_Fline_Pic6", "S3Q8c_GR_Fline_Pic7", "S3Q8c_GR_Fline_Pic8", "S3Q8c_GR_Fline_Pic9", "S3Q8c_GR_Fline_Pic10", "S3Q8d_GR_DThru_Pic1", "S3Q8d_GR_DThru_Pic2", "S3Q8d_GR_DThru_Pic3", "S3Q8d_GR_DThru_Pic4", "S3Q8d_GR_DThru_Pic5", "S3Q8d_GR_DThru_Pic6", "S3Q8d_GR_DThru_Pic7", "S3Q8d_GR_DThru_Pic8", "S3Q8d_GR_DThru_Pic9", "S3Q8d_GR_DThru_Pic10", "S3Q8e_GR_Others_Pic1", "S3Q8e_GR_Others_Pic2", "S3Q8e_GR_Others_Pic3", "S3Q8e_GR_Others_Pic4", "S3Q8e_GR_Others_Pic5", "S3Q8e_GR_Others_Pic6", "S3Q8e_GR_Others_Pic7", "S3Q8e_GR_Others_Pic8", "S3Q8e_GR_Others_Pic9", "S3Q8e_GR_Others_Pic10", "S4Q6_Pic", "S4Q12_Pic1", "S4Q12_Pic2", "S4Q12_Pic3", "S4Q12_Pic4", "S4Q12_Pic5", "S4Q12_Pic6", "S4Q12_Pic7", "S4Q12_Pic8", "S4Q12_Pic9", "S4Q12_Pic10", "S4Q12_Pic11", "S4Q12_Pic12", "S4Q12_Pic13", "S4Q12_Pic14", "S4Q12_Pic15", "S4Q12_Pic16", "S4Q12_Pic17", "S4Q12_Pic18", "S4Q12_Pic19", "S4Q12_Pic20", "S5Q2b_Pic1", "S5Q2b_Pic2", "S5Q2b_Pic3", "S5Q2b_Pic4", "S5Q2b_Pic5", "S5Q2b_Pic6", "S5Q2b_Pic7", "S5Q2b_Pic8", "S5Q2b_Pic9", "S5Q2b_Pic10", "S5Q3b_Pic1", "S5Q3b_Pic2", "S5Q3b_Pic3", "S5Q3b_Pic4", "S5Q3b_Pic5", "S5Q3b_Pic6", "S5Q3b_Pic7", "S5Q3b_Pic8", "S5Q3b_Pic9", "S5Q3b_Pic10", "S5Q4b_Pic1", "S5Q4b_Pic2", "S5Q4b_Pic3", "S5Q4b_Pic4", "S5Q4b_Pic5", "S5Q4b_Pic6", "S5Q4b_Pic7", "S5Q4b_Pic8", "S5Q4b_Pic9", "S5Q4b_Pic10", "S6Q3_Pic", "S6Q4_Pic", "S6Q5_Pic", "S7Q5_WaterFilter_Pic", "S8Q1_ElecPanel_Pic", "S8Q2_Circuits_Pic1", "S8Q2_Circuits_Pic2", "S8Q2_Circuits_Pic3", "S8Q2_Circuits_Pic4", "S8Q2_Circuits_Pic5", "S8Q3_PanelDiagram_Pic");
        $downloadFolder = realpath(APPLICATION_PATH . '/../../htdocs/clients/downloadtmp') . "/surveypiczip";

        if (!is_dir($downloadFolder))
        {
            mkdir($downloadFolder);
        }
        $downloadFolder = $downloadFolder . '/' . $this->_login;
        if (!is_dir($downloadFolder))
        {
            mkdir($downloadFolder);
        }
        $filePath = $downloadFolder . "/S0_AllPhotos_Zip.zip";
        $zip = new ZipArchive;
        if ($zip->open($filePath, ZIPARCHIVE::CREATE) === true)
        {
            foreach ($PicArr as $Pic)
            {
                if (!empty($SurveyInfo[$Pic]))
                {
                    $filecontent = $file->downloadFile($SurveyInfo[$Pic], S3_TECHS_DOCS_DIR);
                    $zip->addFromString($SurveyInfo[$Pic], $filecontent);
                }
            }
            $zip->close();

            $Zname = "WIN" . $win . "-S0_AllPhotos_Zip.zip";
            $uploadResult = $file->uploadFile($Zname, file_get_contents($filePath), S3_TECHS_DOCS_DIR);
            if ($uploadResult)
            {
                $api_TechClass->uploadSurveySimple($surveyID, array("S0_AllPhotos_Zip" => $Zname));
            }
        }
    }

}

?>
