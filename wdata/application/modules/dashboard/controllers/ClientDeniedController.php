<?php
class Dashboard_ClientDeniedController extends Core_Controller_Action
{
    public function init()
    {
		try {
			parent::init();
		} catch (Core_Auth_Exception $e) {
			if ($this->getRequest()->getActionName() != 'state-info') throw $e;
		}
        $request = $this->getRequest();
        $auth = new Zend_Session_Namespace('Auth_User');
        $this->view->name = $auth->name;
        $company = $request->getParam('company',NULL);
		if (empty($company)) $company = $request->getParam('Company_ID', NULL);

        $this->view->company = !empty($company)?$company:$_SESSION['Company_ID'];
        $this->view->Project_ID = $request->getParam('Project_ID', NULL);
	}

	public function denyAction()
    {
        $request = $this->getRequest();
		$this->view->TechID = $request->getParam('TechID', null);
		$this->view->FirstName = $request->getParam('FName', null);
		$this->view->LastName = $request->getParam('LName', null);
    }

	public function doDenyAction()
    {
		$this->_helper->viewRenderer->setNoRender();		
        $request = $this->getRequest();
		$params = $request->getParam('CreateForm', null);
		$techId = $params['TechID'];
        $comments = $params['Comments'];
        $this->view->company = $params['Company_ID'];
		
		$api = new Core_Api_Class;
		$result = $api->techDeny($this->_login.'|pmContext='.$this->view->company, $this->_password, $techId, $comments);
	
		echo "This tech has been added to your admin denied list.  
			<script type='text/javascript'>
				function closeDeny() {       
				try {          opener.focus();       }       
				catch(e) {}       
					setTimeout('window.close()', 20);    
				}    
				setTimeout('closeDeny()', 2000); 
			</script>";
    }

	public function removeDenyAction()
    {
        $request = $this->getRequest();
		$techId = $request->getParam('id', null);
		$this->view->company = $request->getParam('v',null);

		$api = new Core_Api_Class();
        $result = $api->removeTechDeny($this->_login.'|pmContext='.$this->view->company, $this->_password, $techId);
            
        $this->_redirect($_SERVER['HTTP_REFERER']);      
    }

	public function listAction()
    {
        $request = $this->getRequest();

        $size    = ($request->getParam('size', 10)) + 0;
        $size    = ( $size <= 10 ) ? 10 : ( ( $size >= 100 ) ? 100 : $size );
        $page    = ($request->getParam('page', 1)) + 0;
        $page    = ( $page < 1 ) ? 1 : $page;

        $sortBy  = $request->getParam('sortBy', 'TechID');
        $sortDir = $request->getParam('sortDir', 'asc');

        $this->view->sortBy = $sortBy;
        $this->view->sortDir = $sortDir;
        
        $search = new Core_Api_Class;

        $countRows = 0;
        $techDenied = $search->getTechsDenied(
                $this->_login.'|pmContext='.$this->view->company,
                $this->_password,
                NULL,
                (!empty($sortBy) && !empty($sortDir) ) ? "{$sortBy} {$sortDir}" : null,
                ($page-1)*$size,
                $size,
                $countRows
        );
		
        if ( $techDenied->success ) {
            $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($techDenied->data, $countRows));
            $paginator->setCurrentPageNumber($page);
            $this->view->paginator = $paginator;
        }
        $this->view->success    = $techDenied->success;
        $this->view->errors     = $techDenied->errors;
    }
}