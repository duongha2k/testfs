<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Dashboard_ReportsController extends Core_Controller_Action {
    const WO_FIELDS = 'WIN_NUM,WO_ID,DateEntered,PO,ShowTechs,SourceByDate,ShortNotice,StartDate,StartTime,EndDate,EndTime,StartRange,Duration,Region,Route,Type_ID,Project_ID,WO_Category_ID,Headline,Site_Contact_Name,SiteName,SiteNumber,Project_Name,Address,City,State,Zipcode,SitePhone,SiteFax,SiteEmail,Update_Requested,Update_Reason,Description,Requirements,Tools,SpecialInstructions,Lead,Assist,SignOffSheet_Required,ProjectManagerName,ProjectManagerPhone,ProjectManagerEmail,ResourceCoordinatorName,ResourceCoordinatorPhone,ResourceCoordinatorEmail,EmergencyName,EmergencyPhone,EmergencyEmail,TechnicalSupportName,TechnicalSupportPhone,TechnicalSupportEmail,CheckInOutName,CheckInOutNumber,CheckInOutEmail,Tech_ID,Tech_FName,Tech_LName,NoShow_Tech,BackOut_Tech,FLS_ID,BU1FLSID,BU1Info,Backup_FLS_ID,Ship_Contact_Info,PayMax,Amount_Per,Qty_Devices,Tech_Bid_Amount,WorkOrderReviewed,TechCheckedIn_24hrs,CheckedIn,Checkedby,PreCall_1,PreCall_2,CheckInNotes,StoreNotified,Notifiedby,DateNotified,NotifyNotes,TechMarkedComplete,Date_In,Time_In,Date_Out,Time_Out,ContactHD,HDPOC,TechMiles,TechComments,AttachedFile,FLS_OOS,Unexpected_Steps,Unexpected_Desc,AskedBy,AskedBy_Name,Paperwork_Received,Incomplete_Paperwork,MissingComments,Site_Complete,CallClosed,PartsProcessed,ClientComments,Work_Out_of_Scope,OutofScope_Amount,OutofScope_Reason,TripCharge,MileageReimbursement,MaterialsReimbursement,Additional_Pay_Amount,Penalty_Reason,AbortFee,AbortFeeAmount,Add_Pay_AuthBy,Add_Pay_Reason,calculatedTechHrs,baseTechPay,PayAmount,Approved,TechPaid,DatePaid,Deactivated,DeactivationCode,Deactivated_Reason,General1,General2,General3,General4,General5,General6,General7,General8,General9,General10,FS_InternalComments,Username,Invoiced,Company_Name,Company_ID,TechEmail,TechPhone,TechPhoneExt,Date_Assigned,lastModBy,lastModDate,approvedBy,DateApproved,DeactivatedDate,DeactivatedBy,Latitude,Longitude,PricingRan,WO_Category,DateIncomplete,ReminderAcceptance,Reminder24Hr,Reminder1Hr,CheckInCall,CheckOutCall,ReminderNotMarkComplete,ReminderIncomplete,ReminderAll,SMSBlast,MinutesRemainPublished,MinimumSelfRating,MaximumAllowableDistance,isProjectAutoAssign,isWorkOrdersFirstBidder,P2TPreferredOnly,P2TAlreadyAssigned,SATRecommended,SATPerformance,ReturnPartCount,NewPartCount,Qty_Applicants,DateInvoiced,Work_Out_of_Scope,OutOfScope_Reason,Date_Completed,PcntDeduct,DateIncomplete,Net_Pay_Amount,Status,PcntDeductPercent,PayAmount_Final,calculatedInvoice';
 
    private $_restrictedValue = 2;
    private $_openedValue = 1;

    public function init() {
        parent::init();

        $request = $this->getRequest();
        $auth = new Zend_Session_Namespace('Auth_User');
        $this->view->name = $auth->name;
        $this->view->company = $auth->companyId;
    }

    public function createExpenseReimbursementReportAction() {
        $params = $this->getRequest()->getParams();     
        $companyid = $params["v"];
        $filter = array('Company_ID'=>$companyid);
        $apiProjectClass = new Core_Api_ProjectClass;
        $projectList = $apiProjectClass->getAllProjects($this->_login, $this->_password,NULL, $filter);
        $this->view->projectList = $projectList;
        $this->view->companyid = $companyid;   
        $BeforeParams="";
        if(isset($params['Params'])&&!empty($params['Params']))
        {
            $BeforeParams= json_decode(base64_decode(urldecode($params['Params'])));
            
        }
      
        $this->view->Params=  $BeforeParams;        
    }

    public function expenseReimbursementReportAction() {
        $params = $this->getRequest()->getParams();
        $companyid = $params["v"];
        $this->view->params = $params;
        $this->view->companyid = $companyid;
        $this->view->ListProjectID = $params["chkProjectList"];
        $this->view->ApprovalDateFrom = (!empty($params["StartDateApprovalDateRange"])) ? date_format(new DateTime($params["StartDateApprovalDateRange"]), "Y-m-d") : "";
        $this->view->ApprovalDateTo = (!empty($params["EndDateApprovalDateRange"])) ? date_format(new DateTime($params["EndDateApprovalDateRange"]), "Y-m-d") : "";
        
        $this->view->StartDateDateFrom = (!empty($params["StartDateStartDateRange"])) ? date_format(new DateTime($params["StartDateStartDateRange"]), "Y-m-d") : "";
        $this->view->StartDateDateTo = (!empty($params["EndDateStartDateRange"])) ? date_format(new DateTime($params["EndDateStartDateRange"]), "Y-m-d") : "";
        
        $this->view->InvoiceDateFrom = (!empty($params["StartDateInvoiceDateRange"])) ? date_format(new DateTime($params["StartDateInvoiceDateRange"]), "Y-m-d") : "";
        $this->view->InvoiceDateTo = (!empty($params["EndDateInvoiceDateRange"])) ? date_format(new DateTime($params["EndDateInvoiceDateRange"]), "Y-m-d") : "";
        $this->view->WIN = $params["txtWIN"];
        $this->view->ClientWOID = $params["txtClientWOID"];
        $this->view->TechID = $params["txtFSTechID"];
        
        if (isset($params["chkShowApprovedReimbursementsOnly"]) && ($params["chkShowApprovedReimbursementsOnly"] == 1)) {
            $this->view->ShowApprovedReimbursementsOnly = true;
        }
        else
            $this->view->ShowApprovedReimbursementsOnly = false;
        
        if (isset($params["chkShowDetailbyCategory"]) && ($params["chkShowDetailbyCategory"] == 1)) {
            $this->view->ShowDetailbyCategory = true;
        }
        else
            $this->view->ShowDetailbyCategory = false;

        if (isset($params["chkShowTechClaim"]) && ($params["chkShowTechClaim"] == 1)) {
            $this->view->ShowTechClaim = true;
        }
        else
            $this->view->ShowTechClaim = false;
        if (isset($params["chkShowReimbursedthrough"]) && ($params["chkShowReimbursedthrough"] == 1)) {
            $this->view->ShowReimbursedthrough = true;
        }
        else
            $this->view->ShowReimbursedthrough = false;

        if (isset($params["chkShowReimbursedOutside"]) && ($params["chkShowReimbursedOutside"] == 1)) {
            $this->view->ShowReimbursedOutside = true;
        }
        else
            $this->view->ShowReimbursedOutside = false;
    }

    public function downloadExpenseReimbursementReportAction() {
        $objparams = $this->getRequest()->getParams();

        $params = json_decode(base64_decode($objparams['hidParams']));
        $this->view->params = $params;
        //$objPHPExcel = new PHPExcel();
        //$this->view->objPHPExcel=$objPHPExcel;
        $companyid = $params->v;
        $this->view->companyid = $companyid;
        $this->view->ListProjectID = $params->chkProjectList;
        $this->view->ApprovalDateFrom = (!empty($params->StartDateApprovalDateRange)) ? date_format(new DateTime($params->StartDateApprovalDateRange), "Y-m-d") : "";    
        $this->view->ApprovalDateTo = (!empty($params->EndDateApprovalDateRange)) ? date_format(new DateTime($params->EndDateApprovalDateRange), "Y-m-d") : "";
        
        $this->view->StartDateDateFrom = (!empty($params->StartDateStartDateRange)) ? date_format(new DateTime($params->StartDateStartDateRange), "Y-m-d") : "";
        $this->view->StartDateDateTo = (!empty($params->EndDateStartDateRange)) ? date_format(new DateTime($params->EndDateStartDateRange), "Y-m-d") : "";
        
        $this->view->InvoiceDateFrom = (!empty($params->StartDateInvoiceDateRange)) ? date_format(new DateTime($params->StartDateInvoiceDateRange), "Y-m-d") : "";
        $this->view->InvoiceDateTo = (!empty($params->EndDateInvoiceDateRange) )? date_format(new DateTime($params->EndDateInvoiceDateRange), "Y-m-d") : "";
        $this->view->WIN = $params->txtWIN;
        $this->view->ClientWOID = $params->txtClientWOID;
        $this->view->TechID = $params->txtFSTechID;  
        
        if (isset($params->chkShowApprovedReimbursementsOnly) && ($params->chkShowApprovedReimbursementsOnly == 1)) {
            $this->view->ShowApprovedReimbursementsOnly = true;
        }
        else
            $this->view->ShowApprovedReimbursementsOnly = false;
        
        if (isset($params->chkShowDetailbyCategory) && ($params->chkShowDetailbyCategory == 1)) {
            $this->view->ShowDetailbyCategory = true;
        }
        else
            $this->view->ShowDetailbyCategory = false;

        if (isset($params->chkShowTechClaim) && ($params->chkShowTechClaim == 1)) {
            $this->view->ShowTechClaim = true;
        }
        else
            $this->view->ShowTechClaim = false;
        if (isset($params->chkShowReimbursedthrough) && ($params->chkShowReimbursedthrough == 1)) {
            $this->view->ShowReimbursedthrough = true;
        }
        else
            $this->view->ShowReimbursedthrough = false;

        if (isset($params->chkShowReimbursedOutside) && ($params->chkShowReimbursedOutside == 1)) {
            $this->view->ShowReimbursedOutside = true;
        }
        else
            $this->view->ShowReimbursedOutside = false;
    }
    //--- 13440
    public function createTechUtilReportAction()
    {
        $params = $this->getRequest()->getParams();       
        $companyid = $params["v"];
        if(empty($companyid)) $companyid = $_SESSION['PM_Company_ID'];//13478

        
        
        $apiProjectClass = new Core_Api_ProjectClass;
        $filter = array('Company_ID'=>$companyid);
        $projectList = $apiProjectClass->getAllProjects($this->_login, $this->_password,
                                null,$filter);
        
        $contractorTitle = "";
        if($companyid=='FATT') $contractorTitle = "Flex Contractor ID#";
        else if($companyid=='FLS') $contractorTitle = "FLS Contractor ID#";

        
        $BeforeParams="";
        if(isset($params['Params'])&&!empty($params['Params']))
        {
            $BeforeParams= json_decode(base64_decode(urldecode($params['Params'])));
            //echo("<br/>BeforeParams: ");print_r($BeforeParams);
        }      
        
        //$api = new Core_Api_WosClass();
        //$certificationRecords = $api->getCertificationsByCompany($companyid);
        $api = new Core_Api_FSTagClass();
        $certificationRecords = $api->getFSTagList_ForCompany(0,$companyid);
        
        //--- data for view 
        $this->view->projectList = $projectList;
        $this->view->CertRecords=  $certificationRecords; 
        $this->view->companyid = $companyid;
        $this->view->ContractorTitle = $contractorTitle;
        $this->view->Params=  $BeforeParams; 
        
    }
    //--- 13440,13478
    public function techUtilReportAction()
    {
        $params = $this->getRequest()->getParams();
        $this->view->params = $params;
        //--- companyid        
        $companyid = $params["v"];
        if(empty($companyid)) $companyid = $_SESSION['PM_Company_ID'];//13478
        $this->view->companyid = $companyid;
        //--- StartDateFrom,  StartDateTo,  EndDateFrom, EndDateTo     
        $startDateFrom = (!empty($params["StartDateStartDateRange"])) ? date_format(new DateTime($params["StartDateStartDateRange"]), "Y-m-d") : "";
        $startDateTo = (!empty($params["EndDateStartDateRange"])) ? date_format(new DateTime($params["EndDateStartDateRange"]), "Y-m-d") : "";
        $endDateFrom = (!empty($params["StartDateEndDateRange"])) ? date_format(new DateTime($params["StartDateEndDateRange"]), "Y-m-d") : "";
        $endDateTo = (!empty($params["EndDateEndDateRange"])) ? date_format(new DateTime($params["EndDateEndDateRange"]), "Y-m-d") : "";
        $filterStartDateForm = $startDateFrom;
        $filterStartDateTo = $startDateTo;
        $filterEndDateForm = $endDateFrom;
        $filterEndDateTo = $endDateTo;        
        //---  ListProjectID,  TechID, ContractorID, 
        $listProjectID = $params["chkProjectList"];
        $techID = $params["txtFSTechID"];
        $contractorID = $params["txtContractorID"];
        //--- CertID List                
        $clientCredential = $params['ClientCredential'];
        $certIDList = $params["chkCertIDList"];        
        $certIDList_1="";                
        if(!empty($clientCredential)) $certIDList_1 = $clientCredential; 
        else $certIDList_1 = $certIDList; 
        
        //--- contractorTitle
        $contractorTitle = "";
        if($companyid=='FLS') $contractorTitle = "FLS Contractor ID# ";
        if($companyid=='FATT') $contractorTitle = "Flex Contractor ID# ";
        
        //--- wos
        $apiWosClass = new Core_Api_WosClass();
        $wos = $apiWosClass->getWOsWithConditions($startDateFrom,$startDateTo,
            $endDateFrom,$endDateTo,$listProjectID,$techID,
            $contractorID,$certIDList_1,$companyid
        );
        $resultTechIDArray = array();
        if(!empty($wos))
        {
            foreach($wos as $wo) $resultTechIDArray[]=$wo['Tech_ID'];
        }
        //--- resultTechs
        $resultTechs = $apiWosClass->getTechs_ByTechIDsArray($resultTechIDArray,$companyid);

        //--- client available hours
        $clientStartDate = $startDateFrom;
        $clientEndDate = $endDateTo;
        if(!empty($wos))
        {
            $minStartDate = '2100-01-01';
            $maxEndDate = '1900-01-01';
            foreach($wos as $wo)
            {
                if( $minStartDate > $wo['StartDate']) $minStartDate = $wo['StartDate'];
                if( $maxEndDate < $wo['EndDate']) $maxEndDate = $wo['EndDate'];           
            }            
            if(empty($clientStartDate)) $clientStartDate=$minStartDate;
            if(empty($clientEndDate)) $clientEndDate=$maxEndDate;
        }
        $clientAvailHours = $apiWosClass->getAvailHours($clientStartDate,$clientEndDate);
        //echo("<br/>clientStartDate: $clientStartDate, clientEndDate: $clientEndDate; clientAvailHours: $clientAvailHours");
        
        //---
        if(!empty($resultTechs) && !empty($wos))
        {            
            for($i=0;$i<count($resultTechs);$i++)
            {   
                $resultTechID = $resultTechs[$i]['TechID'];
                //echo("<br/>resultTechID: $resultTechID");
                //--- HourFilled
                $hourFilled = 0;
                foreach($wos as $wo)
                {
                    $woTechID = $wo['Tech_ID'];
                    if($resultTechID==$woTechID)
                    {
                        $techWorkHours = $apiWosClass->getWorkHours_ByWOInfo($wo);
                        $hourFilled += $techWorkHours;                        
                    }

                }
                $resultTechs[$i]['HourFilled']=$hourFilled;
                //--- all wos for tech
                if(empty($startDateFrom)) 
                {
                    $startDateFrom=$clientStartDate;
                }
                if(empty($endDateTo))
                {
                    $endDateTo = $clientEndDate;                    
                }                
                
                $techWos = $apiWosClass->getAllWOs_ByTechID($startDateFrom,$startDateTo,
                    $endDateFrom,$endDateTo,$resultTechID);
                //echo("<br/>techWos: ");print_r($techWos);
                //--- FSTotalHourFilled    
                $FSTotalHourFilled = 0;    
                foreach($techWos as $techWO)
                {
                    $techWorkHours = $apiWosClass->getWorkHours_ByWOInfo($techWO);
                    $FSTotalHourFilled += $techWorkHours;                    
                }
                $resultTechs[$i]['FSTotalHourFilled']=$FSTotalHourFilled;
                //$resultTechs[$i]['FSTotalHourFilled']=0;
            }
        }
        
//echo("<br/>techs:<pre>");print_r($resultTechs);        
//echo("<br/>wos:<pre>");print_r($wos);
        //--- data for view
        if(!empty($filterStartDateForm)) $filterStartDateForm = date("m/d/y",strtotime($filterStartDateForm));
        if(!empty($filterStartDateTo)) $filterStartDateTo = date("m/d/y",strtotime($filterStartDateTo));
        if(!empty($filterEndDateForm)) $filterEndDateForm = date("m/d/y",strtotime($filterEndDateForm));
        if(!empty($filterEndDateTo)) $filterEndDateTo = date("m/d/y",strtotime($filterEndDateTo));
        
        $this->view->techs = $resultTechs;
        $this->view->contractorTitle = $contractorTitle;
        $this->view->companyid = $companyid;
        $this->view->clientAvailHours = $clientAvailHours;
        $this->view->filterStartDateForm = $filterStartDateForm;
        $this->view->filterStartDateTo = $filterStartDateTo;
        $this->view->filterEndDateForm = $filterEndDateForm;
        $this->view->filterEndDateTo = $filterEndDateTo;
    }
    
    public function downloadTechUtilReportAction() {
        $objparams = $this->getRequest()->getParams();
        $params = json_decode(base64_decode($objparams['hidParams']));
        $this->view->params = $params;
        
        //--- companyid        
        $companyid = $params->v;
        if(empty($companyid)) $companyid = $_SESSION['PM_Company_ID'];//13478
        $this->view->companyid = $companyid;
        //--- StartDateFrom,  StartDateTo, EndDateFrom, EndDateTo       
        $startDateFrom = (!empty($params->StartDateStartDateRange)) ? date_format(new DateTime($params->StartDateStartDateRange), "Y-m-d") : "";
        $startDateTo = (!empty($params->EndDateStartDateRange)) ? date_format(new DateTime($params->EndDateStartDateRange), "Y-m-d") : "";
        $endDateFrom = (!empty($params->StartDateEndDateRange)) ? date_format(new DateTime($params->StartDateEndDateRange), "Y-m-d") : "";
        $endDateTo = (!empty($params->EndDateEndDateRange)) ? date_format(new DateTime($params->EndDateEndDateRange), "Y-m-d") : "";
        $filterStartDateForm = $startDateFrom;
        $filterStartDateTo = $startDateTo;
        $filterEndDateForm = $endDateFrom;
        $filterEndDateTo = $endDateTo;        
        //---  ListProjectID, TechID, ContractorID, ClientCredential              
        $listProjectID = $params->chkProjectList;
        $techID = $params->txtFSTechID;
        $contractorID = $params->txtContractorID;        
        //--- CertID List        
        $clientCredential = $params->ClientCredential;         
        $certIDList = $params->chkCertIDList;        
        $certIDList_1="";                
        if(!empty($clientCredential)) $certIDList_1 = $clientCredential; 
        else $certIDList_1 = $certIDList; 

        //--- contractorTitle
        $contractorTitle = "";
        if($companyid=='FLS') $contractorTitle = "FLS Contractor ID# ";
        if($companyid=='FATT') $contractorTitle = "Flex Contractor ID# ";
        
        //--- wos
        $apiWosClass = new Core_Api_WosClass();
        $wos = $apiWosClass->getWOsWithConditions($startDateFrom,$startDateTo,
            $endDateFrom,$endDateTo,$listProjectID,$techID,
            $contractorID,$certIDList_1,$companyid
        );
        $resultTechIDArray = array();
        if(!empty($wos))
        {
            foreach($wos as $wo) $resultTechIDArray[]=$wo['Tech_ID'];
        }
        //--- resultTechs
        $resultTechs = $apiWosClass->getTechs_ByTechIDsArray($resultTechIDArray,$companyid);

        //--- client available hours
        $clientStartDate = $startDateFrom;
        $clientEndDate = $endDateTo;
        if(!empty($wos))
        {
            $minStartDate = '2100-01-01';
            $maxEndDate = '1900-01-01';
            foreach($wos as $wo)
            {
                if( $minStartDate > $wo['StartDate']) $minStartDate = $wo['StartDate'];
                if( $maxEndDate < $wo['EndDate']) $maxEndDate = $wo['EndDate'];           
            }            
            if(empty($clientStartDate)) $clientStartDate=$minStartDate;
            if(empty($clientEndDate)) $clientEndDate=$maxEndDate;
        }
        $clientAvailHours = $apiWosClass->getAvailHours($clientStartDate,$clientEndDate);
        //echo("<br/>clientStartDate: $clientStartDate, clientEndDate: $clientEndDate; clientAvailHours: $clientAvailHours");
        
        //---
        if(!empty($resultTechs) && !empty($wos))
        {            
            for($i=0;$i<count($resultTechs);$i++)
            {   
                $resultTechID = $resultTechs[$i]['TechID'];
                //echo("<br/>resultTechID: $resultTechID");
                //--- HourFilled
                $hourFilled = 0;
                foreach($wos as $wo)
                {
                    $woTechID = $wo['Tech_ID'];
                    if($resultTechID==$woTechID)
                    {
                        $techWorkHours = $apiWosClass->getWorkHours_ByWOInfo($wo);
                        $hourFilled += $techWorkHours;                        
                    }

                }
                $resultTechs[$i]['HourFilled']=$hourFilled;
                //--- all wos for tech
                if(empty($startDateFrom)) 
                {
                    $startDateFrom=$clientStartDate;
                }
                if(empty($endDateTo))
                {
                    $endDateTo = $clientEndDate;                    
                }                
                
                $techWos = $apiWosClass->getAllWOs_ByTechID($startDateFrom,$startDateTo,
                    $endDateFrom,$endDateTo,$resultTechID);
                //echo("<br/>techWos: ");print_r($techWos);
                //--- FSTotalHourFilled    
                $FSTotalHourFilled = 0;    
                foreach($techWos as $techWO)
                {
                    $techWorkHours = $apiWosClass->getWorkHours_ByWOInfo($techWO);
                    $FSTotalHourFilled += $techWorkHours;                    
                }
                $resultTechs[$i]['FSTotalHourFilled']=$FSTotalHourFilled;
                //$resultTechs[$i]['FSTotalHourFilled']=0;
            }
        }
        
//echo("<br/>wos:<pre>");print_r($wos);
        //--- data for view
        if(!empty($filterStartDateForm)) $filterStartDateForm = date("m/d/y",strtotime($filterStartDateForm));
        if(!empty($filterStartDateTo)) $filterStartDateTo = date("m/d/y",strtotime($filterStartDateTo));
        if(!empty($filterEndDateForm)) $filterEndDateForm = date("m/d/y",strtotime($filterEndDateForm));
        if(!empty($filterEndDateTo)) $filterEndDateTo = date("m/d/y",strtotime($filterEndDateTo));
        
        $this->view->techs = $resultTechs;
        $this->view->contractorTitle = $contractorTitle;
        $this->view->companyid = $companyid;
        $this->view->clientAvailHours = $clientAvailHours;
        $this->view->filterStartDateForm = $filterStartDateForm;
        $this->view->filterStartDateTo = $filterStartDateTo;
        $this->view->filterEndDateForm = $filterEndDateForm;
        $this->view->filterEndDateTo = $filterEndDateTo;
    }
    
    //--- 13190
    public function createProjectMasterReportAction()
    {
        $params = $this->getRequest()->getParams();       
        $companyid = $params["v"];
                
        $apiProjectClass = new Core_Api_ProjectClass;
        $filter = array('Company_ID'=>$companyid);
        $projectList = $apiProjectClass->getAllProjects($this->_login, $this->_password,
                                null,$filter);        
        //--- data for view 
        $this->view->projectList = $projectList;
        $this->view->companyid = $companyid;        
    }
    
    //--- 13190
    public function downloadProjectMasterReportAction() 
    {
        //--- get data
        $params = $this->getRequest()->getParams();
        $companyID = $params["CompanyID"];
        $createdDateFrom = (!empty($params["StartDateCreatedDateRange"])) ? date_format(new DateTime($params["StartDateCreatedDateRange"]), "Y-m-d") : "";
        $createdDateTo = (!empty($params["EndDateCreatedDateRange"])) ? date_format(new DateTime($params["EndDateCreatedDateRange"]), "Y-m-d") : "";
        $startDateFrom = (!empty($params["StartDateStartDateRange"])) ? date_format(new DateTime($params["StartDateStartDateRange"]), "Y-m-d") : "";
        $startDateTo = (!empty($params["EndDateStartDateRange"])) ? date_format(new DateTime($params["EndDateStartDateRange"]), "Y-m-d") : "";
        $endDateFrom = (!empty($params["StartDateEndDateRange"])) ? date_format(new DateTime($params["StartDateEndDateRange"]), "Y-m-d") : "";
        $endDateTo = (!empty($params["EndDateEndDateRange"])) ? date_format(new DateTime($params["EndDateEndDateRange"]), "Y-m-d") : "";
        $approvedDateFrom = (!empty($params["StartDateApprovedDateRange"])) ? date_format(new DateTime($params["StartDateApprovedDateRange"]), "Y-m-d") : "";
        $approvedDateTo = (!empty($params["EndDateApprovedDateRange"])) ? date_format(new DateTime($params["EndDateApprovedDateRange"]), "Y-m-d") : "";
        $invoicedDateFrom = (!empty($params["StartDateInvoicedDateRange"])) ? date_format(new DateTime($params["StartDateInvoicedDateRange"]), "Y-m-d") : "";
        $invoicedDateTo = (!empty($params["EndDateInvoicedDateRange"])) ? date_format(new DateTime($params["EndDateInvoicedDateRange"]), "Y-m-d") : "";
        $listProjectID = $params["chkProjectList"];
		$geography = array ();
		$win_array = array ();

		$WoTagClass = new Core_Api_WoTagClass();

		//--- get wos
        $apiWosClass = new Core_Api_WosClass();
        $wofieldsArray = split(",",self::WO_FIELDS);
        $wos = $apiWosClass->getWOsWithConditions($startDateFrom,$startDateTo,
            $endDateFrom,$endDateTo,$listProjectID,'','','',$companyID,
            $wofieldsArray,0,0,0,$createdDateFrom,$createdDateTo,$approvedDateFrom,$approvedDateTo,$invoicedDateFrom,$invoicedDateTo
        );
        //--- wos additional fields
        $apiCoreFiles = new Core_Files();

		for ($i = 0; $i < count ($wos); $i++) {
			$geography[$wos[$i]["Zipcode"]] = "";
			$win_Array[] = $wos[$i]["WIN_NUM"];
		}

		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from ("zipcode_sitetypes");
		$select->where ("ZipCode IN (?)", array_keys ($geography));
		$records = Core_Database::fetchAll ($select);
		foreach ($records as $record) {
			$geography[$record["ZipCode"]] = trim ($record["SiteType"]);
		}

		$getPartInfo_old = $apiWosClass->getPartInfo ($win_Array, 0);
		$getPartInfo_new = $apiWosClass->getPartInfo ($win_Array, 1);
		$getPartEntries = $apiWosClass->getPartEntries ($win_Array);
		$getPartsInfo_old = $apiWosClass->getPartsInfo ($win_Array, 0);
		$getPartsInfo_new = $apiWosClass->getPartsInfo ($win_Array, 1);

		$baseWOVisitInfo = $apiWosClass->getBaseWOVisitInfo_ByWinnum ($win_Array, false);
		$numOfVisits = $apiWosClass->getNumOfVisits ($win_Array);
		$durationOnSite = $apiWosClass->getTotatDurationOnSite ($win_Array);
		$fileCounts = $apiCoreFiles->getFilesCount ($win_Array, Core_Files::TECH_FILE);

        for($i=0;$i<count($wos);$i++)
        {
            //--- Lead,Assist
            $wo_id = $wos[$i]['WIN_NUM'];
            $lead = $wos[$i]['Lead'];
            $assist = $wos[$i]['Assist'];
            $techRole = $assist==1? 'Assist':'';

            if($lead==1)
            {
                if(!empty($techRole)) $techRole.=',';
                $techRole .= 'Lead';                
            }
            //--- Device Qunatity
            if($wos[$i]['Amount_Per']=='Visit') $wos[$i]['Qty_Devices'] = 0;
            
            //--- Part Info
            //$numOfParts = $apiWosClass->getNumOfParts($wo_id);
            $numOfParts = isset ($getPartEntries[$wo_id]) ? count ($getPartEntries[$wo_id]) : 0;

            $wos[$i]['NumberOfParts'] = $numOfParts;

            //$newPart = $apiWosClass->getPartInfo($wo_id,1);
            $newPart = $getPartInfo_new[$wo_id];

            $wos[$i]['NewPartsCarrier'] = $newPart['Carrier'];
            $wos[$i]['NewPartsTrackingNum'] = $newPart['TrackingNum'];
            $newPartETA = $newPart['ETA'];
            if(!empty($newPartETA)) $newPartETA = date("Y-m-d",strtotime($newPartETA));
            $wos[$i]['NewPartsETA'] = $newPartETA;
            
            //$returnedOrOldPart = $apiWosClass->getPartInfo($wo_id,0);
            $returnedOrOldPart = $getPartInfo_old[$wo_id];
            $wos[$i]['ReturnOrOldPartsCarrier'] = $returnedOrOldPart['Carrier'];
            $wos[$i]['ReturnOrOldPartsTrackingNum'] = $returnedOrOldPart['TrackingNum'];
            
            //$partEntries = $apiWosClass->getPartEntries($wo_id);
            $partEntries = $getPartEntries[$wo_id];
            //$newParts = $apiWosClass->getPartsInfo($wo_id,1);
            $newParts = $getPartsInfo_new[$wo_id];
            //$returnedOrOldParts = $apiWosClass->getPartsInfo($wo_id,0);
            $returnedOrOldParts = $getPartsInfo_old[$wo_id];
            $wos[$i]['ArrayPartEntries'] = $partEntries;
            $wos[$i]['ArrayNewParts'] = $newParts;
            $wos[$i]['ArrayReturnOrOldParts'] = $returnedOrOldParts;
            
            //--- ActualStartDate, ActualStartTime, DepartureDate, DepartureTime
            //$baseVisit = $apiWosClass->getBaseWOVisitInfo_ByWinnum($wo_id, true);
			$baseVisit = $baseWOVisitInfo[$wo_id];
            if(!empty($baseVisit))
            {
                if($baseVisit['OptionCheckInOut']==1) //tech
                {
                    $wos[$i]['ActualStartDate'] = $baseVisit['TechCheckInDate'];
                    $wos[$i]['ActualStartTime'] = $baseVisit['TechCheckInTime'];
                    $wos[$i]['DepartureDate'] = $baseVisit['TechCheckOutDate'];
                    $wos[$i]['DepartureTime'] = $baseVisit['TechCheckOutTime']; 
                }
                else if($baseVisit['OptionCheckInOut']==2) //client
                {
                    $wos[$i]['ActualStartDate'] = $baseVisit['CORCheckInDate'];
                    $wos[$i]['ActualStartTime'] = $baseVisit['CORCheckInTime'];
                    $wos[$i]['DepartureDate'] = $baseVisit['CORCheckOutDate'];
                    $wos[$i]['DepartureTime'] = $baseVisit['CORCheckOutTime'];
                }
            }
            
            //--- others
            $wos[$i]['TotalHrsOnSite'] = $durationOnSite[$wo_id];
            $wos[$i]['NumOfVisits'] = $numOfVisits[$wo_id];
            $wos[$i]['TechRole'] = $techRole;
            $wos[$i]['NumofTechDocs'] = $fileCounts[$wo_id];
        }   
        //--- max of number of parts
        $maxNumOfParts = 0;
        for($i=0;$i<count($wos);$i++)
        {
            $numOfParts = $wos[$i]['NumberOfParts'];
            if($numOfParts > $maxNumOfParts) $maxNumOfParts = $numOfParts;
		}
        //$this->_helper->viewRenderer->setNoRender();//test

		$this->view->getCompletionStatusForWin = $WoTagClass->getCompletionStatusForWin ($win_Array);

		$this->view->OnTimePerformceList_forWOs = $WoTagClass->GetOnTimePerformceList_forWO($win_Array);

        //--- send data to view        
        $this->view->CompanyID = $companyID;
        $this->view->Wos = $wos;
        $this->view->MaxNumOfParts = $maxNumOfParts;
		$this->view->Geography = $geography;
    }
    //--- 13534
    public function createUpdateRequestReportAction()
    {
        $params = $this->getRequest()->getParams();       
        $companyid = $params["v"];
        if(empty($companyid)) $companyid = $_SESSION['PM_Company_ID'];//13478

        
        
        $apiProjectClass = new Core_Api_ProjectClass;
        $filter = array('Company_ID'=>$companyid);
        $projectList = $apiProjectClass->getAllProjects($this->_login, $this->_password,
                                null,$filter);
        
        $BeforeParams="";
        if(isset($params['Params'])&&!empty($params['Params']))
        {
            $BeforeParams= json_decode(base64_decode(urldecode($params['Params'])));
            //echo("<br/>BeforeParams: ");print_r($BeforeParams);
        }      
        
        $api = new Core_Api_WosClass();
        
        //--- data for view 
        $this->view->projectList = $projectList;
        $this->view->companyid = $companyid;
        $this->view->Params=  $BeforeParams; 
        
    }
    //--- 13534
    public function updateRequestReportAction()
    {
        $params = $this->getRequest()->getParams();
        if(!empty($params["parames"])){
            $params = (array)json_decode($_SESSION["UpdateRequestReportParames"]);
        }
        $this->view->params = $params;
        //--- companyid        
        $companyid = $params["v"];
        if(empty($companyid)) $companyid = $_SESSION['PM_Company_ID'];
        $this->view->companyid = $companyid;
        //--- StartDateFrom,  StartDateTo,  RequestDateFrom, RequestDateTo     
        $startDateFrom = (!empty($params["StartDateStartDateRange"])) ? date_format(new DateTime($params["StartDateStartDateRange"]), "Y-m-d") : "";
        $startDateTo = (!empty($params["EndDateStartDateRange"])) ? date_format(new DateTime($params["EndDateStartDateRange"]), "Y-m-d") : "";
        $requestDateFrom = (!empty($params["StartDateRequestDateRange"])) ? date_format(new DateTime($params["StartDateRequestDateRange"]), "Y-m-d") : "";
        if(!empty($requestDateFrom)) $requestDateFrom = $requestDateFrom." 00:00:01";
        $requestDateTo = (!empty($params["EndDateRequestDateRange"])) ? date_format(new DateTime($params["EndDateRequestDateRange"]), "Y-m-d") : "";
        if(!empty($requestDateTo)) $requestDateTo = $requestDateTo." 23:59:59";
        $filterStartDateForm = $startDateFrom;
        $filterStartDateTo = $startDateTo;
        $filterRequestDateForm = $requestDateFrom;
        $filterRequestDateTo = $requestDateTo;        
        //---  ListProjectID,  
        $listProjectID = $params["chkProjectList"];    
            
        //--- select from timestamsp
        $timeStamp = API_Timestamp::getTimestamp_forUpdateRequest("",$requestDateFrom,$requestDateTo,$startDateFrom,$startDateTo,$listProjectID);

        //--- data for view
        if(!empty($filterStartDateForm)) $filterStartDateForm = date("m/d/y",strtotime($filterStartDateForm));
        if(!empty($filterStartDateTo)) $filterStartDateTo = date("m/d/y",strtotime($filterStartDateTo));
        if(!empty($filterRequestDateForm)) $filterRequestDateForm = date("m/d/y",strtotime($filterRequestDateForm));
        if(!empty($filterRequestDateTo)) $filterRequestDateTo = date("m/d/y",strtotime($filterRequestDateTo));
        
        $this->view->wos = $timeStamp;
        $this->view->companyid = $companyid;
        $this->view->filterStartDateForm = $filterStartDateForm;
        $this->view->filterStartDateTo = $filterStartDateTo;
        $this->view->filterRequestDateForm = $filterRequestDateForm;
        $this->view->filterRequestDateTo = $filterRequestDateTo;
    }
    
    public function downloadUpdateRequestReportAction() 
    {
        $objparams = $this->getRequest()->getParams();
        $params = json_decode(base64_decode($objparams['hidParams']));
        $this->view->params = $params;
        
        //--- companyid        
        $companyid = $params->v;
        if(empty($companyid)) $companyid = $_SESSION['PM_Company_ID'];
        $this->view->companyid = $companyid;
        
        //--- StartDateFrom,  StartDateTo, EndDateFrom, EndDateTo       
        $startDateFrom = (!empty($params->StartDateStartDateRange)) ? date_format(new DateTime($params->StartDateStartDateRange), "Y-m-d") : "";
        $startDateTo = (!empty($params->EndDateStartDateRange)) ? date_format(new DateTime($params->EndDateStartDateRange), "Y-m-d") : "";
        
        $requestDateFrom = (!empty($params->StartDateRequestDateRange)) ? date_format(new DateTime($params->StartDateRequestDateRange), "Y-m-d") : "";
        if(!empty($requestDateFrom)) $requestDateFrom = $requestDateFrom." 00:00:01";

        $requestDateTo = (!empty($params->EndDateRequestDateRange)) ? date_format(new DateTime($params->EndDateRequestDateRange), "Y-m-d") : "";
        if(!empty($requestDateTo)) $requestDateTo = $requestDateTo." 23:59:59";

        
        $filterStartDateForm = $startDateFrom;
        $filterStartDateTo = $startDateTo;
        $filterRequestDateForm = $requestDateFrom;
        $filterRequestDateTo = $requestDateTo;       
         
        //---  ListProjectID
        $listProjectID = $params->chkProjectList;
        
        //--- select from timestamsp
        $timeStamp = API_Timestamp::getTimestamp_forUpdateRequest("",$requestDateFrom,$requestDateTo,$startDateFrom,$startDateTo,$listProjectID);

        
        //--- data for view
        if(!empty($filterStartDateForm)) $filterStartDateForm = date("m/d/y",strtotime($filterStartDateForm));
        if(!empty($filterStartDateTo)) $filterStartDateTo = date("m/d/y",strtotime($filterStartDateTo));
        
        if(!empty($filterRequestDateForm)) $filterRequestDateForm = date("m/d/y",strtotime($filterRequestDateForm));
        if(!empty($filterRequestDateTo)) $filterRequestDateTo = date("m/d/y",strtotime($filterRequestDateTo));
        
        $this->view->wos = $timeStamp;
        $this->view->companyid = $companyid;
        $this->view->filterStartDateForm = $filterStartDateForm;
        $this->view->filterStartDateTo = $filterStartDateTo;
        $this->view->filterRequestDateForm = $filterRequestDateForm;
        $this->view->filterRequestDateTo = $filterRequestDateTo;        
    }
    //14019
     function downloadSiteSurveyAction(){ 
          $params = $this->getRequest()->getParams();     
        $companyid = $params["v"];
        //downloadSite
        $_SESSION['downloadsurveyreport']=array("status"=>"pending");
     
  
  
     // $companyID = 'FLEX';
       
        $coreWosClass = new Core_Api_WosClass();
        $records = $coreWosClass->getSiteSurveyReport($companyid);
        
        $this->view->records=$records;
         
        
       
             
       $this->view->filename = "SiteSurveyReport";                
            $this->render('download-site-survey');
        
    }
    //14019
    
    function downloadSiteStatusAction(){
        //downloadSiteStatus
        $_SESSION['downloadstatus']=array("status"=>"pending");
        $params = $this->getRequest()->getParams();
        $WoTagClass = new Core_Api_WoTagClass();
        $fromStartDate = $params["fromStartDate"];
        $toStartDate = $params["toStartDate"];
        
        $techMarkedCompleteDateGreater= $params["fromMarkedComplete"];
        $techMarkedCompleteDateLess= $params["toMarkedComplete"];
        $approvedDateGreater= $params["fromDateApproved"];
        $approvedDateLess= $params["toDateApproved"];
        $invoicedDateGreater= $params["fromDateInvoiced"];
        $invoicedDateLess= $params["toDateInvoiced"];
        
        $fromStartDate = !empty($fromStartDate)?date_format(new DateTime($fromStartDate), "Y-m-d"):"";
        $toStartDate = !empty($toStartDate)?date_format(new DateTime($toStartDate), "Y-m-d"):"";
        
        $techMarkedCompleteDateGreater= !empty($techMarkedCompleteDateGreater)?date_format(new DateTime($techMarkedCompleteDateGreater), "Y-m-d"):"";
        $techMarkedCompleteDateLess= !empty($techMarkedCompleteDateLess)?date_format(new DateTime($techMarkedCompleteDateLess), "Y-m-d"):"";
        $approvedDateGreater= !empty($approvedDateGreater)?date_format(new DateTime($approvedDateGreater), "Y-m-d"):"";
        $approvedDateLess= !empty($approvedDateLess)?date_format(new DateTime($approvedDateLess), "Y-m-d"):"";
        $invoicedDateGreater= !empty($invoicedDateGreater)?date_format(new DateTime($invoicedDateGreater), "Y-m-d"):"";
        $invoicedDateLess= !empty($invoicedDateLess)?date_format(new DateTime($invoicedDateLess), "Y-m-d"):"";
        
        $this->view->fromStartDate = $fromStartDate;
        $this->view->toStartDate = $toStartDate;
        $project = new Core_Api_ProjectClass();
        //587
        if(!empty($params["Project_ID"]) && is_array($params["Project_ID"]))
        {
            $projectID=$params["Project_ID"][0];
        }
        else
        {
            $projectID=$params["Project_ID"];
        }
        //end 857 
        $projectinfo = $project->getProject($projectID);
        $companyID=$projectinfo['Project_Company_ID'];
        $this->view->companyID=$companyID;
        
        $companyID=$projectinfo['Project_Company_ID'];
        $this->view->companyID=$companyID;
        if($params['downloadSiteStatus'] == 3)
        {
            $this->view->highLevelProjectStatusReport = $WoTagClass->highLevelProjectStatusReport(
                    $params["Project_ID"],
                    $fromStartDate,$toStartDate,
                    $techMarkedCompleteDateGreater,$techMarkedCompleteDateLess,
                    $approvedDateGreater,$approvedDateLess,
                    $invoicedDateGreater,$invoicedDateLess);
            $this->render('download-site-status-highlevel');
        } else if($params['downloadSiteStatus'] == 2) {
            //896
            $this->view->filename = "SLAStatusReport";
            //end 896
            $detailSiteStatusReport = $WoTagClass->detailSiteStatusReport(
                            $params["Project_ID"],"",
                            $fromStartDate,$toStartDate,
                            $techMarkedCompleteDateGreater,
                            $techMarkedCompleteDateLess,
                            $approvedDateGreater,$approvedDateLess,
                            $invoicedDateGreater,$invoicedDateLess);
            $this->view->detailSiteStatusReport = $detailSiteStatusReport; 
            $this->render('download-site-status-detailed');
            
        } else if($params['downloadSiteStatus'] == 1) {
            $this->view->filename = "SiteSLAStatusReport";
            $detailSiteStatusReport = $WoTagClass->detailSiteStatusReport(
                            $params["Project_ID"],
                            $params["SiteNumber"]
                            ,$fromStartDate,$toStartDate);

            $this->view->detailSiteStatusReport = $detailSiteStatusReport;
                            
            $this->render('download-site-status-detailed');
        }
    }
    
    function getdownloadstatusAction()
    {
        
        $this->_helper->viewRenderer->setNoRender();
        session_start();
        if(!isset($_SESSION['downloadstatus'])){
            $_SESSION['downloadstatus']=array("status"=>"pending");
        } 
        $downloadstatus = $_SESSION['downloadstatus'];
        if($downloadstatus['status']=='finished'){
            unset($_SESSION['downloadstatus']);
        }
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($downloadstatus);
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                ->setBody($return);
    }
    //14019
    function getdownloadsurveyreportAction()
    {
        
         
        $this->_helper->viewRenderer->setNoRender();
        session_start();
        if(!isset($_SESSION['downloadsurveyreport'])){
            $_SESSION['downloadsurveyreport']=array("status"=>"pending");
        } 
        $downloadstatus = $_SESSION['downloadsurveyreport'];
        if($downloadstatus['status']=='finished'){
            unset($_SESSION['downloadsurveyreport']);
        }
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($downloadstatus);
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                ->setBody($return);  
    }
    //14019
}

?>
