<?php

class Dashboard_CustomerController extends Core_Controller_Action {

    const FIELDS = 'WIN_NUM,WO_ID,DateEntered,PO,ShowTechs,SourceByDate,ShortNotice,StartDate,StartTime,EndDate,EndTime,StartRange,Duration,Region,Type_ID,Project_ID,WO_Category_ID,Headline,Site_Contact_Name,SiteName,SiteNumber,Project_Name,Address,City,State,Zipcode,SitePhone,SiteFax,SiteEmail,Update_Requested,Update_Reason,Description,Requirements,Tools,SpecialInstructions,Lead,Assist,SignOffSheet_Required,ProjectManagerName,ProjectManagerPhone,ProjectManagerEmail,ResourceCoordinatorName,ResourceCoordinatorPhone,ResourceCoordinatorEmail,EmergencyName,EmergencyPhone,EmergencyEmail,TechnicalSupportName,TechnicalSupportPhone,TechnicalSupportEmail,CheckInOutName,CheckInOutNumber,CheckInOutEmail,Tech_ID,Tech_FName,Tech_LName,NoShow_Tech,BackOut_Tech,FLS_ID,BU1FLSID,BU1Info,Backup_FLS_ID,Ship_Contact_Info,PayMax,Amount_Per,Qty_Devices,Tech_Bid_Amount,WorkOrderReviewed,TechCheckedIn_24hrs,CheckedIn,Checkedby,PreCall_1,PreCall_2,CheckInNotes,StoreNotified,Notifiedby,DateNotified,NotifyNotes,TechMarkedComplete,Date_In,Time_In,Date_Out,Time_Out,ContactHD,HDPOC,TechMiles,TechComments,AttachedFile,FLS_OOS,Unexpected_Steps,Unexpected_Desc,AskedBy,AskedBy_Name,Paperwork_Received,Incomplete_Paperwork,MissingComments,Site_Complete,CallClosed,PartsProcessed,ClientComments,Work_Out_of_Scope,OutofScope_Amount,OutofScope_Reason,TripCharge,MileageReimbursement,MaterialsReimbursement,Additional_Pay_Amount,Penalty_Reason,AbortFee,AbortFeeAmount,Add_Pay_AuthBy,Add_Pay_Reason,calculatedTechHrs,baseTechPay,PayAmount,Approved,TechPaid,DatePaid,Deactivated,DeactivationCode,Deactivated_Reason,General1,General2,General3,General4,General5,General6,General7,General8,General9,General10,FS_InternalComments,Username,Invoiced,Company_Name,Company_ID,TechEmail,TechPhone,TechPhoneExt,Date_Assigned,lastModBy,lastModDate,approvedBy,DateApproved,DeactivatedDate,DeactivatedBy,Latitude,Longitude,PricingRan,WO_Category,DateIncomplete,ReminderAcceptance,Reminder24Hr,Reminder1Hr,CheckInCall,CheckOutCall,ReminderNotMarkComplete,ReminderIncomplete,ReminderAll,SMSBlast,MinutesRemainPublished,MinimumSelfRating,MaximumAllowableDistance,isProjectAutoAssign,isWorkOrdersFirstBidder,P2TPreferredOnly,P2TAlreadyAssigned,SATRecommended,SATPerformance,ReturnPartCount,NewPartCount,Qty_Applicants,Status,DateInvoiced,Work_Out_of_Scope,OutOfScope_Reason,Status,PcntDeduct,Route';

    private $_size;
    private $_page;

    public function init()
    {
        parent::init();

        $request = $this->getRequest();

       //This is constroller can be used for AJAX only!
        if ( !$this->getRequest()->isXmlHttpRequest() ) {
//            $this->_redirect('/');
        }
        $this->_size = ($request->getParam('size', empty($_COOKIE['page_size']) ? 25 : $_COOKIE['page_size'] )) + 0;
//        $this->_size = ( $this->_size <= 10 ) ? 10 : ( ( $this->_size >= 100 ) ? 100 : $this->_size );
 $this->_sortStr = $this->_assembleSortStr(array(
            's1' => $request->getParam('sort1', NULL), 'd1' => $request->getParam('dir1', NULL),
            's2' => $request->getParam('sort2', NULL), 'd2' => $request->getParam('dir2', NULL),
            's3' => $request->getParam('sort3', NULL), 'd3' => $request->getParam('dir3', NULL)
     ));
        if ($request->getParam('size', null) != null)
        {
            $domain = '.' . preg_replace('/^www\./', '', $_SERVER['HTTP_HOST']);
            setcookie("page_size", $this->_size, time() + 3600 * 24 * 30, "/", $domain);
        }

        $this->_page = ($request->getParam('page', 1)) + 0;
        $this->_page = ( $this->_page < 1 ) ? 1 : $this->_page;

        $this->view->pageCurrent = $this->_page;
        $this->view->pageSize = $this->_size;

        $this->view->name = $auth->name;
        $this->view->company = $auth->companyId;
		$this->view->customer = $auth->customerId;
    }
	
	public function updateAction()
    {
        $request   = $this->getRequest();

        $user = new Core_Api_CustomerUser();
        $customerInfo = $user->load(
            array(
                'login' => $this->_login,
                'password' => $this->_password
            )
        );

        //$customer = new Core_Api_CustomerClass();

        $this->view->customer = $customerInfo;
        $this->view->success = $customerInfo !== false;
        //$search = new Core_Api_AdminClass;
       

        $errors = Core_Api_Error::getInstance();
        
        $this->view->messages = $error->message;
    }

	public function doUpdateAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $return = array('success' => 0);
        $needUpdate = true;
        
        $user = new Core_Api_CustomerUser();
        
        $formFields = $this->getRequest()->getParam('UpdateForm', array());

        if (!empty($formFields['PasswordCurrent']))
        {
            $customerInfo = $user->load(
                array(
                    'login' => $this->_login,
                    'password' => $formFields['PasswordCurrent']
                )
            );
            $cid = $customerInfo->customerId;
            if (empty($cid) ) {
                // wrong password confirmation
                $return['success']=0;
                $needUpdate = false;
                $return['errors'][] = 'Wrong Password';
            }
            
            if (empty($formFields['Password']) || $formFields['Password'] != $formFields['PasswordConfirm']) {
                $return['success']=0;
                $needUpdate = false;
                if (empty($return['errors'])) $return['errors'] = array();
                $return['errors'][] = 'Incorrect Password Confirmation';
            }
        }
        
        if (empty($formFields['PasswordCurrent']) 
            || empty($formFields['Password'])
            || empty($formFields['PasswordConfirm'])
            || $formFields['Password'] != $formFields['PasswordConfirm']
        ) {
            // dont update password fields
            unset($formFields['PasswordCurrent']);
            unset($formFields['Password']); 
            unset($formFields['PasswordConfirm']);    
        }
        
        $allowedToUpdate = array(
			'Contact_Name',
			'Contact_Email',
			'Contact_Phone',    
            'Customer_Name',
            'Password'
        );
        
        $updateFields = array();
        foreach ($formFields as $key => $value) {
            if (in_array($key, $allowedToUpdate)) {
                $updateFields[$key] = $value;
            }
        }
        

        $search = new Core_Api_CustomerClass();
        if ($needUpdate) {
		try {
            $result = $search->updateCustomerProfile($this->_login, $this->_password, $updateFields);
		} catch (Exception $e) { echo $e; };
            $errors = Core_Api_Error::getInstance();
            if( !$result->success ) {
                foreach ( $result->errors as $error ) {
                    $return['success'] = 0;
                    $errorMessages->addMessage($error->message);
                }
            } else {
                if (!empty($updateFields['Password']) ) {
                    //change strored in session pass
                    $auth = new Zend_Session_Namespace('Auth_User');
                    $auth->password = $updateFields['Password'];
                }
                
                $return['success']=1;
            }
        }
       
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-Length', strlen($return), true);
        $response->setBody($return);
    }
	
    public function createAction()
    {
        $request = $this->getRequest();
        $search = new Core_Api_CustomerClass();

        $this->view->success = 1;
        $this->view->errors  = null;
        $this->view->customerid = $request->getParam('company');
        $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');

        // projects list
        /*
        $cacheKeyPrj = Core_Cache_Manager::makeKey('projects', $this->view->company, '', $this->_login);
        if (FALSE == ($projects = $cache->load($cacheKeyPrj)))
        {
            $projects = $search->getAllProjects(
                    $this->_login, $this->_password
            );
            if ($projects->success)
            {
                $cache->save($projects, $cacheKeyPrj, array('prj'), LIFETIME_15MIN);
            }
        }
        */

        $projects = $search->getAllProjectsForCustomer(
                    $this->_login, $this->_password
            );
            
        $this->view->projectsSuccess = $projects->success;
        if ($projects->success)
            $this->view->projects = $projects->data;

        // call types list
        $callTypes = $search->getCallTypes();
        $this->view->callTypesSuccess = $callTypes->success;
        if ($callTypes->success)
            $this->view->callTypes = $callTypes->data;

        // states list
        $states = $search->getStates();
        $this->view->statesSuccess = $states->success;
        if ($states->success)
            $this->view->states = $states->data;

        // categories list
        $categories = $search->getWOCategories($this->_login, $this->_password);
        $this->view->categoriesSuccess = $categories->success;
        if ($categories->success)
            $this->view->categories = $categories->data;
    }

    public function doCreateAction()
    {
        $companyId = $this->view->company;

        $this->_helper->viewRenderer->setNoRender();

        $params = $this->getRequest()->getParams();
        $errors = array();
        if ($_SESSION['UserType'] == 'Install Desk' || $_SESSION['UserType']=='Manager') {
        	$params['Project_ID'] = 3149;
        	$params['Type_ID'] = 1;

        	$errors = $this->validateWOCreationByFLSManager($params);
        }

        if (!count($errors)) {
        	$search = new Core_Api_CustomerClass;

        	$return = array('success'=>0);

        	$wo = new API_WorkOrder(NULL);

        	$className = get_class($wo);
        	foreach ($params as $key => $value) {
        		if(property_exists($className, $key)) $wo->$key = $value;
        	}

        	//Special handler for checkboxes.
        	$checkboxes = array('ShowTechs','Update_Requested','Lead','Assist','SignOffSheet_Required','WorkOrderReviewed','TechCheckedIn_24hrs','CheckedIn','StoreNotified','Paperwork_Received','Incomplete_Paperwork','TechMarkedComplete','CallClosed','Approved','Deactivated','PartsProcessed','AbortFee','ShortNotice', 'PcntDeduct');
        	foreach ($checkboxes as $c) {
        		if (!property_exists($className, $c))
        		continue;

        		$wo->$c = isset($params[$c]);
        	}

        	//Special handler for Times
        	$times = array('StartTime','EndTime');
        	foreach ($times as $key) {
        		if (!empty($params[$key]) && property_exists($className,$key)) {
        			$wo->$key = strtoupper($params[$key]);
        		}
        	}

        	//Special handlers for virtual fields
        	if (isset($params['MarkIncompleteV'])) {
        		$wo->TechMarkedComplete = false;
        	}
        	$apiResponse = $search->createWorkOrder($this->_login, $this->_password, isset($params['ShowTechs']), $wo,true);
        	if (empty($apiResponse->errors)) {
                    
                    $apicus = new Core_Api_CustomerClass();
                    $CustomerQTInfo = $apicus->getCustomerQTInfo($params['customerid']);
                    if(!empty($CustomerQTInfo))
                    {
                        $QTmail = new Core_EmailForCreateQuickticketWO();
                        $QTmail->setWorkOrder($apiResponse->data[0]->WIN_NUM);
                        $QTmail->setClientName($CustomerQTInfo['Contact_Name']);
                        $QTmail->setToMail($CustomerQTInfo['ClientEmail']);
                        $QTmail->setCompanyName($CustomerQTInfo['CompanyName']);
                        
                        if(!empty($wo->ResourceCoordinatorEmail))
                        {
                            $QTmail->setCustomerName($CustomerQTInfo['Customer_Name']);
                            $QTmail->setToMailRC($wo->ResourceCoordinatorEmail);
                            $QTmail->setToNameRC($wo->ResourceCoordinatorName);
                        }
                        $QTmail->sendMail();
                    }
                    
                    //end 330
                        //
        		//update tech status
        		$status = 0;
        		if (!empty($params['PreferTechV'])) {
        			$status = 1;
        		}elseif(!empty($params['DenyTechV'])) {
        			$status = 2;
        		}
        		$return['success'] = 1;
        		$return['win'] = $apiResponse->data[0]->WIN_NUM;
        	} else {
        		$return['success'] = 0;
        		$return['errors'] = array();
        		foreach ($apiResponse->errors as $e) {
        			$return['errors'][]=$e->message;
        		}
        	}
        } else {
        	$return['success'] = 0;
        	$return['errors'] = $errors;
        }
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);



        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                 ->setBody($return);
    }

    private function validateWOCreationByFLSManager($params)
    {
    	$errors = array();

    	$requiredFields = array('WO_ID' => 'Client Work Order ID#',
    	'SiteName' => 'Site Name',
            'Country' => 'Country',
    	'Address' => 'Site Address',
    	'City' => 'City',
    	'State' => 'State',
		'Zipcode' => 'Zip or Postal Code',
    	'StartDate' => 'Start Date',
    	'StartTime' => 'StartTime',
    	'Duration' => 'Duration',
            'SitePhone' => 'SitePhone',
    	'Description' => 'Work Description'
    	);

    	foreach ($requiredFields as $field => $name) {
    		if (!isset($params[$field]) || !trim($params[$field])) {
    			$errors[] = $name . ' empty but required';
    		}
    	}

    	return $errors;
    }

    public function projectDocumentsAction()
    {
        $search = new Core_Api_Class;
        $company = $this->getRequest()->getParam('company',NULL);
        $project  = $this->getRequest()->getParam('id',null);
        if ($project) {
            $project = $search->getProjects($this->_login.'|pmContext='.$company,$this->_password,array($project),null);
        }
        $this->view->project = $project;
    }


    public function ajaxUploadFileAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $company = $this->view->company;

        $maxFileSize = 52428800; //50Mb

        $request = $this->getRequest();

        $win   = $request->getParam('win', NULL);
        $id    = $request->getParam('file', NULL);
        $descr = $request->getParam('descr', NULL);
        $descr = trim($descr);

        $return = array('success'=>0);
        if (
            !empty($id)                                                 &&
            !empty($_FILES["Pic{$id}"])                        &&
            is_uploaded_file($_FILES["Pic{$id}"]['tmp_name'])  &&
            is_readable($_FILES["Pic{$id}"]['tmp_name'])       &&
            $_FILES["Pic{$id}"]['error'] == UPLOAD_ERR_OK      &&
            filesize($_FILES["Pic{$id}"]['tmp_name']) <= $maxFileSize &&
            filesize($_FILES["Pic{$id}"]['tmp_name']) > 0 &&
            mb_strlen($descr, 'utf-8') <= 25
        ) {
            $uploader = new Core_Api_CustomerClass();
            $type  = (strpos($id, 'Tech') !== false) ? Core_Files::TECH_FILE : Core_Files::WOS_FILE;
            $fname = $_FILES["Pic{$id}"]['name'];
            if ( strrpos($fname, '/') !== FALSE ) {
                $fname = substr($fname, strrpos($fname, '/'));
            }

            /**
             * Generate unig name for file
             * @author Artem Sukharev
             * @see https://tickets.fieldsolutions.com/issues/12180
             */
            $info = pathinfo($fname);
            $fname = uniqid($info['filename'].'_');
            $fname = $fname . '.' . $info['extension'];

            $fdata     = file_get_contents($_FILES["Pic{$id}"]['tmp_name']);
            $result = $uploader->uploadFileAWS($this->_login . "|pmContext=$company", $this->_password, $win, $fname, $fdata, $type, $descr);
            if ( !$result->success ) {
                $return['success'] = 0;
                $return['errors'] = array();
                foreach ($result->errors as $e) {
                    $return['errors'][]=$e->message;
                }
            } else {
                $return['success']=1;
                $return['id']=$result->data;
            }
        } else {
            $return['success']=0;
            $return['errors']=array();
            if ( empty($id) ) {
                $return['errors'][]='Incorrect parameters: ID is required and cannot be empty';
            } else if ( empty($_FILES["Pic{$id}"]) ) {
                $return['errors'][]= 'File not uploaded';
            } else if ( !is_uploaded_file($_FILES["Pic{$id}"]['tmp_name']) ) {
                $return['errors'][]= 'File not uploaded.';
            } else if ( !is_readable($_FILES["Pic{$id}"]['tmp_name']) ) {
                $return['errors'][]= 'Uploaded file not readabale';
            } else if ( filesize($_FILES["Pic{$id}"]['tmp_name']) > $maxFileSize ) {
                $return['errors'][]= 'Uploaded file size too big. Please call resource coordinator for assistance.';
            } else if ( filesize($_FILES["Pic{$id}"]['tmp_name']) == 0 ) {
                $return['errors'][]= 'Uploaded file is empty.';
            } else if ( $_FILES["Pic{$id}"]['error'] != UPLOAD_ERR_OK ) {
                switch ( $_FILES["Pic{$id}"]['error'] ) {
                    case UPLOAD_ERR_FORM_SIZE :
                        $return['errors'][]= 'Uploaded file size too big. Please call resource coordinator for assistance.';
                        break;
                    case UPLOAD_ERR_PARTIAL :
                        $return['errors'][]= 'The uploaded file was only partially uploaded';
                        break;
                    case UPLOAD_ERR_NO_FILE :
                    case UPLOAD_ERR_NO_TMP_DIR :
                    case UPLOAD_ERR_CANT_WRITE :
                    case UPLOAD_ERR_EXTENSION :
                        $return['errors'][]= 'No file was uploaded';
                }
            } else if(mb_strlen($descr, 'utf-8') >25){
                $return['errors'][]= 'Optional description for uploaded file should containes less then 25 characters.';
            }
            
            error_log('FileUploading: (client:AUFA, win:' . $win . ',id: ' . $id . ') ' . implode('|', $return['errors']));
        }
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-Length', strlen($return), true);
        $response->setBody($return);
    }

    /**
     * Project Info Action
     *
     * @return json with project info
     * @author Alex Scherba
     */
    public function projectInfoAction() {
        $this->_helper->viewRenderer->setNoRender();
        $id = $this->getRequest()->getParam('id',NULL);
        $company = $this->view->company;
        if ($id) {
            $search = new Core_Api_CustomerClass();
            $projectInfo = $search->getProjects($this->_login,$this->_password,array((int)$id),null);
            if ($projectInfo->success) {
                $projectInfo = $projectInfo->data[0];
            }else{
                $projectInfo = null;
            }
        }else{
            $projectInfo = null;
        }

        //filter Project Info
        $return = array();
        $rename_array = array(
            'Project_ID' => 'Project_ID',
            'Project_Name' => 'Project_Name',
            'Project_Manager' => 'ProjectManagerName',
            'Project_Manager_Email' => 'ProjectManagerEmail',
            'Project_Manager_Phone' => 'ProjectManagerPhone',
            'Resource_Coordinator' => 'ResourceCoordinatorName',
            'Resource_Coord_Email' => 'ResourceCoordinatorEmail',
            'Resource_Coord_Phone' => 'ResourceCoordinatorPhone',
            'Qty_Devices'=>'Qty_Devices',
            'Amount_Per'=>'Amount_Per',
            'Default_Amount'=>'PayMax',
            'Description'=>'Description',
            'Requirements'=>'Requirements',
            'Tools'=>'Tools',
            'SpecialInstructions'=>'SpecialInstructions',
            'AutoBlastOnPublish'=>'AutoBlastOnPublish',
            'RecruitmentFromEmail'=>'RecruitmentFromEmail',
            'AutoSMSOnPublish'=>'AutoSMSOnPublish',
            'WO_Category_ID'=>'WO_Category_ID',
            'Headline'=>'Headline',
            'CheckInOutEmail'=>'CheckInOutEmail',
            'CheckInOutName'=>'CheckInOutName',
            'CheckInOutNumber'=>'CheckInOutNumber',
            'EmergencyName'=>'EmergencyName',
            'EmergencyPhone'=>'EmergencyPhone',
            'EmergencyEmail'=>'EmergencyEmail',
            'TechnicalSupportName'=>'TechnicalSupportName',
            'TechnicalSupportPhone'=>'TechnicalSupportPhone',
            'TechnicalSupportEmail'=>'TechnicalSupportEmail'
        );
        if ($projectInfo) {
            foreach ($rename_array as $key=>$value) {
                $return[$value] = $projectInfo->$key;
            }
        }
        if ($return['Tools']) $return['Requirements'] .= "\n" . $return['Tools']; //By CR #12563
        unset($return['Tools']);

        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                 ->setBody($return);
    }
	public function customerInfoAction() {
        $this->_helper->viewRenderer->setNoRender();
        $id = $this->getRequest()->getParam('id',NULL);
        $customer = $this->view->customer;
        if ($id) {
            $search = new Core_Api_CustomerClass();
            $customerInfo = $search->getCustomer($this->_login,$this->_password,array((int)$id),null);
            if ($customerInfo->success) {
                $customerInfo = $customerInfo->data[0];
            }else{
                $customerInfo = null;
            }
        }else{
            $customerInfo = null;
        }

        //filter Customer Info
        $return = array();
        $rename_array = array(
            'UNID' => 'Customer_ID',
            'Customer_Name' => 'Customer_Name',
            'Contact_Name' => 'Contact_Name',
            'Contact_Email' => 'Contact_Email',
            'Contact_Phone' => 'Contact_Phone',
            'Username' => 'Username',
            'Password' => 'Password'
        );
        if ($projectInfo) {
            foreach ($rename_array as $key=>$value) {
                $return[$value] = $projectInfo->$key;
            }
        }

        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                 ->setBody($return);
    }
	public function userCreateAction()
    {
		$this->_helper->layout->disableLayout();
		$this->getHelper('viewRenderer')->setNoRender();
		
        $company_id = $this->getRequest()->getParam('Company_ID', null);
		
		$errorMessages = new Core_Form_ErrorHandler( 'createCustomer' ); 
		
		$this->view->Company_ID = ( !empty($company_id) ) ? $company_id : $this->view->company;
		
        $this->view->messages = $errorMessages->getMessages();
        
        
        $errorMessages->clearMessages();
		
		
    }

    public function doUserCreateAction()
    {
        $errorMessages = new Core_Form_ErrorHandler( 'createCustomer' );
        $request       = $this->getRequest();
        $createForm    = $request->getParam('CreateForm', null);

        if(empty($createForm)) $errorMessages->addMessage( 'Create Customer Form is empty' );
        $errorMessages->clearMessages();

        $customerClass = new Core_Api_CustomerClass();
        $result = $customerClass->createCustomer($this->_login.'|pmContext='.$this->_company, $this->_password, $createForm, $this->_company);

        if( !$result->success ) {
            foreach ( $result->errors as $error ) $errorMessages->addMessage($error->message);
        } else {
            $id = $result->data[0]->CustomerID;
            if ( !empty($_FILES) ){
				$msgs = array();
				if ( !empty($msgs) ) {
					$msgs = array_unique($msgs);
					foreach ( $msgs as $msg ) $errorMessages->addMessage($msg);
				}
            }
        }
        $err = $errorMessages->getMessages();
        if (empty($err))
            $this->_redirect('../clients/usersView.php?v='.$createForm['Company_ID']);
        $this->_redirect($_SERVER['HTTP_REFERER']);
    }

    public function userListAction()
    {
        $request = $this->getRequest();
        $params  = $request->getParams();

        $size    = ($request->getParam('size', 10)) + 0;
        $size    = ( $size <= 10 ) ? 10 : ( ( $size >= 100 ) ? 100 : $size );
        $page    = ($request->getParam('page', 1)) + 0;
        $page    = ( $page < 1 ) ? 1 : $page;

        $sortBy  = $request->getParam('sortBy', 'UserName');
        $sortDir = $request->getParam('sortDir', 'asc');

        $this->view->sortBy = $sortBy;
        $this->view->sortDir = $sortDir;

        $company_id = $this->getRequest()->getParam('Company_ID', null);
        $this->view->Company_ID = ( !empty($company_id) ) ? $company_id : $auth->companyId;
        
        $search = new Core_Api_Class;

        $countRows = 0;
        $clients = $search->getClientsList(
                $this->_login,
                $this->_password,
                $params,
                (!empty($sortBy) && !empty($sortDir) ) ? "{$sortBy} {$sortDir}" : null,
                ($page-1)*$size,
                $size,
                $countRows
        );

        if ( $clients->success ) {
            $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($clients->data, $countRows));
            $paginator->setCurrentPageNumber($page);
            $this->view->paginator = $paginator;
        }
        $this->view->success    = $clients->success;
        $this->view->errors     = $clients->errors;
    }


    public function userUpdateAction()
    {
        $customer_id = $this->getRequest()->getParam('id', null);
        $user      = new Core_Api_CustomerUser($customer_id);
        $client    = $user->loadById($customer_id);

        $errorMessages = new Core_Form_ErrorHandler( 'updateCustomer_'.$customer_id );

        $this->view->customer  = $customer;
        $this->view->success = $customer !== false;

        $search  = new Core_Api_CustomerClass;

        $info = $user->load(
            array(
                'login' => $this->_login.'|pmContext='.$this->_company,
                'password' => $this->_password
            )
        );

        $errors = Core_Api_Error::getInstance();
        $er = $errors->getErrorArray();
        foreach ( $er as $k=>$v ) $errorMessages->addMessage($error->message);

        $this->view->messages = $errorMessages->getMessages();
        $errorMessages->clearMessages();
    }

    public function doUserUpdateAction()
    {
        $updateForm    = $this->getRequest()->getParam('UpdateForm', null);
        $id            = $this->getRequest()->getParam('ClientID', null);
        $removefiles   = $this->getRequest()->getParam('removefiles', null);
		$this->_company = $this->getRequest()->getParam('company', null);
        $errorMessages = new Core_Form_ErrorHandler( 'updateClient_'.$id );
        $errorMessages->clearMessages();

        if(!empty($removefiles)) foreach($removefiles as $rmf) $updateForm[$rmf] = '';

        if(empty($updateForm) || empty($id)) $errorMessages->addMessage( 'Update Client Form is empty' );
        $er = $errorMessages->getMessages();
        if(empty($er)) {
            $search = new Core_Api_Class;
            $checkboxes = array('AccountEnabled','Admin');
            foreach ($checkboxes as $c) $updateForm[$c] = isset($updateForm[$c])?1:0;

            $result = $search->updateClient($this->_login, $this->_password, $id, $updateForm);
            
            $errors = Core_Api_Error::getInstance();
            if( !$result->success ) {
                foreach ( $result->errors as $error ) {
                    $errorMessages->addMessage($error->message);
                }
            } else {
                if ( !empty($_FILES) ){
                $msgs = array();

                foreach ( $_FILES as $k => $fileStruc ) {
                    if (empty($fileStruc['tmp_name']) ) continue;
                    if ( $fileStruc['error'] == UPLOAD_ERR_OK &&
                        is_uploaded_file($fileStruc['tmp_name']) &&
                        filesize($fileStruc['tmp_name']) > 0
                    ) {
                        $fname = $fileStruc['name'];
                        if ( strrpos($fname,'/') !== FALSE ) {
                            $fname = substr($fname, strrpos($fname,'/'));
                        }
                        $info  = pathinfo($fname);
                        $fname = uniqid($info['filename'].'_');
                        $fname = $fname.'.'.$info['extension'];
                        $fdata = file_get_contents($fileStruc['tmp_name']);

                        $result = $search->uploadClientLogo($this->_login,$this->_password,$id,$fname,$fdata,$k);
                        
                        if ( !$result->success ) $msgs[] = $result->errors[0]->message;
                    } else {
                        if ( $fileStruc['error'] != UPLOAD_ERR_NO_FILE ) {
                            $errorMessages->addMessage('File "'.$fileStruc['name'].'" upload failed');
                        } else if ( !is_uploaded_file($fileStruc['tmp_name']) ) {
                            $errorMessages->addMessage('File "'.$fileStruc['name'].'" not uploaded.');
                        } else if ( filesize($fileStruc['tmp_name']) == 0 ) {
                            $errorMessages->addMessage('Uploaded "'.$fileStruc['name'].'" file is empty.');
                        }
                        $errorsDetails = $errorMessages->getMessages();
                        error_log('FileUploading: (admin:fWUA, client:'.$id.') '.implode('|',$errorsDetails));
                    }
                }

                if(!empty($removefiles)) {
                    $files = new Core_File();
                    foreach($removefiles as $rmf) {
                        $files->removeFile($result->data[0]->$rmf, S3_CLIENTS_LOGO_DIR);
                    }
                }
                if ( !empty($msgs) ) {
                    $msgs = array_unique($msgs);
                    foreach ( $msgs as $msg ) $errorMessages->addMessage($msg);
                }
                }
            }
        }
        $err = $errorMessages->getMessages();
        if (empty($err))
            $this->_redirect('../clients/usersView.php?v=' . $this->_company);
        $this->_redirect($_SERVER['HTTP_REFERER']);      
    }

    public function addContactAction () {
        $request = $this->getRequest();

        $this->view->Company_ID = $request->getParam('Company_ID',null);
        $this->view->Created_By = $request->getParam('Created_By',null);
        $this->view->WorkOrder_ID = $request->getParam('WorkOrder_ID',null);
        $this->view->email = $request->getParam('email',null);
        $this->view->techName = $request->getParam('techName',null);
        $this->view->fromEmail = $request->getParam('fromEmail',null);
        $this->view->messages = array();

        $addContactForm = $request->getParam('addContactForm',null);
        if(!empty($addContactForm)){
            $this->_helper->viewRenderer->setNoRender();
            $return = array();
            $return['success'] = 1;
        	$return['errors'] = '';

            $clientClass = new Core_Api_Class();
            $res = $clientClass->addContact($this->_login, $this->_password, $addContactForm, $request->getParam('fromEmail',null), $request->getParam('copy',null));
            if(!$res->success) {
                foreach ($res->errors as $error) $return['errors'] = $error->message;
                $return['success'] = 0;
            }else{
                $copy = $request->getParam('Copy',null);
                if($copy) {

                }
            }
            $response = $this->getResponse();
            $response->clearAllHeaders()->clearBody();
            $return = json_encode($return);
            $response->setHeader('Content-type', 'application/json');
            $response->setHeader('Content-Length', strlen($return), true)
                     ->setBody($return);
        }

    }
    public function viewContactLogAction () {
        $request = $this->getRequest();

        $this->view->Company_ID = $request->getParam('Company_ID',null);
        $this->view->WorkOrder_ID = $request->getParam('win',null);

        $params  = array('Company_ID'=>$this->view->Company_ID, 'WorkOrder_ID'=>$this->view->WorkOrder_ID);

        $size    = ($request->getParam('size', 10)) + 0;
        $size    = ( $size <= 10 ) ? 10 : ( ( $size >= 100 ) ? 100 : $size );
        $page    = ($request->getParam('page', 1)) + 0;
        $page    = ( $page < 1 ) ? 1 : $page;

        $sortBy  = $request->getParam('sortBy', 'Created_Date');
        $sortDir = $request->getParam('sortDir', 'asc');

        $this->view->sortBy = $sortBy;
        $this->view->sortDir = $sortDir;

        $search = new Core_Api_Class;

        $countRows = 0;
        $log = $search->getContactLog(
                $this->_login,
                $this->_password,
                $params,
                (!empty($sortBy) && !empty($sortDir) ) ? "{$sortBy} {$sortDir}" : null,
                ($page-1)*$size,
                $size,
                $countRows
        );

        if ( $log->success ) {
            $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($log->data, $countRows));
            $paginator->setCurrentPageNumber($page);
            $this->view->paginator = $paginator;
        }
        $this->view->success    = $log->success;
        $this->view->errors     = $log->errors;
    }

    public function quickviewAction()
    {
        $authData = array('login' => $user, 'password' => $pass);
        $user = new Core_Api_CustomerUser();
        $customerInfo = $user->checkAuthentication(array(
            'login' => $this->_login,
            'password' => $this->_password
                ));
        $params = $this->getRequest()->getParams();
        $request = $this->getRequest();
        
        $countRows = 0;

        $filters = new API_WorkOrderFilter;
        $filters->Company_ID = $params['company'];

        if($params['tab']=='quickviewopen')
        {
            $filters->WO_State =  "open";
        }
        else
        {
            $filters->WO_State = "close";
        }

        $projects = array();
        if ( NULL !== ($projects = $request->getParam('projects', NULL)) ) {
            if ( is_string($projects) && strpos($projects, '|', 1) ) {
                $projects = explode('|', $projects);
                $request->setParam('projects', $projects);
            }
        }

        if(empty($projects[0])){
            $Core_Api_CustomerClass = new Core_Api_CustomerClass;
            $projectList = $Core_Api_CustomerClass->getAllProjectsForCustomer(
                    $this->_login, $this->_password
            );
            $j = 0;
            if (!empty($projectList->data)) {
                for ($i = 0; $i < sizeof($projectList->data); ++$i){
                    $project = $projectList->data[$i];
                    if ($project->Active){
                        $projects[$j] = $project->Project_ID;
                        $j++;
                    }
                }
            }
        }
        $startdate = $request->getParam('start_date', NULL);
        $enddate = $request->getParam('end_date', NULL);
		if(empty($startdate ) ||  is_null ($startdate )){
			$startdate_format = "";
		}else{
				$startdate_format = date('Y-m-d', strtotime(str_replace('_', '/', $startdate)));
		}
		if(empty($enddate ) ||  is_null ($enddate )){
			$enddate_format = "";
		}else{
				$enddate_format = date('Y-m-d', strtotime(str_replace('_', '/', $enddate)));
		}
		//End 791
		
		
        $filters->TB_UNID = $request->getParam('win', NULL);
        $filters->WO_ID = $request->getParam('client_wo', NULL);
        $filters->PO = $request->getParam('po', NULL);
        $filters->StartDateFrom = $startdate_format;// !empty($startdate)?date_format(new DateTime($startdate), "Y-m-d"):"";
        $filters->StartDateTo = $enddate_format;//!empty($enddate)?date_format(new DateTime($enddate), "Y-m-d"):"";
        $filters->City = $request->getParam('city', NULL);
        $filters->State = $request->getParam('state', NULL);
        $filters->Country = $request->getParam('country', NULL);
        $filters->Zipcode = $request->getParam('zip', NULL);
        $filters->SiteName = $request->getParam('site_name', NULL);
        $filters->SiteNumber = $request->getParam('site_number', NULL);
        $filters->Tech_ID = $request->getParam('tech', NULL);
        $filters->Contractor_ID = $request->getParam('contractor', NULL);
        $filters->Tech_FName = $request->getParam('tech_fname', NULL);
        $filters->Tech_LName = $request->getParam('tech_lname', NULL);
        $filters->Project_ID    = ( is_array($projects) ) ? ( (count($projects) > 1) ? $projects : $projects[0] ) : $projects;
        $tech_confirmed     = $request->getParam('tech_confirmed', NULL);
        $filters->TechCheckedIn_24hrs    = ( NULL === $tech_confirmed  || $tech_confirmed=='') ? NULL : ( ( $tech_confirmed + 0 ) ? true : false );
        $checked_in         = $request->getParam('checked_in', NULL);
        $filters->CheckedIn      = ( NULL === $checked_in || $checked_in=='') ? NULL : ( ( $checked_in + 0 ) ? true : false );
        $filters->CheckedOut      = $request->getParam('checked_out', NULL);
        //13892
        $filters->StandardOffset = $request->getParam('StandardOffset', NULL);
        //end 13892
        $resultquickview = API_WorkOrderFilter::filter($params['company'], self::FIELDS, '', '', '', $filters, $this->_sortStr, $this->_size, ($this->_page - 1) * $this->_size, $countRows);
        $wo = API_WorkOrder::makeWorkOrderArray($resultquickview->toArray(), new API_WorkOrder(NULL));

        $techs = array();
		
        //$checkedOut = $request->getParam('checked_out', NULL);
        $wosClass = new Core_Api_WosClass();
    
        foreach ($wo as $_wo)
        {
        	
            $techs[] = (defined('FLSVersion')) ? $_wo->FLS_ID : $_wo->Tech_ID;
            
		}

        $tech = new API_Tech;
		if ($params['company'] != "FATT")
			$techs = $tech->lookupID(array_unique($techs));
		else
			$techs = $tech->lookupID(array_unique($techs), null, false, 22);	//Include cert number for Flex Contractor ID (cert id 22)
		
	
        $paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($wo, $countRows));
        $paginator->setItemCountPerPage($this->_size);
        $paginator->setCurrentPageNumber($this->_page);
        $this->view->paginator = $paginator;
        $this->view->tab=$params['tab'];
        $this->view->techs = $techs;
        $this->view->success = true;
        $this->view->errors = $wo->errors;
        $this->view->company = $params['company'];
        $this->view->files = new Core_Files();

        // Save workorders into CSV file
        if ($params['download'] === 'csv')
        {
            $offset = 0;
            $limit = 50000;//13928
            $resultquickview = API_WorkOrderFilter::filter($params['company'], self::FIELDS, '', '', '', $filters, $this->_sortStr,$limit, $offset);
            $wo = API_WorkOrder::makeWorkOrderArray($resultquickview->toArray(), new API_WorkOrder(NULL));

			$CSapi = new Core_Api_CustomerClass();
			$QTV = $CSapi->getQTV_ByCustomerID($_SESSION['CustomerID']);
            try
            {
                $csvAdapter = new Core_CSV_Adapter_Customer(
                $this->_login,
                $this->_password,
                $this->_filters,
                self::FIELDS,
                $this->_sortStr,
                    $params['companyid'],
                    $wo,
					$QTV['ShowTechPayDetails'] == 1
                );
                    //public function __construct($partName, $tabName, $adapter, $isFLS = false, $isFLSManager = false, $extendedTechData = false)
                $csvExport = new Core_CSV_Builder('customer', 'quickview', $csvAdapter, defined('FLSVersion'), $this->view->isFLSManager, true);
                
                return $this->_outputCsv($csvExport);
            } catch (Core_CSV_Exception $e)
            {
                print $e;
                print 'error occurred';
                return false;
            }
        }
    }

    public function section1Action()
    {
        $benchmark = Core_Benchmark::getInstance('Start benchmark on sections pop-ups');
        //  This is constroller can be used for AJAX only!
        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest())
        {
            $this->_redirect('/');
        }

        //  Check authentication
        $authData = array('login' => $user, 'password' => $pass);
        $user = new Core_Api_CustomerUser();
        $customerInfo = $user->checkAuthentication(array(
            'login' => $this->_login,
            'password' => $this->_password
                ));

        session_write_close();

        if (!$customerInfo)
        {
            throw new Core_Auth_Exception('Authentication required');
        }
        //  Check authentication end block

        $request = $this->getRequest();
        $win = $request->getParam('win', false);
        $companyId = $request->getParam('companyId', false);
        $this->_companyId = $companyId;
        $fieldList = Core_Database::getFieldList(Core_Database::TABLE_WORK_ORDERS);
        $fieldList = array_combine($fieldList, $fieldList);

        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS, $fieldList)
                ->where("WIN_NUM = ?", $win)
                ->where("Company_ID = ?", $companyId);

        $result = Core_Database::fetchAll($select);

        //$calltype = [];
        if(!empty($result[0]))
        {
            switch($result[0]['Type_ID'])    
             {
                case 1:
                    $result[0]['Type_ID'] = 'Install';
                    break;
                case 2:
                    $result[0]['Type_ID'] = 'Service Call';
                    break;
                case 3:
                    $result[0]['Type_ID'] = 'Recruiting';
                    break;
                default:
                    $result[0]['Type_ID'] = '';
                    break;
            }
            $woapi = new API_WOCategory();
            $cat = $woapi->getWOCategoryById($result[0]['WO_Category_ID']);
            $result[0]['WO_Category_ID']  = $cat['Category'];
            
//            $fieldsList = array('Category_ID','Category');
//            $db = Core_Database::getInstance();
//            $select = $db->select();
//            $select->from(Core_Database::TABLE_WO_CATEGORIES, $fieldsList);
//            $Categoriesresult = Core_Database::fetchAll($select);
//            if(!empty($Categoriesresult))
//            {
//                switch($result[0]['WO_Category_ID'])    
//             {
//                case $Categoriesresult['Category_ID']:
//                    $result[0]['WO_Category_ID'] = 'Install';
//                    break;
//                case 2:
//                    $result[0]['WO_Category_ID'] = 'Service Call';
//                    break;
//                case 3:
//                    $result[0]['WO_Category_ID'] = 'Recruiting';
//                    break;
//                default:
//                    $result[0]['WO_Category_ID'] = '';
//                    break;
//            }
//            }
        }
        $this->view->wo = $result[0];
        $this->view->success = true;
        $this->view->popup = 'yes';
        $this->view->win = $win;
        $this->view->companyId = $companyId;
        
        
    }

    public function startTimeAction()
    {
        $benchmark = Core_Benchmark::getInstance('Start benchmark on sections pop-ups');
        //  This is constroller can be used for AJAX only!
        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest())
        {
            $this->_redirect('/');
        }

        //  Check authentication
        $authData = array('login' => $user, 'password' => $pass);
        $user = new Core_Api_CustomerUser();
        $customerInfo = $user->checkAuthentication(array(
            'login' => $this->_login,
            'password' => $this->_password
                ));

        session_write_close();

        if (!$customerInfo)
        {
            throw new Core_Auth_Exception('Authentication required');
        }
        //  Check authentication end block

        $request = $this->getRequest();
        $win = $request->getParam('win', false);
        $companyId = $request->getParam('companyId', false);
        $this->_companyId = $companyId;
        $fieldList = Core_Database::getFieldList(Core_Database::TABLE_WORK_ORDERS);
        $fieldList = array_combine($fieldList, $fieldList);

        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS, $fieldList)
                ->where("WIN_NUM = ?", $win)
                ->where("Company_ID = ?", $companyId);

        $result = Core_Database::fetchAll($select);

        $this->view->wo = $result[0];
        $this->view->success = true;
        $this->view->popup = 'yes';
        $this->view->win = $win;
        $this->view->companyId = $companyId;
    }

    public function techInfoAction()
    {
        require_once 'Core/Api/Class.php';

        $request = $this->getRequest();
        $techid = $request->getParam('tech', NULL);
        $companyId = $request->getParam('companyId', false);
        $search = new Core_Api_Class;
        //  Check authentication
        $authData = array('login' => $user, 'password' => $pass);
        $user = new Core_Api_CustomerUser();
        $customerInfo = $user->checkAuthentication(array(
            'login' => $this->_login,
            'password' => $this->_password
                ));

        session_write_close();

        if (!$customerInfo)
        {
            throw new Core_Auth_Exception('Authentication required');
        }
        $tech = new API_Tech();

        $tech->lookupID($techid, $companyId);

        $tech->PrimaryPhoneStatus = 1;
        $tech->SecondaryPhoneStatus = 1;
        $api = new Core_Api_TechClass;
        $result = $api->getExts($tech->TechID);

        if (!empty($result['TechID']))
        {
            $tech->PrimaryPhoneStatus = $result['PrimaryPhoneStatus'];
            $tech->SecondaryPhoneStatus = $result['SecondaryPhoneStatus'];
        }
        $this->view->tech = $tech;
        $this->view->success = true;
    }

    public function techcommentAction()
    {
        $benchmark = Core_Benchmark::getInstance('Start benchmark on sections pop-ups');
        //  This is constroller can be used for AJAX only!
        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest())
        {
            $this->_redirect('/');
        }

        //  Check authentication
        $authData = array('login' => $user, 'password' => $pass);
        $user = new Core_Api_CustomerUser();
        $customerInfo = $user->checkAuthentication(array(
            'login' => $this->_login,
            'password' => $this->_password
                ));

        session_write_close();

        if (!$customerInfo)
        {
            throw new Core_Auth_Exception('Authentication required');
        }
        //  Check authentication end block

        $request = $this->getRequest();
        $win = $request->getParam('win', false);
        $companyId = $request->getParam('companyId', false);
        $this->_companyId = $companyId;
        $fieldList = Core_Database::getFieldList(Core_Database::TABLE_WORK_ORDERS);
        $fieldList = array_combine($fieldList, $fieldList);

        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(Core_Database::TABLE_WORK_ORDERS, $fieldList)
                ->where("WIN_NUM = ?", $win)
                ->where("Company_ID = ?", $companyId);

        $result = Core_Database::fetchAll($select);

        $this->view->wo = $result[0];
        $this->view->success = true;
        $this->view->popup = 'yes';
        $this->view->win = $win;
        $this->view->companyId = $companyId;
    }

    public function techDocumentsAction()
    {
        $request = $this->getRequest();
        $win = $request->getParam('win', false);

        $files = new Core_Files();
        $select = $files->select();

        $select->where('WIN_NUM = ?', $win)
                ->where('type = ?', Core_Files::TECH_FILE);
        $filesList = $files->fetchAll($select);
        $this->view->files = $filesList;
    }

    public function wostageAction()
    {
    }
    
    public function configuratorAction()
    {
//      $customer_id = $this->getRequest()->getParam('id', null);
//        $user = new Core_Api_CustomerUser($customer_id);
//        $client = $user->loadById($customer_id);
//
//        $errorMessages = new Core_Form_ErrorHandler('updateCustomer_' . $customer_id);
//
//        $this->view->customer = $customer;
//        $this->view->success = $customer !== false;
//
//        $search = new Core_Api_CustomerClass;
//
//        $info = $user->load(
//                array(
//                    'login' => $this->_login . '|pmContext=' . $this->_company,
//                    'password' => $this->_password
//                )
//        );
//
//        $errors = Core_Api_Error::getInstance();
//        $er = $errors->getErrorArray();
//        foreach ($er as $k => $v)
//            $errorMessages->addMessage($error->message);
//
//        $this->view->messages = $errorMessages->getMessages();
//        $errorMessages->clearMessages();  
    }

	public function quickToolsAction () {
        $request = $this->getRequest();
		$company = $request->getParam ("company", null);
		$project = $request->getParam ("project", null);
		$cert = $request->getParam ("cert", null);
		$params = $request->getParam ("params", null);

		$sort = array (isset ($params["sort1"]) && !empty ($params["sort1"]) && $params["sort1"] != "None" ? $params["sort1"] : "SiteInfo1");
		if (!empty ($params["dir1"])) $sort[0] .= " " . $params["dir1"];

		if (isset ($params["sort2"]) && !empty ($params["sort2"]) && $params["sort2"] != "null") 
			$sort[] = $params["sort2"] . " " . $params["dir2"];
		if (isset ($params["sort3"]) && !empty ($params["sort3"]) && $params["sort3"] != "null") 
			$sort[] = $params["sort3"] . " " . $params["dir3"];

		$db = Core_Database::getInstance();
		$select = $db->select ()->distinct ();
		$select->from (array ("s" => "sites"), array (
			"SiteNumber" => "s.SiteNumber", 
			"SiteName" => "s.SiteName", 
			"SiteInfo1" => "s.SiteInfo1", 
			"SiteInfo2" => "s.SiteInfo2", 
			"City" => "s.City",
			"State" => "s.State",
			"Zipcode" => "s.Zipcode"
		));
		$select->join (
			array ("ts" => "tech_site"), 
			"s.SiteNumber = ts.SiteNumber", 
			array ("DateApproved" => "ts.DateApproved")
		);
		$select->join (
			array ("t" => "TechBankInfo"), 
			"ts.techid = t.TechID", 
			array ("TechID" => "t.TechID", "FirstName" => "t.FirstName", "LastName" => "t.LastName")
		);
		$select->join (
			array ("tc" => "tech_certification"), 
			"t.TechID = tc.TechID", 
			array ("contractor_id" => "tc.number")
		);
		$select->where ("s.Project_ID = ?", $project);

		if (isset ($params["contractor_id"]) && !empty ($params["contractor_id"]) && $params["contractor_id"] != "null") {
			$select->where ("tc.number = ?", $params["contractor_id"]);
		}
		else {
			$select->where ("NOT ISNULL (tc.number) AND tc.number <> ''");
		}

		if (isset ($params["city"]) && !empty ($params["city"]) && $params["city"] != "null") {
			$select->where ("s.City = ?", $params["city"]);
		}

		if (isset ($params["state"]) && !empty ($params["state"]) && $params["state"] != "null") {
			$select->where ("s.State = ?", $params["state"]);
		}

		if (isset ($params["zip"]) && !empty ($params["zip"]) && $params["zip"] != "null") {
			$select->where ("s.Zipcode = ?", $params["zip"]);
		}

		if (isset ($params["siteinfo1"]) && !empty ($params["siteinfo1"]) && $params["siteinfo1"] != "null") {
			$select->where ("s.SiteInfo1 = ?", $params["siteinfo1"]);
		}

		if (isset ($params["siteinfo2"]) && !empty ($params["siteinfo2"]) && $params["siteinfo2"] != "null") {
			$select->where ("s.SiteInfo2 = ?", $params["siteinfo2"]);
		}

		if (isset ($params["displaycontractors"])) {
			if ($params["displaycontractors"] == "approved") {
				$select->where ("NOT ISNULL(ts.DateApproved)");
			}
			elseif ($params["displaycontractors"] == "unapproved") {
				$select->where ("ISNULL(ts.DateApproved)");
			}
		}

		if (isset ($params["lastname"]) && !empty ($params["lastname"]) && $params["lastname"] != "null") {
			$select->where ("t.LastName = ?", $params["lastname"]);
		}

		$select->order ($sort);

		$site_techs = $db->fetchAll ($select);

        //$paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($wo, $countRows));
        //$paginator->setItemCountPerPage($this->_size);
        //$paginator->setCurrentPageNumber($this->_page);
        $this->view->paginator = $paginator;
		$this->view->company = $company;
		$this->view->project = $project;
		$this->view->site_techs = $site_techs;
	}

	public function quickToolsApproveAction () {
		$errors = Core_Api_Error::getInstance();
        $request = $this->getRequest();
		$company = $request->getParam ("company", null);
		$site = $request->getParam ("site", null);
		$project = $request->getParam ("project", null);
		$tech_id = $request->getParam ("tech", null);
		$return = array ("success" => 1);
		$count = 0;

		$db = Core_Database::getInstance();

		try {
			$select = $db->select ();
			$select->from ("tech_site");
			$select->where ("techid = ?", $tech_id);
			$select->where ("SiteNumber = ?", $site);
			//$select->where ("Project_ID = ?", $project);

			$count = $db->update ("tech_site", 
				array ("DateApproved" => date ("Y-m-d H:i:s")), 
				$db->quoteInto ("techid = ?", $tech_id) . 
				$db->quoteInto (" AND SiteNumber = ?", $site)
			);

			if ($count < 1) {
				$errors->addError (7, "Failed to update technician");
				$return["success"] = 0;
			}
		}
		catch (Exception $e) {
			error_log ($e->getMessage ());
			$errors->addError (7, "Failed to update technician");
			$return["success"] = 0;
		};

		if ($errors->hasErrors()) {
			$return["errors"] = (string) $errors;
		}

        $this->_helper->viewRenderer->setNoRender(true);
		$response = $this->getResponse();
		$response->clearHeaders()->clearBody();
		$return = json_encode($return);

		$response->setHeader('Content-Length', strlen($return), true);
		$response->setBody($return);
	}

	//public function quickToolsPopupAction () {
	//}

	public function quickToolsGetSitesAction () {
		$errors = Core_Api_Error::getInstance();
        $request = $this->getRequest();
		$company = $request->getParam ("company", null);
		$project = $request->getParam ("project", null);
		$return = array ("success" => 1);

		$db = Core_Database::getInstance();

		try {
			$query = "select s.*, sl.Name AS StateFull from sites s
				inner join States sl ON sl.Abbreviation = s.State
				where s.Project_ID = $project
				order by StateFull, City";
			$return["sites"] = $db->fetchAll ($query);
		}
		catch (Exception $e) {
			error_log ($e->getMessage ());
			$errors->addError (7, "Failed to get sites information");
			$return["success"] = 0;
		};

		if ($errors->hasErrors()) {
			$return["errors"] = (string) $errors;
		}

        $this->_helper->viewRenderer->setNoRender(true);
		$response = $this->getResponse();
		$response->clearHeaders()->clearBody();
		$return = json_encode($return);

		$response->setHeader('Content-Length', strlen($return), true);
		$response->setBody($return);
	}

	public function quickToolsUpdateContractorAction () {
		$errors = Core_Api_Error::getInstance();
        $request = $this->getRequest();
		$company = $request->getParam ("company", null);
		$project = $request->getParam ("project", null);
		$site = $request->getParam ("SiteNumber", null);
		$tech_id = $request->getParam ("tech_id", null);
		$contractor_id = $request->getParam ("contractor_id", null);
		$return = array ("success" => 1);

		$db = Core_Database::getInstance();

		try {
			if (!empty ($site)) {
				$count = $db->update ("tech_site", 
					array ("SiteNumber" => $site), 
					$db->quoteInto ("techid = ?", $tech_id) . 
					$db->quoteInto (" AND Project_ID = ?", $project)
				);

				if ($count < 1) {
					$errors->addError (7, "Failed to update site information");
					$return["success"] = 0;
				}
			}

			if (!empty ($contractor_id)) {
				$count = $db->update ("tech_certification", 
					array ("number" => $contractor_id), 
					$db->quoteInto ("TechID = ?", $tech_id) . 
					$db->quoteInto (" AND certification_id = ?", 22)
				);

				if ($count < 1) {
					$errors->addError (7, "Failed to update contractor ID");
					$return["success"] = 0;
				}
			}
		}
		catch (Exception $e) {
			error_log ($e->getMessage ());
			$errors->addError (7, "An error has occurred");
			$return["success"] = 0;
		};

		if ($errors->hasErrors()) {
			$return["errors"] = (string) $errors;
		}

        $this->_helper->viewRenderer->setNoRender(true);
		$response = $this->getResponse();
		$response->clearHeaders()->clearBody();
		$return = json_encode($return);

		$response->setHeader('Content-Length', strlen($return), true);
		$response->setBody($return);
	}

    private function _outputCsv(Core_CSV_Builder $csvBuilder)
    {
        $this->_helper->viewRenderer->setNoRender();
        $response = $this->getResponse();
        $response->clearHeaders();
        $response->clearBody();
        $response->setHeader('Pragma', 'public')
                ->setHeader('Expires', '0')
                ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
                ->setHeader('Cache-Control', 'private')
                ->setHeader('Content-Type', 'text/csv')
                ->setHeader('Content-Disposition', 'attachment; filename=' . $params['companyid'] . '-quickview' . '-' . date('mdY') . '.csv', true)
                ->setHeader('Content-Transfer-Encoding', 'binary');
        $response->sendResponse();

        //Flush buffers
        ob_end_flush();
        flush();

        $csvBuilder->printCSVCustomer();
        exit;
    }

}

