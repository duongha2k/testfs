<?php
class Dashboard_ProjectController extends Core_Controller_Action
{
    public function init()
    {
        parent::init();

        $request = $this->getRequest();

        $auth = new Zend_Session_Namespace('Auth_User');
        $this->view->isPM = $auth->isPM;
        $this->view->name = $auth->name;
        

        $user = new Core_Api_User();
        $clientInfoLogin = $user->loadWithExt(
                array(
                    'login' => $this->_login,
                    'password' => $this->_password
                )
        );
        $AccountManagerInfo=$user->getAccountManagerInfo($clientInfoLogin->ClientID);
        $this->view->AccountManagerInfo = $AccountManagerInfo;
        $apiClient = new Core_Api_Class();
        $isGPM = $apiClient->isGPM($clientInfoLogin->ClientID);
        $this->view->isGPM = $isGPM;        
       
        
        $company = $request->getParam('company',NULL);
		
        $deductCompanies = array('HXC','HXWN','HXXO','HXST','BW','suss','CBD','RHOM','TECF','SIS','BLUE','CPI','FDSV','PCS','DATA', 'BBLC', 'EQCS', 'ENPT','KIMS','ITLC','CPSS','Z2T','ENDV','CO1P', 'PSC', 'APEX','DOLO', 'DTPOS', 'OUTT', 'ITDM', 'ZAPC', 'ACEC', 'SUIS', 'TERX', 'CDT', 'B2BC', 'NNS', 'LATE', 'OTS', 'VERD', 'GANT', 'NMT', 'VSN', 'SYSI', 'SABX', 'NSCS', 'TKK', 'FLYE', 'METTC', 'TSWW', 'IKKAN', 'ITS', 'IPM', 'VALC', 'AIMW', 'NEAR', 'WIN', 'ITNS', 'AXAP', 'IPRO', 'CMIT', 'MORAN', 'STG', 'TEAM', 'LINK', 'EVNX', 'VTT', 'VDNI', 'ROI', 'EMIT', 'MML', 'DT', 'MTI', 'NASI', 'BWT', 'LMS', 'GMNS', 'ACG', 'IBA', 'UNITIV', 'BPI', 'NGIT','FLS');

        $this->view->company = !empty($company)?$company:$_SESSION['Company_ID'];
        $this->view->showDeduct = true;  //in_array($this->view->company, $deductCompanies);//$company == 'HXC' || $company == 'HXWN' || $company == 'HXXO' || $company == 'HXST' || $company == 'BW' || $company == 'suss' || $company == 'CBD' || $company == 'RHOM' || $company == 'TECF';
        $callTypes = $apiClient->getCallTypes();
        $this->view->callTypes = $callTypes->data;
    }

    public function createAction()
    {
    	$companyId = $this->view->company;

        $search = new Core_Api_Class;

        $authData = array('login' => $this->_login.'|pmContext='.$companyId, 'password' => $this->_password);
        $client = new Core_Api_User;
        $client->loadWithExt($authData);
        $this->view->client = $client;
		$this->view->clientForOwner = $search->getClientsList($this->_login.'|pmContext='.$companyId, $this->_password, array('Company_ID' => $companyId, 'ShowAllGPM' => $client->isPM()), "Username");
        $this->view->PcntDeductPercent = $client->GetDeductPercent_FSSFeeFrTech($companyId);
        // categories list
        $categories = $search->getWOCategories($this->_login.'|pmContext='.$companyId, $this->_password);
        //echo("categories list: ");print_r($categories);die();//testdie();
        $this->view->categoriesSuccess = $categories->success;
        if ($categories->success)
            $this->view->categories = $categories->data;

        $search = new Core_Api_CustomerClass();
        // customers list
        $customers = $search->getCustomersByCompanyId($companyId);
        $this->view->customersSuccess = $customers->success;
        if ($customers->success)
            $this->view->customers = $customers->data;
        //Expense Reporting
        $Core_Api_ProjectClass  = new Core_Api_ProjectClass();
        $ExpenseReporting = $Core_Api_ProjectClass->getProjectBCats_insertFromClientIfNotExists(0,$companyId);
        $this->view->expenseReporting = $ExpenseReporting;
        $this->view->EnableColspanTechPay = $Core_Api_ProjectClass->enableExpenseReporting(0,$companyId);

        $client_files = $client->getFileList ($companyId, Core_Files::SIGNOFF_FILE);
		if (!empty ($client_files)) {
			$this->view->CustomSignoff = $client_files[0]["path"];
			$this->view->CustomSignoffFile = ltrim (strstr ($client_files[0]["path"], "/"), "/");
			$this->view->CustomSignoffLink = "/widgets/dashboard/do/file-wos-documents-download/filename/" . base64_encode(urlencode($this->view->CustomSignoffFile)) . "/" . $this->view->CustomSignoffFile;
		}
		else
			$this->view->CustomSignoff = false;
                $Core_Api_WosClass = new Core_Api_WosClass();
        $this->view->EstDurationArray = $Core_Api_WosClass->getEstDurationArray();
        //--- 13546 clientEnableExpenseReporting
        $this->view->clientEnableExpenseReporting = $client->enableExpenseReporting($companyId);
        //13624
        $FSTagClass = new  Core_Api_FSTagClass();
        $this->view->FSTagList = $FSTagClass->getFSTagList_ForCompany(0,$companyId);

		$fs_experts = new Core_Api_FSExpertClass ();
		$this->view->fs_experts = $fs_experts->getFSExperts (1);
    }

    public function doCreateAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $params = $this->getRequest()->getParams();
        $companyId = $params['Project_Company_ID'];
        
        $errors = array();
        if (!count($errors)) {
			$urlStripFields = array("Headline", "Description", "Requirements", "SpecialInstructions");
			foreach ($urlStripFields as $field) {
				if (empty($params[$field])) continue;
				$params[$field] = preg_replace("/<\/?a[^>]*>/", "", $params[$field]);
				$params[$field] = preg_replace("/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})/","", $params[$field]);
				$params[$field] = preg_replace("/\b(?:(?:https?|ftp):\/\/)?(?:www\d{0,3}[.])?(?:[a-z0-9\-]|.(?!.))+[.](?:com|co|mx|ca|edu|info|biz|net|org)[\/]?(?:[^\s]+)?/","", $params[$field]);
			}

        	$search = new Core_Api_ProjectClass();

            $return = array('success'=>0);

            $project = new API_Project();
            $className = get_class($project);
            $QuickTicket_ID = $params['QuickTicketID'];
//            echo "QuickTicket_ID: ".$QuickTicket_ID."br/>";
            unset($params['QuickTicketID']);
            
            $className = get_class($project);
            $QuickTicket_ID = $params['QuickTicketID'];
            unset($params['QuickTicketID']);

            foreach ($params as $key => $value)
            {
                if(property_exists($className, $key)) $project->$key = $value;
            }
            //Special handler for checkboxes.
            $checkboxes = array('isProjectAutoAssign','NotifyOnAutoAssign','FixedBid','PcntDeduct',
								'NotifyOnBid','NotifyOnAssign','ReminderAll','AutoSMSOnPublish',
								'ReminderAcceptance','Reminder24Hr','Reminder1Hr','CheckInCall',
								'CheckOutCall','ReminderNotMarkComplete','ReminderIncomplete',/*'SignOffSheet_Required',*/
				'Active','InstallDesk_Trigger','OOS_Trigger',
				"AutoBlastOnPublish","NotifyOnWorkDone","PartManagerUpdate_Required",
				'ReminderCustomHrChecked','ReminderCustomHrChecked_2','ReminderCustomHrChecked_3',
				"SignOff_Disabled","EnableSiteStatusReporting","AllowDeVryInternstoObserveWork","AllowDeVryInternstoPerformField",'ReceiveWorkAcceptedByTechEmail','ReceiveWorkConfirmedByTechEmail','ReceiveTechCheckedInEmail','ReceiveTechCheckedOutEmail', 'PushWM');
        	foreach ($checkboxes as $c) {
        		if (!property_exists($className, $c))
        		continue;
        		$project->$c = isset($params[$c]);
        	}

			//$project->SignOff_Disabled = !$project->SignOff_Disabled;
			
			if(isset($params['SignOffSheet_Required'])){
				$project->SignOffSheet_Required = $params['SignOffSheet_Required'];
			}

        	if (isset ($params["ClientSignoff"])) {
        		if (empty ($params["ClientSignoff_Remove"])) {
        			$project->CustomSignOff = $params["ClientSignoff"];
        		}
        	}

        	$project->CreatedBy = $this->_login;
             //13808 
            $WoTagClass = new Core_Api_WoTagClass(); 
            $companyTagValues = $WoTagClass->getCompanyTagValues($companyId);
            //13964
            if(!$this->view->isGPM) {
                $project->ServiceTypeId = 57;
                $project->FSClientServiceDirectorId = 10;
            }
            //end 13964
            if(!empty($companyTagValues['EntityTypeId']['TagItemId'])) { 
				$project->EntityTypeId = $companyTagValues['EntityTypeId']['TagItemId'];
            }
            //end 13808

        	//Special handlers for virtual fields

        	$apiResponse = $search->createProject($this->_login.'|pmContext='.$companyId, $this->_password, $project);
        	if (empty($apiResponse->errors)) {                
        	    Core_Cache_Manager::factory()->clean('all', array('prj'));
        		$return['success'] = 1;
        		$return['project_id'] = $apiResponse->data[0]->Project_ID;

				// update Certification
				if(isset($params['CredentialCertification_id_1']) && $params['CredentialCertification_id_1'] > 0){
					$Core_ProjectCertification = new Core_ProjectCertification($return['project_id'],$params['CredentialCertification_id_1']); 
					$Core_ProjectCertification->save();
				}

				//Save credential requirements for FS-RUSH
				$process_certs = function ($field, $params) {
					$result = array ();
					$index = 1;

					do {
						$id = null;
						if (isset ($params[$field . "_id_" . $index])) {
							$id = $params[$field . "_id_" . $index];
							$level = $params[$field . "_level_" . $index];
							$result[$id] = $level;
							$index++;
						}
					} while (!empty ($id));

					return $result;
				};
				$error = $search->saveP2TCertRequirements ($return['project_id'], array (
					"client" => $process_certs ("CredentialCertification", $params),
					"public" => $process_certs ("PublicCertification", $params),
					"expert" => $process_certs ("FSExpert", $params),
					"industry" => $process_certs ("IndustryCertification", $params)
				), false);

				// save Mun nhot
				$request = $this->getRequest();
				$Core_Api_ProjectClass  = new Core_Api_ProjectClass();
				$EnableExpenseReportingTechPay = $request->getParam('EnableExpenseReportingTechPay',0);
				$fieldvals = array();
				$fieldvals["enable_expense_reporting"] = $EnableExpenseReportingTechPay;
				$Core_Api_ProjectClass->saveProjectAdditionalFields($prj->Project_ID,$fieldvals);
				$WorkOrderSettingsTechPay = $request->getParam('WorkOrderSettingsTechPay', array());
				if(!empty($WorkOrderSettingsTechPay))
				{
					for($i = 0;$i < count($WorkOrderSettingsTechPay["bcatid"]); $i++)
					{
						if(!empty($WorkOrderSettingsTechPay["bcatname"][$i]))
						{    
							$fieldvals = array();
							$fieldvals["bcatid"] = $WorkOrderSettingsTechPay["bcatid"][$i];
							$fieldvals["bcatname"] = $WorkOrderSettingsTechPay["bcatname"][$i];
							$fieldvals["bcatdesc"] = $WorkOrderSettingsTechPay["bcatdesc"][$i];
							$fieldvals["ischecked"] = $WorkOrderSettingsTechPay["ischecked"][$i];
							$fieldvals["reimbursable_thru_fs"] = $WorkOrderSettingsTechPay["reimbursable_thru_fs"][$i];
							$fieldvals["require_doc_upl"] = $WorkOrderSettingsTechPay["require_doc_upl"][$i];
							$fieldvals["bcattype"] = $WorkOrderSettingsTechPay["bcattype"][$i];
							$fieldvals["projectid"] = $return['project_id'];

							$Core_Api_ProjectClass->saveProjectBCat($fieldvals["bcatid"],$fieldvals);
						}
					}
				}
				
				$apiQT = new Core_Api_CustomerClass();
				$apiQT->addProjectToQTVOnlyOne($QuickTicket_ID,$apiResponse->data[0]->Project_ID);
        	} else {
				$return['success'] = 0;
        		$return['errors'] = array();
        		foreach ($apiResponse->errors as $e) {
        			$return['errors'][]=$e->message;
        		}
        	}
        } else {
        	$return['success'] = 0;
        	$return['errors'] = $errors;
        }
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                 ->setBody($return);
    }

    public function projectDocumentsAction()
    {
        $search = new Core_Api_Class;
        $company = $this->getRequest()->getParam('company',NULL);
        $project  = $this->getRequest()->getParam('id',null);
        if ($project) {
            $project = $search->getProjects($this->_login.'|pmContext='.$company,$this->_password,array($project),null);
        }
        $this->view->project = $project;
    }


    public function updateAction()
    {
        $request   = $this->getRequest();
        $companyId = $this->view->company;
        $prj_id    = $request->getParam('project_id', null);
        $search    = new Core_Api_ProjectClass();

        $prj = $search->getProjects(
            $this->_login.'|pmContext='.$companyId,
            $this->_password,
            $prj_id,
            $companyId
        );
        //14125
        $cp = $request->getParam('cp', null);
        if(!empty($cp))
        {
            $this->view->isCopy = true;   
            $prj->data[0]->Owner = $this->_login;
            $prj->data[0]->CreatedBy = $this->_login;
            $prj->data[0]->Project_Name = "";	
	}//14125
        // load ProjectCertification
        $Core_ProjectCertification = new Core_ProjectCertification($prj_id,"");
        $ProjectCertification = $Core_ProjectCertification->loadByProjectId();
        if(!empty($ProjectCertification)){
            $prj->data[0]->CredentialCertificationId = $ProjectCertification[0]["certification_id"];
        }else{
            $prj->data[0]->CredentialCertificationId = "";
        }
            
        //13992
        $CoreUser = new Core_Api_User();
        $repClientID = $CoreUser->getRepClientIDforCompany($companyId);
        $repClientExtInfo = $CoreUser->getClientExt($repClientID);
        //14078
        $projectClass = new Core_Api_ProjectClass();
        if(!empty($repClientExtInfo)){
            $clientCSDID = $repClientExtInfo[0]['FSClientServiceDirectorId']; 
            $entityTypeId = $repClientExtInfo[0]['EntityTypeId']; 
            $serviceTypeId = $repClientExtInfo[0]['ServiceTypeId']; 
        }

        if(empty($prj->data[0]->FSClientServiceDirectorId)){
            if(empty($clientCSDID)){
                $prj->data[0]->FSClientServiceDirectorId = 10;
            } else {
                $prj->data[0]->FSClientServiceDirectorId = $clientCSDID;                
            }
            
            $result = $projectClass->updateProject_CSDID($prj_id,$prj->data[0]->FSClientServiceDirectorId);
        }
      
        if(empty($prj->data[0]->EntityTypeId)){
            if(empty($entityTypeId)){
                $prj->data[0]->EntityTypeId = 23;
            } else {
                $prj->data[0]->EntityTypeId = $entityTypeId;                
            }
            $result = $projectClass->updateProject_EntityTypeId($prj_id,$prj->data[0]->EntityTypeId);
        }
   
        if(empty($prj->data[0]->ServiceTypeId)){
            if(empty($serviceTypeId)){
                $prj->data[0]->ServiceTypeId = 15;
            } else {
                $prj->data[0]->ServiceTypeId = $serviceTypeId;                
            }
            $result = $projectClass->updateProject_ServiceTypeId($prj_id,$prj->data[0]->ServiceTypeId);
        }
       
        //end 13992 
        //14078        
        $this->view->project = $prj->data[0];
        //echo("<pre>this->view->project: ");print_r($this->view->project);
        $this->view->projectRequirements = $this->getHtmlString($prj->data[0]->Requirements);
        $this->view->projectDescription = $this->getHtmlString($prj->data[0]->Description);
        $this->view->projectSpecialInstructions = $this->getHtmlString($prj->data[0]->SpecialInstructions);
        $this->view->projectSpecialInstructions_AssignedTech = $this->getHtmlString($prj->data[0]->SpecialInstructions_AssignedTech);
        
        
        $this->view->success = $prj->success;
        $this->view->errors  = $prj->errors;

        $search = new Core_Api_Class();
        $authData = array('login' => $this->_login.'|pmContext='.$companyId, 'password' => $this->_password);
        $client = new Core_Api_User;
        $client->loadWithExt($authData);
		$this->view->clientForOwner = $search->getClientsList($this->_login.'|pmContext='.$companyId, $this->_password, array('Company_ID' => $companyId, 'ShowAllGPM' => $client->isPM()), "Username");

		// Client info
        $this->view->client = $client;
        // categories list
        $search = new Core_Api_Class();

        // Client info
        //$this->view->client = $search->getClientByCompanyID ($companyId);
        // categories list
        $categories = $search->getWOCategories($this->_login.'|pmContext='.$companyId, $this->_password);
        $this->view->categoriesSuccess = $categories->success;
        if ($categories->success)
            $this->view->categories = $categories->data;
        $search = new Core_Api_CustomerClass();
        // customers list
        $customers = $search->getCustomersByCompanyId($companyId);
        $this->view->customersSuccess = $customers->success;
        if ($customers->success)
            $this->view->customers = $customers->data;

		$files = new Core_ProjectsFiles();
        $select = $files->select();
        $select->where('Project_ID = ?', $prj_id);
        $filesList = $files->fetchAll($select);

        $this->view->files = $filesList;
        //Expense Reporting
        $Core_Api_ProjectClass  = new Core_Api_ProjectClass();
        $ExpenseReporting = $Core_Api_ProjectClass->getProjectBCats_insertFromClientIfNotExists($prj_id);
        $this->view->expenseReporting = $ExpenseReporting;
        $this->view->EnableColspanTechPay = $Core_Api_ProjectClass->enableExpenseReporting($prj_id);        
        $this->view->EnableColspanCompany = $Core_Api_ProjectClass->enableExpenseReporting(0,$companyId);        
        $callTypes = $search->getCallTypes();
        $this->view->callTypes = $callTypes->data;
         $Core_Api_WosClass = new Core_Api_WosClass();
        $this->view->EstDurationArray = $Core_Api_WosClass->getEstDurationArray();
        //--- 13546 clientEnableExpenseReporting
        $this->view->clientEnableExpenseReporting = $client->enableExpenseReporting($companyId);
        //13624
        $FSTagClass = new  Core_Api_FSTagClass();
        $this->view->FSTagList = $FSTagClass->getFSTagList_ForCompany(0,$companyId);

		$fs_experts = new Core_Api_FSExpertClass ();
		$this->view->fs_experts = $fs_experts->getFSExperts (1);

		$required_certs = $Core_Api_ProjectClass->loadP2TCertRequirements ($prj_id);
		$this->view->required_certs = $required_certs;
	}

    public function doUpdateAction()
    {
        $this->_helper->viewRenderer->setNoRender();        
        $params    = $this->getRequest()->getParams();
        //echo("<pre>");print_r($params);die();//test
        $prjClass  = new Core_Api_ProjectClass();
        $return    = array('success'=>0);
        $companyId = $params['Project_Company_ID'];
        $QuickTicket_ID = $params['QuickTicketID'];
        unset($params['QuickTicketID']);
        //14125
        $cp = $params['isCopy'];       
        if(!isset($cp) || $cp=='0')
        {        	
        $prj = $prjClass->getProjects(
            $this->_login.'|pmContext='.$companyId,
            $this->_password,
            $params['Project_ID'],
            $companyId
        );
        $prjold = $prjClass->getProjects(
            $this->_login.'|pmContext='.$companyId,
            $this->_password,
            $params['Project_ID'],
            $companyId
        );
        $Core_Api_ProjectClass  = new Core_Api_ProjectClass();
        $ExpenseReporting = $Core_Api_ProjectClass->getProjectBCats_insertFromClientIfNotExists($params['Project_ID']);

		if ($prj->success && !empty($prj->data[0]->Project_ID)) {
			$urlStripFields = array("Headline", "Description", "Requirements", "SpecialInstructions");
			foreach ($urlStripFields as $field) {
				if (empty($params[$field])) continue;
				$params[$field] = preg_replace("/<\/?a[^>]*>/", "", $params[$field]);
				$params[$field] = preg_replace("/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})/","", $params[$field]);
				$params[$field] = preg_replace("/\b(?:(?:https?|ftp):\/\/)?(?:www\d{0,3}[.])?(?:[a-z0-9\-]|.(?!.))+[.](?:com|co|mx|ca|edu|info|biz|net|org)[\/]?(?:[^\s]+)?/","", $params[$field]);
			}

			$prj = $prj->data[0];
            $className = get_class($prj);
            $prjold = $prjold->data[0];
            $oldexpen = $ExpenseReporting;

            $currentProject = $prj->Project_ID;

            if (!empty($params['ReminderAll']) ) {
                if (empty($prj->ReminderAll) ) {
                    $prj->ACSAuthBy = $this->_login;
                    $prj->ACSAuthDate = date('Y-m-d H:i:s');
                }
            } else {
                $prj->ACSAuthBy = '';
                $prj->ACSAuthDate = '0000-00-00 00:00:00';
            }
            
            foreach ($params as $key => $value) {
                if(property_exists($className, $key)) $prj->$key = $value;
            }

            if (!empty($params["Project_ID"]) && $currentProject != $params["Project_ID"])
                unset($prj->Project_Name);

            //Special handler for checkboxes. //13483,13676,13732,13763
            $checkboxes = array('isProjectAutoAssign','NotifyOnAutoAssign','FixedBid','PcntDeduct',
								'NotifyOnBid','NotifyOnAssign','ReminderAll','AutoSMSOnPublish',
								'ReminderAcceptance','Reminder24Hr','Reminder1Hr','CheckInCall',
								'CheckOutCall','ReminderNotMarkComplete','ReminderIncomplete',/*'SignOffSheet_Required',*/
				'Active','InstallDesk_Trigger','OOS_Trigger',
				"AutoBlastOnPublish","NotifyOnWorkDone","PartManagerUpdate_Required",
				"ReceiveScheduleConflictNotification", "AllowCustomCommSettings","ReminderCustomHrChecked",
				'ReminderCustomHrChecked_2','ReminderCustomHrChecked_3', "SignOff_Disabled",
				'EnableSiteStatusReporting', 'ReceiveReVisitNotification', 'ReceiveTechQAMessageNotification','AllowDeVryInternstoObserveWork','AllowDeVryInternstoPerformField','SiteContactReminderCall','ReceiveWorkAcceptedByTechEmail','ReceiveWorkConfirmedByTechEmail','ReceiveTechCheckedInEmail','ReceiveTechCheckedOutEmail', 'PushWM');

            foreach ($checkboxes as $c) {
                if (!property_exists($className, $c))
                    continue;
                $prj->$c = isset($params[$c]);
            }

			$prj->SignOff_Disabled = !$prj->SignOff_Disabled;

            $removefiles = $this->getRequest()->getParam('removefiles', null);

            //Special handler for Remove Project Logo
            if ( !empty($params['Project_Logo_Rm']) ) {
                $prjLogoRm = $prj->Project_Logo;
                $prj->Project_Logo = '';
            }
            //Special handler to Remove Custom Signoff Sheet
            if ( !empty($params['CustomSignOff_Rm']) ) {
                $signoffRm = $prj->CustomSignOff_Rm;
                $prj->CustomSignOff = '';
				$Core_Api_ProjectClass->updateAllWOs_CustomSignOff ($prj->Project_ID, NULL, $params["ACSApplyTo"]);
            }
            //Special PcntDeductPercent
            if (empty($params['PcntDeduct']) ) {
                $prj->PcntDeduct = 0;
                $prj->PcntDeductPercent = 0;
            }
            //--- StartDate, StartTime, EndDate, EndTime, StartRange, Duration
            if ( !empty($params['StartDate']) ) {
                $prj->StartDate = $params['StartDate'];
            }
            if ( !empty($params['StartTime']) ) {
                $prj->StartTime = $params['StartTime'];
            }
            if ( !empty($params['EndDate']) ) {
                $prj->EndDate = $params['EndDate'];
            }
            if ( !empty($params['EndTime']) ) {
                $prj->EndTime = $params['EndTime'];
            }
            if ( !empty($params['StartRange']) ) {
                $prj->StartRange = $params['StartRange'];
            }
            if ( !empty($params['Duration']) ) {
                $prj->Duration = $params['Duration'];
            }
            //--- BidNotificationEmails, WorkDoneNotificationEmails 
            if ( !empty($params['BidNotificationEmails']) ) {
                $prj->BidNotificationEmails = $params['BidNotificationEmails'];
            }
            if ( !empty($params['WorkDoneNotificationEmails']) ) {
                $prj->WorkDoneNotificationEmails = $params['WorkDoneNotificationEmails'];
            }
            if(!empty($params['SpecialInstructions']))
            {
                $SpecialInstructions = explode("Expense Reporting:", $params['SpecialInstructions']);
                $SpecialInstructions = $SpecialInstructions[0];
                $SpecialInstructions = trim($SpecialInstructions);
                $prj->SpecialInstructions = $SpecialInstructions;
            }    
            if ( !empty($params['Type_ID']) ) {
                $prj->Type_ID = $params['Type_ID'];
            }
            $prj->CredentialCertificationId = "";
            //echo("<pre>prj: ");print_r($prj);die();//test
            $apiResponse = $prjClass->updateProject(
                $this->_login.'|pmContext='.$companyId,
                $this->_password,
                $prj->Project_ID,
                $prj
             );
			 //--- 13713
			$WosClass = new Core_Api_WosClass();
			if ($prjold->AllowDeVryInternstoObserveWork != $prj->AllowDeVryInternstoObserveWork ||
				$prjold->AllowDeVryInternstoPerformField != $prj->AllowDeVryInternstoPerformField) {
				$WosClass->updateDeVryObserveAndPerform_ofWOs($prj->Project_ID,
						$prj->AllowDeVryInternstoObserveWork,
						$prj->AllowDeVryInternstoPerformField,
						$params['ACSApplyTo']
				);				
			}
			
			if ($prjold->ACSNotifiPOC != $prj->ACSNotifiPOC || 
				$prjold->ACSNotifiPOC_Phone != $prj->ACSNotifiPOC_Phone ||
				$prjold->ACSNotifiPOC_Email != $prj->ACSNotifiPOC_Email) {
				$WoAdditionalFieldValues = array(
					"ACSNotifiPOC" => $prj->ACSNotifiPOC,
					"ACSNotifiPOC_Phone" => $prj->ACSNotifiPOC_Phone,
					"ACSNotifiPOC_Email" => $prj->ACSNotifiPOC_Email
				);
                //echo("<br/>Project_ID: ".$prj->Project_ID);
                //echo("<br/>UpdateACSNotifiPOC");die();
                $WosClass = new Core_Api_WosClass();
				$WosClass->updateWoAdditionalFields_ofWOs($prj->Project_ID,
					$WoAdditionalFieldValues,
					$params['ACSApplyTo']
				);
			}

            //--- end 13713
            $WoTagClass = new Core_Api_WoTagClass();
//            $WoTagClass->saveWOTag_forAllWOsInProject($prj->Project_ID,"ServiceTypeId",$prj->ServiceTypeId);
//            $WoTagClass->saveWOTag_forAllWOsInProject($prj->Project_ID,"FSClientServiceDirectorId",$prj->FSClientServiceDirectorId);
            
			if ($prjold->EntityTypeId != $prj->EntityTypeId) {
	            $WoTagClass->saveWOTag_forAllWOsInProject($prj->Project_ID,"EntityTypeId",$prj->EntityTypeId);
			}
        
            $WosClass = new Core_Api_WosClass();
			
			if ($prjold->ServiceTypeId != $prj->ServiceTypeId) {
	            $WosClass->updateServiceTypeID_AllNonInvoiceWOs_ofProject($prj->Project_ID,$prj->ServiceTypeId);
			}
			
			if ($prjold->FSClientServiceDirectorId != $prj->FSClientServiceDirectorId) {
	            $WosClass->updateCSDID_AllNonInvoiceWOs_ofProject($prj->Project_ID,$prj->FSClientServiceDirectorId);
			}
            //13719
             
            if ( !empty($params['WorkStartTime']) ) {
                $prj->WorkStartTime = $params['WorkStartTime'];
            }
            if ( !empty($params['WorkEndTime']) ) {
                $prj->WorkEndTime = $params['WorkEndTime'];
            }
            if ( !empty($params['EstimatedDuration']) ) {
                $prj->EstimatedDuration = $params['EstimatedDuration'];
            }
            if ( !empty($params['TechArrivalInstructions']) ) {
                $prj->TechArrivalInstructions = $params['TechArrivalInstructions'];
            }            
            if ( !empty($params['StartType']) ) {
                $prj->StartType = $params['StartType'];
            }          
            
            if ($prjold->StartType != $prj->StartType ||
				$prjold->WorkStartTime != $prj->WorkStartTime ||
				$prjold->WorkEndTime != $prj->WorkEndTime ||
				$prjold->EstimatedDuration != $prj->EstimatedDuration ||
				$prjold->TechArrivalInstructions != $prj->TechArrivalInstructions){                        
                    $WosClass->updateServiceSchedule_ofWOs($prj->Project_ID,$prj->WorkStartTime,$prj->WorkEndTime,
                    $prj->StartType,$prj->EstimatedDuration,$prj->TechArrivalInstructions,$params['ACSApplyTo']);                 							
             }   
             //end 13719           
            if ( ($apiResponse->success) ) {
                // update Certification
                if(isset($params['CredentialCertification_id_1']) && $params['CredentialCertification_id_1'] > 0){
                    $Core_ProjectCertification = new Core_ProjectCertification($prj->Project_ID,$params['CredentialCertification_id_1']); 
                    $Core_ProjectCertification->saveExt();
                }

				//Save credential requirements for FS-RUSH
				$process_certs = function ($field, $params) {
					$result = array ();
					$index = 1;

					do {
						$id = null;
						if (isset ($params[$field . "_id_" . $index])) {
							$id = $params[$field . "_id_" . $index];
							$level = $params[$field . "_level_" . $index];
							$result[$id] = $level;
							$index++;
						}
					} while (!empty ($id));

					return $result;
				};
				$error = $prjClass->saveP2TCertRequirements ($prj->Project_ID, array (
					"client" => $process_certs ("CredentialCertification", $params),
					"public" => $process_certs ("PublicCertification", $params),
					"expert" => $process_certs ("FSExpert", $params),
					"industry" => $process_certs ("IndustryCertification", $params)
				));

                if (!empty($removefiles) && is_array($removefiles)) {
                    $files = new Core_ProjectsFiles();
                    $r = $files->removeFiles($prj->Project_ID, $removefiles);
                }
                if(!empty($prjLogoRm)){
                    $f = new Core_File();
                    $f->removeFile($prjLogoRm, S3_PROJECTS_LOGO_DIR);
                }

                Core_Cache_Manager::factory()->clean('all', array('prj'));
                $return['success'] = 1;
                $return['project_id'] = $prj->Project_ID;
                $return['prjold'] = json_encode($prjold);
                $return['oldexpen'] = json_encode($oldexpen);
                $apiQT = new Core_Api_CustomerClass();
                $Quickticketid = $apiQT->getQTVId_ForProject($prj->Project_ID);
                if($Quickticketid != $QuickTicket_ID)
                {
                    $apiQT->addProjectToQTVOnlyOne($QuickTicket_ID,$prj->Project_ID);
                }
                // save Mun nhot
                $request = $this->getRequest();
                $Core_Api_ProjectClass  = new Core_Api_ProjectClass();
                $EnableExpenseReportingTechPay = $request->getParam('EnableExpenseReportingTechPay',0);
                $fieldvals = array();
                $fieldvals["enable_expense_reporting"] = $EnableExpenseReportingTechPay;
                $Core_Api_ProjectClass->saveProjectAdditionalFields($prj->Project_ID,$fieldvals);
                $WorkOrderSettingsTechPay = $request->getParam('WorkOrderSettingsTechPay', array());
                if(!empty($WorkOrderSettingsTechPay))
                {
                    for($i = 0;$i < count($WorkOrderSettingsTechPay["bcatid"]); $i++)
                    {
                        if(!empty($WorkOrderSettingsTechPay["bcatname"][$i]))
                        {    
                            $fieldvals = array();
                            $fieldvals["bcatid"] = $WorkOrderSettingsTechPay["bcatid"][$i];
                            $fieldvals["bcatname"] = $WorkOrderSettingsTechPay["bcatname"][$i];
                            $fieldvals["bcatdesc"] = $WorkOrderSettingsTechPay["bcatdesc"][$i];
                            $fieldvals["ischecked"] = $WorkOrderSettingsTechPay["ischecked"][$i];
                            $fieldvals["reimbursable_thru_fs"] = $WorkOrderSettingsTechPay["reimbursable_thru_fs"][$i];
                            $fieldvals["require_doc_upl"] = $WorkOrderSettingsTechPay["require_doc_upl"][$i];
                            $fieldvals["bcattype"] = $WorkOrderSettingsTechPay["bcattype"][$i];
                            $fieldvals["projectid"] = $prj->Project_ID;

                            $Core_Api_ProjectClass->saveProjectBCat($fieldvals["bcatid"],$fieldvals);
                        }
                    }  
                }
            } else {
                $return['success'] = 0;
                $return['errors'] = array();
                foreach ($apiResponse->errors as $e) {
                    $return['errors'][]=$e->message;
                }
            }
        } else {
            $return['success'] = 0;
            $return['errors'] = array('There is no such Project');
            foreach ($prj->errors as $e) {
                $return['errors'][]=$e->message;
            }
        }
        
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                 ->setBody($return);
        }else
        {
            $companyId = $this->view->company;

            $prjIdCopy = $params['Project_ID'];       
            $prjName = $params['Project_Name'];

            $prjCopy = $prjClass->getProjects(
                                            $this->_login.'|pmContext='.$companyId,
                                            $this->_password,
                                            $prjIdCopy                                           
                                            );
           
            if ($prjCopy->success && !empty($prjCopy->data[0])) {
                $prjold = $prjCopy->data[0];            
                $project = clone $prjold;   
                $project->Owner = $this->_login;
                $project->CreatedBy = $this->_login;    
                           
                $prj = $prjClass->copyProject($this->_login.'|pmContext='.$companyId, $this->_password, $project,$prjName);            
            }
            $prj = $prj->data[0];
            $project_ID = $prj->Project_ID;                  
            if (!empty($project_ID)) {
                    $className = get_class($prj);
                    foreach ($params as $key => $value) {
                        if(property_exists($className, $key)) $prj->$key = $value;
                    }
                    $checkboxes = array('isProjectAutoAssign','NotifyOnAutoAssign','FixedBid','PcntDeduct',
                                                                        'NotifyOnBid','NotifyOnAssign','ReminderAll','AutoSMSOnPublish',
                                                                        'ReminderAcceptance','Reminder24Hr','Reminder1Hr','CheckInCall',
                                                                        'CheckOutCall','ReminderNotMarkComplete','ReminderIncomplete',/*'SignOffSheet_Required',*/
                                        'Active','InstallDesk_Trigger','OOS_Trigger',
                                        "AutoBlastOnPublish","NotifyOnWorkDone","PartManagerUpdate_Required",
                                        "ReceiveScheduleConflictNotification", "AllowCustomCommSettings","ReminderCustomHrChecked",
                                        'ReminderCustomHrChecked_2','ReminderCustomHrChecked_3', "SignOff_Disabled",
                                        'EnableSiteStatusReporting', 'ReceiveReVisitNotification', 'ReceiveTechQAMessageNotification','AllowDeVryInternstoObserveWork','AllowDeVryInternstoPerformField','SiteContactReminderCall','ReceiveWorkAcceptedByTechEmail','ReceiveWorkConfirmedByTechEmail','ReceiveTechCheckedInEmail','ReceiveTechCheckedOutEmail', 'PushWM');

                    foreach ($checkboxes as $c) {
                        if (!property_exists($className, $c))
                            continue;
                        $prj->$c = isset($params[$c]);
                    }
                    $prj->Project_ID= $project_ID;

                    $apiResponse = $prjClass->updateProject(
                        $this->_login.'|pmContext='.$companyId,
                        $this->_password,
                        $project_ID,
                        $prj
                    );       
                    $success = 1;
                    $project_id = $project_ID;  
                    $this->_helper->json(array(
                                    'success'=>$success,                                    
                                    'project_id'=> $project_id                                    
                                    )
                    );     
            }else
            {
                $success = 0;
                $return['errors'] = array();
                foreach ($prj->errors as $e) {
                    $return['errors'][]=$e->message;
                    }
                    $this->_helper->json(array(
                                    'success'=>$success,                                    
                                    'errors'=> $return['errors']                                    
                                    )
                    );     
            }
          
        }
        //14125
    }

    public function ajaxGetProjectAction() {
        $this->_helper->viewRenderer->setNoRender();
        $params    = $this->getRequest()->getParams();
        $prjClass  = new Core_Api_ProjectClass();
        $return    = array('success'=>0);
        $companyId = $params['Project_Company_ID'];

        $prj = $prjClass->getProjects(
            $this->_login.'|pmContext='.$companyId,
            $this->_password,
            $params['Project_ID'],
            $companyId
        );

        if ($prj->success && !empty($prj->data[0]->Project_ID)) {
		$return['success'] = 1;
		$return['project'] = $prj->data[0];
	}

        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                 ->setBody($return);
    }

    public function ajaxUploadFileAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $company = $this->view->company;

        $maxFileSize = 52428800; //50Mb

        $request    = $this->getRequest();
        $id         = $request->getParam('file', NULL);
        $descr      = $request->getParam('descr', NULL);
        $action     = $request->getParam('prj_act', NULL);
        $project_id = $request->getParam('project_id', NULL);
        $option		= $request->getParam('option', 3);

        $return = array('success'=>0);

        $action = ($action == Core_Api_ProjectClass::UPLOAD_FILE_ACTION_CREATE)?
            Core_Api_ProjectClass::UPLOAD_FILE_ACTION_CREATE : Core_Api_ProjectClass::UPLOAD_FILE_ACTION_UPDATE;
        if (
            !empty($id)                            &&
            !empty($_FILES[$id])                   &&
            is_readable($_FILES[$id]['tmp_name'])  &&
            $_FILES[$id]['error'] == UPLOAD_ERR_OK &&
            filesize($_FILES[$id]['tmp_name']) <= $maxFileSize &&
            mb_strlen($descr, 'utf-8') <= 25

        ) {
            $uploader = new Core_Api_ProjectClass();

            $fname = $_FILES[$id]['name'];
            if ( strrpos($fname, '/') !== FALSE ) {
                $fname = substr($fname, strrpos($fname, '/'));
            }

            /**
             * Generate unig name for file
             * @author Artem Sukharev
             * @see https://tickets.fieldsolutions.com/issues/12180
             */
            $info  = pathinfo($fname);
            $fname = uniqid($info['filename'].'_');
            $fname = $fname.'.'.$info['extension'];
            $fdata = file_get_contents($_FILES[$id]['tmp_name']);

            $result = $uploader->uploadFile($this->_login."|pmContext=$company", $this->_password, $project_id, $fname, $fdata, $descr, $action, $id);
           
            if ( !empty($result->errors) ) {
                $return['success'] = 0;
                $return['errors'] = array();
                foreach ($result->errors as $e) {
                    $return['errors'][]=$e->message;
                }
            } else {
            	$return['success'] = 1;
            	if ($id == "CustomSignOff") {
			    	require_once ("../htdocs/library/fpdf/fpdf.php");
			    	require_once ("../htdocs/library/fpdi/fpdi.php");

			    	$fpdf = new FPDI ();
			    	$pagecount = $fpdf->setSourceFile($_FILES[$id]['tmp_name']);

			    	if ($pagecount > 0) {
            			$uploader->updateAllWOs_CustomSignOff ($project_id, $fname, $option, $id);
			    	}
			    	else {
			    		$return["success"] = 0;
		                $return['errors'] = array("Custom Signoff must be an unencrypted PDF");
			    	}
            	}
            	/*if (!$uploader->updateAllWOs_CustomSignOff ($project_id, $fname, $option)) {
					$return['success'] = 0;
					$return['errors'] = array("Failed to update work order files");
				}
            	else {
            		$return['success']=1;
            	}//*/
            }
        } else {
            $return['success']=0;
            $return['errors']=array();
            if ( empty($id) ) {
                $return['errors'][]='Incorrect parameters: ID is required and cannot be empty';
            } else if ( empty($_FILES["{$id}"]) ) {
                $return['errors'][]= 'File not uploaded';
            } else if ( !is_readable($_FILES["{$id}"]['tmp_name']) ) {
                $return['errors'][]= 'Uploaded file not readabale';
            } else if ( filesize($_FILES["{$id}"]['tmp_name']) > $maxFileSize ) {
                $return['errors'][]= 'Uploaded file size too big';
            } else if ( $_FILES["{$id}"]['error'] != UPLOAD_ERR_OK ) {
                switch ( $_FILES["{$id}"]['error'] ) {
                    case UPLOAD_ERR_FORM_SIZE :
                        $return['errors'][]= 'Uploaded file size too big';
                        break;
                    case UPLOAD_ERR_PARTIAL :
                        $return['errors'][]= 'The uploaded file was only partially uploaded';
                        break;
                    case UPLOAD_ERR_NO_FILE :
                    case UPLOAD_ERR_NO_TMP_DIR :
                    case UPLOAD_ERR_CANT_WRITE :
                    case UPLOAD_ERR_EXTENSION :
                        $return['errors'][]= 'No file was uploaded';
                }
            }else if(mb_strlen($descr, 'utf-8') >25){
                 $return['errors'][]= 'Optional description for uploaded file should containes less then 25 characters.';
              }
        }
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-Length', strlen($return), true);
        $response->setBody($return);
    }
    
    //14002
  public function ajaxgetinfoAction(){
		$this->_helper->viewRenderer->setNoRender(true);
        $params    = $this->getRequest()->getParams();
        $prjClass  = new Core_Api_ProjectClass();
        $return    = array('success'=>0);
        $companyId = $params['Project_Company_ID'];

        $prj = $prjClass->getProjects(
            $this->_login.'|pmContext='.$companyId,
            $this->_password,
            $params['Project_ID'],
            $companyId
        );

        if ($prj->success && !empty($prj->data[0]->Project_ID)) {
			$return['success'] = 1;
			$return['project'] = $prj->data[0];
		}
		
		$this->_helper->json(
										array(
														'success'=>$return['success'],
														'isProjectAutoAssign'=> $prj->data[0]->isProjectAutoAssign,
														'StartTimeRequired' => $prj->data[0]->RequiredStartDateTime,
														'EndTimeRequired'=> $prj->data[0]->RequiredEndDateTime
												)
									);
	}
    public function listAction()
    {
        $request = $this->getRequest();
        
        $size    = ($request->getParam('size', 10)) + 0;
        $size    = ( $size <= 10 ) ? 10 : ( ( $size >= 100 ) ? 100 : $size );
        $page    = ($request->getParam('page', 1)) + 0;
        $page    = ( $page < 1 ) ? 1 : $page;
        
        $sortBy = $request->getParam('sortBy', 'Project_ID');
        $sortDir = $request->getParam('sortDir', 'asc');
        
        $this->view->sortBy = $sortBy;
        $this->view->sortDir = $sortDir;
        
        $params = $request->getParams();
        
        $companyId = $this->view->company;

        $search = new Core_Api_Class;

        $countRows = 0;
        
            // Save workorders into CSV file
        if ( $request->getParam('download', false) === 'csv' ) {
            try {
                ob_start();
                
                $projects = $search->getProjectsList(
                    $this->_login.'|pmContext='.$companyId,
                    $this->_password,
                    $params,
					'Project_ID ASC'
                );

                $res = array( "Project_ID","Project_Name","Headline","Project_Manager","Resource_Coordinator","Client_Company_Name","Active","AutoBlastOnPublish");
                $this->putcsv($res);
                
                if ($projects->success )
                foreach ($projects->data as $project) {
                    $row = array(
                        $project->Project_ID,
                        $project->Project_Name,
                        $project->Headline, 
                        $project->Project_Manager, 
                        $project->Resource_Coordinator,
                        $project->Client_Name, 
                        ($project->Active) ? 'yes' : 'no',
                        ($project->AutoBlastOnPublish) ? 'yes' : 'no'
                    );
                    $this->putcsv($row);
                }
                $csv = ob_get_contents();
                ob_end_clean();
                
                return $this->_outputCsv($csv);
            } catch (Exception $e) {
                print 'error occurred';
                return false;
            }
        }
        
        //getProjectsList($user, $pass, $filters, $sort, $offset = 0, $limit = 0, &$count)
        $projects = $search->getProjectsList(
                $this->_login.'|pmContext='.$companyId,
                $this->_password,
                $params,
                (!empty($sortBy) && !empty($sortDir) ) ? "{$sortBy} {$sortDir}" : null,
                ($page-1)*$size,
                $size,
                $countRows
        );
        
		$filters = array('Company_ID' => $companyId);
		$this->view->siteCount = Core_Site::getSiteCount($companyId);

        if ( $projects->success ) {
            $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($projects->data, $countRows));
            $paginator->setCurrentPageNumber($page);
            $this->view->paginator = $paginator;
        }
    
        //$this->view->projects = $projects;
        $this->view->success    = $projects->success;
        $this->view->errors     = $projects->errors;
        
    }
    
    public function updateOptionsAction()
    {
    }

    private function putcsv($row, $fd=',', $quot='"')
    {
       $str='';
       foreach ($row as $cell)
       {
          $cell = str_replace($quot, $quot.$quot, $cell);
          if (strchr($cell, $fd) !== FALSE || strchr($cell, $quot) !== FALSE || strchr($cell, "\n") !== FALSE)
          {
             $str .= $quot.$cell.$quot.$fd;
          }
          else
          {
             $str .= $cell.$fd;
          }
       }
       echo substr($str, 0, -1)."\n";
       
       return strlen($str);
    }
    
    
    private function _outputCsv($csvText)
    {
        $this->_helper->viewRenderer->setNoRender();
        $response = $this->getResponse();
        $response->clearHeaders();
        $response->clearBody();
        $response->setHeader('Pragma', 'public')
                ->setHeader('Expires', '0')
                ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
                ->setHeader('Cache-Control', 'private')
                ->setHeader('Content-Type', 'text/csv')
                ->setHeader('Content-Disposition', 'attachment; filename='.$this->_company.'-'.ucfirst('projects').'-'.date('mdY').'.csv', true)
                ->setHeader('Content-Transfer-Encoding', 'binary');
        $response->sendResponse();

        //Flush buffers
        ob_end_flush();
        flush();
        
        echo $csvText;

        exit;
    }

    private function getHtmlString($text)
    {
        //$ret = str_replace("\n\r","<br/>www",$text);
        //$ret = str_replace("\r\n","<br/>eee",$text);
        //$ret = str_replace("\r","<br/>fff",$text);
        //$ret = str_replace("\n","<br/>ttt",$text);
        
        $ret = $text;
        $ret = str_replace("?","'",$ret);
		$ret = htmlentities($ret);
//        $ret = nl2br($ret);
        
        return $ret;
}
    // 14125
    public function copyProjectAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);

        $project_id = $this->getRequest()->getParam('project_id');
        $companyId = $this->getRequest()->getParam('company');
        $prjClass  = new Core_Api_ProjectClass();
        $return    = array('success'=>0);
       
        $prj = $prjClass->getProjects(
            $this->_login.'|pmContext='.$companyId,
            $this->_password,
            $project_id,
            $companyId
        ); 
        if ($prj->success && !empty($prj->data[0])) {
            $prjold = $prj->data[0];            
            $project = clone $prjold;   
            $project->Owner = $this->_login;
            $project->CreatedBy = $this->_login;          
            $projectCopy = $prjClass->copyProject($this->_login.'|pmContext='.$companyId, $this->_password, $project);
            if ($projectCopy->success && !empty($projectCopy->data[0])) {
                $return['success'] = 1;
                $return['project_id'] = $projectCopy->data[0]->Project_ID;
            }
            $this->_helper->json(array(
                                    'success'=>$return['success'],
                                    'project_id'=> $return['project_id']                                    
                                )
            );
        }		
	
    }
}

