<?php
class Dashboard_TechController extends Core_Controller_Action
{
    const WO_FIELDS = 'TB_UNID,WO_ID,DateEntered,PO,ShowTechs,SourceByDate,ShortNotice,StartDate,StartTime,EndDate,EndTime,StartRange,Duration,Region,Type_ID,Project_ID,WO_Category_ID,Headline,Site_Contact_Name,SiteName,SiteNumber,Project_Name,Address,City,State,Zipcode,SitePhone,SiteFax,SiteEmail,Update_Requested,Update_Reason,Description,Requirements,Tools,SpecialInstructions,Lead,Assist,SignOffSheet_Required,ProjectManagerName,ProjectManagerPhone,ProjectManagerEmail,ResourceCoordinatorName,ResourceCoordinatorPhone,ResourceCoordinatorEmail,EmergencyName,EmergencyPhone,EmergencyEmail,TechnicalSupportName,TechnicalSupportPhone,TechnicalSupportEmail,CheckInOutName,CheckInOutNumber,CheckInOutEmail,Tech_ID,Tech_FName,Tech_LName,NoShow_Tech,BackOut_Tech,FLS_ID,BU1FLSID,BU1Info,Backup_FLS_ID,Ship_Contact_Info,PayMax,Amount_Per,Qty_Devices,Tech_Bid_Amount,WorkOrderReviewed,TechCheckedIn_24hrs,CheckedIn,Checkedby,PreCall_1,PreCall_2,CheckInNotes,StoreNotified,Notifiedby,DateNotified,NotifyNotes,TechMarkedComplete,Date_In,Time_In,Date_Out,Time_Out,ContactHD,HDPOC,TechMiles,TechComments,AttachedFile,FLS_OOS,Unexpected_Steps,Unexpected_Desc,AskedBy,AskedBy_Name,Paperwork_Received,Incomplete_Paperwork,MissingComments,Site_Complete,CallClosed,PartsProcessed,ClientComments,Work_Out_of_Scope,OutofScope_Amount,OutofScope_Reason,TripCharge,MileageReimbursement,MaterialsReimbursement,Additional_Pay_Amount,Penalty_Reason,AbortFee,AbortFeeAmount,Add_Pay_AuthBy,Add_Pay_Reason,calculatedTechHrs,baseTechPay,PayAmount,Approved,TechPaid,DatePaid,Deactivated,DeactivationCode,Deactivated_Reason,General1,General2,General3,General4,General5,General6,General7,General8,General9,General10,FS_InternalComments,Username,Invoiced,Company_Name,Company_ID,TechEmail,TechPhone,TechPhoneExt,Date_Assigned,lastModBy,lastModDate,approvedBy,DateApproved,DeactivatedDate,DeactivatedBy,Latitude,Longitude,PricingRan,WO_Category,DateIncomplete,ReminderAcceptance,Reminder24Hr,Reminder1Hr,CheckInCall,CheckOutCall,ReminderNotMarkComplete,ReminderIncomplete,ReminderAll,SMSBlast,MinutesRemainPublished,MinimumSelfRating,MaximumAllowableDistance,isProjectAutoAssign,isWorkOrdersFirstBidder,P2TPreferredOnly,P2TAlreadyAssigned,SATRecommended,SATPerformance,ReturnPartCount,NewPartCount,Qty_Applicants,Status,DateInvoiced';

    private $_techID;

    public function init()
    { 
        //  This is constroller can be used for AJAX only!
//        $request = $this->getRequest();
//        if ( !$request->isXmlHttpRequest() ) {
//            if ( $this->getRequest()->getParam('download', false) !== 'csv' ) {
//                $this->_redirect('/');
//            }
//        }

        parent::init();
        $this->_techID = $_SESSION['TechID'];
        $auth = new Zend_Session_Namespace('Auth_User');
        //13624
        if ($auth->loggedInAs != 'tech' &&  $auth->loggedInAs != 'admin') {
            throw new Core_Auth_Exception('Authentication required');
        }
    }

    public function getPayStatusAction()
    {
        $request = $this->getRequest();
        $win = $request->getParam('win',null);
        $StartDateFrom = $request->getParam('StartDateFrom',null);
        $StartDateTo = $request->getParam('StartDateTo',null);
        $DatePaidFrom = $request->getParam('DatePaidFrom',null);
        $DatePaidTo = $request->getParam('DatePaidTo',null);
        
        $_size    = ($request->getParam('size', 10)) + 0;
        $_size    = ( $_size <= 10 ) ? 10 : ( ( $_size >= 100 ) ? 100 : $_size );
        $_page    = ($request->getParam('page', 1)) + 0;
        $_page    = ( $_page < 1 ) ? 1 : $_page;


        $this->view->searchParameters = array();

        $search = new Core_Api_TechClass;
        $filters = new API_WorkOrderFilter;
        $filters->WO_State = WO_STATE_COMPLETED_TAB;
        $filters->Tech = $this->_techID+0;
        //var_dump($filters->Tech);exit;
        //$filters->TechPaid = 1;
        //$filters->Sourced = false;
        //$filters->Tech_ID = $this->_techId;
        if ($win) {
            $filters->TB_UNID = $win;
            $this->view->searchParameters['win']=$win;
        }
        if ($StartDateFrom) {
            $filters->StartDateFrom = $StartDateFrom;
            $this->view->searchParameters['StartDateFrom']=$StartDateFrom;
        }
        if ($StartDateTo) {
            $filters->StartDateTo = $StartDateTo;
            $this->view->searchParameters['StartDateTo']=$StartDateTo;
        }
        if ($DatePaidFrom) {
            $filters->DatePaidFrom = $DatePaidFrom;
            $this->view->searchParameters['DatePaidFrom']=$DatePaidFrom;
        }
        if ($DatePaidTo) {
            $filters->DatePaidTo = $DatePaidTo;
            $this->view->searchParameters['DatePaidTo']=$DatePaidTo;
        }
        $countRows = -1;
        //var_dump($this->view->searchParams);var_dump(array(($_page-1)*$_size,$_page*$_size));exit;
        $wo = $search->getWorkOrders($this->_login,$this->_password,self::WO_FIELDS,$filters,($_page-1)*$_size,$_page*$_size,$countRows);
        if ( $wo->success && $countRows>0 && !empty($wo->data) ) {
            $_wo = range(0, $countRows-1);
            for ( $j = 0, $i = ($_page-1)*$_size; $i < $_page*$_size && $i < $countRows; ++$i, ++$j ) {
                $_wo[$i] = $wo->data[$j];
            }
            $wo->data = $_wo;
            unset($_wo);
        }
        if ( $wo->success ) {
            $paginator = new Zend_Paginator(new Zend_Paginator_Adapter_Array($wo->data));
            $paginator->setCurrentPageNumber($_page);
            $this->view->paginator = $paginator;
        }
        $this->view->success    = $wo->success;
        $this->view->errors     = $wo->errors;
    }

    public function statusPopupAction()
    {

    }

    public function printPopupAction()
    {
        //ABSOLETE
        $win = $this->getRequest()->getParam('win',null);
        $search = new Core_Api_TechClass;
        $wo = $search->getWorkOrder($this->_login,$this->_password,$win);
        if ($wo && $wo->success && !empty($wo->data)) {
            $wo = $wo->data[0];
        }else{
            throw new Exception();
        }
        $this->view->wo = $wo;
    }
    public function documentsAction()
    {
        $request    = $this->getRequest();
        $win        = $request->getParam('win', false);

        $cacheKey = Core_Cache_Manager::makeKey($this->_login, 'documents', 'win', $win); //  1 key for whole controller!
        $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');

        $search = new Core_Api_TechClass;

        if ( false === ($wo = $cache->load($cacheKey)) ) {
            $wo = $search->getWorkOrder($this->_login,$this->_password,$win);
            if ($wo && $wo->success && !empty($wo->data)) {
                $wo = $wo->data[0];
            }
            $cache->save($wo, $cacheKey, array('projects'), LIFETIME_15MIN);
        }
        $files = new Core_Files();
        $select = $files->select();
        $select->where('WIN_NUM = ?', $win)
               ->where('type = ?', Core_Files::WOS_FILE);
        $filesList = $files->fetchAll($select);
        $this->view->files = $filesList;

        $prj_id = $wo->Project_ID;
        $this->view->prjFiles = null;
        if ($prj_id){
        /*    $cacheKey = Core_Cache_Manager::makeKey($this->_login, 'projects', 'Project_ID', $prj_id); //  1 key for whole controller!
            $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');

            if ( false === ($projects = $cache->load($cacheKey)) ) {*/
                $prjFilesObj = new Core_ProjectsFiles();
                $select = $prjFilesObj->select();
                $select->where('Project_ID = ?', $prj_id);
                $prjFiles = $prjFilesObj->fetchAll($select);
                
                //pull info for External File in project table
                $db = Core_Database::getInstance();
                $extSelect = $db->select();
                $extSelect->from(Core_Database::TABLE_PROJECTS, array("External_Files"))
                		  ->where("Project_ID = ?",$prj_id);
                $extFiles = Core_Database::fetchAll($extSelect);  

                /*$cache->save($prjFiles, $cacheKey, array('projects'), LIFETIME_15MIN);
            }*/
            $this->view->prjFiles = $prjFiles;
            $this->view->prjExtFile = $extFiles;
        }

        $this->view->hostNoSSL = "http://" . $_SERVER["HTTP_HOST"];
    }

    public function fileDownloadAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $request  = $this->getRequest();

        $filename = $request->getParam('filename', false);
        if ( $filename !== false ) {
            $filename = base64_decode($filename);
        }

        $response = $this->getResponse();
        $response->clearHeaders();
        $response->clearBody();
        $response
            ->setHeader('Content-Type', 'application/octet-stream', true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
            ->setHeader("Content-Disposition", "attachment; filename=\"$filename\"")
            ->setHeader('Last-Modified', date('D, d M Y H:i:s T'), true);

        $getter = new Core_Api_TechClass;
        $file   = $getter->downloadFile($this->_login, $this->_password, $filename);
        if ( $file === NULL || $file instanceof API_Response ) {
            $response->setHeader('Content-Length', 0, true)
                ->setBody('');
        } else {
            //$file = base64_decode($file);
            $response->setHeader('Content-Length', strlen($file), true)
                ->setBody($file);
        }
    }

    public function printWoAction()
    {
    	require_once ("../htdocs/library/dompdf-0.6b/dompdf_config.inc.php");
		$autoloader = Zend_Loader_Autoloader::getInstance();  
		$autoloader->pushAutoloader("DOMPDF_autoload");

    	$request = $this->getRequest();
        $win = $request->getParam('win',null);
        $options = array();
        $options['Map'] = $request->getParam('Map',false);

        $search = new Core_Api_TechClass;
        $wo = $search->getWorkOrder($this->_login,$this->_password,$win);
        if ($wo && $wo->success && !empty($wo->data)) {
            $wo = $wo->data[0];
        }else{
            //throw new Exception();
        }

        $parts = $search->getParts($this->_login,$this->_password,$win);
        if ($parts && $parts->success && !empty($parts->data)) {
            $this->view->parts = $parts->data;
        }else{
            $this->view->parts = null;
        }

        $project = $search->getProjects($this->_login,$this->_password,array((int)$wo->Project_ID),null);
        if ($project->success) {
            $this->view->project_info = $project->data[0];
        }else{
            $this->view->project_info = null;
        }

        // get client info
        if (!empty($wo->Company_ID)) {
            $this->view->companyLogo = $search->getCompanyLogo($this->_login,$this->_password, $wo->Company_ID);
        }
                
        $this->view->wo = $wo;
        $this->view->view_options = $options;

        $Core_Api_WosClass = new Core_Api_WosClass; 
        $this->view->AllWOVisitListClient = $Core_Api_WosClass->GetAllWOVisitList($win,1,1);
        
        //get custom fields for tech wo, where cf are either related to the project for win or related to the owner for win
		/*$cf = new Core_CustomField();
		$client = new Core_Api_User();
		$client->lookupUsername($wo->Username);
		$this->view->customFields = $cf->getTechWorkOrderCustomFields($client->getCompanyId(),$wo->Project_ID);
		*/
		$sections = $this->view->render('tech-wo-details/sections.phtml');
        
        $this->view->errors = $_dompdf_warnings;
        $this->view->pdf = new DOMPDF();
    }

	public function printSignAction()
	{
    	require_once ("../htdocs/library/dompdf-0.6b/dompdf_config.inc.php");
    	//require_once ("../htdocs/library/dompdf-0.6b/include/dompdf.cls.php");
    	require_once ("../htdocs/library/fpdf/fpdf.php");
    	require_once ("../htdocs/library/fpdi/fpdi.php");
    	$autoloader = Zend_Loader_Autoloader::getInstance();  
		$autoloader->pushAutoloader("DOMPDF_autoload");

    	$request = $this->getRequest();
        $win = $request->getParam('win',null);
        $this->view->visit = $request->getParam('visit',0);
        $Core_Api_WosClass = new Core_Api_WosClass; 
        $this->view->AllWOVisitListClient = $Core_Api_WosClass->GetAllWOVisitList($win,1,1);
        $isCustom = false;
        $altContent = null;
        $altFilename = "";
        
        $search = new Core_Api_TechClass;
        $wo = $search->getWorkOrder($this->_login,$this->_password,$win);
		if ($wo && $wo->success && !empty($wo->data)) {
            $wo = $wo->data[0];
        }else{
            throw new Exception();
        }
        
        $project = $search->getProjects($this->_login,$this->_password,array((int)$wo->Project_ID),null);
        if ($project->success) {
            $this->view->project_info = $project->data[0];
        }else{
            $this->view->project_info = null;
        }

        // get client info
        if (!empty($wo->Company_ID) && $wo->CustomSignOff_Enabled == "1") {
        	$this->view->companyLogo = $search->getCompanyLogo ($this->_login,$this->_password, $wo->Company_ID);
        	$files = $search->getWODocList ($this->_login, $this->_password, $win);

        	//Check work order files for custom sign off sheet
			foreach ($files->data as $file) {
				if ($file["type"] == "signoff") {
					$signoff_file = new Core_File ();
            		$altFilename = ltrim (strstr ($file["path"], "/"), "/");
            		if (strstr ($file["path"], "/", true) == "projects-docs") {
            			$altDirectory = S3_PROJECTS_DOCS_DIR;
            		}
            		else {
            			$altDirectory = S3_CLIENTS_DOCS_DIR;
            		}
            		if (!empty ($altFilename)) {
	            		$altContent = $signoff_file->downloadFile ($altFilename, $altDirectory);
	            		$isCustom = true;
            		}
            	}
            }

            /*if (empty ($altContent)) {
				//If no custom sign off in WO, check in project
				if ($this->view->project_info->CustomSignOff) {
					$signoff_file = new Core_File ();
            		$altFilename = $this->view->project_info->CustomSignOff;
            		if (!empty ($altFilename)) {
	            		$altContent = $signoff_file->downloadFile ($altFilename, S3_PROJECTS_DOCS_DIR);
	            		$isCustom = true;
            		}
            	}

            	if (empty ($altContent)) {
            		//If no custom sign off in project, check in company
            		$signoff_file = new Core_File ();
					$altFilename = $search->getSignoffSheet ($this->_login, $this->_password, $wo->Company_ID);
					$altFilename = $altFilename->data;
					if (!empty ($altFilename)) {
						$altContent = $signoff_file->downloadFile ($altFilename, S3_CLIENTS_DOCS_DIR);
	            		$isCustom = true;
					}
				}
            }*/
		}

		$tempfile = uniqid("customsignoff_");
		$tempfile = $tempfile . ".pdf";

        $this->view->wo = $wo;
        $this->view->altContent = $altContent;
        $this->view->altFilename = $altFilename;
        //$this->view->altDirectory = $altDirectory;
        $this->view->isCustom = $isCustom;
        $this->view->tempfile = $tempfile;
        $this->view->pdf = new DOMPDF();
        $this->view->fpdf = new FPDI ();

        $this->view->errors = $_dompdf_warnings;
	}

    public function findworkorderAction()
    {
        require_once 'Core/Api/TechClass.php';

        $win  = $this->getRequest()->getParam('win',NULL);
        $woId = $this->getRequest()->getParam('wo',NULL);

        $search = new Core_Api_TechClass;
        $response = $this->getResponse();
		//14016
        $woId = trim($woId);
		
		if (!empty($win) || !empty($woId)) {
			if($search->isRestrictedByClientAndWoPublished($this->_techID,$win,$woId))
			{
					$error  = Core_Api_Error::getInstance();
					$error->addError(8, "This Work Order is unavailable.");//14016
					$jsonContext = json_encode(API_Response::fail());
					$this->_helper->viewRenderer->setNoRender();
					$response->setHeader('Content-Length', strlen($jsonContext), true)
						->setBody($jsonContext);
					return;
			}
        	//019
			if($search->isRestrictedByClient($this->_techID,$win,$woId))
			{
				$error  = Core_Api_Error::getInstance();
				$error->addError(8, "Your access to this Work Order has been restricted.");
				$jsonContext = json_encode(API_Response::fail());
				$this->_helper->viewRenderer->setNoRender();
				$response->setHeader('Content-Length', strlen($jsonContext), true)
					->setBody($jsonContext);
				return;
			}
        }

		//end 019   
        if (!empty($win)) 
            {
            $this->_helper->viewRenderer->setNoRender();
            $response->setHeader('Content-Type', 'text/json', true);                       
            $jsonContext = json_encode($search->getWorkOrder($this->_login,$this->_password,$win));
            $response->setHeader('Content-Length', strlen($jsonContext), true)
                 ->setBody($jsonContext);
                
        } elseif (!empty ($woId)) {
            $this->_helper->viewRenderer->setNoRender();
            $response->setHeader('Content-Type', 'text/json', true);
            $woInfo = $search->getWorkOrderByClientWorkOrder($woId);             
            if (!empty($woInfo)) {
                $win = $woInfo["WIN_NUM"];
            }else
            {
                $error  = Core_Api_Error::getInstance();
                $error->addError(8, "This Work Order is unavailable.");
                $jsonContext = json_encode(API_Response::fail());
                $response->setHeader('Content-Length', strlen($jsonContext), true)
                    ->setBody($jsonContext);     
            }
            $woInfo = $search->getWorkOrder($this->_login,$this->_password,$win);                      
            //
//            $filters = new API_WorkOrderFilter;
//            $filters->WO_ID = trim($woId);
//            $woInfo = $search->getWorkOrders(
//                $this->_login,
//                $this->_password,
//                'WIN_NUM,WO_ID,Tech_Bid_Amount,Amount_Per,Deactivated,DeactivationCode,Deactivated_Reason,PayMax',
//                $filters,
//                0,0,$countRows,''
//            );//
            $jsonResponce = json_encode($woInfo);
            if (!empty($woInfo) && $woInfo->success && empty($woInfo->data) ) {
                $jsonResponce = '{"success":false,"errors":[{"code":2,"type":"Required fields are not set","message":"' . Core_Api_TechClass::ERROR_WO_INVALID_WO . '"}],"data":null}';
            }
            $jsonContext = $jsonResponce;
            $response->setHeader('Content-Length', strlen($jsonContext), true)
                 ->setBody($jsonContext);
        }//14016
        }

    public function createBidAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        $winNum = $request->getParam('woID', NULL);
        $techID = $request->getParam('techID', NULL);
        $bidAmount = $request->getParam('BidAmount', NULL);
        $comments = $request->getParam('comments', '');

        $isRequestValid = array();
        if ( !$winNum ) $isRequestValid[] = 'Work order id is required.';
        if ( !$bidAmount ) $isRequestValid[] = 'Bid amount id is required.';
        elseif ( (double) $bidAmount <= 0 ) $isRequestValid[] = 'Incorrect Bid amount.';

        if ( !$isRequestValid ) {
            $search = new Core_Api_TechClass();
            $result = $search->createBid( $this->_login, $this->_password, $winNum, $bidAmount, $comments);

            if ( $result->success ) {
                $return = array('success' => 1, 'errors' => array());
            } else {
                $return = array('success' => 0, 'errors' => array());
                foreach( $result->errors as $error ) $return['errors'][] = $error->message;
            }
        } else {
            $return = array('success' => 0, 'errors' => $isRequestValid);
        }

        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                 ->setBody($return);
    }
    public function createWithdrawbidAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        $winNum = $request->getParam('woID', NULL);
        $techID = $request->getParam('techID', NULL);
        $bidAmount = $request->getParam('BidAmount', NULL);
        $comments = $request->getParam('comments', '');

        $isRequestValid = array();
        if (!$winNum)
            $isRequestValid[] = 'Work order id is required.';
        if (!$bidAmount)
            $isRequestValid[] = 'Bid amount id is required.';
        elseif ((double) $bidAmount <= 0)
            $isRequestValid[] = 'Incorrect Bid amount.';

        
            $search = new Core_Api_TechClass();
            $result = $search->createWithdraw($this->_login, $this->_password, $winNum, $bidAmount, $comments);
        if ($result->success)
        {
                $return = array('success' => 1, 'errors' => array());
        } else
        {
                $return = array('success' => 0, 'errors' => array());
            foreach ($result->errors as $error)
                $return['errors'][] = $error->message;
            }
        

        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                 ->setBody($return);
    }
    
    /**
     * update bid action
     * @author Alex Scherba
     */
    public function updateBidAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        $win = $request->getParam('win', NULL);
        $bidAmount = $request->getParam('BidAmount', NULL);
        $comments = $request->getParam('Comments', '');

        $isRequestValid = array();
        if ( !$win ) $isRequestValid[] = 'Work order id is required.';
        //if ( empty($comments) ) $isRequestValid[] = 'Comments is required.';
        if ( !$bidAmount ) $isRequestValid[] = 'Bid amount is required.';
        elseif ( (double) $bidAmount <= 0 ) $isRequestValid[] = 'Incorrect Bid amount.';

        if ( !$isRequestValid ) {
            $search = new Core_Api_TechClass();
            $result = $search->updateBid( $this->_login, $this->_password, $win, $bidAmount, $comments);

            if ( $result->success ) {
                $return = array('success' => 1, 'errors' => array());
            } else {
                $return = array('success' => 0, 'errors' => array());
                foreach( $result->errors as $error ) $return['errors'][] = $error->message;
            }
        } else {
            $return = array('success' => 0, 'errors' => $isRequestValid);
        }

        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                 ->setBody($return);
    }

    /**
     * applied wo action
     * @author Alex Scherba
     */
    public function appliedWoAction()
    {
        require_once 'Core/Api/TechClass.php';
        $win = $this->getRequest()->getParam('win',NULL);
        if ($win) {
            $search = new Core_Api_TechClass;
            $bid = $search->getBid($this->_login, $this->_password, $win);
            if ($bid && $bid->success && !empty($bid->data))
                $this->view->bid = $bid->data[0];
            $wo = $search->getWorkOrder($this->_login,$this->_password,$win);
            if ($wo && $wo->success && !empty($wo->data))
                $this->view->wo  = $wo->data[0];
            $this->view->success = $wo->success && $bid->success;
            $errors = !empty($wo->errors)?$wo->errors:array();
			$prj_id = $wo->success ? $wo->data[0]->Project_ID : '';
			$project = $search->getProjects($this->_login,$this->_password,array($prj_id),'');
			$this->view->prj = $project->success ? $project->data[0] : new stdClass();

            $this->view->errors = !empty($bid->errors)?array_merge($errors, $bid->errors):$errors;
        }
    }
    
    
    
    
    /**
     * Dwonload file from s3
     * @param string $type
     * @throws Zend_Exception
     */
    private function fileDownloadS3($type)
    {
        if (empty($this->_login) || empty($this->_password) ) {
            // no authentification
            throw new Core_Auth_Exception();
        }
        
        // fix IE SSL download bug
        if (isset($_SERVER['HTTP_USER_AGENT']) &&  (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)
            && !empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on') {
            
                header('Location: http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
                exit;
        }
        
        $this->_helper->viewRenderer->setNoRender();

        $showZero = false;
        
        $request  = $this->getRequest();

        $filename = $request->getParam('filename', false);
        $encType = $request->getParam('type', 'base');
        $showAction = $request->getParam('sact', 'down');

        $mimeType = 'application/octet-stream';
        
        if ( $filename !== false && $encType == 'base' ) {
            $filename = base64_decode($filename);
        //} else {
            $filename = urldecode($filename);
        }

        if ($showAction == 'image') {
            $fileImageType = Core_Image::getImageTypeByFileName($filename);
            
            if (!empty($fileImageType) ) {
                $mimeType = Core_Image::getMimeByImgType($fileImageType);
            } else {
                $showZero = true;
            }
        }
       
        //$_f = preg_replace('/[^-.a-z0-9_\/]+/i', '_', $filename);
        $response = $this->getResponse();
        $response->clearHeaders();
        $response->clearBody();
        
        $filename = ltrim($filename, '/');
        
        $file = null;
        switch ($type) {
            case 'wo_docs':
                $file = API_WorkOrder::getFile($filename);
                break;
            case 'project':
                $getter = new Core_Api_ProjectClass();
                $file = $getter->getFile($filename);
                break;
            default:
                throw new Zend_Exception('Wrong type');
                break;
        }

        if ( $file === NULL || $file instanceof API_Response || $showZero) {
            
            if (0 && $encType == 'base') { /*disables caspio logic*/
                // check caspio for file
                $this->fileDownloadAction();
            } else {
                // not found
                $this->getResponse()->setHeader('HTTP/1.1','404 Not Found');
                $this->getResponse()->setHeader('Status','404 File not found');
                $this->getResponse()->setBody('<h1>404 File not found</h1>');
            }
        } else {
            $response
            ->setHeader('Content-Type', $mimeType, true)
            ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
            ->setHeader("Content-Disposition", "attachment; filename=\"$filename\"")
            ->setHeader('Last-Modified', date('D, d M Y H:i:s T'), true);
            
            //$file = base64_decode($file);
            $response->setHeader('Content-Length', strlen($file), true)
                ->setBody($file);
        }
    }
    
    /**
     * Download WOS documents
     */
    public function fileWosDocumentsDownloadAction()
    {
        $this->fileDownloadS3('wo_docs');
    }
    
    
    /**
     * Download project documents
     */
    public function fileProjectDocumentsDownloadAction()
    {
        $this->fileDownloadS3('project');
    }
	
	public function updateAction() {
		$common = new Core_Api_CommonClass;
		$statesOptions = $common->getStatesArray('US');
		$countryOptions = $common->getCountries();
		
		$this->view->techID = $this->_techID;
		
		$this->view->states = $statesOptions->data;
		$this->view->countries = $countryOptions->data;
								
		$bankStates = $common->getStatesArray();
		$this->view->bankStates = $bankStates->data;
		
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from(Core_Database::TABLE_COUNTRIES, array('id', 'Name'))->order('DisplayPriority', 'Name');
		
		$bankCountries = $db->fetchPairs($select);
		$this->view->bankCountries = $bankCountries;		

		$certMapping = new Core_TechCertifications();
		$this->view->certMapping = $certMapping->getMapping();

	}
        
        /**
     *  
     */
    function updaterequitephoneAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getRequest()->getParams();
        $id = $params['id'];
        $phonetype = $params['phonetype'];
        $usertype = $params['usertype'];
        $Core_Api_TechClass = new Core_Api_TechClass;
        $aClient = null;
        $requite = 0;
        switch ($phonetype) {
            case 1:
                $aClient = $Core_Api_TechClass->setPrimaryPhoneValid($id,$usertype, $this->_login);
                if (!empty($aClient)) {
                    $requite = $aClient[0]['PrimaryPhoneStatus'];
}
                break;
            case 2:
                $aClient = $Core_Api_TechClass->setSecondaryPhoneValid($id,$usertype, $this->_login);
                if (!empty($aClient)) {
                    $requite = $aClient[0]['SecondaryPhoneStatus'];
                }
                break;
        }

        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $response->setHeader('Content-Length', strlen($requite), true);
        $response->setBody($requite);
    }
//----------------------------------------------------------
    function copierSkillsAssessmentAction()
    {
        $params = $this->getRequest()->getParams();
        $assId = 1;
        $Core_Api_TechClass = new Core_Api_TechClass();
        if(!isset($params["data"]))
        {    
            $Questions = $Core_Api_TechClass->getQuestions(1);
            $techAssessment = $Core_Api_TechClass->getTechAssessment($this->_techID,$assId);
            $ispassed =  empty($techAssessment['ispassed'])? 0 : $techAssessment['ispassed'];
            $tryingtimes = empty($techAssessment['tryingtimes']) ? 0: $techAssessment['tryingtimes'];
            
            $allowDoTheQuiz = 0;            
            if($ispassed==1)
            {
                $allowDoTheQuiz = 1;
			}
            else if($tryingtimes<2)
            {
                $allowDoTheQuiz = 1;
            }
            
            $yourScrore = $Core_Api_TechClass->getScore($this->_techID,$assId);
            $yourScrore = empty($yourScrore)? 0 : $yourScrore;
            $passedScrore = $Core_Api_TechClass->getPassingScore($assId);
            $passedScrore = empty($passedScrore)? 0 : $passedScrore;
            $totalScrore = $Core_Api_TechClass->getNumOfScoredQuestions($assId);
            $totalScrore = empty($totalScrore)? 0 : $totalScrore;
            
            $this->view->allowDoTheQuiz = $allowDoTheQuiz;
            $this->view->yourScrore = $yourScrore;
            $this->view->passedScrore = $passedScrore;
            $this->view->totalScrore = $totalScrore;
            
            $this->view->techID = $this->_techID;
            $this->view->Questions = $Questions;
            $this->view->Core_Api_TechClass = $Core_Api_TechClass;
            $this->view->post = false;
        }
        else
        {
           $data = $params["data"];
           $json = json_decode("[".base64_decode($data)."]");
           $rf = array();
           if(count($json) > 0)
           {
               for($i= 0;$i < count($json);$i++)
               {
                  if(trim($json[$i]->value) != "")
                  {    
                    $rf[$json[$i]->key] = explode(",",$json[$i]->value);
                  }
                  else
                  {
                      $rf[$json[$i]->key] = array();
                  }    
               }
              
           }
           $Core_Api_TechClass->saveSelectedItems($this->_techID,$assId,$rf);
           
           $techAssessment = $Core_Api_TechClass->getTechAssessment($this->_techID,$assId);
           
           $yourScrore = $techAssessment['last_score'];
           $yourScrore = empty($yourScrore)? 0 : $yourScrore;
           $passedScrore = $Core_Api_TechClass->getPassingScore($assId);
           $passedScrore = empty($passedScrore)? 0 : $passedScrore;
           $totalScrore = $Core_Api_TechClass->getNumOfScoredQuestions($assId);
           $totalScrore = empty($totalScrore)? 0 : $totalScrore;
           
           $ispassed =  empty($techAssessment['ispassed'])? 0 : $techAssessment['ispassed'];
           $tryingtimes = empty($techAssessment['tryingtimes']) ? 0: $techAssessment['tryingtimes'];
           
           $this->view->post = true; 
           $this->view->tryingtimes = $tryingtimes; 
           $this->view->passed = $yourScrore >= $passedScrore ?true:false;
           $this->view->yourScrore = $yourScrore;
           $this->view->passedScrore = $passedScrore;
           $this->view->totalScrore = $totalScrore;
        }    
    }
    
    function computerPlusTagAction()
    {
        
     }

    function blackboxcablingTagAction()
    {
        $params = $this->getRequest()->getParams();
        
        $techID = isset($params['techid'])?$params['techid']:$this->_techID;
        
        $coreApi = new Core_Api_TechClass();
        $testInfo = $coreApi->getBBCablingTestInfo($techID);
        $this->view->TestInfo = $testInfo;
        $this->view->techID = $techID;
    }

    function blackboxtelecomTagAction()
    {
        $params = $this->getRequest()->getParams();
        
        $techID = isset($params['techid'])?$params['techid']:$this->_techID;
        
        $coreApi = new Core_Api_TechClass();
        $testInfo = $coreApi->getBBTelecomTestInfo($techID);
        $this->view->TestInfo = $testInfo;
        $this->view->techID = $techID;
    }

    function blackboxtelecomTagSaveAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $return = array("success" => 0);
        $params = $this->getRequest()->getParams();
    
        $TechID = isset($params['techID'])?$params['techID']:$this->_techID;
        $fieldValues = array();
        $fieldValues['date_taken'] = date("Y-m-d H:i:s");
        $fieldValues['Level'] = $params['TRT'];

        $fieldValues['T1RT_1'] = isset($params['T1RT_1']) ? 1 : 0;
        $fieldValues['T1RT_2'] = isset($params['T1RT_2']) ? 1 : 0;
        $fieldValues['T1RT_3'] = isset($params['T1RT_3']) ? 1 : 0;
        $fieldValues['T1RT_4'] = isset($params['T1RT_4']) ? 1 : 0;
        $fieldValues['T1RT_5'] = isset($params['T1RT_5']) ? 1 : 0;

        $fieldValues['T1RE_1'] = isset($params['T1RE_1']) ? 1 : 0;
        $fieldValues['T1RE_2'] = isset($params['T1RE_2']) ? 1 : 0;
        $fieldValues['T1RE_3'] = isset($params['T1RE_3']) ? 1 : 0;
        $fieldValues['T1RE_4'] = isset($params['T1RE_4']) ? 1 : 0;
        $fieldValues['T1RE_5'] = isset($params['T1RE_5']) ? 1 : 0;
        $fieldValues['T1RE_6'] = isset($params['T1RE_6']) ? 1 : 0;

        $fieldValues['T2RT_1'] = isset($params['T2RT_1']) ? 1 : 0;
        $fieldValues['T2RT_2'] = isset($params['T2RT_2']) ? 1 : 0;
        $fieldValues['T2RT_3'] = isset($params['T2RT_3']) ? 1 : 0;
        $fieldValues['T2RT_4'] = isset($params['T2RT_4']) ? 1 : 0;
        $fieldValues['T2RT_5'] = isset($params['T2RT_5']) ? 1 : 0;
        $fieldValues['T2RT_6'] = isset($params['T2RT_6']) ? 1 : 0;
        $fieldValues['T2RT_7'] = isset($params['T2RT_7']) ? 1 : 0;
        $fieldValues['T2RT_8'] = isset($params['T2RT_8']) ? 1 : 0;
        $fieldValues['T2RT_9'] = isset($params['T2RT_9']) ? 1 : 0;
        $fieldValues['T2RT_10'] = isset($params['T2RT_10']) ? 1 : 0;
        $fieldValues['T2RT_11'] = isset($params['T2RT_11']) ? 1 : 0;
        $fieldValues['T2RT_12'] = isset($params['T2RT_12']) ? 1 : 0;

        $fieldValues['T2RE_1'] = isset($params['T2RE_1']) ? 1 : 0;
        $fieldValues['T2RE_2'] = isset($params['T2RE_2']) ? 1 : 0;

        $TechClass = new Core_Api_TechClass();
        $TechClass->saveBBTelephonyTest($TechID, $fieldValues);

        $return = array("success" => 1, "errors" => "");

        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-Length', strlen($return), true);
        $response->setBody($return);
    }
    
    function blackboxcablingTagSaveAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $return = array("success" => 0);
        $params = $this->getRequest()->getParams();
        $level = 0;

        $TechID = isset($params['techID'])?$params['techID']:$this->_techID;
        $fieldValues = array();
        $fieldValues['date_taken'] = date("Y-m-d H:i:s");
        $fieldValues['Level'] = $params['CRT'];

        $fieldValues['C1RT_1'] = isset($params['C1RT_1']) ? 1 : 0;
        $fieldValues['C1RT_2'] = isset($params['C1RT_2']) ? 1 : 0;
        $fieldValues['C1RT_3'] = isset($params['C1RT_3']) ? 1 : 0;
        $fieldValues['C1RT_4'] = isset($params['C1RT_4']) ? 1 : 0;
        $fieldValues['C1RT_5'] = isset($params['C1RT_5']) ? 1 : 0;

        $fieldValues['C1RE_1'] = isset($params['C1RE_1']) ? 1 : 0;
        $fieldValues['C1RE_2'] = isset($params['C1RE_2']) ? 1 : 0;
        
        $fieldValues['C2RT_1'] = isset($params['C2RT_1']) ? 1 : 0;
        $fieldValues['C2RT_2'] = isset($params['C2RT_2']) ? 1 : 0;
        $fieldValues['C2RT_3'] = isset($params['C2RT_3']) ? 1 : 0;
        $fieldValues['C2RT_4'] = isset($params['C2RT_4']) ? 1 : 0;
        $fieldValues['C2RT_5'] = isset($params['C2RT_5']) ? 1 : 0;
        $fieldValues['C2RT_6'] = isset($params['C2RT_6']) ? 1 : 0;
        $fieldValues['C2RT_7'] = isset($params['C2RT_7']) ? 1 : 0;
        $fieldValues['C2RT_8'] = isset($params['C2RT_8']) ? 1 : 0;
        $fieldValues['C2RT_9'] = isset($params['C2RT_9']) ? 1 : 0;
        $fieldValues['C2RT_10'] = isset($params['C2RT_10']) ? 1 : 0;
        $fieldValues['C2RT_11'] = isset($params['C2RT_11']) ? 1 : 0;
        
        $fieldValues['C2RE_1'] = isset($params['C2RE_1']) ? 1 : 0;
        $fieldValues['C2RE_2'] = isset($params['C2RE_2']) ? 1 : 0;
        $fieldValues['C2RE_3'] = isset($params['C2RE_3']) ? 1 : 0;
        $fieldValues['C2RE_4'] = isset($params['C2RE_4']) ? 1 : 0;
        
        $fieldValues['C3RT_1'] = isset($params['C3RT_1']) ? 1 : 0;
        $fieldValues['C3RT_2'] = isset($params['C3RT_2']) ? 1 : 0;
        $fieldValues['C3RT_3'] = isset($params['C3RT_3']) ? 1 : 0;
        $fieldValues['C3RT_4'] = isset($params['C3RT_4']) ? 1 : 0;
        
        $fieldValues['C3RE_1'] = isset($params['C3RE_1']) ? 1 : 0;
        $fieldValues['C3RE_2'] = isset($params['C3RE_2']) ? 1 : 0;
        $fieldValues['C3RE_3'] = isset($params['C3RE_3']) ? 1 : 0;
        //$return = array ("success" => 1, "errors"=>"");
        $TechClass = new Core_Api_TechClass();
        $TechClass->saveBBCablingTest($TechID, $fieldValues);

        $return = array("success" => 1, "errors" => "");
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-Length', strlen($return), true);
        $response->setBody($return);
    }

    function servrightBrotherCertificationAction()
    {
        $params = $this->getRequest()->getParams();
        $isClicked = false;
        $Core_TechCertifications = new Core_TechCertifications;
        if($_POST && isset($params["saveclicked"]))
        {
            $this->_helper->viewRenderer->setNoRender(true);
            $requite = $this->_techID;
            $Core_TechCertifications->saveWentToBrotherSite($this->_techID,1);
            $response = $this->getResponse();
            $response->clearHeaders()->clearBody();
            $response->setHeader('Content-Length', strlen($requite), true);
            $response->setBody($requite);
        } else if($_POST && isset($params["profile"]))
        {
            $techCerti = new  Core_TechCertification($this->_techID,26);
            $techCertiRow = $techCerti->getRow();  
            $isClicked = $Core_TechCertifications->getWentToBrotherSite($this->_techID)
                    && !empty($techCertiRow);
}
        $this->view->isClicked = $isClicked;
    }
    function ggeTechEvaluationAction()
    {
        $Core_Api_TechClass  = new Core_Api_TechClass;
        $params = $this->getRequest()->getParams();
        if($_POST)
        {
            $this->_helper->viewRenderer->setNoRender(true);
            $inputs = array();
            $correctanswer = 0;
            $correctanswerlist = array(1=>"a", 2=>"h", 3=>"a", 4=>"c", 5=>"d", 6=>"b", 7=>"c", 8=>"h", 9=>"g", 10=>"a",
                                       11=>"e", 12=>"b", 13=>"i", 14=>"h", 15=>"b", 16=>"g", 17=>"b", 18=>"e", 19=>"c", 20=>"f");
            for($i = 1;$i <= 20; $i++)
            {
                $inputs["a".$i."_answer"] = $_POST["a".$i."_answer"];
                if($correctanswerlist[$i] == $_POST["a".$i."_answer"])
                {
                    $correctanswer ++;
            }
            }
            
            $Core_Api_TechClass->saveAnswerForGGETest(
                    $this->_techID,
                    date("Y-m-d H:i:s"),
                    $inputs);

            $times = $Core_Api_TechClass->getTimesForGGETest($this->_techID);
            
            $passScore = 13;
            //--- $score
            $status='';
            
            if($correctanswer < $passScore)
            {
                $status = 'Fail';
            }
            else
            {
                $status = 'Pass';
            }
            if($times == 1)
            {
                $result = $Core_Api_TechClass->saveExts($this->_techID, array('GGE1stAttemptStatus'=>$status,'GGE1stAttemptScore'=>$correctanswer));
            }else
            {
                $result = $Core_Api_TechClass->saveExts($this->_techID, array('GGE2ndAttemptStatus'=>$status,'GGE2ndAttemptScore'=>$correctanswer));
            }
           
            $fileResult = array();
            $fileResult["success"] = 1;
            $fileResult["allowed"] = $Core_Api_TechClass->getTimesForGGETest($this->_techID);
            $fileResult["correctanswer"] = $correctanswer;
            $fileResult["status"] = $status;
            // send email to FS
            $techInfo = Core_Tech::getProfile($this->_techID, true, API_Tech::MODE_TECH, "");
    
            $Core_Api_TechClass = new Core_Api_TechClass();    
            $GGEStatusArray = $Core_Api_TechClass->getStatusArrayForGGECert($techInfo['TechID']);
            $GGE1stAttemptDate = empty($GGEStatusArray['GGE1stAttemptDate'])?"":  date_format(new DateTime($GGEStatusArray['GGE1stAttemptDate']), "m/d/Y");
            $GGE2ndAttemptDate = empty($GGEStatusArray['GGE2ndAttemptDate'])?"":  date_format(new DateTime($GGEStatusArray['GGE2ndAttemptDate']), "m/d/Y");;
//            $toEmail = "fieldsolutions@velocity.org";
//            $fromName = "FieldSolutions Support";
//            $fromEmail = "support@fieldsolutions.com";
//           
//            $subject = "GGE Tech Evaluation - FS-Tech ID# $this->_techID";
//            $fname = $techInfo["Firstname"];
//            $lname = $techInfo["Lastname"];
//            $City = $techInfo["City"];
//            $ST = $techInfo["State"];
//            $zipcode = $techInfo["Zipcode"];
//            $Answers = "";
//            $Exptime = $params["lefttime"] == "1"?"Yes":"No";
//            for($i = 1;$i <= 20; $i++)
//            {
//                $as = $_POST["a".$i."_answer"];
//                $Answers .="$i - $as\n";
//}
//            $body = "Hello,
//                
//                    The technician listed below has completed the GGE Tech Evaluation.
//                    $fname $lname (FS-Tech ID# $this->_techID)
//                    $City, $ST $zipcode
//                    Date of First Attempt: $GGE1stAttemptDate
//                    Date of Second Attempt: $GGE2ndAttemptDate
//                    Time Limit Expired: $Exptime
//                    
//                    Answers:
//                    $Answers
//                    
//                    Thank you,
//                    Your FieldSolutions Team
//                    ";
//            $mail = new Core_Mail();
//            $mail->setBodyText($body);
//            $mail->setFromName($fromName);
//            $mail->setFromEmail($fromEmail);
//            $mail->setToEmail($toEmail);
//            $mail->setSubject($subject);
//            $mail->send();
            $this->_helper->json($fileResult);
        }    
        else
        {
            $this->view->allowed = $Core_Api_TechClass->getTimesForGGETest($this->_techID);
            $this->view->status = $Core_Api_TechClass->getStatusArrayForGGECert($this->_techID);
        }    
    }
    function techForceContractorAgreementAction()
    {
        //13624
        $techID = $this->_techID;
        if(empty($techID)){
            $techID = $_POST['TechID'];
        }
        if(empty($techID)){
            $techID = $_GET['TechID'];
        }
        
        $params = $this->getRequest()->getParams();
        $Core_Api_TechClass = new Core_Api_TechClass();
        if($params["p"] == "yes"){
            $checked = $params["checked"];
            $Core_Api_TechClass->saveTech_TECFICAgreement($techID,$checked);
        }else{
            $info = $Core_Api_TechClass->getTechExts($techID);
            $this->view->TECF_ICAgreement = $info["TECF_ICAgreement"];
}
    }
}
