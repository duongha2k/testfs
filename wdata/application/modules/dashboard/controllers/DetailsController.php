<?php

class Dashboard_DetailsController extends Core_Controller_Action
{
    private  $_infoContent = 'Easily create and manage Work Orders with multiple visits. Technicians are provided with a separate signoff sheet for each visit.';
    public function init()
    {
        parent::init();
       

        $request = $this->getRequest();

        // This is constroller can be used for AJAX only!
        if ( !$this->getRequest()->isXmlHttpRequest() ) {
            //$this->_redirect('/');
        }

        $this->_sortStr = $this->_assembleSortStr(array(
            's1' => $request->getParam('sort1', NULL), 'd1' => $request->getParam('dir1', NULL),
            's2' => $request->getParam('sort2', NULL), 'd2' => $request->getParam('dir2', NULL),
            's3' => $request->getParam('sort3', NULL), 'd3' => $request->getParam('dir3', NULL)
        ));

 
        $user = new Core_Api_User();
        $clientInfoLogin = $user->loadWithExt(
                array(
                    'login' => $this->_login,
                    'password' => $this->_password
                )
        );
        $apiClient = new Core_Api_Class();
        $isGPM = $apiClient->isGPM($clientInfoLogin->ClientID);
        $this->view->isGPM = $isGPM;        
        //---    
        $auth = new Zend_Session_Namespace('Auth_User');
        $this->view->isPM = $auth->isPM;
        $this->view->name = $auth->name;
        $this->view->company = $company = $request->getParam('company',NULL);

		$this->view->showDeduct = $company == 'FLS' || $company == 'HXC' || $company == 'HXWN' || $company == 'HXXO' || $company == 'HXST' || $company == 'BW' || $company == 'suss' || $company == 'CBD' || $company == 'RHOM' || $company == 'TECF' || $company == 'SIS' || $company == 'BLUE' || $company == 'CPI' || $company == 'FDSV' || $company == 'PCS' || $company == 'DATA' || $company == 'BBLC' || $company == 'EQCS' || $company == 'ENPT' || $company == 'KIMS' || $company == 'ITLC' || $company == 'CPSS' || $company == 'Z2T' || $company == 'ENDV' || $company == 'CO1P' || $company == 'PSC' || $company == 'APEX' || $company == 'DOLO' || $company == 'DTPOS' || $company == 'OUTT' || $company == 'ITDM' || $company == 'ZAPC' || $company == 'ACEC' || $company == 'SUIS' || $company == 'TERX' || $company == 'CDT' || $company == 'B2BC' || $company == 'NNS' || $company == 'LATE' || $company == 'OTS' || $company == 'VERD' || $company == 'GANT' || $company == 'NMT' || $company == 'VSN' || $company == 'SYSI' || $company == 'SABX' || $company == 'NSCS' || $company == 'TKK'  || $company == 'FLYE' || $company == 'METTC' || $company == 'TSWW' || $company == 'IKKAN' || $company == 'ITS' || $company == 'IPM' || $company == 'VALC' || $company == 'AIMW' || $company == 'NEAR' || $company == 'WIN' || $company == 'ITNS' || $company == 'AXAP' || $company == 'IPRO' || $company == 'CMIT' || $company == 'MORAN' || $company == 'STG' || $company == 'TEAM' || $company == 'LINK' || $company == 'EVNX' || $company == 'VTT' || $company == 'VDNI' || $company == 'ROI' || $company == 'EMIT' || $company == 'MML' || $company == 'DT' || $company == 'MTI' || $company == 'NASI' || $company == 'BWT' || $company == 'LMS' || $company == 'GMNS' || $company == 'ACG' || $company == 'IBA' || $company == 'UNITIV' || $company == 'BPI' || $company == 'NGIT';
        $this->view->infoContent = $this->_infoContent;
        $this->view->login = $this->_login;
        $this->view->password = $this->_password;

		$this->user = $user;
	}

    public function navigationAction()
    {
        $request    = $this->getRequest();
        $win        = $request->getParam('id', NULL);
        //$tab        = $request->getParam('tab', NULL);
        $search = new Core_Api_Class;
        $company = $this->view->company;
        //14011 Remove              
        $authData = array('login' => $this->_login.'|pmContext='.$company, 'password' => $this->_password);
        $client = new Core_Api_User;
        $client->checkAuthentication($authData);

        $this->view->client     = $client;
        if ($win) {
            $this->view->wo         = $search->getWorkOrder($this->_login.'|pmContext='.$company, $this->_password, $win);
        }
        $this->view->company    = $company;
        $this->view->win        = $win;
        $this->view->FLSManager = ($_SESSION['UserType'] == 'Install Desk' || $_SESSION['UserType']=='Manager');
    }
       //14011
    public function donavigationAction()
    {
	$request    = $this->getRequest();
        $win        = $request->getParam('id', NULL);      
        $search = new Core_Api_Class;
        $company = $request->getParam('company', NULL);    
        //13768
        $counters = array(WO_STATE_CREATED=>0, WO_STATE_PUBLISHED=>0, WO_STATE_ASSIGNED=>0, WO_STATE_WORK_DONE=>0, WO_STATE_INCOMPLETE=>0,WO_STATE_COMPLETED=>0,'total'=>0);
        $search->getWorkOrdersCountByStatus($this->_login."|pmContext=$company", $this->_password, $counters);
     
        $apiWosClass = new Core_Api_WosClass();
        $counters['today'] = $apiWosClass->countWinIDs_ForToday($company);               
        $success = 'error';		
        if($counters){
                $success = "success";
        }else{
                $success = "error";
        }

        $this->_helper->json(array('success'=>$success,'totalData' => $counters));
    }
    private function createOpenTabs($tab)
    {
        $this->view->opentabs = array();
        /**
         * ALL sections collapsed
         * see issue #12361
         */
        $tabs_by_status_options = array(
            'new'=>array(0,1,2,3,4),            /* create WO form */
            'full'=>array(0,1,2,3,4,5,6,7,8,10,11,13,14,15,16,17,18,20,21,22),      /* reserved */
			'techcomments' => array(5,8,10,11,13,14,15,16,17,18,20,21,22)
        );
        $tabs_by_status_options[WO_STATE_CREATED] =
        $tabs_by_status_options[WO_STATE_PUBLISHED] =
        $tabs_by_status_options[WO_STATE_ASSIGNED] =
        $tabs_by_status_options[WO_STATE_WORK_DONE] =
        $tabs_by_status_options[WO_STATE_INCOMPLETE] =
        $tabs_by_status_options[WO_STATE_APPROVED] =
        $tabs_by_status_options[WO_STATE_IN_ACCOUNTING] =
        $tabs_by_status_options[WO_STATE_COMPLETED] =
        $tabs_by_status_options[WO_STATE_DEACTIVATED] =
        $tabs_by_status_options['all']= array(8,10,11,13,14,15,16,17,18,20,21,22);

        if ( isset($tabs_by_status_options[$tab]) ) $this->view->opentabs = $tabs_by_status_options[$tab];
        else $this->view->opentabs = $tabs_by_status_options['full'];

    }

    public function managerCreateAction()
    {
    	// categories list
        $search = new Core_Api_Class();
        $common = new Core_Api_CommonClass;
        $categories = $search->getWOCategories($this->_login.'|pmContext='.$companyId, $this->_password);
        $this->view->categoriesSuccess = $categories->success;
        if ($categories->success)
            $this->view->categories = $categories->data;

        $this->view->success = true;

		// country list
		$countries = $common->getCountries();
        $this->view->countriesSuccess = $countries->success;
        if ($countries->success)
            $this->view->countries = $countries->data;

        // states list
        $states = $common->getStatesArray('US');
        $this->view->statesSuccess = $states->success;
        if ($states->success)
            $this->view->states = $states->data;
        $Core_Api_WosClass = new Core_Api_WosClass();
        $this->view->EstDurationArray = $Core_Api_WosClass->getEstDurationArray();
    }

    public function createAction()
    {
        $db = Core_Database::getInstance();

		$this->view->addScriptPath(DASHBOARD_MODULE_PATH.'views/scripts');
        //check for manager
        if ($_SESSION['UserType'] == 'Install Desk' || $_SESSION['UserType']=='Manager') {
            $this->_forward('manager-create');
            return false;
        }

        $request = $this->getRequest();
        $companyId = $this->view->company;

        $this->createOpenTabs('new');

        $search = new Core_Api_Class;
        $common = new Core_Api_CommonClass;

        $this->view->success = 1;
        $this->view->errors  = null;

        $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');

        // projects list
        $projects = $search->getAllProjects(
            $this->_login.'|pmContext='.$companyId,
            $this->_password, 
			NULL, 
			array('ProjectUserAllowedCreateWO' => true)
        );

        $authData = array('login' => $this->_login.'|pmContext='.$companyId, 'password' => $this->_password);
        $client = new Core_Api_User;
        $client->checkAuthentication($authData);
		
		$params = array();
		$sortBy = "UserName";
		$sortDir = "asc";
		
		$search = new Core_Api_Class;
		$this->view->allClients = $search->getClientsList(
			$this->_login.'|pmContext='.$companyId, $this->_password, $params, "{$sortBy} {$sortDir}", null
		);
		
		
		$this->view->project_id = $request->getParam('project_id', NULL);
        $this->view->client = $client;
        $this->view->projectsSuccess = $projects->success;
        if ($projects->success)
            $this->view->projects = $projects->data;

        // deactivation codes list
        $reasons = $search->getDeactivationCodes();
        if ($reasons->success)
            $this->view->deactivationCodes = $reasons->data;

        // penalty reasons list
        $reasons = $search->getPenaltyReasons();
        if ($reasons->success)
            $this->view->penaltyReasons = $reasons->data;

        // call types list
        $callTypes = $search->getCallTypes();
        $this->view->callTypesSuccess = $callTypes->success;
        if ($callTypes->success)
            $this->view->callTypes = $callTypes->data;

		// country list
		$countries = $common->getCountries();
        $this->view->countriesSuccess = $countries->success;
        if ($countries->success)
            $this->view->countries = $countries->data;

        // states list
        $states = $common->getStatesArray('US');
        $this->view->statesSuccess = $states->success;
        if ($states->success)
            $this->view->states = $states->data;

        // categories list
        $categories = $search->getWOCategories($this->_login.'|pmContext='.$companyId, $this->_password);
        $this->view->categoriesSuccess = $categories->success;
        if ($categories->success)
            $this->view->categories = $categories->data;
        $Core_Api_WosClass = new Core_Api_WosClass();
        $this->view->EstDurationArray = $Core_Api_WosClass->getEstDurationArray();
        $FSTagClass = new  Core_Api_FSTagClass();
        $this->view->FSTagList = $FSTagClass->getFSTagList_ForCompany(0,$companyId);

		$fs_experts = new Core_Api_FSExpertClass ();
		$this->view->fs_experts = $fs_experts->getFSExperts (1);

		/*
		$ind_certs_select = $db->select();
		$ind_certs_select->from(Core_Database::TABLE_CERTIFICATIONS, array ("name", "label"));
		$ind_certs_select->where("hide = 0");
		$ind_certs_results = Core_Database::fetchAll($ind_certs_select);
		$ind_certs = array ();
		foreach ($ind_certs_results as $record) {
			$ind_certs[$record["name"]] = $record["label"];
		}
		$this->view->ind_certs = $ind_certs;
		*/

        $WoTagClass = new Core_Api_WoTagClass(); 
        $this->view->EntityTypeList = $WoTagClass->getEntityTypeList();

        //$FSTagClass = new  Core_Api_FSTagClass();
        //$this->view->FSTagList = $FSTagClass->getFSTagList_ForCompany(0,$companyId);
    }

    public function managerIndexAction()
    {
        $request = $this->getRequest();
        $companyId = $this->view->company;

        $search = new Core_Api_Class;

        // wo data
        $wo = $search->getWorkOrder(
            $this->_login.'|pmContext='.$companyId,
            $this->_password,
            $request->getParam('win', null)
        );

        $this->view->wo      = $wo->data[0];
        $this->view->success = $wo->success;
        $this->view->errors  = $wo->errors;
        $this->view->opentabs = array(0, 1);
        
        $Core_Api_WosClass = new Core_Api_WosClass();
        $this->view->WOVisitList = $Core_Api_WosClass->GetWOVisitList($request->getParam('win'));
        $Core_Api_WosClass = new Core_Api_WosClass();
        $this->view->EstDurationArray = $Core_Api_WosClass->getEstDurationArray();
    }
    
    public function indexAction()
    {
        $db = Core_Database::getInstance();

		$this->view->addScriptPath(DASHBOARD_MODULE_PATH.'views/scripts');
        //check for manager
        if ($_SESSION['UserType'] == 'Install Desk' || $_SESSION['UserType']=='Manager') {
            $this->_forward('manager-index');
            return false;
        }

        $request = $this->getRequest();
        $companyId = $this->view->company;

        $search = new Core_Api_Class;
        $searchProj = new Core_Api_ProjectClass;
        $common = new Core_Api_CommonClass;
        $Core_Api_WosClass = new Core_Api_WosClass();//13881
        $winnum = $request->getParam('win', null);
        if(!empty($winnum))
        {
            $projectclass = new Core_Api_ProjectClass();
            $projectclass->updateServiceTypeWO_FromItsProject($winnum);
            //13881
            $Core_Api_WosClass->updateCalculatedTechHrs_WhenBlankInfo($winnum);            
            //end 13881
        }
        // wo data
        $wo = $search->getWorkOrder(
            $this->_login.'|pmContext='.$companyId,
            $this->_password,
            $request->getParam('win', null)
        );

        //--- 13329
        $certLabel = "";
        $Core_WorkOrderCertification = new Core_WorkOrderCertification($wo->data[0]->WIN_NUM,"");
        $WorkOrderCertification = $Core_WorkOrderCertification->getRowByWIN();
        if(!empty($WorkOrderCertification)){
            $wo->data[0]->CredentialCertificationId = $WorkOrderCertification[0]["certification_id"];
        }else{
            $wo->data[0]->CredentialCertificationId = "";
        }
        if(!empty($wo->data[0]->CredentialCertificationId))
        {
           $certId = $wo->data[0]->CredentialCertificationId;           
           $certLabel = $common->getCertLabel($certId);
        }
        $wo->data[0]->CredentialCertificationLabel = $certLabel;
        // 14078
        $win = $wo->data[0]->WIN_NUM;
        $WosClass = new Core_Api_WosClass;
        $projectinfo1 = $searchProj->getProject($wo->data[0]->Project_ID);
        if(!empty($projectinfo1)){
            $clientCSDID = $projectinfo1['FSClientServiceDirectorId']; 
            $entityTypeId = $projectinfo1['EntityTypeId']; 
            $serviceTypeId = $projectinfo1['ServiceTypeId']; 
        }

        if(empty($wo->data[0]->FSClientServiceDirectorId)){
            if(empty($clientCSDID)){
                $wo->data[0]->FSClientServiceDirectorId = 10;
            } else {
                $wo->data[0]->FSClientServiceDirectorId = $clientCSDID;                
            }
            $result = $WosClass->updateWorkOrder_CSDID($win,$wo->data[0]->FSClientServiceDirectorId);          
        }
      
        if(empty($wo->data[0]->EntityTypeId)){
            if(empty($entityTypeId)){
                $wo->data[0]->EntityTypeId = 23;
            } else {
                $wo->data[0]->EntityTypeId = $entityTypeId;                
            }
            $result = $WosClass->updateWorkOrder_EntityTypeId($win,$wo->data[0]->EntityTypeId);           
        }
   
        if(empty($wo->data[0]->ServiceTypeId)){
            if(empty($serviceTypeId)){
                $wo->data[0]->ServiceTypeId = 15;
            } else {
                $wo->data[0]->ServiceTypeId = $serviceTypeId;                
            }
            $result = $WosClass->updateWorkOrder_ServiceTypeId($win,$wo->data[0]->ServiceTypeId);         
        }       
        //14078            
        $this->view->wo      = $wo->data[0];
        $this->view->success = $wo->success;
        $this->view->errors  = $wo->errors;

        
        $tab = 'full' == $request->getParam('tab',null) || 'all' == $request->getParam('tab',null) || 'techcomments' == $request->getParam('tab',null)
                ? $request->getParam('tab',null)
                : $this->view->wo->Status;
        $this->createOpenTabs( $tab );
		
        if(!$wo->success || !$wo->data[0]) return;
        $wo->data[0]->PrimaryPhoneStatus = "1";
        $wo->data[0]->SecondaryPhoneStatus = "1";
        //14112       
        $this->view->ISOInformation = "";
        
        if (!empty($wo->data[0]->Tech_ID)) {
        	$Core_Api_TechClass = new Core_Api_TechClass;
        	$result = $Core_Api_TechClass->getExts($wo->data[0]->Tech_ID);
            if (!empty($result['TechID'])) {
                $wo->data[0]->PrimaryPhoneStatus = $result['PrimaryPhoneStatus'];
                $wo->data[0]->SecondaryPhoneStatus = $result['SecondaryPhoneStatus'];

            }
            $resultISO = $Core_Api_TechClass->getTechInfo_ForAPIUsing($wo->data[0]->Tech_ID);      
            if (!empty($resultISO['ISO_Affiliation_ID'])&& $resultISO['ISO_Affiliation_ID'] > 0) {             
                $ISO_ID = $resultISO['ISO_Affiliation_ID'];
                $ISO_Name = $resultISO['ISO_Affiliation'];  
                $this->view->ISOInformation = $ISO_Name."(ISO ID# ".$ISO_ID.")";            
            }  
        }//14112    
        
        
        //----
        $wo = $wo->data[0];
        $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');
        // projects list
        $cacheKeyPrj = Core_Cache_Manager::makeKey('projects', $companyId, '', $this->_login);
        if ( FALSE == ($projects = $cache->load($cacheKeyPrj)) ) {
			$projects = $search->getAllProjects(
				$this->_login.'|pmContext='.$companyId,
				$this->_password, 
				NULL, 
				array('ProjectUserAllowedCreateWO' => true, 'ProjectUserAllowedCreateWOAllowList' => array($wo->Project_ID))
			);
            if ( $projects->success ) {
                $cache->save($projects, $cacheKeyPrj, array('prj'), LIFETIME_15MIN);
            }
        }

        /**
         * Load Tech Prefer or Deny
         * @author Artem Sukharev
         */
		$this->view->techStatus = null;
		if ( $this->view->wo->Tech_ID ) {
			$status = $search->techGetPreferStatus($this->_login.'|pmContext='.$companyId, $this->_password, $this->view->wo->Tech_ID);
            if( $status->success && !empty($status->data) ) {
            	$this->view->techStatus = $status->data;
            }
        }

        $authData = array('login' => $this->_login.'|pmContext='.$companyId, 'password' => $this->_password);
		$client = new Core_Api_User;
        $client->checkAuthentication($authData);

        $params = array();
		$sortBy = "UserName";
		$sortDir = "asc";
		$count = 0;
		
		$search = new Core_Api_Class;
		$this->view->allClients = $search->getClientsList(
			$this->_login.'|pmContext='.$companyId, $this->_password, $params, "{$sortBy} {$sortDir}", 0, 0, $count
		);
		

		$this->view->client     = $client;
        /*if ( !empty($projects->data) ) {
            usort($projects->data, 'Core_Controller_Action::_cmpProjects');
        }*/
        $this->view->projectsSuccess = $projects->success;
        if ($projects->success)
            $this->view->projects = $projects->data;

        // deactivation codes list
        $reasons = $search->getDeactivationCodes();
        if ($reasons->success)
            $this->view->deactivationCodes = $reasons->data;

        // penalty reasons list
        $reasons = $search->getPenaltyReasons();
        if ($reasons->success)
            $this->view->penaltyReasons = $reasons->data;

        // call types list
        $callTypes = $search->getCallTypes();
        $this->view->callTypesSuccess = $callTypes->success;
        if ($callTypes->success)
            $this->view->callTypes = $callTypes->data;

		// country list
		$countries = $common->getCountries();
        $this->view->countriesSuccess = $countries->success;
        if ($countries->success)
            $this->view->countries = $countries->data;

        // states list
        $states = $common->getStatesArray($this->view->wo->Country);
        $this->view->statesSuccess = $states->success;
        if ($states->success)
            $this->view->states = $states->data;
		
		// categories list
        $categories = $search->getWOCategories($this->_login.'|pmContext='.$companyId, $this->_password);
        
        $this->view->categoriesSuccess = $categories->success;
        if ($categories->success)
            $this->view->categories = $categories->data;

        $projectInfo = $searchProj->getProjects(
                $this->_login.'|pmContext='.$companyId,
                $this->_password,
                array($wo->Project_ID),
                '',
		$wo->WorkOrderOwner);
        $this->view->project_info = $projectInfo->data[0];
        //print_r($projectInfo->data[0]);//test 
        
		$files = new Core_Files();
        $select = $files->select();
        $select->where('WIN_NUM = ?', $request->getParam('win'));
        $filesList = $files->fetchAll($select);

        $this->view->files = $filesList;
		$this->view->wo_signoff = false;

		if(!empty ($filesList)) {
			foreach ($filesList as $k => $f) {
				if ($f->type == Core_Files::SIGNOFF_FILE) {
					$this->view->wo_signoff = $f;
				}
			}
		}

		$fs_experts = new Core_Api_FSExpertClass ();
		$this->view->fs_experts = $fs_experts->getFSExperts (1);

		$siteCount = Core_Site::getSiteCount($companyId, $wo->Project_ID);
		$this->view->siteCount = empty($siteCount[$wo->Project_ID]) ? 0 : $siteCount[$wo->Project_ID];

	if (!empty($wo->Project_ID)) {
	        $files = new Core_ProjectsFiles();
	        $select = $files->select();
	        $select->where('Project_ID = ?', $wo->Project_ID);
	        $filesList = $files->fetchAll($select);
	       //pull info for External File in project table
	        $extSelect = $db->select();
	        $extSelect->from(Core_Database::TABLE_PROJECTS, array("External_Files", "CustomSignOff"))
	                 ->where("Project_ID = ?",$wo->Project_ID);
	        $extFiles = Core_Database::fetchAll($extSelect);

			$wcs = new Core_WorkOrderCertifications();
			$certswo = $wcs->getWorkOrderCertification($request->getParam('win'));
			
			$this->view->wo->ClientCredential = empty($certswo) ? array() : array_values($certswo[$request->getParam('win')]);
	}
	else {
		$filesList = NULL;
		$extFiles = NULL;
	}

		$this->view->prjFiles = $filesList;
        $this->view->prjExtFile = $extFiles;
        $Core_Api_ProjectClass = new Core_Api_ProjectClass();
        $Wosapi = new  Core_Api_WosClass();
        $ExpenseReporting = $Core_Api_ProjectClass->getProjectTurnedOnBCats_insertFromClientIfNotExists($wo->Project_ID);
        //13940
        $Wosapi->insertBCatsFromProjectIfnotExist($wo->WIN_NUM,$wo->Project_ID,$ExpenseReporting);
        $WosTurnedOnBCatsinfo = $Wosapi->getWorkOrderTurnedOnBCats($wo->WIN_NUM);        
        $this->view->WosTurnedOnBCatsinfo =  $WosTurnedOnBCatsinfo;
        //end 13940
        $this->view->expenseReporting = $ExpenseReporting;
        $this->view->EnableColspanTechPay = $Core_Api_ProjectClass->enableExpenseReporting($wo->Project_ID);
        if($this->view->wo->TechPaid || $this->view->wo->Invoiced)
            $this->view->isdisable = true;
        else
            $this->view->isdisable = false;
        $Core_Api_WosClass = new Core_Api_WosClass();
		$this->view->EstDurationArray = $Core_Api_WosClass->getEstDurationArray();
        $this->view->WOVisitList = $Core_Api_WosClass->GetWOVisitList($request->getParam('win'));
        $this->view->AllWOVisitList = $Core_Api_WosClass->GetAllWOVisitList($request->getParam('win'));
		$this->view->NumQtyVisits = $Core_Api_WosClass->getNumOfVisits($request->getParam('win'));
        //        if(empty($this->view->wo->Qty_Visits)){
//        }
                $WoTagClass = new Core_Api_WoTagClass(); 
        $this->view->EntityTypeList = $WoTagClass->getEntityTypeList();    

		if($wo->Qty_Visits === "" || $wo->Qty_Visits === NULL)
			$this->view->wo->Qty_Visits = $this->view->NumQtyVisits;
        
        $WoTagClass = new Core_Api_WoTagClass();
        $this->view->CompletionStatusTagList = $WoTagClass->getCompletionStatusTagList();
        $this->view->CompletionStatusForWin = $WoTagClass->getCompletionStatusForWin($request->getParam('win'));   
		
    	$mySettingsClass = new Core_FutureWorkOrderInfo;
        $projectinfo = $Core_Api_ProjectClass->getProjectById($wo->Project_ID);
        $mySettingsRow = $mySettingsClass->find($this->_login)->toArray();
		$mySettingsInfo = $mySettingsRow[0];
		$clientInfo = $search->getClientByUsername($wo->WorkOrderOwner);
		$this->view->woOwnerEmail = $clientInfo['Email1'];
		
		if($projectinfo['AllowCustomCommSettings'] == "1" && $mySettingsInfo['TechQAMessageEmailTo'] !== ""){
			$this->view->qaOverrideEmail = $mySettingsInfo['TechQAMessageEmailTo'];
		}
        
         $FSTagClass = new  Core_Api_FSTagClass();
        $this->view->FSTagList = $FSTagClass->getFSTagList_ForCompany(0,$companyId);

		/*
		$ind_certs_select = $db->select();
		$ind_certs_select->from(Core_Database::TABLE_CERTIFICATIONS, array ("name", "label"));
		$ind_certs_select->where("hide = 0");
		$ind_certs_results = Core_Database::fetchAll($ind_certs_select);
		$ind_certs = array ();
		foreach ($ind_certs_results as $record) {
			$ind_certs[$record["name"]] = $record["label"];
		}
		$this->view->ind_certs = $ind_certs;
		*/

        $WosClass = new Core_Api_WosClass();
        $showWInTags = $WosClass->checkforPopupAddWinTags($request->getParam('win'));
        $this->view->showWinTags = $showWInTags  && $this->view->isGPM;
         //13763       
        $future = new Core_FutureWorkOrderInfo;
	$row = $future->find($wo->WorkOrderOwner)->toArray();        
	$this->view->futureInfo = $row[0];		
        //13871     
        $wos = $search->getWorkOrdersWithWinNum($winnum,"","");
        $wos = (object)$wos[0];
        $this->view->wos = $wos;
        if(!empty($wos->Tech_ID)){
            $this->view->TechInfo = $WosClass->getTechInfo($wos->Tech_ID);
        }
        $this->view->OnTimePerformceList_forWO = $WoTagClass->GetOnTimePerformceList_forWO($winnum);        
        $this->view->StartResultList = $WoTagClass->getStartResultList();
        $this->view->getEndResultList = $WoTagClass->getEndResultList();
        $this->view->DurationResultList = $WoTagClass->getDurationResultList();
                            
        if(empty($this->view->CompletionStatusForWin) || count($this->view->CompletionStatusForWin)==0)
        {
           $notesWinTag = "";
        }   
        else
        {         
            foreach($this->view->CompletionStatusForWin as $rec)
            {                
                if($rec['notes']!=""){
                    $tagName = str_replace(array("<sup>", "</sup>"), "", $rec['wo_tag_item_name']);
                    $notesWinTag .= (empty($notesWinTag))?"\r\n\r\n\r\n":"" .$tagName.": ".$rec['notes']."\r\n";                        
                }
            }
        }
        $this->view->NotesWinTag=$notesWinTag;

		$required_certs = $Core_Api_ProjectClass->loadP2TCertRequirements ($request->getParam('win'), false);
		$this->view->required_certs = $required_certs;
	}
    function expenseReportingTechAction()
    {
        $request = $this->getRequest(); 
        $Project_ID = $request->getParam('Project_ID',0);
        $Core_Api_ProjectClass = new Core_Api_ProjectClass();
        $Core_Api_ProjectClass  = new Core_Api_ProjectClass();
        $ExpenseReporting = $Core_Api_ProjectClass->getProjectTurnedOnBCats_insertFromClientIfNotExists($Project_ID);
        $this->view->expenseReporting = $ExpenseReporting;
        $this->view->EnableColspanTechPay = $Core_Api_ProjectClass->enableExpenseReporting($Project_ID);
    }

    function expenseReportingTotalTechAction()
    {
        $request = $this->getRequest(); 
        $Project_ID = $request->getParam('Project_ID',0);
        $Core_Api_ProjectClass = new Core_Api_ProjectClass();
        $Core_Api_ProjectClass  = new Core_Api_ProjectClass();
        $ExpenseReporting = $Core_Api_ProjectClass->getProjectTurnedOnBCats_insertFromClientIfNotExists($Project_ID);
        
        
        $Wosapi = new  Core_Api_WosClass();
        $WosTurnedOnBCatsinfo =  $Wosapi->getWorkOrderTurnedOnBCats_insertFromProjectIfNotExists();
        $this->view->WosTurnedOnBCatsinfo = $Core_Api_ProjectClass->enableExpenseReporting($Project_ID);
    }

    public function doCreateAction()
    {

        $companyId = $this->view->company;

        $this->_helper->viewRenderer->setNoRender();

        $params = $this->getRequest()->getParams();
	
        $errors = array();
        if ($_SESSION['UserType'] == 'Install Desk' || $_SESSION['UserType']=='Manager') {
        	$params['Project_ID'] = 3149;
        	$params['Type_ID'] = 1;

        	$errors = $this->validateWOCreationByFLSManager($params);
        }

        if (!count($errors)) {
			$urlStripFields = array("Headline", "Description", "Requirements", "SpecialInstructions");
			foreach ($urlStripFields as $field) {
				if (empty($params[$field])) continue;
				$params[$field] = preg_replace("/<\/?a[^>]*>/", "", $params[$field]);
				$params[$field] = preg_replace("/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})/","", $params[$field]);
				$params[$field] = preg_replace("/\b(?:(?:https?|ftp):\/\/)?(?:www\d{0,3}[.])?(?:[a-z0-9\-]|.(?!.))+[.](?:com|co|mx|ca|edu|info|biz|net|org)[\/]?(?:[^\s]+)?/","", $params[$field]);
			}

        	$search = new Core_Api_Class;

        	$return = array('success'=>0);

        	$wo = new API_WorkOrder(NULL);

        	$className = get_class($wo);
        	foreach ($params as $key => $value) {
                if($key=='AllowDeVryInternstoObserveWork' || $key=='AllowDeVryInternstoPerformField' || $key=='ACSNotifiPOC' || $key=='ACSNotifiPOC_Phone' || $key=='ACSNotifiPOC_Email' || 
					$key=='SiteContactBackedOutChecked' || $key=='NotifiedTime' ) continue;
        		if(property_exists($className, $key)) $wo->$key = $value;
        	}

        	//Special handler for checkboxes.
        	$checkboxes = array(
				'ShowTechs',
				'Update_Requested',
				'Lead',
				'Assist',
				/*'SignOffSheet_Required',*/
				'WorkOrderReviewed',
				'TechCheckedIn_24hrs',
				'CheckedIn',
				'StoreNotified',
				'Paperwork_Received',
				'Incomplete_Paperwork',
				'TechMarkedComplete',
				'CallClosed',
				'Approved',
				'Deactivated',
				'PartsProcessed',
				'AbortFee',
				'ShortNotice',
				'PcntDeduct',
				'PartManagerUpdate_Required',
				'SignOff_Disabled',
				'PushWM'
			);

        	foreach ($checkboxes as $c) {
        		if (!property_exists($className, $c))
        		continue;

        		$wo->$c = isset($params[$c]);
        	}

        	$wo->SignOff_Disabled = !$wo->SignOff_Disabled;

        	unset($wo->CredentialCertificationId);
			
        	//Special handler for Times
        	$times = array('StartTime','EndTime');
        	foreach ($times as $key) {
        		if (!empty($params[$key]) && property_exists($className,$key)) {
        			$wo->$key = strtoupper($params[$key]);
        		}
        	}

        	//Special handlers for virtual fields
        	if (isset($params['MarkIncompleteV'])) {
        		$wo->TechMarkedComplete = false;
        	}
                
                if (isset($params['Route'])) {
        		$wo->Route = $params['Route'];
        	}

        	//--- 13329: CredentialCertificationId
//            if (isset($params['CredentialCertificationId'])) {
//                $wo->CredentialCertificationId = $params['CredentialCertificationId'];
//            }            
            //--- 13329 end
			$errors = Core_Api_Error::getInstance();
        	$errorServiceSchedule = false;
                $Core_Api_WosClass = new Core_Api_WosClass();
                $ServiceSchedule = $params['ServiceSchedule'];
                $jsonServiceSchedule = array();
                if(!empty($ServiceSchedule)){
                    $jsonServiceSchedule = json_decode("[".base64_decode($ServiceSchedule)."]");
                    if(!empty($jsonServiceSchedule)){
                        $ik = 0;
                        foreach($jsonServiceSchedule as $item){
                            $ik++;
                            if(strtolower($item->Remove) == "true"){
                                continue;
                            }else{
                                //valid 13969
                            	$errors->resetErrors ();
                            	$Core_Api_WosClass->validWOVisit(
                                        $item->WoVisitID,
                                        "",
                                        $item->StartTypeID,
                                        $item->StartDate,
                                        $item->StartTime,
                                        $item->EndDate,
                                        $item->EndTime,
                                        $item->EstimatedDuration,
                                        $item->TechArrivalInstructions,
                                        $params['Project_ID']);

                                        if($errors->hasErrors()){
                                                $errorServiceSchedule = true;                                               
                                                $errorArr = $errors->getErrorArray ();
                                        if(!empty($errorArr))
                                        {
                                            foreach ($errorArr as $error) {
                                                    $return['errors'][] = $error["description"];
                                            }
                                        }
                                        }
                            }
                        }
                    }
                }
            
                
                if(!$errorServiceSchedule){
            $apiResponse = $search->createWorkOrder($this->_login.'|pmContext='.$companyId, $this->_password, isset($params['ShowTechs']), $wo,true);
        	if (empty($apiResponse->errors)) {
                            // save tag
                            $Core_Api_WoTagClass = new Core_Api_WoTagClass();
                            $tagArray = array();
                            $tagArray[0] = array('TagCodeId'=>'ServiceTypeId', 'TagCodeValue'=>$params['ServiceTypeId']);
                            $tagArray[1] = array('TagCodeId'=>'EntityTypeId', 'TagCodeValue'=>$params['EntityTypeId']);
                            //13622
                            $projectID = $params['Project_ID'];
                            $ProjectClass = new Core_Api_ProjectClass();
                            $projectInfo = $ProjectClass->getProject($projectID);                                          $prjCSD_ID =  $projectInfo['FSClientServiceDirectorId'];
                            
                            $tagArray[2] = array('TagCodeId'=>'FSClientServiceDirectorId', 'TagCodeValue'=>$prjCSD_ID);
                            //end 13622
                            $Core_Api_WoTagClass->saveWOTags($apiResponse->data[0]->WIN_NUM,$tagArray);
                            // update Service Schedule
                            if(!empty($jsonServiceSchedule)){
                                $ik = 0;
                                foreach($jsonServiceSchedule as $item){
                                    $ik++;
                                    if(strtolower($item->Remove) == "true"){
                                        continue;
                                    }else{
                                        //valid
                                        //$StartDateSS = !empty($item->StartDate)?date_format(new DateTime($item->StartDate), "Y-m-d"):"";
                                        //$EndDateSS = !empty($item->EndDate)?date_format(new DateTime($item->EndDate), "Y-m-d"):"";
                            			$errors->resetErrors ();
                                    	if($ik == 1){
                                            $Core_Api_WosClass->saveBaseWOVisit(
                                                    $apiResponse->data[0]->WIN_NUM,
                                                    $item->StartTypeID,
                                                    $item->StartDate,
                                                    $item->StartTime,
                                                    $item->EndDate,
                                                    $item->EndTime,
                                                    $item->EstimatedDuration,
                                                    $item->TechArrivalInstructions);
                                        }else{
                                            $Core_Api_WosClass->saveWOVisit(
                                                    $item->WoVisitID,
                                                    $apiResponse->data[0]->WIN_NUM,
                                                    $item->StartTypeID,
                                                    $item->StartDate,
                                                    $item->StartTime,
                                                    $item->EndDate,
                                                    $item->EndTime,
                                                    $item->EstimatedDuration,
                                                    $item->TechArrivalInstructions);
                                        }
                                    }
                                }
                            }
        		//update tech status
        		$status = 0;
        		if (!empty($params['PreferTechV'])) {
        			$status = 1;
        		}elseif(!empty($params['DenyTechV'])) {
        			$status = 2;
        		}
                            $Core_Api_WosClass->synNumVisits($apiResponse->data[0]->WIN_NUM);
        		$return['success'] = 1;
        		$return['win'] = $apiResponse->data[0]->WIN_NUM;
                
                $winNum = $apiResponse->data[0]->WIN_NUM;
                // update CredentialCertificationId
                if (isset($params['CredentialCertification_id_1']) && $params['CredentialCertification_id_1'] > 0){
                    $Core_WorkOrderCertification = new Core_WorkOrderCertification($winNum,$params['CredentialCertification_id_1']);
                    $Core_WorkOrderCertification->save();
                }

				//Save credential requirements for FS-RUSH
				$ProjectClass = new Core_Api_ProjectClass();
				$process_certs = function ($field, $params) {
					$result = array ();
					$index = 1;

					do {
						$id = null;
						if (isset ($params[$field . "_id_" . $index])) {
							$id = $params[$field . "_id_" . $index];
							$level = $params[$field . "_level_" . $index];
							$result[$id] = $level;
							$index++;
						}
					} while (!empty ($id));

					return $result;
				};
				$error = $ProjectClass->saveP2TCertRequirements ($return['win'], array (
					"client" => $process_certs ("CredentialCertification", $params),
					"public" => $process_certs ("PublicCertification", $params),
					"expert" => $process_certs ("FSExpert", $params),
					"industry" => $process_certs ("IndustryCertification", $params)
				), false, false);	//Not an update, not a project, writes to work order instead

                //--- Update Work order additional fields //360                
                $search->saveWorkOrderAuditData($winNum,
                    isset($params['AuditNeeded']),
                    $this->_login,
                    date("Y-m-d H:i:s"),
                    isset($params['AuditComplete']),
                    $this->_login,
                    date("Y-m-d H:i:s")          
                );
            //--- 13713, 13691, 13732
            $tag = array('AllowDeVryInternstoObserveWork','AllowDeVryInternstoPerformField','ACSNotifiPOC','ACSNotifiPOC_Phone','ACSNotifiPOC_Email',
						'SiteContactBackedOutChecked','NotifiedTime');
            $projectID = $params['Project_ID'];
            //$ProjectClass = new Core_Api_ProjectClass();
            $projectInfo = $ProjectClass->getProject($projectID);
            $prjAllowDeVryInternstoObserveWork = $projectInfo['AllowDeVryInternstoObserveWork'];
            $prjAllowDeVryInternstoPerformField = $projectInfo['AllowDeVryInternstoPerformField'];
            
            $tagUpdateFields = array();            
            foreach($tag as $key)
            {
                if($key=='AllowDeVryInternstoObserveWork')
                {
                    $tagUpdateFields[$key] = isset($params[$key]);
                }
                else if($key=='AllowDeVryInternstoPerformField')
                {
                    $tagUpdateFields[$key] = isset($params[$key]);                    
                }
                else if($key=='SiteContactBackedOutChecked')
                {
                    $tagUpdateFields[$key] = isset($params[$key]);                    
                }                
                else
                {
                    if(isset($params[$key]) && !empty($params[$key]))
                    {
                        $tagUpdateFields[$key] = $params[$key];                    
                    }                    
                }
            }

            if (count($tagUpdateFields) > 0) {
                $search->saveWorkOrderTagsData($winNum,$wo,$tagUpdateFields,$this->_login);
            }
                
                
	            if (!empty ($params["CustomSignOff_Filename"]) && $params["CustomSignOff_Remove"] != "1") {
	                $files = new Core_Files();
			        $lastId = $files->insert(array(
			            'WIN_NUM' => $winNum,
			            'path'    => $params["CustomSignOff_Filename"],
			            'type'    => Core_Files::SIGNOFF_FILE
			        ));
	            }
			        
        	} else {
        		$return['success'] = 0;
        		$return['errors'] = array();
        		foreach ($apiResponse->errors as $e) {
        			$return['errors'][]=$e->message;
        		}
        	}
        } else {
        	$return['success'] = 0;
              }
        } else {
        	$return['success'] = 0;
        	$return['errors'] = $errors;
        }
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);



        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                 ->setBody($return);
    }

    private function validateWOCreationByFLSManager($params)
    {
    	$errors = array();

    	$requiredFields = array('WO_ID' => 'Client Work Order ID#',
    	'SiteName' => 'Site Name',
    	'Address' => 'Site Address',
    	'City' => 'City',
        'Country'=>'Country',
    	'State' => 'State',
		'Zipcode' => 'Zip or Postal Code',
    	'StartDate' => 'Start Date',
    	'StartTime' => 'Start Time',
    	//'EndDate' => 'End Date',
    	//'EndTime' => 'EndTime',            
    	'EstimatedDuration' => 'Estimated Duration',
    	'Description' => 'Work Description'
    	);
        if(isset($params["Country"]) && isset($params["Zipcode"]))
        {
            $Country = Core_Country::getCountryInfoByCode($params["Country"]);
            if(!empty($Country))
            {
                if($Country["ZipRequired"] != "1" && trim($params["Zipcode"]) == "")
                {
                    unset($requiredFields["Zipcode"]);
                } 
            }
        }
    	foreach ($requiredFields as $field => $name) {
    		if (!isset($params[$field]) || !trim($params[$field])) {
    			$errors[] = $name . ' empty but required';
    		}
    	}

    	return $errors;
    }

    public function projectDocumentsAction()
    {
    	$params = $this->getRequest();
    	$win = $params->getParam("win_num");
    	$id = $params->getParam("id");
    	$signoff = false;
        $files = new Core_ProjectsFiles();
        $select = $files->select();
        $select->where('Project_ID = ?', $id);
        $filesList = $files->fetchAll($select);

		//pull info for External File in project table
		$db = Core_Database::getInstance();
		$extSelect = $db->select();
		$extSelect->from(Core_Database::TABLE_PROJECTS, array("External_Files", "CustomSignOff"))
			->where("Project_ID = ?", $id);
		$extFiles = Core_Database::fetchAll($extSelect);

		$this->view->wo_signoff = false;

		if ($win != false) {
			$soSelect = $db->select();
			$soSelect->from (Core_Database::TABLE_FILES)
				->where ("WIN_NUM = ?", $win)
				->where ("type = ?", Core_Files::SIGNOFF_FILE);
			$signoff = Core_Database::fetchAll($soSelect);

			if (!empty ($signoff)) $signoff = $signoff[0];
			//if(!empty ($signoff)) {
			//	foreach ($signoff as $k => $f) {
			//		if ($f->type == Core_Files::SIGNOFF_FILE) {
			//			$this->view->wo_signoff = $f;
			//		}
			//	}
			//}
		}  


		$this->view->prjFiles = $filesList;
        $this->view->prjExtFile = $extFiles;
        $this->view->wo_signoff = $signoff;
    }

	public function managerUpdateAction()
	{
        $companyId = $this->view->company;

        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        $params = $this->getRequest()->getParams();

        $search = new Core_Api_Class;

        $return = array('success'=>0);

        $wo = $search->getWorkOrder(
            $this->_login.'|pmContext='.$companyId,
            $this->_password,
            $params['WIN_NUM']
        );

        if ($wo->success && !empty($wo->data[0]->WIN_NUM)) {
            $wo = $wo->data[0];
            $className = get_class($wo);

            //Special handler for checkboxes.
            $checkboxes = array('Update_Requested','SignOffSheet_Required','Paperwork_Received','Incomplete_Paperwork');
            foreach ($checkboxes as $c) {
                $wo->$c = isset($params[$c]);
            }

            $textFields = array('Update_Reason');
            foreach ($textFields as $fName) {
            	if (isset($params[$fName])) $wo->$fName = $params[$fName];
            }

            $apiResponse = $search->updateWorkOrder($this->_login.'|pmContext='.$companyId, $this->_password, $wo->WIN_NUM, $wo,true);

            if ( empty($apiResponse->errors) ) {
                $return['success'] = 1;
            } else {
                $return['success'] = 0;
                $return['errors'] = array();
                foreach ($apiResponse->errors as $e) {
                    $return['errors'][]=$e->message;
                }
            }

        } else {
            $return['success'] = 0;
            $return['errors'] = array('There is no such Work Order');
            foreach ($apiResponse->errors as $e) {
                $return['errors'][]=$e->message;
            }
        }
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        //$return = array('success'=>0,'errors'=>array('firsterror','seconderror'));
        $return = json_encode($return);

        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                 ->setBody($return);
	}

    public function updateAction()
    {
        $request = $this->getRequest();
        $request->setParam('Tools', ''); //Tools textarea was removed by CR #12563
    	//check for manager
        if ($_SESSION['UserType'] == 'Install Desk' || $_SESSION['UserType']=='Manager') {
            $this->_forward('manager-update');
            return false;
        }
        
        $companyId = $this->view->company;

        $this->_helper->viewRenderer->setNoRender();

        $params = $this->getRequest()->getParams();

        $search = new Core_Api_Class;
		$ProjectClass = new Core_Api_ProjectClass();

        $return = array('success'=>0);

        $wo = $search->getWorkOrder(
            $this->_login.'|pmContext='.$companyId,
            $this->_password,
            $params['WIN_NUM']
        );

        if ($wo->success && !empty($wo->data[0]->WIN_NUM)) {
            $wo = $wo->data[0];
            $className = get_class($wo);

            $currentProject = $wo->Project_ID;
            $currentTech = $wo->Tech_ID;

			$urlStripFields = array("Headline", "Description", "Requirements", "SpecialInstructions");
			foreach ($urlStripFields as $field) {
				if (empty($params[$field])) continue;
				$params[$field] = preg_replace("/<\/?a[^>]*>/", "", $params[$field]);
				$params[$field] = preg_replace("/[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})/","", $params[$field]);
				$params[$field] = preg_replace("/\b(?:(?:https?|ftp):\/\/)?(?:www\d{0,3}[.])?(?:[a-z0-9\-]|.(?!.))+[.](?:com|co|mx|ca|edu|info|biz|net|org)[\/]?(?:[^\s]+)?/","", $params[$field]);
			}

            //--- 13555 CheckedTime
            if(isset($params['Date_Checked_in']) && !empty($params['CheckedTime']) )
            {
               $params['Date_Checked_in'] = $params['Date_Checked_in'].' '.$params['CheckedTime']; 
            }
            // 13871
            if(isset($params['ClientComments']) && !empty($params['ClientComments']))
            {     
                $newstring = preg_replace("/[\n\r]/","+++",$params['ClientComments']);  
                $commentsNotes = explode("+++++++++", $newstring);
                $params['ClientComments'] = $commentsNotes[0];                   
            }
            //---
            foreach ($params as $key => $value) {
                if($key=='EntityTypeId' || $key=='ServiceTypeId' || $key=='AllowDeVryInternstoObserveWork' || $key=='AllowDeVryInternstoPerformField' || $key=='ACSNotifiPOC' || $key=='ACSNotifiPOC_Phone' || $key=='ACSNotifiPOC_Email' || 
					$key=='SiteContactBackedOutChecked' || $key=='NotifiedTime') continue;
                if(property_exists($className, $key)) $wo->$key = $value;
            }

            if (!empty($params["Project_ID"]) && $currentProject != $params["Project_ID"])
                unset($wo->Project_Name);

            //Special handler for checkboxes.
            $checkboxes = array(
				'ShowTechs',
				'Update_Requested',
				'Lead',
				'Assist',
				/*'SignOffSheet_Required',*/
				'WorkOrderReviewed',
				'TechCheckedIn_24hrs',
				'CheckedIn',
				'StoreNotified',
				'Paperwork_Received',
				'Incomplete_Paperwork',
				'TechMarkedComplete',
				'CallClosed',
				'Approved',
				'Deactivated',
				'PartsProcessed',
				'AbortFee',
				'ShortNotice',
				'PcntDeduct',
				'PartManagerUpdate_Required',
				'SignOff_Disabled',
				'isProjectAutoAssign',
				'PushWM'
			);

            foreach ($checkboxes as $c) {
                if (!property_exists($className, $c))
                    continue;

                $wo->$c = isset($params[$c]);
            }

			$is_reactivation = $wo->isProjectAutoAssign && $params["isReactivated"] != "0";

        	$wo->SignOff_Disabled = !$wo->SignOff_Disabled;

            $removefiles = $request->getParam('removefiles', null);

            //Special Handler for FLS Back Out/No Show Tech ID's
            if($companyId == "FLS" && ( !empty($params['BackOut_Tech']) || !empty($params['NoShow_Tech']) )){
            	//Lookup Tech_ID from techs table based on FLS ID entered into the BackOut or No Show Field
            	if(!empty($params['BackOut_Tech'])){
            		$tech = new API_Tech();
					//$tech->lookupID($params['BackOut_Tech']);
					if(!$tech->lookupID($params['BackOut_Tech'])){
						$customErrors[]= 'Tech Not Found For The Back Out Tech ID Entered';
					}else{
						$wo->BackOut_Tech = $tech->TechID;
					}
            	}
            	
            	if(!empty($params['NoShow_Tech'])){
            		$tech = new API_Tech();
					//$tech->lookupID($params['NoShow_Tech']);
					if(!$tech->lookupID($params['NoShow_Tech'])){
						$customErrors[]= 'Tech Not Found For The No Show Tech ID Entered';
					}else{
						$wo->NoShow_Tech = $tech->TechID;
					}
            	}
            }

            //Special handler for Times
            $times = array('StartTime','EndTime');
            foreach ($times as $key) {
                if (!empty($params[$key]) && property_exists($className,$key)) {
                    $wo->$key = strtoupper($params[$key]);
                }
            }
            //Special handler for Dates
            $times = array('StartDate' => 'StartTime','EndDate' => 'EndTime');
            foreach ($times as $key => $subkey) {
                if (!empty($params[$key]) && property_exists($className,$key)) {
                    $wo->$key = strtoupper($params[$key]);
                } elseif (property_exists($className,$key)) {
                    $wo->$key = '';
                    $wo->$subkey = '';
                }
            }

            //Special handlers for virtual fields
            if (isset($params['MarkIncompleteV'])) {
                $wo->TechMarkedComplete = false;
            }
            
            
            if (isset($params['Route'])) {
                $wo->Route = $params['Route'];
            }
            
            //--- 13329: CredentialCertificationId
//            if (isset($params['CredentialCertificationId'])) {
//                $wo->CredentialCertificationId = $params['CredentialCertificationId'];
//            }            
            //--- 13329 end
            
             if (isset($params['workOrderOwner']) && $wo->WorkOrderOwner != $params['workOrderOwner']) {
                $wo->WorkOrderOwner = $params['workOrderOwner'];
                
                //Get info from future info table based on new work order owner
             	$future = new Core_FutureWorkOrderInfo();
                // 13731
                $Core_WosClass = new Core_Api_WosClass();
                $owner = $wo->WorkOrderOwner;
			 	//$futureData = $future->find($wo->WorkOrderOwner)->toArray();
                $futureData = $Core_WosClass->getFutureWOInfo($owner);
                //end 13731
			 	if(!empty($futureData)){
			 		if(!empty($futureData['SystemGeneratedEmailTo']))$wo->SystemGeneratedEmailTo = $futureData['SystemGeneratedEmailTo'];
					if(!empty($futureData['SystemGeneratedEmailFrom']))$wo->SystemGeneratedEmailFrom = $futureData['SystemGeneratedEmailFrom'];
					if(!empty($futureData['BidNotificationEmailTo']))$wo->BidNotificationEmailTo = $futureData['BidNotificationEmailTo'];
					if(!empty($futureData['P2TNotificationEmailTo']))$wo->P2TNotificationEmailTo = $futureData['P2TNotificationEmailTo'];
					if(!empty($futureData['WorkAssignedEmailTo']))$wo->WorkAssignedEmailTo = $futureData['WorkAssignedEmailTo'];
					if(!empty($futureData['WorkDoneEmailTo']))$wo->WorkDoneEmailTo = $futureData['WorkDoneEmailTo'];
					if(!empty($futureData['WorkAvailableEmailFrom']))$wo->WorkAvailableEmailFrom = $futureData['WorkAvailableEmailFrom'];
				}
            } 
            
            /**
             * update :
             * How likely would you be to recommend this tech AND
             * The outcome of this field service event was
             * @author Artem Sukharev
             */
            if ( !empty($wo->Tech_ID) && $currentTech == $wo->Tech_ID) {
                $isTRChanged = false;
                if ( null !== $request->getParam('SATRecommended', NULL) ) {
                    $wo->SATRecommended = $request->getParam('SATRecommended', NULL);
                    $isTRChanged = true;
                }
                if ( null !== $request->getParam('SATPerformance', NULL) ) {
                    $wo->SATPerformance = $request->getParam('SATPerformance', NULL);
                    $isTRChanged = true;
                }
                /**
                 * I don't know why but if tech has prefered or denied status
                 * we can not change SATRecommended and SATPerformance fields
                 * in this case first remove status and then after update WO restore it
                 * @TODO: this is temporary solution, fix it
                 */
                if ( $isTRChanged ) {
//                    $search->removeTechPrefer($this->_login.'|pmContext='.$companyId, $this->_password, $wo->Tech_ID);
//                    $search->removeTechDeny($this->_login.'|pmContext='.$companyId, $this->_password, $wo->Tech_ID);
                }
           } else if(
                empty($wo->Tech_ID) || (!empty($wo->Tech_ID) && $currentTech != $wo->Tech_ID))   
           {            
                $wo->SATRecommended = 0;
                $wo->SATPerformance = 0;
                if($wo->Status==WO_STATE_ASSIGNED || $wo->Status==WO_STATE_WORK_DONE || $wo->Status==WO_STATE_INCOMPLETE)
                {
                    $wo->WorkOrderReviewed=0;
                    $wo->TechCheckedIn_24hrs=0;
                    $wo->StoreNotified=0;
                    $wo->Notifiedby="";
                    $wo->DateNotified="";
                    $wo->CheckedIn=0;
                    $wo->Checkedby="";       
                    
                    $wo->Date_In = NULL;
                    $wo->Date_Out = NULL;
                    $wo->Time_In = NULL;
                    $wo->Time_Out = NULL;
                    $wo->Duration = NULL;
                    $wo->TechMiles = NULL;
                    $wo->Paperwork_Received=0;
                    $wo->Incomplete_Paperwork=0;
                    $wo->TechMarkedComplete=0;
                    $wo->CallClosed=0;
                    $wo->TechComments="";
                    //704
                    //$wo->MissingComments=""; 
                    //end 704
                    $wo->bid_comments="";      
                }
            } else {
                $wo->SATRecommended = 0;
                $wo->SATPerformance = 0;
                //982
                $startdate = $wo->StartDate;
                $day1 = new Zend_Date($startdate, 'yyyy-MM-dd');           

                if($day1->isEarlier(Zend_Date::now()->subDay(2)))
                {
                    $wo->TechCheckedIn_24hrs = 0;
            }
                else
                {
                    $daynow = new Zend_Date(Zend_Date::now(), 'yyyy-MM-dd');
                    if($daynow->isEarlier($day1->subDay(2)))
                    {
                        $wo->TechCheckedIn_24hrs =  0;
                    }
                    else
                    {
                        $wo->TechCheckedIn_24hrs =  1;
                    }
                    
                }
                //end 982
            }
            /* ---------------------- */
            $approved_amount1 = 0 ;
            $expenseReportingTotal = $request->getParam('expenseReportingTotalTech',array());
            if(!empty($expenseReportingTotal))
            {
                $Core_Api_WosClass = new Core_Api_WosClass;
                for($i = 0;$i < count($expenseReportingTotal["bcatid"]);$i++)
                {
                    if(!empty($expenseReportingTotal["bcatid"][$i]))
                    {
                        if($expenseReportingTotal["approved"][$i]) {
                            $approved_amount1 += $expenseReportingTotal["approved_amount"][$i];
						}
                    }    
                }    
	            $wo->Total_Reimbursable_Expense = $approved_amount1;
            }   

            //--- update WorkOrder
            $apiResponse = $search->updateWorkOrder($this->_login.'|pmContext='.$companyId, $this->_password, $wo->WIN_NUM, $wo, true, false, $is_reactivation);
            

            //--- Update Work order additional fields //360, 13566, 13622            
            $auditNeededDate = !empty($params['AuditNeededDate'])?$params['AuditNeededDate']:date("Y-m-d H:i:s");
            
            $auditCompleteDate = !empty($params['AuditCompleteDate'])?$params['AuditCompleteDate']:date("Y-m-d H:i:s");
            $search->saveWorkOrderAuditData($wo->WIN_NUM,
                isset($params['AuditNeeded']),$this->_login,date("Y-m-d H:i:s"),
                isset($params['AuditComplete']),$this->_login,date("Y-m-d H:i:s")          
            );
            //--- 13713, 13691, 13732
            $tag = array('EntityTypeId','ServiceTypeId','AllowDeVryInternstoObserveWork','AllowDeVryInternstoPerformField','ACSNotifiPOC','ACSNotifiPOC_Phone','ACSNotifiPOC_Email',
					'SiteContactBackedOutChecked','NotifiedTime');
            $tagUpdateFields = array();            
            foreach($tag as $key)
            {
                if($key=='AllowDeVryInternstoObserveWork' || $key=='AllowDeVryInternstoPerformField' || $key=='SiteContactBackedOutChecked')
                {
                    $tagUpdateFields[$key] = isset($params[$key]);
                }
                else
                {
                    if(isset($params[$key]) && !empty($params[$key]))
                    {
                        $tagUpdateFields[$key] = $params[$key];                    
                    }                    
                }
            }


            if (count($tagUpdateFields) > 0) {
                $search->saveWorkOrderTagsData($wo->WIN_NUM,$wo,$tagUpdateFields,$this->_login);
            }
            
            /**
             * Update Tech Status
             * @author Artem Sukharev
             */
            if ( !empty($wo->Tech_ID) && $currentTech == $wo->Tech_ID) {
                /* Mark Tech as a Preferred Tech */
                if ( $request->getParam('PreferTechV', NULL) ) {
                    $search->removeTechDeny($this->_login.'|pmContext='.$companyId, $this->_password, $wo->Tech_ID);
                    $search->techPrefer($this->_login.'|pmContext='.$companyId, $this->_password, $wo->Tech_ID);
                }
                /* Client Deny Tech */
                elseif ( $request->getParam('DenyTechV', NULL) ) {
                    $search->removeTechPrefer($this->_login.'|pmContext='.$companyId, $this->_password, $wo->Tech_ID);
                    $search->techDeny($this->_login.'|pmContext='.$companyId, $this->_password, $wo->Tech_ID, '');
                }
                /* None */
                else {
                    $search->removeTechPrefer($this->_login.'|pmContext='.$companyId, $this->_password, $wo->Tech_ID);
                    $search->removeTechDeny($this->_login.'|pmContext='.$companyId, $this->_password, $wo->Tech_ID);
                }
            }
            /* ---------------------- */
            //13969
            $valProjectId = 0;
            if($wo->Status==WO_STATE_ASSIGNED || $wo->Status==WO_STATE_PUBLISHED || $wo->Status==WO_STATE_CREATED)
            {
                 $valProjectId = $wo->Project_ID;
            }
            //End 13969
            $errors = Core_Api_Error::getInstance();
            $errorServiceSchedule = false;
            $Core_Api_WosClass = new Core_Api_WosClass();
            $ServiceSchedule = $params['ServiceSchedule'];
            $jsonServiceSchedule = array();
            if(!empty($ServiceSchedule)){
                $jsonServiceSchedule = json_decode("[".base64_decode($ServiceSchedule)."]");
                if(!empty($jsonServiceSchedule)){
                    $ik = 0;
                    foreach($jsonServiceSchedule as $item){
                        $ik++;
                        if(strtolower($item->Remove) == "true"){
                            continue;
                        }else{
                            //valid 13969
                            $errors->resetErrors();
                        	$Core_Api_WosClass->validWOVisit(
                                    $item->WoVisitID,
                                    "",
                                    $item->StartTypeID,
                                    $item->StartDate,
                                    $item->StartTime,
                                    $item->EndDate,
                                    $item->EndTime,
                                    $item->EstimatedDuration,
                                    $item->TechArrivalInstructions,
                                    $valProjectId);
                                if($errors->hasErrors()){
                                        $errorServiceSchedule = true;
                                        $errorArr = $errors->getErrorArray ();                                     
                                        foreach ($errorArr as $error) {                                       
                                            $return['errors'][] = "Visit #".$ik.": " . $error["description"];
                                        }
				}
                        }
                    }
                }
            }                 
            $AllWOVisitList = $params['AllWOVisitList'];
           
            if(!empty($AllWOVisitList) && !$errorServiceSchedule){
                $jsonAllWOVisitList = json_decode("[".base64_decode($AllWOVisitList)."]");
                if(!empty($jsonAllWOVisitList)){
                    foreach($jsonAllWOVisitList as $item){
                        $errors->resetErrors();
                        $Core_Api_WosClass->validWOVisitCheckInOutForClient(
                                $item->woVisitID
                                ,$params['WIN_NUM']
                                ,$item->CORCheckInDate
                                ,$item->CORCheckInTime
                                ,$item->CORCheckOutDate
                                ,$item->CORCheckOutTime
                                ,$item->OptionCheckInOut
                                ,$item->type
                                ,$item->disable);

                        if($errors->hasErrors()){
                                $errorServiceSchedule = true;
                                $err = $errors->getErrorArray ();
                                $return['errors'][] = $err[0]["description"];
                                $err = true;
                        }
                    }
                }
            }
            if(!$errorServiceSchedule){
            if ( empty($apiResponse->errors) && empty($customErrors)) {
                    
                //686
//                    $CompletionStatus = $params["CompletionStatus"];
                    $WoTagClass = new Core_Api_WoTagClass();
                    $CancelInternalIssuestatusval = $params['cbxCompletionStatus_40'];
                    $Incomplete_NaturalCausesOrForceMajeurestatusval = $params['cbxCompletionStatus_36'];
                    $Rescheduled_NaturalCausesOrForceMajeurestatusval = $params['cbxCompletionStatus_37'];
                    //847
                    $Complete1stVisitnote = $params['wintagitemaddnotevalue25'];
                    $CompleteMultiVisitnote = $params['wintagitemaddnotevalue26'];
                    $CompleteReassignednote = $params['wintagitemaddnotevalue27'];
                    $CompleteReschedulednote = $params['wintagitemaddnotevalue28'];

                    $IncompleteNaturalCausesnote = $params['wintagitemaddnotevalue36'];

                    $CancelInternalIssuenote = $params['wintagitemaddnotevalue40'];
                    $RescheduledNaturalCausesnote = $params['wintagitemaddnotevalue37'];

                    //end 847
                    //13898
                    $win = $wo->WIN_NUM;                         
                    $result = $WoTagClass->exists_WinTagForWOWithTagItemCode($win,'Complete1stVisit');                   
                    $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByWinAndWintagitemcode($win,'Complete1stVisit');
                    if(isset($params["Complete1stVisit"]))
                    {
                        if(!$result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Added: Complete - 1st Visit", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }else
                        {    if(!empty($Complete1stVisitnote)&& strcmp($tagListForWin["notes"], $Complete1stVisitnote )!= 0)
                             {                                       
                                        Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Edit Notes: Complete - 1st Visit", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                             }  
                        }
                        $WoTagClass->saveWoTagItemValue_forCompletionStatus($win,"Complete1stVisit",1,1,"",$Complete1stVisitnote);
                                    }
                    else
                    {
                        if($result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Removed: Complete - 1st Visit", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
                        $WoTagClass->deleteWoTagItemValue_byWinAndItemCode_forCompletionStatus($win, "Complete1stVisit");
                    }
                    $result = $WoTagClass->exists_WinTagForWOWithTagItemCode($win,'CompleteMultiVisit');
                    $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByWinAndWintagitemcode($win,'CompleteMultiVisit');
                    if(isset($params["CompleteMultiVisit"]))
                    {
                        if(!$result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Added: Complete - Multi Visit", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
                        else
                        {    if(!empty($CompleteMultiVisitnote) && strcmp($tagListForWin["notes"], $CompleteMultiVisitnote )!= 0)
                             {                                       
                                        Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Edit Notes: Complete - Multi Visit", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                             }  
                        }
                        $WoTagClass->saveWoTagItemValue_forCompletionStatus($win,"CompleteMultiVisit",1,1,"",$CompleteMultiVisitnote);
                    }
                    else
                    {
                        if($result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Removed: Complete - Multi Visit", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
                        $WoTagClass->deleteWoTagItemValue_byWinAndItemCode_forCompletionStatus($wo->WIN_NUM, "CompleteMultiVisit");
                                }
                    $result = $WoTagClass->exists_WinTagForWOWithTagItemCode($win,'CompleteReassigned'); 
                    $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByWinAndWintagitemcode($win,'CompleteReassigned');
                    if(isset($params["CompleteReassigned"]))
                    {
                        if(!$result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Added: Complete - Reassigned", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
                        else
                        {   if(!empty($CompleteReassignednote) && strcmp($tagListForWin["notes"], $CompleteReassignednote )!= 0)
                             {                                       
                                        Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Edit Notes: Complete - Reassigned", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                             }  
                        }
                        $WoTagClass->saveWoTagItemValue_forCompletionStatus($win,"CompleteReassigned",1,1,"",$CompleteReassignednote);
                    }
                    else
                    {
                        if($result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Removed: Complete - Reassigned", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
                        $WoTagClass->deleteWoTagItemValue_byWinAndItemCode_forCompletionStatus($wo->WIN_NUM, "CompleteReassigned");
                            }
                    $result = $WoTagClass->exists_WinTagForWOWithTagItemCode($win,'CompleteRescheduled');        
                    $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByWinAndWintagitemcode($win,'CompleteRescheduled');
                    if(isset($params["CompleteRescheduled"]))
                    {
                        if(!$result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Added: Complete - Rescheduled", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }else
                        {    if(!empty($CompleteReschedulednote) && strcmp($tagListForWin["notes"], $CompleteReschedulednote )!= 0)
                             {                                       
                                        Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Edit Notes: Complete - Rescheduled", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                             }  
                        }
                        $WoTagClass->saveWoTagItemValue_forCompletionStatus($win,"CompleteRescheduled",1,1,"",$CompleteReschedulednote);
                    }
                    else
                    {
                        if($result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Removed: Complete - Rescheduled", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
                        $WoTagClass->deleteWoTagItemValue_byWinAndItemCode_forCompletionStatus($win, "CompleteRescheduled");
                    }
                    $result = $WoTagClass->exists_WinTagForWOWithTagItemCode($win,'CancelInternalIssue');
                    $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByWinAndWintagitemcode($win,'CancelInternalIssue');
                    if(isset($params["CancelInternalIssue"]))
                    {
                        if(!$result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Added: Site Cancelled", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }else
                        {    if(!empty($CancelInternalIssuenote) && strcmp($tagListForWin["notes"], $CancelInternalIssuenote )!= 0)
                             {                                       
                                        Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Edit Notes: Site Cancelled", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                             }  
                        }
                        $WoTagClass->saveWoTagItemValue_forCompletionStatus($win,"CancelInternalIssue",$CancelInternalIssuestatusval,1,"",$CancelInternalIssuenote);
                    }
                    else
                    {
                        if($result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Removed: Site Cancelled", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
                        $WoTagClass->deleteWoTagItemValue_byWinAndItemCode_forCompletionStatus($win, "CancelInternalIssue");
                        }
                    $result = $WoTagClass->exists_WinTagForWOWithTagItemCode($win,'Incomplete_NaturalCausesOrForceMajeure');
                    $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByWinAndWintagitemcode($win,'Incomplete_NaturalCausesOrForceMajeure');
                    if(isset($params["Incomplete_NaturalCausesOrForceMajeure"]))
                    {
                        if(!$result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Added: Incomplete - Natural Causes", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }else
                        {    if(!empty($IncompleteNaturalCausesnote) && strcmp($tagListForWin["notes"], $IncompleteNaturalCausesnote )!= 0)
                             {                                       
                                  Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Edit Notes: Incomplete - Natural Causes", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                             }  
                        }
                        $WoTagClass->saveWoTagItemValue_forCompletionStatus($win,"Incomplete_NaturalCausesOrForceMajeure",$Incomplete_NaturalCausesOrForceMajeurestatusval,1,"",$IncompleteNaturalCausesnote);
                    }
                    else
                    {
                        if($result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Removed: Incomplete - Natural Causes", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
                        $WoTagClass->deleteWoTagItemValue_byWinAndItemCode_forCompletionStatus($win, "Incomplete_NaturalCausesOrForceMajeure");
                    }
                    $result = $WoTagClass->exists_WinTagForWOWithTagItemCode($win,'Rescheduled_NaturalCausesOrForceMajeure');
                    $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByWinAndWintagitemcode($win,'Rescheduled_NaturalCausesOrForceMajeure');
                    if(isset($params["Rescheduled_NaturalCausesOrForceMajeure"]))
                    {
                        if(!$result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Added: Rescheduled - Natural Causes", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }else
                        {    if(!empty($RescheduledNaturalCausesnote) && strcmp($tagListForWin["notes"], $RescheduledNaturalCausesnote )!= 0)
                             {                                       
                                        Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Edit Notes: Rescheduled - Natural Causes", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                             }  
                        }
                        $WoTagClass->saveWoTagItemValue_forCompletionStatus($win,"Rescheduled_NaturalCausesOrForceMajeure",$Rescheduled_NaturalCausesOrForceMajeurestatusval,1,"",$RescheduledNaturalCausesnote);
                    }
                    else
                    {
                        if($result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Removed: Rescheduled - Natural Causes", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
                        $WoTagClass->deleteWoTagItemValue_byWinAndItemCode_forCompletionStatus($win, "Rescheduled_NaturalCausesOrForceMajeure");
                     }


                    $wintagitemvalueid = $params["wintagitemvalueid"];
                    //871
                    $wintagitemdeletevalueid = $params["wintagitemdeletevalueid"];
                    if(is_array($wintagitemdeletevalueid) && !empty($wintagitemdeletevalueid))
                    {
                        foreach($wintagitemdeletevalueid as $valueid){
                            if(!empty($valueid))
                            {
                            $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByID($valueid);                               
                            $des = "WIN-Tag Removed: ".$tagListForWin["item_sortdesc"].' '.$tagListForWin["item_owner"]." Caused";    
                            Core_TimeStamp::createTimeStamp($win, $this->_login,$des , $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);    
                            }                        
                        }
                        $WoTagClass->deleteWoTagItemValueIDs($wintagitemdeletevalueid);
                                }
                    
                    $CompletionStatusval = $params["CompletionStatusval"];
                    $wintagitemaddnotevalue = $params["wintagitemaddnotevalue"];
                    $wintagid = $params["wintagid"];
                    $itemno=0;
                    if(is_array($CompletionStatusval) && !empty($CompletionStatusval)){
                        foreach($CompletionStatusval as $item){
                   
                           $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByID($wintagitemvalueid[$itemno]);  
                           $tagList = $WoTagClass->getTagItemInfo($wintagid[$itemno]);  
                           if(empty($wintagitemvalueid[$itemno]))
                            {
                               //add new
                                $WoTagClass->saveWoTagItemValue($win,$wintagid[$itemno],$item,0,'',1,$wintagitemaddnotevalue[$itemno]);
                                $des = "WIN-Tag Added: ".$tagList["item_sortdesc"].' '.$tagList["item_owner"]." Caused";                                    
                                Core_TimeStamp::createTimeStamp($win, $this->_login, $des, $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);                                                                        
                                                                       
                            }
                            else
                            {
                                if(!in_array($wintagitemvalueid[$itemno], $wintagitemdeletevalueid))
                                {
                                     if(!empty($wintagitemaddnotevalue[$itemno]) && strcmp($tagListForWin["notes"], $wintagitemaddnotevalue[$itemno] )!= 0)
                                    {
                                        $des = "WIN-Tag Edit Notes: ".$tagList["item_sortdesc"].' '.$tagList["item_owner"]." Caused";
                                        Core_TimeStamp::createTimeStamp($win, $this->_login, $des, $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                                    }                                        
                                    $WoTagClass->saveWoTagItemValue($win,$wintagid[$itemno],$item,1,'',0,$wintagitemaddnotevalue[$itemno],$wintagitemvalueid[$itemno]);
                                }
                            }
                            $itemno +=1;
                        }
                    }
//End 13898         
                    //end 871
                    if(!isset($params["chkIncompleteSiteClientCaused"]))
                    {
                        $WoTagClass->deleteWoTagItemValue_byWinAndIncompleteSiteClient($wo->WIN_NUM);
                    }

                    if(!isset($params["chkIncompleteTechProviderCaused"]))
                    {
                        $WoTagClass->deleteWoTagItemValue_byWinAndIncompleteTechProvider($wo->WIN_NUM);
                        }

                    if(!isset($params["chkRescheduledSiteClientCaused"]))
                    {
                        $WoTagClass->deleteWoTagItemValue_byWinAndRescheduledSiteClient($wo->WIN_NUM);
                    } 

                    if(!isset($params["chkRescheduledTechProviderCaused"]))
                    {
                        $WoTagClass->deleteWoTagItemValue_byWinAndRescheduledTechProvider($wo->WIN_NUM);            
                    }
                    //end 686
                    //13843
                    $WoTagClass->updateSiteStatus_ByWin($wo->WIN_NUM);
                    //end 13843                    
                    // save tag
                    $Core_Api_WoTagClass = new Core_Api_WoTagClass();
                    $tagArray = array();
                    $tagArray[0] = array('TagCodeId'=>'ServiceTypeId', 'TagCodeValue'=>$params['ServiceTypeId']);
                    $tagArray[1] = array('TagCodeId'=>'EntityTypeId', 'TagCodeValue'=>$params['EntityTypeId']);
  
                    $Core_Api_WoTagClass->saveWOTags($params['WIN_NUM'],$tagArray);
                    
                    // update CredentialCertificationId
                    if(!isset($params["chkRescheduledTechProviderCaused"]))
                    {
                        $WoTagClass->deleteWoTagItemValue_byWinAndRescheduledTechProvider($wo->WIN_NUM);            
                    }
                    //end 686
                    // save tag
                    $Core_Api_WoTagClass = new Core_Api_WoTagClass();
                    $tagArray = array();
                    $tagArray[0] = array('TagCodeId'=>'ServiceTypeId', 'TagCodeValue'=>$params['ServiceTypeId']);
                    $tagArray[1] = array('TagCodeId'=>'EntityTypeId', 'TagCodeValue'=>$params['EntityTypeId']);
  
                    $Core_Api_WoTagClass->saveWOTags($params['WIN_NUM'],$tagArray);
                    
                    // update CredentialCertificationId
                    if (isset($params['CredentialCertification_id_1']) && $params['CredentialCertification_id_1'] > 0){
                    // update CredentialCertificationId                    if (isset($params['CredentialCertificationId']) && $params['CredentialCertificationId'] > 0){
                        $Core_WorkOrderCertification = new Core_WorkOrderCertification($wo->WIN_NUM,$params['CredentialCertification_id_1']);
                        $Core_WorkOrderCertification->saveExt();
                    }

					//Save credential requirements for FS-RUSH
					$process_certs = function ($field, $params) {
						$result = array ();
						$index = 1;

						do {
							$id = null;
							if (isset ($params[$field . "_id_" . $index])) {
								$id = $params[$field . "_id_" . $index];
								$level = $params[$field . "_level_" . $index];
								$result[$id] = $level;
								$index++;
							}
						} while (!empty ($id));

						return $result;
					};
					$error = $ProjectClass->saveP2TCertRequirements ($params['WIN_NUM'], array (
						"client" => $process_certs ("CredentialCertification", $params),
						"public" => $process_certs ("PublicCertification", $params),
						"expert" => $process_certs ("FSExpert", $params),
						"industry" => $process_certs ("IndustryCertification", $params)
					), true, false);	//Update (true), not a project (false), writes to work order

                    $err = false;
                    // update AllWOVisitList
                    $Core_Api_WosClass = new Core_Api_WosClass();
                    $AllWOVisitList = $params['AllWOVisitList'];
                    if(!empty($AllWOVisitList)){
                        $jsonAllWOVisitList = json_decode("[".base64_decode($AllWOVisitList)."]");

                        if(!empty($jsonAllWOVisitList)){
                            foreach($jsonAllWOVisitList as $item){
								$errors->resetErrors ();

								$Core_Api_WosClass->saveWOVisitCheckInOutForClient(
                                        $item->woVisitID
                                        ,$params['WIN_NUM']
                                        ,$item->CORCheckInDate
                                        ,$item->CORCheckInTime
                                        ,$item->CORCheckOutDate
                                        ,$item->CORCheckOutTime
                                        ,$item->OptionCheckInOut
                                        ,$item->type
                                        ,$item->disable
                                        ,$item->AddedBySection);

//                                        if($errors->hasErrors()){
//                                                $errorServiceSchedule = true;
//                                                $err = $errors->getErrorArray ();
//                                                $return['errors'][] = $err[0]["description"];
//                                                $err = true;
//                                        }
								}
                            }
                        }
                    // update ServiceSchedule
                    $ServiceSchedule = $params['ServiceSchedule'];
                    if(!empty($ServiceSchedule)){
                        $jsonServiceSchedule = json_decode("[".base64_decode($ServiceSchedule)."]");
                                         
                        
                        if(!empty($jsonServiceSchedule)){
                            $ik = 0;
                            foreach($jsonServiceSchedule as $item){
                                //remove
                                $ik++;
                                if(strtolower($item->Remove) == "true"){
                                    if((int)$item->WoVisitID == 0) continue;
                                    $Core_Api_WosClass->removeWOVisit($item->WoVisitID);
                                }else{
                                    //save
//error_log (var_export ($item, true));
                                	$errors = $Core_Api_WosClass->saveWOVisit(
                                            $item->WoVisitID,
                                            $params['WIN_NUM'],
                                            $item->StartTypeID,
                                            $item->StartDate,
                                            $item->StartTime,
                                            $item->EndDate,
                                            $item->EndTime,
                                            $item->EstimatedDuration,
                                            $item->TechArrivalInstructions,
                                            NULL,
                                            NULL,
                                            NULL,
                                            $item->AddedBySection);
//                                    if(!empty($errors)){
//                                        $err = true;
//                                        $return['errors'][] = "Visit #".$ik.": ".$errors->errors[0]->message;;
//                                    }
                                    }
                                }
                            }
                        }
                    // end ServiceSchedule
                if(!empty($removefiles)){
                    $files = new Core_Files();
                    $files->removeFiles($params['WIN_NUM'], $removefiles);
                }
                    if($err){
                        $return['success'] = 0;
                    }else{
                        $Core_Api_WosClass->syncNumVisits($params['WIN_NUM']);
                $return['success'] = 1;
                $return['win'] = $params['WIN_NUM'];
                    }
            } else {
                $return['success'] = 0;
                $return['errors'] = array();
                foreach ($apiResponse->errors as $e) {
                    $return['errors'][]=$e->message;
                }
            	foreach($customErrors as $e){
            		$return['errors'][] = $e;
            	}
            }
            }else{
                $return['success'] = 0;
            }
            
            //update Expense Reporting:
            $expenseReportingTotalTech = $request->getParam('expenseReportingTotalTech',array());
            if(!empty($expenseReportingTotalTech))
            {
                $Core_Api_WosClass = new Core_Api_WosClass;
                for($i = 0;$i < count($expenseReportingTotalTech["bcatid"]);$i++)
                {
                    if(!empty($expenseReportingTotalTech["bcatid"][$i]))
                    {
                        $bcatid = $expenseReportingTotalTech["bcatid"][$i];
                        $approved = $expenseReportingTotalTech["approved"][$i];
                        $reimbursable_thru_fs_id = $expenseReportingTotalTech["reimbursable_thru_fs_id"][$i];
                        $approved_amount = $expenseReportingTotalTech["approved_amount"][$i];
                        $Core_Api_WosClass->updateDataForWOBCat($bcatid,$reimbursable_thru_fs_id,$approved,$approved_amount);
                    }    
                }    
            }    
			
			//--- 13418 update Total_Reimbursable_Expense
            $Api_WosClass = new Core_Api_WosClass();
            $result = $Api_WosClass->update_Total_Reimbursable_Expense($params['WIN_NUM']);       

            //--- 13555 update Base Visit CheckIn For Tech
            if(!empty($wo->CheckedIn) &&  !empty($params['Date_Checked_in']))
            {                
                 $Api_WosClass->updateBaseVisit_CheckInForTech($wo->WIN_NUM,$params['Date_Checked_in'],$params['CheckedTime']);
            }
            //$ProjectClass = new Core_Api_ProjectClass();
            $ProjectClass->updateServiceTypeWO_FromServiceTypeProject($wo->WIN_NUM,$currentProject);
            //13871           
            $visits = $params["Visit"];
            if(is_array($visits) && !empty($visits)){
                $WoTagClass = new Core_Api_WoTagClass();
                foreach($visits as $item){
                    $WoTagClass->saveOntimePerformanceResult($win,$item,
                            $params["StartResultList_".$item],
                            $params["EndResultList_".$item],
                            $params["DurationResultList_".$item]);
                }
            }            
        } else {
            $return['success'] = 0;
            $return['errors'] = array('There is no such Work Order');
            foreach ($apiResponse->errors as $e) {
                $return['errors'][]=$e->message;
            }
            
        }
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                 ->setBody($return);
    }

    public function ajaxUploadFileAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $company = $this->view->company;

        $maxFileSize = 52428800; //50Mb

        $request = $this->getRequest();

        $win   = $request->getParam('win', NULL);
        $id    = $request->getParam('file', NULL);
        $descr = $request->getParam('descr', NULL);
        $descr = trim($descr);

        $return = array('success'=>1);
        if (
            !empty($id)                                        &&
            !empty($_FILES["Pic{$id}"])                        &&
            is_uploaded_file($_FILES["Pic{$id}"]['tmp_name'])  &&
            is_readable($_FILES["Pic{$id}"]['tmp_name'])       &&
            $_FILES["Pic{$id}"]['error'] == UPLOAD_ERR_OK      &&
            filesize($_FILES["Pic{$id}"]['tmp_name']) <= $maxFileSize &&
            filesize($_FILES["Pic{$id}"]['tmp_name']) > 0 &&
            mb_strlen($descr, 'utf-8') <= 25
        ) {
            $uploader = new Core_Api_Class();
            if ($id != "CustomSignOff") {
	            $type  = (strpos($id, 'Tech') !== false) ? Core_Files::TECH_FILE : Core_Files::WOS_FILE;
            }
            else {
            	$type = Core_Files::SIGNOFF_FILE;
            }
            $fname = $_FILES["Pic{$id}"]['name'];
            if ( strrpos($fname, '/') !== FALSE ) {
                $fname = substr($fname, strrpos($fname, '/'));
            }

	        if ($type == Core_Files::SIGNOFF_FILE) {
		    	require_once ("../htdocs/library/fpdf/fpdf.php");
		    	require_once ("../htdocs/library/fpdi/fpdi.php");

		    	$fpdf = new FPDI ();
		    	$pagecount = $fpdf->setSourceFile($_FILES["Pic{$id}"]['tmp_name']);

		    	if ($pagecount <= 0) {
		    		$return["success"] = 0;
	                $return['errors'] = array("Custom Signoff must be an unencrypted PDF");
		    	}
	        }

            /**
             * Generate unig name for file
             * @author Artem Sukharev
             * @see https://tickets.fieldsolutions.com/issues/12180
             */
	        if ($return["success"] == 1) {
	            $info = pathinfo($fname);
	            $fname = uniqid (str_replace (",", "", $info['filename']) . '_');
	            $fname = $fname . '.' . $info['extension'];

	            $fdata     = file_get_contents($_FILES["Pic{$id}"]['tmp_name']);
	            $result = $uploader->uploadFileAWS($this->_login . "|pmContext=$company", $this->_password, $win, $fname, $fdata, $type, $descr);
	            if ( !$result->success ) {
	                $return['success'] = 0;
	                $return['errors'] = array();
	                foreach ($result->errors as $e) {
	                    $return['errors'][]=$e->message;
	                }
	            } else {
	                $return['success']=1;
	                $return['id']=$result->data;
	            }
	        }
        } else {
            $return['success']=0;
            $return['errors']=array();
            if ( empty($id) ) {
                $return['errors'][]='Incorrect parameters: ID is required and cannot be empty';
            } else if ( empty($_FILES["Pic{$id}"]) ) {
                $return['errors'][]= 'File not uploaded';
            } else if ( !is_uploaded_file($_FILES["Pic{$id}"]['tmp_name']) ) {
                $return['errors'][]= 'File not uploaded.';
            } else if ( !is_readable($_FILES["Pic{$id}"]['tmp_name']) ) {
                $return['errors'][]= 'Uploaded file not readabale';
            } else if ( filesize($_FILES["Pic{$id}"]['tmp_name']) > $maxFileSize ) {
                $return['errors'][]= 'Uploaded file size too big. Please call resource coordinator for assistance.';
            } else if ( filesize($_FILES["Pic{$id}"]['tmp_name']) == 0 ) {
                $return['errors'][]= 'Uploaded file is empty.';
            } else if ( $_FILES["Pic{$id}"]['error'] != UPLOAD_ERR_OK ) {
                switch ( $_FILES["Pic{$id}"]['error'] ) {
                    case UPLOAD_ERR_FORM_SIZE :
                        $return['errors'][]= 'Uploaded file size too big. Please call resource coordinator for assistance.';
                        break;
                    case UPLOAD_ERR_PARTIAL :
                        $return['errors'][]= 'The uploaded file was only partially uploaded';
                        break;
                    case UPLOAD_ERR_NO_FILE :
                    case UPLOAD_ERR_NO_TMP_DIR :
                    case UPLOAD_ERR_CANT_WRITE :
                    case UPLOAD_ERR_EXTENSION :
                        $return['errors'][]= 'No file was uploaded';
                }
            } else if(mb_strlen($descr, 'utf-8') >25){
                $return['errors'][]= 'Optional description for uploaded file should containes less then 25 characters.';
            }
            
            error_log('FileUploading: (client:AUFA, win:' . $win . ',id: ' . $id . ') ' . implode('|', $return['errors']));
        }
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-Length', strlen($return), true);
        $response->setBody($return);
    }
    function listTechInSiteAction(){
        $request = $this->getRequest();
        $win   = $request->getParam('win', 0);
        $siteNumber   = $request->getParam('siteNumber', 0);
        $Project_ID   = $request->getParam('Project_ID', 0);
        $company    = $this->view->company;
        $Core_Api_WosClass= new Core_Api_WosClass();
        $search = new Core_Api_Class;
        $wo = $search->getWorkOrder(
            $this->_login.'|pmContext='.$companyId,
            $this->_password,
            $win
        );
        $wo = $wo->data[0];
        $TechsInSite = $Core_Api_WosClass->getTechsInSite($siteNumber,$Project_ID);
        $this->view->TechsInSite = $TechsInSite;
        if(empty($TechsInSite)){
            $this->_helper->viewRenderer->setNoRender(true);
            echo "data empty xml";die;
        }
    }

    public function updateClientDenyTechAction()
    {
        $params = $this->getRequest()->getParams();
        $techID=$params['techID'];
        $clientID=$params['clientID'];
        $companyID=$params['companyID'];
        $Core_Api_WosClass=new Core_Api_WosClass();
        $Result= $Core_Api_WosClass->getClientDeniedTechInfo($techID,$companyID);
        $this->view->comment=$Result['Comments'];
        $this->view->techID=$techID;
        $this->view->clientID=$clientID;
        $this->view->companyID=$companyID;       
    }

    public function doTechInSiteAction(){
        $request = $this->getRequest();
        $win   = $request->getParam('win', 0);
        $Core_Api_WosClass= new Core_Api_WosClass();
        $return = array("success"=>1,"errors"=>"");
        $AssignedTech = $Core_Api_WosClass->Exists_AssignedTech($win);
        if($AssignedTech) {
            $return["success"] = 2;
        }
        $this->_helper->json($return);
	}
    public function doUpdateClientDenyTechAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getRequest()->getParams();
        $techID=$params['techID'];
        $clientID=$params['clientID'];
        $companyID=$params['companyID'];
        $deniedComments=$params['deniedComments'];
        $Core_Api_WosClass=new Core_Api_WosClass();
        $Core_Api_WosClass->saveClientDeniedTechComment($techID,$clientID,$companyID,$deniedComments);
        echo "<script type='text/javascript'>
		parent.ClosePopUpClientDenyTechAfterSave();
        </script>";
    }

    public function viewClientDenyTechAction(){
        $params = $this->getRequest()->getParams();
        $techID=$params['techID'];
        $clientID=$params['clientID'];
        $companyID=$params['companyID'];
        $Core_Api_WosClass=new Core_Api_WosClass();
        $Result= $Core_Api_WosClass->getClientDeniedTechInfo($techID,$companyID);
        $this->view->comment=$Result['Comments'];
        $this->view->clientUserName=$Result['ClientUserName'];
        $this->view->techID=$techID;
        $this->view->clientID=$clientID;
        $this->view->companyID=$companyID;  
    }
    public function getTotalHoursAction(){
        $params = $this->getRequest()->getParams();
        $CORCheckInDate=$params['CORCheckInDate'];
        $CORCheckInTime=$params['CORCheckInTime'];
        $CORCheckOutDate=$params['CORCheckOutDate'];
        $CORCheckOutTime=$params['CORCheckOutTime'];
        
        $dateIn = date("Y-m-d", strtotime($CORCheckInDate));
        $dateOut = date("Y-m-d", strtotime($CORCheckOutDate));
        $inStamp  = strtotime(trim($dateIn).' '.$CORCheckInTime);
        $outStamp = strtotime(trim($dateOut).' '.$CORCheckOutTime);
        $total = $outStamp - $inStamp;        
        $result = "";
        if ($total > 0) {
            $ost = $total%3600;
            $result = (!$ost) ?
                ($total/3600).(($total/3600) == 1 ?" hour":' hours') : 
                (($total-$ost)/3600).((($total-$ost)/3600) == 1?" hour ":' hours ').round($ost/60).' min';;
        }else{
            $total = 0;
    	}

    	$this->_helper->json(array("result"=>$result,"total"=>$total));
    }

    public function updateProjectWorkorderAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        
        $companyId = $this->view->company;
        $params = $this->getRequest()->getParams();
        $search = new Core_Api_Class;
        $return = array('success' => 0);
        $Nosave = array("PayMax");
        $wo = $search->getWorkOrder(
                $this->_login . '|pmContext=' . $companyId, $this->_password, $params['WIN_NUM']
        );

        if ($wo->success && !empty($wo->data[0]->WIN_NUM))
        {
            $wo = $wo->data[0];
            $className = get_class($wo);

            $currentProject = $wo->Project_ID;
            $currentTech = $wo->Tech_ID;

            foreach ($params as $key => $value)
            {
                if (property_exists($className, $key))
                {
                    if(in_array($key, $Nosave))
                    {
                        continue;
}

                    $wo->$key = $value;
                }
            }

            if (!empty($params["Project_ID"]) && $currentProject != $params["Project_ID"])
                unset($wo->Project_Name);

            //Special handler for checkboxes.
            $checkboxes = array('ShowTechs', 'Update_Requested', 'Lead', 'Assist', 'SignOffSheet_Required', 'WorkOrderReviewed', 'TechCheckedIn_24hrs', 'CheckedIn', 'StoreNotified', 'Paperwork_Received', 'Incomplete_Paperwork', 'TechMarkedComplete', 'CallClosed', 'Approved', 'Deactivated', 'PartsProcessed', 'AbortFee', 'ShortNotice', 'PcntDeduct', 'PartManagerUpdate_Required');
            foreach ($checkboxes as $c)
            {
                if (!property_exists($className, $c))
                    continue;

                $wo->$c = isset($params[$c]);
            }

            

            //Special Handler for FLS Back Out/No Show Tech ID's
            if ($companyId == "FLS" && (!empty($params['BackOut_Tech']) || !empty($params['NoShow_Tech']) ))
            {
                //Lookup Tech_ID from techs table based on FLS ID entered into the BackOut or No Show Field
                if (!empty($params['BackOut_Tech']))
                {
                    $tech = new API_Tech();
                    //$tech->lookupID($params['BackOut_Tech']);
                    if (!$tech->lookupID($params['BackOut_Tech']))
                    {
                        $customErrors[] = 'Tech Not Found For The Back Out Tech ID Entered';
                    } else
                    {
                        $wo->BackOut_Tech = $tech->TechID;
                    }
                }

                if (!empty($params['NoShow_Tech']))
                {
                    $tech = new API_Tech();
                    //$tech->lookupID($params['NoShow_Tech']);
                    if (!$tech->lookupID($params['NoShow_Tech']))
                    {
                        $customErrors[] = 'Tech Not Found For The No Show Tech ID Entered';
                    } else
                    {
                        $wo->NoShow_Tech = $tech->TechID;
                    }
                }
            }
            $apiResponse = $search->updateWorkOrder($this->_login . '|pmContext=' . $companyId, $this->_password, $wo->WIN_NUM, $wo, true);
            $apiWosClass= new Core_Api_WosClass();
            $apiWosClass->OverwriteWOERDataFromProjectERData($wo->WIN_NUM,$params["Project_ID"],$params['company']);
            
            if(empty($apiResponse->errors))
            {
                $return['success'] = 1;
            }
            else {
                    $return['success'] = 0;
                    $return['errors'] = array();
                    foreach ($apiResponse->errors as $e) {
                        $return['errors'][]=$e->message;
                    }
                    foreach($customErrors as $e){
                            $return['errors'][] = $e;
                    }
                }
        } else
        {
            $return['success'] = 0;
            $return['errors'] = array('There is no such Work Order');
            foreach ($apiResponse->errors as $e)
            {
                $return['errors'][] = $e->message;
            }
        }
        
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                ->setBody($return);
    }
    function winTagsAction(){
        $WoTagClass = new Core_Api_WoTagClass();
        $request = $this->getRequest();
        $params = $request->getParams();
        $win = $params['win'];
         if(!empty($params["por"])){
             $this->_helper->viewRenderer->setNoRender(true);
             $CompletionStatus = $params["CompletionStatus"];
             if(is_array($CompletionStatus) && !empty($CompletionStatus)){
                foreach($CompletionStatus as $item){
                    $WoTagClass->saveWoTagItemValue($win,$item,$params["cbxCompletionStatus_".$item]);
                }
            }
             $this->_helper->json(array("success" => true));
         }else{
             $this->view->CompletionStatusTagList = $WoTagClass->getCompletionStatusTagList();
         }

    }

	public function getTechPayDataAction () {
		//Anonymous functions for applying rules to limit amounts by base value (13922)
		$limit_at_least = function (&$result, $base, $percentage) {
			if ($result["tech_pay"] < $base["tech_pay"] * $percentage)
				$result["tech_pay"] = $base["tech_pay"] * $percentage;
			if ($result["tech_pay_low"] < $base["tech_pay_low"] * $percentage)
				$result["tech_pay_low"] = $base["tech_pay_low"] * $percentage;
			if ($result["tech_pay_high"] < $base["tech_pay_high"] * $percentage)
				$result["tech_pay_high"] = $base["tech_pay_high"] * $percentage;
		};

		$limit_at_most = function (&$result, $base, $percentage) {
			if ($result["tech_pay"] > $base["tech_pay"] * $percentage)
				$result["tech_pay"] = $base["tech_pay"] * $percentage;
			if ($result["tech_pay_low"] > $base["tech_pay_low"] * $percentage)
				$result["tech_pay_low"] = $base["tech_pay_low"] * $percentage;
			if ($result["tech_pay_high"] > $base["tech_pay_high"] * $percentage)
				$result["tech_pay_high"] = $base["tech_pay_high"] * $percentage;
		};

		$params = $this->getRequest()->getParams();
        $db = Core_Database::getInstance();
		$multiplier = 1.05;
		$compare_tech_pay = false;
		$result = array ();

        $this->_helper->viewRenderer->setNoRender(true);

		//Used to convert durations into value for enumerated field
		$duration_enum = array (
			"N/A",	//"N/A" defaults to 1 Hour as base pay
			"1 Hour",
			"2 Hours",
			"3 - 4 Hours",
			"3 - 4 Hours",
			"5 - 7 Hours",
			"5 - 7 Hours",
			"5 - 7 Hours",
			"8 Hours",
			"8 Hours",
			"8 Hours",
			"8 Hours",
			"8 Hours",
			"8 Hours",
			"8 Hours"
		);

		//Matches values from WIN-Tags to tech pay table
		$lead_time_convert = array (
			"N/A" => "N/A",
			"Scheduled" => "Scheduled",
			"Same Business Day" => "SBD",
			"Next Business Day" => "NBD"
		);

		//Match work order category data to tech pay data
		$category_convert = array (
			"N/A" => "N/A",
			"CCTV / Surveillance" => "CCTV Surveillance",
			"Telephony - Non-VoIP" => "Telephony (VoIP or Non-VoIP)",
			"Telephony - VoIP" => "Telephony (VoIP or Non-VoIP)",
			"PC - Desktop / Laptop" => "Desktop/Laptop",  // 14074
			"Routers / Switches" => "Routers/Switches",
			"Apple brand PCs / Laptops" => "Desktop/Laptop", // 14074
			"Central Office Cabling" => "DSL/D-Mark/T1", // 14074
			"Fiber Cabling" => "DSL/D-Mark/T1", // 14074
			"Home Networking" => "Routers/Switches",
			"Adjustable Bed Repair" => "Telephony (VoIP or Non-VoIP)",
			"Electro-Mechanical Binding Eq." => "General",
			"Enterprise Storage" => "Server Hardware",
			"Flat Panel TV" => "Digital Signage",
			"Mobile Devices" => "Desktop/Laptop",     // 14074
			"RFID" => "Point of Sale",
			"Safes/Security Cabinets" => "Digital Signage",
			"Satellite Install" => "Digital Signage",
            "DSL / DMARC / T1" => "DSL/D-Mark/T1" // 14074
            
		);

		//Lead time info
		$lead_time = Core_Api_CommonClass::WOLeadTime (
			$params["win_num"], 
			$params["start_date"], 
			"1", 
			$params["date_entered"], 
			$params["status"],
			true	//Use current date instead of date entered
		);
		$lead_time_full = !empty ($lead_time) ? $lead_time : "N/A";
		$lead_time = $lead_time_convert[$lead_time_full];

		//Day part info
		$start_date = $params["start_date"];
		if (empty ($start_date)) 
			$start_date = date("m/d/Y", strtotime("Next Wednesday"));
		$day_part = Core_Api_CommonClass::WODatePart($start_date, $params["start_time"]);
		$day_part = !empty ($day_part) ? $day_part : "N/A";

		//Geography info
        $select = $db->select();
        $select->from("zipcode_sitetypes");
        $select->where("ZipCode = ?", $params["zipcode"]);
        $geography = Core_Database::fetchAll($select);

		if (!empty ($geography)) {
			$geography = $geography[0];
			$site_type = !empty ($geography["SiteType"]) ? trim ($geography["SiteType"]) : "N/A";
			$geography = trim ($geography["Geography"]);
		}
		else {
			$site_type = "N/A";
		}

		//Category info
        $select = $db->select();
        $select->from("wo_categories");
        $select->where("Category_ID = ?", $params["category_id"]);
        $category = Core_Database::fetchAll($select);

		if (!empty ($category)) {
			$category = $category[0];
			$category = !empty ($category["Category"]) ? trim ($category["Category"]) : "N/A";

			//These categories get a 20% increase
			$multiplier = (
				$category == "Satellite Install" || 
				$category == "Enterprise Storage"
			) ? 1.26 : 1.05;

			$category = isset ($category_convert[$category]) ? $category_convert[$category] : $category;
		}
		else {
			$category = "N/A";
		}

		//Duration info
		if ($params["visits"] > 1) {
			$duration = "Multi-Day";
		}
		else {
			$duration = $duration_enum[intval ($params["duration"])];
		}

		//Tech pay info
        $select = $db->select();
        $select->from("tech_pay");
        $select->where("skill_category LIKE '%$category%'");
        $select->where("duration = ?", $duration);
        $select->where("site_type = ?", $site_type);
        $select->where("geography = ?", $geography);
        $select->where("day_part = ?", $day_part);
        $select->where("lead_time = ?", $lead_time);
        $tech_pay = Core_Database::fetchAll($select);

		if (!empty ($tech_pay)) {
			if ($category == "Copiers" || $category == "CAT5 Cabling" || $category == "Digital Signage") {
				$select = $db->select();
				$select->from("tech_pay");
				switch ($category) {
					case "Copiers":
						$select->where("skill_category = 'Printers / Copiers'");
						break;
					case "CAT5 Cabling":
					case "Digital Signage":
						$select->where("skill_category = 'PC - Desktop / Laptop / Point of Sale / Routers/Switches'");//13675
				}
				$select->where("duration = ?", $duration);
				$select->where("site_type = ?", $site_type);
				$select->where("geography = ?", $geography);
				$select->where("day_part = ?", $day_part);
				$select->where("lead_time = ?", $lead_time);
				$compare_tech_pay = Core_Database::fetchAll($select);
			}

			//Data is needed for each base pay parameter
			$select = $db->select();
			$select->from("tech_pay");
			$select->where("skill_category LIKE '%$category%'");
			$select->where("duration = ?", "1 Hour");
			$select->where("site_type = ?", $site_type);
			$select->where("geography = ?", $geography);
			$select->where("day_part = ?", $day_part);
			$select->where("lead_time = ?", $lead_time);
			$base_duration_pay = Core_Database::fetchAll($select);

			$select = $db->select();
			$select->from("tech_pay");
			$select->where("skill_category LIKE '%$category%'");
			$select->where("duration = ?", $duration);
			$select->where("site_type = ?", "Suburban");
			$select->where("geography = ?", $geography);
			$select->where("day_part = ?", $day_part);
			$select->where("lead_time = ?", $lead_time);
			$base_sitetype_pay = Core_Database::fetchAll($select);

			$select = $db->select();
			$select->from("tech_pay");
			$select->where("skill_category LIKE '%$category%'");
			$select->where("duration = ?", $duration);
			$select->where("site_type = ?", $site_type);
			$select->where("geography = ?", $geography);
			$select->where("day_part = ?", "Day");
			$select->where("lead_time = ?", $lead_time);
			$base_daypart_pay = Core_Database::fetchAll($select);

			$select = $db->select();
			$select->from("tech_pay");
			$select->where("skill_category LIKE '%$category%'");
			$select->where("duration = ?", $duration);
			$select->where("site_type = ?", $site_type);
			$select->where("geography = ?", $geography);
			$select->where("day_part = ?", $day_part);
			$select->where("lead_time = ?", "NBD");
			$base_leadtime_pay = Core_Database::fetchAll($select);

			$tech_pay_result = $tech_pay[0];
			$base_duration_result = $base_duration_pay[0];
			$base_sitetype_result = $base_sitetype_pay[0];
			$base_daypart_result = $base_daypart_pay[0];
			$base_leadtime_result = $base_leadtime_pay[0];

			//Using LIKE in WHERE clause may pull multiple results. Make sure
			//that the correct record is used for tech pay calculations.
			if (count ($base_duration_pay) > 1) {
				foreach ($base_duration_pay as $record) {
					if ($record["skill_category"] == $category) {
						$base_duration_result = $record;
						break;
					}
				}
			}

			if (count ($base_sitetype_pay) > 1) {
				foreach ($base_sitetype_pay as $record) {
					if ($record["skill_category"] == $category) {
						$base_sitetype_result = $record;
						break;
					}
				}
			}

			if (count ($base_daypart_pay) > 1) {
				foreach ($base_daypart_pay as $record) {
					if ($record["skill_category"] == $category) {
						$base_daypart_result = $record;
						break;
					}
				}
			}

			if (count ($base_leadtime_pay) > 1) {
				foreach ($base_leadtime_pay as $record) {
					if ($record["skill_category"] == $category) {
						$base_leadtime_result = $record;
						break;
					}
				}
			}

			if (count ($tech_pay) > 1) {
				foreach ($tech_pay as $record) {
					if ($record["skill_category"] == $category) {
						$tech_pay_result = $record;
						break;
					}
				}
			}

			//See ticket 13922 for the following additional rules
			if (!empty ($base_sitetype_result)) {
				switch ($site_type) {
					case "Urban/Metro":
						$limit_at_most ($tech_pay_result, $base_sitetype_result, 1.0);
						break;
					case "Rural":
						$limit_at_least ($tech_pay_result, $base_sitetype_result, 1.1);
						break;
					case "Remote":
						$limit_at_least ($tech_pay_result, $base_sitetype_result, 1.2);
				}
			}

			if (!empty ($base_daypart_result)) {
				switch ($day_part) {
					case "Evening":
						$limit_at_most ($tech_pay_result, $base_daypart_result, 1.1);
						break;
					case "Night":
						$limit_at_least ($tech_pay_result, $base_daypart_result, 1.15);
						break;
					case "Weekend":
						$limit_at_least ($tech_pay_result, $base_daypart_result, 1.1);
						break;
				}
			}

			if (!empty ($base_duration_result)) {
				switch ($duration) {
					case "2 Hours":
						$limit_at_most ($tech_pay_result, $base_duration_result, 1.0);
						break;
					case "3 - 4 Hours":
					case "5 - 7 Hours":
					case "8 Hours":
						$limit_at_least ($tech_pay_result, $base_duration_result, 0.95);
						break;
					case "Multi-Day":
						$limit_at_least ($tech_pay_result, $base_duration_result, 0.9);
				}
			}

			if (!empty ($base_leadtime_result)) {
				switch ($lead_time) {
					case "SBD":
						$limit_at_least ($tech_pay_result, $base_leadtime_result, 1.15);
						break;
					case "Scheduled":
						$limit_at_least ($tech_pay_result, $base_leadtime_result, 0.95);
						break;
				}
			}

			if ($compare_tech_pay !== false && !empty ($compare_tech_pay)) {
				switch ($category) {
					case "Copiers":
					case "CAT5 Cabling":
						$limit_at_least ($tech_pay_result, $compare_tech_pay[0], 1.20);
						break;
					case "Digital Signage":
						$limit_at_least ($tech_pay_result, $compare_tech_pay[0], 1.15);
				}
			}

			$result["result"] = $multiplier * (float)$tech_pay_result["tech_pay"];
			$result["result_low"] = $multiplier * (float)$tech_pay_result["tech_pay_low"];
			$result["result_high"] = $multiplier * (float)$tech_pay_result["tech_pay_high"];
		}

		//Build response
		$result["lead_time"] = $lead_time_full;
		$result["day_part"] = $day_part;
		$result["geography"] = $site_type;

		$response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $result = json_encode($result);
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($result), true)
                ->setBody($result);
	}

	public function sendOtgMeetingRequestAction () {
        $this->_helper->viewRenderer->setNoRender(true);

		$params = $this->getRequest()->getParams();

		if ($this->user) {
			$result = array ("success" => 1);

			//$search = new Core_Api_Class ();
			//$client = $search->getClientByUsername ($this->view->name);

			$client_id = $this->user->ClientID;
			$name = $this->user->ContactName;
			$phone = $this->user->ContactPhone1;
			$email = $this->user->Email1;
			$company_name = $this->user->CompanyName;
			$company_id = $this->user->Company_ID;
			$is_manager = $this->user->ProjectManager;

			$mgr_info = $this->user->getAccountManagerInfo ($client_id);
			$manager = $mgr_info["name"];

			$body = "The client user listed below has requested a meeting to discuss the Official Tech Pay Guide:\n";
			$body .= "\n";
			$body .= "Name: $name\n";
			$body .= "Phone: $phone\n";
			$body .= "Email: $email\n";
			$body .= "Company Name: $company_name\n";
			$body .= "Company Code: $company_id\n";
			$body .= "Account Manager: $manager\n";

			$mail = new Core_Mail();
			$mail->setBodyText ($body);
			$mail->setFromName ("no-replies@fieldsolutions.com");
			$mail->setFromEmail ("no-replies@fieldsolutions.com");
			$mail->setToEmail ("DataAnalytics@fieldsolutions.com");
			$mail->setSubject ("Official Tech Pay Guide Meeting Request");
			$mail->send ();
		}
		else {
			$result["success"] = 0;
		}

        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $result = json_encode($result);
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($result), true)
                ->setBody($result);
	}
	
	public function p2tTimeRemainingAction() {
        $this->_helper->viewRenderer->setNoRender();
		$request = $this->getRequest();
        $win        = $request->getParam('id', NULL);
		$totalMins = Core_AutoAssign::getTimeRemaining($win);
		$hrs = (int)($totalMins / 60);
		$mins = $totalMins - ($hrs * 60);
        $this->_helper->json(array('totalMins' => $totalMins, 'hrs' => $hrs, 'mins' => $mins));
	}
}

