<?php
class Dashboard_JsController extends Core_Controller_Action
{
    public function init()
    {
		try {
        parent::init();
		} catch (Core_Auth_Exception $e) {
			if ($this->getRequest()->getActionName() != 'state-info') throw $e;
		}
        $this->_helper->viewRenderer->setNoRender();
    }
    public function getTechInfoAction()
    {
        $response = $this->getResponse();
        $response->setHeader('Content-Type', 'text/javascript', true);

        $request = $this->getRequest();
        $tech    = $request->getParam('id', NULL);

        if(empty ($tech) || !is_numeric($tech)){
            $jsonContext = 'techInfo = null;';
        } else {
            $search  = new Core_Api_TechClass;
            $techInfo = $search->techLookup($this->_login, $this->_password, $tech);
            if($techInfo->success) {
                $jsonContext = 'techInfo = '.json_encode((array)$techInfo->data[0]).';';
            } else {
                /* Potentially no auth. Redirect to login page.
                 *
                 * @author Alex Che
                 */
                $jsonContext = 'techInfo = null; document.location = "/techs/";';
            }
        }
        $response->setHeader('Content-Length', strlen($jsonContext), true)
                 ->setBody($jsonContext);
    }
	
	public function getTechFileAction() {
        $id = $this->getRequest()->getParam('id',NULL);
		$db = Core_Database::getInstance();
		$select = $db->select();
		$select->from("techFiles", array("id", "fileType", "filePath", "displayName", "description", "approved"))
			->where("techID = ?", $id);
			
		$fileResult = $db->fetchAll($select);
		
		if(!empty($fileResult)){
			foreach($fileResult as &$file){
				$file['encodedFilename'] = base64_encode(urlencode($file['filePath']));
			}
		}
		
		$this->_helper->json($fileResult);
	}

    public function prepareWoAction()
    {
        require_once 'Core/Api/TechClass.php';

        $response = $this->getResponse();
        $request = $this->getRequest();
        $win = $request->getParam('win', NULL);
        $key = $request->getParam('key', NULL);
        $get = $request->getParam('get', 0) + 0;
        $output = '';
        $search = new Core_Api_TechClass();

        if ( NULL === $win || NULL === $key ) {
            if ( $get ) {
                $output = '{}';
            } else {
                $output = 'var _prepared = false;';
            }
        } else {
            $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');

            if ( FALSE === ($wo = $cache->load($key)) ) {
                $paths = $this->getFrontController()->getControllerDirectory();
                if ( !empty($paths['dashboard']) && file_exists($paths['dashboard'].'/IndexController.php') ) {
                    require_once $paths['dashboard'].'/IndexController.php';
                    $fields = Dashboard_IndexController::FIELDS;
                } else {
                    $fields = NULL;
                }

                $wo = $search->getWorkOrder($this->_login, $this->_password, $win, null);
                $cache->save($wo, $key, array(), 120);
            }
            if ( FALSE === ($tech = $cache->load($key.'_tech')) ) {
                $tech = new API_Tech();
                $id = ( !empty($_SESSION['FLSID']) ) ? $_SESSION['FLSID'] : ( ( !empty($_SESSION['TechID']) ) ? $_SESSION['TechID'] : NULL );
                if ( $id ) {
                    $tech->lookupID($id);
                    $cache->save($tech, $key.'_tech', array(), 120);
                }
            }

            if ( FALSE === ($project = $cache->load($key.'_project')) ) {
                if($wo->success) {
                    $prj_id = $wo->data[0]->Project_ID;
                    if ($prj_id){
                        $project = $search->getProjects($this->_login,$this->_password,array($prj_id),'');
                        $cache->save($project, $key.'_project', array(), 120);
                    }
                }
            }

            if ( $get ) {
                if ( $wo->success ) {
                    $wo->data[0]->Status = ucwords(strtolower($wo->data[0]->Status));
                    $output = json_encode((array)$wo->data[0]);
                } else {
                    $output = '{}';
                }
            } else {
                $output = 'var _prepared = true;';
            }
        }

        $response
            ->clearAllHeaders()
            ->clearBody();
        $response
            ->setHeader('Content-Type', 'text/javascript')
            ->setHeader('Content-Length', strlen($output))
            ->setBody($output);
    }

    public function stateInfoAction()
    {
		$country = $this->getRequest()->getParam('country',NULL);
		$common = new Core_Api_CommonClass;
		$return = $common->getStatesArray($country);
		if ($return->success)
			$return = $return->data;
		else
			$return = array();
		
        $response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                 ->setBody($return);
	}
	
    public function commInfoAction()
    {
        $username = $this->getRequest()->getParam('username',NULL);
        $pid = $this->getRequest()->getParam('pid',NULL);
        $company = $this->getRequest()->getParam('company',NULL);
		$search = new Core_Api_ProjectClass;
		$projectInfo = $projectInfo->data[0];
		$projectInfo = $search->getProjects($this->_login.$pmContext,$this->_password,array((int)$pid),null);
		$projectInfo = $projectInfo->data[0];
		if ($projectInfo->AllowCustomCommSettings == 0) $this->_helper->json(null);
		$future = new Core_FutureWorkOrderInfo;
		$row = $future->find($username)->toArray();
		$this->_helper->json($row[0]);
	}
	
    public function projectInfoAction()
    {
        $id = $this->getRequest()->getParam('id',NULL);
        $username = $this->getRequest()->getParam('username',NULL);
        $company = $this->getRequest()->getParam('company',NULL);
				
		if ($id) {
			$pmContext = '|pmContext='.$company;
			if ($this->_loggedInAs == 'customers') {
				$search = new Core_Api_CustomerClass;
				$pmContext = '';
		    }
		    else
	            $search = new Core_Api_ProjectClass;
			$projectInfo = $search->getProjects($this->_login.$pmContext,$this->_password,array((int)$id),null,$username);
			if ($projectInfo->success) {
				$projectInfo = $projectInfo->data[0];
			}else{
				$projectInfo = null;
			}			
        }else{
            $projectInfo = null;
        }

        //filter Project Info
        $return = array();
		$rename_array = array(
            'Project_ID' => 'Project_ID',
            'Project_Name' => 'Project_Name',
            'Project_Manager' => 'ProjectManagerName',
            'Project_Company_ID' => 'Project_Company_ID', /*13329*/            
            'Project_Manager_Email' => 'ProjectManagerEmail',
            'Project_Manager_Phone' => 'ProjectManagerPhone',
            'Resource_Coordinator' => 'ResourceCoordinatorName',
            'Resource_Coord_Email' => 'ResourceCoordinatorEmail',
            'Resource_Coord_Phone' => 'ResourceCoordinatorPhone',
            'Qty_Devices'=>'Qty_Devices',
            'Amount_Per'=>'Amount_Per',
            'Default_Amount'=>'PayMax',
            'Description'=>'Description',
            'Requirements'=>'Requirements',
            'SpecialInstructions'=>'SpecialInstructions',
            'SpecialInstructions_AssignedTech'=>'Ship_Contact_Info',
            'AutoBlastOnPublish'=>'AutoBlastOnPublish',
            'RecruitmentFromEmail'=>'RecruitmentFromEmail',
            'AutoSMSOnPublish'=>'AutoSMSOnPublish',
            'WO_Category_ID'=>'WO_Category_ID',
            'PO'=>'PO',
            'General1'=>'General1',
        	'General2'=>'General2',
            'General3'=>'General3',
            'General4'=>'General4',
            'General5'=>'General5',
            'General6'=>'General6',
            'General7'=>'General7',
            'General8'=>'General8',
            'General9'=>'General9',
            'General10'=>'General10',
            'Headline'=>'Headline',
            'CheckInOutEmail'=>'CheckInOutEmail',
            'CheckInOutName'=>'CheckInOutName',
            'CheckInOutNumber'=>'CheckInOutNumber',
            'EmergencyName'=>'EmergencyName',
            'EmergencyPhone'=>'EmergencyPhone',
            'EmergencyEmail'=>'EmergencyEmail',
            'TechnicalSupportName'=>'TechnicalSupportName',
            'TechnicalSupportPhone'=>'TechnicalSupportPhone',
            'TechnicalSupportEmail'=>'TechnicalSupportEmail',
            'Push2Tech'=>'Push2Tech',
            'Reminder24Hr'=>'Reminder24Hr',
            'Reminder1Hr'=>'Reminder1Hr',
            'ReminderCustomHrChecked'=>'ReminderCustomHrChecked',//13483
            'ReminderCustomHr'=>'ReminderCustomHr',            
            'ReminderCustomHrChecked_2'=>'ReminderCustomHrChecked_2',//13483
            'ReminderCustomHr_2'=>'ReminderCustomHr_2',            
            'ReminderCustomHrChecked_3'=>'ReminderCustomHrChecked_3',//13483
            'ReminderCustomHr_3'=>'ReminderCustomHr_3',            
            'ReminderAcceptance'=>'ReminderAcceptance',
            'ReminderNotMarkComplete'=>'ReminderNotMarkComplete',
            'P2TPreferredOnly'=>'P2TPreferredOnly',
            'isWorkOrdersFirstBidder'=>'isWorkOrdersFirstBidder',
            'MaximumAllowableDistance'=>'MaximumAllowableDistance',
            'MinimumSelfRating'=>'MinimumSelfRating',
            'ReminderIncomplete'=>'ReminderIncomplete',
            'CheckOutCall'=>'CheckOutCall',
            'CheckInCall'=>'CheckInCall',
            'SignOffSheet_Required'=>'SignOffSheet_Required',
            'P2TNotificationEmail'=>'P2TNotificationEmail',
            'NotifyOnBid'=>'NotifyOnBid',
            'NotifyOnAssign'=>'NotifyOnAssign',
            'AssignmentNotificationEmail'=>'AssignmentNotificationEmail',
            'NotifyOnAutoAssign'=>'NotifyOnAutoAssign',
            'FixedBid'=>'FixedBid',
            'PcntDeduct'=>'PcntDeduct',
            'MinutesRemainPublished'=>'MinutesRemainPublished',
            'StartDate'=>'StartDate',
            'StartTime'=>'StartTime',
            'EndDate'=>'EndDate',
            'EndTime'=>'EndTime',
            'StartRange'=>'StartRange',
            'Duration'=>'Duration',
            'NotifyOnWorkDone'=>'NotifyOnWorkDone',
            'WorkDoneNotificationEmails'=>'WorkDoneNotificationEmails',
            'BidNotificationEmails'=>'BidNotificationEmails',
            'To_Email'=>'To_Email',
            'PartManagerUpdate_Required'=>'PartManagerUpdate_Required',
            'isProjectAutoAssign'=>'isProjectAutoAssign',
            'PcntDeductPercent'=>'PcntDeductPercent', 
			'CredentialCertificationId'=>'CredentialCertificationId',
			'ClientCredential' => 'ClientCredential',
            'EmailBlastRadius'=>'EmailBlastRadius', 
            'EmailBlastRadiusPreferredOnly'=>'EmailBlastRadiusPreferredOnly',
            'ReceiveScheduleConflictNotificationEmails'=>'ReceiveScheduleConflictNotificationEmails',
            'ReceiveScheduleConflictNotification'=>'ReceiveScheduleConflictNotification',
            'ReceiveReVisitNotification'=>'ReceiveReVisitNotification',
            'ReceiveReVisitNotificationEmails'=>'ReceiveReVisitNotificationEmails',
        	'CustomSignOff' => 'CustomSignOff',
        	'SignOff_Disabled' => 'SignOff_Disabled',
        	'CustomSignOff_Enabled' => 'CustomSignOff_Enabled',
            'StartType' => 'StartType',
            'WorkStartTime' => 'WorkStartTime',
            'WorkEndTime' => 'WorkEndTime',
            'EstimatedDuration' => 'EstimatedDuration',
            'TechArrivalInstructions' => 'TechArrivalInstructions',
            'SpecialInstructions_AssignedTech' => 'SpecialInstructions_AssignedTech',
            'Type_ID' => 'Type_ID',
            'Qty_Visits' => 'Qty_Visits',
            "AllowCustomCommSettings" => "AllowCustomCommSettings",
            "ReceiveTechQAMessageNotificationEmails" => "ReceiveTechQAMessageNotificationEmails",
            "ReceiveTechQAMessageNotification" => "ReceiveTechQAMessageNotification",
            "AllowDeVryInternstoObserveWork" => "AllowDeVryInternstoObserveWork",
            "AllowDeVryInternstoPerformField" => "AllowDeVryInternstoPerformField",
            'ServiceTypeId' => 'ServiceTypeId',
            'EntityTypeId' => 'EntityTypeId',
			'ACSNotifiPOC' => 'ACSNotifiPOC',//14052
			'ACSNotifiPOC_Phone' => 'ACSNotifiPOC_Phone',
			'ACSNotifiPOC_Email' => 'ACSNotifiPOC_Email',
			//13763
            "WorkAcceptedByTechEmailTo" => "WorkAcceptedByTechEmailTo",
			"ReceiveWorkAcceptedByTechEmail" => "ReceiveWorkAcceptedByTechEmail",
            "WorkConfirmedByTechEmailTo" => "WorkConfirmedByTechEmailTo",
			"ReceiveWorkConfirmedByTechEmail" => "ReceiveWorkConfirmedByTechEmail",
			"TechCheckedInEmailTo" => "TechCheckedInEmailTo",
			"ReceiveTechCheckedInEmail" => "ReceiveTechCheckedInEmail",
			"TechCheckedOutEmailTo" => "TechCheckedOutEmailTo",
			"ReceiveTechCheckedOutEmail" => "ReceiveTechCheckedOutEmail",
            //end 13763			
            "SiteContactReminderCall" => "SiteContactReminderCall",
            "SiteContactReminderCallHours" => "SiteContactReminderCallHours",
			"PushWM" => "PushWM"
        );

        if ($projectInfo) {
            foreach ($rename_array as $key => $value) {
                if(($key == "StartDate" 
                        || $key == "StartDate"
                        || $key == "EndTime"
                        || $key == "EndDate")
                        && $this->getRequest()->getParam('caller',"create") == "index"){
                    continue;
                }
                $return[$value] = $projectInfo->$key;
                if ($key == 'BidNotificationEmails') {
                    if (empty($projectInfo->$key)) {
                        $return["To_Email"] = $projectInfo->To_Email;
            		}
        		}
            }
        }
//        var_dump($projectInfo);exit;
        if ((bool)$projectInfo->ReminderAll) {
            $return['ReminderAcceptance'] = '1';
            $return['Reminder24Hr'] = '1';
            $return['Reminder1Hr'] = '1';
            $return['ReminderCustomHrChecked'] = '0';//13483
            $return['ReminderNotMarkComplete'] = '1';
            $return['ReminderIncomplete'] = '1';
            $return['CheckOutCall'] = '1';
            $return['CheckInCall'] = '1';
        }
        if(!empty($return['StartDate']))
            $return['StartDate'] = date_format(new DateTime($return['StartDate']), 'm/d/Y');
        if(!empty($return['EndDate']))
            $return['EndDate'] = date_format(new DateTime($return['EndDate']), 'm/d/Y');

        if (!empty($return["CustomSignOff"])) {
        	$return["CustomSignOffName"] = ltrim (strstr ($return["CustomSignOff"], "/"), "/");
        	if (strstr ($return["CustomSignOff"], "/", true) == "projects-docs")
        		$return["CustomSignOffLink"] = "/widgets/dashboard/do/file-project-documents-download/filename/" . base64_encode(urlencode($return["CustomSignOffName"])) . "/" . $return["CustomSignOffName"];
        	else
        		$return["CustomSignOffLink"] = "/widgets/dashboard/do/file-wos-documents-download/filename/" . base64_encode(urlencode($return["CustomSignOffName"])) . "/" . $return["CustomSignOffName"];
        }

        $siteCount = Core_Site::getSiteCount($company, (int)$id);
		$return['SiteCount'] = empty($siteCount[$id]) ? 0 : $siteCount[$id];
        /*if ($return['Tools']) $return['Requirements'] .= "\n" . $return['Tools']; //By CR #12563
        unset($return['Tools']);*/
        // TechsInSite
        $siteNumber = $this->getRequest()->getParam('siteNumber',0);
        $Core_Api_WosClass = new Core_Api_WosClass;
        $return['TechsInSite'] = $Core_Api_WosClass->Exists_TechsInSite($siteNumber,$return['Project_ID']);

        // for CredentialCertificationId
        $Core_ProjectCertification = new Core_ProjectCertification($return['Project_ID'],"");
        $CredentialCertification = $Core_ProjectCertification->getRowByProjectId();
        
        if(!empty($CredentialCertification)){
            $return['CredentialCertificationId'] = $CredentialCertification[0]["certification_id"];
        }else{
            $return['CredentialCertificationId'] = "";
        }

        //Following fields should contain HTML for Tiny MCE fields.  If they do
        //not, they contain old data and require formatting.
        $return["Description"] = $this->convertToHtml ($return["Description"]);
		$return["Requirements"] = $this->convertToHtml ($return["Requirements"]);
		$return["SpecialInstructions"] = $this->convertToHtml ($return["SpecialInstructions"]);
		$return["SpecialInstructions_AssignedTech"] = $this->convertToHtml ($return["SpecialInstructions_AssignedTech"]);
         // tag
         $Core_Api_WoTagClass = new Core_Api_WoTagClass;       
         $ServiceTypeInfo = $Core_Api_WoTagClass->getTagItemInfo($return["ServiceTypeId"]);
         $return["ServiceTypeValue"] = $ServiceTypeInfo["wo_tag_item_name"];
         $return["EntityTypeId2"] = $return["EntityTypeId"];
		
		$Core_Api_User = new Core_Api_User;

        $clientInfo = $Core_Api_User->loadWithExt(
            array(
                'login' => $this->_login . '|pmContext=' . $company,
                'password' => $this->_password
            )
        );
			
		$mySettingsClass = new Core_FutureWorkOrderInfo;
		$ProjectClass = new Core_Api_ProjectClass;
        $projectinfo = $ProjectClass->getProjectById($wo->Project_ID);
        $mySettingsRow = $mySettingsClass->find($this->_login)->toArray();
		$mySettingsInfo = $mySettingsRow[0];
		if($projectinfo->AllowCustomCommSettings == "1" && ($mySettingsInfo['TechQAMessageEmailTo'] != "" && $mySettingsInfo['TechQAMessageEmailTo'] != null)){
			$return['ReceiveTechQAMessageNotificationEmails'] = $mySettingsInfo['TechQAMessageEmailTo'];
		}elseif($return['ReceiveTechQAMessageNotification'] == "1" && ( empty($return['ReceiveTechQAMessageNotificationEmails']) || $return['ReceiveTechQAMessageNotificationEmails'] == "") ){
			$return['ReceiveTechQAMessageNotificationEmails'] = $clientInfo->Email1;
		}
		
		$return["RequiredCerts"] = $ProjectClass->loadP2TCertRequirements ($id);

		$response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $return = json_encode($return);
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($return), true)
                 ->setBody($return);
    }

    public function companyProjectsAction()
    {
        require_once 'Core/Cache/Manager.php';
        require_once 'Core/Api/Class.php';

        $request    = $this->getRequest();
        $response   = $this->getResponse();

        $company        = $request->getParam('company', NULL);
        $projectsState  = $request->getParam('pstate', 'all');

        $key    = Core_Cache_Manager::makeKey('all_projects', $company);
        $cache  = $this->getInvokeArg('bootstrap')->getResource('Cache');
        $search = new Core_Api_Class;

        if ( FALSE === ($projects = $cache->load($key)) ) {
            $projects = $search->getAllProjects($this->_login.'|pmContext='.$company, $this->_password);
            if ( $projects->success ) {
                $cache->save($projects, $key, array('projects'), LIFETIME_15MIN);
            }
        }

        $output = 'var projects = ';
        if ( $projects->success ) {

            if ( !empty($projects->data) ) {
                usort($projects->data, 'Core_Controller_Action::_cmpProjects');
            }

            $obj = array();
            switch ( strtolower(trim($projectsState)) ) {
                case 'all':
                    foreach ( $projects->data as &$project )
                        $obj[] = array('id' => 0 + $project->Project_ID, 'name' => $project->Project_Name);
                    break;
                case 'active':
                    foreach ( $projects->data as &$project )
                        if ( $project->Active )
                            $obj[] = array('id' => 0 + $project->Project_ID, 'name' => $project->Project_Name);
                    break;
                case 'notactive':
                    foreach ( $projects->data as &$project )
                        if ( !$project->Active )
                            $obj[] = array('id' => 0 + $project->Project_ID, 'name' => $project->Project_Name);
                    break;
            }

            if ( $obj ) {
                $output .= json_encode($obj);
            } else {
                $output .= '[];';
            }
        } else {
            $output .= '[];';
        }

        $response
            ->clearAllHeaders()
            ->clearBody();
        $response
            ->setHeader('Content-Type', 'text/javascript')
            ->setHeader('Content-Length', strlen($output))
            ->setBody($output);
    }

    public function statesAction()
    {
        require_once 'Core/Cache/Manager.php';
        require_once 'Core/Api/Class.php';

        $request    = $this->getRequest();
        $response   = $this->getResponse();

        $search     = new Core_Api_Class;

        $output = 'var states = ';
        $result = $search->getStates();
        if ( $result->success ) {
            $obj = array();
            foreach ( $result->data as $id => $name )
                $obj[] = array('id' => $id, 'name' => $name);

            if ( $obj ) {
                $output .= json_encode($obj);
            } else {
                $output .= '[];';
            }
        } else {
            $output .= '[];';
        }

        $response
            ->clearAllHeaders()
            ->clearBody();
        $response
            ->setHeader('Content-Type', 'text/javascript')
            ->setHeader('Content-Length', strlen($output))
            ->setBody($output);
    }

    public function categoriesAction()
    {
        require_once 'Core/Cache/Manager.php';
        require_once 'Core/Api/Class.php';

        $request    = $this->getRequest();
        $response   = $this->getResponse();

        $search     = new Core_Api_Class;

        $output = 'var categories = ';
        $result = $search->getWOCategories($this->_login, $this->_password);

        if ( $result->success ) {
            $obj = array();
            foreach ( $result->data as $cat )
                $obj[] = array('id' => $cat->Category_ID, 'name' => $cat->Category);
            if ( $obj ) {
                $output .= json_encode($obj);
            } else {
                $output .= '[];';
            }
        } else {
            $output .= '[];';
        }

        $response
            ->clearAllHeaders()
            ->clearBody();
        $response
            ->setHeader('Content-Type', 'text/javascript')
            ->setHeader('Content-Length', strlen($output))
            ->setBody($output);
    }
    public function checkApproveAction() {
            $wo_ids = $this->getRequest()->getParam('wo_ids',array());
            $Core_Api_WosClass = new Core_Api_WosClass;  
            $fileResult = $Core_Api_WosClass->getWins_TechClaimNotApproved($wo_ids);
            $this->_helper->json(array("win"=>$fileResult));
	}

	private function convertToHtml ($value) {
		if(!preg_match ('#(?<=<)\w+(?=[^<]*?>)#', $value)) {
			$lines = explode ("\n", $value);
			$value = "<p>" . implode ("</p><p>", array_filter ($lines)) . "</p>";
		}

		return $value;
	}
}

