<?php

class Dashboard_ClientController extends Core_Controller_Action {
	
	const FIELDS = 'WIN_NUM,WO_ID,DateEntered,PO,ShowTechs,SourceByDate,ShortNotice,StartDate,StartTime,EndDate,EndTime,StartRange,Duration,Region,Route,Type_ID,Project_ID,WO_Category_ID,Headline,Site_Contact_Name,SiteName,SiteNumber,Project_Name,Address,City,State,Zipcode,SitePhone,SiteFax,SiteEmail,Update_Requested,Update_Reason,Description,Requirements,Tools,SpecialInstructions,Lead,Assist,SignOffSheet_Required,ProjectManagerName,ProjectManagerPhone,ProjectManagerEmail,ResourceCoordinatorName,ResourceCoordinatorPhone,ResourceCoordinatorEmail,EmergencyName,EmergencyPhone,EmergencyEmail,TechnicalSupportName,TechnicalSupportPhone,TechnicalSupportEmail,CheckInOutName,CheckInOutNumber,CheckInOutEmail,Tech_ID,Tech_FName,Tech_LName,NoShow_Tech,BackOut_Tech,FLS_ID,BU1FLSID,BU1Info,Backup_FLS_ID,Ship_Contact_Info,PayMax,Amount_Per,Qty_Devices,Tech_Bid_Amount,WorkOrderReviewed,TechCheckedIn_24hrs,CheckedIn,Checkedby,PreCall_1,PreCall_2,CheckInNotes,StoreNotified,Notifiedby,DateNotified,NotifyNotes,TechMarkedComplete,Date_In,Time_In,Date_Out,Time_Out,ContactHD,HDPOC,TechMiles,TechComments,AttachedFile,FLS_OOS,Unexpected_Steps,Unexpected_Desc,AskedBy,AskedBy_Name,Paperwork_Received,Incomplete_Paperwork,MissingComments,Site_Complete,CallClosed,PartsProcessed,ClientComments,Work_Out_of_Scope,OutofScope_Amount,OutofScope_Reason,TripCharge,MileageReimbursement,MaterialsReimbursement,Additional_Pay_Amount,Penalty_Reason,AbortFee,AbortFeeAmount,Add_Pay_AuthBy,Add_Pay_Reason,calculatedTechHrs,baseTechPay,PayAmount,Approved,TechPaid,DatePaid,Deactivated,DeactivationCode,Deactivated_Reason,General1,General2,General3,General4,General5,General6,General7,General8,General9,General10,FS_InternalComments,Username,Invoiced,Company_Name,Company_ID,TechEmail,TechPhone,TechPhoneExt,Date_Assigned,lastModBy,lastModDate,approvedBy,DateApproved,DeactivatedDate,DeactivatedBy,Latitude,Longitude,PricingRan,WO_Category,DateIncomplete,ReminderAcceptance,Reminder24Hr,Reminder1Hr,CheckInCall,CheckOutCall,ReminderNotMarkComplete,ReminderIncomplete,ReminderAll,SMSBlast,MinutesRemainPublished,MinimumSelfRating,MaximumAllowableDistance,isProjectAutoAssign,isWorkOrdersFirstBidder,P2TPreferredOnly,P2TAlreadyAssigned,SATRecommended,SATPerformance,ReturnPartCount,NewPartCount,Qty_Applicants,Status,DateInvoiced,Work_Out_of_Scope,OutOfScope_Reason,Status,PcntDeduct,PcntDeductPercent,Date_Completed';
	

    private $_restrictedValue = 2;
    private $_openedValue = 1;

    public function init() {
        parent::init();

        $request = $this->getRequest();
        $auth = new Zend_Session_Namespace('Auth_User');
        $this->view->name = $auth->name;
        $this->view->company = $auth->companyId;
        $user = new Core_Api_User();
        if (!empty($_SESSION['PM_Company_ID'])) {
            $clients = $user->getRepClientforCompany($_SESSION['PM_Company_ID']);
            $client_id = $clients[0]['ClientID'];
            $client_login = $clients[0]['UserName'];
            $client_password = $clients[0]['Password'];
            $pmContext = '|pmContext=' . $_SESSION['PM_Company_ID'];
            $this->_ClientID = $client_id;
            $this->_companyId = $_SESSION['PM_Company_ID'];
            $this->_companyname = $clients[0]['CompanyName'];
            $this->_emailclient = $clients[0]['Email1'];
            $this->_ContactName = $clients[0]['ContactName'];
        } else {
            $client_login = $this->_login;
            $client_password = $this->_password;
            $clientInfo = $user->loadWithExt(
                    array(
                        'login' => $client_login,
                        'password' => $client_password
                    )
            );
            $this->_ClientID = $clientInfo->ClientID;
            if (!empty($clientInfo)) {
                $this->_companyId = $clientInfo->Company_ID;
                $this->_companyname = $clientInfo->CompanyName;
                $this->_emailclient = $clientInfo->Email1;

    }
        }
    }

     public function getTechProfileStatsAction(){
    	$request = $this->getRequest();
    	$params = $request->getParams();

    	$fullResult = Core_TechStats::getLogbookStats($params['id'], $params['interval']);
    	//$stats = $fullResult[$params['id']]->toArray();
    
    	$response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $return = json_encode($fullResult);

        echo $return;
        $this->_helper->viewRenderer->setNoRender(true);
    }
    
	public function getTechLogbookInfoAction(){
	
	    $request    = $this->getRequest();
	    $params  = $request->getParams();
		
	    $this->view->companyId  = $this->_companyId;
		$authData = array('login'=>$this->_login . '|pmContext=' . $this->_companyId, 'password'=>$this->_password);

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);
		
		if (!$user->isAuthenticate()) return false;
		
		$fields = 'StartDate, Headline, Tech_Bid_Amount, Amount_Per, WO_Category_ID, Tech_ID, Description, SATPerformance';
		$blank = "";
		$page = isset ($params["offset"]) ? intval ($params["offset"]) : 0;
		$numRecords = isset ($params["limit"]) ? intval ($params["limit"]) : "";
		($params["sort"] != "" && $params["sort"] != "woCategoryLabel") ? $sort = $params['sort']." ".$params['sortDirection'] : $sort = "LogbookStartDate DESC";
		$filter = new API_WorkOrderFilter;
		if($params["id"] != "") $filter->Tech_ID = $params['id'];
		$params['Company_ID'] != "" ? $filter->Company_ID = $params['Company_ID'] : $filter->withOutCompanyID = true;
                $filter->Deactivated = 0;
		$countRows = "";
		$search = new Core_Api_Class();

		$woResult =  $search->getWorkOrders(
            $authData['login'],
            $authData['password'],
            $fields,
            $blank,
            $blank,
            $blank,
            $filter,
            $page,
            $numRecords,
            $countRows,
            $sort
        );
        
        $db = Core_Database::getInstance();
        $select = $db->select();
        $select->from(array("wo_categories"), array("Category_ID", "Category"));
		$searchResult = $db->fetchAll($select);
		
		$categoryArr = array();
		foreach($searchResult as $s){
			$categoryArr[$s['Category_ID']] = $s['Category'];
		}
		
        
		$returnArr = array();
        foreach($woResult->data as $data){
        	$d = get_object_vars($data);
        	$d["woCategoryLabel"] = $categoryArr[$d['WO_Category_ID']];
        	!empty($d["StartDate"]) ? $d["StartDate"] = date("m/d/Y",strtotime($d["StartDate"])) : $d["StartDate"] = "";
        	$returnArr[] = $d;
        }
        
        if($params["sort"] == "woCategoryLabel"){
        	foreach($returnArr as $k=>$r){
        		$woCatLabel[$k] = $r['woCategoryLabel'];
        	}
        	$array_lowercase = array_map('strtolower', $woCatLabel);
        	$params['sortDirection'] == 'DESC' 
        		? array_multisort($array_lowercase, SORT_DESC,SORT_STRING,$returnArr) 
        		: array_multisort($array_lowercase, SORT_ASC,SORT_STRING,$returnArr);
        }
    
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $return = json_encode($returnArr);

        echo $return;
        $this->_helper->viewRenderer->setNoRender(true);
		
	}

    public function updateAction() {
        $request = $this->getRequest();
        $mode = $request->getParam('mode') == "edit" ? true : false;

        $user = new Core_Api_User();
		$pmContext = "";
        
        //echo("_SESSION['ClientID']: ");echo($_SESSION['ClientID']);
        //echo(" <br/><pre>");print_r($_SESSION);die();echo("</pre>");
        
        //13334
        $client_login_old = $this->_login;
		if (!empty($_SESSION['PM_Company_ID'])) {
			$clients = $user->getRepClientforCompany($_SESSION['PM_Company_ID']);
			$client_id = $clients[0]['ClientID'];
			$client_login = $clients[0]['UserName'];
			$client_password = $clients[0]['Password'];
			$pmContext = '|pmContext=' . $_SESSION['PM_Company_ID'];
		}
		else {
			$client_login = $this->_login;
			$client_password = $this->_password;
		}
        //13970
        $authData = array('login' => $client_login,'password' => $client_password);
        $clientInfo = $user->loadWithExt($authData,0);            
        //echo("<pre>clientInfo-1: ");print_r($clientInfo);echo("</pre>");die();
        //end 13970

		$client_id = $clientInfo->ClientID;
		$clientCountry = $clientInfo->Country;
 
        $this->view->PcntDeductPercent =$user->GetDeductPercent_FSSFeeFrTech($clientInfo->Company_ID);
		$apiCommon = new Core_Api_CommonClass();   
        $clientCountryCode = $apiCommon->getCountryCode($clientCountry);   
        $states = $apiCommon->getStatesArray($clientCountryCode);   
		$client = new Core_Api_Class();
        $isPM = $client->isGPMByUserName($client_login_old);
        $this->view->isGPM = $isPM;
        $this->view->countries = $client->getCountries($this->_login . $pmContext, $this->_password);
        $this->view->states = $states;
        $this->view->client = $clientInfo;
        $this->view->success = $clientInfo !== false;
        $this->view->mode = $mode;
        
         $WoTagClass = new Core_Api_WoTagClass(); 
         

        $this->view->ClientTagValues = $WoTagClass->getClientTagValues($client_id);
        
        $search = new Core_Api_AdminClass;

        $filelist = $user->getFileList ($clientInfo->Company_ID, Core_Files::SIGNOFF_FILE);
        if (!empty ($filelist))
	        $this->view->customSignoff = ltrim(strstr ($filelist[0]["path"], '/'), '/');
		else
			$this->view->customSignoff = false;

		$this->view->customSignOffEnabled = !!$this->view->customSignoff && !!$clientInfo->CustomSignOff_Enabled;

        $errors = Core_Api_Error::getInstance();

        $this->view->messages = $error->message;
        // $user = new Core_Api_User();
        $clientExt = $user->getClientExt($client_id);
        $_clientExt = array();
        if (!empty($clientExt)) {
            $_clientExt["ClientID"] = $clientInfo->ClientID;
            $_clientExt["FSClientServiceDirectorId"] = $clientExt[0]["FSClientServiceDirectorId"];
            $_clientExt["FSAccountManagerId"] = $clientExt[0]["FSAccountManagerId"];
            $_clientExt["File1URL"] = $clientExt[0]["File1URL"];
            $_clientExt["File2URL"] = $clientExt[0]["File2URL"];
            $_clientExt["File3URL"] = $clientExt[0]["File3URL"];
            $_clientExt["Upload1Date"] = $clientExt[0]["Upload1Date"];
            $_clientExt["Upload2Date"] = $clientExt[0]["Upload2Date"];
            $_clientExt["Upload3Date"] = $clientExt[0]["Upload3Date"];
            $_clientExt["DeniedTech_NoShowGreater"] = $clientInfo->DeniedTech_NoShowGreater;
            $_clientExt["DeniedTech_NoShowPeriod"] = $clientInfo->DeniedTech_NoShowPeriod;
            $_clientExt["DeniedTech_BackOutGreater"] = $clientInfo->DeniedTech_BackOutGreater;
            $_clientExt["DeniedTech_BackOutPeriod"] = $clientInfo->DeniedTech_BackOutPeriod;
            $_clientExt["DeniedTech_PreferenceRatingSmaller"] = $clientInfo->DeniedTech_PreferenceRatingSmaller;
            $_clientExt["DeniedTech_PreferenceRatingPeriod"] = $clientInfo->DeniedTech_PreferenceRatingPeriod;
            $_clientExt["DeniedTech_PerformanceRatingSmaller"] = $clientInfo->DeniedTech_PerformanceRatingSmaller;
            $_clientExt["DeniedTech_PerformanceRatingPeriod"] = $clientInfo->DeniedTech_PerformanceRatingPeriod;
        } else {
            $_clientExt["FSClientServiceDirectorId"] = 0;
            $_clientExt["FSAccountManagerId"] = 0;
            $_clientExt["File1URL"] = "";
            $_clientExt["File2URL"] = "";
            $_clientExt["File3URL"] = "";
            $_clientExt["Upload1Date"] = "";
            $_clientExt["Upload2Date"] = "";
            $_clientExt["Upload3Date"] = "";
            $_clientExt["DeniedTech_NoShowGreater"] = 0;
            $_clientExt["DeniedTech_NoShowPeriod"] = "";

            $_clientExt["DeniedTech_BackOutGreater"] = 0;
            $_clientExt["DeniedTech_BackOutPeriod"] = "";
            $_clientExt["DeniedTech_PreferenceRatingSmaller"] = 0;
            $_clientExt["DeniedTech_PreferenceRatingPeriod"] = "";
            $_clientExt["DeniedTech_PerformanceRatingSmaller"] = 0;
            $_clientExt["DeniedTech_PerformanceRatingPeriod"] = "";
            
        }
        $this->view->clientExt = $_clientExt;
        // for ext user
	}

    public function doUpdateAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $return = array('success' => 0);
        $needUpdate = true;

        $user = new Core_Api_User();
		$pmContext = "";
		if (!empty($_SESSION['PM_Company_ID'])) {
			$clients = $user->getRepClientforCompany($_SESSION['PM_Company_ID']);
			$client_id = $clients[0]['ClientID'];
			$client_login = $clients[0]['UserName'];
			$client_password = $clients[0]['Password'];
			$pmContext = '|pmContext=' . $_SESSION['PM_Company_ID'];
		} else {
			$client_login = $this->_login;
			$client_password = $this->_password;
		}
        //13970

        $authData = array(
                    'login' => $client_login . $pmContext,
                    'password' => $client_password
                );
        $clientInfo = $user->loadWithExt($authData,false);            
        //end 13970

		$client_id = $clientInfo->ClientID;

        $updateForm = $this->getRequest()->getParam('UpdateForm', null);

        $params = $this->getRequest()->getParams();
        $formFields = array();
        foreach ($params as $key => $value) {
            if (strstr($key, 'EditRecord') !== null) {
                $formFields[str_replace('EditRecord', '', $key)] = $value;
            }
        }

        if (!empty($formFields['PasswordCurrent'])) {
            //13970
            $authData = array('login' => $client_login,
                        'password' => $formFields['PasswordCurrent']);
            $clientInfo = $user->loadWithExt($authData,false);
            //end 13970
            $cid = $client_id;
            if (empty($cid)) {
                // wrong password confirmation
                $return['success'] = 0;
                $needUpdate = false;
                $return['errors'][] = 'Wrong Password';
            }

            $errorMessages = new Core_Form_ErrorHandler('updateClient_' . $cid);
            $errorMessages->clearMessages();


            if (empty($formFields['Password']) || $formFields['Password'] != $formFields['PasswordConfirm']) {
                $return['success'] = 0;
                $needUpdate = false;
                if (empty($return['errors']))
                    $return['errors'] = array();
                $return['errors'][] = 'Incorrect Password Confirmation';
            }
        }

        if (empty($formFields['PasswordCurrent'])
                || empty($formFields['Password'])
                || empty($formFields['PasswordConfirm'])
                || $formFields['Password'] != $formFields['PasswordConfirm']
        ) {
            // dont update password fields
            unset($formFields['PasswordCurrent']);
            unset($formFields['Password']);
            unset($formFields['PasswordConfirm']);
        }

        $allowedToUpdate = array(
            'CompanyName',
            'EIN',
            'CompanyDescription',
            'ContactName',
            'BusinessAddress',
            'BusinessCity',
            'BusinessState',
            'BusinessZip',
            'BusinessZip',
            'Country',
            'WebsiteURL',
            'Password',
			'SignOff_Disabled',
			'CustomSignOff_Enabled',
            'DeniedTech_NoShowGreater',
            'DeniedTech_NoShowPeriod',
            'DeniedTech_BackOutGreater',
            'DeniedTech_BackOutPeriod',
            'DeniedTech_PreferenceRatingSmaller',
            'DeniedTech_PreferenceRatingPeriod',
            'DeniedTech_PerformanceRatingSmaller',
            'DeniedTech_PerformanceRatingPeriod',
			'contextPushWMEnabled'
        );
		
        $updateFields = array();
        foreach ($formFields as $key => $value) {
            if (in_array($key, $allowedToUpdate)) {
                $updateFields[$key] = $value;
            }
        }

		if (!$clientInfo->isPM() && empty($_SESSION['PM_Company_ID']))  {
			unset($updateFields['contextPushWMEnabled']);
		}
		else if (!isset($updateFields['contextPushWMEnabled'])) {
			$updateFields['contextPushWMEnabled'] = 0;
		}
		//else {
		//	$updateFields['contextPushWMEnabled'] = 1;
		//}

        if (!empty($formFields['Company_Logo_Rm']) && $formFields['Company_Logo_Rm'] == 1) {
            // remove client logo
            $file = new Core_File();
            $file->removeFile($clientInfo->Company_Logo, S3_CLIENTS_LOGO_DIR); // Remove old file
            $errors = Core_Api_Error::getInstance();
            $errors->resetErrors();
        }

        if (!empty($formFields['CustomSignOff_Rm']) && $formFields['CustomSignOff_Rm'] == 1) {
	        $db = Core_Database::getInstance ();
	        $select = $db->select ();
	        $select->from (Core_Database::TABLE_CLIENTS_FILES)
	        	->where ("company_id = ?", $clientInfo->Company_ID)
	        	->where ("type = ?", Core_Files::SIGNOFF_FILE);
	        $file_row = $select->query()->fetch();

	        if (!empty ($file_row)) {
	        	$file_id = intval ($file_row["id"]);
	        	$file_path = $file_row["path"];

				//$file = new Core_File();
	            //$file->removeFile($file_path, S3_CLIENTS_DOCS_DIR); // Remove old file

	            $query = $db->quoteInto("DELETE FROM clients_files WHERE id = ?", $file_id, "INTEGER");
	            $db->query ($query);
	        }
        }

        $search = new Core_Api_Class();
        if ($needUpdate) {
            //13970
            if(empty($updateFields['DeniedTech_NoShowGreater'])){                
                $updateFields['DeniedTech_NoShowPeriod'] = '';
            }
            if(empty($updateFields['DeniedTech_BackOutGreater'])){
                $updateFields['DeniedTech_BackOutPeriod'] = '';
            }
            if(empty($updateFields['DeniedTech_PreferenceRatingSmaller'])){
                $updateFields['DeniedTech_PreferenceRatingPeriod'] = '';
            }
            if(empty($updateFields['DeniedTech_PerformanceRatingSmaller'])){
                $updateFields['DeniedTech_PerformanceRatingPeriod'] = '';
            }

            $result = $search->updateClientProfile($client_login . $pmContext,
                $client_password, 
                $updateFields,
                false,
                Core_Validation_Factory::VALIDATION_TYPE_CLIENT_PROFILE_UPDATE
            );
            if (!empty($formFields['Company_Logo_Rm']) && $formFields['Company_Logo_Rm'] == 1) {
                $updateFields['Company_Logo'] = "";
            }
            $result_1 = $search->updateCompanyProfile($clientInfo->Company_ID,$updateFields);
            
            
            $needRunDenyTool = false;
            $denyFields = array();            
            $denyFields[]='DeniedTech_NoShowPeriod';
            $denyFields[]='DeniedTech_BackOutGreater';
            $denyFields[]='DeniedTech_BackOutPeriod';
            $denyFields[]='DeniedTech_PreferenceRatingSmaller';
            $denyFields[]='DeniedTech_PreferenceRatingPeriod';
            $denyFields[]='DeniedTech_PerformanceRatingSmaller';
            $denyFields[]='DeniedTech_PerformanceRatingPeriod';
            foreach($denyFields as $field){
                if( $clientInfo->$field != $updateFields["$field"] ){
                    if(!empty($clientInfo->$field) || !empty($updateFields["$field"])){
                        $needRunDenyTool = true;
                        break;
                    }
                }
            }
            
            if(empty($updateFields['DeniedTech_NoShowPeriod']) && empty($updateFields['DeniedTech_NoShowPeriod']) && empty($updateFields['DeniedTech_NoShowPeriod']) && empty($updateFields['DeniedTech_NoShowPeriod'])) {
                $needRunDenyTool = true;
            }
            
            //$needRunDenyTool = true;//test
            
            $TechClass = new Core_Api_TechClass();
            if($needRunDenyTool){                
                //$TechClass->applyDenyTechRule_ByCompany($clientInfo->Company_ID);//test
                $dir = APPLICATION_PATH.'/../../htdocs/cron';
                exec('php ' . $dir . '/autoDenyTechs.php '.escapeshellarg($clientInfo->Company_ID));                
            }
                                    
            //echo('<br/>$needRunDenyTool: ');print_r($needRunDenyTool);
            //echo('<br/>$clientInfo: ');print_r($clientInfo);
            //echo('<br/>$updateFields: ');print_r($updateFields);
            //die();            
            
            //end 13970
            
            $errors = Core_Api_Error::getInstance();
            if (!$result->success) {
                foreach ($result->errors as $error) {
                    $return['success'] = 0;
                    $return['errors'] = array($error->message);
                }
            } else {
                if (!empty($updateFields['Password'])) {
                    //change strored in session pass
                    $auth = new Zend_Session_Namespace('Auth_User');
                    $auth->password = $updateFields['Password'];
                }
                if(isset($params['PcntDeductPercent']) && $params['PcntDeductPercent'] != "")
                {    
                    $user->SaveDeductPercent_FSSFeeFrTech($clientInfo->Company_ID,$params['PcntDeductPercent']);
                }
                $return['success'] = 1;
            }
        }

        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-Length', strlen($return), true);
        $response->setBody($return);
    }

    public function clientTechSearchAction() {
        $this->_helper->viewRenderer->setNoRender(true);
    	$request = $this->getRequest();
        $params = $request->getParams();
        $paramsSearchBack = $params;
        $resultsHtml = "";
        $searchCriteria = "";
        $proxmityArr = array();
        
        $db = Core_Database::getInstance();
         

        $newSearch = $request->getParam("newSearch", NULL);
		$renderAssignSelect = $request->getParam("renderAssignSelect", NULL);
        @$defaultZip = $request->getParam("defaultZip");
         
        if ($newSearch) {
            unset($_SESSION["savedQuickSearch"]);
            $_SESSION["currentResultPage"] = 1;
        }
//1 
        $doSearch = $request->getParam("doSearch", NULL);
        $doSearch = true;
        // echo date("Y-m-d H:i:s");  
        //$autoSubmit = $request->getParam("autoSubmit", NULL);
        //if ($autoSubmit != NULL) {
        //14099  
		
		//14120
        //if ($params['Country'] == "")
        //    $params['Country'] = "US";    
		//End 14120
		
        $params['ProximityZipCode'] = trim($request->getParam("ProximityZipCode"));
        $params['ProximityLat'] = $request->getParam("ProximityLat");
        $params['ProximityLng'] = $request->getParam('ProximityLng');
        $params['ProximityDistance'] = $request->getParam('ProximityDistance');
        $doSearch = true;
       // echo date("Y-m-d H:i:s");
        //}
        // for PrimaryPhone
         //1          
        $PrimaryPhone = "";  
        if(!empty($params['PrimaryPhone'])){
            $PrimaryPhone = preg_replace("/[-() ]/", "", $params['PrimaryPhone']);
            if(!empty($PrimaryPhone)){
                if($PrimaryPhone[0] == "1"){
                    $PrimaryPhone = substr($PrimaryPhone, 1);
                }
            }  
            unset($params['PrimaryPhone']);
        }
          //1 
        if ($params['SATPerformanceAvg'] != "")
            $params['PerformancePercent'] = $params['SATPerformanceAvg'];          //1
               //19.1 
        if ($params['SATRecommendedAvg'] != "")
            $params['PreferencePercent'] = $params['SATRecommendedAvg'];        //1
                      
        $USAuthorized = empty($params['USAuthorized']) ? '' : $params['USAuthorized'];
        $LMSPreferredTechnician = empty($params['LMSPreferredTechnician']) ? '' : $params['LMSPreferredTechnician'];
        $LMSKeyContractor = empty($params['LMSKeyContractor']) ? '' : $params['LMSKeyContractor'];
        $NACICertStatus = empty($params['NACICertStatus']) ? '' : $params['NACICertStatus'];        
        $Copier_Skills_Assessment = empty($params['Copier_Skills_Assessment']) ? '' : $params['Copier_Skills_Assessment'];
        $VerifiedSignLanguageCompetency = empty($params['VerifiedSignLanguageCompetency']) ? '' : $params['VerifiedSignLanguageCompetency'];
        
        unset($params['VerifiedSignLanguageCompetency']);
        //535
        //13735
        $hideISOTech = empty($params['hideisothech']) ? 0 : $params['hideisothech'];
        unset($params['hideisothech']);
        //end 13735
        
        //735
        $arrFSExpertCode = array();
        //--- number of wo (NumOfWO)
        foreach($params as $k=>$v)
        {
            $findFSExpertCode   = 'FSExpertCode';
            $posFSExpertCode = strpos($k, $findFSExpertCode);
            if ($posFSExpertCode === false) {
                continue;
            } else {
                $arrFSExpertCode[$k]=$v;
                unset($params[$k]);
            }                        
        }
        //end 735
          
        //$LanguageSkill
        $languageSkillIds = empty($params['languageSkillId']) ? '' : $params['languageSkillId'];
        unset($params['languageSkillId']);
        if(!empty($languageSkillIds)){
            foreach($languageSkillIds as $k=>$languageSkillId){
                $LanguageSkill[$k]["id"] = $languageSkillId;
                $LanguageSkill[$k]["type"] = "3";
                if(isset($params['chklanguagecompetent_'.$languageSkillId])){
                    $LanguageSkill[$k]["type"] = 0;
                    unset($params['chklanguagecompetent_'.$languageSkillId]);
                }
                if(isset($params['chklanguagefluent_'.$languageSkillId])){
                    $LanguageSkill[$k]["type"] = 2;
                    unset($params['chklanguagefluent_'.$languageSkillId]);
                }
            }
        }
         
        $MCSACertified=empty($params['MCSA']) ? '' : $params['MCSA'];
        unset($params['MCSA']);
        $MCPCertified=empty($params['MCP']) ? '' : $params['MCP'];
        unset($params['MCP']);        
        $BlueRibbonTechnician = empty($params['FStagBlueRibbonTechnician']) ? '' : $params['FStagBlueRibbonTechnician'];
        $BlueRibbonTechnician = empty($params['BlueRibbonTechnician']) ? '' : $params['BlueRibbonTechnician'];
        $BootCampCertified = empty($params['FStagBootCampCertified']) ? '' : $params['FStagBootCampCertified'];
        $BootCampCertified = empty($params['BootCampCertified']) ? '' : $params['BootCampCertified'];
        unset($params['FStagBlueRibbonTechnician']);
        unset($params['FStagBootCampCertified']);
        unset($params['BlueRibbonTechnician']);
        unset($params['BootCampCertified']);
         
        $arrNumOfWO = array();
        //--- number of wo (NumOfWO)
        foreach($params as $k=>$v)
        {
            $findme   = 'NumOfWO';
            $pos = strpos($k, $findme);
            if ($pos === false) {
                continue;
            } else {
                $arrNumOfWO[$k]=$v;
            }                        
        }
        //14056
         //14073
        if(isset($params['NACI'])){
            $params['NACI'] = 2;
            if(isset($params['NACILapsed'])){
                $params['NACI'] = 1;
                unset($params['NACILapsed']);
            }
        }        
        if(isset($params['InterimSecClear'])){
            $params['InterimSecClear'] = 2;
            if(isset($params['InterimSecClearLapsed'])){
                $params['InterimSecClear'] = 1;
                unset($params['InterimSecClearLapsed']);                
            }
        }        
        if(isset($params['FullSecClear'])){
            $params['FullSecClear'] = 2;
            if(isset($params['FullSecClearLapsed'])){
                $params['FullSecClear'] = 1;
                unset($params['FullSecClearLapsed']);                
            }
        }        
        if(isset($params['TopSecClear'])){
            $params['TopSecClear'] = 2;
            if(isset($params['TopSecClearLapsed'])){
                $params['TopSecClear'] = 1;
                unset($params['TopSecClearLapsed']);                
            }
        }        
        if(isset($params['TopSecSCIClear'])){
            $params['TopSecSCIClear'] = 2;
            if(isset($params['TopSecSCIClearLapsed'])){
                $params['TopSecSCIClear'] = 1;
                unset($params['TopSecSCIClearLapsed']);                
            }
        }        
        //end 14056
        //end 14073
        //--- unset
        unset($params['SATPerformanceAvg']);
        unset($params['SATRecommendedAvg']);
        unset($params['emailList']);
        unset($params['USAuthorized']);
        unset($params['LMSPreferredTechnician']);
        unset($params['LMSKeyContractor']);
        unset($params['techIds']);
        unset($params['renderAssignSelect']);
        unset($params['NACICertStatus']);        
        unset($params['Copier_Skills_Assessment']);
        unset($params['latLongs']);
        unset($params['mappedWosPage']);
        foreach($params as $k=>$v)
        {
            $findme   = 'NumOfWO';
            $pos = strpos($k, $findme);
            if ($pos === false) {
                continue;
            } else {
                unset($params[$k]);
            }                        
        }
          
        $apiTechClass = new Core_Api_TechClass();
        //for certifications
        $certifications = "";
        if(isset($params['certifications'])){
           $certifications_input = $params['certifications'];
           //123
           unset($params['certifications']);
           foreach($certifications_input as $certificationsid){
                $techCertified = $apiTechClass->getTechIdsWithGenaralCertified($certificationsid);  
                
                if($certificationsid == 41 || $certificationsid == 42){
                    if(in_array(41, $certifications_input) && in_array(42, $certifications_input)){
                        $techCertified1 = $apiTechClass->getTechIdsWithGenaralCertified(41);  
                        $techCertified2 = $apiTechClass->getTechIdsWithGenaralCertified(42);  
                        if(!empty($techCertified1) && !empty($techCertified2))
                        {
                            $certifications.=' AND (t.TechID IN(' . $techCertified1."," .$techCertified2. '))';
                            
                        }
                            
                        else if((empty($techCertified1) && !empty($techCertified2)) || (!empty($techCertified1) && empty($techCertified2))){
                            $certifications.=' AND t.TechID IN(' . !empty($techCertified1)?$techCertified1:$techCertified2 . ')';
                        }else{
                            $certifications.=' AND 1=0';
                        }
                    }else if(!empty($techCertified)){
                    $certifications.=' AND t.TechID IN(' . $techCertified . ')';
                } else {
                    $certifications.=' AND 1=0';
                } 
           }
                else if (!empty($techCertified)) {
                    $certifications.=' AND t.TechID IN(' . $techCertified . ')';
                } else {
                    $certifications.=' AND 1=0';
				}
			}
        }
        
        //13931
        if(isset($params['certificationsnew'])){
           $certificationsinput = $params['certificationsnew'];  
           if(in_array(19,$certificationsinput)){
                $C_techs = $apiTechClass->getTechIdsArray_ByGGEStatus("Pass","",""); 
                if (!empty($C_techs)) {                   
                    $certifications.=' AND t.TechID IN (' . $C_techs . ')';
                } else {
                    $certifications.=' AND 1=0';
                }                                  
           }
             
           foreach($certificationsinput as $index=>$v){
               if($v==19){
                   unset($certificationsinput[$index]);
               }
           }              
           $FSTagClass = new  Core_Api_FSTagClass();
           
           if(count($certificationsinput)>0){               
               $C_techs = $FSTagClass->getTechIDArray_forFSTag($certificationsinput);
               //echo("<br/>C_techs: ");print_r($C_techs);die();
               if(!empty($C_techs) && count($C_techs) > 0){
                   $tempTechIDs = "";
                   foreach($C_techs as $tempTechID){
                       $tempTechIDs.= empty($tempTechIDs) ? $tempTechID : ','.$tempTechID;
                   }
                   $certifications .= " AND t.TechID IN ($tempTechIDs)";
               } else {
                   $certifications .= " AND 1=0 "; 
               }
           }
           unset($params['certificationsnew']);    
        }    
        //end 13931
        
        //print_r("------test-------");
        //--- Number WO Search
        $apiWOCategory = new API_WOCategory();
        $NUMWOSearchCriteria = "";
        
        if(!empty($arrNumOfWO))   
        {
			$NUMWOSearchCriteriaArray = array();
            foreach($arrNumOfWO as $k=>$v)
            {                
				$parts = explode("NumOfWO",$k);
                $techSRCol = $parts[0];
                $categoryID = $parts[1];
				$NUMWOSearchCriteriaArray[] = "(" . $db->quoteInto('twcc.WO_Category_ID = ?', $categoryID) . $db->quoteInto(' AND twcc.completed >= ?', $v) . ")";    //14121
            }
			if (!empty($NUMWOSearchCriteriaArray)) {
				$NUMWOSearchCriteriaInitial = "(" . implode(" AND ", $NUMWOSearchCriteriaArray) . ")";
				$NumWOSelect = $db->select();
				$NumWOSelect->from(array("twcc" => "tech_wo_category_count"), array("Tech_ID"))
					->where($NUMWOSearchCriteriaInitial);
                   
				$NUMWO_SR = $db->fetchCol($NumWOSelect);
				if (!empty($NUMWO_SR))
					$NUMWOSearchCriteria = $db->quoteInto(' AND TechID IN (?)', $NUMWO_SR);	
				else
					$NUMWOSearchCriteria = "AND 1 = 0";
			}
        }     
        //735
        
        $FSExpertCodeSearchCriteria = "";
        $apiFSExpertClass = new Core_Api_FSExpertClass();
        if(!empty($arrFSExpertCode))   
        {
            $FSExpertCodes = array();
            foreach($arrFSExpertCode as $k=>$v)
            {
                $parts = explode("FSExpertCode",$k);
                $FSExpertCodes[] = $parts[0];
            }

            //--- apply FSExpertRule for all tech -- moved to cron      
/*            foreach($FSExpertCodes as $FSExpertCode){
                 $apiFSExpertClass->applyFSExpertRule_ForAllTech($FSExpertCode);
            }*/
            
            //--- find techid array
            $result="";
            $resultval = "";
            
            $filters=array('FSExpertCodeArray'=>$FSExpertCodes);
            $TechIDArray_hasFSExpert = $apiFSExpertClass->getTechIDArray_hasFSExpert($filters);
            if(!empty($TechIDArray_hasFSExpert) || count($TechIDArray_hasFSExpert)>0) {
                $resultval = implode(",", $TechIDArray_hasFSExpert);
            }            
            if(!empty($resultval) || $resultval!="") {
                $FSExpertCodeSearchCriteria .=  " AND TechID IN (". $resultval .") ";
            } else {
                $FSExpertCodeSearchCriteria .=  " AND 1 = 0 ";
            }       
        }    
        //end 13735
        //--- EMC Certification Search
        //echo("<br/>USAuthorized: $USAuthorized");//test
        $EMCSearchCriteria = "";
        
        $apiEMC = new Core_Api_TechClass;
        $emcitems = $apiEMC->getEMCList();
        
        if(count($emcitems)>0)
        {
            foreach ($emcitems as $item)
            {
                if(isset ($params[$item['name']]) && $params[$item['name']]=='on')
                {
                    //echo "emcitems: ". $params[$item['name']]."<br/>";
                    $EMCsearchresult = $apiEMC-> getTechIdsForCertification($item['name'],'','');
                    if($EMCsearchresult != '')
                        $EMCSearchCriteria .= ' AND TechID IN ('.$EMCsearchresult.')'; 
                }
                
                unset($params[$item['name']]);
            }
        }  //123
        //13757--- Tool Search 
       
        $ToolsSearchCriteria = "";
            
        $toolitems = $apiEMC->getToolsList();
       
        if(count($toolitems)>0)
        {
            foreach ($toolitems as $item)
            {
                if(isset ($params[$item['name']]) && $params[$item['name']]=='on')
                {
                    //echo "Toolsitems: ". $params[$item['name']]."<br/>";
                    $Toolsearchresult = $apiEMC-> getTechIdsForTool($item['name']);
                    if($Toolsearchresult != '')
                        $ToolsSearchCriteria .= ' AND TechID IN ('.$Toolsearchresult.')'; 
                }
                
                unset($params[$item['name']]);
            }
        }       //123  
          //--- Skill Search
       
        $SkillSearchCriteria = "";   
            
        $skillitems = $apiEMC->getSkillsList();
        
        if(count($skillitems)>0)
        {
            foreach ($skillitems as $item)
            {
                if(isset ($params[$item['name']]) && $params[$item['name']]=='on')
                {
                    //echo "emcitems: ". $params[$item['name']]."<br/>";
                    $Skillsearchresult = $apiEMC-> getTechIdsForSkill($item['name']);
                    if($Skillsearchresult != '')
                        $SkillSearchCriteria .= ' AND TechID IN ('.$Skillsearchresult.')'; 
                }
                
                unset($params[$item['name']]);
            }
        }        

        try {
            $searchCriteriaResults = Core_Tech::getUserSearchCriteria($params);
            //print_r($searchCriteriaResults);die();//test
        } catch (Exception $e) {
            // invalid field entered
            $doSearch = false;
            $error = explode("|", $e->getMessage());
        }
         
        //if ($newSearch || (!$doSearch)) {
        // New search
        //$companyID = $request->getParam("v");
        //}else {
        $searchFields = "";
        $ignoreFields = array("doSearch" => 1, "doReset" => 1, "hideBanned" => 1, "preferredOnly" => 1, "newSearchBtn" => 1, "DellMRACompliant" => 1, "DellMRALapse" => 1, "DevryShow" => 1, "TBCShow" => 1, "BlueRibbonTechnicianShow" => 1, "USAuthorizedDummy" => 1, "techIds" => 1, "renderAssignSelect" => 1);
        foreach ($params as $key => $value) {
            if (array_key_exists($key, $ignoreFields))
                continue;
            $searchFields .= "<input id=\"$key\" name=\"$key\" type=\"hidden\" value=\"" . htmlentities($value) . "\"/>";
        }
          
        // get variables
        $companyID = $request->getParam("v", "BLANKID");
        $showBanned = $request->getParam("hideBanned", false);
        $showPreferred = $request->getParam("preferredOnly", false);
        $sort = $request->getParam("sort", NULL);
        $order = $request->getParam("order", 0);

        // result page configuration
        $resultsPerPage = 25;
        $page = $request->getParam("page");
        if ($page != "" && is_numeric($page))
            $currentResultPage = $page;
           
        else
            $currentResultPage = (isset($_SESSION["currentResultPage"]) ? $_SESSION["currentResultPage"] : 1);
          
        // fields pulled from DB
        $preferredTechs = Core_Tech::getClientPreferredTechsArray($companyID);
        $DeVryTechs = Core_TechCertifications::getDeVryTechIdsArray();
        $TBCTechs = Core_TechCertifications::getTBCTechIdsArray();
        $BlueRibbonTechs = Core_TechCertifications::getBlueRibbonTechIdsArray();
        $I9VerifiedTechs = Core_Api_TechClass::getI9VerifiedTechIdsArray();
        $I9PendingTechs = Core_Api_TechClass::getI9PendingTechIdsArray();
        //735
        $client = new Core_Api_Class();
        $isGPM = $client->isGPMByUserName($this->_login);
        $BBCabling="";
        $ClientCre="";
        if($isGPM || in_array($companyID,array('BBAL','BBGC','BBGE','BBCN','BCSST','BBGN','BBJC','BBLC','BBNSG','BBNC','BBS','BB')))
        {
            $BBCabling = " 
                (SELECT Count(*) AS BBCablingCount FROM tech_certification  WHERE tech_certification.certification_id IN (46,47,48) AND tech_certification.TechID=`TechBankInfo`.`TechID`)
              + (SELECT Count(*) AS BBTelecomCount FROM tech_certification WHERE tech_certification.certification_id IN (49,50) AND tech_certification.TechID=`TechBankInfo`.`TechID`) " ;
        }
          
        if($isGPM)
        {
            $ClientCre = " 
                (SELECT Count(*) AS FSTagCount 
                FROM fstag_techs tagte 
                    INNER JOIN fstags tag ON tag.id = tagte.FSTagId 
                WHERE tagte.TechId=`TechBankInfo`.`TechID` AND tag.Active = 1 AND tag.DisplayOnClientCredentials = 1
                        AND tag.VisibilityId = 1 AND  tag.Title NOT LIKE '%Private Tech%') " ;
                         
        }
        
        else
        {
            $ClientCre = " 
                (SELECT Count(*) AS FSTagCount 
                FROM fstag_techs tagte 
                    INNER JOIN fstags tag ON tag.id = tagte.FSTagId 
                WHERE tagte.TechId=`TechBankInfo`.`TechID` AND tag.Active = 1 AND tag.DisplayOnClientCredentials = 1
                    AND tag.VisibilityId = 1  AND  tag.Title NOT LIKE '%Private Tech%' 
                    AND tagte.FSTagId IN (SELECT FSTagId FROM fstag_clients WHERE CompanyId='$companyID')) " ;
                    
        }
         
        $ClientCredentials = "(".$BBCabling." + ".$ClientCre.")";
        $specialColumns = array("ISO" => "(CASE WHEN IFNULL(ISO_Affiliation, '') = '' THEN 'No' ELSE 'Yes' END)",
            "PreferLevel" => "(CASE WHEN (" . ($preferredTechs && $preferredTechs[0] != '' ? 'TechID IN (\'' . implode("','", $preferredTechs) . '\')' : '1 = 0') . ") THEN 'Yes' ELSE 'No' END)",
            "SpecialDistance" => "Distance",
            "TotalWOs" => "(IFNULL(Qty_FLS_Service_Calls,0) + IFNULL(Qty_IMAC_Calls,0))",
            "DellMRACompliant" => "(CASE WHEN (Bg_Test_Pass_Full = 'Pass' AND DrugPassed = '1' AND Bg_Test_ResultsDate_Full >= DATE_SUB(NOW(), INTERVAL 12 MONTH) AND DatePassDrug >= DATE_SUB(NOW(), INTERVAL 12 MONTH)) THEN 'Passed' WHEN (Bg_Test_Pass_Full = 'Pass' AND DrugPassed = '1' AND (Bg_Test_ResultsDate_Full < DATE_SUB(NOW(), INTERVAL 12 MONTH) OR DatePassDrug < DATE_SUB(NOW(), INTERVAL 12 MONTH))) THEN 'Lapsed' ELSE '' END)", "LapsedDate" => "DATE_ADD((CASE WHEN Bg_Test_ResultsDate_Full > DatePassDrug THEN DatePassDrug ELSE Bg_Test_ResultsDate_Full END), INTERVAL 12 MONTH)",
            "DeVryFlag" => "(CASE WHEN (" . (!empty($DeVryTechs) ? 'TechID IN (' . implode(",", $DeVryTechs) . ')' : '1 = 0') . ") THEN 1 ELSE 0 END)",
            "TBCFlag" => "(CASE WHEN (" . (!empty($TBCTechs) ? 'TechID IN (' . implode(",", $TBCTechs) . ')' : '1 = 0') . ") THEN 1 ELSE 0 END)",
            "BlueRibbonFlag" => "(CASE WHEN (" . (!empty($TBCTechs) ? 'TechID IN (' . implode(",", $TBCTechs) . ')' : '1 = 0') . ") THEN 1 ELSE 0 END)" ,
            "I9VerifiedFlag" => "(CASE WHEN (" . (!empty($I9VerifiedTechs) ? 'TechID IN (' . implode(",", $I9VerifiedTechs) . ')' : '1 = 0') . ") THEN 1 ELSE 0 END)",
            "I9PendingFlag" => "(CASE WHEN (" . (!empty($I9PendingTechs) ? 'TechID IN (' . implode(",", $I9PendingTechs) . ')' : '1 = 0') . ") THEN 1 ELSE 0 END)",
            "FSExpert" => "(select count(`fsexpert_techs`.`FSExpertId`) from `fsexpert_techs` where `TechBankInfo`.`TechID` = `fsexpert_techs`.`TechId`)",
            "PublicCredentials" => "(
                                        (case when (Bg_Test_Pass_Lite = 'Pass' and Bg_Test_Pass_Full = 'Pass') or (Bg_Test_Pass_Lite = 'Pass' and  Bg_Test_Pass_Full <> 'Pass') then 1 else 0 end)
                                        +
                                        (case when DrugPassed = '1' or SilverDrugPassed='1' then 1 else 0 end )
                                        +
                                        (case when (CASE WHEN (Bg_Test_Pass_Full = 'Pass' AND DrugPassed = '1' AND Bg_Test_ResultsDate_Full >= DATE_SUB(NOW(), INTERVAL 12 MONTH) AND DatePassDrug >= DATE_SUB(NOW(), INTERVAL 12 MONTH)) THEN 'Passed' WHEN (Bg_Test_Pass_Full = 'Pass' AND DrugPassed = '1' AND (Bg_Test_ResultsDate_Full < DATE_SUB(NOW(), INTERVAL 12 MONTH) OR DatePassDrug < DATE_SUB(NOW(), INTERVAL 12 MONTH))) THEN 'Lapsed' ELSE '' END) <>'' then 1 else 0 end)
                                        +
                                        (case when isnull(`TechBankInfo`.`FS_Mobile_Date`) then 0 else 1 end)
                                        +
                                        (select count(`tech_certification`.`certification_id`) from `tech_certification` where `TechBankInfo`.`TechID` = `tech_certification`.`TechId` and `tech_certification`.`certification_id`=4)
                                        +
                                        (select count(`tech_certification`.`certification_id`) from `tech_certification` where `TechBankInfo`.`TechID` = `tech_certification`.`TechId` and `tech_certification`.`certification_id` in(3, 43))
                                        +
                                        (select count(`tech_ext`.`I9Status`) from `tech_ext` where `TechBankInfo`.`TechID` = `tech_ext`.`TechId` and `tech_ext`.`I9Status` <> 0)
                                        +
                                        (case when (CASE WHEN IFNULL(ISO_Affiliation, '') = '' THEN 'No' ELSE 'Yes' END)='yes' then 1 else 0 end)
                                    )",
            "ClientCredentials" => "$ClientCredentials",
            //943
            //"TotalWosCompany"=>"(SELECT Count(WIN_NUM) FROM `work_orders` WHERE `work_orders`.Tech_ID=`TechBankInfo`.`TechID` AND Company_ID='".$companyID."' AND `work_orders`.Approved=1 AND (`work_orders`.Type_ID = 1 OR `work_orders`.Type_ID =2))"
            //end 943
        );
         
        //13910
        $fieldList = array("TechID" => "TechID", "LastName" => "LastName", "FirstName" => "FirstName", "ISO" => "{$specialColumns["ISO"]}",
                "PrimaryPhone" => "PrimaryPhone", "PrimPhoneType" => "PrimPhoneType", "PrimaryEmail" => "PrimaryEmail", "City" => "City", 
                "State" => "State", "PreferencePercent" => "PreferencePercent", "SATRecommendedTotal" => "SATRecommendedTotal", 
                "Qty_IMAC_Calls" => "Qty_IMAC_Calls", "Qty_FLS_Service_Calls" => "Qty_FLS_Service_Calls", "No_Shows" => "No_Shows", 
                "Back_Outs" => "Back_Outs", "PreferLevel" => "{$specialColumns["PreferLevel"]}", "FS_Cert_Test_Pass" => "FS_Cert_Test_Pass", 
                "Bg_Test_Pass_Lite" => "Bg_Test_Pass_Lite", "Bg_Test_Pass_Full" => "Bg_Test_Pass_Full", "FLSCSP_Rec" => "FLSCSP_Rec", 
                "FLSID" => "FLSID", "BG_Test_ResultsDate_Lite" => "BG_Test_ResultsDate_Lite", "BG_Test_ResultsDate_Full" => "BG_Test_ResultsDate_Full",
                "DatePassDrug" => "DatePassDrug", "FSMobileDate" => "FS_Mobile_Date", "PerformancePercent" => "PerformancePercent",
                "SATPerformanceTotal" => "SATPerformanceTotal", "DrugPassed" => "DrugPassed", "Zip" => "ZipCode",
                "TotalWOs" => "{$specialColumns["TotalWOs"]}", "DellMRACompliant" => $specialColumns["DellMRACompliant"],
                "LapsedDate" => $specialColumns["LapsedDate"], "Latitude" => "Latitude", "Longitude" => "Longitude", "Address1"=>"Address1",
                "SecondaryPhone" =>"SecondaryPhone", "SecondaryEmail" => "SecondaryEmail", "ISO_Affiliation_ID" => "ISO_Affiliation_ID",
                "SilverDrugTest_Pass" =>"SilverDrugTest_Pass","SilverDrugPassed" => "SilverDrugPassed", "DatePassedSilverDrug" => "DatePassedSilverDrug",
                "FSExpert"=>$specialColumns["FSExpert"],
                "PublicCredentials"=>$specialColumns["PublicCredentials"],
                "ClientCredentials"=>$specialColumns["ClientCredentials"],
                "NumComments"
                );
        //end 13910
         
        //result page fields name to field list map. Format - "ColumnName" => "Order Returned in $fieldList"
        $resultFieldMap = array("TechID" => "0", "LastName" => "1", "FirstName" => "2", "ISO" => "3", "PrimaryPhone" => "4", "PrimPhoneType" => "5", "PrimaryEmail" => "6", "City" => "7", "State" => "8", "Qty_IMAC_Calls" => "12", "Qty_FLS_Service_Calls" => "13", "No_Shows" => "14", "Back_Outs" => "15", "PreferLevel" => "16", "FS_Cert_Test_Pass" => "17", "Bg_Test_Pass_Lite" => "18", "FLSCSP_Rec" => "19", "FLSID" => "20", "BG_Test_ResultsDate_Lite" => "21", "PreferencePercent" => "10", "SATRecommendedTotal" => "11", "PerformancePercent" => "22", "SATPerformanceTotal" => "23", "DrugPassed" => "24", "Zip" => "9", "TotalWOs" => "26", "DellMRACompliant" => "27", "TotalWOs" => "26", "LapsedDate" => "28");

        // result page columns label to field name / sortable map. Format - "ColumnLabel" => "ColumnName|isSortable (0 = false, 1 = true)

        if ($companyID == "FLS") {
            $resultColumnMap = array(
				"" => "AssignSelect|0",
				"Snap<br/>Shot" => "RatingBox|0",
        		"FS-Tech<br/>ID#" => "TechID|1",
                "FLS ID" => "FLSID|1",
                "Name<hr>Contact Information" => "LastName|1",
                "City, State<br />Zip" => "City|1",
                "Miles" => "SpecialDistance|1",
                "Preference<hr>Performance" => "PreferencePercent|1",
                "Work<br/>Orders" => "TotalWOs|1",
                "No&nbsp;Shows<hr>Back Outs" => "No_Shows|1",
                "Client<br/>Credentials" => "ClientCredentials|1",
                "Public<br/>Credentials" => "PublicCredentials|1",
                "<a href=\"javascript:;\" onclick=\"sortBy('FSexpert')\" style=\"color:#FFFFFF;\">FS&ndash;Experts&#8482; &ndash; Skill Categories&nbsp;</a><a id=\"expertinfocontrols\" href=\"javascript:void(0)\"><img style=\"vertical-align: middle;height:20px;\" src=\"/widgets/images/get_info.png\"></a>" => "FSexpert|1",
                );
        } else {
            $resultColumnMap = array(
				"" => "AssignSelect|0",
				"Snap<br/>Shot" => "RatingBox|0",
                "FS-Tech<br/>ID#" => "TechID|1",
                "Name<hr>Contact Information" => "LastName|1",
                "City, State<br />Zip" => "City|1",
                "Miles" => "SpecialDistance|1",
                "Preference<hr class='headerHr'>Performance" => "PreferencePercent|1",
                "Work<br/>Orders" => "TotalWOs|1",
                "No&nbsp;Shows<hr class='headerHr'>Back Outs" => "No_Shows|1",
                 "Client<br/>Credentials" => "ClientCredentials|1",
                "Public<br/>Credentials" => "PublicCredentials|1",
                "<a href=\"javascript:;\" onclick=\"sortBy('FSexpert')\" style=\"color:#FFFFFF;\">FS&ndash;Experts&#8482; &ndash; Skill Categories&nbsp;</a><a id=\"expertinfocontrols\" href=\"javascript:void(0)\"><img style=\"vertical-align: middle;height:20px;\" src=\"/widgets/images/get_info.png\"></a>" => "FSexpert|1",
                );
        }
            
        //end 735
        $quickResult = array();
        $searchResult = array();
        $preferredCriteria = ($showPreferred == true ? "AND t.TechID IN (SELECT Tech_ID FROM client_preferred_techs WHERE CompanyID = '$companyID')" : "");
        $clientDeniedCriteria = "";

        if ($showBanned != "1") {
            $deniedList = Core_Tech::getClientDeniedTechsArray($companyID);
            //print_r($deniedList);
            if (!empty($deniedList))
                $clientDeniedCriteria .= ' AND t.TechID NOT IN (' . implode(',', $deniedList) . ')';
        }
        //die("ClientTechSearch_2");

        
        $userSearchCriteria = implode(" AND ", $searchCriteriaResults["TechBankInfo"]);
        
        if ($userSearchCriteria != "")
            $userSearchCriteria = "AND " . $userSearchCriteria;
        //echo('$userSearchCriteria: <pre>');print_r($searchCriteriaResults["TechBankInfo"]);echo('</pre>'); die();//test  


        $equipmentSearchCriteria = $searchCriteriaResults['equipment'];
        if (sizeof($equipmentSearchCriteria) > 0) {
            $equipSearch = new Core_TechEquipment;
            $equipResult = $equipSearch->getTechsWithEquipment($searchCriteriaResults['equipment']);
            if (!empty($equipResult))
                $equipmentSearchCriteria = ' AND t.TechID IN (' . implode(",", $equipResult) . ')';
            else
                $equipmentSearchCriteria = ' AND t.TechID IN (0)';
        }

        //die("ClientTechSearch_3");

        //if($equipmentSearchCriteria != "") $equipmentSearchCriteria = ' AND t.TechID IN (SELECT TechID from tech_equipment WHERE equipment_id IN('.$equipmentSearchCriteria.'))';
            
        $skillSearchCriteria = $searchCriteriaResults['skills'];
        if (sizeof($skillSearchCriteria) > 0) {
            $skillSearch = new Core_TechSkills;
            $skillResult = $skillSearch->getTechsWithSkills($skillSearchCriteria);
            if (!empty($skillResult))
                $skillSearchCriteria = ' AND t.TechID IN (' . implode(",", $skillResult) . ')';
            else
                $skillSearchCriteria = ' AND t.TechID IN (0)';
            //$skillsSearchCriteria = ' AND t.TechID IN (SELECT TechID from tech_skills WHERE skill_id IN('.$skillsSearchCriteria.'))';
        }
               
       $api = new Core_Api_TechClass;
       
        $NACICert = "";
        if (!empty($NACICertStatus)) {
            $ids = $api->getTechIdsWithGenaralCertified("6");
            if (!empty($ids)) {
                $NACICert.=' AND t.TechID IN(' . $ids . ')';
            } else {
                $NACICert.=' AND 1=0';
            }
        } 
        
        //--- BlackBox 13761
        $BBCert = "";
        $BBFilters=array();
        $BBAllFilters = array('BlackBoxCablingHelper','BBCablingHelperIncludePendingRequests','BlackBoxCablingAdvanced','BBCablingAdvancedIncludePendingRequests','BlackBoxCablingLead','BBCablingLeadIncludePendingRequests','BlackBoxTelecomHelper','BBTelecomHelperIncludePendingRequests','BlackBoxTelecomAdvanced','BBTelecomAdvancedIncludePendingRequests');
        foreach($BBAllFilters as $bbfilter){
            $filter = $request->getParam($bbfilter, NULL);
            if(isset($filter)){
                $BBFilters[] = $bbfilter;
            }
        }
        if(count($BBFilters)>0)
        {
            $ids = $api->getTechIds_BBSearch($BBFilters);
            if (!empty($ids)) {
                $BBCert.=' AND t.TechID IN(' . $ids . ')';
            } else {
                $BBCert.=' AND 1=0';
            }
        }
        
        //--- $Copier_Skills_Assessment Search
        $CopierSkillsAssessmentSearchCriteria = "";
        if (!empty($Copier_Skills_Assessment)) {
            $ids = $api->getTechIds_PassCopierSkills();
            if (!empty($ids)) {
                $CopierSkillsAssessmentSearchCriteria.=' AND t.TechID IN(' . $ids . ')';
            } else {
                $CopierSkillsAssessmentSearchCriteria.=' AND 1=0';
            }
        }   
       
        //--- I9 Search
        //echo("<br/>USAuthorized: $USAuthorized");//test
        $I9SearchCriteria = "";
        $api = new Core_Api_TechClass;
        if (!empty($USAuthorized)) {
            $ids = $api->getTechIdsWithUSAuthorizedOrPending();
            if (!empty($ids)) {
                $I9SearchCriteria.=' AND t.TechID IN(' . $ids . ')';
            } else {
                $I9SearchCriteria.=' AND 1=0';
            }
        }

              

        $BlueRibbonTechniciansearch = "";
        if (!empty($BlueRibbonTechnician))
        {
            $BlueRibbonTechniciansearch = ' AND TechID IN (' . implode(",", $BlueRibbonTechs) . ')';
        }
        
        $BootCampCertifiedsearch = "";
        if (!empty($BootCampCertified))
        {
            //735
            $BootCampPlusTechID = $apiTechClass->getTechIdsWithGenaralCertified(43);  
            
            //end 735
            $BootCampCertifiedsearch = ' AND TechID IN (' . implode(",", $TBCTechs) . ','. $BootCampPlusTechID. ')';;
        }
        //end 530
        
        $LanguageSkillsearch ="";
        $TechLanguages = new Core_TechLanguages();
        if(!empty($LanguageSkill))
        {
               
            foreach($LanguageSkill as $skillitem)
            {
                if($skillitem["type"] != "3")
                {
                    $Techidlist = $TechLanguages->findTechIDs_ByLangAndRating($skillitem["id"],$skillitem["type"]);
                    if(!empty($Techidlist))
                    {
                        $LanguageSkillsearch .= " AND TechID IN(".implode(",", $Techidlist).")";
                         
                    }
                    else
                    {
                        $LanguageSkillsearch .= " AND 1=0 ";
                    }
                }
            }
            //A1 
        }
        
        $VerifiedSignLanguageCompetencysearch = "";
        if (!empty($VerifiedSignLanguageCompetency)) {
            $VerifiedSignLanguageCompetencytag = $apiTechClass->getTechIdsWithGenaralCertified(40);  
            if (!empty($VerifiedSignLanguageCompetencytag))
            {
                $VerifiedSignLanguageCompetencysearch.=' AND t.TechID IN(' . $VerifiedSignLanguageCompetencytag . ')';
                
            }
            else
                $VerifiedSignLanguageCompetencysearch .= ' AND 1=0 ';
                
        }
               //  A1
        //530
        $BlueRibbonTechniciansearch = "";
        if (!empty($BlueRibbonTechnician))
        {
            $BlueRibbonTechniciansearch = ' AND TechID IN (' . implode(",", $BlueRibbonTechs) . ')';
            
        }
        //end 530
          
        @$certSearchCriteria = $searchCriteriaResults['certifications'];
        if (sizeof($certSearchCriteria) > 0) {
            $certSearch = new Core_TechCertifications;
            $certResult = $certSearch->getTechsWithCertification($certSearchCriteria);
            if (!empty($certResult)) {
                $certSearchCriteria = ' AND t.TechID IN (' . implode(",", $certResult) . ')';
            } else {
                $certSearchCriteria = ' AND t.TechID IN (0)';
            }
            //A1
        }

        $filterOutTechsBasedOnTags = "";
		$TechClass = new Core_Api_TechClass;
		$hideTechs = $TechClass->getTechsNotVisibleToClient($companyID);
		if (!empty($hideTechs)) {
			$filterOutTechsBasedOnTags = ' AND t.TechID NOT IN (' . implode(",", $hideTechs) . ')';
              
		}

		$LMSPreferredTechnicianCriteria = "";
        if (!empty($LMSPreferredTechnician)) {
            $LMSPreferredTechnicianids = $api->getTechIdsArray_ByLMSPreferred();
            if (!empty($LMSPreferredTechnicianids)) {
                $LMSPreferredTechnicianCriteria.=' AND t.TechID IN(' . implode(",", $LMSPreferredTechnicianids) . ')';
            } else {
                $LMSPreferredTechnicianCriteria.=' AND 1=0';
            }
             //A1
        }
        $LMSKeyContractorCriteria = "";
        if (!empty($LMSKeyContractor)) {
            $LMSKeyContractorids = $api->getTechIdsArray_ByLMSPlusKey();
            if (!empty($LMSKeyContractorids)) {
                $LMSKeyContractorCriteria.=' AND t.TechID IN(' . implode(",", $LMSKeyContractorids) . ')';
            } else {
                $LMSKeyContractorCriteria.=' AND 1=0';
            }
            //A1
        }
        //--- Hide ISO Techs
        //13735
        $hideISOTechCriteria = "";
        if($hideISOTech){
            $hideISOTechCriteria = " AND (t.ISO_Affiliation_ID IS NULL OR t.ISO_Affiliation_ID = '') "; 
        }
        
        // search order
        if( $sort == 'FSexpert')
        {
            $defaultCriteria = "{$specialColumns["DellMRACompliant"]} DESC, {$specialColumns["I9VerifiedFlag"]} DESC, {$specialColumns["I9PendingFlag"]} DESC,  {$specialColumns["DeVryFlag"]} DESC, {$specialColumns["TBCFlag"]} DESC, {$specialColumns["BlueRibbonFlag"]} DESC,{$specialColumns["TotalWOs"]} DESC, TechID DESC";
            $orderCriteria = Core_Tech::getOrderCriteria("FSexpert", $order, $specialColumns, $defaultCriteria);
        }
        else if( $sort == 'PublicCredentials')
        {
            $defaultCriteria = "{$specialColumns["DellMRACompliant"]} DESC, {$specialColumns["I9VerifiedFlag"]} DESC, {$specialColumns["I9PendingFlag"]} DESC,  {$specialColumns["DeVryFlag"]} DESC, {$specialColumns["TBCFlag"]} DESC, {$specialColumns["BlueRibbonFlag"]} DESC,{$specialColumns["TotalWOs"]} DESC, TechID DESC";
            $orderCriteria = Core_Tech::getOrderCriteria("PublicCredentials", $order, $specialColumns, $defaultCriteria);
        }
        else if($sort=="ClientCredentials" )
        {
            $defaultCriteria = "{$specialColumns["DellMRACompliant"]} DESC, {$specialColumns["I9VerifiedFlag"]} DESC, {$specialColumns["I9PendingFlag"]} DESC,  {$specialColumns["DeVryFlag"]} DESC, {$specialColumns["TBCFlag"]} DESC, {$specialColumns["BlueRibbonFlag"]} DESC,{$specialColumns["TotalWOs"]} DESC, TechID DESC";
            $orderCriteria = Core_Tech::getOrderCriteria("ClientCredentials", $order, $specialColumns, $defaultCriteria);
        }
        else
        {
	$defaultCriteria = "{$specialColumns["PreferLevel"]} DESC, {$specialColumns["TotalWOs"]} DESC, (CASE WHEN {$specialColumns["TotalWOs"]} = 0 THEN TechID ELSE 0 END) ASC";
        $orderCriteria = Core_Tech::getOrderCriteria($sort, $order, $specialColumns, $defaultCriteria);
        }
        //end 13735
        //-- 13761,13757,13735
        $searchCriteria .= "(Private = '0' OR (SELECT Client_ID FROM " . Core_Database::TABLE_ISO . " WHERE UNID = ISO_Affiliation_ID) = '$companyID') AND AcceptTerms = 'Yes' AND Deactivated != '1' AND HideBids = 0 $userSearchCriteria $preferredCriteria $clientDeniedCriteria $equipmentSearchCriteria $skillSearchCriteria $filterOutTechsBasedOnTags $certSearchCriteria $I9SearchCriteria $LMSPreferredTechnicianCriteria $LMSKeyContractorCriteria $NACICert $CopierSkillsAssessmentSearchCriteria $EMCSearchCriteria $NUMWOSearchCriteria $MCSACertifiedCriteria $MCPCertifiedCriteriaCertifiedCriteria $certifications $BootCampCertifiedsearch $BlueRibbonTechniciansearch $LanguageSkillsearch $BBCert$SkillSearchCriteria $ToolsSearchCriteria $hideISOTechCriteria $FSExpertCodeSearchCriteria ";
        $proximitySearch = false;
        //if ($doSearch || !$savedSearchAvailable) {
		$renderRowsOnly = false;
		$renderPagination = true;

        //die("ClientTechSearch_3");

        
        if ($doSearch) {
        	
           //This section is for FS-Mapper Project Mapping, which passes in a list of Lat/Longs for each mapped WO
           
            $woLatLongs = $request->getParam("latLongs", NULL);
            if($woLatLongs !== "" && $woLatLongs !== NULL)$woLatLongs = explode(",",$woLatLongs);
            $woLatLongsCount = count($woLatLongs);
            if(!empty($woLatLongs)){
            	$queries = array();
            	$counter = 1;
            	
            	foreach($woLatLongs as $wo){
            		$queries[$counter] = $db->select();	
            		
            		$techIds = $request->getParam("techIds", NULL);
					if (is_string($techIds)) {
						// match only techs from list
						$techIds = explode("|", $techIds);
						$renderRowsOnly = $page > 1;
						$renderPagination = false;
						$resultsPerPage = 25;
						if (empty($techIds)) $techIds = array(0);
						$searchCriteria .= $db->quoteInto(" AND t.TechID IN (?)", $techIds);
					}
		            $queries[$counter]->from(array('t' => Core_Database::TABLE_TECH_BANK_INFO), array("t.TechID", "PrimaryEmail"))
		                    ->where($searchCriteria);
		            $removeParams = array("module", "dashboard", "controller", "action", "latLongs", "mappedWosPage");
		            $sendParams = $paramsSearchBack;
		            foreach ($sendParams as $k => $v) {
		                if (in_array($k, $removeParams)) {
		                    unset($sendParams[$k]);
		                }
		            }
		            
		           $encodedQS = strtr(base64_encode(addslashes(gzcompress(serialize(http_build_query($sendParams)), 9))), '+/=', '-_,');
		           // if ($params["ProximityZipCode"] != "") {
		           		$latLong = explode(" ",$wo);
		           		
		                $params["ProximityZipCode"] = $params["ProximityZipCode"];
		                $params["ProximityLat"] = $latLong[0];
		                $params["ProximityLng"] = $latLong[1];
		                $params["ProximityDistance"] = 50;
		
		                $centerLatLong = array($params["ProximityLat"], $params["ProximityLng"]);
		                if ($params["ProximityLat"] == "") {
		                    $geocoder = new Core_Geocoding_Google();
		                    $geocoder->setZip($params["ProximityZipCode"]);
		                    $centerLatLong = array($geocoder->getLat(), $geocoder->getLon());
		                }
		
		              //  $_SESSION["savedProximityInfo"] = array($centerLatLong[0], $centerLatLong[1], $params["ProximityDistance"]);
		
		               $queries[$counter] = Core_Database::whereProximity($queries[$counter], $centerLatLong[0], $centerLatLong[1], "Latitude", "Longitude", "{$params["ProximityDistance"]}.49", "Distance");
		               $mappedWoslimit = 25;
		               if($woLatLongsCount > 5 && $woLatLongsCount <= 10){
		               	$mappedWoslimit = 20;
		               }elseif($woLatLongsCount > 10 && $woLatLongsCount <= 20){
		               	$mappedWoslimit = 15;
		               }elseif($woLatLongsCount > 20){
		               	$mappedWoslimit = 10;
		               }
		               $mappedWosPage = $request->getParam("mappedWosPage","");
		               $offset = ($mappedWosPage != "") ? (int)$mappedWosPage * $mappedWoslimit : 0;
		               $queries[$counter]->limit($mappedWoslimit,$offset);
		               $queries[$counter]->order($orderCriteria);
					   $queries[$counter] = "(".$queries[$counter].")";
		               $proximitySearch = false;
		           // }
	
		            $counter++;
		        }
		       
		        $unionQuery = $db->select()->union($queries);
		         error_log($unionQuery->__toString());
		        $quickResult = $db->fetchAll($unionQuery);
            }else{
	            
	            // do quick proximity search
	            $select = $db->select();
				$techIds = $request->getParam("techIds", NULL);
				if (is_string($techIds)) {
					// match only techs from list
					$techIds = explode("|", $techIds);
					$renderRowsOnly = $page > 1;
					$renderPagination = false;
					$resultsPerPage = 25;
					if (empty($techIds)) $techIds = array(0);
					$searchCriteria .= $db->quoteInto(" AND t.TechID IN (?)", $techIds);
				}
	            $select->from(array('t' => Core_Database::TABLE_TECH_BANK_INFO), array("t.TechID", "PrimaryEmail"))
	                    ->where($searchCriteria);
        
	            $removeParams = array("module", "dashboard", "controller", "action");
	            $sendParams = $paramsSearchBack;
	            foreach ($sendParams as $k => $v) {
	                if (in_array($k, $removeParams)) {
	                    unset($sendParams[$k]);
	                }
	            }
			
	            $encodedQS = strtr(base64_encode(addslashes(gzcompress(serialize(http_build_query($sendParams)), 9))), '+/=', '-_,');
	
                //13570
                if ($params["ProximityZipCode"] != "")
                {
                    $apiCommon = new Core_Api_CommonClass();
                    $zipCodeExists = $apiCommon->ZipcodeExists($params["ProximityZipCode"]);
                }
                //---
	            if ($params["ProximityZipCode"] != "" && $zipCodeExists==1) {                  
	                $params["ProximityZipCode"] = $params["ProximityZipCode"];
	                $params["ProximityLat"] = $params["ProximityLat"];
	                $params["ProximityLng"] = $params["ProximityLng"];
	                $params["ProximityDistance"] = $params["ProximityDistance"];
	
	                $centerLatLong = array($params["ProximityLat"], $params["ProximityLng"]);
	                if ($params["ProximityLat"] == "") {
	                    $geocoder = new Core_Geocoding_Google();
	                    $geocoder->setZip($params["ProximityZipCode"]);
	                    $centerLatLong = array($geocoder->getLat(), $geocoder->getLon());
	                }
	
	                $_SESSION["savedProximityInfo"] = array($centerLatLong[0], $centerLatLong[1], $params["ProximityDistance"]);
	
	                $select = Core_Database::whereProximity($select, $centerLatLong[0], $centerLatLong[1], "Latitude", "Longitude", "{$params["ProximityDistance"]}.49", "Distance");
	
	                $proximitySearch = true;
	            }
                
                //--- 13570
                if($params["ProximityZipCode"] != "" && $zipCodeExists!=1)
                {
                    $quickResult = "";
                }
                else
                {
                    //14150
                   if(!empty($PrimaryPhone)){
                    $select->where("(replace(replace(replace(replace(PrimaryPhone,' ',''),')',''),'(',''),'-','') like '$PrimaryPhone%' 
                            OR replace(replace(replace(replace(SecondaryPhone,' ',''),')',''),'(',''),'-','') like '$PrimaryPhone%' 
                            OR replace(replace(replace(replace(SMS_Number,' ',''),')',''),'(',''),'-','') like '$PrimaryPhone%')");
                   }
                   //End 14150
	            $quickResult = $db->fetchAll($select);
                }
            }
            $emailList = array();
            $techList = array();
            
            if ($quickResult && sizeof($quickResult) > 0) {
                $numResult = sizeof($quickResult);
                foreach ($quickResult as $row) {
                        $techList[] = "'".$row["TechID"]."'";
                        $emailList[] = $row["PrimaryEmail"];
                        if (!$renderRowsOnly) {
                          $resultsHtml = "<script type='text/javascript'>
                              var emailList = " . json_encode($emailList) . "
                              </script>";
                        }
                }
                $quickResult = implode(",", $techList);
            } else {
                $numResult = 0;
                $quickResult = "";
            }
         
			if (!$renderAssignSelect) {
				unset($resultColumnMap[""]);
			}

            if ($numResult > 0) {
				
                // pull in data based on quick search
                $offset = ($resultsPerPage * ($currentResultPage - 1));
                $techsMatchingSearchCriteria = $quickResult;
                $Core_Api_TechClass = new Core_Api_TechClass;

                if (substr($techsMatchingSearchCriteria, "-1") == ",")
                    $techsMatchingSearchCriteria = rtrim($techsMatchingSearchCriteria, ",");

                $select = $db->select();
                $select->from(Core_Database::TABLE_TECH_BANK_INFO, $fieldList);
                //943
                $select->joinLeft(array("twc" => "tech_company_wo_count"), 
                        "TechBankInfo.TechID = twc.Tech_ID AND twc.Company_ID = '$companyID'",
                        array('TotalWosCompany')
                );
                //end 943
                
                // issue 13535
                if(!empty($PrimaryPhone)){
                    $select->where("(replace(replace(replace(replace(PrimaryPhone,' ',''),')',''),'(',''),'-','') like '$PrimaryPhone%' 
                            OR replace(replace(replace(replace(SecondaryPhone,' ',''),')',''),'(',''),'-','') like '$PrimaryPhone%' 
                            OR replace(replace(replace(replace(SMS_Number,' ',''),')',''),'(',''),'-','') like '$PrimaryPhone%')");
                }
                if (!empty($techsMatchingSearchCriteria))
                    $select->where("TechID IN ($techsMatchingSearchCriteria)");
                $select->order($orderCriteria);
                if ($sort == "LastName")
                    $select->order("FirstName");

                if ($proximitySearch) {
                    $select = Core_Database::whereProximity($select, $_SESSION["savedProximityInfo"][0], $_SESSION["savedProximityInfo"][1], "Latitude", "Longitude", "{$_SESSION["savedProximityInfo"][2]}.49", "Distance");
                }
                //print_r($select->__toString());
                //die();
                
                $searchResult = $db->fetchAll($select);
                
                $mapperOffset = (100 * ($currentResultPage - 1));
				$searchResultMapper = (!empty($woLatLongs)) ? $searchResult : array_slice($searchResult, $mapperOffset, 100);
				$searchResultMapperTotal = count($searchResult);
                $searchResult = array_slice($searchResult, $offset, $resultsPerPage);
              
              
                $tl = array();
                foreach ($searchResult as $t) {
                    $tl[] = $t["TechID"];
                }

                $ratingSearch = new Core_Api_Class;
                $rangeStats = $ratingSearch->get12MonthsCallStats($_SESSION['Auth_User']['login'], $_SESSION['Auth_User']['password'], $tl);
                $lifetimeStats =  $ratingSearch->getLifeTimeCallStats($_SESSION['Auth_User']['login'], $_SESSION['Auth_User']['password'], $tl);
				$certSearch = new Core_TechCertifications;
				$techCertification = $certSearch->getTechCertification($tl);
                if (sizeof($tl) == 1)
                {
                   if(empty($rangeStats[$tl[0]]))
                    $rangeStats = array($tl[0] => $rangeStats);
                }
				if (!$renderRowsOnly) {
                                    //735
	                $resultsHtml .= "<script type=\"text/javascript\" src=\"../library/jquery/jquery.bt.min.js\"></script>
                            <script type=\"text/javascript\" src=\"/library/techCallStats_SAT.js\"></script>
                            <script type=\"text/javascript\">
	                            jQuery(window).ready(function(){
	                                    jQuery(\".hoverText\").bt({
	                                            titleSelector: \"attr('bt-xtitle')\",
	                                            fill: 'white',
	                                            cssStyles: {color: 'black', fontWeight: 'bold', width: 'auto'},
	                                            animate:true
	                                    });
                                            
                                        jQuery(\".hoverTextnew\").bt({
                                            titleSelector: \"attr('bt-xtitle')\",
                                            fill: 'white',
                                            cssStyles: {color: 'black', width: '300px'},
                                            animate:true
                                        });
                                        $(\"#expertinfocontrols\").unbind('click').bind('click', function(){
                                            var content = \"<div id='comtantPopupID'><div style='text-align:center;height:380px;width:900px;background:url(/widgets/images/loader.gif) no-repeat center #fff;'>Loading...</div></div>\";
                                            $(\"<div></div>\").fancybox(
                                            {
                                                'showCloseButton' :true,
                                                'width' : 800,
                                                'height' : \"auto\",
                                                'content': content
                                            }
                                            ).trigger('click');

                                            $.ajax({
                                                type: \"POST\",
                                                url: \"/widgets/dashboard/popup/expert-list\",
                                                data: \"\",
                                                success:function( html ) {
                                                    var content = html;
                                                    $(\"#comtantPopupID\").html(content);
                                                }
                                            });
                                        });
                                            _global.eventOpenProfileTech();
	                            });
	                            </script>";
	                $resultsHtml .= "<table class=\"resultsTable\"><thead><tr>";
                //<td colspan=12 style='padding: 0px'><div id='resultsHeader'><div style='font-weight: bold;'><span style='font-size: 13px;'>SHOWING MY RECENT TECHS</span> <span style='font-style: italic;'>(filter to expand/narrow results)</span></div><div style='font-weight:bold;'><span style='color: #5091CB;font-weight:bold; font-size:20px;vertical-align:bottom;'>&#9733;</span><span style='color: #FFA500;'>Highlighted Technicians are Client Preferred</span></div></td></tr>";                

	                foreach ($resultColumnMap as $label => $map) {
	                    $map = explode("|", $map);
	                    $col = $map[0];
	                    /*                    if ($map[0] == "PreferencePercent") $col = "SATRecommendedAvg"; */
	                    $myClass = ($sort == $col ? "sortAbleSelected" : "sortAble");
                            if(  $col == 'FSexpert')
	                    {
                                $sortCode=($map[1] == 1 ? "class=\"$myClass\"" : "");
                                
                            }
	                    else if($col == 'ClientCredentials' || $col == 'PublicCredentials')
	                    {
	                        $sortCode = ($map[1] == 1 ? "class=\"$myClass\" onclick=\"sortBy('$col')\"" : "");
	                    }
	                    else
	                    {
	                    	if($renderAssignSelect){
	                    		$sortCode = ($map[1] == 1 ? "class=\"$myClass\" onclick=\"sortBy('$col',true)\"" : "");
	                    	}else{
	                    		$sortCode = ($map[1] == 1 ? "class=\"$myClass\" onclick=\"sortBy('$col')\"" : "");
	                    	}
	                    }
	                    $resultsHtml .= "<td $sortCode>$label</td>";
	                }
                        //end 735
	                $resultsHtml .= "</tr></thead><tbody>";
				}
                $rowNumber = 1;
                
				$searchResult = $renderAssignSelect == true ? $searchResultMapper : $searchResult;
		//943
                //die("ClientTechSearch_4");    

                $user = new Core_Api_User();
                $clientInfo = $user->getRepClientforCompany($companyID);
                $CompanyName = $clientInfo[0]["CompanyName"];
                //end 943
                //13910
                $client = new Core_Api_Class();
                $isGPM = $client->isGPMByUserName($this->_login);
                //end 13910
                foreach ($searchResult as $row) {
                    $rowColor = ($rowNumber % 2 == 0 ? "evenRow" : "oddRow");
                    // highlight preferred Tech
                    if ($row["PreferLevel"] == "Yes") {
                        $rowColor = "preferred";
                    }

                    $techID = $row["TechID"];

                    $techRangeStats = $rangeStats[$techID];

                    $techName = $row["FirstName"];
                    $techName .= "&nbsp;";
                    $techName .= $row["LastName"];
                    //13910
                    $TechNetLikes = Core_Tech::getTechNetLikes($techID);
                    $numNetLikes = $TechNetLikes['numNetLikes'];
                    $numComments = 0;
                    if($isGPM){
                        $numComments = $row['NumComments'];                        
                    } else {
                        $numComments = Core_Comments::getNumComments_ForTech_andNonGPMClient($techID,$this->_ClientID,$companyID);
                    }
                    //end 13910
                    $PrimaryPhoneStatus = 1;
                    $SecondaryPhoneStatus = 1;
                    if (!empty($techID)) {
                        
                        $result = $Core_Api_TechClass->getExts($techID);
                        if (!empty($result['TechID'])) {
                          
                            $PrimaryPhoneStatus = $result['PrimaryPhoneStatus'];
                            $SecondaryPhoneStatus = $result['PrimaryPhoneStatus'];
                        }
                    }
                    $contactInfo = $row["PrimaryPhone"];
                    if($PrimaryPhoneStatus == "1" && trim($row["PrimaryPhone"]) != "")
                    {    
                        $contactInfo .= "&nbsp;<img class=\"imgClassReQuitePhone\" align=\"absmiddle\" ";
                        $contactInfo .= " src=\"" . (($PrimaryPhoneStatus == "1") ? '/widgets/images/ico_phonex16.png' : '/widgets/images/ico_Notphonex16.jpg') . '"';
                        $contactInfo .= " title=\"" . (($PrimaryPhoneStatus == "1") ? 'Click to notify ' . $techName . ' that this Phone # is Invalid' : 'Invalid Phone # Reported') . '"';
                        $contactInfo .= ($PrimaryPhoneStatus == "1") ? " onclick=\"javascript:detailObject.onInit.RequitePhoneClick(this,'" . $techID . "' , 1,'" .base64_encode($techName) . "','".$row["PrimaryPhone"]."');\"" : " ";
                        $contactInfo .= " />";
                    }
                    $contactInfo .= "<br/>";  
                    $contactInfo .= $row["PrimaryEmail"];
                    $location = $row["City"];
                    $location .= ",&nbsp;";
                    $location .= $row["State"];
                    $location .= "<br/>";
                    $location .= $row["Zip"];

					if ($row["TotalWOs"] < ($lifetimeStats[$techID]['imacsMain'] + $lifetimeStats[$techID]['serviceMain'])) {
						$row["TotalWOs"] = $lifetimeStats[$techID]['imacsMain'] + $lifetimeStats[$techID]['serviceMain'];
						$row["PreferencePercent"] = $lifetimeStats[$techID]['recommendedAvg'];
						$row["SATRecommendedTotal"] = $lifetimeStats[$techID]['recommendedTotal'];
						$row["PerformancePercent"] = $lifetimeStats[$techID]['performanceAvg'];
						$row["SATPerformanceTotal"] = $lifetimeStats[$techID]['performanceTotal'];
						$row["Back_Outs"] = $lifetimeStats[$techID]['backOutMain'];
						$row["No_Shows"] = $lifetimeStats[$techID]['noShowMain'];
					}


                    $satRecTotal = trim($row["SATRecommendedTotal"], "`");
                    //$ratings = $row["PreferencePercent"];
                    $prefPercent = $satRecTotal == NULL ? "0% (0)" : number_format($row["PreferencePercent"], 0) . "% ($satRecTotal)";
                    $ratings = $prefPercent;
                    $ratings .= "<hr>";
                    $satPerfTotal = trim($row["SATPerformanceTotal"], "`");
                    $perfPercent = $satPerfTotal == NULL ? "0% (0)" : number_format(trim($row["PerformancePercent"], "`"), 0) . "% ($satPerfTotal)";
                    //$ratings .= $row["PerformancePercent"];
                    $ratings .= $perfPercent;

                    $resultsHtml .= "<tr class=\"$rowColor\">";

                    foreach ($resultColumnMap as $label => $mapping) {
                        $FSTagClass = new Core_Api_FSTagClass();
                        $FSTagNo = 0;
                        // Processes each column returned and outputs result. By default, column is outputed as it is formated by the the SQL query        
                        $mapping = explode("|", $mapping);
                        $mapping = $mapping[0];
                        $attr = "";
                        $style = "";
                        switch ($mapping) {
                            case "AssignSelect":
								$field = "<input type='radio' name='assignTechRadioBtn' value='$techID' onclick='assignTechToWo(this)' />";
								break;

                            case "PreferencePercent":
								$style="text-align:center";
                                $field = $ratings;
                                break;
                            /*
                              case "SATRecommendedAvg":
                              $total = trim($row["SATRecommendedTotal"], "`");
                              $field = $total == NULL ? "0.0 (0)" : number_format($row[$mapping], 1) . " ($total)";
                              break;
                              case "SATPerformanceAvg":
                              $total = trim($row["SATPerformanceTotal"], "`");
                              $field = $total == NULL ? "0.0 (0)" : number_format(trim($row[$mapping], "`"), 1) . " ($total)";
                              break;
                             */

                            case "PreferLevel":
                                $style = "min-width: 60px";
								$field = "<span style='white-space:nowrap'>";
                                if ($row['DellMRACompliant'] == 'Passed')
									$color = "<span><img src='/widgets/images/dellMRAPassed.png' class='hoverText' bt-xtitle='Dell MRA Compliant' /></span>";
                                else if ($row['DellMRACompliant'] == 'Lapsed') {
                                    $date = new Zend_Date($row['LapsedDate'], 'YYYY-mm-dd');
                                    if (!empty($date))
                                        $date = $date->toString('mm/dd/YYYY');
									else $date = "";
									$color = "<span><img src='/widgets/images/dellMRAPassed.png' class='hoverText' bt-xtitle='Dell MRA Compliance Lapsed: $date'/></span>";
                                }
                                if (!empty($row['DellMRACompliant']))
                                    $field .= $color;
								else $field = "<span style='white-space:nowrap'>&nbsp;";
                                $field .= "<hr>";
                                @$cert = $techCertification[$techID];
								$field .= "<div style='height: 19px'>";
                                if (sizeof($cert) > 0 && array_key_exists(2, $cert))
                                    $field .= "<span style='white-space:nowrap'><img src='/widgets/images/DeVry.png' style='vertical-align: text-bottom; height:13px' class='hoverText' bt-xtitle='DeVry Intern' /></span>";
								else $field .= "&nbsp;";
                                if (sizeof($cert) > 0 && array_key_exists(3, $cert))
                                    $field .= "<span style='margin-left:15px'>TBC</span>";
								$field .= "</div></span>";
                                break;
                            case "PreferLevel_USAuthorized":
                            //735
                            case "ClientCredentials":

                                $colnum = $isGPM?3:2;
                                $rownum = 0;
                                $colspan = $isGPM?2:1;
                                $isviewmore = false;
                                
                                $BlackBoxCablingStatus = $apiTechClass->GetBlackBoxCablingStatus($techID);
                                $BlackBoxTelecomStatus = $apiTechClass->getBlackBoxTelecomStatus($techID);
                                $isdisplayBlackBoxCabling = !empty($BlackBoxCablingStatus)?1:0;
                                $isdisplayBlackBoxTelecom = !empty($BlackBoxTelecomStatus)?1:0;
                                $isdisplayblackbox =0;
                                if($isGPM || in_array($companyID,array('BBAL','BBGC','BBGE','BBCN','BCSST','BBGN','BBJC','BBLC','BBNSG','BBNC','BBS','BB'))) {
                                    
                                    $isdisplayblackbox = 1;
                                }

                                $FSTagClass = new Core_Api_FSTagClass();
                                $extFilters = array();                                
                                if($isGPM) {
                                    $isOtherCompanyTag = 1;
                                    $callerIsGPM=1;
                                    $extFilters['VisibilityId']=4;
                                } else {
                                    $isOtherCompanyTag = 0;
                                    $callerIsGPM=0;
                                    $extFilters['VisibilityId']=1;
                                }
                                 //931
                                $extFilters['SortByTitle']=1;
                                //end 931
                                //839
                                $getFSTagList = $FSTagClass->getFSTagList_forTech_forCompany($techID,$companyID,$isOtherCompanyTag,1,$callerIsGPM,null,1,$extFilters);
                                
                                $getFSTagListNew = array();
                                foreach($getFSTagList as $elementKey => $element)
                                {
                                    if($getFSTagList[$elementKey]['Name']=="BlackBoxCabling" && $BlackBoxCablingStatus['Status'] != 'Pending')
                                    {
                                        
                                        if($isdisplayblackbox==1)
                                        {
                                            if($isdisplayBlackBoxCabling==1 )
                                            {
                                                 
                                                $getFSTagListNew[] = $getFSTagList[$elementKey];
                                            }
                                        }
                                    }
                                    else if($getFSTagList[$elementKey]['Name']=="BlackBoxTelecom" && $BlackBoxTelecomStatus['Status'] != 'Pending')

                                    {
                                        
                                        if($isdisplayblackbox==1)
                                        {
                                            if($isdisplayBlackBoxTelecom==1 )
                                        {
                                                $getFSTagListNew[] = $getFSTagList[$elementKey];
                                        }

                                        }
                                    }
                                    else if(!empty($getFSTagList[$elementKey]['TechId']))
                                    {
                                        $getFSTagListNew[] = $getFSTagList[$elementKey];
                                    }
                                }
                                $FSTagNo = count($getFSTagListNew);
                                
                                $rownum = 4;
                                if($FSTagNo>12)
                                {
                                    $isviewmore = true;
                                }
                                $field = "";
                                if($isGPM)
                                {
                                    
                                    $field .= "<table cellspacing='0' style=\"text-align:left;\" width=\"111px\">";
                                    $field .= "<colgroup>";
                                    $field .= "<col width=\"33%\">";
                                    $field .= "<col width=\"33%\">";
                                    $field .= "<col width=\"33%\">";
                                    $field .= "</colgroup>";
                                }
                                else
                                {
                                    $field .= "<table cellspacing='0' style=\"text-align:left;\" width=\"74px\">";
                                    $field .= "<colgroup>";
                                    $field .= "<col width=\"50%\">";
                                    
                                    $field .= "<col width=\"50%\">";
                                    $field .= "</colgroup>";
                                }
                                $k=0;
                                //912
                                $defaultnotedit = array('ErgoMotionCertifiedTechnician'=>'ErgoMotionCertifiedTechnician',
                                    'TechForceICAgreement'=>'TechForceICAgreement',
                                    'Dell_MRA_Compliant'=>'Dell_MRA_Compliant',
                                    'BootCampCertified'=>'BootCampCertified',
                                    'FSMobile'=>'FSMobile');
                                //end 912
                                for($i=0;$i<$rownum;$i++)
                                {
                                    $field .= "<tr style='text-align:center;'>";
                                    for($j=0;$j<$colnum;$j++)
                                    {
                                        if($i==$rownum-1 && $isviewmore && $j>0)
                                        {
                                            //A1
                                            $field .= "<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;' colspan='$colspan'>";
                                            $field .= "<a href='wosViewTechProfile.php?simple=0&TechID=$techID&v=$companyID&C=1' style='text-decoration:underline;' class='cmdOpenTechProfileShow'>See More...</a>";
                                            $field .= "</td>";
                                            break;
                                        }
                                        $field .= "<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
                                        if(empty($getFSTagListNew[$k]))
                                        {
                                            $field .= "<div style='height:19px;'>&nbsp;</div>";
                                        }
                                        else
                                        {
                                            if($getFSTagListNew[$k]['Name']=="BlackBoxTelecom")
                                            {
                                                 
                                                $BlackBoxTelecomdate = $BlackBoxTelecomStatus['Date'];
                                                if (!empty($BlackBoxdate))
                                                {
                                                    $strBlackBoxTelecomDate = date_format(new DateTime($BlackBoxTelecomdate), "m/d/Y");
                                                } else
                                                {
                                                    $strBlackBoxTelecomDate = "";
                                                }

                                                if ($BlackBoxTelecomStatus['CertID'] == "49")
                                                {
                                                    $BlackBoxTelecomimg = "BTT1.png";
                                                    $BlackBoxTelecomStatus = "Black Box Telecom - " . $BlackBoxTelecomStatus['LevelText'] . ": " . $strBlackBoxTelecomDate;
                                                } else if ($BlackBoxTelecomStatus['CertID'] == "50")
                                                {
                                                    $BlackBoxTelecomimg = "BTT2.png";
                                                    $BlackBoxTelecomStatus = "Black Box Telecom - " . $BlackBoxTelecomStatus['LevelText'] . ": " . $strBlackBoxTelecomDate;
                                                }

                                                $field .= "<img id='BlackBoxCablingimgtag' style='" . ($BlackBoxTelecomStatus['Status'] != 'Pending' ? "display:'';" : "display:none;") . "' height='19px' bt-xtitle='" . $BlackBoxTelecomStatus . "' class='hoverText' src='/widgets/images/" . $BlackBoxTelecomimg . "' title=''/>";
                                            }
                                            else if($getFSTagListNew[$k]['Name']=="BlackBoxCabling" )
                                            {
                                                
                                                $BlackBoxdate = $BlackBoxCablingStatus['Date'];
                                                if (!empty($BlackBoxdate))
                                                {
                                                    $strBlackBoxDate = date_format(new DateTime($BlackBoxdate), "m/d/Y");
                                                } else
                                                {
                                                    $strBlackBoxDate = "";
                                                }
                                                if ($BlackBoxCablingStatus['CertID'] == "46")
                                                {
                                                    $BlackBoximg = "BTC1.png";
                                                    $BlackBoxStatus = "Black Box Cabling - " . $BlackBoxCablingStatus['LevelText'] . ": " . $strBlackBoxDate;
                                                } else if ($BlackBoxCablingStatus['CertID'] == "47")
                                                {
                                                    $BlackBoximg = "BTC2.png";
                                                    $BlackBoxStatus = "Black Box Cabling - " . $BlackBoxCablingStatus['LevelText'] . ": " . $strBlackBoxDate;
                                                } else if ($BlackBoxCablingStatus['CertID'] == "48")
                                                {
                                                    $BlackBoximg = "BTC3.png";
                                                    $BlackBoxStatus = "Black Box Cabling - " . $BlackBoxCablingStatus['LevelText'] . ": " . $strBlackBoxDate;
                                                } 

                                                $field .= "<img id='BlackBoxCablingimgtag' style='" . ($BlackBoxCablingStatus['Status'] != 'Pending' ? "display:'';" : "display:none;") . "' height='19px' bt-xtitle='" . $BlackBoxStatus . "' class='hoverText' src='/widgets/images/" . $BlackBoximg . "' title=''/>";
                                            }
                                            else
                                            { 
                                                 
                                                if(empty($getFSTagListNew[$k]['Date']))
                                                {
                                                    $date = "";
                                                }
                                                else
                                                {
                                                    $date = new Zend_Date($getFSTagListNew[$k]['Date'], 'YYYY-mm-dd'); 
                                                }

                                                if(!empty($getFSTagListNew[$k]['TagArt']))
                                                {
                                                    $TagArt = "https://s3.amazonaws.com/wos-docs/tags/". str_replace ("|", "", $getFSTagListNew[$k]['TagArt']);
                                                }
                                                else
                                                {
                                                    $TagArt = "/widgets/images/check_green.gif";
                                                }

                                                if(empty($getFSTagListNew[$k]['ExistingImagePath']))
                                                {
                                                    $ExistingImagePath = $TagArt;
                                                }
                                                else
                                                {
                                                    $ExistingImagePath = $getFSTagListNew[$k]['ExistingImagePath'];
                                                }
                                                //931
                                                $HasMultiTagLevels="";
                                                if($getFSTagListNew[$k]['HasMultiTagLevels']==1 && !empty($getFSTagListNew[$k]['TagLevelsData']))
                                                {      
                                                    $HasMultiTagLevels = ": L".$getFSTagListNew[$k]['LevelOrder']."&nbsp;-&nbsp;".$getFSTagListNew[$k]['LevelTitle'];
                                                }
                                                $Title = $getFSTagListNew[$k]['Title'];
                                                if (!empty($date))
                                                {
                                                    $date = ": ".$date->toString('mm/dd/YYYY');
                                                }
                                                else
                                                {
                                                    $date = "";
                                                }
                                                $field .= "<img src='$ExistingImagePath' class='hoverText' bt-xtitle='$Title$HasMultiTagLevels$date' height='19px' />";
                                                //end 931
                                            }
                                        }
                                        $field .= "</td>";
                                        $k++;
                                    }
                                    $field .= "</tr>";
                                }
                                $field .= "</table>";
                                break;
                                //end 839
                            case "PublicCredentials":
                                $cert = $techCertification[$techID];
                                $apiexts = new Core_Api_TechClass;
                                //background check
                                $field ="<table cellspacing='0' cellpadding='0' width='74px'>";
                                $field .="<colgroup><col width=\"50%\"><col width=\"50%\"></colgroup>";
                                $field .="<tr style=\"text-align:center;\">";
                                //Background Check
                                $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
                                if ($row["Bg_Test_Pass_Lite"] == "Pass" && $row["Bg_Test_Pass_Full"] == "Pass") {
                                    $basicDate = strtotime($row['BG_Test_ResultsDate_Lite']);
                                    //14108
                                     if(!empty($basicDate) && $basicDate > 0){    
                                    $basicDate = date("m/d/Y", $basicDate);
                                     }else{
                                        $basicDate = '';
                                     }
                                     //End 14108
                                    $compDate = strtotime($row['BG_Test_ResultsDate_Full']);
                                    //14108
                                    if(!empty($compDate) && $compDate > 0){    
                                    $compDate = date("m/d/Y", $compDate);
                                     }else{
                                       $compDate = '';
                                     }
                                     //End 14108
                                    $field .= "<img src=\"/widgets/images/BCPlus.png\" height=\"19px\" class=\"hoverText\" bt-xtitle=\"Comprehensive Background Check - {$compDate}\" style=\"margin-left: 2px;\"/>";
                                                 //A1
                                } elseif ($row["Bg_Test_Pass_Lite"] == "Pass" && $row["Bg_Test_Pass_Full"] != "Pass") {
                                    $basicDate = strtotime($row['BG_Test_ResultsDate_Lite']);
                                    //14108
                                     if(!empty($basicDate) && $basicDate > 0){    
                                    $basicDate = date("m/d/Y", $basicDate);
                                     }else{
                                       $basicDate = '';
                                     }
                                     //End 14108
                                    $field .= "<img src=\"/widgets/images/BC.png\" height=\"19px\" class=\"hoverText\" bt-xtitle=\"Basic Background Check - {$basicDate}\" />";
                                    
                                } else {
                                    $field .= "<div style='height:19px;'>&nbsp;</div>";
                                }
                                $field .="</td>";
                                
                                //gold drug test
                                $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
                                $img="&nbsp;";
                                if ($row["DrugPassed"] == 1)
                                {
                                    $Date = strtotime($row["DatePassDrug"]);
                                    //14108
                                    if(!empty($Date) && $Date > 0){
                                    $Date = date("m/d/Y", $Date);
                                    }else{
                                      $Date = '';
                                    }
                                    //End 14108
                                    $img = "dt9.png";
                                    $title ="Gold Drug Test (9 Panel)";
                                     
                                }
                                else if ($row["SilverDrugPassed"] == 1)
                                {
                                    $Date = strtotime($row["DatePassedSilverDrug"]);
                                    //14108
                                      if(!empty($Date) && $Date > 0){
                                    $Date = date("m/d/Y", $Date);
                                      }else{
                                        $Date = '';
                                      }
                                      //End 14108
                                    $img = "dt5.png";
                                    $title ="Silver Drug Test (5 Panel)";
                                    
                                }
                                else
                                {
                                    $Date=null;
                                    $title=null;
                                }
                                
                                if($img!="&nbsp;")
									{
                                    $field .="<img src='/widgets/images/$img' height='19px' class='hoverText' bt-xtitle='$title &ndash; {$Date}' />";
                                }
                                else
										{
                                    $field.="<div style='height:19px;'>&nbsp;</div>";
                                }
                                $field .="</td>";
                                $field .="</tr>";
                                $field .="<tr style=\"text-align:center;\">";
                                //dell
                                $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
                                if(!empty($row['DellMRACompliant']))
                                {
                                    if ($row['DellMRACompliant'] == 'Passed')
                                    {
					$field .= "<img src='/widgets/images/dellMRAPassed.png' height='19px' class='hoverText' bt-xtitle='Dell MRA Compliant' />";
                                    }
                                    else if ($row['DellMRACompliant'] == 'Lapsed')
                                    {
                                        $date = new Zend_Date($row['LapsedDate'], 'YYYY-mm-dd');
                                        if (!empty($date))
                                        {
                                            $date = ": ".$date->toString('mm/dd/YYYY');
                                        }
                                        else
                                        {
											$date = "";
										}
                                        
                                        $field .= "<img src='/widgets/images/dellMRAPassed.png' height='19px' class='hoverText' bt-xtitle='Dell MRA Compliance Lapsed $date'/>";
                                    }
                                }
										else
										{
                                    $field.="<div style='height:19px;'>&nbsp;</div>";
										}
                                $field .="</td>";
                                
                                //FS-mobil
                                $FSmobilimg="&nbsp;";
                                $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
                                if(!empty($row['FSMobileDate']))
										{
                                    $Date = strtotime($row['FSMobileDate']);
                                    //14108
                                     if(!empty($Date) && $Date > 0){    
                                    $Date = date("m/d/Y", $Date);
                                     }else{
                                         $Date = '';
                                     }
                                    //End 14108
                                    $FSmobilimg = "fs_mobile_36x32.png";
                                    $title ="I Have FS-Mobile";
										}
										else
										{
                                    $Date=null;
                                    $title=null;
                                }
                                if($FSmobilimg!="&nbsp;")
                                {
                                    $field .="<img src='/widgets/images/$FSmobilimg' height='19px' class='hoverText' bt-xtitle='$title &ndash; {$Date}' />";
                                }
                                else
                                {
                                    $field.="<div style='height:19px;'>&nbsp;</div>";
                                }
                                $field .="</td>";
                                $field .="</tr>";
                                $field .="<tr style=\"text-align:center;\">";
                                //Blue Ribbon Technician
                                $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
                                if (sizeof($cert) > 0 && array_key_exists(4, $cert))
                                {
                                    $TBCCertInfo = $apiexts->getTechCertInfo(4,$row["TechID"]);
                                    if(!empty($TBCCertInfo['date']))
                                    {
                                        $Date = split('-',$TBCCertInfo['date']);
                                        $Datestr = ": ".$Date[1]."/".$Date[2]."/".$Date[0];
                                    }
                                    else
                                    {
                                        $Datestr="";
                                    }
                                     
                                    $field .= "<img src=\"/widgets/images/BRT_Tag.png\" height='19px' class=\"hoverText\" bt-xtitle=\"Blue Ribbon Technician$Datestr\" />";
										}
                                else
                                { 
                                    $field.="<div style='height:19px;'>&nbsp;</div>";
                                }
                                $field .="</td>";

                                //boot camp
                                $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
                                if (sizeof($cert) > 0 && array_key_exists(3, $cert))
										{
                                    $BootCampCertInfo = $apiexts->getTechCertInfo(3,$row["TechID"]);
                                    if(!empty($BootCampCertInfo['date']))
                                    {
                                        $BootCampdate = split('-',$BootCampCertInfo['date']);
                                        $BootCampdatestr = ": ".$BootCampdate[1]."/".$BootCampdate[2]."/".$BootCampdate[0];
										}
										else
										{
                                        $$TBCCertInfodatestr="";
										}
                                          //A1
                                    $field .= "<img src=\"/widgets/images/Boot_Tag.png\" height='19px' class=\"hoverText\" bt-xtitle=\"Boot Camp Certified$BootCampdatestr\" />";
                                }
                                else if (sizeof($cert) > 0 && array_key_exists(43, $cert))
                                {
                                    $BootCampplusCertInfo = $apiexts->getTechCertInfo(43,$row["TechID"]);
                                    if(!empty($BootCampplusCertInfo['date']))
                                    {
                                        $BootCampplusdate = split('-',$BootCampplusCertInfo['date']);
                                        $BootCampplusdatestr = ": ".$BootCampplusdate[1]."/".$BootCampplusdate[2]."/".$BootCampplusdate[0];
                                    }
                                    else
                                    {
                                        $BootCampplusdatestr="";
                                    }
									
                                    $field .= "<img src=\"/widgets/images/Boot_Tag_PLUS.png\" height='19px' class=\"hoverText\" bt-xtitle=\"Boot Camp Plus$BootCampplusdatestr\" />";
                                }
										else
                                { 
                                    $field.="<div style='height:19px;'>&nbsp;</div>";
                                }
                                $field .="</td>";
                                $field .="</tr>";
                                $field .="<tr style=\"text-align:center;\">";
                                //I9
                                $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
                                $apiTechClass = new Core_Api_TechClass();
                                $extsUSAuth = $apiTechClass->getExts($techID);
                                $i9img="&nbsp;";
                                if ($extsUSAuth["I9Status"] == 2)
                                {
                                    $title="Authorized to perform services in the United States";
                                    $Date = strtotime($extsUSAuth['I9Date']);
                                    //14108
                                      if(!empty($Date) && $Date > 0){    
                                    $Date = " &ndash; ".date("m/d/Y", $Date);
                                      }else{
                                        $Date = '';
                                      }
                                    //End 14108
                                    $i9img="VerifiedTall2.png";
                                }
                                else if($extsUSAuth["I9Status"] == 1)
                                {
                                    $title="Authorization to perform services in the United States is pending";
                                    $Date = "";
                                    $i9img="PendingTall2.png";
                                }

                                if($i9img!="&nbsp;")
                                {
                                    $field .="<img src='/widgets/images/$i9img' height='19px' class='hoverText' bt-xtitle='$title {$Date}' />";
								}
                                else
                                {
                                    $field.="<div style='height:19px;'>&nbsp;</div>";
                                }
                                $field .="</td>";
                                        
                                //ISO
                                $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
                                if($row["ISO"]=="Yes")
                                    {
                                    $field .="<img src='/widgets/images/iso-2.png' height='19px' class='hoverText' bt-xtitle='This technician is a member of an Independent Service Organization &#40;ISO&#41;' />";
                                    }
                                else
                                {
                                    $field.="<div style='height:19px;'>&nbsp;</div>";
                                }
                                $field .="</td>";
                                $field .="</tr>";
                                $field .="</table>";
                                break;
                            case "FSexpert":
                                //FS-Experts 
                                $apiFSExpert = new Core_Api_FSExpertClass();
                                $FSExperts = $apiFSExpert->getFSExpertListForTech($row["TechID"]);
                                $field ="<table cellspacing='0' cellpadding='0' width='100%'>";
                                $field .="<colgroup>";
                                $field .="<col width=\"20%\">";
                                $field .="<col width=\"20%\">";
                                $field .="<col width=\"20%\">";
                                $field .="<col width=\"20%\">";
                                $field .="<col width=\"20%\">";
                                $field .="</colgroup>";
                                $k=1;
                                for ($i = 0; $i < 4; $i++)
                                {
                                    $field .="<tr>";
                                    for ($j = 0; $j < 6; $j++)
                                    {
                                        if($j==5)
                                        {
                                            $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD; display:none;'>";
                                        }
                                        else
                                        {
                                        $field .="<td style='padding: 2px 2px;border-bottom:1px solid #DDDDDD;border-right:1px solid #DDDDDD;'>";
                                        }
                                        
                                        if (sizeof($FSExperts) > 0 && array_key_exists($k, $FSExperts))
                                        {
                                            if (!empty($FSExperts[$k]["Title"]))
                                            {
                                                $title = "FS-Expert: <b>".$FSExperts[$k]["Title"]."</b><div style=\"padding-left:15px;padding-top:10px;\"><ul><li style=\"list-style:disc outside none;\">".$FSExperts[$k]["Content"]."</li></ul></div>";
                                            }
                                            else
                                            {
                                            $title = "" ;
                                            }
                                            $field .="<img src='/widgets/images/expert/".$FSExperts[$k]["Abbreviation"].".png' height='19px' k='$k' class='hoverTextnew'  bt-xtitle='$title' />";
                                        } else
                                        {
                                            $field.="<div style='height:19px;width:30px;'>&nbsp;</div>";
                                        }
                                $field .= "</td>";
                                        $k++;
                                    }
                                    $field .= "</tr>";
                                }
                                $field .="</table>";
                                break;
                            //735
                            case "RatingBox":
                                //13910
                                $style = "text-align: left;padding: 0pt 5px;"; 
                                $rating = $techRangeStats['recommendedAvg'];
                                $numberRankings = $techRangeStats['recommendedTotal'];
                                if ($rating == NULL) {
                                    $rating = "";
                                    $numberRankings = 0;
                                }

                                $color = "#AAAAAA";

                                $callsSumm = $techRangeStats['imacsMain'] + $techRangeStats['serviceMain'];
                                if ($callsSumm == 0) {
                                    $color = "#AAAAAA";
                                }

                                if ($callsSumm >= 5) {
                                    $color = "#FFFF00";
                                }

                                if ($techRangeStats['noShowMain'] >= 2 || ($callsSumm > 0 && !empty($techRangeStats['recommendedAvg']) && $techRangeStats['recommendedAvg'] <= 90 && !empty($techRangeStats['performanceAvg']) && $techRangeStats['performanceAvg'] <= 90)) {
                                    $color = "#FF0000";
                                } else if ($callsSumm >= 10 && !empty($techRangeStats['recommendedAvg']) && $techRangeStats['recommendedAvg'] >= 95 && !empty($techRangeStats['performanceAvg']) && $techRangeStats['performanceAvg'] >= 95 && $techRangeStats['backOutMain'] < 6 && $techRangeStats['noShowMain'] < 3) {
                                    $color = "#00FF00";
                                }
                                $NetLikescolor = '';
                                $imageThumbs = '/widgets/images/thumbs-up.png';//14027
                                if($numNetLikes < 0){
                                    $NetLikescolor = 'color:red';
                                    $imageThumbs = '/widgets/images/thumbs-down.png';//14027
                                }
                                
                                $field ="<table width='50px'>
                                            <tr valign='middle' height='17' style='text-align:center;border-right:0px'>
                                                <td style='text-align:center;padding:0px;width:15px;'>
                                                    <div id=\"myRating$techID\" class=\"ratingBox\" style=\"background-color: $color; width: 10px; height: 10px;margin-left:3px;\"></div>
                                                    <script type=\"text/javascript\">colorCode = document.getElementById(\"myRating$techID\");colorCode.rating = \"$rating\";colorCode.numRating = \"$numberRankings\";</script>
                                                </td>
                                                <td style='text-align:center;padding:0px;'></td>
                                            </tr>
                                            <tr valign='middle' height='17' style='text-align:center;'>
                                                <td style='text-align:center;padding:0px;width:15px;'>
                                                    <a class='cmdOpenTechProfileShow' href='wosViewTechProfile.php?simple=0&TechID=$techID&v=$companyID&cl=1' title='Add/View Comments'>
                                                        <img src='$imageThumbs' height='16' />         
                                                    </a>
                                                </td>
                                                <td style='text-align:center;padding:0px;'>
                                                    <a style='$NetLikescolor' class='cmdOpenTechProfileShow' href='wosViewTechProfile.php?simple=0&TechID=$techID&v=$companyID&cl=1' title='Add/View Comments'>
                                                        ".$numNetLikes."
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr valign='middle' height='17' style='text-align:center;'>
                                                <td style='text-align:center;padding:0px;width:15px;'>
                                                    <a class='cmdOpenTechProfileShow' href='wosViewTechProfile.php?simple=0&TechID=$techID&v=$companyID&cl=1' title='Add/View Comments'>
                                                        <img src='/widgets/images/Commentbubble_icon.png?t=1' />
                                                    </a>
                                                </td>
                                                <td style='text-align:center;padding:0px;'>
                                                    <a class='cmdOpenTechProfileShow' href='wosViewTechProfile.php?simple=0&TechID=$techID&v=$companyID&cl=1' title='Add/View Comments'>
                                                        ".$numComments."
                                                    </a>
                                                </td>
                                            </tr>
                                        </table>";
                                //end 13910 , 14027
                                
                                break;
                            case "TechID":
                                /* <img src=\"/widgets/images/calender.png\" /> */
                            	if($renderAssignSelect){
                            		$field = "<a href='javascript:void(0);' onclick='techMapper.showTechProfile(\"$techID\");' target=\"_self\">$techID</a><br/><a id='techScheduleLnk' href=\"javascript:void(0);\" onclick=\"schedule.show($(this),null,{data:{tech_ID:'$techID',date:'',date_interval:'all',display_calendar:1,display_date:1,sort:'starttime',sort_dir:'desc'}});\">Schedule</a>";
                            	}else{
                            		$field = "<a class='cmdOpenTechProfileShow' href=\"wosViewTechProfile.php?simple=0&TechID=$techID&v={$companyID }&search={$encodedQS}\" target=\"_self\">$techID</a><br/><a id='techScheduleLnk' href=\"javascript:void(0);\" onclick=\"schedule.show($(this),null,{data:{tech_ID:'$techID',date:'',date_interval:'all',display_calendar:1,display_date:1,sort:'starttime',sort_dir:'desc'}});\">Schedule</a>";
                            		                            	}
                                break;
                            case "FS_Cert_Test_Pass":
                                if ($row["FS_Cert_Test_Pass"] == 1) {
                                    $color = "#00FF00";
                                    $title = "FS Certified";
                                } else {
                                    $color = "#AAAAAA";
                                    $title = "Not FS Certified";
                                }
                                $field = "<div style=\"background-color: $color; width: 10px; height: 10px; margin: 0px auto\" title=\"$title\"></div>";
                                break;
                            case "Bg_Test_Pass_Lite":
                                break;
                            case "DrugPassed":
                                break;
                            case "No_Shows":
                            case "Back_Outs":
								$style="text-align:center;"; 
                                if ($row['No_Shows'] != "") {
                                    $field = "<span>" . $row['No_Shows'] . "</span>";
                                } else {
                                    $field = "<span style='height: 19px;display:block;'></span>";
                                }
                                $field .= "<hr>";
                                if ($row['Back_Outs'] != "") {
                                    $field .= "<span>" . $row['Back_Outs'] . "</span>";
                                } else {
                                    $field = "<span style='height: 19px;display:block;'></span>";
                                }
                                break;
                            case "FLSCSP_Rec":
                                if ($row["FLSCSP_Rec"] == 1) {
                                    $field = "Yes";
                                } else {
                                    $field = "No";
                                }
                                break;
                            case "SpecialDistance":
								$style="text-align:center;";
                                $field = ($proximitySearch ? round($row["Distance"], 0) : "-");
                                break;
                            //735
                            case "LastName":
                                $field = "<span style='font-weight:bold;' id='techName_{$techID}'>".$techName."</span><br/>";
                                $field .= $contactInfo;
                                break;
//                            case "PrimaryPhone":
//                                $field = $contactInfo;
//                                break;
                            //end 735
                            case "City":
                                $field = $location;
                                break;
                            case "TotalWOs":
                                 //943
                                $style="text-align:center;";   
                                $field = "<span title='All FieldSolutions Work Orders'>".$row[$mapping]."</span>";
                                $field .= "<hr/>";
                                $field .= "<span title='All ". $CompanyName." Work Orders'>".(empty($row["TotalWosCompany"])?"0":$row["TotalWosCompany"])."</span>";
                                if ($field == NULL)
                                    $field = "";
                                break;
                                //end 943
                            default:
                                $field = $row[$mapping];
                                if ($field == NULL)
                                    $field = "";
                                break;
                        }
                      
                        $resultsHtml .= "<td style='$style'>$field</td>";
                    }
                    $resultsHtml .= "</tr>";
                    $rowNumber++;
                }
				if (!$renderRowsOnly)
                	$resultsHtml .= "</tbody>";
                // construct results page footer
				if ($renderPagination) {
                	$prevPageBtn = ($currentResultPage > 1 ? "<span id=\"prevPageBtn\" onclick=\"prevPage()\">&lt;</span>" : "");
	                $numPages = ceil($numResult / $resultsPerPage);
	                $nextPageBtn = ($currentResultPage < $numPages ? "<span id=\"nextPageBtn\" onclick=\"nextPage()\">&gt;</span>" : "");
	                $pageSelect = "<select id='pageSelect' name='pageSelect' style='width: 5em; text-align: center' onChange='gotoPage(this.selectedIndex + 1);'>";
	
	                for ($i = 1; $i <= $numPages; $i++) {
	                    $start = 1 + (($i - 1) * $resultsPerPage);
	                    $pageSelect .= "<option value='$i'" . ($currentResultPage == $i ? " selected='selected' " : "") . ">" . $start . " - " . min($start + $resultsPerPage, $numResult) . "</option>";
                        
	                }
	
	                $pageSelect .= "</select>";
	                $resultsHtml .= "<tfoot><tr><td colspan=\"" . sizeof($resultColumnMap) . "\">$prevPageBtn Records " . $pageSelect . " of $numResult $nextPageBtn</td></tr></tfoot>";
                     
				}
				if (!$renderRowsOnly)
					$resultsHtml .= "</table>";
            } else {
                $resultsHtml = "<div class=\"noResultsDiv\">No Results Found</div>";
            }//end if numResult
        }//end if doSearch

        $this->view->resultsHtml = $resultsHtml;
		
		if (json_encode($resultsHtml) == 'null') {
                $resultsHtml = iconv("ISO-8859-1//IGNORE", "UTF-8", $resultsHtml);
        }
          
        $returnArr = array("listView"=>$resultsHtml, "mapView"=>$searchResultMapper, "mapViewTotal"=>($mappedWosLimit != null && $mappedWosLimit != "") ? $mappedWosLimit : $searchResultMapperTotal);

        if ($request->getParam("sb") != "" || $request->getParam("snp") != "" || $request->getParam("sort") != "" || $request->getParam("order") != "" || $request->getParam("page") != "") {
            // parse querystring
            $arr = Array();
            $ignoreGet = array("sb" => 1, "snp" => 1, "sort" => 1, "order" => 1, "page" => 1, 'autoSubmit' => 1);
              
            foreach ($params as $key => $val) {
                if (array_key_exists($key, $ignoreGet))
                    continue;
                $arr[] = "$key=$val";
            }
            $qs = implode("&", $arr);
        }else {
            $qs = $_SERVER['QUERY_STRING'];
        }
        
        //}//end 
        
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $return = json_encode($returnArr);

        $response->setHeader('Content-Length', strlen($return), true);
        $response->setBody($return);
    }

    public function ajaxUploadFileAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $company = $this->view->company;

        $maxFileSize = 52428800; //50Mb

        $request = $this->getRequest();
        //$id = 'Company_Logo';
        $id = $request->getParam('file', 'Company_Logo');
        $descr = $request->getParam('descr', NULL);

        $return = array('success' => 1);
        
        if (!empty($_FILES[$id])) {
            if (
                    is_readable($_FILES[$id]['tmp_name']) &&
                    $_FILES[$id]['error'] == UPLOAD_ERR_OK &&
                    filesize($_FILES[$id]['tmp_name']) <= $maxFileSize
            ) {
                $uploader = new Core_Api_User();

		        if ($id == "CustomSignOff") {
			    	require_once ("../htdocs/library/fpdf/fpdf.php");
			    	require_once ("../htdocs/library/fpdi/fpdi.php");

			    	$fpdf = new FPDI ();
			    	$pagecount = $fpdf->setSourceFile($_FILES[$id]['tmp_name']);

			    	if ($pagecount <= 0) {
			    		$return["success"] = 0;
		                $return['errors'] = array("Custom Signoff must be an unencrypted PDF");
			    	}
		        }

		        if ($return["success"] == 1) {
	                $fname = $_FILES[$id]['name'];
	                if (strrpos($fname, '/') !== FALSE) {
	                    $fname = substr($fname, strrpos($fname, '/'));
	                }

	                $info = pathinfo($fname);
	                $fname = uniqid($info['filename'] . '_');
	                $fname = $fname . '.' . $info['extension'];
	                $fdata = file_get_contents($_FILES[$id]['tmp_name']);

	                $result = $uploader->uploadFile(trim($_SESSION['UserName']) . "|pmContext=$company", $_SESSION['Password'], $fname, $fdata, $id);
                        //970
                        if ($id == "Company_Logo") {
                        
                        $user = new Core_Api_User();
                        $search = new Core_Api_Class();
                        $authData = array('login' => trim($_SESSION['UserName']),'password' => $_SESSION['Password']);
                        $clientInfo = $user->loadWithExt($authData,false);            
                        $updateFields['Company_Logo'] = $fname;
                        $result_1 = $search->updateCompanyProfile($clientInfo->Company_ID,$updateFields);
                        }
                        //end 970

                        $return['success'] = 0;
	                $return['errors'] = array();

	                if (!empty($result->errors)) {
	                    $return['success'] = 0;
	                    $return['errors'] = array();
	                    foreach ($result->errors as $e) {
	                        $return['errors'][] = $e->message;
	                    }
	                } else {
	                    $return['success'] = 1;
	                }
		        }
            } else {
                $return['success'] = 0;
                $return['errors'] = array();
                if (empty($id)) {
                    $return['errors'][] = 'Incorrect parameters: ID is required and cannot be empty';
                } else if (empty($_FILES["{$id}"])) {
                    //$return['errors'][]= 'File not uploaded';
                } else if (!is_readable($_FILES["{$id}"]['tmp_name'])) {
                    $return['errors'][] = 'Uploaded file not readabale';
                } else if (filesize($_FILES["{$id}"]['tmp_name']) > $maxFileSize) {
                    $return['errors'][] = 'Uploaded file size too big';
                } else if ($_FILES["{$id}"]['error'] != UPLOAD_ERR_OK) {
                    switch ($_FILES["{$id}"]['error']) {
                        case UPLOAD_ERR_FORM_SIZE :
                            $return['errors'][] = 'Uploaded file size too big';
                            break;
                        case UPLOAD_ERR_PARTIAL :
                            $return['errors'][] = 'The uploaded file was only partially uploaded';
                            break;
                        case UPLOAD_ERR_NO_FILE :
                        case UPLOAD_ERR_NO_TMP_DIR :
                        case UPLOAD_ERR_CANT_WRITE :
                        case UPLOAD_ERR_EXTENSION :
                            $return['errors'][] = 'No file was uploaded';
                    }
                }
            }
        }

        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-Length', strlen($return), true);
        $response->setBody($return);
    }

    public function userCreateAction() {
        $errorMessages = new Core_Form_ErrorHandler('createClient');
        $this->view->messages = $errorMessages->getMessages();
        $company_id = $this->getRequest()->getParam('Company_ID', null);
        $this->view->Company_ID = (!empty($company_id) ) ? $company_id : $this->view->company;
        $errorMessages->clearMessages();
    }

    public function doUserCreateAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $errorMessages = new Core_Form_ErrorHandler('createClient');
        $request = $this->getRequest();
        $client_id = $this->getRequest()->getParam('client_id', 0);
        //$createForm = $request->getParam('CreateForm', null);
        $createForm = $this->getRequest()->getParams();
        @$this->_company = $createForm['Company_ID'];
		$filterArr = array("module", "controller", "action","client_id");
		foreach($createForm as $k=>$v){
			if(in_array($k,$filterArr)){
				unset($createForm[$k]);
			}
		}
        
        if (empty($createForm))
            $errorMessages->addMessage('Create Client Form is empty');
        $errorMessages->clearMessages();


        if(isset($createForm['UserType']) &&  $createForm['UserType']=='Admin') 
        {
            $createForm['Admin']=1;
        }            
        if(isset($createForm['Admin']) &&  $createForm['Admin']==1) 
        {
            $createForm['UserType']='Admin';
        }                
        //--- create client    
        $clientClass = new Core_Api_Class();
        $result = $clientClass->createClient($this->_login . '|pmContext=' . $this->_company, $this->_password, $createForm);
        if (!$result->success) {
            foreach ($result->errors as $error)
                $errorMessages->addMessage($error->message);
        } else {
            $id = $result->data[0]->ClientID;
                $Apiuser = new Core_Api_User();
                $msgAccountEnabled = "User Added: ".$createForm["UserName"];
            $Apiuser->add_profile_action_log($this->_ClientID,$this->_login,$msgAccountEnabled);
            
            if (!empty($_FILES)) {
                $msgs = array();

                foreach ($_FILES as $k => $fileStruc) {
                    if (empty($fileStruc['tmp_name']))
                        continue;
                    if ($fileStruc['error'] == UPLOAD_ERR_OK &&
                            is_uploaded_file($fileStruc['tmp_name']) &&
                            filesize($fileStruc['tmp_name']) > 0
                    ) {
                        $fname = $fileStruc['name'];
                        if (strrpos($fname, '/') !== FALSE) {
                            $fname = substr($fname, strrpos($fname, '/'));
                        }
                        $info = pathinfo($fname);
                        $fname = uniqid($info['filename'] . '_');
                        $fname = $fname . '.' . $info['extension'];
                        $fdata = file_get_contents($fileStruc['tmp_name']);

                        $result = $clientClass->uploadClientLogo($this->_login, $this->_password, $id, $fname, $fdata, $k);

                        if (!$result->success)
                            $msgs[] = $result->errors[0]->message;
                    } else {
                        if ($fileStruc['error'] != UPLOAD_ERR_NO_FILE) {
                            $errorMessages->addMessage('File "' . $fileStruc['name'] . '" upload failed');
                        } else if (!is_uploaded_file($fileStruc['tmp_name'])) {
                            $errorMessages->addMessage('File "' . $fileStruc['name'] . '" not uploaded.');
                        } else if (filesize($fileStruc['tmp_name']) == 0) {
                            $errorMessages->addMessage('Uploaded "' . $fileStruc['name'] . '" file is empty.');
                        }
                        $errorsDetails = $errorMessages->getMessages();
                        error_log('FileUploading: (client:fWUA, client:' . $id . ') ' . implode('|', $errorsDetails));
                    }
                }

                if (!empty($removefiles)) {
                    $files = new Core_File();
                    foreach ($removefiles as $rmf) {
                        $files->removeFile($result->data[0]->$rmf, S3_CLIENTS_LOGO_DIR);
                    }
                }
                if (!empty($msgs)) {
                    $msgs = array_unique($msgs);
                    foreach ($msgs as $msg)
                        $errorMessages->addMessage($msg);
                }
            }
            // send email
            $toEmail = $createForm['Email1']; //$techEmail;
            $fromName = "FieldSolutions Support";
            $fromEmail = "support@fieldsolutions.com";
           
            $subject = "Welcome to FieldSolutions!";
            $body = "Welcome to FieldSolutions, ".$createForm['ContactName']."!
                
                    Your login information is:
                    
                    Username: ".$createForm['UserName']."
                    Temporary Password: ".$createForm['Password']."
                        
                    As a safety precaution, please update your temporary password when you first log into the FieldSolutions site. To update your password, click on 'Client Settings' in the blue Navigation Bar at the upper right of your screen, click the 'Edit' button, and scroll down to the bottom of the page.
                    
                    We're looking forward to working with you!
                    
                    Thank you,
                    
                    Your FieldSolutions Team
                    ";
            $mail = new Core_Mail();
            $mail->setBodyText($body);
            $mail->setFromName($fromName);
            $mail->setFromEmail($fromEmail);
            $mail->setToEmail($toEmail);
            $mail->setSubject($subject);
            $mail->send();
        }
        $err = $errorMessages->getMessages();
        if (empty($err)){
        	$this->_helper->json(array("success" => true, ""));
            //$this->_redirect('../clients/profile.php?v=' . $createForm['Company_ID']);
            
        }else{
        	$this->_helper->json(array("success" => false, ""));
        }
        //$this->_redirect($_SERVER['HTTP_REFERER']);
       
    }

    public function customerUpdateAction() {
        $request = $this->getRequest();

        $user = new Core_Api_CustomerUser();
        $id = $this->getRequest()->getParam('id', null);
        $customerInfo = $user->loadById($id);

        $this->view->customer = $customerInfo;
        $this->view->success = $customerInfo !== false;

        $errors = Core_Api_Error::getInstance();

        $this->view->messages = $error->message;
    }

    public function doCustomerUpdateAction() {
        $updateForm = $this->getRequest()->getParam('UpdateForm', null);
        $id = $this->getRequest()->getParam('Customer', null);

        $this->_company = $this->getRequest()->getParam('company', null);
        $errorMessages = new Core_Form_ErrorHandler('updateCustomer_' . $id);
        $errorMessages->clearMessages();

        if (empty($updateForm) || empty($id))
            $errorMessages->addMessage('Update Client Form is empty');
        $er = $errorMessages->getMessages();

        $cid = $id;
        if (!empty($cid)) {
            $user = new Core_Api_CustomerUser($cid);
            $user->loadById($cid);

            $datatoupdate = array();

            $datatoupdate = array();
            foreach ($updateForm as $field => $val) {
                if ($val != $user->$field)
                    $datatoupdate[$field] = $val;
            }

            if (!empty($datatoupdate)) {

                /* $factory = new Core_Validation_Factory(Core_Validation_Factory::VALIDATION_TYPE_CLIENT_PROFILE_UPDATE);
                  $validation = $factory->getValidationObject($datatoupdate);

                  if ($validation->validate() === false) {
                  $errors->addErrors('6', $validation->getErrors());
                  return API_Response::fail();
                  } */

//            $user = new Core_Api_CustomerUser($cid);
//            $user->loadById($cid);
                $result = $user->save($datatoupdate);
            }
        }

        $err = $errorMessages->getMessages();
        if (empty($err))
            $this->_redirect('../clients/customersView.php?v=' . $user->companyId);
        $this->_redirect($_SERVER['HTTP_REFERER']);
    }

    public function customerListAction() {
        $request = $this->getRequest();
        $params = $request->getParams();

        $size = ($request->getParam('size', 20)) + 0;
        $size = ( $size <= 10 ) ? 10 : ( ( $size >= 100 ) ? 100 : $size );
        $page = ($request->getParam('page', 1)) + 0;
        $page = ( $page < 1 ) ? 1 : $page;

        $sortBy = $request->getParam('sortBy', 'UserName');
        $sortDir = $request->getParam('sortDir', 'asc');

        $this->view->sortBy = $sortBy;
        $this->view->sortDir = $sortDir;

        $company_id = $this->getRequest()->getParam('Company_ID', null);
        $this->view->Company_ID = (!empty($company_id) ) ? $company_id : $auth->companyId;

        $search = new Core_Api_Class;

        $countRows = 0;
        $clients = $search->getCustomersList(
                $this->_login, $this->_password, $params, (!empty($sortBy) && !empty($sortDir) ) ? "{$sortBy} {$sortDir}" : null, ($page - 1) * $size, $size, $countRows
        );

        if ($clients->success) {
            $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($clients->data, $countRows));
            $paginator->setCurrentPageNumber($page);
            $this->view->paginator = $paginator;
        }
        $this->view->success = $clients->success;
        $this->view->errors = $clients->errors;
    }

    public function userListAction() {
        $request = $this->getRequest();
        $params = $request->getParams();

        $size = ($request->getParam('size', 20)) + 0;
        $size = ( $size <= 10 ) ? 10 : ( ( $size >= 100 ) ? 100 : $size );
        $page = ($request->getParam('page', 1)) + 0;
        $page = ( $page < 1 ) ? 1 : $page;

        $sortBy = $request->getParam('sortBy', 'UserName');
        $sortDir = $request->getParam('sortDir', 'asc');
        $this->view->client_id = $this->getRequest()->getParam('ClientID', 0);
        $this->view->sortBy = $sortBy;
        $this->view->sortDir = $sortDir;
        //233
        $clientID = $_SESSION['ClientID'];
        $apiGPM = new  Core_Api_Class();
        $this->view->isGPM = $apiGPM ->isGPM($clientID);

        // end 233
        $company_id = $this->getRequest()->getParam('Company_ID', null);
        $this->view->Company_ID = (!empty($company_id) ) ? $company_id : $auth->companyId;

        $search = new Core_Api_Class;
        
        $countRows = 0;
       
        $this->view->accountEnabled = $request->getParam('AccountEnabled', NULL);
        
        if($params['AccountEnabled'] == '0')unset($params['AccountEnabled']);
       
        $clients = $search->getClientsList(
                $this->_login, $this->_password, $params, (!empty($sortBy) && !empty($sortDir) ) ? "{$sortBy} {$sortDir}" : null, ($page - 1) * $size, $size, $countRows
        );
        
        
        //remove account enabled filter
        if(isset($params['AccountEnabled']))unset($params['AccountEnabled']);

        $allClients = $search->getClientsList(
                $this->_login, $this->_password, $params, (!empty($sortBy) && !empty($sortDir) ) ? "{$sortBy} {$sortDir}" : null, ($page - 1) * $size, $size, $countRows
        );

        if ($clients->success) {
            $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($clients->data, $countRows));
            $paginator->setItemCountPerPage(20)->setCurrentPageNumber($page);
            $this->view->paginator = $paginator;
            $this->view->fullClientList = $allClients->data;
        }
        $this->view->success = $clients->success;
        $this->view->errors = $clients->errors;
    }
    
    public function userRemoveAction(){
    	$this->_helper->viewRenderer->setNoRender(true);
    	$request = $this->getRequest();
        $params = $request->getParams();
        $clientId = $this->getRequest()->getParam('clientID', null);
        $client_id = $this->getRequest()->getParam('client_id', 0);
        
        $user = new Core_Api_User($clientId);
        $Apiuser = new Core_Api_User();
        $userinfo = $Apiuser->loadById($clientId); 
        $return = array();
        if($user->remove()){

                $Apiuser = new Core_Api_User();
                $msgAccountEnabled = "User Deleted: ".$userinfo->UserName;
            $Apiuser->add_profile_action_log($this->_ClientID,$this->_login,$msgAccountEnabled);
      
        	 $return['success'] = 0;
             $return['errors'] = array();
        }
        
         $response = $this->getResponse();
         $response->clearAllHeaders()->clearBody();
         $return = json_encode($return);
         $response->setHeader('Content-type', 'application/json');
         $response->setHeader('Content-Length', strlen($return), true)
                  ->setBody($return);
    }

    public function userUpdateAction() {
        $client_id = $this->getRequest()->getParam('id', null);
        $user = new Core_Api_User($client_id);
        $client = $user->loadById($client_id);
        $this->view->client_id = $this->getRequest()->getParam('client_id', 0);
        $clientID = $_SESSION['ClientID'];
        $apiGPM = new  Core_Api_Class();
        $this->view->isGPM = $apiGPM ->isGPM($clientID);

        $errorMessages = new Core_Form_ErrorHandler('updateClient_' . $client_id);

        $this->view->client = $client;
        $this->view->success = $client !== false;

        $search = new Core_Api_Class;

        //get states
        $states = $search->getStates();
        $this->view->states = $states->data;

        $errors = Core_Api_Error::getInstance();
        $er = $errors->getErrorArray();
        foreach ($er as $k => $v)
            $errorMessages->addMessage($error->message);

        $this->view->messages = $errorMessages->getMessages();
        $errorMessages->clearMessages();
    }

    public function doUserUpdateAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $updateForm = $this->getRequest()->getParam('UpdateForm', null);
        $id = $this->getRequest()->getParam('ClientID', null);
        $removefiles = $this->getRequest()->getParam('removefiles', null);
        $this->_company = $this->getRequest()->getParam('company', null);
        $errorMessages = new Core_Form_ErrorHandler('updateClient_' . $id);
        $client_id = $this->getRequest()->getParam('client_id', 0);
        
        $errorMessages->clearMessages();
        if (!empty($removefiles))
            foreach ($removefiles as $rmf)
                $updateForm[$rmf] = '';

        if (empty($updateForm) || empty($id))
            $errorMessages->addMessage('Update Client Form is empty');
        $er = $errorMessages->getMessages();
        if (empty($er)) {
            $search = new Core_Api_Class;
            $checkboxes = array('AccountEnabled', 'Admin');
            foreach ($checkboxes as $c)
                $updateForm[$c] = isset($updateForm[$c]) ? 1 : 0;
            $Apiuser = new Core_Api_User();
            $user = $Apiuser->loadById($id);
            
            if(isset($updateForm['UserType']) &&  $updateForm['UserType']=='Admin') 
            {
                $updateForm['Admin']=1;
            }            
            if(isset($updateForm['Admin']) &&  $updateForm['Admin']==1) 
            {
                $updateForm['UserType']='Admin';
            }                
            //echo("updateForm: <pre>");print_r($updateForm);//test
            
            //--- update
            $result = $search->updateClient($this->_login, $this->_password, $id, $updateForm);
		
            $errors = Core_Api_Error::getInstance();
            if (!$result->success) {
                foreach ($result->errors as $error) {
                    $errorMessages->addMessage($error->message);
                }
            } else {
                // write log
               
                    if($updateForm['AccountEnabled'] != $user->AccountEnabled){
                        $msgAccountEnabled = "";
                        if($updateForm['AccountEnabled']){
                            $msgAccountEnabled = "User Activated: ".$user->UserName;
                        }else{
                            $msgAccountEnabled = "User Marked Inactive: ".$user->UserName;
                        }
                    $Apiuser->add_profile_action_log($this->_ClientID,$this->_login,$msgAccountEnabled);
                    }
                    $fieldChanges = array(
                        'ContactName'=>'Contact Name', 
                        'ContactPhone1'=>'Primary Phone',
                        'ContactPhone2'=>'Secondary Phone',
                        'Email1'=>'Primary Email',
                        'Email2'=>'Secondary Email',
                        'Password'=>'Password',
                        'UserType'=>'User Type');
                    $field = "";
                    foreach ($fieldChanges as $key=>$value){
                        if(trim($updateForm[$key]) != trim($user->$key)){
                            $field .= $value.", ";
                        }
                    }
                    if(!empty($field)){
                        $field = substr($field, 0,count($field) - 3);
                        $msgAccountEnabled = "Edits to ".$user->UserName." Contact Information: ".$field;
                    $Apiuser->add_profile_action_log($this->_ClientID,$this->_login,$msgAccountEnabled);
                    }
                
                if (!empty($_FILES)) {
                    $msgs = array();

                    foreach ($_FILES as $k => $fileStruc) {
                        if (empty($fileStruc['tmp_name']))
                            continue;
                        if ($fileStruc['error'] == UPLOAD_ERR_OK &&
                                is_uploaded_file($fileStruc['tmp_name']) &&
                                filesize($fileStruc['tmp_name']) > 0
                        ) {
                            $fname = $fileStruc['name'];
                            if (strrpos($fname, '/') !== FALSE) {
                                $fname = substr($fname, strrpos($fname, '/'));
                            }
                            $info = pathinfo($fname);
                            $fname = uniqid($info['filename'] . '_');
                            $fname = $fname . '.' . $info['extension'];
                            $fdata = file_get_contents($fileStruc['tmp_name']);

                            $result = $search->uploadClientLogo($this->_login, $this->_password, $id, $fname, $fdata, $k);

                            if (!$result->success)
                                $msgs[] = $result->errors[0]->message;
                        } else {
                            if ($fileStruc['error'] != UPLOAD_ERR_NO_FILE) {
                                $errorMessages->addMessage('File "' . $fileStruc['name'] . '" upload failed');
                            } else if (!is_uploaded_file($fileStruc['tmp_name'])) {
                                $errorMessages->addMessage('File "' . $fileStruc['name'] . '" not uploaded.');
                            } else if (filesize($fileStruc['tmp_name']) == 0) {
                                $errorMessages->addMessage('Uploaded "' . $fileStruc['name'] . '" file is empty.');
                            }
                            $errorsDetails = $errorMessages->getMessages();
                            error_log('FileUploading: (admin:fWUA, client:' . $id . ') ' . implode('|', $errorsDetails));
                        }
                    }

                    if (!empty($removefiles)) {
                        $files = new Core_File();
                        foreach ($removefiles as $rmf) {
                            $files->removeFile($result->data[0]->$rmf, S3_CLIENTS_LOGO_DIR);
                        }
                    }
                    if (!empty($msgs)) {
                        $msgs = array_unique($msgs);
                        foreach ($msgs as $msg)
                            $errorMessages->addMessage($msg);
                    }
                }
            }
        }
        $err = $errorMessages->getMessages();
        if (empty($err)){
            $this->_redirect('../clients/profile.php?v=' . $this->_company."&tabid=clientUsers");
            }
        $this->_redirect($_SERVER['HTTP_REFERER']);
    }

    public function addContactAction() {
        $request = $this->getRequest();

        $this->view->Company_ID = $request->getParam('Company_ID', null);
        $this->view->Created_By = $request->getParam('Created_By', null);
        $this->view->WorkOrder_ID = $request->getParam('WorkOrder_ID', null);
        $this->view->email = $request->getParam('email', null);
        $this->view->techName = $request->getParam('techName', null);
        $this->view->fromEmail = $request->getParam('fromEmail', null);
        $this->view->isLB = $request->getParam('isLB', null);
        $this->view->messages = array();

        $addContactForm = $request->getParam('addContactForm', null);
        if (!empty($addContactForm)) {
            $this->_helper->viewRenderer->setNoRender();
            $return = array();
            $return['success'] = 1;
            $return['errors'] = '';

            $clientClass = new Core_Api_Class();
            $res = $clientClass->addContact($this->_login, $this->_password, $addContactForm, $request->getParam('fromEmail', null), $request->getParam('Copy', null));
            if (!$res->success) {
                foreach ($res->errors as $error)
                    $return['errors'] = $error->message;
                $return['success'] = 0;
            } else {
                $copy = $request->getParam('Copy', null);
                if ($copy) {
                    
                }
            }
            $response = $this->getResponse();
            $response->clearAllHeaders()->clearBody();
            $return = json_encode($return);
            $response->setHeader('Content-type', 'application/json');
            $response->setHeader('Content-Length', strlen($return), true)
                    ->setBody($return);
        }
    }

    public function viewContactLogAction() {
//        $request = $this->getRequest();
//
//        $this->view->Company_ID = $request->getParam('Company_ID', null);
//        $this->view->WorkOrder_ID = $request->getParam('win', null);
//
//        $params = array('Company_ID' => $this->view->Company_ID, 'WorkOrder_ID' => $this->view->WorkOrder_ID);
//
//        $size = ($request->getParam('size', 10)) + 0;
//        $size = ( $size <= 10 ) ? 10 : ( ( $size >= 100 ) ? 100 : $size );
//        $page = ($request->getParam('page', 1)) + 0;
//        $page = ( $page < 1 ) ? 1 : $page;
//
//        $sortBy = $request->getParam('sortBy', 'Created_Date');
//        $sortDir = $request->getParam('sortDir', 'asc');
//
//        $this->view->sortBy = $sortBy;
//        $this->view->sortDir = $sortDir;
//
//        $search = new Core_Api_Class;
//
//        $countRows = 0;
//        $log = $search->getContactLog(
//                $this->_login, $this->_password, $params, (!empty($sortBy) && !empty($sortDir) ) ? "{$sortBy} {$sortDir}" : null, ($page - 1) * $size, $size, $countRows
//        );
//
//        if ($log->success) {
//            $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($log->data, $countRows));
//            $paginator->setCurrentPageNumber($page);
//            $this->view->paginator = $paginator;
//        }
//        $this->view->success = $log->success;
//        $this->view->errors = $log->errors;
        $request = $this->getRequest();
        $params = $request->getParams();
        $win = $request->getParam('win', null);
        $companyId = $request->getParam('Company_ID', null);

        $search = new Core_Api_Class;

        $wo = $search->getWorkOrder($this->_login.'|pmContext='.$companyId, $this->_password, $win);
        $wo = $wo->data[0];

        $WosClass = new Core_Api_WosClass();
        $Core_Api_User = new Core_Api_User;

        $clientInfo = $Core_Api_User->loadWithExt(
            array(
                'login' => $this->_login . '|pmContext=' . $companyId,
                'password' => $this->_password
            )
        );
        $clientID = $clientInfo->getClientId();
        $WosClass->saveClient_LastViewQAWinDatetime($clientID,$win);

        if(isset($params['forTechID']) && $params['forTechID'] !=="")
        {
            $forTechID = $params['forTechID'];
        }
        else 
        {
            if($wo->Tech_ID !="")
            {
                $forTechID = $wo->Tech_ID ;
            }
            else
            {
                $forTechID = 0;
            }
        }

        //$QAlist = $WosClass->getQuestionContactLog($win,0,$clientID,1);
        $QAlist = $WosClass->getQuestionContactLog($win,0,$clientID,1,$forTechID);
        $this->view->WOVisitList = $WosClass->GetWOVisitList($win);
        $this->view->wo = $wo;
        $this->view->QAlist = $QAlist;
        $this->view->clientID = $clientID;
        }

    public function customerCreateAction() {
        $errorMessages = new Core_Form_ErrorHandler('createCustomer');
        $this->view->messages = $errorMessages->getMessages();
        $company_id = $this->getRequest()->getParam('Company_ID', null);
        $this->view->Company_ID = (!empty($company_id) ) ? $company_id : $this->view->company;
        $errorMessages->clearMessages();
    }

    public function doCustomerCreateAction() {
        $errorMessages = new Core_Form_ErrorHandler('createCustomer');
        $this->_helper->viewRenderer->setNoRender(true);
        $return = array('success' => 0);

        $user = new Core_Api_User();

        $formFields = $this->getRequest()->getParam('CreateForm', array());
        if (!empty($formFields['Password'])) {
            $userInfo = $user->load(
                    array(
                        'login' => $this->_login . '|pmContext=' . $this->_company, 'password' => $this->_password)
            );
            $cid = $userInfo->contextId;
            if (empty($cid)) {
                // wrong password confirmation
                $return['success'] = 0;
                $needUpdate = false;
                $return['errors'][] = 'Wrong Password';
            }

            if (empty($formFields['Password']) || $formFields['Password'] != $formFields['PasswordConfirm']) {
                $return['success'] = 0;
                $needUpdate = false;
                if (empty($return['errors']))
                    $return['errors'] = array();
                $return['errors'][] = 'Incorrect Password Confirmation';
            }
        }

        if (empty($formFields['Password'])
                || empty($formFields['PasswordConfirm'])
                || $formFields['Password'] != $formFields['PasswordConfirm']
        ) {
            // clear PasswordConfirm

            unset($formFields['PasswordConfirm']);
        }


        $search = new Core_Api_Class();

        $result = $search->createCustomer($this->_login . '|pmContext=' . $this->_company, $this->_password, $formFields);
        $errors = Core_Api_Error::getInstance();
        if (!$result->success) {
            foreach ($result->errors as $error) {
                $return['success'] = 0;
                $errorMessages->addMessage($error->message);
            }
        } else {


            $return['success'] = 1;
        }

        $err = $errorMessages->getMessages();
        if (empty($err))
            $this->_redirect('../clients/customersView.php?v=' . $this->_company);
        $this->_redirect($_SERVER['HTTP_REFERER']);
    }

    public function mySettingsAction() {

		$company_id = $this->getRequest()->getParam('Company_ID', null);
		if (!empty($company_id))
			$this->_company = $this->view->company = $company_id;
        $Core_Api_User = new Core_Api_User;
        $userInfo = $Core_Api_User->load(
                array(
                    'login' => $this->_login . '|pmContext=' . $this->_company, 'password' => $this->_password)
        );

		$company_id = $userInfo->getContextCompanyId();
		$this->_company = $this->view->company = $company_id;
		$fields = array("ClientID", "ContactName", "ContactPhone1", "ContactPhone2", "UserName", "Password");
		foreach ($fields as $i => $field) {
			$this->view->$field = $userInfo->$field;
		}

		$this->view->Admin = $userInfo->getAdmin();
		$this->view->isPM = $userInfo->isPM();
		
        $search = new Core_Api_ProjectClass;
        $projects = $search->getAllProjects(
            $this->_login.'|pmContext='.$this->_company,
            $this->_password, NULL, $this->view->isPM ? array('ignoreCompany' => true) : NULL
        );
		
		$this->view->projects = $projects;

		$future = new Core_FutureWorkOrderInfo;
		$row = $future->find($userInfo->getLogin())->toArray();
		$this->view->futureInfo = $row[0];
		
		$this->view->MyProjects_AllowedUsers = Core_ProjectUsersAllowedCreateWO::getRestrictionsForAllProjectsOwner($this->_login, $userInfo->isPM() ? NULL : $this->_company);
		
		$this->view->MyProjects_AllowAll = count($this->view->MyProjects_AllowedUsers) == 0;
		
		$search = new Core_Api_Class;
		$this->view->usersAll = $search->getClientsList($this->_login, $this->_password, array('AccountEnabled' => 1), "Username");
		$client_id = $userInfo->getClientId();
		$dashboardSettings = Core_DashboardSettings::getSettings($client_id);
		$projectFilter = empty($dashboardSettings) ? Core_DashboardSettings::FILTER_NONE : $dashboardSettings["filter"];
		$this->view->MyDashboard_Project_UseDefault = $projectFilter == Core_DashboardSettings::FILTER_PROJECT_MY;
		$this->view->MyDashboard_Project_UseCustom = $projectFilter == Core_DashboardSettings::FILTER_PROJECT_SELECT;
		$this->view->MyDashboard_FilterByProjectList = Core_DashboardSettings_Project::getSettings($client_id);
		$this->view->MyDashboard_FilterByProject = $projectFilter == Core_DashboardSettings::FILTER_PROJECT_SELECT || $projectFilter == Core_DashboardSettings::FILTER_PROJECT_MY;
		$this->view->MyDashboard_FilterByDefault = !$this->view->MyDashboard_FilterByProject;
		$this->view->MyDashboard_FilterByWorkOrder = $projectFilter == Core_DashboardSettings::FILTER_WORK_ORDER;
		if ($this->view->MyDashboard_FilterByWorkOrder) {
			$this->view->MyDashboard_FilterByWorkOrder = Core_DashboardSettings_WorkOrder::getSettings($client_id);
			$this->view->MyDashboard_FilterByWorkOrder = empty($this->view->MyDashboard_FilterByWorkOrder) ? true : $this->view->MyDashboard_FilterByWorkOrder["filter"];
		}
		// sorting section
		$this->view->MyDashboard_Sort = empty($dashboardSettings) ? Core_DashboardSettings::SORT_DEFAULT : $dashboardSettings['sort'];
		$this->view->MyDashboard_Sort_CreatedSort = $dashboardSettings['defaultCreatedSort'];
		$this->view->MyDashboard_Sort_CreatedSortDir = $dashboardSettings['defaultCreatedSortDir'];
                $this->view->MyDashboard_Sort_TodayworkSort = $dashboardSettings['defaultTodaysworkSort'];
		$this->view->MyDashboard_Sort_TodayworkSortDir = $dashboardSettings['defaultTodaysworkSortDir'];
		$this->view->MyDashboard_Sort_PublishedSort = $dashboardSettings['defaultPublishedSort'];
		$this->view->MyDashboard_Sort_PublishedSortDir = $dashboardSettings['defaultPublishedSortDir'];
		$this->view->MyDashboard_Sort_AssignedSort = $dashboardSettings['defaultAssignedSort'];
		$this->view->MyDashboard_Sort_AssignedSortDir = $dashboardSettings['defaultAssignedSortDir'];
		$this->view->MyDashboard_Sort_WorkDoneSort = $dashboardSettings['defaultWorkDoneSort'];
		$this->view->MyDashboard_Sort_WorkDoneSortDir = $dashboardSettings['defaultWorkDoneSortDir'];
		$this->view->MyDashboard_Sort_IncompleteSort = $dashboardSettings['defaultIncompleteSort'];
		$this->view->MyDashboard_Sort_IncompleteSortDir = $dashboardSettings['defaultIncompleteSortDir'];
		$this->view->MyDashboard_Sort_CompletedSort = $dashboardSettings['defaultCompletedSort'];
		$this->view->MyDashboard_Sort_CompletedSortDir = $dashboardSettings['defaultCompletedSortDir'];
		$this->view->MyDashboard_Sort_DeactivatedSort = $dashboardSettings['defaultDeactivatedSort'];
		$this->view->MyDashboard_Sort_DeactivatedSortDir = $dashboardSettings['defaultDeactivatedSortDir'];
		$this->view->MyDashboard_Sort_AllSort = $dashboardSettings['defaultAllSort'];
		$this->view->MyDashboard_Sort_AllSortDir = $dashboardSettings['defaultAllSortDir'];
		$this->view->MyDashboard_View = $dashboardSettings['view'];
		$this->view->MyDashboard_UseDefault = $projectFilter == Core_DashboardSettings::FILTER_NONE && $this->view->MyDashboard_Sort == Core_DashboardSettings::SORT_DEFAULT && $this->view->MyDashboard_View == Core_DashboardSettings::VIEW_FULL;
		$this->view->MyDashboard_UseCustom = !$this->view->MyDashboard_UseDefault;
//		$clients = $Core_Api_User->getRepClientforCompany($company_id);
                $Core_Api_NoticeClass = new Core_Api_NoticeClass();
                $this->view->seeNoticeFors = $Core_Api_NoticeClass->getSeeNoticeFor($client_id);
                $this->view->noticeProjectList = $Core_Api_NoticeClass->getProjectList($client_id);
                $this->view->notice = $this->getRequest()->getParam('notice', null);
	}

	
	public function saveMySettingsAction() {
		
		$company_id = $_SESSION['PM_Company_ID'];
		if (!empty($company_id))
			$this->_company = $this->view->company = $company_id;
        $Core_Api_User = new Core_Api_User;
        $userInfo = $Core_Api_User->load(
                array(
                    'login' => $this->_login . '|pmContext=' . $this->_company, 'password' => $this->_password)
        );
		$company_id = $userInfo->getContextCompanyId();
		$this->_company = $this->view->company = $company_id;
		$this->_helper->viewRenderer->setNoRender(true);
		
		$params = $this->getRequest()->getParams();
		$updateFields = array('ContactName','ContactPhone1','ContactPhone2', 'Password', 'UserName');
		foreach($params as $k=>$v){
			if(in_array($k,$updateFields))
				$updateForm[$k] = $v;
		}
		$updateForm['CompanyName'] = $userInfo->getCompanyName();
		$updateForm['Email1'] = $userInfo->getEmailPrimary();
		//Update Client info in Contact and Log In section
		
		$search = new Core_Api_Class;
        $result = $search->updateClient($userInfo->getLogin(), $userInfo->getPassword(), $userInfo->getClientId(), $updateForm);
		if (!empty($updateForm["Password"])) {
			$_SESSION['Password'] = $updateForm["Password"];
			$this->_password = $updateForm["Password"];
		}

		$MyProjects_AllowAll = $this->getRequest()->getParam('MyProjects_AllowAll', 'Restricted');
		$myProjectControls = $this->getRequest()->getParam('MyProjectControls', null);
		if ($MyProjects_AllowAll == 'All' || empty($myProjectControls)) {
			Core_ProjectUsersAllowedCreateWO::removeAllRestrictionsForAllProjectsOwner($this->_login, $userInfo->isPM() ? NULL : $company_id);
		}
		else {
			$myProjectControls = json_decode($myProjectControls);
			foreach ($myProjectControls as $pid => $controls) {
				if ($controls->add) {
					// add user restrictions for the project
					$controls->userlist_key[] = $this->_login; // always include current user
					$controls->userlist_value[] = true; // always include current user
					$userlist = array_combine($controls->userlist_key, $controls->userlist_value);
					$userlistAdd = array();
					$userlistRemove = array();
					$MyProjects_AllowSetting = $this->getRequest()->getParam('MyProjects_AllowSetting' . $pid, null);
					if ($MyProjects_AllowSetting == 'Me') {
						Core_ProjectUsersAllowedCreateWO::removeAllUsersForProjectID($pid);
						$userlistAdd[] = $this->_login;
					}
					else {
						foreach ($userlist as $user => $val) {
							if ($val) $userlistAdd[] = $user;
							else $userlistRemove[] = $user;
						}
					}
					Core_ProjectUsersAllowedCreateWO::addUsersForProjectID($pid, $userlistAdd);
					Core_ProjectUsersAllowedCreateWO::removeUsersForProjectID($pid, $userlistRemove);
				}
				else {
					Core_ProjectUsersAllowedCreateWO::removeAllUsersForProjectID($pid);
				}
			}
		}
		
		// My Dashboard Settings
		$dashboardSettingSave = array();
		$client_id = $userInfo->getClientId();
		$MyDashboardSettings = $this->getRequest()->getParam('MyDashboardSettings', null);
		$MyDashboard_UseDefault = $this->getRequest()->getParam('MyDashboard_UseDefault', null);
		$MyDashboard_FilterBy = $this->getRequest()->getParam('MyDashboard_FilterBy', null);
		$MyDashboard_FilterByProject = $this->getRequest()->getParam('MyDashboard_FilterByProject', null);
		$MyDashboard_FilterByWorkOrder_Owned = intVal($this->getRequest()->getParam('MyDashboard_FilterByWorkOrder_Owned', 0));
		$MyDashboard_FilterByWorkOrder_Created = intVal($this->getRequest()->getParam('MyDashboard_FilterByWorkOrder_Created', 0));
		$MyDashboard_FilterByWorkOrder_Published = intVal($this->getRequest()->getParam('MyDashboard_FilterByWorkOrder_Published', 0));
		$MyDashboard_FilterByWorkOrder_Assigned = intVal($this->getRequest()->getParam('MyDashboard_FilterByWorkOrder_Assigned', 0));
		$MyDashboard_FilterByWorkOrder_Approved = intVal($this->getRequest()->getParam('MyDashboard_FilterByWorkOrder_Approved', 0));
		$MyDashboard_FilterByWorkOrder = $MyDashboard_FilterByWorkOrder_Owned | $MyDashboard_FilterByWorkOrder_Created | $MyDashboard_FilterByWorkOrder_Published | $MyDashboard_FilterByWorkOrder_Assigned | $MyDashboard_FilterByWorkOrder_Approved;
		$MyDashboard_Project_UseDefault = $this->getRequest()->getParam('MyDashboard_Project_UseDefault', null);
		if ($MyDashboard_UseDefault == "Default") {
			$dashboardSettingsSave['filter'] = Core_DashboardSettings::FILTER_NONE;
			$dashboardSettingsSave['sort'] = Core_DashboardSettings::SORT_DEFAULT;
			$dashboardSettingsSave['view'] = Core_DashboardSettings::VIEW_FULL;
			Core_DashboardSettings_Project::clearSettings($client_id);
			Core_DashboardSettings_WorkOrder::clearSettings($client_id);
		} 
		else {
			if ($MyDashboard_FilterBy == "Project") {
				$dashboardSettingsSave['filter'] = $MyDashboard_Project_UseDefault == 'Custom' ? Core_DashboardSettings::FILTER_PROJECT_SELECT : Core_DashboardSettings::FILTER_PROJECT_MY;
				Core_DashboardSettings_WorkOrder::clearSettings($client_id);
				$MyDashboardSettings = json_decode($MyDashboardSettings);
				if ($MyDashboardSettings) {
					$projectFilter = array();
					foreach ($MyDashboardSettings as $pid => $controls) {
						if (!$controls->add) continue;
						$projectFilter[] = $pid;
					}
					Core_DashboardSettings_Project::saveSettings($client_id, $projectFilter);
				}
			}
			else if ($MyDashboard_FilterBy == "Work Order") {
				if (empty($MyDashboard_FilterByWorkOrder)) $MyDashboard_FilterByWorkOrder = Core_DashboardSettings_WorkOrder::FILTER_NONE;
				$dashboardSettingsSave['filter'] = Core_DashboardSettings::FILTER_WORK_ORDER;
				Core_DashboardSettings_WorkOrder::saveSettings($client_id, $MyDashboard_FilterByWorkOrder);
				Core_DashboardSettings_Project::clearSettings($client_id);
			}
			else if ($MyDashboard_FilterBy == "Default") {
				$dashboardSettingsSave['filter'] = Core_DashboardSettings::FILTER_NONE;
				Core_DashboardSettings_Project::clearSettings($client_id);
				Core_DashboardSettings_WorkOrder::clearSettings($client_id);
			}
		}
		
		$MyDashboard_Sort = $this->getRequest()->getParam('MyDashboard_Sort', null);
		if ($MyDashboard_UseDefault != "Default" && $MyDashboard_Sort !== null) {
			$dashboardSettingsSave['sort'] = $MyDashboard_Sort;
			if ($MyDashboard_Sort == Core_DashboardSettings::SORT_DEFAULT_MY) {
				$dashboardSettingsSave = $this->processMyDashboardSettingsSort('createdSort', $dashboardSettingsSave);
                                $dashboardSettingsSave = $this->processMyDashboardSettingsSort('todaysworkSort', $dashboardSettingsSave);
				$dashboardSettingsSave = $this->processMyDashboardSettingsSort('publishedSort', $dashboardSettingsSave);
				$dashboardSettingsSave = $this->processMyDashboardSettingsSort('assignedSort', $dashboardSettingsSave);
				$dashboardSettingsSave = $this->processMyDashboardSettingsSort('workDoneSort', $dashboardSettingsSave);
				$dashboardSettingsSave = $this->processMyDashboardSettingsSort('incompleteSort', $dashboardSettingsSave);
				$dashboardSettingsSave = $this->processMyDashboardSettingsSort('completedSort', $dashboardSettingsSave);
				$dashboardSettingsSave = $this->processMyDashboardSettingsSort('allSort', $dashboardSettingsSave);
				$dashboardSettingsSave = $this->processMyDashboardSettingsSort('deactivatedSort', $dashboardSettingsSave);
			}
		}
		$MyDashboard_View = $this->getRequest()->getParam('MyDashboard_View', null);
		if ($MyDashboard_UseDefault != "Default" && $MyDashboard_View !== null)
			$dashboardSettingsSave['view'] = $MyDashboard_View;
			
		Core_DashboardSettings::saveSettings($client_id, $dashboardSettingsSave);
		// End My Dashboard Settings
		
		$woCommSetting = $this->getRequest()->getParam('WorkOrderCommSetting',null);
		$MyWOCommunications_Select = $this->getRequest()->getParam('MyWOCommunications_Select','Restrict');
		$future = new Core_FutureWorkOrderInfo();
		if ($MyWOCommunications_Select != "Restrict") {
			// clear comm settings
			$db = Core_Database::getInstance();
			$future->delete($db->quoteInto("ClientUserName=?",$userInfo->getLogin()));
		}
		else if($woCommSetting != null && $woCommSetting != "0"){
			// customize comm settings 13691, 13763
			$futureFields = array('ProjectManagerName', 'ProjectManagerPhone', 'ProjectManagerEmail', 'ResourceCoordinatorName', 'ResourceCoordinatorPhone', 'ResourceCoordinatorEmail', 'EmergencyName', 'EmergencyPhone', 'EmergencyEmail', 'CheckInOutName', 'CheckInOutNumber', 
								  'CheckInOutEmail', 'TechnicalSupportName', 'TechnicalSupportPhone', 'TechnicalSupportEmail', 'SystemGeneratedEmailTo', 'SystemGeneratedEmailFrom', 'WorkAvailableEmailFrom', 'BidNotificationEmailTo', 'P2TNotificationEmailTo', 'WorkAssignedEmailTo', 'WorkDoneEmailTo', 'TechQAMessageEmailTo','ACSNotifiPOC','ACSNotifiPOC_Phone','ACSNotifiPOC_Email','WorkAcceptedByTechEmailTo','WorkConfirmedByTechEmailTo','TechCheckedInEmailTo','TechCheckedOutEmailTo');

			$blankValues = array("Name","Phone Number","Email");
			$futureUpdate = array();
			foreach($params as $k=>$v){
				if(in_array($k,$futureFields)){
					if(in_array($v,$blankValues)){
						$futureUpdate[$k] = "";
					}else{
						$futureUpdate[$k] = $v;
					}
				}
			}
			$futureUpdate['dateUpdated'] = new Zend_Db_Expr("NOW()");
			$row = $future->find($userInfo->getLogin());

			if(count($row) > 0){
				$where = $future->getAdapter()->quoteInto('ClientUserName = ?', $userInfo->getLogin());
				$future->update($futureUpdate, $where);
			}else{
				$futureUpdate['dateCreated'] = new Zend_Db_Expr("NOW()");
				$futureUpdate['ClientUserName'] = $userInfo->getLogin();
				$future->insert($futureUpdate);
			}
			
			if($woCommSetting != "3"){
				$db = Core_Database::getInstance();  
	        	$select = $db->select();
	        	$select->from(array("w" => Core_Database::TABLE_WORK_ORDERS),array('WIN_NUM', 'p.Project_ID', 'p.AllowCustomCommSettings'));
				$select->joinLeft(array("p" => Core_Database::TABLE_PROJECTS), "w.Project_ID = p.Project_ID");
	        	$select->where("WorkOrderOwner = ? OR (WorkOrderOwner IS NULL AND Username = ?)",$userInfo->getLogin(), $userInfo->getLogin()); 
	        	$select->where("Deactivated = 0"); 
	        	$select->where("TechMarkedComplete = 0"); 
	        	$select->where("Approved = 0"); 
	        	$select->where("p.AllowCustomCommSettings = '1' ");   
	       		 
				if($woCommSetting == "2"){
					$select->where("(Tech_ID IS NULL OR Tech_ID = '')");
	        		$select->where("(Status = 'created' OR Status = 'published')"); 
				}
	        	$records = Core_Database::fetchAll($select);

	       		if(!empty($records)){
	       			$woIDs = '';
		       		foreach($records as $rec){
			            if(!empty($woIDs)) $woIDs.=',';
			            $woIDs .= $rec['WIN_NUM'];            
			        }
		       		$apiWos = new Core_Api_WosClass();
	       			$apiWos->updateFutureInfoCommDataForWOs($woIDs,$futureUpdate);
	       		}
			}
		}
		
		$MyNotices = $this->getRequest()->getParam('MyNotices', null);
		$MyNotices_Select = $this->getRequest()->getParam('MyNotices_Select', null);
		$MyNotices_Select = $MyNotices_Select != "All"?1:0;
		$Core_Api_NoticeClass = new  Core_Api_NoticeClass;
		$client_id = $userInfo->getClientId();
		if($MyNotices_Select == 0){
			$Core_Api_NoticeClass->saveSeeNoticeFor($client_id,$MyNotices_Select);
		}else{
			$Core_Api_NoticeClass->saveSeeNoticeFor($client_id,$MyNotices_Select);
			if(!empty($MyNotices)){
				$Core_Api_NoticeClass->saveNoticeProjects($client_id,$MyNotices);
			}
		}

		//Reload authentication data if password changed
		if (!empty($updateForm["Password"])) {
			$authData = array('login'=>$this->_login, 'password'=>$updateForm["Password"]);
			$this->checkAuthentication($authData);
		}

		$this->_helper->json(array("success" => true, "MyNotices_Select"=>$MyNotices_Select,"MyNotices"=>$MyNotices));
	}

    //-------------------------------------------------------------
    public function accesssettingprofileAction() {
        $Core_Api_CommonClass = new Core_Api_CommonClass;
        $Core_Api_User = new Core_Api_User();
        $Attributes = $Core_Api_CommonClass->getAllClientAccessAttributes();
        $Core_Api_Class = new Core_Api_Class;

        
	    if (!empty($_SESSION['PM_Company_ID'])) {
	    	$company_id = $_SESSION['PM_Company_ID'];
	        $clients = $Core_Api_User->getRepClientforCompany($company_id);
	        for ($i = 0; $i < count($clients); $i++) {
	          $client_id = $clients[$i]['ClientID'];
	          break;
	         }
	     }
	     else
	     {
		    $userInfo = $Core_Api_User->load(
		                array(
		                    'login' => $this->_login . '|pmContext=' . $this->_company, 'password' => $this->_password)
		        );
		        $client_id = $userInfo->ClientID;
	        }
        if (empty($client_id)) {
            return;
        }
        $_mode = $this->getRequest()->getParam('mode', null);


        $Core_Api_User->SetAccessDefaultValuesForClient($client_id);

        $Values = $Core_Api_Class->getClientAccessControlValues($client_id);

        $_Attributes = $Attributes;
        if (!empty($Attributes)) {
            foreach ($Attributes as $i => $Attribute) {
                $_Attributes[$i]['cliaccess_atrib_value'] = 0;
                $_Attributes[$i]['open'] = true;
                $_Attributes[$i]['restricted'] = false;
                if (!empty($Values)) {
                    foreach ($Values as $j => $Value) {
                        if ($Value['cliaccess_atrib_id'] == $Attribute['id']) {
                            $_Attributes[$i]['cliaccess_atrib_value'] = $Value['cliaccess_atrib_value'];
                            if ($Value['cliaccess_atrib_value'] == 1) {
                                $_Attributes[$i]['open'] = true;
                            } else {
                                $_Attributes[$i]['open'] = false;
                            }
                            if ($Value['cliaccess_atrib_value'] == 2) {
                                $_Attributes[$i]['restricted'] = true;
                            } else {
                                $_Attributes[$i]['restricted'] = false;
                            }
                            if (empty($Value['cliaccess_atrib_value'])) {
                                $_Attributes[$i]['restricted'] = false;
                                $_Attributes[$i]['open'] = true;
                            }
                            break;
                        }
                    }
                }
            }
        }
        $this->view->clientAccessAttributes = $_Attributes;
        $mode1 = ($_mode == 'edit') ? true : false;
        $this->view->mode = $mode1;
    }

    //--------------------------------------------------------
    public function activitylogprofileAction() {
        $request = $this->getRequest();
        $Core_Api_User = new Core_Api_User();
        $size = (int) ($request->getParam('size', 10));
        $size = ( $size <= 10 ) ? 10 : ( ( $size >= 100 ) ? 100 : $size );
        $page = ($request->getParam('page', 1)) + 0;
        $page = ( $page < 1 ) ? 1 : $page;
        $offset = ($page * $size) - $size;
        $userInfo = $Core_Api_User->load(
                array(
                    'login' => $this->_login . '|pmContext=' . $this->_company, 'password' => $this->_password)
        );
        $companyId = $this->getRequest()->getParam('company', null);
        if($companyId != null && $companyId != ""){
        	$db = Core_Database::getInstance();
        	$select = $db->select();
        	$select ->from(Core_Database::TABLE_CLIENTS);
        	$select ->where("Company_ID = '$companyId'");
        	$records = Core_Database::fetchAll($select);  
        	$client_id = $records[0]['ClientID'];
        }else{
       		$client_id = $userInfo->ClientID;
        }
       		
        if (empty($client_id)) {
            return;
        }

        $Core_Api_User = new Core_Api_User();
        $countRows = $Core_Api_User->GetTotalOfAccessLogForClient($client_id);
        $Logs = $Core_Api_User->GetListOfAccessLogForClient($client_id, $size, $offset);


        if ($countRows > 0) {

            $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($Logs, $countRows));
            $paginator->setCurrentPageNumber($page);
            $this->view->paginator = $paginator;
        }

        $this->view->Logs = $Logs;
    }

    //--------------------------------------------------------
    public function accessctlAction() {
        $request = $this->getRequest();
        $allowAccess = 0;
        $attributeIds = $request->getParam('attributeId', 0);

        $user = new Core_Api_User();
        $clientInfo = $user->loadWithExt(
                array(
                    'login' => $this->_login . '|pmContext=' . $this->_company,
                    'password' => $this->_password
                )
        );


        $FSClientServicesDirectorName = $clientInfo->FSClientServiceDirectorName;
        $FSClientServicesDirectorPhone = $clientInfo->FSClientServiceDirectorPhone;
        //--- access value
        $clientID = $clientInfo->ClientID;
        $companyID = $clientInfo->Company_ID;
        
        $Core_Api_Class = new Core_Api_Class();
        $isGPM = $Core_Api_Class->isGPM($clientID);
  
        $arrayAttribs = split(',', $attributeIds);
        if (!$isGPM) {
			if (count ($arrayAttribs) > 0) {
				$allowAccess = 1;
				for ($i = 0; $i < count($arrayAttribs); $i++) {
					$attributeId = $arrayAttribs[$i];
					$accessValue = $user->GetAccessValueByCompanyAndAttribute($companyID, $attributeId);
					if ($accessValue == $this->_restrictedValue) {
						$allowAccess = 0;
						break;
					}
				}
			}
        }
		else {
			$allowAccess = 1;
		}

        //--- send data to view
        $this->view->dname = $FSClientServicesDirectorName;
        $this->view->dphone = $FSClientServicesDirectorPhone;
        $this->view->access = $allowAccess;
    }
    //13755
    public function accessctlrecentAction() {
        $request = $this->getRequest();
        $allowAccess = 1;
        $attributeIds = $request->getParam('attributeId', 0);
        $company_id =  $request->getParam('companyId'); 
        $user = new Core_Api_User();
        $clientInfo = $user->loadWithExt(
                array(
                    'login' => $this->_login . '|pmContext=' . $this->_company,
                    'password' => $this->_password
                )
        );


        $FSClientServicesDirectorName = $clientInfo->FSClientServiceDirectorName;
        $FSClientServicesDirectorPhone = $clientInfo->FSClientServiceDirectorPhone;
        //--- access value
        $clientID = $clientInfo->ClientID;
        $companyID = $clientInfo->Company_ID;
        
        $Core_Api_Class = new Core_Api_Class();
        $isGPM = $Core_Api_Class->isGPM($clientID);
  
        $arrayAttribs = split(',', $attributeIds);  
         if (count ($arrayAttribs) > 0) {
              for ($i = 0; $i < count($arrayAttribs); $i++) {
                $attributeId = $arrayAttribs[$i];       
                $accessValue = $user->GetAccessValueByCompanyAndAttribute($company_id, $attributeId);
                if ($accessValue == $this->_restrictedValue) {
                  $allowAccess = 0;
                  break;
                }
              }
        }
        
        $directorName = empty($FSClientServicesDirectorName)?"":", ".$FSClientServicesDirectorName."";
        $directorPhone = empty($FSClientServicesDirectorPhone)?"":", at ".$FSClientServicesDirectorPhone;
        $msg = "Your access to this area has been restricted. ";
        $msg .= "Please contact your FieldSolutions Client Services Director".$directorName;
        $msg .= $directorPhone." for assistance.";
        
        if($isGPM){
            $isGPM =1;
        }else{
            $isGPM = 0;
        }
        $arrAccess = array('access'=>$allowAccess,'gpm'=>$isGPM,'msg'=>$msg);
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        echo json_encode($arrAccess);
        $this->_helper->viewRenderer->setNoRender(true);
    }
    //End 13755
    //--------------------------------------------------------
    public function getstatisAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $contry_id = $this->getRequest()->getParam('contry_id', null);
        $return = array();
        $client = new Core_Api_Class();

        $countrycodes = array("US","CA","MX","BR","UK","KY","BS");
        $apiCommon = new Core_Api_CommonClass();   
        $states = $apiCommon->getStatesArray("$contry_id");
        $options = '<option data="0"  statecode="" contrycode="" value="">Select State</option>';
        if(!empty($states->data) && in_array($contry_id,$countrycodes))
        {
            $states = $apiCommon->getStatesArray("$contry_id");
            foreach ($states->data as $stateCode => $stateVal) {
                $options .= '<option 
                        contrycode="'.$contry_id.'"  value="' . $stateCode . '">'
                        . $stateVal . '</option>';
            }
            }
        $options .= '<option data="2" value="Other">Other</option>';

        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();

        $response->setHeader('Content-Length', strlen($return), true);
        $response->setBody($options);
    }

    /**
     * updaterequitephone
     */
    public function updaterequitephoneAction() {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getRequest()->getParams();
        $id = $params['id'];
        $phonetype = $params['phonetype'];
        $Core_Api_TechClass = new Core_Api_TechClass;
        $aClient = null;
        $requite = 0;
        switch ($phonetype) {
            case 1:
                $aClient = $Core_Api_TechClass->setPrimaryPhoneInvalid($id, $this->_login);
                if (!empty($aClient)) {
                    $requite = $aClient[0]['PrimaryPhoneStatus'];
                }
                break;
            case 2:
                $aClient = $Core_Api_TechClass->setSecondaryPhoneInvalid($id, $this->_login);
                if (!empty($aClient)) {
                    $requite = $aClient[0]['SecondaryPhoneStatus'];
                }
                break;
        }

        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $response->setHeader('Content-Length', strlen($requite), true);
        $response->setBody($requite);
    }
    public function sendemailtofsAction()
    {
        $request = $this->getRequest(); 
        $clientId = $request->getParam('clientId', 0);          
        $techId = $request->getParam('techId', 0);
        $success=0;
        if(!empty($clientId) && !empty($techId))
        {
            $api = new Core_Api_User;
            $result = $api->sendEmailToFS($clientId,$techId);
            $this->view->success = 1;
        }
        else
        {
            $this->view->success = 0;
        }
    }

	public function getIsoInfoAction(){
		$this->_helper->viewRenderer->setNoRender(true);
		 $request = $this->getRequest(); 
		 $isoId = $request->getParam("isoId");
		 
		 if(!empty($isoId)){
		 	$iso = new Core_ISO();
		 	$iso->load($isoId);
		 	$return =  $iso->toArray();
		 }else{
		 	return false;
}
		 
	    $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $return = json_encode($return);

        $response->setHeader('Content-Length', strlen($return), true);
        $response->setBody($return);
	}

	function workOrderSettingsAction() {
            $Core_Api_User = new Core_Api_User;
            if (!empty($_SESSION['PM_Company_ID'])) {
               $this->_companyId =  $_SESSION['PM_Company_ID'];
			}
            else
            {
                $user = new Core_Api_User();
                $authData = array('login'=>$this->_login . '|pmContext=' . $this->_companyId, 'password'=>$this->_password);
                $user->load($authData);
                $this->_companyId =  $user->getCompanyId();
            }    

            $data = $Core_Api_User->getClientCompanyBCats_insertIfNotExists($this->_companyId);
            $EnableColspanTechPay = $Core_Api_User->getClientCompanyAdditionalFields($this->_companyId);
            
            $clientInfo = $Core_Api_User->loadWithExt(
                array(
                    'login' => $this->_login . '|pmContext=' . $this->_company,
                    'password' => $this->_password
                )
            );
            $clientID = $clientInfo->ClientID;
            $AccountManagerInfo=$Core_Api_User->getAccountManagerInfo($clientID);
            $this->view->name = $AccountManagerInfo['name'];
            $this->view->email = $AccountManagerInfo['email'];
            $this->view->phone = $AccountManagerInfo['phone'];
            if(!empty($EnableColspanTechPay))
            {
                $EnableColspanTechPay = $EnableColspanTechPay["enable_expense_reporting"];
            }
            if($_POST)
            {
                $this->_helper->viewRenderer->setNoRender(true);
                $request = $this->getRequest(); 
                
                $action = $request->getParam('actiontype', "");
                if($action == "TechPay")
                {   
                    $WorkOrderSettingsTechPay = $request->getParam('WorkOrderSettingsTechPay', array());
                    if(!empty($WorkOrderSettingsTechPay))
                    {
                        for($i = 0;$i < count($WorkOrderSettingsTechPay["bcatid"]); $i++)
                        {
                            if(!empty($WorkOrderSettingsTechPay["bcatname"][$i]))
                            {    
                                $fieldvals = array();
                                $fieldvals["bcatid"] = $WorkOrderSettingsTechPay["bcatid"][$i];
                                $fieldvals["bcatname"] = $WorkOrderSettingsTechPay["bcatname"][$i];
                                $fieldvals["bcatdesc"] = $WorkOrderSettingsTechPay["bcatdesc"][$i];
                                $fieldvals["ischecked"] = $WorkOrderSettingsTechPay["ischecked"][$i];
                                $fieldvals["reimbursable_thru_fs"] = $WorkOrderSettingsTechPay["reimbursable_thru_fs"][$i];
                                $fieldvals["require_doc_upl"] = $WorkOrderSettingsTechPay["require_doc_upl"][$i];
                                $fieldvals["bcattype"] = $WorkOrderSettingsTechPay["bcattype"][$i];
                                $fieldvals["companyid"] = $this->_companyId;

                                $Core_Api_User->saveClientCompanyBCat($fieldvals["bcatid"],$fieldvals);
                            }
                        }  
                    }
                    // save mun
                    $EnableExpenseReportingTechPay = $request->getParam('EnableExpenseReportingTechPay',0);
                    $fieldvals = array();
                    $fieldvals["enable_expense_reporting"] = $EnableExpenseReportingTechPay;
                    $Core_Api_User->saveClientCompanyAdditionalFields($this->_companyId,$fieldvals);
                    if($EnableColspanTechPay==0 && $EnableExpenseReportingTechPay ==1)
                    {
                        
                        
                        $sendthis = new Core_Mail();
                        
                        $message="Please be advised that user <b>$clientInfo->UserName</b> has enabled Expense Reporting for <b>$clientInfo->CompanyName</b>. 
Please ensure that the default pricing rule is correct (includes a markup for Expense Reporting) at the client level.
Thank you.
";
                        $vFromEmail = "no-replies@fieldsolutions.com";
                        $vFromName = "FieldSolutions";
                        $eList = $AccountManagerInfo['email'];
                        $vSubject = "Expense Reporting enabled for $clientInfo->CompanyName";
                        
                        $sendthis->setBodyText($message);
                        $sendthis->setFromEmail($vFromEmail);
                        $sendthis->setFromName($vFromName);
                        $sendthis->setToEmail($eList);
                        $sendthis->setToName($eList);
                        $sendthis->setSubject($vSubject);
                        $sendthis->send();    
                	}
                }
                $this->_helper->json(array('success' => true));
            }
            $this->view->data = $data;
            
            $this->view->EnableColspanTechPay = $EnableColspanTechPay;
            $apiuser = new Core_Api_User();
            $Repclientid = $apiuser->getRepClientIDforCompany($this->_companyId);
            $techworkcontrol = $apiuser->getClientExt($Repclientid);
            $this->view->techworkcontrol = $techworkcontrol;
            
            $client_login_old = $this->_login;
            $client = new Core_Api_Class();
            $isPM = $client->isGPMByUserName($client_login_old);
            $this->view->isGPM = $isPM?1:0;
        }
		
    function workOrderTechControlsAction()
    {
        $Core_Api_User = new Core_Api_User;
        if (!empty($_SESSION['PM_Company_ID']))
        {
            $this->_companyId = $_SESSION['PM_Company_ID'];
        } else
        {
            $user = new Core_Api_User();
            $authData = array('login' => $this->_login . '|pmContext=' . $this->_companyId, 'password' => $this->_password);
            $user->load($authData);
            $this->_companyId = $user->getCompanyId();
        }

        $this->_helper->viewRenderer->setNoRender(true);
        $request = $this->getRequest();
        $param = $request->getParams();

        $AllowDeVryInternstoObserveWork = $request->getParam('AllowDeVryInternstoObserveWork')=='on'?1:0;
        $AllowDeVryInternstoPerformField = $request->getParam('AllowDeVryInternstoPerformField')=='on'?1:0;
        $chooseval = $request->getParam('chooseval');

        $apiuser = new Core_Api_User();
        $apiuser->saveClientTechnicianWorkControls($this->_companyId, $AllowDeVryInternstoObserveWork, $AllowDeVryInternstoPerformField, $chooseval);

        $this->_helper->json(array('success' => true));
    }
        
		private function processMyDashboardSettingsSort($name, $saveInfo) {
			$sort = $this->getRequest()->getParam($name, null);
			$dir = $this->getRequest()->getParam($name . "Dir", null);
			$name = "default" . ucfirst($name);
			if ($sort !== null)
				$saveInfo[$name] = $sort;
			if ($dir == 'asc')
				$dir = Core_DashboardSettings::SORT_DIR_ASC;
			else if ($dir == 'desc')
				$dir = Core_DashboardSettings::SORT_DIR_DESC;
			else 
				$dir = null;
			if ($dir !== null)
				$saveInfo[$name . "Dir"] = $dir;
			return $saveInfo;
		}
	
	/**
     * Download CSV File Based on FS-Mapper
     * @param string $type
     * @throws Zend_Exception
     */
     public function mapperDownloadAction(){
     	$params = $this->getRequest()->getParams();
     	$search = new Core_Api_Class();
     	$techSearch = new API_Tech();
	      try {
	      	$this->_helper->viewRenderer->setNoRender(true);
	      	$wins = array();
	      	$tids = array();
	      	if($params['mappedAroundWos'] == true){
	      		foreach($params as $k=>$v){
	      			if(strpos($k,"win-") !== false){
	      				$wins[] = substr($k,4);
	      				foreach($v as $techId){
	      					$tids[] = $techId;
	      				}
	      			}
	      		}
	      		$wos = $search->getWorkOrdersWithWinNum($wins,self::FIELDS, 'WIN_NUM desc');
	      		$techs = $techSearch->lookupID($tids);
	      		
	      		foreach($params as $k=>$v){
	      			if(strpos($k,"win-") !== false){
	      				$count = 0;
	      				foreach($wos as $w){
	      					if($wos[$count]['WIN_NUM'] == substr($k,4)){
			      				foreach($v as $techId){
			      					$wos[$count]['techsMappedAroundWos'][] = $techs[$techId];
			      				}
	      					}
	      				$count++;
	      				}
	      			}
	      		}
	      		$csvMapper = new Core_CSV_Mapper($wos,"", $params['projectMapping'],true);
		      	return $this->_outputCsv($csvMapper);
	      	}else{
		      	//Get WO's based on WIN_NUM's
		      	$wins = explode("|", $params['wos']);
		      	$wos = $search->getWorkOrdersWithWinNum($wins,self::FIELDS, 'WIN_NUM desc');
		      	
		      	//Get Techs based on Tech_ID's
		      	$tids = explode("|", $params['techs']);
		      	
		      	$techs = $techSearch->lookupID($tids);
		        $csvMapper = new Core_CSV_Mapper($wos,$techs, $params['projectMapping']);
		      	return $this->_outputCsv($csvMapper);
	      	}
	      } catch (Core_CSV_Exception $e) {
	      		error_log('error occurred');
	     		return false;
	     }		     	
     }
     
	private function _outputCsv(Core_CSV_Mapper $csvMapper){
        $this->_helper->viewRenderer->setNoRender();
        $response = $this->getResponse();
        $response->clearHeaders();
        $response->clearBody();
        $response->setHeader('Pragma', 'public')
                ->setHeader('Expires', '0')
                ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
                ->setHeader('Cache-Control', 'private')
                ->setHeader('Content-Type', 'text/csv')
                ->setHeader('Content-Disposition', 'attachment; filename=FS_Mapper_'.date('mdY').'.csv', true)
                ->setHeader('Content-Transfer-Encoding', 'binary');
        $response->sendResponse();

        //Flush buffers
        ob_end_flush();
        flush();

        $csvMapper->printCSV();
        exit;
    }
    
    public function getWoBidsByWinAction(){
    	$this->_helper->viewRenderer->setNoRender();
    	 $response = $this->getResponse();
        $response->setHeader('Content-Type', 'text/json', true);
        
    	$params = $this->getRequest()->getParams();
    	
    	if($params['win'] == "" || $params['win'] == NULL) return false;
    	
    	$db = Core_Database::getInstance();
		$query = $db->select();
		$query->from(array("b"=>Core_Database::TABLE_WORK_ORDER_BIDS), array('b.*'));
		$query->joinLeft(array("t" => Core_Database::TABLE_TECH_BANK_INFO), "t.id = (SELECT id FROM TechBankInfo AS z WHERE b.TechID = z.TechID LIMIT 1)", array("t.*"));
		$query->where('WorkOrderID = ?', $params['win']);
		
		if (!$showBanned)
		    $query->where('Hide = ?', 0);
		
		$result = Core_Database::fetchAll($query);
		if(!$result){
			$jsonContext = json_encode(array('error' => 1, 'msg' => 'Could not find bids'));
		}else{
			$jsonContext = json_encode($result);
		}
		$response->setHeader('Content-Length', strlen($jsonContext), true)
                ->setBody($jsonContext);
    }
    
    public function getTechsByProjectAction(){
    	$this->_helper->viewRenderer->setNoRender();
    	$response = $this->getResponse();
        $response->setHeader('Content-Type', 'text/json', true);
        
    	$params = $this->getRequest()->getParams();
    	
    	if($params['projects'] == "" || $params['projects'] == NULL) return false;
    	
    	$techsSearch = new Core_Api_TechClass();
    	
    	$result = $techsSearch->getTechsByProject($params['projects']);
		$searchResultMapperTotal = count($result);
		
		if(!$result){
			$jsonContext = json_encode(array('error' => 1, 'msg' => 'Could not find techs'));
		}else{
			$returnArr = array("mapView"=>$result, "mapViewTotal"=>$searchResultMapperTotal);
			$jsonContext = json_encode($returnArr);	
		}
		$response->setHeader('Content-Length', strlen($jsonContext), true)
                ->setBody($jsonContext);
    }
    
    public function getTechsByBidsAction(){
    	$this->_helper->viewRenderer->setNoRender();
    	$response = $this->getResponse();
        $response->setHeader('Content-Type', 'text/json', true);
        
    	$params = $this->getRequest()->getParams();
    	
    	if($params['projects'] == "" || $params['projects'] == NULL) return false;
    	
    	$techsSearch = new Core_Api_TechClass();
    	    	
    	$result = $techsSearch->getTechsByBids($params['projects']);
		$searchResultMapperTotal = count($result);
		
		if(!$result){
			$jsonContext = json_encode(array('error' => 1, 'msg' => 'Could not find techs'));
		}else{
			$returnArr = array("mapView"=>$result, "mapViewTotal"=>$searchResultMapperTotal);
			$jsonContext = json_encode($returnArr);	
		}
		$response->setHeader('Content-Length', strlen($jsonContext), true)
                ->setBody($jsonContext);
    	
    }
    
	public function regionBundleSearchAction() {
		$params = $this->getRequest()->getParams();
       
        $filters = $this->getWOSearchCritera($params);
        
		$filters->Company_ID = $params['company'];
		
		$client = new Core_Api_WosClass();
        /*
		$result = $client->getWorkOrders(
            $this->_login.'|pmContext='.$this->_company,
            $this->_password,
           "Region,Route", null, null, null, $filters
        );
        */
        $result = $client->getRegionBundleByCriteria($filters);
        
        if (!empty($result)) {
          
           // $this->view->result = $res;
           // $this->view->errors = null;
    
            $matches = array();
            if($params['region'] == true){
				foreach ($result as $wo) {
					if($wo['Region'] != "" && $wo['Region'] != null)$matches[] = array("label" => $wo['Region'], "value" => $wo['Region']);  
		    	}
            }else{
            foreach ($result as $wo) {
					if($wo['Bundle'] != "" && $wo['Bundle'] != null)$matches[] = array("label" => $wo['Bundle'], "value" => $wo['Bundle']);  
		    	}
            }
			$this->_helper->json($matches);
        } else {
            $this->view->errors = $result->errors;
        }
	}
	
	public function bundleInviteAction() {
		$params = $this->getRequest()->getParams();
		$this->view->wins = $params["wins"];
		$this->view->eList = $params["eList"];
		$this->view->highlight = $params["highlight"] == 1;
		$api = new Core_Api_WosClass;
		$bundleName = $api->getBundleForWorkOrders($params["wins"]);
		$this->view->bundleName = empty($bundleName) ? "" : $bundleName;
	}
	public function bundleAssignAction() {
		$params = $this->getRequest()->getParams();
		$this->view->wins = $params["wins"];
		$this->view->wosList = $params["wosList"];
		$api = new Core_Api_WosClass;
		$bundleName = $api->getBundleForWorkOrders($params["wins"]);
		$this->view->bundleName = empty($bundleName) ? "" : $bundleName;
	}

	public function doBundleAction() {
		$params = $this->getRequest()->getParams();
		$api = new Core_Api_WosClass;
		$api->setBundleForWorkOrders($params["name"], $params["wins"]);
		$this->_helper->json(array("success" => true));
	}

	private function getWOSearchCritera($params)
    {
        $filters = new API_WorkOrderFilter;
        
        if (!empty($params['WorkOrder'])) {
        	$filters->WO_State = $params['WorkOrder'];
        }
        
        $companyID = '';
        if (!empty($_SESSION['Company_ID'])) {
            $companyID = $_SESSION['Company_ID'];
        }
        if (!empty($params['PMCompany'])) {
            $companyID = $params['PMCompany'];
        }
        $filters->Company_ID = $companyID;
        
        $filters->Region = $params['Region'];
        $filters->Route = $params['Route'];
        

        if (!empty($params['Project']) && ($params['Project'] > 0)) {
            $filters->Project_ID = (strpos($params['Project'], ",") != false) ? explode(",", $params['Project']) : $params['Project'];
        }
        
        if (!empty($params['StartDate'])) {
            $date = new Zend_Date($params['StartDate'], 'MM/dd/yyyy');
            $filters->StartDateFrom = $date->toString('yyyy-MM-dd');
        }
        
        if (!empty($params['EndDate'])) {
            $date = new Zend_Date($params['EndDate'], 'MM/dd/yyyy');
            $filters->StartDateTo = $date->toString('yyyy-MM-dd');
            
        }
        
        if (!empty($params['category'])) {
            $filters->WO_Category = (int)$params['category'];
        }
        
   		 if (!empty($params['TB_UNID'])) {
            $filters->TB_UNID = $params['TB_UNID'];
        }
        
        return $filters;
    }
    function getCountAction(){
        $this->_helper->viewRenderer->setNoRender(true);
        $db = Core_Database::getInstance ();
        $select = $db->select ();
        $select->from (Core_Database::TABLE_WORK_ORDERS,"count(*) as Count");
        $file_row = $select->query()->fetch();
        $this->_helper->json(array("success" => $file_row));
}
    function myNoticesAction(){
        $params = $this->getRequest()->getParams();
        $this->view->FormMenu = (int)$params["menu"];
        $this->view->ajax = (int)$params["ajax"];
        $this->view->pageNotices = (int)$params["pageNotices"];
        $this->view->pageNotices = empty($this->view->pageNotices)?1:$this->view->pageNotices;
        $Core_Api_NoticeClass = new Core_Api_NoticeClass();
        $user = new Core_Api_User();
        $userInfo = $user->load(
                array(
                    'login' => $this->_login . '|pmContext=' . $this->_company, 'password' => $this->_password)
        );
        $client_id = $userInfo->getClientId();
        $this->view->Notices = $Core_Api_NoticeClass->getNotices($client_id);
        $Core_Api_NoticeClass->saveLastViewDatetime($client_id);
    }
    function getcountnoticesnotviewyetAction(){
        $this->_helper->viewRenderer->setNoRender(true);
        $core = new Core_Api_NoticeClass();
        $countNotices = $core->getCountNotices_NotViewYet($_SESSION['ClientID']);
        $htmlNumberBotice = "";
//13701
        if(empty($countNotices)){
            $htmlNumberBotice = "<div style='float: left;'>Inbox&nbsp;</div>";
        }else if($countNotices < 10){
            $htmlNumberBotice = "<div style='float: left;'>Inbox&nbsp;</div><div class='backgroundNoticeNumBer backgroundNoticeNumBerOne'>$countNotices</div>";
        }else if($countNotices < 100){
            $htmlNumberBotice = "<div style='float: left;'>Inbox&nbsp;</div><div class='backgroundNoticeNumBer backgroundNoticeNumBerTwo'>$countNotices</div>";
        }else{
            $htmlNumberBotice = "<div style='float: left;'>Inbox&nbsp;</div><div class='backgroundNoticeNumBer backgroundNoticeNumBer99'>99+</div>";
}
        $this->_helper->json(array("success" => $htmlNumberBotice));
    }
    
    //13777
    function checkusernameAction(){
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getRequest()->getParams();
        
        $api = new Core_Api_User();
        $isDuplUserName = $api->isDuplicated_Username($params["userName"], $params["companyID"]);

        if($isDuplUserName){
            if($params["inline"]==1){
                $errMsg = 'User Name already exists.';
            } else {
                $errMsg = 'Selected User Name already exists.';
            }
        } else {
            $errMsg = '';            
        }
        $this->_helper->json(array("errMsg" => $errMsg));        
    
    }
    //743
    function isRestrictedAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getRequest()->getParams();
        if(isset($params['v']))
        {
        $companyid = $params['v'];
        }
        else if(isset($params['winnum']))
        {
            $db = Core_Database::getInstance();
            $win = trim($params['winnum']);
            $wo = $db->fetchRow("SELECT WIN_NUM,Company_ID FROM " . Core_Database::TABLE_WORK_ORDERS . " WHERE WIN_NUM=?", array($win));
            $companyid = $wo['Company_ID'];
        }
        
        $user = new Core_Api_User();
        
        $isRestricted=$user->AreFullorPartialRestriction($companyid);
        $clients = $user->getRepClientforCompany($companyid);

        $clientInfo = $user->loadWithExt(
                        array(
                            'login' => $clients[0]['UserName'],
                            'password' => $clients[0]['Password']
                        ));
        $this->_helper->json(array('restrict' => $isRestricted, 'cds' => $clientInfo->FSClientServiceDirectorName,'am' => $clientInfo->FSAccountManagerName));
    }
    //end 743
}
