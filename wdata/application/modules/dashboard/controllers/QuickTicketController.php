<?php

class Dashboard_QuickTicketController extends Core_Controller_Action {

    var $companyId = "";
    var $companyname = "";
    var $ClientID = 0;
    var $emailclient = "";
    var $emailclientsend = "";
    var $ContactName = "";

    public function init() {
        parent::init();
        $user = new Core_Api_User();
        $request = $this->getRequest();
        $params = $request->getParams();
        if (isset($params['client_id'])) {
            $id = $params['client_id'];
            $clientInfo = $user->loadById(
                    $id
            );
            if (!empty($clientInfo)) {
                $this->companyId = $clientInfo->Company_ID;
                $this->companyname = $clientInfo->CompanyName;
                $this->emailclient = $clientInfo->Email1;
                $this->emailclientsend = $clientInfo->Email1;
                $this->ContactName = $clientInfo->ContactName;
            }
            $this->ClientID = (int) $params['client_id'];
        } else {
            if (!empty($_SESSION['PM_Company_ID'])) {
                $clients = $user->getRepClientforCompany($_SESSION['PM_Company_ID']);
                $client_id = $clients[0]['ClientID'];
                $client_login = $clients[0]['UserName'];
                $client_password = $clients[0]['Password'];
                $pmContext = '|pmContext=' . $_SESSION['PM_Company_ID'];
                $this->ClientID = $client_id;
                $this->companyId = $_SESSION['PM_Company_ID'];
                $this->companyname = $clients[0]['CompanyName'];
                $this->emailclient = $clients[0]['Email1'];
                $this->ContactName = $clients[0]['ContactName'];
            } else {
                $client_login = $this->_login;
                $client_password = $this->_password;
                $clientInfo = $user->loadWithExt(
                        array(
                            'login' => $client_login,
                            'password' => $client_password
                        )
                );
                $this->ClientID = $clientInfo->ClientID;
                if (!empty($clientInfo)) {
                    $this->companyId = $clientInfo->Company_ID;
                    $this->companyname = $clientInfo->CompanyName;
                    $this->emailclient = $clientInfo->Email1;
                    
                }
            }
            $clientInfo = $user->loadWithExt(
                        array(
                            'login' => $this->_login,
                            'password' => $this->_password
                        )
            );
            $this->ContactName = $clientInfo->ContactName;
            $this->emailclientsend = $clientInfo->Email1;
        }
        $this->view->companyname = $this->companyname;
        $this->view->companyId = $this->companyId;
        $this->view->emailclient = $this->emailclient;
    }

    public function clientListAction() {
        $Core_Api_CustomerClass = new Core_Api_CustomerClass();
        $data = $Core_Api_CustomerClass->getQTVListForCompanyId($this->companyId);
        $this->view->data = $data;
        $this->view->Core_Api_CustomerClass = $Core_Api_CustomerClass;
    }

    public function clientEditAction() {
        $Core_Api_CustomerClass = new Core_Api_CustomerClass();
        $data = $Core_Api_CustomerClass->getQTVListForCompanyId($this->companyId);
        $this->view->data = $data;
    }

    public function clientSaveAction() {
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();
        $params = $request->getParams();
        $Core_Api_CustomerClass = new Core_Api_CustomerClass;
        $StrJson = $params['json'];

        if (trim($StrJson) != "") {
            $datas = json_decode($StrJson);
            if (count($datas) > 0) {
                foreach ($datas as $item) {
                    $Core_Api_CustomerClass->saveQTV(
                            $item->id, $this->companyId, base64_decode($item->value), $item->active, "");
                }
            }
        }
        $this->_helper->json(array('success' => true));
    }

    public function clientProjectEditAction() {
        $request = $this->getRequest();
        $params = $request->getParams();

        $Core_Api_CustomerClass = new Core_Api_CustomerClass();
        $data = $Core_Api_CustomerClass->getPrjListForCompanyIdWithQTVCheck($this->companyId, $params['id']);
        $this->view->id = $params['id'];
        $this->view->data = $data;
    }

    public function clientSaveProjectEditAction() {
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();
        $params = $request->getParams();
        $prjIds = $params['proid'];
        $id = $params['id'];
        $Core_Api_CustomerClass = new Core_Api_CustomerClass();
        $data = $Core_Api_CustomerClass->savePrjSelectedForQTV($id, $prjIds);
        $this->_helper->json(array('success' => true));
    }

    public function clientUserAddAction() {
        $request = $this->getRequest();
        $params = $request->getParams();

        $this->view->CompanyName = $params['CompanyName'];
        $this->view->CustomerName = $params['CustomerName'];
        $this->view->id = $params['id'];
        $this->view->companyId = $this->companyId;
    }

    public function clientSaveUserAddAction() {

        $errorMessages = new Core_Form_ErrorHandler('createCustomer');
        $this->_helper->viewRenderer->setNoRender(true);
        $return = array('success' => 0);
        $errorMessages->clearMessages();

        $user = new Core_Api_User();

        $formFields = $this->getRequest()->getParam('CreateForm', array());
        if (!empty($formFields['Password'])) {
            $userInfo = $user->load(
                    array(
                        'login' => $this->_login . '|pmContext=' . $this->_company, 'password' => $this->_password)
            );
            $cid = $userInfo->contextId;
            if (empty($cid)) {
                // wrong password confirmation
                $return['success'] = 0;
                $needUpdate = false;
                $return['errors'][] = 'Wrong Password';
            }

            if (empty($formFields['Password']) || $formFields['Password'] != $formFields['PasswordConfirm']) {
                $return['success'] = 0;
                $needUpdate = false;
                if (empty($return['errors']))
                    $return['errors'] = array();
                $return['errors'][] = 'Incorrect Password Confirmation';
            }
        }

        if (empty($formFields['Password'])
                || empty($formFields['PasswordConfirm'])
                || $formFields['Password'] != $formFields['PasswordConfirm']
        ) {
            // clear PasswordConfirm

            unset($formFields['PasswordConfirm']);
        }


        $search = new Core_Api_Class();
     
        $result = $search->createCustomer($this->_login
                . '|pmContext=' . $this->_company, $this->_password, $formFields);

        $errors = Core_Api_Error::getInstance();
        if (!$result->success) {
            foreach ($result->errors as $error) {
                $return['success'] = 0;
                $errorMessages->addMessage($error->message);
            }
        } else {
            $Core_Api_CustomerClass = new Core_Api_CustomerClass();
            $Core_Api_CustomerClass->addCusUserToQTV($formFields['Username'], $this->getRequest()->getParam('hidQTid', 0));
            $return['success'] = 1;
            // send email
            if((int)$formFields['SendWelcomeEmail'] == 1)
            {    
                $name = $formFields['Contact_Name'];
                $user = $formFields['Username'];
                $pass = $formFields['Password'];
                $CustomerName = $formFields['Customer_Name'];
                $message = "Dear $name,

                $this->ContactName at $this->companyname has created an account for you to view the work they are running for $CustomerName:
                User Name: $user
                Password: $pass

                Please sign in at <a href='http://www.fieldsolutions.com/quickticket'>www.fieldsolutions.com/quickticket</a> to get started. 

                Thank you
                ";
                $this->mail = new Core_Mail();
                $this->mail->setBodyText($message);
                $this->mail->setFromEmail($this->emailclientsend);
                $this->mail->setFromName($this->ContactName);
                $this->mail->setToEmail($formFields['Contact_Email']);
                $this->mail->setToName($name);
                $this->mail->setSubject("Welcome to QuickTicket!");
                $this->mail->send();
            }
        }

        $err = $errorMessages->getMessages();
        $return['errors'] = $err;

        //$this->_helper->json($return);
    }

    public function clientUserEditAction() {
        $request = $this->getRequest();
        $params = $request->getParams();

        $this->view->CompanyName = $params['CompanyName'];
        $this->view->CustomerName = $params['CustomerName'];
        $this->view->id = $params['id'];
        $this->view->companyId = $this->companyId;

        $user = new Core_Api_CustomerUser();
        $id = $this->getRequest()->getParam('userid', null);
        $customerInfo = $user->loadById($id);

        $this->view->customer = $customerInfo;
        $this->view->success = $customerInfo !== false;

        $errors = Core_Api_Error::getInstance();

        $this->view->messages = $error->message;
    }

    public function clientSaveUserEditAction() {

        $updateForm = $this->getRequest()->getParam('UpdateForm', null);
        $id = $this->getRequest()->getParam('Customer', null);

        $errorMessages = new Core_Form_ErrorHandler('updateCustomer_' . $id);
        $errorMessages->clearMessages();
        $return = array('success' => 0);
        if (empty($updateForm) || empty($id))
            $errorMessages->addMessage('Update Client Form is empty');
        $er = $errorMessages->getMessages();

        $cid = $id;
        if (!empty($cid)) {
            $user = new Core_Api_CustomerUser($cid);
            $user->loadById($cid);

            $datatoupdate = array();

            foreach ($updateForm as $field => $val) {
                if ($val != $user->$field)
                    $datatoupdate[$field] = $val;
            }
//echo("datatoupdate:<pre>");print_r($datatoupdate);
            if (!empty($datatoupdate)) {
                $result = $user->save($datatoupdate);
                $return['success'] = 1;
            }
        }

        $err = $errorMessages->getMessages();

        $return['errors'] = $err;

        $this->_helper->json($return);
    }

    public function clientUserDeleteAction() {
        $return = array('success' => 1);

        $request = $this->getRequest();
        $params = $request->getParams();
        $id = $params['id'];
        $Core_Api_CustomerClass = new Core_Api_CustomerClass();
        $data = $Core_Api_CustomerClass->removeCusUser($id);

        $this->_helper->json($return);
    }

    public function clientSavecustomsettingAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $request = $this->getRequest();
        $params = $request->getParams();
        
        switch($params['typevalue'])
        {
            case 1:
                $field ='EnableCreateWO';
                break;
            case 2:
                $field ='ShowClientPONum';
                break;
            case 3:
                $field ='ShowTechDirectContactInfo';
                break;
            case 4:
                $field ='ShowTechComments';
                break;
            case 5:
                $field ='ShowTechPayDetails';
                break;
                
        }
            
        $id = $params['itemid'];
        $value = $params['checked'];
        
        $CSapi = new Core_Api_CustomerClass();
        
        return $CSapi->updateQTV_1f($id,$field,$value);
    }
    public function adminListAction() {
        $Core_Api_CustomerClass = new Core_Api_CustomerClass();
        $data = $Core_Api_CustomerClass->getQTVListForCompanyId($this->companyId);
        $this->view->data = $data;
        $this->view->Core_Api_CustomerClass = $Core_Api_CustomerClass;
    }

}
