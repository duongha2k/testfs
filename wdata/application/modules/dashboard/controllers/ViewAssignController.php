<?php
require_once 'API/WorkOrderFilter.php';
require_once 'Core/Api/Class.php';

class Dashboard_ViewAssignController extends Core_Controller_Action
{
    const FIELDS = 'WIN_NUM,WO_ID,DateEntered,PO,ShowTechs,SourceByDate,ShortNotice,StartDate,StartTime,EndDate,EndTime,StartRange,Duration,Region,Type_ID,Project_ID,WO_Category_ID,Headline,Site_Contact_Name,SiteName,SiteNumber,Project_Name,Address,City,State,Zipcode,SitePhone,SiteFax,SiteEmail,Update_Requested,Update_Reason,Description,Requirements,Tools,SpecialInstructions,Lead,Assist,SignOffSheet_Required,ProjectManagerName,ProjectManagerPhone,ProjectManagerEmail,ResourceCoordinatorName,ResourceCoordinatorPhone,ResourceCoordinatorEmail,EmergencyName,EmergencyPhone,EmergencyEmail,TechnicalSupportName,TechnicalSupportPhone,TechnicalSupportEmail,CheckInOutName,CheckInOutNumber,CheckInOutEmail,Tech_ID,Tech_FName,Tech_LName,NoShow_Tech,BackOut_Tech,FLS_ID,BU1FLSID,BU1Info,Backup_FLS_ID,Ship_Contact_Info,PayMax,Amount_Per,Qty_Devices,Tech_Bid_Amount,WorkOrderReviewed,TechCheckedIn_24hrs,CheckedIn,Checkedby,PreCall_1,PreCall_2,CheckInNotes,StoreNotified,Notifiedby,DateNotified,NotifyNotes,TechMarkedComplete,Date_In,Time_In,Date_Out,Time_Out,ContactHD,HDPOC,TechMiles,TechComments,AttachedFile,FLS_OOS,Unexpected_Steps,Unexpected_Desc,AskedBy,AskedBy_Name,Paperwork_Received,Incomplete_Paperwork,MissingComments,Site_Complete,CallClosed,PartsProcessed,ClientComments,Work_Out_of_Scope,OutofScope_Amount,OutofScope_Reason,TripCharge,MileageReimbursement,MaterialsReimbursement,Additional_Pay_Amount,Penalty_Reason,AbortFee,AbortFeeAmount,Add_Pay_AuthBy,Add_Pay_Reason,calculatedTechHrs,baseTechPay,PayAmount,Approved,TechPaid,DatePaid,Deactivated,DeactivationCode,Deactivated_Reason,General1,General2,General3,General4,General5,General6,General7,General8,General9,General10,FS_InternalComments,Username,Invoiced,Company_Name,Company_ID,TechEmail,TechPhone,TechPhoneExt,Date_Assigned,lastModBy,lastModDate,approvedBy,DateApproved,DeactivatedDate,DeactivatedBy,Latitude,Longitude,PricingRan,WO_Category,DateIncomplete,ReminderAcceptance,Reminder24Hr,Reminder1Hr,CheckInCall,CheckOutCall,ReminderNotMarkComplete,ReminderIncomplete,ReminderAll,SMSBlast,MinutesRemainPublished,MinimumSelfRating,MaximumAllowableDistance,isProjectAutoAssign,isWorkOrdersFirstBidder,P2TPreferredOnly,P2TAlreadyAssigned,SATRecommended,SATPerformance,ReturnPartCount,NewPartCount,Qty_Applicants,Status,DateInvoiced,Work_Out_of_Scope,OutOfScope_Reason,Status,PcntDeduct';
	
	protected $_company;
	protected $_filters;

    public function init()
    {
        parent::init();
        //  This is constroller can be used for AJAX only!
        $request = $this->getRequest();

        /* Check for ajax requests only, except 'csv' download */
        if ( !$request->isXmlHttpRequest() ) {
//			$this->_redirect('/');
        }
		if ( NULL !== ($winNums = $request->getParam('winNums', NULL)) ) {
			if ( is_string($winNums) && strpos($winNums, '|', 1) ) {
				$winNums = explode('|', $projects);
				$request->setParam('winNums', $winNums);
			}
		}
		
		$this->_company = ($request->getParam('company', NULL)!==NULL)?$request->getParam('company'):$this->_company;
        $this->view->files        = new Core_Files();
		$this->_filters = new API_WorkOrderFilter;
        //$this->_filters->WO_State = WO_STATE_PUBLISHED;
    }

    public function viewAction()
    {
        $request = $this->getRequest();
		$winNums = $request->getParam('win_nums', NULL);

        $search = new Core_Api_Class;
	
		if (!empty($winNums)) {
			$winNums = explode("|", $winNums);
			$this->_filters->TB_UNID = implode(",", $winNums);
			//--- get workorders 
			$wo = $search->getWorkOrders(
				$this->_login.'|pmContext='.$this->_company,
				$this->_password,
				self::FIELDS,
				'',
				'',
				'',
				$this->_filters,
				0,
				20,
				$countRows,
				''
			);
		}
        if ( $wo->success ) {
            $paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($wo->data,$countRows));
            $paginator->setItemCountPerPage(20);
            $paginator->setCurrentPageNumber(1);
            $this->pullApplicantsCount($paginator);
            $this->view->paginator = $paginator;
        }
		
		$this->view->woCount = $countRows;

		$this->view->company = $this->_company;
		$this->view->noPageControls = true;
        $this->view->success    = $wo->success;
        $this->view->errors     = $wo->errors;
    }

	public function viewWoAction()
    {
        $request = $this->getRequest();
		$winNums = $request->getParam('win_nums', NULL);
		$page = $request->getParam('page', 1);

        $search = new Core_Api_Class;
	
		if (!empty($winNums)) {
			$winNums = explode("|", $winNums);
			$this->_filters->TB_UNID = implode(",", $winNums);
			//--- get workorders 
			$wo = $search->getWorkOrders(
				$this->_login.'|pmContext='.$this->_company,
				$this->_password,
				self::FIELDS,
				'',
				'',
				'',
				$this->_filters,
				($page - 1) * 20,
				20,
				$countRows,
				''
			);
		}
        if ( $wo->success ) {
            $paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($wo->data,$countRows));
            $paginator->setItemCountPerPage($countRows);
            $paginator->setCurrentPageNumber(1);
            $this->view->paginator = $paginator;
        }

		$this->view->company = $this->_company;
		$this->view->count = count($wo->data);
		$this->view->noHeader = true;
		$this->view->noPageControls = true;
        $this->view->success    = $wo->success;
        $this->view->errors     = $wo->errors;
    }

    public function updateWoRoutesAction(){
    	$request = $this->getRequest();
		$routes = $request->getParam('routes', NULL);
		
		
		
    }
    
	private function pullApplicantsCount($wos){
        if(empty($wos)) return;
        $ids = array();
        foreach($wos as $wo) $ids[] = $wo->WIN_NUM;
        if(!empty($ids)){
            $db = Core_Database::getInstance();
            $s = $db->select()
                    ->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('WorkOrderID','c'=>'COUNT(id)'))
                    ->where(Core_Database::TABLE_WORK_ORDER_BIDS.'.WorkOrderID IN (?)', $ids)
                    ->where(Core_Database::TABLE_WORK_ORDER_BIDS.'.Company_ID = ?', $this->_company)
                    ->group(Core_Database::TABLE_WORK_ORDER_BIDS.'.WorkOrderID');
            $res = $db->fetchAll($s);
            $bidsCount = array();
            foreach ($res as $r) $bidsCount[$r['WorkOrderID']] = $r['c'];
        }
        $this->view->bidsCount = (empty($bidsCount)) ? null : $bidsCount;
    }

}
