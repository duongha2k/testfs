<?php

class Dashboard_PopupController extends Core_Controller_Action {

    public function init() {
        parent::init();
        //  This is constroller can be used for AJAX only!
        $request = $this->getRequest();
        if (!$request->isXmlHttpRequest()) {
            //  $this->_redirect('/');
        }
    }

    public function incommentsAction() {
        $mcomments = $this->_getParam('MissingComments');
        $mcomments = htmlspecialchars(trim($mcomments));
        if (empty($mcomments))
            $mcomments = 'No comments';
        $this->view->MissingComments = $mcomments;
    }

    public function netAmountAction() {
        require_once 'Core/Api/TechClass.php';
        require_once 'API/WorkOrderFilter.php';

        $win = $this->_getParam('win');

        if (empty($win)) {
            $this->_helper->viewRenderer->setNoRender(true);

            $response = $this->getResponse();
            $response->clearAllHeaders();
            $response->clearBody();

            $response
                    ->setHttpResponseCode(406)
                    ->setBody('WIN # is required and cannot be empty');
            return;
        }
        $search = new Core_Api_TechClass;
        $wo = $search->getWorkOrder($this->_login, $this->_password, $win);

        if (!$wo->success) {
            $this->_helper->viewRenderer->setNoRender(true);

            $response = $this->getResponse();
            $response->clearAllHeaders();
            $response->clearBody();

            $response
                    ->setHttpResponseCode(406)
                    ->setBody($wo->errors[0]->message);
            return;
        }

        $this->view->wo = $wo->data[0];
    }

    public function techStartTimeAction() {
        require_once 'Core/Api/TechClass.php';
        require_once 'API/WorkOrderFilter.php';

        $win = $this->_getParam('win');
        $techId = $this->_getParam('techId'); 
		//14039
        //viewtype
        $viewtype = $this->_getParam('viewtype'); 
		//End 14039
        if (empty($win)) {
            $this->_helper->viewRenderer->setNoRender(true);

            $response = $this->getResponse();
            $response->clearAllHeaders();
            $response->clearBody();

            $response
                    ->setHttpResponseCode(406)
                    ->setBody('WIN # is required and cannot be empty');
            return;
        }
        $search = new Core_Api_TechClass;
        $tech = $search->techLookup($this->_login, $this->_password, $techId);
        $wo = $search->getWorkOrder($this->_login, $this->_password, $win);

        if (!$wo->success) {
            $this->_helper->viewRenderer->setNoRender(true);

            $response = $this->getResponse();
            $response->clearAllHeaders();
            $response->clearBody();

            $response
                    ->setHttpResponseCode(406)
                    ->setBody($wo->errors[0]->message);
            return;
        }
        //14039
         if(isset($viewtype)){
            $viewtype ='visits';
         }else{
            $viewtype = '';
         }
          $Wosapi = new  Core_Api_WosClass();
          $this->view->woVisit =  $Wosapi->getBaseWOVisitInfoDisplayrules($win);
          $this->view->viewtype =$viewtype;
		  //End 14039
          $this->view->wo = $wo->data[0];
          $this->view->tech = $tech->data[0];
    }

    public function bidSearchWoAction() {
        require_once 'Core/Api/TechClass.php';
        require_once 'API/WorkOrderFilter.php';

        $request = $this->getRequest();
        $win = $request->getParam('win', NULL);
        $techId = $_SESSION['TechID'];//14016 

        if (empty($win)) {

            $this->_helper->viewRenderer->setNoRender(true);

            $response = $this->getResponse();
            $response->clearAllHeaders();
            $response->clearBody();

            $response
                    ->setHttpResponseCode(406)
                    ->setBody('WIN# not found or is no longer available');
            return;
        }

        $paths = $this->getFrontController()->getControllerDirectory();
        if (!empty($paths['dashboard']) && file_exists($paths['dashboard'] . '/IndexController.php')) {
            require_once $paths['dashboard'] . '/IndexController.php';
            $fields = Dashboard_IndexController::FIELDS;
        } else {
            $fields = NULL;
        }

        $filters = new API_WorkOrderFilter;
        $filters->Deactivated = FALSE;
        $filters->ShowTechs = TRUE;
        //$filters->Sourced       = FALSE;
        $filters->Tech = NULL;
        $filters->TB_UNID = $win;

        $search = new Core_Api_TechClass;
        //14016
        if($search->isRestrictedByClient($techId,$win))
        {
            $this->_helper->viewRenderer->setNoRender(true);
            $response = $this->getResponse();
            $response->clearAllHeaders();
            $response->clearBody();
            $response->setBody('This Work Order is unavailable.');
            return;           
        }
        $wo = $search->getWorkOrders($this->_login, $this->_password, $fields, $filters);
        if (!$wo->success) {

            $this->_helper->viewRenderer->setNoRender(true);

            $response = $this->getResponse();
            $response->clearAllHeaders();
            $response->clearBody();

            $response
                    ->setHttpResponseCode(406)
                    ->setBody($wo->errors[0]->message);
            return;
        } else if (!$wo->errors && empty($wo->data)) {

            $this->_helper->viewRenderer->setNoRender(true);

            $response = $this->getResponse();
            $response->clearAllHeaders();
            $response->clearBody();

            $response
                    //->setHttpResponseCode(406)
                    ->setBody('WIN# not found or is no longer available'/* Core_Api_TechClass::ERROR_WO_INVALID_WIN */);
            return;
        }
        //14061
        $this->view->techId =$techId;
        $prj_id = $wo->success ? $wo->data[0]->Project_ID : '';
        $project = $search->getProjects($this->_login, $this->_password, array($prj_id), '');
        $lastwobid = $search->getLastBid($this->_login, $this->_password,$win);
        
        $this->view->wo = $wo->data[0];
        $this->view->prj = $project->success ? $project->data[0] : new stdClass();
        $this->view->lastwobid = $lastwobid;
        $Core_Api_WosClass = new Core_Api_WosClass; 
        $this->view->AllWOVisitList = $Core_Api_WosClass->GetAllWOVisitList($win,1,1);
    }

    public function filtersAction() {
        require_once 'Core/Api/Class.php';
        $ProjectClass = new  Core_Api_ProjectClass;
        $request = $this->getRequest();
        $companyId = $request->getParam('company', NULL);
        $tab = $request->getParam('tab', NULL);

        $search = new Core_Api_Class;

        $cacheKeyFilters = Core_Cache_Manager::makeKey('filters', $companyId, $tab, $this->_login);
        $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');

        if (FALSE == ($projectList = $cache->load($cacheKeyFilters))) {
            $projectList = $search->getAllProjects($this->_login . '|pmContext=' . $companyId, $this->_password);
            if ($projectList->success) {
                $cache->save($projectList, $cacheKeyFilters, array('filter'), LIFETIME_15MIN);
            }
        }
		
        $Core_Api_User = new Core_Api_User;
        $userInfo = $Core_Api_User->load(
                array(
                    'login' => $this->_login . '|pmContext=' . $companyId, 'password' => $this->_password)
        );
		$this->view->isPM = $userInfo->isPM();
		$this->view->usersAll = $search->getClientsList($this->_login, $this->_password, array('AccountEnabled' => 1), "Username");

        $result = $search->getStates();
        if ($result->success) {
            $states = $result->data;
        }

        if (!empty($projectList->data)) {
            usort($projectList->data, 'Core_Controller_Action::_cmpProjects');
        }

        $this->view->states = $states;
        $this->view->projects = $projectList->data;
        $this->view->callTypes = $search->getCallTypes()->data;
        $this->view->companyId = $companyId;
        $this->view->clientId = $userInfo->getClientId();

        $this->view->countries = $this->getCountries();
        
        $this->view->SiteStatusList = $ProjectClass->getSiteStatusList();
    }

    public function findAvailableWorkAction() {
        require_once 'Core/Api/Class.php';

        $request = $this->getRequest();
        $companyId = $request->getParam('company', NULL);
        $tab = $request->getParam('tab', NULL);

        $search = new Core_Api_Class;

        $cacheKeyFilters = Core_Cache_Manager::makeKey('filters', $companyId, $tab, $this->_login);
        $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');

        if (FALSE == ($projectList = $cache->load($cacheKeyFilters))) {
            $projectList = $search->getAllProjects($this->_login . '|pmContext=' . $companyId, $this->_password);
            if ($projectList->success) {
                $cache->save($projectList, $cacheKeyFilters, array('filter'), LIFETIME_15MIN);
            }
        }

        $result = $search->getStates();
        if ($result->success) {
            $states = $result->data;
        }

        if (!empty($projectList->data)) {
            usort($projectList->data, 'Core_Controller_Action::_cmpProjects');
        }

        $this->view->states = $states;
        $this->view->projects = $projectList->data;
        $this->view->companyId = $companyId;
        $this->view->countries = $this->getCountries();
    }

    public function techFiltersAction() {
        $request = $this->getRequest();
        $tab = $request->getParam('tab', NULL);

        $search = new Core_Api_Class;

        $result = $search->getStates();
        if ($result->success) {
            $states = $result->data;
        }
        $this->view->states = $states;
        $this->view->callTypes = $search->getCallTypes()->data;

        $this->view->countries = $this->getCountries();
    }

    public function findworkorderAction() {

        require_once 'Core/Api/Class.php';

        $request = $this->getRequest();
        $companyId = $request->getParam('company', NULL);
        $mapper = $request->getParam('mapper', NULL);

        $search = new Core_Api_Class;

        $cacheKeyFilters = Core_Cache_Manager::makeKey('findworkorder', $companyId, $this->_login);
        $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');

        if (FALSE == ($projectList = $cache->load($cacheKeyFilters))) {
            $projectList = $search->getAllProjects($this->_login . '|pmContext=' . $companyId, $this->_password);
            if ($projectList->success) {
                $cache->save($projectList, $cacheKeyFilters, array('filter'), LIFETIME_15MIN);
            }
        }

        $this->view->projects = $projectList->data;
        $this->view->companyId = $companyId;
        
        if($mapper == true){
        	$this->_helper->viewRenderer->setNoRender(true);
        	$response = $this->getResponse();
        	$response->clearHeaders()->clearBody();
        	$return = json_encode($projectList->data);

        	$response->setHeader('Content-Length', strlen($return), true);
        	$response->setBody($return);
        }
    }

    public function mytechsAction() {
        require_once 'Core/Api/Class.php';

        $client = new Core_Api_Class();

        $this->view->distances = $client->getDistances();
    }

    public function techInfoAction() {
        require_once 'Core/Api/Class.php';

        $request = $this->getRequest();
        $tech = $request->getParam('tech', NULL);

        $search = new Core_Api_Class;

        $techInfo = $search->techLookup($this->_login, $this->_password, $tech);
        $techInfo->data[0]->PrimaryPhoneStatus = 1;
        $techInfo->data[0]->SecondaryPhoneStatus = 1;
        $api = new Core_Api_TechClass;
        $result = $api->getExts($techInfo->data[0]->TechID);

        if (!empty($result['TechID'])) {
            $techInfo->data[0]->PrimaryPhoneStatus = $result['PrimaryPhoneStatus'];
            $techInfo->data[0]->SecondaryPhoneStatus = $result['SecondaryPhoneStatus'];
        }
        $this->view->tech = $techInfo;
    }

    public function satFormAction() {
        $request = $this->getRequest();
        $win = $request->getParam('id', null);
        $companyId = $request->getParam('companyID', null);
        $this->view->updated = false;

        if (!$win) {
            $this->view->error_message = 'Cannot load WO information. Parameter isn\'t specified.';
            $this->getResponse()->setBody($this->view->render('popup/sat-form-error.phtml'));
            $this->_helper->viewRenderer->setNoRender(true);
            return;
        }
        //735
        require_once 'Core/Api/Class.php';
        $user = new Core_Api_User();
        $search = new Core_Api_Class();
        $clientInfoLogin = $user->loadWithExt(
                array(
                    'login' => $this->_login,
                    'password' => $this->_password
                )
        );

        $isGPM = $search->isGPM($clientInfoLogin->ClientID);
        $this->view->isGPM = $isGPM;
        //end 735
        
        $wo = $search->getWorkOrder($this->_login . '|pmContext=' . $companyId, $this->_password, $win);
        if ($wo->success && !empty($wo->data[0])) {
            $this->view->wo = $wo->data[0];
        } else {
            $this->view->error_message = 'Cannot load WO information.';
            $this->getResponse()->setBody($this->view->render('popup/sat-form-error.phtml'));
            $this->_helper->viewRenderer->setNoRender(true);
            return;
        }

        if (!$this->view->wo->Tech_ID) {
            $this->view->error_message = 'This work order isn\'t assigned to any tech.';
            $this->getResponse()->setBody($this->view->render('popup/sat-form-error.phtml'));
            $this->_helper->viewRenderer->setNoRender(true);
            return;
        }

        $techID = $this->view->wo->Tech_ID;

        /* Handler */
        if ($request->getParam('update', NULL)) {
            $isChanged = false;
            if (null !== $request->getParam('SATRecommended', NULL)) {
                $this->view->wo->SATRecommended = $request->getParam('SATRecommended', NULL);
                $isChanged = true;
            }
            if (null !== $request->getParam('SATPerformance', NULL)) {
                $this->view->wo->SATPerformance = $request->getParam('SATPerformance', NULL);
                $isChanged = true;
            }
            if ($isChanged) {
//                $search->removeTechPrefer($this->_login.'|pmContext='.$companyId, $this->_password, $techID);
//                $search->removeTechDeny($this->_login.'|pmContext='.$companyId, $this->_password, $techID);
                $search->updateWorkOrder($this->_login . '|pmContext=' . $companyId, $this->_password, $win, $this->view->wo, false);
            }

            if (null !== $request->getParam('PreferredStatus', NULL)) {
                $preferredStatus = $request->getParam('PreferredStatus', NULL);
                if ($preferredStatus == 0) {
                    $search->removeTechPrefer($this->_login . '|pmContext=' . $companyId, $this->_password, $techID);
                    $search->removeTechDeny($this->_login . '|pmContext=' . $companyId, $this->_password, $techID);
                } elseif ($preferredStatus == 1) {
                    $search->removeTechDeny($this->_login . '|pmContext=' . $companyId, $this->_password, $techID);
                    $search->techPrefer($this->_login . '|pmContext=' . $companyId, $this->_password, $techID);
                } elseif ($preferredStatus == 2) {
                    $search->removeTechPrefer($this->_login . '|pmContext=' . $companyId, $this->_password, $techID);
                    $search->techDeny($this->_login . '|pmContext=' . $companyId, $this->_password, $techID, '');
                }
            }

            //735
            $apifsexpert = new Core_Api_FSExpertClass();
            if($request->getParam('chkskillcategory', NULL)!="null")
            {
                $apifsexpert->saveTechFSExpert($techID,$request->getParam('FSExpertId'),1);
            }
            else
            {
                $apifsexpert->saveTechFSExpert($techID,$request->getParam('FSExpertId'),0);
            }
            //end 735
            $this->view->updated = true;
        }
        /* View */ 
        else {
            $this->view->techStatus = null;
            $status = $search->techGetPreferStatus($this->_login . '|pmContext=' . $companyId, $this->_password, $techID);
            if ($status->success && !empty($status->data)) {
                $this->view->techStatus = $status->data;
            }

            $tech = $search->techLookup($this->_login . '|pmContext=' . $companyId, $this->_password, $techID);
            if ($tech->success && !empty($tech->data[0])) {
                $this->view->company  = $companyId;
                $user = new Core_Api_User();
                $authData = array('login'=>$this->_login . '|pmContext=' . $companyId, 'password'=>$this->_password);
                $user->checkAuthentication($authData);
                $this->view->ClientId = $user->getClientId();
                $this->view->tech = $tech->data[0];
            } else {
                $this->view->error_message = 'Cannot load Tech information.';
                $this->getResponse()->setBody($this->view->render('popup/sat-form-error.phtml'));
                $this->_helper->viewRenderer->setNoRender(true);
                return;
            }
        }
    }

    public function getTechInfoAction() {
        require_once 'Core/Api/Class.php';

        $this->_helper->viewRenderer->setNoRender();
        $response = $this->getResponse();
        $response->setHeader('Content-Type', 'text/json', true);

        $request = $this->getRequest();
        $tech = $request->getParam('tech', NULL);
        $wo = $request->getParam('wo', NULL);

        $company = $request->getParam('company', NULL);

        $context = !empty($company) ? '|pmContext=' . $company : '';

        if (empty($tech)) {
            $jsonContext = json_encode(array('error' => 1, 'msg' => 'Tech Id is empty or not numeric'));
        } else if (empty($wo) && $request->getParam('needBid', false)) {
            $jsonContext = json_encode(array('error' => 1, 'msg' => 'WO Id is empty or not numeric'));
        } else {
            $search = new Core_Api_Class;

            $bidInfo = NULL;

            if ($request->getParam('needBid', false)) {
                $bidInfo = $search->getLastWOBidComment($this->_login, $this->_password, $tech, $wo);
            }

            $techInfo = $search->techLookup($this->_login . $context, $this->_password, $tech);

            if ($techInfo->success && !empty($techInfo->data[0]->TechID)) {
                $Core_Api_TechClass = new Core_Api_TechClass;
                $result = $Core_Api_TechClass->getExts($techInfo->data[0]->TechID);
                $PrimaryPhoneStatus = "1";
                $SecondaryPhoneStatus = "1";
                if (!empty($result['TechID'])) {
                    $PrimaryPhoneStatus = $result['PrimaryPhoneStatus'];
                    $SecondaryPhoneStatus = $result['SecondaryPhoneStatus'];
                }
                // 14112
                $resultISO = $Core_Api_TechClass->getTechInfo_ForAPIUsing($techInfo->data[0]->TechID);
                if (!empty($resultISO)) {
                    $ISO_ID = !empty($resultISO['ISO_Affiliation_ID'])?$resultISO['ISO_Affiliation_ID']:0;
                    $ISO_Name = $resultISO['ISO_Affiliation'];
                }
                $info = array(
                    'TechID' => $techInfo->data[0]->TechID,
                    'FLSID' => $techInfo->data[0]->FLSID,
                    'fName' => $techInfo->data[0]->Firstname,
                    'lName' => $techInfo->data[0]->Lastname,
                    'email' => $techInfo->data[0]->PrimaryEmail,
                    'email2' => $techInfo->data[0]->SecondaryEmail,
                    'phone' => $techInfo->data[0]->PrimaryPhone,
                    'phone2' => $techInfo->data[0]->SecondaryPhone,
                    'isDenied' => $techInfo->data[0]->isDenied,
                	'isPreferred' => $techInfo->data[0]->isPreferred,
                    'ISO_ID' => $ISO_ID,
                    'ISO_Name' => $ISO_Name,
                    'PrimaryPhoneStatus' => $PrimaryPhoneStatus,
                    'SecondaryPhoneStatus' => $SecondaryPhoneStatus);

                $result = array('error' => 0, 'techInfo' => $info);
                if (!empty($bidInfo)) {
                    $result += array('bidInfo' => array('comments' => $bidInfo['Comments']));
                }
                $jsonContext = json_encode($result);
            } else {
                $jsonContext = json_encode(array('error' => 1, 'msg' => 'There is no such tech'));
            }
        }
        $response->setHeader('Content-Length', strlen($jsonContext), true)
                ->setBody($jsonContext);
    }
    public function getTechFlsInfoAction() {
        $this->_helper->viewRenderer->setNoRender();
        $response = $this->getResponse();
        $response->setHeader('Content-Type', 'text/json', true);

        $request = $this->getRequest();
        $tech = $request->getParam('tech', NULL);

        $company = $request->getParam('company', NULL);

        $context = !empty($company) ? '|pmContext=' . $company : '';

        if (empty($tech)) {
            $jsonContext = json_encode(array('error' => 1, 'msg' => 'Tech Id is empty or not numeric'));
        } else {
            $search = new Core_Api_Class;

            $techInfo = $search->techLookupFLSID($this->_login . $context, $this->_password, $tech);

            if ($techInfo->success && !empty($techInfo->data[0]->TechID)) {
                $Core_Api_TechClass = new Core_Api_TechClass;
                $result = $Core_Api_TechClass->getExts($techInfo->data[0]->TechID);
                $PrimaryPhoneStatus = "1";
                $SecondaryPhoneStatus = "1";
                if (!empty($result['TechID'])) {
                    $PrimaryPhoneStatus = $result['PrimaryPhoneStatus'];
                    $SecondaryPhoneStatus = $result['SecondaryPhoneStatus'];
                }
                $info = array(
                    'TechID' => $techInfo->data[0]->TechID,
                    'FLSID' => $techInfo->data[0]->FLSID,
                    'fName' => $techInfo->data[0]->Firstname,
                    'lName' => $techInfo->data[0]->Lastname,
                    'email' => $techInfo->data[0]->PrimaryEmail,
                    'email2' => $techInfo->data[0]->SecondaryEmail,
                    'phone' => $techInfo->data[0]->PrimaryPhone,
                    'phone2' => $techInfo->data[0]->SecondaryPhone,
                    'isDenied' => $techInfo->data[0]->isDenied,
                    'PrimaryPhoneStatus' => $PrimaryPhoneStatus,
                    'SecondaryPhoneStatus' => $SecondaryPhoneStatus);
                $result = array('error' => 0, 'techInfo' => $info);
                $jsonContext = json_encode($result);
            } else {
                $jsonContext = json_encode(array('error' => 1, 'msg' => 'There is no such tech'));
            }
        }
        $response->setHeader('Content-Length', strlen($jsonContext), true)
                ->setBody($jsonContext);
    }
    public function techScheduleAction() {

        /* START: load params */

        /* tech to view */
        $tech_ID = $this->getRequest()->getParam('tech_ID', null);

        /* Start Date */
        $date = strtolower($this->getRequest()->getParam('date', null));
        if (null === $date || trim($date) == '') {
            $start_date = new Zend_Date(); //'2010-10-11'
        } elseif ($date == 'now')
            $start_date = new Zend_Date();
        else
            $start_date = new Zend_Date($date, Zend_Date::ISO_8601);
        $nowDate = new Zend_Date();

        /* Date Interval */
        $date_interval_set = array('all' => null, 'day' => Zend_Date::DAY, 'week' => Zend_Date::WEEK, 'month' => Zend_Date::MONTH, 'year' => Zend_Date::YEAR);
        $date_interval = strtolower($this->getRequest()->getParam('date_interval', 'all'));
        $date_interval = array_key_exists($date_interval, $date_interval_set) ? $date_interval : 'all';
        $date_interval_query = $date_interval_set[$date_interval];

        /* Date Interval Count */
        $date_interval_count = (int) $this->getRequest()->getParam('date_interval_count', 1);
        $date_interval_count = $date_interval_count ? $date_interval_count : 1;

        /* Sorting */
        $sort = strtolower($this->getRequest()->getParam('sort', 'starttime'));
        $sort_dir = strtolower($this->getRequest()->getParam('sort_dir', 'asc'));
        $sort_dir = in_array($sort_dir, array('asc', 'desc')) ? strtolower($sort_dir) : 'asc';
        $sort_set = ($sort_dir == 'asc') ? array(
            'starttime' => 'StartDate ASC, StartTime ASC',
            'endtime' => 'EndDate ASC, EndTime ASC',
            'wo' => 'TB_UNID ASC',
            'location' => 'City ASC, State ASC',
            'zip' => 'Zipcode ASC',
            'status' => 'Status ASC') : array(
            'starttime' => 'StartDate DESC, StartTime DESC',
            'endtime' => 'EndDate DESC, EndTime DESC',
            'wo' => 'TB_UNID DESC',
            'location' => 'City DESC, State DESC',
            'zip' => 'Zipcode DESC',
            'status' => 'Status DESC');
        $sort_query = array_key_exists($sort, $sort_set) ? $sort_set[$sort] : $sort_set['starttime'];

        //echo("<pre>");print_r($sort_query);//test

        /* Display mode */
        $display_date = (int) $this->getRequest()->getParam('display_date', 1);
        $display_time = (int) $this->getRequest()->getParam('display_time', 1);
        $display_calendar = (int) $this->getRequest()->getParam('display_calendar', 0);


        /* END: load params */

        $period_interval = 1;
        $period = Zend_Date::MONTH;

        /* from date */
        $dateFrom = clone $start_date;
        $dateFrom->set('00:00:00', Zend_Date::TIMES);
        if (null === $date || trim($date) == '')
            $dateFrom->sub(200, Zend_Date::YEAR);

        /* to date */
        $dateTo = clone $start_date;
        $dateTo->set('00:00:00', Zend_Date::TIMES);
        if ($date_interval_query === null)
            $dateTo->add(200, Zend_Date::YEAR);
        else
            $dateTo->add($date_interval_count, $date_interval_query)->sub(1, Zend_Date::SECOND);

        $countRows = -1;

        $filters = new API_WorkOrderFilter();
        $filters->Deactivated = false; //2012-03-05 (Re-instated by GB on 2012-08-17)
        $filters->withOutCompanyID = true;
        $filters->Tech_ID = $tech_ID;
        $filters->StartDateFrom = $dateFrom->toString('yyyy-MM-dd');
        $filters->StartDateTo = $dateTo->toString('yyyy-MM-dd');

        require_once 'Core/Api/Class.php';
        $search = new Core_Api_Class();
        $totalCount = $search->getTotalWorkOrders($this->_login, $this->_password, NULL,
                NULL, NULL, NULL, $filters, 0, 0, $countRows, $sort_query);
        $request = $this->getRequest();
        $size = (int) ($request->getParam('size', 200));
        $page = ($request->getParam('page', 1)) + 0;
        $page = ( $page < 1 ) ? 1 : $page;
        $offset = ($page * $size) - $size;

        $wos = $search->getWorkOrders($this->_login, $this->_password, NULL,
                NULL, NULL, NULL, $filters, $offset, $size, $countRows, $sort_query);

        if ($totalCount > 0 && !empty($wos->data)) {
            $paginator = new Zend_Paginator(new Core_Paginator_Sliced_Array($wos->data, $totalCount));
            $paginator->setCurrentPageNumber($page);
            $paginator->setItemCountPerPage($size);
            $this->view->paginator = $paginator;
        }

        if ($wos->success && $countRows && !empty($wos->data)) {
            $dateTemp = array();
            if ($sort == 'starttime' && $sort_dir == 'desc') {
                $j = 0;
                for ($i = 0; $i < count($wos->data); $i++) {
                    if (trim($wos->data[$i]->StartDate) != "") {
                        $dateTemp[$j] = $wos->data[$i];
                        $j++;
                    }
                }
                for ($i = 0; $i < count($wos->data); $i++) {
                    if (trim($wos->data[$i]->StartDate) == "") {
                        $dateTemp[$j] = $wos->data[$i];
                        $j++;
                    }
                }
                $wos->data = $dateTemp;
            }
        }
        $this->view->wos = $wos->data;
        $this->view->success = $wo->success;
        $this->view->errors = $wo->errors;
        $this->view->dateFrom = $dateFrom;
        $this->view->dateTo = $dateTo;
        $this->view->dateNow = $nowDate;

        $this->view->Tech_ID = $tech_ID;
        $this->view->date = $date;
        $this->view->date_interval = $date_interval;
        $this->view->date_interval_count = $date_interval_count;
        $this->view->sort = $sort;
        $this->view->sort_dir = $sort_dir;
        $this->view->display_date = $display_date;
        $this->view->display_time = $display_time;
        $this->view->display_calendar = $display_calendar;


//        print "Results:".$countRows;
//        print_r($wos);
//        exit;
    }

    public function techAppliedWorkAction() {
        
    }

    public function getWoInfoAction() {
        require_once 'Core/Api/Class.php';

        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();

        $woId = $request->getParam('WO_ID', NULL);
        $winNum = intval($request->getParam('WIN_NUM', NULL));
        $company = trim($request->getParam('company', NULL));
        $woIdMode = trim($request->getParam('WO_ID_Mode', 0));
        $search = new Core_Api_Class;

        if (!empty($winNum) && is_numeric($winNum)) {
          
            $woInfo = $search->getWorkOrder($this->_login . '|pmContext=' . $company, $this->_password, (int) $winNum);
        } elseif (!empty($woId)) {
            $filters = new API_WorkOrderFilter;
            $filters->WO_ID = trim($woId);
            $filters->WO_ID_Mode = $woIdMode;
             //print_r("<pre>");
             //print_r($filters);
             //print_r("</pre>");
            $woInfo = $search->getWorkOrders(
                    $this->_login . '|pmContext=' . $company, $this->_password, 'WIN_NUM,WO_ID,Tech_Bid_Amount,Amount_Per,Deactivated,DeactivationCode,Deactivated_Reason,PayMax', '', '', '', $filters, 0, 0, $countRows, ''
            );
        }
        //echo("woInfo: <pre>");print_r($woInfo);echo("</pre>");die();

        $response = $this->getResponse();
        $response->setHeader('Content-Type', 'text/json', true);

        if ($woInfo->success) {
            if (!empty($woInfo->data[0]->WIN_NUM)) {
                $redirect = 'wodetails';
                $iMatch = 0;
                if (!empty($winNum) && is_numeric($winNum)) {    
                    $redirect = 'wodetails'; 
                    $wins = $woInfo->data[0]->WIN_NUM;  
                } else if (!empty($woId)) { 
                    $winArray = array();
                    //print_r($woInfo->data);die();
                    for($i=0;$i<count($woInfo->data);$i++){
                        $winArray[]=$woInfo->data[$i]->WIN_NUM; 
                    } 
                                        
                    $wins='';
                    //echo("<br/>woId: $woId");
                    //echo("<br/>count: ".count($woInfo->data));
                    //print_r($woInfo->data[0]);die();
                    if($woInfo->data[0]->WO_ID == $woId && count($woInfo->data)==1){
                        $redirect = 'wodetails';
                        $wins = $woInfo->data[0]->WIN_NUM;
                    } else {
                        $redirect = 'all';  
                        $wins = implode(',',$winArray);                
                    }                   
                }     
                //echo("<br/>count: ".count($woInfo->data));         
                //echo("<br/>redirect: $redirect");
                //echo("<br/>redirect: $wins");
                //die();
                $jsonContext = json_encode(array('error' => 0,
                            'redirect'=>$redirect,
                            'wins'=>$wins,
                            'woInfo' => $woInfo->data)
                    );
 
            } else {
                $jsonContext = json_encode(array('error' => 1, 'msg' => 'There is no such Work Order'));              
            }
        } else  {
            $jsonContext = json_encode(array('error' => 1, 'msg' => 'There is no such Work Order'));          
        }

        $response->setHeader('Content-Length', strlen($jsonContext), true)
                ->setBody($jsonContext);
    }

    public function getFullWoInfoAction() {
        require_once 'Core/Api/Class.php';

        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();

        $woId = $request->getParam('WO_ID', NULL);
        $winNum = intval($request->getParam('WIN_NUM', NULL));
        $company = trim($request->getParam('company', NULL));

        $response = $this->getResponse();
        $response->setHeader('Content-Type', 'text/json', true);

        $search = new Core_Api_Class;
        if (!empty($winNum) && is_numeric($winNum)) {
            $woInfo = $search->getWorkOrder($this->_login . '|pmContext=' . $company, $this->_password, (int) $winNum,true);
        } elseif (!empty($woId)) {
            $filters = new API_WorkOrderFilter;
            $filters->WO_ID = trim($woId);

            $woInfo = $search->getWorkOrders(
                    $this->_login . '|pmContext=' . $company, $this->_password, 'WIN_NUM', '', '', '', $filters, 0, 0, $countRows, '',true);//360
            if ($woInfo->success && !empty($woInfo->data[0]->WIN_NUM)) {
                $woInfo = $search->getWorkOrder($this->_login . '|pmContext=' . $company, $this->_password, (int) $woInfo->data[0]->WIN_NUM,true);//360
            } else {
                $jsonContext = json_encode(array('error' => 1, 'msg' => 'There is no such Work Order'));
                $response->setHeader('Content-Length', strlen($jsonContext), true)
                        ->setBody($jsonContext);
                return;
            }
        }

        if ($woInfo->success) {
            if (!empty($woInfo->data[0]->WIN_NUM)) {
                $Wosapi = new  Core_Api_WosClass();
                $this->view->EnableColspanTechPay = $Wosapi->enableExpenseReporting($winNum);
                if($this->view->EnableColspanTechPay)
                {
                $this->view->WosTurnedOnBCatsinfo = $Wosapi->getWorkOrderTurnedOnBCats_insertFromProjectIfNotExists($winNum);
                    //863
                    $sections = $this->view->render('details/expense-reporting-total-approved.phtml');
                    //end 863
                }
                else
                {
                    $woInfo->data[0]->ExpenseReporting = '';
                }
                
                if(empty($woInfo->data[0]->Qty_Visits)){
                    $woInfo->data[0]->Qty_Visits = $Wosapi->getNumOfVisits($woInfo->data[0]->WIN_NUM);
                }
                //863
                $jsonContext = json_encode(array('error' => 0, 'woInfo' => $woInfo->data[0], 'ExpenseReporting'=>$sections));
                //end 863
            } else
                $jsonContext = json_encode(array('error' => 1, 'msg' => 'There is no such Work Order'));
        } else
            $jsonContext = json_encode(array('error' => 1, 'msg' => 'There is no such Work Order'));
        
        $response->setHeader('Content-Length', strlen($jsonContext), true)
                ->setBody($jsonContext);
    }

    public function activityLogAction() {
        require_once 'API/Timestamp.php';
        require_once 'Core/Api/Class.php';
        require_once 'API/WorkOrderFilter.php';

        $this->_helper->viewRenderer->setNoRender();

        $request = $this->getRequest();
        $response = $this->getResponse();

        $win = intval($request->getParam('win', ''));
        $woid = $request->getParam('woid', '');
        $company = $request->getParam('company', NULL);

        if (!empty($win)) {
            $filter = new API_WorkOrderFilter();
            $filter->TB_UNID = $win;

            $result = API_WorkOrderFilter::filter($company, 'WO_ID', '', '', '', $filter, '')->toArray();
            if (!empty($result) && is_array($result)) {
                $woid = $result[0]['WO_ID'];
            }

            $log = API_Timestamp::getTimestamps($win, $company, 0, 0, 'DateTime_Stamp DESC');
        } else if (!empty($woid)) {

            $filter = new API_WorkOrderFilter();
            $filter->WO_ID = $woid;

            $result = API_WorkOrderFilter::filter($company, 'TB_UNID', '', '', '', $filter, '')->toArray();
            if (!empty($result) && is_array($result)) {
                $win = $result[0]['TB_UNID'];
                $log = API_Timestamp::getTimestamps($win, $company, 0, 0, 'DateTime_Stamp DESC');
            } else {
                $log = null;
            }
        } else {
            $log = null;
        }

        if (!empty($log) && is_array($log))
            foreach ($log as &$ts)
                $ts->description = str_replace(array(',', ';', ':', '.', '-'), array(', ', '; ', ': ', '- '), $ts->description);

        $this->view->log = $log;

        $data = array('win' => $win, 'woid' => $woid, 'html' => $this->view->render('popup/activity-log.phtml'));

        $response->clearBody();
        $response
                ->clearAllHeaders()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(json_encode($data));
    }

    public function approveAction() {
        
    }

    public function contactLogDetailsAction() {
        $id = $this->getRequest()->getParam('id', null);
        if (!empty($id)) {
            $log = new Core_ContactLog();
            $s = $log->select()->where('id=?', $id);
            $res = $log->fetchRow($s);
            $this->view->success = !empty($res);
            if (empty($res))
                $this->view->errors = array('No results.');
            $this->view->data = $res;
        } else {
            $this->view->success = false;
            $this->view->errors = array('No results.');
        }
    }

    private function getCountries() {
        $common = new Core_Api_CommonClass;
        // country list
        $countries = $common->getCountries();
        if ($countries->success)
            return $countries->data;
        return array();
    }

    function copierSkillsAssessmentViewAction() {
        $params = $this->getRequest()->getParams();
        $Question1 = array();
        $Question2 = array();
        $Question3 = array();
        $Question4 = array();
        $techid = 0;
        if (isset($params["techid"])) {
            $techid = $params["techid"];
            $Core_Api_TechClass = new Core_Api_TechClass();
            $Question1 = $Core_Api_TechClass->getAnsweredItems($techid, 1);
            $Question2 = $Core_Api_TechClass->getAnsweredItems($techid, 2);
            $Question3 = $Core_Api_TechClass->getAnsweredItems($techid, 3);
            $Question4 = $Core_Api_TechClass->getAnsweredItems($techid, 4);

            $search = new Core_Api_Class;
            $company = $this->getRequest()->getParam('company', NULL);
            $context = !empty($company) ? '|pmContext=' . $company : '';
            $techInfo = $search->techLookup($this->_login . $context, $this->_password, $techid);
            $this->view->techInfo = $techInfo->data[0];
        }
        $this->view->Question1 = $Question1;
        $this->view->Question2 = $Question2;
        $this->view->Question3 = $Question3;
        $this->view->Question4 = $Question4;
        $this->view->techid = $techid;
    }

    function emcCertAction() {
        $request = $this->getRequest();
        $params = $request->getParams();
        $Core_Api_TechClass = new Core_Api_TechClass();
        $techid = 0;
        
        if (isset($params['techid'])) {
            // for admin
            $techid = (int) $params['techid'];
        } else {
            // for tech
            $techid = $_SESSION['TechID'];
        }
        if (!isset($params['json'])) {
            // for view

            $datacbx = $Core_Api_TechClass->getEMCList();
            $EMCListForTechnician = $Core_Api_TechClass->getEMCListForTechnician($techid);
            $this->view->data = $EMCListForTechnician;
            $EMCListForTechnician_name = array(); 
            if (!empty($EMCListForTechnician)) {
                foreach ($EMCListForTechnician as $i => $value) {
                    $EMCListForTechnician_name[$i] = $value['name'];
                }
            }
            $this->view->EMCListForTechnician_name = $EMCListForTechnician_name;
            $this->view->datacbx = $datacbx;
        } else {
            // for save
            $this->_helper->viewRenderer->setNoRender();
            $json = $params['json'];
            if (trim($json) != "") {
                $data = json_decode("[" . $json . "]");

                if (!empty($data)) {
                    $selectedEMCNames="";
                    foreach ($data as $item) {
                        $EMCName = base64_decode($item->value);
                        $selectedEMCNames .= empty($selectedEMCNames)?"'".$EMCName."'":  ",'".$EMCName."'";
                        $date = base64_decode($item->date);
                        $Core_Api_TechClass->saveTechCertification(
                                $techid, base64_decode($item->value), (trim($date) != "") ? date_format(new DateTime($date), 'Y-m-d') : ''
                        );
                    }
                    //remove spam
                    $Core_Api_TechClass->selectOnlyEMCNames($techid,$selectedEMCNames);
                }
            }

            $this->_helper->json(array('success' => true));
        }
    }

    public function bidWithdrawAction() {
        require_once 'Core/Api/TechClass.php';
        require_once 'API/WorkOrderFilter.php';

        $request = $this->getRequest();
        $win = $request->getParam('win', NULL);
        $techId = $request->getParam('tech_id', NULL);

        if (empty($win)) {

            $this->_helper->viewRenderer->setNoRender(true);

            $response = $this->getResponse();
            $response->clearAllHeaders();
            $response->clearBody();

            $response
                    ->setHttpResponseCode(406)
                    ->setBody('WIN# not found or is no longer available');
            return;
        }

        $paths = $this->getFrontController()->getControllerDirectory();
        if (!empty($paths['dashboard']) && file_exists($paths['dashboard'] . '/IndexController.php')) {
            require_once $paths['dashboard'] . '/IndexController.php';
            $fields = Dashboard_IndexController::FIELDS;
        } else {
            $fields = NULL;
        }

        $filters = new API_WorkOrderFilter;
        $filters->Deactivated = FALSE;
        $filters->ShowTechs = TRUE;
        //$filters->Sourced       = FALSE;
        $filters->Tech = NULL;
        $filters->TB_UNID = $win;

        $search = new Core_Api_TechClass;

        $wo = $search->getWorkOrders($this->_login, $this->_password, $fields, $filters);
        if (!$wo->success) {

            $this->_helper->viewRenderer->setNoRender(true);

            $response = $this->getResponse();
            $response->clearAllHeaders();
            $response->clearBody();

            $response
                    ->setHttpResponseCode(406)
                    ->setBody($wo->errors[0]->message);
            return;
        } else if (!$wo->errors && empty($wo->data)) {

            $this->_helper->viewRenderer->setNoRender(true);

            $response = $this->getResponse();
            $response->clearAllHeaders();
            $response->clearBody();

            $response
                    //->setHttpResponseCode(406)
                    ->setBody('WIN# not found or is no longer available'/* Core_Api_TechClass::ERROR_WO_INVALID_WIN */);
            return;
        }

        $prj_id = $wo->success ? $wo->data[0]->Project_ID : '';
        $project = $search->getProjects($this->_login, $this->_password, array($prj_id), '');
        $lastwobid = $search->getLastBid($this->_login, $this->_password,$win);
        
        $this->view->wo = $wo->data[0];
        $this->view->prj = $project->success ? $project->data[0] : new stdClass();
        $this->view->lastwobid = $lastwobid;
    }
    //end 415
    
    //13433
    function techsinsiteAction()
    {
          $this->_helper->viewRenderer->setNoRender();
          //--- get request
          $request = $this->getRequest();
          $siteNumber = $request->getParam('SiteNumber', NULL);
          $projectID = $request->getParam('ProjectID', NULL);
          $techIDs = $request->getParam('TechIDs', NULL);
          $companyID = $request->getParam('CompanyID', NULL);
          if(!empty($projectID))
          {
          //--- get list
          $api = new Core_Api_WosClass();
          $techList = $api->getTechsInSite($siteNumber,$projectID);
          //--- prject data 
          $apiPrj = new  Core_Api_ProjectClass();
          $project = $apiPrj->getProject($projectID);
          $companyID = $project['Project_Company_ID'];
          }
          else if(!empty($techIDs))
          {
              //--- get list
              $api = new Core_Api_WosClass();
              $techList = $api->getTechsInSite('','',$techIDs);              
          }    
          //--- view data 
          $this->view->TechList = $techList;
          $this->view->CompanyID = $companyID;
          $this->view->SiteNumber = $siteNumber;
          $this->view->ProjectID = $projectID;
          
          //--- data 
          $data = array('html' => $this->view->render('popup/techsinsite.phtml'));                   
          //--- response
          $response = $this->getResponse();
          $response->clearBody();
          $response
                ->clearAllHeaders()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(json_encode($data));
    }
    //13433    
    public function sorttechinfoAction()
    {
          $this->_helper->viewRenderer->setNoRender();
          //--- get request
          $request = $this->getRequest();
          $techID = $request->getParam('TechID', NULL);
          $FLSID = $request->getParam('FLSID', NULL);
          //--- techInfo
          if (empty($techID) && empty($FLSID) ) {
              $result = array('error' => 1, 
                            'msg' => 'Tech Id AND FLSID is empty or not numeric'
              );
          }
          else
          {
              $api = new Core_Api_WosClass();
              $tech = $api->getTechInfo($techID,$FLSID);              
              $result = array('error' => 0, 'techInfo' => $tech);
          }
          //--- response
          $response = $this->getResponse();
          $response->clearBody();
          $response
                ->clearAllHeaders()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(json_encode($result));
    }
    //13433    
    public function savetechsiteAction()
    {
         $this->_helper->viewRenderer->setNoRender();
         //--- get request
         $request = $this->getRequest();
         $params = $request->getParams();
         $siteNumber =  $params['SiteNumber'];
         $projectID =  $params['ProjectID'];
         $techIDArray = isset($params['techid'])? $params['techid']:null;
         //--- save techs to site         
         $api = new Core_Api_WosClass();
         if(empty($techIDArray)) $api->RemoveAllTechsFromSite($siteNumber,$projectID);
         else $api->saveTechsToSite($techIDArray,$siteNumber,$projectID);         
         $result = array('error' => 0);         
         //--- response
          $response = $this->getResponse();
          $response->clearBody();
          $response
                ->clearAllHeaders()
                ->setHeader('Content-Type', 'application/json')
                ->setBody(json_encode($result));
        
    }
    function filterQuickticketAction(){
        $search = new Core_Api_Class;
        $result = $search->getStates();
        if ($result->success) {
            $states = $result->data;
        }
        $Core_Api_CustomerClass = new Core_Api_CustomerClass;
        $projectList  = $Core_Api_CustomerClass->getAllProjectsForCustomer(
                    $this->_login, $this->_password
            );
        $authData = array('login'=>$this->_login, 'password'=>$this->_password);

        $user = new Core_Api_CustomerUser();
        $user->checkAuthentication($authData);

        if (!$user->isAuthenticate())
            return API_Response::fail();

        $cusId  = $user->getCustomerId();
        $this->view->IsShownClientPO = $Core_Api_CustomerClass->IsShownClientPO($cusId);
        
        if (!empty($projectList->data)) {
            usort($projectList->data, 'Core_Controller_Action::_cmpProjects');

		}

        $this->view->states = $states;
        $this->view->projects = $projectList->data;
        $this->view->callTypes = $search->getCallTypes()->data;
        $this->view->companyId = $user->getCompanyId();

        $this->view->countries = $this->getCountries();
    }
    function clientCheckinAction() {
        $request = $this->getRequest();
        $this->view->win = $request->getParam('win', NULL);
        $this->view->clientid = $request->getParam('clientid', NULL);
        $this->view->techid = $request->getParam('techid', NULL);
        $this->view->fname = $request->getParam('fname', NULL);
        $this->view->lname = $request->getParam('lname', NULL);
        $this->view->reviewed = $request->getParam('reviewed', NULL);
        $this->view->techcheckedin24hrs = $request->getParam('techcheckedin24hrs', NULL);
        $this->view->checkedin = $request->getParam('checkedin', NULL);
        $apiWos = new Core_Api_WosClass();
        $WOVisitInfo = $apiWos->getBaseWOVisitInfo_ByWinnum($request->getParam('win', NULL));
        
        $this->view->CORCheckInDate ='';
        $this->view->CORCheckInTime = '';
        $this->view->CORCheckOutDate = '';
        $this->view->CORCheckOutTime = '';
        
        //13881
        if(!empty($WOVisitInfo))
        {
            if($WOVisitInfo['OptionCheckInOut']==2 && empty($WOVisitInfo['CORCheckInDate']) && empty($WOVisitInfo['CORCheckInTime']) && empty($WOVisitInfo['CORCheckOutDate']) && empty($WOVisitInfo['CORCheckOutTime']) ){
                $this->view->CORCheckInDate ='';
                $this->view->CORCheckInTime = '';
                $this->view->CORCheckOutDate = '';
                $this->view->CORCheckOutTime = '';
                $this->view->checkedin = 0;
            } else if (!empty($WOVisitInfo['CORCheckInDate']) || !empty($WOVisitInfo['CORCheckInTime']) || !empty($WOVisitInfo['CORCheckOutDate']) || !empty($WOVisitInfo['CORCheckOutTime'])) {
                $this->view->CORCheckInDate = $WOVisitInfo['CORCheckInDate'];
                $this->view->CORCheckInTime = $WOVisitInfo['CORCheckInTime'];
                $this->view->CORCheckOutDate = $WOVisitInfo['CORCheckOutDate'];
                $this->view->CORCheckOutTime = $WOVisitInfo['CORCheckOutTime'];
            }  else if (!empty($WOVisitInfo['TechCheckInDate']) || !empty($WOVisitInfo['TechCheckInTime']) || !empty($WOVisitInfo['TechCheckOutDate']) || !empty($WOVisitInfo['TechCheckOutTime'])) {
                $this->view->CORCheckInDate = $WOVisitInfo['TechCheckInDate'];
                $this->view->CORCheckInTime = $WOVisitInfo['TechCheckInTime'];
                $this->view->CORCheckOutDate = $WOVisitInfo['TechCheckOutDate'];
                $this->view->CORCheckOutTime = $WOVisitInfo['TechCheckOutTime'];
            }  else  {
                $this->view->CORCheckInDate ='';
                $this->view->CORCheckInTime = '';
                $this->view->CORCheckOutDate = '';
                $this->view->CORCheckOutTime = '';
            }
        }
        //end 13881
    }
    
    public function saveClientCheckinAction()
    {
        $this->_helper->viewRenderer->setNoRender();
        $request = $this->getRequest();
        $params = $request->getParams();
        $win = $params['win'];
        $reviewed = $params['reviewed'];
        $techcheckedin24hrs = $params['techcheckedin24hrs'];
        $checkedin = $params['checkedin'];
        $checkindate = !empty($params['checkindate']) ? date_format(new DateTime($params['checkindate']), "Y-m-d") : "";
        $checkintime = $params['checkintime'];
        $checkoutdate = !empty($params['checkoutdate']) ? date_format(new DateTime($params['checkoutdate']), "Y-m-d") : "";
        $checkouttime = $params['checkouttime'];
        $apiWos = new Core_Api_WosClass();
        $WOVisitInfo = $apiWos->getBaseWOVisitInfo_ByWinnum($win);
        $return["success"] = 1;

        if ((!empty($checkindate) && !empty($checkintime)) || (!empty($checkoutdate) && !empty($checkouttime) ))
        {
            if (!empty($WOVisitInfo))
            {
                $fieldvals = array();
                if(!empty($checkindate)) $fieldvals['CORCheckInDate']=$checkindate;
                if(!empty($checkintime)) $fieldvals['CORCheckInTime']=$checkintime;
                if(!empty($checkoutdate)) $fieldvals['CORCheckOutDate']=$checkoutdate;
                if(!empty($checkouttime)) $fieldvals['CORCheckOutTime']=$checkouttime;
                $fieldvals['OptionCheckInOut']=2;
                $errors = $apiWos->saveWOVisit_arrayPara($WOVisitInfo['WoVisitID'],$fieldvals);
                //$errors = $apiWos->saveWOVisitCheckInOutForClient(, $win, $checkindate, $checkintime, $checkoutdate, $checkouttime, 2);
                if (!empty($errors))
                {
                    $err = true;
                    $return['errors'][] = $errors->errors[0]->message;
                    $return["success"] = 0;
                }
            }
        }
        
        if (!$err)
        {
            $wofieldvals = array('WorkOrderReviewed' => $reviewed,
                'TechCheckedIn_24hrs' => $techcheckedin24hrs,
                'CheckedIn' => $checkedin);
            $errors = $apiWos->updateWoInfo($win, $wofieldvals,1,0,0);
           
        }
        $this->_helper->json($return);
    }
    public function questionandanswerAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();
        $win = $params['WIN_NUM'];
        $search = new Core_Api_TechClass();
        
        $Class = new Core_Api_Class;
        $wo = $Class->getWorkOrdersWithWinNum($win,"","");
        $wo = (object)$wo[0];

        $WosClass = new Core_Api_WosClass();

        $authData = array('login' => $this->_login, 'password' => $this->_password);
        $tech = new Core_Api_TechUser;
        $tech->checkAuthentication($authData);

        $WosClass->saveTech_LastViewQAWinDatetime($tech->getTechId(),$win);
//
//        $QAlist = $WosClass->getQuestionContactLog($win, $tech->getTechId());
        $QAlist = $WosClass->getQuestionContactLog_forTech($win,$tech->getTechId());
        $this->view->wo = $wo;
        $this->view->QAlist = $QAlist;
        $this->view->techid = $tech->getTechId();
        $this->view->techname = $tech->getFirstName()." ".$tech->getLastName();
}

    public function questionandanswerlistAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();
        $win = $params['WIN_NUM'];
        
        $search = new Core_Api_Class;
        $companyId = $_SESSION['PM_Company_ID'];
        $wo = $search->getWorkOrdersWithWinNum($win,"","");
        $wo = (object)$wo[0];

        $WosClass = new Core_Api_WosClass();
        $Core_Api_User = new Core_Api_User;
    
        $clientInfo = $Core_Api_User->loadWithExt(
            array(
                'login' => $this->_login . '|pmContext=' . $companyId,
                'password' => $this->_password
            )
        );
        
        if(isset($params['forTechID']) && $params['forTechID'] !=="")
        {
            $forTechID = $params['forTechID'];
        }
        else 
        {
            if($wo->Tech_ID !="")
            {
                $forTechID = $wo->Tech_ID ;
            }
            else
            {
                $forTechID = 0;
            }
        }
        
        $clientID = $clientInfo->getClientId();
        
        $WosClass->saveClient_LastViewQAWinDatetime($clientID,$win);
        
        $QAlist = $WosClass->getQuestionContactLog($win,1,$clientID,0,$forTechID);
        $this->view->WOVisitList = $WosClass->GetWOVisitList($win);
        $this->view->wo = $wo;
        $this->view->QAlist = $QAlist;
    }
    public function questionandanswercontactloglistAction()
    {
         $request = $this->getRequest();
        $params = $request->getParams();
        
        $win = $request->getParam('WIN_NUM', null);

        $search = new Core_Api_Class;
        $companyId = $_SESSION['PM_Company_ID'];
        $wo = $search->getWorkOrdersWithWinNum($win,"","");
        $wo = (object)$wo[0];

        $WosClass = new Core_Api_WosClass();
        $Core_Api_User = new Core_Api_User;

        $clientInfo = $Core_Api_User->loadWithExt(
            array(
                'login' => $this->_login . '|pmContext=' . $companyId,
                'password' => $this->_password
            )
        );
        
        if(isset($params['forTechID']) && $params['forTechID'] !=="")
        {
            $forTechID = $params['forTechID'];
        }
        else 
        {
            if($wo->Tech_ID !="")
            {
                $forTechID = $wo->Tech_ID ;
            }
            else
            {
                $forTechID = 0;
            }
        }
        
        $clientID = $clientInfo->getClientId();
        $WosClass->saveClient_LastViewQAWinDatetime($clientID,$win);

        $QAlist = $WosClass->getQuestionContactLog($win,0,$clientID,1,$forTechID);
        $this->view->WOVisitList = $WosClass->GetWOVisitList($win);
        $this->view->wo = $wo;
        $this->view->QAlist = $QAlist;
    }
    
    public function clientquestionandanswerAction()
    {
        $request = $this->getRequest();
        $params = $request->getParams();
        $win = $params['WIN_NUM'];
        
        $search = new Core_Api_Class;
        $companyId = $_SESSION['PM_Company_ID'];
        $wo = $search->getWorkOrdersWithWinNum($win,"","");
        $wo = (object)$wo[0];

        $WosClass = new Core_Api_WosClass();
        $Core_Api_User = new Core_Api_User;
    
        $clientInfo = $Core_Api_User->loadWithExt(
            array(
                'login' => $this->_login . '|pmContext=' . $companyId,
                'password' => $this->_password
            )
        );
        
        if($wo->Tech_ID !="")
        {
            $forTechID = $wo->Tech_ID ;
        }
        else
        {
            $forTechID = (isset($params['forTechID']) && $params['forTechID'] !=="")?$params['forTechID']:"0";
        }
        
        $clientID = $clientInfo->getClientId();
        
        $WosClass->saveClient_LastViewQAWinDatetime($clientID,$win);
        
        $QAlist = $WosClass->getQuestionContactLog($win,1,$clientID,0,$forTechID);
        $this->view->WOVisitList = $WosClass->GetWOVisitList($win);
        $this->view->wo = $wo;
        $this->view->QAlist = $QAlist;
        $this->view->clientID = $clientID;
        
    }
    
    public function postquestionAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $request = $this->getRequest();
        $params = $request->getParams();
        $question = $params['question'];
        $win = $params['WIN_NUM'];
        
        $search = new Core_Api_TechClass();
        $Core_Api_User = new Core_Api_User;
        $WosClass = new Core_Api_WosClass();
        
        $Class = new Core_Api_Class;
        $wo = $Class->getWorkOrdersWithWinNum($win,"","");
        $wo = (object)$wo[0];
        
        $WO_ID = $wo->WO_ID;
        $companyId = $wo->Company_ID;
        
        $authData = array('login' => $this->_login, 'password' => $this->_password);
        $tech = new Core_Api_TechUser;
        $tech->checkAuthentication($authData);
        $techid = $tech->getTechId();
        $techname = $tech->getFirstName().' '.$tech->getLastName();
        $WosClass->addTechQuestion($win,$this->_login, $techname, $techid, $question);
        
        //send mail
        $clientInfo = $Class->getClientByUsername($wo->WorkOrderOwner);

        $clientname = $clientInfo['ContactName'];
        $clientemail = $clientInfo['Email1'];
        $sendthis = new Core_Mail();
        $message="$techname (FS-Tech ID# $techid) has sent a Q&A for WIN#$win (Client WO ID $WO_ID):<br/><br/>
                    $question<br/><br/>
                    Click here to review this work order: <a href='https://www.fieldsolutions.com/clients/wosDetails.php?v=$companyId&id=$win'>https://www.fieldsolutions.com/clients/wosDetails.php?v=$companyId&id=$win</a><br/><br/>
                    REMINDER: You must be logged into FieldSolutions for the link to work correctly.<br/><br/>
                    Thank you,<br/>
                    Your FieldSolutions Team";
        $vFromEmail = "no-replies@fieldsolutions.com";
        $vFromName = "FieldSolutions";
        $eList = $clientInfo['Email1'];
        $vSubject = "WIN#$win (Client WO ID $WO_ID) ? Tech $techid has sent a Q&A";
 
        
        $ProjectClass = new Core_Api_ProjectClass;
        $mySettingsClass = new Core_FutureWorkOrderInfo;
        $projectinfo = $ProjectClass->getProjectById($wo->Project_ID);
        $mySettingsRow = $mySettingsClass->find($wo->WorkOrderOwner)->toArray();
		$mySettingsInfo = $mySettingsRow[0];
		
		if($projectinfo['AllowCustomCommSettings'] == "1" && ($mySettingsInfo['TechQAMessageEmailTo'] != "" || $mySettingsInfo['TechQAMessageEmailTo'] != null)){
				$sendthis->setBodyText($message);
                $sendthis->setFromEmail($vFromEmail);
                $sendthis->setFromName($vFromName);
                $sendthis->setToEmail($mySettingsInfo['TechQAMessageEmailTo']);
                $sendthis->setToName($clientname);
                $sendthis->setSubject($vSubject);
                $sendthis->send();    
                Core_TimeStamp::createTimeStamp($win, $this->_login, "Q&A Message Notification sent to Client: $clientemail", '', '', '', '', $techid, "");
		}elseif(!empty($projectinfo) && $projectinfo['ReceiveTechQAMessageNotification'] == "1"){
            $ReceiveTechQAMessageNotificationEmails = $projectinfo['ReceiveTechQAMessageNotificationEmails'];
            if(!empty($ReceiveTechQAMessageNotificationEmails)){
                $sendthis->setBodyText($message);
                $sendthis->setFromEmail($vFromEmail);
                $sendthis->setFromName($vFromName);
                $sendthis->setToEmail($ReceiveTechQAMessageNotificationEmails);
                $sendthis->setToName($ReceiveTechQAMessageNotificationEmails);
                $sendthis->setSubject($vSubject);
                $sendthis->send(); 
                Core_TimeStamp::createTimeStamp($win, $this->_login, "Q&A Message Notification sent to Client:  $ReceiveTechQAMessageNotificationEmails ", '', '', '', '', $techid, $techname);
            }
            else
            {
                $sendthis->setBodyText($message);
                $sendthis->setFromEmail($vFromEmail);
                $sendthis->setFromName($vFromName);
                $sendthis->setToEmail($clientemail);
                $sendthis->setToName($clientname);
                $sendthis->setSubject($vSubject);
                $sendthis->send();    
                Core_TimeStamp::createTimeStamp($win, $this->_login, "Q&A Message Notification sent to Client: $clientemail", '', '', '', '', $techid, "");
            }
        }
        
        //If Receive Message Notification checkbox not checked, no email goes out
        /*
        else
        {
            $sendthis->setBodyText($message);
            $sendthis->setFromEmail($vFromEmail);
            $sendthis->setFromName($vFromName);
            $sendthis->setToEmail($clientemail);
            $sendthis->setToName($clientname);
            $sendthis->setSubject($vSubject);
            $sendthis->send();    
            Core_TimeStamp::createTimeStamp($win, $this->_login, "Q&A Message Notification sent to Client: $clientemail", '', '', '', '', $techid, "");
        }
        */
        
        echo "Your message has been sent.";
    }
    
    public function postmsgAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $request = $this->getRequest();
        $params = $request->getParams();
        $question = $params['question'];
        $win = $params['WIN_NUM'];
        
        $sendtotech = (isset($params['assigntech']) && $params['assigntech'] !=="")?$params['assigntech']:$params['techlist'];
        $sendtotechname = (isset($params['assigntech']) && $params['assigntech'] !=="")?$params['assigntechname']:$params['techName'];
        
        $search = new Core_Api_TechClass();
        $Core_Api_User = new Core_Api_User;
        $WosClass = new Core_Api_WosClass();
        
        $Class = new Core_Api_Class;
        $wo = $Class->getWorkOrdersWithWinNum($win,"","");
        $wo = (object)$wo[0];
        
        $WO_ID = $wo->WO_ID;
        $companyId = $wo->Company_ID;
        
      
        $clientInfo = $Core_Api_User->loadWithExt(
            array(
                'login' => $this->_login . '|pmContext=' . $companyId,
                'password' => $this->_password
            )
        );
        $clientID = $clientInfo->getClientId();
        $clientname = $clientInfo->getContactName();
        $clientUsername= $clientInfo->getLogin();
        $groupName = "";
        if($params['techlist'] != "" && (int)$params['techlist'] <= 0){
            $groupName = $params['techlist'];
            $sendtotech = "";
        }
        
        $WosClass->addClientQAMessage($win,$clientUsername,$clientname,$clientID,$companyId,$question,$sendtotech,$sendtotechname,$groupName);
        //send mail
        $sendthis = new Core_Mail();
        $tech = new Core_Api_TechUser;
        $tech->checkAuthentication($authData);
        $techid = $tech->getTechId();
        $techEmail = $tech->getEmail();
        $techname = $tech->getFirstName()." ".$tech->getLastName();
        $vFromName = "FieldSolutions";
        $vFromEmail = "no-replies@fieldsolutions.com";

       $vFromEmail = "no-replies@fieldsolutions.com";
       $vFromName = "FieldSolutions";
       $vSubject = "WIN#$win (Client WO ID $WO_ID) Client has sent you a message";
        
        if(empty($sendtotech) && empty($groupName))
        {
            $techlist = $WosClass->getTechIDList_HaveBidOrQuestion($win);
            foreach($techlist as $techitem)
            {
           		$techId = $techitem["TechID"];
            	 $message="You have received a message regarding WIN#$win (Client WO ID $WO_ID):<br/>
                        <br/>
                    $question<br/>
                        <br/>
                    Click here to review this work order:<a href='https://www.fieldsolutions.com/techs/wosDetails.php?id=$win&v=$techId'>https://www.fieldsolutions.com/techs/wosDetails.php?id=$win&v=$techId</a><br/><br/>
                    REMINDER: You must be logged into FieldSolutions for the link to work correctly.<br/><br/>
                    Thank you,<br/>
                    Your FieldSolutions Team";
        			
                
                $sendthis->setBodyText($message);
                $sendthis->setFromEmail($vFromEmail);
                $sendthis->setFromName($vFromName);
                $sendthis->setToEmail($techitem['PrimaryEmail']);
                $sendthis->setToName($techitem['FirstName']." ".$techitem['LastName']);
                $sendthis->setSubject($vSubject);
                $sendthis->send();    
			}
        }
        else
        {
        	//  $authData = array('login' => $this->_login, 'password' => $this->_password);
      		//  $tech = new Core_Api_TechUser;
      		//  $tech->checkAuthentication($authData);
      		$tech = $WosClass->getTechInfo($sendtotech);
        	$techid = $tech['techid'];
        	$techname = $tech['FirstName'].' '.$tech['LastName'];
        	$techEmail = $tech['PrimaryEmail'];
        	 $message="You have received a message regarding WIN#$win (Client WO ID $WO_ID):<br/><br/>
                    $question<br/><br/>
                    Click here to review this work order:<a href='https://www.fieldsolutions.com/techs/wosDetails.php?id=$win&v=$techid'>https://www.fieldsolutions.com/techs/wosDetails.php?id=$win&v=$techid</a><br/><br/>
                    REMINDER: You must be logged into FieldSolutions for the link to work correctly.<br/><br/>
                    Thank you,<br/>
                    Your FieldSolutions Team";

            $sendthis->setBodyText($message);
            $sendthis->setFromEmail($vFromEmail);
            $sendthis->setFromName($vFromName);
            $sendthis->setToEmail($techEmail);
            $sendthis->setToName($techname);
            $sendthis->setSubject($vSubject);
            $sendthis->send();    
        }
        $date = date_format(new DateTime(), 'm/d/Y h:i A' );
        if($params['techlist']=="GPM")
        {
           echo("<tr>
                    <td colspan='3' style='vertical-align:top;padding-top:5px;'><span style='color:#00B050;'>$clientname</span> sent a message to GPMs ONLY: $question</td>
                    <td style='vertical-align:top;width:145px;padding-top:5px;text-align: left;'>
                        $date CST
                    </td>
                </tr>
                <tr>
                    <td colspan='4'>
                        <hr style='background-color:#d9d9d9;'>
                    </td>
                </tr>");
        }
        else if($params['techlist']=="InternalNote")
        {
            echo("<tr>
                    <td colspan='3' style='vertical-align:top;padding-top:5px;'><span style='color:#00B050;'>$clientname</span> sent an internal message: $question</td>
                    <td style='vertical-align:top;width:145px;padding-top:5px;text-align: left;'>
                        $date CST
                    </td>
                </tr>
                <tr>
                    <td colspan='4'>
                        <hr style='background-color:#d9d9d9;'>
                    </td>
                </tr>");
        }
        else if($params['techlist']=="0")
        {
            echo("<tr>
                    <td colspan='3' style='vertical-align:top;padding-top:5px;'><span style='color:#00B050;'>$clientname</span> sent a message to ALL Techs: $question</td>
                    <td style='vertical-align:top;width:145px;padding-top:5px;text-align: left;'>
                        $date CST
                    </td>
                </tr>
                <tr>
                    <td colspan='4'>
                        <hr style='background-color:#d9d9d9;'>
                    </td>
                </tr>");
        }
        else
        {
            echo("<tr>
                    <td colspan='3' style='vertical-align:top;padding-top:5px;'><span style='color:#00B050;'>$clientname</span> sent a message to <span style='color:#E46C0A;'></span> (ID: <a comid='$companyId' techid='$techid' class='techprofileforquestion' style='cursor:pointer;'>$techid</a>) $question</td>
                    <td style='vertical-align:top;width:145px;padding-top:5px;text-align:left;'>
                        $date CST
                    </td>
                </tr>
                <tr>
                    <td colspan='4'>
                        <hr style='background-color:#d9d9d9;'>
                    </td>
                </tr>");
        }
    }
    
    public function postanswerAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $request = $this->getRequest();
        $params = $request->getParams();
        $question = $params['QuestionID'];
        $win = $params['WIN_NUM'];
        $answer = $params['msg'];
        $user = new Core_Api_User();
        $WosClass = new Core_Api_WosClass();
        $authData = array('login' => $this->_login, 'password' => $this->_password);
        $clientInfo = $user->loadWithExt($authData);
        
        $UserName = $clientInfo->UserName;
        $ContactName = $clientInfo->ContactName;
        $ClientID =  $clientInfo->ClientID;
        $companyId = $_SESSION['PM_Company_ID'];
        $WosClass->addClientAnswer($win,$UserName,$ContactName,$ClientID,$companyId,$question,$answer);
        
        $tech = new Core_Api_TechUser;
        $tech->checkAuthentication($authData);
        $techid = $tech->getTechId();
        $techEmail = $tech->getEmail();
        $techname = $tech->getFirstName()." ".$tech->getLastName();
        $AccountManagerInfo=$user->getAccountManagerInfo($clientID);
        $clientname = $AccountManagerInfo['name'];
        $clientemail = $AccountManagerInfo['email'];
        $mail = new Core_Mail();
        $message="You have received a message regarding WIN#$win (Client WO ID $WO_ID):<br/><br/>
                    $answer<br/><br/>
                    Click here to review this work order:<a href='https://www.fieldsolutions.com/techs/wosDetails.php?id=$win&v=$techid'>https://www.fieldsolutions.com/techs/wosDetails.php?id=$win&v=$techid</a><br/><br/>
                    REMINDER: You must be logged into FieldSolutions for the link to work correctly.<br/><br/>
                    Thank you,<br/>
                    Your FieldSolutions Team";
        $vFromEmail = "no-replies@fieldsolutions.com";
        $vFromName = "FieldSolutions";
        $vSubject = "WIN#$win (Client WO ID $WO_ID) Client has sent you a message";
        $mail->setBodyText($message);
        $mail->setFromEmail($vFromEmail);
        $mail->setFromName($vFromName);
        $mail->setToEmail($techEmail);
        $mail->setToName($techname);
        $mail->setSubject($vSubject);
        $mail->send();
        $date = date_format(new DateTime(), 'm/d/Y h:i A' );
        echo ("<tr><td style='vertical-align:top;padding-left:20px;padding-top:5px;color:#00B050;'>$ContactName</td><td style='vertical-align:top;padding-top:5px;' colspan='2'>$answer</td><td style='vertical-align:top;'>$date</td></tr>");
    }
    
    public function clientpostquestionAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $request = $this->getRequest();
        $params = $request->getParams();
        $question = $params['question'];
        $win = $params['WIN_NUM'];
        
        $sendtotech = (isset($params['assigntech']) && $params['assigntech'] !=="")?$params['assigntech']:$params['techlist'];
        $sendtotechname = (isset($params['assigntech']) && $params['assigntech'] !=="")?$params['assigntechname']:$params['techName'];
        
        $search = new Core_Api_TechClass();
        $Core_Api_User = new Core_Api_User;
        $WosClass = new Core_Api_WosClass();
        
        $Class = new Core_Api_Class;
        $wo = $Class->getWorkOrdersWithWinNum($win,"","");
        $wo = (object)$wo[0];
        
        $WO_ID = $wo->WO_ID;
        $companyId = $wo->Company_ID;
        
      
        $clientInfo = $Core_Api_User->loadWithExt(
            array(
                'login' => $this->_login . '|pmContext=' . $companyId,
                'password' => $this->_password
            )
        );
        $clientID = $clientInfo->getClientId();
        $clientname = $clientInfo->getContactName();
        $clientUsername= $clientInfo->getLogin();
        $groupName = "";
        if($params['techlist'] != "" && (int)$params['techlist'] <= 0){
            $groupName = $params['techlist'];
            $sendtotech = "";
        }
        
        $WosClass->addClientQAMessage($win,$clientUsername,$clientname,$clientID,$companyId,$question,$sendtotech,$sendtotechname,$groupName);
        //send mail
        $sendthis = new Core_Mail();
        $tech = new Core_Api_TechUser;
        $tech->checkAuthentication($authData);
        $techid = $tech->getTechId();
        $techEmail = $tech->getEmail();
        $techname = $tech->getFirstName()." ".$tech->getLastName();
        $vFromName = "FieldSolutions";
        $vFromEmail = "no-replies@fieldsolutions.com";

       $vFromEmail = "no-replies@fieldsolutions.com";
       $vFromName = "FieldSolutions";
       $vSubject = "WIN#$win (Client WO ID $WO_ID) Client has sent you a message";
        
        if(empty($sendtotech) && empty($groupName))
        {
            $techlist = $WosClass->getTechIDList_HaveBidOrQuestion($win);
            foreach($techlist as $techitem)
            {
           		$techId = $techitem["TechID"];
            	 $message="You have received a message regarding WIN#$win (Client WO ID $WO_ID):<br/><br/>
                    $question<br/><br/>
                    Click here to review this work order:<a href='https://www.fieldsolutions.com/techs/wosDetails.php?id=$win&v=$techId'>https://www.fieldsolutions.com/techs/wosDetails.php?id=$win&v=$techId</a><br/><br/>
                    REMINDER: You must be logged into FieldSolutions for the link to work correctly.<br/><br/>
                    Thank you,<br/>
                    Your FieldSolutions Team";
        			
                
                $sendthis->setBodyText($message);
                $sendthis->setFromEmail($vFromEmail);
                $sendthis->setFromName($vFromName);
                $sendthis->setToEmail($techitem['PrimaryEmail']);
                $sendthis->setToName($techitem['FirstName']." ".$techitem['LastName']);
                $sendthis->setSubject($vSubject);
                $sendthis->send();    
			}
        }
        else
        {
        	//  $authData = array('login' => $this->_login, 'password' => $this->_password);
      		//  $tech = new Core_Api_TechUser;
      		//  $tech->checkAuthentication($authData);
      		$tech = $WosClass->getTechInfo($sendtotech);
        	$techid = $tech['techid'];
        	$techname = $tech['FirstName'].' '.$tech['LastName'];
        	$techEmail = $tech['PrimaryEmail'];
        	 $message="You have received a message regarding WIN#$win (Client WO ID $WO_ID):<br/><br/>
                    $question<br/><br/>
                    Click here to review this work order:<a href='https://www.fieldsolutions.com/techs/wosDetails.php?id=$win&v=$techid'>https://www.fieldsolutions.com/techs/wosDetails.php?id=$win&v=$techid</a><br/><br/>
                    REMINDER: You must be logged into FieldSolutions for the link to work correctly.<br/><br/>
                    Thank you,<br/>
                    Your FieldSolutions Team";

            $sendthis->setBodyText($message);
            $sendthis->setFromEmail($vFromEmail);
            $sendthis->setFromName($vFromName);
            $sendthis->setToEmail($techEmail);
            $sendthis->setToName($techname);
            $sendthis->setSubject($vSubject);
            $sendthis->send();    
        }
        
        echo "Your message has been sent.";
    }
    
    function updatePasswordSecurityAction(){
        $parames =$this->getRequest()->getParams();
        $this->view->loggedInAs = $parames["loggedInAs"];
    }
    function doUpdatePasswordSecurityAction(){
        $this->_helper->viewRenderer->setNoRender(true);
        $PasswordUpdateClass = new Core_Api_PasswordUpdateClass;
        $EmailForUpdatePassword = new Core_EmailForUpdatePassword();
        $parames =$this->getRequest()->getParams();

        switch ($parames["loggedInAs"]){
            case "tech":
                $PasswordUpdateClass->UpdatePassword_Tech($parames['userid'],$parames['password']);
                $EmailForUpdatePassword->sendMailtoTech($parames['userid']);
                break;
            case "client":
                $PasswordUpdateClass->UpdatePassword_Client($parames['userid'],$parames['password']);
                $EmailForUpdatePassword->sendMailtoClient($parames['userid']);
                break;
            case "admin":
                $PasswordUpdateClass->UpdatePassword_AdminUsername($parames['userid'],$parames['password']);
                $EmailForUpdatePassword->sendMailtoAdmin($parames['userid']);
                break;
        }
        
        if(!empty($parames['password'])){
            $_SESSION['Auth_User']['password']=$parames['password']; 
            $_SESSION["Password"] = $parames['password'];
	    	$_SESSION['UserPassword'] = $parames['password'];
        }
        
        $this->_helper->json(array("success" => true));
    }
    function winTagsAction(){
        $request = $this->getRequest();
        $params = $request->getParams();
        $win = $params['win'];
        $WosClass = new Core_Api_WosClass();
        $search = new Core_Api_Class;
        $companyId = $_SESSION['PM_Company_ID'];
        $wo = $search->getWorkOrdersWithWinNum($win,"","");
        $wo = (object)$wo[0];
        $this->view->wo = $wo;
        //13686
        $WoTagClass = new Core_Api_WoTagClass();
        $WoTagClass->adjustSiteStatus($win);
        //end 13686
        $categories = $search->getWOCategories($this->_login.'|pmContext='.$companyId, $this->_password);
        $this->view->categoriesSuccess = $categories->success;
        if ($categories->success)
            $this->view->categories = $categories->data;
        if(!empty($wo->Tech_ID)){
            $this->view->TechInfo = $WosClass->getTechInfo($wo->Tech_ID);
        }
        $this->view->CompletionStatusTagList = $WoTagClass->getCompletionStatusTagList();
        $this->view->CompletionStatusForWin = $WoTagClass->getCompletionStatusForWin($win);
        $this->view->OnTimePerformceList_forWO = $WoTagClass->GetOnTimePerformceList_forWO($win);
        
        $this->view->StartResultList = $WoTagClass->getStartResultList();
        $this->view->getEndResultList = $WoTagClass->getEndResultList();
        $this->view->DurationResultList = $WoTagClass->getDurationResultList();
        
        
        $ProjectClass = new Core_Api_ProjectClass();
        $this->view->SiteStatus = $ProjectClass->getSiteStatus_BySiteOfProject($wo->SiteNumber,$wo->Project_ID);
         

        $user = new Core_Api_User();
        $clientInfoLogin = $user->loadWithExt(
                array(
                    'login' => $this->_login,
                    'password' => $this->_password
                )
        );
        $apiClient = new Core_Api_Class();
        $isGPM = $apiClient->isGPM($clientInfoLogin->ClientID);
        
        $this->view->isGPM = $isGPM;
    }
    //686
    function editWintagsAction(){
        
        $request = $this->getRequest();
        $params = $request->getParams();
        $win = $params['win'];
        $update = $params['update'];
        $WosClass = new Core_Api_WosClass();
        $search = new Core_Api_Class;
        $companyId = $_SESSION['PM_Company_ID'];
        $wo = $search->getWorkOrdersWithWinNum($win,"","");
        $wo = (object)$wo[0];
        $this->view->wo = $wo;
        //13686
        $WoTagClass = new Core_Api_WoTagClass();
        $WoTagClass->adjustSiteStatus($win);
        //end 13686
        $categories = $search->getWOCategories($this->_login.'|pmContext='.$companyId, $this->_password);
        $this->view->categoriesSuccess = $categories->success;
        if ($categories->success)
            $this->view->categories = $categories->data;
        if(!empty($wo->Tech_ID)){
            $this->view->TechInfo = $WosClass->getTechInfo($wo->Tech_ID);
        }
        $this->view->CompletionStatusTagList = $WoTagClass->getCompletionStatusTagList();
        $this->view->CompletionStatusForWin = $WoTagClass->getCompletionStatusForWin($win);
        $this->view->OnTimePerformceList_forWO = $WoTagClass->GetOnTimePerformceList_forWO($win);
        
        $this->view->StartResultList = $WoTagClass->getStartResultList();
        $this->view->getEndResultList = $WoTagClass->getEndResultList();
        $this->view->DurationResultList = $WoTagClass->getDurationResultList();
        $this->view->update=$update;
        
        $ProjectClass = new Core_Api_ProjectClass();
        $this->view->SiteStatus = $ProjectClass->getSiteStatus_BySiteOfProject($wo->SiteNumber,$wo->Project_ID);
         

        $user = new Core_Api_User();
        $clientInfoLogin = $user->loadWithExt(
                array(
                    'login' => $this->_login,
                    'password' => $this->_password
                )
        );
        $apiClient = new Core_Api_Class();
        $isGPM = $apiClient->isGPM($clientInfoLogin->ClientID);
        $this->view->WinNumArray_InSiteOfProject = $WosClass->getWinNumArray_InSiteOfProject($wo->SiteNumber,$wo->Project_ID);
        $this->view->isGPM = $isGPM;
        
    }
    function saveWinTagsAction(){
        $this->_helper->viewRenderer->setNoRender(true);
        $request = $this->getRequest();
        $params = $request->getParams();
        $win = $params['win'];
        $CancelInternalIssuestatusval = $params['cbxCompletionStatus_40'];
        $Incomplete_NaturalCausesOrForceMajeurestatusval = $params['cbxCompletionStatus_36'];
        $Rescheduled_NaturalCausesOrForceMajeurestatusval = $params['cbxCompletionStatus_37'];
        
        //847
        $Complete1stVisitnote = $params['wintagitemaddnotevalue25'];
        $CompleteMultiVisitnote = $params['wintagitemaddnotevalue26'];
        $CompleteReassignednote = $params['wintagitemaddnotevalue27'];
        $CompleteReschedulednote = $params['wintagitemaddnotevalue28'];
        
        $IncompleteNaturalCausesnote = $params['wintagitemaddnotevalue36'];
        
        $CancelInternalIssuenote = $params['wintagitemaddnotevalue40'];
        $RescheduledNaturalCausesnote = $params['wintagitemaddnotevalue37'];
        
        //end 847
        $WoTagClass = new Core_Api_WoTagClass();
        //686
        //735 
        $search = new Core_Api_Class();
        if (!empty($_SESSION['PM_Company_ID']))
        {
            $companyId = $_SESSION['PM_Company_ID'];
        } else
        {
            $companyId = $_SESSION['Company_ID'];
        }
        $wo = $search->getWorkOrder($this->_login . '|pmContext=' . $companyId, $this->_password, $win);
        $wo = $wo->data[0];
        //end 735
        //847
        //13898
        $search = new Core_Api_Class();      
        if(!empty($_SESSION['PM_Company_ID']))
        {
        $companyId = $_SESSION['PM_Company_ID']; 
        }  else {
            $companyId = $_SESSION['Company_ID']; 
        }
        $wo = $search->getWorkOrder($this->_login . '|pmContext=' . $companyId, $this->_password, $win);           
        $wo = $wo->data[0];
	//14062	
        $result = $WoTagClass->exists_WinTagForWOWithTagItemCode($win,'Complete1stVisit');
        $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByWinAndWintagitemcode($win,'Complete1stVisit');
        if(isset($params["Complete1stVisit"]))
                    {
                        if(!$result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Added: Complete - 1st Visit", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }else

                        {    if(!empty($Complete1stVisitnote)&& strcmp($tagListForWin["notes"], $Complete1stVisitnote )!= 0)
                             {                                       
                                        Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Edit Notes: Complete - 1st Visit", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                             }  
                        }
                        $WoTagClass->saveWoTagItemValue_forCompletionStatus($win,"Complete1stVisit",1,1,"",$Complete1stVisitnote);
                                    }
                    else
                    {
                        if($result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Removed: Complete - 1st Visit", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
                        $WoTagClass->deleteWoTagItemValue_byWinAndItemCode_forCompletionStatus($win, "Complete1stVisit");
                    }
                    $result = $WoTagClass->exists_WinTagForWOWithTagItemCode($win,'CompleteMultiVisit');
                    $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByWinAndWintagitemcode($win,'CompleteMultiVisit');
         if(isset($params["CompleteMultiVisit"]))
                    {
                        if(!$result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Added: Complete - Multi Visit", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
                        else
                        {    if(!empty($CompleteMultiVisitnote) && strcmp($tagListForWin["notes"], $CompleteMultiVisitnote )!= 0)
                             {                                       
                                        Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Edit Notes: Complete - Multi Visit", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                             }  
                        }
                        $WoTagClass->saveWoTagItemValue_forCompletionStatus($win,"CompleteMultiVisit",1,1,"",$CompleteMultiVisitnote);
                    }
                    else
                    {
                        if($result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Removed: Complete - Multi Visit", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
                        $WoTagClass->deleteWoTagItemValue_byWinAndItemCode_forCompletionStatus($wo->WIN_NUM, "CompleteMultiVisit");
                        }
                    //14062    
                    $result = $WoTagClass->exists_WinTagForWOWithTagItemCode($win,'CompleteReassigned'); 
                    $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByWinAndWintagitemcode($win,'CompleteReassigned');
        if(isset($params["CompleteReassigned"]))
        {
                        if(!$result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Added: Complete - Reassigned", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
                        else
                        {   if(!empty($CompleteReassignednote) && strcmp($tagListForWin["notes"], $CompleteReassignednote )!= 0)
                             {                                       
                                        Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Edit Notes: Complete - Reassigned", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                             }  
                        }
            $WoTagClass->saveWoTagItemValue_forCompletionStatus($win,"CompleteReassigned",1,1,"",$CompleteReassignednote);
            }
        else
        {
                        if($result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Removed: Complete - Reassigned", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
                        $WoTagClass->deleteWoTagItemValue_byWinAndItemCode_forCompletionStatus($wo->WIN_NUM, "CompleteReassigned");
                    }
                    $result = $WoTagClass->exists_WinTagForWOWithTagItemCode($win,'CompleteRescheduled');        
                    $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByWinAndWintagitemcode($win,'CompleteRescheduled');
        if(isset($params["CompleteRescheduled"]))
        {
                        if(!$result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Added: Complete - Rescheduled", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }else
                        {    if(!empty($CompleteReschedulednote) && strcmp($tagListForWin["notes"], $CompleteReschedulednote )!= 0)
                             {                                       
                                        Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Edit Notes: Complete - Rescheduled", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                             }  
                        }
            $WoTagClass->saveWoTagItemValue_forCompletionStatus($win,"CompleteRescheduled",1,1,"",$CompleteReschedulednote);
        }
        else
        {
                        if($result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Removed: Complete - Rescheduled", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
            $WoTagClass->deleteWoTagItemValue_byWinAndItemCode_forCompletionStatus($win, "CompleteRescheduled");
            }
                    $result = $WoTagClass->exists_WinTagForWOWithTagItemCode($win,'CancelInternalIssue');
                    $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByWinAndWintagitemcode($win,'CancelInternalIssue');
        if(isset($params["CancelInternalIssue"]))
        {
                        if(!$result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Added: Site Cancelled", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }else
                        {    if(!empty($CancelInternalIssuenote) && strcmp($tagListForWin["notes"], $CancelInternalIssuenote )!= 0)
                             {                                       
                                        Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Edit Notes: Site Cancelled", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                             }  
                        }
            $WoTagClass->saveWoTagItemValue_forCompletionStatus($win,"CancelInternalIssue",$CancelInternalIssuestatusval,1,"",$CancelInternalIssuenote);
        }
        else
        {
                        if($result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Removed: Site Cancelled", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
            $WoTagClass->deleteWoTagItemValue_byWinAndItemCode_forCompletionStatus($win, "CancelInternalIssue");
        }
                    $result = $WoTagClass->exists_WinTagForWOWithTagItemCode($win,'Incomplete_NaturalCausesOrForceMajeure');
                    $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByWinAndWintagitemcode($win,'Incomplete_NaturalCausesOrForceMajeure');
        if(isset($params["Incomplete_NaturalCausesOrForceMajeure"]))
        {
                        if(!$result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Added: Incomplete - Natural Causes", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }else
                        {    if(!empty($IncompleteNaturalCausesnote) && strcmp($tagListForWin["notes"], $IncompleteNaturalCausesnote )!= 0)
                             {                                       
                                  Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Edit Notes: Incomplete - Natural Causes", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                             }  
                        }
            $WoTagClass->saveWoTagItemValue_forCompletionStatus($win,"Incomplete_NaturalCausesOrForceMajeure",$Incomplete_NaturalCausesOrForceMajeurestatusval,1,"",$IncompleteNaturalCausesnote);
        }
        else
        {
                        if($result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Removed: Incomplete - Natural Causes", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
            $WoTagClass->deleteWoTagItemValue_byWinAndItemCode_forCompletionStatus($win, "Incomplete_NaturalCausesOrForceMajeure");
        }
                    $result = $WoTagClass->exists_WinTagForWOWithTagItemCode($win,'Rescheduled_NaturalCausesOrForceMajeure');
                    $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByWinAndWintagitemcode($win,'Rescheduled_NaturalCausesOrForceMajeure');
        if(isset($params["Rescheduled_NaturalCausesOrForceMajeure"]))
        {
                        if(!$result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Added: Rescheduled - Natural Causes", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }else
                        {    if(!empty($RescheduledNaturalCausesnote) && strcmp($tagListForWin["notes"], $RescheduledNaturalCausesnote )!= 0)
                             {                                       
                                        Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Edit Notes: Rescheduled - Natural Causes", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                             }  
                        }
            $WoTagClass->saveWoTagItemValue_forCompletionStatus($win,"Rescheduled_NaturalCausesOrForceMajeure",$Rescheduled_NaturalCausesOrForceMajeurestatusval,1,"",$RescheduledNaturalCausesnote);
        }
        else
        {
                        if($result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Removed: Rescheduled - Natural Causes", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
            $WoTagClass->deleteWoTagItemValue_byWinAndItemCode_forCompletionStatus($win, "Rescheduled_NaturalCausesOrForceMajeure");
        }
        
        $wintagitemvalueid = $params["wintagitemvalueid"];
        //871
        $wintagitemdeletevalueid = $params["wintagitemdeletevalueid"];
        if(is_array($wintagitemdeletevalueid) && !empty($wintagitemdeletevalueid)){
             foreach($wintagitemdeletevalueid as $valueid){
                            if(!empty($valueid))
                            {
                            $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByID($valueid);                               
                            $des = "WIN-Tag Removed: ".$tagListForWin["item_sortdesc"].' '.$tagListForWin["item_owner"]." Caused";    
                            Core_TimeStamp::createTimeStamp($win, $this->_login,$des , $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);    
                            }                        
                        }
            $WoTagClass->deleteWoTagItemValueIDs($wintagitemdeletevalueid);
        }
        
        $CompletionStatusval = $params["CompletionStatusval"];
        $wintagitemaddnotevalue = $params["wintagitemaddnotevalue"];
        
        $wintagid = $params["wintagid"];
        $itemno=0;
        if(is_array($CompletionStatusval) && !empty($CompletionStatusval)){
            foreach($CompletionStatusval as $item){
                   
                           $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByID($wintagitemvalueid[$itemno]);  
                           $tagList = $WoTagClass->getTagItemInfo($wintagid[$itemno]);  
                if(empty($wintagitemvalueid[$itemno]))
                {
                               //add new
                    $WoTagClass->saveWoTagItemValue($win,$wintagid[$itemno],$item,0,'',1,$wintagitemaddnotevalue[$itemno]);
                                $des = "WIN-Tag Added: ".$tagList["item_sortdesc"].' '.$tagList["item_owner"]." Caused";                                    
                                Core_TimeStamp::createTimeStamp($win, $this->_login, $des, $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);                                                                        
                                                                       
                }
                else
                {
                    if(!in_array($wintagitemvalueid[$itemno], $wintagitemdeletevalueid))
                    {
                                    if(!empty($wintagitemaddnotevalue[$itemno]) && strcmp($tagListForWin["notes"], $wintagitemaddnotevalue[$itemno] )!= 0)
                                    {
                                        $des = "WIN-Tag Edit Notes: ".$tagList["item_sortdesc"].' '.$tagList["item_owner"]." Caused";
                                        Core_TimeStamp::createTimeStamp($win, $this->_login, $des, $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                                    }                                        
                        $WoTagClass->saveWoTagItemValue($win,$wintagid[$itemno],$item,1,'',0,$wintagitemaddnotevalue[$itemno],$wintagitemvalueid[$itemno]);
                    }
                }
                $itemno +=1;
            }
        }
        //End 13898   
        //end 871
        //end 847
        
        if(!isset($params["chkIncompleteSiteClientCaused"]))
        {
            $WoTagClass->deleteWoTagItemValue_byWinAndIncompleteSiteClient($win);
        }
        
        if(!isset($params["chkIncompleteTechProviderCaused"]))
        {
            $WoTagClass->deleteWoTagItemValue_byWinAndIncompleteTechProvider($win);
        }
        
        if(!isset($params["chkRescheduledSiteClientCaused"]))
        {
            $WoTagClass->deleteWoTagItemValue_byWinAndRescheduledSiteClient($win);
        }
        
        if(!isset($params["chkRescheduledTechProviderCaused"]))
        {
            $WoTagClass->deleteWoTagItemValue_byWinAndRescheduledTechProvider($win);            
        }
        
        //
        //end 686;
        //save visit
        $visits = $params["Visit"];
        if(is_array($visits) && !empty($visits)){
            foreach($visits as $item){
                $WoTagClass->saveOntimePerformanceResult($win,$item,
                        $params["StartResultList_".$item],
                        $params["EndResultList_".$item],
                        $params["DurationResultList_".$item]);
            }
        }
        // save WIN
        if(isset($params["SATRecommended"]) && isset($params["SATPerformance"])){
        $WoTagClass->saveQualityPerformance_forWin($win,$params["SATRecommended"],$params["SATPerformance"]);
        }
        //--- adjustSiteStatus
        //13843
        $WoTagClass->updateSiteStatus_ByWin($win);
        //end 13843
        //13686
        $WoTagClass->adjustSiteStatus($win);
        //end 13686

        //735
        $apifsexpert = new Core_Api_FSExpertClass();
        if(!empty($wo->Tech_ID))
        {
            if(isset($params['chkskillcategory']))
            {
                $apifsexpert->saveTechFSExpert($wo->Tech_ID,$params['FSExpertId'],1);
            }
            else
            {
                $apifsexpert->saveTechFSExpert($wo->Tech_ID,$params['FSExpertId'],0);
            }
        }
        //end 735

        //--- response
        $this->_helper->json(array("success" => true));
    }
    
    //13843
    public function updateandaddwintagAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        $request = $this->getRequest();
        $params = $request->getParams();
        $win = $params['win'];
        //$CompletionStatus = $params["CompletionStatus"];
        $CancelInternalIssuestatusval = $params['cbxCompletionStatus_40'];
        $Incomplete_NaturalCausesOrForceMajeurestatusval = $params['cbxCompletionStatus_36'];
        $Rescheduled_NaturalCausesOrForceMajeurestatusval = $params['cbxCompletionStatus_37'];
        //847
        $Complete1stVisitnote = $params['wintagitemaddnotevalue25'];
        $CompleteMultiVisitnote = $params['wintagitemaddnotevalue26'];
        $CompleteReassignednote = $params['wintagitemaddnotevalue27'];
        $CompleteReschedulednote = $params['wintagitemaddnotevalue28'];
        
        $IncompleteNaturalCausesnote = $params['wintagitemaddnotevalue36'];
        
        $CancelInternalIssuenote = $params['wintagitemaddnotevalue40'];
        $RescheduledNaturalCausesnote = $params['wintagitemaddnotevalue37'];
        
        //end 847
        $WoTagClass = new Core_Api_WoTagClass();
    
        if (update == 'true')
        {
            $WIN_Select = $params['WIN_Select'];
            $updateDateAdd = $params["DateNew_" . $woTagItemValueIDNew];
            //$updateWinNum,$updateWoTagItemID,$updateValueID,$updateDateAdd
            $WoTagClass->insertWoTagItemValue_ByID(
                    $WIN_Select, "", "", date_format(new DateTime($updateDateAdd), "Y-m-d h:i:s"));
            $win = $WIN_Select;
        }
        //847
        //13898
        $search = new Core_Api_Class();      
        if(!empty($_SESSION['PM_Company_ID']))
        {
        $companyId = $_SESSION['PM_Company_ID']; 
        }  else {
            $companyId = $_SESSION['Company_ID']; 
        }
        $wo = $search->getWorkOrder($this->_login . '|pmContext=' . $companyId, $this->_password, $win);           
        $wo = $wo->data[0];
		
        $result = $WoTagClass->exists_WinTagForWOWithTagItemCode($win,'Complete1stVisit');
        $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByWinAndWintagitemcode($win,'Complete1stVisit');
                    if(isset($params["Complete1stVisit"]))
                    {            
                        if(!$result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Added: Complete - 1st Visit", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }else
                        {    if(!empty($Complete1stVisitnote)&& strcmp($tagListForWin["notes"], $Complete1stVisitnote )!= 0)
                             {                                       
                                        Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Edit Notes: Complete - 1st Visit", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                             }  
                        }
                        $WoTagClass->saveWoTagItemValue_forCompletionStatus($win,"Complete1stVisit",1,1,"",$Complete1stVisitnote);
                                    }
                    else
                    {
                        if($result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Removed: Complete - 1st Visit", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
            $WoTagClass->deleteWoTagItemValue_byWinAndItemCode_forCompletionStatus($win, "Complete1stVisit");
        }
                    $result = $WoTagClass->exists_WinTagForWOWithTagItemCode($win,'CompleteMultiVisit');
                    $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByWinAndWintagitemcode($win,'CompleteMultiVisit');
                    if(isset($params["CompleteMultiVisit"]))
                    {
                        if(!$result)
        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Added: Complete - Multi Visit", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
        }
                        else
                        {    if(!empty($CompleteMultiVisitnote) && strcmp($tagListForWin["notes"], $CompleteMultiVisitnote )!= 0)
        {
                                        Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Edit Notes: Complete - Multi Visit", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                             }  
                        }
                        $WoTagClass->saveWoTagItemValue_forCompletionStatus($win,"CompleteMultiVisit",1,1,"",$CompleteMultiVisitnote);
        }
                    else
                    {
                        if($result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Removed: Complete - Multi Visit", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
                        $WoTagClass->deleteWoTagItemValue_byWinAndItemCode_forCompletionStatus($wo->WIN_NUM, "CompleteMultiVisit");
                    }
                    $result = $WoTagClass->exists_WinTagForWOWithTagItemCode($win,'CompleteReassigned'); 
                    $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByWinAndWintagitemcode($win,'CompleteReassigned');
                    if(isset($params["CompleteReassigned"]))
                    {
                        if(!$result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Added: Complete - Reassigned", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
                        else
                        {   if(!empty($CompleteReassignednote) && strcmp($tagListForWin["notes"], $CompleteReassignednote )!= 0)
                             {                                       
                                        Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Edit Notes: Complete - Reassigned", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                             }  
                        }
                        $WoTagClass->saveWoTagItemValue_forCompletionStatus($win,"CompleteReassigned",1,1,"",$CompleteReassignednote);
                    }
                    else
                    {
                        if($result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Removed: Complete - Reassigned", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
                        $WoTagClass->deleteWoTagItemValue_byWinAndItemCode_forCompletionStatus($wo->WIN_NUM, "CompleteReassigned");
                    }
                    $result = $WoTagClass->exists_WinTagForWOWithTagItemCode($win,'CompleteRescheduled');        
                    $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByWinAndWintagitemcode($win,'CompleteRescheduled');
                    if(isset($params["CompleteRescheduled"]))
                    {
                        if(!$result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Added: Complete - Rescheduled", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }else
                        {    if(!empty($CompleteReschedulednote) && strcmp($tagListForWin["notes"], $CompleteReschedulednote )!= 0)
                             {                                       
                                        Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Edit Notes: Complete - Rescheduled", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                             }  
                        }
                        $WoTagClass->saveWoTagItemValue_forCompletionStatus($win,"CompleteRescheduled",1,1,"",$CompleteReschedulednote);
                    }
                    else
                    {
                        if($result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Removed: Complete - Rescheduled", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
            $WoTagClass->deleteWoTagItemValue_byWinAndItemCode_forCompletionStatus($win, "CompleteRescheduled");
        }
                    $result = $WoTagClass->exists_WinTagForWOWithTagItemCode($win,'CancelInternalIssue');
                    $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByWinAndWintagitemcode($win,'CancelInternalIssue');
                    if(isset($params["CancelInternalIssue"]))
                    {
                        if(!$result)
        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Added: Site Cancelled", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }else
                        {    if(!empty($CancelInternalIssuenote) && strcmp($tagListForWin["notes"], $CancelInternalIssuenote )!= 0)
                             {                                       
                                        Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Edit Notes: Site Cancelled", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                             }  
                        }
                        $WoTagClass->saveWoTagItemValue_forCompletionStatus($win,"CancelInternalIssue",$CancelInternalIssuestatusval,1,"",$CancelInternalIssuenote);
                    }
                    else
                    {
                        if($result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Removed: Site Cancelled", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
            $WoTagClass->deleteWoTagItemValue_byWinAndItemCode_forCompletionStatus($win, "CancelInternalIssue");
        }
                    $result = $WoTagClass->exists_WinTagForWOWithTagItemCode($win,'Incomplete_NaturalCausesOrForceMajeure');
                    $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByWinAndWintagitemcode($win,'Incomplete_NaturalCausesOrForceMajeure');
                    if(isset($params["Incomplete_NaturalCausesOrForceMajeure"]))
                    {
                        if(!$result)
        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Added: Incomplete - Natural Causes", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }else
                        {    if(!empty($IncompleteNaturalCausesnote) && strcmp($tagListForWin["notes"], $IncompleteNaturalCausesnote )!= 0)
                             {                                       
                                  Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Edit Notes: Incomplete - Natural Causes", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                             }  
                        }
                        $WoTagClass->saveWoTagItemValue_forCompletionStatus($win,"Incomplete_NaturalCausesOrForceMajeure",$Incomplete_NaturalCausesOrForceMajeurestatusval,1,"",$IncompleteNaturalCausesnote);
                    }
                    else
                    {
                        if($result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Removed: Incomplete - Natural Causes", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
            $WoTagClass->deleteWoTagItemValue_byWinAndItemCode_forCompletionStatus($win, "Incomplete_NaturalCausesOrForceMajeure");
        }
                    $result = $WoTagClass->exists_WinTagForWOWithTagItemCode($win,'Rescheduled_NaturalCausesOrForceMajeure');
                    $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByWinAndWintagitemcode($win,'Rescheduled_NaturalCausesOrForceMajeure');
                    if(isset($params["Rescheduled_NaturalCausesOrForceMajeure"]))
                    {
                        if(!$result)
        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Added: Rescheduled - Natural Causes", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }else
                        {    if(!empty($RescheduledNaturalCausesnote) && strcmp($tagListForWin["notes"], $RescheduledNaturalCausesnote )!= 0)
                             {                                       
                                        Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Edit Notes: Rescheduled - Natural Causes", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                             }  
                        }
                        $WoTagClass->saveWoTagItemValue_forCompletionStatus($win,"Rescheduled_NaturalCausesOrForceMajeure",$Rescheduled_NaturalCausesOrForceMajeurestatusval,1,"",$RescheduledNaturalCausesnote);
                    }
                    else
                    {
                        if($result)
                        {
                            Core_TimeStamp::createTimeStamp($win, $this->_login, "WIN-Tag Removed: Rescheduled - Natural Causes", $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                        }
            $WoTagClass->deleteWoTagItemValue_byWinAndItemCode_forCompletionStatus($win, "Rescheduled_NaturalCausesOrForceMajeure");
        }

        $wintagitemvalueid = $params["wintagitemvalueid"];
        //871
        $wintagitemdeletevalueid = $params["wintagitemdeletevalueid"];
        if(is_array($wintagitemdeletevalueid) && !empty($wintagitemdeletevalueid)){
             foreach($wintagitemdeletevalueid as $valueid){
                            if(!empty($valueid))
                            {
                            $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByID($valueid);                               
                            $des = "WIN-Tag Removed: ".$tagListForWin["item_sortdesc"].' '.$tagListForWin["item_owner"]." Caused";    
                            Core_TimeStamp::createTimeStamp($win, $this->_login,$des , $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);    
                            }                        
                        }
            $WoTagClass->deleteWoTagItemValueIDs($wintagitemdeletevalueid);
        }
        
        $CompletionStatusval = $params["CompletionStatusval"];
        $wintagitemaddnotevalue = $params["wintagitemaddnotevalue"];
        //end 847
        
        $wintagid = $params["wintagid"];
        $itemno=0;
        if(is_array($CompletionStatusval) && !empty($CompletionStatusval)){
              foreach($CompletionStatusval as $item){
                   
                           $tagListForWin = $WoTagClass->getWinTagItemValueInfo_ByID($wintagitemvalueid[$itemno]);  
                           $tagList = $WoTagClass->getTagItemInfo($wintagid[$itemno]);  
               if(empty($wintagitemvalueid[$itemno]))
                {
                               //add new
                    $WoTagClass->saveWoTagItemValue($win,$wintagid[$itemno],$item,0,'',1,$wintagitemaddnotevalue[$itemno]);
                                $des = "WIN-Tag Added: ".$tagList["item_sortdesc"].' '.$tagList["item_owner"]." Caused";                                    
                                Core_TimeStamp::createTimeStamp($win, $this->_login, $des, $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);                                                                        
                                                                       
                }
                else
                {
                    if(!in_array($wintagitemvalueid[$itemno], $wintagitemdeletevalueid))
                    {
                                    if(!empty($wintagitemaddnotevalue[$itemno]) && strcmp($tagListForWin["notes"], $wintagitemaddnotevalue[$itemno] )!= 0)
                                    {
                                        $des = "WIN-Tag Edit Notes: ".$tagList["item_sortdesc"].' '.$tagList["item_owner"]." Caused";
                                        Core_TimeStamp::createTimeStamp($win, $this->_login, $des, $companyId,"", $wo->Project_Name,"", $wo->Tech_ID, $wo->Tech_FName . ' ' . $wo->Tech_LName);
                                    }                                        
                        $WoTagClass->saveWoTagItemValue($win,$wintagid[$itemno],$item,1,'',0,$wintagitemaddnotevalue[$itemno],$wintagitemvalueid[$itemno]);
                    }
                }
                $itemno +=1;
            }
        }
        //End 13898  

        //end 871
        if(!isset($params["chkIncompleteSiteClientCaused"]))
        {
            $WoTagClass->deleteWoTagItemValue_byWinAndIncompleteSiteClient($win);
        }
        
        if(!isset($params["chkIncompleteTechProviderCaused"]))
        {
            $WoTagClass->deleteWoTagItemValue_byWinAndIncompleteTechProvider($win);
        }
        
        if(!isset($params["chkRescheduledSiteClientCaused"]))
        {
            $WoTagClass->deleteWoTagItemValue_byWinAndRescheduledSiteClient($win);
        }
        
        if(!isset($params["chkRescheduledTechProviderCaused"]))
        {
            $WoTagClass->deleteWoTagItemValue_byWinAndRescheduledTechProvider($win);            
        }
        //13843
        $WoTagClass->updateSiteStatus_ByWin($win);   
        $WoTagClass->adjustSiteStatus($win);        
    }
    public function bidWithdrawQaAction() {
        require_once 'Core/Api/TechClass.php';
        require_once 'API/WorkOrderFilter.php';

        $request = $this->getRequest();
        $win = $request->getParam('win', NULL);
        $techId = $request->getParam('tech_id', NULL);

        if (empty($win)) {

            $this->_helper->viewRenderer->setNoRender(true);

            $response = $this->getResponse();
            $response->clearAllHeaders();
            $response->clearBody();

            $response
                    ->setHttpResponseCode(406)
                    ->setBody('WIN# not found or is no longer available');
            return;
        }

        $paths = $this->getFrontController()->getControllerDirectory();
        if (!empty($paths['dashboard']) && file_exists($paths['dashboard'] . '/IndexController.php')) {
            require_once $paths['dashboard'] . '/IndexController.php';
            $fields = Dashboard_IndexController::FIELDS;
        } else {
            $fields = NULL;
        }

        $filters = new API_WorkOrderFilter;
        $filters->Deactivated = FALSE;
        $filters->ShowTechs = TRUE;
        //$filters->Sourced       = FALSE;
        $filters->Tech = NULL;
        $filters->TB_UNID = $win;

        $search = new Core_Api_TechClass;

        $wo = $search->getWorkOrders($this->_login, $this->_password, $fields, $filters);
        if (!$wo->success) {

            $this->_helper->viewRenderer->setNoRender(true);

            $response = $this->getResponse();
            $response->clearAllHeaders();
            $response->clearBody();

            $response
                    ->setHttpResponseCode(406)
                    ->setBody($wo->errors[0]->message);
            return;
        } else if (!$wo->errors && empty($wo->data)) {

            $this->_helper->viewRenderer->setNoRender(true);

            $response = $this->getResponse();
            $response->clearAllHeaders();
            $response->clearBody();

            $response
                    //->setHttpResponseCode(406)
                    ->setBody('WIN# not found or is no longer available'/* Core_Api_TechClass::ERROR_WO_INVALID_WIN */);
            return;
        }

        $prj_id = $wo->success ? $wo->data[0]->Project_ID : '';
        $project = $search->getProjects($this->_login, $this->_password, array($prj_id), '');
        $lastwobid = $search->getLastBid($this->_login, $this->_password,$win);
        
        $this->view->wo = $wo->data[0];
        $this->view->prj = $project->success ? $project->data[0] : new stdClass();
        $this->view->lastwobid = $lastwobid;
    }
    function siteStatusAction(){
        $request = $this->getRequest();
        $params = $request->getParams();
        $WosClass = new Core_Api_WosClass();
        $WoTagClass = new Core_Api_WoTagClass();
        $ProjectClass = new  Core_Api_ProjectClass;
        if(!empty($params["por"])){
            $this->_helper->viewRenderer->setNoRender(true);
            $woTagItemValueIDs = $params['woTagItemValueID'];
            $woTagItemValueIDNews = $params['woTagItemValueIDNew'];
            // update Site status
            $ProjectClass->saveSiteStatus_BySiteOfProject($params["SiteNumber"],$params["Project_ID"],$params["cbxSiteStatus"]);
            // add tag
            if(is_array($woTagItemValueIDNews) && !empty($woTagItemValueIDNews)){
                foreach($woTagItemValueIDNews as $woTagItemValueIDNew){
                    $updateDateAdd = $params["DateNew_".$woTagItemValueIDNew];
                    $WoTagClass->insertWoTagItemValue_ByID(
                        $params["WINNew_".$woTagItemValueIDNew],
                        $params["TagsNew_".$woTagItemValueIDNew],
                        $params["InvoiceableNew_".$woTagItemValueIDNew],
                        !empty($updateDateAdd)?date_format(new DateTime($updateDateAdd), "Y-m-d"):"");
}
            }
            // update tag
            if(is_array($woTagItemValueIDs) && !empty($woTagItemValueIDs)){
                foreach($woTagItemValueIDs as $woTagItemValueID){
                    $updateDateAdd = $params["Date_".$woTagItemValueID];
                    $WoTagClass->updateWoTagItemValue_ByID(
                        $woTagItemValueID,
                        $params["WIN_".$woTagItemValueID],
                        $params["Tags_".$woTagItemValueID],
                        $params["Invoiceable_".$woTagItemValueID],
                        !empty($updateDateAdd)?date_format(new DateTime($updateDateAdd), "Y-m-d"):"");
                }
            }
            $this->_helper->json(array("success" => $params));
        }else{
            $win = $params['win'];
            $WosClass->createSiteIfNotExist_withDatafromWO($win);
            $search = new Core_Api_Class;
            $companyId = $_SESSION['PM_Company_ID'];
            $wo = $search->getWorkOrdersWithWinNum($win,"","");
            $wo = (object)$wo[0];
            $this->view->wo = $wo;
            $this->view->SiteStatusList = $ProjectClass->getSiteStatusList();
            $this->view->NumOfWOs_InSiteOfProject = $WosClass->getNumOfWOs_InSiteOfProject($wo->SiteNumber,$wo->Project_ID);
            $this->view->CSWinTagsInSiteOfProject = $WoTagClass->getCompletionStatusWinTags_InSiteOfProject($wo->SiteNumber,$wo->Project_ID);
            $this->view->WinNumArray_InSiteOfProject = $WosClass->getWinNumArray_InSiteOfProject($wo->SiteNumber,$wo->Project_ID);
            $this->view->CompletionStatusTagList = $WoTagClass->getCompletionStatusTagList(array('Scheduled'));
            $this->view->siteInfo = $ProjectClass->getSiteStatus_BySiteOfProject($wo->SiteNumber,$wo->Project_ID);
        }

    }
    
    //735
    function expertListAction(){
        $apiFSExpertClass = new Core_Api_FSExpertClass();
        $FSExpertslist = $apiFSExpertClass->getFSExperts();
        $this->view->Expertlist = $FSExpertslist;
}
    //end 735
}
