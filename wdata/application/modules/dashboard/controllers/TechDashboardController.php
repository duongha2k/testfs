<?php

class Dashboard_TechDashboardController extends Core_Controller_Action {

    const FIELDS = 'WIN_NUM,WO_ID,DateEntered,PO,ShowTechs,SourceByDate,ShortNotice,StartDate,StartTime,EndDate,EndTime,StartRange,Duration,Region,Route,Type_ID,Project_ID,WO_Category_ID,Headline,Site_Contact_Name,SiteName,SiteNumber,Project_Name,Address,City,State,Zipcode,SitePhone,SiteFax,SiteEmail,Update_Requested,Update_Reason,Description,Requirements,Tools,SpecialInstructions,Lead,Assist,SignOffSheet_Required,ProjectManagerName,ProjectManagerPhone,ProjectManagerEmail,ResourceCoordinatorName,ResourceCoordinatorPhone,ResourceCoordinatorEmail,EmergencyName,EmergencyPhone,EmergencyEmail,TechnicalSupportName,TechnicalSupportPhone,TechnicalSupportEmail,CheckInOutName,CheckInOutNumber,CheckInOutEmail,Tech_ID,Tech_FName,Tech_LName,NoShow_Tech,BackOut_Tech,FLS_ID,BU1FLSID,BU1Info,Backup_FLS_ID,Ship_Contact_Info,PayMax,Amount_Per,Qty_Devices,Tech_Bid_Amount,WorkOrderReviewed,TechCheckedIn_24hrs,CheckedIn,Checkedby,PreCall_1,PreCall_2,CheckInNotes,StoreNotified,Notifiedby,DateNotified,NotifyNotes,TechMarkedComplete,Date_In,Time_In,Date_Out,Time_Out,ContactHD,HDPOC,TechMiles,TechComments,AttachedFile,FLS_OOS,Unexpected_Steps,Unexpected_Desc,AskedBy,AskedBy_Name,Paperwork_Received,Incomplete_Paperwork,MissingComments,Site_Complete,CallClosed,PartsProcessed,ClientComments,Work_Out_of_Scope,OutofScope_Amount,OutofScope_Reason,TripCharge,MileageReimbursement,MaterialsReimbursement,Additional_Pay_Amount,Penalty_Reason,AbortFee,AbortFeeAmount,Add_Pay_AuthBy,Add_Pay_Reason,calculatedTechHrs,baseTechPay,PayAmount,Approved,TechPaid,DatePaid,Deactivated,DeactivationCode,Deactivated_Reason,General1,General2,General3,General4,General5,General6,General7,General8,General9,General10,FS_InternalComments,Username,Invoiced,Company_Name,Company_ID,TechEmail,TechPhone,TechPhoneExt,Date_Assigned,lastModBy,lastModDate,approvedBy,DateApproved,DeactivatedDate,DeactivatedBy,Latitude,Longitude,PricingRan,WO_Category,DateIncomplete,ReminderAcceptance,Reminder24Hr,Reminder1Hr,CheckInCall,CheckOutCall,ReminderNotMarkComplete,ReminderIncomplete,ReminderAll,SMSBlast,MinutesRemainPublished,MinimumSelfRating,MaximumAllowableDistance,isProjectAutoAssign,isWorkOrdersFirstBidder,P2TPreferredOnly,P2TAlreadyAssigned,SATRecommended,SATPerformance,ReturnPartCount,NewPartCount,Qty_Applicants,DateInvoiced,Work_Out_of_Scope,OutOfScope_Reason,Date_Completed,PcntDeduct,DateIncomplete,Net_Pay_Amount,Status,PcntDeductPercent,PayAmount_Final';

    private $_techId;
    private $_filters;
    private $_size;
    private $_page;

    public function init() {
        parent::init();

        //  This is constroller can be used for AJAX only!
        $request = $this->getRequest();
        if ( !$request->isXmlHttpRequest() ) {
            if ( $this->getRequest()->getParam('download', false) !== 'csv' &&
                 $this->getRequest()->getParam('rt', false) !== 'iframe') {
                $this->_redirect('/');
            }
        }

        $this->_techId = $_SESSION['TechID'];//$request->getParam('tech_id', NULL);
        $this->view->techId = $this->_techId;

        $this->_sortStr = $this->_assembleSortStr(array(
            's1' => $request->getParam('sort1', NULL), 'd1' => $request->getParam('dir1', NULL),
            's2' => $request->getParam('sort2', NULL), 'd2' => $request->getParam('dir2', NULL),
            's3' => $request->getParam('sort3', NULL), 'd3' => $request->getParam('dir3', NULL)
        ));

        $version = $request->getParam('version', NULL);

        if(!empty($version) && $version == 'Lite')
            $this->view->addScriptPath(DASHBOARD_MODULE_PATH.'views'.DIRECTORY_SEPARATOR.'scripts'.DIRECTORY_SEPARATOR.'lite');

        // Get Method when user download a CSV file
        if ( $request->isGet() ) {
            if ( NULL !== ($projects = $request->getParam('projects', NULL)) ) {
                if ( is_string($projects) && strpos($projects, '|', 1) ) {
                    $projects = explode('|', $projects);
                    $request->setParam('projects', $projects);
                }
            }
            if ( NULL !== ($startdate = $request->getParam('start_date', NULL)) ) {
                $request->setParam('start_date', str_replace('_', '/', $startdate));
            }
            if ( NULL !== ($enddate = $request->getParam('end_date', NULL)) ) {
                $request->setParam('end_date', str_replace('_', '/', $enddate));
            }
        }

        $projects           = $request->getParam('projects', NULL);
        $show_to_tech       = $request->getParam('show_to_tech', NULL);
        $update_req         = $request->getParam('update_req', NULL);
        $store_notify       = $request->getParam('store_notify', NULL);
        $checked_in         = $request->getParam('checked_in', NULL);
        $reviewed_wo        = $request->getParam('reviewed_wo', NULL);
        $tech_confirmed     = $request->getParam('tech_confirmed', NULL);
        $tech_complete      = $request->getParam('tech_complete', NULL);
        $site_complete      = $request->getParam('site_complete', NULL);
        $approved           = $request->getParam('approved', NULL);
        $out_of_scope       = $request->getParam('out_of_scope', NULL);
        $tech_paid          = $request->getParam('tech_paid', NULL);
        $show_sourced       = $request->getParam('show_sourced', NULL);
        $show_not_sourced   = $request->getParam('show_not_sourced', NULL);
        $paper_received     = $request->getParam('paper_received', NULL);
        $lead               = $request->getParam('lead', NULL);
        $assist             = $request->getParam('assist', NULL);
        $deactivated        = $request->getParam('deactivated', NULL);

        if ( $show_not_sourced === NULL && $show_sourced === NULL )
            $sourced = NULL;
        else if ( $show_sourced + 0 > 0 && $show_not_sourced + 0 > 0 )
            $sourced = NULL;
        else if ( $show_not_sourced + 0 > 0  )
            $sourced = 'False';
        else
            $sourced = 'True';

        $this->_filters = new API_WorkOrderFilter;
        $this->_filters->Sourced                = $sourced;
        $this->_filters->TB_UNID                = $request->getParam('win', NULL);
        $this->_filters->Call_Type              = $request->getParam('call_type', NULL);
        $this->_filters->Project_ID             = ( is_array($projects) ) ? ( (count($projects) > 1) ? $projects : $projects[0] ) : $projects;
        $this->_filters->StartDateFrom          = $request->getParam('start_date', NULL);
        if (!empty($this->_filters->StartDateFrom) ) {
            $this->_filters->StartDateFrom = str_replace('s', '/', $this->_filters->StartDateFrom);
        }
        $this->_filters->StartDateTo            = $request->getParam('end_date', NULL);
            if (!empty($this->_filters->StartDateTo) ) {
            $this->_filters->StartDateTo = str_replace('s', '/', $this->_filters->StartDateTo);
        }
        $this->_filters->WO_ID                  = $request->getParam('client_wo', NULL);
        $this->_filters->PO                     = $request->getParam('po', NULL);
        $this->_filters->ShowTechs              = ( NULL === $show_to_tech) ? NULL : ( ( $show_to_tech + 0 ) ? true : false );
        $this->_filters->Region                 = $request->getParam('region', NULL);
        $this->_filters->Route                 = $request->getParam('route', NULL);
        $this->_filters->SiteName               = $request->getParam('site_name', NULL);
        $this->_filters->SiteNumber             = $request->getParam('site_number', NULL);
        $this->_filters->City                   = $request->getParam('city', NULL);
        $this->_filters->State                  = $request->getParam('state', NULL);
        $this->_filters->Country                  = $request->getParam('country', NULL);
        $this->_filters->Zipcode                = $request->getParam('zip', NULL);
        $this->_filters->Update_Requested       = ( NULL === $update_req ) ? NULL : ( ( $update_req + 0 ) ? true : false );
        $this->_filters->CheckedIn              = ( NULL === $checked_in ) ? NULL : ( ( $checked_in + 0 ) ? true : false );
        $this->_filters->WorkOrderReviewed      = ( NULL === $reviewed_wo ) ? NULL : ( ( $reviewed_wo + 0 ) ? true : false );
        $this->_filters->TechCheckedIn_24hrs    = ( NULL === $tech_confirmed ) ? NULL : ( ( $tech_confirmed + 0 ) ? true : false );
        $this->_filters->TechMarkedComplete     = ( NULL === $tech_complete ) ? NULL : ( ( $tech_complete + 0 ) ? true : false );
        $this->_filters->Site_Complete          = ( NULL === $site_complete ) ? NULL : ( ( $site_complete + 0 ) ? true : false );
        $this->_filters->Approved               = ( NULL === $approved ) ? NULL : ( ( $approved + 0 ) ? true : false );
        $this->_filters->Additional_Pay_Amount  = $request->getParam('extra_pay', NULL);
        $this->_filters->Work_Out_of_Scope      = ( NULL === $out_of_scope ) ? NULL : ( ( $out_of_scope + 0 ) ? true : false );
        $this->_filters->TechPaid               = ( NULL === $tech_paid ) ? NULL : ( ( $tech_paid + 0 ) ? true : false );
        $this->_filters->Tech_FName             = $request->getParam('tech_fname', NULL);
        $this->_filters->Tech_LName             = $request->getParam('tech_lname', NULL);
        $this->_filters->Tech                   = $this->_techId+0;
        $this->_filters->Deactivated            = ( NULL === $deactivated ) ? NULL : ( ( $deactivated + 0 ) ? true : false );
        $this->_filters->StoreNotified          = ( NULL === $store_notify) ? NULL : ( ( $store_notify + 0 ) ? true : false );
        $this->_filters->Paperwork_Received     = ( NULL === $paper_received ) ? NULL : ( ( $paper_received + 0 ) ? true : false );
        $this->_filters->Lead                   = ( NULL === $lead ) ? NULL : ( ( $lead + 0 ) ? true : false );
        $this->_filters->Assist                 = ( NULL === $assist ) ? NULL : ( ( $assist + 0 ) ? true : false );

        $distance = $request->getParam('distance', NULL);
        if (!empty($distance))
            $this->_filters->Distance = '<=' . $distance;

        $this->_size    = ($request->getParam('size', empty($_COOKIE['page_size']) ? 25 : $_COOKIE['page_size'] )) + 0;
        $this->_size    = ( $this->_size <= 10 ) ? 10 : ( ( $this->_size >= 100 ) ? 100 : $this->_size );
        
        if ($request->getParam('size', null) != null) {
            $domain = '.' . preg_replace('/^www\./', '', $_SERVER['HTTP_HOST']);
            setcookie("page_size", $this->_size, time() + 3600 * 24 * 30, "/", $domain);
        }
        
        $this->_page    = ($request->getParam('page', 1)) + 0;
        $this->_page    = ( $this->_page < 1 ) ? 1 : $this->_page;

        $this->view->pageCurrent = $this->_page;
        $this->view->pageSize = $this->_size;
        
        $this->view->nonav = $request->getParam('nonav',false);
        $this->view->iframeMode = ($this->getRequest()->getParam('rt', false) == 'iframe');
    }

        /**
     * availableAction
     *
     * Render data for 'available' tab
     *
     * @access public
     * @return void
     */
    public function availableAction() {
        $request    = $this->getRequest();
        $search     = new Core_Api_TechClass;
        $countRows  = -1;
        $this->_filters->Tech        = NULL;
        $this->_filters->WO_State = "published";
        if ($this->_filters->Distance === null)
            $this->_filters->Distance = "<= 9999999";
        $this->_filters->hideAvaiableIfClientDeniedForTech = $this->_techId;//197

        /** Save workorders into CSV file **/
        if ( $request->getParam('download', false) === 'csv' ) {
            ini_set('memory_limit', '512M');
            $wos = $search->getWorkOrders(
                    $this->_login, $this->_password, self::FIELDS, $this->_filters, 0, 0, $countRows, $this->_sortStr
            );
            return $this->_formatResultsAsCsv($wos, 'available');
        }
        /**  End section: Save created into CSV file  **/
        $tab = $request->getParam('tab');
        $wos = $search->getWorkOrders(
                $this->_login, $this->_password, self::FIELDS, $this->_filters, ($this->_page - 1) * $this->_size, $this->_size, $countRows, $this->_sortStr,$tab
        );
        if ( $wos->success ) {
            $paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($wos->data,$countRows));
            $paginator->setCurrentPageNumber($this->_page);
            $paginator->setItemCountPerPage($this->_size);
            $this->pullApplicantsInfo($paginator);
            $this->pullwithdrawApplicantsInfo($paginator);
            $this->view->paginator = $paginator;
        }
        $this->view->success    = $wos->success;
        $this->view->errors     = $wos->errors;
    }

    /**
     * assignedAction
     *
     * Render data for 'assigned' tab
     *
     * @access public
     * @return void
     */
    public function assignedAction() {
        $request = $this->getRequest();
        $search = new Core_Api_TechClass;
        $countRows = -1;
        $this->_filters->WO_State = 'assigned';
		defined("WO_FILTER_TECH_VIEW_MODE") || define("WO_FILTER_TECH_VIEW_MODE", true);
        if ($this->_filters->Distance === null)
            $this->_filters->Distance = "<= 9999999";
        /** Save workorders into CSV file **/
        if ( $request->getParam('download', false) === 'csv' ) {
            ini_set('memory_limit', '512M');
            $wo = $search->getWorkOrders(
                    $this->_login, $this->_password, self::FIELDS, $this->_filters, 0, 0, $countRows, $this->_sortStr
            );
            return $this->_formatResultsAsCsv($wo, 'assigned');
        }
        /**  End section: Save created into CSV file  **/
        $tab = $request->getParam('tab');
        $wo = $search->getWorkOrders(
                $this->_login, $this->_password, self::FIELDS, $this->_filters, ($this->_page - 1) * $this->_size, $this->_size, $countRows, $this->_sortStr,$tab
        );
        if ( $wo->success ) {
            $paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($wo->data,$countRows));
            $paginator->setCurrentPageNumber($this->_page);
            $paginator->setItemCountPerPage($this->_size);
            $this->view->paginator = $paginator;
        }
        $this->view->success    = $wo->success;
        $this->view->errors     = $wo->errors;
    }

    /**
     * workdoneAction
     *
     * Render data for 'workdone' tab
     *
     * @access public
     * @return void
     */
    public function workdoneAction() {
        $request = $this->getRequest();
        $search = new Core_Api_TechClass;
        $countRows = -1;
        $this->_filters->WO_State = 'work done';

        /** Save workorders into CSV file **/
        if ( $request->getParam('download', false) === 'csv' ) {
            ini_set('memory_limit', '512M');
            $wo = $search->getWorkOrders(
                    $this->_login, $this->_password, self::FIELDS, $this->_filters, 0, 0, $countRows, $this->_sortStr
            );
            return $this->_formatResultsAsCsv($wo, 'workdone');
        }
        /**  End section: Save created into CSV file  **/
        $wo = $search->getWorkOrders(
                $this->_login, $this->_password, self::FIELDS, $this->_filters, ($this->_page - 1) * $this->_size, $this->_size, $countRows, $this->_sortStr
        );
        if ( $wo->success ) {
            $paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($wo->data,$countRows));
            $paginator->setCurrentPageNumber($this->_page);
            $paginator->setItemCountPerPage($this->_size);
            $this->view->paginator = $paginator;
        }
        $this->view->success    = $wo->success;
        $this->view->errors     = $wo->errors;
    }

    /**
     * approvedAction
     *
     * Render data for 'approved' tab
     *
     * @access public
     * @return void
     */
    public function approvedAction() {
        $request        = $this->getRequest();
        $search = new Core_Api_TechClass;
        $countRows = -1;
        $this->_filters->WO_State = 'approved';
        /** Save workorders into CSV file **/
        if ( $request->getParam('download', false) === 'csv' ) {
            ini_set('memory_limit', '512M');
            $wo = $search->getWorkOrders(
                    $this->_login, $this->_password, self::FIELDS, $this->_filters, 0, 0, $countRows, $this->_sortStr
            );
            return $this->_formatResultsAsCsv($wo, 'approved');
        }
        /**  End section: Save created into CSV file  **/
        $wo = $search->getWorkOrders(
                $this->_login, $this->_password, self::FIELDS, $this->_filters, ($this->_page - 1) * $this->_size, $this->_size, $countRows, $this->_sortStr
        );
        if ( $wo->success ) {
            $paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($wo->data,$countRows));
            $paginator->setCurrentPageNumber($this->_page);
            $paginator->setItemCountPerPage($this->_size);
            $this->view->paginator = $paginator;
        }
        $this->view->success    = $wo->success;
        $this->view->errors     = $wo->errors;
    }

    public function netpayyearAction() {
        $request = $this->getRequest();
        $coreAPITech = new Core_Api_TechClass;
        $countRows = -1;
        $this->_filters->WO_State = 'assigned';
        defined("WO_FILTER_TECH_VIEW_MODE") || define("WO_FILTER_TECH_VIEW_MODE", true);
        if ($this->_filters->Distance === null)
            $this->_filters->Distance = "<= 9999999";
        
        /**  $netPayYear **/                
        $netPayYear=$coreAPITech->getNetPaidYearToDate($this->_login,$this->_password);
        $this->view->netPayYear = $netPayYear;
    }

    public function netpayforperiodAction() {
        $request = $this->getRequest();
        //$startDate = '2007-01-01';
        $startDate = $request->getParam('startDate', false);
        $endDate = $request->getParam('endDate', false);
        //$endDate = '2010-01-01';
        $coreAPITech = new Core_Api_TechClass;
        $countRows = -1;
        $this->_filters->WO_State = 'assigned';
        defined("WO_FILTER_TECH_VIEW_MODE") || define("WO_FILTER_TECH_VIEW_MODE", true);
        if ($this->_filters->Distance === null)
            $this->_filters->Distance = "<= 9999999";
        
        /**  netPayForPeriod **/                
        $netPay=$coreAPITech->getNetPayForPeriod($this->_login,$this->_password,$startDate,$endDate);
        //$this->view->netPayForPeriod = $netPayForPeriod;
        $this->view->netPay = $netPay;
    }

    public function paystubsperiodAction() {
        $request = $this->getRequest(); 
        $startDate = $request->getParam('startDate', false);          
        $endDate = $request->getParam('endDate', false);
        /* test */
        //$startDate='2007-01-01';
        /*end of  test */
        $ey = date('Y',strtotime($endDate));
        
        $search = new Core_Api_TechClass;
        $netPayForPeriod=$search->getNetPayForPeriod($this->_login,$this->_password,$startDate,$endDate);
        
        //$netPayYear=$search->getNetPayForYear($this->_login,$this->_password,$ey);
        $netPayYear=$search->getNetPayYTD($this->_login,$this->_password,$startDate,$endDate);
        
        $result = $search->getPayStubsForPeriod($this->_login,$this->_password,$startDate,$endDate);
        //displayStartDate
        $this->view->startDate = $request->getParam('displayStartDate', '');
        $this->view->endDate = $endDate;
        $this->view->netPayForPeriod = $netPayForPeriod;
        $this->view->netPayYear = $netPayYear;
        $this->view->result = $result;               
    }

    private function GetPeriodsToRegister($registerCreated) {
        $registerCreated = new DateTime($registerCreated);
        $y = date("Y", time());
        $d = date("d", time());
        $m = date("m", time());

        $date_array = array();

        $date_start = getdate(mktime(0, 0, 0, $m, $d, $y));
        if ($date_start['wday'] <= 4) {
            $Thuday = $date_start['wday'] + 3;
        } else {
            $Thuday = $date_start['wday'] - 4;
    }
        $date_start = mktime(0, 0, 0, $m, $d - $Thuday, $y);
        $y = date("Y", $date_start);
        $d = date("d", $date_start);
        $m = date("m", $date_start);
        $i = 0;
        $_date_start = new DateTime($y . '-' . $m . '-' . $d);
        while ($_date_start >= $registerCreated) {

            $date_start = mktime(0, 0, 0, $m, $d - 6, $y);
            $date_end = mktime(0, 0, 0, $m, $d, $y);

            $_y = date("Y", $date_start);
            $_d = date("d", $date_start);
            $_m = date("m", $date_start);
            $_ey = date("Y", $date_end);

            $_date_start = new DateTime($_y . '-' . $_m . '-' . $_d);
            if ($_date_start < $registerCreated) {
                break;
            }
            $date_array[$i][0] = $_ey;
            $date_array[$i][1] = date("m/d/Y", $date_start);
            $date_array[$i][2] = date("m/d/Y", $date_end);
            $date_array[$i][3] = date("Y-m-d", $date_start);
            $date_array[$i][4] = date("Y-m-d", $date_end);
            $i++;

            $_date = mktime(0, 0, 0, $m, $d - 7, $y);

            $y = date("Y", $_date);
            $d = date("d", $_date);
            $m = date("m", $_date);
        }
        return $date_array;
    }

    public function paystubsyearAction() {
        $request = $this->getRequest();
        $search = new Core_Api_TechClass();
        $tech = new Core_Api_TechUser;
        $authData = array('login' => $this->_login, 'password' => $this->_password);
        $tech = new Core_Api_TechUser;
        $tech->checkAuthentication($authData);

        if (!$tech->isAuthenticate()) {
            return API_Response::fail();
        }

        $registerCreated = $tech->getRegDate();
        $aYear4Week = $this->GetPeriodsToRegister($registerCreated);

        $this->view->aYear4Week = $aYear4Week;
        $this->view->UserName = $this->_login;
        $this->view->UserPassword = $this->_password;
        $this->view->netPayYear = $request->getParam('netPayYear', 0);
        $this->view->GrossPayForYear = $request->getParam('GrossPayForYear', 0);
        $this->view->i = $request->getParam('i', 0);
        $this->view->tableid = $request->getParam('tableid', 0);
    }

    function paystubsdayAction() {
		$datePay = array();
		$datePayDb = '';
	    $GrossPayForPeriod = '';
		$NetPayForPeriod = '';
		
        $this->_helper->viewRenderer->setNoRender(true);
        $params = $this->getRequest();
        $startday = $params->getParam('startday', "");
        $endday = $params->getParam('endday', "");
        $search = new Core_Api_TechClass();
		//NetPay
        $NetPayForPeriod = $search->getNetPayForPeriod($this->_login
                        , $this->_password
                        , $startday
                        , $endday);
        if(!empty($NetPayForPeriod)){
			$NetPayForPeriod = number_format($NetPayForPeriod,2,'.','');   
		}
		//GrossPay	
        $GrossPayForPeriod = $search->getGrossPayForPeriod($this->_login
                        , $this->_password
                        , $startday
                        , $endday);
		if(!empty($GrossPayForPeriod)){
			$GrossPayForPeriod = number_format($GrossPayForPeriod,2,'.','');                
		}
		

        $datePay['NetPayForPeriod'] = $NetPayForPeriod;
        $datePay['GrossPayForPeriod'] = $GrossPayForPeriod;

        $datePay['NetPayForPeriod'] = "$" . $NetPayForPeriod;
        $datePay['GrossPayForPeriod'] = "$" . $GrossPayForPeriod;
		//Date Pay
        $datePayDb = $search->getPaidDateForPeriod(
                $this->_login
                , $this->_password
                , $startday
                , $endday);
        


		if (!empty($datePayDb)){
            $datePay['dateDisplay'] = date_format(new DateTime($datePayDb),'m/d/Y');
            $datePay['dateValue'] = $datePayDb;
        }else{
			$datePay['dateDisplay'] = '';
            $datePay['dateValue'] = '';
		}
		//

		$response = $this->getResponse();
        $response->clearAllHeaders()->clearBody();
        $datePay = json_encode($datePay);
        $response->setHeader('Content-type', 'application/json');
        $response->setHeader('Content-Length', strlen($datePay), true)
                ->setBody($datePay);
    }

    /**
     * paidAction
     *
     * Render data for 'all' tab
     *
     * @access public
     * @return void
     */
    public function paidAction() {
        $request = $this->getRequest();
        $search = new Core_Api_TechClass;
        $countRows = -1;
        $this->_filters->WO_State = 'tech paid tab';

        /** Save workorders into CSV file **/
        if ( $request->getParam('download', false) === 'csv' ) {
            ini_set('memory_limit', '512M');
            $wo = $search->getWorkOrders(
                    $this->_login, $this->_password, self::FIELDS, $this->_filters, 0, 0, $countRows, $this->_sortStr
            );
            return $this->_formatResultsAsCsv($wo, 'paid');
        }
        /**  End section: Save created into CSV file  **/
        $wo = $search->getWorkOrders(
                $this->_login, $this->_password, self::FIELDS, $this->_filters, ($this->_page - 1) * $this->_size, $this->_size, $countRows, $this->_sortStr
        );
        if ( $wo->success ) {
            $paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($wo->data,$countRows));
            $paginator->setCurrentPageNumber($this->_page);
            $paginator->setItemCountPerPage($this->_size);
            $this->view->paginator = $paginator;
        }
        $this->view->success    = $wo->success;
        $this->view->errors     = $wo->errors;
    }

    /**
     * incompleteAction
     *
     * Render data for 'incomplete' tab
     *
     * @access public
     * @return void
     */
    public function incompleteAction() {
        $request        = $this->getRequest();

        $search = new Core_Api_TechClass;
        $countRows = -1;
        $this->_filters->WO_State = 'incomplete';
        /** Save workorders into CSV file **/
        if ( $request->getParam('download', false) === 'csv' ) {
            ini_set('memory_limit', '512M');
            $wo = $search->getWorkOrders(
                    $this->_login, $this->_password, self::FIELDS, $this->_filters, 0, 0, $countRows, $this->_sortStr
            );
            return $this->_formatResultsAsCsv($wo, 'incomplete');
        }
        /**  End section: Save created into CSV file  **/
        $wo = $search->getWorkOrders(
                $this->_login, $this->_password, self::FIELDS, $this->_filters, ($this->_page - 1) * $this->_size, $this->_size, $countRows, $this->_sortStr
        );
        if ( $wo->success ) {
            $paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($wo->data,$countRows));
            $paginator->setCurrentPageNumber($this->_page);
            $paginator->setItemCountPerPage($this->_size);
            $this->view->paginator = $paginator;
        }
        $this->view->success    = $wo->success;
        $this->view->errors     = $wo->errors;
    }

    /**
     * allAction
     *
     * Render data for 'all' tab
     *
     * @access public
     * @return void
     */
    public function allAction() {
        $request = $this->getRequest();

        $search = new Core_Api_TechClass;
        $countRows = -1;
        $this->_filters->WO_State = 'all active';
        $this->_filters->Tech = $this->_techId;
        $countRows = -1;
        /** Save workorders into CSV file **/
        if ( $request->getParam('download', false) === 'csv' ) {
            //ini_set('memory_limit', '512M');
            $wo = $search->getWorkOrders(
                    $this->_login, $this->_password, self::FIELDS, $this->_filters, 0, 0, $countRows, $this->_sortStr
            );
            return $this->_formatResultsAsCsv($wo, 'all');
        }
        /**  End section: Save created into CSV file  **/
        $wo = $search->getWorkOrders(
                $this->_login, $this->_password, self::FIELDS, $this->_filters, ($this->_page - 1) * $this->_size, $this->_size, $countRows, $this->_sortStr
        );
        if ( $wo->success ) {
            $paginator = new Zend_Paginator(new Core_Paginator_Adapter_Static($wo->data,$countRows));
            $paginator->setCurrentPageNumber($this->_page);
            $paginator->setItemCountPerPage($this->_size);
            $this->view->paginator = $paginator;
        }
        $this->view->success    = $wo->success;
        $this->view->errors     = $wo->errors;
    }

    private function _formatResultsAsCsv(&$wo, $tab) {
        $this->_helper->viewRenderer->setNoRender();
        $company  = $this->getRequest()->getParam('company', null);
        $response = $this->getResponse();
        $response->clearHeaders();
        $response->clearBody();
        $response->setHeader('Pragma', 'public')
                 ->setHeader('Expires', '0')
                 ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
                 ->setHeader('Cache-Control', 'private')
                 ->setHeader('Content-Type', 'text/csv')
                 ->setHeader('Content-Disposition', 'attachment; filename='.ucfirst($tab).'-'.date('mdY').'.csv', true)
                 ->setHeader('Content-Transfer-Encoding', 'binary');
        $tab = strtolower(trim($tab));

        if ( FALSE !== ($h = tmpfile()) ) {
            switch ( $tab ) {
            case 'available' :
                fputcsv($h, array('Win #','Skill Category','WO Headline','Route','Start Date','Address','City','State','Zip','Distance','Offer $','Fee Deducted'));
                break;
            case 'assigned' :
                fputcsv($h, array('Win #','Client WO#','Skill Category','WO Headline','Route','Site #','Site Name','Start Date','Address','City','State','Zip','Distance','Agreed','Fee Deducted'));
                break;
            case 'workdone':
                fputcsv($h, array('Win #','Client ID','Headline','Route','Site #','Site Name','Start Date','Address','City','State','Zip','Agreed','10% Deducted','Tech Marked Complete'));
                break;
            case 'approved' :
		fputcsv($h, array('Win #', 'Client WO#', 'WO Headline','Route', 'Site #', 'Site Name', 'Start Date', 'Start Time', 'Address', 'City', 'ST', 'Zip','Tech Marked Complete', 'Approved Date', 'Approved Net Pay $', 'Fee Deduct'));
                break;
            case 'paid' :
		fputcsv($h, array('Win #', 'Client WO#', 'WO Headline','Route', 'Site #', 'Site Name', 'Start Date', 'Start Time','Address',  'City', 'ST', 'Zip','Tech Marked Complete', 'Approved Date', 'Paid Date', 'Net Pay $', 'Fee Deduct'));
                break;
            case 'incomplete' :
                fputcsv($h, array('Win #','Client ID','Skill','Headline','Route','Site','Start Date','Address','City','State','Zip','Pay Amount','Incomplete'));
                break;
            case 'all':
                fputcsv($h, array('Win #','Client WO# ','WO Status','Skill','WO Headline','Route','Site #','Site Name','Start Date', 'Start Time','Address', 'City','State','Zip','Net Pay $','Fee Deducted'));
                break;
            default :
                throw new Zend_Exception('Incorrect tab name: "'.$tab.'"');
            }

            $displayDate      = new Dashboard_View_Helper_DisplayDate();
            $displayBid       = new Dashboard_View_Helper_DisplayBid();
            $displayMoney     = new Dashboard_View_Helper_DisplayMoney();

            if ( $wo->success && sizeof($wo->data) ) {
                foreach ( $wo->data as &$worder ) {
                    switch ( $tab ) {
                    case 'available' :
                        $this->view->wo = $worder;
                        fputcsv($h, array(
                            $worder->WIN_NUM,
                            $worder->WO_Category,
                            $worder->Headline,
                            $worder->Route,
                            $displayDate->displayDate('n/j/Y', $worder->StartDate).$displayDate->displayDate('g:i A', $worder->Time_In),
                            ( !empty($worder->Address) )? $worder->Address : '',
			                ( !empty($worder->City) )? $worder->City : '',
                            ( !empty($worder->State) )? $worder->State : '',
                            ( !empty($worder->Zipcode) )? '"'.$worder->Zipcode.'"' : '',
                            ( isset($worder->Distance) )? sprintf("%.2f", round($worder->Distance,2)) : '',
                            $displayMoney->displayMoney($worder->PayMax, "",$worder->Amount_Per),
                            (empty($worder->PcntDeductPercent) || $worder->PcntDeductPercent == 0)?"No":(($worder->PcntDeductPercent == 0.05)?"5%":"10%")//334
                        ));
                        break;
                    case 'assigned' :
                        $this->view->wo = $worder;
                        fputcsv($h, array(
                            $worder->WIN_NUM,
                            $worder->WO_ID,
                            $worder->WO_Category,
                            $worder->Headline,
                            $worder->Route,
                            $worder->SiteNumber,
                            $worder->SiteName,
                            $displayDate->displayDate('n/j/Y', $worder->StartDate).$displayDate->displayDate('g:i A', $worder->Time_In),
                            ( !empty($worder->Address) )? $worder->Address : '',
			                      ( !empty($worder->City) )? $worder->City : '',
                            ( !empty($worder->State) )? $worder->State : '',
                            ( !empty($worder->Zipcode) )? '"'.$worder->Zipcode.'"' : '',
                            ( isset($worder->Distance) )? sprintf("%.2f", round($worder->Distance,2)) : '',
                            $displayBid->displayBid($worder->Tech_Bid_Amount, $worder->Amount_Per),
                            (empty($worder->PcntDeductPercent) || $worder->PcntDeductPercent == 0)?"No":(($worder->PcntDeductPercent == 0.05)?"5%":"10%")//334
                        ));
                        break;
                    case 'workdone' :
                        fputcsv($h, array(
                            $worder->WIN_NUM,
                            $worder->WO_ID,
                            $worder->Headline,
                            $worder->Route,
                            $worder->SiteNumber,
                            $worder->SiteName,
                            $displayDate->displayDate('n/j/Y', $worder->StartDate).$displayDate->displayDate('g:i A', $worder->Time_In),
                            ( !empty($worder->Address) )? $worder->Address : '',
			                ( !empty($worder->City) )? $worder->City : '',
                            ( !empty($worder->State) )? $worder->State : '',
                            ( !empty($worder->Zipcode) )? '"'.$worder->Zipcode.'"' : '',
                            $displayMoney->displayMoney($worder->Tech_Bid_Amount, "",$worder->Amount_Per),
                            (empty($worder->PcntDeductPercent) || $worder->PcntDeductPercent == 0)?"No":(($worder->PcntDeductPercent == 0.05)?"5%":"10%"),//334
                            $displayDate->displayDate('n/j/Y', $worder->Date_Completed)
                        ));
                        break;
                    case 'approved' :
                        $this->view->wo = $worder;
                        fputcsv($h, array(
                            $worder->WIN_NUM,
                            $worder->WO_ID,
                            $worder->Headline,
                            $worder->Route,
                            $worder->SiteNumber,
                            $worder->SiteName,
                            $displayDate->displayDate('n/j/Y', $worder->StartDate),
                            $displayDate->displayDate('g:i A', $worder->StartTime),
                            ( !empty($worder->Address) )? $worder->Address : '',
			                ( !empty($worder->City) )? $worder->City : '',
                            ( !empty($worder->State) )? $worder->State : '',
                            ( !empty($worder->Zipcode) )? '"'.$worder->Zipcode.'"' : '',
                            $displayDate->displayDate('n/j/Y', $worder->Date_Completed),
                            $displayDate->displayDate('n/j/Y', $worder->DateApproved),
                            $displayMoney->displayMoney($worder->Net_Pay_Amount),
                            (empty($worder->PcntDeductPercent) || $worder->PcntDeductPercent == 0)?"No":(($worder->PcntDeductPercent == 0.05)?"5%":"10%")//334
                        ));
                        break;
                    case 'paid' :
                        $this->view->wo = $worder;
                        fputcsv($h, array(
                            $worder->WIN_NUM,
                            $worder->WO_ID,
                            $worder->Headline,
                            $worder->Route,
                            $worder->SiteNumber,
                            $worder->SiteName,
                            $displayDate->displayDate('n/j/Y', $worder->StartDate),
                            $displayDate->displayDate('g:i A', $worder->StartTime),
                            ( !empty($worder->Address) )? $worder->Address : '',
			                ( !empty($worder->City) )? $worder->City : '',
                            ( !empty($worder->State) )? $worder->State : '',
                            ( !empty($worder->Zipcode) )? '"'.$worder->Zipcode.'"' : '',
                            $displayDate->displayDate('n/j/Y', $worder->Date_Completed),
                            $displayDate->displayDate('n/j/Y', $worder->DateApproved),
                            $worder->Status === "in accounting" ? "In Process" : $displayDate->displayDate('n/j/Y', $worder->DatePaid),
                            $displayMoney->displayMoney($worder->Net_Pay_Amount),
                            (empty($worder->PcntDeductPercent) || $worder->PcntDeductPercent == 0)?"No":(($worder->PcntDeductPercent == 0.05)?"5%":"10%")//334
                        ));
                        break;
                    case 'incomplete':
                        $this->view->wo = $worder;
                        fputcsv($h, array(
                            $worder->WIN_NUM,
                            $worder->WO_ID,
                            $worder->WO_Category,
                            $worder->Headline,
                            $worder->Route,
                            $worder->SiteNumber,
                            $worder->SiteName,
                            $displayDate->displayDate('n/j/Y', $worder->StartDate).$displayDate->displayDate('g:i A', $worder->Time_In),
                            ( !empty($worder->Address) )? $worder->Address : '',
			                ( !empty($worder->City) )? $worder->City : '',
                            ( !empty($worder->State) )? $worder->State : '',
                            ( !empty($worder->Zipcode) )? '"'.$worder->Zipcode.'"' : '',
                            $displayMoney->displayMoney($worder->Net_Pay_Amount),
                            $displayDate->displayDate('n/j/Y', $worder->DateIncomplete)
                        ));
                        break;
                    case 'all' :
                        $this->view->wo = $worder;
                        fputcsv($h, array(
                            $worder->WIN_NUM,
                            $worder->WO_ID,
                            $worder->Status,
                            $worder->WO_Category,
                            $worder->Headline,
                            $worder->Route,
                            $worder->SiteNumber,
                            $worder->SiteName,
                            $displayDate->displayDate('n/j/Y', $worder->StartDate),
                            $displayDate->displayDate('g:i A', $worder->StartTime),
                            ( !empty($worder->Address) )? $worder->Address : '',
			                      ( !empty($worder->City) )? $worder->City : '',
                            ( !empty($worder->State) )? $worder->State : '',
                            ( !empty($worder->Zipcode) )? '"'.$worder->Zipcode.'"' : '',
                            $displayMoney->displayMoney($worder->Net_Pay_Amount),
                            (empty($worder->PcntDeductPercent) || $worder->PcntDeductPercent == 0)?"No":(($worder->PcntDeductPercent == 0.05)?"5%":"10%")//334
                        ));
                        break;
                    }
                }
            }
            $csv = '';
            fflush($h);
            fseek($h, 0, SEEK_SET);
            while( !feof($h) ) {
                $csv .= fread($h, 4096);
            }
        } else {
            $csv = 'Sorry, but error occured.';
        }
        $response->setHeader('Content-Length', strlen($csv), true)
                 ->setBody($csv);
    }

    private function pullApplicantsInfo($wos){
        if (empty($wos))
            return;
        $ids = array();
        foreach ($wos as $wo)
            $ids[] = $wo->WIN_NUM;
        if(!empty($ids)){
            $db = Core_Database::getInstance();
            $s = $db->select()
                    ->from(Core_Database::TABLE_WORK_ORDER_BIDS, array('WorkOrderID'))
                    ->where(Core_Database::TABLE_WORK_ORDER_BIDS.'.WorkOrderID IN (?)', $ids)
                    ->where(Core_Database::TABLE_WORK_ORDER_BIDS.'.TechID = ?', $this->_techId)
                    ->group(Core_Database::TABLE_WORK_ORDER_BIDS.'.WorkOrderID');
            $res = $db->fetchAll($s);
            $bidsCount = array();
            foreach ($res as $r)
                $bidsCount[$r['WorkOrderID']] = 1;
        }

        $this->view->bidsCount = (empty($bidsCount)) ? null : $bidsCount;
    }
    private function pullwithdrawApplicantsInfo($wos){
        if (empty($wos))
            return;
        $ids = array();
        foreach ($wos as $wo)
            $ids[] = $wo->WIN_NUM;
        if(!empty($ids)){
            $db = Core_Database::getInstance();
            $s = $db->select()
                    ->from("work_orders_techwithdrawbids", array('WorkOrderID'))
                    ->where("work_orders_techwithdrawbids".'.WorkOrderID IN (?)', $ids)
                    ->where("work_orders_techwithdrawbids".'.TechID = ?', $this->_techId)
                    ->group("work_orders_techwithdrawbids".'.WorkOrderID');
            $res = $db->fetchAll($s);
            $bidswithdrawCount = array();
            foreach ($res as $r)
                $bidswithdrawCount[$r['WorkOrderID']] = 1;
        }

        $this->view->bidswithdrawCount = (empty($bidswithdrawCount)) ? null : $bidswithdrawCount;
    }
    
    function scheduleinweekAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);   
        
        $params = $this->getRequest();                
        $firstdayofweek= $params->getParam('firstdayofweek',"");
        
        $asssignedArray = array();
        $appliedArray = array();
        $resultArray = array();
        $asssignedArray = $this->GetAsssignedInWeekArray($firstdayofweek);
        $appliedArray = $this->GetAppliedInWeekArray($firstdayofweek,$asssignedArray);

        $tempappliedArray = array();
        for($i = 0;$i < count($appliedArray);$i++)
        {
            $keep  = true;
            for($j = 0;$j < count($asssignedArray);$j++)
            {
                if($appliedArray[$i]["id"] == $asssignedArray[$j]['id']
                && strtolower($appliedArray[$i]["start"]) == strtolower($asssignedArray[$j]['start'])
                && strtolower($appliedArray[$i]["end"]) == strtolower($asssignedArray[$j]['end'])
                && strtolower($appliedArray[$i]["workstatus"]) == 'applied'
                && strtolower($asssignedArray[$i]["workstatus"]) == 'assigned')
                {
                    $keep = false;
                    break;
}
            }
            if($keep)
            {
                $tempappliedArray[] = $appliedArray[$i];
            }
        }
        $resultArray = array_merge((array)$tempappliedArray,(array)$asssignedArray);
        
        $this->_helper->viewRenderer->setNoRender(true);        
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $response->setHeader('Content-Length', strlen(100000), true);
        $response->setBody(json_encode($resultArray));        
    }
    function convertDateTimeSchedule(&$startDate,&$startTime,&$endDate,&$endTime)
    {
        if(empty($endDate))
        {
            $endDate = $startDate;
        }
        if(!empty($startDate))
            $startDate = date_format(new DateTime($startDate),"Y-m-d");
    
        if(!empty($endDate))
            $endDate = date_format(new DateTime($endDate),"Y-m-d");   
        if(empty($startTime) && empty($endTime)) 
        {
            $startTime="08:00:00"; 
            $endTime="17:00:00"; 
        }
        else if(!empty($startTime) && empty($endTime) && !empty($endDate)) 
        {
            $time = array();
            $_time = $startTime;
            $StartTime = $startTime;
            $isA = false;
            if(strrpos(strtolower($_time),'am') > 0)
            {
                $StartTime = str_replace(' am', "" ,strtolower($_time));
                $isA = true;
            }  
            $time = explode(":", $StartTime);
            if(strrpos(strtolower($_time),'pm')  > 0)
            {
                $StartTime = str_replace(' pm', "" ,strtolower($_time));
                $time = explode(":", $StartTime);
                if((int)$time[0] < 12)
                $time[0] = $time[0] + 12;
                $isA = true;
            }     
            $time[0] = (int)$time[0];
            $time[1] = (int)$time[1];
            if(count($time) == 2) $time[2] = 0;
            $date = explode("-", $endDate);
            $year = $date[0];
            $mon = $date[1];
            $day = $date[2];
            $date  = mktime($time[0] + 1, $time[1], $time[2], $mon  , $day, $year);
            if($isA)
                $StartTime = date( "h:i A",$date);
            else
                $StartTime = date( "H:i:s",$date);
            $endDate = date( "Y-m-d",$date);
            $endTime = $StartTime;
        }
    }
    function GetAsssignedInWeekArray($firstdayofweek)
    {
        $arrReturn = array();
        $search = new Core_Api_TechClass();
        
        
        if(!empty($firstdayofweek))
        {
            //--- filter
            $this->_filters->WO_State = 'assigned';
            defined("WO_FILTER_TECH_VIEW_MODE") || define("WO_FILTER_TECH_VIEW_MODE", true);
            if ($this->_filters->Distance === null) {
                $this->_filters->Distance = "<= 9999999";
            }
            ini_set('memory_limit', '512M');
            //--- $api
            $apiWOCate = new API_WOCategory();            
            $apiTechClass = new Core_Api_TechClass();
            //--- wos
            $fieldlist="WIN_NUM,WO_ID,Headline,SiteName,City,State,Zipcode,Amount_Per,PayMax,Tech_ID,StartDate,EndDate,StartTime,EndTime,WO_Category";            
            $wo = $apiTechClass->getWorkOrders(
                $this->_login,
                $this->_password,
                $fieldlist,
                $this->_filters,
                0,
                0
            );
            if ( $wo->success && sizeof($wo->data) ) {
                $date1 = new DateTime($firstdayofweek);
                $date2 = new DateTime($firstdayofweek);
                $date2->add(new DateInterval('P6D'));
                $datetime1 = $date1->format('Y-m-d');
                $datetime2 = $date2->format('Y-m-d');
                $rowid=0;
                foreach ( $wo->data as &$worder ) {
                    $win = $worder->WIN_NUM;
                    $startDate = $worder->StartDate;
                    $endDate = $worder->EndDate;
                    $startTime = $worder->StartTime;
                    $endTime = $worder->EndTime;
                    
                                        
                    $this->convertDateTimeSchedule($startDate,
                            $startTime,
                            $endDate,
                            $endTime);
                    $startTimeTitle = $startTime;
                    $endTimeTitle = $endTime;
                    
                    if( !empty($startDate) && !empty($endDate) &&
                    ($startDate <= $endDate) && (($startDate >= $datetime1 && $startDate <= $datetime2)||
                        ($endDate >= $datetime1 && $endDate <= $datetime2) ||
                        ($startDate < $datetime1 && $endDate > $datetime2))
                       )
                    {                        
                        $categoryName = $worder->WO_Category;                        
                        $ClientWONum = $worder->WO_ID;
                        $headline = $worder->Headline;
                        $siteName = $worder->SiteName;
                        $cityStateZip = $worder->City.', '.$worder->State.' '.$worder->Zipcode;
                        $amountPer = $worder->Amount_Per;
                        $payMax = $worder->PayMax;
                        $startDate = date_format(new DateTime($startDate),"Y-m-d");
                        $endDate = date_format(new DateTime($endDate),"Y-m-d");
                       
                        //$title = "WIN#: $win";
                        $title = "";
                        $title .= "Client WO #: $ClientWONum";
                        $title .= "<br/>$startTimeTitle to $endTimeTitle";
                        $title .= "<br/>$categoryName - $headline";
                        $title .= "<br/>$siteName";
                        $title .= "<br/>$cityStateZip";
                        $title .= "<br/>Pay: $payMax PER $amountPer";

                        $workstatus = "assigned";
                        
                        $startDateObj = new DateTime($startDate);
                        $syear = $startDateObj->format("Y");
                        $smonth = $startDateObj->format("m");
                        $sday = $startDateObj->format("d");
                        $endDateObj = new DateTime($endDate);
                        $interval = $startDateObj->diff($endDateObj);
                        $sDateObj = new DateTime($startDate);
                        for($i=0;$i<=$interval->days;$i++)
                        {
                            $sDateObj->setDate($syear,$smonth,$sday+$i);
                            $start =new DateTime($sDateObj->format("Y-m-d")." ".$startTime);
                            $end = new DateTime($sDateObj->format("Y-m-d")." ".$endTime);
                            $arrReturn[$rowid]=array("id"=>$win,
                                "start"=>$start->format("Y-m-d\TH:i:s"),
                                "end"=>$end->format("Y-m-d\TH:i:s"),
                                "title"=>$title,
                                "workstatus"=>$workstatus,          
                            );                 
                            $rowid++;             
                        }                            
                    }//end of if
                }//end of for
            }            
        }
        return $arrReturn;
    }
    
    function GetAvailableInWeekArray($firstdayofweek)
    {
        $arrReturn = array();        
        if(!empty($firstdayofweek))
        {
            $date1 = new DateTime($firstdayofweek);
            $date2 = new DateTime($firstdayofweek);
            $date2->add(new DateInterval('P6D'));
            $datetime1 = $date1->format('Y-m-d');
            $datetime2 = $date2->format('Y-m-d');
            
            //--- filter
            $this->_filters->Tech        = NULL;
            $this->_filters->WO_State = "published";
            if ($this->_filters->Distance === null)
                $this->_filters->Distance = "<= 9999999";            
            //--- $api
            $apiWOCate = new API_WOCategory();            
            $apiTechClass = new Core_Api_TechClass();
            //--- wos
            $fieldlist="WIN_NUM,WO_ID,Headline,SiteName,City,State,Zipcode,Amount_Per,PayMax,Tech_ID,StartDate,EndDate,StartTime,EndTime";
            $wo = $apiTechClass->getWorkOrders(
                $this->_login,
                $this->_password,
                $fieldlist,
                $this->_filters,
                0,
                0
            );
            if ( $wo->success && sizeof($wo->data) ) {
                $rowid=0;
                foreach ( $wo->data as $worder ) {
                    $win = $worder->WIN_NUM;
                    $startDate = $worder->StartDate;
                    $endDate = $worder->EndDate;
                    $startTime = $worder->StartTime;
                    $endTime = $worder->EndTime;

                    if(!empty($startDate))
                       $startDate = date_format(new DateTime($startDate),"Y-m-d");
                                        
                    if(!empty($endDate))
                       $endDate = date_format(new DateTime($endDate),"Y-m-d");
                       
                    if( !empty($startDate) && !empty($endDate) &&
                    ($startDate <= $endDate) && (($startDate >= $datetime1 && $startDate <= $datetime2)||
                        ($endDate >= $datetime1 && $endDate <= $datetime2) ||
                        ($startDate < $datetime1 && $endDate > $datetime2))
                       )
                    {                        

                        $categoryName = $worder->WO_Category;                        
                        $ClientWONum = $worder->WO_ID;
                        $headline = $worder->Headline;
                        $siteName = $worder->SiteName;
                        $cityStateZip = $worder->City.', '.$worder->State.' '.$worder->Zipcode;
                        $amountPer = $worder->Amount_Per;
                        $payMax = $worder->PayMax;
                        $startDate = date_format(new DateTime($startDate),"Y-m-d");
                        $endDate = date_format(new DateTime($endDate),"Y-m-d");
                        
                        if(empty($startTime)) 
                        {
                            $startTime="08:00:00";   
                        }
                        if(empty($endTime)) 
                        {
                            $endTime="17:00:00";   
                        }                        
                        
                        $title = "WIN#: $win";
                        $title .= "<br/>Client WO #: $ClientWONum";
                        $title .= "<br/>$categoryName - $headline";
                        $title .= "<br/>$siteName";
                        $title .= "<br/>$cityStateZip";
                        $title .= "<br/>Pay: $payMax PER $amountPer";

                        $workstatus = "available";
                        
                        $orinalStartDateObj = new DateTime($startDate);
                        $orinalEndDateObj = new DateTime($endDate);
                        
                        if($startDate > $datetime1 )
                        {
                            $startDateObj = new DateTime($startDate);
                            $sDateObj = new DateTime($startDate);
                        }
                        else
                        {
                            $startDateObj = new DateTime($datetime1);
                            $sDateObj = new DateTime($datetime1);        
                        }
                            
                        
                        $syear = $startDateObj->format("Y");
                        $smonth = $startDateObj->format("m");
                        $sday = $startDateObj->format("d");
                        
                        if($endDate > $datetime2)
                            $endDateObj = new DateTime($datetime2);
                        else
                            $endDateObj = new DateTime($endDate);     
                        
                        $interval = $startDateObj->diff($endDateObj);
                        
                        for($i=0;$i<=$interval->days;$i++)
                        {
                            $sTime = $startTime;
                            $eTime = $endTime;
                            
                            $sDateObj->setDate($syear,$smonth,$sday+$i);
                            
                            if($sDateObj > $orinalStartDateObj) $sTime="08:00:00";
                            if($sDateObj < $orinalEndDateObj) $eTime="17:00:00";
                            
                            $arrReturn[$rowid]=array("id"=>$win,
                                "start"=>$sDateObj->format("Y-m-d")."T".$sTime,
                                "end"=>$sDateObj->format("Y-m-d")."T".$eTime,
                                "title"=>$title,
                                "workstatus"=>$workstatus,          
                            );                          
                            $rowid++;  
                        }//end of for;                            
                    }//end of if
                }//end of for
            }            
        }
        return $arrReturn;
    }
    
    function GetAppliedInWeekArray($firstdayofweek,$assignArray = array())
    {
        $assignWinArray = array();
        if(!empty($assignArray))
        {
            foreach($assignArray as $item)
            {
                $assignWinArray[] = $item["id"];
            }
        }
        $arrReturn=array();
        $apiTechClass = new Core_Api_TechClass();
        $bids = $apiTechClass->getBids($this->_techId);
        if(empty($bids)) return $arrReturn;
        
        $wosList = "";
        foreach ($bids as $bid) {
            $wosList .= empty($wosList)? $bid['WorkOrderID']:','.$bid['WorkOrderID'];
        }
        
        $wos=null;
        if(!empty($wosList))
            $wos=$apiTechClass->getWos($wosList);
                    
        if(!empty($wos)){
            $date1 = new DateTime($firstdayofweek);
            $date2 = new DateTime($firstdayofweek);
            $date2->add(new DateInterval('P6D'));
            $datetime1 = $date1->format('Y-m-d');
            $datetime2 = $date2->format('Y-m-d');
            
            $rowid=0;
            
            foreach ( $wos as $wo ) {
                $win = $wo['WIN_NUM'];
                if(!empty($assignArray)) 
                {
                    if(in_array($win, $assignWinArray))
                    {
                        continue;
                    }
                }
                $startDate = $wo['StartDate'];
                $endDate = $wo['EndDate'];
                $startTime = $wo['StartTime'];                    
                $endTime = $wo['EndTime'];  
                $this->convertDateTimeSchedule($startDate,
                            $startTime,
                            $endDate,
                            $endTime);
                $startTimeTitle = $startTime;
                $endTimeTitle = $endTime;
                
                if(!empty($startDate))
                    $startDate = date_format(new DateTime($startDate),"Y-m-d");
                    
                if(!empty($endDate))
                    $endDate = date_format(new DateTime($endDate),"Y-m-d");

                if( !empty($startDate) && !empty($endDate) &&
                    ($startDate <= $endDate) && (($startDate >= $datetime1 && $startDate <= $datetime2)||
                        ($endDate >= $datetime1 && $endDate <= $datetime2) ||
                        ($startDate < $datetime1 && $endDate > $datetime2))
                       )
                {                        
                 
                    $categoryName = $wo['WO_Category'];                       
                    $ClientWONum = $wo['WO_ID'];
                    $headline = $wo['Headline'];
                    $siteName = $wo['SiteName'];
                    $cityStateZip = $wo['City'].', '.$wo['State'].' '.$wo['Zipcode'];
                    $amountPer = $wo['Amount_Per'];
                    $payMax = $wo['PayMax'];
                    $startDate = date_format(new DateTime($startDate),"Y-m-d");
                    $endDate = date_format(new DateTime($endDate),"Y-m-d");
                   
                    
                    //$title = "WIN#: $win";
                    $title = "";
                    $title .= "<br/>Client WO #: $ClientWONum";
                    $title .= "<br/>$startTimeTitle to $endTimeTitle";
                    $title .= "<br/>$categoryName - $headline";
                    $title .= "<br/>$siteName";
                    $title .= "<br/>$cityStateZip";
                    $title .= "<br/>Pay: $payMax PER $amountPer";

                    $workstatus = "applied";
                    
                    $startDateObj = new DateTime($startDate);
                    $syear = $startDateObj->format("Y");
                    $smonth = $startDateObj->format("m");
                    $sday = $startDateObj->format("d");
                    $endDateObj = new DateTime($endDate);
                    $interval = $startDateObj->diff($endDateObj);
                    $sDateObj = new DateTime($startDate);
                    for($i=0;$i<=$interval->days;$i++)
                    {
                        $sDateObj->setDate($syear,$smonth,$sday+$i);
                        $start =new DateTime($sDateObj->format("Y-m-d")." ".$startTime);
                        $end = new DateTime($sDateObj->format("Y-m-d")." ".$endTime);
                        $arrReturn[$rowid]=array("id"=>$win,
                            "start"=>$start->format("Y-m-d\TH:i:s"),
                            "end"=>$end->format("Y-m-d\TH:i:s"),
                            "title"=>$title,
                            "workstatus"=>$workstatus,          
                        );                            
                        $rowid++;  

                    }                            
                }//end of if
            }//end of for
        }//end of if
        return $arrReturn;
    }//end of function
    
    function assignedworkicsAction()
    {
        $arrReturn = array();        
        //--- filter
        $this->_filters->WO_State = 'assigned';
        defined("WO_FILTER_TECH_VIEW_MODE") || define("WO_FILTER_TECH_VIEW_MODE", true);
        if ($this->_filters->Distance === null) {
            $this->_filters->Distance = "<= 9999999";
        }
        ini_set('memory_limit', '512M');
        //--- $api
        $apiWOCate = new API_WOCategory();            
        $apiTechClass = new Core_Api_TechClass();
        //--- wos
        $fieldlist="WIN_NUM";
        $wo = $apiTechClass->getWorkOrders(
            $this->_login,
            $this->_password,
            $fieldlist,
            $this->_filters,
            0,
            0
        );
        if ( $wo->success && sizeof($wo->data) ) 
        {
            $irow = 0;
            foreach ( $wo->data as &$worder ) {
                $win = $worder->WIN_NUM;
                $arrReturn[$irow]=array("WINNUM"=>$win,"workstatus"=>"assigned");
                $irow++;
            }
        }

        $this->_helper->viewRenderer->setNoRender(true);        
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $response->setHeader('Content-Length', strlen(100000), true);
        $response->setBody(json_encode($arrReturn));
    }
    
    function availableworkicsAction()
    {
        $arrReturn = array();        
        //--- filter
        $this->_filters->Tech        = NULL;
        $this->_filters->WO_State = "published";
        if ($this->_filters->Distance === null)
            $this->_filters->Distance = "<= 9999999";
        //--- $api
        $apiTechClass = new Core_Api_TechClass();
        //--- wos
        $fieldlist="WIN_NUM";
        $wo = $apiTechClass->getWorkOrders(
            $this->_login,
            $this->_password,
            $fieldlist,
            $this->_filters,
            0,
            0
        );
        if ( $wo->success && sizeof($wo->data) ) 
        {
            $irow = 0;
            foreach ( $wo->data as &$worder ) {
                $win = $worder->WIN_NUM;
                $arrReturn[$irow]=array("WINNUM"=>$win,"workstatus"=>"available");
                $irow++;
            }
        }

        $this->_helper->viewRenderer->setNoRender(true);        
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $response->setHeader('Content-Length', strlen(100000), true);
        $response->setBody(json_encode($arrReturn));
    }    
    
    function appliedworkicsAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);      
        $params = $this->getRequest();                
        $assign= $params->getParam('assign',"");
        $assignArray = explode(",", $assign);
        $apiTechClass = new Core_Api_TechClass();
        $bids = $apiTechClass->getBids($this->_techId);
        if(empty($bids)) return $arrReturn;
        
        $arrReturn=array();
        $irow = 0;
        foreach ($bids as $bid) {
            $win = $bid['WorkOrderID'];
            if(!empty($assignArray))
            {
                if(in_array($win."-assigned", $assignArray))  continue;
            }    
            $arrReturn[$irow]=array("WINNUM"=>$win,"workstatus"=>"applied");
            $irow++;
        }
               
        //--- response
        $response = $this->getResponse();
        $response->clearHeaders()->clearBody();
        $response->setHeader('Content-Length', strlen(100000), true);
        $response->setBody(json_encode($arrReturn));        
    }
}
