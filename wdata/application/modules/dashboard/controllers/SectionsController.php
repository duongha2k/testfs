<?php
class Dashboard_SectionsController extends Core_Controller_Action
{

	/**
     * _wo
     *
     * @var API_Response
     * @access private
     */
    private $_wo;

    public function init()
    {
		parent::init();

        $benchmark = Core_Benchmark::getInstance('Start benchmark on sections pop-ups');
        //  This is constroller can be used for AJAX only!
        $request = $this->getRequest();
        if ( !$request->isXmlHttpRequest() ) {
            $this->_redirect('/');
        }

        //  Check authentication
        $auth = new Zend_Session_Namespace('Auth_User');
        $this->_login = $auth->login;
        $this->_password = $auth->password;
        session_write_close();

        if ( !$this->_login || !$this->_password ) {
            throw new Core_Auth_Exception('Authentication required');
        }
        //  Check authentication end block

        $request    = $this->getRequest();
        $win        = $request->getParam('win', false);
        $companyId  = $request->getParam('companyId', false);
		$this->_companyId = $companyId;

        if ( !$win || !$companyId ) {
            $this->view->success = true;
            if ($request->getParam('action') == 'quickassigntool') return;
            $this->view->success = false;
            $this->view->errors  = "Sorry, but Work Order ID or Company ID are incorrect! Request skiped.";
            return;
        }

        $cacheKey = Core_Cache_Manager::makeKey($this->_login, 'sections', 'win', $win); //  1 key for whole controller!
        $cacheKeyCategories = Core_Cache_Manager::makeKey('wo_categiries', 'sections');
        $cache = $this->getInvokeArg('bootstrap')->getResource('Cache');

        $benchmark->point('Initialization complete');

        if ( $request->getParam('action') == 'final' || false === ($this->_wo = $cache->load($cacheKey)) ) {
            $benchmark->point('Load WO #'.$win.' from Caspio');
            $search = new Core_Api_Class;

            $this->_wo = $search->getWorkOrder($this->_login.'|pmContext='.$companyId, $this->_password, $win,true);
            
            $benchmark->point('WO #'.$win.' loaded');
            if ( $this->_wo->success ) {   //  save if operation success

                //  load Call Type
                $result = $search->getCallTypes();
                if ( $result->success && array_key_exists($this->_wo->data[0]->Type_ID, $result->data) ) {
                    $this->_wo->data[0]->Type_ID = $result->data[$this->_wo->data[0]->Type_ID];
                } else {
                    $this->_wo->data[0]->Type_ID = NULL;
                }

                //  load Category
                if ( $this->_wo->data[0]->WO_Category_ID ) {
                    if ( FALSE === ($categories = $cache->load($cacheKeyCategories)) ) {
                        $categories = $search->getWOCategories($this->_login.'|pmContext='.$companyId, $this->_password);
                        if ( $categories->success ) {
                            $cache->save($categories, $cacheKeyCategories, array('filter'), LIFETIME_15MIN);
                        }
                    }
                    if ( $categories->success ) {
                        foreach ( $categories->data as &$c ) {
                            if ( $c->Category_ID == $this->_wo->data[0]->WO_Category_ID ) {
                                $this->_wo->data[0]->WO_Category_ID = $c->Category;
                                break;
                            }
                        }
                    }
                }

                $cache->save($this->_wo, $cacheKey, array('wokr_orders'), LIFETIME_15MIN);
            }
        } else {
            $benchmark->point('WO #'.$win.' was loaded from cache');
        }

        $benchmark->point('View is rendering...');
//var_dump($this->_wo);exit;
        $Wosapi = new  Core_Api_WosClass();
        $this->view->EnableColspanTechPay = $Wosapi->enableExpenseReporting($win);
        
        if($this->view->EnableColspanTechPay)
        {
        $this->view->WosTurnedOnBCatsinfo = $Wosapi->getWorkOrderTurnedOnBCats_insertFromProjectIfNotExists($win);
        }
        
        $this->view->wo         = $this->_wo->data[0];
        
        $this->view->success    = $this->_wo->success;
        $this->view->errors     = $this->_wo->errors;
        $this->view->win        = $win;
        $this->view->companyId  = $companyId;
        $this->view->woVisit    = $Wosapi->getBaseWOVisitInfoDisplayrules($win);//14005            
    }

    public function section1Action()
    {
    }

    public function section2Action()
    {
    }

    public function section3Action()
    {
    }

    public function section4Action()
    {
    }

    public function section5Action()
    {
    }

    public function section6Action()
    {
    }

    public function section7Action()
    {
    }

    public function finalAction()
    {
    	$wo = $this->view->wo;
        $Wosapi = new  Core_Api_WosClass();
        //if(empty($this->view->wo->Qty_Visits)){
		$visits = $Wosapi->getWOVisitCount($wo->WIN_NUM);
		$wo->Qty_Visits = $visits;
        //}

        $payAmount = "";

        if($wo->Amount_Per == "Visit") {
			$payAmount = $wo->PayMax * $wo->Qty_Visits;
		}
		elseif ($wo->Amount_Per == "Device"){
			$payAmount = $wo->baseTechPay * $wo->Qty_Devices;
		}else{
			$payAmount =  $wo->baseTechPay;
		}

		$Total_ApReThFS = $Wosapi->getTotal_ApReThFS($wo->WIN_NUM);

		$this->view->payAmount = $Total_ApReThFS + $payAmount;
    }

    public function finalReadAction()
    {
        $search = new Core_Api_Class();
        $request    = $this->getRequest();
        $win        = $request->getParam('win', false);
        if(!empty($win))
        {
           $winAddtionFields = $search->getWorkOrderAdditionalFields($win);
           if(!empty($winAddtionFields))
           {
                $this->view->AuditNeeded=$winAddtionFields['AuditNeeded'];
                $this->view->AuditComplete=$winAddtionFields['AuditComplete'];
           }
                $this->view->isdisable = true;
         if(empty($this->view->wo->Qty_Visits)){
         	$Wosapi = new Core_Api_WosClass();
        	$this->view->wo->Qty_Visits = $Wosapi->getNumOfVisits($win);
        }
           }

    }

    public function startTimeAction()
    {
    }

    public function quickassigntoolAction()
    {
    }

    public function documentsAction()
    {
        $files = new Core_Files();
        $select = $files->select();
        $select->where('WIN_NUM = ?', $this->view->win)
               ->where('type = ?', Core_Files::TECH_FILE);
        $filesList = $files->fetchAll($select);
        $this->view->files = $filesList;

        $this->view->core = new Core_Api_Class();
        $this->view->user_login = $this->_login;
        $this->view->user_password = $this->_password;
    }

    public function techDocumentsAction()
    {
        $request    = $this->getRequest();
        $parames    = $request->getParams();
        if($parames["por"] == "yes"){
            $this->_helper->viewRenderer->setNoRender(true);
            $WosClass = new Core_Api_WosClass();
            $WosClass->updatePaperWork($parames["win"],$parames["Paperwork_Received"],$parames["Incomplete_Paperwork"]);
            $this->_helper->json(array("success" => true));
        }else{
        $files = new Core_Files();
        $select = $files->select();
        $select->where('WIN_NUM = ?', $this->view->win)
               ->where('type = ?', Core_Files::TECH_FILE);
        $filesList = $files->fetchAll($select);
        $this->view->files = $filesList;

        $this->view->core = new Core_Api_Class();
        $this->view->user_login = $this->_login;
        $this->view->user_password = $this->_password;
            $search = new Core_Api_Class;
            $wo = $search->getWorkOrdersWithWinNum($parames["win"],"","");
            $wo = (object)$wo[0];
            $this->view->wov2 = $wo;
    }
    }

    public function techCommentsAction()
    {
		$search = new Core_Api_Class;
		$wo = $search->getWorkOrder($this->_login . '|pmContext=' . $this->_companyId, $this->_password, $this->view->win);
		if ($wo->success)
			$this->view->TechComments = $wo->data[0]->TechComments;
    }

    public function incompleteReasonAction()
    {
    }
	
	public function dashContentLeftAction() {
		$this->view->companyId  = $this->_companyId;
		$authData = array('login'=>$this->_login . '|pmContext=' . $this->_companyId, 'password'=>$this->_password);

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);
		
		if (!$user->isAuthenticate()) return false;

		$this->view->installManager = $user->getUserType();
		$this->view->installManager = $this->installManager == "Install Desk" || $this->installManager == "Manager";
		$search = new Core_Api_Class();
		$this->view->projects = $search->getAllProjects($authData['login'],$authData['password'], NULL, array('ProjectUserAllowedCreateWO' => true));
	}

	public function dashContentRightAction(){
	    $request    = $this->getRequest();
		$this->view->showrightpanel = $request->getParam('showrightpanel', false);

	        $this->view->companyId  = $this->_companyId;
		$authData = array('login'=>$this->_login . '|pmContext=' . $this->_companyId, 'password'=>$this->_password);

		$user = new Core_Api_User();
		$user->checkAuthentication($authData);
		
		if (!$user->isAuthenticate()) return false;

		$this->view->installManager = $user->getUserType();
		$this->view->installManager = $this->installManager == "Install Desk" || $this->installManager == "Manager";
		
		$fields = 'WIN_NUM, Tech_FName, Tech_LName, lastModDate, WO_ID, Tech_ID, Description';
		$blank = "";
		$page = "1";
		$numRecords = "5";
		$sort = "lastModDate DESC";
		$filter = new API_WorkOrderFilter;
		$countRows = "";
		$search = new Core_Api_Class();
		
		$this->view->wopanels = $search->getWorkOrders(
            $authData['login'],
            $authData['password'],
            $fields,
            $blank,
            $blank,
            $blank,
            $filter,
            $page,
            $numRecords,
            $countRows,
            $sort
        );
		
		

		$recent = $search->getMyRecentTechIds($authData['login'], $authData['password'], $this->_companyId);

		$i = 0;
		$this->view->techs = array();
		foreach($recent as $key=>$value){
			if($i==5) break;
			$this->view->techs[] = $search->techLookup($authData['login'], $authData['password'],$value);
		
			++$i;
		}

		$installManager = FALSE;
		if ($this->view->FLSManager = ($_SESSION['UserType'] == 'Install Desk' || $_SESSION['UserType']=='Manager')){$installManager = TRUE;};
	}
	
    public function bcatDocumentsAction()
    {
        $apiWosClass = new Core_Api_WosClass();
        $request    = $this->getRequest();
        $wobcatid        = $request->getParam('wobcatid', false);
        
        $DocumentList =  $apiWosClass->getDocumentListByWOBCatID($wobcatid);
        $this->view->files = $DocumentList;
}
}

