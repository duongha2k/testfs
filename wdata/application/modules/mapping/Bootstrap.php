<?php
/*
 * Current class initialize Dashboard module
 */
class Mapping_Bootstrap extends Zend_Application_Module_Bootstrap
{
    /**
     * _initView
     *
     * Initialize View
     *
     * @access protected
     * @return Zend_View
     */
    protected function _initView()
    {
        $viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();

        $view = new Zend_View();
        $view->setHelperPath(DASHBOARD_MODULE_PATH.'views/helpers/', 'Dashboard_View_Helper');

        $view->strictVars(true);
        
        $viewRenderer->setView($view);
        Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);

        return $view;
    }
    /**
     * _initAuthentication
     *
     * @access protected
     * @return void
     */
    protected function _initAuth()
    {
        //  Name from headerStartSession.php
        session_name('fsTemplate');

        $auth = new Zend_Session_Namespace('Auth_User');

        if ( empty($_SESSION['UserName']) || $auth->login !== $_SESSION['UserName'] || strtolower(trim($_SESSION['loggedIn'])) !== 'yes' ) {
            $auth->login    = NULL;
            $auth->password = NULL;
            $auth->loggedInAs = NULL;
        }

        if ( strtolower(trim($_SESSION['loggedIn'])) === 'yes') {
            $auth->login = trim($_SESSION['UserName']);
            $auth->loggedInAs = $_SESSION['loggedInAs'];

            if (empty($auth->password)) {
                if ($_SESSION['loggedInAs'] == 'client') {
                    $result = Core_Caspio::caspioSelectAdv(TABLE_CLIENT_LIST, "Password", "UserName= '".$auth->login."'", "", false, "`", "|");
                }elseif ($_SESSION['loggedInAs'] == 'tech') {
                    $result = Core_Caspio::caspioSelectAdv(TABLE_MASTER_LIST, "Password", "UserName= '".$auth->login."'", "", false, "`", "|");
                }
                if ( !empty($result[0][0]) ) {
                    $auth->password = $result[0][0];
                }
            }
        }
    }
}

