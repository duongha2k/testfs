<?php

/**
 * Mapping service
 * 
 * @author Alexander Cheshchevik
 *
 */
class Mapping_IndexController extends Core_Mapping_Controller_Action
{
    const WO_FIELDS = "WIN_NUM,Deactivated,Latitude,Longitude,Tech_ID,TechMarkedComplete,WO_ID,StartDate,Project_ID,SiteName,Address,City,State,Zipcode,Tech_Bid_Amount,ShowTechs,StartTime,Amount_Per,ShowTechs,PayMax,Company_ID,Headline,Status,Qty_Devices,Project_Name";

    private $_company;

    public function init()
    {
        parent::init();

        //  This is constroller can be used for AJAX only!
        $request = $this->getRequest();
        $this->_company = $request->getParam('company', NULL);
		if (empty($this->_company))
	        $this->_company = $request->getParam('company_id', NULL);
    }

    public function findtechsAction()
    {
        $client = new Core_Api_Class();

        $params = $this->getRequest()->getParams();
       
        $result = $client->searchTechs(
            $this->_login.'|pmContext='.$this->_company,
            $this->_password,
            $params
        );
 
        if ($result->success) {
            
            // prepare array to view readable format
            $res = array();
            foreach ($result->data as $k => $r) {
                $i = (string)$r->Latitude . (string)$r->Longitude;
                if (!isset($res[$i]) ) {
                    $res[$i] = array();
                }
                $res[$i][] = $r;
            }
    
            $this->view->result = $res;
            $this->view->errors = null;
            $this->view->mapCenter = array(
                'Lat' => $this->getRequest()->getParam('ProximityLat', null), 
                'Lng' => $this->getRequest()->getParam('ProximityLng', null)
            );
    
            if (!empty($result->data) ) {
                //add used marker color to session
                if (!empty($_POST['markerColor'])) {
                    Core_Map::addUsedColorToSession($_POST['markerColor']);
                }
            }
        } else {
            $this->view->errors = $result->errors;
        }

    }
    
    
    public function findwoAction()
    {
        $client = new Core_Api_Class();

        $params = $this->getRequest()->getParams();
                
        $filters = $this->getWOSearchCritera($params);
        
		$filters->Company_ID = $params['company'];
		
        $result = $client->getWorkOrders(
            $this->_login.'|pmContext='.$this->_company,
            $this->_password,
            self::WO_FIELDS, null, null, null, $filters
        );

        if ($result->success) {
            
            // prepare array to view readable format
            $res = array();
            foreach ($result->data as $k => $r) {
                $i = (string)$r->Latitude . (string)$r->Longitude;
                if (!isset($res[$i]) ) {
                    $res[$i] = array();
                }
                $res[$i][] = $r;
            }
            $this->view->result = $res;
            $this->view->errors = null;
            $this->view->zoomInState = ($params['ZoomIn'] && !empty($params['State']) ) ? $params['State'] : '';
    
            if (!empty($result->data) ) {
                //add used marker color to session
                if (!empty($_POST['markerColor'])) {
                    Core_Map::addUsedColorToSession($_POST['markerColor']);
                }
            }
        } else {
            $this->view->errors = $result->errors;
        }
    }
    
    
    public function getwodetailsAction()
    {
        $client = new Core_Api_Class();

        $params = $this->getRequest()->getParams();
        if (!empty($params['TB_UNIDs']) ) {
            $filter = new API_WorkOrderFilter();
            $filter->TB_UNID = $params['TB_UNIDs'];
            $result = $client->getWorkOrders(
                $this->_login.'|pmContext='.$this->_company,
                $this->_password,
                self::WO_FIELDS, null, null, null, $filter
            );
            
            $this->view->result = $result->data;
        }
    }
    
    
    public function getdetailsAction()
    {
        $client = new Core_Api_Class();

        $params = $this->getRequest()->getParams();

        if (!empty($params['TechIDs']) ) {
            $filters = new API_TechFilter();
            $filters->TechIDs = $params['TechIDs'];
            
            $result = $client->searchTechs(
                $this->_login.'|pmContext='.$this->_company,
                $this->_password,
                $filters
            );
    
            $this->view->result = $result->data;
        }
    }
    
    
    /**
     * preapre Tech search criteria
     * @param array $params
     * @throws Exception
     * @return Ambigous <multitype:, string>
     */
    private function getUserSearchCriteria($params)
    {
        // coverts search form fields into SQL search criteria
        $ignoreFields = array("doSearch" => 1, "doReset" => 1, "hideBanned" => 1, "preferredOnly" => 1, "newSearchBtn" => 1, "ProximityZipCode" => 1 , "ProximityLat" => 1 , "ProximityLng" => 1, "ProximityDistance" => 1); // ignores these fields on the search form
        $searchCriteria = array();
        foreach ($params as $key => $value) {
            // parse searchCriteria
            if (array_key_exists($key, $ignoreFields) || $value == "") continue; // ignore blank fields and certain fields
            $value = caspioEscape($value);
            if ($value == "Y") $value = 1;
            if ($value == "N") $value = 0;
            switch ($key) {
                case "FLSIDExists":
                    // field not blank
                    if ($value == "1")
                        $searchCriteria[] = "ISNULL(FLSID, '') <> ''";
                    break;
                case "CompTIA":
                    $searchCriteria[] = "ISNULL($key, '') <> ''";
                    break;
                case "DellCert":
                    $searchCriteria[] = "$key LIKE '%".addslashes($value)."%'";
                    break;
                case "No_Shows":
                    if (!is_numeric($value))
                        throw new Exception("$key|must be numeric");
                    $searchCriteria[] = "$key <= '$value'";
                    break;
                case "Qty_IMAC_Calls":
                case "SATRecommendedAvg":
                case "SATPerformanceAvg":
                case "Qty_FLS_Service_Calls":
                    if (!is_numeric($value))
                        throw new Exception("$key|must be numeric");
                    $searchCriteria[] = "$key >= '$value'";
                    break;
                case "selfRating":
                    foreach ($value as $k=>$val) {
                        if (!is_numeric($val))
                        throw new Exception("$k|must be numeric");
                        $searchCriteria[] = "$k >= '$val'";
                    }
                    break;
                case "Bg_Test_Pass_Lite":
                    if (!is_numeric($value))
                        throw new Exception("$key|must be numeric");
                    $searchCriteria[] = "$key >= '$value'";
                    break;
                default:
                    $searchCriteria[] = "$key = '$value'";
                    break;
            }
        }
        return $searchCriteria;
    }
    
    
    public function getdetailformAction()
    {
        $this->view->assign = $this->getRequest()->getParam('assign', false);
        $this->view->companyId = $companyId = $this->getRequest()->getParam('company_id', '');
        
        $woIds = trim($this->getRequest()->getParam('workorders', ''));
        $client = new Core_Api_Class();
        
        //get woIds
        if (!empty($woIds)) {
            $filters = new API_WorkOrderFilter();
            $filters->TB_UNID = $woIds;
            
            $woSize    = 25;
            $woPage    = $this->getRequest()->getParam('woPage', 1);
            $woPage    = ( $woPage < 1 ) ? 1 : $woPage;
            
            $woOrderBy = $this->getRequest()->getParam('woOrderBy', 'TB_UNID');
            $woOrderType = $this->getRequest()->getParam('woOrderType', 0);
            
            $allowedSorting = array(
                'TB_UNID' => 'TB_UNID'
            );
            
            if (key_exists($woOrderBy, $allowedSorting)) {
                $woOrderBy = $allowedSorting[$woOrderBy];
            } else {
                $woOrderBy = 'TechID';
            }
            
            $countRows = 0;
            
            $result = $client->getWorkOrders(
                $this->_login.'|pmContext='.$this->_company,
                $this->_password,
                self::WO_FIELDS, null, null, null, $filters,
                ($woPage-1) * $woSize,
                $woPage * $woSize,
                $countRows,
                $woOrderBy . ($woOrderType ? ' ASC' : ' DESC')
            );

            $this->view->wo = $result;
            $this->view->woPage = $woPage;
            $this->view->woOrderBy = $woOrderBy;
            $this->view->woOrderType = $woOrderType;
            $this->view->woCount = $countRows;
        }
        
        // get techs
        $techsIds = trim($this->getRequest()->getParam('techs', ''));

        if (!empty($techsIds)) {
            $filters = new API_TechFilter();
            $filters->TechIDs = $techsIds;
            
            $techSize    = 25;
            $techPage    = $this->getRequest()->getParam('techPage', 1);
            $techPage    = ( $techPage < 1 ) ? 1 : $techPage;
            
            $techOrderBy = $this->getRequest()->getParam('techOrderBy', 'TechID');
            $techOrderType = $this->getRequest()->getParam('techOrderType', 0);
            
            $allowedSorting = array(
                'TechID' => 'TechID',
                'FLSID' => 'FLSID',
                'LastName' => 'LastName',
                'ZipCode' => 'ZipCode',
               // 'Preferred' => "(CASE WHEN(SELECT TOP 1 PreferLevel FROM Client_Preferred_Techs WHERE CompanyID = '" . Core_Caspio::caspioEscape($companyId) . "' AND TechID = Tech_ID) IS NOT NULL THEN 'Yes' ELSE 'No' END)",
                'Qty_IMAC_Calls' => 'Qty_IMAC_Calls',
                'SATRecommendedAvg' => 'SATRecommendedAvg',
                'No_Shows' => 'No_Shows'
            );
            
            $preferredList = Core_Tech::getClientPreferredTechsArray($companyId);
        	(!empty($preferredList)) ? $allowedSorting["Preferred"] = '(CASE WHEN(TechID IN ('.implode(",",$preferredList). ') THEN "Yes" ELSE "No" END)' :$allowedSorting["Preferred"] = "No";
            
            if (key_exists($techOrderBy, $allowedSorting)) {
                $techOrderBy = $allowedSorting[$techOrderBy];
            } else {
                $techOrderBy = 'TechID';
            }
            
            $countRows = null;

            $result = $client->searchTechs(
                $this->_login.'|pmContext='.$this->_company,
                $this->_password,
                $filters,
                $techPage,
                $techSize,
                $countRows,
                $techOrderBy . ($techOrderType ? ' ASC' : ' DESC')
            );

            $this->view->techs = $result;
            $this->view->techPage = $techPage;
            $this->view->techOrderBy = $techOrderBy;
            $this->view->techOrderType = $techOrderType;
            $this->view->countRows = $countRows;
        }
        
        $this->view->companyId = $this->getRequest()->getParam('company_id', '');
        
    }
    
    
    public function assignwoAction()
    {
        $this->view->result = true;
        
        $winNum = $this->getRequest()->getParam('woId', null);
        $techID = $this->getRequest()->getParam('techId', null);
        $bidAmount = $this->getRequest()->getParam('agreed', null);
        
        $client = new Core_Api_Class();
        $responce = $client->assignWorkOrder(
            $this->_login.'|pmContext='.$this->_company,
            $this->_password,
            $winNum, 
            $techID, 
            $bidAmount
        );
        $this->view->responce = $responce;
    }
    
    
    private function getWOSearchCritera($params)
    {
        $filters = new API_WorkOrderFilter;
        
        if(!empty($params['TB_UNID']) || !empty($params['WO_ID'])){
	        if (!empty($params['TB_UNID'])) {
	            $filters->TB_UNID = $params['TB_UNID'];
	        }
	        
	    	if (!empty($params['WO_ID'])) {
	            $filters->WO_ID = $params['WO_ID'];
	        }
        }else{
        
	        if (!empty($params['WorkOrder'])) {
	        	switch ($params['WorkOrder']){
	        		case 'Published':
	        			$filters->WO_State = WO_STATE_PUBLISHED;
	        			break;
	        		case 'Created':
	        			$filters->WO_State = WO_STATE_CREATED;
	        			break;
	        		case 'Assigned':
	        			$filters->WO_State = WO_STATE_ASSIGNED;
	        			break;
	        		case 'Work_Done':
	        			$filters->WO_State = WO_STATE_WORK_DONE;
	        			break;
	        		case 'Approved':
	        			$filters->WO_State = WO_STATE_APPROVED;
	        			break;
	        		case 'In_Accounting':
	        			$filters->WO_State = WO_STATE_IN_ACCOUNTING;
	        			break;
	        		case 'Incomplete':
	        			$filters->WO_State = WO_STATE_INCOMPLETE;
	        			break;
	        		case 'Complete':
	        			$filters->WO_State = WO_STATE_COMPLETED;
	        			break;
	        		case 'Deactivated':
	        			$filters->WO_State = WO_STATE_DEACTIVATED;
	        			break;
	        		case 'All':
	        		    $filters->WO_State = WO_STATE_ALL;
	        			break;
	        		default:
	        			break;
	        	}
	        }
	        
	        $companyID = '';
	        if (!empty($_SESSION['Company_ID'])) {
	            $companyID = $_SESSION['Company_ID'];
	        }
	        if (!empty($params['PMCompany'])) {
	            $companyID = $params['PMCompany'];
	        }
	        $filters->Company_ID = $companyID;
	        
	        $filters->Region = $params['Region'];
	        $filters->Route = $params['Route'];
	        
	
	        if (!empty($params['Project']) && ($params['Project'] > 0)) {
	            $filters->Project_ID = (strpos($params['Project'], ",") != false) ? explode(",", $params['Project']) : $params['Project'];
	        }
	        
	        if (!empty($params['StartDate'])) {
	            $date = new Zend_Date($params['StartDate'], 'MM/dd/yyyy');
	            $filters->StartDateFrom = $date->toString('yyyy-MM-dd');
	        }
	        
	        if (!empty($params['EndDate'])) {
	            $date = new Zend_Date($params['EndDate'], 'MM/dd/yyyy');
	            $filters->StartDateTo = $date->toString('yyyy-MM-dd');
	            
	        }
	        
	        if (!empty($params['category'])) {
	            $filters->WO_Category = (int)$params['category'];
	        }
	        
        }

        $filters->withCoordinates = true;
        
        return $filters;
    }
}

