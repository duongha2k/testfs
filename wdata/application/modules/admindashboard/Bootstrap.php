<?php
/*
 * Current class initialize Dashboard module
 */
class Admindashboard_Bootstrap extends Zend_Application_Module_Bootstrap
{
    /**
     * _initView
     *
     * Initialize View
     *
     * @access protected
     * @return Zend_View
     */
    protected function _initView()
    {
        //$this->bootstrap('Defines');

        $viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();

        $view = new Zend_View();
        $view->setHelperPath(ADMINDASHBOARD_MODULE_PATH.'views/helpers/', 'Admindashboard_View_Helper');
        $view->strictVars(true);

        $viewRenderer->setView($view);
        Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
        /* View scripts paths defined in Core_Controller_Action */

        return $view;
    }
}

