<?php
class Admindashboard_View_Helper_DisplayAmountPer extends Zend_View_Helper_Abstract
{
    public function displayAmountPer($amountPer)
    {
        switch ( trim(strtolower($amountPer)) ) {
        case 'hour':
            return 'per hour';
		case 'site':
            return 'per site';
		case 'device':
            return 'per device';
        default :
            return $amountPer;
        }
    }
}

