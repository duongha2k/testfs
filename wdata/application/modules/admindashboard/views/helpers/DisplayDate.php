<?php
class Admindashboard_View_Helper_DisplayDate extends Zend_View_Helper_Abstract
{
    /**
     * displayDate 
     * 
     * @param string $format String format same as format in php 'date' function
     * @param string $strDate  Date in format 'n/j/Y H:i:s A' -> 12/8/2008 12:00:00 AM
     * @access public
     * @return string|false
     */
    public function displayDate($format, $strDate)
    {
        if (!empty($strDate) ) { 
            $stamp = strtotime($strDate);
            if ( $stamp && $stamp !== -1 )
                return (date($format, $stamp));
        }
        return '';
    }
}

