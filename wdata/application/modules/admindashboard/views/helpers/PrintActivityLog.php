<?php

require_once 'Zend/View/Helper/Abstract.php';

class Admindashboard_View_Helper_PrintActivityLog extends Zend_View_Helper_Abstract
{
    /**
     * printActivityLog
     * 
     * @param array $timestamps Array of API_Timestamp objects
     * @param int $win 
     * @param string $rowSeparator 
     * @access public
     * @return string
     */
    public function printActivityLog( array $timestamps, $win, $rowSeparator = '<br />' )
    {
        if ( empty($timestamps) || empty($win) )
            return $rowSeparator;

        $output = '';
        if ( !empty($timestamps[$win]->data) ) {
            if ( is_array($timestamps[$win]->data) ) {
                for ($i = 0; $i < count($timestamps[$win]->data); ++$i ) {
                    $ts =& $timestamps[$win]->data[$i];
                    $output .= $this->view->displayDate('n/j/y; H:i ', $ts->dateTime);
                    $output .= str_replace(array(',', ';', ':', '.', '-'), array(', ', '; ', ': ', '- '), $ts->description);
                    $output .= ( ($i+1 !== count($timestamps[$win]->data)) ? $rowSeparator : '' );
                }
            }
        }
        return $output;
    }
}

