<?php
class Admindashboard_View_Helper_Truncate extends Zend_View_Helper_Abstract
{
    public function truncate($string, $length = 50, $postfix = '&hellip;', $escape = true)
    {
        $truncated = trim($string);
        $length = (int)$length;
        if (!$string) {
            return $truncated;
        }
        $fullLength = iconv_strlen($truncated, 'UTF-8');
        if ($fullLength > $length) {
            $truncated = trim(iconv_substr($truncated, 0, $length, 'UTF-8'));
            if ($escape) {
                $truncated = htmlspecialchars($truncated);
            }
            $truncated .= $postfix;
        } else if ($escape) {
            $truncated = htmlspecialchars($truncated);
        }
        return $truncated;   
    }
}