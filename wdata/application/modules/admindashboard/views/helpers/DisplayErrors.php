<?php
class Admindashboard_View_Helper_DisplayErrors extends Zend_View_Helper_Abstract
{
    /**
     * displayErrors  
     * 
     * @param array $errors Array of API_Error objects
     * @param string $message Message for users on production server
     * @access public
     * @return string
     */
    public function displayErrors(array $errors, $message)
    {
        $output = '';
        if ( APPLICATION_ENV !== 'production' ) {
            foreach ($errors as $error )
                $output .= '<span class="errorMsg"><b>Code:</b> '.$error->code.'; <b>Type:</b> '.$error->type.'; <b>Message:</b> '.$error->message.'</span>';
        } else {
            $output = '<span class="errorMsg">'.$message.'</span>';
        }

        return $output;
    }
}

