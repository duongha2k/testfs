<?php
class Admindashboard_View_Helper_MinutesToAnyFull extends Zend_View_Helper_Abstract
{
    /**
     * minutesToAnyFull 
     * 
     * Function divide number of $minutes into full days,
     * hour or minutes depend on $any parameter
     * @param int $minutes 
     * @param string $any 
     * @access public
     * @return int
     */
    public function minutesToAnyFull($minutes, $any)
    {
        $minutes = $minutes + 0; // convert to number
        if ( empty($minutes) ) {
            return 0;
        }

        $_d_min = 24*60;    // minutes in a day
        $_h_min = 60;       // minutes in a hour

        switch ( strtolower(trim($any)) ) {
        case 'days':
            return (int)floor($minutes / $_d_min);
            break;
        case 'hours':
            return (int)floor($minutes % $_d_min / $_h_min);
            break;
        case 'minutes':
            return $minutes % $_d_min % $_h_min;
            break;
        default :
            throw new Zend_Exception('Incorrect parameter any: "'.$any.'"');
        }
    }
}

