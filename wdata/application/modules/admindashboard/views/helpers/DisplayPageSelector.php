<?php
class Admindashboard_View_Helper_DisplayPageSelector extends Zend_View_Helper_Abstract
{
    /**
     * displayPageSelector 
     * 
     * @param string|float $amount
     * @param string $currencySymbol
     * @access public
     * @return string
     */
    public function displayPageSelector($currentSelector)
    {
        $pager = array(
            '10' => '10',
            '25' => '25',
            '50' => '50',
            '100' => '100'
        );
        
        $pagerLine = ' <select id="pageSizeSelector" name="pageSizeSelector" onchange="onChangePageSize();">';
        foreach ($pager as $pKey => $pValue) {
            $pagerLine .= "<option value=\"{$pKey}\"" . ( ((int)$pKey) == $currentSelector ? " selected=\"selected\" " : "") . ">{$pValue}</option>";
        }
        $pagerLine .= '</select>';
        
        return $pagerLine;
    }
}

