<?php
class Admindashboard_View_Helper_DisplayAddress extends Zend_View_Helper_Abstract
{
    /**
     * displayAddress  
     * 
     * Function return address string for display in browser
     *
     * @param string $city
     * @param string $state 
     * @param string $zip
     * @param string $address
     * @access public
     * @return string
     */
    public function displayAddress($city, $state, $zip, $address = null)
    {
        $city   = trim($city);
        $state  = trim($state);
        $zip    = trim($zip);
        $output = $city;
        $delimeter  = '';

        if ( $output !== '' && (!empty($state) || !empty($zip)) ) {
            $output .= ', ';
        }
        $output .= $state;
        if ( !empty($state) && !empty($zip) ) {
            $output .= ' ';
        }
        $output .= $zip;
        if ( !empty($address)) {
            if( !empty($output) )
                $output = $address.', '.$output;
            else
                $output = $address;
        }

        return trim($output);
    }
}

