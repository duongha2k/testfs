<?php
class Admindashboard_View_Helper_DisplayMiles extends Zend_View_Helper_Abstract
{
    public function displayMiles( $miles )
    {
        if ( !is_numeric($miles) || !is_string($miles) ) {
            $miles = 0;
        }
        $miles += 0;    // Cast to float or int

        return sprintf("%.2f", round($miles, 2));
    }
}

