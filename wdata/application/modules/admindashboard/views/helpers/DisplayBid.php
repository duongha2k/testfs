<?php

class Admindashboard_View_Helper_DisplayBid extends Zend_View_Helper_Abstract {

    /**
     * displayBid  
     * 
     * @param string|float $cash 
     * @param string $per 
     * @access public
     * @return string
     */
    public function displayBid($cash, $per) {
        $per = strtolower(trim($per));

        $cash = trim($cash);
        if (strlen($cash) && $cash{0} === '$') {
            $cash = substr($cash, 1);
        } else if (empty($cash)) {
            return "";
        }

        $cash = sprintf("%.2f", $cash + 0);

        if (!empty($per)) {
            return "$cash / $per";
        } else {
            return $cash;
        }
    }

}

